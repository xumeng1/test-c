<?xml version="1.0"?>
<doc>
    <assembly>
        <name>PcasDataWrite</name>
    </assembly>
    <members>
        <member name="T:Bookham.TestLibrary.ExternalData.PCASdropFile">
            <summary>
            Creates a PCASdropFile object.
            </summary>
        </member>
        <member name="M:Bookham.TestLibrary.ExternalData.PCASdropFile.#ctor(System.String,System.String,System.String)">
            <summary>
            Creates a PCASdropFile object.
            </summary>
            <param name="localDir">The folder in which to create drop files before sending to the drop folder</param>
            <param name="dropDir">The root folder from which PCAS picks up drop files</param>
            <param name="nodeId">The nodeId of the Test Set. The PCAS Streamer Picks up data from the dropDir/nodeId subdirectory</param>
        </member>
        <member name="M:Bookham.TestLibrary.ExternalData.PCASdropFile.MoveToDropDir">
            <summary>
            Move the local drop file to the PCAS drop folder.
            </summary>
            <returns>final drop file path string</returns>
        </member>
        <member name="M:Bookham.TestLibrary.ExternalData.PCASdropFile.createLocal(System.String,System.String,System.String,System.String,System.Collections.Specialized.StringDictionary,System.Boolean)">
            <summary>
            Creates a drop file on the local PC,
            before moving it to the PCAS drop folder.
            </summary>
            <param name="deviceType">The PCAS device type</param>
            <param name="stage">The PCAS test stage</param>
            <param name="specName">The PCAS spec_id</param>
            <param name="compID">The PCAS serial number</param>
            <param name="resultList">The result list to be written</param>
            <param name="addTimeDate">Whether to add the TimeDate to the results.</param>
        </member>
        <member name="M:Bookham.TestLibrary.ExternalData.PCASdropFile.addParam(System.String,System.String)">
            <summary>
            Adds a param / value pair to the output data
            </summary>
            <param name="name">Name of param to add</param>
            <param name="val">Value to be added</param>
        </member>
        <member name="M:Bookham.TestLibrary.ExternalData.PCASdropFile.dropfileAsText(System.String,System.String,System.String,System.String,System.Collections.Specialized.StringDictionary,System.Boolean)">
            <summary>
            Create the text to go into the drop file
            </summary>
            <param name="deviceType">PCAS device type e.g. inp_mz</param>
            <param name="stage">PCAS test stage e.g. coc_final</param>
            <param name="specName">PCAS spec name</param>
            <param name="serialNumber">The DUT component ID e.g. CL12345.001</param>
            <param name="addTimeDate">If true, add any 'TIME_DATE' parameter appearing in the user supplied data</param>
            <param name="resultList">List of name value pairs to create drop file with.</param>		
            <returns>The text to go into the drop file</returns>
        </member>
        <member name="F:Bookham.TestLibrary.ExternalData.PCASdropFile.localDir">
            <summary>
            Temporary drop area
            </summary>
        </member>
        <member name="F:Bookham.TestLibrary.ExternalData.PCASdropFile.dropDir">
            <summary>
            Final drop area
            </summary>
        </member>
        <member name="F:Bookham.TestLibrary.ExternalData.PCASdropFile.filename">
            <summary>
            Name of drop file.
            </summary>
        </member>
        <member name="F:Bookham.TestLibrary.ExternalData.PCASdropFile.localFilePath">
            <summary>
            Local file path
            </summary>
        </member>
        <member name="F:Bookham.TestLibrary.ExternalData.PCASdropFile.dropFilePath">
            <summary>
            Final file path
            </summary>
        </member>
        <member name="F:Bookham.TestLibrary.ExternalData.PCASdropFile.fileContents">
            <summary>
            The contents of the drop file in string format.
            </summary>
        </member>
        <member name="T:Bookham.TestLibrary.ExternalData.PcasDataWrite">
            <summary>
            Implementation of IDataWrite for PCAS.
            </summary>
        </member>
        <member name="F:Bookham.TestLibrary.ExternalData.PcasDataWrite.LogLabel">
            <summary>
            Log application label string to "mark" log entry as originating from this domain.
            </summary>
        </member>
        <member name="F:Bookham.TestLibrary.ExternalData.PcasDataWrite.PositiveInfinityTokenString">
            <summary>
            Constant special token string value to represent +ve Infinity and NaN.
            <para>
            The PCAS software (written by Ian Gurney) expects this value.
            The value depended on VB 6.0 requirements and hence fits in a Single float type.
            </para>
            </summary>
        </member>
        <member name="F:Bookham.TestLibrary.ExternalData.PcasDataWrite.NegativeInfinityTokenString">
            <summary>
            Constant special token string value to represent -ve Infinity.
            <para>
            The PCAS software (written by Ian Gurney) expects this value.
            The value depended on VB 6.0 requirements and hence fits in a Single float type.
            </para>
            </summary>
        </member>
        <member name="M:Bookham.TestLibrary.ExternalData.PcasDataWrite.#ctor(Bookham.TestEngine.PluginInterfaces.ExternalData.ILogging,System.String)">
            <summary>
            Default constructor.
            </summary>
            <param name="logger">Reference to logging component</param>
            <param name="dropDirectory">Path to the location of the drop directory. The directory 'pcasdrop' will be created in the application base directory if this parameter is empty.</param>		
        </member>
        <member name="M:Bookham.TestLibrary.ExternalData.PcasDataWrite.createDirectory(System.String,Bookham.TestLibrary.ExternalData.PcasDataWrite)">
            <summary>
            Creates a directory.
            </summary>
            <param name="directoryName">Name of directory to create.</param>
            <param name="thisObject">This Object</param>
        </member>
        <member name="M:Bookham.TestLibrary.ExternalData.PcasDataWrite.SetBlobDataDropDirectory">
            <summary>
            Sets the location of the Blob data streamer drop directory.
            This is found in configuration file PcasWriteConfig.xml, if present.
            If no alternative configuration is given, then the Blob data streamer drop directory 
            is left at the default of ./pcasBlobDataStreamerDrop.
            </summary>
        </member>
        <member name="M:Bookham.TestLibrary.ExternalData.PcasDataWrite.Write(Bookham.TestEngine.Framework.Limits.TestData,System.Collections.Specialized.StringDictionary,System.String,System.Boolean,System.Boolean)">
            <summary>
            Extracts test data that matches specification specName. Creates a drop file in pcas format.
            </summary>
            <param name="testData">The set of testData gathered so far.</param>
            <param name="keyData">Must contain SCHEMA, SERIAL_NUMBER, DEVICE_TYPE and TEST_STAGE</param>
            <param name="specName">A name of a specification within TestData</param>
            <param name="ignoreMissing">Whether to ignore null result values within the test data.</param>
            <returns>Was there missing data? Always false if "ignoreMissing" is true</returns>
        </member>
        <member name="F:Bookham.TestLibrary.ExternalData.PcasDataWrite.dropArea">
            <summary>
            Location of the drop area used for writing PCAS data.
            </summary>
        </member>
        <member name="F:Bookham.TestLibrary.ExternalData.PcasDataWrite.blobDataStreamerDropDirectory">
            <summary>
            Location of the drop directory for Blob data. Set to pcasBlobDataStreamerDrop subdirectory 
            from the TestEngine.Core.exe by default. This value can be changed if a 
            PcasWriteconfig.xml file is present, and contains the RawDataDropDirectory tag with 
            a new Blob Data streamer directory specified.
            </summary>
        </member>
        <member name="F:Bookham.TestLibrary.ExternalData.PcasDataWrite.renameBlobDataFiles">
            <summary>
            It is usually a good idea to name blob data files uniquely when
            moving them to their final destination directory.
            However, there are some circumstances where use of certain data viewing 
            tools is made easier when the original name is preserved.
            </summary>
        </member>
        <member name="M:Bookham.TestLibrary.ExternalData.PcasDataWrite.CheckSchema(System.String)">
            <summary>
            Checks that the schema exists in PCASschema
            </summary>
            <param name="schemaName">The name of the schema to check.</param>
            <returns>TRUE if the schema exists.</returns>
        </member>
        <member name="M:Bookham.TestLibrary.ExternalData.PcasDataWrite.CheckKeyData(System.Collections.Specialized.StringDictionary)">
            <summary>
            Checks for existence of keys.
            </summary>		
            <param name="keyData">Collection of key information</param>
        </member>
        <member name="F:Bookham.TestLibrary.ExternalData.PcasDataWrite.logger">
            <summary>
            Reference to component providing logging functionality.
            </summary>
        </member>
        <member name="F:Bookham.TestLibrary.ExternalData.PcasDataWrite.nodeId">
            <summary>
            Node Id for the Test Set, for the current Test Stage.
            </summary>
        </member>
        <member name="T:Bookham.TestLibrary.ExternalData.PCASschema">
            <summary>
            A list of schemas currently supported by PCAS.
            </summary>
        </member>
    </members>
</doc>

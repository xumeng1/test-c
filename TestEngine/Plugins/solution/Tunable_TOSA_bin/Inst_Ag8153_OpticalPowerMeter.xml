<?xml version="1.0"?>
<doc>
    <assembly>
        <name>Inst_Ag8153_OpticalPowerMeter</name>
    </assembly>
    <members>
        <member name="T:Bookham.TestLibrary.Instruments.Inst_Ag8153_OpticalPowerMeter">
            <summary>
            Class implementing the instrument commands for an 8153 optical power meter
            </summary>
        </member>
        <member name="M:Bookham.TestLibrary.Instruments.Inst_Ag8153_OpticalPowerMeter.#ctor(System.String,System.String,System.String,System.String,Bookham.TestEngine.PluginInterfaces.Chassis.Chassis)">
            <summary>
            Instrument Constructor.
            </summary>
            <param name="instrumentNameInit">Instrument name</param>
            <param name="driverNameInit">Instrument driver name</param>
            <param name="slotInit">Slot ID for the instrument</param>
            <param name="subSlotInit">Sub Slot ID for the instrument</param>
            <param name="chassisInit">Chassis through which the instrument communicates</param>
        </member>
        <member name="M:Bookham.TestLibrary.Instruments.Inst_Ag8153_OpticalPowerMeter.SetDefaultState">
            <summary>
            Set instrument to default state
            </summary>
        </member>
        <member name="M:Bookham.TestLibrary.Instruments.Inst_Ag8153_OpticalPowerMeter.ReadPower">
            <summary>
            Initiate and return a power measurement. Returned units depend 
            on the current mode of the instrument (dBm, mW, dB). 
            </summary>
        </member>
        <member name="M:Bookham.TestLibrary.Instruments.Inst_Ag8153_OpticalPowerMeter.ZeroDarkCurrent_Start">
            <summary>
            Starts a calibration of power meter dark-current 
            value on the power meter channel. For this calibration to succeed the meter must 
            have zero light input. This function returns immediately once the command
            has been sent to the instrument (Async command).
            </summary>
            <remarks>The same value affects both channel 1 and 2, so sets may have 
            unforeseen consequences</remarks>
        </member>
        <member name="M:Bookham.TestLibrary.Instruments.Inst_Ag8153_OpticalPowerMeter.ZeroDarkCurrent_End">
            <summary>
            Ends a calibration of optical dark-current. Waits for a previously started
            dark current cal to complete.
            </summary>
            <remarks>
            N.B.: The same value affects both channel 1 and 2, so sets may have 
            unforeseen consequences</remarks>
        </member>
        <member name="M:Bookham.TestLibrary.Instruments.Inst_Ag8153_OpticalPowerMeter.ReadPowerMaxMin">
            <summary>
            Return  the max and min power readings observed since the last activation 
            of Max/Min Hold Mode.
            </summary>
        </member>
        <member name="M:Bookham.TestLibrary.Instruments.Inst_Ag8153_OpticalPowerMeter.getPowerReadingAndCheckInRange(System.String)">
            <summary>
            Command to get the power meter reading and check if it is in range
            </summary>
            <param name="command">GPIB command to get the power meter reading</param>
            <returns>The reading</returns>
        </member>
        <member name="M:Bookham.TestLibrary.Instruments.Inst_Ag8153_OpticalPowerMeter.SetContinuousModeOff">
            <summary>
            put this entire slot into non continuous mode
            </summary>
        </member>
        <member name="M:Bookham.TestLibrary.Instruments.Inst_Ag8153_OpticalPowerMeter.chassisWrite(System.String)">
            <summary>
            Chassis "Write" command - write to the chassis to change its state
            </summary>
            <param name="command">Command to send</param>
        </member>
        <member name="M:Bookham.TestLibrary.Instruments.Inst_Ag8153_OpticalPowerMeter.chassisQuery(System.String)">
            <summary>
            Chassis "Query" command - read some information from the chassis
            </summary>
            <param name="command">Command to send</param>
        </member>
        <member name="M:Bookham.TestLibrary.Instruments.Inst_Ag8153_OpticalPowerMeter.inAutoRangeMode">
            <summary>
            Is the instrument in autoranging mode
            </summary>
            <returns>true if autoranging, false otherwise</returns>
        </member>
        <member name="M:Bookham.TestLibrary.Instruments.Inst_Ag8153_OpticalPowerMeter.checkSlotNotEmpty">
            <summary>
            Check that the slot isn't empty, and return the product id string. This may happen before errors setup, so 
            DON'T called _Checked Chassis methods.
            </summary>
        </member>
        <member name="F:Bookham.TestLibrary.Instruments.Inst_Ag8153_OpticalPowerMeter.instrumentChassis">
            <summary>
            Instrument's chassis
            </summary>
        </member>
        <member name="F:Bookham.TestLibrary.Instruments.Inst_Ag8153_OpticalPowerMeter.currMode">
            <summary>
            Current meter mode state cache
            </summary>
        </member>
        <member name="F:Bookham.TestLibrary.Instruments.Inst_Ag8153_OpticalPowerMeter.maxPowerRange_dBm">
            <summary>
            Maximum power range supported by this Chassis
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.Instruments.Inst_Ag8153_OpticalPowerMeter.FirmwareVersion">
            <summary>
            Firmware version of this instrument.
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.Instruments.Inst_Ag8153_OpticalPowerMeter.HardwareIdentity">
            <summary>
            Hardware Identity of this instrument.
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.Instruments.Inst_Ag8153_OpticalPowerMeter.IsOnline">
            <summary>
            Setup the instrument as it goes online
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.Instruments.Inst_Ag8153_OpticalPowerMeter.Mode">
            <summary>
            Get / Set the mode of optical power meter. 
            If a set of a mode which is not supported by this instrument is attempted, 
            an exception should be thrown.
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.Instruments.Inst_Ag8153_OpticalPowerMeter.Wavelength_nm">
            <summary>
            Reads/sets the expected input signal wavelength in nanometres
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.Instruments.Inst_Ag8153_OpticalPowerMeter.CalOffset_dB">
            <summary>
            Get / Set calibration offset in decibels.
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.Instruments.Inst_Ag8153_OpticalPowerMeter.ReferencePower">
            <summary>
            Get/Set reference power for relative measurements. Uses the 
            current MeterMode for units.
            MeterMode.Absolute_dBm -> dBm
            MeterMode.Absolute_mW  -> mW
            MeterMode.Relative_dB  -> dBm
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.Instruments.Inst_Ag8153_OpticalPowerMeter.Range">
            <summary>
            Sets/Returns the measurement range. Units depend on current mode.
            The range method guarantees to set a power at or *above* that specified in this function.
            I.e. the power level specified in the set is guaranteed to be measureable in this mode.        
            </summary>
            <remarks>N.B.: Manual ranging is independent of channel!</remarks>
        </member>
        <member name="P:Bookham.TestLibrary.Instruments.Inst_Ag8153_OpticalPowerMeter.AveragingTime_s">
            <summary>
            Sets/returns the measurement averaging value in seconds for the power meter channel.
            </summary>
            <remarks>
            The same value affects both channel 1 and 2, so sets may have 
            unforeseen consequences</remarks>
        </member>
        <member name="P:Bookham.TestLibrary.Instruments.Inst_Ag8153_OpticalPowerMeter.MaxMinOn">
            <summary>
            Get/Set Enable/Disable Min/Max hold function.
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.Instruments.Inst_Ag8153_OpticalPowerMeter.SafeMode">
            <summary>
            SafeMode flag. If true, instrument will always wait for completion after every command and check
            the error registers. If false it will do neither.
            </summary>
        </member>
    </members>
</doc>

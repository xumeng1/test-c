<?xml version="1.0"?>
<doc>
    <assembly>
        <name>Inst_Nt10a</name>
    </assembly>
    <members>
        <member name="T:Bookham.TestLibrary.Instruments.TecsDataSetPoints">
            <summary>
            Internal class to act as a container for the tecs set point values
            </summary>
        </member>
        <member name="T:Bookham.TestLibrary.Instruments.TecsDataValues">
            <summary>
            Internal class to act as a container for the tecs data values
            </summary>
        </member>
        <member name="T:Bookham.TestLibrary.Instruments.Inst_Nt10a">
            <summary>
            Instrument driver for KD Optics Nt10a temperature controller unit
            </summary>
        </member>
        <member name="M:Bookham.TestLibrary.Instruments.Inst_Nt10a.#ctor(System.String,System.String,System.String,System.String,Bookham.TestEngine.PluginInterfaces.Chassis.Chassis)">
            <summary>
            Constructor.
            </summary>
            <param name="instrumentName">Instrument name</param>
            <param name="driverName">Instrument driver name</param>
            <param name="slotId">Slot ID for the instrument</param>
            <param name="subSlotId">Sub Slot ID for the instrument</param>
            <param name="chassis">Chassis through which the instrument communicates</param>
        </member>
        <member name="M:Bookham.TestLibrary.Instruments.Inst_Nt10a.SetDefaultState">
            <summary>
            Configures the instrument into a default state.
            </summary>
        </member>
        <member name="M:Bookham.TestLibrary.Instruments.Inst_Nt10a.convertThermistorResistanceKohmsToTemperatureDegC(System.Double)">
            <summary>
            Convert a thermistor resistnce Value (KOhms) into a Temperature in 
            Degrees Celsius, using the Steinhart-Hart equation.
            </summary>
            <param name="resistance">Resistance (KOhms) to be converted.</param>
            <returns>The Temperature in Celsius.</returns>
        </member>
        <member name="M:Bookham.TestLibrary.Instruments.Inst_Nt10a.convertTemperatureDegCToThermistorResistanceKohms(System.Double)">
            <summary>
            Convert a temperature in Degrees Celsius to a Thermistor Resistance (K ohms)
            using the Steinhart-Hart equation.
            </summary>
            <param name="temperature">The Temperature in Celsius.</param>
            <returns>Calculated Resistance (K ohms)</returns>
        </member>
        <member name="M:Bookham.TestLibrary.Instruments.Inst_Nt10a.ReadTecDataSetPoints">
            <summary>
            This function will read the tec controllers set point values,
            and can decipher if we are in temperature or resistance mode of operation
            Note: response[0] will be a string indicating set point temperature or resistance, it
            will have a T or a P on the end to indicate if we are in thermistor (Resistance, T) or PRT (Temperature, P) mode.
            </summary>
            <returns>TecsDataSetPoints structure</returns>
        </member>
        <member name="M:Bookham.TestLibrary.Instruments.Inst_Nt10a.ReadTecsDataValues">
            <summary>
            This function will read the tec controllers set point values,
            and can decipher if we are in temperature or resistance mode of operation
            Note: response[0] will be a string indicating actual temperature or resistance, it
            will have a T or a P on the end to indicate if we are in thermistor (Resistance, T) or PRT (Temperature, P) mode.
            </summary>
            <returns>TecsDataValues structure</returns>
        </member>
        <member name="M:Bookham.TestLibrary.Instruments.Inst_Nt10a.getDblFromString(System.String,System.String)">
            <summary>
            Helper function to convert a string to a double, catching any errors that occur.
            </summary>
            <param name="command">Command that was sent</param>
            <param name="response">Response to convert</param>
            <returns>The double value</returns>
        </member>
        <member name="M:Bookham.TestLibrary.Instruments.Inst_Nt10a.getboolFromTecString(System.String,System.String)">
            <summary>
            Helper function dealing specifically with this tec controllers outputs,
            will decode the F or O that the tec controller provides into a true or false
            </summary>
            <param name="command">the command sent to instrument</param>
            <param name="response">the response of the instrument</param>
            <returns>bool indication of what the tec controller replies</returns>
        </member>
        <member name="M:Bookham.TestLibrary.Instruments.Inst_Nt10a.getIntFromString(System.String,System.String)">
            <summary>
            Helper function to convert a string to a int32, catching any errors that occur.
            </summary>
            <param name="command">Command that was sent</param>
            <param name="response">Response to convert</param>
            <returns>The int32 value</returns>
        </member>
        <member name="M:Bookham.TestLibrary.Instruments.Inst_Nt10a.Write(System.String)">
            <summary>
            Write and checks for error or just writes, depending on safeModeOperation
            </summary>
            <param name="command">the command we wish to write to chassis</param>
        </member>
        <member name="M:Bookham.TestLibrary.Instruments.Inst_Nt10a.Query(System.String)">
            <summary>
            Query, depending on safemodeoperation will check for error as well
            </summary>
            <param name="command"></param>
            <returns>the output from the instrument</returns>
        </member>
        <member name="P:Bookham.TestLibrary.Instruments.Inst_Nt10a.HardwareIdentity">
            <summary>
            Unique hardware identification string
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.Instruments.Inst_Nt10a.FirmwareVersion">
            <summary>
            Hardware firmware version
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.Instruments.Inst_Nt10a.SafeModeOperation">
            <summary>
            Gets / sets whether we are in safe mode of operation, 
            we are in this mode by default
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.Instruments.Inst_Nt10a.OutputEnabled">
            <summary>
            Allows us to enable the output
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.Instruments.Inst_Nt10a.OperatingMode">
            <summary>
            Gets or Sets the operating mode of the Nt10a, only expects Resistance or Temperature
            This cmd will result in output being OFF if mode is switched from existing mode to a new one.
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.Instruments.Inst_Nt10a.ProportionalGain">
            <summary>
            Sets/returns the Proportional Gain Constant.
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.Instruments.Inst_Nt10a.DerivativeGain">
            <summary>
            Sets/returns the Derivative Gain Constant. Not Supported by this instrument.
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.Instruments.Inst_Nt10a.IntegralGain">
            <summary>
            Sets/returns the Integral Gain Constant.
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.Instruments.Inst_Nt10a.Sensor_Type">
            <summary>
            Gets the sensor type thermistor or rtd, doesn't support set operations
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.Instruments.Inst_Nt10a.SteinhartHartConstants">
            <summary>
            Sets/returns the Thermistor Sensor Steinhart-Hart Equation coefficients. 
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.Instruments.Inst_Nt10a.CallendarVanDusenConstants">
            <summary>
            Sets/returns the RTD Sensor Callendar-Van Dusen Equation coefficients. 
            Not supported for this instrument, as it only supports IEC 755 PT100 compliant 
            RTDs. The instrument carries out the calculations converting from resistance to 
            Temperature and vice versa internally.
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.Instruments.Inst_Nt10a.SensorTemperatureSetPoint_C">
            <summary>
            Sets/returns the temperature set point.
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.Instruments.Inst_Nt10a.SensorTemperatureActual_C">
            <summary>
            Returns the actual temperature.
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.Instruments.Inst_Nt10a.SensorResistanceSetPoint_ohm">
            <summary>
            Set/returns the sensor resistance  set point
            Note that set should throw an exception if the controller is not 
            operating in the appropriate mode 
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.Instruments.Inst_Nt10a.SensorResistanceActual_ohm">
            <summary>
            Returns the actual sensor resistance
            Note that get should throw an exception if the controller is not 
            operating in the appropriate mode 
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.Instruments.Inst_Nt10a.SensorCurrent_amp">
            <summary>
            This functionality is not supported by this Tec unit
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.Instruments.Inst_Nt10a.TecCurrentSetPoint_amp">
            <summary>
            Set/returns the peltier current set point
            This operation is not supported by this instrument. 
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.Instruments.Inst_Nt10a.TecCurrentActual_amp">
            <summary>
            Returns the actual TEC (peltier) current.
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.Instruments.Inst_Nt10a.TecVoltageSetPoint_volt">
            <summary>
            Set/returns the TEC voltage set point
            Not suppored by this instrument.
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.Instruments.Inst_Nt10a.TecVoltageActual_volt">
            <summary>
            Returns the actual TEC voltage
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.Instruments.Inst_Nt10a.TecResistanceDC_ohm">
            <summary>
            This functionality is not supported by this Tec unit
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.Instruments.Inst_Nt10a.TecResistanceAC_ohm">
            <summary>
            This functionality is not supported by this Tec unit
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.Instruments.Inst_Nt10a.TecCurrentCompliance_amp">
            <summary>
            Get or set the maximum current the Tec unit can supply
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.Instruments.Inst_Nt10a.TecVoltageCompliance_volt">
            <summary>
            This functionality is not supported by this Tec unit
            </summary>
        </member>
    </members>
</doc>

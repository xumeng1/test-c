using System;
using Bookham.TestEngine.PluginInterfaces.TestJig;
using Bookham.TestEngine.Equipment;



namespace Bookham.TestSolution.TestJig
{
	/// <summary>
	/// Summary description for myTestJig.
	/// </summary>
	public class MyTestJig : ITestJig
	{

		public InstrumentCollection InstCollection
		{
			set
			{
				this.instCollection = value;
			}
		}
		public ChassisCollection ChassisCollection
		{
			set
			{
				this.chassisCollection = value;
			}
		}

		public string GetJigID( bool InDebugMode, bool InSimulationMode )
		{
			return "A";
		}


		private InstrumentCollection instCollection;
		private ChassisCollection chassisCollection;

	}
}

Imports Bookham.TestEngine.PluginInterfaces.Module
Imports Bookham.TestEngine.Framework.InternalData
Imports Bookham.TestEngine.Equipment

Public Class VBDemoModule
    Implements ITestModule


    Public Function DoTest(ByVal engine As ITestEngine, ByVal userType As ModulePrivilegeLevel, ByVal configData As DatumList, ByVal instruments As InstrumentCollection, ByVal chassis As Bookham.TestEngine.Equipment.ChassisCollection, ByVal calData As Bookham.TestEngine.Framework.InternalData.DatumList, ByVal previousTestData As Bookham.TestEngine.Framework.InternalData.DatumList) As Bookham.TestEngine.Framework.InternalData.DatumList Implements ITestModule.DoTest
        engine.SendToGui(configData.ReadString("Message"))
        engine.GuiShow()
        engine.GuiToFront()
        engine.ReceiveFromGui()
        Return Nothing
    End Function

    Public ReadOnly Property UserControl() As System.Type Implements ITestModule.UserControl
        Get
            Return GetType(VBTestModuleCtrl)
        End Get
    End Property
End Class

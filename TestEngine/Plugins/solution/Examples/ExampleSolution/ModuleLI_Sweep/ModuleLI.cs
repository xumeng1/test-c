using System;
using System.Collections;
using System.IO;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Equipment;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestEngine.Framework.InternalData;


namespace ModuleLI_Sweep
{
	/// <summary>
	///  Structure holding the sweep data - as an ArrayList so that this can be easily
	///  updated and plotted as the sweep progresses.
	/// </summary>
	internal class LI_Sweep_Data
	{		
		/// <summary>
		/// Constructor taking the number of points to set the ArrayLists' capacity to.
		/// </summary>
		/// <param name="numberOfPoints"></param>
		internal LI_Sweep_Data(int numberOfPoints)
		{
			this.SetLaserCurrent = new ArrayList(numberOfPoints);
			this.ReadLaserCurrent = new ArrayList(numberOfPoints);
			this.LaserVoltage = new ArrayList(numberOfPoints);
			this.Bfm1Current = new ArrayList(numberOfPoints);
			this.Bfm2Current = new ArrayList(numberOfPoints);
		}
		
		/// <summary>
		/// Set laser current
		/// </summary>
		internal ArrayList SetLaserCurrent;
		
		/// <summary>
		/// Laser current as read back by meter
		/// </summary>
		internal ArrayList ReadLaserCurrent;

		/// <summary>
		/// Laser voltage
		/// </summary>
		internal ArrayList LaserVoltage;

		/// <summary>
		/// Back facet 1 current
		/// </summary>
		internal ArrayList Bfm1Current;

		/// <summary>
		/// Back facet 2 current
		/// </summary>
		internal ArrayList Bfm2Current;		
	}


	/// <summary>
	/// Module LI sweep function (stepped)
	/// </summary>
	public class ModuleLI_Sweep : ITestModule
	{
		#region ITestModule Members

		public Type UserControl
		{
			get
			{
				return typeof(ModuleLICtrl);
			}
		}

		public DatumList DoTest(ITestEngine engine, 
			ModulePrivilegeLevel userType, 
			DatumList configData, InstrumentCollection instrs, 
			ChassisCollection chassis, DatumList calData, DatumList previousTestData)
		{
			engine.GuiShow();
			engine.GuiToFront();

			DatumList moduleResults = new DatumList();

			// Set up instruments
			InstType_ElectricalSource laserI = null;
			InstType_ElectricalSource bfm1 = null;
			InstType_ElectricalSource bfm2 = null;

			if (!engine.IsSimulation)
			{
				laserI = (InstType_ElectricalSource) instrs["CurrentSrc"];
				bfm1 = (InstType_ElectricalSource) instrs["Bfm1"];
				bfm2 = (InstType_ElectricalSource) instrs["Bfm2"];

			}

			// Get params for sweep
			double startCurrent = configData.ReadDouble("StartCurrent");
			double stopCurrent = configData.ReadDouble("StopCurrent");
			double stepCurrent = configData.ReadDouble("StepCurrent");
			int stepTime_ms = (int) (configData.ReadDouble("StepDelay_sec") * 1000);
			
			int numberPoints = (int) ((stopCurrent - startCurrent) / stepCurrent) + 1;

			// Initialise data array. 
			LI_Sweep_Data liSweepData = new LI_Sweep_Data(numberPoints);
			
			// loop vars
			double setCurrent;

			// Do the sweep. This should be changed to a triggered sweep when the Keithley 2400
			// and triggered power meter driver types are done
			for (int ii= 0; ii < numberPoints; ii++)
			{
				// Set the current
				setCurrent = startCurrent + ii * stepCurrent;
				liSweepData.SetLaserCurrent.Add(setCurrent);

				// Make measurements
				if (engine.IsSimulation)
				{
					// pick values to test graphing, not for realism ;-)
					liSweepData.ReadLaserCurrent.Add(setCurrent - 0.000214);
					liSweepData.LaserVoltage.Add(setCurrent * 1000 + 0.154);
					liSweepData.Bfm1Current.Add(setCurrent * setCurrent);
					liSweepData.Bfm2Current.Add(setCurrent * setCurrent * 0.742);
				}
				else
				{
					laserI.CurrentSetPoint_amp = setCurrent;
					liSweepData.ReadLaserCurrent.Add(laserI.CurrentActual_amp);
					liSweepData.LaserVoltage.Add(laserI.VoltageActual_Volt);
					liSweepData.Bfm1Current.Add(bfm1.CurrentActual_amp);
					liSweepData.Bfm2Current.Add(bfm2.CurrentActual_amp);
				}
				
				engine.SendToGui(liSweepData);
			
				// Allow device to settle
				System.Threading.Thread.Sleep((int)stepTime_ms);
			}
			
			// Set laser current back to 0
			if (!engine.IsSimulation) laserI.CurrentSetPoint_amp = 0.0;

			// Return sweep data. This should return the sweep data using the module data 
			// Have to perform some processing to convert the ArrayList to a double[].
			PlotAxis laserCurrentPlot = new PlotAxis("Laser Current", "A" , 
				(double[]) liSweepData.ReadLaserCurrent.ToArray(typeof(double)));
			PlotAxis laserVoltagePlot = new PlotAxis("Laser Voltage", "V", 
				(double[]) liSweepData.LaserVoltage.ToArray(typeof(double)));
			PlotAxis bfm1Plot = new PlotAxis("Back Facet Monitor #1", "A", 
				(double[]) liSweepData.Bfm1Current.ToArray(typeof(double)));
			PlotAxis bfm2Plot = new PlotAxis("Back Facet Monitor #2", "A", 
				(double[]) liSweepData.Bfm2Current.ToArray(typeof(double)));
			
			DatumPlot plotData = new DatumPlot("LI_Sweep_Plot");
			plotData.AddAxis(laserCurrentPlot);
			plotData.AddAxis(laserVoltagePlot);
			plotData.AddAxis(bfm1Plot);
			plotData.AddAxis(bfm2Plot);

            // Store the Plot data in a CSV file. 

            // Make a temporary area in the system temp path to store the plot data          
            string localDir = Path.Combine(	Path.GetTempPath(), "blobData") ;
 
            if ( !Directory.Exists(localDir))
            {
                Directory.CreateDirectory(localDir);
            } 

            // The plot filename shall be derived from the creation time, and shall be in the 
            // form cureentdateandtime_LI_PLOT.txt
            System.DateTime now = new System.DateTime();
            now = DateTime.Now;

            string plotFilename = now.Year.ToString() + now.Month.ToString() + now.Day.ToString() 
                                  + now.Hour.ToString() + now.Minute.ToString() + now.Second.ToString() 
                                  + now.Millisecond.ToString() + "ms" + "_LI_Plot.csv";
            string plotFileFullPath = localDir + "/" + plotFilename;

            //Write out the plot data.
            writeDataToPlotFile(plotFileFullPath, "Laser Current", "A", liSweepData.ReadLaserCurrent, false);
            writeDataToPlotFile(plotFileFullPath, "Laser Voltage", "V", liSweepData.LaserVoltage, false);
            writeDataToPlotFile(plotFileFullPath, "Back Facet Monitor #1", "A", liSweepData.Bfm1Current, false);
            writeDataToPlotFile(plotFileFullPath, "Back Facet Monitor #2", "A", liSweepData.Bfm2Current, true);

            //Create a DatumFileLink, that the External Read/Write Library will use to point the external database 
            //to the file eventually.
            DatumFileLink plot = new DatumFileLink("LI_Sweep_Plot", localDir, plotFilename);

			moduleResults.Add(plot);

			// wait for acknowledgement
			engine.SendToGui(true); // get GUI to enable the continue button
			engine.GuiUserAttention();
			engine.ReceiveFromGui();
			engine.GuiCancelUserAttention();

			return moduleResults;

		}

        /// <summary>
        /// Add Plot data to a text file, specified by the plotFilePath experience.
        /// </summary>
        /// <param name="plotFilePath">The Full path of the file to write the plot data to.</param>
        /// <param name="plotTitle">The Title of the plot.</param>
        /// <param name="plotUnits">The Units of the data contained in the plot</param>
        /// <param name="data">An Array List of Plot data, assumed to be of type double.</param>
        /// <param name="endOfFile">True if this is the last data plot to be added to plotFilePath.</param>
        private void writeDataToPlotFile(string plotFilePath, string plotTitle, string plotUnits, ArrayList data, bool endOfFile)
        {
            //Open a StreamWriter.
            StreamWriter writer = File.AppendText(plotFilePath);

            //Write the Plot header.
            writer.Write("NEWPLOT, ");
            writer.Write(plotTitle + ", ");
            writer.Write(plotUnits + ", ");
            writer.WriteLine();

            //Stuff the plot data into the file.
            for (int i = 0; i < data.Count; i++)
            {
                if ((i != 0) && ((i % 8) == 0))
                {
                    writer.WriteLine();
                }

                writer.Write((double)data[i]);

                if (!((i == data.Count - 1) && (endOfFile)))
                {
                    writer.Write(", ");
                }
            }
            writer.WriteLine();
            writer.Close();
        }
		#endregion

	}
}

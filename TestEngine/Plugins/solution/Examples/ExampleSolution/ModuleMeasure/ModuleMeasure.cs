using System;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Equipment;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestEngine.Framework.InternalData;


namespace ModuleMeasure
{
	internal enum FibreMovePrompt
	{
		Fibre2Wavemeter,
		Fibre2PowerMeter
	}

	/// <summary>
	/// Module measure for 14-pin laser example program
	/// </summary>
	public class ModuleMeasure : ITestModule
	{
		#region ITestModule Members

		public Type UserControl
		{
			get
			{
				// No user control
				return typeof(ModuleMeasureCtrl);
			}
		}

		public DatumList DoTest(ITestEngine engine, 
			ModulePrivilegeLevel userType, 
			DatumList configData, InstrumentCollection instrs, 
			ChassisCollection chassis, DatumList calData, DatumList previousTestData)
		{
            int simDelay = 100;

            // show the GUI
			engine.GuiShow();
			engine.GuiToFront();
            
            // check we haven't been asked to error by the program :-)
            bool moduleError;
            if (configData.IsPresent("ForceModuleError"))
            {
                moduleError = configData.ReadBool("ForceModuleError");
                if (moduleError) throw new Exception("I'm an exception");
            }


            if (configData.IsPresent("ForceModuleNonParam"))
            {
                moduleError = configData.ReadBool("ForceModuleNonParam");
                if (moduleError) engine.RaiseNonParamFail(1234, "DUT failed in a non parametric way!");
            }

            if (configData.IsPresent("ForceModuleLockup"))
            {
                moduleError = configData.ReadBool("ForceModuleLockup");
                if (moduleError)
                {
                    while (true) ;
                }
            }
            bool testNanInfinity = false;
            if (configData.IsPresent("TestNanInfinity"))
            {
                testNanInfinity = configData.ReadBool("TestNanInfinity");
            }

            DatumList moduleResults = new DatumList();

			// Set up instruments
			InstType_ElectricalSource laserI = null;
			InstType_ElectricalSource redMon = null;
			InstType_ElectricalSource blueMon = null;
			InstType_Wavemeter wavemeter = null;
			InstType_OpticalPowerMeter powerMtr = null;

			if (!engine.IsSimulation)
			{
				laserI = (InstType_ElectricalSource) instrs["LaserI"];
				redMon = (InstType_ElectricalSource) instrs["RedMon"];
				blueMon = (InstType_ElectricalSource) instrs["BlueMon"];
				wavemeter = (InstType_Wavemeter) instrs["Wavemeter"];
				powerMtr = (InstType_OpticalPowerMeter) instrs["PowerMtr"];

				// Make sure laser current is 0
				laserI.CurrentSetPoint_amp = 0.0;
			}
			
			// Set the current & record it
			DatumDouble datumSetCurrent_mA = new DatumDouble("LaserSetCurrent_mA");
			configData.Read(datumSetCurrent_mA);			
			// Send the current to the GUI
			engine.SendToGui(datumSetCurrent_mA);

			double setCurrent_mA = datumSetCurrent_mA.Value;

			// Measure combined (sum) BFM dark currents at 5V
			double combMonitorCurrents_nA;
			if (engine.IsSimulation)
			{
				// sleep one second
                System.Threading.Thread.Sleep(simDelay);
				combMonitorCurrents_nA = 5.13;
			}
			else
			{
				combMonitorCurrents_nA = redMon.CurrentActual_amp * 1e9;
				combMonitorCurrents_nA += blueMon.CurrentActual_amp * 1e9;
			}
			DatumDouble datumCombMonitorCurrents = 
				new DatumDouble("DarkCurrent_nA", combMonitorCurrents_nA);
			// send this to the GUI
			engine.SendToGui(datumCombMonitorCurrents);
			// add to results list
			moduleResults.Add(datumCombMonitorCurrents);


			// Apply current. This should be changed to lock current as the program is developed
			
			double readBackCurrent_mA;
			if (engine.IsSimulation)
			{
				// sleep one second
                System.Threading.Thread.Sleep(simDelay);
				readBackCurrent_mA = (setCurrent_mA - 1.3);
			}
			else
			{
				// set current
				laserI.CurrentSetPoint_amp = setCurrent_mA / 1e3;
				// read it back again
				readBackCurrent_mA = laserI.CurrentActual_amp * 1e3;
			}
			
			DatumDouble datumReadBackCurrent= 
				new DatumDouble("LaserCurrent_mA", readBackCurrent_mA);
			// send this to the GUI
			engine.SendToGui(datumReadBackCurrent);
			// add to results list
			moduleResults.Add(datumReadBackCurrent);

			// Measure fiber power
			double fiberPower_dBm;
			if (engine.IsSimulation)
			{
				// sleep one second
                System.Threading.Thread.Sleep(simDelay);
				fiberPower_dBm = setCurrent_mA / 98.0;			
			}
			else
			{
				fiberPower_dBm = powerMtr.ReadPower();
			}
			
			fiberPower_dBm = Math.Round(fiberPower_dBm,2);
			DatumDouble datumFiberPower = 
				new DatumDouble("FiberPower_dBm", fiberPower_dBm);
			// send this to the GUI
			engine.SendToGui(datumFiberPower);
			// add to results list
			moduleResults.Add(datumFiberPower);

			// Prompt for fibre change
			this.sendPrompt(engine, FibreMovePrompt.Fibre2Wavemeter);


			// Read and store the wavelength
			double wavlengthILock_nm;
			if (engine.IsSimulation)
			{
				// sleep one second
                System.Threading.Thread.Sleep(simDelay);
				wavlengthILock_nm = 1552.7823;
			}
			else
			{
				wavlengthILock_nm = wavemeter.Wavelength_nm;
			}
			DatumDouble datumWavILock = 
				new DatumDouble("LaserWavelength_nm", wavlengthILock_nm);
			engine.SendToGui(datumWavILock);
			moduleResults.Add(datumWavILock);

			// Prompt for fibre change
			this.sendPrompt(engine, FibreMovePrompt.Fibre2PowerMeter);
			
			// Set laser current to 0 before exiting
			if (!engine.IsSimulation) laserI.CurrentSetPoint_amp = 0.0;
            else System.Threading.Thread.Sleep(simDelay); // sleep one second in sim mode
			
	        // Test dev code to show PcasDataWrite can now handle a value of infinity:
            double nbr1 = 1.0;
            double nbr2 = 2.0;
            if (testNanInfinity)
            {
                nbr1 = Double.NaN;
                nbr2 = Double.NegativeInfinity;
            }
            
            moduleResults.AddDouble("TEST_DEV_DOUBLE_1", nbr1);
            moduleResults.AddDouble("TEST_DEV_DOUBLE_2", nbr2);

			// return the results
			return moduleResults;
		}

		#endregion

		private void sendPrompt(ITestEngine engine, FibreMovePrompt prompt)
		{
            string qText;
            switch (prompt)
            {
                case FibreMovePrompt.Fibre2Wavemeter:
                    qText = "Connect the fibre to the wavemeter and press Continue when ready...";
                    break;
                case FibreMovePrompt.Fibre2PowerMeter:
                    qText = "Connect the fibre to the power meter and press Continue when ready...";
                    break;
                default:
                    throw new Exception("Invalid prompt type: " + prompt.ToString());
            }

            engine.ShowContinueUserQuery(qText);
		}
	}
}

using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Bookham.TestEngine.Framework.InternalData;

namespace ModuleMeasure
{
	public class ModuleMeasureCtrl : Bookham.TestEngine.Framework.Messages.ManagedCtlBase
	{
		private System.Windows.Forms.Label setCurrent;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label readCurrent;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label combDarkCurrent;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label laserWavelength;
		private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label fiberPower;
		private System.ComponentModel.IContainer components = null;

		public ModuleMeasureCtrl()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.setCurrent = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.readCurrent = new System.Windows.Forms.Label();
            this.combDarkCurrent = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.laserWavelength = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.fiberPower = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // setCurrent
            // 
            this.setCurrent.BackColor = System.Drawing.SystemColors.Window;
            this.setCurrent.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.setCurrent.Location = new System.Drawing.Point(16, 40);
            this.setCurrent.Name = "setCurrent";
            this.setCurrent.Size = new System.Drawing.Size(100, 23);
            this.setCurrent.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(16, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Set Current (mA)";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(256, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "Read Current (mA)";
            // 
            // readCurrent
            // 
            this.readCurrent.BackColor = System.Drawing.SystemColors.Window;
            this.readCurrent.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.readCurrent.Location = new System.Drawing.Point(256, 40);
            this.readCurrent.Name = "readCurrent";
            this.readCurrent.Size = new System.Drawing.Size(100, 23);
            this.readCurrent.TabIndex = 1;
            // 
            // combDarkCurrent
            // 
            this.combDarkCurrent.BackColor = System.Drawing.SystemColors.Window;
            this.combDarkCurrent.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.combDarkCurrent.Location = new System.Drawing.Point(136, 40);
            this.combDarkCurrent.Name = "combDarkCurrent";
            this.combDarkCurrent.Size = new System.Drawing.Size(100, 23);
            this.combDarkCurrent.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(136, 8);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(112, 32);
            this.label4.TabIndex = 2;
            this.label4.Text = "Combined Dark Current (nA)";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(496, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 32);
            this.label3.TabIndex = 4;
            this.label3.Text = "Laser Wavelength (nm)";
            // 
            // laserWavelength
            // 
            this.laserWavelength.BackColor = System.Drawing.SystemColors.Window;
            this.laserWavelength.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.laserWavelength.Location = new System.Drawing.Point(496, 40);
            this.laserWavelength.Name = "laserWavelength";
            this.laserWavelength.Size = new System.Drawing.Size(100, 23);
            this.laserWavelength.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(376, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 16);
            this.label5.TabIndex = 6;
            this.label5.Text = "Fiber Power (dBm)";
            // 
            // fiberPower
            // 
            this.fiberPower.BackColor = System.Drawing.SystemColors.Window;
            this.fiberPower.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.fiberPower.Location = new System.Drawing.Point(376, 40);
            this.fiberPower.Name = "fiberPower";
            this.fiberPower.Size = new System.Drawing.Size(100, 23);
            this.fiberPower.TabIndex = 5;
            // 
            // ModuleMeasureCtrl
            // 
            this.Controls.Add(this.label5);
            this.Controls.Add(this.fiberPower);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.laserWavelength);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.setCurrent);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.readCurrent);
            this.Controls.Add(this.combDarkCurrent);
            this.Controls.Add(this.label4);
            this.Name = "ModuleMeasureCtrl";
            this.MsgReceived += new Bookham.TestEngine.Framework.Messages.ManagedCtlBase.MsgReceivedDlg(this.ModuleMeasureCtrl_MsgReceived);
            this.ResumeLayout(false);

		}
		#endregion

		private void ModuleMeasureCtrl_MsgReceived(object payload, long inMsgSeq, long respSeq)
		{
			if (payload.GetType() == typeof(DatumDouble))
			{
				DatumDouble datumDbl = (DatumDouble) payload;
				switch (datumDbl.Name)
				{
					case "LaserSetCurrent_mA":
						this.setCurrent.Text = String.Format("{0}", datumDbl.Value);
						break;
					case "DarkCurrent_nA":
						this.combDarkCurrent.Text = String.Format("{0}", datumDbl.Value);
						break;
					case "LaserCurrent_mA":
						this.readCurrent.Text = String.Format("{0}", datumDbl.Value);
						break;
					case "FiberPower_dBm":
						this.fiberPower.Text = datumDbl.Value.ToString("F2");
						break;
					case "LaserWavelength_nm":
						this.laserWavelength.Text = datumDbl.Value.ToString();
						break;
					default:
						throw new ArgumentException("Invalid datum received: " + datumDbl.Name);
				}
			}			
		}
		
		private void OKcontinueBtn_Click(object sender, System.EventArgs e)
		{
			// send anything back to worker - this is only message back
			this.sendToWorker(true);
		}

	}
}


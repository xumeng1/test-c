using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace ExampleProgram
{
	public class ExampleProgramCtrl : Bookham.TestEngine.Framework.Messages.ManagedCtlBase
	{
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Button button1;
		private System.ComponentModel.IContainer components = null;

		public ExampleProgramCtrl()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.button1 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(192, 72);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(432, 40);
			this.label1.TabIndex = 0;
			this.label1.Text = "Example Test Program (14-pin Laser Test)";
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(256, 112);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(208, 20);
			this.textBox1.TabIndex = 1;
			this.textBox1.Text = "textBox1";
			// 
			// button1
			// 
			this.button1.Enabled = false;
			this.button1.Location = new System.Drawing.Point(312, 176);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(88, 32);
			this.button1.TabIndex = 2;
			this.button1.Text = "Continue";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// ExampleProgramCtrl
			// 
			this.Controls.Add(this.button1);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.label1);
			this.Name = "ExampleProgramCtrl";
			this.MsgReceived += new Bookham.TestEngine.Framework.Messages.ManagedCtlBase.MsgReceivedDlg(this.ExampleProgramCtrl_MsgReceived);
			this.ResumeLayout(false);

		}
		#endregion

		private void ExampleProgramCtrl_MsgReceived(object payload, long inMsgSeq, long respSeq)
		{
			Type payloadType = payload.GetType();
			if (payloadType == typeof(string)) 
			{
				this.textBox1.Text = (string) payload;
			}
			else if (payloadType == typeof(bool)) 
			{
				this.button1.Enabled = true;
			}
			else throw new ArgumentException("Invalid payload received: " 
					 + payloadType.ToString());
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			// send anything back to the worker
			this.sendToWorker(true);
			// disable the button again
			this.button1.Enabled = false;
		}
	}
}


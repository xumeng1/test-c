using System;
using System.Collections;
using System.Collections.Specialized;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.Equipment;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestEngine.Framework.InternalData; // for DatumList, etc..
using Bookham.TestEngine.Framework.Limits; // for TestData, etc..
using Bookham.TestEngine.Config;

namespace ExampleProgram
{

	/// <summary>
	/// Class containing all the instruments used by the Example Test Program
	/// </summary>
	internal class ExampleProgramInstrs
	{
		/// <summary>
		/// Constructor for example program - extract the relevant instruments
		/// from the global list
		/// </summary>
		/// <param name="instrs">Global Instrument List</param>
		internal ExampleProgramInstrs(InstrumentCollection instrs)
		{
			this.LaserCurrent = (InstType_ElectricalSource) instrs["CurrentSource3"];
			this.Bfm1 = (InstType_ElectricalSource) instrs["Monitor1"];
			this.Bfm2 = (InstType_ElectricalSource) instrs["Monitor2"];
			this.LaserTec = (InstType_TecController) instrs["TEC1"];
			this.PowerMeter = (InstType_OpticalPowerMeter) instrs["PowerMeter3"];
			this.WaveMeter = (InstType_Wavemeter) instrs["Wavemeter1"];
		}

		/// <summary>
		/// Laser Current source
		/// </summary>
		internal InstType_ElectricalSource LaserCurrent;

		/// <summary>
		/// Back Facet Monitor 1
		/// </summary>
		internal InstType_ElectricalSource Bfm1;

		/// <summary>
		/// Back Facet Monitor 2
		/// </summary>
		internal InstType_ElectricalSource Bfm2;

		/// <summary>
		/// Laser TEC Controller
		/// </summary>
		internal InstType_TecController LaserTec;

		/// <summary>
		/// Optical Power Meter
		/// </summary>
		internal InstType_OpticalPowerMeter PowerMeter;
	
		/// <summary>
		/// Wavelength Meter
		/// </summary>
		internal InstType_Wavemeter WaveMeter;
	}

    /// <summary>
    /// Enumerated type to simulate error
    /// </summary>
    internal enum ErrorProgramSim
    {
        None,
        InitError,
        ProgramRunTime,
        ModuleRunTime,
        ProgramNonParam,
        ModuleNonParam,
        PostRunError,
        DataWriteMissing,
        DataWriteError,
        InfiniteLoopProgram,
        InfiniteLoopModule
    }

	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public class UnitTestProgram : ITestProgram
	{
		#region ITestProgram Members
		public Type UserControl
		{
			get
			{
				return typeof(ExampleProgramCtrl);
			}
		}

		public void InitCode(ITestEngineInit engine, DUTObject dutObject, InstrumentCollection instrs, ChassisCollection chassis)
		{
            //Version control config
            string programConfigFile = "Plugins/solution/Examples/ExampleSolution/ExampleProgram.xml";
            engine.SetVersionControl(true, true);
            engine.AddCommonConfigFileName(programConfigFile);
            engine.AddSpecificConfigFileName(this.instrumentConfigFile);

            //disable module completedMessage 
            engine.DisableModuleCompletedMsg(true);

            /******************/
            /* refresh module list until it's necessary. the code is commented here.*/
            //engine.SetModulesListRefreshingMode(true);
            /******************/

			// read config
            ConfigDataAccessor progConfigAccess = new ConfigDataAccessor(programConfigFile, "ProgramConfig");
			StringDictionary keys = new StringDictionary();
			keys.Add("Index", "1");
			DatumList progConfigData = progConfigAccess.GetData(keys , false);
			
            // setup Dark Current limit
            bool failProgram = progConfigData.ReadBool("FailProgramSim");            

			engine.GuiShow();			
			engine.SendToGui("Config: " + progConfigData.ReadString("StartupMessage"));
			
			SpecList specList = null;
			Specification specA = null;

			// don't read previous data in simulation mode
			if (progConfigData.ReadBool("ReadFromPcas"))
			{
				// load limits from external data default location
				StringDictionary keyData = new StringDictionary();
				keyData.Add("SCHEMA", "coc");
				keyData.Add("DEVICE_TYPE", "dummy3");
				keyData.Add("TEST_STAGE", "the_test");
				
				specList = engine.GetLimitReader().GetLimit(keyData); // -> GetSpeclist				
			}
			else
			{
                // load limits from a CSV file
                StringDictionary keyData = new StringDictionary();
                keyData.Add("filename", @"Plugins\solution\Examples\ExampleSolution\ExampleLimits.csv");                
                specList = engine.GetLimitReader("PCAS_FILE").GetLimit(keyData); // -> GetSpeclist	               
			}
            
            // we don't know the exact name of our specification, but just get the first one using
            // the standard C# enumerator
            IEnumerator ie = specList.GetEnumerator();
            ie.Reset();
            ie.MoveNext();
            specA = (Specification)ie.Current;                							

			// setup the program's specification list
			engine.SetSpecificationList(specList);

			if (progConfigData.ReadBool("PauseAtBegin"))
			{
				engine.SendToGui(true); // get the GUI to enable the button
				engine.GuiUserAttention();
				engine.ReceiveFromGui();
				engine.GuiCancelUserAttention();
			}

			// remember if config asked us to pause at end
			this.pauseAtEnd = (progConfigData.ReadBool("PauseAtEnd"));

			// remember if config asked us to error during program
            string errorProgramSimStr = progConfigData.ReadString("ErrorProgramSim");
            if (errorProgramSimStr.Length > 0)
            {
                this.errorProgramSim = (ErrorProgramSim)
                    Enum.Parse(typeof(ErrorProgramSim), errorProgramSimStr);
            }

            if (errorProgramSim == ErrorProgramSim.InitError)
            {
                engine.ErrorInProgram("Error in Init Phase");
            }

			// Module run object (used during initialisation)
			ModuleRun modRun = null;
			
			// Get the instruments required for this program, and set them up...
			// NB: don't initialise the instruments in simulation mode
			if (!engine.IsSimulation)
			{								
				initInstruments(instrs);				
			}

            // *** SETUP VB DEMO MODULE ***
            modRun = engine.AddModuleRun("VBDemo", "VBDemoModule", "");
            modRun.ConfigData.AddString("Message", "Calling VB Module from C# Program");
			
			// *** SETUP POWER UP MODULE ***
			modRun = engine.AddModuleRun("PowerUp", "ModulePowerUpDown", "");
			modRun.ConfigData.AddBool("State", true);
			
			InstrumentCollection powerUpDownInstrs = null;
			// PowerUp instruments - don't init in simulation mode
			if (!engine.IsSimulation)
			{
				powerUpDownInstrs = new InstrumentCollection();
				powerUpDownInstrs.Add("CurrentDriver", progInstrs.LaserCurrent);
				powerUpDownInstrs.Add("BackFacet1", progInstrs.Bfm1);
				powerUpDownInstrs.Add("BackFacet2", progInstrs.Bfm2);
				powerUpDownInstrs.Add("Tec", progInstrs.LaserTec);
				
				modRun.Instrs.Add(powerUpDownInstrs);
			}
			
						
			// *** SETUP MEASURE MODULES ***
			InstrumentCollection measureInstrs = null;
			if (!engine.IsSimulation)
			{
				measureInstrs = new InstrumentCollection();
				measureInstrs.Add("LaserI", progInstrs.LaserCurrent);
				measureInstrs.Add("RedMon", progInstrs.Bfm1);
				measureInstrs.Add("BlueMon", progInstrs.Bfm2);
				measureInstrs.Add("PowerMtr", progInstrs.PowerMeter);
				measureInstrs.Add("Wavemeter", progInstrs.WaveMeter);
			}
						
			// Measurement Module #1 - 100 mA
			modRun = engine.AddModuleRun("Measure@100mA", "ModuleMeasure", "");			
			// Add Config
			modRun.ConfigData.AddDouble("LaserSetCurrent_mA", 100);
            modRun.ConfigData.AddBool("TestNanInfinity", failProgram);
			// Add Limit translation
			modRun.Limits.AddParameter(specA, "BFM_DARK_CURRENT", "DarkCurrent_nA"); 
			modRun.Limits.AddParameter(specA, "WAVELENGTH_AT_100MA", "LaserWavelength_nm");
			modRun.Limits.AddParameter(specA, "FIBER_POWER_100MA_DBM", "FiberPower_dBm");            
            modRun.Limits.AddParameter(specA, "TEST_DEV_DOUBLE_1", "TEST_DEV_DOUBLE_1");
            modRun.Limits.AddParameter(specA, "TEST_DEV_DOUBLE_2", "TEST_DEV_DOUBLE_2");

			if (measureInstrs!=null) modRun.Instrs.Add(measureInstrs);

			// Measurement Module #2 - 150 mA
            modRun = engine.AddModuleRun("Measure@150mA", "ModuleMeasure", "", true);// add module with acceleration because the ModuleMeasure plugin is loaded when adding Measure@100mA.
			modRun.ConfigData.AddDouble("LaserSetCurrent_mA", 150);
            // module looks for this it can throw an error - 
            // -  that way we can have the fault in one instance of the module-run but not the other
            // - this can be used for testing failure-handling code. 
            if (errorProgramSim == ErrorProgramSim.ModuleRunTime)
                modRun.ConfigData.AddBool("ForceModuleError", true);
            if (errorProgramSim == ErrorProgramSim.ModuleNonParam)
                modRun.ConfigData.AddBool("ForceModuleNonParam", true);
            if (errorProgramSim == ErrorProgramSim.InfiniteLoopModule)
                modRun.ConfigData.AddBool("ForceModuleLockup", true);

			modRun.Limits.AddParameter(specA, "WAVELENGTH_AT_150MA", "LaserWavelength_nm");
			modRun.Limits.AddParameter(specA, "FIBER_POWER_150MA_DBM", "FiberPower_dBm");

			if (measureInstrs!=null) modRun.Instrs.Add(measureInstrs);

			// *** SETUP LI MODULE ***
			modRun = engine.AddModuleRun("LI", "ModuleLI_Sweep", "");
			modRun.ConfigData.AddDouble("StartCurrent", 0.0);
			modRun.ConfigData.AddDouble("StopCurrent", 100.0e-3);
			modRun.ConfigData.AddDouble("StepCurrent", 1e-3);
			modRun.ConfigData.AddDouble("StepDelay_sec", 0.0);

            //Make sure that LI Sweep Plot raw data is stored on the server.
            modRun.Limits.AddParameter(specA, "PLOT_LIV", "LI_Sweep_Plot"); 

			// LI instruments
			InstrumentCollection liInstrs = null;				
			if (!engine.IsSimulation)
			{													
				liInstrs = new InstrumentCollection();
				liInstrs.Add("CurrentSrc", progInstrs.LaserCurrent);
				liInstrs.Add("Bfm1", progInstrs.Bfm1);
				liInstrs.Add("Bfm2", progInstrs.Bfm2);
				liInstrs.Add("OpticalPower", progInstrs.PowerMeter);
				
				modRun.Instrs.Add(liInstrs);
			}
			
		}

		public void Run(ITestEngineRun engine, DUTObject dutObject, InstrumentCollection instrs, ChassisCollection chassis)
		{
            engine.RunModule("VBDemo");
            engine.RunModule("PowerUp");
			engine.RunModule("Measure@100mA");
			// simulate an error 
            if (errorProgramSim == ErrorProgramSim.ProgramRunTime) throw new Exception("Hi");
            if (errorProgramSim == ErrorProgramSim.ProgramNonParam) engine.RaiseNonParamFail(1234, "Force non-parm fail");

            if (errorProgramSim == ErrorProgramSim.InfiniteLoopProgram)
            {
                while (true) ;
            }

			engine.RunModule("Measure@150mA");
			engine.RunModule("LI");

            bool state = false;
			// demo init and run of module in Run
            //for (int ii = 0; ii < 100; ii++)
            //{
            //    string modRunName = "PowerUp#" + ii.ToString();
            //    ModuleRun modRun = engine.AddModuleRun(modRunName, "ModulePowerUpDown", "");
            //    modRun.ConfigData.AddBool("State", state);
            //    engine.RunModule(modRunName);
            //    state = !state;                
            //}
		}

        public void PostRun(ITestEnginePostRun engine, DUTObject dutObject, InstrumentCollection instrs, ChassisCollection chassis)
		{
			// no need to shutdown engine in simulation mode
			if (!engine.IsSimulation) shutDownInstruments();

            if (errorProgramSim == ErrorProgramSim.PostRunError)
                engine.ErrorInProgram("Bad Post-run!");

			if (this.pauseAtEnd)
			{
				engine.SendToGui("In PostRun...");
				engine.SendToGui(true); // get the GUI to enable the button
				engine.GuiUserAttention();
				engine.ReceiveFromGui();
				engine.GuiCancelUserAttention();
			}
		}

		public void DataWrite(ITestEngineDataWrite engine, DUTObject dutObject, DUTOutcome dutOutcome, TestData results, UserList userList)
		{
            if (this.errorProgramSim == ErrorProgramSim.DataWriteError)
            {
                engine.ErrorInProgram("Simulate error in data write phase");
            }

            //
			// Add keys to specify where data will be written to
			//
			string specName = dutOutcome.OutputSpecificationNames[0];
			//By default uses default sink
			//engine.SetDataDestination( "PCAS_PAIGNTON" ); // X-refs CorePlugInsConfig 'Data Sink Label'
			StringDictionary keyData = new StringDictionary();
			keyData.Add( "SCHEMA", "coc" );
			keyData.Add( "DEVICE_TYPE", "dummy" );
			keyData.Add( "TEST_STAGE", "the_test" );
			keyData.Add( "SPECIFICATION", specName );
			keyData.Add( "SERIAL_NO", dutObject.SerialNumber );
			engine.SetDataKeys( keyData );

			//
			// Collate trace data from DUTObject and UserList
			// Trace data *IS* limit checked (but you'd expect they weren't set!)
			//
			DatumList traceData = new DatumList();
			traceData.AddString( "OPERATOR", userList.UserListString );
			traceData.AddString( "EQUIP_ID", "UNKNOWN" );
            traceData.AddSint32("NODE", dutObject.NodeID);
            if (this.errorProgramSim != ErrorProgramSim.DataWriteMissing)
            {
                traceData.AddString("CHIP_ID", "UNKNOWN");                
            }

            // Add time data...
            double testTime = engine.GetTestTime();
            double idleTime = testTime - engine.TestProcessTime;
            traceData.AddDouble("PROCESS_TIME", engine.TestProcessTime);
            traceData.AddDouble("TEST_TIME", testTime);
            traceData.AddDouble("IDLE_TIME", idleTime);
            traceData.AddString("START", engine.GetStartDateTime().ToString("dd/MMM/yyyy"));

            // These trace data can be added if you like.
            string softwareId = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + " " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            traceData.AddString("SOFTWARE_ID", softwareId);
            traceData.AddString("COMPUTER_ID", System.Environment.MachineName);
            traceData.AddString("LOT_ID", dutObject.BatchID);
            //traceData.AddString("VARIANT_TYPE", dutObject.PartCode);
          
            traceData.AddString("COMMENTS", engine.GetProgramRunComments());

            //test solution IDs which are calculated by Test Engine.
            traceData.AddString("SOFTWARE_GENERIC_VERSION", engine.SpecificTestSolutionId.ToString());
            traceData.AddString("SOFTWARE_SPECIFIC_VERSION", engine.CommonTestSolutionId.ToString());
            
            // This is our way of confirming if the program ran OK or was aborted or errored
            traceData.AddString("TESTENGINE_STATUS", engine.GetProgramRunStatus().ToString());
            
            // Standard Test Status string (PASS/FAIL)
            traceData.AddString("TEST_STATUS", engine.GetOverallPassFail().ToString().ToUpper());            
            
			engine.SetTraceData( specName, traceData );
		}

		#endregion

		#region Helper functions		

		void initInstruments(InstrumentCollection instrs)
		{
			// read config
            ConfigDataAccessor instrConfigAccess = new ConfigDataAccessor(this.instrumentConfigFile, "InstrumentConfig");
			StringDictionary keys = new StringDictionary();
			keys.Add("Index", "1");
			DatumList instrConfigData = instrConfigAccess.GetData(keys , false);

			progInstrs = new ExampleProgramInstrs(instrs);
			
			// Laser TEC controller
			InstType_TecController laserTec = progInstrs.LaserTec;
			laserTec.SetDefaultState();					// Put into a known state
			// Set the control mode & sensor type
			InstType_TecController.ControlMode ctrlMode = (InstType_TecController.ControlMode)
				Enum.Parse(typeof(InstType_TecController.ControlMode), instrConfigData.ReadString("TecCtrlMode"));
			laserTec.OperatingMode = ctrlMode;
			InstType_TecController.SensorType sensor = (InstType_TecController.SensorType)
				Enum.Parse(typeof(InstType_TecController.SensorType), instrConfigData.ReadString("TecSensor"));
		
			laserTec.Sensor_Type = sensor;
			laserTec.SensorCurrent_amp = instrConfigData.ReadDouble("TecSensorCurrent_A");
			laserTec.TecCurrentCompliance_amp = instrConfigData.ReadDouble("TecComply_A");
			laserTec.TecVoltageCompliance_volt = instrConfigData.ReadDouble("TecComply_V");
						
			// Current source
			InstType_ElectricalSource laserCurrent = progInstrs.LaserCurrent;
			laserCurrent.SetDefaultState();				// Put into a known state
			laserCurrent.VoltageComplianceSetPoint_Volt = instrConfigData.ReadDouble("LaserComply_V");
			laserCurrent.CurrentComplianceSetPoint_Amp = instrConfigData.ReadDouble("LaserComply_A");

			// BFM1
			InstType_ElectricalSource bfm1 = progInstrs.Bfm1;
			bfm1.SetDefaultState();						// Put into a known state
			// Compliance not supported on this instrument
			//bfm1.VoltageCompliance_Volt = instrConfigData.ReadDouble("BfmComply_V");
			//bfm1.CurrentCompliance_Amp = instrConfigData.ReadDouble("BfmComply_A");
			bfm1.VoltageSetPoint_Volt = instrConfigData.ReadDouble("BfmBias_V");

			// BFM2
			InstType_ElectricalSource bfm2 = progInstrs.Bfm2;
			bfm2.SetDefaultState();						// Put into a known state
			// Compliance not supported on this instrument
			//bfm2.VoltageCompliance_Volt = instrConfigData.ReadDouble("BfmComply_V");
			//bfm2.CurrentCompliance_Amp = instrConfigData.ReadDouble("BfmComply_A");
			bfm2.VoltageSetPoint_Volt = instrConfigData.ReadDouble("BfmBias_V");

			// Power meter
			InstType_OpticalPowerMeter pwrMtr = progInstrs.PowerMeter;
			pwrMtr.SetDefaultState();					// Put into a known state
            pwrMtr.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;
			pwrMtr.Range = InstType_OpticalPowerMeter.AutoRange;		// Autorange
			pwrMtr.Wavelength_nm = 1550.0;				// Set expected wavelength
		}


		void shutDownInstruments()
		{
			progInstrs.LaserCurrent.CurrentSetPoint_amp = 0.0;
			progInstrs.LaserCurrent.OutputEnabled = false;
									
			progInstrs.Bfm1.OutputEnabled = false;
			
			progInstrs.Bfm2.OutputEnabled = false;

			progInstrs.LaserTec.OutputEnabled = false;	
		}

		#endregion

		#region Private data
		/// <summary>
		/// Instruments for the example program.
		/// </summary>
		ExampleProgramInstrs progInstrs;

		/// <summary>
		/// Flag for pause at end of the program (runtime will default to false).
		/// </summary>
		bool pauseAtEnd;

		/// <summary>
		/// Enumerated type for error in simulation mode
		/// </summary>
		ErrorProgramSim errorProgramSim;

        /// <summary>
        /// Instrument config file.
        /// </summary>
        string instrumentConfigFile = "Plugins/solution/Examples/ExampleSolution/InstrumentConfig.xml";
		#endregion
	}
}

using System;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Equipment;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestEngine.Framework.InternalData;


namespace ModulePowerUp
{
	/// <summary>
	/// Module power up for 14-pin laser example program
	/// </summary>
	public class ModulePowerUpDown : ITestModule
	{
		#region ITestModule Members

		public Type UserControl
		{
			get
			{
				// No user control
				return null;
			}
		}

		public DatumList DoTest(ITestEngine engine, 
			ModulePrivilegeLevel userType, 
			DatumList configData, InstrumentCollection instrs, 
			ChassisCollection chassis, DatumList calData, DatumList previousTestData)
		{
			if (engine.IsSimulation) return null;

			InstType_ElectricalSource laserCurrent = (InstType_ElectricalSource) instrs["CurrentDriver"];
			InstType_ElectricalSource bfm1 = (InstType_ElectricalSource) instrs["BackFacet1"];
			InstType_ElectricalSource bfm2 = (InstType_ElectricalSource) instrs["BackFacet2"];
			InstType_TecController tec = (InstType_TecController) instrs["Tec"];


			bool state = configData.ReadBool("State");			

			laserCurrent.OutputEnabled = state;
			bfm1.OutputEnabled = state;
			bfm2.OutputEnabled = state;
			tec.OutputEnabled = state;

			// no data
			return null;
		}

		#endregion
	}

}

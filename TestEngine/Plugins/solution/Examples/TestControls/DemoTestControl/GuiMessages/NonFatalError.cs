// [Copyright]
//
// Bookham Modular Test Engine
// Test Control Plug-in
//
// NonFatalError.cs
// 
// Author: Joseph Olajubu
// Design: Test Control DD

using System;

namespace Bookham.DemoTestControlPlugin.GuiMessages
{

	/// <summary>
	/// Message from the Test Control plug-in to it's Test Control GUI informing it of a 
	/// non-fatal error while attempting to load a batch.
	/// </summary>
	internal sealed  class NonFatalError
	{
		/// <summary>
		/// Constructor - initialises error string.
		/// </summary>
		/// <param name="message">Error message.</param>
		internal NonFatalError( string message )
		{
			this.message = message;
		}

		/// <summary>
		/// Error message.
		/// </summary>
		internal readonly string message;
	}

}

// [Copyright]
//
// Bookham Modular Test Engine
// Test Control Plug-in
//
// SelectNextDUTRequest.cs
// 
// Author: Joseph Olajubu
// Design: Test Control DD

using System;

namespace Bookham.DemoTestControlPlugin.GuiMessages
{
	/// <summary>
	/// Request by the Test Control Plug-in Worker thread to the BatchView GUI to select 
	/// the next DUT for test.
	/// </summary>
	internal sealed class SelectNextDUTRequest
	{
		/// <summary>
		/// Constructor.
		/// </summary>
		internal SelectNextDUTRequest()
		{
		}
	}
}

// [Copyright]
//
// Bookham Modular Test Engine
// Test Control Plug-in
//
// LoadBatchResponse.cs
// 
// Author: Joseph Olajubu
// Design: Test Control DD

using System;

namespace Bookham.DemoTestControlPlugin.GuiMessages
{
	/// <summary>
	/// Load Batch Response Message, Sent from Test control GUI to the Test Control Plug-in worker thread. 
	/// Holds the selected BatchId.
	/// </summary>
	internal sealed class LoadBatchResponse
	{
		#region Constructor
		public LoadBatchResponse(string batchId)
		{
			//Store the BatchId of new lot to be loaded.
			this.batchId = batchId;
		}
		#endregion

		#region Internal data

		/// <summary>
		/// Batch Id fo the batch to be loaded.
		/// </summary>
		internal readonly string batchId;
		#endregion

	}
}

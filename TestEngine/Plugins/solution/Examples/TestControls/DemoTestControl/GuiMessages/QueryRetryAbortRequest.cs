// [Copyright]
//
// Bookham Modular Test Engine
// Test Control Plug-in
//
// QueryRetryAbortRequest.cs
// 
// Author: Joseph Olajubu
// Design: Test Control DD

using System;

namespace Bookham.DemoTestControlPlugin.GuiMessages
{
	/// <summary>
	/// Request message sent from the Test Control Plug-in Worker thread to the Test Control Tab GUI
	/// to request whether we should Retry or Abort loading Batch.
	/// </summary>
	internal sealed class QueryRetryAbortRequest
	{

		/// <summary>
		/// Constructor. Stores the required error message to the user.
		/// </summary>
		internal QueryRetryAbortRequest(string message)
		{
			this.message = message;
		}

		internal readonly string message;
	}
}

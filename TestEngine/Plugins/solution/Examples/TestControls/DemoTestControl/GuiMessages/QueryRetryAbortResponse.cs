
// [Copyright]
//
// Bookham Modular Test Engine
// Test Control Plug-in
//
// QueryRetryAbortResponse.cs
// 
// Author: Joseph Olajubu
// Design: Test Control DD
using System;

namespace Bookham.DemoTestControlPlugin.GuiMessages
{
	/// <summary>
	/// Response to a QueryRetryAbortRequest message sent from the Test control Tab GUI 
	/// to the Test control Plug-in Worker thread. Indicates whether the user has elected to 
	/// retry or abort loading a batch. Contains the user's response.
	/// </summary>
	internal sealed class QueryRetryAbortResponse
	{
		/// <summary>
		/// Constructor. initialise the response field.
		/// </summary>
		/// <param name="response">The User's response.</param>
		internal QueryRetryAbortResponse (QueryRetryAbort response)
		{
			this.response = response;
		}

		/// <summary>
		/// The User's Response.
		/// </summary>
		internal readonly QueryRetryAbort response;
	}
}

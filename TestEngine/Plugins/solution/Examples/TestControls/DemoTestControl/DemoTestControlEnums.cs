// [Copyright]
//
// Bookham Modular Test Engine
// Test Control Plug-in
//
// DemoTestControlEnums.cs
// 
// Author: Joseph Olajubu
// Design: Test Control DD


using System;

namespace Bookham.DemoTestControlPlugin
{
	/// <summary>
	/// User query result enum: Retry or abort current operation.
	/// </summary>
	internal enum QueryRetryAbort
	{
		/// <summary>
		/// Retry
		/// </summary>
		Retry,

		/// <summary>
		/// Abort.
		/// </summary>
		Abort
	}

	/// <summary>
	/// Test Status of devices within a batch
	/// </summary>
	internal enum TestStatus
	{
		/// <summary>
		/// Untested.
		/// </summary>
		Untested,

		/// <summary>
		/// Marked for restest.
		/// </summary>
		MarkForRetest,

		/// <summary>
		/// Passed.
		/// </summary>
		Passed,

		/// <summary>
		/// Failed.
		/// </summary>
		Failed
	}
}

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestSolution.IlmzCommonData ;

namespace Bookham.TestSolution.ILMZFinalData
{
    /// <summary>
    /// struct defines Parameters to be check in qual test
    /// </summary>
    public struct IlmzChannelQualTestRstData
    {
        
        /// <summary>  Measured frequency         </summary>
        public double Freq_GHz;
        /// <summary>  Measured close loop frequency         </summary>
        public double FreqLock_GHz;
        /// <summary>  </summary>
        public double FreqLockedChange_GHz;
        /// <summary/>
        public double RthGB_ohm;
        /// <summary> Measured GB Tec current        /// </summary>
        public double ITecGB_mA;
        /// <summary> Measured GB Tec Voltage        /// </summary>
        public double VTecGB_V;
        ///// <summary> Mod Bias coresponding to min optical power in Mod Se LV sweep</summary>
        //public double MzDcModVMin_V;
        ///// <summary> Mod Bias coresponding to max optical power in Mod Se LV sweep</summary>
        //public double MzDcModVmax_V;
        /// <summary/>
        public double FibrePwrPeak_dBm;
        /// <summary/>
        public double FibrePwrPeak_CL_dBm;
        /// <summary> </summary>
        public double PwrAtPeakChange_OL_dB;
        /// <summary/>
        public double FibrePwrQuad_dBm;
        /// <summary/>
        public double FibrePwrQuad_CL_dBm;
        /// <summary> </summary>
        public double PwrAtQuadChange_OL_dB;
        /// <summary/>
        public double FibrePwrTrough_dBm;
        /// <summary> </summary>
        public double PwrAtTroughChange_OL_dB;

        /// <summary/>
        public double MzVcmCal_V;
        /// <summary> Mod SE Vpi </summary>
        public double MzDcVpi_V;
        /// <summary> DC ER In  Mod Se LV </summary>
        public double MzDcEr_dB;

        /// <summary/>
        public double ItxLock_mA;
        /// <summary/>
        public double IrxLock_mA;
        /// <summary/>
        public double LockRatio;

        /// <summary/>
        public double ItxLock_CL_mA;
        /// <summary/>
        public double IrxLock_CL_mA;
        /// <summary/>
        public double LockRatio_CL;
        /// <summary/>
        //public double LockerSlopeEff;
        ///// <summary/>
        //public double PhaseRatioSlopeEff;
        
        /// <summary> </summary>
        public bool TapLevellingAtQuadOk;
        /// <summary> </summary>
        public bool TapLevellingAtPeakOk;
        /// <summary> </summary>
        public double PwrAtQuadChange_CL_dB;
        /// <summary> </summary>
        public double PwrAtPeakChange_CL_dB;
        /// <summary/>
        public double TapCompPhotoCurrentThrough_mA;
        /// <summary/>
        public double TapCompPhotoCurrentQuad_mA;
        /// <summary/>
        public double TapCompPhotoCurrentPeak_mA;

        /// <summary/>
        public double TapCompPhotoCurrentQuad_CL_mA;
        /// <summary/>
        public double TapCompPhotoCurrentPeak_CL_mA;
        /// <summary/>
        public double ISoaTapLevel_Quad_mA;

        /// <summary/>
        public double MzLeftArmModPeak_V;        
        /// <summary/>
        public double MzRightArmModPeak_V;        
        /// <summary/>
        public double MzLeftArmModQuad_V;       
        /// <summary/>
        public double MzRightArmModQuad_V;  
        /// <summary/>
        public double MzLeftArmModMinima_V ;       
        /// <summary/>
        public double MzRightArmModMinima_V;
        /// <summary/>
        public double PackagePower_W;

        /// <summary/> Left Imb current to get Min optical power in Imb Diff LI sweep <summary/>
        public double MzLeftArmImb_mA;
        /// <summary/> Right Imb current to get Min optical power in Imb Diff LI sweep <summary/>
        public double MzRightArmImb_mA;
        /// <summary> Vcc current dissipation  </summary>
        public double VccTotalCurrent_mA;
        ///// <summary> Left Mod Se LV sweep data file </summary>
        //public string SeLV_LeftModSweepFile;
        ///// <summary> Right Mod Se LV sweep data file </summary>
        //public string SeLV_RightModSweepFile;
        /// <summary> Imb Differential LI sweep data file </summary>
        public string DiffLI_ImbsFile;
        /// <summary> Mod Differential LV sweep data file </summary>
        public string DiffLV_ModFile;
    }
    /// <summary>
    /// struct defines Data to retrieve from previous stage to use as reference during the test
    /// </summary>
    public struct IlmzQualChanRefData
    {
        /// <summary> </summary>
        public double Freq_Ghz;
        /// <summary> </summary>
        public double Vcm_V;
        /// <summary> </summary>
        public double ItxLock_mA;
        /// <summary> </summary>
        public double IrxLock_mA;
        /// <summary> </summary>
        public double LockerRatio;
        /// <summary> </summary>
        public double PhaseRatioSlopeEff;
        /// <summary> </summary>
        public double FibrePwrPeak_dBm;
        /// <summary> </summary>
        public double FibrePwrQuad_dBm;
        /// <summary> </summary>
        public double FibrePwrTrough_dBm;
        /// <summary> </summary>
        public double TapPhotocurrentQuad_A;

        /// <summary/> Left Imb current to get Min optical power in Imb Diff LI sweep <summary/>
        public double MzLeftArmImb_mA;
        /// <summary/> Right Imb current to get Min optical power in Imb Diff LI sweep <summary/>
        public double MzRightArmImb_mA;
        /// <summary> </summary>
        //public double TapPhotocurrentPeak_A;


    }
    /// <summary>
    /// Channel Dsdbr setting and characterise parameters from previous stage
    /// </summary>
    public struct IlmzQualChanPreviousData : IComparable
    {
        /// <summary> DSDBR Settings in previous stage </summary>
        public DsdbrChannelData DsdbrData;
        /// <summary> channel character data from previous stage to make reference to </summary>
        public IlmzQualChanRefData RefChanData;

        /// <summary>
        /// sort the measure data by Itu channel index
        /// </summary>
        /// <param name="obj"> Qual test data</param>
        /// <returns></returns>
        public int CompareTo(object obj)
        {
            IlmzQualChanPreviousData chData = (IlmzQualChanPreviousData)obj;

            int compareResult = this.DsdbrData.ItuChannelIndex.CompareTo(
                chData.DsdbrData.ItuChannelIndex);
            return compareResult;
        }
    }
    
    /// <summary>
    /// 
    /// </summary>
    public class IlmzQualChanData : IComparable
    {
       
        /// <summary>
        /// reference channel data
        /// </summary>
        public IlmzQualChanPreviousData ChanRefData;
        /// <summary> qual test result </summary>
        public IlmzChannelQualTestRstData ChanMeasData;
        
        /// <summary>
        /// sort the measure data by Itu channel index
        /// </summary>
        /// <param name="obj"> Qual test result data </param>
        /// <returns></returns>
        public int CompareTo(object obj)
        {
            IlmzQualChanData chData = (IlmzQualChanData)obj;

            int compareResult = this.ChanRefData.DsdbrData.ItuChannelIndex.CompareTo(
                chData.ChanRefData.DsdbrData .ItuChannelIndex);
            return compareResult;
        }
        

    }
}

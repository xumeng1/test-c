// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// DsdbrTuning.cs
//
// Author: Tony Foster, 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Threading;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.Toolkit.CloseGrid;
using Bookham.TestSolution.TcmzCommonData;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestSolution.TcmzCommonInstrs;
using System.IO;
namespace Bookham.TestSolution.TcmzCommonUtils
{

    /// <summary>
    /// Choose to test the different EOL 16/12GHz.
    /// </summary>
    internal static class ChannelSelectTest
    {
        public static bool GetSelectTestChannelCondition(DatumList TuneParams, double initIRear, double initIPhase)
        {
            //String LASER_WAFER_SIZE = TuneParams.ReadString("LASER_WAFER_SIZE").Trim();
            //if (!string.IsNullOrEmpty(LASER_WAFER_SIZE)) return true;
            String BandType = TuneParams.ReadString("BandType").Trim();
            //if (BandType.Equals("C", StringComparison.InvariantCultureIgnoreCase)) return true;
            if (BandType.Equals("L", StringComparison.InvariantCultureIgnoreCase)) return false;

            //need todo Band == "c"
            Double IFsNonConst_mA = TuneParams.ReadDouble("IFsNonConst_mA");

            //get LM
            //Todo: get the Num_LM num from SM file.
            //int NUM_LM = TuneParams.ReadSint32("NUM_LM");
            int MinimumLM, MaximumLM;
            int MinimumLM_L, MaximumLM_L;
            int MinimumLM_M, MaximumLM_M;
            int Supermode = TuneParams.ReadSint32("Supermode");
            int LongitudinalMode = TuneParams.ReadSint32("LongitudinalMode");
            String[] SmFiles = TuneParams.ReadStringArray("SmFiles");
            String[] NUM_LM = TuneParams.ReadStringArray("NUM_LM");
            //int Num_Lm = NUM_LM[Supermode];
            string Num_Lm = NUM_LM[Supermode];
            int Num = Convert.ToInt32(Num_Lm);
            
            if (LongitudinalMode == 0)
            {
                ChannelSelectTest.GetMaximumAndMinimumWidthOfLM(Supermode, LongitudinalMode, SmFiles,
                out MinimumLM, out MaximumLM);
                ChannelSelectTest.GetMaximumAndMinimumWidthOfLM_M(Supermode, LongitudinalMode + 1, SmFiles,
                out MinimumLM_M, out MaximumLM_M);

                bool SelectTestFlag = initIPhase > 7 ||
                 initIRear < 2.5 || initIRear > 55 ||
                 IFsNonConst_mA < 0.1 || IFsNonConst_mA > 4.9 ||
                 MinimumLM < 8 || MaximumLM > 12 ||
                 MinimumLM_M < 8 || MaximumLM_M > 12;

                return SelectTestFlag;
            }
            else if (LongitudinalMode == Num - 1)
            {
                ChannelSelectTest.GetMaximumAndMinimumWidthOfLM(Supermode, LongitudinalMode, SmFiles,
                out MinimumLM, out MaximumLM);
                ChannelSelectTest.GetMaximumAndMinimumWidthOfLM_L(Supermode, LongitudinalMode - 1, SmFiles,
                    out MinimumLM_L, out MaximumLM_L);

                bool SelectTestFlag = initIPhase > 7 ||
                 initIRear < 2.5 || initIRear > 55 ||
                 IFsNonConst_mA < 0.1 || IFsNonConst_mA > 4.9 ||
                 MinimumLM < 8 || MaximumLM > 12 ||
                 MinimumLM_L < 8 || MaximumLM_L > 12;

                return SelectTestFlag;
            }
            else
            {
                ChannelSelectTest.GetMaximumAndMinimumWidthOfLM(Supermode, LongitudinalMode, SmFiles,
                    out MinimumLM, out MaximumLM);
                ChannelSelectTest.GetMaximumAndMinimumWidthOfLM_L(Supermode, LongitudinalMode - 1, SmFiles,
                    out MinimumLM_L, out MaximumLM_L);
                ChannelSelectTest.GetMaximumAndMinimumWidthOfLM_M(Supermode, LongitudinalMode + 1, SmFiles,
                    out MinimumLM_M, out MaximumLM_M);

                bool SelectTestFlag = initIPhase > 7 ||
                initIRear < 2.5 || initIRear > 55 ||
                IFsNonConst_mA < 0.1 || IFsNonConst_mA > 4.9 ||
                MinimumLM < 8 || MaximumLM > 12 ||
                MinimumLM_L < 8 || MaximumLM_L > 12 ||
                MinimumLM_M < 8 || MaximumLM_M > 12;

                return SelectTestFlag;
            }

            //bool SelectTestFlag = initIPhase > 7 ||
            //     initIRear < 2.5 || initIRear > 55 ||
            //     IFsNonConst_mA < 0.1 || IFsNonConst_mA > 4.9 ||
            //     MinimumLM < 8 || MaximumLM > 12 ||
            //     MinimumLM_L < 8 || MaximumLM_L > 12 ||
            //     MinimumLM_M < 8 || MaximumLM_M > 12;

            //Todo:if LongitudinalMode nearly this mode more than 12, or less than 8 need to test.
            //return SelectTestFlag;
        }

        public static void GetMaximumAndMinimumWidthOfLM(int Supermode, int LongitudinalMode, 
                                     String[] SmFiles,out int MinimumLM, out int MaximumLM)
        {
            int baseRow = LongitudinalMode * 9 + 8 - 1;
            int baseCol = 8 - 1;
            string[] qaMetrics = File.ReadAllLines(SmFiles[Supermode]);
            for (int i = 0; i < qaMetrics.Length; i++)
            {
                if (qaMetrics[i].Contains("Slope Window Size"))
                {
                    baseRow = LongitudinalMode * 9 + i + 7;
                    break;
                }
            }
            string[] strArr = qaMetrics[baseRow].Split(',');
            MaximumLM = int.Parse(strArr[baseCol]);
            strArr = qaMetrics[baseRow + 1].Split(',');
            MinimumLM = int.Parse(strArr[baseCol]);
        }

        public static void GetMaximumAndMinimumWidthOfLM_L(int Supermode, int LongitudinalMode,
                                    String[] SmFiles,out int MinimumLM_L, out int MaximumLM_L)
        {
            int baseRow = LongitudinalMode * 9 + 8 - 1;
            int baseCol = 8 - 1;
            string[] qaMetrics = File.ReadAllLines(SmFiles[Supermode]);
            for (int i = 0; i < qaMetrics.Length; i++)
            {
                if (qaMetrics[i].Contains("Slope Window Size"))
                {
                    baseRow = LongitudinalMode * 9 + i + 7;
                    break;
                }
            }
            string[] strArr = qaMetrics[baseRow].Split(',');
            MaximumLM_L = int.Parse(strArr[baseCol]);
            strArr = qaMetrics[baseRow + 1].Split(',');
            MinimumLM_L = int.Parse(strArr[baseCol]);
        }

        public static void GetMaximumAndMinimumWidthOfLM_M(int Supermode, int LongitudinalMode,
                                    String[] SmFiles,out int MinimumLM_M, out int MaximumLM_M)
        {
            int baseRow = LongitudinalMode * 9 + 8 - 1;
            int baseCol = 8 - 1;
            string[] qaMetrics = File.ReadAllLines(SmFiles[Supermode]);
            for (int i = 0; i < qaMetrics.Length; i++)
            {
                if (qaMetrics[i].Contains("Slope Window Size"))
                {
                    baseRow = LongitudinalMode * 9 + i + 7;
                    break;
                }
            }
            string[] strArr = qaMetrics[baseRow].Split(',');
            MaximumLM_M = int.Parse(strArr[baseCol]);
            strArr = qaMetrics[baseRow + 1].Split(',');
            MinimumLM_M = int.Parse(strArr[baseCol]);
        }

    }
    /// <summary>
    /// Static class for DSDBR frequency tuning
    /// </summary>
    public static class DsdbrTuning
    {
        #region ITU and EOL Tune

        /// <summary>
        /// ITU and end of life tuning
        /// </summary>
        /// <param name="TuneParams">Tune configuration parameters</param>
        /// <returns>Tuning results</returns>
        public static DatumList ItuEolTune(DatumList TuneParams)
        {
            // Initialise 
            double ituFreq_GHz = TuneParams.ReadDouble("ItuFreq_GHz");
            double calFreq_GHz = TuneParams.ReadDouble("CalFreq_GHz");
            double posOffsetFreq_GHz = calFreq_GHz + TuneParams.ReadDouble("FreqOffsetPos_GHz");
            double negOffsetFreq_GHz = calFreq_GHz - TuneParams.ReadDouble("FreqOffsetNeg_GHz");

            DatumList posPhaseTuneResults = new DatumList();
            DatumList negPhaseTuneResults = new DatumList();
            double deltaFreq_GHz = 0;
            double smsrPos = 0;
            double smsrNeg = 0;
            double pte = 0;
            bool tuneOk = false;

            // Add default values
            posPhaseTuneResults.AddDouble("Freq_GHz", double.NaN);
            posPhaseTuneResults.AddDouble("PhaseCurrent_mA", double.NaN);
            negPhaseTuneResults.AddDouble("Freq_GHz", double.NaN);
            negPhaseTuneResults.AddDouble("PhaseCurrent_mA", double.NaN);

            // Record phase & rear currents
            double initIRear = DsdbrUtils.ReadSectionCurrent_mA(DSDBRSection.Rear);
            double initIPhase = DsdbrUtils.ReadSectionCurrent_mA(DSDBRSection.Phase);
            double iRear = initIRear;

            // Return data
            DatumList rtnData = new DatumList();
            //define ReCalibrateFlag according to PTE Error
            double PTE_Error = 0;
            bool ReCalibrateFlag = false;
            bool ReDoEol = false;
            //Get Select Test condition
            bool SelectTestFlag = ChannelSelectTest.GetSelectTestChannelCondition(TuneParams, initIRear, initIPhase);

            // EOL tests
            Re_Tune_Reason ReTuneReason = Re_Tune_Reason.NoReTune;
            StringBuilder EOL_Testing_Info = new StringBuilder();
            do
            {
                if (SelectTestFlag || ReCalibrateFlag)
                {
                    ReDoEol = ReCalibrateFlag;
                    for (int tuneCount = 0; tuneCount < 5; tuneCount++)
                    {
                        // Save current Irear
                        iRear = DsdbrUtils.ReadSectionCurrent_mA(DSDBRSection.Rear);

                        // Do a clockwise start to cope with missing lines
                        DsdbrUtils.ClockwiseStart(initIPhase, iRear);

                        // Phase tune to end of life positive frequency
                        // Tune to [TargetFrequency-freqTolerance_GHz, PositiveInfinity ]
                        //posPhaseTuneResults = PhaseTune(posOffsetFreq_GHz, double.PositiveInfinity, 
                        //    freqTolerance_GHz, 0, PteSlopeEolPos, PteInterceptEolPos);

                        // Tune to [TargetFrequency-freqTolerance_GHz, TargetFrequency+freqTolerance_GHz ]
                        
                        posPhaseTuneResults = PhaseTune(posOffsetFreq_GHz, 2,
                            freqTolerance_GHz, 0, PteSlopeEolPos, PteInterceptEolPos);
                        pte = posPhaseTuneResults.ReadDouble("PhaseTuningEff");

                        EOL_Testing_Info.AppendFormat("{{{0}Pos;{4};{1:f3};{2:f3};{3:f3}", tuneCount, iRear,
                            posPhaseTuneResults.ReadDouble("PhaseCurrent_mA"),
                            posPhaseTuneResults.ReadDouble("Freq_GHz"),
                            posPhaseTuneResults.ReadString("EOL_Testing_Info"));
                        // If EOL pos frequency not reached retune rear down then try again
                        if (!posPhaseTuneResults.ReadBool("TuneOK"))
                        {
                            ReTuneReason = Re_Tune_Reason.PhaseTunePosFail;
                            EOL_Testing_Info.AppendFormat(";{0}}}", "NA");
                            deltaFreq_GHz += RearTune(rearAdjustFreq_GHz, initIPhase);
                            continue;
                        }

                        // Measure SMSR at EOL pos frequency
                        smsrPos = SMSRMeasurement.MeasureSMSR(posOffsetFreq_GHz);
                        EOL_Testing_Info.AppendFormat(";{0}}}", smsrPos.ToString("f3"));
                        if (smsrPos < smsrPosMinLimit_dB)
                        {

                            //Todo:freqTolerance_GHz First [-0.25--2]GHz
                            //Todo: if SMSR fail and fail on >0.25 freqTolerance_GHz, trun phase freqTolerance_GHz[-0.25--0.25]
                            ReTuneReason |= Re_Tune_Reason.SMSREOLPosFail;
                            double currFreq_GHz = posPhaseTuneResults.ReadDouble("Freq_GHz");
                            if (Math.Abs(currFreq_GHz - posOffsetFreq_GHz) > freqTolerance_GHz)
                            {
                                // Need fine tune phase
                                //old method
                                //DatumList posPhaseTuneResults_FineTune = PhaseTune(posOffsetFreq_GHz, pte);
                                //new method
                                posPhaseTuneResults = PhaseTune(posOffsetFreq_GHz, pte);
                                if (posPhaseTuneResults.ReadBool("TuneOK"))
                                {
                                    smsrPos = SMSRMeasurement.MeasureSMSR(posOffsetFreq_GHz);
                                    if (smsrPos < smsrPosMinLimit_dB)
                                    {
                                        deltaFreq_GHz += RearTune(rearAdjustFreq_GHz, initIPhase);
                                        continue;
                                    }
                                }
                                else
                                {
                                    deltaFreq_GHz += RearTune(rearAdjustFreq_GHz, initIPhase);
                                    continue;
                                }
                            }
                            else
                            {
                                deltaFreq_GHz += RearTune(rearAdjustFreq_GHz, initIPhase);
                                continue;
                            }
                        }

                        // Phase tune to end of life negative frequency
                        DsdbrUtils.ClockwiseStart(initIPhase, iRear);

                        // Tune to [NegtiveInfinity, TargetFrequency + freqTolerance_GHz]
                        //negPhaseTuneResults = PhaseTune(negOffsetFreq_GHz, freqTolerance_GHz, double.PositiveInfinity, 0, PteSlopeEolNeg, PteInterceptEolNeg);

                        // Tune to [TargetFrequency-freqTolerance_GHz, TargetFrequency+freqTolerance_GHz ]                        

                        negPhaseTuneResults = PhaseTune(negOffsetFreq_GHz, freqTolerance_GHz, 2, 0, PteSlopeEolNeg, PteInterceptEolNeg);
                        pte = negPhaseTuneResults.ReadDouble("PhaseTuningEff");

                        EOL_Testing_Info.AppendFormat("{{{0}Neg;{4};{1:f3};{2:f3};{3:f3}", tuneCount, iRear,
                            negPhaseTuneResults.ReadDouble("PhaseCurrent_mA"),
                            negPhaseTuneResults.ReadDouble("Freq_GHz"),//191516.666
                            negPhaseTuneResults.ReadString("EOL_Testing_Info"));//191559.875
                        // If EOL neg frequency not reached retune rear up then try again                
                        if (!negPhaseTuneResults.ReadBool("TuneOK"))
                        {
                            ReTuneReason |= Re_Tune_Reason.PhaseTuneNegFail;
                            EOL_Testing_Info.AppendFormat(";{0}}}", "NA");
                            deltaFreq_GHz += RearTune(-rearAdjustFreq_GHz, initIPhase);
                            continue;
                        }

                        // Measure SMSR at EOL neg frequency
                        smsrNeg = SMSRMeasurement.MeasureSMSR(negOffsetFreq_GHz);
                        EOL_Testing_Info.AppendFormat(";{0}}}", smsrNeg.ToString("f3"));
                        if (smsrNeg < smsrNegMinLimit_dB)
                        {
                            //Todo:freqTolerance_GHz First [-0.25--2]GHz
                            //Todo: if SMSR fail and fail on >0.25 freqTolerance_GHz, trun phase freqTolerance_GHz[-0.25--0.25]
                            ReTuneReason |= Re_Tune_Reason.SMSREOLNegFail;
                            double currFreq_GHz = negPhaseTuneResults.ReadDouble("Freq_GHz");
                            if (Math.Abs(currFreq_GHz - negOffsetFreq_GHz) > freqTolerance_GHz)
                            {
                                //Need fine tune phase
                                //old method
                                //DatumList negPhaseTuneResults_FineTune = PhaseTune(negOffsetFreq_GHz, pte);
                                negPhaseTuneResults = PhaseTune(negOffsetFreq_GHz, pte);
                                if (negPhaseTuneResults.ReadBool("TuneOK"))
                                {
                                    smsrNeg = SMSRMeasurement.MeasureSMSR(negOffsetFreq_GHz);
                                    if (smsrNeg < smsrNegMinLimit_dB)
                                    {
                                        deltaFreq_GHz += RearTune(-rearAdjustFreq_GHz, initIPhase);
                                        continue;
                                    }
                                }
                                else
                                {
                                    deltaFreq_GHz += RearTune(-rearAdjustFreq_GHz, initIPhase);
                                    continue;
                                }
                            }
                            else
                            {
                                deltaFreq_GHz += RearTune(-rearAdjustFreq_GHz, initIPhase);
                                continue;
                            }
                        }

                        // Tuning has succeeded if we reach this point
                        tuneOk = true;
                        break;
                    }

                    rtnData.AddDouble("FreqOffsetPos_GHz", posPhaseTuneResults.ReadDouble("Freq_GHz"));
                    rtnData.AddDouble("IphaseOffsetPos_mA", posPhaseTuneResults.ReadDouble("PhaseCurrent_mA"));
                    rtnData.AddDouble("SmsrOffsetPos_dB", smsrPos);
                    rtnData.AddDouble("FreqOffsetNeg_GHz", negPhaseTuneResults.ReadDouble("Freq_GHz"));
                    rtnData.AddDouble("IphaseOffsetNeg_mA", negPhaseTuneResults.ReadDouble("PhaseCurrent_mA"));
                    rtnData.AddDouble("SmsrOffsetNeg_dB", smsrNeg);
                    rtnData.AddDouble("DeltaFreq_GHz", deltaFreq_GHz);

                    // Do a clockwise start 
                    DsdbrUtils.ClockwiseStart(initIPhase, iRear);
                }

                // Phase tune to cal freq and measure
                DatumList phaseTuneResults = new DatumList();
                phaseTuneResults = PhaseTune(calFreq_GHz, 0);
                if (!phaseTuneResults.ReadBool("TuneOK")) tuneOk = false;
                if (rtnData.IsPresent("FreqCal_GHz"))
                    rtnData.UpdateDouble("FreqCal_GHz", phaseTuneResults.ReadDouble("Freq_GHz"));
                else
                    rtnData.AddDouble("FreqCal_GHz", phaseTuneResults.ReadDouble("Freq_GHz"));
                if (rtnData.IsPresent("IphaseCal_mA"))
                    rtnData.UpdateDouble("IphaseCal_mA", phaseTuneResults.ReadDouble("PhaseCurrent_mA"));
                else
                    rtnData.AddDouble("IphaseCal_mA", phaseTuneResults.ReadDouble("PhaseCurrent_mA"));

                // Phase tune to ITU freq and measure
                phaseTuneResults = PhaseTune(ituFreq_GHz, 0);
                if (!phaseTuneResults.ReadBool("TuneOK")) tuneOk = false;
                if (rtnData.IsPresent("FreqItu_GHz"))
                    rtnData.UpdateDouble("FreqItu_GHz", phaseTuneResults.ReadDouble("Freq_GHz"));
                else
                    rtnData.AddDouble("FreqItu_GHz", phaseTuneResults.ReadDouble("Freq_GHz"));
                if (rtnData.IsPresent("IphaseItu_mA"))
                    rtnData.UpdateDouble("IphaseItu_mA", phaseTuneResults.ReadDouble("PhaseCurrent_mA"));
                else
                    rtnData.AddDouble("IphaseItu_mA", phaseTuneResults.ReadDouble("PhaseCurrent_mA"));

                // Calculate the PhaseTuneEff - PTE_6GHz
                if (rtnData.ReadDouble("FreqCal_GHz") - rtnData.ReadDouble("FreqItu_GHz") != 0)
                {
                    double phaseTuneEff =
                        (rtnData.ReadDouble("IphaseCal_mA") - rtnData.ReadDouble("IphaseItu_mA")) / (rtnData.ReadDouble("FreqCal_GHz") - rtnData.ReadDouble("FreqItu_GHz"));
                    if (rtnData.IsPresent("PhaseTuneEff"))
                        rtnData.UpdateDouble("PhaseTuneEff", phaseTuneEff);
                    else
                        rtnData.AddDouble("PhaseTuneEff", phaseTuneEff);
                }
                else
                {
                    // This is just in case...
                    if (rtnData.IsPresent("PhaseTuneEff"))
                        rtnData.UpdateDouble("PhaseTuneEff", phaseTuneResults.ReadDouble("PhaseTuningEff"));
                    else
                        rtnData.AddDouble("PhaseTuneEff", phaseTuneResults.ReadDouble("PhaseTuningEff"));
                }
                //Cal Estimated PTE
                //double EstimatedPTE = 0.019 * initIPhase + 0.035;
                double EstimatedPTE = PteSlope * initIPhase + PteIntercept;
                //Cal Measured PTE
                double MeasuredPTE = (rtnData.ReadDouble("IphaseItu_mA") - rtnData.ReadDouble("IphaseCal_mA")) /
                    (rtnData.ReadDouble("FreqItu_GHz") - rtnData.ReadDouble("FreqCal_GHz"));
                PTE_Error = EstimatedPTE / MeasuredPTE * 100 - 100;
                if (!ReCalibrateFlag)
                {
                    if (!SelectTestFlag)
                    {
                        ReCalibrateFlag = PTE_Error < -25 || PTE_Error > 25;
                    }
                }
            } while (ReCalibrateFlag && !ReDoEol);

            //add eol test flag
            bool DoneEOLTestFlag = SelectTestFlag || ReCalibrateFlag || ReDoEol;
            rtnData.AddBool("DoneEOLTestFlag", DoneEOLTestFlag);
            //rtnData.AddBool("SelectTestFlag", SelectTestFlag);
            //Re_Tune_Reason
            rtnData.AddEnum("Re_Tune_Reason", ReTuneReason);
            rtnData.AddString("EOL_Testing_Info", string.Format("PTE_Error={0:f3};{1}", PTE_Error, EOL_Testing_Info.ToString()));
            rtnData.AddBool("TuneOk", tuneOk);
            return rtnData;
        }

        #endregion

        #region Phase Tune

        /// <summary>
        /// Phase tune DSDBR
        /// </summary>
        /// <param name="TargetFreq_GHz">Target frequency in GHz</param>
        /// <param name="PhaseTuningEff">Phase tuning efficiency. '0.0' to calculate internally</param>
        /// <returns>Tuning results</returns>
        public static DatumList PhaseTune(double TargetFreq_GHz, double PhaseTuningEff)
        {
            DatumList returnData = new DatumList();

            returnData = PhaseTune(TargetFreq_GHz, freqTolerance_GHz, freqTolerance_GHz, PhaseTuningEff, PteSlope, PteIntercept);

            return returnData;
        }

        /// <summary>
        /// Phase tune DSDBR
        /// </summary>
        /// <param name="TargetFreq_GHz">Target frequency in GHz</param>
        /// <param name="FreqTolerancePos_GHz">Absolute value of frequency tolerance at positive side of target frequency in GHz</param>
        /// <param name="FreqToleranceNeg_GHz">Absolute value of frequency tolerance at negative side of target frequency in GHz</param>
        /// <param name="PhaseTuningEff">Phase tuning efficiency. '0.0' to calculate internally</param>
        /// <param name="PhaseTuningEffSlope">The slope of Pte vs Iphase</param>
        /// <param name="PhaseTuningEffIntercept">The intercept of Pte vs Iphase</param>
        /// <returns>Tuning results</returns>
        public static DatumList PhaseTune(double TargetFreq_GHz, double FreqTolerancePos_GHz, 
            double FreqToleranceNeg_GHz, double PhaseTuningEff, double PhaseTuningEffSlope, double PhaseTuningEffIntercept)
        {
            // Save initial conditions
            double initFreq_GHz = Measurements.ReadFrequency_GHz();
            double initIphase = DsdbrUtils.ReadSectionCurrent_mA(DSDBRSection.Phase);
            double initIrear = DsdbrUtils.ReadSectionCurrent_mA(DSDBRSection.Rear);

            // Set default phase tuning efficiency if not supplied
            if (PhaseTuningEff < MinPhaseTuningEff || PhaseTuningEff > MaxPhaseTuningEff)
            {
                PhaseTuningEff = initIphase * PhaseTuningEffSlope + PhaseTuningEffIntercept;
                PhaseTuningEff = Math.Max(PhaseTuningEff, MinPhaseTuningEff);
                PhaseTuningEff = Math.Min(PhaseTuningEff, MaxPhaseTuningEff);
            }

            // Initialise tuning
            double deltaIphaseReduction = 1;
            double iPhase = initIphase;
            double lastGoodFreq = initFreq_GHz;
            double lastGoodIphase = initIphase;
            double lastGoodPhaseTuningEff = PhaseTuningEff;
            double freq_GHz = initFreq_GHz;
            int loopCount = 0;
            double deltaIphaseLimit = 0;
            iPhase = DsdbrUtils.ReadSectionCurrent_mA(DSDBRSection.Phase);

            StringBuilder EOL_Testing_Info = new StringBuilder();
            EOL_Testing_Info.AppendFormat("PhaseTune{{0;{0:f3};{1:f3};", iPhase, freq_GHz);
            // Tuning loop
            while ((freq_GHz < TargetFreq_GHz - FreqToleranceNeg_GHz || freq_GHz > TargetFreq_GHz + FreqTolerancePos_GHz) && loopCount < PhaseTuneMaxIterations)
            {
                // Calculate adjusted phase current
                iPhase = DsdbrUtils.ReadSectionCurrent_mA(DSDBRSection.Phase);
                double deltaIphase = (TargetFreq_GHz - freq_GHz) * PhaseTuningEff * deltaIphaseReduction;

                // Clip deltaIphase to less than 32GHz max 
                if (deltaIphase > 0)
                {
                    deltaIphaseLimit = (PteSlopeEolPos * lastGoodIphase + PteInterceptEolPos) * 0.8 * 32;
                    deltaIphase = Math.Min(deltaIphase, deltaIphaseLimit);
                }
                else
                {
                    deltaIphaseLimit = -(PteSlopeEolNeg * lastGoodIphase + PteInterceptEolNeg) * 0.8 * 32;
                    deltaIphase = Math.Max(deltaIphase, deltaIphaseLimit);
                }

                // Calculate new Iphase
                iPhase += deltaIphase;

                // Exit if phase current out of spec
                if (iPhase < iphaseMin_mA || iPhase > iphaseMax_mA) break;

                // Apply adjusted phase current
                DsdbrUtils.SetSectionCurrent_mA(DSDBRSection.Phase, iPhase);
                Thread.Sleep(LoopDelay_ms);

                // Adjust values
                double deltaFreq_GHz = Measurements.ReadFrequency_GHz() - freq_GHz;
                freq_GHz += deltaFreq_GHz;
                double newPhaseTuningEff = deltaFreq_GHz != 0 ? deltaIphase / deltaFreq_GHz : PhaseTuningEff;

                // Check for mode hop 
                if (newPhaseTuningEff >= 0)
                {
                    // No mode hop
                    // Record good Iphase & recalculate PhaseTuningEff
                    lastGoodIphase = iPhase;
                    if (newPhaseTuningEff > MinPhaseTuningEff && newPhaseTuningEff < MaxPhaseTuningEff) PhaseTuningEff = (PhaseTuningEff + newPhaseTuningEff) / 2;
                }
                else
                {
                    int count = 0;
                    // Mode hopped
                    // Clockwise start back to last known good position
                    do
                    {
                        iPhase = lastGoodIphase;
                        DsdbrUtils.ClockwiseStart(initIphase, initIrear);
                        DsdbrUtils.SetSectionCurrent_mA(DSDBRSection.Phase, lastGoodIphase);
                        Thread.Sleep(LoopDelay_ms);

                        // Read frequency & reduce Iphase step
                        freq_GHz = Measurements.ReadFrequency_GHz();
                        count++;
                    } while (Math.Abs(lastGoodFreq - freq_GHz) > 2 && count < 3);

                    if ((count == 3) || (deltaIphaseReduction == MinIphaseReduction))
                    {
                        ////Rear Truning
                        //RearTune(rearAdjustFreq_GHz, initIphase);
                        break;
                    }
                    deltaIphaseReduction = Math.Max(deltaIphaseReduction / 2, MinIphaseReduction);
                }

                // Increment loop count
                loopCount++;
                EOL_Testing_Info.AppendFormat("{0};{1:f3};{2:f3};", loopCount, iPhase, freq_GHz);
            }
            EOL_Testing_Info.Remove(EOL_Testing_Info.Length - 1, 1);
            EOL_Testing_Info.Append("}");

            // Return data
            DatumList rtn = new DatumList();
            //old mehtod
            //rtn.AddDouble("Freq_GHz", Measurements.ReadFrequency_GHz());
            //rtn.AddDouble("PhaseCurrent_mA", DsdbrUtils.ReadSectionCurrent_mA(DSDBRSection.Phase));
            rtn.AddDouble("Freq_GHz", freq_GHz);
            rtn.AddDouble("PhaseCurrent_mA", iPhase);
            rtn.AddString("EOL_Testing_Info", EOL_Testing_Info.ToString());

            // Reset to original conditions if tune failed
            if (freq_GHz < TargetFreq_GHz - FreqToleranceNeg_GHz || freq_GHz > TargetFreq_GHz + FreqTolerancePos_GHz)
            {
                // Tune failed
                // Set status & configure device back to initial conditions
                rtn.AddBool("TuneOK", false);
                DsdbrUtils.ClockwiseStart(initIphase, initIrear);
                Thread.Sleep(LoopDelay_ms);
            }
            else
            {
                // Tune OK
                rtn.AddBool("TuneOK", true);
            }

            // Return
            //rtn.AddDouble("Freq_GHz", Measurements.ReadFrequency_GHz());
            //rtn.AddDouble("PhaseCurrent_mA", DsdbrUtils.ReadSectionCurrent_mA(DSDBRSection.Phase));
            rtn.AddDouble("PhaseTuningEff", PhaseTuningEff);
            return rtn;
        }

        #endregion

        #region Rear Tune

        /// <summary>
        /// Adjust frequency with Rear section current
        /// </summary>
        /// <param name="DeltaFreq_GHz">Target frequency change</param>
        /// <param name="PhaseCurrent_mA">Phase current to set near centre of mode</param>
        /// <returns>Measured frequency change</returns>
        public static double RearTune(double DeltaFreq_GHz, double PhaseCurrent_mA)
        {
            // Set phase current (should be near centre of mode)
            DsdbrUtils.SetSectionCurrent_mA(DSDBRSection.Phase, PhaseCurrent_mA);
            Thread.Sleep(LoopDelay_ms);

            // Read frequency
            double preTuneFreq_GHz = Measurements.ReadFrequency_GHz();

            // Calculate and set new Irear
            // Assume Irear to frequency slope = IrearFreqTuneConst/Irear GHz/mA. Limit to 20 GHz/mA max.
            double iRear_mA = DsdbrUtils.ReadSectionCurrent_mA(DSDBRSection.Rear);
            iRear_mA += DeltaFreq_GHz / Math.Min(20, IrearFreqTuneConst / iRear_mA);
            iRear_mA = Math.Max(iRear_mA, irearMin_mA);
            iRear_mA = Math.Min(iRear_mA, irearMax_mA);
            DsdbrUtils.SetSectionCurrent_mA(DSDBRSection.Rear, iRear_mA);
            Thread.Sleep(LoopDelay_ms);

            // Read adjusted frequency
            double postTuneFreq_GHz = Measurements.ReadFrequency_GHz();

            // Return delta frequency
            return postTuneFreq_GHz - preTuneFreq_GHz;
        }

        #endregion

        #region Soft Lock
        /// <summary>
        /// Set a locker ratio
        /// </summary>
        /// <param name="TargetMonitorRatio">Target locker ratio</param>
        /// <param name="PhaseRatioSlopeEff">Iphase to Lock ratio slope efficiency</param>
        /// <returns></returns>
        public static DatumList SoftLock(double TargetMonitorRatio, double PhaseRatioSlopeEff)
        {
            // Initialise 
            double lockRatio = DsdbrUtils.ReadLockerCurrents().LockRatio;
            double iRearInit = DsdbrUtils.ReadSectionCurrent_mA(DSDBRSection.Rear);
            double iPhase = DsdbrUtils.ReadSectionCurrent_mA(DSDBRSection.Phase);
            double iPhaseInit = iPhase;
            double iPhasePrev;
            int loopCount = 0;

            // Tuning loop
            while (Math.Abs(TargetMonitorRatio - lockRatio) > lockRatioTolerance && loopCount < SoftLockMaxIterations)
            {

                // Calculate Iphase to achieve target lock ratio
                iPhasePrev = iphaseMax_mA;
                iPhase += (TargetMonitorRatio - lockRatio) * PhaseRatioSlopeEff;

                // Exit if phase current out of spec
                if (iPhase < iphaseMin_mA || iPhase > iphaseMax_mA) break;

                // Apply adjusted phase current
                DsdbrUtils.SetSectionCurrent_mA(DSDBRSection.Phase, iPhase);
                Thread.Sleep(LoopDelay_ms);

                // Read adjusted lock ratio
                lockRatio = DsdbrUtils.ReadLockerCurrents().LockRatio;

                // Increment loop coounter
                loopCount++;
            }

            // Return data
            DatumList rtn = new DatumList();

            // Reset to original conditions if tune failed
            if (Math.Abs(TargetMonitorRatio - lockRatio) > lockRatioTolerance)
            {
                // Tune failed
                // Set status & configure device back to initial conditions
                rtn.AddBool("LockOK", false);
                DsdbrUtils.ClockwiseStart(iPhaseInit, iRearInit);
                Thread.Sleep(LoopDelay_ms);
            }
            else
            {
                // Tune OK
                rtn.AddBool("LockOK", true);
            }

            // Return
            DsdbrUtils.LockerCurrents lc = DsdbrUtils.ReadLockerCurrents();
            rtn.AddDouble("LockRatio", lc.LockRatio);
            rtn.AddDouble("ITx_mA", lc.TxCurrent_mA);
            rtn.AddDouble("IRx_mA", lc.RxCurrent_mA);
            rtn.AddDouble("PhaseCurrent_mA", DsdbrUtils.ReadSectionCurrent_mA(DSDBRSection.Phase));
            if (DsdbrUtils .DsdbrDriverInstrsToUse == DsdbrDriveInstruments .FCU2AsicInstrument )
                rtn.AddSint32("PhaseCurrent_Dac", DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic.IPhase_Dac);
            return rtn;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Phase current minimum
        /// </summary>
        public static double IphaseMin_mA
        {
            set { iphaseMin_mA = value; }
            get { return iphaseMin_mA; }
        }

        /// <summary>
        /// Phase current maximum
        /// </summary>
        public static double IphaseMax_mA
        {
            set { iphaseMax_mA = value; }
            get { return iphaseMax_mA; }
        }

        /// <summary>
        /// Rear current minimum
        /// </summary>
        public static double IrearMin_mA
        {
            set { irearMin_mA = value; }
            get { return irearMin_mA; }
        }

        /// <summary>
        /// Rear current maximum
        /// </summary>
        public static double IrearMax_mA
        {
            set { irearMax_mA = value; }
            get { return irearMax_mA; }
        }

        /// <summary>
        /// Frequency tune tolerance
        /// </summary>
        public static double FreqTolerance_GHz
        {
            set { freqTolerance_GHz = value; }
            get { return freqTolerance_GHz; }
        }

        /// <summary>
        /// SMSR positive frequency offset minimum limit
        /// </summary>
        public static double SmsrPosMinLimit_dB
        {
            set { smsrPosMinLimit_dB = value; }
            get { return smsrPosMinLimit_dB; }
        }

        /// <summary>
        /// SMSR negative frequency offset minimum limit
        /// </summary>
        public static double SmsrNegMinLimit_dB
        {
            set { smsrNegMinLimit_dB = value; }
            get { return smsrNegMinLimit_dB; }
        }

        /// <summary>
        /// Supermode SR minimum limit
        /// </summary>
        public static double SupermodeSrMinLimit_dB
        {
            set { supermodeSrMinLimit_dB = value; }
            get { return supermodeSrMinLimit_dB; }
        }

        /// <summary>
        /// Rear tune frequency step 
        /// </summary>
        public static double RearAdjustFreq_GHz
        {
            set { rearAdjustFreq_GHz = value; }
            get { return rearAdjustFreq_GHz; }
        }

        /// <summary>
        /// Lock Ratio Tolerance 
        /// </summary>
        public static double LockRatioTolerance
        {
            set { lockRatioTolerance = value; }
            get { return lockRatioTolerance; }
        }

        #endregion

        #region private variables

        private static double iphaseMin_mA;
        private static double iphaseMax_mA;
        private static double irearMin_mA;
        private static double irearMax_mA;
        private static double freqTolerance_GHz;
        private static double smsrPosMinLimit_dB;
        private static double smsrNegMinLimit_dB;
        private static double supermodeSrMinLimit_dB;
        private static double rearAdjustFreq_GHz;
        private static double lockRatioTolerance;

        #endregion

        #region Local Constants

        private const double PteSlope = 0.019;
        private const double PteIntercept = 0.035;
        private const double PteSlopeEolPos = 0.02;
        private const double PteSlopeEolNeg = 0.016;
        private const double PteInterceptEolPos = 0.03;
        private const double PteInterceptEolNeg = 0.026;

        private const double MinIphaseReduction = 0.25;
        private const double MinPhaseTuningEff = 0.03;
        private const double MaxPhaseTuningEff = 0.3;
        private const double IrearFreqTuneConst = 60;
        private const int LoopDelay_ms = 100;
        private const int PhaseTuneMaxIterations = 20;
        private const int SoftLockMaxIterations = 10;

        #endregion

    }
}

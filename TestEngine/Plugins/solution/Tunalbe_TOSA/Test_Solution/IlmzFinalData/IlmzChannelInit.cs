using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestSolution.IlmzCommonData;

namespace Bookham.TestSolution.ILMZFinalData
{
    /// <summary>
    /// Class containing initial conditions found after CloseGrid characterisation of DSDBR
    /// and MZ characterisation
    /// </summary>
    public struct IlmzChannelInit
    {
        /// <summary>
        /// DSDBR data from CloseGrid
        /// </summary>
        public DsdbrChannelData Dsdbr;

        /// <summary>
        /// Interpolated MZ setup condition to use
        /// </summary>
        public MzData Mz;
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.Framework.Limits;
using Bookham.TestSolution.IlmzCommonData;

namespace Bookham.TestSolution.ILMZFinalData
{
    /// <summary>
    /// Collection class to contain all channels
    /// </summary>
    public class IlmzChannels
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="ilmzChansInit">Channel initialisers</param>
        /// <param name="pcasSpec">PCAS spec</param>
        /// <param name="lowTempDegC">Low Temperature</param>
        /// <param name="highTempDegC">High Temperature</param>
        public IlmzChannels(IlmzChannelInit[] ilmzChansInit, Specification pcasSpec, 
            double lowTempDegC, double highTempDegC)
        {
            // initialise specification lookup for Mid-Temp limits
            IlmzChannelMeas.InitLookup(pcasSpec);
            IlmzChannelTempMeas.InitSpec(pcasSpec);
            IlmzChannelTempMeas.InitLookupAtTemp(TcmzTestTemp.High, highTempDegC);
            IlmzChannelTempMeas.InitLookupAtTemp(TcmzTestTemp.Low, lowTempDegC);

            int nbrChans = ilmzChansInit.Length;
            this.allChannels = new ILMZChannel[nbrChans];
            for (int ii = 0; ii < nbrChans; ii++)
            {
                this.allChannels[ii] =
                    new ILMZChannel(ilmzChansInit[ii]);
                this.allChannels[ii].Index = ii;
            }
        }

        /// <summary>
        /// The constructor for final_ot Stage
        /// </summary>
        /// <param name="ilmzChansInit">Channel initialisers</param>
        /// <param name="pcasSpec">PCAS spec</param>
        /// <param name="lowTempDegC">Low Temperature</param>
        /// <param name="highTempDegC">High Temperature</param>
        /// <param name="final_OT"></param>
        public IlmzChannels(IlmzChannelInit[] ilmzChansInit, Specification pcasSpec,
            double lowTempDegC, double highTempDegC, bool final_OT)
        {
            // initialise specification lookup for Mid-Temp limits
            IlmzChannelMeas.InitLookup(pcasSpec, true);
            IlmzChannelTempMeas.InitSpec(pcasSpec);
            IlmzChannelTempMeas.InitLookupAtTemp(TcmzTestTemp.High, highTempDegC, true);
            IlmzChannelTempMeas.InitLookupAtTemp(TcmzTestTemp.Low, lowTempDegC, true);

            int nbrChans = ilmzChansInit.Length;
            this.allChannels = new ILMZChannel[nbrChans];
            for (int ii = 0; ii < nbrChans; ii++)
            {
                this.allChannels[ii] =
                    new ILMZChannel(ilmzChansInit[ii]);
                this.allChannels[ii].Index = ii;
            }
        }

        /// <summary>
        /// Return an array of channels for a given ITU frequency. Sorts in decreasing
        /// Figure of Merit (FOM), and assigns priorities to channels
        /// </summary>
        /// <param name="ItuFreq_GHz">Frequency in GHz</param>
        /// <returns>Channel list, sorted in decreasing FOM</returns>
        public ILMZChannel[] GetChannelsAtItu(double ItuFreq_GHz)
        {
            List<ILMZChannel> filteredChans = new List<ILMZChannel>();

            // Find all channels with matching ITU frequency
            foreach (ILMZChannel chan in allChannels)
            {
                if (chan.IlmzInitialSettings.Dsdbr.ItuFreq_GHz == ItuFreq_GHz)
                {
                    filteredChans.Add(chan);
                }
            }

            // sort the channels by decreasing FOM
            filteredChans.Sort();
            // set priority (0 is highest)
            for (int ii = 0; ii < filteredChans.Count; ii++)
            {
                filteredChans[ii].Priority = ii;
            }
            
            ILMZChannel[] filteredChansArray = filteredChans.ToArray();
            return filteredChansArray;
        }

        public int GetChannelIndexAtItu(double ItuFreq_GHz)
        {
            
            // Find first channels index  with matching ITU frequency
            int chanIndex = 0;
            int returnIndex = -1;
            foreach (ILMZChannel chan in allChannels)
            {
                if (chan.IlmzInitialSettings.Dsdbr.ItuFreq_GHz == ItuFreq_GHz)
                {
                    //find itu channel
                    returnIndex = chanIndex;
                    break;
                }
                chanIndex++;
            }

            return returnIndex;
        }

        /// <summary>
        /// All channel options not sorted
        /// </summary>
        public ILMZChannel[] AllOptions
        {
            get
            {
                return allChannels;
            }
        }

        /// <summary>
        /// Passed Channels sorted by ITUChannelIndex
        /// </summary>
        public ILMZChannel[] Passed
        {
            get
            {
                List<ILMZChannel> passedList = new List<ILMZChannel>();
                foreach (ILMZChannel chan in allChannels)
                {
                    if ((chan.Completeness != SpecComplete.NoParams) && 
                        (chan.PassStatus == PassFail.Pass))
                    {
                        passedList.Add(chan);
                    }
                }
                passedList.Sort(new TcmzChannelComparer());
                ILMZChannel[] passedArray = passedList.ToArray();
                return passedArray;
            }
        }

        /// <summary>
        /// Mid Temperature Passed Channels sorted by ITUChannelIndex
        /// </summary>
        public ILMZChannel[] PassedMidTemp
        {
            get
            {
                List<ILMZChannel> passedList = new List<ILMZChannel>();
                foreach (ILMZChannel chan in allChannels)
                {
                    if ((chan.Completeness != SpecComplete.NoParams) &&
                        (chan.MidTempData.OverallStatus == PassFail.Pass))
                    {
                        passedList.Add(chan);
                    }
                }
                passedList.Sort(new TcmzChannelComparer());
                ILMZChannel[] passedArray = passedList.ToArray();
                return passedArray;
            }
        }

        /// <summary>
        /// Failed Channels sorted by ITUChannelIndex
        /// </summary>
        public ILMZChannel[] Failed
        {
            get
            {
                List<ILMZChannel> failedList = new List<ILMZChannel>();
                foreach (ILMZChannel chan in allChannels)
                {
                    if ((chan.Completeness != SpecComplete.NoParams) &&
                        (chan.PassStatus == PassFail.Fail))
                    {
                        failedList.Add(chan);
                    }
                }
                failedList.Sort(new TcmzChannelComparer());
                ILMZChannel[] failedArray = failedList.ToArray();
                return failedArray;
            }
        }

        /// <summary>
        /// raul added required by stream 2012.5.22
        /// All tested Channels options sorted by ITUChannelIndex
        /// </summary>
        public ILMZChannel[] AllTested
        {
            get
            {
                List<ILMZChannel> TestedList = new List<ILMZChannel>();
                foreach (ILMZChannel chan in allChannels)
                {
                    if (chan.Completeness != SpecComplete.NoParams)
                    {
                        TestedList.Add(chan);
                    }
                }
                TestedList.Sort(new TcmzChannelComparer());
                ILMZChannel[] TestedArray = TestedList.ToArray();
                return TestedArray;
            }
        }

        /// <summary>
        /// raul added required by stream 2012.5.22
        /// Last option Channels sorted by ITUChannelIndex
        /// </summary>
        public ILMZChannel[] LastOption
        {
            get
            {
                List<ILMZChannel> TestedList = new List<ILMZChannel>();
                foreach (ILMZChannel chan in AllTested)
                {
                    TestedList.Add(chan);
                }

                #region Try to store the pass options as the LastOption  - chongjian.liang 2014.11.25

                for (int i = 0; i < TestedList.Count - 1; )
                {
                    if (TestedList[i].MidTempData.GetValueSint32(EnumTosaParam.ItuChannelIndex) 
                        == TestedList[i + 1].MidTempData.GetValueSint32(EnumTosaParam.ItuChannelIndex))
                    {
                        if (TestedList[i].MidTempData.GetValueBool(EnumTosaParam.Pass_Flag)
                            || !TestedList[i + 1].MidTempData.GetValueBool(EnumTosaParam.Pass_Flag))
                        {
                            TestedList.Remove(TestedList[i + 1]);
                        }
                        else
                        {
                            TestedList.Remove(TestedList[i]);
                        }

                        continue;
                    }

                    i++;
                } 
                #endregion

                TestedList.Sort(new TcmzChannelComparer());
                ILMZChannel[] LastOptionArray = TestedList.ToArray();
                return LastOptionArray;
            }
        }

        /// <summary>
        /// Structure containing the ITU channel index of the highest and lowest power 
        /// dissipating channels
        /// </summary>
        public struct ExtremeChannelIndexes
        {
            /// <summary>
            /// Index of first channel
            /// </summary>
            public int FirstChannelIndex;
            /// <summary>
            /// Index of last channel
            /// </summary>
            public int LastChannelIndex;
            /// <summary>
            /// Index of highest laser power dissipation channel
            /// </summary>
            public int HighestLaserDissIndex;
            /// <summary>
            /// Index of lowest laser power dissipation channel
            /// </summary>
            public int LowestLaserDissIndex;
            /// <summary>
            /// Index of channel with lowest SOA current
            /// </summary>
            public int LowestIsoaIndex;
            /// <summary>
            /// Index of channel with highest SOA current
            /// </summary>
            public int HighestestIsoaIndex;
            /// <summary>
            /// Index of channel with highest lock slope efficiency
            /// </summary>
            public int HighestLockSlopeEffIndex;
            /// <summary>
            /// Index of channel with lowest lock slope efficiency , echo added for locker spec decision
            /// </summary>
            public int LowestLockSlopeEffIndex;
        }

        /// <summary>
        /// Gets the ITU channel index of the highest and lowest power 
        /// dissipating channels from the Passed channels list
        /// </summary>
        /// <returns>Structure containing indices</returns>
        public ExtremeChannelIndexes GetItuExtremeChannels()
        {
            double lasDissMax = double.NegativeInfinity;
            double lasDissMin = double.PositiveInfinity;
            double iSoaMax = double.NegativeInfinity;
            double iSoaMin = double.PositiveInfinity;
            double iLockSlopeEffMax = double.NegativeInfinity;
            double iLockSlopeEffMin = double.PositiveInfinity;
            int maxDissIdx = -1;
            int minDissIdx = -1;
            int maxIsoaIdx = -1;
            int minIsoaIdx = -1;
            int minLockSlopeIdx = -1;
            int maxLockSlopeIdx = -1;//Echo new added this items under stream's requirement, for future determine lock slope spec 2010-11-25

            foreach (ILMZChannel chan in this.PassedMidTemp)
            {
                // Ignore unpopulated value exceptions
                try
                {
                    double chanDissipation = chan.MidTempData.GetValueDouble(EnumTosaParam.VccPowerDiss_W);
                    int ituIndex = chan.MidTempData.GetValueSint32(EnumTosaParam.ItuChannelIndex);
                    if (chanDissipation > lasDissMax)
                    {
                        lasDissMax = chanDissipation;
                        maxDissIdx = ituIndex;
                    }
                    if (chanDissipation < lasDissMin)
                    {
                        lasDissMin = chanDissipation;
                        minDissIdx = ituIndex;
                    }
                }
                catch { }

                // Ignore unpopulated value exceptions
                try
                {
                    double chanIsoa = chan.MidTempData.GetValueDouble(EnumTosaParam.ISoa_mA);
                    int ituIndex = chan.MidTempData.GetValueSint32(EnumTosaParam.ItuChannelIndex);
                    if (chanIsoa > iSoaMax)
                    {
                        iSoaMax = chanIsoa;
                        maxIsoaIdx = ituIndex;
                    }
                    if (chanIsoa < iSoaMin)
                    {
                        iSoaMin = chanIsoa;
                        minIsoaIdx = ituIndex;
                    }
                }
                catch { }
                //Echo new added this block to find max locker slope efficiency and min lockslopeefficiency.
                //This is temporary use
                try
                {
                    double chanLockSlopeEffAbs = chan.MidTempData.GetValueDouble(EnumTosaParam.LockerSlopeEffAbs);
                    int ituIndex = chan.MidTempData.GetValueSint32(EnumTosaParam.ItuChannelIndex);
                    if (chanLockSlopeEffAbs > 0)//to avoid to select the CH Lockerslope no test. jack.zhang 2012-11-08
                    {
                        if (chanLockSlopeEffAbs > iLockSlopeEffMax)
                        {
                            iLockSlopeEffMax = chanLockSlopeEffAbs;
                            maxLockSlopeIdx = ituIndex;
                        }
                        if (chanLockSlopeEffAbs < iLockSlopeEffMin)
                        {
                            iLockSlopeEffMin = chanLockSlopeEffAbs;
                            minLockSlopeIdx = ituIndex;
                        }
                    }
                }
                catch { }


            }
            ExtremeChannelIndexes eci = new ExtremeChannelIndexes();
            eci.HighestLaserDissIndex = maxDissIdx;
            eci.LowestLaserDissIndex = minDissIdx;
            eci.HighestestIsoaIndex = maxIsoaIdx;
            eci.LowestIsoaIndex = minIsoaIdx;
            //Echo new added the followed two lines
            eci.HighestLockSlopeEffIndex = maxLockSlopeIdx;
            eci.LowestLockSlopeEffIndex = minLockSlopeIdx;
            if (this.PassedMidTemp.Length > 0)
            {
                eci.FirstChannelIndex = this.PassedMidTemp[0].MidTempData.GetValueSint32(EnumTosaParam.ItuChannelIndex);
                eci.LastChannelIndex = this.PassedMidTemp[this.PassedMidTemp.Length - 1].MidTempData.GetValueSint32(EnumTosaParam.ItuChannelIndex);
            }
            else
            {
                eci.FirstChannelIndex = -1;
                eci.LastChannelIndex = -1;
            }
            return eci;
        }

        public ExtremeChannelIndexes GetItuExtremeChannels(ILMZChannel[] PassedChannelData)
        {
            double lasDissMax = double.NegativeInfinity;
            double lasDissMin = double.PositiveInfinity;
            double iSoaMax = double.NegativeInfinity;
            double iSoaMin = double.PositiveInfinity;
            double iLockSlopeEffMax = double.NegativeInfinity;
            double iLockSlopeEffMin = double.PositiveInfinity;
            int maxDissIdx = -1;
            int minDissIdx = -1;
            int maxIsoaIdx = -1;
            int minIsoaIdx = -1;
            int minLockSlopeIdx = -1;
            int maxLockSlopeIdx = -1;//Echo new added this items under stream's requirement, for future determine lock slope spec 2010-11-25

            foreach (ILMZChannel chan in PassedChannelData)
            {
                // Ignore unpopulated value exceptions
                try
                {
                    double chanDissipation = chan.MidTempData.GetValueDouble(EnumTosaParam.VccPowerDiss_W);//0.7492
                    int ituIndex = chan.MidTempData.GetValueSint32(EnumTosaParam.ItuChannelIndex);
                    if (chanDissipation > lasDissMax)
                    {
                        lasDissMax = chanDissipation;
                        maxDissIdx = ituIndex;
                    }
                    if (chanDissipation < lasDissMin)
                    {
                        lasDissMin = chanDissipation;
                        minDissIdx = ituIndex;
                    }
                }
                catch { }

                // Ignore unpopulated value exceptions
                try
                {
                    double chanIsoa = chan.MidTempData.GetValueDouble(EnumTosaParam.ISoa_mA);//35.781
                    int ituIndex = chan.MidTempData.GetValueSint32(EnumTosaParam.ItuChannelIndex);
                    if (chanIsoa > iSoaMax)
                    {
                        iSoaMax = chanIsoa;
                        maxIsoaIdx = ituIndex;
                    }
                    if (chanIsoa < iSoaMin)
                    {
                        iSoaMin = chanIsoa;
                        minIsoaIdx = ituIndex;
                    }
                }
                catch { }
                //Echo new added this block to find max locker slope efficiency and min lockslopeefficiency.
                //This is temporary use
                try
                {
                    double chanLockSlopeEffAbs = chan.MidTempData.GetValueDouble(EnumTosaParam.LockerSlopeEffAbs);//1.783
                    int ituIndex = chan.MidTempData.GetValueSint32(EnumTosaParam.ItuChannelIndex);
                    if (chanLockSlopeEffAbs > iLockSlopeEffMax)
                    {
                        iLockSlopeEffMax = chanLockSlopeEffAbs;
                        maxLockSlopeIdx = ituIndex;
                    }
                    if (chanLockSlopeEffAbs < iLockSlopeEffMin)
                    {
                        iLockSlopeEffMin = chanLockSlopeEffAbs;
                        minLockSlopeIdx = ituIndex;
                    }
                }
                catch { }


            }
            ExtremeChannelIndexes eci = new ExtremeChannelIndexes();
            eci.HighestLaserDissIndex = maxDissIdx;
            eci.LowestLaserDissIndex = minDissIdx;
            eci.HighestestIsoaIndex = maxIsoaIdx;
            eci.LowestIsoaIndex = minIsoaIdx;
            //Echo new added the followed two lines
            eci.HighestLockSlopeEffIndex = maxLockSlopeIdx;
            eci.LowestLockSlopeEffIndex = minLockSlopeIdx;
            if (PassedChannelData.Length > 0)
            {
                eci.FirstChannelIndex = PassedChannelData[0].MidTempData.GetValueSint32(EnumTosaParam.ItuChannelIndex);
                eci.LastChannelIndex = PassedChannelData[PassedChannelData.Length - 1].MidTempData.GetValueSint32(EnumTosaParam.ItuChannelIndex);
            }
            else
            {
                eci.FirstChannelIndex = -1;
                eci.LastChannelIndex = -1;
            }
            return eci;
        }

        /// <summary>
        /// Get the ITU channels which should be tested against soa control range test
        /// </summary>
        /// <param name="chanSelectMode">Channel select mode</param>
        /// <param name="chanTestMode">Channel test mode</param>
        /// <returns>Array of structures containing the ITU channels with soa control range test and their testing modes</returns>
        public SoaControlRangeChan[] GetSOAControlRangeTestChannels(SoaControlRangeChanSelectMode chanSelectMode, SoaControlRangeTestMode chanTestMode)
        {
            List<SoaControlRangeChan> soaControlRangeChanList = new List<SoaControlRangeChan>();

            switch (chanSelectMode)
            {
                case SoaControlRangeChanSelectMode.HighestLowestSoa:
                    {
                        List<int> testChannelIndices = new List<int>();
                        int highestSoaChanCount = 12;
                        int lowestSoaChanCount = 0;

                        testChannelIndices.AddRange(GetChanIndicesBySoa(highestSoaChanCount, false));
                        testChannelIndices.AddRange(GetChanIndicesBySoa(lowestSoaChanCount, true));

                        foreach (int chanIndex in testChannelIndices)
                        {
                            SoaControlRangeChan chan = new SoaControlRangeChan();
                            chan.chanIndex = chanIndex;
                            chan.testMode = chanTestMode;
                            soaControlRangeChanList.Add(chan);
                        }
                    }
                    break;
                case SoaControlRangeChanSelectMode.LmInSm:
                    {
                        List<int> testChannelIndices = new List<int>();
                        testChannelIndices = GetChanIndicesByLmInSm();

                        foreach (int chanIndex in testChannelIndices)
                        {
                            SoaControlRangeChan chan = new SoaControlRangeChan();
                            chan.chanIndex = chanIndex;
                            chan.testMode = chanTestMode;
                            soaControlRangeChanList.Add(chan);
                        }
                    }
                    break;
                case SoaControlRangeChanSelectMode.AllChannels:
                    {
                        List<int> addedChanList = new List<int>();

                        foreach (ILMZChannel tcmzChan in this.allChannels)
                        {
                            int chanIndex = tcmzChan.MidTempData.GetValueSint32(EnumTosaParam.ItuChannelIndex);
                            if (!addedChanList.Contains(chanIndex))
                            {
                                addedChanList.Add(chanIndex);

                                SoaControlRangeChan chan = new SoaControlRangeChan();
                                chan.chanIndex = chanIndex;
                                chan.testMode = chanTestMode;
                                soaControlRangeChanList.Add(chan);
                            }
                        }
                    }
                    break;
                default:
                    throw new ArgumentException("Invalid soa control range channel select mode!");
            }

            SoaControlRangeChan[] rtn = soaControlRangeChanList.ToArray();
            return rtn;
        }

        #region Private Function
        /// <summary>
        /// Get indices of the first several channels in channelList
        /// </summary>
        /// <param name="channelList">channel list</param>
        /// <param name="chanCount">channel count</param>
        /// <returns>channel indexes</returns>
        private List<int> GetIndicesOfFirstChannels(List<ILMZChannel> channelList, int chanCount)
        {
            List<int> rtnIndexes = new List<int>();
            
            int i=0;
            int start = 0;
            while (i < chanCount)
            {
                try
                {
                    int chanIndex = channelList[start].MidTempData.GetValueSint32(EnumTosaParam.ItuChannelIndex);
                    if (!rtnIndexes.Contains(chanIndex))
                    {
                        rtnIndexes.Add(chanIndex);
                        i++;
                    }

                    int j = 0;
                    do
                    {
                        j = channelList[++start].MidTempData.GetValueSint32(EnumTosaParam.ItuChannelIndex);
                    } while (j == chanIndex);
                }
                catch 
                {
                    i++;
                }
            }

            return rtnIndexes;
        }

        /// <summary>
        /// Get indices of certain channels by soa current
        /// </summary>
        /// <param name="chanCount">Number of the certain channels</param>
        /// <param name="increaseOrder">Indicate getting highest soa channels or lowest soa channels</param>
        /// <returns>Indices of certain channels</returns>
        private List<int> GetChanIndicesBySoa(int chanCount, bool increaseOrder)
        {
            List<int> chanIndices = new List<int>();

            List<ILMZChannel> allTcmzChannels = new List<ILMZChannel>();
            foreach (ILMZChannel chan in this.PassedMidTemp)
            {
                allTcmzChannels.Add(chan);
            }
            allTcmzChannels.Sort(new TcmzChannelComparer(EnumTosaParam.ISoa_mA));
            if (!increaseOrder)
                allTcmzChannels.Reverse();

            for (int ii = 0; ii < chanCount && ii < allTcmzChannels.Count; ii++)
            {
                int chanIndex = allTcmzChannels[ii].MidTempData.GetValueSint32(EnumTosaParam.ItuChannelIndex);
                chanIndices.Add(chanIndex);
            }

            return chanIndices;
        }

        /// <summary>
        /// Get indices of certain channels by LmInSm
        /// </summary>
        /// <returns>Indices of certain channels</returns>
        private List<int> GetChanIndicesByLmInSm()
        {
            List<ILMZChannel> channelsOnSM0 = new List<ILMZChannel>();
            List<ILMZChannel> channelsOnSM1 = new List<ILMZChannel>();
            List<ILMZChannel> channelsOnSM2 = new List<ILMZChannel>();
            List<ILMZChannel> channelsOnSM4 = new List<ILMZChannel>();
            List<ILMZChannel> channelsOnSM5 = new List<ILMZChannel>();
            List<ILMZChannel> channelsOnSM6 = new List<ILMZChannel>();

            foreach (ILMZChannel chan in this.AllOptions)
            {
                try
                {
                    int sm = chan.MidTempData.GetValueSint32(EnumTosaParam.Supermode);
                    switch (sm)
                    {
                        case 0:
                            channelsOnSM0.Add(chan);
                            break;
                        case 1:
                            channelsOnSM1.Add(chan);
                            break;
                        case 2:
                            channelsOnSM2.Add(chan);
                            break;
                        case 4:
                            channelsOnSM4.Add(chan);
                            break;
                        case 5:
                            channelsOnSM5.Add(chan);
                            break;
                        case 6:
                            channelsOnSM6.Add(chan);
                            break;
                        default:
                            break;
                    }
                }
                catch { }
            }

            TcmzChannelComparer myComp = new TcmzChannelComparer(EnumTosaParam.ItuChannelIndex);
            List<int> testChannelIndexes = new List<int>();
            if (channelsOnSM0.Count != 0)
            {
                channelsOnSM0.Sort(myComp);
                testChannelIndexes.AddRange(GetIndicesOfFirstChannels(channelsOnSM0, 3));
            }
            else if (channelsOnSM2.Count != 0)
            {
                channelsOnSM2.Sort(myComp);
                testChannelIndexes.AddRange(GetIndicesOfFirstChannels(channelsOnSM2, 3));
            }

            if (channelsOnSM1.Count != 0)
            {
                channelsOnSM1.Sort(myComp);
                testChannelIndexes.AddRange(GetIndicesOfFirstChannels(channelsOnSM1, 3));
            }

            if (channelsOnSM6.Count != 0)
            {
                channelsOnSM6.Sort(myComp);
                channelsOnSM6.Reverse();
                testChannelIndexes.AddRange(GetIndicesOfFirstChannels(channelsOnSM6, 3));
            }
            else if (channelsOnSM4.Count != 0)
            {
                channelsOnSM4.Sort(myComp);
                channelsOnSM4.Reverse();
                testChannelIndexes.AddRange(GetIndicesOfFirstChannels(channelsOnSM4, 3));
            }

            if (channelsOnSM5.Count != 0)
            {
                channelsOnSM5.Sort(myComp);
                channelsOnSM5.Reverse();
                testChannelIndexes.AddRange(GetIndicesOfFirstChannels(channelsOnSM5, 3));
            }

            return testChannelIndexes;
        }

        #endregion


        #region Private data
        private readonly ILMZChannel[] allChannels;
        #endregion
    }


    /// <summary>
    /// User-defined comparer for TcmzChannel
    /// </summary>
    public class TcmzChannelComparer : Comparer<ILMZChannel>
    {
        // Default compare parameter
        private EnumTosaParam compParam = EnumTosaParam.ItuChannelIndex;

        /// <summary>
        /// Default constructor
        /// </summary>
        public TcmzChannelComparer()
        {
            this.compParam = EnumTosaParam.ItuChannelIndex;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="tcmzParam">Compare parameter</param>
        public TcmzChannelComparer(EnumTosaParam tcmzParam)
        {
            this.compParam = tcmzParam;
        }

        /// <summary>
        /// Performs a comparison of two objects of the same type and returns a value indicating
        /// whether one object is less than,or greater than the other
        /// </summary>
        /// <param name="x">The first object to compare</param>
        /// <param name="y">The second object to compare</param>
        /// <returns>-1 indicating x is less than y, and 1 indicating x is greater than y</returns>
        public override int Compare(ILMZChannel x, ILMZChannel y)
        {
            int retn = 0;

            try
            {
                double px, py;
                px = double.Parse(x.MidTempData.GetMeasuredData(this.compParam).ValueToString());
                py = double.Parse(y.MidTempData.GetMeasuredData(this.compParam).ValueToString());

                if (px < py)
                {
                    retn = -1;
                }
                else if (px == py)
                {
                    retn = 0;
                }
                else
                {
                    retn = 1;
                }
            }
            catch { }

            return retn;
        }
    }

    /// <summary>
    /// Method of selecting channels with soa control range test
    /// </summary>
    public enum SoaControlRangeChanSelectMode
    {
        /// <summary>
        /// Select channels with highest/lowest soa current after power levelling
        /// </summary>
        HighestLowestSoa,
        /// <summary>
        /// Select channels from specified LMs in specified SMs
        /// </summary>
        LmInSm,
        /// <summary>
        /// All channels
        /// </summary>
        AllChannels
    }

    /// <summary>
    /// Method of soa control range test at a channel
    /// </summary>
    public enum SoaControlRangeTestMode
    {
        /// <summary>
        /// Performs SOA sweep between minSoaCurrent_mA and maxSoaCurrent_mA
        /// </summary>
        Sweep,
        /// <summary>
        /// Measure at the maximum SOA current
        /// </summary>
        MeasureAtMaxSoa,
        /// <summary>
        /// Measure at maximun and minimum SOA current
        /// </summary>
        MeasureAtMaxMinSoa
    }

    /// <summary>
    /// Structure to describe a channel with soa control range test and its test mode
    /// </summary>
    public struct SoaControlRangeChan
    {
        /// <summary>
        /// The channel index
        /// </summary>
        public int chanIndex;
        /// <summary>
        /// The soa control range test mode
        /// </summary>
        public SoaControlRangeTestMode testMode;
    }
}

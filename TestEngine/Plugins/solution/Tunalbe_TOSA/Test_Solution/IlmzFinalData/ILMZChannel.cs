using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.Framework.Limits;
using Bookham.TestSolution.IlmzCommonUtils;
using Bookham.TestSolution.IlmzCommonData;

namespace Bookham.TestSolution.ILMZFinalData
{
    /// <summary>
    /// All characterisation and over temperature data for a TCMZ
    /// </summary>
    public class ILMZChannel : IComparable
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="chanInit">Channel initialiser</param>
        public ILMZChannel(IlmzChannelInit chanInit)
        {
            testStarted = false;
            // remember settings
            this.IlmzInitialSettings = chanInit;
            // initialise mid-temp data structures
            this.MidTemp = new IlmzChannelMeas();
            MidTemp.ApplyIlmzChannelInit(IlmzInitialSettings); 
            // initialise over temp data structures
            this.LowTemp = new IlmzChannelTempMeas(TcmzTestTemp.Low);
            this.HighTemp = new IlmzChannelTempMeas(TcmzTestTemp.High);
            
        }

        /// <summary>
        /// ILMZ Initial Settings
        /// </summary>
        public readonly IlmzChannelInit IlmzInitialSettings;

        /// <summary>
        /// Completeness of this channel - use to detect if anything has been tested yet
        /// </summary>
        public SpecComplete Completeness
        {
            get
            {
                // if test not declared as started, return NoParams
                if (!testStarted) return SpecComplete.NoParams;

                SpecComplete midStat = MidTemp.OverallCompleteness;
                SpecComplete lowStat = LowTemp.OverallCompleteness;
                SpecComplete highStat = HighTemp.OverallCompleteness;

                // if all the same, then return that value (NoParams, Incomplete, Complete)
                if ((midStat == lowStat) && (midStat == highStat))
                {
                    return midStat;
                }
                // otherwise by definition, things must be incomplete!
                else return SpecComplete.Incomplete;
            }
        }

        /// <summary>
        /// PassFail status for this channel
        /// </summary>
        public PassFail PassStatus
        {
            get
            {
                PassFail midStat = MidTemp.OverallStatus;
                PassFail lowStat = LowTemp.OverallStatus;
                PassFail highStat = HighTemp.OverallStatus;

                if ((midStat == PassFail.Fail) || (lowStat == PassFail.Fail)
                    || (highStat == PassFail.Fail))
                {
                    return PassFail.Fail;
                }
                else return PassFail.Pass;
            }
        }

        /// <summary>
        /// Priority of the channel
        /// </summary>
        public int Priority
        {
            get
            {
                return MidTemp.Priority;
            }
            set
            {
                MidTemp.Priority = value;
            }
        }

        /// <summary>
        /// Index
        /// </summary>
        public int Index
        {
            get { return idx; }
            set { idx = value; }
        }
        
        /// <summary>
        /// Mid temperature data
        /// </summary>
        public IlmzChannelMeas MidTempData
        {
            get { return MidTemp; }
            set { MidTemp = value; }
        }

        /// <summary>
        /// Low temperature data
        /// </summary>
        public IlmzChannelTempMeas LowTempData
        {
            get { return LowTemp; }
            set { LowTemp = value; }
        }
        
        /// <summary>
        /// High temperature data
        /// </summary>
        public IlmzChannelTempMeas HighTempData
        {
            get { return HighTemp; }
            set { HighTemp = value; }
        }

        /// <summary>
        /// Channel Fail Code
        /// </summary>
        public ChannelFailCode FailCode
        {
            get
            {
                ChannelFailCode midTempFailCode = MidTemp.FailCode;
                ChannelFailCode highTempFailCode = HighTemp.FailCode;
                ChannelFailCode lowTempFailCode = LowTemp.FailCode;

                ChannelFailCode combinedFailCode = (midTempFailCode | highTempFailCode | lowTempFailCode);
                return combinedFailCode;
            }
        }

        /// <summary>
        /// Call this function at start of channel test to declare that there is now
        /// data
        /// </summary>
        public void StartChannelTest()
        {
            testStarted = true;
        }

        /// <summary>
        /// ReTuneReason
        /// </summary>
		public Re_Tune_Reason ReTuneReason = Re_Tune_Reason.NoReTune;

        /// <summary>
        /// EOL_Testing_Info
        /// </summary>
        public string EOL_Testing_Info = string.Empty;

        /// <summary>
        /// +-3GHz offset of ITU for locker slope calculation - chongjian.liang 2013.7.9 per Stream's request
        /// </summary>
        public string ITU3GHzOffset_Info = string.Empty;

        /// <summary>
        /// The out of limit low trough power readings - Add to collect the trough power readings issue by chongjian.liang 2013.8.12
        /// </summary>
        public string TroughPower_Info = string.Empty;

        #region Private Data
        private int idx;
        private bool testStarted;
        private IlmzChannelMeas MidTemp;
        private IlmzChannelTempMeas LowTemp;
        private IlmzChannelTempMeas HighTemp;

        #endregion

        #region IComparable Members

        /// <summary>
        /// Compare function using figure of merit
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int CompareTo(object obj)
        {
            ILMZChannel ch = (ILMZChannel)obj;

            int compareResult = this.MidTempData.FigureOfMerit.CompareTo(ch.MidTempData.FigureOfMerit);
            // want sorted in reverse order by default
            return -compareResult;
        }

        #endregion
    }
}

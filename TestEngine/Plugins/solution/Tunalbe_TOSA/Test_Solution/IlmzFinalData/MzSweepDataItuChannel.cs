using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.Framework.Limits;
using Bookham.TestLibrary.Utilities;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestSolution.IlmzCommonData;

namespace Bookham.TestSolution.ILMZFinalData
{
    #region differential measurements
    /// <summary>
    /// Measurements for a MZ characterisation sweep at a single ITU channel, 
    /// for a single common mode bias (Vcm)
    /// </summary>
    public enum MzItuAtVcmMeasurements
    {
        Isoa_mA,     
		 /// <summary>Imbalance current trough mA on left arm,after shift phase</summary>
        IImbQuadL_mA,
        /// <summary>Imbalance current trough mA on right arm,after shift phase</summary>
        IImbQuadR_mA,
        /// <summary>Imbalance current peak mA on left Echoxl.wang, 2011-03-26</summary>
        IImbPeakL_mA,
        /// <summary>Imbalance current peak mA on right arm.</summary>
        IImbPeakR_mA,
        /// <summary>Ipi_mA=abs(DiffIpeak-DiffImin), Echoxl.wang 2011-03-26</summary>
        Ipi_mA,
        /// <summary>max power when do diff li sweep, This is NOT power at peak point</summary>
        Diff_Imb_Max_Power_mW,
        /// <summary>max ctap current when do diff li sweep, This is NOT Ctap at peak point</summary>
        Diff_Imb_Max_Ctap_mA,
        /// <summary>left arm voltage at peak point in single ended left arm sweep,Echoxl.wang</summary>
        SeVpeakL_V,
        /// <summary>right arm voltage when do single ended left arm LV sweep,This may be a fix value, Echoxl.wang</summary>
        SeVR_V,
        /// <summary>singled ended left arm LV sweep vpi, Echoxl.wang </summary>
        SeVpi_V,
        /// <summary>Voltage at Quadrature on left arm</summary>
        VquadL_V,
        /// <summary>Voltage at Quadrature on right arm</summary>
        VquadR_V,
        /// <summary>Voltage at Peak on left arm</summary>
        VpeakL_V,
        /// <summary>Voltage at Peak on right arm</summary>
        VpeakR_V,
        /// <summary>Voltage at Trough on left arm</summary>
        VminL_V,
        /// <summary>Voltage at Trough on right arm</summary>
        VminR_V,
        /// <summary>VPi</summary>
        Vpi_V,
        /// <summary>Extinction Ratio in dB</summary>
        ER_dB
    }

    /// <summary>
    /// Data from a Mz characterisation measured for a single channel, at a single common
    /// mode bias voltage
    /// </summary>
    public class MzItuAtVcmData : LimitCheckedDataStore<MzItuAtVcmMeasurements>
    {
        /// <summary>
        /// Method to initialise the lookup between PCAS limit names 
        /// and our parameter names
        /// </summary>
        public static void InitLookup(Specification spec)
        {
            specification = spec;
            specNameLookup = new Dictionary<MzItuAtVcmMeasurements, string>();
            // NOTE: we need to check against the "TC_*" limits, rather than the "CH_*" limits...
            //specNameLookup.Add(MzItuAtVcmMeasurements.Vpi_V, "CH_MZ_DC_VPI");
            //specNameLookup.Add(MzItuAtVcmMeasurements.IImbQuadL_mA, "CH_MZ_CTRL_L_I");
            //specNameLookup.Add(MzItuAtVcmMeasurements.IImbQuadR_mA, "CH_MZ_CTRL_R_I");
            //specNameLookup.Add(MzItuAtVcmMeasurements.VquadL_V, "CH_MZ_BIAS_L_QUAD_V");
            //specNameLookup.Add(MzItuAtVcmMeasurements.VquadR_V, "CH_MZ_BIAS_R_QUAD_V");

            #region Build local param limits list

            RawParamLimit paramLimit;
            localLimits = new List<RawParamLimit>();

            #region Comment out the useless lines - chongjian.liang 2012.12.10
            ////IImbQuadL_mA_original & IImbQuadR_mA_original
            //paramLimit = buildRawParamLimit("IImbQuadL_mA_original", "TC_MZ_IMB_LEFT_LIMIT", "mA");
            //localLimits.Add(paramLimit);
            //paramLimit = buildRawParamLimit("IImbQuadR_mA_original", "TC_MZ_IMB_RIGHT_LIMIT", "mA");
            //localLimits.Add(paramLimit); 
            #endregion

            //Vpi_V
            paramLimit = buildRawParamLimit("Vpi_V", "TC_MZ_VPI_CAL_LIMIT", "V");
            localLimits.Add(paramLimit);

            // IImbQuadL_mA & IImbQuadR_mA
            //paramLimit = buildRawParamLimit("IImbQuadL_mA", "TC_MZ_IMB_LEFT_LIMIT", "mA");
            paramLimit = buildRawParamLimit_ThermalPhase("IImbQuadL_mA", "TC_MZ_IMB_LEFT_LIMIT", "mA"); // chongjian.liang 2014.12.9
            localLimits.Add(paramLimit);
            //paramLimit = buildRawParamLimit("IImbQuadR_mA", "TC_MZ_IMB_RIGHT_LIMIT", "mA");
            paramLimit = buildRawParamLimit_ThermalPhase("IImbQuadR_mA", "TC_MZ_IMB_RIGHT_LIMIT", "mA"); // chongjian.liang 2014.12.9
            localLimits.Add(paramLimit);
            // VquadL_V & VquadR_V
            paramLimit = buildRawParamLimit("VquadL_V", "TC_MZ_QUAD_V_LIMIT", "V");
            localLimits.Add(paramLimit);
            paramLimit = buildRawParamLimit("VquadR_V", "TC_MZ_QUAD_V_LIMIT", "V");
            localLimits.Add(paramLimit);
            // VpeakL_V & VpeakR_V
            paramLimit = buildRawParamLimit("VpeakL_V", "TC_MZ_PEAK_V_LIMIT", "V");
            localLimits.Add(paramLimit);
            paramLimit = buildRawParamLimit("VpeakR_V", "TC_MZ_PEAK_V_LIMIT", "V");
            localLimits.Add(paramLimit);
            // VminL_V & VminR_V
            paramLimit = buildRawParamLimit("VminL_V", "TC_MZ_MINIMA_V_LIMIT", "V");
            localLimits.Add(paramLimit);
            paramLimit = buildRawParamLimit("VminR_V", "TC_MZ_MINIMA_V_LIMIT", "V");
            localLimits.Add(paramLimit);

            #endregion
        }

        private static Dictionary<MzItuAtVcmMeasurements, string> specNameLookup;
        private static Specification specification;
        private static List<RawParamLimit> localLimits;

        /// <summary>
        /// Used for build "TC_*" limit with both low and high limits. Don't use it in other case as it's really special! - chongjian.liang 2014.12.9
        /// </summary>
        /// <param name="paramName">The target param name</param>
        /// <param name="paramNameInSpec">The param name to get raw limits from spec</param>
        /// <param name="unit">The target param unit</param>
        /// <returns>The target parm limit</returns>
        private static RawParamLimit buildRawParamLimit_ThermalPhase(string paramName, string paramNameInSpec, string unit)
        {
            ParamLimit paramLimitMin = specification.GetParamLimit(paramNameInSpec + "_MIN");
            ParamLimit paramLimitMax = specification.GetParamLimit(paramNameInSpec + "_MAX");

            DatumDouble lowLimit;
            DatumDouble highLimit;

            if (ParamManager.Conditions.IsThermalPhase)
            {
                lowLimit = new DatumDouble(paramName, double.Parse(((DatumDouble)paramLimitMin.HighLimit).ValueToString()));
                highLimit = new DatumDouble(paramName, double.Parse(((DatumDouble)paramLimitMax.HighLimit).ValueToString()));
            }
            else
            {
                lowLimit = new DatumDouble(paramName, double.Parse(((DatumDouble)paramLimitMin.LowLimit).ValueToString()));
                highLimit = new DatumDouble(paramName, double.Parse(((DatumDouble)paramLimitMax.LowLimit).ValueToString()));
            }

            return new RawParamLimit(paramName, DatumType.Double, lowLimit, highLimit, LimitOperand.InRange, 0, paramLimitMax.AccuracyFactor, unit);
        }

        /// <summary>
        /// Used for build "TC_*" limit with both low and high limits. Don't use it in other case as it's really special!
        /// </summary>
        /// <param name="paramName">The target param name</param>
        /// <param name="paramNameInSpec">The param name to get raw limits from spec</param>
        /// <param name="unit">The target param unit</param>
        /// <returns>The target parm limit</returns>
        private static RawParamLimit buildRawParamLimit(string paramName, string paramNameInSpec, string unit)
        {
            RawParamLimit paramLimit;
            ParamLimit paramLimitMin;
            ParamLimit paramLimitMax;

            paramLimitMin = specification.GetParamLimit(paramNameInSpec + "_MIN");
            paramLimitMax = specification.GetParamLimit(paramNameInSpec + "_MAX");
            DatumDouble lowLimit = new DatumDouble(paramName, double.Parse(((DatumDouble)paramLimitMin.LowLimit).ValueToString()));
            DatumDouble highLimit = new DatumDouble(paramName, double.Parse(((DatumDouble)paramLimitMax.HighLimit).ValueToString()));

            paramLimit = new RawParamLimit(paramName, DatumType.Double,
                                        lowLimit,
                                        highLimit,
                                        LimitOperand.InRange, 0, paramLimitMax.AccuracyFactor, unit);



            return paramLimit;
        }

        /// <summary>
        /// Constructor taking the PCAS spec 
        /// </summary>
        /// <param name="vcm_V">Common mode bias voltage</param>
        public MzItuAtVcmData(double vcm_V)
        {
            // NOTE: we need to check against the "TC_*" limits, rather than the "CH_*" limits...So use local param limits list
            //base.InitLimits(specification, specNameLookup);
            base.InitLimits(specification, specNameLookup, localLimits);
            Vcm_V = vcm_V;
            ISoa_mA = 0;
        }
        /// <summary>
        /// use this function to assosiate the SOA with Coresponding Vcm
        /// </summary>
        /// <param name="vcm_V"></param>
        /// <param name="iSoa_mA"></param>
        public MzItuAtVcmData(double vcm_V, double iSoa_mA)
        {
            // NOTE: we need to check against the "TC_*" limits, rather than the "CH_*" limits...So use local param limits list
            //base.InitLimits(specification, specNameLookup);
            base.InitLimits(specification, specNameLookup, localLimits);
            Vcm_V = vcm_V;
            ISoa_mA = iSoa_mA;
        }

        /// <summary>
        /// File name for differential current sweep
        /// </summary>
        public string DiffLISweepFile;

        /// <summary>
        /// File name for differential voltage sweep
        /// </summary>
        public string DiffLVSweepFile;

        /// <summary>
        /// Common mode bias voltage used
        /// </summary>
        public readonly double Vcm_V;
        
        // Alice.Huang    2010-05-17
        // add this parameter by Jack.Zhang's requirement

        /// <summary>
        /// for ILMZ, different SOA Current cause different Vcm
        /// </summary>
        public readonly double ISoa_mA;
    }

   

    /// <summary>
    /// Data captured from MZ sweeps at a single ITU channel
    /// </summary>
    public class MzSweepDataItuChannel
    {
        /// <summary>
        /// Channel index
        /// </summary>
        public int ItuChannelIndex;
        /// <summary>
        /// Channel frequency GHz
        /// </summary>
        public double ItuFrequency_GHz;
        /// <summary>
        /// Differential Current sweep data at various Common mode bias voltages
        /// </summary>
        public List<MzItuAtVcmData> VcmData = new List<MzItuAtVcmData>();
        public double optimisedVcm_V;
        public double optimisedVpi_V;
        public bool IsRecalculatedVcmDataAvailable;
    }
    #endregion

    #region single ended measurements
    /// <summary>
    /// Single ended MZ measurements at a single ITU channel
    /// </summary>
    public enum MzItuAtChannelMeasurements
    {
        /// <summary>Left imbalance control current for minimum power</summary>
        IImbMinL_mA,
        /// <summary>Right imbalance control current for minimum power</summary>
        IImbMinR_mA,
        /// <summary>Left modulator voltage for quadrature</summary>
        VQuadL_V,
        /// <summary>Left modulator voltage for minimum power</summary>
        VMinL_V,
        /// <summary>Right modulator voltage for minimum power</summary>
        VoffsetMinR_V,
        /// <summary>VPi</summary>
        Vpi_V
    }
    /// <summary>
    /// Data captured from Single-ended MZ sweeps at a single ITU channel
    /// </summary>
    public class MzSESweepDataItuChannel
    {
        /// <summary>
        /// Channel index
        /// </summary>
        public int ItuChannelIndex;
        /// <summary>
        /// Single-ended sweep data at at a single ITU channel
        /// </summary>
        public MzItuData ItuData = new MzItuData();
    }

    /// <summary>
    /// Limit checked container for MZ measurements
    /// </summary>
    public class MzItuData : LimitCheckedDataStore<MzItuAtChannelMeasurements>
    {
        /// <summary>
        /// Method to initialise the lookup between PCAS limit names 
        /// and our parameter names
        /// </summary>
        public static void InitLookup(Specification spec)
        {
            specification = spec;
#warning Add limits for MZ charactersiation
            specNameLookup = new Dictionary<MzItuAtChannelMeasurements, string>();
            specNameLookup.Add(MzItuAtChannelMeasurements.VMinL_V, "TC_MZ_MOD_CAL_LIMIT_MAX");     // Or similar
            //specNameLookup.Add(MzItuAtChannelMeasurements.VQuadL_V, "TC_MZ_VCM_CAL_LIMIT_MIN");     // Or similar
            //specNameLookup.Add(MzItuAtVcmMeasurements.IImbQuadR_mA, "MZLIM_IMB_QUAD");
            //specNameLookup.Add(MzItuAtVcmMeasurements.Vpi_V, "MZLIM_V_PI");
            specNameLookup.Add(MzItuAtChannelMeasurements.VoffsetMinR_V, "CH_MZ_BIAS_R_QUAD_V");
        }

        private static Dictionary<MzItuAtChannelMeasurements, string> specNameLookup;

        private static Specification specification;

        /// <summary>
        /// Constructor taking the PCAS spec 
        /// </summary>
        public MzItuData()
        {
            base.InitLimits(specification, specNameLookup);
        }

        /// <summary>
        /// File name for differential current sweep
        /// </summary>
        public string DiffLISweepFile;

        /// <summary>
        /// File name for differential voltage sweep on left modulator
        /// </summary>
        public string SeLeftLVSweepFile;        
        
        /// <summary>
        /// File name for differential voltage sweep on right modulator
        /// </summary>
        public string SeRightLVSweepFile;

        /// <summary>
        /// Right arm bias to be used if imbalance correction method is required.
        /// </summary>
        public double PreferredRightArmBias_V;

    }

    #endregion
}

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.Utilities;
using System.Collections.Specialized;
using System.Collections;
using Bookham.TestSolution.IlmzCommonData;
using Bookham.TestLibrary.Instruments;
using Bookham.TestEngine.Framework.Limits;

namespace Bookham.TestSolution.ILMZFinalData
{
    /// <summary>
    /// Helper class for accessing tcmz channel data files(eg. make TSFF fle)
    /// </summary>
    public class ChannelDataFiles
    {
        /// <summary>
        /// Constructor
        /// </summary>
        private ChannelDataFiles()
        {
        }

        private const int tsffColCount = 20;
        private static ILMZChannel[] IlmzFailedData;

        /// <summary>
        /// TSFF Cal File Header
        /// </summary>
        private static readonly string[,] TSFF_DataHeaders
            = new string[2, tsffColCount]{ {"ItuFreq_GHz", "ISoa_mA", "IGain_mA", "IRear_mA", "IPhaseITU_mA",
                                  "IPhaseCal_mA", "IPhaseModeAcq_mA", "FrontSectionPair", "IFsConst_mA", "IFsNonConst_mA",
                                  "ItxLock_mA", "IrxLock_mA", "LockerSlopeEff", "IPhaseLowerEOL_mA", "IPhaseUpperEOL_mA",
                                  "MzLeftArmImb_mA", "MzRightArmImb_V", "MzLeftArmModQuad_V", "MzRightArmModQuad_V", "MzAcVpi_V"},
                                 {"Freq GHz", "SOA mA", "Gain mA", "Rear mA", "ITU Phase mA",
                                  "Cal Phase mA", "Mode Phase mA", "FS Pair", "FSn mA", "FSn+1 mA",
                                  "LPDt mA","LPDr mA","LPD Slope","PhaseEolLow","PhaseEolHi",
                                  "MZ_IMB_PIN_32","MZ_IMB_PIN_33","MZ_BIAS_PIN_34","MZ_BIAS_PIN_35","AC Vpi V"} };

        /// <summary>
        /// Make TSFF Cal File
        /// </summary>
        /// <param name="tcmzChannelData">Tcmz channel data to write</param>
        /// <param name="filename">TSFF cal filename</param>
        public static void MakeTSFFCalFile(ILMZChannel[] tcmzChannelData, string filename)
        {
            List<string> listStr_TsffFileData = new List<string>();

            //Make the file header
            string str_fileHeader = "";
            for (int ii = 0; ii < tsffColCount; ii++)
            {
                if (ii != tsffColCount - 1)
                    str_fileHeader = str_fileHeader + TSFF_DataHeaders[1, ii] + ",";
                else
                    str_fileHeader = str_fileHeader + TSFF_DataHeaders[1, ii];
            }
            listStr_TsffFileData.Add(str_fileHeader);

            //Get the file data
            int nbrChannelCount = tcmzChannelData.Length;
            for (int ii = 0; ii < nbrChannelCount; ii++)
            {
                IlmzChannelMeas midTempChanData = tcmzChannelData[ii].MidTempData;
                StringBuilder fileData = new StringBuilder();

                for (int jj = 0; jj < tsffColCount; jj++)
                {
                    string nameAsStr = TSFF_DataHeaders[0, ii];
                    EnumTosaParam parameter = (EnumTosaParam)Enum.Parse(typeof(EnumTosaParam), nameAsStr);
                    if (jj != tsffColCount - 1)
                    {
                        if (midTempChanData.IsTested(parameter))
                        {
                            fileData.Append(midTempChanData.GetMeasuredData(parameter));
                        }
                        else
                        {
                            fileData.Append("");
                        }
                        fileData.Append(",");
                    }
                    else
                    {
                        if (midTempChanData.IsTested(parameter))
                        {
                            fileData.Append(midTempChanData.GetMeasuredData(parameter));
                        }
                        else
                        {
                            fileData.Append("");
                        }
                    }
                }

                listStr_TsffFileData.Add(fileData.ToString());
            }

            //Write data to file
            using (CsvWriter csvWriter = new CsvWriter())
            {
                csvWriter.WriteFile(filename, listStr_TsffFileData);
            }
        }


        /// <summary>
        /// Make tcmz channel data file
        /// </summary>
        /// <param name="ilmzChannelData">Tcmz channel data to write</param>
        /// <param name="filename">Name of the file to make</param>
        public static void WriteIlmzmzData(ILMZChannel[] ilmzChannelData, string filename)
        {
            List<string> listStr_ilmzChannelData = new List<string>();

            int nbrChannelCount = ilmzChannelData.Length;
            StringBuilder fileHeader = new StringBuilder();
            StringBuilder paramLowLimit = new StringBuilder();
            StringBuilder paramHighLimit = new StringBuilder();

            for (int ii = 0; ii < nbrChannelCount; ii++)
            {
                //Get current ilmzChannelData
                ILMZChannel ilmzChan = ilmzChannelData[ii];

                StringBuilder fileData = new StringBuilder();
                int columnCount = 0;

                //Mid temp channel data
                IlmzChannelMeas midTempChanData = ilmzChan.MidTempData;
                foreach (string nameAsStr in Enum.GetNames(typeof(EnumTosaParam)))
                {
                    if (columnCount++ > 0)
                    {
                        if (ii == 0)
                        {
                            fileHeader.Append(",");
                            paramLowLimit.Append(",");
                            paramHighLimit.Append(",");
                        }
                        fileData.Append(",");
                    }

                    EnumTosaParam parameter = (EnumTosaParam)Enum.Parse(typeof(EnumTosaParam), nameAsStr);
                    if (ii == 0)
                    {
                        fileHeader.Append(nameAsStr);
                        paramLowLimit.Append(midTempChanData.GetMeasuredDataLowLimit(parameter).ToString());
                        paramHighLimit.Append(midTempChanData.GetMeasuredDataHighLimit(parameter).ToString());
                    }

                    if (midTempChanData.IsTested(parameter))
                    {
                        fileData.Append(midTempChanData.GetMeasuredData(parameter).ValueToString());
                    }
                    else
                    {
                        fileData.Append("");
                    }
                }


                //Low temp channel data
                IlmzChannelTempMeas lowTempChanData = ilmzChan.LowTempData;
                string temp = lowTempChanData.TempDegC.ToString() + "C";
                foreach (string nameAsStr in Enum.GetNames(typeof(TOSATempParam)))
                {
                    if (columnCount++ > 0)
                    {
                        if (ii == 0)
                        {
                            fileHeader.Append(",");
                            paramLowLimit.Append(",");
                            paramHighLimit.Append(",");
                        }
                        fileData.Append(",");
                    }

                    TOSATempParam parameter = (TOSATempParam)Enum.Parse(typeof(TOSATempParam), nameAsStr);
                    if (ii == 0)
                    {
                        fileHeader.Append(nameAsStr + "_" + temp);
                        paramLowLimit.Append(lowTempChanData.GetMeasuredDataLowLimit(parameter).ToString());
                        paramHighLimit.Append(lowTempChanData.GetMeasuredDataHighLimit(parameter).ToString());
                    }
                    
                    if (lowTempChanData.IsTested(parameter))
                    {
                        fileData.Append(lowTempChanData.GetMeasuredData(parameter).ValueToString());
                    }
                    else
                    {
                        fileData.Append("");
                    }
                }

                //High temp channel data
                IlmzChannelTempMeas highTempChanData = ilmzChan.HighTempData;
                temp = highTempChanData.TempDegC.ToString() + "C";
                foreach (string nameAsStr in Enum.GetNames(typeof(TOSATempParam)))
                {
                    if (columnCount++ > 0)
                    {
                        if (ii == 0)
                        {
                            fileHeader.Append(",");
                            paramLowLimit.Append(",");
                            paramHighLimit.Append(",");
                        }
                        fileData.Append(",");
                    }

                    TOSATempParam parameter = (TOSATempParam)Enum.Parse(typeof(TOSATempParam), nameAsStr);
                    if (ii == 0)
                    {
                        fileHeader.Append(nameAsStr + "_" + temp);
                        paramLowLimit.Append(highTempChanData.GetMeasuredDataLowLimit(parameter).ToString());
                        paramHighLimit.Append(highTempChanData.GetMeasuredDataHighLimit(parameter).ToString());
                    }
                    
                    if (highTempChanData.IsTested(parameter))
                    {
                        fileData.Append(highTempChanData.GetMeasuredData(parameter).ValueToString());
                    }
                    else
                    {
                        fileData.Append("");
                    }
                }

                // Priority and Fail Code
                if (columnCount++ > 0)
                {
                    if (ii == 0)
                        fileHeader.Append(",");
                    fileData.Append(",");
                }

                ////Add for CSR
                //if (ii == 0)
                //{
                //    fileHeader.Append("CSR");                    
                //    fileData.Append(tcmzChan.CSR);
                //}

                if (ii == 0)
                    fileHeader.Append("Priority,FailCode,Re_Tune_Reason,EOL_Testing_Info,ITU3GHzOffset_Info,TroughPower_Info");

                fileData.Append(ilmzChan.Priority.ToString());
                fileData.Append(",");
                fileData.Append(ilmzChan.FailCode.ToString().Replace(",", ";"));
                fileData.Append(";FailedParams:");
                string failedParamNames = "";
                foreach (EnumTosaParam tp in ilmzChan.MidTempData.FailedParams) { failedParamNames += tp.ToString() + ";"; }
                foreach (TOSATempParam ttp in ilmzChan.LowTempData.FailedParams) { failedParamNames += ttp.ToString() + "_" + ilmzChan.LowTempData.TempDegC.ToString() + "C;"; }
                foreach (TOSATempParam ttp in ilmzChan.HighTempData.FailedParams) { failedParamNames += ttp.ToString() + "_" + ilmzChan.HighTempData.TempDegC.ToString() + "C;"; }
                fileData.Append(failedParamNames);

				//
                fileData.Append(",");
                fileData.Append(((int)ilmzChan.ReTuneReason).ToString() + ";" + ilmzChan.ReTuneReason.ToString().Replace(",", ";"));
                fileData.Append(",");
                fileData.Append(ilmzChan.EOL_Testing_Info);
                fileData.Append(",");
                fileData.Append(ilmzChan.ITU3GHzOffset_Info);
                fileData.Append(",");
                fileData.Append(ilmzChan.TroughPower_Info);
                //
                if (ii == 0)
                {
                    listStr_ilmzChannelData.Add(fileHeader.ToString());
                    listStr_ilmzChannelData.Add(paramLowLimit.ToString());
                    listStr_ilmzChannelData.Add(paramHighLimit.ToString());
                }
                listStr_ilmzChannelData.Add(fileData.ToString());
            }

            //Write data to file
            using (CsvWriter csvWriter = new CsvWriter())
            {
                csvWriter.WriteFile(filename, listStr_ilmzChannelData);
            }
        }


        /// <summary>
        /// For the Final_OT stage
        /// </summary>
        /// <param name="ilmzChannelData"></param>
        /// <param name="filename"></param>
        /// <param name="extremeChans"></param>
        public static void WriteFinal_OT_Data(ILMZChannel[] ilmzChannelData, string filename, IlmzChannels.ExtremeChannelIndexes extremeChans, List<int> channelsIndexes_TestOT)
        {
            if (channelsIndexes_TestOT == null)
            {
                throw new Exception("No channels have been tested for over temp.");
            }

            List<string> listStr_ilmzChannelData = new List<string>();

            int nbrChannelCount = ilmzChannelData.Length;
            StringBuilder fileHeader = new StringBuilder();
            StringBuilder paramLowLimit = new StringBuilder();
            StringBuilder paramHighLimit = new StringBuilder();

            foreach (int channelIndex_TestOT in channelsIndexes_TestOT)
            {

                //Get current ilmzChannelData
                ILMZChannel ilmzChan = ilmzChannelData[channelIndex_TestOT];

                StringBuilder fileData = new StringBuilder();
                int columnCount = 0;

                //Mid temp channel data
                IlmzChannelMeas midTempChanData = ilmzChan.MidTempData;
                foreach (string nameAsStr in Enum.GetNames(typeof(EnumTosaParam)))
                {
                    if (columnCount++ > 0)
                    {
                        if (channelIndex_TestOT == 0)
                        {
                            fileHeader.Append(",");
                            paramLowLimit.Append(",");
                            paramHighLimit.Append(",");
                        }
                        fileData.Append(",");
                    }

                    EnumTosaParam parameter = (EnumTosaParam)Enum.Parse(typeof(EnumTosaParam), nameAsStr);
                    if (channelIndex_TestOT == 0)
                    {
                        fileHeader.Append(nameAsStr);
                        paramLowLimit.Append(midTempChanData.GetMeasuredDataLowLimit(parameter).ToString());
                        paramHighLimit.Append(midTempChanData.GetMeasuredDataHighLimit(parameter).ToString());
                    }

                    if (midTempChanData.IsTested(parameter))
                    {
                        fileData.Append(midTempChanData.GetMeasuredData(parameter).ValueToString());
                    }
                    else
                    {
                        fileData.Append("");
                    }
                }


                //Low temp channel data
                IlmzChannelTempMeas lowTempChanData = ilmzChan.LowTempData;
                string temp = lowTempChanData.TempDegC.ToString() + "C";
                foreach (string nameAsStr in Enum.GetNames(typeof(TOSATempParam)))
                {
                    if (columnCount++ > 0)
                    {
                        if (channelIndex_TestOT == 0)
                        {
                            fileHeader.Append(",");
                            paramLowLimit.Append(",");
                            paramHighLimit.Append(",");
                        }
                        fileData.Append(",");
                    }

                    TOSATempParam parameter = (TOSATempParam)Enum.Parse(typeof(TOSATempParam), nameAsStr);
                    if (channelIndex_TestOT == 0)
                    {
                        fileHeader.Append(nameAsStr + "_" + temp);
                        paramLowLimit.Append(lowTempChanData.GetMeasuredDataLowLimit(parameter).ToString());
                        paramHighLimit.Append(lowTempChanData.GetMeasuredDataHighLimit(parameter).ToString());
                    }

                    if (lowTempChanData.IsTested(parameter))
                    {
                        fileData.Append(lowTempChanData.GetMeasuredData(parameter).ValueToString());
                    }
                    else
                    {
                        fileData.Append("");
                    }
                }

                //High temp channel data
                IlmzChannelTempMeas highTempChanData = ilmzChan.HighTempData;
                temp = highTempChanData.TempDegC.ToString() + "C";
                foreach (string nameAsStr in Enum.GetNames(typeof(TOSATempParam)))
                {
                    if (columnCount++ > 0)
                    {
                        if (channelIndex_TestOT == 0)
                        {
                            fileHeader.Append(",");
                            paramLowLimit.Append(",");
                            paramHighLimit.Append(",");
                        }
                        fileData.Append(",");
                    }

                    TOSATempParam parameter = (TOSATempParam)Enum.Parse(typeof(TOSATempParam), nameAsStr);
                    if (channelIndex_TestOT == 0)
                    {
                        fileHeader.Append(nameAsStr + "_" + temp);
                        paramLowLimit.Append(highTempChanData.GetMeasuredDataLowLimit(parameter).ToString());
                        paramHighLimit.Append(highTempChanData.GetMeasuredDataHighLimit(parameter).ToString());
                    }

                    if (highTempChanData.IsTested(parameter))
                    {
                        fileData.Append(highTempChanData.GetMeasuredData(parameter).ValueToString());
                    }
                    else
                    {
                        fileData.Append("");
                    }
                }

                // Priority and Fail Code
                if (columnCount++ > 0)
                {
                    if (channelIndex_TestOT == 0)
                        fileHeader.Append(",");
                    fileData.Append(",");
                }

                ////Add for CSR
                //if (channelIndex_TestOT == 0)
                //{
                //    fileHeader.Append("CSR");                    
                //    fileData.Append(tcmzChan.CSR);
                //}

                if (channelIndex_TestOT == 0)
                    fileHeader.Append("Priority,FailCode,Re_Tune_Reason,EOL_Testing_Info");

                fileData.Append(ilmzChan.Priority.ToString());
                fileData.Append(",");
                fileData.Append(ilmzChan.FailCode.ToString().Replace(",", ";"));
                fileData.Append(";FailedParams:");
                string failedParamNames = "";
                foreach (EnumTosaParam tp in ilmzChan.MidTempData.FailedParams) { failedParamNames += tp.ToString() + ";"; }
                foreach (TOSATempParam ttp in ilmzChan.LowTempData.FailedParams) { failedParamNames += ttp.ToString() + "_" + ilmzChan.LowTempData.TempDegC.ToString() + "C;"; }
                foreach (TOSATempParam ttp in ilmzChan.HighTempData.FailedParams) { failedParamNames += ttp.ToString() + "_" + ilmzChan.HighTempData.TempDegC.ToString() + "C;"; }
                fileData.Append(failedParamNames);

                //
                fileData.Append(",");
                fileData.Append(((int)ilmzChan.ReTuneReason).ToString() + ";" + ilmzChan.ReTuneReason.ToString().Replace(",", ";"));
                fileData.Append(",");
                fileData.Append(ilmzChan.EOL_Testing_Info);
                //
                if (channelIndex_TestOT == 0)
                {
                    listStr_ilmzChannelData.Add(fileHeader.ToString());
                    listStr_ilmzChannelData.Add(paramLowLimit.ToString());
                    listStr_ilmzChannelData.Add(paramHighLimit.ToString());
                }
                listStr_ilmzChannelData.Add(fileData.ToString());
            }

            //Write data to file
            using (CsvWriter csvWriter = new CsvWriter())
            {
                csvWriter.WriteFile(filename, listStr_ilmzChannelData);
            }
        }

        /// <summary>
        /// Make tcmz channel data file
        /// </summary>
        /// <param name="ilmzChannelData">Tcmz channel data to write</param>
        /// <param name="filename">Name of the file to make</param>
        /// <param name="direction">Name of DoubleSlopeJuge</param>
        public static void WriteIlmzData(ILMZChannel[] ilmzChannelData, string filename,bool direction)
        {
            List<string> listStr_tcmzChannelData = new List<string>();

            int nbrChannelCount = ilmzChannelData.Length;
            StringBuilder fileHeader = new StringBuilder();
            StringBuilder paramLowLimit = new StringBuilder();
            StringBuilder paramHighLimit = new StringBuilder();

            for (int ii = 0; ii < nbrChannelCount; ii++)
            {
                //Get current tcmzChannelData
                ILMZChannel tcmzChan = ilmzChannelData[ii];

                StringBuilder fileData = new StringBuilder();
                int columnCount = 0;

                //Mid temp channel data
                IlmzChannelMeas midTempChanData = tcmzChan.MidTempData;
                foreach (string nameAsStr in Enum.GetNames(typeof(EnumTosaParam)))
                {
                    if (columnCount++ > 0)
                    {
                        if (ii == 0)
                        {
                            fileHeader.Append(",");
                            paramLowLimit.Append(",");
                            paramHighLimit.Append(",");
                        }
                        fileData.Append(",");
                    }

                    EnumTosaParam parameter = (EnumTosaParam)Enum.Parse(typeof(EnumTosaParam), nameAsStr);
                    if (ii == 0)
                    {
                        fileHeader.Append(nameAsStr);
                        paramLowLimit.Append(midTempChanData.GetMeasuredDataLowLimit(parameter).ToString());
                        paramHighLimit.Append(midTempChanData.GetMeasuredDataHighLimit(parameter).ToString());
                    }

                    if (midTempChanData.IsTested(parameter))
                    {
                        fileData.Append(midTempChanData.GetMeasuredData(parameter).ValueToString());
                    }
                    else
                    {
                        fileData.Append("");
                    }
                }
                ////add info about double slope 
                ////edited by bob.lv 2008-09-01
                //if (ii == 0)
                //{
                //    fileHeader.Append(",");
                //    fileHeader.Append("LV_SLOPE");
                //    paramLowLimit.Append(",");
                //    paramHighLimit.Append(",");
                //    paramLowLimit.Append(" ");
                //    paramHighLimit.Append(" ");
                //}

                //fileData.Append(",");
                //if (direction)
                //{
                //    fileData.Append("1");
                //}
                //else
                //{
                //    fileData.Append("-1");
                //}

                //Low temp channel data
                IlmzChannelTempMeas lowTempChanData = tcmzChan.LowTempData;
                string temp = lowTempChanData.TempDegC.ToString() + "C";
                foreach (string nameAsStr in Enum.GetNames(typeof(TOSATempParam)))
                {
                    if (columnCount++ > 0)
                    {
                        if (ii == 0)
                        {
                            fileHeader.Append(",");
                            paramLowLimit.Append(",");
                            paramHighLimit.Append(",");
                        }
                        fileData.Append(",");
                    }

                    TOSATempParam parameter = (TOSATempParam)Enum.Parse(typeof(TOSATempParam), nameAsStr);
                    if (ii == 0)
                    {
                        fileHeader.Append(nameAsStr + "_" + temp);
                        paramLowLimit.Append(lowTempChanData.GetMeasuredDataLowLimit(parameter).ToString());
                        paramHighLimit.Append(lowTempChanData.GetMeasuredDataHighLimit(parameter).ToString());
                    }

                    if (lowTempChanData.IsTested(parameter))
                    {
                        fileData.Append(lowTempChanData.GetMeasuredData(parameter).ValueToString());
                    }
                    else
                    {
                        fileData.Append("");
                    }
                }

                //High temp channel data
                IlmzChannelTempMeas highTempChanData = tcmzChan.HighTempData;
                temp = highTempChanData.TempDegC.ToString() + "C";
                foreach (string nameAsStr in Enum.GetNames(typeof(TOSATempParam)))
                {
                    if (columnCount++ > 0)
                    {
                        if (ii == 0)
                        {
                            fileHeader.Append(",");
                            paramLowLimit.Append(",");
                            paramHighLimit.Append(",");
                        }
                        fileData.Append(",");
                    }

                    TOSATempParam parameter = (TOSATempParam)Enum.Parse(typeof(TOSATempParam), nameAsStr);
                    if (ii == 0)
                    {
                        fileHeader.Append(nameAsStr + "_" + temp);
                        paramLowLimit.Append(highTempChanData.GetMeasuredDataLowLimit(parameter).ToString());
                        paramHighLimit.Append(highTempChanData.GetMeasuredDataHighLimit(parameter).ToString());
                    }

                    if (highTempChanData.IsTested(parameter))
                    {
                        fileData.Append(highTempChanData.GetMeasuredData(parameter).ValueToString());
                    }
                    else
                    {
                        fileData.Append("");
                    }
                }

                // Priority and Fail Code
                if (columnCount++ > 0)
                {
                    if (ii == 0)
                        fileHeader.Append(",");
                    fileData.Append(",");
                }

                if (ii == 0)
                    fileHeader.Append("Priority,FailCode,Re_Tune_Reason,EOL_Testing_Info");
                fileData.Append(tcmzChan.Priority.ToString());
                fileData.Append(",");
                fileData.Append(tcmzChan.FailCode.ToString().Replace(",", ";"));
                fileData.Append(";FailedParams:");
                string failedParamNames = "";
                foreach (EnumTosaParam tp in tcmzChan.MidTempData.FailedParams) { failedParamNames += tp.ToString() + ";"; }
                foreach (TOSATempParam ttp in tcmzChan.LowTempData.FailedParams) { failedParamNames += ttp.ToString() + "_" + tcmzChan.LowTempData.TempDegC.ToString() + "C;"; }
                foreach (TOSATempParam ttp in tcmzChan.HighTempData.FailedParams) { failedParamNames += ttp.ToString() + "_" + tcmzChan.HighTempData.TempDegC.ToString() + "C;"; }
                fileData.Append(failedParamNames);

                //For ReTuneReason/Eol_Testing_Info;
                fileData.Append(",");
                fileData.Append(((int)tcmzChan.ReTuneReason).ToString() + ";" + tcmzChan.ReTuneReason.ToString().Replace(",", ";"));
                fileData.Append(",");
                fileData.Append(tcmzChan.EOL_Testing_Info);
                ////End modified.
                if (ii == 0)
                {
                    listStr_tcmzChannelData.Add(fileHeader.ToString());
                    listStr_tcmzChannelData.Add(paramLowLimit.ToString());
                    listStr_tcmzChannelData.Add(paramHighLimit.ToString());
                }
                listStr_tcmzChannelData.Add(fileData.ToString());
            }

            //Write data to file
            using (CsvWriter csvWriter = new CsvWriter())
            {
                csvWriter.WriteFile(filename, listStr_tcmzChannelData);
            }
        }
        /// <summary>
        /// Writes a subset of mid temperature data to a file.
        /// Parameter names are converted, and parameters are written in the order 
        /// specified in templateFilename
        /// </summary>
        /// <param name="ilmzChannelData">The TcmzChannelData</param>
        /// <param name="templateFilename">
        /// A comma seperated list in the format 'internalName','tsFFName'.
        /// The file should contain no header, and one parameter per line</param>
        /// <param name="outputFilename">The full pathname of the file to be written.</param>
        public static void WriteTsffData(ILMZChannel[] ilmzChannelData, string templateFilename, string outputFilename)
        {
            NameValueCollection paramNameConversion = new NameValueCollection();

            // Read internal and external names from file.
            using (CsvReader csvReader = new CsvReader())
            {
                List<string[]> lines = csvReader.ReadFile(templateFilename);
                foreach (string[] line in lines)
                {
                    paramNameConversion.Add(line[0], line[1]);
                }
            }

            List<string> listStr_tcmzChannelData = new List<string>();

            int nbrChannelCount = ilmzChannelData.Length;
            StringBuilder fileHeader = new StringBuilder();

            for (int ii = 0; ii < nbrChannelCount; ii++)
            {
                //Get current tcmzChannelData
                ILMZChannel tcmzChan = ilmzChannelData[ii];

                StringBuilder fileData = new StringBuilder();
                int columnCount = 0;

                //Use mid-temp channel data
                IlmzChannelMeas midTempChanData = tcmzChan.MidTempData;
                foreach (string paramName in paramNameConversion.Keys)
                {
                    // Comma seperated
                    if (columnCount++ > 0)
                    {
                        if (ii == 0)
                            fileHeader.Append(",");
                        fileData.Append(",");
                    }

                    // Add renamed parameter to the header
                    if (ii == 0)
                        fileHeader.Append(paramNameConversion[paramName]);

                    EnumTosaParam parameter = (EnumTosaParam)Enum.Parse(typeof(EnumTosaParam), paramName);

                    // Write the value of the parameter
                    if (midTempChanData.IsTested(parameter))
                    {
                        fileData.Append(midTempChanData.GetMeasuredData(parameter).ValueToString());
                    }
                    else
                    {
                        fileData.Append("");
                    }
                }

                // Add this channel's parameters to the list
                if (ii == 0)
                    listStr_tcmzChannelData.Add(fileHeader.ToString());
                listStr_tcmzChannelData.Add(fileData.ToString());
            }

            //Write data to file
            using (CsvWriter csvWriter = new CsvWriter())
            {
                csvWriter.WriteFile(outputFilename, listStr_tcmzChannelData);
            }
        }

       
        /// <summary>
        /// Writes a subset of mid temperature data to a file.
        /// Parameter names are converted, and parameters are written in the order 
        /// specified in templateFilename
        /// </summary>
        /// <param name="ilmzChannelData">The TcmzChannelData</param>
        /// <param name="templateFilename">
        /// A comma seperated list in the format 'internalName','tsFFName'.
        /// The file should contain no header, and one parameter per line</param>
        /// <param name="outputFilename">The full pathname of the file to be written.</param>
        /// <param name="iTtaLaserNumber">ITTALASER NUMBER</param>
        /// <param name="mzTechnology">MZTECHNOLOGY</param>
        public static void WriteTTAData(ILMZChannel[] ilmzChannelData, string templateFilename, string outputFilename, string iTtaLaserNumber, string mzTechnology)
        {
            List<string> listStr_tcmzChannelData = new List<string>();
            listStr_tcmzChannelData.Add("ITTALASER NUMBER," + iTtaLaserNumber);
            listStr_tcmzChannelData.Add("MZTECHNOLOGY," + mzTechnology);
            listStr_tcmzChannelData.Add("");

            TranslateDFFparams(ilmzChannelData, templateFilename, listStr_tcmzChannelData);

            //Write data to file
            using (CsvWriter csvWriter = new CsvWriter())
            {
                csvWriter.WriteFile(outputFilename, listStr_tcmzChannelData);
            }
        }

        /// <summary>
        /// Writes a subset of mid temperature data to a file.
        /// Parameter names are converted, and parameters are written in the order 
        /// specified in templateFilename 
        /// </summary>
        /// <param name="ilmzChannelData">Tcmz channel data to write</param>
        /// <param name="templateFilename"> Template file path 
        ///  A comma seperated list in the format 'ILMZChannelParamName','TSFFaramName'.
        /// The file should contain no header, and one parameter per line
        /// </param>
        /// <param name="outputFilename">The full pathname of the file to be written.</param>
        public static void WriteTSFFData(ILMZChannel[] ilmzChannelData, string templateFilename, string outputFilename)
        {
            List<string> listStr_tcmzChannelData = new List<string>();
            TranslateDFFparams(ilmzChannelData, templateFilename, listStr_tcmzChannelData);

            //Write data to file
            using (CsvWriter csvWriter = new CsvWriter())
            {
                csvWriter.WriteFile(outputFilename, listStr_tcmzChannelData);
            }
        }
        /// <summary>
        /// /// Read params mappping information between ILMZ and Txfp form template file, 
        /// then pick out the param from ILMZ Channel test result and 
        /// store it to Txfp file according to the mapping information
        /// use this fuction to deliver ILmz channel data to Txfp test
        /// </summary>
        /// <param name="ILmzChannelData"> Ilmz channel data to be writen </param>
        /// <param name="templateFilename">Template file path 
        ///  A comma seperated list in the format 'ILMZChannelParamName','TXFParamName'.
        /// The file should contain no header, and one parameter per line</param>
        /// <param name="outputFilename">The full pathname of the file to be written.</param>
        public static void WriteTXFPData(ILMZChannel[] ILmzChannelData, string templateFilename, string outputFilename)
        {
            List<string> listStr_ilmzChannelData = new List<string>();
            TranslateDFFparams(ILmzChannelData, templateFilename, listStr_ilmzChannelData);

            //Write data to file
            using (CsvWriter csvWriter = new CsvWriter())
            {
                csvWriter.WriteFile(outputFilename, listStr_ilmzChannelData);
            }
        }

        public static void LoadMidTempFailedChannelData(ILMZChannel[] ILmzChannelData)
        {
            IlmzFailedData=ILmzChannelData;
        }


        public static IlmzChannelMeas GetFailedChanDataByItuFreq(double ItuFreq_Ghz,int sm,int lm)
        {
            IlmzChannelMeas midTempChanData=null;

            for (int ii = 0; ii < IlmzFailedData.Length; ii++) //loop through  mid temperature failed channels
            {
                //Get current tcmzChannelData
                ILMZChannel tcmzChan = IlmzFailedData[ii];

                //Use mid-temp channel data
                IlmzChannelMeas findOneChannel = tcmzChan.MidTempData;

                double ituFreq = double.Parse(findOneChannel.GetMeasuredData(EnumTosaParam.ItuFreq_GHz).ValueToString());
                int sm_no = int.Parse(findOneChannel.GetMeasuredData(EnumTosaParam.Supermode).ValueToString());
                int lm_no = int.Parse(findOneChannel.GetMeasuredData(EnumTosaParam.LongitudinalMode).ValueToString());
                if (ituFreq == ItuFreq_Ghz && sm_no==sm && lm_no==lm)
                {
                    midTempChanData = findOneChannel;
                    break;
                }
            }
            return midTempChanData;
        }
            


        /// <summary>
        /// write passed mid temperature data and initial itu operation point information to fle, this function only for hitt2 low cost solution,titan test will using it
        /// </summary>
        /// <param name="ILmzChannelData">passed mid temperature data</param>
        /// <param name="templateFilename">titan file template</param>
        /// <param name="outputFilename">titan file</param>
        /// <param name="initialItuChan">itu operation point generated after mapping characterise module </param>
        public static void WriteTXFPData(ILMZChannel[] ILmzChannelData, string templateFilename, string outputFilename,string ituOperationFileName,Inst_Fcu2Asic Fcu)
        {
            List<string> listStr_ilmzChannelData = new List<string>();
            TranslateDFFparams(ILmzChannelData, templateFilename, listStr_ilmzChannelData, ituOperationFileName,Fcu);

            //Write data to file
            using (CsvWriter csvWriter = new CsvWriter())
            {
                csvWriter.WriteFile(outputFilename, listStr_ilmzChannelData);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ilmzChannelData"></param>
        /// <param name="templateFilename"></param>
        /// <param name="outputFilename"></param>
        /// <param name="direction"></param>
        public static void WriteTSFFData(ILMZChannel[] ilmzChannelData, string templateFilename, string outputFilename,bool direction)
        {
            List<string> listStr_ilmzChannelData = new List<string>();
            TranslateDFFparams(ilmzChannelData, templateFilename, listStr_ilmzChannelData,direction);

            //Write data to file
            using (CsvWriter csvWriter = new CsvWriter())
            {
                csvWriter.WriteFile(outputFilename, listStr_ilmzChannelData);
            }
        }

        /// <summary>
        /// Add for Huawei TL6000ZCH by chongjian.liang 2013.5.3
        /// </summary>
        /// <param name="ilmzChannelData"></param>
        /// <param name="templateFilename"></param>
        /// <param name="outputFilename"></param>
        /// <param name="mainSpec"></param>
        public static void WriteTSFFData(ILMZChannel[] ilmzChannelData, string templateFilename, string outputFilename, Specification mainSpec)
        {
            List<string> listStr_ilmzChannelData = new List<string>();

            TranslateDFFparams(ilmzChannelData, templateFilename, listStr_ilmzChannelData, mainSpec);

            //Write data to file
            using (CsvWriter csvWriter = new CsvWriter())
            {
                csvWriter.WriteFile(outputFilename, listStr_ilmzChannelData);
            }
        }

        /// <summary>
        /// Read params mappping information between TCMZ and Tsff or Txfp form template file, 
        /// then pick out the param from TCMZ Channel test result and 
        /// store it to Txfp file according to the mapping information
        /// use this fuction to deliver tcmz channel data to Tsff or Txfp test
        /// </summary>
        /// <param name="ilmzChannelData"></param>
        /// <param name="templateFilename"></param>
        /// <param name="listStr_tcmzChannelData"></param>
        private static void TranslateDFFparams(ILMZChannel[] ilmzChannelData, string templateFilename, List<string> listStr_ilmzChannelData)
        {
            NameValueCollection paramNameConversion = new NameValueCollection();

            // Read internal and external names from file.
            using (CsvReader csvReader = new CsvReader())
            {
                List<string[]> lines = csvReader.ReadFile(templateFilename);
                foreach (string[] line in lines)
                {
                    paramNameConversion.Add(line[0], line[1]);
                }
            }

            int nbrChannelCount = ilmzChannelData.Length;
            StringBuilder fileHeader = new StringBuilder();

            for (int ii = 0; ii < nbrChannelCount; ii++)
            {
                //Get current tcmzChannelData
                ILMZChannel tcmzChan = ilmzChannelData[ii];

                StringBuilder fileData = new StringBuilder();
                int columnCount = 0;

                //Use mid-temp channel data
                IlmzChannelMeas midTempChanData = tcmzChan.MidTempData;
                foreach (string paramName in paramNameConversion.Keys)
                {
                    // Comma seperated
                    if (columnCount++ > 0)
                    {
                        if (ii == 0)
                            fileHeader.Append(",");
                        fileData.Append(",");
                    }

                    // Add renamed parameter to the header
                    if (ii == 0)
                        fileHeader.Append(paramNameConversion[paramName]);

                    EnumTosaParam parameter = (EnumTosaParam)Enum.Parse(typeof(EnumTosaParam), paramName);

                    // Write the value of the parameter
                    if (midTempChanData.IsTested(parameter))
                    {
                        fileData.Append(midTempChanData.GetMeasuredData(parameter).ValueToString());
                    }
                    else
                    {
                        fileData.Append("");
                    }
                }

                // Add this channel's parameters to the list
                if (ii == 0)
                    listStr_ilmzChannelData.Add(fileHeader.ToString());
                listStr_ilmzChannelData.Add(fileData.ToString());
            }
        }

        /// <summary>
        /// Read params mappping information between TCMZ and Tsff or Txfp form template file, 
        /// then pick out the param from TCMZ Channel test result and 
        /// store it to Txfp file according to the mapping information
        /// use this fuction to deliver tcmz channel data to Tsff or Txfp test
        /// </summary>
        /// <param name="ilmzChannelData"></param>
        /// <param name="templateFilename"></param>
        /// <param name="listStr_tcmzChannelData"></param>
        /// <param name="direction"> LV Sweep Slope information  </param>
        private static void TranslateDFFparams(ILMZChannel[] ilmzChannelData, string templateFilename, List<string> listStr_tcmzChannelData,bool direction)
        {
            NameValueCollection paramNameConversion = new NameValueCollection();

            // Read internal and external names from file.
            using (CsvReader csvReader = new CsvReader())
            {
                List<string[]> lines = csvReader.ReadFile(templateFilename);
                foreach (string[] line in lines)
                {
                    paramNameConversion.Add(line[0], line[1]);
                }
            }

            int nbrChannelCount = ilmzChannelData.Length;
            StringBuilder fileHeader = new StringBuilder();

            for (int ii = 0; ii < nbrChannelCount; ii++)
            {
                //Get current tcmzChannelData
                ILMZChannel tcmzChan = ilmzChannelData[ii];

                StringBuilder fileData = new StringBuilder();
                int columnCount = 0;

                //Use mid-temp channel data
                IlmzChannelMeas midTempChanData = tcmzChan.MidTempData;
                foreach (string paramName in paramNameConversion.Keys)
                {
                    // Comma seperated
                    if (columnCount++ > 0)
                    {
                        if (ii == 0)
                            fileHeader.Append(",");
                        fileData.Append(",");
                    }

                    // Add renamed parameter to the header
                    if (ii == 0)
                        fileHeader.Append(paramNameConversion[paramName]);

                    EnumTosaParam parameter = (EnumTosaParam)Enum.Parse(typeof(EnumTosaParam), paramName);

                    // Write the value of the parameter
                    if (midTempChanData.IsTested(parameter))
                    {
                        fileData.Append(midTempChanData.GetMeasuredData(parameter).ValueToString());
                    }
                    else
                    {
                        fileData.Append("");
                    }
                }
                if (ii == 0)
                {
                    fileHeader.Append(",");
                    fileHeader.Append("MZ_LV_SLOPE");
                }
                fileData.Append(",");
                if (direction)
                {
                    fileData.Append("1");
                }
                else
                {
                    fileData.Append("-1");
                }
                // Add this channel's parameters to the list
                if (ii == 0)
                    listStr_tcmzChannelData.Add(fileHeader.ToString());
                listStr_tcmzChannelData.Add(fileData.ToString());
            }
        }

        private static void TranslateDFFparams(ILMZChannel[] ilmzChannelData, string templateFilename, List<string> listStr_tcmzChannelData, string  ituFile,Inst_Fcu2Asic Fcu)
        {
            List<string[]> Itulines = null;
            using (CsvReader csvReader = new CsvReader())
            {
                Itulines = csvReader.ReadFile(ituFile);
            }
            NameValueCollection paramNameConversion = new NameValueCollection();
            NameValueCollection ItuParamNameConvert = new NameValueCollection();

            // Read internal and external names from file.
            using (CsvReader csvReader = new CsvReader())
            {
                List<string[]> lines = csvReader.ReadFile(templateFilename);
                foreach (string[] line in lines)
                {
                    paramNameConversion.Add(line[0], line[1]);
                    ItuParamNameConvert.Add(line[0], line[2]);
                }
            }

            int nbrChannelCount = ilmzChannelData.Length;
            StringBuilder fileHeader = new StringBuilder();
            
            for (int ii = 0; ii < nbrChannelCount; ii++) //loop through  mid temperature passed channels
            {
                //Get current tcmzChannelData
                ILMZChannel tcmzChan = ilmzChannelData[ii];

                
                StringBuilder fileData = new StringBuilder();
                int columnCount = 0;

                //Use mid-temp channel data
                IlmzChannelMeas midTempChanData = tcmzChan.MidTempData;
                foreach (string paramName in paramNameConversion.Keys)
                {
                    // Comma seperated
                    if (columnCount++ > 0)
                    {
                        if (ii == 0)
                            fileHeader.Append(",");
                        fileData.Append(",");
                    }

                    // Add renamed parameter to the header
                    if (ii == 0)
                        fileHeader.Append(paramName);

                    try
                    {
                        EnumTosaParam parameter = (EnumTosaParam)Enum.Parse(typeof(EnumTosaParam), paramNameConversion[paramName]);

                        // Write the value of the parameter
                        if (midTempChanData.IsTested(parameter))
                        {
                            fileData.Append(midTempChanData.GetMeasuredData(parameter).ValueToString());
                        }else
                        {
                            fileData.Append("");
                        }
                    }
                    catch
                    {
                        //fileData.Append("");
                        switch (paramName)
                        {
                            case ("ITU Channel Number"):
                                fileData.Append(tcmzChan.IlmzInitialSettings.Dsdbr.ItuChannelIndex.ToString());
                                break;
                            case ("Supermode number"):
                                fileData.Append(tcmzChan.IlmzInitialSettings.Dsdbr.Supermode.ToString());
                                break;
                            case ("Longitudinal Mode Number"):
                                fileData.Append(tcmzChan.IlmzInitialSettings.Dsdbr.LongitudinalMode.ToString());
                                break;
                            case ("Index on Middle line of Frquency"):
                                fileData.Append(tcmzChan.IlmzInitialSettings.Dsdbr.MiddleLineIndex.ToString());
                                break;
                            case ("Slope Frequency To Index"):
                                fileData.Append("0");
                                break;
                            case ("Mode Phase Dac"):
                                fileData.Append("0");
                                break;
                            default:
                                fileData.Append("");
                                break;
                        }
                    }
                }
               
                // Add this channel's parameters to the list
                if (ii == 0)
                    listStr_tcmzChannelData.Add(fileHeader.ToString());

                listStr_tcmzChannelData.Add(fileData.ToString());


                double itu_freq = double.Parse(midTempChanData.GetMeasuredData(EnumTosaParam.ItuFreq_GHz).ValueToString());


                IlmzChannelMeas failed_thisChannel_Data;

                             
                int aboveX = int.Parse(midTempChanData.GetMeasuredData(EnumTosaParam.BoundAboveX).ValueToString());
                int aboveY = int.Parse(midTempChanData.GetMeasuredData(EnumTosaParam.BoundAboveY).ValueToString());
                int belowX = int.Parse(midTempChanData.GetMeasuredData(EnumTosaParam.BoundBelowX).ValueToString());
                int belowY = int.Parse(midTempChanData.GetMeasuredData(EnumTosaParam.BoundBelowY).ValueToString());
                double Isoa_mA = 0;
                if (midTempChanData.IsTested(EnumTosaParam.ISoa_mA))
                {
                    Isoa_mA = double.Parse(midTempChanData.GetMeasuredData(EnumTosaParam.ISoa_mA).ValueToString());
                }
                double imbLeft_mA = 0;
                if (midTempChanData.IsTested(EnumTosaParam.MzLeftArmImb_mA))
                {
                    imbLeft_mA = double.Parse(midTempChanData.GetMeasuredData(EnumTosaParam.MzLeftArmImb_mA).ValueToString());
                }
                double imbRight_mA = 0;
                if (midTempChanData.IsTested(EnumTosaParam.MzRightArmImb_mA))
                {
                    imbRight_mA = double.Parse(midTempChanData.GetMeasuredData(EnumTosaParam.MzRightArmImb_mA).ValueToString());
                }
                double modLeftQuad_V = 0;
                if (midTempChanData.IsTested(EnumTosaParam.MzLeftArmModQuad_V))
                {
                    modLeftQuad_V = double.Parse(midTempChanData.GetMeasuredData(EnumTosaParam.MzLeftArmModQuad_V).ValueToString());
                }
                double modRightQuad_V = 0;
                if (midTempChanData.IsTested(EnumTosaParam.MzRightArmModQuad_V))
                {
                    modRightQuad_V = double.Parse(midTempChanData.GetMeasuredData(EnumTosaParam.MzRightArmModQuad_V).ValueToString());
                }
                double mzAcVpi = 0;
                if (midTempChanData.IsTested(EnumTosaParam.MzAcVpi_V))
                {
                    mzAcVpi = double.Parse(midTempChanData.GetMeasuredData(EnumTosaParam.MzAcVpi_V).ValueToString());
                }
                double mzDcEr = 0;
                if (midTempChanData.IsTested(EnumTosaParam.MzDcEr_dB))
                {
                    mzDcEr = double.Parse(midTempChanData.GetMeasuredData(EnumTosaParam.MzDcEr_dB).ValueToString());
                }
                int Isoa_DAC = 0;
                if (midTempChanData.IsTested(EnumTosaParam.ISoa_Dac))
                {
                    Isoa_DAC = int.Parse(midTempChanData.GetMeasuredData(EnumTosaParam.ISoa_Dac).ValueToString());
                }
                double imbRight_DAC = 0;
                if (midTempChanData.IsTested(EnumTosaParam.MzRightArmImb_Dac))
                {
                    imbRight_DAC = int.Parse(midTempChanData.GetMeasuredData(EnumTosaParam.MzRightArmImb_Dac).ValueToString());
                }
                double imbLeft_DAC = 0;
                if (midTempChanData.IsTested(EnumTosaParam.MzLeftArmImb_Dac))
                {
                    imbLeft_DAC = int.Parse(midTempChanData.GetMeasuredData(EnumTosaParam.MzLeftArmImb_Dac).ValueToString());
                }
                double phaseTuningEff = 0;

                float Igain_mA = 0;
                float Irear_mA = 0;
                float IrearSoa_mA = 0;
                float IphaseItu_mA = 0;
                float IFsCon_mA = 0;
                float IFsNonCon_mA = 0;
                //below add  backup channels of this pass channels from itu operation points file 
                
              
                for (int ituline= 1; ituline < Itulines.Count; ituline++)
                {
                    if (double.Parse(Itulines[ituline][1]) - itu_freq != 0) continue;
                    if ((int)(float.Parse(Itulines[ituline][17])) == aboveX
                            && (int)(float.Parse(Itulines[ituline][18])) == aboveY
                            && (int)(float.Parse(Itulines[ituline][19])) == belowX
                            && (int)(float.Parse(Itulines[ituline][20])) == belowY)
                    {
                        continue;//this channel is passed mid temperature channel and itu file come from szn-hitt-02
                    }
                    int sm_no = int.Parse(Itulines[ituline][2]);
                    int lm_no = int.Parse(Itulines[ituline][3]);
                    failed_thisChannel_Data = GetFailedChanDataByItuFreq(itu_freq, sm_no, lm_no);

                    if ((int)(float.Parse(Itulines[ituline][13])) == aboveX
                            && (int)(float.Parse(Itulines[ituline][14])) == aboveY
                            && (int)(float.Parse(Itulines[ituline][15])) == belowX
                            && (int)(float.Parse(Itulines[ituline][16])) == belowY)
                    {
                        continue;//this channel is passed mid temperature channel and itu file come from szn-hitt-01
                    }
                    // Echo new added below If() block to avoid adding failed instance to BlackBox Cal file. 2011-09-16
                    if (failed_thisChannel_Data != null) // This channel has backup instance which have failed mid temp test,
                    {
                        int failed_aboveX = int.Parse(failed_thisChannel_Data.GetMeasuredData(EnumTosaParam.BoundAboveX).ValueToString());
                        int failed_aboveY = int.Parse(failed_thisChannel_Data.GetMeasuredData(EnumTosaParam.BoundAboveY).ValueToString());
                        int failed_belowX = int.Parse(failed_thisChannel_Data.GetMeasuredData(EnumTosaParam.BoundBelowX).ValueToString());
                        int failed_belowY = int.Parse(failed_thisChannel_Data.GetMeasuredData(EnumTosaParam.BoundBelowY).ValueToString());

                        if ((int)(float.Parse(Itulines[ituline][17])) == failed_aboveX
                            && (int)(float.Parse(Itulines[ituline][18])) == failed_aboveY
                            && (int)(float.Parse(Itulines[ituline][19])) == failed_belowX
                            && (int)(float.Parse(Itulines[ituline][20])) == failed_belowY)
                        {
                            continue;//this channel is failed mid temperature channel and itu file come from szn-hitt-02
                        }
                        if ((int)(float.Parse(Itulines[ituline][13])) == failed_aboveX
                                && (int)(float.Parse(Itulines[ituline][14])) == failed_aboveY
                                && (int)(float.Parse(Itulines[ituline][15])) == failed_belowX
                                && (int)(float.Parse(Itulines[ituline][16])) == failed_belowY)
                        {
                            continue;//this channel is failed mid temperature channel and itu file come from szn-hitt-01
                        }

                    }

                    StringBuilder ExtraPassedItuChan = new StringBuilder();
                    foreach (string paramName in ItuParamNameConvert.Keys)
                    {
                        int ituCol_index = int.Parse(ItuParamNameConvert[paramName].ToString());
                        if (ituCol_index <= Itulines[ituline].Length-1)
                        {
                            if (ituCol_index == 8) //Isoa use mid temperature Isoa
                            {
                                ExtraPassedItuChan.Append(Isoa_mA.ToString());
                                ExtraPassedItuChan.Append(",");

                            }
                            else
                            {
                                ExtraPassedItuChan.Append(Itulines[ituline][ituCol_index]);
                                ExtraPassedItuChan.Append(",");
                            }
                            switch (ituCol_index)
                            {
                                case 7:
                                    Igain_mA = float.Parse(Itulines[ituline][ituCol_index]);
                                    break;
                                case 6:
                                    Irear_mA = float.Parse(Itulines[ituline][ituCol_index]);
                                    break;
                                case 22:
                                    IrearSoa_mA = float.Parse(Itulines[ituline][ituCol_index]);
                                    break;
                                case 26:
                                    IrearSoa_mA = float.Parse(Itulines[ituline][ituCol_index]);
                                    break;
                                case 5:
                                    IphaseItu_mA = float.Parse(Itulines[ituline][ituCol_index]);
                                    break;
                                case 10:
                                    IFsCon_mA = float.Parse(Itulines[ituline][ituCol_index]);
                                    break;
                                case 11:
                                    IFsNonCon_mA = float.Parse(Itulines[ituline][ituCol_index]);
                                    break;
                            }

                        }
                        else
                        {
                            ExtraPassedItuChan.Append(imbRight_mA.ToString() + ",");
                            ExtraPassedItuChan.Append(imbLeft_mA.ToString() + ",");
                            ExtraPassedItuChan.Append(modRightQuad_V.ToString() + ",");
                            ExtraPassedItuChan.Append(modLeftQuad_V.ToString() + ",");
                            ExtraPassedItuChan.Append(mzAcVpi.ToString() + ",");
                            ExtraPassedItuChan.Append(mzDcEr.ToString() + ",");
                            ExtraPassedItuChan.Append(Isoa_DAC.ToString() + ",");
                            ExtraPassedItuChan.Append(Fcu.mAToDacForGain_Asic(Igain_mA) + ",");
                            ExtraPassedItuChan.Append(Fcu.mAToDacForRear_Asic(Irear_mA) + ",");
                            ExtraPassedItuChan.Append(Fcu.mAToDacForRearSoa_Asic(IrearSoa_mA) + ",");
                            ExtraPassedItuChan.Append(Fcu.mAToDacForPhase_Asic(IphaseItu_mA) + ",");
                            ExtraPassedItuChan.Append("0,");
                            ExtraPassedItuChan.Append(Fcu.mAToDacForFront_Asic(IFsCon_mA) + ",");
                            ExtraPassedItuChan.Append(Fcu.mAToDacForFront_Asic(IFsNonCon_mA) + ",");
                            ExtraPassedItuChan.Append(imbRight_DAC.ToString() + ",");
                            ExtraPassedItuChan.Append(imbLeft_DAC.ToString()+",");
                            ExtraPassedItuChan.Append(phaseTuningEff.ToString());
                            break;
                        }
                    }

                    /*for (int ituCol = 0; ituCol < Itulines[ituline].Length; ituCol++)
                    {
                        if (ituCol >= 13 && ituCol <= 16) continue;
                        
                        ExtraPassedItuChan.Append(Itulines[ituline][ituCol]);
                        
                        if (ituCol != Itulines[ituline].Length)
                        {
                            ExtraPassedItuChan.Append(",");
                        }
                    }*/ // old method notused
                    listStr_tcmzChannelData.Add(ExtraPassedItuChan.ToString());
                }
                
            }
        }

        /// <summary>
        /// Read params mappping information from template file, 
        /// pick out the param from HITT Channel test result and 
        /// store it to the delivery file - chongjian.liang 2013.5.3
        /// </summary>
        /// <param name="ilmzChannelData">HITT Channel test result</param>
        /// <param name="templateFilename">Template file</param>
        /// <param name="listStr_ilmzChannelData">Delivery file data</param>
        /// <param name="mainSpec">Limit file specification</param>
        private static void TranslateDFFparams(ILMZChannel[] ilmzChannelData, string templateFilename, List<string> listStr_ilmzChannelData, Specification mainSpec)
        {
            Dictionary<string, string> paramNamePairs = new Dictionary<string, string>();

            // Read internal and external names from file.
            using (CsvReader csvReader = new CsvReader())
            {
                List<string[]> lines = csvReader.ReadFile(templateFilename);

                foreach (string[] line in lines)
                {
                    paramNamePairs.Add(line[0], line[1]);
                }
            }

            int nbrChannelCount = ilmzChannelData.Length;

            StringBuilder fileHeader = new StringBuilder();

            foreach (string paramName in paramNamePairs.Keys)
            {
                fileHeader.Append(paramName + ",");
            }

            // Add the header line
            listStr_ilmzChannelData.Add(fileHeader.ToString());

            // Add the data lines
            for (int i = 0; i < nbrChannelCount; i++)
            {
                StringBuilder fileData = new StringBuilder();

                //Use current mid-temp channel data
                IlmzChannelMeas midTempChanData = ilmzChannelData[i].MidTempData;

                int frontSectionPair = int.Parse(midTempChanData.GetMeasuredData(EnumTosaParam.FrontSectionPair).ValueToString());

                foreach (string externalName in paramNamePairs.Values)
                {
                    string paramName = "";
                    string paramValue = "";
                    EnumTosaParam parameter;
                    
                    // ":" in externalName this is its value
                    if (externalName.Contains(":"))
                    {
                        paramValue = externalName.Split(':')[1];
                    }
                    // Get the value from limit file by this key
                    else if (externalName.Contains("^"))
                    {
                        string limitKey = externalName.Split('^')[1];

                        if (mainSpec.ParamLimitExists(limitKey))
                        {
                            paramValue = mainSpec.GetParamLimit(limitKey).LowLimit.ValueToString();
                        }
                        else
                        {
                            throw new Exception(string.Format("Cannot find {0} from limit file, please contact software engineer. 无法从LIMIT FILE找到{0},请联系软件工程师.", limitKey));
                        }
                    }
                    // "/" in externalName means it has two param names
                    //else if (externalName.Contains("/"))
                    //{
                    //    // When fs pair is odd + even, the const is odd, non-const is even, use the first param name
                    //    // When fs pair is even + odd, the const is even, non-const is odd, use the second param name
                    //    bool isUseFirstParamName = (frontSectionPair % 2 == 1) ? true : false;

                    //    string[] paramNames = externalName.Split('/');

                    //    if (isUseFirstParamName)
                    //    {
                    //        paramName = paramNames[0];
                    //    }
                    //    else
                    //    {
                    //        paramName = paramNames[1];
                    //    }
                    //}
                    else // The externalName is the param name
                    {
                        paramName = externalName;
                    }

                    if (!string.IsNullOrEmpty(paramName))
                    {
                        parameter = (EnumTosaParam)Enum.Parse(typeof(EnumTosaParam), paramName);

                        if (midTempChanData.IsTested(parameter))
                        {
                            paramValue = midTempChanData.GetMeasuredData(parameter).ValueToString();
                        }
                    }

                    // Add the value of the parameter to this line
                    fileData.Append(paramValue + ",");
                }

                // Add this line
                listStr_ilmzChannelData.Add(fileData.ToString());
            }
        }
    }
}

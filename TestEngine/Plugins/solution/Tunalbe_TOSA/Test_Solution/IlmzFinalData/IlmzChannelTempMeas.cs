using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.Framework.Limits;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestLibrary.Utilities;
using Bookham.TestSolution.IlmzCommonData;
using Bookham.TestSolution.IlmzCommonUtils;

namespace Bookham.TestSolution.ILMZFinalData
{
    /// <summary>
    /// TCMZ channel temperature measurement object
    /// </summary>
    public class IlmzChannelTempMeas : LimitCheckedDataStore<TOSATempParam>
    {
        #region Static members
        static IlmzChannelTempMeas()
        {
            tempLookup = new Dictionary<TcmzTestTemp, double>();
            specNameLookupPerTemp = new Dictionary<TcmzTestTemp, Dictionary<TOSATempParam, string> >();

            localLimits = new List<RawParamLimit>();
            // set Tuning Ok flag limit
            DatumBool datumTrue = new DatumBool("True", true);
            RawParamLimit rpl;

            //rpl = new RawParamLimit("LockOk", DatumType.BoolType,
            //    datumTrue, datumTrue, LimitOperand.InRange, 0, 0, "");
            //localLimits.Add(rpl);

            rpl = new RawParamLimit("TapLevellingOk", DatumType.BoolType,
                datumTrue, datumTrue, LimitOperand.InRange, 0, 0, "");
            localLimits.Add(rpl);
        }

        /// <summary>
        /// Initialise the specification
        /// </summary>
        /// <param name="spec">Specification (e.g. from PCAS)</param>
        public static void InitSpec(Specification spec)
        {
            specification = spec;
        }

        /// <summary>
        /// Static constructor - use it to initialise the lookup between PCAS limit names 
        /// and our parameter names
        /// </summary>
        /// <param name="tempDegC">Temp in degrees C</param>
        /// <param name="testTemp">Test temperature enum value</param>
        public static void InitLookupAtTemp(TcmzTestTemp testTemp, double tempDegC)
        {
            tempLookup[testTemp] = tempDegC;
            string tempStr = testTemp.ToString().ToUpper();
            Dictionary<TOSATempParam, string> specNameLookup = new Dictionary<TOSATempParam, string>();
            // reset lookup table

            specNameLookup.Add(TOSATempParam.FreqLockedChange_GHz, "CH_DELTA_FREQ_TCASE_MID_" + tempStr);
            specNameLookup.Add(TOSATempParam.FreqUnlockedChange_GHz, "CH_DELTA_ULFREQ_TCASE_MID_" + tempStr);

            specNameLookup.Add(TOSATempParam.PwrAtPeak_OL_dBm, "CH_PWR_PK_OL_TCASE_" + tempStr);
            specNameLookup.Add(TOSATempParam.PwrAtPeakChange_OL_dB, "CH_PWR_PK_OL_DELTA_TCASE_" + tempStr);
            specNameLookup.Add(TOSATempParam.PwrAtPeak_CL_dBm, "CH_PWR_PK_CL_TCASE_" + tempStr);
            specNameLookup.Add(TOSATempParam.PwrAtPeakChange_CL_dB, "CH_PWR_PK_CL_DELTA_TCASE_" + tempStr);
            specNameLookup.Add(TOSATempParam.PwrAtQuad_OL_dBm, "CH_PWR_QD_OL_TCASE_" + tempStr);
            specNameLookup.Add(TOSATempParam.PwrAtQuadChange_OL_dB, "CH_PWR_QD_OL_DELTA_TCASE_" + tempStr);
            specNameLookup.Add(TOSATempParam.PwrAtQuad_CL_dBm, "CH_PWR_QD_CL_TCASE_" + tempStr);
            specNameLookup.Add(TOSATempParam.PwrAtQuadChange_CL_dB, "CH_PWR_QD_CL_DELTA_TCASE_" + tempStr);
            specNameLookup.Add(TOSATempParam.MzPeakModLeftChange_V, "CH_MZ_MOD_PEAK_L_V_DELTA_" + tempStr);
            specNameLookup.Add(TOSATempParam.MzPeakModRightChange_V, "CH_MZ_MOD_PEAK_R_V_DELTA_" + tempStr);

            specNameLookup.Add(TOSATempParam.TapCompPhotoCurrentPeak_OL_mA, "CH_TAP_COMP_PEAK_OL_I_" + tempStr);
            specNameLookup.Add(TOSATempParam.TapCompPhotoCurrentPeak_CL_mA, "CH_TAP_COMP_PEAK_CL_I_" + tempStr);
            specNameLookup.Add(TOSATempParam.TapCompPhotoCurrentQuad_OL_mA, "CH_TAP_COMP_QUAD_OL_I_" + tempStr);
            specNameLookup.Add(TOSATempParam.TapCompPhotoCurrentQuad_CL_mA, "CH_TAP_COMP_QUAD_CL_I_" + tempStr);
            //specNameLookup.Add(TOSATempParam.TapInlinePhotoCurrentPeak_OL_mA, "CH_TAP_INLINE_PEAK_OL_I_" + tempStr);
            //specNameLookup.Add(TOSATempParam.TapInlinePhotoCurrentPeak_CL_mA, "CH_TAP_INLINE_PEAK_CL_I_" + tempStr);
            //specNameLookup.Add(TOSATempParam.TapInlinePhotoCurrentQuad_OL_mA, "CH_TAP_INLINE_QUAD_OL_I_" + tempStr);
            //specNameLookup.Add(TOSATempParam.TapInlinePhotoCurrentQuad_CL_mA, "CH_TAP_INLINE_QUAD_CL_I_" + tempStr);

            specNameLookup.Add(TOSATempParam.IrxLock_mA, "CH_IRX_LOCK_TCASE_" + tempStr);
            specNameLookup.Add(TOSATempParam.ItxLock_mA, "CH_ITX_LOCK_TCASE_" + tempStr);
            specNameLookup.Add(TOSATempParam.IPhaseITU_mA, "CH_PHASE_I_TCASE_" + tempStr);
            specNameLookup.Add(TOSATempParam.RthDsdbr_ohm, "CH_LASER_RTH_TCASE_" + tempStr);
            specNameLookup.Add(TOSATempParam.LockOk, "CH_LOCK_FAIL_TCASE_" + tempStr);
            //specNameLookup.Add(TOSATempParam.RthMz_ohm, "CH_MZ_RTH_TCASE_" + tempStr);

            specNameLookup.Add(TOSATempParam.PwrCtrlMinPower_dBm, "CH_PWRCTRL_POWER_MIN_" + tempStr);
            specNameLookup.Add(TOSATempParam.PwrCtrlMaxPower_dBm, "CH_PWRCTRL_POWER_MAX_" + tempStr);
            specNameLookup.Add(TOSATempParam.PwrCtrlMinItx_mA, "CH_PWRCTRL_ITX_MIN_" + tempStr);
            specNameLookup.Add(TOSATempParam.PwrCtrlMaxItx_mA, "CH_PWRCTRL_ITX_MAX_" + tempStr);
            specNameLookup.Add(TOSATempParam.PwrCtrlMinIrx_mA, "CH_PWRCTRL_IRX_MIN_" + tempStr);
            specNameLookup.Add(TOSATempParam.PwrCtrlMaxIrx_mA, "CH_PWRCTRL_IRX_MAX_" + tempStr);
            specNameLookup.Add(TOSATempParam.PwrCtrlMinIsoa_mA, "CH_PWRCTRL_ISOA_MIN_" + tempStr);
            specNameLookup.Add(TOSATempParam.PwrCtrlMaxIsoa_mA, "CH_PWRCTRL_ISOA_MAX_" + tempStr);
            ////specNameLookup.Add(TOSATempParam.PwrCtrlMinInlineTap_mA, "CH_PWRCTRL_TAP_INLINE_MIN_" + tempStr);
            ////specNameLookup.Add(TOSATempParam.PwrCtrlMaxInlineTap_mA, "CH_PWRCTRL_TAP_INLINE_MAX_" + tempStr);
            specNameLookup.Add(TOSATempParam.PwrCtrlMinLaserCurrentSum_mA, "CH_PWRCTRL_LASER_I_MIN_" + tempStr);
            specNameLookup.Add(TOSATempParam.PwrCtrlMaxLaserCurrentSum_mA, "CH_PWRCTRL_LASER_I_MAX_" + tempStr);
            specNameLookup.Add(TOSATempParam.PwrCtrlMinLaserDiss_W, "CH_PWRCTRL_LASER_DISS_MIN_" + tempStr);
            specNameLookup.Add(TOSATempParam.PwrCtrlMaxLaserDiss_W, "CH_PWRCTRL_LASER_DISS_MAX_" + tempStr);
            specNameLookup.Add(TOSATempParam.LaserCurrentSum_mA, "CH_LASER_TOTAL_I_TCASE_" + tempStr);

            specNameLookup.Add(TOSATempParam.ItxLock_Change, "CH_ITX_LOCK_DELTA_TCASE_" + tempStr);
            specNameLookup.Add(TOSATempParam.IrxLock_Change, "CH_IRX_LOCK_DELTA_TCASE_" + tempStr);
            specNameLookup.Add(TOSATempParam.PwrCtrlMinItx_Change, "CH_PWRCTRL_ITX_DELTA_MIN_" + tempStr);
            specNameLookup.Add(TOSATempParam.PwrCtrlMinIrx_Change, "CH_PWRCTRL_IRX_DELTA_MIN_" + tempStr);
            specNameLookup.Add(TOSATempParam.PwrCtrlMaxItx_Change, "CH_PWRCTRL_ITX_DELTA_MAX_" + tempStr);
            specNameLookup.Add(TOSATempParam.PwrCtrlMaxIrx_Change, "CH_PWRCTRL_IRX_DELTA_MAX_" + tempStr);

            specNameLookupPerTemp[testTemp] = specNameLookup;            
        }

        /// <summary>
        /// for final_ot stage
        /// </summary>
        /// <param name="testTemp"></param>
        /// <param name="tempDegC"></param>
        /// <param name="final_ot"></param>
        public static void InitLookupAtTemp(TcmzTestTemp testTemp, double tempDegC, bool final_ot)
        {
            tempLookup[testTemp] = tempDegC;
            string tempStr = testTemp.ToString().ToUpper();
            Dictionary<TOSATempParam, string> specNameLookup = new Dictionary<TOSATempParam, string>();
            // reset lookup table

            specNameLookup.Add(TOSATempParam.FreqLockedChange_GHz, "CH_DELTA_FREQ_TCASE_MID_" + tempStr);
            specNameLookup.Add(TOSATempParam.FreqUnlockedChange_GHz, "CH_DELTA_ULFREQ_TCASE_MID_" + tempStr);

            specNameLookup.Add(TOSATempParam.PwrAtPeak_OL_dBm, "CH_PWR_PK_OL_TCASE_" + tempStr);
            specNameLookup.Add(TOSATempParam.PwrAtPeakChange_OL_dB, "CH_PWR_PK_OL_DELTA_TCASE_" + tempStr);
            //specNameLookup.Add(TOSATempParam.PwrAtPeak_CL_dBm, "CH_PWR_PK_CL_TCASE_" + tempStr);
            //specNameLookup.Add(TOSATempParam.PwrAtPeakChange_CL_dB, "CH_PWR_PK_CL_DELTA_TCASE_" + tempStr);
            specNameLookup.Add(TOSATempParam.PwrAtQuad_OL_dBm, "CH_PWR_QD_OL_TCASE_" + tempStr);
            specNameLookup.Add(TOSATempParam.PwrAtQuadChange_OL_dB, "CH_PWR_QD_OL_DELTA_TCASE_" + tempStr);
            //specNameLookup.Add(TOSATempParam.PwrAtQuad_CL_dBm, "CH_PWR_QD_CL_TCASE_" + tempStr);
            //specNameLookup.Add(TOSATempParam.PwrAtQuadChange_CL_dB, "CH_PWR_QD_CL_DELTA_TCASE_" + tempStr);
            specNameLookup.Add(TOSATempParam.MzPeakModLeftChange_V, "CH_MZ_MOD_PEAK_L_V_DELTA_" + tempStr);
            specNameLookup.Add(TOSATempParam.MzPeakModRightChange_V, "CH_MZ_MOD_PEAK_R_V_DELTA_" + tempStr);

            specNameLookup.Add(TOSATempParam.TapCompPhotoCurrentPeak_OL_mA, "CH_TAP_COMP_PEAK_OL_I_" + tempStr);
            //specNameLookup.Add(TOSATempParam.TapCompPhotoCurrentPeak_CL_mA, "CH_TAP_COMP_PEAK_CL_I_" + tempStr);
            specNameLookup.Add(TOSATempParam.TapCompPhotoCurrentQuad_OL_mA, "CH_TAP_COMP_QUAD_OL_I_" + tempStr);
            //specNameLookup.Add(TOSATempParam.TapCompPhotoCurrentQuad_CL_mA, "CH_TAP_COMP_QUAD_CL_I_" + tempStr);
            ////specNameLookup.Add(TOSATempParam.TapInlinePhotoCurrentPeak_OL_mA, "CH_TAP_INLINE_PEAK_OL_I_" + tempStr);
            ////specNameLookup.Add(TOSATempParam.TapInlinePhotoCurrentPeak_CL_mA, "CH_TAP_INLINE_PEAK_CL_I_" + tempStr);
            ////specNameLookup.Add(TOSATempParam.TapInlinePhotoCurrentQuad_OL_mA, "CH_TAP_INLINE_QUAD_OL_I_" + tempStr);
            ////specNameLookup.Add(TOSATempParam.TapInlinePhotoCurrentQuad_CL_mA, "CH_TAP_INLINE_QUAD_CL_I_" + tempStr);

            specNameLookup.Add(TOSATempParam.IrxLock_mA, "CH_IRX_LOCK_TCASE_" + tempStr);
            specNameLookup.Add(TOSATempParam.ItxLock_mA, "CH_ITX_LOCK_TCASE_" + tempStr);
            specNameLookup.Add(TOSATempParam.IPhaseITU_mA, "CH_PHASE_I_TCASE_" + tempStr);
            specNameLookup.Add(TOSATempParam.RthDsdbr_ohm, "CH_LASER_RTH_TCASE_" + tempStr);
            specNameLookup.Add(TOSATempParam.LockOk, "CH_LOCK_FAIL_TCASE_" + tempStr);
            ////specNameLookup.Add(TOSATempParam.RthMz_ohm, "CH_MZ_RTH_TCASE_" + tempStr);

            //specNameLookup.Add(TOSATempParam.PwrCtrlMinPower_dBm, "CH_PWRCTRL_POWER_MIN_" + tempStr);
            //specNameLookup.Add(TOSATempParam.PwrCtrlMaxPower_dBm, "CH_PWRCTRL_POWER_MAX_" + tempStr);
            //specNameLookup.Add(TOSATempParam.PwrCtrlMinItx_mA, "CH_PWRCTRL_ITX_MIN_" + tempStr);
            //specNameLookup.Add(TOSATempParam.PwrCtrlMaxItx_mA, "CH_PWRCTRL_ITX_MAX_" + tempStr);
            //specNameLookup.Add(TOSATempParam.PwrCtrlMinIrx_mA, "CH_PWRCTRL_IRX_MIN_" + tempStr);
            //specNameLookup.Add(TOSATempParam.PwrCtrlMaxIrx_mA, "CH_PWRCTRL_IRX_MAX_" + tempStr);
            //specNameLookup.Add(TOSATempParam.PwrCtrlMinIsoa_mA, "CH_PWRCTRL_ISOA_MIN_" + tempStr);
            //specNameLookup.Add(TOSATempParam.PwrCtrlMaxIsoa_mA, "CH_PWRCTRL_ISOA_MAX_" + tempStr);
            ////specNameLookup.Add(TOSATempParam.PwrCtrlMinInlineTap_mA, "CH_PWRCTRL_TAP_INLINE_MIN_" + tempStr);
            ////specNameLookup.Add(TOSATempParam.PwrCtrlMaxInlineTap_mA, "CH_PWRCTRL_TAP_INLINE_MAX_" + tempStr);
            //specNameLookup.Add(TOSATempParam.PwrCtrlMinLaserCurrentSum_mA, "CH_PWRCTRL_LASER_I_MIN_" + tempStr);
            //specNameLookup.Add(TOSATempParam.PwrCtrlMaxLaserCurrentSum_mA, "CH_PWRCTRL_LASER_I_MAX_" + tempStr);
            //specNameLookup.Add(TOSATempParam.PwrCtrlMinLaserDiss_W, "CH_PWRCTRL_LASER_DISS_MIN_" + tempStr);
            //specNameLookup.Add(TOSATempParam.PwrCtrlMaxLaserDiss_W, "CH_PWRCTRL_LASER_DISS_MAX_" + tempStr);
            specNameLookup.Add(TOSATempParam.LaserCurrentSum_mA, "CH_LASER_TOTAL_I_TCASE_" + tempStr);

            specNameLookup.Add(TOSATempParam.ItxLock_Change, "CH_ITX_LOCK_DELTA_TCASE_" + tempStr);
            specNameLookup.Add(TOSATempParam.IrxLock_Change, "CH_IRX_LOCK_DELTA_TCASE_" + tempStr);
            //specNameLookup.Add(TOSATempParam.PwrCtrlMinItx_Change, "CH_PWRCTRL_ITX_DELTA_MIN_" + tempStr);
            //specNameLookup.Add(TOSATempParam.PwrCtrlMinIrx_Change, "CH_PWRCTRL_IRX_DELTA_MIN_" + tempStr);
            //specNameLookup.Add(TOSATempParam.PwrCtrlMaxItx_Change, "CH_PWRCTRL_ITX_DELTA_MAX_" + tempStr);
            //specNameLookup.Add(TOSATempParam.PwrCtrlMaxIrx_Change, "CH_PWRCTRL_IRX_DELTA_MAX_" + tempStr);

            specNameLookupPerTemp[testTemp] = specNameLookup;
        }

        private static Dictionary<TcmzTestTemp, double> tempLookup;
        private static Dictionary<TcmzTestTemp, Dictionary<TOSATempParam, string> > specNameLookupPerTemp;
        private static List<RawParamLimit> localLimits;
        private static Specification specification;
        
        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="temp">Temperature this data was obtained at</param>
        public IlmzChannelTempMeas(TcmzTestTemp temp)
        {
            Dictionary<TOSATempParam, string> specNameLookup = specNameLookupPerTemp[temp];
            double tempDegC = tempLookup[temp];
            base.InitLimits(specification, specNameLookup, localLimits);
            this.TempDegC = tempDegC;            
        }

        /// <summary>
        /// Temperature in deg C
        /// </summary>
        public readonly double TempDegC;

        /// <summary>
        /// Fail Code
        /// </summary>
        public ChannelFailCode FailCode
        {
            get
            {
                ChannelFailCode failCode = ChannelFailCode.NoFail;
                TOSATempParam[] failedParams = this.FailedParams;
                foreach (TOSATempParam param in failedParams)
                {
                    switch (param)
                    {
                        case TOSATempParam.LockOk:
                            failCode |= ChannelFailCode.ItuTuning;
                            break;
                        default:
                            failCode |= ChannelFailCode.ParamOutOfSpec;
                            break;
                    }
                }
                return failCode;
            }
        }
    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestSolution.ILMZFinalData
{
    /// <summary>
    /// Enum containing a list of TCMZ parameters that can be tested during final test
    /// characterisation
    /// </summary>
    public enum EnumTosaParam
    {
        /// <summary/>
        ItuChannelIndex,
        /// <summary/>
        ItuFreq_GHz,
        /// <summary/>
        Pass_Flag,
        /// <summary/>
        Supermode,
        /// <summary/>
        LongitudinalMode,
        /// <summary/>
        MiddleLineIndex,
        /// <summary/>
        IPhaseCal_mA,
        /// <summary/>
        /// <summary/>
        IPhaseITU_mA,
        /// <summary/>
        IPhaseModeAcq_mA,
        /// <summary/>
        IRear_mA,
        /// <summary/>
        IGain_mA,
        /// <summary/>
        ISoa_mA,
        /// <summary>
        /// 
        /// </summary>
        IRearSoa_mA,
        /// <summary/>
        FrontSectionPair,
        /// <summary/>
        IFsConst_mA,
        /// <summary/>
        IFsNonConst_mA,
        ///// <summary/>
        //VPhase_V,
        ///// <summary/>
        //VRear_V,
        /// <summary/>
        //VGain_V,
        ///// <summary/>
        //VSoa_V,
        ///// <summary/>
        //VFsConst_V,
        ///// <summary/>
        //VFsNonConst_V,
        /// <summary/>
        Freq_GHz,
        /// <summary/>
        CoarseFreq_GHz,
        /// <summary/>
        OptPowerRatio,
        /// <summary/>
        BoundAboveX,
        /// <summary/>
        BoundAboveY,
        /// <summary/>
        BoundBelowX,
        /// <summary/>
        BoundBelowY,
        /// <summary/>
        BoundLeftX,
        /// <summary/>
        BoundLeftY,
        /// <summary/>
        BoundRightX,
        /// <summary/>
        BoundRightY,
        /// <summary/>
        FigureOfMerit,
        /// <summary/>
        MzLeftArmModPeak_V,
        /// <summary/>
        MzLeftArmModPeak_mA,
        /// <summary/>
        MzRightArmModPeak_V,
        /// <summary/>
        MzRightArmModPeak_mA,
        /// <summary/>
        MzLeftArmModQuad_V,
        /// <summary/>
        MzLeftArmModQuad_mA,
        /// <summary/>
        MzRightArmModQuad_V,
        /// <summary/>
        MzRightArmModQuad_mA,
        /// <summary/>
        MzLeftArmModMinima_V,
        /// <summary/>
        MzLeftArmModMinima_mA,
        /// <summary/>
        MzRightArmModMinima_V,
        /// <summary/>
        MzRightArmModMinima_mA,
        ///// <summary/>
        //MzLeftArmImb_V,
        /// <summary/>
        MzLeftArmImb_mA,
        ///// <summary/>
        //MzRightArmImb_V,
        /// <summary/>
        MzRightArmImb_mA,
        /// <summary/>
        MzDcVpi_V,
        /// <summary/>
        MzAcVpi_V,
        /// <summary/>
        MzVpiCal_V,
        /// <summary/>
        MzVcmCal_V,
        /// <summary/>
        MzDcEr_dB,
        /// <summary/>
        LowSOAMzDcEr_dB,
        /// <summary/>
        LowSOAFibrePwrPeak_dBm,
        /// <summary/>
        LowSOAFibrePwrMin_dBm,
        /// <summary/>
        FibrePwrPeak_dBm,
        /// <summary/>
        FibrePwrQuad_dBm,
        /// <summary/>
        TapCompPhotoCurrentQuad_mA,
        /// <summary/>
        TapCompPhotoCurrentPeak_mA,
        /// <summary/>
        //TapInlinePhotoCurrentQuad_mA,
        /// <summary/>
        //TapInlinePhotoCurrentPeak_mA,
        /// <summary/>
        RthDsdbr_ohm,
        /// <summary/>
        ITecDsdbr_mA,
        /// <summary/>
        VTecDsdbr_V,
        ///// <summary/>
        //RthMz_ohm,
        /// <summary/>
        //ITecMz_mA,
        ///// <summary/>
        //VTecMz_V,
        /// <summary/>
        VccTotalCurrent_mA,
        /// <summary/>
        VccPowerDiss_W,
        /// <summary/>
        PackagePowerDiss_W,
        /// <summary/>
        IPhaseLowerEOL_mA,
        /// <summary/>
        IPhaseUpperEOL_mA,
        /// <summary/>
        Freq_PTR_EOL_Pos_GHz,
        /// <summary/>
        Freq_PTR_EOL_Neg_GHz,
        /// <summary/>
        SMSR_PTR_EOL_Pos_dB,
        /// <summary/>
        SMSR_PTR_EOL_Neg_dB,
        /// <summary/>
        DeltaFreq_GHz,
        /// <summary/>
        FreqCal_GHz,
        /// <summary/>
        SMSR_dB,
        /// <summary/>
        SupermodeSR_dB,
        /// <summary/>
        ItxLock_mA,
        /// <summary/>
        IrxLock_mA,
        /// <summary/>
        LockRatio,
        /// <summary/>
        LockerSlopeEff,
        /// <summary/>
        LockerSlopeEffAbs,
        /// <summary/>
        PhaseTuningEff,
        /// <summary/>
        PhaseRatioSlopeEff,
        /// <summary/>
        FibrePwrISoaMin_dBm,
        /// <summary/>
        FibrePwrISoaMax_dBm,
        /// <summary/>
        //VSoaShutter_V,
        /// <summary/>
        ISoaShutter_mA,
        /// <summary/>
        //SoaPwrShuttered_mW,
        /// <summary/>
        FibrePwrShuttered_dBm,
        /// <summary/>
        SoaShutteredAtten_dB,
        ///// <summary/>
        //ItxShuttered_mA,
        ///// <summary/>
        //IrxShuttered_mA,
        /// <summary/>
        PtrSlopeMetric,
        /// <summary/>
        TuningOk,
        /// <summary/>
        PowerLevellingOk,
        /// <summary/>
        MZ_LV_Slope,
        /// <summary/>
        CUM_POWER_LEFT,
        /// <summary/>
        CUM_POWER_RIGHT,
        /// <summary/>
        MZ_CSR_POWER_RATIO,
        /// <summary/>
        MZ_CSR_IMB_C,

        // Alice.Huang 2010-02-09 
        //  Add parameter below to record additional information for TOSA's data deliver to BB
        /// <summary/>
        IPhaseCal_Dac,
        /// <summary/>        
        IPhaseITU_Dac,
        /// <summary/>
        IPhaseModeAcq_Dac,
        /// <summary/>
        IPhaseLowerEOL_Dac,
        /// <summary/>
        IPhaseUpperEOL_Dac,
        /// <summary/>
        IRear_Dac,
        /// <summary/>
        IGain_Dac,
        /// <summary/>
        ISoa_Dac,
        /// <summary>
        /// 
        /// </summary>
        IRearSoa_Dac,
        /// <summary/>
        IFsConst_Dac,
        /// <summary/>
        IFsNonConst_Dac,
        /// <summary/>
        MzLeftArmImb_Dac,
        /// <summary/>
        MzRightArmImb_Dac,
        /// <summary/>
        LowIphaseForLockerSlope_mA,
        /// <summary/>
        LowFreqForLockerSlope_GHz,
        /// <summary/>
        HighIphaseForLockerSlope_mA,
        /// <summary/>
        HighFreqForLockerSlope_GHz,
        /// <summary/>
        LMPhaseScanRipple_GHz
    }
    
    /// <summary>
    /// Enum containing a list of TCMZ parameters that can be tested during 
    /// over temperature test
    /// </summary>
    public enum TOSATempParam
    {
        /// <summary/>
        FreqUnlocked_GHz,
        /// <summary/>
        FreqUnlockedChange_GHz,
        /// <summary/>
        FreqLocked_GHz,
        /// <summary/>
        FreqLockedChange_GHz,       
        /// <summary/>
        LockOk,
        /// <summary/>
        LockerRatio,

        /// <summary/>
        IPhaseITU_mA,
        /// <summary/>
        ItxLock_mA,
        /// <summary/>
        IrxLock_mA,
        /// <summary/>
        RthDsdbr_ohm,
        /// <summary/>
        //RthMz_ohm,
       
        /// <summary/>        
        MzPeakModLeft_V,
        /// <summary/>        
        MzPeakModRight_V,
        /// <summary/>        
        MzPeakModLeftChange_V,
        /// <summary/>        
        MzPeakModRightChange_V,
        
        /// <summary/>
        PwrAtPeak_OL_dBm,
        /// <summary/>
        PwrAtPeakChange_OL_dB,
        /// <summary/>
        PwrAtPeak_CL_dBm,
        /// <summary/>
        PwrAtPeakChange_CL_dB,

        /// <summary/>
        PwrAtQuad_OL_dBm,
        /// <summary/>
        PwrAtQuadChange_OL_dB,
        /// <summary/>
        PwrAtQuad_CL_dBm,
        /// <summary/>
        PwrAtQuadChange_CL_dB,

        /// <summary/>        
        TapCompPhotoCurrentPeak_OL_mA,
        /// <summary/>        
        TapCompPhotoCurrentPeak_CL_mA,
        /// <summary/>        
        TapCompPhotoCurrentQuad_OL_mA,
        /// <summary/>        
        TapCompPhotoCurrentQuad_CL_mA,
        /////// <summary/>        
        ////TapInlinePhotoCurrentPeak_OL_mA,
        /////// <summary/>        
        ////TapInlinePhotoCurrentPeak_CL_mA,
        /////// <summary/>        
        ////TapInlinePhotoCurrentQuad_OL_mA,
        /////// <summary/>        
        ////TapInlinePhotoCurrentQuad_CL_mA,

        /// <summary/>        
        TapLevellingOk,
        /// <summary/>
        ISoaTapLevel_mA,

        /// <summary/>
        TecDsdbrCurrent_A,
        /// <summary/>
        TecDsdbrVoltage_V,
        ///// <summary/>
        //TecMzCurrent_A,
        ///// <summary/>
        //TecMzVoltage_V,
        /// <summary/>
        LaserCurrentSum_mA,
        /// <summary/>
        LaserPowerDiss_W,
        /// <summary/>
        PackagePowerDiss_W,

        /// <summary/>
        PwrCtrlMinPower_dBm,
        /// <summary/>
        PwrCtrlMaxPower_dBm,
        /// <summary/>
        PwrCtrlMinItx_mA,
        /// <summary/>
        PwrCtrlMaxItx_mA,
        /// <summary/>
        PwrCtrlMinIrx_mA,
        /// <summary/>
        PwrCtrlMaxIrx_mA,
        /// <summary/>
        PwrCtrlMinIsoa_mA,
        /// <summary/>
        PwrCtrlMaxIsoa_mA,
        /// <summary/>
        //PwrCtrlMinInlineTap_mA,
        /// <summary/>
        //PwrCtrlMaxInlineTap_mA,
        /// <summary/>
        PwrCtrlMinLaserCurrentSum_mA,
        /// <summary/>
        PwrCtrlMaxLaserCurrentSum_mA,
        /// <summary/>
        PwrCtrlMinLaserDiss_W,
        /// <summary/>
        PwrCtrlMaxLaserDiss_W,

        /// <summary/>
        ItxLock_Change,
        /// <summary/>
        IrxLock_Change,
        /// <summary/>
        PwrCtrlMinItx_Change,
        /// <summary/>
        PwrCtrlMinIrx_Change,
        /// <summary/>
        PwrCtrlMaxItx_Change,
        /// <summary/>
        PwrCtrlMaxIrx_Change,

        /// <summary/>
        IPhaseITU_Dac,
        /// <summary/>
        ISoaTapLevel_Dac,
        /// <summary/>
        PwrCtrlMinIsoa_Dac,
        /// <summary/>
        PwrCtrlMaxIsoa_Dac,
    }
}

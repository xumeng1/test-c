using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.Framework.Limits;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestSolution.IlmzCommonData;
using Bookham.TestLibrary.Utilities;
using Bookham.TestSolution.IlmzCommonUtils;

namespace Bookham.TestSolution.ILMZFinalData
{
    /// <summary>
    /// TCMZ channel measurement object
    /// </summary>
    public class IlmzChannelMeas : LimitCheckedDataStore<EnumTosaParam>
    {
        #region Private members
        /// <summary>
        /// Indicated whether this channel has finished measure.
        /// </summary>
        bool finishedMeasure = false;
        private static Specification specification;
        private static List<RawParamLimit> localLimits;
        private static Dictionary<EnumTosaParam, string> specNameLookup;
        /// <summary> Figure of merit </summary>
        private double figureOfMerit;
        /// <summary> Indicated whether figure of merit has been set </summary>
        private bool hasSetFOM = false;
        #endregion

        #region Public members and properties.
        /// <summary>
        /// Priority field
        /// </summary>
        public int Priority;

        /// <summary>
        /// Indicate whether this channel have finished measuring.
        /// </summary>
        public bool Finished
        {
            get
            {
                return this.finishedMeasure;
            }
        }
        #endregion

        #region Public methods
        /// <summary>
        /// Declare this channel as finished.
        /// </summary>
        public void SetFinished()
        {
            this.finishedMeasure = true;
        }
        #endregion

        #region Static members
        /// <summary>
        /// Static initialiser method - use it to initialise the lookup between PCAS limit names 
        /// and our parameter names
        /// </summary>
        public static void InitLookup(Specification spec)
        {
            specification = spec;
            // clear any previous lookup data
            specNameLookup = new Dictionary<EnumTosaParam, string>();

            specNameLookup.Add(EnumTosaParam.IPhaseCal_mA, "CH_CAL_PHASE_I");
            specNameLookup.Add(EnumTosaParam.FibrePwrQuad_dBm, "CH_DC_QUAD_POWER");
            specNameLookup.Add(EnumTosaParam.IFsConst_mA, "CH_FSN_I");
            //specNameLookup.Add(EnumTosaParam.VFsConst_V, "CH_FSN_V");
            specNameLookup.Add(EnumTosaParam.IFsNonConst_mA, "CH_FSNPLUSONE_I");
            //specNameLookup.Add(EnumTosaParam.VFsNonConst_V, "CH_FSNPLUSONE_V");
            specNameLookup.Add(EnumTosaParam.IGain_mA, "CH_GAIN_I");
            //specNameLookup.Add(EnumTosaParam.VGain_V, "CH_GAIN_V");
            specNameLookup.Add(EnumTosaParam.IrxLock_mA, "CH_IRX_LOCK");
            //specNameLookup.Add(EnumTosaParam.IrxShuttered_mA, "CH_IRX_SHUTTERED");
            specNameLookup.Add(EnumTosaParam.IPhaseITU_mA, "CH_ITU_PHASE_I");
            specNameLookup.Add(EnumTosaParam.ISoa_mA, "CH_ITU_SOA_I");
            specNameLookup.Add(EnumTosaParam.ItxLock_mA, "CH_ITX_LOCK");
            //specNameLookup.Add(EnumTosaParam.ItxShuttered_mA, "CH_ITX_SHUTTERED");
            specNameLookup.Add(EnumTosaParam.RthDsdbr_ohm, "CH_LASER_RTH");
            specNameLookup.Add(EnumTosaParam.LockRatio, "CH_LOCK_RATIO");
            specNameLookup.Add(EnumTosaParam.IPhaseModeAcq_mA, "CH_MODE_PHASE_I");
            specNameLookup.Add(EnumTosaParam.MzAcVpi_V, "CH_MZ_AC_VPI");
            specNameLookup.Add(EnumTosaParam.MzLeftArmModPeak_V, "CH_MZ_MOD_PEAK_L_V");
            specNameLookup.Add(EnumTosaParam.MzRightArmModPeak_V, "CH_MZ_MOD_PEAK_R_V");
            specNameLookup.Add(EnumTosaParam.MzLeftArmModQuad_V, "CH_MZ_BIAS_L_QUAD_V");
            specNameLookup.Add(EnumTosaParam.MzRightArmModQuad_V, "CH_MZ_BIAS_R_QUAD_V");
            specNameLookup.Add(EnumTosaParam.MzLeftArmModMinima_V, "CH_MZ_MOD_MIN_L_V");
            specNameLookup.Add(EnumTosaParam.MzRightArmModMinima_V, "CH_MZ_MOD_MIN_R_V");
            specNameLookup.Add(EnumTosaParam.MzLeftArmImb_mA, "CH_MZ_CTRL_L_I");
            specNameLookup.Add(EnumTosaParam.MzRightArmImb_mA, "CH_MZ_CTRL_R_I");
            specNameLookup.Add(EnumTosaParam.MzDcVpi_V, "CH_MZ_DC_VPI");
            specNameLookup.Add(EnumTosaParam.MzDcEr_dB, "CH_MZ_ER");
            //specNameLookup.Add(EnumTosaParam.RthMz_ohm, "CH_MZ_RTH");
            specNameLookup.Add(EnumTosaParam.LockerSlopeEff, "CH_NORM_SLOPE");
            specNameLookup.Add(EnumTosaParam.LockerSlopeEffAbs, "CH_NORM_SLOPE_ABS");
            specNameLookup.Add(EnumTosaParam.FibrePwrPeak_dBm, "CH_PEAK_POWER");
            specNameLookup.Add(EnumTosaParam.FibrePwrISoaMax_dBm, "CH_PEAK_POWER_MAX_SOA");
            specNameLookup.Add(EnumTosaParam.FibrePwrISoaMin_dBm, "CH_PEAK_POWER_MIN_SOA");
            specNameLookup.Add(EnumTosaParam.FibrePwrShuttered_dBm, "CH_PEAK_POWER_SHUTTERED");
            specNameLookup.Add(EnumTosaParam.IPhaseLowerEOL_mA, "CH_PHASE_I_LOWER_EOL");
            specNameLookup.Add(EnumTosaParam.IPhaseUpperEOL_mA, "CH_PHASE_I_UPPER_EOL");
            //specNameLookup.Add(EnumTosaParam.VPhase_V, "CH_PHASE_V");
            specNameLookup.Add(EnumTosaParam.PhaseTuningEff, "CH_PTE");
            specNameLookup.Add(EnumTosaParam.PhaseRatioSlopeEff, "CH_PHASE_RATIO_SLOPE_EFF");
            specNameLookup.Add(EnumTosaParam.TapCompPhotoCurrentPeak_mA, "CH_TAP_COMP_PEAK_I");
            specNameLookup.Add(EnumTosaParam.TapCompPhotoCurrentQuad_mA, "CH_TAP_COMP_QUAD_I");
            ////specNameLookup.Add(EnumTosaParam.TapInlinePhotoCurrentPeak_mA, "CH_TAP_INLINE_PEAK_I");
            ////specNameLookup.Add(EnumTosaParam.TapInlinePhotoCurrentQuad_mA, "CH_TAP_INLINE_QUAD_I");
            specNameLookup.Add(EnumTosaParam.IRear_mA, "CH_REAR_I");
            specNameLookup.Add(EnumTosaParam.IRearSoa_mA, "CH_REARSOA_I");
            //specNameLookup.Add(EnumTosaParam.VRear_V, "CH_REAR_V");
            //specNameLookup.Add(EnumTosaParam.VSoaShutter_V, "CH_SHUTTER_SOA_V");
            specNameLookup.Add(EnumTosaParam.SoaShutteredAtten_dB, "CH_SHUTTERED_ATTEN");
            specNameLookup.Add(EnumTosaParam.SMSR_dB, "CH_SMSR");
            specNameLookup.Add(EnumTosaParam.SMSR_PTR_EOL_Pos_dB, "CH_SMSR_AT_PTR_MAX_EOL");
            specNameLookup.Add(EnumTosaParam.SMSR_PTR_EOL_Neg_dB, "CH_SMSR_AT_PTR_MIN_EOL");
            //specNameLookup.Add(EnumTosaParam.VSoa_V, "CH_SOA_V");
            specNameLookup.Add(EnumTosaParam.SupermodeSR_dB, "CH_SUPERMODE_SR");
            specNameLookup.Add(EnumTosaParam.VccTotalCurrent_mA, "CH_LASER_TOTAL_I");
            specNameLookup.Add(EnumTosaParam.PtrSlopeMetric, "CH_PTR_SLOPE_METRIC");

            ////Add CH_MZ_LV_SLOPE for 'ZDD'
            if (spec.ParamLimitExists("CH_MZ_LV_SLOPE"))
            {
                specNameLookup.Add(EnumTosaParam.MZ_LV_Slope, "CH_MZ_LV_SLOPE");
            }
            if (spec.ParamLimitExists("CH_CUM_POWER_LEFT"))
            {
                specNameLookup.Add(EnumTosaParam.CUM_POWER_LEFT, "CH_CUM_POWER_LEFT");
            }
            if (spec.ParamLimitExists("CH_CUM_POWER_RIGHT"))
            {
                specNameLookup.Add(EnumTosaParam.CUM_POWER_RIGHT, "CH_CUM_POWER_RIGHT");
            }

            if (spec.ParamLimitExists("CH_MZ_CSR_POWER_RATIO"))
            {
                specNameLookup.Add(EnumTosaParam.MZ_CSR_POWER_RATIO, "CH_MZ_CSR_POWER_RATIO");
            }
            if (spec.ParamLimitExists("CH_MZ_CSR_IMB_C"))
            {
                specNameLookup.Add(EnumTosaParam.MZ_CSR_IMB_C, "CH_MZ_CSR_IMB_C");
            }

            if (spec.ParamLimitExists("CH_LM_PHASE_SCAN_RIPPLE"))
            {
                specNameLookup.Add(EnumTosaParam.LMPhaseScanRipple_GHz, "CH_LM_PHASE_SCAN_RIPPLE");
            }
            // End add param;

            // initialise local limits
            localLimits = new List<RawParamLimit>();
            // set Tuning Ok flag limit
            DatumBool datumTrue = new DatumBool("True", true);

            RawParamLimit rpl;
            rpl = new RawParamLimit("Pass_Flag", DatumType.BoolType,
                datumTrue, datumTrue, LimitOperand.InRange, 0, 0, "");
            localLimits.Add(rpl);

            rpl = new RawParamLimit("TuningOk", DatumType.BoolType,
                datumTrue, datumTrue, LimitOperand.InRange, 0, 0, "");
            localLimits.Add(rpl);

            // set Power levelling Ok flag limit
            rpl = new RawParamLimit("PowerLevellingOk", DatumType.BoolType,
                            datumTrue, datumTrue, LimitOperand.InRange, 0, 0, "");
            localLimits.Add(rpl);

            // Front section pair
            rpl = new RawParamLimit("FrontSectionPair", DatumType.Sint32);
            localLimits.Add(rpl);

            // ItuChannelIndex
            rpl = new RawParamLimit("ItuChannelIndex", DatumType.Sint32);
            localLimits.Add(rpl);

            // Supermode
            rpl = new RawParamLimit("Supermode", DatumType.Sint32);
            localLimits.Add(rpl);

            // LongitudinalMode
            rpl = new RawParamLimit("LongitudinalMode", DatumType.Sint32);
            localLimits.Add(rpl);

            //START MODIFICATION: Sep.04.2007, by Ken.Wu
            //  Read this limit item from limits file/server.
            // set phase slope metric limit
            //#warning - for debug only. Needs to be in limit file as a CH_PTR_SLOPE_METRIC or similar
            //            rpl = new RawParamLimit("PtrSlopeMetric", DatumType.Double,
            //                            new DatumDouble("PtrSlopeMetric", 0.85),
            //                            new DatumDouble("PtrSlopeMetric", 1.1),
            //                            LimitOperand.InRange, 0, 0, "");
            //            localLimits.Add(rpl);
            // END  MODIFICATION: Sep.04.2007, by Ken.Wu
        }
        #endregion

        #region for finl_ot
        /// <summary>
        /// for final_ot stage
        /// </summary>
        /// <param name="spec"></param>
        /// <param name="final_ot"></param>
        public static void InitLookup(Specification spec, bool final_ot)
        {
            specification = spec;
            // clear any previous lookup data
            specNameLookup = new Dictionary<EnumTosaParam, string>();

            specNameLookup.Add(EnumTosaParam.IPhaseCal_mA, "CH_CAL_PHASE_I");
            specNameLookup.Add(EnumTosaParam.FibrePwrQuad_dBm, "CH_DC_QUAD_POWER");
            specNameLookup.Add(EnumTosaParam.IFsConst_mA, "CH_FSN_I");
            //specNameLookup.Add(EnumTosaParam.VFsConst_V, "CH_FSN_V");
            specNameLookup.Add(EnumTosaParam.IFsNonConst_mA, "CH_FSNPLUSONE_I");
            //specNameLookup.Add(EnumTosaParam.VFsNonConst_V, "CH_FSNPLUSONE_V");
            specNameLookup.Add(EnumTosaParam.IGain_mA, "CH_GAIN_I");
            //specNameLookup.Add(EnumTosaParam.VGain_V, "CH_GAIN_V");
            specNameLookup.Add(EnumTosaParam.IrxLock_mA, "CH_IRX_LOCK");
            //specNameLookup.Add(EnumTosaParam.IrxShuttered_mA, "CH_IRX_SHUTTERED");
            specNameLookup.Add(EnumTosaParam.IPhaseITU_mA, "CH_ITU_PHASE_I");
            specNameLookup.Add(EnumTosaParam.ISoa_mA, "CH_ITU_SOA_I");
            specNameLookup.Add(EnumTosaParam.ItxLock_mA, "CH_ITX_LOCK");
            //specNameLookup.Add(EnumTosaParam.ItxShuttered_mA, "CH_ITX_SHUTTERED");
            specNameLookup.Add(EnumTosaParam.RthDsdbr_ohm, "CH_LASER_RTH");
            specNameLookup.Add(EnumTosaParam.LockRatio, "CH_LOCK_RATIO");
            //specNameLookup.Add(EnumTosaParam.IPhaseModeAcq_mA, "CH_MODE_PHASE_I");
            //specNameLookup.Add(EnumTosaParam.MzAcVpi_V, "CH_MZ_AC_VPI");
            specNameLookup.Add(EnumTosaParam.MzLeftArmModPeak_V, "CH_MZ_MOD_PEAK_L_V");
            specNameLookup.Add(EnumTosaParam.MzRightArmModPeak_V, "CH_MZ_MOD_PEAK_R_V");
            specNameLookup.Add(EnumTosaParam.MzLeftArmModQuad_V, "CH_MZ_BIAS_L_QUAD_V");
            specNameLookup.Add(EnumTosaParam.MzRightArmModQuad_V, "CH_MZ_BIAS_R_QUAD_V");
            specNameLookup.Add(EnumTosaParam.MzLeftArmModMinima_V, "CH_MZ_MOD_MIN_L_V");
            specNameLookup.Add(EnumTosaParam.MzRightArmModMinima_V, "CH_MZ_MOD_MIN_R_V");
            specNameLookup.Add(EnumTosaParam.MzLeftArmImb_mA, "CH_MZ_CTRL_L_I");
            specNameLookup.Add(EnumTosaParam.MzRightArmImb_mA, "CH_MZ_CTRL_R_I");
            specNameLookup.Add(EnumTosaParam.MzDcVpi_V, "CH_MZ_DC_VPI");
            specNameLookup.Add(EnumTosaParam.MzDcEr_dB, "CH_MZ_ER");
            ////specNameLookup.Add(EnumTosaParam.RthMz_ohm, "CH_MZ_RTH");
            //specNameLookup.Add(EnumTosaParam.LockerSlopeEff, "CH_NORM_SLOPE");
            //specNameLookup.Add(EnumTosaParam.LockerSlopeEffAbs, "CH_NORM_SLOPE_ABS");
            specNameLookup.Add(EnumTosaParam.FibrePwrPeak_dBm, "CH_PEAK_POWER");
            //specNameLookup.Add(EnumTosaParam.FibrePwrISoaMax_dBm, "CH_PEAK_POWER_MAX_SOA");
            //specNameLookup.Add(EnumTosaParam.FibrePwrISoaMin_dBm, "CH_PEAK_POWER_MIN_SOA");
            //specNameLookup.Add(EnumTosaParam.FibrePwrShuttered_dBm, "CH_PEAK_POWER_SHUTTERED");
            //specNameLookup.Add(EnumTosaParam.IPhaseLowerEOL_mA, "CH_PHASE_I_LOWER_EOL");
            //specNameLookup.Add(EnumTosaParam.IPhaseUpperEOL_mA, "CH_PHASE_I_UPPER_EOL");
            ////specNameLookup.Add(EnumTosaParam.VPhase_V, "CH_PHASE_V");
            //specNameLookup.Add(EnumTosaParam.PhaseTuningEff, "CH_PTE");
            //specNameLookup.Add(EnumTosaParam.PhaseRatioSlopeEff, "CH_PHASE_RATIO_SLOPE_EFF");
            specNameLookup.Add(EnumTosaParam.TapCompPhotoCurrentPeak_mA, "CH_TAP_COMP_PEAK_I");
            specNameLookup.Add(EnumTosaParam.TapCompPhotoCurrentQuad_mA, "CH_TAP_COMP_QUAD_I");
            ////specNameLookup.Add(EnumTosaParam.TapInlinePhotoCurrentPeak_mA, "CH_TAP_INLINE_PEAK_I");
            ////specNameLookup.Add(EnumTosaParam.TapInlinePhotoCurrentQuad_mA, "CH_TAP_INLINE_QUAD_I");
            specNameLookup.Add(EnumTosaParam.IRear_mA, "CH_REAR_I");
            specNameLookup.Add(EnumTosaParam.IRearSoa_mA, "CH_REARSOA_I");
            ////specNameLookup.Add(EnumTosaParam.VRear_V, "CH_REAR_V");
            ////specNameLookup.Add(EnumTosaParam.VSoaShutter_V, "CH_SHUTTER_SOA_V");
            specNameLookup.Add(EnumTosaParam.SoaShutteredAtten_dB, "CH_SHUTTERED_ATTEN");

            // Del it by tim for the final_ot check the file
            //specNameLookup.Add(EnumTosaParam.SMSR_dB, "CH_SMSR");
            //specNameLookup.Add(EnumTosaParam.SMSR_PTR_EOL_Pos_dB, "CH_SMSR_AT_PTR_MAX_EOL");
            //specNameLookup.Add(EnumTosaParam.SMSR_PTR_EOL_Neg_dB, "CH_SMSR_AT_PTR_MIN_EOL");
            //specNameLookup.Add(EnumTosaParam.VSoa_V, "CH_SOA_V");
            //specNameLookup.Add(EnumTosaParam.SupermodeSR_dB, "CH_SUPERMODE_SR");
            specNameLookup.Add(EnumTosaParam.VccTotalCurrent_mA, "CH_LASER_TOTAL_I");
            //specNameLookup.Add(EnumTosaParam.PtrSlopeMetric, "CH_PTR_SLOPE_METRIC");

            ////Add CH_MZ_LV_SLOPE for 'ZDD'
            if (spec.ParamLimitExists("CH_MZ_LV_SLOPE"))
            {
                specNameLookup.Add(EnumTosaParam.MZ_LV_Slope, "CH_MZ_LV_SLOPE");
            }
            if (spec.ParamLimitExists("CH_CUM_POWER_LEFT"))
            {
                specNameLookup.Add(EnumTosaParam.CUM_POWER_LEFT, "CH_CUM_POWER_LEFT");
            }
            if (spec.ParamLimitExists("CH_CUM_POWER_RIGHT"))
            {
                specNameLookup.Add(EnumTosaParam.CUM_POWER_RIGHT, "CH_CUM_POWER_RIGHT");
            }

            if (spec.ParamLimitExists("CH_MZ_CSR_POWER_RATIO"))
            {
                specNameLookup.Add(EnumTosaParam.MZ_CSR_POWER_RATIO, "CH_MZ_CSR_POWER_RATIO");
            }
            if (spec.ParamLimitExists("CH_MZ_CSR_IMB_C"))
            {
                specNameLookup.Add(EnumTosaParam.MZ_CSR_IMB_C, "CH_MZ_CSR_IMB_C");
            }
            // End add param;

            // initialise local limits
            localLimits = new List<RawParamLimit>();
            // set Tuning Ok flag limit
            DatumBool datumTrue = new DatumBool("True", true);

            RawParamLimit rpl;
            rpl = new RawParamLimit("Pass_Flag", DatumType.BoolType,
                datumTrue, datumTrue, LimitOperand.InRange, 0, 0, "");
            localLimits.Add(rpl);

            rpl = new RawParamLimit("TuningOk", DatumType.BoolType,
                datumTrue, datumTrue, LimitOperand.InRange, 0, 0, "");
            localLimits.Add(rpl);

            // set Power levelling Ok flag limit
            rpl = new RawParamLimit("PowerLevellingOk", DatumType.BoolType,
                            datumTrue, datumTrue, LimitOperand.InRange, 0, 0, "");
            localLimits.Add(rpl);

            // Front section pair
            rpl = new RawParamLimit("FrontSectionPair", DatumType.Sint32);
            localLimits.Add(rpl);

            // ItuChannelIndex
            rpl = new RawParamLimit("ItuChannelIndex", DatumType.Sint32);
            localLimits.Add(rpl);

            // Supermode
            rpl = new RawParamLimit("Supermode", DatumType.Sint32);
            localLimits.Add(rpl);

            // LongitudinalMode
            rpl = new RawParamLimit("LongitudinalMode", DatumType.Sint32);
            localLimits.Add(rpl);
        }
        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        public IlmzChannelMeas()
        {
            base.InitLimits(specification, specNameLookup, localLimits);
        }

        /// <summary>
        /// Apply ILMZ Channel initialisation data
        /// </summary>
        /// <param name="chanInit">TcmzChannelInit object containing initialisation data 
        /// obtained by CloseGrid and MZ characterisation</param>
        public void ApplyIlmzChannelInit(IlmzChannelInit chanInit)
        {
            //record data from initialisation
            DsdbrChannelData dsdbrData = chanInit.Dsdbr;
            // laser section currents
            DsdbrChannelSetup dsdbrSetup = dsdbrData.Setup;
            SetValueDouble(EnumTosaParam.IPhaseCal_mA, dsdbrSetup.IPhase_mA);
            SetValueDouble(EnumTosaParam.IRear_mA, dsdbrSetup.IRear_mA);
            SetValueDouble(EnumTosaParam.IGain_mA, dsdbrSetup.IGain_mA);
            SetValueDouble(EnumTosaParam.ISoa_mA, dsdbrSetup.ISoa_mA);
            SetValueSint32(EnumTosaParam.FrontSectionPair, dsdbrSetup.FrontPair);
            SetValueDouble(EnumTosaParam.IFsConst_mA, dsdbrSetup.IFsFirst_mA);
            SetValueDouble(EnumTosaParam.IFsNonConst_mA, dsdbrSetup.IFsSecond_mA);
            // other DSDBR data from CloseGrid
            SetValueSint32(EnumTosaParam.ItuChannelIndex, dsdbrData.ItuChannelIndex);
            SetValueDouble(EnumTosaParam.ItuFreq_GHz, dsdbrData.ItuFreq_GHz);
            SetValueSint32(EnumTosaParam.Supermode, dsdbrData.Supermode);
            SetValueSint32(EnumTosaParam.LongitudinalMode, dsdbrData.LongitudinalMode);
            SetValueDouble(EnumTosaParam.MiddleLineIndex, dsdbrData.MiddleLineIndex);
            SetValueDouble(EnumTosaParam.Freq_GHz, dsdbrData.Freq_GHz);
            SetValueDouble(EnumTosaParam.CoarseFreq_GHz, dsdbrData.CoarseFreq_GHz);
            SetValueDouble(EnumTosaParam.OptPowerRatio, dsdbrData.OptPowerRatio);
            SetValueDouble(EnumTosaParam.BoundAboveX, dsdbrData.BoundAboveX);
            SetValueDouble(EnumTosaParam.BoundAboveY, dsdbrData.BoundAboveY);
            SetValueDouble(EnumTosaParam.BoundBelowX, dsdbrData.BoundBelowX);
            SetValueDouble(EnumTosaParam.BoundBelowY, dsdbrData.BoundBelowY);
            SetValueDouble(EnumTosaParam.BoundLeftX, dsdbrData.BoundLeftX);
            SetValueDouble(EnumTosaParam.BoundLeftY, dsdbrData.BoundLeftY);
            SetValueDouble(EnumTosaParam.BoundRightX, dsdbrData.BoundRightX);
            SetValueDouble(EnumTosaParam.BoundRightY, dsdbrData.BoundRightY);
            // alice.Huang    2010-04-16
            // add this parameter for TOSA
            SetValueDouble(EnumTosaParam.IRearSoa_mA, dsdbrSetup.IRearSoa_mA);

            // MZ data
            MzData mzData = chanInit.Mz;
            SetValueDouble(EnumTosaParam.MzLeftArmModPeak_V, mzData.LeftArmMod_Peak_V);
            SetValueDouble(EnumTosaParam.MzRightArmModPeak_V, mzData.RightArmMod_Peak_V);
            SetValueDouble(EnumTosaParam.MzLeftArmModQuad_V, mzData.LeftArmMod_Quad_V);
            SetValueDouble(EnumTosaParam.MzRightArmModQuad_V, mzData.RightArmMod_Quad_V);
            SetValueDouble(EnumTosaParam.MzLeftArmModMinima_V, mzData.LeftArmMod_Min_V);
            SetValueDouble(EnumTosaParam.MzRightArmModMinima_V, mzData.RightArmMod_Min_V);
            SetValueDouble(EnumTosaParam.MzLeftArmImb_mA, mzData.LeftArmImb_mA);
            SetValueDouble(EnumTosaParam.MzRightArmImb_mA, mzData.RightArmImb_mA);
            SetValueDouble(EnumTosaParam.MzLeftArmImb_Dac, (double )mzData.LeftArmImb_Dac);
            SetValueDouble(EnumTosaParam.MzRightArmImb_Dac, (double)mzData.RightArmImb_Dac);
        }


        /// <summary>
        /// Fail Code
        /// </summary>
        public ChannelFailCode FailCode
        {
            get
            {
                ChannelFailCode failCode = ChannelFailCode.NoFail;
                EnumTosaParam[] failedParams = this.FailedParams;
                foreach (EnumTosaParam param in failedParams)
                {
                    switch (param)
                    {
                        case EnumTosaParam.TuningOk:
                            failCode |= ChannelFailCode.ItuTuning;
                            break;
                        case EnumTosaParam.PowerLevellingOk:
                            failCode |= ChannelFailCode.PowerLevel;
                            break;
                        default:
                            failCode |= ChannelFailCode.ParamOutOfSpec;
                            break;
                    }
                }
                return failCode;
            }
        }

        /// <summary>
        /// Figure of Merit for this channel.
        /// As Iphase and Irear could be changed by ITU tuning, ensure FOM is calculated once!
        /// </summary>
        public double FigureOfMerit
        {
            get
            {
                if (hasSetFOM)
                {
                    return figureOfMerit;
                }
                else
                {
                    // FOM = supermode * 1000 - front pair no * 100 - Iphase
                    double fom = this.GetValueSint32(EnumTosaParam.Supermode) * 1000
                        - this.GetValueSint32(EnumTosaParam.FrontSectionPair) * 100
                        - this.GetValueDouble(EnumTosaParam.IPhaseCal_mA);

                    // Add 10000 to FOM if Irear > 2.5 mA
                    if (this.GetValueDouble(EnumTosaParam.IRear_mA) > 2.5) fom += 10000;

                    // minus 10000 to FOM if rear<4mA and front section change happened. Echo-2011-09-21 Jack.Z's requirement
                    if (this.GetValueDouble(EnumTosaParam.IRear_mA )< 4 && (this.GetValueDouble(EnumTosaParam.IFsConst_mA) < 0.5 || this.GetValueDouble(EnumTosaParam.IFsNonConst_mA) > 4))
                    {
                        fom -= 10000;
                    }

                    // Reduce priority of supermode 6 
                    if (this.GetValueSint32(EnumTosaParam.Supermode) == 6)
                        //fom -= 2000;

                    // Set FOM = -999 if Iphase or Irear out of spec
                    if (this.ParamStatus(EnumTosaParam.IPhaseCal_mA) == PassFail.Fail ||
                        this.ParamStatus(EnumTosaParam.IRear_mA) == PassFail.Fail ||
                        this.GetValueSint32(EnumTosaParam.Supermode) == 7)
                        fom = -999999;

                    this.figureOfMerit = fom;
                    hasSetFOM = true;

                    // Return
                    return fom;
                }
            }
        }

        /// <summary>
        /// Dsdbr channel setup - generate from underlying values as tweaked
        /// </summary>
        public DsdbrChannelSetup DsdbrSetup
        {
            get
            {
                // laser section currents
                DsdbrChannelSetup dsdbrSetup;
                dsdbrSetup.IPhase_mA = GetValueDouble(EnumTosaParam.IPhaseCal_mA);
                dsdbrSetup.IRear_mA = GetValueDouble(EnumTosaParam.IRear_mA);
                dsdbrSetup.IGain_mA = GetValueDouble(EnumTosaParam.IGain_mA);
                dsdbrSetup.ISoa_mA = GetValueDouble(EnumTosaParam.ISoa_mA);
                dsdbrSetup.FrontPair = GetValueSint32(EnumTosaParam.FrontSectionPair);
                dsdbrSetup.IFsFirst_mA = GetValueDouble(EnumTosaParam.IFsConst_mA);
                dsdbrSetup.IFsSecond_mA = GetValueDouble(EnumTosaParam.IFsNonConst_mA);
                dsdbrSetup.IRearSoa_mA = GetValueDouble(EnumTosaParam.IRearSoa_mA);
                return dsdbrSetup;
            }
        }
        /// <summary>
        /// return frequency,  Echo new added 2010-12-21
        /// </summary>
        public double Freq_GHz
        {
            get
            {
                return GetValueDouble(EnumTosaParam.Freq_GHz);
            }
        }
            

        /// <summary>
        /// MZ settings at Peak and Quadrature transmission
        /// </summary>
        public MzData MzSettings
        {
            get
            {
                // MZ data
                MzData mzData;
                mzData.LeftArmMod_Peak_V = GetValueDouble(EnumTosaParam.MzLeftArmModPeak_V);
                mzData.RightArmMod_Peak_V = GetValueDouble(EnumTosaParam.MzRightArmModPeak_V);
                mzData.LeftArmMod_Quad_V = GetValueDouble(EnumTosaParam.MzLeftArmModQuad_V);
                mzData.RightArmMod_Quad_V = GetValueDouble(EnumTosaParam.MzRightArmModQuad_V);
                mzData.LeftArmMod_Min_V = GetValueDouble(EnumTosaParam.MzLeftArmModMinima_V);
                mzData.RightArmMod_Min_V = GetValueDouble(EnumTosaParam.MzRightArmModMinima_V);
                mzData.LeftArmImb_mA = GetValueDouble(EnumTosaParam.MzLeftArmImb_mA);
                mzData.RightArmImb_mA = GetValueDouble(EnumTosaParam.MzRightArmImb_mA);
                mzData.LeftArmImb_Dac = GetValueDouble(EnumTosaParam.MzLeftArmImb_Dac);
                mzData.RightArmImb_Dac = GetValueDouble(EnumTosaParam.MzRightArmImb_Dac);
                return mzData;
            }
        }
    }
}


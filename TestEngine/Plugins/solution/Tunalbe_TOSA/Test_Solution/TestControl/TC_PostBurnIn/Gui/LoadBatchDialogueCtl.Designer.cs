// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestControl
//
// LoadBatchDialogueCtl.Designer.cs
//
// Author: tommy.yu, 2007
// Design: [Reference design documentation]

namespace Bookham.TestSolution.TestControl
{
    partial class LoadBatchDialogueCtl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoadBatchDialogueCtl));
            this.retryAbortLabel = new System.Windows.Forms.Label();
            this.retryButton = new System.Windows.Forms.Button();
            this.abortButton = new System.Windows.Forms.Button();
            this.messageLabel = new System.Windows.Forms.Label();
            this.loadBatchScreenLabel = new System.Windows.Forms.Label();
            this.bookhamLogoPictureBox = new System.Windows.Forms.PictureBox();
            this.loadBatchButton = new System.Windows.Forms.Button();
            this.loadBatchTextBox = new System.Windows.Forms.TextBox();
            this.Button_NoMes = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.bookhamLogoPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // retryAbortLabel
            // 
            this.retryAbortLabel.AutoSize = true;
            this.retryAbortLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.retryAbortLabel.ForeColor = System.Drawing.Color.Red;
            this.retryAbortLabel.Location = new System.Drawing.Point(220, 332);
            this.retryAbortLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.retryAbortLabel.Name = "retryAbortLabel";
            this.retryAbortLabel.Size = new System.Drawing.Size(581, 25);
            this.retryAbortLabel.TabIndex = 6;
            this.retryAbortLabel.Text = "Load Batch operation failed. Please decide what to do next.";
            // 
            // retryButton
            // 
            this.retryButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.retryButton.Location = new System.Drawing.Point(697, 96);
            this.retryButton.Margin = new System.Windows.Forms.Padding(4);
            this.retryButton.Name = "retryButton";
            this.retryButton.Size = new System.Drawing.Size(111, 38);
            this.retryButton.TabIndex = 3;
            this.retryButton.Text = "Retry";
            this.retryButton.Click += new System.EventHandler(this.retryButton_Click);
            // 
            // abortButton
            // 
            this.abortButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.abortButton.Location = new System.Drawing.Point(829, 96);
            this.abortButton.Margin = new System.Windows.Forms.Padding(4);
            this.abortButton.Name = "abortButton";
            this.abortButton.Size = new System.Drawing.Size(111, 38);
            this.abortButton.TabIndex = 4;
            this.abortButton.Text = "Abort";
            this.abortButton.Click += new System.EventHandler(this.abortButton_Click);
            // 
            // messageLabel
            // 
            this.messageLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.messageLabel.ForeColor = System.Drawing.Color.Red;
            this.messageLabel.Location = new System.Drawing.Point(316, 162);
            this.messageLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.messageLabel.Name = "messageLabel";
            this.messageLabel.Size = new System.Drawing.Size(716, 55);
            this.messageLabel.TabIndex = 5;
            this.messageLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // loadBatchScreenLabel
            // 
            this.loadBatchScreenLabel.AutoSize = true;
            this.loadBatchScreenLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadBatchScreenLabel.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.loadBatchScreenLabel.Location = new System.Drawing.Point(191, 23);
            this.loadBatchScreenLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.loadBatchScreenLabel.Name = "loadBatchScreenLabel";
            this.loadBatchScreenLabel.Size = new System.Drawing.Size(578, 25);
            this.loadBatchScreenLabel.TabIndex = 4;
            this.loadBatchScreenLabel.Text = "Test Engine TestControl - Post-BurnIn: Load Batch Screen";
            // 
            // bookhamLogoPictureBox
            // 
            this.bookhamLogoPictureBox.Image = ((System.Drawing.Image)(resources.GetObject("bookhamLogoPictureBox.Image")));
            this.bookhamLogoPictureBox.Location = new System.Drawing.Point(9, 9);
            this.bookhamLogoPictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.bookhamLogoPictureBox.Name = "bookhamLogoPictureBox";
            this.bookhamLogoPictureBox.Size = new System.Drawing.Size(129, 44);
            this.bookhamLogoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.bookhamLogoPictureBox.TabIndex = 10;
            this.bookhamLogoPictureBox.TabStop = false;
            // 
            // loadBatchButton
            // 
            this.loadBatchButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadBatchButton.Location = new System.Drawing.Point(9, 162);
            this.loadBatchButton.Margin = new System.Windows.Forms.Padding(4);
            this.loadBatchButton.Name = "loadBatchButton";
            this.loadBatchButton.Size = new System.Drawing.Size(256, 55);
            this.loadBatchButton.TabIndex = 1;
            this.loadBatchButton.Text = "Load Batch";
            this.loadBatchButton.Click += new System.EventHandler(this.loadBatchButton_Click);
            // 
            // loadBatchTextBox
            // 
            this.loadBatchTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadBatchTextBox.Location = new System.Drawing.Point(9, 97);
            this.loadBatchTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.loadBatchTextBox.Name = "loadBatchTextBox";
            this.loadBatchTextBox.Size = new System.Drawing.Size(679, 34);
            this.loadBatchTextBox.TabIndex = 0;
            this.loadBatchTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.loadBatchTextBox_KeyPress);
            this.loadBatchTextBox.Enter += new System.EventHandler(this.loadBatchTextBox_Enter);
            // 
            // Button_NoMes
            // 
            this.Button_NoMes.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_NoMes.Location = new System.Drawing.Point(9, 249);
            this.Button_NoMes.Margin = new System.Windows.Forms.Padding(4);
            this.Button_NoMes.Name = "Button_NoMes";
            this.Button_NoMes.Size = new System.Drawing.Size(256, 55);
            this.Button_NoMes.TabIndex = 2;
            this.Button_NoMes.Text = "No MES";
            this.Button_NoMes.Visible = false;
            this.Button_NoMes.Click += new System.EventHandler(this.Button_NoMes_Click);
            // 
            // LoadBatchDialogueCtl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.Button_NoMes);
            this.Controls.Add(this.retryAbortLabel);
            this.Controls.Add(this.retryButton);
            this.Controls.Add(this.abortButton);
            this.Controls.Add(this.messageLabel);
            this.Controls.Add(this.loadBatchScreenLabel);
            this.Controls.Add(this.bookhamLogoPictureBox);
            this.Controls.Add(this.loadBatchButton);
            this.Controls.Add(this.loadBatchTextBox);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "LoadBatchDialogueCtl";
            this.Size = new System.Drawing.Size(1056, 396);
            this.VisibleChanged += new System.EventHandler(this.LoadBatchDialogueCtl_VisibleChanged);
            this.MsgReceived += new Bookham.TestEngine.Framework.Messages.ManagedCtlBase.MsgReceivedDlg(this.LoadBatchDialogueCtl_MsgReceived);
            ((System.ComponentModel.ISupportInitialize)(this.bookhamLogoPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label retryAbortLabel;
        private System.Windows.Forms.Button retryButton;
        private System.Windows.Forms.Button abortButton;
        private System.Windows.Forms.Label messageLabel;
        private System.Windows.Forms.Label loadBatchScreenLabel;
        private System.Windows.Forms.PictureBox bookhamLogoPictureBox;
        private System.Windows.Forms.Button loadBatchButton;
        private System.Windows.Forms.TextBox loadBatchTextBox;
        private System.Windows.Forms.Button Button_NoMes;



    }
}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestControl
//
// FcuMapping.cs
//
// Author: tommy.yu, 2007
// Design: [Reference design documentation]

using System;
using System.Xml;
using System.Collections;
using Bookham.TestEngine.Framework.Exceptions;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.PluginInterfaces.TestControl;
using Bookham.TestEngine.PluginInterfaces.TestJig;
using Bookham.TestEngine.PluginInterfaces.MES;
using Bookham.TestEngine.PluginInterfaces.Security;
using System.Collections.Specialized;
using System.Collections.Generic;
using Bookham.TestEngine.Config;
using Bookham.TestEngine.Framework.InternalData;
using System.IO;
using Bookham.Solution.Config;
using System.Data;
using Bookham.TestLibrary.Utilities;
using Bookham.TestLibrary.MES;
//using Bookham.TestSolution.IlmzCommonUtils;
using Bookham.TestEngine.Framework.Limits;

//using Bookham.TestSolution.TestPrograms;

namespace Bookham.TestSolution.TestControl
{

    /// <summary>
    /// Test Control Plug-in Template.
    /// NB: Implements a Test Control with a single device in the batch.
    /// The following parameters are hardcoded in the DUTObject and will need to be changed,
    /// either by replacement value or by reading the MES:
    /// - ProgramPluginName and Version
    /// - PartCode
    /// - TestStage
    /// </summary>
    public class TC_PostBurnIn : ITestControl
    {
        #region private members

        const string CONFIGFWPath = "Configuration/FactoryWorksConfig.xml";
        const string SettingsPath = "Plugins/solution/PrePostPhaseScan/xdata/TestSettings.xml";
        const string DeviceTypePath = "Configuration/TOSA Final/DeviceTypeLookUp.csv"; //"Plugins/solution/PrePostPhaseScan/xdata/DeviceTypeLookUp.csv";
        const string TestStagePath = "Configuration/TOSA Final/PhaseScan/TestStageLookUp_Post.csv"; //"Plugins/solution/PrePostPhaseScan/xdata/TestStageLookUp_pre.csv";
        // The current BatchID
        string batchID = "";
        string jigID = "";

        // Reference to Object passed by the Test Control Server that implements the 
        // IMES interface.
        IMES mes;
        FactoryWorks mes1;


        /// <summary>
        /// flag to indicate if test control configureation file was read or not
        /// </summary>
        bool isTCPliginConfigRead;

        /// <summary>
        /// Flag to indicate if Fws is available or not
        /// </summary>
        bool isMesOnline;
        /// <summary>
        /// Flag to indicate if dut trackout after test
        /// </summary>
        bool isDutTrackOut;

        //True if the Config file has been read
        bool SettingsHasRead;

        //Temporary simulation flag.
        bool inSimulationMode;

        // When True, the Plugin is retrying loading a Batch from the MES.
        bool retryingMesLoadBatch;

        //When True, we have a valid BatchId. 
        bool gotValidBatchId;

        //True if Trackout is to be inhibited. Read from the internal configuration file.
        bool trackoutInhibit;

        //Equipment ID as read from the configuration file.
        string equipmentId;

        // Object to hold the loaded batch.
        MESbatch loadedBatch;

        // Internal store of Test Statuses for the current batch.
        TestStatus[] testStatuses;
        // Flag for the program ran (so we can abort the batch)
        bool programRanOnce = false;

        /// <summary>
        /// Flag for use in GetNextDUT(). State of 'false' on first call, 'true' thereafter.
        /// </summary>
        bool subsequentCallOfGetNextDUT; // mfx

        // flag for if need to track out
        bool needtrackout;

        // The Index of the current DUT within the batch & Test Statuses array.
        int currentDutIndex;

        // The Program status, as stored in the Post Device operation.
        ProgramStatus programStatus;

        // Test results for the current DUT.
        TestResults testResults;

        int maxTestCount = 10;
        int FailTimeTrackTeckAcess = 3;

        DUTObject currentDut = new DUTObject();
        string Chip_ID = "";
        string Wafer_ID = "";
        string Coc_SN = "";
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor. 
        /// </summary>
        public TC_PostBurnIn()
        {
            // initialise the Batch View tab control array (private variable)
            this.batchCtlList = new Type[1];
            this.batchCtlList[0] = typeof(BatchViewCtl);
            // TODO: Add any new controls here

            // initialise the Test Control tab control array (private variable)
            this.testCtlList = new Type[2];
            this.testCtlList[0] = typeof(LoadBatchDialogueCtl);
            // TODO: Add any new controls here
            this.testCtlList[1] = typeof(LoadSnDialogueCtl);
        }

        #endregion

        #region ITestControl Implementation.

        /// <summary>
        /// Get Property.
        /// Informs the Test Engine of all the allowed Batch View GUI User Controls
        /// NB: DON'T CHANGE THIS!!!
        /// </summary>
        public Type[] BatchContentsCtlTypeList
        {
            get
            {
                return (batchCtlList);
            }
        }

        /// <summary>
        /// Get Property.
        /// Informs the Test Engine of all the allowed Batch View GUI User Controls
        /// NB: DON'T CHANGE THIS!!!
        /// </summary>
        public Type[] TestControlCtlTypeList
        {
            get
            {
                return (testCtlList);
            }
        }

        /// <summary>
        /// Get Property.
        /// This specifies whether the Test Control Plug-in operated in Manual or Automatic Mode.
        /// </summary>
        public AutoManual TestType
        {
            get
            {
                // TODO: Update. Return AutoManual.AUTO if this is an automatic mode Test Control Plug-in.
                return AutoManual.AUTO;
            }
        }

        /// <summary>
        /// Load a batch. Called when the Core requests that a new batch be loaded from the MES, via the 
        /// Test Control Server.
        /// </summary>
        /// <param name="testEngine">Object implementing the ITestEngine Interface.</param>
        /// <param name="mes">Object implementing the IMES interface.</param>
        /// <param name="operatorName">The name of the currently logged in user.</param>
        /// <param name="operatorType">The privilege level of the currently logged in user.</param>
        /// <returns>The BatchId of the loaded batch.</returns>
        public string LoadBatch(ITestEngine testEngine, IMES mes, string operatorName, TestControlPrivilegeLevel operatorType)
        {
            testEngine.SendStatusMsg("Load Batch");

            if (TC_Common.IsToCheckSoftwareUpdate)
            {
                CheckSoftwareUpdate.CheckUpdate(SOFTWARE_ID.HITT_GB_Test);
            }

            batchID = "";
            this.mes = mes;
            this.operatorID = operatorName;
            readTestControlConfig(testEngine);
            //if (!SettingsHasRead)
            //    ReadTestControlSettings(testEngine);

            // Bring the Test Control Tab to the front of the GUI, and attract the user's attention.
            testEngine.PageToFront(TestControlCtl.BatchView);
            testEngine.PageToFront(TestControlCtl.TestControl);
            testEngine.GetUserAttention(TestControlCtl.TestControl);
            while (true)
            {
                #region Comment out by chongjian.liang 2015.8.17

                ////Ask the operator if we are to run in Manual or Automatic Mode.--raul added at 2011.7.5
                //string manualModeResponseString = "MANUAL";
                //string autoModeResponseString = "AUTOMATIC";

                //Bookham.TestEngine.PluginInterfaces.TestControl.ButtonInfo manualButton
                //    = new Bookham.TestEngine.PluginInterfaces.TestControl.ButtonInfo("MANUAL", manualModeResponseString);

                //Bookham.TestEngine.PluginInterfaces.TestControl.ButtonInfo autoButton
                //    = new Bookham.TestEngine.PluginInterfaces.TestControl.ButtonInfo("AUTOMATIC", autoModeResponseString);

                //object modeRespObj = testEngine.ShowUserQuery(TestControlTab.TestControl, "Please specify whether Test Programs should be Manually or Automatically selected for each stage of test.",
                //                                            manualButton, autoButton);

                //string modeResponse = modeRespObj as string; 
                #endregion

                //inSimulationMode = true;
                //if (modeResponse == "AUTOMATIC")
                //if (!inSimulationMode)
                if (this.isMesOnline)
                {
                    testEngine.SelectTestControlCtlToDisplay(typeof(LoadBatchDialogueCtl));
                    testEngine.SendToCtl(TestControlCtl.TestControl, typeof(LoadBatchDialogueCtl), GuiMsgEnum.LoadBatchRequest);
                    isDutTrackOut = true;//need track out if auto test
                }
                else
                {
                    testEngine.SelectTestControlCtlToDisplay(typeof(LoadSnDialogueCtl));
                    List<string> stageList;
                    List<string> typeList;
                    readOfflineStagesAndTypes(testEngine, out stageList, out typeList);

                    this.MainTestStage = stageList[0];
                    this.Location = Location.DSDBR;

                    DatumStringArray sa = new DatumStringArray("stageList", stageList.ToArray());
                    testEngine.SendToCtl(TestControlCtl.TestControl, typeof(LoadSnDialogueCtl), sa);
                    //raise event
                    //UpdateUI.Instance.SendToGui(sa);

                    sa = new DatumStringArray("typeList", typeList.ToArray());
                    testEngine.SendToCtl(TestControlCtl.TestControl, typeof(LoadSnDialogueCtl), sa);
                    isDutTrackOut = false;//no need track out if manual test
                }
                // wait for the message to say "OK" has been clicked...
                object msg = testEngine.WaitForCtlMsg().Payload;

                // Check SPC 2016.09.02
                if (!TC_Common.CheckSPCStatus(testEngine, mes.Node))
                {
                    continue;
                }

                // Add Msg_NoMesRequest to switch to manual mode - chongjian.liang 2016.2.19
                if (msg is Msg_NoMesRequest)
                {
                    this.isMesOnline = false;
                    this.isDutTrackOut = false;

                    // To fill and disable the SN textbox in LoadSnDialogueCtl
                    if (msg is Msg_NoMesRequestWithSN)
                    {
                        testEngine.SendToCtl(TestControlCtl.TestControl, typeof(LoadSnDialogueCtl), msg);
                    }

                    continue;
                }

                //this.gotValidBatchId = false;
                //this.retryingMesLoadBatch = false;

                //if (modeResponse == "MANUAL")
                if (!this.isMesOnline)
                {
                    // Wait for a message from the Test Control GUI.
                    //CtlMsg msg = testEngine.WaitForCtlMsg();
                    //object obj = msg.Payload;
                    //object obj= MessageClass.WaitForGuiMsg();

                    if (msg is DatumString)
                    {
                        DatumString ds = msg as DatumString;
                        switch (ds.Name)
                        {
                            case "batchID":
                                batchID = ds.Value;
                                break;
                            default:
                                break;
                        }
                        //Stop attracting the user's attention.
                        testEngine.CancelUserAttention(TestControlCtl.TestControl);

                        //Set flag to indicate that there is a valid batchId.
                        //this.gotValidBatchId = true;
                    }
                    else if (msg is DatumList)
                    {
                        DatumList dl = msg as DatumList;
                        DUTObject dut = new DUTObject();

                        dut.PartCode = dl.ReadString("DeviceType");
                        dut.TestStage = dl.ReadString("TestStage");
                        dut.SerialNumber = dl.ReadString("SerialNumber");

                        dut.OverrideSimulationMode = false;
                        dut.IsSimulation = false;
                        dut.ContinueOnFail = false;
                        dut.BatchID = "BogusBatch";
                        dut.FirstInBatch = true;
                        dut.LastInBatch = true;
                        dut.TrayPosition = "96";
                        dut.Attributes.AddString("UnitTestType", "fcu map Integration");
                        dut.Attributes.AddString("ChipId", "Offline Test");
                        dut.Attributes.AddString("lot_type", "Engineering");

                        loadedBatch = new MESbatch();
                        loadedBatch.Add(dut);
                        loadedBatch.PartCode = dut.PartCode;
                        loadedBatch.Stage = dut.TestStage;
                        batchID = "BogusBatch";
                        //Stop attracting the user's attention.
                        testEngine.CancelUserAttention(TestControlCtl.TestControl);

                        //this.gotValidBatchId = true;
                        //break;
                    }
                    else
                    {
                        //Invalid message recieved.
                        testEngine.ErrorRaise("Invalid message recieved: " + msg.ToString());
                        break;
                    }

                    this.testStatuses = new TestStatus[loadedBatch.Count];
                    // Initialise each device to untested status.
                    int index = 0;
                    foreach (DUTObject dut in loadedBatch)
                    {
                        testEngine.SendToCtl(TestControlCtl.BatchView, typeof(BatchViewCtl), dut);
                        testStatuses[index++] = TestStatus.Untested;

                        dut.NodeID = mes.Node;

                        dut.Attributes.AddString("Location", Location.ToString());
                    }
                    // Send a load batch complete message to the Test Control GUI, so that it can hide 
                    // the load batch dialogue.
                    testEngine.SendToCtl(TestControlCtl.TestControl, typeof(LoadSnDialogueCtl), GuiMsgEnum.LoadBatchComplete);
                    testEngine.SendToCtl(TestControlCtl.BatchView, typeof(BatchViewCtl), testStatuses);
                    break;
                }
                else
                {
                    try
                    {
                        if (msg is DatumString)
                        {
                            DatumString ds = msg as DatumString;
                            this.batchID = ds.Value;

                            this.mes1 = (FactoryWorks)this.mes;
                            loadedBatch = this.mes1.LOTQUERYFULL_NEW(batchID, operatorID);
                            
                            //this.loadedBatch = mes.LoadBatch(this.batchID, true); 
                        }

                        // Confirm the factorywork stage - chongjian.liang 2015.9.6
                        if (loadedBatch.Stage != "POSTBI")
                        {
                            if (loadedBatch.Stage == "POSTBIPHASECUTSCAN")
                            {
                                testEngine.ShowContinueUserQuery(TestControlTab.TestControl, "Please reassign the Lot and set the step to POSTBI in the factorywork.");
                            }
                            else
                            {
                                testEngine.ShowContinueUserQuery(TestControlTab.TestControl, string.Format("The current step in the factorywork should be POSTBI instead of {0} for this test.", loadedBatch.Stage));
                            }

                            testEngine.SendToCtl(TestControlCtl.TestControl, typeof(LoadBatchDialogueCtl), GuiMsgEnum.LoadBatchRequest);

                            continue;
                        }


                        //this.mes.TrackIn(this.batchID);
                        this.mes1.TrackIn(batchID);
                        System.Threading.Thread.Sleep(10000);
                        Chip_ID = this.mes1.GetComponentAttributes(loadedBatch[0].BatchID, loadedBatch[0].SerialNumber, "X_CHIP_ID");
                        Wafer_ID = this.mes1.GetComponentAttributes(loadedBatch[0].BatchID, loadedBatch[0].SerialNumber, "X_WAFER_ID");
                        Coc_SN = this.mes1.GetComponentAttributes(loadedBatch[0].BatchID, loadedBatch[0].SerialNumber, "X_COC_SN");
                        // Initialise a new Test Statuses array.
                        this.testStatuses = new TestStatus[loadedBatch.Count];

                        // Initialise each device to untested status.
                        int index = 0;
                        foreach (DUTObject dut in loadedBatch)
                        {
                            testEngine.SendToCtl(TestControlCtl.BatchView, typeof(BatchViewCtl), dut);
                            testStatuses[index++] = TestStatus.Untested;

                            dut.NodeID = mes.Node;
                            dut.Attributes.AddString("Location", Location.ToString());
                        }

                        testEngine.SendToCtl(TestControlCtl.TestControl, typeof(LoadBatchDialogueCtl), GuiMsgEnum.LoadBatchComplete);
                        testEngine.SendToCtl(TestControlCtl.BatchView, typeof(BatchViewCtl), testStatuses);
                        break;
                    }
                    catch(Exception e)
                    {
                        string msgEx = string.Format("Failed to load the batch {0}.", this.batchID);

                        testEngine.SendStatusMsg(msgEx);

                        testEngine.ShowContinueUserQuery(TestControlTab.TestControl, msgEx);

                        testEngine.SendToCtl(TestControlCtl.TestControl, typeof(LoadBatchDialogueCtl), GuiMsgEnum.LoadBatchRequest);

                        continue;
                    }
                }
            }

            testEngine.CancelUserAttention(TestControlCtl.TestControl);
            testEngine.GuiClear(TestControlCtl.TestControl);
            // Reset one-time run flag
            programRanOnce = false;
            // Return serial number or batch id to Test Engine
            return batchID;
        }

        private string ReadTxt(string filePathName)
        {

            string titleString = null;

            if (File.Exists(filePathName))
            {
                using (StreamReader sr = File.OpenText(filePathName))
                {
                    string tempString;
                    while ((tempString = sr.ReadLine()) != null)
                    {
                        titleString = tempString;
                    }
                }
            }
            return titleString;
        }

        private void readOfflineStagesAndTypes_old(ITestEngine testEngine, out List<string> stageList, out List<string> typeList)
        {
            ConfigurationManager cfg = new ConfigurationManager(SettingsPath);
            DataTable progLookup = (DataTable)cfg.GetSection("TestControl/" + Location.ToString() + "_TestStages/MainTestStages");

            stageList = new List<string>();
            typeList = new List<string>();
            foreach (DataRow row in progLookup.Rows)
            {
                // Get Test Stages
                string str = (string)row["TestStage"];
                if (string.IsNullOrEmpty(str)) throw new Exception("No TestStage node!");
                if (!stageList.Contains(str)) stageList.Add(str);

                // Get Part Codes
                str = (string)row["DeviceType"];
                if (string.IsNullOrEmpty(str)) throw new Exception("No DeviceType node!");
                if (!typeList.Contains(str)) typeList.Add(str);

            }

        }
        private void readOfflineStagesAndTypes(ITestEngine testEngine, out List<string> stageList, out List<string> typeList)
        {
            stageList = new List<string>();
            typeList = new List<string>();
            CsvReader csvReader = new CsvReader();
            List<string[]> lines = csvReader.ReadFile(DeviceTypePath);
            // the first line is title , so we read device type data from line2
            for (int i = 1; i < lines.Count; ++i)
            {
                typeList.Add(lines[i][0]);

            }
            lines.Clear();
            CsvReader CR = new CsvReader();
            lines = CR.ReadFile(TestStagePath);
            for (int ii = 1; ii < lines.Count; ++ii)
            {
                stageList.Add(lines[ii][0]);
            }


        }


        private void dealWithLoadBatchFromMesError(ITestEngine testEngine)
        {
            testEngine.SendToCtl(TestControlCtl.TestControl, typeof(LoadBatchDialogueCtl), new DatumString("RetryAbortRequest", "MES Load Batch operation Timed Out."));
            // wait for response.
            CtlMsg msg = testEngine.WaitForCtlMsg();
            object obj = msg.Payload;
            //object obj = MessageClass.WaitForGuiMsg();

            if (obj.GetType().Equals(typeof(QueryRetryAbort)))
            {
                QueryRetryAbort qr = (QueryRetryAbort)obj;
                switch (qr)
                {
                    case QueryRetryAbort.Abort:
                        // the User elected to abort. Request that the user select another batch for loading.
                        testEngine.SendStatusMsg("User aborted load batch operation");
                        //Instruct the Test Control User Control to get user input to load a batch.
                        testEngine.SendToCtl(TestControlCtl.TestControl, typeof(LoadBatchDialogueCtl), GuiMsgEnum.LoadBatchRequest);
                        retryingMesLoadBatch = false;
                        break;
                    default:
                        retryingMesLoadBatch = true;
                        break;
                }
            }
            else
            {
                // Something very wrong - Log an error.
                testEngine.ErrorRaise("Invalid message recieved");
            }
        }

        /// <summary>
        /// Get the next available DUT from the Batch.
        /// </summary>
        /// <param name="testEngine">Object that implements the ITestEngine Interface.</param>
        /// <param name="testJigArg">Object tha implements ITestJig Interface.</param>
        /// <param name="operatorName">The name of the currently logged in user.</param>
        /// <param name="operatorType">Privilege level of the currently logged in user.</param>
        /// <returns>DUT object for the next selected device for test or Null if end of batch.</returns>
        public DUTObject GetNextDUT(ITestEngine testEngine, ITestJig testJigArg, string operatorName, TestControlPrivilegeLevel operatorType)
        {

            bool found = false;
            string receivedSerialNumber = " ";

            // mfxV

            if (this.loadedBatch.Stage == "manual_fxit" && // special Manual mode (no BatchView)
                this.subsequentCallOfGetNextDUT == false)
            {
                // this.subsequentCallOfGetNextDUT will be set 'true' for next time - to allow user to re-test

                bool foundTheFirstOne = false;
                this.currentDutIndex = 0; // only the one DUT.

                foreach (DUTObject dutObject in loadedBatch)
                {
                    foundTheFirstOne = true; // Found the DUT Object 
                    break;    // Break out of the For loop.
                }

                if (foundTheFirstOne == false)
                {
                    // The required DUT is not in the batch. This is a fatal error.
                    testEngine.ErrorRaise("Get Next DUT: No DUT Specified");
                }

            }
            else // normal 'prompt-user-with-batchView' path ...
            {
                this.subsequentCallOfGetNextDUT = true;
                // mfxV

                //Grab the user's attention, and instruct them to select a DUT.
                testEngine.PageToFront(TestControlCtl.BatchView);
                testEngine.SendStatusMsg("Select a DUT");
                testEngine.GetUserAttention(TestControlCtl.BatchView);
                testEngine.SendToCtl(TestControlCtl.BatchView, typeof(BatchViewCtl), GuiMsgEnum.SelectNextDUTRequest);

                // Select DUT - wait for message from BatchDisplay user ctl

                while (true)
                {
                    CtlMsg msg = testEngine.WaitForCtlMsg();
                    Type messageType = msg.Payload.GetType();
                    object obj = msg.Payload;
                    //object obj = MessageClass.WaitForGuiMsg();
                    //Type messageType = obj.GetType();

                    if (messageType == typeof(GuiMsgEnum))
                    {
                        // Exit here with a Null DUT object if we have reached the end of the batch.
                        GuiMsgEnum gm = (GuiMsgEnum)obj;
                        if (gm == GuiMsgEnum.EndOfBatchNotification)
                        {
                            testEngine.CancelUserAttention(TestControlCtl.BatchView);
                            return null;
                        }
                    }
                    else if (messageType == typeof(DatumString))
                    {
                        // The user marked a device for retest. Update the internal test status for the device.
                        DatumString response = (DatumString)obj;
                        if (response.Name == "NextSN")
                        {
                            receivedSerialNumber = response.Value;
                            //Find the index of the DUT Object in the loaded batch. Store this in the 
                            // currentDutIndex class private variable.
                            int index = 0;
                            foreach (DUTObject dutObject in loadedBatch)
                            {
                                if (dutObject.SerialNumber == receivedSerialNumber)
                                {
                                    currentDutIndex = index;
                                    found = true;
                                    // Found the DUT Object for the next device to be tested. Break out of the For loop.
                                    break;
                                }
                                index++;
                            }
                            // Break out of the message recieve while (true) loop.
                            break;
                        }
                        else if (response.Name == "MarkSN")
                        {
                            for (int i = 0; i < testStatuses.Length; i++)
                            {
                                if (loadedBatch[i].SerialNumber == response.Value)
                                {
                                    testStatuses[i] = TestStatus.MarkForRetest;
                                    // Break out of for loop, as we've found the deivce and updated it's status.
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        testEngine.ErrorRaise("Test Control Plug-in Get Next DUT Operation - Invalid Message received: " + messageType.ToString());
                    }
                } // While (true)

                if (!found)
                {
                    //The required DUT is not in the bacth. This is a fatal error.
                    testEngine.ErrorRaise("Get Next DUT: Invalid DUT Specified " + receivedSerialNumber);
                }

                // Stop attracting user attention
                testEngine.CancelUserAttention(TestControlCtl.BatchView);

                // mfxV
            } // end of normal 'prompt-user-with-batchView'

            this.subsequentCallOfGetNextDUT = true; // for next time
            // mfx^ 

            DUTObject dut = new DUTObject();

            dut.BatchID = this.loadedBatch[this.currentDutIndex].BatchID;
            dut.SerialNumber = this.loadedBatch[this.currentDutIndex].SerialNumber;
            dut.NodeID = this.mes.Node;
            dut.ProgramPluginName = "Prog_PostPhaseCutScan";
            dut.ProgramPluginVersion = string.Empty;
            dut.TestStage = "post burn";
            dut.PartCode = "HIT2";
            dut.TestJigID = testJigArg.GetJigID(testEngine.InDebugMode, testEngine.InSimulationMode);
            dut.Attributes.AddString("lot_type", this.loadedBatch[this.currentDutIndex].Attributes.GetDatumString("lot_type").ValueToString());
            dut.Attributes.AddString("x_chip_id", Chip_ID);
            dut.Attributes.AddString("x_wafer_id", Wafer_ID);
            dut.Attributes.AddString("x_coc_sn", Coc_SN);
            if (dut.Attributes.GetDatumString("lot_type").ValueToString().ToLower().Equals("production"))
            {
                try
                {
                    //add max continous test count
                    if (RetestOutofRange(testEngine, dut.TestStage, dut))
                    {
                        testEngine.ShowContinueUserQuery(TestControlTab.TestControl,
                            string.Format("Retest count out of spec, testing denied! MaxTestCount[{0}]", this.maxTestCount));

                        return null;
                    }
                }
                catch { }
            }

            return dut;
        }


        #region[add   for retest issue    ------------add by Dong.Chen ]                                                  ]
        private bool RetestOutofRange(ITestEngine engine, string stage, DUTObject dut)
        {
            StringDictionary mapKeys = new StringDictionary();

            mapKeys.Add("SCHEMA", "hiberdb");
            mapKeys.Add("SERIAL_NO", dut.SerialNumber);//this.serialNum);
            mapKeys.Add("TEST_STAGE", dut.TestStage);  //this.pcasTestStage);
            //mapKeys.Add("DEVICE_TYPE", dut.PartCode);  //this.pcasDeviceType);//这个要加上，因为测试次数达到最大时，可以转Code再测试。

            DatumList list = engine.GetDataReader().GetLatestResults(mapKeys, false);

            if (list.IsPresent("RETEST"))
            {
                //string retest = list.ReadDouble("RETEST").ToString();   // list["RETEST"].ValueToString();
                string retest = list.GetDatumString("RETEST").ValueToString();

                engine.SendStatusMsg("RETEST:" + retest);

                int CL = this.ReadRetest(stage);
                maxTestCount = CL;
                int RC = 999;
                int.TryParse(retest.Trim(), out RC);

                if (RC == CL - 1)
                    engine.ShowContinueUserQuery(TestControlTab.TestControl, "Warning: This is the last time to test this device on this stage!!!");
                else if (RC == CL)
                {
                    return true;
                }
            }
            return false;
        }

        private int ReadRetest(string stage)
        {
            string file = "Configuration\\TOSA Final\\RetestTimeConfiguration.csv";
            if (!File.Exists(file)) return 999;

            int count = 999;

            using (StreamReader sr = new StreamReader(file))
            {
                while (!sr.EndOfStream)
                {
                    string[] line = sr.ReadLine().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                    if (line.Length != 2) continue;

                    if (stage.ToLower() == line[0].Trim().ToLower())
                    {
                        int.TryParse(line[1].Trim(), out count);

                        break;
                    }
                }
            }
            return count;
        }
        #endregion
        private void SetTestProgram(ITestEngine testEngine)
        {
            DUTObject dut = this.loadedBatch[this.currentDutIndex];
            currentDut = dut;
            if (dut.TestStage.ToLower().Contains("pre"))
            {
                dut.ProgramPluginName = "Prog_PrePhaseCutScan";
            }
            if (dut.TestStage.ToLower().Contains("post"))
            {
                dut.ProgramPluginName = "Prog_PostPhaseCutScan";
            }
            dut.ProgramPluginVersion = "";
            dut.TestJigID = jigID;
            //dut.Attributes.AddString("OPERATOR_ID", this.operatorID);
            //ConfigurationManager cfg = new ConfigurationManager(SettingsPath);
            //DataTable progLookup = (DataTable)cfg.GetSection("TestControl/" + Location.ToString() + "_TestStages/MainTestStages");


            //string TestProgramName = null;
            //string TestProgramVersion = null;
            //foreach (DataRow row in progLookup.Rows)
            //{
            //    if ((string)row["DeviceType"] == dut.PartCode && (string)row["TestStage"] == dut.TestStage)
            //    {
            //        TestProgramName = (string)row["TestProgramName"];
            //        TestProgramVersion = (string)row["TestProgramVersion"];
            //        break;
            //    }
            //}

            //// First check if there is a matching data row
            //if (string.IsNullOrEmpty(TestProgramName))
            //{
            //    //Invalid configuration file contents.
            //    testEngine.ErrorRaise("Cannot find matching row in " + SettingsPath + " for DeviceType=" + dut.PartCode + " TestStage=" + dut.TestStage);
            //    dut.ProgramPluginName = "UNKNOWN";
            //    dut.ProgramPluginVersion = string.Empty;
            //}
            //else
            //{
            //    dut.ProgramPluginName = TestProgramName;
            //    dut.ProgramPluginVersion = TestProgramVersion;
            //}
        }


        /// <summary>
        /// Called after the test program has concluded, to decide what the outgoing 
        /// Part ID and other attributes are, having inspected the test results.
        /// </summary>
        /// <param name="testEngine">Reference to object implmenting ITestEngine</param>
        /// <param name="programStatus">The Status of the Test Program Run (succeeded/error/aborted etc).</param>
        /// <param name="testResults">Detailed test results</param>
        /// <returns>Outcome of the results analysis.</returns>
        public DUTOutcome PostDevice(ITestEngine testEngine, ProgramStatus programStatus, TestResults testResults)
        {
            // Store the results for use in the Post Device commit Phase.
            testEngine.SendStatusMsg("Post Device");
            this.testResults = testResults;
            this.programStatus = programStatus;

            // Extract the Specification name from the test results. Fill this in the DUToutcome.
            SpecResultsList specs = testResults.SpecResults;
            string[] specNames = new string[specs.Count];

            int i = 0;
            foreach (SpecResults specRes in specs)
            {
                specNames[i++] = specRes.Name;
            }

            dutOutcome.DUTData = new DatumList();
            dutOutcome.OutputSpecificationNames = new string[1];
            dutOutcome.OutputSpecificationNames[0] = specNames[0];

            return dutOutcome;
        }

        /// <summary>
        /// Commit any post-test MES updates prepared in method PostDevice.
        /// </summary>
        /// <param name="testEngine">Object implementing ITestEngine</param>
        /// <param name="dataWriteError">True if there was a problem writing Test Results to the database.</param>
        public void PostDeviceCommit(ITestEngine testEngine, bool dataWriteError)
        {
            //TODO: For those Test Software Solutions which interact with an MES, store the test
            //status for the current device under test to the MES.
            testEngine.SendStatusMsg("Post Device Commit");

            //Mark the Device as Tested. Also record it's pass fail status.
            if (testResults.ProgramStatus.Status == MultiSpecPassFail.AllPass)
            {
                if ((this.programStatus == ProgramStatus.Success) && (!dataWriteError))
                {
                    //The Test Passed. Mark the device as completed.
                    testStatuses[currentDutIndex] = TestStatus.Passed;

                    if (isMesOnline && isDutTrackOut)
                    {
                        this.mes1.TrackOutBatchPass(loadedBatch[0].BatchID, loadedBatch.Stage, loadedBatch.PartCode);
                    }
                }
                else if ((this.programStatus == ProgramStatus.Success) && (dataWriteError))
                {
                    //Problem writing Test Data. Mark for retest.
                    testStatuses[currentDutIndex] = TestStatus.MarkForRetest;
                }
            }
            else
            {
                //if (this.programStatus == ProgramStatus.NonParametricFailure)
                //// We only want to condemn a device which is definitely duff, so assume all
                //// other statuses leave the device as "untested".
                //{
                testStatuses[currentDutIndex] = TestStatus.Failed;

                if (this.dutOutcome.DUTData.ReadSint32("RETEST") >= this.FailTimeTrackTeckAcess)
                {
                    this.mes1.TrackOutComponentRework(loadedBatch[0].BatchID, loadedBatch.Stage, loadedBatch[0].SerialNumber, loadedBatch.PartCode);
                }

                //Update the Component level MES Data for the device if not in Simulation mode.
                //if (!inSimulationMode)
                //{
                //    mes.SetComponentAttribute(batchID, loadedBatch[currentDutIndex].SerialNumber, "PartId", "FAILED");
                //}
                //}
                //else
                //{
                //    testStatuses[currentDutIndex] = TestStatus.Untested;
                //}
            }

            testEngine.SendToCtl(TestControlCtl.BatchView, typeof(BatchViewCtl), testStatuses);
        }

        /// <summary>
        /// The Test Engine has concluded operations on the currently loaded batch. Update Batch level parameters 
        /// in the MES.
        /// </summary>
        /// <param name="testEngine">Reference to object that implements the ITestEngine Interface.</param>
        public void PostBatch(ITestEngine testEngine)
        {
            //TODO: For those Test Software Solutions which interact with an MES, store the test
            //status for the currently loaded Batch in the MES.
            testEngine.SendStatusMsg("Post Batch");
        }

        /// <summary>
        /// Mark all devices in the Batch for retest.
        /// </summary>
        /// <param name="testEngine">Reference to object that implements the ITestEngine Interface.</param>
        public void RestartBatch(ITestEngine testEngine)
        {

            //TODO: Use whatever method is appropriate to your implmentation to mark all devices in the 
            //current batch untested. It is up to the implmenter to decide whether this means restart all 
            //devices tested in this session or whether to restart the entire batch. Where this functionality
            //is not required, the solution developer may choose to take no action here.
            testEngine.SendStatusMsg("Restart Batch");

            for (int i = 0; i < testStatuses.Length; i++)
            {
                if (testStatuses[i] != TestStatus.Untested)
                {
                    testStatuses[i] = TestStatus.MarkForRetest;
                }

                testEngine.SendToCtl(TestControlCtl.BatchView, typeof(BatchViewCtl), testStatuses);
            }
        }
        # endregion

        #region Private Methods
        private void readTestControlConfig(ITestEngine engine)
        {
            if (isTCPliginConfigRead)
                return;

            int result;
            string commonErrorMessageString = "Unable to read Test Control Plug-in configuration File: ";
            string CONFIGFilePath = "Configuration/TOSA Final/";
            //Read Configuration file.
            /* Open user data XML file. This constructor call only conducts a scant validation of the
             * file name. Any expected error will not occur until the first call to Read. */
            XmlTextReader config = new XmlTextReader(CONFIGFilePath + "/TestControlPluginConfig.xml");

            try
            {
                /* Scan file, one element a time. */
                while (config.Read())
                {
                    if (config.NodeType == XmlNodeType.Element)
                    {
                        if (config.LocalName == "UseMES")
                        {
                            string useMes = config.ReadString();

                            result = String.Compare(useMes.Trim(), "yes", true, System.Globalization.CultureInfo.InvariantCulture);
                            if (result == 0)
                            {
                                isMesOnline = true;
                            }
                            else
                            {
                                result = String.Compare(useMes.Trim(), "no", true, System.Globalization.CultureInfo.InvariantCulture);

                                if (result == 0)
                                {
                                    isMesOnline = false;
                                }
                                else
                                {
                                    //Invalid configuration file contents.
                                    engine.ErrorRaise("Invalid UseMES field contents in Configuration/ITLA/TestControlPluginConfig.xml configuration file.");
                                }
                            }  // end else
                        }  // end if "UseMes"

                        if (config.LocalName == "InhibitTrackout")
                        {
                            string trackoutInhibitString = config.ReadString();

                            result = String.Compare(trackoutInhibitString.Trim(), "yes", true);
                            if (result == 0)
                            {
                                isDutTrackOut = false;
                            }
                            else
                            {
                                result = String.Compare(trackoutInhibitString.Trim(), "no", true);

                                if (result == 0)
                                {
                                    isDutTrackOut = true;
                                }
                                else
                                {
                                    //Invalid configuration file contents.
                                    engine.ErrorRaise("Invalid InhibitTrackout field contents in Configuration/ITLA/TestControlPluginConfig.xml configuration file.");
                                }
                            }  // end else: "InhibitTrackout" != "yes"
                        }  // end if "InhibitTrackout"


                        //FailTrackTeckAcess

                        if (config.LocalName == "FailTrackTeckAcess")
                        {
                            string FailTrackTeckAcess = config.ReadString();
                            ///int FailTimeTrackTeckAcess;
                            if (!int.TryParse(FailTrackTeckAcess.Trim(), out this.FailTimeTrackTeckAcess))
                            {
                                //Invalid configuration file contents.
                                engine.ErrorRaise("Invalid InhibitTrackout field contents in Configuration/ITLA/TestControlPluginConfig.xml configuration file.");
                            }

                        }  // end if "Inh

                    } // end if config.NodeType == XmlNodeType.Element: get available text nod
                }  // end while loop

                // Set flag to indicate that configuration has been read.
                isTCPliginConfigRead = true;
            }
            catch (UnauthorizedAccessException uae)
            {

                /* User did not have read permission for file. */
                engine.ErrorRaise(commonErrorMessageString + "Access denied " + uae.Message + uae.StackTrace);
            }
            catch (System.IO.FileNotFoundException fnfe)
            {
                /* File not found error. */
                engine.ErrorRaise(commonErrorMessageString + "Configuration File not found " + fnfe.Message);
            }
            catch (System.IO.IOException ioe)
            {
                /* General IO failure. Not file not found. */
                engine.ErrorRaise(commonErrorMessageString + "General IO Failure " + ioe.Message);
            }
            catch (System.Xml.XmlException xe)
            {
                /* XML parse error. */
                engine.ErrorRaise(commonErrorMessageString + "XMl Parser Error " + xe.Message);
            }
            finally
            {
                /* Close file. */
                if (config != null) config.Close();
            }
        }
        private void ReadTestControlSettings(ITestEngine testEngine)
        {
            SettingsHasRead = false;
            ConfigurationManager cfg = new ConfigurationManager(SettingsPath);
            // commented Alice.Huang   2010-03-01
            NameValueCollection Settings = (NameValueCollection)cfg.GetSection("TestControl/ControlSettings");
            this.inSimulationMode = bool.Parse(Settings["SimulateMes"]);
            //this.trackoutInhibit = bool.Parse(Settings["InhibitTrackout"]);
            //this.nodeID = int.Parse(Settings["NodeId"]);
            //this.equipmentId = Settings["EquipmentId"];
            this.Location = (Location)Enum.Parse(typeof(Location), Settings["Location"]);
            DataTable TestStages = (DataTable)cfg.GetSection("TestControl/" + Location.ToString() + "_TestStages/MainTestStages");
            MainTestStage = (string)TestStages.Rows[0]["TestStage"];
            // Set flag to indicate that configuration has been read.
            SettingsHasRead = true;

        }

        #endregion
        #region Private data
        // Batch View tab controls
        Type[] batchCtlList;
        // Test Control tab controls
        Type[] testCtlList;
        Location Location;
        string MainTestStage;
        string operatorID;
        DUTOutcome dutOutcome = new DUTOutcome();
        #endregion

    }

    internal enum Location
    {
        NONE,
        DSDBR,
        TCMZ,

    }
    internal enum GuiMsgEnum
    {
        LoadBatchRequest,
        LoadBatchComplete,
        EndOfBatchNotification,
        SelectNextDUTRequest,

    }
    /// <summary>
    /// User query result enum: Retry or abort current operation.
    /// </summary>
    internal enum QueryRetryAbort
    {
        /// <summary>
        /// Retry
        /// </summary>
        Retry,

        /// <summary>
        /// Abort.
        /// </summary>
        Abort
    }

    /// <summary>
    /// Test Status of devices within a batch
    /// </summary>
    internal enum TestStatus
    {
        /// <summary>
        /// Untested.
        /// </summary>
        Untested,

        /// <summary>
        /// Marked for restest.
        /// </summary>
        MarkForRetest,

        /// <summary>
        /// Passed.
        /// </summary>
        Passed,

        /// <summary>
        /// Failed.
        /// </summary>
        Failed
    }

}

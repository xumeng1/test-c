// [Copyright]
//
// Bookham Test Engine
// $projectname$
//
// TC_Txp40GDataDeliver/ManualLoadBatchDialogueCtl
// 
// Author: alice.huang
// Design: TODO

namespace Bookham.TestSolution.TestControl
{
    /// <summary>
    /// GUI to allow user to input DUT Information when Fws Offline
    /// </summary>
    partial class ManualLoadBatchDialogueCtl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtSN = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnLoadBatch = new System.Windows.Forms.Button();
            this.lvDeviceType = new System.Windows.Forms.ListView();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbStage = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // txtSN
            // 
            this.txtSN.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSN.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSN.Location = new System.Drawing.Point(184, 87);
            this.txtSN.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtSN.Name = "txtSN";
            this.txtSN.Size = new System.Drawing.Size(199, 37);
            this.txtSN.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 87);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(124, 31);
            this.label1.TabIndex = 10;
            this.label1.Text = "DUT SN:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(436, 87);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(204, 31);
            this.label2.TabIndex = 13;
            this.label2.Text = "Input Part Code";
            // 
            // btnLoadBatch
            // 
            this.btnLoadBatch.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoadBatch.Location = new System.Drawing.Point(22, 238);
            this.btnLoadBatch.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnLoadBatch.Name = "btnLoadBatch";
            this.btnLoadBatch.Size = new System.Drawing.Size(361, 53);
            this.btnLoadBatch.TabIndex = 3;
            this.btnLoadBatch.Text = "OK";
            this.btnLoadBatch.UseVisualStyleBackColor = true;
            this.btnLoadBatch.Click += new System.EventHandler(this.btnLoadBatch_Click);
            // 
            // lvDeviceType
            // 
            this.lvDeviceType.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvDeviceType.GridLines = true;
            this.lvDeviceType.HideSelection = false;
            this.lvDeviceType.LabelWrap = false;
            this.lvDeviceType.Location = new System.Drawing.Point(704, 76);
            this.lvDeviceType.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lvDeviceType.MultiSelect = false;
            this.lvDeviceType.Name = "lvDeviceType";
            this.lvDeviceType.Size = new System.Drawing.Size(232, 142);
            this.lvDeviceType.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.lvDeviceType.TabIndex = 2;
            this.lvDeviceType.UseCompatibleStateImageBehavior = false;
            this.lvDeviceType.View = System.Windows.Forms.View.List;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(16, 162);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(154, 31);
            this.label3.TabIndex = 6;
            this.label3.Text = "Test Stage:";
            // 
            // cmbStage
            // 
            this.cmbStage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbStage.FormattingEnabled = true;
            this.cmbStage.Items.AddRange(new object[] {
            "hitt_spc",
            "hitt_spc_base"});
            this.cmbStage.Location = new System.Drawing.Point(184, 165);
            this.cmbStage.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbStage.Name = "cmbStage";
            this.cmbStage.Size = new System.Drawing.Size(199, 28);
            this.cmbStage.Sorted = true;
            this.cmbStage.TabIndex = 1;
            // 
            // ManualLoadBatchDialogueCtl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.cmbStage);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lvDeviceType);
            this.Controls.Add(this.btnLoadBatch);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtSN);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "ManualLoadBatchDialogueCtl";
            this.Size = new System.Drawing.Size(972, 401);
            this.MsgReceived += new Bookham.TestEngine.Framework.Messages.ManagedCtlBase.MsgReceivedDlg(this.ManualLoadBatchDialogueCtl_MsgReceived);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtSN;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnLoadBatch;
        private System.Windows.Forms.ListView lvDeviceType;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbStage;
    }
}

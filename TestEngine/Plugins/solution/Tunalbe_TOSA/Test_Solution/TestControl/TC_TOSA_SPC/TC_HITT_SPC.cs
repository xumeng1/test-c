// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestControl
//
// TC_HITT_SPC.cs
//
// Author: alice.huang, 2010
// Design: [Reference design documentation]

using System;
using System.Xml;
using System.Collections;
using System.Collections.Generic;
using Bookham.TestEngine.Framework.Exceptions;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.PluginInterfaces.TestControl;
using Bookham.TestEngine.PluginInterfaces.TestJig;
using Bookham.TestEngine.PluginInterfaces.MES;
using Bookham.TestEngine.PluginInterfaces.Security;
using System.Collections.Specialized;
using Bookham.TestSolution.IlmzCommonUtils;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestLibrary.Utilities;

namespace Bookham.TestSolution.TestControl
{
    /// <summary>
    /// Test Control Plug-in Template.
    /// NB: Implements a Test Control with a single device in the batch.
    /// The following parameters are hardcoded in the DUTObject and will need to be changed,
    /// either by replacement value or by reading the MES:
    /// - ProgramPluginName and Version
    /// - PartCode
    /// - TestStage
    /// </summary>
    public class TC_HITT_SPC : ITestControl
    {
        #region Constructor
        /// <summary>
        /// Constructor. 
        /// </summary>
        public TC_HITT_SPC()
        {
            // initialise the Batch View tab control array (private variable)
            this.batchCtlList = new Type[1];
            this.batchCtlList[0] = typeof(BatchViewCtl);  // show dut infomation
            // TODO: Add any new controls here

            // initialise the Test Control tab control array (private variable)
            this.testCtlList = new Type[2];
            this.testCtlList[0] = typeof(LoadBatchDialogueCtl);  // mes online
            this.testCtlList[1] = typeof(ManualLoadBatchDialogueCtl);  // Mes offline, input Dut info manually
            // TODO: Add any new controls here
        }

        #endregion

        #region ITestControl Implementation.

        /// <summary>
        /// Get Property.
        /// Informs the Test Engine of all the allowed Batch View GUI User Controls
        /// NB: DON'T CHANGE THIS!!!
        /// </summary>
        public Type[] BatchContentsCtlTypeList
        {
            get
            {
                return (batchCtlList);
            }
        }

        /// <summary>
        /// Get Property.
        /// Informs the Test Engine of all the allowed Batch View GUI User Controls
        /// NB: DON'T CHANGE THIS!!!
        /// </summary>
        public Type[] TestControlCtlTypeList
        {
            get
            {
                return (testCtlList);
            }
        }

        /// <summary>
        /// Get Property.
        /// This specifies whether the Test Control Plug-in operated in Manual or Automatic Mode.
        /// </summary>
        public AutoManual TestType
        {
            get
            {
                // TODO: Update. Return AutoManual.AUTO if this is an automatic mode Test Control Plug-in.
                return AutoManual.MANUAL;
            }
        }

        /// <summary>
        /// Load a batch. Called when the Core requests that a new batch be loaded from the MES, via the 
        /// Test Control Server.
        /// </summary>
        /// <param name="testEngine">Object implementing the ITestEngine Interface.</param>
        /// <param name="mes">Object implementing the IMES interface.</param>
        /// <param name="operatorName">The name of the currently logged in user.</param>
        /// <param name="operatorType">The privilege level of the currently logged in user.</param>
        /// <returns>The BatchId of the loaded batch.</returns>
        public string LoadBatch(ITestEngine testEngine, IMES mes, 
                        string operatorName, TestControlPrivilegeLevel operatorType)
        {
            testEngine.SendStatusMsg("Load Batch");

            if (TC_Common.IsToCheckSoftwareUpdate)
            {
                CheckSoftwareUpdate.CheckUpdate(SOFTWARE_ID.HITT_GB_Test);
            }

            // Get the Node from IMES (e.g. For FactoryWorks this is set by it's config XML file)
            this.inSimulationMode = testEngine.InSimulationMode;
            this.mes = mes;
            this.node = mes.Node;

            DutInfo = null;
            //readTestControlConfig(testEngine);
            isMesOnline = false;

            System.Collections.Generic.Dictionary<string, DeviceTypeLookupItem> deviceTypeInfo =
                TosaDeviceTypeConfig.LoadDeviceTypeLookup("configuration/TOSA Final/" + "DeviceTypeLookup.csv");
            if ((deviceTypeInfo == null) || (deviceTypeInfo.Count < 1))
            {
                testEngine.ErrorRaise(
                    " can't find config file to provide device type and Chirp type mapping message");
                return null;
            }
            while (true)
            {
                // SHOW THE USER CONTROL

                testEngine.PageToFront(TestControlCtl.TestControl);
                if (isMesOnline)
                {

                    testEngine.SelectTestControlCtlToDisplay(typeof(LoadBatchDialogueCtl));
                    //testEngine.SendToCtl(TestControlCtl.TestControl, typeof(LoadBatchDialogueCtl),
                    //new MesLoadDeviceMsg("Please input a batch to load", true, true));
                }// MES ON LINE
                else
                {
                    testEngine.SelectTestControlCtlToDisplay(typeof(ManualLoadBatchDialogueCtl));
                    testEngine.SendToCtl(TestControlCtl.TestControl,
                        typeof(ManualLoadBatchDialogueCtl), deviceTypeInfo);
                    
                }// MES NOT ON LINE
                testEngine.GetUserAttention(TestControlCtl.TestControl);

                // wait for the message to say "OK" has been clicked...
                CtlMsg msg = testEngine.WaitForCtlMsg();
                this.DutInfo = (DutLoadInfo)msg.Payload;
                // if the GUI Msg is not available dut information, reload batch
                if (this.DutInfo == null)
                {
                    testEngine.SendStatusMsg(" invalid batch information!\n reloading batch information ...");
                    continue;
                }

                if (!isMesOnline)
                {
                    break;
                }// mes not no line
                else
                {
                    try
                    {
                        this.mes.Node = node;
                        MESbatch dutBatch = mes.LoadBatch(DutInfo.DutSerialNumber, true);
                        if (dutBatch[0] != null)
                        {// if batch information has been retrieve, process the information
                            DutInfo.PartCode = dutBatch.PartCode;
                            mes.TrackIn(DutInfo.DutSerialNumber);

                            // if tha batch is not available for stage deliver, promote to reload batch
                            if (dutBatch[0].TestStage.Trim() != "final")   // may be can set it as string "stage in factory work"in configration file
                            {
                                testEngine.SendStatusMsg("Batch is in incorrected stage in Mes");
                                testEngine.ShowUserQuery(TestControlTab.TestControl,
                                    " the batch atay in stage of " + dutBatch[0].TestStage +
                                    " \n so it can't process in data deliver stage, please reload batch!",
                                    new Bookham.TestEngine.PluginInterfaces.TestControl.ButtonInfo("&Continue", null));
                                testEngine.SendToCtl(TestControlCtl.TestControl,
                                    typeof(LoadBatchDialogueCtl),
                                    new MesLoadDeviceMsg("Please input a batch to load", true, true));
                                continue;
                            }
                            if (deviceTypeInfo.ContainsKey(DutInfo.PartCode))
                            {
                                DutInfo.ChirpType = deviceTypeInfo[DutInfo.PartCode].ChirpType;
                                break;
                            }
                            else
                            {
                                testEngine.ErrorRaise(" can't find device type & chirp type" +
                                    "mapping message for " +  DutInfo.PartCode + 
                                        " \n please check the device type & chirp type " +
                                        "message mapping configuration file and ensure " + 
                                        " this device type is available to test with this test control");
                                continue;
                            }
                        }// if available mes information has been achieve
                        else
                        {
                            continue;   // if no available dut in batch, go on batch load loop
                        }

                    }// try to retrieve mes infromation                    

                    catch (MESTimeoutException)
                    {
                        // Log the fact that there was an MES Timeout exception.
                        testEngine.SendStatusMsg("MES Load Batch Operation timedout.");

                        //Just start over.
                        continue;
                    }
                    catch (MESInvalidOperationException e)
                    {
                        // Log the fact that there was an MES Invalid operation exception.
                        testEngine.SendStatusMsg("MES Invalid operation exception." + e.Message + e.StackTrace);


                        //Just start over.
                        continue;
                    }
                    catch (MESCommsException e)
                    {
                        testEngine.SendStatusMsg("MES Communications exception." + e.Message + e.StackTrace);

                        //Just start over.
                        continue;
                    }
                }   // mes on line

            }  // while ( true) to get available batch information

            // ... Clear up GUI.
            testEngine.CancelUserAttention(TestControlCtl.TestControl);
            testEngine.GuiClear(TestControlCtl.TestControl);
            // Reset one-time run flag
            programRanOnce = false;
            // Return serial number to Test Engine

            testEngine.SendStatusMsg("Batch load completed!");
            return DutInfo.DutSerialNumber;
        }
        /// <summary>
        /// Get the next available DUT from the Batch.
        /// </summary>
        /// <param name="testEngine">Object that implements the ITestEngine Interface.</param>
        /// <param name="testJigArg">Object tha implements ITestJig Interface.</param>
        /// <param name="operatorName">The name of the currently logged in user.</param>
        /// <param name="operatorType">Privilege level of the currently logged in user.</param>
        /// <returns>DUT object for the next selected device for test or Null if end of batch.</returns>
        public DUTObject GetNextDUT(ITestEngine testEngine, ITestJig testJigArg, 
                    string operatorName, TestControlPrivilegeLevel operatorType)
        {
            if (this.programRanOnce) return null; // end of batch after one run!
            testEngine.SelectBatchCtlToDisplay(typeof(BatchViewCtl));
            testEngine.PageToFront(TestControlCtl.BatchView);

            //Comstruct DUTObject - test engine get required program, serial no etc. from here
            dut = new DUTObject();
            dut.BatchID = DutInfo.DutSerialNumber;  //from Load Batch
            dut.SerialNumber = DutInfo.DutSerialNumber;

            // Set results node
            dut.NodeID = this.node;
            // Get Test Jig ID
            dut.TestJigID = testJigArg.GetJigID(testEngine.InDebugMode, testEngine.InSimulationMode);

            // TODO: CHANGE THE FOLLOWING LINES FOR REQUIRED PROGRAM AND DEVICE
            
            //dut.PartCode = "PA006435";
            dut.PartCode = DutInfo.PartCode;
            dut.TestStage = DutInfo .TestStage;
            
            // Select program plugin depends on chirp type
            dut.ProgramPluginName = "Prog_ILMZ_Final_SPC";
            /*switch (DutInfo.ChirpType)
            {
                case "ND":
                    if (dut.TestStage == "final")
                        dut.ProgramPluginName = "Prog_ILMZ_Final_ND";
                    else if (dut.TestStage == "qual" || dut .TestStage == "qual0")
                        dut.ProgramPluginName = "Prog_ILMZ_Qual_ND";
                    break;
                case "ZD":
                    dut.ProgramPluginName = "";
                    break;
                default:
                    testEngine.ErrorRaise("Unhandled device type '" + DutInfo .ChirpType + "'");
                    break;    
            }*/

            dut.ProgramPluginVersion = "";
            dut.Attributes.Add(new DatumBool("IsUseMes", isMesOnline));
            //dut.Attributes.AddString("OPERATOR_ID", this.operatorID);

            testEngine.GetUserAttention(TestControlCtl.BatchView);
            testEngine.SendToCtl(TestControlCtl.BatchView, typeof(BatchViewCtl), dut);
            
            CtlMsg msg = testEngine.WaitForCtlMsg();
            testEngine.CancelUserAttention(TestControlCtl.BatchView);

            DutTestOption testOption = (DutTestOption)msg.Payload;
            if (testOption == null) return null;

            // only allowed though here once!
            if (testOption.TestOption == enmTestOption.GoOnTest)
            {
                this.programRanOnce = true;
                return dut;
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// Called after the test program has concluded, to decide what the outgoing 
        /// Part ID and other attributes are, having inspected the test results.
        /// </summary>
        /// <param name="testEngine">Reference to object implmenting ITestEngine</param>
        /// <param name="programStatus">The Status of the Test Program Run (succeeded/error/aborted etc).</param>
        /// <param name="testResults">Detailed test results</param>
        /// <returns>Outcome of the results analysis.</returns>
        public DUTOutcome PostDevice(ITestEngine testEngine, ProgramStatus programStatus, TestResults testResults)
        {
            //TODO: Modify DUTOutcome object, 
            //If required: add details based on analysis of the test results, specify 
            //what specifications the test results are to be stored against plus any other supporting data.
            this.programStatus = programStatus;
            int numberOfSupermodes = int.MaxValue;
            string smSpecNameStub = "";

            if (dut.Attributes.IsPresent("NumberOfSupermodes"))
            {
                numberOfSupermodes = dut.Attributes.ReadSint32("NumberOfSupermodes");
            }
            if (dut.Attributes.IsPresent("SupermodeSpecNameStub"))
            {
                smSpecNameStub = dut.Attributes.ReadString("SupermodeSpecNameStub");
            }

            List<String> specsToUse = new List<String>();
            foreach (SpecResults specRes in testResults.SpecResults)
            {
                string specName = specRes.Name;
                if (smSpecNameStub.Length > 0 && specName.Contains(smSpecNameStub) && specName.Contains("_"))
                {
                    // It's a supermode spec. Should we add it ?
                    int lastUnderscore = specName.LastIndexOf("_");
                    int smNumber = int.Parse(specName.Substring(lastUnderscore + 1, specName.Length - lastUnderscore - 1));
                    // If the spec is less than the supermode count we can add it
                    if (smNumber < numberOfSupermodes)
                        specsToUse.Add(specName);
                }
                else if (!specName.ToLower().Contains("dummyspec"))
                {
                    specsToUse.Add(specName);
                }
            }

            DUTOutcome dutOutcome = new DUTOutcome();
            dutOutcome.OutputSpecificationNames = specsToUse.ToArray();
            return dutOutcome;
        }

        /// <summary>
        /// Commit any post-test MES updates prepared in method PostDevice.
        /// </summary>
        /// <param name="testEngine">Object implementing ITestEngine</param>
        /// <param name="dataWriteError">True if there was a problem writing Test Results to the database.</param>
        public void PostDeviceCommit(ITestEngine testEngine, bool dataWriteError)
        {
           /* //TODO: For those Test Software Solutions which interact with an MES, store the test
            //status for the current device under test to the MES.
            testEngine.SendStatusMsg("Post Device Commit");
            ////Mark the Device as Tested. Also record it's pass fail status.
            if ((this.programStatus == ProgramStatus.Success) && (!dataWriteError))
            {
                //The Test Passed. Mark the device as completed.
                //testStatuses[currentDutIndex] = TestStatus.Passed;
            }
            //else if ((this.programStatus == ProgramStatus.Success) && (dataWriteError))
            //{
            //    //Problem writing Test Data. Mark for retest.
            //    testStatuses[currentDutIndex] = TestStatus.MarkForRetest;
            //}
            else if (this.programStatus == ProgramStatus.Failed ||
                     this.programStatus == ProgramStatus.NonParametricFailure)
            // We only want to condemn a device which is definitely duff, so assume all
            // other statuses leave the device as "untested".
            {
                //testStatuses[currentDutIndex] = TestStatus.Failed;

                //Update the Component level MES Data for the device if not in Simulation mode.
                if (!inSimulationMode)
                {
                    mes.SetComponentAttribute(dut .BatchID ,dut .SerialNumber , dut .PartCode, "FAILED");
                }
            }
            //else
            //{
            //    testStatuses[currentDutIndex] = TestStatus.Untested;
            //}

            //UpdateTestStatuses message = new UpdateTestStatuses(testStatuses);
            //testEngine.SendToCtl(TestControlCtl.BatchView, typeof(DemoBatchViewCtl), message);*/
        }

        /// <summary>
        /// The Test Engine has concluded operations on the currently loaded batch. Update Batch level parameters 
        /// in the MES.
        /// </summary>
        /// <param name="testEngine">Reference to object that implements the ITestEngine Interface.</param>
        public void PostBatch(ITestEngine testEngine)
        {
            /*//TODO: For those Test Software Solutions which interact with an MES, store the test
            //status for the currently loaded Batch in the MES.
            testEngine.SendStatusMsg("Post Batch");

            //Trackout the Batch if we are not in Simulation Mode.
            if ((!this.inSimulationMode) && (isDutTrackOut))
            {
                mes.TrackOutBatchPass(dut .BatchID, dut.TestStage , "Passed");
            }*/
        }

        /// <summary>
        /// Mark all devices in the Batch for retest.
        /// </summary>
        /// <param name="testEngine">Reference to object that implements the ITestEngine Interface.</param>
        public void RestartBatch(ITestEngine testEngine)
        {

            //TODO: Use whatever method is appropriate to your implmentation to mark all devices in the 
            //current batch untested. It is up to the implmenter to decide whether this means restart all 
            //devices tested in this session or whether to restart the entire batch. Where this functionality
            //is not required, the solution developer may choose to take no action here.
            testEngine.SendToCtl(TestControlCtl.BatchView, typeof(BatchViewCtl), DutInfo);
            testEngine.SendStatusMsg("Restart Batch");           
            
            
        }
        # endregion

        private void readTestControlConfig(ITestEngine engine)
        {
            if (isTCPliginConfigRead)
                return;

            int result;
            string commonErrorMessageString = "Unable to read Test Control Plug-in configuration File: ";

            //Read Configuration file.
            /* Open user data XML file. This constructor call only conducts a scant validation of the
             * file name. Any expected error will not occur until the first call to Read. */
            XmlTextReader config = new XmlTextReader(CONFIGFilePath + "/TestControlPluginConfig.xml");

            try
            {
                /* Scan file, one element a time. */
                while (config.Read())
                {
                    if (config.NodeType == XmlNodeType.Element)
                    {
                        if (config.LocalName == "UseMES")
                        {
                            string useMes = config.ReadString();

                            result = String.Compare(useMes.Trim(), "yes", true, System.Globalization.CultureInfo.InvariantCulture);
                            if (result == 0)
                            {
                                isMesOnline = true;
                            }
                            else
                            {
                                result = String.Compare(useMes.Trim(), "no", true, System.Globalization.CultureInfo.InvariantCulture);

                                if (result == 0)
                                {
                                    isMesOnline = false;
                                }
                                else
                                {
                                    //Invalid configuration file contents.
                                    engine.ErrorRaise("Invalid UseMES field contents in Configuration/ITLA/TestControlPluginConfig.xml configuration file.");
                                }
                            }  // end else
                        }  // end if "UseMes"

                        if (config.LocalName == "InhibitTrackout")
                        {
                            string trackoutInhibitString = config.ReadString();

                            result = String.Compare(trackoutInhibitString.Trim(), "yes", true);
                            if (result == 0)
                            {
                                isDutTrackOut = false;
                            }
                            else
                            {
                                result = String.Compare(trackoutInhibitString.Trim(), "no", true);

                                if (result == 0)
                                {
                                    isDutTrackOut = true;
                                }
                                else
                                {
                                    //Invalid configuration file contents.
                                    engine.ErrorRaise("Invalid InhibitTrackout field contents in Configuration/ITLA/TestControlPluginConfig.xml configuration file.");
                                }
                            }  // end else: "InhibitTrackout" != "yes"
                        }  // end if "InhibitTrackout"
                    } // end if config.NodeType == XmlNodeType.Element: get available text nod
                }  // end while loop

                // Set flag to indicate that configuration has been read.
                isTCPliginConfigRead = true;
            }
            catch (UnauthorizedAccessException uae)
            {

                /* User did not have read permission for file. */
                engine.ErrorRaise(commonErrorMessageString + "Access denied " + uae.Message + uae.StackTrace);
            }
            catch (System.IO.FileNotFoundException fnfe)
            {
                /* File not found error. */
                engine.ErrorRaise(commonErrorMessageString + "Configuration File not found " + fnfe.Message);
            }
            catch (System.IO.IOException ioe)
            {
                /* General IO failure. Not file not found. */
                engine.ErrorRaise(commonErrorMessageString + "General IO Failure " + ioe.Message);
            }
            catch (System.Xml.XmlException xe)
            {
                /* XML parse error. */
                engine.ErrorRaise(commonErrorMessageString + "XMl Parser Error " + xe.Message);
            }
            finally
            {
                /* Close file. */
                if (config != null) config.Close();
            }
        }
        #region Private data
        // Batch View tab controls
        Type[] batchCtlList;
        // Test Control tab controls
        Type[] testCtlList;

        // Flag for the program ran (so we can abort the batch)
        bool programRanOnce = false;
        // MES / Results database (PCAS) Node
        int node;
        // Serial number variable
        DutLoadInfo DutInfo;
        /// <summary>
        /// flag to indicate if test control configureation file was read or not
        /// </summary>
        bool isTCPliginConfigRead;
        /// <summary>
        /// Flag to indicate if Fws is available or not
        /// </summary>
        bool isMesOnline;
        /// <summary>
        /// Flag to indicate if dut trackout after test
        /// </summary>
        bool isDutTrackOut;
        IMES mes;

        string operatorID;

        ProgramStatus programStatus;
        bool inSimulationMode;
        const string CONFIGFilePath = "configuration/TOSA Final/";
        DUTObject dut;
        #endregion

    }
}

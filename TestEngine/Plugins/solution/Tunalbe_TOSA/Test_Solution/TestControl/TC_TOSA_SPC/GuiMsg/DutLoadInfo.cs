using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestSolution.TestControl
{
    internal  class DutLoadInfo
    {
        string partCode;

        public string PartCode
        {
            get { return partCode; }
            set { partCode = value; }
        }
        string dutSerialNumber;

        public string DutSerialNumber
        {
            get { return dutSerialNumber; }
            set { dutSerialNumber = value; }
        }
        string chirpType;

        public string ChirpType
        {
            get { return chirpType; }
            set { chirpType = value; }
        }
        

        private string testStage;

        public string TestStage
        {
            get { return testStage; }
            set { testStage = value; }
        }
    }
}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestControl
//
// LoadBatchDialogueCtl.cs
//
// Author: tommy.yu, 2007
// Design: [Reference design documentation

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.PluginInterfaces.MES;
using Bookham.TestEngine.Framework.InternalData;


namespace Bookham.TestSolution.TestControl
{
    /// <summary>
    /// Test Control Plug-in Template Batch View User Control
    /// </summary>
    public partial class LoadBatchDialogueCtl : Bookham.TestEngine.Framework.Messages.ManagedCtlBase
    {

        /// <summary>
        /// Constructor
        /// </summary>
        public LoadBatchDialogueCtl()
        {
            /* Call designer generated code. */
            InitializeComponent();
            this.loadBatchTextBox.CharacterCasing = CharacterCasing.Upper;
            
        }

        /// <summary>
        /// Process recieved messages from the worker thread.
        /// </summary>
        /// <param name="payload">Message Payload</param>
        /// <param name="inMsgSeq">Incoming sequence number</param>
        /// <param name="respSeq">Response sequence number</param>
        private void LoadBatchDialogueCtl_MsgReceived(object payload, long inMsgSeq, long respSeq)
        {
            if (payload.GetType().Equals(typeof(DatumStringArray)))
            {
                DatumStringArray sa = (DatumStringArray)payload;

                if (sa.Name == "typeList")
                {
                    this.cmbDeviceType.DataSource = sa.ValueCopy();
                    this.cmbDeviceType.SelectedItem = null;
                }
                else if(sa.Name == "typeListPA0010199")
                {
                
                    this.cmbDeviceType.DataSource = sa.ValueCopy();
                }

            }
            //TODO: Process messages from the Test Control Plug-in worker thread.
            else if (payload.GetType().Equals(typeof(GuiMsgEnum)))
            {
                GuiMsgEnum gm = (GuiMsgEnum)payload;
                switch (gm)
                {
                    case GuiMsgEnum.LoadBatchRequest:

                        displayControls();

                        this.messageLabel.Text = "Please input a batch to load";
                        this.cmbDeviceType.Enabled = true;
                        this.loadBatchButton.Enabled = true;
                        this.Button_NoMes.Enabled = true;
                        this.loadBatchTextBox.Enabled = true;
                        this.loadBatchTextBox.SelectAll();
                        this.loadBatchTextBox.Focus();
                        break;
                    case GuiMsgEnum.LoadBatchComplete:
                        // The batch has been loaded. Hide the controls and prepare for the 
                        // next Load Batch operation.
                        this.loadBatchTextBox.Text = "";
                        this.messageLabel.Text = "";
                        hideControls();
                        break;

                }
            }
            else if (payload.GetType().Equals(typeof(DatumString)))
            {
                DatumString ds = (DatumString)payload;
                switch (ds.Name)
                {
                    case "PA010199":
          
                        for (int i = 0; i < this.cmbDeviceType.Items.Count; i++)
                        {
                            if (this.cmbDeviceType.Items[i].Equals(ds.Value))
                            {
                                this.cmbDeviceType.Items.RemoveAt(i);
                            }
                        }
                            
                        //this.cmbDeviceType.Items.Remove(ds.Value);
                        break;
                    case "PartCode":
                        this.messageLabel.Text = string.Format("The device type is {0} in MES.", ds.Value);
                        this.cmbDeviceType.SelectedItem = ds.Value;
                        this.cmbDeviceType.Enabled = true;
                        this.loadBatchButton.Enabled = true;
                        break;
                    case "RetryAbortRequest":
                        // Disable the Load Batch Controls
                        disableLoadBatchControls();

                        // Display the error message.
                        this.messageLabel.Text = ds.Value;
                        break;
                    default:
                        break;
                }
            }
            else if (payload is string)
            {
                messageLabel.Text = payload.ToString();
            }
            else
            {
                //Error - invalid message.
                System.Diagnostics.Trace.Write("Warning unknown messaage type " + payload.GetType());
            }
        }

        /// <summary>
        /// The User clicked the load batch button. Send a message to the worker thread, with the 
        /// user's desired Batch ID.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">event parameters.</param>
        private void loadBatchButton_Click(object sender, System.EventArgs e)
        {
            // Validate the part code. - chongjian.liang 2016.2.22

            string partCode = this.cmbDeviceType.SelectedItem as string;

            if (string.IsNullOrEmpty(partCode))
            {
                MessageBox.Show("Please select a device type.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                this.loadBatchButton.Enabled = false;

                this.sendToWorker(partCode);
            }
        }

        /// <summary>
        /// Check and load the device type for the inputted serial number - chongjian.liang 2016.02.22
        /// </summary>
        private void CmbDeviceType_Enter(object sender, EventArgs e)
        {
            if (this.cmbDeviceType.SelectedItem == null)
            {
                string batchID = loadBatchTextBox.Text.Trim().ToUpper();
                
                // Load RMA SN
                if (TC_Common.RMA_SNList != null
                    && TC_Common.RMA_SNList.Contains(batchID))
                {
                    this.sendToWorker(new Msg_NoMesRequestWithSN(batchID));
                }
                else // Load Batch ID
                {
                    if (!batchID.StartsWith("HG", true, null))
                    {
                        MessageBox.Show("Batch ID must start with \"HG\".");

                        this.loadBatchTextBox.Focus();
                    }
                        /*  delete by dong.chen 20170728
                    else if (!batchID.EndsWith(".1", true, null))
                    {
                        MessageBox.Show("Batch ID must end with \".1\".");

                        this.loadBatchTextBox.Focus();
                    }*/
                    else if (batchID.Length != 10)
                    {
                        MessageBox.Show("Batch ID must contain exact 10 characters.");

                        this.loadBatchTextBox.Focus();
                    }
                    else
                    {
                        this.messageLabel.Text = "Please wait while retrieving device type from MES ...";
                        this.cmbDeviceType.Text = "Please wait..";
                        this.cmbDeviceType.Enabled = false;
                        this.loadBatchTextBox.Enabled = false;
                        this.loadBatchButton.Enabled = false;
                        this.Button_NoMes.Enabled = false;

                        this.sendToWorker(new DatumString("batchID", loadBatchTextBox.Text));
                    }
                }
            }
        }

        /// <summary>
        /// Dispay normal load batch screen controls.
        /// </summary>
        private void displayControls()
        {
            this.loadBatchScreenLabel.Show();
            this.bookhamLogoPictureBox.Show();
            showLoadBatchControls();
        }

        /// <summary>
        /// Hide All  Controls
        /// </summary>
        private void hideControls()
        {
            this.loadBatchScreenLabel.Hide();
            this.bookhamLogoPictureBox.Hide();
            hideLoadBatchControls();
        }

        /// <summary>
        /// Hide Load Batch operation associated controls.
        /// </summary>
        private void hideLoadBatchControls()
        {
            this.loadBatchTextBox.Hide();
            this.loadBatchButton.Hide();
            this.messageLabel.Hide();
        }

        /// <summary>
        /// Show Load Batch operation associated controls.
        /// </summary>
        private void showLoadBatchControls()
        {
            this.loadBatchTextBox.Show();
            this.loadBatchButton.Show();
            this.messageLabel.Show();
        }

        /// <summary>
        /// Disable Load Batch operation associated controls.
        /// </summary>
        private void disableLoadBatchControls()
        {
            this.loadBatchTextBox.Enabled = false;
            this.loadBatchButton.Enabled = false;
        }

        /// <summary>
        /// Enable Load Batch operation associated controls.
        /// </summary>
        private void enableLoadBatchControls()
        {
            this.loadBatchTextBox.Enabled = true;
            this.loadBatchButton.Enabled = true;
        }


        /// <summary>
        /// User clicks the retry button. Send a retry message to the worker thread and resume 
        /// displaying the normal load batch screen.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Event parameters</param>
        private void retryButton_Click(object sender, System.EventArgs e)
        {
            // Message the worker thread.
            //this.sendToWorker(QueryRetryAbort.Retry);
            //MessageClass.SendToWorker(QueryRetryAbort.Retry);

            //Enable Load Batch Controls
            enableLoadBatchControls();

            // Remove the error message.
            this.messageLabel.Text = "";
        }


        /// <summary>
        /// User clicks the abort button. Send a abort message to the worker thread and resume 
        /// displaying the normal load batch screen.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Event parameters</param>
        private void abortButton_Click(object sender, System.EventArgs e)
        {
            // Message the worker thread.
            //this.sendToWorker(QueryRetryAbort.Abort);
            //MessageClass.SendToWorker(QueryRetryAbort.Abort);

            //Enable Load Batch Controls
            enableLoadBatchControls();

            // Remove the error message.
            this.messageLabel.Text = "";

            // Prompt the user
            this.loadBatchTextBox.Text = "";

            this.loadBatchButton.Enabled = true;
        }

        private void loadBatchTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!string.IsNullOrEmpty(loadBatchTextBox.Text.Trim()) && e.KeyChar == '\r')
                if (!loadBatchButton.Focused) loadBatchButton.Focus();

        }

        private void LoadBatchDialogueCtl_VisibleChanged(object sender, EventArgs e)
        {
            if (Visible && !loadBatchTextBox.Focused)
            {
                loadBatchTextBox.Focus();
            }
        }

        private void loadBatchTextBox_Enter(object sender, EventArgs e)
        {
            loadBatchTextBox.SelectAll();
        }

        private void Button_NoMes_Click(object sender, EventArgs e)
        {
            sendToWorker(new Msg_NoMesRequest());
        }

    }
}
      

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestControl
//
// LoadBatchDialogueCtl.Designer.cs
//
// Author: tommy.yu, 2007
// Design: [Reference design documentation]

namespace Bookham.TestSolution.TestControl
{
    partial class LoadBatchDialogueCtl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoadBatchDialogueCtl));
            this.loadBatchScreenLabel = new System.Windows.Forms.Label();
            this.bookhamLogoPictureBox = new System.Windows.Forms.PictureBox();
            this.loadBatchButton = new System.Windows.Forms.Button();
            this.loadBatchTextBox = new System.Windows.Forms.TextBox();
            this.Button_NoMes = new System.Windows.Forms.Button();
            this.lblPartCode = new System.Windows.Forms.Label();
            this.cmbDeviceType = new System.Windows.Forms.ComboBox();
            this.lblBatchID = new System.Windows.Forms.Label();
            this.messageLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.bookhamLogoPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // loadBatchScreenLabel
            // 
            this.loadBatchScreenLabel.AutoSize = true;
            this.loadBatchScreenLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadBatchScreenLabel.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.loadBatchScreenLabel.Location = new System.Drawing.Point(191, 23);
            this.loadBatchScreenLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.loadBatchScreenLabel.Name = "loadBatchScreenLabel";
            this.loadBatchScreenLabel.Size = new System.Drawing.Size(506, 25);
            this.loadBatchScreenLabel.TabIndex = 4;
            this.loadBatchScreenLabel.Text = "Test Engine TestControl - Map: Load Batch Screen";
            // 
            // bookhamLogoPictureBox
            // 
            this.bookhamLogoPictureBox.Image = ((System.Drawing.Image)(resources.GetObject("bookhamLogoPictureBox.Image")));
            this.bookhamLogoPictureBox.Location = new System.Drawing.Point(9, 9);
            this.bookhamLogoPictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.bookhamLogoPictureBox.Name = "bookhamLogoPictureBox";
            this.bookhamLogoPictureBox.Size = new System.Drawing.Size(129, 44);
            this.bookhamLogoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.bookhamLogoPictureBox.TabIndex = 10;
            this.bookhamLogoPictureBox.TabStop = false;
            // 
            // loadBatchButton
            // 
            this.loadBatchButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadBatchButton.Location = new System.Drawing.Point(9, 161);
            this.loadBatchButton.Margin = new System.Windows.Forms.Padding(4);
            this.loadBatchButton.Name = "loadBatchButton";
            this.loadBatchButton.Size = new System.Drawing.Size(256, 55);
            this.loadBatchButton.TabIndex = 2;
            this.loadBatchButton.Text = "Load Batch";
            this.loadBatchButton.Click += new System.EventHandler(this.loadBatchButton_Click);
            // 
            // loadBatchTextBox
            // 
            this.loadBatchTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadBatchTextBox.Location = new System.Drawing.Point(9, 97);
            this.loadBatchTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.loadBatchTextBox.Name = "loadBatchTextBox";
            this.loadBatchTextBox.Size = new System.Drawing.Size(256, 34);
            this.loadBatchTextBox.TabIndex = 0;
            this.loadBatchTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.loadBatchTextBox_KeyPress);
            this.loadBatchTextBox.Enter += new System.EventHandler(this.loadBatchTextBox_Enter);
            // 
            // Button_NoMes
            // 
            this.Button_NoMes.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_NoMes.Location = new System.Drawing.Point(9, 237);
            this.Button_NoMes.Margin = new System.Windows.Forms.Padding(4);
            this.Button_NoMes.Name = "Button_NoMes";
            this.Button_NoMes.Size = new System.Drawing.Size(256, 55);
            this.Button_NoMes.TabIndex = 3;
            this.Button_NoMes.Text = "No MES";
            this.Button_NoMes.Visible = false;
            this.Button_NoMes.Click += new System.EventHandler(this.Button_NoMes_Click);
            // 
            // lblPartCode
            // 
            this.lblPartCode.AutoSize = true;
            this.lblPartCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblPartCode.Location = new System.Drawing.Point(309, 68);
            this.lblPartCode.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPartCode.Name = "lblPartCode";
            this.lblPartCode.Size = new System.Drawing.Size(128, 25);
            this.lblPartCode.TabIndex = 15;
            this.lblPartCode.Text = "Device Type:";
            // 
            // cmbDeviceType
            // 
            this.cmbDeviceType.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cmbDeviceType.FormattingEnabled = true;
            this.cmbDeviceType.Location = new System.Drawing.Point(314, 97);
            this.cmbDeviceType.Margin = new System.Windows.Forms.Padding(4);
            this.cmbDeviceType.Name = "cmbDeviceType";
            this.cmbDeviceType.Size = new System.Drawing.Size(256, 33);
            this.cmbDeviceType.TabIndex = 1;
            this.cmbDeviceType.Enter += new System.EventHandler(this.CmbDeviceType_Enter);
            // 
            // lblBatchID
            // 
            this.lblBatchID.AutoSize = true;
            this.lblBatchID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblBatchID.Location = new System.Drawing.Point(10, 68);
            this.lblBatchID.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblBatchID.Name = "lblBatchID";
            this.lblBatchID.Size = new System.Drawing.Size(92, 25);
            this.lblBatchID.TabIndex = 16;
            this.lblBatchID.Text = "Batch ID:";
            // 
            // messageLabel
            // 
            this.messageLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.messageLabel.ForeColor = System.Drawing.Color.Red;
            this.messageLabel.Location = new System.Drawing.Point(309, 161);
            this.messageLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.messageLabel.Name = "messageLabel";
            this.messageLabel.Size = new System.Drawing.Size(726, 55);
            this.messageLabel.TabIndex = 17;
            this.messageLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // LoadBatchDialogueCtl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.messageLabel);
            this.Controls.Add(this.lblBatchID);
            this.Controls.Add(this.lblPartCode);
            this.Controls.Add(this.cmbDeviceType);
            this.Controls.Add(this.Button_NoMes);
            this.Controls.Add(this.loadBatchScreenLabel);
            this.Controls.Add(this.bookhamLogoPictureBox);
            this.Controls.Add(this.loadBatchButton);
            this.Controls.Add(this.loadBatchTextBox);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "LoadBatchDialogueCtl";
            this.Size = new System.Drawing.Size(1056, 384);
            this.VisibleChanged += new System.EventHandler(this.LoadBatchDialogueCtl_VisibleChanged);
            this.MsgReceived += new Bookham.TestEngine.Framework.Messages.ManagedCtlBase.MsgReceivedDlg(this.LoadBatchDialogueCtl_MsgReceived);
            ((System.ComponentModel.ISupportInitialize)(this.bookhamLogoPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label loadBatchScreenLabel;
        private System.Windows.Forms.PictureBox bookhamLogoPictureBox;
        private System.Windows.Forms.Button loadBatchButton;
        private System.Windows.Forms.TextBox loadBatchTextBox;
        private System.Windows.Forms.Button Button_NoMes;
        private System.Windows.Forms.Label lblPartCode;
        private System.Windows.Forms.ComboBox cmbDeviceType;
        private System.Windows.Forms.Label lblBatchID;
        private System.Windows.Forms.Label messageLabel;



    }
}

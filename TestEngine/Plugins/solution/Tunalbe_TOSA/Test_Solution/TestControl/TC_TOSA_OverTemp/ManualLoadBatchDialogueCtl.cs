// [Copyright]
//
// Bookham Test Engine
// TODO - Full name of assembly
//
// TC_Txp40GDataDeliver/ManualLoadBatchDialogueCtl.cs
// 
// Author: alice.huang
// Design: TODO Title of design document

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Bookham.TestSolution.IlmzCommonUtils;

namespace Bookham.TestSolution.TestControl
{
    public partial class ManualLoadBatchDialogueCtl : Bookham.TestEngine.Framework.Messages.ManagedCtlBase
    {
        /// <summary>
        /// constructor
        /// </summary>
        public ManualLoadBatchDialogueCtl()
        {
            /* Call designer generated code. */
            InitializeComponent();

            /* TODO - Write constructor code here. */
        }
        /// <summary>
        /// initialise controls on gui when load
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.txtSN.Focus();
        }

        private void btnLoadBatch_Click(object sender, EventArgs e)
        {
            DutLoadInfo dutInfo = new DutLoadInfo();

            // Load serial number - chongjian.liang 2013.11.20

            string serialNO = this.txtSN.Text.Trim();

            if (string.IsNullOrEmpty(serialNO))
            {
                MessageBox.Show("Please input a serial number.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.txtSN.Focus();
                return;
            }
            else
            {
                dutInfo.DutSerialNumber = serialNO;
            }

            // Load device type - chongjian.liang 2013.11.20

            if (this.DeviceTypeCombo.SelectedItem == null)
            {
                MessageBox.Show("Please select a device type.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            else
            {
                DeviceTypeLookupItem selectedDeviceType = (DeviceTypeLookupItem)this.DeviceTypeCombo.SelectedItem;

                dutInfo.PartCode = selectedDeviceType.DeviceType;
                dutInfo.ChirpType = selectedDeviceType.ChirpType;
            }

            // Send dut info - chongjian.liang 2013.11.20

            sendToWorker(dutInfo);
        }

        private void ManualLoadBatchDialogueCtl_MsgReceived(object payload, long inMsgSeq, long respSeq)
        {
            // Only present the device type from previous stage's result - chongjian.liang 2013.11.20
            if (payload is DeviceTypeLookupItem)
            {
                if (this.DeviceTypeCombo.Items.Count == 0)
                {
                    this.DeviceTypeCombo.Items.Add(payload);
                    this.DeviceTypeCombo.DisplayMember = "DeviceType";
                }

                this.DeviceTypeCombo.SelectedItem = payload;
                this.DeviceTypeCombo.Enabled = true;
            }
            else if (payload is string)
            {
                string msg = payload as string;

                // Alert the user to input the correct serial number - chongjian.liang 2013.11.20
                if (msg == "LastStageMissing" || msg == "LastStageFailed")
                {
                    this.DeviceTypeCombo.Text = "";
                    this.DeviceTypeCombo.Enabled = true;
                    this.txtSN.Enabled = true;

                    if (msg == "LastStageMissing")
                    {
                        MessageBox.Show(string.Format("Missing final result for serial number \"{0}\".", this.txtSN.Text));
                    }
                    else if (msg == "LastStageFailed")
                    {
                        MessageBox.Show(string.Format("Final result is failed for serial number \"{0}\".", this.txtSN.Text));
                    }

                    this.txtSN.Focus();
                    this.txtSN.SelectAll();
                }
            }
            else if (payload is Msg_NoMesRequestWithSN)
            {
                this.txtSN.Text = (payload as Msg_NoMesRequestWithSN).SerialNO;
                this.txtSN.Enabled = false;
            }
        }

        /// <summary>
        /// Check and load the device type for the inputted serial number. - chongjian.liang 2013.11.20
        /// </summary>
        private void DeviceTypeCombo_Enter(object sender, EventArgs e)
        {
            if (this.DeviceTypeCombo.SelectedItem == null)
            {
                if (string.IsNullOrEmpty(this.txtSN.Text.Trim()))
                {
                    MessageBox.Show("Please input a serial number.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    this.txtSN.Focus();
                    this.txtSN.SelectAll();
                }
                else
                {
                    this.DeviceTypeCombo.Text = "Please wait..";
                    this.DeviceTypeCombo.Enabled = false;
                    this.txtSN.Enabled = false;

                    string msg = this.txtSN.Text.Trim();

                    sendToWorker(msg);
                }
            }
        }
    }
}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestControl
//
// TC_TOSA_FINAL.cs
//
// Author: alice.huang, 2010
// Design: [Reference design documentation]

using System;
using System.Xml;
using System.Collections;
using System.Collections.Generic;
using Bookham.TestEngine.Framework.Exceptions;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.PluginInterfaces.TestControl;
using Bookham.TestEngine.PluginInterfaces.TestJig;
using Bookham.TestEngine.PluginInterfaces.MES;
using Bookham.TestEngine.PluginInterfaces.ExternalData;
using Bookham.TestEngine.PluginInterfaces.Security;
using System.Collections.Specialized;
using Bookham.TestSolution.IlmzCommonUtils;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Framework.Limits;
using System.IO;

namespace Bookham.TestSolution.TestControl
{
    /// <summary>
    /// Test Control Plug-in Template.
    /// NB: Implements a Test Control with a single device in the batch.
    /// The following parameters are hardcoded in the DUTObject and will need to be changed,
    /// either by replacement value or by reading the MES:
    /// - ProgramPluginName and Version
    /// - PartCode
    /// - TestStage
    /// </summary>
    public class TC_TOSA_OverTemp : ITestControl
    {
        #region Constructor
        /// <summary>
        /// Constructor. 
        /// </summary>
        public TC_TOSA_OverTemp()
        {
            // initialise the Batch View tab control array (private variable)
            this.batchCtlList = new Type[1];
            this.batchCtlList[0] = typeof(BatchViewCtl);  // show dut infomation
            // TODO: Add any new controls here

            // initialise the Test Control tab control array (private variable)
            this.testCtlList = new Type[2];
            this.testCtlList[0] = typeof(LoadBatchDialogueCtl);  // mes online
            this.testCtlList[1] = typeof(ManualLoadBatchDialogueCtl);  // Mes offline, input Dut info manually
            // TODO: Add any new controls here
        }

        #endregion

        #region ITestControl Implementation.

        /// <summary>
        /// Get Property.
        /// Informs the Test Engine of all the allowed Batch View GUI User Controls
        /// NB: DON'T CHANGE THIS!!!
        /// </summary>
        public Type[] BatchContentsCtlTypeList
        {
            get
            {
                return (batchCtlList);
            }
        }

        /// <summary>
        /// Get Property.
        /// Informs the Test Engine of all the allowed Batch View GUI User Controls
        /// NB: DON'T CHANGE THIS!!!
        /// </summary>
        public Type[] TestControlCtlTypeList
        {
            get
            {
                return (testCtlList);
            }
        }

        /// <summary>
        /// Get Property.
        /// This specifies whether the Test Control Plug-in operated in Manual or Automatic Mode.
        /// </summary>
        public AutoManual TestType
        {
            get
            {
                // TODO: Update. Return AutoManual.AUTO if this is an automatic mode Test Control Plug-in.
                return AutoManual.AUTO;
            }
        }

        /// <summary>
        /// Load a batch. Called when the Core requests that a new batch be loaded from the MES, via the 
        /// Test Control Server.
        /// </summary>
        /// <param name="testEngine">Object implementing the ITestEngine Interface.</param>
        /// <param name="mes">Object implementing the IMES interface.</param>
        /// <param name="operatorName">The name of the currently logged in user.</param>
        /// <param name="operatorType">The privilege level of the currently logged in user.</param>
        /// <returns>The BatchId of the loaded batch.</returns>
        public string LoadBatch(ITestEngine testEngine, IMES mes,
                        string operatorName, TestControlPrivilegeLevel operatorType)
        {
            testEngine.SendStatusMsg("Load Batch");

            if (TC_Common.IsToCheckSoftwareUpdate)
            {
                CheckSoftwareUpdate.CheckUpdate(SOFTWARE_ID.HITT_GB_Test);
            }

            // Get the Node from IMES (e.g. For FactoryWorks this is set by it's config XML file)
            this.inSimulationMode = testEngine.InSimulationMode;
            this.mes = mes;

            DutInfo = null;
            readTestControlConfig(testEngine);

            System.Collections.Generic.Dictionary<string, DeviceTypeLookupItem> deviceTypeInfo =
                TosaDeviceTypeConfig.LoadDeviceTypeLookup("configuration/TOSA Final/" + "DeviceTypeLookup.csv");
            if ((deviceTypeInfo == null) || (deviceTypeInfo.Count < 1))
            {
                testEngine.ErrorRaise(
                    " can't find config file to provide device type and Chirp type mapping message");
                return null;
            }

            testEngine.PageToFront(TestControlCtl.TestControl);
            testEngine.GetUserAttention(TestControlCtl.TestControl);

            Type typeOf_LoadBatchDialogueCtl = null;

            while (true)
            {

                object msg;

                DatumList lastStageResult = null;

                bool isLastStagePassed = false;

                if (this.isMesOnline)
                {
                    #region Auto Load Batch

                    typeOf_LoadBatchDialogueCtl = typeof(LoadBatchDialogueCtl);
                    testEngine.SelectTestControlCtlToDisplay(typeOf_LoadBatchDialogueCtl);

                    testEngine.SendToCtl(TestControlCtl.TestControl, typeOf_LoadBatchDialogueCtl, new MesLoadDeviceMsg("Please input a batch to load", true, true));
                    testEngine.SendToCtl(TestControlCtl.TestControl, typeOf_LoadBatchDialogueCtl, deviceTypeInfo);

                    msg = testEngine.WaitForCtlMsg().Payload;

                    // Check SPC 2016.09.02
                    if (!TC_Common.CheckSPCStatus(testEngine, mes.Node))
                    {
                        continue;
                    }
            
                    if (msg is Msg_NoMesRequest)
                    {
                        this.isMesOnline = false;
                        this.isDutTrackOut = false;

                        // To fill and disable the SN textbox in ManualLoadBatchDialogueCtl
                        if (msg is Msg_NoMesRequestWithSN)
                        {
                            testEngine.SendToCtl(TestControlCtl.TestControl, typeof(ManualLoadBatchDialogueCtl), msg);
                        }

                        continue;
                    }
                    else if (msg is DatumString)
                    {
                        DatumString ds = msg as DatumString;

                        if (ds.Name == "batchID")
                        {
                            DutInfo = new DutLoadInfo();
                            DutInfo.DutLotID = ds.Value;
                        }
                    }

                    isDutTrackOut = true;//need track out if auto test

                    try
                    {
                        dutBatch = mes.LoadBatch(DutInfo.DutLotID, true);

                        if (dutBatch[0] != null)
                        {
                            // Confirm the factorywork stage - chongjian.liang 2015.9.6
                            if (dutBatch.Stage != "OTTEST")
                            {
                                testEngine.ShowContinueUserQuery(TestControlTab.TestControl, string.Format("The current step in the factorywork should be OTTEST instead of {0} for this test.", dutBatch.Stage));

                                testEngine.SendToCtl(TestControlCtl.TestControl, typeof(LoadBatchDialogueCtl), new MesLoadDeviceMsg("Please input a batch to load", true, true));

                                continue;
                            }

                            this.mes.TrackIn(DutInfo.DutLotID);

                            testEngine.SendToCtl(TestControlCtl.TestControl, typeOf_LoadBatchDialogueCtl, deviceTypeInfo[dutBatch.PartCode]);
                        }
                        else
                        {
                            testEngine.SendToCtl(TestControlCtl.TestControl, typeof(LoadBatchDialogueCtl), new MesLoadDeviceMsg("Please input a batch to load", true, true));

                            continue;   // if no available dut in batch, go on batch load loop
                        }
                    }
                    catch
                    {
                        string msgEx = string.Format("Failed to load or check in batch {0}.", DutInfo.DutLotID);

                        testEngine.SendStatusMsg(msgEx);

                        testEngine.ShowContinueUserQuery(TestControlTab.TestControl, msgEx);

                        testEngine.SendToCtl(TestControlCtl.TestControl, typeof(LoadBatchDialogueCtl), new MesLoadDeviceMsg("Please input a batch to load", true, true));

                        continue;
                    }

                    if (!deviceTypeInfo.ContainsKey(dutBatch.PartCode))
                    {
                        testEngine.ErrorRaise(" can't find device type & chirp type" +
                            "mapping message for " + dutBatch.PartCode +
                                " \n please check the device type & chirp type " +
                                "message mapping configuration file and ensure " +
                                " this device type is available to test with this test control");
                        continue;
                    }

                    msg = testEngine.WaitForCtlMsg().Payload;

                    DeviceTypeLookupItem deviceTypeLookupItem = (DeviceTypeLookupItem)msg;

                    DutInfo.PartCode = dutBatch.PartCode = deviceTypeLookupItem.DeviceType;
                    DutInfo.ChirpType = deviceTypeLookupItem.ChirpType;
                    DutInfo.DutSerialNumber = dutBatch[0].SerialNumber;
                    DutInfo.TestStage = "final_ot";
                    DutInfo.LotType = dutBatch.Attributes.GetDatumString("lot_type").ValueToString();
                   
                    break;

                    #endregion
                }
                else
                {
                    #region Manual Load DutInfo

                    typeOf_LoadBatchDialogueCtl = typeof(ManualLoadBatchDialogueCtl);
                    testEngine.SelectTestControlCtlToDisplay(typeOf_LoadBatchDialogueCtl);

                    testEngine.SendToCtl(TestControlCtl.TestControl, typeOf_LoadBatchDialogueCtl, deviceTypeInfo);

                    #region Get device type from last stage's test result - chongjian.liang 2013.11.20

                    do
                    {
                        msg = testEngine.WaitForCtlMsg().Payload;

                        // Check SPC 2016.09.02
                        if (!TC_Common.CheckSPCStatus(testEngine, mes.Node))
                        {
                            continue;
                        }
            
                        if (msg is string)
                        {
                            string serialNO = msg as string;

                            StringDictionary keys = new StringDictionary();
                            keys.Add("SCHEMA", "hiberdb");
                            keys.Add("SERIAL_NO", serialNO);
                            keys.Add("TEST_STAGE", "final");

                            lastStageResult = testEngine.GetDataReader("PCAS_SHENZHEN").GetLatestResults(keys, false);

                            // Prevent the user from proceeding when serialNO is wrong
                            if (lastStageResult == null
                                || lastStageResult.Count == 0)
                            {
                                testEngine.SendToCtl(TestControlCtl.TestControl, typeOf_LoadBatchDialogueCtl, "LastStageMissing");
                            }
                            // Read device type and send to GUI
                            else
                            {
                                isLastStagePassed = lastStageResult.ReadString("TEST_STATUS").ToLower().Contains("pass");

                                if (!isLastStagePassed)
                                {
                                    testEngine.SendToCtl(TestControlCtl.TestControl, typeOf_LoadBatchDialogueCtl, "LastStageFailed");
                                }
                                else
                                {
                                    string deviceType = lastStageResult.ReadString("PRODUCT_CODE");

                                    testEngine.SendToCtl(TestControlCtl.TestControl, typeOf_LoadBatchDialogueCtl, deviceTypeInfo[deviceType]);
                                }
                            }
                        }
                    } while (lastStageResult == null || !isLastStagePassed);
                    #endregion

                    do
                    {
                        msg = testEngine.WaitForCtlMsg().Payload;

                        if (msg is DutLoadInfo)
                        {
                            this.DutInfo = (DutLoadInfo)msg;
                            this.DutInfo.TestStage = "final_ot";
                            //this.DutInfo.Attributes.AddString("lot_type", "Engineering");
                            DutInfo.LotType = "Engineering";
                        }
                    } while (this.DutInfo == null);

                    isDutTrackOut = false;//no need track out if manual test

                    break;

                    #endregion
                }
            }

            // ... Clear up GUI.
            testEngine.CancelUserAttention(TestControlCtl.TestControl);
            testEngine.GuiClear(TestControlCtl.TestControl);
            // Reset one-time run flag
            programRanOnce = false;
            // Return serial number to Test Engine

            testEngine.SendStatusMsg("Batch load completed!");
            return DutInfo.DutLotID;
        }

        /// <summary>
        /// Get the next available DUT from the Batch.
        /// </summary>
        /// <param name="testEngine">Object that implements the ITestEngine Interface.</param>
        /// <param name="testJigArg">Object tha implements ITestJig Interface.</param>
        /// <param name="operatorName">The name of the currently logged in user.</param>
        /// <param name="operatorType">Privilege level of the currently logged in user.</param>
        /// <returns>DUT object for the next selected device for test or Null if end of batch.</returns>
        public DUTObject GetNextDUT(ITestEngine testEngine, ITestJig testJigArg,
                    string operatorName, TestControlPrivilegeLevel operatorType)
        {
            if (this.programRanOnce) return null; // end of batch after one run!
            testEngine.SelectBatchCtlToDisplay(typeof(BatchViewCtl));
            testEngine.PageToFront(TestControlCtl.BatchView);

            //Comstruct DUTObject - test engine get required program, serial no etc. from here
            dut = new DUTObject();
            if (isDutTrackOut)  //raul added to check if auto test or manual test
            {
                dut.BatchID = DutInfo.DutLotID;  //from Load Batch
            }
            else
            {
                dut.BatchID = DutInfo.DutSerialNumber;  //if manual test the batchid = sn
            }

            dut.SerialNumber = DutInfo.DutSerialNumber;
            dut.PartCode = DutInfo.PartCode;
            dut.TestStage = DutInfo.TestStage;
            dut.NodeID = this.mes.Node;
            dut.TestJigID = testJigArg.GetJigID(testEngine.InDebugMode, testEngine.InSimulationMode);
            dut.Attributes.AddString("lot_type", DutInfo.LotType);
            
            // Select program plugin depends on chirp type
            switch (DutInfo.ChirpType)
            {
                case "ND":
                    //if (dut.TestStage.ToLower().Contains("final_ot"))
                    dut.ProgramPluginName = "Prog_ILMZ_OverTemp";
                    //else if (dut.TestStage == "qual" || dut .TestStage == "qual0")
                    //    dut.ProgramPluginName = "Prog_ILMZ_Qual_ND";
                    break;
                case "ZD":
                    dut.ProgramPluginName = "Prog_ILMZ_OverTemp";
                    break;
                default:
                    testEngine.ErrorRaise("Unhandled device type '" + DutInfo.ChirpType + "'");
                    break;
            }

            dut.ProgramPluginVersion = "";
            dut.Attributes.AddString("ChirpType", DutInfo.ChirpType);

            if (dut.Attributes.GetDatumString("lot_type").ValueToString().ToLower().Equals("production"))
            {
                try
                {

                    //add max continous test count
                    if (RetestOutofRange(testEngine, dut.TestStage, dut))
                    {
                        testEngine.ShowContinueUserQuery(TestControlTab.TestControl,
                            string.Format("Retest count out of spec, testing denied! MaxTestCount[{0}]", this.maxTestCount));

                        return null;
                    }
                }
                catch { }
            }

            testEngine.GetUserAttention(TestControlCtl.BatchView);
            testEngine.SendToCtl(TestControlCtl.BatchView, typeof(BatchViewCtl), dut);

            CtlMsg msg = testEngine.WaitForCtlMsg();
            testEngine.CancelUserAttention(TestControlCtl.BatchView);

            DutTestOption testOption = (DutTestOption)msg.Payload;
            if (testOption == null) return null;

            // only allowed though here once!
            if (testOption.TestOption == enmTestOption.GoOnTest)
            {
                this.programRanOnce = true;
                return dut;
            }
            else
            {
                return null;
            }
        }



        #region[add   for retest issue    ------------add by Dong.Chen ]                                                  ]
        private bool RetestOutofRange(ITestEngine engine, string stage, DUTObject dut)
        {
            StringDictionary mapKeys = new StringDictionary();

            mapKeys.Add("SCHEMA", "hiberdb");
            mapKeys.Add("SERIAL_NO", dut.SerialNumber);//this.serialNum);
            mapKeys.Add("TEST_STAGE", dut.TestStage);  //this.pcasTestStage);
            //mapKeys.Add("DEVICE_TYPE", dut.PartCode);  //this.pcasDeviceType);//这个要加上，因为测试次数达到最大时，可以转Code再测试。

            DatumList list = engine.GetDataReader().GetLatestResults(mapKeys, false);

            if (list.IsPresent("RETEST"))
            {
                //string retest = list.ReadDouble("RETEST").ToString();   // list["RETEST"].ValueToString();
                string retest = list.GetDatumString("RETEST").ValueToString();

                engine.SendStatusMsg("RETEST:" + retest);

                int CL = this.ReadRetest(stage);
                this.maxTestCount = CL;
                int RC = 999;
                int.TryParse(retest.Trim(), out RC);

                if (RC == CL - 1)
                    engine.ShowContinueUserQuery(TestControlTab.TestControl, "Warning: This is the last time to test this device on this stage!!!");
                else if (RC == CL)
                {
                    return true;
                }
            }
            return false;
        }

        private int ReadRetest(string stage)
        {
            string file = "Configuration\\TOSA Final\\RetestTimeConfiguration.csv";
            if (!File.Exists(file)) return 999;

            int count = 999;

            using (StreamReader sr = new StreamReader(file))
            {
                while (!sr.EndOfStream)
                {
                    string[] line = sr.ReadLine().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                    if (line.Length != 2) continue;

                    if (stage.ToLower() == line[0].Trim().ToLower())
                    {
                        int.TryParse(line[1].Trim(), out count);

                        break;
                    }
                }
            }
            return count;
        }
        #endregion

        /// <summary>
        /// Called after the test program has concluded, to decide what the outgoing 
        /// Part ID and other attributes are, having inspected the test results.
        /// </summary>
        /// <param name="testEngine">Reference to object implmenting ITestEngine</param>
        /// <param name="programStatus">The Status of the Test Program Run (succeeded/error/aborted etc).</param>
        /// <param name="testResults">Detailed test results</param>
        /// <returns>Outcome of the results analysis.</returns>
        public DUTOutcome PostDevice(ITestEngine testEngine, ProgramStatus programStatus, TestResults testResults)
        {
            testEngine.SendStatusMsg("Post Device");
            this.programStatus = programStatus;
            this.testResults = testResults;
            // Extract the Specification name from the test results. Fill this in the DUToutcome.
            SpecResultsList specs = testResults.SpecResults;
            string[] specNames = new string[specs.Count];

            int i = 0;
            foreach (SpecResults specRes in specs)
            {
                specNames[i++] = specRes.Name;
            }

            dutOutcome.DUTData = new DatumList();
            dutOutcome.OutputSpecificationNames = new string[1];
            dutOutcome.OutputSpecificationNames[0] = specNames[0];
            return dutOutcome;
        }

        /// <summary>
        /// Commit any post-test MES updates prepared in method PostDevice.
        /// </summary>
        /// <param name="testEngine">Object implementing ITestEngine</param>
        /// <param name="dataWriteError">True if there was a problem writing Test Results to the database.</param>
        public void PostDeviceCommit(ITestEngine testEngine, bool dataWriteError)
        {
            //TODO: For those Test Software Solutions which interact with an MES, store the test
            //status for the current device under test to the MES.
            testEngine.SendStatusMsg("Post Device Commit");
            ////Mark the Device as Tested. Also record it's pass fail status.
            if (testResults.ProgramStatus.Status == MultiSpecPassFail.AllPass)
            {
                if ((this.programStatus == ProgramStatus.Success) && (!dataWriteError))
                {
                    //The Test Passed. Mark the device as completed.
                    this.TestStatus = TestStatus.Passed;

                    if (isMesOnline && isDutTrackOut)
                    {
                        //this.mes.SetBatchAttribute(dut.BatchID, "PartId", dutBatch.PartCode);
                        //System.Threading.Thread.Sleep(5000);
                        this.mes.TrackOutBatchPass(dut.BatchID, dutBatch.Stage, dutBatch.PartCode);
                    }
                }
                else if ((this.programStatus == ProgramStatus.Success) && (dataWriteError))
                {
                    //Problem writing Test Data. Mark for retest.
                    this.TestStatus = TestStatus.MarkForRetest;
                }
            }
            else
            {
                this.TestStatus = TestStatus.Failed;

                if (this.dutOutcome.DUTData.ReadSint32("RETEST") >= this.FailTimeTrackTeckAcess)
                {
                    //this.mes.SetBatchAttribute(dut.BatchID, "PartId", dutBatch.PartCode);
                    //System.Threading.Thread.Sleep(5000);
                    this.mes.TrackOutComponentRework(dut.BatchID, dutBatch.Stage, dut.SerialNumber, dutBatch.PartCode);
                }
            }

            //UpdateTestStatuses message = new UpdateTestStatuses(testStatuses);
            //testEngine.SendToCtl(TestControlCtl.BatchView, typeof(DemoBatchViewCtl), message);
            testEngine.SendToCtl(TestControlCtl.BatchView, typeof(BatchViewCtl), TestStatus);
        }

        /// <summary>
        /// The Test Engine has concluded operations on the currently loaded batch. Update Batch level parameters 
        /// in the MES.
        /// </summary>
        /// <param name="testEngine">Reference to object that implements the ITestEngine Interface.</param>
        public void PostBatch(ITestEngine testEngine)
        {
            //TODO: For those Test Software Solutions which interact with an MES, store the test
            //status for the currently loaded Batch in the MES.
            testEngine.SendStatusMsg("Post Batch");
        }

        /// <summary>
        /// Mark all devices in the Batch for retest.
        /// </summary>
        /// <param name="testEngine">Reference to object that implements the ITestEngine Interface.</param>
        public void RestartBatch(ITestEngine testEngine)
        {

            //TODO: Use whatever method is appropriate to your implmentation to mark all devices in the 
            //current batch untested. It is up to the implmenter to decide whether this means restart all 
            //devices tested in this session or whether to restart the entire batch. Where this functionality
            //is not required, the solution developer may choose to take no action here.
            testEngine.SendToCtl(TestControlCtl.BatchView, typeof(BatchViewCtl), DutInfo);
            testEngine.SendStatusMsg("Restart Batch");


        }
        # endregion

        private void readTestControlConfig(ITestEngine engine)
        {
            if (isTCPliginConfigRead)
                return;

            int result;
            string commonErrorMessageString = "Unable to read Test Control Plug-in configuration File: ";

            //Read Configuration file.
            /* Open user data XML file. This constructor call only conducts a scant validation of the
             * file name. Any expected error will not occur until the first call to Read. */
            XmlTextReader config = new XmlTextReader(CONFIGFilePath + "/TestControlPluginConfig.xml");

            try
            {
                /* Scan file, one element a time. */
                while (config.Read())
                {
                    if (config.NodeType == XmlNodeType.Element)
                    {
                        if (config.LocalName == "UseMES")
                        {
                            string useMes = config.ReadString();

                            result = String.Compare(useMes.Trim(), "yes", true, System.Globalization.CultureInfo.InvariantCulture);
                            if (result == 0)
                            {
                                isMesOnline = true;
                            }
                            else
                            {
                                result = String.Compare(useMes.Trim(), "no", true, System.Globalization.CultureInfo.InvariantCulture);

                                if (result == 0)
                                {
                                    isMesOnline = false;
                                }
                                else
                                {
                                    //Invalid configuration file contents.
                                    engine.ErrorRaise("Invalid UseMES field contents in Configuration/ITLA/TestControlPluginConfig.xml configuration file.");
                                }
                            }  // end else
                        }  // end if "UseMes"

                        if (config.LocalName == "InhibitTrackout")
                        {
                            string trackoutInhibitString = config.ReadString();

                            result = String.Compare(trackoutInhibitString.Trim(), "yes", true);
                            if (result == 0)
                            {
                                isDutTrackOut = false;
                            }
                            else
                            {
                                result = String.Compare(trackoutInhibitString.Trim(), "no", true);

                                if (result == 0)
                                {
                                    isDutTrackOut = true;
                                }
                                else
                                {
                                    //Invalid configuration file contents.
                                    engine.ErrorRaise("Invalid InhibitTrackout field contents in Configuration/ITLA/TestControlPluginConfig.xml configuration file.");
                                }
                            }  // end else: "InhibitTrackout" != "yes"
                        }  // end if "InhibitTrackout"

                        //FailTrackTeckAcess

                        if (config.LocalName == "FailTrackTeckAcess")
                        {
                            string FailTrackTeckAcess = config.ReadString();
                            ///int FailTimeTrackTeckAcess;
                            if (!int.TryParse(FailTrackTeckAcess.Trim(), out this.FailTimeTrackTeckAcess))
                            {
                                //Invalid configuration file contents.
                                engine.ErrorRaise("Invalid InhibitTrackout field contents in Configuration/ITLA/TestControlPluginConfig.xml configuration file.");
                            }

                        }  // end if "Inh
                    } // end if config.NodeType == XmlNodeType.Element: get available text nod
                }  // end while loop

                // Set flag to indicate that configuration has been read.
                isTCPliginConfigRead = true;
            }
            catch (UnauthorizedAccessException uae)
            {

                /* User did not have read permission for file. */
                engine.ErrorRaise(commonErrorMessageString + "Access denied " + uae.Message + uae.StackTrace);
            }
            catch (System.IO.FileNotFoundException fnfe)
            {
                /* File not found error. */
                engine.ErrorRaise(commonErrorMessageString + "Configuration File not found " + fnfe.Message);
            }
            catch (System.IO.IOException ioe)
            {
                /* General IO failure. Not file not found. */
                engine.ErrorRaise(commonErrorMessageString + "General IO Failure " + ioe.Message);
            }
            catch (System.Xml.XmlException xe)
            {
                /* XML parse error. */
                engine.ErrorRaise(commonErrorMessageString + "XMl Parser Error " + xe.Message);
            }
            finally
            {
                /* Close file. */
                if (config != null) config.Close();
            }
        }

        #region Private data
        // Batch View tab controls
        Type[] batchCtlList;
        // Test Control tab controls
        Type[] testCtlList;

        // Flag for the program ran (so we can abort the batch)
        bool programRanOnce = false;
        // Serial number variable
        DutLoadInfo DutInfo;
        /// <summary>
        /// flag to indicate if test control configureation file was read or not
        /// </summary>
        bool isTCPliginConfigRead;
        /// <summary>
        /// Flag to indicate if Fws is available or not
        /// </summary>
        bool isMesOnline;
        /// <summary>
        /// Flag to indicate if dut trackout after test
        /// </summary>
        bool isDutTrackOut;

        string operatorID;

        IMES mes;

        MESbatch dutBatch = null;

        ProgramStatus programStatus;
        TestResults testResults;
        TestStatus TestStatus;
        bool inSimulationMode;
        const string CONFIGFilePath = "configuration/TOSA Final/";
        const string TESTSTAGE_PATH = "Configuration/TOSA Final/FinalTest/TestStageLookUp.csv";
        DUTObject dut;
        DUTOutcome dutOutcome = new DUTOutcome();
        int maxTestCount = 10;
        int FailTimeTrackTeckAcess = 3;
        #endregion
    }
    /// <summary>
    /// Test Status of devices within a batch
    /// </summary>
    internal enum TestStatus
    {
        /// <summary>
        /// Untested.
        /// </summary>
        Untested,

        /// <summary>
        /// Marked for restest.
        /// </summary>
        MarkForRetest,

        /// <summary>
        /// Passed.
        /// </summary>
        Passed,

        /// <summary>
        /// Failed.
        /// </summary>
        Failed
    }
}

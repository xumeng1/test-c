// [Copyright]
//
// Bookham Test Engine
// $projectname$
//
// TC_Txp40GDataDeliver/ManualLoadBatchDialogueCtl
// 
// Author: alice.huang
// Design: TODO

namespace Bookham.TestSolution.TestControl
{
    /// <summary>
    /// GUI to allow user to input DUT Information when Fws Offline
    /// </summary>
    partial class ManualLoadBatchDialogueCtl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtSN = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnLoadBatch = new System.Windows.Forms.Button();
            this.DeviceTypeFrame = new System.Windows.Forms.GroupBox();
            this.DeviceTypeCombo = new System.Windows.Forms.ComboBox();
            this.DeviceTypeFrame.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtSN
            // 
            this.txtSN.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSN.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSN.Location = new System.Drawing.Point(192, 87);
            this.txtSN.Margin = new System.Windows.Forms.Padding(4);
            this.txtSN.Name = "txtSN";
            this.txtSN.Size = new System.Drawing.Size(199, 37);
            this.txtSN.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 87);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(138, 31);
            this.label1.TabIndex = 10;
            this.label1.Text = "DUT SN : ";
            // 
            // btnLoadBatch
            // 
            this.btnLoadBatch.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoadBatch.Location = new System.Drawing.Point(22, 162);
            this.btnLoadBatch.Margin = new System.Windows.Forms.Padding(4);
            this.btnLoadBatch.Name = "btnLoadBatch";
            this.btnLoadBatch.Size = new System.Drawing.Size(369, 56);
            this.btnLoadBatch.TabIndex = 3;
            this.btnLoadBatch.Text = "OK";
            this.btnLoadBatch.UseVisualStyleBackColor = true;
            this.btnLoadBatch.Click += new System.EventHandler(this.btnLoadBatch_Click);
            // 
            // DeviceTypeFrame
            // 
            this.DeviceTypeFrame.Controls.Add(this.DeviceTypeCombo);
            this.DeviceTypeFrame.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DeviceTypeFrame.Location = new System.Drawing.Point(501, 59);
            this.DeviceTypeFrame.Margin = new System.Windows.Forms.Padding(4);
            this.DeviceTypeFrame.Name = "DeviceTypeFrame";
            this.DeviceTypeFrame.Padding = new System.Windows.Forms.Padding(4);
            this.DeviceTypeFrame.Size = new System.Drawing.Size(285, 66);
            this.DeviceTypeFrame.TabIndex = 1;
            this.DeviceTypeFrame.TabStop = false;
            this.DeviceTypeFrame.Text = "Device Type";
            // 
            // DeviceTypeCombo
            // 
            this.DeviceTypeCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.DeviceTypeCombo.FormattingEnabled = true;
            this.DeviceTypeCombo.Location = new System.Drawing.Point(15, 27);
            this.DeviceTypeCombo.Margin = new System.Windows.Forms.Padding(4);
            this.DeviceTypeCombo.Name = "DeviceTypeCombo";
            this.DeviceTypeCombo.Size = new System.Drawing.Size(251, 28);
            this.DeviceTypeCombo.TabIndex = 2;
            this.DeviceTypeCombo.Enter += new System.EventHandler(this.DeviceTypeCombo_Enter);
            // 
            // ManualLoadBatchDialogueCtl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.DeviceTypeFrame);
            this.Controls.Add(this.btnLoadBatch);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtSN);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "ManualLoadBatchDialogueCtl";
            this.Size = new System.Drawing.Size(1291, 556);
            this.MsgReceived += new Bookham.TestEngine.Framework.Messages.ManagedCtlBase.MsgReceivedDlg(this.ManualLoadBatchDialogueCtl_MsgReceived);
            this.DeviceTypeFrame.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtSN;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnLoadBatch;
        private System.Windows.Forms.GroupBox DeviceTypeFrame;
        private System.Windows.Forms.ComboBox DeviceTypeCombo;
    }
}

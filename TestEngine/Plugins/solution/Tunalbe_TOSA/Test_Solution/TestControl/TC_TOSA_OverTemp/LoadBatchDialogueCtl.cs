// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestControl
//
// LoadBatchDialogueCtl.cs
//
// Author: tommy.yu, 2007
// Design: [Reference design documentation

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.PluginInterfaces.MES;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestSolution.IlmzCommonUtils;

namespace Bookham.TestSolution.TestControl
{
    /// <summary>
    /// Test Control Plug-in Template Batch View User Control
    /// </summary>
    public partial class LoadBatchDialogueCtl : Bookham.TestEngine.Framework.Messages.ManagedCtlBase
    {

        /// <summary>
        /// Constructor
        /// </summary>
        public LoadBatchDialogueCtl()
        {
            /* Call designer generated code. */
            InitializeComponent();

            this.loadBatchTextBox.CharacterCasing = CharacterCasing.Upper;
            
        }

        /// <summary>
        /// Process recieved messages from the worker thread.
        /// </summary>
        /// <param name="payload">Message Payload</param>
        /// <param name="inMsgSeq">Incoming sequence number</param>
        /// <param name="respSeq">Response sequence number</param>
        private void LoadBatchDialogueCtl_MsgReceived(object payload, long inMsgSeq, long respSeq)
        {
            //TODO: Process messages from the Test Control Plug-in worker thread.
            if (payload.GetType() == typeof(MesLoadDeviceMsg))
            {
                displayControls();

                MesLoadDeviceMsg batchLoadRsp = (MesLoadDeviceMsg)payload;

                this.messageLabel.Text = batchLoadRsp.MesMessage;
                this.DeviceTypeCombo.Enabled = batchLoadRsp.EnableLoadDutInformation;
                this.loadBatchButton.Enabled = batchLoadRsp.EnableLoadDutInformation;
                this.Button_NoMes.Enabled = batchLoadRsp.EnableLoadDutInformation;
                this.loadBatchTextBox.Enabled = batchLoadRsp.EnableLoadDutInformation;
                this.loadBatchTextBox.SelectAll();
                this.loadBatchTextBox.Focus();
            }
            // Only present the device type from previous stage's result - chongjian.liang 2013.11.20
            else if (payload is DeviceTypeLookupItem)
            {
                if (this.DeviceTypeCombo.Items.Count == 0)
                {
                    this.DeviceTypeCombo.Items.Add(payload);
                    this.DeviceTypeCombo.DisplayMember = "DeviceType";
                }

                this.messageLabel.Text = string.Format("The device type is {0} in MES.", ((DeviceTypeLookupItem)payload).DeviceType);
                this.DeviceTypeCombo.SelectedItem = payload;
                this.DeviceTypeCombo.Enabled = true;
                this.loadBatchButton.Enabled = true;
            }
        }

        /// <summary>
        /// The User clicked the load batch button. Send a message to the worker thread, with the 
        /// user's desired Batch ID.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">event parameters.</param>
        private void loadBatchButton_Click(object sender, System.EventArgs e)
        {
            if (this.DeviceTypeCombo.SelectedItem == null)
            {
                MessageBox.Show("Please select a device type.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                this.sendToWorker(this.DeviceTypeCombo.SelectedItem);
            }
        }

        /// <summary>
        /// Check and load the device type for the inputted serial number - chongjian.liang 2016.02.22
        /// </summary>
        private void DeviceTypeCombo_Enter(object sender, EventArgs e)
        {
            if (this.DeviceTypeCombo.SelectedItem == null)
            {
                string batchID = loadBatchTextBox.Text.Trim().ToUpper();

                // Load RMA SN
                if (TC_Common.RMA_SNList != null
                    && TC_Common.RMA_SNList.Contains(batchID))
                {
                    this.sendToWorker(new Msg_NoMesRequestWithSN(batchID));
                }
                else // Load Batch ID
                {
                    if (!batchID.StartsWith("HG", true, null))
                    {
                        MessageBox.Show("Batch ID must start with \"HG\".");

                        this.loadBatchTextBox.Focus();
                    }
                        /*  delete by dong.chen 20170728
                    else if (!batchID.EndsWith(".1", true, null))
                    {
                        MessageBox.Show("Batch ID must end with \".1\".");

                        this.loadBatchTextBox.Focus();
                    }*/
                    else if (batchID.Length != 10)
                    {
                        MessageBox.Show("Batch ID must contain exact 10 characters.");

                        this.loadBatchTextBox.Focus();
                    }
                    else
                    {
                        this.messageLabel.Text = "Please wait while retrieving device type from MES ...";
                        this.DeviceTypeCombo.Text = "Please wait..";
                        this.DeviceTypeCombo.Enabled = false;
                        this.loadBatchTextBox.Enabled = false;
                        this.loadBatchButton.Enabled = false;
                        this.Button_NoMes.Enabled = false;

                        this.sendToWorker(new DatumString("batchID", batchID));
                    }
                }
            }
        }

        /// <summary>
        /// Dispay normal load batch screen controls.
        /// </summary>
        private void displayControls()
        {
            this.loadBatchScreenLabel.Show();
            this.bookhamLogoPictureBox.Show();
            showLoadBatchControls();
        }

        /// <summary>
        /// Hide All  Controls
        /// </summary>
        private void hideControls()
        {
            this.loadBatchScreenLabel.Hide();
            this.bookhamLogoPictureBox.Hide();
            hideLoadBatchControls();
        }

        /// <summary>
        /// Hide Load Batch operation associated controls.
        /// </summary>
        private void hideLoadBatchControls()
        {
            this.loadBatchTextBox.Hide();
            this.loadBatchButton.Hide();
            this.messageLabel.Hide();
        }

        /// <summary>
        /// Show Load Batch operation associated controls.
        /// </summary>
        private void showLoadBatchControls()
        {
            this.loadBatchTextBox.Show();
            this.loadBatchButton.Show();
            this.messageLabel.Show();
        }

        /// <summary>
        /// Disable Load Batch operation associated controls.
        /// </summary>
        private void disableLoadBatchControls()
        {
            this.loadBatchTextBox.Enabled = false;
            this.loadBatchButton.Enabled = false;
        }

        /// <summary>
        /// Enable Load Batch operation associated controls.
        /// </summary>
        private void enableLoadBatchControls()
        {
            this.loadBatchTextBox.Enabled = true;
            this.loadBatchButton.Enabled = true;
        }


        /// <summary>
        /// User clicks the retry button. Send a retry message to the worker thread and resume 
        /// displaying the normal load batch screen.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Event parameters</param>
        private void retryButton_Click(object sender, System.EventArgs e)
        {
            // Message the worker thread.
            //this.sendToWorker(QueryRetryAbort.Retry);
            //MessageClass.SendToWorker(QueryRetryAbort.Retry);

            //Enable Load Batch Controls
            enableLoadBatchControls();

            // Remove the error message.
            this.messageLabel.Text = "";
        }


        /// <summary>
        /// User clicks the abort button. Send a abort message to the worker thread and resume 
        /// displaying the normal load batch screen.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Event parameters</param>
        private void abortButton_Click(object sender, System.EventArgs e)
        {
            // Message the worker thread.
            //this.sendToWorker(QueryRetryAbort.Abort);
            //MessageClass.SendToWorker(QueryRetryAbort.Abort);

            //Enable Load Batch Controls
            enableLoadBatchControls();

            // Remove the error message.
            this.messageLabel.Text = "";

            // Prompt the user
            this.loadBatchTextBox.Text = "";

            this.loadBatchButton.Enabled = true;
        }

        private void loadBatchTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!string.IsNullOrEmpty(loadBatchTextBox.Text.Trim()) && e.KeyChar == '\r')
                if (!loadBatchButton.Focused) loadBatchButton.Focus();

        }

        private void LoadBatchDialogueCtl_VisibleChanged(object sender, EventArgs e)
        {
            if (Visible && !loadBatchTextBox.Focused)
            {
                loadBatchTextBox.Focus();
            }
        }

        private void loadBatchTextBox_Enter(object sender, EventArgs e)
        {
            loadBatchTextBox.SelectAll();
        }

        private void Button_NoMes_Click(object sender, EventArgs e)
        {
            sendToWorker(new Msg_NoMesRequest());
        }
    }
}
      

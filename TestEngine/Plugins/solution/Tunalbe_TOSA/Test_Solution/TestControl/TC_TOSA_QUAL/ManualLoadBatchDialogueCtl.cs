// [Copyright]
//
// Bookham Test Engine
// TODO - Full name of assembly
//
// TC_Txp40GDataDeliver/ManualLoadBatchDialogueCtl.cs
// 
// Author: alice.huang
// Re-design by: chongjian.liang 2016.03.30
// Design: TODO Title of design document

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Bookham.TestSolution.IlmzCommonUtils;

namespace Bookham.TestSolution.TestControl
{
    public partial class ManualLoadBatchDialogueCtl : Bookham.TestEngine.Framework.Messages.ManagedCtlBase
    {
        /// <summary>
        /// constructor
        /// </summary>
        public ManualLoadBatchDialogueCtl()
        {
            /* Call designer generated code. */
            InitializeComponent();

            /* TODO - Write constructor code here. */
        }
        /// <summary>
        /// initialise controls on gui when load
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.txtSN.Focus();
        }

        private void btnLoadBatch_Click(object sender, EventArgs e)
        {
            DutLoadInfo dutInfo = new DutLoadInfo();

            // Load serial number

            string serialNO = this.txtSN.Text.Trim();

            if (string.IsNullOrEmpty(serialNO))
            {
                MessageBox.Show("Please input a serial number.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.txtSN.Focus();
                return;
            }
            else
            {
                dutInfo.DutSerialNumber = serialNO;
            }

            // Load test stage

            if (this.cmbStage.SelectedItem == null)
            {
                MessageBox.Show("Please select a test stage.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.cmbStage.Focus();
                return;
            }
            else
            {
                dutInfo.TestStage = cmbStage.Text.Trim();
            }

            // Load device type

            if (this.DeviceTypeCombo.SelectedItem == null)
            {
                MessageBox.Show("Please select a device type.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            else
            {
                DeviceTypeLookupItem selectedDeviceType = (DeviceTypeLookupItem)this.DeviceTypeCombo.SelectedItem;

                dutInfo.PartCode = selectedDeviceType.DeviceType;
                dutInfo.ChirpType = selectedDeviceType.ChirpType;
            }

            // Send dut info

            sendToWorker(dutInfo);
        }

        private void ManualLoadBatchDialogueCtl_MsgReceived(object payload, long inMsgSeq, long respSeq)
        {
            // Use combobox instead of ListView.
            if (payload is Dictionary<string, DeviceTypeLookupItem>)
            {
                foreach (DeviceTypeLookupItem item in ((Dictionary<string, DeviceTypeLookupItem>)payload).Values)
                {
                    this.DeviceTypeCombo.Items.Add(item);
                }

                this.DeviceTypeCombo.DisplayMember = "DeviceType";
            }
            // Select the device type from previous stage's test result.
            else if (payload is DeviceTypeLookupItem)
            {
                this.DeviceTypeCombo.SelectedItem = payload;
                this.DeviceTypeCombo.Enabled = true;
            }
            else if (payload is string)
            {
                string msg = payload as string;

                // Alert the user to input the correct serial number.
                if (msg == "MissingPreviousPassedStage")
                {
                    this.DeviceTypeCombo.Text = "";
                    this.DeviceTypeCombo.Enabled = true;
                    this.cmbStage.Enabled = true;
                    this.txtSN.Enabled = true;

                    if (this.cmbStage.SelectedItem.ToString().ToLower().Contains("qual0"))
                    {
                        MessageBox.Show(string.Format("Missing passed final result for serial number \"{0}\".", this.txtSN.Text));
                    }
                    else
                    {
                        MessageBox.Show(string.Format("Missing passed qual0 result for serial number \"{0}\".", this.txtSN.Text));
                    }

                    this.txtSN.Focus();
                    this.txtSN.SelectAll();
                }
            }
        }

        /// <summary>
        /// Check and load the device type for the inputted serial number
        /// </summary>
        private void DeviceTypeCombo_Enter(object sender, EventArgs e)
        {
            if (this.DeviceTypeCombo.SelectedItem == null)
            {
                if (string.IsNullOrEmpty(this.txtSN.Text.Trim()))
                {
                    MessageBox.Show("Please input a serial number.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    
                    this.txtSN.Focus();
                    this.txtSN.SelectAll();
                }
                else if (this.cmbStage.SelectedItem == null)
                {
                    MessageBox.Show("Please select a test stage.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    
                    this.cmbStage.Focus();
                }
                else
                {
                    this.DeviceTypeCombo.Text = "Please wait..";
                    this.DeviceTypeCombo.Enabled = false;
                    this.cmbStage.Enabled = false;
                    this.txtSN.Enabled = false;

                    sendToWorker(new Msg_SN_Stage(this.txtSN.Text.ToUpper().Trim(), this.cmbStage.SelectedItem.ToString()));
                }
            }
        }
    }
}

// Author: chongjian.liang 2016.03.30

using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestSolution.TestControl
{
    class Msg_SN_Stage
    {
        public string SerialNumber;
        public string TestStage;

        public Msg_SN_Stage(string serialNumber, string testStage)
        {
            this.SerialNumber = serialNumber;
            this.TestStage = testStage;
        }
    }
}

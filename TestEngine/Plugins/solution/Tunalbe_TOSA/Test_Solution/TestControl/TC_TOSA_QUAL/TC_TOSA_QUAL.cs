// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestControl
//
// TC_TOSA_QUAL.cs
//
// Author: alice.huang, 2010
// Re-design by: chongjian.liang 2016.03.30
// Design: [Reference design documentation]

using System;
using System.Xml;
using System.Collections;
using System.Collections.Generic;
using Bookham.TestEngine.Framework.Exceptions;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.PluginInterfaces.TestControl;
using Bookham.TestEngine.PluginInterfaces.TestJig;
using Bookham.TestEngine.PluginInterfaces.MES;
using Bookham.TestEngine.PluginInterfaces.ExternalData;
using Bookham.TestEngine.PluginInterfaces.Security;
using System.Collections.Specialized;
using Bookham.TestSolution.IlmzCommonUtils;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Framework.Limits;
using Bookham.TestLibrary.Utilities;

namespace Bookham.TestSolution.TestControl
{
    /// <summary>
    /// Test Control Plug-in Template.
    /// NB: Implements a Test Control with a single device in the batch.
    /// The following parameters are hardcoded in the DUTObject and will need to be changed,
    /// either by replacement value or by reading the MES:
    /// - ProgramPluginName and Version
    /// - PartCode
    /// - TestStage
    /// </summary>
    public class TC_TOSA_QUAL : ITestControl
    {
        #region Constructor
        /// <summary>
        /// Constructor. 
        /// </summary>
        public TC_TOSA_QUAL()
        {
            // initialise the Batch View tab control array (private variable)
            this.batchCtlList = new Type[1];
            this.batchCtlList[0] = typeof(BatchViewCtl);  // show dut infomation
            // TODO: Add any new controls here

            // initialise the Test Control tab control array (private variable)
            this.testCtlList = new Type[1];
            this.testCtlList[0] = typeof(ManualLoadBatchDialogueCtl);  // Mes offline, input Dut info manually
            // TODO: Add any new controls here
        }

        #endregion

        #region ITestControl Implementation.

        /// <summary>
        /// Get Property.
        /// Informs the Test Engine of all the allowed Batch View GUI User Controls
        /// NB: DON'T CHANGE THIS!!!
        /// </summary>
        public Type[] BatchContentsCtlTypeList
        {
            get
            {
                return (batchCtlList);
            }
        }

        /// <summary>
        /// Get Property.
        /// Informs the Test Engine of all the allowed Batch View GUI User Controls
        /// NB: DON'T CHANGE THIS!!!
        /// </summary>
        public Type[] TestControlCtlTypeList
        {
            get
            {
                return (testCtlList);
            }
        }

        /// <summary>
        /// Get Property.
        /// This specifies whether the Test Control Plug-in operated in Manual or Automatic Mode.
        /// </summary>
        public AutoManual TestType
        {
            get
            {
                // TODO: Update. Return AutoManual.AUTO if this is an automatic mode Test Control Plug-in.
                return AutoManual.MANUAL;
            }
        }

        /// <summary>
        /// Load a batch. Called when the Core requests that a new batch be loaded from the MES, via the 
        /// Test Control Server.
        /// </summary>
        /// <param name="testEngine">Object implementing the ITestEngine Interface.</param>
        /// <param name="mes">Object implementing the IMES interface.</param>
        /// <param name="operatorName">The name of the currently logged in user.</param>
        /// <param name="operatorType">The privilege level of the currently logged in user.</param>
        /// <returns>The BatchId of the loaded batch.</returns>
        public string LoadBatch(ITestEngine testEngine, IMES mes, 
                        string operatorName, TestControlPrivilegeLevel operatorType)
        {
            testEngine.SendStatusMsg("Load Batch");

            if (TC_Common.IsToCheckSoftwareUpdate)
            {
                CheckSoftwareUpdate.CheckUpdate(SOFTWARE_ID.HITT_GB_Test);
            }

            // Get the Node from IMES (e.g. For FactoryWorks this is set by it's config XML file)
            this.inSimulationMode = testEngine.InSimulationMode;
            this.mes = mes;

            DutInfo = null;

            System.Collections.Generic.Dictionary<string, DeviceTypeLookupItem> deviceTypeInfo = TosaDeviceTypeConfig.LoadDeviceTypeLookup("configuration/TOSA Final/" + "DeviceTypeLookup.csv");
            
            if ((deviceTypeInfo == null) || (deviceTypeInfo.Count < 1))
            {
                testEngine.ErrorRaise(" can't find config file to provide device type and Chirp type mapping message");
                return null;
            }

            testEngine.PageToFront(TestControlCtl.TestControl);
            testEngine.GetUserAttention(TestControlCtl.TestControl);

            while (true)
            {
                testEngine.SelectTestControlCtlToDisplay(typeof(ManualLoadBatchDialogueCtl));
                testEngine.SendToCtl(TestControlCtl.TestControl, typeof(ManualLoadBatchDialogueCtl), deviceTypeInfo);

                object msg;

                DatumList lastStageResult = null;

                bool isLastStagePassed = false;

                #region Get device type from last stage's test result - chongjian.liang 2013.11.20

                do
                {
                    msg = testEngine.WaitForCtlMsg().Payload;

                    if (msg is Msg_SN_Stage)
                    {
                        Msg_SN_Stage msg_SN_Stage = msg as Msg_SN_Stage;

                        string lastStage = null;

                        if (msg_SN_Stage.TestStage.ToLower().Contains("qual0"))
                        {
                            lastStage = "final";
                        }
                        else
                        {
                            lastStage = "qual0";
                        }

                        StringDictionary keys = new StringDictionary();
                        keys.Add("SCHEMA", "hiberdb");
                        keys.Add("SERIAL_NO", msg_SN_Stage.SerialNumber);
                        keys.Add("TEST_STAGE", lastStage);

                        lastStageResult = testEngine.GetDataReader("PCAS_SHENZHEN").GetLatestResults(keys, false, "UPPER(TEST_STATUS) LIKE '%PASS%'");

                        if (lastStageResult == null
                            || lastStageResult.Count == 0)
                        {
                            testEngine.SendToCtl(TestControlCtl.TestControl, typeof(ManualLoadBatchDialogueCtl), "MissingPreviousPassedStage");
                        }
                        else
                        {
                            testEngine.SendToCtl(TestControlCtl.TestControl, typeof(ManualLoadBatchDialogueCtl), deviceTypeInfo[lastStageResult.ReadString("PRODUCT_CODE")]);
                        }
                    }
                } while (lastStageResult == null); 
                #endregion

                #region Wait for "OK" button being clicked - chongjian.liang 2013.11.20
                
                do
                {
                    msg = testEngine.WaitForCtlMsg().Payload;

                    if (msg is DutLoadInfo)
                    {
                        this.DutInfo = (DutLoadInfo)msg;
                    }
                } while (this.DutInfo == null); 
                #endregion

                break;
            }

            testEngine.CancelUserAttention(TestControlCtl.TestControl);
            testEngine.GuiClear(TestControlCtl.TestControl);
            // Reset one-time run flag
            programRanOnce = false;
            // Return serial number to Test Engine

            testEngine.SendStatusMsg("Batch load completed!");
            return DutInfo.DutLotID;
        }

        /// <summary>
        /// Get the next available DUT from the Batch.
        /// </summary>
        /// <param name="testEngine">Object that implements the ITestEngine Interface.</param>
        /// <param name="testJigArg">Object tha implements ITestJig Interface.</param>
        /// <param name="operatorName">The name of the currently logged in user.</param>
        /// <param name="operatorType">Privilege level of the currently logged in user.</param>
        /// <returns>DUT object for the next selected device for test or Null if end of batch.</returns>
        public DUTObject GetNextDUT(ITestEngine testEngine, ITestJig testJigArg, 
                    string operatorName, TestControlPrivilegeLevel operatorType)
        {
            if (this.programRanOnce) return null; // end of batch after one run!
            testEngine.SelectBatchCtlToDisplay(typeof(BatchViewCtl));
            testEngine.PageToFront(TestControlCtl.BatchView);

            //Comstruct DUTObject - test engine get required program, serial no etc. from here
            dut = new DUTObject();

            dut.BatchID = DutInfo.DutSerialNumber;
            dut.SerialNumber = DutInfo.DutSerialNumber;
            dut.NodeID = this.mes.Node;
            dut.TestJigID = testJigArg.GetJigID(testEngine.InDebugMode, testEngine.InSimulationMode);
            dut.PartCode = DutInfo.PartCode;
            dut.TestStage = DutInfo.TestStage;
            dut.ProgramPluginName = "Prog_ILMZ_Qual";
            dut.Attributes.AddString("ChirpType", DutInfo.ChirpType);
            dut.ProgramPluginVersion = "";

            testEngine.GetUserAttention(TestControlCtl.BatchView);
            testEngine.SendToCtl(TestControlCtl.BatchView, typeof(BatchViewCtl), dut);
            
            CtlMsg msg = testEngine.WaitForCtlMsg();
            testEngine.CancelUserAttention(TestControlCtl.BatchView);

            DutTestOption testOption = (DutTestOption)msg.Payload;
            if (testOption == null) return null;

            // only allowed though here once!
            if (testOption.TestOption == enmTestOption.GoOnTest)
            {
                this.programRanOnce = true;
                return dut;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Called after the test program has concluded, to decide what the outgoing 
        /// Part ID and other attributes are, having inspected the test results.
        /// </summary>
        /// <param name="testEngine">Reference to object implmenting ITestEngine</param>
        /// <param name="programStatus">The Status of the Test Program Run (succeeded/error/aborted etc).</param>
        /// <param name="testResults">Detailed test results</param>
        /// <returns>Outcome of the results analysis.</returns>
        public DUTOutcome PostDevice(ITestEngine testEngine, ProgramStatus programStatus, TestResults testResults)
        {
            testEngine.SendStatusMsg("Post Device");
            this.programStatus = programStatus;
            this.testResults = testResults;
            // Extract the Specification name from the test results. Fill this in the DUToutcome.
            SpecResultsList specs = testResults.SpecResults;
            string[] specNames = new string[specs.Count];

            int i = 0;
            foreach (SpecResults specRes in specs)
            {
                specNames[i++] = specRes.Name;
            }


            DUTOutcome dutOutcome = new DUTOutcome();

            // Fill in the first specification name from the results for the DUToutcome.
            dutOutcome.OutputSpecificationNames = new string[1];
            dutOutcome.OutputSpecificationNames[0] = specNames[0];
            return dutOutcome;
        }

        /// <summary>
        /// Commit any post-test MES updates prepared in method PostDevice.
        /// </summary>
        /// <param name="testEngine">Object implementing ITestEngine</param>
        /// <param name="dataWriteError">True if there was a problem writing Test Results to the database.</param>
        public void PostDeviceCommit(ITestEngine testEngine, bool dataWriteError)
        {
            //TODO: For those Test Software Solutions which interact with an MES, store the test
            //status for the current device under test to the MES.
            testEngine.SendStatusMsg("Post Device Commit");
            ////Mark the Device as Tested. Also record it's pass fail status.
            if (testResults.ProgramStatus.Status == MultiSpecPassFail.AllPass)
            {
                if ((this.programStatus == ProgramStatus.Success) && (!dataWriteError))
                {
                    //The Test Passed. Mark the device as completed.
                    this.TestStatus = TestStatus.Passed;
                }
                else if ((this.programStatus == ProgramStatus.Success) && (dataWriteError))
                {
                    //Problem writing Test Data. Mark for retest.
                    this.TestStatus = TestStatus.MarkForRetest;
                }
            }
            else
            {
                this.TestStatus = TestStatus.Failed;
            }

            //UpdateTestStatuses message = new UpdateTestStatuses(testStatuses);
            //testEngine.SendToCtl(TestControlCtl.BatchView, typeof(DemoBatchViewCtl), message);
            testEngine.SendToCtl(TestControlCtl.BatchView, typeof(BatchViewCtl), TestStatus);
        }

        /// <summary>
        /// The Test Engine has concluded operations on the currently loaded batch. Update Batch level parameters 
        /// in the MES.
        /// </summary>
        /// <param name="testEngine">Reference to object that implements the ITestEngine Interface.</param>
        public void PostBatch(ITestEngine testEngine)
        {
            //TODO: For those Test Software Solutions which interact with an MES, store the test
            //status for the currently loaded Batch in the MES.
            testEngine.SendStatusMsg("Post Batch");
        }

        /// <summary>
        /// Mark all devices in the Batch for retest.
        /// </summary>
        /// <param name="testEngine">Reference to object that implements the ITestEngine Interface.</param>
        public void RestartBatch(ITestEngine testEngine)
        {

            //TODO: Use whatever method is appropriate to your implmentation to mark all devices in the 
            //current batch untested. It is up to the implmenter to decide whether this means restart all 
            //devices tested in this session or whether to restart the entire batch. Where this functionality
            //is not required, the solution developer may choose to take no action here.
            testEngine.SendToCtl(TestControlCtl.BatchView, typeof(BatchViewCtl), DutInfo);
            testEngine.SendStatusMsg("Restart Batch");           
            
            
        }
        # endregion

        /// <summary>
        /// Read test stage from file. - Added by chongjian.liang
        /// </summary>
        /// <param name="testEngine"></param>
        /// <param name="stageList"></param>
        private void ReadStagesFromFile(ITestEngine testEngine, string filePath, out List<string> stageList)
        {
            stageList = new List<string>();

            CsvReader csvReader = new CsvReader();

            List<string[]> lines = csvReader.ReadFile(filePath);

            for (int index = 1; index < lines.Count; ++index)
            {
                stageList.Add(lines[index][0]);
            }
        }

        #region Private data
        // Batch View tab controls
        Type[] batchCtlList;
        // Test Control tab controls
        Type[] testCtlList;

        // Flag for the program ran (so we can abort the batch)
        bool programRanOnce = false;
        // Serial number variable
        DutLoadInfo DutInfo;
        /// <summary>
        /// flag to indicate if test control configureation file was read or not
        /// </summary>
        bool isTCPliginConfigRead;
        /// <summary>
        /// Flag to indicate if Fws is available or not
        /// </summary>

        string operatorID;

        IMES mes;

        ProgramStatus programStatus;
        TestResults testResults;
        TestStatus TestStatus;
        bool inSimulationMode;
        const string CONFIGFilePath = "configuration/TOSA Final/";
        const string TESTSTAGE_PATH = "Configuration/TOSA Final/FinalTest/TestStageLookUp.csv";
        DUTObject dut;
        #endregion
    }
    /// <summary>
    /// Test Status of devices within a batch
    /// </summary>
    internal enum TestStatus
    {
        /// <summary>
        /// Untested.
        /// </summary>
        Untested,

        /// <summary>
        /// Marked for restest.
        /// </summary>
        MarkForRetest,

        /// <summary>
        /// Passed.
        /// </summary>
        Passed,

        /// <summary>
        /// Failed.
        /// </summary>
        Failed
    }
}

// Author: chongjian.liang 2016.07.08

namespace Bookham.TestSolution.TestControl
{
    /// <summary>
    /// Test Control Plug-in Template Batch View User Control
    /// </summary>
    partial class BatchViewCtl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BatchViewCtl));
            this.SuspendLayout();
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Name = "BatchViewCtl";
            this.Size = new System.Drawing.Size(856, 307);
            this.MsgReceived += new Bookham.TestEngine.Framework.Messages.ManagedCtlBase.MsgReceivedDlg(this.BatchViewCtl_MsgReceived);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
    }
}

// Author: chongjian.liang 2016.07.08

using System;
using System.Xml;
using System.Collections;
using System.Collections.Generic;
using Bookham.TestEngine.Framework.Exceptions;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.PluginInterfaces.TestControl;
using Bookham.TestEngine.PluginInterfaces.TestJig;
using Bookham.TestEngine.PluginInterfaces.MES;
using Bookham.TestEngine.PluginInterfaces.ExternalData;
using Bookham.TestEngine.PluginInterfaces.Security;
using System.Collections.Specialized;
using Bookham.TestSolution.IlmzCommonUtils;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Framework.Limits;

namespace Bookham.TestSolution.TestControl
{
    public class TC_HITT_Tool : ITestControl
    {
        #region Constructor

        public TC_HITT_Tool()
        {
            this.batchCtlList = new Type[1];
            this.batchCtlList[0] = typeof(BatchViewCtl);

            this.testCtlList = new Type[1];
            this.testCtlList[0] = typeof(LoadBatchDialogueCtl);
        }

        #endregion

        #region ITestControl Implementation.

        public Type[] BatchContentsCtlTypeList
        {
            get
            {
                return (batchCtlList);
            }
        }

        public Type[] TestControlCtlTypeList
        {
            get
            {
                return (testCtlList);
            }
        }

        public AutoManual TestType
        {
            get
            {
                return AutoManual.MANUAL;
            }
        }

        public string LoadBatch(ITestEngine testEngine, IMES mes, string operatorName, TestControlPrivilegeLevel operatorType)
        {
            testEngine.SendStatusMsg("Load Batch");

            this.mes = mes;

            return string.Empty;
        }

        public DUTObject GetNextDUT(ITestEngine testEngine, ITestJig testJigArg,
                    string operatorName, TestControlPrivilegeLevel operatorType)
        {
            testEngine.SendStatusMsg("Get Next DUT");

            if (this.programRanOnce)
            {
                return null;
            }

            string DUMMY_STRING = "N/A";

            DUTObject dut = new DUTObject();
            dut.BatchID = DUMMY_STRING;
            dut.SerialNumber = DUMMY_STRING;
            dut.PartCode = DUMMY_STRING;
            dut.TestStage = DUMMY_STRING;
            dut.NodeID = this.mes.Node;
            dut.TestJigID = testJigArg.GetJigID(testEngine.InDebugMode, testEngine.InSimulationMode);
            dut.ProgramPluginName = "Prog_HITT_Tool";
            dut.ProgramPluginVersion = string.Empty;

            this.programRanOnce = true;

            return dut;
        }

        public DUTOutcome PostDevice(ITestEngine testEngine, ProgramStatus programStatus, TestResults testResults)
        {
            testEngine.SendStatusMsg("Post Device");

            return new DUTOutcome();
        }

        public void PostDeviceCommit(ITestEngine testEngine, bool dataWriteError)
        {
            testEngine.SendStatusMsg("Post Device Commit");
        }

        public void PostBatch(ITestEngine testEngine)
        {
            testEngine.SendStatusMsg("Post Batch");
        }

        public void RestartBatch(ITestEngine testEngine)
        {
            testEngine.SendStatusMsg("Restart Batch");


        }
        # endregion

        #region Private data

        Type[] batchCtlList;
        Type[] testCtlList;

        bool programRanOnce = false;

        IMES mes;

        #endregion
    }
}

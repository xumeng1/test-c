// Author: chongjian.liang 2016.07.08

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.Framework.InternalData;

namespace Bookham.TestSolution.TestControl
{
    public partial class BatchViewCtl : Bookham.TestEngine.Framework.Messages.ManagedCtlBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public BatchViewCtl()
        {
            /* Call designer generated code. */
            InitializeComponent();
        }

        /// <summary>
        /// Handle incoming messages.
        /// </summary>
        /// <param name="payload">Message payload</param>
        /// <param name="inMsgSeq">Input message sequence number.</param>
        /// <param name="respSeq">Outgoing message sequence number.</param>
        private void BatchViewCtl_MsgReceived(object payload, long inMsgSeq, long respSeq)
        {
        }
    }
}

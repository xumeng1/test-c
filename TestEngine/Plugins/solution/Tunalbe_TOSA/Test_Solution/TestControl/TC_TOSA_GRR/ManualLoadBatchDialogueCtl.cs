// [Copyright]
//
// Bookham Test Engine
// TODO - Full name of assembly
//
// TC_Txp40GDataDeliver/ManualLoadBatchDialogueCtl.cs
// 
// Author: alice.huang
// Design: TODO Title of design document

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Bookham.TestSolution.IlmzCommonUtils;

namespace Bookham.TestSolution.TestControl
{
    public partial class ManualLoadBatchDialogueCtl : Bookham.TestEngine.Framework.Messages.ManagedCtlBase
    {
        /// <summary>
        /// constructor
        /// </summary>
        public ManualLoadBatchDialogueCtl()
        {
            /* Call designer generated code. */
            InitializeComponent();

            /* TODO - Write constructor code here. */
        }
        /// <summary>
        /// initialise controls on gui when load
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.txtSN.Focus();
        }

        private void btnLoadBatch_Click(object sender, EventArgs e)
        {
            #region verify daily SPC
            /*
            string showTitleMessage = ReadTxtFile.ReadTxt(@"Configuration\Node_SPC.txt");
            string node = showTitleMessage;// System.Net.Dns.GetHostName();

            //verify daily SPC
            {
                TestLinkQueryStruct query = new TestLinkQueryStruct();
                query.StartTime = DateTime.Now.AddDays(-1);
                query.TestStage = "hitt_spc";
                query.Schema = PcasSchema.HIBERDB;
                List<TestLink> links = TestLink.GetTestLinks(query);

                bool spcTested = false;
                foreach (TestLink link in links)
                {
                    if (link == null)
                    {
                        continue;
                    }

                    Result result = link.Result;
                    if (result == null)
                    {
                        continue;
                    }

                    object obEquip_id = result["NODE"];
                    if (obEquip_id == null)
                    {
                        continue;
                    }

                    if (obEquip_id.ToString() == node)
                    {
                        spcTested = true;
                        break;
                    }
                }

                if (spcTested == false)
                {
                    MessageBox.Show("Daily SPC device not been tested for more than 24 hrs!",
                        "Warning",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                    return;
                }
            }
            */

            #endregion

            DutLoadInfo dutInfo = new DutLoadInfo();

            // Load serial number - chongjian.liang 2013.11.20

            string serialNO = this.txtSN.Text.Trim();

            if (string.IsNullOrEmpty(serialNO))
            {
                MessageBox.Show("Please input a serial number.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.txtSN.Focus();
                return;
            }
            else
            {
                dutInfo.DutSerialNumber = serialNO;
            }

            // Load test stage - chongjian.liang 2013.11.20

            if (this.cmbStage.SelectedItem == null)
            {
                MessageBox.Show("Please select a test stage.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.cmbStage.Focus();
                return;
            }
            else
            {
                dutInfo.TestStage = cmbStage.Text.Trim();
            }

            // Load device type - chongjian.liang 2013.11.20

            if (this.DeviceTypeCombo.SelectedItem == null)
            {
                MessageBox.Show("Please select a device type.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            else
            {
                DeviceTypeLookupItem selectedDeviceType = (DeviceTypeLookupItem)this.DeviceTypeCombo.SelectedItem;

                dutInfo.PartCode = selectedDeviceType.DeviceType;
                dutInfo.ChirpType = selectedDeviceType.ChirpType;
            }

            // Send dut info - chongjian.liang 2013.11.20

            sendToWorker(dutInfo);
        }

        private void ManualLoadBatchDialogueCtl_MsgReceived(object payload, long inMsgSeq, long respSeq)
        {
            // Get test stage list from file. - Added by chongjian.liang
            if (payload is List<string>)
            {
                cmbStage.Items.AddRange((payload as List<string>).ToArray());

                cmbStage.SelectedIndex = 3;
            }
            // Use combobox instead of ListView. - chongjian.liang 2013.11.20
            else if (payload is Dictionary<string, DeviceTypeLookupItem>)
            {
                foreach (DeviceTypeLookupItem item in ((Dictionary<string, DeviceTypeLookupItem>)payload).Values)
                {
                    this.DeviceTypeCombo.Items.Add(item);
                }

                this.DeviceTypeCombo.DisplayMember = "DeviceType";
            }
            // Select the device type from previous stage's test result - chongjian.liang 2013.11.20
            else if (payload is DeviceTypeLookupItem)
            {
                this.DeviceTypeCombo.SelectedItem = payload;
                this.DeviceTypeCombo.Enabled = true;
            }
            else if (payload is string)
            {
                string msg = payload as string;

                // Alert the user to input the correct serial number - chongjian.liang 2013.11.20
                if (msg == "LastStageMissing" || msg == "LastStageFailed")
                {
                    this.DeviceTypeCombo.Text = "";
                    this.DeviceTypeCombo.Enabled = true;
                    this.cmbStage.Enabled = true;
                    this.txtSN.Enabled = true;

                    if (msg == "LastStageMissing")
                    {
                        if (this.IsQualTest)
                        {
                            MessageBox.Show(string.Format("Missing final result for serial number \"{0}\".", this.txtSN.Text));
                        }
                        else
                        {
                            MessageBox.Show(string.Format("Missing map result for serial number \"{0}\".", this.txtSN.Text));
                        }
                    }
                    else if (msg == "LastStageFailed")
                    {
                        if (this.IsQualTest)
                        {
                            MessageBox.Show(string.Format("Final result is failed for serial number \"{0}\".", this.txtSN.Text));
                        }
                        else
                        {
                            MessageBox.Show(string.Format("Map result is failed for serial number \"{0}\".", this.txtSN.Text));
                        }
                    }

                    this.txtSN.Focus();
                    this.txtSN.SelectAll();
                }
            }
            // For RMA & GRR test - chongjian.liang 2016.2.29
            else if (payload is Msg_NoMesRequestWithSN)
            {
                this.txtSN.Text = (payload as Msg_NoMesRequestWithSN).SerialNO;
                this.txtSN.Enabled = false;

                if (payload is Msg_NoMesRequestWithSNAndPartCode)
                {
                    this.isGRRTest = true;
                }
            }
        }

        /// <summary>
        /// Check and load the device type for the inputted serial number. - chongjian.liang 2013.11.20
        /// </summary>
        private void DeviceTypeCombo_Enter(object sender, EventArgs e)
        {
            if (this.DeviceTypeCombo.SelectedItem == null)
            {
                if (string.IsNullOrEmpty(this.txtSN.Text.Trim()))
                {
                    MessageBox.Show("Please input a serial number.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    
                    this.txtSN.Focus();
                    this.txtSN.SelectAll();
                }
                else if (this.cmbStage.SelectedItem == null)
                {
                    MessageBox.Show("Please select a test stage.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    
                    this.cmbStage.Focus();
                }
                else if (!isGRRTest)
                {
                    this.DeviceTypeCombo.Text = "Please wait..";
                    this.DeviceTypeCombo.Enabled = false;
                    this.cmbStage.Enabled = false;
                    this.txtSN.Enabled = false;

                    string msg = this.txtSN.Text.Trim();

                    if (this.IsQualTest)
                    {
                        msg += "qual";
                    }

                    sendToWorker(msg);
                }
            }
        }

        private bool IsQualTest
        {
            get { return this.cmbStage.Text.ToLower().Contains("qual"); }
        }

        private bool isGRRTest = false;
    }
}

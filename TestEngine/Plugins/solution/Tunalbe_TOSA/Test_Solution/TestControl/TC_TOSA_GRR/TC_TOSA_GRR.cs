// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestControl
//
// TC_TOSA_FINAL.cs
//
// Author: alice.huang, 2010
// Design: [Reference design documentation]

using System;
using System.Xml;
using System.Collections;
using System.Collections.Generic;
using Bookham.TestEngine.Framework.Exceptions;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.PluginInterfaces.TestControl;
using Bookham.TestEngine.PluginInterfaces.TestJig;
using Bookham.TestEngine.PluginInterfaces.MES;
using Bookham.TestEngine.PluginInterfaces.ExternalData;
using Bookham.TestEngine.PluginInterfaces.Security;
using System.Collections.Specialized;
using Bookham.TestSolution.IlmzCommonUtils;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Framework.Limits;
using Bookham.TestLibrary.Utilities;
using System.IO;

namespace Bookham.TestSolution.TestControl
{
    /// <summary>
    /// Test Control Plug-in Template.
    /// NB: Implements a Test Control with a single device in the batch.
    /// The following parameters are hardcoded in the DUTObject and will need to be changed,
    /// either by replacement value or by reading the MES:
    /// - ProgramPluginName and Version
    /// - PartCode
    /// - TestStage
    /// </summary>
    public class TC_TOSA_GRR : ITestControl
    {
        #region Constructor
        /// <summary>
        /// Constructor. 
        /// </summary>
        public TC_TOSA_GRR()
        {
            // initialise the Batch View tab control array (private variable)
            this.batchCtlList = new Type[1];
            this.batchCtlList[0] = typeof(BatchViewCtl);  // show dut infomation
            // TODO: Add any new controls here

            // initialise the Test Control tab control array (private variable)
            this.testCtlList = new Type[2];
            this.testCtlList[0] = typeof(LoadBatchDialogueCtl);  // mes online
            this.testCtlList[1] = typeof(ManualLoadBatchDialogueCtl);  // Mes offline, input Dut info manually
            // TODO: Add any new controls here
        }

        #endregion

        #region ITestControl Implementation.

        /// <summary>
        /// Get Property.
        /// Informs the Test Engine of all the allowed Batch View GUI User Controls
        /// NB: DON'T CHANGE THIS!!!
        /// </summary>
        public Type[] BatchContentsCtlTypeList
        {
            get
            {
                return (batchCtlList);
            }
        }

        /// <summary>
        /// Get Property.
        /// Informs the Test Engine of all the allowed Batch View GUI User Controls
        /// NB: DON'T CHANGE THIS!!!
        /// </summary>
        public Type[] TestControlCtlTypeList
        {
            get
            {
                return (testCtlList);
            }
        }

        /// <summary>
        /// Get Property.
        /// This specifies whether the Test Control Plug-in operated in Manual or Automatic Mode.
        /// </summary>
        public AutoManual TestType
        {
            get
            {
                // TODO: Update. Return AutoManual.AUTO if this is an automatic mode Test Control Plug-in.
                return AutoManual.AUTO;
            }
        }

        /// <summary>
        /// Get CH_L_MZ_VCM Data From Package Build stage
        /// </summary>
        /// <param name="engine">test engine</param>
        /// <param name="serialNo">serialNo</param>
        private double GetDataFromCocFinal(ITestEngine engine, string serialNo)
        {
            IDataRead dataReader = engine.GetDataReader();
            string str_CocSn = "";

            if (serialNo.Contains(".1"))
            {
                serialNo = serialNo.Replace(".1", ".001");
            }

            StringDictionary paramKey = new StringDictionary();
            paramKey.Add("SCHEMA", "HIBERDB");
            paramKey.Add("SERIAL_NO", serialNo);
            paramKey.Add("TEST_STAGE", "package build");
            DatumList paramDataCocSn = dataReader.GetLatestResults(paramKey, true);

            if (paramDataCocSn != null)
            {

                str_CocSn = paramDataCocSn.GetDatumString("COC_SN").ValueToString();


                if (str_CocSn == null)//|| str_ChipId == null)
                {
                    engine.ErrorRaise("Please Check Param:¢CocSn¡");
                    return 0.0;
                }
            }
            else
            {
                str_CocSn = "No Data";
                return 0.0;
                //engine.ErrorInProgram(string.Format("There is no Param Data,Please Check "));
            }



            double dCH_L_MZ_VCM = 0.0;
            //StringDictionary paramKey = new StringDictionary();
            /*paramKey.Remove("TEST_STAGE");
            paramKey.Remove("SERIAL_NO");
            paramKey.Remove("SCHEMA");*/
            paramKey.Clear();
            paramKey.Add("SCHEMA", "COC");
            paramKey.Add("SERIAL_NO", str_CocSn);
            paramKey.Add("TEST_STAGE", "coc_final");


            DatumList paramData = dataReader.GetLatestResults(paramKey, true);

            if (paramData != null)
            {

                dCH_L_MZ_VCM = paramData.GetDatumDouble("CH_L_MZ_VCM").Value;//double.Parse(paramData.GetDatumString("P_AT_XMA").ValueToString());
            }
            else
            {
                //    str_WaferId = "No Data";
                //    str_CocSn = "No Data";
                //    str_ChipId = "No Data";
                engine.ErrorRaise(string.Format("There is no coc_final data, please check it in pcas!"));
            }
            return dCH_L_MZ_VCM;
        }


        private int GetDataFromGBFinal(ITestEngine engine, string serialNo)
        {
            int retest_time = 0;

            IDataRead dataReader = engine.GetDataReader();

            if (serialNo.Contains(".1"))
            {
                serialNo = serialNo.Replace(".1", ".001");
            }

            StringDictionary paramKey = new StringDictionary();
            paramKey.Add("SCHEMA", "HIBERDB");
            paramKey.Add("SERIAL_NO", serialNo);
            paramKey.Add("TEST_STAGE", "final");
            DatumList paramDataFinalRetest = dataReader.GetLatestResults(paramKey, true);

            if (paramDataFinalRetest != null)
            {

                retest_time = int.Parse(paramDataFinalRetest.GetDatumString("RETEST").Value);

            }
            else
            {
                //为空则判定重测次数为0
                retest_time = 0;
                //return 0.0;
                //engine.ErrorInProgram(string.Format("There is no Param Data,Please Check "));
            }

            return retest_time;
        }

        /// <summary>
        /// Load a batch. Called when the Core requests that a new batch be loaded from the MES, via the 
        /// Test Control Server.
        /// </summary>
        /// <param name="testEngine">Object implementing the ITestEngine Interface.</param>
        /// <param name="mes">Object implementing the IMES interface.</param>
        /// <param name="operatorName">The name of the currently logged in user.</param>
        /// <param name="operatorType">The privilege level of the currently logged in user.</param>
        /// <returns>The BatchId of the loaded batch.</returns>
        public string LoadBatch(ITestEngine testEngine, IMES mes,
                        string operatorName, TestControlPrivilegeLevel operatorType)
        {
            testEngine.SendStatusMsg("Load Batch");

            if (TC_Common.IsToCheckSoftwareUpdate)
            {
                //CheckSoftwareUpdate.CheckUpdate(SOFTWARE_ID.HITT_GB_Test);
            }

            // Get the Node from IMES (e.g. For FactoryWorks this is set by it's config XML file)
            this.inSimulationMode = testEngine.InSimulationMode;
            this.mes = mes;

            DutInfo = null;
            readTestControlConfig(testEngine); 
            
            deviceTypeInfo =
                TosaDeviceTypeConfig.LoadDeviceTypeLookup("configuration/TOSA Final/" + "DeviceTypeLookup.csv");
            if ((deviceTypeInfo == null) || (deviceTypeInfo.Count < 1))
            {
                testEngine.ErrorRaise(
                    " can't find config file to provide device type and Chirp type mapping message");
                return null;
            }

            Type typeOf_LoadBatchDialogueCtl = null;

            testEngine.PageToFront(TestControlCtl.TestControl);
            testEngine.GetUserAttention(TestControlCtl.TestControl);

            bool isGRRTest = false;
            string grrPartCode = null;

            while (true)
            {
                object msg;

                DatumList lastStageResult = null;

                bool isLastStagePassed = false;

                bool isQualTest = false;
                string deviceType="";
                //d'nt check batchid 
                isGRRTest = true;
                grrPartCode = "Hitt_GR&R";
               /* if (this.isMesOnline)
                {
                    #region Auto Load Batch

                    typeOf_LoadBatchDialogueCtl = typeof(LoadBatchDialogueCtl);
                    testEngine.SelectTestControlCtlToDisplay(typeOf_LoadBatchDialogueCtl);

                    testEngine.SendToCtl(TestControlCtl.TestControl, typeOf_LoadBatchDialogueCtl, new MesLoadDeviceMsg("Please input a batch to load", true, true));
                    testEngine.SendToCtl(TestControlCtl.TestControl, typeOf_LoadBatchDialogueCtl, deviceTypeInfo);

                    msg = testEngine.WaitForCtlMsg().Payload;

                    if (msg is Msg_NoMesRequest)
                    {
                        this.isMesOnline = false;
                        this.isDutTrackOut = false;

                        // To fill and disable the SN textbox in ManualLoadBatchDialogueCtl
                        if (msg is Msg_NoMesRequestWithSN)
                        {
                            if (msg is Msg_NoMesRequestWithSNAndPartCode)
                            {
                                isGRRTest = true;
                                grrPartCode = (msg as Msg_NoMesRequestWithSNAndPartCode).PartCode;
                            }

                            testEngine.SendToCtl(TestControlCtl.TestControl, typeof(ManualLoadBatchDialogueCtl), msg);
                        }

                        continue;
                    }
                    else if (msg is DatumString)
                    {
                        DatumString ds = msg as DatumString;

                        if (ds.Name == "batchID")
                        {
                            DutInfo = new DutLoadInfo();
                            DutInfo.DutLotID = ds.Value;
                        }
                    }

                    // Check SPC 2016.09.02
                    if (!TC_Common.CheckSPCStatus(testEngine, mes.Node))
                    {
                        continue;
                    }
            
                    isDutTrackOut = true;//need track out if auto test

                    try
                    {
                        dutBatch = mes.LoadBatch(DutInfo.DutLotID, true);

                        if (dutBatch[0] != null)
                        {
                            // Confirm the factorywork stage - chongjian.liang 2015.9.6
                            if (dutBatch.Stage != "FINALTEST" && dutBatch.Stage != "MAPTEST")
                            {
                                testEngine.ShowContinueUserQuery(TestControlTab.TestControl, string.Format("The current step in the factorywork should be FINALTEST instead of {0} for this test.", dutBatch.Stage));

                                testEngine.SendToCtl(TestControlCtl.TestControl, typeof(LoadBatchDialogueCtl), new MesLoadDeviceMsg("Please input a batch to load", true, true));

                                continue;
                            }

                            this.mes.TrackIn(DutInfo.DutLotID);

                            if (dutBatch.Stage != "MAPTEST")
                            {
                                testEngine.SendToCtl(TestControlCtl.TestControl, typeOf_LoadBatchDialogueCtl, deviceTypeInfo[dutBatch.PartCode]);
                            }
                            else//为maptest 需要把整个deviceTypeInf保存到tmp里 通过ChirpType过滤 并发送给loadbatchdialogue
                            {

                                System.Collections.Generic.Dictionary<string, DeviceTypeLookupItem> deviceTypeInfoTmp=new Dictionary<string,DeviceTypeLookupItem>();

                                foreach (DeviceTypeLookupItem itemtmp in deviceTypeInfo.Values)
                                {

                                    if (deviceTypeInfo[dutBatch.PartCode].ChirpType == itemtmp.ChirpType)
                                    {
                                        deviceTypeInfoTmp.Add(itemtmp.DeviceType, itemtmp);
                                    }
                                }
                                 
                                if (deviceTypeInfo[dutBatch.PartCode].ChirpType == "ZD")
                                {
                                    double dCH_L_MZ_VCM = this.GetDataFromCocFinal(testEngine, dutBatch[0].SerialNumber);
                                    int retestTime = this.GetDataFromGBFinal(testEngine, dutBatch[0].SerialNumber);

                                    //testEngine.SendToCtl(TestControlCtl.TestControl, typeOf_LoadBatchDialogueCtl, new DatumString("PartCode", DutInfo.PartCode));

                                    if (retestTime == 0)//非返工料需要判定是否不小于3.9 不小于3.9不能选PA010199
                                    {
                                        if (dCH_L_MZ_VCM < 3.9)
                                        {
                                            deviceTypeInfoTmp.Remove("PA010199");
                                            // deviceTypeInfo.Remove("PA010199");
                                            //testEngine.SendToCtl(TestControlCtl.TestControl, typeOf_LoadBatchDialogueCtl, new DatumStringArray("typeListPA0010199", typeList.ToArray()));
                                            //testEngine.SendToCtl(TestControlCtl.TestControl, typeOf_LoadBatchDialogueCtl, new DatumString("PA010199","PA010199"));
                                        }
                                    }

                                    testEngine.SendToCtl(TestControlCtl.TestControl, typeOf_LoadBatchDialogueCtl, deviceTypeInfoTmp);
                                }
                                testEngine.SendToCtl(TestControlCtl.TestControl, typeOf_LoadBatchDialogueCtl, deviceTypeInfoTmp[dutBatch.PartCode]);

                            }
                        }
                        else
                        {
                            testEngine.SendToCtl(TestControlCtl.TestControl, typeof(LoadBatchDialogueCtl), new MesLoadDeviceMsg("Please input a batch to load", true, true));

                            continue;   // if no available dut in batch, go on batch load loop
                        }
                    }
                    catch(Exception e1)
                    {
                        string msgEx = string.Format("Failed to load or check in batch {0}.", DutInfo.DutLotID);

                        testEngine.SendStatusMsg(msgEx);

                        testEngine.ShowContinueUserQuery(TestControlTab.TestControl, msgEx);

                        testEngine.SendToCtl(TestControlCtl.TestControl, typeof(LoadBatchDialogueCtl), new MesLoadDeviceMsg("Please input a batch to load", true, true));

                        continue;
                    }

                    if (!deviceTypeInfo.ContainsKey(dutBatch.PartCode))
                    {
                        testEngine.ErrorRaise(" can't find device type & chirp type" +
                            "mapping message for " + dutBatch.PartCode +
                                " \n please check the device type & chirp type " +
                                "message mapping configuration file and ensure " +
                                " this device type is available to test with this test control");
                        continue;
                    }

   

                    msg = testEngine.WaitForCtlMsg().Payload;

                    DeviceTypeLookupItem deviceTypeLookupItem = (DeviceTypeLookupItem)msg;

                    DutInfo.PartCode = deviceTypeLookupItem.DeviceType;
                    dutBatch.PartCode = deviceTypeLookupItem.DeviceType;
                    DutInfo.ChirpType = deviceTypeLookupItem.ChirpType;
                    DutInfo.DutSerialNumber = dutBatch[0].SerialNumber;


                   

                    if(dutBatch.Stage == "FINALTEST")
                    {
                        DutInfo.TestStage = "final";
                    }
                    else if (dutBatch.Stage == "MAPTEST")
                    {
                         DutInfo.TestStage = "hitt_map";
                    }
                    //else if()
                    DutInfo.LotType = dutBatch.Attributes.GetDatumString("lot_type").ValueToString();

                    break;

                    #endregion
                }
                else
                {*/
                    #region Manual Load DutInfo

                    typeOf_LoadBatchDialogueCtl = typeof(ManualLoadBatchDialogueCtl);
                    testEngine.SelectTestControlCtlToDisplay(typeOf_LoadBatchDialogueCtl);

                    List<string> stageList;

                    ReadStagesFromFile(testEngine, TESTSTAGE_PATH, out stageList);

                    testEngine.SendToCtl(TestControlCtl.TestControl, typeOf_LoadBatchDialogueCtl, stageList);
                    testEngine.SendToCtl(TestControlCtl.TestControl, typeOf_LoadBatchDialogueCtl, deviceTypeInfo);

                    if (isGRRTest)
                    {
                        /*testEngine.SendToCtl(TestControlCtl.TestControl, typeOf_LoadBatchDialogueCtl, deviceTypeInfo[grrPartCode]);
                    }
                    else
                    {*/
                        #region Get device type from last stage's test result - chongjian.liang 2013.11.20

        
                        do
                        {
                            msg = testEngine.WaitForCtlMsg().Payload;

                            // Check SPC 2016.09.02
                            if (!TC_Common.CheckSPCStatus(testEngine, mes.Node))
                            {
                                continue;
                            }
            
                            if (msg is string)
                            {
                                string msgString = msg as string;

                                string serialNO = msgString;
                                string lastStage = "hitt_map";

                                // If the user chooses to do qual test
                                if (msgString.Contains("qual"))
                                {
                                    serialNO = serialNO.Replace("qual", "");
                                    lastStage = "final";
                                    isQualTest = true;
                                }

                                StringDictionary keys = new StringDictionary();
                                keys.Add("SCHEMA", "hiberdb");
                                keys.Add("SERIAL_NO", serialNO);
                                keys.Add("TEST_STAGE", lastStage);

                                lastStageResult = testEngine.GetDataReader("PCAS_SHENZHEN").GetLatestResults(keys, false);

                                // Prevent the user from proceeding when serialNO is wrong
                                if (lastStageResult == null
                                    || lastStageResult.Count == 0)
                                {
                                    testEngine.SendToCtl(TestControlCtl.TestControl, typeOf_LoadBatchDialogueCtl, "LastStageMissing");
                                }
                                // Read device type and send to GUI
                                else
                                {
                                    isLastStagePassed = lastStageResult.ReadString("TEST_STATUS").ToLower().Contains("pass");

                                    if (!isLastStagePassed && !isQualTest)
                                    {
                                        testEngine.SendToCtl(TestControlCtl.TestControl, typeOf_LoadBatchDialogueCtl, "LastStageFailed");
                                    }
                                    else
                                    {
                                        deviceType = lastStageResult.ReadString("PRODUCT_CODE");

                                        testEngine.SendToCtl(TestControlCtl.TestControl, typeOf_LoadBatchDialogueCtl, deviceTypeInfo[deviceType]);
        
                                    }
                                }
                            }
                        } while (lastStageResult == null || (!isLastStagePassed && !isQualTest));
                        #endregion
                    }

                    do
                    {
                        msg = testEngine.WaitForCtlMsg().Payload;

                        if (deviceTypeInfo[deviceType].ChirpType == "ZD")
                        {

                            testEngine.SendStatusMsg("GR&R 暂时只支持NC");
                            //testEngine.( "GR&R 暂时只支持NC");
                            //return "";
                        }

                        if (msg is DutLoadInfo)
                        {
                            this.DutInfo = (DutLoadInfo)msg;
                            //this.DutInfo.Attributes.AddString("lot_type", "Engineering");
                            this.DutInfo.LotType = "Engineering";
                        }
                    } while (this.DutInfo == null);

                    isDutTrackOut = false;//no need track out if manual test

                    break;

                    #endregion
                //}
            }

            // ... Clear up GUI.
            testEngine.CancelUserAttention(TestControlCtl.TestControl);
            testEngine.GuiClear(TestControlCtl.TestControl);
            // Reset one-time run flag
            programRanOnce = false;
            // Return serial number to Test Engine

            isFirstLoadBatch = false;
            testEngine.SendStatusMsg("Batch load completed!");
            return DutInfo.DutLotID;
        }

        private bool CheckStageStatus(string stageName)
        {
            bool status = false;

            if (stageName == "finalottest")
                return true;

            if (stageName == "finaltest")
                return true;

            if (stageName == "maptest")
                return true;

            return false;
        }

        private bool LoadBatchAndTrackin(ITestEngine testEngine)
        {
            try
            {
                //this.mes.Node = node;
                testEngine.SendToCtl(TestControlCtl.TestControl, typeof(LoadBatchDialogueCtl),
                    new MesLoadDeviceMsg(" Retrieve batch information from MES, Please wait ... ", false, false));
                testEngine.SendStatusMsg("Retrieving Batch information from MES ...");

                //MES Load Batch
                this.dutBatch = mes.LoadBatch(DutInfo.DutLotID, true);//this.mes.LOTQUERYFULL_NEW(DutInfo.BatchID, UserId);
                if (dutBatch[0] != null)
                {
                    // if batch information has been retrieve, process the information
                    DutInfo.PartCode = dutBatch.PartCode;

                    DutInfo.LotType = dutBatch.Attributes.ReadString("lot_type");

                    //MES Trackin
                    mes.TrackIn(DutInfo.DutSerialNumber);

                    if (!CheckStageStatus(dutBatch[0].TestStage.ToLower().Trim()))   // may be can set it as string "stage in factory work"in configration file
                    {
                        testEngine.SendStatusMsg("Batch is in incorrected stage in Mes");
                        testEngine.ShowUserQuery(TestControlTab.TestControl,
                            " the batch atay in stage of " + dutBatch[0].TestStage +
                            " \n so it can't process in data deliver stage, please reload batch!",
                            new Bookham.TestEngine.PluginInterfaces.TestControl.ButtonInfo("&Continue", null));
                        testEngine.SendToCtl(TestControlCtl.TestControl,
                            typeof(LoadBatchDialogueCtl),
                            new MesLoadDeviceMsg("  Waiting to load new batch", true, true));

                        return false;
                    }
                    if (deviceTypeInfo.ContainsKey(DutInfo.PartCode))
                    {
                        DutInfo.ChirpType = deviceTypeInfo[DutInfo.PartCode].ChirpType;
                    }
                    else
                    {
                        testEngine.ErrorRaise(" can't find device type & chirp type" +
                            "mapping message for " + DutInfo.PartCode +
                                " \n please check the device type & chirp type " +
                                "message mapping configuration file and ensure " +
                                " this device type is available to test with this test control");

                        testEngine.SendToCtl(TestControlCtl.TestControl,
                            typeof(LoadBatchDialogueCtl),
                            new MesLoadDeviceMsg("  Waiting to load new batch", true, true));

                        return false;
                    }
                }// if available mes information has been achieve
                else
                {
                    return false;   // if no available dut in batch, go on batch load loop
                }


            }
            catch (Exception ex)
            {
                testEngine.SendStatusMsg("MES Communications exception." + ex.Message + ex.StackTrace);
                testEngine.ShowUserQuery(TestControlTab.TestControl,
                            "Loadbatch error " + ex.Message,
                            new Bookham.TestEngine.PluginInterfaces.TestControl.ButtonInfo("&Continue", null));
                testEngine.SendToCtl(TestControlCtl.TestControl,
                            typeof(LoadBatchDialogueCtl),
                            new MesLoadDeviceMsg("  Waiting to load new batch", true, true));
                //Just start over.
                return false;
            }

            return true;

            #endregion
        }
        /// <summary>
        /// Get the next available DUT from the Batch.
        /// </summary>
        /// <param name="testEngine">Object that implements the ITestEngine Interface.</param>
        /// <param name="testJigArg">Object tha implements ITestJig Interface.</param>
        /// <param name="operatorName">The name of the currently logged in user.</param>
        /// <param name="operatorType">Privilege level of the currently logged in user.</param>
        /// <returns>DUT object for the next selected device for test or Null if end of batch.</returns>
        public DUTObject GetNextDUT(ITestEngine testEngine, ITestJig testJigArg, 
                    string operatorName, TestControlPrivilegeLevel operatorType)
        {
            dut = new DUTObject();
            if (this.programRanOnce) return null; // end of batch after one run!
            //testEngine.SelectBatchCtlToDisplay(typeof(BatchViewCtl));
            //testEngine.PageToFront(TestControlCtl.BatchView);
            /*if (!isFirstLoadBatch)
            {
 

                if (tsetStage == dutBatch.Stage)
                {
                    return null;
                }
                else
                {
                    tsetStage = dutBatch.Stage.ToUpper();
                }
            }*/
                   //If is first load batch then skip load function
            /*if (!isFirstLoadBatch)
            {
                if (!LoadBatchAndTrackin(testEngine))
                {
                    return null;
                }

                if (tsetStage == dutBatch.Stage)
                {
                    return null;
                }
                else
                {
                    tsetStage = dutBatch.Stage.ToUpper();
                }
            }
             else
            {
                isFirstLoadBatch = false;
               
                    tsetStage = dutBatch.Stage.ToUpper();
                    if (tsetStage == "FINALTEST")
                    {
                        dut.TestStage = "final";
                    }
                    else if (tsetStage == "MAPTEST")
                    {
                        dut.TestStage = "hitt_map";
                    }
         
            //} */
            testEngine.SelectBatchCtlToDisplay(typeof(BatchViewCtl));
            testEngine.PageToFront(TestControlCtl.BatchView);

            //Comstruct DUTObject - test engine get required program, serial no etc. from here

            if (isDutTrackOut)  //raul added to check if auto test or manual test
            {
                dut.BatchID = DutInfo.DutLotID;  //from Load Batch
            }
            else
            {
                dut.BatchID = DutInfo.DutSerialNumber;  //if manual test the batchid = sn
            }

            dut.SerialNumber = DutInfo.DutSerialNumber;
            dut.PartCode = DutInfo.PartCode;
            //dut.TestStage = DutInfo.TestStage;   
            //tsetStage = dutBatch.Stage.ToUpper();
 
            dut.NodeID = this.mes.Node;
            dut.TestJigID = testJigArg.GetJigID(testEngine.InDebugMode, testEngine.InSimulationMode);
            dut.Attributes.AddString("lot_type", this.DutInfo.LotType);

            /*if (tsetStage == "FINALTEST")//dut.TestStage == "final")
            {
                dut.TestStage = "final";
                switch (DutInfo.ChirpType)
                {
                    case "ND":
                        dut.ProgramPluginName = "Prog_ILMZ_Final_ND";
                        break;
                    case "ZD":
                        dut.ProgramPluginName = "Prog_ILMZ_Final_ZD";
                        break;
                    default:
                        testEngine.ErrorRaise("Unhandled device type '" + DutInfo.ChirpType + "'");
                        break;
                }
            }
            else if (tsetStage == "MAPTEST")
            {
                dut.TestStage = "hitt_map";
                this.dut.ProgramPluginName = "Prog_ILMZ_Map";
            }
    
            else
            {
                return null;
            }*/
            dut.ProgramPluginVersion = "";
            this.dut.Attributes.AddBool("Autostage", true);
            if (dut.Attributes.GetDatumString("lot_type").ValueToString().ToLower().Equals("production"))
            {
                try
                {

                    //add max continous test count
                    if (RetestOutofRange(testEngine, dut.TestStage, dut))
                    {
                        testEngine.ShowContinueUserQuery(TestControlTab.TestControl,
                            string.Format("Retest count out of spec, testing denied! MaxTestCount[{0}]", this.maxTestCount));

                        return null;
                    }
                }
                catch { }
            }
            dut.TestStage = DutInfo.TestStage;//"final";
            switch (dut.TestStage)
            {
                case "FinalGRR":
                    //dut.TestStage = "Final
                    //
                    dut.PartCode = DutInfo.PartCode;
                    dut.TestStage = "hitt debug01";//dut.TestStage;
                    dut.Attributes.AddString("ChirpType", DutInfo.ChirpType);
                    dut.ProgramPluginName = "Prog_ILMZ_SPC_new";
                    break;
                   /*dut.TestStage = "Final";
                    dut.ProgramPluginName = "Prog_ILMZ_Final_ND";
                    dut.Attributes.AddBool("GR&R", true);
                    break;*/
                case "Final":
                    if (DutInfo.ChirpType == "ND")
                    {
                        dut.ProgramPluginName = "Prog_ILMZ_Final_ND";
                    }
                    else if (DutInfo.ChirpType == "ZD")
                    {
                        dut.ProgramPluginName = "Prog_ILMZ_Final_ZD";
                    }
                    else
                    {
                        testEngine.ErrorRaise("Unhandled device type '" + DutInfo.ChirpType + "'");
                        break;
                    }
                    break;
                case "Final_OT":
                    dut.ProgramPluginName = "Prog_ILMZ_OverTemp";
                    break;
                case "hitt_Map":
                    dut.ProgramPluginName = "Prog_ILMZ_Map";
                    break;
                    break;
                case "Post Burn":
                    dut.ProgramPluginName = "Prog_PostPhaseCutScan";
                    break;
                case "Pre Burn":
                    dut.ProgramPluginName = "Prog_PrePhaseCutScan";
                    break;
                case "Qual":
                case "Qual0":
                    dut.Attributes.AddString("ChirpType", DutInfo.ChirpType);
                    dut.ProgramPluginName = "Prog_ILMZ_Qual";
                    break;
                default:
                    testEngine.ErrorRaise("Unhandled  prog'"); 
                    break;

            }
            //dut.PartCode = "HITT_GR&R";

            testEngine.GetUserAttention(TestControlCtl.BatchView);
            testEngine.SendToCtl(TestControlCtl.BatchView, typeof(BatchViewCtl), dut);

  
            testEngine.CancelUserAttention(TestControlCtl.BatchView);
            this.programRanOnce = true;
            return dut;
 
         
        }


        #region[add   for retest issue    ------------add by Dong.Chen ]                                                  ]
        private bool RetestOutofRange(ITestEngine engine, string stage, DUTObject dut)
        {
            StringDictionary mapKeys = new StringDictionary();

            mapKeys.Add("SCHEMA", "hiberdb");
            mapKeys.Add("SERIAL_NO", dut.SerialNumber);//this.serialNum);
            mapKeys.Add("TEST_STAGE", dut.TestStage);  //this.pcasTestStage);
            //mapKeys.Add("DEVICE_TYPE", dut.PartCode);  //this.pcasDeviceType);//这个要加上，因为测试次数达到最大时，可以转Code再测试。

            DatumList list = engine.GetDataReader().GetLatestResults(mapKeys, false);

            if (list.IsPresent("RETEST"))
            {
                string retest = list.GetDatumString("RETEST").ValueToString();
                //string retest = list.ReadDouble("RETEST").ToString();   // list["RETEST"].ValueToString();


                engine.SendStatusMsg("RETEST:" + retest);

                int CL = this.ReadRetest(stage);
                this.maxTestCount = CL;
                int RC = 999;
                int.TryParse(retest.Trim(), out RC);

                if (RC == CL - 1)
                    engine.ShowContinueUserQuery(TestControlTab.TestControl, "Warning: This is the last time to test this device on this stage!!!");
                else if (RC == CL)
                {
                    return true;
                }
            }
            return false;
        }

        private int ReadRetest(string stage)
        {
            string file = "Configuration\\TOSA Final\\RetestTimeConfiguration.csv";
            if (!File.Exists(file)) return 999;

            int count = 999;

            using (StreamReader sr = new StreamReader(file))
            {
                while (!sr.EndOfStream)
                {
                    string[] line = sr.ReadLine().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                    if (line.Length != 2) continue;

                    if (stage.ToLower() == line[0].Trim().ToLower())
                    {
                        int.TryParse(line[1].Trim(), out count);

                        break;
                    }
                }
            }
            return count;
        }
        #endregion

        /// <summary>
        /// Called after the test program has concluded, to decide what the outgoing 
        /// Part ID and other attributes are, having inspected the test results.
        /// </summary>
        /// <param name="testEngine">Reference to object implmenting ITestEngine</param>
        /// <param name="programStatus">The Status of the Test Program Run (succeeded/error/aborted etc).</param>
        /// <param name="testResults">Detailed test results</param>
        /// <returns>Outcome of the results analysis.</returns>
        public DUTOutcome PostDevice(ITestEngine testEngine, ProgramStatus programStatus, TestResults testResults)
        {
            testEngine.SendStatusMsg("Post Device");
            this.programStatus = programStatus;
            this.testResults = testResults;
            // Extract the Specification name from the test results. Fill this in the DUToutcome.
            SpecResultsList specs = testResults.SpecResults;
            string[] specNames = new string[specs.Count];


            if (dut.TestStage.ToLower() == "hitt_map")//判断为MAP_TEST
            {

                int supermodeNumber = int.MaxValue;
                string smSpecNameStub = "";

                if (dut.Attributes.IsPresent("supermodeNumber"))
                {
                    supermodeNumber = dut.Attributes.ReadSint32("supermodeNumber");
                }
                if (dut.Attributes.IsPresent("SupermodeSpecNameStub"))
                {
                    smSpecNameStub = dut.Attributes.ReadString("SupermodeSpecNameStub");
                }

                List<string> specsToUse = new List<string>();

                foreach (SpecResults specRes in testResults.SpecResults)
                {
                    string specName = specRes.Name;
                    if (smSpecNameStub.Length > 0 && specName.Contains(smSpecNameStub) && specName.Contains("_"))
                    {
                        int lastUnderScore = specName.LastIndexOf("_");
                        int smNumber = int.Parse(specName.Substring(lastUnderScore + 1, specName.Length - lastUnderScore - 1));
                        if (smNumber < supermodeNumber)
                            specsToUse.Add(specName);
                    }
                    else
                    {
                        specsToUse.Add(specName);
                    }
                }

                dutOutcome.DUTData = new DatumList();
                dutOutcome.OutputSpecificationNames = specsToUse.ToArray();
            }
            else
            {
                int i = 0;
                foreach (SpecResults specRes in specs)
                {
                    specNames[i++] = specRes.Name;
                }



                dutOutcome.DUTData = new DatumList();
                dutOutcome.OutputSpecificationNames = new string[1];
                dutOutcome.OutputSpecificationNames[0] = specNames[0];
            }
            return dutOutcome;
        }

        /// <summary>
        /// Commit any post-test MES updates prepared in method PostDevice.
        /// </summary>
        /// <param name="testEngine">Object implementing ITestEngine</param>
        /// <param name="dataWriteError">True if there was a problem writing Test Results to the database.</param>
        public void PostDeviceCommit(ITestEngine testEngine, bool dataWriteError)
        {
            //TODO: For those Test Software Solutions which interact with an MES, store the test
            //status for the current device under test to the MES.
            testEngine.SendStatusMsg("Post Device Commit");
            ////Mark the Device as Tested. Also record it's pass fail status.
            if (testResults.ProgramStatus.Status == MultiSpecPassFail.AllPass)
            {
                if ((this.programStatus == ProgramStatus.Success) && (!dataWriteError))
                {
                    //The Test Passed. Mark the device as completed.
                    this.TestStatus = TestStatus.Passed;

                    if (isMesOnline && isDutTrackOut)
                    {
                       
                        this.mes.SetBatchAttribute(dut.BatchID, "PartId", dutBatch.PartCode);
                        System.Threading.Thread.Sleep(10000);
                        this.mes.TrackOutBatchPass(dut.BatchID, dutBatch.Stage, dutBatch.PartCode);
                    }
                }
                else if ((this.programStatus == ProgramStatus.Success) && (dataWriteError))
                {
                    //Problem writing Test Data. Mark for retest.
                    this.TestStatus = TestStatus.MarkForRetest;
                }
            }
            else
            {
                this.TestStatus = TestStatus.Failed;

                if (this.dutOutcome.DUTData.ReadSint32("RETEST") >= this.FailTimeTrackTeckAcess)
                {
                    //this.mes.SetBatchAttribute(dut.BatchID, "PartId", dutBatch.PartCode);
                    //System.Threading.Thread.Sleep(5000);
                    this.mes.TrackOutComponentRework(dut.BatchID, dutBatch.Stage, dut.SerialNumber, dutBatch.PartCode);
                }
            }

            //UpdateTestStatuses message = new UpdateTestStatuses(testStatuses);
            //testEngine.SendToCtl(TestControlCtl.BatchView, typeof(DemoBatchViewCtl), message);
            testEngine.SendToCtl(TestControlCtl.BatchView, typeof(BatchViewCtl), TestStatus);
        }

        /// <summary>
        /// The Test Engine has concluded operations on the currently loaded batch. Update Batch level parameters 
        /// in the MES.
        /// </summary>
        /// <param name="testEngine">Reference to object that implements the ITestEngine Interface.</param>
        public void PostBatch(ITestEngine testEngine)
        {
            //TODO: For those Test Software Solutions which interact with an MES, store the test
            //status for the currently loaded Batch in the MES.
            testEngine.SendStatusMsg("Post Batch");
        }

        /// <summary>
        /// Mark all devices in the Batch for retest.
        /// </summary>
        /// <param name="testEngine">Reference to object that implements the ITestEngine Interface.</param>
        public void RestartBatch(ITestEngine testEngine)
        {

            //TODO: Use whatever method is appropriate to your implmentation to mark all devices in the 
            //current batch untested. It is up to the implmenter to decide whether this means restart all 
            //devices tested in this session or whether to restart the entire batch. Where this functionality
            //is not required, the solution developer may choose to take no action here.
            testEngine.SendToCtl(TestControlCtl.BatchView, typeof(BatchViewCtl), DutInfo);
            testEngine.SendStatusMsg("Restart Batch");           
            
            
        }
      

        private void readTestControlConfig(ITestEngine engine)
        {
            if (isTCPliginConfigRead)
                return;

            int result;
            string commonErrorMessageString = "Unable to read Test Control Plug-in configuration File: ";

            //Read Configuration file.
            /* Open user data XML file. This constructor call only conducts a scant validation of the
             * file name. Any expected error will not occur until the first call to Read. */
            XmlTextReader config = new XmlTextReader(CONFIGFilePath + "/TestControlPluginConfig.xml");

            try
            {
                /* Scan file, one element a time. */
                while (config.Read())
                {
                    if (config.NodeType == XmlNodeType.Element)
                    {
                        if (config.LocalName == "UseMES")
                        {
                            string useMes = config.ReadString();

                            result = String.Compare(useMes.Trim(), "yes", true, System.Globalization.CultureInfo.InvariantCulture);
                            if (result == 0)
                            {
                                isMesOnline = true;
                            }
                            else
                            {
                                result = String.Compare(useMes.Trim(), "no", true, System.Globalization.CultureInfo.InvariantCulture);

                                if (result == 0)
                                {
                                    isMesOnline = false;
                                }
                                else
                                {
                                    //Invalid configuration file contents.
                                    engine.ErrorRaise("Invalid UseMES field contents in Configuration/ITLA/TestControlPluginConfig.xml configuration file.");
                                }
                            }  // end else
                        }  // end if "UseMes"

                        if (config.LocalName == "InhibitTrackout")
                        {
                            string trackoutInhibitString = config.ReadString();

                            result = String.Compare(trackoutInhibitString.Trim(), "yes", true);
                            if (result == 0)
                            {
                                isDutTrackOut = false;
                            }
                            else
                            {
                                result = String.Compare(trackoutInhibitString.Trim(), "no", true);

                                if (result == 0)
                                {
                                    isDutTrackOut = true;
                                }
                                else
                                {
                                    //Invalid configuration file contents.
                                    engine.ErrorRaise("Invalid InhibitTrackout field contents in Configuration/ITLA/TestControlPluginConfig.xml configuration file.");
                                }
                            }  // end else: "InhibitTrackout" != "yes"
                        }  // end if "InhibitTrackout"

                        //FailTrackTeckAcess

                        if (config.LocalName == "FailTrackTeckAcess")
                        {
                            string FailTrackTeckAcess = config.ReadString();
                            ///int FailTimeTrackTeckAcess;
                            if (!int.TryParse(FailTrackTeckAcess.Trim(), out this.FailTimeTrackTeckAcess))
                            {
                                //Invalid configuration file contents.
                                engine.ErrorRaise("Invalid InhibitTrackout field contents in Configuration/ITLA/TestControlPluginConfig.xml configuration file.");
                            }

                        }  // end if "Inh
                    } // end if config.NodeType == XmlNodeType.Element: get available text nod
                }  // end while loop

                // Set flag to indicate that configuration has been read.
                isTCPliginConfigRead = true;
            }
            catch (UnauthorizedAccessException uae)
            {

                /* User did not have read permission for file. */
                engine.ErrorRaise(commonErrorMessageString + "Access denied " + uae.Message + uae.StackTrace);
            }
            catch (System.IO.FileNotFoundException fnfe)
            {
                /* File not found error. */
                engine.ErrorRaise(commonErrorMessageString + "Configuration File not found " + fnfe.Message);
            }
            catch (System.IO.IOException ioe)
            {
                /* General IO failure. Not file not found. */
                engine.ErrorRaise(commonErrorMessageString + "General IO Failure " + ioe.Message);
            }
            catch (System.Xml.XmlException xe)
            {
                /* XML parse error. */
                engine.ErrorRaise(commonErrorMessageString + "XMl Parser Error " + xe.Message);
            }
            finally
            {
                /* Close file. */
                if (config != null) config.Close();
            }
        }

        /// <summary>
        /// Read test stage from file. - Added by chongjian.liang
        /// </summary>
        /// <param name="testEngine"></param>
        /// <param name="stageList"></param>
        private void ReadStagesFromFile(ITestEngine testEngine, string filePath, out List<string> stageList)
        {
            stageList = new List<string>();

            CsvReader csvReader = new CsvReader();

            List<string[]> lines = csvReader.ReadFile(filePath);
            
            for (int index = 1; index < lines.Count; ++index)
            {
                stageList.Add(lines[index][0]);
            }
               /*        
             stageList.Add("Final_OT");
             stageList.Add("FinalGRR");
             stageList.Add("Map");
             stageList.Add("Post_Burn");
             stageList.Add("Pre_Burn");*/
        }

        #region Private data
        // Batch View tab controls
        Type[] batchCtlList;
        // Test Control tab controls
        Type[] testCtlList;

        // Flag for the program ran (so we can abort the batch)
        bool programRanOnce = false;
        // Serial number variable
        DutLoadInfo DutInfo;
        /// <summary>
        /// flag to indicate if test control configureation file was read or not
        /// </summary>
        bool isTCPliginConfigRead;
        /// <summary>
        /// Flag to indicate if Fws is available or not
        /// </summary>
        bool isMesOnline;
        /// <summary>
        /// Flag to indicate if dut trackout after test
        /// </summary>
        bool isDutTrackOut;


        string tsetStage = "";
        bool isFirstLoadBatch = true;
        System.Collections.Generic.Dictionary<string, DeviceTypeLookupItem> deviceTypeInfo;

        string operatorID;

        IMES mes;

        MESbatch dutBatch = null;

        ProgramStatus programStatus;
        TestResults testResults;
        TestStatus TestStatus;
        bool inSimulationMode;
        const string CONFIGFilePath = "configuration/TOSA Final/";
        const string TESTSTAGE_PATH = "Configuration/TOSA Final/FinalTest/TestStageLookUp.csv";
        const string DeviceTypePath = "Configuration/TOSA Final/DeviceTypeLookUp.csv";
        DUTObject dut;
        DUTOutcome dutOutcome = new DUTOutcome();
        int maxTestCount = 10;
        int FailTimeTrackTeckAcess = 3;
        #endregion
    }
    /// <summary>
    /// Test Status of devices within a batch
    /// </summary>
    internal enum TestStatus
    {
        /// <summary>
        /// Untested.
        /// </summary>
        Untested,

        /// <summary>
        /// Marked for restest.
        /// </summary>
        MarkForRetest,

        /// <summary>
        /// Passed.
        /// </summary>
        Passed,

        /// <summary>
        /// Failed.
        /// </summary>
        Failed
    }
}

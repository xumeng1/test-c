// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestControl
//
// LoadBatchDialogueCtl.Designer.cs
//
// Author: tommy.yu, 2007
// Design: [Reference design documentation]

namespace Bookham.TestSolution.TestControl
{
    partial class LoadBatchDialogueCtl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoadBatchDialogueCtl));
            this.messageLabel = new System.Windows.Forms.Label();
            this.loadBatchScreenLabel = new System.Windows.Forms.Label();
            this.bookhamLogoPictureBox = new System.Windows.Forms.PictureBox();
            this.loadBatchButton = new System.Windows.Forms.Button();
            this.loadBatchTextBox = new System.Windows.Forms.TextBox();
            this.Button_NoMes = new System.Windows.Forms.Button();
            this.lblBatchID = new System.Windows.Forms.Label();
            this.DeviceTypeFrame = new System.Windows.Forms.GroupBox();
            this.DeviceTypeCombo = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.bookhamLogoPictureBox)).BeginInit();
            this.DeviceTypeFrame.SuspendLayout();
            this.SuspendLayout();
            // 
            // messageLabel
            // 
            this.messageLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.messageLabel.ForeColor = System.Drawing.Color.Red;
            this.messageLabel.Location = new System.Drawing.Point(315, 162);
            this.messageLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.messageLabel.Name = "messageLabel";
            this.messageLabel.Size = new System.Drawing.Size(726, 55);
            this.messageLabel.TabIndex = 5;
            this.messageLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // loadBatchScreenLabel
            // 
            this.loadBatchScreenLabel.AutoSize = true;
            this.loadBatchScreenLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadBatchScreenLabel.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.loadBatchScreenLabel.Location = new System.Drawing.Point(191, 23);
            this.loadBatchScreenLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.loadBatchScreenLabel.Name = "loadBatchScreenLabel";
            this.loadBatchScreenLabel.Size = new System.Drawing.Size(511, 25);
            this.loadBatchScreenLabel.TabIndex = 4;
            this.loadBatchScreenLabel.Text = "Test Engine TestControl - Final: Load Batch Screen";
            // 
            // bookhamLogoPictureBox
            // 
            this.bookhamLogoPictureBox.Image = ((System.Drawing.Image)(resources.GetObject("bookhamLogoPictureBox.Image")));
            this.bookhamLogoPictureBox.Location = new System.Drawing.Point(9, 9);
            this.bookhamLogoPictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.bookhamLogoPictureBox.Name = "bookhamLogoPictureBox";
            this.bookhamLogoPictureBox.Size = new System.Drawing.Size(129, 44);
            this.bookhamLogoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.bookhamLogoPictureBox.TabIndex = 10;
            this.bookhamLogoPictureBox.TabStop = false;
            // 
            // loadBatchButton
            // 
            this.loadBatchButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadBatchButton.Location = new System.Drawing.Point(9, 162);
            this.loadBatchButton.Margin = new System.Windows.Forms.Padding(4);
            this.loadBatchButton.Name = "loadBatchButton";
            this.loadBatchButton.Size = new System.Drawing.Size(256, 55);
            this.loadBatchButton.TabIndex = 3;
            this.loadBatchButton.Text = "Load Batch";
            this.loadBatchButton.Click += new System.EventHandler(this.loadBatchButton_Click);
            // 
            // loadBatchTextBox
            // 
            this.loadBatchTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadBatchTextBox.Location = new System.Drawing.Point(9, 97);
            this.loadBatchTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.loadBatchTextBox.Name = "loadBatchTextBox";
            this.loadBatchTextBox.Size = new System.Drawing.Size(256, 34);
            this.loadBatchTextBox.TabIndex = 0;
            this.loadBatchTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.loadBatchTextBox_KeyPress);
            this.loadBatchTextBox.Enter += new System.EventHandler(this.loadBatchTextBox_Enter);
            // 
            // Button_NoMes
            // 
            this.Button_NoMes.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_NoMes.Location = new System.Drawing.Point(9, 241);
            this.Button_NoMes.Margin = new System.Windows.Forms.Padding(4);
            this.Button_NoMes.Name = "Button_NoMes";
            this.Button_NoMes.Size = new System.Drawing.Size(256, 55);
            this.Button_NoMes.TabIndex = 4;
            this.Button_NoMes.Text = "No MES";
            this.Button_NoMes.Visible = false;
            this.Button_NoMes.Click += new System.EventHandler(this.Button_NoMes_Click);
            // 
            // lblBatchID
            // 
            this.lblBatchID.AutoSize = true;
            this.lblBatchID.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblBatchID.Location = new System.Drawing.Point(4, 68);
            this.lblBatchID.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblBatchID.Name = "lblBatchID";
            this.lblBatchID.Size = new System.Drawing.Size(80, 20);
            this.lblBatchID.TabIndex = 17;
            this.lblBatchID.Text = "Batch ID:";
            // 
            // DeviceTypeFrame
            // 
            this.DeviceTypeFrame.Controls.Add(this.DeviceTypeCombo);
            this.DeviceTypeFrame.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DeviceTypeFrame.Location = new System.Drawing.Point(320, 68);
            this.DeviceTypeFrame.Margin = new System.Windows.Forms.Padding(4);
            this.DeviceTypeFrame.Name = "DeviceTypeFrame";
            this.DeviceTypeFrame.Padding = new System.Windows.Forms.Padding(4);
            this.DeviceTypeFrame.Size = new System.Drawing.Size(285, 66);
            this.DeviceTypeFrame.TabIndex = 1;
            this.DeviceTypeFrame.TabStop = false;
            this.DeviceTypeFrame.Text = "Device Type";
            // 
            // DeviceTypeCombo
            // 
            this.DeviceTypeCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.DeviceTypeCombo.FormattingEnabled = true;
            this.DeviceTypeCombo.Location = new System.Drawing.Point(15, 27);
            this.DeviceTypeCombo.Margin = new System.Windows.Forms.Padding(4);
            this.DeviceTypeCombo.Name = "DeviceTypeCombo";
            this.DeviceTypeCombo.Size = new System.Drawing.Size(251, 28);
            this.DeviceTypeCombo.TabIndex = 2;
            this.DeviceTypeCombo.Enter += new System.EventHandler(this.DeviceTypeCombo_Enter);
            // 
            // LoadBatchDialogueCtl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.DeviceTypeFrame);
            this.Controls.Add(this.lblBatchID);
            this.Controls.Add(this.Button_NoMes);
            this.Controls.Add(this.messageLabel);
            this.Controls.Add(this.loadBatchScreenLabel);
            this.Controls.Add(this.bookhamLogoPictureBox);
            this.Controls.Add(this.loadBatchButton);
            this.Controls.Add(this.loadBatchTextBox);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "LoadBatchDialogueCtl";
            this.Size = new System.Drawing.Size(1056, 419);
            this.VisibleChanged += new System.EventHandler(this.LoadBatchDialogueCtl_VisibleChanged);
            this.MsgReceived += new Bookham.TestEngine.Framework.Messages.ManagedCtlBase.MsgReceivedDlg(this.LoadBatchDialogueCtl_MsgReceived);
            ((System.ComponentModel.ISupportInitialize)(this.bookhamLogoPictureBox)).EndInit();
            this.DeviceTypeFrame.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label messageLabel;
        private System.Windows.Forms.Label loadBatchScreenLabel;
        private System.Windows.Forms.PictureBox bookhamLogoPictureBox;
        private System.Windows.Forms.Button loadBatchButton;
        private System.Windows.Forms.TextBox loadBatchTextBox;
        private System.Windows.Forms.Button Button_NoMes;
        private System.Windows.Forms.Label lblBatchID;
        private System.Windows.Forms.GroupBox DeviceTypeFrame;
        private System.Windows.Forms.ComboBox DeviceTypeCombo;



    }
}

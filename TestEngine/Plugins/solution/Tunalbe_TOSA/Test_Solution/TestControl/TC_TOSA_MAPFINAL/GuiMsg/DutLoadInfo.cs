using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestSolution.TestControl
{
    internal class DutLoadInfo
    {
        string partCode;

        public string PartCode
        {
            get { return partCode; }
            set { partCode = value; }
        }
        string dutLotID;

        public string DutLotID
        {
            get { return dutLotID; }
            set { dutLotID = value; }
        }

        string dutSerialNumber;

        public string DutSerialNumber
        {
            get { return dutSerialNumber; }
            set { dutSerialNumber = value; }
        }

        string chirpType;

        public string ChirpType
        {
            get { return chirpType; }
            set { chirpType = value; }
        }
        string chip_ID;

        public string Chip_ID
        {
            get { return chip_ID; }
            set { chip_ID = value; }
        }
        string wafer_ID;

        public string Wafer_ID
        {
            get { return wafer_ID; }
            set { wafer_ID = value; }
        }
        string coc_SN;

        public string Coc_SN
        {
            get { return coc_SN; }
            set { coc_SN = value; }
        }

        private string testStage;

        public string TestStage
        {
            get { return testStage; }
            set { testStage = value; }
        }

        private string lotType;

        public string LotType
        {
            get { return lotType; }
            set { lotType = value; }
        }
    }
}

// Author: chongjian.liang 2016.2.29

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Bookham.TestLibrary.Utilities;
using Bookham.TestEngine.PluginInterfaces.TestControl;

namespace Bookham.TestSolution.TestControl
{
    public class TC_Common
    {
        private const string GRR_SN_PATH = @"Configuration\TOSA Final\GRRSerialNOList.csv";
        private const string RMA_SN_PATH = @"\\10.22.3.108\departments\SZ_Public\Transmission Tunable\meng\HIT2 RMA\SYS_HITT_RMA_SN.CSV";

        private static List<string> grr_SNList;
        private static List<string> rma_SNList;

        public static List<string> GRR_SNList
        {
            get
            {
                if (grr_SNList == null)
                {
                    grr_SNList = Load_SNList(GRR_SN_PATH);
                }

                return grr_SNList;
            }
        }

        public static List<string> RMA_SNList
        {
            get
            {
                if (rma_SNList == null)
                {
                    // delete by dong.chen 20170728   check RM by FW
                    //rma_SNList = Load_SNList(RMA_SN_PATH);
                }

                return rma_SNList;
            }
        }

        private static List<string> Load_SNList(string filePath)
        {
            using (CsvReader csvReader = new CsvReader())
            {
                List<string> snList = new List<string>();
                
                try
                {
                    List<string[]> fileContext = csvReader.ReadFile(filePath);

                    foreach (string[] fileLine in fileContext)
                    {
                        if (fileLine.Length > 0
                            && !string.IsNullOrEmpty(fileLine[0]))
                        {
                            snList.Add(fileLine[0].Trim().ToUpper());
                        }
                    }
                }
                catch
                {
                    MessageBox.Show(string.Format("The file {0} is openning in another process.", filePath), "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                return snList;
            }
        }

        public static bool CheckSPCStatus(ITestEngine testEngine, int node)
        {
            ActionResult result = ActionResult.Empty;

            try
            {
                result = SpcHelper.MustDoSpcTest(testEngine, node, "ALL");
            }
            catch
            {
                result = ActionResult.Empty;
            }

            if (result == ActionResult.Information)
            {
                testEngine.ShowContinueUserQuery(TestControlTab.TestControl, "Please do spc test today as soon as posible!\n请尽快测试SPC。");
            }
            else if (result == ActionResult.Stop)
            {
                testEngine.ShowContinueUserQuery(TestControlTab.TestControl, "Please do spc test first today!\n请先完成SPC测试。");

                return false;
            }
            else if (result == ActionResult.Empty)
            {
                testEngine.SendStatusMsg("Error to judge daily spc status: Action is Empty!!!");
            }
            else if (result == ActionResult.ForceStop)
            {
                testEngine.ShowContinueUserQuery(TestControlTab.TestControl, "please contact enginer !\n请联系技术员核对spc结果。");

                return false;
            }

            return true;
        }

        public const bool IsToCheckSoftwareUpdate = true;
    }
}

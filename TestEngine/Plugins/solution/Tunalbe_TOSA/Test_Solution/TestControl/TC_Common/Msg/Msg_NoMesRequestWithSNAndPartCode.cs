// Author: chongjian.liang 2016.2.19

namespace Bookham.TestSolution.TestControl
{
    public class Msg_NoMesRequestWithSNAndPartCode : Msg_NoMesRequestWithSN
    {
        public string PartCode;

        public Msg_NoMesRequestWithSNAndPartCode(string serialNO, string partCode) : base(serialNO)
        {
            this.PartCode = partCode;
        }
    }
}

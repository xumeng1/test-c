// Author: chongjian.liang 2016.2.19

namespace Bookham.TestSolution.TestControl
{
    public class Msg_NoMesRequestWithSN : Msg_NoMesRequest
    {
        public string SerialNO;

        public Msg_NoMesRequestWithSN(string serialNO)
        {
            this.SerialNO = serialNO;
        }
    }
}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// TcmzInitMzSweep.cs
//
// Author: Paul.Annetts, 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.ToolKit.Mz;
using Bookham.TestLibrary.Algorithms;
using Bookham.TestLibrary.Utilities;
using Bookham.TestSolution.TcmzCommonUtils;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Test Module Template.
    /// </summary>
    public class TcmzInitMzSweep : ITestModule
    {
        #region ITestModule Members

        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, DatumList configData, InstrumentCollection instruments, ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {
            MzInstruments mzInstrs = (MzInstruments) configData.ReadReference("MzInstruments");
            MzDriverUtils mzUtils = new MzDriverUtils(mzInstrs);
            TcmzMzSweep_Config mzConfig = new TcmzMzSweep_Config(configData);

            double mzMaxBias_V = mzConfig.MzMaxBias_V;
            double fixedModBias_V = mzConfig.MZInitFixedModBias_volt;
            double imbalance_A = mzConfig.MzCtrlINominal_mA / 1000;
            double sweepMaxAbs_V = mzConfig.MZInitSweepMaxAbs_V;
            sweepMaxAbs_V = Math.Abs(sweepMaxAbs_V);
            double sweepStop_V = 0.0;
            double sweepStart_V = -sweepMaxAbs_V;
            double stepSize_V = mzConfig.MZInitSweepStepSize_mV / 1000;
            int nbrPoints = (int)(1 + Math.Abs(sweepStart_V - sweepStop_V) / stepSize_V);
            double tapBias_V = mzConfig.MZTapBias_V;
            string fileDirectory = mzConfig.MzFileDirectory;
            //string dutSerialNbr = mzConfig.DutSerialNbr;
            string dutSerialNbr = configData.ReadString("DutSerialNbr");
            
            // Initialise GUI
            engine.GuiShow();
            engine.GuiToFront();           

            // Cleanup K2400s in case instruments have been left in a bad way ( failed module, retest)
            engine.SendToGui("Initialising instruments");
            CleanUpK2400Sweep(mzInstrs);
            mzUtils.EnableAllOutputs(false);

            // record photocurrents at max voltage - left arm
            engine.SendToGui("Reading photocurrents at max bias");
            mzInstrs.LeftArmMod.InitSourceVMeasureI_UntriggeredSpotMeas();
            mzInstrs.LeftArmMod.VoltageSetPoint_Volt = mzMaxBias_V;
            mzInstrs.LeftArmMod.OutputEnabled = true;
            double leftPhotoCurrentAtMax = mzInstrs.LeftArmMod.CurrentActual_amp;
            mzInstrs.LeftArmMod.OutputEnabled = false;

            // record photocurrents at max voltage - right arm
            mzInstrs.RightArmMod.InitSourceVMeasureI_UntriggeredSpotMeas();
            mzInstrs.RightArmMod.VoltageSetPoint_Volt = mzMaxBias_V;
            mzInstrs.RightArmMod.OutputEnabled = true;
            double rightPhotoCurrentAtMax = mzInstrs.RightArmMod.CurrentActual_amp;
            mzInstrs.RightArmMod.OutputEnabled = false;
            // calc sum and ratio of left and right currents
            double photoCurrentSum = leftPhotoCurrentAtMax + rightPhotoCurrentAtMax;
            double photoCurrentRatio = rightPhotoCurrentAtMax == 0 ? double.PositiveInfinity : leftPhotoCurrentAtMax / rightPhotoCurrentAtMax;

            double currentCompliance_A = mzConfig.MZCurrentCompliance_A;
            double currentRange_A = mzConfig.MZCurrentRange_A;
            double voltageCompliance_V = mzConfig.MZVoltageCompliance_V;
            double voltageRange_V = mzConfig.MZVoltageRange_V;
            int numberOfAverages = mzConfig.MzInitNumberOfAverages;
            double integrationRate = mzConfig.MZIntegrationRate;
            double sourceMeasureDelay = mzConfig.MZSourceMeasureDelay_s;
            double powerMeterAveragingTime = mzConfig.MZPowerMeterAveragingTime_s;
            
            engine.SendToGui("Preparing instruments for sweep");
            mzUtils.InitialiseMZArms(currentCompliance_A, currentRange_A);
            mzUtils.InitialiseImbalanceArms(voltageCompliance_V, voltageRange_V);
            mzUtils.InitialiseTaps(currentCompliance_A, currentRange_A);
            mzUtils.SetMeasurementAccuracy(numberOfAverages, integrationRate, sourceMeasureDelay, powerMeterAveragingTime);
            mzInstrs.PowerMeter.Mode = Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.MeterMode.Absolute_mW;

            double opmRange = configData.ReadDouble("OpmRangeInit_mW");
            mzInstrs.PowerMeter.Range = opmRange;

            // Cache the WL in mzUtils so that we can calibrate power after the sweep.
            double wavelength = Measurements.ReadWavelength_nm();
            mzInstrs.PowerMeter.Wavelength_nm = wavelength;
            // These measurements are uncalibrated, but we need to temporarily initialise the cal class.
            OpticalPowerCal.Initialise(2);

            // return data
            DatumList returnData = new DatumList();

            // Run the sweeps and check for over-ranging
            bool statusOk = false;
            do
            {
                statusOk = RunSweepsAndAnalyse(engine, mzInstrs, mzUtils, 
                    fixedModBias_V, imbalance_A, sweepStop_V, sweepStart_V, nbrPoints, tapBias_V, 
                    fileDirectory, dutSerialNbr, ref returnData);

            } while (!statusOk);

            mzInstrs.LeftArmMod.OutputEnabled = true;
            mzInstrs.RightArmMod.OutputEnabled = true;
            mzInstrs.LeftArmImb.OutputEnabled = true;
            mzInstrs.RightArmImb.OutputEnabled = true;
            double measPower_mW = mzInstrs.PowerMeter.ReadPower();
            double measPower_dBm = Alg_PowConvert_dB.Convert_mWtodBm(measPower_mW);

            returnData.AddDouble("LeftModCurrentAtMaxBias", leftPhotoCurrentAtMax);
            returnData.AddDouble("RightModCurrentAtMaxBias", rightPhotoCurrentAtMax);
            returnData.AddDouble("ModCurrentAtMaxBiasSum", photoCurrentSum);
            returnData.AddDouble("ModCurrentAtMaxBiasRatio", photoCurrentRatio);
            returnData.AddDouble("MZ_MAX_V_BIAS", mzMaxBias_V);
            returnData.AddDouble("MeasFibrePowerMax_dBm", measPower_dBm);
            returnData.AddDouble("MeasuredFrequency", Alg_FreqWlConvert.Wave_nm_TO_Freq_GHz(wavelength) );

            return returnData;
        }

        private bool RunSweepsAndAnalyse(ITestEngine engine, MzInstruments mzInstrs, MzDriverUtils mzUtils, double fixedModBias_V, double imbalance_A, double sweepStop_V, double sweepStart_V, int nbrPoints, double tapBias_V, string fileDirectory, string dutSerialNbr, ref DatumList returnData)
        {
            // Clear the DatumList in case we need to re-run the sweeps.
            returnData = new DatumList();
            bool dataOk = true;
            double minLevel_dBm = -60;

            // do the right-arm modulation sweep
            // do the left-arm modulation sweep
            engine.SendToGui("Running LeftArm sweep");
            MZSweepData sweepDataLeft = null;
            do
            {
                bool bTimeOut = true;
                bool bRetry = false;

                // START MODIFICATION: Sep.06.2007, by Ken.Wu
                //  Adds timeout handling for left arm sweep
                do
                {
                    bTimeOut = false;
                    try
                    {
                        sweepDataLeft = mzUtils.LeftModArm_SingleEndedSweep
                            (fixedModBias_V, imbalance_A, imbalance_A, sweepStart_V,
                            sweepStop_V, nbrPoints, tapBias_V, tapBias_V);
                    }
                    catch (TimeoutException e)
                    {
                        bTimeOut = true;
                        string prompt = string.Format("TIMEOUT: \n[{0}]\nTry again?",
                            e.Message);
                        ButtonId buttonId = engine.ShowYesNoUserQuery(prompt);
                        bRetry = (buttonId == ButtonId.Yes);
                    }
                }
                while (bTimeOut && bRetry);

                if (sweepDataLeft == null)
                {
                    engine.ShowContinueUserQuery(
                        "Left Arm sweep data can't be null, please check the PowerMeter and retry.");
                    dataOk = false;
                    break;
                }
                // END MODIFICATION: Sep.06.2007, by Ken.Wu

                // If overrange run again
                if (MzAnalysisWrapper.CheckForOverrange(sweepDataLeft.Sweeps[SweepDataType.FibrePower_mW]))
                {
                    dataOk = false;
                    mzInstrs.PowerMeter.Range = mzInstrs.PowerMeter.Range * 10;
                }
                else
                {
                    // if underrange fix the data and continue
                    dataOk = true;
                    if (MzAnalysisWrapper.CheckForUnderrange(sweepDataLeft.Sweeps[SweepDataType.FibrePower_mW]))
                    {
                        sweepDataLeft.Sweeps[SweepDataType.FibrePower_mW] = 
                            MzAnalysisWrapper.FixUnderRangeData(sweepDataLeft.Sweeps[SweepDataType.FibrePower_mW]
                            , minLevel_dBm);
                    }
                }
            }
            while (!dataOk);

            engine.SendToGui(sweepDataLeft);
            engine.SendToGui("Running RightArm sweep");

            MZSweepData sweepDataRight;
            do
            {
                sweepDataRight =
                    mzUtils.RightModArm_SingleEndedSweep
                    (fixedModBias_V, imbalance_A, imbalance_A, sweepStart_V,
                    sweepStop_V, nbrPoints, tapBias_V, tapBias_V);

                // If overrange run again
                if (MzAnalysisWrapper.CheckForOverrange(sweepDataRight.Sweeps[SweepDataType.FibrePower_mW]))
                {
                    dataOk = false;
                    mzInstrs.PowerMeter.Range = mzInstrs.PowerMeter.Range * 10;
                }
                else
                {
                    // if underrange fix the data and continue
                    dataOk = true;
                    if (MzAnalysisWrapper.CheckForUnderrange(sweepDataRight.Sweeps[SweepDataType.FibrePower_mW]))
                    {
                        sweepDataRight.Sweeps[SweepDataType.FibrePower_mW] =
                            MzAnalysisWrapper.FixUnderRangeData(sweepDataRight.Sweeps[SweepDataType.FibrePower_mW]
                            , minLevel_dBm);
                    }
                }
            }
            while (!dataOk);

            engine.SendToGui(sweepDataRight);
            engine.SendToGui("Completed RightArm sweep");

            // Clean up
            CleanUpK2400Sweep(mzInstrs);
            ResetK2400Triggering(mzInstrs);
            mzInstrs.PowerMeter.EnableLogging = false;
            mzInstrs.PowerMeter.EnableInputTrigger(false);


            // filenames
            string leftFileName = Util_GenerateFileName.GenWithTimestamp(fileDirectory, "InitMzLV_L",
                dutSerialNbr, "csv");
            string rightFileName = Util_GenerateFileName.GenWithTimestamp(fileDirectory, "InitMzLV_R",
                dutSerialNbr, "csv");
            string joinedFileName = Util_GenerateFileName.GenWithTimestamp(fileDirectory, "InitMzLV",
                dutSerialNbr, "csv");
            // write left sweep to file
            MzSweepFileWriter.WriteSweepData(leftFileName, sweepDataLeft,
                new SweepDataType[] { SweepDataType.LeftArmModBias_V, 
                                      SweepDataType.FibrePower_mW, 
                                      SweepDataType.LeftArmModBias_mA,
                                      SweepDataType.TapComplementary_mA});
            // write right sweep to file
            MzSweepFileWriter.WriteSweepData(rightFileName, sweepDataRight,
                new SweepDataType[] { SweepDataType.RightArmModBias_V, 
                                      SweepDataType.FibrePower_mW, 
                                      SweepDataType.RightArmModBias_mA,
                                      SweepDataType.TapComplementary_mA});

            // write joined sweep to file
            MzSweepFileWriter.WriteStitchedSingleEndLVData(joinedFileName,
                sweepDataLeft, sweepDataRight,
                new SweepDataType[] { SweepDataType.FibrePower_mW,
                                      SweepDataType.LeftArmModBias_mA, 
                                      SweepDataType.TapComplementary_mA});

            // analyse the data to find min and max bias
            // do analysis
            MzAnalysisWrapper.MzAnalysisResults mzAnly = null;
            try
            {
                mzAnly = MzAnalysisWrapper.ZeroChirp_SingleEndedSweeps(sweepDataLeft, sweepDataRight,
                        fixedModBias_V, fixedModBias_V);
            }
            catch (AlgorithmException e)
            {
                // Generate a meaningful error message
                throw new AlgorithmException("Unable to analyse MZ data", e);
            }
            catch (System.InvalidOperationException)
            {
                //  TODO - fix this properly !
                mzInstrs.PowerMeter.Range = mzInstrs.PowerMeter.Range * 10;
                return false;
            }

            returnData.AddDouble("LeftModMin_V", mzAnly.Min_SrcL);
            returnData.AddDouble("RightModMin_V", mzAnly.Min_SrcR);
            returnData.AddDouble("LeftModMax_V", mzAnly.Max_SrcL);
            returnData.AddDouble("RightModMax_V", mzAnly.Max_SrcR);
            returnData.AddDouble("CalcFibrePowerMax_dBm", mzAnly.PowerAtMax_dBm);

            // Create an archive containing all of the plot data
            string zipFileName = Util_GenerateFileName.GenWithTimestamp(fileDirectory,
            "MzInitialSeSweeps", dutSerialNbr, "zip");
            Util_ZipFile zipFile = new Util_ZipFile(zipFileName);
            using (zipFile)
            {
                zipFile.AddFileToZip(leftFileName);
                zipFile.AddFileToZip(rightFileName);
                zipFile.AddFileToZip(joinedFileName);
            }
            returnData.AddFileLink("MzInitialSeSweeps", zipFileName);

            // set MZ to maximum point
            mzInstrs.LeftArmMod.VoltageSetPoint_Volt = mzAnly.Max_SrcL;
            mzInstrs.RightArmMod.VoltageSetPoint_Volt = mzAnly.Max_SrcR;
            mzInstrs.LeftArmImb.CurrentSetPoint_amp = imbalance_A;
            mzInstrs.RightArmImb.CurrentSetPoint_amp = imbalance_A;

            // check that the meter didn't overrange
            if (Math.Abs(mzAnly.PowerAtMax_dBm) + Math.Abs(mzAnly.PowerAtMin_dBm) > 200)
            {
                // It has probably over-ranged. Increase the range
                mzInstrs.PowerMeter.Range = mzInstrs.PowerMeter.Range * 10;
                return false;
            }
            return true;
        }

        private void CleanUpK2400Sweep(MzInstruments insts)
        {
            // Clear buffers & clean up
            insts.LeftArmImb.CleanUpSweep();
            insts.LeftArmMod.CleanUpSweep();
            insts.RightArmImb.CleanUpSweep();
            insts.RightArmMod.CleanUpSweep();
            insts.TapComplementary.CleanUpSweep();
            if (insts.InlineTapOnline)
                insts.TapInline.CleanUpSweep();
        }

        private static void ResetK2400Triggering(MzInstruments insts)
        {
            // Leave the meters in a state suitable for spot readings.
            insts.LeftArmImb.InitSourceIMeasureV_UntriggeredSpotMeas();
            insts.LeftArmMod.InitSourceVMeasureI_UntriggeredSpotMeas();
            insts.RightArmImb.InitSourceIMeasureV_UntriggeredSpotMeas();
            insts.RightArmMod.InitSourceVMeasureI_UntriggeredSpotMeas();
            insts.TapComplementary.InitSourceVMeasureI_UntriggeredSpotMeas();
            if (insts.InlineTapOnline)
                insts.TapInline.InitSourceVMeasureI_UntriggeredSpotMeas();
        }

        public Type UserControl
        {
            get { return (typeof(TcmzInitMzSweepGui)); }
        }

        #endregion


    }
}

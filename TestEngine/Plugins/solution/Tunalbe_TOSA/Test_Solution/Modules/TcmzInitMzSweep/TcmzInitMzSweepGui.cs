// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// TcmzInitMzSweepGui.cs
//
// Author: Paul.Annetts, 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Bookham.ToolKit.Mz;
using NPlot;
using Bookham.TestLibrary.Algorithms;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Module Template GUI User Control
    /// </summary>
    public partial class TcmzInitMzSweepGui : Bookham.TestEngine.Framework.Messages.ManagedCtlBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public TcmzInitMzSweepGui()
        {
            /* Call designer generated code. */
            InitializeComponent();
        }

        /// <summary>
        /// Incoming message event handler
        /// </summary>
        /// <param name="payload">Message Payload</param>
        /// <param name="inMsgSeq">Incoming message sequence number</param>
        /// <param name="respSeq">Outgoing message sequence number</param>
        private void ModuleGui_MsgReceived(object payload, long inMsgSeq, long respSeq)
        {   
            if (payload.GetType() == typeof(MZSweepData))
            {
                MZSweepData sweepData = (MZSweepData)payload;
                this.MessageLabel.Text = "Completed " + sweepData.Type.ToString() + " sweep on " + sweepData.SrcMeter.ToString();

                try
                {
                    double[] xData = new double[0];
                    if (sweepData.SrcMeter == SourceMeter.LeftModArm)
                    {
                        xData = sweepData.Sweeps[SweepDataType.LeftArmModBias_mA];
                    }
                    if (sweepData.SrcMeter == SourceMeter.RightModArm)
                    {
                        xData = sweepData.Sweeps[SweepDataType.RightArmModBias_mA];
                    }
                    double[] yData = Alg_PowConvert_dB.Convert_mWtodBm(sweepData.Sweeps[SweepDataType.FibrePower_mW]);

                    mzplot.Clear();
                    LinePlot plotData = new LinePlot(yData, xData);
                    plotData.Color = Color.DarkBlue;
                    plotData.Label = "Optical Power (dBm)";

                    mzplot.Add(plotData, PlotSurface2D.XAxisPosition.Bottom, PlotSurface2D.YAxisPosition.Right, 1);
                    mzplot.XAxis1.Label = sweepData.SrcMeter.ToString() + " current (mA)";

                    if ( mzplot.YAxis2 != null )
                        mzplot.YAxis2.Label = "Optical power (dBm)";
                    
                    this.mzplot.Visible = true;
                    mzplot.Refresh();
                }
                catch (SystemException)
                {
                    this.mzplot.Visible = false;
                    // Any GUI errors should not be fatal.
                }
            }
            else if (payload.GetType() == typeof(String))
            {
                this.MessageLabel.Text = (string)payload;
            }
        }
    }
}

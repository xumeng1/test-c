// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// TCMZInitMzSweep_Config.cs
//
// Author: Mark Fullalove
// Design: [Reference design documentation]

using System;
using System.Text;
using System.Collections.Generic;
using Bookham.TestEngine.Framework.InternalData;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Container for all config data for TCMZInitMzSweep
    /// </summary>
    public class TcmzMzSweep_Config
    {
        /// <summary>
        /// empty constructor
        /// </summary>
        public TcmzMzSweep_Config()
        { 
        }

        /// <summary>
        /// Populates the data from a datumlist
        /// </summary>
        /// <param name="dataToAdd">A datumlist containing the data to be added</param>
        public TcmzMzSweep_Config(DatumList dataToAdd)
        {
            this.MzMaxBias_V = dataToAdd.ReadDouble("MzMaxBias_V");
            this.MZInitFixedModBias_volt = dataToAdd.ReadDouble("MZInitFixedModBias_volt");
            this.MzCtrlINominal_mA = dataToAdd.ReadDouble("MzCtrlINominal_mA");
            this.MZInitSweepMaxAbs_V = dataToAdd.ReadDouble("MZInitSweepMaxAbs_V");
            this.MzInitNumberOfAverages = dataToAdd.ReadSint32("MZInitNumberOfAverages");
            this.MZTapBias_V = dataToAdd.ReadDouble("MZTapBias_V");
            this.MZCurrentCompliance_A = dataToAdd.ReadDouble("MZCurrentCompliance_A");
            this.MZCurrentRange_A = dataToAdd.ReadDouble("MZCurrentRange_A");
            this.MZVoltageCompliance_V = dataToAdd.ReadDouble("MZVoltageCompliance_V");
            this.MZVoltageRange_V = dataToAdd.ReadDouble("MZVoltageRange_V");
            this.MZIntegrationRate = dataToAdd.ReadDouble("MZIntegrationRate");
            this.MZSourceMeasureDelay_s = dataToAdd.ReadDouble("MZSourceMeasureDelay_s");
            this.MZPowerMeterAveragingTime_s = dataToAdd.ReadDouble("MZPowerMeterAveragingTime_s");
            this.MZInitSweepStepSize_mV = dataToAdd.ReadDouble("MZInitSweepStepSize_mV");
            this.LockRatioTolerance = dataToAdd.ReadDouble("LockRatioTolerance");
            this.OpmRangeInit_mW = dataToAdd.ReadDouble("OpmRangeInit_mW");
        }

        /// <summary>
        /// Returns a datumlist containing the config data.
        /// Hint: Use this method to help populate the class from configuration data.
        /// </summary>
        /// <returns>A datum list</returns>
        public DatumList GetDatumList()
        {
            DatumList dataOut = new DatumList();

            DatumDouble MzMaxBias_V = new DatumDouble("MzMaxBias_V", this.MzMaxBias_V);
            dataOut.Add(MzMaxBias_V);
            DatumDouble MZInitFixedModBias_volt = new DatumDouble("MZInitFixedModBias_volt", this.MZInitFixedModBias_volt);
            dataOut.Add(MZInitFixedModBias_volt);
            DatumDouble MzCtrlINominal_mA = new DatumDouble("MzCtrlINominal_mA", this.MzCtrlINominal_mA);
            dataOut.Add(MzCtrlINominal_mA);
            DatumDouble MZInitSweepMaxAbs_V = new DatumDouble("MZInitSweepMaxAbs_V", this.MZInitSweepMaxAbs_V);
            dataOut.Add(MZInitSweepMaxAbs_V);
            DatumSint32 MZInitNumberOfAverages = new DatumSint32("MZInitNumberOfAverages", this.MzInitNumberOfAverages);
            dataOut.Add(MZInitNumberOfAverages);
            DatumDouble MZTapBias_V = new DatumDouble("MZTapBias_V", this.MZTapBias_V);
            dataOut.Add(MZTapBias_V);
            DatumDouble MZCurrentCompliance_A = new DatumDouble("MZCurrentCompliance_A", this.MZCurrentCompliance_A);
            dataOut.Add(MZCurrentCompliance_A);
            DatumDouble MZCurrentRange_A = new DatumDouble("MZCurrentRange_A", this.MZCurrentRange_A);
            dataOut.Add(MZCurrentRange_A);
            DatumDouble MZVoltageCompliance_V = new DatumDouble("MZVoltageCompliance_V", this.MZVoltageCompliance_V);
            dataOut.Add(MZVoltageCompliance_V);
            DatumDouble MZVoltageRange_V = new DatumDouble("MZVoltageRange_V", this.MZVoltageRange_V);
            dataOut.Add(MZVoltageRange_V);
            DatumDouble MZIntegrationRate = new DatumDouble("MZIntegrationRate", this.MZIntegrationRate);
            dataOut.Add(MZIntegrationRate);
            DatumDouble MZSourceMeasureDelay_s = new DatumDouble("MZSourceMeasureDelay_s", this.MZSourceMeasureDelay_s);
            dataOut.Add(MZSourceMeasureDelay_s);
            DatumDouble MZPowerMeterAveragingTime_s = new DatumDouble("MZPowerMeterAveragingTime_s", this.MZPowerMeterAveragingTime_s);
            dataOut.Add(MZPowerMeterAveragingTime_s);
            DatumDouble MZInitSweepStepSize_mV = new DatumDouble("MZInitSweepStepSize_mV", this.MZInitSweepStepSize_mV);
            dataOut.Add(MZInitSweepStepSize_mV);
            DatumDouble LockRatioTolerance = new DatumDouble("LockRatioTolerance", this.LockRatioTolerance);
            dataOut.Add(LockRatioTolerance);
            DatumDouble OpmRangeInit_mW = new DatumDouble("OpmRangeInit_mW", this.OpmRangeInit_mW);
            dataOut.Add(OpmRangeInit_mW);

            return dataOut;
        }

        /// <summary>
        /// The max MZ bias
        /// </summary>
        public double MzMaxBias_V;
        /// <summary>
        /// The modulator arm bias voltage
        /// </summary>
        public double MZInitFixedModBias_volt;
        /// <summary>
        /// The nominal imbalance current
        /// </summary>
        public double MzCtrlINominal_mA;
        /// <summary>
        /// The magnitude of the voltage to be applied during the sweep. 
        /// </summary>
        public double MZInitSweepMaxAbs_V;
        /// <summary>
        /// The number of averages be taken during the MZ sweep.
        /// </summary>
        public int MzInitNumberOfAverages;
        /// <summary>
        /// The bias voltage to apply to the power taps
        /// </summary>
        public double MZTapBias_V;
        /// <summary>
        /// The current compliance to apply to the MZ sourcemeters
        /// </summary>
        public double MZCurrentCompliance_A;
        /// <summary>
        /// The current range to set on the MZ sourcemeters
        /// </summary>
        public double MZCurrentRange_A;
        /// <summary>
        /// The voltage compliance level.
        /// </summary>
        public double MZVoltageCompliance_V;
        /// <summary>
        /// The voltage range to set on the MZ sourcemeters.
        /// </summary>
        public double MZVoltageRange_V;
        /// <summary>
        /// The integration rate to apply.
        /// </summary>
        public double MZIntegrationRate;
        /// <summary>
        /// The source-measure delay to apply during the sweep.
        /// </summary>
        public double MZSourceMeasureDelay_s;
        /// <summary>
        /// The power meter averaging time to apply during the sweep.
        /// </summary>
        public double MZPowerMeterAveragingTime_s;
        /// <summary>
        /// Sweep step size
        /// </summary>
        public double MZInitSweepStepSize_mV;
        /// <summary>
        /// Lock ratio tolerance
        /// </summary>
        public double LockRatioTolerance;
        /// <summary>
        /// Initial Power Meter Range mW
        /// </summary>
        public double OpmRangeInit_mW;
    }
}

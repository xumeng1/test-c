// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// TcmzSectionIVTestGui.cs
//
// Author: rong.guo, 2008
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Bookham.TestSolution.TcmzCommonUtils;
using Bookham.TestSolution.TcmzCommonData;
using NPlot;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Module Template GUI User Control
    /// </summary>
    public partial class TcmzSectionIVTestGui : Bookham.TestEngine.Framework.Messages.ManagedCtlBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public TcmzSectionIVTestGui()
        {
            /* Call designer generated code. */
            InitializeComponent();

            /* TODO - Write constructor code here. */
        }

        /// <summary>
        /// Incoming message event handler
        /// </summary>
        /// <param name="payload">Message Payload</param>
        /// <param name="inMsgSeq">Incoming message sequence number</param>
        /// <param name="respSeq">Outgoing message sequence number</param>
        private void ModuleGui_MsgReceived(object payload, long inMsgSeq, long respSeq)
        {
            if (payload.GetType() == typeof(SectionIVSweepData))
            {
                SectionIVSweepData sweepData = (SectionIVSweepData)payload;
                this.MessageLabel.Text = "Completed " + this.MessageLabel.Text;

                try
                {
                    double[] xData = sweepData.Idata_mA;
                    double[] yData = sweepData.Vdata_V;

                    sectionIVPlot.Clear();
                    LinePlot plotData = new LinePlot(yData, xData);
                    plotData.Color = Color.DarkBlue;
                    plotData.Label = "Section Voltage (V)";

                    sectionIVPlot.Add(plotData, PlotSurface2D.XAxisPosition.Bottom, PlotSurface2D.YAxisPosition.Right, 1);
                    sectionIVPlot.XAxis1.Label = "Section Current (mA)";

                    if (sectionIVPlot.YAxis2 != null)
                        sectionIVPlot.YAxis2.Label = "Section Voltage (V)";

                    this.sectionIVPlot.Visible = true;
                    sectionIVPlot.Refresh();
                }
                catch (SystemException)
                {
                    this.sectionIVPlot.Visible = false;
                    // Any GUI errors should not be fatal.
                }
            }
            else if (payload.GetType() == typeof(String))
            {
                this.MessageLabel.Text = (string)payload;
            }
        }
    }
}

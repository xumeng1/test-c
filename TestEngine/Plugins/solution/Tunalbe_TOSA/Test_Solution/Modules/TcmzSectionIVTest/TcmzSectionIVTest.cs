// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// TcmzSectionIVTest.cs
//
// Author: rong.guo, 2008
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestSolution.TcmzCommonData;
using Bookham.TestSolution.TcmzCommonUtils;
using Bookham.TestLibrary.Utilities;
using NPlot.Windows;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Test Module Template.
    /// </summary>
    public class TcmzSectionIVTest : ITestModule
    {
        #region ITestModule Members

        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, DatumList configData, InstrumentCollection instruments, ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {
            // GUI show
            engine.GuiToFront();
            engine.GuiShow();

            // Read configs
            string dutSerialNbr = configData.ReadString("DutSerialNbr");
            string DsdbrFileDirectory = configData.ReadString("DsdbrFileDirectory");
            List<SectionIVSweepSetting> sectionIVSweepSettings = (List<SectionIVSweepSetting>)configData.ReadReference("SectionIVSweepSettings");

            // IV sweep test at each section
            string sectionIVSweepResultFile = Util_GenerateFileName.GenWithTimestamp(DsdbrFileDirectory, "SectionIVSweepTestResults", dutSerialNbr, "csv");
            using (StreamWriter writer = new StreamWriter(sectionIVSweepResultFile))
            {
                foreach (SectionIVSweepSetting sweepSetting in sectionIVSweepSettings)
                {
                    DSDBRSection section = sweepSetting.Section;
                    string sectionName = section.ToString();

                    // Send message to GUI
                    engine.SendToGui(sectionName + " section IV scan");

                    // Seciton IV scan
                    SectionIVSweepData sweepResult = SectionIVSweep.PerformSectionIVSweep(section, sweepSetting);

                    // Send data to GUI
                    engine.SendToGui(sweepResult);

                    // Save section data to file
                    writer.Write("I" + sectionName + "_mA");
                    foreach (double Idata in sweepResult.Idata_mA)
                    {
                        writer.Write(",");
                        writer.Write(Idata);
                    }
                    writer.WriteLine();
                    writer.Write("V" + sectionName + "_V");
                    foreach (double Vdata in sweepResult.Vdata_V)
                    {
                        writer.Write(",");
                        writer.Write(Vdata);
                    }
                    writer.WriteLine();
                }
            }

            // return data
            DatumList returnData = new DatumList();
            returnData.AddFileLink("SectionIVSweepTestResults", sectionIVSweepResultFile);
            return returnData;
        }

        public Type UserControl
        {
            get { return (typeof(TcmzSectionIVTestGui)); }
        }

        #endregion
    }
}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// SPCMidTemp.cs
//
// Author: ken.wu, 2008
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using TCMZDefinitions;
//using Bookham.TestSolution.TcmzCommonUtils;
//using Bookham.TestSolution.TcmzCommonData;
//using Bookham.TestSolution.TcmzCommonInstrs;
using Bookham.ToolKit.Mz;
using Bookham.TestLibrary.InstrTypes;
using Bookham.Toolkit.CloseGrid.TEEquipment;
using Bookham.Toolkit.CloseGrid;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Test Module Template.
    /// </summary>
    public class SPCMidTemp : ITestModule
    {
        #region ITestModule Members

        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, DatumList configData, InstrumentCollection instruments, ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {
            SPCStageData spcData = configData.ReadReference("SPCStageData") as SPCStageData;
            DatumList baseline = configData.ReadListDatum("BaselineDatum");
            FCUInstruments dsdbrInsts = configData.ReadReference("FcuInstruments") as FCUInstruments;
            MzInstruments mzInstrs = configData.ReadReference("MzInstrs") as MzInstruments;
            IInstType_TecController tecDsdbr = configData.ReadReference("TecDsdbr") as IInstType_TecController;
            IInstType_TecController tecMz = configData.ReadReference("TecMz") as IInstType_TecController;
            IInstType_TecController tecCase = configData.ReadReference("TecCase") as IInstType_TecController;
            IInstType_OSA Osa = instruments["Osa"] as IInstType_OSA;
            IInstType_Wavemeter waveMeter = instruments["Wavemeter"] as IInstType_Wavemeter;
            IInstType_TriggeredOpticalPowerMeter OpmMz = instruments["OpmMz"] as IInstType_TriggeredOpticalPowerMeter;
            DsdbrUtils.FcuInstrumentGroup = dsdbrInsts;

            if (spcData == null)
            {
                throw new NullReferenceException("SPCData can't be null.");
            }

            //setup osa;
            SMSRMeasurement.OSA = Osa;
            SMSRMeasurement.SMSRSpan_nm = configData.ReadDouble("SMSRMeasure_Span_nm");
            SMSRMeasurement.SMSRRBW_nm = configData.ReadDouble("SMSRMeasure_RBW_nm");
            SMSRMeasurement.SMSROSASensitivity_dBm = configData.ReadDouble("SMSRMeasure_OSASensitivity_dBm");
            SMSRMeasurement.SMSRNumPts = configData.ReadSint32("SMSRMeasure_NumPts");

            //DsdbrChannelSetup setup = finalChannel.GetDsdbrChannelSetup();
            DsdbrChannelSetup setup = new DsdbrChannelSetup();
            Datum frontSectionPair = baseline.GetDatum("FRONTSECTIONPAIR");
            Datum ifsConst = baseline.GetDatum("IFSCONST");
            Datum iGain = baseline.GetDatum("IGAIN");

            setup.FrontPair = int.Parse(frontSectionPair.ValueToString());
            setup.IFsFirst_mA = int.Parse(ifsConst.ValueToString());
            setup.IFsSecond_mA = baseline.ReadDouble("IFSNONCONST");
            setup.IGain_mA = int.Parse(iGain.ValueToString());
            setup.IPhase_mA = baseline.ReadDouble("IPHASEITU");
            setup.IRear_mA = baseline.ReadDouble("IREAR");
            setup.ISoa_mA = baseline.ReadDouble("ISOA");

            DsdbrUtils.SetDsdbrCurrents_mA(setup);

            //InstCloseGrid_ElectricalSource.
            //CloseGridWrapper cgw = CloseGridWrapper.Singleton;

            //dsdbrInsts.LockerRx.IsOnline = true;
            //dsdbrInsts.LockerTx.IsOnline = true;
            //DsdbrUtils.SetLockerBiasVoltage_V(LockerId.TX, finalChannel.ItxLock_mA);
            //DsdbrUtils.SetLockerBiasVoltage_V(LockerId.RX, finalChannel.IrxLock_mA);

            MzDriverUtils mzUtils = new MzDriverUtils(mzInstrs);

            //MzData mzData = finalChannel.GetMzData();
            MzData mzData = new MzData();
            mzData.LeftArmImb_mA = baseline.ReadDouble("MZLEFTARMIMB");
            //20080306:Ken.Wu
            // Can't apply left and right bias voltage at the same time.
            mzData.LeftArmMod_Peak_V = baseline.ReadDouble("MZLEFTARMMODPEAK_V");
            mzData.LeftArmMod_Quad_V = baseline.ReadDouble("MZLEFTARMMODQUAD_V");
            mzData.RightArmImb_mA = baseline.ReadDouble("MZRIGHTARMIMB");
            mzData.RightArmMod_Peak_V = baseline.ReadDouble("MZRIGHTARMMODPEAK_V");
            mzData.RightArmMod_Quad_V = baseline.ReadDouble("MZRIGHTARMMODQUAD_V");

            mzUtils.SetupMzToPeak(mzData);

            mzUtils.SetVoltage(SourceMeter.ComplementaryTap, -3.5);

            if (mzInstrs.InlineTapOnline)
            {
                mzUtils.SetVoltage(SourceMeter.InlineTap, -3.5);
            }

            #region Wait for Rth stable
            {
                double laserRthHigh_ohm = configData.ReadDouble("LaserRthHigh_ohm");
                double laserRthLow_ohm = configData.ReadDouble("LaserRthLow_ohm");
                double mzRthHigh_ohm = configData.ReadDouble("MzRthHigh_ohm");
                double mzRthLow_ohm = configData.ReadDouble("MzRthLow_ohm");
                const int rthDelay_ms = 500;
                const int rthRetryCount = 10;

                System.Threading.Thread.Sleep(rthDelay_ms);
                int triedCount = 0;
                do
                {
                    System.Threading.Thread.Sleep(rthDelay_ms);
                    double mzRth_ohm = tecMz.SensorResistanceActual_ohm;
                    double dsdbrRth_ohm = tecDsdbr.SensorResistanceActual_ohm;
                    if (mzRth_ohm >= mzRthLow_ohm && mzRth_ohm <= mzRthHigh_ohm &&
                        dsdbrRth_ohm >= laserRthLow_ohm && dsdbrRth_ohm <= laserRthHigh_ohm)
                    {
                        break;
                    }
                } while (++triedCount <= rthRetryCount);
            }
            #endregion

            //DSDBR section monitor
            #region Measure DSDBR section voltage.
            //{
            //    spcData.GainVoltage_V = dsdbrInsts.CurrentSources[DSDBRSection.Gain].VoltageActual_Volt;
            //    spcData.SOAVoltage_V = dsdbrInsts.CurrentSources[DSDBRSection.SOA].VoltageActual_Volt;
            //    spcData.RearVoltage_V = dsdbrInsts.CurrentSources[DSDBRSection.Rear].VoltageActual_Volt;
            //    spcData.PhaseVoltage_V = dsdbrInsts.CurrentSources[DSDBRSection.Phase].VoltageActual_Volt;
            //}
            #endregion
            //this._results.NonFrontVoltage_V = progInfo.Instrs.Dsdbr.CurrentSources[DSDBRSection.n
            // TODO: Read front section voltage and non-front section voltage.
            //


            #region Measure Mz parameters.
            spcData.MzLeftArmImb_V = mzInstrs.LeftArmImb.VoltageActual_Volt;
            spcData.MzRightArmImb_V = mzInstrs.RightArmImb.VoltageActual_Volt;
            spcData.MzLeftArmMod_mA = mzInstrs.LeftArmMod.CurrentActual_amp * ((double)1000);
            spcData.MzRightArmMod_mA = mzInstrs.RightArmMod.CurrentActual_amp * ((double)1000);
            spcData.TapCompCurrent_mA = mzInstrs.TapComplementary.CurrentActual_amp * ((double)1000);
            if (mzInstrs.InlineTapOnline)
            {
                spcData.TapInlineCurrent_mA = mzInstrs.TapInline.CurrentActual_amp * ((double)1000);
            }
            #endregion

            //Frequency measure (Wavemeter)
            spcData.ActualFrequency_GHz = waveMeter.Frequency_GHz;

           ////Power measurement for FCU
            OpmMz.AveragingTime_s = 0.5;
            OpmMz.Wavelength_nm = waveMeter.Wavelength_nm;
            OpmMz.CalOffset_dB=0;
            OpmMz.Mode = InstCloseGrid_OpticalPowerMeter.MeterMode.Absolute_mW;
            double mzPower = OpmMz.ReadPower();
            double offset = 3.37;
            spcData.FiberPowerPeak_mW = mzPower+offset;
            ////Power measurement (Power Meter) 8163
            //CloseGridWrapper closeGrid = CloseGridWrapper.Singleton;
            //double pwrPeak = closeGrid.ReadPower_mW(PowerMeterHead.Reference);
            //spcData.PowerDirect_mW = pwrPeak;

            //double calPwr = OpticalPowerCal.GetCorrectedPower_mW(OpticalPowerHead.OpmCgDirect, pwrPeak, spcData.ActualFrequency_GHz);
            //spcData.FiberPowerPeak_mW = calPwr;

            //pwrPeak = closeGrid.ReadPower_mW(PowerMeterHead.Filter);
            //spcData.PowerFiltered_mW = pwrPeak;

            DsdbrUtils.LockerCurrents lockerCurrents = DsdbrUtils.ReadLockerCurrents();

            spcData.RxLockerCurrent_mA = lockerCurrents.RxCurrent_mA;
            spcData.TxLockerCurrent_mA = lockerCurrents.TxCurrent_mA;

            //Power ratio measurement (306II,Box)
            // Read power ??
            //_results.CH1Current_mA = Measurements.OpmCgDirect.
            //Measurements.OpmCgDirect.ReadPower

            //SMSR (OSA monitor)
            //spcData.SMSR_dB = SMSRMeasurement.MeasureSMSR( finalChannel.ItuFrequency_GHz );
            double ituFreq = 0;
            switch (baseline.GetDatumType("ITUFREQ"))
            {
                case DatumType.Sint32:
                    ituFreq = (double)baseline.ReadSint32("ITUFREQ");
                    break;

                case DatumType.Uint32:
                    ituFreq = (double)baseline.ReadUint32("ITUFREQ");
                    break;

                case DatumType.Double:
                    ituFreq = (double)baseline.ReadDouble("ITUFREQ");
                    break;
            }

            spcData.SMSR_dB = SMSRMeasurement.MeasureSMSR(ituFreq);

            #region Last step: measure Rth;
            {
                spcData.Rth_DSDBR_ohm = tecDsdbr.SensorResistanceActual_ohm;
                spcData.Rth_Mz_ohm = tecMz.SensorResistanceActual_ohm;
                spcData.TEC_DSDBRVoltage_V = tecDsdbr.TecVoltageActual_volt;
                spcData.TEC_DSDBRCurrent_mA = tecDsdbr.TecCurrentActual_amp;
                spcData.TEC_MzVoltage_V = tecMz.TecVoltageActual_volt;
                spcData.TEC_MzCurrent_mA = tecMz.TecCurrentActual_amp;
            }
            #endregion

            spcData.CalculateDelta(baseline);

            // return data
            DatumList returnData = new DatumList();

            returnData.AddDouble("PowerFiltered_dBm", spcData.PowerFiltered_dBm);
            returnData.AddDouble("PowerDirect_dBm", spcData.PowerDirect_dBm);

            returnData.AddDouble("FiberPowerPeak", spcData.FiberPowerPeak_dBm);
            returnData.AddDouble("deltaFiberPowerPeak", spcData.deltaFiberPowerPeak_dBm ?? 0.0);
            returnData.AddDouble("Frequency", spcData.ActualFrequency_GHz);
            returnData.AddDouble("deltaFrequency", spcData.deltaActualFrequency_GHz ?? 0.0);
            returnData.AddDouble("TxCurrentLock", spcData.TxLockerCurrent_mA);
            returnData.AddDouble("RxCurrentLock", spcData.RxLockerCurrent_mA);
            returnData.AddDouble("LockRatio", spcData.LockerRatio);
            returnData.AddDouble("deltaLockRatio", spcData.deltaLockRatio ?? 0.0);
            returnData.AddDouble("deltaRxCurrentLock_mA", spcData.DeltaRxLockerCurrent_mA ?? 0);
            returnData.AddDouble("PowerRatio", spcData.PowerRatio);
            returnData.AddDouble("deltaPowerRatio", spcData.deltaPowerRatio ?? 0.0);
            returnData.AddDouble("SMSR", spcData.SMSR_dB);
            returnData.AddDouble("deltaSMSR", spcData.deltaSMSR_dB ?? 0.0);

            //Section Voltages
            returnData.AddDouble("GainVoltage_V", spcData.GainVoltage_V);
            returnData.AddDouble("deltaGainVoltage_V", spcData.DeltaGainVoltage_V ?? 0.0);
            returnData.AddDouble("PhaseVoltage_V", spcData.PhaseVoltage_V);
            returnData.AddDouble("deltaPhaseVoltage_V", spcData.DeltaPhastVoltage_V ?? 0.0);

            //Rth
            returnData.AddDouble("LaserRth_ohm", spcData.Rth_DSDBR_ohm);
            returnData.AddDouble("deltaLaserRth_ohm", spcData.DeltaRth_DSDBR_ohm ?? 0.0);
            returnData.AddDouble("MzRth_ohm", spcData.Rth_Mz_ohm);
            returnData.AddDouble("deltaMzRth_ohm", spcData.DeltaRth_Mz_ohm ?? 0.0);

            //Mz
            returnData.AddDouble("CTapCurrent_mA", spcData.TapCompCurrent_mA);
            returnData.AddDouble("deltaCTapCurrent_mA", spcData.DeltaCompTapCurrent_mA ?? 0);
            returnData.AddDouble("ITapCurrent_mA", spcData.TapInlineCurrent_mA ?? 0);
            returnData.AddDouble("deltaITapCurrent_mA", spcData.DeltaInlineTapCurrent_mA ?? 0);
            return returnData;
        }

        public Type UserControl
        {
            get { return (typeof(SPCMidTempGui)); }
        }

        #endregion
    }
}

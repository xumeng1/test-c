﻿// Author: chongjian.liang 2016.11.04

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestSolution.Instruments;
using Bookham.TestLibrary.Instruments;
using Bookham.TestLibrary.Utilities;
using Bookham.TestEngine.Framework.Limits;
using Bookham.TestSolution.IlmzCommonData;
using Bookham.TestSolution.IlmzCommonUtils;
using Bookham.TestSolution.IlmzCommonInstrs;

namespace Bookham.TestSolution.TestModules
{
    public class Mod_EtalonScan : ITestModule
    {
        #region ITestModule Members

        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, DatumList configData, InstrumentCollection instruments, ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {
            engine.GuiToFront();
            engine.GuiShow();

            Inst_Fcu2Asic fcu2Asic = instruments["Fcu2Asic"] as Inst_Fcu2Asic;

            FailModeCheck failModeCheck = configData.ReadReference("FailModeCheck") as FailModeCheck;
            string work_dir = configData.ReadString("RESULT_DIR");
            string laser_id = configData.ReadString("LASER_ID");
            string timestamp = configData.ReadString("TIME_STAMP");

            float Igain_mA = (float)configData.ReadDouble("I_ETALON_GAIN");
            float Irear_mA = (float)configData.ReadDouble("I_ETALON_REAR");
            float Iphase_mA = (float)configData.ReadDouble("I_ETALON_PHASE");
            float Isoa_mA = (float)configData.ReadDouble("I_ETALON_SOA");
            float IRearSoa_mA = (float)configData.ReadDouble("I_ETALON_REAR_SOA");
            float IfsCon_mA = (float)configData.ReadDouble("I_ETALON_FS_A");
            float IfsNonCon_mA = (float)configData.ReadDouble("I_ETALON_FS_B");
            int fp = configData.ReadSint32("ETALON_FS_PAIR");
            int lmMidLineIntercept = configData.ReadSint32("LM_MID_LINE_INTER");
            double lmMidLineSlope = configData.ReadDouble("LM_MID_LINE_SLOPE");

            float imbLeft_mA = (float)configData.ReadDouble("ImbLeft_mA");
            float imbRight_mA = (float)configData.ReadDouble("ImbRight_mA");

            double[] rearScanPoints = configData.ReadDoubleArray("RearScanPoints");
            double[] phaseScanPoints = configData.ReadDoubleArray("PhaseScanPoints");

            string etalonCurveSweepResultsFilePath = Path.Combine(work_dir, string.Format("{0}_EtalonCurveSweep_{1}.csv", laser_id, timestamp));

            int SWEEP_START_INDEX = 15;
            int SWEEP_END_INDEX = 95;
            
            DsdbrChannelData oneChannel = new DsdbrChannelData();
            oneChannel.Setup.FrontPair = fp;
            oneChannel.Setup.IFsFirst_mA = IfsCon_mA;
            oneChannel.Setup.IFsSecond_mA = IfsNonCon_mA;
            oneChannel.Setup.IGain_mA = Igain_mA;
            oneChannel.Setup.IRear_mA = Irear_mA;
            oneChannel.Setup.ISoa_mA = Isoa_mA;
            oneChannel.Setup.IRearSoa_mA = IRearSoa_mA;
            oneChannel.Setup.IPhase_mA = Iphase_mA;

            DsdbrUtils.SetDsdbrCurrents_mA(oneChannel.Setup);

            //fcu2Asic.IimbLeft_mA = imbLeft_mA;
            //fcu2Asic.IimbRight_mA = imbRight_mA;

            System.Threading.Thread.Sleep(30);

            List<RearPhaseCurrent> etalonCurveSweepPoints = this.GetEtalonCurveSweepPoints(SWEEP_START_INDEX, SWEEP_END_INDEX, lmMidLineSlope, lmMidLineIntercept, rearScanPoints, phaseScanPoints);

            List<EtalonCurveSweepResultItem> etalonCurveSweepResult = this.EtalonCurveSweep(engine, etalonCurveSweepPoints);

            EtalonSlopeEffInfo slopeEffInfo_50GHz;
            EtalonSlopeEffInfo slopeEffInfo_00GHz;

            this.Calc_EtalonSlopeEff(failModeCheck, engine, etalonCurveSweepResult, out slopeEffInfo_50GHz, out slopeEffInfo_00GHz);

            this.StoreToFile_EtalonCurveSweepResults(etalonCurveSweepResultsFilePath, etalonCurveSweepResult);

            if (File.Exists(etalonCurveSweepResultsFilePath))
            {
                string etalonCurveSweepResults_Dir = @"\\szn-sfl-clst-01\services\software\chongjian.liang\HITT\EtalonCurveSweepResults";

                if (!Directory.Exists(etalonCurveSweepResults_Dir))
                {
                    Directory.CreateDirectory(etalonCurveSweepResults_Dir);
                }

                File.Copy(etalonCurveSweepResultsFilePath, Path.Combine(etalonCurveSweepResults_Dir, string.Format("{0}_EtalonCurveSweep_{1}.csv", laser_id, timestamp)), true);
            }

            DatumList result = new DatumList();
            result.AddDouble("FIRST_50_LOCKERSLOPEEFF", slopeEffInfo_50GHz.SlopeEff);
            result.AddDouble("FIRST_00_LOCKERSLOPEEFF", slopeEffInfo_00GHz.SlopeEff);
            result.AddDouble("FIRST_50_LOCKERSLOPE_QUAD", slopeEffInfo_50GHz.Quad.Frequency_GHz);
            result.AddDouble("FIRST_00_LOCKERSLOPE_QUAD", slopeEffInfo_00GHz.Quad.Frequency_GHz);
            result.AddFileLink("ETALON_CURVE_SWEEP_FILE", etalonCurveSweepResultsFilePath);

            return result;
        }

        private List<RearPhaseCurrent> GetEtalonCurveSweepPoints(int sweepStartIndex, int sweepEndIndex, double lmMidLineSlope, int lmMidLineIntercept, double[] rearScanPoints, double[] phaseScanPoints)
        {
            List<RearPhaseCurrent> etalonCurveSweepPoints = new List<RearPhaseCurrent>();

            for (int i = sweepStartIndex; i <= sweepEndIndex; i++)
            {
                double rear_mA = this.GetIndexCurrent(i * lmMidLineSlope + lmMidLineIntercept, rearScanPoints);
                double phase_mA = this.GetIndexCurrent(i, phaseScanPoints);

                etalonCurveSweepPoints.Add(new RearPhaseCurrent(rear_mA, phase_mA));
            }

            return etalonCurveSweepPoints;
        }

        private List<EtalonCurveSweepResultItem> EtalonCurveSweep(ITestEngine engine, List<RearPhaseCurrent> etalonCurveSweepPoints)
        {
            List<EtalonCurveSweepResultItem> etalonCurveSweepResult = new List<EtalonCurveSweepResultItem>();

            for (int i = 0; i < etalonCurveSweepPoints.Count; i++)
            {
                DsdbrUtils.SetSectionCurrent_mA(DSDBRSection.Rear, etalonCurveSweepPoints[i].Rear_mA);
                DsdbrUtils.SetSectionCurrent_mA(DSDBRSection.Phase, etalonCurveSweepPoints[i].Phase_mA);

                System.Threading.Thread.Sleep(10);

                double frequency_GHz = Measurements.ReadFrequency_GHz();

                DsdbrUtils.LockerCurrents lockerCurrents = DsdbrUtils.ReadLockerCurrents();

                etalonCurveSweepResult.Add(new EtalonCurveSweepResultItem(etalonCurveSweepPoints[i], frequency_GHz, lockerCurrents));

                engine.SendToGui((int)Math.Round((double)i / etalonCurveSweepPoints.Count * 100));
            }

            return etalonCurveSweepResult;
        }

        /// <summary>
        /// Get the 1st 50GHz and 1st 00GHz ITU - chongjian.liang 2016.11.24
        /// </summary>
        private void FindFstITUPointsIndexes(FailModeCheck failModeCheck, ITestEngine engine, List<EtalonCurveSweepResultItem> etalonCurveSweepResult, out int o_fstItu_50GHz_Index, out int o_fstItu_00GHz_Index)
        {
            int DATA_REAR_COUNT_TO_TRIM = 5;

            if (etalonCurveSweepResult.Count < DATA_REAR_COUNT_TO_TRIM * 2)
            {
                failModeCheck.RaiseError_Mod_NonParamFail(engine, string.Format("The etalon sweep curve only has {0} points.", etalonCurveSweepResult.Count), FailModeCategory_Enum.PR);
            }

            int FREQ_50GHz = 50;
            int FREQ_100GHz = 100;

            int index_StartPoint = DATA_REAR_COUNT_TO_TRIM;
            double freq_StartPoint_GHz = etalonCurveSweepResult[index_StartPoint].Frequency_GHz;

            bool isFreqOrderAToZ = etalonCurveSweepResult[etalonCurveSweepResult.Count - 1].Frequency_GHz - etalonCurveSweepResult[0].Frequency_GHz > 0;

            int fstItu_00GHz;
            {
                if (isFreqOrderAToZ)
                {
                    fstItu_00GHz = (int)Math.Ceiling(freq_StartPoint_GHz / FREQ_100GHz) * FREQ_100GHz;
                }
                else
                {
                    fstItu_00GHz = (int)Math.Floor(freq_StartPoint_GHz / FREQ_100GHz) * FREQ_100GHz;
                }
            }

            int fstItu_50GHz;
            {
                if (isFreqOrderAToZ)
                {
                    if (fstItu_00GHz - freq_StartPoint_GHz >= FREQ_50GHz)
                    {
                        fstItu_50GHz = fstItu_00GHz - FREQ_50GHz;
                    }
                    else
                    {
                        fstItu_50GHz = fstItu_00GHz + FREQ_50GHz;
                    }
                }
                else
                {
                    if (freq_StartPoint_GHz - fstItu_00GHz >= FREQ_50GHz)
                    {
                        fstItu_50GHz = fstItu_00GHz + FREQ_50GHz;
                    }
                    else
                    {
                        fstItu_50GHz = fstItu_00GHz - FREQ_50GHz;
                    }
                }
            }

            o_fstItu_00GHz_Index = this.FindIndexOfNearestElement(etalonCurveSweepResult, fstItu_00GHz);
            o_fstItu_50GHz_Index = this.FindIndexOfNearestElement(etalonCurveSweepResult, fstItu_50GHz);
        }

        private int FindIndexOfNearestElement(List<EtalonCurveSweepResultItem> etalonCurveSweepResult, double valueToFind)
        {
            double smallestDifference = double.MaxValue;
            int nearestElement = 0;

            for (int i = 0; i < etalonCurveSweepResult.Count; i++)
            {
                double difference = Math.Abs(etalonCurveSweepResult[i].Frequency_GHz - valueToFind);
                if (difference < smallestDifference)
                {
                    smallestDifference = difference;
                    nearestElement = i;
                }
            }

            return nearestElement;
        }

        private void Calc_EtalonSlopeEff(FailModeCheck failModeCheck, ITestEngine engine, List<EtalonCurveSweepResultItem> etalonCurveSweepResult, out EtalonSlopeEffInfo slopeEffInfo_50GHz, out EtalonSlopeEffInfo slopeEffInfo_00GHz)
        {
            int fstItu_50GHz_Index;
            int fstItu_00GHz_Index;
            {
                this.FindFstITUPointsIndexes(failModeCheck, engine, etalonCurveSweepResult, out fstItu_50GHz_Index, out fstItu_00GHz_Index);
            }

            slopeEffInfo_00GHz = new EtalonSlopeEffInfo();
            slopeEffInfo_00GHz.Quad = etalonCurveSweepResult[fstItu_00GHz_Index];
            // Set slopeEffInfo_00GHz.QuadMinus
            {
                if (fstItu_00GHz_Index > 0)
                {
                    slopeEffInfo_00GHz.QuadMinus = etalonCurveSweepResult[fstItu_00GHz_Index - 1];
                }
                else
                {
                    slopeEffInfo_00GHz.QuadMinus = etalonCurveSweepResult[fstItu_00GHz_Index];
                }
            }
            // Set slopeEffInfo_00GHz.QuadPlus
            {
                if (fstItu_00GHz_Index < etalonCurveSweepResult.Count - 1)
                {
                    slopeEffInfo_00GHz.QuadPlus = etalonCurveSweepResult[fstItu_00GHz_Index + 1];
                }
                else
                {
                    slopeEffInfo_00GHz.QuadPlus = etalonCurveSweepResult[fstItu_00GHz_Index];
                }
            }
            slopeEffInfo_00GHz.SlopeEff = EtalonSlopeEffInfo.Calc_EtalonSlopeEff(slopeEffInfo_00GHz);

            slopeEffInfo_50GHz = new EtalonSlopeEffInfo();
            slopeEffInfo_50GHz.Quad = etalonCurveSweepResult[fstItu_50GHz_Index];
            // Set slopeEffInfo_50GHz.QuadMinus
            {
                if (fstItu_50GHz_Index > 0)
                {
                    slopeEffInfo_50GHz.QuadMinus = etalonCurveSweepResult[fstItu_50GHz_Index - 1];
                }
                else
                {
                    slopeEffInfo_50GHz.QuadMinus = etalonCurveSweepResult[fstItu_50GHz_Index];
                }
            }
            // Set slopeEffInfo_50GHz.QuadPlus
            {
                if (fstItu_50GHz_Index < etalonCurveSweepResult.Count - 1)
                {
                    slopeEffInfo_50GHz.QuadPlus = etalonCurveSweepResult[fstItu_50GHz_Index + 1];
                }
                else
                {
                    slopeEffInfo_50GHz.QuadPlus = etalonCurveSweepResult[fstItu_50GHz_Index];
                }
            }
            slopeEffInfo_50GHz.SlopeEff = EtalonSlopeEffInfo.Calc_EtalonSlopeEff(slopeEffInfo_50GHz);
        }

        private void StoreToFile_EtalonCurveSweepResults(string filePath, List<EtalonCurveSweepResultItem> etalonCurveSweepResult)
        {
            using (StreamWriter streamWriter = new StreamWriter(filePath))
            {
                streamWriter.WriteLine("Rear_mA, Phase_mA, Frequency_GHz, LockRatio, Locker_TX_mA, Locker_RX_mA");

                foreach (EtalonCurveSweepResultItem etalonCurveSweepResultItem in etalonCurveSweepResult)
                {
                    streamWriter.WriteLine(string.Format("{0},{1},{2},{3},{4},{5}",
                        etalonCurveSweepResultItem.RearPhaseCurrent.Rear_mA,
                        etalonCurveSweepResultItem.RearPhaseCurrent.Phase_mA,
                        etalonCurveSweepResultItem.Frequency_GHz,
                        etalonCurveSweepResultItem.LockerCurrents.LockRatio,
                        etalonCurveSweepResultItem.LockerCurrents.TxCurrent_mA,
                        etalonCurveSweepResultItem.LockerCurrents.RxCurrent_mA));
                }
            }
        }

        /// <summary>
        /// rachel.wang 2016.11.04
        /// </summary>
        private double GetIndexCurrent(double index, double[] dataArray)
        {
            double lowIndex = 0;
            double highIndex = 0;
            double temp;
            temp = Math.Ceiling(index);
            if (temp != index)
            {
                lowIndex = temp;
                highIndex = temp + 1;
                //((y1 - y2) / (x1 - x2)) * (x3 - x2) + y2
                return (dataArray[(int)lowIndex + 2] - dataArray[(int)highIndex + 2]) / (lowIndex - highIndex) * (index - highIndex) + dataArray[(int)highIndex + 2];
            }
            else
            {
                return dataArray[(int)temp + 2];
            }
        }

        private class EtalonSlopeEffInfo
        {
            public EtalonCurveSweepResultItem Quad;
            public EtalonCurveSweepResultItem QuadPlus;
            public EtalonCurveSweepResultItem QuadMinus;
            public double SlopeEff = -99999;

            public static double Calc_EtalonSlopeEff(EtalonSlopeEffInfo slopeEffInfo)
            {
                double quadPlusErr = (slopeEffInfo.QuadPlus.LockerCurrents.RxCurrent_mA - slopeEffInfo.QuadPlus.LockerCurrents.TxCurrent_mA) / (slopeEffInfo.QuadPlus.LockerCurrents.RxCurrent_mA + slopeEffInfo.QuadPlus.LockerCurrents.TxCurrent_mA);
                double quadMinusErr = (slopeEffInfo.QuadMinus.LockerCurrents.RxCurrent_mA - slopeEffInfo.QuadMinus.LockerCurrents.TxCurrent_mA) / (slopeEffInfo.QuadMinus.LockerCurrents.RxCurrent_mA + slopeEffInfo.QuadMinus.LockerCurrents.TxCurrent_mA);

                return 100 * ((quadPlusErr - quadMinusErr) / (slopeEffInfo.QuadPlus.Frequency_GHz - slopeEffInfo.QuadMinus.Frequency_GHz));
            }
        }

        private struct RearPhaseCurrent
        {
            public double Rear_mA;
            public double Phase_mA;

            public RearPhaseCurrent(double rear_mA, double phase_mA)
            {
                this.Rear_mA = rear_mA;
                this.Phase_mA = phase_mA;
            }
        }

        private struct EtalonCurveSweepResultItem
        {
            public RearPhaseCurrent RearPhaseCurrent;
            public double Frequency_GHz;
            public DsdbrUtils.LockerCurrents LockerCurrents;

            public EtalonCurveSweepResultItem(RearPhaseCurrent rearPhaseCurrent, double frequency_GHz, DsdbrUtils.LockerCurrents lockerCurrents)
            {
                this.RearPhaseCurrent = rearPhaseCurrent;
                this.Frequency_GHz = frequency_GHz;
                this.LockerCurrents = lockerCurrents;
            }
        }

        public Type UserControl
        {
            get { return (typeof(Mod_PhaseScanGui)); }
        }

        #endregion
    }
}

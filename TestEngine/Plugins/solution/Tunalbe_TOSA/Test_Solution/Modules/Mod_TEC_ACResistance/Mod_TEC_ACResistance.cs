// [Copyright]
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
// Mod_TEC_ACResistance.cs
// Author: chongjian.liang, 2013
// Design: [Reference design documentation]

using System;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.Instruments;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Test Module to check AC resistance - chongjian.liang 2013.4.9
    /// </summary>
    public class Mod_TEC_ACResistance : ITestModule
    {
        #region ITestModule Members

        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, DatumList configData, InstrumentCollection instruments, ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {
            Inst_Ke2510 tecDsdbr = instruments["TecDsdbr"] as Inst_Ke2510;

            DatumList returnData = new DatumList();

            returnData.AddDouble("DUT_TEC_ACR", tecDsdbr.TecResistanceAC_ohm);

            return returnData;
        }

        public Type UserControl
        {
            get { return (typeof(Mod_TEC_ACResistanceGui)); }
        }

        #endregion
    }
}
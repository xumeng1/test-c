// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// IlmzGroupResults.cs
//
// Author: Tony.Foster, 2007
// Design: [Reference design documentation]

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Framework.Limits;
using Bookham.TestSolution.IlmzCommonData;
using Bookham.TestSolution.ILMZFinalData;
using Bookham.TestEngine.Equipment;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// TCMZ Group Results Module 
    /// </summary>
    public class IlmzGroupResults : ITestModule
    {
        #region ITestModule Members

        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, DatumList configData, InstrumentCollection instruments, ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {
            // Init local data 
            DatumList retData = new DatumList();
            IlmzChannels tcmzChannels = (IlmzChannels)configData.ReadReference("TcmzItuChannels");
            bool lowTempTestEnabled = configData.ReadBool("LowTempTestEnabled");

            double LOCK_FREQ_CHAN_TEMP_MAX = 0;
            double LOCK_FREQ_CHAN_TEMP_MIN = 0;
            string path_TempTestFailedFreqs = null;

            if (lowTempTestEnabled)
            {
                Specification spec = (Specification)configData.ReadReference("MainSpec");
                LOCK_FREQ_CHAN_TEMP_MAX = double.Parse(spec.GetParamLimit("LOCK_FREQ_CHAN_TEMP_MAX").HighLimit.ValueToString());
                LOCK_FREQ_CHAN_TEMP_MIN = double.Parse(spec.GetParamLimit("LOCK_FREQ_CHAN_TEMP_MIN").LowLimit.ValueToString());
                path_TempTestFailedFreqs = Path.Combine(configData.ReadString("RESULT_DIR"), string.Format("{0}_FailedFrequencies_{1}.csv", configData.ReadString("LASER_ID"), configData.ReadString("TIME_STAMP")));
            }

            bool fibrePwrChangeTcaseLow = false, fibrePwrChangeTcaseHigh = false, fibrePwrFailOverTcase = false,
                lockfreqChangeTcaseLow = false, lockfreqChangeTcaseHigh = false, lockFreqFailOverTcase = false;

            double avgLockRatio = 0;
            double fibrePwrChanTempMin = 999;
            double fibrePwrChanTempMax = -999;
            double lockFreqChanTempMin = 999,
                lockFreqChanTempMax = -999, phaseIChangeTcaseLow = 0, phaseIChangeTcaseHigh = 0, freqFirstChanFail = 0;

            double trackErrorLkFreqTCase = -999, trackErrorUlFreqTCase = -999, trackErrorPowerTCase = -999;

            int chanCount = 0;

            // Pass channels results
            ILMZChannel[] passChannel; //tcmzChannels.Passed; // by tim

            if (lowTempTestEnabled)
            {
                passChannel = tcmzChannels.AllOptions;
            }
            else
            {
                passChannel = tcmzChannels.Passed;
            }

            List<Frequency> frequencies_Failed = new List<Frequency>();

            foreach (ILMZChannel chan in passChannel)
            {
                try
                {
                    // AVERAGE_LOCK_RATIO
                    avgLockRatio += chan.MidTempData.GetValueDouble(EnumTosaParam.LockRatio);
                    chanCount++;

                    // If low temperature test enabled, calculate the following parameters taking low-temp data into account.
                    if (lowTempTestEnabled)
                    {
                        // FIBRE_POWER_CHAN_TEMP_MIN
                        double minPwrOverTemp = Math.Min(chan.LowTempData.GetValueDouble(TOSATempParam.PwrAtPeak_OL_dBm),
                                                         chan.HighTempData.GetValueDouble(TOSATempParam.PwrAtPeak_OL_dBm));
                        minPwrOverTemp = Math.Min(minPwrOverTemp, chan.MidTempData.GetValueDouble(EnumTosaParam.FibrePwrPeak_dBm));
                        if (minPwrOverTemp < fibrePwrChanTempMin)
                        {
                            fibrePwrChanTempMin = minPwrOverTemp;
                        }
                        // FIBRE_POWER_CHAN_TEMP_MAX
                        double maxPwrOverTemp = Math.Max(chan.LowTempData.GetValueDouble(TOSATempParam.PwrAtPeak_OL_dBm),
                                                         chan.HighTempData.GetValueDouble(TOSATempParam.PwrAtPeak_OL_dBm));
                        maxPwrOverTemp = Math.Max(maxPwrOverTemp, chan.MidTempData.GetValueDouble(EnumTosaParam.FibrePwrPeak_dBm));
                        if (maxPwrOverTemp > fibrePwrChanTempMax)
                        {
                            fibrePwrChanTempMax = maxPwrOverTemp;
                        }

                        #region Lock frequency over channel & temperature - chongjian.liang 2013.1.19

                        double ituFreq_GHz = chan.MidTempData.GetValueDouble(EnumTosaParam.ItuFreq_GHz);
                        double midFreq_GHz = chan.MidTempData.GetValueDouble(EnumTosaParam.Freq_GHz);
                        double lowFreq_GHz = chan.LowTempData.GetValueDouble(TOSATempParam.FreqLocked_GHz);
                        double highFreq_GHz = chan.HighTempData.GetValueDouble(TOSATempParam.FreqLocked_GHz);

                        double delta_MidToITU_GHz = midFreq_GHz - ituFreq_GHz;
                        double delta_LowToITU_GHz = lowFreq_GHz - ituFreq_GHz;
                        double delta_HighToITU_GHz = highFreq_GHz - ituFreq_GHz;

                        double delta_Min_GHz = Math.Min(delta_MidToITU_GHz, Math.Min(delta_LowToITU_GHz, delta_HighToITU_GHz));

                        if (delta_Min_GHz < lockFreqChanTempMin)
                        {
                            lockFreqChanTempMin = delta_Min_GHz;
                        }

                        double delta_Max_GHz = Math.Max(delta_MidToITU_GHz, Math.Max(delta_LowToITU_GHz, delta_HighToITU_GHz));

                        if (delta_Max_GHz > lockFreqChanTempMax)
                        {
                            lockFreqChanTempMax = delta_Max_GHz;
                        }

                        int ituIndex = chan.MidTempData.GetValueSint32(EnumTosaParam.ItuChannelIndex);

                        if (delta_Max_GHz > LOCK_FREQ_CHAN_TEMP_MAX)
                        {
                            frequencies_Failed.Add(new Frequency(ituIndex, ituFreq_GHz, midFreq_GHz, lowFreq_GHz, highFreq_GHz, delta_Max_GHz));
                        }
                        else if (delta_Min_GHz < LOCK_FREQ_CHAN_TEMP_MIN)
                        {
                            frequencies_Failed.Add(new Frequency(ituIndex, ituFreq_GHz, midFreq_GHz, lowFreq_GHz, highFreq_GHz, delta_Min_GHz));
                        } 
                        #endregion
                        
                        // Phase current over channel & temperature
                        double iPhaseMid_mA = chan.MidTempData.GetValueDouble(EnumTosaParam.IPhaseITU_mA);
                        double iPhaseDeltaLow = chan.LowTempData.GetValueDouble(TOSATempParam.IPhaseITU_mA) - iPhaseMid_mA;
                        if (Math.Abs(iPhaseDeltaLow) > Math.Abs(phaseIChangeTcaseLow)) phaseIChangeTcaseLow = iPhaseDeltaLow;
                        double iPhaseDeltaHigh = chan.HighTempData.GetValueDouble(TOSATempParam.IPhaseITU_mA) - iPhaseMid_mA;
                        if (Math.Abs(iPhaseDeltaHigh) > Math.Abs(phaseIChangeTcaseHigh)) phaseIChangeTcaseHigh = iPhaseDeltaHigh;


                        // TRACK_ERROR_FREQ_TCASE
                        double minLkFreqOverTemp = Math.Min(chan.LowTempData.GetValueDouble(TOSATempParam.FreqLocked_GHz),
                                                            chan.HighTempData.GetValueDouble(TOSATempParam.FreqLocked_GHz));
                        minLkFreqOverTemp = Math.Min(minLkFreqOverTemp, chan.MidTempData.GetValueDouble(EnumTosaParam.Freq_GHz));
                        double maxLkFreqOverTemp = Math.Max(chan.LowTempData.GetValueDouble(TOSATempParam.FreqLocked_GHz),
                                                            chan.HighTempData.GetValueDouble(TOSATempParam.FreqLocked_GHz));
                        maxLkFreqOverTemp = Math.Max(maxLkFreqOverTemp, chan.MidTempData.GetValueDouble(EnumTosaParam.Freq_GHz));
                        if (maxLkFreqOverTemp - minLkFreqOverTemp > trackErrorLkFreqTCase)
                        {
                            trackErrorLkFreqTCase = maxLkFreqOverTemp - minLkFreqOverTemp;
                        }


                        // TRACK_ERROR_ULFREQ_TCASE
                        double minUlFreqOverTemp = Math.Min(chan.LowTempData.GetValueDouble(TOSATempParam.FreqUnlocked_GHz),
                                                            chan.HighTempData.GetValueDouble(TOSATempParam.FreqUnlocked_GHz));
                        minUlFreqOverTemp = Math.Min(minUlFreqOverTemp, chan.MidTempData.GetValueDouble(EnumTosaParam.Freq_GHz));
                        double maxUlFreqOverTemp = Math.Max(chan.LowTempData.GetValueDouble(TOSATempParam.FreqUnlocked_GHz),
                                                            chan.HighTempData.GetValueDouble(TOSATempParam.FreqUnlocked_GHz));
                        maxUlFreqOverTemp = Math.Max(maxUlFreqOverTemp, chan.MidTempData.GetValueDouble(EnumTosaParam.Freq_GHz));
                        if (maxUlFreqOverTemp - minUlFreqOverTemp > trackErrorUlFreqTCase)
                        {
                            trackErrorUlFreqTCase = maxUlFreqOverTemp - minUlFreqOverTemp;
                        }


                        // TRACK_ERROR_PWR_TCASE
                        double minOlPowerOverTemp = Math.Min(chan.LowTempData.GetValueDouble(TOSATempParam.PwrAtPeak_OL_dBm),
                                                            chan.HighTempData.GetValueDouble(TOSATempParam.PwrAtPeak_OL_dBm));
                        minOlPowerOverTemp = Math.Min(minOlPowerOverTemp, chan.MidTempData.GetValueDouble(EnumTosaParam.FibrePwrPeak_dBm));
                        double maxOlPowerOverTemp = Math.Max(chan.LowTempData.GetValueDouble(TOSATempParam.PwrAtPeak_OL_dBm),
                                                            chan.HighTempData.GetValueDouble(TOSATempParam.PwrAtPeak_OL_dBm));
                        maxOlPowerOverTemp = Math.Max(maxOlPowerOverTemp, chan.MidTempData.GetValueDouble(EnumTosaParam.FibrePwrPeak_dBm));
                        if (maxOlPowerOverTemp - minOlPowerOverTemp > trackErrorPowerTCase)
                        {
                            trackErrorPowerTCase = maxOlPowerOverTemp - minOlPowerOverTemp;
                        }
                    }
                    else
                    {
                        // FIBRE_POWER_CHAN_TEMP_MIN
                        double minPwrOverTemp = chan.HighTempData.GetValueDouble(TOSATempParam.PwrAtPeak_OL_dBm);
                        minPwrOverTemp = Math.Min(minPwrOverTemp, chan.MidTempData.GetValueDouble(EnumTosaParam.FibrePwrPeak_dBm));
                        if (minPwrOverTemp < fibrePwrChanTempMin)
                        {
                            fibrePwrChanTempMin = minPwrOverTemp;
                        }
                        // FIBRE_POWER_CHAN_TEMP_MAX
                        double maxPwrOverTemp = chan.HighTempData.GetValueDouble(TOSATempParam.PwrAtPeak_OL_dBm);
                        maxPwrOverTemp = Math.Max(maxPwrOverTemp, chan.MidTempData.GetValueDouble(EnumTosaParam.FibrePwrPeak_dBm));
                        if (maxPwrOverTemp > fibrePwrChanTempMax)
                        {
                            fibrePwrChanTempMax = maxPwrOverTemp;
                        }


                        // Lock frequency over channel & temperature
                        double ituFreq_GHz = chan.MidTempData.GetValueDouble(EnumTosaParam.ItuFreq_GHz);
                        double minFreqDevOverTemp = chan.HighTempData.GetValueDouble(TOSATempParam.FreqLocked_GHz) - ituFreq_GHz;
                        minFreqDevOverTemp = Math.Min(minFreqDevOverTemp, chan.MidTempData.GetValueDouble(EnumTosaParam.Freq_GHz) - ituFreq_GHz);
                        if (minFreqDevOverTemp < lockFreqChanTempMin) lockFreqChanTempMin = minFreqDevOverTemp;

                        double maxFreqDevOverTemp = chan.HighTempData.GetValueDouble(TOSATempParam.FreqLocked_GHz) - ituFreq_GHz;
                        maxFreqDevOverTemp = Math.Max(maxFreqDevOverTemp, chan.MidTempData.GetValueDouble(EnumTosaParam.Freq_GHz) - ituFreq_GHz);
                        if (maxFreqDevOverTemp > lockFreqChanTempMax) lockFreqChanTempMax = maxFreqDevOverTemp;


                        // Phase current over channel & temperature
                        double iPhaseMid_mA = chan.MidTempData.GetValueDouble(EnumTosaParam.IPhaseITU_mA);
                        double iPhaseDeltaHigh = chan.HighTempData.GetValueDouble(TOSATempParam.IPhaseITU_mA) - iPhaseMid_mA;
                        if (Math.Abs(iPhaseDeltaHigh) > Math.Abs(phaseIChangeTcaseHigh)) phaseIChangeTcaseHigh = iPhaseDeltaHigh;


                        // TRACK_ERROR_FREQ_TCASE
                        double minLkFreqOverTemp = chan.HighTempData.GetValueDouble(TOSATempParam.FreqLocked_GHz);
                        minLkFreqOverTemp = Math.Min(minLkFreqOverTemp, chan.MidTempData.GetValueDouble(EnumTosaParam.Freq_GHz));
                        double maxLkFreqOverTemp = chan.HighTempData.GetValueDouble(TOSATempParam.FreqLocked_GHz);
                        maxLkFreqOverTemp = Math.Max(maxLkFreqOverTemp, chan.MidTempData.GetValueDouble(EnumTosaParam.Freq_GHz));
                        if (maxLkFreqOverTemp - minLkFreqOverTemp > trackErrorLkFreqTCase)
                        {
                            trackErrorLkFreqTCase = maxLkFreqOverTemp - minLkFreqOverTemp;
                        }


                        // TRACK_ERROR_ULFREQ_TCASE
                        double minUlFreqOverTemp = chan.HighTempData.GetValueDouble(TOSATempParam.FreqUnlocked_GHz);
                        minUlFreqOverTemp = Math.Min(minUlFreqOverTemp, chan.MidTempData.GetValueDouble(EnumTosaParam.Freq_GHz));
                        double maxUlFreqOverTemp = chan.HighTempData.GetValueDouble(TOSATempParam.FreqUnlocked_GHz);
                        maxUlFreqOverTemp = Math.Max(maxUlFreqOverTemp, chan.MidTempData.GetValueDouble(EnumTosaParam.Freq_GHz));
                        if (maxUlFreqOverTemp - minUlFreqOverTemp > trackErrorUlFreqTCase)
                        {
                            trackErrorUlFreqTCase = maxUlFreqOverTemp - minUlFreqOverTemp;
                        }


                        // TRACK_ERROR_PWR_TCASE
                        double minOlPowerOverTemp = chan.HighTempData.GetValueDouble(TOSATempParam.PwrAtPeak_OL_dBm);
                        minOlPowerOverTemp = Math.Min(minOlPowerOverTemp, chan.MidTempData.GetValueDouble(EnumTosaParam.FibrePwrPeak_dBm));
                        double maxOlPowerOverTemp = chan.HighTempData.GetValueDouble(TOSATempParam.PwrAtPeak_OL_dBm);
                        maxOlPowerOverTemp = Math.Max(maxOlPowerOverTemp, chan.MidTempData.GetValueDouble(EnumTosaParam.FibrePwrPeak_dBm));
                        if (maxOlPowerOverTemp - minOlPowerOverTemp > trackErrorPowerTCase)
                        {
                            trackErrorPowerTCase = maxOlPowerOverTemp - minOlPowerOverTemp;
                        }
                    }
                }
                catch { }
            }

            // Fail channels results
            ILMZChannel[] failChannels = tcmzChannels.Failed;
            if (failChannels.Length > 0)
            {
                freqFirstChanFail = double.PositiveInfinity;
                foreach (ILMZChannel chan in failChannels)
                {
                    // Frequency of first fail channel (lowest frequency)
                    if (chan.MidTempData.GetValueDouble(EnumTosaParam.ItuFreq_GHz) < freqFirstChanFail)
                        freqFirstChanFail = chan.MidTempData.GetValueDouble(EnumTosaParam.ItuFreq_GHz);

                    // Low temp Fail flags
                    if (lowTempTestEnabled)
                    {
                        foreach (TOSATempParam param in chan.LowTempData.FailedParams)
                        {
                            switch (param)
                            {
                                case TOSATempParam.PwrAtPeak_CL_dBm:
                                    fibrePwrFailOverTcase = true;
                                    break;
                                case TOSATempParam.PwrAtPeakChange_CL_dB:
                                    fibrePwrChangeTcaseLow = true;
                                    break;
                                case TOSATempParam.FreqLocked_GHz:
                                    lockFreqFailOverTcase = true;
                                    break;
                                case TOSATempParam.FreqLockedChange_GHz:
                                    lockfreqChangeTcaseLow = true;
                                    break;
                                default:
                                    break;
                            }
                        }
                    }

                    // High temp Fail flags
                    foreach (TOSATempParam param in chan.HighTempData.FailedParams)
                    {
                        switch (param)
                        {
                            case TOSATempParam.PwrAtPeak_CL_dBm:
                                fibrePwrFailOverTcase = true;
                                break;
                            case TOSATempParam.PwrAtPeakChange_CL_dB:
                                fibrePwrChangeTcaseHigh = true;
                                break;
                            case TOSATempParam.FreqLocked_GHz:
                                lockFreqFailOverTcase = true;
                                break;
                            case TOSATempParam.FreqLockedChange_GHz:
                                lockfreqChangeTcaseHigh = true;
                                break;
                            default:
                                break;
                        }
                    }
                }
            }

            #region Write temp test failed frequencies to file - chongjian.liang 2013.1.19

            if (lowTempTestEnabled && frequencies_Failed.Count > 0)
            {
                using (StreamWriter csvWriter = new StreamWriter(path_TempTestFailedFreqs))
                {
                    csvWriter.WriteLine("ITUIndex,ITUFrequency_GHz,MidTempFreq_GHz,LowTempFreq_GHz,HighTempFreq_GHz,LOCK_FREQ_CHAN_TEMP_GHz");

                    foreach (Frequency frequency_Failed in frequencies_Failed)
                    {
                        csvWriter.Write(frequency_Failed.ITUIndex + ",");
                        csvWriter.Write(frequency_Failed.ITUFrequency_GHz + ",");
                        csvWriter.Write(frequency_Failed.MidTempFreq_GHz + ",");
                        csvWriter.Write(frequency_Failed.LowTempFreq_GHz + ",");
                        csvWriter.Write(frequency_Failed.HighTempFreq_GHz + ",");
                        csvWriter.WriteLine(frequency_Failed.LOCK_FREQ_CHAN_TEMP_GHz);
                        csvWriter.Flush();
                    }
                } 
            }
            #endregion

            // Return data
            double averageLockRatio = 0;
            if(chanCount>0) averageLockRatio = avgLockRatio / chanCount;

            if (!lowTempTestEnabled)
            {
                retData.AddDouble("AVERAGE_LOCK_RATIO", averageLockRatio);
            }
            retData.AddDouble("OPTICAL_FREQ_FIRST_CHAN_FAIL", freqFirstChanFail);
            retData.AddSint32("FULL_PASS_CHAN_COUNT_OVERALL", passChannel.Length);

            retData.AddDouble("FIBRE_POWER_CHAN_TEMP_MIN", fibrePwrChanTempMin);
            retData.AddDouble("FIBRE_POWER_CHAN_TEMP_MAX", fibrePwrChanTempMax);
            retData.AddSint32("FIBRE_POWER_CHANGE_TCASE_HIGH", fibrePwrChangeTcaseHigh ? 0 : 1);
            if (lowTempTestEnabled)
            {
                retData.AddSint32("FIBRE_POWER_CHANGE_TCASE_LOW", fibrePwrChangeTcaseLow ? 0 : 1);
                retData.AddSint32("FIBRE_POWER_FAIL_OVER_TCASE", fibrePwrChangeTcaseLow || fibrePwrChangeTcaseHigh ? 0 : 1);
            }
            else
            {
                retData.AddSint32("FIBRE_POWER_FAIL_OVER_TCASE", fibrePwrChangeTcaseHigh ? 0 : 1);
            }
            retData.AddSint32("FIBRE_POWER_FAIL_CHAN_TEMP", fibrePwrFailOverTcase ? 0 : 1);
            if (!lowTempTestEnabled)
            {
                retData.AddDouble("TRACK_ERROR_PWR_TCASE", trackErrorPowerTCase);
            }

            retData.AddDouble("LOCK_FREQ_CHAN_TEMP_MIN", lockFreqChanTempMin);
            retData.AddDouble("LOCK_FREQ_CHAN_TEMP_MAX", lockFreqChanTempMax);
            retData.AddSint32("LOCK_FREQ_CHANGE_TCASE_HIGH", lockfreqChangeTcaseHigh ? 0 : 1);
            if (lowTempTestEnabled)
            {
                retData.AddSint32("LOCK_FREQ_CHANGE_TCASE_LOW", lockfreqChangeTcaseLow ? 0 : 1);
                retData.AddSint32("LOCK_FREQ_FAIL_OVER_TCASE", lockfreqChangeTcaseLow || lockfreqChangeTcaseHigh ? 0 : 1);
            }
            else
            {
                retData.AddSint32("LOCK_FREQ_FAIL_OVER_TCASE", lockfreqChangeTcaseHigh ? 0 : 1);
            }
            retData.AddSint32("LOCK_FREQ_FAIL_CHAN_TEMP", lockFreqFailOverTcase ? 0 : 1);

            if(!lowTempTestEnabled)
            {
                retData.AddDouble("TRACK_ERROR_FREQ_TCASE", trackErrorLkFreqTCase);
                retData.AddDouble("TRACK_ERROR_ULFREQ_TCASE", trackErrorUlFreqTCase);
            }

            if (lowTempTestEnabled)
            {
                retData.AddDouble("PHASE_I_CHANGE_TCASE_LOW", phaseIChangeTcaseLow);
            }
            if (lowTempTestEnabled)
            {
                retData.AddDouble("PHASE_I_CHANGE_TCASE_HIGH", phaseIChangeTcaseHigh);
                retData.AddDouble("TRACK_ERROR_FREQ_TCASE", trackErrorLkFreqTCase);
                retData.AddDouble("TRACK_ERROR_ULFREQ_TCASE", trackErrorUlFreqTCase);
                retData.AddDouble("AVERAGE_LOCK_RATIO", averageLockRatio);
                retData.AddDouble("TRACK_ERROR_PWR_TCASE", trackErrorPowerTCase);
            }
            return retData;
        }

        struct Frequency
        {
            public int ITUIndex;
            public double ITUFrequency_GHz;
            public double MidTempFreq_GHz;
            public double LowTempFreq_GHz;
            public double HighTempFreq_GHz;
            public double LOCK_FREQ_CHAN_TEMP_GHz;

            public Frequency(int ituIndex, double ituFrequency_GHz, double midTempFreq_GHz, double lowTempFreq_GHz, double highTempFreq_GHz, double LOCK_FREQ_CHAN_TEMP_GHz)
            {
                this.ITUIndex = ituIndex;
                this.ITUFrequency_GHz = ituFrequency_GHz;
                this.MidTempFreq_GHz = midTempFreq_GHz;
                this.LowTempFreq_GHz = lowTempFreq_GHz;
                this.HighTempFreq_GHz = highTempFreq_GHz;
                this.LOCK_FREQ_CHAN_TEMP_GHz = LOCK_FREQ_CHAN_TEMP_GHz;
            }
        }

        #endregion

        public Type UserControl
        {
            // GUI for this module
            get { return null; }
        }
    }
}

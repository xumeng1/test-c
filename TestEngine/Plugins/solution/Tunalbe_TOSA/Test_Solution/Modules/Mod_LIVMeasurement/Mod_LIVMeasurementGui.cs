// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// TXFP_LIVMeasurementGui.cs
//
// Author: jerryxw.hu, 2009
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using Bookham.TestEngine.Framework.InternalData;
using System.IO;
using NPlot;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Module Template GUI User Control
    /// </summary>
    public partial class TXFP_LIVMeasurementGui : Bookham.TestEngine.Framework.Messages.ManagedCtlBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public TXFP_LIVMeasurementGui()
        {
            /* Call designer generated code. */
            InitializeComponent();

            /* TODO - Write constructor code here. */
        }

        /// <summary>
        /// Incoming message event handler
        /// </summary>
        /// <param name="payload">Message Payload</param>
        /// <param name="inMsgSeq">Incoming message sequence number</param>
        /// <param name="respSeq">Outgoing message sequence number</param>
        private void ModuleGui_MsgReceived(object payload, long inMsgSeq, long respSeq)
        {
            if (payload.GetType() == typeof(PlotLIVData))
            {
                PlotLIVData livData = (PlotLIVData)payload;
                this.MessageLabel.Text = "LIV Sweep Done !";

                try
                {
                    // clear the data in the NPlot object
                    LivPlot.Clear();
                    double[] sectionCurrentData = livData.LivData[LIVDataType.SectionCurrent];
                    //double[] sectionVoltageData = livData.LivData[LIVDataType.SectionVoltage];
                    double[] powerData = livData.LivData[LIVDataType.Power];
                    //double[] photocurrentData = livData.LivData[LIVDataType.Photocurrent];

                    // set up the plot lines
                    //LinePlot voltPlot = new LinePlot(sectionVoltageData, sectionCurrentData);
                    //voltPlot.Color = Color.DarkBlue;
                    //voltPlot.Label = "SectionVoltage_V";

                    LinePlot powerPlot = new LinePlot(powerData,sectionCurrentData);
                    powerPlot.Color = Color.DarkRed;
                    powerPlot.Label = "Power_mW";

                    //LinePlot photocurrentPlot = new LinePlot(photocurrentData,sectionCurrentData);
                    //photocurrentPlot.Color = Color.DarkGreen;
                    //photocurrentPlot.Label = "Photocurrent_mA";

                    // set up the plot itself
                    //LivPlot.Add(voltPlot);
                    LivPlot.Add(powerPlot);
                    //LivPlot.Add(photocurrentPlot);
                    LivPlot.Legend = new Legend();

                    LivPlot.XAxis1.Label = "Gain current (mA)";
                    LivPlot.YAxis1.Label = "L_I (mW_mA)";
                    //and redraw it
                    LivPlot.Refresh();

                    //Display lines
                    this.LivPlot.Visible = true;
                    LivPlot.Refresh();
                    
                    this.sendToWorker(true);
                }
                catch (SystemException)
                {
                    this.LivPlot.Visible = false;
                    // Any GUI errors should not be fatal.
                    throw new ArgumentException("Invalid message type received: " + payload.GetType().ToString());
                }
            }
            else if (payload.GetType() == typeof(String))
            {
                this.MessageLabel.Text = (string)payload;
                this.sendToWorker(true);
            }
        }
    }
}

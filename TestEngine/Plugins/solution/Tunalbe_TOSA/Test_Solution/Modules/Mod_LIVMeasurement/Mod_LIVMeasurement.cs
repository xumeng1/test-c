// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// Mod_LIVMeasurement.cs
//
// Author: jerryxw.hu, 2009
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using System.IO.Ports;
using System.Collections;
using System.IO;
using System.Threading;


using Bookham.ToolKit.Mz;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.Algorithms;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.Instruments;
using Bookham.TestSolution.Instruments;
using Bookham.TestSolution.IlmzCommonData;
using Bookham.TestSolution.IlmzCommonUtils;
using Bookham.TestSolution.IlmzCommonInstrs;


namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Test Module Template.
    /// </summary>
    public class Mod_LIVMeasurement : ITestModule
    {
        #region ITestModule Members

        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, DatumList configData, InstrumentCollection instruments, ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {
            engine.GuiShow();
            engine.GuiToFront();
            DatumList returnData = new DatumList();

            double finalIth_mA = 0;
            engine.SendToGui("Preparing for sweep...");

            Inst_Fcu2Asic fcu = (Inst_Fcu2Asic)instruments["Fcu2Asic"];
            if (fcu.IsOnline == false)
            {
                fcu.IsOnline = true;
            }

            string timestamp = configData.ReadString("TIME_STAMP");

            //Inst_Titan_TMC dutTxfp = (Inst_Titan_TMC)instruments["dutTxfp"];
            IInstType_OpticalPowerMeter powerMeter = (IInstType_OpticalPowerMeter)instruments["OpmMz"];
            Inst_Ke2510 Tecdsdbr = (Inst_Ke2510)instruments["TecDsdbr"];
            //InstType_OpticalPowerMeter powerMeter = (InstType_OpticalPowerMeter)instruments["powerMeter"];

            double startCurrent_mA = configData.ReadDouble("StartCurrent_mA");
            double stopCurrent_mA = configData.ReadDouble("StopCurrent_mA");
            //double startCurrent_mA = 16;
            //double stopCurrent_mA = 41;
            int numOfPoints = configData.ReadSint32("NbrPoints");
            int stepDelay_mS = configData.ReadSint32("StepDelay_mS");
            string serialNumber = configData.ReadString("SerialNumber");
            string filePath = configData.ReadString("SweepFilePath");
            //double pdRef_slope = configData.ReadDouble("PdRef_slope");
            //double pdRef_Intercept = configData.ReadDouble("PdRef_Intercept");
            double PowerFixer = configData.ReadDouble("PowerFixer");
            minPwr_mW = configData.ReadDouble("minPwr_mW");
            minD2ldi2 = configData.ReadDouble("minD2ldi2");

            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            //power up laser
            DsdbrChannelData oneChannel = new DsdbrChannelData();
            oneChannel.Setup.IFsFirst_mA = configData.ReadDouble("TC_FSA");
            oneChannel.Setup.IFsSecond_mA = configData.ReadDouble("TC_FSB");
            oneChannel.Setup.IGain_mA = startCurrent_mA;
            oneChannel.Setup.IRear_mA = configData.ReadDouble("TC_REAR");
            oneChannel.Setup.ISoa_mA = configData.ReadDouble("TC_SOA");
            oneChannel.Setup.IRearSoa_mA = configData.ReadDouble("TC_REARSOA");
            oneChannel.Setup.FrontPair = Convert.ToInt32(configData.ReadDouble("TC_FRONT_PAIR"));
            oneChannel.Setup.IPhase_mA = configData.ReadDouble("TC_PHASE");
            DsdbrUtils.SetDsdbrCurrents_mA(oneChannel.Setup);

            fcu.IimbLeft_mA = (float)configData.ReadDouble("MzPeak_LEFT_IMBForITH");
            fcu.IimbRight_mA = (float)configData.ReadDouble("MzPeak_RIGHT_IMBForITH");
            fcu.VLeftModBias_Volt = (float)configData.ReadDouble("TC_LEFT_BIAS");
            fcu.VRightModBias_Volt = (float)configData.ReadDouble("TC_RIGHT_BIAS");


            double LaserTemperature = Tecdsdbr.SensorTemperatureActual_C;
            engine.SendStatusMsg(string.Format("LaserTemperature: {0:0.0000}C", LaserTemperature));
            Thread.Sleep(3000);

            engine.SendToGui("Do LIV sweep...");

            powerMeter.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_mW;

            double[] currentArray_mA = new double[numOfPoints];
            //double[] voltageArray_V = new double[numOfPoints];
            double[] powerArray_mW = new double[numOfPoints];

            double currentStep_mA = (stopCurrent_mA - startCurrent_mA) / (numOfPoints - 1);
            double newCurrent_mA;

            for (int i = 0; i < numOfPoints; i++)
            {

                newCurrent_mA = startCurrent_mA + i * currentStep_mA;
                //int gainDac = DsdbrConvert.DoubleToDac(DsdbrDacChannel.Gain, newCurrent_mA);
                //dutTxfp.SetGainDAC(gainDac);
                fcu.IGain_mA = (float)newCurrent_mA;
                //Thread.Sleep(100);
                System.Threading.Thread.Sleep(stepDelay_mS);
                currentArray_mA[i] = newCurrent_mA;
                //voltageArray_V[i] = 1233444;// ReadVoltage
                powerArray_mW[i] = powerMeter.ReadPower() * PowerFixer;


            }


            PlotLIVData plotLivData = new PlotLIVData("");
            plotLivData.LivData[LIVDataType.SectionCurrent] = currentArray_mA;
            //plotLivData.LivData[LIVDataType.SectionVoltage] = voltageArray_V;
            //plotLivData.LivData[LIVDataType.Power] = powerArray_mW;
            plotLivData.LivData[LIVDataType.Power] =Alg_ArrayFunctions.NormalizeArray(powerArray_mW);

            //1st+ element often random! just equate to 5th say
            for (int ii = 0; ii < 4; ii++)
            {
                plotLivData.LivData[LIVDataType.Power][ii] = plotLivData.LivData[LIVDataType.Power][4];
            }

            //X(mW) = [Ip(mA) - c]/m
            //Calculate Power from photocurrent step 1 subtract intercept
            //double[] tempArray = Alg_ArrayFunctions.SubtractFromEachArrayElement
            //    (plotLivData.LivData[LIVDataType.Power], pdRef_Intercept);
            //plotLivData.LivData[LIVDataType.Power] = Alg_ArrayFunctions.DivideEachArrayElement(tempArray, pdRef_slope);



            // setup currents old


            // Send plot data to GUI
            engine.SendToGui(plotLivData);
            Thread.Sleep(3000);



            //find Max for Threshold point using Index fom 2line Ith
            double[] powerArray = plotLivData.LivData[LIVDataType.Power];
            int maxPowerIndex = 0;//Alg_PointSearch.FindFirstIndexOfMaxValueInArray(powerArray);
            for (int i = 1; i < powerArray.Length; i++)
            {
                if (powerArray[i] > powerArray[maxPowerIndex])
                {

                    maxPowerIndex = i;
                }
            }
            double maxPower = powerArray[maxPowerIndex];


            double[] dldi = null;
            double[] d2ldi2 = null;
            try
            {
                if (maxPower < minPwr_mW)
                {
                    // Not enough pwr to do Thresh calcs
                    finalIth_mA = 0.0;
                }
                else
                {
                    ////Calculate approx Threshold
                    //double ith2Line = Alg_Ith2Line.CalculateIth2Line(plotLivData.LivData[LIVDataType.SectionCurrent], plotLivData.LivData[LIVDataType.Power], plotLivData.LivData[LIVDataType.Power][0], plotLivData.LivData[LIVDataType.Power][maxPowerIndex], discountFactor);
                    ////Find approx index 
                    //int ith2LineIndex = Alg_ArrayFunctions.FindIndexOfNearestElement(plotLivData.LivData[LIVDataType.SectionCurrent], ith2Line);

                    //Calc 1st diff
                    int numSmoothPoints_dLdI = configData.ReadSint32("NumSmoothPoints_dLdI");
                    dldi = Alg_Differential.SmoothedDifferential(plotLivData.LivData[LIVDataType.SectionCurrent], plotLivData.LivData[LIVDataType.Power], numSmoothPoints_dLdI);

                    //calc 2st diff
                    int numSmoothPoints_d2LdI2 = configData.ReadSint32("NumSmoothPoints_d2LdI2");
                    d2ldi2 = Alg_Differential.SmoothedDifferential(plotLivData.LivData[LIVDataType.SectionCurrent], dldi, numSmoothPoints_d2LdI2);

                    for (int i = 2; i < d2ldi2.Length - 2; i++)
                    {
                        if (d2ldi2[i] > minD2ldi2       //change to 0.06 from 0.3 for ITH calc   
                            && d2ldi2[i - 1] < d2ldi2[i]
                            && d2ldi2[i - 2] < d2ldi2[i]
                            && d2ldi2[i + 1] < d2ldi2[i]
                            && d2ldi2[i + 2] < d2ldi2[i]
                            )
                        {
                            finalIth_mA = plotLivData.LivData[LIVDataType.SectionCurrent][i];
                            break;
                        }
                    }

                    ////find Max for Threshold point
                    //double[] searchArray = Alg_ArrayFunctions.ExtractSubArray(d2ldi2, ith2LineIndex - finalIthScanWidthPoints, ith2LineIndex + finalIthScanWidthPoints);

                    //int currentIndex = Alg_PointSearch.FindFirstIndexOfMaxValueInArray(searchArray);

                    //currentIndex = currentIndex + ith2LineIndex - finalIthScanWidthPoints;  

                    //if (currentIndex <0 || currentIndex > powerArray.Length)
                    //{
                    //    engine.ErrorInModule("Index was outside the bounds of the array,can't find thresh current!");
                    //}

                    ////Look up index for Thresh current
                    //finalIth_mA = plotLivData.LivData[LIVDataType.SectionCurrent][currentIndex];
                }
            }
            catch (Exception ex)
            { }
            engine.SendStatusMsg("ITH_mA = " + finalIth_mA.ToString());
            
            #region SavePlotData
            // Have to perform some processing to convert the ArrayList to a double[].
            double[] sectionCurrentData = plotLivData.LivData[LIVDataType.SectionCurrent];
            //double[] sectionVoltageData = plotLivData.LivData[LIVDataType.SectionVoltage];
            //double[] photocurrentData = plotLivData.LivData[LIVDataType.Photocurrent];
            double[] powerData = plotLivData.LivData[LIVDataType.Power];
            // Store the Plot data in a CSV file. 

            // Make a temporary area in the system temp path to store the plot data   

            string plotFileFullPath = filePath + @"\LIV_Plot_" + serialNumber + "_" + timestamp + ".csv";// string.Format("{0:yyyyMMddHHmmss}.csv", DateTime.Now);
            plotFileFullPath = Path.Combine(filePath
                , Path.GetFileNameWithoutExtension(plotFileFullPath) + ".csv");
            StreamWriter sw = new StreamWriter(plotFileFullPath);
            sw.WriteLine("SectionCurrent(mA)," + "Power(mW),"+ "Normalize_Power,"+"1st,"+"2nd" );
            for (int i = 0; i < sectionCurrentData.Length; i++)
            {
                //sectionVoltageData[i],,{5}
                sw.WriteLine("{0},{1},{2},{3},{4}", sectionCurrentData[i], powerArray_mW[i].ToString(),powerData[i].ToString(), (dldi == null ? 0 : dldi[i]), (d2ldi2 == null ? 0 : d2ldi2[i]));
            }
            sw.Flush();
            sw.Close();
            //File.Copy(plotFileFullPath, Path.Combine(@"C:\TXFPCoCDebug\TestDev\pcas"
            //    , Path.GetFileNameWithoutExtension(plotFileFullPath) + "-" + finalIth_mA.ToString() + ".csv"));


            #endregion

            if (configData.IsPresent("PRE_ITH"))
            {
                double Pre_ITH = configData.ReadDouble("PRE_ITH");

                double Post_ITH = finalIth_mA;

                if (Pre_ITH == 0)//Temp for old pre and new post limit file. jack.zhang 2012-05-10
                {
                    Pre_ITH = Post_ITH;
                }

                returnData.AddDouble("ITH_SHIFT_RATIO", (Pre_ITH == 0) ? 100 : (Post_ITH - Pre_ITH) / Pre_ITH * 100.0);
            }

            //returnData.AddDouble("I_AT_XMA", opticsDetectorCurrent_mA);
            returnData.AddDouble("ITH", finalIth_mA);
            //returnData.AddDouble("P_AT_XMA", powerAtXmA);
            returnData.AddFileLink("PLOT_LIV", plotFileFullPath);
            return returnData;
        }

        /// <summary>
        /// Add Plot data to a text file, specified by the plotFilePath experience.
        /// </summary>
        /// <param name="plotFilePath">The Full path of the file to write the plot data to.</param>
        /// <param name="plotTitle">The Title of the plot.</param>
        /// <param name="plotUnits">The Units of the data contained in the plot</param>
        /// <param name="data">An Array List of Plot data, assumed to be of type double.</param>
        /// <param name="endOfFile">True if this is the last data plot to be added to plotFilePath.</param>
        /// </summary>
        /// 
        private void writeDataToPlotFile(string plotFilePath, string plotTitle, string plotUnits, ArrayList data, bool endOfFile)
        {

            //PlotAxis currentPlot = new PlotAxis("SectionCurrent", "mA", sectionCurrentData);
            //PlotAxis voltPlot = new PlotAxis("SectionVoltage", "V", sectionVoltageData);
            //PlotAxis photocurrentPlot = new PlotAxis("Photocurrent", "mA", photocurrentData);
            //PlotAxis powerPlot = new PlotAxis("Power", "mW", powerData);

            //DatumPlot plotData = new DatumPlot("PLOT_LIV");
            //plotData.AddAxis(voltPlot);
            //plotData.AddAxis(powerPlot);
            //plotData.AddAxis(photocurrentPlot);
            //plotData.AddAxis(currentPlot);
            //ArrayList sectionCurrentArr = new ArrayList();
            //ArrayList sectionVoltageArr = new ArrayList();
            //ArrayList photocurrentArr = new ArrayList();
            //ArrayList powerArr = new ArrayList();
            //for (int i = 0; i < sectionCurrentData.Length; i++)
            //{
            //    sectionCurrentArr.Add(sectionCurrentData[i]);
            //    sectionVoltageArr.Add(sectionVoltageData[i]);
            //    photocurrentArr.Add(photocurrentData[i]);
            //    powerArr.Add(powerData[i]);
            //}
            ////Write out the plot data.
            //writeDataToPlotFile(plotFileFullPath, "SectionCurrent", "mA", sectionCurrentArr, false);
            //writeDataToPlotFile(plotFileFullPath, "SectionVoltage", "V", sectionVoltageArr, false);
            //writeDataToPlotFile(plotFileFullPath, "Photocurrent", "mA", photocurrentArr, false);
            //writeDataToPlotFile(plotFileFullPath, "Power", "mW", powerArr, true);

            //Create a DatumFileLink, that the External Read/Write Library will use to point the external database 
            //to the file eventually.
            //DatumFileLink plot = new DatumFileLink("PLOT_LIV", localDir, plotFilename);
            //returnData.Add(plot);

            ////Open a StreamWriter.
            //StreamWriter writer = File.AppendText(plotFilePath);

            ////Write the Plot header.
            //writer.Write("LIV_PLOT, ");
            //writer.Write(plotTitle + ", ");
            //writer.Write(plotUnits + ", ");
            //writer.WriteLine();

            ////Stuff the plot data into the file.
            //for (int i = 0; i < data.Count; i++)
            //{
            //    if ((i != 0) && ((i % 8) == 0))
            //    {
            //        writer.WriteLine();
            //    }

            //    writer.Write((double)data[i]);

            //    if (!((i == data.Count - 1) && (endOfFile)))
            //    {
            //        writer.Write(", ");
            //    }
            //}
            //writer.WriteLine();
            //writer.Close();
        }

        #endregion

        public Type UserControl
        {
            get { return (typeof(TXFP_LIVMeasurementGui)); }
        }

        private double minPwr_mW = 0.7;
        private double minD2ldi2 = 0.06;
        private const double startPower_mW = 1.0;
        private const double discountFactor = 0.9;
        private const int finalIthScanWidthPoints = 5;
    }

    /// <summary>
    /// A structure to contain the LIV sweep data
    /// </summary>
    public class PlotLIVData
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public PlotLIVData(string plotFilename)
        {
            this.livData = new Dictionary<LIVDataType, double[]>();
            this.plotLivDataFile = plotFilename;
        }
        /// <summary>
        /// Private dictionary - protects us from losing the entire dictionary!
        /// </summary>
        private Dictionary<LIVDataType, double[]> livData;
        /// <summary>
        /// Plot liv data file name
        /// </summary>
        private string plotLivDataFile;

        /// <summary>
        /// SweepData dictionary object
        /// </summary>
        public Dictionary<LIVDataType, double[]> LivData
        {
            get { return livData; }
        }
        /// <summary>
        /// Plot lIV data file name
        /// </summary>
        public string PlotLivDataFile
        {
            get { return plotLivDataFile; }
        }
    }


    /// <summary>
    /// LIV data type
    /// </summary>
    public enum LIVDataType
    {
        /// <summary>
        /// Section current
        /// </summary>
        SectionCurrent,
        /// <summary>
        /// Section voltage
        /// </summary>
        SectionVoltage,
        /// <summary>
        /// Photocurrent
        /// </summary>
        Photocurrent,
        /// <summary>
        /// Power
        /// </summary>
        Power,
    }
}

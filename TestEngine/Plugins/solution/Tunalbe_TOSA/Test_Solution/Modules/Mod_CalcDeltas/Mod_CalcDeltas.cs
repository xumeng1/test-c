// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// Mod_CalcDeltas.cs
//
// Author: scott.alexander, 2014
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Test Module Template.
    /// </summary>
    public class Mod_CalcDeltas : ITestModule
    {
        #region ITestModule Members

        public enum DeltaType : int
        {

            SUBTRACT = 0,
            PERCENTAGE_CHANGE = 1,
            PERCENTAGE_RATIO = 2,
            DIVIDE = 3
        }




        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, DatumList configData, InstrumentCollection instruments, ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {

            DatumList returnData = new DatumList();
            DeltaType Delta;
            int DisplayTime_ms = 750;

            if (configData.IsPresent("TITLE"))
                engine.SendToGui(new DatumString("SET_TITLE", configData.ReadString("TITLE")));

            if (configData.IsPresent("DISPLAY_TIME_S"))
                DisplayTime_ms = (int)configData.ReadDouble("DISPLAY_TIME_S") * 1000;

            if (configData.IsPresent("INPUT_VALUE_1"))
                engine.SendToGui(new DatumDouble("INPUT_VALUE_1", configData.ReadDouble("INPUT_VALUE_1")));
            else
                engine.ErrorInModule("INPUT_VALUE_1 not defined");

            if (configData.IsPresent("INPUT_VALUE_2"))
                engine.SendToGui(new DatumDouble("INPUT_VALUE_2", configData.ReadDouble("INPUT_VALUE_2")));
            else
                engine.ErrorInModule("INPUT_VALUE_2 not defined");



            if ((configData.IsPresent("DELTA_TYPE")))
            {
                engine.GuiToFront();
                engine.GuiShow();

                Delta = (DeltaType)configData.ReadSint32("DELTA_TYPE");
                switch (Delta)
                {

                    case DeltaType.SUBTRACT:
                        engine.SendToGui(new DatumString("SET_OPERATION", "-"));
                        engine.SendToGui(new DatumDouble("OUTPUT_VALUE", configData.ReadDouble("INPUT_VALUE_1") - configData.ReadDouble("INPUT_VALUE_2")));
                        System.Threading.Thread.Sleep(DisplayTime_ms);
                        returnData.AddDouble("CALC_DELTA", configData.ReadDouble("INPUT_VALUE_1") - configData.ReadDouble("INPUT_VALUE_2"));
                        break;

                    case DeltaType.PERCENTAGE_CHANGE:
                        engine.SendToGui(new DatumString("SET_OPERATION", "+/- %"));
                        engine.SendToGui(new DatumDouble("OUTPUT_VALUE", ((configData.ReadDouble("INPUT_VALUE_1") / configData.ReadDouble("INPUT_VALUE_2")) - 1) * 100.0));
                        System.Threading.Thread.Sleep(DisplayTime_ms);
                        returnData.AddDouble("CALC_DELTA", ((configData.ReadDouble("INPUT_VALUE_1") / configData.ReadDouble("INPUT_VALUE_2")) - 1) * 100.0);
                        break;

                    case DeltaType.PERCENTAGE_RATIO:
                        engine.SendToGui(new DatumString("SET_OPERATION", "%"));
                        engine.SendToGui(new DatumDouble("OUTPUT_VALUE", (configData.ReadDouble("INPUT_VALUE_1") / configData.ReadDouble("INPUT_VALUE_2") * 100.0)));
                        System.Threading.Thread.Sleep(DisplayTime_ms);
                        returnData.AddDouble("CALC_DELTA", (configData.ReadDouble("INPUT_VALUE_1") / configData.ReadDouble("INPUT_VALUE_2") * 100.0));
                        break;

                    case DeltaType.DIVIDE:
                        engine.SendToGui(new DatumString("SET_OPERATION", "/"));
                        engine.SendToGui(new DatumDouble("OUTPUT_VALUE", configData.ReadDouble("INPUT_VALUE_1") / configData.ReadDouble("INPUT_VALUE_2")));
                        System.Threading.Thread.Sleep(DisplayTime_ms);
                        returnData.AddDouble("CALC_DELTA", (configData.ReadDouble("INPUT_VALUE_1") / configData.ReadDouble("INPUT_VALUE_2")));
                        break;

                }


            }


            else //Default
            {
                returnData.AddDouble("CALC_DELTA", configData.ReadDouble("INPUT_VALUE_1") - configData.ReadDouble("INPUT_VALUE_2"));
            }


            // return data       

            return returnData;
        }

        public Type UserControl
        {
            get { return (typeof(Mod_CalcDeltasGui)); }
        }

        #endregion
    }
}

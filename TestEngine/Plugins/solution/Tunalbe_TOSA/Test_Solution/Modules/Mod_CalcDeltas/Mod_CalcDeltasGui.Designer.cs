// [Copyright]
//
// Bookham Test Engine
// Mod_CalcDeltas
//
// Bookham.TestSolution.TestModules/Mod_CalcDeltasGui
// 
// Author: paul.annetts
// Design: TODO

namespace Bookham.TestSolution.TestModules
{
    partial class Mod_CalcDeltasGui
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Title_label = new System.Windows.Forms.Label();
            this.Input1_label = new System.Windows.Forms.Label();
            this.Operation_label = new System.Windows.Forms.Label();
            this.Input2_label = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Output_label = new System.Windows.Forms.Label();
            this.Input1Name_label = new System.Windows.Forms.Label();
            this.Input2Name_label = new System.Windows.Forms.Label();
            this.OutputName_label = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Title_label
            // 
            this.Title_label.AutoSize = true;
            this.Title_label.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.Title_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Title_label.Location = new System.Drawing.Point(77, 42);
            this.Title_label.Name = "Title_label";
            this.Title_label.Size = new System.Drawing.Size(231, 37);
            this.Title_label.TabIndex = 153;
            this.Title_label.Text = "Calulate Delta";
            // 
            // Input1_label
            // 
            this.Input1_label.BackColor = System.Drawing.Color.White;
            this.Input1_label.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Input1_label.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.Input1_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Input1_label.ForeColor = System.Drawing.Color.Black;
            this.Input1_label.Location = new System.Drawing.Point(84, 138);
            this.Input1_label.Name = "Input1_label";
            this.Input1_label.Size = new System.Drawing.Size(131, 45);
            this.Input1_label.TabIndex = 154;
            this.Input1_label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Operation_label
            // 
            this.Operation_label.BackColor = System.Drawing.Color.White;
            this.Operation_label.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Operation_label.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.Operation_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Operation_label.ForeColor = System.Drawing.Color.Black;
            this.Operation_label.Location = new System.Drawing.Point(245, 138);
            this.Operation_label.Name = "Operation_label";
            this.Operation_label.Size = new System.Drawing.Size(74, 45);
            this.Operation_label.TabIndex = 155;
            this.Operation_label.Text = "-";
            this.Operation_label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Input2_label
            // 
            this.Input2_label.BackColor = System.Drawing.Color.White;
            this.Input2_label.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Input2_label.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.Input2_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Input2_label.ForeColor = System.Drawing.Color.Black;
            this.Input2_label.Location = new System.Drawing.Point(347, 138);
            this.Input2_label.Name = "Input2_label";
            this.Input2_label.Size = new System.Drawing.Size(131, 45);
            this.Input2_label.TabIndex = 156;
            this.Input2_label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label2.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(510, 138);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 45);
            this.label2.TabIndex = 157;
            this.label2.Text = "=";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Output_label
            // 
            this.Output_label.BackColor = System.Drawing.Color.White;
            this.Output_label.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Output_label.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.Output_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Output_label.ForeColor = System.Drawing.Color.Black;
            this.Output_label.Location = new System.Drawing.Point(597, 138);
            this.Output_label.Name = "Output_label";
            this.Output_label.Size = new System.Drawing.Size(131, 45);
            this.Output_label.TabIndex = 158;
            this.Output_label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Input1Name_label
            // 
            this.Input1Name_label.AutoSize = true;
            this.Input1Name_label.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.Input1Name_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Input1Name_label.Location = new System.Drawing.Point(118, 97);
            this.Input1Name_label.Name = "Input1Name_label";
            this.Input1Name_label.Size = new System.Drawing.Size(53, 16);
            this.Input1Name_label.TabIndex = 159;
            this.Input1Name_label.Text = "Input 1";
            // 
            // Input2Name_label
            // 
            this.Input2Name_label.AutoSize = true;
            this.Input2Name_label.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.Input2Name_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Input2Name_label.Location = new System.Drawing.Point(378, 97);
            this.Input2Name_label.Name = "Input2Name_label";
            this.Input2Name_label.Size = new System.Drawing.Size(53, 16);
            this.Input2Name_label.TabIndex = 160;
            this.Input2Name_label.Text = "Input 2";
            // 
            // OutputName_label
            // 
            this.OutputName_label.AutoSize = true;
            this.OutputName_label.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.OutputName_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OutputName_label.Location = new System.Drawing.Point(631, 97);
            this.OutputName_label.Name = "OutputName_label";
            this.OutputName_label.Size = new System.Drawing.Size(52, 16);
            this.OutputName_label.TabIndex = 161;
            this.OutputName_label.Text = "Output";
            // 
            // Mod_CalcDeltasGui
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.OutputName_label);
            this.Controls.Add(this.Input2Name_label);
            this.Controls.Add(this.Input1Name_label);
            this.Controls.Add(this.Output_label);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Input2_label);
            this.Controls.Add(this.Operation_label);
            this.Controls.Add(this.Input1_label);
            this.Controls.Add(this.Title_label);
            this.Name = "Mod_CalcDeltasGui";
            this.MsgReceived += new Bookham.TestEngine.Framework.Messages.ManagedCtlBase.MsgReceivedDlg(this.ModuleGui_MsgReceived);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Title_label;
        private System.Windows.Forms.Label Input1_label;
        private System.Windows.Forms.Label Operation_label;
        private System.Windows.Forms.Label Input2_label;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label Output_label;
        private System.Windows.Forms.Label Input1Name_label;
        private System.Windows.Forms.Label Input2Name_label;
        private System.Windows.Forms.Label OutputName_label;
    }
}

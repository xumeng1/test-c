// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// Mod_CalcDeltasGui.cs
//
// Author: scott.alexander, 2014
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Bookham.TestEngine.Framework.InternalData;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Module Template GUI User Control
    /// </summary>
    public partial class Mod_CalcDeltasGui : Bookham.TestEngine.Framework.Messages.ManagedCtlBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public Mod_CalcDeltasGui()
        {
            /* Call designer generated code. */
            InitializeComponent();

            /* TODO - Write constructor code here. */
        }

        /// <summary>
        /// Incoming message event handler
        /// </summary>
        /// <param name="payload">Message Payload</param>
        /// <param name="inMsgSeq">Incoming message sequence number</param>
        /// <param name="respSeq">Outgoing message sequence number</param>
        private void ModuleGui_MsgReceived(object payload, long inMsgSeq, long respSeq)
        {

            if (payload.GetType() == typeof(DatumString))
            {

                DatumString DatumItem = (DatumString)payload;

                switch (DatumItem.Name)
                {

                    case "SET_TITLE":
                        Title_label.Text = DatumItem.Value;
                        break;

                    case "SET_OPERATION":
                        Operation_label.Text = DatumItem.Value;
                        break;


                }


            }


            else if (payload.GetType() == typeof(DatumDouble))
            {

                DatumDouble DatumItem = (DatumDouble)payload;

                switch (DatumItem.Name)
                {


                    case "INPUT_VALUE_1":
                        Input1_label.Text = DatumItem.Value.ToString("0.0000");
                        break;

                    case "INPUT_VALUE_2":
                        Input2_label.Text = DatumItem.Value.ToString("0.0000");
                        break;

                    case "OUTPUT_VALUE":
                        Output_label.Text = DatumItem.Value.ToString("0.0000");
                        break;

                }

            }
        }
    }
}

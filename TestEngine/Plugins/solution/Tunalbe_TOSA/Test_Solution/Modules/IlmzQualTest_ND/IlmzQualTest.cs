// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// IlmzQualTest_ND.cs
//
// Author: alice.huang, 2010
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.ToolKit.Mz;
using Bookham.TestSolution.IlmzCommonData;
using Bookham.TestSolution.ILMZFinalData;
using Bookham.TestEngine.Framework.Limits;
using Bookham.TestSolution.IlmzCommonUtils;
using Bookham.TestLibrary.Algorithms;
using Bookham.TestLibrary.Utilities;
using Bookham.TestLibrary.InstrTypes ;
using System.IO;
using ICSharpCode.SharpZipLib.Zip;
using Bookham.TestLibrary.Utilities;
using Bookham.TestLibrary.Instruments;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Test Module Template.
    /// </summary>
    public class IlmzQualTest : ITestModule
    {
        private Alg_MZAnalysis.MzFeaturePowerType featureToCenter;  // Center MIN to put most bias on the left arm. Center QUAD for minimum Vpi and lower loss
        private IlMzDriverUtils ilmzUtils;
        private double[] MZFixedModBias_volt;
        private double MzCtrlINominal_A;
        //private double mzImbLeftIdeal_mA;
        //private double mzImbRightIdeal_mA;
        private double mzImbLeftMinLimit_mA;
        private double mzImbLeftMaxLimit_mA;
        private double mzImbRightMinLimit_mA;
        private double mzImbRightMaxLimit_mA;
        private double mzInitFixedModBias_volt;
        private double mzInitSweepMaxAbs_V;
        //private double mzVleftMinQuadLimit;
        //private double mzVleftMaxQuadLimit;
        //private double vcmMinLimit;
        //private double vcmMaxLimit;
        private double vpiMinLimit;
        private double vpiMaxLimit;
        private double mzVleftInset = 0.05;
        private double initialVcmOffset = 0;
        private int MZSourceMeasureDelay_ms = 5;
        private int NumberPoints;
        private double MZTapBias_V;
        private double tapInline_V;
        private string MzFileDirectory;
        private string dutSerialNbr;
        private Inst_Ke2510 Optical_switch;
        private IInstType_TecController gbTecController;
        private InstType_ElectricalSource vccSource;
        private  Specification mainSpec;
        private bool ArmSourceByAsic = true;
        private List<string> filesToZip;

        private DatumList returnData = new DatumList();
        private ITestEngine ImbSweepEngine = null;
        private string ChirpType = null;

        #region ITestModule Members

        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, 
            DatumList configData, InstrumentCollection instruments, 
            ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {


            #region read config
            // get MZ instruments
            IlMzInstruments mzInstrs = (IlMzInstruments)configData.ReadReference("MzInstruments");
            Optical_switch = (Inst_Ke2510)instruments["GBTecController"];
            gbTecController = (IInstType_TecController)instruments["GBTecController"];
            vccSource = (InstType_ElectricalSource)instruments["VccSource"];
            // get list of DSDBR channels
            IlmzQualChanPreviousData[] chansRefData = (IlmzQualChanPreviousData[])configData.ReadReference("ChanReferenceData");
            // get test spec (for limit checking)
            mainSpec = (Specification)configData.ReadReference("TestSpec");
            MzItuAtVcmData.InitLookup(mainSpec);

            this.filesToZip = configData.ReadReference("FilesToZip") as List<string>;

            //this.mzImbLeftIdeal_mA = Convert.ToDouble(mainSpec.GetParamLimit("TC_MZ_IMB_LEFT").LowLimit.ValueToString());
            //this.mzImbRightIdeal_mA = Convert.ToDouble(mainSpec.GetParamLimit("TC_MZ_IMB_RIGHT").LowLimit.ValueToString());
            //this.vcmMinLimit = Convert.ToDouble(mainSpec.GetParamLimit("TC_MZ_VCM_CAL_LIMIT_MIN").LowLimit.ValueToString());
            //this.vcmMaxLimit = Convert.ToDouble(mainSpec.GetParamLimit("TC_MZ_VCM_CAL_LIMIT_MAX").HighLimit.ValueToString());
            this.vpiMinLimit = Convert.ToDouble(mainSpec.GetParamLimit("TC_MZ_VPI_CAL_LIMIT_MIN").LowLimit.ValueToString());
            this.vpiMaxLimit = Convert.ToDouble(mainSpec.GetParamLimit("TC_MZ_VPI_CAL_LIMIT_MAX").HighLimit.ValueToString());
            //this.mzVleftMinQuadLimit = Convert.ToDouble(mainSpec.GetParamLimit("CH_MZ_BIAS_L_QUAD_V").LowLimit.ValueToString());
            //this.mzVleftMaxQuadLimit = Convert.ToDouble(mainSpec.GetParamLimit("CH_MZ_BIAS_L_QUAD_V").HighLimit.ValueToString());
            this.mzImbLeftMinLimit_mA = ParamManager.Spec.CH_MZ_CTRL_L_I.Low;
            this.mzImbLeftMaxLimit_mA = ParamManager.Spec.CH_MZ_CTRL_L_I.High;
            this.mzImbRightMinLimit_mA = ParamManager.Spec.CH_MZ_CTRL_R_I.Low;
            this.mzImbRightMaxLimit_mA = ParamManager.Spec.CH_MZ_CTRL_R_I.High;

            this.mzInitFixedModBias_volt = configData.ReadDouble("MZInitFixedModBias_volt");
            this.mzInitSweepMaxAbs_V = configData.ReadDouble("MZInitSweepMaxAbs_V");

            // details to go into our filenames
            this.MzFileDirectory = configData.ReadString("MzFileDirectory");
            this.dutSerialNbr = configData.ReadString("DutSerialNbr");
            // MZ scan setup data
            this.MZFixedModBias_volt = configData.ReadDoubleArray("MZFixedModBias_volt");

            if (ParamManager.Conditions.IsThermalPhase)
            {
                this.MzCtrlINominal_A = configData.ReadDouble("MzCtrlINominal_thermal_mA") / 1000;
            }
            else
            {
                this.MzCtrlINominal_A = configData.ReadDouble("MzCtrlINominal_mA") / 1000;
            }
            this.NumberPoints = configData.ReadSint32("NumberOfPoints");
            this.MZTapBias_V = configData.ReadDouble("MZTapBias_V");
            if (mzInstrs.InlineTapOnline)
            {
                this.tapInline_V = configData.ReadDouble("MZInlineTapBias_V");
            }
            else
            {
                // belt and braces - this should be the value anyway by C# default.
                this.tapInline_V = 0.0;
            }
            DsdbrTuning.IphaseMin_mA = Convert.ToDouble(mainSpec.GetParamLimit("CH_PHASE_I_LOWER_EOL").LowLimit.ValueToString());
            DsdbrTuning.IphaseMax_mA = Convert.ToDouble(mainSpec.GetParamLimit("CH_PHASE_I_UPPER_EOL").HighLimit.ValueToString());
            DsdbrTuning.LockRatioTolerance = configData.ReadDouble("LockRatioTolerance");
            this.MZSourceMeasureDelay_ms = configData.ReadSint32("MZSourceMeasureDelay_ms");

            this.ChirpType = configData.ReadString("ChirpType");
#endregion

            // Initialise GUI
            ImbSweepEngine = engine;
            engine.GuiShow();
            engine.GuiToFront();

            #region setup for sweep
            // setup our private data 
            this.ilmzUtils = new IlMzDriverUtils(mzInstrs);
            this.ilmzUtils.opticalSwitch = Optical_switch;
            // Setup instruments for sweep
            TcmzMzSweep_Config mzConfig = new TcmzMzSweep_Config(configData);
            if (!ArmSourceByAsic)
            {
                ilmzUtils.CleanupAfterSweep();
            }

            double mzMaxBias_V = mzConfig.MzMaxBias_V;
            IlMzDriverUtils.MaxModBias_V = mzConfig.MzMaxBias_V;
            double fixedModBias_V = mzConfig.MZInitFixedModBias_volt;
            double imbalance_A = mzConfig.MzCtrlINominal_mA / 1000;
            double sweepMaxAbs_V = mzConfig.MZInitSweepMaxAbs_V;
            sweepMaxAbs_V = Math.Abs(sweepMaxAbs_V);
            double sweepStop_V = 0.0;
            double sweepStart_V = -sweepMaxAbs_V;
            double stepSize_V = mzConfig.MZInitSweepStepSize_mV / 1000;
            int nbrPoints = (int)(1 + Math.Abs(sweepStart_V - sweepStop_V) / stepSize_V);
            double tapBias_V = mzConfig.MZTapBias_V;

            double currentCompliance_A = mzConfig.MZCurrentCompliance_A;
            double currentRange_A = mzConfig.MZCurrentRange_A;
            double voltageCompliance_V = mzConfig.MZVoltageCompliance_V;
            double voltageRange_V = mzConfig.MZVoltageRange_V;
            int numberOfAverages = mzConfig.MzInitNumberOfAverages;
            double integrationRate = mzConfig.MZIntegrationRate;
            double sourceMeasureDelay = mzConfig.MZSourceMeasureDelay_s;
            double powerMeterAveragingTime = mzConfig.MZPowerMeterAveragingTime_s;
            /*
             * Set this next variable to select the test method.
             * 
             * Quad at zero will allow the quadrature point to move close to zero when Vpi is large.
             * Min at zero follows the original XS, keeping the NULL of the response at zero.
             * 
             */
            // In order to achieve maximum bias depth and hence minimise Vpi we use the imbalance currents to set quadrature close to zero
            this.featureToCenter = Alg_MZAnalysis.MzFeaturePowerType.Quad;  // Quadrature to minimise Vpi            
            //this.featureToCenter = Alg_MZAnalysis.MzFeaturePowerType.Min;  // Quadrature to minimise Vpi 
            engine.SendToGui("Preparing instruments for sweep");
             mzInstrs.PowerMeter.Mode = Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.MeterMode.Absolute_mW;           
            #endregion
             
            #region control DUT temp
             int rthCount = 0;
             int rthDelay_mS = 500;
             int numRthTries = 10;
             double rth_Reading;
             double rth_Min = Convert.ToDouble(mainSpec.GetParamLimit("CH_LASER_RTH").LowLimit.ValueToString());
             double rth_Max = Convert.ToDouble(mainSpec.GetParamLimit("CH_LASER_RTH").HighLimit.ValueToString());
             // Allow a number of attempts to settle
             do
             {
                 if (!gbTecController.OutputEnabled)
                     gbTecController.OutputEnabled = true;//to avoid Ke2510 turn off sometime. jack.zhang 2012-11-13
                 System.Threading.Thread.Sleep(rthDelay_mS);
                 rth_Reading = gbTecController.SensorResistanceActual_ohm;
             }
             while ((rth_Reading < rth_Min || rth_Reading > rth_Max) && rthCount++ < numRthTries);
             returnData.AddDouble("Laser_Rth", rth_Reading);
             #endregion control DUT temp

             List<IlmzQualChanData> chanMzCheckDataList = new List<IlmzQualChanData>();

            
            bool isError = false;
            string errorInformation = "";
            int chanIndex = 1;
            foreach (IlmzQualChanPreviousData chanRefData in chansRefData)
            {
                DsdbrChannelData oneChannelData = chanRefData.DsdbrData;
                doChannelPhaseScan(engine,oneChannelData, chanIndex++);
                
                IlmzQualChanData chanCheckData = measureMzAtFrequency(
                    engine, mzInstrs, chanRefData, configData, out isError, out errorInformation);
                chanMzCheckDataList.Add(chanCheckData);

                if (isError)
                {
                    break;
                }
            }

            chanMzCheckDataList.Sort();            
            // Clean up
            mzInstrs.PowerMeter.EnableLogging = false;

            // Alice.Huang     2010-02-26
            //commented all methjod related with   IInstType_TriggeredOpticalPowerMeter   with  InstType_OpticalPowerMeter 
            mzInstrs.PowerMeter.EnableInputTrigger(false);
           
            // return the data

            //DatumList returnData = processMzSweepData(chanMzCheckDataList);
            processMzSweepData(chanMzCheckDataList);
            returnData.AddBool("IsError", isError);
            if (isError)
            {
                returnData.AddString("ErrorInformation", errorInformation);
            }
            return returnData;
        }

        /// <summary>
        /// do phase scan from 0 to 15ma, 100 points. and save scan result to file
        /// </summary>
        /// <param name="oneChannelData">channel current setting</param>
        /// <param name="chanIndex">channel index</param>
        private void doChannelPhaseScan(ITestEngine engine, DsdbrChannelData oneChannelData, int chanIndex)
        {
            string phaseCurrentFile=@"Configuration\TOSA Final\FinalTest\phaseCalidata.csv";
            string phaseScanFile = Util_GenerateFileName.GenWithTimestamp(MzFileDirectory, 
                                    "CH"+oneChannelData.ItuChannelIndex.ToString()+"_QualTest_PhaseScan", dutSerialNbr,"csv");


            CsvReader cr=new CsvReader ();
            List<string[]> lines=cr.ReadFile(phaseCurrentFile);
            List<double> phaseLines = new List<double>();
            List<double> FreqList = new List<double>();
            engine.SendToGui("Do phase current sweep on channel " + oneChannelData.ItuChannelIndex.ToString()+"  "+oneChannelData.ItuFreq_GHz.ToString()+"GHz");
            for (int i = 0; i < lines.Count; i++)
            {
                double _phase_mA = double.Parse(lines[i][0]);
                phaseLines.Add(_phase_mA);
                oneChannelData.Setup.IPhase_mA = _phase_mA;
                DsdbrUtils.SetDsdbrCurrents_mA(oneChannelData.Setup);
                System.Threading.Thread.Sleep(50);
                double freq = Measurements.ReadFrequency_GHz();
                FreqList.Add(freq);
                
                try
                {
                    int percentage =(int)Math.Round(i / (lines.Count*1.0 - 1) * 100); 
                    engine.SendToGui(percentage);
                }
                catch (Exception e)
                {
                    string a = e.Message;
                }

                
            }

            if (File.Exists(phaseScanFile))
            {
                File.Delete(phaseScanFile);
            }
            else
            {
                StreamWriter sw = new StreamWriter(phaseScanFile);
                sw.WriteLine("Itu_Frequency of this channel is :" + oneChannelData.ItuFreq_GHz.ToString());
                sw.WriteLine("I_Phase_mA,Freq_Ghz");
                for (int i = 0; i < FreqList.Count; i++)
                {
                    sw.Write(phaseLines[i].ToString() + ",");
                    sw.WriteLine(FreqList[i].ToString());
                }
                sw.Close();
            }
            
            switch (chanIndex)
            {
                case 1:
                    returnData.AddFileLink("PhaseScan_lowChannel_file", phaseScanFile);
                    break;
                case 2:
                    returnData.AddFileLink("PhaseScan_midChannel_file", phaseScanFile);
                    break;
                case 3:
                    returnData.AddFileLink("PhaseScan_highChannel_file", phaseScanFile);
                    break;
            }
            return;

        }

        private IlmzQualChanData measureMzAtFrequency(ITestEngine engine,
            IlMzInstruments mzInstrs, IlmzQualChanPreviousData chanPrevData, DatumList configData,
            out bool isError, out string errorInformation)
        {
            isError = false;
            errorInformation = "";
            engine.SendToGui("Do qual test on Channel " + chanPrevData.DsdbrData.ItuFreq_GHz.ToString());
            IlmzQualChanData checkData = new IlmzQualChanData();
            checkData.ChanRefData = chanPrevData;
            checkData .ChanMeasData = new IlmzChannelQualTestRstData();
            // Set Vpi and Vcm limits in analysis wrapper 
            MzAnalysisWrapper.vpiMinLimit = this.vpiMinLimit;
            MzAnalysisWrapper.vpiMaxLimit = this.vpiMaxLimit;
            
            // setup the DSDBR
            DsdbrUtils.SetDsdbrCurrents_mA(chanPrevData.DsdbrData.Setup);
            //DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic.ISoa_mA =(float)( configData.ReadDouble("SoaSetupCurrent_mA")); ;//Jack.Zhang for Initial MZ Calibration
            int chanNbr = chanPrevData.DsdbrData.ItuChannelIndex;

            //checkData.ChanRefData.DsdbrData = chanPrevData;

            // check GB Tec resistance reading

            #region Thermistor Resistances
            int rthCount = 0;
            int rthDelay_mS = 500;
            int numRthTries = 10;
            double rth_Reading;
            double rth_Min =Convert .ToDouble( mainSpec .GetParamLimit("CH_LASER_RTH").LowLimit.ValueToString());
            double rth_Max = Convert.ToDouble(mainSpec.GetParamLimit("CH_LASER_RTH").HighLimit.ValueToString());
            // Allow a number of attempts to settle
            do
            {
                if (!gbTecController.OutputEnabled)
                    gbTecController.OutputEnabled = true;//to avoid Ke2510 turn off sometime. jack.zhang 2012-11-13
                System.Threading.Thread.Sleep(rthDelay_mS);
                rth_Reading = gbTecController.SensorResistanceActual_ohm;
            }
            while ((rth_Reading < rth_Min || rth_Reading > rth_Max) && rthCount++ < numRthTries);
            checkData.ChanMeasData.RthGB_ohm = rth_Reading;
            checkData.ChanMeasData.VTecGB_V = gbTecController.TecVoltageActual_volt;
            checkData.ChanMeasData.ITecGB_mA = gbTecController.TecCurrentActual_amp * 1000;
            checkData.ChanMeasData.VccTotalCurrent_mA = vccSource.CurrentActual_amp * 1000;
            #endregion Thermistor Resistances
            // Cache the WL in mzUtils so that we can calibrate power after the sweep.
            int numberOfAverages = configData.ReadSint32("MZInitNumberOfAverages");
            double integrationRate = configData.ReadDouble("MZIntegrationRate");
            double sourceMeasureDelay = configData.ReadDouble("MZSourceMeasureDelay_s");
            double powerMeterAveragingTime = configData.ReadDouble("MZPowerMeterAveragingTime_s");

            checkData.ChanMeasData.Freq_GHz = Measurements.ReadFrequency_GHz();
            mzInstrs.PowerMeter.Wavelength_nm = Alg_FreqWlConvert.Freq_GHz_TO_Wave_nm(checkData.ChanMeasData.Freq_GHz);
            mzInstrs.PowerMeter.Mode = Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.MeterMode.Absolute_mW;
            mzInstrs.PowerMeter.Range = configData.ReadDouble("OpmRangeInit_mW");
            mzInstrs.PowerMeter.Range = InstType_OpticalPowerMeter.AutoRange;
            Measurements.FrequencyWithoutMeter = checkData.ChanMeasData.Freq_GHz;

            DsdbrUtils.LockerCurrents lc = DsdbrUtils.ReadLockerCurrents();
            checkData.ChanMeasData.IrxLock_mA = lc.RxCurrent_mA;
            checkData.ChanMeasData.ItxLock_mA = lc.TxCurrent_mA;
            checkData.ChanMeasData.LockRatio = lc.LockRatio;

            // if required locker ratio adjustment, do the soft lock tuning 
            if (configData.ReadBool("DoSoftLockerRatioTuning"))
            {
                double refLockerRatio = chanPrevData.RefChanData.LockerRatio;
                double refPhaseRatioSlopeEff = chanPrevData.RefChanData.PhaseRatioSlopeEff;

                DatumList res = DsdbrTuning.DynamicSoftLock(refLockerRatio, refPhaseRatioSlopeEff);

                checkData .ChanMeasData .ItxLock_CL_mA  = res.ReadDouble("ITx_mA");
                checkData .ChanMeasData .IrxLock_CL_mA  = res.ReadDouble("IRx_mA");
                checkData.ChanMeasData.LockRatio_CL = res.ReadDouble("LockRatio");

                double refFreq = chanPrevData.RefChanData.Freq_Ghz;
                checkData.ChanMeasData.FreqLock_GHz = Measurements.ReadFrequency_GHz();
                checkData.ChanMeasData.FreqLockedChange_GHz = checkData.ChanMeasData.FreqLock_GHz - refFreq;
            }

            double iImbLeft_mA =0.0 ;
            double iImbRight_mA =0.0 ;

            //Alice.Huang    2010-08-31
            // set power meter range to its initial range before any sweep 

            
            bool redoLISweeps = true;
            MzSweepDataItuChannel mzItuChanData;
            do
            {
                // loop through the common mode bias voltages                                
                #region we use the quad currents on the Min_Power point closer to zero
               
                string preErrorInfo = "";
                
                double newLeftBias_V = 0; //Jack.Zhang set left and right bias arm to zero Voltage for imb diff sweep
                double newRightBias_V = 0;
                engine.SendToGui(0);
                engine.SendToGui("Running Differential Imbalance sweep on channel " + chanNbr.ToString() + "   " + chanPrevData.DsdbrData.ItuFreq_GHz.ToString()+"GHz");
                ilmzUtils.ImbSweepProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(ImbSweepProgressChanged);
                ILMZSweepResult diffIsweep = ilmzUtils.ImbArm_DifferentialSweep
                     (newLeftBias_V, newRightBias_V,
                      0.0, 2 * MzCtrlINominal_A, NumberPoints * 3 / 4, MZSourceMeasureDelay_ms, tapInline_V, MZTapBias_V, ArmSourceByAsic);
                string sweepFilePrefix= string .Format("Ch{0}_QualTest_DiffLI_",chanNbr );
                string diffLIFileName;
                recordSweepData(diffIsweep, sweepFilePrefix, out diffLIFileName);
                checkData.ChanMeasData.DiffLI_ImbsFile = diffLIFileName;
                // Display the plot data
                engine.SendToGui(diffIsweep);

                #region Analyse the data per ChirpType - chongjian.liang 2014.8.7

                MzAnalysisWrapper.MzAnalysisResults mzAnlyDiffI = null;

                if (this.ChirpType == "ND")
                {
                    if (ParamManager.Conditions.IsThermalPhase)
                    {
                        mzAnlyDiffI = MzAnalysisWrapper.ZeroChirp_DifferentialSweep(diffIsweep, MzCtrlINominal_A, 0);
                    }
                    else
                    {
                        mzAnlyDiffI = MzAnalysisWrapper.DiffImbalance_NegChirp_FindFeaturePointByRef(diffIsweep, MzCtrlINominal_A, 0, this.featureToCenter);
                    }
                }
                else if (this.ChirpType == "ZD")
                {
                    if (ParamManager.Conditions.IsThermalPhase)
                    {
                        mzAnlyDiffI = MzAnalysisWrapper.DiffImbalance_NegChirp_FindFeaturePointByRef(diffIsweep, MzCtrlINominal_A, 0, this.featureToCenter);
                    }
                    else
                    {
                        mzAnlyDiffI = MzAnalysisWrapper.ZeroChirp_DifferentialSweep(diffIsweep, MzCtrlINominal_A);
                    }
                } 
                #endregion

                // calculate imbalance currents
                iImbLeft_mA = mzAnlyDiffI.Quad_SrcL * 1000;
                iImbRight_mA = mzAnlyDiffI.Quad_SrcR * 1000;

                if (iImbLeft_mA < this.mzImbLeftMinLimit_mA || iImbLeft_mA > this.mzImbLeftMaxLimit_mA ||
                    iImbRight_mA < this.mzImbRightMinLimit_mA || iImbRight_mA > this.mzImbRightMaxLimit_mA)
                {
                    // We have used the maximum amount of MZ modulator bias and the maximum amount of 
                    // MZ imbalance bias and we still cannot meet the spec. 
                    // Stop now. This device is unuseable with the present limits.
                    // 2007-11-29: Ken.Wu: Imbalance too high issue. Bug-fix 1#.
                    //      Record error information into PCAS.
                    //engine.RaiseNonParamFail(0, "MZ imbalance is too high to be useable within the limits for this device type. " +
                    //    "Unable to continue test.");
                    errorInformation += preErrorInfo;
                    isError = true;
                    break;
                }                   

                redoLISweeps = false;       
                #endregion
                
                // Set imbalance arms to get quad power output
                ilmzUtils.SetCurrent(SourceMeter.LeftImbArm, mzAnlyDiffI .Quad_SrcL);
                ilmzUtils.SetCurrent(SourceMeter.RightImbArm, mzAnlyDiffI .Quad_SrcR);
                                

            } while (redoLISweeps);

            #region Differential LV 
            // find the common mode voltage we are using
            double vcm_V = checkData.ChanRefData.RefChanData.Vcm_V;            
            double iSOA_mA = DsdbrUtils.ReadSectionCurrent_mA(DSDBRSection.SOA);

            MzItuAtVcmData vcmData = new MzItuAtVcmData(vcm_V, iSOA_mA);

            // store imbalance biases
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.IImbQuadL_mA, iImbLeft_mA);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.IImbQuadR_mA, iImbRight_mA);

            // diff voltage sweep - assume VCM is negative. Apply calculated IQuad to the 
            // imbalance electrodes
            engine.SendToGui("Running Differential Modulator sweep on channel " + 
                chanNbr.ToString() + " from 0V to " + vcm_V.ToString() + "V");
            bool dataOk = true;
            double minLevel_dBm = -60;
            string sweepFileName;
            ILMZSweepResult diffVsweep = null;
            do
            {
                diffVsweep = ilmzUtils.ModArm_DifferentialSweepByTrigger
                    (vcmData.GetValueDouble(MzItuAtVcmMeasurements.IImbQuadL_mA) / 1000,
                     vcmData.GetValueDouble(MzItuAtVcmMeasurements.IImbQuadR_mA) / 1000,
                     2 * vcm_V, 0.0, NumberPoints, MZSourceMeasureDelay_ms, tapInline_V, MZTapBias_V, ArmSourceByAsic);
                sweepFileName = "";
                string fileNamePrefix = string.Format("CH{0}_QualTest_DILV", chanNbr);
                recordSweepData(diffVsweep, fileNamePrefix, out sweepFileName);

                // If overrange run again
                if (MzAnalysisWrapper.CheckForOverrange(diffVsweep.SweepData[ILMZSweepDataType.FibrePower_mW]))
                {
                    dataOk = false;
                    if (double.IsNaN(ilmzUtils.MzInstrs.PowerMeter.Range)) // auto range?
                        ilmzUtils.MzInstrs.PowerMeter.Range = 1;
                    else
                        ilmzUtils.MzInstrs.PowerMeter.Range = ilmzUtils.MzInstrs.PowerMeter.Range * 10;
                }
                else
                {
                    // if underrange fix the data and continue
                    dataOk = true;
                    if (MzAnalysisWrapper.CheckForUnderrange(diffVsweep.SweepData[ILMZSweepDataType.FibrePower_mW]))
                    {
                        diffVsweep.SweepData[ILMZSweepDataType.FibrePower_mW] =
                            MzAnalysisWrapper.FixUnderRangeData(diffVsweep.SweepData[ILMZSweepDataType.FibrePower_mW]
                            , minLevel_dBm);
                    }
                }
            } while (!dataOk);

            // Display data
            engine.SendToGui(diffVsweep);
            //// write data to file
            //string diffVsweepFileNameStem = string.Format("MzDiffLV_CH{0}", chanNbr);
            //vcmData.DiffLVSweepFile = Util_GenerateFileName.GenWithTimestamp
            //    (MzFileDirectory, diffVsweepFileNameStem, dutSerialNbr, "csv");
            //MzSweepFileWriter.WriteSweepData(vcmData.DiffLVSweepFile, diffVsweep,
            //    new ILMZSweepDataType[] 
            //    {ILMZSweepDataType.LeftArmModBias_V,
            //     ILMZSweepDataType.RightArmModBias_V,
            //     ILMZSweepDataType.FibrePower_mW,
            //     ILMZSweepDataType.TapComplementary_mA});
            checkData.ChanMeasData.DiffLV_ModFile = sweepFileName;
            // analyse the data.
            //
            // The first point needs to be close to zero. After that 
            // search for quadrature close to initialVcmOffset 
            // to help ensure that we pick the same slope each time.

            #region Analyse the data per ChirpType - chongjian.liang 2014.8.7

            MzAnalysisWrapper.MzAnalysisResults mzAnlyDiffV = null;

            if (this.ChirpType == "ND")
            {
                mzAnlyDiffV = MzAnalysisWrapper.Differential_NegChirp_DifferentialSweep(diffVsweep, vcm_V, initialVcmOffset);
            }
            else if (this.ChirpType == "ZD")
            {
                mzAnlyDiffV = MzAnalysisWrapper.ZeroChirp_DifferentialSweep(diffVsweep, vcm_V, initialVcmOffset);
            } 
            #endregion

            #endregion

            #region Feature point measurement 
            
            MzData newMzBiases = new MzData();
            newMzBiases.RightArmImb_mA =iImbRight_mA ;
            newMzBiases.LeftArmImb_mA = iImbLeft_mA ;
            checkData.ChanMeasData.MzLeftArmImb_mA = iImbLeft_mA;
            checkData.ChanMeasData.MzRightArmImb_mA = iImbRight_mA;

            newMzBiases.LeftArmMod_Min_V = mzAnlyDiffV.Min_SrcL;
            newMzBiases.RightArmMod_Min_V = mzAnlyDiffV.Min_SrcR;
            checkData.ChanMeasData.MzLeftArmModMinima_V = mzAnlyDiffV.Min_SrcL;
            checkData.ChanMeasData.MzRightArmModMinima_V = mzAnlyDiffV.Min_SrcR;

            newMzBiases.LeftArmMod_Quad_V = mzAnlyDiffV.Quad_SrcL;
            newMzBiases.RightArmMod_Quad_V = mzAnlyDiffV.Quad_SrcR;
            checkData.ChanMeasData.MzLeftArmModQuad_V = mzAnlyDiffV.Quad_SrcL;
            checkData.ChanMeasData.MzRightArmModQuad_V = mzAnlyDiffV.Quad_SrcR;

            newMzBiases.LeftArmMod_Peak_V = mzAnlyDiffV.Max_SrcL;            
            newMzBiases.RightArmMod_Peak_V = mzAnlyDiffV .Max_SrcR;
            checkData.ChanMeasData.MzLeftArmModPeak_V = mzAnlyDiffV.Max_SrcL;
            checkData.ChanMeasData.MzRightArmModPeak_V = mzAnlyDiffV.Max_SrcR;

            checkData.ChanMeasData.MzDcVpi_V = mzAnlyDiffV.Vpi;
            checkData.ChanMeasData.MzVcmCal_V = vcm_V;

            // set opotical power meter to auto range to ensure the measurement accuracy
            mzInstrs.PowerMeter.Range = InstType_OpticalPowerMeter.AutoRange;
            IInstType_DigitalIO outLine_Ctap_Reference = this.Optical_switch.GetDigiIoLine(IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine);//3
            outLine_Ctap_Reference.LineState = IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine_State;//true
            // set mz to Peak and get the peak fibre power
            IlMzDriverUtils.SetupMzToPeak(mzInstrs.FCU2Asic, newMzBiases);
            System.Threading.Thread.Sleep(100);
            double fibrePower_peak_dBm = Measurements.ReadOpticalPower(
                OpticalPowerHead.MZHead, Measurements.PowerUnits.dBm);
            checkData.ChanMeasData.FibrePwrPeak_dBm = fibrePower_peak_dBm;
            checkData.ChanMeasData.TapCompPhotoCurrentPeak_mA = mzInstrs.FCU2Asic.ICtap_mA;

            // set mz to through fibre power
            IlMzDriverUtils.SetupMzToTrough(mzInstrs.FCU2Asic, newMzBiases);
            System.Threading.Thread.Sleep(100);
            double fibrePower_trough_dBm = Measurements.ReadOpticalPower
                    (OpticalPowerHead.MZHead, Measurements.PowerUnits.dBm);
            checkData.ChanMeasData.TapCompPhotoCurrentThrough_mA = mzInstrs.FCU2Asic.ICtap_mA;
            checkData.ChanMeasData.FibrePwrTrough_dBm = fibrePower_trough_dBm;

            checkData.ChanMeasData.MzDcEr_dB = fibrePower_peak_dBm - fibrePower_trough_dBm;

            IlMzDriverUtils.SetupMzToQuad(mzInstrs.FCU2Asic, newMzBiases);
            System.Threading.Thread.Sleep(100);
            double fibrepower_Quad_dBm = Measurements.ReadOpticalPower(
                OpticalPowerHead.MZHead, Measurements.PowerUnits.dBm);
            checkData.ChanMeasData.FibrePwrQuad_dBm = fibrepower_Quad_dBm;
            checkData.ChanMeasData.TapCompPhotoCurrentQuad_mA =mzInstrs.FCU2Asic.ICtap_mA;

            if (configData .ReadBool ("DoTapLevelling"))
            {
                double tapPhotocurrentReferenceQuad_A= checkData .ChanRefData .RefChanData .TapPhotocurrentQuad_A;
                double tapPhotoCurrentToleranceRatio= configData.ReadDouble("TapPhotoCurrentToleranceRatio");

                // tune SOA power / power level to tap
                engine.SendToGui( "Adjusting SOA power using complementary tap");
                SoaPowerLevel.TapLevelRes tapLevelResults = SoaPowerLevel.TapLevelWithAdaptiveSlope(
                    mzInstrs.FCU2Asic,
                    tapPhotocurrentReferenceQuad_A,
                    tapPhotoCurrentToleranceRatio);
                checkData .ChanMeasData .TapLevellingAtQuadOk = tapLevelResults.LevelOk;
                checkData .ChanMeasData.ISoaTapLevel_Quad_mA = tapLevelResults.SoaCurrent_mA;
                            
                double power_dBm = Measurements.ReadOpticalPower(OpticalPowerHead.MZHead, Measurements.PowerUnits.dBm);
                checkData.ChanMeasData.FibrePwrQuad_CL_dBm = power_dBm;
                checkData .ChanMeasData .PwrAtQuadChange_CL_dB = power_dBm - 
                    checkData .ChanRefData .RefChanData .FibrePwrQuad_dBm ;
                checkData.ChanMeasData.TapCompPhotoCurrentQuad_CL_mA = mzInstrs.FCU2Asic.ICtap_mA;

                // MZ peak closed loop measurements (tap power levelled)
                engine.SendToGui("MZ peak measurements");
                IlMzDriverUtils.SetupMzToPeak(mzInstrs.FCU2Asic, newMzBiases);
                System .Threading.Thread.Sleep(100);

                power_dBm = Measurements.ReadOpticalPower(OpticalPowerHead.MZHead, Measurements.PowerUnits.dBm);
                checkData.ChanMeasData.FibrePwrPeak_CL_dBm = power_dBm;
                checkData.ChanMeasData.PwrAtPeakChange_CL_dB = power_dBm -
                    checkData.ChanRefData.RefChanData.FibrePwrPeak_dBm;
                checkData.ChanMeasData.TapCompPhotoCurrentPeak_CL_mA = mzInstrs.FCU2Asic.ICtap_mA;
            }
            // If nessesary, calculate the feature points' power delta
            if (configData .ReadBool ("CalMzChrPowerDelta"))
            {
                double refPower;

                refPower = checkData.ChanRefData.RefChanData.FibrePwrPeak_dBm;
                checkData.ChanMeasData.PwrAtPeakChange_OL_dB = fibrePower_peak_dBm - refPower;

                refPower = checkData.ChanRefData.RefChanData.FibrePwrQuad_dBm;
                checkData.ChanMeasData.PwrAtQuadChange_OL_dB = fibrepower_Quad_dBm - refPower;

                refPower = checkData.ChanRefData.RefChanData.FibrePwrTrough_dBm;
                checkData.ChanMeasData.PwrAtTroughChange_OL_dB = fibrePower_trough_dBm - refPower;
            }
             
            #endregion
            return checkData;
        }

        /// <summary>
        /// Do Se LV sweep on both Mod arm and do analysis and store the result to measData
        /// </summary>
        /// <param name="engine">engine reference</param>
        /// <param name="chanIndx"> use to identify the sweep data file name </param>
        // <param name="measData"> store all Se analysis result in its 'ChanMeasData'</param>
        /// <returns> </returns>
        private DatumList measureSingleEndedResponse(ITestEngine engine,
                                        int chanIndx /*, IlmzChannelQualCheckMeas measData*/)
        {
            bool dataOk = true;
            double minLevel_dBm = -60;
            DatumList seSweepRst = new DatumList();
            // Alice.Huang    2010-03-10
            // add to ensure rf sourmeter work in Vssouce mode

            //mzDriverUtils.SetVoltage(mzDriverUtils.MzInstrs.LeftArmMod, -this.mzInitSweepMaxAbs_V);
            //mzDriverUtils .SetVoltage (mzDriverUtils .MzInstrs .RightArmMod , -this.mzInitSweepMaxAbs_V);

            // do the left-arm modulation sweep

            engine.SendToGui("Running Left Modulator sweep");
            ILMZSweepResult sweepDataLeft;
            do
            {
                sweepDataLeft = ilmzUtils.LeftModArm_SingleEndedSweep
                 (this.mzInitFixedModBias_volt,0,0,
                 -this.mzInitSweepMaxAbs_V,
                 0.0,
                 this.NumberPoints,
                 tapInline_V,
                 MZTapBias_V,
                 ArmSourceByAsic);

                // If overrange run again
                if (MzAnalysisWrapper.CheckForOverrange(sweepDataLeft.SweepData[ILMZSweepDataType.FibrePower_mW]))
                {
                    dataOk = false;
                    if (double.IsNaN(ilmzUtils.MzInstrs.PowerMeter.Range)) // auto range?
                        ilmzUtils.MzInstrs.PowerMeter.Range = 1;
                    else
                        ilmzUtils.MzInstrs.PowerMeter.Range = ilmzUtils.MzInstrs.PowerMeter.Range * 10;
                }
                else
                {
                    // if underrange fix the data and continue
                    dataOk = true;
                    if (MzAnalysisWrapper.CheckForUnderrange(sweepDataLeft.SweepData[ILMZSweepDataType.FibrePower_mW]))
                    {
                        sweepDataLeft.SweepData[ILMZSweepDataType.FibrePower_mW] =
                            MzAnalysisWrapper.FixUnderRangeData(sweepDataLeft.SweepData[ILMZSweepDataType.FibrePower_mW]
                            , minLevel_dBm);
                    }
                }
            }
            while (!dataOk);
            engine.SendToGui(sweepDataLeft);
            string sweepfileName;
            string fileNamePrefix = string .Format ("CH{0}_QualSeLv_LeftMod_",chanIndx );
            recordSweepData(sweepDataLeft, fileNamePrefix, out sweepfileName);
            //measData.ChanMeasData.SeLV_LeftModSweepFile = sweepfileName;
            seSweepRst.AddString("LeftModSweepFile", sweepfileName);

            engine.SendToGui("Running Right Modulator sweep");

            ILMZSweepResult sweepDataRight;
            do
            {
                sweepDataRight = ilmzUtils.RightModArm_SingleEndedSweep
                    (this.mzInitFixedModBias_volt,0,0,
                    -this.mzInitSweepMaxAbs_V,
                    0.0,
                    this.NumberPoints,
                    tapInline_V,
                    MZTapBias_V,
                    ArmSourceByAsic);

                // If overrange run again
                if (MzAnalysisWrapper.CheckForOverrange(sweepDataRight.SweepData[ILMZSweepDataType.FibrePower_mW]))
                {
                    dataOk = false;
                    if (double.IsNaN(ilmzUtils.MzInstrs.PowerMeter.Range)) // auto range?
                        ilmzUtils.MzInstrs.PowerMeter.Range = 1;
                    else
                        ilmzUtils.MzInstrs.PowerMeter.Range = ilmzUtils.MzInstrs.PowerMeter.Range * 10;
                }
                else
                {
                    // if underrange fix the data and continue
                    dataOk = true;
                    if (MzAnalysisWrapper.CheckForUnderrange(sweepDataRight.SweepData[ILMZSweepDataType.FibrePower_mW]))
                    {
                        sweepDataRight.SweepData[ILMZSweepDataType.FibrePower_mW] =
                            MzAnalysisWrapper.FixUnderRangeData(sweepDataRight.SweepData[ILMZSweepDataType.FibrePower_mW]
                            , minLevel_dBm);
                    }
                }
            }
            while (!dataOk);
            engine.SendToGui(sweepDataRight);
            fileNamePrefix = string .Format ("CH{0}_QualSeLv_RightMod_",chanIndx );
            recordSweepData(sweepDataRight, fileNamePrefix, out sweepfileName);
            //measData.ChanMeasData.SeLV_RightModSweepFile = sweepfileName;
            seSweepRst.AddString("RightModSweepFile", sweepfileName);

            engine.SendToGui("Completed RightArm sweep");

            #region display joined plot data
             // Make some data to display
            double[] reversedRightArmPower = Alg_ArrayFunctions.ReverseArray(sweepDataRight.SweepData[ILMZSweepDataType.FibrePower_mW]);
            double[] reversedRightArmVoltage = Alg_ArrayFunctions.ReverseArray(sweepDataRight.SweepData[ILMZSweepDataType.RightArmModBias_V]);
            double[] positiveReversedRightArmVoltage = Alg_ArrayFunctions.MultiplyEachArrayElement(reversedRightArmVoltage, -1);
            double[] joinedModBias = Alg_ArrayFunctions.JoinArrays(sweepDataLeft.SweepData[ILMZSweepDataType.LeftArmModBias_V], positiveReversedRightArmVoltage);
            double[] joinedPower = Alg_ArrayFunctions.JoinArrays(sweepDataLeft.SweepData[ILMZSweepDataType.FibrePower_mW], reversedRightArmPower);

            ILMZSweepResult dataToDisplay = new ILMZSweepResult();
            dataToDisplay.Type = SweepType.SingleEndVoltage;
            dataToDisplay.SrcMeter = SourceMeter.LeftModArm;
            dataToDisplay.SweepData.Add(ILMZSweepDataType.LeftArmModBias_V, joinedModBias);
            dataToDisplay.SweepData.Add(ILMZSweepDataType.FibrePower_mW, joinedPower);
            engine.SendToGui(dataToDisplay);
            engine.SendToGui("Completed both single LV sweep");

            #endregion

            

            Alg_MZAnalysis.MZAnalysis mzSeAnly = Alg_MZAnalysis.ZeroChirpAnalysis(
                sweepDataLeft.SweepData[ILMZSweepDataType.LeftArmModBias_V],
                sweepDataRight.SweepData[ILMZSweepDataType.RightArmModBias_V],
                sweepDataLeft.SweepData[ILMZSweepDataType.FibrePower_mW],
                sweepDataRight.SweepData[ILMZSweepDataType.FibrePower_mW],
                false, false, 0); // Mark - was -2 not 0 ????

            seSweepRst.AddReference("SweepAnalysisRst", mzSeAnly);
            return seSweepRst;
            //int maxPeak = Alg_FindFeature.FindIndexForMaxPeak(sweepDataLeft.SweepData[ILMZSweepDataType.FibrePower_mW]);
            //int minPeak = Alg_FindFeature.FindIndexForMinValley(sweepDataLeft.SweepData[ILMZSweepDataType.FibrePower_mW]);
            
            //measData.ChanMeasData.MzDcModVMin_V = mzSeAnly.VoltageAtMin;
            //measData.ChanMeasData.MzDcModVmax_V = mzSeAnly.VoltageAtMax;
            //measData.ChanMeasData.MzDcVpi_V = mzSeAnly.Vpi;
            //measData.ChanMeasData.MzDcEr_dB = maxPeak - minPeak;
            //measData.ChanMeasData.FibrePwrPeak_dBm = maxPeak;
            
        }

        /// <summary>
        /// write arm sweep data to file 
        /// </summary>
        /// <param name="sweepData"> sweep data</param>
        /// <param name="file_prefix"> file name prefix </param>
        /// <param name="sweepFileName"> file name that the sweep data save as </param>
        private void recordSweepData(ILMZSweepResult sweepData,  string file_prefix,
            out string sweepFileName)
        {
            Dictionary<ILMZSweepDataType, double[]> sweepDataListTemp = sweepData.SweepData;
            List<string> dataNameList = new List<string>();
            List<double[]> sweepDataList = new List<double[]>();
            foreach (ILMZSweepDataType dataName in sweepDataListTemp.Keys)
            {
                dataNameList.Add(dataName.ToString());
                sweepDataList.Add(sweepDataListTemp[dataName]);
            }

            double[][] sweepDataArray = sweepDataList.ToArray();
            string[] dataNameArray = dataNameList.ToArray();

            if ((sweepDataArray.Length > 0) && (dataNameArray[0].Length > 0))
            { }
            else
            {
                sweepFileName = "";
                return;
            }

            string mzSweepDataResultsFile = Util_GenerateFileName.GenWithTimestamp
                (MzFileDirectory, file_prefix, dutSerialNbr, "csv");

            using (StreamWriter writer = new StreamWriter(mzSweepDataResultsFile))
            {
                // file header
                StringBuilder fileHeader = new StringBuilder();

                for (int count = 0; count < dataNameArray.Length; count++)
                {
                    if (count != 0) fileHeader.Append(",");
                    fileHeader.Append(dataNameArray[count]);
                }

                // file contents
                List<string> fileContents = new List<string>();
                for (int irow = 0; irow < sweepDataArray[0].Length; irow++)
                {
                    StringBuilder aLine = new StringBuilder();
                    for (int icol = 0; icol < dataNameArray.Length; icol++)
                    {
                        if (icol != 0) aLine.Append(",");
                        aLine.Append(sweepDataArray[icol][irow].ToString());
                    }
                    fileContents.Add(aLine.ToString());
                }

                // write file header to file
                writer.WriteLine(fileHeader.ToString());

                // writer file contents to file
                foreach (string row in fileContents)
                {
                    writer.WriteLine(row);
                }
            }
            sweepFileName = mzSweepDataResultsFile;
        }
        /// <summary>
        /// Process the MZ Sweep results, writing out a zip of the sweep results, a summary CSV file and
        /// returning a flag to say if all sweep results passed or not
        /// </summary>
        /// <param name="chanDataList">all chans check data to be processed</param>
        // <param name="mzSweepDataResultsFile">Data results file</param>
        // <param name="mzSweepDataZipFile">Zip sweep file</param>
        // <param name="sweepsAllPassed">True if all sweep results passed, false if any failed</param>
        private void processMzSweepData(List<IlmzQualChanData> chanDataList )
        {
           // DatumList rtnList = new DatumList();
            string mzSweepDataZipFile = Util_GenerateFileName.GenWithTimestamp(MzFileDirectory, 
                                    "MzSweepDataZipFile", dutSerialNbr,"zip");

            using (ZipOutputStream zipStream = new ZipOutputStream(File.Create(mzSweepDataZipFile)))
            {
                zipStream.SetLevel(9); // 0 - store only to 9 - means best compression

                byte[] buffer = new byte[4096]; // byte buffer to zip into
                foreach (IlmzQualChanData chanMeas in chanDataList)
                {   
                    //if (chanMeas.ChanMeasData .SeLV_LeftModSweepFile.Trim() != "")
                    //    addZipFile(zipStream, chanMeas.ChanMeasData .SeLV_LeftModSweepFile , buffer);
                    //if (chanMeas.ChanMeasData .SeLV_RightModSweepFile.Trim() != "")
                    //    addZipFile(zipStream, chanMeas.ChanMeasData .SeLV_RightModSweepFile , buffer);
                    if (chanMeas .ChanMeasData .DiffLI_ImbsFile.Trim() != "")
                        addZipFile(zipStream, chanMeas.ChanMeasData.DiffLI_ImbsFile, buffer);
                    if (chanMeas.ChanMeasData.DiffLV_ModFile.Trim() != "")
                        addZipFile(zipStream, chanMeas.ChanMeasData.DiffLV_ModFile, buffer);
                }

                // Since some files were generated but not zipped, so zip these files to this zip file - chongjian.liang 2013.5.21
                foreach (string fileToZip in filesToZip)
                {
                    addZipFile(zipStream, fileToZip, buffer);
                }
            }

            returnData.AddFileLink("SweepDataZipFile", mzSweepDataZipFile);

            bool chansAllPassed = false ;
            IlmzQualChanData[] allChansData = chanDataList.ToArray();
            IlmzQualChanData[] rcdChansData;
            // allChansData's length should be => 3, and the data is sort in acscending by chanIndex
            rcdChansData = new IlmzQualChanData [] {allChansData[0], 
                allChansData[(int) (allChansData .Length -1)/2], allChansData[ allChansData .Length -1]};
            string[] strSuffix = new string[] { "_Low", "_Mid", "_High"};

            for (int indx = 0; indx < 3; indx++)
            {
                returnData.AddSint32("ChannelIndex" + strSuffix[indx], 
                    rcdChansData[indx].ChanRefData.DsdbrData.ItuChannelIndex);
                returnData.AddDouble("Frequency_GHz" + strSuffix[indx], 
                    rcdChansData[indx].ChanMeasData.Freq_GHz);
                returnData.AddDouble("FreqLock_GHz" + strSuffix[indx],
                    rcdChansData[indx].ChanMeasData.FreqLock_GHz);
                returnData.AddDouble("FreqLockedChange_GHz" + strSuffix[indx],
                    rcdChansData[indx].ChanMeasData.FreqLockedChange_GHz);
                returnData.AddDouble("RthGB_ohm" + strSuffix[indx],
                    rcdChansData[indx].ChanMeasData.RthGB_ohm);
                returnData.AddDouble("ITecGB_mA" + strSuffix[indx], 
                    rcdChansData[indx].ChanMeasData.ITecGB_mA);
                returnData.AddDouble("VTecGB_V" + strSuffix[indx], 
                    rcdChansData[indx].ChanMeasData.VTecGB_V);
                //rtnList.AddDouble("MzDcModVMin_V" + strSuffix[indx], 
                //    rcdChansData[indx].ChanMeasData.MzDcModVMin_V);
                //rtnList.AddDouble("MzDcModVmax_V" +strSuffix[indx], 
                //    rcdChansData[indx].ChanMeasData.MzDcModVmax_V);
                returnData.AddDouble("MzLeftArmModMinima_V" + strSuffix[indx],
                    rcdChansData[indx].ChanMeasData.MzLeftArmModMinima_V);
                returnData.AddDouble("MzLeftArmModQuad_V" + strSuffix[indx],
                    rcdChansData[indx].ChanMeasData.MzLeftArmModQuad_V);
                returnData.AddDouble("MzLeftArmModPeak_V" + strSuffix[indx],
                    rcdChansData[indx].ChanMeasData.MzLeftArmModPeak_V);
                returnData.AddDouble("MzRightArmModMinima_V" + strSuffix[indx],
                    rcdChansData[indx].ChanMeasData.MzRightArmModMinima_V);
                returnData.AddDouble("MzRightArmModQuad_V" + strSuffix[indx],
                    rcdChansData[indx].ChanMeasData.MzRightArmModQuad_V);
                returnData.AddDouble("MzRightArmModPeak_V" + strSuffix[indx],
                    rcdChansData[indx].ChanMeasData.MzRightArmModPeak_V);

                returnData.AddDouble("FibrePwrPeak_dBm" + strSuffix[indx],
                    rcdChansData[indx].ChanMeasData.FibrePwrPeak_dBm) ;
                returnData.AddDouble("FibrePwrPeak_CL_dBm" + strSuffix[indx],
                    rcdChansData[indx].ChanMeasData.FibrePwrPeak_CL_dBm);
                returnData.AddDouble("PwrAtPeakChange_OL_dB" + strSuffix[indx],
                    rcdChansData[indx].ChanMeasData.PwrAtPeakChange_OL_dB);
                returnData.AddDouble("FibrePwrQuad_dBm" + strSuffix[indx],
                    rcdChansData[indx].ChanMeasData.FibrePwrQuad_dBm);
                returnData.AddDouble("FibrePwrQuad_CL_dBm" + strSuffix[indx],
                    rcdChansData[indx].ChanMeasData.FibrePwrQuad_CL_dBm);
                returnData.AddDouble("PwrAtQuadChange_OL_dB" + strSuffix[indx],
                    rcdChansData[indx].ChanMeasData.PwrAtQuadChange_OL_dB);
                returnData.AddDouble("FibrePwrTrough_dBm" + strSuffix[indx],
                    rcdChansData[indx].ChanMeasData.FibrePwrTrough_dBm);
                returnData.AddDouble("PwrAtTroughChange_OL_dB" + strSuffix[indx],
                    rcdChansData[indx].ChanMeasData.PwrAtTroughChange_OL_dB);
                returnData.AddDouble("MzVcmCal_V" + strSuffix[indx],
                    rcdChansData[indx].ChanMeasData.MzVcmCal_V);
                returnData.AddDouble("MzDcVpi_V" + strSuffix[indx], 
                    rcdChansData[indx].ChanMeasData.MzDcVpi_V);
                returnData.AddDouble("MzDcEr_dB" + strSuffix[indx], 
                    rcdChansData[indx].ChanMeasData.MzDcEr_dB);
                returnData.AddDouble("MzLeftArmImb_mA" + strSuffix[indx],
                    rcdChansData[indx].ChanMeasData.MzLeftArmImb_mA);
                returnData.AddDouble("MzRightArmImb_mA" + strSuffix[indx], 
                    rcdChansData[indx].ChanMeasData.MzRightArmImb_mA);
                returnData.AddDouble("Vcc_Total_I_mA" + strSuffix[indx],
                    rcdChansData[indx].ChanMeasData.VccTotalCurrent_mA);
                returnData.AddDouble("I_Tx_mA" + strSuffix[indx],
                    rcdChansData[indx].ChanMeasData.ItxLock_mA);
                returnData.AddDouble("I_Rx_mA" + strSuffix[indx],
                    rcdChansData[indx].ChanMeasData.IrxLock_mA);
                returnData.AddDouble("LockRatio" + strSuffix[indx],
                    rcdChansData[indx].ChanMeasData.LockRatio);
                returnData.AddDouble("I_Rx_CL_mA" + strSuffix[indx],
                    rcdChansData[indx].ChanMeasData.IrxLock_CL_mA);
                returnData.AddDouble("I_Tx_CL_mA" + strSuffix[indx],
                    rcdChansData[indx].ChanMeasData.ItxLock_CL_mA);
                returnData.AddDouble("LockRatio_CL" + strSuffix[indx],
                    rcdChansData[indx].ChanMeasData.LockRatio_CL);
                returnData.AddSint32("TapLevellingAtQuadOk" + strSuffix[indx],
                    (rcdChansData[indx].ChanMeasData.TapLevellingAtQuadOk)? 1:0);
                
                returnData.AddDouble("PwrAtQuadChange_CL_dB" + strSuffix[indx],
                    rcdChansData[indx].ChanMeasData.PwrAtQuadChange_CL_dB);
                returnData.AddDouble("PwrAtPeakChange_CL_dB" + strSuffix[indx],
                    rcdChansData[indx].ChanMeasData.PwrAtPeakChange_CL_dB);
                returnData.AddDouble("I_Ctap_Trough_mA" + strSuffix[indx],
                    rcdChansData[indx].ChanMeasData.TapCompPhotoCurrentThrough_mA);
                returnData.AddDouble("I_Ctap_Quad_mA" + strSuffix[indx],
                    rcdChansData[indx].ChanMeasData.TapCompPhotoCurrentQuad_mA);
                returnData.AddDouble("I_Ctap_Peak_mA" + strSuffix[indx],
                    rcdChansData[indx].ChanMeasData.TapCompPhotoCurrentPeak_mA);
                returnData.AddDouble("I_Ctap_Peak_CL_mA" + strSuffix[indx],
                    rcdChansData[indx].ChanMeasData.TapCompPhotoCurrentPeak_CL_mA);
                returnData.AddDouble("I_Ctap_Quad_CL_mA" + strSuffix[indx],
                    rcdChansData[indx].ChanMeasData.TapCompPhotoCurrentQuad_CL_mA);
                returnData.AddDouble("ISoaTapLevel_Quad_mA" + strSuffix[indx],
                    rcdChansData[indx].ChanMeasData.ISoaTapLevel_Quad_mA);

                returnData.AddDouble("PhaseRatioSlopeEff" + strSuffix[indx],
                    rcdChansData[indx].ChanRefData.RefChanData.PhaseRatioSlopeEff);
            }

            chansAllPassed = true;
            
            //return rtnList;

        }
        /// <summary>
        /// Add a file to our zip file
        /// </summary>
        /// <param name="zipStream">Zip Output Stream</param>
        /// <param name="file">file name to add (relative path)</param>
        /// <param name="buffer">byte buffer to read data from the file to</param>
        private void addZipFile(ZipOutputStream zipStream, string file, byte[] buffer)
        {
            if (!File.Exists(file)) return;
            ZipEntry entry = new ZipEntry(Path.GetFileName(file));
            entry.DateTime = DateTime.Now;
            zipStream.PutNextEntry(entry);
            using (FileStream fs = File.OpenRead(file))
            {

                // Using a fixed size buffer here makes no noticeable difference for output
                // but keeps a lid on memory usage.
                int sourceBytes;
                do
                {
                    sourceBytes = fs.Read(buffer, 0, buffer.Length);
                    zipStream.Write(buffer, 0, sourceBytes);
                } while (sourceBytes > 0);
            }
            zipStream.Flush();
        }
        /// <summary>
        /// 
        /// </summary>
        public Type UserControl
        {
            get { return (typeof(IlmzQualTest_NDGui)); }
        }
        private void ImbSweepProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            int value = (int)(e.ProgressPercentage);
            if (value > 100)
                value = 100;
            if (value < 0)
                value = 0;
            ImbSweepEngine.SendToGui(value);
            
        }
        #endregion
    }
}

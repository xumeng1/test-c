// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// IlmzQualTest_ND.cs
//
// Author: alice.huang, 2010
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.ToolKit.Mz;
using Bookham.TestSolution.IlmzCommonData;
using Bookham.TestSolution.ILMZFinalData;
using Bookham.TestEngine.Framework.Limits;
using Bookham.TestSolution.IlmzCommonUtils;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Test Module Template.
    /// </summary>
    public class IlmzQualTest_ND : ITestModule
    {
        private IlMzDriverUtils mzDriverUtils;
        private double[] MZFixedModBias_volt;
        private double MzCtrlINominal_A;
        private double mzImbLeftIdeal_mA;
        private double mzImbRightIdeal_mA;
        private double mzImbLeftMinLimit_mA;
        private double mzImbLeftMaxLimit_mA;
        private double mzImbRightMinLimit_mA;
        private double mzImbRightMaxLimit_mA;
        private double mzInitFixedModBias_volt;
        private double mzInitSweepMaxAbs_V;
        private double mzVleftMinQuadLimit;
        private double mzVleftMaxQuadLimit;
        private double vcmMinLimit;
        private double vcmMaxLimit;
        private double vpiMinLimit;
        private double vpiMaxLimit;
        private double mzVleftInset = 0.05;
        private double initialVcmOffset = 0;
        private bool initialOffsetSelected;

        private int NumberPoints;
        private double MZTapBias_V;
        private double tapInline_V;
        private string MzFileDirectory;
        private string dutSerialNbr;
        #region ITestModule Members

        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, DatumList configData, InstrumentCollection instruments, ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {


            #region read config
            // get MZ instruments
            IlMzInstruments mzInstrs = (IlMzInstruments)configData.ReadReference("MzInstruments");
            // get list of DSDBR channels
            DsdbrChannelData[] ituChannels = (DsdbrChannelData[])previousTestData.ReadReference("ItuChannels");
            // get test spec (for limit checking)
            Specification testSpec = (Specification)configData.ReadReference("TestSpec");
            MzItuAtVcmData.InitLookup(testSpec);

            this.mzImbLeftIdeal_mA = Convert.ToDouble(testSpec.GetParamLimit("TC_MZ_IMB_LEFT").LowLimit.ValueToString());
            this.mzImbRightIdeal_mA = Convert.ToDouble(testSpec.GetParamLimit("TC_MZ_IMB_RIGHT").LowLimit.ValueToString());
            this.vcmMinLimit = Convert.ToDouble(testSpec.GetParamLimit("TC_MZ_VCM_CAL_LIMIT_MIN").LowLimit.ValueToString());
            this.vcmMaxLimit = Convert.ToDouble(testSpec.GetParamLimit("TC_MZ_VCM_CAL_LIMIT_MAX").HighLimit.ValueToString());
            this.vpiMinLimit = Convert.ToDouble(testSpec.GetParamLimit("TC_MZ_VPI_CAL_LIMIT_MIN").LowLimit.ValueToString());
            this.vpiMaxLimit = Convert.ToDouble(testSpec.GetParamLimit("TC_MZ_VPI_CAL_LIMIT_MAX").HighLimit.ValueToString());
            this.mzVleftMinQuadLimit = Convert.ToDouble(testSpec.GetParamLimit("CH_MZ_BIAS_L_QUAD_V").LowLimit.ValueToString());
            this.mzVleftMaxQuadLimit = Convert.ToDouble(testSpec.GetParamLimit("CH_MZ_BIAS_L_QUAD_V").HighLimit.ValueToString());
            this.mzImbLeftMinLimit_mA = Convert.ToDouble(testSpec.GetParamLimit("CH_MZ_CTRL_L_I").LowLimit.ValueToString());
            this.mzImbLeftMaxLimit_mA = Convert.ToDouble(testSpec.GetParamLimit("CH_MZ_CTRL_L_I").HighLimit.ValueToString());
            this.mzImbRightMinLimit_mA = Convert.ToDouble(testSpec.GetParamLimit("CH_MZ_CTRL_R_I").LowLimit.ValueToString());
            this.mzImbRightMaxLimit_mA = Convert.ToDouble(testSpec.GetParamLimit("CH_MZ_CTRL_R_I").HighLimit.ValueToString());


            this.mzInitFixedModBias_volt = configData.ReadDouble("MZInitFixedModBias_volt");
            this.mzInitSweepMaxAbs_V = configData.ReadDouble("MZInitSweepMaxAbs_V");

            // details to go into our filenames
            this.MzFileDirectory = configData.ReadString("MzFileDirectory");
            this.dutSerialNbr = configData.ReadString("DutSerialNbr");
            // MZ scan setup data
            this.MZFixedModBias_volt = configData.ReadDoubleArray("MZFixedModBias_volt");
            this.MzCtrlINominal_A = configData.ReadDouble("MzCtrlINominal_mA") / 1000;
            this.NumberPoints = configData.ReadSint32("NumberOfPoints");
            this.MZTapBias_V = configData.ReadDouble("MZTapBias_V");
            if (mzInstrs.InlineTapOnline)
            {
                this.tapInline_V = configData.ReadDouble("MZInlineTapBias_V");
            }
            else
            {
                // belt and braces - this should be the value anyway by C# default.
                this.tapInline_V = 0.0;
            }
            #endregion

            // Initialise GUI
            engine.GuiShow();
            engine.GuiToFront();

            #region setup for sweep
            // setup our private data 
            this.mzDriverUtils = new IlMzDriverUtils(mzInstrs);

            // Setup instruments for sweep
            TcmzMzSweep_Config mzConfig = new TcmzMzSweep_Config(configData);
            mzDriverUtils.CleanupAfterSweep();

            double mzMaxBias_V = mzConfig.MzMaxBias_V;
            IlMzDriverUtils.MaxModBias_V = mzConfig.MzMaxBias_V;
            double fixedModBias_V = mzConfig.MZInitFixedModBias_volt;
            double imbalance_A = mzConfig.MzCtrlINominal_mA / 1000;
            double sweepMaxAbs_V = mzConfig.MZInitSweepMaxAbs_V;
            sweepMaxAbs_V = Math.Abs(sweepMaxAbs_V);
            double sweepStop_V = 0.0;
            double sweepStart_V = -sweepMaxAbs_V;
            double stepSize_V = mzConfig.MZInitSweepStepSize_mV / 1000;
            int nbrPoints = (int)(1 + Math.Abs(sweepStart_V - sweepStop_V) / stepSize_V);
            double tapBias_V = mzConfig.MZTapBias_V;
            string fileDirectory = mzConfig.MzFileDirectory;

            double currentCompliance_A = mzConfig.MZCurrentCompliance_A;
            double currentRange_A = mzConfig.MZCurrentRange_A;
            double voltageCompliance_V = mzConfig.MZVoltageCompliance_V;
            double voltageRange_V = mzConfig.MZVoltageRange_V;
            int numberOfAverages = mzConfig.MzInitNumberOfAverages;
            double integrationRate = mzConfig.MZIntegrationRate;
            double sourceMeasureDelay = mzConfig.MZSourceMeasureDelay_s;
            double powerMeterAveragingTime = mzConfig.MZPowerMeterAveragingTime_s;

            engine.SendToGui("Preparing instruments for sweep");
            mzDriverUtils.InitialiseMZArms(currentCompliance_A, currentRange_A);

            mzDriverUtils.InitialiseImbalanceArms(voltageCompliance_V, voltageRange_V);
            mzDriverUtils.InitialiseTaps(currentCompliance_A, currentRange_A);
            mzDriverUtils.SetMeasurementAccuracy(numberOfAverages, integrationRate, sourceMeasureDelay, powerMeterAveragingTime);
            mzInstrs.PowerMeter.Mode = Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.MeterMode.Absolute_mW;
            #endregion

            // measure at first and last channels
            initialOffsetSelected = false;
            int[] chansToMeas = new int[] { 0, (int)(ituChannels .Length +1)/2,ituChannels.Length - 1 };
            List<MzSweepDataItuChannel> mzSweepData = new List<MzSweepDataItuChannel>();

            //START MODIFICATION, 2007.09.26, BY Ken.Wu ( TCMZ Vpi Correction )
            double tuneISOAPowerSlope = configData.ReadDouble("TuneISOAPowerSlope");
            double tuneOpcicalPowerTolerancemW
                = configData.ReadDouble("TuneOpticalPowerToleranceInmW");
            double TargetFibrePower_dBm = configData.ReadDouble("TargetFibrePower_dBm");

            bool isError = false;
            string errorInformation = "";
            foreach (int channelIndex in chansToMeas)
            {
                MzSweepDataItuChannel mzDataItu = measureMzAtFrequency(
                    engine, mzInstrs, ituChannels[channelIndex],
                    tuneISOAPowerSlope, tuneOpcicalPowerTolerancemW, TargetFibrePower_dBm,
                    configData, out isError, out errorInformation);
                mzSweepData.Add(mzDataItu);

                if (isError)
                {
                    break;
                }
            }
            //END MODIFICATION, 2007.09.26, BY Ken.Wu ( TCMZ Vpi Correction )

            // Clean up
            mzInstrs.PowerMeter.EnableLogging = false;

            // Alice.Huang     2010-02-26
            //commented all methjod related with   IInstType_TriggeredOpticalPowerMeter   with  InstType_OpticalPowerMeter 
            mzInstrs.PowerMeter.EnableInputTrigger(false);

            string mzSweepDataResultsFile;
            string mzSweepDataZipFile;
            bool allSweepsPassed;
            processMzSweepData(mzSweepData, out mzSweepDataResultsFile, out mzSweepDataZipFile,
                out allSweepsPassed);

            // return the data
            DatumList returnData = new DatumList();
            returnData.AddFileLink("MzSweepDataZipFile", mzSweepDataZipFile);
            returnData.AddFileLink("MzSweepDataResultsFile", mzSweepDataResultsFile);
            returnData.AddSint32("AllSweepsPassed", allSweepsPassed ? 1 : 0);
            returnData.AddReference("MzSweepData", mzSweepData);

            returnData.AddBool("IsError", isError);
            if (isError)
            {
                returnData.AddString("ErrorInformation", errorInformation);
            }
            return returnData;
        }

        private MzSweepDataItuChannel measureMzAtFrequency(ITestEngine engine, 
            IlMzInstruments mzInstrs, DsdbrChannelData dsdbrChannelData,
            double tuneISOAPowerSlope, double tuneOpcicalPowerTolerancemW,
            double TargetFibrePower_dBm, DatumList configData,
            out bool isError, out string errorInformation)
        {
            isError = false;
            errorInformation = "";

            // Set Vpi and Vcm limits in analysis wrapper 
            MzAnalysisWrapper.vpiMinLimit = this.vpiMinLimit;
            MzAnalysisWrapper.vpiMaxLimit = this.vpiMaxLimit;
            MzAnalysisWrapper.vcmMinLimit = this.vcmMinLimit;
            MzAnalysisWrapper.vcmMaxLimit = this.vcmMaxLimit;

            // setup the DSDBR
            DsdbrUtils.SetDsdbrCurrents_mA(dsdbrChannelData .Setup);
            //DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic.ISoa_mA =(float)( configData.ReadDouble("SoaSetupCurrent_mA")); ;//Jack.Zhang for Initial MZ Calibration
            int chanNbr = dsdbrChannelData.ItuChannelIndex;

            // Cache the WL in mzUtils so that we can calibrate power after the sweep.
            int numberOfAverages = configData.ReadSint32("MZInitNumberOfAverages");
            double integrationRate = configData.ReadDouble("MZIntegrationRate");
            double sourceMeasureDelay = configData.ReadDouble("MZSourceMeasureDelay_s");
            double powerMeterAveragingTime = configData.ReadDouble("MZPowerMeterAveragingTime_s");

            double wavelength = Measurements.ReadWavelength_nm();
            mzInstrs.PowerMeter.Wavelength_nm = wavelength;
            // And apply WL to power meter
            double power_mW = mzInstrs.PowerMeter.ReadPower();

            double iImbLeft_mA =0.0 ;
            double iImbRight_mA =0.0 ;

            mzDriverUtils.SetMeasurementAccuracy(numberOfAverages, integrationRate, sourceMeasureDelay, powerMeterAveragingTime);
            mzInstrs.PowerMeter.Mode = Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.MeterMode.Absolute_mW;

            // Measure imbalance V from single-ended sweeps
            Alg_MZAnalysis.MZAnalysis seResponse = measureSingleEndedResponse(engine);

        }

        /// <summary>
        /// Measure imbalance voltage from two single ended sweeps
        /// </summary>
        /// <param name="engine">engine reference</param>
        /// <returns>Vimb</returns>
        private Alg_MZAnalysis.MZAnalysis measureSingleEndedResponse(ITestEngine engine)
        {
            bool dataOk = true;
            double minLevel_dBm = -60;
            // Alice.Huang    2010-03-10
            // add to ensure rf sourmeter work in Vssouce mode

            //mzDriverUtils.SetVoltage(mzDriverUtils.MzInstrs.LeftArmMod, -this.mzInitSweepMaxAbs_V);
            //mzDriverUtils .SetVoltage (mzDriverUtils .MzInstrs .RightArmMod , -this.mzInitSweepMaxAbs_V);

            // do the left-arm modulation sweep

            engine.SendToGui("Running Left Modulator sweep");
            ILMZSweepData sweepDataLeft;
            do
            {
                sweepDataLeft = mzDriverUtils.LeftModArm_SingleEndedSweep
                 (this.mzInitFixedModBias_volt,
                 this.mzImbLeftIdeal_mA / 1000,
                 this.mzImbRightIdeal_mA / 1000,
                 -this.mzInitSweepMaxAbs_V,
                 0.0,
                 this.NumberPoints,
                 tapInline_V,
                 MZTapBias_V);

                // If overrange run again
                if (MzAnalysisWrapper.CheckForOverrange(sweepDataLeft.Sweeps[ILMZSweepDataType.FibrePower_mW]))
                {
                    dataOk = false;
                    if (double.IsNaN(mzDriverUtils.MzInstrs.PowerMeter.Range)) // auto range?
                        mzDriverUtils.MzInstrs.PowerMeter.Range = 1;
                    else
                        mzDriverUtils.MzInstrs.PowerMeter.Range = mzDriverUtils.MzInstrs.PowerMeter.Range * 10;
                }
                else
                {
                    // if underrange fix the data and continue
                    dataOk = true;
                    if (MzAnalysisWrapper.CheckForUnderrange(sweepDataLeft.Sweeps[ILMZSweepDataType.FibrePower_mW]))
                    {
                        sweepDataLeft.Sweeps[ILMZSweepDataType.FibrePower_mW] =
                            MzAnalysisWrapper.FixUnderRangeData(sweepDataLeft.Sweeps[ILMZSweepDataType.FibrePower_mW]
                            , minLevel_dBm);
                    }
                }
            }
            while (!dataOk);

            string sweepfileName;
            string fileNamePrefix = "MZCR_LeftMod_LV_";
            recordSweepData(sweepDataLeft, fileNamePrefix, out sweepfileName);


            engine.SendToGui(sweepDataLeft);
            //engine.SendToGui("Running Right Modulator sweep");

            //MZSweepData sweepDataRight;
            //do
            //{
            //    sweepDataRight = mzDriverUtils.RightModArm_SingleEndedSweep
            //        (this.mzInitFixedModBias_volt,
            //        this.mzImbLeftIdeal_mA / 1000,
            //        this.mzImbRightIdeal_mA / 1000,
            //        -this.mzInitSweepMaxAbs_V,
            //        0.0,
            //        this.NumberPoints,
            //        tapInline_V,
            //        MZTapBias_V);

            //    // If overrange run again
            //    if (MzAnalysisWrapper.CheckForOverrange(sweepDataRight.Sweeps[ILMZSweepDataType.FibrePower_mW]))
            //    {
            //        dataOk = false;
            //        if (double.IsNaN(mzDriverUtils.MzInstrs.PowerMeter.Range)) // auto range?
            //            mzDriverUtils.MzInstrs.PowerMeter.Range = 1;
            //        else
            //            mzDriverUtils.MzInstrs.PowerMeter.Range = mzDriverUtils.MzInstrs.PowerMeter.Range * 10;
            //    }
            //    else
            //    {
            //        // if underrange fix the data and continue
            //        dataOk = true;
            //        if (MzAnalysisWrapper.CheckForUnderrange(sweepDataRight.Sweeps[ILMZSweepDataType.FibrePower_mW]))
            //        {
            //            sweepDataRight.Sweeps[ILMZSweepDataType.FibrePower_mW] =
            //                MzAnalysisWrapper.FixUnderRangeData(sweepDataRight.Sweeps[ILMZSweepDataType.FibrePower_mW]
            //                , minLevel_dBm);
            //        }
            //    }
            //}
            //while (!dataOk);
            //fileNamePrefix = "MZCR_RightMod_LV_";
            //recordSweepData(sweepDataRight, fileNamePrefix,out sweepfileName);

            #region display joined plot data
            // Make some data to display
            //double[] reversedRightArmPower = Alg_ArrayFunctions.ReverseArray(sweepDataRight.Sweeps[ILMZSweepDataType.FibrePower_mW]);
            //double[] reversedRightArmVoltage = Alg_ArrayFunctions.ReverseArray(sweepDataRight.Sweeps[ILMZSweepDataType.RightArmModBias_V]);
            //double[] positiveReversedRightArmVoltage = Alg_ArrayFunctions.MultiplyEachArrayElement(reversedRightArmVoltage, -1);
            //double[] joinedModBias = Alg_ArrayFunctions.JoinArrays(sweepDataLeft.Sweeps[ILMZSweepDataType.LeftArmModBias_V], positiveReversedRightArmVoltage);
            //double[] joinedPower = Alg_ArrayFunctions.JoinArrays(sweepDataLeft.Sweeps[ILMZSweepDataType.FibrePower_mW], reversedRightArmPower);

            //MZSweepData dataToDisplay = new MZSweepData();
            //dataToDisplay.Type = SweepType.SingleEndVoltage;
            //dataToDisplay.SrcMeter = SourceMeter.LeftModArm;
            //dataToDisplay.Sweeps.Add(ILMZSweepDataType.LeftArmModBias_V, joinedModBias);
            //dataToDisplay.Sweeps.Add(ILMZSweepDataType.FibrePower_mW, joinedPower);
            //engine.SendToGui(dataToDisplay);
            #endregion

            //engine.SendToGui("Completed RightArm sweep");

            //Alg_MZAnalysis.MZAnalysis mzSeAnly = Alg_MZAnalysis.ZeroChirpAnalysis(
            //    sweepDataLeft.Sweeps[ILMZSweepDataType.LeftArmModBias_V],
            //    sweepDataRight.Sweeps[ILMZSweepDataType.RightArmModBias_V],
            //    sweepDataLeft.Sweeps[ILMZSweepDataType.FibrePower_mW],
            //    sweepDataRight.Sweeps[ILMZSweepDataType.FibrePower_mW],
            //    false, false, 0); // Mark - was -2 not 0 ????

            //Alg_MZAnalysis.MZAnalysis mzSeAnly = Alg_MZAnalysis.ZeroChirpAnalysis( 
            //    sweepDataLeft.Sweeps[ILMZSweepDataType.LeftArmModBias_V],
            //    sweepDataLeft.Sweeps[ILMZSweepDataType.FibrePower_mW],
            //    false, false, 0); // Mark - was -2 not 0 ????
            //int maxPeak = Alg_FindFeature.FindIndexForMaxPeak(sweepDataLeft.Sweeps[ILMZSweepDataType.FibrePower_mW]);
            //int minPeak = Alg_FindFeature.FindIndexForMinValley(sweepDataLeft.Sweeps[ILMZSweepDataType.FibrePower_mW]);
            //mzSeAnly.VoltageAtMax =  sweepDataLeft.Sweeps[ILMZSweepDataType.LeftArmModBias_V][maxPeak];  //jack.Zhang change by Mark suggestion
            //mzSeAnly.VoltageAtMin = sweepDataLeft.Sweeps[ILMZSweepDataType.LeftArmModBias_V][minPeak];
            //mzSeAnly.Vpi =Math.Abs (
            //    sweepDataLeft.Sweeps[ILMZSweepDataType.LeftArmModBias_V][minPeak] - 
            //    sweepDataLeft.Sweeps[ILMZSweepDataType.LeftArmModBias_V][maxPeak]);

            // Alice.huang    2010-05-05
            // use this new function to ensure to find the valley closest to offset
            Alg_MZAnalysis.MZAnalysis mzSeAnly = Alg_MZAnalysis.FindFeaturePointClosestToOffset(
                sweepDataLeft.Sweeps[ILMZSweepDataType.LeftArmModBias_V],
                sweepDataLeft.Sweeps[ILMZSweepDataType.FibrePower_mW],
                false, 0, Alg_MZAnalysis.MzFeaturePowerType.Min);
            return mzSeAnly;

        }
        public Type UserControl
        {
            get { return (typeof(IlmzQualTest_NDGui)); }
        }

        #endregion
    }
}

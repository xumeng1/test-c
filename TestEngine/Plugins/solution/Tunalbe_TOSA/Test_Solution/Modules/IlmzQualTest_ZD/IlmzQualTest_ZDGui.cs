// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// IlmzQualTest_ZDGui.cs
//
// Author: alice.huang, 2010
// ILMZ GB version : Mark Fullalove 2011
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Bookham.ToolKit.Mz;
using NPlot;
using Bookham.TestLibrary.Algorithms;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Module Template GUI User Control
    /// </summary>
    public partial class IlmzQualTest_ZDGui : Bookham.TestEngine.Framework.Messages.ManagedCtlBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public IlmzQualTest_ZDGui()
        {
            /* Call designer generated code. */
            InitializeComponent();

            /* TODO - Write constructor code here. */
        }

        /// <summary>
        /// Incoming message event handler
        /// </summary>
        /// <param name="payload">Message Payload</param>
        /// <param name="inMsgSeq">Incoming message sequence number</param>
        /// <param name="respSeq">Outgoing message sequence number</param>
        private void ModuleGui_MsgReceived(object payload, long inMsgSeq, long respSeq)
        {
            try
            {
                if (payload.GetType() == typeof(ILMZSweepData))
                {
                    ILMZSweepData sweepData = (ILMZSweepData)payload;
                    this.plotTitle.Text = sweepData.Type.ToString();
                    string labelText = "";

                    double[] xData = new double[0];
                    if (sweepData.Type == SweepType.DifferentialCurrent)
                    {
                        double[] leftData = sweepData.Sweeps[ILMZSweepDataType.LeftArmImb_A];
                        double[] rightData = sweepData.Sweeps[ILMZSweepDataType.RightArmImb_A];
                        xData = Alg_ArrayFunctions.SubtractArrays(leftData, rightData);
                        labelText = "Differential current (A)";
                    }
                    if (sweepData.Type == SweepType.DifferentialVoltage)
                    {
                        double[] leftData = sweepData.Sweeps[ILMZSweepDataType.LeftArmModBias_V];
                        double[] rightData = sweepData.Sweeps[ILMZSweepDataType.RightArmModBias_V];
                        xData = Alg_ArrayFunctions.SubtractArrays(leftData, rightData);
                        labelText = "Differential bias (V)";
                    }
                    if (sweepData.Type == SweepType.SingleEndVoltage)
                    {
                        if (sweepData.Sweeps.ContainsKey(ILMZSweepDataType.LeftArmModBias_V) &&
                                sweepData.Sweeps[ILMZSweepDataType.LeftArmModBias_V] != null)
                        {
                            xData = sweepData.Sweeps[ILMZSweepDataType.LeftArmModBias_V];
                        }
                        else if (sweepData.Sweeps.ContainsKey(ILMZSweepDataType.RightArmModBias_V) &&
                                sweepData.Sweeps[ILMZSweepDataType.RightArmModBias_V] != null)
                        {
                            xData = sweepData.Sweeps[ILMZSweepDataType.RightArmModBias_V];
                        }
                        labelText = "Bias (V)";
                    }
                    double[] yData = Alg_PowConvert_dB.Convert_mWtodBm(sweepData.Sweeps[ILMZSweepDataType.FibrePower_mW]);

                    mzPlot.Clear();
                    LinePlot plotData = new LinePlot(yData, xData);
                    plotData.Color = Color.DarkBlue;
                    plotData.Label = "Optical Power (mW)";
                    mzPlot.Add(plotData, PlotSurface2D.XAxisPosition.Bottom, PlotSurface2D.YAxisPosition.Right, 1);
                    mzPlot.XAxis1.Label = labelText;

                    if (mzPlot.YAxis2 != null)
                        mzPlot.YAxis2.Label = "Optical power (mW)";

                    this.mzPlot.Visible = true;
                    mzPlot.Refresh();

                }
                else if (payload.GetType() == typeof(String))
                {
                    this.titleLabel.Text = (string)payload;
                }

            }
            catch (SystemException)
            {
                this.mzPlot.Visible = false;
                // No GUI errors should be fatal
            }
        }
    }
    

}

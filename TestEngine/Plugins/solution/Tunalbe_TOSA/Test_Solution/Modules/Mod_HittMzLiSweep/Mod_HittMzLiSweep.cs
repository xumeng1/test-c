// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// Mod_HittMzLiSweep.cs
//
// Author: Echoxl.wang, 2011
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestSolution.Instruments;//Inst_Fcu
//using Bookham.TestLibrary.FcuCommonUtils;
using Bookham.ToolKit.Mz;
using System.IO;
using Bookham.TestLibrary.Algorithms;
using Bookham.TestLibrary.Instruments;//Inst_fcu2asic
using Bookham.TestLibrary.InstrTypes;//power meter
using Bookham.TestSolution.IlmzCommonData;
using Bookham.TestSolution.IlmzCommonUtils;
using Bookham.TestSolution.IlmzCommonInstrs;
using Bookham.TestLibrary.Utilities;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Test Module Template.
    /// </summary>
    public class Mod_HittMzLiSweep : ITestModule
    {
        #region ITestModule Members

        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, DatumList configData, InstrumentCollection instruments, ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {

            engine.GuiToFront();
            engine.GuiShow();

            #region initialize instrument and parameters

            //MapSettings = (TestParamConfigAccessor)configData.ReadReference("Mapping_Settings");

            bool isToCheckSNMixUp = false;

            if (configData.IsPresent("IsToCheckSNMixUp"))
            {
                isToCheckSNMixUp = configData.ReadBool("IsToCheckSNMixUp");
            }
            FailModeCheck failModeCheck = configData.ReadReference("FailModeCheck") as FailModeCheck;
            string work_dir = configData.ReadString("RESULT_DIR");
            string laser_id = configData.ReadString("LASER_ID");
            string timestamp = configData.ReadString("TIME_STAMP");
            float Igain_mA = (float)configData.ReadDouble("Igain_mA");
            float Irear_mA = (float)configData.ReadDouble("Irear_mA");
            float Isoa_mA = (float)configData.ReadDouble("Isoa_mA");
            float IRearSoa_mA = (float)configData.ReadDouble("IrearSoa_mA");
            float IfsCon_mA = (float)configData.ReadDouble("IfsCon_mA");
            float IfsNonCon_mA = (float)configData.ReadDouble("IfsNonCon_mA");
            int fp = configData.ReadSint32("FrontPair");
            float IPhase_mA = (float)configData.ReadDouble("Iphase_mA");
            double IMaxMzSweep_A =configData.ReadDouble("MaxCurrentMzSweep_mA")/1000.0;
            int MzSweepPointNumber = configData.ReadSint32("MzSweepNumber");//161
            IlMzInstruments mzInstrs = (IlMzInstruments)configData.ReadReference("MzInstruments");
            double tapInline_V = 0;
            double MZTapBias_V = -3.5;
            bool ArmSourceByAsic = true;
            mzInstrs.ImbsSourceType = IlMzInstruments.EnumSourceType.Asic;

            mzInstrs.FCU2Asic = (Inst_Fcu2Asic)instruments["Fcu2Asic"];
            if (mzInstrs.FCU2Asic.IsOnline == false)
            {
                mzInstrs.FCU2Asic.IsOnline = true;
            }
            mzInstrs.PowerMeter = (IInstType_TriggeredOpticalPowerMeter)instruments["OpmMz"];
            mzInstrs.PowerMeter.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_mW;
            mzInstrs.PowerMeter.Range = InstType_OpticalPowerMeter.AutoRange;
            #endregion

            #region power up laser
            DsdbrChannelData oneChannel = new DsdbrChannelData();
            oneChannel.Setup.IFsFirst_mA = IfsCon_mA;
            oneChannel.Setup.IFsSecond_mA = IfsNonCon_mA;
            oneChannel.Setup.IGain_mA = Igain_mA;
            oneChannel.Setup.IRear_mA = Irear_mA;
            oneChannel.Setup.ISoa_mA = Isoa_mA;
            oneChannel.Setup.IRearSoa_mA = IRearSoa_mA;
            oneChannel.Setup.FrontPair = fp;
            oneChannel.Setup.IPhase_mA = IPhase_mA;   //raul changed,the original is 0.001
            DsdbrUtils.SetDsdbrCurrents_mA(oneChannel.Setup);
            #endregion 

            double freq_mz_sweep = Measurements.ReadFrequency_GHz();

            if (isToCheckSNMixUp)
            {
                #region Get the freq that is closest to the freq under which the PRE stage scanned & found the IMB - chongjian.liang 2013.3.20

                double freqClosestToPREs_RearScan = configData.ReadDouble("FreqClosestToPREs_RearScan");

                double freq_PRE_GHz = configData.ReadDouble("FREQ_PRE_GHz");

                double freqDValue_Rear = Math.Abs(freqClosestToPREs_RearScan - freq_PRE_GHz);
                double freqDValue_Current = Math.Abs(freq_mz_sweep - freq_PRE_GHz);

                double freqDValue_Min = Math.Min(freqDValue_Rear, freqDValue_Current);

                // If this freq comparing to the PRE freq is under tolerance, sweep & found IMB and compare with PRE's
                if (freqDValue_Min < configData.ReadDouble("FreqToleranceForIMB_GHz"))
                {
                    // If the freq from RearCutScan is closer to freq from PRE, then sweep & found IMB under this freq
                    if (freqDValue_Min == freqDValue_Rear)
                    {
                        oneChannel.Setup = (DsdbrChannelSetup)configData.ReadReference("ISetup_FreqClosestToPREs_RearScan");

                        DsdbrUtils.SetDsdbrCurrents_mA(oneChannel.Setup);
                    }
                }
                else
                {
                    return new DatumList();
                }
                #endregion
            }

            #region do mz diff Li sweep (0~7 mA)
            double newLeftBias_V = 0;
            double newRightBias_V = 0;
            ILMZSweepResult diffIsweep;
            bool dataOk = true;
            double minLevel_dBm = -50;
            IlMzDriverUtils mzDriverUtils = new IlMzDriverUtils(mzInstrs);
            do
            {
                diffIsweep = mzDriverUtils.ImbDifferentialSweepWithAsicByTrigger
                     (newLeftBias_V, newRightBias_V,
                      0.0, 2 * IMaxMzSweep_A, MzSweepPointNumber, tapInline_V, MZTapBias_V, ArmSourceByAsic, false);// by tim to combine solution
                #region for Ag8163 need fix range when trigger
                if (MzAnalysisWrapper.CheckForOverrange(diffIsweep.SweepData[ILMZSweepDataType.FibrePower_mW]))
                {
                    dataOk = false;
                    //modify by purney at2012-08-06 for Ag8163 NaN;
                    mzInstrs.PowerMeter.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_mW;
                    mzInstrs.PowerMeter.Range = mzInstrs.PowerMeter.Range * 10;
                    if (double.IsNaN(mzInstrs.PowerMeter.Range))
                    {
                        mzInstrs.PowerMeter.Range = 10;
                    }
                    
                    //end
                    //mzInstrs.PowerMeter.Range = mzInstrs.PowerMeter.Range * 10;
                }
                else
                {
                    // if underrange fix the data and continue
                    dataOk = true;
                    if (MzAnalysisWrapper.CheckForUnderrange(diffIsweep.SweepData[ILMZSweepDataType.FibrePower_mW]))
                    {
                        diffIsweep.SweepData[ILMZSweepDataType.FibrePower_mW] =
                            MzAnalysisWrapper.FixUnderRangeData(diffIsweep.SweepData[ILMZSweepDataType.FibrePower_mW]
                            , minLevel_dBm);
                    }
                }
                #endregion for Ag8163 need fix range when trigger
            }
            while (!dataOk);
            #endregion

            string sweepFilePrefix = null;
            
            if (isToCheckSNMixUp)
            {
                sweepFilePrefix = string.Format("{0}_MzLiSweep_ForSNMixup_{1}.csv", laser_id, timestamp);
            }
            else
            {
                sweepFilePrefix = string.Format("{0}_MzLiSweep_{1}.csv", laser_id, timestamp);
            }

            string diffLIFileName = Path.Combine(work_dir, sweepFilePrefix);

            recordSweepData(diffIsweep, diffLIFileName);

            // Collect the MZ sweep file for pre burn test - chongjian.liang 2013.5.22
            if (configData.IsPresent("MZSweepFilesToZip"))
            {
                List<string> mzSweepFilesToZip = configData.ReadReference("MZSweepFilesToZip") as List<string>;

                mzSweepFilesToZip.Add(diffLIFileName);
            }
            
            #region Analyze sweep result

            // analyse the data
            bool iserror = false;
            MzAnalysisWrapper.MzAnalysisResults mzAnlyDiffI = null;
            try
            {
                mzAnlyDiffI =
                    MzAnalysisWrapper.DiffImbalance_NegChirp_FindFeaturePointByRef(
                    diffIsweep, IMaxMzSweep_A, 0,
                    Alg_MZAnalysis.MzFeaturePowerType.Min);
            }
            catch (Exception e)
            {
                engine.ShowContinueUserQuery("Error occurs when analysis Diff LI sweep Data.\n" + e.Message + "\n you can continue do next testing!");
                iserror = true;
            }

            //set imb current to peak point
            double iImbLeft_mA = 0;// find max point for li sweep
            double iImbRight_mA = 0;

            if (!iserror)
            {
                iImbLeft_mA = mzAnlyDiffI.Max_SrcL * 1000; // find max point for li sweep
                iImbRight_mA = mzAnlyDiffI.Max_SrcR * 1000;
            }

            mzInstrs.FCU2Asic.IimbLeft_mA = float.Parse(iImbLeft_mA.ToString());
            mzInstrs.FCU2Asic.IimbRight_mA = float.Parse(iImbRight_mA.ToString());
            #endregion

            DatumList returnData = new DatumList();

            if (isToCheckSNMixUp)
            {
                #region Record the IMB difference between POST & PRE stage - chongjian.liang 2013.3.20

                double IMBLeft_PRE_mA = configData.ReadDouble("IMBLeft_PRE_mA");
                double IMBRight_PRE_mA = configData.ReadDouble("IMBRight_PRE_mA");

                double IMBLeft_POST_mA = mzAnlyDiffI.Quad_SrcL * 1000;
                double IMBRight_POST_mA = mzAnlyDiffI.Quad_SrcR * 1000;

                double IMBLeft_PRE_Minus_POST_mA = IMBLeft_PRE_mA - IMBLeft_POST_mA;

                if (double.IsNaN(IMBLeft_PRE_Minus_POST_mA))
                {
                    failModeCheck.RaiseError_Mod_NonParamFail(engine, string.Format("IMBLeft_PRE_Minus_POST_mA which results from mzAnlyDiffI.Quad_SrcL({0}) is NaN.", mzAnlyDiffI.Quad_SrcL), FailModeCategory_Enum.PR);
                }

                /* The MZ was not affected in burn-in, so IMB under the same
                 * freq (or very close) should be very close between PRE & POST stages.*/

                returnData.AddDouble("IMBLeft_PRE_Minus_POST_mA", IMBLeft_PRE_Minus_POST_mA);
                returnData.AddFileLink("SN_MIXUP_CHECK_LI_SWEEP_FILE", diffLIFileName);
                #endregion
            }
            else
            {
                returnData.AddDouble("Freq_mz_sweep_Ghz", freq_mz_sweep);
                returnData.AddReference("MzAnlyResult", mzAnlyDiffI);
            }

            return returnData;
        }
        /// <summary>
        /// write arm sweep data to file 
        /// </summary>
        /// <param name="sweepResult"> sweep data</param>
        /// <param name="sweepFileName"> file name that the sweep data save as </param>
        private void recordSweepData(ILMZSweepResult sweepData,string sweepFileName)
                                
        {
            Dictionary<ILMZSweepDataType, double[]> sweepDataListTemp = sweepData.SweepData;
            List<string> dataNameList = new List<string>();
            List<double[]> sweepDataList = new List<double[]>();
            foreach (ILMZSweepDataType dataName in sweepDataListTemp.Keys)
            {
                dataNameList.Add(dataName.ToString());
                sweepDataList.Add(sweepDataListTemp[dataName]);
            }

            double[][] sweepDataArray = sweepDataList.ToArray();
            string[] dataNameArray = dataNameList.ToArray();

            if ((sweepDataArray.Length > 0) && (dataNameArray[0].Length > 0))
            { }
            else
            {
                sweepFileName = "";
                return;
            }

            using (StreamWriter writer = new StreamWriter(sweepFileName))
            {
                // file header
                StringBuilder fileHeader = new StringBuilder();

                for (int count = 0; count < dataNameArray.Length; count++)
                {
                    if (count != 0) fileHeader.Append(",");
                    fileHeader.Append(dataNameArray[count]);
                }

                // file contents
                List<string> fileContents = new List<string>();
                for (int irow = 0; irow < sweepDataArray[0].Length; irow++)
                {
                    StringBuilder aLine = new StringBuilder();
                    for (int icol = 0; icol < dataNameArray.Length; icol++)
                    {
                        if (icol != 0) aLine.Append(",");
                        aLine.Append(sweepDataArray[icol][irow].ToString());
                    }
                    fileContents.Add(aLine.ToString());
                }

                // write file header to file
                writer.WriteLine(fileHeader.ToString());

                // writer file contents to file
                foreach (string row in fileContents)
                {
                    writer.WriteLine(row);
                }
            }
           
        }


        public Type UserControl
        {
            get { return (typeof(Mod_HittMzLiSweepGui)); }
        }

        #endregion
    }
}

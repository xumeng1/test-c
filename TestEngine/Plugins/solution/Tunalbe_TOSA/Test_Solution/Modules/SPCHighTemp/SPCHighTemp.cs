// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// SPCHighTemp.cs
//
// Author: ken.wu, 2008
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using TCMZDefinitions;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Test Module Template.
    /// </summary>
    public class SPCHighTemp : ITestModule
    {
        #region ITestModule Members

        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, DatumList configData, InstrumentCollection instruments, ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {
            //Get instruments.
            IInstType_TriggeredOpticalPowerMeter powerMeter = instruments["OpmMz"] as IInstType_TriggeredOpticalPowerMeter;
            IInstType_Wavemeter waveMeter = instruments["Wavemeter"] as IInstType_Wavemeter;
            IInstType_TecController tecDsdbr = instruments["TecDsdbr"] as IInstType_TecController;
            IInstType_TecController tecMz = instruments["TecMz"] as IInstType_TecController;
            IInstType_TecController tecCase = instruments["TecCase"] as IInstType_TecController;

            SPCStageData spcData = configData.ReadReference("SPCStageData") as SPCStageData;
            DatumList baseline = configData.ReadListDatum("BaselineDatum");
            
            powerMeter.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;
            spcData.Thi_PeakPowerChangeOpenLoop_dB = powerMeter.ReadPower();

            spcData.Thi_FrequencyUnlockedChange_GHz = waveMeter.Frequency_GHz;
            spcData.Thi_DSDBRVoltage_V = tecDsdbr.TecVoltageActual_volt;
            spcData.Thi_DSDBRCurrent_mA = tecDsdbr.TecCurrentActual_amp;

            spcData.Thi_MzVoltage_V = tecMz.TecVoltageActual_volt;
            spcData.Thi_MzCurrent_mA = tecMz.TecCurrentActual_amp;

            //
            //spcData.CalculateDelta(finalChannel);
            spcData.CalculateDelta(baseline);

            // return data
            DatumList returnData = new DatumList();

            returnData.AddDouble( "PowerChangeOpenLoop", spcData.Thi_PeakPowerChangeOpenLoop_dB );
            returnData.AddDouble( "deltaPowerChangeOpenLoop", spcData.deltaThi_PeakPowerChangeOpenLoop_dB ?? 0);
            returnData.AddDouble( "TecDSDBRVoltage", spcData.Thi_DSDBRVoltage_V );
            returnData.AddDouble( "deltaTecDSDBRVoltage_V", spcData.deltaThi_DSDBRVoltage_V ?? 0 );
            returnData.AddDouble( "TecDSDBRCurrent", spcData.Thi_DSDBRCurrent_mA );
            returnData.AddDouble( "TecMzVoltageVoltage", spcData.Thi_MzVoltage_V );
            returnData.AddDouble( "TecMzCurrentVoltage", spcData.Thi_MzCurrent_mA );
            returnData.AddDouble("deltaFrequencyChangeUnlocked", spcData.deltaThi_FrequencyUnlockedChange_GHz ?? 0);

            return returnData;
        }

        public Type UserControl
        {
            get { return (typeof(SPCHighTempGui)); }
        }

        #endregion
    }
}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// TcmzChannelChar_Config.cs
//
// Author: Tony.Foster, Mark Fullalove 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestSolution.ILMZFinalData;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// This structure encapsulates all the data requirements of the test module
    /// At test program level the user declares an instance of the structure, but not a new instance, 
    /// ie mystruct myInstance;
    /// then they can make populate the members of the struct
    /// ie
    /// myInstance.param1 = 20;
    /// myInstance.param2 = 20;
    /// 
    /// Only when the structure is fully populated can they call
    /// DatumList myD = myInstance.GetDatumList();
    /// 
    /// Else they will get a compile time error, as the object myInstance
    /// only exists at the point where all members are populated!
    /// 
    /// In the test module level the user simply calls
    /// mystruct myInstance = new mystruct(configData);
    /// and they can read the myInstance.param1, myInstance.param2 = 20 values;
    /// </summary>
    public struct TcmzChannelChar_ZD_Config
    {
        /// <summary>
        /// Method populates a datumlist based on this structure data
        /// </summary>
        /// <returns>datumlist</returns>
        public DatumList GetDatumList()
        {
            DatumList myLocalDatumList = new DatumList();

            myLocalDatumList.AddBool("ItuChar_TestIfChansMissing", this.ItuChar_TestIfChansMissing);
            myLocalDatumList.AddDouble("Tune_IsoaPowerSlope", this.Tune_IsoaPowerSlope);
            //myLocalDatumList.AddDouble("Tune_OpticalPowerTolerance_dB", this.Tune_OpticalPowerTolerance_dB);
            //myLocalDatumList.AddDouble("Tune_OpticalPowerTolerance_mW", this.Tune_OpticalPowerTolerance_mW);
            myLocalDatumList.AddDouble("PtrPosOffset_GHz", this.PtrPosOffset_GHz);
            myLocalDatumList.AddDouble("PtrNegOffset_GHz", this.PtrNegOffset_GHz);
            myLocalDatumList.AddDouble("ItuFreqTolerance_GHz", this.ItuFreqTolerance_GHz);
            //myLocalDatumList.AddDouble("MzCtrlINominal_mA", this.MzCtrlINominal_mA);
            //myLocalDatumList.AddDouble("MzTapBias_V", this.MzTapBias_V);
            myLocalDatumList.AddDouble("AcVpiScaleFactor", this.AcVpiScaleFactor);
            myLocalDatumList.AddDouble("SoaSweep_MinCurrent_mA", this.SoaSweep_MinCurrent_mA);
            myLocalDatumList.AddDouble("SoaSweep_MaxCurrent_mA", this.SoaSweep_MaxCurrent_mA);
            myLocalDatumList.AddDouble("SoaSweep_RefCurrent_mA", this.SoaSweep_RefCurrent_mA);
            myLocalDatumList.AddSint32("SoaSweep_Stepdelay_ms", this.SoaSweep_Stepdelay_ms);
            myLocalDatumList.AddDouble("LockerBias_V", this.LockerBias_V);
            myLocalDatumList.AddDouble("Tune_IrearAdjustFreq_GHz", this.Tune_IrearAdjustFreq_GHz);
            myLocalDatumList.AddDouble("SMSRMeasure_Span_nm", this.SMSRMeasure_Span_nm);
            myLocalDatumList.AddDouble("SMSRMeasure_RBW_nm", this.SMSRMeasure_RBW_nm);
            myLocalDatumList.AddDouble("SMSRMeasure_OSASensitivity_dBm", this.SMSRMeasure_OSASensitivity_dBm);
            myLocalDatumList.AddSint32("SMSRMeasure_NumPts", this.SMSRMeasure_NumPts);
            myLocalDatumList.AddDouble("SupermodeSRMeasure_Span_nm", this.SupermodeSRMeasure_Span_nm);
            myLocalDatumList.AddDouble("SupermodeSRMeasure_RBW_nm", this.SupermodeSRMeasure_RBW_nm);
            myLocalDatumList.AddDouble("SupermodeSRMeasure_OSASensitivity_dBm", this.SupermodeSRMeasure_OSASensitivity_dBm);
            myLocalDatumList.AddSint32("SupermodeSRMeasure_NumPts", this.SupermodeSRMeasure_NumPts);
            myLocalDatumList.AddDouble("SupermodeSRMeasure_Exclude_nm", this.SupermodeSRMeasure_Exclude_nm);
            myLocalDatumList.AddDouble("SoaShutter_MaxCurrent_mA", this.SoaShutter_MaxCurrent_mA);
            myLocalDatumList.AddDouble("SoaShutter_MaxSoaElectricalPower_mW", this.SoaShutter_MaxSoaElectricalPower_mW);
            myLocalDatumList.AddDouble("SoaShutter_MaxVoltage_V", this.SoaShutter_MaxVoltage_V);
            myLocalDatumList.AddDouble("SoaShutter_VoltageComplianceOffset_V", this.SoaShutter_VoltageComplianceOffset_V);
            myLocalDatumList.AddDouble("PtrIphaseOutset_mA", this.PtrIphaseOutset_mA);
            myLocalDatumList.AddSint32("TTRLVSweepChanCount", this.TTRLVSweepChanCount);

            return (myLocalDatumList);
        }

        /// <summary>
        /// Constructor which takes a datumList and populates the type
        /// </summary>
        /// <param name="mydata"></param>
        public TcmzChannelChar_ZD_Config(DatumList mydata)
        {
            this.ItuChar_TestIfChansMissing = mydata.ReadBool("ItuChar_TestIfChansMissing");
            this.Tune_IsoaPowerSlope = mydata.ReadDouble("Tune_IsoaPowerSlope");
            this.Tune_OpticalPowerTolerance_dB = mydata.ReadDouble("Tune_OpticalPowerTolerance_dB");
            //this.Tune_OpticalPowerTolerance_mW = mydata.ReadDouble("Tune_OpticalPowerTolerance_mW");
            this.PtrPosOffset_GHz = mydata.ReadDouble("PtrPosOffset_GHz");
            this.PtrNegOffset_GHz = mydata.ReadDouble("PtrNegOffset_GHz");
            this.ItuFreqTolerance_GHz = mydata.ReadDouble("ItuFreqTolerance_GHz");
            //this.MzCtrlINominal_mA = mydata.ReadDouble("MzCtrlINominal_mA");
            //this.MzTapBias_V = mydata.ReadDouble("MzTapBias_V");
            this.AcVpiScaleFactor = mydata.ReadDouble("AcVpiScaleFactor");
            this.SoaSweep_MinCurrent_mA = mydata.ReadDouble("SoaSweep_MinCurrent_mA");
            this.SoaSweep_MaxCurrent_mA = mydata.ReadDouble("SoaSweep_MaxCurrent_mA");
            this.SoaSweep_RefCurrent_mA = mydata.ReadDouble("SoaSweep_RefCurrent_mA");
            this.SoaSweep_Stepdelay_ms = mydata.ReadSint32("SoaSweep_Stepdelay_ms");
            this.LockerBias_V = mydata.ReadDouble("LockerBias_V");
            this.Tune_IrearAdjustFreq_GHz = mydata.ReadDouble("Tune_IrearAdjustFreq_GHz");
            this.SMSRMeasure_Span_nm = mydata.ReadDouble("SMSRMeasure_Span_nm");
            this.SMSRMeasure_RBW_nm = mydata.ReadDouble("SMSRMeasure_RBW_nm");
            this.SMSRMeasure_OSASensitivity_dBm = mydata.ReadDouble("SMSRMeasure_OSASensitivity_dBm");
            this.SMSRMeasure_NumPts = mydata.ReadSint32("SMSRMeasure_NumPts");
            this.SupermodeSRMeasure_Span_nm = mydata.ReadDouble("SupermodeSRMeasure_Span_nm");
            this.SupermodeSRMeasure_RBW_nm = mydata.ReadDouble("SupermodeSRMeasure_RBW_nm");
            this.SupermodeSRMeasure_OSASensitivity_dBm = mydata.ReadDouble("SupermodeSRMeasure_OSASensitivity_dBm");
            this.SupermodeSRMeasure_NumPts = mydata.ReadSint32("SupermodeSRMeasure_NumPts");
            this.SupermodeSRMeasure_Exclude_nm = mydata.ReadDouble("SupermodeSRMeasure_Exclude_nm");
            this.SoaShutter_MaxCurrent_mA = mydata.ReadDouble("SoaShutter_MaxCurrent_mA");
            this.SoaShutter_MaxSoaElectricalPower_mW = mydata.ReadDouble("SoaShutter_MaxSoaElectricalPower_mW");
            this.SoaShutter_MaxVoltage_V = mydata.ReadDouble("SoaShutter_MaxVoltage_V");
            this.SoaShutter_VoltageComplianceOffset_V = mydata.ReadDouble("SoaShutter_VoltageComplianceOffset_V");
            this.PtrIphaseOutset_mA = mydata.ReadDouble("PtrIphaseOutset_mA");
            this.TTRLVSweepChanCount = mydata.ReadSint32("TTRLVSweepChanCount");
        }

        //List of all parameters
        #region List of all parameters
        /// <summary>
        /// Test flag
        /// </summary>
        public bool ItuChar_TestIfChansMissing;
        /// <summary>
        /// Tune constant Isoa/Power slope
        /// </summary>
        public double Tune_IsoaPowerSlope;
        /// <summary>
        /// Tune power tolerance dB
        /// </summary>
        public double Tune_OpticalPowerTolerance_dB;
        ///// <summary>
        ///// Tune power tolerance mW
        ///// </summary>
        ////public double Tune_OpticalPowerTolerance_mW;
        /// <summary>
        /// PTR positive frequency offset
        /// </summary>
        public double PtrPosOffset_GHz;
        /// <summary>
        /// PTR negative frequency offset
        /// </summary>
        public double PtrNegOffset_GHz;
        /// <summary>
        /// ITU frequency tolerance
        /// </summary>
        public double ItuFreqTolerance_GHz;
        ///// <summary>
        ///// Nominal MZ control current
        ///// </summary>
        //public double MzCtrlINominal_mA;
        ///// <summary>
        ///// MZ Tap bias voltage
        ///// </summary>
        //public double MzTapBias_V;
        /// <summary>
        /// Ac Vpi scale factor
        /// </summary>
        public double AcVpiScaleFactor;
        /// <summary>
        /// Soa Sweep Minimum current
        /// </summary>
        public double SoaSweep_MinCurrent_mA;
        /// <summary>
        /// Soa Sweep Maximum current
        /// </summary>
        public double SoaSweep_MaxCurrent_mA;
        /// <summary>
        /// Soa Sweep Reference current
        /// </summary>
        public double SoaSweep_RefCurrent_mA;
        /// <summary>
        /// Soa Sweep step delay
        /// </summary>
        public int SoaSweep_Stepdelay_ms;
        /// <summary>
        /// Locker Bias voltage
        /// </summary>
        public double LockerBias_V;
        /// <summary>
        /// Tuning Irear adjust frequency
        /// </summary>
        public double Tune_IrearAdjustFreq_GHz;
        /// <summary>
        /// Span - narrow span around ITU
        /// </summary>
        public double SMSRMeasure_Span_nm;
        /// <summary>
        /// Resolution bandwidth
        /// </summary>
        public double SMSRMeasure_RBW_nm;
        /// <summary>
        /// OSA sensitivity setting - set to resolve lower peaks
        /// </summary>
        public double SMSRMeasure_OSASensitivity_dBm;
        /// <summary>
        /// Number of trace points
        /// </summary>
        public int SMSRMeasure_NumPts;
        /// <summary>
        /// Span - wide span to observe supermodes
        /// </summary>
        public double SupermodeSRMeasure_Span_nm;
        /// <summary>
        /// Resolution bandwidth
        /// </summary>
        public double SupermodeSRMeasure_RBW_nm;
        /// <summary>
        /// OSA sensitivity setting - set to resolve lower peaks
        /// </summary>
        public double SupermodeSRMeasure_OSASensitivity_dBm;
        /// <summary>
        /// Number of trace points
        /// </summary>
        public int SupermodeSRMeasure_NumPts;
        /// <summary>
        /// Exclusion region around carrier
        /// </summary>
        public double SupermodeSRMeasure_Exclude_nm;
        /// <summary>
        /// SOA Shutter max current
        /// </summary>
        public double SoaShutter_MaxCurrent_mA;
        /// <summary>
        /// SOA Shutter max voltage
        /// </summary>
        public double SoaShutter_MaxVoltage_V;
        /// <summary>
        /// SOA Shutter max electrical power
        /// </summary>
        public double SoaShutter_MaxSoaElectricalPower_mW;
        /// <summary>
        /// SOA Shutter SOA source voltage compliance setting offset
        /// </summary>
        public double SoaShutter_VoltageComplianceOffset_V;
        /// <summary>
        /// IphaseOutset_mA in Phase tuning range
        /// </summary>
        public double PtrIphaseOutset_mA;
        /// <summary>
        /// Count of channels with actual LV sweeps for TTR
        /// </summary>
        public int TTRLVSweepChanCount;
        #endregion
    }
}

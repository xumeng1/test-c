// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// TcmzCrossCalibration.cs
//
// Author: rong.guo, 2008
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestSolution.IlmzCommonData;
using Bookham.TestSolution.IlmzCommonUtils;
using Bookham.TestLibrary.Instruments;
using Bookham.TestLibrary.InstrTypes;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Test Module Template.
    /// </summary>
    public class IlmzCrossCalibration: ITestModule
    {
        #region ITestModule Members

        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, DatumList configData, InstrumentCollection instruments, ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {
            engine.GuiToFront();
            engine.GuiShow();

            DatumList returnData = new DatumList();
            DsdbrChannelData[] dsdbrChannels = (DsdbrChannelData[])configData.ReadReference("DsdbrChannels");
            int OpticalCal_delay_ms = configData.ReadSint32("OpticalCal_delay_ms");
            double freqDivTolerance_GHz = configData.ReadDouble("FreqDivTolerance_GHz");
            //DsdbrUtils.opticalSwitch= (Inst_Ke2510)instruments["Optical_switch"];
            DsdbrTuning.OpticalSwitch= (Inst_Ke2510)instruments["Optical_switch"];
            IInstType_TecController DUT_TEC = (IInstType_TecController)instruments["Optical_switch"];
            int rthCount = 0;
            double[] freqCalOffset_GHz = new double[dsdbrChannels.Length];

            double[] freqCal_GHz = new double[dsdbrChannels.Length];
            bool isWaveMeterOnline = false;

            // The oftenly use of IsOnline may produce logging queue full error - chongjian.liang 2013.4.20
            if (Measurements.Wavemeter != null)
            {
                isWaveMeterOnline = Measurements.Wavemeter.IsOnline;
            }

            do
            {
                if (!DUT_TEC.OutputEnabled)
                {
                    DUT_TEC.OutputEnabled = true;//to avoid Ke2510 turn off sometime. jack.zhang 2012-11-13
                    System.Threading.Thread.Sleep(OpticalCal_delay_ms);
                }

                for (int ii = 0; ii < dsdbrChannels.Length; ii++)
                {
                    DsdbrChannelData chan = dsdbrChannels[ii];

                    // Set DSDBR currents
                    DsdbrUtils.SetDsdbrCurrents_mA(chan.Setup);
                    System.Threading.Thread.Sleep(OpticalCal_delay_ms);

                    // Read frequency
                    double freq_GHz = 0;

                    if (isWaveMeterOnline)
                    {
                        freq_GHz = Measurements.ReadFrequency_GHz();
                    }
                    else
                    {
                        freq_GHz = DsdbrTuning.ReadFreq_GhzViaLowCostMeter(chan.ItuFreq_GHz);
                    }
                    freqCalOffset_GHz[ii] = freq_GHz - chan.ItuFreq_GHz;
                    freqCal_GHz[ii] = freq_GHz;
                    engine.SendToGui(string.Format("Frequency measured at CH{0} is {1} GHz. The calibration offset is {2} GHz",
                        chan.ItuChannelIndex, freq_GHz, freqCalOffset_GHz[ii]));
                }
            } while (!DUT_TEC.OutputEnabled && rthCount++ < numRthTries);//to avoid Ke2510 turn off sometime. jack.zhang 2012-11-13
            // return data
           
            returnData.AddDouble("XCAL_CH_LO", freqCalOffset_GHz[0]);
            returnData.AddDouble("XCAL_CH_MID", freqCalOffset_GHz[1]);
            returnData.AddDouble("XCAL_CH_HI", freqCalOffset_GHz[2]);
            return returnData;
        }

        public Type UserControl
        {
            get { return (typeof(TcmzCrossCalibrationGui)); }
        }

        #endregion

        #region private Data

        const int numRthTries = 5;

        #endregion private Data
    }
}

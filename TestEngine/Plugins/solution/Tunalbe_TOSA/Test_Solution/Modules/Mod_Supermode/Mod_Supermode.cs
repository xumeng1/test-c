// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// Mod_Supermode.cs
//
// Author: tommy.yu, 2009
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.fcumapping.CommonData;
using System.Collections.Specialized;
using Bookham.TestLibrary.Instruments;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.FcuCommonUtils;
using System.IO;
using Bookham.TestLibrary.Utilities;
using Bookham.TestSolution.IlmzCommonUtils;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Test Module Template.
    /// </summary>
    public class Mod_Supermode : ITestModule
    {
        #region ITestModule Members
        private ITestEngine myengine = null;
        private const int totalPercentage = 201;
        private string showStr = "";

        string cg_passfail_sm_file;
        string cg_qametrics_sm_file;
        string cg_lm_lines_sm_file;

        int Mild_MultiMode = 1;


        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, DatumList configData, InstrumentCollection instruments, ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {
            //get the fcu instrument and the wavemeter instrument
            myengine = engine;
            Inst_Fcu2Asic fcu = (Inst_Fcu2Asic)instruments["FCU"];
            int num_supermode = configData.ReadSint32("NUM_SUPERMODE");
            string PhaseCurrentSetupFile = configData.ReadString("PHASE_SETUP_FILE_PATH");
            string MiddleLineFilePath = configData.ReadString("MIDDLE_LINE_PATH");
            string work_dir = configData.ReadString("RESULT_DIR");
            string laser_id = configData.ReadString("LASER_ID");
            string timestamp = configData.ReadString("TIME_STAMP");
            long supermode_num = configData.ReadUint32("SUPERMODE_NUM");
            string equipId = configData.ReadString("EQUIP_ID");
            string specId = configData.ReadString("SPEC_ID");
            int NodeId = configData.ReadSint32("NODE");
            bool  useDUTEtalon = configData.ReadBool("USEDUTETALON");
            double soa_powerLevel = configData.ReadDouble("Soa_powerLevel");
            double Mz_Imb_Left_mA = configData.ReadDouble("Mz_Imb_Left_mA");
            double Mz_Imb_Right_mA = configData.ReadDouble("Mz_Imb_Right_mA");
            double locker_tran_pot = configData.ReadDouble("locker_tran_pot");
            Inst_Ke2510 Optical_Switch = (Inst_Ke2510)instruments["Optical_switch"];
            string Rear_RearSoa_Switch = configData.ReadString("Rear_RearSoa_Switch");

            string testTimeStamp = DateTime.Now.ToString("yyyyMMddHHmmss");
            //TestParamConfigAccessor MapSettings = (TestParamConfigAccessor)configData.ReadReference("Mapping_Settings");

            cg_passfail_sm_file = configData.ReadString("NoFilePath");
            cg_qametrics_sm_file = configData.ReadString("NoFilePath");
            cg_lm_lines_sm_file = configData.ReadString("NoFilePath");

            // TODO - add your code here!
            engine.GuiToFront();
            engine.GuiShow();
            showStr = "SuperMode" + num_supermode.ToString() + " Map ";
            engine.SendToGui(showStr);

            CDSDBRSuperMode sm = new CDSDBRSuperMode();
            sm.setup(laser_id, timestamp, work_dir);
            sm.loadAllFromFile = false;//simulation for loab SM data jack Zhang
            sm.LoadImCurrent(MiddleLineFilePath);
            sm.LoadPhaseCurrent(PhaseCurrentSetupFile);
            fcu.IsOnline = true;
            sm.Inst = fcu;
            fcu.SupermapProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(fcu_SupermapProgressChanged);
            sm.ScanDirectionChange += new EventHandler(sm_ScanDirectionChange);
            sm.StartSuperModeAnalysis += new EventHandler(sm_StartSuperModeAnalysis);
            sm.Sm_number = (short)supermode_num;
            engine.SendToGui(showStr + "\nTwo Direction Scan");
            sm.I_gain = configData.ReadDouble("IgainForMapping_mA");
            sm.I_soa = soa_powerLevel;
            sm.Mz_Imb_Left_mA = Mz_Imb_Left_mA;
            sm.Mz_Imb_Right_mA = Mz_Imb_Right_mA;
            sm.I_rearsoa = configData.ReadDouble("IrearsoaForMapping_mA");
            sm.UseLockerRatioForSM = configData.ReadBool("USELockerRatioForSM");
            IInstType_DigitalIO outLine_Tx = Optical_Switch.GetDigiIoLine(IlmzOpticalSwitchLines.Etalon_Tx_DigiIoLine);//Jack.zhang Rx swith between DUT GB and Etalon Wavemeter
            IInstType_DigitalIO outLine_Rx = Optical_Switch.GetDigiIoLine(IlmzOpticalSwitchLines.Etalon_Rx_DigiIoLine); //raul changed
            outLine_Tx.LineState = IlmzOpticalSwitchLines.Etalon_Tx_DigiIoLine_State;
            outLine_Rx.LineState = IlmzOpticalSwitchLines.Etalon_Rx_DigiIoLine_State;
            if (useDUTEtalon)
            {
                outLine_Tx.LineState = IlmzOpticalSwitchLines.Dut_Tx_DigiIoLine_State;
                outLine_Rx.LineState = IlmzOpticalSwitchLines.Dut_Rx_DigiIoLine_State;  //raul changed
            }
            sm.Rear_RearSoa_Switch = Rear_RearSoa_Switch;
            sm.screen(useDUTEtalon);

            
            Mild_MultiMode = sm.getMildMultiMode();

            // return data
            DatumList returnData = new DatumList();
            returnData.AddReference("SUMPERMODE" + supermode_num.ToString(), sm);
            returnData.AddString("CGid", sm.CG_ID);
            returnData.AddString("ErrorInformation", sm.ErrorInformation);
            //temp set to zero
            returnData.AddSint32("MILD_MULTIMODE_SM" + sm.Sm_number.ToString(), Mild_MultiMode);
            returnData.AddDouble("MODAL_DISTORT_SM" + sm.Sm_number.ToString(), sm.getModeDistortion());



            #region add trace limit file result

            if (File.Exists(sm.LinesFilePath))
            {
                cg_lm_lines_sm_file = sm.LinesFilePath;
            }
            double gain_i_sm_map = sm.I_gain;
            double hysteresis_percent = sm.getHyteresisPercent();

            if (File.Exists(sm.passFailMetricsFilePath))
            {
                cg_passfail_sm_file = sm.passFailMetricsFilePath;
            }
            if (File.Exists(sm.qaMetricsFilePath))
            {
                cg_qametrics_sm_file = sm.qaMetricsFilePath;
            }

            double modal_distort = sm.getModeDistortion();
            int num_lm = sm.Num_lm;
            int smID = sm.Sm_number;
            double sm_i_soa = sm.I_soa;

            returnData.AddString("timestamp", timestamp);
            returnData.AddFileLink("cg_lm_lines_sm_file", cg_lm_lines_sm_file);
            returnData.AddString("equipId", equipId);
            returnData.AddDouble("gain_i_sm_map", gain_i_sm_map);
            returnData.AddDouble("hysteresis_percent", hysteresis_percent);
            returnData.AddFileLink("pRatio_fwd", sm.Matrix_PowerRatio_forward_filepath);
            returnData.AddFileLink("pRatio_rev", sm.Matrix_PowerRatio_reverse_filepath);
            returnData.AddFileLink("photo1_fwd", sm.Matrix_TX_forward_filePath); //Locker_Tx
            returnData.AddFileLink("photo1_rev", sm.Matrix_TX_reverse_filePath);//Locker_Tx
            returnData.AddFileLink("photo2_fwd", sm.Matrix_RX_forward_filepath);//Locker_Rx
            returnData.AddFileLink("photo2_rev", sm.Matrix_RX_reverse_filepath);//Locker_Rx
            returnData.AddDouble("modal_distort", modal_distort);
            returnData.AddSint32("num_lm", num_lm);
            returnData.AddSint32("smID", smID);
            returnData.AddDouble("sm_i_soa", sm_i_soa);
            //returnData.AddString("TEST_STATUS", "TEST_STATUS");
            returnData.AddString("specId", specId);
            returnData.AddString("testTimeStamp", testTimeStamp);
            //returnData.AddSint32("NodeId", NodeId);
            //returnData.AddString("SPEC_ID", specId);
            //returnData.AddString("SERIAL_NO", laser_id);
            returnData.AddFileLink("cg_passfail_sm_file", cg_passfail_sm_file);
            returnData.AddFileLink("cg_qametrics_sm_file", cg_qametrics_sm_file);
            returnData.AddSint32("screen_result_sm", sm.screen_result_sm);
            returnData.AddDouble("rearsoa_i_sm", 20.0);


            #endregion
            return returnData;
        }

        void sm_StartSuperModeAnalysis(object sender, EventArgs e)
        {
            myengine.SendToGui(showStr + "\nStart SuperMode Analysis");
        }

        void sm_ScanDirectionChange(object sender, EventArgs e)
        {
            myengine.SendToGui(showStr + "\nRevese Scan");
        }

        void fcu_SupermapProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            int value = (int)(e.ProgressPercentage * 1.0 / totalPercentage * 100);
            if (value > 100)
                value = 100;
            if (value < 0)
                value = 0;
            myengine.SendToGui(value);
        }

        public Type UserControl
        {
            get { return (typeof(Mod_SupermodeGui)); }
        }

        #endregion
    }
}

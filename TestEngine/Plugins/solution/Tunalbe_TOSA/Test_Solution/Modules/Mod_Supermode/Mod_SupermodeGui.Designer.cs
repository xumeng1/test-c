// [Copyright]
//
// Bookham Test Engine
// Mod_Supermode
//
// Bookham.TestSolution.TestModules/Mod_SupermodeGui
// 
// Author: paul.annetts
// Design: TODO

namespace Bookham.TestSolution.TestModules
{
    partial class Mod_SupermodeGui
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelInfoStage = new System.Windows.Forms.Label();
            this.superModeProgressBar = new System.Windows.Forms.ProgressBar();
            this.LabelPercentage = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelInfoStage
            // 
            this.labelInfoStage.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInfoStage.Location = new System.Drawing.Point(52, 54);
            this.labelInfoStage.Name = "labelInfoStage";
            this.labelInfoStage.Size = new System.Drawing.Size(277, 76);
            this.labelInfoStage.TabIndex = 0;
            this.labelInfoStage.Text = "SuperMode Map";
            // 
            // superModeProgressBar
            // 
            this.superModeProgressBar.ForeColor = System.Drawing.SystemColors.Desktop;
            this.superModeProgressBar.Location = new System.Drawing.Point(57, 145);
            this.superModeProgressBar.Name = "superModeProgressBar";
            this.superModeProgressBar.Size = new System.Drawing.Size(460, 45);
            this.superModeProgressBar.TabIndex = 1;
            // 
            // LabelPercentage
            // 
            this.LabelPercentage.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelPercentage.Location = new System.Drawing.Point(523, 156);
            this.LabelPercentage.Name = "LabelPercentage";
            this.LabelPercentage.Size = new System.Drawing.Size(76, 21);
            this.LabelPercentage.TabIndex = 2;
            this.LabelPercentage.Text = "0%";
            // 
            // Mod_SupermodeGui
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.LabelPercentage);
            this.Controls.Add(this.superModeProgressBar);
            this.Controls.Add(this.labelInfoStage);
            this.Name = "Mod_SupermodeGui";
            this.MsgReceived += new Bookham.TestEngine.Framework.Messages.ManagedCtlBase.MsgReceivedDlg(this.ModuleGui_MsgReceived);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelInfoStage;
        private System.Windows.Forms.ProgressBar superModeProgressBar;
        private System.Windows.Forms.Label LabelPercentage;
    }
}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// Mod_GetDUTLockRatio.cs
//
// Author: tim.yang, 2012
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections.Specialized;
using System.IO;

using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.ExternalData;
using Bookham.TestLibrary.Utilities;
using Bookham.TestLibrary.Algorithms;


namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Test Module Template.
    /// </summary>
    public class Mod_GetDUTLockRatio : ITestModule
    {
        #region ITestModule Members

        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, DatumList configData, InstrumentCollection instruments, ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {

            // TODO - add your code here!
            string errDescription = "";
            FailModeCheck failModeCheck = configData.ReadReference("FailModeCheck") as FailModeCheck;
            string ResultFileName = configData.ReadString("ResultFileName");
            double FrequencyCheckRange = configData.ReadDouble("FrequencyCheckRange");
            bool GetFileOK = true;
            DatumList PostBurn_testStatus = configData.ReadListDatum("PostBurn_testStatus");
            DatumList PreBurn_testStatus = configData.ReadListDatum("PreBurn_testStatus");
            double Target_Start_Freq_GHz = configData.ReadDouble("Target_Start_Freq_GHz");
            double Target_End_Freq_GHz = configData.ReadDouble("Target_End_Freq_GHz");
            CsvFileObject csvWrite = new CsvFileObject(ResultFileName);
            if (File.Exists(ResultFileName))
                csvWrite.Read();
            List<double> DUTLockerRatioDeltFreq_50GHz = new List<double>();
            List<double> DUTLockerRatioDeltFreq_100GHz = new List<double>();
            List<double> DUTLockerRatio_50GHz = new List<double>();
            List<double> DUTLockerRatio_100GHz = new List<double>();
            LinearLeastSquaresFit DUTLockerRatioSlope_50GHz;
            LinearLeastSquaresFit DUTLockerRatioSlope_100GHz;


            // return data
            DatumList returnData = new DatumList();

            try
            {
                    DataTable FreqToDUTLockerRatio = new DataTable("FreqToDUTLockerRatio");
                    FreqToDUTLockerRatio.Columns.Add("Freq", typeof(Double));
                    FreqToDUTLockerRatio.Columns.Add("DutLockerRatio", typeof(Double));

                    #region Get Freq to DUT LR data from post burn phase cut scan test
                    int node = PostBurn_testStatus.ReadSint32("NODE");
                    string Datafile_temp = PostBurn_testStatus.ReadFileLinkFullPath("PHASE_CUT_POSTBURN_FILE");
                    Datafile_temp = CheckPCASFilePath(Datafile_temp, node.ToString());

                    if (Datafile_temp == null)
                    {
                        //errDescription = "Can't find the PHASE_CUT_POSTBURN_FILE file." +
                        //        "\n please contact IS to confirm this!";
                        //engine.ShowContinueUserQuery(errDescription);
                        GetFileOK = false;
                    }
                    else
                    {
                        CsvFileObject csvReader = new CsvFileObject(Datafile_temp);
                        if (File.Exists(Datafile_temp))
                            csvReader.Read();

                        for (int i = 1; i <= csvReader.RowsCount; i++)
                        {
                            csvWrite[i, 11] = csvReader[i, 3];//Freq_GHz
                            if (i != 1)
                            {
                                FreqToDUTLockerRatio.Rows.Add(new object[] { Convert.ToDouble(csvReader[i, 3]), 1/Convert.ToDouble(csvReader[i, 5]) });
                                csvWrite[i, 12] = csvReader[i, 5];//DUT_LockerRatio(Tx/Rx)
                            }
                            else
                                csvWrite[i, 12] = csvReader[i, 5] + "_Post_Phase";
                        }
                    }
                    #endregion
                    #region Get Freq to DUT LR data from post burn rear cut scan test
                    if (GetFileOK)
                    {
                        Datafile_temp = PostBurn_testStatus.ReadFileLinkFullPath("REAR_CUT_POSTBURN_FILE");
                        Datafile_temp = CheckPCASFilePath(Datafile_temp, node.ToString());

                        if (Datafile_temp == null)
                        {
                            //errDescription = "Can't find the REAR_CUT_POSTBURN_FILE file." +
                            //        "\n please contact IS to confirm this!";
                            //engine.ShowContinueUserQuery(errDescription);
                            GetFileOK = false;
                        }
                        else
                        {
                            CsvFileObject csvReader = new CsvFileObject(Datafile_temp);
                            if (File.Exists(Datafile_temp))
                                csvReader.Read();

                            for (int i = 1; i <= csvReader.RowsCount; i++)
                            {
                                csvWrite[i, 7] = csvReader[i, 3];//Freq_GHz
                                if (i != 1)
                                {
                                    FreqToDUTLockerRatio.Rows.Add(new object[] { Convert.ToDouble(csvReader[i, 3]), 1/Convert.ToDouble(csvReader[i, 5]) });
                                    csvWrite[i, 8] = csvReader[i, 5];////DUT_LockerRatio(Tx/Rx)
                                }
                                else
                                    csvWrite[i, 8] = csvReader[i, 5] + "_Post_Rear";
                            }
                        }
                    }
                    #endregion
                    #region Get Freq to DUT LR data from pre burn phase cut scan test
                    if (GetFileOK)
                    {
                        Datafile_temp = PreBurn_testStatus.ReadFileLinkFullPath("PHASE_CUT_PREBURN_FILE");
                        Datafile_temp = CheckPCASFilePath(Datafile_temp, node.ToString());

                        if (Datafile_temp == null)
                        {
                            //errDescription = "Can't find the PHASE_CUT_PREBURN_FILE file." +
                            //        "\n please contact IS to confirm this!";
                            //engine.ShowContinueUserQuery(errDescription);
                            GetFileOK = false;
                        }
                        else
                        {
                            CsvFileObject csvReader = new CsvFileObject(Datafile_temp);
                            if (File.Exists(Datafile_temp))
                                csvReader.Read();

                            for (int i = 1; i <= csvReader.RowsCount; i++)
                            {
                                csvWrite[i, 9] = csvReader[i, 3];//Freq_GHz
                                if (i != 1)
                                {
                                    FreqToDUTLockerRatio.Rows.Add(new object[] { Convert.ToDouble(csvReader[i, 3]), 1/Convert.ToDouble(csvReader[i, 5]) });
                                    csvWrite[i, 10] = csvReader[i, 5];////DUT_LockerRatio(Tx/Rx)
                                }
                                else
                                    csvWrite[i, 10] = csvReader[i, 5] + "_Pre_Phase";
                            }
                        }
                    }
                    #endregion
                    #region Get Freq to DUT LR data from pre burn rear cut scan test
                    if (GetFileOK)
                    {
                        Datafile_temp = PreBurn_testStatus.ReadFileLinkFullPath("REAR_CUT_PREBURN_FILE");
                        Datafile_temp = CheckPCASFilePath(Datafile_temp, node.ToString());

                        if (Datafile_temp == null)
                        {
                            //errDescription = "Can't find the REAR_CUT_PREBURN_FILE file." +
                            //        "\n please contact IS to confirm this!";
                            //engine.ShowContinueUserQuery(errDescription);
                            GetFileOK = false;
                        }
                        else
                        {
                            CsvFileObject csvReader = new CsvFileObject(Datafile_temp);
                            if (File.Exists(Datafile_temp))
                                csvReader.Read();

                            for (int i = 1; i <= csvReader.RowsCount; i++)
                            {
                                csvWrite[i, 5] = csvReader[i, 3];//Freq_GHz
                                if (i != 1)
                                {
                                    FreqToDUTLockerRatio.Rows.Add(new object[] { Convert.ToDouble(csvReader[i, 3]), 1/Convert.ToDouble(csvReader[i, 5]) });
                                    csvWrite[i, 6] = csvReader[i, 5];////DUT_LockerRatio(Tx/Rx)
                                }
                                else
                                    csvWrite[i, 6] = csvReader[i, 5] + "_Pre_Rear";
                            }
                        }
                    }
                    #endregion
                    #region Get Freq to DUT LR data from pre burn front cut scan test
                    if (GetFileOK)
                    {
                        Datafile_temp = PreBurn_testStatus.ReadFileLinkFullPath("FSB_PREBURN_FILE");
                        Datafile_temp = this.CheckPCASFilePath(Datafile_temp, node.ToString());

                        if (Datafile_temp == null)
                        {
                            //errDescription = "Can't find the Front_CUT_PREBURN_FILE file." +
                            //        "\n please contact IS to confirm this!";
                            //engine.ShowContinueUserQuery(errDescription);
                            GetFileOK = false;
                        }
                        else
                        {
                            CsvFileObject csvReader = new CsvFileObject(Datafile_temp);
                            if (File.Exists(Datafile_temp))
                                csvReader.Read();

                            for (int i = 1; i <= csvReader.RowsCount; i++)
                            {
                                csvWrite[i, 13] = csvReader[i, 3];//Freq_GHz
                                if (i != 1)
                                {
                                    FreqToDUTLockerRatio.Rows.Add(new object[] { Convert.ToDouble(csvReader[i, 3]), 1/Convert.ToDouble(csvReader[i, 4]) });
                                    csvWrite[i, 14] = csvReader[i, 4];////DUT_LockerRatio(Tx/Rx)
                                }
                                else
                                    csvWrite[i, 14] = csvReader[i, 4] + "_Pre_Front";
                            }
                        }
                    }
                    #endregion
                    if (GetFileOK)
                    {
                        DataRow[] rows = FreqToDUTLockerRatio.Select("", "Freq,DutLockerRatio");
                        csvWrite[1, 1] = "Freq";
                        csvWrite[1, 2] = "DutLockerRatio(Rx/Tx)";
                        csvWrite[1, 3] = "ODDorEven ";
                        csvWrite[1, 4] = "Mod_GHz";
                        for (int rIndex = 0; rIndex < rows.Length; rIndex++)
                        {
                            csvWrite[rIndex + 2, 1] = rows[rIndex][0].ToString();
                            csvWrite[rIndex + 2, 2] = rows[rIndex][1].ToString();
                            long Temp =(long)Math.Round(((double)rows[rIndex][0]/50.0),0);
                            double DevidRem=(double)rows[rIndex][0]-Temp*50.0;
                            Math.DivRem(Temp,2,out Temp);
                            csvWrite[rIndex + 2, 3] =(Temp==0?"100":"50");
                            csvWrite[rIndex + 2, 4] = DevidRem.ToString();

                            if (Math.Abs(DevidRem) <= FrequencyCheckRange
&& ((double)rows[rIndex][0] >= Target_Start_Freq_GHz) && ((double)rows[rIndex][0] <= Target_End_Freq_GHz))
                            {
                                if (Temp == 0)
                                {
                                    DUTLockerRatio_100GHz.Add((double)rows[rIndex][1]);
                                    DUTLockerRatioDeltFreq_100GHz.Add(DevidRem);
                                }
                                else
                                {
                                    DUTLockerRatio_50GHz.Add((double)rows[rIndex][1]);
                                    DUTLockerRatioDeltFreq_50GHz.Add(DevidRem);
                                }
                            }
                        }
                        csvWrite.Save();
                        if (DUTLockerRatio_50GHz.Count > 1)
                        {
                            DUTLockerRatioSlope_50GHz = LinearLeastSquaresFitAlgorithm.Calculate(DUTLockerRatioDeltFreq_50GHz.ToArray(), DUTLockerRatio_50GHz.ToArray(), 0, DUTLockerRatio_50GHz.Count - 1);
                            returnData.AddReference("DUTLockerRatioSlope_50GHz", DUTLockerRatioSlope_50GHz);
                        }
                        if (DUTLockerRatio_100GHz.Count > 1)
                        {
                            DUTLockerRatioSlope_100GHz = LinearLeastSquaresFitAlgorithm.Calculate(DUTLockerRatioDeltFreq_100GHz.ToArray(), DUTLockerRatio_100GHz.ToArray(), 0, DUTLockerRatio_100GHz.Count - 1);
                            returnData.AddReference("DUTLockerRatioSlope_100GHz", DUTLockerRatioSlope_100GHz);
                        }
                    }
            }
            catch (Exception e)
            {
                failModeCheck.RaiseError_Mod_NonParamFail(engine, e.Message, FailModeCategory_Enum.UN);
            }
            
            returnData.AddBool("GetDataFileOK", GetFileOK);
            returnData.AddString("ErrorInformation", errDescription);            
            return returnData;
        }

        public Type UserControl
        {
            get { return (typeof(Mod_GetDUTLockRatioGui)); }
        }
        private string CheckPCASFilePath(string PCAS_File, string node)
        {
            if (!PCAS_File.ToLower().Contains("unknown")) return PCAS_File; // vlaid path, return directly
            string filename = PCAS_File.Substring(PCAS_File.LastIndexOf("\\") + 1);
            string strYear = filename.Substring(filename.LastIndexOf('_') + 1, 4);
            string strMonth = filename.Substring(filename.LastIndexOf('_') + 5, 2);
            string strYearMonth = filename.Substring(filename.LastIndexOf('_') + 1, 6);
            string strYearMonthDay = filename.Substring(filename.LastIndexOf('_') + 1, 8);
            string nodeNumber = "Node" + node;


            string serverName = "";
            switch (strYear)
            {
                case "2011":
                    serverName = "\\\\szn-mcad-01\\Archive2010.08";
                    break;
                case "2009":
                    serverName = "\\\\szn-mcad-01\\Archive2009";
                    break;

                case "2008":
                    serverName = "\\\\szn-mcad-01\\Archive2008";
                    break;
                case "2010":
                    if (int.Parse(strMonth) <= 5) serverName = "\\\\szn-sfl-04\\Archive2010";
                    else
                        if (int.Parse(strMonth) >= 6 && int.Parse(strMonth) <= 8) serverName = "\\\\szn-sfl-04\\Archive2010.06";
                        else serverName = "\\\\szn-mcad-01\\Archive2010.08";
                    break;
            }
            string itufilename = Path.Combine(serverName, strYear);
            itufilename = Path.Combine(itufilename, strYearMonth);
            itufilename = Path.Combine(itufilename, nodeNumber);
            itufilename = Path.Combine(itufilename, strYearMonthDay);
            itufilename = Path.Combine(itufilename, filename);
            if (File.Exists(itufilename))
                return itufilename;
            else
                return null;
        }
        #endregion
    }
}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// IlmzMzCharacterise_ZD.cs
//
// Author: Paul.Annetts, Mark Fullalove 2007
// ILMZ GB version : Mark Fullalove 2011
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestSolution.IlmzCommonData;
using Bookham.TestSolution.IlmzCommonInstrs;
using Bookham.TestSolution.IlmzCommonUtils;
using Bookham.ToolKit.Mz;
using Bookham.TestLibrary.Algorithms;
using Bookham.TestSolution.ILMZFinalData;
using Bookham.TestEngine.Framework.Limits;
using Bookham.TestLibrary.Utilities;
using ICSharpCode.SharpZipLib.Zip;
using System.IO;
using Bookham.TestSolution.TestModules;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// TCMZ MZ Characterisation module - does MZ sweeps at various ITU channels
    /// 
    /// Zero chirp, differential sweep
    /// For ILMZ the imbalances and MZ biases are both set to the quadrature point on the positive slope
    /// 
    /// </summary>
    public class IlmzMzCharacterise_ZD : ITestModule
    {
        private IlMzDriverUtils mzDriverUtils;
        private double[] MZCommonModeBiasDepth_v;
        private double MzCtrlINominal_A;
        private double mzImbLeftIdeal_mA;
        private double mzImbRightIdeal_mA;
        private double mzImbLeftMinLimit_mA;
        private double mzImbLeftMaxLimit_mA;
        private double mzImbRightMinLimit_mA;
        private double mzImbRightMaxLimit_mA;
        private double mzInitFixedModBias_volt;
        private double mzInitSweepMaxAbs_V;
        private double mzVleftMinQuadLimit;
        private double mzVleftMaxQuadLimit;
        private double vcmMinLimit;
        private double vcmMaxLimit;
        private double vpiMinLimit;
        private double vpiMaxLimit;
        private double maxArmBias_v;
        private double mzVleftInset = 0.05;
        private double vpiApproxLinearRange_V = 1;
        private double initialVcmOffset = 0;
        private bool initialOffsetSelected;

        private int NumberPoints;
        private double MZTapBias_V;
        private double tapInline_V;
        private string MzFileDirectory;
        private string dutSerialNbr;
        private List<string> LiSweepFile=new List<string> ();

        #region ITestModule Members
        /// <summary>
        /// 
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="userType"></param>
        /// <param name="configData"></param>
        /// <param name="instruments"></param>
        /// <param name="chassis"></param>
        /// <param name="calData"></param>
        /// <param name="previousTestData"></param>
        /// <returns></returns>
        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, 
            DatumList configData, InstrumentCollection instruments,
            ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {

            #region read config
            // get MZ instruments
            IlMzInstruments mzInstrs = (IlMzInstruments)configData.ReadReference("MzInstruments");

            // get list of DSDBR channels
            DsdbrChannelData[] ituChannels = (DsdbrChannelData[])previousTestData.ReadReference("ItuChannels");

            // get test spec (for limit checking)
            Specification testSpec = (Specification)configData.ReadReference("TestSpec");
            MzItuAtVcmData.InitLookup(testSpec);

            this.mzImbLeftIdeal_mA = Convert.ToDouble(testSpec.GetParamLimit("TC_MZ_IMB_LEFT").LowLimit.ValueToString());
            this.mzImbRightIdeal_mA = Convert.ToDouble(testSpec.GetParamLimit("TC_MZ_IMB_RIGHT").LowLimit.ValueToString());
            this.vcmMinLimit = Convert.ToDouble(testSpec.GetParamLimit("TC_MZ_VCM_CAL_LIMIT_MIN").LowLimit.ValueToString());
            this.vcmMaxLimit = Convert.ToDouble(testSpec.GetParamLimit("TC_MZ_VCM_CAL_LIMIT_MAX").HighLimit.ValueToString());
            this.vpiMinLimit = Convert.ToDouble(testSpec.GetParamLimit("TC_MZ_VPI_CAL_LIMIT_MIN").LowLimit.ValueToString());
            this.vpiMaxLimit = Convert.ToDouble(testSpec.GetParamLimit("TC_MZ_VPI_CAL_LIMIT_MAX").HighLimit.ValueToString());
            this.mzVleftMinQuadLimit = Convert.ToDouble(testSpec.GetParamLimit("CH_MZ_BIAS_L_QUAD_V").LowLimit.ValueToString());
            this.mzVleftMaxQuadLimit = Convert.ToDouble(testSpec.GetParamLimit("CH_MZ_BIAS_L_QUAD_V").HighLimit.ValueToString());
            this.mzImbLeftMinLimit_mA = Convert.ToDouble(testSpec.GetParamLimit("CH_MZ_CTRL_L_I").LowLimit.ValueToString());
            this.mzImbLeftMaxLimit_mA = Convert.ToDouble(testSpec.GetParamLimit("CH_MZ_CTRL_L_I").HighLimit.ValueToString());
            this.mzImbRightMinLimit_mA = Convert.ToDouble(testSpec.GetParamLimit("CH_MZ_CTRL_R_I").LowLimit.ValueToString());
            this.mzImbRightMaxLimit_mA = Convert.ToDouble(testSpec.GetParamLimit("CH_MZ_CTRL_R_I").HighLimit.ValueToString());
            //this.maxArmBias_v = Convert.ToDouble(testSpec.GetParamLimit("TC_MZ_MAX_BIAS_V").LowLimit.ValueToString());  // -5V
            this.maxArmBias_v = -5;

            this.mzInitFixedModBias_volt = configData.ReadDouble("MZInitFixedModBias_volt");
            this.mzInitSweepMaxAbs_V = configData.ReadDouble("MZInitSweepMaxAbs_V");

            // details to go into our filenames
            this.MzFileDirectory = configData.ReadString("MzFileDirectory");
            this.dutSerialNbr = configData.ReadString("DutSerialNbr");

            // MZ scan setup data
            double[] MZFixedModBias_volt_FirstChannel = configData.ReadDoubleArray("MZFixedModBias_volt_FirstChannel");
            double[] MZFixedModBias_volt_LastChannel = configData.ReadDoubleArray("MZFixedModBias_volt_LastChannel");
            this.MzCtrlINominal_A = configData.ReadDouble("MzCtrlINominal_mA") / 1000;
            this.NumberPoints = configData.ReadSint32("NumberOfPoints");
            this.MZTapBias_V = configData.ReadDouble("MZTapBias_V");
            
            if (mzInstrs.InlineTapOnline)
                this.tapInline_V = configData.ReadDouble("MZInlineTapBias_V");

            #endregion

            // Initialise GUI
            engine.GuiShow();
            engine.GuiToFront();

            #region setup for sweep

            // setup our private data 
            this.mzDriverUtils = new IlMzDriverUtils(mzInstrs);

            // Setup instruments for sweep
            TcmzMzSweep_Config mzConfig = new TcmzMzSweep_Config(configData);

            if (!engine.IsSimulation)
                mzDriverUtils.CleanupAfterSweep();

            double mzMaxBias_V = mzConfig.MzMaxBias_V;
            IlMzDriverUtils.MaxModBias_V = mzConfig.MzMaxBias_V;
            double fixedModBias_V = mzConfig.MZInitFixedModBias_volt;
            double imbalance_A = mzConfig.MzCtrlINominal_mA / 1000;
            double sweepMaxAbs_V = mzConfig.MZInitSweepMaxAbs_V;
            sweepMaxAbs_V = Math.Abs(sweepMaxAbs_V);
            double sweepStop_V = 0.0;
            double sweepStart_V = -sweepMaxAbs_V;
            double stepSize_V = mzConfig.MZInitSweepStepSize_mV / 1000;
            int nbrPoints = (int)(1 + Math.Abs(sweepStart_V - sweepStop_V) / stepSize_V);
            double tapBias_V = mzConfig.MZTapBias_V;
            string fileDirectory = mzConfig.MzFileDirectory;

            double currentCompliance_A = mzConfig.MZCurrentCompliance_A;
            double currentRange_A = mzConfig.MZCurrentRange_A;
            double voltageCompliance_V = mzConfig.MZVoltageCompliance_V;
            double voltageRange_V = mzConfig.MZVoltageRange_V;
            int numberOfAverages = mzConfig.MzInitNumberOfAverages;
            double integrationRate = mzConfig.MZIntegrationRate;
            double sourceMeasureDelay = mzConfig.MZSourceMeasureDelay_s;
            double powerMeterAveragingTime = mzConfig.MZPowerMeterAveragingTime_s;

            engine.SendToGui("Preparing instruments for sweep");
            if (!engine.IsSimulation)
            {
                mzDriverUtils.InitialiseMZArms(currentCompliance_A, currentRange_A);

                mzDriverUtils.InitialiseImbalanceArms(voltageCompliance_V, voltageRange_V);
                mzDriverUtils.InitialiseTaps(currentCompliance_A, currentRange_A);
                mzDriverUtils.SetMeasurementAccuracy(numberOfAverages, integrationRate, sourceMeasureDelay, powerMeterAveragingTime);
                mzInstrs.PowerMeter.Mode = Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.MeterMode.Absolute_mW;

                // set power meter range to its initial range before any sweep 
                mzInstrs.PowerMeter.Range = configData.ReadDouble("OpmRangeInit_mW");
            }
            #endregion

            // measure at first and last channels
            initialOffsetSelected = false;
            int[] chansToMeas = new int[] { 0, ituChannels.Length - 1 };
            List<MzSweepDataItuChannel> mzSweepData = new List<MzSweepDataItuChannel>();

            double tuneISOAPowerSlope = configData.ReadDouble("TuneISOAPowerSlope");
            double tuneOpticalPowerTolerance_mW = configData.ReadDouble("TuneOpticalPowerToleranceInmW");
            double TargetFibrePower_dBm = configData.ReadDouble("TargetFibrePower_dBm");

            bool errorOccurred = false;
            string errorInformation = string.Empty;
         
            // Set up the Vcm bias depth data for the first channel
            this.MZCommonModeBiasDepth_v = MZFixedModBias_volt_FirstChannel;

            foreach (int channelIndex in chansToMeas)
            {
                MzSweepDataItuChannel mzDataItu = measureMzAtFrequency(
                    engine, 
                    mzInstrs, 
                    ituChannels[channelIndex],
                    tuneISOAPowerSlope, 
                    tuneOpticalPowerTolerance_mW, 
                    TargetFibrePower_dBm,
                    configData,
                    out errorOccurred, 
                    out errorInformation );

                mzSweepData.Add(mzDataItu);

                if (errorOccurred)
                    break;

                // Update the Vcm bias settings for the next channel.
                this.MZCommonModeBiasDepth_v = MZFixedModBias_volt_LastChannel;
            }

            // Clean up
            if (!engine.IsSimulation)
            {
                mzInstrs.PowerMeter.EnableLogging = false;
                mzInstrs.PowerMeter.EnableInputTrigger(false);
            }

            string mzSweepDataResultsFile;
            string mzSweepDataZipFile;
            bool allSweepsPassed;

            processMzSweepData(
                mzSweepData, 
                out mzSweepDataResultsFile, 
                out mzSweepDataZipFile,
                out allSweepsPassed);

            // return the data
            DatumList returnData = new DatumList();
            returnData.AddFileLink("MzSweepDataZipFile", mzSweepDataZipFile);
            returnData.AddFileLink("MzSweepDataResultsFile", mzSweepDataResultsFile);
            returnData.AddSint32("AllSweepsPassed", allSweepsPassed ? 1 : 0);
            returnData.AddReference("MzSweepData", mzSweepData);
            returnData.AddBool("IsError", errorOccurred);

            if (errorOccurred)
                returnData.AddString("ErrorInformation", errorInformation);

            return returnData;
        }

        /// <summary>
        /// Characterise MZ @ the specified ITU frequency
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="mzInstrs"></param>
        /// <param name="dsdbrSetup"></param>
        /// <param name="ISOAPowerSlope"></param>
        /// <param name="opticalPowerTolerance_mW"></param>
        /// <param name="TargetFibrePower_dBm"></param>
        /// <param name="configData"></param>
        /// <param name="isError"></param>
        /// <param name="errorInformation"></param>
        /// <returns></returns>
        private MzSweepDataItuChannel measureMzAtFrequency(
            ITestEngine engine, 
            IlMzInstruments mzInstrs, 
            DsdbrChannelData dsdbrSetup,
            double ISOAPowerSlope, 
            double opticalPowerTolerance_mW, 
            double TargetFibrePower_dBm, 
            DatumList configData,
            out bool isError, 
            out string errorInformation)
        {
            //initialise [out]-parameter
            isError = false;
            errorInformation = string.Empty;

            // Set Vpi and Vcm limits in analysis wrapper 
            MzAnalysisWrapper.vpiMinLimit = this.vpiMinLimit;
            MzAnalysisWrapper.vpiMaxLimit = this.vpiMaxLimit;
            MzAnalysisWrapper.vcmMinLimit = this.vcmMinLimit;
            MzAnalysisWrapper.vcmMaxLimit = this.vcmMaxLimit;

            // setup the DSDBR
            if (!engine.IsSimulation)
                DsdbrUtils.SetDsdbrCurrents_mA(dsdbrSetup.Setup);
            int chanNbr = dsdbrSetup.ItuChannelIndex;

            // Cache the WL in mzUtils so that we can calibrate power after the sweep.
            int numberOfAverages = configData.ReadSint32("MZInitNumberOfAverages");
            double integrationRate = configData.ReadDouble("MZIntegrationRate");
            double sourceMeasureDelay = configData.ReadDouble("MZSourceMeasureDelay_s");
            double powerMeterAveragingTime = configData.ReadDouble("MZPowerMeterAveragingTime_s");

            double wavelength;
            double power_mW;
            if (!engine.IsSimulation)
            {
                wavelength = Measurements.ReadWavelength_nm();
                mzInstrs.PowerMeter.Wavelength_nm = wavelength;

                // And apply WL to power meter
                power_mW = mzInstrs.PowerMeter.ReadPower();
            }

            double iImbLeft_mA = this.mzImbLeftIdeal_mA;
            double iImbRight_mA = this.mzImbRightIdeal_mA;

            bool redoVcmSweeps = false;
            bool adjustImbCurrents = true;
            MzSweepDataItuChannel mzItuChanData;
                        
            do
            {
                mzItuChanData = new MzSweepDataItuChannel();
                mzItuChanData.ItuChannelIndex = chanNbr;
                // loop through the common mode bias voltages

                if (adjustImbCurrents)
                {
                    #region we use the imbalance currents to set the quadrature point close to zero
                   
                    string preErrorInfo = "";
                    
                    double newLeftBias_V = configData.ReadDouble("LeftBias_V_LI_Sweep");
                    double newRightBias_V = configData.ReadDouble("RightBias_V_LI_Sweep"); // Pa006690 & Pa007371 at -2.5V, others at 0V
                    
                    engine.SendToGui("Running Differential Imbalance sweep on channel " + chanNbr.ToString() );

                    ILMZSweepData diffIsweep = new ILMZSweepData();
                    if (!engine.IsSimulation)
                    {
                        diffIsweep = mzDriverUtils.ImbArm_DifferentialSweep(
                            newLeftBias_V,
                            newRightBias_V,
                            0.0,
                            2 * MzCtrlINominal_A,
                            NumberPoints,
                            tapInline_V,
                            MZTapBias_V);
                    }
                    #region offline debug
                    else
                    {
                        // Load plot data for offline debugging
                        engine.SendToGui("Loading debug data from file");

                        diffIsweep.Type = SweepType.DifferentialCurrent;
                        diffIsweep.SrcMeter = SourceMeter.LeftImbArm;

                        string fileName = string.Format("{0}\\Offline\\Debug_MZCR_Ch{1}_DiffLI.csv", MzFileDirectory, chanNbr);

                        string searchPattern = string.Format("MZCR_Ch{0}_DiffLI*.csv", chanNbr);
                        string[] mzFiles = Directory.GetFiles(MzFileDirectory, searchPattern, SearchOption.AllDirectories);
                        if (mzFiles.Length > 0)
                            fileName = mzFiles[0];

                        MzRawData rawData = MzSweepFileWriter.ReadPlotData(fileName);

                        for (int column = 0; column < rawData.plotData.Length; column++)
                        {
                            ILMZSweepDataType sweepType = (ILMZSweepDataType)Enum.Parse(typeof(ILMZSweepDataType), rawData.header[column]);
                            double[] plotArray = (double[])rawData.plotData[column].ToArray(typeof(double));
                            diffIsweep.Sweeps.Add(sweepType, plotArray);                         
                        }
                    }
                    #endregion

                    string sweepFilePrefix = string.Format("MZCR_Ch{0}_DiffLI_",chanNbr );
                    string diffLIFileName;
                    recordSweepData(diffIsweep, sweepFilePrefix, out diffLIFileName);
                    LiSweepFile.Add(diffLIFileName);

                    // Display the plot data
                    engine.SendToGui(diffIsweep);
                    
                    // analyse the data
                    MzAnalysisWrapper.MzAnalysisResults mzAnlyDiffI = MzAnalysisWrapper.ZeroChirp_DifferentialSweep(
                        diffIsweep, 
                        MzCtrlINominal_A);

                    // Add a nice arrow to point at the quadrature point
                    NPlot.PointD marker = new NPlot.PointD(mzAnlyDiffI.Quad_SrcL - mzAnlyDiffI.Quad_SrcR, mzAnlyDiffI.PowerAtMax_dBm - 3);
                    engine.SendToGui(marker);

                    NPlot.TextItem markerText = new NPlot.TextItem(new NPlot.PointD(marker.X, marker.Y), "Quadrature");
                    engine.SendToGui(markerText);

                    if (System.Diagnostics.Debugger.IsAttached)
                        System.Threading.Thread.Sleep(2000);

                    // calculate imbalance currents. 
                    // Quadrature point on positive slope
                    iImbLeft_mA = mzAnlyDiffI.Quad_SrcL * 1000;
                    iImbRight_mA = mzAnlyDiffI.Quad_SrcR * 1000;
                    
                    if (iImbLeft_mA < this.mzImbLeftMinLimit_mA || iImbLeft_mA > this.mzImbLeftMaxLimit_mA ||
                        iImbRight_mA < this.mzImbRightMinLimit_mA || iImbRight_mA > this.mzImbRightMaxLimit_mA)
                    {
                        // We have used the maximum amount of MZ modulator bias and the maximum amount of 
                        // MZ imbalance bias and we still cannot meet the spec. 
                        // Stop now. This device is unuseable with the present limits.

                        // 2007-11-29: Ken.Wu: Imbalance too high issue. Bug-fix 1#.
                        //      Just record error information into PCAS.
                        engine.ShowContinueUserQuery("MZ imbalance is too high to be useable within the limits, but you can continue test. ");
                        errorInformation += preErrorInfo;
                        //isError = true;
                       // break;
                    }                  

                    redoVcmSweeps = false;
                    adjustImbCurrents = false;//only allow adjusting imblance currents once
                   
                    #endregion

                    #region do power leveling at Imb_Peak after imb sweep

                    // Jack.Zhang  2010-05-14
                    // do power leveling here to ensure the mz is characterised at the right fibre power
                    // that can get the right Vpi, Vcm, & Peak bias
                    engine.SendToGui(" Running power leveling on channel " + chanNbr.ToString());
                    double powerOffset_dB = configData.ReadDouble("PowerlevelingOffset_dB");
                    double tuneISOAPowerSlope = configData.ReadDouble("TuneISOAPowerSlope");
                    double tuneOpticalPowerTolerance_mW = configData.ReadDouble("TuneOpticalPowerToleranceInmW");
                    double targetPowerDbm = configData.ReadDouble("TargetFibrePower_dBm");
                    double powerOffset = configData.ReadDouble("PowerlevelingOffset_dB");

                    // Set imbalance arms to get max power output
                    if (!engine.IsSimulation)
                    {
                        mzDriverUtils.SetCurrent(SourceMeter.LeftImbArm, mzAnlyDiffI.Max_SrcL);
                        mzDriverUtils.SetCurrent(SourceMeter.RightImbArm, mzAnlyDiffI.Max_SrcR);

                        // power level
                        SoaPowerLevel.PowerLevelWithAdaptiveSlope(
                            Alg_PowConvert_dB.Convert_dBmtomW(targetPowerDbm + powerOffset),
                            tuneOpticalPowerTolerance_mW,
                            tuneISOAPowerSlope);

                        // Set imbalance arms to operating point
                        mzDriverUtils.SetCurrent(SourceMeter.LeftImbArm, iImbLeft_mA / 1000);
                        mzDriverUtils.SetCurrent(SourceMeter.RightImbArm, iImbRight_mA / 1000);
                    }
                    #endregion

                }                

            } while (redoVcmSweeps);

            mzImbLeftIdeal_mA = iImbLeft_mA;
            mzImbRightIdeal_mA = iImbRight_mA;
            if (!engine.IsSimulation)
            {
                mzDriverUtils.SetMeasurementAccuracy(numberOfAverages, integrationRate, sourceMeasureDelay, powerMeterAveragingTime);
                mzInstrs.PowerMeter.Mode = Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.MeterMode.Absolute_mW;
            }

            /* For the ILMZ we can't easily use the single ended performance to estimate
             * the range required for differential drive.
             * Just sweep twice and interpolate. 
             */

            MZCommonModeBiasDepth_v[0] = Math.Max(vcmMinLimit, MZCommonModeBiasDepth_v[0]);
            MZCommonModeBiasDepth_v[1] = Math.Max(vcmMinLimit, MZCommonModeBiasDepth_v[1]);
            MZCommonModeBiasDepth_v[0] = Math.Min(vcmMaxLimit, MZCommonModeBiasDepth_v[0]);
            MZCommonModeBiasDepth_v[1] = Math.Min(vcmMaxLimit, MZCommonModeBiasDepth_v[1]);

             redoVcmSweeps = false;
             adjustImbCurrents = false;// Now we use fixed imbalance currents, thus set adjustImbCurrents = false


            mzItuChanData = new MzSweepDataItuChannel();
            mzItuChanData.ItuChannelIndex = chanNbr;

            // loop through the common mode bias voltages
            for (int vcmIndex = 0; vcmIndex < MZCommonModeBiasDepth_v.Length; vcmIndex++)
            {
                MzItuAtVcmData vcmData = measureMzAtVcmLevel(
                    engine,
                    chanNbr, 
                    vcmIndex, 
                    iImbLeft_mA, 
                    iImbRight_mA);

                mzItuChanData.VcmData.Add(vcmData);
            }

            #region Check Vcm values were suitable
            // Vcm vs Vpi is not quite linear in ILMZ
            // Check that our two values of Vpi are suitably close to target.

            double vpi0 = mzItuChanData.VcmData[0].GetValueDouble(MzItuAtVcmMeasurements.Vpi_V);
            double vpi1 = mzItuChanData.VcmData[1].GetValueDouble(MzItuAtVcmMeasurements.Vpi_V);
            double vpi0_Difference = Math.Abs(this.vpiMaxLimit - vpi0);
            double vpi1_Difference = Math.Abs(this.vpiMaxLimit - vpi1);

            //bool repeatVcm0 = false;
            //bool repeatVcm1 = false;
            
            //if (vpi0_Difference > this.vpiApproxLinearRange_V && vpi1_Difference > this.vpiApproxLinearRange_V)
            //{
            //    // Both are too far away.
            //    // Don't repeat them both as we'd be doing them at the same recalculated Vcm
            //    repeatVcm0 = vpi0_Difference > vpi1_Difference;
            //    repeatVcm1 = !repeatVcm0;
            //}
            //else
            //{
            //    // Repeat one measurement as necessary
            //    repeatVcm0 = vpi0_Difference > this.vpiApproxLinearRange_V;
            //    repeatVcm1 = vpi1_Difference > this.vpiApproxLinearRange_V;
            //}
            //Echo remed above block,

            bool repeatVcm0 = true;
            if (repeatVcm0)
            {
                this.MZCommonModeBiasDepth_v[0] = CalculateVcmForMaxVpi(mzItuChanData, new double[] { vpi0, vpi1 }, vpiMaxLimit);

                // Repeat and update this measurement
                MzItuAtVcmData thirdSweepData = new MzItuAtVcmData(MZCommonModeBiasDepth_v[0]);
                //mzItuChanData.VcmData[0] = measureMzAtVcmLevel(
                thirdSweepData = measureMzAtVcmLevel(
                    engine,
                    chanNbr,
                    0,
                    iImbLeft_mA,
                    iImbRight_mA);
                mzItuChanData.VcmData.Add(thirdSweepData);
            }

            //if (repeatVcm1)
            //{
            //    this.MZCommonModeBiasDepth_v[1] = CalculateVcmForMaxVpi(mzItuChanData, new double[] { vpi0, vpi1 }, vpiMaxLimit);

            //    // Repeat and update this measurement
            //    mzItuChanData.VcmData[1] = measureMzAtVcmLevel(
            //        engine,
            //        chanNbr,
            //        1,
            //        iImbLeft_mA,
            //        iImbRight_mA);
            //}
            #endregion

#warning Do we need to power level again here ? If grossly out we'll need to repeat Vcm sweeps


           
            return mzItuChanData;
        }



        /// <summary>
        /// Characterise MZ at a given common mode bias voltage
        /// </summary>
        /// <param name="engine">test engine</param>
        /// <param name="ituChannelIndex">channel index</param>
        /// <param name="vcmIndex">Vcm Index</param>
        /// <param name="iImbLeft">left imb current</param>
        /// <param name="iImbRight">right imb current</param>
        /// <returns>MzData</returns>
        private MzItuAtVcmData measureMzAtVcmLevel(ITestEngine engine, int ituChannelIndex, int vcmIndex, double iImbLeft, double iImbRight)
        {
            // find the common mode voltage we are using
            double vcm_V = MZCommonModeBiasDepth_v[vcmIndex];

            double iSOA_mA = 0;
            if (!engine.IsSimulation)
                iSOA_mA = DsdbrUtils.ReadSectionCurrent_mA(DSDBRSection.SOA);

            // initialise the data object (including the limits)
            MzItuAtVcmData vcmData = new MzItuAtVcmData(vcm_V, iSOA_mA);

            // store imbalance biases
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.IImbQuadL_mA, iImbLeft);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.IImbQuadR_mA, iImbRight);

            // If Vcm is high we need to restrict the max bias level on either arm to its max limit by just performing the middle section of the sweep.
            double startBias = 2 * vcm_V;
            double stopBias = 0;
            if (startBias < maxArmBias_v)
            {
                double offset = startBias - maxArmBias_v;
                startBias = maxArmBias_v;
                stopBias = offset;
            }

            // diff voltage sweep - assume VCM is negative. Apply calculated IQuad to the imbalance electrodes
            engine.SendToGui("Running Differential Modulator sweep on channel " + ituChannelIndex.ToString() + " from 0V to " + vcm_V.ToString() + "V");
            bool dataOk = true;
            double minLevel_dBm = -60;
            ILMZSweepData diffVsweep = new ILMZSweepData();
            do
            {
                if (!engine.IsSimulation)
                {
                    double a = vcmData.GetValueDouble(MzItuAtVcmMeasurements.IImbQuadL_mA);
                    double b = vcmData.GetValueDouble(MzItuAtVcmMeasurements.IImbQuadR_mA);
                    diffVsweep = mzDriverUtils.ModArm_DifferentialSweep
                        (vcmData.GetValueDouble(MzItuAtVcmMeasurements.IImbQuadL_mA) / 1000,
                         vcmData.GetValueDouble(MzItuAtVcmMeasurements.IImbQuadR_mA) / 1000,
                         startBias, stopBias, NumberPoints, tapInline_V, MZTapBias_V);
                }
                #region offline debug
                else
                {
                    // Load plot data for offline debugging
                    engine.SendToGui("Loading debug data from file");

                    diffVsweep.Type = SweepType.DifferentialVoltage;
                    diffVsweep.SrcMeter = SourceMeter.LeftModArm;

                    string fileName = string.Format("{0}\\Offline\\Debug_MZCR_CH{1}_DILV_VCM{2}.csv", MzFileDirectory, ituChannelIndex, vcmIndex);

                    string searchPattern = string.Format("MZCR_CH{0}_DILV_VCM{1}*.csv", ituChannelIndex, vcmIndex);
                    string[] mzFiles = Directory.GetFiles(MzFileDirectory, searchPattern, SearchOption.AllDirectories);
                    if (mzFiles.Length > 0)
                        fileName = mzFiles[0];

                    MzRawData rawData = MzSweepFileWriter.ReadPlotData(fileName);

                    for (int column = 0; column < rawData.plotData.Length; column++)
                    {
                        ILMZSweepDataType sweepType = (ILMZSweepDataType)Enum.Parse(typeof(ILMZSweepDataType), rawData.header[column]);
                        double[] plotArray = (double[])rawData.plotData[column].ToArray(typeof(double));
                        diffVsweep.Sweeps.Add(sweepType, plotArray);
                    }
                }
                #endregion

                string sweepFileName;
                string fileNamePrefix = string.Format("MZCR_CH{0}_DILV_VCM{1}_", ituChannelIndex, vcmIndex);
                recordSweepData(diffVsweep, fileNamePrefix, out sweepFileName);

                // If overrange run again
                if (MzAnalysisWrapper.CheckForOverrange(diffVsweep.Sweeps[ILMZSweepDataType.FibrePower_mW]))
                {
                    dataOk = false;
                    if (double.IsNaN(mzDriverUtils.MzInstrs.PowerMeter.Range)) // auto range?
                        mzDriverUtils.MzInstrs.PowerMeter.Range = 1;
                    else
                        mzDriverUtils.MzInstrs.PowerMeter.Range = mzDriverUtils.MzInstrs.PowerMeter.Range * 10;
                }
                else
                {
                    // if underrange fix the data and continue
                    dataOk = true;
                    if (MzAnalysisWrapper.CheckForUnderrange(diffVsweep.Sweeps[ILMZSweepDataType.FibrePower_mW]))
                    {
                        diffVsweep.Sweeps[ILMZSweepDataType.FibrePower_mW] =
                            MzAnalysisWrapper.FixUnderRangeData(diffVsweep.Sweeps[ILMZSweepDataType.FibrePower_mW],
                            minLevel_dBm);
                    }
                }
            } while (!dataOk);
        
            // Display data
            engine.SendToGui(diffVsweep);

            // write data to file
            string diffVsweepFileNameStem = string.Format("MzDiffLV_CH{0}_VCM{1}", ituChannelIndex, vcmIndex);
            vcmData.DiffLVSweepFile = Util_GenerateFileName.GenWithTimestamp
                (MzFileDirectory, diffVsweepFileNameStem, dutSerialNbr, "csv");

            MzSweepFileWriter.WriteSweepData(vcmData.DiffLVSweepFile, diffVsweep,
                new ILMZSweepDataType[] 
                {ILMZSweepDataType.LeftArmModBias_V,
                 ILMZSweepDataType.RightArmModBias_V,
                 ILMZSweepDataType.FibrePower_mW,
                 ILMZSweepDataType.TapComplementary_mA});

            // analyse the data.
            //
            // The first point needs to be close to zero. After that 
            // search for quadrature close to initialVcmOffset 
            // to help ensure that we pick the same slope each time.
            MzAnalysisWrapper.MzAnalysisResults mzAnlyDiffV =
                MzAnalysisWrapper.ZeroChirp_DifferentialSweep(diffVsweep, vcm_V, initialVcmOffset);

            // Add a nice arrow to point at the quadrature point
            NPlot.PointD marker = new NPlot.PointD(mzAnlyDiffV.Quad_SrcL - mzAnlyDiffV.Quad_SrcR, mzAnlyDiffV.PowerAtMax_dBm - 3);
            engine.SendToGui(marker);

            // Add some information to help technicians & engineers
            NPlot.TextItem markerText = new NPlot.TextItem(new NPlot.PointD(marker.X, mzAnlyDiffV.PowerAtMin_dBm + 5), "Vpi = " + mzAnlyDiffV.MzAnalysis.Vpi.ToString("0.0"));
            engine.SendToGui(markerText);

            NPlot.VerticalLine verticalLine = new NPlot.VerticalLine(mzAnlyDiffV.MzAnalysis.VoltageAtMax, System.Drawing.Color.IndianRed);
            engine.SendToGui(verticalLine);

            verticalLine = new NPlot.VerticalLine(mzAnlyDiffV.MzAnalysis.VoltageAtMin, System.Drawing.Color.IndianRed);
            engine.SendToGui(verticalLine);

            if (System.Diagnostics.Debugger.IsAttached)
                System.Threading.Thread.Sleep(2000);

            vcmData.SetValueDouble(MzItuAtVcmMeasurements.VquadL_V, mzAnlyDiffV.Imb_SrcL);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.VquadR_V, mzAnlyDiffV.Imb_SrcR);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.VminL_V, mzAnlyDiffV.Min_SrcL);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.VminR_V, mzAnlyDiffV.Min_SrcR);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.VpeakL_V, mzAnlyDiffV.Max_SrcL);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.VpeakR_V, mzAnlyDiffV.Max_SrcR);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.Vpi_V, mzAnlyDiffV.Vpi);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.ER_dB, mzAnlyDiffV.ExtinctionRatio_dB);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.Isoa_mA, iSOA_mA);

            // Choose subsequent slopes to be close to the first selected slope,
            if (!initialOffsetSelected)
                initialVcmOffset = (vcmData.GetValueDouble(MzItuAtVcmMeasurements.VquadL_V )
                    - vcmData.GetValueDouble(MzItuAtVcmMeasurements.VquadR_V )) / 2;

            initialOffsetSelected = true;

            return vcmData;
        }

        /// <summary>
        /// Unse linear interpolation to calculate Vcm for a target Vpi
        /// </summary>
        /// <param name="mzItuChanData"></param>
        /// <param name="vpi"></param>
        /// <returns></returns>
        private double CalculateVcmForMaxVpi(MzSweepDataItuChannel mzItuChanData, double[] vpi, double vpiTarget)
        {
            // Vpi vs Vcm
            double vpiVcmGradient = (vpi[0] - vpi[1]) / (mzItuChanData.VcmData[0].Vcm_V - mzItuChanData.VcmData[1].Vcm_V);
            // Offset required is Y at X=0, so swap X & Y and invert gradient
            double newVcm_V = EstimateXAtY.Calculate(mzItuChanData.VcmData[0].Vcm_V, vpi[0], vpiVcmGradient, vpiTarget);         

            // To avoid driving the device too hard we should clamp the calculated Vcm against the limits
            newVcm_V = Math.Max(newVcm_V, this.vcmMinLimit);
            newVcm_V = Math.Min(newVcm_V, this.vcmMaxLimit);

            return newVcm_V;
        }

        /// <summary>
        /// Add a file to our zip file
        /// </summary>
        /// <param name="zipStream">Zip Output Stream</param>
        /// <param name="file">file name to add (relative path)</param>
        /// <param name="buffer">byte buffer to read data from the file to</param>
        private void addZipFile(ZipOutputStream zipStream, string file, byte[] buffer)
        {
            if (!File.Exists(file)) 
                return;

            ZipEntry entry = new ZipEntry(Path.GetFileName(file));
            entry.DateTime = DateTime.Now;
            zipStream.PutNextEntry(entry);

            using (FileStream fs = File.OpenRead(file))
            {
                // Using a fixed size buffer here makes no noticeable difference for output
                // but keeps a lid on memory usage.
                int sourceBytes;
                do
                {
                    sourceBytes = fs.Read(buffer, 0, buffer.Length);
                    zipStream.Write(buffer, 0, sourceBytes);
                } while (sourceBytes > 0);
            }
            zipStream.Flush();
        }

        /// <summary>
        /// Process the MZ Sweep results, writing out a zip of the sweeps, a summary CSV file and
        /// returning a flag to say if all sweeps passed or not
        /// </summary>
        /// <param name="mzSweepData">Sweep data to be processed</param>
        /// <param name="mzSweepDataResultsFile">Data results file</param>
        /// <param name="mzSweepDataZipFile">Zip sweep file</param>
        /// <param name="sweepsAllPassed">True if all sweeps passed, false if any failed</param>
        private void processMzSweepData(List<MzSweepDataItuChannel> mzSweepData,
            out string mzSweepDataResultsFile, out string mzSweepDataZipFile,
            out bool sweepsAllPassed)
        {
            mzSweepDataZipFile = Util_GenerateFileName.GenWithTimestamp(
                MzFileDirectory, 
                "MzSweepDataZipFile", 
                dutSerialNbr,
                "zip");

            using (ZipOutputStream zipStream = new ZipOutputStream(File.Create(mzSweepDataZipFile)))
            {
                zipStream.SetLevel(9); // 0 - store only to 9 - means best compression

                byte[] buffer = new byte[4096]; // byte buffer to zip into
                foreach (MzSweepDataItuChannel ituChan in mzSweepData)
                {
                    foreach (MzItuAtVcmData vcmMeas in ituChan.VcmData)
                    {
                        addZipFile(zipStream, vcmMeas.DiffLISweepFile, buffer);
                        addZipFile(zipStream, vcmMeas.DiffLVSweepFile, buffer);
                    }
                }
                foreach (string Lifile in LiSweepFile)
                {
                    addZipFile(zipStream, Lifile, buffer);
                }
            }

            sweepsAllPassed = true;

            // write the summary file and check if it all passed
            mzSweepDataResultsFile = Util_GenerateFileName.GenWithTimestamp
                (MzFileDirectory, "MzSweepDataResultsFile", dutSerialNbr, "csv");

            int channel;
            using (StreamWriter writer = new StreamWriter(mzSweepDataResultsFile))
            {
                // file header
                string defaultFileHeader = "ITUChannel,Vcm_V,Status,IImbL_mA,IImbR_mA,VquadL_V,VquadR_V,VpeakL_V,VpeakR_V,VminL_V,VminR_V,Vpi_V, ER_dB";
                StringBuilder fileHeader = new StringBuilder();
                bool fileHeaderOK = false;
                fileHeader.Append("ITUChannel,Vcm_V,Status");

                // file contents
                List<string> fileContents = new List<string>();

                foreach (MzSweepDataItuChannel ituChan in mzSweepData)
                {
                    channel = ituChan.ItuChannelIndex;
                    foreach (MzItuAtVcmData vcmData in ituChan.VcmData)
                    {
                        // fail check
                        if (vcmData.OverallStatus == PassFail.Fail) sweepsAllPassed = false;

                        // each line
                        StringBuilder aLine = new StringBuilder();
                        aLine.Append(channel);
                        aLine.Append(",");
                        aLine.Append(vcmData.Vcm_V);
                        aLine.Append(",");
                        aLine.Append(vcmData.OverallStatus.ToString());

                        // for each parameter...
                        foreach (string nameAsStr in Enum.GetNames(typeof(MzItuAtVcmMeasurements)))
                        {
                            MzItuAtVcmMeasurements parameter = (MzItuAtVcmMeasurements)Enum.Parse(typeof(MzItuAtVcmMeasurements), nameAsStr);

                            // build file header
                            if (!fileHeaderOK)
                            {
                                fileHeader.Append(",");
                                fileHeader.Append(nameAsStr);
                                fileHeader.Append("[");
                                fileHeader.Append(vcmData.GetMeasuredDataLowLimit(parameter));
                                fileHeader.Append(";");
                                fileHeader.Append(vcmData.GetMeasuredDataHighLimit(parameter));
                                fileHeader.Append("]");
                            }

                            // build the line
                            aLine.Append(",");
                            if (vcmData.IsTested(parameter))
                                aLine.Append(vcmData.GetValueDouble(parameter));
                        }

                        if (!fileHeaderOK)
                            fileHeaderOK = true;

                        fileContents.Add(aLine.ToString());
                    }
                }

                // write file header to file
                if (!fileHeaderOK)
                {
                    writer.WriteLine(defaultFileHeader);
                }
                else
                {
                    writer.WriteLine(fileHeader.ToString());
                }

                // writer file contents to file
                foreach (string row in fileContents)
                {
                    writer.WriteLine(row);
                }
            }
        }
        /// <summary>
        /// write arm sweep data to file 
        /// </summary>
        /// <param name="sweepData"> sweep data</param>
        /// <param name="file_prefix"> file name prefix </param>
        /// <param name="sweepFileName"> file name that the sweep data save as </param>
        private void recordSweepData(ILMZSweepData sweepData, 
                                string file_prefix, out string sweepFileName)
        {
            Dictionary<ILMZSweepDataType, double[]> sweepDataListTemp = sweepData.Sweeps;
            List<string> dataNameList = new List<string>();
            List<double[]> sweepDataList = new List<double[]>();
            foreach (ILMZSweepDataType dataName in sweepDataListTemp.Keys)
            {
                dataNameList.Add(dataName.ToString());
                sweepDataList.Add(sweepDataListTemp[dataName]);
            }

            double[][] sweepDataArray = sweepDataList.ToArray();
            string[] dataNameArray = dataNameList.ToArray();

            if ((sweepDataArray.Length > 0) && (dataNameArray[0].Length > 0))
            { }
            else
            {
                sweepFileName = "";
                return;
            }

            string mzSweepDataResultsFile = Util_GenerateFileName.GenWithTimestamp
                (MzFileDirectory, file_prefix, dutSerialNbr, "csv");

            using (StreamWriter writer = new StreamWriter(mzSweepDataResultsFile))
            {
                // file header
                StringBuilder fileHeader = new StringBuilder();

                for (int count = 0; count < dataNameArray.Length; count++)
                {
                    if (count != 0) fileHeader.Append(",");
                    fileHeader.Append(dataNameArray[count]);
                }

                // file contents
                List<string> fileContents = new List<string>();
                for (int irow = 0; irow < sweepDataArray[0].Length; irow++)
                {
                    StringBuilder aLine = new StringBuilder();
                    for (int icol = 0; icol < dataNameArray.Length; icol++)
                    {
                        if (icol != 0) aLine.Append(",");
                        aLine.Append(sweepDataArray[icol][irow].ToString());
                    }
                    fileContents.Add(aLine.ToString());
                }

                // write file header to file
                writer.WriteLine(fileHeader.ToString());

                // writer file contents to file
                foreach (string row in fileContents)
                {
                    writer.WriteLine(row);
                }
            }
            sweepFileName = mzSweepDataResultsFile;
        }

        /// <summary>
        /// Write a single MZ measurement to CSV file
        /// </summary>
        /// <param name="writer">Text Writer that is writing our file</param>
        /// <param name="vcmData">VCM data to write from</param>
        /// <param name="measurement">Which measurement to write?</param>
        private void writeMzMeas(TextWriter writer, MzItuAtVcmData vcmData, MzItuAtVcmMeasurements measurement)
        {
            string valueStr = "";
            if (vcmData.IsTested(measurement))
            {
                double d = vcmData.GetValueDouble(measurement);
                valueStr = d.ToString();
            }
            writer.Write(',');
            writer.Write(valueStr);
        }
        /// <summary>
        /// 
        /// </summary>
        public Type UserControl
        {
            get { return (typeof(IlmzMzCharacteriseGui)); }
        }

        #endregion
    }
}

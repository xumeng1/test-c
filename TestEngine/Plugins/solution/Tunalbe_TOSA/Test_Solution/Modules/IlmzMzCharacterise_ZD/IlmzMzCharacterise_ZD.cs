// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// IlmzMzCharacterise_ZD.cs
//
// Author: Paul.Annetts, Mark Fullalove 2007
// ILMZ GB version : Mark Fullalove 2011
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestSolution.IlmzCommonData;
using Bookham.TestSolution.IlmzCommonInstrs;
using Bookham.TestSolution.IlmzCommonUtils;
using Bookham.ToolKit.Mz;
using Bookham.TestLibrary.Algorithms;
using Bookham.TestSolution.ILMZFinalData;
using Bookham.TestEngine.Framework.Limits;
using Bookham.TestLibrary.Utilities;
using ICSharpCode.SharpZipLib.Zip;
using System.IO;
using Bookham.TestSolution.TestModules;
using Bookham.TestLibrary.Instruments;
using Bookham.TestLibrary.InstrTypes;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// TCMZ MZ Characterisation module - does MZ sweeps at various ITU channels
    /// 
    /// Zero chirp, differential sweep
    /// For ILMZ the imbalances and MZ biases are both set to the quadrature point on the positive slope
    /// 
    /// </summary>
    public class IlmzMzCharacterise_ZD : ITestModule
    {
        private IlMzDriverUtils mzDriverUtils;
        private double[] MZFixedModBias_volt;
        private double MzCtrlINominal_A;
        private double mzImbLeftIdeal_mA;
        private double mzImbRightIdeal_mA;
        private double mzImbLeftMinLimit_mA;
        private double mzImbLeftMaxLimit_mA;
        private double mzImbRightMinLimit_mA;
        private double mzImbRightMaxLimit_mA;
        private double mzInitFixedModBias_volt;
        private double mzInitSweepMaxAbs_V;
        private double mzVleftMinQuadLimit;
        private double mzVleftMaxQuadLimit;
        private double vcmMinLimit;
        private double vcmMaxLimit;
        private double vpiMinLimit;
        private double vpiMaxLimit;
        private double dcerMinLimit;
        private double dcerMaxLimit;
        private double vquadMinLimit;
        private double maxArmBias_v;
        private double mzVleftInset = 0.05;
        private double vpiApproxLinearRange_V = 1;
        private double initialVcmOffset = 0;
        private bool initialOffsetSelected;
        private double chan0Quad_Diff = 0;
        private double chan57Quad_Diff = 0;
        private double Margin_ER = 0.5;
        private double TargetVpiOffset = 0;
        private IInstType_DigiIOCollection OpticalSwitch = null;
        private double Vpi_offset = 0;

        private double mzFixedModBias_V_firstChan = 0;
        private double mzFixedModBias_V_middleChan = 0;
        private double mzFixedModBias_V_lastChan = 0;

        private int NumberPoints;
        private double MZTapBias_V;
        private double tapInline_V;
        private string MzFileDirectory;
        private string dutSerialNbr;
        private List<string> LiSweepFile=new List<string> ();
		private bool ArmSourceByAsic = true;
        double vpiCorrectionUpperAdjustDelta;
        private int MZSourceMeasureDelay_ms = 5;
        private int firstChOrLastCh;//indicate testing channel is first channel or last channel
        private bool isUseNegSlopeImb = false;

        #region ITestModule Members
        /// <summary>
        /// 
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="userType"></param>
        /// <param name="configData"></param>
        /// <param name="instruments"></param>
        /// <param name="chassis"></param>
        /// <param name="calData"></param>
        /// <param name="previousTestData"></param>
        /// <returns></returns>
        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, 
            DatumList configData, InstrumentCollection instruments,
            ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {

            #region read config
            // get MZ instruments
            IlMzInstruments mzInstrs = (IlMzInstruments)configData.ReadReference("MzInstruments");

			// Is this a Low cost testset ?
            double locker_tran_pot = 0;
            if (ArmSourceByAsic)
            {
                mzInstrs.FCU2Asic = (Inst_Fcu2Asic)instruments["FCU"];
                OpticalSwitch = (IInstType_DigiIOCollection)instruments["Optical_switch"];
                locker_tran_pot = configData.ReadDouble("locker_tran_pot");
                mzInstrs.ImbsSourceType = IlMzInstruments.EnumSourceType.Asic;
            }
            
            // get list of DSDBR channels
            DsdbrChannelData[] ituChannels = (DsdbrChannelData[])previousTestData.ReadReference("ItuChannels");

            // get test spec (for limit checking)
            Specification testSpec = (Specification)configData.ReadReference("TestSpec");
            MzItuAtVcmData.InitLookup(testSpec);

            double midChannelFrequency = configData.ReadDouble("MidChannelFrequency");

            this.vpiCorrectionUpperAdjustDelta = configData.ReadDouble("VpiCorrectionUpperAdjustDelta");

            this.mzImbLeftIdeal_mA = Convert.ToDouble(testSpec.GetParamLimit("TC_MZ_IMB_LEFT").LowLimit.ValueToString());
            this.mzImbRightIdeal_mA = Convert.ToDouble(testSpec.GetParamLimit("TC_MZ_IMB_RIGHT").LowLimit.ValueToString());
            this.vcmMinLimit = Convert.ToDouble(testSpec.GetParamLimit("TC_MZ_VCM_CAL_LIMIT_MIN").LowLimit.ValueToString());
            this.vcmMaxLimit = Convert.ToDouble(testSpec.GetParamLimit("TC_MZ_VCM_CAL_LIMIT_MAX").HighLimit.ValueToString());
            this.vpiMinLimit = Convert.ToDouble(testSpec.GetParamLimit("TC_MZ_VPI_CAL_LIMIT_MIN").LowLimit.ValueToString());
            this.vpiMaxLimit = Convert.ToDouble(testSpec.GetParamLimit("TC_MZ_VPI_CAL_LIMIT_MAX").HighLimit.ValueToString()) - 0.1;
            this.mzVleftMinQuadLimit = Convert.ToDouble(testSpec.GetParamLimit("CH_MZ_BIAS_L_QUAD_V").LowLimit.ValueToString());
            this.mzVleftMaxQuadLimit = Convert.ToDouble(testSpec.GetParamLimit("CH_MZ_BIAS_L_QUAD_V").HighLimit.ValueToString());
            this.mzImbLeftMinLimit_mA = ParamManager.Spec.CH_MZ_CTRL_L_I.Low;
            this.mzImbLeftMaxLimit_mA = ParamManager.Spec.CH_MZ_CTRL_L_I.High;
            this.mzImbRightMinLimit_mA = ParamManager.Spec.CH_MZ_CTRL_R_I.Low;
            this.mzImbRightMaxLimit_mA = ParamManager.Spec.CH_MZ_CTRL_R_I.High ;
            //this.maxArmBias_v = Convert.ToDouble(testSpec.GetParamLimit("TC_MZ_MAX_BIAS_V").LowLimit.ValueToString());  // -5V
            //this.maxArmBias_v = -6.3;
            //this.dcerMinLimit = Convert.ToDouble(testSpec.GetParamLimit("CH_MZ_ER").LowLimit.ValueToString()); ;
            //this.dcerMaxLimit = Convert.ToDouble(testSpec.GetParamLimit("CH_MZ_ER").HighLimit.ValueToString()); ;
            this.dcerMinLimit = configData.ReadDouble("MzCharacterise_MinER");
            this.dcerMaxLimit = configData.ReadDouble("MzCharacterise_MaxER");

            this.vquadMinLimit = Convert.ToDouble(testSpec.GetParamLimit("TC_MZ_QUAD_V_LIMIT_MIN").HighLimit.ValueToString()); ;
            this.TargetVpiOffset = configData.ReadDouble("TargetVpiOffset");
            this.Vpi_offset = configData.ReadDouble("Vpi_offset");//0.2

            this.mzInitFixedModBias_volt = configData.ReadDouble("MZInitFixedModBias_volt"); //should be -1.1V
            this.mzInitSweepMaxAbs_V = configData.ReadDouble("MZInitSweepMaxAbs_V"); //LeftMod sweep range 6.2
            
            this.isUseNegSlopeImb = configData.ReadBool("IsUseNegSlopeImbInZC");

            // details to go into our filenames
            this.MzFileDirectory = configData.ReadString("ResultDire");
            this.dutSerialNbr = configData.ReadString("DutSerialNbr");

            // MZ scan setup data
            //double[] MZFixedModBias_volt_FirstChannel = configData.ReadDoubleArray("MZFixedModBias_volt_FirstChannel");
            //double[] MZFixedModBias_volt_LastChannel = configData.ReadDoubleArray("MZFixedModBias_volt_LastChannel");
            this.mzFixedModBias_V_firstChan = configData.ReadDouble("MzFixedModBias_volt_firstChan");
            this.mzFixedModBias_V_middleChan = configData.ReadDouble("MzFixedModBias_volt_middleChan");
            this.mzFixedModBias_V_lastChan = configData.ReadDouble("MzFixedModBias_volt_lastChan");

            if (ParamManager.Conditions.IsThermalPhase)
            {
                this.MzCtrlINominal_A = configData.ReadDouble("MzCtrlINominal_thermal_mA") / 1000;
            }
            else
            {
                this.MzCtrlINominal_A = configData.ReadDouble("MzCtrlINominal_mA") / 1000;
            }
            this.NumberPoints = configData.ReadSint32("NumberOfPoints");
            this.MZTapBias_V = configData.ReadDouble("MZTapBias_V");
            this.MZSourceMeasureDelay_ms = configData.ReadSint32("MZSourceMeasureDelay_ms");
            if (mzInstrs.InlineTapOnline)
                this.tapInline_V = configData.ReadDouble("MZInlineTapBias_V");
			else
            {
                // belt and braces - this should be the value anyway by C# default.
                this.tapInline_V = 0.0;
            }

            #endregion

            // Initialise GUI
            engine.GuiShow();
            engine.GuiToFront();

            #region setup for sweep

            // setup our private data 
            this.mzDriverUtils = new IlMzDriverUtils(mzInstrs);
            mzDriverUtils.opticalSwitch = OpticalSwitch;
            mzDriverUtils.locker_port = int.Parse(locker_tran_pot.ToString());

            // Setup instruments for sweep
            TcmzMzSweep_Config mzConfig = new TcmzMzSweep_Config(configData);

            //if (!engine.IsSimulation)
            //    mzDriverUtils.CleanupAfterSweep();

            //double mzMaxBias_V = mzConfig.MzMaxBias_V;
            double mzMaxBias_V = Convert.ToDouble(testSpec.GetParamLimit("TC_MZ_MINIMA_V_LIMIT_MIN").LowLimit.ValueToString());//get the limit from limit file. Jack.zhang 2012.04.10
            IlMzDriverUtils.MaxModBias_V = mzMaxBias_V;
            this.maxArmBias_v = mzConfig.MzMaxBias_V; 
            double fixedModBias_V = mzConfig.MZInitFixedModBias_volt;
            double imbalance_A = mzConfig.MzCtrlINominal_mA / 1000;
            double sweepMaxAbs_V = mzConfig.MZInitSweepMaxAbs_V;
            sweepMaxAbs_V = Math.Abs(sweepMaxAbs_V);
            double sweepStop_V = 0.0;
            double sweepStart_V = -sweepMaxAbs_V;
            double stepSize_V = mzConfig.MZInitSweepStepSize_mV / 1000;
            int nbrPoints = (int)(1 + Math.Abs(sweepStart_V - sweepStop_V) / stepSize_V);
            double tapBias_V = mzConfig.MZTapBias_V;

            double currentCompliance_A = mzConfig.MZCurrentCompliance_A;
            double currentRange_A = mzConfig.MZCurrentRange_A;
            double voltageCompliance_V = mzConfig.MZVoltageCompliance_V;
            double voltageRange_V = mzConfig.MZVoltageRange_V;
            int numberOfAverages = mzConfig.MzInitNumberOfAverages;
            double integrationRate = mzConfig.MZIntegrationRate;
            double sourceMeasureDelay = mzConfig.MZSourceMeasureDelay_s;
            double powerMeterAveragingTime = mzConfig.MZPowerMeterAveragingTime_s;

            engine.SendToGui("Preparing instruments for sweep");
            if (!engine.IsSimulation)
            {
                if (ArmSourceByAsic)
                {   // ASIC
                    mzInstrs.PowerMeter.SetDefaultState();
                }
                else
                {   // NON-ASIC           
                    mzDriverUtils.InitialiseMZArms(currentCompliance_A, currentRange_A);
                    mzDriverUtils.InitialiseImbalanceArms(voltageCompliance_V, voltageRange_V);
                    mzDriverUtils.InitialiseTaps(currentCompliance_A, currentRange_A);
                    mzDriverUtils.SetMeasurementAccuracy(numberOfAverages, integrationRate, sourceMeasureDelay, powerMeterAveragingTime);
                }

                mzInstrs.PowerMeter.Mode = Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.MeterMode.Absolute_mW;

                // set power meter range to its initial range before any sweep 
                //mzInstrs.PowerMeter.Range = configData.ReadDouble("OpmRangeInit_mW");
                mzInstrs.PowerMeter.Range = Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.AutoRange;
            }
            #endregion

            // measure at first and last channels
            initialOffsetSelected = false;

            // measure at first, middle and last channels
            int midChannelIndex = 0;
            for (int i = 0; i < ituChannels.Length; i++)
            {
                DsdbrChannelData channel = ituChannels[i];
                if (Math.Abs(channel.ItuFreq_GHz - midChannelFrequency) < double.Epsilon)
                {
                    midChannelIndex = i;
                    break;
                }
            }
            int[] chansToMeas = new int[] { 0, midChannelIndex, ituChannels.Length - 1 };
            List<MzSweepDataItuChannel> mzSweepData = new List<MzSweepDataItuChannel>();

            double tuneISOAPowerSlope = configData.ReadDouble("TuneISOAPowerSlope");
            double tuneOpticalPowerTolerance_mW = configData.ReadDouble("TuneOpticalPowerToleranceInmW");
            double TargetFibrePower_dBm = configData.ReadDouble("TargetFibrePower_dBm");

            bool errorOccurred = false;
            string errorInformation = string.Empty;
         
            //// Set up the Vcm bias depth data for the first channel
            //this.MZFixedModBias_volt = MZFixedModBias_volt_FirstChannel;
            firstChOrLastCh = 0;
            foreach (int channelIndex in chansToMeas)
            {
                MzSweepDataItuChannel mzDataItu = measureMzAtFrequency(
                    engine, 
                    mzInstrs, 
                    ituChannels[channelIndex],
                    tuneISOAPowerSlope, 
                    tuneOpticalPowerTolerance_mW, 
                    TargetFibrePower_dBm,
                    configData,
                    out errorOccurred, 
                    out errorInformation );

                mzSweepData.Add(mzDataItu);

                if (errorOccurred)
                    break;

                //// Update the Vcm bias settings for the next channel.
                //this.MZFixedModBias_volt = MZFixedModBias_volt_LastChannel;

                firstChOrLastCh++;

                vpiMaxLimit = vpiMaxLimit - 0.1;//raul changed according to tscr522
            }

            // Clean up
            if (!engine.IsSimulation)
            {
                mzInstrs.PowerMeter.EnableLogging = false;
                mzInstrs.PowerMeter.EnableInputTrigger(false);
            }

            string mzSweepDataResultsFile;
            string mzSweepDataZipFile;
            bool allSweepsPassed;

            processMzSweepData(
                mzSweepData, 
                out mzSweepDataResultsFile, 
                out mzSweepDataZipFile,
                out allSweepsPassed);

            // return the data
            DatumList returnData = new DatumList();
            returnData.AddFileLink("MzSweepDataZipFile", mzSweepDataZipFile);
            returnData.AddFileLink("MzSweepDataResultsFile", mzSweepDataResultsFile);
            returnData.AddSint32("AllSweepsPassed", allSweepsPassed ? 1 : 0);
            returnData.AddReference("MzSweepData", mzSweepData);
            returnData.AddBool("IsError", errorOccurred);

            if (errorOccurred)
                returnData.AddString("ErrorInformation", errorInformation);

            return returnData;
        }

        /// <summary>
        /// Characterise MZ @ the specified ITU frequency
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="mzInstrs"></param>
        /// <param name="dsdbrSetup"></param>
        /// <param name="ISOAPowerSlope"></param>
        /// <param name="opticalPowerTolerance_mW"></param>
        /// <param name="TargetFibrePower_dBm"></param>
        /// <param name="configData"></param>
        /// <param name="isError"></param>
        /// <param name="errorInformation"></param>
        /// <returns></returns>
        private MzSweepDataItuChannel measureMzAtFrequency(
            ITestEngine engine, 
            IlMzInstruments mzInstrs, 
            DsdbrChannelData dsdbrSetup,
            double ISOAPowerSlope, 
            double opticalPowerTolerance_mW, 
            double TargetFibrePower_dBm, 
            DatumList configData,
            out bool isError, 
            out string errorInformation)
        {
            //initialise [out]-parameter
            isError = false;
            errorInformation = string.Empty;
            // Set Vpi and Vcm limits in analysis wrapper 
            MzAnalysisWrapper.vpiMinLimit = this.vpiMinLimit;
            MzAnalysisWrapper.vpiMaxLimit = this.vpiMaxLimit;
            MzAnalysisWrapper.vcmMinLimit = this.vcmMinLimit;
            MzAnalysisWrapper.vcmMaxLimit = this.vcmMaxLimit;

            // setup the DSDBR
            if (!engine.IsSimulation)
                DsdbrUtils.SetDsdbrCurrents_mA(dsdbrSetup.Setup);
            int chanNbr = dsdbrSetup.ItuChannelIndex;

            // Cache the WL in mzUtils so that we can calibrate power after the sweep.
            int numberOfAverages = configData.ReadSint32("MZInitNumberOfAverages");
            double integrationRate = configData.ReadDouble("MZIntegrationRate");
            double sourceMeasureDelay = configData.ReadDouble("MZSourceMeasureDelay_s");
            double powerMeterAveragingTime = configData.ReadDouble("MZPowerMeterAveragingTime_s");

            double wavelength;
            double power_mW;
            if (!engine.IsSimulation)
            {
                if (ArmSourceByAsic)
                {
                    // ASIC
                    Measurements.FrequencyWithoutMeter = dsdbrSetup.ItuFreq_GHz;
                    mzInstrs.PowerMeter.Wavelength_nm = Alg_FreqWlConvert.Freq_GHz_TO_Wave_nm(dsdbrSetup.ItuFreq_GHz);
                    // And apply WL to power meter
                    power_mW = mzInstrs.PowerMeter.ReadPower();
                }
                else
                {
                    wavelength = Measurements.ReadWavelength_nm();
                    mzInstrs.PowerMeter.Wavelength_nm = wavelength;

                    // And apply WL to power meter
                    power_mW = mzInstrs.PowerMeter.ReadPower();
                }
            }

            double iImbLeft_mA = this.mzImbLeftIdeal_mA;
            double iImbRight_mA = this.mzImbRightIdeal_mA;

            bool redoVcmSweeps = false;
            bool adjustImbCurrents = true;
            MzSweepDataItuChannel mzItuChanData;
            double imbalanceIpi_mA = 0;

            double newLeftBias_V = 0.0;
            double newRightBias_V = 0.0;
            string preErrorInfo = "";
            ILMZSweepResult diffIsweep = new ILMZSweepResult();
            string sweepFilePrefix="";
            string diffLIFileName = "";
            

            double peakToQuad_v = 0.0;
            double quadToNull_v = 0.0;
            double powerOffset_dB = 0.0;
            double tuneISOAPowerSlope = 0.0;
            double tuneOpticalPowerTolerance_mW = 0.0;
            double targetPowerDbm = 0.0;
            double powerOffset = 0.0;
            double MaxISoaForPowerLeveling = 0.0;
            MzAnalysisWrapper.MzAnalysisResults mzAnlyDiffI;
            NPlot.TextItem markerText;
            NPlot.PointD marker;
            do
            {
                mzItuChanData = new MzSweepDataItuChannel();
                mzItuChanData.ItuChannelIndex = chanNbr;
                mzItuChanData.ItuFrequency_GHz = dsdbrSetup.ItuFreq_GHz;
                // loop through the common mode bias voltages

                if (adjustImbCurrents)
                {
                    #region we use the imbalance currents to set the quadrature point close to zero
                   
                    
                    
                    newLeftBias_V = configData.ReadDouble("LeftBias_V_LI_Sweep");
                     newRightBias_V = configData.ReadDouble("RightBias_V_LI_Sweep"); // Pa006690 & Pa007371 at -2.5V, others at 0V
                    
                    engine.SendToGui("Running Differential Imbalance sweep on channel " + chanNbr.ToString() );

                   
                    if (!engine.IsSimulation)
                    {
                        diffIsweep = mzDriverUtils.ImbArm_DifferentialSweep(
                            newLeftBias_V,
                            newRightBias_V,
                            0.0,
                            2 * MzCtrlINominal_A,
                            NumberPoints * 3/4,
                            MZSourceMeasureDelay_ms,
                            tapInline_V,
                            MZTapBias_V, 
							ArmSourceByAsic);
                    }
                    #region offline debug
                    /*else
                    {
                        // Load plot data for offline debugging
                        engine.SendToGui("Loading debug data from file");

                        diffIsweep.Type = SweepType.DifferentialCurrent;
                        diffIsweep.SrcMeter = SourceMeter.LeftImbArm;

                        string fileName = string.Format("{0}\\Offline\\Debug_MZCR_Ch{1}_DiffLI.csv", MzFileDirectory, chanNbr);

                        string searchPattern = string.Format("MZCR_Ch{0}_DiffLI*.csv", chanNbr);
                        string[] mzFiles = Directory.GetFiles(MzFileDirectory, searchPattern, SearchOption.AllDirectories);
                        if (mzFiles.Length > 0)
                            fileName = mzFiles[0];

                        MzRawData rawData = MzSweepFileWriter.ReadPlotData(fileName);

                        for (int column = 0; column < rawData.plotData.Length; column++)
                        {
                            ILMZSweepDataType sweepType = (ILMZSweepDataType)Enum.Parse(typeof(ILMZSweepDataType), rawData.header[column]);
                            double[] plotArray = (double[])rawData.plotData[column].ToArray(typeof(double));
                            diffIsweep.SweepData.Add(sweepType, plotArray);                         
                        }
                    }*/
                    #endregion

                    sweepFilePrefix = string.Format("MZCR_Ch{0}_DiffLI", chanNbr);
                    
                    recordSweepData(diffIsweep, sweepFilePrefix, out diffLIFileName);
                    LiSweepFile.Add(diffLIFileName);

                    // Display the plot data
                    engine.SendToGui(diffIsweep);

                   // MzAnalysisWrapper.MzAnalysisResults mzAnlyDiffI;
                    
                    // analyse the data

                    /* Add isUseNegSlopeImb for some devices types which require
                     * imb in negative slope instead of positive one. - chongjian.liang 2013.9.2*/
                    if (ParamManager.Conditions.IsThermalPhase ^ this.isUseNegSlopeImb)
                    {
                        mzAnlyDiffI = MzAnalysisWrapper.Differential_NegChirp_DifferentialSweep(diffIsweep, MzCtrlINominal_A, 0);//raul added
                    }
                    else
                    {
                        mzAnlyDiffI = MzAnalysisWrapper.ZeroChirp_DifferentialSweep(diffIsweep, MzCtrlINominal_A);
                    }

                    // Check analysis results in case we did not sweep the full range
                      peakToQuad_v = Math.Abs(mzAnlyDiffI.MzAnalysis.VoltageAtMax - mzAnlyDiffI.MzAnalysis.VImb);
                      quadToNull_v = Math.Abs(mzAnlyDiffI.MzAnalysis.VImb - mzAnlyDiffI.MzAnalysis.VoltageAtMin);
                    imbalanceIpi_mA = Math.Max(mzAnlyDiffI.MzAnalysis.Vpi, 2 * peakToQuad_v);
                    imbalanceIpi_mA = Math.Max(imbalanceIpi_mA, 2 * quadToNull_v);
                    imbalanceIpi_mA *= 1000;    // convert the units to mA

                    if (ParamManager.Conditions.IsThermalPhase)
                        imbalanceIpi_mA *= -1;//for thermal Imb the index increase with the Imb current different with normal imb.   jack.zhang 2012-02-11

                    // Add a nice arrow to point at the quadrature point
                   marker = new NPlot.PointD(mzAnlyDiffI.Quad_SrcL - mzAnlyDiffI.Quad_SrcR, mzAnlyDiffI.PowerAtMax_dBm - 3);
                    engine.SendToGui(marker);

                     markerText = new NPlot.TextItem(new NPlot.PointD(marker.X, marker.Y), "Quadrature");
                    engine.SendToGui(markerText);

                    if (System.Diagnostics.Debugger.IsAttached)
                        System.Threading.Thread.Sleep(2000);

                    // calculate imbalance currents. 
                    // Quadrature point on positive slope
                    iImbLeft_mA = mzAnlyDiffI.Quad_SrcL * 1000;
                    iImbRight_mA = mzAnlyDiffI.Quad_SrcR * 1000;

                    if (Math.Abs(iImbLeft_mA) < this.mzImbLeftMinLimit_mA || Math.Abs(iImbLeft_mA) > this.mzImbLeftMaxLimit_mA ||
                        Math.Abs(iImbRight_mA) < this.mzImbRightMinLimit_mA || Math.Abs(iImbRight_mA) > this.mzImbRightMaxLimit_mA)
                    {
                        // We have used the maximum amount of MZ modulator bias and the maximum amount of 
                        // MZ imbalance bias and we still cannot meet the spec. 
                        // Stop now. This device is unuseable with the present limits.

                        // 2007-11-29: Ken.Wu: Imbalance too high issue. Bug-fix 1#.
                        //      Just record error information into PCAS.
                        //engine.ShowContinueUserQuery("MZ imbalance is too high to be useable within the limits, but you can continue test. ");
                        errorInformation += preErrorInfo;
                        //isError = true;
                       // break;
                    }                  

                    redoVcmSweeps = false;
                    adjustImbCurrents = false;//only allow adjusting imblance currents once
                   
                    #endregion

                    #region do power leveling at Imb_Peak after imb sweep

                    // Jack.Zhang  2010-05-14
                    // do power leveling here to ensure the mz is characterised at the right fibre power
                    // that can get the right Vpi, Vcm, & Peak bias
                    engine.SendToGui(" Running power leveling on channel " + chanNbr.ToString());
                     powerOffset_dB = configData.ReadDouble("PowerlevelingOffset_dB");
                     tuneISOAPowerSlope = configData.ReadDouble("TuneISOAPowerSlope");
                     tuneOpticalPowerTolerance_mW = configData.ReadDouble("TuneOpticalPowerToleranceInmW");
                     targetPowerDbm = configData.ReadDouble("TargetFibrePower_dBm");
                     powerOffset = Math.Abs(configData.ReadDouble("PowerlevelingOffset_dB"));
                     MaxISoaForPowerLeveling = configData.ReadDouble("MaxSoaForPwrLeveling");

                    // Set imbalance arms to get max power output
                    if (!engine.IsSimulation)
                    {
                        mzDriverUtils.SetCurrent(SourceMeter.LeftImbArm, mzAnlyDiffI.Max_SrcL);
                        mzDriverUtils.SetCurrent(SourceMeter.RightImbArm, mzAnlyDiffI.Max_SrcR);

                        // power level
                        SoaPowerLevel.PowerLevelWithAdaptiveSlope(Alg_PowConvert_dB.Convert_dBmtomW(targetPowerDbm + powerOffset), powerOffset, tuneISOAPowerSlope, MaxISoaForPowerLeveling);

                        // Set imbalance arms to operating point
                        mzDriverUtils.SetCurrent(SourceMeter.LeftImbArm, iImbLeft_mA / 1000);
                        mzDriverUtils.SetCurrent(SourceMeter.RightImbArm, iImbRight_mA / 1000);
                    }
                    #endregion

                }                

            } while (redoVcmSweeps);

            mzImbLeftIdeal_mA = iImbLeft_mA;
            mzImbRightIdeal_mA = iImbRight_mA;
            if (!engine.IsSimulation)
            {
				if (!ArmSourceByAsic)
                mzDriverUtils.SetMeasurementAccuracy(numberOfAverages, integrationRate, sourceMeasureDelay, powerMeterAveragingTime);
                mzInstrs.PowerMeter.Mode = Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.MeterMode.Absolute_mW;
            }

            /* For the ILMZ we can't easily use the single ended performance to estimate
             * the range required for differential drive.
             * Just sweep twice and interpolate. 
             */
            //Alg_MZAnalysis.MZAnalysis seResponse = new Alg_MZAnalysis.MZAnalysis();

            ////mzImbLeftIdeal_mA = iImbLeft_mA;
            ////mzImbRightIdeal_mA = iImbRight_mA;

            //seResponse = measureLeftArmSingleEndedResponse(engine);
            //double LeftArmModBias_V = seResponse.VQuad;

            //mzImbLeftIdeal_mA = iImbLeft_mA;
            //mzImbRightIdeal_mA = iImbRight_mA;
            if (!engine.IsSimulation)
            {
                mzDriverUtils.SetCurrent(SourceMeter.LeftImbArm, iImbLeft_mA / 1000);
                mzDriverUtils.SetCurrent(SourceMeter.RightImbArm, iImbRight_mA / 1000);
            }
            MZFixedModBias_volt = new double[] { 0,0,0,0,0,0 };
            if (firstChOrLastCh == 0)
            {
                MZFixedModBias_volt[0] = this.mzFixedModBias_V_firstChan;
                MZFixedModBias_volt[1] = MZFixedModBias_volt[0] + 1;
            }
            else if (firstChOrLastCh == 1)
            {
                MZFixedModBias_volt[0] = this.mzFixedModBias_V_middleChan;
                MZFixedModBias_volt[1] = MZFixedModBias_volt[0] + 1;
            }
            else
            {
                MZFixedModBias_volt[0] = this.mzFixedModBias_V_lastChan;
                MZFixedModBias_volt[1] = MZFixedModBias_volt[0] + 1;
            }
            MZFixedModBias_volt[0] = Math.Max(vcmMinLimit, MZFixedModBias_volt[0]);
            MZFixedModBias_volt[1] = Math.Max(vcmMinLimit, MZFixedModBias_volt[1]);
            MZFixedModBias_volt[0] = Math.Min(vcmMaxLimit, MZFixedModBias_volt[0]);
            MZFixedModBias_volt[1] = Math.Min(vcmMaxLimit, MZFixedModBias_volt[1]);

             redoVcmSweeps = false;
             adjustImbCurrents = false;// Now we use fixed imbalance currents, thus set adjustImbCurrents = false

            // loop through the common mode bias voltages
            for (int vcmIndex = 0; vcmIndex < 2; vcmIndex++)
            {
                MzItuAtVcmData vcmData = measureMzAtVcmLevel(
                    engine,
                    chanNbr, 
                    vcmIndex, 
                    iImbLeft_mA, 
                    iImbRight_mA);

                mzItuChanData.VcmData.Add(vcmData);
            }

            double Vcm0_Vpi_V = mzItuChanData.VcmData[0].GetValueDouble(MzItuAtVcmMeasurements.Vpi_V);
            double Vcm0_Vcm_V = mzItuChanData.VcmData[0].Vcm_V;

            double Vcm1_Vpi_V = mzItuChanData.VcmData[1].GetValueDouble(MzItuAtVcmMeasurements.Vpi_V);
            double Vcm1_Vcm_V = mzItuChanData.VcmData[1].Vcm_V;
            if (Vcm0_Vcm_V == Vcm1_Vcm_V || Vcm0_Vpi_V == Vcm1_Vpi_V)
            {
                string errorMsg = string.Format("Failed to optimize Channel {0} Vcm Vpi, \n Stop testing!", chanNbr);
                isError = true;
                errorInformation = errorMsg;
            }

            double vMinBias = mzItuChanData.VcmData[0].GetMeasuredDataHighLimit(MzItuAtVcmMeasurements.VminL_V);
            double vMaxBias = mzItuChanData.VcmData[0].GetMeasuredDataLowLimit(MzItuAtVcmMeasurements.VminL_V);
            double vBiasInset = 0.01;
            vMinBias -= vBiasInset;
            vMaxBias += vBiasInset;

            VcmCalculator vcmCalc = new VcmCalculator(Vcm0_Vpi_V, Vcm0_Vcm_V, Vcm1_Vpi_V, Vcm1_Vcm_V, vMinBias, vMaxBias);
            double targetVcm = vcmCalc.CalculateVcm(this.vpiMaxLimit);

            /**** Set Left and Right arms to Vcm ****/
            newLeftBias_V = targetVcm;
            newRightBias_V = targetVcm;
            engine.SendToGui("Sweep imbalance at target Vcm");


            #region LISWEEP AND POWERLEVELING AGAIN

            if (!engine.IsSimulation)
                    {
                        diffIsweep = mzDriverUtils.ImbArm_DifferentialSweep(
                            newLeftBias_V,
                            newRightBias_V,
                            0.0,
                            2 * MzCtrlINominal_A,
                            NumberPoints * 3/4,
                            MZSourceMeasureDelay_ms,
                            tapInline_V,
                            MZTapBias_V, 
							ArmSourceByAsic);
                    }
                    #region offline debug
                    /*else
                    {
                        // Load plot data for offline debugging
                        engine.SendToGui("Loading debug data from file");

                        diffIsweep.Type = SweepType.DifferentialCurrent;
                        diffIsweep.SrcMeter = SourceMeter.LeftImbArm;

                        string fileName = string.Format("{0}\\Offline\\Debug_MZCR_Ch{1}_DiffLI.csv", MzFileDirectory, chanNbr);

                        string searchPattern = string.Format("MZCR_Ch{0}_DiffLI*.csv", chanNbr);
                        string[] mzFiles = Directory.GetFiles(MzFileDirectory, searchPattern, SearchOption.AllDirectories);
                        if (mzFiles.Length > 0)
                            fileName = mzFiles[0];

                        MzRawData rawData = MzSweepFileWriter.ReadPlotData(fileName);

                        for (int column = 0; column < rawData.plotData.Length; column++)
                        {
                            ILMZSweepDataType sweepType = (ILMZSweepDataType)Enum.Parse(typeof(ILMZSweepDataType), rawData.header[column]);
                            double[] plotArray = (double[])rawData.plotData[column].ToArray(typeof(double));
                            diffIsweep.SweepData.Add(sweepType, plotArray);                         
                        }
                    }*/
                    #endregion

                    sweepFilePrefix = string.Format("MZCR_Ch{0}_DiffLI", chanNbr);
                    
                    recordSweepData(diffIsweep, sweepFilePrefix, out diffLIFileName);
                    LiSweepFile.Add(diffLIFileName);

                    // Display the plot data
                    engine.SendToGui(diffIsweep);

                   
                    
                    // analyse the data

                    /* Add isUseNegSlopeImb for some devices types which require
                     * imb in negative slope instead of positive one. - chongjian.liang 2013.9.2*/
                    if (ParamManager.Conditions.IsThermalPhase ^ this.isUseNegSlopeImb)
                    {
                        mzAnlyDiffI = MzAnalysisWrapper.Differential_NegChirp_DifferentialSweep(diffIsweep, MzCtrlINominal_A, 0);//raul added
                    }
                    else
                    {
                        mzAnlyDiffI = MzAnalysisWrapper.ZeroChirp_DifferentialSweep(diffIsweep, MzCtrlINominal_A);
                    }

                    // Check analysis results in case we did not sweep the full range
                      peakToQuad_v = Math.Abs(mzAnlyDiffI.MzAnalysis.VoltageAtMax - mzAnlyDiffI.MzAnalysis.VImb);
                      quadToNull_v = Math.Abs(mzAnlyDiffI.MzAnalysis.VImb - mzAnlyDiffI.MzAnalysis.VoltageAtMin);
                    imbalanceIpi_mA = Math.Max(mzAnlyDiffI.MzAnalysis.Vpi, 2 * peakToQuad_v);
                    imbalanceIpi_mA = Math.Max(imbalanceIpi_mA, 2 * quadToNull_v);
                    imbalanceIpi_mA *= 1000;    // convert the units to mA

                    if (ParamManager.Conditions.IsThermalPhase)
                        imbalanceIpi_mA *= -1;//for thermal Imb the index increase with the Imb current different with normal imb.   jack.zhang 2012-02-11

                    // Add a nice arrow to point at the quadrature point
                    marker = new NPlot.PointD(mzAnlyDiffI.Quad_SrcL - mzAnlyDiffI.Quad_SrcR, mzAnlyDiffI.PowerAtMax_dBm - 3);
                    engine.SendToGui(marker);

                    markerText = new NPlot.TextItem(new NPlot.PointD(marker.X, marker.Y), "Quadrature");
                    engine.SendToGui(markerText);

                    if (System.Diagnostics.Debugger.IsAttached)
                        System.Threading.Thread.Sleep(2000);

                    // calculate imbalance currents. 
                    // Quadrature point on positive slope
                    iImbLeft_mA = mzAnlyDiffI.Quad_SrcL * 1000;
                    iImbRight_mA = mzAnlyDiffI.Quad_SrcR * 1000;

                    if (Math.Abs(iImbLeft_mA) < this.mzImbLeftMinLimit_mA || Math.Abs(iImbLeft_mA) > this.mzImbLeftMaxLimit_mA ||
                        Math.Abs(iImbRight_mA) < this.mzImbRightMinLimit_mA || Math.Abs(iImbRight_mA) > this.mzImbRightMaxLimit_mA)
                    {
                        // We have used the maximum amount of MZ modulator bias and the maximum amount of 
                        // MZ imbalance bias and we still cannot meet the spec. 
                        // Stop now. This device is unuseable with the present limits.

                        // 2007-11-29: Ken.Wu: Imbalance too high issue. Bug-fix 1#.
                        //      Just record error information into PCAS.
                        //engine.ShowContinueUserQuery("MZ imbalance is too high to be useable within the limits, but you can continue test. ");
                        errorInformation += preErrorInfo;
                        //isError = true;
                       // break;
                    }                  

                    redoVcmSweeps = false;
                    adjustImbCurrents = false;//only allow adjusting imblance currents once
                   
                    #endregion

                    #region do power leveling at Imb_Peak after imb sweep again

                    // Jack.Zhang  2010-05-14
                    // do power leveling here to ensure the mz is characterised at the right fibre power
                    // that can get the right Vpi, Vcm, & Peak bias
                    engine.SendToGui(" Running power leveling on channel " + chanNbr.ToString());
                     powerOffset_dB = configData.ReadDouble("PowerlevelingOffset_dB");
                     tuneISOAPowerSlope = configData.ReadDouble("TuneISOAPowerSlope");
                     tuneOpticalPowerTolerance_mW = configData.ReadDouble("TuneOpticalPowerToleranceInmW");
                     targetPowerDbm = configData.ReadDouble("TargetFibrePower_dBm");
                     powerOffset = Math.Abs(configData.ReadDouble("PowerlevelingOffset_dB"));
                     MaxISoaForPowerLeveling = configData.ReadDouble("MaxSoaForPwrLeveling");

                    // Set imbalance arms to get max power output
                    if (!engine.IsSimulation)
                    {
                        mzDriverUtils.SetCurrent(SourceMeter.LeftImbArm, iImbLeft_mA / 1000);// mzAnlyDiffI.Max_SrcL);
                        mzDriverUtils.SetCurrent(SourceMeter.RightImbArm,iImbRight_mA / 1000);// mzAnlyDiffI.Max_SrcR);

                        // power level
                        SoaPowerLevel.PowerLevelWithAdaptiveSlope(Alg_PowConvert_dB.Convert_dBmtomW(targetPowerDbm + powerOffset), powerOffset, tuneISOAPowerSlope, MaxISoaForPowerLeveling);

                        // Set imbalance arms to operating point
                        mzDriverUtils.SetCurrent(SourceMeter.LeftImbArm, iImbLeft_mA / 1000);
                        mzDriverUtils.SetCurrent(SourceMeter.RightImbArm, iImbRight_mA / 1000);
                    }
                    #endregion


                    mzImbLeftIdeal_mA = iImbLeft_mA;
                    mzImbRightIdeal_mA = iImbRight_mA;
                    if (!engine.IsSimulation)
                    {
                        if (!ArmSourceByAsic)
                            mzDriverUtils.SetMeasurementAccuracy(numberOfAverages, integrationRate, sourceMeasureDelay, powerMeterAveragingTime);
                        mzInstrs.PowerMeter.Mode = Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.MeterMode.Absolute_mW;
                    }

                    /* For the ILMZ we can't easily use the single ended performance to estimate
                     * the range required for differential drive.
                     * Just sweep twice and interpolate. 
                     */
                    //Alg_MZAnalysis.MZAnalysis seResponse = new Alg_MZAnalysis.MZAnalysis();

                    ////mzImbLeftIdeal_mA = iImbLeft_mA;
                    ////mzImbRightIdeal_mA = iImbRight_mA;

                    //seResponse = measureLeftArmSingleEndedResponse(engine);
                    //double LeftArmModBias_V = seResponse.VQuad;

                    //mzImbLeftIdeal_mA = iImbLeft_mA;
                    //mzImbRightIdeal_mA = iImbRight_mA;
                    if (!engine.IsSimulation)
                    {
                        mzDriverUtils.SetCurrent(SourceMeter.LeftImbArm, iImbLeft_mA / 1000);
                        mzDriverUtils.SetCurrent(SourceMeter.RightImbArm, iImbRight_mA / 1000);
                    }
                    MZFixedModBias_volt = new double[] { 0, 0, 0, 0, 0, 0 };
                    if (firstChOrLastCh == 0)
                    {
                        MZFixedModBias_volt[0] = this.mzFixedModBias_V_firstChan;
                        MZFixedModBias_volt[1] = MZFixedModBias_volt[0] + 1;
                    }
                    else if (firstChOrLastCh == 1)
                    {
                        MZFixedModBias_volt[0] = this.mzFixedModBias_V_middleChan;
                        MZFixedModBias_volt[1] = MZFixedModBias_volt[0] + 1;
                    }
                    else
                    {
                        MZFixedModBias_volt[0] = this.mzFixedModBias_V_lastChan;
                        MZFixedModBias_volt[1] = MZFixedModBias_volt[0] + 1;
                    }
                    MZFixedModBias_volt[0] = Math.Max(vcmMinLimit, MZFixedModBias_volt[0]);
                    MZFixedModBias_volt[1] = Math.Max(vcmMinLimit, MZFixedModBias_volt[1]);
                    MZFixedModBias_volt[0] = Math.Min(vcmMaxLimit, MZFixedModBias_volt[0]);
                    MZFixedModBias_volt[1] = Math.Min(vcmMaxLimit, MZFixedModBias_volt[1]);

                    redoVcmSweeps = false;
                    adjustImbCurrents = false;// Now we use fixed imbalance currents, thus set adjustImbCurrents = false

                    // loop through the common mode bias voltages
                    for (int vcmIndex = 0; vcmIndex < 2; vcmIndex++)
                    {
                        MzItuAtVcmData vcmData = measureMzAtVcmLevel(
                            engine,
                            chanNbr,
                            vcmIndex,
                            iImbLeft_mA,
                            iImbRight_mA);

                        mzItuChanData.VcmData.Add(vcmData);
                    }

                    #region get data from Vcm0 and Vcm1
                    Vcm0_Vpi_V = mzItuChanData.VcmData[0].GetValueDouble(MzItuAtVcmMeasurements.Vpi_V);
                    Vcm0_Vcm_V = mzItuChanData.VcmData[0].Vcm_V;

                     Vcm1_Vpi_V = mzItuChanData.VcmData[1].GetValueDouble(MzItuAtVcmMeasurements.Vpi_V);
                     Vcm1_Vcm_V = mzItuChanData.VcmData[1].Vcm_V;
                    if (Vcm0_Vcm_V == Vcm1_Vcm_V || Vcm0_Vpi_V == Vcm1_Vpi_V)
                    {
                        string errorMsg = string.Format("Failed to optimize Channel {0} Vcm Vpi, \n Stop testing!", chanNbr);
                        isError = true;
                        errorInformation = errorMsg;
                    }


            // Calculate straight line fits for Vpi
            //
            // Vpi vs Vcm between vcm0 and vcm1
            double Vcm0Vcm1_Gradient = (Vcm0_Vpi_V - Vcm1_Vpi_V) / (Vcm0_Vcm_V - Vcm1_Vcm_V);
            // Offset required is Y at X=0, so swap X & Y and invert gradient
            double Vcm0Vcm1_Offset = EstimateXAtY.Calculate(Vcm0_Vpi_V, Vcm0_Vcm_V, 1 / Vcm0Vcm1_Gradient, 0);

            // AbsVoffset vs Vcm between vcm0 and vcm1
            double Vcm0AbsVimb_V = mzItuChanData.VcmData[0].GetValueDouble(MzItuAtVcmMeasurements.VquadL_V) -
                mzItuChanData.VcmData[0].GetValueDouble(MzItuAtVcmMeasurements.VquadR_V);
            double Vcm1AbsVimb_V = mzItuChanData.VcmData[1].GetValueDouble(MzItuAtVcmMeasurements.VquadL_V) -
                mzItuChanData.VcmData[1].GetValueDouble(MzItuAtVcmMeasurements.VquadR_V);

            double Vcm0Vcm1_AbsVoffsetFit_Gradient = (Vcm0AbsVimb_V - Vcm1AbsVimb_V) /
                (Vcm0_Vcm_V - Vcm1_Vcm_V);
            // Offset required is Y at X=0, so swap X & Y and invert gradient
            double Vcm0Vcm1_AbsVoffsetFit_Offset = EstimateXAtY.Calculate(Vcm0AbsVimb_V, Vcm0_Vcm_V, 1 / Vcm0Vcm1_AbsVoffsetFit_Gradient, 0);
            #endregion




            #region mid channel need close to first channel,last channel need close to mid channel
            if (firstChOrLastCh == 0)
            {
                chan0Quad_Diff = iImbLeft_mA - iImbRight_mA;
            }
            else if (firstChOrLastCh == 1)
            {
                double Diff_1 = iImbLeft_mA - iImbRight_mA;
                double Diff_2 = Diff_1 + 2 * imbalanceIpi_mA;
                double Diff_3 = Diff_1 - 2 * imbalanceIpi_mA;
                double diff_min = Math.Min(Math.Abs(chan0Quad_Diff - Diff_1), Math.Abs(chan0Quad_Diff - Diff_2));
                diff_min = Math.Min(diff_min, Math.Abs(chan0Quad_Diff - Diff_3));
                if (diff_min == Math.Abs(chan0Quad_Diff - Diff_1))
                { }
                else if (diff_min == Math.Abs(chan0Quad_Diff - Diff_2))
                {
                    iImbLeft_mA += imbalanceIpi_mA;
                    iImbRight_mA -= imbalanceIpi_mA;
                    if (Math.Abs(iImbLeft_mA) < this.mzImbLeftMinLimit_mA || Math.Abs(iImbLeft_mA) > this.mzImbLeftMaxLimit_mA ||
                        Math.Abs(iImbRight_mA) < this.mzImbRightMinLimit_mA || Math.Abs(iImbRight_mA) > this.mzImbRightMaxLimit_mA)
                    {
                        isError = true;
                        errorInformation = string.Format("MZ imbalance out of limit when Mid channel trying to close first channel,\n Stop testing!");
                    }
                }
                else if (diff_min == Math.Abs(chan0Quad_Diff - Diff_3))
                {
                    iImbLeft_mA -= imbalanceIpi_mA;
                    iImbRight_mA += imbalanceIpi_mA;
                    if (Math.Abs(iImbLeft_mA) < this.mzImbLeftMinLimit_mA || Math.Abs(iImbLeft_mA) > this.mzImbLeftMaxLimit_mA ||
                        Math.Abs(iImbRight_mA) < this.mzImbRightMinLimit_mA || Math.Abs(iImbRight_mA) > this.mzImbRightMaxLimit_mA)
                    {
                        isError = true;
                        errorInformation = string.Format("MZ imbalance out of limit when Mid channel trying to close first channel,\n Stop testing!");
                    }
                }
                chan57Quad_Diff = iImbLeft_mA - iImbRight_mA;
            }
            else if (firstChOrLastCh == 2)
            {
                double Diff_1 = iImbLeft_mA - iImbRight_mA;
                double Diff_2 = Diff_1 + 2 * imbalanceIpi_mA;
                double Diff_3 = Diff_1 - 2 * imbalanceIpi_mA;
                double diff_min = Math.Min(Math.Abs(chan57Quad_Diff - Diff_1), Math.Abs(chan57Quad_Diff - Diff_2));
                diff_min = Math.Min(diff_min, Math.Abs(chan57Quad_Diff - Diff_3));
                if (diff_min == Math.Abs(chan57Quad_Diff - Diff_1))
                { }
                else if (diff_min == Math.Abs(chan57Quad_Diff - Diff_2))
                {
                    iImbLeft_mA += imbalanceIpi_mA;
                    iImbRight_mA -= imbalanceIpi_mA;
                    if (Math.Abs(iImbLeft_mA) < this.mzImbLeftMinLimit_mA || Math.Abs(iImbLeft_mA) > this.mzImbLeftMaxLimit_mA ||
                        Math.Abs(iImbRight_mA) < this.mzImbRightMinLimit_mA || Math.Abs(iImbRight_mA) > this.mzImbRightMaxLimit_mA)
                    {
                        isError = true;
                        errorInformation = string.Format("MZ imbalance out of limit when Last channel trying to close Mid channel,\n Stop testing!");
                    }
                }
                else if (diff_min == Math.Abs(chan57Quad_Diff - Diff_3))
                {
                    iImbLeft_mA -= imbalanceIpi_mA;
                    iImbRight_mA += imbalanceIpi_mA;
                    if (Math.Abs(iImbLeft_mA) < this.mzImbLeftMinLimit_mA || Math.Abs(iImbLeft_mA) > this.mzImbLeftMaxLimit_mA ||
                        Math.Abs(iImbRight_mA) < this.mzImbRightMinLimit_mA || Math.Abs(iImbRight_mA) > this.mzImbRightMaxLimit_mA)
                    {
                        isError = true;
                        errorInformation = string.Format("MZ imbalance out of limit when Last channel trying to close Mid channel,\n Stop testing!");
                    }
                }
            }
            #endregion
            double ER_optimiseVCM = 0;
            if (vpiMaxLimit >= Vcm0_Vpi_V - Vpi_offset && vpiMaxLimit <= Vcm1_Vpi_V + Vpi_offset)
            {
                #region Sweeping MZ under Vcm2 condition
                double Vcm2_Vcm_V = (vpiMaxLimit - Vcm0Vcm1_Offset) / Vcm0Vcm1_Gradient;
                MZFixedModBias_volt[2] = Vcm2_Vcm_V;
                MzItuAtVcmData vcm2Data = measureMzAtVcmLevel(
                    engine,
                    chanNbr,
                    2,
                    iImbLeft_mA,
                    iImbRight_mA);

                mzItuChanData.VcmData.Add(vcm2Data);
                ER_optimiseVCM = vcm2Data.GetValueDouble(MzItuAtVcmMeasurements.ER_dB);
                #endregion
            }
            else if (vpiMaxLimit >= Vcm1_Vpi_V + Vpi_offset)
            {
                #region Sweeping MZ under Vcm2 condition
                double Vcm2_Vcm_V = (-0.5 - Vcm0Vcm1_Offset / 4 - Vcm0Vcm1_AbsVoffsetFit_Offset / 2) /
                    (1 + Vcm0Vcm1_Gradient / 4 + Vcm0Vcm1_AbsVoffsetFit_Gradient / 2);
                Vcm2_Vcm_V = Math.Min((vpiMaxLimit - Vcm0Vcm1_Offset) / Vcm0Vcm1_Gradient, Vcm2_Vcm_V);
                MZFixedModBias_volt[2] = Vcm2_Vcm_V;
                MzItuAtVcmData vcm2Data = measureMzAtVcmLevel(
                    engine,
                    chanNbr,
                    2,
                    iImbLeft_mA,
                    iImbRight_mA);

                mzItuChanData.VcmData.Add(vcm2Data);
                
                #endregion

                if (Vcm1_Vcm_V != Vcm2_Vcm_V)
                {
                    #region get data from Vcm1 and Vcm2

                    double Vcm2_Vpi_V = vcm2Data.GetValueDouble(MzItuAtVcmMeasurements.Vpi_V);

                    // Vpi vs Vcm between vcm1 and vcm2
                    double Vcm1Vcm2_Gradient = (Vcm1_Vpi_V - Vcm2_Vpi_V) / (Vcm1_Vcm_V - Vcm2_Vcm_V);
                    // Offset required is Y at X=0, so swap X & Y and invert gradient
                    double Vcm1Vcm2_Offset = EstimateXAtY.Calculate(Vcm1_Vpi_V, Vcm1_Vcm_V, 1 / Vcm1Vcm2_Gradient, 0);

                    // AbsVoffset vs Vcm between vcm0 and vcm1
                    double Vcm2AbsVimb_V = vcm2Data.GetValueDouble(MzItuAtVcmMeasurements.VquadL_V)
                        - vcm2Data.GetValueDouble(MzItuAtVcmMeasurements.VquadR_V);

                    double Vcm1Vcm2_AbsVoffsetFit_Gradient = (Vcm1AbsVimb_V - Vcm2AbsVimb_V) /
                        (Vcm1_Vcm_V - Vcm2_Vcm_V);
                    // Offset required is Y at X=0, so swap X & Y and invert gradient
                    double Vcm1Vcm2_AbsVoffsetFit_Offset = EstimateXAtY.Calculate(Vcm1AbsVimb_V, Vcm1_Vcm_V, 1 / Vcm1Vcm2_AbsVoffsetFit_Gradient, 0);
                    #endregion

                    #region Sweeping MZ under Vcm3 condition

                    double Vcm3_Vcm_V = (-0.5 - Vcm1Vcm2_Offset / 4 - Vcm1Vcm2_AbsVoffsetFit_Offset / 2) /
                        (1 + Vcm1Vcm2_Gradient / 4 + Vcm1Vcm2_AbsVoffsetFit_Gradient / 2);

                    Vcm3_Vcm_V = Math.Min((vpiMaxLimit - Vcm1Vcm2_Offset) / Vcm1Vcm2_Gradient, Vcm3_Vcm_V);

                    MZFixedModBias_volt[3] = Vcm3_Vcm_V;

                    MzItuAtVcmData vcm3Data = measureMzAtVcmLevel(
                        engine,
                        chanNbr,
                        3,
                        iImbLeft_mA,
                        iImbRight_mA);

                    mzItuChanData.VcmData.Add(vcm3Data);

                    ER_optimiseVCM = vcm3Data.GetValueDouble(MzItuAtVcmMeasurements.ER_dB);

                    #endregion 
                }
                else
                {
                    ER_optimiseVCM = vcm2Data.GetValueDouble(MzItuAtVcmMeasurements.ER_dB);
                }
            }
            else if (vpiMaxLimit <= Vcm0_Vpi_V - Vpi_offset)
            {
                #region Sweeping MZ under Vcm2 condition
                double Vcm2_Vcm_V = Math.Max((vpiMaxLimit - Vcm0Vcm1_Offset) / Vcm0Vcm1_Gradient, this.vcmMinLimit);

                MZFixedModBias_volt[2] = Vcm2_Vcm_V;
                MzItuAtVcmData vcm2Data = measureMzAtVcmLevel(
                    engine,
                    chanNbr,
                    2,
                    iImbLeft_mA,
                    iImbRight_mA);

                mzItuChanData.VcmData.Add(vcm2Data);
                #endregion

                if (Vcm0_Vcm_V != Vcm2_Vcm_V)
                {
                    #region get data from Vcm0 and Vcm2
                    double Vcm2_Vpi_V = vcm2Data.GetValueDouble(MzItuAtVcmMeasurements.Vpi_V);

                    // Vpi vs Vcm between vcm0 and vcm2
                    double Vcm0Vcm2_Gradient = (Vcm0_Vpi_V - Vcm2_Vpi_V) / (Vcm0_Vcm_V - Vcm2_Vcm_V);
                    // Offset required is Y at X=0, so swap X & Y and invert gradient
                    double Vcm0Vcm2_Offset = EstimateXAtY.Calculate(Vcm0_Vpi_V, Vcm0_Vcm_V, 1 / Vcm0Vcm2_Gradient, 0);

                    // AbsVoffset vs Vcm between vcm0 and vcm1
                    double Vcm2AbsVimb_V = vcm2Data.GetValueDouble(MzItuAtVcmMeasurements.VquadL_V)
                        - vcm2Data.GetValueDouble(MzItuAtVcmMeasurements.VquadR_V);

                    double Vcm0Vcm2_AbsVoffsetFit_Gradient = (Vcm0AbsVimb_V - Vcm2AbsVimb_V) /
                        (Vcm0_Vcm_V - Vcm2_Vcm_V);
                    // Offset required is Y at X=0, so swap X & Y and invert gradient
                    double Vcm0Vcm2_AbsVoffsetFit_Offset = EstimateXAtY.Calculate(Vcm0AbsVimb_V, Vcm0_Vcm_V, 1 / Vcm0Vcm2_AbsVoffsetFit_Gradient, 0);
                    #endregion

                    #region Sweeping MZ under Vcm3 condition
                    double Vcm3_Vcm_V = Math.Max((vpiMaxLimit - Vcm0Vcm2_Offset) / Vcm0Vcm2_Gradient, this.vcmMinLimit);

                    MZFixedModBias_volt[3] = Vcm3_Vcm_V;
                    MzItuAtVcmData vcm3Data = measureMzAtVcmLevel(
                        engine,
                        chanNbr,
                        3,
                        iImbLeft_mA,
                        iImbRight_mA);

                    mzItuChanData.VcmData.Add(vcm3Data);
                    ER_optimiseVCM = vcm3Data.GetValueDouble(MzItuAtVcmMeasurements.ER_dB);
                    #endregion 
                }
                else
                {
                    ER_optimiseVCM = vcm2Data.GetValueDouble(MzItuAtVcmMeasurements.ER_dB);
                }
            }

            // Judge DCER compair with limit, then go to adjustImbCurrents condition to do
            if (ER_optimiseVCM < dcerMinLimit + Margin_ER || ER_optimiseVCM > dcerMaxLimit - Margin_ER)
            {
                adjustImbCurrents = true;
            }
            
            //raul added if need shift 2pi to do sweep judged by dcer,this is only used by the first channel
            //the last channel will closed to the first channel

            if (firstChOrLastCh == 0 && adjustImbCurrents && !ParamManager.Conditions.IsThermalPhase)
            {
                int Ch0_VcmIndex = 0;
                if (MZFixedModBias_volt[3] != 0) Ch0_VcmIndex = 3;
                else Ch0_VcmIndex = 2;
                //check if need do the 4th sweep
                #region Optimize imbalance(shift 2pi)to do the 5th sweep if ER fail
                if (ER_optimiseVCM >= this.dcerMaxLimit - Margin_ER)//left shift 2pi
                {
                    iImbLeft_mA -= imbalanceIpi_mA;
                    iImbRight_mA += imbalanceIpi_mA;
                    
                    //check if the shifted imb current not out of limit,then we do the 4th sweep
                    if ((Math.Abs(iImbLeft_mA) >= this.mzImbLeftMinLimit_mA) &&
                        (Math.Abs(iImbRight_mA) <= this.mzImbRightMaxLimit_mA))
                    {
                        MzItuAtVcmData LeftShiftSweepData = measureMzAtVcmLevel(
                            engine,
                            chanNbr,
                            Ch0_VcmIndex,
                            iImbLeft_mA,
                            iImbRight_mA);
                        mzItuChanData.VcmData.Add(LeftShiftSweepData);
                    }
                    else //out of limit,then reset to original quad point
                    {
                        iImbLeft_mA += imbalanceIpi_mA;
                        iImbRight_mA -= imbalanceIpi_mA;
                    }
                }

                else if (ER_optimiseVCM <= this.dcerMinLimit + Margin_ER) //right shift 2pi
                {
                    iImbLeft_mA += imbalanceIpi_mA;
                    iImbRight_mA -= imbalanceIpi_mA;

                    //check if right shift current is in limit
                    if ((Math.Abs(iImbLeft_mA) <= this.mzImbLeftMaxLimit_mA) &&
                        (Math.Abs(iImbRight_mA) >= this.mzImbRightMinLimit_mA))
                    {
                        MzItuAtVcmData RightShiftSweepData = measureMzAtVcmLevel(
                            engine,
                            chanNbr,
                            Ch0_VcmIndex,
                            iImbLeft_mA,
                            iImbRight_mA);
                        mzItuChanData.VcmData.Add(RightShiftSweepData);
                    }
                    else //out of limit,then reset to original quad point
                    {
                        iImbLeft_mA -= imbalanceIpi_mA;
                        iImbRight_mA += imbalanceIpi_mA;
                    }
                }
                chan0Quad_Diff = iImbLeft_mA - iImbRight_mA;
                #endregion
                
            }

            // If ER is less than limit, set VCM to the minimum value - chongjian.liang 2012.12.21
            if (ER_optimiseVCM <= this.dcerMinLimit + Margin_ER)
            {
                int index_VCM = 4;

                MZFixedModBias_volt[index_VCM] = vcmMinLimit;

                MzItuAtVcmData LeftShiftSweepData = measureMzAtVcmLevel(
                    engine,
                    chanNbr,
                    index_VCM,
                    iImbLeft_mA,
                    iImbRight_mA);

                mzItuChanData.VcmData.Add(LeftShiftSweepData);
            }

            return mzItuChanData;
        }



        /// <summary>
        /// Characterise MZ at a given common mode bias voltage
        /// </summary>
        /// <param name="engine">test engine</param>
        /// <param name="ituChannelIndex">channel index</param>
        /// <param name="vcmIndex">Vcm Index</param>
        /// <param name="iImbLeft">left imb current</param>
        /// <param name="iImbRight">right imb current</param>
        /// <returns>MzData</returns>
        private MzItuAtVcmData measureMzAtVcmLevel(ITestEngine engine, int ituChannelIndex, int vcmIndex, double iImbLeft, double iImbRight)
        {
            // find the common mode voltage we are using
            double vcm_V = MZFixedModBias_volt[vcmIndex];

            double iSOA_mA = 0;
            if (!engine.IsSimulation)
                iSOA_mA = DsdbrUtils.ReadSectionCurrent_mA(DSDBRSection.SOA);

            // initialise the data object (including the limits)
            MzItuAtVcmData vcmData = new MzItuAtVcmData(vcm_V, iSOA_mA);

            // store imbalance biases
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.IImbQuadL_mA, Math.Abs(iImbLeft));
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.IImbQuadR_mA, Math.Abs(iImbRight));

            // If Vcm is high we need to restrict the max bias level on either arm to its max limit by just performing the middle section of the sweep.
            double startBias = 2 * vcm_V;
            double stopBias = 0;
            if (startBias < maxArmBias_v)
            {
                double offset = startBias - maxArmBias_v;
                startBias = maxArmBias_v;
                stopBias = offset;
            }

            // diff voltage sweep - assume VCM is negative. Apply calculated IQuad to the imbalance electrodes
            engine.SendToGui("Running Differential Modulator sweep on channel " + ituChannelIndex.ToString() + " from 0V to " + vcm_V.ToString() + "V");
            bool dataOk = true;
            double minLevel_dBm = -60;
            ILMZSweepResult diffVsweep = new ILMZSweepResult();
            do
            {
                if (!engine.IsSimulation)
                {
                    diffVsweep = mzDriverUtils.ModArm_DifferentialSweepByTrigger
                        (vcmData.GetValueDouble(MzItuAtVcmMeasurements.IImbQuadL_mA) / 1000,
                         vcmData.GetValueDouble(MzItuAtVcmMeasurements.IImbQuadR_mA) / 1000,
                         startBias, stopBias, NumberPoints,MZSourceMeasureDelay_ms, tapInline_V, MZTapBias_V, ArmSourceByAsic);
                }
                #region offline debug
                else
                {
                    // Load plot data for offline debugging
                    /*engine.SendToGui("Loading debug data from file");

                    diffVsweep.Type = SweepType.DifferentialVoltage;
                    diffVsweep.SrcMeter = SourceMeter.LeftModArm;

                    string fileName = string.Format("{0}\\Offline\\Debug_MZCR_CH{1}_DILV_VCM{2}.csv", MzFileDirectory, ituChannelIndex, vcmIndex);

                    string searchPattern = string.Format("MZCR_CH{0}_DILV_VCM{1}*.csv", ituChannelIndex, vcmIndex);
                    string[] mzFiles = Directory.GetFiles(MzFileDirectory, searchPattern, SearchOption.AllDirectories);
                    if (mzFiles.Length > 0)
                        fileName = mzFiles[0];

                    MzRawData rawData = MzSweepFileWriter.ReadPlotData(fileName);

                    for (int column = 0; column < rawData.plotData.Length; column++)
                    {
                        ILMZSweepDataType sweepType = (ILMZSweepDataType)Enum.Parse(typeof(ILMZSweepDataType), rawData.header[column]);
                        double[] plotArray = (double[])rawData.plotData[column].ToArray(typeof(double));
                        diffVsweep.SweepData.Add(sweepType, plotArray);
                    }*/
                }
                #endregion

                string sweepFileName;
                string fileNamePrefix = string.Format("MZCR_CH{0}_DILV_VCM{1}", ituChannelIndex, vcmIndex);
                recordSweepData(diffVsweep, fileNamePrefix, out sweepFileName);
                LiSweepFile.Add(sweepFileName);

                // If overrange run again
                if (MzAnalysisWrapper.CheckForOverrange(diffVsweep.SweepData[ILMZSweepDataType.FibrePower_mW]))
                {
                    dataOk = false;
                    if (double.IsNaN(mzDriverUtils.MzInstrs.PowerMeter.Range)) // auto range?
                        mzDriverUtils.MzInstrs.PowerMeter.Range = 1;
                    else
                        mzDriverUtils.MzInstrs.PowerMeter.Range = mzDriverUtils.MzInstrs.PowerMeter.Range * 10;
                }
                else
                {
                    // if underrange fix the data and continue
                    dataOk = true;
                    if (MzAnalysisWrapper.CheckForUnderrange(diffVsweep.SweepData[ILMZSweepDataType.FibrePower_mW]))
                    {
                        diffVsweep.SweepData[ILMZSweepDataType.FibrePower_mW] =
                            MzAnalysisWrapper.FixUnderRangeData(diffVsweep.SweepData[ILMZSweepDataType.FibrePower_mW],
                            minLevel_dBm);
                    }
                }
            } while (!dataOk);
        
            // Display data
            engine.SendToGui(diffVsweep);

            // write data to file
            string diffVsweepFileNameStem = string.Format("MzDiffLV_CH{0}_VCM{1}", ituChannelIndex, vcmIndex);
            vcmData.DiffLVSweepFile = Util_GenerateFileName.GenWithTimestamp
                (MzFileDirectory, diffVsweepFileNameStem, dutSerialNbr, "csv");

            MzSweepFileWriter.WriteSweepData(vcmData.DiffLVSweepFile, diffVsweep,
                new ILMZSweepDataType[] 
                {ILMZSweepDataType.LeftArmModBias_V,
                 ILMZSweepDataType.RightArmModBias_V,
                 ILMZSweepDataType.LeftMinusRight,
                 ILMZSweepDataType.FibrePower_mW,
                 ILMZSweepDataType.TapComplementary_mA});

            // analyse the data.
            //
            // The first point needs to be close to zero. After that 
            // search for quadrature close to initialVcmOffset 
            // to help ensure that we pick the same slope each time.

            MzAnalysisWrapper.MzAnalysisResults mzAnlyDiffV;

            if (this.isUseNegSlopeImb)
            {
                mzAnlyDiffV = MzAnalysisWrapper.Differential_NegChirp_DifferentialSweep(diffVsweep, vcm_V, initialVcmOffset);
            }
            else
            {
                mzAnlyDiffV = MzAnalysisWrapper.ZeroChirp_DifferentialSweep(diffVsweep, vcm_V, initialVcmOffset);
            }

            // Add a nice arrow to point at the quadrature point
            NPlot.PointD marker = new NPlot.PointD(mzAnlyDiffV.Quad_SrcL - mzAnlyDiffV.Quad_SrcR, mzAnlyDiffV.PowerAtMax_dBm - 3);
            engine.SendToGui(marker);

            // Add some information to help technicians & engineers
            NPlot.TextItem markerText = new NPlot.TextItem(new NPlot.PointD(marker.X, mzAnlyDiffV.PowerAtMin_dBm + 5), "Vpi = " + mzAnlyDiffV.MzAnalysis.Vpi.ToString("0.0"));
            engine.SendToGui(markerText);

            NPlot.VerticalLine verticalLine = new NPlot.VerticalLine(mzAnlyDiffV.MzAnalysis.VoltageAtMax, System.Drawing.Color.IndianRed);
            engine.SendToGui(verticalLine);

            verticalLine = new NPlot.VerticalLine(mzAnlyDiffV.MzAnalysis.VoltageAtMin, System.Drawing.Color.IndianRed);
            engine.SendToGui(verticalLine);

            if (System.Diagnostics.Debugger.IsAttached)
                System.Threading.Thread.Sleep(2000);

            vcmData.SetValueDouble(MzItuAtVcmMeasurements.VquadL_V, mzAnlyDiffV.Imb_SrcL);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.VquadR_V, mzAnlyDiffV.Imb_SrcR);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.VminL_V, mzAnlyDiffV.Min_SrcL);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.VminR_V, mzAnlyDiffV.Min_SrcR);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.VpeakL_V, mzAnlyDiffV.Max_SrcL);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.VpeakR_V, mzAnlyDiffV.Max_SrcR);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.Vpi_V, mzAnlyDiffV.Vpi);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.ER_dB, mzAnlyDiffV.ExtinctionRatio_dB);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.Isoa_mA, iSOA_mA);

            // Choose subsequent slopes to be close to the first selected slope,
            if (!initialOffsetSelected)
                initialVcmOffset = (vcmData.GetValueDouble(MzItuAtVcmMeasurements.VquadL_V )
                    - vcmData.GetValueDouble(MzItuAtVcmMeasurements.VquadR_V )) / 2;

            initialOffsetSelected = true;

            return vcmData;
        }

       
        /// <summary>
        /// Measure left arm response
        /// </summary>
        /// <param name="engine">engine reference</param>
        /// <returns>Vpi</returns>
        private Alg_MZAnalysis.MZAnalysis measureLeftArmSingleEndedResponse(ITestEngine engine)
        {
            bool dataOk = true;
            double minLevel_dBm = -60;

            // do the left-arm modulation sweep
            engine.SendToGui("Running Left Modulator sweep");
            ILMZSweepResult sweepDataLeft = new ILMZSweepResult();
            do
            {
                if (!engine.IsSimulation)
                {
                    sweepDataLeft = mzDriverUtils.LeftModArm_SingleEndedSweep_ForZCHit2
                     (this.mzInitFixedModBias_volt,
                     this.mzImbLeftIdeal_mA / 1000,
                     this.mzImbRightIdeal_mA / 1000,
                     -this.mzInitSweepMaxAbs_V,
                     0.0,
                     this.NumberPoints,
                     tapInline_V,
                     MZTapBias_V,
		             ArmSourceByAsic);
                    
                }
                #region offline debug
                else
                {
                    // Load plot data for offline debugging
                    /*engine.SendToGui("Loading debug data from file");

                    sweepDataLeft.Type = SweepType.SingleEndVoltage;
                    sweepDataLeft.SrcMeter = SourceMeter.LeftModArm;

                    string fileName = string.Empty;

                    string searchPattern = string.Format("MZCR_LeftMod_LV*.csv");
                    string[] mzFiles = Directory.GetFiles(MzFileDirectory, searchPattern, SearchOption.AllDirectories);
                    if (mzFiles.Length > 0)
                        fileName = mzFiles[0];

                    MzRawData rawData = MzSweepFileWriter.ReadPlotData(fileName);

                    for (int column = 0; column < rawData.plotData.Length; column++)
                    {
                        ILMZSweepDataType sweepType = (ILMZSweepDataType)Enum.Parse(typeof(ILMZSweepDataType), rawData.header[column]);
                        double[] plotArray = (double[])rawData.plotData[column].ToArray(typeof(double));
                        sweepDataLeft.SweepData.Add(sweepType, plotArray);
                    }*/
                }
                #endregion

                // If overrange run again
                if (MzAnalysisWrapper.CheckForOverrange(sweepDataLeft.SweepData[ILMZSweepDataType.FibrePower_mW]))
                {
                    dataOk = false;
                    mzDriverUtils.MzInstrs.PowerMeter.Range = Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.AutoRange;
                    //if (double.IsNaN(mzDriverUtils.MzInstrs.PowerMeter.Range)) // auto range?
                    //    mzDriverUtils.MzInstrs.PowerMeter.Range = 1;
                    //else
                    //    mzDriverUtils.MzInstrs.PowerMeter.Range = mzDriverUtils.MzInstrs.PowerMeter.Range * 10;
                }
                else
                {
                    // if underrange fix the data and continue
                    dataOk = true;
                    if (MzAnalysisWrapper.CheckForUnderrange(sweepDataLeft.SweepData[ILMZSweepDataType.FibrePower_mW]))
                    {
                        sweepDataLeft.SweepData[ILMZSweepDataType.FibrePower_mW] =
                            MzAnalysisWrapper.FixUnderRangeData(sweepDataLeft.SweepData[ILMZSweepDataType.FibrePower_mW]
                            , minLevel_dBm);
                    }
                }
            }
            while (!dataOk);

            string sweepfileName;
            string fileNamePrefix = "MZCR_LeftMod_LV";
            recordSweepData(sweepDataLeft, fileNamePrefix, out sweepfileName);
            LiSweepFile.Add(sweepfileName);

            engine.SendToGui(sweepDataLeft);


            // If debugging, give the engineer a chance to look at the plot
            if (System.Diagnostics.Debugger.IsAttached)
                System.Threading.Thread.Sleep(5000);


            // Alice.huang    2010-05-05
            // use this new function to ensure to find the valley closest to offset
            Alg_MZAnalysis.MZAnalysis mzSeAnly = Alg_MZAnalysis.FindFeaturePointClosestToOffset(
                sweepDataLeft.SweepData[ILMZSweepDataType.LeftArmModBias_V],
                sweepDataLeft.SweepData[ILMZSweepDataType.FibrePower_mW],
                false, 0, Alg_MZAnalysis.MzFeaturePowerType.Min);
                      
            return mzSeAnly;
        }
		/// <summary>
        /// Unse linear interpolation to calculate Vcm for a target Vpi
        /// </summary>
        /// <param name="mzItuChanData"></param>
        /// <param name="vpi"></param>
        /// <returns></returns>
        private double CalculateVcmForMaxVpi(MzSweepDataItuChannel mzItuChanData, double[] vpi, double vpiTarget)
        {
            // Vpi vs Vcm
            double vpiVcmGradient = (vpi[0] - vpi[1]) / (mzItuChanData.VcmData[0].Vcm_V - mzItuChanData.VcmData[1].Vcm_V);
            // Offset required is Y at X=0, so swap X & Y and invert gradient
            double newVcm_V = EstimateXAtY.Calculate(mzItuChanData.VcmData[0].Vcm_V, vpi[0], vpiVcmGradient, vpiTarget);         

            // To avoid driving the device too hard we should clamp the calculated Vcm against the limits
            newVcm_V = Math.Max(newVcm_V, this.vcmMinLimit);
            newVcm_V = Math.Min(newVcm_V, this.vcmMaxLimit);

            return newVcm_V;
        }

        /// <summary>
        /// Add a file to our zip file
        /// </summary>
        /// <param name="zipStream">Zip Output Stream</param>
        /// <param name="file">file name to add (relative path)</param>
        /// <param name="buffer">byte buffer to read data from the file to</param>
        private void addZipFile(ZipOutputStream zipStream, string file, byte[] buffer)
        {
            if (!File.Exists(file)) 
                return;

            ZipEntry entry = new ZipEntry(Path.GetFileName(file));
            entry.DateTime = DateTime.Now;
            zipStream.PutNextEntry(entry);

            using (FileStream fs = File.OpenRead(file))
            {
                // Using a fixed size buffer here makes no noticeable difference for output
                // but keeps a lid on memory usage.
                int sourceBytes;
                do
                {
                    sourceBytes = fs.Read(buffer, 0, buffer.Length);
                    zipStream.Write(buffer, 0, sourceBytes);
                } while (sourceBytes > 0);
            }
            zipStream.Flush();
        }

        /// <summary>
        /// Process the MZ Sweep results, writing out a zip of the sweep results, a summary CSV file and
        /// returning a flag to say if all sweep results passed or not
        /// </summary>
        /// <param name="mzSweepData">Sweep data to be processed</param>
        /// <param name="mzSweepDataResultsFile">Data results file</param>
        /// <param name="mzSweepDataZipFile">Zip sweep file</param>
        /// <param name="sweepsAllPassed">True if all sweep results passed, false if any failed</param>
        private void processMzSweepData(List<MzSweepDataItuChannel> mzSweepData,
            out string mzSweepDataResultsFile, out string mzSweepDataZipFile,
            out bool sweepsAllPassed)
        {
            mzSweepDataZipFile = Util_GenerateFileName.GenWithTimestamp(
                MzFileDirectory, 
                "MzSweepDataZipFile", 
                dutSerialNbr,
                "zip");

            using (ZipOutputStream zipStream = new ZipOutputStream(File.Create(mzSweepDataZipFile)))
            {
                zipStream.SetLevel(9); // 0 - store only to 9 - means best compression

                byte[] buffer = new byte[4096]; // byte buffer to zip into
                foreach (MzSweepDataItuChannel ituChan in mzSweepData)
                {
                    foreach (MzItuAtVcmData vcmMeas in ituChan.VcmData)
                    {
                        addZipFile(zipStream, vcmMeas.DiffLISweepFile, buffer);
                        addZipFile(zipStream, vcmMeas.DiffLVSweepFile, buffer);
                    }
                }
                foreach (string Lifile in LiSweepFile)
                {
                    addZipFile(zipStream, Lifile, buffer);
                }
            }

            sweepsAllPassed = true;

            // write the summary file and check if it all passed
            mzSweepDataResultsFile = Util_GenerateFileName.GenWithTimestamp
                (MzFileDirectory, "MzSweepDataResultsFile", dutSerialNbr, "csv");

            int channel;
            using (StreamWriter writer = new StreamWriter(mzSweepDataResultsFile))
            {
                // file header
                string defaultFileHeader = "ITUChannel,Frequency_GHz,Vcm_V,Status,IImbL_mA,IImbR_mA,VquadL_V,VquadR_V,VpeakL_V,VpeakR_V,VminL_V,VminR_V,Vpi_V, ER_dB";
                StringBuilder fileHeader = new StringBuilder();
                bool fileHeaderOK = false;
                fileHeader.Append("ITUChannel,Frequency_GHz,Vcm_V,Status");

                // file contents
                List<string> fileContents = new List<string>();

                foreach (MzSweepDataItuChannel ituChan in mzSweepData)
                {
                    channel = ituChan.ItuChannelIndex;
                    foreach (MzItuAtVcmData vcmData in ituChan.VcmData)
                    {
                        // fail check
                        if (vcmData.OverallStatus == PassFail.Fail) sweepsAllPassed = false;

                        // each line
                        StringBuilder aLine = new StringBuilder();
                        aLine.Append(channel);
                        aLine.Append(",");
                        aLine.Append(ituChan.ItuFrequency_GHz);
                        aLine.Append(",");
                        aLine.Append(vcmData.Vcm_V);
                        aLine.Append(",");
                        aLine.Append(vcmData.OverallStatus.ToString());

                        // for each parameter...
                        foreach (string nameAsStr in Enum.GetNames(typeof(MzItuAtVcmMeasurements)))
                        {
                            MzItuAtVcmMeasurements parameter = (MzItuAtVcmMeasurements)Enum.Parse(typeof(MzItuAtVcmMeasurements), nameAsStr);

                            // build file header
                            if (!fileHeaderOK)
                            {
                                fileHeader.Append(",");
                                fileHeader.Append(nameAsStr);
                                fileHeader.Append("[");
                                fileHeader.Append(vcmData.GetMeasuredDataLowLimit(parameter));
                                fileHeader.Append(";");
                                fileHeader.Append(vcmData.GetMeasuredDataHighLimit(parameter));
                                fileHeader.Append("]");
                            }

                            // build the line
                            aLine.Append(",");
                            if (vcmData.IsTested(parameter))
                                aLine.Append(vcmData.GetValueDouble(parameter));
                        }

                        if (!fileHeaderOK)
                            fileHeaderOK = true;

                        fileContents.Add(aLine.ToString());
                    }
                }

                // write file header to file
                if (!fileHeaderOK)
                {
                    writer.WriteLine(defaultFileHeader);
                }
                else
                {
                    writer.WriteLine(fileHeader.ToString());
                }

                // writer file contents to file
                foreach (string row in fileContents)
                {
                    writer.WriteLine(row);
                }
            }
        }
        /// <summary>
        /// write arm sweep data to file 
        /// </summary>
        /// <param name="sweepData"> sweep data</param>
        /// <param name="file_prefix"> file name prefix </param>
        /// <param name="sweepFileName"> file name that the sweep data save as </param>
        private void recordSweepData(ILMZSweepResult sweepData, 
                                string file_prefix, out string sweepFileName)
        {
            Dictionary<ILMZSweepDataType, double[]> sweepDataListTemp = sweepData.SweepData;
            List<string> dataNameList = new List<string>();
            List<double[]> sweepDataList = new List<double[]>();
            foreach (ILMZSweepDataType dataName in sweepDataListTemp.Keys)
            {
                dataNameList.Add(dataName.ToString());
                sweepDataList.Add(sweepDataListTemp[dataName]);
            }

            double[][] sweepDataArray = sweepDataList.ToArray();
            string[] dataNameArray = dataNameList.ToArray();

            if ((sweepDataArray.Length > 0) && (dataNameArray[0].Length > 0))
            { }
            else
            {
                sweepFileName = "";
                return;
            }

            string mzSweepDataResultsFile = Util_GenerateFileName.GenWithTimestamp
                (MzFileDirectory, file_prefix, dutSerialNbr, "csv");

            using (StreamWriter writer = new StreamWriter(mzSweepDataResultsFile))
            {
                // file header
                StringBuilder fileHeader = new StringBuilder();

                for (int count = 0; count < dataNameArray.Length; count++)
                {
                    if (count != 0) fileHeader.Append(",");
                    fileHeader.Append(dataNameArray[count]);
                }

                // file contents
                List<string> fileContents = new List<string>();
                for (int irow = 0; irow < sweepDataArray[0].Length; irow++)
                {
                    StringBuilder aLine = new StringBuilder();
                    for (int icol = 0; icol < dataNameArray.Length; icol++)
                    {
                        if (icol != 0) aLine.Append(",");
                        aLine.Append(sweepDataArray[icol][irow].ToString());
                    }
                    fileContents.Add(aLine.ToString());
                }

                // write file header to file
                writer.WriteLine(fileHeader.ToString());

                // writer file contents to file
                foreach (string row in fileContents)
                {
                    writer.WriteLine(row);
                }
            }
            sweepFileName = mzSweepDataResultsFile;
        }

        /// <summary>
        /// Write a single MZ measurement to CSV file
        /// </summary>
        /// <param name="writer">Text Writer that is writing our file</param>
        /// <param name="vcmData">VCM data to write from</param>
        /// <param name="measurement">Which measurement to write?</param>
        private void writeMzMeas(TextWriter writer, MzItuAtVcmData vcmData, MzItuAtVcmMeasurements measurement)
        {
            string valueStr = "";
            if (vcmData.IsTested(measurement))
            {
                double d = vcmData.GetValueDouble(measurement);
                valueStr = d.ToString();
            }
            writer.Write(',');
            writer.Write(valueStr);
        }
        /// <summary>
        /// 
        /// </summary>
        public Type UserControl
        {
            get { return (typeof(IlmzMzCharacteriseGui)); }
        }

        #endregion
    }
}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// IlmzTempTest_ND.cs
//
// Author: Paul.Annetts, Mark Fullalove 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Framework.Limits;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestSolution.ILMZFinalData;
using Bookham.TestLibrary.InstrTypes;
using Bookham.ToolKit.Mz;
using Bookham.TestSolution.IlmzCommonData;
using Bookham.TestSolution.IlmzCommonUtils;
using Bookham.TestSolution.IlmzCommonInstrs;
using Bookham.TestLibrary.Algorithms;
using Bookham.TestLibrary.Instruments;
using Bookham.TestLibrary.Utilities;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// ND Temp Test Module 
    /// </summary>
    public class IlmzTempTest_ND : ITestModule
    {
        #region ITestModule Members

        /// <summary>
        ///  Module Test
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="userType"></param>
        /// <param name="configData"></param>
        /// <param name="instruments"></param>
        /// <param name="chassis"></param>
        /// <param name="calData"></param>
        /// <param name="previousTestData"></param>
        /// <returns></returns>
        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, DatumList configData, InstrumentCollection instruments, ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {
            //get case temp data
            double caseTec_SetPointTemperature_C = configData.ReadDouble("CaseTec_SetPointTemperature_C");
            double caseTec_TemperatureTolerance_C = configData.ReadDouble("CaseTec_TemperatureTolerance_C");
            caseTec = (IInstType_TecController)instruments["CaseTec"];
            double caseTemp_C = caseTec.SensorTemperatureSetPoint_C;
            if ((caseTemp_C > (caseTec_SetPointTemperature_C + caseTec_TemperatureTolerance_C)) || (caseTemp_C < (caseTec_SetPointTemperature_C - caseTec_TemperatureTolerance_C)))
            {
                caseTec.SensorTemperatureSetPoint_C = caseTec_SetPointTemperature_C;
            }
            if (caseTec.OutputEnabled == false)
            {
                caseTec.OutputEnabled = true;
            }
            int tryCount = 0;
            while ((caseTemp_C > (caseTec_SetPointTemperature_C + caseTec_TemperatureTolerance_C)) || (caseTemp_C < (caseTec_SetPointTemperature_C - caseTec_TemperatureTolerance_C)))
            {

                caseTec.SensorTemperatureSetPoint_C = caseTec_SetPointTemperature_C;
                System.Threading.Thread.Sleep(500);
                caseTemp_C = caseTec.SensorTemperatureSetPoint_C;
                if (tryCount > 300)
                {
                    engine.ErrorInModule("can't reach target temperature:" + caseTec_SetPointTemperature_C.ToString() + ",current temperature is :" + caseTemp_C.ToString());
                }
                tryCount++;
            }


            this.failModeCheck = configData.ReadReference("FailModeCheck") as FailModeCheck;

            // previous data
            IlmzChannels tcmzChannels = (IlmzChannels)configData.ReadReference("TcmzItuChannels");
            this.MZSourceMeasureDelay_ms = configData.ReadSint32("MZSourceMeasureDelay_ms");

            MzFileDirectory = configData.ReadString("ResultDir");

            // Init instruments
            dsdbrTec = (IInstType_TecController)instruments["DsdbrTec"];
            if (instruments.Contains("MzTec")) mzTec = (IInstType_TecController)instruments["MzTec"];
            //switchOsaMzOpm = (Switch_Osa_MzOpm)configData.ReadReference("SwitchOsaMzOpm");

            // read config
            TcmzTestTemp testTemp = (TcmzTestTemp)configData.ReadEnum("Temp");
            TestSelection testSelect = (TestSelection)configData.ReadReference("TestSelect");
            tapPhotoCurrentToleranceRatio = configData.ReadDouble("TapPhotoCurrentToleranceRatio");
            DsdbrTuning.LockRatioTolerance = configData.ReadDouble("LockRatioTolerance");

            Specification mainSpec = (Specification)configData.ReadReference("Specification");

            List<bool> passFlags_LastModule = null;
            if (configData.IsPresent("PassFlags_LastModule"))
            {
                passFlags_LastModule = (List<bool>)configData.ReadReference("PassFlags_LastModule");
            }


            if (testTemp == TcmzTestTemp.Low)
            {
                DsdbrTuning.IphaseMax_mA = Convert.ToDouble(mainSpec.GetParamLimit("CH_PHASE_I_TCASE_LOW").HighLimit.ValueToString());
                DsdbrTuning.IphaseMin_mA = Convert.ToDouble(mainSpec.GetParamLimit("CH_PHASE_I_TCASE_LOW").LowLimit.ValueToString());
            }
            else if (testTemp == TcmzTestTemp.High)
            {
                DsdbrTuning.IphaseMax_mA = Convert.ToDouble(mainSpec.GetParamLimit("CH_PHASE_I_TCASE_HIGH").HighLimit.ValueToString());
                DsdbrTuning.IphaseMin_mA = Convert.ToDouble(mainSpec.GetParamLimit("CH_PHASE_I_TCASE_HIGH").LowLimit.ValueToString());
            }

            //Convert.ToDouble(mainSpec.GetParamLimit("TC_MZ_MINIMA_V_LIMIT_MIN").LowLimit.ValueToString());  //by tim
            this.maxArmBias_v = configData.ReadDouble("TC_MZ_MINIMA_V_LIMIT_MIN");
            IlMzDriverUtils.MaxModBias_V = maxArmBias_v;

            Optical_switch = (Inst_Ke2510)instruments["Optical_switch"];
            DsdbrTuning.OpticalSwitch = Optical_switch;
            DsdbrUtils.opticalSwitch = Optical_switch;
            
            double locker_tran_pot = configData.ReadDouble("locker_tran_pot");
            bool DO_GRandR = configData.ReadBool("DO_GRandR");

            // Create some debug information for test time reduction
            if (configData.IsPresent("LoggingEnabled"))
            {
                bool loggingEnabled = configData.ReadBool("LoggingEnabled");
                GuiLoggingEnable guiMsg = new GuiLoggingEnable(loggingEnabled, "TestTime_" + testTemp.ToString());
                engine.SendToGui(guiMsg);
            }

            // Initialise GUI
            engine.GuiShow();
            engine.GuiToFront();
            engine.SendToGui(new GuiTitleMessage("Hitt " + testTemp.ToString() + " temperature tests"));
            engine.SendToGui(new GuiTextMessage("Initialising", 0));

            // MZ driver utils
            IlMzInstruments mzInstrs = (IlMzInstruments)configData.ReadReference("MzInstruments");
            mzDriverUtils = new IlMzDriverUtils(mzInstrs);
            mzDriverUtils.opticalSwitch = Optical_switch;
            mzDriverUtils.locker_port = int.Parse(locker_tran_pot.ToString());

            #region MZ Sweep setup
            TcmzMzSweep_Config mzConfig = new TcmzMzSweep_Config(configData);
            double currentCompliance_A = mzConfig.MZCurrentCompliance_A;
            double currentRange_A = mzConfig.MZCurrentRange_A;
            double voltageCompliance_V = mzConfig.MZVoltageCompliance_V;
            double voltageRange_V = mzConfig.MZVoltageRange_V;
            int numberOfAverages = mzConfig.MzInitNumberOfAverages;
            double integrationRate = mzConfig.MZIntegrationRate;
            double sourceMeasureDelay = mzConfig.MZSourceMeasureDelay_s;
            double powerMeterAveragingTime = mzConfig.MZPowerMeterAveragingTime_s;
            inlineTap_V = mzConfig.MZTapBias_V;
            compTap_V = mzConfig.MZTapBias_V;
            mzLvStepSize = mzConfig.MZInitSweepStepSize_mV;
            //string dutSerialNbr = configData.ReadString("DutSerialNbr");


            engine.SendToGui(new GuiTextMessage("Preparing instruments for sweeping", 0));
            if (!ArmSourceByAsic)
            {
                mzDriverUtils.InitialiseMZArms(currentCompliance_A, currentRange_A);
                mzDriverUtils.InitialiseImbalanceArms(voltageCompliance_V, voltageRange_V);
                mzDriverUtils.InitialiseTaps(currentCompliance_A, currentRange_A);
                mzDriverUtils.SetMeasurementAccuracy(numberOfAverages, integrationRate, sourceMeasureDelay, powerMeterAveragingTime);
                inlineTap_V = mzConfig.MZTapBias_V;
                compTap_V = mzConfig.MZTapBias_V;
                mzLvStepSize = mzConfig.MZInitSweepStepSize_mV;
            }

            mzInstrs.PowerMeter.Mode = Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.MeterMode.Absolute_mW;

            
            #endregion

            //modify by mx for final fix ot
            ILMZChannel[] channelsToTest = DO_GRandR ? tcmzChannels.LastOption : tcmzChannels.PassedMidTemp;

            if (channelsToTest.Length < 1)
            {
                channelsToTest = tcmzChannels.AllOptions;
            }
            //AllOptions;
            // Confirm that at least 1 channel is present
            if (channelsToTest.Length < 1)
            {
                engine.RaiseNonParamFail(0, "No channels found");
            }

            // find the channels with the highest and lowest power dissipation
           // IlmzChannels.ExtremeChannelIndexes extremeChans = tcmzChannels.GetItuExtremeChannels();
            IlmzChannels.ExtremeChannelIndexes extremeChans = (IlmzChannels.ExtremeChannelIndexes)configData.ReadReference("ExtremeChannels");

            // Initialise data
            DatumList returnData = new DatumList();
            bool allPass = true;
            int numChanTested = 0;
            int numChanPass = 0;
            int numLockFails = 0;
            bool highDissTested = false;
            bool lowDissTested = false;

            double BreakFreqIndex = 0.0;

            int channelIndex = -1;

            List<int> channelsIndexes_TestOT = new List<int>();

            // loop through the channels that passed
            foreach (ILMZChannel channel in channelsToTest)
            {
                channelIndex++;

                //double itu_freq = channel.MidTempData.GetValueDouble(EnumTosaParam.ItuFreq_GHz);

                //if (BreakFreqIndex == itu_freq)
                //{ continue; }
                //BreakFreqIndex = itu_freq;

                //ILMZChannel[] a = tcmzChannels.PassedMidTemp;
                //int itu_index = a[69].MidTempData.GetValueSint32(EnumTosaParam.ItuChannelIndex);

                double ituFreq_GHz = channel.MidTempData.GetValueDouble(EnumTosaParam.ItuFreq_GHz);
                IlmzChannelTempMeas meas = new IlmzChannelTempMeas(testTemp);
                IlmzChannelMeas midTempData = channel.MidTempData;
                
                bool doTest = false;

                int ituChannelIndex = midTempData.GetValueSint32(EnumTosaParam.ItuChannelIndex);

                // is this one of the extreme channels?
                if (ituChannelIndex == extremeChans.HighestLaserDissIndex ||
                    ituChannelIndex == extremeChans.LowestLaserDissIndex ||
                    ituChannelIndex == extremeChans.HighestestIsoaIndex ||
                    ituChannelIndex == extremeChans.LowestIsoaIndex ||
                    ituChannelIndex == extremeChans.FirstChannelIndex ||
                    ituChannelIndex == extremeChans.LastChannelIndex ||
                    ituChannelIndex == extremeChans.LowestLockSlopeEffIndex ||
                    ituChannelIndex == extremeChans.HighestLockSlopeEffIndex||
                    DO_GRandR)
                {
                    doTest = true;
                }
                else
                {
                    // is this channel selected in the test select file?
                    switch (testTemp)
                    {
                        case TcmzTestTemp.High:
                            meas = channel.HighTempData;
                            doTest = testSelect.IsTestSelected(ituFreq_GHz, "HighTemp") && passFlags_LastModule[channelIndex];
                            break;
                        case TcmzTestTemp.Low:
                            meas = channel.LowTempData;
                            doTest = testSelect.IsTestSelected(ituFreq_GHz, "LowTemp") && passFlags_LastModule[channelIndex];
                            break;
                        default:
                            this.failModeCheck.RaiseError_Mod_NonParamFail(engine, "Invalid temperature: " + testTemp, FailModeCategory_Enum.SW);
                            break;
                    }
                }

                if (doTest)
                {
                    channelsIndexes_TestOT.Add(channelIndex);

                    engine.SendToGui(new GuiTextMessage("Testing " + ituFreq_GHz.ToString() + " GHz channel", 0));
                    engine.SendToGui(new GuiProgressMessage((numChanTested * 100) / 5));

                    // test the channel
                    ChanTestNumber = numChanTested;
                    bool tempTestSuccess = tempTestChannel(engine, mzInstrs, ituFreq_GHz, meas, midTempData, configData);// false if missing temp test
                    numChanTested++;
                    ChanTestNumber = numChanTested;

                    // Save results
                    switch (testTemp)
                    {
                        case TcmzTestTemp.High:
                            channel.HighTempData = meas;

                            // EOL & High dissipation measurements
                            if (tempTestSuccess)
                            {
                                if (ituChannelIndex == extremeChans.HighestLaserDissIndex)
                                {
                                    highDissTested = true;

                                    // TECs normal 
                                    returnData.AddDouble("HIGH_DISS_OPTICAL_FREQ", channel.MidTempData.GetValueDouble(EnumTosaParam.ItuFreq_GHz));
                                    returnData.AddDouble("HIGH_DISS_SOL", meas.GetValueDouble(TOSATempParam.LaserPowerDiss_W));
                                    returnData.AddDouble("PACK_DISS_TCASE_HIGH", meas.GetValueDouble(TOSATempParam.PackagePowerDiss_W));
                                    returnData.AddDouble("LASER_TEC_I_TCASE_HIGH", meas.GetValueDouble(TOSATempParam.TecDsdbrCurrent_A));
                                    returnData.AddDouble("LASER_TEC_V_TCASE_HIGH", meas.GetValueDouble(TOSATempParam.TecDsdbrVoltage_V));
                                    //returnData.AddDouble("MZ_TEC_I_TCASE_HIGH", meas.GetValueDouble(TOSATempParam.TecMzCurrent_A));
                                    //returnData.AddDouble("MZ_TEC_V_TCASE_HIGH", meas.GetValueDouble(TOSATempParam.TecMzVoltage_V));

                                    // Laser & MZ TEC EOL 
                                    engine.SendToGui(new GuiTextMessage("End Of Life temperature tests", 0));
                                    double laserTemp_C = dsdbrTec.SensorTemperatureSetPoint_C;
                                    //double mzTemp_C = mzTec.SensorTemperatureSetPoint_C;
                                    dsdbrTec.SensorTemperatureSetPoint_C = laserTemp_C - configData.ReadDouble("LaserTecEolDeltaT_C");
                                    //mzTec.SensorTemperatureSetPoint_C = mzTemp_C - configData.ReadDouble("MzTecEolDeltaT_C");
                                    Thread.Sleep(eolTempSettlingTime_s * 1000);
                                    Measurements.PowerDissResults powerResult = Measurements.ElecPowerDissipation(
                                        midTempData.DsdbrSetup.FrontPair, dsdbrTec, mzTec);
                                    returnData.AddDouble("LASER_TEC_I_TCASE_HIGH_EOL", powerResult.TecDsdbr_A);
                                    returnData.AddDouble("LASER_TEC_V_TCASE_HIGH_EOL", powerResult.TecDsdbr_V);
                                    //returnData.AddDouble("MZ_TEC_I_TCASE_HIGH_EOL", powerResult.TecMz_A);
                                    //returnData.AddDouble("MZ_TEC_V_TCASE_HIGH_EOL", powerResult.TecMz_V);
                                    returnData.AddDouble("PACK_DISS_TCASE_HIGH_EOL", powerResult.PackagePower_W);
                                    dsdbrTec.SensorTemperatureSetPoint_C = laserTemp_C;
                                    //mzTec.SensorTemperatureSetPoint_C = mzTemp_C;
                                    Thread.Sleep(eolTempSettlingTime_s * 1000);
                                    engine.SendToGui(new GuiTextMessage("End Of Life temperature tests completed", 0));
                                }

                                #region Trace tone test
                                if (ituChannelIndex == extremeChans.HighestestIsoaIndex && configData.ReadBool("TraceToneTestEnabled"))
                                {
                                    engine.SendToGui(new GuiTextMessage("Soa sweep for trace tone test", 0));
                                    TraceToneTest.PerformTraceSoaSweep(TcmzTestTemp.High);
                                    engine.SendToGui(new GuiTextMessage("Soa sweep for trace tone test completed", 0));
                                }
                                #endregion

                                if (channel.HighTempData.OverallStatus == PassFail.Pass) numChanPass++;
                            }
                            break;
                        case TcmzTestTemp.Low:
                            channel.LowTempData = meas;

                            // Low dissipation measurements
                            if (tempTestSuccess)
                            {
                                if (ituChannelIndex == extremeChans.LowestLaserDissIndex)
                                {
                                    lowDissTested = true;

                                    double a = channel.MidTempData.GetValueDouble(EnumTosaParam.ItuFreq_GHz);
                                    returnData.AddDouble("LOW_DISS_OPTICAL_FREQ", channel.MidTempData.GetValueDouble(EnumTosaParam.ItuFreq_GHz));
                                    returnData.AddDouble("LOW_DISS_SOL", meas.GetValueDouble(TOSATempParam.LaserPowerDiss_W));
                                    returnData.AddDouble("PACK_DISS_TCASE_LOW", meas.GetValueDouble(TOSATempParam.PackagePowerDiss_W));
                                    returnData.AddDouble("LASER_TEC_I_TCASE_LOW", meas.GetValueDouble(TOSATempParam.TecDsdbrCurrent_A));
                                    returnData.AddDouble("LASER_TEC_V_TCASE_LOW", meas.GetValueDouble(TOSATempParam.TecDsdbrVoltage_V));
                                    //returnData.AddDouble("MZ_TEC_I_TCASE_LOW", meas.GetValueDouble(TOSATempParam.TecMzCurrent_A));
                                    //returnData.AddDouble("MZ_TEC_V_TCASE_LOW", meas.GetValueDouble(TOSATempParam.TecMzVoltage_V));
                                }

                                #region Trace tone test
                                if (ituChannelIndex == extremeChans.HighestestIsoaIndex && configData.ReadBool("TraceToneTestEnabled"))
                                {
                                    engine.SendToGui(new GuiTextMessage("Soa sweep for trace tone test", 0));
                                    TraceToneTest.PerformTraceSoaSweep(TcmzTestTemp.Low);
                                    engine.SendToGui(new GuiTextMessage("Soa sweep for trace tone test completed", 0));
                                }
                                #endregion

                                if (channel.LowTempData.OverallStatus == PassFail.Pass) numChanPass++;
                            }
                            break;
                        default:
                            this.failModeCheck.RaiseError_Mod_NonParamFail(engine, "Invalid temperature: " + testTemp, FailModeCategory_Enum.SW);
                            break;
                    }

                    if (tempTestSuccess)
                    {
                        if (meas.GetValueSint32(TOSATempParam.LockOk) == 0)
                        {
                            numLockFails++;
                        }

                        if (allPass && meas.OverallStatus == PassFail.Fail)
                        {
                            allPass = false;
                        }
                    }
                    else
                    {
                        numLockFails++;
                        allPass = false;
                    }

                    if (tempTestSuccess)
                    {
                        if (meas.OverallStatus == PassFail.Pass)
                        {
                            if (passFlags_LastModule != null)
                            {
                                if (passFlags_LastModule[channelIndex])
                                {
                                    channel.MidTempData.SetValueBool(EnumTosaParam.Pass_Flag, true);
                                }
                            }
                            else
                            {
                                channel.MidTempData.SetValueBool(EnumTosaParam.Pass_Flag, true);
                            }

                            engine.SendToGui(new GuiTextMessage("Channel option passed !", 0));
                        }
                        else
                        {
                            channel.MidTempData.SetValueBool(EnumTosaParam.Pass_Flag, false);

                            engine.SendToGui(new GuiTextMessage("Channel option failed !", 0));
                        }
                    }
                    else
                    {
                        channel.MidTempData.SetValueBool(EnumTosaParam.Pass_Flag, false);

                        engine.SendToGui(new GuiTextMessage("Channel option failed !", 0));
                    }

                    if (passFlags_LastModule != null && passFlags_LastModule[channelIndex] == false)
                    {
                        channel.MidTempData.SetValueBool(EnumTosaParam.Pass_Flag, false);
                    }
                }
            }

            engine.SendToGui(new GuiTextMessage("Completed " + testTemp.ToString() + " temperature tests", 0));
            
            // Check if dissipation measurement was done
            switch (testTemp)
            {
                case TcmzTestTemp.High:
                    if (!highDissTested)
                    {
                        returnData.AddDouble("HIGH_DISS_OPTICAL_FREQ", channelsToTest[0].IlmzInitialSettings.Dsdbr.ItuFreq_GHz);
                        returnData.AddDouble("HIGH_DISS_SOL", 0);
                        returnData.AddDouble("PACK_DISS_TCASE_HIGH", 0);
                        returnData.AddDouble("LASER_TEC_I_TCASE_HIGH", 0);
                        returnData.AddDouble("LASER_TEC_V_TCASE_HIGH", 0);
                        returnData.AddDouble("LASER_TEC_I_TCASE_HIGH_EOL", 0);
                        returnData.AddDouble("LASER_TEC_V_TCASE_HIGH_EOL", 0);
                        returnData.AddDouble("PACK_DISS_TCASE_HIGH_EOL", 0);
                    }
                    break;
                case TcmzTestTemp.Low:
                    if (!lowDissTested)
                    {
                        returnData.AddDouble("LOW_DISS_OPTICAL_FREQ", channelsToTest[0].IlmzInitialSettings.Dsdbr.ItuFreq_GHz);
                        returnData.AddDouble("LOW_DISS_SOL", 0);
                        returnData.AddDouble("PACK_DISS_TCASE_LOW", 0);
                        returnData.AddDouble("LASER_TEC_I_TCASE_LOW", 0);
                        returnData.AddDouble("LASER_TEC_V_TCASE_LOW", 0);
                    }
                    break;
                default:
                    this.failModeCheck.RaiseError_Mod_NonParamFail(engine, "Invalid temperature: " + testTemp, FailModeCategory_Enum.SW);
                    break;
            }
            
            // Return data
            returnData.AddSint32("PassFailFlag", (allPass || DO_GRandR) ? 1 : 0);
            returnData.AddSint32("NumChannelsTested", numChanTested);
            returnData.AddSint32("NumChannelsPass", DO_GRandR?numChanTested:numChanPass);
            returnData.AddSint32("NumLockFails", DO_GRandR? 0:numLockFails);
            returnData.AddReference("ChannelsIndexes_TestOT", channelsIndexes_TestOT);
            return returnData;
        }

        /// <summary>
        /// Temperature test at a channel. Return true if ran successfully
        /// </summary>
        /// <param name="engine">Test engine</param>
        /// <param name="mzInstrs">Mz instruments collection</param>
        /// <param name="ituFreq_GHz">The channel ITU frequency in GHz</param>
        /// <param name="meas">Temperature measured data</param>
        /// <param name="midTempData">Mid-temperature measured data</param>
        /// <param name="configData">Configuration data</param>
        /// <returns>Temperature test running status</returns>
        private bool tempTestChannel(ITestEngine engine, IlMzInstruments mzInstrs,
                        double ituFreq_GHz, IlmzChannelTempMeas meas,
                        IlmzChannelMeas midTempData, DatumList configData)
        {
            double freqGHz, freqChange_Ghz;


            engine.SendToGui(new GuiTextMessage("Unlocked measurements", 1));

            // get data from MidTemp to setup the device
            DsdbrChannelSetup dsdbrSetup = midTempData.DsdbrSetup;

            // DSDBR setup - set to ITU freq
            DsdbrUtils.SetDsdbrCurrents_mA(dsdbrSetup);
            DsdbrUtils.SetSectionCurrent_mA(DSDBRSection.Phase, midTempData.GetValueDouble(EnumTosaParam.IPhaseCal_mA));
           
            // MZ setup
            MzData midTempMzSettings = midTempData.MzSettings;          
            MzData newMzSetup = midTempMzSettings;
            if (!ArmSourceByAsic)
            {
                mzDriverUtils.SetupMzToPeak(midTempMzSettings);
            }
            else
            {
                IlMzDriverUtils.SetupMzToPeak(mzInstrs.FCU2Asic, midTempMzSettings);
            }

            // Thermistor resistances
            // Allow a number of attempts to settle
            Thread.Sleep(rthDelay_mS*6);
            int rthCount = 0;
            do
            {
                Thread.Sleep(rthDelay_mS);
                meas.SetValueDouble(TOSATempParam.RthDsdbr_ohm, dsdbrTec.SensorResistanceActual_ohm);
                //meas.SetValueDouble(TOSATempParam.RthMz_ohm, mzTec.SensorResistanceActual_ohm);
            } while ((meas.ParamStatus(TOSATempParam.RthDsdbr_ohm) == PassFail.Fail)
                && rthCount++ < numRthTries);    //    || meas.ParamStatus(TOSATempParam.RthMz_ohm) == PassFail.Fail    

            //double Rth_now = dsdbrTec.SensorResistanceActual_ohm;
            //double Rth_Last = 0;   
            //do
            //{
            //    Rth_Last = Rth_now;
            //    Thread.Sleep(rthDelay_mS);
            //    Rth_now = dsdbrTec.SensorResistanceActual_ohm;
            //    meas.SetValueDouble(TOSATempParam.RthDsdbr_ohm, Rth_now);
                
            //    //meas.SetValueDouble(TOSATempParam.RthMz_ohm, mzTec.SensorResistanceActual_ohm);
            //} while (Math.Abs(Rth_Last - Rth_now) > rthTolerance_ohm && rthCount++ < numRthTries);
            //if (rthCount > numRthTries) return false;
                 

            // Retrieve reference values from MidTemp results to compare against
            double freqReference_Ghz = double.NegativeInfinity;
            double powerPeakReference_dBm = double.NegativeInfinity;
            double tapPhotocurrentReferencePeak_A = double.NegativeInfinity;
            double tapPhotocurrentReferenceQuad_A = double.NegativeInfinity;
            double powerQuadReference_dBm = double.NegativeInfinity;
            double targetMonitorRatio = double.NegativeInfinity;
            double phaseRatioSlopeEff = double.NegativeInfinity;
            double itxLockReference_mA = double.NegativeInfinity;
            double irxLockReference_mA = double.NegativeInfinity;
            try
            {
                freqReference_Ghz = midTempData.GetValueDouble(EnumTosaParam.Freq_GHz);
                powerPeakReference_dBm = midTempData.GetValueDouble(EnumTosaParam.FibrePwrPeak_dBm);
                powerQuadReference_dBm = midTempData.GetValueDouble(EnumTosaParam.FibrePwrQuad_dBm);
                tapPhotocurrentReferencePeak_A = midTempData.GetValueDouble(EnumTosaParam.TapCompPhotoCurrentPeak_mA) / 1000;
                tapPhotocurrentReferenceQuad_A = midTempData.GetValueDouble(EnumTosaParam.TapCompPhotoCurrentQuad_mA) / 1000;
                targetMonitorRatio = midTempData.GetValueDouble(EnumTosaParam.LockRatio);
                phaseRatioSlopeEff = midTempData.GetValueDouble(EnumTosaParam.PhaseRatioSlopeEff);
                itxLockReference_mA = midTempData.GetValueDouble(EnumTosaParam.ItxLock_mA);
                irxLockReference_mA = midTempData.GetValueDouble(EnumTosaParam.IrxLock_mA);
            }
            catch
            {
                engine.SendToGui(new GuiTextMessage("Mid temperature measurement(s) missing !", 0));
                return false; // Indicating tempTestChannel ran unsuccessfully
            }

            // The oftenly use of IsOnline may produce the logging queue full error - chongjian.liang 2013.4.20
            bool isWaveMeterOnline = false;
            if (Measurements.Wavemeter != null)
            {
                isWaveMeterOnline = Measurements.Wavemeter.IsOnline;
            }

            // Unlocked frequency
            if (isWaveMeterOnline)
            {
                freqGHz = Measurements.ReadFrequency_GHz();
            }
            else
            {
                freqGHz = DsdbrTuning.ReadFreq_GhzViaLowCostMeter(freqReference_Ghz);
            }
            freqChange_Ghz = freqGHz - freqReference_Ghz;

            meas.SetValueDouble(TOSATempParam.FreqUnlocked_GHz, freqGHz);
            meas.SetValueDouble(TOSATempParam.FreqUnlockedChange_GHz, freqChange_Ghz);

           
                // Call soft-lock routine
            engine.SendToGui(new GuiTextMessage("Locking frequency", 1));

            DatumList res = DsdbrTuning.SoftLock(targetMonitorRatio, phaseRatioSlopeEff);
            if (res.ReadBool("LockOK") == false)//suggest we can only use DynamicSoftLock and do not need SoftLock. jack.zhang 2013-02-05
            {
                engine.SendToGui(new GuiTextMessage("Locking frequency failed,retry with new phaseRatioSlopeEff.", 2));
                res = DsdbrTuning.DynamicSoftLock(targetMonitorRatio, phaseRatioSlopeEff);
            }

            // Locked frequency measurements
            // Allow a number of attempts to settle
            engine.SendToGui(new GuiTextMessage("Locked measurements", 1));

            meas.SetValueDouble(TOSATempParam.LockerRatio, res.ReadDouble("LockRatio"));
            meas.SetValueDouble(TOSATempParam.IPhaseITU_mA, res.ReadDouble("PhaseCurrent_mA"));
            if (DsdbrUtils.DsdbrDriverInstrsToUse == DsdbrDriveInstruments.FCU2AsicInstrument)
                meas.SetValueDouble(TOSATempParam.IPhaseITU_Dac, res.ReadDouble("PhaseCurrent_Dac"));

            meas.SetValueDouble(TOSATempParam.ItxLock_mA, res.ReadDouble("ITx_mA"));
            meas.SetValueDouble(TOSATempParam.IrxLock_mA, res.ReadDouble("IRx_mA"));
            double itxLockChange = (res.ReadDouble("ITx_mA") - itxLockReference_mA) / itxLockReference_mA * 100;
            meas.SetValueDouble(TOSATempParam.ItxLock_Change, itxLockChange);
            double irxLockChange = (res.ReadDouble("IRx_mA") - irxLockReference_mA) / irxLockReference_mA * 100;
            meas.SetValueDouble(TOSATempParam.IrxLock_Change, irxLockChange);
       
            int lockedMeasCount = 0;
            do
            {
                Thread.Sleep(lockedMeasDelay_mS);

                if (isWaveMeterOnline)
                {
                    freqGHz = Measurements.ReadFrequency_GHz();
                }
                else
                {
                    freqGHz = DsdbrTuning.ReadFreq_GhzViaLowCostMeter(freqReference_Ghz);
                }
                freqChange_Ghz = freqGHz - freqReference_Ghz;
                meas.SetValueDouble(TOSATempParam.FreqLocked_GHz, freqGHz);
                meas.SetValueDouble(TOSATempParam.FreqLockedChange_GHz, freqChange_Ghz);
            }
            while ((meas.ParamStatus(TOSATempParam.FreqLocked_GHz) == PassFail.Fail ||
                meas.ParamStatus(TOSATempParam.FreqLockedChange_GHz) == PassFail.Fail)
                && lockedMeasCount++ < numLockedMeasTries);





            int locked_count = 0;

            while (meas.ParamStatus(TOSATempParam.FreqLockedChange_GHz) == PassFail.Fail && locked_count < 10)
            {

                engine.SendToGui(new GuiTextMessage("Locking frequency again beause lockfreq failed", 1));

                // Call soft-lock routine
                engine.SendToGui(new GuiTextMessage("Locking frequency", 1));
                res = DsdbrTuning.SoftLock(targetMonitorRatio, phaseRatioSlopeEff);

                if (res.ReadBool("LockOK") == false)
                {
                    // Attempt to debug soft locking failures ...
                    engine.SendToGui(new GuiTextMessage("Soft Lock Failed. Trying again with new phaseRatioSlopeEff", 2));
                    //res = DsdbrTuning.SoftLock(targetMonitorRatio, phaseRatioSlopeEff / 2);
                    res = DsdbrTuning.DynamicSoftLock(targetMonitorRatio, phaseRatioSlopeEff);
                }

                // Locked frequency measurements
                // Allow a number of attempts to settle
                engine.SendToGui(new GuiTextMessage("Locked measurements", 1));
                meas.SetValueSint32(TOSATempParam.LockOk, res.ReadBool("LockOK") ? 1 : 0);
                meas.SetValueDouble(TOSATempParam.LockerRatio, res.ReadDouble("LockRatio"));
                meas.SetValueDouble(TOSATempParam.IPhaseITU_mA, res.ReadDouble("PhaseCurrent_mA"));
                if (DsdbrUtils.DsdbrDriverInstrsToUse == DsdbrDriveInstruments.FCU2AsicInstrument)
                    meas.SetValueDouble(TOSATempParam.IPhaseITU_Dac, res.ReadDouble("PhaseCurrent_Dac"));

                meas.SetValueDouble(TOSATempParam.ItxLock_mA, res.ReadDouble("ITx_mA"));
                meas.SetValueDouble(TOSATempParam.IrxLock_mA, res.ReadDouble("IRx_mA"));
                itxLockChange = (res.ReadDouble("ITx_mA") - itxLockReference_mA) / itxLockReference_mA * 100;
                meas.SetValueDouble(TOSATempParam.ItxLock_Change, itxLockChange);
                irxLockChange = (res.ReadDouble("IRx_mA") - irxLockReference_mA) / irxLockReference_mA * 100;
                meas.SetValueDouble(TOSATempParam.IrxLock_Change, irxLockChange);
                locked_count++;

                Thread.Sleep(lockedMeasDelay_mS);

                if (isWaveMeterOnline)
                {
                    freqGHz = Measurements.ReadFrequency_GHz();
                }
                else
                {
                    freqGHz = DsdbrTuning.ReadFreq_GhzViaLowCostMeter(freqReference_Ghz);
                }
                freqChange_Ghz = freqGHz - freqReference_Ghz;
                meas.SetValueDouble(TOSATempParam.FreqLocked_GHz, freqGHz);
                meas.SetValueDouble(TOSATempParam.FreqLockedChange_GHz, freqChange_Ghz);

                if (meas.ParamStatus(TOSATempParam.FreqLockedChange_GHz) == PassFail.Fail)
                {
                    locked_count++;
                }

            }

            //link LockOk with Freqchange but not delt locker ratio. jack.zhang 2013-02-28
            meas.SetValueSint32(TOSATempParam.LockOk, meas.ParamStatus(TOSATempParam.FreqLockedChange_GHz) == PassFail.Pass ? 1 : 0);
            // MZ stuff
            //switchOsaMzOpm.SetState(Switch_Osa_MzOpm.State.MzOpm);
            double Vcm = midTempMzSettings.LeftArmMod_Quad_V + midTempMzSettings.RightArmMod_Quad_V;
            int NumberOfSteps = (int)(1 + Math.Abs(Vcm / (mzLvStepSize / 1000) ));
            mzInstrs.PowerMeter.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_mW;

            //Alice.Huang    2010-08-31
            // set power meter range to its initial range before any sweep 
            mzInstrs.PowerMeter.Range = configData.ReadDouble("OpmRangeInit_mW");

            engine.SendToGui(new GuiTextMessage("MZ LV Scan", 1));

            // MZ LV Scan
            ILMZSweepResult lvSweepData;
            double minLevel_dBm = -60;
            bool dataOk = true;

            //double startBias = Vcm * 2;
            //double stopBias = 0;
            //if (startBias < maxArmBias_v)
            //{
            //    double offset = startBias - maxArmBias_v;
            //    startBias = maxArmBias_v;
            //    stopBias = offset;
            //    //if (startBias > stopBias)
            //    //{
            //    //    //switch start and stop bias value
            //    //    double a = startBias;
            //    //    startBias = stopBias;
            //    //    stopBias = a;
            //    //}
            //}

            IInstType_DigitalIO ctap = Optical_switch.GetDigiIoLine(IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine);//3
            ctap.LineState = IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine_State;//true
            Measurements.FrequencyWithoutMeter = freqReference_Ghz;
            do
            {
            	lvSweepData = mzDriverUtils.ModArm_DifferentialSweepByTrigger(
                midTempMzSettings.LeftArmImb_mA / 1000, midTempMzSettings.RightArmImb_mA / 1000,
                 Vcm, 0.0,
                NumberOfSteps, MZSourceMeasureDelay_ms,inlineTap_V, compTap_V, ArmSourceByAsic);
                
                // If overrange run again
                if (MzAnalysisWrapper.CheckForOverrange(lvSweepData.SweepData[ILMZSweepDataType.FibrePower_mW]))
                {
                    //modify by tim for NaN at 2008-09-05; 
                    dataOk = false;
                    if (double.IsNaN(mzInstrs.PowerMeter.Range))
                        mzInstrs.PowerMeter.Range = 1;
                    else
                        mzInstrs.PowerMeter.Range = mzInstrs.PowerMeter.Range * 10;
                    //End
                }
                else
                {
                    // if underrange fix the data and continue
                    dataOk = true;
                    if (MzAnalysisWrapper.CheckForUnderrange(lvSweepData.SweepData[ILMZSweepDataType.FibrePower_mW]))
                    {
                        lvSweepData.SweepData[ILMZSweepDataType.FibrePower_mW] =
                            MzAnalysisWrapper.FixUnderRangeData(lvSweepData.SweepData[ILMZSweepDataType.FibrePower_mW]
                            , minLevel_dBm);
                    }
                }
            }
            while (!dataOk);

            // store plots
            string LVFileName = Util_GenerateFileName.GenWithTimestamp(MzFileDirectory,
                "FinalDiffMzLV_" + freqReference_Ghz.ToString() + "_" + midTempData.Priority.ToString(),"HG100000.000", "csv");
            // write to file
            MzSweepFileWriter.WriteSweepData(LVFileName, lvSweepData,
                new ILMZSweepDataType[] { ILMZSweepDataType.LeftArmModBias_V,
                                        ILMZSweepDataType.RightArmModBias_V,
                                        ILMZSweepDataType.LeftMinusRight,
                                        ILMZSweepDataType.FibrePower_mW,
                                        ILMZSweepDataType.TapComplementary_mA});
            engine.SendToGui(lvSweepData);
                             
            MzAnalysisWrapper.MzAnalysisResults mzAnlyDiffV =
                MzAnalysisWrapper.Differential_NegChirp_DifferentialSweep(lvSweepData, Vcm / 2, 0);

            
            newMzSetup.LeftArmMod_Peak_V = mzAnlyDiffV.Max_SrcL;
            newMzSetup.RightArmMod_Peak_V = mzAnlyDiffV.Max_SrcR;
            
            // Record actual peak voltages and change in voltages
            meas.SetValueDouble(TOSATempParam.MzPeakModLeft_V, newMzSetup.LeftArmMod_Peak_V);
            meas.SetValueDouble(TOSATempParam.MzPeakModRight_V, newMzSetup.RightArmMod_Peak_V);
            double mzModChange_V;
            mzModChange_V = newMzSetup.LeftArmMod_Peak_V - midTempMzSettings.LeftArmMod_Peak_V;
            meas.SetValueDouble(TOSATempParam.MzPeakModLeftChange_V, mzModChange_V);
            mzModChange_V = newMzSetup.RightArmMod_Peak_V - midTempMzSettings.RightArmMod_Peak_V;
            meas.SetValueDouble(TOSATempParam.MzPeakModRightChange_V, mzModChange_V);

            // MZ peak open loop measurements (tap not power levelled)
            //Alice.Huang    2010-08-31
            // set power meter range to auto range after sweep finished 
            mzInstrs.PowerMeter.Range = InstType_OpticalPowerMeter .AutoRange;

            engine.SendToGui(new GuiTextMessage("MZ peak measurements", 1));
            IlMzDriverUtils.SetupMzToPeak(mzInstrs.FCU2Asic,newMzSetup);
            Thread.Sleep(250);
            double power_dBm = Measurements.ReadOpticalPower(OpticalPowerHead.MZHead, Measurements.PowerUnits.dBm);
            double powerChange_dB = power_dBm - powerPeakReference_dBm;
            meas.SetValueDouble(TOSATempParam.PwrAtPeak_OL_dBm, power_dBm);
            meas.SetValueDouble(TOSATempParam.PwrAtPeakChange_OL_dB, powerChange_dB);
            IInstType_DigitalIO ctapLine = Optical_switch.GetDigiIoLine(IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine);//3
            ctapLine.LineState = IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine_State;//true
            meas.SetValueDouble(TOSATempParam.TapCompPhotoCurrentPeak_OL_mA, mzInstrs.FCU2Asic.ICtap_mA);
                                

            // MZ quad open loop measurements (tap not power levelled)
            engine.SendToGui(new GuiTextMessage("MZ quad point measurements", 1));
            IlMzDriverUtils.SetupMzToQuad(mzInstrs.FCU2Asic, newMzSetup);
            Thread.Sleep(250);
            power_dBm = Measurements.ReadOpticalPower(OpticalPowerHead.MZHead, Measurements.PowerUnits.dBm);
            powerChange_dB = power_dBm - powerQuadReference_dBm;
            meas.SetValueDouble(TOSATempParam.PwrAtQuad_OL_dBm, power_dBm);
            meas.SetValueDouble(TOSATempParam.PwrAtQuadChange_OL_dB, powerChange_dB);
            meas.SetValueDouble(TOSATempParam.TapCompPhotoCurrentQuad_OL_mA,
                                mzInstrs.FCU2Asic.ICtap_mA);

            if (configData.ReadBool("DoTapLevelling"))
            {
                // MZ quad closed loop measurements (tap power levelled)
                engine.SendToGui(new GuiTextMessage("MZ quad point measurements", 1));
                newMzSetup.LeftArmMod_Quad_V = mzAnlyDiffV.Quad_SrcL;
                newMzSetup.RightArmMod_Quad_V = mzAnlyDiffV.Quad_SrcR;
                //mzDriverUtils.SetupMzToQuad(newMzSetup);
                IlMzDriverUtils.SetupMzToQuad(mzInstrs.FCU2Asic, newMzSetup);
                Thread.Sleep(250);

                // tune SOA power / power level to tap
                engine.SendToGui(new GuiTextMessage("Adjusting SOA power using complementary tap", 1));
                SoaPowerLevel.TapLevelRes tapLevelResults = SoaPowerLevel.TapLevelWithAdaptiveSlope(
                    mzDriverUtils.MzInstrs.FCU2Asic,
                    tapPhotocurrentReferenceQuad_A,
                    tapPhotoCurrentToleranceRatio);
                meas.SetValueBool(TOSATempParam.TapLevellingOk, tapLevelResults.LevelOk);
                meas.SetValueDouble(TOSATempParam.ISoaTapLevel_mA, tapLevelResults.SoaCurrent_mA);

                if (DsdbrUtils.DsdbrDriverInstrsToUse == DsdbrDriveInstruments.FCU2AsicInstrument)
                    meas.SetValueDouble(TOSATempParam.ISoaTapLevel_Dac, (double) tapLevelResults.SoaCurrent_Dac);
                                                
                power_dBm = Measurements.ReadOpticalPower(OpticalPowerHead.MZHead, Measurements.PowerUnits.dBm);
                powerChange_dB = power_dBm - powerQuadReference_dBm;
                meas.SetValueDouble(TOSATempParam.PwrAtQuad_CL_dBm, power_dBm);
                meas.SetValueDouble(TOSATempParam.PwrAtQuadChange_CL_dB, powerChange_dB);
                meas.SetValueDouble(TOSATempParam.TapCompPhotoCurrentQuad_CL_mA, mzInstrs.FCU2Asic.ICtap_mA);

                // MZ peak closed loop measurements (tap power levelled)
                engine.SendToGui(new GuiTextMessage("MZ peak measurements", 1));
                IlMzDriverUtils.SetupMzToPeak(mzInstrs.FCU2Asic,newMzSetup);
                Thread.Sleep(250);

                power_dBm = Measurements.ReadOpticalPower(OpticalPowerHead.MZHead, Measurements.PowerUnits.dBm);
                powerChange_dB = power_dBm - powerPeakReference_dBm;
                meas.SetValueDouble(TOSATempParam.PwrAtPeak_CL_dBm, power_dBm);
                meas.SetValueDouble(TOSATempParam.PwrAtPeakChange_CL_dB, powerChange_dB);
                meas.SetValueDouble(TOSATempParam.TapCompPhotoCurrentPeak_CL_mA, mzInstrs.FCU2Asic.ICtap_mA);
            }

            // Set MZ to peak
            IlMzDriverUtils.SetupMzToPeak(mzInstrs.FCU2Asic, newMzSetup);
            Thread.Sleep(250);

            // Record TEC volts and currents, and total power
            engine.SendToGui(new GuiTextMessage("Electrical power measurements", 1));
            Measurements.PowerDissResults powerResult =  Measurements.ElecPowerDissipation(
                dsdbrSetup.FrontPair, dsdbrTec, mzTec);
            meas.SetValueDouble(TOSATempParam.TecDsdbrCurrent_A, powerResult.TecDsdbr_A);
            meas.SetValueDouble(TOSATempParam.TecDsdbrVoltage_V, powerResult.TecDsdbr_V);
            //meas.SetValueDouble(TOSATempParam.TecMzCurrent_A, powerResult.TecMz_A);
            //meas.SetValueDouble(TOSATempParam.TecMzVoltage_V, powerResult.TecMz_V);
            meas.SetValueDouble(TOSATempParam.LaserCurrentSum_mA, powerResult.LaserTotalCurrent_A * 1000);
            meas.SetValueDouble(TOSATempParam.LaserPowerDiss_W, powerResult.LaserPower_W);
            meas.SetValueDouble(TOSATempParam.PackagePowerDiss_W, powerResult.PackagePower_W);

            if (configData.ReadBool("DoPwrCtrlRangeTests"))
            {
                // Power Control Range tests
                double targetPowerLowDbm = configData.ReadDouble("PwrControlRangeLow");
                double targetPowerHighDbm = configData.ReadDouble("PwrControlRangeHigh");
                double tune_OpticalPowerTolerance_dB = configData.ReadDouble("Tune_OpticalPowerTolerance_dB");
                double tune_OpticalPowerTolerance_mW = configData.ReadDouble("Tune_OpticalPowerTolerance_mW");
                double tune_IsoaPowerSlope = configData.ReadDouble("Tune_IsoaPowerSlope");
                double maxSoaForPwrLeveling = configData.ReadDouble("MaxSoaForPwrLeveling");

                DsdbrUtils.LockerCurrents lc;
                IlMzDriverUtils mzUtils = new IlMzDriverUtils(mzInstrs);

                // Set MZ to peak
                IlMzDriverUtils.SetupMzToPeak(mzInstrs.FCU2Asic, newMzSetup);
                Thread.Sleep(250);

                // Low power
                double targetPowerLow_mW = Alg_PowConvert_dB.Convert_dBmtomW(targetPowerLowDbm);
                DatumList powerLevelResults = IlmzCommonUtils.SoaPowerLevel.PowerLevelWithAdaptiveSlope(
                    targetPowerLow_mW, tune_OpticalPowerTolerance_mW, tune_IsoaPowerSlope,maxSoaForPwrLeveling);
                bool powerLevelOk = powerLevelResults.ReadBool("PowerLevelOk");
                if (powerLevelOk)
                {
                    meas.SetValueDouble(TOSATempParam.PwrCtrlMinPower_dBm,
                    powerLevelResults.ReadDouble("lastTunedPower_dBm"));
                    meas.SetValueDouble(TOSATempParam.PwrCtrlMinIsoa_mA,
                        powerLevelResults.ReadDouble("lastTunedIsoa"));
                    if (DsdbrUtils.DsdbrDriverInstrsToUse == DsdbrDriveInstruments.FCU2AsicInstrument)
                        meas .SetValueDouble(TOSATempParam .PwrCtrlMinIsoa_Dac,
                            powerLevelResults.ReadDouble("lastTunedIsoaDac"));
                }
                else
                {
                    meas.SetValueDouble(TOSATempParam.PwrCtrlMinPower_dBm,
                        powerLevelResults.ReadDouble("powerAtMaxIsoa_dBm"));
                    meas.SetValueDouble(TOSATempParam.PwrCtrlMinIsoa_mA,
                        powerLevelResults.ReadDouble("maxIsoa_mA"));
                    if (DsdbrUtils.DsdbrDriverInstrsToUse == DsdbrDriveInstruments.FCU2AsicInstrument)
                        meas .SetValueDouble (TOSATempParam .PwrCtrlMinIsoa_Dac,
                            powerLevelResults.ReadDouble("maxIsoa_Dac"));
                }
                lc = DsdbrUtils.ReadLockerCurrents();
                meas.SetValueDouble(TOSATempParam.PwrCtrlMinItx_mA, lc.TxCurrent_mA);
                meas.SetValueDouble(TOSATempParam.PwrCtrlMinIrx_mA, lc.RxCurrent_mA);
                double pwrCtrlMinItxChange = (lc.TxCurrent_mA - itxLockReference_mA) / itxLockReference_mA * 100;
                meas.SetValueDouble(TOSATempParam.PwrCtrlMinItx_Change, pwrCtrlMinItxChange);
                double pwrCtrlMinIrxChange = (lc.RxCurrent_mA - irxLockReference_mA) / irxLockReference_mA * 100;
                meas.SetValueDouble(TOSATempParam.PwrCtrlMinIrx_Change, pwrCtrlMinIrxChange);
                powerResult = Measurements.ElecPowerDissipation(dsdbrSetup.FrontPair, dsdbrTec, mzTec);
                //meas.SetValueDouble(TOSATempParam.PwrCtrlMinIsoa_mA, powerResult.SOA_A * 1000); 
                meas.SetValueDouble(TOSATempParam.PwrCtrlMinLaserCurrentSum_mA, powerResult.LaserTotalCurrent_A * 1000);
                meas.SetValueDouble(TOSATempParam.PwrCtrlMinLaserDiss_W, powerResult.LaserPower_W);
                //if (mzInstrs.InlineTapOnline)
                //    meas.SetValueDouble(TOSATempParam.PwrCtrlMinInlineTap_mA, mzUtils.ReadCurrent(SourceMeter.InlineTap) * 1000);

                // High power
                double targetPowerHigh_mW = Alg_PowConvert_dB.Convert_dBmtomW(targetPowerHighDbm);
                powerLevelResults = IlmzCommonUtils.SoaPowerLevel.PowerLevelWithAdaptiveSlope(
                    targetPowerHigh_mW, tune_OpticalPowerTolerance_mW, tune_IsoaPowerSlope,maxSoaForPwrLeveling);
                powerLevelOk = powerLevelResults.ReadBool("PowerLevelOk");
                if (powerLevelOk)
                {
                    meas.SetValueDouble(TOSATempParam.PwrCtrlMaxPower_dBm,
                        powerLevelResults.ReadDouble("lastTunedPower_dBm"));
                    meas.SetValueDouble(TOSATempParam.PwrCtrlMaxIsoa_mA,
                        powerLevelResults.ReadDouble("lastTunedIsoa"));
                    if (DsdbrUtils.DsdbrDriverInstrsToUse == DsdbrDriveInstruments.FCU2AsicInstrument)
                        meas.SetValueDouble(TOSATempParam.PwrCtrlMaxIsoa_Dac,
                            powerLevelResults.ReadDouble("lastTunedIsoaDac"));
                }
                else
                {
                    meas.SetValueDouble(TOSATempParam.PwrCtrlMaxPower_dBm,
                    powerLevelResults.ReadDouble("powerAtMaxIsoa_dBm"));
                    meas.SetValueDouble(TOSATempParam.PwrCtrlMaxIsoa_mA,
                        powerLevelResults.ReadDouble("maxIsoa_mA"));
                    if (DsdbrUtils.DsdbrDriverInstrsToUse == DsdbrDriveInstruments.FCU2AsicInstrument)
                        meas.SetValueDouble(TOSATempParam.PwrCtrlMaxIsoa_Dac,
                            powerLevelResults.ReadDouble("maxIsoa_Dac"));
                }
                lc = DsdbrUtils.ReadLockerCurrents();
                meas.SetValueDouble(TOSATempParam.PwrCtrlMaxItx_mA, lc.TxCurrent_mA);
                meas.SetValueDouble(TOSATempParam.PwrCtrlMaxIrx_mA, lc.RxCurrent_mA);
                double pwrCtrlMaxItxChange = (lc.TxCurrent_mA - itxLockReference_mA) / itxLockReference_mA * 100;
                meas.SetValueDouble(TOSATempParam.PwrCtrlMaxItx_Change, pwrCtrlMaxItxChange);
                double pwrCtrlMaxIrxChange = (lc.RxCurrent_mA - irxLockReference_mA) / irxLockReference_mA * 100;
                meas.SetValueDouble(TOSATempParam.PwrCtrlMaxIrx_Change, pwrCtrlMaxIrxChange);
                powerResult = Measurements.ElecPowerDissipation(dsdbrSetup.FrontPair, dsdbrTec, mzTec);
                //meas.SetValueDouble(TOSATempParam.PwrCtrlMaxIsoa_mA, powerResult.SOA_A * 1000); 
                meas.SetValueDouble(TOSATempParam.PwrCtrlMaxLaserCurrentSum_mA, powerResult.LaserTotalCurrent_A * 1000);
                meas.SetValueDouble(TOSATempParam.PwrCtrlMaxLaserDiss_W, powerResult.LaserPower_W);
                //if (mzInstrs.InlineTapOnline)
                //    meas.SetValueDouble(TOSATempParam.PwrCtrlMaxInlineTap_mA, mzUtils.ReadCurrent(SourceMeter.InlineTap) * 1000);
            }

            // Set power back to tap level value in case we need to do EOL measurements
            if (configData.ReadBool("DoTapLevelling"))
            {
                DsdbrUtils.SetSectionCurrent_mA(DSDBRSection.SOA, meas.GetValueDouble(TOSATempParam.ISoaTapLevel_mA));
            }
            else
            {
                DsdbrUtils.SetSectionCurrent_mA(DSDBRSection.SOA, midTempData.GetValueDouble(EnumTosaParam.ISoa_mA));
            }

            return true; // Indicating tempTestChannel ran OK
        }

        public Type UserControl
        {
            get { return (typeof(IlmzTempTestGui)); }
        }

        #endregion

        #region Private data
        private int ChanTestNumber = 0;
        // Instruments
        IInstType_TecController dsdbrTec;
        IInstType_TecController caseTec;
        IInstType_TecController mzTec=null ;
        Inst_Ke2510 Optical_switch=null;
        
        //Switch_Osa_MzOpm switchOsaMzOpm;

        // MZ data
        IlMzDriverUtils mzDriverUtils;
        double inlineTap_V;
        double compTap_V;
        
        double mzLvStepSize;
        int MZSourceMeasureDelay_ms = 5;
        // Tap tuning
        double tapPhotoCurrentToleranceRatio;

        // EOL 
        const int eolTempSettlingTime_s = 15;

        const int numRthTries = 10;
        const int rthDelay_mS = 500;
        const int numLockedMeasTries = 2;
        const int lockedMeasDelay_mS = 250;
        bool ArmSourceByAsic = true;
        double rthTolerance_ohm = 10;

        string MzFileDirectory;
        double maxArmBias_v;
        private FailModeCheck failModeCheck;
        
        #endregion
    }
}

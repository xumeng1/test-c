// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// TcmzTempTestGui.cs
//
// Author: Tony Foster, Mark Fullalove 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Bookham.TestEngine.Framework.Logging;
using Bookham.ToolKit.Mz;
using Bookham.TestLibrary.Algorithms;
using NPlot;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// TCMZ Temperature test GUI User Control
    /// </summary>
    public partial class IlmzTempTestGui : Bookham.TestEngine.Framework.Messages.ManagedCtlBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public IlmzTempTestGui()
        {
            /* Call designer generated code. */
            InitializeComponent();
            previousMsg = new GuiTextMessage("", 0);
        }

        /// <summary>
        /// Incoming message event handler
        /// </summary>
        /// <param name="payload">Message Payload</param>
        /// <param name="inMsgSeq">Incoming message sequence number</param>
        /// <param name="respSeq">Outgoing message sequence number</param>
        private void ModuleGui_MsgReceived(object payload, long inMsgSeq, long respSeq)
        {
            // Handle payloads
            if (payload.GetType() == typeof(GuiTextMessage))
            {
                GuiTextMessage txtMsg = (GuiTextMessage)payload;
                addTextMessage(txtMsg);
            }
            else if (payload.GetType() == typeof(GuiTitleMessage))
            {
                GuiTitleMessage titleMsg = (GuiTitleMessage)payload;
                title.Text = titleMsg.titleText;
            }
            else if (payload.GetType() == typeof(GuiProgressMessage))
            {
                GuiProgressMessage progressMsg = (GuiProgressMessage)payload;
                progressBar1.Value = progressMsg.progressPercent;
            }
            else if (payload.GetType() == typeof(GuiPlotData))
            {
                GuiPlotData guiPlotData = (GuiPlotData)payload;
                mzPlotTitle.Text = guiPlotData.plotTitle;

                mzPlot.Clear();
                LinePlot plotData = new LinePlot(guiPlotData.yData, guiPlotData.xData);
                plotData.Color = Color.DarkBlue;
                plotData.Label = guiPlotData.plotTitle;
                mzPlot.Add(plotData, PlotSurface2D.XAxisPosition.Bottom, PlotSurface2D.YAxisPosition.Right, 1);

                if (mzPlot.YAxis2 != null)
                    mzPlot.YAxis2.Label = guiPlotData.yDataTitle;

                this.mzPlot.Visible = true;
                mzPlot.Refresh();
            }
            else if (payload.GetType() == typeof(ILMZSweepResult))
            {
                ILMZSweepResult sweepData = (ILMZSweepResult)payload;

                double[] xData = new double[0];
                if (sweepData.Type == SweepType.DifferentialCurrent)
                {
                    double[] leftData = sweepData.SweepData[ILMZSweepDataType.LeftArmImb_A];
                    double[] rightData = sweepData.SweepData[ILMZSweepDataType.RightArmImb_A];
                    xData = Alg_ArrayFunctions.SubtractArrays(leftData, rightData);
                    mzPlotTitle.Text = "Differential current (mA)";
                }
                if (sweepData.Type == SweepType.DifferentialVoltage)
                {
                    double[] leftData = sweepData.SweepData[ILMZSweepDataType.LeftArmModBias_V];
                    double[] rightData = sweepData.SweepData[ILMZSweepDataType.RightArmModBias_V];
                    xData = Alg_ArrayFunctions.SubtractArrays(leftData, rightData);
                    mzPlotTitle.Text = "Differential bias (V)";
                }
                if (sweepData.Type == SweepType.SingleEndVoltage)
                {
                    if (sweepData.SrcMeter == SourceMeter.RightModArm)
                    {
                        xData = sweepData.SweepData[ILMZSweepDataType.RightArmModBias_V];
                        mzPlotTitle.Text = "Right Arm bias (V)";
                    }
                    else
                    {
                        xData = sweepData.SweepData[ILMZSweepDataType.LeftArmModBias_V];
                        mzPlotTitle.Text = "Left Arm bias (V)";
                    }
                }

                double[] yData = Alg_PowConvert_dB.Convert_mWtodBm(sweepData.SweepData[ILMZSweepDataType.FibrePower_mW]);

                mzPlot.Clear();
                LinePlot plotData = new LinePlot(yData, xData);
                plotData.Color = Color.DarkBlue;
                plotData.Label = "Optical Power (dBm)";
                mzPlot.Add(plotData, PlotSurface2D.XAxisPosition.Bottom, PlotSurface2D.YAxisPosition.Right, 1);

                if (mzPlot.YAxis2 != null)
                    mzPlot.YAxis2.Label = "Optical power (dBm)";

                this.mzPlot.Visible = true;
                mzPlot.Refresh();

            }
            else if (payload.GetType() == typeof(GuiLoggingEnable))
            {
                GuiLoggingEnable enableMsg = (GuiLoggingEnable)payload;
                this.loggingEnabled = enableMsg.loggingEnabled;
                this.messageLabel = enableMsg.messageLabel;
            }
            else
            {
                GuiTextMessage otherMsg = new GuiTextMessage(payload.ToString(), 0);
                addTextMessage(otherMsg);
            }
        }

        // Add text message
        private void addTextMessage(GuiTextMessage msg)
        {
            if (msg.msgText.Length > 0)
            {
                if (previousMsg.msgText.Length > 0)
                    msgList.Items.Insert(0, previousMsg.msgTime + "  " + previousMsg.insetPad + previousMsg.msgText);
                currentMsg.Text = msg.msgTime + "  " + msg.msgText;
                previousMsg = msg;

                //if (loggingEnabled)
                //    Log.DeveloperWrite(messageLabel, msg.msgTime + "  " + msg.msgText);
            }
        }

        private GuiTextMessage previousMsg;
        private bool loggingEnabled = false;
        private string messageLabel;
    }


    // Text message 
    internal class GuiTextMessage
    {
        internal GuiTextMessage(string messageText, int insetLevel)
        {
            this.msgText = messageText;
            this.insetLevel = insetLevel;
            this.msgTime = DateTime.Now.ToString("HH:mm:ss");
            this.insetPad = new string(' ', this.insetLevel * 4);
        }

        internal readonly string msgText;
        internal readonly string msgTime;
        internal readonly string insetPad;
        internal readonly int insetLevel;
    }

    // Title message
    internal class GuiTitleMessage
    {
        internal GuiTitleMessage(string titleText)
        {
            this.titleText = titleText;
        }

        internal readonly string titleText;
    }

    // Progress bar setting
    internal class GuiProgressMessage
    {
        internal GuiProgressMessage(int progressPercent)
        {
            this.progressPercent = Math.Max(progressPercent, 0);
            this.progressPercent = Math.Min(this.progressPercent, 100);
        }

        internal readonly int progressPercent;
    }
    internal class GuiPlotData
    {
        internal GuiPlotData(double[] xData, double[] yData, string title, string yDataTitle)
        {
            this.plotTitle = title;
            this.yDataTitle = yDataTitle;
            this.xData = xData;
            this.yData = yData;
        }
        internal readonly double[] xData;
        internal readonly double[] yData;
        internal readonly string plotTitle;
        internal readonly string yDataTitle;
    }
    /// <summary>
    /// Custom message to enable logging of test times.
    /// </summary>
    internal class GuiLoggingEnable
    {
        internal GuiLoggingEnable(bool loggingEnabled, string messageLabel)
        {
            this.loggingEnabled = loggingEnabled;
            this.messageLabel = messageLabel;
        }

        internal readonly bool loggingEnabled;
        internal readonly string messageLabel;
    }
}

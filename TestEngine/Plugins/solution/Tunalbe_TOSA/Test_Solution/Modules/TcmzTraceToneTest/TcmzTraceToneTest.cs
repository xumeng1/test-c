// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// TcmzTraceToneTest.cs
//
// Author: mark.fullalove, 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestSolution.IlmzCommonData;
using Bookham.TestSolution.ILMZFinalData;
using Bookham.TestSolution.IlmzCommonInstrs;
using Bookham.TestSolution.IlmzCommonUtils;
using Bookham.ToolKit.Mz;
using Bookham.TestLibrary.Algorithms;
using Bookham.TestLibrary.Utilities;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Test Module Template.
    /// </summary>
    public class TcmzTraceToneTest : ITestModule
    {
        #region ITestModule Members

        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, DatumList configData, InstrumentCollection instruments, ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {
            string plotFileName = Util_GenerateFileName.GenWithTimestamp(configData.ReadString("PlotFileDirectory"), "TraceToneResults", configData.ReadString("DutSerialNumber"), "csv");
            DatumList returnData = TraceToneTest.PerformTraceToneTest(plotFileName);
            returnData.AddFileLink("TraceSoaCurves", plotFileName);
            return returnData;
        }

        public Type UserControl
        {
            get { return (typeof(TcmzTraceToneTestGui)); }
        }

        #endregion
    }
}

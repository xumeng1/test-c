// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// IlmzChannelChar_ZD.cs
//
// Author: Tony.Foster, Mark Fullalove 2007
// ILMZ GB version : Mark Fullalove 2011
// Design: [Reference design documentation]
//

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Framework.Limits;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestLibrary.Utilities;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.Algorithms;
using Bookham.Toolkit.CloseGrid;//jack.Zhang
using Bookham.ToolKit.Mz;
using Bookham.TestSolution.ILMZFinalData;
using Bookham.TestSolution.IlmzCommonData;
using Bookham.TestSolution.IlmzCommonInstrs;
using Bookham.TestSolution.IlmzCommonUtils;
using Bookham.TestSolution.TestModules;
using Bookham.TestLibrary.Instruments;


namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// TCMZ Initialise Module 
    /// </summary>
    public class IlmzChannelChar_ZD : ITestModule
    {
        #region ITestModule Members

        /// <summary>
        /// 
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="userType"></param>
        /// <param name="configData"></param>
        /// <param name="instruments"></param>
        /// <param name="chassis"></param>
        /// <param name="calData"></param>
        /// <param name="previousTestData"></param>
        /// <returns></returns>
        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType,
            DatumList configData, InstrumentCollection instruments, 
            ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {
            // Initialise GUI
            engine.GuiShow();
            engine.GuiToFront();
            engine.SendToGui(new GuiTitleMessage("ILMZ Channel Characterisation"));
            engine.SendToGui(new GuiTextMessage("Initialising", 0));

            failModeCheck = configData.ReadReference("FailModeCheck") as FailModeCheck;

            // Initialise ILMZ Channels
            ilmzItuChannels = (IlmzChannels)configData.ReadReference("IlmzItuChannels");

            // Initialise Configs
            ilmzChannelChar_ZD_configObj = new TcmzChannelChar_ZD_Config(configData);
            mzConfig = new TcmzMzSweep_Config(configData);

            // Initialise Local Data
            mainSpec = (Specification)configData.ReadReference("Specification");
            mzFileDirectory = configData.ReadString("ResultDire");
            this.testEvenITUFail = configData.ReadBool("TestEvenITUFail");
            int numChans = configData.GetDatumSint32("NumChans").Value;
            freqLow_GHz = configData.ReadDouble("FreqLow_GHz");
            freqHigh_GHz = configData.ReadDouble("FreqHigh_GHz");
            freqSpace_GHz = configData.ReadDouble("FreqSpace_GHz");
            targetPowerDbm = configData.ReadDouble("TargetFibrePower_dBm");
            calOffset_GHz = configData.ReadDouble("CalOffset");
            dutSerialNbr = configData.ReadString("DutSerialNbr");
            opmRange = configData.ReadDouble("OpmRangeInit_mW");
            powerLevelingBreakCount = (uint)configData.ReadUint32("BreakPowerLevelingCount");
            powerLevelingFailedCount = 0;
            alwaysTuneLaser = configData.ReadBool("AlwaysTuneLaser");
            mzImbMinLimit_mA = ParamManager.Spec.CH_MZ_CTRL_L_I.Low;
            mzImbMaxLimit_mA = ParamManager.Spec.CH_MZ_CTRL_L_I.High;
            maxArmBias_v = Convert.ToDouble(mainSpec.GetParamLimit("TC_MZ_MINIMA_V_LIMIT_MIN").LowLimit.ValueToString());  // -5V
            this.isUseNegSlopeImb = configData.ReadBool("IsUseNegSlopeImbInZC");

            bool isUseDynamicER = configData.ReadBool("IsUseDynamicER");

            dynamicER_LowLimits = configData.ReadReference("DynamicER_LowLimits") as Dictionary<double, double>;
            dynamicER_HighLimits = configData.ReadReference("DynamicER_HighLimits") as Dictionary<double, double>;
            
            double erLimit_Low = Convert.ToDouble(mainSpec.GetParamLimit("CH_MZ_ER").LowLimit.ValueToString());
            double erLimit_High = Convert.ToDouble(mainSpec.GetParamLimit("CH_MZ_ER").HighLimit.ValueToString());

            IPhaseModAcqFilesCollection = configData.ReadString("IPhaseModAcqFilesCollection");
            maxSoaForPwrLeveling = configData.ReadDouble("MaxSoaForPwrLeveling");
            MZSourceMeasureDelay_ms = configData.ReadSint32("MZSourceMeasureDelay_ms");
            /* If we're testing engineering parts at a reduced power it's likely that the 
             * configuration limit in mW may be wider than the spec limits so we'll fail 
             * power tuning. We attempt to deal with that here. */
            this.tune_OpticalPowerTolerance_mW = configData.ReadDouble("Tune_OpticalPowerTolerance_mW");
            double peakPowerMax_dBm = double.Parse(mainSpec.GetParamLimit("CH_PEAK_POWER").HighLimit.ValueToString());
            double peakPowerMin_dBm = double.Parse(mainSpec.GetParamLimit("CH_PEAK_POWER").LowLimit.ValueToString());
            if (peakPowerMax_dBm != peakPowerMin_dBm)
            {
                double maxPowerDelta_mW = Alg_PowConvert_dB.Convert_dBmtomW((peakPowerMax_dBm - peakPowerMin_dBm) / 2);
                this.tune_OpticalPowerTolerance_mW = Math.Min(maxPowerDelta_mW, tune_OpticalPowerTolerance_mW);
            }

            // Obsolete the method of using ER_dynamic_limit.csv to interpolate the ER for channels - chongjian.liang 2013.4.29
            // LoadERdynamicLimitArray(configData.ReadString("ERdynamicLimitFile"));

            // Initialise Instruments
            mzInstrs = (IlMzInstruments)configData.ReadReference("MzInstruments");
            PowerHead = (IInstType_OpticalPowerMeter)instruments["PowerHead"];

            this.CH_LASER_RTH_Min = configData.ReadDouble("CH_LASER_RTH_Min");
            this.CH_LASER_RTH_Max = configData.ReadDouble("CH_LASER_RTH_Max");

            this.isTestEOL = configData.ReadBool("IsTestEOL");
            this.isTestModeACQ = configData.ReadBool("IsTestModeACQ");
            this.isTestRippleOfEOL = configData.ReadBool("IsTestRippleOfEOL");

            this.iPhaseOfMeritDegrade_mA = configData.ReadDouble("IPhaseOfMeritDegrade_mA");

            this.decompressedFileFolder = Path.Combine(this.mzFileDirectory, "DecompressedFiles");

            if (this.isTestRippleOfEOL)
            {
                this.useLowCostSolution = configData.ReadBool("UseLowCostSolution");
                this.phaseScanSequenceFilePath = configData.ReadString("PhaseCurrentFile");
                this.eolPhaseScanDropFilePath = configData.ReadString("EOLPhaseScanDropFilePath");

                this.ripple_Min = configData.ReadDouble("Ripple_MIN");
                this.ripple_Max = configData.ReadDouble("Ripple_MAX");
                this.afterJumpIgnoreCount = configData.ReadSint32("AfterJumpIgnoreCount");
                this.beforeJumpIgnoreCount = configData.ReadSint32("BeforeJumpIgnoreCount");
                this.multiple = configData.ReadDouble("Multiple");
                this.multipleFilterString = configData.ReadString("MultipleFilterString");
            }

            if (this.isTestEOL)
            {
                this.eolSMSRTestTimeDropFilePath = configData.ReadString("EOLSMSRTestTimeDropFilePath");

                // Write eolSMSRTestTimeFile the first line
                using (StreamWriter swriter = new StreamWriter(eolSMSRTestTimeDropFilePath, true))
                {
                    swriter.Write("Read LockerRatio Time[sec],");
                    swriter.Write("Read WaveMeter Time[sec],");
                    swriter.Write("EOL SMSR Test Time[sec],");
                    swriter.Write("Mode ACQ Test Time[sec],");
                    swriter.WriteLine("SuperModeSR Test Time[sec]");
                }
            }

            // ArmSoureByAsic is not a condition for OSA. Commented out by chongjian.liang 2013.4.17
            //if (!ArmSoureByAsic)
            //{
            //    Osa = (IInstType_OSA)instruments["OSA"];
            //}

            // Added by chongjian.liang 2013.4.17
            if (instruments.Contains("OSA"))
            {
                Osa = (IInstType_OSA)instruments["OSA"];
            }

            Optical_switch = (IInstType_DigiIOCollection)instruments["Optical_switch"];
            OutLineCtap = Optical_switch.GetDigiIoLine(IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine);//line 3
            OutLineCtap.LineState = IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine_State; // read Ctap current, true

            double locker_tran_pot = configData.ReadDouble("locker_tran_pot");
            DsdbrTuning.locker_pot = locker_tran_pot;
            DsdbrUtils.locker_port = locker_tran_pot;
            mzInstrs.FCU2Asic.Locker_tran_pot = int.Parse(locker_tran_pot.ToString());
            mzInstrs.FCU2Asic.Locker_refi_pot = int.Parse(locker_tran_pot.ToString());
            // Alice.Huang  2010-03-07
            // commented for debug

            //Inst_Ag8614xA ag8614xAOsa = Osa as Inst_Ag8614xA;
            //if (ag8614xAOsa != null && ag8614xAOsa.DisplayFeature)
            //    ag8614xAOsa.Display = false;
            dsdbrTec = (IInstType_TecController)instruments["DsdbrTec"];
            if (instruments.Contains("MzTec")) mzTec = (IInstType_TecController)instruments["MzTec"];
            mzInstrs.PowerMeter.Range = opmRange;

            // Initialise Utils
            ilmzUtils = new IlMzDriverUtils(mzInstrs);
            ilmzUtils.opticalSwitch = Optical_switch;
            ilmzUtils.locker_port = int.Parse(locker_tran_pot.ToString());

            testSelect = (TestSelection)configData.ReadReference("TestSelect");
            //switchOsaMzOpm = (Switch_Osa_MzOpm)configData.ReadReference("SwitchOsaMzOpm");
            // Create zip file containing sweep data for each channel.
            string zipFileName = Util_GenerateFileName.GenWithTimestamp(
                mzFileDirectory, "ChannelPlotData", dutSerialNbr, "zip");
            zipFile = new Util_ZipFile(zipFileName);

            // Initialise Selected Channel Tests
            soaControlRange_PostTest = false;
            soaControlRangeTestChannels = ilmzItuChannels.GetSOAControlRangeTestChannels(
                                        SoaControlRangeChanSelectMode.AllChannels, 
                                        SoaControlRangeTestMode.MeasureAtMaxSoa);

            ttrLvSweepChans = new TTRLVSweepChannels(
                freqLow_GHz, 
                freqHigh_GHz,
                ilmzChannelChar_ZD_configObj.TTRLVSweepChanCount, 
                freqSpace_GHz);

            ttrLockerSlopeChans = new TTRLockerSlopeChannels(
                testSelect, 
                freqLow_GHz,
                freqHigh_GHz, freqSpace_GHz,
                configData.ReadDouble("LockerSlopeEffAbsLimitMin"),
                configData.ReadDouble("LockerSlopeEffAbsLimitMax"));

            // Initialise Return Data
            DatumList retData = new DatumList();
            retData.AddFileLink("ChannelPlotData", zipFileName);

            // Initialise MZ
            double currentCompliance_A = mzConfig.MZCurrentCompliance_A;
            double currentRange_A = mzConfig.MZCurrentRange_A;
            double voltageCompliance_V = mzConfig.MZVoltageCompliance_V;
            double voltageRange_V = mzConfig.MZVoltageRange_V;
            int numberOfAverages = mzConfig.MzInitNumberOfAverages;
            double integrationRate = mzConfig.MZIntegrationRate;
            double sourceMeasureDelay = mzConfig.MZSourceMeasureDelay_s;
            double powerMeterAveragingTime = mzConfig.MZPowerMeterAveragingTime_s;

            numberOfAverages = 1;
            // Create some debug information for test time reduction
            if ((configData != null) && (configData.IsPresent("LoggingEnabled")))
            {
                bool loggingEnabled = configData.ReadBool("LoggingEnabled");
                GuiLoggingEnable guiMsg = new GuiLoggingEnable(loggingEnabled, "TestTime_MID");
                engine.SendToGui(guiMsg);
            }
            else
            {
                throw new ArgumentNullException("no available config data");
            }

            // setup our private data 
            engine.SendToGui(new GuiTextMessage("Preparing instruments for sweeping", 0));

            if (!ArmSoureByAsic)
            {
                ilmzUtils.InitialiseMZArms(currentCompliance_A, currentRange_A);
                ilmzUtils.InitialiseImbalanceArms(voltageCompliance_V, voltageRange_V);
                ilmzUtils.InitialiseTaps(currentCompliance_A, currentRange_A);
                if (mzInstrs.InlineTapOnline)
                {
                    // apply bias to InlineTap...
                    // If not, the power levelling at CH0 will be done when Bias_InlineTap = 0 (should be -3.5)
                    // Then after MZ final characterise, the fibrePwrPeak will become smaller as partial power is absorbed by InlineTap...
                    ilmzUtils.SetVoltage(SourceMeter.InlineTap, mzConfig.MZTapBias_V);
                }

                ilmzUtils.SetMeasurementAccuracy(
                    numberOfAverages,
                    integrationRate,
                    sourceMeasureDelay,
                    powerMeterAveragingTime);

                mzInstrs.PowerMeter.Mode = Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.MeterMode.Absolute_mW;
                mzInstrs.PowerMeter.Range = opmRange;
            }

            int numChanTested = 0;
            int numChanPass = 0;

            // Initialise tuning functions
            DsdbrTuning.FreqTolerance_GHz = configData.ReadDouble("ItuFreqTolerance_GHz");
            DsdbrTuning.FreqTolerance_EOL_GHz = configData.ReadDouble("ItuFreqTolerance_EOL_GHz");
            DsdbrTuning.RearAdjustFreq_GHz = configData.ReadDouble("Tune_IrearAdjustFreq_GHz");
            double ptrIphaseOutset_mA = configData.ReadDouble("PtrIphaseOutset_mA");
            DsdbrTuning.IphaseMin_mA = Convert.ToDouble(mainSpec.GetParamLimit(
                "CH_PHASE_I_LOWER_EOL").LowLimit.ValueToString()) - ptrIphaseOutset_mA;
            DsdbrTuning.IphaseMax_mA = Convert.ToDouble(mainSpec.GetParamLimit(
                "CH_PHASE_I_UPPER_EOL").HighLimit.ValueToString()) + ptrIphaseOutset_mA;
            DsdbrTuning.IrearMin_mA = Convert.ToDouble(mainSpec.GetParamLimit("CH_REAR_I").LowLimit.ValueToString());
            DsdbrTuning.IrearMax_mA = Convert.ToDouble(mainSpec.GetParamLimit("CH_REAR_I").HighLimit.ValueToString());
            DsdbrTuning.SmsrNegMinLimit_dB = Convert.ToDouble(mainSpec.GetParamLimit("CH_SMSR_AT_PTR_MIN_EOL").LowLimit.ValueToString());
            DsdbrTuning.SmsrPosMinLimit_dB = Convert.ToDouble(mainSpec.GetParamLimit("CH_SMSR_AT_PTR_MAX_EOL").LowLimit.ValueToString());
            DsdbrTuning.SupermodeSrMinLimit_dB = Convert.ToDouble(mainSpec.GetParamLimit("CH_SUPERMODE_SR").LowLimit.ValueToString());


            // Initialise SMSR measurement & SupermodeSR measurement
            if (this.Osa != null)
            {
                SMSRMeasurement.OSA = Osa;
                SMSRMeasurement.SMSRSpan_nm = ilmzChannelChar_ZD_configObj.SMSRMeasure_Span_nm;
                SMSRMeasurement.SMSRRBW_nm = ilmzChannelChar_ZD_configObj.SMSRMeasure_RBW_nm;
                SMSRMeasurement.SMSROSASensitivity_dBm = ilmzChannelChar_ZD_configObj.SMSRMeasure_OSASensitivity_dBm;
                SMSRMeasurement.SMSRNumPts = ilmzChannelChar_ZD_configObj.SMSRMeasure_NumPts;

                SupermodeSRMeasurement.OSA = Osa;
                SupermodeSRMeasurement.SupermodeSRSpan_nm = ilmzChannelChar_ZD_configObj.SupermodeSRMeasure_Span_nm;
                SupermodeSRMeasurement.SupermodeSRRBW_nm = ilmzChannelChar_ZD_configObj.SupermodeSRMeasure_RBW_nm;
                SupermodeSRMeasurement.SupermodeSROSASensitivity_dBm = ilmzChannelChar_ZD_configObj.SupermodeSRMeasure_OSASensitivity_dBm;
                SupermodeSRMeasurement.SupermodeSRNumPts = ilmzChannelChar_ZD_configObj.SupermodeSRMeasure_NumPts;
                SupermodeSRMeasurement.SupermodeSRExclude_nm = ilmzChannelChar_ZD_configObj.SupermodeSRMeasure_Exclude_nm;
            }

            // Sanity check channel configuration            
            if ((freqHigh_GHz - freqLow_GHz) / freqSpace_GHz + 1 != numChans)
                engine.RaiseNonParamFail(0, "Invalid channel configuration");

            // Confirm that at least 1 channel is present            
            if (ilmzItuChannels.AllOptions.Length < 1)
                engine.RaiseNonParamFail(0, "No channels found");

  
            #region MZ SingleEnded LV sweep at 193700; C-band
            /*
            if (freqLow_GHz > 190000)
            {


                if ((!retData.IsPresent("Power_Right_Peak")) || (!retData.IsPresent("Power_Left_Peak")))
                {
                    // CROSS calibration with Inline  steven.cui
                    // Configure device to 193700
                    ILMZChannel[] CrossIlineChan = ilmzItuChannels.GetChannelsAtItu(193700);
                    ILMZChannel TestChan = CrossIlineChan[0];
                    DsdbrChannelSetup Calchannel = TestChan.IlmzInitialSettings.Dsdbr.Setup;
                    //Calchannel.ISoa_mA = 100;//Jack.zhang
                    DsdbrUtils.SetDsdbrCurrents_mA(Calchannel);


                    ilmzUtils.SetCurrentSenseRange(SourceMeter.LeftModArm, mzConfig.MZCurrentCompliance_A);
                    ilmzUtils.SetCurrentSenseRange(SourceMeter.RightModArm, mzConfig.MZCurrentCompliance_A);

                    //double fixedModBias_V = mzConfig.MZInitFixedModBias_volt;       // 0V
                    double fixedModBias_V = 0;
                    //double imbalance_A = mzConfig.MzCtrlINominal_mA / 1000;         // +4.5mA
                    double imbalance_A = 0;
                    double sweepStop_V = 0;
                    double sweepStart_V = -mzConfig.MZInitSweepMaxAbs_V;   // -8V
                    //double sweepStart_V = -8;
                    int nbrPoints = (int)(1 + Math.Abs(sweepStart_V - sweepStop_V) / (mzConfig.MZInitSweepStepSize_mV / 1000));
                    double tapBias_V = mzConfig.MZTapBias_V;                        // 2V

                    mzInstrs.PowerMeter.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_mW;
                    
                    mzInstrs.PowerMeter.MaxMinOn = true;
                    mzInstrs.PowerMeter.Range = opmRange;


                    //left sweep
                    engine.SendToGui(new GuiTextMessage("Running LeftArm sweep", 2));
                    ILMZSweepResult sweepDataLeft =
                        ilmzUtils.LeftModArm_SingleEndedSweep
                        (fixedModBias_V, imbalance_A, imbalance_A, sweepStart_V,
                        sweepStop_V, nbrPoints, tapBias_V, tapBias_V);
                    engine.SendToGui(sweepDataLeft);

                    // Analysis left Data
                    MzAnalysisWrapper.MzSingleEndedAnalysisResults mzLeftSingleEndedLVResults;
                    //double[] tt = sweepDataLeft.SweepData[ILMZSweepDataType.LeftArmModBias_V];
                    //double[] dd = sweepDataLeft.SweepData[ILMZSweepDataType.FibrePower_mW];
                    bool error = false;
                    mzLeftSingleEndedLVResults = MzAnalysisWrapper.SingleEnded_NegChirp_RightArmModulatorSweep(sweepDataLeft.SweepData[ILMZSweepDataType.LeftArmModBias_V], sweepDataLeft.SweepData[ILMZSweepDataType.FibrePower_mW],out error);
                    if (error)
                    {
                        engine.RaiseNonParamFail(1, "Can't find peak or valley point when do Left arm sweep!");
                    }

                    double leftPeakPower = mzLeftSingleEndedLVResults.PowerAtMax_dBm;

                    if ((leftPeakPower > 100) || (leftPeakPower < -100))
                    {
                        leftPeakPower = 999;
                    }

                    
                    ////////Right sweep
                    //////engine.SendToGui(new GuiTextMessage("Running RightArm sweep", 2));
                    //////ILMZSweepResult sweepDataRight =
                    //////    ilmzUtils.RightModArm_SingleEndedSweep
                    //////    (fixedModBias_V, imbalance_A, imbalance_A, sweepStart_V,
                    //////    sweepStop_V, nbrPoints, tapBias_V, tapBias_V);
                    //////engine.SendToGui(sweepDataRight);

                    //////// Analysis right Data
                    //////MzAnalysisWrapper.MzSingleEndedAnalysisResults mzRightSingleEndedLVResults;
                    //////mzRightSingleEndedLVResults = MzAnalysisWrapper.SingleEnded_NegChirp_RightArmModulatorSweep(sweepDataRight.SweepData[ILMZSweepDataType.RightArmModBias_V], sweepDataRight.SweepData[ILMZSweepDataType.FibrePower_mW]);

                    

                    //double rightPeakPower = mzRightSingleEndedLVResults.PowerAtMax_dBm;
                    double rightPeakPower = 99;

                    if ((rightPeakPower > 100) || (rightPeakPower < -100))
                    {
                        rightPeakPower = 999;
                    }

                    if (!retData.IsPresent("Power_Right_Peak"))
                    {
                        retData.AddDouble("Power_Right_Peak", rightPeakPower); //Echo remed this block according to stream.su's require
                    }
                    if (!retData.IsPresent("Power_Left_Peak"))
                    {
                        retData.AddDouble("Power_Left_Peak", leftPeakPower);
                    }
                }

            }
            #endregion

            #region MZ SingleEnded LV sweep at 189000; L-band

            if (freqLow_GHz < 190000)
            {

                if ((!retData.IsPresent("Power_Right_Peak")) || (!retData.IsPresent("Power_Left_Peak")))
                {
                    // CROSS calibration with Inline  steven.cui
                    // Configure device to 189000
                    ILMZChannel[] CrossIlineChan = ilmzItuChannels.GetChannelsAtItu(189000);
                    ILMZChannel TestChan = CrossIlineChan[0];
                    DsdbrChannelSetup Calchannel = TestChan.IlmzInitialSettings.Dsdbr.Setup;
                    Calchannel.ISoa_mA = 100;
                    DsdbrUtils.SetDsdbrCurrents_mA(Calchannel);


                    ilmzUtils.SetCurrentSenseRange(SourceMeter.LeftModArm, mzConfig.MZCurrentCompliance_A);
                    ilmzUtils.SetCurrentSenseRange(SourceMeter.RightModArm, mzConfig.MZCurrentCompliance_A);

                    //double fixedModBias_V = mzConfig.MZInitFixedModBias_volt;       // 0V
                    double fixedModBias_V = 0;
                    //double imbalance_A = mzConfig.MzCtrlINominal_mA / 1000;         // +4.5mA
                    double imbalance_A = 0;
                    double sweepStop_V = 0;
                    //double sweepStart_V = -Math.Abs(mzConfig.MZInitSweepMaxAbs_V);   // -8V
                    double sweepStart_V = -8;
                    int nbrPoints = (int)(1 + Math.Abs(sweepStart_V - sweepStop_V) / (mzConfig.MZInitSweepStepSize_mV / 1000));
                    double tapBias_V = mzConfig.MZTapBias_V;                        // 2V

                    mzInstrs.PowerMeter.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_mW;
                    mzInstrs.PowerMeter.MaxMinOn = true;
                    mzInstrs.PowerMeter.Range = opmRange;


                    //left sweep
                    engine.SendToGui(new GuiTextMessage("Running LeftArm sweep", 2));
                    ILMZSweepResult sweepDataLeft =
                        ilmzUtils.LeftModArm_SingleEndedSweep
                        (fixedModBias_V, imbalance_A, imbalance_A, sweepStart_V,
                        sweepStop_V, nbrPoints, tapBias_V, tapBias_V);
                    engine.SendToGui(sweepDataLeft);

                    // Analysis left Data
                    MzAnalysisWrapper.MzSingleEndedAnalysisResults mzLeftSingleEndedLVResults;
                    //double[] tt = sweepDataLeft.SweepData[ILMZSweepDataType.LeftArmModBias_V];
                    //double[] dd = sweepDataLeft.SweepData[ILMZSweepDataType.FibrePower_mW];
                    mzLeftSingleEndedLVResults = MzAnalysisWrapper.SingleEnded_NegChirp_RightArmModulatorSweep(sweepDataLeft.SweepData[ILMZSweepDataType.LeftArmModBias_V], sweepDataLeft.SweepData[ILMZSweepDataType.FibrePower_mW]);

                    double leftPeakPower = mzLeftSingleEndedLVResults.PowerAtMax_dBm;

                    if ((leftPeakPower > 100) || (leftPeakPower < -100))
                    {
                        leftPeakPower = 999;
                    }

                    
                    //////Right sweep
                    ////engine.SendToGui(new GuiTextMessage("Running RightArm sweep", 2));
                    ////ILMZSweepResult sweepDataRight =
                    ////    ilmzUtils.RightModArm_SingleEndedSweep
                    ////    (fixedModBias_V, imbalance_A, imbalance_A, sweepStart_V,
                    ////    sweepStop_V, nbrPoints, tapBias_V, tapBias_V);
                    ////engine.SendToGui(sweepDataRight);

                    ////// Analysis right Data
                    ////MzAnalysisWrapper.MzSingleEndedAnalysisResults mzRightSingleEndedLVResults;
                    ////mzRightSingleEndedLVResults = MzAnalysisWrapper.SingleEnded_NegChirp_RightArmModulatorSweep(sweepDataRight.SweepData[ILMZSweepDataType.RightArmModBias_V], sweepDataRight.SweepData[ILMZSweepDataType.FibrePower_mW]);
                    
                    //double rightPeakPower = mzRightSingleEndedLVResults.PowerAtMax_dBm;
                    double rightPeakPower = 99;

                    if ((rightPeakPower > 100) || (rightPeakPower < -100))
                    {
                        rightPeakPower = 999;
                    }

                    if (!retData.IsPresent("Power_Right_Peak"))
                    {
                        retData.AddDouble("Power_Right_Peak", rightPeakPower);
                    }//Echo remed this block according to streamsu's require 2010-10-14
                    if (!retData.IsPresent("Power_Left_Peak"))
                    {
                        retData.AddDouble("Power_Left_Peak", leftPeakPower);
                    }
                }

            }*/
            #endregion 

            double leftPeakPower = 0;
            double rightPeakPower = 0;

            if (!retData.IsPresent("Power_Right_Peak"))
            {
                retData.AddDouble("Power_Right_Peak", rightPeakPower);
            }
            if (!retData.IsPresent("Power_Left_Peak"))
            {
                retData.AddDouble("Power_Left_Peak", leftPeakPower);
            }

            /* *************************************************************************************** */
            /*                                  CHANNEL LOOP                                           */            

            using (zipFile)
            {
                // Loop through all the channels need to do differential LV sweep actually
                for (int ii = 0; ii < ttrLvSweepChans.ChannelsToTestActually.Count; ii++)
                    //  ttrLvSweepChans.ChannelsToTestActually.Count//ttrLvSweepChans.ChannelsToTestActually.Count
                {
                    
                    double chanFreq_GHz = ttrLvSweepChans.ChannelsToTestActually[ii];
                    engine.SendToGui(new GuiProgressMessage((int)(numChanTested /
                        ((freqHigh_GHz - freqLow_GHz) / freqSpace_GHz + 1) * 100)));
                    testChannel(chanFreq_GHz, engine,ref numChanTested, ref numChanPass,
                                 configData, true, true,previousTestData);
                    if (ForceStopTesting) break;
                }
                //raul added to check ER dynamic limit on 2012.4.24,requested by Stream.su
                #region check ER limit

                engine.SendToGui(new GuiTitleMessage("Checking ER dynamic limit"));
                //if (ER_List.Count != ttrLvSweepChans.ChannelsToTestActually.Count)
                //    engine.ErrorInModule("ER check fail:some channel ER value missing or not test");

                bool ER_Pass_Fail_flag = true;

                for (int chnbr = 0; chnbr < ttrLvSweepChans.ChannelsToTestActually.Count; chnbr++)
                {
                    if (!ER_Pass_Fail_flag) break;

                    // Obsolete the method of using ER_dynamic_limit.csv to interpolate the ER for channels - chongjian.liang 2013.4.29
                    //double upperLimit = Alg_ArrayFunctions.interpolateOnX(upper_List_X, upper_List_Y, chnbr);
                    //double lowerLimit = Alg_ArrayFunctions.interpolateOnX(lower_List_X, lower_List_Y, chnbr);

                    //double Channel_ER = ER_List[chnbr];
                    double chanFreq_GHz = ttrLvSweepChans.ChannelsToTestActually[chnbr];
                    ILMZChannel[] channelsAtItu = ilmzItuChannels.GetChannelsAtItu(chanFreq_GHz);

                    foreach(ILMZChannel channelAtItu in channelsAtItu)
                    {
                        if (channelAtItu.MidTempData.IsTested(EnumTosaParam.MzDcEr_dB))
                        {
                            double Channel_ER = channelAtItu.MidTempData.GetValueDouble(EnumTosaParam.MzDcEr_dB);

                            if (isUseDynamicER)
                            {
                                if ((Channel_ER < dynamicER_LowLimits[chanFreq_GHz]) || (Channel_ER > dynamicER_HighLimits[chanFreq_GHz]))
                                {
                                    ER_Pass_Fail_flag = false; //ER check fail
                                }
                            }
                            else
                            {
                                if ((Channel_ER < erLimit_Low) || (Channel_ER > erLimit_High))
                                {
                                    ER_Pass_Fail_flag = false; //ER check fail
                                }
                            }

                            if (!ER_Pass_Fail_flag)
                            {
                                retData.AddString("ErrorInformation", "the failed ER chnbr is " + chnbr + " the ER value is " + Channel_ER);
                                break;
                            }
                        }
                    }
                }
                retData.AddSint32("ERPassFailFlag", ER_Pass_Fail_flag ? 1 : 0);
                #endregion

                #region check if good linearity
                string linearityFile = Util_GenerateFileName.GenWithTimestamp(mzFileDirectory,
                    "Linearity_TTRLVSweeps", dutSerialNbr, "csv");
                double linearityLimitMin = configData.ReadDouble("MzDcVpiSlopeLimitMin");
                double linearityLimitMax = configData.ReadDouble("MzDcVpiSlopeLimitMax");
                bool goodLinearity = false;//echo new added
                bool fullLVSweep = !goodLinearity;
                retData.AddSint32("FullLvSweepFlag", fullLVSweep ? 1 : 0);
                zipFile.AddFileToZip(linearityFile);

                #endregion
                 
                // Loop through all channels
                for (double chanFreq_GHz = freqLow_GHz; 
                    chanFreq_GHz <= freqHigh_GHz; chanFreq_GHz += freqSpace_GHz )
                {
                    if (powerLevelingFailedCount >= powerLevelingBreakCount)
                    {
                        string errorPrompt = string.Format("Break ILMZ Channel Characterisation after {0} channels failed on power leveling",
                            powerLevelingFailedCount);

                        engine.SendToGui(errorPrompt);
                        break;
                    }

                    // Test the channel
                    if (!ttrLvSweepChans.IsToTestActually(chanFreq_GHz))
                    {
                        engine.SendToGui(new GuiProgressMessage((int)(numChanTested / 
                            ((freqHigh_GHz - freqLow_GHz) / freqSpace_GHz + 1) * 100)));

                        testChannel(
                            chanFreq_GHz, 
                            engine, 
                            ref numChanTested, 
                            ref numChanPass,
                            configData,  
                            true,  
                            true, 
                            previousTestData);
                    }
                }
                if (!retData.IsPresent("LM_PHASE_SCAN_RIPPLE_MID"))
                {
                    retData.AddDouble("LM_PHASE_SCAN_RIPPLE_MID", maxRipple);
                }
                else
                {
                    retData.UpdateDouble("LM_PHASE_SCAN_RIPPLE_MID", maxRipple);
                
                }
                #region Soa control range test at certain number of highest/lowest SOA channels
                if (soaControlRange_PostTest)
                {
                    soaControlRangeTestChannels = ilmzItuChannels.GetSOAControlRangeTestChannels(
                        SoaControlRangeChanSelectMode.HighestLowestSoa, 
                        SoaControlRangeTestMode.MeasureAtMaxMinSoa);

                    engine.SendToGui(new GuiTextMessage("Prepare for SOA control range test at highest/lowest SOA channels", 0));

                    foreach (ILMZChannel chan in ilmzItuChannels.PassedMidTemp)
                    {
                        // Check if do soa control range test, and get the test mode
                        int ituChannelIndex = chan.MidTempData.GetValueSint32(EnumTosaParam.ItuChannelIndex);
                        double chanItuFreq_GHz = chan.MidTempData.GetValueDouble(EnumTosaParam.ItuFreq_GHz);
                        bool doTest = false;
                        SoaControlRangeTestMode testMode = SoaControlRangeTestMode.Sweep;
                        foreach (SoaControlRangeChan chanToTest in soaControlRangeTestChannels)
                        {
                            if (chanToTest.chanIndex == ituChannelIndex)
                            {
                                doTest = true;
                                testMode = chanToTest.testMode;
                                break;
                            }
                        }

                        // Do soa control range test
                        if (doTest)
                        {
                            engine.SendToGui(new GuiTextMessage("Soa control range test at channel " + chanItuFreq_GHz.ToString() + " GHz", 1));

                            // config device to channel
                            configIlmzToChannel(chan);

							if (!ArmSoureByAsic)
                            {
                            // Reset measurement range to allow for high photocurrent in arms
                            ilmzUtils.SetCurrentSenseRange(SourceMeter.LeftModArm, mzConfig.MZCurrentCompliance_A);
                            ilmzUtils.SetCurrentSenseRange(SourceMeter.RightModArm, mzConfig.MZCurrentCompliance_A);
					    	}

                            // soa control range test
                            if (testMode == SoaControlRangeTestMode.Sweep)
                            {
                                string plotFileQualifier = chanItuFreq_GHz.ToString() + "_" + 
                                    chan.MidTempData.Priority.ToString() + "_" + dutSerialNbr;

                                DatumList soaSwpResults =
                                    SoaSweep.SweepAtSingleChannel(
                                    ilmzChannelChar_ZD_configObj.SoaSweep_MinCurrent_mA, 
                                    ilmzChannelChar_ZD_configObj.SoaSweep_MaxCurrent_mA,
                                    ilmzChannelChar_ZD_configObj.SoaSweep_RefCurrent_mA, 
                                    ilmzChannelChar_ZD_configObj.SoaSweep_Stepdelay_ms,
                                    mzFileDirectory, 
                                    plotFileQualifier);

                                chan.MidTempData.SetValueDouble(EnumTosaParam.FibrePwrISoaMin_dBm,
                                    soaSwpResults.ReadDouble("SOA_MinPwr_dBm"));

                                chan.MidTempData.SetValueDouble(EnumTosaParam.FibrePwrISoaMax_dBm,
                                    soaSwpResults.ReadDouble("SOA_MaxPwr_dBm"));

                                string plotFile = soaSwpResults.ReadString("SOA_sweep_plot");
                                zipFile.AddFileToZip(plotFile);

                                double[] xData = soaSwpResults.ReadDoubleArray("SOA_mA");
                                double[] yData = soaSwpResults.ReadDoubleArray("FibrePower_dBm");
                                GuiPlotData plotData = new GuiPlotData(xData, yData, 
                                    "SOA Control Range", "Optical Power (dBm)");

                                engine.SendToGui(plotData);
                            }
                            else if (testMode == SoaControlRangeTestMode.MeasureAtMaxMinSoa)
                            {
                                DatumList soaMeasResults =
                                    SoaSweep.MeasurePowerAtMinAndMaxSoa(
                                    ilmzChannelChar_ZD_configObj.SoaSweep_MinCurrent_mA,
                                    ilmzChannelChar_ZD_configObj.SoaSweep_MaxCurrent_mA,
                                    ilmzChannelChar_ZD_configObj.SoaSweep_Stepdelay_ms);

                                chan.MidTempData.SetValueDouble(EnumTosaParam.FibrePwrISoaMin_dBm, 
                                    soaMeasResults.ReadDouble("SOA_MinPwr_dBm"));

                                chan.MidTempData.SetValueDouble(EnumTosaParam.FibrePwrISoaMax_dBm, 
                                    soaMeasResults.ReadDouble("SOA_MaxPwr_dBm"));
                            }
                            else
                            {
                                double soaMeasResult =
                                    SoaSweep.MeasurePowerAtMaxSoa(
                                    ilmzChannelChar_ZD_configObj.SoaSweep_MaxCurrent_mA, 
                                    ilmzChannelChar_ZD_configObj.SoaSweep_Stepdelay_ms);

                                chan.MidTempData.SetValueDouble(
                                    EnumTosaParam.FibrePwrISoaMax_dBm, soaMeasResult);
                            }

                            if (chan.PassStatus == PassFail.Pass)
                            {
                                engine.SendToGui(new GuiTextMessage("Soa control range test at channel " + 
                                chanItuFreq_GHz.ToString() + " GHz Passed!", 1));
                            }
                            else
                            {
                                engine.SendToGui(new GuiTextMessage("Soa control range test at channel " +
                                            chanItuFreq_GHz.ToString() + " GHz Failed!", 1));

                                numChanPass--;
                            }
                        }
                    }
                    engine.SendToGui(new GuiTextMessage("SOA control range test at highest/lowest SOA channels finished!", 0));
                }
                #endregion
            }   // using zipfile

            /*                                  CHANNEL LOOP                                           */            
            /* *************************************************************************************** */

            engine.SendToGui(new GuiTextMessage("TCMZ Channel Characterisation Finished", 0));

            if (this.isTestRippleOfEOL)
            {
                retData.AddFileLink("LM_PHASE_SCAN_FOR_RIPPLE_FILE", this.eolPhaseScanDropFilePath);
            }

            // Return results
            // Break testing while certain number of channels failed on power level
            if ((powerLevelingFailedCount >= powerLevelingBreakCount))
            {
                retData.AddSint32("FailAbortFlag", 1);

                string comment = string.Format(
                    "{0} channels power leveling failed", 
                    powerLevelingFailedCount);

                retData.AddString("FailAbortReason", comment);

                retData.AddSint32("FullLkSlopeTest", 0); // Add default FULL_LK_SLOPE_FLAG 0 here...
                retData.AddSint32("PassFailFlag", numChanPass == numChanTested ? 1 : 0);
                retData.AddSint32("NumChannelsTested", numChanTested);
                retData.AddSint32("NumChannelsPass", numChanPass);
                return retData; // return and exit the module here as no need to measure locker slope for over-temp test...
                // we will also exit the program without over-temp test...
            }
            else
            {
                // Abort test when FailAbortReason is marked true - chongjian.liang 2013.4.20
                if (ForceStopTesting)
                {
                    retData.AddSint32("FailAbortFlag", 1);
                    retData.AddString("FailAbortReason", this.FailAbortReason);
                    retData.AddSint32("NumChannelsTested", 0);
                    retData.AddSint32("NumChannelsPass", 0);
                    retData.AddSint32("FullLkSlopeTest", 0); // Add default FULL_LK_SLOPE_FLAG 0 here...
                    retData.AddSint32("PassFailFlag", numChanPass == numChanTested ? 1 : 0);

                    return retData;
                }
                else
                {
                    retData.AddSint32("FailAbortFlag", 0);
                }
            }

            #region Locker Slope at ItuExtremeChannels for over-temp test or all channels if in need

            // if TTRLockerSlopeChannels status isn't Passed, all channels with LockerSlope test
            bool fullLockerSlopeTest = !(ttrLockerSlopeChans.TTRLockerSlopeChannelsPassed(ilmzItuChannels));
            retData.AddSint32("FullLkSlopeTest", fullLockerSlopeTest ? 1 : 0);
            engine.SendToGui(new GuiTextMessage("Prepare for additional locker slope test", 0));

            IlmzChannels.ExtremeChannelIndexes extremeChans = ilmzItuChannels.GetItuExtremeChannels();

            /* *************************************************************************************** */
            /*                           CHANNEL LOOP START - LOCKER SLOPE                             */

            // loop through all mid-temp passed channels
            foreach (ILMZChannel chan in ilmzItuChannels.PassedMidTemp)
            {
                int ituChannelIndex = chan.MidTempData.GetValueSint32(EnumTosaParam.ItuChannelIndex);
                double chanItuFreq_GHz = chan.MidTempData.GetValueDouble(EnumTosaParam.ItuFreq_GHz);

                if ((fullLockerSlopeTest ||
                    ituChannelIndex == extremeChans.HighestLaserDissIndex ||
                    ituChannelIndex == extremeChans.LowestLaserDissIndex ||
                    ituChannelIndex == extremeChans.HighestestIsoaIndex ||
                    ituChannelIndex == extremeChans.LowestIsoaIndex ||
                    ituChannelIndex == extremeChans.FirstChannelIndex ||
                    ituChannelIndex == extremeChans.LastChannelIndex ||
                    testSelect.IsTestSelected(chanItuFreq_GHz, "HighTemp") ||
                    testSelect.IsTestSelected(chanItuFreq_GHz, "LowTemp")) &&
                    (!ttrLockerSlopeChans.IsTTRLockerSlopeChannel(chanItuFreq_GHz)))
                {
                    engine.SendToGui(new GuiTextMessage("Measuring Locker slope at channel " +
                        chanItuFreq_GHz.ToString() + " GHz", 1));

                    // config device to channel
                    configIlmzToChannel(chan);
                    Measurements.FrequencyWithoutMeter = chanItuFreq_GHz;
                    // locker slope measurement
                    DatumList lockerSlopeResults = LockerSlope.MeasureLockerSlope();

                    // save data
                    chan.MidTempData.SetValueDouble(EnumTosaParam.LockerSlopeEff, 
                        lockerSlopeResults.ReadDouble("LockerSlopeEff"));

                    chan.MidTempData.SetValueDouble(EnumTosaParam.LockerSlopeEffAbs,
                        Math.Abs(lockerSlopeResults.ReadDouble("LockerSlopeEff")));

                    chan.MidTempData.SetValueDouble(EnumTosaParam.PhaseRatioSlopeEff,
                        lockerSlopeResults.ReadDouble("PhaseRatioSlopeEff"));
                    chan.MidTempData.SetValueDouble(EnumTosaParam.PhaseTuningEff, lockerSlopeResults.ReadDouble("PhaseTuningEff"));


                    if (chan.PassStatus == PassFail.Pass)
                    {
                        engine.SendToGui(new GuiTextMessage("Locker slope passed!", 1));
                    }
                    else
                    {
                        engine.SendToGui(new GuiTextMessage("Locker slope failed!", 1));
                        numChanPass--;
                    }
                }
            }

            /*                           CHANNEL LOOP END - LOCKER SLOPE                               */
            /* *************************************************************************************** */


            engine.SendToGui(new GuiTextMessage("Locker slope test finished!", 0));

            #endregion

            if (!retData.IsPresent("LM_PHASE_SCAN_RIPPLE_MID"))
            {
                retData.AddDouble("LM_PHASE_SCAN_RIPPLE_MID", maxRipple);
            }
            else
            {
                retData.UpdateDouble("LM_PHASE_SCAN_RIPPLE_MID", maxRipple);

            }

            //retData.AddBool("SelectTestFlag", SelectTestFlag);
            retData.AddSint32("PassFailFlag", numChanPass == numChanTested ? 1 : 0);
            retData.AddSint32("NumChannelsTested", numChanTested);
            retData.AddSint32("NumChannelsPass", numChanPass);
            //mx
            retData.AddReference("ExtremeChannels", extremeChans);
            mzInstrs.FCU2Asic.DisableAllOutput();

            return retData;
        }

        /// <summary>
        /// GUI for this module
        /// </summary>
        public Type UserControl
        {
            
            get { return typeof(IlmzChannelCharGui); }
        }

        #endregion

        #region Private Functions

        /// <summary>
        /// Test the specified channel
        /// </summary>
        /// <param name="chanFreq_GHz">Which channel to test</param>
        /// <param name="engine">Test engine interface</param>
        /// <param name="numChanTested">Tested channel count</param>
        /// <param name="numChanPass">Passed channel count</param>
        /// <param name="configData">Config data</param>
        /// <param name="actualLVSweep">Do LV sweeps actually?</param>
        /// <param name="TTR_LVChannel">This channel is a selected TTR_LVSweep channel?</param>
        /// <param name="previousTestData"></param>
        private void testChannel(double chanFreq_GHz, ITestEngine engine, ref int numChanTested, ref  int numChanPass,
            DatumList configData, bool actualLVSweep, bool TTR_LVChannel, DatumList previousTestData)
        {
            // Alice.Huang  2010-02-09
            // add these two variants to allow convertion between current & Dac and record
            // for TOSA GB Test 

            double curReading;
            int dacTemp;

            engine.SendToGui(new GuiTextMessage("Testing " + chanFreq_GHz.ToString() + " GHz channels", 0));
            //engine.SendToGui(new GuiProgressMessage((int)((chanFreq_GHz - freqLow_GHz) / (freqHigh_GHz - freqLow_GHz) * 100)));

            // Get indexes of channels in 'tcmzChannels' with target frequency
            // Channels are sorted by figure of merit, highest fom in [0]
            ILMZChannel[] thisFreqChans = ilmzItuChannels.GetChannelsAtItu(chanFreq_GHz);

            if (iPhaseOfMeritDegrade_mA != 999)
            {
                #region Degrade the channel by phase current - chongjian.liang 2013.5.11

                List<ILMZChannel> thisFreqChanList = new List<ILMZChannel>(thisFreqChans);

                // Set channel as the last option if its iphase is more than iPhaseOfMeritDegrade_mA.
                for (int i = 0; i < thisFreqChanList.Count; i++)
                {
                    if (thisFreqChans[i].MidTempData.DsdbrSetup.IPhase_mA > iPhaseOfMeritDegrade_mA)
                    {
                        thisFreqChanList.Remove(thisFreqChans[i]);
                        thisFreqChanList.Add(thisFreqChans[i]);
                    }
                }

                thisFreqChanList.CopyTo(thisFreqChans);

                #endregion
            }

            bool chanPass = false;
            int powerLevelingFailedInstanceCount = 0;
            bool beingTested = false;// to indicate if need to increase the counter numChanTested
            //double Ch_DC_ER = 0;//to get the ER of this channel

            ILMZChannel ilmzChan = null;

            // Test channel options starting with highest priority until 
            // one passes or all channel options have been tested
            for (int chanIndex = 0; chanIndex < thisFreqChans.Length; chanIndex++)
            {
                ilmzChan = thisFreqChans[chanIndex];

                engine.SendToGui(new GuiTextMessage("Testing channel option " + ilmzChan.MidTempData.Priority.ToString(), 1));

                #region Try next channel if this one is already tested

                if (ilmzChan.MidTempData.Finished)
                {
                    if (ilmzChan.MidTempData.OverallStatus == PassFail.Pass)
                    {
                        engine.SendToGui(new GuiTextMessage("Channel option test stopped, already passed. Go to next channel", 1));
                        chanPass = true;
                        break;
                    }
                    else if (chanIndex + 1 >= thisFreqChans.Length)
                    {
                        engine.SendToGui(new GuiTextMessage("Channel option test stopped, already failed but no other option. Go to next channel", 1));
                        chanPass = false;
                        break;
                    }
                    else
                    {
                        chanPass = false;
                        // check if the next option started or not...
                        if (thisFreqChans[chanIndex + 1].Completeness == SpecComplete.NoParams)
                        {
                            engine.SendToGui(new GuiTextMessage("Channel option test stopped, already failed but no need to try next option. Go to next channel", 1));
                            break;
                        }
                        else
                        {
                            engine.SendToGui(new GuiTextMessage("Channel option test stopped, already failed. Go to next option", 1));
                            continue;
                        }
                    }
                } 
                #endregion

                ilmzChan.StartChannelTest();
                beingTested = true;
                //int failCode = (int)ChannelFailCode.NoFail;

                // Record figure of merit
                ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.FigureOfMerit, ilmzChan.MidTempData.FigureOfMerit);

                // Try next channel option if any params out of limits
                if ((!alwaysTuneLaser) &&
                    (ilmzChan.PassStatus == PassFail.Fail)/* &&
                    (tcmzChan.MidTempData.OverallCompleteness == SpecComplete.Complete)*/
                   )
                {
                    engine.SendToGui(new GuiTextMessage("Channel option test stopped, parameter(s) out of spec: " + ilmzChan.MidTempData.FailedParamNames, 1));
                    ilmzChan.MidTempData.SetValueBool(EnumTosaParam.Pass_Flag, false);
                    ilmzChan.MidTempData.SetFinished();
                    continue;
                }

                if (ilmzChan.PassStatus == PassFail.Fail)
                    engine.SendToGui(new GuiTextMessage("Forcing test to continue despite failing for : " + ilmzChan.MidTempData.FailedParamNames, 1));

                // Configure device to channel
                DsdbrUtils.SetDsdbrCurrents_mA(ilmzChan.IlmzInitialSettings.Dsdbr.Setup);
                // Set MZ biases
                if (!ArmSoureByAsic)
                {
                    IlMzDriverUtils.SetupMzToPeak(mzInstrs, ilmzChan.IlmzInitialSettings.Mz);
                }
                else
                {
                    IlMzDriverUtils.SetupMzToPeak(mzInstrs.FCU2Asic, ilmzChan.IlmzInitialSettings.Mz);
                }

                #region Thermistor Resistances
                // Allow a number of attempts to settle
                int rthCount = 0;
                do
                {
                    System.Threading.Thread.Sleep(rthDelay_mS);
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.RthDsdbr_ohm, dsdbrTec.SensorResistanceActual_ohm);
                    //tcmzChan.MidTempData.SetValueDouble(EnumTosaParam.RthMz_ohm, mzTec.SensorResistanceActual_ohm);
                }
                while (ilmzChan.MidTempData.ParamStatus(EnumTosaParam.RthDsdbr_ohm) == PassFail.Fail 
                    && rthCount++ < numRthTries);//||tcmzChan.MidTempData.ParamStatus(EnumTosaParam.RthMz_ohm) == PassFail.Fail)


                #region
                                RearSoaTuningTxRx(configData, ref mzInstrs, ref ilmzChan);
             

                #endregion

                double tmpRxCurrent_mA=0.0;
                //mx
    


                // Try next channel option if fail
                if (!alwaysTuneLaser && ilmzChan.PassStatus == PassFail.Fail)
                {
                    engine.SendToGui(new GuiTextMessage("Channel option test stopped, thermistor resistances fail", 2));
                    ilmzChan.MidTempData.SetValueBool(EnumTosaParam.Pass_Flag, false);
                    ilmzChan.MidTempData.SetFinished();
                    continue;
                }
                if (ilmzChan.PassStatus == PassFail.Fail)
                    engine.SendToGui(new GuiTextMessage("Forcing test to continue despite failing for : " + ilmzChan.MidTempData.FailedParamNames, 1));
                #endregion

                // Switch to MZ power meter
                //switchOsaMzOpm.SetState(Switch_Osa_MzOpm.State.MzOpm);

                #region Power Level
                engine.SendToGui(new GuiTextMessage("Power levelling", 2));
                
				mzInstrs.PowerMeter.Range = InstType_OpticalPowerMeter.AutoRange;

                // Power level
                double targetPower_mW = Alg_PowConvert_dB.Convert_dBmtomW(targetPowerDbm);
                double tune_OpticalPowerTolerance_mW = configData.ReadDouble("Tune_OpticalPowerTolerance_mW");
                double tune_OpticalPowerTolerance_dB = configData.ReadDouble("Tune_OpticalPowerTolerance_dB");
                Measurements.FrequencyWithoutMeter = chanFreq_GHz;
                DatumList powerLevelResults = IlmzCommonUtils.SoaPowerLevel.PowerLevelWithAdaptiveSlope(
                    targetPower_mW,
                    tune_OpticalPowerTolerance_dB, 
                    ilmzChannelChar_ZD_configObj.Tune_IsoaPowerSlope, 
                    maxSoaForPwrLeveling);

                // Store result and stop testing this channel option if fail
                ilmzChan.MidTempData.SetValueBool(EnumTosaParam.PowerLevellingOk, powerLevelResults.ReadBool("PowerLevelOk"));
                ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.ISoa_mA, powerLevelResults.ReadDouble("lastTunedIsoa"));

                // Try next channel option if fail
                if (alwaysTuneLaser && ilmzChan.PassStatus == PassFail.Fail)
                {
                    engine.SendToGui(new GuiTextMessage("Channel option test stopped, unable to set target fibre power", 2));
                    powerLevelingFailedInstanceCount++;
                    ilmzChan.MidTempData.SetValueBool(EnumTosaParam.Pass_Flag, false);
                    ilmzChan.MidTempData.SetFinished();
                    continue;
                }
                if (ilmzChan.PassStatus == PassFail.Fail)
                    engine.SendToGui(new GuiTextMessage("Forcing test to continue despite failing for : " + ilmzChan.MidTempData.FailedParamNames, 1));

                #endregion

                // Switch to OSA
                //switchOsaMzOpm.SetState(Switch_Osa_MzOpm.State.Osa);

                double readLockerRatioTime = 0;
                double readWaveMeterTime = 0;
                double eolSMSRTestTime = 0;
                double modeACQTestTime = 0;
                double superModeSRTestTime = 0;

                DateTime eolSMSRTestStartTime = DateTime.Now;

                if (this.isTestEOL)
                {
                    #region EOL Neg & Pos Test - chongjian.liang 2013.4.9

                    engine.SendToGui(new GuiTextMessage("EOL tuning", 2));

                    #region EOL Negative & Positive frequencies tuning & do SMSR

                    DatumList ituEolTuneParams = new DatumList();
                    ituEolTuneParams.AddDouble("FreqOffsetPos_GHz", chanFreq_GHz + ilmzChannelChar_ZD_configObj.PtrPosOffset_GHz);
                    ituEolTuneParams.AddDouble("FreqOffsetNeg_GHz", chanFreq_GHz - ilmzChannelChar_ZD_configObj.PtrNegOffset_GHz);
                    ituEolTuneParams.Add(previousTestData.GetDatum("BandType"));

                    IlmzCommonUtils.DsdbrTuning.OpticalSwitch = this.Optical_switch;

                    // ITU tuning
                    DatumList ituEolTuneResults = IlmzCommonUtils.DsdbrTuning.ItuEolTune(ituEolTuneParams);
                    #endregion

                    #region Record test data

                    // Record the EOL Neg phase current
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.IPhaseLowerEOL_mA, ituEolTuneResults.ReadDouble("IphaseOffsetNeg_mA"));
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.IPhaseUpperEOL_mA, ituEolTuneResults.ReadDouble("IphaseOffsetPos_mA"));

                    // Record the EOL Neg phase current in DAC
                    if (DsdbrUtils.DsdbrDriverInstrsToUse == DsdbrDriveInstruments.FCU2AsicInstrument)
                    {
                        ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.IPhaseLowerEOL_Dac, DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic.mAToDacForPhase_Asic((float)ituEolTuneResults.ReadDouble("IphaseOffsetNeg_mA")));
                        ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.IPhaseUpperEOL_Dac, DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic.mAToDacForPhase_Asic((float)ituEolTuneResults.ReadDouble("IphaseOffsetPos_mA")));
                    }

                    // Record the EOL frequency
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.Freq_PTR_EOL_Neg_GHz, ituEolTuneResults.ReadDouble("FreqOffsetNeg_GHz"));
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.Freq_PTR_EOL_Pos_GHz, ituEolTuneResults.ReadDouble("FreqOffsetPos_GHz"));

                    // Record SMSR
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.SMSR_PTR_EOL_Neg_dB, ituEolTuneResults.ReadDouble("SmsrOffsetNeg_dB"));
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.SMSR_PTR_EOL_Pos_dB, ituEolTuneResults.ReadDouble("SmsrOffsetPos_dB"));
                    
                    // Record if TuningOk
                    ilmzChan.MidTempData.SetValueBool(EnumTosaParam.TuningOk, ituEolTuneResults.ReadBool("TuningOk"));

                    // Record rear delta freq after tuning
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.DeltaFreq_GHz, ituEolTuneResults.ReadDouble("DeltaFreq_GHz"));

                    // Append the EOL tuning information
                    ilmzChan.EOL_Testing_Info = ilmzChan.EOL_Testing_Info + ituEolTuneResults.ReadString("EOL_Testing_Info");
                    ilmzChan.ReTuneReason = (Re_Tune_Reason)ituEolTuneResults.ReadEnum("Re_Tune_Reason");
                    #endregion

                    #region Try next channel option if fail

                    if (ilmzChan.PassStatus == PassFail.Fail)
                    {
                        engine.SendToGui(new GuiTextMessage("Channel option test stopped, test failed for : " + ilmzChan.MidTempData.FailedParamNames, 1));

                        if (alwaysTuneLaser)
                        {
                            ilmzChan.MidTempData.SetValueBool(EnumTosaParam.Pass_Flag, false);
                            ilmzChan.MidTempData.SetFinished();
                            continue;
                        }
                    }
                    #endregion

                    #endregion
                }

                eolSMSRTestTime = (DateTime.Now - eolSMSRTestStartTime).TotalSeconds;

                #region ITU & EOL tuning
                // Tune device
                engine.SendToGui(new GuiTextMessage("ITU Tuning", 2));

                // Initialise parameters
                DatumList ituTuneParams = new DatumList();
                string Laser_Wafer_Size;
                Laser_Wafer_Size = previousTestData.GetDatumString("LASER_WAFER_SIZE").ToString();
                ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.FreqCal_GHz, chanFreq_GHz + calOffset_GHz);
                ituTuneParams.AddDouble("ItuFreq_GHz", chanFreq_GHz);
                ituTuneParams.AddDouble("CalFreq_GHz", ilmzChan.MidTempData.GetValueDouble(EnumTosaParam.FreqCal_GHz));

                if (Laser_Wafer_Size.Contains("3"))
                {
                    ituTuneParams.AddDouble("FreqOffsetPos_GHz", ilmzChannelChar_ZD_configObj.PtrPosOffset_GHz);
                }
                else
                {
                    ituTuneParams.AddDouble("FreqOffsetPos_GHz", 0);
                }
                if (Laser_Wafer_Size.Contains("3"))
                {
                    ituTuneParams.AddDouble("FreqOffsetNeg_GHz", ilmzChannelChar_ZD_configObj.PtrNegOffset_GHz);                    
                }
                else
                {
                    //ituTuneParams.AddDouble("FreqOffsetPos_GHz", 16);
                    ituTuneParams.AddDouble("FreqOffsetNeg_GHz", 0);
                }

                // previousTestData
                ituTuneParams.Add(previousTestData.GetDatum("LASER_WAFER_SIZE"));
                ituTuneParams.Add(previousTestData.GetDatum("BandType"));
                //ituTuneParams.Add(previousTestData.GetDatum("SmFiles"));
                //ituTuneParams.Add(previousTestData.GetDatum("NUM_LM"));
                //ituTuneParams.AddSint32("Supermode", tcmzChan.MidTempData.GetValueSint32(EnumTosaParam.Supermode));
                //ituTuneParams.AddSint32("LongitudinalMode", tcmzChan.MidTempData.GetValueSint32(EnumTosaParam.LongitudinalMode));

                // Alice.Huang   2010-02-10
                // add FCU2ASIC FrontSection reading here
                if (DsdbrUtils.DsdbrDriverInstrsToUse !=
                    DsdbrDriveInstruments.FCU2AsicInstrument)
                {
                    int fsNonConstNum = ilmzChan.MidTempData.GetValueSint32(EnumTosaParam.FrontSectionPair) + 1;
                    DSDBRSection fsNonConst = (DSDBRSection)Enum.Parse(typeof(DSDBRSection), "Front" + fsNonConstNum.ToString());
                    ituTuneParams.AddDouble("IFsNonConst_mA", DsdbrUtils.ReadSectionCurrent_mA(fsNonConst));
                }
                else
                {
                    int fsNonConstNum = ilmzChan.MidTempData.GetValueSint32(EnumTosaParam.FrontSectionPair) + 1;

                    DSDBRSection fsNonConst = (DSDBRSection)Enum.Parse(typeof(DSDBRSection), "Front" + fsNonConstNum.ToString());
                    curReading = DsdbrUtils.ReadSectionCurrent_mA(fsNonConst);

                    dacTemp =(int ) ( curReading * 
                        DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic.SectionsDacCalibration.FsDac_CalFactor +
                        DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic.SectionsDacCalibration.FsDac_CalOffset);

                    ituTuneParams.AddDouble("IFsNonConst_mA", curReading );
                    ituTuneParams.AddDouble("IFsNonConst_Dac",(double) dacTemp);                
                }

                // ITU tuning
                DatumList ituTuneResults = null;
                bool DoneEOLTestFlag = false;


                IlmzCommonUtils.DsdbrTuning.IsTestSMSR = false;
                IlmzCommonUtils.DsdbrTuning.OpticalSwitch = this.Optical_switch;
                ituTuneResults = IlmzCommonUtils.DsdbrTuning.IlmzItuTuning(ituTuneParams);
                //ituTuneResults = IlmzCommonUtils.DsdbrTuning.ItuEolTune(ituTuneParams);
                ilmzChan.EOL_Testing_Info += ituTuneResults.ReadString("itu_Tunning_Info");

                //SelectTestFlag = ituTuneResults.ReadBool("SelectTestFlag");
                //ilmzChan.EOL_Testing_Info = ituTuneResults.ReadString("EOL_Testing_Info");
               // ilmzChan.ReTuneReason = (Re_Tune_Reason)ituTuneResults.ReadEnum("Re_Tune_Reason");

                // Save results          
                
                //ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.PhaseTuningEff, ituTuneResults.ReadDouble("PhaseTuneEff")); //Echo remed this line because in this product itu point is equal to itu_cal point. and then this PTE calculate is wrong,we get PTE value inside locker slope testing.
                ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.FreqCal_GHz, ituTuneResults.ReadDouble("FreqCal_GHz"));
                double freqItu_Ghz = ituTuneResults.ReadDouble("FreqItu_GHz");
                ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.Freq_GHz, ituTuneResults.ReadDouble("FreqItu_GHz"));
                if (freqItu_Ghz != 0 && float.Parse(freqHigh_GHz.ToString()) > 0)
                {
                    double waveLength_Itu_nm = Alg_FreqWlConvert.Freq_GHz_TO_Wave_nm(freqItu_Ghz);
                    mzInstrs.PowerMeter.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_mW;
                    mzInstrs.PowerMeter.Wavelength_nm = waveLength_Itu_nm;
                }
                ilmzChan.MidTempData.SetValueBool(EnumTosaParam.TuningOk, ituTuneResults.ReadBool("TuneOk"));

                // Alice.Huang 2010-03-07
                // skip all dac store because there is  no limit

                // Update Dsdbr currents

                curReading = ituTuneResults.ReadDouble("IphaseCal_mA");
                ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.IPhaseCal_mA, curReading);
                if (DsdbrUtils.DsdbrDriverInstrsToUse == DsdbrDriveInstruments.FCU2AsicInstrument)
                {
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.IPhaseCal_Dac, (double)DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic.mAToDacForPhase_Asic((float)curReading));
                }
                curReading = ituTuneResults.ReadDouble("IphaseItu_mA");
                ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.IPhaseITU_mA, curReading);
                if (DsdbrUtils.DsdbrDriverInstrsToUse == DsdbrDriveInstruments.FCU2AsicInstrument)
                {
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.IPhaseITU_Dac, (double)DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic.mAToDacForPhase_Asic((float)curReading));
                }
                // Commented by Alice.Huang, 2010-02-09
                // it seems to just repeat the value setting done above
                //tcmzChan.MidTempData.SetValueDouble(EnumTosaParam.IPhaseCal_mA, ituTuneResults.ReadDouble("IphaseCal_mA"));

                curReading = DsdbrUtils.ReadSectionCurrent_mA(DSDBRSection.Rear);
                ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.IRear_mA, curReading);

                if (DsdbrUtils.DsdbrDriverInstrsToUse == DsdbrDriveInstruments.FCU2AsicInstrument)
                {
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.IRear_Dac, (double)DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic.mAToDacForRear_Asic((float)curReading));
                }

                curReading = DsdbrUtils.ReadSectionCurrent_mA(DSDBRSection.Gain);
                ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.IGain_mA, curReading);

                if (DsdbrUtils.DsdbrDriverInstrsToUse == DsdbrDriveInstruments.FCU2AsicInstrument)
                {
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.IGain_Dac, (double)DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic.mAToDacForGain_Asic((float)curReading));
                }

                if (DsdbrUtils.DsdbrDriverInstrsToUse == DsdbrDriveInstruments.FCU2AsicInstrument)
                {
                    curReading = ilmzChan.MidTempData.DsdbrSetup.IRearSoa_mA;
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.IRearSoa_mA, curReading);
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.IRearSoa_Dac, (double)DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic.mAToDacForRearSoa_Asic((float)curReading));
                }


                curReading = DsdbrUtils.ReadSectionCurrent_mA(DSDBRSection.SOA);
                ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.ISoa_mA, curReading);

                if (DsdbrUtils.DsdbrDriverInstrsToUse == DsdbrDriveInstruments.FCU2AsicInstrument)
                {
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.ISoa_Dac, (double)DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic.mAToDacForFrontSoa_Asic((float)curReading));
                }

                int fsConstNum = ilmzChan.MidTempData.GetValueSint32(EnumTosaParam.FrontSectionPair);

                if (DsdbrUtils.DsdbrDriverInstrsToUse != DsdbrDriveInstruments.FCU2AsicInstrument)
                {
                    int fsNinConstNum = fsConstNum + 1;
                    DSDBRSection iFsConst = (DSDBRSection)Enum.Parse(typeof(DSDBRSection), "Front" + fsConstNum.ToString().Trim());
                    DSDBRSection iFsNonConst = (DSDBRSection)Enum.Parse(typeof(DSDBRSection), "Front" + fsNinConstNum.ToString().Trim());
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.IFsConst_mA, DsdbrUtils.ReadSectionCurrent_mA(iFsConst));
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.IFsNonConst_mA, DsdbrUtils.ReadSectionCurrent_mA(iFsNonConst));
                }
                else
                {
                    double iFsConst;
                    double iFsNonConst;

                    int fsNinConstNum = fsConstNum + 1;
                    DSDBRSection scFsConst = (DSDBRSection)Enum.Parse(typeof(DSDBRSection), "Front" + fsConstNum.ToString().Trim());
                    DSDBRSection scFsNonConst = (DSDBRSection)Enum.Parse(typeof(DSDBRSection), "Front" + fsNinConstNum.ToString().Trim());

                    iFsConst = DsdbrUtils.ReadSectionCurrent_mA(scFsConst);
                    iFsNonConst = DsdbrUtils.ReadSectionCurrent_mA(scFsNonConst);

                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.IFsConst_mA, iFsConst);
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.IFsNonConst_mA, iFsNonConst );
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.IFsConst_Dac, (double)DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic.mAToDacForFront_Asic((float)iFsConst));
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.IFsNonConst_Dac, (double)DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic.mAToDacForFront_Asic((float)iFsNonConst));
                }
                if (DoneEOLTestFlag)
                {
                    ilmzChan.MidTempData.SetValueBool(EnumTosaParam.TuningOk, ituTuneResults.ReadBool("TuneOk"));
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.Freq_PTR_EOL_Pos_GHz, ituTuneResults.ReadDouble("FreqOffsetPos_GHz"));

                    curReading = ituTuneResults.ReadDouble("IphaseOffsetPos_mA");
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.IPhaseUpperEOL_mA, Math.Round(curReading,4));
                    if (DsdbrUtils.DsdbrDriverInstrsToUse == DsdbrDriveInstruments.FCU2AsicInstrument)
                    {
                        ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.IPhaseUpperEOL_Dac, (double)DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic.mAToDacForPhase_Asic((float)curReading));
                    }

                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.SMSR_PTR_EOL_Pos_dB, ituTuneResults.ReadDouble("SmsrOffsetPos_dB"));
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.Freq_PTR_EOL_Neg_GHz, ituTuneResults.ReadDouble("FreqOffsetNeg_GHz"));
                    
                    curReading =ituTuneResults.ReadDouble("IphaseOffsetNeg_mA");
                    if (curReading.ToString().ToLower().Contains("nan")) curReading = 0;
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.IPhaseLowerEOL_mA, Math.Round(curReading,4));
                    if (DsdbrUtils .DsdbrDriverInstrsToUse == DsdbrDriveInstruments .FCU2AsicInstrument )
                    {
                        ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.IPhaseLowerEOL_Dac, (double)DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic.mAToDacForPhase_Asic((float)curReading));
                    }

                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.SMSR_PTR_EOL_Neg_dB, ituTuneResults.ReadDouble("SmsrOffsetNeg_dB"));
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.DeltaFreq_GHz, ituTuneResults.ReadDouble("DeltaFreq_GHz"));

                    //Echo remed the following lines, according to david's formula mail
                /*// Mode hop detection
                double ptrEolSlope =
                    (ilmzChan.MidTempData.GetValueDouble(EnumTosaParam.IPhaseUpperEOL_mA)
                      - ilmzChan.MidTempData.GetValueDouble(EnumTosaParam.IPhaseLowerEOL_mA))
                    /
                    (ilmzChan.MidTempData.GetValueDouble(EnumTosaParam.Freq_PTR_EOL_Pos_GHz)
                      - ilmzChan.MidTempData.GetValueDouble(EnumTosaParam.Freq_PTR_EOL_Neg_GHz));

                double ptrItuCalSlope =
                    (ilmzChan.MidTempData.GetValueDouble(EnumTosaParam.IPhaseITU_mA)
                      - ilmzChan.MidTempData.GetValueDouble(EnumTosaParam.IPhaseCal_mA))
                    /
                    (ilmzChan.MidTempData.GetValueDouble(EnumTosaParam.Freq_GHz)
                      - ilmzChan.MidTempData.GetValueDouble(EnumTosaParam.FreqCal_GHz));*/

                    double ptrEolSlope =
                        (ilmzChan.MidTempData.GetValueDouble(EnumTosaParam.IPhaseUpperEOL_mA)
                          - ilmzChan.MidTempData.GetValueDouble(EnumTosaParam.IPhaseCal_mA))
                        /
                        (ilmzChan.MidTempData.GetValueDouble(EnumTosaParam.Freq_PTR_EOL_Pos_GHz)
                          - ilmzChan.MidTempData.GetValueDouble(EnumTosaParam.FreqCal_GHz));

                    double ptrItuCalSlope =
                        (ilmzChan.MidTempData.GetValueDouble(EnumTosaParam.IPhaseITU_mA)
                          - ilmzChan.MidTempData.GetValueDouble(EnumTosaParam.IPhaseLowerEOL_mA))
                        /
                        (ilmzChan.MidTempData.GetValueDouble(EnumTosaParam.Freq_GHz)
                         - ilmzChan.MidTempData.GetValueDouble(EnumTosaParam.Freq_PTR_EOL_Neg_GHz));

                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.PtrSlopeMetric, ptrEolSlope / ptrItuCalSlope);
                }

                // Try next channel option if fail
                if (alwaysTuneLaser && ilmzChan.PassStatus == PassFail.Fail)
                {
                    engine.SendToGui(new GuiTextMessage("Channel option test stopped, unable to tune channel", 1));
                    ilmzChan.MidTempData.SetValueBool(EnumTosaParam.Pass_Flag, false);
                    ilmzChan.MidTempData.SetFinished();                                                       
                    continue;
                }

                if (ilmzChan.PassStatus == PassFail.Fail)
                    engine.SendToGui(new GuiTextMessage("Forcing test to continue despite failing for : " + ilmzChan.MidTempData.FailedParamNames, 1));

                #endregion

                if (isTestEOL)
                {
                    #region Calc PtrSlopeMetric

                    double ptrEolSlope = (ilmzChan.MidTempData.GetValueDouble(EnumTosaParam.IPhaseUpperEOL_mA) - ilmzChan.MidTempData.GetValueDouble(EnumTosaParam.IPhaseCal_mA))
                        / (ilmzChan.MidTempData.GetValueDouble(EnumTosaParam.Freq_PTR_EOL_Pos_GHz) - ilmzChan.MidTempData.GetValueDouble(EnumTosaParam.FreqCal_GHz));

                    double ptrItuCalSlope = (ilmzChan.MidTempData.GetValueDouble(EnumTosaParam.IPhaseITU_mA) - ilmzChan.MidTempData.GetValueDouble(EnumTosaParam.IPhaseLowerEOL_mA))
                        / (ilmzChan.MidTempData.GetValueDouble(EnumTosaParam.Freq_GHz) - ilmzChan.MidTempData.GetValueDouble(EnumTosaParam.Freq_PTR_EOL_Neg_GHz));

                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.PtrSlopeMetric, ptrEolSlope / ptrItuCalSlope);
                    #endregion
                } 

                DateTime modeACQTestTimeStart = DateTime.Now;

                #region Mode Acquisition Current

                engine.SendToGui(new GuiTextMessage("Calculating mode acquisition current", 2));

                double iphaseModeAcq = 0;

                if (this.isTestModeACQ) // Enable the ACQ test for Huawei device. - chongjian.liang 2013.4.30
                {
                    short sm = (short)ilmzChan.MidTempData.GetValueSint32(EnumTosaParam.Supermode);
                    short lm = (short)ilmzChan.MidTempData.GetValueSint32(EnumTosaParam.LongitudinalMode);

                    string phaseModAcqFilename = this.GetFileToCalPhaseModeAcq(IPhaseModAcqFilesCollection, this.decompressedFileFolder, lm, sm);
                    iphaseModeAcq = CsvDataLookup.IPhaseAcquisition(ilmzChan.MidTempData.DsdbrSetup.IRear_mA, phaseModAcqFilename);
                    iphaseModeAcq = Math.Min(iphaseModeAcq, ilmzChan.MidTempData.GetValueDouble(EnumTosaParam.IPhaseITU_mA));
                    //iphaseModeAcq = Math.Max(iphaseModeAcq, ilmzChan.MidTempData.GetValueDouble(EnumTosaParam.IPhaseCal_mA));
                }
                else
                {
                    iphaseModeAcq = ilmzChan.MidTempData.GetValueDouble(EnumTosaParam.IPhaseCal_mA);
                }                

                curReading = iphaseModeAcq ;
                ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.IPhaseModeAcq_mA, curReading);

                if (DsdbrUtils .DsdbrDriverInstrsToUse == DsdbrDriveInstruments .FCU2AsicInstrument )
                {
                    dacTemp = (int ) (curReading * 
                        DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic.SectionsDacCalibration.PhaseDac_CalFactor +
                        DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic.SectionsDacCalibration.PhaseDac_CalOffset );

                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.IPhaseModeAcq_Dac,(double ) dacTemp);
                }

                if (this.isTestModeACQ)
                {
                    // We now have all the laser data. Try next channel option if fail
                    if (ilmzChan.PassStatus == PassFail.Fail)
                    {
                        if (alwaysTuneLaser)
                        {
                            engine.SendToGui(new GuiTextMessage("Channel option test stopped, mode acquisition current failed", 1));
                            
                            ilmzChan.MidTempData.SetValueBool(EnumTosaParam.Pass_Flag, false);
                            ilmzChan.MidTempData.SetFinished();
                            continue;
                        }
                    }
                }

                #endregion

                modeACQTestTime = (DateTime.Now - modeACQTestTimeStart).TotalSeconds;

                // Switch to MZ power meter
                //switchOsaMzOpm.SetState(Switch_Osa_MzOpm.State.MzOpm);

                #region MZ final characterisation (Differential sweeps)
                // MZ sweeps 

                //
                // Imbalance calculations
                //

                //engine.SendToGui(new GuiTextMessage("TTR - Skipped MZ differential imbalance sweep", 2));

                IlmzChannelInit initMz = ilmzChan.IlmzInitialSettings;
                mzInstrs.PowerMeter.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_mW;
                //Alice.Huang    2010-08-31
                // set power meter range to its initial range before any sweep 
                //for yokogawa powermeter,we always use autorange ,Echo,2011-07-28
                mzInstrs.PowerMeter.Range = configData.ReadDouble("OpmRangeInit_mW");
                

                ILMZSweepResult mzDiffData;
                double minLevel_dBm = -50;
                bool dataOk = true;
                bool mzSweepFailed = false;
                
                // Use interpolated imbalance values
                ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.MzLeftArmImb_mA, initMz.Mz.LeftArmImb_mA);
                ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.MzRightArmImb_mA, initMz.Mz.RightArmImb_mA);

                // Prepare new MZ Bias settings
                IlmzChannelInit newMzBiases = new IlmzChannelInit();
                // Use new imbalance settings from last sweep
                newMzBiases.Mz.LeftArmImb_mA = initMz.Mz.LeftArmImb_mA;
                newMzBiases.Mz.RightArmImb_mA = initMz.Mz.RightArmImb_mA;

                // add this part for osa test without imb sweep    2010-05-31   for osa test
                if (mzInstrs.ImbsSourceType == IlMzInstruments.EnumSourceType.Asic )
                {
                    newMzBiases.Mz.LeftArmImb_Dac = (int)(newMzBiases.Mz.LeftArmImb_mA 
                        * mzInstrs.FCU2Asic.SectionsDacCalibration.ImbLeftDac_CalFactor 
                        + mzInstrs.FCU2Asic.SectionsDacCalibration.ImbLeftDac_CalOffset);

                    newMzBiases.Mz.RightArmImb_Dac = (int)(newMzBiases.Mz.RightArmImb_mA 
                        * mzInstrs.FCU2Asic.SectionsDacCalibration.ImbRightDac_CalFactor 
                        + mzInstrs.FCU2Asic.SectionsDacCalibration.ImbRightDac_CalOffset);

                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.MzLeftArmImb_Dac, newMzBiases.Mz.LeftArmImb_Dac);
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.MzRightArmImb_Dac, newMzBiases.Mz.RightArmImb_Dac);
                }

                
                if (actualLVSweep)
                {
                    //
                    // Differential Modulator sweep.
                    //
                    engine.SendToGui(new GuiTextMessage("MZ differential modulator sweep", 2));

                    // Range = 0 to 2 x Vcm, fixed step size
                    double Vcm = (initMz.Mz.LeftArmMod_Quad_V + initMz.Mz.RightArmMod_Quad_V) / 2;
                    int NumberOfSteps = (int)(1 + Math.Abs(Vcm * 2 / (mzConfig.MZInitSweepStepSize_mV / 1000)));

                    // If Vcm is high we need to restrict the max bias level on either arm to its max limit by just performing the middle section of the sweep.
                    double startBias = 2 * Vcm;
                    double stopBias = 0;
                    // Retry if necessary
                    MzAnalysisWrapper.MzAnalysisResults mzDiffModResults;
                    do
                    {
                        mzSweepFailed = false;
                        do
                        {

                                mzDiffData = ilmzUtils.ModArm_DifferentialSweepByTrigger(
                                    newMzBiases.Mz.LeftArmImb_mA / 1000, 
                                    newMzBiases.Mz.RightArmImb_mA/1000,
                                    startBias, 
                                    stopBias, 
                                    NumberOfSteps, 
                                    MZSourceMeasureDelay_ms,
                                    mzConfig.MZTapBias_V, 
                                    mzConfig.MZTapBias_V, 
							        ArmSoureByAsic);
                            // If overrange run again
                            if (MzAnalysisWrapper.CheckForOverrange(mzDiffData.SweepData[ILMZSweepDataType.FibrePower_mW]))
                            {
                                dataOk = false;

                                mzInstrs.PowerMeter.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_mW;
                                mzInstrs.PowerMeter.Range = mzInstrs.PowerMeter.Range * 10;
                                if (double.IsNaN(mzInstrs.PowerMeter.Range))
                                {
                                    mzInstrs.PowerMeter.Range = 10;
                                }
                            }
                            else
                            {
                                // if underrange fix the data and continue
                                dataOk = true;
                                if (MzAnalysisWrapper.CheckForUnderrange(mzDiffData.SweepData[ILMZSweepDataType.FibrePower_mW]))
                                {
                                    mzDiffData.SweepData[ILMZSweepDataType.FibrePower_mW] =
                                        MzAnalysisWrapper.FixUnderRangeData(
                                        mzDiffData.SweepData[ILMZSweepDataType.FibrePower_mW],
                                        minLevel_dBm);
                                }
                            }
                        }
                        while (!dataOk);
                        
                        // Analyse LV data

                        if (this.isUseNegSlopeImb)
                        {
                            mzDiffModResults = MzAnalysisWrapper.Differential_NegChirp_DifferentialSweep(mzDiffData, Vcm, 0);
                        }
                        else
                        {
                            mzDiffModResults = MzAnalysisWrapper.ZeroChirp_DifferentialSweep(mzDiffData, Vcm);
                        }

                        // Check that the data looked ok.
                        if (Math.Abs(mzDiffModResults.PowerAtMax_dBm) > 200)
                        {
                            // The power meter has probably over-ranged. Increase the range and try again.
                            engine.SendToGui(new GuiTextMessage("Retry MZ differential modulator sweep at higher power range", 3));
                            opmRange = mzInstrs.PowerMeter.Range;
                            if (opmRange < 1)
                            {
                                mzInstrs.PowerMeter.Range = opmRange * 10;
                                mzSweepFailed = true;
                            }
                        }
                    } while (mzSweepFailed == true);
                                        
                    // Save results
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.MzLeftArmModPeak_V, mzDiffModResults.Max_SrcL);
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.MzRightArmModPeak_V, mzDiffModResults.Max_SrcR);
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.MzLeftArmModQuad_V, mzDiffModResults.Quad_SrcL);
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.MzRightArmModQuad_V, mzDiffModResults.Quad_SrcR);
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.MzLeftArmModMinima_V, mzDiffModResults.Min_SrcL);
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.MzRightArmModMinima_V, mzDiffModResults.Min_SrcR);
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.MzDcVpi_V, mzDiffModResults.Vpi);
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.MzAcVpi_V, mzDiffModResults.Vpi * ilmzChannelChar_ZD_configObj.AcVpiScaleFactor);
                    // Comment out below line since ExtinctionRatio_dB is not right to save as Freq_PTR_EOL_Pos_GHz - chongjian.liang 2013.4.16
                    // ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.Freq_PTR_EOL_Pos_GHz, mzDiffModResults.ExtinctionRatio_dB);//raul added to record the 1st ER
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.FibrePwrPeak_dBm, mzDiffModResults.PowerAtMax_dBm);
                    
                    // Fill the MZ LV slope by chongjian.liang 2013.5.11
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.MZ_LV_Slope, mzDiffModResults.Max_SrcL > mzDiffModResults.Min_SrcL ? 1 : -1);

                    newMzBiases.Mz.LeftArmMod_Peak_V = mzDiffModResults.Max_SrcL;
                    newMzBiases.Mz.RightArmMod_Peak_V = mzDiffModResults.Max_SrcR;
                    newMzBiases.Mz.LeftArmMod_Quad_V = mzDiffModResults.Quad_SrcL;
                    newMzBiases.Mz.RightArmMod_Quad_V = mzDiffModResults.Quad_SrcR;
                    newMzBiases.Mz.LeftArmMod_Min_V = mzDiffModResults.Min_SrcL;
                    newMzBiases.Mz.RightArmMod_Min_V = mzDiffModResults.Min_SrcR;

                    // store plots
                    string LVFileName = Util_GenerateFileName.GenWithTimestamp(
                        mzFileDirectory,
                        "FinalDiffMzLV_" + chanFreq_GHz.ToString() + "_" + ilmzChan.MidTempData.Priority.ToString(), 
                        dutSerialNbr, 
                        "csv");

                    // write to file
                    MzSweepFileWriter.WriteSweepData(LVFileName, mzDiffData,
                        new ILMZSweepDataType[] { ILMZSweepDataType.LeftArmModBias_V,
                                        ILMZSweepDataType.RightArmModBias_V,
                                        ILMZSweepDataType.LeftMinusRight,
                                        ILMZSweepDataType.FibrePower_mW,
                                        ILMZSweepDataType.TapComplementary_mA});
                    // Add to archive
                    zipFile.AddFileToZip(LVFileName);
                    engine.SendToGui(mzDiffData);

                    // Add a nice arrow to point at the quadrature point
                    NPlot.PointD marker = new NPlot.PointD(mzDiffModResults.Imb_SrcL - mzDiffModResults.Imb_SrcR, mzDiffModResults.PowerAtMax_dBm - 3);
                    engine.SendToGui(marker);

                    // Add some information to help technicians & engineers
                    NPlot.TextItem markerText = new NPlot.TextItem(new NPlot.PointD(marker.X, mzDiffModResults.PowerAtMin_dBm + 5), "Vpi = " + mzDiffModResults.Vpi.ToString("0.0"));
                    engine.SendToGui(markerText);

                    NPlot.VerticalLine verticalLine = new NPlot.VerticalLine(mzDiffModResults.Max_SrcL - mzDiffModResults.Max_SrcR, System.Drawing.Color.IndianRed);
                    engine.SendToGui(verticalLine);

                    verticalLine = new NPlot.VerticalLine(mzDiffModResults.Min_SrcL - mzDiffModResults.Min_SrcR, System.Drawing.Color.IndianRed);
                    engine.SendToGui(verticalLine);
                }
                else
                {
                    //
                    // Calculate Differential Modulator Sweep Data
                    //
                    engine.SendToGui(new GuiTextMessage("Calculate Differential Modulator Sweep Data", 2));
                    double mzLeftArmModPeak_V = interpolateParam(engine, EnumTosaParam.MzLeftArmModPeak_V, chanFreq_GHz);
                    double mzLeftArmModQuad_V = interpolateParam(engine, EnumTosaParam.MzLeftArmModQuad_V, chanFreq_GHz);
                    double mzRightArmModPeak_V = interpolateParam(engine, EnumTosaParam.MzRightArmModPeak_V, chanFreq_GHz);
                    double mzRightArmModQuad_V = interpolateParam(engine, EnumTosaParam.MzRightArmModQuad_V, chanFreq_GHz);
                    double mzDcVpi_V = interpolateParam(engine, EnumTosaParam.MzDcVpi_V, chanFreq_GHz);
                    double mzAcVpi_V = mzDcVpi_V * ilmzChannelChar_ZD_configObj.AcVpiScaleFactor;
                    double fibrePwrPeak_dBm = interpolateParam(engine, EnumTosaParam.FibrePwrPeak_dBm, chanFreq_GHz);

                    // Save results
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.MzLeftArmModPeak_V, mzLeftArmModPeak_V);
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.MzRightArmModPeak_V, mzRightArmModPeak_V);
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.MzLeftArmModQuad_V, mzLeftArmModQuad_V);
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.MzRightArmModQuad_V, mzRightArmModQuad_V);
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.MzLeftArmModMinima_V, mzLeftArmModPeak_V + mzDcVpi_V / 2);
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.MzRightArmModMinima_V, mzRightArmModPeak_V - mzDcVpi_V / 2);
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.MzDcVpi_V, mzDcVpi_V);
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.MzAcVpi_V, mzAcVpi_V);
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.FibrePwrPeak_dBm, fibrePwrPeak_dBm);

                    // Build newMzBiases.Mz
                    newMzBiases.Mz.LeftArmMod_Peak_V = mzLeftArmModPeak_V;
                    newMzBiases.Mz.RightArmMod_Peak_V = mzRightArmModPeak_V;
                    newMzBiases.Mz.LeftArmMod_Quad_V = mzLeftArmModQuad_V;
                    newMzBiases.Mz.RightArmMod_Quad_V = mzRightArmModQuad_V;
                    newMzBiases.Mz.LeftArmMod_Min_V = mzLeftArmModPeak_V + mzDcVpi_V / 2;// Quad is on negative slope
                    newMzBiases.Mz.RightArmMod_Min_V = mzRightArmModPeak_V - mzDcVpi_V / 2;
                }

                engine.SendToGui(new GuiTextMessage("MZ differential modulator sweep finished", 2));
                
                // Alice.Huang    2010-08-31
                // set power meter tko auto range in feature point measurement
                mzInstrs.PowerMeter.Range = InstType_OpticalPowerMeter.AutoRange;
                System.Threading.Thread.Sleep(100);

                // Set MZ to quad & measure
                int loopMzQualPointMax = 5;
                int loopMzQualPoint = 0;
                double fibrePowerQuad_dBm = 0;
                while (loopMzQualPoint < loopMzQualPointMax)
                {
                    if (!ArmSoureByAsic)
                    {
                        IlMzDriverUtils.SetupMzToQuad(mzInstrs, newMzBiases.Mz);
                    }
                    else
                    {
                        IlMzDriverUtils.SetupMzToQuad(mzInstrs.FCU2Asic, newMzBiases.Mz);
                    }
                    System.Threading.Thread.Sleep(100);
                    fibrePowerQuad_dBm = Measurements.ReadOpticalPower(OpticalPowerHead.MZHead, Measurements.PowerUnits.dBm);
                    if (fibrePowerQuad_dBm > ilmzChan.MidTempData.GetMeasuredDataLowLimit(EnumTosaParam.FibrePwrQuad_dBm) && fibrePowerQuad_dBm < ilmzChan.MidTempData.GetMeasuredDataHighLimit(EnumTosaParam.FibrePwrQuad_dBm))
                    {
                        break;
                    }
                    loopMzQualPoint++;
                }
                //mzUtils.SetCurrentSenseRange(SourceMeter.ComplementaryTap, 0.001);
                ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.FibrePwrQuad_dBm, fibrePowerQuad_dBm);
                ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.MzLeftArmModQuad_mA, mzInstrs.FCU2Asic.IimbLeft_mA);
                ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.MzRightArmModQuad_mA, mzInstrs.FCU2Asic.IimbRight_mA);

                if (DsdbrUtils.DsdbrDriverInstrsToUse != DsdbrDriveInstruments.FCU2AsicInstrument)
                {
                    //tcmzChan.MidTempData.SetValueDouble(EnumTosaParam.MzLeftArmImb_V, ilmzUtils.ReadVoltage(SourceMeter.LeftImbArm));
                    //tcmzChan.MidTempData.SetValueDouble(EnumTosaParam.MzRightArmImb_V, ilmzUtils.ReadVoltage(SourceMeter.RightImbArm));
                }
                IInstType_DigitalIO outLine_Ctap_Reference = this.Optical_switch.GetDigiIoLine(IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine);//3
                outLine_Ctap_Reference.LineState = IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine_State;//true
                mzInstrs.FCU2Asic.Locker_refi_pot = int.Parse(DsdbrTuning.locker_pot.ToString());
                mzInstrs.FCU2Asic.Locker_tran_pot = int.Parse(DsdbrTuning.locker_pot.ToString());
                ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.TapCompPhotoCurrentQuad_mA, mzInstrs.FCU2Asic.ICtap_mA);
                //if (mzInstrs.InlineTapOnline)
                    ////ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.TapInlinePhotoCurrentQuad_mA, ilmzUtils.ReadCurrent(SourceMeter.InlineTap) * 1000);

                double fibrePowerTrough_dBm = 0;
                double fibrePowerPeak_dBm = 0;

                // Set MZ to trough & measure
                if (!ArmSoureByAsic)
                {
                    IlMzDriverUtils.SetupMzToTrough(mzInstrs, newMzBiases.Mz);
                }
				else
                {
                    IlMzDriverUtils.SetupMzToTrough(mzInstrs.FCU2Asic, newMzBiases.Mz);
                }
				
                ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.MzLeftArmModMinima_mA, mzInstrs.FCU2Asic.IimbLeft_mA);
                ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.MzRightArmModMinima_mA, mzInstrs.FCU2Asic.IimbRight_mA);
                mzInstrs.PowerMeter.Range = InstType_OpticalPowerMeter.AutoRange;
                System.Threading.Thread.Sleep(100);

                fibrePowerTrough_dBm= Measurements.ReadOpticalPower(OpticalPowerHead.MZHead, Measurements.PowerUnits.dBm);

                #region Retry the out of limit trough power, and record the retry data. - chongjian.liang 2013.8.12

                int readTroughPowerCount = 0;

                const int READ_TROUGH_POWER_COUNT_MAX = 10;

                double fibrePowerTrough_BeforeRetry_dBm = fibrePowerTrough_dBm;

                double peakPowerUpperLimit = double.Parse(this.mainSpec.GetParamLimit("CH_PEAK_POWER").HighLimit.ValueToString());

                string readingsByNotResetMZ = "ReadingsByNotResetMZ[dBm]: ";
                //string readingsByResetMZ = "ReadingsByResetMZ[dBm]: ";

                // If trough power is too low, read the power again in 10 times. - chongjian.liang 2013.8.12
                while (readTroughPowerCount++ < READ_TROUGH_POWER_COUNT_MAX && fibrePowerTrough_dBm < peakPowerUpperLimit - dynamicER_HighLimits[chanFreq_GHz])
                {
                    //if (readTroughPowerCount >= READ_TROUGH_POWER_COUNT_MAX / 2)
                    //{
                    //    IlMzDriverUtils.SetupMzToTrough(mzInstrs.FCU2Asic, newMzBiases.Mz);

                    //    System.Threading.Thread.Sleep(100);

                    //    fibrePowerTrough_dBm = Measurements.ReadOpticalPower(OpticalPowerHead.MZHead, Measurements.PowerUnits.dBm);

                    //   readingsByResetMZ += fibrePowerTrough_dBm.ToString("#0.000") + ";";
                    //}
                    //else
                    {
                        System.Threading.Thread.Sleep(100);

                        fibrePowerTrough_dBm = Measurements.ReadOpticalPower(OpticalPowerHead.MZHead, Measurements.PowerUnits.dBm);

                        readingsByNotResetMZ += fibrePowerTrough_dBm.ToString("#0.000") + ";";
                    }
                }

                ilmzChan.TroughPower_Info = fibrePowerTrough_BeforeRetry_dBm.ToString("#0.000") + "; " + readingsByNotResetMZ;

                #endregion

                // Set MZ to new peak
                if (!ArmSoureByAsic)
                {
                    IlMzDriverUtils.SetupMzToPeak(mzInstrs, newMzBiases.Mz);
                }
                else
                {
                    IlMzDriverUtils.SetupMzToPeak(mzInstrs.FCU2Asic, newMzBiases.Mz);
                }
                System.Threading.Thread.Sleep(100);
                fibrePowerPeak_dBm = Measurements.ReadOpticalPower(OpticalPowerHead.MZHead, Measurements.PowerUnits.dBm);
                ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.FibrePwrPeak_dBm, fibrePowerPeak_dBm);

                if (ilmzChan.MidTempData.ParamStatus(EnumTosaParam.FibrePwrPeak_dBm) == PassFail.Fail)
                {
                    engine.SendToGui(new GuiTextMessage("Fine-tuning Optical Power", 2));

                    // Fine tune the power (again) if necessary
                    powerLevelResults = IlmzCommonUtils.SoaPowerLevel.PowerLevelWithAdaptiveSlope(
                        targetPower_mW,
                        tune_OpticalPowerTolerance_mW,
                        ilmzChannelChar_ZD_configObj.Tune_IsoaPowerSlope,
                        maxSoaForPwrLeveling);

                    ilmzChan.MidTempData.SetValueBool(EnumTosaParam.PowerLevellingOk, powerLevelResults.ReadBool("PowerLevelOk"));
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.ISoa_mA, powerLevelResults.ReadDouble("lastTunedIsoa"));
                    fibrePowerPeak_dBm = Measurements.ReadOpticalPower(OpticalPowerHead.MZHead, Measurements.PowerUnits.dBm);
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.FibrePwrPeak_dBm, fibrePowerPeak_dBm);

                }

                ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.MzLeftArmModPeak_mA,mzInstrs.FCU2Asic.IimbLeft_mA);
                ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.MzRightArmModPeak_mA,mzInstrs.FCU2Asic.IimbRight_mA );
				outLine_Ctap_Reference = Optical_switch.GetDigiIoLine(IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine);//3
                outLine_Ctap_Reference.LineState = IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine_State;//true
                mzInstrs.FCU2Asic.Locker_tran_pot = int.Parse(DsdbrTuning.locker_pot.ToString());
                mzInstrs.FCU2Asic.Locker_refi_pot = int.Parse(DsdbrTuning.locker_pot.ToString());
                ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.TapCompPhotoCurrentPeak_mA, mzInstrs.FCU2Asic.ICtap_mA);

                
                //if (mzInstrs.InlineTapOnline)
                //    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.TapInlinePhotoCurrentPeak_mA, ilmzUtils.ReadCurrent(SourceMeter.InlineTap) * 1000);
                //if (!actualLVSweep)
                //{
                ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.MzDcEr_dB, fibrePowerPeak_dBm - fibrePowerTrough_dBm);
                //}
                //raul added to record trough point pwr for tech access
                // Comment out below line since fibrePowerTrough_dBm is not right to save as Freq_PTR_EOL_Neg_GHz - chongjian.liang 2013.4.16
                // ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.Freq_PTR_EOL_Neg_GHz, fibrePowerTrough_dBm);
                //Ch_DC_ER = ilmzChan.MidTempData.GetValueDouble(EnumTosaParam.MzDcEr_dB);
                // Try next channel option if fail

                // Alice    2010- 03 -14     
                // commented for debug  
                if (ilmzChan.PassStatus == PassFail.Fail)
                {
                    engine.SendToGui(new GuiTextMessage("Channel option test stopped, modulator sweep test failed: " + ilmzChan.MidTempData.FailedParamNames, 1));
                    ilmzChan.MidTempData.SetValueBool(EnumTosaParam.Pass_Flag, false);
                    ilmzChan.MidTempData.SetFinished();
                    break;
                }


                #endregion

                if (this.isTestRippleOfEOL)
                {
                    #region Phase scan and calc ripple of the ITU longitudinal mode - chongjian.liang 2013.4.9

                    engine.SendToGui(new GuiTextMessage("ITU longitudinal mode scan", 2));

                    // Get current value before scan
                    double initIPhase = DsdbrUtils.ReadSectionCurrent_mA(DSDBRSection.Phase);
                    double initIRear = DsdbrUtils.ReadSectionCurrent_mA(DSDBRSection.Rear);

                    #region Calc scan bound from lower EOL - 2 to upper EOL + 2

                    PhaseScanOperations.IntializePhaseScanSequence(this.phaseScanSequenceFilePath);
                    
                    double phaseLowerEOL_mA = ilmzChan.MidTempData.GetValueDouble(EnumTosaParam.IPhaseLowerEOL_mA);
                    double phaseUpperEOL_mA = ilmzChan.MidTempData.GetValueDouble(EnumTosaParam.IPhaseUpperEOL_mA);

                    int index = 0;

                    while (phaseLowerEOL_mA > PhaseScanOperations.PhaseScanSequence_mA[index])
                    {
                        index++;
                    }

                    // Get the phase scan index of lower EOL - 2 as the scan start point
                    int phaseScanStartIndex = Math.Max(0, index - 2);

                    index = PhaseScanOperations.PhaseScanSequence_mA.Count - 1;

                    while (phaseUpperEOL_mA < PhaseScanOperations.PhaseScanSequence_mA[index])
                    {
                        index--;
                    }

                    // Get the phase scan index of upper EOL + 2 as the scan end point
                    int phaseScanEndIndex = Math.Min(PhaseScanOperations.PhaseScanSequence_mA.Count - 1, index + 2);
                    #endregion
                    
                    // Do ripple test
                    double ripple_GHz = RippleTestOperations.DoRippleTest(this.useLowCostSolution, null, chanFreq_GHz, this.phaseScanSequenceFilePath, this.eolPhaseScanDropFilePath, phaseScanStartIndex, phaseScanEndIndex,
                        dsdbrTec, this.CH_LASER_RTH_Min, this.CH_LASER_RTH_Max,
                        this.ripple_Min, this.ripple_Max, this.afterJumpIgnoreCount, this.beforeJumpIgnoreCount, this.multiple, this.multipleFilterString,
                        out readLockerRatioTime, out readWaveMeterTime);

                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.LMPhaseScanRipple_GHz, ripple_GHz);



                    if (ripple_GHz > maxRipple && ripple_GHz<4)
                    {
                        maxRipple = ripple_GHz;
                    }
                    // Set current back to value after scan
                    DsdbrUtils.ClockwiseStart(initIPhase, initIRear);

                    #endregion
                }
                
                #region MZ sweep at Low SOA
                    if (testSelect.IsPresent("LowSoaER") && //  optional configuration item
                        testSelect.IsTestSelected(chanFreq_GHz, "LowSoaER"))
                    {
#warning read low soa current from configuration
                        double lowSoaCurrent_mA = 20;
                        double levelledSoaCurrent_mA = DsdbrUtils.ReadSectionCurrent_mA(DSDBRSection.SOA);
                        double levelledPowerMeterRange = Measurements.MzHead.Range;

                        mzInstrs.PowerMeter.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_mW;
                        mzInstrs.PowerMeter.Range = configData.ReadDouble("OpmRangeInit_mW");
                        mzInstrs.PowerMeter.Range = InstType_OpticalPowerMeter.AutoRange;
                        DsdbrUtils.SetSectionCurrent_mA(DSDBRSection.SOA, lowSoaCurrent_mA);

                        if (!ArmSoureByAsic)
                        {
                            ilmzUtils.CleanupAfterSweep();
                        }

                        //
                        // Differential Modulator sweep.
                        //
                        engine.SendToGui(new GuiTextMessage("MZ differential modulator sweep at low SOA", 2));
                        // Range = 0 to 2 x Vcm, fixed step size
                        double Vcm = (initMz.Mz.LeftArmMod_Quad_V + initMz.Mz.RightArmMod_Quad_V) / 2;
                        int NumberOfSteps = (int)(1 + Math.Abs(Vcm * 2 / (mzConfig.MZInitSweepStepSize_mV / 1000)));

                        // Retry if necessary
                        MzAnalysisWrapper.MzAnalysisResults mzDiffModResultsAtLowSOA;
                        do
                        {
                            mzSweepFailed = false;
                            do
                            {
                                mzDiffData = ilmzUtils.ModArm_DifferentialSweepByTrigger(newMzBiases.Mz.LeftArmImb_mA / 1000, newMzBiases.Mz.RightArmImb_mA / 1000,
                                Vcm * 2, 0.0, NumberOfSteps,MZSourceMeasureDelay_ms, mzConfig.MZTapBias_V, mzConfig.MZTapBias_V, ArmSoureByAsic);

                                // display it
                                engine.SendToGui(mzDiffData);

                                // If overrange run again
                                if (MzAnalysisWrapper.CheckForOverrange(mzDiffData.SweepData[ILMZSweepDataType.FibrePower_mW]))
                                {
                                    dataOk = false;
                                    mzInstrs.PowerMeter.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_mW;
                                    //mzInstrs.PowerMeter.Range = InstType_OpticalPowerMeter.AutoRange;
                                    mzInstrs.PowerMeter.Range = mzInstrs.PowerMeter.Range * 10;
                                    if (double.IsNaN(mzInstrs.PowerMeter.Range))
                                    {
                                        mzInstrs.PowerMeter.Range = 10;
                                    }
                                }
                                else
                                {
                                    // if underrange fix the data and continue
                                    dataOk = true;
                                    if (MzAnalysisWrapper.CheckForUnderrange(mzDiffData.SweepData[ILMZSweepDataType.FibrePower_mW]))
                                    {
                                        mzDiffData.SweepData[ILMZSweepDataType.FibrePower_mW] =
                                            MzAnalysisWrapper.FixUnderRangeData(mzDiffData.SweepData[ILMZSweepDataType.FibrePower_mW]
                                            , minLevel_dBm);
                                    }
                                }
                            }
                            while (!dataOk);

                            if (!ArmSoureByAsic)
                            {
                                ilmzUtils.CleanupAfterSweep();
                            }

                            // Analyse LV data

                            if (this.isUseNegSlopeImb)
                            {
                                mzDiffModResultsAtLowSOA = MzAnalysisWrapper.Differential_NegChirp_DifferentialSweep(mzDiffData, Vcm, 0);
                            }
                            else
                            {
                                mzDiffModResultsAtLowSOA = MzAnalysisWrapper.ZeroChirp_DifferentialSweep(mzDiffData, Vcm, 0);
                            }

                            // Check that the data looked ok.
                            if (Math.Abs(mzDiffModResultsAtLowSOA.PowerAtMax_dBm) > 200)
                            {
                                // The power meter has probably over-ranged. Increase the range and try again.
                                engine.SendToGui(new GuiTextMessage("Retry MZ differential modulator sweep at higher power range", 3));
                                //mzInstrs.PowerMeter.Range = InstType_OpticalPowerMeter.AutoRange;
                                opmRange = mzInstrs.PowerMeter.Range;
                                if (opmRange < 1)
                                {
                                    mzInstrs.PowerMeter.Range = opmRange * 10;
                                    mzSweepFailed = true;
                                }
                            }
                        } while (mzSweepFailed == true);

                        MzData lowSoaMzBiases = new MzData();
                        lowSoaMzBiases.LeftArmMod_Min_V = mzDiffModResultsAtLowSOA.Min_SrcL;
                        lowSoaMzBiases.LeftArmMod_Peak_V = mzDiffModResultsAtLowSOA.Max_SrcL;
                        lowSoaMzBiases.RightArmMod_Min_V = mzDiffModResultsAtLowSOA.Min_SrcR;
                        lowSoaMzBiases.RightArmMod_Peak_V = mzDiffModResultsAtLowSOA.Max_SrcR;
                        lowSoaMzBiases.LeftArmImb_mA = newMzBiases.Mz.LeftArmImb_mA;
                        lowSoaMzBiases.RightArmImb_mA = newMzBiases.Mz.RightArmImb_mA;

                        NPlot.PointD marker = new NPlot.PointD(mzDiffModResultsAtLowSOA.Imb_SrcL - mzDiffModResultsAtLowSOA.Imb_SrcR, mzDiffModResultsAtLowSOA.PowerAtMax_dBm - 3);
                        NPlot.TextItem markerText = new NPlot.TextItem(new NPlot.PointD(marker.X, mzDiffModResultsAtLowSOA.PowerAtMin_dBm + 5), "ER = " + mzDiffModResultsAtLowSOA.ExtinctionRatio_dB.ToString("0.0"));
                        engine.SendToGui(markerText);

                        // Set MZ to trough & measure
                        IlMzDriverUtils.SetupMzToTrough(mzInstrs.FCU2Asic, lowSoaMzBiases);
                        double fibrePowerTroughLowSOA_dBm = Measurements.ReadOpticalPower(OpticalPowerHead.MZHead, Measurements.PowerUnits.dBm);
                        IlMzDriverUtils.SetupMzToPeak(mzInstrs.FCU2Asic, lowSoaMzBiases);

                        double fibrePowerPeakLowSOA_dBm = Measurements.ReadOpticalPower(OpticalPowerHead.MZHead, Measurements.PowerUnits.dBm);

                        // store data
                        ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.LowSOAMzDcEr_dB, fibrePowerPeakLowSOA_dBm - fibrePowerTroughLowSOA_dBm);
                        ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.LowSOAFibrePwrPeak_dBm, fibrePowerPeakLowSOA_dBm);
                        ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.LowSOAFibrePwrMin_dBm, fibrePowerTroughLowSOA_dBm);

                        // store plots
                        string lowSoaLVFileName = Util_GenerateFileName.GenWithTimestamp(mzFileDirectory,
                            "LowSOADiffMzLV_" + chanFreq_GHz.ToString() + "_" + ilmzChan.MidTempData.Priority.ToString(), dutSerialNbr, "csv");
                        // write to file
                        MzSweepFileWriter.WriteSweepData(lowSoaLVFileName, mzDiffData,
                            new ILMZSweepDataType[] { ILMZSweepDataType.LeftArmModBias_V,
                                    ILMZSweepDataType.RightArmModBias_V,
                                    ILMZSweepDataType.FibrePower_mW,
                                    ILMZSweepDataType.TapComplementary_mA});
                        // Add to archive
                        zipFile.AddFileToZip(lowSoaLVFileName);

                        // All done. Return to peak power with correct SOA current.
                        IlMzDriverUtils.SetupMzToPeak(mzInstrs.FCU2Asic, newMzBiases.Mz);
                        DsdbrUtils.SetSectionCurrent_mA(DSDBRSection.SOA, levelledSoaCurrent_mA);
                        Measurements.MzHead.Range = levelledPowerMeterRange;
                        System.Threading.Thread.Sleep(3000);
                    }
                    #endregion

                #region MZ SingleEnded LV

                //ilmzUtils.SetCurrentSenseRange(SourceMeter.LeftModArm, mzConfig.MZCurrentCompliance_A);
                //ilmzUtils.SetCurrentSenseRange(SourceMeter.RightModArm, mzConfig.MZCurrentCompliance_A);

                if (testSelect.IsTestSelected(chanFreq_GHz, "MZ SingleEnded LV"))
                {
                    // Same as init mz sweep
                    double fixedModBias_V = mzConfig.MZInitFixedModBias_volt;       // 0V
                    double imbalance_A = mzConfig.MzCtrlINominal_mA / 1000;         // +4.5mA
                    double sweepStop_V = 0;
                    double sweepStart_V = -Math.Abs(mzConfig.MZInitSweepMaxAbs_V);   // -8V
                    int nbrPoints = (int)(1 + Math.Abs(sweepStart_V - sweepStop_V) / (mzConfig.MZInitSweepStepSize_mV / 1000));
                    double tapBias_V = mzConfig.MZTapBias_V;                        // 2V

                    mzInstrs.PowerMeter.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_mW;

                    engine.SendToGui(new GuiTextMessage("Running LeftArm sweep", 2));

                    ILMZSweepResult sweepDataLeft = ilmzUtils.LeftModArm_SingleEndedSweep(
                        fixedModBias_V, 
                        imbalance_A, 
                        imbalance_A, 
                        sweepStart_V,
                        sweepStop_V, 
                        nbrPoints, 
                        tapBias_V, 
                        tapBias_V, 
						ArmSoureByAsic);

                    engine.SendToGui(sweepDataLeft);

                    engine.SendToGui(new GuiTextMessage("Running RightArm sweep", 2));
                    ILMZSweepResult sweepDataRight = ilmzUtils.RightModArm_SingleEndedSweep(
                        fixedModBias_V, 
                        imbalance_A, 
                        imbalance_A, 
                        sweepStart_V,
                        sweepStop_V, 
                        nbrPoints, 
                        tapBias_V, 
                        tapBias_V, 
						ArmSoureByAsic);

                    engine.SendToGui(sweepDataRight);

                    // No analysis(?) Just return plots.
                    string joinedFileName = Util_GenerateFileName.GenWithTimestamp(mzFileDirectory,
                        "FinalSEMzLV", dutSerialNbr, "csv");

                    // write joined sweep to file
                    MzSweepFileWriter.WriteStitchedSingleEndLVData(joinedFileName,
                        sweepDataLeft, sweepDataRight,
                        new ILMZSweepDataType[] { ILMZSweepDataType.FibrePower_mW,
                                      ILMZSweepDataType.LeftArmModBias_mA, 
                                      ILMZSweepDataType.TapComplementary_mA});

                    // Add to archive
                    zipFile.AddFileToZip(joinedFileName);


                    // Reset MZ to peak when done.
                    IlMzDriverUtils.SetupMzToPeak(mzInstrs, newMzBiases.Mz);
                }

                #endregion

                #region Device measurements

                // Device, Power & TEC measurements
                engine.SendToGui(new GuiTextMessage("Device parameter measurements", 2));


                Measurements.PowerDissResults pdr = Measurements.ElecPowerDissipation(
                  initMz.Dsdbr.Setup.FrontPair, dsdbrTec, mzTec);
                
                // Alice.Huang    2010-02-10  
                // Comment all voltage on this section

                //tcmzChan.MidTempData.SetValueDouble(EnumTosaParam.VSoa_V, pdr.SOA_V);
                ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.ISoa_mA, pdr.SOA_A * 1000);
                //tcmzChan.MidTempData.SetValueDouble(EnumTosaParam.VGain_V, pdr.Gain_V);
                ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.IGain_mA, pdr.Gain_A * 1000);
                //tcmzChan.MidTempData.SetValueDouble(EnumTosaParam.VRear_V, pdr.Rear_V);
                ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.IRear_mA, pdr.Rear_A * 1000);
                //tcmzChan.MidTempData.SetValueDouble(EnumTosaParam.VPhase_V, pdr.Phase_V);
                ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.IPhaseITU_mA, pdr.Phase_A * 1000);
                //tcmzChan.MidTempData.SetValueDouble(EnumTosaParam.VFsConst_V, pdr.FrontFirst_V);
                ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.IFsConst_mA, pdr.FrontFirst_A * 1000);
                //tcmzChan.MidTempData.SetValueDouble(EnumTosaParam.VFsNonConst_V, pdr.FrontSecond_V);
                ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.IFsNonConst_mA, pdr.FrontSecond_A * 1000);
                ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.IRearSoa_mA, pdr.RearSOA_A * 1000);

                if (DsdbrUtils.DsdbrDriverInstrsToUse == DsdbrDriveInstruments .FCU2AsicInstrument )
                {
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.ISoa_Dac, (double)DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic.mAToDacForFrontSoa_Asic((float)(pdr.SOA_A * 1000)));

                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.IGain_Dac, (double)DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic.mAToDacForGain_Asic((float)(pdr.Gain_A * 1000)));

                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.IRear_Dac, (double)DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic.mAToDacForRear_Asic((float)(pdr.Rear_A * 1000)));

                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.IPhaseITU_Dac, (double)DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic.mAToDacForPhase_Asic((float)(pdr.Phase_A * 1000)));

                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.IFsConst_Dac, (double)DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic.mAToDacForFront_Asic((float)(pdr.FrontFirst_A * 1000)));

                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.IFsNonConst_Dac, (double)DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic.mAToDacForFront_Asic((float)(pdr.FrontSecond_A * 1000)));

                }
                ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.VccTotalCurrent_mA, pdr.LaserTotalCurrent_A * 1000);
                ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.VccPowerDiss_W, pdr.LaserPower_W);
                ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.PackagePowerDiss_W, pdr.PackagePower_W);
                ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.ITecDsdbr_mA, pdr.TecDsdbr_A * 1000);
                ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.VTecDsdbr_V, pdr.TecDsdbr_V);
                ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.RthDsdbr_ohm, dsdbrTec.SensorResistanceActual_ohm);
                ////tcmzChan.MidTempData.SetValueDouble(EnumTosaParam.ITecMz_mA, pdr.TecMz_A * 1000);
                ////tcmzChan.MidTempData.SetValueDouble(EnumTosaParam.VTecMz_V, pdr.TecMz_V);
                //System.Threading.Thread.Sleep(1000);

               
                //tcmzChan.MidTempData.SetValueDouble(EnumTosaParam.RthMz_ohm, mzTec.SensorResistanceActual_ohm);

                #endregion

                // Switch to OSA
                //switchOsaMzOpm.SetState(Switch_Osa_MzOpm.State.Osa);

                #region SMSR

                if (testSelect.IsTestSelected(chanFreq_GHz, "SMSR"))
                {
                    engine.SendToGui(new GuiTextMessage("Measuring SMSR", 2));

                    double smsr = SMSRMeasurement.MeasureSMSR(chanFreq_GHz);

                    bool debugging = System.Diagnostics.Debugger.IsAttached;

                    if (smsr < 10 && debugging)
                        System.Diagnostics.Debugger.Break();

                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.SMSR_dB, smsr);

                    #region Try next channel option if fail

                    if (ilmzChan.PassStatus == PassFail.Fail)
                    {
                        engine.SendToGui(new GuiTextMessage("Channel option test stopped, test failed for : " + ilmzChan.MidTempData.FailedParamNames, 1));

                        if (alwaysTuneLaser)
                        {
                            ilmzChan.MidTempData.SetValueBool(EnumTosaParam.Pass_Flag, false);
                            ilmzChan.MidTempData.SetFinished();
                            continue;
                        }
                    }
                    #endregion
                }

                #endregion

                DateTime superModeSRTestStartTime = DateTime.Now;

                #region Supermode SR

                if (testSelect.IsTestSelected(chanFreq_GHz, "Supermode SR"))
                {
                    engine.SendToGui(new GuiTextMessage("Measuring Supermode SR", 2));

                    double spmsr = SupermodeSRMeasurement.MeasureSupermodeSR(chanFreq_GHz);
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.SupermodeSR_dB, spmsr);
                }
                // Try next channel option if fail
                if (alwaysTuneLaser && ilmzChan.PassStatus == PassFail.Fail)
                {
                    engine.SendToGui(new GuiTextMessage("Channel option test stopped, unable to tune channel", 1));
                    ilmzChan.MidTempData.SetValueBool(EnumTosaParam.Pass_Flag, false);
                    ilmzChan.MidTempData.SetFinished();
                    continue;
                }
                if (ilmzChan.PassStatus == PassFail.Fail)
                    engine.SendToGui(new GuiTextMessage("Forcing test to continue despite failing for : " + ilmzChan.MidTempData.FailedParamNames, 1));
                
                #endregion

                superModeSRTestTime = (DateTime.Now - superModeSRTestStartTime).TotalSeconds;

                if (this.isTestEOL)
                {
                    #region Write test time results to local file - chongjian.liang 2013.4.20

                    using (StreamWriter swriter = new StreamWriter(eolSMSRTestTimeDropFilePath, true))
                    {
                        swriter.Write(readLockerRatioTime + ",");
                        swriter.Write(readWaveMeterTime + ",");
                        swriter.Write(eolSMSRTestTime + ",");
                        swriter.Write(modeACQTestTime + ",");
                        swriter.WriteLine(superModeSRTestTime);
                    }
                    #endregion 
                }

                #region Locker

                // Locker test
               
                if (testSelect.IsTestSelected(chanFreq_GHz, "Locker Test"))
                {
                    engine.SendToGui(new GuiTextMessage("Locker measurements", 2));
                    DsdbrUtils.opticalSwitch = Optical_switch;
                    DsdbrUtils.LockerCurrents lc = DsdbrUtils.ReadLockerCurrents();
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.ItxLock_mA, lc.TxCurrent_mA);
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.IrxLock_mA, lc.RxCurrent_mA);
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.LockRatio, lc.LockRatio);
                }

                #endregion

                #region Locker Slope
                double lockslopeEff=0;
                if (ttrLockerSlopeChans.IsTTRLockerSlopeChannel(chanFreq_GHz))
                {
                    engine.SendToGui(new GuiTextMessage("Measuring Locker slope", 2));

                    DatumList lockerSlopeResults = null;
                    {
                        int LOOP_MAX_COUNT = 5;
                        int loopCount = 0;
                        while (loopCount++ < LOOP_MAX_COUNT) // Add loop to avoid unstable DUT TX & RX reading - chongjian.liang 2016.11.03
                        {
                            if (configData.IsPresent("LockerSlope_FreqOffset_GHz"))
                            {
                                double freqOffset_Ghz = configData.ReadDouble("LockerSlope_FreqOffset_GHz");
                                lockerSlopeResults = LockerSlope.MeasureLockerSlope(freqOffset_Ghz);
                            }
                            else
                            {
                                Measurements.FrequencyWithoutMeter = chanFreq_GHz;
                                lockerSlopeResults = LockerSlope.MeasureLockerSlope();
                            }

                            double lockerSlopeEff = Math.Abs(lockerSlopeResults.ReadDouble("LockerSlopeEff"));

                            if (lockerSlopeEff >= ilmzChan.MidTempData.GetMeasuredDataLowLimit(EnumTosaParam.LockerSlopeEffAbs)
                                && lockerSlopeEff <= ilmzChan.MidTempData.GetMeasuredDataHighLimit(EnumTosaParam.LockerSlopeEffAbs))
                            {
                                break;
                            }
                        }
                    }

                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.LowIphaseForLockerSlope_mA,
                       lockerSlopeResults.ReadDouble("LowIphaseForLockerSlope"));
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.HighIphaseForLockerSlope_mA,
                        lockerSlopeResults.ReadDouble("HighIphaseForLockerSlope"));
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.LowFreqForLockerSlope_GHz,
                        lockerSlopeResults.ReadDouble("LowFreqForLockerSlope"));
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.HighFreqForLockerSlope_GHz,
                        lockerSlopeResults.ReadDouble("HighFreqForLockerSlope"));

					lockslopeEff=lockerSlopeResults.ReadDouble("LockerSlopeEff");
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.LockerSlopeEff,
                        lockerSlopeResults.ReadDouble("LockerSlopeEff"));

                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.LockerSlopeEffAbs,
                        Math.Abs(lockerSlopeResults.ReadDouble("LockerSlopeEff")));
                    
                    // No need to record the MZ_CSR_POWER_RATIO, per Stream's request - chongjian.liang 2013.5.10
                    //ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.MZ_CSR_POWER_RATIO,
                    //    lockerSlopeResults.ReadDouble("MZ_CSR_POWER_RATIO"));

                    // Don't record PhaseTuningEff - PTE_2GHz here, use the value - PTE_6GHz from ITU and EOL tuning instead
                    //tcmzChan.MidTempData.SetValueDouble(EnumTosaParam.PhaseTuningEff, lockerSlopeResults.ReadDouble("PhaseTuningEff"));
                    //Echo new added the followed one line. because we calculate this PTE in itu tuning phase is wrong
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.PhaseTuningEff, lockerSlopeResults.ReadDouble("PhaseTuningEff"));
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.PhaseRatioSlopeEff, lockerSlopeResults.ReadDouble("PhaseRatioSlopeEff"));
                    if (!ilmzChan.MidTempData.IsTested(EnumTosaParam.TuningOk))
                    {
                        ilmzChan.MidTempData.SetValueBool(EnumTosaParam.TuningOk, lockerSlopeResults.ReadBool("TuneOk"));
                    }
                    ilmzChan.EOL_Testing_Info = ilmzChan.EOL_Testing_Info + lockerSlopeResults.ReadString("EOL_Testing_Info");
                    ilmzChan.ITU3GHzOffset_Info = lockerSlopeResults.ReadString("ITU3GHzOffset_Info");
                }

                // Locker slope should be positive in 50GHz, negative in 100GHz according to the current locker slope formula - chongjian.liang 2013.6.11
                if ((int)chanFreq_GHz % 100 == 0)
                {
                    if (lockslopeEff > 0)
                    {
                        engine.SendStatusMsg("Invalid lock slope sign, force to stop testing");
                        ForceStopTesting = true;
                        FailAbortReason = "Invalid lock slope sign";
                        return;
                    }
                }
                else
                {
                    if (lockslopeEff < 0)
                    {
                        engine.SendStatusMsg("Invalid lock slope sign, force to stop testing");
                        ForceStopTesting = true;
                        FailAbortReason = "Invalid lock slope sign";
                        return;
                    }
                }

                #endregion

                //switchOsaMzOpm.SetState(Switch_Osa_MzOpm.State.MzOpm);

                #region SOA Control Range
                // SOA Control Range test
                if (!soaControlRange_PostTest)
                {
                    // Check if do soa control range test, and get the test mode
                    int ituChannelIndex = ilmzChan.MidTempData.GetValueSint32(EnumTosaParam.ItuChannelIndex);
                    bool doTest = false;
                    SoaControlRangeTestMode testMode = SoaControlRangeTestMode.Sweep;
                    foreach (SoaControlRangeChan chanToTest in soaControlRangeTestChannels)
                    {
                        if (chanToTest.chanIndex == ituChannelIndex)
                        {
                            doTest = true;
                            testMode = chanToTest.testMode;
                            break;
                        }
                    }

                    // Do soa control range test
                    if (doTest)
                    {
                        engine.SendToGui(new GuiTextMessage("SOA Control Range", 2));
                        Measurements.MzHead.Range = InstType_OpticalPowerMeter.AutoRange;
						if (!ArmSoureByAsic)
                        {
                        // Reset measurement range to allow for high photocurrent in arms
                        ilmzUtils.SetCurrentSenseRange(SourceMeter.LeftModArm, mzConfig.MZCurrentCompliance_A);
                        ilmzUtils.SetCurrentSenseRange(SourceMeter.RightModArm, mzConfig.MZCurrentCompliance_A);
						}

                        if (testMode == SoaControlRangeTestMode.Sweep)
                        {
                            string plotFileQualifier = chanFreq_GHz.ToString() + "_" + 
                                ilmzChan.MidTempData.Priority.ToString() + "_" + dutSerialNbr;
                            
                            DatumList soaSwpResults = SoaSweep.SweepAtSingleChannel(
                                ilmzChannelChar_ZD_configObj.SoaSweep_MinCurrent_mA,
                                ilmzChannelChar_ZD_configObj.SoaSweep_MaxCurrent_mA,
                                ilmzChannelChar_ZD_configObj.SoaSweep_RefCurrent_mA, 
                                ilmzChannelChar_ZD_configObj.SoaSweep_Stepdelay_ms,
                                mzFileDirectory, plotFileQualifier);

                            ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.FibrePwrISoaMin_dBm, 
                                soaSwpResults.ReadDouble("SOA_MinPwr_dBm"));

                            ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.FibrePwrISoaMax_dBm,
                                soaSwpResults.ReadDouble("SOA_MaxPwr_dBm"));

                            string plotFile = soaSwpResults.ReadString("SOA_sweep_plot");
                            zipFile.AddFileToZip(plotFile);

                            double[] xData = soaSwpResults.ReadDoubleArray("SOA_mA");
                            double[] yData = soaSwpResults.ReadDoubleArray("FibrePower_dBm");
                            GuiPlotData plotData = new GuiPlotData(xData, yData, "SOA Control Range", "Optical Power (dBm)");
                            engine.SendToGui(plotData);
                        }
                        else if (testMode == SoaControlRangeTestMode.MeasureAtMaxMinSoa)
                        {
                            Measurements.FrequencyWithoutMeter = chanFreq_GHz;
                            DatumList soaMeasResults = SoaSweep.MeasurePowerAtMinAndMaxSoa(
                                ilmzChannelChar_ZD_configObj.SoaSweep_MinCurrent_mA,
                                ilmzChannelChar_ZD_configObj.SoaSweep_MaxCurrent_mA,
                                ilmzChannelChar_ZD_configObj.SoaSweep_Stepdelay_ms);
                            ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.FibrePwrISoaMin_dBm,
                                soaMeasResults.ReadDouble("SOA_MinPwr_dBm"));

                            ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.FibrePwrISoaMax_dBm, 
                                soaMeasResults.ReadDouble("SOA_MaxPwr_dBm"));
                        }
                        else
                        {
                            double soaMeasResult =SoaSweep.MeasurePowerAtMaxSoa(
                                ilmzChannelChar_ZD_configObj.SoaSweep_MaxCurrent_mA, 
                                ilmzChannelChar_ZD_configObj.SoaSweep_Stepdelay_ms);

                            ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.FibrePwrISoaMax_dBm, soaMeasResult);                           
                        }

                    }
                }
                #endregion

                #region SOA Shutter

                // SOA shutter test
                if (testSelect.IsTestSelected(chanFreq_GHz, "SOA Shutter"))
                {
                    engine.SendToGui(new GuiTextMessage("SOA shutter test", 2));
                    //mzInstrs.PowerMeter.Range = InstType_OpticalPowerMeter.AutoRange;//jack.zhang 2010/07/22 add for SOA shutter
                    //DatumList soaShutterResults =
                    //    SoaShutterMeasurement.MeasureAtSingleChannel_New(
                    //    ilmzChannelChar_ZD_configObj.SoaShutter_MaxCurrent_mA, 
                    //    ilmzChannelChar_ZD_configObj.SoaShutter_MaxVoltage_V,
                    //    ilmzChannelChar_ZD_configObj.SoaShutter_MaxSoaElectricalPower_mW,
                    //    ilmzChannelChar_ZD_configObj.SoaShutter_VoltageComplianceOffset_V, 
                    //    PowerHead);

                    DatumList soaShutterResults =
                        SoaShutterMeasurement.MeasureAtSingleChannel_New(
                        ilmzChannelChar_ZD_configObj.SoaShutter_MaxCurrent_mA,
                        ilmzChannelChar_ZD_configObj.SoaShutter_MaxSoaElectricalPower_mW,
                        DsdbrUtils.Fcu2AsicInstrumentGroup,PowerHead);

                    //tcmzChan.MidTempData.SetValueDouble(EnumTosaParam.VSoaShutter_V, soaShutterResults.ReadDouble("VsoaShutter"));
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.ISoaShutter_mA, soaShutterResults.ReadDouble("IsoaShutter"));
                    //tcmzChan.MidTempData.SetValueDouble(EnumTosaParam.SoaPwrShuttered_mW, soaShutterResults.ReadDouble("SoaPwrShuttered"));
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.FibrePwrShuttered_dBm, soaShutterResults.ReadDouble("ShutteredPower_dBm"));
                    //tcmzChan.MidTempData.SetValueDouble(EnumTosaParam.ItxShuttered_mA, soaShutterResults.ReadDouble("ItxShuttered"));
                    //tcmzChan.MidTempData.SetValueDouble(EnumTosaParam.IrxShuttered_mA, soaShutterResults.ReadDouble("IrxShuttered"));
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.SoaShutteredAtten_dB,
                        ilmzChan.MidTempData.GetValueDouble(EnumTosaParam.FibrePwrPeak_dBm) - soaShutterResults.ReadDouble("ShutteredPower_dBm"));
                }

                #endregion

                // Stop testing this channel when one option passes
                if (ilmzChan.PassStatus == PassFail.Pass)
                {
                    engine.SendToGui(new GuiTextMessage("Channel option passed !", 1));
                    chanPass = true;
                    numChanPass++;
                    ilmzChan.MidTempData.SetValueBool(EnumTosaParam.Pass_Flag, true);
                    ilmzChan.MidTempData.SetFinished();
                    break;
                }
                else
                {
                    #region If channel option failed

                    string failedParamNames = "";
                    string[] mzParamsNames ={ "CH_TAP", "CH_MZ", "VPI", "DCER", "FIBREPWRPEAK_DBM" };
                    int mzParamsFailCount = 0;
                    foreach (EnumTosaParam tp in ilmzChan.MidTempData.FailedParams) 
                    {
                        string failStr=tp.ToString();

                        failedParamNames += failStr + "; ";
                        for (int k = 0; k < mzParamsNames.Length; k++)
                        {
                            if (failStr.ToUpper().Contains(mzParamsNames[k]))
                            {
                                mzParamsFailCount++;
                                break;
                            }
                        }
                    }
                    if (mzParamsFailCount == ilmzChan.MidTempData.FailedParams.Length)
                    {
                        StringBuilder failMsg = new StringBuilder();
                        failMsg.Append("Channel option test failed for: ");
                        ilmzChan.MidTempData.SetValueBool(EnumTosaParam.Pass_Flag, false);

                        string[] tmpStrArray = failedParamNames.Split(';');

                        foreach (string tmpStr in tmpStrArray)
                        {
                            try
                            {
                                EnumTosaParam tmpEnum = (EnumTosaParam)Enum.Parse(typeof(EnumTosaParam), tmpStr.Trim());

                                Datum failDatum = ilmzChan.MidTempData.GetMeasuredData(tmpEnum);
                                string failValStr = failDatum.ValueToString();

                                failMsg.Append(tmpStr + " : ");
                                failMsg.Append(failValStr + ", ");
                            }
                            catch { }
                        }
                        
                        engine.SendToGui(failMsg.ToString());
                        ilmzChan.MidTempData.SetFinished();
                        break;
                    }

                    StringBuilder failMsg1 = new StringBuilder();
                    failMsg1.Append("Channel option test failed for: ");
                    ilmzChan.MidTempData.SetValueBool(EnumTosaParam.Pass_Flag, false);

                    string[] tmpStrArray1 = failedParamNames.Split(';');

                    foreach (string tmpStr1 in tmpStrArray1)
                    {
                        try
                        {
                            EnumTosaParam tmpEnum = (EnumTosaParam)Enum.Parse(typeof(EnumTosaParam), tmpStr1.Trim());

                            Datum failDatum = ilmzChan.MidTempData.GetMeasuredData(tmpEnum);
                            string failValStr = failDatum.ValueToString();

                            failMsg1.Append(tmpStr1 + " : ");
                            failMsg1.Append(failValStr + ", ");
                        }
                        catch { }
                    }

                    engine.SendToGui(failMsg1.ToString());
 
                    ilmzChan.MidTempData.SetFinished();
                    continue;
                    #endregion
                } 

            }   // foreach channel option

            //if (Ch_DC_ER != 0)
            //ER_List.Add(Ch_DC_ER);  //add the DCER value into the list
            // START MODIFICATION: By Ken.Wu 2007.09.25
            // Break testing while certain number of channels failed on power level
            if (thisFreqChans != null && thisFreqChans.Length != 0 && 
                powerLevelingFailedInstanceCount >= thisFreqChans.Length)
            {
                powerLevelingFailedCount++;

                string prompt = string.Format("Power level failed ( COUNT:{0} )", powerLevelingFailedCount);
                engine.SendToGui(new GuiTextMessage(prompt, 1));
            }
            // END MODIFICATION: By Ken.Wu 2007.09.25

            // Metrics
            if (beingTested)
            {
                numChanTested++;
            }

            #region Check if this channel can be used to interpolate other channels
            TTR_LVChannel = false;
            if (!chanPass && TTR_LVChannel)
            {
                // This channel can't be used to interpolate other channels,
                // so add next channel into ttrLvSweepChans.ChannelsToTestActually list
                if (chanFreq_GHz == freqLow_GHz)
                {
                    double nextChanFreq_GHz = chanFreq_GHz + freqSpace_GHz;
                    // Check if this channel is already in the list
                    if (!ttrLvSweepChans.IsToTestActually(nextChanFreq_GHz))
                    {
                        ttrLvSweepChans.ChannelsToTestActually.Add(nextChanFreq_GHz);
                    }
                }
                else if (chanFreq_GHz == freqHigh_GHz)
                {
                    double nextChanFreq_GHz = chanFreq_GHz - freqSpace_GHz;
                    // Check if this channel is already in the list
                    if (!ttrLvSweepChans.IsToTestActually(nextChanFreq_GHz))
                    {
                        ttrLvSweepChans.ChannelsToTestActually.Add(nextChanFreq_GHz);
                    }
                }
                else
                {
                    double nextLowerChanFreq_GHz = chanFreq_GHz - freqSpace_GHz;
                    double nextHigherChanFreq_GHz = chanFreq_GHz + freqSpace_GHz;
                    // Check if this channel is already in the list
                    if (!ttrLvSweepChans.IsToTestActually(nextLowerChanFreq_GHz))
                    {
                        ttrLvSweepChans.ChannelsToTestActually.Add(nextLowerChanFreq_GHz);
                    }
                    // Check if this channel is already in the list
                    if (!ttrLvSweepChans.IsToTestActually(nextHigherChanFreq_GHz))
                    {
                        ttrLvSweepChans.ChannelsToTestActually.Add(nextHigherChanFreq_GHz);
                    }
                }
            }
            #endregion

            #region Abort the test if any ITU fails. - chongjian.liang 2013.9.2

            if (ilmzChan != null && ilmzChan.MidTempData.OverallStatus == PassFail.Fail && !testEvenITUFail)
            {
                engine.SendStatusMsg("All alternative channels of ITU '" + chanFreq_GHz + "' fails, quit and fail the test.");

                ForceStopTesting = true;
                FailAbortReason = "All alternative channels of ITU '" + chanFreq_GHz + "' fails.";
            }
            #endregion
        }

        private void LoadERdynamicLimitArray(string filename)
        {
            CsvReader csvReader = new CsvReader();
            List<string[]> ER_Limit_List = csvReader.ReadFile(filename);
            for (int i = 1; i < ER_Limit_List.Count; i++)
            {
                lower_List_X.Add(Convert.ToInt32(ER_Limit_List[i][0]));
                lower_List_Y.Add(Convert.ToDouble(ER_Limit_List[i][1]));
                upper_List_X.Add(Convert.ToInt32(ER_Limit_List[i][0]));
                upper_List_Y.Add(Convert.ToDouble(ER_Limit_List[i][2]));
            }
        }
        /// <summary>
        /// Interpolate a single paramete across TCMZ channels
        /// </summary>
        /// <param name="tcmzParam">The parameter name</param>
        /// <param name="ituFreq_GHz">The channel at which to interpolate</param>
        /// <returns>The interpolated value</returns>
        private double interpolateParam(ITestEngine engine, EnumTosaParam tcmzParam, double ituFreq_GHz)
        {
            double chanFreq_GHz = ituFreq_GHz;
            int chanIndex = ilmzItuChannels.GetChannelsAtItu(chanFreq_GHz)[0].MidTempData.GetValueSint32(EnumTosaParam.ItuChannelIndex);
            int firstPassedLowerChanIndex = -1;
            ILMZChannel firstPassedLowerChan = null;
            int firstPassedHigherChanIndex = -1;
            ILMZChannel firstPassedHigherChan = null;

            // Search the first passed lower channel in ttrLvSweepChans.ChannelsToTestActually list
            double tempFreq_GHz = chanFreq_GHz - freqSpace_GHz;
            while (tempFreq_GHz >= freqLow_GHz)
            {
                if (ttrLvSweepChans.IsToTestActually(tempFreq_GHz))
                {
                    ILMZChannel[] tcmzChanList = ilmzItuChannels.GetChannelsAtItu(tempFreq_GHz);
                    foreach (ILMZChannel tcmzChan in tcmzChanList)
                    {
                        if (tcmzChan.PassStatus == PassFail.Pass)
                        {
                            firstPassedLowerChan = tcmzChan;
                            firstPassedLowerChanIndex = tcmzChan.MidTempData.GetValueSint32(EnumTosaParam.ItuChannelIndex);
                            break;
                        }
                    }

                    if (firstPassedLowerChanIndex != -1)
                        break;
                }
                tempFreq_GHz -= freqSpace_GHz;
            }

            // Search the first passed higher channel in ttrLvSweepChans.ChannelsToTestActually list
            tempFreq_GHz = chanFreq_GHz + freqSpace_GHz;
            while (tempFreq_GHz <= freqHigh_GHz)
            {
                if (ttrLvSweepChans.IsToTestActually(tempFreq_GHz))
                {
                    ILMZChannel[] tcmzChanList = ilmzItuChannels.GetChannelsAtItu(tempFreq_GHz);
                    foreach (ILMZChannel tcmzChan in tcmzChanList)
                    {
                        if (tcmzChan.PassStatus == PassFail.Pass)
                        {
                            firstPassedHigherChan = tcmzChan;
                            firstPassedHigherChanIndex = tcmzChan.MidTempData.GetValueSint32(EnumTosaParam.ItuChannelIndex);
                            break;
                        }
                    }

                    if (firstPassedHigherChanIndex != -1)
                    {
                        break;
                    }
                }
                tempFreq_GHz += freqSpace_GHz;
            }

            if (firstPassedHigherChanIndex == -1 || firstPassedLowerChanIndex == -1)
            {
                this.failModeCheck.RaiseError_Mod_NonParamFail(engine, "Can't find the lower or higher channel needed for linear interpolation.", FailModeCategory_Enum.UN);
            }

            // Interpolate data
            double[] paramVal = new double[2];
            paramVal[0] = firstPassedLowerChan.MidTempData.GetValueDouble(tcmzParam);
            paramVal[1] = firstPassedHigherChan.MidTempData.GetValueDouble(tcmzParam);
            if (paramVal[0] - paramVal[1] != 0)
            {
                double chanDistance = firstPassedLowerChanIndex - firstPassedHigherChanIndex;
                double gradient = chanDistance / (paramVal[0] - paramVal[1]);
                return EstimateXAtY.Calculate(paramVal[0], firstPassedLowerChanIndex, gradient, chanIndex);
            }
            else
            {
                return paramVal[0];
            }
        }

        /// <summary>
        /// Config device to a channel and wait until the temperature is stabilised.
        /// </summary>
        /// <param name="chan">Tcmz channel</param>
        private void configIlmzToChannel(ILMZChannel chan)
        {
            // power up laser and mz
            // configure device to channel
            int triedCount = 0;
            bool setupOk = true;
            do
            {
                triedCount++;
                try
                {
                    DsdbrChannelSetup dsdbrChanSetup = new DsdbrChannelSetup();
                    dsdbrChanSetup.IGain_mA = chan.MidTempData.GetValueDouble(EnumTosaParam.IGain_mA);
                    dsdbrChanSetup.ISoa_mA = chan.MidTempData.GetValueDouble(EnumTosaParam.ISoa_mA);
                    dsdbrChanSetup.IRear_mA = chan.MidTempData.GetValueDouble(EnumTosaParam.IRear_mA);
                    dsdbrChanSetup.IPhase_mA = chan.MidTempData.GetValueDouble(EnumTosaParam.IPhaseITU_mA);
                    dsdbrChanSetup.FrontPair = chan.MidTempData.GetValueSint32(EnumTosaParam.FrontSectionPair);
                    dsdbrChanSetup.IFsFirst_mA = chan.MidTempData.GetValueDouble(EnumTosaParam.IFsConst_mA);
                    dsdbrChanSetup.IFsSecond_mA = chan.MidTempData.GetValueDouble(EnumTosaParam.IFsNonConst_mA);

                    // Alice.Huang    2010-03-10
                    double iRearSoa = 0.0;
                    try
                    {
                        iRearSoa = chan.MidTempData.GetValueDouble(EnumTosaParam.IRearSoa_mA);
                    }
                    catch
                    { }
                    dsdbrChanSetup.IRearSoa_mA = iRearSoa;

                    DsdbrUtils.SetDsdbrCurrents_mA(dsdbrChanSetup);

                    // Set MZ biases
                    MzData mzData = new MzData();
                    mzData.LeftArmImb_mA = chan.MidTempData.GetValueDouble(EnumTosaParam.MzLeftArmImb_mA);
                    mzData.RightArmImb_mA = chan.MidTempData.GetValueDouble(EnumTosaParam.MzRightArmImb_mA);

                    if (mzInstrs.ImbsSourceType  == IlMzInstruments.EnumSourceType.Asic  )
                    {
                        mzData.LeftArmImb_Dac = (int)(mzData.LeftArmImb_mA * mzInstrs.FCU2Asic.SectionsDacCalibration.ImbLeftDac_CalFactor +
                            mzInstrs.FCU2Asic.SectionsDacCalibration.ImbLeftDac_CalOffset);
                        mzData.RightArmImb_Dac = (int)(mzData.RightArmImb_mA * mzInstrs.FCU2Asic.SectionsDacCalibration.ImbRightDac_CalFactor +
                            mzInstrs.FCU2Asic.SectionsDacCalibration.ImbRightDac_CalOffset);
                        
                    }

                    mzData.LeftArmMod_Peak_V = chan.MidTempData.GetValueDouble(EnumTosaParam.MzLeftArmModPeak_V);
                    mzData.RightArmMod_Peak_V = chan.MidTempData.GetValueDouble(EnumTosaParam.MzRightArmModPeak_V);
                    mzData.LeftArmMod_Quad_V = chan.MidTempData.GetValueDouble(EnumTosaParam.MzLeftArmModQuad_V);
                    mzData.RightArmMod_Quad_V = chan.MidTempData.GetValueDouble(EnumTosaParam.MzRightArmModQuad_V);
                    mzData.LeftArmMod_Min_V = chan.MidTempData.GetValueDouble(EnumTosaParam.MzLeftArmModMinima_V);
                    mzData.RightArmMod_Min_V = chan.MidTempData.GetValueDouble(EnumTosaParam.MzRightArmModMinima_V);
                    if (!ArmSoureByAsic)
                    {
                        IlMzDriverUtils.SetupMzToPeak(mzInstrs, mzData);
                    }
                    else
                    {
                        IlMzDriverUtils.SetupMzToPeak(mzInstrs.FCU2Asic, mzData);
                    }
                }
                catch (Exception e)
                {
                    setupOk = false;
                    if (triedCount >= numSetupIlmzTries)
                        throw e;
                }
            } while (!setupOk);

            // wait for temperature stabilisation
            System.Threading.Thread.Sleep(rthDelay_mS);
            triedCount = 0;
            do
            {
                System.Threading.Thread.Sleep(rthDelay_mS);
                //double mzRth_ohm = mzTec.SensorResistanceActual_ohm;
                //mzRth_ohm >= chan.MidTempData.GetMeasuredDataLowLimit(EnumTosaParam.RthMz_ohm) &&
                    //mzRth_ohm <= chan.MidTempData.GetMeasuredDataHighLimit(EnumTosaParam.RthMz_ohm) &&
                double dsdbrRth_ohm = dsdbrTec.SensorResistanceActual_ohm;

                if (dsdbrRth_ohm >= chan.MidTempData.GetMeasuredDataLowLimit(EnumTosaParam.RthDsdbr_ohm) &&
                    dsdbrRth_ohm <= chan.MidTempData.GetMeasuredDataHighLimit(EnumTosaParam.RthDsdbr_ohm))
                {
                    break;
                }
            } while (++triedCount <= numRthTries);
        }

        public void DoRearSoATuningAddRX(IlMzInstruments mzInstrs, double maxRearSOA, double minRearSOA, double tagetRxCurrent, double stepSOAcurrent)
        {
            DsdbrUtils.LockerCurrents lc;
            double prerearSOA = mzInstrs.FCU2Asic.IRearSoa_mA;
            double tempRearSOA = 0.0;
            lc = DsdbrUtils.ReadLockerCurrents();
            tempRearSOA = prerearSOA;
            while (lc.RxCurrent_mA <= tagetRxCurrent)
            {
                tempRearSOA += stepSOAcurrent;
                tempRearSOA = Math.Max(minRearSOA, tempRearSOA);
                tempRearSOA = Math.Min(maxRearSOA, tempRearSOA);
                mzInstrs.FCU2Asic.IRearSoa_mA = 0;
                System.Threading.Thread.Sleep(100);
                mzInstrs.FCU2Asic.IRearSoa_mA = (float)tempRearSOA;
                System.Threading.Thread.Sleep(100);
                lc = DsdbrUtils.ReadLockerCurrents();

                if (tempRearSOA == maxRearSOA)
                {
                    break;
                }
            }

        }

        /// <summary>
        /// Get the unique file to calculate IphaseModAcq by LM and SM from the files collection
        /// </summary>
        /// <param name="filesCollection"></param>
        /// <param name="decompressedFileFolder"></param>
        /// <param name="lm"></param>
        /// <param name="sm"></param>
        /// <returns></returns>
        private string GetFileToCalPhaseModeAcq(string filesCollection, string decompressedFileFolder, short lm, short sm)
        {
            if (!phaseModAcqFilesUnzip)
            {
                Util_UnZipFile unzipFile = new Util_UnZipFile(filesCollection);

                unzipFile.UnZipFile(decompressedFileFolder);
                phaseModAcqFilesUnzip = true;
            }

            string[] files = Directory.GetFiles(decompressedFileFolder, string.Format("ImiddleLower_LM{0}_*SM{1}.csv", lm, sm));

            if (files.Length == 0)
            {
                throw new FileNotFoundException(string.Format("Can't find file \"ImiddleLower_LM{0}*SM{1}.csv\"", lm, sm));
            }

            return files[0];
        }

        #endregion


        public void DoRearSoATuning(IlMzInstruments mzInstrs, double maxRearSOA, double minRearSOA, double tagetTxCurrent, double stepSOAcurrent)
        {
            DsdbrUtils.LockerCurrents lc;
            double prerearSOA = mzInstrs.FCU2Asic.IRearSoa_mA;
            double tempRearSOA = 0.0;
            lc = DsdbrUtils.ReadLockerCurrents();
            tempRearSOA = prerearSOA;
            while (lc.TxCurrent_mA <= tagetTxCurrent)
            {
                tempRearSOA += stepSOAcurrent;
                tempRearSOA = Math.Max(minRearSOA, tempRearSOA);
                tempRearSOA = Math.Min(maxRearSOA, tempRearSOA);
                mzInstrs.FCU2Asic.IRearSoa_mA = 0;
                System.Threading.Thread.Sleep(100);
                mzInstrs.FCU2Asic.IRearSoa_mA = (float)tempRearSOA;
                System.Threading.Thread.Sleep(100);
                lc = DsdbrUtils.ReadLockerCurrents();

                if (tempRearSOA == maxRearSOA)
                {
                    break;
                }
            }

        }
        public void DoRearSoATuningRX(IlMzInstruments mzInstrs, double maxRearSOA, double minRearSOA, double tagetRxCurrent, double stepSOAcurrent)
        {
            DsdbrUtils.LockerCurrents lc;
            double prerearSOA = mzInstrs.FCU2Asic.IRearSoa_mA;
            double tempRearSOA = 0.0;
            lc = DsdbrUtils.ReadLockerCurrents();
            tempRearSOA = prerearSOA;
            while (lc.RxCurrent_mA >= tagetRxCurrent)
            {
                tempRearSOA -= stepSOAcurrent;
                tempRearSOA = Math.Max(minRearSOA, tempRearSOA);
                tempRearSOA = Math.Min(maxRearSOA, tempRearSOA);
                mzInstrs.FCU2Asic.IRearSoa_mA = 0;
                System.Threading.Thread.Sleep(100);
                mzInstrs.FCU2Asic.IRearSoa_mA = (float)tempRearSOA;
                System.Threading.Thread.Sleep(100);
                lc = DsdbrUtils.ReadLockerCurrents();

                if (tempRearSOA == maxRearSOA)
                {
                    break;
                }
            }
        }

        public void RearSoaTuningTxRx(DatumList configData, ref IlMzInstruments mzInstrs, ref ILMZChannel ilmzChan)//, double maxRearSOA, double minRearSOA, double tagetTxCurrent, double stepSOAcurrent)
        {
            double itx_min = configData.ReadDouble("ItxMINValue");
            double irx_min = configData.ReadDouble("IrxMINValue");
            double itx_max = configData.ReadDouble("ItxMAXValue");
            double irx_max = configData.ReadDouble("IrxMAXValue");

            if (1 == 1)
            {
                DsdbrUtils.LockerCurrents lc;
                lc = DsdbrUtils.ReadLockerCurrents();
                if (lc.TxCurrent_mA < itx_min)
                {
                    double max_reaSOA = configData.ReadDouble("RearSOAMax");
                    double min_reaSOA = configData.ReadDouble("RearSOAMin");

                    double stepSOAcurrent = 0.2;
                    double targetTxCurrent = itx_min + 0.02;     //need read from configuration file
                    DoRearSoATuning(mzInstrs, max_reaSOA, min_reaSOA, targetTxCurrent, stepSOAcurrent);


                    lc = DsdbrUtils.ReadLockerCurrents();
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.ItxLock_mA, lc.TxCurrent_mA);//TOSATempParam.PwrCtrlMinItx_mA, lc.TxCurrent_mA);
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.IrxLock_mA, lc.RxCurrent_mA);//TOSATempParam.PwrCtrlMinIrx_mA, lc.RxCurrent_mA);
                }
                else if (lc.RxCurrent_mA < irx_min)
                {
                    double max_reaSOA = configData.ReadDouble("RearSOAMax");
                    double min_reaSOA = configData.ReadDouble("RearSOAMin");

                    double stepSOAcurrent = 0.2;
                    double targetRxCurrent = irx_min + 0.02;     //need read from configuration file
                    DoRearSoATuningAddRX(mzInstrs, max_reaSOA, min_reaSOA, targetRxCurrent, stepSOAcurrent);


                    lc = DsdbrUtils.ReadLockerCurrents();
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.ItxLock_mA, lc.TxCurrent_mA);//TOSATempParam.PwrCtrlMinItx_mA, lc.TxCurrent_mA);
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.IrxLock_mA, lc.RxCurrent_mA);//TOSATempParam.PwrCtrlMinIrx_mA, lc.RxCurrent_mA);
                }
                else if (lc.RxCurrent_mA > irx_max)// (lc.RxCurrent_mA > irx_max)
                {
                    double max_reaSOA = configData.ReadDouble("RearSOAMax");
                    double min_reaSOA = configData.ReadDouble("RearSOAMin");

                    double stepSOAcurrent = 0.2;
                    double targetRxCurrent = irx_max - 0.02;// -0.01;     //need read from configuration file
                    DoRearSoATuningRX(mzInstrs, max_reaSOA, min_reaSOA, targetRxCurrent, stepSOAcurrent);


                    lc = DsdbrUtils.ReadLockerCurrents();
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.ItxLock_mA, lc.TxCurrent_mA);//TOSATempParam.PwrCtrlMinItx_mA, lc.TxCurrent_mA);
                    ilmzChan.MidTempData.SetValueDouble(EnumTosaParam.IrxLock_mA, lc.RxCurrent_mA);//TOSATempParam.PwrCtrlMinIrx_mA, lc.RxCurrent_mA);
                }
            }

        }
        //
        public void DoRearSoATuning(ref IlMzInstruments mzInstrs, double Delta_Irearsoa)
        {

            double Irearsoa_mA = mzInstrs.FCU2Asic.IRearSoa_mA;// Math.Round(fcu.IRearSoa_mA, 0);//

            double Irearsoa_mA_new = Irearsoa_mA + Delta_Irearsoa;
            mzInstrs.FCU2Asic.IRearSoa_mA = Irearsoa_mA_new;
        }


        #region private Data

        private Specification mainSpec;
        private bool isUseNegSlopeImb = false;

        private bool testEvenITUFail = false;

        private string decompressedFileFolder;
        private bool isTestEOL;
        private bool isTestModeACQ;
        private bool isTestRippleOfEOL;

        //private double dTestRippleRes;
        const int numRthTries = 10;
        const int rthDelay_mS = 500;

        const int numSetupIlmzTries = 3;


        private double maxRipple = 0.0;
        // Ilmz channels
        IlmzChannels ilmzItuChannels;

        // Configs

        double CH_LASER_RTH_Min;
        double CH_LASER_RTH_Max;

        bool useLowCostSolution;

        string phaseScanSequenceFilePath;
        string eolPhaseScanDropFilePath;

        double ripple_Min;
        double ripple_Max;
        int afterJumpIgnoreCount;
        int beforeJumpIgnoreCount;
        double multiple;
        string multipleFilterString;

        TcmzChannelChar_ZD_Config ilmzChannelChar_ZD_configObj;
        TcmzMzSweep_Config mzConfig;
        // Local data
        double freqLow_GHz;
        double freqHigh_GHz;
        double freqSpace_GHz;
        double targetPowerDbm;
        double calOffset_GHz;
        string dutSerialNbr;
        double opmRange;
        uint powerLevelingBreakCount;
        uint powerLevelingFailedCount;
        bool alwaysTuneLaser;
        double mzImbMinLimit_mA;
        double mzImbMaxLimit_mA;
        double maxArmBias_v;
        string IPhaseModAcqFilesCollection;
        double tune_OpticalPowerTolerance_mW;
        string mzFileDirectory;
        string eolSMSRTestTimeDropFilePath;
        double iPhaseOfMeritDegrade_mA;

        // Instruments
        IlMzInstruments mzInstrs;
        IInstType_OpticalPowerMeter PowerHead;
        IInstType_OSA Osa = null;
        IInstType_TecController dsdbrTec;
        IInstType_TecController mzTec = null;
        IInstType_DigiIOCollection Optical_switch;
        IInstType_DigitalIO OutLineCtap;

        // Utils
        IlMzDriverUtils ilmzUtils;
        Util_ZipFile zipFile;
        TestSelection testSelect;
        Switch_Osa_MzOpm switchOsaMzOpm;

        // Selected channel tests
        bool soaControlRange_PostTest = false;
        SoaControlRangeChan[] soaControlRangeTestChannels;
        TTRLVSweepChannels ttrLvSweepChans;
        TTRLockerSlopeChannels ttrLockerSlopeChans;

        bool phaseModAcqFilesUnzip = false;

        // Alice.Huang    2010-05-07
        // add this variant to avoind const soa limit in power leveling 
        // while use the ch_isoa_ma for flexible.
        double maxSoaForPwrLeveling;
        int MZSourceMeasureDelay_ms =5;
        //bool SelectTestFlag = false;
        bool ArmSoureByAsic = true; //bias arm or imb arm were sourced by Asic
        bool ForceStopTesting = false;
        string FailAbortReason = "";
        //ER dynamic limit
        List<int> upper_List_X = new List<int>();
        List<double> upper_List_Y = new List<double>();
        List<int> lower_List_X = new List<int>();
        List<double> lower_List_Y = new List<double>();
        //List<double> ER_List = new List<double>();

        Dictionary<double, double> dynamicER_LowLimits;
        Dictionary<double, double> dynamicER_HighLimits;
        FailModeCheck failModeCheck;
        #endregion
    }
}


// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// TcmzPowerHeadCal.cs
//
// Author: Mark Fullalove, 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestSolution.IlmzCommonUtils;
using Bookham.TestSolution.IlmzCommonData;
using Bookham.TestLibrary.Algorithms;
using Bookham.Toolkit.CloseGrid;//jack.Zhang
using System.IO;
using Bookham.TestLibrary.Utilities;
using DSDBR;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Test Module Template.
    /// </summary>
    public class HittSPCPowerHeadCal : ITestModule
    {
        #region ITestModule Members
        /// <summary>
        /// 
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="userType"></param>
        /// <param name="configData"></param>
        /// <param name="instruments"></param>
        /// <param name="chassis"></param>
        /// <param name="calData"></param>
        /// <param name="previousTestData"></param>
        /// <returns></returns>
        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType,
            DatumList configData, InstrumentCollection instruments, 
            ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {
            bool isCalPXIT = configData.IsPresent("IsCalPXIT") ?
                configData.ReadBool("IsCalPXIT") : true;
            

            // Get instruments.
            this.OpmReference = (IInstType_OpticalPowerMeter)instruments["OpmReference"];
            this.OpmMZ = (IInstType_TriggeredOpticalPowerMeter)instruments["OpmMZ"];
            OpmReference.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_mW;
            OpmMZ.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_mW;
            //this.Wavemeter = (IInstType_Wavemeter)instruments["Wavemeter"];

            // Get configs.
            this.plotFileDirectory = configData.ReadString("PlotFileDirectory");
            //int numberOfSupermodes = configData.ReadSint32("NUM_SM");
            int delay_ms = configData.ReadSint32("OpticalCal_delay_ms");
            double mzCalFactorMax = configData.ReadDouble("OpticalCal_MZ_Max");
            double mzCalFactorMin = configData.ReadDouble("OpticalCal_MZ_Min");

            this.SN = configData.ReadString("SN");
            // Initialise GUI
            engine.GuiShow();
            engine.GuiToFront();

            // Get list of channels
            List<DsdbrChannelSetup> channelList = new List<DsdbrChannelSetup>();


            BaseLineData[] baseLineChannels = (BaseLineData[])configData.ReadReference("BaseLineChannels"); ;
            
            //DsdbrChannelData[] channelDataALL = objchanData as DsdbrChannelData[];
            for (int i = 0; i<baseLineChannels.Length;i++)
            {
                DsdbrChannelData dsdbrChanData = baseLineChannels[i].Dsdbr;
                channelList.Add(dsdbrChanData.Setup);
                freqArrayFromMap[i] = dsdbrChanData.ItuFreq_GHz;//Echo new added, because no wavemeter on test kit , so we need guess freq with reference freq/
            }

            bool isSuccessful = true;

            // Do power head calibration
            ButtonId retryResponse = ButtonId.No;
            do
            {
                retryResponse = ButtonId.No;

                RunCalRoutineWithoutPXIT(engine, delay_ms, channelList.ToArray());
                
                double mzCalFactor = OpticalPowerCal.GetCorrectionFactor_dBm(OpticalPowerHead.MZHead, 193650);

                // Release the power cali check - chongjian.liang 2016.1.19
                if (mzCalFactor > mzCalFactorMax || mzCalFactor < mzCalFactorMin)
                {
                    string retryMessage = String.Format("Calibration looks incorrect ( MZ = {0}). \nDo you want to try again ?", mzCalFactor.ToString("0.0"));
                    DateTime start = DateTime.Now;
                    retryResponse = engine.ShowYesNoUserQuery(retryMessage);
                    DateTime end = DateTime.Now;
                    TimeSpan span = end.Subtract(start);
                    labourTime += span.TotalMinutes;

                    if (retryResponse == ButtonId.No)
                    {
                        isSuccessful = false;
                    }
                }
               
            } while (retryResponse == ButtonId.Yes);

            // return data
            DatumList returnData = new DatumList();
            returnData.AddDouble("LabourTime", labourTime);
            returnData.AddBool("IsSuccessful", isSuccessful);
            returnData.AddDoubleArray("PowerOffsetArray", DeltaPowerArray_simple);
            return returnData;
        }

        /*
        private void RunCalRoutineWithNoPXIT(ITestEngine engine, int delay_ms, DsdbrChannelData[] channelData)
        {
            double[] refFrequencies = new double[channelData.Length];
            double[] mzPowers = new double[channelData.Length];
            double[] mzPowers_dBm = new double[channelData.Length];
            double[] refPowers = new double[channelData.Length];
            double[] refPowers_dBm = new double[channelData.Length];

            //get all frequencies first.
            for (int i = 0; i < channelData.Length; i++)
            {
                DsdbrChannelData channel = channelData[i];
                DsdbrUtils.SetDsdbrCurrents_mA(channel.Setup);

                System.Threading.Thread.Sleep(delay_ms);
                refFrequencies[i] = Wavemeter.Frequency_GHz;
                engine.SendToGui("Measuring wavelength at channel " + (i + 1).ToString() + ":" + refFrequencies[i].ToString());
            }

            //switch to external power head.
            DsdbrUtils.EnableDsdbr(false);
            DateTime start = DateTime.Now;
            engine.ShowContinueUserQuery("Please move the fibre to the external power head");
            DateTime end = DateTime.Now;
            TimeSpan span = end.Subtract(start);
            labourTime += span.TotalMinutes;//Record the labour time

            for (int i = 0; i < channelData.Length; i++)
            {
                DsdbrChannelData channel = channelData[i];
                DsdbrUtils.SetDsdbrCurrents_mA(channel.Setup);

                System.Threading.Thread.Sleep(delay_ms * 3);

                double wavelength_nm = Alg_FreqWlConvert.Freq_GHz_TO_Wave_nm(refFrequencies[i]);
                OpmReference.Wavelength_nm = wavelength_nm;

                double power_mW = OpmReference.ReadPower();
                refPowers[i] = power_mW;
                engine.SendToGui("Measuring reference power at channel " + (i + 1).ToString() + ":" + refFrequencies[i].ToString());
            }

            DsdbrUtils.EnableDsdbr(false);
            start = DateTime.Now;
            engine.ShowContinueUserQuery("Please move the fibre to the patch cord");
            end = DateTime.Now;
            span = end.Subtract(start);
            labourTime += span.TotalMinutes;//Record the labour time

            OpmMZ.Range = InstType_OpticalPowerMeter.AutoRange;
            for (int i = 0; i < channelData.Length; i++)
            {
                DsdbrChannelData channel = channelData[i];
                DsdbrUtils.SetDsdbrCurrents_mA(channel.Setup);

                System.Threading.Thread.Sleep(delay_ms * 3);

                double wavelength_nm = Alg_FreqWlConvert.Freq_GHz_TO_Wave_nm(refFrequencies[i]);
                OpmMZ.Wavelength_nm = wavelength_nm;

                double power_mW = OpmMZ.ReadPower();
                engine.SendToGui("Measuring MZ power at channel " + (i + 1).ToString() + ":" + refFrequencies[i].ToString());

                mzPowers[i] = power_mW;
            }

            for (int i = 0; i < channelData.Length; i++)
            {
                refPowers_dBm[i] = Alg_PowConvert_dB.Convert_mWtodBm(refPowers[i]);
                mzPowers_dBm[i] = Alg_PowConvert_dB.Convert_mWtodBm(mzPowers[i]);
            }

            int orderOfFit = 2;
            OpticalPowerCal.Initialise(orderOfFit);            
            
            string plotFileName = Util_GenerateFileName.GenWithTimestamp(plotFileDirectory, "CalPlot", "", "csv");
            using (StreamWriter writer = new StreamWriter(plotFileName))
            {
                writer.WriteLine("Frequency,Reference power dBm,MZ power dBm");

                // For each channel
                for (int ii = 0; ii < channelData.Length; ii++)
                {
                    OpticalPowerCal.AddCalibratedPoint(OpticalPowerHead.MZHead, refFrequencies[ii],
                        refPowers_dBm[ii] - mzPowers_dBm[ii]);

                    writer.WriteLine(refFrequencies[ii] + "," + refPowers_dBm[ii] + "," + mzPowers_dBm[ii] );
                }

                writer.Close();
            }
        }
          */
        /// <summary>
        /// 
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="delay_ms"></param>
        /// <param name="channelList"></param>
        private void RunCalRoutineWithoutPXIT(ITestEngine engine, int delay_ms, DsdbrChannelSetup[] channelList)
        {
            // Set up arrays for powers
            double[] referencePower = new double[channelList.Length];
            double[] referenceFrequency = new double[channelList.Length];
            double[] rawMZ_mW = new double[channelList.Length];

            // Init channel setup
            DsdbrChannelSetup dsdbrCurrents;

            // For each channel
            int i = 0;
            //double powerForRange = double.MinValue;
            foreach (DsdbrChannelSetup channel in channelList)
            {
                //   Set to channel
                dsdbrCurrents = channel;
                DsdbrUtils.SetDsdbrCurrents_mA(dsdbrCurrents);

                engine.SendToGui("Measuring wavelength at channel " + (i + 1).ToString());
                System.Threading.Thread.Sleep(delay_ms);
                
                //   Measure freq + store to array;
                referenceFrequency[i] = freqArrayFromMap[i];

                //// Note the peak power
                //double powerNow = OpmCgDirect.ReadPower();
                //powerForRange = Math.Max(powerForRange, powerNow);

                i++;
            }

            //OpmMZ.Range = powerForRange;

            // Fibre to cal head
            DsdbrUtils.EnableDsdbr(false);
            DateTime start = DateTime.Now;
            engine.ShowContinueUserQuery("Please move the fibre to the external power head");
            DateTime end = DateTime.Now;
            TimeSpan span = end.Subtract(start);
            labourTime += span.TotalMinutes;//Record the labour time

            // For each channel
            i = 0;
            //OpmMZ.EnableInputTrigger(false)
            foreach (DsdbrChannelSetup channel in channelList)
            {
                //   Set to channel
                dsdbrCurrents = channel;
                DsdbrUtils.SetDsdbrCurrents_mA(dsdbrCurrents);

                // Allow DSDBR to settle after powering back on
                if (i < 1)
                    System.Threading.Thread.Sleep(delay_ms * 2);

                double wavelength_nm = Alg_FreqWlConvert.Freq_GHz_TO_Wave_nm(referenceFrequency[i]);
                engine.SendToGui("Measuring reference power at " + referenceFrequency[i].ToString("F") + " GHz , " + wavelength_nm.ToString("F") + " nm");
                System.Threading.Thread.Sleep(delay_ms);

                // Apply WL to OPM
                OpmReference.Wavelength_nm = wavelength_nm;
                double power_mW = OpmReference.ReadPower();
                referencePower[i++] = Alg_PowConvert_dB.Convert_mWtodBm(power_mW);
            }

            // Fibre to switch
            DsdbrUtils.EnableDsdbr(false);
            start = DateTime.Now;
            engine.ShowContinueUserQuery("Please move the fibre to the patch cord");
            end = DateTime.Now;
            span = end.Subtract(start);
            labourTime += span.TotalMinutes;//Record the labour time
            OpmMZ.EnableInputTrigger(false);
            OpmMZ.EnableOutputTrigger(false);
            // Measure power via patch cord.

            // Need to do this because the loss will have changed.
            // It may be possible to measure across the frequency range while reading from the wavemeter
            // and simply take a spot reading to apply the ( non-frequency dependent ) change
            // in loss resulting from the last fibre move.
           
            // For each channel
            i = 0;
            foreach (DsdbrChannelSetup channel in channelList)
            {
                //   Set to channel
                dsdbrCurrents = channel;
                DsdbrUtils.SetDsdbrCurrents_mA(dsdbrCurrents);

                // Allow DSDBR to settle after powering back on
                if (i < 1)
                    System.Threading.Thread.Sleep(delay_ms * 2);

                // Apply WL to OPM
                double wavelength_nm = Alg_FreqWlConvert.Freq_GHz_TO_Wave_nm(referenceFrequency[i]);
                engine.SendToGui("Measuring uncalibrated powers at " + referenceFrequency[i].ToString("F") + " GHz , " + wavelength_nm.ToString("F") + " nm");
                System.Threading.Thread.Sleep(delay_ms);

                // Set WL on each OPM
                OpmMZ.Wavelength_nm = Alg_FreqWlConvert.Freq_GHz_TO_Wave_nm(referenceFrequency[i]);

                // Measure power from each OPM
                double power_mw = OpmMZ.ReadPower();
                rawMZ_mW[i] = Alg_PowConvert_dB.Convert_mWtodBm(power_mw);
                i++;
            }
            DeltaPowerArray_simple = new double[rawMZ_mW.Length];
            for (int k = 0; k < rawMZ_mW.Length; k++)
            {
                DeltaPowerArray_simple[k] = referencePower[k] - rawMZ_mW[k];
            }

            string currrentFileName = @"Configuration\PowerOffset_dBm.csv";
            ReadColumnDataFromCSV reader = new ReadColumnDataFromCSV(currrentFileName);
            reader.readFile(ref WaveLenghtArray, ref DeltaPowerArray);
            ButtonId retryResponse = ButtonId.No;
            double Delta_power = double.MinValue;
            int n = 0;
            double offset = double.MinValue;

            // record power offset max value
            double offset_max = double.MinValue;
            string offset_string = "";
            string Delta_power_string = "";
            string wavelengh_nm_string = "";


            for (; n < channelList.Length; n++)
            {
                double wavelength_nm = Alg_FreqWlConvert.Freq_GHz_TO_Wave_nm(referenceFrequency[n]);
                offset = referencePower[n] - rawMZ_mW[n];
                for (int ii = 0; ii < DeltaPowerArray.Length; ii++)
                {
                    if ((wavelength_nm >= DeltaPowerArray[ii]) && (wavelength_nm < DeltaPowerArray[ii + 1]))
                    {
                        Delta_power = (WaveLenghtArray[ii] + WaveLenghtArray[ii + 1]) / 2;
                        break;
                    }
                }
                double Delta_offset = Math.Abs(Delta_power - offset);

                // record power offset max value  ***** jack add 20090820

                if ((Delta_offset > 0.15) && (offset > offset_max))
                {
                    offset_max = offset;
                    offset_string = offset_max.ToString();
                    Delta_power_string = Delta_power.ToString();
                    wavelengh_nm_string = wavelength_nm.ToString();
                }


                //if (Delta_offset < Powercal_offset)
                //{
                //    PowerCalOK = true;
                //    retryResponse = ButtonId.Yes;
                //        "请清洁光纤头和POWER HEAD.\n重   试?", Delta_offset, Powercal_offset,wavelength_nm,offset,referencePower[n],rawCgDirect_mW[n]);
                //    retryResponse = engine.ShowYesNoUserQuery(prompt);
                //    this.Comments += "offset=" + Delta_offset.ToString() + ">" + Powercal_offset.ToString() + "&&" + "PowerCalCount=" + PowerCalCount.ToString();
                //    break;
                //}

            }

            string path = @"C:\Poweroffset_record.csv";
            using (StreamWriter writer = new StreamWriter(path, true))
            {
                writer.Write(this.SN + "," + DateTime.Now.ToLocalTime().ToString("yyyyMMddHHmmss") +
                    "," + offset_string + "," + Delta_power_string + "," +
                    wavelengh_nm_string + "," + System.Environment.MachineName.ToString());
                writer.WriteLine();
            }



            // END modification for self-calibration
            // Setup power cal class. Perform a second order fit. 
            // Put this in config if required, but 2nd order should be ok for C + L band.
            int orderOfFit = 2;
            OpticalPowerCal.Initialise(orderOfFit);

            if (!Directory.Exists(plotFileDirectory))
            {
                Directory.CreateDirectory(plotFileDirectory);
            }

            string plotFileName = Util_GenerateFileName.GenWithTimestamp(plotFileDirectory, "CalPlot", "", "csv");
            using (StreamWriter writer = new StreamWriter(plotFileName))
            {
                writer.WriteLine("Frequency,Reference power dBm,MZ power dBm");

                // For each channel
                for (int ii = 0; ii < channelList.Length; ii++)
                {
                    OpticalPowerCal.AddCalibratedPoint(OpticalPowerHead.MZHead, referenceFrequency[ii],
                        referencePower[ii] - rawMZ_mW[ii]);

                    writer.WriteLine(referenceFrequency[ii] + "," + referencePower[ii] + "," + rawMZ_mW[ii]);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="numberOfSupermodes"></param>
        /// <param name="delay_ms"></param>
        /// <param name="channelList"></param>
        private void RunCalRoutine(ITestEngine engine, int numberOfSupermodes, int delay_ms, DsdbrChannelSetup[] channelList)
        {
            // Set up arrays for powers
            double[] referencePower = new double[channelList.Length];
            double[] referenceFrequency = new double[channelList.Length];
            double[] rawCgDirect_mW = new double[channelList.Length];
            double[] rawCgFiltered_mW = new double[channelList.Length];
            double[] rawMZ_mW = new double[channelList.Length];

            // Init channel setup
            DsdbrChannelSetup dsdbrCurrents;

            // For each channel
            int i = 0;
            double powerForRange = double.MinValue;
            foreach (DsdbrChannelSetup channel in channelList)
            {
                //   Set to channel
                dsdbrCurrents = channel;
                DsdbrUtils.SetDsdbrCurrents_mA(dsdbrCurrents);

                engine.SendToGui("Measuring wavelength at channel " + (i + 1).ToString());
                System.Threading.Thread.Sleep(delay_ms);

                //   Measure freq + store to array;
                referenceFrequency[i] = Wavemeter.Frequency_GHz;

                // Note the peak power
                double powerNow = OpmCgDirect.ReadPower();
                powerForRange = Math.Max(powerForRange, powerNow);

                i++;
            }

            OpmMZ.Range = powerForRange;

            // Fibre to cal head
            DsdbrUtils.EnableDsdbr(false);
            DateTime start = DateTime.Now;
            engine.ShowContinueUserQuery("Please move the fibre to the external power head");
            DateTime end = DateTime.Now;
            TimeSpan span = end.Subtract(start);
            labourTime += span.TotalMinutes;//Record the labour time

            // For each channel
            i = 0;
            foreach (DsdbrChannelSetup channel in channelList)
            {
                //   Set to channel
                dsdbrCurrents = channel;
                DsdbrUtils.SetDsdbrCurrents_mA(dsdbrCurrents);

                // Allow DSDBR to settle after powering back on
                if (i < 1)
                    System.Threading.Thread.Sleep(delay_ms * 2);

                double wavelength_nm = Alg_FreqWlConvert.Freq_GHz_TO_Wave_nm(referenceFrequency[i]);
                engine.SendToGui("Measuring reference power at " + referenceFrequency[i].ToString("F") + 
                                        " GHz , " + wavelength_nm.ToString("F") + " nm");
                System.Threading.Thread.Sleep(delay_ms);

                // Apply WL to OPM
                OpmReference.Wavelength_nm = wavelength_nm;
                double power_mW = OpmReference.ReadPower();
                referencePower[i++] = Alg_PowConvert_dB.Convert_mWtodBm(power_mW);
            }

            // Fibre to switch
            DsdbrUtils.EnableDsdbr(false);
            start = DateTime.Now;
            engine.ShowContinueUserQuery("Please move the fibre to the patch cord");
            end = DateTime.Now;
            span = end.Subtract(start);
            labourTime += span.TotalMinutes;//Record the labour time

            // Measure power via patch cord.

            // Need to do this because the loss will have changed.
            // It may be possible to measure across the frequency range while reading from the wavemeter
            // and simply take a spot reading to apply the ( non-frequency dependent ) change
            // in loss resulting from the last fibre move.

            // For each channel
            i = 0;
            foreach (DsdbrChannelSetup channel in channelList)
            {
                //   Set to channel
                dsdbrCurrents = channel;
                DsdbrUtils.SetDsdbrCurrents_mA(dsdbrCurrents);

                // Allow DSDBR to settle after powering back on
                if (i < 1)
                    System.Threading.Thread.Sleep(delay_ms * 2);

                // Apply WL to OPM
                double wavelength_nm = Alg_FreqWlConvert.Freq_GHz_TO_Wave_nm(referenceFrequency[i]);
                engine.SendToGui("Measuring uncalibrated powers at " + 
                    referenceFrequency[i].ToString("F") + " GHz , " + wavelength_nm.ToString("F") + " nm");
                System.Threading.Thread.Sleep(delay_ms);

                // Set WL on each OPM
                OpmCgDirect.Wavelength_nm = Alg_FreqWlConvert.Freq_GHz_TO_Wave_nm(referenceFrequency[i]);
                OpmCgFilter.Wavelength_nm = Alg_FreqWlConvert.Freq_GHz_TO_Wave_nm(referenceFrequency[i]);
                OpmMZ.Wavelength_nm = Alg_FreqWlConvert.Freq_GHz_TO_Wave_nm(referenceFrequency[i]);

                //   Measure powers + store to cal arrays;
                double power_mw = OpmCgDirect.ReadPower();
                if (double.IsNaN(power_mw) ||
                    double.IsInfinity(power_mw))
                {
                    //TODO: Reset powermeter.
                }
                rawCgDirect_mW[i] = Alg_PowConvert_dB.Convert_mWtodBm(power_mw);

                power_mw = OpmCgFilter.ReadPower();
                rawCgFiltered_mW[i] = Alg_PowConvert_dB.Convert_mWtodBm(power_mw);

                power_mw = OpmMZ.ReadPower();
                rawMZ_mW[i] = Alg_PowConvert_dB.Convert_mWtodBm(power_mw);
                i++;
            }

            // Setup power cal class. Perform a second order fit. 
            // Put this in config if required, but 2nd order should be ok for C + L band.
            // There's not enough data for the fit if there are fewer supermodes than the order of fit so check.
            int orderOfFit = 2;
            if (orderOfFit > numberOfSupermodes)
                orderOfFit = numberOfSupermodes;
            OpticalPowerCal.Initialise(orderOfFit);

            string plotFileName = Util_GenerateFileName.GenWithTimestamp(plotFileDirectory, "CalPlot", "", "csv");
            using (StreamWriter writer = new StreamWriter(plotFileName))
            {
                writer.WriteLine("Frequency,Reference power dBm,MZ power dBm,CG direct power dBm, CG filtered power dBm");

                // For each channel
                for (int ii = 0; ii < channelList.Length; ii++)
                {
                    OpticalPowerCal.AddCalibratedPoint(OpticalPowerHead.OpmCgDirect, referenceFrequency[ii],
                        referencePower[ii] - rawCgDirect_mW[ii]);

                    OpticalPowerCal.AddCalibratedPoint(OpticalPowerHead.OpmCgFiltered, referenceFrequency[ii],
                        referencePower[ii] - rawCgFiltered_mW[ii]);

                    OpticalPowerCal.AddCalibratedPoint(OpticalPowerHead.MZHead, referenceFrequency[ii],
                        referencePower[ii] - rawMZ_mW[ii]);

                    writer.WriteLine(referenceFrequency[ii] + "," + referencePower[ii] + "," + rawMZ_mW[ii] + "," + rawCgDirect_mW[ii] + "," + rawCgFiltered_mW[ii]);
                }
            }
        }

        private IInstType_OpticalPowerMeter OpmCgDirect;
        private IInstType_OpticalPowerMeter OpmCgFilter;
        private IInstType_OpticalPowerMeter OpmReference;
        private IInstType_TriggeredOpticalPowerMeter OpmMZ;
        private IInstType_Wavemeter Wavemeter;
        private Double labourTime = 0;
        private string plotFileDirectory;

        //private int PowerCalCount = 0;
        //private bool PowerCalOK = false;
        //private double Powercal_offset = double.MinValue;
        //private string Comments = "";
        private string SN = "";
        private double[] DeltaPowerArray;
        private double[] WaveLenghtArray;
        private double[] DeltaPowerArray_simple;
        private double[] freqArrayFromMap=new double[3];
        /// <summary>
        /// 
        /// </summary>
        public Type UserControl
        {
            get { return (typeof(HittSPCPowerHeadCalGui)); }
        }

        #endregion
    }
}

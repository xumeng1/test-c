using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.Framework.InternalData;
using System.IO;
using System.Collections;

namespace DSDBR
{
    class ReadColumnDataFromCSV
    {
        /// <summary>
        /// Default constructor. Not used.
        /// </summary>
        private ReadColumnDataFromCSV()
        {
        }

        /// <summary>
        /// A class to read a CSV file containing channel file information.
        /// </summary>
        /// <param name="FileName">Full path to CSV file</param>
        public ReadColumnDataFromCSV(string FileName)
        {
            fileName = FileName;
        }

        /// <summary>
        /// Reads channel data from an external CSV file.
        /// </summary>
        /// <returns>Returns a DatumList containing a DatumList for each channel</returns>
        public void readFile(ref double[] IPhase,ref double[] IRear)
        {
            string header = null;

            ArrayList columns = new ArrayList();
            StreamReader sr = new StreamReader(fileName);
            using (sr)
            {
                header = sr.ReadLine();
                while (sr.Peek() >= 0)
                {
                    columns.Add(sr.ReadLine());
                }
                sr.Close();
            }

            //DatumList fullCurrentList = new DatumList();
            IPhase = new double[101];
            IRear = new double[210];
            int count = 0;

            string[] columnNames = header.Split(new string[] { "," }, System.StringSplitOptions.None);
            foreach (string column in columns)
            {
                //string name = null;

                DatumList singleCurrentList = new DatumList();

                string[] fields = column.Split(new string[] { "," }, System.StringSplitOptions.None);
                for (int i = 0; i < columnNames.GetUpperBound(0)+1; i++)
                {
                    if(i == 1&& count < 101)
                    {
                        IPhase[count] = double.Parse(fields[i]);
                    }
                    if (i == 0 && count < 210)
                    {
                        IRear[count] = double.Parse(fields[i]);

                    }
                    

                    //DatumDouble value = new DatumDouble(columnNames[i], double.Parse(fields[i]));
                    //singleCurrentList.Add(value);
                }
                count++;

                //if (count == 0)
                //    name = "REAR";
                //else if (count == 1)
                //    name = "PHASE";

                //fullCurrentList.AddListDatum("CURRENT_" + name, singleCurrentList);
                //count++;
            }
            //return fullCurrentList;
        }

        private string fileName;
    }
}

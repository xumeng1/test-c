// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// TcmzInitialise_Config.cs
//
// Author: Tony.Foster, 2007 (KP 2006)
// Design: [Reference design documentation]

using System;
using System.Text;
using System.Collections.Generic;
using Bookham.TestEngine.Framework.InternalData;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// This structure encapsulates all the data requirements of the test module
    /// At test program level the user declares an instance of the structure, but not a new instance, 
    /// ie mystruct myInstance;
    /// then they can make populate the members of the struct
    /// ie
    /// myInstance.param1 = 20;
    /// myInstance.param2 = 20;
    /// 
    /// Only when the structure is fully populated can they call
    /// DatumList myD = myInstance.GetDatumList();
    /// 
    /// Else they will get a compile time error, as the object myInstance
    /// only exists at the point where all members are populated!
    /// 
    /// In the test module level the user simply calls
    /// mystruct myInstance = new mystruct(configData);
    /// and they can read the myInstance.param1, myInstance.param2 = 20 values;
    /// </summary>
    public struct TcmzInitialise_Config
    {
        /// <summary>
        /// Method populates a datumlist based on this structure data
        /// </summary>
        /// <returns>datumlist</returns>
        public DatumList GetDatumList()
        {
            DatumList myLocalDatumList = new DatumList();

            DatumDouble MzContinuityTest_SetBias_mA = new DatumDouble("MzContinuityTest_SetBias_mA", this.MzContinuityTest_SetBias_mA);
            DatumDouble MzTapContinuityTest_SetBias_mA = new DatumDouble("MzTapContinuityTest_SetBias_mA", this.MzContinuityTest_SetBias_mA);
            DatumDouble MzContinuityTest_MinMeasV_volt = new DatumDouble("MzContinuityTest_MinMeasV_volt", this.MzContinuityTest_MinMeasV_volt);
            DatumDouble MzContinuityTest_MaxMeasV_volt = new DatumDouble("MzContinuityTest_MaxMeasV_volt", this.MzContinuityTest_MaxMeasV_volt);

            DatumDouble MzLeftImb_CurrentCompliance_A = new DatumDouble("MzLeftImb_CurrentCompliance_A", this.MzLeftImb_CurrentCompliance_A);
            DatumDouble MzLeftImb_VoltageCompliance_volt = new DatumDouble("MzLeftImb_VoltageCompliance_volt", this.MzLeftImb_VoltageCompliance_volt);
            DatumDouble MzRightImb_CurrentCompliance_A = new DatumDouble("MzRightImb_CurrentCompliance_A", this.MzRightImb_CurrentCompliance_A);
            DatumDouble MzRightImb_VoltageCompliance_volt = new DatumDouble("MzRightImb_VoltageCompliance_volt", this.MzRightImb_VoltageCompliance_volt);

            DatumDouble MzLeftMod_CurrentCompliance_A = new DatumDouble("MzLeftMod_CurrentCompliance_A", this.MzLeftMod_CurrentCompliance_A);
            DatumDouble MzLeftMod_VoltageCompliance_volt = new DatumDouble("MzLeftMod_VoltageCompliance_volt", this.MzLeftMod_VoltageCompliance_volt);
            DatumDouble MzRightMod_CurrentCompliance_A = new DatumDouble("MzRightMod_CurrentCompliance_A", this.MzRightMod_CurrentCompliance_A);
            DatumDouble MzRightMod_VoltageCompliance_volt = new DatumDouble("MzRightMod_VoltageCompliance_volt", this.MzRightMod_VoltageCompliance_volt);

            DatumDouble MzPowerTap_CurrentCompliance_A = new DatumDouble("MzPowerTap_CurrentCompliance_A", this.MzPowerTap_CurrentCompliance_A);
            DatumDouble MzPowerTap_VoltageCompliance_volt = new DatumDouble("MzPowerTap_VoltageCompliance_volt", this.MzPowerTap_VoltageCompliance_volt);

            myLocalDatumList.Add(MzContinuityTest_SetBias_mA);
            myLocalDatumList.Add(MzTapContinuityTest_SetBias_mA);
            myLocalDatumList.Add(MzContinuityTest_MinMeasV_volt);
            myLocalDatumList.Add(MzContinuityTest_MaxMeasV_volt);
            myLocalDatumList.Add(MzLeftImb_CurrentCompliance_A);
            myLocalDatumList.Add(MzLeftImb_VoltageCompliance_volt);
            myLocalDatumList.Add(MzRightImb_CurrentCompliance_A);
            myLocalDatumList.Add(MzRightImb_VoltageCompliance_volt);
            myLocalDatumList.Add(MzLeftMod_CurrentCompliance_A);
            myLocalDatumList.Add(MzLeftMod_VoltageCompliance_volt);
            myLocalDatumList.Add(MzRightMod_CurrentCompliance_A);
            myLocalDatumList.Add(MzRightMod_VoltageCompliance_volt);
            myLocalDatumList.Add(MzPowerTap_CurrentCompliance_A);
            myLocalDatumList.Add(MzPowerTap_VoltageCompliance_volt);

            return (myLocalDatumList);
        }

        /// <summary>
        /// Constructor which takes a datumList and populates the type
        /// </summary>
        /// <param name="mydata"></param>
        public TcmzInitialise_Config(DatumList mydata)
        {
            this.MzContinuityTest_SetBias_mA = mydata.ReadDouble("MzContinuityTest_SetBias_mA");
            this.MzTapContinuityTest_SetBias_mA = mydata.ReadDouble("MzTapContinuityTest_SetBias_mA");
            this.MzContinuityTest_MinMeasV_volt = mydata.ReadDouble("MzContinuityTest_MinMeasV_volt");
            this.MzContinuityTest_MaxMeasV_volt = mydata.ReadDouble("MzContinuityTest_MaxMeasV_volt");
            this.MzLeftImb_CurrentCompliance_A = mydata.ReadDouble("MzLeftImb_CurrentCompliance_A");
            this.MzLeftImb_VoltageCompliance_volt = mydata.ReadDouble("MzLeftImb_VoltageCompliance_volt");
            this.MzLeftMod_CurrentCompliance_A = mydata.ReadDouble("MzLeftMod_CurrentCompliance_A");
            this.MzLeftMod_VoltageCompliance_volt = mydata.ReadDouble("MzLeftMod_VoltageCompliance_volt"); 
            this.MzLeftImb_CurrentCompliance_A = mydata.ReadDouble("MzLeftImb_CurrentCompliance_A");
            this.MzRightImb_CurrentCompliance_A = mydata.ReadDouble("MzRightImb_CurrentCompliance_A");
            this.MzRightImb_VoltageCompliance_volt = mydata.ReadDouble("MzRightImb_VoltageCompliance_volt");
            this.MzRightMod_CurrentCompliance_A = mydata.ReadDouble("MzRightMod_CurrentCompliance_A");
            this.MzRightMod_VoltageCompliance_volt = mydata.ReadDouble("MzRightMod_VoltageCompliance_volt"); 
            this.MzLeftImb_CurrentCompliance_A = mydata.ReadDouble("MzLeftImb_CurrentCompliance_A");
            this.MzPowerTap_VoltageCompliance_volt = mydata.ReadDouble("MzPowerTap_VoltageCompliance_volt");
            this.MzPowerTap_CurrentCompliance_A = mydata.ReadDouble("MzPowerTap_CurrentCompliance_A");
        }

        //List of all my parameters i will use
        /// <summary>
        /// Mz Continuity test set bias
        /// </summary>
        public double MzContinuityTest_SetBias_mA;
        /// <summary>
        /// Mz Tap Continuity test set bias
        /// </summary>
        public double MzTapContinuityTest_SetBias_mA;
        /// <summary>
        /// Mz Continuity test minimum measured voltage
        /// </summary>
        public double MzContinuityTest_MinMeasV_volt;
        /// <summary>
        /// Mz Continuity test maximum measured voltage
        /// </summary>
        public double MzContinuityTest_MaxMeasV_volt;
        /// <summary>
        /// MZ Left Imbalance Arm Current compliance
        /// </summary>
        public double MzLeftImb_CurrentCompliance_A;
        /// <summary>
        /// MZ Left Modulator Arm Current compliance
        /// </summary>
        public double MzLeftMod_CurrentCompliance_A;
        /// <summary>
        /// MZ Left Imbalance Arm Voltage compliance
        /// </summary>
        public double MzLeftImb_VoltageCompliance_volt;        
        /// <summary>
        /// MZ Left Modulator Arm Voltage compliance
        /// </summary>
        public double MzLeftMod_VoltageCompliance_volt;
        /// <summary>
        /// MZ Right Imbalance Arm Current compliance
        /// </summary>
        public double MzRightImb_CurrentCompliance_A;
        /// <summary>
        /// MZ Right Imbalance Arm Voltage compliance
        /// </summary>
        public double MzRightImb_VoltageCompliance_volt;
        /// <summary>
        /// MZ Right Modulator Arm Current compliance
        /// </summary>
        public double MzRightMod_CurrentCompliance_A;
        /// <summary>
        /// MZ Right Modulator Arm Voltage compliance
        /// </summary>
        public double MzRightMod_VoltageCompliance_volt;
        /// <summary>
        /// MZ Power Tap Current compliance
        /// </summary>
        public double MzPowerTap_CurrentCompliance_A;
        /// <summary>
        /// MZ Power Tap Voltage compliance
        /// </summary>
        public double MzPowerTap_VoltageCompliance_volt;
  
    }
}


using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestSolution.ILMZFinalData;
using System.IO;
using Bookham.TestSolution.IlmzCommonUtils;
using Bookham.TestEngine.Framework.Limits;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Struct for test time reduction LVSweep channels
    /// </summary>
    struct TTRLVSweepChannels
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="startFreq_GHz">Start frequency in GHz</param>
        /// <param name="endFreq_GHz">End frequency in GHz</param>
        /// <param name="chanCount">The required channel count to test actually</param>
        /// <param name="chanSpace_GHz">Channels space in GHz</param>
        public TTRLVSweepChannels(double startFreq_GHz, double endFreq_GHz, int chanCount, double chanSpace_GHz)
        {
            // set local data
            ChannelsToTestActually = new List<double>();
            initSelectedChans = new List<double>();
            freqLow_GHz = startFreq_GHz;
            freqHigh_GHz = endFreq_GHz;
            freqSpace_GHz = chanSpace_GHz;


            double step_GHz = 0.0;
            int tempChanCount = (int)((endFreq_GHz - startFreq_GHz) / chanSpace_GHz + 1);
            if (tempChanCount > chanCount)
            {
                int temp = (int)((endFreq_GHz - startFreq_GHz) / ((chanCount - 1) * chanSpace_GHz));
                step_GHz = temp * chanSpace_GHz;
            }
            else
            {
                step_GHz = chanSpace_GHz;
                chanCount = tempChanCount;
            }

            initSelectedChans.Add(startFreq_GHz);
            for (int ii = 1; ii <= chanCount - 2; ii++)
            {
                double freq_Ghz = startFreq_GHz + ii * step_GHz;
                if (freq_Ghz >= endFreq_GHz)
                {
                    break;
                }
                initSelectedChans.Add(freq_Ghz);
            }
            initSelectedChans.Add(endFreq_GHz);

            ChannelsToTestActually.AddRange(initSelectedChans);
        }

        #region Public Functions
        /// <summary>
        /// Check if this channel it to be tested actually
        /// </summary>
        /// <param name="chanFreq_GHz">Channel frequency to check</param>
        /// <returns>True if this channel needs to be tested actually</returns>
        public bool IsToTestActually(double chanFreq_GHz)
        {
            bool retn = false;

            foreach (double freq_GHz in ChannelsToTestActually)
            {
                if (freq_GHz == chanFreq_GHz)
                {
                    retn = true;
                    break;
                }
            }

            return retn;
        }

        /// <summary>
        /// Return true if good linearity on the specified EnumTosaParam to check
        /// </summary>
        /// <param name="tcmzChannels"></param>
        /// <param name="paramToCheckLinearity"></param>
        /// <param name="paramLowLimit"></param>
        /// <param name="paramHighLimit"></param>
        /// <param name="paramsToCal"></param>
        /// <param name="linearityCheckMode"></param>
        /// <param name="linearityFile"></param>
        /// <returns></returns>
        public bool GoodLinearity(IlmzChannels tcmzChannels, EnumTosaParam paramToCheckLinearity, 
                    double paramLowLimit, double paramHighLimit,EnumTosaParam[] paramsToCal,
                    LinearityCheckMode linearityCheckMode,string linearityFile)
        {
            bool goodLinearity = true;

            List<ILMZChannel> chansForLinearity = getTcmzChansForVpiLinearityCal(tcmzChannels, paramToCheckLinearity);

            switch (linearityCheckMode)
            {
                case LinearityCheckMode.ratio:
                    processCalRatioGoodLinearity(paramsToCal, chansForLinearity,
                        paramToCheckLinearity, paramLowLimit, paramHighLimit, linearityFile, out goodLinearity);
                    break;
                case LinearityCheckMode.diff:
                    processCalDiffGoodLinearity(paramsToCal, chansForLinearity,
                        paramToCheckLinearity, paramLowLimit, paramHighLimit, linearityFile, out goodLinearity);
                    break;
                default:
                    throw new ArgumentException("Invalid linearity check mode!");
            }

            return goodLinearity;
        }
        #endregion

        #region Private Functions
        /// <summary>
        /// If the EnumTosaParam is tested, return the option of the channel
        /// This option should be the last one of this channel which has the EnumTosaParam tested, or null
        /// </summary>
        /// <param name="tcmzChannels"></param>
        /// <param name="freq_GHz"></param>
        /// <param name="tcmzParam"></param>
        /// <param name="tcmzChannel"></param>
        /// <returns></returns>
        private bool IsTested(IlmzChannels tcmzChannels, double freq_GHz, EnumTosaParam tcmzParam, out ILMZChannel tcmzChannel)
        {
            bool paramTested = false;
            tcmzChannel = null;
            // here we need a special EnumTosaParam to indicate if final LV characterise was really done in mid-temp test at a channel
            // because sometimes, the paramToCheckLinearity has a initial value( so it seems to be tesed in mid-temp), e.g. MzRightArmModQuad_V...
            EnumTosaParam lvParam = EnumTosaParam.MzDcVpi_V;

            ILMZChannel[] allOptions = tcmzChannels.GetChannelsAtItu(freq_GHz);

            // check if the parameter was tested or not
            for (int ii = allOptions.Length - 1; ii >= 0; ii--)
            {
                ILMZChannel option = allOptions[ii];
                if (option.Completeness != SpecComplete.NoParams && option.MidTempData.IsTested(lvParam) && option.MidTempData.IsTested(tcmzParam))
                {
                    paramTested = true;
                    tcmzChannel = option;
                    break;
                }
            }

            return paramTested;
        }

        /// <summary>
        /// Get the list of TCMZ channels for Vpi linearity calculation
        /// </summary>
        /// <param name="tcmzChannels"></param>
        /// <param name="paramToCheckLinearity"></param>
        /// <returns></returns>
        private List<ILMZChannel> getTcmzChansForVpiLinearityCal(IlmzChannels tcmzChannels, EnumTosaParam paramToCheckLinearity)
        {
            List<ILMZChannel> chansForLinearity = new List<ILMZChannel>();

            // loop through all initially selected channels...
            // if vpi of a channel hasn't been tested for all options(i.e. this channel is missed or all options failed before MZ sweep), then
            // we can't use this channel to calculate vpi linearity. Need to find a substitute...
            foreach (double freq_GHz in initSelectedChans)
            {
                EnumTosaParam param = paramToCheckLinearity;
                ILMZChannel chan = null;
                bool paramTested = IsTested(tcmzChannels, freq_GHz, param, out chan);

                // if the parameter was not tested for all options, find a substitute
                if (paramTested)
                {
                    chansForLinearity.Add(chan);
                }
                else
                {
                    // Search the first lower channel with the parameter tested in ttrLvSweepChans.ChannelsToTestActually list
                    double tempFreq_GHz = freq_GHz - freqSpace_GHz;
                    while (tempFreq_GHz >= freqLow_GHz)
                    {
                        if (IsToTestActually(tempFreq_GHz))
                        {
                            ILMZChannel chanLow = null;
                            if (IsTested(tcmzChannels, tempFreq_GHz, param, out chanLow))
                            {
                                if (!chansForLinearity.Contains(chanLow))
                                    chansForLinearity.Add(chanLow);
                                break;
                            }
                        }
                        tempFreq_GHz -= freqSpace_GHz;
                    }
                    // Search the first higher channel with the parameter tested in ttrLvSweepChans.ChannelsToTestActually list
                    tempFreq_GHz = freq_GHz + freqSpace_GHz;
                    while (tempFreq_GHz <= freqHigh_GHz)
                    {
                        if (IsToTestActually(tempFreq_GHz))
                        {
                            ILMZChannel chanHigh = null;
                            if (IsTested(tcmzChannels, tempFreq_GHz, param, out chanHigh))
                            {
                                if (!chansForLinearity.Contains(chanHigh))
                                    chansForLinearity.Add(chanHigh);
                                break;

                            }
                        }
                        tempFreq_GHz += freqSpace_GHz;
                    }
                }
            }

            chansForLinearity.Sort(new TcmzChannelComparer());
            return chansForLinearity;
        }

        /// <summary>
        /// Check if good linearity or not, and save the linearity file
        /// </summary>
        /// <param name="paramsToCalRatio"></param>
        /// <param name="chansForLinearity"></param>
        /// <param name="paramToCheckLinearity"></param>
        /// <param name="paramLowLimit"></param>
        /// <param name="paramHighLimit"></param>
        /// <param name="linearityFile"></param>
        /// <param name="goodLinearity"></param>
        private void processCalRatioGoodLinearity(EnumTosaParam[] paramsToCalRatio,
                    List<ILMZChannel> chansForLinearity, EnumTosaParam paramToCheckLinearity, 
                    double paramLowLimit, double paramHighLimit,
                    string linearityFile, out bool goodLinearity)
        {
            #region calculate the ratios of slope differences for paramsToCalRatio between channels
            // ratio(n) = [slope(n+1)-slope(n)]/slope(T)

            double[] slope = new double[paramsToCalRatio.Length];
            double[,] ratio = new double[chansForLinearity.Count, paramsToCalRatio.Length];

            // loop through all the parameters in paramsToCalRatio
            for (int pp = 0; pp < paramsToCalRatio.Length; pp++)
            {
                EnumTosaParam paramToCal = paramsToCalRatio[pp];
                int count = chansForLinearity.Count;

                // calculate the vpi slope between the first and last chan
                if (count >= 2)
                {
                    double yDistance =
                        chansForLinearity[count - 1].MidTempData.GetValueDouble(paramToCal) -
                        chansForLinearity[0].MidTempData.GetValueDouble(paramToCal);
                    double xDistance =
                        chansForLinearity[count - 1].MidTempData.GetValueSint32(EnumTosaParam.ItuChannelIndex) -
                        chansForLinearity[0].MidTempData.GetValueSint32(EnumTosaParam.ItuChannelIndex);

                    if (xDistance != 0)
                    {
                        slope[pp] = yDistance / xDistance;
                        if (slope[pp] == 0)
                        {
                            throw new ArgumentException("Can't calculate ratio as the vpi slope between the first and last channels is 0!!!");
                        }
                    }
                    else
                    {
                        throw new ArgumentException("Can't calculate slope(T) as only 1 channel selected for Vpi linearity calculation!!!");
                    }
                }

                // calculate the ratio(n) for all the selected segements...
                if (count >= 3)
                {
                    // calculate all the ratios
                    for (int ii = 0; ii < chansForLinearity.Count - 2; ii++)
                    {
                        double[] slopes = new double[2];

                        for (int nn = 0; nn <= 1; nn++)
                        {
                            double yDistance =
                                chansForLinearity[ii + nn + 1].MidTempData.GetValueDouble(paramToCal) -
                                chansForLinearity[ii + nn].MidTempData.GetValueDouble(paramToCal);
                            double xDistance =
                                chansForLinearity[ii + nn + 1].MidTempData.GetValueSint32(EnumTosaParam.ItuChannelIndex) - 
                                chansForLinearity[ii + nn].MidTempData.GetValueSint32(EnumTosaParam.ItuChannelIndex);
                            if (xDistance != 0)
                            {
                                slopes[nn] = yDistance / xDistance;
                            }
                            else
                            {
                                throw new ArgumentException(string.Format("Can't calculate slope({0}) as there are at least two same channels selected for Vpi linearity calculation!", ii + nn));
                            }
                        }

                        // calculate the ratio
                        ratio[ii, pp] = (slopes[1] - slopes[0]) / slope[pp];
                    }// end calculate all the ratios
                }
            }// end loop through all the parameters in paramsToCalRatio

            #endregion

            goodLinearity = true;

            #region calculate the good linearity status
            // Calculate the good linearity...
            for (int ii = 0; ii < chansForLinearity.Count - 2; ii++)
            {
                double d_ratio = ratio[ii, 0];// the first column is the ratios for paramToCheckLinearity...

                for (int jj = 0; jj < paramsToCalRatio.Length; jj++)
                {
                    if (paramsToCalRatio[jj] == paramToCheckLinearity)
                    {
                        // should be ratio[ii, 0] as the first column is the ratios for paramToCheckLinearity...
                        d_ratio = ratio[ii, jj];
                        break;
                    }
                }

                if (d_ratio < paramLowLimit || d_ratio > paramHighLimit)
                {
                    goodLinearity = false;
                    break;
                }
            }
            #endregion

            #region write data to file
            // Save the linearity file here...
            using (StreamWriter writer = new StreamWriter(linearityFile))
            {
                // The first line is the filename...
                string[] str = linearityFile.Split('\\');
                writer.WriteLine(str[str.Length - 1].Replace(".csv", ""));

                // Then write the slope(T) for each paramsToCalRatio...
                StringBuilder s1 = new StringBuilder();
                StringBuilder s2 = new StringBuilder();
                int i = 0;
                foreach (EnumTosaParam param in paramsToCalRatio)
                {
                    string s = Enum.GetName(typeof(EnumTosaParam), param);
                    writer.WriteLine("Slope_" + s + "," + slope[i].ToString());

                    if (param == paramToCheckLinearity)
                    {
                        s1.Append(s + ",");
                        s2.Append(string.Format("Ratio_" + s + "[{0};{1}],", paramLowLimit, paramHighLimit));
                    }
                    else
                    {
                        s1.Append(s + ",");
                        s2.Append("Ratio_" + s + ",");
                    }
                    i++;
                }

                // Then the file header...
                StringBuilder header = new StringBuilder();
                header.Append("Channel,ItuFreq_GHz,");
                header.Append(s1.ToString());
                header.Append(s2.ToString());
                header.Append("Status");
                writer.WriteLine(header.ToString());

                // Then the file content...
                int ii = 0;
                foreach (ILMZChannel chan in chansForLinearity)
                {
                    writer.Write(chan.MidTempData.GetValueSint32(EnumTosaParam.ItuChannelIndex));
                    writer.Write(",");
                    writer.Write(chan.MidTempData.GetValueDouble(EnumTosaParam.ItuFreq_GHz));
                    foreach (EnumTosaParam param in paramsToCalRatio)
                    {
                        writer.Write(",");
                        writer.Write(chan.MidTempData.GetValueDouble(param));
                    }

                    if (ii < chansForLinearity.Count - 2)
                    {
                        writer.Write(",");

                        double d_ratio = 0;
                        for (int jj = 0; jj < paramsToCalRatio.Length; jj++)
                        {
                            writer.Write(ratio[ii, jj].ToString());
                            writer.Write(",");
                            if (paramsToCalRatio[jj] == paramToCheckLinearity)
                                d_ratio = ratio[ii, jj];
                        }
                        if (d_ratio < paramLowLimit || d_ratio > paramHighLimit)
                        {
                            writer.Write("Fail");
                        }
                        else
                        {
                            writer.Write("Pass");
                        }

                    }

                    writer.WriteLine();
                    ii++;
                }
            }
            #endregion
        }

        /// <summary>
        /// Check if good linearity or not, and save the linearity file
        /// </summary>
        /// <param name="paramsToCalDiff"></param>
        /// <param name="chansForLinearity"></param>
        /// <param name="paramToCheckLinearity"></param>
        /// <param name="paramLowLimit"></param>
        /// <param name="paramHighLimit"></param>
        /// <param name="linearityFile"></param>
        /// <param name="goodLinearity"></param>
        private void processCalDiffGoodLinearity(EnumTosaParam[] paramsToCalDiff, List<ILMZChannel> chansForLinearity,
            EnumTosaParam paramToCheckLinearity, double paramLowLimit, double paramHighLimit, string linearityFile, out bool goodLinearity)
        {
            #region calculate the slope differences for paramsToCalDiff between channels
            // diff(n) = slope(n+1)-slope(n)

            double[,] diff = new double[chansForLinearity.Count, paramsToCalDiff.Length];

            // loop through all the parameters in paramsToCalDiff
            for (int pp = 0; pp < paramsToCalDiff.Length; pp++)
            {
                EnumTosaParam paramToCal = paramsToCalDiff[pp];
                int count = chansForLinearity.Count;

                // calculate the diff(n) for all the selected segements...
                if (count >= 3)
                {
                    // calculate all the diffs
                    for (int ii = 0; ii < chansForLinearity.Count - 2; ii++)
                    {
                        double[] slopes = new double[2];

                        for (int nn = 0; nn <= 1; nn++)
                        {
                            double yDistance =
                                chansForLinearity[ii + nn + 1].MidTempData.GetValueDouble(paramToCal) - chansForLinearity[ii + nn].MidTempData.GetValueDouble(paramToCal);
                            double xDistance =
                                chansForLinearity[ii + nn + 1].MidTempData.GetValueSint32(EnumTosaParam.ItuChannelIndex) - chansForLinearity[ii + nn].MidTempData.GetValueSint32(EnumTosaParam.ItuChannelIndex);
                            if (xDistance != 0)
                            {
                                slopes[nn] = yDistance / xDistance;
                            }
                            else
                            {
                                throw new ArgumentException(string.Format("Can't calculate slope({0}) as there are at least two same channels selected for Vpi linearity calculation!", ii + nn));
                            }
                        }

                        // calculate the diff
                        diff[ii, pp] = slopes[1] - slopes[0];
                    }// end calculate all the diffs
                }
            }// end loop through all the parameters in paramsToCalDiff
            #endregion

            goodLinearity = true;

            #region calculate the good linearity status
            // Calculate the good linearity...
            for (int ii = 0; ii < chansForLinearity.Count - 2; ii++)
            {
                double d_diff = diff[ii, 0];// the first column is the diff for paramToCheckLinearity...

                for (int jj = 0; jj < paramsToCalDiff.Length; jj++)
                {
                    if (paramsToCalDiff[jj] == paramToCheckLinearity)
                    {
                        // should be diff[ii, 0] as the first column is the diff for paramToCheckLinearity...
                        d_diff = diff[ii, jj];
                        break;
                    }
                }

                if (d_diff < paramLowLimit || d_diff > paramHighLimit)
                {
                    goodLinearity = false;
                    break;
                }
            }
            #endregion

            #region write data to file
            // Save the linearity file here...
            using (StreamWriter writer = new StreamWriter(linearityFile))
            {
                // The first line is the filename...
                string[] str = linearityFile.Split('\\');
                writer.WriteLine(str[str.Length - 1].Replace(".csv", ""));

                // construct the file header...
                StringBuilder s1 = new StringBuilder();
                StringBuilder s2 = new StringBuilder();
                foreach (EnumTosaParam param in paramsToCalDiff)
                {
                    string s = Enum.GetName(typeof(EnumTosaParam), param);
                    if (param == paramToCheckLinearity)
                    {
                        s1.Append(s + ",");
                        s2.Append(string.Format("SlopeDiff_" + s + "[{0};{1}],", paramLowLimit, paramHighLimit));
                    }
                    else
                    {
                        s1.Append(s + ",");
                        s2.Append("SlopeDiff_" + s + ",");
                    }
                }

                // Then the file header...
                StringBuilder header = new StringBuilder();
                header.Append("Channel,ItuFreq_GHz,");
                header.Append(s1.ToString());
                header.Append(s2.ToString());
                header.Append("Status");
                writer.WriteLine(header.ToString());

                // Then the file content...
                int ii = 0;
                foreach (ILMZChannel chan in chansForLinearity)
                {
                    writer.Write(chan.MidTempData.GetValueSint32(EnumTosaParam.ItuChannelIndex));
                    writer.Write(",");
                    writer.Write(chan.MidTempData.GetValueDouble(EnumTosaParam.ItuFreq_GHz));
                    foreach (EnumTosaParam param in paramsToCalDiff)
                    {
                        writer.Write(",");
                        writer.Write(chan.MidTempData.GetValueDouble(param));
                    }


                    if (ii < chansForLinearity.Count - 2)
                    {
                        writer.Write(",");

                        double d_diff = 0;
                        for (int jj = 0; jj < paramsToCalDiff.Length; jj++)
                        {
                            writer.Write(diff[ii, jj].ToString());
                            writer.Write(",");
                            if (paramsToCalDiff[jj] == paramToCheckLinearity)
                                d_diff = diff[ii, jj];
                        }
                        if (d_diff < paramLowLimit || d_diff > paramHighLimit)
                        {
                            writer.Write("Fail");
                        }
                        else
                        {
                            writer.Write("Pass");
                        }

                    }

                    writer.WriteLine();
                    ii++;
                }
            }
            #endregion
        }

        #endregion

        #region Public Members
        public List<double> ChannelsToTestActually;
        #endregion

        #region Private Members
        private double freqLow_GHz;
        private double freqHigh_GHz;
        private double freqSpace_GHz;
        private List<double> initSelectedChans;
        #endregion
    }

    /// <summary>
    /// Linearity checking mode for Mz parameter
    /// </summary>
    enum LinearityCheckMode
    {
        /// <summary>
        /// Ration(n)=[Slope(n+1)-Slope(n)]/Slope(T)
        /// </summary>
        ratio = 1,
        /// <summary>
        /// Diff(n)=Slope(n+1)-Slope(n)
        /// </summary>
        diff = 2
    }

    /// <summary>
    /// Struct for test time reduction LockerSlope channels
    /// </summary>
    struct TTRLockerSlopeChannels
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="testSelect">Test Selection utility</param>
        /// <param name="startFreq_GHz">Start frequency in GHz</param>
        /// <param name="endFreq_GHz">End frequency in GHz</param>
        /// <param name="chanSpace_GHz">Channel frequency space in GHz</param>
        /// <param name="lockerSlopeLimitLow">The low limit for checking the LockerSlopeMeasurement status of a channel</param>
        /// <param name="lockerSlopeLimitHigh">The high limit for checking the LockerSlopeMeasurement status of a channel</param>
        public TTRLockerSlopeChannels(TestSelection testSelect, double startFreq_GHz,
                            double endFreq_GHz, double chanSpace_GHz, 
                            double lockerSlopeLimitLow, double lockerSlopeLimitHigh)
        {
            ttrLockerSlopeChannels = new Dictionary<double, LockerSlopeChannelTestStatus>();

            // Loop through all channels
            for (double chanFreq_GHz = startFreq_GHz; chanFreq_GHz <= endFreq_GHz; chanFreq_GHz += chanSpace_GHz)
            {
                if (testSelect.IsTestSelected(chanFreq_GHz, "Locker Slope"))
                {
                    ttrLockerSlopeChannels.Add(chanFreq_GHz, LockerSlopeChannelTestStatus.NotTested);
                }
            }

            this.lockerSlopeLimitHigh = lockerSlopeLimitHigh;
            this.lockerSlopeLimitLow = lockerSlopeLimitLow;
            this.paramToCheck = EnumTosaParam.LockerSlopeEffAbs;
        }

        #region Public Functions
        /// <summary>
        /// False only when all TTRLockerSlopeChannels are tested against LockerSlopeMeasurement,and at least one failure
        /// </summary>
        /// <param name="tcmzChannels"></param>
        /// <returns></returns>
        public bool TTRLockerSlopeChannelsPassed(IlmzChannels tcmzChannels)
        {
            bool paramFail = false;
            ILMZChannel chan = null;

            // loop through all TTRLockerSlopeChannels
            foreach (double freq_GHz in ttrLockerSlopeChannels.Keys)
            {
                if (this.IsTestedAtTheLastTestedOption(tcmzChannels, freq_GHz, paramToCheck, out chan))
                {
                    double paramVal = chan.MidTempData.GetValueDouble(paramToCheck);
                    if (paramVal < this.lockerSlopeLimitLow || paramVal > this.lockerSlopeLimitHigh)
                    {
                        paramFail = true;
                    }
                }
                else
                {
                    // if find a channel missed test against LockerSlopeMeasurement, exists with true.
                    return true;
                }
            }

            // if can reach here, it means all TTRLockerSlopeChannels were tested against LockerSlopeMeasurement
            if (paramFail)
                return false;
            else
                return true;
        }

        /// <summary>
        /// Check if this channel is a TTRLockerSlopeChannel.
        /// If it is, then don't test this channel when there's a need to test LockerSlope for all channels as this channel should be tested already,
        /// or missed, or failed before LockerSlope test. So is the ItuExtremeChannels
        /// </summary>
        /// <param name="chanFreq_GHz"></param>
        /// <returns></returns>
        public bool IsTTRLockerSlopeChannel(double chanFreq_GHz)
        {
            return ttrLockerSlopeChannels.ContainsKey(chanFreq_GHz);
        }
        #endregion

        #region Private Functions
        /// <summary>
        /// Check if the EnumTosaParam is tested at the last channel option which has finished test. 
        /// Always retun the last channel option which has finished test
        /// </summary>
        /// <param name="tcmzChannels"></param>
        /// <param name="freq_GHz"></param>
        /// <param name="tcmzParam"></param>
        /// <param name="tcmzChannel"></param>
        /// <returns></returns>
        private bool IsTestedAtTheLastTestedOption(IlmzChannels tcmzChannels, double freq_GHz, EnumTosaParam tcmzParam, out ILMZChannel tcmzChannel)
        {
            bool paramTested = false;
            tcmzChannel = null;

            ILMZChannel[] allOptions = tcmzChannels.GetChannelsAtItu(freq_GHz);

            // find the last channel option which has finished test
            for (int ii = allOptions.Length - 1; ii >= 0; ii--)
            {
                ILMZChannel option = allOptions[ii];
                if (option.MidTempData.Finished)
                {
                    tcmzChannel = option;
                    break;
                }
            }

            // check if the EnumTosaParam is tested at this option
            if (tcmzChannel != null && tcmzChannel.MidTempData.IsTested(tcmzParam))
                paramTested = true;

            return paramTested;
        }
        #endregion

        #region Private Members
        private Dictionary<double, LockerSlopeChannelTestStatus> ttrLockerSlopeChannels;
        private double lockerSlopeLimitLow;
        private double lockerSlopeLimitHigh;
        private EnumTosaParam paramToCheck;
        #endregion
    }

    /// <summary>
    /// Enum for the LockerSlope test status of the channel with locker slope measurement
    /// </summary>
    enum LockerSlopeChannelTestStatus
    {
        Passed = 0,
        Failed = 1,
        NotTested = 2
    }
}

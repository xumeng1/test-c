// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// IlmzChannelChar_OverTemp.cs
//
// Author: Tony.Foster, Mark Fullalove 2007
// Design: [Reference design documentation]
//

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Framework.Limits;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestLibrary.Utilities;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.Algorithms;
using Bookham.Toolkit.CloseGrid;//jack.Zhang
using Bookham.ToolKit.Mz;
using Bookham.TestSolution.ILMZFinalData;
using Bookham.TestSolution.IlmzCommonData;
using Bookham.TestSolution.IlmzCommonInstrs;
using Bookham.TestSolution.IlmzCommonUtils;
using Bookham.TestSolution.TestModules;
using Bookham.TestLibrary.Instruments;
using System.Threading;



namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// ND Temp Test Module 
    /// </summary>
    public class IlmzChannelChar_OverTemp : ITestModule
    {
        #region ITestModule Members

        /// <summary>
        ///  Module Test
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="userType"></param>
        /// <param name="configData"></param>
        /// <param name="instruments"></param>
        /// <param name="chassis"></param>
        /// <param name="calData"></param>
        /// <param name="previousTestData"></param>
        /// <returns></returns>
        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, DatumList configData, InstrumentCollection instruments, ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {
            // previous data
            IlmzChannels tcmzChannels = (IlmzChannels)configData.ReadReference("IlmzItuChannels");
            //this.MZSourceMeasureDelay_ms = configData.ReadSint32("MZSourceMeasureDelay_ms");

            // Init instruments
            dsdbrTec = (IInstType_TecController)instruments["DsdbrTec"];
            if (instruments.Contains("MzTec")) mzTec = (IInstType_TecController)instruments["MzTec"];
            //switchOsaMzOpm = (Switch_Osa_MzOpm)configData.ReadReference("SwitchOsaMzOpm");

            // read config
            mzFileDirectory = configData.ReadString("ResultDir");
            TcmzTestTemp testTemp = (TcmzTestTemp)configData.ReadEnum("Temp");

            List<int> extremeChansOfIRear = null;

            if (configData.IsPresent("ExtremeChansOfIRear"))
            {
                extremeChansOfIRear = configData.ReadReference("ExtremeChansOfIRear") as List<int>;
            }

            List<int> extremeChansOfRipple = null;

            if (configData.IsPresent("ExtremeChansOfRipple"))
            {
                extremeChansOfRipple = configData.ReadReference("ExtremeChansOfRipple") as List<int>;
            }

            TestSelection testSelect = (TestSelection)configData.ReadReference("TestSelect");
            tapPhotoCurrentToleranceRatio = configData.ReadDouble("TapPhotoCurrentToleranceRatio");
            DsdbrTuning.LockRatioTolerance = configData.ReadDouble("LockRatioTolerance");

            Optical_switch = (Inst_Ke2510)instruments["Optical_switch"];
            DsdbrTuning.OpticalSwitch = Optical_switch;
            DsdbrUtils.opticalSwitch = Optical_switch;

            double locker_tran_pot = configData.ReadDouble("locker_tran_pot");
            bool DO_GRandR = configData.ReadBool("DO_GRandR");

            // Create some debug information for test time reduction
            if (configData.IsPresent("LoggingEnabled"))
            {
                bool loggingEnabled = configData.ReadBool("LoggingEnabled");
                GuiLoggingEnable guiMsg = new GuiLoggingEnable(loggingEnabled, "TestTime_" + testTemp.ToString());
                engine.SendToGui(guiMsg);
            }

            // Initialise GUI
            engine.GuiShow();
            engine.GuiToFront();
            engine.SendToGui(new GuiTitleMessage("Hitt Test Mid-Temperature extreme channels"));
            engine.SendToGui(new GuiTextMessage("Initialising", 0));

            // MZ driver utils
            IlMzInstruments mzInstrs = (IlMzInstruments)configData.ReadReference("MzInstruments");
            mzDriverUtils = new IlMzDriverUtils(mzInstrs);
            mzDriverUtils.opticalSwitch = Optical_switch;
            mzDriverUtils.locker_port = int.Parse(locker_tran_pot.ToString());

            #region MZ Sweep setup
            TcmzMzSweep_Config mzConfig = new TcmzMzSweep_Config(configData);
            double currentCompliance_A = mzConfig.MZCurrentCompliance_A;
            double currentRange_A = mzConfig.MZCurrentRange_A;
            double voltageCompliance_V = mzConfig.MZVoltageCompliance_V;
            double voltageRange_V = mzConfig.MZVoltageRange_V;
            int numberOfAverages = mzConfig.MzInitNumberOfAverages;
            double integrationRate = mzConfig.MZIntegrationRate;
            double sourceMeasureDelay = mzConfig.MZSourceMeasureDelay_s;
            double powerMeterAveragingTime = mzConfig.MZPowerMeterAveragingTime_s;
            inlineTap_V = mzConfig.MZTapBias_V;
            compTap_V = mzConfig.MZTapBias_V;
            mzLvStepSize = mzConfig.MZInitSweepStepSize_mV;
            //string dutSerialNbr = configData.ReadString("DutSerialNbr");


            engine.SendToGui(new GuiTextMessage("Preparing instruments for sweeping", 0));
            if (!ArmSourceByAsic)
            {
                mzDriverUtils.InitialiseMZArms(currentCompliance_A, currentRange_A);
                mzDriverUtils.InitialiseImbalanceArms(voltageCompliance_V, voltageRange_V);
                mzDriverUtils.InitialiseTaps(currentCompliance_A, currentRange_A);
                mzDriverUtils.SetMeasurementAccuracy(numberOfAverages, integrationRate, sourceMeasureDelay, powerMeterAveragingTime);
                inlineTap_V = mzConfig.MZTapBias_V;
                compTap_V = mzConfig.MZTapBias_V;
                mzLvStepSize = mzConfig.MZInitSweepStepSize_mV;
            }

            mzInstrs.PowerMeter.Mode = Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.MeterMode.Absolute_mW;


            #endregion

            ILMZChannel[] channelsToTest = tcmzChannels.AllOptions;
            // Confirm that at least 1 channel is present
            if (channelsToTest.Length < 1)
            {
                engine.RaiseNonParamFail(0, "No channels found");
            }

            // find the channels with the highest and lowest power dissipation
            // IlmzChannels.ExtremeChannelIndexes extremeChans = tcmzChannels.GetItuExtremeChannels();
            IlmzChannels.ExtremeChannelIndexes extremeChans = (IlmzChannels.ExtremeChannelIndexes)configData.ReadReference("ExtremeChannels");

            // Initialise data
            DatumList returnData = new DatumList();
            bool allPass = true;
            int numChanTested = 0;
            int numChanPass = 0;
            int numLockFails = 0;

            double BreakFreqIndex = 0.0;

            // loop through the channels that passed
            //foreach(ILMZChannel channel in tcmzChannels.PassedMidTemp)
            foreach (ILMZChannel channel in tcmzChannels.AllOptions)
            {
                double itu_freq = channel.MidTempData.GetValueDouble(EnumTosaParam.ItuFreq_GHz);

                if (BreakFreqIndex == itu_freq)
                { continue; }
                BreakFreqIndex = itu_freq;

                double ituFreq_GHz = channel.MidTempData.GetValueDouble(EnumTosaParam.ItuFreq_GHz);
                IlmzChannelMeas midTempData = channel.MidTempData;


                bool doTest = false;
                bool isTestRipple = false;
                int ituChannelIndex = midTempData.GetValueSint32(EnumTosaParam.ItuChannelIndex);

                int channelIndexAtItu = tcmzChannels.GetChannelIndexAtItu(ituFreq_GHz);

                if (extremeChansOfRipple != null && extremeChansOfRipple.Contains(channelIndexAtItu))
                {
                    doTest = true;
                    isTestRipple = true;
                }
                else if (extremeChansOfIRear != null && extremeChansOfIRear.Contains(ituChannelIndex))
                {
                    doTest = true;
                }
                else if (ituChannelIndex == extremeChans.HighestLaserDissIndex ||
                    ituChannelIndex == extremeChans.LowestLaserDissIndex ||
                    ituChannelIndex == extremeChans.HighestestIsoaIndex ||
                    ituChannelIndex == extremeChans.LowestIsoaIndex ||
                    ituChannelIndex == extremeChans.FirstChannelIndex ||
                    ituChannelIndex == extremeChans.LastChannelIndex ||
                    ituChannelIndex == extremeChans.LowestLockSlopeEffIndex ||
                    ituChannelIndex == extremeChans.HighestLockSlopeEffIndex)
                {
                    doTest = true;
                }

                if (doTest)
                {
                    engine.SendToGui(new GuiTextMessage("Testing " + ituFreq_GHz.ToString() + " GHz channel", 0));
                    engine.SendToGui(new GuiProgressMessage((numChanTested * 100) / 5));

                    // test the channel
                    ChanTestNumber = numChanTested;
                    bool tempTestSuccess = MidTempTestChannel(engine, mzInstrs, ituFreq_GHz, midTempData, configData);// false if missing temp test
                    numChanTested++;
                    ChanTestNumber = numChanTested;

                    if (tempTestSuccess)
                    {
                        if (midTempData.OverallStatus == PassFail.Pass)
                        {
                            numChanPass++;

                            midTempData.SetValueBool(EnumTosaParam.Pass_Flag, true);
                        }
                        else
                        {
                            allPass = false;

                            midTempData.SetValueBool(EnumTosaParam.Pass_Flag, false);
                        }
                    }
                    else
                    {
                        allPass = false;

                        midTempData.SetValueBool(EnumTosaParam.Pass_Flag, false);
                    }
                }

                if (isTestRipple)
                {
                    #region ITU longitudinal mode scan and check ripple - chongjian.liang 2013.5.4

                    engine.SendToGui(new GuiTextMessage("ITU longitudinal mode scan", 2));

                    bool useLowCostSolution = configData.ReadBool("UseLowCostSolution");

                    // Get current value before scan
                    double initIPhase = DsdbrUtils.ReadSectionCurrent_mA(DSDBRSection.Phase);
                    double initIRear = DsdbrUtils.ReadSectionCurrent_mA(DSDBRSection.Rear);

                    // Initialize configs
                    double CH_LASER_RTH_Min = configData.ReadDouble("CH_LASER_RTH_Min");
                    double CH_LASER_RTH_Max = configData.ReadDouble("CH_LASER_RTH_Max");

                    string phaseScanSequenceFilePath = configData.ReadString("PhaseCurrentFile");
                    string eolPhaseScanDropFilePath = configData.ReadString("EOLPhaseScanDropFilePath");

                    double ripple_Min = configData.ReadDouble("Ripple_MIN");
                    double ripple_Max = configData.ReadDouble("Ripple_MAX");
                    int afterJumpIgnoreCount = configData.ReadSint32("AfterJumpIgnoreCount");
                    int beforeJumpIgnoreCount = configData.ReadSint32("BeforeJumpIgnoreCount");
                    double multiple = configData.ReadDouble("Multiple");
                    string multipleFilterString = configData.ReadString("MultipleFilterString");

                    int scanStartIndex = configData.ReadSint32("EOLScanBoundLeft");
                    int scanEndIndex = configData.ReadSint32("EOLScanBoundRight");

                    // Do ripple test
                    double ripple_GHz = RippleTestOperations.DoRippleTest(useLowCostSolution, "OT_MID", ituFreq_GHz, phaseScanSequenceFilePath, eolPhaseScanDropFilePath, scanStartIndex, scanEndIndex,
                        dsdbrTec, CH_LASER_RTH_Min, CH_LASER_RTH_Max,
                        ripple_Min, ripple_Max, afterJumpIgnoreCount, beforeJumpIgnoreCount, multiple, multipleFilterString);

                    // Set current back to value after scan
                    DsdbrUtils.ClockwiseStart(initIPhase, initIRear);

                    returnData.AddDouble("LM_PHASE_SCAN_RIPPLE_MID", ripple_GHz);

                    #endregion
                } 
            }

            engine.SendToGui(new GuiTextMessage("Completed " + "Mid" + " temperature tests", 0));

            // Return data
            returnData.AddSint32("PassFailFlag", allPass || DO_GRandR ? 1 : 0);
            returnData.AddSint32("NumChannelsTested", numChanTested);
            returnData.AddSint32("NumChannelsPass", numChanPass);
            returnData.AddSint32("NumLockFails", numLockFails);
            return returnData;
        }

        /// <summary>
        /// Temperature test at a channel. Return true if ran successfully
        /// </summary>
        /// <param name="engine">Test engine</param>
        /// <param name="mzInstrs">Mz instruments collection</param>
        /// <param name="ituFreq_GHz">The channel ITU frequency in GHz</param>
        /// <param name="meas">Temperature measured data</param>
        /// <param name="midTempData">Mid-temperature measured data</param>
        /// <param name="configData">Configuration data</param>
        /// <returns>Temperature test running status</returns>
        private bool MidTempTestChannel(ITestEngine engine, IlMzInstruments mzInstrs,
                        double ituFreq_GHz,IlmzChannelMeas midTempData, DatumList configData)
        {
            double freqGHz, freqChange_Ghz;


            engine.SendToGui(new GuiTextMessage("Unlocked measurements", 1));

            // get data from MidTemp to setup the device
            DsdbrChannelSetup dsdbrSetup = midTempData.DsdbrSetup;

            // DSDBR setup - set to ITU freq
            DsdbrUtils.SetDsdbrCurrents_mA(dsdbrSetup);
            //DsdbrUtils.SetSectionCurrent_mA(DSDBRSection.Phase, midTempData.GetValueDouble(EnumTosaParam.IPhaseCal_mA));

            // MZ setup
            MzData midTempMzSettings = midTempData.MzSettings;
            MzData newMzSetup = midTempMzSettings;
            if (!ArmSourceByAsic)
            {
                mzDriverUtils.SetupMzToPeak(midTempMzSettings);
            }
            else
            {
                IlMzDriverUtils.SetupMzToPeak(mzInstrs.FCU2Asic, midTempMzSettings);
            }

            // Thermistor resistances
            // Allow a number of attempts to settle
            Thread.Sleep(rthDelay_mS * 6);
            int rthCount = 0;
            do
            {
                Thread.Sleep(rthDelay_mS);
                midTempData.SetValueDouble(EnumTosaParam.RthDsdbr_ohm, dsdbrTec.SensorResistanceActual_ohm);
            } while ((midTempData.ParamStatus(EnumTosaParam.RthDsdbr_ohm) == PassFail.Fail)
                && rthCount++ < numRthTries);


            // Retrieve reference values from MidTemp results to compare against
            double freqReference_Ghz = double.NegativeInfinity;
            double powerPeakReference_dBm = double.NegativeInfinity;
            double tapPhotocurrentReferencePeak_A = double.NegativeInfinity;
            double tapPhotocurrentReferenceQuad_A = double.NegativeInfinity;
            double powerQuadReference_dBm = double.NegativeInfinity;
            double targetMonitorRatio = double.NegativeInfinity;
            double phaseRatioSlopeEff = double.NegativeInfinity;
            double itxLockReference_mA = double.NegativeInfinity;
            double irxLockReference_mA = double.NegativeInfinity;            
            double lockRatio =double.NegativeInfinity;
            double tapCompPhotoCurrentPeak_mA = double.NegativeInfinity;
            double tapCompPhotoCurrentQuad_mA = double.NegativeInfinity;

            // Unlocked frequency
            if (Measurements.Wavemeter != null
                && Measurements.Wavemeter.IsOnline)
            {
                freqGHz = Measurements.ReadFrequency_GHz();
            }
            else
            {
                freqReference_Ghz = midTempData.GetValueDouble(EnumTosaParam.Freq_GHz);
                freqGHz = DsdbrTuning.ReadFreq_GhzViaLowCostMeter(freqReference_Ghz);
            }
            freqReference_Ghz = freqGHz;

            // Call soft-lock routine
            engine.SendToGui(new GuiTextMessage("Locking frequency", 1));

            Bookham.TestSolution.IlmzCommonUtils.DsdbrUtils.LockerCurrents MidLockercurrent;
            MidLockercurrent = DsdbrUtils.ReadLockerCurrents();
            itxLockReference_mA = MidLockercurrent.TxCurrent_mA;
            irxLockReference_mA = MidLockercurrent.RxCurrent_mA;
            lockRatio = MidLockercurrent.LockRatio;

            // MZ stuff
            //switchOsaMzOpm.SetState(Switch_Osa_MzOpm.State.MzOpm);
            #region To do LV sweep
            bool doLVSweep = false;

            IInstType_DigitalIO ctap = Optical_switch.GetDigiIoLine(IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine);//3
            ctap.LineState = IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine_State;//true

            if (doLVSweep)
            {
                double Vcm = midTempMzSettings.LeftArmMod_Quad_V + midTempMzSettings.RightArmMod_Quad_V;
                int NumberOfSteps = (int)(1 + Math.Abs(Vcm / (mzLvStepSize / 1000)));
                mzInstrs.PowerMeter.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_mW;

                //Alice.Huang    2010-08-31
                // set power meter range to its initial range before any sweep 
                mzInstrs.PowerMeter.Range = configData.ReadDouble("OpmRangeInit_mW");

                engine.SendToGui(new GuiTextMessage("MZ LV Scan", 1));

                // MZ LV Scan
                ILMZSweepResult lvSweepData;
                double minLevel_dBm = -60;
                bool dataOk = true;
                
                Measurements.FrequencyWithoutMeter = freqReference_Ghz;
                do
                {
                    lvSweepData = mzDriverUtils.ModArm_DifferentialSweepByTrigger(
                    midTempMzSettings.LeftArmImb_mA / 1000, midTempMzSettings.RightArmImb_mA / 1000,
                     Vcm, 0.0,
                    NumberOfSteps, MZSourceMeasureDelay_ms, inlineTap_V, compTap_V, ArmSourceByAsic);

                    // If overrange run again
                    if (MzAnalysisWrapper.CheckForOverrange(lvSweepData.SweepData[ILMZSweepDataType.FibrePower_mW]))
                    {
                        //modify by tim for NaN at 2008-09-05; 
                        dataOk = false;
                        if (double.IsNaN(mzInstrs.PowerMeter.Range))
                            mzInstrs.PowerMeter.Range = 1;
                        else
                            mzInstrs.PowerMeter.Range = mzInstrs.PowerMeter.Range * 10;
                        //End
                    }
                    else
                    {
                        // if underrange fix the data and continue
                        dataOk = true;
                        if (MzAnalysisWrapper.CheckForUnderrange(lvSweepData.SweepData[ILMZSweepDataType.FibrePower_mW]))
                        {
                            lvSweepData.SweepData[ILMZSweepDataType.FibrePower_mW] =
                                MzAnalysisWrapper.FixUnderRangeData(lvSweepData.SweepData[ILMZSweepDataType.FibrePower_mW]
                                , minLevel_dBm);
                        }
                    }
                }
                while (!dataOk);

                // store plots
                string LVFileName = Util_GenerateFileName.GenWithTimestamp(mzFileDirectory,
                    "FinalDiffMzLV_" + freqReference_Ghz.ToString() + "_" + midTempData.Priority.ToString(), "HG100000.000", "csv");
                // write to file
                MzSweepFileWriter.WriteSweepData(LVFileName, lvSweepData,
                    new ILMZSweepDataType[] { ILMZSweepDataType.LeftArmModBias_V,
                                        ILMZSweepDataType.RightArmModBias_V,
                                        ILMZSweepDataType.LeftMinusRight,
                                        ILMZSweepDataType.FibrePower_mW,
                                        ILMZSweepDataType.TapComplementary_mA});
                engine.SendToGui(lvSweepData);

                MzAnalysisWrapper.MzAnalysisResults mzAnlyDiffV =
                    MzAnalysisWrapper.Differential_NegChirp_DifferentialSweep(lvSweepData, Vcm / 2, 0);

                engine.SendToGui(lvSweepData);
                newMzSetup.LeftArmMod_Peak_V = mzAnlyDiffV.Max_SrcL;
                newMzSetup.RightArmMod_Peak_V = mzAnlyDiffV.Max_SrcR;

                // Record actual peak voltages and change in voltages;
                double mzModChange_V;
                mzModChange_V = newMzSetup.LeftArmMod_Peak_V - midTempMzSettings.LeftArmMod_Peak_V;
                mzModChange_V = newMzSetup.RightArmMod_Peak_V - midTempMzSettings.RightArmMod_Peak_V;

                // MZ peak open loop measurements (tap not power levelled)
                mzInstrs.PowerMeter.Range = InstType_OpticalPowerMeter.AutoRange;

                engine.SendToGui(new GuiTextMessage("MZ peak measurements", 1));
                IlMzDriverUtils.SetupMzToPeak(mzInstrs.FCU2Asic, midTempMzSettings);
                Thread.Sleep(250);
                double power_dBm = Measurements.ReadOpticalPower(OpticalPowerHead.MZHead, Measurements.PowerUnits.dBm);
                double powerChange_dB = power_dBm - powerPeakReference_dBm;
                IInstType_DigitalIO ctapLine = Optical_switch.GetDigiIoLine(IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine);//3
                ctapLine.LineState = IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine_State;//true


                // MZ quad open loop measurements (tap not power levelled)
                engine.SendToGui(new GuiTextMessage("MZ quad point measurements", 1));
                IlMzDriverUtils.SetupMzToQuad(mzInstrs.FCU2Asic, midTempMzSettings);
                Thread.Sleep(250);
                power_dBm = Measurements.ReadOpticalPower(OpticalPowerHead.MZHead, Measurements.PowerUnits.dBm);
                powerChange_dB = power_dBm - powerQuadReference_dBm;
            }
            #endregion To do LV sweep

            bool getMidPwr = true;
            if (getMidPwr)
            {
                //Set PowerMeter wavelength
                Measurements.FrequencyWithoutMeter = freqGHz;

                // MZ quad closed loop measurements (tap power levelled)
                engine.SendToGui(new GuiTextMessage("MZ quad point measurements", 1));
                //mzDriverUtils.SetupMzToQuad(newMzSetup);
                IlMzDriverUtils.SetupMzToQuad(mzInstrs.FCU2Asic, midTempMzSettings);
                Thread.Sleep(250);

                // tune SOA power / power level to tap
                engine.SendToGui(new GuiTextMessage("Get the powerQuadReference_dBm", 1));

                powerQuadReference_dBm = Measurements.ReadOpticalPower(OpticalPowerHead.MZHead, Measurements.PowerUnits.dBm);

                IInstType_DigitalIO cctap = Optical_switch.GetDigiIoLine(IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine);//3
                cctap.LineState = IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine_State;//true
                tapCompPhotoCurrentQuad_mA = mzInstrs.FCU2Asic.ICtap_mA;

                // MZ peak closed loop measurements (tap power levelled)
                engine.SendToGui(new GuiTextMessage("MZ peak measurements", 1));
                IlMzDriverUtils.SetupMzToPeak(mzInstrs.FCU2Asic, midTempMzSettings);
                Thread.Sleep(250);

                powerPeakReference_dBm = Measurements.ReadOpticalPower(OpticalPowerHead.MZHead, Measurements.PowerUnits.dBm);
                tapCompPhotoCurrentPeak_mA = mzInstrs.FCU2Asic.ICtap_mA;
            }

            // Set MZ to peak
            IlMzDriverUtils.SetupMzToPeak(mzInstrs.FCU2Asic, midTempMzSettings);
            Thread.Sleep(250);

            // Record TEC volts and currents, and total power
            engine.SendToGui(new GuiTextMessage("Electrical power measurements", 1));
            Measurements.PowerDissResults powerResult = Measurements.ElecPowerDissipation(
                dsdbrSetup.FrontPair, dsdbrTec, mzTec);

            try
            {
                midTempData.SetValueDouble(EnumTosaParam.Freq_GHz, freqReference_Ghz);
                midTempData.SetValueDouble(EnumTosaParam.FibrePwrPeak_dBm, powerPeakReference_dBm);
                midTempData.SetValueDouble(EnumTosaParam.FibrePwrQuad_dBm, powerQuadReference_dBm);
                midTempData.SetValueDouble(EnumTosaParam.ItxLock_mA, itxLockReference_mA);
                midTempData.SetValueDouble(EnumTosaParam.IrxLock_mA, irxLockReference_mA);
                midTempData.SetValueDouble(EnumTosaParam.LockRatio, lockRatio);
                midTempData.SetValueDouble(EnumTosaParam.TapCompPhotoCurrentPeak_mA, tapCompPhotoCurrentPeak_mA);//midTempData.GetValueDouble(EnumTosaParam.TapCompPhotoCurrentPeak_mA));
                midTempData.SetValueDouble(EnumTosaParam.TapCompPhotoCurrentQuad_mA, tapCompPhotoCurrentQuad_mA);//,midTempData.GetValueDouble(EnumTosaParam.TapCompPhotoCurrentQuad_mA));                
                midTempData.SetValueDouble(EnumTosaParam.PhaseRatioSlopeEff, midTempData.GetValueDouble(EnumTosaParam.PhaseRatioSlopeEff));                
            }
            catch
            {
                engine.SendToGui(new GuiTextMessage("Recheck the Mid temperature Unmeasurement(s) missing some parameters!", 0));
                return false; // Indicating MidTempTestChannel ran unsuccessfully
            }

            return true; // Indicating MidTempTestChannel ran OK
        }

        public Type UserControl
        {
            get { return (typeof(IlmzChannelCharGui)); }
        }

        #endregion

        #region Private data

        private int ChanTestNumber = 0;
        // Instruments
        IInstType_TecController dsdbrTec;
        IInstType_TecController mzTec = null;
        Inst_Ke2510 Optical_switch = null;

        //Switch_Osa_MzOpm switchOsaMzOpm;

        // MZ data
        IlMzDriverUtils mzDriverUtils;
        double inlineTap_V;
        double compTap_V;

        double mzLvStepSize;
        int MZSourceMeasureDelay_ms = 5;
        // Tap tuning
        double tapPhotoCurrentToleranceRatio;

        // EOL 
        const int eolTempSettlingTime_s = 15;

        const int numRthTries = 10;
        const int rthDelay_mS = 500;
        const int numLockedMeasTries = 2;
        const int lockedMeasDelay_mS = 250;
        bool ArmSourceByAsic = true;
        double rthTolerance_ohm = 10;

        string mzFileDirectory;

        #endregion
    }
}


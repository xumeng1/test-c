﻿// [Copyright]
//
// Bookham.TestSolution.TestModules
//
// Mod_PinCheck.cs
//
// Author: purney.xie, 2011, chongjian.liang 2012
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestSolution.IlmzCommonInstrs;
using Bookham.TestLibrary.Instruments;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Test Module Template.
    /// </summary>
    public class Mod_PinCheck : ITestModule
    {
        private Inst_Fcu2Asic Fcu2Asic;
        private IInstType_TecController TecDsdbr;
        private InstType_ElectricalSource ASIC_VccSource;
        private InstType_ElectricalSource ASIC_VeeSource;
        private InstType_ElectricalSource FCUMKI_Source;

        private double Temperature_LowLimit_Degree;
        private double Temperature_HighLimit_Degree;

        private DatumList returnData = new DatumList();
        private bool isRedoPinCheckRequired = false;
        private int runCount = 0;

        /// <summary>
        /// Initialize parameters and instruments
        /// </summary>
        /// <param name="configData">parameters</param>
        /// <param name="instruments">instruments</param>
        private void Initialize(DatumList configData, InstrumentCollection instruments)
        {
            Temperature_LowLimit_Degree = configData.ReadDouble("Temperature_LowLimit_Degree");
            Temperature_HighLimit_Degree = configData.ReadDouble("Temperature_HighLimit_Degree");

            Fcu2Asic = (Inst_Fcu2Asic)instruments["Fcu2Asic"];
            TecDsdbr = (IInstType_TecController)instruments["TecDsdbr"];
            ASIC_VccSource = (InstType_ElectricalSource)instruments["ASIC_VccSource"];
            ASIC_VeeSource = (InstType_ElectricalSource)instruments["ASIC_VeeSource"];

            if (instruments.Contains("FCUMKI_Source"))
            {
                FCUMKI_Source = (InstType_ElectricalSource)instruments["FCUMKI_Source"];
            }
        }

        /// <summary>
        /// True: Redo pin check; 
        /// False: Fail pin check
        /// </summary>
        /// <returns></returns>
        private bool IsRedoPinCheck(string pinCheckInfo, string pinCheckInfo_chinese, ITestEngine engine)
        {
            // Close all related instruments when pin check failed
            if (FCUMKI_Source != null)
            {
                FCUMKI_Source.OutputEnabled = false;
            }
            ASIC_VccSource.OutputEnabled = false;
            ASIC_VeeSource.OutputEnabled = false;

            // Fail Pin check after redoing pin check 3 times
            if (runCount++ == 3)
            {
                engine.ShowContinueUserQuery("\nTest failed due to pin check failed; click Continue to finish the test\n\n由于pin check不过关，测试失败，点击 Continue 完成测试");

                returnData.AddBool("Pin_Check_Pass", false);
                returnData.AddString("ErrorInformation", "Pin check failed on " + pinCheckInfo);

                return false;
            }
            else // Redo pin check
            {
                isRedoPinCheckRequired = true;

                engine.ShowContinueUserQuery("Pin check failed on " + pinCheckInfo + " , please reload DUT and retry\n\n" + pinCheckInfo_chinese + "，请重新取放器件后，点击 Continue 重试");

                return true;
            }
        }

        #region ITestModule Members

        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, DatumList configData, InstrumentCollection instruments, ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {
            // Initialize parameters and instruments
            Initialize(configData, instruments);

            #region Redo pin check 3 times while pin check failed -- chongjian.liang 2012.8.24
            do
            {
                // If pin check pass all the way, isRedoPinCheckRequired will be false, exit the while loop
                isRedoPinCheckRequired = false;

                #region Check TecDsdbr temperature in room temperature

                // If TecDsdbr is enabled, close it to let it cool down the room temperature
                if (TecDsdbr.OutputEnabled)
                {
                    TecDsdbr.OutputEnabled = false;
                }

                if (FCUMKI_Source != null)
                {
                    FCUMKI_Source.OutputEnabled = true;
                }

                ASIC_VccSource.OutputEnabled = true;
                ASIC_VeeSource.OutputEnabled = true;

                Thread.Sleep(5000);

                // Get the tecDsdbrTemperature in room temperature
                double tecDsdbrTemperature = TecDsdbr.SensorTemperatureActual_C;

                // If TecDsdbr temperature not in limits, check if fail the test or redo pin check
                if (tecDsdbrTemperature > Temperature_HighLimit_Degree || tecDsdbrTemperature < Temperature_LowLimit_Degree)
                {
                    // Redo pin check
                    if (IsRedoPinCheck("DUT_TEC_Sensor_Pin", "DUT_TEC_Sensor_Pin未能正确测量温度", engine))
                    {
                        continue;
                    }
                    else // Fail the module as pin check fails 3 times
                    {
                        return returnData;
                    }
                }
                #endregion

                #region Check Vcc connection
                Thread.Sleep(500);
                double VccSourceCurrent = ASIC_VccSource.CurrentActual_amp;
                
                if (VccSourceCurrent == 0 || VccSourceCurrent > 0.005)
                {
                    // Redo pin check
                    if (IsRedoPinCheck("Vcc_Power_Source", "Asic工作电流异常", engine))
                    {
                        continue;
                    }
                    else // Fail the module as pin check fails 3 times
                    {
                        return returnData;
                    }
                }
                #endregion

                #region Check ASIC comunication

                if (!Fcu2Asic.CloseAllAsic().ToLower().Trim().EndsWith("ok"))
                {
                    // Redo pin check
                    if (IsRedoPinCheck("FCU comunicating with ASIC", "FCU与Asic通讯异常", engine))
                    {
                        continue;
                    }
                    else // Fail the module as pin check fails 3 times
                    {
                        return returnData;
                    }
                }
                #endregion 

                #region Check Vee Connection
                Fcu2Asic.ISoa_Dac = 0;//add a negtive current to SOA.jack.zhang 2013-05-03
                Thread.Sleep(500);
                double VeeSourceCurrent = ASIC_VeeSource.CurrentActual_amp;

                if (VeeSourceCurrent <0.0001)//the default current is about 0.1 mA if set Isao=0mA. jack.zhang 2013-05-03
                {
                    // Redo pin check
                    if (IsRedoPinCheck("Vee_Power_Source", "Vee工作电流异常", engine))
                    {
                        continue;
                    }
                    else // Fail the module as pin check fails 3 times
                    {
                        return returnData;
                    }
                }
                #endregion Check Vee Connection

            } while (isRedoPinCheckRequired);
            Fcu2Asic.CloseAllAsic();
            #endregion
            
            returnData.AddBool("Pin_Check_Pass", true);

            return returnData;
        }

        public Type UserControl
        {
            get { return (typeof(Mod_PinCheckGui)); }
        }

        #endregion
    }
}
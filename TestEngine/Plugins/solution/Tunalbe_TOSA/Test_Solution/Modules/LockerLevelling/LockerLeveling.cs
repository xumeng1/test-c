// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// PowerLevellingandITUTuning.cs
//
// Author: Echoxl.wang,2010
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestSolution.IlmzCommonUtils;
using Bookham.TestSolution.Instruments;
using Bookham.TestLibrary.InstrTypes;
using System.IO;
using Bookham.TestLibrary.Utilities;
using Bookham.Solution.Config;
using System.Collections.Specialized;
using Bookham.TestLibrary.Instruments;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Test Module Template.
    /// </summary>
    public class LockerLeveling : ITestModule
    {
        #region private data
        Inst_Fcu2Asic fcu = null;
        ITestEngine LockerLevelEngine = null;
        double locker_Slope = 0; // initial locker slope

        string work_dir ="";
        string laser_id = "";
        string timestamp = "";

        int numberOfSupermodes = 0;
        float Iphase_lockLevel_mA = 0;
        double irx_target_mA=0;
        //double irx_tolerance_percent = 0;
        double Min_LockerCurrent_mA = 0;
        double Max_LockerCurrent_mA = 0;
        double Irearsoa_min_mA = 0;
        double Irearsoa_max_mA = 0;
        float Irearsoa_mA = 0;

        private List<double> slope_rearsoa = new List<double>();
        List<double> constant_rear_rearsoa = new List<double>();

        double[,] slope_rear_rearsoa;

        TestParamConfigAccessor MapSettings = null;
        StreamWriter sw=null;

        #endregion

        #region ITestModule Members

        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, DatumList configData, InstrumentCollection instruments, ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {

            // TODO - add your code here!
            engine.GuiToFront();
            engine.GuiShow();
            LockerLevelEngine = engine;

            #region initialize instrument and parameters

            MapSettings = (TestParamConfigAccessor)configData.ReadReference("Mapping_Settings");

            work_dir = configData.ReadString("RESULT_DIR");
            laser_id = configData.ReadString("LASER_ID");
            timestamp = configData.ReadString("TIME_STAMP");
            string LockerFile = "LockerLeveling_" + laser_id + "_" + timestamp + ".csv";
            string LockerLevelingFile = Path.Combine(work_dir, LockerFile);
            numberOfSupermodes = configData.ReadSint32("supermodeNumber");
            slope_rear_rearsoa = new double[numberOfSupermodes, 2];
            Iphase_lockLevel_mA = float.Parse(MapSettings.GetStringParam("IphaseForLockerLevel_mA"));
            //Irearsoa_mA = float.Parse(MapSettings.GetStringParam("IrearsoaForMapping_mA"));
            Inst_Ke2510 opticalSwitch = (Inst_Ke2510)instruments["Optical_switch"];
            fcu = (Inst_Fcu2Asic)instruments["Fcu2Asic"];
            if (fcu.IsOnline == false)
            {
                fcu.IsOnline = true;
            }

            locker_Slope = double.Parse(MapSettings.GetStringParam("LockerCurrent_Slope"));
            Max_LockerCurrent_mA = configData.ReadDouble("Max_LockerCurrent_mA");
            Min_LockerCurrent_mA = configData.ReadDouble("Min_LockerCurrent_mA");
            irx_target_mA = (Max_LockerCurrent_mA + Min_LockerCurrent_mA)/2.0;//configData.ReadDouble("RX_TARGET_CURRENT");
            double LR_DUT = configData.ReadBool("GetPrePostDataFileOK") ? Math.Max(FreqCalByEtalon.DUTLockerRatioSlope_50GHz.YIntercept, FreqCalByEtalon.DUTLockerRatioSlope_100GHz.YIntercept)
                                                             : 2.0;//set DUT locker ratio to 0.5 as default if no avilable post and pre buring data.(ex. diff DUT temp between pre/post and mapping; old pre/post data)jack.zhang 2012-11-23
            irx_target_mA = irx_target_mA * LR_DUT;//LR_DUT=Rx/Etalon/,Etalon=Min_LR_Current, Rx=Min_LR_Current*LR. jack.zhang 2012-11-08
            Max_LockerCurrent_mA = Max_LockerCurrent_mA * LR_DUT;
            Min_LockerCurrent_mA = Min_LockerCurrent_mA * LR_DUT;//now we do not know the freq.so just meet the locker refer target.jack.zhang 2013-04-16
            Irearsoa_min_mA = configData.ReadDouble("MIN_REARSOA");
            Irearsoa_max_mA = configData.ReadDouble("MAX_REARSOA");
            DsdbrUtils.opticalSwitch = opticalSwitch;
             #endregion

            #region do locker leveling on two SM

            sw = new StreamWriter(LockerLevelingFile);
            for (int i = 0; i < numberOfSupermodes; i++)
            {
                //if (i == 0)
                //    Irearsoa_max_mA += 10;// Vito Hu suggest add the 10mA offset just for SM0 due to lower locker power for low frequency.

                doLockerLeveling(i);

                //if (i == 0)
                //    Irearsoa_max_mA -= 10;// Vito Hu suggest add the 10mA offset just for SM0 due to lower locker power for low frequency.
            }
            sw.Close();
            #endregion

            #region generate return data
            // return data
            DatumList returnData = new DatumList();
            //returnData.AddDouble("itx_target_mA", itx_target_mA);
            returnData.AddReference("slope_rear_rearsoa", slope_rear_rearsoa);

            return returnData;
            #endregion
        }


        private void doLockerLeveling(int smNbr)
        {
            //read current from SM3 middle line file
            string sub_SMFile = "Im_DSDBR01_" + laser_id + "_" + timestamp + "_SM"+smNbr.ToString()+".csv";

            string SMCurrentFile = Path.Combine(work_dir, sub_SMFile);
            if (!File.Exists(SMCurrentFile))
            {
                LockerLevelEngine.RaiseNonParamFail(0, "No SM" + smNbr.ToString() + " middle line file. Program will Exist!");
            }
            CsvReader cr = new CsvReader();
            List<string[]> lines = cr.ReadFile(SMCurrentFile);
            double Temp_slope_rearsoa = 0;
            double Temp_constant_rear_rearsoa = 0;
            int retry_count = 0;
            int Max_retry_count = 3;

            if (lines.Count < 3)
            {
                LockerLevelEngine.RaiseNonParamFail(0, "invalid Super mode middle line file!");
            }
            do
            {
                Irearsoa_mA = (float)Irearsoa_min_mA; ;
                //do locker current leveling on first point in sm(N) middle line
                PowerLevelReturnDataType result_first = doLockerCurrentLeveling(lines[1], sw);

                sw.WriteLine("SuperMode" + smNbr.ToString() + " First point locker leveling result is :");
                sw.WriteLine("Rear:" + result_first.Irear.ToString() + " mA , rearsoa: " + result_first.Irearsoa.ToString() + " mA, locker:" + result_first.Irx.ToString());
                sw.Flush();

                //do locker current leveling on last point in sm(N) middle line
                PowerLevelReturnDataType result_last = new PowerLevelReturnDataType();
                if (result_first.Irearsoa < Irearsoa_max_mA)
                {
                    int Max_Irear_tuning_count = 20;
                    int i_Step = (lines.Count - 1) / Max_Irear_tuning_count;
                    int index_2 = lines.Count - 1;
                    double LastGoodDeltIrx_mA = 999;
                    List<PowerLevelReturnDataType> result_List = new List<PowerLevelReturnDataType>();
                    PowerLevelReturnDataType result_1 = new PowerLevelReturnDataType();
                    PowerLevelReturnDataType result_2 = new PowerLevelReturnDataType();
                    for (int index_1 = lines.Count - 1; index_1 > 0; index_1 -= i_Step)
                    {
                        bool EndTuning = false;
                        Irearsoa_mA = (float)Irearsoa_max_mA;
                        result_1 = doLockerCurrentLeveling(lines[index_1], sw);
                        result_1.Irear_Index=index_1;
                        result_List.Add(result_1);

                        if (result_1.Irx >= Min_LockerCurrent_mA
                        && (result_1.Irearsoa > Min_LockerCurrent_mA || index_1 == lines.Count - 1))
                        {
                            //if Irx meet the target when irearsoa close to Irearsoa_max_mA or at the last Irear current. jack.zhang 2013-03-18
                            result_last = result_1;
                            break;
                        }
                        else
                        {
                            double target_rear_mA;
                            double i_target = lines.Count -1- i_Step;
                            if (smNbr > 0)
                            {
                                target_rear_mA = (Irearsoa_max_mA - slope_rear_rearsoa[smNbr - 1, 1]) / slope_rear_rearsoa[smNbr - 1, 0];
                                for (int n = index_1-2; n > 0; n--)
                                {
                                    string Irear_1 = lines[n][0];
                                    string Irear_2 = lines[n+1][0];
                                    if ((target_rear_mA - double.Parse(Irear_1)) >= 0)
                                    {
                                        i_target = Math.Abs(target_rear_mA - double.Parse(Irear_1)) > Math.Abs(target_rear_mA - double.Parse(Irear_2)) ? n + 1 : n;
                                        break;
                                    }
                                }
                            }
                            if (result_List.Count > 1)
                            {
                                double Low_delt_Irx_mA = -999;
                                double High_delt_Irx_mA = 999;
                                foreach (PowerLevelReturnDataType item in result_List)
                                {
                                    double delt_Irx_mA = item.Irx_At_MaxIrearsoa - irx_target_mA;
                                    if (delt_Irx_mA < 0 && delt_Irx_mA > Low_delt_Irx_mA)
                                    {
                                        Low_delt_Irx_mA = delt_Irx_mA;
                                        result_1 = item;
                                    }
                                    if (delt_Irx_mA > 0 && delt_Irx_mA < High_delt_Irx_mA)
                                    {
                                        High_delt_Irx_mA = delt_Irx_mA;
                                        result_2 = item;
                                    }
                                }
                                if (result_1.Irear_Index == result_2.Irear_Index)
                                    i_target = result_1.Irear_Index - i_Step;
                                else
                                    i_target = (result_2.Irear_Index - result_1.Irear_Index) * (irx_target_mA - result_1.Irx_At_MaxIrearsoa) / (result_2.Irx_At_MaxIrearsoa - result_1.Irx_At_MaxIrearsoa) + result_1.Irear_Index;
                                
                                i_target =Math.Round( i_target ,0);
                                i_target = Math.Min(i_target, lines.Count - 1);
                                i_target = Math.Max(i_target, 1);//Irear[0]has been test as result_frist.
                                 
                            }
                            //if (index_1 < lines.Count - 1)//if not the result_1 is null.jack.zhang 2013-03-20 
                            //{
                            //    i_target = (index_2 - index_1) * (irx_target_mA - result_1.Irx_At_MaxIrearsoa) / (result_2.Irx_At_MaxIrearsoa - result_1.Irx_At_MaxIrearsoa) + index_1;
                            //    i_target = Math.Min(i_target, lines.Count - 1);
                            //    i_target = Math.Max(i_target, 1);//Irear[0]has been test as result_frist.
                            //}
                            result_2 = result_1;
                            index_2 = index_1;
                            //the new index can not be great than current index
                            foreach (PowerLevelReturnDataType item in result_List)
                            {
                                if (item.Irear_Index == i_target)
                                    EndTuning=true;
                            }
                            index_1 = (int)Math.Round(i_target + i_Step, 0);//for i need minus step for next. jack.zhang 2012-11-09
                        }
                        if (EndTuning)
                            break;
                    }
                    //find the last good high Irear. jack.zhang 2013-04-08
                    foreach (PowerLevelReturnDataType item in result_List)
                    {
                        double delt_Irx_mA = Math.Abs(item.Irx_At_MaxIrearsoa - irx_target_mA);
                        if (delt_Irx_mA < LastGoodDeltIrx_mA)
                            result_last = item;
                    }
                }
                else
                {
                    result_last.Irear = double.Parse(lines[lines.Count - 1][0]);
                    result_last.Irearsoa = Irearsoa_max_mA;
                    result_last.lockercurrent_slope = 0;
                    result_last.Irx = 0;
                }
                sw.WriteLine("last point locker leveling result is :");
                sw.WriteLine("Rear:" + result_last.Irear.ToString() + " mA , rearsoa: " + result_last.Irearsoa.ToString() + " mA, locker:" + result_last.Irx.ToString());
                sw.Flush();

                if (result_first.Irear != result_last.Irear)
                {
                    Temp_slope_rearsoa = (result_last.Irearsoa - result_first.Irearsoa) / (result_last.Irear - result_first.Irear);
                    Temp_constant_rear_rearsoa = result_last.Irearsoa - Temp_slope_rearsoa * result_last.Irear;
                }
                LockerLevelEngine.SendStatusMsg("SuperMode" + smNbr.ToString() + " rearsoa_slope is :" + Temp_slope_rearsoa.ToString());
                LockerLevelEngine.SendStatusMsg("SuperMode" + smNbr.ToString() + " constant parameter of slope is :" + Temp_constant_rear_rearsoa.ToString());

            }
            while (Temp_slope_rearsoa < 0 && retry_count++ < Max_retry_count);//retry locker leveling when slope is negtive due to the last Irear too close to the frist Irear. jack.zhang 2013-03-18
            sw.WriteLine("");
            sw.WriteLine("SuperMode" + smNbr.ToString() + " rearsoa_slope is :" + Temp_slope_rearsoa.ToString());
            sw.WriteLine("SuperMode" + smNbr.ToString() + " constant parameter of slope is :" + Temp_constant_rear_rearsoa.ToString());
            sw.WriteLine("");

            if (Temp_slope_rearsoa < 0)
            {
                sw.Close();
                LockerLevelEngine.RaiseNonParamFail(0, "SM" + smNbr.ToString() + " Irear_rearsoa_Slope is negtive!");
            }

            slope_rear_rearsoa[smNbr, 0] = Temp_slope_rearsoa;
            slope_rear_rearsoa[smNbr, 1] = Temp_constant_rear_rearsoa;
        }


        private PowerLevelReturnDataType doLockerCurrentLeveling(string[] currents,StreamWriter SW)
        {
            SetFcu(currents,Irearsoa_mA);
            PowerLevelReturnDataType powerlevelresult = new PowerLevelReturnDataType();
            double Irx_mA = DsdbrUtils.ReadLockerCurrents().RxCurrent_mA;
            if (Irearsoa_mA == Irearsoa_max_mA)
                powerlevelresult.Irx_At_MaxIrearsoa = Irx_mA;
            double Irx_mA_new=Irx_mA;
            double Irearsoa_mA_new = Irearsoa_mA;
            double lockercurrent_slope = locker_Slope;
            double lockercurrent_slope_new = locker_Slope;
            double Delt_Irx_mA = 999;
            double Irearsoa_mA_LastGood = Irearsoa_mA;
            double Irx_mA_LastGood = Irx_mA;
            int TuneCount = 0;
            int Max_tune_count = 20;

            SW.WriteLine("rear is :" + currents[0].ToString());
            SW.WriteLine("Phase is :" + Iphase_lockLevel_mA.ToString());
            SW.WriteLine("frontPairNbr is :" + currents[1].ToString());
            SW.WriteLine("fsNonConstant is:" + currents[3].ToString());
            sw.WriteLine("Rear, Rearsoa,LockerRx,LockerSlope");
            SW.WriteLine(currents[0].ToString() + "," + Irearsoa_mA.ToString() + "," + Irx_mA.ToString()+","+lockercurrent_slope.ToString());

            while ((Irx_mA_new > Max_LockerCurrent_mA || Irx_mA_new <Min_LockerCurrent_mA)
                    && TuneCount < Max_tune_count)
            {
                Irx_mA=Irx_mA_new;
                Irearsoa_mA = (float)Irearsoa_mA_new;
                double Delta_Irearsoa = (irx_target_mA - Math.Abs(Irx_mA)) / lockercurrent_slope;

                Irearsoa_mA_new = Irearsoa_mA + Delta_Irearsoa;
                Irearsoa_mA_new = Math.Max(Irearsoa_min_mA, Irearsoa_mA_new);
                
                Irearsoa_mA_new = Math.Min(Irearsoa_max_mA, Irearsoa_mA_new);
                if (Irearsoa_mA_new == Irearsoa_mA) break;

                //SetFcu(currents, float.Parse(Irearsoa_mA_new.ToString()));
                fcu.IRearSoa_mA = Irearsoa_mA_new;
                System.Threading.Thread.Sleep(5);
                Irx_mA_new = DsdbrUtils.ReadLockerCurrents().RxCurrent_mA;

                if (Irearsoa_mA_new == Irearsoa_max_mA)
                    powerlevelresult.Irx_At_MaxIrearsoa = Irx_mA_new;

                double Delt_Irx_mA_new =Math.Abs(irx_target_mA - Irx_mA_new);
                if (Delt_Irx_mA_new < Delt_Irx_mA)//save the lastgood point which delt locker current  is close to zero.Jack.zhang 2013-03-14
                {
                    Delt_Irx_mA = Delt_Irx_mA_new;
                    Irearsoa_mA_LastGood = Irearsoa_mA_new;
                    Irx_mA_LastGood = Irx_mA_new;
                }
                lockercurrent_slope_new = (Irx_mA - Irx_mA_new) / (Irearsoa_mA - Irearsoa_mA_new);
                if (lockercurrent_slope_new > 0)//to avoid the wrong cal Irearsoa come from Itx measured error between two close points. jack.zhang 2012-11-09
                    lockercurrent_slope = lockercurrent_slope_new;
                SW.WriteLine(currents[0].ToString() + "," + Irearsoa_mA_new.ToString() + "," + Irx_mA_new.ToString() + "," + lockercurrent_slope.ToString());
                TuneCount++;
            } 
            
            SW.Flush();

            locker_Slope = lockercurrent_slope;
            powerlevelresult.Irear = double.Parse(currents[0]);
            powerlevelresult.Irearsoa = Irearsoa_mA_LastGood;
            powerlevelresult.lockercurrent_slope = lockercurrent_slope;
            powerlevelresult.Irx = Irx_mA_LastGood;
            return powerlevelresult;

        }

        private double MeasureLockerRx()
        {
            double IRX_mA = fcu.Locker_Rx_mA(true, true, false);
            return IRX_mA;
        }

        private void SetFcu(string[] currents,float rear_soa_mA)
        {
            fcu.IRear_mA = float.Parse(currents[0]);
            fcu.IPhase_mA = Iphase_lockLevel_mA;
            fcu.IRearSoa_mA= rear_soa_mA;
            System.Threading.Thread.Sleep(1000);//rearsoa current change between max and min current. so need delay. Jack,zhang 2012-04-09

            int frontPairNbr = int.Parse(currents[1]);
            float fs_constant = float.Parse(currents[2]);
            float fs_nonConstant = float.Parse(currents[3]);

            fcu.SetFrontPairCurrent_mA(frontPairNbr, fs_constant, fs_nonConstant);
        }

        private struct PowerLevelReturnDataType
        {
            public double Irx_At_MaxIrearsoa;
            public double Irearsoa;
            public double Irx;
            public double lockercurrent_slope;
            public double Irear;
            public int Irear_Index;
        }
       
        public Type UserControl
        {
            get { return (typeof(LockerLevelingGui)); }
        }

        #endregion
    }
}

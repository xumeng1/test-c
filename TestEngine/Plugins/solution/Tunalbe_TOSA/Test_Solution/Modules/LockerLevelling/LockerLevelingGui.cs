// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// PowerLevellingandITUTuningGui.cs
//
// Author: purney.xie, 2009
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Module Template GUI User Control
    /// </summary>
    public partial class LockerLevelingGui : Bookham.TestEngine.Framework.Messages.ManagedCtlBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public LockerLevelingGui()
        {
            /* Call designer generated code. */
            InitializeComponent();

            /* TODO - Write constructor code here. */
        }

        /// <summary>
        /// Incoming message event handler
        /// </summary>
        /// <param name="payload">Message Payload</param>
        /// <param name="inMsgSeq">Incoming message sequence number</param>
        /// <param name="respSeq">Outgoing message sequence number</param>
        private void ModuleGui_MsgReceived(object payload, long inMsgSeq, long respSeq)
        {
            //if (payload.GetType() == typeof(ProgressValueMax))
            //{
            //    PLTprogressBar.Maximum = ((ProgressValueMax)payload).max;
            //    PLTprogressBar.Value = ((ProgressValueMax)payload).value;
            //}
        }
    }
}

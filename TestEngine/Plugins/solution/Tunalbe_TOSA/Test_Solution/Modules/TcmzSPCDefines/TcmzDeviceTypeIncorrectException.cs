using System;
using System.Collections.Generic;
using System.Text;

namespace TCMZDefinitions
{
    public class TcmzDeviceTypeIncorrectException : TcmzException
    {
        private string _deviceType;

        /// <summary>
        /// Constructor for TcmzDeviceTypeIncorrectException
        /// </summary>
        /// <param name="deviceType">Incorrect Device Type</param>
        public TcmzDeviceTypeIncorrectException(string deviceType)
        {
            this._deviceType = deviceType;
        }

        public string DeviceType
        {
            get
            {
                return this._deviceType;
            }
        }
    }
}

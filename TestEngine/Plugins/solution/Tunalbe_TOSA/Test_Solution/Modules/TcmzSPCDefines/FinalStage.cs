using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.Framework.InternalData;
using Util_CsvFileAccess;
using Bookham.TestSolution.TcmzCommonData;

namespace TcmzFinalStage
{
    /// <summary>
    /// data infeed from final stage.
    /// </summary>
    public class FinalStageData
    {
        public FinalStageData(DatumList dl)
        {
            //read channel pass file
            this._channelPassFileName = dl.ReadFileLinkFullPath("CHANNEL_PASS_FILE");
            CSVDbReader reader = new CSVDbReader(this._channelPassFileName);

            // skip two lines for limits.
            reader.Skip(2);
            while (reader.Read())
            {
                FinalChannelInstance channel = new FinalChannelInstance(reader);
                this._channels.Add(channel);
            }
            reader.Close();

            this._channelFailFileName = dl.ReadFileLinkFullPath("CHANNEL_FAIL_FILE");
            ////read channel failed file
            //reader = new CSVDbReader(filePath);

            //// skip two lines for limits.
            //reader.Skip(2);
            //while (reader.Read())
            //{
            //    FinalChannelInstance channel = new FinalChannelInstance(reader);
            //    this._channels.Add(channel);
            //}
        }

        private List<FinalChannelInstance> _channels = new List<FinalChannelInstance>();
        private string _channelPassFileName;
        private string _channelFailFileName;

        public FinalChannelInstance FindBestChannelForSPC()
        {
            //FinalChannelInstance selectedChannel = null;

            List<FinalChannelInstance> fullTestedChannel = new List<FinalChannelInstance>();

            foreach (FinalChannelInstance channel in this._channels)
            {
                if (channel.HighTempFullTested)
                {
                    fullTestedChannel.Add(channel);
                }
            }

            if (fullTestedChannel.Count != 0)
            {
                foreach (FinalChannelInstance channel in fullTestedChannel)
                {
                    if (channel.ItuFrequency_GHz == 191700)
                    {
                        return channel;
                    }
                }

                foreach (FinalChannelInstance channel in fullTestedChannel)
                {
                    if (channel.ItuFrequency_GHz == 193700)
                    {
                        return channel;
                    }
                }

                foreach (FinalChannelInstance channel in fullTestedChannel)
                {
                    if (channel.ItuFrequency_GHz == 194700)
                    {
                        return channel;
                    }
                }

                return fullTestedChannel[0];
            }

            return null;
        }
    }

    public class FinalChannelInstance
    {
        /// <summary>
        /// Construct a FinalChannelInstance from a line of csv file.
        /// </summary>
        /// <param name="reader"></param>
        public FinalChannelInstance(CSVDbReader reader)
        {
            this.ItuChannelIndex = reader.GetUInt("ItuChannelIndex");
            this.ItuFrequency_GHz = reader.GetUInt("ItuFreq_GHz");
            this.SuperMode = reader.GetUInt("Supermode");

            this.MzLeftArmImb_mA = reader.GetDouble("MzLeftArmImb_mA");
            this.MzRightArmImb_mA = reader.GetDouble("MzRightArmImb_mA");
            this.MzLeftArmModPeak_V = reader.GetDouble("MzLeftArmModPeak_V");
            this.MzLeftArmModQuad_V = reader.GetDouble("MzLeftArmModQuad_V");
            this.MzRightArmModPeak_V = reader.GetDouble("MzRightArmModPeak_V");
            this.MzRightArmModQuad_V = reader.GetDouble("MzRightArmModQuad_V");

            this.FrontSectionPair = reader.GetInt("FrontSectionPair");
            this.IFsConst_mA = reader.GetDouble("IFsConst_mA");
            this.IFsNonConst_mA = reader.GetDouble("IFsNonConst_mA");
            this.IGain_mA = reader.GetUInt("IGain_mA");
            this.ISOA_mA = reader.GetDouble("ISOA_mA");
            this.IPhaseModeAcq_mA = reader.GetDouble("IPhaseModeAcq_mA");
            this.IPhaseITU_mA = reader.GetDouble("IPhaseITU_mA");
            this.IPhaseCal_mA = reader.GetDouble("IPhaseCal_mA");
            this.IRear_mA = reader.GetDouble("IRear_mA");
            //Mid-temp parameters
            this.OpticalPowerRatio = reader.GetDouble("OptPowerRatio");
            this.ItxLock_mA = reader.GetDouble("ItxLock_mA");
            this.IrxLock_mA = reader.GetDouble("IrxLock_mA");
            this.LockRatio  = reader.GetDouble("LockRatio");//excepted to be ItxLock_mA / IrxLock_mA

            //High temp parameters;
            int index = Math.Max(reader.FindIndex("PwrAtPeakChange_CL_dB_75C"), reader.FindIndex("PwrAtPeakChange_CL_dB_72C"));
            this.Thi_PeakPowerChangeClosedLoop_dB = reader.GetNullableDouble(index);

            index = Math.Max(reader.FindIndex("PwrAtPeakChange_OL_dB_75C"), reader.FindIndex("PwrAtPeakChange_OL_dB_72C"));
            this.Thi_PeakPowerChangeOpenLoop_dB = reader.GetNullableDouble(index);

            index = Math.Max(reader.FindIndex("FreqUnlocked_GHz_72C"), reader.FindIndex("FreqUnlocked_GHz_75C"));
            this.Thi_FrequencyUnlocked_GHz = reader.GetNullableDouble(index);

            index = Math.Max(reader.FindIndex("FreqLocked_GHz_72C"), reader.FindIndex("FreqLocked_GHz_75C"));
            this.Thi_FrequencyLocked_GHz = reader.GetNullableDouble(index);

            index = Math.Max(reader.FindIndex("FreqUnlockedChange_GHz_72C"), reader.FindIndex("FreqUnlockedChange_GHz_75C"));
            this.Thi_FrequencyUnlockedChange_GHz = reader.GetNullableDouble(index);

            index = Math.Max(reader.FindIndex("FreqLockedChange_GHz_72C"), reader.FindIndex("FreqLockedChange_GHz_75C"));
            this.Thi_FrequencyLockedChange_GHz = reader.GetNullableDouble(index);

            //Low temp parameters;
            this.TLo_PeakPowerOpenLoop_dBm = reader.GetNullableDouble("PwrAtPeak_OL_dBm_-5C");
            this.TLo_PeakPowerChangeOpenLoop_dB = reader.GetNullableDouble("PwrAtPeakChange_OL_dB_-5C");

            this.TLo_PeakPowerClosedLoop_dBm = reader.GetNullableDouble("PwrAtPeak_CL_dBm_-5C");
            this.TLo_PeakPowerChangeClosedLoop_dB = reader.GetNullableDouble("PwrAtPeakChange_CL_dB_-5C");

            string tmpstr = reader.GetString("FailCode");
            string[] failParams = tmpstr.Split(new char[] { ';', ':' });
            foreach (string str in failParams)
            {
                if ((str == null) || (str.Length == 0))
                {
                    continue;
                }

                string lowstr = str.ToLower();

                switch (lowstr)
                {
                    case "paramoutofspec":
                    case "failedparams":
                    case "nofail":
                        break;

                    default:
                        this._failedParameters.Add(str);
                        break;
                }
            }
        }

        public List<string> FailedParameters
        {
            get
            {
                return this._failedParameters;
            }
        }

        public uint ItuChannelIndex = 0;
        public uint ItuFrequency_GHz = 0;
        public uint SuperMode = 0;

        #region Paramters @ 25 degree centigrade
        /// <summary>
        /// Optical power ration @ mid-temp.
        /// </summary>
        public double OpticalPowerRatio;
        /// <summary>
        /// Actual frequency(GHz) at 25 degree centigrade.
        /// </summary>
        public double Frequency_GHz = 0;
        public double ItxLock_mA;
        public double IrxLock_mA;

        /// <summary>
        /// LockRatio at 25 degree centigrade.
        /// LockRatio = (Tx Current)/(Rx Current)
        /// </summary>
        public double LockRatio = 0;
        #endregion

        #region Parameter @75 degree centigrade
        /// <summary>
        /// Unlocked frequency @ hi-temp.
        /// </summary>
        public double? Thi_FrequencyUnlocked_GHz;
        /// <summary>
        /// Locked frequency @ hi-temp.
        /// </summary>
        public double? Thi_FrequencyLocked_GHz;
        /// <summary>
        /// Unlocked frequency change @ hi-temp
        /// </summary>
        public double? Thi_FrequencyUnlockedChange_GHz;
        /// <summary>
        /// Locked frequency change @ hi-temp.
        /// </summary>
        public double? Thi_FrequencyLockedChange_GHz;
        /// <summary>
        /// Power Change with tap leveling.
        /// </summary>
        public double? Thi_PeakPowerChangeClosedLoop_dB;
        /// <summary>
        /// Power change without tap leveling.
        /// </summary>
        public double? Thi_PeakPowerChangeOpenLoop_dB;
        #endregion

        #region Parameters @-5 degree centigrade
        /// <summary>
        /// low-temp peak power without tap leveling
        /// </summary>
        public double? TLo_PeakPowerOpenLoop_dBm;
        /// <summary>
        /// low-temp peak power change without tap leveling.
        /// </summary>
        public double? TLo_PeakPowerChangeOpenLoop_dB;
        /// <summary>
        /// Low-temp peak power with tap leveling.
        /// </summary>
        public double? TLo_PeakPowerClosedLoop_dBm;
        /// <summary>
        /// Low-temp peak power change with tap-leveling
        /// </summary>
        public double? TLo_PeakPowerChangeClosedLoop_dB;
        #endregion
        //...
        //...
        public double MzLeftArmImb_mA = 0;
        public double MzRightArmImb_mA = 0;
        public double MzLeftArmModPeak_V = 0;
        public double MzLeftArmModQuad_V = 0;
        public double MzRightArmModPeak_V = 0;
        public double MzRightArmModQuad_V = 0;

        public int FrontSectionPair = 0;
        public double IFsConst_mA;
        public double IFsNonConst_mA;
        public uint IGain_mA = 0;
        public double ISOA_mA = 0;
        //public uint IPhase_mA = 0;
        public double IPhaseITU_mA = 0;
        public double IPhaseCal_mA = 0;
        public double IPhaseModeAcq_mA = 0;
        public double IRear_mA = 0;
        //...
        public bool? Passed = null;

        #region Private members
        /// <summary>
        /// Parsed failed code string list.
        /// </summary>
        private List<string> _failedParameters = new List<string>();
        #endregion

        #region Public Properties
        public bool FullTested
        {
            get
            {
                return (this.LowTempFullTested) && (this.HighTempFullTested);
            }
        }

        public bool LowTempFullTested
        {
            get
            {
                if ((this.TLo_PeakPowerChangeClosedLoop_dB == null) ||
                    (this.TLo_PeakPowerChangeOpenLoop_dB == null) ||
                    (this.TLo_PeakPowerClosedLoop_dBm == null) ||
                    (this.TLo_PeakPowerOpenLoop_dBm == null))
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        public bool HighTempFullTested
        {
            get
            {
                if (
                    (this.Thi_FrequencyLocked_GHz == null) ||
                    (this.Thi_FrequencyUnlocked_GHz == null) ||
                    (this.Thi_FrequencyUnlockedChange_GHz == null) ||
                    (this.Thi_FrequencyLockedChange_GHz == null) ||
                    (this.Thi_PeakPowerChangeOpenLoop_dB == null))
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
        #endregion

        #region Public Methids
        public MzData GetMzData()
        {
            MzData ret = new MzData();
            ret.LeftArmImb_mA = this.MzLeftArmImb_mA;
            ret.LeftArmMod_Peak_V = this.MzLeftArmModPeak_V;
            ret.LeftArmMod_Quad_V = this.MzLeftArmModQuad_V;
            ret.RightArmImb_mA = this.MzRightArmImb_mA;
            ret.RightArmMod_Peak_V = this.MzRightArmModPeak_V;
            ret.RightArmMod_Quad_V = this.MzRightArmModQuad_V;

            return ret;
        }

        public DsdbrChannelSetup GetDsdbrChannelSetup()
        {
            DsdbrChannelSetup ret = new DsdbrChannelSetup();

            ret.FrontPair = this.FrontSectionPair;
            ret.IFsFirst_mA = this.IFsConst_mA;
            ret.IFsSecond_mA = this.IFsNonConst_mA;
            ret.IGain_mA = this.IGain_mA;
            ret.IPhase_mA = this.IPhaseModeAcq_mA;
            ret.IRear_mA = this.IRear_mA;
            ret.ISoa_mA = this.ISOA_mA;

            return ret;
        }
        #endregion
    }
}

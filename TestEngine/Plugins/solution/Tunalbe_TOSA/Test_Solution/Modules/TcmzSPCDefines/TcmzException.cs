using System;
using System.Collections.Generic;
using System.Text;

namespace TCMZDefinitions
{
    public class TcmzException : Exception
    {
        public TcmzException()
        {
        }

        public TcmzException( string message )
            : base( message )
        {
        }
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestLibrary.Algorithms;

namespace TCMZDefinitions
{
    public class SPCDefinitions
    {
        public const string __infeedSchema = "hiberdb";
        public const string __infeedStage = "tcmz_final_spcb";

        public const string __Schema = "HIBERDB";
        public const string __TestStage = "tcmz_final_spc";
    }

    public class SPCStageData
    {
        /// <summary>
        /// reference power
        /// </summary>
        private double _powerDirect_dBm = 0;
        /// <summary>
        /// filtered power.
        /// </summary>
        private double _powerFiltered_dBm = 0;

        public uint SelectedFrequency_GHz;
        /// <summary>
        /// Selected channel index;
        /// </summary>
        public uint SelectedIndex;

        //TEC section
        public double Rth_DSDBR_ohm;
        private double? _deltaRth_DSDBR_ohm;
        public double Rth_Mz_ohm;
        private double? _deltaRth_Mz_ohm;
        public double TEC_DSDBRVoltage_V;
        public double TEC_DSDBRCurrent_mA;
        public double TEC_MzVoltage_V;
        public double TEC_MzCurrent_mA;

        //DSDBR Section
        public double GainVoltage_V;
        private double? _deltaGainVoltage_V;
        public double SOAVoltage_V;
        public double RearVoltage_V;
        public double PhaseVoltage_V;
        private double? _deltaPhaseVoltage_V;
        public double NonFrontVoltage_V;
        public double ConstFrontVoltage_V;

        //Mz section
        public double MzLeftArmImb_V;
        public double MzRightArmImb_V;
        public double MzLeftArmMod_mA;
        public double MzRightArmMod_mA;
        public double TapCompCurrent_mA;
        private double? _deltaCompTapCurrent_mA;
        public double ?TapInlineCurrent_mA;
        private double? _deltaTapInlineCurrent_mA;

        //PowerMeter measurement
        public double FiberPowerPeak_dBm;

        public double? DeltaCompTapCurrent_mA
        {
            get
            {
                return _deltaCompTapCurrent_mA;
            }
        }

        public double ? DeltaInlineTapCurrent_mA
        {
            get
            {
                return _deltaTapInlineCurrent_mA;
            }
        }

        public double? DeltaRth_DSDBR_ohm
        {
            get
            {
                return _deltaRth_DSDBR_ohm;
            }
        }

        /// <summary>
        /// Delta resistance of Mz Tec.
        /// </summary>
        public double? DeltaRth_Mz_ohm
        {
            get
            {
                return _deltaRth_Mz_ohm;
            }
        }

        public double? DeltaGainVoltage_V
        {
            get
            {
                return _deltaGainVoltage_V;
            }
        }

        public double? DeltaPhastVoltage_V
        {
            get
            {
                return _deltaPhaseVoltage_V;
            }
        }

        /// <summary>
        /// Peak fibre power ( target );
        /// </summary>
        public double FiberPowerPeak_mW
        {
            set
            {
                this.FiberPowerPeak_dBm = Alg_PowConvert_dB.Convert_mWtodBm(value);
            }
        }

        /// <summary>
        /// filter power as dbm
        /// </summary>
        public double PowerFiltered_dBm
        {
            set
            {
                _powerFiltered_dBm = value;
            }
            get
            {
                return _powerFiltered_dBm;
            }
        }

        /// <summary>
        /// filtered power as mW
        /// </summary>
        public double PowerFiltered_mW
        {
            get
            {
                return Alg_PowConvert_dB.Convert_dBmtomW(_powerFiltered_dBm); ;
            }
            set
            {
                this._powerFiltered_dBm = Alg_PowConvert_dB.Convert_mWtodBm(value);
            }
        }

        /// <summary>
        /// Reference power as dBm
        /// </summary>
        public double PowerDirect_dBm
        {
            get
            {
                return this._powerDirect_dBm;
            }
            set
            {
                this._powerDirect_dBm = value;
            }
        }

        /// <summary>
        /// Reference Power Property as mW
        /// </summary>
        public double PowerDirect_mW
        {
            get
            {
                return Alg_PowConvert_dB.Convert_dBmtomW(_powerDirect_dBm);
            }
            set
            {
                _powerDirect_dBm = Alg_PowConvert_dB.Convert_mWtodBm(value);
            }
        }

        public double? deltaFiberPowerPeak_dBm;

        //WaveMeter measurement
        public double ActualFrequency_GHz;
        public double? deltaActualFrequency_GHz;

        //Photocurrent measurement ( for 306EE );
        public double TxLockerCurrent_mA;
        public double RxLockerCurrent_mA;
        private double? _deltaRxLockerCurrent_mA;
        //public double LockRatio;
        public double? deltaLockRatio;

        //Power ratio measurement ( for 360II )
        public double CH1Current_mA;
        public double CH2Current_mA;
        public double? deltaPowerRatio;

        //SMSR => OSA monitor
        public double SMSR_dB;
        public double? deltaSMSR_dB;

        //High temperature monitor
        public double Thi_PeakPowerOpenLoop_dBm;
        public double Thi_PeakPowerChangeOpenLoop_dB;
        public double? deltaThi_PeakPowerChangeOpenLoop_dB;

        public double Thi_PeakPowerChangeClosedLoop_dB;
        public double? deltaThi_PeakPowerChangeCloseLoop_dB;

        public double Thi_FrequencyUnlockedChange_GHz;
        public double Thi_FrequencyLockedChange_GHz;

        public double? deltaThi_FrequencyUnlockedChange_GHz;
        public double? deltaThi_FrequencyLockedChange_GHz;

        public double Thi_DSDBRVoltage_V;
        private double ? _deltaThi_DSDBRVoltage_V;
        public double Thi_DSDBRCurrent_mA;
        public double Thi_MzVoltage_V;
        public double Thi_MzCurrent_mA;

        public double Thi_TempStab_Sec;

        public double? deltaThi_DSDBRVoltage_V
        {
            get
            {
                return this._deltaThi_DSDBRVoltage_V;
            }
        }

        public double? DeltaRxLockerCurrent_mA
        {
            get
            {
                return _deltaRxLockerCurrent_mA;
            }
        }

        public double LockerRatio
        {
            get
            {
                return this.TxLockerCurrent_mA / this.RxLockerCurrent_mA;
            }
        }

        /// <summary>
        /// returns power ratio ( filtered power / reference power ).
        /// </summary>
        public double PowerRatio
        {
            get
            {
                return this.PowerFiltered_mW / this.PowerDirect_mW;
            }
        }

        // In-actived code...
        //public void CalculateDelta(SPCStageData previousData)
        //{
        //    this.deltaActualFrequency_GHz = this.ActualFrequency_GHz - previousData.ActualFrequency_GHz;
        //    this.deltaLockRatio = this.LockerRatio - previousData.LockerRatio;
        //    this.deltaFiberPowerPeak_dBm = this.FiberPowerPeak_dBm - previousData.FiberPowerPeak_dBm;
        //    this.deltaPower2_dBm = this.CH2Power_dBm - previousData.CH2Power_dBm;
        //    this.deltaPowerRatio = this.PowerRatio - previousData.PowerRatio;
        //    this.deltaSMSR_dB = this.SMSR_dB - previousData.SMSR_dB;
        //    //this.deltaThi_FrequencUnlockedyChange_GHz = this.Thi_FrequencyUnlockedChange_GHz - previousData.Thi_FrequencyChange_GHz;
        //    this.deltaThi_FrequencyUnlockedChange_GHz = this.Thi_FrequencyUnlockedChange_GHz - previousData.Thi_FrequencyUnlockedChange_GHz;
        //    //this.deltaThi_PowerChange_dB = this.deltaThi_PowerChange_dB - previousData.deltaThi_PowerChange_dB;
        //    this.deltaThi_PeakPowerChangeOpenLoop_dB = this.Thi_PeakPowerChangeOpenLoop_dB - previousData.Thi_PeakPowerChangeOpenLoop_dB;
        //    //this.deltaThi_PeakPowerChangeClosedLoop_dB = this.Thi_PeakPowerChangeClosedLoop_dB - this.Thi_PeakPowerChangeClosedLoop_dBm;
        //    this.deltaThi_PeakPowerChangeCloseLoop_dB = this.Thi_PeakPowerChangeOpenLoop_dB - previousData.Thi_PeakPowerChangeOpenLoop_dB;
        //}

        public void CalculateDelta(DatumList baseline)
        {
            if ((this.deltaActualFrequency_GHz == null) &&
                baseline.IsPresent("FREQ") )
            {
                this.deltaActualFrequency_GHz = this.ActualFrequency_GHz - baseline.ReadDouble("FREQ");
            }

            if ((this.deltaFiberPowerPeak_dBm == null) &&
                baseline.IsPresent("FIBREPWRPEAK"))
            {
                this.deltaFiberPowerPeak_dBm = this.FiberPowerPeak_dBm - baseline.ReadDouble("FIBREPWRPEAK");
            }

            if ((this.deltaLockRatio == null) &&
                baseline.IsPresent("LOCKRATIO"))
            {
                this.deltaLockRatio = this.LockerRatio - baseline.ReadDouble("LOCKRATIO");
            }

            if ((this.deltaPowerRatio == null) &&
                baseline.IsPresent("OPTPOWERRATIO"))
            {
                this.deltaPowerRatio = this.PowerRatio - baseline.ReadDouble("OPTPOWERRATIO");
            }

            if ((this.deltaSMSR_dB == null) &&
                baseline.IsPresent("SMSR"))
            {
                this.deltaSMSR_dB = this.SMSR_dB - baseline.ReadDouble("SMSR");
            }

            if ((this.deltaThi_FrequencyLockedChange_GHz == null) &&
                baseline.IsPresent("THI_FREQLOCKEDCHANGE"))
            {
                this.deltaThi_FrequencyLockedChange_GHz = this.Thi_FrequencyLockedChange_GHz - baseline.ReadDouble("THI_FREQLOCKEDCHANGE");
            }

            if ((this.deltaThi_FrequencyUnlockedChange_GHz == null) &&
                baseline.IsPresent("THI_FREQUNLOCKEDCHANGE"))
            {
                this.deltaThi_FrequencyUnlockedChange_GHz = this.Thi_FrequencyUnlockedChange_GHz - baseline.ReadDouble("THI_FREQUNLOCKEDCHANGE");
            }

            if ((this.deltaThi_PeakPowerChangeCloseLoop_dB == null) &&
                baseline.IsPresent("THI_PWRATPEAK_CL"))
            {
                this.deltaThi_PeakPowerChangeCloseLoop_dB = this.Thi_PeakPowerChangeClosedLoop_dB - baseline.ReadDouble("THI_PWRATPEAK_CL");
            }

            if ((this.deltaThi_PeakPowerChangeOpenLoop_dB == null) &&
                baseline.IsPresent("THI_PWRATPEAK_OL"))
            {
                this.deltaThi_PeakPowerChangeOpenLoop_dB = this.Thi_PeakPowerChangeOpenLoop_dB - baseline.ReadDouble("THI_PWRATPEAK_OL");
            }

            if ((this._deltaThi_DSDBRVoltage_V == null) &&
                (baseline.IsPresent("THI_TECDSDBRVOLTAGE_V")))
            {
                this._deltaThi_DSDBRVoltage_V =
                    this.Thi_DSDBRVoltage_V - baseline.ReadDouble("THI_TECDSDBRVOLTAGE_V");
            }

            if ( this._deltaGainVoltage_V == null )
            {
                if (baseline.IsPresent("VGAIN_V"))
                {
                    this._deltaGainVoltage_V = this.GainVoltage_V - baseline.ReadDouble("VGAIN_V");
                }
                else
                {
                    this._deltaGainVoltage_V = null;
                }
            }

            if ( this._deltaPhaseVoltage_V == null )
            {
                if (baseline.IsPresent("VPHASE_V"))
                {
                    this._deltaPhaseVoltage_V = this.PhaseVoltage_V - baseline.ReadDouble("VPHASE_V");
                }
                else
                {
                    this._deltaPhaseVoltage_V = null;
                }
            }

            if (this._deltaRth_DSDBR_ohm == null)
            {
                if (baseline.IsPresent("RTHDSDBR"))
                {
                    this._deltaRth_DSDBR_ohm = this.Rth_DSDBR_ohm - baseline.ReadDouble("RTHDSDBR");
                }
                else
                {
                    this._deltaRth_DSDBR_ohm = null;
                }
            }

            if (this._deltaRth_Mz_ohm == null)
            {
                if (baseline.IsPresent("RTHMZ"))
                {
                    this._deltaRth_Mz_ohm = this.Rth_Mz_ohm - baseline.ReadDouble("RTHMZ");
                }
                else
                {
                    this._deltaRth_Mz_ohm = null;
                }
            }

            string KeyName = "TAPINLINE_I_QUAD";
            if (baseline.IsPresent(KeyName))
            {
                this._deltaTapInlineCurrent_mA = this.TapInlineCurrent_mA - baseline.ReadDouble(KeyName);
            }
            else
            {
                this._deltaTapInlineCurrent_mA = null;
            }

            KeyName = "TAPCOMP_I_QUAD";
            if (baseline.IsPresent(KeyName))
            {
                this._deltaCompTapCurrent_mA = this.TapCompCurrent_mA - baseline.ReadDouble(KeyName);
            }
            else
            {
                this._deltaCompTapCurrent_mA = null;
            }

            KeyName = "IRXLOCK";
            if (baseline.IsPresent(KeyName))
            {
                this._deltaRxLockerCurrent_mA = this.RxLockerCurrent_mA - baseline.ReadDouble(KeyName);
            }
            else
            {
                this._deltaRxLockerCurrent_mA = null;
            }
        }
    }
}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// IlmzPowerUp.cs
//
// Author: Tony.Foster, 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestSolution.IlmzCommonData;
using Bookham.TestSolution.IlmzCommonInstrs;
using Bookham.TestSolution.IlmzCommonUtils;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestSolution.ILMZFinalData;
using Bookham.ToolKit.Mz;
using Bookham.TestLibrary.Instruments;
using System.Collections.Specialized;
using Bookham.TestEngine.Config;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// TCMZ Initialise Module 
    /// </summary>
    public class IlmzPowerUp : ITestModule
    {
        #region ITestModule Members
        /// <summary>
        /// 
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="userType"></param>
        /// <param name="configData"></param>
        /// <param name="instruments"></param>
        /// <param name="chassis"></param>
        /// <param name="calData"></param>
        /// <param name="previousTestData"></param>
        /// <returns></returns>
        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, DatumList configData, InstrumentCollection instruments, ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {
            DatumList moduleTestResults = new DatumList();

            // Get instruments
            IlMzInstruments mzInstrs = (IlMzInstruments)configData.ReadReference("MzInstruments");
            IlMzDriverUtils mzDriverUtils = new IlMzDriverUtils(mzInstrs);
            IlmzChannelInit tcmzSettings = (IlmzChannelInit)configData.ReadReference("IlmzSettings");
            IInstType_OpticalPowerMeter powerMeter = (IInstType_OpticalPowerMeter)instruments["OpticalPowerMeter"];
            Inst_Fcu2Asic Fcu=(Inst_Fcu2Asic)instruments["Fcu2Asic"];
            
            IInstType_TecController mzTec = null;
            //FCU2AsicInstruments fcu2AsicInstrs = (FCU2AsicInstruments)configData.ReadReference("FCU2AsicInstruments");

            // Make sure DSDBR and MZ are off            
            // Note that locker biases are applied during CG startup
            //IlMzDriverUtils.GroundAllOutputs(mzInstrs);

            // Apply MZ tap bias
            //fcu2AsicInstrs.Fcu2Asic.VCtap_Volt = (float)configData.GetDatumDouble("MzTapBias_V").Value;
            //mzInstrs.TapComplementary.VoltageSetPoint_Volt = configData.GetDatumDouble("MzTapBias_V").Value;
            //mzInstrs.TapComplementary.OutputEnabled = true;
            //if (mzInstrs.TapInline != null && mzInstrs.TapInline.IsOnline)
            //{
            //    mzInstrs.TapInline.VoltageSetPoint_Volt = configData.GetDatumDouble("MzTapBias_V").Value;
            //    mzInstrs.TapInline.OutputEnabled = true;
            //}
            Fcu.ZeroCurrent();
            Fcu.CloseAllAsic();

            // Measure DUT Locker dark currents
            DsdbrUtils.LockerCurrents lockerDarkCurrents = DsdbrUtils.ReadLockerCurrents();

            // Measure MZ Tap dark current
            //double mzTapCompDark_A = fcu2AsicInstrs.Fcu2Asic.ICtap_mA / 1000.0;
            //double mzTapInlineDark_A = 0;
            //if (mzInstrs.TapInline != null && mzInstrs.TapInline.IsOnline)
            //{
            //    mzTapInlineDark_A = mzInstrs.TapInline.CurrentActual_amp;
            //}

            // Measure MZ arm dark currents
            //double mzLeftModDarkI_A = fcu2AsicInstrs.Fcu2Asic.IimbLeft_mA / 1000.0;
            //double mzRightModDarkI_A = fcu2AsicInstrs.Fcu2Asic.IRightModBias_mA / 1000.0;           
            //double mzLeftImbDarkI_A = mzDriverUtils.ReadDarkCurrent(SourceMeter.LeftImbArm);
            //double mzRightImbDarkI_A = mzDriverUtils.ReadDarkCurrent(SourceMeter.RightImbArm);

            double mzLeftImbDarkI_A = Fcu.IimbLeft_mA / 1000.0;
            double mzRightImbDarkI_A = Fcu.IimbRight_mA / 1000.0;
            // Check that power meter has zeroed OK
            // Waits for end of zeroing and raises exception if fail 
            //Fcu.CloseAllAsic();
            System.Threading.Thread.Sleep(500);
            try
            {
                //mzInstrs.PowerMeter.ZeroDarkCurrent_Start();
                mzInstrs.PowerMeter.ZeroDarkCurrent_End();
            }
            catch (Exception e)
            {
                ButtonId response = ButtonId.No;
                string errorMes = "Power meter do zeroing fail, Do you wish to Continue testing?";
                response = engine.ShowYesNoUserQuery(errorMes);
                if (response == ButtonId.No)
                {
                    moduleTestResults.AddBool("ErrorExists", true);
                    moduleTestResults.AddString("ErrorString", "Power Meter do zeroing fail");
                    return moduleTestResults;
                }

            }

            /*// Power up DSDBR
            Inst_Ke24xx keSource = DsdbrUtils.Fcu2AsicInstrumentGroup.AsicVccSource as Inst_Ke24xx;
            if (keSource != null)
            {
               
                keSource.SenseCurrent(1.0, 1.050);
            }

            
            keSource = DsdbrUtils.Fcu2AsicInstrumentGroup.AsicVeeSource as Inst_Ke24xx;
            if (keSource != null)
            {
                keSource.SenseCurrent(0.05, 0.05);
                keSource.UseFrontTerminals = true;
            }*/
            

            //DsdbrUtils.SetDsdbrCurrents_mA(tcmzSettings.Dsdbr.Setup);

            double curReading = DsdbrUtils.Fcu2AsicInstrumentGroup.AsicVccSource.CurrentActual_amp;
            if (instruments.Contains("MzTec")) mzTec = (IInstType_TecController)instruments["MzTec"];
            //curReading = DsdbrUtils.Fcu2AsicInstrumentGroup.AsicVeeSource.CurrentActual_amp;

            // Power up MZ

            //double senseRange = mzInstrs.LeftArmMod.CurrentComplianceSetPoint_Amp;
            //mzInstrs.LeftArmMod.SenseCurrent(senseRange, senseRange);
            //mzInstrs.RightArmMod.SenseCurrent(senseRange, senseRange);
            //IlMzDriverUtils.SetupMzToPeak(mzInstrs, tcmzSettings.Mz);
            //IlMzDriverUtils.SetupMzToPeak(Fcu, tcmzSettings.Mz);

            // Optical Power check
            //mzInstrs.PowerMeter.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_mW;
            ButtonId retryResponse = ButtonId.No;
            double power_mW = 0;
            do
            {
                #region[power up when retry.20170804  fix by Dong.Chen ]
                DsdbrUtils.SetDsdbrCurrents_mA(tcmzSettings.Dsdbr.Setup);
                curReading = DsdbrUtils.Fcu2AsicInstrumentGroup.AsicVeeSource.CurrentActual_amp;
                mzInstrs.PowerMeter.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_mW;
                IlMzDriverUtils.SetupMzToPeak(Fcu, tcmzSettings.Mz);

                #endregion


                power_mW = powerMeter.ReadPower();
                if (power_mW < minPower_mW)
                {
                    string prompt = string.Format("Optical power({0} mW) is too low at power up(min:{1} mW).\n" +
                        "Ensure the device fibre is connected to the patch cord.\nWould you like to retry?\n"+"请确认光纤是清洁并未断裂的接入patch cord，谢谢！",
                        power_mW, minPower_mW);

                    retryResponse = engine.ShowYesNoUserQuery(prompt);
                }

            } while (retryResponse == ButtonId.Yes && power_mW < minPower_mW);

            string errStr = "";
            bool error = false;
            DsdbrUtils.LockerCurrents lockerOpCurrents = DsdbrUtils.ReadLockerCurrents();
            if (power_mW < minPower_mW)
            {
                errStr = "Optical power too low at power up: " + power_mW + " mW";
                error = true;
                lockerOpCurrents = new DsdbrUtils.LockerCurrents();
                lockerOpCurrents.RxCurrent_mA = -999;
                lockerOpCurrents.TxCurrent_mA = -999;
                lockerOpCurrents.LockRatio = -999;
            }
            else
            {
                // Locker currents check
                lockerOpCurrents = DsdbrUtils.ReadLockerCurrents();
                // alice.huang 2010-03-03   change minLockerCurrent_A to 0.02
                if (lockerOpCurrents.TxCurrent_mA < 0.02 || lockerOpCurrents.RxCurrent_mA < 0.02)   // 0.02->minLockerCurrent_A
                {
                    do
                    {
                        errStr = "Locker current(s) too low at power up: TX locker " +
                        Math.Round(lockerOpCurrents.TxCurrent_mA, 3) + " mA, RX locker " +
                        Math.Round(lockerOpCurrents.RxCurrent_mA, 3) + " mA";
                        //error = true;

                        string prompt1 = string.Format(errStr + "请确认器件是否放置正确 PIN 针是否压严实，谢谢！");
                        //断电
                        //close the instrs ,becase we need relod device
                       // InstCommands.CloseInstToLoadDUT(engine, DsdbrUtils.Fcu2AsicInstrumentGroup.FcuSource, DsdbrUtils.Fcu2AsicInstrumentGroup.AsicVccSource, DsdbrUtils.Fcu2AsicInstrumentGroup.AsicVeeSource, );

                        retryResponse = engine.ShowYesNoUserQuery(prompt1);
                        if (retryResponse == ButtonId.Yes)
                        {
         

                            #region[power up when retry.20170804  fix by Dong.Chen ]

                            //重新上电
                           //InstCommands.OpenInstToLoadDUT(engine, DsdbrUtils.Fcu2AsicInstrumentGroup.FcuSource, DsdbrUtils.Fcu2AsicInstrumentGroup.AsicVccSource, DsdbrUtils.Fcu2AsicInstrumentGroup.AsicVeeSource, progInfo.Instrs.TecDsdbr);

                            DsdbrUtils.SetDsdbrCurrents_mA(tcmzSettings.Dsdbr.Setup);
                            curReading = DsdbrUtils.Fcu2AsicInstrumentGroup.AsicVeeSource.CurrentActual_amp;
                            //mzInstrs.PowerMeter.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_mW;
                            IlMzDriverUtils.SetupMzToPeak(Fcu, tcmzSettings.Mz);
                            #endregion
                        }
                        else
                        {
                            continue;
                        }
                        lockerOpCurrents = DsdbrUtils.ReadLockerCurrents();
                        if (lockerOpCurrents.TxCurrent_mA < 0.02 || lockerOpCurrents.RxCurrent_mA < 0.02)   // 0.02->minLockerCurrent_A
                        {
                        }
                        else
                        {
                            break;
                        }

                    } while ((lockerOpCurrents.TxCurrent_mA < 0.02 || lockerOpCurrents.RxCurrent_mA < 0.02));
                }
            }



            // Setup Mz to peak for the following measurements
            IlMzDriverUtils.SetupMzToPeak(Fcu, tcmzSettings.Mz);

            // Add results
            moduleTestResults.AddDouble("TxLockerDarkCurrent_mA", lockerDarkCurrents.TxCurrent_mA);
            moduleTestResults.AddDouble("RxLockerDarkCurrent_mA", lockerDarkCurrents.RxCurrent_mA);
            moduleTestResults.AddDouble("MzTapCompDarkCurrent_mA", 0);
            moduleTestResults.AddDouble("MzLeftModDarkCurrent_mA", 0);
            moduleTestResults.AddDouble("MzRightModDarkCurrent_mA", 0);
            moduleTestResults.AddDouble("MzLeftImbDarkCurrent_mA", mzLeftImbDarkI_A * 1000);
            moduleTestResults.AddDouble("MzRightImbDarkCurrent_mA", mzRightImbDarkI_A * 1000);
            moduleTestResults.AddDouble("OpticalPower_mW", power_mW);
            moduleTestResults.AddDouble("TxLockerPhoto_A", lockerOpCurrents.TxCurrent_mA);
            moduleTestResults.AddDouble("RxLockerPhoto_A", lockerOpCurrents.RxCurrent_mA);
            moduleTestResults.AddBool("ErrorExists", error);
            moduleTestResults.AddString("ErrorString", errStr);
            return moduleTestResults;
        }
        /// <summary>
        /// Read instruments calibration data
        /// </summary>
        /// <param name="InstrumentName"></param>
        /// <param name="InstrumentSerialNo"></param>
        /// <param name="ParameterName"></param>
        /// <param name="CalFactor"></param>
        /// <param name="CalOffset"></param>
        private void readCalData(string InstrumentName, string InstrumentSerialNo,
            string ParameterName, out double CalFactor, out double CalOffset)
        {
            StringDictionary keys = new StringDictionary();
            keys.Add("Instrument_name", InstrumentName);
            keys.Add("Parameter", ParameterName);
            if (InstrumentSerialNo != null)
            {
                keys.Add("Instrument_SerialNo", InstrumentSerialNo);
            }
            DatumList calParams = this.InstrumentsCalData.GetData(keys, false);

            CalFactor = calParams.ReadDouble("CalFactor");
            CalOffset = calParams.ReadDouble("CalOffset");
        }
        internal ConfigDataAccessor InstrumentsCalData;
        /// <summary>
        /// 
        /// </summary>
        public Type UserControl
        {
            // No GUI for this module
            get { return null; }
        }

        #endregion

        #region Private Constants
        private const double minPower_mW = 0.0009;//jack.zhang   //raul changed from 0.02 to 0.005
        private const double minLockerCurrent_A = 0.05;
       #endregion
    }
}

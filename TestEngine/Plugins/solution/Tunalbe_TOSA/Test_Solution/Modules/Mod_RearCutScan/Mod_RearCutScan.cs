﻿// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// Mod_RearCutScan.cs
//
// Author: tim.yang, 2011
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestSolution.Instruments;
using Bookham.TestLibrary.Instruments;
//using Bookham.TestLibrary.FcuCommonUtils;
using Bookham.TestLibrary.Utilities;
using System.IO;
using Bookham.TestSolution.IlmzCommonData;
using Bookham.TestSolution.IlmzCommonUtils;
using Bookham.TestSolution.IlmzCommonInstrs;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Test Module Template.
    /// </summary>
    public class Mod_RearCutScan : ITestModule
    {
        #region Instrument and parameters fields

        bool isToCheckSNMixUp = false;
        double freq_PRE_GHz;
        DatumList returnData;
        string work_dir;
        string laser_id;
        string timestamp;
        float Igain_mA;
        float Iphase_mA;
        float Isoa_mA;
        float IRearSoa_mA;
        float IfsCon_mA;
        float IfsNonCon_mA;
        int fp;
        string RearSetupFile;
        string Mz_Rear_Cut_file;
        double Iasic_RearScan;

        float imbLeft_mA;
        float imbRight_mA;

        bool isRTHFailed;
        double rth_Limit_Min_ohm;
        double rth_Limit_Max_ohm;
        double rth_Min_ohm;
        double rth_Max_ohm;         
        double rth_Offset_ohm;

        Inst_Fcu2Asic Fcu2Asic;
        Inst_Ke2510 TecDsdbr;
        IInstType_ElectricalSource ASIC_VccSource;
        IInstType_ElectricalSource ASIC_VeeSource;
        InstType_ElectricalSource FCUMKI_Source;

        private const int UNTEST_PARAM_Value = -999;
        private string fail_Abort_Reason;

            #endregion

        /// <summary>
        /// Initialize instrument and parameters
        /// </summary>
        /// <param name="configData"></param>
        /// <param name="instruments"></param>
        private void Initialize(DatumList configData, InstrumentCollection instruments)
        {
            if (configData.IsPresent("IsToCheckSNMixUp"))
            {
                this.isToCheckSNMixUp = configData.ReadBool("IsToCheckSNMixUp");
            }

            if (configData.IsPresent("FREQ_PRE_GHz"))
            {
                this.freq_PRE_GHz = configData.ReadDouble("FREQ_PRE_GHz");
            }            

            this.fail_Abort_Reason = null;

            returnData = new DatumList();

            work_dir = configData.ReadString("RESULT_DIR");
            laser_id = configData.ReadString("LASER_ID");
            timestamp = configData.ReadString("TIME_STAMP");
            Igain_mA = float.Parse(configData.ReadString("Igain_mA"));
            Iphase_mA = float.Parse(configData.ReadString("Iphase_mA"));
            Isoa_mA = float.Parse(configData.ReadString("Isoa_mA"));
            IRearSoa_mA = float.Parse(configData.ReadString("IrearSoa_mA"));
            IfsCon_mA = float.Parse(configData.ReadString("IfsCon_mA"));
            IfsNonCon_mA = float.Parse(configData.ReadString("IfsNonCon_mA"));
            fp = int.Parse(configData.ReadString("FrontPair"));
            RearSetupFile = configData.ReadString("RearSetupFile_Path");
            Mz_Rear_Cut_file = Path.Combine(work_dir, string.Format("{0}_RearCutScan_{1}.csv", laser_id, timestamp));

            if (!Directory.Exists(work_dir))
            {
                Directory.CreateDirectory(work_dir);
            }

            File.Create(Mz_Rear_Cut_file).Close();

            imbLeft_mA = float.Parse((configData.ReadDouble("ImbLeft_mA")).ToString());
            imbRight_mA = float.Parse((configData.ReadDouble("ImbRight_mA")).ToString());

            isRTHFailed = false;

            rth_Limit_Min_ohm = configData.ReadDouble("RTH_LIMIT_MIN_ohm");
            rth_Limit_Max_ohm = configData.ReadDouble("RTH_LIMIT_MAX_ohm");

            rth_Min_ohm = configData.ReadDouble("RTH_MIN_ohm");
            rth_Max_ohm = configData.ReadDouble("RTH_MAX_ohm"); 
            
            rth_Offset_ohm = configData.ReadDouble("RTH_Offset_ohm");//Jack.zhang for post limit file RTH update. 2012-05-09

            Fcu2Asic = (Inst_Fcu2Asic)instruments["Fcu2Asic"];
            TecDsdbr = (Inst_Ke2510)instruments["TecDsdbr"];
            ASIC_VccSource = (IInstType_ElectricalSource)instruments["ASIC_VccSource"];
            ASIC_VeeSource = (IInstType_ElectricalSource)instruments["ASIC_VeeSource"];

            if (instruments.Contains("FCUMKI_Source"))
            {
                FCUMKI_Source = (InstType_ElectricalSource)instruments["FCUMKI_Source"];
            }

            if (Fcu2Asic.IsOnline == false)
            {
                Fcu2Asic.IsOnline = true;
            }
        }

        private void CheckRTH(ITestEngine engine, DatumList configData, ref double rth_Max_ohm, ref double rth_Min_ohm, out double rth_ohm, out bool isRTHFailed, out DatumList result)
        {
            // Jack.zhang for post limit file RTH update. 2012-05-09
            rth_ohm = TecDsdbr.SensorResistanceActual_ohm + rth_Offset_ohm;

            int timeWaitCount = 0;

            result = null;

            isRTHFailed = false;

            while (rth_ohm < rth_Limit_Min_ohm || rth_ohm > rth_Limit_Max_ohm)
            {
                // Check RTH of TecDsdbr every 0.5s
                System.Threading.Thread.Sleep(500);

                rth_ohm = TecDsdbr.SensorResistanceActual_ohm + rth_Offset_ohm;

                // If RTH is still out of limits after 5*0.5s=2.5s, mark RTH failed
                if (timeWaitCount++ >= 5)
                {
                    isRTHFailed = true;

                    // IsReturnDummyResult is true to return a not failed result
                    if (configData.ReadBool("IsReturnDummyResult"))
                    {
                        ASIC_VccSource.OutputEnabled = false;
                        ASIC_VeeSource.OutputEnabled = false;
                        FCUMKI_Source.OutputEnabled = false;
                        TecDsdbr.OutputEnabled = false;

                        result = GetDummyResult;
                    }
                    else // IsReturnDummyResult is false to return the real result
                    {
                        rth_Max_ohm = Math.Max(rth_Max_ohm, rth_ohm);
                        rth_Min_ohm = Math.Min(rth_Min_ohm, rth_ohm);

                        fail_Abort_Reason = "RTH failed 3 times, please retest this device in another test kit.";

                        engine.ShowContinueUserQuery(fail_Abort_Reason + " 请在其他测试台重新测试这个器件.");

                        result = GetRTHFailedResult;
                    }

                    return;
                }
            }

            rth_Max_ohm = Math.Max(rth_Max_ohm, rth_ohm);
            rth_Min_ohm = Math.Min(rth_Min_ohm, rth_ohm);
        }

        /// <summary>
        /// The real result, whether passed or failed
        /// </summary>
        private DatumList GetGenuineResult
        {
            get
            {
                if (!string.IsNullOrEmpty(fail_Abort_Reason))
                {
                    returnData.AddString("FAIL_ABORT_REASON", fail_Abort_Reason);
                }

                returnData.AddBool("IsRTHFailed", this.isRTHFailed);
                returnData.AddDouble("RTH_MIN_ohm", this.rth_Min_ohm);
                returnData.AddDouble("RTH_MAX_ohm", this.rth_Max_ohm);

                returnData.AddFileLink("Mz_Rear_Cut_file", Mz_Rear_Cut_file);
                returnData.AddDouble("Iasic_RearScan", Iasic_RearScan);

                return returnData;
            }
        }

        /// <summary>
        /// When RTH failed, we need a dummy passed test result to suppress the module failed dialogue, so that we can start over the program
        /// </summary>
        private DatumList GetDummyResult
        {
            get
            {
                returnData.AddBool("IsRTHFailed", this.isRTHFailed);
                returnData.AddDouble("RTH_MIN_ohm", this.rth_Limit_Min_ohm);
                returnData.AddDouble("RTH_MAX_ohm", this.rth_Limit_Max_ohm);

                returnData.AddSint32("PHASE_SCAN_FREQ_JUMP_OK", 1);
                returnData.AddFileLink("Mz_Rear_Cut_file", Mz_Rear_Cut_file);
                returnData.AddDouble("Iasic_RearScan", Iasic_RearScan);

                return returnData;
            }
        }

        /// <summary>
        /// Return not tested result when precedent modules failed or aborted
        /// </summary>
        private DatumList GetNotTestedResult
        {
            get
            {
                returnData.AddDouble("RTH_MIN_ohm", UNTEST_PARAM_Value);
                returnData.AddDouble("RTH_MAX_ohm", UNTEST_PARAM_Value);

                returnData.AddFileLink("Mz_Rear_Cut_file", Mz_Rear_Cut_file);
                returnData.AddSint32("PHASE_SCAN_FREQ_JUMP_OK", UNTEST_PARAM_Value);
                returnData.AddDouble("Iasic_RearScan", UNTEST_PARAM_Value);

                return returnData;
            }
        }

        /// <summary>
        /// Return RTH failed result
        /// </summary>
        private DatumList GetRTHFailedResult
        {
            get
            {
                returnData.AddBool("IsRTHFailed", this.isRTHFailed);
                returnData.AddString("FAIL_ABORT_REASON", fail_Abort_Reason);

                returnData.AddDouble("RTH_MIN_ohm", this.rth_Min_ohm);
                returnData.AddDouble("RTH_MAX_ohm", this.rth_Max_ohm);

                returnData.AddFileLink("Mz_Rear_Cut_file", Mz_Rear_Cut_file);
                returnData.AddSint32("PHASE_SCAN_FREQ_JUMP_OK", UNTEST_PARAM_Value);
                returnData.AddDouble("Iasic_RearScan", UNTEST_PARAM_Value);

                return returnData;
            }
        }

        #region ITestModule Members

        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, DatumList configData, InstrumentCollection instruments, ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {
            engine.GuiToFront();
            engine.GuiShow();

            // Initialize instrument and parameters
            Initialize(configData, instruments);

            if (configData.ReadBool("ReturnNotTestedResult"))
            {
                return GetNotTestedResult;
            }

            //power up laser
            DsdbrChannelData oneChannel = new DsdbrChannelData();
            oneChannel.Setup.IFsFirst_mA = IfsCon_mA;
            oneChannel.Setup.IFsSecond_mA = IfsNonCon_mA;
            oneChannel.Setup.IGain_mA = Igain_mA;
            oneChannel.Setup.IRear_mA = 1;
            oneChannel.Setup.ISoa_mA = Isoa_mA;
            oneChannel.Setup.IRearSoa_mA = IRearSoa_mA;
            oneChannel.Setup.FrontPair = fp;
            oneChannel.Setup.IPhase_mA = Iphase_mA;
            DsdbrUtils.SetDsdbrCurrents_mA(oneChannel.Setup);

            //set up imb current
            Fcu2Asic.IimbLeft_mA = imbLeft_mA;
            Fcu2Asic.IimbRight_mA = imbRight_mA;
            System.Threading.Thread.Sleep(2000);

            //record asic current before rear scan
            Iasic_RearScan = ASIC_VccSource.CurrentActual_amp;

            #region Do rear cut scan

            CsvReader cr = new CsvReader();
            List<string[]> lines = cr.ReadFile(RearSetupFile);

            double[] power_dBm_Array = new double[lines.Count];
            double[] freq_Ghz_Array = new double[lines.Count];
            double[] DUT_LockerRatio = new double[lines.Count];
            double[] rthArray = new double[lines.Count];

            for (int i = 0; i < lines.Count; i++)
            {
                double Irear_mA = double.Parse(lines[i][0]);
                DsdbrUtils.SetSectionCurrent_mA(DSDBRSection.Rear, Irear_mA);

                #region Read RTH every "StepWidthToReadRTH" points(Default is 10) - chongjian.liang - 2013.3.28

                if (i % configData.ReadDouble("StepWidthToReadRTH") == 0)
                {
                    // Leave some time to let tec stabilize when change dsdbr current obviouly.
                    if (i == 0)
                    {
                        System.Threading.Thread.Sleep(10);
                    }

                    DatumList result = null;

                    CheckRTH(engine, configData, ref rth_Max_ohm, ref rth_Min_ohm, out rthArray[i], out this.isRTHFailed, out result);

                    if (this.isRTHFailed)
                    {
                        return result;
                    }
                }
                #endregion

                power_dBm_Array[i] = Measurements.MzHead.ReadPower();
                freq_Ghz_Array[i] = Measurements.ReadFrequency_GHz();
                DUT_LockerRatio[i] = DsdbrUtils.ReadLockerCurrents().LockRatio;

                int p = 0;
                try
                {
                    double allPercent = lines.Count;
                    double percentage = i / allPercent * 100;
                    double pp = Math.Round(percentage);
                    p = int.Parse(pp.ToString());
                }
                catch (Exception e)
                {
                    string a = e.Message;
                }

                engine.SendToGui(p);
            }
            engine.SendToGui(100);

            #endregion            
                       
            #region Write scan result to file
            
            StreamWriter sw = new StreamWriter(Mz_Rear_Cut_file);

            sw.WriteLine("Rear_Index,IRear_mA,Freq_Ghz,Power_dBm,DUT_LockerRatio,RTH");

            for (int i = 0; i < lines.Count; i++)
            {
                sw.Write(i.ToString() + ",");
                sw.Write(lines[i][0] + ",");
                sw.Write(freq_Ghz_Array[i].ToString() + ",");
                sw.Write(power_dBm_Array[i].ToString() + ",");
                sw.Write(DUT_LockerRatio[i].ToString() + ",");
                sw.WriteLine(rthArray[i].ToString());
                sw.Flush();
            }
            sw.Close();

            #endregion

            #region Get the freq closest to the PRE stage LI sweep freq, to check SN mix up problem. - chongjian.liang 2013.3.20

            if (this.isToCheckSNMixUp)
            {
                double deltaFreq = 0;
                double deltaFreq_Min = double.MaxValue;
                int indexOfFreqClosestToPREs = 0;

                // Get the closest freq to freq_PRE_GHz
                for (int i = 0; i < lines.Count; i++)
                {
                    deltaFreq = Math.Abs(this.freq_PRE_GHz - freq_Ghz_Array[i]);

                    if (deltaFreq_Min > deltaFreq)
                    {
                        deltaFreq_Min = deltaFreq;

                        indexOfFreqClosestToPREs = i;
                    }
                }

                oneChannel.Setup.IRear_mA = double.Parse(lines[indexOfFreqClosestToPREs][0]);

                returnData.AddDouble("FreqClosestToPREs", freq_Ghz_Array[indexOfFreqClosestToPREs]);
                returnData.AddReference("ISetup_FreqClosestToPREs", (object)oneChannel.Setup);
            } 
            #endregion

            return GetGenuineResult;
        }

        public Type UserControl
        {
            get { return (typeof(Mod_RearCutScanGui)); }
        }

        #endregion
    }
}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// IlmzTempTest_ND.cs
//
// Author: Paul.Annetts, Mark Fullalove 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Framework.Limits;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestSolution.ILMZFinalData;
using Bookham.TestLibrary.InstrTypes;
using Bookham.ToolKit.Mz;
using Bookham.TestSolution.IlmzCommonData;
using Bookham.TestSolution.IlmzCommonUtils;
using Bookham.TestSolution.IlmzCommonInstrs;
using Bookham.TestLibrary.Algorithms;
using Bookham.TestLibrary.Instruments;
using Bookham.TestLibrary.Utilities;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// ND Temp Test Module 
    /// </summary>
    public class IlmzTempTest_SPC : ITestModule
    {
        #region ITestModule Members
        ITestEngine spcEngine = null;
        FailModeCheck failModeCheck;

        /// <summary>
        ///  Module Test
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="userType"></param>
        /// <param name="configData"></param>
        /// <param name="instruments"></param>
        /// <param name="chassis"></param>
        /// <param name="calData"></param>
        /// <param name="previousTestData"></param>
        /// <returns></returns>
        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, DatumList configData, InstrumentCollection instruments, ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {
            string current_condition = configData.ReadString("current_condition");

            // Initialise GUI
            spcEngine = engine;
            engine.GuiShow();
            engine.GuiToFront();
            engine.SendToGui(new GuiTitleMessage("Hitt low cost SPC " + current_condition + " testing....."));
            engine.SendToGui(new GuiTextMessage("Initialising", 0));

            failModeCheck = configData.ReadReference("FailModeCheck") as FailModeCheck;

            // Initialise Baseline Channels
            baseLineChannels = (BaseLineData[])configData.ReadReference("BaseLineChannels");

            // Initialise Configs
            //Specification mainSpec = (Specification)configData.ReadReference("Specification");
            //dutSerialNbr = configData.ReadString("DutSerialNbr");
            //opmRange = configData.ReadDouble("OpmRangeInit_mW");
            string testStage = configData.ReadString("TestStage");
            string baseReferenceFile = configData.ReadString("BaseLineReferenceFile");//
            double[] powerOffsetArray = configData.ReadDoubleArray("PowerOffsetArray");

            // Initialise Instruments
            mzInstrs = (IlMzInstruments)configData.ReadReference("MzInstruments");
            //PowerHead = (IInstType_OpticalPowerMeter)instruments["PowerHead"];
            //Osa = (IInstType_OSA)instruments["OSA"];
            dsdbrTec = (IInstType_TecController)instruments["DsdbrTec"];

            // fcuSource = (InstType_ElectricalSource)instruments["FcuSource"];
            if (instruments.Contains("MzTec")) mzTec = (IInstType_TecController)instruments["MzTec"];
            //mzInstrs.PowerMeter.Range = opmRange;//echo remed
            Inst_Ke2510 Optical_switch = (Inst_Ke2510)instruments["Optical_switch"];
            DsdbrTuning.OpticalSwitch = Optical_switch;
            DsdbrUtils.opticalSwitch = Optical_switch;
            IInstType_DigitalIO OutLineCtap = Optical_switch.GetDigiIoLine(IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine);//line 3

            this.MZSourceMeasureDelay_ms = configData.ReadSint32("MZSourceMeasureDelay_ms");
            double minLevel_dBm = 0;
            DatumList retData = new DatumList();

            ///////                 do spc base or spc testing                      ////////////////////


            for (int chanNo = 0; chanNo < baseLineChannels.Length; chanNo++)
            {
                #region spc base or spec test for each channel

                //power up laser 
                #region power up laser
                engine.SendToGui(new GuiTextMessage("power up laser at channel " + chanNo.ToString(), 0));
                DsdbrChannelSetup baseLineCurrentGroup = baseLineChannels[chanNo].Dsdbr.Setup;
                DsdbrUtils.SetDsdbrCurrents_mA(baseLineCurrentGroup);
                mzInstrs.FCU2Asic.IimbLeft_mA = 0;
                mzInstrs.FCU2Asic.IimbRight_mA = 0;
                System.Threading.Thread.Sleep(1000);
                Measurements.FrequencyWithoutMeter = baseLineChannels[chanNo].Dsdbr.ItuFreq_GHz;
                #endregion

                //Do LV sweep. and set mz arm current


                #region Diff LV sweep
                #region DiffLV sweep setting
                double MZInitSweepStepSize_mV = configData.ReadDouble("MZInitSweepStepSize_mV");

                engine.SendToGui(new GuiTextMessage("MZ differential modulator sweep (LV sweep)...", 2));
                // Range = 0 to 2 x Vcm, fixed step size
                double Vcm = baseLineChannels[chanNo].vcm;
                int NumberOfSteps = (int)(1 + Math.Abs(Vcm * 2 / (MZInitSweepStepSize_mV / 1000)));
                bool mzSweepFailed;
                ILMZSweepResult mzDiffData;
                double LeftArmImb_mA = baseLineChannels[chanNo].MzLeftArmImb_mA;
                double RightArmImb_mA = baseLineChannels[chanNo].MzRightArmImb_mA;

                mzInstrs.FCU2Asic.IimbLeft_mA = float.Parse(LeftArmImb_mA.ToString());
                mzInstrs.FCU2Asic.IimbRight_mA = float.Parse(RightArmImb_mA.ToString());

                // Retry if necessary
                MzAnalysisWrapper.MzAnalysisResults mzDiffModResults;
                double MZTapBias_V = configData.ReadDouble("MZTapBias_V");
                //ilmzUtils.SetVoltage(SourceMeter.ComplementaryTap, MZTapBias_V);
                mzInstrs.PowerMeter.SetDefaultState();
                mzInstrs.PowerMeter.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_mW;
                mzInstrs.PowerMeter.Range = configData.ReadDouble("OpmRangeInit_mW");
                bool modArmSourceByAsic = true;
                #endregion

 #endregion

                #region do DiffLV sweep
                // Initialise Utils
                ilmzUtils = new IlMzDriverUtils(mzInstrs);
                do
                {
                    mzSweepFailed = false;
                    do
                    {
                        mzDiffData = ilmzUtils.ModArm_DifferentialSweepByTrigger(LeftArmImb_mA / 1000, RightArmImb_mA / 1000,
                        Vcm * 2, 0.0, NumberOfSteps,MZSourceMeasureDelay_ms, MZTapBias_V, MZTapBias_V, modArmSourceByAsic);
                        // If overrange run again
                        if (MzAnalysisWrapper.CheckForOverrange(mzDiffData.SweepData[ILMZSweepDataType.FibrePower_mW]))
                        {
                            dataOk = false;
                            //modify by tim at2008-09-04 for Ag8163 NaN;
                            mzInstrs.PowerMeter.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_mW;
                            mzInstrs.PowerMeter.Range = mzInstrs.PowerMeter.Range * 10;
                            string a = mzInstrs.PowerMeter.Range.ToString();
                            if (a == "NaN")
                            {
                                mzInstrs.PowerMeter.Range = 10;
                            }
                            //end
                            //mzInstrs.PowerMeter.Range = mzInstrs.PowerMeter.Range * 10;
                        }
                        else
                        {
                            // if underrange fix the data and continue
                            dataOk = true;
                            if (MzAnalysisWrapper.CheckForUnderrange(mzDiffData.SweepData[ILMZSweepDataType.FibrePower_mW]))
                            {
                                mzDiffData.SweepData[ILMZSweepDataType.FibrePower_mW] =
                                    MzAnalysisWrapper.FixUnderRangeData(mzDiffData.SweepData[ILMZSweepDataType.FibrePower_mW]
                                    , minLevel_dBm);
                            }
                        }
                    }
                    while (!dataOk);
                    // Analyse LV data
                    mzDiffModResults = MzAnalysisWrapper.Differential_NegChirp_DifferentialSweep(mzDiffData, Vcm, 0);

                    // Check that the data looked ok.
                    if (Math.Abs(mzDiffModResults.PowerAtMax_dBm) > 200)
                    {
                        // The power meter has probably over-ranged. Increase the range and try again.
                        engine.SendToGui(new GuiTextMessage("Retry MZ differential modulator sweep at higher power range", 3));
                        double opmRange = mzInstrs.PowerMeter.Range;
                        if (opmRange < 1)
                        {
                            mzInstrs.PowerMeter.Range = opmRange * 10;
                            mzSweepFailed = true;
                        }
                    }
                } while (mzSweepFailed == true);
                #endregion

                #region store plots and data
                engine.SendToGui(new GuiTextMessage("Create LV sweep file......", 4));
                // store plots
                int lvchanNbr = chanNo + 1;
                string MzFileDirectory = configData.ReadString("MzFileDirectory");
                string LVFileName = Util_GenerateFileName.GenWithTimestamp(MzFileDirectory,
                    "FinalDiffMzLV_Chan" + lvchanNbr.ToString() + current_condition, "", "csv");
                // write to file
                MzSweepFileWriter.WriteSweepData(LVFileName, mzDiffData,
                    new ILMZSweepDataType[] { ILMZSweepDataType.LeftArmModBias_V,
                                        ILMZSweepDataType.RightArmModBias_V,
                                        ILMZSweepDataType.FibrePower_mW,
                                        ILMZSweepDataType.TapComplementary_mA});
                // Add to archive
                //zipFile.AddFileToZip(LVFileName);
                engine.SendToGui(mzDiffData);
                #endregion

                //set mz to peak point

                #region set MZ to peak point
                engine.SendToGui(new GuiTextMessage("Set MZ to peak point......", 0));

                MzData peakData = new MzData();
                peakData.LeftArmImb_mA = LeftArmImb_mA;
                peakData.RightArmImb_mA = RightArmImb_mA;
                peakData.LeftArmMod_Peak_V = mzDiffModResults.Max_SrcL;
                peakData.RightArmMod_Peak_V = mzDiffModResults.Max_SrcR;

                IlMzDriverUtils.SetupMzToPeak(mzInstrs.FCU2Asic, peakData);
                #endregion

                //read frequency, power, smsr, locker current
                #region read frequency, power and locker current
                //engine.SendToGui(new GuiTextMessage("Test power at peak point.....", 0));

                engine.SendToGui(new GuiTextMessage("Test frequency ...", 0));

                mzInstrs.PowerMeter.Range = InstType_OpticalPowerMeter.AutoRange;
                double freq_GHz = 0;
                if (Measurements.Wavemeter != null
                    && Measurements.Wavemeter.IsOnline)
                {
                    freq_GHz = Measurements.ReadFrequency_GHz();
                }
                else
                {
                    //freq_GHz = DsdbrTuning.ReadFreq_GhzViaLowCostMeter(baseLineChannels[chanNo].Dsdbr.ItuFreq_GHz);
                    freq_GHz = DsdbrTuning.ReadFreq_GhzViaLowCostMeter();
                }
                engine.SendStatusMsg("Frequency of " + chanNo.ToString() + " channel is :" + freq_GHz.ToString() + " GHz");

                engine.SendToGui(new GuiTextMessage("Test power ....", 0));
                mzInstrs.PowerMeter.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;
                double uncalibratedPower = mzInstrs.PowerMeter.ReadPower();
                double power = uncalibratedPower + powerOffsetArray[chanNo];
                engine.SendStatusMsg("Power of " + chanNo.ToString() + " channel is :" + power.ToString() + " dBm");

                #endregion

                //power dissspation measurement,added by raul 2011.11.16

                #region Device measurements

                // Device, Power & TEC measurements
                engine.SendToGui(new GuiTextMessage("Device parameter measurements", 2));

                Measurements.PowerDissResults pdr = Measurements.ElecPowerDissipation(
                  baseLineCurrentGroup.FrontPair, dsdbrTec, mzTec);

                #endregion

                #region record test result,only record freq and power
                string deltaPrefix = "_delta";
                string datumPrefix = string.Empty;

                switch (chanNo)
                {
                    case 0:
                        datumPrefix = "_fst";
                        break;
                    case 1:
                        datumPrefix = "_mid";
                        break;
                    case 2:
                        datumPrefix = "_lst";
                        break;
                    default:
                        this.failModeCheck.RaiseError_Mod_NonParamFail(engine, "invalid base line template data, it must be 3 channels!", FailModeCategory_Enum.SW);
                        break;
                }

                //record ot freq and power
                if (current_condition == "Low_temp")
                {
                    retData.AddDouble("Freq_ch" + datumPrefix + "_Low", freq_GHz);
                    retData.AddDouble("Pwr_ch" + datumPrefix + "_Low", power);
                }
                else
                {
                    retData.AddDouble("Freq_ch" + datumPrefix + "_High", freq_GHz);
                    retData.AddDouble("Pwr_ch" + datumPrefix + "_High", power);
                    //raul added record high temp power dissipation parameters 2011.11.16
                    retData.AddDouble("TecDsdbrCurrent_ch" + datumPrefix + "_High", pdr.TecDsdbr_A);
                    retData.AddDouble("TecDsdbrVoltage_ch" + datumPrefix + "_High", pdr.TecDsdbr_V);
                    retData.AddDouble("PackagePower_ch" + datumPrefix + "_High", pdr.PackagePower_W);
                }

                if (!testStage.Contains("base"))
                {
                    CsvReader cr = new CsvReader();
                    List<string[]> ReferenceLines = cr.ReadFile(baseReferenceFile);
                    if (current_condition == "Low_temp")
                    {
                        retData.AddDouble("Freq_ch" + datumPrefix + "_lowtemp" + deltaPrefix,
                            freq_GHz - FindValueByName(engine, ReferenceLines, "FREQ_CH" + datumPrefix.ToUpper() + "_LOWTEMP"));
                        retData.AddDouble("Pwr_ch" + datumPrefix + "_lowtemp" + deltaPrefix,
                            power - FindValueByName(engine, ReferenceLines, "PWR_CH" + datumPrefix.ToUpper() + "_LOWTEMP"));
                    }
                    else
                    {
                        retData.AddDouble("Freq_ch" + datumPrefix + "_hightemp" + deltaPrefix,
                            freq_GHz - FindValueByName(engine, ReferenceLines, "FREQ_CH" + datumPrefix.ToUpper() + "_HIGHTEMP"));
                        retData.AddDouble("Pwr_ch" + datumPrefix + "_hightemp" + deltaPrefix,
                            power - FindValueByName(engine, ReferenceLines, "PWR_CH" + datumPrefix.ToUpper() + "_HIGHTEMP"));
                        //raul added record high temp power dissipation parameter
                        retData.AddDouble("TecDsdbrCurrent_ch" + datumPrefix + "_hightemp", pdr.TecDsdbr_A);
                        retData.AddDouble("TecDsdbrVoltage_ch" + datumPrefix + "_hightemp", pdr.TecDsdbr_V);
                        retData.AddDouble("PackagePower_ch" + datumPrefix + "_hightemp" + deltaPrefix,
                            pdr.PackagePower_W - FindValueByName(engine, ReferenceLines, "PACKAGEPOWER_CH" + datumPrefix.ToUpper() + "_H"));
                    }
                }
                engine.SendToGui(new GuiTextMessage("The" + chanNo.ToString() + " channel test finished!", 0));
            }
            return retData;
                #endregion

               
        }
        /// <summary>
        /// find base line reference data by parameter name
        /// </summary>
        /// <param name="lines">reference data lines</param>
        /// <param name="paramName">parameter name</param>
        /// <returns></returns>
        private double FindValueByName(ITestEngine engine, List<string[]> lines, string paramName)
        {
            for (int i = 1; i < lines.Count; i++) //first line is head title
            {
                for (int j = 0; j < lines[i].Length; j++)
                {
                    if (lines[i][j].Contains(paramName))
                    {
                        double retValue = 0;
                        try
                        {
                            retValue = Double.Parse(lines[i][j + 1]);
                        }
                        catch
                        {
                            this.failModeCheck.RaiseError_Mod_NonParamFail(engine, "invalid " + paramName + " item in base line reference file! \n please treat it and try this module again!", FailModeCategory_Enum.SW);
                        }
                        return retValue;
                    }
                }
            }
            this.failModeCheck.RaiseError_Mod_NonParamFail(engine, "The" + paramName + " item is not in reference file, pls check and try again!", FailModeCategory_Enum.SW);
            return 0;

        }

        public Type UserControl
        {
            get { return (typeof(IlmzTempTestGui)); }
        }
                #endregion
        #endregion
        #region Private data
        private int ChanTestNumber = 0;
        const int numRthTries = 10;
        const int rthDelay_mS = 500;

        const int numSetupIlmzTries = 3;

        double opmRange;
        bool dataOk;

        // Ilmz channels
        BaseLineData[] baseLineChannels;

        // Configs
        TcmzMzSweep_Config mzConfig;

        // Instruments
        IlMzInstruments mzInstrs;
        IInstType_OpticalPowerMeter PowerHead;
        IInstType_OSA Osa = null;
        IInstType_TecController dsdbrTec;
        IInstType_TecController mzTec = null;
        InstType_ElectricalSource fcuSource = null;

        // Utils
        IlMzDriverUtils ilmzUtils;
        //Util_ZipFile zipFile;
        TestSelection testSelect;
        Switch_Osa_MzOpm switchOsaMzOpm;


        double Dsdbr_temp_cl = 0;

        double Mz_vpeak_left_ch;
        double Mz_vpeak_right_ch;

        double Mz_vpi_ch;

        double Pwr_ch;

        double Smsr_ch;

        double Freq_ch;

        double Irx_ch;

        double Itx_ch;

        double Mz_DcEr_ch;

        double Mz_sweep_file_ch;

        int MZSourceMeasureDelay_ms = 5;
        #endregion
    }
}

﻿// Copyright (C), 1988-1999, Oclaro. Co., Ltd.
// Bookham.TestSolution.TestModules
// Mod_PhaseScan.cs
// Author: Echoxl.wang, 2011
// Design: XS documentation
// History:
// chongjian.liang, 2012.8.21, Modify this document to avoid RTH failed
// chongjian.liang, 2012.9.9, Modify this document to guarantee uploading .csv files' pointers to pcas when ModulesSkipped happens

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestSolution.Instruments;//Inst_Fcu
using Bookham.TestLibrary.Instruments;//Inst_fcu2asic
using Bookham.TestLibrary.Utilities; //csvReader
using System.IO;
using Bookham.TestEngine.Framework.Limits;
using Bookham.TestSolution.IlmzCommonData;
using Bookham.TestSolution.IlmzCommonUtils;
using Bookham.TestSolution.IlmzCommonInstrs;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// This module is to do phase scan before and after burn-in
    /// </summary>
    public class Mod_PhaseScan : ITestModule
    {
        #region Instrument and parameters fields

        bool isScanForRipple;
        DatumList returnData;
        string work_dir;
        string laser_id;
        string timestamp;
        float Igain_mA;
        float Iinitphase_mA;
        float Irear_mA;
        float Isoa_mA;
        float IRearSoa_mA;
        float IfsCon_mA;
        float IfsNonCon_mA;
        int fp;
        double Iasic_phasescan;
        float Ifsb_forphase_mA; // Only be calculated out in preburn, need send to PCAS
        List<int> JumpPointIndex;
        string PhaseSetupFile;
        string FSBCurrentSetupFile;

        float imbLeft_mA;
        float imbRight_mA;

        bool isRTHFailed;
        double rth_Limit_Min_ohm;
        double rth_Limit_Max_ohm;
        double rth_Min_ohm;
        double rth_Max_ohm;        
        double rth_Offset_ohm;

        // Define limit parameters
        int isFreqJumpOK;
        double Bragg_Freq_GHz;
        int Last_Jump_Index;
        int Last_Mode_Length;
        double Delta_Bragg_Freq_GHz;
        double Mode_Shift_Ratio_Percent;
        string FSB_Cut_file;
        string Mz_Phase_Cut_file;

        double preBurn_braggFreq_GHz;
        int preLastJumpIndex;
        int preLastModeLength;
        int preJumpCount;
        int prelastsecondJumpIndex;
        bool IsPostBurn;
        double lockRatioPointOffset = 0;
        double maxLockerRatioRangeOffset = 0;

        int sm_Index;
        List<double> rippleOfSM;
        List<int> rippleOfSMCount;

        Inst_Fcu2Asic Fcu2Asic;
        Inst_Ke2510 TecDsdbr;
        IInstType_ElectricalSource ASIC_VccSource;
        IInstType_ElectricalSource ASIC_VeeSource;
        InstType_ElectricalSource FCUMKI_Source;

        private const int UNTEST_PARAM_Value = -999;
        private string fail_Abort_Reason;

        #endregion

        #region ITestModule Members

        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, DatumList configData, InstrumentCollection instruments, ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {
            engine.GuiToFront();
            engine.GuiShow();

            // Initialize instrument and parameters
            Initialize(configData, instruments);

            // For phase scan for ripple module
            if (isScanForRipple &&
                (!configData.ReadBool("IsCanScanForRipple") || configData.ReadBool("ReturnNotTestedResult")))
            {
                return GetNotTestedResultForRippleScan;
            }
            // For phase scan module
            else if (configData.ReadBool("ReturnNotTestedResult"))
            {
                return GetNotTestedResult;
            }

            // Power up laser
            // Bookham.TestLibrary.FcuCommonUtils.DsdbrChannelSetup oneChannel = new Bookham.TestLibrary.FcuCommonUtils.DsdbrChannelSetup();
            DsdbrChannelData oneChannel = new DsdbrChannelData();
            oneChannel.Setup.IFsFirst_mA = IfsCon_mA;
            oneChannel.Setup.IFsSecond_mA = IfsNonCon_mA;
            oneChannel.Setup.IGain_mA = Igain_mA;
            oneChannel.Setup.IRear_mA = Irear_mA;
            oneChannel.Setup.ISoa_mA = Isoa_mA;
            oneChannel.Setup.IRearSoa_mA = IRearSoa_mA;
            oneChannel.Setup.FrontPair = fp;
            oneChannel.Setup.IPhase_mA = Iinitphase_mA;   //raul changed,the original is 0.001
            DsdbrUtils.SetDsdbrCurrents_mA(oneChannel.Setup);

            // Set up imb current
            Fcu2Asic.IimbLeft_mA = imbLeft_mA;
            Fcu2Asic.IimbRight_mA = imbRight_mA;

            #region Sweep FSB
            //================================================================================
            //raul added on 2011.11.21,required by zoe chen
            //sweep FSB current from 0.05-5 mA,get the most smooth fsb current for phase scan used.
            double[] freq_Ghz_Array = new double[0];
            double[] DUT_LockerRatio = new double[0];

            if (!IsPostBurn && !isScanForRipple)
            {
                engine.SendToGui("Do FSB Current Scan......");

                #region do FSB current scan
                CsvReader CsvReader = new CsvReader();
                List<string[]> lines_fsb = CsvReader.ReadFile(FSBCurrentSetupFile);
                Array.Resize(ref freq_Ghz_Array, lines_fsb.Count);
                Array.Resize(ref DUT_LockerRatio, lines_fsb.Count);
                for (int i = 0; i < lines_fsb.Count; i++)
                {
                    float Ifsb_mA = float.Parse(lines_fsb[i][0]);
                    Fcu2Asic.SetFrontPairCurrent_mA(fp, IfsCon_mA, Ifsb_mA);
                    System.Threading.Thread.Sleep(50);

                    freq_Ghz_Array[i] = Measurements.ReadFrequency_GHz();
                    DUT_LockerRatio[i] = DsdbrUtils.ReadLockerCurrents().LockRatio;
                    int p = 0;
                    try
                    {
                        double allPercent = lines_fsb.Count;
                        double percentage = i / allPercent * 100;
                        double pp = Math.Round(percentage);
                        p = int.Parse(pp.ToString());
                    }
                    catch (Exception e)
                    {
                        string a = e.Message;
                    }

                    engine.SendToGui(p);
                }
                engine.SendToGui(100);

                #endregion

                #region write FSB scan result to file

                File.Create(FSB_Cut_file).Close();

                StreamWriter sw = new StreamWriter(FSB_Cut_file);
                sw.WriteLine("FSB_Index,Ifsb_mA,Freq_Ghz,DUT_LockerRatio");
                for (int i = 0; i < lines_fsb.Count; i++)
                {
                    sw.Write(i.ToString() + ",");
                    sw.Write(lines_fsb[i][0] + ",");
                    sw.Write(freq_Ghz_Array[i].ToString() + ",");
                    sw.WriteLine(DUT_LockerRatio[i].ToString());
                    sw.Flush();
                }
                sw.Close();
                #endregion

                #region analysis the freq data to get the most smooth Ifsb
                double[] freq_delta_Array = new double[freq_Ghz_Array.Length - 1];  //delta array should less 1
                for (int freqCount = 0; freqCount < freq_Ghz_Array.Length - 1; freqCount++)
                {
                    freq_delta_Array[freqCount] = freq_Ghz_Array[freqCount + 1] - freq_Ghz_Array[freqCount];
                }
                //get the most smooth point from the array
                const double FREQ_TOLERENCE = 10;
                int fre_index = FindTheMostSmoothPoint(freq_delta_Array, FREQ_TOLERENCE);
                fre_index = fre_index + 1;
                Ifsb_forphase_mA = float.Parse(lines_fsb[fre_index][0]);

                #endregion

                //apply the fsb current for phase cut scan
                Fcu2Asic.SetFrontPairCurrent_mA(fp, IfsCon_mA, Ifsb_forphase_mA);
                System.Threading.Thread.Sleep(500);
            }
            //end added
            //============================================================================

            #endregion

            //record ASIC current before phase scan,required by zoe
            System.Threading.Thread.Sleep(500);

            Iasic_phasescan = ASIC_VccSource.CurrentActual_amp;

            #region Do phase cut scan

            if (isScanForRipple)
            {
                engine.SendToGui("Do Phase Current Scan for ripple......");
            }
            else
            {
                engine.SendToGui("Do Phase Current Scan......");
            }

            CsvReader cr = new CsvReader();
            List<string[]> lines = cr.ReadFile(PhaseSetupFile);

            if (isScanForRipple)
            {
                lines.RemoveRange(0, 14);
                lines.RemoveRange(lines.Count - 3, 3);
            }
            else
            {
                lines.RemoveRange(lines.Count - 4, 4);//zoe request to remove last 4 points by raul
            }
            
            double[] power_dBm_Array = new double[lines.Count];

            Array.Resize(ref freq_Ghz_Array, lines.Count);
            Array.Resize(ref DUT_LockerRatio, lines.Count);

            double[] rthArray = new double[lines.Count];

            for (int i = 0; i < lines.Count; i++)
            {
                double Iphase_mA = double.Parse(lines[i][0]);
                DsdbrUtils.SetSectionCurrent_mA(DSDBRSection.Phase, Iphase_mA);

                #region Read RTH every "StepWidthToReadRTH" points(Default is 10) - chongjian.liang - 2013.3.28

                if (i % configData.ReadDouble("StepWidthToReadRTH") == 0)
                {
                    // Leave some time to let tec stabilize when change dsdbr current obviouly.
                    if (i == 0)
                    {
                        System.Threading.Thread.Sleep(10);
                    }

                    DatumList result = null;

                    CheckRTH(engine, configData, ref rth_Max_ohm, ref rth_Min_ohm, out rthArray[i], out this.isRTHFailed, out result);

                    if (this.isRTHFailed)
                    {
                        return result;
                    }
                } 
                #endregion

                power_dBm_Array[i] = Measurements.MzHead.ReadPower();
                freq_Ghz_Array[i] = Measurements.ReadFrequency_GHz();
                DUT_LockerRatio[i] = DsdbrUtils.ReadLockerCurrents().LockRatio;

                int p = 0;
                try
                {
                    double allPercent = lines.Count;
                    double percentage = i / allPercent * 100;
                    double pp = Math.Round(percentage);
                    p = int.Parse(pp.ToString());
                }
                catch (Exception e)
                {
                    string a = e.Message;
                }

                engine.SendToGui(p);
            }
            engine.SendToGui(100);

            #endregion
            
            #region Check phase cut scan locker ratios by etalon align stage test data - chongjian.liang 2013.12.01

            if (!IsPostBurn && !isScanForRipple && configData.IsPresent("LM_MID_LINE_SWEEP_FILE"))
            {
                List<string[]> LM_MID_LINE_SWEEP_FILE = configData.ReadReference("LM_MID_LINE_SWEEP_FILE") as List<string[]>;

                const int LOCKER_RATIO_INDEX = 2;
                const int FREQUENCY_INDEX = 1;

                #region Check if locker ratio is in range with etalon align stage

                double peakLockerRatioOfEtalonAlign = double.MinValue;
                double troughLockerRatioOfEtalonAlign = double.MaxValue;

                // Get peak & trough locker ratio value of etalon align stage
                for (int i = 0; i < LM_MID_LINE_SWEEP_FILE.Count; i++)
                {
                    double lockerRatioOfEtalonAlign = double.Parse(LM_MID_LINE_SWEEP_FILE[i][LOCKER_RATIO_INDEX]);
                    
                    peakLockerRatioOfEtalonAlign = Math.Max(peakLockerRatioOfEtalonAlign, lockerRatioOfEtalonAlign);
                    troughLockerRatioOfEtalonAlign = Math.Min(troughLockerRatioOfEtalonAlign, lockerRatioOfEtalonAlign);
                }

                double peakLockerRatioOfPreStage = double.MinValue;
                double troughLockerRatioOfPreStage = double.MaxValue;

                // Get peak & trough locker ratio value of current pre stage
                foreach (double lockerRatio in DUT_LockerRatio)
                {
                    peakLockerRatioOfPreStage = Math.Max(peakLockerRatioOfPreStage, lockerRatio);
                    troughLockerRatioOfPreStage = Math.Min(troughLockerRatioOfPreStage, lockerRatio);
                }

                // Get the max of peak & trough locker ratio out of range value compare with the etalon align stage
                double peakLockerRatioRangeOffset = Math.Max(0, peakLockerRatioOfPreStage - peakLockerRatioOfEtalonAlign);
                double troughLockerRatioRangeOffset = Math.Max(0, troughLockerRatioOfEtalonAlign - troughLockerRatioOfPreStage);

                this.maxLockerRatioRangeOffset = Math.Max(peakLockerRatioRangeOffset, troughLockerRatioRangeOffset);

                #endregion

                #region Check locker ratio offset with etalon align stage

                double minOffset_GHz = double.MaxValue;
                double checkedPoint_GHz = double.NaN;
                double checkedPoint_LockerRatio = double.NaN;

                // Get the point closest to 0 or 25 or 50 or 75GHz from current phase scan
                for (int i = 0; i < DUT_LockerRatio.Length; i++)
                {
                    double offset_GHz = freq_Ghz_Array[i] % 25;

                    if (minOffset_GHz > offset_GHz)
                    {
                        minOffset_GHz = offset_GHz;
                        checkedPoint_GHz = freq_Ghz_Array[i];
                        checkedPoint_LockerRatio = DUT_LockerRatio[i];
                    }
                }

                // Get the point closet to same position of checkedPoint from etalon align stage data
                if (!double.IsNaN(checkedPoint_GHz))
                {
                    minOffset_GHz = double.MaxValue;

                    for (int i = 0; i < LM_MID_LINE_SWEEP_FILE.Count; i++)
                    {
                        double offset_GHz = Math.Abs(checkedPoint_GHz % 100 - double.Parse(LM_MID_LINE_SWEEP_FILE[i][FREQUENCY_INDEX]) % 100);

                        if (minOffset_GHz > offset_GHz)
                        {
                            minOffset_GHz = offset_GHz;
                            lockRatioPointOffset = Math.Abs(checkedPoint_LockerRatio - double.Parse(LM_MID_LINE_SWEEP_FILE[i][LOCKER_RATIO_INDEX]));
                        }
                    }
                }

                #endregion
            }
            #endregion

            #region Write scan result to file

            File.Create(Mz_Phase_Cut_file).Close();

            StreamWriter swriter = new StreamWriter(Mz_Phase_Cut_file);

            swriter.WriteLine("Phase_Index,IPhase_mA,Freq_Ghz,Power_dBm,DUT_LockerRatio,RTH");

            for (int i = 0; i < lines.Count; i++)
            {
                swriter.Write(i.ToString() + ",");
                swriter.Write(lines[i][0] + ",");
                swriter.Write(freq_Ghz_Array[i].ToString() + ",");
                swriter.Write(power_dBm_Array[i].ToString() + ",");
                swriter.Write(DUT_LockerRatio[i].ToString() + ",");
                swriter.WriteLine(rthArray[i].ToString());
                swriter.Flush();
            }
            swriter.Close(); 
            #endregion

            if (isScanForRipple)
            {
                double ripple_Min = configData.ReadDouble("Ripple_MIN");
                double ripple_Max = configData.ReadDouble("Ripple_MAX");
                int afterJumpIgnoreCount = configData.ReadSint32("AfterJumpIgnoreCount");
                int beforeJumpIgnoreCount = configData.ReadSint32("BeforeJumpIgnoreCount");
                double multiple = configData.ReadDouble("Multiple");
                string multipleFilterString = configData.ReadString("MultipleFilterString");

                RippleTestOperations.AnalyseRipple(freq_Ghz_Array, ripple_Min, ripple_Max, afterJumpIgnoreCount, beforeJumpIgnoreCount, multiple, multipleFilterString, out rippleOfSM, out rippleOfSMCount);
            }
            else
            {
                #region Analyse scan result

                // Analyse the freq data to get Bragg freq
                for (int freqIndex = 1; freqIndex < freq_Ghz_Array.Length; freqIndex++)
                {
                    if (Math.Abs(freq_Ghz_Array[freqIndex] - freq_Ghz_Array[freqIndex - 1]) > 50)
                    {
                        fail_Abort_Reason += "There is at least one frequency jump more than 50GHz, please call product engineer.";
                        
                        this.isFreqJumpOK = 0;

                        break;
                    }

                    if (freq_Ghz_Array[freqIndex] - freq_Ghz_Array[freqIndex - 1] < -30)
                    {
                        JumpPointIndex.Add(freqIndex);
                    }
                }

                // This data coming from production engineer,experiment results
                if (JumpPointIndex.Count >= 3)
                {
                    // check the jump freq should be less than 50G Hz
                    for (int i = 0; i < JumpPointIndex.Count; i++)
                    {
                        if (freq_Ghz_Array[JumpPointIndex[i]] - freq_Ghz_Array[JumpPointIndex[i] - 1] < -50)
                        {
                            fail_Abort_Reason += "There is at least one frequency jump less than -50GHz, please call product engineer.";

                            this.isFreqJumpOK = 0;

                            break;
                        }
                    }

                    // calculate bragg freq
                    if (freq_Ghz_Array[JumpPointIndex[0] - 1] - freq_Ghz_Array[0] > 30)
                    {
                        Bragg_Freq_GHz = ((freq_Ghz_Array[JumpPointIndex[0]]) + (freq_Ghz_Array[JumpPointIndex[0] - 1])) / 2;
                    }
                    else
                    {
                        for (int i = 1; i < JumpPointIndex.Count; i++)
                        {
                            if (freq_Ghz_Array[JumpPointIndex[i] - 1] - freq_Ghz_Array[JumpPointIndex[i - 1]] > 30)
                            {
                                Bragg_Freq_GHz = ((freq_Ghz_Array[JumpPointIndex[i]]) + (freq_Ghz_Array[JumpPointIndex[i] - 1])) / 2;
                                break;
                            }
                        }
                    }

                    Last_Jump_Index = JumpPointIndex[JumpPointIndex.Count - 1];

                    //if (JumpPointIndex.Count > 3)
                    //{
                    //    Bragg_Freq_GHz = ((freq_Ghz_Array[JumpPointIndex[i]]) + (freq_Ghz_Array[JumpPointIndex[i] - 1])) / 2;
                    //    Last_Jump_Index = JumpPointIndex[JumpPointIndex.Count - 1];
                    //}
                    //else if (JumpPointIndex.Count <= 3)
                    //{
                    //    Bragg_Freq_GHz = ((freq_Ghz_Array[JumpPointIndex[0]]) + (freq_Ghz_Array[JumpPointIndex[0]] - 1)) / 2;
                    //    Last_Jump_Index = JumpPointIndex[JumpPointIndex.Count - 1];
                    //}

                    if (!IsPostBurn)
                    {
                        Last_Mode_Length = Last_Jump_Index - JumpPointIndex[JumpPointIndex.Count - 2];
                    }

                    if (IsPostBurn)
                    {
                        Delta_Bragg_Freq_GHz = Bragg_Freq_GHz - preBurn_braggFreq_GHz;

                        if (preLastModeLength != 0)
                        {
                            if (JumpPointIndex.Count == preJumpCount)
                            {
                                if (Math.Abs(Last_Jump_Index - preLastJumpIndex) < 15)
                                {
                                    Mode_Shift_Ratio_Percent = (Last_Jump_Index - preLastJumpIndex);
                                    Mode_Shift_Ratio_Percent = Mode_Shift_Ratio_Percent * 100 / preLastModeLength;
                                }
                                else
                                {
                                    if (Last_Jump_Index > preLastJumpIndex)
                                    {
                                        Last_Jump_Index = JumpPointIndex[JumpPointIndex.Count - 2];
                                        Mode_Shift_Ratio_Percent = (Last_Jump_Index - preLastJumpIndex);
                                        Mode_Shift_Ratio_Percent = Mode_Shift_Ratio_Percent * 100 / preLastModeLength;
                                    }
                                    else
                                    {
                                        Mode_Shift_Ratio_Percent = (Last_Jump_Index - prelastsecondJumpIndex);
                                        Mode_Shift_Ratio_Percent = Mode_Shift_Ratio_Percent * 100 / preLastModeLength;
                                    }
                                }
                            }
                            else if (JumpPointIndex.Count > preJumpCount)
                            {
                                if (Math.Abs(Last_Jump_Index - preLastJumpIndex) < 15)
                                {
                                    Mode_Shift_Ratio_Percent = (Last_Jump_Index - preLastJumpIndex);
                                    Mode_Shift_Ratio_Percent = Mode_Shift_Ratio_Percent * 100 / preLastModeLength;
                                }
                                else
                                {
                                    Last_Jump_Index = JumpPointIndex[JumpPointIndex.Count - 2];
                                    Mode_Shift_Ratio_Percent = (Last_Jump_Index - preLastJumpIndex);
                                    Mode_Shift_Ratio_Percent = Mode_Shift_Ratio_Percent * 100 / preLastModeLength;
                                }
                            }
                            else if (JumpPointIndex.Count < preJumpCount)
                            {
                                if (Math.Abs(Last_Jump_Index - preLastJumpIndex) < 15)
                                {
                                    Mode_Shift_Ratio_Percent = (Last_Jump_Index - preLastJumpIndex);
                                    Mode_Shift_Ratio_Percent = Mode_Shift_Ratio_Percent * 100 / preLastModeLength;
                                }
                                else
                                {
                                    Mode_Shift_Ratio_Percent = (Last_Jump_Index - prelastsecondJumpIndex);
                                    Mode_Shift_Ratio_Percent = Mode_Shift_Ratio_Percent * 100 / preLastModeLength;
                                }
                            }
                        }
                    }
                }
                else if(IsPostBurn)
                {
                    fail_Abort_Reason += "The jump points count are less than 3, please drop to Bin or check the hardware.";

                    this.isFreqJumpOK = 0;
                }
                #endregion
            }

            return GetGenuineResult;
        }

        public Type UserControl
        {
            get { return (typeof(Mod_PhaseScanGui)); }
        }

        #endregion

        /// <summary>
        /// Initialize instrument and parameters
        /// </summary>
        /// <param name="configData"></param>
        /// <param name="instruments"></param>
        private void Initialize(DatumList configData, InstrumentCollection instruments)
        {
            this.fail_Abort_Reason = null;

            returnData = new DatumList();

            work_dir = configData.ReadString("RESULT_DIR");
            laser_id = configData.ReadString("LASER_ID");
            timestamp = configData.ReadString("TIME_STAMP");

            Igain_mA = (float)configData.ReadDouble("Igain_mA");
            Irear_mA = (float)configData.ReadDouble("Irear_mA");
            Isoa_mA = (float)configData.ReadDouble("Isoa_mA");
            IRearSoa_mA = (float)configData.ReadDouble("IrearSoa_mA");
            IfsCon_mA = (float)configData.ReadDouble("IfsCon_mA");
            IfsNonCon_mA = (float)configData.ReadDouble("IfsNonCon_mA");
            fp = configData.ReadSint32("FrontPair");
            PhaseSetupFile = configData.ReadString("PhaseSetupFile_Path");
            JumpPointIndex = new List<int>();

            imbLeft_mA = (float)configData.ReadDouble("ImbLeft_mA");
            imbRight_mA = (float)configData.ReadDouble("ImbRight_mA");

            isRTHFailed = false;

            rth_Limit_Min_ohm = configData.ReadDouble("RTH_LIMIT_MIN_ohm");
            rth_Limit_Max_ohm = configData.ReadDouble("RTH_LIMIT_MAX_ohm");

            rth_Min_ohm = configData.ReadDouble("RTH_MIN_ohm");
            rth_Max_ohm = configData.ReadDouble("RTH_MAX_ohm");

            isFreqJumpOK = 1;

            if (configData.IsPresent("IsScanForRipple"))
            {
                isScanForRipple = configData.ReadBool("IsScanForRipple");
            }

            if (!isScanForRipple)
            {
                Iinitphase_mA = (float)configData.ReadDouble("Iphase_mA");
                FSBCurrentSetupFile = configData.ReadString("FSBCurrentSetupFile_Path"); //raul added
                
                rth_Offset_ohm = configData.ReadDouble("RTH_Offset_ohm"); //Jack.zhang for post limit file RTH update. 2012-05-09
                
                FSB_Cut_file = Path.Combine(work_dir, string.Format("{0}_FSBCutScan_{1}.csv", laser_id, timestamp));
            }
            else
            {
                sm_Index = configData.ReadSint32("SM_INDEX");
            }

            Mz_Phase_Cut_file = Path.Combine(work_dir, string.Format("{0}_PhaseCutScan_{1}.csv", laser_id, timestamp));

            IsPostBurn = configData.ReadBool("IsPostBurn");

            if (IsPostBurn)
            {
                preBurn_braggFreq_GHz = configData.ReadDouble("preBurn_braggFreq_GHz");
                preLastJumpIndex = configData.ReadSint32("preLastJumpIndex");
                preLastModeLength = configData.ReadSint32("preLastModeLength");
                preJumpCount = configData.ReadSint32("preJumpCount");
                prelastsecondJumpIndex = configData.ReadSint32("prelastsecondJumpIndex");
            }

            Fcu2Asic = (Inst_Fcu2Asic)instruments["Fcu2Asic"];
            TecDsdbr = (Inst_Ke2510)instruments["TecDsdbr"];
            ASIC_VccSource = (IInstType_ElectricalSource)instruments["ASIC_VccSource"];
            ASIC_VeeSource = (IInstType_ElectricalSource)instruments["ASIC_VeeSource"];

            if (instruments.Contains("FCUMKI_Source"))
            {
                FCUMKI_Source = (InstType_ElectricalSource)instruments["FCUMKI_Source"];
            }

            if (Fcu2Asic.IsOnline == false)
            {
                Fcu2Asic.IsOnline = true;
            }
        }

        private void CheckRTH(ITestEngine engine, DatumList configData, ref double rth_Max_ohm, ref double rth_Min_ohm, out double rth_ohm, out bool isRTHFailed, out DatumList result)
        {
            // Jack.zhang for post limit file RTH update. 2012-05-09
            rth_ohm = TecDsdbr.SensorResistanceActual_ohm + rth_Offset_ohm;

            int timeWaitCount = 0;

            result = null;

            isRTHFailed = false;

            while (rth_ohm < rth_Limit_Min_ohm || rth_ohm > rth_Limit_Max_ohm)
            {
                // Check RTH of TecDsdbr every 0.5s
                System.Threading.Thread.Sleep(500);

                rth_ohm = TecDsdbr.SensorResistanceActual_ohm + rth_Offset_ohm;

                // If RTH is still out of limits after 5*0.5s=2.5s, mark RTH failed
                if (timeWaitCount++ >= 5)
                {
                    if (!isScanForRipple)
                    {
                        isRTHFailed = true;

                        // IsReturnDummyResult is true to return a not failed result
                        if (configData.ReadBool("IsReturnDummyResult"))
                        {
                            ASIC_VccSource.OutputEnabled = false;
                            ASIC_VeeSource.OutputEnabled = false;
                            FCUMKI_Source.OutputEnabled = false;
                            TecDsdbr.OutputEnabled = false;

                            result = GetDummyResult;
                        }
                        else // IsReturnDummyResult is false to return the real result
                        {
                            rth_Max_ohm = Math.Max(rth_Max_ohm, rth_ohm);
                            rth_Min_ohm = Math.Min(rth_Min_ohm, rth_ohm);

                            fail_Abort_Reason += "RTH failed 3 times, please retest this device by another test kit.";

                            engine.ShowContinueUserQuery(fail_Abort_Reason + " 请在其他测试台重新测试这个器件.");

                            result = GetRTHFailedResult;
                        }
                    }

                    return;
                }
            }

            rth_Max_ohm = Math.Max(rth_Max_ohm, rth_ohm);
            rth_Min_ohm = Math.Min(rth_Min_ohm, rth_ohm);
        }

        /// <summary>
        /// The real result, whether passed or failed
        /// </summary>
        private DatumList GetGenuineResult
        {
            get
            {
                if (!string.IsNullOrEmpty(fail_Abort_Reason))
                {
                    returnData.AddString("FAIL_ABORT_REASON", fail_Abort_Reason);
                }

                returnData.AddBool("IsRTHFailed", this.isRTHFailed);
                returnData.AddDouble("RTH_MIN_ohm", this.rth_Min_ohm);
                returnData.AddDouble("RTH_MAX_ohm", this.rth_Max_ohm);

                if (isScanForRipple)
                {
                    returnData.AddDouble("RippleOfSM" + sm_Index, rippleOfSM[0]);
                    returnData.AddSint32("RippleOfSM" + sm_Index + "_Jumps", rippleOfSMCount[0]);
                    returnData.AddFileLink("PhaseScanFileSM" + sm_Index, Mz_Phase_Cut_file);
                    return returnData;
                }
                else
                {
                    returnData.AddSint32("PHASE_SCAN_FREQ_JUMP_OK", this.isFreqJumpOK);
                    returnData.AddDouble("Bragg_Freq_GHz", Math.Round(Bragg_Freq_GHz, 3));
                    returnData.AddSint32("Last_Jump_IndexPoint", Last_Jump_Index);
                    returnData.AddDouble("Iasic_PhaseScan", Iasic_phasescan);
                    returnData.AddFileLink("Mz_Phase_Cut_file", Mz_Phase_Cut_file);

                    if (!IsPostBurn)
                    {
                        returnData.AddFileLink("FSB_Cut_file", FSB_Cut_file);
                        returnData.AddDouble("Ifsb_forphase_mA", Ifsb_forphase_mA);
                        returnData.AddSint32("Last_Mode_length", Last_Mode_Length);
                        returnData.AddSint32("Count_JumpPoint", JumpPointIndex.Count);
                        returnData.AddDouble("LK_KEY_POINTS_OFFSET", Math.Round(this.lockRatioPointOffset, 3));
                        returnData.AddDouble("LK_RANGE_OFFSET", Math.Round(this.maxLockerRatioRangeOffset, 3));

                        if (JumpPointIndex.Count - 2 >= 0)
                        {
                            returnData.AddSint32("Pre_Last_Jump_Index", JumpPointIndex[JumpPointIndex.Count - 2]);
                        }
                        else
                        {
                            returnData.AddSint32("Pre_Last_Jump_Index", UNTEST_PARAM_Value);
                        }
                    }
                    else
                    {
                        returnData.AddDouble("Delta_Bragg_Freq_GHz", Math.Round(Delta_Bragg_Freq_GHz, 3));
                        returnData.AddDouble("Mode_Shift_Ratio_Percent", Math.Round(Mode_Shift_Ratio_Percent, 3));
                    }

                    return returnData;
                }
            }
        }

        /// <summary>
        /// When RTH failed, we need a dummy passed test result to suppress the module failed dialogue, so that we can start over the program
        /// </summary>
        private DatumList GetDummyResult
        {
            get
            {
                returnData.AddBool("IsRTHFailed", this.isRTHFailed);
                returnData.AddDouble("RTH_MIN_ohm", this.rth_Limit_Min_ohm);
                returnData.AddDouble("RTH_MAX_ohm", this.rth_Limit_Max_ohm);

                // Add dummy result with required limits
                returnData.AddFileLink("Mz_Phase_Cut_file", Mz_Phase_Cut_file);
                returnData.AddSint32("PHASE_SCAN_FREQ_JUMP_OK", 1);
                returnData.AddDouble("Bragg_Freq_GHz", 0);
                returnData.AddSint32("Last_Jump_IndexPoint", 0);
                returnData.AddDouble("Iasic_PhaseScan", 0);

                if (!IsPostBurn)
                {
                    // Add dummy result for PreBurn
                    returnData.AddFileLink("FSB_Cut_file", FSB_Cut_file);
                    returnData.AddDouble("Ifsb_forphase_mA", 0);
                    returnData.AddSint32("Last_Mode_length", 0);
                    returnData.AddSint32("Count_JumpPoint", 3);
                    returnData.AddSint32("Pre_Last_Jump_Index", 0);
                    returnData.AddDouble("LK_KEY_POINTS_OFFSET", 0);
                    returnData.AddDouble("LK_RANGE_OFFSET", 0);
                }
                else
                {
                    // Add dummy result for PostBurn
                    returnData.AddDouble("Delta_Bragg_Freq_GHz", 0);
                    returnData.AddDouble("Mode_Shift_Ratio_Percent", 0);
                }

                return returnData;
            }
        }

        /// <summary>
        /// Return not tested result when precedent modules failed or aborted
        /// </summary>
        private DatumList GetNotTestedResult
        {
            get
            {
                returnData.AddDouble("RTH_MIN_ohm", UNTEST_PARAM_Value);
                returnData.AddDouble("RTH_MAX_ohm", UNTEST_PARAM_Value);

                returnData.AddFileLink("Mz_Phase_Cut_file", Mz_Phase_Cut_file);
                returnData.AddSint32("PHASE_SCAN_FREQ_JUMP_OK", UNTEST_PARAM_Value);
                returnData.AddDouble("Bragg_Freq_GHz", UNTEST_PARAM_Value);
                returnData.AddSint32("Last_Jump_IndexPoint", UNTEST_PARAM_Value);
                returnData.AddDouble("Iasic_PhaseScan", UNTEST_PARAM_Value);

                if (!IsPostBurn)
                {
                    returnData.AddFileLink("FSB_Cut_file", FSB_Cut_file);
                    returnData.AddDouble("Ifsb_forphase_mA", UNTEST_PARAM_Value);
                    returnData.AddSint32("Last_Mode_length", UNTEST_PARAM_Value);
                    returnData.AddSint32("Count_JumpPoint", UNTEST_PARAM_Value);
                    returnData.AddSint32("Pre_Last_Jump_Index", UNTEST_PARAM_Value);
                    returnData.AddDouble("LK_KEY_POINTS_OFFSET", UNTEST_PARAM_Value);
                    returnData.AddDouble("LK_RANGE_OFFSET", UNTEST_PARAM_Value);
                }
                else
                {
                    returnData.AddDouble("Delta_Bragg_Freq_GHz", UNTEST_PARAM_Value);
                    returnData.AddDouble("Mode_Shift_Ratio_Percent", UNTEST_PARAM_Value);
                }

                return returnData;
            }
        }

        /// <summary>
        /// Return not tested result for ripple scan module when no ripple scan result in COC test
        /// </summary>
        private DatumList GetNotTestedResultForRippleScan
        {
            get
            {
                returnData.AddDouble("RTH_MIN_ohm", this.rth_Min_ohm);
                returnData.AddDouble("RTH_MAX_ohm", this.rth_Max_ohm);

                string emptyFilePath = Path.Combine(work_dir, string.Format("{0}_EmptyFile_{1}.csv", laser_id, timestamp));

                using (StreamWriter csvWriter = new StreamWriter(File.Create(emptyFilePath)))
                {
                    csvWriter.WriteLine("Phase_Index,IPhase_mA,Freq_Ghz,Power_dBm,DUT_LockerRatio,RTH");
                    csvWriter.WriteLine("0,0,0,0,0,0");
                }

                returnData.AddDouble("RippleOfSM" + sm_Index, UNTEST_PARAM_Value);
                returnData.AddSint32("RippleOfSM" + sm_Index + "_Jumps", UNTEST_PARAM_Value);
                returnData.AddFileLink("PhaseScanFileSM" + sm_Index, emptyFilePath);

                return returnData;
            }
        }
        
        /// <summary>
        /// Return RTH failed result
        /// </summary>
        private DatumList GetRTHFailedResult
        {
            get
            {
                returnData.AddBool("IsRTHFailed", this.isRTHFailed);
                returnData.AddString("FAIL_ABORT_REASON", fail_Abort_Reason);

                returnData.AddDouble("RTH_MIN_ohm", this.rth_Min_ohm);
                returnData.AddDouble("RTH_MAX_ohm", this.rth_Max_ohm);

                returnData.AddFileLink("Mz_Phase_Cut_file", Mz_Phase_Cut_file);
                returnData.AddSint32("PHASE_SCAN_FREQ_JUMP_OK", UNTEST_PARAM_Value);
                returnData.AddDouble("Bragg_Freq_GHz", UNTEST_PARAM_Value);
                returnData.AddSint32("Last_Jump_IndexPoint", UNTEST_PARAM_Value);
                returnData.AddDouble("Iasic_PhaseScan", UNTEST_PARAM_Value);

                if (!IsPostBurn)
                {
                    returnData.AddFileLink("FSB_Cut_file", FSB_Cut_file);
                    returnData.AddDouble("Ifsb_forphase_mA", UNTEST_PARAM_Value);
                    returnData.AddSint32("Last_Mode_length", UNTEST_PARAM_Value);
                    returnData.AddSint32("Count_JumpPoint", UNTEST_PARAM_Value);
                    returnData.AddSint32("Pre_Last_Jump_Index", UNTEST_PARAM_Value);
                    returnData.AddDouble("LK_KEY_POINTS_OFFSET", UNTEST_PARAM_Value);
                    returnData.AddDouble("LK_RANGE_OFFSET", UNTEST_PARAM_Value);
                }
                else
                {
                    returnData.AddDouble("Delta_Bragg_Freq_GHz", UNTEST_PARAM_Value);
                    returnData.AddDouble("Mode_Shift_Ratio_Percent", UNTEST_PARAM_Value);
                }

                return returnData;
            }
        }

        /// <summary>
        /// Find the most smooth point from the array
        /// </summary>
        /// <param name="array"> sweep data</param>
        /// <param name="freq_tolerence"> freq tolerence</param>
        private int FindTheMostSmoothPoint(double[] array, double freq_tolerence)
        {
            int index = 0;
            List<int> jumpPointIndex = new List<int>();
            for (int i = 0; i < array.Length; i++)
            {
                if (Math.Abs(array[i]) > freq_tolerence)
                    jumpPointIndex.Add(i);
            }
            if (jumpPointIndex.Count == 0)  //if no jump point, then get the middle point.
            {
                if (array.Length % 2 == 0) index = array.Length / 2;
                else index = (array.Length + 1) / 2;
            }
            else if (jumpPointIndex.Count == 1) //if one jump point,then judge which area is longer
            {
                if (jumpPointIndex[0] > 5)  //confirm continue 5 points as the smooth area
                {
                    if (jumpPointIndex[0] % 2 == 0) index = jumpPointIndex[0] / 2;
                    else index = (jumpPointIndex[0] + 1) / 2;
                }
                else
                {
                    // the Ifsb for index 15 is 2.92mA, the Ifsb should be less than 3mA
                    if ((15 - jumpPointIndex[0]) % 2 == 0) index = jumpPointIndex[0] + (15 - jumpPointIndex[0]) / 2;
                    else index = jumpPointIndex[0] + ((15 - jumpPointIndex[0]) + 1) / 2;
                }
            }
            else //if more than one jump point,then devide into several piece
            {
                if (jumpPointIndex[0] > 5)    //the first piece
                {
                    if (jumpPointIndex[0] % 2 == 0) index = jumpPointIndex[0] / 2;
                    else index = (jumpPointIndex[0] + 1) / 2;
                }
                else
                {
                    for (int i = 0; i < jumpPointIndex.Count - 1; i++)  //find the point in the middle part(between the first and the last jump point)
                    {
                        if ((jumpPointIndex[i + 1] - jumpPointIndex[i]) > 5)
                        {
                            if ((jumpPointIndex[i + 1] - jumpPointIndex[i]) % 2 == 0)
                                index = (jumpPointIndex[i + 1] + jumpPointIndex[i]) / 2;
                            else
                                index = (jumpPointIndex[i + 1] + jumpPointIndex[i] + 1) / 2;
                            break;
                        }
                    }
                    if (index == 0) //still not find in middla part
                    {
                        if ((array.Length - 1 - jumpPointIndex[jumpPointIndex.Count - 1]) > 5) //the last piece
                        {
                            if ((array.Length - 1 - jumpPointIndex[jumpPointIndex.Count - 1]) % 2 == 0)
                                index = jumpPointIndex[jumpPointIndex.Count - 1] + (array.Length - 1 - jumpPointIndex[jumpPointIndex.Count - 1]) / 2;
                            else
                                index = jumpPointIndex[jumpPointIndex.Count - 1] + (array.Length - jumpPointIndex[jumpPointIndex.Count - 1]) / 2;
                        }
                    }
                }
            }
            return index;
        }
    }
}

// Copyright (C), 1988-1999, Oclaro. Co., Ltd.
// Bookham.TestSolution.TestModules
// Mod_IlmzMzCharacterise_ND.cs
// Author: Paul.Annetts, Mark Fullalove 2007
// Design: XS documentation
// History:
// chongjian.liang, 2012.9.26, modify this document according to review result.
// chongjian.liang, 2012.12.12, modify this document to remedy Mz Characterise method.

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestSolution.IlmzCommonData;
using Bookham.TestSolution.IlmzCommonInstrs;
using Bookham.TestSolution.IlmzCommonUtils;
using Bookham.ToolKit.Mz;
using Bookham.TestLibrary.Algorithms;
using Bookham.TestSolution.ILMZFinalData;
using Bookham.TestEngine.Framework.Limits;
using Bookham.TestLibrary.Utilities;
using ICSharpCode.SharpZipLib.Zip;
using System.IO;
using Bookham.TestSolution.TestModules;
using Bookham.TestLibrary.Instruments;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.Utilities;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// TCMZ MZ Characterisation module - does MZ sweepResult at various ITU channels
    /// 
    /// Negative chirp, differential sweep
    /// 
    /// </summary>
    public class IlmzMzCharacterise_ND : ITestModule
    {
        #region private data
        private ITestEngine engine;
        private Specification testSpec;
        private IlMzDriverUtils mzDriverUtils;
        private double[] vcm_V;
        private double mzInitFixedModBias_volt;
        private double mzInitSweepMaxAbs_V;
        private double mzVleftMinQuadLimit;
        private double mzVleftMaxQuadLimit;
        private double vcmMinLimit;
        private double vcmMaxLimit;
        private double vpiMinLimit;
        private double vpiMaxLimit;
        private double targetVpi_V;
        private double dcerMinLimit;
        private double dcerMaxLimit;
        private double mzVleftInset = 0.05;
        private double initialVcmOffset = 0;
        private bool initialOffsetSelected;
        private IInstType_DigiIOCollection OpticalSwitch = null;

        private int NumberPoints;
        private double MZTapBias_V;
        private double tapInline_V;
        private string MzFileDirectory;
        private string dutSerialNbr;
        private List<string> allSweepFilesNames = new List<string>();
        private bool ArmSourceByAsic = true;
        private int indexOf3Channels;//indicate testing channel is first channel or last channel

        private double VcmFactorFirstChannel = 0;
        private double VcmFactorMiddleChannel = 0;
        private double VcmFactorLastChannel = 0;

        private double iPi_mA = 0;
        private double seVpeakL_V = 0;//left voltage on peak point when do single ended Left arm LV sweep.
        private double seVR_V = 0;//right arm voltage when do single ended left arm sweep.
        private double seVpi_V = 0;//vpi when do single ended Left arm sweep.
        private bool IsMZBiasesMustSimilar;

        #endregion private data

        /// <summary>
        /// Define the preliminary IMB should be at Quad or MIN point
        /// The Quad point results in minimum Vpi and lower loss
        /// The MIN point results in maximum bias on the left arm. 
        /// </summary>
        private Alg_MZAnalysis.MzFeaturePowerType featureToCenter;
        private double vcm_FirstChannel_V = 0;
        private double vcm_MiddleChannel_V = 0;
        private double vcm_LastChannel_V = 0;
        private double vmax_firstChan = 0;
        private double vmax_middleChan = 0;
        private double vmax_lastChan = 0;
        private double Target_DCER = 0.0;
        private double Margin_ER = 2; // Change this value from 0.5 to 2dBm according to product engineer's requirement. - chongjian.liang 2012.12.6
        private double margin = 0.25;
        private double[] OptimizedVcm = new double[3];
        private double vpiOffset_V = 0;
        private double LeftBias_V_LI_Sweep_FirstChan = 0;
        private double RightBias_V_LI_Sweep_FirstChan = 0;

        private double LeftBias_V_LI_Sweep_MiddleChan = 0;
        private double RightBias_V_LI_Sweep_MiddleChan = 0;

        private double LeftBias_V_LI_Sweep_LastChan = 0;
        private double RightBias_V_LI_Sweep_LastChan = 0;
        private int MZSourceMeasureDelay_ms = 5;
        MzAnalysisWrapper.MzAnalysisResults shiftedLvResults;
        private FailModeCheck failModeCheck;

        #region ITestModule Members

        /// <summary>
        /// 
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="userType"></param>
        /// <param name="configData"></param>
        /// <param name="instruments"></param>
        /// <param name="chassis"></param>
        /// <param name="calData"></param>
        /// <param name="previousTestData"></param>
        /// <returns></returns>
        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType,
            DatumList configData, InstrumentCollection instruments,
            ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {
            #region read config

            this.engine = engine;
            this.failModeCheck = configData.ReadReference("FailModeCheck") as FailModeCheck;

            // get MZ instruments
            IlMzInstruments mzInstrs = (IlMzInstruments)configData.ReadReference("MzInstruments");

            //this.ArmSourceByAsic = configData.ReadBool("ArmSourceByAsic");
            double midChannelFrequency = configData.ReadDouble("MidChannelFrequency");    // May not be the middle channel for all codes

            // Is this a Low cost testset ?
            double locker_tran_pot = 0;
            if (ArmSourceByAsic)
            {
                mzInstrs.FCU2Asic = (Inst_Fcu2Asic)instruments["FCU"];
                OpticalSwitch = (IInstType_DigiIOCollection)instruments["Optical_switch"];
                locker_tran_pot = configData.ReadDouble("locker_tran_pot");
                mzInstrs.ImbsSourceType = IlMzInstruments.EnumSourceType.Asic;
            }

            // get list of DSDBR channels
            DsdbrChannelData[] ituChannels = (DsdbrChannelData[])previousTestData.ReadReference("ItuChannels");
            // get test spec (for limit checking)
            testSpec = (Specification)configData.ReadReference("TestSpec");
            MzItuAtVcmData.InitLookup(testSpec);

            this.vcmMinLimit = Convert.ToDouble(testSpec.GetParamLimit("TC_MZ_VCM_CAL_LIMIT_MIN").LowLimit.ValueToString());
            this.vcmMaxLimit = Convert.ToDouble(testSpec.GetParamLimit("TC_MZ_VCM_CAL_LIMIT_MAX").HighLimit.ValueToString());
            this.vpiMinLimit = Convert.ToDouble(testSpec.GetParamLimit("TC_MZ_VPI_CAL_LIMIT_MIN").LowLimit.ValueToString());
            this.vpiMaxLimit = Convert.ToDouble(testSpec.GetParamLimit("TC_MZ_VPI_CAL_LIMIT_MAX").HighLimit.ValueToString());
            this.targetVpi_V = vpiMaxLimit - 0.1;
            this.mzVleftMinQuadLimit = Convert.ToDouble(testSpec.GetParamLimit("CH_MZ_BIAS_L_QUAD_V").LowLimit.ValueToString());
            this.mzVleftMaxQuadLimit = Convert.ToDouble(testSpec.GetParamLimit("CH_MZ_BIAS_L_QUAD_V").HighLimit.ValueToString());
            this.dcerMinLimit = Convert.ToDouble(testSpec.GetParamLimit("CH_MZ_ER").LowLimit.ValueToString()); ;
            this.dcerMaxLimit = Convert.ToDouble(testSpec.GetParamLimit("CH_MZ_ER").HighLimit.ValueToString()); ;

            // These two are overridden for debugging only to help to force software into using more imbalance
            //this.vcmMinLimit = Convert.ToDouble(testSpec.GetParamLimit("TC_MZ_VCM_CAL_LIMIT_MIN").LowLimit.ValueToString());// -4.5;
            //this.targetVpi_V = Convert.ToDouble(testSpec.GetParamLimit("TC_MZ_VPI_CAL_LIMIT_MAX").LowLimit.ValueToString());// 3;

            /*
             * Set this next variable to select the test method.
             * 
             * Quad at zero will allow the quadrature point to move close to zero when Vpi is large.
             * Min at zero follows the original XS, keeping the NULL of the response at zero.
             * 
             */
            // In order to achieve maximum bias depth and hence minimise Vpi we use the imbalance currents to set quadrature close to zero
            this.featureToCenter = Alg_MZAnalysis.MzFeaturePowerType.Quad;  // Quadrature to minimise Vpi            
            //this.featureToCenter = Alg_MZAnalysis.MzFeaturePowerType.Min;  // Quadrature to minimise Vpi            

            this.mzInitFixedModBias_volt = configData.ReadDouble("MZInitFixedModBias_volt");
            this.mzInitSweepMaxAbs_V = configData.ReadDouble("MZInitSweepMaxAbs_V");

            // details to go into our filenames
            this.MzFileDirectory = configData.ReadString("ResultDir");
            this.dutSerialNbr = configData.ReadString("DutSerialNbr");
            // MZ scan setup data
            this.vcm_V = configData.ReadDoubleArray("MZFixedModBias_volt");
            this.NumberPoints = configData.ReadSint32("NumberOfPoints");
            this.MZTapBias_V = configData.ReadDouble("MZTapBias_V");
            this.VcmFactorFirstChannel = configData.ReadDouble("VcmFactor_FirstChan");
            this.VcmFactorMiddleChannel = configData.ReadDouble("VcmFactor_MiddleChan");
            this.VcmFactorLastChannel = configData.ReadDouble("VcmFactor_LastChan");
            this.vcm_FirstChannel_V = configData.ReadDouble("MzFixedModBias_volt_firstChan");
            this.vcm_MiddleChannel_V = configData.ReadDouble("MzFixedModBias_volt_middleChan");
            this.vcm_LastChannel_V = configData.ReadDouble("MzFixedModBias_volt_lastChan");
            this.vmax_firstChan = configData.ReadDouble("Vmax_firstChan");//-1
            this.vmax_middleChan = configData.ReadDouble("Vmax_middleChan");
            this.vmax_lastChan = configData.ReadDouble("Vmax_lastChan");//-0.5
            this.Target_DCER = configData.ReadDouble("Target_ER");
            this.vpiOffset_V = configData.ReadDouble("Vpi_offset");//0.2
            this.IsMZBiasesMustSimilar = configData.ReadBool("IsMZBiasesMustSimilar");

            //this.shiftLVWhenQuadLlessThan = configData.ReadDouble("shiftLVWhenQuadLlessThan");//-4; 

            if (mzInstrs.InlineTapOnline)
            {
                this.tapInline_V = configData.ReadDouble("MZInlineTapBias_V");
            }
            else
            {
                // belt and braces - this should be the value anyway by C# default.
                this.tapInline_V = 0.0;
            }

            this.LeftBias_V_LI_Sweep_FirstChan = configData.ReadDouble("LeftBias_V_LI_Sweep_FirstChan");
            this.LeftBias_V_LI_Sweep_MiddleChan = configData.ReadDouble("LeftBias_V_LI_Sweep_MiddleChan");
            this.LeftBias_V_LI_Sweep_LastChan = configData.ReadDouble("LeftBias_V_LI_Sweep_LastChan");
            this.RightBias_V_LI_Sweep_FirstChan = configData.ReadDouble("RightBias_V_LI_Sweep_FirstChan");
            this.RightBias_V_LI_Sweep_MiddleChan = configData.ReadDouble("RightBias_V_LI_Sweep_MiddleChan");
            this.RightBias_V_LI_Sweep_LastChan = configData.ReadDouble("RightBias_V_LI_Sweep_LastChan");
            this.MZSourceMeasureDelay_ms = configData.ReadSint32("MZSourceMeasureDelay_ms");
            //this.OxideWafer = configData.ReadBool("OxideWafer");
            //if (this.OxideWafer)
            //{
            //    this.mzImbLeftIdeal_mA = 3.5;
            //   this.mzImbRightIdeal_mA = 3.5;
            //    //this.mzImbLeftMinLimit_mA = Convert.ToDouble(testSpec.GetParamLimit("CH_MZ_CTRL_L_I").LowLimit.ValueToString());
            //    this.mzImbLeftMaxLimit_mA = 6;
            //    //this.mzImbRightMinLimit_mA = Convert.ToDouble(testSpec.GetParamLimit("CH_MZ_CTRL_R_I").LowLimit.ValueToString());
            //   this.mzImbRightMaxLimit_mA = 6;
            //   this.vcmMinLimit = -4.75;
            //    this.vcmMaxLimit = -1;
            //    this.vpiMinLimit = 2;
            //    this.targetVpi_V = 2.6;
            //    this.mzVleftMinQuadLimit = -4.85;
            //    this.mzVleftMaxQuadLimit = -1;
            //    this.vcm_V[0] = -3.5;
            //    this.vcm_V[1] = -1.5;
            //    this.MzCtrlINominal_A = 0.003;
            //    this.vcm_LastChannel_V = -2.5;
            // }

            #endregion

            // Initialise GUI
            engine.GuiShow();
            engine.GuiToFront();

            #region setup for sweep
            // setup our private data 
            this.mzDriverUtils = new IlMzDriverUtils(mzInstrs);
            mzDriverUtils.opticalSwitch = OpticalSwitch;
            mzDriverUtils.locker_port = int.Parse(locker_tran_pot.ToString());

            // Setup instruments for sweep
            TcmzMzSweep_Config mzConfig = new TcmzMzSweep_Config(configData);

            //if (!engine.IsSimulation)
            // mzDriverUtils.CleanupAfterSweep();

            //double mzMaxBias_V = mzConfig.MzMaxBias_V;
            double mzMaxBias_V = Convert.ToDouble(testSpec.GetParamLimit("TC_MZ_MINIMA_V_LIMIT_MIN").LowLimit.ValueToString());//get the limit from limit file. Jack.zhang 2012.04.10
            IlMzDriverUtils.MaxModBias_V = mzMaxBias_V;
            double fixedModBias_V = mzConfig.MZInitFixedModBias_volt;
            double imbalance_A = mzConfig.MzCtrlINominal_mA / 1000;
            double sweepMaxAbs_V = mzConfig.MZInitSweepMaxAbs_V;
            sweepMaxAbs_V = Math.Abs(sweepMaxAbs_V);
            double sweepStop_V = 0.0;
            double sweepStart_V = -sweepMaxAbs_V;
            double stepSize_V = mzConfig.MZInitSweepStepSize_mV / 1000;
            int nbrPoints = (int)(1 + Math.Abs(sweepStart_V - sweepStop_V) / stepSize_V);
            double tapBias_V = mzConfig.MZTapBias_V;

            double currentCompliance_A = mzConfig.MZCurrentCompliance_A;
            double currentRange_A = mzConfig.MZCurrentRange_A;
            double voltageCompliance_V = mzConfig.MZVoltageCompliance_V;
            double voltageRange_V = mzConfig.MZVoltageRange_V;
            int numberOfAverages = mzConfig.MzInitNumberOfAverages;
            double integrationRate = mzConfig.MZIntegrationRate;
            double sourceMeasureDelay = mzConfig.MZSourceMeasureDelay_s;
            double powerMeterAveragingTime = mzConfig.MZPowerMeterAveragingTime_s;

            engine.SendToGui("Preparing instruments for sweep");
            if (!engine.IsSimulation)
            {
                if (ArmSourceByAsic)
                {   // ASIC
                    mzInstrs.PowerMeter.SetDefaultState();
                }
                else
                {   // NON-ASIC           
                    mzDriverUtils.InitialiseMZArms(currentCompliance_A, currentRange_A);
                    mzDriverUtils.InitialiseImbalanceArms(voltageCompliance_V, voltageRange_V);
                    mzDriverUtils.InitialiseTaps(currentCompliance_A, currentRange_A);
                    mzDriverUtils.SetMeasurementAccuracy(numberOfAverages, integrationRate, sourceMeasureDelay, powerMeterAveragingTime);
                }

                mzInstrs.PowerMeter.Mode = Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.MeterMode.Absolute_mW;
                //Alice.Huang    2010-08-31
                // set power meter range to its initial range before any sweep 
                //mzInstrs.PowerMeter.Range = configData.ReadDouble("OpmRangeInit_mW");
            }
            #endregion

            // measure at first, middle and last channels
            int midChannelIndex = 0;

            for (int i = 0; i < ituChannels.Length; i++)
            {
                DsdbrChannelData channel = ituChannels[i];
                if (Math.Abs(channel.ItuFreq_GHz - midChannelFrequency) < double.Epsilon)
                {
                    midChannelIndex = i;
                    break;
                }
            }

            int[] chansToMeas = new int[] { 0, midChannelIndex, ituChannels.Length - 1 };
            List<MzSweepDataItuChannel> mzSweepData = new List<MzSweepDataItuChannel>();
            initialOffsetSelected = false;

            double tuneISOAPowerSlope = configData.ReadDouble("TuneISOAPowerSlope");
            double tuneOpticalPowerTolerance_mW
                = configData.ReadDouble("TuneOpticalPowerToleranceInmW");
            double TargetFibrePower_dBm = configData.ReadDouble("TargetFibrePower_dBm");

            bool isError = false;
            string errorInformation = "";
            indexOf3Channels = 0;

            foreach (int channelIndex in chansToMeas)
            {
                MzSweepDataItuChannel mzDataItu = measureMzAtFrequency(
                    engine, mzInstrs, ituChannels[channelIndex],
                    tuneISOAPowerSlope, tuneOpticalPowerTolerance_mW, TargetFibrePower_dBm,
                    configData, out isError, out errorInformation);

                mzSweepData.Add(mzDataItu);

                if (isError)
                    break;

                indexOf3Channels++;//Echo new adde to check whether this channel is last channel

                targetVpi_V = targetVpi_V - 0.1;//raul changed according to tscr522
            }


            if (!engine.IsSimulation)
            {
                // Clean up
                mzInstrs.PowerMeter.EnableLogging = false;

                // Alice.Huang     2010-02-26
                //commented all methjod related with   IInstType_TriggeredOpticalPowerMeter   with  InstType_OpticalPowerMeter 
                mzInstrs.PowerMeter.EnableInputTrigger(false);
            }

            string mzSweepDataResultsFile;
            string mzSweepDataZipFile;
            bool allSweepsPassed;
            processMzSweepData(mzSweepData, out mzSweepDataResultsFile, out mzSweepDataZipFile, out allSweepsPassed);


            // return the data
            DatumList returnData = new DatumList();

            returnData.AddFileLink("MzSweepDataZipFile", mzSweepDataZipFile);
            returnData.AddFileLink("MzSweepDataResultsFile", mzSweepDataResultsFile);
            returnData.AddSint32("AllSweepsPassed", allSweepsPassed ? 1 : 0);
            returnData.AddReference("MzSweepData", mzSweepData);
            returnData.AddBool("IsError", isError);
            returnData.AddDoubleArray("OptimizedVcmArray", this.OptimizedVcm);
            if (isError)
            {
                returnData.AddString("ErrorInformation", errorInformation);
            }
            return returnData;
        }

        /// <summary>
        /// Characterise MZ @ the specified ITU frequency
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="mzInstrs"></param>
        /// <param name="dsdbrSetup"></param>
        /// <param name="ISOAPowerSlope"></param>
        /// <param name="opticalPowerTolerance_mW"></param>
        /// <param name="TargetFibrePower_dBm"></param>
        /// <param name="configData"></param>
        /// <param name="isError"></param>
        /// <param name="errorInformation"></param>
        /// <returns></returns>
        private MzSweepDataItuChannel measureMzAtFrequency(ITestEngine engine,
            IlMzInstruments mzInstrs, DsdbrChannelData dsdbrSetup,
            double ISOAPowerSlope, double opticalPowerTolerance_mW,
            double TargetFibrePower_dBm, DatumList configData,
            out bool isError, out string errorInformation)
        {
            #region Initialise test parameters
            
            isError = false;
            errorInformation = "";

            // Set Vpi and Vcm limits in analysis wrapper 
            MzAnalysisWrapper.vpiMinLimit = this.vpiMinLimit;
            MzAnalysisWrapper.vpiMaxLimit = this.targetVpi_V;
            MzAnalysisWrapper.vcmMinLimit = this.vcmMinLimit;
            MzAnalysisWrapper.vcmMaxLimit = this.vcmMaxLimit;

            // setup the DSDBR
            if (!engine.IsSimulation)
                DsdbrUtils.SetDsdbrCurrents_mA(dsdbrSetup.Setup);

            int chanNbr = dsdbrSetup.ItuChannelIndex;

            // Cache the WL in mzUtils so that we can calibrate power after the sweep.
            int numberOfAverages = configData.ReadSint32("MZInitNumberOfAverages");
            double integrationRate = configData.ReadDouble("MZIntegrationRate");
            double sourceMeasureDelay = configData.ReadDouble("MZSourceMeasureDelay_s");
            double powerMeterAveragingTime = configData.ReadDouble("MZPowerMeterAveragingTime_s");

            double icm_mA = 0;

            if (ParamManager.Conditions.IsThermalPhase)
            {
                icm_mA = configData.ReadDouble("MzCtrlINominal_thermal_mA");
            }
            else
            {
                icm_mA = configData.ReadDouble("MzCtrlINominal_mA");
            }

            IMB imb = new IMB(icm_mA);

            MoveIMBTo moveIMBTo = new MoveIMBTo();

            MzAnalysisWrapper.MzAnalysisResults lvResult = null;

            MzSweepDataItuChannel mzItuChanData = new MzSweepDataItuChannel();
            mzItuChanData.ItuChannelIndex = chanNbr;
            mzItuChanData.ItuFrequency_GHz = dsdbrSetup.ItuFreq_GHz;
            #endregion

            #region Setup power meter
            
            if (!engine.IsSimulation)
            {
                if (ArmSourceByAsic)
                {
                    // ASIC
                    Measurements.FrequencyWithoutMeter = dsdbrSetup.ItuFreq_GHz;
                    mzInstrs.PowerMeter.Wavelength_nm = Alg_FreqWlConvert.Freq_GHz_TO_Wave_nm(dsdbrSetup.ItuFreq_GHz);
                }
                else
                {
                    // NON-ASIC
                    double wavelength = Measurements.ReadWavelength_nm();
                    mzInstrs.PowerMeter.Wavelength_nm = wavelength;
                }
            } 
            #endregion

            #region Imbalance sweep and analysis - chongjian.liang 2013.1.13

            #region LI sweep

            double leftBias_LISweep_V = 0;
            double rightBias_LISweep_V = 0;

            if (indexOf3Channels == 0)
            {
                leftBias_LISweep_V = this.LeftBias_V_LI_Sweep_FirstChan;
                rightBias_LISweep_V = this.RightBias_V_LI_Sweep_FirstChan;
            }
            else if (indexOf3Channels == 1)
            {
                leftBias_LISweep_V = this.LeftBias_V_LI_Sweep_MiddleChan;
                rightBias_LISweep_V = this.RightBias_V_LI_Sweep_MiddleChan;
            }
            else
            {
                leftBias_LISweep_V = this.LeftBias_V_LI_Sweep_LastChan;
                rightBias_LISweep_V = this.RightBias_V_LI_Sweep_LastChan;
            }

            // Perform the sweep
            engine.SendToGui("Running Differential Imbalance sweep on channel " + chanNbr.ToString());

            ILMZSweepResult liSweepResult = new ILMZSweepResult();

            if (!engine.IsSimulation)
            {
                liSweepResult = mzDriverUtils.ImbArm_DifferentialSweep(leftBias_LISweep_V, rightBias_LISweep_V, 0.0, 2 * imb.ICM_A, NumberPoints * 3 / 4, MZSourceMeasureDelay_ms, tapInline_V, MZTapBias_V, ArmSourceByAsic);
            }
            #endregion

            #region Save the sweep result to file

            string liSweepFileNamePrefix = string.Format("MZCR_Ch{0}_DiffLI", chanNbr);

            string liSweepFileName;

            SaveSweepDataToFile(liSweepResult, liSweepFileNamePrefix, out liSweepFileName);

            allSweepFilesNames.Add(liSweepFileName); 
            #endregion

            #region Display the LI sweep result

            engine.SendToGui(liSweepResult); 
            #endregion

            #region Analyse the LI sweep result

            MzAnalysisWrapper.MzAnalysisResults mzAnlyDiffI = MzAnalysisWrapper.DiffImbalance_NegChirp_FindFeaturePointByRef(liSweepResult, imb.ICM_A, 0, this.featureToCenter);

            if (ParamManager.Conditions.IsThermalPhase)
            {
                mzAnlyDiffI = MzAnalysisWrapper.ZeroChirp_DifferentialSweep(liSweepResult, imb.ICM_A, 0); //for thermal Imb the index increase with the Imb current different with normal imb_mA in Pi.   jack.zhang 2012-02-11
            } 
            #endregion

            #region Get values from the LI sweep analysis result

            for (int i = 0; i < liSweepResult.SweepData[ILMZSweepDataType.FibrePower_mW].Length; i++)
            {
                double tempPower_mW = liSweepResult.SweepData[ILMZSweepDataType.FibrePower_mW][i];

                if (tempPower_mW > imb.MaxPower_mW)
                {
                    imb.MaxPower_mW = tempPower_mW;
                }
            }

            // Check analysis results in case we did not sweep the full range
            double peakToQuad_v = Math.Abs(mzAnlyDiffI.MzAnalysis.VoltageAtMax - mzAnlyDiffI.MzAnalysis.VImb);
            double quadToNull_v = Math.Abs(mzAnlyDiffI.MzAnalysis.VImb - mzAnlyDiffI.MzAnalysis.VoltageAtMin);
            iPi_mA = Math.Max(mzAnlyDiffI.MzAnalysis.Vpi, 2 * peakToQuad_v);
            iPi_mA = Math.Max(iPi_mA, 2 * quadToNull_v);
            iPi_mA *= 1000;    // convert the units to mA

            //if(ParamManager.Conditions.IsThermalPhase)
            //    iPi_mA *= -1;//for thermal Imb the index increase with the Imb current different with normal imb_mA.   jack.zhang 2012-02-11

            Alg_MZLVMinMax.DataPoint[] peakArray_LI = null;
            Alg_MZLVMinMax.DataPoint[] valleyArray_LI = null;

            // Get all peak and valley points voltage array
            MzAnalysisWrapper.GetPeakAndValleyPointArray(liSweepResult, out peakArray_LI, out valleyArray_LI);

            if (indexOf3Channels != 0)
            {
                // Close the Imb Current to last channel: chan57 close to chan0, chan99 close to chan57
                CloseImbToLastChan(imb, peakArray_LI, valleyArray_LI, mzAnlyDiffI);
            }

            switch (this.featureToCenter)
            {
                case Alg_MZAnalysis.MzFeaturePowerType.MaxMinus3dB:
                    imb.Quad_L_mA = mzAnlyDiffI.Quad_SrcL * 1000;
                    imb.Quad_R_mA = mzAnlyDiffI.Quad_SrcR * 1000;
                    break;
                case Alg_MZAnalysis.MzFeaturePowerType.Quad:
                    imb.Quad_L_mA = mzAnlyDiffI.Imb_SrcL * 1000;
                    imb.Quad_R_mA = mzAnlyDiffI.Imb_SrcR * 1000;
                    break;
                case Alg_MZAnalysis.MzFeaturePowerType.Max:
                    imb.Current_L_mA = mzAnlyDiffI.Max_SrcL * 1000;
                    imb.Current_R_mA = mzAnlyDiffI.Max_SrcR * 1000;
                    break;
                case Alg_MZAnalysis.MzFeaturePowerType.Min:
                    imb.Current_L_mA = mzAnlyDiffI.Min_SrcL * 1000;
                    imb.Current_R_mA = mzAnlyDiffI.Min_SrcR * 1000;
                    break;
                default:
                    this.failModeCheck.RaiseError_Mod_NonParamFail(engine, "Software misconfiguration. Don't know how to extract data for " + this.featureToCenter.ToString(), FailModeCategory_Enum.SW);
                    break;
            }

            if (imb.Quad_L_mA < imb.Min_L_mA || imb.Quad_L_mA > imb.Max_L_mA)
            {
                if (imb.Quad_L_mA < imb.Min_L_mA)
                {
                    imb.Quad_L_mA = imb.Min_L_mA;
                    imb.Quad_R_mA = imb.Max_R_mA;
                }
                else
                {
                    imb.Quad_L_mA = imb.Max_L_mA;
                    imb.Quad_R_mA = imb.Min_R_mA;
                }
            }

            if (indexOf3Channels == 0)
            {
                // Record the IMB of the first channel, to be closed by the middle channel. - chongjian.liang
                IMB.Final_1stChannel_mA = imb.Quad_L_mA - imb.Quad_R_mA;
            }
            else if (indexOf3Channels == 1)
            {
                // Record the IMB of the middle channel, to be closed by the last channel. - chongjian.liang
                IMB.Final_MiddleChannel_mA = imb.Quad_L_mA - imb.Quad_R_mA;
            }

            imb.Peak_L_mA = mzAnlyDiffI.Max_SrcL * 1000;//Echo store this result to write into result file,2011-03-26
            imb.Peak_R_mA = mzAnlyDiffI.Max_SrcR * 1000; 
            #endregion

            #region Display the analysis result

            // Add a nice arrow to point at the quadrature point
            NPlot.PointD marker = new NPlot.PointD(mzAnlyDiffI.Quad_SrcL - mzAnlyDiffI.Quad_SrcR, mzAnlyDiffI.PowerAtMax_dBm - 3);
            engine.SendToGui(marker);

            NPlot.TextItem markerText = new NPlot.TextItem(new NPlot.PointD(marker.X, marker.Y), "Quadrature");
            engine.SendToGui(markerText);

            NPlot.VerticalLine verticalLine = new NPlot.VerticalLine(mzAnlyDiffI.Min_SrcL - mzAnlyDiffI.Min_SrcR, System.Drawing.Color.IndianRed);
            engine.SendToGui(verticalLine);

            if (System.Diagnostics.Debugger.IsAttached)
            {
                System.Threading.Thread.Sleep(5000);
            }
            #endregion

            #endregion

            #region Power leveling at peak of IMB
            // Jack.Zhang  2010-05-14
            // do power leveling here to esure the mz is characterise at the right fibre power
            // that can get the right Vpi, Vcm, & HalfPILeft bias
            engine.SendToGui(" Running power leveling on channel " + chanNbr.ToString());
            double powerOffset_dB = configData.ReadDouble("PowerlevelingOffset_dB");
            double tuneISOAPowerSlope = configData.ReadDouble("TuneISOAPowerSlope");
            double tuneOpticalPowerTolerance_mW = configData.ReadDouble("TuneOpticalPowerToleranceInmW");
            double targetPowerDbm = configData.ReadDouble("TargetFibrePower_dBm");
            double powerOffset = Math.Abs(configData.ReadDouble("PowerlevelingOffset_dB"));
            double MaxISoaForPowerLeveling = configData.ReadDouble("MaxSoaForPwrLeveling");

            // Set imbalance arms to get max power output

            if (!engine.IsSimulation)
            {
                mzDriverUtils.SetCurrent(SourceMeter.LeftImbArm, mzAnlyDiffI.Max_SrcL);
                mzDriverUtils.SetCurrent(SourceMeter.RightImbArm, mzAnlyDiffI.Max_SrcR);

                SoaPowerLevel.PowerLevelWithAdaptiveSlope(Alg_PowConvert_dB.Convert_dBmtomW(targetPowerDbm + powerOffset), powerOffset, tuneISOAPowerSlope, MaxISoaForPowerLeveling);

                // Set imbalance arms back to their operating condition
                mzDriverUtils.SetCurrent(SourceMeter.LeftImbArm, imb.Quad_L_mA / 1000);
                mzDriverUtils.SetCurrent(SourceMeter.RightImbArm, imb.Quad_R_mA / 1000);
            }
            #endregion

            #region Set the power meter
            
            if (!engine.IsSimulation)
            {
                if (!ArmSourceByAsic)
                {
                    mzDriverUtils.SetMeasurementAccuracy(numberOfAverages, integrationRate, sourceMeasureDelay, powerMeterAveragingTime);
                }

                mzInstrs.PowerMeter.Mode = Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.MeterMode.Absolute_mW;
            } 
            #endregion
            
            #region Optimise VCM - chongjian.liang 2013.1.13

            #region Get Vcm0 & Vcm1 for the current channel

            #region Get Vcm0 & Vcm1 from config file

            if (this.featureToCenter == Alg_MZAnalysis.MzFeaturePowerType.Quad)
            {
                // Sweep left arm with null at 0
                Alg_MZAnalysis.MZAnalysis seResponse = measureLeftArmSingleEndedResponse(engine, mzAnlyDiffI.Min_SrcL * 1000, mzAnlyDiffI.Min_SrcR * 1000);

                if (indexOf3Channels == 0)
                {
                    vcm_V[0] = this.vcm_FirstChannel_V;
                    vcm_V[1] = vcm_V[0] + 0.75; // Change Vcm1(first chan) to -4 from -3 according to product engineer's requirement. - chongjian.liang 2012.12.6
                    margin = vmax_firstChan;
                }
                else if (indexOf3Channels == 1)
                {
                    vcm_V[0] = this.vcm_MiddleChannel_V;
                    vcm_V[1] = vcm_V[0] + 1;
                    margin = vmax_middleChan;
                }
                else
                {
                    vcm_V[0] = this.vcm_LastChannel_V;
                    vcm_V[1] = vcm_V[0] + 1;
                    margin = vmax_lastChan;
                }
                MzAnalysisWrapper.margin = margin;
            }
            #region else(Not reachable at present)
            else
            {
                //// Just use left arm to locate minumim + maximum, calculate Vpi from this

                //Alg_MZAnalysis.MZAnalysis seResponse = measureSingleEndedResponse(engine, chanNbr, ideal_L_mA, ideal_R_mA);

                //// Calculate suitable common mode voltages
                ////Echoxl.wang  2011=-3-07 , if first channel then vcm=-2+(vpi/2)+abs(voffset/2);
                ////                          if last channel then vcm=-1+(vpi/2)+abs(voffset/2);
                ////                          old method use -1 without check first or last channel.
                //double VcmFactor = -1;
                //if (indexOf3Channels == 0)
                //    VcmFactor = VcmFactorFirstChannel;
                //else if (indexOf3Channels == 1)
                //    VcmFactor = VcmFactorMiddleChannel;
                //else
                //    VcmFactor = VcmFactorLastChannel;

                //vcm_V[0] = VcmFactor - seResponse.Vpi / 2 - Math.Abs((seResponse.VoltageAtMin - this.mzInitFixedModBias_volt) / 2);//jack.zhang
                //vcm_V[1] = vcm_V[0] - 1;
            }
            #endregion

            #endregion

            #region Crop Vcm0 & Vcm1 by Vcm limits

            vcm_V[0] = Math.Max(vcmMinLimit, vcm_V[0]);
            vcm_V[1] = Math.Max(vcmMinLimit, vcm_V[1]);
            vcm_V[0] = Math.Min(vcmMaxLimit, vcm_V[0]);
            vcm_V[1] = Math.Min(vcmMaxLimit, vcm_V[1]);

            if (vcm_V[0] == vcm_V[1])
            {
                if (vcm_V[0] == vcmMinLimit)
                {
                    vcm_V[0] = vcm_V[0] + 1;
                }
                else
                {
                    vcm_V[1] = vcm_V[1] - 1;
                }
            }
            #endregion 

            #endregion

            #region LV sweep and analysis under VCM0 & VCM1 condition

            for (int vcmIndex = 0; vcmIndex < vcm_V.Length; vcmIndex++)
            {
                MeasureMzAtVcmLevel(engine, mzItuChanData, chanNbr, vcmIndex, imb);
            }
            #endregion
            
            #region Get a line(Vcm, Vpi) by Vcm0 & Vcm1 & their Vpi values

            double vpi_AtVcm0_V = mzItuChanData.VcmData[0].GetValueDouble(MzItuAtVcmMeasurements.Vpi_V);
            double vcm0_V = mzItuChanData.VcmData[0].Vcm_V;

            double vpi_AtVcm1_V = mzItuChanData.VcmData[1].GetValueDouble(MzItuAtVcmMeasurements.Vpi_V);
            double vcm1_V = mzItuChanData.VcmData[1].Vcm_V;

            // We must have 2 seperated points get a line. - chongjian.liang
            if (vcm0_V == vcm1_V || vpi_AtVcm0_V == vpi_AtVcm1_V)
            {
                string errorMsg = string.Format("Failed to optimize Channel {0} Vcm Vpi, \n Stop testing!", chanNbr);
                isError = true;
                errorInformation = errorMsg;
            }

            double slope_LineOfVcm0Vcm1ToVpi = (vpi_AtVcm0_V - vpi_AtVcm1_V) / (vcm0_V - vcm1_V);

            // Offset required is Y at X=0, so swap X & Y and invert gradient
            double intercept_Vpi_LineOfVcm0Vcm1ToVpi_V = EstimateXAtY.Calculate(vpi_AtVcm0_V, vcm0_V, 1 / slope_LineOfVcm0Vcm1ToVpi, 0);
            #endregion

            #region Get a line(Vcm, DiffVQuad) by Vcm0 & Vcm1 & their DiffVQuad values

            double diffVQuad_AtVcm0_V = mzItuChanData.VcmData[0].GetValueDouble(MzItuAtVcmMeasurements.VquadL_V) - mzItuChanData.VcmData[0].GetValueDouble(MzItuAtVcmMeasurements.VquadR_V);
            double diffVQuad_AtVcm1_V = mzItuChanData.VcmData[1].GetValueDouble(MzItuAtVcmMeasurements.VquadL_V) - mzItuChanData.VcmData[1].GetValueDouble(MzItuAtVcmMeasurements.VquadR_V);

            double slope_LineOfVcm0Vcm1ToDiffVQuad = (diffVQuad_AtVcm0_V - diffVQuad_AtVcm1_V) / (vcm0_V - vcm1_V);

            // Offset required is Y at X=0, so swap X & Y and invert gradient
            double intercept_DiffVQuad_LineOfVcm0Vcm1ToDiffVQuad_V = EstimateXAtY.Calculate(diffVQuad_AtVcm0_V, vcm0_V, 1 / slope_LineOfVcm0Vcm1ToDiffVQuad, 0);
            #endregion 

            double vcm_Optimised_V = 0;
            double slope_LineOfVcmToVpi_final = 0;
            double intercept_Vpi_LineOfVcmToVpi_final_V = 0;

            if (targetVpi_V >= vpi_AtVcm0_V - vpiOffset_V && targetVpi_V <= vpi_AtVcm1_V + vpiOffset_V)
            {
                #region Get Vcm2 by targetVpi from line(Vcm0&1, Vpi)

                // Let vcm2_V <= the value to prevent LV arm bias > -0.5V, which prevents the Peak or Trough in LV sweep into the lazy region
                double vcm2_V = (-0.5 - intercept_Vpi_LineOfVcm0Vcm1ToVpi_V / 4 - intercept_DiffVQuad_LineOfVcm0Vcm1ToDiffVQuad_V / 2) /
                    (1 + slope_LineOfVcm0Vcm1ToVpi / 4 + slope_LineOfVcm0Vcm1ToDiffVQuad / 2);

                // Limit the vcm with its min & max limits and the above value
                vcm2_V = Math.Min(vcmMaxLimit, Math.Max(vcmMinLimit, Math.Min(vcm2_V, (targetVpi_V - intercept_Vpi_LineOfVcm0Vcm1ToVpi_V) / slope_LineOfVcm0Vcm1ToVpi)));

                #endregion

                #region LV sweep and analysis under Vcm2 condition

                engine.SendToGui("Sweeping MZ under Vcm2 condition");

                LVSweepAndAnalysis(imb, 2 * vcm2_V, 0.0, NumberPoints, vcm2_V,
                    string.Format("MZCR_Ch{0}_Vcm2_M_LV", chanNbr),
                    mzItuChanData, out lvResult);

                if (LimitJudger.IsERTooLarge(lvResult, this.Target_DCER + this.Margin_ER))
                {
                    moveIMBTo.HalfPILeft = true;
                }
                else if (LimitJudger.IsERTooSmall(lvResult, this.dcerMinLimit + this.Margin_ER))
                {
                    moveIMBTo.HalfPIRight = true;
                }
                else if (LimitJudger.IsAllPass(mzItuChanData, lvResult, this.Target_DCER + this.Margin_ER, this.dcerMinLimit + this.Margin_ER))
                {
                    return mzItuChanData;
                }

                #endregion

                // Some device can exceed vpi max limit under Vcm2
                if (lvResult.Vpi > vpiMaxLimit && vcm0_V != vcm2_V)
                {
                    #region Get a line(Vcm, Vpi) by Vcm0 & Vcm2 & their Vpi values

                    double vpi_AtVcm2_V = lvResult.Vpi;

                    double slope_LineOfVcm0Vcm2ToVpi = (vpi_AtVcm0_V - vpi_AtVcm2_V) / (vcm0_V - vcm2_V);

                    // Offset required is Y at X=0, so swap X & Y and invert gradient
                    double intercept_Vpi_LineOfVcm0Vcm2ToVpi_V = EstimateXAtY.Calculate(vpi_AtVcm0_V, vcm0_V, 1 / slope_LineOfVcm0Vcm2ToVpi, 0);

                    #endregion

                    #region Get a line(Vcm, DiffVQuad) by Vcm0 & Vcm2 & their DiffVQuad values

                    double diffVQuad_AtVcm2_V = lvResult.Quad_SrcL - lvResult.Quad_SrcR;

                    double slope_LineOfVcm0Vcm2ToDiffVQuad = (diffVQuad_AtVcm0_V - diffVQuad_AtVcm2_V) / (vcm0_V - vcm2_V);

                    // Offset required is Y at X=0, so swap X & Y and invert gradient
                    double intercept_DiffVQuad_LineOfVcm0Vcm2ToDiffVQuad_V = EstimateXAtY.Calculate(diffVQuad_AtVcm0_V, vcm0_V, 1 / slope_LineOfVcm0Vcm2ToDiffVQuad, 0);
                    #endregion

                    #region Get Vcm3 by targetVpi from line(Vcm0&2, Vpi)

                    double vcm3_V = (-0.5 - intercept_Vpi_LineOfVcm0Vcm2ToVpi_V / 4 - intercept_DiffVQuad_LineOfVcm0Vcm2ToDiffVQuad_V / 2) /
                        (1 + slope_LineOfVcm0Vcm2ToVpi / 4 + slope_LineOfVcm0Vcm2ToDiffVQuad / 2);

                    vcm3_V = Math.Min(vcmMaxLimit, Math.Max(vcmMinLimit, Math.Min(vcm3_V, (targetVpi_V - intercept_Vpi_LineOfVcm0Vcm2ToVpi_V) / slope_LineOfVcm0Vcm2ToVpi)));
                    
                    #endregion

                    #region LV sweep and analysis under Vcm3 condition

                    engine.SendToGui("Sweeping MZ under Vcm3 condition");

                    LVSweepAndAnalysis(imb, 2 * vcm3_V, 0.0, NumberPoints, vcm3_V,
                        string.Format("MZCR_Ch{0}_Vcm3_M_LV", chanNbr),
                        mzItuChanData, out lvResult);

                    #endregion

                    #region Record the final result

                    vcm_Optimised_V = vcm3_V;
                    slope_LineOfVcmToVpi_final = slope_LineOfVcm0Vcm2ToVpi;
                    intercept_Vpi_LineOfVcmToVpi_final_V = intercept_Vpi_LineOfVcm0Vcm2ToVpi_V;
                    #endregion
                }
                else
                {
                    #region Record the final result

                    vcm_Optimised_V = vcm2_V;
                    slope_LineOfVcmToVpi_final = slope_LineOfVcm0Vcm1ToVpi;
                    intercept_Vpi_LineOfVcmToVpi_final_V = intercept_Vpi_LineOfVcm0Vcm1ToVpi_V;
                    #endregion
                }
            }
            else if (targetVpi_V >= vpi_AtVcm1_V + vpiOffset_V)
            {
                #region Get Vcm2 by targetVpi from line(Vcm0&1, Vpi)

                // Let vcm2_V <= the value to prevent LV arm bias > -0.5V, which prevents the Peak or Trough in LV sweep into the lazy region
                double vcm2_V = (-0.5 - intercept_Vpi_LineOfVcm0Vcm1ToVpi_V / 4 - intercept_DiffVQuad_LineOfVcm0Vcm1ToDiffVQuad_V / 2) /
                            (1 + slope_LineOfVcm0Vcm1ToVpi / 4 + slope_LineOfVcm0Vcm1ToDiffVQuad / 2);

                // Limit the vcm with its max limit and the above value
                vcm2_V = Math.Min(vcmMaxLimit, (Math.Min(vcm2_V, targetVpi_V - intercept_Vpi_LineOfVcm0Vcm1ToVpi_V) / slope_LineOfVcm0Vcm1ToVpi));

                #endregion

                #region LV sweep and analysis under Vcm2 condition

                engine.SendToGui("Sweeping MZ under Vcm2 condition");

                LVSweepAndAnalysis(imb, 2 * vcm2_V, 0.0, NumberPoints, vcm2_V,
                    string.Format("MZCR_Ch{0}_Vcm2_R_LV", chanNbr),
                    mzItuChanData, out lvResult);

                #endregion

                if (vcm1_V != vcm2_V)
                {
                    #region Get a line(Vcm, Vpi) by Vcm1 & Vcm2 & their Vpi values

                    double vpi_AtVcm2_V = lvResult.Vpi;

                    double slope_LineOfVcm1Vcm2ToVpi = (vpi_AtVcm1_V - vpi_AtVcm2_V) / (vcm1_V - vcm2_V);

                    // Offset required is Y at X=0, so swap X & Y and invert gradient
                    double intercept_Vpi_LineOfVcm1Vcm2ToVpi_V = EstimateXAtY.Calculate(vpi_AtVcm1_V, vcm1_V, 1 / slope_LineOfVcm1Vcm2ToVpi, 0);

                    #endregion

                    #region Get a line(Vcm, DiffVQuad) by Vcm1 & Vcm2 & their DiffVQuad values

                    double diffVQuad_AtVcm2_V = lvResult.Quad_SrcL - lvResult.Quad_SrcR;

                    double slope_LineOfVcm1Vcm2ToDiffVQuad = (diffVQuad_AtVcm1_V - diffVQuad_AtVcm2_V) / (vcm1_V - vcm2_V);

                    // Offset required is Y at X=0, so swap X & Y and invert gradient
                    double intercept_DiffVQuad_LineOfVcm1Vcm2ToDiffVQuad_V = EstimateXAtY.Calculate(diffVQuad_AtVcm1_V, vcm1_V, 1 / slope_LineOfVcm1Vcm2ToDiffVQuad, 0);
                    #endregion

                    #region Get Vcm3 by targetVpi from line(Vcm1&2, Vpi), and limit Vcm3 by preventing LV Bias fall into lazy region

                    double vcm3_V = (-0.5 - intercept_Vpi_LineOfVcm1Vcm2ToVpi_V / 4 - intercept_DiffVQuad_LineOfVcm1Vcm2ToDiffVQuad_V / 2) /
                        (1 + slope_LineOfVcm1Vcm2ToVpi / 4 + slope_LineOfVcm1Vcm2ToDiffVQuad / 2);

                    vcm3_V = Math.Min(vcmMaxLimit, Math.Min(vcm3_V, (targetVpi_V - intercept_Vpi_LineOfVcm1Vcm2ToVpi_V) / slope_LineOfVcm1Vcm2ToVpi));

                    #endregion

                    #region LV sweep and analysis under Vcm3 condition

                    engine.SendToGui("Sweeping MZ under Vcm3 condition");

                    LVSweepAndAnalysis(imb, 2 * vcm3_V, 0.0, NumberPoints, vcm3_V,
                        string.Format("MZCR_Ch{0}_Vcm3_R_LV", chanNbr),
                        mzItuChanData, out lvResult);

                    #endregion

                    #region Record the final result

                    vcm_Optimised_V = vcm3_V;
                    slope_LineOfVcmToVpi_final = slope_LineOfVcm1Vcm2ToVpi;
                    intercept_Vpi_LineOfVcmToVpi_final_V = intercept_Vpi_LineOfVcm1Vcm2ToVpi_V;
                    #endregion
                }
                else
                {
                    #region Record the final result

                    vcm_Optimised_V = vcm2_V;
                    slope_LineOfVcmToVpi_final = slope_LineOfVcm0Vcm1ToVpi;
                    intercept_Vpi_LineOfVcmToVpi_final_V = intercept_Vpi_LineOfVcm0Vcm1ToVpi_V;
                    #endregion
                }
            }
            else if (targetVpi_V <= vpi_AtVcm0_V - vpiOffset_V)
            {
                #region Get Vcm2 by targetVpi from line(Vcm0&1, Vpi)

                // Let vcm2_V <= the value to prevent LV arm bias > -0.5V, which prevents the Peak or Trough in LV sweep into the lazy region
                double vcm2_V = (-0.5 - intercept_Vpi_LineOfVcm0Vcm1ToVpi_V / 4 - intercept_DiffVQuad_LineOfVcm0Vcm1ToDiffVQuad_V / 2) /
                            (1 + slope_LineOfVcm0Vcm1ToVpi / 4 + slope_LineOfVcm0Vcm1ToDiffVQuad / 2);

                // Limit the vcm with its min limit and the above value
                vcm2_V = Math.Max(this.vcmMinLimit, Math.Min(vcm2_V, (targetVpi_V - intercept_Vpi_LineOfVcm0Vcm1ToVpi_V) / slope_LineOfVcm0Vcm1ToVpi));
                #endregion

                #region LV sweep and analysis under Vcm2 condition

                engine.SendToGui("Sweeping MZ under Vcm2 condition");

                LVSweepAndAnalysis(imb, 2 * vcm2_V, 0.0, NumberPoints, vcm2_V,
                    string.Format("MZCR_Ch{0}_Vcm2_L_LV", chanNbr),
                    mzItuChanData, out lvResult);

                #endregion

                if (vcm0_V != vcm2_V)
                {
                    #region Get a line(Vcm, Vpi) by Vcm2 & Vcm0 & their Vpi values

                    double vpi_AtVcm2_V = lvResult.Vpi;

                    double slope_LineOfVcm2Vcm0ToVpi = (vpi_AtVcm0_V - vpi_AtVcm2_V) / (vcm0_V - vcm2_V);

                    // Offset required is Y at X=0, so swap X & Y and invert gradient
                    double intercept_Vpi_LineOfVcm2Vcm0ToVpi_V = EstimateXAtY.Calculate(vpi_AtVcm0_V, vcm0_V, 1 / slope_LineOfVcm2Vcm0ToVpi, 0);
                    #endregion

                    #region Get Vcm3 by targetVpi from line(Vcm0&1, Vpi)

                    double vcm3_V = Math.Max(this.vcmMinLimit, (targetVpi_V - intercept_Vpi_LineOfVcm2Vcm0ToVpi_V) / slope_LineOfVcm2Vcm0ToVpi);

                    #endregion

                    #region LV sweep and analysis under Vcm3 condition

                    engine.SendToGui("Sweeping MZ under Vcm3 condition");

                    LVSweepAndAnalysis(imb, 2 * vcm3_V, 0.0, NumberPoints, vcm3_V,
                        string.Format("MZCR_Ch{0}_Vcm3_L_LV", chanNbr),
                        mzItuChanData, out lvResult);

                    #endregion

                    #region Record the final result

                    vcm_Optimised_V = vcm3_V;
                    slope_LineOfVcmToVpi_final = slope_LineOfVcm2Vcm0ToVpi;
                    intercept_Vpi_LineOfVcmToVpi_final_V = intercept_Vpi_LineOfVcm2Vcm0ToVpi_V;
                    #endregion 
                }
                else
                {
                    #region Record the final result

                    vcm_Optimised_V = vcm2_V;
                    slope_LineOfVcmToVpi_final = slope_LineOfVcm0Vcm1ToVpi;
                    intercept_Vpi_LineOfVcmToVpi_final_V = intercept_Vpi_LineOfVcm0Vcm1ToVpi_V;
                    #endregion 
                }
            }

            if (LimitJudger.IsERTooLarge(lvResult, this.Target_DCER + this.Margin_ER)
                && !this.IsMZBiasesMustSimilar)
            {
                moveIMBTo.HalfPILeft = true;
            }
            else if (LimitJudger.IsERTooSmall(lvResult, this.dcerMinLimit + this.Margin_ER)
                && !this.IsMZBiasesMustSimilar)
            {
                moveIMBTo.HalfPIRight = true;
            }
            else if (LimitJudger.IsAllPass(mzItuChanData, lvResult, this.Target_DCER + this.Margin_ER, this.dcerMinLimit + this.Margin_ER))
            {
                return mzItuChanData;
            }
            // For some specific device types that are requested that the both MZ biases must similar to each other - chongjian.liang 2016.06.31
            else if (LimitJudger.IsAllPass_WithoutER(mzItuChanData)
                && this.IsMZBiasesMustSimilar)
            {
                return mzItuChanData;
            }
            #endregion

            #region Optimise DCER - chongjian.liang 2012.12.21

            bool hasMoved2PI = false;

            #region Try to move IMB 1/2PI to the right from LI Quad

            if (moveIMBTo.HalfPIRight)
            {
                double imb_QuadHalfPIRight_L_mA = 0;
                double imb_QuadHalfPIRight_R_mA = 0;

                MoveIMBHalfPIToRight(ref imb, mzItuChanData, iPi_mA, out imb_QuadHalfPIRight_L_mA, out imb_QuadHalfPIRight_R_mA);

                // If IMB changes distinctly
                if (Math.Abs(imb_QuadHalfPIRight_L_mA - imb.QuadHalfPIRight_L_mA) > IMB.MinToMove_mA)
                {
                    imb.QuadHalfPIRight_L_mA = imb_QuadHalfPIRight_L_mA;
                    imb.QuadHalfPIRight_R_mA = imb_QuadHalfPIRight_R_mA;

                    mzDriverUtils.SetCurrent(SourceMeter.LeftImbArm, imb.QuadHalfPIRight_L_mA / 1000);
                    mzDriverUtils.SetCurrent(SourceMeter.RightImbArm, imb.QuadHalfPIRight_R_mA / 1000);

                    engine.SendToGui("Sweeping MZ after trying to move IMB 1/2PI to the right.");

                    double er_AtQuadIMB_dB = lvResult.ExtinctionRatio_dB;

                    LVSweepAndAnalysis(imb, 2 * vcm_Optimised_V, 0.0, NumberPoints, vcm_Optimised_V,
                        string.Format("MZCR_Ch{0}_AfterIMBMovedHalfPIToRight_LV", chanNbr),
                        mzItuChanData, out lvResult);

                    imb.Slope_ERToLeftIMB = (imb.QuadHalfPIRight_L_mA - imb.Quad_L_mA) / (lvResult.ExtinctionRatio_dB - er_AtQuadIMB_dB);
                    
                    if (LimitJudger.IsERTooLarge(lvResult, this.Target_DCER + this.Margin_ER))
                    {
                        moveIMBTo.AttenuateToTargetER = true;
                    }
                    /* (For mid & last channel)To prevent IMB set at positive slope, 
                     * don't allow to enlarge ER when cannot move 2PI right.*/
                    else if (indexOf3Channels == 0
                        && ParamManager.Conditions.IsThermalPhase == false
                        && LimitJudger.IsERTooSmall(lvResult, this.dcerMinLimit + this.Margin_ER))
                    {
                        moveIMBTo.TwoPIRight = true;
                    }

                    if (LimitJudger.IsAllPass(mzItuChanData, lvResult, this.Target_DCER + this.Margin_ER, this.dcerMinLimit + this.Margin_ER))
                    {
                        return mzItuChanData;
                    }
                }
            }
            #endregion

            #region Try to move IMB 1/2PI to the left from LI Quad

            else if (moveIMBTo.HalfPILeft)
            {
                double imb_QuadHalfPILeft_L_mA = 0;
                double imb_QuadHalfPILeft_R_mA = 0;

                MoveIMBHalfPIToLeft(ref imb, mzItuChanData, iPi_mA, out imb_QuadHalfPILeft_L_mA, out imb_QuadHalfPILeft_R_mA);

                // If IMB changes distinctly
                if (Math.Abs(imb_QuadHalfPILeft_L_mA - imb.QuadHalfPILeft_L_mA) > IMB.MinToMove_mA)
                {
                    imb.QuadHalfPILeft_L_mA = imb_QuadHalfPILeft_L_mA;
                    imb.QuadHalfPILeft_R_mA = imb_QuadHalfPILeft_R_mA;

                    mzDriverUtils.SetCurrent(SourceMeter.LeftImbArm, imb.QuadHalfPILeft_L_mA / 1000);
                    mzDriverUtils.SetCurrent(SourceMeter.RightImbArm, imb.QuadHalfPILeft_R_mA / 1000);

                    engine.SendToGui("Sweeping MZ after trying to move IMB 1/2PI to the left.");

                    double er_AtQuadIMB_dB = lvResult.ExtinctionRatio_dB;

                    LVSweepAndAnalysis(imb, 2 * vcm_Optimised_V, 0.0, NumberPoints, vcm_Optimised_V,
                        string.Format("MZCR_Ch{0}_AfterIMBMovedHalfPIToLeft_LV", chanNbr),
                        mzItuChanData, out lvResult);

                    imb.Slope_ERToLeftIMB = (imb.QuadHalfPILeft_L_mA - imb.Quad_L_mA) / (lvResult.ExtinctionRatio_dB - er_AtQuadIMB_dB);

                    if (LimitJudger.IsERTooSmall(lvResult, this.dcerMinLimit + this.Margin_ER))
                    {
                        moveIMBTo.EnlargeToTargetER = true;
                    }
                    /* (For mid & last channel)To prevent IMB set at positive slope,
                     * don't allow to attenuate ER when cannot move 2PI left.*/
                    else if (indexOf3Channels == 0
                        && ParamManager.Conditions.IsThermalPhase == false
                        && LimitJudger.IsERTooLarge(lvResult, this.Target_DCER + this.Margin_ER))
                    {
                        moveIMBTo.TwoPILeft = true;
                    }

                    if (LimitJudger.IsAllPass(mzItuChanData, lvResult, this.Target_DCER + this.Margin_ER, this.dcerMinLimit + this.Margin_ER))
                    {
                        return mzItuChanData;
                    }
                }
            }
            #endregion

            #region Try to move IMB 2PI to the right from LI Quad

            if (moveIMBTo.TwoPIRight)
            {
                double imb_Quad2PIRight_L_mA = imb.Quad_L_mA + iPi_mA;
                double imb_Quad2PIRight_R_mA = imb.Quad_R_mA - iPi_mA;
                double imb_Quad2PIRight_Diff_mA = imb_Quad2PIRight_L_mA - imb_Quad2PIRight_R_mA;

                if (imb_Quad2PIRight_Diff_mA <= imb.Max_Diff_mA)
                {
                    #region Move IMB 2PI to the right

                    imb.Quad2PIRight_L_mA = imb_Quad2PIRight_L_mA;
                    imb.Quad2PIRight_R_mA = imb_Quad2PIRight_R_mA;
                    
                    mzDriverUtils.SetCurrent(SourceMeter.LeftImbArm, imb.Quad2PIRight_L_mA / 1000);
                    mzDriverUtils.SetCurrent(SourceMeter.RightImbArm, imb.Quad2PIRight_R_mA / 1000);

                    engine.SendToGui("Sweeping MZ after moving IMB 2PI to right from Quad point.");

                    LVSweepAndAnalysis(imb, 2 * vcm_Optimised_V, 0.0, NumberPoints, vcm_Optimised_V,
                        string.Format("MZCR_Ch{0}_AfterIMBMoved2PIToRight_LV", chanNbr),
                        mzItuChanData, out lvResult);

                    if (LimitJudger.IsERTooLarge(lvResult, this.Target_DCER + this.Margin_ER))
                    {
                        moveIMBTo.AttenuateToTargetER = true;
                    }
                    else if (LimitJudger.IsERTooSmall(lvResult, this.dcerMinLimit + this.Margin_ER))
                    {
                        moveIMBTo.EnlargeToTargetER = true;
                    }

                    hasMoved2PI = true;

                    if (LimitJudger.IsAllPass_WithoutER(mzItuChanData))
                    {
                        IMB.Final_1stChannel_mA = imb.Quad2PIRight_Diff_mA;

                        if (LimitJudger.IsAllPass(mzItuChanData, lvResult, this.Target_DCER + this.Margin_ER, this.dcerMinLimit + this.Margin_ER))
                        {
                            return mzItuChanData;
                        }
                    }

                    #endregion
                }
                else // If IMB out of Max_Diff limit
                {
                    /// STEP 1: If imb_mA.Max_Diff is at negative slope,
                    /// use the imb_mA.Max_Diff as the new IMB - chongjian.liang 2012.12.10

                    #region Move IMB to Max_Diff

                    double leftTroughClosestToMax = 0;

                    // Find left the valley point closest but not equal the imb_mA.Max_Diff
                    foreach (Alg_MZLVMinMax.DataPoint valley in valleyArray_LI)
                    {
                        if (valley.Voltage < imb.Max_Diff_mA / 1000)
                        {
                            leftTroughClosestToMax = valley.Voltage;
                        }
                        else
                        {
                            break;
                        }
                    }

                    double leftPeakClosestToMax = 0;

                    // Find left the peak point closest or equal the imb_mA.Max_Diff
                    foreach (Alg_MZLVMinMax.DataPoint peak in peakArray_LI)
                    {
                        if (peak.Voltage <= imb.Max_Diff_mA / 1000)
                        {
                            leftPeakClosestToMax = peak.Voltage;
                        }
                        else
                        {
                            break;
                        }
                    }

                    // If below condition is true, imb_mA.Max_Diff is at negative slope
                    if (leftPeakClosestToMax > leftTroughClosestToMax)
                    {
                        imb.Quad2PIRight_L_mA = imb.Max_L_mA;
                        imb.Quad2PIRight_R_mA = imb.Min_R_mA;

                        mzDriverUtils.SetCurrent(SourceMeter.LeftImbArm, imb.Quad2PIRight_L_mA / 1000);
                        mzDriverUtils.SetCurrent(SourceMeter.RightImbArm, imb.Quad2PIRight_R_mA / 1000);

                        engine.SendToGui("Sweeping MZ under imbalance max differential value.");

                        LVSweepAndAnalysis(imb, 2 * vcm_Optimised_V, 0.0, NumberPoints, vcm_Optimised_V,
                            string.Format("MZCR_Ch{0}_AfterIMBMovedToDiffMax_LV", chanNbr),
                            mzItuChanData, out lvResult);

                        if (LimitJudger.IsERTooLarge(lvResult, this.Target_DCER + this.Margin_ER))
                        {
                            moveIMBTo.AttenuateToTargetER = true;
                        }

                        hasMoved2PI = true;

                        if (LimitJudger.IsAllPass_WithoutER(mzItuChanData))
                        {
                            IMB.Final_1stChannel_mA = imb.Quad2PIRight_Diff_mA;

                            if (LimitJudger.IsAllPass(mzItuChanData, lvResult, this.Target_DCER + this.Margin_ER, this.dcerMinLimit + this.Margin_ER))
                            {
                                return mzItuChanData;
                            }
                        }
                    }
                    #endregion

                    /// STEP 2: If imb_mA.Max_Diff is at positive slope, 
                    /// try to increase Vcm to the value that obtained by target Vpi.

                    #region LV sweep and analysis under the Vcm at target Vpi
                    else
                    {
                        double vcm_AtTargetVpi_V = Math.Min(vcmMaxLimit, Math.Max(vcmMinLimit, (targetVpi_V - intercept_Vpi_LineOfVcmToVpi_final_V) / slope_LineOfVcmToVpi_final));

                        if (vcm_AtTargetVpi_V != vcm_Optimised_V)
                        {
                            vcm_Optimised_V = vcm_AtTargetVpi_V;

                            mzDriverUtils.SetCurrent(SourceMeter.LeftImbArm, imb.QuadHalfPIRight_L_mA / 1000);
                            mzDriverUtils.SetCurrent(SourceMeter.RightImbArm, imb.QuadHalfPIRight_R_mA / 1000);

                            engine.SendToGui("Sweeping MZ under Vcm at target Vpi.");

                            imb.Current_L_mA = imb.QuadHalfPIRight_L_mA;
                            imb.Current_R_mA = imb.QuadHalfPIRight_R_mA;

                            LVSweepAndAnalysis(imb, 2 * vcm_Optimised_V, 0.0, NumberPoints, vcm_Optimised_V,
                                string.Format("MZCR_Ch{0}_VcmAtTargetVpi_LV", chanNbr),
                                mzItuChanData, out lvResult);
                        }
                    }
                    #endregion
                }
            }
            #endregion

            #region Try to move IMB 2PI to the left from LI Quad

            else if (moveIMBTo.TwoPILeft)
            {
                double imb_Quad2PILeft_L_mA = imb.Quad_L_mA - iPi_mA;
                double imb_Quad2PILeft_R_mA = imb.Quad_R_mA + iPi_mA;
                double imb_Quad2PILeft_Diff_mA = imb_Quad2PILeft_L_mA - imb_Quad2PILeft_R_mA;

                if (imb_Quad2PILeft_Diff_mA >= imb.Min_Diff_mA)
                {
                    #region Move IMB 2PI to the left

                    imb.Quad2PILeft_L_mA = imb_Quad2PILeft_L_mA;
                    imb.Quad2PILeft_R_mA = imb_Quad2PILeft_R_mA;

                    mzDriverUtils.SetCurrent(SourceMeter.LeftImbArm, imb.Quad2PILeft_L_mA / 1000);
                    mzDriverUtils.SetCurrent(SourceMeter.RightImbArm, imb.Quad2PILeft_R_mA / 1000);

                    engine.SendToGui("Sweeping MZ after moving IMB 2PI to left from Quad point.");

                    LVSweepAndAnalysis(imb, 2 * vcm_Optimised_V, 0.0, NumberPoints, vcm_Optimised_V,
                        string.Format("MZCR_Ch{0}_AfterIMBMoved2PIToLeft_LV", chanNbr),
                        mzItuChanData, out lvResult);

                    if (LimitJudger.IsERTooLarge(lvResult, this.Target_DCER + this.Margin_ER))
                    {
                        moveIMBTo.AttenuateToTargetER = true;
                    }
                    else if (LimitJudger.IsERTooSmall(lvResult, this.dcerMinLimit + this.Margin_ER))
                    {
                        moveIMBTo.EnlargeToTargetER = true;
                    }

                    hasMoved2PI = true;

                    if (LimitJudger.IsAllPass_WithoutER(mzItuChanData))
                    {
                        IMB.Final_1stChannel_mA = imb.Quad2PILeft_Diff_mA;

                        if (LimitJudger.IsAllPass(mzItuChanData, lvResult, this.Target_DCER + this.Margin_ER, this.dcerMinLimit + this.Margin_ER))
                        {
                            return mzItuChanData;
                        }
                    }

                    #endregion
                }
                else // If IMB out of Min_Diff limit
                {
                    // If imb_mA.Min_Diff is at negative slope, use imb_mA.Min_Diff as the new IMB

                    #region Move IMB to Min_Diff

                    double rightTroughClosestToMin = 0;

                    // Find left the valley point closest or equal the imb_mA.Min_Diff
                    foreach (Alg_MZLVMinMax.DataPoint valley in valleyArray_LI)
                    {
                        if (valley.Voltage >= imb.Min_Diff_mA / 1000)
                        {
                            rightTroughClosestToMin = valley.Voltage;

                            break;
                        }
                    }

                    double rightPeakClosestToMin = 0;

                    // Find left the peak point closest but not equal the imb_mA.Min_Diff
                    foreach (Alg_MZLVMinMax.DataPoint peak in peakArray_LI)
                    {
                        if (peak.Voltage > imb.Min_Diff_mA / 1000)
                        {
                            rightPeakClosestToMin = peak.Voltage;

                            break;
                        }
                    }

                    // If below condition is true, imb_mA.Min_Diff is at negative slope
                    if (rightPeakClosestToMin > rightTroughClosestToMin)
                    {
                        imb.Quad2PILeft_L_mA = imb.Min_L_mA;
                        imb.Quad2PILeft_R_mA = imb.Max_R_mA;

                        mzDriverUtils.SetCurrent(SourceMeter.LeftImbArm, imb.Quad2PILeft_L_mA / 1000);
                        mzDriverUtils.SetCurrent(SourceMeter.RightImbArm, imb.Quad2PILeft_R_mA / 1000);

                        engine.SendToGui("Sweeping MZ under imbalance min differential value.");

                        LVSweepAndAnalysis(imb, 2 * vcm_Optimised_V, 0.0, NumberPoints, vcm_Optimised_V,
                            string.Format("MZCR_Ch{0}_AfterIMBMovedToDiffMin_LV", chanNbr),
                            mzItuChanData, out lvResult);

                        if (LimitJudger.IsERTooSmall(lvResult, this.dcerMinLimit + this.Margin_ER))
                        {
                            moveIMBTo.EnlargeToTargetER = true;
                        }

                        hasMoved2PI = true;

                        if (LimitJudger.IsAllPass_WithoutER(mzItuChanData))
                        {
                            IMB.Final_1stChannel_mA = imb.Quad2PILeft_Diff_mA;

                            if (LimitJudger.IsAllPass(mzItuChanData, lvResult, this.Target_DCER + this.Margin_ER, this.dcerMinLimit + this.Margin_ER))
                            {
                                return mzItuChanData;
                            }
                        }
                    }
                    #endregion
                }
            }
            #endregion

            #region Try to move IMB right to meet target ER

            if (moveIMBTo.EnlargeToTargetER)
            {
                double imb_TargetER_L_mA = 0;
                double imb_TargetER_R_mA = 0;

                MoveIMBRightForTargetER(ref imb, mzItuChanData, lvResult, iPi_mA, out imb_TargetER_L_mA, out imb_TargetER_R_mA);

                if (Math.Abs(imb_TargetER_L_mA - imb.TargetER_L_mA) > IMB.MinToMove_mA)
                {
                    imb.TargetER_L_mA = imb_TargetER_L_mA;
                    imb.TargetER_R_mA = imb_TargetER_R_mA;

                    mzDriverUtils.SetCurrent(SourceMeter.LeftImbArm, imb.TargetER_L_mA / 1000);
                    mzDriverUtils.SetCurrent(SourceMeter.RightImbArm, imb.TargetER_R_mA / 1000);

                    engine.SendToGui("Sweeping MZ after moving IMB right to meet TargetER.");

                    LVSweepAndAnalysis(imb, 2 * vcm_Optimised_V, 0.0, NumberPoints, vcm_Optimised_V,
                        string.Format("MZCR_Ch{0}_AfterIMBMovedToRightForTargetER_LV", chanNbr),
                        mzItuChanData, out lvResult);

                    if (hasMoved2PI && LimitJudger.IsAllPass_WithoutER(mzItuChanData))
                    {
                        IMB.Final_1stChannel_mA = imb.TargetER_Diff_mA;
                    }

                    if (LimitJudger.IsAllPass(mzItuChanData, lvResult, this.Target_DCER + this.Margin_ER, this.dcerMinLimit + this.Margin_ER))
                    {
                        return mzItuChanData;
                    }
                }
            } 
            #endregion

            #region Try to move IMB left to meet target ER

            else if (moveIMBTo.AttenuateToTargetER)
            {
                double imb_TargetER_L_mA = 0;
                double imb_TargetER_R_mA = 0;

                MoveIMBLeftForTargetER(ref imb, mzItuChanData, lvResult, iPi_mA, out imb_TargetER_L_mA, out imb_TargetER_R_mA);

                if (Math.Abs(imb_TargetER_L_mA - imb.TargetER_L_mA) > IMB.MinToMove_mA)
                {
                    imb.TargetER_L_mA = imb_TargetER_L_mA;
                    imb.TargetER_R_mA = imb_TargetER_R_mA;

                    mzDriverUtils.SetCurrent(SourceMeter.LeftImbArm, imb.TargetER_L_mA / 1000);
                    mzDriverUtils.SetCurrent(SourceMeter.RightImbArm, imb.TargetER_R_mA / 1000);

                    engine.SendToGui("Sweeping MZ after moving IMB left to meet TargetER.");

                    LVSweepAndAnalysis(imb, 2 * vcm_Optimised_V, 0.0, NumberPoints, vcm_Optimised_V,
                        string.Format("MZCR_Ch{0}_AfterIMBMovedToLeftForTargetER_LV", chanNbr),
                        mzItuChanData, out lvResult);

                    if (hasMoved2PI && LimitJudger.IsAllPass_WithoutER(mzItuChanData))
                    {
                        IMB.Final_1stChannel_mA = imb.TargetER_Diff_mA;
                    }

                    if (LimitJudger.IsAllPass(mzItuChanData, lvResult, this.Target_DCER + this.Margin_ER, this.dcerMinLimit + this.Margin_ER))
                    {
                        return mzItuChanData;
                    }
                }
            }
            #endregion
            
            #region Try to move IMB right to to meet Quad_LV bias limits

            if (LimitJudger.IsQuadOutOfRight(lvResult, this.mzVleftMinQuadLimit))
            {
                double deltaIMB_L_mA = -(this.mzVleftMinQuadLimit - lvResult.Quad_SrcR + 0.1) / lvResult.Vpi * iPi_mA;

                double imb_LVQuadLimit_L_mA = 0;
                double imb_LVQuadLimit_R_mA = 0;
                double imb_LVQuadLimit_Diff_mA = 0;

                if (ParamManager.Conditions.IsThermalPhase == false)
                {
                    imb_LVQuadLimit_L_mA = imb.Current_L_mA + Math.Abs(deltaIMB_L_mA);
                }
                else
                {
                    imb_LVQuadLimit_L_mA = imb.Current_L_mA - Math.Abs(deltaIMB_L_mA);
                }

                imb_LVQuadLimit_R_mA = 2 * imb.ICM_mA - imb_LVQuadLimit_L_mA;
                imb_LVQuadLimit_Diff_mA = imb_LVQuadLimit_L_mA - imb_LVQuadLimit_R_mA;

                if (imb_LVQuadLimit_Diff_mA >= imb.Min_Diff_mA && imb_LVQuadLimit_Diff_mA <= imb.Max_Diff_mA)
                {
                    imb.LVQuadLimit_L_mA = imb_LVQuadLimit_L_mA;
                    imb.LVQuadLimit_R_mA = imb_LVQuadLimit_R_mA;

                    mzDriverUtils.SetCurrent(SourceMeter.LeftImbArm, imb.LVQuadLimit_L_mA / 1000);
                    mzDriverUtils.SetCurrent(SourceMeter.RightImbArm, imb.LVQuadLimit_R_mA / 1000);

                    engine.SendToGui("Sweeping MZ after moving IMB right to meet Quad_LV bias.");

                    LVSweepAndAnalysis(imb, 2 * vcm_Optimised_V, 0.0, NumberPoints, vcm_Optimised_V,
                        string.Format("MZCR_Ch{0}_AfterIMBMovedToRightForQuadBias_LV", chanNbr),
                        mzItuChanData, out lvResult);
                }

                if (hasMoved2PI && LimitJudger.IsAllPass_WithoutER(mzItuChanData))
                {
                    IMB.Final_1stChannel_mA = imb.LVQuadLimit_Diff_mA;
                }
            }
            #endregion 

            #region Try to move IMB left to to meet Quad_LV bias limits

            else if (LimitJudger.IsQuadOutOfLeft(lvResult, this.mzVleftMinQuadLimit))
            {
                double deltaIMB_L_mA = (this.mzVleftMinQuadLimit - lvResult.Quad_SrcL + 0.1) / lvResult.Vpi * iPi_mA;

                double imb_LVQuadLimit_L_mA = 0;
                double imb_LVQuadLimit_R_mA = 0;
                double imb_LVQuadLimit_Diff_mA = 0;

                if (ParamManager.Conditions.IsThermalPhase == false)
                {
                    imb_LVQuadLimit_L_mA = imb.Current_L_mA - Math.Abs(deltaIMB_L_mA);
                }
                else
                {
                    imb_LVQuadLimit_L_mA = imb.Current_L_mA + Math.Abs(deltaIMB_L_mA);
                }

                imb_LVQuadLimit_R_mA = 2 * imb.ICM_mA - imb_LVQuadLimit_L_mA;
                imb_LVQuadLimit_Diff_mA = imb_LVQuadLimit_L_mA - imb_LVQuadLimit_R_mA;

                if (imb_LVQuadLimit_Diff_mA >= imb.Min_Diff_mA && imb_LVQuadLimit_Diff_mA <= imb.Max_Diff_mA)
                {
                    imb.LVQuadLimit_L_mA = imb_LVQuadLimit_L_mA;
                    imb.LVQuadLimit_R_mA = imb_LVQuadLimit_R_mA;

                    mzDriverUtils.SetCurrent(SourceMeter.LeftImbArm, imb.LVQuadLimit_L_mA / 1000);
                    mzDriverUtils.SetCurrent(SourceMeter.RightImbArm, imb.LVQuadLimit_R_mA / 1000);

                    engine.SendToGui("Sweeping MZ after moving IMB left to meet Quad_LV bias.");

                    LVSweepAndAnalysis(imb, 2 * vcm_Optimised_V, 0.0, NumberPoints, vcm_Optimised_V,
                        string.Format("MZCR_Ch{0}_AfterIMBMovedToLeftForQuadBias_LV", chanNbr),
                        mzItuChanData, out lvResult);
                }

                if (hasMoved2PI && LimitJudger.IsAllPass_WithoutER(mzItuChanData))
                {
                    IMB.Final_1stChannel_mA = imb.LVQuadLimit_Diff_mA;
                }
            }
            #endregion

            return mzItuChanData;

            #endregion
        }

        /// <summary>
        /// Try to move IMB PI/2 to right at LI sweep curve
        /// </summary>
        private void MoveIMBHalfPIToRight(ref IMB imb, MzSweepDataItuChannel mzItuChanData, double imbalanceIpi_mA, out double imb_QuadHalfPIRight_L_mA, out double imb_QuadHalfPIRight_R_mA)
        {
            int indexOfLastVcmData = mzItuChanData.VcmData.Count - 1;

            double delta_QuarterPI = Math.Abs(mzItuChanData.VcmData[indexOfLastVcmData].GetValueDouble(MzItuAtVcmMeasurements.Vpi_V) / 4);
            double delta_QuadToMin_V = Math.Abs(mzItuChanData.VcmData[indexOfLastVcmData].GetValueDouble(MzItuAtVcmMeasurements.VquadL_V) - mzVleftMinQuadLimit - 0.1);
            double delta_PeakToMax_V = Math.Abs(mzItuChanData.VcmData[indexOfLastVcmData].GetValueDouble(MzItuAtVcmMeasurements.VpeakL_V) - IlMzDriverUtils.MaxModBias_V - 0.1);
            double delta_PeakToMin_V = Math.Abs(mzItuChanData.VcmData[indexOfLastVcmData].GetValueDouble(MzItuAtVcmMeasurements.VpeakR_V) - (-0.5));

            double delta_V = Math.Min(Math.Min(Math.Min(delta_QuarterPI, delta_QuadToMin_V), delta_PeakToMax_V), delta_PeakToMin_V);

            double delta_IMB_mA = imbalanceIpi_mA * delta_V / mzItuChanData.VcmData[indexOfLastVcmData].GetValueDouble(MzItuAtVcmMeasurements.Vpi_V);

            if (ParamManager.Conditions.IsThermalPhase == false) // Move IMB to right
            {
                imb_QuadHalfPIRight_L_mA = imb.Quad_L_mA + delta_IMB_mA;
                imb_QuadHalfPIRight_L_mA = Math.Min(imb_QuadHalfPIRight_L_mA, imb.Max_L_mA);
                imb_QuadHalfPIRight_R_mA = 2 * imb.ICM_mA - imb_QuadHalfPIRight_L_mA;

            }
            else // Move IMB to left
            {
                imb_QuadHalfPIRight_L_mA = imb.Quad_L_mA - delta_IMB_mA;
                imb_QuadHalfPIRight_L_mA = Math.Max(imb_QuadHalfPIRight_L_mA, imb.Min_L_mA);
                imb_QuadHalfPIRight_R_mA = 2 * imb.ICM_mA - imb_QuadHalfPIRight_L_mA;
            }
        }

        /// <summary>
        /// Try to move IMB PI/2 to left at LI sweep curve
        /// </summary>
        private void MoveIMBHalfPIToLeft(ref IMB imb, MzSweepDataItuChannel mzItuChanData, double imbalanceIpi_mA, out double imb_QuadHalfPILeft_L_mA, out double imb_QuadHalfPILeft_R_mA)
        {
            int indexOfLastVcmData = mzItuChanData.VcmData.Count - 1;

            double delta_QuarterPI = Math.Abs(mzItuChanData.VcmData[indexOfLastVcmData].GetValueDouble(MzItuAtVcmMeasurements.Vpi_V) / 4);
            double delta_QuadToMin_V = Math.Abs(mzItuChanData.VcmData[indexOfLastVcmData].GetValueDouble(MzItuAtVcmMeasurements.VquadR_V) - mzVleftMinQuadLimit - 0.1);
            double delta_PeakToMax_V = Math.Abs(mzItuChanData.VcmData[indexOfLastVcmData].GetValueDouble(MzItuAtVcmMeasurements.VminR_V) - IlMzDriverUtils.MaxModBias_V - 0.1);
            double delta_PeakToMin_V = Math.Abs(mzItuChanData.VcmData[indexOfLastVcmData].GetValueDouble(MzItuAtVcmMeasurements.VminL_V) - (-0.5));

            double delta_V = Math.Min(Math.Min(Math.Min(delta_QuarterPI, delta_QuadToMin_V), delta_PeakToMax_V), delta_PeakToMin_V);

            double delta_IMB_mA = imbalanceIpi_mA * delta_V / mzItuChanData.VcmData[indexOfLastVcmData].GetValueDouble(MzItuAtVcmMeasurements.Vpi_V);

            if (ParamManager.Conditions.IsThermalPhase == false) // Move IMB to left
            {
                imb_QuadHalfPILeft_L_mA = imb.Quad_L_mA - delta_IMB_mA;
                imb_QuadHalfPILeft_L_mA = Math.Max(imb_QuadHalfPILeft_L_mA, imb.Min_L_mA);
                imb_QuadHalfPILeft_R_mA = 2 * imb.ICM_mA - imb_QuadHalfPILeft_L_mA;
            }
            else // Move IMB to right
            {
                imb_QuadHalfPILeft_L_mA = imb.Quad_L_mA + delta_IMB_mA;
                imb_QuadHalfPILeft_L_mA = Math.Min(imb_QuadHalfPILeft_L_mA, imb.Max_L_mA);
                imb_QuadHalfPILeft_R_mA = 2 * imb.ICM_mA - imb_QuadHalfPILeft_L_mA;
            }
        }
        
        /// <summary>
        /// Try to move IMB PI/2 to right at LI sweep curve
        /// </summary>
        private void MoveIMBRightForTargetER(ref IMB imb, MzSweepDataItuChannel mzItuChanData, MzAnalysisWrapper.MzAnalysisResults lvResult, double imbalanceIpi_mA, out double imb_TargetER_L_mA, out double imb_TargetER_R_mA)
        {
            int indexOfLastVcmData = mzItuChanData.VcmData.Count - 1;

            double delta_QuadToMin_V = Math.Abs(mzItuChanData.VcmData[indexOfLastVcmData].GetValueDouble(MzItuAtVcmMeasurements.VquadL_V) - mzVleftMinQuadLimit - 0.1);
            double delta_PeakToMax_V = Math.Abs(mzItuChanData.VcmData[indexOfLastVcmData].GetValueDouble(MzItuAtVcmMeasurements.VpeakL_V) - IlMzDriverUtils.MaxModBias_V - 0.1);
            double delta_PeakToMin_V = Math.Abs(mzItuChanData.VcmData[indexOfLastVcmData].GetValueDouble(MzItuAtVcmMeasurements.VpeakR_V) - (-0.5));

            double delta_V = Math.Min(Math.Min(delta_QuadToMin_V, delta_PeakToMax_V), delta_PeakToMin_V);
            double delta_IMB_mA = imbalanceIpi_mA * delta_V / mzItuChanData.VcmData[indexOfLastVcmData].GetValueDouble(MzItuAtVcmMeasurements.Vpi_V);

            double imb_BiasLimited_L_mA;
            double imb_ERLimited_L_mA;

            if (ParamManager.Conditions.IsThermalPhase == false) // Move IMB to right
            {
                imb_BiasLimited_L_mA = imb.Current_L_mA + delta_IMB_mA;
                imb_ERLimited_L_mA = imb.Current_L_mA + Math.Abs(imb.Slope_ERToLeftIMB * (this.Target_DCER - lvResult.ExtinctionRatio_dB));
                imb_TargetER_L_mA = Math.Min(Math.Min(imb_BiasLimited_L_mA, imb_ERLimited_L_mA), imb.Max_L_mA);
                imb_TargetER_R_mA = 2 * imb.ICM_mA - imb_TargetER_L_mA;
            }
            else // Move IMB to left
            {
                imb_BiasLimited_L_mA = imb.Current_L_mA - delta_IMB_mA;
                imb_ERLimited_L_mA = imb.Current_L_mA - Math.Abs(imb.Slope_ERToLeftIMB * (this.Target_DCER - lvResult.ExtinctionRatio_dB));
                imb_TargetER_L_mA = Math.Max(Math.Max(imb_BiasLimited_L_mA, imb_ERLimited_L_mA), imb.Min_L_mA);
                imb_TargetER_R_mA = 2 * imb.ICM_mA - imb_TargetER_L_mA;
            }
        }

        /// <summary>
        /// Try to move IMB PI/2 to left at LI sweep curve
        /// </summary>
        private void MoveIMBLeftForTargetER(ref IMB imb, MzSweepDataItuChannel mzItuChanData, MzAnalysisWrapper.MzAnalysisResults lvResult, double imbalanceIpi_mA, out double imb_TargetER_L_mA, out double imb_TargetER_R_mA)
        {
            int indexOfLastVcmData = mzItuChanData.VcmData.Count - 1;

            double delta_QuadToMin_V = Math.Abs(mzItuChanData.VcmData[indexOfLastVcmData].GetValueDouble(MzItuAtVcmMeasurements.VquadR_V) - mzVleftMinQuadLimit - 0.1);
            double delta_PeakToMax_V = Math.Abs(mzItuChanData.VcmData[indexOfLastVcmData].GetValueDouble(MzItuAtVcmMeasurements.VminR_V) - IlMzDriverUtils.MaxModBias_V - 0.1);
            double delta_PeakToMin_V = Math.Abs(mzItuChanData.VcmData[indexOfLastVcmData].GetValueDouble(MzItuAtVcmMeasurements.VminL_V) - (-0.5));

            double delta_V = Math.Min(Math.Min(delta_QuadToMin_V, delta_PeakToMax_V), delta_PeakToMin_V);

            double delta_IMB_mA = imbalanceIpi_mA * delta_V / mzItuChanData.VcmData[indexOfLastVcmData].GetValueDouble(MzItuAtVcmMeasurements.Vpi_V);

            double imb_BiasLimited_L_mA;
            double imb_ERLimited_L_mA;

            if (ParamManager.Conditions.IsThermalPhase == false) // Move IMB to left
            {
                imb_BiasLimited_L_mA = imb.Current_L_mA - delta_IMB_mA;
                imb_ERLimited_L_mA = imb.Current_L_mA - Math.Abs(imb.Slope_ERToLeftIMB * (this.Target_DCER - lvResult.ExtinctionRatio_dB));
                imb_TargetER_L_mA = Math.Max(Math.Max(imb_BiasLimited_L_mA, imb_ERLimited_L_mA), imb.Min_L_mA);
                imb_TargetER_R_mA = 2 * imb.ICM_mA - imb_TargetER_L_mA;
            }
            else // Move IMB to right
            {
                imb_BiasLimited_L_mA = imb.Current_L_mA + delta_IMB_mA;
                imb_ERLimited_L_mA = imb.Current_L_mA + Math.Abs(imb.Slope_ERToLeftIMB * (this.Target_DCER - lvResult.ExtinctionRatio_dB));
                imb_TargetER_L_mA = Math.Min(Math.Min(imb_BiasLimited_L_mA, imb_ERLimited_L_mA), imb.Max_L_mA);
                imb_TargetER_R_mA = 2 * imb.ICM_mA - imb_TargetER_L_mA;
            }
        }

        private void LVSweepAndAnalysis(IMB imb, double start_V, double stop_V, int numberOfPoints, double vcm_V, string sweepFileNamePrefix,
             MzSweepDataItuChannel mzItuChanData, out MzAnalysisWrapper.MzAnalysisResults analysisResult_LV)
        {
            // Sweep
            ILMZSweepResult sweepResult_LV = mzDriverUtils.ModArm_DifferentialSweepByTrigger(imb.Current_L_mA / 1000, imb.Current_R_mA / 1000, start_V, stop_V, numberOfPoints, MZSourceMeasureDelay_ms, tapInline_V, MZTapBias_V, ArmSourceByAsic);
            this.engine.SendToGui(sweepResult_LV);

            // Analysis
            analysisResult_LV = MzAnalysisWrapper.Differential_NegChirp_DifferentialSweep(sweepResult_LV, vcm_V, 0);

            if (System.Diagnostics.Debugger.IsAttached)
            {
                System.Threading.Thread.Sleep(5000);
            }

            string sweepFileName = "";

            // Save sweep data
            SaveSweepDataToFile(sweepResult_LV, sweepFileNamePrefix, out sweepFileName);

            allSweepFilesNames.Add(sweepFileName);

            // Save analysis data
            MzItuAtVcmData savedAnalysisData = SaveAnalysisData(imb, vcm_V, analysisResult_LV);

            mzItuChanData.VcmData.Add(savedAnalysisData);
        }

        private MzItuAtVcmData SaveAnalysisData(IMB imb, double vcm_V, MzAnalysisWrapper.MzAnalysisResults sweepResults)
        {
            double iSOA_mA = DsdbrUtils.ReadSectionCurrent_mA(DSDBRSection.SOA);
            MzItuAtVcmData vcmData = new MzItuAtVcmData(vcm_V, iSOA_mA);
            // store imbalance biases
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.IImbQuadL_mA, imb.Current_L_mA);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.IImbQuadR_mA, imb.Current_R_mA);
            // store sweep result
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.VquadL_V, sweepResults.Imb_SrcL);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.VquadR_V, sweepResults.Imb_SrcR);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.VminL_V, sweepResults.Min_SrcL);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.VminR_V, sweepResults.Min_SrcR);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.VpeakL_V, sweepResults.Max_SrcL);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.VpeakR_V, sweepResults.Max_SrcR);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.Vpi_V, sweepResults.Vpi);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.ER_dB, sweepResults.ExtinctionRatio_dB);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.Isoa_mA, iSOA_mA);

            //Echoxl.wang 2011-03-26
            //new add below parameter report to result file
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.IImbPeakL_mA, imb.Peak_L_mA);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.IImbPeakR_mA, imb.Peak_R_mA);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.Ipi_mA, this.iPi_mA);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.Diff_Imb_Max_Ctap_mA, Math.Abs(imb.MaxCtap_mA * 1000));
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.Diff_Imb_Max_Power_mW, imb.MaxPower_mW);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.SeVpeakL_V, this.seVpeakL_V);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.SeVR_V, this.seVR_V);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.SeVpi_V, this.seVpi_V);

            return vcmData;

        }

        /// <summary>
        /// Characterise MZ at a given common mode bias voltage
        /// </summary>
        /// <param name="ituChannelIndex"></param>
        /// <param name="vcmIndex"></param>
        private void MeasureMzAtVcmLevel(ITestEngine engine, MzSweepDataItuChannel mzItuChanData, int ituChannelIndex, int vcmIndex, IMB imb)
        {
            double iSOA_mA = DsdbrUtils.ReadSectionCurrent_mA(DSDBRSection.SOA);

            MzItuAtVcmData vcmData = new MzItuAtVcmData(this.vcm_V[vcmIndex], iSOA_mA);

            // store imbalance biases
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.IImbQuadL_mA, imb.Quad_L_mA);//original value is MzItuAtVcmMeasurements.IImbQuadL_mA, Echo update it
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.IImbQuadR_mA, imb.Quad_R_mA);

            // diff voltage sweep - assume VCM is negative. Apply calculated IQuad to the 
            // imbalance electrodes
            engine.SendToGui("Running Differential Modulator sweep on channel " + ituChannelIndex.ToString() + " from 0V to " + this.vcm_V[vcmIndex].ToString() + "V");
            ILMZSweepResult diffVsweep = null;

                if (!engine.IsSimulation)
                {
                    diffVsweep = mzDriverUtils.ModArm_DifferentialSweepByTrigger
                        (vcmData.GetValueDouble(MzItuAtVcmMeasurements.IImbQuadL_mA) / 1000,
                         vcmData.GetValueDouble(MzItuAtVcmMeasurements.IImbQuadR_mA) / 1000,
                         2 * this.vcm_V[vcmIndex], 0.0, NumberPoints, MZSourceMeasureDelay_ms, tapInline_V, MZTapBias_V, ArmSourceByAsic);
                }

                string sweepFileName;
                string fileNamePrefix = string.Format("MZCR_CH{0}_Vcm{1}_LV", ituChannelIndex, vcmIndex);
                SaveSweepDataToFile(diffVsweep, fileNamePrefix, out sweepFileName);
                allSweepFilesNames.Add(sweepFileName);

            // Display data
            engine.SendToGui(diffVsweep);
            //// write data to file
            //string diffVsweepFileNameStem = string.Format("MzDiffLV_CH{0}_VCM{1}", ituChannelIndex, vcmIndex);

            //vcmData.DiffLVSweepFile = Util_GenerateFileName.GenWithTimestamp
            //    (MzFileDirectory, diffVsweepFileNameStem, dutSerialNbr, "csv");

            //MzSweepFileWriter.WriteSweepData(vcmData.DiffLVSweepFile, diffVsweep,
            //    new ILMZSweepDataType[] 
            //    {ILMZSweepDataType.LeftArmModBias_V,
            //     ILMZSweepDataType.RightArmModBias_V,
            //     ILMZSweepDataType.FibrePower_mW,
            //     ILMZSweepDataType.TapComplementary_mA});

            // analyse the data.
            //
            // The first point needs to be close to zero. After that 
            // search for quadrature close to initialVcmOffset 
            // to help ensure that we pick the same slope each time.
            MzAnalysisWrapper.MzAnalysisResults mzAnlyDiffV =
                MzAnalysisWrapper.Differential_NegChirp_DifferentialSweep(diffVsweep, this.vcm_V[vcmIndex], initialVcmOffset);

            // Add a nice arrow to point at the quadrature point
            NPlot.PointD marker = new NPlot.PointD(mzAnlyDiffV.Quad_SrcL - mzAnlyDiffV.Quad_SrcR, mzAnlyDiffV.PowerAtMax_dBm - 3);
            engine.SendToGui(marker);

            // Add some information to help technicians & engineers
            NPlot.TextItem markerText = new NPlot.TextItem(new NPlot.PointD(marker.X, mzAnlyDiffV.PowerAtMin_dBm + 5), "Vpi = " + mzAnlyDiffV.MzAnalysis.Vpi.ToString("0.0"));
            engine.SendToGui(markerText);

            NPlot.VerticalLine verticalLine = new NPlot.VerticalLine(mzAnlyDiffV.MzAnalysis.VoltageAtMax, System.Drawing.Color.IndianRed);
            engine.SendToGui(verticalLine);

            verticalLine = new NPlot.VerticalLine(mzAnlyDiffV.MzAnalysis.VoltageAtMin, System.Drawing.Color.IndianRed);
            engine.SendToGui(verticalLine);

            if (System.Diagnostics.Debugger.IsAttached)
                System.Threading.Thread.Sleep(5000);

            vcmData.SetValueDouble(MzItuAtVcmMeasurements.VquadL_V, mzAnlyDiffV.Imb_SrcL);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.VquadR_V, mzAnlyDiffV.Imb_SrcR);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.VminL_V, mzAnlyDiffV.Min_SrcL);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.VminR_V, mzAnlyDiffV.Min_SrcR);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.VpeakL_V, mzAnlyDiffV.Max_SrcL);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.VpeakR_V, mzAnlyDiffV.Max_SrcR);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.Vpi_V, mzAnlyDiffV.Vpi);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.ER_dB, mzAnlyDiffV.ExtinctionRatio_dB);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.Isoa_mA, iSOA_mA);

            //Echoxl.wang 2011-03-26
            //new add below parameter report to result file
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.IImbPeakL_mA, imb.Peak_L_mA);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.IImbPeakR_mA, imb.Peak_R_mA);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.Ipi_mA, this.iPi_mA);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.Diff_Imb_Max_Ctap_mA, Math.Abs(imb.MaxCtap_mA * 1000));
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.Diff_Imb_Max_Power_mW, imb.MaxPower_mW);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.SeVpeakL_V, this.seVpeakL_V);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.SeVR_V, this.seVR_V);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.SeVpi_V, this.seVpi_V);

            // Choose subsequent slopes to be close to the first selected slope,
            // looking half way between 0 and the imbalance point should work.
            // This should help to choose the same slope in all cases

            if (this.featureToCenter == Alg_MZAnalysis.MzFeaturePowerType.Quad)
            {
                if (!initialOffsetSelected)
                    initialVcmOffset = (vcmData.GetValueDouble(MzItuAtVcmMeasurements.VquadL_V)
                        - vcmData.GetValueDouble(MzItuAtVcmMeasurements.VquadR_V)) / 2;
            }
            else
            {
                // jack.zhang 2010-04-02 tosa use Vmin instead of Vquadr
                if (!initialOffsetSelected)
                    initialVcmOffset = (vcmData.GetValueDouble(MzItuAtVcmMeasurements.VminL_V)
                        - vcmData.GetValueDouble(MzItuAtVcmMeasurements.VminR_V)) / 2;
            }
            initialOffsetSelected = true;

            mzItuChanData.VcmData.Add(vcmData);
        }

        /// <summary>
        /// Measure imbalance voltage from two single ended sweepResult
        /// </summary>
        /// <param name="engine">engine reference</param>
        /// <returns>Vimb</returns>
        private Alg_MZAnalysis.MZAnalysis measureSingleEndedResponse(ITestEngine engine, int chanNbr, double ideal_L_mA, double ideal_R_mA)
        {
            bool dataOk = true;
            double minLevel_dBm = -60;
            // Alice.Huang    2010-03-10
            // add to ensure rf sourmeter work in Vssouce mode

            //mzDriverUtils.SetVoltage(mzDriverUtils.MzInstrs.LeftArmMod, -this.mzInitSweepMaxAbs_V);
            //mzDriverUtils .SetVoltage (mzDriverUtils .MzInstrs .RightArmMod , -this.mzInitSweepMaxAbs_V);

            // do the left-arm modulation sweep

            engine.SendToGui("Running Left Modulator sweep");
            ILMZSweepResult sweepDataLeft = new ILMZSweepResult(); ;
            do
            {
                this.seVR_V = mzInitFixedModBias_volt;//record this result for writing into result file, Echo 2011-03-26
                if (!engine.IsSimulation)
                {
                    sweepDataLeft = mzDriverUtils.LeftModArm_SingleEndedSweep
                     (0,
                     ideal_L_mA / 1000,
                     ideal_R_mA / 1000,
                     -this.mzInitSweepMaxAbs_V,
                     0.0,
                     this.NumberPoints,
                     tapInline_V,
                     MZTapBias_V,
                     ArmSourceByAsic);
                }
                #region offline debug
                else
                {
                    // Load plot data for offline debugging
                    engine.SendToGui("Loading debug data from file");

                    sweepDataLeft.Type = SweepType.SingleEndVoltage;
                    sweepDataLeft.SrcMeter = SourceMeter.LeftModArm;

                    string fileName = string.Empty;

                    string searchPattern = string.Format("MZCR_LeftMod_LV*.csv");
                    string[] mzFiles = Directory.GetFiles(MzFileDirectory, searchPattern, SearchOption.AllDirectories);
                    if (mzFiles.Length > 0)
                        fileName = mzFiles[0];

                    Bookham.ToolKit.Mz.ILMZSweepResult.MzRawData rawData = MzSweepFileWriter.ReadPlotData(fileName);

                    for (int column = 0; column < rawData.plotData.Length; column++)
                    {
                        ILMZSweepDataType sweepType = (ILMZSweepDataType)Enum.Parse(typeof(ILMZSweepDataType), rawData.header[column]);
                        double[] plotArray = (double[])rawData.plotData[column].ToArray(typeof(double));
                        sweepDataLeft.SweepData.Add(sweepType, plotArray);
                    }
                }
                #endregion

                // If overrange run again
                if (MzAnalysisWrapper.CheckForOverrange(sweepDataLeft.SweepData[ILMZSweepDataType.FibrePower_mW]))
                {
                    dataOk = false;
                    if (double.IsNaN(mzDriverUtils.MzInstrs.PowerMeter.Range)) // auto range?
                        mzDriverUtils.MzInstrs.PowerMeter.Range = 1;
                    else
                        mzDriverUtils.MzInstrs.PowerMeter.Range = mzDriverUtils.MzInstrs.PowerMeter.Range * 10;
                }
                else
                {
                    // if underrange fix the data and continue
                    dataOk = true;
                    if (MzAnalysisWrapper.CheckForUnderrange(sweepDataLeft.SweepData[ILMZSweepDataType.FibrePower_mW]))
                    {
                        sweepDataLeft.SweepData[ILMZSweepDataType.FibrePower_mW] =
                            MzAnalysisWrapper.FixUnderRangeData(sweepDataLeft.SweepData[ILMZSweepDataType.FibrePower_mW]
                            , minLevel_dBm);
                    }
                }
            }
            while (!dataOk);

            string sweepfileName;
            string fileNamePrefix = "MZCR_LeftMod_LV_CH" + chanNbr.ToString() + "_LV";
            SaveSweepDataToFile(sweepDataLeft, fileNamePrefix, out sweepfileName);
            allSweepFilesNames.Add(sweepfileName);

            engine.SendToGui(sweepDataLeft);
            engine.SendToGui("Running Right Modulator sweep");

            ILMZSweepResult sweepDataRight;
            do
            {
                sweepDataRight = mzDriverUtils.RightModArm_SingleEndedSweep
                    (this.mzInitFixedModBias_volt,
                    ideal_L_mA / 1000,
                    ideal_R_mA / 1000,
                    -this.mzInitSweepMaxAbs_V,
                    0.0,
                    this.NumberPoints,
                    tapInline_V,
                    MZTapBias_V,
                    ArmSourceByAsic);

                // If overrange run again
                if (MzAnalysisWrapper.CheckForOverrange(sweepDataRight.SweepData[ILMZSweepDataType.FibrePower_mW]))
                {
                    dataOk = false;
                    if (double.IsNaN(mzDriverUtils.MzInstrs.PowerMeter.Range)) // auto range?
                        mzDriverUtils.MzInstrs.PowerMeter.Range = 1;
                    else
                        mzDriverUtils.MzInstrs.PowerMeter.Range = mzDriverUtils.MzInstrs.PowerMeter.Range * 10;
                }
                else
                {
                    // if underrange fix the data and continue
                    dataOk = true;
                    if (MzAnalysisWrapper.CheckForUnderrange(sweepDataRight.SweepData[ILMZSweepDataType.FibrePower_mW]))
                    {
                        sweepDataRight.SweepData[ILMZSweepDataType.FibrePower_mW] =
                            MzAnalysisWrapper.FixUnderRangeData(sweepDataRight.SweepData[ILMZSweepDataType.FibrePower_mW]
                            , minLevel_dBm);
                    }
                }
            }
            while (!dataOk);
            fileNamePrefix = "MZCR_RightMod_RV";
            SaveSweepDataToFile(sweepDataRight, fileNamePrefix, out sweepfileName);
            allSweepFilesNames.Add(sweepfileName);

            #region display joined plot data
            // Make some data to display
            double[] reversedRightArmPower = Alg_ArrayFunctions.ReverseArray(sweepDataRight.SweepData[ILMZSweepDataType.FibrePower_mW]);
            double[] reversedRightArmVoltage = Alg_ArrayFunctions.ReverseArray(sweepDataRight.SweepData[ILMZSweepDataType.RightArmModBias_V]);
            double[] positiveReversedRightArmVoltage = Alg_ArrayFunctions.MultiplyEachArrayElement(reversedRightArmVoltage, -1);
            double[] joinedModBias = Alg_ArrayFunctions.JoinArrays(sweepDataLeft.SweepData[ILMZSweepDataType.LeftArmModBias_V], positiveReversedRightArmVoltage);
            double[] joinedPower = Alg_ArrayFunctions.JoinArrays(sweepDataLeft.SweepData[ILMZSweepDataType.FibrePower_mW], reversedRightArmPower);

            ILMZSweepResult dataToDisplay = new ILMZSweepResult();
            dataToDisplay.Type = SweepType.SingleEndVoltage;
            dataToDisplay.SrcMeter = SourceMeter.LeftModArm;
            dataToDisplay.SweepData.Add(ILMZSweepDataType.LeftArmModBias_V, joinedModBias);
            dataToDisplay.SweepData.Add(ILMZSweepDataType.FibrePower_mW, joinedPower);
            engine.SendToGui(dataToDisplay);
            #endregion

            engine.SendToGui("Completed RightArm sweep");


            Alg_MZAnalysis.MZAnalysis mzSeAnly = Alg_MZAnalysis.ZeroChirpAnalysis(
                sweepDataLeft.SweepData[ILMZSweepDataType.LeftArmModBias_V],
                sweepDataRight.SweepData[ILMZSweepDataType.RightArmModBias_V],
                sweepDataLeft.SweepData[ILMZSweepDataType.FibrePower_mW],
                sweepDataRight.SweepData[ILMZSweepDataType.FibrePower_mW],
                false, false, 0);

            this.seVpeakL_V = mzSeAnly.VoltageAtMax;//store below two results for writing into result file //Echoxl.wang 2011-03-26
            this.seVpi_V = mzSeAnly.Vpi;

            return mzSeAnly;


        }

        /// <summary>
        /// Measure left arm response
        /// </summary>
        /// <param name="engine">engine reference</param>
        /// <returns>Vpi</returns>
        private Alg_MZAnalysis.MZAnalysis measureLeftArmSingleEndedResponse(ITestEngine engine, double ideal_L_mA, double ideal_R_mA)
        {
            bool dataOk = true;
            double minLevel_dBm = -60;

            // do the left-arm modulation sweep
            engine.SendToGui("Running Left Modulator sweep");
            ILMZSweepResult sweepDataLeft = new ILMZSweepResult();
            do
            {
                if (!engine.IsSimulation)
                {
                    sweepDataLeft = mzDriverUtils.LeftModArm_SingleEndedSweep
                     (this.mzInitFixedModBias_volt,
                     ideal_L_mA / 1000,
                     ideal_R_mA / 1000,
                     -this.mzInitSweepMaxAbs_V,
                     0.0,
                     this.NumberPoints,
                     tapInline_V,
                     MZTapBias_V,
                     ArmSourceByAsic);
                }
                #region offline debug
                else
                {
                    // Load plot data for offline debugging
                    engine.SendToGui("Loading debug data from file");

                    sweepDataLeft.Type = SweepType.SingleEndVoltage;
                    sweepDataLeft.SrcMeter = SourceMeter.LeftModArm;

                    string fileName = string.Empty;

                    string searchPattern = string.Format("MZCR_LeftMod_LV*.csv");
                    string[] mzFiles = Directory.GetFiles(MzFileDirectory, searchPattern, SearchOption.AllDirectories);
                    if (mzFiles.Length > 0)
                        fileName = mzFiles[0];


                    Bookham.ToolKit.Mz.ILMZSweepResult.MzRawData rawData = MzSweepFileWriter.ReadPlotData(fileName);

                    for (int column = 0; column < rawData.plotData.Length; column++)
                    {
                        ILMZSweepDataType sweepType = (ILMZSweepDataType)Enum.Parse(typeof(ILMZSweepDataType), rawData.header[column]);
                        double[] plotArray = (double[])rawData.plotData[column].ToArray(typeof(double));
                        sweepDataLeft.SweepData.Add(sweepType, plotArray);
                    }
                }
                #endregion

                // If overrange run again
                if (MzAnalysisWrapper.CheckForOverrange(sweepDataLeft.SweepData[ILMZSweepDataType.FibrePower_mW]))
                {
                    dataOk = false;
                    if (double.IsNaN(mzDriverUtils.MzInstrs.PowerMeter.Range)) // auto range?
                        mzDriverUtils.MzInstrs.PowerMeter.Range = 1;
                    else
                        mzDriverUtils.MzInstrs.PowerMeter.Range = mzDriverUtils.MzInstrs.PowerMeter.Range * 10;
                }
                else
                {
                    // if underrange fix the data and continue
                    dataOk = true;
                    if (MzAnalysisWrapper.CheckForUnderrange(sweepDataLeft.SweepData[ILMZSweepDataType.FibrePower_mW]))
                    {
                        sweepDataLeft.SweepData[ILMZSweepDataType.FibrePower_mW] =
                            MzAnalysisWrapper.FixUnderRangeData(sweepDataLeft.SweepData[ILMZSweepDataType.FibrePower_mW]
                            , minLevel_dBm);
                    }
                }
            }
            while (!dataOk);

            string sweepfileName;
            string fileNamePrefix = "MZCR_LeftMod_LV";
            SaveSweepDataToFile(sweepDataLeft, fileNamePrefix, out sweepfileName);
            allSweepFilesNames.Add(sweepfileName);

            engine.SendToGui(sweepDataLeft);


            // If debugging, give the engineer a chance to look at the plot
            if (System.Diagnostics.Debugger.IsAttached)
                System.Threading.Thread.Sleep(5000);


            // Alice.huang    2010-05-05
            // use this new function to ensure to find the valley closest to offset
            Alg_MZAnalysis.MZAnalysis mzSeAnly = Alg_MZAnalysis.FindFeaturePointClosestToOffset(
                sweepDataLeft.SweepData[ILMZSweepDataType.LeftArmModBias_V],
                sweepDataLeft.SweepData[ILMZSweepDataType.FibrePower_mW],
                false, 0, Alg_MZAnalysis.MzFeaturePowerType.Min);

            this.seVpeakL_V = mzSeAnly.VoltageAtMax;//store below two results for writing into result file //Echoxl.wang 2011-03-26
            this.seVpi_V = mzSeAnly.Vpi;

            return mzSeAnly;
        }

        /// <summary>
        /// Add a file to our zip file
        /// </summary>
        /// <param name="zipStream">Zip Output Stream</param>
        /// <param name="file">file name to add (relative path)</param>
        /// <param name="buffer">byte buffer to read data from the file to</param>
        private void addZipFile(ZipOutputStream zipStream, string file, byte[] buffer)
        {
            if (!File.Exists(file)) return;
            ZipEntry entry = new ZipEntry(Path.GetFileName(file));
            entry.DateTime = DateTime.Now;
            zipStream.PutNextEntry(entry);
            using (FileStream fs = File.OpenRead(file))
            {

                // Using a fixed size buffer here makes no noticeable difference for output
                // but keeps a lid on memory usage.
                int sourceBytes;
                do
                {
                    sourceBytes = fs.Read(buffer, 0, buffer.Length);
                    zipStream.Write(buffer, 0, sourceBytes);
                } while (sourceBytes > 0);
            }
            zipStream.Flush();
        }

        /// <summary>
        /// Close the Imb Current to last channel:chan57 close to chan0 and chan99 close to chan57
        /// </summary>
        private void CloseImbToLastChan(IMB imb, Alg_MZLVMinMax.DataPoint[] peakArray, Alg_MZLVMinMax.DataPoint[] valleyArray, MzAnalysisWrapper.MzAnalysisResults mzAnlyDiffI)
        {
            double imbToClose_mA = 0;

            if (indexOf3Channels == 1)
            {
                imbToClose_mA = IMB.Final_1stChannel_mA;
            }
            else if (indexOf3Channels == 2)
            {
                imbToClose_mA = IMB.Final_MiddleChannel_mA;
            }

            /// STEP 1:  Find the IMB - chongjian.liang

            double imb_mA = (mzAnlyDiffI.Imb_SrcL - mzAnlyDiffI.Imb_SrcR) * 1000;
            double imb_2PIRight_mA = imb_mA + 2 * this.iPi_mA;
            double imb_2PILeft_mA = imb_mA - 2 * this.iPi_mA;

            double imb_Difference_mA = Math.Abs(imbToClose_mA - imb_mA);
            double IMB_2PIRight_Difference_mA = double.MaxValue;
            double IMB_2PILeft_Difference_mA = double.MaxValue;

            /* Don't limit the point with the IMB current limit, because even the
             * point after moving 2PI fails the IMB current limit, it may still be
             * in the same PI phase with the last channel' IMB.
             * Because we want all channels work in the same phase, so we still move
             * 2PI, but crop the point after moving 2PI with IMB current limit.
             * This step is executed right after this function. - chongjian.liang*/

            // Guarantee the point exists in the LI sweep result after moving 2PI.

            if ((imb_2PIRight_mA <= valleyArray[valleyArray.Length - 1].Voltage * 1000 && !ParamManager.Conditions.IsThermalPhase)
                || (imb_2PIRight_mA <= peakArray[peakArray.Length - 1].Voltage * 1000 && ParamManager.Conditions.IsThermalPhase))
            {
                IMB_2PIRight_Difference_mA = Math.Abs(imbToClose_mA - imb_2PIRight_mA);
            }

            if ((imb_2PILeft_mA >= peakArray[0].Voltage * 1000 && !ParamManager.Conditions.IsThermalPhase)
                || (imb_2PILeft_mA >= valleyArray[0].Voltage * 1000 && ParamManager.Conditions.IsThermalPhase))
            {
                IMB_2PILeft_Difference_mA = Math.Abs(imbToClose_mA - imb_2PILeft_mA);
            }

            double difference_MIN_mA = Math.Min(Math.Min(imb_Difference_mA, IMB_2PIRight_Difference_mA), IMB_2PILeft_Difference_mA);

            if (difference_MIN_mA == IMB_2PIRight_Difference_mA)
            {
                mzAnlyDiffI.Imb_SrcL += this.iPi_mA / 1000;
                mzAnlyDiffI.Imb_SrcR -= this.iPi_mA / 1000;
            }
            else if (difference_MIN_mA == IMB_2PILeft_Difference_mA)
            {
                mzAnlyDiffI.Imb_SrcL -= this.iPi_mA / 1000;
                mzAnlyDiffI.Imb_SrcR += this.iPi_mA / 1000;
            }

            /// STEP 2: Calc out the relative feature for this new IMB

            if (difference_MIN_mA != imb_Difference_mA)
            {
                double IMB_V = mzAnlyDiffI.Imb_SrcL - mzAnlyDiffI.Imb_SrcR;

                // Find the peak

                Alg_MZLVMinMax.DataPoint peak_IMB = new Alg_MZLVMinMax.DataPoint();

                foreach (Alg_MZLVMinMax.DataPoint peak in peakArray)
                {
                    if (peak.Voltage < IMB_V)
                    {
                        peak_IMB = peak;
                    }
                    else
                    {
                        break;
                    }
                }

                // Find the valley

                Alg_MZLVMinMax.DataPoint valley_IMB = new Alg_MZLVMinMax.DataPoint();

                for (int i = valleyArray.Length - 1; i >= 0; i--)
                {
                    if (valleyArray[i].Voltage > IMB_V)
                    {
                        valley_IMB = valleyArray[i];
                    }
                    else
                    {
                        break;
                    }
                }

                // Recalc out the relative feature for this new IMB

                MzAnalysisWrapper.CalcDifferentialArmValues(peak_IMB.Voltage, imb.ICM_A, out mzAnlyDiffI.Max_SrcL, out mzAnlyDiffI.Max_SrcR);
                MzAnalysisWrapper.CalcDifferentialArmValues(valley_IMB.Voltage, imb.ICM_A, out mzAnlyDiffI.Min_SrcL, out mzAnlyDiffI.Min_SrcR);
            }
        }

        /// <summary>
        /// Process the MZ Sweep results, writing out a zip of the sweepResult, a summary CSV file and
        /// returning a flag to say if all sweepResult passed or not
        /// </summary>
        /// <param name="mzSweepData">Sweep data to be processed</param>
        /// <param name="mzSweepDataResultsFile">Data results file</param>
        /// <param name="mzSweepDataZipFile">Zip sweep file</param>
        /// <param name="sweepsAllPassed">True if all sweepResult passed, false if any failed</param>
        private void processMzSweepData(List<MzSweepDataItuChannel> mzSweepData, out string mzSweepDataResultsFile, out string mzSweepDataZipFile, out bool sweepsAllPassed)
        {
            mzSweepDataZipFile = Util_GenerateFileName.GenWithTimestamp(MzFileDirectory, "MzSweepDataZipFile", dutSerialNbr, "zip");

            using (ZipOutputStream zipStream = new ZipOutputStream(File.Create(mzSweepDataZipFile)))
            {
                zipStream.SetLevel(9); // 0 - store only to 9 - means best compression

                byte[] buffer = new byte[4096]; // byte buffer to zip into
                foreach (MzSweepDataItuChannel ituChan in mzSweepData)
                {
                    foreach (MzItuAtVcmData vcmMeas in ituChan.VcmData)
                    {
                        addZipFile(zipStream, vcmMeas.DiffLISweepFile, buffer);
                        addZipFile(zipStream, vcmMeas.DiffLVSweepFile, buffer);
                    }
                }
                foreach (string file in allSweepFilesNames)
                {
                    addZipFile(zipStream, file, buffer);
                }
            }

            sweepsAllPassed = true;

            // write the summary file and check if it all passed
            mzSweepDataResultsFile = Util_GenerateFileName.GenWithTimestamp
                (MzFileDirectory, "MzSweepDataResultsFile", dutSerialNbr, "csv");

            int channel;
            using (StreamWriter writer = new StreamWriter(mzSweepDataResultsFile))
            {
                // file header
                string defaultFileHeader = "ITUChannel,Frequency_GHz,Vcm_V,Status,IImbL_mA,IImbR_mA,VquadL_V,VquadR_V,VpeakL_V,VpeakR_V,VminL_V,VminR_V,Vpi_V, ER_dB";
                StringBuilder fileHeader = new StringBuilder();
                bool fileHeaderOK = false;
                fileHeader.Append("ITUChannel, Frequency_GHz,Vcm_V,Status");

                // file contents
                List<string> fileContents = new List<string>();

                foreach (MzSweepDataItuChannel ituChan in mzSweepData)
                {
                    channel = ituChan.ItuChannelIndex;
                    foreach (MzItuAtVcmData vcmData in ituChan.VcmData)
                    {
                        // fail check
                        if (vcmData.OverallStatus == PassFail.Fail) sweepsAllPassed = false;

                        // each line
                        StringBuilder aLine = new StringBuilder();
                        aLine.Append(channel);
                        aLine.Append(",");
                        aLine.Append(ituChan.ItuFrequency_GHz);
                        aLine.Append(",");
                        aLine.Append(vcmData.Vcm_V);
                        aLine.Append(",");
                        aLine.Append(vcmData.OverallStatus.ToString());

                        // for each parameter...
                        foreach (string nameAsStr in Enum.GetNames(typeof(MzItuAtVcmMeasurements)))
                        {
                            MzItuAtVcmMeasurements parameter = (MzItuAtVcmMeasurements)Enum.Parse(typeof(MzItuAtVcmMeasurements), nameAsStr);

                            //if (!nameAsStr.Contains("original"))
                            {
                                // build file header
                                if (!fileHeaderOK)
                                {
                                    fileHeader.Append(",");
                                    fileHeader.Append(nameAsStr);
                                    fileHeader.Append("[");
                                    fileHeader.Append(vcmData.GetMeasuredDataLowLimit(parameter));
                                    fileHeader.Append(";");
                                    fileHeader.Append(vcmData.GetMeasuredDataHighLimit(parameter));
                                    fileHeader.Append("]");
                                }

                                // build the line
                                aLine.Append(",");
                                if (vcmData.IsTested(parameter))
                                    aLine.Append(vcmData.GetValueDouble(parameter));
                            }
                        }

                        if (!fileHeaderOK)
                            fileHeaderOK = true;

                        fileContents.Add(aLine.ToString());
                    }
                }

                // write file header to file
                if (!fileHeaderOK)
                {
                    writer.WriteLine(defaultFileHeader);
                }
                else
                {
                    writer.WriteLine(fileHeader.ToString());
                }

                // writer file contents to file
                foreach (string row in fileContents)
                {
                    writer.WriteLine(row);
                }
            }
        }

        /// <summary>
        /// Write arm sweep data to a file, and out the file name. - chongjian.liang
        /// </summary>
        /// <param name="sweepResult"> sweep data</param>
        /// <param name="file_prefix"> file name prefix </param>
        /// <param name="sweepFileName"> file name that the sweep data save as </param>
        private void SaveSweepDataToFile(ILMZSweepResult sweepData, string file_prefix, out string sweepFileName)
        {
            Dictionary<ILMZSweepDataType, double[]> sweepDataListTemp = sweepData.SweepData;
            List<string> dataNameList = new List<string>();
            List<double[]> sweepDataList = new List<double[]>();

            foreach (ILMZSweepDataType dataName in sweepDataListTemp.Keys)
            {
                dataNameList.Add(dataName.ToString());
                sweepDataList.Add(sweepDataListTemp[dataName]);
            }

            double[][] sweepDataArray = sweepDataList.ToArray();

            string[] dataNameArray = dataNameList.ToArray();

            if ((sweepDataArray.Length > 0) && (dataNameArray[0].Length > 0))
            { }
            else
            {
                sweepFileName = "";
                return;
            }

            string mzSweepDataResultsFile = Util_GenerateFileName.GenWithTimestamp
                (MzFileDirectory, file_prefix, dutSerialNbr, "csv");

            using (StreamWriter writer = new StreamWriter(mzSweepDataResultsFile))
            {
                // file header
                StringBuilder fileHeader = new StringBuilder();

                for (int count = 0; count < dataNameArray.Length; count++)
                {
                    if (count != 0) fileHeader.Append(",");
                    fileHeader.Append(dataNameArray[count]);
                }

                // file contents
                List<string> fileContents = new List<string>();
                for (int irow = 0; irow < sweepDataArray[0].Length; irow++)
                {
                    StringBuilder aLine = new StringBuilder();
                    for (int icol = 0; icol < dataNameArray.Length; icol++)
                    {
                        if (icol != 0) aLine.Append(",");
                        aLine.Append(sweepDataArray[icol][irow].ToString());
                    }
                    fileContents.Add(aLine.ToString());
                }

                // write file header to file
                writer.WriteLine(fileHeader.ToString());

                // writer file contents to file
                foreach (string row in fileContents)
                {
                    writer.WriteLine(row);
                }
            }
            sweepFileName = mzSweepDataResultsFile;
        }

        /// <summary>
        /// Write a single MZ measurement to CSV file
        /// </summary>
        /// <param name="writer">Text Writer that is writing our file</param>
        /// <param name="vcmData">VCM data to write from</param>
        /// <param name="measurement">Which measurement to write?</param>
        private void writeMzMeas(TextWriter writer, MzItuAtVcmData vcmData, MzItuAtVcmMeasurements measurement)
        {
            string valueStr = "";
            if (vcmData.IsTested(measurement))
            {
                double d = vcmData.GetValueDouble(measurement);
                valueStr = d.ToString();
            }
            writer.Write(',');
            writer.Write(valueStr);
        }

        /// <summary>
        /// 
        /// </summary>
        public Type UserControl
        {
            get { return (typeof(IlmzMzCharacteriseGui)); }
        }

        #endregion

        private class IMB
        {
            public IMB(double icm_mA)
            {
                this.Min_L_mA = ParamManager.Spec.CH_MZ_CTRL_L_I.Low;
                this.Max_L_mA = ParamManager.Spec.CH_MZ_CTRL_L_I.High;
                this.Min_R_mA = ParamManager.Spec.CH_MZ_CTRL_R_I.Low;
                this.Max_R_mA = ParamManager.Spec.CH_MZ_CTRL_R_I.High;
                this.ICM_mA = icm_mA;
                this.ICM_A = icm_mA / 1000;
            }

            public double Current_L_mA;
            public double Current_R_mA;

            public double Quad_L_mA
            {
                get { return propertyFields.Quad_L_mA; }
                set
                {
                    propertyFields.Quad_L_mA = value;

                    this.Current_L_mA = value;
                }
            }

            public double Quad_R_mA
            {
                get { return propertyFields.Quad_R_mA; }
                set
                {
                    propertyFields.Quad_R_mA = value;

                    this.Current_R_mA = value;
                }
            }

            public double Peak_L_mA;
            public double Peak_R_mA;

            public double QuadHalfPILeft_L_mA
            {
                get { return propertyFields.QuadHalfPILeft_L_mA; }
                set
                {
                    propertyFields.QuadHalfPILeft_L_mA = value;

                    this.Current_L_mA = value;
                }
            }

            public double QuadHalfPILeft_R_mA
            {
                get { return propertyFields.QuadHalfPILeft_R_mA; }
                set
                {
                    propertyFields.QuadHalfPILeft_R_mA = value;

                    this.Current_R_mA = value;
                }
            }

            public double QuadHalfPIRight_L_mA
            {
                get { return propertyFields.QuadHalfPIRight_L_mA; }
                set
                {
                    propertyFields.QuadHalfPIRight_L_mA = value;

                    this.Current_L_mA = value;
                }
            }

            public double QuadHalfPIRight_R_mA
            {
                get { return propertyFields.QuadHalfPIRight_R_mA; }
                set
                {
                    propertyFields.QuadHalfPIRight_R_mA = value;

                    this.Current_R_mA = value;
                }
            }

            public double Quad2PILeft_L_mA
            {
                get { return propertyFields.Quad2PILeft_L_mA; }
                set
                {
                    propertyFields.Quad2PILeft_L_mA = value;

                    this.Current_L_mA = value;
                }
            }

            public double Quad2PILeft_R_mA
            {
                get { return propertyFields.Quad2PILeft_R_mA; }
                set
                {
                    propertyFields.Quad2PILeft_R_mA = value;

                    this.Current_R_mA = value;
                }
            }

            public double Quad2PILeft_Diff_mA
            {
                get
                {
                    return Quad2PILeft_L_mA - Quad2PILeft_R_mA;
                }
            }

            public double Quad2PIRight_L_mA
            {
                get { return propertyFields.Quad2PIRight_L_mA; }
                set
                {
                    propertyFields.Quad2PIRight_L_mA = value;

                    this.Current_L_mA = value;
                }
            }

            public double Quad2PIRight_R_mA
            {
                get { return propertyFields.Quad2PIRight_R_mA; }
                set
                {
                    propertyFields.Quad2PIRight_R_mA = value;

                    this.Current_R_mA = value;
                }
            }

            public double Quad2PIRight_Diff_mA
            {
                get
                {
                    return Quad2PIRight_L_mA - Quad2PIRight_R_mA;
                }
            }

            public double TargetER_L_mA
            {
                get { return propertyFields.TargetER_L_mA; }
                set
                {
                    propertyFields.TargetER_L_mA = value;

                    this.Current_L_mA = value;
                }
            }

            public double TargetER_R_mA
            {
                get { return propertyFields.TargetER_R_mA; }
                set
                {
                    propertyFields.TargetER_R_mA = value;

                    this.Current_R_mA = value;
                }
            }

            public double TargetER_Diff_mA
            {
                get
                {
                    return TargetER_L_mA - TargetER_R_mA;
                }
            }

            public double LVQuadLimit_L_mA
            {
                get { return propertyFields.LVQuadLimit_L_mA; }
                set
                {
                    propertyFields.LVQuadLimit_L_mA = value;

                    this.Current_L_mA = value;
                }
            }

            public double LVQuadLimit_R_mA
            {
                get { return propertyFields.LVQuadLimit_R_mA; }
                set
                {
                    propertyFields.LVQuadLimit_R_mA = value;

                    this.Current_R_mA = value;
                }
            }

            public double LVQuadLimit_Diff_mA
            {
                get
                {
                    return LVQuadLimit_L_mA - LVQuadLimit_R_mA;
                }
            }

            public double Slope_ERToLeftIMB;

            public readonly double Max_L_mA;
            public readonly double Max_R_mA;

            public readonly double Min_L_mA;
            public readonly double Min_R_mA;

            public readonly double ICM_mA;
            public readonly double ICM_A;

            public double Max_Diff_mA
            {
                get
                {
                    return Max_L_mA - Min_R_mA;
                }
            }

            public double Min_Diff_mA
            {
                get
                {
                    return Min_L_mA - Max_R_mA; 
                }
            }

            public double MaxCtap_mA;
            public double MaxPower_mW;

            public static double Final_1stChannel_mA;
            public static double Final_MiddleChannel_mA;

            public const double MinToMove_mA = 0.0001;

            private PropertyFields propertyFields = new PropertyFields();

            private struct PropertyFields
            {
                public double Quad_L_mA;
                public double Quad_R_mA;

                public double QuadHalfPILeft_L_mA;
                public double QuadHalfPILeft_R_mA;

                public double QuadHalfPIRight_L_mA;
                public double QuadHalfPIRight_R_mA;

                public double Quad2PILeft_L_mA;
                public double Quad2PILeft_R_mA;

                public double Quad2PIRight_L_mA;
                public double Quad2PIRight_R_mA;

                public double TargetER_L_mA;
                public double TargetER_R_mA;

                public double LVQuadLimit_L_mA;
                public double LVQuadLimit_R_mA;
            }
        }

        private struct LimitJudger
        {
            public static bool IsERTooLarge(MzAnalysisWrapper.MzAnalysisResults lvResult, double max_ER_dB)
            {
                return lvResult.ExtinctionRatio_dB > max_ER_dB;
            }

            public static bool IsERTooSmall(MzAnalysisWrapper.MzAnalysisResults lvResult, double min_ER_dB)
            {
                return lvResult.ExtinctionRatio_dB < min_ER_dB;
            }

            public static bool IsQuadOutOfLeft(MzAnalysisWrapper.MzAnalysisResults lvResult, double quadMin_mA)
            {
                return lvResult.Quad_SrcL < quadMin_mA;
            }

            public static bool IsQuadOutOfRight(MzAnalysisWrapper.MzAnalysisResults lvResult, double quadMin_mA)
            {
                return lvResult.Quad_SrcR < quadMin_mA;
            }

            public static bool IsAllPass_WithoutER(MzSweepDataItuChannel mzItuChanData)
            {
                if (mzItuChanData.VcmData[mzItuChanData.VcmData.Count - 1].OverallStatus == PassFail.Pass)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            public static bool IsAllPass(MzSweepDataItuChannel mzItuChanData, MzAnalysisWrapper.MzAnalysisResults lvResult, double max_ER_dB, double min_ER_dB)
            {
                if (mzItuChanData.VcmData[mzItuChanData.VcmData.Count - 1].OverallStatus == PassFail.Pass
                    && !IsERTooLarge(lvResult, max_ER_dB)
                    && !IsERTooSmall(lvResult, min_ER_dB))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        private struct MoveIMBTo
        {
            public bool HalfPIRight;
            public bool TwoPIRight;
            public bool EnlargeToTargetER;

            public bool HalfPILeft;
            public bool TwoPILeft;
            public bool AttenuateToTargetER;
        }
    }
}

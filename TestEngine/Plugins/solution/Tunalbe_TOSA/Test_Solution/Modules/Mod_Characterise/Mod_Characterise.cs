// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// Mod_Characterise.cs
//
// Author: tommy.yu, 2009
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.fcumapping.CommonData;
using Bookham.TestLibrary.Instruments;
using System.Collections.Specialized;
using System.ComponentModel;
using Bookham.TestLibrary.Utilities;
using System.IO;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestSolution.IlmzCommonUtils;
using Bookham.TestSolution.Instruments;


namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Test Module Template.
    /// </summary>
    public class Mod_Characterise : ITestModule
    {
        #region ITestModule Members

        string Itu_OP_file = "";
        int Itu_OP_count = 0;
        string GuessItuFile = "";
        //string[] MissingLineArray = null;
        private ITestEngine myEngine = null;

        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, DatumList configData, InstrumentCollection instruments, ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {

            // TODO - add your code here!
            myEngine = engine;
            engine.GuiToFront();
            engine.GuiShow();
            TestParamConfigAccessor settings = (TestParamConfigAccessor)configData.ReadReference("Characterization_Settings");
            List<CDSDBRSuperMode> supermodes = (List<CDSDBRSuperMode>)configData.ReadReference("SUPERMODES");
            string frontsection_setup_file_path = settings.GetStringParam("FrontCurrentFile");
            string testtimeStamp = configData.ReadString("TIME_STAMP");
            string laserId = configData.ReadString("LASERID");
            string work_dir = configData.ReadString("RESULT_DIR");
            string equipId = configData.ReadString("EQUIP_ID");
            double startFrequency = configData.ReadDouble("StartFrequency");
            double FrequencyTolerance_GHz =Math.Abs(configData.ReadDouble("FrequencyTolerance_GHz"));
            bool useDUTEtalon = configData.ReadBool("USEDUTETALON");
            bool UseLowCostSolution = configData.ReadBool("UseLowCostSolution");
            //Can not use lowcost solution if use DUT LockerRatio as mapping source and DUT temperature different between mapping and pre/pos buring. jack.zhang 2013-04-15
            if(UseLowCostSolution&&useDUTEtalon&&!FreqCalByEtalon.UseDUTLockerRatio)
                UseLowCostSolution = false;
            double Target_LockerCurrent = configData.ReadDouble("Target_LockerCurrent");
            double Max_LockerCurrent_mA = configData.ReadDouble("Max_LockerCurrent_mA");
            double Min_LockerCurrent_mA = configData.ReadDouble("Min_LockerCurrent_mA");
            double MAX_REARSOA =configData.ReadDouble("MAX_REARSOA");
            double MIN_REARSOA = configData.ReadDouble("MIN_REARSOA");
            int channelNbr = configData.ReadSint32("ChanNbr");
            int FrequencyStep = configData.ReadSint32("FrequencyStep");
            double maxFreq = startFrequency + (channelNbr - 1) * FrequencyStep;
            //bool isDelItuFile_When_MissingLine = configData.ReadBool("isDelItuFile_MissingLine");
            //MissingLineArray = configData.ReadStringArray("MissingLineArray");
            CsvReader csvReader = new CsvReader();
            List<string[]> FrontSection_Current_List = csvReader.ReadFile(frontsection_setup_file_path);
            List<double> Mz_Imb_Left_mA_SM=(List<double>)configData.ReadReference("Mz_Imb_Left_mA_List");
            List<double> Mz_Imb_Right_mA_SM=(List<double>)configData.ReadReference("Mz_Imb_Right_mA_List");

            IInstType_TecController DUT_Tec = (IInstType_TecController)instruments["TecDsdbr"];
            Inst_Ke2510 Optical_Switch = (Inst_Ke2510)instruments["TecDsdbr"];
            IInstType_DigitalIO outLine_Tx = Optical_Switch.GetDigiIoLine(IlmzOpticalSwitchLines.Etalon_Tx_DigiIoLine);//Jack.zhang Tx swith between DUT GB and Etalon Wavemeter
            IInstType_DigitalIO outLine_Rx = Optical_Switch.GetDigiIoLine(IlmzOpticalSwitchLines.Etalon_Rx_DigiIoLine);//Jack.zhang Tx swith between DUT GB and Etalon Wavemeter
            IInstType_DigitalIO outLine_Reference_Ctap = Optical_Switch.GetDigiIoLine(IlmzOpticalSwitchLines.Etalon_Ref_DigiIoLine);//Jack.zhang PD control
            outLine_Reference_Ctap.LineState = IlmzOpticalSwitchLines.Etalon_Ref_DigiIoLine_State;//Jack,false for reference true for ctap

            Inst_Fcu2Asic fcu = (Inst_Fcu2Asic)instruments["Fcu2Asic"];
            CCGOperatingPoints cgops = null;
            cgops = new CCGOperatingPoints(fcu, supermodes);
            cgops.ProgramSettings = settings;
            cgops.setup(testtimeStamp, laserId, settings.GetStringParam("ITUSetupFile"));
            cgops.Result_dir = work_dir;
            cgops.OnLongitudinalModeFinished += MyEventHandler;
            cgops.setMaxMinFrequency(maxFreq, startFrequency);
            cgops.setChannelNbr(channelNbr, FrequencyStep);
            cgops.slope_rear_rearsoa = (double[,])configData.ReadReference("slope_rear_rearsoa");
            cgops.Target_LockerCurrent_mA = Target_LockerCurrent;
            cgops.Max_LockerCurrent_mA =  Max_LockerCurrent_mA;
            cgops.Min_LockerCurrent_mA =  Min_LockerCurrent_mA;
            cgops.Max_RearSoa_mA = MAX_REARSOA;
            cgops.Min_RearSoa_mA = MIN_REARSOA;
            cgops.Mz_Imb_Left_mA_SM = Mz_Imb_Left_mA_SM;
            cgops.Mz_Imb_Right_mA_SM = Mz_Imb_Right_mA_SM;
            cgops.FrequencyTolerance_GHz = FrequencyTolerance_GHz;
            cgops.FrontSectionCurrent_mA_List = new List<double>();
            cgops.useDUTEtalon=useDUTEtalon;
            cgops.outLine_Tx = outLine_Tx;
            cgops.outLine_Rx = outLine_Rx;
            foreach (string[] item in FrontSection_Current_List)
            {
                cgops.FrontSectionCurrent_mA_List.Add(Convert.ToDouble(item[0]));
            }
            if (UseLowCostSolution)
            {
                cgops.findITUOperatingPointsByCalibration_Method();//cal freq by Etalon Wavemeter. Jack.zhang12/11/2010
                GuessItuFile = cgops.ITUEstimateFilePath;
            }
            else
            {
                if (Measurements.Wavemeter != null
                    && Measurements.Wavemeter.IsOnline)
                {
                    cgops.findITUOperatingPointsUsingWavemeter();
                    GuessItuFile = cgops.ITUOperatingPointsFilePath;
                }
                else
                {
                    string errorDes = string.Format("Can not achieve mapping, recheck Map setting!");
                    engine.ShowContinueUserQuery(errorDes);
                }
            }

            Itu_OP_file = cgops.ITUOperatingPointsFilePath;
            Itu_OP_count = cgops.ITUPointsCount;

            if (cgops.CalFreqWrong)
            {
                string emailSubject = "FreqWrong_" + laserId + "_" + testtimeStamp + "_" + equipId;
                string emailBody ="For Detail pls refer "+GuessItuFile.Substring(8, GuessItuFile.Length - 8);
                try
                {
                    myEngine.SendEmail("LowCost_FrequencyError", emailSubject, emailBody);
                }
                catch (Exception ex)
                {
                    myEngine.ShowContinueUserQuery("Fail to Send email for LowCost_Mapping message.See developer log for more.");
                    myEngine.SendStatusMsg(ex.ToString());
                }
            }
            //if (MissingLineArray.Length > 0 && isDelItuFile_When_MissingLine)
            //{
            //    ReBuildItuFile();
            //}

            bool isRTHFailed = false;

            #region Read RTH - chongjian.liang 2013.4.25

            double RTH_ohm = DUT_Tec.SensorResistanceActual_ohm;

            int timeWaitCount = 0;

            double CH_LASER_RTH_Min = configData.ReadDouble("CH_LASER_RTH_Min");
            double CH_LASER_RTH_Max = configData.ReadDouble("CH_LASER_RTH_Max");

            while (RTH_ohm < CH_LASER_RTH_Min || RTH_ohm > CH_LASER_RTH_Max)
            {
                // Check RTH of TecDsdbr every 0.01s
                System.Threading.Thread.Sleep(10);

                RTH_ohm = DUT_Tec.SensorResistanceActual_ohm;

                // If RTH is still out of limits in 500*0.01s = 5s, abort the test.
                if (timeWaitCount++ >= 500)
                {
                    isRTHFailed = true;
                    break;
                }
            }
            #endregion

            DatumList returnData = new DatumList();

            returnData.AddFileLink("CG_CHAR_ITU_FILE", Itu_OP_file);
            returnData.AddSint32("CHAR_ITU_OP_COUNT", Itu_OP_count);
            //returnData.AddFileLink("GuessItuFile", GuessItuFile);
            returnData.AddDouble("Laser_Rth", RTH_ohm);
            returnData.AddBool("IsRTHFailed", isRTHFailed); // Add param to check RTH - chongjian.liang 2013.4.25
            return returnData;
        }
        private void ReBuildItuFile()
        {
/*
            List<string> ChanIndexNoRepeat = new List<string>();

            CsvReader cr = new CsvReader();
            List<string[]> ituChannelDataList = cr.ReadFile(Itu_OP_file);

            File.Delete(Itu_OP_file);

            StreamWriter sw = new StreamWriter(Itu_OP_file);
            for (int i = 0; i < ituChannelDataList.Count; i++)
            {
                string chanNo = ituChannelDataList[i][0];
                string SmNo = ituChannelDataList[i][2];
                string LmNo = ituChannelDataList[i][3];
                string MissingLineStr = SmNo + "_" + LmNo;

                bool isInBadLM = false;
                for (int k = 0; k < MissingLineArray.Length; k++)
                {
                    string str_missingArray = MissingLineArray[k];
                    if (str_missingArray.Contains(MissingLineStr))
                    {
                        isInBadLM = true;
                        break;
                    }
                }
                if (isInBadLM) continue; // if itu point located in bad LM, then ignore this itu point.

                for (int j = 0; j < ituChannelDataList[i].Length; j++)
                {
                    if (j == ituChannelDataList[i].Length - 1) // the last data in itu channel data line
                    {
                        sw.WriteLine(ituChannelDataList[i][j]);
                    }
                    else
                    {
                        sw.Write(ituChannelDataList[i][j] + ",");
                    }
                    sw.Flush();
                }
                if (i > 0)
                {
                    if (!ChanIndexNoRepeat.Contains(chanNo))
                    {
                        ChanIndexNoRepeat.Add(chanNo);
                    }
                }

            }
            sw.Close();
            this.Itu_OP_count = ChanIndexNoRepeat.Count;
*/
        }

        public Type UserControl
        {
            get { return (typeof(Mod_CharacteriseGui)); }
        }

        public void MyEventHandler(object sender, ProgressChangedEventArgs e)
        {
            if (myEngine != null)
                myEngine.SendToGui(e.ProgressPercentage);
        }

        #endregion
    }
}

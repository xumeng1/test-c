// [Copyright]
//
// Bookham Test Engine
// Mod_Characterise
//
// Bookham.TestSolution.TestModules/Mod_CharacteriseGui
// 
// Author: paul.annetts
// Design: TODO

namespace Bookham.TestSolution.TestModules
{
    partial class Mod_CharacteriseGui
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LabelInfo = new System.Windows.Forms.Label();
            this.myprogressBar = new System.Windows.Forms.ProgressBar();
            this.lablePercentage = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // LabelInfo
            // 
            this.LabelInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelInfo.Location = new System.Drawing.Point(57, 38);
            this.LabelInfo.Name = "LabelInfo";
            this.LabelInfo.Size = new System.Drawing.Size(325, 43);
            this.LabelInfo.TabIndex = 0;
            this.LabelInfo.Text = "Characterization";
            // 
            // myprogressBar
            // 
            this.myprogressBar.Location = new System.Drawing.Point(75, 148);
            this.myprogressBar.Name = "myprogressBar";
            this.myprogressBar.Size = new System.Drawing.Size(396, 45);
            this.myprogressBar.TabIndex = 1;
            // 
            // lablePercentage
            // 
            this.lablePercentage.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lablePercentage.Location = new System.Drawing.Point(493, 148);
            this.lablePercentage.Name = "lablePercentage";
            this.lablePercentage.Size = new System.Drawing.Size(64, 21);
            this.lablePercentage.TabIndex = 2;
            // 
            // Mod_CharacteriseGui
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lablePercentage);
            this.Controls.Add(this.myprogressBar);
            this.Controls.Add(this.LabelInfo);
            this.Name = "Mod_CharacteriseGui";
            this.Size = new System.Drawing.Size(840, 309);
            this.MsgReceived += new Bookham.TestEngine.Framework.Messages.ManagedCtlBase.MsgReceivedDlg(this.ModuleGui_MsgReceived);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label LabelInfo;
        private System.Windows.Forms.ProgressBar myprogressBar;
        private System.Windows.Forms.Label lablePercentage;
    }
}

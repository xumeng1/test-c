// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// IlmzMzItuEstimate_ZD.cs
//
// Author: Paul.Annetts, Mark Fullalove 2007
// ILMZ GB version : Mark Fullalove 2011
// Design: [Reference design documentation]

using System;
using System.Text;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Framework.Limits;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestSolution.IlmzCommonData;
using Bookham.TestSolution.ILMZFinalData;
using Bookham.TestSolution.IlmzCommonUtils;
using Bookham.TestLibrary.Algorithms;
using Bookham.ToolKit.Mz;
using Bookham.TestLibrary.Utilities;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// TCMZ MZ ITU Channel characterisation
    /// 
    /// Zero chirp, differential sweep
    /// 
    /// </summary>
    public class IlmzMzItuEstimate_ZD : ITestModule
    {
        #region Private Members

        double vcmMinLimit;
        double vcmMaxLimit;
        double vpiMinLimit;
        double vpiMaxLimit;
        double vquadMinLimit;
        double vquadMaxLimit;
        double commonModeCurrent_mA;
        int MZSourceMeasureDelay_ms = 5;
        double vpiCorrectionUpperAdjustDelta = 0.06;
        double vpiCorrectionLowerAdjustDelta = 0.06;

        double maxArmBias_v;
        /// <summary>the name of a configuration parameter</summary>
        public const string keyVpiCorrectionUpperAdjustDelta = "VpiCorrectionUpperAdjustDelta";

        /// <summary>the name of a configuration parameter</summary>
        public const string keyVpiCorrectionLowerAdjustDelta = "VpiCorrectionLowerAdjustDelta";

        // add this variant to record sweep file
        string MzFileDirectory;
        private IlMzDriverUtils mzDriverUtils;
        string dutSerialNbr;
        private double MZTapBias_V;
        private double tapInline_V;
        private DsdbrChannelData[] ituChannels = null;
        int NumberPoints;
        private double Vmax; // Used to help calculate new Vcm value.
        ITestEngine ituEstimateEngine;
		private List<MzSweepDataItuChannel> mzSweepData = null;
        private List<MzItuAtVcmData> VcmResultData = new List<MzItuAtVcmData>();

        private bool ArmSourceByAsic = true;
        private bool isUseNegSlopeImb = false;

        #endregion

        #region ITestModule Members
        /// <summary>
        /// 
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="userType"></param>
        /// <param name="configData"></param>
        /// <param name="instruments"></param>
        /// <param name="chassis"></param>
        /// <param name="calData"></param>
        /// <param name="previousTestData"></param>
        /// <returns></returns>
        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, 
            DatumList configData, InstrumentCollection instruments,
            ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {
            ituEstimateEngine = engine;
            engine.GuiShow();
            engine.GuiToFront();

            ituChannels = (DsdbrChannelData[])previousTestData.ReadReference("ItuChannels");
            mzSweepData = (List<MzSweepDataItuChannel>)previousTestData.ReadReference("MzSweepData");

            List<MzSweepDataItuChannel> lowFreqData = new List<MzSweepDataItuChannel>(2);
            List<MzSweepDataItuChannel> highFreqData = new List<MzSweepDataItuChannel>(2);
            lowFreqData.Add(mzSweepData[0]);
            lowFreqData.Add(mzSweepData[1]);
            highFreqData.Add(mzSweepData[1]);
            if (mzSweepData.Count > 2)
                highFreqData.Add(mzSweepData[2]);            

            // get list of DSDBR channels
            DsdbrChannelData[] dsdbrItuChannels = (DsdbrChannelData[])previousTestData.ReadReference("ItuChannelsIn");
            int nbrChannels = dsdbrItuChannels.Length;
            IlmzChannelInit[] ilmzChannelInit = new IlmzChannelInit[nbrChannels];
           
            double calfreqOffset_GHz = configData.ReadDouble("CalOffset");
            double highTemperature = configData.ReadDouble("HighTemp");
            double lowTemperature = configData.ReadDouble("LowTemp");
            double freqLow_GHz = configData.ReadDouble("FreqLow_GHz");
            double freqHigh_GHz = configData.ReadDouble("FreqHigh_GHz");
            double freqSpace_GHz = configData.ReadDouble("FreqSpace_GHz");
            Vmax = configData.ReadDouble("Vmax");

            this.isUseNegSlopeImb = configData.ReadBool("IsUseNegSlopeImbInZC");
            
            this.dutSerialNbr = configData.ReadString("DutSerialNbr");
            
            MzFileDirectory = configData.ReadString("MzFileDirectory");
            this.NumberPoints = configData.ReadSint32("NumberOfPoints");
            IlMzInstruments mzInstrs = (IlMzInstruments)configData.ReadReference("MzInstruments");
            mzDriverUtils = new IlMzDriverUtils(mzInstrs);
            this.MZTapBias_V = configData.ReadDouble("MzTapBias_V");

            if (mzInstrs.InlineTapOnline)
                this.tapInline_V = configData.ReadDouble("MZInlineTapBias_V");

            Specification spec = (Specification)configData.ReadReference("Specification");
            this.vcmMinLimit = Convert.ToDouble(spec.GetParamLimit("TC_MZ_VCM_CAL_LIMIT_MIN").LowLimit.ValueToString());
            this.vcmMaxLimit = Convert.ToDouble(spec.GetParamLimit("TC_MZ_VCM_CAL_LIMIT_MAX").HighLimit.ValueToString());
            this.vpiMinLimit = Convert.ToDouble(spec.GetParamLimit("TC_MZ_VPI_CAL_LIMIT_MIN").LowLimit.ValueToString());
            this.vpiMaxLimit = Convert.ToDouble(spec.GetParamLimit("TC_MZ_VPI_CAL_LIMIT_MAX").HighLimit.ValueToString());
            this.vquadMinLimit = Convert.ToDouble(spec.GetParamLimit("TC_MZ_QUAD_V_LIMIT_MIN").LowLimit.ValueToString());
            this.vquadMaxLimit = Convert.ToDouble(spec.GetParamLimit("TC_MZ_QUAD_V_LIMIT_MAX").HighLimit.ValueToString());
            //this.maxArmBias_v = Convert.ToDouble(spec.GetParamLimit("TC_MZ_MAX_BIAS_V").LowLimit.ValueToString());  // -5V
            this.maxArmBias_v = Bookham.ToolKit.Mz.IlMzDriverUtils.MaxModBias_V;

            this.commonModeCurrent_mA = configData.ReadDouble("MzCtrlINominal_mA");
            this.MZSourceMeasureDelay_ms = configData.ReadSint32("MZSourceMeasureDelay_ms");
            if (configData.IsPresent(keyVpiCorrectionUpperAdjustDelta))              
                this.vpiCorrectionUpperAdjustDelta = configData.ReadDouble(keyVpiCorrectionUpperAdjustDelta); // 0.06 for Default

            if (configData.IsPresent(keyVpiCorrectionLowerAdjustDelta))
                this.vpiCorrectionLowerAdjustDelta = configData.ReadDouble(keyVpiCorrectionLowerAdjustDelta); // 0.06 for Default

            // Check MZ data has > 2 channels
            if (mzSweepData.Count < 2)
            {
                throw new ArgumentException("MZ channel characterisation data is incorrect." +
                    " Need > 2 channels measurements, have " + mzSweepData.Count.ToString());
            }
            // Chech that the channels are at different ITU points
            if (mzSweepData[0].ItuChannelIndex == mzSweepData[1].ItuChannelIndex || mzSweepData[0].ItuChannelIndex == mzSweepData[mzSweepData.Count - 1].ItuChannelIndex)
            {
                throw new ArgumentException("MZ channel characterisation data is incorrect." +
                    " Need measurements at different ITU channels, have two at " + mzSweepData[0].ItuChannelIndex.ToString() +
                    " and " + mzSweepData[mzSweepData.Count - 1].ItuChannelIndex.ToString());
            }

            // Calculate new Vpi and Vcm at the extreme channels
            /* bool flagVpiVquadOK;
            MzChannelData[] mzChannelChar = CalculateMZatNewVcmVpi(engine, mzSweepData, out flagVpiVquadOK); //CalculateMZatNewVcm(mzSweepData);

            if (!flagVpiVquadOK)
            {
                string NewVcmResultsFile = Util_GenerateFileName.GenWithTimestamp(
               configData.ReadString("MzFileDirectory"), "NewVcmResultsFile",
               configData.ReadString("DUTSerialNbr"), "csv");

               recordVcmVpi(
                   mzChannelChar, 
                   NewVcmResultsFile,
                   previousTestData.ReadString("MzSweepDataResultsFile"));

                engine.ShowContinueUserQuery("MZ channel characterisation data is incorrect. " +
                    "the Vpi or VQuad is out of limit");
            }*/

            // Create a lookup for calculated Vpi at each channel   //Echo remed above block, we use optimised VcmVpi which come from Characterise module, no need to recalculate.
            Hashtable VpiAtItuChannel = new Hashtable();

            for (int chanIndx = 0; chanIndx < nbrChannels; chanIndx++)
            {
                IlmzChannelInit initChan = new IlmzChannelInit();
                initChan.Dsdbr = dsdbrItuChannels[chanIndx];

                int ituChannelIndex = dsdbrItuChannels[chanIndx].ItuChannelIndex;

                // Interpolate using the appropriate dataset
                InterpolatedMzData interpolatedMzData;
                if (ituChannelIndex < mzSweepData[1].ItuChannelIndex)
                {
                    interpolatedMzData = InterpolateMZData(ituChannelIndex, lowFreqData);
                    initChan.Mz = interpolatedMzData.mzData;
                }
                else
                {
                    interpolatedMzData = InterpolateMZData(ituChannelIndex, highFreqData);
                    initChan.Mz = interpolatedMzData.mzData;
                }

                // Add calculated Vpi to hashtable so that we can store the values later
                // Note: The key is the ITU channel index
                if (!VpiAtItuChannel.ContainsKey(ituChannelIndex))
                    VpiAtItuChannel.Add(ituChannelIndex, interpolatedMzData.Vpi_V);

                initChan.Dsdbr.ItuFreq_GHz -= calfreqOffset_GHz;
                ilmzChannelInit[chanIndx] = initChan;
            }

            // Build ILMZ channels
            IlmzChannels ilmzItuChannels = new IlmzChannels(ilmzChannelInit, spec, lowTemperature, highTemperature);

            // Populate calculated Vpi figures.
            for (double chanFreq_GHz = freqLow_GHz; chanFreq_GHz <= freqHigh_GHz; chanFreq_GHz += freqSpace_GHz)
            {
                ILMZChannel[] channelsAtItu = ilmzItuChannels.GetChannelsAtItu(chanFreq_GHz);
                foreach (ILMZChannel channelOption in channelsAtItu)
                {
                    int ituChannelIndex = channelOption.MidTempData.GetValueSint32(EnumTosaParam.ItuChannelIndex);
                    if (VpiAtItuChannel[ituChannelIndex] != null)   // Allow testing of a reduced channel list
                    {
                        double Vpi = (double)VpiAtItuChannel[ituChannelIndex];
                        double Vcm = (channelOption.MidTempData.MzSettings.LeftArmMod_Quad_V
                            + channelOption.MidTempData.MzSettings.RightArmMod_Quad_V) / 2;

                        channelOption.MidTempData.SetValueDouble(EnumTosaParam.MzVpiCal_V, Vpi);
                        channelOption.MidTempData.SetValueDouble(EnumTosaParam.MzVcmCal_V, Vcm);
                    }
                }
            }

            // Check limits to get MZ status at new Vcm
            //double[] channelsToCheck = new double[2];// channels for checking MZ status at new Vcm
            //channelsToCheck[0] = dsdbrItuChannels[0].ItuFreq_GHz - configData.ReadDouble("CalOffset");
            //channelsToCheck[1] = dsdbrItuChannels[dsdbrItuChannels.Length - 1].ItuFreq_GHz - configData.ReadDouble("CalOffset");
			
			double[] channelsToCheck = new double[ilmzItuChannels.AllOptions.Length];// channels for checking MZ status at new Vcm
            for (int ii = 0; ii < ilmzItuChannels.AllOptions.Length; ii++)
            {
                channelsToCheck[ii] = dsdbrItuChannels[ii].ItuFreq_GHz - configData.ReadDouble("CalOffset");
            }
			
            string mzAtNewVcmResultsFile = Util_GenerateFileName.GenWithTimestamp(
                configData.ReadString("MzFileDirectory"), "MzSweepDataAtNewVcmResultsFile", configData.ReadString("DUTSerialNbr"), "csv");

            bool mzAtNewVcmPass = GetMzAtNewVcmStatus(ilmzItuChannels, channelsToCheck, mzAtNewVcmResultsFile, previousTestData.ReadString("MzSweepDataResultsFile"));

            // return data
            DatumList returnData = new DatumList();
            returnData.AddSint32("MzAtNewVcmPass", mzAtNewVcmPass ? 1 : 0);
            returnData.AddFileLink("MzAtNewVcmResultsFile", mzAtNewVcmResultsFile);
            returnData.AddReference("TcmzItuChannels", ilmzItuChannels);
            return returnData;
        }
        /// <summary>
        /// 
        /// </summary>
        public Type UserControl
        {
            get { return (typeof(IlmzMzItuEstimateGui)); }
        }

        #endregion


        #region Private methods
        /// <summary>
        /// Get Mz status at new Vcm
        /// </summary>
        /// <param name="ilmzItuChannels"></param>
        /// <param name="channelsToCheck"></param>
        /// <param name="resultsFile"></param>
        /// <param name="fileToCopy">Put MzSweepDataResultsFile into the same file, product engineers' requirement</param>
        /// <returns>True if Mz status at new Vcm is passed</returns>
        private bool GetMzAtNewVcmStatus(IlmzChannels ilmzItuChannels, double[] channelsToCheck, string resultsFile, string fileToCopy)
        {
            // get mz data at channels to check
            MzSweepDataItuChannel[] mzDataAtItuChannels = new MzSweepDataItuChannel[channelsToCheck.Length];
            for (int ii = 0; ii < channelsToCheck.Length; ii++)
            {
                double freq_GHz = channelsToCheck[ii];
                MzSweepDataItuChannel mzDataAtItuChannel = new MzSweepDataItuChannel();

                ILMZChannel[] chans = ilmzItuChannels.GetChannelsAtItu(freq_GHz);

                if (chans == null || chans.Length == 0)
                {
                    // should not come to here...
                    throw new ArgumentException(string.Format("No tcmz channel at frequency {0} !", freq_GHz));
                }

                ILMZChannel chan = chans[0];// Any option is ok.
                mzDataAtItuChannel.ItuChannelIndex = chan.MidTempData.GetValueSint32(EnumTosaParam.ItuChannelIndex);
                MzItuAtVcmData mzDataAtNewVcm = new MzItuAtVcmData(chan.MidTempData.GetValueDouble(EnumTosaParam.MzVcmCal_V));

                mzDataAtNewVcm.SetValueDouble(MzItuAtVcmMeasurements.IImbQuadL_mA, chan.MidTempData.GetValueDouble(EnumTosaParam.MzLeftArmImb_mA));
                mzDataAtNewVcm.SetValueDouble(MzItuAtVcmMeasurements.IImbQuadR_mA, chan.MidTempData.GetValueDouble(EnumTosaParam.MzRightArmImb_mA));
                mzDataAtNewVcm.SetValueDouble(MzItuAtVcmMeasurements.VminL_V, chan.MidTempData.GetValueDouble(EnumTosaParam.MzLeftArmModMinima_V));
                mzDataAtNewVcm.SetValueDouble(MzItuAtVcmMeasurements.VminR_V, chan.MidTempData.GetValueDouble(EnumTosaParam.MzRightArmModMinima_V));
                mzDataAtNewVcm.SetValueDouble(MzItuAtVcmMeasurements.VpeakL_V, chan.MidTempData.GetValueDouble(EnumTosaParam.MzLeftArmModPeak_V));
                mzDataAtNewVcm.SetValueDouble(MzItuAtVcmMeasurements.VpeakR_V, chan.MidTempData.GetValueDouble(EnumTosaParam.MzRightArmModPeak_V));
                mzDataAtNewVcm.SetValueDouble(MzItuAtVcmMeasurements.VquadL_V, chan.MidTempData.GetValueDouble(EnumTosaParam.MzLeftArmModQuad_V));
                mzDataAtNewVcm.SetValueDouble(MzItuAtVcmMeasurements.VquadR_V, chan.MidTempData.GetValueDouble(EnumTosaParam.MzRightArmModQuad_V));
                mzDataAtNewVcm.SetValueDouble(MzItuAtVcmMeasurements.Vpi_V, chan.MidTempData.GetValueDouble(EnumTosaParam.MzVpiCal_V));
                // we have no ER here...
                mzDataAtItuChannel.VcmData.Add(mzDataAtNewVcm);

                mzDataAtItuChannels[ii] = mzDataAtItuChannel;
                VcmResultData.Add(mzDataAtNewVcm);//echo new added 2011-07-22
            }

            #region Check limits to get Mz Status at new Vcm and record data into file
            bool mzAtNewVcmStatusPassed = true;
            int channel_index = 0;
            double channel = 0;
            using (StreamWriter writer = new StreamWriter(resultsFile))
            {
                // Copy text from MzSweepDataResultsFile
                StreamReader reader = new StreamReader(fileToCopy);
                writer.Write(reader.ReadToEnd());
                writer.WriteLine();

                // file header
                string defaultFileHeader = "ITUChannel,Frequency_GHz,Vcm_V,Status,IImbL_mA,IImbR_mA,VquadL_V,VquadR_V,VpeakL_V,VpeakR_V,VminL_V,VminR_V,Vpi_V, ER_dB";
                StringBuilder fileHeader = new StringBuilder();
                bool fileHeaderOK = false;
                fileHeader.Append("ITUChannel,Frequency_GHz,Vcm_V,Status");

                // file contents
                List<string> fileContents = new List<string>();
                int index = 0;
                foreach (MzItuAtVcmData vcmData in VcmResultData)
                {
                    channel = channelsToCheck[index];
                    if(channel!=channelsToCheck[Math.Max(0,index-1)])
                    channel_index++;
                    index++;
                        // fail check
                        bool vpiStatus = true;

                        if (vcmData.OverallStatus == PassFail.Fail)
                        {
                            mzAtNewVcmStatusPassed = false;
                            //Echoxl.wang added this code to continue testing even though vpi marginally fails
                            double newVpi = vcmData.GetValueDouble(MzItuAtVcmMeasurements.Vpi_V);
                            if ((newVpi <= this.vpiMaxLimit + 0.1) || (newVpi>=this.vpiMinLimit - 0.1))
                            {
                                mzAtNewVcmStatusPassed = true;
                                vpiStatus = false;
                            }
                        }

                        // each line
                        StringBuilder aLine = new StringBuilder();
                        aLine.Append(channel_index);
                        aLine.Append(",");
                        aLine.Append(channel);
                        aLine.Append(",");
                        aLine.Append(vcmData.Vcm_V);
                        aLine.Append(",");

                        // record the fact that Vpi was just out of limits
                        if (vpiStatus)
                            aLine.Append(vcmData.OverallStatus.ToString());
                        else
                            aLine.Append("True(extend limit range)");

                        // for each parameter...
                        foreach (string nameAsStr in Enum.GetNames(typeof(MzItuAtVcmMeasurements)))
                        {
                            MzItuAtVcmMeasurements parameter = (MzItuAtVcmMeasurements)Enum.Parse(typeof(MzItuAtVcmMeasurements), nameAsStr);

                            // build file header
                            if (!fileHeaderOK)
                            {
                                fileHeader.Append(",");
                                fileHeader.Append(nameAsStr);
                                fileHeader.Append("[");
                                fileHeader.Append(vcmData.GetMeasuredDataLowLimit(parameter));
                                fileHeader.Append(";");
                                fileHeader.Append(vcmData.GetMeasuredDataHighLimit(parameter));
                                fileHeader.Append("]");
                            }

                            // build the line
                            aLine.Append(",");
                            if (vcmData.IsTested(parameter))
                            {
                                aLine.Append(vcmData.GetValueDouble(parameter));
                            }
                            else
                            {
                                double e = 0;
                                if (channel == 0)
                                {
                                    if (mzSweepData[0].VcmData[0].IsTested(parameter))
                                    {
                                        e = mzSweepData[0].VcmData[0].GetValueDouble(parameter);
                                    }
                                }
                                else
                                {
                                    if (mzSweepData[1].VcmData[0].IsTested(parameter))
                                    {
                                        e = mzSweepData[1].VcmData[0].GetValueDouble(parameter);
                                    }
                                }
                                aLine.Append(e);
                            }
                        }//end of foreach(string nameAsStr in Enum.GetNames

                        if (!fileHeaderOK)
                            fileHeaderOK = true;

                        fileContents.Add(aLine.ToString());
                    }//end of foreach (MzItuAtVcmData vcmData in VcmResultData)

                // write file header to file
                if (!fileHeaderOK)
                    writer.WriteLine(defaultFileHeader);
                else
                    writer.WriteLine(fileHeader.ToString());

                // writer file contents to file
                foreach (string row in fileContents)
                {
                    writer.WriteLine(row);
                }

            } // write data into file
            #endregion

            return mzAtNewVcmStatusPassed;
        }

        /// <summary>
        /// Calculate the differential Voffset given the individual L and R bias voltages
        /// </summary>
        /// <param name="mzItuAtVcmData">A structure containing MZ sweep data</param>
        /// <returns>Differential voffset</returns>
        private double CalculateVoffset(MzItuAtVcmData mzItuAtVcmData)
        {
            double left_V = mzItuAtVcmData.GetValueDouble(MzItuAtVcmMeasurements.VquadL_V);
            double right_V = mzItuAtVcmData.GetValueDouble(MzItuAtVcmMeasurements.VquadR_V);

            return left_V - right_V;
        }

        private double CalculateVoffset_Min(MzItuAtVcmData mzItuAtVcmData)
        {
            double left_V = mzItuAtVcmData.GetValueDouble(MzItuAtVcmMeasurements.VminL_V);
            double right_V = mzItuAtVcmData.GetValueDouble(MzItuAtVcmMeasurements.VminR_V);

            return left_V - right_V;
        }
        /// <summary>
        /// Interpolates MZ setup at an ITU channel.
        /// Returns the setup point for peak power and an estimated Vpi
        /// Note that if the estimated Vpi fails limits when stored to the channel data structure
        /// this will cause the channel to be marked as a failure and skipped.
        /// </summary>
        /// <param name="ituChannelIndex">ITU channel index</param>
        /// <param name="mzChannelData">MZ channel characteristics</param>
        /// <returns>Structure containing MZ setup and calculated Vpi</returns>
        private InterpolatedMzData InterpolateMZData(int ituChannelIndex, MzChannelData[] mzChannelData)
        {
            InterpolatedMzData interpolatedMzData = new InterpolatedMzData();
            interpolatedMzData.mzData = new MzData();

            // Estimate Vpi
            double Vpi_gradient = mzChannelData[0].ItuChannelIndex - mzChannelData[1].ItuChannelIndex / (mzChannelData[0].Vpi_V - mzChannelData[1].Vpi_V);
            interpolatedMzData.Vpi_V = EstimateXAtY.Calculate(mzChannelData[0].Vpi_V, mzChannelData[0].ItuChannelIndex, Vpi_gradient, ituChannelIndex);

            // Calculate new voffset
            double Voffset_gradient = mzChannelData[0].ItuChannelIndex - mzChannelData[1].ItuChannelIndex / (mzChannelData[0].Voffset_V - mzChannelData[1].Voffset_V);
            double Voffset = EstimateXAtY.Calculate(mzChannelData[0].Voffset_V, mzChannelData[0].ItuChannelIndex, Voffset_gradient, ituChannelIndex);

            // Interpolate Vcm.
            double Vcm_gradient = mzChannelData[0].ItuChannelIndex - mzChannelData[1].ItuChannelIndex / (mzChannelData[0].Vcm_V - mzChannelData[1].Vcm_V);
            double Vcm = EstimateXAtY.Calculate(mzChannelData[0].Vcm_V, mzChannelData[0].ItuChannelIndex, Vcm_gradient, ituChannelIndex);

            // The peak power will be Vpi / 2 from the imbalance point.
            double vPeak = Voffset + (interpolatedMzData.Vpi_V / 2);

            // The min power will be Vpi / 2 from the imbalance point.
            double vTrough = Voffset - (interpolatedMzData.Vpi_V / 2);

            // Interpolate Iimb.
            double Iimb_gradient = mzChannelData[0].ItuChannelIndex - mzChannelData[1].ItuChannelIndex / (mzChannelData[0].Icm_mA - mzChannelData[1].Icm_mA);
            double Iimb = EstimateXAtY.Calculate(mzChannelData[0].Icm_mA, mzChannelData[0].ItuChannelIndex, Iimb_gradient, ituChannelIndex);

            // Convert differential to single-ended drive

            // Imbalance point
            MzAnalysisWrapper.CalcDifferentialArmValues(Voffset, Vcm,
                out interpolatedMzData.mzData.LeftArmMod_Quad_V, out interpolatedMzData.mzData.RightArmMod_Quad_V);

            
            // Peak
            MzAnalysisWrapper.CalcDifferentialArmValues(vPeak, Vcm,
                out interpolatedMzData.mzData.LeftArmMod_Peak_V, out interpolatedMzData.mzData.RightArmMod_Peak_V);

            // Trough
            MzAnalysisWrapper.CalcDifferentialArmValues(Voffset, Vcm,
                out interpolatedMzData.mzData.LeftArmMod_Min_V, out interpolatedMzData.mzData.RightArmMod_Min_V);

            // Iimb
            MzAnalysisWrapper.CalcDifferentialArmValues(Iimb, commonModeCurrent_mA,
                out interpolatedMzData.mzData.LeftArmImb_mA, out interpolatedMzData.mzData.RightArmImb_mA);

            return interpolatedMzData;
        }


        /// <summary>
        /// Interpolates MZ setup at an ITU channel.
        /// Returns the setup point for peak power and an estimated Vpi
        /// Note that if the estimated Vpi fails limits when stored to the channel data structure
        /// this will cause the channel to be marked as a failure and skipped.
        /// </summary>
        /// <param name="ituChannelIndex">ITU channel index</param>
        /// <param name="mzChannelData">MZ channel characteristics</param>
        /// <returns>Structure containing MZ setup and calculated Vpi</returns>
        private InterpolatedMzData InterpolateMZData(int ituChannelIndex, List<MzSweepDataItuChannel> mzSweepData)
        {
            InterpolatedMzData interpolatedMzData = new InterpolatedMzData();
            interpolatedMzData.mzData = new MzData();

            int Data1LastIndex = mzSweepData[0].VcmData.Count - 1;
            int Data2LastIndex = mzSweepData[1].VcmData.Count - 1;

            // Estimate Vpi
            double Vpi_gradient = (mzSweepData[0].ItuChannelIndex - mzSweepData[1].ItuChannelIndex) / (mzSweepData[0].VcmData[Data1LastIndex].GetValueDouble(MzItuAtVcmMeasurements.Vpi_V) - mzSweepData[1].VcmData[Data2LastIndex].GetValueDouble(MzItuAtVcmMeasurements.Vpi_V));
            interpolatedMzData.Vpi_V = EstimateXAtY.Calculate(mzSweepData[0].VcmData[Data1LastIndex].GetValueDouble(MzItuAtVcmMeasurements.Vpi_V), mzSweepData[0].ItuChannelIndex, Vpi_gradient, ituChannelIndex);

            //// Calculate new voffset
            //double Voffset_gradient = (mzChannelData[0].ItuChannelIndex - mzChannelData[1].ItuChannelIndex) / (mzChannelData[0].Voffset_V - mzChannelData[1].Voffset_V);
            //double Voffset = EstimateXAtY.Calculate(mzChannelData[0].Voffset_V, mzChannelData[0].ItuChannelIndex, Voffset_gradient, ituChannelIndex);

            // Interpolate Vcm.
            double Vcm_gradient = (mzSweepData[0].ItuChannelIndex - mzSweepData[1].ItuChannelIndex) / (mzSweepData[0].VcmData[Data1LastIndex].Vcm_V - mzSweepData[1].VcmData[Data2LastIndex].Vcm_V);
            double Vcm = EstimateXAtY.Calculate(mzSweepData[0].VcmData[Data1LastIndex].Vcm_V, mzSweepData[0].ItuChannelIndex, Vcm_gradient, ituChannelIndex);

            //  // Interpolate Iimb.
            //double Iimb_gradient = mzChannelData[0].ItuChannelIndex - mzChannelData[1].ItuChannelIndex / (mzChannelData[0].Icm_mA - mzChannelData[1].Icm_mA);
            //double Iimb = EstimateXAtY.Calculate(mzChannelData[0].Icm_mA, mzChannelData[0].ItuChannelIndex, Iimb_gradient, ituChannelIndex);

            double LeftArmMod_Quad_V_gradient = (mzSweepData[0].ItuChannelIndex - mzSweepData[1].ItuChannelIndex) / (mzSweepData[0].VcmData[Data1LastIndex].GetValueDouble(MzItuAtVcmMeasurements.VquadL_V) - mzSweepData[1].VcmData[Data2LastIndex].GetValueDouble(MzItuAtVcmMeasurements.VquadL_V));
            interpolatedMzData.mzData.LeftArmMod_Quad_V = EstimateXAtY.Calculate(mzSweepData[0].VcmData[Data1LastIndex].GetValueDouble(MzItuAtVcmMeasurements.VquadL_V), mzSweepData[0].ItuChannelIndex, LeftArmMod_Quad_V_gradient, ituChannelIndex);

            double RightArmMod_Quad_V_gradient = (mzSweepData[0].ItuChannelIndex - mzSweepData[1].ItuChannelIndex) / (mzSweepData[0].VcmData[Data1LastIndex].GetValueDouble(MzItuAtVcmMeasurements.VquadR_V) - mzSweepData[1].VcmData[Data2LastIndex].GetValueDouble(MzItuAtVcmMeasurements.VquadR_V));
            interpolatedMzData.mzData.RightArmMod_Quad_V = EstimateXAtY.Calculate(mzSweepData[0].VcmData[Data1LastIndex].GetValueDouble(MzItuAtVcmMeasurements.VquadR_V), mzSweepData[0].ItuChannelIndex, RightArmMod_Quad_V_gradient, ituChannelIndex);

            double LeftArmMod_Peak_V_gradient = (mzSweepData[0].ItuChannelIndex - mzSweepData[1].ItuChannelIndex) / (mzSweepData[0].VcmData[Data1LastIndex].GetValueDouble(MzItuAtVcmMeasurements.VpeakL_V) - mzSweepData[1].VcmData[Data2LastIndex].GetValueDouble(MzItuAtVcmMeasurements.VpeakL_V));
            interpolatedMzData.mzData.LeftArmMod_Peak_V = EstimateXAtY.Calculate(mzSweepData[0].VcmData[Data1LastIndex].GetValueDouble(MzItuAtVcmMeasurements.VpeakL_V), mzSweepData[0].ItuChannelIndex, LeftArmMod_Peak_V_gradient, ituChannelIndex);


            double RightArmMod_Peak_V_gradient = (mzSweepData[0].ItuChannelIndex - mzSweepData[1].ItuChannelIndex) / (mzSweepData[0].VcmData[Data1LastIndex].GetValueDouble(MzItuAtVcmMeasurements.VpeakR_V) - mzSweepData[1].VcmData[Data2LastIndex].GetValueDouble(MzItuAtVcmMeasurements.VpeakR_V));
            interpolatedMzData.mzData.RightArmMod_Peak_V = EstimateXAtY.Calculate(mzSweepData[0].VcmData[Data1LastIndex].GetValueDouble(MzItuAtVcmMeasurements.VpeakR_V), mzSweepData[0].ItuChannelIndex, RightArmMod_Peak_V_gradient, ituChannelIndex);

            double LeftArmMod_Min_V_gradient = (mzSweepData[0].ItuChannelIndex - mzSweepData[1].ItuChannelIndex) / (mzSweepData[0].VcmData[Data1LastIndex].GetValueDouble(MzItuAtVcmMeasurements.VminL_V) - mzSweepData[1].VcmData[Data2LastIndex].GetValueDouble(MzItuAtVcmMeasurements.VminL_V));
            interpolatedMzData.mzData.LeftArmMod_Min_V = EstimateXAtY.Calculate(mzSweepData[0].VcmData[Data1LastIndex].GetValueDouble(MzItuAtVcmMeasurements.VminL_V), mzSweepData[0].ItuChannelIndex, LeftArmMod_Min_V_gradient, ituChannelIndex);

            double RightArmMod_Min_V_gradient = (mzSweepData[0].ItuChannelIndex - mzSweepData[1].ItuChannelIndex) / (mzSweepData[0].VcmData[Data1LastIndex].GetValueDouble(MzItuAtVcmMeasurements.VminR_V) - mzSweepData[1].VcmData[Data2LastIndex].GetValueDouble(MzItuAtVcmMeasurements.VminR_V));
            interpolatedMzData.mzData.RightArmMod_Min_V = EstimateXAtY.Calculate(mzSweepData[0].VcmData[Data1LastIndex].GetValueDouble(MzItuAtVcmMeasurements.VminR_V), mzSweepData[0].ItuChannelIndex, RightArmMod_Min_V_gradient, ituChannelIndex);

            double LeftArmImb_mA_gradient = (mzSweepData[0].ItuChannelIndex - mzSweepData[1].ItuChannelIndex) / (mzSweepData[0].VcmData[Data1LastIndex].GetValueDouble(MzItuAtVcmMeasurements.IImbQuadL_mA) - mzSweepData[1].VcmData[Data2LastIndex].GetValueDouble(MzItuAtVcmMeasurements.IImbQuadL_mA));
            interpolatedMzData.mzData.LeftArmImb_mA = EstimateXAtY.Calculate(mzSweepData[0].VcmData[Data1LastIndex].GetValueDouble(MzItuAtVcmMeasurements.IImbQuadL_mA), mzSweepData[0].ItuChannelIndex, LeftArmImb_mA_gradient, ituChannelIndex);

            double RightArmImb_mA_gradient = (mzSweepData[0].ItuChannelIndex - mzSweepData[1].ItuChannelIndex) / (mzSweepData[0].VcmData[Data1LastIndex].GetValueDouble(MzItuAtVcmMeasurements.IImbQuadR_mA) - mzSweepData[1].VcmData[Data2LastIndex].GetValueDouble(MzItuAtVcmMeasurements.IImbQuadR_mA));
            interpolatedMzData.mzData.RightArmImb_mA = EstimateXAtY.Calculate(mzSweepData[0].VcmData[Data1LastIndex].GetValueDouble(MzItuAtVcmMeasurements.IImbQuadR_mA), mzSweepData[0].ItuChannelIndex, RightArmImb_mA_gradient, ituChannelIndex);

            //// Imbalance point
            //MzAnalysisWrapper.CalcDifferentialArmValues(vQuard, Vcm,
            //    out interpolatedMzData.mzData.LeftArmMod_Quad_V, out interpolatedMzData.mzData.RightArmMod_Quad_V);

            //// Peak
            //MzAnalysisWrapper.CalcDifferentialArmValues(vPeak, Vcm,
            //    out interpolatedMzData.mzData.LeftArmMod_Peak_V, out interpolatedMzData.mzData.RightArmMod_Peak_V);

            //// Trough
            //MzAnalysisWrapper.CalcDifferentialArmValues(Voffset, Vcm,
            //    out interpolatedMzData.mzData.LeftArmMod_Min_V, out interpolatedMzData.mzData.RightArmMod_Min_V);

            //// Iimb
            //MzAnalysisWrapper.CalcDifferentialArmValues(Iimb, commonModeCurrent_mA,
            //    out interpolatedMzData.mzData.LeftArmImb_mA, out interpolatedMzData.mzData.RightArmImb_mA);

            return interpolatedMzData;
        }

        

        /// <summary>
        /// For each of the two extreme channels this will recalculate Vcm ,
        /// then recalculate the Vpi and Voffset for start and stop channel.
        /// The resulting data can then be used to interpolate Voffset and Vpi by channel.
        /// Values exceeding a Vcm limit are clamped at that limit.
        /// </summary>
        /// <param name="testEngine"></param>
        /// <param name="mzSweepData">A list containing MZ sweep data 2 data points at 2 channels</param>
        /// <param name="vcmOkFlag"></param>
        /// <returns>An array containing recalculated VPI and VCM at two ITU channels</returns>
        private MzChannelData[] CalculateMZatNewVcmVpi(ITestEngine testEngine, 
            List<MzSweepDataItuChannel> mzSweepData , out bool vcmOkFlag)
        {
            // Create return container
            MzChannelData[] mzChannelDataList = new MzChannelData[2];

            // Process each channel
            int channel = 0;
            bool flagVcmVpiFail = false;
            
            int[] chansToMeas = new int[] { 0, ituChannels.Length - 1 };
            int dsdbrChannelIndex = 0;
            foreach (MzSweepDataItuChannel mzChannel in mzSweepData)
            {
                if (mzChannel.VcmData.Count < 2)
                {
                    throw new ArgumentException("MZ channel characterisation data is incorrect."+
                        " Need 2 Vcm measurements, have " + mzChannel.VcmData.Count.ToString());
                }
                if (mzChannel.VcmData[0].Vcm_V == mzChannel.VcmData[1].Vcm_V)
                {
                    throw new ArgumentException("MZ channel characterisation data is incorrect."+
                        " Need 2 Vcm measurements at different values of Vcm, have two at " +
                        mzChannel.VcmData[0].Vcm_V.ToString());
                }

                MzRawData mzRawData_Vcm1 = new MzRawData();
                MzRawData mzRawData_Vcm2 = new MzRawData();

                MzChannelData mzChannelData = new MzChannelData();
                mzChannelData.ItuChannelIndex = mzChannel.ItuChannelIndex;
                //
                // Get data
                //
                // Data from Vcm point 1
                mzRawData_Vcm1.Vpi_V = mzChannel.VcmData[0].GetValueDouble(MzItuAtVcmMeasurements.Vpi_V);
                mzRawData_Vcm1.AbsVimb_V = Math.Abs(CalculateVoffset(mzChannel.VcmData[0]));
                mzRawData_Vcm1.Vimb_V = CalculateVoffset(mzChannel.VcmData[0]);               

                mzRawData_Vcm1.Vcm_V = mzChannel.VcmData[0].Vcm_V;
                mzRawData_Vcm1.Iimb_mA = (mzChannel.VcmData[0].GetValueDouble(MzItuAtVcmMeasurements.IImbQuadL_mA  )
                    - mzChannel.VcmData[0].GetValueDouble(MzItuAtVcmMeasurements.IImbQuadR_mA ));

                // Data from Vcm point 2
                mzRawData_Vcm2.Vpi_V = mzChannel.VcmData[1].GetValueDouble(MzItuAtVcmMeasurements.Vpi_V);
                mzRawData_Vcm2.AbsVimb_V = Math.Abs(CalculateVoffset(mzChannel.VcmData[1]));
                mzRawData_Vcm2.Vimb_V = CalculateVoffset(mzChannel.VcmData[1]);

                mzRawData_Vcm2.Vcm_V = mzChannel.VcmData[1].Vcm_V;
                mzRawData_Vcm2.Iimb_mA = (mzChannel.VcmData[1].GetValueDouble(MzItuAtVcmMeasurements.IImbQuadL_mA )
                    - mzChannel.VcmData[1].GetValueDouble(MzItuAtVcmMeasurements.IImbQuadR_mA ));


                //
                // Calculate straight line fits for Vpi , Voffset & Iimb
                //
                // Vpi vs Vcm
                mzChannelData.VpiFit.Gradient = (mzRawData_Vcm1.Vpi_V - mzRawData_Vcm2.Vpi_V) / (mzRawData_Vcm1.Vcm_V - mzRawData_Vcm2.Vcm_V);
                // Offset required is Y at X=0, so swap X & Y and invert gradient
                mzChannelData.VpiFit.Offset = EstimateXAtY.Calculate(mzRawData_Vcm1.Vpi_V, mzRawData_Vcm1.Vcm_V, 1 / mzChannelData.VpiFit.Gradient, 0);

                // Voffset vs Vcm
                mzChannelData.VoffsetFit.Gradient = (mzRawData_Vcm1.Vimb_V - mzRawData_Vcm2.Vimb_V) / (mzRawData_Vcm1.Vcm_V - mzRawData_Vcm2.Vcm_V);
                // Offset required is Y at X=0, so swap X & Y and invert gradient
                mzChannelData.VoffsetFit.Offset = EstimateXAtY.Calculate(mzRawData_Vcm1.Vimb_V, mzRawData_Vcm1.Vcm_V, 1 / mzChannelData.VoffsetFit.Gradient, 0);

                // AbsVoffset vs Vcm
                mzChannelData.AbsVoffsetFit.Gradient = (mzRawData_Vcm1.AbsVimb_V - mzRawData_Vcm2.AbsVimb_V) / (mzRawData_Vcm1.Vcm_V - mzRawData_Vcm2.Vcm_V);
                // Offset required is Y at X=0, so swap X & Y and invert gradient
                mzChannelData.AbsVoffsetFit.Offset = EstimateXAtY.Calculate(mzRawData_Vcm1.AbsVimb_V, mzRawData_Vcm1.Vcm_V, 1 / mzChannelData.AbsVoffsetFit.Gradient, 0);

                // Iimb vs Vcm
                mzChannelData.IimbFit.Gradient = (mzRawData_Vcm1.Iimb_mA - mzRawData_Vcm2.Iimb_mA) / (mzRawData_Vcm1.Vcm_V - mzRawData_Vcm2.Vcm_V);
                // Offset required is Y at X=0, so swap X & Y and invert gradient
                mzChannelData.IimbFit.Offset = EstimateXAtY.Calculate(mzRawData_Vcm1.Iimb_mA, mzRawData_Vcm1.Vcm_V, 1 / mzChannelData.IimbFit.Gradient, 0);
                               
                /* Calculate new Vcm
                 *             
                 *  Corrected formula from Dave :
                 *  Vcm = (- 0.5 - Cvpi/4 - Coff/2) / (1 + Mvpi/4 + Moff/2) 
                 */
                //mzChannelData.Vcm_V =
                //    (Vmax - (mzChannelData.VpiFit.Offset / 4) - (mzChannelData.AbsVoffsetFit.Offset / 2))
                //    / (1 + (mzChannelData.VpiFit.Gradient / 4) + (mzChannelData.AbsVoffsetFit.Gradient / 2));
                
                // Pick Vcm to target a constant Vpi close to the max limit
                mzChannelData.Vcm_V = (vpiMaxLimit - this.vpiCorrectionUpperAdjustDelta - mzChannelData.VpiFit.Offset) / mzChannelData.VpiFit.Gradient;

                // To avoid driving the device too hard we should clamp the calculated Vcm against the limits
                mzChannelData.Vcm_V = Math.Max(mzChannelData.Vcm_V, this.vcmMinLimit);
                mzChannelData.Vcm_V = Math.Min(mzChannelData.Vcm_V, this.vcmMaxLimit);

                // do a differential sweep on Mod to get real VPi & so on 

                // get imb values from previous diff_LV sweep. The value is the same for both sets of Vcm data.
                double iImbLeft = mzChannel.VcmData[0].GetValueDouble(MzItuAtVcmMeasurements.IImbQuadL_mA);                   
                double iImbRight=mzChannel.VcmData[0].GetValueDouble(MzItuAtVcmMeasurements.IImbQuadR_mA );
                
                // Get SOA current. The values are the same in VcmData[0] and VcmData[1]
                double iSOA_mA = mzChannel.VcmData[0].GetValueDouble(MzItuAtVcmMeasurements.Isoa_mA);

                DsdbrChannelData dsdbrChannelData = ituChannels[chansToMeas[dsdbrChannelIndex]];
                if (!testEngine.IsSimulation)
                {
                    DsdbrUtils.SetDsdbrCurrents_mA(dsdbrChannelData.Setup);

                    // use the power levelled ISOA from MZ Characterise because
                    // for ILMZ, different SOA current will cause different VCM
                    DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic.ISoa_mA = (float)iSOA_mA;

                    mzDriverUtils.MzInstrs.PowerMeter.Wavelength_nm = Measurements.ReadWavelength_nm();
                    double power_mW = mzDriverUtils.MzInstrs.PowerMeter.ReadPower();
                }

                // Measure at predicted settings. This should minimise the difference between Vpi_Cal and measured Vpi on each channel
                MzItuAtVcmData vcmData = measureMzAtVcmLevel(
                    testEngine, 
                    mzChannel.ItuChannelIndex, 
                    mzChannelData.Vcm_V,
                    iImbLeft, 
                    iImbRight,
                    iSOA_mA);
                
				VcmResultData.Add(vcmData);//echo new added 2011-07-22
                // Update Vpi with the real value
                mzChannelData.Vpi_V = vcmData.GetValueDouble(MzItuAtVcmMeasurements.Vpi_V);                
                                              
                //  ... and Voffset at new Vcm
                mzChannelData.Voffset_V = vcmData.GetValueDouble(MzItuAtVcmMeasurements.VquadL_V) -  vcmData.GetValueDouble(MzItuAtVcmMeasurements.VquadR_V);
               
#warning if Icm[0] = Icm[1] - remove this
                // ... and Icm at new Vcm
                mzChannelData.Icm_mA = mzChannelData.IimbFit.Gradient * mzChannelData.Vcm_V + mzChannelData.IimbFit.Offset;

                // Add this channel's data to the collection
                mzChannelDataList[channel++] = mzChannelData;
                dsdbrChannelIndex++;
            }
            channel++;//echo new add 2011-07-22
            vcmOkFlag = !flagVcmVpiFail;

            // A final check that all is well.
            if (mzChannelDataList[0].ItuChannelIndex == mzChannelDataList[1].ItuChannelIndex)
            {
                throw new ArgumentException("MZ channel characterisation data is incorrect. Need measurements at different ITU channels, have two at " + mzChannelDataList[0].ItuChannelIndex.ToString());
            }
            
            return mzChannelDataList;
        }
                       
        /// <summary>
        /// Characterise MZ at a given common mode bias voltage
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="ituChannelIndex"></param>
        /// <param name="vcm_V"></param>
        /// <param name="iImbLeft"></param>
        /// <param name="iImbRight"></param>
        /// <returns></returns>
        private MzItuAtVcmData measureMzAtVcmLevel(ITestEngine engine, int ituChannelIndex, 
            double vcm_V, double iImbLeft, double iImbRight, double iSoa_mA )
        {
            // find the common mode voltage we are using          
            // initialise the data object (including the limits)
            MzItuAtVcmData vcmData = new MzItuAtVcmData(vcm_V, iSoa_mA);

            // store imbalance biases
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.IImbQuadL_mA, iImbLeft);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.IImbQuadR_mA, iImbRight);
            
            // If Vcm is high we need to restrict the max bias level on either arm to its max limit by just performing the middle section of the sweep.
            double startBias = 2 * vcm_V;
            double stopBias = 0;
            if (startBias < maxArmBias_v)
            {
                double offset = startBias - maxArmBias_v;
                startBias = maxArmBias_v;
                stopBias = offset;
            }

            // diff voltage sweep - assume VCM is negative. Apply calculated IQuad to the 
            // imbalance electrodes
            engine.SendToGui("Running Differential Modulator sweep on channel " + ituChannelIndex.ToString() + " from 0V to " + vcm_V.ToString("0.0") + "V");
            bool dataOk = true;
            double minLevel_dBm = -60;
            ILMZSweepResult diffVsweep = new ILMZSweepResult();
           
            do
            {
                if (!engine.IsSimulation)
                {
                    diffVsweep = mzDriverUtils.ModArm_DifferentialSweepByTrigger(
                        vcmData.GetValueDouble(MzItuAtVcmMeasurements.IImbQuadL_mA) / 1000,
                        vcmData.GetValueDouble(MzItuAtVcmMeasurements.IImbQuadR_mA) / 1000,
                        startBias,
                        stopBias,
                        NumberPoints,
                        MZSourceMeasureDelay_ms,
                        tapInline_V,
                        MZTapBias_V,
                        ArmSourceByAsic);
                }
                #region offline debug
               /* else
                {
                    // Load plot data for offline debugging
                    engine.SendToGui("Loading debug data from file");

                    diffVsweep.Type = SweepType.DifferentialVoltage;
                    diffVsweep.SrcMeter = SourceMeter.LeftModArm;

                    string fileName = string.Format("{0}\\Offline\\Debug_MZCR_CH{1}_DILV_ItuEst.csv", MzFileDirectory, ituChannelIndex);

                    string searchPattern = string.Format("MZCR_CH{0}_DILV_ItuEst*.csv", ituChannelIndex);
                    string[] mzFiles = Directory.GetFiles(MzFileDirectory, searchPattern, SearchOption.AllDirectories);
                    if (mzFiles.Length > 0)
                        fileName = mzFiles[0];

                    MzRawData rawData = MzSweepFileWriter.ReadPlotData(fileName);

                    for (int column = 0; column < rawData.plotData.Length; column++)
                    {
                        ILMZSweepDataType sweepType = (ILMZSweepDataType)Enum.Parse(typeof(ILMZSweepDataType), rawData.header[column]);
                        double[] plotArray = (double[])rawData.plotData[column].ToArray(typeof(double));
                        diffVsweep.SweepData.Add(sweepType, plotArray);
                    }
                }*/
                #endregion

                string sweepFileName;
                string fileNamePrefix = string.Format("MZCR_CH{0}_DILV_ItuEst", ituChannelIndex);
                recordSweepData(diffVsweep, fileNamePrefix, out sweepFileName);
                
                // If overrange run again
                if (MzAnalysisWrapper.CheckForOverrange(diffVsweep.SweepData[ILMZSweepDataType.FibrePower_mW]))
                {
                    dataOk = false;
                    mzDriverUtils.MzInstrs.PowerMeter.Range = Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.AutoRange;
                    //if (double.IsNaN(mzDriverUtils.MzInstrs.PowerMeter.Range)) // auto range?
                    //    mzDriverUtils.MzInstrs.PowerMeter.Range = 1;
                    //else
                    //    mzDriverUtils.MzInstrs.PowerMeter.Range = mzDriverUtils.MzInstrs.PowerMeter.Range * 10;
                }
                else
                {
                    // if underrange fix the data and continue
                    dataOk = true;
                    if (MzAnalysisWrapper.CheckForUnderrange(diffVsweep.SweepData[ILMZSweepDataType.FibrePower_mW]))
                    {
                        diffVsweep.SweepData[ILMZSweepDataType.FibrePower_mW] =
                            MzAnalysisWrapper.FixUnderRangeData(diffVsweep.SweepData[ILMZSweepDataType.FibrePower_mW]
                            , minLevel_dBm);
                    }
                }
            } while (!dataOk);

            // Display data
            engine.SendToGui(diffVsweep);

            // write data to file
            string diffVsweepFileNameStem = string.Format("MzDiffLV_CH{0}_ItuEst", ituChannelIndex);

            vcmData.DiffLVSweepFile = Util_GenerateFileName.GenWithTimestamp
                (MzFileDirectory, diffVsweepFileNameStem, dutSerialNbr, "csv");

            MzSweepFileWriter.WriteSweepData(vcmData.DiffLVSweepFile, diffVsweep,
                new ILMZSweepDataType[] 
                {ILMZSweepDataType.LeftArmModBias_V,
                 ILMZSweepDataType.RightArmModBias_V,
                 ILMZSweepDataType.FibrePower_mW,
                 ILMZSweepDataType.TapComplementary_mA});

            // analyse the data.
            //
            // The first point needs to be close to zero. After that 
            // search for quadrature close to initialVcmOffset 
            // to help ensure that we pick the same slope each time.

            MzAnalysisWrapper.MzAnalysisResults mzAnlyDiffV;

            if (this.isUseNegSlopeImb)
            {
                mzAnlyDiffV = MzAnalysisWrapper.Differential_NegChirp_DifferentialSweep(diffVsweep, vcm_V, 0);
            }
            else
            {
                mzAnlyDiffV = MzAnalysisWrapper.ZeroChirp_DifferentialSweep(diffVsweep, vcm_V);
            }
            
            // Add a nice arrow to point at the quadrature point
            NPlot.PointD marker = new NPlot.PointD(mzAnlyDiffV.Imb_SrcL - mzAnlyDiffV.Imb_SrcR, mzAnlyDiffV.PowerAtMax_dBm - 3);
            engine.SendToGui(marker);

            // Add some information to help technicians & engineers
            NPlot.TextItem markerText = new NPlot.TextItem(new NPlot.PointD(marker.X, mzAnlyDiffV.PowerAtMin_dBm + 5), "Vpi = " + mzAnlyDiffV.Vpi.ToString("0.0"));
            engine.SendToGui(markerText);

            NPlot.VerticalLine verticalLine = new NPlot.VerticalLine(mzAnlyDiffV.Max_SrcL - mzAnlyDiffV.Max_SrcR, System.Drawing.Color.IndianRed);
            engine.SendToGui(verticalLine);

            verticalLine = new NPlot.VerticalLine(mzAnlyDiffV.Min_SrcL - mzAnlyDiffV.Min_SrcR, System.Drawing.Color.IndianRed);
            engine.SendToGui(verticalLine);

            if (System.Diagnostics.Debugger.IsAttached)
                System.Threading.Thread.Sleep(2000);

            vcmData.SetValueDouble(MzItuAtVcmMeasurements.VquadL_V, mzAnlyDiffV.Imb_SrcL);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.VquadR_V, mzAnlyDiffV.Imb_SrcR);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.VminL_V, mzAnlyDiffV.Min_SrcL);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.VminR_V, mzAnlyDiffV.Min_SrcR);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.VpeakL_V, mzAnlyDiffV.Max_SrcL);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.VpeakR_V, mzAnlyDiffV.Max_SrcR);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.Vpi_V, mzAnlyDiffV.Vpi);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.ER_dB, mzAnlyDiffV.ExtinctionRatio_dB);
            vcmData.SetValueDouble(MzItuAtVcmMeasurements.Isoa_mA, iSoa_mA);

            // Choose subsequent slopes to be close to the first selected slope,
            // looking half way between 0 and the imbalance point should work.
            // This should help to choose the same slope in all cases
            //if (!initialOffsetSelected)
            //    initialVcmOffset = (vcmData.GetValueDouble(MzItuAtVcmMeasurements.VminL_V)
            //        - vcmData.GetValueDouble(MzItuAtVcmMeasurements.VminR_V)) / 2;
            //initialOffsetSelected = true;

            return vcmData;
        }
        ///// <summary>
        /// write arm sweep data to file 
        /// </summary>
        /// <param name="sweepData"> sweep data</param>
        /// <param name="file_prefix"> file name prefix </param>
        /// <param name="sweepFileName"> file name that the sweep data save as </param>
        private void recordSweepData(ILMZSweepResult sweepData, string file_prefix, out string sweepFileName)
        {
            Dictionary<ILMZSweepDataType, double[]> sweepDataListTemp = sweepData.SweepData;
            List<string> dataNameList = new List<string>();
            List<double[]> sweepDataList = new List<double[]>();
            foreach (ILMZSweepDataType dataName in sweepDataListTemp.Keys)
            {
                dataNameList.Add(dataName.ToString());
                sweepDataList.Add(sweepDataListTemp[dataName]);
            }

            double[][] sweepDataArray = sweepDataList.ToArray();
            string[] dataNameArray = dataNameList.ToArray();

            if ((sweepDataArray.Length > 0) && (dataNameArray[0].Length > 0))
            { }
            else
            {
                sweepFileName = "";
                return;
            }

            string mzSweepDataResultsFile = Util_GenerateFileName.GenWithTimestamp
                (MzFileDirectory, file_prefix, dutSerialNbr, "csv");

            using (StreamWriter writer = new StreamWriter(mzSweepDataResultsFile))
            {
                // file header
                StringBuilder fileHeader = new StringBuilder();

                for (int count = 0; count < dataNameArray.Length; count++)
                {
                    if (count != 0) fileHeader.Append(",");
                    fileHeader.Append(dataNameArray[count]);
                }

                // file contents
                List<string> fileContents = new List<string>();
                for (int irow = 0; irow < sweepDataArray[0].Length; irow++)
                {
                    StringBuilder aLine = new StringBuilder();
                    for (int icol = 0; icol < dataNameArray.Length; icol++)
                    {
                        if (icol != 0) aLine.Append(",");
                        aLine.Append(sweepDataArray[icol][irow].ToString());
                    }
                    fileContents.Add(aLine.ToString());
                }

                // write file header to file
                writer.WriteLine(fileHeader.ToString());

                // writer file contents to file
                foreach (string row in fileContents)
                {
                    writer.WriteLine(row);
                }
            }
            sweepFileName = mzSweepDataResultsFile;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="channelData"></param>
        /// <param name="resultsFile"></param>
        /// <param name="fileToCopy"></param>
        private void recordVcmVpi(MzChannelData[] channelData,string resultsFile, string fileToCopy)
        {
            using (StreamWriter writer = new StreamWriter(resultsFile))
            {
                // Copy text from MzSweepDataResultsFile
                StreamReader reader = new StreamReader(fileToCopy);
                writer.Write(reader.ReadToEnd());
                writer.WriteLine();

                // file header
                if (channelData != null)
                {
                    writer.WriteLine(channelData[0].MenberToString());
                    foreach (MzChannelData data in channelData)
                    {
                        writer.WriteLine(data.DataToString());
                    }
                }
            }

        }
        #endregion
        /// <summary>
        /// 
        /// </summary>
        private struct InterpolatedMzData
        {
            public MzData mzData;
            public double Vpi_V;
        }

        /// <summary>
        /// A container for MZ data which may be useful for interpolation across ITU channels
        /// </summary>
        private struct MzChannelData
        {
            /// <summary>
            /// The ITU channel index
            /// </summary>
            public int ItuChannelIndex;
            /// <summary>
            /// VPI calculated at this channel
            /// </summary>
            public double Vpi_V;
            /// <summary>
            /// Differential Iimbalance calculated at this channel
            /// </summary>      
            public double Icm_mA;
            /// <summary>
            /// Voffset calculated at this channel
            /// </summary>
            public double Voffset_V;
            /// <summary>
            /// recalculated VCM
            /// </summary>
            public double Vcm_V;
            /// <summary>
            /// Gradient and offset for VPI vs VCM
            /// </summary>
            public StraightLineFit VpiFit;
            /// <summary>
            /// Gradient and offset for Voffset vs VCM 
            /// </summary>
            public StraightLineFit VoffsetFit;
            /// <summary>
            /// Gradient and offset for Absolute value of Voffset vs VCM 
            /// </summary>            
            public StraightLineFit AbsVoffsetFit;
            /// <summary>
            /// Gradient and offset for Iimb vs VCM 
            /// </summary>
            public StraightLineFit IimbFit;

            public string MenberToString()
            {
                return "ItuChannelIndex, Vpi_V, Icm_mA, Voffset_V, Vcm_V, VpiFit, VoffsetFit, AbsVoffsetFit, IimbFit";
            }
            public string DataToString()
            {
                string temp = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8}",
                    ItuChannelIndex, Vpi_V, Icm_mA, Voffset_V, Vcm_V, VpiFit, VoffsetFit, AbsVoffsetFit, IimbFit);
                return temp;
            }
        }

        /// <summary>
        /// Container for measured MZ data
        /// </summary>
        private struct MzRawData
        {
            /// <summary>
            /// Vpi
            /// </summary>
            public double Vpi_V;
            /// <summary>
            /// Vcm
            /// </summary>
            public double Vcm_V;
            /// <summary>
            /// Voffset
            /// </summary>
            public double Vimb_V;
            /// <summary>
            /// Absolute value of Voffset
            /// </summary>
            public double AbsVimb_V;
            /// <summary>
            /// Iimbalance
            /// </summary>
            public double Iimb_mA;
        }

        /// <summary>
        /// Container to hold the characteristics of a straight line
        /// </summary>
        private struct StraightLineFit
        {
            /// <summary>
            /// Gradient
            /// </summary>
            public double Gradient;
            /// <summary>
            /// Intercept at X=0
            /// </summary>
            public double Offset;
        }
    }
}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// IlmzInitialise.cs
//
// Author: Tony.Foster, 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestSolution.IlmzCommonData;
using Bookham.Toolkit.CloseGrid;//jack.Zhang
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.Instruments;
using Bookham.TestLibrary.Utilities;
using Bookham.ToolKit.Mz;
using Bookham.TestSolution.IlmzCommonUtils;
using Bookham.TestSolution.IlmzCommonInstrs;
using Microsoft.Win32;
using System.IO;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// TCMZ Initialise Module 
    /// </summary>
    public class IlmzInitialise : ITestModule
    {
        #region ITestModule Members

        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, 
            DatumList configData, InstrumentCollection instruments, 
            ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {
            engine.SendStatusMsg(" Running TCMZ Initialise ");
            // Return datum list
            DatumList retData = new DatumList();

            // Get testset hardware info.
            DsdbrDriveInstruments dsdbrInstrs =configData.IsPresent("DsdbrInstrsGroup") ?
                (DsdbrDriveInstruments)configData.ReadEnum("DsdbrInstrsGroup") :
                DsdbrDriveInstruments.FCU2AsicInstrument;

            // Get config data.
            TcmzInitialise_Config mzConfigData = new TcmzInitialise_Config(configData);
           //IlMzInstruments mzInstrs = (IlMzInstruments)configData.ReadReference("MzInstruments");
            string dutSerialNbr = configData.ReadString("DutSerialNbr");
            string MzFileDirectory = configData.ReadString("MzFileDirectory");
            FCU2AsicInstruments fcu2AsicInstrs = (FCU2AsicInstruments)configData.ReadReference("FCU2AsicInstruments");


            #region General checks
            // Check channel configuration            
            int numChans = configData.GetDatumSint32("NumChans").Value;
            double freqLow_GHz = configData.ReadDouble("FreqLow_GHz");
            double freqHigh_GHz = configData.ReadDouble("FreqHigh_GHz");
            double freqSpace_GHz = configData.ReadDouble("FreqSpace_GHz");

            if ((freqHigh_GHz - freqLow_GHz) / freqSpace_GHz + 1 != numChans)
            {
                engine.RaiseNonParamFail(0, "Invalid channel configuration: Low freq = '" + freqLow_GHz +
                    "', High freq = '" + freqHigh_GHz + "', channel space = '" + freqSpace_GHz +
                    "', number of channels = '" + numChans + "'.");
            }

            #endregion

            #region Close Grid/Fcu2Asic / FCU
            if ( dsdbrInstrs == DsdbrDriveInstruments .FCU2AsicInstrument)
            {
                #region FCU2Asic

                // Initialise FCU
                //FCU2AsicInstruments fcu2AsicInstrs = (FCU2AsicInstruments)configData.ReadReference("FCU2AsicInstruments");

                // Alice.Huang 2010-02-01
                // i don't realy know what the fcu's serail operation should be done by 
                // FCU2ASIC with ASIC instruction. 
                // i guest it is to disable all ouput & set channel's digital pot 
                fcu2AsicInstrs.Fcu2Asic.IGain_mA = 0;
                if (fcu2AsicInstrs .Fcu2Asic .GetFrontPairNumber()!=0)
                {
                    fcu2AsicInstrs.Fcu2Asic.IEven_mA = 0;
                    fcu2AsicInstrs.Fcu2Asic.IOdd_mA = 0;
                }
                fcu2AsicInstrs.Fcu2Asic.IRear_mA = 0;
                fcu2AsicInstrs.Fcu2Asic.IPhase_mA = 0;
                fcu2AsicInstrs.Fcu2Asic.ISoa_mA = 0;

                fcu2AsicInstrs .Fcu2Asic .TecEnable = Inst_Fcu2Asic.OnOff .off ;
                //fcu2AsicInstrs .Fcu2Asic .
                //fcu2AsicInstrs.FullbandControlUnit.SetControlRegLaserPSUOn(false);
                //fcu2AsicInstrs.FullbandControlUnit.SetControlRegLaserTecOn(false);
                //fcu2AsicInstrs.FullbandControlUnit.SetControlRegLockerOn(false);
                //fcu2AsicInstrs.FullbandControlUnit.OIFLaserEnable(false);
                // Need to set the range for the Tx Locker ADC
                fcu2AsicInstrs .Fcu2Asic .SetAdcChannelCalibrate(
                    Inst_Fcu2Asic.EnumAdcCalibrate.transmit ,
                    Convert.ToInt32(fcu2AsicInstrs.FCUCalibrationData.TxCoarsePot_CalFactor));
                fcu2AsicInstrs .Fcu2Asic .SetAdcChannelCalibrate(
                    Inst_Fcu2Asic .EnumAdcCalibrate .reflect ,
                    Convert .ToInt32 (fcu2AsicInstrs .FCUCalibrationData .RxCoarsePot_CalFactor ));

                fcu2AsicInstrs .Fcu2Asic .SetAdcChannelCalibrate (
                    Inst_Fcu2Asic.EnumAdcCalibrate .lockfine ,
                    Convert.ToInt32(fcu2AsicInstrs.FCUCalibrationData.TxFinePot_CalFactor));
                //fcu2AsicInstrs.FullbandControlUnit.SetTxCoarsePot(Convert.ToInt32(fcu2AsicInstrs.FCUCalibrationData.TxCoarsePot_CalFactor));
                //fcu2AsicInstrs.FullbandControlUnit.SetTxFinePot(Convert.ToInt32(fcu2AsicInstrs.FCUCalibrationData.TxFinePot_CalFactor));

                // SOA check continuity  
                // --- Can't be done with FCU2Asic, because we can't read voltage on SOA
                

                #endregion
            }
            else
            {
                #region FCU

                // Initialise FCU
                FCUInstruments fcuInstrs = (FCUInstruments)configData.ReadReference("FcuInstruments");
                fcuInstrs.FullbandControlUnit.SetControlRegLaserPSUOn(false);
                fcuInstrs.FullbandControlUnit.SetControlRegLaserTecOn(false);
                fcuInstrs.FullbandControlUnit.SetControlRegLockerOn(false);
                fcuInstrs.FullbandControlUnit.OIFLaserEnable(false);
                // Need to set the range for the Tx Locker ADC
                fcuInstrs.FullbandControlUnit.SetTxCoarsePot(Convert.ToInt32(fcuInstrs.FCUCalibrationData.TxCoarsePot_CalFactor));
                fcuInstrs.FullbandControlUnit.SetTxFinePot(Convert.ToInt32(fcuInstrs.FCUCalibrationData.TxFinePot_CalFactor));

                // SOA check
                const double connTestCurr_mA = 30;
                const int connTestSettleTime_mS = 20;
                const double maxVolThreshold_V = 2.0;
                const double minVolThreshold_V = 0.5;

                if (fcuInstrs.SoaCurrentSource.DriverName.Contains("Ke24xx"))
                {
                    fcuInstrs.SoaCurrentSource.HardwareData["4wireSense"] = "true";
                }
                fcuInstrs.SoaCurrentSource.SetDefaultState();
                fcuInstrs.SoaCurrentSource.CurrentSetPoint_amp = 0.0;
                fcuInstrs.SoaCurrentSource.VoltageComplianceSetPoint_Volt = maxVolThreshold_V;
                fcuInstrs.SoaCurrentSource.CurrentSetPoint_amp = connTestCurr_mA / 1000;
                fcuInstrs.SoaCurrentSource.OutputEnabled = true;
                System.Threading.Thread.Sleep(connTestSettleTime_mS);
                double testVol_V = fcuInstrs.SoaCurrentSource.VoltageActual_Volt;
                fcuInstrs.SoaCurrentSource.CurrentSetPoint_amp = 0.0;
                fcuInstrs.SoaCurrentSource.OutputEnabled = false;
                if (minVolThreshold_V != maxVolThreshold_V && (testVol_V < minVolThreshold_V || testVol_V > maxVolThreshold_V))
                {
                    engine.RaiseNonParamFail(0, "Continuity fail on SOA section (" + testVol_V.ToString("0.000") + "V)");
                }

                #endregion
            }

            #endregion

            #region MZ bias controllers

            // Get common config values
            double contBiasSet_A = mzConfigData.MzContinuityTest_SetBias_mA / 1000;
            double contTapBiasSet_A = mzConfigData.MzTapContinuityTest_SetBias_mA / 1000;
            double contMinV_volt = mzConfigData.MzContinuityTest_MinMeasV_volt;
            double contMaxV_volt = mzConfigData.MzContinuityTest_MaxMeasV_volt;
            double contMeasV_volt;

            
            /*// Left arm mod
            fcu2AsicInstrs.Fcu2Asic.ILeftModBias_mA = (float)contBiasSet_A * 1000;

            contMeasV_volt = fcu2AsicInstrs.Fcu2Asic.VLeftModBias_Volt;

            fcu2AsicInstrs.Fcu2Asic.ILeftModBias_mA = (float)0;
            if (contMinV_volt != contMaxV_volt && (contMeasV_volt < contMinV_volt || contMeasV_volt > contMaxV_volt))
            {
                engine.RaiseNonParamFail(0, "Continuity fail on MZ 'Left Arm Modulation' (" + contMeasV_volt.ToString("0.000") + "V)");
            }

            // Right arm mod
            fcu2AsicInstrs.Fcu2Asic.IRightModBias_mA=(float)contBiasSet_A*1000;

            contMeasV_volt = fcu2AsicInstrs.Fcu2Asic.VRightModBias_Volt;
            fcu2AsicInstrs.Fcu2Asic.IRightModBias_mA = (float)0;
            if (contMinV_volt != contMaxV_volt && (contMeasV_volt < contMinV_volt || contMeasV_volt > contMaxV_volt))
            {
                engine.RaiseNonParamFail(0, "Continuity fail on MZ 'Right Arm Modulation' (" + contMeasV_volt.ToString("0.000") + "V)");
            }

            // Power tap ( complementary )
            fcu2AsicInstrs.Fcu2Asic.ICtap_mA = (float)contTapBiasSet_A * 1000;

            contMeasV_volt = fcu2AsicInstrs.Fcu2Asic.VCtap_Volt;
            fcu2AsicInstrs.Fcu2Asic.ICtap_mA = (float)0;
            if (contMinV_volt != contMaxV_volt && (contMeasV_volt < contMinV_volt || contMeasV_volt > contMaxV_volt))
            {
                engine.RaiseNonParamFail(0, "Continuity fail on MZ 'Complementary Power Tap' (" + contMeasV_volt.ToString("0.000") + "V)");
            }*/

            #endregion

            // DatumList return
            return  null;
        }

        public Type UserControl
        {
            // No GUI for this module
            get { return null; }
        }

        #endregion
           

    }
}

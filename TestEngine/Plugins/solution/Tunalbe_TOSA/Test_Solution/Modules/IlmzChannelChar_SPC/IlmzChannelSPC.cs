// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// IlmzChannelSPC.cs
//
// Author: Tony.Foster, Mark Fullalove 2007
// Design: [Reference design documentation]
//

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Framework.Limits;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestLibrary.Utilities;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.Algorithms;
using Bookham.Toolkit.CloseGrid;//jack.Zhang
using Bookham.ToolKit.Mz;
using Bookham.TestSolution.IlmzCommonData;
using Bookham.TestSolution.ILMZFinalData;
using Bookham.TestSolution.IlmzCommonInstrs;
using Bookham.TestSolution.IlmzCommonUtils;
using Bookham.TestSolution.TestModules;
using Bookham.TestLibrary.Instruments;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// TCMZ Initialise Module 
    /// </summary>
    public class IlmzChannelSPC : ITestModule
    {
        #region ITestModule Members
        ITestEngine spcEngine = null;
        FailModeCheck failModeCheck;

        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, DatumList configData, InstrumentCollection instruments,
            ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {
            // Initialise GUI
            spcEngine = engine;
            engine.GuiShow();
            engine.GuiToFront();
            engine.SendToGui(new GuiTitleMessage("Hitt low cost SPC starting....."));
            engine.SendToGui(new GuiTextMessage("Initialising", 0));

            failModeCheck = configData.ReadReference("FailModeCheck") as FailModeCheck;

            // Initialise Baseline Channels
            baseLineChannels = (BaseLineData[])configData.ReadReference("BaseLineChannels");

            // read dsdbr temperature when closeloop

            Dsdbr_temp_cl = configData.ReadDouble("Tcase_temp_cl");//echo new added 2010-10-14

            // Initialise Configs
            Specification mainSpec = (Specification)configData.ReadReference("Specification");
            //dutSerialNbr = configData.ReadString("DutSerialNbr");
            //opmRange = configData.ReadDouble("OpmRangeInit_mW");
            string testStage = configData.ReadString("TestStage");
            string baseReferenceFile = configData.ReadString("BaseLineReferenceFile");//
            double[] powerOffsetArray = configData.ReadDoubleArray("PowerOffsetArray");
            string FreqBandType = configData.ReadString("BandType");

            // Initialise Instruments
            mzInstrs = (IlMzInstruments)configData.ReadReference("MzInstruments");
            //PowerHead = (IInstType_OpticalPowerMeter)instruments["PowerHead"];

            if (instruments.Contains("OSA"))
            {
                this.Osa = (IInstType_OSA)instruments["OSA"];
                SMSRMeasurement.OSA = this.Osa;
                SMSRMeasurement.SMSRSpan_nm = configData.ReadDouble("SMSRMeasure_Span_nm");
                SMSRMeasurement.SMSRRBW_nm = configData.ReadDouble("SMSRMeasure_RBW_nm");
                SMSRMeasurement.SMSROSASensitivity_dBm = configData.ReadDouble("SMSRMeasure_OSASensitivity_dBm");
                SMSRMeasurement.SMSRNumPts = configData.ReadSint32("SMSRMeasure_NumPts");
            }

            dsdbrTec = (IInstType_TecController)instruments["DsdbrTec"];

            // fcuSource = (InstType_ElectricalSource)instruments["FcuSource"];
            if (instruments.Contains("MzTec")) mzTec = (IInstType_TecController)instruments["MzTec"];
            //mzInstrs.PowerMeter.Range = opmRange;//echo remed
            Inst_Ke2510 Optical_switch = (Inst_Ke2510)instruments["Optical_switch"];
            DsdbrTuning.OpticalSwitch = Optical_switch;
            DsdbrUtils.opticalSwitch = Optical_switch;
            IInstType_DigitalIO OutLineCtap = Optical_switch.GetDigiIoLine(IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine);//line 3
            this.MZSourceMeasureDelay_ms = configData.ReadSint32("MZSourceMeasureDelay_ms");
            double minLevel_dBm = 0;

            DatumList retData = new DatumList();

            List<string[]> referenceLines = null;
            
            if (!testStage.Contains("base"))
            {
                using (CsvReader cr = new CsvReader())
                {
                    referenceLines = cr.ReadFile(baseReferenceFile);
                }
            }

            //// do spc base or spc testing

            for (int chanNo = 0; chanNo < baseLineChannels.Length; chanNo++)
            {
                #region spc base or spec test for each channel

                //DsdbrUtils.Fcu2AsicInstrumentGroup.AsicVccSource.OutputEnabled = false;
                //DsdbrUtils.Fcu2AsicInstrumentGroup.AsicVeeSource.OutputEnabled = false;
                //// DsdbrUtils.Fcu2AsicInstrumentGroup.FcuSource.OutputEnabled = false;
                //System.Threading.Thread.Sleep(500);
                //DsdbrUtils.Fcu2AsicInstrumentGroup.AsicVccSource.OutputEnabled = true;
                //DsdbrUtils.Fcu2AsicInstrumentGroup.AsicVeeSource.OutputEnabled = true;
                //// DsdbrUtils.Fcu2AsicInstrumentGroup.FcuSource.OutputEnabled = true;
                //System.Threading.Thread.Sleep(1000);

                mzInstrs.FCU2Asic.IimbLeft_mA = 0;
                mzInstrs.FCU2Asic.IimbRight_mA = 0;

                string deltaPrefix = "_delta";
                string datumPrefix = string.Empty;

                switch (chanNo)
                {
                    case 0:
                        datumPrefix = "_fst";
                        break;
                    case 1:
                        datumPrefix = "_mid";
                        break;
                    case 2:
                        datumPrefix = "_lst";
                        break;
                    default:
                        this.failModeCheck.RaiseError_Mod_NonParamFail(engine, "invalid base line template data, it must be 3 channels!", FailModeCategory_Enum.SW);
                        break;
                }

                double referenceSMSR = 0;

                if (this.Osa != null && referenceLines != null)
                {
                    referenceSMSR = FindValueByName(engine, referenceLines, "SMSR_CH" + datumPrefix.ToUpper());
                }

                System.Threading.Thread.Sleep(100);

                //power up laser 
                #region power up laser
                engine.SendToGui(new GuiTextMessage("power up laser at channel " + chanNo.ToString(), 0));
                DsdbrChannelSetup baseLineCurrentGroup = baseLineChannels[chanNo].Dsdbr.Setup;
                DsdbrUtils.SetDsdbrCurrents_mA(baseLineCurrentGroup);
                System.Threading.Thread.Sleep(1000);
                Measurements.FrequencyWithoutMeter = baseLineChannels[chanNo].Dsdbr.ItuFreq_GHz;
                #endregion

                //Do LV sweep. and set mz arm current


                #region Diff LV sweep
                #region DiffLV sweep setting
                double MZInitSweepStepSize_mV = configData.ReadDouble("MZInitSweepStepSize_mV");

                engine.SendToGui(new GuiTextMessage("MZ differential modulator sweep (LV sweep)...", 2));
                // Range = 0 to 2 x Vcm, fixed step size
                double Vcm = baseLineChannels[chanNo].vcm;
                int NumberOfSteps = (int)(1 + Math.Abs(Vcm * 2 / (MZInitSweepStepSize_mV / 1000)));
                bool mzSweepFailed;
                ILMZSweepResult mzDiffData;
                double LeftArmImb_mA = baseLineChannels[chanNo].MzLeftArmImb_mA;
                double RightArmImb_mA = baseLineChannels[chanNo].MzRightArmImb_mA;

                mzInstrs.FCU2Asic.IimbLeft_mA = float.Parse(LeftArmImb_mA.ToString());
                mzInstrs.FCU2Asic.IimbRight_mA = float.Parse(RightArmImb_mA.ToString());

                // Retry if necessary
                MzAnalysisWrapper.MzAnalysisResults mzDiffModResults;
                double MZTapBias_V = configData.ReadDouble("MZTapBias_V");
                //ilmzUtils.SetVoltage(SourceMeter.ComplementaryTap, MZTapBias_V);
                mzInstrs.PowerMeter.SetDefaultState();
                mzInstrs.PowerMeter.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_mW;
                mzInstrs.PowerMeter.Range = configData.ReadDouble("OpmRangeInit_mW");
                bool modArmSourceByAsic = true;
                #endregion



                #region do DiffLV sweep
                // Initialise Utils
                ilmzUtils = new IlMzDriverUtils(mzInstrs);
                do
                {
                    mzSweepFailed = false;

                    do
                    {
                        mzDiffData = ilmzUtils.ModArm_DifferentialSweepByTrigger(LeftArmImb_mA / 1000, RightArmImb_mA / 1000,
                        Vcm * 2, 0.0, NumberOfSteps, MZSourceMeasureDelay_ms, MZTapBias_V, MZTapBias_V, modArmSourceByAsic);
                        // If overrange run again
                        if (MzAnalysisWrapper.CheckForOverrange(mzDiffData.SweepData[ILMZSweepDataType.FibrePower_mW]))
                        {
                            dataOk = false;
                            //modify by tim at2008-09-04 for Ag8163 NaN;
                            mzInstrs.PowerMeter.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_mW;
                            mzInstrs.PowerMeter.Range = mzInstrs.PowerMeter.Range * 10;
                            string a = mzInstrs.PowerMeter.Range.ToString();
                            if (a == "NaN")
                            {
                                mzInstrs.PowerMeter.Range = 10;
                            }
                            //end
                            //mzInstrs.PowerMeter.Range = mzInstrs.PowerMeter.Range * 10;
                        }
                        else
                        {
                            // if underrange fix the data and continue
                            dataOk = true;
                            if (MzAnalysisWrapper.CheckForUnderrange(mzDiffData.SweepData[ILMZSweepDataType.FibrePower_mW]))
                            {
                                mzDiffData.SweepData[ILMZSweepDataType.FibrePower_mW] =
                                    MzAnalysisWrapper.FixUnderRangeData(mzDiffData.SweepData[ILMZSweepDataType.FibrePower_mW]
                                    , minLevel_dBm);
                            }
                        }
                    }
                    while (!dataOk);
                    // Analyse LV data
                    mzDiffModResults = MzAnalysisWrapper.Differential_NegChirp_DifferentialSweep(mzDiffData, Vcm, 0);

                    // Check that the data looked ok.
                    if (Math.Abs(mzDiffModResults.PowerAtMax_dBm) > 200)
                    {
                        // The power meter has probably over-ranged. Increase the range and try again.
                        engine.SendToGui(new GuiTextMessage("Retry MZ differential modulator sweep at higher power range", 3));
                        double opmRange = mzInstrs.PowerMeter.Range;
                        if (opmRange < 1)
                        {
                            mzInstrs.PowerMeter.Range = opmRange * 10;
                            mzSweepFailed = true;
                        }
                    }
                } while (mzSweepFailed == true);
                #endregion


                #region store plots and data
                engine.SendToGui(new GuiTextMessage("Create LV sweep file......", 4));
                // store plots
                int lvchanNbr = chanNo + 1;
                string MzFileDirectory = configData.ReadString("MzFileDirectory");
                string LVFileName = Util_GenerateFileName.GenWithTimestamp(MzFileDirectory,
                    "FinalDiffMzLV_Chan" + lvchanNbr.ToString(), "", "csv");
                // write to file
                MzSweepFileWriter.WriteSweepData(LVFileName, mzDiffData,
                    new ILMZSweepDataType[] { ILMZSweepDataType.LeftArmModBias_V,
                                        ILMZSweepDataType.RightArmModBias_V,
                                        ILMZSweepDataType.FibrePower_mW,
                                        ILMZSweepDataType.TapComplementary_mA});
                // Add to archive
                //zipFile.AddFileToZip(LVFileName);
                engine.SendToGui(mzDiffData);
                #endregion
                #endregion


                //set mz to peak point

                #region set MZ to peak point
                engine.SendToGui(new GuiTextMessage("Set MZ to peak point......", 0));

                MzData peakData = new MzData();
                peakData.LeftArmImb_mA = LeftArmImb_mA;
                peakData.RightArmImb_mA = RightArmImb_mA;
                peakData.LeftArmMod_Peak_V = mzDiffModResults.Max_SrcL;
                peakData.RightArmMod_Peak_V = mzDiffModResults.Max_SrcR;

                IlMzDriverUtils.SetupMzToPeak(mzInstrs.FCU2Asic, peakData);
                #endregion

                //read frequency, power, smsr, locker current

                #region read frequency, power and locker current
                //engine.SendToGui(new GuiTextMessage("Test power at peak point.....", 0));


                engine.SendToGui(new GuiTextMessage("Test frequency ...", 0));

                mzInstrs.PowerMeter.Range = InstType_OpticalPowerMeter.AutoRange;
                double freq_GHz = 0;
                if (Measurements.Wavemeter != null
                    && Measurements.Wavemeter.IsOnline)
                {
                    freq_GHz = Measurements.ReadFrequency_GHz();
                }
                else
                {
                    // freq_GHz = DsdbrTuning.ReadFreq_GhzViaLowCostMeter(baseLineChannels[chanNo].Dsdbr.ItuFreq_GHz);
                    freq_GHz = DsdbrTuning.ReadFreq_GhzViaLowCostMeter();
                }
                engine.SendStatusMsg("Frequency of " + chanNo.ToString() + " channel is :" + freq_GHz.ToString() + " GHz");

                engine.SendToGui(new GuiTextMessage("Test power ....", 0));
                mzInstrs.PowerMeter.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;
                double uncalibratedPower = mzInstrs.PowerMeter.ReadPower();
                double power = uncalibratedPower + powerOffsetArray[chanNo];
                engine.SendStatusMsg("Power of " + chanNo.ToString() + " channel is :" + power.ToString() + " dBm");


                engine.SendToGui(new GuiTextMessage("Test Locker current....", 0));
                //switch the switch to dut to measure locker ratio
                IInstType_DigitalIO outLine_Tx = Optical_switch.GetDigiIoLine(IlmzOpticalSwitchLines.Dut_Tx_DigiIoLine);//Jack.zhang Tx swith between DUT GB and Etalon Wavemeter
                IInstType_DigitalIO outLine_Rx = Optical_switch.GetDigiIoLine(IlmzOpticalSwitchLines.Dut_Rx_DigiIoLine);//Jack.zhang Tx swith between DUT GB and Etalon Wavemeter
                outLine_Tx.LineState = IlmzOpticalSwitchLines.Dut_Tx_DigiIoLine_State;
                outLine_Rx.LineState = IlmzOpticalSwitchLines.Dut_Rx_DigiIoLine_State;

                DsdbrUtils.LockerCurrents lkcurrents = DsdbrUtils.ReadLockerCurrents();

                const double NOT_TESTED_SMSR = -999;

                double smsr = NOT_TESTED_SMSR;

                if (this.Osa != null)
                {
                    engine.SendToGui(new GuiTextMessage("Test SMSR....", 0));

                    smsr = SMSRMeasurement.MeasureSMSR(freq_GHz);
                }
                else
                {
                    engine.SendToGui(new GuiTextMessage("No need to test SMSR....", 0));
                    engine.SendStatusMsg("SMSR of " + chanNo.ToString() + " channel is temperally set to :" + smsr.ToString());
                }

                #endregion

                //Set mz to quad point, read the ctap current.

                #region set MZ to qual point and read Ctap current
                engine.SendToGui(new GuiTextMessage("Set MZ to quad point, and  test Ctap current...", 0));
                MzData quadData = new MzData();
                quadData.LeftArmImb_mA = LeftArmImb_mA;
                quadData.RightArmImb_mA = RightArmImb_mA;
                quadData.LeftArmMod_Quad_V = mzDiffModResults.Quad_SrcL;
                quadData.RightArmMod_Quad_V = mzDiffModResults.Quad_SrcR;

                IlMzDriverUtils.SetupMzToQuad(mzInstrs.FCU2Asic, quadData);

                OutLineCtap = Optical_switch.GetDigiIoLine(IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine);//line 3

                OutLineCtap.LineState = IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine_State; // read Ctap current, true

                double ctap_Amp = mzInstrs.FCU2Asic.ICtap_mA;
                #endregion

                #region read power ratio
                //switch the switch to etalon to measure power ratio
                IInstType_DigitalIO outLine_C_L_Band = Optical_switch.GetDigiIoLine(IlmzOpticalSwitchLines.C_Band_DigiIoLine);//Jack.zhang PD control line is 1

                if (FreqBandType.Contains("C"))//if device is C band then set outLine_C_L_Band.LineState = false,
                {
                    outLine_C_L_Band.LineState = IlmzOpticalSwitchLines.C_Band_DigiIoLine_State;//false,Jack,zhang first switch the relay card to optical box for power ratio overall map  
                }
                if (FreqBandType.Contains("L"))
                {
                    outLine_C_L_Band.LineState = IlmzOpticalSwitchLines.L_Band_DigiIoLine_State;//true,if device is L band then set outLine_C_L_Band.LineState = true;
                }

                double PowerRatio = DsdbrUtils.ReadPowerRatio(true, true, true, false);
                #endregion

                #region record test result

                retData.AddDouble("Freq_ch" + datumPrefix, freq_GHz);
                retData.AddDouble("Irx_ch" + datumPrefix, lkcurrents.RxCurrent_mA);
                retData.AddDouble("Itx_ch" + datumPrefix, lkcurrents.TxCurrent_mA);
                retData.AddDouble("LRatio_ch" + datumPrefix, lkcurrents.LockRatio);
                retData.AddDouble("PRatio_ch" + datumPrefix, PowerRatio);
                retData.AddDouble("Pwr_ch" + datumPrefix, power);
                retData.AddDouble("Smsr_ch" + datumPrefix, smsr);
                retData.AddDouble("Mz_vpeak_left_ch" + datumPrefix, mzDiffModResults.Max_SrcL);
                retData.AddDouble("Mz_vpeak_right_ch" + datumPrefix, mzDiffModResults.Max_SrcR);
                retData.AddDouble("Mz_vpi_ch" + datumPrefix, mzDiffModResults.Vpi);
                retData.AddDouble("Mz_DcEr_ch" + datumPrefix, mzDiffModResults.ExtinctionRatio_dB);
                retData.AddDouble("Ctap_quad_ch" + datumPrefix, ctap_Amp);
                retData.AddFileLink("Mz_sweep_file_ch" + datumPrefix, LVFileName);
                if (!retData.IsPresent("Dsdbr_temp_cl"))
                    retData.AddDouble("Dsdbr_temp_cl", Dsdbr_temp_cl);

                if (!testStage.Contains("base"))
                {
                    retData.AddDouble("Freq_ch" + datumPrefix + deltaPrefix, freq_GHz - FindValueByName(engine, referenceLines, "FREQ_CH" + datumPrefix.ToUpper()));
                    retData.AddDouble("Irx_ch" + datumPrefix + deltaPrefix, lkcurrents.RxCurrent_mA - FindValueByName(engine, referenceLines, "IRX_CH" + datumPrefix.ToUpper()));
                    retData.AddDouble("Itx_ch" + datumPrefix + deltaPrefix, lkcurrents.TxCurrent_mA - FindValueByName(engine, referenceLines, "ITX_CH" + datumPrefix.ToUpper()));
                    retData.AddDouble("LRatio_ch" + datumPrefix + deltaPrefix, lkcurrents.LockRatio - FindValueByName(engine, referenceLines, "LRATIO_CH" + datumPrefix.ToUpper()));
                    retData.AddDouble("PRatio_ch" + datumPrefix + deltaPrefix, PowerRatio - FindValueByName(engine, referenceLines, "PRATIO_CH" + datumPrefix.ToUpper()));
                    retData.AddDouble("Pwr_ch" + datumPrefix + deltaPrefix, power - FindValueByName(engine, referenceLines, "PWR_CH" + datumPrefix.ToUpper()));
                    retData.AddDouble("Mz_vpeak_left_ch" + datumPrefix + deltaPrefix, mzDiffModResults.Max_SrcL - FindValueByName(engine, referenceLines, "MZ_VPEAK_LEFT_CH" + datumPrefix.ToUpper()));
                    retData.AddDouble("Mz_vpeak_right_ch" + datumPrefix + deltaPrefix, mzDiffModResults.Max_SrcR - FindValueByName(engine, referenceLines, "MZ_VPEAK_RIGHT_CH" + datumPrefix.ToUpper()));
                    retData.AddDouble("Mz_vpi_ch" + datumPrefix + deltaPrefix, mzDiffModResults.Vpi - FindValueByName(engine, referenceLines, "MZ_VPI_CH" + datumPrefix.ToUpper()));
                    retData.AddDouble("Mz_DcEr_ch" + datumPrefix + deltaPrefix, mzDiffModResults.ExtinctionRatio_dB - FindValueByName(engine, referenceLines, "MZ_DCER_CH" + datumPrefix.ToUpper()));
                    retData.AddDouble("Ctap_quad_ch" + datumPrefix + deltaPrefix, ctap_Amp - FindValueByName(engine, referenceLines, "MZ_CTAP_QUAD_CH" + datumPrefix.ToUpper()));
                    
                    if (!retData.IsPresent("Tcase_hight_cl_delta"))
                    {
                        retData.AddDouble("Tcase_hight_cl_delta", Dsdbr_temp_cl - FindValueByName(engine, referenceLines, "TCASE_HIGHT_CL"));
                    }
                   
                    if (smsr == NOT_TESTED_SMSR)
                    {
                        retData.AddDouble("Smsr_ch" + datumPrefix + deltaPrefix, 0);
                    }
                    else
                    {
                        retData.AddDouble("Smsr_ch" + datumPrefix + deltaPrefix, smsr - referenceSMSR);
                    }
                }
                engine.SendToGui(new GuiTextMessage("The" + chanNo.ToString() + " channel test finished!", 0));
                #endregion

                #endregion
            }

            return retData;
        }




        /// <summary>
        /// find base line reference data by parameter name
        /// </summary>
        /// <param name="lines">reference data lines</param>
        /// <param name="paramName">parameter name</param>
        /// <returns></returns>
        private double FindValueByName(ITestEngine engine, List<string[]> lines, string paramName)
        {
            for (int i = 1; i < lines.Count; i++) //first line is head title
            {
                for (int j = 0; j < lines[i].Length; j++)
                {
                    if (lines[i][j].Contains(paramName))
                    {
                        double retValue = 0;
                        try
                        {
                            retValue = Double.Parse(lines[i][j + 1]);
                        }
                        catch
                        {
                            this.failModeCheck.RaiseError_Mod_NonParamFail(engine, "invalid " + paramName + " item in base line reference file! \n please treat it and try this module again!", FailModeCategory_Enum.SW);
                        }
                        return retValue;
                    }
                }
            }

            this.failModeCheck.RaiseError_Mod_NonParamFail(engine, "The" + paramName + " item is not in reference file, pls check and try again!", FailModeCategory_Enum.SW);
            return 0;

        }

        public Type UserControl
        {
            // GUI for this module
            get { return typeof(IlmzChannelSPCGui); }
        }

        #endregion

        #region private Data

        const int numRthTries = 10;
        const int rthDelay_mS = 500;

        const int numSetupIlmzTries = 3;

        double opmRange;
        bool dataOk;

        // Ilmz channels
        BaseLineData[] baseLineChannels;

        // Configs
        TcmzChannelChar_ND_Config ilmzChannelChar_ND_configObj;
        TcmzMzSweep_Config mzConfig;

        // Instruments
        IlMzInstruments mzInstrs;
        IInstType_OpticalPowerMeter PowerHead;
        IInstType_OSA Osa = null;
        IInstType_TecController dsdbrTec;
        IInstType_TecController mzTec = null;
        InstType_ElectricalSource fcuSource = null;

        // Utils
        IlMzDriverUtils ilmzUtils;
        Util_ZipFile zipFile;
        TestSelection testSelect;
        Switch_Osa_MzOpm switchOsaMzOpm;


        double Dsdbr_temp_cl = 0;

        double Mz_vpeak_left_ch;
        double Mz_vpeak_right_ch;

        double Mz_vpi_ch;

        double Pwr_ch;

        double Smsr_ch;

        double Freq_ch;

        double Irx_ch;

        double Itx_ch;

        double Mz_DcEr_ch;

        double Mz_sweep_file_ch;

        int MZSourceMeasureDelay_ms = 5;
        #endregion
    }
}


// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// Mod_CG.cs
//
// Author: tommy.yu, 2009
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.Framework.Limits;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.ExternalData;
using System.Collections.Specialized;
using Bookham.fcumapping.CommonData;
using Bookham.TestLibrary.Algorithms;
using Bookham.TestLibrary.Instruments;
using Bookham.TestLibrary.InstrTypes;
using System.IO;
using System.Windows.Forms;
using Bookham.TestLibrary.Utilities;
using Bookham.TestLibrary.FcuCommonUtils;
using Bookham.TestSolution.Instruments;
using Bookham.TestSolution.IlmzCommonUtils;
using Bookham.ToolKit.Mz;
using Bookham.TestSolution.IlmzCommonData;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Test Module Template.
    /// </summary>
    public class Mod_OverallMap : ITestModule
    {

        #region parameter
        private ITestEngine overallMapEngine = null;
        private const int totalPercentage = 147;
        private IInstType_OpticalPowerMeter OpmLaser;
        private IInstType_TriggeredOpticalPowerMeter PowerMeterWithTrigger;

        private double calFreq_Ghz=0;
        private double max_I_ImbDiffSweep_mA; // max Imb current  ==7mA
        private double power_offset;//cal the power offset by Ctap_mA on trough to Peak Power. Jack.zhang 2012-06-11
        Inst_Fcu2Asic fcu;

        double Mz_Peak_Imb_Left_mA = 0;
        double Mz_Peak_Imb_Right_mA = 0;

        double Mz_Trough_Imb_Left_mA = 0;
        double Mz_Trough_Imb_Right_mA = 0;

        double von_left = 0;
        double von_right = 0;
        double voff_left = 0;
        double voff_right = 0;

        double ref_mz_sum = 0;
        double ref_mz_ratio = 0;
        double ref_power = 0;
        string fileFullPath;
        double tap_comp_dark_i = 0;
        string linesOverallMapFilePath;
        string cg_pratio_wl_file;
        string cg_passfail_file;
        string cg_qametrics_file;
        string matrix_pratio_summary_map;
        string cg_sm_lines_file;
        string cg_setting_path;

        float IsoaForMapping_mA = 0;
        float IgainForMapping_mA = 0;
        float IphaseForMapping_mA = 0;
        float IrearsoaForMapping_mA = 0;

        List<double> SuperMode_Isoa_mA = new List<double>();
        List<double> SuperMode_Mz_Imb_Left_mA = new List<double>();
        List<double> SuperMode_Mz_Imb_Right_mA = new List<double>();
        List<int> SuperMode_Var_Pot = new List<int>();
        List<int> SuperMode_Tx_Pot = new List<int>();
        List<int> SuperMode_Rx_Pot = new List<int>();

        Inst_Ke2510 Optical_switch;

        string laserWaferID = "";//jim

        Specification mainSpec;
        int frontSweepPointsCount;

        int fp_Lower_Limit;
        int fp_Upper_Limit;
        
        private FailModeCheck failModeCheck;
        
        #endregion

        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, DatumList configData, InstrumentCollection instruments, ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {
            // TODO - add your code here!
            engine.GuiToFront();
            engine.GuiShow();
            overallMapEngine = engine;

            #region initialize instrument and parameters

            engine.SendToGui("Overall Map Sweep");

            this.failModeCheck = configData.ReadReference("FailModeCheck") as FailModeCheck;
            this.mainSpec = configData.ReadReference("MainSpec") as Specification;
            this.frontSweepPointsCount = configData.ReadSint32("FrontSweepPointsCount");
            this.fp_Lower_Limit = configData.ReadSint32("FP_LOWER_LIMIT");
            this.fp_Upper_Limit = configData.ReadSint32("FP_UPPER_LIMIT");

            Dictionary<string, double> cocResults_UPPER_FP_CROSSING = configData.ReadReference("CocResults_UPPER_FP_CROSSING") as Dictionary<string, double>;

            bool IsErrorHappen = false;
            bool RetryModule = false;
            string ErrorInformation = "";
            DatumList returnData = new DatumList();
            TestParamConfigAccessor MapSettings = (TestParamConfigAccessor)configData.ReadReference("Mapping_Settings");
            TestParamConfigAccessor MzsweepSettings = (TestParamConfigAccessor)configData.ReadReference("Mzsweep_Settings");
            TestParamConfigAccessor OpticalCalConfig = (TestParamConfigAccessor)configData.ReadReference("OpticalCalConfig");
            string work_dir = configData.ReadString("RESULT_DIR");
            string laser_id = configData.ReadString("LASER_ID");
            string timestamp = configData.ReadString("TIME_STAMP");
            double power_Offset_dB = 10;// optical box power offset
            try
            {
                power_Offset_dB = OpticalCalConfig.GetDoubleParam("OpticalBox_Power_Offset_dB");
            }
            catch(Exception ex)
            {
                power_Offset_dB = 10;
            }
            double power_tollerance_dB = configData.ReadDouble("TARGET_POWER_Tollerance_dB");
            double target_power_dBm = configData.ReadDouble("TARGET_POWER_dBm");
            double target_power_mw = Alg_PowConvert_dB.Convert_dBmtomW(target_power_dBm);
            double Min_target_power_mw = Alg_PowConvert_dB.Convert_dBmtomW(target_power_dBm - power_tollerance_dB);
            double Max_target_power_mw = Alg_PowConvert_dB.Convert_dBmtomW(target_power_dBm + power_tollerance_dB);

            double Min_target_referencePD_mA = FreqCalByEtalon.Reference_PD_Responsivity.Slope * Min_target_power_mw + FreqCalByEtalon.Reference_PD_Responsivity.YIntercept;//cal the reference PD current in light of laser target power. jack.zhang 2012-11-19
            double Max_target_referencePD_mA = FreqCalByEtalon.Reference_PD_Responsivity.Slope * Max_target_power_mw + FreqCalByEtalon.Reference_PD_Responsivity.YIntercept;//cal the reference PD current in light of laser target power. jack.zhang 2012-11-19
            double target_referencePD_mA = FreqCalByEtalon.Reference_PD_Responsivity.Slope * target_power_mw + FreqCalByEtalon.Reference_PD_Responsivity.YIntercept;//cal the reference PD current in light of laser target power. jack.zhang 2012-11-19
            Min_target_referencePD_mA = Min_target_power_mw * FreqCalByEtalon.Average_Reference_PD_Responsivity;
            Max_target_referencePD_mA = Max_target_power_mw * FreqCalByEtalon.Average_Reference_PD_Responsivity;
            target_referencePD_mA = target_power_mw* FreqCalByEtalon.Average_Reference_PD_Responsivity;
            if (Math.Abs(FreqCalByEtalon.Reference_PD_Responsivity.Slope) > 0.35)//temp to avoid etaloncal dtat with wrong power data. jack.zhang 2012-11-19
            {
                target_referencePD_mA = target_power_mw * 0.15;
                Min_target_referencePD_mA =Min_target_power_mw * 0.15;
                Max_target_referencePD_mA = Max_target_power_mw * 0.15;
            }
            double maxSoaInMapping_mA = configData.ReadDouble("Max_Isoa_mA");
            double minSoaInMapping_mA = configData.ReadDouble("Min_Isoa_mA");
            double DUTBeamSpliter_ReferPercent = MapSettings.GetDoubleParam("DUTBeamSpliter_ReferPercent"); 
            bool useDUTEtalon = configData.ReadBool("USEDUTETALON");
            int SM_NUM = 0;
            double MzPeakImbLeft_mA = 0,MzPeakImbRight_mA = 0,Power_Output_mW=0,Power_Input_mW=0;
            double L_dark_mA = 0, R_dark_mA = 0;
            double ITX_DARK_mA = 0, IRX_DARK_mA = 0;
            string Overmap_MiddlePoint_MZImb_file="";
            cg_setting_path = configData.ReadString("CG_Setting_File");
            laserWaferID = configData.ReadString("LaserWaferID");//jim
            string Rear_RearSoa_Switch = configData.ReadString("Rear_RearSoa_Switch");

            this.cg_sm_lines_file = configData.ReadString("NoFilePath");
            fileFullPath = configData.ReadString("NoFilePath");
            linesOverallMapFilePath = configData.ReadString("NoFilePath");
            cg_pratio_wl_file = configData.ReadString("NoFilePath");
            cg_passfail_file = configData.ReadString("NoFilePath");
            cg_qametrics_file = configData.ReadString("NoFilePath");
            matrix_pratio_summary_map = configData.ReadString("NoFilePath");

            string FreqBandType = configData.ReadString("FreqBand");
            fcu = (Inst_Fcu2Asic)instruments["Fcu2Asic"];
            OpmLaser = (InstType_OpticalPowerMeter)instruments["OpmMz"];
            PowerMeterWithTrigger=(IInstType_TriggeredOpticalPowerMeter)instruments["OpmMz"];
            Optical_switch = (Inst_Ke2510)instruments["Optical_switch"];

            fcu.OverallmapProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(fcu_OverallmapProgressChanged);
            if (fcu.IsOnline == false)
            {
                fcu.IsOnline = true;
            }

            Bookham.fcumapping.CommonData.CDSDBROverallModeMap overallMap = new Bookham.fcumapping.CommonData.CDSDBROverallModeMap(fcu, work_dir, laser_id, timestamp, instruments);

            #endregion   initialize instrument and parameters

            #region get dark current
            try
            {
                fcu.ZeroCurrent();
                fcu.CloseAllAsic();
            }
            catch(Exception e)
            {
                ErrorInformation=e.Message;
                IsErrorHappen = true;
            }
            if (!IsErrorHappen)
            {
            #endregion  get dark current

                #region set fcu

                IsoaForMapping_mA = (float)(configData.ReadDouble("IsoaForMapping_mA"));
                IgainForMapping_mA = (float)(configData.ReadDouble("IgainForMapping_mA")); ;
                IphaseForMapping_mA = (float)(configData.ReadDouble("IphaseForMapping_mA"));
                IrearsoaForMapping_mA = (float)(configData.ReadDouble("IrearsoaForMapping_mA"));

                float IrearForMzImbPeakSweep_mA = float.Parse(MapSettings.GetStringParam("IrearForPowerLeveling_mA"));
                int FSNbrForMzImbPeakSweep = int.Parse(MapSettings.GetStringParam("FrontPairForPowerLeveling"));

                fcu.IGain_mA = IgainForMapping_mA;
                fcu.ISoa_mA = IsoaForMapping_mA;
                fcu.IRear_mA = IrearForMzImbPeakSweep_mA;
                fcu.IPhase_mA = IphaseForMapping_mA;
                fcu.SetFrontSectionPairCurrent(FSNbrForMzImbPeakSweep, (float)5, (float)1.3);
                fcu.IRearSoa_mA = IrearsoaForMapping_mA;
                fcu.VLeftModBias_Volt = (float)0.0;
                fcu.VRightModBias_Volt = (float)0.0;

                #endregion set fcu

                #region set switch status for mapping
                IInstType_DigitalIO outLine_C_L_Band = Optical_switch.GetDigiIoLine(IlmzOpticalSwitchLines.C_Band_DigiIoLine);//Jack.zhang PD control line is 1

                if (FreqBandType.Contains("C"))//if device is C band then set outLine_C_L_Band.LineState = false,
                {
                    outLine_C_L_Band.LineState = IlmzOpticalSwitchLines.C_Band_DigiIoLine_State;//false,Jack,zhang first switch the relay card to optical box for power ratio overall map  
                }
                if (FreqBandType.Contains("L"))
                {
                    outLine_C_L_Band.LineState = IlmzOpticalSwitchLines.L_Band_DigiIoLine_State;//true,if device is L band then set outLine_C_L_Band.LineState = true;
                }

                IInstType_DigitalIO outLine_Tx = Optical_switch.GetDigiIoLine(IlmzOpticalSwitchLines.Etalon_Tx_DigiIoLine);//Jack.zhang PD control
                outLine_Tx.LineState = (useDUTEtalon) ? IlmzOpticalSwitchLines.Dut_Tx_DigiIoLine_State : IlmzOpticalSwitchLines.Etalon_Tx_DigiIoLine_State;//Jack,zhang first switch the relay card to optical box for power ratio overall map 

                IInstType_DigitalIO outLine_Rx = Optical_switch.GetDigiIoLine(IlmzOpticalSwitchLines.Etalon_Rx_DigiIoLine);//Jack.zhang PD control
                outLine_Rx.LineState = (useDUTEtalon) ? IlmzOpticalSwitchLines.Dut_Rx_DigiIoLine_State : IlmzOpticalSwitchLines.Etalon_Rx_DigiIoLine_State;//Jack,zhang first switch the relay card to optical box for power ratio overall map 

                IInstType_DigitalIO outLine_Reference_Ctap = Optical_switch.GetDigiIoLine(IlmzOpticalSwitchLines.Etalon_Ref_DigiIoLine);//Jack.zhang PD control
                outLine_Reference_Ctap.LineState = IlmzOpticalSwitchLines.Etalon_Ref_DigiIoLine_State;//Jack,false for reference true for ctap

                #endregion  set switch status for mapping

                #region do mz differencial tuning

                engine.SendStatusMsg("do MZ differencial tuning.");
                PowerMeterWithTrigger.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_mW;
                PowerMeterWithTrigger.Range = InstType_OpticalPowerMeter.AutoRange;
                //double max_power_at_imb_peak = DoMzTuneAndGenerateLVFile(work_dir, laser_id, timestamp, MzsweepSettings, 999);
                ImbDiffSweepWithAsicFcu(work_dir, laser_id, timestamp, MzsweepSettings, OpticalCalConfig, 999, out Power_Output_mW, out Power_Input_mW);
                Overmap_MiddlePoint_MZImb_file = fileFullPath;
                MzPeakImbLeft_mA = Mz_Peak_Imb_Left_mA;
                MzPeakImbRight_mA = Mz_Peak_Imb_Right_mA;//for save OV middle point Mz imb set in PCAS but not the last SM middle point. jack.zhang 2012-06-12
                engine.SendStatusMsg("do MZ tuning ok.");

                #endregion   do mz differencial tuning
                bool reachTargetPower = false;
                string continueMessage = "Max laser power is :" + Power_Input_mW.ToString() + " mW," + "\nDo you want continue?";
                if (Power_Input_mW < target_power_mw)
                {
                    DoPowerLevelingForeachSM(engine, Min_target_referencePD_mA, Max_target_referencePD_mA, minSoaInMapping_mA, maxSoaInMapping_mA);
                }

                if (fcu.Fix_Port_mA(false) < target_referencePD_mA / 2.0)
                {
                    reachTargetPower = PowerLev(engine, target_power_dBm, power_Offset_dB, power_tollerance_dB, minSoaInMapping_mA, maxSoaInMapping_mA);
                   
                }

                if ((fcu.Fix_Port_mA(false) > target_referencePD_mA / 2.0) || reachTargetPower)//power too low(if <target_power/2) to do OVmap.Jack.zhang 2012-06-12
                {
                    IsoaForMapping_mA = fcu.ISoa_mA;

                    #region fine tunning filter pot

                    //set laser for max high optical power
                    fcu.IGain_mA = IgainForMapping_mA;
                    fcu.ISoa_mA = IsoaForMapping_mA;
                    fcu.IRear_mA = IrearForMzImbPeakSweep_mA;
                    fcu.IPhase_mA = IphaseForMapping_mA;
                    int FrontPair_PotLeveling = 7;
                    if (FreqCalByEtalon.Average_Power_Ratio_SlopeToGHz < 0)
                        FrontPair_PotLeveling = 1;
                    fcu.SetFrontSectionPairCurrent(FrontPair_PotLeveling, (float)5, (float)1.3);
                    fcu.IRearSoa_mA = IrearsoaForMapping_mA;

                    fcu.TuneVarPotValue();//if Filter DAC value too High or Low
                    //temp_Var_Amp = fcu.Locker_Tx_mA(true);
                    //temp_Var_Amp = fcu.Locker_Rx_mA(true);
                    fcu.Locker_tran_pot = FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Tx[FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Tx.Count - 1].Pot;
                    fcu.Locker_refi_pot = FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Rx[FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Rx.Count - 1].Pot;

                    #endregion fine tunning filter pot

                    #region do overall map
                    engine.SendStatusMsg("do overallMap running");
                    fcu.IGain_mA = IgainForMapping_mA;
                    fcu.ISoa_mA = IsoaForMapping_mA;
                    fcu.IPhase_mA = IphaseForMapping_mA;
                    fcu.IRearSoa_mA = IrearsoaForMapping_mA;
                    overallMap.Rear_RearSoa_Switch = Rear_RearSoa_Switch;
                    try
                    {
                        overallMap.CollectData();
                    }
                    catch (Exception e)
                    {
                        ErrorInformation = "Fail to Get Overall Map Data:" + e.Message;
                        engine.SendStatusMsg(ErrorInformation);
                        IsErrorHappen = true;
                    }
                    if (!IsErrorHappen)
                    {
                        engine.SendStatusMsg("Begin analysis overallmap......");

                        Alg_MapAnalysis map = new Alg_MapAnalysis(work_dir, cg_setting_path);
                        ScreeningOverallMapResults Om = map.DoScreeningOverallMapAnalysis(laser_id, timestamp, laserWaferID);//jim
                        SM_NUM = Om.SupermodeCount;

                        #region Power Leveling for SM
                        engine.SendStatusMsg("do Pot leveling for each SM");

                        #region To checkthe average Reference DAC in each SM Middle if the optical power too low, if low then tune Isoa to meet it and modify Var pot
                        int _SuperMode_Var_Pot = 0;
                        int _SuperMode_Tx_Pot = 0;
                        int _SuperMode_Rx_Pot = 0;
                        CsvReader cr = new CsvReader();
                        List<string[]> lines = new List<string[]>();
                        OpmLaser.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_mW;

                        // chongjian.liang 2016.08.29
                        List<float> phasePointsmAList_SMScanPotReckoning_mA = configData.ReadReference("PhasePointsmAList_SMScanPotReckoning") as List<float>;

                        for (int i = 0; i < Om.SupermodeCount; i++)
                        {
                            engine.SendStatusMsg("do MZ differencial tuning on SM " + i.ToString());

                            //consider optical power will be reduce with FP increase.so we can mask below in order to save power leveling test time.Jack.zhang 2011-11-14
                            //fcu.ISoa_mA = IsoaForMapping_mA;

                            lines = cr.ReadFile(Om.MiddleLineCurrentsFile[i]);

                            SetFcu(lines[(int)Math.Round(3 * lines.Count / 4.0, 0)]);//use Mid Irear_mA(~30mA) due to Mid Power for SOA leveling. jack.zhang 2012-11-13

                            ImbDiffSweepWithAsicFcu(work_dir, laser_id, timestamp, MzsweepSettings, OpticalCalConfig, i, out Power_Output_mW, out Power_Input_mW);

                            SuperMode_Mz_Imb_Left_mA.Add(Mz_Peak_Imb_Left_mA);
                            SuperMode_Mz_Imb_Right_mA.Add(Mz_Peak_Imb_Right_mA);
                            if (!reachTargetPower)
                            {
                                if (Power_Input_mW > Max_target_power_mw)
                                {
                                    fcu.ISoa_mA = (float)minSoaInMapping_mA;
                                    DoPowerLevelingForeachSM(engine, Min_target_referencePD_mA, Max_target_referencePD_mA, minSoaInMapping_mA, maxSoaInMapping_mA);
                                }
                                if (Power_Input_mW < Min_target_power_mw)
                                    DoPowerLevelingForeachSM(engine, Min_target_referencePD_mA, Max_target_referencePD_mA, minSoaInMapping_mA, maxSoaInMapping_mA);
                            }
                            SuperMode_Isoa_mA.Add(fcu.ISoa_mA);

                            _SuperMode_Var_Pot = FindPossiblePotForSMAverageDAC(65535 / 2, fcu.Var_Port_mA(true, false, true), FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Var);

                            if (!useDUTEtalon)
                            {
                                _SuperMode_Tx_Pot = FindPossiblePotForSMAverageDAC(65535 / 2, fcu.Fix_Port_mA(false) * FreqCalByEtalon.MidRatioEtalonTxToReference, FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Tx);//for 50:50 beamsplitter in etalon box and front output same as back output. jack.zhang 2011-11-14
                                _SuperMode_Rx_Pot = FindPossiblePotForSMAverageDAC(65535 / 2, fcu.Fix_Port_mA(false) * FreqCalByEtalon.MidRatioEtalonRxToReference, FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Rx);//for 50:50 beamsplitter in etalon box and front output same as back output. jack.zhang 2011-11-14
                            }
                            // Get the pot value for SM scanning from the max value of 6 points in SM map - chongjian.liang 2016.08.29
                            else
                            {
                                if (lines.Count < 7)
                                {
                                    this.failModeCheck.RaiseError_Mod_NonParamFail(engine, string.Format("There are only {} points in file {1}", lines.Count - 1, Om.MiddleLineCurrentsFile[i]), FailModeCategory_Enum.PR);
                                }

                                double locker_Tx_Max_mA = 0;
                                double locker_Rx_Max_mA = 0;
                                {
                                    List<float> rearLineIndexList_SMScanPotReckoning = new List<float>();
                                    rearLineIndexList_SMScanPotReckoning.Add(1);
                                    rearLineIndexList_SMScanPotReckoning.Add(lines.Count / 10);
                                    rearLineIndexList_SMScanPotReckoning.Add(lines.Count * 2 / 10);
                                    rearLineIndexList_SMScanPotReckoning.Add(lines.Count * 5 / 10);
                                    rearLineIndexList_SMScanPotReckoning.Add(lines.Count - 1);

                                    foreach (int rearLineIndex in rearLineIndexList_SMScanPotReckoning)
                                    {
                                        foreach (float phasemA in phasePointsmAList_SMScanPotReckoning_mA)
                                        {
                                            this.SetFcu(lines[rearLineIndex], phasemA);

                                            locker_Tx_Max_mA = Math.Max(locker_Tx_Max_mA, fcu.Locker_Tx_mA(true, false, true));
                                            locker_Rx_Max_mA = Math.Max(locker_Rx_Max_mA, fcu.Locker_Rx_mA(true, false, true));
                                        }
                                    }
                                }

                                _SuperMode_Tx_Pot = FindPossiblePotForSMAverageDAC(65535 * 3 / 5, locker_Tx_Max_mA, FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Tx);//for 50:50 beamsplitter in etalon box and front output same as back output. jack.zhang 2011-11-14
                                _SuperMode_Rx_Pot = FindPossiblePotForSMAverageDAC(65535 * 3 / 5, locker_Rx_Max_mA, FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Rx);//for 50:50 beamsplitter in etalon box and front output same as back output. jack.zhang 2011-11-14
                            }

                            SuperMode_Var_Pot.Add(_SuperMode_Var_Pot);
                            SuperMode_Tx_Pot.Add(_SuperMode_Tx_Pot);
                            SuperMode_Rx_Pot.Add(_SuperMode_Rx_Pot);

                            //continueMessage = "SuperMode" + i.ToString() + " optical power is lower:" + Average_Reference_Power.ToString() + "\nDo you want continue?";
                            //response = engine.ShowYesNoUserQuery(continueMessage);
                            //if (response == ButtonId.No)
                            //{
                            //    engine.ErrorInModule("User terminate overallmap scan for Low Optical Power do Mapping!");
                            //    break;
                            //}
                        }
                        #endregion To checkthe average Reference DAC in each SM Middle if the optical power too low, if low then tune Isoa to meet it and modify Var pot

                        #endregion Power Leveling for SM

                        #region add data to datumlist
                        if (File.Exists(Om.SupermodeLinesFile))
                        {
                            cg_sm_lines_file = Om.SupermodeLinesFile;
                        }
                        if (File.Exists(Om.PowerRatioSmFile))
                        {
                            cg_pratio_wl_file = Om.PowerRatioSmFile;
                        }
                        if (File.Exists(Om.PassFailFile))
                        {
                            cg_passfail_file = Om.PassFailFile;
                        }
                        if (File.Exists(Om.QaMetricsFile))
                        {
                            cg_qametrics_file = Om.QaMetricsFile;
                        }
                        if (File.Exists(Om.CombinedDFSMapFile))
                        {
                            matrix_pratio_summary_map = Om.CombinedDFSMapFile;
                        }
                        #endregion  add data to datumlist
                        engine.SendStatusMsg("do overall map ok.");
                    #endregion do overall map
                    }
                }
            }

            Dictionary<string, double> testResults_UPPER_FP_CROSSING;

            this.TestLimit_UPPER_FP_CROSSING(out testResults_UPPER_FP_CROSSING);
            
            Dictionary<string, double> testResults_DELTA_FP_CROSSING;

            this.TestLimit_DELTA_FP_CROSSING(testResults_UPPER_FP_CROSSING, cocResults_UPPER_FP_CROSSING, out testResults_DELTA_FP_CROSSING);

            #region generate return data

            foreach (string limitName in testResults_UPPER_FP_CROSSING.Keys)
            {
                returnData.AddDouble(limitName, testResults_UPPER_FP_CROSSING[limitName]);
            }

            foreach (string limitName in testResults_DELTA_FP_CROSSING.Keys)
            {
                returnData.AddDouble(limitName, testResults_DELTA_FP_CROSSING[limitName]);
            }

            returnData.AddSint32("SM_NUM", SM_NUM);
            returnData.AddDouble("ref_power", ref_power);
            returnData.AddDouble("Ref_fiber_power", ref_power);
            returnData.AddDouble("calFreq_Ghz", calFreq_Ghz);
            returnData.AddDouble("von_left", von_left);
            returnData.AddDouble("von_right", von_right);
            returnData.AddDouble("voff_left", voff_left);
            returnData.AddDouble("voff_right", voff_right);
            returnData.AddDouble("ref_mz_sum", ref_mz_sum);
            returnData.AddDouble("ref_mz_ratio", ref_mz_ratio);
            returnData.AddDouble("max_V_Bias", max_I_ImbDiffSweep_mA);
            returnData.AddFileLink("fileFullPath", Overmap_MiddlePoint_MZImb_file);
            returnData.AddDouble("ITX_DARK_mA", ITX_DARK_mA);
            returnData.AddDouble("IRX_DARK_mA", IRX_DARK_mA);
            returnData.AddDouble("L_dark_mA", L_dark_mA);
            returnData.AddDouble("R_dark_mA", R_dark_mA);
            returnData.AddDouble("tap_comp_dark_i", tap_comp_dark_i);
            returnData.AddFileLink("cg_sm_lines_file", cg_sm_lines_file);
            returnData.AddFileLink("cg_pratio_wl_file", cg_pratio_wl_file);
            returnData.AddFileLink("cg_passfail_file", cg_passfail_file);
            returnData.AddFileLink("cg_qametrics_file", cg_qametrics_file);
            returnData.AddFileLink("matrix_pratio_summary_map", matrix_pratio_summary_map);
            returnData.AddFileLink("registry_file", cg_setting_path);
            returnData.AddDouble("imb_left", MzPeakImbLeft_mA); // max imbalance current
            returnData.AddDouble("imb_right", MzPeakImbRight_mA); // max imbalance current
            returnData.AddDouble("SOA_PowerLevel", IsoaForMapping_mA);

            returnData.AddReference("SuperMode_Isoa_mA_List", SuperMode_Isoa_mA);
            returnData.AddReference("SuperMode_Mz_Imb_Left_mA_List", SuperMode_Mz_Imb_Left_mA);
            returnData.AddReference("SuperMode_Mz_Imb_Right_mA_List", SuperMode_Mz_Imb_Right_mA);
            returnData.AddReference("SuperMode_Var_Pot_List", SuperMode_Var_Pot);
            returnData.AddReference("SuperMode_Tx_Pot_List", SuperMode_Tx_Pot);
            returnData.AddReference("SuperMode_Rx_Pot_List", SuperMode_Rx_Pot);
            returnData.AddString("ErrorInformation",ErrorInformation);
            returnData.AddBool("RetryModule", RetryModule);
            #endregion

            return returnData;
        }

        /// <summary>
        /// Test limit UPPER_FP{}_CROSSING - chongjian.liang 2013.9.10
        /// </summary>
        /// <param name="testResults">Containing the limit names and the test results</param>
        private void TestLimit_UPPER_FP_CROSSING(out Dictionary<string, double> testResults)
        {
            testResults = new Dictionary<string, double>();

            using (CsvReader csvReader = new CsvReader())
            {
                if (File.Exists(this.cg_sm_lines_file))
                {
                    List<string[]> lines = csvReader.ReadFile(this.cg_sm_lines_file);

                    // Test all UPPER_FP{}_CROSSING limits exist in mainspec
                    for (int fpIndex = this.fp_Lower_Limit; fpIndex <= this.fp_Upper_Limit; fpIndex++)
                    {
                        string limitName = string.Format("UPPER_FP{0}_CROSSING", fpIndex);
                        ParamLimit limit = this.mainSpec.GetParamLimit(limitName);

                        int upperFpCrossingPoint = this.frontSweepPointsCount * fpIndex - 1;

                        // Test this limit if exist in mainspec
                        if (limit != null)
                        {
                            // Get the rear point of this fp crossing point from the overall map result
                            for (int i = 0; i < lines.Count; i++)
                            {
                                if (lines[i].Length < 4) {
                                    continue;
                                }

                                // The first rear point is the point of fp crossing point
                                if (lines[i][1] == "Upper" 
                                    && int.Parse(lines[i][2]) == upperFpCrossingPoint)
                                {
                                    int rearPoint = int.Parse(lines[i][3]);
                                    testResults.Add(limitName, rearPoint);
                                    break;
                                }
                            }

                            if (!testResults.ContainsKey(limitName))
                            {
                                testResults.Add(limitName, 999);
                            }
                        }
                    }                    
                }
            }
        }

        /// <summary>
        /// Test limit DELTA_FP{}_CROSSING - chongjian.liang 2013.9.10
        /// </summary>
        /// <param name="testResults">Containing the limit names and the test results</param>
        private void TestLimit_DELTA_FP_CROSSING(Dictionary<string, double> testResults_UPPER_FP_CROSSING, Dictionary<string, double> cocResults_UPPER_FP_CROSSING, out Dictionary<string, double> testResults)
        {
            testResults = new Dictionary<string, double>();

            foreach (string limitName_UPPER_FP_CROSSING in testResults_UPPER_FP_CROSSING.Keys)
            {
                string limitName = limitName_UPPER_FP_CROSSING.Replace("UPPER", "DELTA");

                if (testResults_UPPER_FP_CROSSING[limitName_UPPER_FP_CROSSING] != 999
                    && cocResults_UPPER_FP_CROSSING.ContainsKey(limitName_UPPER_FP_CROSSING))
                {
                    double limitValue = testResults_UPPER_FP_CROSSING[limitName_UPPER_FP_CROSSING] - cocResults_UPPER_FP_CROSSING[limitName_UPPER_FP_CROSSING];

                    testResults.Add(limitName, limitValue);
                }
                else
                {
                    testResults.Add(limitName, 999);
                }
            }
        }

        private void SetFcu(string[] currents)
        {
            fcu.IRear_mA = float.Parse(currents[0]);
            int frontPairNbr = int.Parse(currents[1]);
            float fs_constant = float.Parse(currents[2]);
            float fs_nonConstant = float.Parse(currents[3]);

            fcu.SetFrontPairCurrent_mA(frontPairNbr, fs_constant, fs_nonConstant);
        }

        /// <summary>
        /// chongjian.liang 2016.08.30
        /// </summary>
        private void SetFcu(string[] currents, float phase_mA)
        {
            int frontPairNbr = int.Parse(currents[1]);
            float fs_constant = float.Parse(currents[2]);
            float fs_nonConstant = float.Parse(currents[3]);

            fcu.SetFrontPairCurrent_mA(frontPairNbr, fs_constant, fs_nonConstant);
            fcu.IRear_mA = float.Parse(currents[0]);
            fcu.IPhase_mA = phase_mA;

            System.Threading.Thread.Sleep(10);
        }

        private void DoPowerLevelingForeachSM(ITestEngine engine, double Min_target_power, double Max_target_power, double MinISoaForMapping_mA, double MaxISoaForMapping_mA)
        {
            engine.SendStatusMsg("do SOA power leveling.");
            double Soa_PowerLeveling_mA = fcu.ISoa_mA;
            double Last_Soa_PowerLeveling_mA = Soa_PowerLeveling_mA;
            double laserPower = fcu.Fix_Port_mA(false);
            double Last_laser_power = 0;
            double Isoa_mA_ReferencePower_Slope = 80.0;
            int Temp_Count = 0;
            int Max_Count = 20;
            ButtonId response = ButtonId.No;

            Last_laser_power = laserPower;
            do
            {
                while (((laserPower < Min_target_power)||(laserPower > Max_target_power)) && Temp_Count < Max_Count)
                {

                    Soa_PowerLeveling_mA = (float)(Soa_PowerLeveling_mA + ((Min_target_power+Max_target_power)/2.0 - laserPower) * Isoa_mA_ReferencePower_Slope);
                    Soa_PowerLeveling_mA = Math.Max(MinISoaForMapping_mA, Soa_PowerLeveling_mA);
                    Soa_PowerLeveling_mA = Math.Min(MaxISoaForMapping_mA, Soa_PowerLeveling_mA);

                    fcu.ISoa_mA = (float)Soa_PowerLeveling_mA;
                    System.Threading.Thread.Sleep(500);
                    laserPower = fcu.Fix_Port_mA(false);

                    if (laserPower != Last_laser_power)
                        Isoa_mA_ReferencePower_Slope =
                            (Soa_PowerLeveling_mA - Last_Soa_PowerLeveling_mA) / (laserPower - Last_laser_power);
                    if (Soa_PowerLeveling_mA == MinISoaForMapping_mA || Soa_PowerLeveling_mA == MaxISoaForMapping_mA)
                        break;
                    Last_Soa_PowerLeveling_mA = Soa_PowerLeveling_mA;
                    Last_laser_power = laserPower;
                    Temp_Count++;
                    engine.SendStatusMsg("do SOA Leveling,SOA = " + Soa_PowerLeveling_mA.ToString() + " ...");
                }
                if (((Soa_PowerLeveling_mA == MaxISoaForMapping_mA) && (laserPower < Min_target_power)) || ((Soa_PowerLeveling_mA == MinISoaForMapping_mA) && (laserPower > Max_target_power)))
                {
                    string messageStr = "Can't reach target power , please check firber tail and facet!Do you want to retry? 光功率偏低，是否检查光路及光纤端面并重试？";
                    response = overallMapEngine.ShowYesNoUserQuery(messageStr);

                    if (response != ButtonId.Yes) // Forbid the test to proceed in power low case. - chongjian.liang 2014.11.25
                    {
                        //this.failModeCheck.RaiseError_Mod_NonParamFail(engine, "Can't reach target power. 光功率偏低", FailModeCategory_Enum.UN);
                    }
                }
                else
                    response = ButtonId.No;

                if (response == ButtonId.Yes)
                {
                    Temp_Count = 0;
                    fcu.ISoa_mA = IsoaForMapping_mA;
                    laserPower = fcu.Fix_Port_mA(false);
                }
            } while (response == ButtonId.Yes);
            //return Soa_PowerLeveling_mA;
        }

        private bool PowerLev(ITestEngine engine, double target_power, double PowerOffset, double PowerTolerance, double MinISoaForMapping_mA, double MaxISoaForMapping_mA)
        {
            engine.SendStatusMsg("do SOA Leveling...");
            //double maxSoaInMapping_mA = 65.0;//Max SOA
            double Soa_PowerLeveling_mA = IsoaForMapping_mA;
            int tuningCount = 0;
            InstType_OpticalPowerMeter.MeterMode rawMode = OpmLaser.Mode;
            OpmLaser.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;
            double laserPower = OpmLaser.ReadPower();
            while ((Math.Abs(laserPower - (target_power - PowerOffset)) > PowerTolerance) && (Soa_PowerLeveling_mA < MaxISoaForMapping_mA) && (Soa_PowerLeveling_mA > MinISoaForMapping_mA))
            {
                tuningCount++;
                if (laserPower > target_power - PowerOffset)
                    Soa_PowerLeveling_mA--;
                else
                    Soa_PowerLeveling_mA++;

                fcu.ISoa_mA = (float)Soa_PowerLeveling_mA;

                System.Threading.Thread.Sleep(500);

                laserPower = OpmLaser.ReadPower();
                laserPower += power_offset;

                engine.SendStatusMsg("do SOA Leveling,SOA = " + Soa_PowerLeveling_mA.ToString() + " ...");

            }
            OpmLaser.Mode = rawMode;
            engine.SendStatusMsg("do SOA Levelig  finish...");
            if (Math.Abs(laserPower - (target_power - PowerOffset)) > PowerTolerance)
            {
                double realPower_dbm = laserPower - power_offset;
                engine.SendToGui("Can't reach target power.real power read from power meter is " + realPower_dbm.ToString() + ",power offset is:" + power_offset.ToString());
                this.failModeCheck.RaiseError_Mod_NonParamFail(engine, "Can't reach target power.real power read from power meter is " + realPower_dbm.ToString() + ",power offset is:" + power_offset.ToString(), FailModeCategory_Enum.UN);
                return false;
            }
            else 
            {
                return true;
            }

        }



        private int FindPossiblePotForSMAverageDAC(int targetDac, double Cal_mA, List<FCUMKI_DAC2mA_Cal_Parameter> Port)
        {
            double slope = 0;
            double offset = 0;
            int mid_pot = 0;
            double mid_mA = 0;
            double Delt_mA = 0;
            double Min_Delt_mA = 999;

            for (int i = 0; i < Port.Count; i++)
            {
                slope = Port[i].Slope;
                offset = Port[i].offset;
                mid_mA = targetDac * slope + offset;
                Delt_mA = Math.Abs(mid_mA - Cal_mA);
                if (Delt_mA < Min_Delt_mA)
                {
                    Min_Delt_mA = Math.Min(Min_Delt_mA, Math.Abs(mid_mA - Cal_mA));
                    mid_pot = Port[i].Pot;
                }
            }

            return mid_pot;
        }

        private double GeneratePowerCalibrateFile(string CalFileName)
        {
            double fiber_power_main;
            double fiber_power_branch;
            double power_offset;


            overallMapEngine.ShowContinueUserQuery("please move fiber to power meter directly!");
            fiber_power_main = OpmLaser.ReadPower();
            overallMapEngine.ShowContinueUserQuery("please move fiber back to laser!");
            fiber_power_branch = OpmLaser.ReadPower();

            power_offset = fiber_power_main - fiber_power_branch;
            //write power calibrate value to file

            StreamWriter sw = new StreamWriter(CalFileName);
            sw.WriteLine(power_offset.ToString());
            sw.Close();
            return power_offset;
        }

        private double DoMzTuneAndGenerateLVFile(string result_dir, string laserid, string time, TestParamConfigAccessor MzsweepSettings, int SM_Num)
        {
            //do MZ difference tune for Mapping -------Echo,15-04-2010

            double power_mw = 0, max_power_mw = 0, min_power_mw = double.MaxValue;
            int PointNumber = MzsweepSettings.GetIntParam("MzSweepNumberOfPoints");
            double temp;
            double I_left_mA, I_Right_mA;
            double Ctap_mA;

            double mzPercentStep = PointNumber / 100;


            //read left mode dark current and right mode dark current


            List<LVDataType> LVDataList = new List<LVDataType>();
            LVDataType LVData;

            List<double> power_data_mw = new List<double>();

            max_I_ImbDiffSweep_mA = Convert.ToDouble(MzsweepSettings.GetDoubleParam("MzCtrlINominal_mA").ToString());
            //max_I_Bias = 7; // 7mA
            double IStep = max_I_ImbDiffSweep_mA / (PointNumber + 1);
            double Delta_I_LR = 0;  // I_left_mA-I_Right_mA
            double Delta_I_LR_atMaxPow = max_I_ImbDiffSweep_mA; // I_left_mA-I_Right_mA current at max power position
            double Delta_I_LR_atMinPow = max_I_ImbDiffSweep_mA; // I_left_mA-I_Right_mA current at min power position

            I_left_mA = 0;
            I_Right_mA = max_I_ImbDiffSweep_mA;
            int progressPercentage = 0;
            DatumString guiString = new DatumString("Progress", "1");

            fcu.VLeftModBias_Volt = 0;
            fcu.VRightModBias_Volt = 0;

            for (int i = 0; i < PointNumber; i++)
            {
                I_left_mA = I_left_mA + IStep;
                I_Right_mA = I_Right_mA - IStep;

                //Delta_I_LR = Math.Abs(I_left_mA - I_Right_mA);

                //fcu.ImbLeft_mAbyAsic = float.Parse(I_left_mA.ToString());
                fcu.IimbLeft_mA = float.Parse(I_left_mA.ToString());
                fcu.IimbRight_mA = float.Parse(I_Right_mA.ToString());

                power_mw = OpmLaser.ReadPower();
                Ctap_mA = fcu.Fix_Port_Dac;

                power_data_mw.Add(power_mw);

                LVData.Lbias = I_left_mA;
                LVData.Rbias = I_Right_mA;
                LVData.power_mw = power_mw;
                LVData.Ctap_mA = Ctap_mA;
                LVDataList.Add(LVData);

                progressPercentage = (int)(i / mzPercentStep);
                guiString.Update(progressPercentage.ToString());
                overallMapEngine.SendToGui(guiString);

            }
            //ensure mz tune percentage is 100%
            guiString.Update("100");
            overallMapEngine.SendToGui(guiString);



            //find max current in both imbalcne arm


            int[] PeakIndexArry = Alg_FindFeature.FindIndexesForAllPeaks(power_data_mw.ToArray(), 5);
            //find min abs(left-right)
            double minAbsL_R = 999;
            double tempMinAbs;
            int maxImbCurrentIndex = 0;

            for (int i = 0; i < PeakIndexArry.Length; i++)
            {
                int PowerArrayIndex = PeakIndexArry[i];
                tempMinAbs = Math.Abs(LVDataList[PowerArrayIndex].Lbias - LVDataList[PowerArrayIndex].Rbias);
                if (tempMinAbs < minAbsL_R)
                {
                    minAbsL_R = tempMinAbs;
                    maxImbCurrentIndex = PowerArrayIndex;
                }
            }

            Mz_Peak_Imb_Left_mA = LVDataList[maxImbCurrentIndex].Lbias;
            Mz_Peak_Imb_Right_mA = LVDataList[maxImbCurrentIndex].Rbias;


            fcu.IimbLeft_mA = (float)(Mz_Peak_Imb_Left_mA);
            fcu.IimbRight_mA = (float)(Mz_Peak_Imb_Right_mA);

            //write mz tune bias voltage to file
            GenerateLVFile(LVDataList, result_dir, laserid, time, SM_Num);

            double max_power_at_imb_peak = LVDataList[maxImbCurrentIndex].power_mw;
            max_power_at_imb_peak = LVDataList[maxImbCurrentIndex].Ctap_mA;
            return max_power_at_imb_peak;


        }
        /// <summary>
        /// imb defferential sweep, 
        /// Left imb starts from iStart and end at iStop
        /// Right imb starts from iStop and end at iStart
        /// </summary>
        /// <param name="iStartDAC">Imb sweep start current</param>
        /// <param name="iStopDAC">Imb sweep stop current</param>
        /// <param name="nbrPoints">Number of points</param>
        /// <param name="currentSettledTime_mS">time has to wait to get a stable reading from instrument 
        /// after a new current apply on imb</param>
        /// <returns>
        /// Left imb current and DAC, .
        /// Right imb current and Dac
        /// left & Right RF Voltage
        /// Ctap & Tap ( if source exists) currents
        /// opotical power in mW   
        /// </returns>
        private void ImbDiffSweepWithAsicFcu(string result_dir, string laserid, string time, TestParamConfigAccessor MzsweepSettings, TestParamConfigAccessor OpticalCalConfig, int SM_Num,out double Power_output_mW,out double Power_input_mW)
        {
            List<LVDataType> LVDataList = new List<LVDataType>();
            LVDataType LVData;
            Power_output_mW = 0;
            Power_input_mW = 0;
            int nbrPoints = MzsweepSettings.GetIntParam("MzSweepNumberOfPoints");

            if (ParamManager.Conditions.IsThermalPhase)
            {
                max_I_ImbDiffSweep_mA = 2.0 * MzsweepSettings.GetDoubleParam("MzCtrlINominal_thermal_mA");
            }
            else
            {
                max_I_ImbDiffSweep_mA = 2.0 * MzsweepSettings.GetDoubleParam("MzCtrlINominal_mA");
            }
            double Power_offset_Max = OpticalCalConfig.GetDoubleParam("OpticalCal_MZ_Max");
            double Power_offset_Min = OpticalCalConfig.GetDoubleParam("OpticalCal_MZ_Min");
            int iStartDAC = fcu.mAToDacForLeftImb_Asic(0);
            int iStopDAC = fcu.mAToDacForLeftImb_Asic((float)max_I_ImbDiffSweep_mA);
            int delaytime_ms = 5;
            bool dataOk = true;
            double minLevel_dBm = -50;
            double[] uncalibratedPower_mW;
            double[] Ctap_mA;
            DatumString guiString = new DatumString("Progress", "1");
            ButtonId response = ButtonId.No;
            IInstType_DigitalIO outLine_Reference_Ctap = Optical_switch.GetDigiIoLine(IlmzOpticalSwitchLines.Etalon_Ref_DigiIoLine);//Jack.zhang PD control
            outLine_Reference_Ctap.LineState = IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine_State;//Jack,false for reference true for ctap
            if (nbrPoints < 2) throw new ArgumentException(
                 " can't carry a sweep with data points with " + nbrPoints.ToString());
            do
            {
                do
                {
                    PowerMeterWithTrigger.EnableInputTrigger(false);
                    PowerMeterWithTrigger.ConfigureDataLogging(nbrPoints, delaytime_ms / 1000.0);
                    PowerMeterWithTrigger.EnableInputTrigger(true);
                    PowerMeterWithTrigger.StartDataLogging();
                    System.Threading.Thread.Sleep(1000);
                    guiString.Update("0");
                    overallMapEngine.SendToGui(guiString);
                    fcu.AsicImbDiffTrig(iStartDAC, iStopDAC, nbrPoints, delaytime_ms);
                    System.Threading.Thread.Sleep(nbrPoints * delaytime_ms + 500);
                    uncalibratedPower_mW = PowerMeterWithTrigger.GetSweepData();
                    Ctap_mA = fcu.GetReferenceAfterMZsweep();
                    //find max current in both imbalcne arm
                    PowerMeterWithTrigger.StopDataLogging();
                    PowerMeterWithTrigger.EnableInputTrigger(false);
                    #region for Ag8163 need fix range when trigger
                    if (MzAnalysisWrapper.CheckForOverrange(uncalibratedPower_mW))
                    {
                        dataOk = false;
                        //modify by tim at2008-09-04 for Ag8163 NaN;

                        PowerMeterWithTrigger.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_mW;
                        PowerMeterWithTrigger.Range = PowerMeterWithTrigger.Range * 10;
                        if (double.IsNaN(PowerMeterWithTrigger.Range))
                        {
                            PowerMeterWithTrigger.Range = 10;
                        }
                        //end
                    }
                    else
                    {
                        // if underrange fix the data and continue
                        dataOk = true;
                        if (MzAnalysisWrapper.CheckForUnderrange(uncalibratedPower_mW))
                        {
                            uncalibratedPower_mW =
                                MzAnalysisWrapper.FixUnderRangeData(uncalibratedPower_mW, minLevel_dBm);
                        }
                    }
                    #endregion for Ag8163 need fix range when trigger
                }
                while (!dataOk);
                guiString.Update("100");
                overallMapEngine.SendToGui(guiString);
                // sweep imb arms

                double iStop = max_I_ImbDiffSweep_mA;
                double iStart = 0;
                double iStep = (iStop - iStart) / (nbrPoints - 1);
                for (int i = 0; i < nbrPoints; i++)
                {
                    LVData.Lbias = (iStart + i * iStep);
                    LVData.Rbias = (iStop - i * iStep);
                    LVData.power_mw = uncalibratedPower_mW[i];
                    LVData.Ctap_mA = Ctap_mA[i];
                    LVDataList.Add(LVData);
                }

                int[] PeakIndexArry = Alg_FindFeature.FindIndexesForAllPeaks(uncalibratedPower_mW, 5);
                int[] TroughIndexArry = Alg_FindFeature.FindIndexesForAllValleys(uncalibratedPower_mW, 5);
                if (PeakIndexArry.Length > 0 && TroughIndexArry.Length > 0)
                {
                    //find min abs(left-right)
                    double minAbsL_R = 999;
                    double tempMinAbs;
                    int maxImbCurrentIndex = 0;
                    
                    #region measure Ctap_mA on trough point
                    for (int i = 0; i < TroughIndexArry.Length; i++)
                    {
                        int PowerArrayIndex = TroughIndexArry[i];
                        tempMinAbs = Math.Abs(LVDataList[PowerArrayIndex].Lbias - LVDataList[PowerArrayIndex].Rbias);
                        if (tempMinAbs < minAbsL_R)
                        {
                            minAbsL_R = tempMinAbs;
                            maxImbCurrentIndex = PowerArrayIndex;
                        }
                    }

                    Mz_Trough_Imb_Left_mA = LVDataList[maxImbCurrentIndex].Lbias;
                    Mz_Trough_Imb_Right_mA = LVDataList[maxImbCurrentIndex].Rbias;

                    fcu.IimbLeft_mA = (float)(Mz_Trough_Imb_Left_mA);
                    fcu.IimbRight_mA = (float)(Mz_Trough_Imb_Right_mA);
                    #endregion measure Ctap_mA on trough point
                    double Ctap_mA_imb_peak = fcu.Fix_Port_mA(false);
                    Power_output_mW = Ctap_mA_imb_peak / 0.35;//Ctap efficiency about 0.35+-0.1mA/mW for C_band;0.15mA/mW for L band;Jack.zhang 2013-01-16
                    minAbsL_R = 999;

                    #region measure power_mW on peak point
                    for (int i = 0; i < PeakIndexArry.Length; i++)
                    {
                        int PowerArrayIndex = PeakIndexArry[i];
                        tempMinAbs = Math.Abs(LVDataList[PowerArrayIndex].Lbias - LVDataList[PowerArrayIndex].Rbias);
                        if (tempMinAbs < minAbsL_R)
                        {
                            minAbsL_R = tempMinAbs;
                            maxImbCurrentIndex = PowerArrayIndex;
                        }
                    }

                    Mz_Peak_Imb_Left_mA = LVDataList[maxImbCurrentIndex].Lbias;
                    Mz_Peak_Imb_Right_mA = LVDataList[maxImbCurrentIndex].Rbias;

                    fcu.IimbLeft_mA = (float)(Mz_Peak_Imb_Left_mA);
                    fcu.IimbRight_mA = (float)(Mz_Peak_Imb_Right_mA);
                    #endregion measure power_mW on peak point
                    double max_power_at_imb_peak = PowerMeterWithTrigger.ReadPower();
                    outLine_Reference_Ctap.LineState = IlmzOpticalSwitchLines.Etalon_Ref_DigiIoLine_State;//Jack,false for reference true for ctap
                    System.Threading.Thread.Sleep(500);
                    double Reference_mA = fcu.Fix_Port_mA(false);
                    if (Math.Abs(FreqCalByEtalon.Reference_PD_Responsivity.Slope) > 0.35)//temp to avoid etaloncal dtat with wrong power data. jack.zhang 2012-11-19
                        Power_input_mW = Reference_mA * 0.15;
                    else
                        //Power_input_mW =( Reference_mA-FreqCalByEtalon.Reference_PD_Responsivity.YIntercept)/FreqCalByEtalon.Reference_PD_Responsivity.Slope;
                        Power_input_mW = Reference_mA / FreqCalByEtalon.Average_Reference_PD_Responsivity;
                    power_offset = Power_input_mW / Power_output_mW;
                    //if (power_offset > Power_offset_Max || power_offset < Power_offset_Min)
                    //{
                    //    string messageStr = "power offset is incorrect , please check firber tail adaptor and facet!Do you want to retry? 光功率衰减不正常，是否检查光路及光纤端面并重试？";
                    //    response = overallMapEngine.ShowYesNoUserQuery(messageStr);
                    //}
                    //write mz tune bias voltage to file
                }
                else
                {
                    string messageStr = "Mz Imb Sweep data is incorrect , please check if MZ is OK! Mz电流扫描不正常，请检查器件MZ是否正常！";
                    overallMapEngine.ShowContinueUserQuery(messageStr);
                    fcu.CloseAllAsic();//stop the test if MZ broken because Fix DAC will be zero after close asic. Jack.Zhang 2012-06-12
                }
                GenerateLVFile(LVDataList, result_dir, laserid, time, SM_Num);
            } while (response == ButtonId.Yes);
            //outLine_Reference_Ctap.LineState = IlmzOpticalSwitchLines.Etalon_Ref_DigiIoLine_State;//Jack,false for reference true for ctap
        }
        private void GenerateLVFile(List<LVDataType> LVDataList, string file_dir, string laser_id, string time, int SM_Num)
        {

            string filename = "SM" + SM_Num.ToString() + "_MZTuning_" + laser_id + "_" + time + ".csv";
            fileFullPath = Path.Combine(file_dir, filename);

            if (!Directory.Exists(file_dir))
            {
                Directory.CreateDirectory(file_dir);
            }

            if (File.Exists(fileFullPath))
            {
                File.Delete(fileFullPath);
            }


            StreamWriter sw = new StreamWriter(fileFullPath);

            sw.WriteLine("Left_Imb_mA,Right_Imb_mA,Left-Right_mA,Power_mW,Ctap_mA");
            for (int i = 0; i < LVDataList.Count; i++)
            {
                double Delta = LVDataList[i].Lbias - LVDataList[i].Rbias; // left imbalance current substract right imbalance current 
                sw.WriteLine(LVDataList[i].Lbias.ToString() + "," + LVDataList[i].Rbias.ToString() + "," + Delta.ToString() + "," + LVDataList[i].power_mw.ToString() + "," + LVDataList[i].Ctap_mA.ToString());

            }
            sw.WriteLine("");
            sw.WriteLine("Max left imbalance current:" + Mz_Peak_Imb_Left_mA.ToString() + "mA");
            sw.WriteLine("Max right imbalance current:" + Mz_Peak_Imb_Right_mA.ToString() + "mA");
            sw.WriteLine("Min left imbalance current:" + Mz_Trough_Imb_Left_mA.ToString() + "mA");
            sw.WriteLine("Min right imbalance current:" + Mz_Trough_Imb_Right_mA.ToString() + "mA");
            sw.WriteLine("Power Offset:" + power_offset.ToString());
            sw.WriteLine("Fcu soa current:" + fcu.ISoa_mA.ToString() + "mA");

            sw.Close();
        }

        void fcu_OverallmapProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            int value = (int)(e.ProgressPercentage * 1.0 / totalPercentage * 100);
            if (value > 100)
                value = 100;
            if (value < 0)
                value = 0;
            overallMapEngine.SendToGui(value);
        }

        public Type UserControl
        {
            get { return (typeof(Mod_OverallMapGui)); }
        }

        private struct LVDataType
        {
            public double Lbias;
            public double Rbias;
            public double power_mw;
            public double Ctap_mA;
        }
    }
}

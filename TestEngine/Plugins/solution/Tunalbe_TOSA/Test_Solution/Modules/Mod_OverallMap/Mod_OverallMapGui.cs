// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// Mod_CGGui.cs
//
// Author: tommy.yu, 2009
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Bookham.TestEngine.Framework.InternalData;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Module Template GUI User Control
    /// </summary>
    public partial class Mod_OverallMapGui : Bookham.TestEngine.Framework.Messages.ManagedCtlBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public Mod_OverallMapGui()
        {
            /* Call designer generated code. */
            InitializeComponent();

            /* TODO - Write constructor code here. */
        }

        /// <summary>
        /// Incoming message event handler
        /// </summary>
        /// <param name="payload">Message Payload</param>
        /// <param name="inMsgSeq">Incoming message sequence number</param>
        /// <param name="respSeq">Outgoing message sequence number</param>
        private void ModuleGui_MsgReceived(object payload, long inMsgSeq, long respSeq)
        {
            //if (payload.GetType() == typeof(String))
            //{
            //    this.labelShowInfo.Text = (String)payload;
            //}
            
            if (payload.GetType() == typeof(int))
            {
                this.overallMapProgressBar.Value =  (int)payload;
                this.percentlabe.Text = ((int)payload).ToString() + "%";
            }
            if (payload.GetType() == typeof(DatumString))
            {
                //this.MzTuneProgressBar.Value = (int)payload;
                DatumString procentage = (DatumString)payload;
                this.MzTuneProgressBar.Value = Int32.Parse(procentage.Value.ToString());
                this.MzPercent.Text = procentage.Value.ToString() + "%";
                
            }
        }
    }
}

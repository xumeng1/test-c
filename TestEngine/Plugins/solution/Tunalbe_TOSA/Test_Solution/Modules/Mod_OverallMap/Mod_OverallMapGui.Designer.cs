// [Copyright]
//
// Bookham Test Engine
// Mod_CG
//
// Bookham.TestSolution.TestModules/Mod_CGGui
// 
// Author: paul.annetts
// Design: TODO

namespace Bookham.TestSolution.TestModules
{
    partial class Mod_OverallMapGui
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelStageName = new System.Windows.Forms.Label();
            this.overallMapProgressBar = new System.Windows.Forms.ProgressBar();
            this.percentlabe = new System.Windows.Forms.Label();
            this.MzTuneProgressBar = new System.Windows.Forms.ProgressBar();
            this.labelMZTune = new System.Windows.Forms.Label();
            this.MzPercent = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelStageName
            // 
            this.labelStageName.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStageName.Location = new System.Drawing.Point(45, 131);
            this.labelStageName.Name = "labelStageName";
            this.labelStageName.Size = new System.Drawing.Size(281, 41);
            this.labelStageName.TabIndex = 1;
            this.labelStageName.Text = "Overall Mapping";
            // 
            // overallMapProgressBar
            // 
            this.overallMapProgressBar.Location = new System.Drawing.Point(50, 185);
            this.overallMapProgressBar.Name = "overallMapProgressBar";
            this.overallMapProgressBar.Size = new System.Drawing.Size(416, 45);
            this.overallMapProgressBar.TabIndex = 2;
            // 
            // percentlabe
            // 
            this.percentlabe.AutoSize = true;
            this.percentlabe.Location = new System.Drawing.Point(487, 201);
            this.percentlabe.Name = "percentlabe";
            this.percentlabe.Size = new System.Drawing.Size(21, 13);
            this.percentlabe.TabIndex = 3;
            this.percentlabe.Text = "0%";
            // 
            // MzTuneProgressBar
            // 
            this.MzTuneProgressBar.Location = new System.Drawing.Point(50, 74);
            this.MzTuneProgressBar.Name = "MzTuneProgressBar";
            this.MzTuneProgressBar.Size = new System.Drawing.Size(416, 39);
            this.MzTuneProgressBar.TabIndex = 4;
            // 
            // labelMZTune
            // 
            this.labelMZTune.AutoSize = true;
            this.labelMZTune.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMZTune.Location = new System.Drawing.Point(47, 36);
            this.labelMZTune.Name = "labelMZTune";
            this.labelMZTune.Size = new System.Drawing.Size(100, 20);
            this.labelMZTune.TabIndex = 5;
            this.labelMZTune.Text = "do MZ tune";
            // 
            // MzPercent
            // 
            this.MzPercent.AutoSize = true;
            this.MzPercent.Location = new System.Drawing.Point(487, 85);
            this.MzPercent.Name = "MzPercent";
            this.MzPercent.Size = new System.Drawing.Size(21, 13);
            this.MzPercent.TabIndex = 6;
            this.MzPercent.Text = "0%";
            // 
            // Mod_OverallMapGui
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.MzPercent);
            this.Controls.Add(this.labelMZTune);
            this.Controls.Add(this.MzTuneProgressBar);
            this.Controls.Add(this.percentlabe);
            this.Controls.Add(this.overallMapProgressBar);
            this.Controls.Add(this.labelStageName);
            this.Name = "Mod_OverallMapGui";
            this.MsgReceived += new Bookham.TestEngine.Framework.Messages.ManagedCtlBase.MsgReceivedDlg(this.ModuleGui_MsgReceived);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelStageName;
        private System.Windows.Forms.ProgressBar overallMapProgressBar;
        private System.Windows.Forms.Label percentlabe;
        private System.Windows.Forms.ProgressBar MzTuneProgressBar;
        private System.Windows.Forms.Label labelMZTune;
        private System.Windows.Forms.Label MzPercent;
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.Framework.Limits;
using Bookham.TestLibrary.Utilities;
using Bookham.TestEngine.Config;
using Bookham.TestSolution.IlmzCommonData;

namespace Bookham.TestSolution.TestPrograms
{
    internal class Prog_ILMZ_QualInfo
    {
        internal Specification MainSpec;
        //internal SpecList SupermodeSpecs;

        internal IlmzQualTestConds TestConditions;

        internal progIlmzQualInstruments Instrs;

        internal TestParamConfigAccessor TestParamsConfig;

        internal TestParamConfigAccessor MapParamsConfig;

        internal TestParamConfigAccessor LocalTestParamsConfig;

        internal TestParamConfigAccessor OpticalSwitchConfig;

        internal TempTableConfigAccessor TempConfig;

        internal TestParamConfigAccessor lightTowerConfig;      

        internal FinalTestStage TestStage;

        internal DsdbrDriveInstruments DsdbrInstrsUsed;
    }
}

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Bookham.TestSolution.TestPrograms
{
    internal partial class IlmzPowerCalDataCtrl : UserControl
    {
        Prog_ILMZ_QualGui parent;
        public IlmzPowerCalDataCtrl(Prog_ILMZ_QualGui  parent)
        {
            this .parent  = parent;
            InitializeComponent();
        }

        private void buttonFindFile_Click(object sender, EventArgs e)
        {
            DialogResult res = openFileDialog1.ShowDialog(this);
            if (res == DialogResult.OK)
            {
                textBoxTcmzSettingsFile.Text = openFileDialog1.FileName;
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            GuiMsgs.IlmzPowerCalDataResponse resp = new GuiMsgs.IlmzPowerCalDataResponse(textBoxTcmzSettingsFile.Text);
            parent.SendToWorker(resp);
            parent.CtrlFinished();
        }       

    }
}

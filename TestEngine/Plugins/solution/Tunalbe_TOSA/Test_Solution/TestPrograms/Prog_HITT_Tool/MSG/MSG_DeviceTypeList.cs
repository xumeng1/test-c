// Author: chongjian.liang 2016.07.11
using System.Collections.Generic;

namespace Bookham.TestSolution.TestPrograms
{
    class MSG_DeviceTypeList
    {
        public List<string> DeviceTypeList;

        public MSG_DeviceTypeList(List<string> deviceTypeList)
        {
            this.DeviceTypeList = deviceTypeList;
        }
    }
}

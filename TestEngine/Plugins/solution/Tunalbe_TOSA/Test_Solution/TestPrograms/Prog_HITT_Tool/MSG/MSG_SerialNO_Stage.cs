// Author: chongjian.liang 2016.07.11

namespace Bookham.TestSolution.TestPrograms
{
    class MSG_SerialNO_Stage
    {
        public string SerialNO;
        public string Stage;

        public MSG_SerialNO_Stage(string serialNO, string stage)
        {
            this.SerialNO = serialNO;
            this.Stage = stage;
        }
    }


    public class MSG_PhaseCutScan
    {
        public double currentFrom;
        public double currentTo;
        public double currentStep;
        public string sn;
        public MSG_PhaseCutScan(double phaseFrom, double phaseTo, double phaseSetp,string serialNumber)
        {
            this.currentFrom = phaseFrom;
            this.currentTo = phaseTo;
            this.currentStep = phaseSetp;
            this.sn = serialNumber;
        }
    }

    public class MSG_RearCutScan
    {
        public double currentFrom;
        public double currentTo;
        public double currentStep;
        public string sn;
        public MSG_RearCutScan(double phaseFrom, double phaseTo, double phaseSetp, string serialNumber)
        {
            this.currentFrom = phaseFrom;
            this.currentTo = phaseTo;
            this.currentStep = phaseSetp;
            this.sn = serialNumber;
        }
    }
}

// Author: chongjian.liang 2016.07.11

namespace Bookham.TestSolution.TestPrograms
{
    class MSG_SerialNO_DeviceType_Stage_FileKey
    {
        public string SerialNO;
        public string DeviceType;
        public string Stage;
        public string FileKey;

        public MSG_SerialNO_DeviceType_Stage_FileKey(string serialNO, string deviceType, string stage, string fileKey)
        {
            this.SerialNO = serialNO;
            this.DeviceType = deviceType;
            this.Stage = stage;
            this.FileKey = fileKey;
        }
    }
}

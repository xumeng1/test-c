// Author: chongjian.liang 2016.07.11

namespace Bookham.TestSolution.TestPrograms
{
    class MSG_DeviceType
    {
        public string DeviceType;

        public MSG_DeviceType(string deviceType)
        {
            this.DeviceType = deviceType;
        }
    }
}

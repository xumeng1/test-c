// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestControl
//
// LoadBatchDialogueCtl.cs
//
// Author: alice.huang, 2009
// Design: [Reference design documentation

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.PluginInterfaces.MES;

namespace Bookham.TestSolution.TestControl
{
    /// <summary>
    /// Test Control Plug-in Template Batch View User Control
    /// </summary>
    public partial class LoadBatchDialogueCtl : Bookham.TestEngine.Framework.Messages.ManagedCtlBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public LoadBatchDialogueCtl()
        {
            /* Call designer generated code. */
            InitializeComponent();
        }
        /// <summary>
        ///  loading action: initialise Gui information
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            lblMesMessage.Text = " Waiting DUT SN Input ...";
            txtSN.Focus();
        }

        /// <summary>
        /// Process recieved messages from the worker thread.
        /// </summary>
        /// <param name="payload">Message Payload</param>
        /// <param name="inMsgSeq">Incoming sequence number</param>
        /// <param name="respSeq">Response sequence number</param>
        private void LoadBatchDialogueCtl_MsgReceived(object payload, long inMsgSeq, long respSeq)
        {
            //TODO: Process messages from the Test Control Plug-in worker thread.
            if (payload.GetType() == typeof(MesLoadDeviceMsg))
            {
                MesLoadDeviceMsg batchLoadRsp = (MesLoadDeviceMsg)payload;

                lblMesMessage.Text = batchLoadRsp.MesMessage;
                btnLoadBatch.Enabled = batchLoadRsp.EnableLoadDutInformation;
                if (batchLoadRsp.NeedDutInfoInput)
                {
                    txtSN.SelectAll();
                    txtSN.Focus();
                }
            }
        }

        private void btnLoadBatch_Click(object sender, EventArgs e)
        {
            if (txtSN.Text.Trim().Length >= 6)
            {
                DutLoadInfo dutmsg = new DutLoadInfo();
                dutmsg.DutLotID = txtSN.Text.Trim();
                sendToWorker(dutmsg);
            }
        }

        private void txtSN_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar == '\n' ) && (e .KeyChar =='\r') ) btnLoadBatch.Focus();
        }

       
    }
}

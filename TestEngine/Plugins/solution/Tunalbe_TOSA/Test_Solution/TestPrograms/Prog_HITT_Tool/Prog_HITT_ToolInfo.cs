using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.Framework.Limits;
using Bookham.TestLibrary.Utilities;
using Bookham.TestSolution.ILMZFinalData;
using Bookham.TestSolution.IlmzCommonUtils;
using Bookham.TestEngine.Config;
using Bookham.TestSolution.IlmzCommonData;

namespace Bookham.TestSolution.TestPrograms
{
    internal class Prog_HITT_ToolInfo
    {
        internal TestParamConfigAccessor TestParamsConfig;

        internal TestParamConfigAccessor LocalTestParamsConfig;

        internal TestParamConfigAccessor OpticalSwitchConfig;

        internal TestParamConfigAccessor MapParamsConfig;

        internal TempTableConfigAccessor TempConfig;

        internal ConfigDataAccessor InstrumentsCalData;
        
        internal double TecDsdbr_SensorTemperatureSetPoint_C;
        internal double TecCase_SensorTemperatureSetPoint_C;
            
        internal List<string> PartCodeList;

        internal string FREQ_BAND = "C";
        internal double ITU_SPACING_GHz = 50;
    }
}

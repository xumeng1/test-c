// Author: chongjian.liang 2016.07.08

using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.Specialized;
using Bookham.TestEngine.Framework.Limits;
using Bookham.TestEngine.Config;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.PluginInterfaces.ExternalData;
using Bookham.TestLibrary.Utilities;
using Bookham.TestSolution.ILMZFinalData;
using Bookham.TestSolution.IlmzCommonUtils;
using Bookham.TestSolution.IlmzCommonData;

namespace Bookham.TestSolution.TestPrograms
{
    internal class Prog_HITT_ToolData
    {
        private ITestEngineInit engine;

        public Prog_HITT_ToolData(ITestEngineInit engine)
        {
            this.engine = engine;
        }

        public Dictionary<string, TargetFileChannel> GetChannelsFromTargetFile(MSG_SerialNO_DeviceType_Stage_FileKey serialNO_DeviceType_Stage_FileKey)
        {
            string filePath = this.GetLatestResultFilePath(serialNO_DeviceType_Stage_FileKey);

            return this.GetTargetFileChannels(filePath);
        }

        public string GetLatestResultDeviceType(MSG_SerialNO_Stage serialNO_Stage)
        {
            return this.GetLatestResultDeviceType(serialNO_Stage.SerialNO, serialNO_Stage.Stage);
        }

        public string GetLatestResultDeviceType(string serialNO, string stage)
        {
            IDataRead dataRead = engine.GetDataReader("PCAS_SHENZHEN");

            StringDictionary keys = new StringDictionary();
            keys.Add("SCHEMA", "HIBERDB");
            keys.Add("TEST_STAGE", stage);
            keys.Add("SERIAL_NO", serialNO);

            DatumList result = null;

            try
            {
                result = dataRead.GetLatestResults(keys, false, "UPPER(TEST_STATUS) LIKE '%PASS%'");
            }
            catch (Exception e)
            {
                this.engine.ShowContinueUserQuery(string.Format("Invalid \"{0}\" stage result for {1}.\n{2}", stage, serialNO, e.Message));

                return null;
            }

            if (result == null
                || result.Count == 0)
            {
                this.engine.ShowContinueUserQuery(string.Format("Missing passed \"{0}\" stage result for {1}.", stage, serialNO));

                ButtonId loadFailedResultAnswer = this.engine.ShowYesNoUserQuery(string.Format("Would you like to get the device type from the lastest failed \"{0}\" stage result for {1}.", stage, serialNO));

                if (loadFailedResultAnswer == ButtonId.Yes)
                {
                    result = dataRead.GetLatestResults(keys, false, "UPPER(TEST_STATUS) LIKE '%FAIL%'");

                    if (result == null
                        || result.Count == 0)
                    {
                        this.engine.ShowContinueUserQuery(string.Format("Missing failed \"{0}\" stage result for {1}.", stage, serialNO));
                        
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }

            if (result.IsPresent("PRODUCT_CODE"))
            {
                return result.ReadString("PRODUCT_CODE");
            }
            else if (result.IsPresent("PART_CODE"))
            {
                return result.ReadString("PART_CODE");
            }
            else if (result.IsPresent("DEVICE_TYPE"))
            {
                return result.ReadString("DEVICE_TYPE");
            }

            this.engine.ShowContinueUserQuery(string.Format("Missing device type from \"{0}\" stage result for {1}.", stage, serialNO));

            return null;
        }

        private string GetLatestResultFilePath(MSG_SerialNO_DeviceType_Stage_FileKey serialNO_DeviceType_Stage_FileKey)
        {
            return this.GetLatestResultFilePath(serialNO_DeviceType_Stage_FileKey.SerialNO, serialNO_DeviceType_Stage_FileKey.DeviceType, serialNO_DeviceType_Stage_FileKey.Stage, serialNO_DeviceType_Stage_FileKey.FileKey);
        }

        private string GetLatestResultFilePath(string serialNO, string deviceType, string stage, string fileKey)
        {
            IDataRead dataRead = engine.GetDataReader("PCAS_SHENZHEN");

            StringDictionary keys = new StringDictionary();
            keys.Add("SCHEMA", "HIBERDB");
            keys.Add("TEST_STAGE", stage);
            keys.Add("DEVICE_TYPE", deviceType);
            keys.Add("SERIAL_NO", serialNO);

            DatumList result = null;

            try
            {
                result = dataRead.GetLatestResults(keys, true, "UPPER(TEST_STATUS) LIKE '%PASS%'");
            }
            catch (Exception e)
            {
                this.engine.ShowContinueUserQuery(string.Format("Invalid \"{0}\" stage result for {1} {2}.\n{3}", stage, serialNO, deviceType, e.Message));

                return null;
            }

            if (result == null
                || result.Count == 0)
            {
                this.engine.ShowContinueUserQuery(string.Format("Missing passed \"{0}\" stage result for {1} {2}.", stage, serialNO, deviceType));

                ButtonId loadFailedResultAnswer = this.engine.ShowYesNoUserQuery(string.Format("Would you like to load the {0} file from the latest failed {1} stage result for {2} {3}.", fileKey, stage, serialNO, deviceType));

                if (loadFailedResultAnswer == ButtonId.Yes)
                {
                    result = dataRead.GetLatestResults(keys, true, "UPPER(TEST_STATUS) LIKE '%FAIL%'");

                    if (result == null
                        || result.Count == 0)
                    {
                        this.engine.ShowContinueUserQuery(string.Format("Missing failed \"{0}\" stage result for {1} {2}.", stage, serialNO, deviceType));
                        
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }

            if (result.IsPresent(fileKey))
            {
                string filePath = result.ReadFileLinkFullPath(fileKey);

                if (!string.IsNullOrEmpty(filePath)
                    && File.Exists(filePath))
                {
                    return filePath;
                }
            }

            this.engine.ShowContinueUserQuery(string.Format("Missing {0} file from {1} stage result for {2} {3}.", fileKey, stage, serialNO, deviceType));

            return null;
        }

        private Dictionary<string, TargetFileChannel> GetTargetFileChannels(string targetFilePath)
        {
            if (targetFilePath == null)
            {
                return null;
            }

            Dictionary<string, TargetFileChannel> targetFileChannelDict = new Dictionary<string, TargetFileChannel>();

            using (CsvReader csvReader = new CsvReader())
            {
                List<string[]> fileContext = csvReader.ReadFile(targetFilePath);

                for (int i = 0; i < 3; i++)
                {
                    fileContext.RemoveAt(0);
                }

                int lineIndex = 0;

                foreach (string[] fileLine in fileContext)
                {
                    if (Enum.GetNames(typeof(TARGET_FILE_COLUMN)).Length > fileLine.Length)
                    {
                        continue;
                    }

                    TargetFileChannel channel = new TargetFileChannel();

                    try
                    {
                        channel.DsdbrSetup = new DsdbrChannelSetup();
                        channel.DsdbrSetup.FrontPair = int.Parse(fileLine[(int)TARGET_FILE_COLUMN.FrontSectionPair]);
                        channel.DsdbrSetup.IFsFirst_mA = double.Parse(fileLine[(int)TARGET_FILE_COLUMN.IFsConst_mA]);
                        channel.DsdbrSetup.IFsSecond_mA = double.Parse(fileLine[(int)TARGET_FILE_COLUMN.IFsNonConst_mA]);
                        channel.DsdbrSetup.IGain_mA = double.Parse(fileLine[(int)TARGET_FILE_COLUMN.IGain_mA]);
                        channel.DsdbrSetup.IPhase_mA = double.Parse(fileLine[(int)TARGET_FILE_COLUMN.IPhaseITU_mA]);
                        channel.DsdbrSetup.IRear_mA = double.Parse(fileLine[(int)TARGET_FILE_COLUMN.IRear_mA]);
                        channel.DsdbrSetup.IRearSoa_mA = double.Parse(fileLine[(int)TARGET_FILE_COLUMN.IRearSoa_mA]);
                        channel.DsdbrSetup.ISoa_mA = double.Parse(fileLine[(int)TARGET_FILE_COLUMN.ISoa_mA]);

                        channel.MzSetup = new MzData();
                        channel.MzSetup.LeftArmImb_Dac = double.NaN;
                        channel.MzSetup.LeftArmImb_mA = double.Parse(fileLine[(int)TARGET_FILE_COLUMN.MzLeftArmImb_mA]);
                        channel.MzSetup.LeftArmMod_Min_V = double.Parse(fileLine[(int)TARGET_FILE_COLUMN.MzLeftArmModMinima_V]);
                        channel.MzSetup.LeftArmMod_Peak_V = double.Parse(fileLine[(int)TARGET_FILE_COLUMN.MzLeftArmModPeak_V]);
                        channel.MzSetup.LeftArmMod_Quad_V = double.Parse(fileLine[(int)TARGET_FILE_COLUMN.MzLeftArmModQuad_V]);
                        channel.MzSetup.RightArmImb_Dac = double.NaN;
                        channel.MzSetup.RightArmImb_mA = double.Parse(fileLine[(int)TARGET_FILE_COLUMN.MzRightArmImb_mA]);
                        channel.MzSetup.RightArmMod_Min_V = double.Parse(fileLine[(int)TARGET_FILE_COLUMN.MzRightArmModMinima_V]);
                        channel.MzSetup.RightArmMod_Peak_V = double.Parse(fileLine[(int)TARGET_FILE_COLUMN.MzRightArmModPeak_V]);
                        channel.MzSetup.RightArmMod_Quad_V = double.Parse(fileLine[(int)TARGET_FILE_COLUMN.MzRightArmModQuad_V]);

                        channel.TestResult = new TestResult();
                        channel.TestResult.IrxLock_mA = double.Parse(fileLine[(int)TARGET_FILE_COLUMN.IrxLock_mA]);
                        channel.TestResult.ItuChannelIndex = int.Parse(fileLine[(int)TARGET_FILE_COLUMN.ItuChannelIndex]);
                        channel.TestResult.ItuFreq_GHz = double.Parse(fileLine[(int)TARGET_FILE_COLUMN.ItuFreq_GHz]);
                        channel.TestResult.ItxLock_mA = double.Parse(fileLine[(int)TARGET_FILE_COLUMN.ItxLock_mA]);
                        channel.TestResult.LockRatio = double.Parse(fileLine[(int)TARGET_FILE_COLUMN.LockRatio]);
                        channel.TestResult.Supermode = int.Parse(fileLine[(int)TARGET_FILE_COLUMN.Supermode]);
                        channel.TestResult.TapCompPhotoCurrentPeak_mA = double.Parse((fileLine[(int)TARGET_FILE_COLUMN.TapCompPhotoCurrentPeak_mA]));
                        channel.TestResult.TapCompPhotoCurrentQuad_mA = double.Parse((fileLine[(int)TARGET_FILE_COLUMN.TapCompPhotoCurrentQuad_mA]));
                    }
                    catch
                    {
                        lineIndex++;

                        this.engine.ShowContinueUserQuery(string.Format("Failed to load channel data in line[{0}], click Continue to load the rest.", fileContext.IndexOf(fileLine)));

                        continue;
                    }

                    string key = string.Format("Line{0}: \"{1}\"", lineIndex++, channel.TestResult.ItuChannelIndex.ToString());

                    targetFileChannelDict.Add(key, channel);
                }
            }

            return targetFileChannelDict;
        }

        private enum TARGET_FILE_COLUMN
        {
            ItuChannelIndex = 0,
            ItuFreq_GHz = 1,
            Supermode = 3,
            IPhaseITU_mA = 7,
            IRear_mA = 9,
            IGain_mA = 10,
            ISoa_mA = 11,
            IRearSoa_mA = 12,
            FrontSectionPair = 13,
            IFsConst_mA = 14,
            IFsNonConst_mA = 15,
            MzLeftArmModPeak_V = 28,
            MzRightArmModPeak_V = 30,
            MzLeftArmModQuad_V = 32,
            MzRightArmModQuad_V = 34,
            MzLeftArmModMinima_V = 36,
            MzRightArmModMinima_V = 38,
            MzLeftArmImb_mA = 40,
            MzRightArmImb_mA = 41,
            TapCompPhotoCurrentQuad_mA = 52,
            TapCompPhotoCurrentPeak_mA = 53,
            ItxLock_mA = 70,
            IrxLock_mA = 71,
            LockRatio = 72
        }
    }

    public struct TestResult
    {
        public int ItuChannelIndex;
        public double ItuFreq_GHz;
        public int Supermode;
        public double TapCompPhotoCurrentQuad_mA;
        public double TapCompPhotoCurrentPeak_mA;
        public double ItxLock_mA;
        public double IrxLock_mA;
        public double LockRatio;
    }

    public struct TargetFileChannel
    {
        public DsdbrChannelSetup DsdbrSetup;
        public MzData MzSetup;
        public TestResult TestResult;
    }
}

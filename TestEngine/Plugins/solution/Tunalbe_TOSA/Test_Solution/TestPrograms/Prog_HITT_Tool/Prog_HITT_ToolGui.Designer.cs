namespace Bookham.TestSolution.TestPrograms
{
    partial class Prog_HITT_ToolGui
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox_OpticalLineSwitch = new System.Windows.Forms.GroupBox();
            this.radioButton_Relay_DUT = new System.Windows.Forms.RadioButton();
            this.radioButton_Relay_OpticalBox = new System.Windows.Forms.RadioButton();
            this.label_ChannelInfo_Index = new System.Windows.Forms.Label();
            this.textBox_ChannelInfo_ITU_GHz = new System.Windows.Forms.TextBox();
            this.button_RearSOA_mA = new System.Windows.Forms.TextBox();
            this.button_MZ_Right_IMB_mA = new System.Windows.Forms.TextBox();
            this.button_MZ_Left_IMB_mA = new System.Windows.Forms.TextBox();
            this.button_MZ_Right_Bias_V = new System.Windows.Forms.TextBox();
            this.button_MZ_Left_Bias_V = new System.Windows.Forms.TextBox();
            this.button_Phase_mA = new System.Windows.Forms.TextBox();
            this.button_NonConstF_mA = new System.Windows.Forms.TextBox();
            this.button_ConstF_mA = new System.Windows.Forms.TextBox();
            this.button_Rear_mA = new System.Windows.Forms.TextBox();
            this.button_SOA_mA = new System.Windows.Forms.TextBox();
            this.button_Gain_mA = new System.Windows.Forms.TextBox();
            this.comboBox_ChannelInfo_Indexes = new System.Windows.Forms.ComboBox();
            this.label_RearSOA_mA = new System.Windows.Forms.Label();
            this.label_MZ_Right_IMB_mA = new System.Windows.Forms.Label();
            this.label_MZ_Left_IMB_mA = new System.Windows.Forms.Label();
            this.label_MZ_Right_Bias_V = new System.Windows.Forms.Label();
            this.label_MZ_Left_Bias_V = new System.Windows.Forms.Label();
            this.label_NonConstF_mA = new System.Windows.Forms.Label();
            this.label_ConstF_mA = new System.Windows.Forms.Label();
            this.label_FrontPair = new System.Windows.Forms.Label();
            this.label_Phase_mA = new System.Windows.Forms.Label();
            this.label_Rear_mA = new System.Windows.Forms.Label();
            this.label_SOA_mA = new System.Windows.Forms.Label();
            this.label_Gain_mA = new System.Windows.Forms.Label();
            this.comboBox_FrontPairs = new System.Windows.Forms.ComboBox();
            this.button_LaserOnOff = new System.Windows.Forms.Button();
            this.label_ITU_GHz = new System.Windows.Forms.Label();
            this.comboBox_ChannelInfo_MZWorkPoints = new System.Windows.Forms.ComboBox();
            this.label_ChannelInfo_MZWorkPoint = new System.Windows.Forms.Label();
            this.textBox_ChannelInfo_LockRatio = new System.Windows.Forms.TextBox();
            this.textBox_ChannelInfo_RX_mA = new System.Windows.Forms.TextBox();
            this.textBox_ChannelInfo_TX_mA = new System.Windows.Forms.TextBox();
            this.textBox_ChannelInfo_SuperMode = new System.Windows.Forms.TextBox();
            this.label_ChannelInfo_LockRatio = new System.Windows.Forms.Label();
            this.label_ChannelInfo_RX_mA = new System.Windows.Forms.Label();
            this.label_ChannelInfo_TX_mA = new System.Windows.Forms.Label();
            this.label_ChannelInfo_SuperMode = new System.Windows.Forms.Label();
            this.textBox_ChannelInfo_CTap_Peak_mA = new System.Windows.Forms.TextBox();
            this.textBox_ChannelInfo_CTap_Quad_mA = new System.Windows.Forms.TextBox();
            this.label_ChannelInfo_CTap_Peak_mA = new System.Windows.Forms.Label();
            this.label_ChannelInfo_CTap_Quad_mA = new System.Windows.Forms.Label();
            this.groupBox_LaserSetup = new System.Windows.Forms.GroupBox();
            this.groupBox_MZSetup = new System.Windows.Forms.GroupBox();
            this.groupBox_ChannelsInfo = new System.Windows.Forms.GroupBox();
            this.groupBox_Measurement = new System.Windows.Forms.GroupBox();
            this.label_LaserTemperatureReading_C = new System.Windows.Forms.Label();
            this.label_CaseTemperatureReading_C = new System.Windows.Forms.Label();
            this.textBox_CaseTemperatureReading_C = new System.Windows.Forms.TextBox();
            this.textBox_LaserTemperatureReading_C = new System.Windows.Forms.TextBox();
            this.label_Frequency_GHz = new System.Windows.Forms.Label();
            this.label_Reference_mA = new System.Windows.Forms.Label();
            this.label_PowerRatio = new System.Windows.Forms.Label();
            this.label_OpticalPower_dBm = new System.Windows.Forms.Label();
            this.label_EtalonTX_mA = new System.Windows.Forms.Label();
            this.label_EtalonRX_mA = new System.Windows.Forms.Label();
            this.textBox_CTap_mA = new System.Windows.Forms.TextBox();
            this.label_LockRatio = new System.Windows.Forms.Label();
            this.label_CTap_mA = new System.Windows.Forms.Label();
            this.label_Filter_mA = new System.Windows.Forms.Label();
            this.textBox_Frequency_GHz = new System.Windows.Forms.TextBox();
            this.textBox_PowerRatio = new System.Windows.Forms.TextBox();
            this.textBox_OpticalPower_dBm = new System.Windows.Forms.TextBox();
            this.textBox_Reference_mA = new System.Windows.Forms.TextBox();
            this.textBox_EtalonTX_mA = new System.Windows.Forms.TextBox();
            this.textBox_Filter_mA = new System.Windows.Forms.TextBox();
            this.textBox_EtalonRX_mA = new System.Windows.Forms.TextBox();
            this.textBox_LockRatio = new System.Windows.Forms.TextBox();
            this.groupBox_DUTInfo = new System.Windows.Forms.GroupBox();
            this.button_LoadChannels = new System.Windows.Forms.Button();
            this.comboBox_TargetStages = new System.Windows.Forms.ComboBox();
            this.comboBox_PartCodes = new System.Windows.Forms.ComboBox();
            this.comboBox_PCASFileKeys = new System.Windows.Forms.ComboBox();
            this.label_SerialNO = new System.Windows.Forms.Label();
            this.textBox_SerialNO = new System.Windows.Forms.TextBox();
            this.label_TargetStage = new System.Windows.Forms.Label();
            this.label_PartCode = new System.Windows.Forms.Label();
            this.label_PCASFileKey = new System.Windows.Forms.Label();
            this.button_PowerOnOff = new System.Windows.Forms.Button();
            this.groupBox_TemperatureSetting = new System.Windows.Forms.GroupBox();
            this.label_LaserTemperatureSetting_C = new System.Windows.Forms.Label();
            this.textBox_LaserTemperatureSetting_C = new System.Windows.Forms.TextBox();
            this.textBox_CaseTemperatureSetting_C = new System.Windows.Forms.TextBox();
            this.label_CaseTemperatureSetting_C = new System.Windows.Forms.Label();
            this.groupBox_Sweep = new System.Windows.Forms.GroupBox();
            this.btn_ResetSoa = new System.Windows.Forms.Button();
            this.txt_ResetSoa = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btn_PowerOffLaserByOnePoint = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.txtBox_RearCutScan_Step = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btn_RearCutScan = new System.Windows.Forms.Button();
            this.txtBox_RearCutScan_To = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtBox_RearCutScan_From = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtBox_PhaseCutScan_Step = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btn_PhaseCutScan = new System.Windows.Forms.Button();
            this.txtBox_PhaseCutScan_To = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtBox_PhaseCutScan_From = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_PowerUpLaserByOnePoint = new System.Windows.Forms.Button();
            this.groupBox_OpticalLineSwitch.SuspendLayout();
            this.groupBox_LaserSetup.SuspendLayout();
            this.groupBox_MZSetup.SuspendLayout();
            this.groupBox_ChannelsInfo.SuspendLayout();
            this.groupBox_Measurement.SuspendLayout();
            this.groupBox_DUTInfo.SuspendLayout();
            this.groupBox_TemperatureSetting.SuspendLayout();
            this.groupBox_Sweep.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox_OpticalLineSwitch
            // 
            this.groupBox_OpticalLineSwitch.Controls.Add(this.radioButton_Relay_DUT);
            this.groupBox_OpticalLineSwitch.Controls.Add(this.radioButton_Relay_OpticalBox);
            this.groupBox_OpticalLineSwitch.Enabled = false;
            this.groupBox_OpticalLineSwitch.Location = new System.Drawing.Point(640, 386);
            this.groupBox_OpticalLineSwitch.Name = "groupBox_OpticalLineSwitch";
            this.groupBox_OpticalLineSwitch.Size = new System.Drawing.Size(146, 72);
            this.groupBox_OpticalLineSwitch.TabIndex = 188;
            this.groupBox_OpticalLineSwitch.TabStop = false;
            this.groupBox_OpticalLineSwitch.Text = "Optical Line Switch";
            // 
            // radioButton_Relay_DUT
            // 
            this.radioButton_Relay_DUT.AutoSize = true;
            this.radioButton_Relay_DUT.Checked = true;
            this.radioButton_Relay_DUT.Location = new System.Drawing.Point(6, 22);
            this.radioButton_Relay_DUT.Name = "radioButton_Relay_DUT";
            this.radioButton_Relay_DUT.Size = new System.Drawing.Size(122, 17);
            this.radioButton_Relay_DUT.TabIndex = 110;
            this.radioButton_Relay_DUT.TabStop = true;
            this.radioButton_Relay_DUT.Text = "DUT TX | RX | CTap";
            this.radioButton_Relay_DUT.UseVisualStyleBackColor = true;
            // 
            // radioButton_Relay_OpticalBox
            // 
            this.radioButton_Relay_OpticalBox.AutoSize = true;
            this.radioButton_Relay_OpticalBox.Location = new System.Drawing.Point(6, 45);
            this.radioButton_Relay_OpticalBox.Name = "radioButton_Relay_OpticalBox";
            this.radioButton_Relay_OpticalBox.Size = new System.Drawing.Size(141, 17);
            this.radioButton_Relay_OpticalBox.TabIndex = 111;
            this.radioButton_Relay_OpticalBox.Text = "OpticalBox TX | RX | Ref";
            this.radioButton_Relay_OpticalBox.UseVisualStyleBackColor = true;
            // 
            // label_ChannelInfo_Index
            // 
            this.label_ChannelInfo_Index.AutoSize = true;
            this.label_ChannelInfo_Index.Location = new System.Drawing.Point(17, 28);
            this.label_ChannelInfo_Index.Name = "label_ChannelInfo_Index";
            this.label_ChannelInfo_Index.Size = new System.Drawing.Size(81, 13);
            this.label_ChannelInfo_Index.TabIndex = 186;
            this.label_ChannelInfo_Index.Text = "Channel_Index:";
            // 
            // textBox_ChannelInfo_ITU_GHz
            // 
            this.textBox_ChannelInfo_ITU_GHz.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_ChannelInfo_ITU_GHz.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.textBox_ChannelInfo_ITU_GHz.Location = new System.Drawing.Point(105, 78);
            this.textBox_ChannelInfo_ITU_GHz.Name = "textBox_ChannelInfo_ITU_GHz";
            this.textBox_ChannelInfo_ITU_GHz.ReadOnly = true;
            this.textBox_ChannelInfo_ITU_GHz.Size = new System.Drawing.Size(118, 26);
            this.textBox_ChannelInfo_ITU_GHz.TabIndex = 184;
            // 
            // button_RearSOA_mA
            // 
            this.button_RearSOA_mA.AcceptsTab = true;
            this.button_RearSOA_mA.Location = new System.Drawing.Point(137, 191);
            this.button_RearSOA_mA.Name = "button_RearSOA_mA";
            this.button_RearSOA_mA.Size = new System.Drawing.Size(50, 20);
            this.button_RearSOA_mA.TabIndex = 163;
            // 
            // button_MZ_Right_IMB_mA
            // 
            this.button_MZ_Right_IMB_mA.AcceptsTab = true;
            this.button_MZ_Right_IMB_mA.Location = new System.Drawing.Point(137, 102);
            this.button_MZ_Right_IMB_mA.Name = "button_MZ_Right_IMB_mA";
            this.button_MZ_Right_IMB_mA.Size = new System.Drawing.Size(50, 20);
            this.button_MZ_Right_IMB_mA.TabIndex = 161;
            // 
            // button_MZ_Left_IMB_mA
            // 
            this.button_MZ_Left_IMB_mA.AcceptsTab = true;
            this.button_MZ_Left_IMB_mA.Location = new System.Drawing.Point(137, 78);
            this.button_MZ_Left_IMB_mA.Name = "button_MZ_Left_IMB_mA";
            this.button_MZ_Left_IMB_mA.Size = new System.Drawing.Size(50, 20);
            this.button_MZ_Left_IMB_mA.TabIndex = 159;
            // 
            // button_MZ_Right_Bias_V
            // 
            this.button_MZ_Right_Bias_V.Location = new System.Drawing.Point(137, 53);
            this.button_MZ_Right_Bias_V.Name = "button_MZ_Right_Bias_V";
            this.button_MZ_Right_Bias_V.Size = new System.Drawing.Size(50, 20);
            this.button_MZ_Right_Bias_V.TabIndex = 156;
            // 
            // button_MZ_Left_Bias_V
            // 
            this.button_MZ_Left_Bias_V.Location = new System.Drawing.Point(137, 26);
            this.button_MZ_Left_Bias_V.Name = "button_MZ_Left_Bias_V";
            this.button_MZ_Left_Bias_V.Size = new System.Drawing.Size(50, 20);
            this.button_MZ_Left_Bias_V.TabIndex = 153;
            // 
            // button_Phase_mA
            // 
            this.button_Phase_mA.Location = new System.Drawing.Point(137, 92);
            this.button_Phase_mA.Name = "button_Phase_mA";
            this.button_Phase_mA.Size = new System.Drawing.Size(50, 20);
            this.button_Phase_mA.TabIndex = 121;
            // 
            // button_NonConstF_mA
            // 
            this.button_NonConstF_mA.Location = new System.Drawing.Point(137, 167);
            this.button_NonConstF_mA.Name = "button_NonConstF_mA";
            this.button_NonConstF_mA.Size = new System.Drawing.Size(50, 20);
            this.button_NonConstF_mA.TabIndex = 120;
            // 
            // button_ConstF_mA
            // 
            this.button_ConstF_mA.Location = new System.Drawing.Point(137, 143);
            this.button_ConstF_mA.Name = "button_ConstF_mA";
            this.button_ConstF_mA.Size = new System.Drawing.Size(50, 20);
            this.button_ConstF_mA.TabIndex = 119;
            // 
            // button_Rear_mA
            // 
            this.button_Rear_mA.Location = new System.Drawing.Point(137, 67);
            this.button_Rear_mA.Name = "button_Rear_mA";
            this.button_Rear_mA.Size = new System.Drawing.Size(50, 20);
            this.button_Rear_mA.TabIndex = 118;
            // 
            // button_SOA_mA
            // 
            this.button_SOA_mA.Location = new System.Drawing.Point(137, 43);
            this.button_SOA_mA.Name = "button_SOA_mA";
            this.button_SOA_mA.Size = new System.Drawing.Size(50, 20);
            this.button_SOA_mA.TabIndex = 117;
            // 
            // button_Gain_mA
            // 
            this.button_Gain_mA.Location = new System.Drawing.Point(137, 19);
            this.button_Gain_mA.Name = "button_Gain_mA";
            this.button_Gain_mA.Size = new System.Drawing.Size(50, 20);
            this.button_Gain_mA.TabIndex = 116;
            // 
            // comboBox_ChannelInfo_Indexes
            // 
            this.comboBox_ChannelInfo_Indexes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_ChannelInfo_Indexes.FormattingEnabled = true;
            this.comboBox_ChannelInfo_Indexes.Location = new System.Drawing.Point(105, 26);
            this.comboBox_ChannelInfo_Indexes.Name = "comboBox_ChannelInfo_Indexes";
            this.comboBox_ChannelInfo_Indexes.Size = new System.Drawing.Size(118, 21);
            this.comboBox_ChannelInfo_Indexes.TabIndex = 182;
            this.comboBox_ChannelInfo_Indexes.SelectedIndexChanged += new System.EventHandler(this.comboBox_ChannelInfo_Indexes_SelectedIndexChanged);
            // 
            // label_RearSOA_mA
            // 
            this.label_RearSOA_mA.AutoSize = true;
            this.label_RearSOA_mA.Location = new System.Drawing.Point(56, 195);
            this.label_RearSOA_mA.Name = "label_RearSOA_mA";
            this.label_RearSOA_mA.Size = new System.Drawing.Size(76, 13);
            this.label_RearSOA_mA.TabIndex = 164;
            this.label_RearSOA_mA.Text = "RearSOA_mA:";
            // 
            // label_MZ_Right_IMB_mA
            // 
            this.label_MZ_Right_IMB_mA.AutoSize = true;
            this.label_MZ_Right_IMB_mA.Location = new System.Drawing.Point(31, 105);
            this.label_MZ_Right_IMB_mA.Name = "label_MZ_Right_IMB_mA";
            this.label_MZ_Right_IMB_mA.Size = new System.Drawing.Size(103, 13);
            this.label_MZ_Right_IMB_mA.TabIndex = 162;
            this.label_MZ_Right_IMB_mA.Text = "MZ_Right_IMB_mA:";
            // 
            // label_MZ_Left_IMB_mA
            // 
            this.label_MZ_Left_IMB_mA.AutoSize = true;
            this.label_MZ_Left_IMB_mA.Location = new System.Drawing.Point(37, 80);
            this.label_MZ_Left_IMB_mA.Name = "label_MZ_Left_IMB_mA";
            this.label_MZ_Left_IMB_mA.Size = new System.Drawing.Size(96, 13);
            this.label_MZ_Left_IMB_mA.TabIndex = 160;
            this.label_MZ_Left_IMB_mA.Text = "MZ_Left_IMB_mA:";
            // 
            // label_MZ_Right_Bias_V
            // 
            this.label_MZ_Right_Bias_V.AutoSize = true;
            this.label_MZ_Right_Bias_V.Location = new System.Drawing.Point(35, 57);
            this.label_MZ_Right_Bias_V.Name = "label_MZ_Right_Bias_V";
            this.label_MZ_Right_Bias_V.Size = new System.Drawing.Size(96, 13);
            this.label_MZ_Right_Bias_V.TabIndex = 158;
            this.label_MZ_Right_Bias_V.Text = "MZ_Right_Bias_V:";
            // 
            // label_MZ_Left_Bias_V
            // 
            this.label_MZ_Left_Bias_V.AutoSize = true;
            this.label_MZ_Left_Bias_V.Location = new System.Drawing.Point(43, 30);
            this.label_MZ_Left_Bias_V.Name = "label_MZ_Left_Bias_V";
            this.label_MZ_Left_Bias_V.Size = new System.Drawing.Size(89, 13);
            this.label_MZ_Left_Bias_V.TabIndex = 155;
            this.label_MZ_Left_Bias_V.Text = "MZ_Left_Bias_V:";
            // 
            // label_NonConstF_mA
            // 
            this.label_NonConstF_mA.AutoSize = true;
            this.label_NonConstF_mA.Location = new System.Drawing.Point(48, 171);
            this.label_NonConstF_mA.Name = "label_NonConstF_mA";
            this.label_NonConstF_mA.Size = new System.Drawing.Size(84, 13);
            this.label_NonConstF_mA.TabIndex = 147;
            this.label_NonConstF_mA.Text = "NonConstF_mA:";
            // 
            // label_ConstF_mA
            // 
            this.label_ConstF_mA.AutoSize = true;
            this.label_ConstF_mA.Location = new System.Drawing.Point(68, 147);
            this.label_ConstF_mA.Name = "label_ConstF_mA";
            this.label_ConstF_mA.Size = new System.Drawing.Size(64, 13);
            this.label_ConstF_mA.TabIndex = 146;
            this.label_ConstF_mA.Text = "ConstF_mA:";
            // 
            // label_FrontPair
            // 
            this.label_FrontPair.AutoSize = true;
            this.label_FrontPair.Location = new System.Drawing.Point(72, 121);
            this.label_FrontPair.Name = "label_FrontPair";
            this.label_FrontPair.Size = new System.Drawing.Size(58, 13);
            this.label_FrontPair.TabIndex = 145;
            this.label_FrontPair.Text = "Front_Pair:";
            // 
            // label_Phase_mA
            // 
            this.label_Phase_mA.AutoSize = true;
            this.label_Phase_mA.Location = new System.Drawing.Point(70, 96);
            this.label_Phase_mA.Name = "label_Phase_mA";
            this.label_Phase_mA.Size = new System.Drawing.Size(61, 13);
            this.label_Phase_mA.TabIndex = 144;
            this.label_Phase_mA.Text = "Phase_mA:";
            // 
            // label_Rear_mA
            // 
            this.label_Rear_mA.AutoSize = true;
            this.label_Rear_mA.Location = new System.Drawing.Point(77, 72);
            this.label_Rear_mA.Name = "label_Rear_mA";
            this.label_Rear_mA.Size = new System.Drawing.Size(54, 13);
            this.label_Rear_mA.TabIndex = 143;
            this.label_Rear_mA.Text = "Rear_mA:";
            // 
            // label_SOA_mA
            // 
            this.label_SOA_mA.AutoSize = true;
            this.label_SOA_mA.Location = new System.Drawing.Point(79, 46);
            this.label_SOA_mA.Name = "label_SOA_mA";
            this.label_SOA_mA.Size = new System.Drawing.Size(53, 13);
            this.label_SOA_mA.TabIndex = 142;
            this.label_SOA_mA.Text = "SOA_mA:";
            // 
            // label_Gain_mA
            // 
            this.label_Gain_mA.AutoSize = true;
            this.label_Gain_mA.Location = new System.Drawing.Point(78, 23);
            this.label_Gain_mA.Name = "label_Gain_mA";
            this.label_Gain_mA.Size = new System.Drawing.Size(53, 13);
            this.label_Gain_mA.TabIndex = 141;
            this.label_Gain_mA.Text = "Gain_mA:";
            // 
            // comboBox_FrontPairs
            // 
            this.comboBox_FrontPairs.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_FrontPairs.FormattingEnabled = true;
            this.comboBox_FrontPairs.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7"});
            this.comboBox_FrontPairs.Location = new System.Drawing.Point(137, 117);
            this.comboBox_FrontPairs.Name = "comboBox_FrontPairs";
            this.comboBox_FrontPairs.Size = new System.Drawing.Size(50, 21);
            this.comboBox_FrontPairs.TabIndex = 130;
            // 
            // button_LaserOnOff
            // 
            this.button_LaserOnOff.BackColor = System.Drawing.Color.DarkGray;
            this.button_LaserOnOff.Location = new System.Drawing.Point(512, 386);
            this.button_LaserOnOff.Name = "button_LaserOnOff";
            this.button_LaserOnOff.Size = new System.Drawing.Size(122, 34);
            this.button_LaserOnOff.TabIndex = 113;
            this.button_LaserOnOff.Text = "Laser is Off";
            this.button_LaserOnOff.UseVisualStyleBackColor = false;
            this.button_LaserOnOff.Click += new System.EventHandler(this.button_LaserOnOff_Click);
            // 
            // label_ITU_GHz
            // 
            this.label_ITU_GHz.AutoSize = true;
            this.label_ITU_GHz.Location = new System.Drawing.Point(41, 84);
            this.label_ITU_GHz.Name = "label_ITU_GHz";
            this.label_ITU_GHz.Size = new System.Drawing.Size(55, 13);
            this.label_ITU_GHz.TabIndex = 185;
            this.label_ITU_GHz.Text = "ITU_GHz:";
            // 
            // comboBox_ChannelInfo_MZWorkPoints
            // 
            this.comboBox_ChannelInfo_MZWorkPoints.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_ChannelInfo_MZWorkPoints.FormattingEnabled = true;
            this.comboBox_ChannelInfo_MZWorkPoints.Items.AddRange(new object[] {
            "Peak",
            "Quad",
            "Trough"});
            this.comboBox_ChannelInfo_MZWorkPoints.Location = new System.Drawing.Point(105, 52);
            this.comboBox_ChannelInfo_MZWorkPoints.Name = "comboBox_ChannelInfo_MZWorkPoints";
            this.comboBox_ChannelInfo_MZWorkPoints.Size = new System.Drawing.Size(118, 21);
            this.comboBox_ChannelInfo_MZWorkPoints.TabIndex = 192;
            this.comboBox_ChannelInfo_MZWorkPoints.SelectedIndexChanged += new System.EventHandler(this.comboBox_ChannelInfo_MZWorkPoints_SelectedIndexChanged);
            // 
            // label_ChannelInfo_MZWorkPoint
            // 
            this.label_ChannelInfo_MZWorkPoint.AutoSize = true;
            this.label_ChannelInfo_MZWorkPoint.Location = new System.Drawing.Point(11, 54);
            this.label_ChannelInfo_MZWorkPoint.Name = "label_ChannelInfo_MZWorkPoint";
            this.label_ChannelInfo_MZWorkPoint.Size = new System.Drawing.Size(88, 13);
            this.label_ChannelInfo_MZWorkPoint.TabIndex = 193;
            this.label_ChannelInfo_MZWorkPoint.Text = "MZ_Work_Point:";
            // 
            // textBox_ChannelInfo_LockRatio
            // 
            this.textBox_ChannelInfo_LockRatio.AcceptsTab = true;
            this.textBox_ChannelInfo_LockRatio.Location = new System.Drawing.Point(105, 183);
            this.textBox_ChannelInfo_LockRatio.Name = "textBox_ChannelInfo_LockRatio";
            this.textBox_ChannelInfo_LockRatio.ReadOnly = true;
            this.textBox_ChannelInfo_LockRatio.Size = new System.Drawing.Size(118, 20);
            this.textBox_ChannelInfo_LockRatio.TabIndex = 201;
            // 
            // textBox_ChannelInfo_RX_mA
            // 
            this.textBox_ChannelInfo_RX_mA.AcceptsTab = true;
            this.textBox_ChannelInfo_RX_mA.Location = new System.Drawing.Point(105, 158);
            this.textBox_ChannelInfo_RX_mA.Name = "textBox_ChannelInfo_RX_mA";
            this.textBox_ChannelInfo_RX_mA.ReadOnly = true;
            this.textBox_ChannelInfo_RX_mA.Size = new System.Drawing.Size(118, 20);
            this.textBox_ChannelInfo_RX_mA.TabIndex = 199;
            // 
            // textBox_ChannelInfo_TX_mA
            // 
            this.textBox_ChannelInfo_TX_mA.Location = new System.Drawing.Point(105, 133);
            this.textBox_ChannelInfo_TX_mA.Name = "textBox_ChannelInfo_TX_mA";
            this.textBox_ChannelInfo_TX_mA.ReadOnly = true;
            this.textBox_ChannelInfo_TX_mA.Size = new System.Drawing.Size(118, 20);
            this.textBox_ChannelInfo_TX_mA.TabIndex = 197;
            // 
            // textBox_ChannelInfo_SuperMode
            // 
            this.textBox_ChannelInfo_SuperMode.Location = new System.Drawing.Point(105, 109);
            this.textBox_ChannelInfo_SuperMode.Name = "textBox_ChannelInfo_SuperMode";
            this.textBox_ChannelInfo_SuperMode.ReadOnly = true;
            this.textBox_ChannelInfo_SuperMode.Size = new System.Drawing.Size(118, 20);
            this.textBox_ChannelInfo_SuperMode.TabIndex = 195;
            // 
            // label_ChannelInfo_LockRatio
            // 
            this.label_ChannelInfo_LockRatio.AutoSize = true;
            this.label_ChannelInfo_LockRatio.Location = new System.Drawing.Point(15, 185);
            this.label_ChannelInfo_LockRatio.Name = "label_ChannelInfo_LockRatio";
            this.label_ChannelInfo_LockRatio.Size = new System.Drawing.Size(88, 13);
            this.label_ChannelInfo_LockRatio.TabIndex = 202;
            this.label_ChannelInfo_LockRatio.Text = "DUT_LockRatio:";
            // 
            // label_ChannelInfo_RX_mA
            // 
            this.label_ChannelInfo_RX_mA.AutoSize = true;
            this.label_ChannelInfo_RX_mA.Location = new System.Drawing.Point(27, 161);
            this.label_ChannelInfo_RX_mA.Name = "label_ChannelInfo_RX_mA";
            this.label_ChannelInfo_RX_mA.Size = new System.Drawing.Size(75, 13);
            this.label_ChannelInfo_RX_mA.TabIndex = 200;
            this.label_ChannelInfo_RX_mA.Text = "DUT_RX_mA:";
            // 
            // label_ChannelInfo_TX_mA
            // 
            this.label_ChannelInfo_TX_mA.AutoSize = true;
            this.label_ChannelInfo_TX_mA.Location = new System.Drawing.Point(27, 137);
            this.label_ChannelInfo_TX_mA.Name = "label_ChannelInfo_TX_mA";
            this.label_ChannelInfo_TX_mA.Size = new System.Drawing.Size(74, 13);
            this.label_ChannelInfo_TX_mA.TabIndex = 198;
            this.label_ChannelInfo_TX_mA.Text = "DUT_TX_mA:";
            // 
            // label_ChannelInfo_SuperMode
            // 
            this.label_ChannelInfo_SuperMode.AutoSize = true;
            this.label_ChannelInfo_SuperMode.Location = new System.Drawing.Point(32, 109);
            this.label_ChannelInfo_SuperMode.Name = "label_ChannelInfo_SuperMode";
            this.label_ChannelInfo_SuperMode.Size = new System.Drawing.Size(65, 13);
            this.label_ChannelInfo_SuperMode.TabIndex = 196;
            this.label_ChannelInfo_SuperMode.Text = "SuperMode:";
            // 
            // textBox_ChannelInfo_CTap_Peak_mA
            // 
            this.textBox_ChannelInfo_CTap_Peak_mA.AcceptsTab = true;
            this.textBox_ChannelInfo_CTap_Peak_mA.Location = new System.Drawing.Point(105, 232);
            this.textBox_ChannelInfo_CTap_Peak_mA.Name = "textBox_ChannelInfo_CTap_Peak_mA";
            this.textBox_ChannelInfo_CTap_Peak_mA.ReadOnly = true;
            this.textBox_ChannelInfo_CTap_Peak_mA.Size = new System.Drawing.Size(118, 20);
            this.textBox_ChannelInfo_CTap_Peak_mA.TabIndex = 205;
            // 
            // textBox_ChannelInfo_CTap_Quad_mA
            // 
            this.textBox_ChannelInfo_CTap_Quad_mA.AcceptsTab = true;
            this.textBox_ChannelInfo_CTap_Quad_mA.Location = new System.Drawing.Point(105, 207);
            this.textBox_ChannelInfo_CTap_Quad_mA.Name = "textBox_ChannelInfo_CTap_Quad_mA";
            this.textBox_ChannelInfo_CTap_Quad_mA.ReadOnly = true;
            this.textBox_ChannelInfo_CTap_Quad_mA.Size = new System.Drawing.Size(118, 20);
            this.textBox_ChannelInfo_CTap_Quad_mA.TabIndex = 203;
            // 
            // label_ChannelInfo_CTap_Peak_mA
            // 
            this.label_ChannelInfo_CTap_Peak_mA.AutoSize = true;
            this.label_ChannelInfo_CTap_Peak_mA.Location = new System.Drawing.Point(14, 234);
            this.label_ChannelInfo_CTap_Peak_mA.Name = "label_ChannelInfo_CTap_Peak_mA";
            this.label_ChannelInfo_CTap_Peak_mA.Size = new System.Drawing.Size(88, 13);
            this.label_ChannelInfo_CTap_Peak_mA.TabIndex = 206;
            this.label_ChannelInfo_CTap_Peak_mA.Text = "CTap_Peak_mA:";
            // 
            // label_ChannelInfo_CTap_Quad_mA
            // 
            this.label_ChannelInfo_CTap_Quad_mA.AutoSize = true;
            this.label_ChannelInfo_CTap_Quad_mA.Location = new System.Drawing.Point(11, 210);
            this.label_ChannelInfo_CTap_Quad_mA.Name = "label_ChannelInfo_CTap_Quad_mA";
            this.label_ChannelInfo_CTap_Quad_mA.Size = new System.Drawing.Size(89, 13);
            this.label_ChannelInfo_CTap_Quad_mA.TabIndex = 204;
            this.label_ChannelInfo_CTap_Quad_mA.Text = "CTap_Quad_mA:";
            // 
            // groupBox_LaserSetup
            // 
            this.groupBox_LaserSetup.Controls.Add(this.label_Gain_mA);
            this.groupBox_LaserSetup.Controls.Add(this.button_Gain_mA);
            this.groupBox_LaserSetup.Controls.Add(this.label_SOA_mA);
            this.groupBox_LaserSetup.Controls.Add(this.button_SOA_mA);
            this.groupBox_LaserSetup.Controls.Add(this.label_Rear_mA);
            this.groupBox_LaserSetup.Controls.Add(this.button_Rear_mA);
            this.groupBox_LaserSetup.Controls.Add(this.label_Phase_mA);
            this.groupBox_LaserSetup.Controls.Add(this.button_Phase_mA);
            this.groupBox_LaserSetup.Controls.Add(this.label_FrontPair);
            this.groupBox_LaserSetup.Controls.Add(this.comboBox_FrontPairs);
            this.groupBox_LaserSetup.Controls.Add(this.label_ConstF_mA);
            this.groupBox_LaserSetup.Controls.Add(this.button_ConstF_mA);
            this.groupBox_LaserSetup.Controls.Add(this.label_NonConstF_mA);
            this.groupBox_LaserSetup.Controls.Add(this.button_NonConstF_mA);
            this.groupBox_LaserSetup.Controls.Add(this.label_RearSOA_mA);
            this.groupBox_LaserSetup.Controls.Add(this.button_RearSOA_mA);
            this.groupBox_LaserSetup.Location = new System.Drawing.Point(284, 11);
            this.groupBox_LaserSetup.Name = "groupBox_LaserSetup";
            this.groupBox_LaserSetup.Size = new System.Drawing.Size(211, 227);
            this.groupBox_LaserSetup.TabIndex = 189;
            this.groupBox_LaserSetup.TabStop = false;
            this.groupBox_LaserSetup.Text = "Laser Setup";
            // 
            // groupBox_MZSetup
            // 
            this.groupBox_MZSetup.Controls.Add(this.label_MZ_Left_Bias_V);
            this.groupBox_MZSetup.Controls.Add(this.button_MZ_Left_Bias_V);
            this.groupBox_MZSetup.Controls.Add(this.button_MZ_Right_Bias_V);
            this.groupBox_MZSetup.Controls.Add(this.button_MZ_Left_IMB_mA);
            this.groupBox_MZSetup.Controls.Add(this.button_MZ_Right_IMB_mA);
            this.groupBox_MZSetup.Controls.Add(this.label_MZ_Right_Bias_V);
            this.groupBox_MZSetup.Controls.Add(this.label_MZ_Left_IMB_mA);
            this.groupBox_MZSetup.Controls.Add(this.label_MZ_Right_IMB_mA);
            this.groupBox_MZSetup.Location = new System.Drawing.Point(284, 245);
            this.groupBox_MZSetup.Name = "groupBox_MZSetup";
            this.groupBox_MZSetup.Size = new System.Drawing.Size(211, 128);
            this.groupBox_MZSetup.TabIndex = 190;
            this.groupBox_MZSetup.TabStop = false;
            this.groupBox_MZSetup.Text = "MZ Setup";
            // 
            // groupBox_ChannelsInfo
            // 
            this.groupBox_ChannelsInfo.Controls.Add(this.label_ChannelInfo_CTap_Peak_mA);
            this.groupBox_ChannelsInfo.Controls.Add(this.textBox_ChannelInfo_CTap_Peak_mA);
            this.groupBox_ChannelsInfo.Controls.Add(this.label_ChannelInfo_CTap_Quad_mA);
            this.groupBox_ChannelsInfo.Controls.Add(this.label_ChannelInfo_LockRatio);
            this.groupBox_ChannelsInfo.Controls.Add(this.label_ChannelInfo_MZWorkPoint);
            this.groupBox_ChannelsInfo.Controls.Add(this.textBox_ChannelInfo_CTap_Quad_mA);
            this.groupBox_ChannelsInfo.Controls.Add(this.comboBox_ChannelInfo_MZWorkPoints);
            this.groupBox_ChannelsInfo.Controls.Add(this.label_ChannelInfo_RX_mA);
            this.groupBox_ChannelsInfo.Controls.Add(this.label_ChannelInfo_TX_mA);
            this.groupBox_ChannelsInfo.Controls.Add(this.textBox_ChannelInfo_LockRatio);
            this.groupBox_ChannelsInfo.Controls.Add(this.textBox_ChannelInfo_RX_mA);
            this.groupBox_ChannelsInfo.Controls.Add(this.label_ChannelInfo_SuperMode);
            this.groupBox_ChannelsInfo.Controls.Add(this.label_ChannelInfo_Index);
            this.groupBox_ChannelsInfo.Controls.Add(this.textBox_ChannelInfo_ITU_GHz);
            this.groupBox_ChannelsInfo.Controls.Add(this.textBox_ChannelInfo_TX_mA);
            this.groupBox_ChannelsInfo.Controls.Add(this.textBox_ChannelInfo_SuperMode);
            this.groupBox_ChannelsInfo.Controls.Add(this.comboBox_ChannelInfo_Indexes);
            this.groupBox_ChannelsInfo.Controls.Add(this.label_ITU_GHz);
            this.groupBox_ChannelsInfo.Location = new System.Drawing.Point(20, 197);
            this.groupBox_ChannelsInfo.Name = "groupBox_ChannelsInfo";
            this.groupBox_ChannelsInfo.Size = new System.Drawing.Size(245, 262);
            this.groupBox_ChannelsInfo.TabIndex = 191;
            this.groupBox_ChannelsInfo.TabStop = false;
            this.groupBox_ChannelsInfo.Text = "Channels Info";
            // 
            // groupBox_Measurement
            // 
            this.groupBox_Measurement.Controls.Add(this.label_LaserTemperatureReading_C);
            this.groupBox_Measurement.Controls.Add(this.label_CaseTemperatureReading_C);
            this.groupBox_Measurement.Controls.Add(this.textBox_CaseTemperatureReading_C);
            this.groupBox_Measurement.Controls.Add(this.textBox_LaserTemperatureReading_C);
            this.groupBox_Measurement.Controls.Add(this.label_Frequency_GHz);
            this.groupBox_Measurement.Controls.Add(this.label_Reference_mA);
            this.groupBox_Measurement.Controls.Add(this.label_PowerRatio);
            this.groupBox_Measurement.Controls.Add(this.label_OpticalPower_dBm);
            this.groupBox_Measurement.Controls.Add(this.label_EtalonTX_mA);
            this.groupBox_Measurement.Controls.Add(this.label_EtalonRX_mA);
            this.groupBox_Measurement.Controls.Add(this.textBox_CTap_mA);
            this.groupBox_Measurement.Controls.Add(this.label_LockRatio);
            this.groupBox_Measurement.Controls.Add(this.label_CTap_mA);
            this.groupBox_Measurement.Controls.Add(this.label_Filter_mA);
            this.groupBox_Measurement.Controls.Add(this.textBox_Frequency_GHz);
            this.groupBox_Measurement.Controls.Add(this.textBox_PowerRatio);
            this.groupBox_Measurement.Controls.Add(this.textBox_OpticalPower_dBm);
            this.groupBox_Measurement.Controls.Add(this.textBox_Reference_mA);
            this.groupBox_Measurement.Controls.Add(this.textBox_EtalonTX_mA);
            this.groupBox_Measurement.Controls.Add(this.textBox_Filter_mA);
            this.groupBox_Measurement.Controls.Add(this.textBox_EtalonRX_mA);
            this.groupBox_Measurement.Controls.Add(this.textBox_LockRatio);
            this.groupBox_Measurement.Location = new System.Drawing.Point(512, 11);
            this.groupBox_Measurement.Name = "groupBox_Measurement";
            this.groupBox_Measurement.Size = new System.Drawing.Size(274, 368);
            this.groupBox_Measurement.TabIndex = 207;
            this.groupBox_Measurement.TabStop = false;
            this.groupBox_Measurement.Text = "Measurement";
            // 
            // label_LaserTemperatureReading_C
            // 
            this.label_LaserTemperatureReading_C.AutoSize = true;
            this.label_LaserTemperatureReading_C.Location = new System.Drawing.Point(38, 309);
            this.label_LaserTemperatureReading_C.Name = "label_LaserTemperatureReading_C";
            this.label_LaserTemperatureReading_C.Size = new System.Drawing.Size(115, 13);
            this.label_LaserTemperatureReading_C.TabIndex = 212;
            this.label_LaserTemperatureReading_C.Text = "Laser_Temperature_C:";
            // 
            // label_CaseTemperatureReading_C
            // 
            this.label_CaseTemperatureReading_C.AutoSize = true;
            this.label_CaseTemperatureReading_C.Location = new System.Drawing.Point(39, 336);
            this.label_CaseTemperatureReading_C.Name = "label_CaseTemperatureReading_C";
            this.label_CaseTemperatureReading_C.Size = new System.Drawing.Size(113, 13);
            this.label_CaseTemperatureReading_C.TabIndex = 213;
            this.label_CaseTemperatureReading_C.Text = "Case_Temperature_C:";
            // 
            // textBox_CaseTemperatureReading_C
            // 
            this.textBox_CaseTemperatureReading_C.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_CaseTemperatureReading_C.Location = new System.Drawing.Point(161, 332);
            this.textBox_CaseTemperatureReading_C.Name = "textBox_CaseTemperatureReading_C";
            this.textBox_CaseTemperatureReading_C.ReadOnly = true;
            this.textBox_CaseTemperatureReading_C.Size = new System.Drawing.Size(91, 26);
            this.textBox_CaseTemperatureReading_C.TabIndex = 211;
            // 
            // textBox_LaserTemperatureReading_C
            // 
            this.textBox_LaserTemperatureReading_C.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_LaserTemperatureReading_C.Location = new System.Drawing.Point(161, 301);
            this.textBox_LaserTemperatureReading_C.Name = "textBox_LaserTemperatureReading_C";
            this.textBox_LaserTemperatureReading_C.ReadOnly = true;
            this.textBox_LaserTemperatureReading_C.Size = new System.Drawing.Size(91, 26);
            this.textBox_LaserTemperatureReading_C.TabIndex = 210;
            // 
            // label_Frequency_GHz
            // 
            this.label_Frequency_GHz.AutoSize = true;
            this.label_Frequency_GHz.Location = new System.Drawing.Point(70, 28);
            this.label_Frequency_GHz.Name = "label_Frequency_GHz";
            this.label_Frequency_GHz.Size = new System.Drawing.Size(87, 13);
            this.label_Frequency_GHz.TabIndex = 209;
            this.label_Frequency_GHz.Text = "Frequency_GHz:";
            // 
            // label_Reference_mA
            // 
            this.label_Reference_mA.AutoSize = true;
            this.label_Reference_mA.Location = new System.Drawing.Point(19, 247);
            this.label_Reference_mA.Name = "label_Reference_mA";
            this.label_Reference_mA.Size = new System.Drawing.Size(138, 13);
            this.label_Reference_mA.TabIndex = 180;
            this.label_Reference_mA.Text = "OpticalBox_Reference_mA:";
            // 
            // label_PowerRatio
            // 
            this.label_PowerRatio.AutoSize = true;
            this.label_PowerRatio.Location = new System.Drawing.Point(86, 278);
            this.label_PowerRatio.Name = "label_PowerRatio";
            this.label_PowerRatio.Size = new System.Drawing.Size(71, 13);
            this.label_PowerRatio.TabIndex = 181;
            this.label_PowerRatio.Text = "Power_Ratio:";
            // 
            // label_OpticalPower_dBm
            // 
            this.label_OpticalPower_dBm.AutoSize = true;
            this.label_OpticalPower_dBm.Location = new System.Drawing.Point(51, 58);
            this.label_OpticalPower_dBm.Name = "label_OpticalPower_dBm";
            this.label_OpticalPower_dBm.Size = new System.Drawing.Size(106, 13);
            this.label_OpticalPower_dBm.TabIndex = 150;
            this.label_OpticalPower_dBm.Text = "Optical_Power_dBm:";
            // 
            // label_EtalonTX_mA
            // 
            this.label_EtalonTX_mA.AutoSize = true;
            this.label_EtalonTX_mA.Location = new System.Drawing.Point(76, 121);
            this.label_EtalonTX_mA.Name = "label_EtalonTX_mA";
            this.label_EtalonTX_mA.Size = new System.Drawing.Size(81, 13);
            this.label_EtalonTX_mA.TabIndex = 171;
            this.label_EtalonTX_mA.Text = "Etalon_TX_mA:";
            // 
            // label_EtalonRX_mA
            // 
            this.label_EtalonRX_mA.AutoSize = true;
            this.label_EtalonRX_mA.Location = new System.Drawing.Point(75, 154);
            this.label_EtalonRX_mA.Name = "label_EtalonRX_mA";
            this.label_EtalonRX_mA.Size = new System.Drawing.Size(82, 13);
            this.label_EtalonRX_mA.TabIndex = 172;
            this.label_EtalonRX_mA.Text = "Etalon_RX_mA:";
            // 
            // textBox_CTap_mA
            // 
            this.textBox_CTap_mA.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_CTap_mA.Location = new System.Drawing.Point(161, 84);
            this.textBox_CTap_mA.Name = "textBox_CTap_mA";
            this.textBox_CTap_mA.ReadOnly = true;
            this.textBox_CTap_mA.Size = new System.Drawing.Size(91, 26);
            this.textBox_CTap_mA.TabIndex = 189;
            // 
            // label_LockRatio
            // 
            this.label_LockRatio.AutoSize = true;
            this.label_LockRatio.Location = new System.Drawing.Point(93, 185);
            this.label_LockRatio.Name = "label_LockRatio";
            this.label_LockRatio.Size = new System.Drawing.Size(65, 13);
            this.label_LockRatio.TabIndex = 173;
            this.label_LockRatio.Text = "Lock_Ratio:";
            // 
            // label_CTap_mA
            // 
            this.label_CTap_mA.AutoSize = true;
            this.label_CTap_mA.Location = new System.Drawing.Point(97, 93);
            this.label_CTap_mA.Name = "label_CTap_mA";
            this.label_CTap_mA.Size = new System.Drawing.Size(57, 13);
            this.label_CTap_mA.TabIndex = 190;
            this.label_CTap_mA.Text = "CTap_mA:";
            // 
            // label_Filter_mA
            // 
            this.label_Filter_mA.AutoSize = true;
            this.label_Filter_mA.Location = new System.Drawing.Point(46, 216);
            this.label_Filter_mA.Name = "label_Filter_mA";
            this.label_Filter_mA.Size = new System.Drawing.Size(110, 13);
            this.label_Filter_mA.TabIndex = 179;
            this.label_Filter_mA.Text = "OpticalBox_Filter_mA:";
            // 
            // textBox_Frequency_GHz
            // 
            this.textBox_Frequency_GHz.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_Frequency_GHz.Location = new System.Drawing.Point(161, 23);
            this.textBox_Frequency_GHz.Name = "textBox_Frequency_GHz";
            this.textBox_Frequency_GHz.ReadOnly = true;
            this.textBox_Frequency_GHz.Size = new System.Drawing.Size(91, 26);
            this.textBox_Frequency_GHz.TabIndex = 135;
            // 
            // textBox_PowerRatio
            // 
            this.textBox_PowerRatio.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_PowerRatio.Location = new System.Drawing.Point(161, 270);
            this.textBox_PowerRatio.Name = "textBox_PowerRatio";
            this.textBox_PowerRatio.ReadOnly = true;
            this.textBox_PowerRatio.Size = new System.Drawing.Size(91, 26);
            this.textBox_PowerRatio.TabIndex = 176;
            // 
            // textBox_OpticalPower_dBm
            // 
            this.textBox_OpticalPower_dBm.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_OpticalPower_dBm.Location = new System.Drawing.Point(161, 54);
            this.textBox_OpticalPower_dBm.Name = "textBox_OpticalPower_dBm";
            this.textBox_OpticalPower_dBm.ReadOnly = true;
            this.textBox_OpticalPower_dBm.Size = new System.Drawing.Size(91, 26);
            this.textBox_OpticalPower_dBm.TabIndex = 137;
            // 
            // textBox_Reference_mA
            // 
            this.textBox_Reference_mA.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_Reference_mA.Location = new System.Drawing.Point(161, 239);
            this.textBox_Reference_mA.Name = "textBox_Reference_mA";
            this.textBox_Reference_mA.ReadOnly = true;
            this.textBox_Reference_mA.Size = new System.Drawing.Size(91, 26);
            this.textBox_Reference_mA.TabIndex = 175;
            // 
            // textBox_EtalonTX_mA
            // 
            this.textBox_EtalonTX_mA.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_EtalonTX_mA.Location = new System.Drawing.Point(161, 115);
            this.textBox_EtalonTX_mA.Name = "textBox_EtalonTX_mA";
            this.textBox_EtalonTX_mA.ReadOnly = true;
            this.textBox_EtalonTX_mA.Size = new System.Drawing.Size(91, 26);
            this.textBox_EtalonTX_mA.TabIndex = 166;
            // 
            // textBox_Filter_mA
            // 
            this.textBox_Filter_mA.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_Filter_mA.Location = new System.Drawing.Point(161, 208);
            this.textBox_Filter_mA.Name = "textBox_Filter_mA";
            this.textBox_Filter_mA.ReadOnly = true;
            this.textBox_Filter_mA.Size = new System.Drawing.Size(91, 26);
            this.textBox_Filter_mA.TabIndex = 174;
            // 
            // textBox_EtalonRX_mA
            // 
            this.textBox_EtalonRX_mA.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_EtalonRX_mA.Location = new System.Drawing.Point(161, 146);
            this.textBox_EtalonRX_mA.Name = "textBox_EtalonRX_mA";
            this.textBox_EtalonRX_mA.ReadOnly = true;
            this.textBox_EtalonRX_mA.Size = new System.Drawing.Size(91, 26);
            this.textBox_EtalonRX_mA.TabIndex = 167;
            // 
            // textBox_LockRatio
            // 
            this.textBox_LockRatio.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_LockRatio.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.textBox_LockRatio.Location = new System.Drawing.Point(161, 177);
            this.textBox_LockRatio.Name = "textBox_LockRatio";
            this.textBox_LockRatio.ReadOnly = true;
            this.textBox_LockRatio.Size = new System.Drawing.Size(91, 26);
            this.textBox_LockRatio.TabIndex = 168;
            // 
            // groupBox_DUTInfo
            // 
            this.groupBox_DUTInfo.Controls.Add(this.button_LoadChannels);
            this.groupBox_DUTInfo.Controls.Add(this.comboBox_TargetStages);
            this.groupBox_DUTInfo.Controls.Add(this.comboBox_PartCodes);
            this.groupBox_DUTInfo.Controls.Add(this.comboBox_PCASFileKeys);
            this.groupBox_DUTInfo.Controls.Add(this.label_SerialNO);
            this.groupBox_DUTInfo.Controls.Add(this.textBox_SerialNO);
            this.groupBox_DUTInfo.Controls.Add(this.label_TargetStage);
            this.groupBox_DUTInfo.Controls.Add(this.label_PartCode);
            this.groupBox_DUTInfo.Controls.Add(this.label_PCASFileKey);
            this.groupBox_DUTInfo.Location = new System.Drawing.Point(20, 11);
            this.groupBox_DUTInfo.Name = "groupBox_DUTInfo";
            this.groupBox_DUTInfo.Size = new System.Drawing.Size(245, 180);
            this.groupBox_DUTInfo.TabIndex = 191;
            this.groupBox_DUTInfo.TabStop = false;
            this.groupBox_DUTInfo.Text = "DUT Info";
            // 
            // button_LoadChannels
            // 
            this.button_LoadChannels.BackColor = System.Drawing.Color.CornflowerBlue;
            this.button_LoadChannels.Location = new System.Drawing.Point(105, 129);
            this.button_LoadChannels.Name = "button_LoadChannels";
            this.button_LoadChannels.Size = new System.Drawing.Size(117, 36);
            this.button_LoadChannels.TabIndex = 208;
            this.button_LoadChannels.Text = "Load Channels";
            this.button_LoadChannels.UseVisualStyleBackColor = false;
            this.button_LoadChannels.Click += new System.EventHandler(this.button_LoadChannels_Click);
            // 
            // comboBox_TargetStages
            // 
            this.comboBox_TargetStages.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_TargetStages.FormattingEnabled = true;
            this.comboBox_TargetStages.Items.AddRange(new object[] {
            "final",
            "final_ot"});
            this.comboBox_TargetStages.Location = new System.Drawing.Point(105, 50);
            this.comboBox_TargetStages.Name = "comboBox_TargetStages";
            this.comboBox_TargetStages.Size = new System.Drawing.Size(118, 21);
            this.comboBox_TargetStages.TabIndex = 165;
            this.comboBox_TargetStages.SelectedIndexChanged += new System.EventHandler(this.comboBox_TargetStages_SelectedIndexChanged);
            // 
            // comboBox_PartCodes
            // 
            this.comboBox_PartCodes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_PartCodes.FormattingEnabled = true;
            this.comboBox_PartCodes.Location = new System.Drawing.Point(105, 74);
            this.comboBox_PartCodes.Name = "comboBox_PartCodes";
            this.comboBox_PartCodes.Size = new System.Drawing.Size(118, 21);
            this.comboBox_PartCodes.TabIndex = 164;
            this.comboBox_PartCodes.Enter += new System.EventHandler(this.comboBox_PartCodes_Enter);
            // 
            // comboBox_PCASFileKeys
            // 
            this.comboBox_PCASFileKeys.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_PCASFileKeys.FormattingEnabled = true;
            this.comboBox_PCASFileKeys.Location = new System.Drawing.Point(105, 101);
            this.comboBox_PCASFileKeys.Name = "comboBox_PCASFileKeys";
            this.comboBox_PCASFileKeys.Size = new System.Drawing.Size(118, 21);
            this.comboBox_PCASFileKeys.TabIndex = 163;
            this.comboBox_PCASFileKeys.Enter += new System.EventHandler(this.comboBox_PCASFileKeys_Enter);
            // 
            // label_SerialNO
            // 
            this.label_SerialNO.AutoSize = true;
            this.label_SerialNO.Location = new System.Drawing.Point(41, 30);
            this.label_SerialNO.Name = "label_SerialNO";
            this.label_SerialNO.Size = new System.Drawing.Size(58, 13);
            this.label_SerialNO.TabIndex = 155;
            this.label_SerialNO.Text = "Serial_NO:";
            // 
            // textBox_SerialNO
            // 
            this.textBox_SerialNO.Location = new System.Drawing.Point(105, 26);
            this.textBox_SerialNO.Name = "textBox_SerialNO";
            this.textBox_SerialNO.Size = new System.Drawing.Size(118, 20);
            this.textBox_SerialNO.TabIndex = 153;
            this.textBox_SerialNO.TextChanged += new System.EventHandler(this.textBox_SerialNO_TextChanged);
            // 
            // label_TargetStage
            // 
            this.label_TargetStage.AutoSize = true;
            this.label_TargetStage.Location = new System.Drawing.Point(25, 55);
            this.label_TargetStage.Name = "label_TargetStage";
            this.label_TargetStage.Size = new System.Drawing.Size(75, 13);
            this.label_TargetStage.TabIndex = 158;
            this.label_TargetStage.Text = "Target_Stage:";
            // 
            // label_PartCode
            // 
            this.label_PartCode.AutoSize = true;
            this.label_PartCode.Location = new System.Drawing.Point(40, 80);
            this.label_PartCode.Name = "label_PartCode";
            this.label_PartCode.Size = new System.Drawing.Size(60, 13);
            this.label_PartCode.TabIndex = 160;
            this.label_PartCode.Text = "Part_Code:";
            // 
            // label_PCASFileKey
            // 
            this.label_PCASFileKey.AutoSize = true;
            this.label_PCASFileKey.Location = new System.Drawing.Point(16, 103);
            this.label_PCASFileKey.Name = "label_PCASFileKey";
            this.label_PCASFileKey.Size = new System.Drawing.Size(84, 13);
            this.label_PCASFileKey.TabIndex = 162;
            this.label_PCASFileKey.Text = "PCAS_File_Key:";
            // 
            // button_PowerOnOff
            // 
            this.button_PowerOnOff.BackColor = System.Drawing.Color.Gold;
            this.button_PowerOnOff.Location = new System.Drawing.Point(512, 422);
            this.button_PowerOnOff.Name = "button_PowerOnOff";
            this.button_PowerOnOff.Size = new System.Drawing.Size(122, 37);
            this.button_PowerOnOff.TabIndex = 208;
            this.button_PowerOnOff.Text = "Power is On";
            this.button_PowerOnOff.UseVisualStyleBackColor = false;
            this.button_PowerOnOff.Click += new System.EventHandler(this.button_PowerOnOff_Click);
            // 
            // groupBox_TemperatureSetting
            // 
            this.groupBox_TemperatureSetting.Controls.Add(this.label_LaserTemperatureSetting_C);
            this.groupBox_TemperatureSetting.Controls.Add(this.textBox_LaserTemperatureSetting_C);
            this.groupBox_TemperatureSetting.Controls.Add(this.textBox_CaseTemperatureSetting_C);
            this.groupBox_TemperatureSetting.Controls.Add(this.label_CaseTemperatureSetting_C);
            this.groupBox_TemperatureSetting.Location = new System.Drawing.Point(284, 379);
            this.groupBox_TemperatureSetting.Name = "groupBox_TemperatureSetting";
            this.groupBox_TemperatureSetting.Size = new System.Drawing.Size(211, 80);
            this.groupBox_TemperatureSetting.TabIndex = 191;
            this.groupBox_TemperatureSetting.TabStop = false;
            this.groupBox_TemperatureSetting.Text = "Temperature Setting";
            // 
            // label_LaserTemperatureSetting_C
            // 
            this.label_LaserTemperatureSetting_C.AutoSize = true;
            this.label_LaserTemperatureSetting_C.Location = new System.Drawing.Point(15, 28);
            this.label_LaserTemperatureSetting_C.Name = "label_LaserTemperatureSetting_C";
            this.label_LaserTemperatureSetting_C.Size = new System.Drawing.Size(115, 13);
            this.label_LaserTemperatureSetting_C.TabIndex = 155;
            this.label_LaserTemperatureSetting_C.Text = "Laser_Temperature_C:";
            // 
            // textBox_LaserTemperatureSetting_C
            // 
            this.textBox_LaserTemperatureSetting_C.Location = new System.Drawing.Point(137, 27);
            this.textBox_LaserTemperatureSetting_C.Name = "textBox_LaserTemperatureSetting_C";
            this.textBox_LaserTemperatureSetting_C.Size = new System.Drawing.Size(50, 20);
            this.textBox_LaserTemperatureSetting_C.TabIndex = 153;
            // 
            // textBox_CaseTemperatureSetting_C
            // 
            this.textBox_CaseTemperatureSetting_C.Location = new System.Drawing.Point(137, 50);
            this.textBox_CaseTemperatureSetting_C.Name = "textBox_CaseTemperatureSetting_C";
            this.textBox_CaseTemperatureSetting_C.Size = new System.Drawing.Size(50, 20);
            this.textBox_CaseTemperatureSetting_C.TabIndex = 156;
            // 
            // label_CaseTemperatureSetting_C
            // 
            this.label_CaseTemperatureSetting_C.AutoSize = true;
            this.label_CaseTemperatureSetting_C.Location = new System.Drawing.Point(18, 49);
            this.label_CaseTemperatureSetting_C.Name = "label_CaseTemperatureSetting_C";
            this.label_CaseTemperatureSetting_C.Size = new System.Drawing.Size(113, 13);
            this.label_CaseTemperatureSetting_C.TabIndex = 158;
            this.label_CaseTemperatureSetting_C.Text = "Case_Temperature_C:";
            // 
            // groupBox_Sweep
            // 
            this.groupBox_Sweep.Controls.Add(this.btn_ResetSoa);
            this.groupBox_Sweep.Controls.Add(this.txt_ResetSoa);
            this.groupBox_Sweep.Controls.Add(this.label9);
            this.groupBox_Sweep.Controls.Add(this.btn_PowerOffLaserByOnePoint);
            this.groupBox_Sweep.Controls.Add(this.label8);
            this.groupBox_Sweep.Controls.Add(this.txtBox_RearCutScan_Step);
            this.groupBox_Sweep.Controls.Add(this.label5);
            this.groupBox_Sweep.Controls.Add(this.btn_RearCutScan);
            this.groupBox_Sweep.Controls.Add(this.txtBox_RearCutScan_To);
            this.groupBox_Sweep.Controls.Add(this.label6);
            this.groupBox_Sweep.Controls.Add(this.txtBox_RearCutScan_From);
            this.groupBox_Sweep.Controls.Add(this.label7);
            this.groupBox_Sweep.Controls.Add(this.txtBox_PhaseCutScan_Step);
            this.groupBox_Sweep.Controls.Add(this.label4);
            this.groupBox_Sweep.Controls.Add(this.btn_PhaseCutScan);
            this.groupBox_Sweep.Controls.Add(this.txtBox_PhaseCutScan_To);
            this.groupBox_Sweep.Controls.Add(this.label3);
            this.groupBox_Sweep.Controls.Add(this.txtBox_PhaseCutScan_From);
            this.groupBox_Sweep.Controls.Add(this.label2);
            this.groupBox_Sweep.Controls.Add(this.label1);
            this.groupBox_Sweep.Controls.Add(this.btn_PowerUpLaserByOnePoint);
            this.groupBox_Sweep.Location = new System.Drawing.Point(805, 10);
            this.groupBox_Sweep.Name = "groupBox_Sweep";
            this.groupBox_Sweep.Size = new System.Drawing.Size(423, 448);
            this.groupBox_Sweep.TabIndex = 209;
            this.groupBox_Sweep.TabStop = false;
            this.groupBox_Sweep.Text = "Sweep";
            // 
            // btn_ResetSoa
            // 
            this.btn_ResetSoa.Enabled = false;
            this.btn_ResetSoa.Location = new System.Drawing.Point(351, 26);
            this.btn_ResetSoa.Name = "btn_ResetSoa";
            this.btn_ResetSoa.Size = new System.Drawing.Size(59, 26);
            this.btn_ResetSoa.TabIndex = 134;
            this.btn_ResetSoa.Text = "reset soa";
            this.btn_ResetSoa.UseVisualStyleBackColor = true;
            this.btn_ResetSoa.Click += new System.EventHandler(this.btn_ResetSoa_Click);
            // 
            // txt_ResetSoa
            // 
            this.txt_ResetSoa.Location = new System.Drawing.Point(295, 27);
            this.txt_ResetSoa.Name = "txt_ResetSoa";
            this.txt_ResetSoa.Size = new System.Drawing.Size(50, 20);
            this.txt_ResetSoa.TabIndex = 133;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(242, 32);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(47, 13);
            this.label9.TabIndex = 132;
            this.label9.Text = "Soa_mA";
            // 
            // btn_PowerOffLaserByOnePoint
            // 
            this.btn_PowerOffLaserByOnePoint.Location = new System.Drawing.Point(118, 25);
            this.btn_PowerOffLaserByOnePoint.Name = "btn_PowerOffLaserByOnePoint";
            this.btn_PowerOffLaserByOnePoint.Size = new System.Drawing.Size(93, 26);
            this.btn_PowerOffLaserByOnePoint.TabIndex = 131;
            this.btn_PowerOffLaserByOnePoint.Text = "power Off laser";
            this.btn_PowerOffLaserByOnePoint.UseVisualStyleBackColor = true;
            this.btn_PowerOffLaserByOnePoint.Click += new System.EventHandler(this.btn_PowerOffLaserByOnePoint_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 126);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(69, 13);
            this.label8.TabIndex = 130;
            this.label8.Text = "rear cut scan";
            // 
            // txtBox_RearCutScan_Step
            // 
            this.txtBox_RearCutScan_Step.Location = new System.Drawing.Point(245, 144);
            this.txtBox_RearCutScan_Step.Name = "txtBox_RearCutScan_Step";
            this.txtBox_RearCutScan_Step.Size = new System.Drawing.Size(50, 20);
            this.txtBox_RearCutScan_Step.TabIndex = 129;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(212, 148);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(27, 13);
            this.label5.TabIndex = 128;
            this.label5.Text = "step";
            // 
            // btn_RearCutScan
            // 
            this.btn_RearCutScan.Enabled = false;
            this.btn_RearCutScan.Location = new System.Drawing.Point(327, 141);
            this.btn_RearCutScan.Name = "btn_RearCutScan";
            this.btn_RearCutScan.Size = new System.Drawing.Size(90, 26);
            this.btn_RearCutScan.TabIndex = 127;
            this.btn_RearCutScan.Text = "rear cut scan";
            this.btn_RearCutScan.UseVisualStyleBackColor = true;
            this.btn_RearCutScan.Click += new System.EventHandler(this.btn_RearCutScan_Click);
            // 
            // txtBox_RearCutScan_To
            // 
            this.txtBox_RearCutScan_To.Location = new System.Drawing.Point(156, 143);
            this.txtBox_RearCutScan_To.Name = "txtBox_RearCutScan_To";
            this.txtBox_RearCutScan_To.Size = new System.Drawing.Size(50, 20);
            this.txtBox_RearCutScan_To.TabIndex = 126;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(115, 147);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 13);
            this.label6.TabIndex = 125;
            this.label6.Text = "to(mA)";
            // 
            // txtBox_RearCutScan_From
            // 
            this.txtBox_RearCutScan_From.Location = new System.Drawing.Point(59, 143);
            this.txtBox_RearCutScan_From.Name = "txtBox_RearCutScan_From";
            this.txtBox_RearCutScan_From.Size = new System.Drawing.Size(50, 20);
            this.txtBox_RearCutScan_From.TabIndex = 124;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 148);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 13);
            this.label7.TabIndex = 123;
            this.label7.Text = "from(mA)";
            // 
            // txtBox_PhaseCutScan_Step
            // 
            this.txtBox_PhaseCutScan_Step.Location = new System.Drawing.Point(245, 92);
            this.txtBox_PhaseCutScan_Step.Name = "txtBox_PhaseCutScan_Step";
            this.txtBox_PhaseCutScan_Step.Size = new System.Drawing.Size(50, 20);
            this.txtBox_PhaseCutScan_Step.TabIndex = 122;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(212, 96);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 13);
            this.label4.TabIndex = 121;
            this.label4.Text = "step";
            // 
            // btn_PhaseCutScan
            // 
            this.btn_PhaseCutScan.Enabled = false;
            this.btn_PhaseCutScan.Location = new System.Drawing.Point(327, 89);
            this.btn_PhaseCutScan.Name = "btn_PhaseCutScan";
            this.btn_PhaseCutScan.Size = new System.Drawing.Size(90, 26);
            this.btn_PhaseCutScan.TabIndex = 120;
            this.btn_PhaseCutScan.Text = "phase cut scan";
            this.btn_PhaseCutScan.UseVisualStyleBackColor = true;
            this.btn_PhaseCutScan.Click += new System.EventHandler(this.btn_PhaseCutScan_Click);
            // 
            // txtBox_PhaseCutScan_To
            // 
            this.txtBox_PhaseCutScan_To.Location = new System.Drawing.Point(156, 91);
            this.txtBox_PhaseCutScan_To.Name = "txtBox_PhaseCutScan_To";
            this.txtBox_PhaseCutScan_To.Size = new System.Drawing.Size(50, 20);
            this.txtBox_PhaseCutScan_To.TabIndex = 119;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(115, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 118;
            this.label3.Text = "to(mA)";
            // 
            // txtBox_PhaseCutScan_From
            // 
            this.txtBox_PhaseCutScan_From.Location = new System.Drawing.Point(59, 91);
            this.txtBox_PhaseCutScan_From.Name = "txtBox_PhaseCutScan_From";
            this.txtBox_PhaseCutScan_From.Size = new System.Drawing.Size(50, 20);
            this.txtBox_PhaseCutScan_From.TabIndex = 117;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 96);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "from(mA)";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "phase cut scan";
            // 
            // btn_PowerUpLaserByOnePoint
            // 
            this.btn_PowerUpLaserByOnePoint.Location = new System.Drawing.Point(6, 25);
            this.btn_PowerUpLaserByOnePoint.Name = "btn_PowerUpLaserByOnePoint";
            this.btn_PowerUpLaserByOnePoint.Size = new System.Drawing.Size(93, 26);
            this.btn_PowerUpLaserByOnePoint.TabIndex = 0;
            this.btn_PowerUpLaserByOnePoint.Text = "power on laser";
            this.btn_PowerUpLaserByOnePoint.UseVisualStyleBackColor = true;
            this.btn_PowerUpLaserByOnePoint.Click += new System.EventHandler(this.btn_PowerUpLaserByOnePoint_Click);
            // 
            // Prog_HITT_ToolGui
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox_Sweep);
            this.Controls.Add(this.groupBox_TemperatureSetting);
            this.Controls.Add(this.button_PowerOnOff);
            this.Controls.Add(this.groupBox_DUTInfo);
            this.Controls.Add(this.groupBox_Measurement);
            this.Controls.Add(this.groupBox_ChannelsInfo);
            this.Controls.Add(this.groupBox_MZSetup);
            this.Controls.Add(this.groupBox_LaserSetup);
            this.Controls.Add(this.groupBox_OpticalLineSwitch);
            this.Controls.Add(this.button_LaserOnOff);
            this.Name = "Prog_HITT_ToolGui";
            this.Size = new System.Drawing.Size(1243, 477);
            this.MsgReceived += new Bookham.TestEngine.Framework.Messages.ManagedCtlBase.MsgReceivedDlg(this.ProgramGui_MsgReceived);
            this.groupBox_OpticalLineSwitch.ResumeLayout(false);
            this.groupBox_OpticalLineSwitch.PerformLayout();
            this.groupBox_LaserSetup.ResumeLayout(false);
            this.groupBox_LaserSetup.PerformLayout();
            this.groupBox_MZSetup.ResumeLayout(false);
            this.groupBox_MZSetup.PerformLayout();
            this.groupBox_ChannelsInfo.ResumeLayout(false);
            this.groupBox_ChannelsInfo.PerformLayout();
            this.groupBox_Measurement.ResumeLayout(false);
            this.groupBox_Measurement.PerformLayout();
            this.groupBox_DUTInfo.ResumeLayout(false);
            this.groupBox_DUTInfo.PerformLayout();
            this.groupBox_TemperatureSetting.ResumeLayout(false);
            this.groupBox_TemperatureSetting.PerformLayout();
            this.groupBox_Sweep.ResumeLayout(false);
            this.groupBox_Sweep.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox_OpticalLineSwitch;
        private System.Windows.Forms.RadioButton radioButton_Relay_DUT;
        private System.Windows.Forms.RadioButton radioButton_Relay_OpticalBox;
        private System.Windows.Forms.Label label_ChannelInfo_Index;
        private System.Windows.Forms.TextBox textBox_ChannelInfo_ITU_GHz;
        private System.Windows.Forms.TextBox button_RearSOA_mA;
        private System.Windows.Forms.TextBox button_MZ_Right_IMB_mA;
        private System.Windows.Forms.TextBox button_MZ_Left_IMB_mA;
        private System.Windows.Forms.TextBox button_MZ_Right_Bias_V;
        private System.Windows.Forms.TextBox button_MZ_Left_Bias_V;
        private System.Windows.Forms.TextBox button_Phase_mA;
        private System.Windows.Forms.TextBox button_NonConstF_mA;
        private System.Windows.Forms.TextBox button_ConstF_mA;
        private System.Windows.Forms.TextBox button_Rear_mA;
        private System.Windows.Forms.TextBox button_SOA_mA;
        private System.Windows.Forms.TextBox button_Gain_mA;
        private System.Windows.Forms.ComboBox comboBox_ChannelInfo_Indexes;
        private System.Windows.Forms.Label label_RearSOA_mA;
        private System.Windows.Forms.Label label_MZ_Right_IMB_mA;
        private System.Windows.Forms.Label label_MZ_Left_IMB_mA;
        private System.Windows.Forms.Label label_MZ_Right_Bias_V;
        private System.Windows.Forms.Label label_MZ_Left_Bias_V;
        private System.Windows.Forms.Label label_NonConstF_mA;
        private System.Windows.Forms.Label label_ConstF_mA;
        private System.Windows.Forms.Label label_FrontPair;
        private System.Windows.Forms.Label label_Phase_mA;
        private System.Windows.Forms.Label label_Rear_mA;
        private System.Windows.Forms.Label label_SOA_mA;
        private System.Windows.Forms.Label label_Gain_mA;
        private System.Windows.Forms.ComboBox comboBox_FrontPairs;
        private System.Windows.Forms.Button button_LaserOnOff;
        private System.Windows.Forms.Label label_ITU_GHz;
        private System.Windows.Forms.ComboBox comboBox_ChannelInfo_MZWorkPoints;
        private System.Windows.Forms.Label label_ChannelInfo_MZWorkPoint;
        private System.Windows.Forms.TextBox textBox_ChannelInfo_LockRatio;
        private System.Windows.Forms.TextBox textBox_ChannelInfo_RX_mA;
        private System.Windows.Forms.TextBox textBox_ChannelInfo_TX_mA;
        private System.Windows.Forms.TextBox textBox_ChannelInfo_SuperMode;
        private System.Windows.Forms.Label label_ChannelInfo_LockRatio;
        private System.Windows.Forms.Label label_ChannelInfo_RX_mA;
        private System.Windows.Forms.Label label_ChannelInfo_TX_mA;
        private System.Windows.Forms.Label label_ChannelInfo_SuperMode;
        private System.Windows.Forms.TextBox textBox_ChannelInfo_CTap_Peak_mA;
        private System.Windows.Forms.TextBox textBox_ChannelInfo_CTap_Quad_mA;
        private System.Windows.Forms.Label label_ChannelInfo_CTap_Peak_mA;
        private System.Windows.Forms.Label label_ChannelInfo_CTap_Quad_mA;
        private System.Windows.Forms.GroupBox groupBox_LaserSetup;
        private System.Windows.Forms.GroupBox groupBox_MZSetup;
        private System.Windows.Forms.GroupBox groupBox_ChannelsInfo;
        private System.Windows.Forms.GroupBox groupBox_Measurement;
        private System.Windows.Forms.Label label_Reference_mA;
        private System.Windows.Forms.Label label_PowerRatio;
        private System.Windows.Forms.Label label_EtalonTX_mA;
        private System.Windows.Forms.Label label_EtalonRX_mA;
        private System.Windows.Forms.Label label_LockRatio;
        private System.Windows.Forms.Label label_Filter_mA;
        private System.Windows.Forms.TextBox textBox_Frequency_GHz;
        private System.Windows.Forms.TextBox textBox_OpticalPower_dBm;
        private System.Windows.Forms.TextBox textBox_EtalonTX_mA;
        private System.Windows.Forms.TextBox textBox_EtalonRX_mA;
        private System.Windows.Forms.TextBox textBox_LockRatio;
        private System.Windows.Forms.TextBox textBox_Filter_mA;
        private System.Windows.Forms.TextBox textBox_Reference_mA;
        private System.Windows.Forms.TextBox textBox_PowerRatio;
        private System.Windows.Forms.Label label_CTap_mA;
        private System.Windows.Forms.TextBox textBox_CTap_mA;
        private System.Windows.Forms.Label label_OpticalPower_dBm;
        private System.Windows.Forms.Label label_Frequency_GHz;
        private System.Windows.Forms.GroupBox groupBox_DUTInfo;
        private System.Windows.Forms.ComboBox comboBox_PCASFileKeys;
        private System.Windows.Forms.Label label_SerialNO;
        private System.Windows.Forms.TextBox textBox_SerialNO;
        private System.Windows.Forms.Label label_TargetStage;
        private System.Windows.Forms.Label label_PartCode;
        private System.Windows.Forms.Label label_PCASFileKey;
        private System.Windows.Forms.ComboBox comboBox_TargetStages;
        private System.Windows.Forms.ComboBox comboBox_PartCodes;
        private System.Windows.Forms.Button button_LoadChannels;
        private System.Windows.Forms.Button button_PowerOnOff;
        private System.Windows.Forms.GroupBox groupBox_TemperatureSetting;
        private System.Windows.Forms.Label label_LaserTemperatureSetting_C;
        private System.Windows.Forms.TextBox textBox_LaserTemperatureSetting_C;
        private System.Windows.Forms.TextBox textBox_CaseTemperatureSetting_C;
        private System.Windows.Forms.Label label_CaseTemperatureSetting_C;
        private System.Windows.Forms.Label label_LaserTemperatureReading_C;
        private System.Windows.Forms.Label label_CaseTemperatureReading_C;
        private System.Windows.Forms.TextBox textBox_CaseTemperatureReading_C;
        private System.Windows.Forms.TextBox textBox_LaserTemperatureReading_C;
        private System.Windows.Forms.GroupBox groupBox_Sweep;
        private System.Windows.Forms.Button btn_PowerUpLaserByOnePoint;
        private System.Windows.Forms.TextBox txtBox_PhaseCutScan_Step;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_PhaseCutScan;
        private System.Windows.Forms.TextBox txtBox_PhaseCutScan_To;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBox_PhaseCutScan_From;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtBox_RearCutScan_Step;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btn_RearCutScan;
        private System.Windows.Forms.TextBox txtBox_RearCutScan_To;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtBox_RearCutScan_From;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btn_PowerOffLaserByOnePoint;
        private System.Windows.Forms.Button btn_ResetSoa;
        private System.Windows.Forms.TextBox txt_ResetSoa;
        private System.Windows.Forms.Label label9;

    }
}

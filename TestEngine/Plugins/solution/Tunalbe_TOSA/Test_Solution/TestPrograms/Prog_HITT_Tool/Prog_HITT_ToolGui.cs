// Author: chongjian.liang 2016.07.06

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.IO;
using System.Windows.Forms;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Framework.Messages;
using Bookham.TestSolution.ILMZFinalData;
using Bookham.ToolKit.Mz;
using Bookham.TestSolution.IlmzCommonUtils;
using Bookham.TestSolution.IlmzCommonData;
using Bookham.TestSolution.IlmzCommonInstrs;
using Bookham.TestLibrary.Algorithms;
using Bookham.TestLibrary.InstrTypes;

namespace Bookham.TestSolution.TestPrograms
{
    public partial class Prog_HITT_ToolGui : ManagedCtlBase
    {
        private Dictionary<string, TargetFileChannel> targetFileChannelCollection;
        private Prog_HITT_ToolInsts progInsts;
        private Prog_HITT_ToolInfo progInfo;

        private bool isToLoadDeviceTypeFromPCAS = true;

        private Timer timer_OutputReading = new Timer();
        private int READING_INTERVAL_MS = 2000;
        private bool isLaserOn = false;
        private bool isPowerOn = true;

        private double LASER_TEMPERATURE_TOL_C = 0.02;
        private double CASE_TEMPERATURE_TOL_C = 0.5;

        public Prog_HITT_ToolGui()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.comboBox_TargetStages.SelectedIndex = 0;
            this.timer_OutputReading.Interval = this.READING_INTERVAL_MS;
            this.timer_OutputReading.Tick += this.timer_OutputReading_Tick;
        }

        private void ProgramGui_MsgReceived(object payload, long inMsgSeq, long respSeq)
        {
            if (payload is Prog_HITT_ToolInfo)
            {
                this.progInfo = payload as Prog_HITT_ToolInfo;

                this.comboBox_PartCodes.DataSource = this.progInfo.PartCodeList;
                this.comboBox_PartCodes.SelectedIndex = -1;

                this.textBox_CaseTemperatureSetting_C.Text = this.progInfo.TecCase_SensorTemperatureSetPoint_C.ToString();
                this.textBox_LaserTemperatureSetting_C.Text = this.progInfo.TecDsdbr_SensorTemperatureSetPoint_C.ToString();
            }
            else if (payload is Prog_HITT_ToolInsts)
            {
                this.progInsts = payload as Prog_HITT_ToolInsts;
            }
            else if (payload is MSG_DeviceType)
            {
                string deviceType = (payload as MSG_DeviceType).DeviceType;

                if (deviceType == null)
                {
                    this.isToLoadDeviceTypeFromPCAS = true;

                    this.comboBox_PartCodes.SelectedIndex = -1;
                    this.comboBox_PartCodes.Enabled = true;

                    return;
                }

                this.isToLoadDeviceTypeFromPCAS = false;

                this.comboBox_PartCodes.SelectedItem = deviceType;
                this.comboBox_PartCodes.Enabled = true;
            }
            else if (payload is Dictionary<string, TargetFileChannel>)
            {
                this.targetFileChannelCollection = payload as Dictionary<string, TargetFileChannel>;

                this.comboBox_ChannelInfo_Indexes.Items.Clear();

                if (this.targetFileChannelCollection == null)
                {
                    return;
                }

                foreach (string targetChannelKey in this.targetFileChannelCollection.Keys)
                {
                    this.comboBox_ChannelInfo_Indexes.Items.Add(targetChannelKey);
                }

                if (this.comboBox_ChannelInfo_Indexes.Items.Count > 0)
                {
                    this.comboBox_ChannelInfo_Indexes.SelectedIndex = 0;
                }

                this.comboBox_ChannelInfo_MZWorkPoints.SelectedIndex = 0;

                this.button_LoadChannels.Text = "Load Channels";
                this.button_LoadChannels.Enabled = true;
            }
            else if (payload is MSG_OnEnding)
            {
                this.progInsts.Mz.FCU2Asic.CloseAllAsic();
                this.progInsts.Mz.FCU2Asic.VLeftModBias_Volt = 0;
                this.progInsts.Mz.FCU2Asic.VRightModBias_Volt = 0;
                this.progInsts.Fcu2AsicInstrsGroups.FcuSource.OutputEnabled = false;
                this.progInsts.Fcu2AsicInstrsGroups.AsicVccSource.OutputEnabled = false;
                this.progInsts.Fcu2AsicInstrsGroups.AsicVeeSource.OutputEnabled = false;
                this.progInsts.TecDsdbr.OutputEnabled = false;
                this.progInsts.TecCase.OutputEnabled = false;

                this.timer_OutputReading.Stop();
            }
        }

        private void textBox_SerialNO_TextChanged(object sender, System.EventArgs e)
        {
            if (this.isToLoadDeviceTypeFromPCAS == false)
            {
                this.isToLoadDeviceTypeFromPCAS = true;
            }
        }

        private void comboBox_TargetStages_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            this.comboBox_PCASFileKeys.Items.Clear();

            if (this.comboBox_TargetStages.SelectedIndex == 0)
            {
                this.comboBox_PCASFileKeys.Items.AddRange(new object[] {
                    "CHANNEL_OVERALL_FILE",
                    "CHANNEL_LASTOPTION_FILE",
                    "CHANNEL_PASS_FILE",
                    "CHANNEL_FAIL_FILE"});

                this.comboBox_PCASFileKeys.SelectedIndex = 0;
            }
            else if (this.comboBox_TargetStages.SelectedIndex == 1)
            {
                this.comboBox_PCASFileKeys.Items.Add("CHANNEL_OVER_TEMP_FILE");

                this.comboBox_PCASFileKeys.SelectedIndex = 0;
            }

            this.isToLoadDeviceTypeFromPCAS = true;
        }

        private void comboBox_PartCodes_Enter(object sender, EventArgs e)
        {
            string serialNO = this.textBox_SerialNO.Text.Trim();

            if (string.IsNullOrEmpty(serialNO))
            {
                MessageBox.Show("Please input a serial number.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.textBox_SerialNO.Focus();
                this.textBox_SerialNO.SelectAll();

                return;
            }

            if (this.comboBox_TargetStages.SelectedItem == null)
            {
                MessageBox.Show("Please select a test stage.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.comboBox_TargetStages.Focus();

                return;
            }

            if (this.isToLoadDeviceTypeFromPCAS)
            {
                this.comboBox_PartCodes.Enabled = false;

                sendToWorker(new MSG_SerialNO_Stage(serialNO, this.comboBox_TargetStages.SelectedItem as string));
            }
        }

        private void comboBox_PCASFileKeys_Enter(object sender, EventArgs e)
        {
            if (this.comboBox_TargetStages.SelectedItem == null)
            {
                MessageBox.Show("Please select a test stage.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.comboBox_TargetStages.Focus();

                return;
            }
        }
        
        private void button_LoadChannels_Click(object sender, EventArgs e)
        {
            string serialNO = this.textBox_SerialNO.Text.Trim();

            if (string.IsNullOrEmpty(serialNO))
            {
                MessageBox.Show("Please input a serial number.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.textBox_SerialNO.Focus();
                this.textBox_SerialNO.SelectAll();

                return;
            }

            if (this.comboBox_TargetStages.SelectedItem == null)
            {
                MessageBox.Show("Please select a test stage.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.comboBox_TargetStages.Focus();

                return;
            }

            if (this.isToLoadDeviceTypeFromPCAS)
            {
                this.comboBox_PartCodes.Focus();

                return;
            }

            if (this.comboBox_PCASFileKeys.SelectedItem == null)
            {
                MessageBox.Show("Please select a PCAS FILE.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.comboBox_PCASFileKeys.Focus();

                return;
            }

            this.button_LoadChannels.Text = "Please wait..";
            this.button_LoadChannels.Enabled = false;

            MSG_SerialNO_DeviceType_Stage_FileKey serialNO_DeviceType_Stage_FileKey = new MSG_SerialNO_DeviceType_Stage_FileKey(
                this.textBox_SerialNO.Text.Trim(),
                this.comboBox_PartCodes.SelectedItem as string,
                this.comboBox_TargetStages.SelectedItem as string,
                this.comboBox_PCASFileKeys.SelectedItem as string);

            sendToWorker(serialNO_DeviceType_Stage_FileKey);
        }

        private void comboBox_ChannelInfo_Indexes_SelectedIndexChanged(object sender, EventArgs e)
        {
            string targetChannelKey = this.comboBox_ChannelInfo_Indexes.SelectedItem as string;

            this.button_Gain_mA.Text = this.targetFileChannelCollection[targetChannelKey].DsdbrSetup.IGain_mA.ToString();
            this.button_Rear_mA.Text = this.targetFileChannelCollection[targetChannelKey].DsdbrSetup.IRear_mA.ToString();
            this.button_Phase_mA.Text = this.targetFileChannelCollection[targetChannelKey].DsdbrSetup.IPhase_mA.ToString();
            this.button_SOA_mA.Text = this.targetFileChannelCollection[targetChannelKey].DsdbrSetup.ISoa_mA.ToString();
            this.button_RearSOA_mA.Text = this.targetFileChannelCollection[targetChannelKey].DsdbrSetup.IRearSoa_mA.ToString();
            this.comboBox_FrontPairs.SelectedItem = this.targetFileChannelCollection[targetChannelKey].DsdbrSetup.FrontPair.ToString();
            this.button_ConstF_mA.Text = this.targetFileChannelCollection[targetChannelKey].DsdbrSetup.IFsFirst_mA.ToString();
            this.button_NonConstF_mA.Text = this.targetFileChannelCollection[targetChannelKey].DsdbrSetup.IFsSecond_mA.ToString();

            this.textBox_ChannelInfo_ITU_GHz.Text = this.targetFileChannelCollection[targetChannelKey].TestResult.ItuFreq_GHz.ToString();
            this.textBox_ChannelInfo_SuperMode.Text = this.targetFileChannelCollection[targetChannelKey].TestResult.Supermode.ToString();
            this.textBox_ChannelInfo_TX_mA.Text = this.targetFileChannelCollection[targetChannelKey].TestResult.ItxLock_mA.ToString();
            this.textBox_ChannelInfo_RX_mA.Text = this.targetFileChannelCollection[targetChannelKey].TestResult.IrxLock_mA.ToString();
            this.textBox_ChannelInfo_LockRatio.Text = this.targetFileChannelCollection[targetChannelKey].TestResult.LockRatio.ToString();
            this.textBox_ChannelInfo_CTap_Quad_mA.Text = this.targetFileChannelCollection[targetChannelKey].TestResult.TapCompPhotoCurrentQuad_mA.ToString();
            this.textBox_ChannelInfo_CTap_Peak_mA.Text = this.targetFileChannelCollection[targetChannelKey].TestResult.TapCompPhotoCurrentPeak_mA.ToString();

            this.comboBox_ChannelInfo_MZWorkPoints_SelectedIndexChanged(null, EventArgs.Empty);

            if (this.isLaserOn)
            {
                this.ChannelSetup();
            }
        }

        private void comboBox_ChannelInfo_MZWorkPoints_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.comboBox_ChannelInfo_Indexes.Items.Count == 0
                || this.comboBox_ChannelInfo_Indexes.SelectedItem == null)
            {
                return;
            }

            string targetChannelKey = this.comboBox_ChannelInfo_Indexes.SelectedItem as string;
            string selectedMZWorkPoints = comboBox_ChannelInfo_MZWorkPoints.SelectedItem as string;

            this.button_MZ_Left_IMB_mA.Text = this.targetFileChannelCollection[targetChannelKey].MzSetup.LeftArmImb_mA.ToString();
            this.button_MZ_Right_IMB_mA.Text = this.targetFileChannelCollection[targetChannelKey].MzSetup.RightArmImb_mA.ToString();

            if (comboBox_ChannelInfo_MZWorkPoints.SelectedItem as string == "Peak")
            {
                this.button_MZ_Left_Bias_V.Text = this.targetFileChannelCollection[targetChannelKey].MzSetup.LeftArmMod_Peak_V.ToString();
                this.button_MZ_Right_Bias_V.Text = this.targetFileChannelCollection[targetChannelKey].MzSetup.RightArmMod_Peak_V.ToString();
            }
            else if (comboBox_ChannelInfo_MZWorkPoints.SelectedItem as string == "Quad")
            {
                this.button_MZ_Left_Bias_V.Text = this.targetFileChannelCollection[targetChannelKey].MzSetup.LeftArmMod_Quad_V.ToString();
                this.button_MZ_Right_Bias_V.Text = this.targetFileChannelCollection[targetChannelKey].MzSetup.RightArmMod_Quad_V.ToString();
            }
            else if (comboBox_ChannelInfo_MZWorkPoints.SelectedItem as string == "Trough")
            {
                this.button_MZ_Left_Bias_V.Text = this.targetFileChannelCollection[targetChannelKey].MzSetup.LeftArmMod_Min_V.ToString();
                this.button_MZ_Right_Bias_V.Text = this.targetFileChannelCollection[targetChannelKey].MzSetup.RightArmMod_Min_V.ToString();
            }

            if (this.isLaserOn)
            {
                this.ChannelSetup();
            }
        }

        private void button_LaserOnOff_Click(object sender, EventArgs e)
        {
            if (!this.isLaserOn)
            {
                if (comboBox_ChannelInfo_Indexes.SelectedIndex == -1)
                {
                    return;
                }

                if (!this.isPowerOn)
                {
                    MessageBox.Show("Please power on firstly.");
                    
                    return;
                }

                this.isLaserOn = true;

                this.timer_OutputReading.Start();

                this.button_LaserOnOff.Text = "Laser is On";
                this.button_LaserOnOff.BackColor = Color.Yellow;

                this.ChannelSetup();
            }
            else
            {
                this.isLaserOn = false;

                this.timer_OutputReading.Stop();

                this.progInsts.Mz.FCU2Asic.CloseAllAsic();
                this.progInsts.Mz.FCU2Asic.VLeftModBias_Volt = 0;
                this.progInsts.Mz.FCU2Asic.VRightModBias_Volt = 0;

                this.button_LaserOnOff.Text = "Laser is Off";
                this.button_LaserOnOff.BackColor = Color.DarkGray;
            }
        }

        private void button_PowerOnOff_Click(object sender, EventArgs e)
        {
            this.isPowerOn = !this.isPowerOn;

            if (this.isPowerOn)
            {
                MessageBox.Show("Please load DUT, and connect the fiber to the powermeter head or wavemeter.");

                this.CaseTemperatureSetup();
                this.LaserTemperatureSetup();

                this.progInsts.Fcu2AsicInstrsGroups.FcuSource.OutputEnabled = true;
                this.progInsts.Fcu2AsicInstrsGroups.AsicVccSource.OutputEnabled = true;
                this.progInsts.Fcu2AsicInstrsGroups.AsicVeeSource.OutputEnabled = true;
                this.progInsts.TecDsdbr.OutputEnabled = true;
                this.progInsts.TecCase.OutputEnabled = true;

                int FCU2ASIC_COMMUNICATE_COUNT_MAX = 10;
                int fcu2Asic_Communicate_Count = 0;

                while (fcu2Asic_Communicate_Count++ < FCU2ASIC_COMMUNICATE_COUNT_MAX)
                {
                    try
                    {
                        this.progInsts.Mz.FCU2Asic.SetDefaultState();
                        this.progInsts.Mz.FCU2Asic.SetI2CBaud(400);

                        break;
                    }
                    catch (Exception)
                    {
                        System.Threading.Thread.Sleep(1000);
                    }
                }

                this.button_PowerOnOff.Text = "Power is On";
                this.button_PowerOnOff.BackColor = Color.Gold;
            }
            else
            {
                this.isLaserOn = false;

                this.progInsts.Mz.FCU2Asic.CloseAllAsic();
                this.progInsts.Mz.FCU2Asic.VLeftModBias_Volt = 0;
                this.progInsts.Mz.FCU2Asic.VRightModBias_Volt = 0;
                this.progInsts.Fcu2AsicInstrsGroups.FcuSource.OutputEnabled = false;
                this.progInsts.Fcu2AsicInstrsGroups.AsicVccSource.OutputEnabled = false;
                this.progInsts.Fcu2AsicInstrsGroups.AsicVeeSource.OutputEnabled = false;
                this.progInsts.TecDsdbr.OutputEnabled = false;
                this.progInsts.TecCase.OutputEnabled = false;

                this.timer_OutputReading.Stop();

                this.button_LaserOnOff.Text = "Laser is Off";
                this.button_LaserOnOff.BackColor = Color.DarkGray;

                this.button_PowerOnOff.Text = "Power is Off";
                this.button_PowerOnOff.BackColor = Color.DimGray;
            }
        }

        private void timer_OutputReading_Tick(object sender, EventArgs e)
        {
            this.textBox_CTap_mA.Text = this.progInsts.Mz.FCU2Asic.ICtap_mA.ToString();

            double tx_mA = this.progInsts.Mz.FCU2Asic.Locker_Tx_mA(true, false, true);
            this.textBox_EtalonTX_mA.Text = tx_mA.ToString();

            double rx_mA = this.progInsts.Mz.FCU2Asic.Locker_Rx_mA(true, false, true);
            this.textBox_EtalonRX_mA.Text = rx_mA.ToString();
            this.textBox_LockRatio.Text = ((double)(tx_mA / rx_mA)).ToString();

            double freq_GHz = Measurements.Wavemeter.Frequency_GHz;
            this.textBox_Frequency_GHz.Text = freq_GHz.ToString();

            this.progInsts.Mz.PowerMeter.Wavelength_nm = Alg_FreqWlConvert.Freq_GHz_TO_Wave_nm(freq_GHz);
            this.textBox_OpticalPower_dBm.Text = this.progInsts.OpmReference.ReadPower().ToString();

            this.textBox_LaserTemperatureReading_C.Text = this.progInsts.TecDsdbr.SensorTemperatureActual_C.ToString();
            this.textBox_CaseTemperatureReading_C.Text = this.progInsts.TecCase.SensorTemperatureActual_C.ToString();

            if (Math.Abs(this.progInsts.TecDsdbr.SensorTemperatureActual_C - this.progInsts.TecDsdbr.SensorTemperatureSetPoint_C) <= this.LASER_TEMPERATURE_TOL_C)
            {
                this.textBox_LaserTemperatureReading_C.BackColor = SystemColors.Control;
            }
            else
            {
                this.textBox_LaserTemperatureReading_C.BackColor = Color.Red;
            }

            if (Math.Abs(this.progInsts.TecCase.SensorTemperatureActual_C - this.progInsts.TecCase.SensorTemperatureSetPoint_C) <= this.CASE_TEMPERATURE_TOL_C)
            {
                this.textBox_CaseTemperatureReading_C.BackColor = SystemColors.Control;
            }
            else
            {
                this.textBox_CaseTemperatureReading_C.BackColor = Color.Red;
            }
        }

        private void ChannelSetup()
        {
            this.CaseTemperatureSetup();

            this.LaserSetup();
            this.MZSetup();

            this.LaserTemperatureSetup();
        }

        public void LaserSetup()
        {
            DsdbrChannelSetup channelSetup = new DsdbrChannelSetup();
            try
            {
                channelSetup.IGain_mA = double.Parse(this.button_Gain_mA.Text.Trim());
                channelSetup.IRear_mA = double.Parse(this.button_Rear_mA.Text.Trim());
                channelSetup.IPhase_mA = double.Parse(this.button_Phase_mA.Text.Trim());
                channelSetup.ISoa_mA = double.Parse(this.button_SOA_mA.Text.Trim());
                channelSetup.IRearSoa_mA = double.Parse(this.button_RearSOA_mA.Text.Trim());
                channelSetup.FrontPair = int.Parse(this.comboBox_FrontPairs.Text.Trim());
                channelSetup.IFsFirst_mA = double.Parse(this.button_ConstF_mA.Text.Trim());
                channelSetup.IFsSecond_mA = double.Parse(this.button_NonConstF_mA.Text.Trim());

                if (targetFileChannelCollection == null)
                {
                    DsdbrUtils.SetDsdbrCurrents_mA(channelSetup);
                }
                else
                {
                    DsdbrUtils.SetDsdbrCurrents_mA(this.targetFileChannelCollection[this.comboBox_ChannelInfo_Indexes.SelectedItem as string].DsdbrSetup);
                }
            }
            catch (Exception ex) 
            {
                MessageBox.Show("please check laser current format,is it numberic?");
            }
        }

        public void MZSetup()
        {
            float leftArmMod_V = float.Parse(this.button_MZ_Left_Bias_V.Text);
            float rightArmMod_V = float.Parse(this.button_MZ_Right_Bias_V.Text);
            float leftArmImb_mA = float.Parse(this.button_MZ_Left_IMB_mA.Text);
            float rightArmImb_mA = float.Parse(this.button_MZ_Right_IMB_mA.Text);

            this.progInsts.Mz.FCU2Asic.VLeftModBias_Volt = leftArmMod_V;
            this.progInsts.Mz.FCU2Asic.VRightModBias_Volt = rightArmMod_V;
            this.progInsts.Mz.FCU2Asic.IimbLeft_mA = leftArmImb_mA;
            this.progInsts.Mz.FCU2Asic.IimbRight_mA = rightArmImb_mA;
        }

        public void CaseTemperatureSetup()
        {
            this.progInsts.TecCase.SensorTemperatureSetPoint_C = double.Parse(this.textBox_CaseTemperatureSetting_C.Text);
        }

        public void LaserTemperatureSetup()
        {
            this.progInsts.TecDsdbr.SensorTemperatureSetPoint_C = double.Parse(this.textBox_LaserTemperatureSetting_C.Text);
            this.progInsts.TecDsdbr.OutputEnabled = true;
        }

        private void btn_PowerUpLaserByOnePoint_Click(object sender, EventArgs e)
        {
            if (button_Gain_mA.Text.Trim() == "" || button_SOA_mA.Text.Trim() == "" || button_Rear_mA.Text.Trim() == "" || button_Phase_mA.Text.Trim() == "" || 
                comboBox_FrontPairs.Text.Trim() == "" || button_ConstF_mA.Text.Trim() == "" || button_NonConstF_mA.Text.Trim() == "" || button_RearSOA_mA.Text.Trim() == "" || 
                button_MZ_Left_Bias_V.Text.Trim() == "" || button_MZ_Right_Bias_V.Text.Trim() == "" || button_MZ_Left_IMB_mA.Text.Trim() == "" || button_MZ_Right_IMB_mA.Text.Trim() == "")
            {
                MessageBox.Show("laser section current and mz current can't be empty! please check laser and mz setting!");
            }
            else
            {
                btn_PowerUpLaserByOnePoint.BackColor = Color.Green;
                btn_ResetSoa.Enabled = true;
                btn_PhaseCutScan.Enabled = true;
                btn_RearCutScan.Enabled = true;
                //power up laser
                ChannelSetup();
                System.Threading.Thread.Sleep(500);
                double freq_GHz = Measurements.Wavemeter.Frequency_GHz;
                this.textBox_Frequency_GHz.Text = freq_GHz.ToString();

                this.progInsts.Mz.PowerMeter.Wavelength_nm = Alg_FreqWlConvert.Freq_GHz_TO_Wave_nm(freq_GHz);
                this.textBox_OpticalPower_dBm.Text = this.progInsts.OpmReference.ReadPower().ToString();
            }
        }

        private void btn_PhaseCutScan_Click(object sender, EventArgs e)
        {
            string serialNO = this.textBox_SerialNO.Text.Trim();
            double scanFrom;
            double scanTo;
            double scanStep;
            if (txtBox_PhaseCutScan_From.Text.Trim() == "" || txtBox_PhaseCutScan_To.Text.Trim() == "" || txtBox_PhaseCutScan_Step.Text.Trim() == "")
            {
                MessageBox.Show("scan range and step can't be empty!");
            }
            else 
            {
                scanFrom = double.Parse(txtBox_PhaseCutScan_From.Text.Trim());
                scanTo = double.Parse(txtBox_PhaseCutScan_To.Text.Trim());
                scanStep = double.Parse(txtBox_PhaseCutScan_Step.Text.Trim());
                sendToWorker(new MSG_PhaseCutScan(scanFrom,scanTo,scanStep,serialNO));
            }
        }

        private void btn_RearCutScan_Click(object sender, EventArgs e)
        {
            string serialNO = this.textBox_SerialNO.Text.Trim();
            double scanFrom;
            double scanTo;
            double scanStep;
            if (txtBox_RearCutScan_From.Text.Trim() == "" || txtBox_RearCutScan_To.Text.Trim() == "" || txtBox_RearCutScan_Step.Text.Trim() == "")
            {
                MessageBox.Show("scan range and step can't be empty!");
            }
            else
            {
                scanFrom = double.Parse(txtBox_RearCutScan_From.Text.Trim());
                scanTo = double.Parse(txtBox_RearCutScan_To.Text.Trim());
                scanStep = double.Parse(txtBox_RearCutScan_Step.Text.Trim());
                sendToWorker(new MSG_RearCutScan(scanFrom, scanTo, scanStep, serialNO));
            }
        }

        private void btn_PowerOffLaserByOnePoint_Click(object sender, EventArgs e)
        {
            btn_PowerUpLaserByOnePoint.BackColor = Color.Red;
            btn_ResetSoa.Enabled = false;
            btn_PhaseCutScan.Enabled = false;
            btn_RearCutScan.Enabled = false;
            //power off laser
            this.progInsts.Mz.FCU2Asic.CloseAllAsic();
            this.progInsts.Mz.FCU2Asic.VLeftModBias_Volt = 0;
            this.progInsts.Mz.FCU2Asic.VRightModBias_Volt = 0;
        }

        private void btn_ResetSoa_Click(object sender, EventArgs e)
        {
            if (txt_ResetSoa.Text.Trim() != "") 
            {
                double soa_Data = double.Parse(txt_ResetSoa.Text.Trim());
                DsdbrUtils.SetSectionCurrent_mA(DSDBRSection.SOA, soa_Data);
            }
        }
    }
}

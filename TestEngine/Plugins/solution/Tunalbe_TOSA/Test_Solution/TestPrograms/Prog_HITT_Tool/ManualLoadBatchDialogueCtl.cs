// [Copyright]
//
// Bookham Test Engine
// TODO - Full name of assembly
//
// TC_Txp40GDataDeliver/ManualLoadBatchDialogueCtl.cs
// 
// Author: alice.huang
// Design: TODO Title of design document

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Bookham.TestSolution.IlmzCommonUtils;

namespace Bookham.TestSolution.TestControl
{
    public partial class ManualLoadBatchDialogueCtl : Bookham.TestEngine.Framework.Messages.ManagedCtlBase
    {
        /// <summary>
        /// constructor
        /// </summary>
        public ManualLoadBatchDialogueCtl()
        {
            /* Call designer generated code. */
            InitializeComponent();

            /* TODO - Write constructor code here. */
        }
        /// <summary>
        /// initialise controls on gui when load
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            lvDeviceType.Items.Clear();
            txtSN.Text = "";
            txtSN.Focus();
            //deviceTypeLookupTable = TosaDeviceTypeConfig.LoadDeviceTypeLookup("configuration/TOSA Final" + "DeviceTypeLookup");
            //if (deviceTypeLookupTable.Count > 0)
            //{
            //    foreach (DeviceTypeLookupItem temp in deviceTypeLookupTable.Values)
            //    {
            //        ListViewItem lvi = lvDeviceType.Items.Add(temp.DeviceType, temp.ChirpType);
            //        lvi.Tag = temp;
            //    }
            //}
        }
        private void btnLoadBatch_Click(object sender, EventArgs e)
        {
            if (txtSN.Text.Trim().Length < 4)  // i'm not sure of how many chars the SN should contain
            {
                MessageBox.Show("incorrect DUT SN Input!\n please re-input Dut's SN:");
                txtSN.SelectAll();
                txtSN.Focus();
                return;
            }

            if (lvDeviceType.SelectedItems.Count != 1)// i'm not sure how many chars the device types should contain
            {
                MessageBox.Show(" Please select a device type.", "Message", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                lvDeviceType.Focus();
                return;
            }

            if (!cmbStage.Items.Contains(cmbStage.Text.Trim()))
            {
                MessageBox.Show(" Please select a device type.", "Message", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                cmbStage.Focus();
                return;
            }
            DeviceTypeLookupItem item= (DeviceTypeLookupItem ) this .lvDeviceType.SelectedItems[0].Tag ;
            DutLoadInfo dutInfo = new DutLoadInfo();
            dutInfo.DutSerialNumber = txtSN.Text.Trim();
            dutInfo.PartCode = item.DeviceType;
            dutInfo.ChirpType = item.ChirpType;
            dutInfo.TestStage = cmbStage.Text.Trim();
            sendToWorker(dutInfo);
        }

        private void ManualLoadBatchDialogueCtl_MsgReceived(object payload, long inMsgSeq, long respSeq)
        {
            if (payload.GetType() == typeof(Dictionary<string,DeviceTypeLookupItem >))
            {
                lvDeviceType.Items.Clear();
                deviceTypeLookupTable = (Dictionary<string ,DeviceTypeLookupItem> ) payload;
                foreach (DeviceTypeLookupItem  info in deviceTypeLookupTable.Values)
                {
                    ListViewItem lvi=lvDeviceType.Items.Add(info.DeviceType, info.ChirpType);
                    lvi.Tag = info; 
                }
                lvDeviceType.Update();
            }
            //else if(payload.GetType()== typeof())
            //{}
        }

        

        private Dictionary<string,DeviceTypeLookupItem> deviceTypeLookupTable;

        

        

        

       

         
    }
}

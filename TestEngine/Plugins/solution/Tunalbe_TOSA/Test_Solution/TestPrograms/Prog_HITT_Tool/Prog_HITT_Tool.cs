﻿// Author: chongjian.liang 2016.07.06

using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.ExternalData;
using Bookham.TestEngine.Framework.Limits;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Framework.Messages;
using Bookham.TestEngine.Config;
using Bookham.TestLibrary.Utilities;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.Instruments;
using Bookham.TestLibrary.Algorithms;
using Bookham.TestSolution.ILMZFinalData;
using Bookham.TestSolution.IlmzCommonUtils;
using Bookham.TestSolution.IlmzCommonInstrs;
using Bookham.TestSolution.IlmzCommonData;
using Bookham.TestSolution.TestModules;
using Bookham.TestSolution.Instruments;
using Bookham.ToolKit.Mz;
using Bookham.fcumapping.CommonData;
using Bookham.Solution.Config;
using Bookham.Program.Core;

namespace Bookham.TestSolution.TestPrograms
{
    public class Prog_HITT_Tool : ITestProgram
    {
        private Prog_HITT_ToolData progData;
        private Prog_HITT_ToolInfo progInfo;
        private Prog_HITT_ToolInsts progInsts;

        public Type UserControl
        {
            get { return typeof(Prog_HITT_ToolGui); }
        }

        #region ITestProgram Members


        public void InitCode(ITestEngineInit engine, DUTObject dutObject, InstrumentCollection instrs, ChassisCollection chassis)
        {
            this.progData = new Prog_HITT_ToolData(engine);
            this.progInfo = new Prog_HITT_ToolInfo();

            this.InitConfig(engine, dutObject);
            this.InitInstrs(engine, dutObject, instrs, chassis);
            this.InitModules(engine, dutObject, instrs, chassis);

            engine.SetSpecificationList(new SpecList());
        }

        public void Run(ITestEngineRun engine, DUTObject dutObject, InstrumentCollection instrs, ChassisCollection chassis)
        {
            InstCommands.CloseInstToLoadDUT(engine, progInsts.Fcu2AsicInstrsGroups.FcuSource, progInsts.Fcu2AsicInstrsGroups.AsicVccSource, progInsts.Fcu2AsicInstrsGroups.AsicVeeSource, progInsts.TecDsdbr);

            engine.ShowContinueUserQuery("Please load DUT, and connect the fiber to the powermeter head or wavemeter.");

            progInsts.Mz.PowerMeter.ZeroDarkCurrent_Start();

            this.ConfigureFCU2AsicInstruments(progInsts.Fcu2AsicInstrsGroups);

            int FCU2ASIC_COMMUNICATE_COUNT_MAX = 10;
            int fcu2Asic_Communicate_Count = 0;

            while (fcu2Asic_Communicate_Count++ < FCU2ASIC_COMMUNICATE_COUNT_MAX)
            {
                try
                {
                    progInsts.Fcu2AsicInstrsGroups.Fcu2Asic.SetupChannelCalibration();
                    progInsts.Fcu2AsicInstrsGroups.Fcu2Asic.Inital_Etalon_WaveMter();
                    progInsts.Fcu2AsicInstrsGroups.Fcu2Asic.ZeroCurrent();
                    progInsts.Fcu2AsicInstrsGroups.Fcu2Asic.CloseAllAsic();

                    break;
                }
                catch (Exception)
                {
                    System.Threading.Thread.Sleep(1000);
                }
            }

            try
            {
                progInsts.Mz.PowerMeter.ZeroDarkCurrent_End();
            }
            catch
            {
                string error = "Power meter zeroing fails.";

                engine.ShowYesNoUserQuery(error);
                engine.ErrorInProgram(error);
            }

            this.progInsts.TecCase.OutputEnabled = true;
            this.progInsts.TecDsdbr.OutputEnabled = true;

            engine.SendToGui(this.progInfo);
            engine.SendToGui(this.progInsts);

            engine.GuiShow();
            engine.GuiToFront();

            while (true)
            {
                object msg = engine.ReceiveFromGui().Payload;

                if (msg is MSG_SerialNO_Stage)
                {
                    string deviceType = this.progData.GetLatestResultDeviceType(msg as MSG_SerialNO_Stage);

                    engine.SendToGui(new MSG_DeviceType(deviceType));
                }
                if (msg is MSG_PhaseCutScan)
                {
                    PhaseCutScan(msg as MSG_PhaseCutScan,engine);
                }
                if (msg is MSG_RearCutScan)
                {
                    RearCutScan(msg as MSG_RearCutScan,engine);
                }
                else if (msg is MSG_SerialNO_DeviceType_Stage_FileKey)
                {
                    Dictionary<string, TargetFileChannel> fileChannels = this.progData.GetChannelsFromTargetFile(msg as MSG_SerialNO_DeviceType_Stage_FileKey);

                    engine.SendToGui(fileChannels);
                }
            }
        }

        public void PostRun(ITestEnginePostRun engine, DUTObject dutObject, InstrumentCollection instrs, ChassisCollection chassis)
        {
            TestProgramCore.CloseInstrs_Wavemeter();

            engine.SendToGui(new MSG_OnEnding());
        }

        public void DataWrite(ITestEngineDataWrite engine, DUTObject dutObject, DUTOutcome dutOutcome, TestData results, UserList userList) { }

        #endregion

        #region Implement InitCode Member

        private void InitConfig(ITestEngineInit engine, DUTObject dutObject)
        {
            progInfo.TestParamsConfig = new TestParamConfigAccessor(dutObject,
                @"Configuration\TOSA Final\FinalTest\CommonTestParams.xml", "", "TestParams");

            progInfo.InstrumentsCalData = new ConfigDataAccessor(@"Configuration\TOSA Final\InstrsCalibration_new.xml", "Calibration");

            progInfo.TempConfig = new TempTableConfigAccessor(dutObject, 1,
                @"Configuration\TOSA Final\TempTable.xml");

            progInfo.LocalTestParamsConfig = new TestParamConfigAccessor(dutObject,
                 @"Configuration\TOSA Final\LocalTestParams.xml", "", "TestParams");

            progInfo.OpticalSwitchConfig = new TestParamConfigAccessor(dutObject,
                 @"Configuration\TOSA Final\IlmzOpticalSwitch.xml", "", "IlmzOpticalSwitchParams");

            progInfo.MapParamsConfig = new TestParamConfigAccessor(dutObject, @"Configuration\TOSA Final\MapTest\MappingTestConfig.xml", "", "MappingTestParams");

            progInfo.TecDsdbr_SensorTemperatureSetPoint_C = progInfo.TestParamsConfig.GetDoubleParam("TecDsdbr_SensorTemperatureSetPoint_C");
            progInfo.TecCase_SensorTemperatureSetPoint_C = progInfo.TestParamsConfig.GetDoubleParam("TecCase_SensorTemperatureSetPoint_C");
            
            progInfo.PartCodeList = new List<string>();
            progInfo.PartCodeList.AddRange(TosaDeviceTypeConfig.LoadDeviceTypeLookup("configuration/TOSA Final/DeviceTypeLookup.csv").Keys);
        }

        private void InitInstrs(ITestEngineInit engine, DUTObject dutObject, InstrumentCollection instrs, ChassisCollection chassis)
        {
            if (engine.IsSimulation)
            {
                return;
            }

            engine.SendStatusMsg("Initialise Instruments");

            foreach (Chassis var in chassis.Values) var.EnableLogging = false;
            foreach (Instrument var in instrs.Values) var.EnableLogging = false;

            this.progInsts = new Prog_HITT_ToolInsts();

            progInsts.Fcu2AsicInstrsGroups = new FCU2AsicInstruments();

            if (instrs.Contains("FCUMKI_Source"))
            {
                progInsts.Fcu2AsicInstrsGroups.FcuSource = (InstType_ElectricalSource)instrs["FCUMKI_Source"];
            }

            progInsts.Fcu2AsicInstrsGroups.AsicVccSource = (InstType_ElectricalSource)instrs["ASIC_VccSource"];
            progInsts.Fcu2AsicInstrsGroups.AsicVeeSource = (InstType_ElectricalSource)instrs["ASIC_VeeSource"];
            progInsts.Fcu2AsicInstrsGroups.Fcu2Asic = (Inst_Fcu2Asic)instrs["Fcu2Asic"];

            progInsts.Mz = new IlMzInstruments();

            progInsts.Mz.FCU2Asic = (Inst_Fcu2Asic)instrs["Fcu2Asic"];
            progInsts.Mz.FCU2Asic.SwapWirebond = ParamManager.Conditions.IsSwapWirebond;

            progInsts.Mz.PowerMeter = (IInstType_TriggeredOpticalPowerMeter)instrs["OpmMz"];
            progInsts.Mz.PowerMeter.EnableInputTrigger(false);
            progInsts.Mz.PowerMeter.SetDefaultState();

            progInsts.MzDriverUtils = new IlMzDriverUtils(progInsts.Mz);

            progInsts.Mz.ImbsSourceType = IlMzInstruments.EnumSourceType.Asic;

            progInsts.OpmReference = (IInstType_OpticalPowerMeter)instrs["OpmRef"];
            progInsts.OpmReference.SetDefaultState();

            TestProgramCore.InitInstrs_Wavemeter(new FailModeCheck(), engine, dutObject, instrs, progInfo.LocalTestParamsConfig, progInfo.OpticalSwitchConfig);

            progInsts.TecDsdbr = (IInstType_TecController)instrs["TecDsdbr"];

            progInsts.TecCase = (IInstType_TecController)instrs["TecCase"];

            DsdbrUtils.Fcu2AsicInstrumentGroup = new FCU2AsicInstruments();
            DsdbrUtils.Fcu2AsicInstrumentGroup.FcuSource = progInsts.Fcu2AsicInstrsGroups.FcuSource;
            DsdbrUtils.Fcu2AsicInstrumentGroup.AsicVccSource = progInsts.Fcu2AsicInstrsGroups.AsicVccSource;
            DsdbrUtils.Fcu2AsicInstrumentGroup.AsicVeeSource = progInsts.Fcu2AsicInstrsGroups.AsicVeeSource;
            DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic = progInsts.Fcu2AsicInstrsGroups.Fcu2Asic;
            DsdbrUtils.opticalSwitch = (IInstType_DigiIOCollection)progInsts.TecDsdbr;
            DsdbrTuning.OpticalSwitch = (IInstType_DigiIOCollection)progInsts.TecDsdbr;

            IlMzDriverUtils mzDriverUtils = new IlMzDriverUtils(progInsts.Mz);
            mzDriverUtils.opticalSwitch = (IInstType_DigiIOCollection)progInsts.TecDsdbr;

            ConfigureTecController(progInsts.TecDsdbr, "TecDsdbr", false, progInfo.TecDsdbr_SensorTemperatureSetPoint_C);
            ConfigureTecController(progInsts.TecCase, "TecCase", true, progInfo.TecCase_SensorTemperatureSetPoint_C);
            
            this.InitOpticalSwitchIoLine();

            IInstType_DigitalIO outLine_C_L_Band = mzDriverUtils.opticalSwitch.GetDigiIoLine(IlmzOpticalSwitchLines.C_Band_DigiIoLine);
            outLine_C_L_Band.LineState = IlmzOpticalSwitchLines.C_Band_DigiIoLine_State;

            IInstType_DigitalIO outLine_Tx = mzDriverUtils.opticalSwitch.GetDigiIoLine(IlmzOpticalSwitchLines.Dut_Tx_DigiIoLine);
            outLine_Tx.LineState = IlmzOpticalSwitchLines.Dut_Tx_DigiIoLine_State;

            IInstType_DigitalIO outLine_Rx = mzDriverUtils.opticalSwitch.GetDigiIoLine(IlmzOpticalSwitchLines.Dut_Rx_DigiIoLine);
            outLine_Rx.LineState = IlmzOpticalSwitchLines.Dut_Rx_DigiIoLine_State;

            IInstType_DigitalIO ctapLine = mzDriverUtils.opticalSwitch.GetDigiIoLine(IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine);
            ctapLine.LineState = IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine_State;
        }

        private void InitOpticalSwitchIoLine()
        {
            IlmzOpticalSwitchLines.C_Band_DigiIoLine = int.Parse(progInfo.OpticalSwitchConfig.GetStringParam("C_Band_Filter_IO_Line"));
            IlmzOpticalSwitchLines.C_Band_DigiIoLine_State = progInfo.OpticalSwitchConfig.GetBoolParam("C_Band_Filter_IO_Line_State");
            IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine = int.Parse(progInfo.OpticalSwitchConfig.GetStringParam("Dut_Ctap_IO_Line"));
            IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine_State = progInfo.OpticalSwitchConfig.GetBoolParam("Dut_Ctap_IO_Line_State");
            IlmzOpticalSwitchLines.Dut_Rx_DigiIoLine = int.Parse(progInfo.OpticalSwitchConfig.GetStringParam("Dut_Rx_IO_Line"));
            IlmzOpticalSwitchLines.Dut_Rx_DigiIoLine_State = progInfo.OpticalSwitchConfig.GetBoolParam("Dut_Rx_IO_Line_State");
            IlmzOpticalSwitchLines.Dut_Tx_DigiIoLine = int.Parse(progInfo.OpticalSwitchConfig.GetStringParam("Dut_Tx_IO_LIne"));
            IlmzOpticalSwitchLines.Dut_Tx_DigiIoLine_State = progInfo.OpticalSwitchConfig.GetBoolParam("Dut_Tx_IO_LIne_State");
            IlmzOpticalSwitchLines.Etalon_Ref_DigiIoLine = int.Parse(progInfo.OpticalSwitchConfig.GetStringParam("Etalon_Ref_IO_Line"));
            IlmzOpticalSwitchLines.Etalon_Ref_DigiIoLine_State = progInfo.OpticalSwitchConfig.GetBoolParam("Etalon_Ref_IO_Line_State");
            IlmzOpticalSwitchLines.Etalon_Rx_DigiIoLine = int.Parse(progInfo.OpticalSwitchConfig.GetStringParam("Etalon_Rx_IO_Line"));
            IlmzOpticalSwitchLines.Etalon_Rx_DigiIoLine_State = progInfo.OpticalSwitchConfig.GetBoolParam("Etalon_Rx_IO_Line_State");
            IlmzOpticalSwitchLines.Etalon_Tx_DigiIoLine = int.Parse(progInfo.OpticalSwitchConfig.GetStringParam("Etalon_Tx_IO_Line"));
            IlmzOpticalSwitchLines.Etalon_Tx_DigiIoLine_State = progInfo.OpticalSwitchConfig.GetBoolParam("Etalon_Tx_IO_Line_State");
            IlmzOpticalSwitchLines.L_Band_DigiIoLine = int.Parse(progInfo.OpticalSwitchConfig.GetStringParam("L_Band_Filter_IO_Line"));
            IlmzOpticalSwitchLines.L_Band_DigiIoLine_State = progInfo.OpticalSwitchConfig.GetBoolParam("L_Band_Filter_IO_Line_State");
        }

        private void InitModules(ITestEngineInit engine, DUTObject dutObject, InstrumentCollection instrs, ChassisCollection chassis)
        {
            engine.SendStatusMsg("Initialise modules");

            this.DeviceTempControl_InitModule(engine, "DsdbrTemperature", progInsts.TecDsdbr, "TecDsdbr");
            this.CaseTempControl_InitModule(engine, "CaseTemp_Mid", 25, this.progInfo.TestParamsConfig.GetDoubleParam("TecCase_SensorTemperatureSetPoint_C"));
        }

        #region Configure instruments

        private void ConfigureTecController(IInstType_TecController tecCtlr, string tecCtlId, bool isTecCase, double temperatureSetPoint_C)
        {
            SteinhartHartCoefficients stCoeffs = new SteinhartHartCoefficients();

            if (tecCtlr.DriverName.Contains("Ke2510"))
            {
                // Keithley 2510 specific commands (must be done before 'SetDefaultState')
                tecCtlr.HardwareData["MinTemperatureLimit_C"] = progInfo.TestParamsConfig.GetStringParam(tecCtlId + "_MinTemp_C");
                tecCtlr.HardwareData["MaxTemperatureLimit_C"] = progInfo.TestParamsConfig.GetStringParam(tecCtlId + "_MaxTemp_C");

                tecCtlr.SetDefaultState();

                tecCtlr.Sensor_Type = (InstType_TecController.SensorType)Enum.Parse(typeof(InstType_TecController.SensorType), progInfo.TestParamsConfig.GetStringParam(tecCtlId + "_Sensor_Type"));

                tecCtlr.TecVoltageCompliance_volt = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_TecVoltageCompliance_volt");

                // If Ke2510 is used as TecCase
                if (isTecCase)
                {
                    tecCtlr.DerivativeGain = progInfo.LocalTestParamsConfig.GetDoubleParam(tecCtlId + "_DerivativeGain");
                }
                else
                {
                    tecCtlr.DerivativeGain = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_DerivativeGain");
                }
            }
            else
            {
                tecCtlr.SetDefaultState();
            }

            tecCtlr.OperatingMode = (InstType_TecController.ControlMode)Enum.Parse(typeof(InstType_TecController.ControlMode), progInfo.TestParamsConfig.GetStringParam(tecCtlId + "_OperatingMode"));

            // Thermistor characteristics
            if (tecCtlr.Sensor_Type == InstType_TecController.SensorType.Thermistor_2wire ||
                tecCtlr.Sensor_Type == InstType_TecController.SensorType.Thermistor_4wire)
            {
                stCoeffs.A = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_stCoeffs_A");
                stCoeffs.B = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_stCoeffs_B");
                stCoeffs.C = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_stCoeffs_C");
                tecCtlr.SteinhartHartConstants = stCoeffs;
            }

            if (isTecCase)
            {
                tecCtlr.TecCurrentCompliance_amp = progInfo.LocalTestParamsConfig.GetDoubleParam(tecCtlId + "_TecCurrentCompliance_amp");
                tecCtlr.ProportionalGain = progInfo.LocalTestParamsConfig.GetDoubleParam(tecCtlId + "_ProportionalGain");
                tecCtlr.IntegralGain = progInfo.LocalTestParamsConfig.GetDoubleParam(tecCtlId + "_IntegralGain");
            }
            else
            {
                tecCtlr.TecCurrentCompliance_amp = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_TecCurrentCompliance_amp");
                tecCtlr.ProportionalGain = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_ProportionalGain");
                tecCtlr.IntegralGain = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_IntegralGain");
            }

            tecCtlr.SensorTemperatureSetPoint_C = temperatureSetPoint_C;

            if (isTecCase)
            {
                tecCtlr.OutputEnabled = true;
            }
        }

        private void ConfigureFCU2AsicInstruments(FCU2AsicInstruments fcu2AsicInstrs)
        {
            double curMax = progInfo.LocalTestParamsConfig.GetDoubleParam("FcuSource_CurrentCompliance_A");
            if (fcu2AsicInstrs.FcuSource != null)
            {
                fcu2AsicInstrs.FcuSource.SetDefaultState();
                fcu2AsicInstrs.FcuSource.VoltageSetPoint_Volt = progInfo.LocalTestParamsConfig.GetDoubleParam("FcuSource_Voltage_V");
                fcu2AsicInstrs.FcuSource.CurrentComplianceSetPoint_Amp = curMax;
                fcu2AsicInstrs.FcuSource.OutputEnabled = true;
            }

            fcu2AsicInstrs.AsicVccSource.SetDefaultState();
            curMax = progInfo.LocalTestParamsConfig.GetDoubleParam("AsicVccSource_CurrentCompliance_A");

            fcu2AsicInstrs.AsicVccSource.VoltageSetPoint_Volt = progInfo.LocalTestParamsConfig.GetDoubleParam("AsicVccSource_Voltage_V");
            fcu2AsicInstrs.AsicVccSource.CurrentComplianceSetPoint_Amp = curMax;
            fcu2AsicInstrs.AsicVccSource.OutputEnabled = true;

            fcu2AsicInstrs.AsicVeeSource.SetDefaultState();
            curMax = progInfo.LocalTestParamsConfig.GetDoubleParam("AsicVeeSource_CurrentCompliance_A");
            fcu2AsicInstrs.AsicVeeSource.VoltageSetPoint_Volt =
                        progInfo.LocalTestParamsConfig.GetDoubleParam("AsicVeeSource_Voltage_V");
            fcu2AsicInstrs.AsicVeeSource.CurrentComplianceSetPoint_Amp = curMax;
            fcu2AsicInstrs.AsicVeeSource.OutputEnabled = true;

            string FrequencyCalibration_file_Path = null;

            if (this.progInfo.FREQ_BAND == "C")
            {
                FrequencyCalibration_file_Path = "C_band_FrequencyCalibration_file_Path";
            }
            else
            {
                FrequencyCalibration_file_Path = "L_band_FrequencyCalibration_file_Path";
            }

            string freqCalFilename = progInfo.MapParamsConfig.GetStringParam(FrequencyCalibration_file_Path);
            string FCUMKI_Tx_CalFilename = progInfo.MapParamsConfig.GetStringParam("FCUMKI_Tx_Calibration_file_Path");
            string FCUMKI_Rx_CalFilename = progInfo.MapParamsConfig.GetStringParam("FCUMKI_Rx_Calibration_file_Path");
            string FCUMKI_Var_CalFilename = progInfo.MapParamsConfig.GetStringParam("FCUMKI_Var_Calibration_file_Path");
            string FCUMKI_Fix_CalFilename = progInfo.MapParamsConfig.GetStringParam("FCUMKI_Fix_Calibration_file_Path");

            FreqCalByEtalon.LoadFreqlCalArray(freqCalFilename, 0.0, progInfo.TestParamsConfig.GetDoubleParam("ItuFreqTolerance_GHz"), this.progInfo.ITU_SPACING_GHz);
            FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Tx = FreqCalByEtalon.LoadFCUMKICalArray(FCUMKI_Tx_CalFilename);
            FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Rx = FreqCalByEtalon.LoadFCUMKICalArray(FCUMKI_Rx_CalFilename);
            FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Var = FreqCalByEtalon.LoadFCUMKICalArray(FCUMKI_Var_CalFilename);
            FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Fix = FreqCalByEtalon.LoadFCUMKICalArray(FCUMKI_Fix_CalFilename);
        }

        private void readCalData(string InstrumentName, string InstrumentSerialNo, string ParameterName, out double CalFactor, out double CalOffset)
        {
            StringDictionary keys = new StringDictionary();
            keys.Add("Instrument_name", InstrumentName);
            keys.Add("Parameter", ParameterName);
            if (InstrumentSerialNo != null)
            {
                keys.Add("Instrument_SerialNo", InstrumentSerialNo);
            }
            DatumList calParams = progInfo.InstrumentsCalData.GetData(keys, false);

            CalFactor = calParams.ReadDouble("CalFactor");
            CalOffset = calParams.ReadDouble("CalOffset");
        }

        #endregion

        #region Initialize modules

        private void DeviceTempControl_InitModule(ITestEngineInit engine, string moduleName, IInstType_SimpleTempControl tecController, string configPrefix)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun(moduleName, "SimpleTempControl", "");

            // Add instrument
            modRun.Instrs.Add("Controller", (Instrument)tecController);

            //load config
            double timeout_S = this.progInfo.TestParamsConfig.GetDoubleParam(configPrefix + "_Timeout_S");
            double stabTime_S = this.progInfo.TestParamsConfig.GetDoubleParam(configPrefix + "_StabiliseTime_S");
            int updateTime_mS = this.progInfo.TestParamsConfig.GetIntParam(string.Format("{0}_UpdateTime_mS", configPrefix));
            double sensorSetPoint_C = this.progInfo.TestParamsConfig.GetDoubleParam(string.Format("{0}_SensorTemperatureSetPoint_C", configPrefix));
            double tolerance_C = this.progInfo.TestParamsConfig.GetDoubleParam(string.Format("{0}_Tolerance_C", configPrefix));

            // Add config data
            DatumList tempConfig = new DatumList();
            tempConfig.AddDouble("datumMaxExpectedTimeForOperation_s", timeout_S);
            tempConfig.AddDouble("RqdStabilisationTime_s", stabTime_S);
            tempConfig.AddSint32("TempTimeBtwReadings_ms", updateTime_mS);
            tempConfig.AddDouble("SetPointTemperature_C", sensorSetPoint_C);
            tempConfig.AddDouble("TemperatureTolerance_C", tolerance_C);

            modRun.ConfigData.AddListItems(tempConfig);
        }

        private void CaseTempControl_InitModule(ITestEngineInit engine, string moduleName, Double currentTemp, Double tempSetPoint)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun(moduleName, "SimpleTempControl", "");

            // Add instrument
            modRun.Instrs.Add("Controller", (Instrument)progInsts.TecCase);

            // Add config data
            TempTablePoint tempCal = progInfo.TempConfig.GetTemperatureCal(currentTemp, tempSetPoint)[0];
            DatumList tempConfig = new DatumList();
            int guiTimeOut_ms = this.progInfo.TestParamsConfig.GetIntParam("CaseTempGui_UpdateTime_mS");
            // Timeout = 2 * calibrated time, minimum of 60secs
            double timeout_s = Math.Max(tempCal.DelayTime_s * 15, 60);
            tempConfig.AddDouble("datumMaxExpectedTimeForOperation_s", timeout_s);
            // Stabilisation time = 0.5 * calibrated time
            tempConfig.AddDouble("RqdStabilisationTime_s", tempCal.DelayTime_s);
            tempConfig.AddSint32("TempTimeBtwReadings_ms", guiTimeOut_ms);
            // Actual temperature to set
            tempConfig.AddDouble("SetPointTemperature_C", tempSetPoint + tempCal.OffsetC);
            tempConfig.AddDouble("TemperatureTolerance_C", tempCal.Tolerance_degC);
            modRun.ConfigData.AddListItems(tempConfig);
        }

        #endregion

        #endregion

        #region  public function
        public void PhaseCutScan(MSG_PhaseCutScan msg, ITestEngineRun engine) 
        {
            double scanFrom = msg.currentFrom;
            double scanTo = msg.currentTo;
            double scanStep = msg.currentStep;
            string serialNo = msg.sn;
            #region no used
            //List<double> power_dBm_Array = new List<double>();
            //List<double> freq_Ghz_Array = new List<double>();
            //List<double> dut_LockerRatio = new List<double>();
            //List<double> dut_Tx = new List<double>();
            //List<double> dut_Rx = new List<double>();
            //List<string> lines = new List<string>();
            #endregion
            double  power_dBm ;
            double freq_Ghz ;
            double dut_LockerRatio;
            double dut_Tx ;
            double dut_Rx;
            List<string> lines = new List<string>();
            string contextStr="phase_mA,power_dbm,Freq_Ghz,Tx_mA,Rx_mA,LockerRatio";
            lines.Add(contextStr);
            for (double i = scanFrom; i <= scanTo; i = i + scanStep) 
            {
                DsdbrUtils.SetSectionCurrent_mA(DSDBRSection.Phase, i);
                contextStr = "";
                #region no used
                //power_dBm_Array.Add(Measurements.MzHead.ReadPower());
                //freq_Ghz_Array.Add(Measurements.ReadFrequency_GHz());
                //DsdbrUtils.LockerCurrents lk = DsdbrUtils.ReadLockerCurrents();
                //dut_LockerRatio.Add(lk.LockRatio);
                //dut_Tx.Add(lk.TxCurrent_mA);
                //dut_Rx.Add(lk.RxCurrent_mA);
                #endregion
                //power_dBm = Measurements.MzHead.ReadPower();
                power_dBm = progInsts.Mz.PowerMeter.ReadPower();
                freq_Ghz = Measurements.ReadFrequency_GHz();
                DsdbrUtils.LockerCurrents lk = DsdbrUtils.ReadLockerCurrents();
                dut_Rx = lk.RxCurrent_mA;
                dut_Tx = lk.TxCurrent_mA;
                dut_LockerRatio = lk.LockRatio;

                contextStr = i.ToString() + "," + power_dBm.ToString() + "," + freq_Ghz.ToString() + "," + dut_Tx.ToString() + "," + dut_Rx.ToString() + "," + dut_LockerRatio.ToString();
                lines.Add(contextStr);
                engine.SendStatusMsg(contextStr);
            }
            RecordData(engine,lines, serialNo, "PhaseCutScan");
        }

        public void RearCutScan(MSG_RearCutScan msg, ITestEngineRun engine)
        {
            double scanFrom = msg.currentFrom;
            double scanTo = msg.currentTo;
            double scanStep = msg.currentStep;
            string serialNo = msg.sn;
            #region no used
            //List<double> power_dBm_Array = new List<double>();
            //List<double> freq_Ghz_Array = new List<double>();
            //List<double> dut_LockerRatio = new List<double>();
            //List<double> dut_Tx = new List<double>();
            //List<double> dut_Rx = new List<double>();
            #endregion
            double power_dBm;
            double freq_Ghz;
            double dut_LockerRatio;
            double dut_Tx;
            double dut_Rx;
            List<string> lines = new List<string>();
            string contextStr = "rear_mA,power_dbm,Freq_Ghz,Tx_mA,Rx_mA,LockerRatio";
            lines.Add(contextStr);

            for (double i = scanFrom; i <= scanTo; i = i + scanStep)
            {
                #region no used
                //DsdbrUtils.SetSectionCurrent_mA(DSDBRSection.Rear, i);
                //power_dBm_Array.Add(Measurements.MzHead.ReadPower());
                //freq_Ghz_Array.Add(Measurements.ReadFrequency_GHz());
                //DsdbrUtils.LockerCurrents lk = DsdbrUtils.ReadLockerCurrents();
                //dut_LockerRatio.Add(lk.LockRatio);
                //dut_Tx.Add(lk.TxCurrent_mA);
                //dut_Rx.Add(lk.RxCurrent_mA);
                #endregion

                DsdbrUtils.SetSectionCurrent_mA(DSDBRSection.Rear, i);
                contextStr = "";
                //power_dBm = Measurements.MzHead.ReadPower();
                power_dBm = progInsts.Mz.PowerMeter.ReadPower();
                freq_Ghz = Measurements.ReadFrequency_GHz();
                DsdbrUtils.LockerCurrents lk = DsdbrUtils.ReadLockerCurrents();
                dut_Rx = lk.RxCurrent_mA;
                dut_Tx = lk.TxCurrent_mA;
                dut_LockerRatio = lk.LockRatio;

                contextStr = i.ToString() + "," + power_dBm.ToString() + "," + freq_Ghz.ToString() + "," + dut_Tx.ToString() + "," + dut_Rx.ToString() + "," + dut_LockerRatio.ToString();
                lines.Add(contextStr);
                engine.SendStatusMsg(contextStr);
            }

            RecordData(engine,lines, serialNo, "RearCutScan");
        }

        public void RecordData(ITestEngineRun engine,List<string> writeData,string serialNo,string scanType)
        {
            string pathStr = "";
            if (Directory.Exists("c:\\scandata"))
            {
                pathStr = "c:\\scandata";
            }
            else if (Directory.Exists("d:\\scandata"))
            {
                pathStr = "d:\\scandata";
            }
            else if (Directory.Exists("d:\\scandata"))
            {
                pathStr = "e:\\scandata";
            }
            else{
                engine.SendStatusMsg("folder does not exist,please create it!c:\\scandata,d:\\scandata or e:\\scandata  ");
            }
            pathStr = pathStr + "\\" + scanType + "_" + serialNo + "_" + DateTime.Now.ToString("yyyyMMddhhmmssfff") + ".CSV";
            using (StreamWriter sw = new StreamWriter(pathStr))
            {
                for (int j = 0; j < writeData.Count; j++) 
                {
                    sw.WriteLine(writeData[j]);
                }
                sw.Close();
            }
        }

        #endregion
    }
}



using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.InstrTypes;
using Bookham.ToolKit.Mz;
using Bookham.TestSolution.IlmzCommonInstrs;
using Bookham.TestLibrary.Utilities;
using Bookham.TestSolution.IlmzCommonUtils;
using Bookham.TestLibrary.Instruments;

namespace Bookham.TestSolution.TestPrograms
{
    internal class Prog_HITT_ToolInsts
    {
        internal FCU2AsicInstruments Fcu2AsicInstrsGroups;

        internal IlMzInstruments Mz;

        internal IlMzDriverUtils MzDriverUtils;
        
        internal IInstType_OpticalPowerMeter OpmReference;

        internal IInstType_TecController TecDsdbr;

        internal IInstType_TecController TecCase;        
    }
}

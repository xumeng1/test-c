namespace Bookham.TestSolution.TestPrograms
{
    partial class GetFinalTestData
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnGetItuFile = new System.Windows.Forms.Button();
            this.txtItuFileName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtImbLeft = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtImbRight = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtVleft = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtVright = new System.Windows.Forms.TextBox();
            this.txtChannels = new System.Windows.Forms.TextBox();
            this.cbStartFreq = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSet = new System.Windows.Forms.Button();
            this.btnGetChanPassFile = new System.Windows.Forms.Button();
            this.txtPassedChannelFileName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnGetItuFile
            // 
            this.btnGetItuFile.Location = new System.Drawing.Point(511, 154);
            this.btnGetItuFile.Name = "btnGetItuFile";
            this.btnGetItuFile.Size = new System.Drawing.Size(51, 22);
            this.btnGetItuFile.TabIndex = 12;
            this.btnGetItuFile.Text = "Browse";
            this.btnGetItuFile.UseVisualStyleBackColor = true;
            this.btnGetItuFile.Click += new System.EventHandler(this.btnGetItuFile_Click);
            // 
            // txtItuFileName
            // 
            this.txtItuFileName.Location = new System.Drawing.Point(214, 156);
            this.txtItuFileName.Name = "txtItuFileName";
            this.txtItuFileName.Size = new System.Drawing.Size(291, 20);
            this.txtItuFileName.TabIndex = 11;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(48, 156);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(122, 20);
            this.label3.TabIndex = 10;
            this.label3.Text = "ITU Channel file";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.txtImbLeft);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.txtImbRight);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txtVleft);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtVright);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(52, 284);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(510, 120);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "MZ Settings";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(21, 82);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(129, 20);
            this.label11.TabIndex = 28;
            this.label11.Text = "Ion_LeftImb_mA";
            // 
            // txtImbLeft
            // 
            this.txtImbLeft.Location = new System.Drawing.Point(140, 82);
            this.txtImbLeft.Name = "txtImbLeft";
            this.txtImbLeft.Size = new System.Drawing.Size(79, 22);
            this.txtImbLeft.TabIndex = 29;
            this.txtImbLeft.Text = "2.77";
            this.txtImbLeft.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(266, 82);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(139, 20);
            this.label12.TabIndex = 26;
            this.label12.Text = "Ion_RightImb_mA";
            // 
            // txtImbRight
            // 
            this.txtImbRight.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtImbRight.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtImbRight.Location = new System.Drawing.Point(397, 82);
            this.txtImbRight.Multiline = true;
            this.txtImbRight.Name = "txtImbRight";
            this.txtImbRight.Size = new System.Drawing.Size(79, 20);
            this.txtImbRight.TabIndex = 27;
            this.txtImbRight.Text = "4.23";
            this.txtImbRight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(21, 33);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(126, 20);
            this.label7.TabIndex = 17;
            this.label7.Text = "Von_LeftMod_V";
            // 
            // txtVleft
            // 
            this.txtVleft.Location = new System.Drawing.Point(140, 33);
            this.txtVleft.Name = "txtVleft";
            this.txtVleft.Size = new System.Drawing.Size(79, 22);
            this.txtVleft.TabIndex = 21;
            this.txtVleft.Text = "0";
            this.txtVleft.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(266, 33);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(136, 20);
            this.label6.TabIndex = 13;
            this.label6.Text = "Von_RightMod_V";
            // 
            // txtVright
            // 
            this.txtVright.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVright.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtVright.Location = new System.Drawing.Point(397, 33);
            this.txtVright.Multiline = true;
            this.txtVright.Name = "txtVright";
            this.txtVright.Size = new System.Drawing.Size(79, 20);
            this.txtVright.TabIndex = 14;
            this.txtVright.Text = "0";
            this.txtVright.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtChannels
            // 
            this.txtChannels.Location = new System.Drawing.Point(192, 20);
            this.txtChannels.Name = "txtChannels";
            this.txtChannels.Size = new System.Drawing.Size(59, 20);
            this.txtChannels.TabIndex = 24;
            this.txtChannels.Text = "89";
            // 
            // cbStartFreq
            // 
            this.cbStartFreq.FormattingEnabled = true;
            this.cbStartFreq.Items.AddRange(new object[] {
            "191500",
            "191600",
            "191700",
            "191800",
            "192000"});
            this.cbStartFreq.Location = new System.Drawing.Point(214, 88);
            this.cbStartFreq.Name = "cbStartFreq";
            this.cbStartFreq.Size = new System.Drawing.Size(162, 21);
            this.cbStartFreq.Sorted = true;
            this.cbStartFreq.TabIndex = 23;
            this.cbStartFreq.Text = "191700";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(48, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(166, 20);
            this.label2.TabIndex = 22;
            this.label2.Text = "Start Frequency(GHz)";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(48, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 20);
            this.label1.TabIndex = 21;
            this.label1.Text = "Channels";
            // 
            // btnSet
            // 
            this.btnSet.Location = new System.Drawing.Point(593, 366);
            this.btnSet.Name = "btnSet";
            this.btnSet.Size = new System.Drawing.Size(95, 38);
            this.btnSet.TabIndex = 25;
            this.btnSet.Text = "Set";
            this.btnSet.UseVisualStyleBackColor = true;
            this.btnSet.Click += new System.EventHandler(this.btnSet_Click);
            // 
            // btnGetChanPassFile
            // 
            this.btnGetChanPassFile.Location = new System.Drawing.Point(511, 220);
            this.btnGetChanPassFile.Name = "btnGetChanPassFile";
            this.btnGetChanPassFile.Size = new System.Drawing.Size(51, 22);
            this.btnGetChanPassFile.TabIndex = 28;
            this.btnGetChanPassFile.Text = "Browse";
            this.btnGetChanPassFile.UseVisualStyleBackColor = true;
            this.btnGetChanPassFile.Click += new System.EventHandler(this.btnGetChanPassFile_Click);
            // 
            // txtPassedChannelFileName
            // 
            this.txtPassedChannelFileName.Location = new System.Drawing.Point(214, 222);
            this.txtPassedChannelFileName.Name = "txtPassedChannelFileName";
            this.txtPassedChannelFileName.Size = new System.Drawing.Size(291, 20);
            this.txtPassedChannelFileName.TabIndex = 27;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(48, 222);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(158, 20);
            this.label4.TabIndex = 26;
            this.label4.Text = "Passed Channel  File";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.RestoreDirectory = true;
            this.openFileDialog1.SupportMultiDottedExtensions = true;
            // 
            // GetFinalTestData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnGetChanPassFile);
            this.Controls.Add(this.txtPassedChannelFileName);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnSet);
            this.Controls.Add(this.txtChannels);
            this.Controls.Add(this.cbStartFreq);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnGetItuFile);
            this.Controls.Add(this.txtItuFileName);
            this.Controls.Add(this.label3);
            this.Name = "GetFinalTestData";
            this.Size = new System.Drawing.Size(718, 435);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnGetItuFile;
        private System.Windows.Forms.TextBox txtItuFileName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtVleft;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtVright;
        private System.Windows.Forms.TextBox txtChannels;
        private System.Windows.Forms.ComboBox cbStartFreq;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSet;
        private System.Windows.Forms.Button btnGetChanPassFile;
        private System.Windows.Forms.TextBox txtPassedChannelFileName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtImbLeft;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtImbRight;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}

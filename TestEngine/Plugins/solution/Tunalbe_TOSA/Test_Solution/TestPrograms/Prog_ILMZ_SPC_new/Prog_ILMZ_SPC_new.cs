// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestPrograms
//
// Prog_ILMZ_Qual_ND.cs
//
// Author: alice.huang, 2010
// Design: [Reference design documentation]

using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.ExternalData;
using Bookham.TestEngine.Framework.Limits;
using System.Collections.Specialized;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.ToolKit.Mz;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestSolution.IlmzCommonData;
using Bookham.TestSolution.IlmzCommonInstrs;
using Bookham.TestLibrary.Instruments;
using Bookham.TestLibrary.Utilities;
using Bookham.TestSolution.IlmzCommonUtils;
using Bookham.TestSolution.TestModules;
using Bookham.TestSolution.ILMZFinalData;
using System.Threading;
using System.IO.Ports;
using Bookham.TestEngine.Config;
using Bookham.TestLibrary.BlackBoxes;
using Bookham.Program.Core;
using Bookham.TestSolution.Instruments;

namespace Bookham.TestSolution.TestPrograms
{
    /// <summary>
    /// 
    /// </summary>
    public class Prog_ILMZ_SPC_new : TestProgramCore
    {
        #region Private data

        private bool isgrr = false;
        private Prog_ILMZ_QualInfo progInfo;
        private progIlmzQualInstruments progInstrs = null;
        private DatumList finalTestResult;
        private double labourTime = 0;
        private IlmzChannels IlmzItuChannels;
        private string errorInformation;
        private string channelPassFile;
        private string Nofilepath = @"Configuration\TOSA Final\MapTest\NoDataFile.txt";

        /// channel dsdbr settings from mapping test, use this setting to find 193700GHz or 18900 GHz        
        private DsdbrChannelData[] closeGridCharData;
        private DsdbrChannelData[] ituChanData;

        private List<IlmzQualChanPreviousData> qualTestLowChanList = null;
        private List<IlmzQualChanPreviousData> qualTestMidChanList = null;
        private List<IlmzQualChanPreviousData> qualTestHighChanList = null;

        private int numberOfSupermodes;

        private string ChirpType = null;

        string itufile;

        //bool SelectTestFlag;
        //private List<SectionIVSweepData> SectionIVCurveData;

        #endregion

        #region for Light Tower.

        private SerialPort _serialPort;
        /// <summary>
        /// Serial port to control the lighter to indicate test messeage
        /// </summary>
        public SerialPort SerialPort
        {
            get { return _serialPort; }
        }

        /// <summary>
        /// Construct a serial port object from parts of the resource string.
        /// </summary>
        /// <param name="comPort">COMx port portion of resource string.</param>
        /// <param name="rate">bps rate portion of resource string.</param>
        /// <returns>Constructed SerialPort handler instance.</returns>
        private SerialPort buildSerPort(string comPort, string rate)
        {
            return new System.IO.Ports.SerialPort(
                "COM" + comPort,                                /* COMn */
                int.Parse(rate),                                /* bps */
                System.IO.Ports.Parity.None, 8, StopBits.One);  /* 8N1 */
        }
        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        public Prog_ILMZ_SPC_new()
        {
            this.progInfo = new Prog_ILMZ_QualInfo();
        }

        /// <summary>
        /// 
        /// </summary>
        public override Type UserControl
        {
            get { return typeof(Prog_ILMZ_QualGui); }
        }

        #region Implement InitCode method

        /// <summary>
        /// Initialise test configuration
        /// </summary>
        /// <param name="dutObj">Device under test</param>
        /// <param name="engine">Test engine</param>
        protected override void InitConfig(ITestEngineInit engine, DUTObject dutObject)
        {
            base.InitConfig(engine, dutObject);

            progInfo.TestParamsConfig = new TestParamConfigAccessor(dutObject,
                @"Configuration\TOSA Final\FinalTest\CommonTestParams.xml", "", "TestParams");

            progInfo.LocalTestParamsConfig = new TestParamConfigAccessor(dutObject,
                 @"Configuration\TOSA Final\LocalTestParams.xml", "", "TestParams");

            progInfo.OpticalSwitchConfig = new TestParamConfigAccessor(dutObject,
                @"Configuration\TOSA Final\IlmzOpticalSwitch.xml", "", "IlmzOpticalSwitchParams");

            progInfo.TempConfig = new TempTableConfigAccessor(dutObject, 1, @"Configuration\TOSA Final\TempTable.xml");

            progInfo.MapParamsConfig = new TestParamConfigAccessor(dutObject, @"Configuration\TOSA Final\MapTest\MappingTestConfig.xml", "", "MappingTestParams");

            string Nofilepath_Temp = Path.Combine(this.CommonTestConfig.ResultsSubDirectory, Path.GetFileNameWithoutExtension(Nofilepath) + "_" + TestTimeStamp_Start + Path.GetExtension(Nofilepath));
            File.Copy(Nofilepath, Nofilepath_Temp);
            File.SetAttributes(Nofilepath_Temp, FileAttributes.Normal);
            Nofilepath = Nofilepath_Temp;//Jack.zhang fix Nofile load by stream bug.2011-09-06
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="engine"></param>
        /// <returns></returns>
        protected override void LoadSpecs(ITestEngineInit engine, DUTObject dutObject)
        {
            engine.SendStatusMsg("Loading Specification");
            ILimitRead limitReader = null;
            StringDictionary mainSpecKeys = new StringDictionary();
            StringDictionary finalSpecKeys = new StringDictionary();

            if (progInfo.TestParamsConfig.GetBoolParam("UseLocalLimitFile"))
            {
                // initialise limit file reader
                limitReader = engine.GetLimitReader("PCAS_FILE");
                // main specification
                string final_SpecNameFile = progInfo.TestParamsConfig.GetStringParam("SPCLimitFileName");
                string finalSpecFullFilename = progInfo.TestParamsConfig.GetStringParam("LocalLimitFileDirectory")
                    + @"\" + final_SpecNameFile;
                mainSpecKeys.Add("Filename", finalSpecFullFilename);
            }
            else
            {
                // Use default limit source
                limitReader = engine.GetLimitReader();


                mainSpecKeys.Add("SCHEMA", "HIBERDB");
                mainSpecKeys.Add("TEST_STAGE", "hitt debug01");
                mainSpecKeys.Add("DEVICE_TYPE", "hitt_spc_daily");
                /*
                if (dutObject.TestStage.Equals("hitt_spc"))
                {
                    mainSpecKeys.Add("TEST_STAGE", "Qual");
                    isgrr = false;
                }
                else if(dutObject.TestStage.Equals("FinalGRR"))
                {
                     mainSpecKeys.Add("TEST_STAGE", "Qual");
                     isgrr = true;
                }
                else
                {
                    mainSpecKeys.Add("TEST_STAGE", dutObject.TestStage);
                }
                if (dutObject.PartCode.ToUpper().Contains("8335"))
                {
                    mainSpecKeys.Add("DEVICE_TYPE", "HITT_QUAL_PA008335");
                }
                else
                {
                    mainSpecKeys.Add("DEVICE_TYPE", "HITT_QUAL_BETA");
                }*/
            }

            SpecList tempSpecList = limitReader.GetLimit(mainSpecKeys);
            progInfo.MainSpec = tempSpecList[0];
            this.MainSpec = progInfo.MainSpec;

            Specification dummySpec = new Specification("DummySpec", new StringDictionary(), 1);
            // declare spec list to Test Engine
            SpecList specList = new SpecList();
            specList.Add(progInfo.MainSpec);
            specList.Add(dummySpec);
            engine.SetSpecificationList(specList);

            progInfo.TestConditions = new IlmzQualTestConds(progInfo.MainSpec);

            base.LoadSpecs(engine, dutObject);
        }

        /// <summary>
        /// Validate DSDBR drive instruments - PXI, FCU2Asic or FCU
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        protected override void ValidateDsdbrDriveInstrs(ITestEngineInit engine, DUTObject dutObject)
        {
            if (dutObject.TestJigID == "FCUTestJigID")
            {
                progInfo.DsdbrInstrsUsed = DsdbrDriveInstruments.FCUInstruments;
            }
            else if (dutObject.TestJigID == "PXITestJigID")
            {
                progInfo.DsdbrInstrsUsed = DsdbrDriveInstruments.PXIInstruments;
            }
            else if (dutObject.TestJigID == "FCU2AsicTestJigId")
            {
                progInfo.DsdbrInstrsUsed = DsdbrDriveInstruments.FCU2AsicInstrument;
            }
            else
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "Unknown DSDBR drive instruments used!", FailModeCategory_Enum.HW);
            }

            if (progInfo.DsdbrInstrsUsed != DsdbrDriveInstruments.FCU2AsicInstrument)
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "Incorrect test Jig is used for qual test! Only the Fcu2Axic is available for this test. The test jig inused is " + dutObject.TestJigID, FailModeCategory_Enum.HW);
            }
        }

        /// <summary>
        /// Check Dut's test stage, if it is not in "qual" stage, prompt Error in program to stop test
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        protected override void ValidateTestStage(ITestEngineInit engine, DUTObject dutObject)
        {
            if (dutObject.TestStage.ToLower() != "qual" && dutObject.TestStage.ToLower() != "qual0" && dutObject.TestStage.ToLower() != "hitt_spc" && dutObject.TestStage.ToLower() != "hitt debug01")
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, string.Format("Un-matched test stage {0}. Expected \"qual\"", dutObject.TestStage), FailModeCategory_Enum.OP);
            }
            // Get test stage information - Must be "final"

            if (dutObject.TestStage.ToLower() == "hitt debug01")
            {
                progInfo.TestStage   = (FinalTestStage)Enum.Parse(typeof(FinalTestStage),
                                        "hitt_debug01", true);
            }
            else
            {
                progInfo.TestStage = (FinalTestStage)Enum.Parse(typeof(FinalTestStage),
                                        dutObject.TestStage.ToLowerInvariant(), true);
            }
        }

        /// <summary>
        /// Validate Map StageDate
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        protected override void ValidateMapStageData(ITestEngineInit engine, DUTObject dutObject)
        {
            IDataRead dataRead = engine.GetDataReader("PCAS_SHENZHEN");

            StringDictionary mapKeys = new StringDictionary();
            mapKeys.Add("SCHEMA", ILMZSchema);
            mapKeys.Add("SERIAL_NO", dutObject.SerialNumber);
            mapKeys.Add("TEST_STAGE", "hitt_map");

            try
            {
                this._mapResult = dataRead.GetLatestResults(mapKeys, true);
            }
            catch (Exception e)
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "Invalid map stage result for " + dutObject.SerialNumber + "\n" + e.Message, FailModeCategory_Enum.OP);
            }
            //check this map result is come from low cost test kits or not? Echo, 2011-11-02

            string equip_id = this._mapResult.ReadString("EQUIP_ID");

            //if (IsMapComeFromLowCostKits(equip_id))
            //{
            //    string errorMesg = "This map result is come from low cost kits,\nAnd Can't do final test on this kits,\n\nPlease test this device on other test kits! \n此器件需在其他非LOwCost測試台做Final測試！";
            //    engine.ErrorInProgram(errorMesg);
            //}

            bool mapPass = true;
            if (this._mapResult != null
                && this._mapResult.Count > 0)
            {
                mapPass = this._mapResult.ReadString("TEST_STATUS").ToLower().Contains("pass") ? true : false;
            }
            if (this._mapResult == null || !mapPass)
            {
                // engine.ErrorInProgram("Invalid map stage result for " + dutObject.SerialNumber + "\n" + "Test status is fail or test result is null.");

            }

            try
            {
                string MAP_TC_NUM_CHAN_REQUIRED = this._mapResult.ReadSint32("TC_NUM_CHAN_REQUIRED").ToString();
                string MAP_TC_OPTICAL_FREQ_START = this._mapResult.ReadDouble("TC_OPTICAL_FREQ_START").ToString();
            }
            catch
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "Can't load this 2 parameters @final test results: 'TC_NUM_CHAN_REQUIRED' & 'TC_OPTICAL_FREQ_START'.", FailModeCategory_Enum.SW);
            }

            #region Comment out by chongjian.liang 2015.7.13
            // Get data from ITU operating file
            //string itufile_temp = this._mapResult.ReadFileLinkFullPath("CG_CHAR_ITU_FILE");
            //int node = this._mapResult.ReadSint32("NODE");
            //this.itufile = CheckItuFilePath(itufile_temp, node.ToString());

            //if (!File.Exists(itufile))
            //{
            //    string errDescription = itufile + " does not exist.\n\nPlease contact the test engineer.";

            //    engine.ErrorInProgram(errDescription);
            //} 
            #endregion

            // chongjian.liang 2015.7.13
            this.itufile = this._mapResult.ReadFileLinkFullPath("CG_CHAR_ITU_FILE");

            this.closeGridCharData = Bookham.Toolkit.CloseGrid.CsvDataLookup.GetItuOperatingPoints(itufile);
        }

        /// <summary>
        ///  Validate all parameter required in qual test has contains in previous stage results
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        protected override void ValidateFinalStageData(ITestEngineInit engine, DUTObject dutObject)
        {
            if (progInfo.TestParamsConfig.GetBoolParam("IsUsePcasFinalData"))
            {
                IDataRead dataRead = engine.GetDataReader("PCAS_SHENZHEN");

                //2008-3-24: by Ken: Determine whether tcmz_map stage tested.
                StringDictionary finalKeys = new StringDictionary();
                finalKeys.Add("SCHEMA", ILMZSchema);
                finalKeys.Add("SERIAL_NO", dutObject.SerialNumber);
                //finalKeys.Add("DEVICE_TYPE", dutObject.PartCode);
                finalKeys.Add("TEST_STAGE", "final");
                //finalKeys.Add("LOT_TYPE", "Production");
                try
                {
                    this.finalTestResult = dataRead.GetLatestResults(finalKeys, true);
                }
                catch (Exception e)
                {
                    this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "Invalid final stage result for " + dutObject.SerialNumber + "\n" + e.Message, FailModeCategory_Enum.OP);
                }
                bool finalPass = true;

                this.finalTestResult = dataRead.GetLatestResults(finalKeys, true);

                if (finalTestResult != null
                    && finalTestResult.Count >= 0)
                {
                    finalPass = finalTestResult.ReadString("TEST_STATUS").ToLower().Contains("pass") ? true : false;
                }
                if (finalTestResult == null || !finalPass)
                {
                    // engine.ErrorInProgram("Invalid final stage result for " + dutObject.SerialNumber + "\n" + "Test status is fail or test result is null.");
                }

                #region Comment out by chongjian.liang 2015.7.13
                ////// Get data from Channel paas file
                //channelPassFile = this.finalTestResult.ReadFileLinkFullPath("CHANNEL_LASTOPTION_FILE");
                //int node = this.finalTestResult.ReadSint32("NODE");
                //channelPassFile = CheckItuFilePath(channelPassFile, node.ToString());

                //if (channelPassFile == null)
                //{
                //    string errDescription = "Can't find Itu operation point file." +
                //            "\n please contact IS to confirm this!";
                //    engine.ErrorInProgram(errDescription);
                //} 
                #endregion

                this.channelPassFile = this.finalTestResult.ReadFileLinkFullPath("CHANNEL_PASS_FILE");

                this.ituChanData = Bookham.Toolkit.CloseGrid.CsvDataLookup.GetChannelPassPoints(channelPassFile, true); //by tim
            }
            else
            {
                ManualEntryFinalTestReults(engine);
            }

            if (this.finalTestResult.IsPresent("TC_DSDBR_TEMP"))//temp for add DSDBR_temp in limit file jack.zhang 2012-04-19
                progInfo.TestConditions.DSDBR_TEMP_C = this.finalTestResult.ReadDouble("TC_DSDBR_TEMP");
        }

        /// <summary>
        /// Initialise instruments
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="instrs"></param>   
        /// <param name="chassis"></param>
        protected override void InitInstrs(ITestEngineInit engine, DUTObject dutObject, InstrumentCollection instrs, ChassisCollection chassis)
        {
            //add for avoidig instrument logging error
            engine.SendStatusMsg("Initialise Instruments");

            #region light_tower

            /* Set up Serial Binary Comms */
            // _serialPort = buildSerPort(comPort, rate);
            string lightTower = @"Configuration\TOSA Final\LightTowerParams.xml";
            progInfo.lightTowerConfig = new TestParamConfigAccessor(dutObject,
                lightTower, "_", "LightTowerParams");
            if (progInfo.lightTowerConfig.GetBoolParam("LightExist"))
            {
                try
                {

                    _serialPort = this.buildSerPort(progInfo.lightTowerConfig.GetStringParam("ComNum"), progInfo.lightTowerConfig.GetStringParam("BaudRate"));
                    _serialPort.Close();
                    if (!_serialPort.IsOpen)
                    {
                        _serialPort.Open();
                    }

                    // Add the light tower color.
                    _serialPort.WriteLine(progInfo.lightTowerConfig.GetStringParam("LightGreen"));
                    _serialPort.Close();
                }
                catch { }

            }
            #endregion

            progInstrs = new progIlmzQualInstruments();
            progInfo.Instrs = progInstrs;

            // Initialise DSDBR drive instruments
            switch (progInfo.DsdbrInstrsUsed)
            {
                case DsdbrDriveInstruments.FCUInstruments:
                    // FCU
                    progInstrs.Fcu = new FCUInstruments();
                    progInstrs.Fcu.FullbandControlUnit = (Instr_FCU)instrs["FullbandControlUnit"];
                    progInstrs.Fcu.SoaCurrentSource =
                                        (InstType_ElectricalSource)instrs["SoaCurrentSource"];
                    break;

                // Alice.Huang add FCU2Asic function    2010-02-01
                case DsdbrDriveInstruments.FCU2AsicInstrument:
                    engine.SendStatusMsg("Configuring FCU2Asic instruments");
                    progInstrs.Fcu2AsicInstrsGroups = new FCU2AsicInstruments();
                    if (instrs.Contains("FCUMKI_Source"))
                        progInstrs.Fcu2AsicInstrsGroups.FcuSource =
                                    (InstType_ElectricalSource)instrs["FCUMKI_Source"];
                    else
                        progInstrs.Fcu2AsicInstrsGroups.FcuSource = null;

                    progInstrs.Fcu2AsicInstrsGroups.AsicVccSource =
                                        (InstType_ElectricalSource)instrs["ASIC_VccSource"];
                    progInstrs.Fcu2AsicInstrsGroups.AsicVeeSource =
                                        (InstType_ElectricalSource)instrs["ASIC_VeeSource"];
                    progInstrs.Fcu2AsicInstrsGroups.Fcu2Asic = (Inst_Fcu2Asic)instrs["Fcu2Asic"];
                    break;

            }


            progInstrs.Mz = new IlMzInstruments();
            progInstrs.Mz.PowerMeter = (IInstType_TriggeredOpticalPowerMeter)instrs["OpmMz"];
            // OpmRef
            progInstrs.OpmReference = (IInstType_OpticalPowerMeter)instrs["OpmRef"];

            progInstrs.Mz.PowerMeter.EnableInputTrigger(false);
            progInstrs.Mz.PowerMeter.SetDefaultState();
            progInstrs.OpmReference.SetDefaultState();

            TestProgramCore.InitInstrs_Wavemeter(this.failModeCheck, engine, dutObject, instrs, progInfo.LocalTestParamsConfig, progInfo.OpticalSwitchConfig);
            
            // TECs
            progInstrs.TecDsdbr = (IInstType_TecController)instrs["TecDsdbr"];
            DsdbrUtils.opticalSwitch = (Inst_Ke2510)instrs["TecDsdbr"];
            DsdbrTuning.OpticalSwitch = (Inst_Ke2510)instrs["TecDsdbr"];

            progInstrs.TecCase = (IInstType_TecController)instrs["TecCase"];

            // Config instruments if not simulation mode
            if (!engine.IsSimulation)
            {
                engine.SendStatusMsg("Configuring Instruments");
                // Configure FCU
                ConfigureFCU2AsicInstruments(progInstrs.Fcu2AsicInstrsGroups);

                // Configure TEC controllers

                ConfigureTecController(engine, progInstrs.TecDsdbr, "TecDsdbr", false, progInfo.TestConditions.DSDBR_TEMP_C);
                ConfigureTecController(engine, progInstrs.TecCase, "TecCase", true, progInfo.TestConditions.MidTemp);

                //Echo remed it
                progInstrs.Mz.FCU2Asic = (Inst_Fcu2Asic)instrs["Fcu2Asic"];
                progInstrs.Mz.FCU2Asic.SwapWirebond = ParamManager.Conditions.IsSwapWirebond;
                progInstrs.OpmReference.SetDefaultState();
                progInstrs.Mz.PowerMeter.SetDefaultState();
            }
        }

        /// <summary>
        /// Initialise utilities
        /// </summary>
        /// <param name="engine"></param>
        protected override void InitUtils(ITestEngineInit engine)
        {
            engine.SendStatusMsg("Initialise utilities");
            // Initialise DsdbrUtils and Measurements utilities by testkit hardware info.
            switch (progInfo.DsdbrInstrsUsed)
            {
                case DsdbrDriveInstruments.FCU2AsicInstrument:
                    DsdbrUtils.Fcu2AsicInstrumentGroup = progInfo.Instrs.Fcu2AsicInstrsGroups;
                    Measurements.FCU2AsicInstrs = progInfo.Instrs.Fcu2AsicInstrsGroups;
                    SoaShutterMeasurement.Fcu2AsicInstrs = progInfo.Instrs.Fcu2AsicInstrsGroups;
                    break;
                case DsdbrDriveInstruments.FCUInstruments:
                    DsdbrUtils.FcuInstrumentGroup = progInfo.Instrs.Fcu;
                    Measurements.FCUInstrs = progInfo.Instrs.Fcu;
                    SoaShutterMeasurement.FCUInstrs = progInfo.Instrs.Fcu;
                    break;

            }
            Measurements.OSA = progInfo.Instrs.Osa;
            Measurements.MzHead = progInfo.Instrs.Mz.PowerMeter;
            Measurements.ExternalHead = progInfo.Instrs.OpmReference;
            //Measurements.SectionIVSweepRawData = null;
        }

        protected override void InitOpticalSwitchIoLine()
        {
            IlmzOpticalSwitchLines.C_Band_DigiIoLine = int.Parse(progInfo.OpticalSwitchConfig.GetStringParam("C_Band_Filter_IO_Line"));
            IlmzOpticalSwitchLines.C_Band_DigiIoLine_State = progInfo.OpticalSwitchConfig.GetBoolParam("C_Band_Filter_IO_Line_State");
            IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine = int.Parse(progInfo.OpticalSwitchConfig.GetStringParam("Dut_Ctap_IO_Line"));
            IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine_State = progInfo.OpticalSwitchConfig.GetBoolParam("Dut_Ctap_IO_Line_State");
            IlmzOpticalSwitchLines.Dut_Rx_DigiIoLine = int.Parse(progInfo.OpticalSwitchConfig.GetStringParam("Dut_Rx_IO_Line"));
            IlmzOpticalSwitchLines.Dut_Rx_DigiIoLine_State = progInfo.OpticalSwitchConfig.GetBoolParam("Dut_Rx_IO_Line_State");
            IlmzOpticalSwitchLines.Dut_Tx_DigiIoLine = int.Parse(progInfo.OpticalSwitchConfig.GetStringParam("Dut_Tx_IO_LIne"));
            IlmzOpticalSwitchLines.Dut_Tx_DigiIoLine_State = progInfo.OpticalSwitchConfig.GetBoolParam("Dut_Tx_IO_LIne_State");
            IlmzOpticalSwitchLines.Etalon_Ref_DigiIoLine = int.Parse(progInfo.OpticalSwitchConfig.GetStringParam("Etalon_Ref_IO_Line"));
            IlmzOpticalSwitchLines.Etalon_Ref_DigiIoLine_State = progInfo.OpticalSwitchConfig.GetBoolParam("Etalon_Ref_IO_Line_State");
            IlmzOpticalSwitchLines.Etalon_Rx_DigiIoLine = int.Parse(progInfo.OpticalSwitchConfig.GetStringParam("Etalon_Rx_IO_Line"));
            IlmzOpticalSwitchLines.Etalon_Rx_DigiIoLine_State = progInfo.OpticalSwitchConfig.GetBoolParam("Etalon_Rx_IO_Line_State");
            IlmzOpticalSwitchLines.Etalon_Tx_DigiIoLine = int.Parse(progInfo.OpticalSwitchConfig.GetStringParam("Etalon_Tx_IO_Line"));
            IlmzOpticalSwitchLines.Etalon_Tx_DigiIoLine_State = progInfo.OpticalSwitchConfig.GetBoolParam("Etalon_Tx_IO_Line_State");
            IlmzOpticalSwitchLines.L_Band_DigiIoLine = int.Parse(progInfo.OpticalSwitchConfig.GetStringParam("L_Band_Filter_IO_Line"));
            IlmzOpticalSwitchLines.L_Band_DigiIoLine_State = progInfo.OpticalSwitchConfig.GetBoolParam("L_Band_Filter_IO_Line_State");
        }

        /// <summary>
        /// Initialise test modules
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        protected override void InitModules(ITestEngineInit engine, DUTObject dutObject, InstrumentCollection instrs, ChassisCollection chassis)
        {
            engine.SendStatusMsg("Initialise modules");
            this.InitMod_Pin_Check(engine, instrs, chassis, dutObject);
            this.IlmzInitialise_InitModule(engine, dutObject);
            this.DeviceTempControl_InitModule(engine, "DsdbrTemperature", progInfo.Instrs.TecDsdbr, "TecDsdbr");
            //this.DeviceTempControl_InitModule(engine, "MzTemperature", progInfo.Instrs.TecMz, "TecMz");
            this.CaseTempControl_InitModule(engine, "CaseTemp_Mid", 25, progInfo.TestConditions.MidTemp);

            this.CaseTempControl_InitModule(engine, "CaseTemp_High", 25, 75);
            this.IlmzPowerUp_InitModule(engine, instrs);
          //this.IlmzCrossCalibration_InitModule(engine, instrs);
            this.IlmzPowerHeadCal_InitModule(engine, dutObject);
            this.IlmzQualTest_InitModule(engine, dutObject);
            this.IlmzQualTest_hightemp_InitModule(engine, dutObject);
            //this.IlmzGroupResults_InitModule(engine, dutObject);
            this.CaseTempControl_InitModule(engine, "CaseTemp_Safe", progInfo.TestConditions.MidTemp, 25);   // TODO - tidier method?
        }

        #endregion

        #region Initialize modules

        /// <summary>
        /// Initial Pin check for hittGB test(check the pins correlation with TCE and Vcc)
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="instrs"></param>
        /// <param name="chassis"></param>
        /// <param name="dutObject"></param>
        private void InitMod_Pin_Check(ITestEngineInit engine, InstrumentCollection instrs, ChassisCollection chassis, DUTObject dutObject)
        {
            double Temperature_LowLimit_Degree = 15.0;
            double Temperature_HighLimit_Degree = 40.0;

            ModuleRun modRun = engine.AddModuleRun("Mod_PinCheck", "Mod_PinCheck", string.Empty);

            modRun.Instrs.Add(instrs);
            modRun.Chassis.Add(chassis);
            modRun.ConfigData.AddString("TIME_STAMP", TestTimeStamp_Start);
            modRun.ConfigData.AddString("LASER_ID", dutObject.SerialNumber);
            modRun.ConfigData.AddDouble("Temperature_LowLimit_Degree", Temperature_LowLimit_Degree);
            modRun.ConfigData.AddDouble("Temperature_HighLimit_Degree", Temperature_HighLimit_Degree);
        }

        /// <summary>
        /// Configure Initialise Device module
        /// </summary>
        private void IlmzInitialise_InitModule(ITestEngineInit engine, DUTObject dutObject)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("IlmzInitialise", "IlmzInitialise", "");

            // Add instruments
            modRun.Instrs.Add("Osa", (Instrument)progInfo.Instrs.Osa);
            modRun.ConfigData.AddReference("MzInstruments", progInfo.Instrs.Mz);
            switch (progInfo.DsdbrInstrsUsed)
            {
                // added FCU2ASIC Option by Alice.Huang, for TOSA GB test     2010-02-01
                case DsdbrDriveInstruments.FCU2AsicInstrument:
                    modRun.ConfigData.AddReference("FCU2AsicInstruments",
                                            progInfo.Instrs.Fcu2AsicInstrsGroups);
                    break;
                case DsdbrDriveInstruments.FCUInstruments:
                    modRun.ConfigData.AddReference("FcuInstruments", progInfo.Instrs.Fcu);
                    break;
            }

            // Add config data
            TcmzInitialise_Config tcmzInitConfig = new TcmzInitialise_Config();
            DatumList datumsRequired = tcmzInitConfig.GetDatumList();
            ExtractAndAddDatums(modRun, datumsRequired);
            modRun.ConfigData.AddSint32("NumChans", progInfo.TestConditions.NbrChannels);
            modRun.ConfigData.AddDouble("FreqLow_GHz", progInfo.TestConditions.ItuLowFreq_GHz);
            modRun.ConfigData.AddDouble("FreqHigh_GHz", progInfo.TestConditions.ItuHighFreq_GHz);
            modRun.ConfigData.AddDouble("FreqSpace_GHz", progInfo.TestConditions.ItuSpacing_GHz);
            modRun.ConfigData.AddString("DutSerialNbr", dutObject.SerialNumber);
            modRun.ConfigData.AddString("MzFileDirectory", this.CommonTestConfig.ResultsSubDirectory);
            modRun.ConfigData.AddEnum("DsdbrInstrsGroup", progInfo.DsdbrInstrsUsed);
            switch (progInfo.DsdbrInstrsUsed)
            {
                // added FCU2ASIC Option by Alice.Huang, for TOSA GB test     2010-02-01
                case DsdbrDriveInstruments.FCU2AsicInstrument:

                case DsdbrDriveInstruments.FCUInstruments:
                    break;
                case DsdbrDriveInstruments.PXIInstruments:
                    string closeGridRegKey = this.progInfo.TestParamsConfig.GetStringParam("CloseGridRegKey");
                    modRun.ConfigData.AddString("CloseGridRegKey", closeGridRegKey);
                    break;
            }
        }

        /// <summary>
        /// Configure a Device Temperature Control module
        /// </summary>
        private void DeviceTempControl_InitModule(ITestEngineInit engine, string moduleName,
            IInstType_SimpleTempControl tecController, string configPrefix)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun(moduleName, "SimpleTempControl", "");

            // Add instrument
            modRun.Instrs.Add("Controller", (Instrument)tecController);

            //load config
            double timeout_S = this.progInfo.TestParamsConfig.GetDoubleParam(configPrefix + "_Timeout_S");
            double stabTime_S = this.progInfo.TestParamsConfig.GetDoubleParam(configPrefix + "_StabiliseTime_S");
            int updateTime_mS = this.progInfo.TestParamsConfig.GetIntParam(string.Format("{0}_UpdateTime_mS", configPrefix));
            double sensorSetPoint_C = this.progInfo.TestConditions.DSDBR_TEMP_C;
            double tolerance_C = this.progInfo.TestParamsConfig.GetDoubleParam(string.Format("{0}_Tolerance_C", configPrefix));

            // Add config data
            DatumList tempConfig = new DatumList();
            tempConfig.AddDouble("datumMaxExpectedTimeForOperation_s", timeout_S);
            tempConfig.AddDouble("RqdStabilisationTime_s", stabTime_S);
            tempConfig.AddSint32("TempTimeBtwReadings_ms", updateTime_mS);
            tempConfig.AddDouble("SetPointTemperature_C",  sensorSetPoint_C);
            tempConfig.AddDouble("TemperatureTolerance_C", tolerance_C);

            modRun.ConfigData.AddListItems(tempConfig);
        }

        /// <summary>
        /// Configure Case Temperature module
        /// </summary>
        private void CaseTempControl_InitModule(ITestEngineInit engine,
            string moduleName, Double currentTemp, Double tempSetPoint)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun(moduleName, "SimpleTempControl", "");

            // Add instrument
            modRun.Instrs.Add("Controller", (Instrument)progInfo.Instrs.TecCase);

            // Add config data
            TempTablePoint tempCal = progInfo.TempConfig.GetTemperatureCal(currentTemp, tempSetPoint)[0];
            DatumList tempConfig = new DatumList();
            int guiTimeOut_ms = this.progInfo.TestParamsConfig.GetIntParam("CaseTempGui_UpdateTime_mS");
            // Timeout = 2 * calibrated time, minimum of 60secs
            double timeout_s = Math.Max(tempCal.DelayTime_s * 15, 60);
            tempConfig.AddDouble("datumMaxExpectedTimeForOperation_s", timeout_s);
            // Stabilisation time = 0.5 * calibrated time
            tempConfig.AddDouble("RqdStabilisationTime_s", tempCal.DelayTime_s);
            tempConfig.AddSint32("TempTimeBtwReadings_ms", guiTimeOut_ms);
            // Actual temperature to set
            tempConfig.AddDouble("SetPointTemperature_C", tempSetPoint + tempCal.OffsetC);
            tempConfig.AddDouble("TemperatureTolerance_C", tempCal.Tolerance_degC);
            modRun.ConfigData.AddListItems(tempConfig);
        }

        /// <summary>
        /// Initialise IlmzPowerUp module
        /// </summary>
        /// <param name="engine"></param>
        private void IlmzPowerUp_InitModule(ITestEngineInit engine, InstrumentCollection instrs)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("IlmzPowerUp", "IlmzPowerUp", "");

            // Add instruments
            modRun.Instrs.Add("OpticalPowerMeter", (Instrument)progInfo.Instrs.Mz.PowerMeter);
            modRun.Instrs.Add(instrs);
            modRun.ConfigData.AddReference("MzInstruments", progInfo.Instrs.Mz);

            // Add config data
            modRun.ConfigData.AddDouble("MzTapBias_V", progInfo.TestParamsConfig.GetDoubleParam("MzTapBias_V"));
            modRun.ConfigData.AddDouble("PowerupMinipower_mW", progInfo.TestParamsConfig.GetDoubleParam("PowerupMinipower_mW"));
            modRun.Limits.AddParameter(progInfo.MainSpec, "IRX_DARK", "RxLockerDarkCurrent_mA");
            modRun.Limits.AddParameter(progInfo.MainSpec, "ITX_DARK", "TxLockerDarkCurrent_mA");
            modRun.Limits.AddParameter(progInfo.MainSpec, "MZ_MOD_LEFT_DARK_I", "MzLeftModDarkCurrent_mA");
            modRun.Limits.AddParameter(progInfo.MainSpec, "MZ_MOD_RIGHT_DARK_I", "MzRightModDarkCurrent_mA");
            modRun.Limits.AddParameter(progInfo.MainSpec, "TAP_COMP_DARK_I", "MzTapCompDarkCurrent_mA");
        }

        /// <summary>
        /// Initialise IlmzCrossCalibration module
        /// </summary>
        /// <param name="engine"></param>
        private void IlmzCrossCalibration_InitModule(ITestEngineInit engine, InstrumentCollection instrs)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("IlmzCrossCalibration", "IlmzCrossCalibration", "");
            modRun.ConfigData.AddDouble("FreqDivTolerance_GHz", Convert.ToDouble(
               progInfo.MainSpec.GetParamLimit("XCAL_CH_MID_MAP_GB").HighLimit.ValueToString()));
            // Add instruments

            // Add config data
            modRun.ConfigData.AddSint32("OpticalCal_delay_ms", 2000);

            modRun.Instrs.Add("Optical_switch", (Instrument)progInfo.Instrs.TecDsdbr);
            modRun.Instrs.Add(instrs);
            
            // Tie up to limits


             modRun.Limits.AddParameter(progInfo.MainSpec, "XCAL_CH_LO_MAP_GB", "XCAL_CH_LO");
            modRun.Limits.AddParameter(progInfo.MainSpec, "XCAL_CH_MID_MAP_GB", "XCAL_CH_MID");
             modRun.Limits.AddParameter(progInfo.MainSpec, "XCAL_CH_HI_MAP_GB", "XCAL_CH_HI");
        }
        

        /// <summary>
        /// Initialise IlmzPowerHeadCal module
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutobject"></param>
        private void IlmzPowerHeadCal_InitModule(ITestEngineInit engine, DUTObject dutobject)
        {
            ModuleRun modRun;
            modRun = engine.AddModuleRun("IlmzPowerHeadCal", "TcmzPowerHeadCal", "");

            // Add instruments
            modRun.Instrs.Add("OpmReference", (Instrument)progInfo.Instrs.OpmReference);
            modRun.Instrs.Add("Wavemeter", (Instrument)progInfo.Instrs.Wavemeter);
            modRun.Instrs.Add("OpmMZ", (Instrument)progInfo.Instrs.Mz.PowerMeter);
            switch (progInfo.DsdbrInstrsUsed)
            {
                // added FCU2ASIC Option by Alice.Huang, for TOSA GB test     2010-02-01
                case DsdbrDriveInstruments.FCU2AsicInstrument:
                    modRun.Instrs.Add("Fcu2Asic", (Instrument)progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic);
                    break;
                case DsdbrDriveInstruments.FCUInstruments:
                    break;
                case DsdbrDriveInstruments.PXIInstruments:
                    modRun.Instrs.Add("OpmCgDirect", (Instrument)progInfo.Instrs.OpmReference);
                    modRun.Instrs.Add("OpmCgFilter", (Instrument)progInfo.Instrs.OpmCgFilter);
                    break;
            }

            // Add any config data here ...
            modRun.ConfigData.AddEnum("TestStage", progInfo.TestStage);
            modRun.ConfigData.AddSint32("OpticalCal_delay_ms", 2000);
            modRun.ConfigData.AddDouble("OpticalCal_MZ_Max", progInfo.LocalTestParamsConfig.GetDoubleParam("OpticalCal_MZ_Max"));
            modRun.ConfigData.AddDouble("OpticalCal_MZ_Min", progInfo.LocalTestParamsConfig.GetDoubleParam("OpticalCal_MZ_Min"));
            modRun.ConfigData.AddString("PlotFileDirectory", this.CommonTestConfig.ResultsSubDirectory);
            modRun.ConfigData.AddString("SN", dutobject.SerialNumber);
            modRun.ConfigData.AddDouble("Ref_Mz_IMB_Left_mA", this._mapResult.ReadDouble("REF_MZ_IMB_LEFT"));
            modRun.ConfigData.AddDouble("Ref_Mz_IMB_Right_mA", this._mapResult.ReadDouble("REF_MZ_IMB_RIGHT"));
            modRun.ConfigData.AddString("Frequency_band", progInfo.TestConditions.FreqBand.ToString());
            switch (progInfo.DsdbrInstrsUsed)
            {
                // added FCU2ASIC Option by Alice.Huang, for TOSA GB test     2010-02-01
                case DsdbrDriveInstruments.FCU2AsicInstrument:

                case DsdbrDriveInstruments.FCUInstruments:
                    modRun.ConfigData.AddBool("IsCalPXIT", false);
                    break;
                case DsdbrDriveInstruments.PXIInstruments:
                    modRun.ConfigData.AddBool("IsCalPXIT", true);
                    modRun.ConfigData.AddDouble("OpticalCal_CgDirect_Max", progInfo.LocalTestParamsConfig.GetDoubleParam("OpticalCal_CgDirect_Max"));
                    modRun.ConfigData.AddDouble("OpticalCal_CgDirect_Min", progInfo.LocalTestParamsConfig.GetDoubleParam("OpticalCal_CgDirect_Min"));
                    break;
            }
        }

        /// <summary>
        /// Initialise IlmzMzCharacterise module
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        private void IlmzQualTest_hightemp_InitModule(ITestEngineInit engine, DUTObject dutObject)
        {
            // Initialise module
            ModuleRun modRun;
            IlmzQualChanPreviousData[] chanPrevData = null;

            // First get channel data from final stage
            #region retrieve data from final data
            try
            {
                chanPrevData = GetChanDsdbrData(engine);
            }
            catch (FileNotFoundException ex)
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, ex.Message + " please check the previous stage test result or file server!", FailModeCategory_Enum.OP);
            }
            catch (FormatException) // chongjian.liang 2014.11.25
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "Some data of the failed channels for Qual/Qual0 test is probably missing, please make sure the failed channels also contains the complete data as a passed channel!", FailModeCategory_Enum.OP);
            }
            catch (Exception ex) // chongjian.liang 2014.11.25
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, ex.Message, FailModeCategory_Enum.UN);
            }

            // Check if low, mid and high channel are  all available for test
            if (chanPrevData.Length == 0 || chanPrevData == null)
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "No passed channel from previous stage is available for qual test, please stop test on this device.", FailModeCategory_Enum.OP);
            }
            if (chanPrevData.Length < 3)
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "Passed channels in previous stage <3, this device is not available for qual test, please stop test on this device.", FailModeCategory_Enum.OP);
            }
            if (qualTestLowChanList.Count == 0)
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "No passed low channels from previous stage is available for qual test, please stop test on this device.", FailModeCategory_Enum.OP);
            }
            if (qualTestMidChanList.Count == 0)
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "No passed middle channels from previous stage is available for qual test, please stop test on this device.", FailModeCategory_Enum.OP);
            }
            if (qualTestHighChanList.Count == 0)
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "No passed high channels from previous stage is available for qual test, please stop test on this device.", FailModeCategory_Enum.OP);
            }
            #endregion
            // If previou stage is not final stage, update the channels's reference data from previous test results
            if (progInfo.TestParamsConfig.GetStringParam("PreviousStageToRetrieveData") != "final")
                GetChanPreviousData(engine, dutObject, ref chanPrevData);

            modRun = engine.AddModuleRun("Qual test high", "IlmzQualTest_spc", "");

            modRun.ConfigData.AddString("ChirpType", dutObject.Attributes.ReadString("ChirpType"));

            // Add instruments
            modRun.Instrs.Add("VccSource", (Instrument)progInfo.Instrs.Fcu2AsicInstrsGroups.AsicVccSource);
            modRun.Instrs.Add("VeeSource", (Instrument)progInfo.Instrs.Fcu2AsicInstrsGroups.AsicVeeSource);
            modRun.Instrs.Add("GBTecController", (Instrument)progInfo.Instrs.TecDsdbr);
            modRun.ConfigData.AddReference("MzInstruments", progInfo.Instrs.Mz);

            TcmzMzSweep_Config tcmzConfig = new TcmzMzSweep_Config();
            DatumList datumsRequired = tcmzConfig.GetDatumList();
            ExtractAndAddDatums(modRun, datumsRequired);

            double mzBias1 = progInfo.TestParamsConfig.GetDoubleParam("MzFixedModBias_1_V");
            double mzBias2 = progInfo.TestParamsConfig.GetDoubleParam("MzFixedModBias_2_V");

            //START MODIFICATION, 2007.09.26, BY Ken.Wu ( TCMZ Vpi Correction )
            // Read values needed for TCMZ Vpi calculation from config file.

            modRun.ConfigData.AddDoubleArray("MZFixedModBias_volt", new double[] { mzBias1, mzBias2 });

            modRun.ConfigData.AddSint32("NumberOfPoints",
                progInfo.TestParamsConfig.GetIntParam("MzSweepNumberOfPoints"));
            modRun.ConfigData.AddString("DutSerialNbr", dutObject.SerialNumber);
            modRun.ConfigData.AddReference("TestSpec", progInfo.MainSpec);
            modRun.ConfigData.AddString("MzFileDirectory", this.CommonTestConfig.ResultsSubDirectory);
            modRun.ConfigData.AddReference("ChanReferenceData", chanPrevData);
            //#warning Add MZTapBias_V to config class
            modRun.ConfigData.AddDouble("MZInlineTapBias_V",
                modRun.ConfigData.ReadDouble("MZTapBias_V"));
            modRun.ConfigData.AddBool("DoSoftLockerRatioTuning",
                progInfo.TestParamsConfig.GetBoolParam("DoSoftLockerRatioTuning"));
            modRun.ConfigData.AddBool("DoTapLevelling",
                progInfo.TestParamsConfig.GetBoolParam("DoTapLevelling_Qual"));
            modRun.ConfigData.AddDouble("TapPhotoCurrentToleranceRatio",
                progInfo.TestParamsConfig.GetDoubleParam("TapPhotoCurrentToleranceRatio"));
            modRun.ConfigData.AddBool("CalMzChrPowerDelta",
                progInfo.TestParamsConfig.GetBoolParam("CalMzChrPowerDelta"));
            modRun.ConfigData.AddSint32("MZSourceMeasureDelay_ms", progInfo.TestParamsConfig.GetIntParam("MZSourceMeasureDelay_ms"));
            modRun.ConfigData.AddDouble("MzCtrlINominal_thermal_mA", progInfo.TestParamsConfig.GetDoubleParam("MzCtrlINominal_thermal_mA"));
            modRun.ConfigData.AddReference("FilesToZip", GetFilesToZip(progInfo.TestParamsConfig.GetStringParam("ZipFileListOnQual")));

            modRun.ConfigData.AddString("TestStage", progInfo.TestStage.ToString());


            // Limits link
            //modRun.Limits.AddParameter(progInfo.MainSpec, "LASER_RTH", "Laser_Rth");
            //modRun.Limits.AddParameter(progInfo.MainSpec, "MZ_INITIAL_CAL_SWEEP_FILE", "SweepDataZipFile");

           /* modRun.Limits.AddParameter(progInfo.MainSpec, "PACK_DISS_TCASE_HIGH_HT", "PACK_DISS_TCASE_HIGH");
            modRun.Limits.AddParameter(progInfo.MainSpec, "PACK_DISS_TCASE_MID_HT", "PACK_DISS_TCASE_MID");
            modRun.Limits.AddParameter(progInfo.MainSpec, "PACK_DISS_TCASE_LOW_HT", "PACK_DISS_TCASE_LOW");

            modRun.Limits.AddParameter(progInfo.MainSpec, "FREQ_UNLOCK_CH_HIGH_HT", "FREQ_UNLOCK_CH_HIGH");
            modRun.Limits.AddParameter(progInfo.MainSpec, "FREQ_UNLOCK_CH_MID_HT", "FREQ_UNLOCK_CH_MID");
            modRun.Limits.AddParameter(progInfo.MainSpec, "FREQ_UNLOCK_CH_LOW_HT", "FREQ_UNLOCK_CH_LOW");
            */
            // Add parameter for each channel

            // Parameter name in module result is strParamInMod + strParamModSuffix
            // Parameter name in PCAS result is strParamInPcas + strParamPCASSuffix
            string[] strParamModSuffix = new string[] { "_Low", "_Mid", "_High" };
            string[] strParamPCASSuffix = new string[] { "_CH_LOW_HT", "_CH_MID_HT", "_CH_HIGH_HT" };
            string[] strParamInMod = new string[] { "ITecGB_mA","VTecGB_V",
                "Vcc_Total_I_mA","MzLeftArmImb_mA","MzRightArmImb_mA","MzDcVpi_V","MzDcEr_dB",
                "MzDcModVMin_V","MzDcModVmax_V","FibrePwrPeak_dBm","Frequency_GHz" ,
                "I_Rx_mA","I_Tx_mA","I_Ctap_Trough_mA"};
            //string[] strParamInPcas = new string[] { "CHAN_INDX", "GB_TEC_I", "GB_TEC_V",
               // "LASER_TOTAL_I","MZ_CTRL_L_I","MZ_CTRL_R_I","MZ_DC_VPI","MZ_ER",
                //"MZ_MOD_MIN_L_V","MZ_MOD_PEAK_L_V","PEAK_POWER","OPTICAL_FREQ", 
               // "IRX_LOCK","ITX_LOCK","I_CTAP_TROUGH_OL"};
            string[] strParamInPcas = new string[] {  "GB_TEC_I", "GB_TEC_V",
                "LASER_TOTAL_I","MZ_CTRL_L_I","MZ_CTRL_R_I","MZ_DC_VPI","MZ_ER",
                "MZ_MOD_MIN_L_V","MZ_MOD_PEAK_L_V","PEAK_POWER","OPTICAL_FREQ", 
                "IRX_LOCK","ITX_LOCK","I_CTAP_TROUGH_OL"};

            //string paramMappingFileName = @"Configuration\TOSA Final\FinalTest\" +
                //progInfo.TestParamsConfig.GetStringParam("QualTestParamMapFile");

            string paramMappingFileName = @"Configuration\TOSA Final\FinalTest\SPCQualTestResultMapping_ht.csv";
            List<string[]> lines = null;
            try
            {
                using (CsvReader csvReader = new CsvReader())
                {
                    lines = csvReader.ReadFile(paramMappingFileName);
                }
            }
            catch
            { }

            if ((lines == null) || lines.Count < 2)
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "No qual test parameters mapping information is read from " + paramMappingFileName, FailModeCategory_Enum.OP);

                return;
            }

            List<string> modParamList = new List<string>();
            List<string> pcasParamList = new List<string>();

            for (int lineNbr = 1; lineNbr < lines.Count; lineNbr++)
            {
                string[] line = lines[lineNbr];
                // skip any empty lines - as detected by lack of commas
                if (line.Length <= 1) continue;
                modParamList.Add(line[0]);
                pcasParamList.Add(line[1]);
            }
            strParamInMod = modParamList.ToArray();
            strParamInPcas = pcasParamList.ToArray();
            string strModParam;
            string strPcasParam;

            for (int paramIndx = 0; paramIndx < strParamInMod.Length; paramIndx++)
            {
                for (int chanIndx = 0; chanIndx < 3; chanIndx++)
                {
                    strModParam = strParamInMod[paramIndx] + strParamModSuffix[chanIndx];
                    strPcasParam = strParamInPcas[paramIndx] + strParamPCASSuffix[chanIndx];
                    modRun.Limits.AddParameter(progInfo.MainSpec, strPcasParam, strModParam);
                }
            }
        }


        /// <summary>
        /// Initialise IlmzMzCharacterise module
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        private void IlmzQualTest_InitModule(ITestEngineInit engine, DUTObject dutObject)
        {
            // Initialise module
            ModuleRun modRun;
            IlmzQualChanPreviousData[] chanPrevData = null;

            // First get channel data from final stage
            #region retrieve data from final data
            try
            {
                chanPrevData = GetChanDsdbrData(engine);
            }
            catch (FileNotFoundException ex)
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, ex.Message + " please check the previous stage test result or file server!", FailModeCategory_Enum.OP);
            }
            catch (FormatException) // chongjian.liang 2014.11.25
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "Some data of the failed channels for Qual/Qual0 test is probably missing, please make sure the failed channels also contains the complete data as a passed channel!", FailModeCategory_Enum.OP);
            }
            catch (Exception ex) // chongjian.liang 2014.11.25
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, ex.Message, FailModeCategory_Enum.UN);
            }

            // Check if low, mid and high channel are  all available for test
            if (chanPrevData.Length == 0 || chanPrevData == null) {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "No passed channel from previous stage is available for qual test, please stop test on this device.", FailModeCategory_Enum.OP);
            }
            if (chanPrevData.Length < 3) {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "Passed channels in previous stage <3, this device is not available for qual test, please stop test on this device.", FailModeCategory_Enum.OP);
            }
            if (qualTestLowChanList.Count == 0) {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "No passed low channels from previous stage is available for qual test, please stop test on this device.", FailModeCategory_Enum.OP);
            }
            if (qualTestMidChanList.Count == 0) {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "No passed middle channels from previous stage is available for qual test, please stop test on this device.", FailModeCategory_Enum.OP);
            }
            if (qualTestHighChanList.Count == 0) {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "No passed high channels from previous stage is available for qual test, please stop test on this device.", FailModeCategory_Enum.OP);
            }
            #endregion
            // If previou stage is not final stage, update the channels's reference data from previous test results
            if (progInfo.TestParamsConfig.GetStringParam("PreviousStageToRetrieveData") != "final")
                GetChanPreviousData(engine, dutObject, ref chanPrevData);

            modRun = engine.AddModuleRun("Qual test", "IlmzQualTest_spc", "");

            modRun.ConfigData.AddString("ChirpType", dutObject.Attributes.ReadString("ChirpType"));

            // Add instruments
            modRun.Instrs.Add("VccSource", (Instrument)progInfo.Instrs.Fcu2AsicInstrsGroups.AsicVccSource);
            modRun.Instrs.Add("VeeSource", (Instrument)progInfo.Instrs.Fcu2AsicInstrsGroups.AsicVeeSource);
            modRun.Instrs.Add("GBTecController", (Instrument)progInfo.Instrs.TecDsdbr);
            modRun.ConfigData.AddReference("MzInstruments", progInfo.Instrs.Mz);

            TcmzMzSweep_Config tcmzConfig = new TcmzMzSweep_Config();
            DatumList datumsRequired = tcmzConfig.GetDatumList();
            ExtractAndAddDatums(modRun, datumsRequired);

            double mzBias1 = progInfo.TestParamsConfig.GetDoubleParam("MzFixedModBias_1_V");
            double mzBias2 = progInfo.TestParamsConfig.GetDoubleParam("MzFixedModBias_2_V");

            //START MODIFICATION, 2007.09.26, BY Ken.Wu ( TCMZ Vpi Correction )
            // Read values needed for TCMZ Vpi calculation from config file.

            modRun.ConfigData.AddDoubleArray("MZFixedModBias_volt", new double[] { mzBias1, mzBias2 });

            modRun.ConfigData.AddSint32("NumberOfPoints",
                progInfo.TestParamsConfig.GetIntParam("MzSweepNumberOfPoints"));
            modRun.ConfigData.AddString("DutSerialNbr", dutObject.SerialNumber);
            modRun.ConfigData.AddReference("TestSpec", progInfo.MainSpec);
            modRun.ConfigData.AddString("MzFileDirectory", this.CommonTestConfig.ResultsSubDirectory);
            modRun.ConfigData.AddReference("ChanReferenceData", chanPrevData);
            //#warning Add MZTapBias_V to config class
            modRun.ConfigData.AddDouble("MZInlineTapBias_V",
                modRun.ConfigData.ReadDouble("MZTapBias_V"));
            modRun.ConfigData.AddBool("DoSoftLockerRatioTuning",
                progInfo.TestParamsConfig.GetBoolParam("DoSoftLockerRatioTuning"));
            modRun.ConfigData.AddBool("DoTapLevelling",
                progInfo.TestParamsConfig.GetBoolParam("DoTapLevelling_Qual"));
            modRun.ConfigData.AddDouble("TapPhotoCurrentToleranceRatio",
                progInfo.TestParamsConfig.GetDoubleParam("TapPhotoCurrentToleranceRatio"));
            modRun.ConfigData.AddBool("CalMzChrPowerDelta",
                progInfo.TestParamsConfig.GetBoolParam("CalMzChrPowerDelta"));
            modRun.ConfigData.AddSint32("MZSourceMeasureDelay_ms", progInfo.TestParamsConfig.GetIntParam("MZSourceMeasureDelay_ms"));
            modRun.ConfigData.AddDouble("MzCtrlINominal_thermal_mA", progInfo.TestParamsConfig.GetDoubleParam("MzCtrlINominal_thermal_mA"));
            modRun.ConfigData.AddReference("FilesToZip", GetFilesToZip(progInfo.TestParamsConfig.GetStringParam("ZipFileListOnQual")));

            modRun.ConfigData.AddString("TestStage", progInfo.TestStage.ToString());


            // Limits link
            modRun.Limits.AddParameter(progInfo.MainSpec, "LASER_RTH", "Laser_Rth");
            modRun.Limits.AddParameter(progInfo.MainSpec, "MZ_INITIAL_CAL_SWEEP_FILE", "SweepDataZipFile");

           /* modRun.Limits.AddParameter(progInfo.MainSpec, "PACK_DISS_TCASE_HIGH", "PACK_DISS_TCASE_HIGH");
            modRun.Limits.AddParameter(progInfo.MainSpec, "PACK_DISS_TCASE_MID", "PACK_DISS_TCASE_MID");
            modRun.Limits.AddParameter(progInfo.MainSpec, "PACK_DISS_TCASE_LOW", "PACK_DISS_TCASE_LOW");

            modRun.Limits.AddParameter(progInfo.MainSpec, "FREQ_UNLOCK_CH_HIGH", "FREQ_UNLOCK_CH_HIGH");
            modRun.Limits.AddParameter(progInfo.MainSpec, "FREQ_UNLOCK_CH_MID", "FREQ_UNLOCK_CH_MID");
            modRun.Limits.AddParameter(progInfo.MainSpec, "FREQ_UNLOCK_CH_LOW", "FREQ_UNLOCK_CH_LOW");
            */
            // Add parameter for each channel

            // Parameter name in module result is strParamInMod + strParamModSuffix
            // Parameter name in PCAS result is strParamInPcas + strParamPCASSuffix
            string[] strParamModSuffix = new string[] { "_Low", "_Mid", "_High" };
            string[] strParamPCASSuffix = new string[] { "_CH_LOW", "_CH_MID", "_CH_HIGH" };
            string[] strParamInMod = new string[] { "ChannelIndex","ITecGB_mA","VTecGB_V",
                "Vcc_Total_I_mA","MzLeftArmImb_mA","MzRightArmImb_mA","MzDcVpi_V","MzDcEr_dB",
                "MzDcModVMin_V","MzDcModVmax_V","FibrePwrPeak_dBm","Frequency_GHz" ,
                "I_Rx_mA","I_Tx_mA","I_Ctap_Trough_mA"};
            string[] strParamInPcas = new string[] { "CHAN_INDX", "GB_TEC_I", "GB_TEC_V",
                "LASER_TOTAL_I","MZ_CTRL_L_I","MZ_CTRL_R_I","MZ_DC_VPI","MZ_ER",
                "MZ_MOD_MIN_L_V","MZ_MOD_PEAK_L_V","PEAK_POWER","OPTICAL_FREQ", 
                "IRX_LOCK","ITX_LOCK","I_CTAP_TROUGH_OL"};

           // string paramMappingFileName = @"Configuration\TOSA Final\FinalTest\" +
                //progInfo.TestParamsConfig.GetStringParam("QualTestParamMapFile");

            string paramMappingFileName = @"Configuration\TOSA Final\FinalTest\SPCQualTestResultMapping.csv";
            List<string[]> lines = null;
            try
            {
                using (CsvReader csvReader = new CsvReader())
                {
                    lines = csvReader.ReadFile(paramMappingFileName);
                }
            }
            catch
            { }

            if ((lines == null) || lines.Count < 2)
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "No qual test parameters mapping information is read from " + paramMappingFileName, FailModeCategory_Enum.OP);

                return;
            }

            List<string> modParamList = new List<string>();
            List<string> pcasParamList = new List<string>();

            for (int lineNbr = 1; lineNbr < lines.Count; lineNbr++)
            {
                string[] line = lines[lineNbr];
                // skip any empty lines - as detected by lack of commas
                if (line.Length <= 1) continue;
                modParamList.Add(line[0]);
                pcasParamList.Add(line[1]);
            }
            strParamInMod = modParamList.ToArray();
            strParamInPcas = pcasParamList.ToArray();
            string strModParam;
            string strPcasParam;

            for (int paramIndx = 0; paramIndx < strParamInMod.Length; paramIndx++)
            {
                for (int chanIndx = 0; chanIndx < 3; chanIndx++)
                {
                    strModParam = strParamInMod[paramIndx] + strParamModSuffix[chanIndx];
                    strPcasParam = strParamInPcas[paramIndx] + strParamPCASSuffix[chanIndx];
                    modRun.Limits.AddParameter(progInfo.MainSpec, strPcasParam, strModParam);
                }
            }
        }

        /// <summary>
        /// Initialise TcmzGroupResults module
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        private void IlmzGroupResults_InitModule(ITestEngineInit engine, DUTObject dutObject)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("IlmzGroupResults", "IlmzGroupResults", "");

            // Add config data
            modRun.ConfigData.AddBool("LowTempTestEnabled", progInfo.TestParamsConfig.GetBoolParam("LowTempTestEnabled"));

            // Tie up limits
            Specification spec = progInfo.MainSpec;
            modRun.Limits.AddParameter(spec, "AVERAGE_LOCK_RATIO", "AVERAGE_LOCK_RATIO");
            modRun.Limits.AddParameter(spec, "OPTICAL_FREQ_FIRST_CHAN_FAIL", "OPTICAL_FREQ_FIRST_CHAN_FAIL");
            modRun.Limits.AddParameter(spec, "FULL_PASS_CHAN_COUNT_OVERALL", "FULL_PASS_CHAN_COUNT_OVERALL");

            modRun.Limits.AddParameter(spec, "FIBRE_POWER_CHAN_TEMP_MIN", "FIBRE_POWER_CHAN_TEMP_MIN");
            modRun.Limits.AddParameter(spec, "FIBRE_POWER_CHAN_TEMP_MAX", "FIBRE_POWER_CHAN_TEMP_MAX");
            modRun.Limits.AddParameter(spec, "FIBRE_POWER_CHANGE_TCASE_HIGH", "FIBRE_POWER_CHANGE_TCASE_HIGH");
            modRun.Limits.AddParameter(spec, "FIBRE_POWER_FAIL_CHAN_TEMP", "FIBRE_POWER_FAIL_CHAN_TEMP");
            modRun.Limits.AddParameter(spec, "FIBRE_POWER_FAIL_OVER_TCASE", "FIBRE_POWER_FAIL_OVER_TCASE");
            modRun.Limits.AddParameter(spec, "TRACK_ERROR_PWR_TCASE", "TRACK_ERROR_PWR_TCASE");

            modRun.Limits.AddParameter(spec, "LOCK_FREQ_CHAN_TEMP_MIN", "LOCK_FREQ_CHAN_TEMP_MIN");
            modRun.Limits.AddParameter(spec, "LOCK_FREQ_CHAN_TEMP_MAX", "LOCK_FREQ_CHAN_TEMP_MAX");
            modRun.Limits.AddParameter(spec, "LOCK_FREQ_CHANGE_TCASE_HIGH", "LOCK_FREQ_CHANGE_TCASE_HIGH");
            modRun.Limits.AddParameter(spec, "LOCK_FREQ_FAIL_CHAN_TEMP", "LOCK_FREQ_FAIL_CHAN_TEMP");
            modRun.Limits.AddParameter(spec, "LOCK_FREQ_FAIL_OVER_TCASE", "LOCK_FREQ_FAIL_OVER_TCASE");
            modRun.Limits.AddParameter(spec, "TRACK_ERROR_FREQ_TCASE", "TRACK_ERROR_FREQ_TCASE");
            modRun.Limits.AddParameter(spec, "TRACK_ERROR_ULFREQ_TCASE", "TRACK_ERROR_ULFREQ_TCASE");

            modRun.Limits.AddParameter(spec, "PHASE_I_CHANGE_TCASE_HIGH", "PHASE_I_CHANGE_TCASE_HIGH");

            if (progInfo.TestParamsConfig.GetBoolParam("LowTempTestEnabled"))
            {
                modRun.Limits.AddParameter(spec, "FIBRE_POWER_CHANGE_TCASE_LOW", "FIBRE_POWER_CHANGE_TCASE_LOW");
                modRun.Limits.AddParameter(spec, "LOCK_FREQ_CHANGE_TCASE_LOW", "LOCK_FREQ_CHANGE_TCASE_LOW");
                modRun.Limits.AddParameter(spec, "PHASE_I_CHANGE_TCASE_LOW", "PHASE_I_CHANGE_TCASE_LOW");
            }
        }
        #endregion

        #region Configure instruments

        /// <summary>
        /// Configure a TEC controller using config data
        /// </summary>
        /// <param name="tecCtlr">Instrument reference</param>
        /// <param name="tecCtlId">Config table data prefix</param>
        private void ConfigureTecController(ITestEngineBase engine, IInstType_TecController tecCtlr, string tecCtlId, bool isTecCase, double temperatureSetPoint_C)
        {
            SteinhartHartCoefficients stCoeffs = new SteinhartHartCoefficients();

            if (tecCtlr.DriverName.Contains("Ke2510"))
            {
                // Keithley 2510 specific commands (must be done before 'SetDefaultState')
                tecCtlr.HardwareData["MinTemperatureLimit_C"] = progInfo.TestParamsConfig.GetStringParam(tecCtlId + "_MinTemp_C");
                tecCtlr.HardwareData["MaxTemperatureLimit_C"] = progInfo.TestParamsConfig.GetStringParam(tecCtlId + "_MaxTemp_C");

                tecCtlr.SetDefaultState();

                tecCtlr.Sensor_Type = (InstType_TecController.SensorType)Enum.Parse(typeof(InstType_TecController.SensorType), progInfo.TestParamsConfig.GetStringParam(tecCtlId + "_Sensor_Type"));

                tecCtlr.TecVoltageCompliance_volt = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_TecVoltageCompliance_volt");

                if (isTecCase)
                {
                    tecCtlr.DerivativeGain = progInfo.LocalTestParamsConfig.GetDoubleParam(tecCtlId + "_DerivativeGain");
                }
                else
                {
                    tecCtlr.DerivativeGain = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_DerivativeGain");
                }
            }
            else
            {
                tecCtlr.SetDefaultState();
            }

            tecCtlr.OperatingMode = (InstType_TecController.ControlMode)Enum.Parse(typeof(InstType_TecController.ControlMode), progInfo.TestParamsConfig.GetStringParam(tecCtlId + "_OperatingMode"));

            // Thermistor characteristics
            if (tecCtlr.Sensor_Type == InstType_TecController.SensorType.Thermistor_2wire ||
                tecCtlr.Sensor_Type == InstType_TecController.SensorType.Thermistor_4wire)
            {
                stCoeffs.A = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_stCoeffs_A");
                stCoeffs.B = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_stCoeffs_B");
                stCoeffs.C = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_stCoeffs_C");
                tecCtlr.SteinhartHartConstants = stCoeffs;
            }

            // Additional parameters
            if (isTecCase)
            {
                tecCtlr.TecCurrentCompliance_amp = progInfo.LocalTestParamsConfig.GetDoubleParam(tecCtlId + "_TecCurrentCompliance_amp");
                tecCtlr.ProportionalGain = progInfo.LocalTestParamsConfig.GetDoubleParam(tecCtlId + "_ProportionalGain");
                tecCtlr.IntegralGain = progInfo.LocalTestParamsConfig.GetDoubleParam(tecCtlId + "_IntegralGain");
            }
            else
            {
                tecCtlr.TecCurrentCompliance_amp = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_TecCurrentCompliance_amp");
                tecCtlr.ProportionalGain = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_ProportionalGain");
                tecCtlr.IntegralGain = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_IntegralGain");
            }

            try
            {
                tecCtlr.SensorTemperatureSetPoint_C = temperatureSetPoint_C;
            }
            catch (Exception e)
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, e.Message, FailModeCategory_Enum.UN);
            }

        }

        // added FCU2ASIC Option by Alice.Huang, for TOSA GB test     2010-02-01
        /// <summary>
        /// initialise FCU2ASIC INSTRUMENTS 
        /// </summary>
        /// <param name="fcu2AsicInstrs">FCU2ASIC instruments group </param>
        private void ConfigureFCU2AsicInstruments(FCU2AsicInstruments fcu2AsicInstrs)
        {
            double curMax = progInfo.LocalTestParamsConfig.GetDoubleParam("FcuSource_CurrentCompliance_A");

            if (fcu2AsicInstrs.FcuSource != null)
            {
                // Set to voltage source mode & set current compliance
                fcu2AsicInstrs.FcuSource.SetDefaultState();
                fcu2AsicInstrs.FcuSource.VoltageSetPoint_Volt = progInfo.LocalTestParamsConfig.GetDoubleParam("FcuSource_Voltage_V");
                fcu2AsicInstrs.FcuSource.CurrentComplianceSetPoint_Amp = curMax;
                fcu2AsicInstrs.FcuSource.OutputEnabled = true;
                System.Threading.Thread.Sleep(5000);
            }

            fcu2AsicInstrs.AsicVccSource.SetDefaultState();
            curMax = progInfo.LocalTestParamsConfig.GetDoubleParam("AsicVccSource_CurrentCompliance_A");

            // Set to voltage source mode & set current compliance
            fcu2AsicInstrs.AsicVccSource.VoltageSetPoint_Volt = progInfo.LocalTestParamsConfig.GetDoubleParam("AsicVccSource_Voltage_V");
            fcu2AsicInstrs.AsicVccSource.CurrentComplianceSetPoint_Amp = curMax;
            fcu2AsicInstrs.AsicVccSource.OutputEnabled = true;

            fcu2AsicInstrs.AsicVeeSource.SetDefaultState();
            curMax = progInfo.LocalTestParamsConfig.GetDoubleParam("AsicVeeSource_CurrentCompliance_A");
            // Set to voltage source mode & set current compliance  
            fcu2AsicInstrs.AsicVeeSource.VoltageSetPoint_Volt =
                        progInfo.LocalTestParamsConfig.GetDoubleParam("AsicVeeSource_Voltage_V");
            fcu2AsicInstrs.AsicVeeSource.CurrentComplianceSetPoint_Amp = curMax;
            fcu2AsicInstrs.AsicVeeSource.OutputEnabled = true;
            //load etalon wavemeter cal data from file

            string FrequencyCalibration_file_Path = "L_band_FrequencyCalibration_file_Path";
            string str = progInfo.TestConditions.FreqBand.ToString();
            if (str == "C")
                FrequencyCalibration_file_Path = "C_band_FrequencyCalibration_file_Path";

            string freqCalFilename = progInfo.MapParamsConfig.GetStringParam(FrequencyCalibration_file_Path);
            string FCUMKI_Tx_CalFilename = progInfo.MapParamsConfig.GetStringParam("FCUMKI_Tx_Calibration_file_Path");
            string FCUMKI_Rx_CalFilename = progInfo.MapParamsConfig.GetStringParam("FCUMKI_Rx_Calibration_file_Path");
            string FCUMKI_Var_CalFilename = progInfo.MapParamsConfig.GetStringParam("FCUMKI_Var_Calibration_file_Path");
            string FCUMKI_Fix_CalFilename = progInfo.MapParamsConfig.GetStringParam("FCUMKI_Fix_Calibration_file_Path");

            FreqCalByEtalon.LoadFreqlCalArray(freqCalFilename, 0.0, progInfo.TestConditions.FreqTolerance_GHz, progInfo.TestConditions.ItuSpacing_GHz);
            FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Tx = FreqCalByEtalon.LoadFCUMKICalArray(FCUMKI_Tx_CalFilename);
            FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Rx = FreqCalByEtalon.LoadFCUMKICalArray(FCUMKI_Rx_CalFilename);
            FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Var = FreqCalByEtalon.LoadFCUMKICalArray(FCUMKI_Var_CalFilename);
            FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Fix = FreqCalByEtalon.LoadFCUMKICalArray(FCUMKI_Fix_CalFilename);
            //FreqCalByEtalon.FrequencyTolerance_GHz = progInfo.TestParamsConfig.GetDoubleParam("LowCost_FrequencyTolerance_GHz");//jack.zhang to reduce LowCost ITU tuning marginal fail.(active after configure file merger) 2012-08-02
            FreqCalByEtalon.FrequencyTolerance_GHz = 1;//jack.zhang to reduce LowCost ITU tuning marginal fail.(temp) 2012-08-02
        }

        /// <summary>
        /// Configure a MZ bias source
        /// </summary>
        /// <param name="biasSrc">Instrument reference</param>
        /// <param name="biasSrcId">Config table data prefix</param>
        private void ConfigureMzBiasSource(IInstType_ElectricalSource biasSrc, string biasSrcId)
        {
            // Keithley 2400 specific commands (must be done before 'SetDefaultState')
            if (biasSrc.DriverName.Contains("Ke24xx"))
            {
                biasSrc.HardwareData["4wireSense"] = "true";
            }

            // Configure instrument
            biasSrc.SetDefaultState();
            // Set to current source mode & set compliance
            biasSrc.CurrentSetPoint_amp = 0.0;
            biasSrc.CurrentComplianceSetPoint_Amp = progInfo.TestParamsConfig.GetDoubleParam(biasSrcId + "_CurrentCompliance_A");
            // Set to voltage source mode & set compliance
            biasSrc.VoltageSetPoint_Volt = 0.0;
            biasSrc.VoltageComplianceSetPoint_Volt = progInfo.TestParamsConfig.GetDoubleParam(biasSrcId + "_VoltageCompliance_volt");
        }

        #endregion

        #region Program running

        /// <summary>
        /// 
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        /// <param name="instrs"></param>
        /// <param name="chassis"></param>
        public override void Run(ITestEngineRun engine, DUTObject dutObject,
            InstrumentCollection instrs, ChassisCollection chassis)
        {
            ModuleRun modRun;
            InstCommands.CloseInstToLoadDUT(engine, progInfo.Instrs.Fcu2AsicInstrsGroups.FcuSource, progInfo.Instrs.Fcu2AsicInstrsGroups.AsicVccSource, progInfo.Instrs.Fcu2AsicInstrsGroups.AsicVeeSource, progInfo.Instrs.TecDsdbr);

            //engine.ShowContinueUserQuery("Please click the continue button to test,请放置器件,点击 Continue 测试");

            if (progInfo.Instrs.Fcu2AsicInstrsGroups.FcuSource != null)
                progInfo.Instrs.Fcu2AsicInstrsGroups.FcuSource.OutputEnabled = true;
            //First do pin check
            engine.RunModule("Mod_PinCheck");
            ModRunData Pin_Check_Results = engine.GetModuleRunData("Mod_PinCheck");
            bool Pin_Check_Pass = Pin_Check_Results.ModuleData.ReadBool("Pin_Check_Pass");
            if (!Pin_Check_Pass)
            {
                this.errorInformation += Pin_Check_Results.ModuleData.ReadString("ErrorInformation");
                return;
            }
            //ModuleRun modRun;
            // Set retest count
            this.SetRetestCount(engine, dutObject);

            // Initialise 
            engine.RunModule("IlmzInitialise");

            // Asic Part
            progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.SetupChannelCalibration();

            progInstrs.Fcu2AsicInstrsGroups.Fcu2Asic.Inital_Etalon_WaveMter();//to avoid Var pot large change in overallmap FCUMKI pot leveling Jack.Zhang 2011-01-12
            // Set temperatures
            //  pre set the case temp to save some time
            progInfo.Instrs.TecCase.SensorTemperatureSetPoint_C = progInfo.TestConditions.MidTemp;
            // TODO set MZ & DSDBR according to FF data
            progInfo.Instrs.TecCase.OutputEnabled = true;
            //progInfo.Instrs.TecMz.OutputEnabled = true;

            #region Temperature control - chongjian.liang 2012.12.3

            // Set and wait for temperatures to stabilise

            ModuleRunReturn caseControlResult = engine.RunModule("CaseTemp_Mid");

            if (caseControlResult.ModuleRunData.ModuleData.IsPresent("ErrorInformation")) {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, caseControlResult.ModuleRunData.ModuleData.ReadString("ErrorInformation"), FailModeCategory_Enum.UN);
            }

            ModuleRunReturn dsdbrControlResult = engine.RunModule("DsdbrTemperature");

            if (dsdbrControlResult.ModuleRunData.ModuleData.IsPresent("ErrorInformation"))
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, dsdbrControlResult.ModuleRunData.ModuleData.ReadString("ErrorInformation"), FailModeCategory_Enum.UN);
            }
            #endregion

            // Get tcmz_map data.!
            this.CopyDataFromMap(this._mapResult, engine);
            this.numberOfSupermodes = this._mapResult.ReadSint32("NUM_SM");

            // Power up
            RunIlmzPowerUp(engine);

            // Measure of cross-calibration of frequency between map stage and final stage
            //RunCrossCalibrationMeasure(engine);

            // Power head calibration
            RunIlmzPowerHeadCal(engine);

            //// MZ Characterise 
            //modRun = engine.GetModuleRun("IlmzMzCharacterise");
            //modRun.PreviousTestData.AddReference("ItuChannels", closeGridCharData);
    
            ModuleRunReturn modRunReturn = engine.RunModule("Qual test");
            bool isTcmzMzCharacteriseError =
                modRunReturn.ModuleRunData.ModuleData.IsPresent("IsError") ? modRunReturn.ModuleRunData.ModuleData.ReadBool("IsError") : false;


            // Send the case temperature back to ambient whilst we clean up.
            progInfo.Instrs.TecCase.SensorTemperatureSetPoint_C = 25;


            ModuleRunReturn caseControlResulthigh = engine.RunModule("CaseTemp_High");

            if (caseControlResulthigh.ModuleRunData.ModuleData.IsPresent("ErrorInformation"))
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, caseControlResulthigh.ModuleRunData.ModuleData.ReadString("ErrorInformation"), FailModeCategory_Enum.UN);
            }
           
 
             
            ModuleRunReturn modRunReturnhigh = engine.RunModule("Qual test high");
            isTcmzMzCharacteriseError =
                modRunReturn.ModuleRunData.ModuleData.IsPresent("IsError") ? modRunReturnhigh.ModuleRunData.ModuleData.ReadBool("IsError") : false;

            // Consolodate results
            //modRun = engine.GetModuleRun("IlmzGroupResults");
            //modRun.ConfigData.AddReference("TcmzItuChannels", IlmzItuChannels);
            //engine.RunModule("IlmzGroupResults");

            // Set device to safe case temperature                 
            engine.RunModule("CaseTemp_Safe");
        }

        /// <summary>
        /// Run IlmzPowerHeadCal module
        /// </summary>
        /// <param name="engine"></param>
        private void RunIlmzPowerHeadCal(ITestEngineRun engine)
        {
            ModuleRun modRun;
            modRun = engine.GetModuleRun("IlmzPowerHeadCal");
            modRun.ConfigData.AddReference("CloseGridCharData", this.closeGridCharData);
            modRun.ConfigData.AddSint32("NUM_SM", numberOfSupermodes);
            ModuleRunReturn moduleRunReturn = engine.RunModule("IlmzPowerHeadCal");
            labourTime += moduleRunReturn.ModuleRunData.ModuleData.GetDatumDouble("LabourTime").Value;

            if (!moduleRunReturn.ModuleRunData.ModuleData.ReadBool("IsSuccessful") &&
                !progInfo.TestParamsConfig.GetBoolParam("IsProceedWhenPowerCaliFailed"))
            {
                // Raise error to end the test and write error to pcas result - chongjian.liang 2013.11.25
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "Power calibration failed. Please clean the fiber head, or ask the technician to recalibrate the power if fiber head is OK.", FailModeCategory_Enum.OP);
            }
        }

        /// <summary>
        /// Run IlmzPowerUp module
        /// </summary>
        /// <param name="engine"></param>
        private void RunIlmzPowerUp(ITestEngineRun engine)
        {
            ModuleRun modRun;
            modRun = engine.GetModuleRun("IlmzPowerUp");
            IlmzChannelInit chanInit = new IlmzChannelInit();

            // MZ data is from TcmzInitMzSweep
            chanInit.Mz.LeftArmImb_mA = this._mapResult.ReadDouble("REF_MZ_IMB_LEFT");
            chanInit.Mz.RightArmImb_mA = this._mapResult.ReadDouble("REF_MZ_IMB_RIGHT");
            chanInit.Mz.LeftArmMod_Min_V = 0;// this.mapTestResult.ReadDouble("REF_MZ_VOFF_LEFT"); //Jack.Zhang need to Get from Map PACS
            chanInit.Mz.RightArmMod_Min_V = 0;// this.mapTestResult.ReadDouble("REF_MZ_VOFF_RIGHT");
            chanInit.Mz.LeftArmMod_Peak_V = 0;// this.mapTestResult.ReadDouble("REF_MZ_VON_LEFT");
            chanInit.Mz.RightArmMod_Peak_V = 0;// this.mapTestResult.ReadDouble("REF_MZ_VON_RIGHT");

            if (this._mapResult.IsPresent("REF_MZ_VOFF_LEFT"))
            {
                chanInit.Mz.LeftArmMod_Min_V = this._mapResult.ReadDouble("REF_MZ_VOFF_LEFT"); //Jack.Zhang need to Get from Map PACS
            }
            if (this._mapResult.IsPresent("REF_MZ_VOFF_RIGHT"))
            {
                chanInit.Mz.RightArmMod_Min_V = this._mapResult.ReadDouble("REF_MZ_VOFF_RIGHT");
            }
            if (this._mapResult.IsPresent("REF_MZ_VON_LEFT"))
            {
                chanInit.Mz.LeftArmMod_Peak_V = this._mapResult.ReadDouble("REF_MZ_VON_LEFT");
            }
            if (this._mapResult.IsPresent("REF_MZ_VON_RIGHT"))
            {
                chanInit.Mz.RightArmMod_Peak_V = this._mapResult.ReadDouble("REF_MZ_VON_RIGHT");
            }
            // DSDBR data
            double testFreq_GHz = 193700;

            //Add for "L" band by Tim at 2009-2-13.
            if (progInfo.TestConditions.FreqBand == FreqBand.L)
                testFreq_GHz = 189000;
            foreach (DsdbrChannelData chanData in this.closeGridCharData)
            {
                if (Math.Abs(chanData.ItuFreq_GHz - testFreq_GHz) <= 20)
                {
                    chanInit.Dsdbr = chanData;
                    break;
                }
            }

            // switch the fcu cal data to pot1 to measure lock current

            if (!modRun.ConfigData.IsPresent("IlmzSettings")) modRun.ConfigData.AddReference("IlmzSettings", chanInit);
            ModuleRunReturn tcmzPowerUpRtn = engine.RunModule("IlmzPowerUp");
            if (tcmzPowerUpRtn.ModuleRunData.ModuleData.ReadBool("ErrorExists") == true)
            {
                // Raise error to end the test and write error to pcas result - chongjian.liang 2013.11.19
                engine.ErrorInProgram(tcmzPowerUpRtn.ModuleRunData.ModuleData.ReadString("ErrorString"));
            }
        }

        /// <summary>
        /// Run IlmzCrossCalibration module
        /// </summary>
        /// <param name="engine"></param>
        private void RunCrossCalibrationMeasure(ITestEngineRun engine)
        {
            ModuleRun modRun;
            modRun = engine.GetModuleRun("IlmzCrossCalibration");
            if (this.closeGridCharData.Length < 3)
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "Frequency channel less than 3 channels, pls check ITU operation point file!", FailModeCategory_Enum.OP);
            }

            // Get DsdbrChannelData for frequency cross-calibration
            int[] chanIndices = new int[3];
            chanIndices[0] = 0; // low channel
            chanIndices[1] = (progInfo.TestConditions.NbrChannels - 1) / 2; // mid channel 45
            chanIndices[2] = progInfo.TestConditions.NbrChannels - 1; // high channel

            if (this.closeGridCharData[closeGridCharData.Length - 1].ItuChannelIndex < chanIndices[2])
            {
                chanIndices[2] = closeGridCharData[closeGridCharData.Length - 1].ItuChannelIndex;
                chanIndices[1] = (chanIndices[0] + chanIndices[2]) / 2;
            }


            DsdbrChannelData[] dsdbrChansData = new DsdbrChannelData[3];
            for (int ii = 0; ii < chanIndices.Length; ii++)
            {
                foreach (DsdbrChannelData chanData in this.closeGridCharData)
                {
                    if (chanData.ItuChannelIndex == chanIndices[ii])
                    {
                        dsdbrChansData[ii] = chanData;
                        break;
                    }
                }
            }

            // Add config data to test module
            modRun.ConfigData.AddReference("DsdbrChannels", dsdbrChansData);
            // Run test module
            engine.RunModule("IlmzCrossCalibration");
        }
        #endregion

        #region End of Program

        public override void PostRun(ITestEnginePostRun engine, DUTObject dutObject,
            InstrumentCollection instrs, ChassisCollection chassis)
        {
            // Display temperature progress
            engine.GuiShow();
            engine.GuiToFront();

            // Cleanup & power off
            if (!engine.IsSimulation)
            {
                TestProgramCore.CloseInstrs_Wavemeter();

                if (progInfo.DsdbrInstrsUsed == DsdbrDriveInstruments.FCU2AsicInstrument)
                {
                    InstCommands.CloseInstToUnLoadDUT(engine, progInstrs.Fcu2AsicInstrsGroups.Fcu2Asic, progInfo.Instrs.Fcu2AsicInstrsGroups.FcuSource, progInfo.Instrs.Fcu2AsicInstrsGroups.AsicVccSource, progInfo.Instrs.Fcu2AsicInstrsGroups.AsicVeeSource, progInfo.Instrs.TecDsdbr, progInfo.Instrs.TecCase, 25);
                }

                #region Else(Not used)
                else if (progInfo.DsdbrInstrsUsed == DsdbrDriveInstruments.FCUInstruments)
                {
                    // modify for replacing FCU 2400 steven.cui
                    // Shutdown FCU
                    // Call methods on the FCU to switch off the laser.
                    progInfo.Instrs.Fcu.SoaCurrentSource.OutputEnabled = false;

                    progInfo.Instrs.Fcu.FullbandControlUnit.SetIVSource((float)0.0, IVSourceIndexNumbers.Rear);
                    progInfo.Instrs.Fcu.FullbandControlUnit.SetIVSource((float)0.0, IVSourceIndexNumbers.Phase);
                    progInfo.Instrs.Fcu.FullbandControlUnit.SetIVSource((float)0.0, IVSourceIndexNumbers.SOA);
                    progInfo.Instrs.Fcu.FullbandControlUnit.SetIVSource((float)0.0, IVSourceIndexNumbers.Gain);
                    progInfo.Instrs.Fcu.FullbandControlUnit.SetControlRegLaserPSUOn(false);
                    progInfo.Instrs.Fcu.FullbandControlUnit.SetControlRegLaserTecOn(false);

                    progInfo.Instrs.Fcu.FullbandControlUnit.SetControlRegLaserPSUOn(false);
                    TxCommand cmd1 = new TxCommand(0, 0, 0); // TunableModules command
                    cmd1 = progInfo.Instrs.Fcu.FullbandControlUnit.ReadTxCommand();
                    TxCommand cmd2 = new TxCommand(cmd1.Data1, cmd1.Data2,
                        (byte)(cmd1.Data3 | (byte)0x01)); // set bit0 of byte "3"
                    progInfo.Instrs.Fcu.FullbandControlUnit.SetTxCommand(cmd2); // Bit 'LSENABLE' is set for disabling it.
                    progInfo.Instrs.Fcu.FullbandControlUnit.OIFLaserEnable(false);
                    //progInfo.Instrs.Fcu.FCU_Source.OutputEnabled = false;

                    InstCommands.CloseTecsWithSafeTemperature(engine, progInfo.Instrs.TecDsdbr, progInfo.Instrs.TecCase, 25);
                }
                #endregion

                #region light_tower

                if (progInfo.lightTowerConfig.GetBoolParam("LightExist"))
                {
                    try
                    {

                        _serialPort.Close();
                        if (!_serialPort.IsOpen)
                        {
                            _serialPort.Open();
                        }

                        // Add the light tower color.
                        _serialPort.WriteLine(progInfo.lightTowerConfig.GetStringParam("LightYellow"));
                        _serialPort.Close();
                    }
                    catch { }
                }
                #endregion
            }
        }

        public override void DataWrite(ITestEngineDataWrite engine, DUTObject dutObject,
            DUTOutcome dutOutcome, TestData results, UserList userList)
        {
            DatumList traceData = new DatumList();

            #region Fill in blank TC and CH results

            // Add 'missing data' for TC_* and CH_* parameters
            foreach (ParamLimit paramLimit in progInfo.MainSpec)
            {
                if (paramLimit.ExternalName.StartsWith("TC_") || paramLimit.ExternalName.StartsWith("CH_"))
                {
                    if (!traceData.IsPresent(paramLimit.ExternalName))
                    {
                        Datum dummyValue = null;

                        if (paramLimit.ExternalName.Contains("TC_MZ_IMB_"))
                        {
                            if (ParamManager.Conditions.IsThermalPhase)
                            {
                                dummyValue = new DatumDouble(paramLimit.ExternalName, Convert.ToDouble(paramLimit.HighLimit.ValueToString()));
                            }
                            else
                            {
                                dummyValue = new DatumDouble(paramLimit.ExternalName, Convert.ToDouble(paramLimit.LowLimit.ValueToString()));
                            }
                        }
                        else
                        {
                            switch (paramLimit.ParamType)
                            {
                                case DatumType.Double:
                                    dummyValue = new DatumDouble(paramLimit.ExternalName, Convert.ToDouble(paramLimit.HighLimit.ValueToString()));
                                    break;
                                case DatumType.Sint32:
                                    dummyValue = new DatumSint32(paramLimit.ExternalName, Convert.ToInt32(paramLimit.HighLimit.ValueToString()));
                                    break;
                                case DatumType.StringType:
                                    dummyValue = new DatumString(paramLimit.ExternalName, "MISSING");
                                    break;
                                case DatumType.Uint32:
                                    dummyValue = new DatumUint32(paramLimit.ExternalName, Convert.ToUInt32(paramLimit.HighLimit.ValueToString()));
                                    break;
                            }
                        }

                        if (dummyValue != null)
                        {
                            traceData.Add(dummyValue);
                        }
                    }
                }
            }
            #endregion

            double testTime = Math.Round(engine.GetTestTime() / 60, 2);
            double processTime = Math.Round(engine.TestProcessTime / 60, 2);
            double laborTime = testTime - processTime;

            // Add time date
            traceData.AddString("TIME_DATE", TestTimeStamp_Start);
            traceData.AddDouble("TEST_TIME", testTime);
            traceData.AddDouble("LABOUR_TIME", laborTime);
            // Add more detail into the main specification
            traceData.AddString("FACTORY_WORKS_FAILMODE", "");
            //traceData.AddString("FACTORY_WORKS_LOTID", dutObject.BatchID);

            DatumListAddOrUpdate(ref traceData, "FACTORY_WORKS_LOTID", dutObject.BatchID);



            traceData.AddString("FACTORY_WORKS_PARTID", dutObject.PartCode);
            traceData.AddString("PRODUCT_CODE", dutObject.PartCode);

            //tcTraceData.AddString("SOFTWARE_ID", dutObject.ProgramPluginName + dutObject.ProgramPluginVersion);
            traceData.AddString("SOFTWARE_ID", dutObject.ProgramPluginName +
                " SW_version: " + dutObject.ProgramPluginVersion +
                System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString());
            traceData.AddString("OPERATOR_ID", userList.InitialUser);
            if (!isgrr)
            {
                traceData.AddString("COMMENTS", engine.GetProgramRunComments());
            }
            else
            {
                traceData.AddString("COMMENTS", "GRR TEST"+engine.GetProgramRunComments());
            }
            if ((this.errorInformation != null) &&
                (this.errorInformation.Length != 0))
            {
                if (this.errorInformation.Length > 80)
                    this.errorInformation = this.errorInformation.Substring(0, 80);
                traceData.AddString("FAIL_ABORT_REASON", this.errorInformation);
            }
            else
            {
                traceData.AddString("FAIL_ABORT_REASON", engine.ProgramRunStatus.ToString());
            }

            traceData.AddString("LASER_CHIP_ID", finalTestResult.ReadString("CHIP_ID"));
            traceData.AddString("LASER_WAFER_ID", finalTestResult.ReadString("WAFER_ID"));
            traceData.AddDouble("MZ_MAX_V_BIAS", finalTestResult.ReadDouble("MZ_MAX_V_BIAS"));
            traceData.AddDouble("REF_FIBRE_POWER", finalTestResult.ReadDouble("REF_FIBRE_POWER"));
            traceData.AddDouble("TEC_JIG_CL_TEMP", finalTestResult.ReadDouble("TEC_JIG_CL_TEMP"));
            traceData.AddString("SWAPWIREBOND", ParamManager.Conditions.IsSwapWirebond ? "1" : "0");
            // Write keys required for external data (example below for PCAS)            
            StringDictionary keys = new StringDictionary();
            // TODO: MUST Add real values below!
            keys.Add("SCHEMA", "HIBERDB");

			// chongjian.liang 2015.5.20
            /*if (dutObject.PartCode.ToUpper().Contains("8335"))
            {
                keys.Add("DEVICE_TYPE", "HITT_QUAL_PA008335");
            }
            else
            {
                keys.Add("DEVICE_TYPE", "HITT_QUAL_BETA");
            }*/

            keys.Add("DEVICE_TYPE", dutObject.PartCode);
            keys.Add("TEST_STAGE", dutObject.TestStage);
            keys.Add("SPECIFICATION", progInfo.MainSpec.Name);
            keys.Add("SERIAL_NO", dutObject.SerialNumber);
            // Tell Test Engine about it...
            engine.SetDataKeys(keys);

            // Add any other data required for external data (example below for PCAS)
            // !(beware, all these parameters MUST exist in the specification)!...

            traceData.AddSint32("NODE", dutObject.NodeID);
            traceData.AddString("EQUIP_ID", dutObject.EquipmentID);
            traceData.AddString("TEST_STATUS", engine.GetOverallPassFail().ToString());
            //traceData.AddString("TESTENGINE_STATUS", engine.GetProgramRunStatus().ToString());            

            this.failModeCheck.CheckMainSpec(engine, dutObject, this.MainSpec, this.CommonTestConfig.PCASFailMode_FilePath, ref traceData);

            // pick the specification to add this data to...
            engine.SetTraceData(progInfo.MainSpec.Name, traceData);
        }

        #endregion

        #region Private Helper Functions

        /// <summary>
        /// Get the times of retest for the current device
        /// </summary>
        /// <param name="testEngine"></param>
        /// <param name="dutObject"></param>
        private void SetRetestCount(ITestEngineRun testEngine, DUTObject dutObject)
        {
            IDataRead dataRead = testEngine.GetDataReader("PCAS_SHENZHEN");
            int retestCount = 0;
            try
            {
                //get parameter "retest"
                StringDictionary finalKeys = new StringDictionary();
                finalKeys.Add("SCHEMA", "hiberdb");
                finalKeys.Add("DEVICE_TYPE", dutObject.PartCode);
                finalKeys.Add("TEST_STAGE", dutObject.TestStage);
                finalKeys.Add("SERIAL_NO", dutObject.SerialNumber);
                DatumList finalData = dataRead.GetLatestResults(finalKeys, false);
                if (finalData == null
                    || finalData.Count == 0)
                {
                    retestCount = 0;
                }
                else
                {
                    if (finalData.IsPresent("RETEST"))
                    {
                        retestCount = int.Parse(finalData.ReadString("RETEST"));
                    }
                    retestCount++;
                }

            }
            catch
            {
            }

            // Set retest count
            DatumList dl = new DatumList();
            dl.AddString("RETEST", retestCount.ToString());
            progInfo.MainSpec.SetTraceData(dl);
        }

        /// <summary>
        /// Retrieves datums from config file via the ConfigAccessor.
        /// </summary>
        /// <param name="modRun">The ModuleRun to add the items to.</param>
        /// <param name="datumsRequired">The datum names in this datumlist specify the names of the datums to be retrieved and added to the ModuleRun</param>
        private void ExtractAndAddDatums(ModuleRun modRun, DatumList datumsRequired)
        {
            foreach (Datum datum in datumsRequired)
            {
                switch (datum.Type)
                {
                    case DatumType.Double:
                        {
                            modRun.ConfigData.Add(new DatumDouble(datum.Name, this.progInfo.TestParamsConfig.GetDoubleParam(datum.Name)));
                        }
                        break;

                    case DatumType.StringType:
                        {
                            modRun.ConfigData.Add(new DatumString(datum.Name, this.progInfo.TestParamsConfig.GetStringParam(datum.Name)));
                        }
                        break;

                    case DatumType.Uint32:
                        {
                            modRun.ConfigData.Add(new DatumUint32(datum.Name, this.progInfo.TestParamsConfig.GetIntParam(datum.Name)));
                        }
                        break;

                    case DatumType.Sint32:
                        {
                            modRun.ConfigData.Add(new DatumSint32(datum.Name, this.progInfo.TestParamsConfig.GetIntParam(datum.Name)));
                        }
                        break;

                    case DatumType.BoolType:
                        {
                            modRun.ConfigData.Add(new DatumBool(datum.Name, this.progInfo.TestParamsConfig.GetBoolParam(datum.Name)));
                        }
                        break;

                    default:
                        {
                            throw new ArgumentException("Invalid DatumType '" + datum.Type + "' of '" + datum.Name + "'");
                        }
                }
            }
        }

        /// <summary>
        /// Retrieve all required channels' DSDBR settings from "CHANNEL_PASS_FILE" @ final stage        
        /// it will retrieve channels' data with the first channel in 'QualTestLowChannelList',
        /// middle channel in 'QualTestMidChannelList' and the last channel in 'QualTestHighChannelList' 
        /// </summary>
        /// <returns>  DSDBR Settings for ITU channels and some reference data
        /// if all channel data could be retrieved, 
        /// the sequence should be as follow:
        /// 0: low
        /// 1: mid
        /// 2: high
        /// </returns>
        protected IlmzQualChanPreviousData[] GetChanDsdbrData(ITestEngineInit engine)
        {
            List<int> lowChannelSelectedList = new List<int>();
            List<int> midChannelSelectedList = new List<int>();
            List<int> highChannelSelectedList = new List<int>();

            qualTestHighChanList = new List<IlmzQualChanPreviousData>();
            qualTestLowChanList = new List<IlmzQualChanPreviousData>();
            qualTestMidChanList = new List<IlmzQualChanPreviousData>();
            #region Get channels to chek from config file
            string strTemp = progInfo.TestParamsConfig.GetStringParam("QualTestLowChannelList");
            if (strTemp.Trim().Length > 0)
            {
                string[] chansStr = strTemp.Split(',');

                foreach (string chanStr in chansStr)
                    lowChannelSelectedList.Add(Convert.ToInt16(chanStr));
                lowChannelSelectedList.Sort();
            }
            strTemp = progInfo.TestParamsConfig.GetStringParam("QualTestMidChannelList");
            if (strTemp.Trim().Length > 0)
            {
                string[] chansStr = strTemp.Split(',');

                foreach (string chanStr in chansStr)
                    midChannelSelectedList.Add(Convert.ToInt16(chanStr));
                midChannelSelectedList.Sort();
            }

            string partcode = this.finalTestResult.ReadString("PRODUCT_CODE");

            strTemp = progInfo.TestParamsConfig.GetStringParam("QualTestHighChannelList");

            if (strTemp.Trim().Length > 0)
            {
                string[] chansStr = strTemp.Split(',');

                foreach (string chanStr in chansStr)
                    highChannelSelectedList.Add(Convert.ToInt16(chanStr));
                highChannelSelectedList.Sort();
            }
            #endregion
            List<string[]> lines;
            // using statement will auto-shutdown the file even in presence of exceptions

            using (CsvReader csvReader = new CsvReader())
            {
                lines = csvReader.ReadFile(this.channelPassFile);
            }

            if (lines.Count < 4)
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "No channel data recorded in 'CHANNEL_PASS_FILE' From previous stage", FailModeCategory_Enum.OP);
                
                return null;
            }
            // create a new list with appropriate capacity
            List<IlmzQualChanPreviousData> qualChanRefDataList = new List<IlmzQualChanPreviousData>();
            // go through the lines of the file - zero based, so therefore starting at the 4nd line:
            // skipping the header line , low limit & high limit
            for (int lineNbr = 3; lineNbr < lines.Count; lineNbr++)
            {
                string[] line = lines[lineNbr];
                // skip any empty lines - as detected by lack of commas
                if (line.Length <= 1) continue;
                // get one channel's info

                int chanIndx = int.Parse(line[(int)EnumTosaParam.ItuChannelIndex]);
                // if select pass channel to test Qual Test but this chan is not 
                // contains in one of low, mid & high channel list, 
                // it has no neccesary to process with
                if ((lowChannelSelectedList.Count > 0 && !lowChannelSelectedList.Contains(chanIndx)) &&
                    (midChannelSelectedList.Count > 0 && !midChannelSelectedList.Contains(chanIndx)) &&
                    (highChannelSelectedList.Count > 0 && !highChannelSelectedList.Contains(chanIndx)))
                    continue;

                IlmzQualChanPreviousData qualChanRefData = new IlmzQualChanPreviousData();
                // Channel character data from previous stage to make reference to
                IlmzQualChanRefData chanRefData = new IlmzQualChanRefData();
                DsdbrChannelData ituDsdbrData = new DsdbrChannelData();
                DsdbrChannelSetup dsdbrSetup = new DsdbrChannelSetup();

                #region Dsdbr datas
                //ITU Channel Number,                
                ituDsdbrData.ItuChannelIndex = chanIndx;
                //ITU Channel Frequency,
                ituDsdbrData.ItuFreq_GHz = double.Parse(line[(int)EnumTosaParam.ItuFreq_GHz]);
                //Supermode Number,
                ituDsdbrData.Supermode = int.Parse(line[(int)EnumTosaParam.Supermode]);
                //Longitudinal Mode Number,
                ituDsdbrData.LongitudinalMode = int.Parse(line[(int)EnumTosaParam.LongitudinalMode]);
                //Index on Middle Line Of Frequency,
                ituDsdbrData.MiddleLineIndex = double.Parse(line[(int)EnumTosaParam.MiddleLineIndex]);
                //Phase [mA],
                dsdbrSetup.IPhase_mA = double.Parse(line[(int)EnumTosaParam.IPhaseITU_mA]);
                //Rear [mA],
                dsdbrSetup.IRear_mA = double.Parse(line[(int)EnumTosaParam.IRear_mA]);
                //Gain [mA],
                dsdbrSetup.IGain_mA = double.Parse(line[(int)EnumTosaParam.IGain_mA]);
                //SOA [mA],
                dsdbrSetup.ISoa_mA = double.Parse(line[(int)EnumTosaParam.ISoa_mA]);
                //Front Pair Number,
                dsdbrSetup.FrontPair = int.Parse(line[(int)EnumTosaParam.FrontSectionPair]);
                //Constant Front [mA],
                dsdbrSetup.IFsFirst_mA = double.Parse(line[(int)EnumTosaParam.IFsConst_mA]);
                //Non-constant Front [mA],
                dsdbrSetup.IFsSecond_mA = double.Parse(line[(int)EnumTosaParam.IFsNonConst_mA]);

                ////Frequency [GHz],
                ituDsdbrData.Freq_GHz = double.Parse(line[(int)EnumTosaParam.Freq_GHz]);
                ////Coarse Frequency [GHz],
                //chan.CoarseFreq_GHz = double.Parse(line[13]);
                ////Optical Power Ratio [0:1],
                //chan.OptPowerRatio = double.Parse(line[14]);
                ////Optical Power [mW],
                //chan.OptPower_mW = double.Parse(line[15]);
                //X coordinate of boundary above,
                ituDsdbrData.BoundAboveX = double.Parse(line[(int)EnumTosaParam.BoundAboveX]);
                //Y coordinate of boundary above,
                ituDsdbrData.BoundAboveY = double.Parse(line[(int)EnumTosaParam.BoundAboveY]);
                //X coordinate of boundary below,
                ituDsdbrData.BoundBelowX = double.Parse(line[(int)EnumTosaParam.BoundBelowX]);
                //Y coordinate of boundary below,
                ituDsdbrData.BoundBelowY = double.Parse(line[(int)EnumTosaParam.BoundBelowY]);
                //X coordinate of boundary to left,
                ituDsdbrData.BoundLeftX = double.Parse(line[(int)EnumTosaParam.BoundLeftX]);
                //Y coordinate of boundary to left,
                ituDsdbrData.BoundLeftY = double.Parse(line[(int)EnumTosaParam.BoundLeftY]);
                //X coordinate of boundary to right,
                ituDsdbrData.BoundRightX = double.Parse(line[(int)EnumTosaParam.BoundRightX]);
                //Y coordinate of boundary to right,
                ituDsdbrData.BoundRightY = double.Parse(line[(int)EnumTosaParam.BoundRightY]);

                dsdbrSetup.IRearSoa_mA = double.Parse(line[(int)EnumTosaParam.IRearSoa_mA]);
                #endregion

                // Get reference data 
                chanRefData.Freq_Ghz = double.Parse(line[(int)EnumTosaParam.Freq_GHz]);
                chanRefData.PhaseRatioSlopeEff = double.Parse(line[(int)EnumTosaParam.PhaseRatioSlopeEff]);
                chanRefData.Vcm_V = double.Parse(line[(int)EnumTosaParam.MzVcmCal_V]);
                chanRefData.FibrePwrPeak_dBm = double.Parse(line[(int)EnumTosaParam.FibrePwrPeak_dBm]);
                chanRefData.FibrePwrQuad_dBm = double.Parse(line[(int)EnumTosaParam.FibrePwrQuad_dBm]);
                chanRefData.FibrePwrTrough_dBm = chanRefData.FibrePwrPeak_dBm -
                    double.Parse(line[(int)EnumTosaParam.MzDcEr_dB]);
                chanRefData.IrxLock_mA = double.Parse(line[(int)EnumTosaParam.IrxLock_mA]);
                chanRefData.ItxLock_mA = double.Parse(line[(int)EnumTosaParam.ItxLock_mA]);
                chanRefData.LockerRatio = double.Parse(line[(int)EnumTosaParam.LockRatio]);
                chanRefData.TapPhotocurrentQuad_A = double.Parse(line[(int)EnumTosaParam.TapCompPhotoCurrentQuad_mA]) / 1000;
                chanRefData.MzLeftArmImb_mA = double.Parse(line[(int)EnumTosaParam.MzLeftArmImb_mA]);
                chanRefData.MzRightArmImb_mA = double.Parse(line[(int)EnumTosaParam.MzRightArmImb_mA]);
                // add setup data to the channel object (NB: struct here, so copies!)
                ituDsdbrData.Setup = dsdbrSetup;
                qualChanRefData.DsdbrData = ituDsdbrData;
                qualChanRefData.RefChanData = chanRefData;

                if (lowChannelSelectedList.Contains(chanIndx)) qualTestLowChanList.Add(qualChanRefData);
                if (midChannelSelectedList.Contains(chanIndx)) qualTestMidChanList.Add(qualChanRefData);
                if (highChannelSelectedList.Contains(chanIndx)) qualTestHighChanList.Add(qualChanRefData);

            }

            // sort the list by ItuChannelIndex            
            qualTestMidChanList.Sort();
            qualTestLowChanList.Sort();
            qualTestHighChanList.Sort();
            if (qualTestLowChanList.Count > 0) qualChanRefDataList.Add(qualTestLowChanList[0]);
            if (qualTestMidChanList.Count > 0 && !qualChanRefDataList.Contains(qualTestMidChanList[(int)((qualTestMidChanList.Count - 1) / 2)]))
                qualChanRefDataList.Add(qualTestMidChanList[(int)((qualTestMidChanList.Count - 1) / 2)]);
            if (qualTestHighChanList.Count > 0 && !qualChanRefDataList.Contains(qualTestHighChanList[qualTestHighChanList.Count - 1]))
                qualChanRefDataList.Add(qualTestHighChanList[qualTestHighChanList.Count - 1]);

            qualChanRefDataList.Sort();

            // return as a normal array
            return qualChanRefDataList.ToArray();
        }

        /// <summary>
        ///  Update the channels refernce data with values from previous stage's result
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        /// <param name="chanData"> the low, mid and high channel's final stage's channel data         
        /// the sequence should be as follow:
        /// 0: low
        /// 1: mid
        /// 2: high
        /// </param>
        private void GetChanPreviousData(ITestEngineInit engine,
            DUTObject dutObject, ref IlmzQualChanPreviousData[] chanData)
        {
            IDataRead dataRead = engine.GetDataReader("PCAS_SHENZHEN");
            StringDictionary prevStageKeys = new StringDictionary();
            prevStageKeys.Add("SCHEMA", ILMZSchema);
            prevStageKeys.Add("SERIAL_NO", dutObject.SerialNumber);
            prevStageKeys.Add("TEST_STAGE", progInfo.TestParamsConfig.GetStringParam("PreviousStageToRetrieveData"));

            DatumList prevStageData = dataRead.GetLatestResults(prevStageKeys, true);

            // If no data retrieved from previous stage, prompt to choose abort the test or not
            if (prevStageData == null
                || prevStageData.Count == 0)
            {
                ButtonId rsp = engine.ShowYesNoUserQuery("No test result available @" +
                    progInfo.TestParamsConfig.GetStringParam("PreviousStageToRetrieveData") +
                    "\n No reference data to calculate the parameters delta" +
                    "\n would you go to abort the test?");

                if (rsp == ButtonId.Yes) {
                    this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "No test result available @" + progInfo.TestParamsConfig.GetStringParam("PreviousStageToRetrieveData"), FailModeCategory_Enum.OP);

                    return;
                }
            }
            string[] strPCASSuffix = new string[] { "_CH_LOW", "_CH_MID", "_CH_HIGH" };

            for (int indx = 0; indx < 3; indx++)
            {
                chanData[indx].RefChanData.Freq_Ghz =
                    prevStageData.ReadDouble("OPTICAL_FREQ" + strPCASSuffix[indx]);
                chanData[indx].RefChanData.FibrePwrPeak_dBm =
                    prevStageData.ReadDouble("PEAK_POWER" + strPCASSuffix[indx]);
                chanData[indx].RefChanData.FibrePwrQuad_dBm =
                    prevStageData.ReadDouble("QUAD_POWER" + strPCASSuffix[indx]);
                chanData[indx].RefChanData.FibrePwrTrough_dBm =
                    prevStageData.ReadDouble("TROUGH_POWER" + strPCASSuffix[indx]);
                chanData[indx].RefChanData.IrxLock_mA =
                    prevStageData.ReadDouble("IRX_LOCK" + strPCASSuffix[indx]);
                chanData[indx].RefChanData.ItxLock_mA =
                    prevStageData.ReadDouble("ITX_LOCK" + strPCASSuffix[indx]);
                chanData[indx].RefChanData.LockerRatio =
                    prevStageData.ReadDouble("LOCK_RATIO" + strPCASSuffix[indx]);
                chanData[indx].RefChanData.PhaseRatioSlopeEff =
                    prevStageData.ReadDouble("PHASERT_SLP_EFF" + strPCASSuffix[indx]);
                chanData[indx].RefChanData.TapPhotocurrentQuad_A =
                    prevStageData.ReadDouble("I_CTAP_QUAD_OL" + strPCASSuffix[indx]) / 1000;
                chanData[indx].RefChanData.Vcm_V = prevStageData.ReadDouble("MZ_VCM_CAL" + strPCASSuffix[indx])
                    + progInfo.TestParamsConfig.GetDoubleParam("VcmAdjust2PreviousStage");
            }
        }

        // <summary>
        /// if no pcas result for final stage, get nessesary information by manual
        /// it is useful for Eng debug
        /// </summary>
        /// <param name="engine"></param>
        internal void ManualEntryFinalTestReults(ITestEngineInit engine)
        {
            engine.GuiShow();
            engine.GuiToFront();
            engine.SendToGui(new GuiMsgs.ilmzGetFinalDataRqst());

            engine.SendStatusMsg("Construct Engineering final data");
            GuiMsgs.ilmzFinalDataRsps resp = (GuiMsgs.ilmzFinalDataRsps)engine.ReceiveFromGui().Payload;

            this.finalTestResult = resp.finalTestData;
            if (this.finalTestResult == null)
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "No Hitt final test Data , please test this device at hitt final stage first!", FailModeCategory_Enum.OP);
            }
            try
            {
                string MAP_TC_NUM_CHAN_REQUIRED = this.finalTestResult.ReadSint32("TC_NUM_CHAN_REQUIRED").ToString();
                string MAP_TC_OPTICAL_FREQ_START = this.finalTestResult.ReadDouble("TC_OPTICAL_FREQ_START").ToString();//Jack.Zhang
                //string MAP_TC_OPTICAL_FREQ_START = this.mapTestResult.ReadDouble("TC_OPTICAL_FREQ_START").ToString();
            }
            catch
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "Can't load this 2 parameters:'TC_NUM_CHAN_REQUIRED' & 'TC_OPTICAL_FREQ_START'.", FailModeCategory_Enum.SW);
            }
            //if (!this.finalTestResult.ReadString("TEST_STATUS").ToLower().Contains("pass"))
            //{
            //    string errorDescription = "No passed  Hitt final stage data in PCAS,\n" +
            //        "Please test this device at Hitt final stage first!";
            //    engine.ShowContinueUserQuery(errorDescription);
            //    engine.ErrorInProgram(errorDescription);
            //}

            // Get data from ITU operating file - chongjian.liang 2015.7.13
            string channelCharFileName = this.finalTestResult.ReadFileLinkFullPath("CG_CHAR_ITU_FILE");
            //string channelCharFileName = this.finalTestResult.ReadFileLinkFullPath("CG_CHAR_ITU_FILE");
            this.closeGridCharData = Bookham.Toolkit.CloseGrid.CsvDataLookup.GetItuOperatingPoints(channelCharFileName);


        }

        /// <summary>
        /// Copy data from ilmz final stage.
        /// </summary>
        /// <param name="finalData"></param>
        /// <param name="engine"></param>
        private void CopyDataFromMap(DatumList MapData, ITestEngineRun engine)
        {
            try
            {
                DatumList traceDataMapStage = new DatumList();

                // Feed forward data
                //traceDataFinalStage.Add(finalData.GetDatum("ETALON1_FS_B"));
                //traceDataFinalStage.Add(finalData.GetDatum("ETALON1_FS_PAIR"));
                //traceDataFinalStage.Add(finalData.GetDatum("ETALON1_PHASE"));
                //traceDataFinalStage.Add(finalData.GetDatum("ETALON1_REAR"));
                //traceDataFinalStage.Add(finalData.GetDatum("I_ETALON_FS_A"));
                //traceDataFinalStage.Add(finalData.GetDatum("I_ETALON_GAIN"));
                //traceDataFinalStage.Add(finalData.GetDatum("I_ETALON_SOA"));

                //traceDataFinalStage.Add(finalData.GetDatum("LASER_RTH"));
                //traceDataFinalStage.Add(finalData.GetDatum("ETALON1_FREQ"));

                //traceDataFinalStage.Add(finalData.GetDatum("LASER_CHIP_ID"));


                //// IlmzInitialise
                //traceDataFinalStage.Add(finalData.GetDatum("CG_DUT_FILE_TIMESTAMP"));
                //traceDataFinalStage.Add(finalData.GetDatum("REGISTRY_FILE"));



                //// TcmzInitMzSweep
                //traceDataFinalStage.Add(finalData.GetDatum("MZ_MAX_V_BIAS"));
                ////traceDataMapStage.Add(mapData.GetDatum("REF_LV_FILE"));
                traceDataMapStage.Add(MapData.GetDatum("REF_MZ_VON_LEFT"));
                traceDataMapStage.Add(MapData.GetDatum("REF_MZ_VOFF_LEFT"));
                traceDataMapStage.Add(MapData.GetDatum("REF_MZ_VON_RIGHT"));
                traceDataMapStage.Add(MapData.GetDatum("REF_MZ_VOFF_RIGHT"));
                //traceDataFinalStage.Add(finalData.GetDatum("REF_FIBRE_POWER"));
                traceDataMapStage.Add(MapData.GetDatum("REF_MZ_IMB_LEFT"));
                traceDataMapStage.Add(MapData.GetDatum("REF_MZ_IMB_RIGHT"));

                //traceDataFinalStage.Add(finalData.GetDatum("REF_MZ_SUM"));
                //traceDataFinalStage.Add(finalData.GetDatum("REF_MZ_RATIO"));
                traceDataMapStage.Add(MapData.GetDatum("REF_FREQ"));

                ////CloseGridOverallMap Module
                traceDataMapStage.Add(MapData.GetDatum("NUM_SM"));
                //traceDataFinalStage.Add(finalData.GetDatum("CG_SM_LINES_FILE"));
                //traceDataFinalStage.Add(finalData.GetDatum("CG_PRATIO_WL_FILE"));
                //traceDataFinalStage.Add(finalData.GetDatum("CG_QAMETRICS_FILE"));
                //traceDataFinalStage.Add(finalData.GetDatum("MATRIX_PRATIO_SUMMARY_MAP"));


                ////CloseGridCharacterise Module

                //traceDataFinalStage.Add(finalData.GetDatum("CG_ITUEST_POINTS_FILE"));
                //traceDataFinalStage.Add(finalData.GetDatum("ITU_OP_EST_COUNT"));
                //traceDataFinalStage.Add(finalData.GetDatum("CHAR_ITU_OP_COUNT"));
                traceDataMapStage.Add(new DatumFileLink("CG_CHAR_ITU_FILE", this.itufile));

                // Set trace data
                progInfo.MainSpec.SetTraceData(traceDataMapStage);
            }
            catch (Exception e)
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "Copy map stage data error!" + e.Message, FailModeCategory_Enum.OP);
            }
        }

        #endregion
    }
}

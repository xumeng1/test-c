using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Bookham.TestEngine.Framework.InternalData;
using System.IO;

namespace Bookham.TestSolution.TestPrograms
{
    public partial class GetFinalTestData : UserControl
    {
        Prog_ILMZ_QualGui parent;
        public GetFinalTestData( Prog_ILMZ_QualGui parent)
        {
            this.parent = parent;
            InitializeComponent();
        }

        private void btnGetItuFile_Click(object sender, EventArgs e)
        {
            //openFileDialog1.Filter = "|.csv";
            openFileDialog1.Title = "Get Itu File";
            DialogResult res = openFileDialog1.ShowDialog(this);
            if (res == DialogResult.OK)
            {
                txtItuFileName.Text = openFileDialog1.FileName;
            }
        }
        private void btnGetChanPassFile_Click(object sender, EventArgs e)
        {
            //openFileDialog1.Filter = "|.csv";
            openFileDialog1.Title = "Get passed channel File";
            DialogResult res = openFileDialog1.ShowDialog(this);
            if (res == DialogResult.OK)
            {
                txtPassedChannelFileName.Text = openFileDialog1.FileName;
            }
        }
        private void btnSet_Click(object sender, EventArgs e)
        {
            int channels = int.Parse(txtChannels.Text);
            if (channels == 0)
            {
                MessageBox.Show(" Please re-entry the totals channels count", "Total channels can't be 0");
                return;
            }
            
            double startFreq = double.Parse(cbStartFreq.Text);
            if (startFreq < 191500)
            {
                MessageBox.Show("Please re-entry the start frequency ", "Frequency should start greater than 191500");
                return;
            }
            string ituFile = txtItuFileName.Text.Trim();
            if (!File.Exists(ituFile))
            {
                MessageBox.Show("ITU file doesn't exist at " + ituFile + " ,Plese re entry the CG Itu file", " Can't find file");
                return;
            }
            string channelFile = txtPassedChannelFileName.Text;
            if (!File.Exists(channelFile))
            {
                MessageBox.Show("Passed channel record file doesn't exist at " + channelFile + " ,Plese re entry the passed channel file", " Can't find file");
                return;
            }
            double vLeft = Convert.ToDouble(txtVleft.Text);
            double vRight = Convert.ToDouble(txtVright.Text);
            double iLeft = Convert.ToDouble(txtImbLeft .Text);
            double iRight = Convert.ToDouble(txtImbRight.Text);

            DatumList finalResult = new DatumList();
            finalResult.Add(new DatumString("TEST_STATUS", "Pass"));
            finalResult.Add(new DatumSint32("TC_NUM_CHAN_REQUIRED", channels));
            finalResult.Add(new DatumDouble("TC_OPTICAL_FREQ_START", startFreq));
            finalResult.Add(new DatumFileLink("CG_CHAR_ITU_FILE", ituFile));
            finalResult.Add(new DatumFileLink("CHANNEL_LASTOPTION_FILE", channelFile));          
            finalResult.Add(new DatumDouble("REF_MZ_VOFF_LEFT", 0));//Jack.Zhang
            finalResult.Add(new DatumDouble("REF_MZ_VOFF_RIGHT", 0));
            finalResult.Add(new DatumDouble("REF_MZ_VON_LEFT", vLeft));
            finalResult.Add(new DatumDouble("REF_MZ_VON_RIGHT", vRight));
            finalResult.Add(new DatumDouble("REF_MZ_IMB_LEFT", iLeft));
            finalResult.Add(new DatumDouble("REF_MZ_IMB_RIGHT", iRight));
            finalResult .Add (new DatumSint32("NUM_SM", 7));

            GuiMsgs.ilmzFinalDataRsps  rsp = new GuiMsgs.ilmzFinalDataRsps(finalResult);

            parent.SendToWorker(rsp);

            parent.CtrlFinished();
        }       
    }
}

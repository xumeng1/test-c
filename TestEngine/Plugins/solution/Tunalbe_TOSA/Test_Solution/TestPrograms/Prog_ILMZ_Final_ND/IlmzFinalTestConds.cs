using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestSolution.IlmzCommonData;
using Bookham.TestLibrary.Utilities;
using Bookham.TestEngine.Framework.Limits;

namespace Bookham.TestSolution.TestPrograms
{

    internal class IlmzFinalTestConds
    {
        internal const int MaxNbrSupermodes = 8;

        internal IlmzFinalTestConds(Specification spec)
        {
            // get test conditions
            string[] testCondNames = new string[]
            {
                "TC_CAL_OFFSET",
                "TC_GAIN_I_OVERALL_MAP",
                "TC_LASER_TEC_EOL_DELTA_T",
                "TC_LOCKER_V_BIAS",
                "TC_MZ_TEC_EOL_DELTA_T",
                "TC_MZ_VCM_CAL_LIMIT_MAX",
                "TC_MZ_VCM_CAL_LIMIT_MIN",
                // Maximum allowed DC Vpi
                "TC_MZ_VPI_CAL_LIMIT_MAX",
                // Minimum allowed DC Vpi;
                "TC_MZ_VPI_CAL_LIMIT_MIN",
                "TC_MZ_IMB_LEFT_LIMIT_MIN",
                "TC_MZ_IMB_LEFT_LIMIT_MAX",
                "TC_MZ_IMB_RIGHT_LIMIT_MIN",
                "TC_MZ_IMB_RIGHT_LIMIT_MAX",
                "TC_MZ_PEAK_V_LIMIT_MIN",
                "TC_MZ_PEAK_V_LIMIT_MAX",
                "TC_MZ_MINIMA_V_LIMIT_MIN",
                "TC_MZ_MINIMA_V_LIMIT_MAX",
                "TC_MZ_QUAD_V_LIMIT_MIN",
                "TC_MZ_QUAD_V_LIMIT_MAX",
                "TC_NUM_CHAN_REQUIRED",
                "TC_OPTICAL_FREQ_STEP",
                "TC_OPTICAL_FREQ_TOL",
                "TC_PHASE_I_OVERALL_MAP",
                "TC_SOA_I_OVERALL_MAP",
                "TC_OPTICAL_FREQ_START",
                "TC_T_CASE_HIGH",
                "TC_T_CASE_LOW",
                "TC_T_CASE_MID",
                "TC_FIBRE_TARGET_POWER",
                "TC_FIBRE_TARGET_POWER_TOL",
                "TC_FREQ_BAND", 
                "TC_PWR_CTRL_RANGE_MIN",
                "TC_PWR_CTRL_RANGE_MAX",
                "TC_AGE_COND_1",
                "TC_AGE_COND_2",
                "TC_REARSOA_I_OVERALL_MAP",
                "TC_REARSOA_MAX",
                "TC_REARSOA_MIN",
                "TC_TX_TARGET_CURRENT",
                "TC_DSDBR_TEMP"
            };
            DatumList testConds = SpecValuesToDatumList.GetTestConditions(spec, testCondNames);

            foreach (Datum d in testConds)
            {
                string datumName = d.Name;
                switch (datumName)
                {
                    case "TC_CAL_OFFSET":
                        this.CalOffset = ((DatumDouble)d).Value;
                        break;
                    case "TC_GAIN_I_OVERALL_MAP":
                        this.OverallMapIGain_mA = ((DatumDouble)d).Value;
                        break;
                    case "TC_LASER_TEC_EOL_DELTA_T":
                        this.LaserTecEolDeltaT_C = ((DatumDouble)d).Value;
                        break;
                    case "TC_LOCKER_V_BIAS":
                        this.LockerBias_V = ((DatumDouble)d).Value;
                        break;
                    case "TC_MZ_TEC_EOL_DELTA_T":
                        this.MzTecEolDeltaT_C = ((DatumDouble)d).Value;
                        break;
                    case "TC_MZ_VCM_CAL_LIMIT_MAX":
                        this.MzVcmCalLimitMax_V = ((DatumDouble)d).Value;
                        break;
                    case "TC_MZ_VCM_CAL_LIMIT_MIN":
                        this.MzVcmCalLimitMin_V = ((DatumDouble)d).Value;
                        break;
                    case "TC_MZ_VPI_CAL_LIMIT_MAX":
                        this.MzVpiCalLimitMax_V = ((DatumDouble)d).Value;
                        break;
                    case "TC_MZ_VPI_CAL_LIMIT_MIN":
                        this.MzVpiCalLimitMin_V = ((DatumDouble)d).Value;
                        break;
                    case "TC_MZ_IMB_LEFT_LIMIT_MIN":
                        this.MzImbLeftLimitMin_mA = ((DatumDouble)d).Value;
                        break;
                    case "TC_MZ_IMB_LEFT_LIMIT_MAX":
                        this.MzImbLeftLimitMax_mA = ((DatumDouble)d).Value;
                        break;
                    case "TC_MZ_IMB_RIGHT_LIMIT_MIN":
                        this.MzImbRightLimitMin_mA = ((DatumDouble)d).Value;
                        break;
                    case "TC_MZ_IMB_RIGHT_LIMIT_MAX":
                        this.MzImbRightLimitMax_mA = ((DatumDouble)d).Value;
                        break;
                    case "TC_MZ_PEAK_V_LIMIT_MIN":
                        this.MzPeakVLimitMin_V = ((DatumDouble)d).Value;
                        break;
                    case "TC_MZ_PEAK_V_LIMIT_MAX":
                        this.MzPeakVLimitMax_V = ((DatumDouble)d).Value;
                        break;
                    case "TC_MZ_MINIMA_V_LIMIT_MIN":
                        this.MzMinVLimitMin_V = ((DatumDouble)d).Value;
                        break;
                    case "TC_MZ_MINIMA_V_LIMIT_MAX":
                        this.MzMinVLimitMax_V = ((DatumDouble)d).Value;
                        break;
                    case "TC_MZ_QUAD_V_LIMIT_MIN":
                        this.MzQuadVLimitMin_V = ((DatumDouble)d).Value;
                        break;
                    case "TC_MZ_QUAD_V_LIMIT_MAX":
                        this.MzQuadVLimitMax_V = ((DatumDouble)d).Value;
                        break;
                    case "TC_NUM_CHAN_REQUIRED":
                        this.NbrChannels = ((DatumSint32)d).Value;
                        break;
                    case "TC_FREQ_BAND":
                        int temp = ((DatumSint32)d).Value;
                        this.FreqBand = (FreqBand)temp;
                        break;
                    case "TC_OPTICAL_FREQ_START":
                        this.ItuLowFreq_GHz = ((DatumDouble)d).Value;
                        break;
                    case "TC_OPTICAL_FREQ_STEP":
                        this.ItuSpacing_GHz = ((DatumDouble)d).Value;
                        break;
                    case "TC_SOA_I_OVERALL_MAP":
                        this.OverallMapISoa_mA = ((DatumDouble)d).Value;
                        break;
                    case "TC_PHASE_I_OVERALL_MAP":
                        this.OverallMapIPhase_mA = ((DatumDouble)d).Value;
                        break;
                    case "TC_FIBRE_TARGET_POWER":
                        this.TargetFibrePower_dBm = ((DatumDouble)d).Value;
                        break;
                    case "TC_FIBRE_TARGET_POWER_TOL":
                        this.TargetFibrePowerTol_dB =Math.Abs(((DatumDouble)d).Value);
                        break;
                    case "TC_OPTICAL_FREQ_TOL":
                        this.FreqTolerance_GHz = Math.Abs(((DatumDouble)d).Value);
                        break;
                    case "TC_T_CASE_MID":
                        this.MidTemp = ((DatumDouble)d).Value;
                        break;
                    case "TC_T_CASE_LOW":
                        this.LowTemp = ((DatumDouble)d).Value;
                        break;
                    case "TC_T_CASE_HIGH":
                        this.HighTemp = ((DatumDouble)d).Value;
                        break;
                    case "TC_PWR_CTRL_RANGE_MIN":
                        this.PwrControlRangeLow = ((DatumDouble)d).Value;
                        break;
                    case "TC_PWR_CTRL_RANGE_MAX":
                        this.PwrControlRangeHigh = ((DatumDouble)d).Value;
                        break;
                    case "TC_AGE_COND_1":
                        this.TraceToneTestAgeCond1_dB = ((DatumDouble)d).Value;
                        break;
                    case "TC_AGE_COND_2":
                        this.TraceToneTestAgeCond2_dB = ((DatumDouble)d).Value;
                        break;
                    case "TC_REARSOA_I_OVERALL_MAP":
                        this.RearsoaMap = ((DatumDouble)d).Value;
                        break;
                    case "TC_REARSOA_MAX":
                        this.RearsoaMax = ((DatumDouble)d).Value;
                        break;
                    case "TC_REARSOA_MIN":
                        this.RearsoaMin = ((DatumDouble)d).Value;
                        break;
                    case "TC_TX_TARGET_CURRENT":
                        this.RxTargetCurrent = ((DatumDouble)d).Value;
                        break;
                    case "TC_DSDBR_TEMP":
                        this.DSDBR_TEMP_C = ((DatumDouble)d).Value;
                        break;
                }
            }

            // Add high frequency
            this.ItuHighFreq_GHz = this.ItuLowFreq_GHz + ItuSpacing_GHz * (NbrChannels - 1);

            // Get the low/high limit for MzDcVpiSlope
            ParamLimit paramLimit = spec.GetParamLimit("TC_MZ_DC_VPI_SLOPE");
            this.MzDcVpiSlopeLimitMin = ((DatumDouble)(paramLimit.LowLimit)).Value;
            this.MzDcVpiSlopeLimitMax = ((DatumDouble)(paramLimit.HighLimit)).Value;

            // Get the low/high limit for LockerSlopeEffAbs
            paramLimit = spec.GetParamLimit("TC_NORM_SLOPE_ABS_SELECTED");
            this.LockerSlopeEffAbsLimitMin = ((DatumDouble)(paramLimit.LowLimit)).Value;
            this.LockerSlopeEffAbsLimitMax = ((DatumDouble)(paramLimit.HighLimit)).Value;

            paramLimit = spec.GetParamLimit("CH_REARSOA_I");
            this.RearSOAMin = ((DatumDouble)(paramLimit.LowLimit)).Value;
            this.RearSOAMax = ((DatumDouble)(paramLimit.HighLimit)).Value;

            paramLimit = spec.GetParamLimit("CH_ITX_LOCK");
            this.ItxMAXValue = ((DatumDouble)(paramLimit.HighLimit)).Value;

            this.ItxMINValue = ((DatumDouble)(paramLimit.LowLimit)).Value;

            paramLimit = spec.GetParamLimit("CH_IRX_LOCK");
            this.IrxMAXValue = ((DatumDouble)(paramLimit.HighLimit)).Value;
            this.IrxMINValue = ((DatumDouble)(paramLimit.LowLimit)).Value;
        }

        internal double CalOffset;
        internal double OverallMapIGain_mA;
        internal double LaserTecEolDeltaT_C;
        internal double LockerBias_V;
        internal double MzTecEolDeltaT_C;
        internal double MzVcmCalLimitMax_V;
        internal double MzVcmCalLimitMin_V;
        internal double MzVpiCalLimitMax_V;
        internal double MzVpiCalLimitMin_V;
        internal double MzImbLeftLimitMin_mA;
        internal double MzImbLeftLimitMax_mA;
        internal double MzImbRightLimitMin_mA;
        internal double MzImbRightLimitMax_mA;
        internal double MzPeakVLimitMin_V;
        internal double MzPeakVLimitMax_V;
        internal double MzMinVLimitMin_V;
        internal double MzMinVLimitMax_V;
        internal double MzQuadVLimitMin_V;
        internal double MzQuadVLimitMax_V;
        internal int NbrChannels;
        internal FreqBand FreqBand;
        internal double ItuLowFreq_GHz;
        internal double ItuHighFreq_GHz;
        internal double ItuSpacing_GHz;
        internal double OverallMapISoa_mA;
        internal double OverallMapIPhase_mA;
        internal double TargetFibrePower_dBm;
        internal double TargetFibrePowerTol_dB;
        internal double FreqTolerance_GHz;
        internal double MidTemp;
        internal double LowTemp;
        internal double HighTemp;
        internal double PwrControlRangeLow;
        internal double PwrControlRangeHigh;
        internal double TraceToneTestAgeCond1_dB;
        internal double TraceToneTestAgeCond2_dB;
        internal double MzDcVpiSlopeLimitMax;
        internal double MzDcVpiSlopeLimitMin;
        internal double LockerSlopeEffAbsLimitMax;
        internal double LockerSlopeEffAbsLimitMin;
        internal double RearsoaMap;
        internal double RearsoaMax;
        internal double RearsoaMin;
        internal double RxTargetCurrent;
        internal double DSDBR_TEMP_C;
        internal double RearSOAMin;
        internal double RearSOAMax;


        internal double ItxMAXValue;
        internal double ItxMINValue;
        internal double IrxMAXValue;
        internal double IrxMINValue;
    }
}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestPrograms
//
// Prog_ILMZ_Qual_ND.cs
//
// Author: alice.huang, 2010
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.ExternalData;
using Bookham.TestEngine.Framework.Limits;
using System.Collections.Specialized;
using Bookham.TestEngine.Framework.InternalData;

namespace Bookham.TestSolution.TestPrograms
{
    public class Prog_ILMZ_Qual_ND : ITestProgram
    {
        #region Private data
        /// <summary>
        /// Remember the specification name we are using in the program
        /// </summary>
        private string specName;
        #endregion

        #region ITestProgram Members
        public Type UserControl
        {
            get { return typeof(Prog_ILMZ_Qual_NDGui); }
        }
        #region Initialise phase
        public void InitCode(ITestEngineInit engine, DUTObject dutObject,
            InstrumentCollection instrs, ChassisCollection chassis)
        {
            // load specification
            SpecList specs = this.loadSpecs(engine);
            // declare it to Test Engine
            engine.SetSpecificationList(specs);
            // Get our specification object (so we can initialise modules with appropriate limits)
            Specification spec = specs[0];
            this.specName = spec.Name;
            // TODO: read previous data

            // TODO: initialise modules
        }

        private SpecList loadSpecs(ITestEngineInit engine)
        {
            // TODO: Replace with the real specification that is required, from real location!
            // NB: This reads PCAS limits from a CSV file on the hard disk. If you want to get
            // limits from PCAS server, need to change the GetLimitReader call to point to a
            // PcasLimitsRead plugin (this is normally the default plugin), so:
            //
            // ILimitRead limitReader = engine.GetLimitReader();
            //
            // Also, will need to specify more keys, SCHEMA, DEVICE_TYPE, TEST_STAGE etc...

            ILimitRead limitReader = engine.GetLimitReader("PCAS_FILE");
            StringDictionary keys = new StringDictionary();
            keys.Add("Filename", "./Configuration/Training/limits.csv");

            SpecList specList = limitReader.GetLimit(keys);
            return specList;
        }
        /// <summary>
        /// Initialise instruments
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="instrs"></param>   
        /// <param name="chassis"></param>
        protected override void initInstrs(ITestEngineInit engine, InstrumentCollection instrs, ChassisCollection chassis)
        {
            //add for avoidig instrument logging error
            engine.SendStatusMsg("Initialise Instruments");
            ProgIlmzFinalInstruments progInstrs = new ProgIlmzFinalInstruments();
            progInfo.Instrs = progInstrs;

            //foreach (Chassis var in chassis.Values) var.EnableLogging = false;
            //foreach (Instrument var in instrs.Values) var.EnableLogging = false;

            // Initialise DSDBR drive instruments
            switch (progInfo.DsdbrInstrsUsed)
            {
                case DsdbrDriveInstruments.FCUInstruments:
                    // FCU
                    progInstrs.Fcu = new FCUInstruments();
                    progInstrs.Fcu.FullbandControlUnit = (Instr_FCU)instrs["FullbandControlUnit"];
                    progInstrs.Fcu.SoaCurrentSource =
                                        (InstType_ElectricalSource)instrs["SoaCurrentSource"];
                    // modify for replacing FCU 2400 steven.cui
                    //progInstrs.Fcu.FCU_Source = (InstType_ElectricalSource)instrs["FCU_Source"];
                    break;

                // Alice.Huang add FCU2Asic function    2010-02-01
                case DsdbrDriveInstruments.FCU2AsicInstrument:
                    engine.SendStatusMsg("Configuring FCU2Asic instruments");
                    progInstrs.Fcu2AsicInstrsGroups = new FCU2AsicInstruments();
                    if (instrs.Contains("FCU_Source"))
                        progInstrs.Fcu2AsicInstrsGroups.FcuSource =
                                    (InstType_ElectricalSource)instrs["FCU_Source"];
                    else
                        progInstrs.Fcu2AsicInstrsGroups.FcuSource = null;

                    progInstrs.Fcu2AsicInstrsGroups.AsicVccSource =
                                        (InstType_ElectricalSource)instrs["ASIC_VccSource"];
                    progInstrs.Fcu2AsicInstrsGroups.AsicVeeSource =
                                        (InstType_ElectricalSource)instrs["ASIC_VeeSource"];
                    progInstrs.Fcu2AsicInstrsGroups.Fcu2Asic = (Inst_Fcu2Asic)instrs["Fcu2Asic"];
                    break;
                case DsdbrDriveInstruments.PXIInstruments:

                    // Alice.Huang    2010-04-20
                    // commented this part to remove CloseGrid OCX
                    // PXI
                    // Get instruments from CloseGrid
                    //Chassis_CloseGrid chassisCloseGrid = Chassis_CloseGrid.Singleton;//jack.Zhang
                    //progInstrs.Dsdbr = new DsdbrInstruments();//jack.Zhang
                    //progInstrs.Dsdbr.CurrentSources = chassisCloseGrid.CurrentSources;//jack.Zhang
                    //progInstrs.OpmCgDirect = chassisCloseGrid.OpticalPowerMeters[PowerMeterHead.Reference];//Jack.Zhang
                    //progInstrs.OpmCgFilter = chassisCloseGrid.OpticalPowerMeters[PowerMeterHead.Filter];
                    //// SOA current actuators            
                    //Inst_DigiIoComboSwitch soaRelays = new Inst_DigiIoComboSwitch("SoaRelays", @"Configuration\TOSA Final\SoaRelayConfig.xml",
                    //    instrs);//jack.Zhang
                    //progInstrs.DsdbrSoaRelays = soaRelays;//jack.Zhang
                    break;
            }

            // MZs
            progInstrs.Mz = new IlMzInstruments();
            bool inlineTapPresent = progInfo.TestParamsConfig.GetBoolParam("InlineTapEnabled");
            progInstrs.Mz.TapComplementary = (InstType_TriggeredElectricalSource)instrs["MzTapComp"];
            if (inlineTapPresent)
                progInstrs.Mz.TapInline = (InstType_TriggeredElectricalSource)instrs["MzTapInline"];
            progInstrs.Mz.LeftArmMod = (InstType_TriggeredElectricalSource)instrs["MzLeftMod"];
            progInstrs.Mz.RightArmMod = (InstType_TriggeredElectricalSource)instrs["MzRightMod"];

            // Alice.Huang 2010-02-01
            // if DSDBR is source by ASIC, the Imbs should be source by Asic, 
            // so no intstrument will be set 
            // else, get in-dependant sources for Imbs arm
            if (progInfo.DsdbrInstrsUsed != DsdbrDriveInstruments.FCU2AsicInstrument)
            {
                progInstrs.Mz.FCU2Asic = null;
                progInstrs.Mz.ImbsSourceType = IlMzInstruments.EnumSourceType.ElectricalSource;
                progInstrs.Mz.LeftArmImb = (InstType_TriggeredElectricalSource)instrs["MzLeftImb"];
                progInstrs.Mz.RightArmImb = (InstType_TriggeredElectricalSource)instrs["MzRightImb"];
            }
            else
            {
                progInstrs.Mz.LeftArmImb = null;
                progInstrs.Mz.RightArmImb = null;
                progInstrs.Mz.FCU2Asic = (Inst_Fcu2Asic)instrs["Fcu2Asic"];
                progInstrs.Mz.ImbsSourceType = IlMzInstruments.EnumSourceType.Asic;

            }
            progInstrs.Mz.PowerMeter = (IInstType_TriggeredOpticalPowerMeter)instrs["OpmMz"];
            // Alice.Huang   2010-04-21
            // set wavemeter in single measurement state to avoid error reading
            Inst_Ag86122x ag8612x = progInstrs.Mz.PowerMeter as Inst_Ag86122x;
            if (ag8612x != null) ag8612x.ContinualMeasurement = false;
            // OpmRef
            progInstrs.OpmReference = (IInstType_OpticalPowerMeter)instrs["OpmRef"];

            // OSA
            progInstrs.Osa = (IInstType_OSA)instrs["Osa"];

            // WM
            progInstrs.Wavemeter = (IInstType_Wavemeter)instrs["Wavemeter"];

            // Switch path manager
            Util_SwitchPathManager switchPathManager = new Util_SwitchPathManager
                (@"Configuration\TOSA Final\switches.xml", instrs);
            progInstrs.SwitchPathManager = switchPathManager;

            // Switch between OSA and Opm
            Switch_Osa_MzOpm switchOsaMzOpm = new Switch_Osa_MzOpm(switchPathManager);
            progInstrs.SwitchOsaMzOpm = switchOsaMzOpm;

            // TECs
            progInstrs.TecDsdbr = (IInstType_TecController)instrs["TecDsdbr"];
            ////progInstrs.TecMz = (IInstType_TecController)instrs["TecMz"];
            progInstrs.TecCase = (IInstType_TecController)instrs["TecCase"];

            // Config instruments if not simulation mode
            if (!engine.IsSimulation)
            {
                engine.SendStatusMsg("Configuring Instruments");
                // Configure FCU
                if (progInfo.DsdbrInstrsUsed == DsdbrDriveInstruments.FCUInstruments)
                    ConfigureFCUInstruments(progInstrs.Fcu);

                    // Alice.Huang   2010-02-01
                // add FCU2ASIC instrument initialise for TOSA Test
                else if (progInfo.DsdbrInstrsUsed == DsdbrDriveInstruments.FCU2AsicInstrument)
                    ConfigureFCU2AsicInstruments(progInstrs.Fcu2AsicInstrsGroups);

                // Configure TEC controllers
                ConfigureTecController(progInstrs.TecDsdbr, "TecDsdbr");
                //ConfigureTecController(progInstrs.TecMz, "TecMz");
                ConfigureTecController(progInstrs.TecCase, "TecCase");

                // Configure MZ bias sources
                ConfigureMzBiasSource(progInstrs.Mz.LeftArmMod, "MzLeftMod");
                ConfigureMzBiasSource(progInstrs.Mz.RightArmMod, "MzRightMod");

                // Alice.Huang 2010-02-01
                // if DSDBR is source by ASIC, the Imbs should be source by Asic, 
                // so no intstrument will be set and initialise 
                // else, initialise  in-dependant sources for Imbs arm
                if (progInfo.DsdbrInstrsUsed != DsdbrDriveInstruments.FCU2AsicInstrument)
                {
                    ConfigureMzBiasSource(progInstrs.Mz.LeftArmImb, "MzLeftImb");
                    ConfigureMzBiasSource(progInstrs.Mz.RightArmImb, "MzRightImb");
                }

                ConfigureMzBiasSource(progInstrs.Mz.TapComplementary, "MzTapComp");

                if (inlineTapPresent)
                    ConfigureMzBiasSource(progInstrs.Mz.TapInline, "MzTapInline");
                //alice : here add asa share option, and put it in a try-catch to insure unlock
                try
                {
                    bool lockflag;
                    int lockCount = 0;
                    do
                    {
                        lockflag = TcmzOSAshare.LockInstruments();
                        lockCount++;
                    }
                    while ((!lockflag) && (lockCount < 3));
                    if (!lockflag)
                    {
                        throw new InstrumentException("can't lock instrument" +
                            "\nplease check if shared instrument used by other test kit without release" +
                            " or instrument lock file is exist in the setting path");
                    }
                    progInstrs.Osa.SetDefaultState();
                    progInstrs.Osa.ReferenceLevel_dBm = 10.0;
                    progInstrs.Osa.WavelengthStart_nm = 1520.0;
                    progInstrs.Osa.WavelengthStop_nm = 1620.0;
                    if (progInstrs.Osa.DriverName.Contains("Ag8614x"))
                    {
                        Inst_Ag8614xA ag8614xOsa = (Inst_Ag8614xA)progInstrs.Osa;
                        ag8614xOsa.Sensitivity_dBm = Convert.ToDouble(progInfo.TestParamsConfig.GetStringParam("Osa_Sensitivity"));
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
                finally
                {
                    if (TcmzOSAshare.OsaShareFlag) TcmzOSAshare.UnlockInstruments();
                }
                // Configure power meter
                progInstrs.OpmReference.SetDefaultState();
                progInstrs.Mz.PowerMeter.SetDefaultState();

                // Configure switches
                progInstrs.SwitchOsaMzOpm.SetState(Switch_Osa_MzOpm.State.MzOpm);
            }
        }

        /// <summary>
        /// Configure a Device Temperature Control module
        /// </summary>
        private void DeviceTempControl_InitModule(ITestEngineInit engine, string moduleName,
            IInstType_SimpleTempControl tecController, string configPrefix)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun(moduleName, "SimpleTempControl", "");

            // Add instrument
            modRun.Instrs.Add("Controller", (Instrument)tecController);

            //load config
            double timeout_S = this.progInfo.TestParamsConfig.GetDoubleParam(configPrefix + "_Timeout_S");
            double stabTime_S = this.progInfo.TestParamsConfig.GetDoubleParam(configPrefix + "_StabiliseTime_S");
            int updateTime_mS = this.progInfo.TestParamsConfig.GetIntParam(string.Format("{0}_UpdateTime_mS", configPrefix));
            double sensorSetPoint_C = this.progInfo.TestParamsConfig.GetDoubleParam(string.Format("{0}_SensorTemperatureSetPoint_C", configPrefix));
            double tolerance_C = this.progInfo.TestParamsConfig.GetDoubleParam(string.Format("{0}_Tolerance_C", configPrefix));

            // Add config data
            DatumList tempConfig = new DatumList();
            tempConfig.AddDouble("datumMaxExpectedTimeForOperation_s", timeout_S);
            tempConfig.AddDouble("RqdStabilisationTime_s", stabTime_S);
            tempConfig.AddSint32("TempTimeBtwReadings_ms", updateTime_mS);
            tempConfig.AddDouble("SetPointTemperature_C", sensorSetPoint_C);
            tempConfig.AddDouble("TemperatureTolerance_C", tolerance_C);

            modRun.ConfigData.AddListItems(tempConfig);
        }

        /// <summary>
        /// Configure Case Temperature module
        /// </summary>
        private void CaseTempControl_InitModule(ITestEngineInit engine,
            string moduleName, Double currentTemp, Double tempSetPoint)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun(moduleName, "SimpleTempControl", "");

            // Add instrument
            modRun.Instrs.Add("Controller", (Instrument)progInfo.Instrs.TecCase);

            // Add config data
            TempTablePoint tempCal = progInfo.TempConfig.GetTemperatureCal(currentTemp, tempSetPoint)[0];
            DatumList tempConfig = new DatumList();
            int guiTimeOut_ms = this.progInfo.TestParamsConfig.GetIntParam("CaseTempGui_UpdateTime_mS");
            // Timeout = 2 * calibrated time, minimum of 60secs
            double timeout_s = Math.Max(tempCal.DelayTime_s * 15, 60);
            tempConfig.AddDouble("datumMaxExpectedTimeForOperation_s", timeout_s);
            // Stabilisation time = 0.5 * calibrated time
            tempConfig.AddDouble("RqdStabilisationTime_s", tempCal.DelayTime_s);
            tempConfig.AddSint32("TempTimeBtwReadings_ms", guiTimeOut_ms);
            // Actual temperature to set
            tempConfig.AddDouble("SetPointTemperature_C", tempSetPoint + tempCal.OffsetC);
            tempConfig.AddDouble("TemperatureTolerance_C", tempCal.Tolerance_degC);
            modRun.ConfigData.AddListItems(tempConfig);
        }
        /// <summary>
        /// Initialise IlmzPowerUp module
        /// </summary>
        /// <param name="engine"></param>
        private void IlmzPowerUp_InitModule(ITestEngineInit engine)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("IlmzPowerUp", "IlmzPowerUp", "");

            // Add instruments
            modRun.Instrs.Add("OpticalPowerMeter", (Instrument)progInfo.Instrs.Mz.PowerMeter);
            modRun.ConfigData.AddReference("MzInstruments", progInfo.Instrs.Mz);

            // Add config data
            modRun.ConfigData.AddDouble("MzTapBias_V", progInfo.TestParamsConfig.GetDoubleParam("MzTapBias_V"));
            // Alice.Huang    2010-04-22
            // this parameter has never been used in module
            //modRun.ConfigData.AddDouble("LockerBias_V", progInfo.TestParamsConfig.GetDoubleParam("LockerBias_V"));

            // Alice.Huang  2010-02-10: Remember to add currentsetting of TMMI(Rear Soa)
        }

        /// <summary>
        /// Initialise IlmzCrossCalibration module
        /// </summary>
        /// <param name="engine"></param>
        private void IlmzCrossCalibration_InitModule(ITestEngineInit engine)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("IlmzCrossCalibration", "TcmzCrossCalibration", "");
            modRun.ConfigData.AddDouble("FreqDivTolerance_GHz", Convert.ToDouble(
                progInfo.MainSpec.GetParamLimit("XCAL_CH_MID_MAP_GB").HighLimit.ValueToString()));
            // Add instruments

            // Add config data
            modRun.ConfigData.AddSint32("OpticalCal_delay_ms", 2000);

            // Tie up to limits
            modRun.Limits.AddParameter(progInfo.MainSpec, "XCAL_CH_LO_MAP_GB", "XCAL_CH_LO");
            modRun.Limits.AddParameter(progInfo.MainSpec, "XCAL_CH_MID_MAP_GB", "XCAL_CH_MID");
            modRun.Limits.AddParameter(progInfo.MainSpec, "XCAL_CH_HI_MAP_GB", "XCAL_CH_HI");
        }

        /// <summary>
        /// Initialise IlmzPowerHeadCal module
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutobject"></param>
        private void IlmzPowerHeadCal_InitModule(ITestEngineInit engine, DUTObject dutobject)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("IlmzPowerHeadCal", "TcmzPowerHeadCal", "");

            // Add instruments
            modRun.Instrs.Add("OpmReference", (Instrument)progInfo.Instrs.OpmReference);
            modRun.Instrs.Add("Wavemeter", (Instrument)progInfo.Instrs.Wavemeter);
            modRun.Instrs.Add("OpmMZ", (Instrument)progInfo.Instrs.Mz.PowerMeter);
            switch (progInfo.DsdbrInstrsUsed)
            {
                // added FCU2ASIC Option by Alice.Huang, for TOSA GB test     2010-02-01
                case DsdbrDriveInstruments.FCU2AsicInstrument:
                case DsdbrDriveInstruments.FCUInstruments:
                    break;
                case DsdbrDriveInstruments.PXIInstruments:
                    modRun.Instrs.Add("OpmCgDirect", (Instrument)progInfo.Instrs.OpmCgDirect);
                    modRun.Instrs.Add("OpmCgFilter", (Instrument)progInfo.Instrs.OpmCgFilter);
                    break;
            }

            // Add any config data here ...
            modRun.ConfigData.AddEnum("TestStage", progInfo.TestStage);
            modRun.ConfigData.AddSint32("OpticalCal_delay_ms", 2000);
            modRun.ConfigData.AddDouble("OpticalCal_MZ_Max", progInfo.TestParamsConfig.GetDoubleParam("OpticalCal_MZ_Max"));
            modRun.ConfigData.AddDouble("OpticalCal_MZ_Min", progInfo.TestParamsConfig.GetDoubleParam("OpticalCal_MZ_Min"));
            modRun.ConfigData.AddString("PlotFileDirectory", progInfo.TestParamsConfig.GetStringParam("DSDBRFileDirectory"));
            modRun.ConfigData.AddString("SN", dutobject.SerialNumber);
            switch (progInfo.DsdbrInstrsUsed)
            {
                // added FCU2ASIC Option by Alice.Huang, for TOSA GB test     2010-02-01
                case DsdbrDriveInstruments.FCU2AsicInstrument:

                case DsdbrDriveInstruments.FCUInstruments:
                    modRun.ConfigData.AddBool("IsCalPXIT", false);
                    break;
                case DsdbrDriveInstruments.PXIInstruments:
                    modRun.ConfigData.AddBool("IsCalPXIT", true);
                    modRun.ConfigData.AddDouble("OpticalCal_CgDirect_Max", progInfo.TestParamsConfig.GetDoubleParam("OpticalCal_CgDirect_Max"));
                    modRun.ConfigData.AddDouble("OpticalCal_CgDirect_Min", progInfo.TestParamsConfig.GetDoubleParam("OpticalCal_CgDirect_Min"));
                    break;
            }
        }

        #endregion
        public void Run(ITestEngineRun engine, DUTObject dutObject, 
            InstrumentCollection instrs, ChassisCollection chassis)
        {
            // TODO: run some modules			                      
        }

        public void PostRun(ITestEnginePostRun engine, DUTObject dutObject,
            InstrumentCollection instrs, ChassisCollection chassis)
        {
            // TODO: shutdown actions
        }

        public void DataWrite(ITestEngineDataWrite engine, DUTObject dutObject, 
            DUTOutcome dutOutcome, TestData results, UserList userList)
        {
            // Write keys required for external data (example below for PCAS)            
            StringDictionary keys = new StringDictionary();
            // TODO: MUST Add real values below!
            keys.Add("SCHEMA", "COC");
            keys.Add("DEVICE_TYPE", dutObject.PartCode);
            keys.Add("TEST_STAGE", dutObject.TestStage);
            keys.Add("SPECIFICATION", this.specName);
            keys.Add("SERIAL_NO", dutObject.SerialNumber);
            // Tell Test Engine about it...
            engine.SetDataKeys(keys);

            // Add any other data required for external data (example below for PCAS)
            // !(beware, all these parameters MUST exist in the specification)!...
            DatumList traceData = new DatumList();
            traceData.AddSint32("NODE", dutObject.NodeID);
            traceData.AddString("EQUIP_ID", dutObject.EquipmentID);
            traceData.AddString("TEST_STATUS", engine.GetOverallPassFail().ToString());
            traceData.AddString("TESTENGINE_STATUS", engine.GetProgramRunStatus().ToString());
            traceData.AddString("OPERATOR_ID", userList.UserListString);
            traceData.AddString("COMMENTS", engine.GetProgramRunComments());
            // pick the specification to add this data to...
            engine.SetTraceData(specName, traceData);
        }
        /// <summary>
        /// Initialise IlmzMzCharacterise module
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        private void IlmzMzCharacterise_InitModule(ITestEngineInit engine, DUTObject dutObject)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("IlmzMzCharacterise", "IlmzMzCharacterise_ND", "");

            // Add instruments
            modRun.ConfigData.AddReference("MzInstruments", progInfo.Instrs.Mz);

            TcmzMzSweep_Config tcmzConfig = new TcmzMzSweep_Config();
            DatumList datumsRequired = tcmzConfig.GetDatumList();
            ExtractAndAddDatums(modRun, datumsRequired);

            double mzBias1 = progInfo.TestParamsConfig.GetDoubleParam("MzFixedModBias_1_V");
            double mzBias2 = progInfo.TestParamsConfig.GetDoubleParam("MzFixedModBias_2_V");

            //START MODIFICATION, 2007.09.26, BY Ken.Wu ( TCMZ Vpi Correction )
            // Read values needed for TCMZ Vpi calculation from config file.
            double tuneISOAPowerSlope
                = progInfo.TestParamsConfig.GetDoubleParam(externalKeyTuneISOAPowerSlope);

            double tuneOpticalPowerToleranceInmW
                = progInfo.TestParamsConfig.GetDoubleParam(externalKeyTuneOpticalPowerToleranceInmW);

            double tuneOpticalPowerToleranceIndB
                = progInfo.TestParamsConfig.GetDoubleParam(externalKeyTuneOpticalPowerToleranceIndB);

            modRun.ConfigData.AddDoubleArray("MZFixedModBias_volt", new double[] { mzBias1, mzBias2 });
            // Alice.Huang   2010-04-27
            // add SOA Current value for convenience
            //modRun.ConfigData.AddDouble("SoaSetupCurrent_mA", 
            //                progInfo.TestParamsConfig.GetDoubleParam("MzChrSetupSoa_mA"));
            modRun.ConfigData.AddDouble("TuneISOAPowerSlope", tuneISOAPowerSlope);
            modRun.ConfigData.AddDouble("TuneOpticalPowerToleranceInmW", tuneOpticalPowerToleranceInmW);
            modRun.ConfigData.AddDouble("TargetFibrePower_dBm",
                            progInfo.TestConditions.TargetFibrePower_dBm);
            modRun.ConfigData.AddDouble("PowerlevelingOffset_dB",
                            progInfo.TestParamsConfig.GetDoubleParam("MzChrPwrlevelingOffset_dB"));
            //END MODIFICATION, 2007.09.26, By Ken.Wu

            modRun.ConfigData.AddSint32("NumberOfPoints", progInfo.TestParamsConfig.GetIntParam("MzSweepNumberOfPoints"));
            modRun.ConfigData.AddString("DutSerialNbr", dutObject.SerialNumber);
            modRun.ConfigData.AddReference("TestSpec", progInfo.MainSpec);
            //#warning Add MZTapBias_V to config class
            modRun.ConfigData.AddDouble("MZInlineTapBias_V", modRun.ConfigData.ReadDouble("MZTapBias_V"));
            // Limits
            modRun.Limits.AddParameter(progInfo.MainSpec, "MZ_INITIAL_CAL_SWEEP_FILE", "MzSweepDataZipFile");
            modRun.Limits.AddParameter(progInfo.MainSpec, "MZ_CAL_EST_FILE", "MzSweepDataResultsFile");
            modRun.Limits.AddParameter(progInfo.MainSpec, "MZ_CAL_EST_FLAG", "AllSweepsPassed");
        }

        #endregion
    }
}

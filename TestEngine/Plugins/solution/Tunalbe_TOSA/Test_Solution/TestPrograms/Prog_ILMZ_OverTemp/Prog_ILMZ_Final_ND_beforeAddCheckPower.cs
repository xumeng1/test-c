// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestPrograms
//
// Prog_TCMZ_Final.cs
//
// Author: paul.annetts, Mark Fullalove 2006
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.ExternalData;
using Bookham.TestEngine.Framework.Limits;
using System.Collections.Specialized;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestLibrary.Utilities;
using Bookham.TestSolution.IlmzCommonInstrs;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestSolution.IlmzCommonData;
using Bookham.TestSolution.ILMZFinalData;
using Bookham.TestSolution.IlmzCommonUtils;
using Bookham.TestSolution.TestModules;
using Bookham.TestLibrary.Instruments;
using Bookham.ToolKit.Mz;
using Bookham.TestEngine.Config;
using System.Collections;
using Bookham.TestLibrary.BlackBoxes;
using Bookham.Tcmz.Core;
using System.Threading;
using System.IO.Ports;
using Bookham.fcumapping.CommonData;
using Bookham.Solution.Config;
using Bookham.TestLibrary.Algorithms;
using Bookham.TestSolution.Instruments;

namespace Bookham.TestSolution.TestPrograms
{
    /// <summary>
    /// ILMZ Negtive Chirp chip test program
    /// </summary>
    public class Prog_ILMZ_Final_ND : TcmzProgram, ITestProgram
    {
        #region Private data
        /// <summary>
        /// Remember the specification name we are using in the program
        /// </summary>
        private Prog_ILMZ_FinalInfo progInfo;
        private DatumList _mapResult;
        private DatumList _cocResult;
        private DateTime testTime_End;
        private double labourTime = 0;
        private IlmzChannels IlmzItuChannels;
        private const string ILMZSchema = "hiberdb";

        private string errorInformation;

        private string ituOperationPointfile = "";
        private DsdbrChannelData[] closeGridCharData;

        private const string externalKeyTuneOpticalPowerToleranceInmW
            = "TC_FIBRE_TARGET_POWER_TOL_MID";
        private const string externalKeyTuneOpticalPowerToleranceIndB
            = "Tune_OpticalPowerTolerance_dB";
        private const string externalKeyTuneISOAPowerSlope
            = "Tune_IsoaPowerSlope";

        private int numberOfSupermodes;
        private bool doMapping = true;
        private double imb_left = 0;
        private double imb_right = 0;

        private double locker_pot_value = 0;

        //bool SelectTestFlag;
        //private List<SectionIVSweepData> SectionIVCurveData;
        internal SpecList SupermodeSpecs;

        private string MzFileDirectory;
        private string TestTimeStamp;
        private string smSpecName; // supermode spec name
        private double soa_powerLeveling=0;
        private int[] MILD_MULTIMODE_SM = { 1, 1, 1, 1, 1, 1, 1 };
        private string CG_ID = "CG no id";
        private string zipFilename;
        private int itu_op_count = 0;// used to store itu point number after characterise module, to determine test status for map stage
        private const string cg_setting_file=@"Configuration\TOSA Final\MapTest\CloseGridSettings.xml";
        private List<CDSDBRSuperMode> supermodes = new List<CDSDBRSuperMode>();
        private ConfigurationManager CGcfgReader = new ConfigurationManager(cg_setting_file);
        private DatumList tcTraceData = new DatumList();
        private string Nofilepath = @"Configuration\TOSA Final\MapTest\NoDataFile.txt";
        private ProgIlmzFinalInstruments progInstrs = null;

        private string[] Cg_Dut_File_TimeStamp = new string[7];
        private string[] Cg_Lm_Lines_Sm_File = new string[7];
        private string[] Cg_PassFail_Sm_File = new string[7];
        private string[] Cg_Qametrics_Sm_File = new string[7];
        private double[] Gain_i_sm_map = new double[7];
        private double[] Soa_i_sm_map = new double[7];
        private double[] RearSoa_i_sm_map = new double[7];
        private string[] Matrix_ph1_Fwd_Map_Sm = new string[7];
        private string[] Matrix_ph1_Rev_Map_Sm = new string[7];
        private string[] Matrix_ph2_Fwd_Map_Sm = new string[7];
        private string[] Matrix_ph2_Rev_Map_Sm = new string[7];
        private string[] Matrix_Pratio_Fwd_Map_Sm = new string[7];
        private string[] Matrix_Pratio_Rev_Map_Sm = new string[7];
        private double[] Modal_Distort_SM = { 0, 0, 0, 0, 0, 0, 0 };
        private int[] Num_Lm = { 0, 0, 0, 0, 0, 0, 0 };
        private int[] Screen_Result_Sm =  { 0, 0, 0, 0, 0, 0, 0 };
        private int[] Sm_ID =  { 0, 0, 0, 0, 0, 0, 0 };
        private string[] Test_status = { "fail", "fail", "fail", "fail", "fail", "fail", "fail" };
        private string[] Time_Date = { "0", "0", "0", "0", "0", "0", "0" };
        private int[] Node = new int[7];
        private string[] Equip_Id = new string[7];
        


        #endregion

        #region for Light Tower.

        private SerialPort _serialPort;
        /// <summary>
        /// Serial port to control the lighter to indicate test messeage
        /// </summary>
        public SerialPort SerialPort
        {
            get { return _serialPort; }
        }

        /// <summary>
        /// Construct a serial port object from parts of the resource string.
        /// </summary>
        /// <param name="comPort">COMx port portion of resource string.</param>
        /// <param name="rate">bps rate portion of resource string.</param>
        /// <returns>Constructed SerialPort handler instance.</returns>
        private SerialPort buildSerPort(string comPort, string rate)
        {
            return new System.IO.Ports.SerialPort(
                "COM" + comPort,                                /* COMn */
                int.Parse(rate),                                /* bps */
                System.IO.Ports.Parity.None, 8, StopBits.One);  /* 8N1 */
        }
        #endregion

        

        /// <summary>
        /// Constructor
        /// </summary>
        public Prog_ILMZ_Final_ND()
        {
            this.progInfo = new Prog_ILMZ_FinalInfo();
        }
        /// <summary>
        ///  GUI for program 
        /// </summary>
        public override Type UserControl
        {
            get { return typeof(Prog_ILMZ_FinalGui); }
        }

        /// <summary>
        /// Program initiate phase
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        /// <param name="instrs"></param>
        /// <param name="chassis"></param>
        public override void InitCode(ITestEngineInit engine, DUTObject dutObject, InstrumentCollection instrs, ChassisCollection chassis)
        {
            #region light_tower

            /* Set up Serial Binary Comms */
            // _serialPort = buildSerPort(comPort, rate);
            string lightTower = @"Configuration\TOSA Final\LightTowerParams.xml";
            progInfo.lightTowerConfig = new TestParamConfigAccessor(dutObject,
                lightTower, "_", "LightTowerParams");
            if (progInfo.lightTowerConfig.GetBoolParam("LightExist"))
            {
                try
                {

                    _serialPort = this.buildSerPort(progInfo.lightTowerConfig.GetStringParam("ComNum"), progInfo.lightTowerConfig.GetStringParam("BaudRate"));
                    _serialPort.Close();
                    if (!_serialPort.IsOpen)
                    {
                        _serialPort.Open();
                    }

                    // Add the light tower color.
                    _serialPort.WriteLine(progInfo.lightTowerConfig.GetStringParam("LightGreen"));
                    _serialPort.Close();
                }
                catch { }

            }
            #endregion light_tower...

            base.InitCode(engine, dutObject, instrs, chassis);
           // string _MzFileDirectory = progInfo.TestParamsConfig.GetStringParam("MzFileDirectory");
            //string _fileDir_subFolder = "\\" + dutObject.SerialNumber + "_" + String.Format("{0:yyyyMMddhhmmss}", DateTime.Now);

            //MzFileDirectory = _MzFileDirectory + _fileDir_subFolder.Replace('.', '_');
            int node_id = dutObject.NodeID;
            Node=new int[7] {node_id,node_id,node_id,node_id,node_id,node_id,node_id};
            string equip_id=dutObject.EquipmentID;
            Equip_Id = new string[7] { equip_id, equip_id, equip_id, equip_id, equip_id, equip_id, equip_id };

        }
        /// <summary>
        /// Check Dut's test stage, if it is not in "final" stage, prompt Error in program to stop test
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        protected override void GetTestStageInformation(ITestEngineInit engine, DUTObject dutObject)
        {            
            if (!dutObject.TestStage.ToLower().Contains("final"))
            {
                engine.ErrorInProgram(string.Format("Un-matched test stage {0}. Expected \"final\" or \"Map+Final\" ", dutObject.TestStage));
            }
            // Get test stage information - Must be "final"
            progInfo.TestStage = FinalTestStage.Final;
            
        }
        /// <summary>
        /// verify device internal high temperature...
        /// </summary>
        /// <param name="engine"></param>
        protected override void VerifyJigTemperauture(ITestEngineInit engine)
        {
            if (VerifyJigTemperautureIsOk) return;
            engine.SendStatusMsg("verify device internal high temperature...");

            //get inst
            Inst_Ke2510 dsdbrTec = progInfo.Instrs.TecDsdbr as Inst_Ke2510;

            Inst_Nt10a jigTec = progInfo.Instrs.TecCase as Inst_Nt10a;

            double JigHighTempValue = progInfo.TestConditions.HighTemp;
            dsdbrTec.OutputEnabled = false;
            
            dsdbrTec.Set_Protection_Level_Temperature(Inst_Ke2510.LimitType.UPPER, JigHighTempValue + 5);

            //jigTec.Set_Protection_Level_Temperature(Inst_Ke2510.LimitType.UPPER, JigHighTempValue + 5);//Echo remed this line, nt8A can't support this function

            jigTec.SensorTemperatureSetPoint_C = JigHighTempValue;
            jigTec.OutputEnabled = true;

            int count = 0;
            double Tolerance = 3;
            do
            {
                Thread.Sleep(2000);
                double dsdbrTemp = dsdbrTec.SensorTemperatureActual_C;
                //double mztemp = mzTec.SensorTemperatureActual_C;
                //&& JigHighTempValue - Tolerance < mztemp && mztemp < JigHighTempValue + Tolerance
                if (JigHighTempValue - Tolerance < dsdbrTemp && dsdbrTemp < JigHighTempValue + Tolerance+5 )
                    break;
                count++;
                if (count > 60)
                {
                    jigTec.SensorTemperatureSetPoint_C = progInfo.TestParamsConfig.GetDoubleParam("TecCase_SensorTemperatureSetPoint_C");
                    Thread.Sleep(2000);
                    jigTec.OutputEnabled = false;
                    //engine.ErrorInModule("");
                    throw new Exception("The Dsdbr and Mz temperature can't reach the CaseTemp:" + JigHighTempValue.ToString());
                }
            } while (true);
            //restore settings
            TEC_LASER_OL_TEMP = dsdbrTec.SensorTemperatureActual_C;
            //TEC_MZ_OL_TEMP = mzTec.SensorTemperatureActual_C;
            TEC_JIG_CL_TEMP = jigTec.SensorTemperatureActual_C;
            jigTec.SensorTemperatureSetPoint_C = progInfo.TestParamsConfig.GetDoubleParam("TecCase_SensorTemperatureSetPoint_C");
            Thread.Sleep(1000);
            jigTec.OutputEnabled = false;
            VerifyJigTemperautureIsOk = true;
            engine.SendStatusMsg("verify is ok!");

        }
        /// <summary>
        /// Initialise test configuration
        /// </summary>
        /// <param name="dutObj">Device under test</param>
        /// <param name="engine">Test engine</param>
        protected override void initConfig(DUTObject dutObj, ITestEngineInit engine)
        {
           
            progInfo.TestParamsConfig = new TestParamConfigAccessor(dutObj,
                @"Configuration\TOSA Final\FinalTest\TcmzTestParams.xml", "", "TcmzTestParams");
            bool usepcas = progInfo.TestParamsConfig.GetBoolParam("UseLocalLimitFile");

            progInfo.InstrumentsCalData = new ConfigDataAccessor(@"Configuration\TOSA Final\InstrsCalibration_new.xml", "Calibration");

            progInfo.TempConfig = new TempTableConfigAccessor(dutObj, 1,
                @"Configuration\TOSA Final\TempTable.xml");
            progInfo.OpticalSwitchConfig=new TestParamConfigAccessor (dutObj,
                 @"Configuration\TOSA Final\FinalTest\IlmzOpticalSwitch.xml", "", "IlmzOpticalSwitchParams");

            progInfo.TestSelect = new TestSelection(@"Configuration\TOSA Final\FinalTest\testSelect_" + dutObj.PartCode + ".xml");

            progInfo.MapParamsConfig = new TestParamConfigAccessor(dutObj,@"Configuration\TOSA Final\MapTest\MappingTestConfig.xml","", "MappingTestParams");
            string _MzFileDirectory = progInfo.TestParamsConfig.GetStringParam("MzFileDirectory");
            string _fileDir_subFolder = "\\" + dutObj.SerialNumber + "_" + DateTime.Now.ToString("yyyyMMddHHmmss");

            MzFileDirectory = _MzFileDirectory + _fileDir_subFolder.Replace('.', '_');
            if (!Directory.Exists(MzFileDirectory))
            {
                Directory.CreateDirectory(MzFileDirectory);
            }

        }

        /// <summary>
        /// Initialise instruments
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="instrs"></param>   
        /// <param name="chassis"></param>
        protected override void initInstrs(ITestEngineInit engine, InstrumentCollection instrs, ChassisCollection chassis)
        {
            //add for avoidig instrument logging error
            engine.SendStatusMsg("Initialise Instruments");
            progInstrs = new ProgIlmzFinalInstruments();
            progInfo.Instrs = progInstrs;

            //foreach (Chassis var in chassis.Values) var.EnableLogging = false;
            //foreach (Instrument var in instrs.Values) var.EnableLogging = false;

            // Initialise DSDBR drive instruments
            switch (progInfo.DsdbrInstrsUsed)
            {
                case DsdbrDriveInstruments.FCUInstruments:
                    // FCU
                    progInstrs.Fcu = new FCUInstruments();
                    progInstrs.Fcu.FullbandControlUnit = (Instr_FCU)instrs["FullbandControlUnit"];
                    progInstrs.Fcu.SoaCurrentSource = 
                                        (InstType_ElectricalSource)instrs["SoaCurrentSource"];
                    // modify for replacing FCU 2400 steven.cui
                    //progInstrs.Fcu.FCU_Source = (InstType_ElectricalSource)instrs["FCU_Source"];
                    break;

                // Alice.Huang add FCU2Asic function    2010-02-01
                case DsdbrDriveInstruments .FCU2AsicInstrument :
                    engine.SendStatusMsg("Configuring FCU2Asic instruments");
                    progInstrs.Fcu2AsicInstrsGroups = new FCU2AsicInstruments();
                    if (instrs .Contains("FCU_Source"))
                        progInstrs .Fcu2AsicInstrsGroups .FcuSource =
                                    (InstType_ElectricalSource)instrs["FCU_Source"];
                    else 
                        progInstrs .Fcu2AsicInstrsGroups .FcuSource = null ;

                    progInstrs .Fcu2AsicInstrsGroups.AsicVccSource = 
                                        (InstType_ElectricalSource ) instrs["ASIC_VccSource"];
                    progInstrs.Fcu2AsicInstrsGroups.AsicVeeSource =
                                        (InstType_ElectricalSource)instrs["ASIC_VeeSource"];
                    progInstrs.Fcu2AsicInstrsGroups.Fcu2Asic = (Inst_Fcu2Asic)instrs["Fcu2Asic"];
                    //progInstrs.Fcu2AsicInstrsGroups.FcuSource = (Inst_Ke24xx)instrs["FcuSource"];
                    break;
                
            }

            // MZs
            /*progInstrs.Mz = new IlMzInstruments();
            bool inlineTapPresent = progInfo.TestParamsConfig.GetBoolParam("InlineTapEnabled");
            progInstrs.Mz.TapComplementary = (InstType_TriggeredElectricalSource)instrs["MzTapComp"];
            if (inlineTapPresent)
                progInstrs.Mz.TapInline = (InstType_TriggeredElectricalSource)instrs["MzTapInline"];
            progInstrs.Mz.LeftArmMod = (InstType_TriggeredElectricalSource)instrs["MzLeftMod"];
            progInstrs.Mz.RightArmMod = (InstType_TriggeredElectricalSource)instrs["MzRightMod"];*///Echo remed this block

          
           /* // Alice.Huang 2010-02-01
            // if DSDBR is source by ASIC, the Imbs should be source by Asic, 
            // so no intstrument will be set 
            // else, get in-dependant sources for Imbs arm
            if (progInfo.DsdbrInstrsUsed != DsdbrDriveInstruments.FCU2AsicInstrument)
            {
                progInstrs.Mz.FCU2Asic = null;
                progInstrs.Mz.ImbsSourceType = IlMzInstruments.EnumSourceType.ElectricalSource;
                progInstrs.Mz.LeftArmImb = (InstType_TriggeredElectricalSource)instrs["MzLeftImb"];
                progInstrs.Mz.RightArmImb = (InstType_TriggeredElectricalSource)instrs["MzRightImb"];
            }
            else
            {
                progInstrs.Mz.LeftArmImb = null;
                progInstrs.Mz.RightArmImb = null;
                progInstrs.Mz.FCU2Asic = (Inst_Fcu2Asic)instrs["Fcu2Asic"];
                progInstrs.Mz.ImbsSourceType = IlMzInstruments.EnumSourceType.Asic;

            }
            progInstrs.Mz.PowerMeter = (IInstType_TriggeredOpticalPowerMeter)instrs["OpmMz"];
            // Alice.Huang   2010-04-21
            // set wavemeter in single measurement state to avoid error reading
            Inst_Ag86122x ag8612x = progInstrs.Mz.PowerMeter as Inst_Ag86122x;
            if (ag8612x !=null ) ag8612x .ContinualMeasurement = false ;*///Echo remed this block
            //Inst_Ag816x_OpticalPowerMeter tempMeter = (Inst_Ag816x_OpticalPowerMeter)instrs["OpmMz"];
            progInstrs.Mz = new IlMzInstruments();
            progInstrs.Mz.PowerMeter = (IInstType_TriggeredOpticalPowerMeter)instrs["OpmMz"];
            // OpmRef
            progInstrs.OpmReference = (IInstType_OpticalPowerMeter)instrs["OpmRef"];

            progInstrs.Mz.PowerMeter.EnableInputTrigger(false);
            progInstrs.Mz.PowerMeter.SetDefaultState();
            progInstrs.OpmReference.SetDefaultState();
           
            // OSA
          /*  progInstrs.Osa = (IInstType_OSA)instrs["Osa"];

            // WM
            progInstrs.Wavemeter = (IInstType_Wavemeter)instrs["Wavemeter"];*/ //Echo remed this block

            // Switch path manager
            //Util_SwitchPathManager switchPathManager = new Util_SwitchPathManager
             //   (@"Configuration\TOSA Final\switches.xml", instrs);
           // progInstrs.SwitchPathManager = switchPathManager;

            // Switch between OSA and Opm
           // Switch_Osa_MzOpm switchOsaMzOpm = new Switch_Osa_MzOpm(switchPathManager);
           // progInstrs.SwitchOsaMzOpm = switchOsaMzOpm;

            // TECs
            progInstrs.TecDsdbr = (IInstType_TecController)instrs["TecDsdbr"];
            DsdbrUtils.opticalSwitch = (Inst_Ke2510)instrs["TecDsdbr"];
            DsdbrTuning.OpticalSwitch = (Inst_Ke2510)instrs["TecDsdbr"];

            ////progInstrs.TecMz = (IInstType_TecController)instrs["TecMz"];
            progInstrs.TecCase = (IInstType_TecController)instrs["TecCase"];
            progInstrs.TecCase.SensorTemperatureSetPoint_C = progInfo.TestParamsConfig.GetDoubleParam("TecCase_SensorTemperatureSetPoint_C");

            // Config instruments if not simulation mode
            if (!engine.IsSimulation)
            {
                engine.SendStatusMsg("Configuring Instruments");
                // Configure FCU
                if (progInfo.DsdbrInstrsUsed == DsdbrDriveInstruments.FCUInstruments)
                    ConfigureFCUInstruments(progInstrs.Fcu);

                    // Alice.Huang   2010-02-01
                    // add FCU2ASIC instrument initialise for TOSA Test
                else if (progInfo.DsdbrInstrsUsed == DsdbrDriveInstruments.FCU2AsicInstrument)
                    ConfigureFCU2AsicInstruments(progInstrs.Fcu2AsicInstrsGroups);

                // Configure TEC controllers

                ConfigureTecController(progInstrs.TecDsdbr, "TecDsdbr");
                //ConfigureTecController(progInstrs.TecMz, "TecMz");
                ConfigureTecController(progInstrs.TecCase, "TecCase");

               

                /*// Configure MZ bias sources
                ConfigureMzBiasSource(progInstrs.Mz.LeftArmMod, "MzLeftMod");
                ConfigureMzBiasSource(progInstrs.Mz.RightArmMod, "MzRightMod");
                
                
                // Alice.Huang 2010-02-01
                // if DSDBR is source by ASIC, the Imbs should be source by Asic, 
                // so no intstrument will be set and initialise 
                // else, initialise  in-dependant sources for Imbs arm
                if (progInfo.DsdbrInstrsUsed != DsdbrDriveInstruments.FCU2AsicInstrument)
                {
                    ConfigureMzBiasSource(progInstrs.Mz.LeftArmImb, "MzLeftImb");
                    ConfigureMzBiasSource(progInstrs.Mz.RightArmImb, "MzRightImb");
                }

                ConfigureMzBiasSource(progInstrs.Mz.TapComplementary, "MzTapComp");
                
                if (inlineTapPresent)
                    ConfigureMzBiasSource(progInstrs.Mz.TapInline, "MzTapInline");
                //alice : here add asa share option, and put it in a try-catch to insure unlock
                try
                {
                    bool lockflag;
                    int lockCount = 0;
                    do
                    {
                        lockflag = TcmzOSAshare.LockInstruments();
                        lockCount++;
                    }
                    while ((!lockflag) && (lockCount < 3));
                    if (!lockflag)
                    {
                        throw new InstrumentException("can't lock instrument" +
                            "\nplease check if shared instrument used by other test kit without release" +
                            " or instrument lock file is exist in the setting path");
                    }
                progInstrs.Osa.SetDefaultState();
                progInstrs.Osa.ReferenceLevel_dBm = 10.0;
                progInstrs.Osa.WavelengthStart_nm = 1520.0;
                progInstrs.Osa.WavelengthStop_nm = 1620.0;
                if (progInstrs.Osa.DriverName.Contains("Ag8614x"))
                {
                    Inst_Ag8614xA ag8614xOsa = (Inst_Ag8614xA)progInstrs.Osa;
                    ag8614xOsa.Sensitivity_dBm = Convert.ToDouble(progInfo.TestParamsConfig.GetStringParam("Osa_Sensitivity"));
                }
                }
                catch (Exception e)
                {
                    throw e;
                }
                finally
                {
                    if (TcmzOSAshare.OsaShareFlag) TcmzOSAshare.UnlockInstruments();
                }
                // Configure power meter
                progInstrs.OpmReference.SetDefaultState();
                //progInstrs.Mz.PowerMeter.SetDefaultState();

                // Configure switches
                progInstrs.SwitchOsaMzOpm.SetState(Switch_Osa_MzOpm.State.MzOpm);*///Echo remed it
                progInstrs.OpmReference.SetDefaultState();
                progInstrs.Mz.PowerMeter.SetDefaultState();
            }
        }

        /// <summary>
        /// Load specifications
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        protected override void loadSpecs(ITestEngineInit engine, DUTObject dutObject)
        {
            engine.SendStatusMsg("Loading Specification");
            ILimitRead limitReader = null;
            StringDictionary mainSpecKeys = new StringDictionary();
            StringDictionary smSpecKeys = new StringDictionary();//supermode spec key
            SpecList specList = new SpecList();
            string smSpecNameStub = "sm";
            dutObject.PartCode = "HITTNEW";

            if (progInfo.TestParamsConfig.GetBoolParam("UseLocalLimitFile"))
            {
                // initialise limit file reader
                limitReader = engine.GetLimitReader("PCAS_FILE");
                // main specification
                string final_SpecNameFile = progInfo.TestParamsConfig.GetStringParam("FinalLocalLimitFileName");
                string finalSpecFullFilename = progInfo.TestParamsConfig.GetStringParam("LocalLimitFileDirectory")
                    + @"\" + final_SpecNameFile;
                mainSpecKeys.Add("Filename", finalSpecFullFilename);

                string map_SpecNameFile = progInfo.TestParamsConfig.GetStringParam("MapLocalLimitFileName");
                string mapSpecFullFilename = progInfo.TestParamsConfig.GetStringParam("LocalLimitFileDirectory")
                    + @"\" + map_SpecNameFile;
                smSpecKeys.Add("Filename", mapSpecFullFilename);
            }
            else
            {
                // Use default limit source
                limitReader = engine.GetLimitReader();

                mainSpecKeys.Add("SCHEMA", "HIBERDB");
                mainSpecKeys.Add("TEST_STAGE", "final");
                mainSpecKeys.Add("DEVICE_TYPE", dutObject.PartCode);

                smSpecKeys.Add("SCHEMA", "hiberdb");
                smSpecKeys.Add("DEVICE_TYPE", dutObject.PartCode);
                smSpecKeys.Add("TEST_STAGE", "map_sm0");
            }

            // load main specification

            SpecList tempSpecList = limitReader.GetLimit(mainSpecKeys);
            progInfo.MainSpec = tempSpecList[0];

            dutObject.Attributes.AddString("SupermodeSpecNameStub", smSpecNameStub);

            tempSpecList = limitReader.GetLimit(smSpecKeys);
            Specification smSpecMaster = tempSpecList[0];
            smSpecName = smSpecMaster.Name;

            this.SupermodeSpecs = new SpecList();

            for (int ii = 0; ii < 7; ii++)
            {
                Specification smSpec_ii = new Specification(smSpecNameStub + "_" + ii.ToString(), smSpecMaster);
                this.SupermodeSpecs.Add(smSpec_ii);

            }
            specList.Add(progInfo.MainSpec);
            foreach (Specification smSpec in this.SupermodeSpecs)
            {
                specList.Add(smSpec);
            }


            // Get our specification object (so we can initialise modules with appropriate limits)
            
            string FINAL_TC_NUM_CHAN_REQUIRED = progInfo.MainSpec.GetParamLimit("TC_NUM_CHAN_REQUIRED").LowLimit.ValueToString();
            string FINAL_TC_OPTICAL_FREQ_START = progInfo.MainSpec.GetParamLimit("TC_OPTICAL_FREQ_START").LowLimit.ValueToString();
            

            // Create a dummy spec to avoid "All specifications has failed" promotion by Test Engine
            Specification dummySpec = new Specification("DummySpec", new StringDictionary(), 1);

            // declare spec list to Test Engine
            
            specList.Add(dummySpec);
            engine.SetSpecificationList(specList);
           
            //get test conditions , Echo update this for debug
            //progInfo.TestConditions = new IlmzFinalTestConds(progInfo.MainSpec);
            // Use default limit source
            if (!doMapping)
            {
                //limitReader = engine.GetLimitReader();
                //StringDictionary ConditionSpecKeys = new StringDictionary();

                //ConditionSpecKeys.Add("SCHEMA", "HIBERDB");
                //ConditionSpecKeys.Add("TEST_STAGE", "final");
                //ConditionSpecKeys.Add("DEVICE_TYPE", "PA007371");
                //SpecList tempSpecList2 = limitReader.GetLimit(ConditionSpecKeys);
                //Specification ConditionSpec = tempSpecList2[0];
                progInfo.TestConditions = new IlmzFinalTestConds(progInfo.MainSpec);
                GetSuperModeResultFromPcas(engine, dutObject);
            }
            else
            {
                progInfo.TestConditions = new IlmzFinalTestConds(progInfo.MainSpec);
            }

            

        }

        /// <summary>
        /// Initialise utilities
        /// </summary>
        /// <param name="engine"></param>
        protected override void initUtils(ITestEngineInit engine)
        {
            engine.SendStatusMsg("Initialise utilities");
            // Initialise DsdbrUtils and Measurements utilities by testkit hardware info.
            switch (progInfo.DsdbrInstrsUsed)
            {
                // added FCU2ASIC Option by Alice.Huang, for TOSA GB test     2010-02-01
                case DsdbrDriveInstruments .FCU2AsicInstrument :
                    DsdbrUtils.Fcu2AsicInstrumentGroup = progInfo.Instrs.Fcu2AsicInstrsGroups;
                    Measurements.FCU2AsicInstrs = progInfo.Instrs.Fcu2AsicInstrsGroups;
                    SoaShutterMeasurement.Fcu2AsicInstrs = progInfo.Instrs.Fcu2AsicInstrsGroups;
                    break;
                case DsdbrDriveInstruments.FCUInstruments:
                    DsdbrUtils.FcuInstrumentGroup = progInfo.Instrs.Fcu;
                    Measurements.FCUInstrs = progInfo.Instrs.Fcu;
                    SoaShutterMeasurement.FCUInstrs = progInfo.Instrs.Fcu;
                    break;                
            }
            //Measurements.OSA = progInfo.Instrs.Osa;
            //Measurements.Wavemeter = progInfo.Instrs.Wavemeter;
            Measurements.MzHead = progInfo.Instrs.Mz.PowerMeter;
            Measurements.ExternalHead = progInfo.Instrs.OpmReference;

            
            //Measurements.SectionIVSweepRawData = null;
            
            // Initialise TraceToneTest utility by device types
            if (progInfo.TestParamsConfig.GetBoolParam("TraceToneTestEnabled"))
            {
                TraceToneTest.Initialise(
                    progInfo.TestParamsConfig.GetIntParam("TraceToneTest_OrderOfFit"),
                    progInfo.TestConditions.TargetFibrePower_dBm,
                    progInfo.TestConditions.TraceToneTestAgeCond1_dB,
                    progInfo.TestConditions.TraceToneTestAgeCond2_dB,
                    progInfo.TestParamsConfig.GetDoubleParam("TraceToneTest_ModulationIndex"),
                    progInfo.TestParamsConfig.GetDoubleParam("TraceToneTest_SoaStart_mA"),
                    progInfo.TestParamsConfig.GetDoubleParam("TraceToneTest_SoaEnd_mA"),
                    progInfo.TestParamsConfig.GetDoubleParam("TraceToneTest_SoaStep_mA"),
                    progInfo.TestParamsConfig.GetIntParam("TraceToneTest_StepDelay_mS"));
            }
            this.initOpticalSwitchIoLine();
                       
        }

        private void initOpticalSwitchIoLine()
        {
            IlmzOpticalSwitchLines.C_Band_DigiIoLine = int.Parse(progInfo.OpticalSwitchConfig.GetStringParam("C_Band_Filter_IO_Line"));
            IlmzOpticalSwitchLines.C_Band_DigiIoLine_State = progInfo.OpticalSwitchConfig.GetBoolParam("C_Band_Filter_IO_Line_State");
            IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine = int.Parse(progInfo.OpticalSwitchConfig.GetStringParam("Dut_Ctap_IO_Line"));
            IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine_State = progInfo.OpticalSwitchConfig.GetBoolParam("Dut_Ctap_IO_Line_State");
            IlmzOpticalSwitchLines.Dut_Rx_DigiIoLine = int.Parse(progInfo.OpticalSwitchConfig.GetStringParam("Dut_Rx_IO_Line"));
            IlmzOpticalSwitchLines.Dut_Rx_DigiIoLine_State = progInfo.OpticalSwitchConfig.GetBoolParam("Dut_Rx_IO_Line_State");
            IlmzOpticalSwitchLines.Dut_Tx_DigiIoLine = int.Parse(progInfo.OpticalSwitchConfig.GetStringParam("Dut_Tx_IO_LIne"));
            IlmzOpticalSwitchLines.Dut_Tx_DigiIoLine_State = progInfo.OpticalSwitchConfig.GetBoolParam("Dut_Tx_IO_LIne_State");
            IlmzOpticalSwitchLines.Etalon_Ref_DigiIoLine = int.Parse(progInfo.OpticalSwitchConfig.GetStringParam("Etalon_Ref_IO_Line"));
            IlmzOpticalSwitchLines.Etalon_Ref_DigiIoLine_State = progInfo.OpticalSwitchConfig.GetBoolParam("Etalon_Ref_IO_Line_State");
            IlmzOpticalSwitchLines.Etalon_Rx_DigiIoLine = int.Parse(progInfo.OpticalSwitchConfig.GetStringParam("Etalon_Rx_IO_Line"));
            IlmzOpticalSwitchLines.Etalon_Rx_DigiIoLine_State = progInfo.OpticalSwitchConfig.GetBoolParam("Etalon_Rx_IO_Line_State");
            IlmzOpticalSwitchLines.Etalon_Tx_DigiIoLine = int.Parse(progInfo.OpticalSwitchConfig.GetStringParam("Etalon_Tx_IO_Line"));
            IlmzOpticalSwitchLines.Etalon_Tx_DigiIoLine_State = progInfo.OpticalSwitchConfig.GetBoolParam("Etalon_Tx_IO_Line_State");
            IlmzOpticalSwitchLines.L_Band_DigiIoLine = int.Parse(progInfo.OpticalSwitchConfig.GetStringParam("L_Band_Filter_IO_Line"));
            IlmzOpticalSwitchLines.L_Band_DigiIoLine_State = progInfo.OpticalSwitchConfig.GetBoolParam("L_Band_Filter_IO_Line_State");
        }

        /// <summary>
        /// Initialise test modules
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        protected override void initModules(ITestEngineInit engine, DUTObject dutObject, InstrumentCollection instrs, ChassisCollection chassis)
        {
            engine.SendStatusMsg("Initialise modules");
            InitMod_OverallMap(engine, instrs, chassis, dutObject);
            InitMod_Supermode(engine, instrs, chassis, dutObject);
            InitMod_Characterise(engine, instrs, chassis, dutObject);
            initMod_LockerLeveling(engine, instrs, chassis, dutObject);
            this.IlmzInitialise_InitModule(engine, dutObject);
            this.DeviceTempControl_InitModule(engine, "DsdbrTemperature", progInfo.Instrs.TecDsdbr, "TecDsdbr");
            //this.DeviceTempControl_InitModule(engine, "MzTemperature", progInfo.Instrs.TecMz, "TecMz");
            this.CaseTempControl_InitModule(engine, "CaseTemp_Mid", 25, progInfo.TestConditions.MidTemp);
            this.IlmzPowerUp_InitModule(engine,instrs);
            this.IlmzCrossCalibration_InitModule(engine,instrs);
            this.IlmzPowerHeadCal_InitModule(engine, dutObject);
            this.IlmzMzCharacterise_InitModule(engine, dutObject,instrs);
            this.IlmzMzItuEstimate_InitModule(engine, dutObject);
            this.IlmzChannelChar_InitModule(engine, dutObject);
            if (progInfo.TestParamsConfig.GetBoolParam("LowTempTestEnabled"))
            {
                this.CaseTempControl_InitModule(engine, "CaseTemp_Low", progInfo.TestConditions.MidTemp, progInfo.TestConditions.LowTemp);
                this.CaseTempControl_InitModule(engine, "CaseTemp_High", progInfo.TestConditions.LowTemp, progInfo.TestConditions.HighTemp);
                this.IlmzLowTempTest_InitModule(engine, dutObject);
            }
            else
            {
                this.CaseTempControl_InitModule(engine, "CaseTemp_High", progInfo.TestConditions.MidTemp, progInfo.TestConditions.HighTemp);
            }
            this.IlmzHighTempTest_InitModule(engine, dutObject);
            if (progInfo.TestParamsConfig.GetBoolParam("TraceToneTestEnabled"))
            {
                this.TcmzTraceToneTest_InitModule(engine, dutObject);
            }
            this.IlmzGroupResults_InitModule(engine, dutObject);
            this.CaseTempControl_InitModule(engine, "CaseTemp_Safe", progInfo.TestConditions.HighTemp, 25);   // TODO - tidier method?
            //new added from mapping software
            
           
        }

        private void initMod_LockerLeveling(ITestEngineInit engine, InstrumentCollection instrs, ChassisCollection chassis, DUTObject dutObject)
        {
            ModuleRun modRun = engine.AddModuleRun("LockerLeveling", "LockerLeveling", string.Empty);
            modRun.ConfigData.AddReference("Mapping_Settings", progInfo.MapParamsConfig);
            modRun.Instrs.Add(instrs);
            modRun.Chassis.Add(chassis);
            modRun.ConfigData.AddString("TIME_STAMP", TestTimeStamp);
            modRun.ConfigData.AddString("LASER_ID", dutObject.SerialNumber);
            modRun.ConfigData.AddString("RESULT_DIR", MzFileDirectory);
            modRun.ConfigData.AddDouble("RX_TARGET_CURRENT", progInfo.TestConditions.RxTargetCurrent);
            modRun.ConfigData.AddDouble("MIN_REARSOA",  progInfo.TestConditions.RearsoaMin);
            modRun.ConfigData.AddDouble("MAX_REARSOA", progInfo.TestConditions.RearsoaMax);
            modRun.ConfigData.AddDouble("locker_tran_pot", locker_pot_value);
            modRun.Instrs.Add("Optical_switch", (Instrument)progInfo.Instrs.TecDsdbr);
        }

        //private void InitMod_Characterise(ITestEngineInit engine, object instrs, object chassis, DUTObject dutObject)
        private void InitMod_Characterise(ITestEngineInit engine, InstrumentCollection instrs, ChassisCollection chassis, DUTObject dutObject)
        {
            int chanNbr = 0;
            double startFreq = 0;

            if (MzFileDirectory[MzFileDirectory.Length - 1] != '\\')
            {
                MzFileDirectory = MzFileDirectory + "\\";
            }

            ModuleRun modRun = engine.AddModuleRun("Mod_Characterise", "Mod_Characterise", string.Empty);

           
            #region add configure data

            bool isDelItuFile_When_MissingLine = progInfo.MapParamsConfig.GetBoolParam("DelItuFile_When_MissingLine");
            
            modRun.ConfigData.AddString("TESTTIMESTAMP", GetCurrentTimeStamp());
            modRun.ConfigData.AddString("RESULT_DIR", MzFileDirectory);
            modRun.ConfigData.AddString("LASERID", dutObject.SerialNumber);
            modRun.ConfigData.AddSint32("ChanNbr", progInfo.TestConditions.NbrChannels);
            modRun.ConfigData.AddDouble("StartFrequency", progInfo.TestConditions.ItuLowFreq_GHz);

            //modRun.ConfigData.AddReference("Overall_Map_Settings", ProgramCfgReader);
            //modRun.ConfigData.AddReference("Supermode_Map_Settings", ProgramCfgReader);
            modRun.ConfigData.AddReference("Characterization_Settings", progInfo.MapParamsConfig);
            modRun.ConfigData.AddBool("isDelItuFile_MissingLine", isDelItuFile_When_MissingLine);

            modRun.Instrs.Add(instrs);
            modRun.Chassis.Add(chassis);

            //link limits
            Specification spec = progInfo.MainSpec;
            modRun.Limits.AddParameter(spec, "CG_CHAR_ITU_FILE", "CG_CHAR_ITU_FILE");
            modRun.Limits.AddParameter(spec, "CHAR_ITU_OP_COUNT", "CHAR_ITU_OP_COUNT");
            #endregion
        }

        private void InitMod_Supermode(ITestEngineInit engine, InstrumentCollection instrs, ChassisCollection chassis, DUTObject dutObject)
        {
            if (MzFileDirectory[MzFileDirectory.Length - 1] != '\\')
            {
                MzFileDirectory = MzFileDirectory + "\\";
            }
            numberOfSupermodes = 7;

            NameValueCollection SupperModeMapDataSource_Etalon = (NameValueCollection)CGcfgReader.GetSection("CloseGridSetting/SuperMap/BoundaryDetection");
            int useEtalon = int.Parse(SupperModeMapDataSource_Etalon["UseEtalonData"].ToString());

            for (int i = 0; i < numberOfSupermodes; i++)
            {
                ModuleRun modRun = engine.AddModuleRun("Mod_Supermode_" + i.ToString(), "Mod_Supermode", string.Empty);
                //modRun.ConfigData.AddReference("Overall_Map_Settings", cfgReader.GetSection("TestModule/MappingSettings/Overall_Map_Settings"));
                string FileName = "Im_DSDBR01_" + dutObject.SerialNumber + "_" + GetCurrentTimeStamp() + "_SM" + i.ToString() + ".csv";
                modRun.ConfigData.AddSint32("NUM_SUPERMODE", i);
                modRun.ConfigData.AddString("MIDDLE_LINE_PATH", MzFileDirectory + FileName);
                modRun.ConfigData.AddString("PHASE_SETUP_FILE_PATH", this.progInfo.MapParamsConfig.GetStringParam("PhaseCurrentFile"));
                modRun.ConfigData.AddReference("Mapping_Settings", progInfo.MapParamsConfig);
                modRun.ConfigData.AddString("RESULT_DIR", MzFileDirectory);
                modRun.ConfigData.AddString("TIME_STAMP", TestTimeStamp);
                modRun.ConfigData.AddString("LASER_ID", dutObject.SerialNumber);
                modRun.ConfigData.AddUint32("SUPERMODE_NUM", (long)i);
                modRun.ConfigData.AddString("EQUIP_ID", dutObject.EquipmentID);
                modRun.ConfigData.AddString("SPEC_ID", smSpecName);
                modRun.ConfigData.AddSint32("NODE", dutObject.NodeID);
                modRun.ConfigData.AddSint32("USEETALON", useEtalon);
                modRun.ConfigData.AddString("NoFilePath", Nofilepath);
                modRun.Instrs.Add("FCU", (Instrument)(Inst_Fcu2Asic)instrs["Fcu2Asic"]);
                modRun.Instrs.Add("Optical_switch", (Instrument)instrs["TecDsdbr"]);
                modRun.Chassis.Add(chassis);

                #region Tie up limits
                //Specification spec=SpecDict["map_sm" + i.ToString()];
                Specification spec = this.SupermodeSpecs[i];

                modRun.Limits.AddParameter(spec, "CG_DUT_FILE_TIMESTAMP", "timestamp");
                modRun.Limits.AddParameter(spec, "CG_LM_LINES_SM_FILE", "cg_lm_lines_sm_file");
                //modRun.Limits.AddParameter(spec, "EQUIP_ID", "equipId");
                modRun.Limits.AddParameter(spec, "GAIN_I_SM_MAP", "gain_i_sm_map");
                
               modRun.Limits.AddParameter(spec, "MATRIX_PRATIO_FWD_MAP_SM", "pRatio_fwd");
                modRun.Limits.AddParameter(spec, "MATRIX_PRATIO_REV_MAP_SM", "pRatio_rev");
                
                modRun.Limits.AddParameter(spec, "MATRIX_PHOTO1_FWD_MAP_SM", "photo1_fwd");
                modRun.Limits.AddParameter(spec, "MATRIX_PHOTO1_REV_MAP_SM", "photo1_rev");
                
                modRun.Limits.AddParameter(spec, "MATRIX_PHOTO2_FWD_MAP_SM", "photo2_fwd");
                modRun.Limits.AddParameter(spec, "MATRIX_PHOTO2_REV_MAP_SM", "photo2_rev");

                modRun.Limits.AddParameter(spec, "MODAL_DISTORT_SM", "modal_distort");
                modRun.Limits.AddParameter(spec, "NUM_LM", "num_lm");
                modRun.Limits.AddParameter(spec, "SM_ID", "smID");
                modRun.Limits.AddParameter(spec, "SOA_I_SM_MAP", "sm_i_soa");
                modRun.Limits.AddParameter(spec, "TIME_DATE", "testTimeStamp");
                modRun.Limits.AddParameter(spec, "CG_PASSFAIL_SM_FILE", "cg_passfail_sm_file");
                modRun.Limits.AddParameter(spec, "CG_QAMETRICS_SM_FILE", "cg_qametrics_sm_file");
                modRun.Limits.AddParameter(spec, "SCREEN_RESULT_SM", "screen_result_sm");
                modRun.Limits.AddParameter(spec, "REARSOA_I_SM_MAP", "rearsoa_i_sm");
                //modRun.Limits.AddParameter(spec, "CG_ID", "CGid");
                //modRun.Limits.AddParameter(spec,"NODE", "NodeId");

                #endregion

            }
        }

        //private void InitMod_OverallMap(ITestEngineInit engine, object instrs, object chassis, DUTObject dutObject)
        private void InitMod_OverallMap(ITestEngineInit engine, InstrumentCollection instrs, ChassisCollection chassis, DUTObject dutObject)
        {
            string FrequencyCalibration_file_Path = "L_band_FrequencyCalibration_file_Path";

            string str = progInfo.TestConditions.FreqBand.ToString();
            if (str == "C")
                FrequencyCalibration_file_Path = "C_band_FrequencyCalibration_file_Path";
            string freqCalFilename = progInfo.MapParamsConfig.GetStringParam(FrequencyCalibration_file_Path);
            FreqCalByEtalon.LoadFreqlCalArray(freqCalFilename, double.Parse(progInfo.MainSpec.GetParamLimit("TC_CAL_OFFSET").LowLimit.ValueToString()));
 
            ModuleRun modRun = engine.AddModuleRun("Mod_OverallMap", "Mod_OverallMap", string.Empty);
            modRun.ConfigData.AddReference("Overall_Map_Settings", CGcfgReader.GetSection("CloseGridSetting/OverAllMap/BoundaryDetection"));
            modRun.ConfigData.AddReference("Mapping_Settings", progInfo.MapParamsConfig);
            modRun.ConfigData.AddString("CG_Setting_File", cg_setting_file);
            modRun.ConfigData.AddString("NoFilePath", Nofilepath);
            modRun.Instrs.Add(instrs);
            modRun.Chassis.Add(chassis);
            this.GetCurrentTimeStamp();
            modRun.ConfigData.AddString("TIME_STAMP", TestTimeStamp);
            modRun.ConfigData.AddString("LASER_ID", dutObject.SerialNumber);
            modRun.ConfigData.AddString("RESULT_DIR", MzFileDirectory);

            modRun.ConfigData.AddDouble("TARGET_POWER", progInfo.TestConditions.TargetFibrePower_dBm);
            modRun.ConfigData.AddString("FreqBand", progInfo.TestConditions.FreqBand.ToString());
            
            modRun.Instrs.Add("Optical_switch", (Instrument)instrs["TecDsdbr"]);
            modRun.Instrs.Add("FCU", (Instrument)(Inst_Fcu2Asic)instrs["Fcu2Asic"]);
            
               
            Specification spec = progInfo.MainSpec;
            modRun.Limits.AddParameter(spec, "NUM_SM", "SM_NUM");
            modRun.Limits.AddParameter(spec, "REF_FIBRE_POWER", "Ref_fiber_power");
            modRun.Limits.AddParameter(spec, "REF_FREQ", "calFreq_Ghz");
            //modRun.Limits.AddParameter(spec, "REF_MZ_VON_LEFT", "von_left");
            //modRun.Limits.AddParameter(spec, "REF_MZ_VON_RIGHT", "von_right");
            //modRun.Limits.AddParameter(spec, "REF_MZ_VOFF_LEFT", "voff_left");
            //modRun.Limits.AddParameter(spec, "REF_MZ_VOFF_RIGHT", "voff_right");
            //modRun.Limits.AddParameter(spec, "REF_MZ_SUM", "ref_mz_sum");
            //modRun.Limits.AddParameter(spec, "REF_MZ_RATIO", "ref_mz_ratio");
            modRun.Limits.AddParameter(spec, "MZ_MAX_V_BIAS", "max_V_Bias");
            modRun.Limits.AddParameter(spec, "REF_LI_FILE", "fileFullPath");
            //modRun.Limits.AddParameter(spec, "ITX_DARK", "ITX_DARK_mA");
            //modRun.Limits.AddParameter(spec, "IRX_DARK", "IRX_DARK_mA");
            //modRun.Limits.AddParameter(spec, "MZ_MOD_LEFT_DARK_I", "L_dark_mA");
           // modRun.Limits.AddParameter(spec, "MZ_MOD_RIGHT_DARK_I", "R_dark_mA");
            //modRun.Limits.AddParameter(spec, "TAP_COMP_DARK_I", "tap_comp_dark_i"); //Echo remed this block because initialise mz module has added these parameter 
            modRun.Limits.AddParameter(spec, "CG_SM_LINES_FILE", "cg_sm_lines_file");
            modRun.Limits.AddParameter(spec, "CG_PRATIO_WL_FILE", "cg_pratio_wl_file");
            modRun.Limits.AddParameter(spec, "CG_PASSFAIL_FILE", "cg_passfail_file");
            modRun.Limits.AddParameter(spec, "CG_QAMETRICS_FILE", "cg_qametrics_file");
            modRun.Limits.AddParameter(spec, "MATRIX_PRATIO_SUMMARY_MAP", "matrix_pratio_summary_map");
            modRun.Limits.AddParameter(spec, "REGISTRY_FILE", "registry_file");
            modRun.Limits.AddParameter(spec, "REF_MZ_IMB_LEFT", "imb_left");
            modRun.Limits.AddParameter(spec, "REF_MZ_IMB_RIGHT", "imb_right");
            modRun.Limits.AddParameter(spec, "SOA_POWER_LEVELING", "SOA_PowerLevel");
        }

              

        /// <summary>
        /// Creates an adjusted specifiation for engineering use
        /// </summary>
        /// <param name="engine">reference to test engine</param>
        /// <returns>An adjusted specification</returns>
        private Specification ConstructEngineeringSpec(ITestEngineInit engine)
        {
            engine.SendStatusMsg("Construct Engineering specification");
            GuiMsgs.TcmzEngineeringGuiResponse resp = (GuiMsgs.TcmzEngineeringGuiResponse)engine.ReceiveFromGui().Payload;
            double engAdjustment = (double)resp.FiddleFactor;

            // Calculate power difference
            double upperLimit = Convert.ToDouble(progInfo.MainSpec.GetParamLimit("CH_PEAK_POWER").HighLimit.ValueToString());
            double lowerLimit = Convert.ToDouble(progInfo.MainSpec.GetParamLimit("CH_PEAK_POWER").LowLimit.ValueToString());
            double limitTargetPower = (upperLimit - lowerLimit) / 2 + lowerLimit;
            double adjustment = limitTargetPower - engAdjustment;

            List<String> paramsToAdjust = new List<string>();
            paramsToAdjust.Add("CH_PEAK_POWER");
            paramsToAdjust.Add("CH_PWRCTRL_POWER_MAX_HIGH");
            paramsToAdjust.Add("CH_PWRCTRL_POWER_MAX_LOW");
            paramsToAdjust.Add("CH_PWRCTRL_POWER_MIN_HIGH");
            paramsToAdjust.Add("CH_PWRCTRL_POWER_MIN_LOW");
            paramsToAdjust.Add("TC_FIBRE_TARGET_POWER");
            paramsToAdjust.Add("TC_POWER_CTRL_RANGE_MAX");
            paramsToAdjust.Add("TC_POWER_CTRL_RANGE_MIN");

            Specification powerAdjustedSpec = new Specification(progInfo.MainSpec.Name, progInfo.MainSpec.ActualDatabaseKeys, progInfo.MainSpec.Priority);
            foreach (ParamLimit pl in progInfo.MainSpec)
            {
                Datum highLimit = pl.HighLimit;
                Datum lowLimit = pl.LowLimit;
                if (paramsToAdjust.Contains(pl.InternalName))
                {
                    double oldHighLimit = Convert.ToDouble(pl.HighLimit.ValueToString());
                    double trimmedHighLimit = Math.Truncate(oldHighLimit);
                    if (trimmedHighLimit != 999 && trimmedHighLimit != 9999)
                    {
                        DatumDouble newHighLimit = new DatumDouble(pl.InternalName, oldHighLimit - adjustment);
                        highLimit = (Datum)newHighLimit;
                    }

                    double oldLowLimit = Convert.ToDouble(pl.LowLimit.ValueToString());
                    double trimmedLowLimit = Math.Truncate(oldLowLimit);
                    if (trimmedLowLimit != -999 && trimmedLowLimit != -9999)
                    {
                        DatumDouble newLowLimit = new DatumDouble(pl.InternalName, oldLowLimit - adjustment);
                        lowLimit = (Datum)newLowLimit;
                    }
                }
                RawParamLimit rpl = new RawParamLimit(pl.InternalName, pl.ParamType, lowLimit, highLimit, pl.Operand, pl.Priority, pl.AccuracyFactor, pl.Units);
                powerAdjustedSpec.Add(rpl);
            }
            return powerAdjustedSpec;
        }

        /// <summary>
        /// if no pcas result for mapping, get nessesary information by manual
        /// it is useful for Eng debug
        /// </summary>
        /// <param name="engine"></param>
        public void ManualEntryMappingReults(ITestEngineInit engine)
        {
            engine.GuiShow();
            engine.GuiToFront();
            engine.SendToGui(new GuiMsgs.tosaMappingResultRequst());

            engine.SendStatusMsg("Construct Engineering mapping data");
            GuiMsgs.tosaMappingResponse  resp = (GuiMsgs.tosaMappingResponse )engine.ReceiveFromGui().Payload;

            this._mapResult = resp.mapReult;           
            if (this._mapResult == null)
            {
                string errorDescription = " No TOSA Mapping Data ,\n" + 
                    "Please test this device at Hitt_Map stage first!";

                engine.ErrorInProgram(errorDescription);
            }
            try
            {
                string MAP_TC_NUM_CHAN_REQUIRED = this._mapResult.ReadSint32("TC_NUM_CHAN_REQUIRED").ToString();
                string MAP_TC_OPTICAL_FREQ_START = this._mapResult.ReadDouble("TC_OPTICAL_FREQ_START").ToString();//Jack.Zhang
                //string MAP_TC_OPTICAL_FREQ_START = this._mapResult.ReadDouble("TC_OPTICAL_FREQ_START").ToString();
            }
            catch
            {
                string errorDescription = "Can't load this 2 map parameters: "+ 
                    "'TC_NUM_CHAN_REQUIRED' & 'TC_OPTICAL_FREQ_START'; please contact Product Engineer!";
                engine.ShowContinueUserQuery(errorDescription);
                engine.ErrorInProgram(errorDescription);
            }
            if (!this._mapResult.ReadString("TEST_STATUS").ToLower().Contains("pass"))
            {
                string errorDescription = "No passed TCMZ_Map stage data in PCAS,\n" + 
                    "Please test this device at Hitt_Map stage first!";
                engine.ShowContinueUserQuery(errorDescription);
                engine.ErrorInProgram(errorDescription);
            }

            // Get data from ITU operating file
            string closeGridCharFileName = this._mapResult.ReadFileLinkFullPath("CG_CHAR_ITU_FILE");
            this.closeGridCharData = Bookham.Toolkit.CloseGrid.CsvDataLookup.GetItuOperatingPoints(closeGridCharFileName);
            //this.SmFiles = this._mapResult.ReadStringArray("SM_FILES");
            
            // Alice.Huang    2010-05-18
            // Commented for we don't need to get DSDBR Section voltage by check a table 
            // that mapping the current with Votage on this section any more for ILMZ

            //// Section IV curve data
            //this.SectionIVCurveData = new List<SectionIVSweepData>();
            //string sectionIVCurveFilename = this._mapResult.ReadFileLinkFullPath("SECTION_IVTEST_RESULTS_FILE");
            //// read section IV curve data from file
            //using (StreamReader reader = new StreamReader(sectionIVCurveFilename))
            //{
            //    while (!reader.EndOfStream)
            //    {
            //        string[] IDataLine = reader.ReadLine().Split(',');
            //        string[] VDataLine = reader.ReadLine().Split(',');
            //        if (IDataLine.Length > 0 && VDataLine.Length > 0)
            //        {
            //            string sectionNameInfo = IDataLine[0]; // "I"+sectionName+"_mA"
            //            foreach (string sectionName in Enum.GetNames(typeof(DSDBRSection)))
            //            {
            //                if (sectionNameInfo.ToLower().Contains(sectionName.ToLower()))
            //                {
            //                    sectionNameInfo = sectionName;
            //                    break;
            //                }
            //            }

            //            // get section
            //            DSDBRSection section = (DSDBRSection)Enum.Parse(typeof(DSDBRSection), sectionNameInfo);
            //            double[] Idata = new double[IDataLine.Length - 1];
            //            double[] Vdata = new double[VDataLine.Length - 1];
            //            for (int ii = 1; ii < IDataLine.Length; ii++)
            //            {
            //                Idata[ii - 1] = double.Parse(IDataLine[ii]);
            //                Vdata[ii - 1] = double.Parse(VDataLine[ii]);
            //            }
            //            SectionIVSweepData sweepData = new SectionIVSweepData(section, Idata, Vdata);
            //            //sweepData.PolynomialFit(5); // Fit data
            //            this.SectionIVCurveData.Add(sweepData);
            //        }
            //    }
            //}

        }

        #region Initialise modules
        /// <summary>
        /// Configure Initialise Device module
        /// </summary>
        private void IlmzInitialise_InitModule(ITestEngineInit engine, DUTObject dutObject)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("IlmzInitialise", "IlmzInitialise", "");

            // Add instruments
            //modRun.Instrs.Add("Osa", (Instrument)progInfo.Instrs.Osa);
            modRun.ConfigData.AddReference("MzInstruments", progInfo.Instrs.Mz);
            switch (progInfo.DsdbrInstrsUsed)
            {
                // added FCU2ASIC Option by Alice.Huang, for TOSA GB test     2010-02-01
                case DsdbrDriveInstruments.FCU2AsicInstrument:
                    modRun.ConfigData.AddReference("FCU2AsicInstruments", 
                                            progInfo.Instrs.Fcu2AsicInstrsGroups);
                    break;
                case DsdbrDriveInstruments.FCUInstruments:
                    modRun.ConfigData.AddReference("FcuInstruments", progInfo.Instrs.Fcu);
                    break;                
            }

            // Add config data
            TcmzInitialise_Config tcmzInitConfig = new TcmzInitialise_Config();
            DatumList datumsRequired = tcmzInitConfig.GetDatumList();
            ExtractAndAddDatums(modRun, datumsRequired);
            modRun.ConfigData.AddSint32("NumChans", progInfo.TestConditions.NbrChannels);
            modRun.ConfigData.AddDouble("FreqLow_GHz", progInfo.TestConditions.ItuLowFreq_GHz);
            modRun.ConfigData.AddDouble("FreqHigh_GHz", progInfo.TestConditions.ItuHighFreq_GHz);
            modRun.ConfigData.AddDouble("FreqSpace_GHz", progInfo.TestConditions.ItuSpacing_GHz);
            modRun.ConfigData.AddString("DutSerialNbr", dutObject.SerialNumber);
            modRun.ConfigData.AddString("MzFileDirectory", MzFileDirectory);
            modRun.ConfigData.AddEnum("DsdbrInstrsGroup", progInfo.DsdbrInstrsUsed);
            
        }

        private string GetCurrentTimeStamp()
        {
            if (TestTimeStamp == null)
            {
                System.DateTime currentTime = new System.DateTime();
                currentTime = System.DateTime.Now;

                //assume the year is four digital number
                string year = currentTime.Year.ToString();
                string month = currentTime.Month > 9 ? currentTime.Month.ToString() : "0" + currentTime.Month.ToString();
                string day = currentTime.Day > 9 ? currentTime.Day.ToString() : "0" + currentTime.Day.ToString();
                string hours = currentTime.Hour > 9 ? currentTime.Hour.ToString() : "0" + currentTime.Hour.ToString();
                string minute = currentTime.Minute > 9 ? currentTime.Minute.ToString() : "0" + currentTime.Minute.ToString();
                string second = currentTime.Second > 9 ? currentTime.Second.ToString() : "0" + currentTime.Second.ToString();
                TestTimeStamp = string.Format("{0}{1}{2}{3}{4}{5}",
                    year, month, day,
                    hours, minute, second);
            }
            return TestTimeStamp;
        }

        /// <summary>
        /// Configure a Device Temperature Control module
        /// </summary>
        private void DeviceTempControl_InitModule(ITestEngineInit engine, string moduleName, 
            IInstType_SimpleTempControl tecController, string configPrefix)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun(moduleName, "SimpleTempControl", "");

            // Add instrument
            modRun.Instrs.Add("Controller", (Instrument)tecController);

            //load config
            double timeout_S = this.progInfo.TestParamsConfig.GetDoubleParam(configPrefix + "_Timeout_S");
            double stabTime_S = this.progInfo.TestParamsConfig.GetDoubleParam(configPrefix + "_StabiliseTime_S");
            int updateTime_mS = this.progInfo.TestParamsConfig.GetIntParam(string.Format("{0}_UpdateTime_mS", configPrefix));
            double sensorSetPoint_C = this.progInfo.TestParamsConfig.GetDoubleParam(string.Format("{0}_SensorTemperatureSetPoint_C", configPrefix));
            double tolerance_C = this.progInfo.TestParamsConfig.GetDoubleParam(string.Format("{0}_Tolerance_C", configPrefix));

            // Add config data
            DatumList tempConfig = new DatumList();
            tempConfig.AddDouble("datumMaxExpectedTimeForOperation_s", timeout_S);
            tempConfig.AddDouble("RqdStabilisationTime_s", stabTime_S);
            tempConfig.AddSint32("TempTimeBtwReadings_ms", updateTime_mS);
            tempConfig.AddDouble("SetPointTemperature_C", sensorSetPoint_C);
            tempConfig.AddDouble("TemperatureTolerance_C", tolerance_C);

            modRun.ConfigData.AddListItems(tempConfig);
        }

        /// <summary>
        /// Configure Case Temperature module
        /// </summary>
        private void CaseTempControl_InitModule(ITestEngineInit engine,
            string moduleName, Double currentTemp, Double tempSetPoint)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun(moduleName, "SimpleTempControl", "");

            // Add instrument
            modRun.Instrs.Add("Controller", (Instrument)progInfo.Instrs.TecCase);

            // Add config data
            TempTablePoint tempCal = progInfo.TempConfig.GetTemperatureCal(currentTemp, tempSetPoint)[0];
            DatumList tempConfig = new DatumList();
            int guiTimeOut_ms = this.progInfo.TestParamsConfig.GetIntParam("CaseTempGui_UpdateTime_mS");
            // Timeout = 2 * calibrated time, minimum of 60secs
            double timeout_s = Math.Max(tempCal.DelayTime_s * 15, 60);
            tempConfig.AddDouble("datumMaxExpectedTimeForOperation_s", timeout_s);
            // Stabilisation time = 0.5 * calibrated time
            tempConfig.AddDouble("RqdStabilisationTime_s", tempCal.DelayTime_s);
            tempConfig.AddSint32("TempTimeBtwReadings_ms", guiTimeOut_ms);
            // Actual temperature to set
            tempConfig.AddDouble("SetPointTemperature_C", tempSetPoint + tempCal.OffsetC);
            tempConfig.AddDouble("TemperatureTolerance_C", tempCal.Tolerance_degC);
            modRun.ConfigData.AddListItems(tempConfig);
        }

        /// <summary>
        /// Initialise IlmzPowerUp module
        /// </summary>
        /// <param name="engine"></param>
        private void IlmzPowerUp_InitModule(ITestEngineInit engine, InstrumentCollection instrs)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("IlmzPowerUp", "IlmzPowerUp", "");

            // Add instruments
            modRun.Instrs.Add("OpticalPowerMeter", (Instrument)progInfo.Instrs.Mz.PowerMeter);
            modRun.Instrs.Add(instrs);
            modRun.ConfigData.AddReference("MzInstruments", progInfo.Instrs.Mz);

            // Add config data
            modRun.ConfigData.AddDouble("MzTapBias_V", progInfo.TestParamsConfig.GetDoubleParam("MzTapBias_V"));
            modRun.Limits.AddParameter(progInfo.MainSpec, "MZ_MOD_LEFT_DARK_I", "MzLeftModDarkCurrent_mA");
            modRun.Limits.AddParameter(progInfo.MainSpec, "MZ_MOD_RIGHT_DARK_I", "MzRightModDarkCurrent_mA");
            modRun.Limits.AddParameter(progInfo.MainSpec, "TAP_COMP_DARK_I", "MzTapCompDarkCurrent_mA");
            modRun.Limits.AddParameter(progInfo.MainSpec, "ITX_DARK", "TxLockerDarkCurrent_mA");
            modRun.Limits.AddParameter(progInfo.MainSpec, "IRX_DARK", "RxLockerDarkCurrent_mA");

        }

        /// <summary>
        /// Initialise IlmzCrossCalibration module
        /// </summary>
        /// <param name="engine"></param>
        private void IlmzCrossCalibration_InitModule(ITestEngineInit engine,InstrumentCollection instrs)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("IlmzCrossCalibration", "TcmzCrossCalibration", "");
            modRun.ConfigData.AddDouble("FreqDivTolerance_GHz", Convert.ToDouble(
                progInfo.MainSpec.GetParamLimit("XCAL_CH_MID_MAP_GB").HighLimit.ValueToString()));
            // Add instruments

            // Add config data
            modRun.ConfigData.AddSint32("OpticalCal_delay_ms", 2000);
            modRun.ConfigData.AddBool("IsRunThisModule", !doMapping);

            modRun.Instrs.Add("Optical_switch", (Instrument)progInfo.Instrs.TecDsdbr);
            modRun.Instrs.Add(instrs);

            // Tie up to limits
            modRun.Limits.AddParameter(progInfo.MainSpec, "XCAL_CH_LO_MAP_GB", "XCAL_CH_LO");
            modRun.Limits.AddParameter(progInfo.MainSpec, "XCAL_CH_MID_MAP_GB", "XCAL_CH_MID");
            modRun.Limits.AddParameter(progInfo.MainSpec, "XCAL_CH_HI_MAP_GB", "XCAL_CH_HI");
        }

        /// <summary>
        /// Initialise IlmzPowerHeadCal module
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutobject"></param>
        private void IlmzPowerHeadCal_InitModule(ITestEngineInit engine,DUTObject dutobject)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("IlmzPowerHeadCal", "TcmzPowerHeadCal", "");

            // Add instruments
            modRun.Instrs.Add("OpmReference", (Instrument)progInfo.Instrs.OpmReference);
            modRun.Instrs.Add("Wavemeter", (Instrument)progInfo.Instrs.Wavemeter);
            modRun.Instrs.Add("OpmMZ", (Instrument)progInfo.Instrs.Mz.PowerMeter);
            switch (progInfo.DsdbrInstrsUsed)
            {
                // added FCU2ASIC Option by Alice.Huang, for TOSA GB test     2010-02-01
                case DsdbrDriveInstruments .FCU2AsicInstrument :
                case DsdbrDriveInstruments.FCUInstruments:
                    break;
                case DsdbrDriveInstruments.PXIInstruments:
                    modRun.Instrs.Add("OpmCgDirect", (Instrument)progInfo.Instrs.OpmCgDirect);
                    modRun.Instrs.Add("OpmCgFilter", (Instrument)progInfo.Instrs.OpmCgFilter);
                    break;
            }

            // Add any config data here ...
            modRun.ConfigData.AddEnum("TestStage", progInfo.TestStage);
            modRun.ConfigData.AddSint32("OpticalCal_delay_ms", 2000);
            modRun.ConfigData.AddDouble("OpticalCal_MZ_Max", progInfo.TestParamsConfig.GetDoubleParam("OpticalCal_MZ_Max"));
            modRun.ConfigData.AddDouble("OpticalCal_MZ_Min", progInfo.TestParamsConfig.GetDoubleParam("OpticalCal_MZ_Min"));
            modRun.ConfigData.AddString("PlotFileDirectory", MzFileDirectory);
            modRun.ConfigData.AddString("SN", dutobject.SerialNumber);
            switch (progInfo.DsdbrInstrsUsed)
            {
                // added FCU2ASIC Option by Alice.Huang, for TOSA GB test     2010-02-01
                case DsdbrDriveInstruments .FCU2AsicInstrument : 

                case DsdbrDriveInstruments.FCUInstruments:
                    modRun.ConfigData.AddBool("IsCalPXIT", false);
                    break;
                case DsdbrDriveInstruments.PXIInstruments:
                    modRun.ConfigData.AddBool("IsCalPXIT", true);
                    modRun.ConfigData.AddDouble("OpticalCal_CgDirect_Max", progInfo.TestParamsConfig.GetDoubleParam("OpticalCal_CgDirect_Max"));
                    modRun.ConfigData.AddDouble("OpticalCal_CgDirect_Min", progInfo.TestParamsConfig.GetDoubleParam("OpticalCal_CgDirect_Min"));
                    break;
            }
        }

        /// <summary>
        /// Initialise IlmzMzCharacterise module
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        private void IlmzMzCharacterise_InitModule(ITestEngineInit engine, DUTObject dutObject,InstrumentCollection instrs)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("IlmzMzCharacterise", "IlmzMzCharacterise_ND", "");

            // Add instruments
            modRun.ConfigData.AddReference("MzInstruments", progInfo.Instrs.Mz);

            modRun.Instrs.Add("FCU", (Instrument)(Inst_Fcu2Asic)instrs["Fcu2Asic"]);
            modRun.Instrs.Add("Optical_switch", (Instrument)progInfo.Instrs.TecDsdbr);
            

            TcmzMzSweep_Config tcmzConfig = new TcmzMzSweep_Config();
            DatumList datumsRequired = tcmzConfig.GetDatumList();
            ExtractAndAddDatums(modRun, datumsRequired);

            double mzBias1 = progInfo.TestParamsConfig.GetDoubleParam("MzFixedModBias_1_V");
            double mzBias2 = progInfo.TestParamsConfig.GetDoubleParam("MzFixedModBias_2_V");

            //START MODIFICATION, 2007.09.26, BY Ken.Wu ( TCMZ Vpi Correction )
            // Read values needed for TCMZ Vpi calculation from config file.
            double tuneISOAPowerSlope
                = progInfo.TestParamsConfig.GetDoubleParam(externalKeyTuneISOAPowerSlope);

            double tuneOpticalPowerToleranceInmW
                = ((DatumDouble )(progInfo .MainSpec .GetParamLimit(externalKeyTuneOpticalPowerToleranceInmW).HighLimit)).Value ;

            double tuneOpticalPowerToleranceIndB
                = progInfo.TestParamsConfig.GetDoubleParam(externalKeyTuneOpticalPowerToleranceIndB);

            //Echoxl.wang 2011-02-18
            //add locker pot value to ensure we can get correct ctap value;
            modRun.ConfigData.AddDouble("locker_tran_pot", locker_pot_value);
            modRun.ConfigData.AddDoubleArray("MZFixedModBias_volt", new double[] { mzBias1, mzBias2 });
            // Alice.Huang   2010-04-27
            // add SOA Current value for convenience
            //modRun.ConfigData.AddDouble("SoaSetupCurrent_mA", 
            //                progInfo.TestParamsConfig.GetDoubleParam("MzChrSetupSoa_mA"));
            modRun.ConfigData.AddDouble("TuneISOAPowerSlope", tuneISOAPowerSlope);
            modRun.ConfigData.AddDouble("TuneOpticalPowerToleranceInmW", tuneOpticalPowerToleranceInmW);
            modRun.ConfigData.AddDouble("TargetFibrePower_dBm", 
                            progInfo.TestConditions.TargetFibrePower_dBm);
            modRun.ConfigData.AddDouble("PowerlevelingOffset_dB", 
                 ((DatumDouble ) progInfo .MainSpec .GetParamLimit ("TC_MZ_PWR_CHR_OFFSET").HighLimit ).Value);
            //END MODIFICATION, 2007.09.26, By Ken.Wu
            modRun.ConfigData.AddDouble("VcmFactor_FirstChan", progInfo.TestParamsConfig.GetDoubleParam("MzVcmFactor_FirstChan_ForMzChar"));
            modRun.ConfigData.AddDouble("VcmFactor_LastChan", progInfo.TestParamsConfig.GetDoubleParam("MzVcmFactor_LastChan_ForMzChar"));
            modRun.ConfigData.AddSint32("NumberOfPoints", progInfo.TestParamsConfig.GetIntParam("MzSweepNumberOfPoints"));
            modRun.ConfigData.AddString("DutSerialNbr", dutObject.SerialNumber);
            modRun.ConfigData.AddReference("TestSpec", progInfo.MainSpec);
            modRun.ConfigData.AddString("ResultDir", this.MzFileDirectory);
            modRun.ConfigData.AddDouble("LeftBias_V_LI_Sweep", progInfo.TestParamsConfig.GetDoubleParam("LeftBias_V_LI_Sweep"));
            modRun.ConfigData.AddDouble("RightBias_V_LI_Sweep", progInfo.TestParamsConfig.GetDoubleParam("RightBias_V_LI_Sweep"));
//#warning Add MZTapBias_V to config class
            modRun.ConfigData.AddDouble("MZInlineTapBias_V", modRun.ConfigData.ReadDouble("MZTapBias_V"));
            modRun.ConfigData.AddDouble("MzFixedModBias_volt_firstChan", progInfo.TestParamsConfig.GetDoubleParam("MzFixedModBias_volt_FirstChan"));//-4
            modRun.ConfigData.AddDouble("MzFixedModBias_volt_lastChan", progInfo.TestParamsConfig.GetDoubleParam("MzFixedModBias_volt_LastChan"));//-3
            modRun.ConfigData.AddDouble("Vmax_firstChan", progInfo.TestParamsConfig.GetDoubleParam("MzVcmFactor_Vmax"));//-1
            modRun.ConfigData.AddDouble("Vmax_lastChan", progInfo.TestParamsConfig.GetDoubleParam("MzVcmFactor_LastChan_Vmax"));//-0.5

            // Limits
            modRun.Limits.AddParameter(progInfo.MainSpec, "MZ_INITIAL_CAL_SWEEP_FILE", "MzSweepDataZipFile");
            modRun.Limits.AddParameter(progInfo.MainSpec, "MZ_CAL_EST_FILE", "MzSweepDataResultsFile");
            modRun.Limits.AddParameter(progInfo.MainSpec, "MZ_CAL_EST_FLAG", "AllSweepsPassed");
        }

        /// <summary>
        /// Initialise IlmzMzItuEstimate module
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        private void IlmzMzItuEstimate_InitModule(ITestEngineInit engine, DUTObject dutObject)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("IlmzMzItuEstimate", "IlmzMzItuEstimate_ND", "");

            // Add instruments
            modRun.ConfigData.AddReference("MzInstruments", progInfo.Instrs.Mz);

            // Add config data
            modRun.Instrs.Add("Optical_switch", (Instrument)progInfo.Instrs.TecDsdbr);

            modRun.ConfigData.AddDouble("locker_tran_pot", locker_pot_value);
            modRun.ConfigData.AddReference("Specification", progInfo.MainSpec);
            modRun.ConfigData.AddDouble("MzTapBias_V", progInfo.TestParamsConfig.GetDoubleParam("MzTapBias_V"));
            modRun.ConfigData.AddDouble("MZInlineTapBias_V", progInfo.TestParamsConfig.GetDoubleParam("MzTapBias_V"));
            modRun.ConfigData.AddDouble("CalOffset", progInfo.TestConditions.CalOffset);
            modRun.ConfigData.AddDouble("HighTemp", progInfo.TestConditions.HighTemp);
            modRun.ConfigData.AddDouble("LowTemp", progInfo.TestConditions.LowTemp);
            modRun.ConfigData.AddDouble("FreqLow_GHz", progInfo.TestConditions.ItuLowFreq_GHz);
            modRun.ConfigData.AddDouble("FreqHigh_GHz", progInfo.TestConditions.ItuHighFreq_GHz);
            modRun.ConfigData.AddDouble("FreqSpace_GHz", progInfo.TestConditions.ItuSpacing_GHz);
            modRun.ConfigData.AddDouble("MzCtrlINominal_mA", progInfo.TestParamsConfig.GetDoubleParam("MzCtrlINominal_mA"));
            modRun.ConfigData.AddString("DUTSerialNbr", dutObject.SerialNumber);
            modRun.ConfigData.AddString("MzFileDirectory", MzFileDirectory);
            modRun.ConfigData.AddDouble("Vmax", progInfo.TestParamsConfig.GetDoubleParam("MzVcmFactor_Vmax"));
            modRun.ConfigData.AddDouble("Vmax_lastChan", progInfo.TestParamsConfig.GetDoubleParam("MzVcmFactor_LastChan_Vmax"));

            // for Vpi correction
            double VpiCorrectionUpperAdjustDelta = progInfo.TestParamsConfig.GetDoubleParam("VpiCorrectionUpperAdjustDelta");
            double VpiCorrectionLowerAdjustDelta = progInfo.TestParamsConfig.GetDoubleParam("VpiCorrectionLowerAdjustDelta");
            modRun.ConfigData.AddDouble("VpiCorrectionUpperAdjustDelta", VpiCorrectionUpperAdjustDelta);
            modRun.ConfigData.AddDouble("VpiCorrectionLowerAdjustDelta", VpiCorrectionLowerAdjustDelta);
            modRun.ConfigData.AddSint32("NumberOfPoints", progInfo.TestParamsConfig.GetIntParam("MzSweepNumberOfPoints"));
            modRun.ConfigData.AddString("DutSerialNbr", dutObject.SerialNumber);
            // Add limits
            modRun.Limits.AddParameter(progInfo.MainSpec, "MZ_CAL_EST_FILE_FINAL_VCM", "MzAtNewVcmResultsFile");
            modRun.Limits.AddParameter(progInfo.MainSpec, "MZ_CAL_EST_FLAG_FINAL_VCM", "MzAtNewVcmPass");
        }

        /// <summary>
        /// Configure Channel Characterisation module
        /// </summary>
        private void IlmzChannelChar_InitModule(ITestEngineInit engine, DUTObject dutObject)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("IlmzChannelChar", "IlmzChannelChar_ND", "");
            modRun.PreviousTestData.AddString("LASER_WAFER_SIZE", LASER_WAFER_SIZE == null ? string.Empty : LASER_WAFER_SIZE);
            //modRun.PreviousTestData.AddStringArray("SmFiles", SmFiles);
            modRun.PreviousTestData.AddString("BandType", progInfo.TestConditions.FreqBand.ToString());
            //modRun.PreviousTestData.AddStringArray("NUM_LM", NUM_LM);

            // Add instruments
            modRun.ConfigData.AddReference("MzInstruments", progInfo.Instrs.Mz);

            modRun.Instrs.Add("Optical_switch", (Instrument)progInfo.Instrs.TecDsdbr);
            modRun.ConfigData.AddDouble("locker_tran_pot",locker_pot_value);
            modRun.Instrs.Add("OSA", (Instrument)progInfo.Instrs.Osa);
            // modRun.Instrs.Add("MzTec", (Instrument)progInfo.Instrs.TecMz);
            modRun.Instrs.Add("DsdbrTec", (Instrument)progInfo.Instrs.TecDsdbr);
            
            switch (progInfo.DsdbrInstrsUsed)
            {
                // added FCU2ASIC Option by Alice.Huang, for TOSA GB test     2010-02-01
                case DsdbrDriveInstruments .FCU2AsicInstrument :
                case DsdbrDriveInstruments.FCUInstruments:
                    modRun.Instrs.Add("PowerHead", (Instrument)progInfo.Instrs.Mz.PowerMeter);
                    break;
                case DsdbrDriveInstruments.PXIInstruments:
                    modRun.Instrs.Add("PowerHead", (Instrument)progInfo.Instrs.OpmCgDirect);
                    break;
            }

            // Add config data
            TcmzChannelChar_ND_Config tcmzChanCharConfig = new TcmzChannelChar_ND_Config();
            DatumList datumsRequired = tcmzChanCharConfig.GetDatumList();
            ExtractAndAddDatums(modRun, datumsRequired);

            TcmzMzSweep_Config tcmzConfig = new TcmzMzSweep_Config();
            datumsRequired = tcmzConfig.GetDatumList();
            ExtractAndAddDatums(modRun, datumsRequired);

            //modRun.ConfigData.AddReference("SwitchOsaMzOpm", progInfo.Instrs.SwitchOsaMzOpm);
            modRun.ConfigData.AddString("ResultDir", this.MzFileDirectory);
            modRun.ConfigData.AddReference("TestSelect", progInfo.TestSelect);
            modRun.ConfigData.AddReference("Specification", progInfo.MainSpec);
            modRun.ConfigData.AddReference("FreqBand", progInfo.TestConditions.FreqBand);
            modRun.ConfigData.AddDouble("FreqLow_GHz", progInfo.TestConditions.ItuLowFreq_GHz);
            modRun.ConfigData.AddDouble("FreqHigh_GHz", progInfo.TestConditions.ItuHighFreq_GHz);
            modRun.ConfigData.AddDouble("FreqSpace_GHz", progInfo.TestConditions.ItuSpacing_GHz);
            modRun.ConfigData.AddSint32("NumChans", progInfo.TestConditions.NbrChannels);
            modRun.ConfigData.AddDouble("CaseTempLow_C", progInfo.TestConditions.LowTemp);
            modRun.ConfigData.AddDouble("CaseTempHigh_C", progInfo.TestConditions.HighTemp);
            modRun.ConfigData.AddDouble("TargetFibrePower_dBm", progInfo.TestConditions.TargetFibrePower_dBm);
            modRun .ConfigData .AddDouble("Tune_OpticalPowerTolerance_mW",
                ((DatumDouble)progInfo.MainSpec.GetParamLimit(externalKeyTuneOpticalPowerToleranceInmW).HighLimit).Value);
            modRun.ConfigData.AddDouble("CalOffset", progInfo.TestConditions.CalOffset);
            modRun.ConfigData.AddString("DutSerialNbr", dutObject.SerialNumber);
            modRun.ConfigData.AddString("DeviceType", dutObject.PartCode);
            modRun.ConfigData.AddDouble("MzDcVpiSlopeLimitMin", progInfo.TestConditions.MzDcVpiSlopeLimitMin);
            modRun.ConfigData.AddDouble("MzDcVpiSlopeLimitMax", progInfo.TestConditions.MzDcVpiSlopeLimitMax);
            modRun.ConfigData.AddDouble("LockerSlopeEffAbsLimitMin", progInfo.TestConditions.LockerSlopeEffAbsLimitMin);
            modRun.ConfigData.AddDouble("LockerSlopeEffAbsLimitMax", progInfo.TestConditions.LockerSlopeEffAbsLimitMax);
            try
            {
                modRun.ConfigData.AddDouble("LockerSlope_FreqOffset_GHz",
                    progInfo.TestParamsConfig.GetDoubleParam("LockerSlope_FreqOffset_GHz"));
            }
            catch { }

            modRun.ConfigData.AddDouble("MaxSoaForPwrLeveling", Convert.ToDouble(
                progInfo.MainSpec.GetParamLimit("CH_ITU_SOA_I").HighLimit.ValueToString()));
            bool loggingEnabled = true;
            try
            {
                loggingEnabled = this.progInfo.TestParamsConfig.GetBoolParam("LoggintEnabled");
            }
            catch (Exception /*e*/ ) { }
            modRun.ConfigData.AddBool("LoggingEnabled", loggingEnabled);

#warning Add to config
            modRun.ConfigData.AddBool("AlwaysTuneLaser", true);
            //modRun.ConfigData.AddString("IPhaseModAcqFilesCollection", this._mapResult.ReadFileLinkFullPath("CG_PHASE_MODE_ACQ_FILE"));

            // Tie up limits
            Specification spec = progInfo.MainSpec;
            modRun.Limits.AddParameter(spec, "TCASE_MID_PASS_FAIL_FLAG", "PassFailFlag");
            modRun.Limits.AddParameter(spec, "NUM_CHAN_TEST_TCASE_MID", "NumChannelsTested");
            modRun.Limits.AddParameter(spec, "NUM_CHAN_PASS_TCASE_MID", "NumChannelsPass");
            modRun.Limits.AddParameter(spec, "CHANNEL_RAW_DATA_FILE", "ChannelPlotData");
            modRun.Limits.AddParameter(spec, "FULL_LV_SWEEP_FLAG", "FullLvSweepFlag");
            modRun.Limits.AddParameter(spec, "FULL_LK_SLOPE_FLAG", "FullLkSlopeTest");
            //if (spec.ParamLimitExists("REF_MZ_RIGHT"))
            //{
            //    modRun.Limits.AddParameter(spec, "REF_MZ_RIGHT", "Power_Right_Peak");
            //}
            //modRun.Limits.AddParameter(spec, "REF_MZ_LEFT", "Power_Left_Peak");
        }

        /// <summary>
        /// Initialise IlmzLowTempTest module
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        private void IlmzLowTempTest_InitModule(ITestEngineInit engine, DUTObject dutObject)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("IlmzLowTempTest", "IlmzTempTest_ND", "");

            // Add instruments
            modRun.ConfigData.AddReference("MzInstruments", progInfo.Instrs.Mz);
            //modRun.Instrs.Add("MzTec", (Instrument)progInfo.Instrs.TecMz);
            modRun.Instrs.Add("DsdbrTec", (Instrument)progInfo.Instrs.TecDsdbr);
            modRun.Instrs.Add("Optical_switch", (Instrument)progInfo.Instrs.TecDsdbr);
            
            modRun.ConfigData.AddDouble("locker_tran_pot", locker_pot_value);
            //modRun.ConfigData.AddReference("SwitchOsaMzOpm", progInfo.Instrs.SwitchOsaMzOpm);

            // Add config data
            TcmzMzSweep_Config tcmzConfig = new TcmzMzSweep_Config();
            DatumList datumsRequired = tcmzConfig.GetDatumList();
            ExtractAndAddDatums(modRun, datumsRequired);
            modRun.ConfigData.AddEnum("Temp", TcmzTestTemp.Low);
            modRun.ConfigData.AddReference("TestSelect", progInfo.TestSelect);
            modRun.ConfigData.AddString("DutSerialNbr", dutObject.SerialNumber);
            //Modify non-rules old statment at 2008-10-10 by tim;
            //modRun.ConfigData.AddBool("DoPwrCtrlRangeTests", dutObject.PartCode.ToLower().Contains("lmt10ncc"));
            modRun.ConfigData.AddBool("DoPwrCtrlRangeTests", progInfo.TestParamsConfig.GetBoolParam("DoPwrCtrlRangeTests"));
            //Modify non-ruless old statment at 2008-10-10;
            //modRun.ConfigData.AddBool("DoTapLevelling", !dutObject.PartCode.ToLower().Contains("lmt10ncb"));
            modRun.ConfigData.AddBool("DoTapLevelling",progInfo.TestParamsConfig.GetBoolParam("DoTapLevelling"));
            modRun.ConfigData.AddDouble("TapPhotoCurrentToleranceRatio",
            progInfo.TestParamsConfig.GetDoubleParam("TapPhotoCurrentToleranceRatio"));
            //modRun.ConfigData.AddDouble("MZModSweepMax_volt", progInfo.TestParamsConfig.GetDoubleParam("MzSeModSweepMax_volt"));
            //modRun.ConfigData.AddDouble("MZFixedModBias_volt", progInfo.TestParamsConfig.GetDoubleParam("MzSeFixedModBias_volt"));
            modRun.ConfigData.AddDouble("PwrControlRangeLow", progInfo.TestConditions.PwrControlRangeLow);
            modRun.ConfigData.AddDouble("PwrControlRangeHigh", progInfo.TestConditions.PwrControlRangeHigh);
            modRun.ConfigData.AddDouble("Tune_OpticalPowerTolerance_dB", 
                progInfo.TestParamsConfig.GetDoubleParam("Tune_OpticalPowerTolerance_dB"));
            modRun.ConfigData.AddDouble("Tune_OpticalPowerTolerance_mW",
                ((DatumDouble)progInfo.MainSpec.GetParamLimit(externalKeyTuneOpticalPowerToleranceInmW).HighLimit).Value);
            modRun.ConfigData.AddDouble("Tune_IsoaPowerSlope", progInfo.TestParamsConfig.GetDoubleParam("Tune_IsoaPowerSlope"));
            modRun.ConfigData.AddDouble("MaxSoaForPwrLeveling", Convert.ToDouble(
                progInfo.MainSpec.GetParamLimit("CH_ITU_SOA_I").HighLimit.ValueToString()));
            bool loggingEnabled = true;
            try
            {
                loggingEnabled = this.progInfo.TestParamsConfig.GetBoolParam("LoggintEnabled");
            }
            catch (Exception /*e*/ ) { }
            modRun.ConfigData.AddBool("LoggingEnabled", loggingEnabled);
            modRun.ConfigData.AddBool("TraceToneTestEnabled", progInfo.TestParamsConfig.GetBoolParam("TraceToneTestEnabled"));

            // Tie up limits
            Specification spec = progInfo.MainSpec;
            modRun.Limits.AddParameter(spec, "LOW_DISS_OPTICAL_FREQ", "LOW_DISS_OPTICAL_FREQ");
            modRun.Limits.AddParameter(spec, "LOW_DISS_SOL", "LOW_DISS_SOL");
            modRun.Limits.AddParameter(spec, "PACK_DISS_TCASE_LOW", "PACK_DISS_TCASE_LOW");
            modRun.Limits.AddParameter(spec, "TEC_I_TCASE_LOW", "LASER_TEC_I_TCASE_LOW");
            modRun.Limits.AddParameter(spec, "TEC_V_TCASE_LOW", "LASER_TEC_V_TCASE_LOW");
            //modRun.Limits.AddParameter(spec, "MZ_TEC_I_TCASE_LOW", "MZ_TEC_I_TCASE_LOW");
            //modRun.Limits.AddParameter(spec, "MZ_TEC_V_TCASE_LOW", "MZ_TEC_V_TCASE_LOW");
            modRun.Limits.AddParameter(spec, "NUM_CHAN_TEST_TCASE_LOW", "NumChannelsTested");
            modRun.Limits.AddParameter(spec, "NUM_CHAN_PASS_TCASE_LOW", "NumChannelsPass");
            modRun.Limits.AddParameter(spec, "NUM_LOCK_FAIL_TCASE_LOW", "NumLockFails");
            modRun.Limits.AddParameter(spec, "TCASE_LOW_PASS_FAIL_FLAG", "PassFailFlag");
        }

        /// <summary>
        /// Initialise TcmHighTempTest module
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        private void IlmzHighTempTest_InitModule(ITestEngineInit engine, DUTObject dutObject)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("IlmzHighTempTest", "IlmzTempTest_ND", "");

            // Add instruments
            modRun.ConfigData.AddReference("MzInstruments", progInfo.Instrs.Mz);
            //modRun.Instrs.Add("MzTec", (Instrument)progInfo.Instrs.TecMz);
            modRun.Instrs.Add("DsdbrTec", (Instrument)progInfo.Instrs.TecDsdbr);
            modRun.Instrs.Add("Optical_switch", (Instrument)progInfo.Instrs.TecDsdbr);
           
            modRun.ConfigData.AddDouble("locker_tran_pot", locker_pot_value);
            //modRun.ConfigData.AddReference("SwitchOsaMzOpm", progInfo.Instrs.SwitchOsaMzOpm);

            // Add config data
            TcmzMzSweep_Config tcmzConfig = new TcmzMzSweep_Config();
            DatumList datumsRequired = tcmzConfig.GetDatumList();
            ExtractAndAddDatums(modRun, datumsRequired);
            modRun.ConfigData.AddEnum("Temp", TcmzTestTemp.High);
            modRun.ConfigData.AddReference("TestSelect", progInfo.TestSelect);
            modRun.ConfigData.AddString("DutSerialNbr", dutObject.SerialNumber);
            //modRun.ConfigData.AddBool("DoPwrCtrlRangeTests", dutObject.PartCode.ToLower().Contains("lmt10ncc"));
            modRun.ConfigData.AddBool("DoPwrCtrlRangeTests", progInfo.TestParamsConfig.GetBoolParam("DoPwrCtrlRangeTests"));
            //Modify non-rules old statment;
            //modRun.ConfigData.AddBool("DoTapLevelling", !dutObject.PartCode.ToLower().Contains("lmt10ncb"));
            modRun.ConfigData.AddBool("DoTapLevelling",progInfo.TestParamsConfig.GetBoolParam("DoTapLevelling"));
            modRun.ConfigData.AddDouble("TapPhotoCurrentToleranceRatio",
                progInfo.TestParamsConfig.GetDoubleParam("TapPhotoCurrentToleranceRatio"));
            modRun.ConfigData.AddDouble("LaserTecEolDeltaT_C", progInfo.TestConditions.LaserTecEolDeltaT_C);
            //modRun.ConfigData.AddDouble("MzTecEolDeltaT_C", progInfo.TestConditions.MzTecEolDeltaT_C);
            modRun.ConfigData.AddDouble("PwrControlRangeLow", Convert.ToDouble(progInfo.MainSpec.GetParamLimit("TC_PWR_CTRL_RANGE_MIN").LowLimit.ValueToString()));
            modRun.ConfigData.AddDouble("PwrControlRangeHigh", Convert.ToDouble(progInfo.MainSpec.GetParamLimit("TC_PWR_CTRL_RANGE_MAX").LowLimit.ValueToString()));
            modRun.ConfigData.AddDouble("Tune_OpticalPowerTolerance_dB", 
                progInfo.TestParamsConfig.GetDoubleParam("Tune_OpticalPowerTolerance_dB"));
            modRun.ConfigData.AddDouble("Tune_OpticalPowerTolerance_mW",
                ((DatumDouble)progInfo.MainSpec.GetParamLimit(externalKeyTuneOpticalPowerToleranceInmW).HighLimit).Value);
            modRun.ConfigData.AddDouble("Tune_IsoaPowerSlope", 
                progInfo.TestParamsConfig.GetDoubleParam("Tune_IsoaPowerSlope"));
            modRun.ConfigData.AddDouble("MaxSoaForPwrLeveling", Convert.ToDouble(
                progInfo.MainSpec.GetParamLimit("CH_ITU_SOA_I").HighLimit.ValueToString()));
            bool loggingEnabled = true;
            try
            {
                loggingEnabled = this.progInfo.TestParamsConfig.GetBoolParam("LoggintEnabled");
            }
            catch (Exception /*e*/ ) { }
            modRun.ConfigData.AddBool("LoggingEnabled", loggingEnabled);
            modRun.ConfigData.AddBool("TraceToneTestEnabled", progInfo.TestParamsConfig.GetBoolParam("TraceToneTestEnabled"));

            // Tie up limits
            Specification spec = progInfo.MainSpec;
            modRun.Limits.AddParameter(spec, "HIGH_DISS_OPTICAL_FREQ", "HIGH_DISS_OPTICAL_FREQ");
            modRun.Limits.AddParameter(spec, "HIGH_DISS_SOL", "HIGH_DISS_SOL");
            modRun.Limits.AddParameter(spec, "PACK_DISS_TCASE_HIGH", "PACK_DISS_TCASE_HIGH");
            modRun.Limits.AddParameter(spec, "TEC_I_TCASE_HIGH", "LASER_TEC_I_TCASE_HIGH");
            modRun.Limits.AddParameter(spec, "TEC_V_TCASE_HIGH", "LASER_TEC_V_TCASE_HIGH");
            //modRun.Limits.AddParameter(spec, "MZ_TEC_I_TCASE_HIGH", "MZ_TEC_I_TCASE_HIGH");
            //modRun.Limits.AddParameter(spec, "MZ_TEC_V_TCASE_HIGH", "MZ_TEC_V_TCASE_HIGH");
            modRun.Limits.AddParameter(spec, "TEC_I_TCASE_HIGH_EOL", "LASER_TEC_I_TCASE_HIGH_EOL");
            modRun.Limits.AddParameter(spec, "TEC_V_TCASE_HIGH_EOL", "LASER_TEC_V_TCASE_HIGH_EOL");
            //modRun.Limits.AddParameter(spec, "MZ_TEC_I_TCASE_HIGH_EOL", "MZ_TEC_I_TCASE_HIGH_EOL");
            //modRun.Limits.AddParameter(spec, "MZ_TEC_V_TCASE_HIGH_EOL", "MZ_TEC_V_TCASE_HIGH_EOL");
            modRun.Limits.AddParameter(spec, "PACK_DISS_TCASE_HIGH_EOL", "PACK_DISS_TCASE_HIGH_EOL");
            modRun.Limits.AddParameter(spec, "NUM_CHAN_TEST_TCASE_HIGH", "NumChannelsTested");
            modRun.Limits.AddParameter(spec, "NUM_CHAN_PASS_TCASE_HIGH", "NumChannelsPass");
            modRun.Limits.AddParameter(spec, "NUM_LOCK_FAIL_TCASE_HIGH", "NumLockFails");
            modRun.Limits.AddParameter(spec, "TCASE_HIGH_PASS_FAIL_FLAG", "PassFailFlag");
        }

        /// <summary>
        /// Initialise TcmzTraceToneTest module
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        private void TcmzTraceToneTest_InitModule(ITestEngineInit engine, DUTObject dutObject)
        {
            // Initialise module
            ModuleRun modRun = engine.AddModuleRun("TcmzTraceToneTest", "TcmzTraceToneTest", "");
            modRun.ConfigData.AddString("DutSerialNumber", dutObject.SerialNumber);
            modRun.ConfigData.AddString("PlotFileDirectory", MzFileDirectory);

            // Tie up limits when added to spec
            modRun.Limits.AddParameter(progInfo.MainSpec, "I_SOA_1", "I_Soa_1");
            modRun.Limits.AddParameter(progInfo.MainSpec, "MAX_I_SOA_TRACE_SOLOT", "Max_I_Soa_Trace_SOLOT");
            modRun.Limits.AddParameter(progInfo.MainSpec, "MAX_TRACE_I_SOLOT", "Max_Trace_I_SOLOT");
            modRun.Limits.AddParameter(progInfo.MainSpec, "SOA_GRADIENT_SOLOT", "Soa_Gradient_SOLOT");
            modRun.Limits.AddParameter(progInfo.MainSpec, "I_SOA_TRACE_EOLOT1", "I_Soa_Trace_EOLOT1");
            modRun.Limits.AddParameter(progInfo.MainSpec, "TRACE_I_EOLOT1", "Trace_I_EOLOT1");
            modRun.Limits.AddParameter(progInfo.MainSpec, "SOA_GRADIENT_EOLOT1", "Soa_Gradient_EOLOT1");
            modRun.Limits.AddParameter(progInfo.MainSpec, "I_SOA_TRACE_EOLOT2", "I_Soa_Trace_EOLOT2");
            modRun.Limits.AddParameter(progInfo.MainSpec, "TRACE_I_EOLOT2", "Trace_I_EOLOT2");
            modRun.Limits.AddParameter(progInfo.MainSpec, "SOA_GRADIENT_EOLOT2", "Soa_Gradient_EOLOT2");
            modRun.Limits.AddParameter(progInfo.MainSpec, "TRACE_SOA_PLOT", "TraceSoaCurves");
        }

        /// <summary>
        /// Initialise TcmzGroupResults module
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        private void IlmzGroupResults_InitModule(ITestEngineInit engine, DUTObject dutObject)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("IlmzGroupResults", "IlmzGroupResults", "");

            // Add config data
            modRun.ConfigData.AddBool("LowTempTestEnabled", progInfo.TestParamsConfig.GetBoolParam("LowTempTestEnabled"));

            // Tie up limits
            Specification spec = progInfo.MainSpec;
            modRun.Limits.AddParameter(spec, "AVERAGE_LOCK_RATIO", "AVERAGE_LOCK_RATIO");
            modRun.Limits.AddParameter(spec, "OPTICAL_FREQ_FIRST_CHAN_FAIL", "OPTICAL_FREQ_FIRST_CHAN_FAIL");
            modRun.Limits.AddParameter(spec, "FULL_PASS_CHAN_COUNT_OVERALL", "FULL_PASS_CHAN_COUNT_OVERALL");

            modRun.Limits.AddParameter(spec, "FIBRE_POWER_CHAN_TEMP_MIN", "FIBRE_POWER_CHAN_TEMP_MIN");
            modRun.Limits.AddParameter(spec, "FIBRE_POWER_CHAN_TEMP_MAX", "FIBRE_POWER_CHAN_TEMP_MAX");
            modRun.Limits.AddParameter(spec, "FIBRE_POWER_CHANGE_TCASE_HIGH", "FIBRE_POWER_CHANGE_TCASE_HIGH");
            modRun.Limits.AddParameter(spec, "FIBRE_POWER_FAIL_CHAN_TEMP", "FIBRE_POWER_FAIL_CHAN_TEMP");
            modRun.Limits.AddParameter(spec, "FIBRE_POWER_FAIL_OVER_TCASE", "FIBRE_POWER_FAIL_OVER_TCASE");
            modRun.Limits.AddParameter(spec, "TRACK_ERROR_PWR_TCASE", "TRACK_ERROR_PWR_TCASE");

            modRun.Limits.AddParameter(spec, "LOCK_FREQ_CHAN_TEMP_MIN", "LOCK_FREQ_CHAN_TEMP_MIN");
            modRun.Limits.AddParameter(spec, "LOCK_FREQ_CHAN_TEMP_MAX", "LOCK_FREQ_CHAN_TEMP_MAX");
            modRun.Limits.AddParameter(spec, "LOCK_FREQ_CHANGE_TCASE_HIGH", "LOCK_FREQ_CHANGE_TCASE_HIGH");
            modRun.Limits.AddParameter(spec, "LOCK_FREQ_FAIL_CHAN_TEMP", "LOCK_FREQ_FAIL_CHAN_TEMP");
            modRun.Limits.AddParameter(spec, "LOCK_FREQ_FAIL_OVER_TCASE", "LOCK_FREQ_FAIL_OVER_TCASE");
            modRun.Limits.AddParameter(spec, "TRACK_ERROR_FREQ_TCASE", "TRACK_ERROR_FREQ_TCASE");
            modRun.Limits.AddParameter(spec, "TRACK_ERROR_ULFREQ_TCASE", "TRACK_ERROR_ULFREQ_TCASE");

            modRun.Limits.AddParameter(spec, "PHASE_I_CHANGE_TCASE_HIGH", "PHASE_I_CHANGE_TCASE_HIGH");

            if (progInfo.TestParamsConfig.GetBoolParam("LowTempTestEnabled"))
            {
                modRun.Limits.AddParameter(spec, "FIBRE_POWER_CHANGE_TCASE_LOW", "FIBRE_POWER_CHANGE_TCASE_LOW");
                modRun.Limits.AddParameter(spec, "LOCK_FREQ_CHANGE_TCASE_LOW", "LOCK_FREQ_CHANGE_TCASE_LOW");
                modRun.Limits.AddParameter(spec, "PHASE_I_CHANGE_TCASE_LOW", "PHASE_I_CHANGE_TCASE_LOW");
            }
        }

        #endregion

        #region Configure instruments
        /// <summary>
        /// Configure a TEC controller using config data
        /// </summary>
        /// <param name="tecCtlr">Instrument reference</param>
        /// <param name="tecCtlId">Config table data prefix</param>
        private void ConfigureTecController(IInstType_TecController tecCtlr, string tecCtlId)
        {
            SteinhartHartCoefficients stCoeffs = new SteinhartHartCoefficients();

            // Keithley 2510 specific commands (must be done before 'SetDefaultState')
            if (tecCtlr.DriverName.Contains("Ke2510"))
            {
                tecCtlr.HardwareData["MinTemperatureLimit_C"] = progInfo.TestParamsConfig.GetStringParam(tecCtlId + "_MinTemp_C");
                tecCtlr.HardwareData["MaxTemperatureLimit_C"] = progInfo.TestParamsConfig.GetStringParam(tecCtlId + "_MaxTemp_C");
                tecCtlr.Sensor_Type = (InstType_TecController.SensorType)
                Enum.Parse(typeof(InstType_TecController.SensorType), progInfo.TestParamsConfig.GetStringParam(tecCtlId + "_Sensor_Type"));
                tecCtlr.TecVoltageCompliance_volt = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_TecVoltageCompliance_volt");
                tecCtlr.DerivativeGain = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_DerivativeGain");

            }
            // Operating modes

            tecCtlr.SetDefaultState();
            tecCtlr.OperatingMode = (InstType_TecController.ControlMode)
                Enum.Parse(typeof(InstType_TecController.ControlMode), progInfo.TestParamsConfig.GetStringParam(tecCtlId + "_OperatingMode"));
            
            // Thermistor characteristics
            if (tecCtlr.Sensor_Type == InstType_TecController.SensorType.Thermistor_2wire ||
                tecCtlr.Sensor_Type == InstType_TecController.SensorType.Thermistor_4wire)
            {
                stCoeffs.A = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_stCoeffs_A");
                stCoeffs.B = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_stCoeffs_B");
                stCoeffs.C = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_stCoeffs_C");
                tecCtlr.SteinhartHartConstants = stCoeffs;
            }

            // Additional parameters
            double degree = tecCtlr.SensorTemperatureActual_C;
            
            tecCtlr.TecCurrentCompliance_amp = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_TecCurrentCompliance_amp");
            tecCtlr.ProportionalGain = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_ProportionalGain");
            tecCtlr.IntegralGain = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_IntegralGain");
            try
            {
                tecCtlr.SensorTemperatureSetPoint_C = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_SensorTemperatureSetPoint_C");
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            
        }
        //private void ConfigureTecController(Inst_Nt10a tecCtlr, string tecCtlId)
        //{
        //    SteinhartHartCoefficients stCoeffs = new SteinhartHartCoefficients();

        //    tecCtlr.SetDefaultState();
        //    tecCtlr.OperatingMode = (InstType_TecController.ControlMode)
        //        Enum.Parse(typeof(InstType_TecController.ControlMode), progInfo.TestParamsConfig.GetStringParam(tecCtlId + "_OperatingMode"));

        //    // Thermistor characteristics
        //    if (tecCtlr.Sensor_Type == InstType_TecController.SensorType.Thermistor_2wire ||
        //        tecCtlr.Sensor_Type == InstType_TecController.SensorType.Thermistor_4wire)
        //    {
        //        stCoeffs.A = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_stCoeffs_A");
        //        stCoeffs.B = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_stCoeffs_B");
        //        stCoeffs.C = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_stCoeffs_C");
        //        tecCtlr.SteinhartHartConstants = stCoeffs;
        //    }

        //    // Additional parameters
        //    tecCtlr.TecCurrentCompliance_amp = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_TecCurrentCompliance_amp");
        //    tecCtlr.ProportionalGain = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_ProportionalGain");
        //    tecCtlr.IntegralGain = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_IntegralGain");
        //    tecCtlr.SensorTemperatureSetPoint_C = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_SensorTemperatureSetPoint_C");
        //}


        /// <summary>
        /// Configure FCU instruments
        /// </summary>
        /// <param name="fcuInstrs"></param>
        private void ConfigureFCUInstruments(FCUInstruments fcuInstrs)
        {
            // modify for replacing FCU 2400 steven.cui
            // Config FCU source
            // Keithley 2400 specific commands (must be done before 'SetDefaultState')
            //InstType_ElectricalSource FcuSource = progInfo.Instrs.Fcu.FCU_Source;
            //if (FcuSource.DriverName.Contains("Ke24xx"))
            //{
            //    FcuSource.HardwareData["4wireSense"] = "true";
            //}

             //Configure instrument
            //FcuSource.SetDefaultState();
            //// Set to voltage source mode & set current compliance
            //FcuSource.VoltageSetPoint_Volt = 0.0;
            //FcuSource.CurrentComplianceSetPoint_Amp = progInfo.TestParamsConfig.GetDoubleParam("FCUSource_CurrentCompliance_A");
            //FcuSource.VoltageSetPoint_Volt = progInfo.TestParamsConfig.GetDoubleParam("FCUSource_Voltage_V");
            //FcuSource.OutputEnabled = true;
            //double i = FcuSource.CurrentActual_amp;

            // The Ixolite Wrapper Chassis should already have been ctor'd with driver name
            // "Ixolite" and resource string "LPT,378,41" {for accessing printer port 1, 
            // I2C address hex 41}.
            // The FCU instrument should already have been ctor'd with driver name
            // "Ixolite", no slots, and a reference to the above chassis instance.
            fcuInstrs.FullbandControlUnit.ResetCPN();
            fcuInstrs.FullbandControlUnit.EnterProtectMode();
            fcuInstrs.FullbandControlUnit.EnterVendorProtectMode("THURSDAY"); // Need this privilege level
                                                                              // in order to use IOIFBkhm interface.
                                                                              // This password has to be used.
            // Config FCU calibration data
            progInfo.Instrs.Fcu.FCUCalibrationData = new FCUInstruments.FCUCalData();
            string fcuSerialNumber = fcuInstrs.FullbandControlUnit.GetUnitSerialNumber().Trim();
            double CalFactor;
            double CalOffset;
            // Tx ADC
            readCalData("FCU2Asic", fcuSerialNumber, "Tx ADC", out CalFactor, out CalOffset);
            progInfo.Instrs.Fcu2AsicInstrsGroups.FCUCalibrationData.TxADC_CalFactor = CalFactor;
            progInfo.Instrs.Fcu2AsicInstrsGroups.FCUCalibrationData.TxADC_CalOffset = CalOffset;
            // Rx ADC
            readCalData("FCU2Asic", fcuSerialNumber, "Rx ADC", out CalFactor, out CalOffset);
            progInfo.Instrs.Fcu2AsicInstrsGroups.FCUCalibrationData.RxADC_CalFactor = CalFactor;
            progInfo.Instrs.Fcu2AsicInstrsGroups.FCUCalibrationData.RxADC_CalOffset = CalOffset;
            // TxCoarsePot
            readCalData("FCU2Asic", fcuSerialNumber, "TxCoarsePot", out CalFactor, out CalOffset);
            progInfo.Instrs.Fcu2AsicInstrsGroups.FCUCalibrationData.TxCoarsePot_CalFactor = CalFactor;
            progInfo.Instrs.Fcu2AsicInstrsGroups.FCUCalibrationData.TxCoarsePot_CalOffset = CalOffset;
            // TxFinePot
            readCalData("FCU2Asic", fcuSerialNumber, "TxFinePot", out CalFactor, out CalOffset);
            progInfo.Instrs.Fcu2AsicInstrsGroups.FCUCalibrationData.TxFinePot_CalFactor = CalFactor;
            progInfo.Instrs.Fcu2AsicInstrsGroups.FCUCalibrationData.TxFinePot_CalOffset = CalOffset;
            // Fix ADC
            readCalData("FCU2Asic", fcuSerialNumber, "Fix ADC", out CalFactor, out CalOffset);
            progInfo.Instrs.Fcu2AsicInstrsGroups.FCUCalibrationData.FixADC_CalFactor = CalFactor;
            progInfo.Instrs.Fcu2AsicInstrsGroups.FCUCalibrationData.FixADC_CalOffset = CalOffset;

            // ThermistorResistance
            readCalData("FCU2Asic", fcuSerialNumber, "ThermistorResistance", out CalFactor, out CalOffset);
            progInfo.Instrs.Fcu2AsicInstrsGroups.FCUCalibrationData.ThermistorResistance_CalFactor = CalFactor;
            progInfo.Instrs.Fcu2AsicInstrsGroups.FCUCalibrationData.ThermistorResistance_CalOffset = CalOffset;
        }

        // added FCU2ASIC Option by Alice.Huang, for TOSA GB test     2010-02-01
        /// <summary>
        /// initialise FCU2ASIC INSTRUMENTS 
        /// </summary>
        /// <param name="fcu2AsicInstrs">FCU2ASIC instruments group </param>
        private void ConfigureFCU2AsicInstruments(FCU2AsicInstruments fcu2AsicInstrs)
        {

            string fcu2asic_InstrName = "FCU2Asic";
            //Configure instrument

            fcu2AsicInstrs.AsicVccSource.SetDefaultState();
            double curMax = progInfo.TestParamsConfig.GetDoubleParam("AsicVccSource_CurrentCompliance_A");
            Inst_Ke24xx keSource= fcu2AsicInstrs .AsicVccSource  as Inst_Ke24xx ;
            
            //fcu2AsicInstrs .AsicVccSource.U
            //fcu2AsicInstrs.AsicVccSource 
            // Set to voltage source mode & set current compliance
            fcu2AsicInstrs.AsicVccSource.VoltageSetPoint_Volt = progInfo.TestParamsConfig.GetDoubleParam("AsicVccSource_Voltage_V");
            fcu2AsicInstrs.AsicVccSource.CurrentComplianceSetPoint_Amp = curMax;
            fcu2AsicInstrs.AsicVccSource.OutputEnabled = true;
            if (keSource != null)
            {
                keSource.UseFrontTerminals = true;
                keSource.FourWireSense = false;
                keSource.SenseCurrent(curMax, curMax);
            }
            double i = fcu2AsicInstrs.AsicVccSource.CurrentActual_amp;

            fcu2AsicInstrs.AsicVeeSource.SetDefaultState();

            keSource = fcu2AsicInstrs.AsicVccSource as Inst_Ke24xx;
            curMax =progInfo.TestParamsConfig.GetDoubleParam("AsicVeeSource_CurrentCompliance_A");            
            
            // Set to voltage source mode & set current compliance  
            fcu2AsicInstrs.AsicVeeSource.VoltageSetPoint_Volt =
                        progInfo.TestParamsConfig.GetDoubleParam("AsicVeeSource_Voltage_V");
            fcu2AsicInstrs.AsicVeeSource.CurrentComplianceSetPoint_Amp = curMax;
            fcu2AsicInstrs.AsicVeeSource.OutputEnabled = true;
            if (keSource != null)
            {
                keSource.UseFrontTerminals = true;
                keSource.FourWireSense = false;
                keSource.SenseCurrent(curMax, curMax);
            }
            i = fcu2AsicInstrs.AsicVeeSource.CurrentActual_amp;

            // if fcu asic instrs exists, setup fcu source output

            if (fcu2AsicInstrs.FcuSource != null)
            {
                /*fcu2AsicInstrs.FcuSource.SetDefaultState();
                // Set to voltage source mode & set current compliance
                fcu2AsicInstrs.FcuSource.VoltageSetPoint_Volt = 0.0;
                fcu2AsicInstrs.FcuSource.CurrentComplianceSetPoint_Amp =
                            progInfo.TestParamsConfig.GetDoubleParam("FcuSource_CurrentCompliance_A");
                fcu2AsicInstrs.FcuSource.VoltageSetPoint_Volt =
                            progInfo.TestParamsConfig.GetDoubleParam("FcuSource_Voltage_V");
                fcu2AsicInstrs.FcuSource.OutputEnabled = true;
                i = fcu2AsicInstrs.FcuSource.CurrentActual_amp;
                Thread.Sleep(2000);*/
                fcu2AsicInstrs.FcuSource.OutputEnabled = false;

                fcu2AsicInstrs.FcuSource.VoltageSetPoint_Volt = 5.0;
                fcu2AsicInstrs.FcuSource.CurrentComplianceSetPoint_Amp = 1.0500;
                fcu2AsicInstrs.FcuSource.OutputEnabled = true;
                Thread.Sleep(2000);
            }
            // The Ixolite Wrapper Chassis should already have been ctor'd with driver name
            // "Ixolite" and resource string "LPT,378,41" {for accessing printer port 1, 
            // I2C address hex 41}.
            // The FCU instrument should already have been ctor'd with driver name
            // "Ixolite", no slots, and a reference to the above chassis instance.

            // Commented by Alice.Huang  2010-02-01  
            // for i don't know how to implement these fucntion to FCU2Asic
            //fcuInstrs.FullbandControlUnit.ResetCPN();
            //fcuInstrs.FullbandControlUnit.EnterProtectMode();
            //fcuInstrs.FullbandControlUnit.EnterVendorProtectMode("THURSDAY"); // Need this privilege level
            // in order to use IOIFBkhm interface.


            // This password has to be used.
            // Config FCU calibration data

            // Commented by Alice.Huang  2010-02-10  
            // Add FCU2ASIC calibration data 
            

            double CalFactor;
            double CalOffset;

            
            // Tx ADC
            readCalData(fcu2asic_InstrName, "all", "Tx ADC", out CalFactor, out CalOffset);
            progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.FcuDac2mACalibration.TxADC_CalFactor=CalFactor;
            progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.FcuDac2mACalibration.TxADC_CalOffset = CalOffset;
            // Rx ADC
            readCalData(fcu2asic_InstrName, "all", "Rx ADC", out CalFactor, out CalOffset);
            progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.FcuDac2mACalibration.RxADC_CalFactor = CalFactor;
            progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.FcuDac2mACalibration.RxADC_CalOffset = CalOffset;
            // TxCoarsePot
            readCalData(fcu2asic_InstrName, "all", "TxCoarsePot", out CalFactor, out CalOffset);
            progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.FcuDac2mACalibration.TxCoarsePot_CalFactor = CalFactor;
            progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.FcuDac2mACalibration.TxCoarsePot_CalOffset = CalOffset;
            // TxCoarsePot
            readCalData(fcu2asic_InstrName, "all", "RxCoarsePot", out CalFactor, out CalOffset);
            locker_pot_value = CalFactor;
            progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.FcuDac2mACalibration.RxCoarsePot_CalFactor = CalFactor;
            progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.FcuDac2mACalibration.RxCoarsePot_CalOffset = CalOffset;
            // TxFinePot
            readCalData(fcu2asic_InstrName, "all", "TxFinePot", out CalFactor, out CalOffset);
            progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.FcuDac2mACalibration.TxFinePot_CalFactor = CalFactor;
            progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.FcuDac2mACalibration.TxFinePot_CalOffset = CalOffset;
            // Fix ADC
            readCalData(fcu2asic_InstrName, "all", "Fix ADC", out CalFactor, out CalOffset);
            progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.FcuDac2mACalibration.FixADC_CalFactor = CalFactor;
            progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.FcuDac2mACalibration.FixADC_CalOffset = CalOffset;
            // ThermistorResistance
            readCalData(fcu2asic_InstrName, "all", "ThermistorResistance", out CalFactor, out CalOffset);
            progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.FcuDac2mACalibration.ThermistorResistance_CalFactor = CalFactor;
            progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.FcuDac2mACalibration.ThermistorResistance_CalOffset = CalOffset;

            // Asic Part
            progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.SetupChannelCalibration();

            //readCalData(fcu2asic_InstrName, "all", "FsDac", out CalFactor, out CalOffset);
            //progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.SectionsDacCalibration.FsDac_CalFactor = CalFactor;
            //progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.SectionsDacCalibration.FsDac_CalOffset = CalOffset;

            //readCalData(fcu2asic_InstrName, "all", "PhaseDac", out CalFactor, out CalOffset);
            //progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.SectionsDacCalibration.PhaseDac_CalFactor = CalFactor;
            //progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.SectionsDacCalibration.PhaseDac_CalOffset = CalOffset;

            //readCalData(fcu2asic_InstrName, "all", "RearSoaDac", out CalFactor, out CalOffset);
            //progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.SectionsDacCalibration.RearSoaDac_CalFactor = CalFactor;
            //progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.SectionsDacCalibration.RearSoaDac_CalOffset = CalOffset;

            //readCalData(fcu2asic_InstrName, "all", "GainDac", out CalFactor, out CalOffset);
            //progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.SectionsDacCalibration.GainDac_CalFactor = CalFactor;
            //progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.SectionsDacCalibration.GainDac_CalOffset = CalOffset;

            //readCalData(fcu2asic_InstrName, "all", "FrontSoaDac_Pos", out CalFactor, out CalOffset);
            //progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.SectionsDacCalibration.FrontSoaDac_Pos_CalFactor = CalFactor;
            //progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.SectionsDacCalibration.FrontSoaDac_Pos_CalOffset = CalOffset;

            //readCalData(fcu2asic_InstrName, "all", "FrontSoaDac_Neg",out CalFactor, out CalOffset);
            //progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.SectionsDacCalibration.FrontSoaDac_Neg_CalFactor = CalFactor;
            //progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.SectionsDacCalibration.FrontSoaDac_Neg_CalOffset = CalOffset ;

            //readCalData(fcu2asic_InstrName, "all", "RearDac", out CalFactor, out CalOffset);
            //progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.SectionsDacCalibration.RearDac_CalFactor = CalFactor;
            //progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.SectionsDacCalibration.RearDac_CalOffset = CalOffset;

            //readCalData(fcu2asic_InstrName, "all", "ImbLeftDac", out CalFactor, out CalOffset);
            //progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.SectionsDacCalibration.ImbLeftDac_CalFactor = CalFactor;
            //progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.SectionsDacCalibration.ImbLeftDac_CalOffset = CalOffset;

            //readCalData(fcu2asic_InstrName, "all", "ImbRightDac", out CalFactor, out CalOffset);
            //progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.SectionsDacCalibration.ImbRightDac_CalFactor = CalFactor;
            //progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.SectionsDacCalibration.ImbRightDac_CalOffset = CalOffset;

        }
        /// <summary>
        /// Configure a MZ bias source
        /// </summary>
        /// <param name="biasSrc">Instrument reference</param>
        /// <param name="biasSrcId">Config table data prefix</param>
        private void ConfigureMzBiasSource(IInstType_ElectricalSource biasSrc, string biasSrcId)
        {
            // Keithley 2400 specific commands (must be done before 'SetDefaultState')
            if (biasSrc.DriverName.Contains("Ke24xx"))
            {
                biasSrc.HardwareData["4wireSense"] = "true";
            }

            // Configure instrument
            biasSrc.SetDefaultState();
            // Set to current source mode & set compliance
            biasSrc.CurrentSetPoint_amp = 0.0;
            biasSrc.CurrentComplianceSetPoint_Amp = progInfo.TestParamsConfig.GetDoubleParam(biasSrcId + "_CurrentCompliance_A");
            // Set to voltage source mode & set compliance
            biasSrc.VoltageSetPoint_Volt = 0.0;            
            biasSrc.VoltageComplianceSetPoint_Volt = progInfo.TestParamsConfig.GetDoubleParam(biasSrcId + "_VoltageCompliance_volt");
        }

        #endregion

       
        #region Program Running

        public override void Run(ITestEngineRun engine, DUTObject dutObject,InstrumentCollection instrs, ChassisCollection chassis)
        {
            ModuleRun modRun;

            // Set retest count
            this.SetRetestCount(engine, dutObject);

            // Initialise 
           // engine.RunModule("IlmzInitialise");

            // Set switches for the filter box
            if (!engine.IsSimulation && progInfo.DsdbrInstrsUsed == DsdbrDriveInstruments.PXIInstruments)
            {
                switch (progInfo.TestConditions.FreqBand)
                {
                    case FreqBand.C:
                        progInfo.Instrs.SwitchPathManager.SetToPosn("FILTERBOX_C");
                        break;
                    case FreqBand.L:
                        progInfo.Instrs.SwitchPathManager.SetToPosn("FILTERBOX_L"); 
                        break;
                    default:
                        engine.ErrorInProgram("Invalid frequency band: " + progInfo.TestConditions.FreqBand);
                        break;
                }
            }

            // Set temperatures
            //  pre set the case temp to save some time
            progInfo.Instrs.TecCase.SensorTemperatureSetPoint_C = progInfo.TestConditions.MidTemp;
            // TODO set MZ & DSDBR according to FF data
            progInfo.Instrs.TecCase.OutputEnabled = true;

            progInstrs.Fcu2AsicInstrsGroups.Fcu2Asic.Inital_Etalon_WaveMter();//to avoid Var pot large change in overallmap FCUMKI pot leveling Jack.Zhang 2011-01-12
            System.Threading.Thread.Sleep(10000);
            

            engine.RunModule("DsdbrTemperature");
            engine.RunModule("CaseTemp_Mid");

            #region process from mapping---- Echo new added at 10/12/2010

            
            if (doMapping)
            //if (false) //Echo changed this line because Optical Box doesn't work, we cancel mapping stage,
            {
                FcuMKIDarkCurrent darkCurrent = new FcuMKIDarkCurrent();
                engine.RunModule("Mod_OverallMap");
                ModRunData overallMapResults = engine.GetModuleRunData("Mod_OverallMap");
                this.numberOfSupermodes = overallMapResults.ModuleData.ReadSint32("SM_NUM");
                this.soa_powerLeveling = overallMapResults.ModuleData.ReadDouble("SOA_PowerLevel");
                this.imb_left = overallMapResults.ModuleData.ReadDouble("imb_left");
                this.imb_right = overallMapResults.ModuleData.ReadDouble("imb_right");
                darkCurrent = (FcuMKIDarkCurrent)overallMapResults.ModuleData.ReadReference("FcuMkI_Dark_Current_Class");

                dutObject.Attributes.AddSint32("supermodeNumber", numberOfSupermodes);

                if (numberOfSupermodes <= 0)
                {
                    engine.ShowContinueCancelUserQuery("No valid super mode");
                }
                DatumList QaMetricsFiles = new DatumList();
                progInstrs.Fcu2AsicInstrsGroups.Fcu2Asic.Inital_Etalon_WaveMter();//to avoid Var pot large change in overallmap FCUMKI pot leveling Jack.Zhang 2011-01-12
                System.Threading.Thread.Sleep(10000);


                for (int i = 0; i < numberOfSupermodes; i++)
                {
                    // Specification smSpec = SpecDict["map_sm" + i.ToString()];
                    string moduleRunName = "Mod_Supermode_" + i.ToString();
                    Specification smSpec = this.SupermodeSpecs[i];

                    modRun = engine.GetModuleRun(moduleRunName);
                    modRun.ConfigData.AddDouble("Soa_powerLevel", soa_powerLeveling);
                    modRun.ConfigData.AddReference("Dark_Current_FcuI", darkCurrent);

                    ModuleRunReturn mrr = engine.RunModule(moduleRunName);
                    CDSDBRSuperMode sm = (CDSDBRSuperMode)mrr.ModuleRunData.ModuleData.ReadReference("SUMPERMODE" + i.ToString());

                    supermodes.Add(sm);

                    //get returndata for future data write
                    this.CG_ID = mrr.ModuleRunData.ModuleData.ReadString("CGid");
                    QaMetricsFiles.AddFileLink(moduleRunName, mrr.ModuleRunData.ModuleData.ReadFileLinkFullPath("cg_qametrics_sm_file"));
                    switch (i)
                    {
                        case 0: this.MILD_MULTIMODE_SM[i] = mrr.ModuleRunData.ModuleData.ReadSint32("MILD_MULTIMODE_SM0");
                            this.Modal_Distort_SM[i] = mrr.ModuleRunData.ModuleData.ReadDouble("MODAL_DISTORT_SM0");
                            break;
                        case 1: this.MILD_MULTIMODE_SM[i] = mrr.ModuleRunData.ModuleData.ReadSint32("MILD_MULTIMODE_SM1");
                            this.Modal_Distort_SM[i] = mrr.ModuleRunData.ModuleData.ReadDouble("MODAL_DISTORT_SM1");
                            break;
                        case 2: this.MILD_MULTIMODE_SM[i] = mrr.ModuleRunData.ModuleData.ReadSint32("MILD_MULTIMODE_SM2");
                            this.Modal_Distort_SM[i] = mrr.ModuleRunData.ModuleData.ReadDouble("MODAL_DISTORT_SM2");

                            break;
                        case 3: this.MILD_MULTIMODE_SM[i] = mrr.ModuleRunData.ModuleData.ReadSint32("MILD_MULTIMODE_SM3");
                            this.Modal_Distort_SM[i] = mrr.ModuleRunData.ModuleData.ReadDouble("MODAL_DISTORT_SM3");
                            break;
                        case 4:
                            this.MILD_MULTIMODE_SM[i] = mrr.ModuleRunData.ModuleData.ReadSint32("MILD_MULTIMODE_SM4");
                            this.Modal_Distort_SM[i] = mrr.ModuleRunData.ModuleData.ReadDouble("MODAL_DISTORT_SM4");
                            break;
                        case 5:
                            this.MILD_MULTIMODE_SM[i] = mrr.ModuleRunData.ModuleData.ReadSint32("MILD_MULTIMODE_SM5");
                            this.Modal_Distort_SM[i] = mrr.ModuleRunData.ModuleData.ReadDouble("MODAL_DISTORT_SM5");
                            break;
                        case 6:
                            this.MILD_MULTIMODE_SM[i] = mrr.ModuleRunData.ModuleData.ReadSint32("MILD_MULTIMODE_SM6");
                            this.Modal_Distort_SM[i] = mrr.ModuleRunData.ModuleData.ReadDouble("MODAL_DISTORT_SM6");
                            break;
                        case 7:
                            this.MILD_MULTIMODE_SM[i] = mrr.ModuleRunData.ModuleData.ReadSint32("MILD_MULTIMODE_SM7");
                            this.Modal_Distort_SM[i] = mrr.ModuleRunData.ModuleData.ReadDouble("MODAL_DISTORT_SM7");
                            break;
                    }

                }
                #region check LM missingline
                List<string> MissingLineList = new List<string>();
                string message = "Check Longitude Mode missing line.....";
                engine.SendToGui(message);

                bool LongiMode_ok = this.CheckLongitudeMode(QaMetricsFiles, out message, ref MissingLineList);
                engine.SendToGui(message);
                #endregion check lm missing line

                ModuleRunReturn LockerLevelResults = engine.RunModule("LockerLeveling");
                double rearsoa_slope_SM1 = LockerLevelResults.ModuleRunData.ModuleData.ReadDouble("slope_rearsoa_SM1");
                double constant_rear_rearsoa_SM1 = LockerLevelResults.ModuleRunData.ModuleData.ReadDouble("constant_rear_rearsoa_SM1");
                double rearsoa_slope_SM3 = LockerLevelResults.ModuleRunData.ModuleData.ReadDouble("slope_rearsoa_SM3");
                double constant_rear_rearsoa_SM3 = LockerLevelResults.ModuleRunData.ModuleData.ReadDouble("constant_rear_rearsoa_SM3");
                
                progInstrs.Fcu2AsicInstrsGroups.Fcu2Asic.Inital_Etalon_WaveMter();//to avoid Var pot large change in overallmap FCUMKI pot leveling Jack.Zhang 2011-01-12
                System.Threading.Thread.Sleep(10000);
                               
                modRun = engine.GetModuleRun("Mod_Characterise");
                modRun.ConfigData.AddReference("SUPERMODES", supermodes);
                modRun.ConfigData.AddDouble("Slope_rearsoa_SM1", rearsoa_slope_SM1);
                modRun.ConfigData.AddDouble("constant_rear_rearsoa_SM1", constant_rear_rearsoa_SM1);
                modRun.ConfigData.AddDouble("Slope_rearsoa_SM3", rearsoa_slope_SM3);
                modRun.ConfigData.AddDouble("constant_rear_rearsoa_SM3", constant_rear_rearsoa_SM3);
                
                modRun.ConfigData.AddStringArray("MissingLineArray", MissingLineList.ToArray());

                ModuleRunReturn modCharacterizeResult = engine.RunModule("Mod_Characterise");
                zipFilename = this.zipFilesToCalIPhaseAcq(CollectFilesToCalIPhaseAcq(dutObject.SerialNumber), dutObject);

                string closeGridCharFileName = modCharacterizeResult.ModuleRunData.ModuleData.ReadFileLinkFullPath("CG_CHAR_ITU_FILE");
                ituOperationPointfile = closeGridCharFileName;
                itu_op_count = modCharacterizeResult.ModuleRunData.ModuleData.ReadSint32("CHAR_ITU_OP_COUNT");
                this.closeGridCharData = Bookham.Toolkit.CloseGrid.CsvDataLookup.GetItuOperatingPoints(closeGridCharFileName);

                //if test stage is mapping only, then exit the program
                if (dutObject.Attributes.IsPresent("ExtraStage"))
                {
                    if (dutObject.Attributes.GetDatumString("ExtraStage").ValueToString().ToLower().Contains("mapping"))
                    {
                        return;
                    }
                }

            }
            else
            {
                // Cancel mapping stage and Get mapping data from pcas
                this.CopyDataFromMap(this._mapResult, engine);
                this.numberOfSupermodes = _mapResult.ReadSint32("NUM_SM");
            }
            #endregion
            
            // Power up
           RunIlmzPowerUp(engine);//Echo remed this module 

            // Measure of cross-calibration of frequency between map stage and final stage
            RunCrossCalibrationMeasure(engine);

            // Power head calibration
            RunIlmzPowerHeadCal(engine);//Echo remed for debug,

            // MZ Characterise 
            modRun = engine.GetModuleRun("IlmzMzCharacterise");
            modRun.PreviousTestData.AddReference("ItuChannels", closeGridCharData);
            ModuleRunReturn modRunReturn = engine.RunModule("IlmzMzCharacterise");
            bool isTcmzMzCharacteriseError =
                modRunReturn.ModuleRunData.ModuleData.IsPresent("IsError") ? modRunReturn.ModuleRunData.ModuleData.ReadBool("IsError") : false;
            if (isTcmzMzCharacteriseError)
            {
                this.errorInformation +=
                    modRunReturn.ModuleRunData.ModuleData.ReadString("ErrorInformation");
                // Set device to safe case temperature
                engine.RunModule("CaseTemp_Safe");
                return;
            }

            // MZ ITU settings estimate
            Datum mzSweepData = modRunReturn.ModuleRunData.ModuleData.GetDatum("MzSweepData");
            modRun = engine.GetModuleRun("IlmzMzItuEstimate");
            modRun.PreviousTestData.Add(mzSweepData);
            modRun.PreviousTestData.AddReference("ItuChannels", closeGridCharData);
            modRun.PreviousTestData.AddReference("ItuChannelsIn", this.closeGridCharData);
            modRun.PreviousTestData.AddString("MzSweepDataResultsFile", modRunReturn.ModuleRunData.ModuleData.ReadFileLinkFullPath("MzSweepDataResultsFile"));
            ModuleRunReturn ituEstRunReturn = engine.RunModule("IlmzMzItuEstimate",false);

            bool isExit=false;
            try
            {
                if (ituEstRunReturn.ModuleRunData.ModuleData.ReadSint32("MzAtNewVcmPass") == 0)
                {
                    isExit = true;
                }
            }
            catch (Exception e)
            {
                //if program goes here, then module didn't return any data
                isExit = true;
            }
           
            //if (ituEstRunReturn==null || ituEstRunReturn.ModuleRunData.ModuleData.ReadSint32("MzAtNewVcmPass") == 0)
            if (isExit)
            {
                this.errorInformation += "MZ status at new preferred Vcm failed, test stopped.";
                // Set device to safe case temperature
                engine.RunModule("CaseTemp_Safe");
                return;
            }
           IlmzItuChannels = (IlmzChannels)ituEstRunReturn.ModuleRunData.ModuleData.ReadReference("TcmzItuChannels");
           
             //Channel Characterise
            int breakPowerLevelingCount = progInfo.TestParamsConfig.GetIntParam("BreakPowerLevelingCount");
            modRun = engine.GetModuleRun("IlmzChannelChar");
            modRun.ConfigData.AddReference("IlmzItuChannels", IlmzItuChannels);
            modRun.ConfigData.AddUint32("BreakPowerLevelingCount", breakPowerLevelingCount);
            ModuleRunReturn chanCharRunReturn=null ;
            //do
            //{

             chanCharRunReturn = engine.RunModule("IlmzChannelChar");
                
            //} while (true);

            //SelectTestFlag = chanCharRunReturn.ModuleRunData.ModuleData.ReadBool("SelectTestFlag");
            //if ((chanCharRunReturn != null ) &&(chanCharRunReturn.ModuleRunData.ModuleData.ReadSint32("FailAbortFlag") == 1))
            //Echo update above lines to below line,
              isExit = false;
              try
              {
                  if (chanCharRunReturn.ModuleRunData.ModuleData.ReadSint32("FailAbortFlag") == 1)
                  {
                      isExit = true;
                  }
              }
              catch (Exception e)
              {
                  isExit = true;
              }
            //if ((chanCharRunReturn == null) || (chanCharRunReturn.ModuleRunData.ModuleData.ReadSint32("FailAbortFlag") == 1))
              if (isExit)
            {
               // this.errorInformation = chanCharRunReturn.ModuleRunData.ModuleData.ReadString("FailAbortReason");
                this.errorInformation = "Powerleveling Fail in Channel Char Test";
                // Set device to safe case temperature
                engine.RunModule("CaseTemp_Safe");
                return;
            }
             

            // Low case temperature tests
                if (progInfo.TestParamsConfig.GetBoolParam("LowTempTestEnabled"))
                {
                    if (IlmzItuChannels.Passed.Length > 0)
                    {
                        // Set Case temperature to low 
                        engine.RunModule("CaseTemp_Low");

                        // Low temperature test 
                        modRun = engine.GetModuleRun("IlmzLowTempTest");
                        modRun.ConfigData.AddReference("TcmzItuChannels", IlmzItuChannels);
                        modRun.ConfigData.AddReference("ExtremeChannels", chanCharRunReturn.ModuleRunData.ModuleData.ReadReference("ExtremeChannels"));
                        engine.RunModule("IlmzLowTempTest");
                    }
                }

                // High case temperature tests
                if (IlmzItuChannels.Passed.Length > 0)
                {
                    // Set Case temperature to high 
                    engine.RunModule("CaseTemp_High");

                    // High temperature test 
                    modRun = engine.GetModuleRun("IlmzHighTempTest");
                    modRun.ConfigData.AddReference("TcmzItuChannels", IlmzItuChannels);
                    modRun.ConfigData.AddReference("ExtremeChannels", chanCharRunReturn.ModuleRunData.ModuleData.ReadReference("ExtremeChannels"));
                    engine.RunModule("IlmzHighTempTest");
                }

                // Send the case temperature back to ambient whilst we clean up.
                progInfo.Instrs.TecCase.SensorTemperatureSetPoint_C = 25;

                // Trace tone test - make sure over-temp test were done!
                if (IlmzItuChannels.Passed.Length > 0 && progInfo.TestParamsConfig.GetBoolParam("TraceToneTestEnabled"))
                {
                    // One-off measurements on individual channels
                    engine.RunModule("TcmzTraceToneTest");
                }

                // Consolodate results
                modRun = engine.GetModuleRun("IlmzGroupResults");
                modRun.ConfigData.AddReference("TcmzItuChannels", IlmzItuChannels);
                engine.RunModule("IlmzGroupResults");

            // Set device to safe case temperature                 
            engine.RunModule("CaseTemp_Safe");
        }

        /// <summary>
        /// Run IlmzPowerHeadCal module
        /// </summary>
        /// <param name="engine"></param>
        private void RunIlmzPowerHeadCal(ITestEngineRun engine)
        {
            ModuleRun modRun;
            ButtonId skipPowerCal = ButtonId.No;
            if (progInfo.TestParamsConfig.GetBoolParam("AllowSkipPowerCalibration"))
            {
                skipPowerCal = engine.ShowYesNoUserQuery("Do you want to skip power calibration?");
            }
            if (skipPowerCal == ButtonId.Yes)
            {
                GuiMsgs.TcmzPowerCalDataResponse resp;
                do
                {
                    engine.ShowContinueUserQuery("Select Power Calibration Data file");
                    engine.GuiShow();
                    engine.GuiToFront();
                    engine.SendToGui(new GuiMsgs.TcmzPowerCalDataRequest());
                    resp = (GuiMsgs.TcmzPowerCalDataResponse)engine.ReceiveFromGui().Payload;
                    if (!File.Exists(resp.Filename))
                    {
                        engine.ShowContinueUserQuery("Power Calibration Data file doesn't exist, please try again!");
                    }
                } while (!File.Exists(resp.Filename));

                OpticalPowerCal.Initialise(2);
                using (CsvReader cr = new CsvReader())
                {
                    List<string[]> contents = cr.ReadFile(resp.Filename);
                    for (int ii = 1; ii < contents.Count; ii++)
                    {
                        OpticalPowerCal.AddCalibratedPoint(OpticalPowerHead.MZHead, double.Parse(contents[ii][0]), double.Parse(contents[ii][1]) - double.Parse(contents[ii][2]));
                        if (progInfo.DsdbrInstrsUsed == DsdbrDriveInstruments.PXIInstruments)
                        {
                            OpticalPowerCal.AddCalibratedPoint(OpticalPowerHead.OpmCgDirect, double.Parse(contents[ii][0]), double.Parse(contents[ii][1]) - double.Parse(contents[ii][3]));
                            OpticalPowerCal.AddCalibratedPoint(OpticalPowerHead.OpmCgFiltered, double.Parse(contents[ii][0]), double.Parse(contents[ii][1]) - double.Parse(contents[ii][4]));
                        }
                    }
                }
            }
            else
            {
                modRun = engine.GetModuleRun("IlmzPowerHeadCal");
                modRun.ConfigData.AddReference("CloseGridCharData", this.closeGridCharData);
                modRun.ConfigData.AddSint32("NUM_SM", numberOfSupermodes);
                ModuleRunReturn moduleRunReturn = engine.RunModule("IlmzPowerHeadCal");
                labourTime += moduleRunReturn.ModuleRunData.ModuleData.GetDatumDouble("LabourTime").Value;
            }
        }

        /// <summary>
        /// Run IlmzPowerUp module
        /// </summary>
        /// <param name="engine"></param>
        private void RunIlmzPowerUp(ITestEngineRun engine)
        {
            ModuleRun modRun;
            modRun = engine.GetModuleRun("IlmzPowerUp");
            IlmzChannelInit chanInit = new IlmzChannelInit();
            if (doMapping)
            {
                // MZ data is from Mapping module 
                chanInit.Mz.LeftArmImb_mA = this.imb_left;
                chanInit.Mz.RightArmImb_mA = this.imb_right;
                chanInit.Mz.LeftArmMod_Min_V = 0; //this._mapResult.ReadDouble("REF_MZ_VOFF_LEFT"); //Jack.Zhang need to Get from Map PACS
                chanInit.Mz.RightArmMod_Min_V = 0;// this._mapResult.ReadDouble("REF_MZ_VOFF_RIGHT");
                chanInit.Mz.LeftArmMod_Peak_V = 0;// this._mapResult.ReadDouble("REF_MZ_VON_LEFT");
                chanInit.Mz.RightArmMod_Peak_V = 0;// this._mapResult.ReadDouble("REF_MZ_VON_RIGHT");
            }
            else
            {
                // MZ data is from TcmzInitMzSweep
                chanInit.Mz.LeftArmImb_mA = this._mapResult.ReadDouble("REF_MZ_IMB_LEFT");
                chanInit.Mz.RightArmImb_mA = this._mapResult.ReadDouble("REF_MZ_IMB_RIGHT");
                chanInit.Mz.LeftArmMod_Min_V = 0;// this._mapResult.ReadDouble("REF_MZ_VOFF_LEFT"); //Jack.Zhang need to Get from Map PACS
                chanInit.Mz.RightArmMod_Min_V = 0;// this._mapResult.ReadDouble("REF_MZ_VOFF_RIGHT");
                chanInit.Mz.LeftArmMod_Peak_V = 0;// this._mapResult.ReadDouble("REF_MZ_VON_LEFT");
                chanInit.Mz.RightArmMod_Peak_V = 0;// this._mapResult.ReadDouble("REF_MZ_VON_RIGHT");
            }
            // DSDBR data
            double testFreq_GHz = 193700;

            //Add for "L" band by Tim at 2009-2-13.
            if (progInfo.TestConditions.FreqBand == FreqBand.L)
                testFreq_GHz = 189000; 
            foreach (DsdbrChannelData chanData in this.closeGridCharData)
            {
                if (Math.Abs(chanData.ItuFreq_GHz - testFreq_GHz) <= 20)
                {
                    chanInit.Dsdbr = chanData;
                    break;
                }
            }
            if ( !modRun .ConfigData .IsPresent( "IlmzSettings")) modRun.ConfigData.AddReference("IlmzSettings", chanInit);
            ModuleRunReturn tcmzPowerUpRtn = engine.RunModule("IlmzPowerUp");
            if (tcmzPowerUpRtn.ModuleRunData.ModuleData.ReadBool("ErrorExists") == true)
            {
                engine.RaiseNonParamFail(0, tcmzPowerUpRtn.ModuleRunData.ModuleData.ReadString("ErrorString"));
            }
        }

        /// <summary>
        /// Run IlmzCrossCalibration module
        /// </summary>
        /// <param name="engine"></param>
        private void RunCrossCalibrationMeasure(ITestEngineRun engine)
        {
            ModuleRun modRun;
            modRun = engine.GetModuleRun("IlmzCrossCalibration");
            if (this.closeGridCharData.Length < 3)
            {
                engine.RaiseNonParamFail(1,"Frequency channel less than 3 channels, pls check ITU operation point file!");
            }

            // Get DsdbrChannelData for frequency cross-calibration
            int[] chanIndices = new int[3];
            chanIndices[0] = 0; // low channel
            chanIndices[1] = (progInfo.TestConditions.NbrChannels - 1) / 2; // mid channel 45
            chanIndices[2] = progInfo.TestConditions.NbrChannels - 1; // high channel

            if (this.closeGridCharData[closeGridCharData.Length - 1].ItuChannelIndex < chanIndices[2])
            {
                chanIndices[2] = closeGridCharData[closeGridCharData.Length - 1].ItuChannelIndex;
                chanIndices[1] = (chanIndices[0] + chanIndices[2]) / 2;
            }


            DsdbrChannelData[] dsdbrChansData = new DsdbrChannelData[3];
            for (int ii = 0; ii < chanIndices.Length; ii++)
            {
                foreach (DsdbrChannelData chanData in this.closeGridCharData)
                {
                    if (chanData.ItuChannelIndex == chanIndices[ii])
                    {
                        dsdbrChansData[ii] = chanData;
                        break;
                    }
                }
            }

            // Add config data to test module
            modRun.ConfigData.AddReference("DsdbrChannels", dsdbrChansData);
            // Run test module
            engine.RunModule("IlmzCrossCalibration");
        }

        #endregion


        #region End of Program
        /// <summary>
        /// reset instrument after dut test
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        /// <param name="instrs"></param>
        /// <param name="chassis"></param>
        public override void PostRun(ITestEnginePostRun engine, DUTObject dutObject, 
                    InstrumentCollection instrs, ChassisCollection chassis)
        {
            // This takes a few seconds
            string archiveFile = archiveCGfiles(dutObject);

            // Display temperature progress
            engine.GuiShow();
            engine.GuiToFront();

            // Cleanup & power off
            if (!engine.IsSimulation)
            {                

                SetCaseTempSafe(engine, 25);

                /*IlMzInstruments mzInstrs = progInfo.Instrs.Mz;
                mzInstrs.LeftArmMod.CleanUpSweep();                
                mzInstrs.RightArmMod.CleanUpSweep();

                // Alice.Huang   2010-02-03
                // add if to support FCU2ASIC for TOSA GB Test
                if (mzInstrs.ImbsSourceType == IlMzInstruments.EnumSourceType.ElectricalSource)
                {
                    mzInstrs.LeftArmImb.CleanUpSweep();
                    mzInstrs.RightArmImb.CleanUpSweep();
                }
                mzInstrs.TapComplementary.CleanUpSweep();
                if (mzInstrs.InlineTapOnline)
                {
                    mzInstrs.TapInline.CleanUpSweep();
                }*/

                if ( progInfo .DsdbrInstrsUsed == DsdbrDriveInstruments .FCU2AsicInstrument )
                {
                    
                    // Shutdown FCU
                    // Call methods on the FCU to switch off the laser.
                    
                    /*progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.IRear_mA = 0;
                    progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.IPhase_mA = 0;
                    progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.ISoa_mA = 0;
                    progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.IGain_mA = 0;

                    //progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.DisableAllOutput();
                    progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.IRearSoa_mA = 0;
                    progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.IimbLeft_mA = 0;
                    progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.IimbRight_mA = 0;
                    progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.IEven_mA = 0;
                    progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.IOdd_mA = 0;

                    //Echo add above three lines instand of disableAllOutput clause;2010-09-30
                    progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.TecEnable = Inst_Fcu2Asic.OnOff.off;*/

                    
                    if (progInfo .Instrs .Fcu2AsicInstrsGroups.FcuSource != null )
                        progInfo.Instrs.Fcu2AsicInstrsGroups.FcuSource.OutputEnabled = false;
                    progInfo.Instrs.Fcu2AsicInstrsGroups.AsicVeeSource.OutputEnabled = false;
                    progInfo.Instrs.Fcu2AsicInstrsGroups.AsicVccSource.OutputEnabled = false;
                                        
                }

                else if (progInfo .DsdbrInstrsUsed == DsdbrDriveInstruments .FCUInstruments )
                {
                    // modify for replacing FCU 2400 steven.cui
                    // Shutdown FCU
                    // Call methods on the FCU to switch off the laser.
                    progInfo.Instrs.Fcu.SoaCurrentSource.OutputEnabled = false;

                    progInfo.Instrs.Fcu.FullbandControlUnit.SetIVSource((float)0.0, IVSourceIndexNumbers.Rear);
                    progInfo.Instrs.Fcu.FullbandControlUnit.SetIVSource((float)0.0, IVSourceIndexNumbers.Phase);
                    progInfo.Instrs.Fcu.FullbandControlUnit.SetIVSource((float)0.0, IVSourceIndexNumbers.SOA);
                    progInfo.Instrs.Fcu.FullbandControlUnit.SetIVSource((float)0.0, IVSourceIndexNumbers.Gain);
                    progInfo.Instrs.Fcu.FullbandControlUnit.SetControlRegLaserPSUOn(false);
                    progInfo.Instrs.Fcu.FullbandControlUnit.SetControlRegLaserTecOn(false);

                    progInfo.Instrs.Fcu.FullbandControlUnit.SetControlRegLaserPSUOn(false);
                    TxCommand cmd1 = new TxCommand(0, 0, 0); // TunableModules command
                    cmd1 = progInfo.Instrs.Fcu.FullbandControlUnit.ReadTxCommand();
                    TxCommand cmd2 = new TxCommand(cmd1.Data1, cmd1.Data2,
                        (byte)(cmd1.Data3 | (byte)0x01)); // set bit0 of byte "3"
                    progInfo.Instrs.Fcu.FullbandControlUnit.SetTxCommand(cmd2); // Bit 'LSENABLE' is set for disabling it.
                    progInfo.Instrs.Fcu.FullbandControlUnit.OIFLaserEnable(false);
                    //progInfo.Instrs.Fcu.FCU_Source.OutputEnabled = false;
                }

                progInfo.Instrs.TecCase.OutputEnabled = false;
                progInfo.Instrs.TecDsdbr.OutputEnabled = false;
                
            }
        }

        //private string archiveSMSRfiles(string SN)
        //{
        //    const string SMSR_PATH = "results\\smsr";
        //    string[] files = Directory.GetFiles(SMSR_PATH);
        //    string SMSRzipfilename = "results\\SMSR_Spectrum" + "_" + SN + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".zip";
        //    using (Util_ZipFile zipFile = new Util_ZipFile(SMSRzipfilename))
        //    {
        //        foreach (string strFileName in files)
        //        {
        //            zipFile.AddFileToZip(strFileName);
        //            File.Delete(strFileName);
        //        }
        //    }

        //    return Path.GetFullPath(SMSRzipfilename);
        //}
        /// <summary>
        /// Write Dut test result
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        /// <param name="dutOutcome"></param>
        /// <param name="results"></param>
        /// <param name="userList"></param>
        public override void DataWrite(ITestEngineDataWrite engine, DUTObject dutObject,
                        DUTOutcome dutOutcome, TestData results, UserList userList)
        {
            //DatumList tcTraceData = new DatumList();
            //generate keys for main spec

            StringDictionary finalKeys = new StringDictionary();
            finalKeys.Add("SCHEMA", "HIBERDB");
            finalKeys.Add("DEVICE_TYPE", dutObject.PartCode);
            finalKeys.Add("SERIAL_NO", dutObject.SerialNumber);
            finalKeys.Add("SPECIFICATION",progInfo.MainSpec.Name);
            finalKeys.Add("TEST_STAGE","final");


            #region  If do mapping then write these parameter to limit
            Data_Ignore_Write(engine);

            string mapTestStatus=itu_op_count<progInfo.TestConditions.NbrChannels? "Fail":"Pass";
            

            if (doMapping)
            {
                //if (engine.GetProgramRunStatus() != ProgramStatus.Success)
                //{
                //    tcTraceData.AddString("TEST_STATUS_DSDBRMAP", engine.GetProgramRunStatus().ToString());
                //}
                //else
                //{
                //    tcTraceData.AddString("TEST_STATUS_DSDBRMAP", engine.GetOverallPassFail().ToString());
                //}
                tcTraceData.AddString("TEST_STATUS_DSDBRMAP", mapTestStatus);

                tcTraceData.AddString("CG_DUT_FILE_TIMESTAMP", GetCurrentTimeStamp());
                // tcTraceData.AddFileLink("CG_PHASE_MODE_ACQ_FILE", zipFilename);
                //tcTraceData.AddSint32("MILD_MULTIMODE_SM0", this.MILD_MULTIMODE_SM0);
                tcTraceData.AddDouble("MODAL_DISTORT_SM0", this.Modal_Distort_SM[0]);
                //tcTraceData.AddSint32("MILD_MULTIMODE_SM1", this.MILD_MULTIMODE_SM1);
                tcTraceData.AddDouble("MODAL_DISTORT_SM1", this.Modal_Distort_SM[1]);
                //tcTraceData.AddSint32("MILD_MULTIMODE_SM2", this.MILD_MULTIMODE_SM2);
                tcTraceData.AddDouble("MODAL_DISTORT_SM2", this.Modal_Distort_SM[2]);
                //tcTraceData.AddSint32("MILD_MULTIMODE_SM3", this.MILD_MULTIMODE_SM3);
                tcTraceData.AddDouble("MODAL_DISTORT_SM3", this.Modal_Distort_SM[3]);
                //tcTraceData.AddSint32("MILD_MULTIMODE_SM4", this.MILD_MULTIMODE_SM4);
                tcTraceData.AddDouble("MODAL_DISTORT_SM4", this.Modal_Distort_SM[4]);
                //tcTraceData.AddSint32("MILD_MULTIMODE_SM5", this.MILD_MULTIMODE_SM5);
                tcTraceData.AddDouble("MODAL_DISTORT_SM5", this.Modal_Distort_SM[5]);
                //tcTraceData.AddSint32("MILD_MULTIMODE_SM6", this.MILD_MULTIMODE_SM6);
                tcTraceData.AddDouble("MODAL_DISTORT_SM6", this.Modal_Distort_SM[6]);
                // tcTraceData.AddSint32("MILD_MULTIMODE_SM7", this.MILD_MULTIMODE_SM7);
                tcTraceData.AddDouble("MODAL_DISTORT_SM7", 0);
            }

            // pick the specification to add this data to...
            //engine.SetTraceData(progInfo.MainSpec.Name, tcTraceData);

            string smSpecNameStub = null;
            if (dutObject.Attributes.IsPresent("SupermodeSpecNameStub"))
                smSpecNameStub = dutObject.Attributes.ReadString("SupermodeSpecNameStub");

            foreach (Specification spec in results.Specifications)
            {
                if (spec.Name == progInfo.MainSpec.Name) continue;
                if (spec.Name.Contains("DummySpec")) continue;
                StringDictionary smKeys = new StringDictionary();

                smKeys.Add("SCHEMA", "HIBERDB");
                smKeys.Add("DEVICE_TYPE", dutObject.PartCode);
                smKeys.Add("SERIAL_NO", dutObject.SerialNumber);

                DatumList traceData = new DatumList();
                
                if (smSpecNameStub.Length > 0 && spec.Name.Contains(smSpecNameStub))
                {
                    int lastUnderscore = spec.Name.LastIndexOf("_");

                    int smNumber = int.Parse(spec.Name.Substring(lastUnderscore + 1, spec.Name.Length - lastUnderscore - 1));

                    smKeys.Add("TEST_STAGE", "map_sm" + smNumber.ToString());
                    smKeys.Add("SPECIFICATION", smSpecName);
                    if (doMapping)
                    {
                        traceData.AddSint32("NODE", dutObject.NodeID);
                        //if (engine.GetProgramRunStatus() != ProgramStatus.Success)
                        //{
                        //    traceData.AddString("TEST_STATUS", engine.GetProgramRunStatus().ToString());
                        //}
                        //else
                        //{
                        //    traceData.AddString("TEST_STATUS", engine.GetOverallPassFail().ToString());
                        //}
                        traceData.AddString("TEST_STATUS", mapTestStatus);
                    }
                    else
                    {
                        if (Cg_Dut_File_TimeStamp[smNumber]!=null)
                        {
                            traceData.AddString("CG_DUT_FILE_TIMESTAMP", Cg_Dut_File_TimeStamp[smNumber]);
                        }
                        if (Cg_Lm_Lines_Sm_File[smNumber] != null)
                        {
                            traceData.AddFileLink("CG_LM_LINES_SM_FILE", Cg_Lm_Lines_Sm_File[smNumber]);
                        }
                        if (Cg_PassFail_Sm_File[smNumber] != null)
                        {
                            traceData.AddFileLink("CG_PASSFAIL_SM_FILE", Cg_PassFail_Sm_File[smNumber]);
                        }
                        if (Cg_Qametrics_Sm_File[smNumber] != null)
                        {
                            traceData.AddFileLink("CG_QAMETRICS_SM_FILE", Cg_Qametrics_Sm_File[smNumber]);
                        }
                        
                        traceData.AddDouble("GAIN_I_SM_MAP", Gain_i_sm_map[smNumber]);
                        traceData.AddDouble("SOA_I_SM_MAP", Soa_i_sm_map[smNumber]);
                        traceData.AddDouble("REARSOA_I_SM_MAP", RearSoa_i_sm_map[smNumber]);
                       
                        if ((Matrix_ph1_Fwd_Map_Sm[smNumber]!=null) && (!Matrix_ph1_Fwd_Map_Sm[smNumber].ToLower().Contains("unknown")))
                        {
                            traceData.AddFileLink("MATRIX_PHOTO1_FWD_MAP_SM", Matrix_ph1_Fwd_Map_Sm[smNumber]);
                        }
                       
                        if ((Matrix_ph1_Rev_Map_Sm[smNumber]!=null) && (!Matrix_ph1_Rev_Map_Sm[smNumber].ToLower().Contains("unknown")))
                        {
                            traceData.AddFileLink("MATRIX_PHOTO1_REV_MAP_SM", Matrix_ph1_Rev_Map_Sm[smNumber]);
                        }
                        if ((Matrix_ph2_Fwd_Map_Sm[smNumber]!=null) && (!Matrix_ph2_Fwd_Map_Sm[smNumber].ToLower().Contains("unknown")))
                        {
                            traceData.AddFileLink("MATRIX_PHOTO2_FWD_MAP_SM", Matrix_ph2_Fwd_Map_Sm[smNumber]);
                        }
                        if ((Matrix_ph2_Rev_Map_Sm[smNumber]!=null )&& ( !Matrix_ph2_Rev_Map_Sm[smNumber].ToLower().Contains("unknown")))
                        {
                            traceData.AddFileLink("MATRIX_PHOTO2_REV_MAP_SM", Matrix_ph2_Rev_Map_Sm[smNumber]);
                        }
                        if ((Matrix_Pratio_Fwd_Map_Sm[smNumber]!=null) && (!Matrix_Pratio_Fwd_Map_Sm[smNumber].ToLower().Contains("unknown")))
                        {
                            traceData.AddFileLink("MATRIX_PRATIO_FWD_MAP_SM", Matrix_Pratio_Fwd_Map_Sm[smNumber]);
                        }
                        if ((Matrix_Pratio_Rev_Map_Sm[smNumber]!=null) && (!Matrix_Pratio_Rev_Map_Sm[smNumber].ToLower().Contains("unknown")))
                        {
                            traceData.AddFileLink("MATRIX_PRATIO_REV_MAP_SM", Matrix_Pratio_Rev_Map_Sm[smNumber]);
                        }
                        traceData.AddDouble("MODAL_DISTORT_SM", Modal_Distort_SM[smNumber]);
                        traceData.AddSint32("NUM_LM",Num_Lm[smNumber]);
                        traceData.AddSint32("SCREEN_RESULT_SM",Screen_Result_Sm[smNumber]);
                        traceData.AddSint32("SM_ID",Sm_ID[smNumber]);
                        traceData.AddString("TEST_STATUS",Test_status[smNumber]);
                        traceData.AddString("EQUIP_ID",Equip_Id[smNumber]);
                        traceData.AddSint32("NODE", dutObject.NodeID);
                        traceData.AddString("TIME_DATE",Time_Date[smNumber]);
                    }
                    engine.SetDataKeysPerSpec(spec.Name, smKeys);
                    engine.SetTraceData(spec.Name, traceData);
                }

            }

                
            #endregion


            #region Fill in blank TC and CH results
            // Add 'missing data' for TC_* and CH_* parameters
            foreach (ParamLimit paramLimit in progInfo.MainSpec)
            {
                if (paramLimit.ExternalName.StartsWith("TC_") || paramLimit.ExternalName.StartsWith("CH_"))
                {
                    if (!tcTraceData.IsPresent(paramLimit.ExternalName))
                    {
                        Datum dummyValue = null;
                        switch (paramLimit.ParamType)
                        {
                            case DatumType.Double:
                                dummyValue = new DatumDouble(paramLimit.ExternalName, Convert.ToDouble(paramLimit.HighLimit.ValueToString()));
                                break;
                            case DatumType.Sint32:
                                dummyValue = new DatumSint32(paramLimit.ExternalName, Convert.ToInt32(paramLimit.HighLimit.ValueToString()));
                                break;
                            case DatumType.StringType:
                                dummyValue = new DatumString(paramLimit.ExternalName, "MISSING");
                                break;
                            case DatumType.Uint32:
                                dummyValue = new DatumUint32(paramLimit.ExternalName, Convert.ToUInt32(paramLimit.HighLimit.ValueToString()));
                                break;
                        }
                        if (dummyValue != null)
                            tcTraceData.Add(dummyValue);
                    }
                }
            }
            #endregion

            //tcTraceData.AddString("EQUIP_ID", dutObject.EquipmentID);
            //write coc information
            if (_cocResult != null)
            {
                tcTraceData.AddString("CHIP_ID", _cocResult.ReadString("CHIP_ID"));
                tcTraceData.AddString("COC_SERIAL_NO", _cocResult.ReadString("COC_SN"));
                tcTraceData.AddString("WAFER_ID", _cocResult.ReadString("WAFER_ID"));
            }

            tcTraceData.AddSint32("NODE", dutObject.NodeID);
            if (doMapping)
            {
                tcTraceData.AddString("CG_ID", this.CG_ID);
            }
            // Add time date
            tcTraceData.AddString("TIME_DATE", DateTime.Now.ToString("yyyyMMddHHmmss"));
            // Add testTime
            testTime_End = DateTime.Now;
            TimeSpan testTime = testTime_End.Subtract(testTime_Start);
            tcTraceData.AddDouble("TEST_TIME", testTime.TotalMinutes);
            tcTraceData.AddDouble("LABOUR_TIME", labourTime);
            // Add more detail into the main specification
            tcTraceData.AddString("FACTORY_WORKS_FAILMODE", "");
            tcTraceData.AddString("FACTORY_WORKS_LOTID", dutObject.BatchID);
            tcTraceData.AddString("FACTORY_WORKS_PARTID", dutObject.PartCode);
            tcTraceData.AddString("SOFTWARE_ID", dutObject.ProgramPluginName + " SW_version: " + dutObject.ProgramPluginVersion + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + "  " + engine.SpecificTestSolutionId.ToString());
            tcTraceData.AddString("OPERATOR_ID", userList.UserListString);
            tcTraceData.AddString("COMMENTS", engine.GetProgramRunComments());
            tcTraceData.AddString("PRODUCT_CODE", dutObject.PartCode);
            tcTraceData.AddString("FULL_PTR_TEST_FLAG", "0");
            tcTraceData.AddString("PTR_EOL_FREQ_DELTA", "16");
            tcTraceData.AddDouble("TEC_LASER_OL_TEMP", TEC_LASER_OL_TEMP);
            tcTraceData.AddDouble("TEC_JIG_CL_TEMP", TEC_JIG_CL_TEMP);
            tcTraceData.AddString("EQUIP_ID", dutObject.EquipmentID);
            tcTraceData.AddString("WAFER_SIZE", "3");

            #region Write Channel pass/fail files and TTA/TXFP cal files
            if(IlmzItuChannels == null)
            {
                if (engine.GetProgramRunStatus() != ProgramStatus.Success)
                {
                    tcTraceData.AddString("TEST_STATUS", engine.GetProgramRunStatus().ToString());
                }
                else
                {
                    string status = progInfo.MainSpec.Status.Status.ToString();
                    tcTraceData.AddString("TEST_STATUS", status);
                }
                tcTraceData.AddString("FAIL_ABORT_REASON", "Do map only");

                engine.SetDataKeysPerSpec(progInfo.MainSpec.Name, finalKeys);
                try
                {
                    engine.SetTraceData(progInfo.MainSpec.Name, tcTraceData);
                }
                catch (Exception e)
                {
                    engine.ShowContinueUserQuery(e.Message + "\n Error occurs, please check pcas drop data!");
                }
            }
            else
            {
                // Write channel files
                //string ttaTemplate = @"Configuration\TOSA Final\FinalTest\" + 
                //    progInfo.TestParamsConfig.GetStringParam("TTA_PARAM_FILE");
                string tsffTemplate = @"Configuration\TOSA Final\FinalTest\" +
                    progInfo.TestParamsConfig.GetStringParam("TXFP_PARAM_FILE");
                string tsffTemplate_LowCost = @"Configuration\TOSA Final\FinalTest\" +
                    progInfo.TestParamsConfig.GetStringParam("TXFP_PARAM_FILE_lowCost");
                string tempFileDirectory = progInfo.TestParamsConfig.GetStringParam("DSDBRFileDirectory");
                if(MzFileDirectory.Substring(MzFileDirectory.Length-2,2).Contains("\\"))
                {
                    MzFileDirectory = MzFileDirectory.Substring(0, MzFileDirectory.Length - 1);
                }

                string passFile = Util_GenerateFileName.GenWithTimestamp(MzFileDirectory,
                                            "CHANNEL_PASS", dutObject.SerialNumber, "csv");

                string failFile = Util_GenerateFileName.GenWithTimestamp(MzFileDirectory,
                                            "CHANNEL_FAIL", dutObject.SerialNumber, "csv");
                //string ttaFile = tempFileDirectory + Path.DirectorySeparatorChar + 
                //    dutObject.SerialNumber.Replace(".", "_") +
                //    "_" + DateTime.Now.ToUniversalTime().ToString("yyyyMMddHHmmss") + "_ITU.csv";
                string tsffFile = MzFileDirectory + Path.DirectorySeparatorChar +
                    dutObject.SerialNumber.Replace(".", "_") +
                    "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + "_TITAN.csv";

                string tsff_LowCost_File=MzFileDirectory + Path.DirectorySeparatorChar +
                    dutObject.SerialNumber.Replace(".", "_") +
                    "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + "_TITAN_LowCost.csv";

                ChannelDataFiles.WriteIlmzmzData(IlmzItuChannels.Passed, passFile);
                ChannelDataFiles.WriteIlmzmzData(IlmzItuChannels.Failed, failFile);

                while (!File.Exists(tsffTemplate))
                {
                    string prompt = string.Format("Missing file: \"{0}\", Can't write HITT_CAL_FILE.\n" +
                            "Do you want to retry? If press No, this program won't generate HITT_CAL_FILE.",
                            tsffTemplate);
                    if (engine.ShowYesNoUserQuery(prompt) == ButtonId.Yes)
                    {
                        continue;
                    }
                    else
                    {
                        break;
                    }
                }

                if (File.Exists(tsffTemplate))
                {
                    ChannelDataFiles.WriteTXFPData(IlmzItuChannels.Passed, tsffTemplate, tsffFile);
                    ChannelDataFiles.WriteTXFPData(IlmzItuChannels.Passed, tsffTemplate_LowCost, tsff_LowCost_File, ituOperationPointfile,progInstrs.Fcu2AsicInstrsGroups.Fcu2Asic);
                }

                tcTraceData.AddFileLink("CHANNEL_PASS_FILE", passFile);
                tcTraceData.AddFileLink("CHANNEL_FAIL_FILE", failFile);
                //tcTraceData.AddFileLink("TTA_CAL_FILE", ttaFile);
                tcTraceData.AddFileLink("HITT_CAL_FILE", tsff_LowCost_File);

            #endregion

                if ((this.errorInformation != null) &&
                    (this.errorInformation.Length != 0))
                {
                    if (this.errorInformation.Length > 80)
                        this.errorInformation = this.errorInformation.Substring(0, 80);
                    tcTraceData.AddString("FAIL_ABORT_REASON", this.errorInformation);
                }
                else
                {
                    tcTraceData.AddString("FAIL_ABORT_REASON", "Success");
                }
                if (engine.GetProgramRunStatus() != ProgramStatus.Success)
                {
                    tcTraceData.AddString("TEST_STATUS", engine.GetProgramRunStatus().ToString());
                }
                else
                {
                    string status = progInfo.MainSpec.Status.Status.ToString();
                    tcTraceData.AddString("TEST_STATUS", status);
                }

                // Set trace data above here
                engine.SetDataKeysPerSpec(progInfo.MainSpec.Name, finalKeys);
                try
                {
                    engine.SetTraceData(progInfo.MainSpec.Name, tcTraceData);
                }
                catch (Exception e)
                {
                    engine.ShowContinueUserQuery(e.Message + " \nError occcurs, please check pcas drop data!");
                }

                //// Test status and data keys
                //foreach (Specification spec in results.Specifications)
                //{
                //    if (!spec.Name.ToLower().Contains("dummyspec"))
                //    {
                //        StringDictionary dataKeys = new StringDictionary();
                //        dataKeys.Add("SCHEMA", "HIBERDB");
                //        dataKeys.Add("DEVICE_TYPE", dutObject.PartCode);
                //        dataKeys.Add("SERIAL_NO", dutObject.SerialNumber);
                //        dataKeys.Add("TEST_STAGE", "final");
                //        dataKeys.Add("SPECIFICATION", spec.Name);

                //        // Add any other data required for external data (example below for PCAS)
                //        // !(beware, all these parameters MUST exist in the specification)!...
                //        DatumList traceData = new DatumList();
                //        traceData.AddSint32("NODE", dutObject.NodeID);
                //        traceData.AddString("EQUIP_ID", dutObject.EquipmentID);
                //        if (engine.GetProgramRunStatus() != ProgramStatus.Success)
                //        {
                //            string status = engine.GetProgramRunStatus().ToString();
                //            traceData.AddString("TEST_STATUS", status.Substring(0, Math.Min(14, status.Length)));
                //            //traceData.AddString("TEST_STATUS", "FAIL");
                //        }
                //        else
                //        {
                //            string status = spec.Status.Status.ToString();

                //            traceData.AddString("TEST_STATUS", status.Substring(0, Math.Min(14, status.Length)));
                //        }

                //        // pick the specification to add key and trace data to...
                //        engine.SetDataKeysPerSpec(spec.Name, dataKeys);
                //        engine.SetTraceData(spec.Name, traceData);
                //    }
                //}

                #region light_tower

                if (progInfo.lightTowerConfig.GetBoolParam("LightExist"))
                {
                    try
                    {

                        _serialPort.Close();
                        if (!_serialPort.IsOpen)
                        {
                            _serialPort.Open();
                        }

                        // Add the light tower color.
                        _serialPort.WriteLine(progInfo.lightTowerConfig.GetStringParam("LightYellow"));
                        _serialPort.Close();
                    }
                    catch { }
                }
                #endregion light_tower


            }
        }

        /// <summary>
        /// fill in empty ingore parameters
        /// </summary>
        /// <param name="engine"></param>
        private void Data_Ignore_Write(ITestEngineDataWrite engine)
        {
            string[] ignoreParas = {
                "ITU_OP_EST_COUNT",
                ////////The belowed items was read from coc in the future
                "I_ETALON_FS_A",
                "I_ETALON_GAIN",
                "I_ETALON_SOA",
                "CG_ITUEST_POINTS_FILE",
                "CG_PHASE_MODE_ACQ_FILE",
                //"HITT_CAL_FILE",
                "NUM_MISSING_EST_CHAN"
            };
            //DatumList tcTraceData = new DatumList();
            foreach (string para in ignoreParas)
            {
                foreach (ParamLimit paramLimit in progInfo.MainSpec)
                {
                    if (paramLimit.ExternalName == para)
                    {
                        if (!tcTraceData.IsPresent(paramLimit.ExternalName))
                        {
                            Datum dummyValue = null;
                            switch (paramLimit.ParamType)
                            {
                                case DatumType.Double:
                                    dummyValue = new DatumDouble(paramLimit.ExternalName, Convert.ToDouble(paramLimit.HighLimit.ValueToString()));
                                    break;
                                case DatumType.Sint32:
                                    dummyValue = new DatumSint32(paramLimit.ExternalName, Convert.ToInt32(paramLimit.HighLimit.ValueToString()));
                                    break;
                                case DatumType.StringType:
                                    dummyValue = new DatumString(paramLimit.ExternalName, "No Value");
                                    break;
                                case DatumType.Uint32:
                                    dummyValue = new DatumUint32(paramLimit.ExternalName, Convert.ToUInt32(paramLimit.HighLimit.ValueToString()));
                                    break;
                                case DatumType.FileLinkType:
                                    dummyValue = new DatumFileLink(paramLimit.ExternalName, this.Nofilepath);
                                    break;
                            }
                            if (dummyValue != null)
                                //tcTraceData.Add(dummyValue);
                                this.tcTraceData.Add(dummyValue);
                        }//if
                    }//if (paramLimit.ExternalName == para)
                }//foreach (ParamLimit paramLimit in mainSpec)
            }// foreach (string para in ignoreParas)
            //engine.SetTraceData(this.progInfo.MainSpec.Name, tcTraceData);
        }


        /// <summary>
        /// Wait for the case temperature to reach a safe temperature
        /// </summary>
        /// <param name="engine">ITestEnginePostRun</param>
        /// <param name="safeTemperature_C"></param>
        private void SetCaseTempSafe(ITestEnginePostRun engine, double safeTemperature_C)
        {
            // Set case TEC to safe temperature
            progInfo.Instrs.TecCase.SensorTemperatureSetPoint_C = safeTemperature_C;
            double deltaTemp = double.MaxValue;
            do
            {
                double caseTemp = progInfo.Instrs.TecCase.SensorTemperatureActual_C;
                deltaTemp = Math.Abs(safeTemperature_C - caseTemp);
                engine.SendToGui("Case temperature = " + String.Format("{0:f}", caseTemp));
                System.Threading.Thread.Sleep(500);

            } while (deltaTemp > 10);
        }

        #endregion


        #region Private Helper Functions
        /// <summary>
        /// zip files 
        /// </summary>
        /// <param name="files">to be zipped files</param>
        /// <param name="dutObject">dut</param>
        /// <returns></returns>

        private string zipFilesToCalIPhaseAcq(string[] files, DUTObject dutObject)
        {
            string zipFileName = Util_GenerateFileName.GenWithTimestamp(MzFileDirectory,
                "IPhaseModeAcqFiles", dutObject.SerialNumber, "zip");

            if (files.Length > 0)
            {
                Util_ZipFile zipFile = new Util_ZipFile(zipFileName);
                zipFile.SetCompressionLevel(5); // 5 is reasonably fast with good compression
                using (zipFile)
                {
                    foreach (string fileToAdd in files)
                    {
                        zipFile.AddFileToZip(fileToAdd);
                    }
                }
                return zipFileName;

            }

            return null;
        }

        private string[] CollectFilesToCalIPhaseAcq(string SerialNbr)
        {

            string searchPattern = string.Format("ImiddleLower_LM*_DSDBR01_" +
                 SerialNbr + "_" + this.TestTimeStamp + "_SM?.csv");

            return Directory.GetFiles(MzFileDirectory, searchPattern);

        }
      

        /// <summary>
        /// Get the times of retest for the current device
        /// </summary>
        /// <param name="testEngine"></param>
        /// <param name="dutObject"></param>
        private void SetRetestCount(ITestEngineRun testEngine, DUTObject dutObject)
        {
            IDataRead dataRead = testEngine.GetDataReader("PCAS_SHENZHEN");
            int retestCount = 0;
            try
            {
                //get parameter "retest"
                StringDictionary finalKeys = new StringDictionary();
                finalKeys.Add("SCHEMA", "hiberdb");
                finalKeys.Add("DEVICE_TYPE", dutObject.PartCode);
                finalKeys.Add("TEST_STAGE", "final");
                finalKeys.Add("SERIAL_NO", dutObject.SerialNumber);
                DatumList finalData = dataRead.GetLatestResults(finalKeys, false);
                if (finalData == null)
                {
                    retestCount = 0;
                }
                else
                {
                    if (finalData.IsPresent("RETEST"))
                    {
                        retestCount = int.Parse(finalData.ReadString("RETEST"));
                    }
                    retestCount++;
                }

            }
            catch
            {
            }

            // Set retest count
            DatumList dl = new DatumList();
            dl.AddString("RETEST", retestCount.ToString());
            progInfo.MainSpec.SetTraceData(dl);
        }

        /// <summary>
        /// Retrieves datums from config file via the ConfigAccessor.
        /// </summary>
        /// <param name="modRun">The ModuleRun to add the items to.</param>
        /// <param name="datumsRequired">The datum names in this datumlist specify the names of the datums to be retrieved and added to the ModuleRun</param>
        private void ExtractAndAddDatums(ModuleRun modRun, DatumList datumsRequired)
        {
            foreach (Datum datum in datumsRequired)
            {
                switch (datum.Type)
                {
                    case DatumType.Double:
                        {
                            modRun.ConfigData.Add(new DatumDouble(datum.Name, this.progInfo.TestParamsConfig.GetDoubleParam(datum.Name)));
                        }
                        break;

                    case DatumType.StringType:
                        {
                            modRun.ConfigData.Add(new DatumString(datum.Name, this.progInfo.TestParamsConfig.GetStringParam(datum.Name)));
                        }
                        break;

                    case DatumType.Uint32:
                        {
                            modRun.ConfigData.Add(new DatumUint32(datum.Name, this.progInfo.TestParamsConfig.GetIntParam(datum.Name)));
                        }
                        break;

                    case DatumType.Sint32:
                        {
                            modRun.ConfigData.Add(new DatumSint32(datum.Name, this.progInfo.TestParamsConfig.GetIntParam(datum.Name)));
                        }
                        break;

                    case DatumType.BoolType:
                        {
                            modRun.ConfigData.Add(new DatumBool(datum.Name, this.progInfo.TestParamsConfig.GetBoolParam(datum.Name)));
                        }
                        break;

                    default:
                        {
                            throw new ArgumentException("Invalid DatumType '" + datum.Type + "' of '" + datum.Name + "'");
                        }
                }
            }
        }

        /// <summary>
        /// Read instruments calibration data
        /// </summary>
        /// <param name="InstrumentName"></param>
        /// <param name="InstrumentSerialNo"></param>
        /// <param name="ParameterName"></param>
        /// <param name="CalFactor"></param>
        /// <param name="CalOffset"></param>
        private void readCalData(string InstrumentName, string InstrumentSerialNo, 
            string ParameterName, out double CalFactor, out double CalOffset)
        {
            StringDictionary keys = new StringDictionary();
            keys.Add("Instrument_name", InstrumentName);
            keys.Add("Parameter", ParameterName);
            if (InstrumentSerialNo != null)
            {
                keys.Add("Instrument_SerialNo", InstrumentSerialNo);
            }
            DatumList calParams = progInfo.InstrumentsCalData.GetData(keys, false);

            CalFactor = calParams.ReadDouble("CalFactor");
            CalOffset = calParams.ReadDouble("CalOffset");
        }


        private bool CheckLongitudeMode(DatumList QaMetricsFiles, out string message, ref List<string> missingLineMode)
        {   // this module was set forth by roger.li

            bool passFail = true;

            int sm = 0;

            message = "";


            //string[] tmp = new string[2];
            string sSm = "";
            string sLongMode = "";

            double Min_forward_mode_width_low = this.progInfo.MapParamsConfig.GetDoubleParam("Min_forward_mode_width_low");//5
            double Min_reverse_mode_width_low = this.progInfo.MapParamsConfig.GetDoubleParam("Min_reverse_mode_width_low");//5
            double Min_reverse_mode_width_up = this.progInfo.MapParamsConfig.GetDoubleParam("Min_reverse_mode_width_up");//13
            double Min_forward_mode_width_up = this.progInfo.MapParamsConfig.GetDoubleParam("Min_forward_mode_width_up");//13
            double Mean_reverse_mode_width_up = this.progInfo.MapParamsConfig.GetDoubleParam("Mean_reverse_mode_width_up");//14
            double Mean_forward_mode_width_up = this.progInfo.MapParamsConfig.GetDoubleParam("Mean_forward_mode_width_up");//14

            int indexStart = 5;

            for (int i = 0; i < QaMetricsFiles.Count - 1 && sm < numberOfSupermodes - 1; i++, sm++)
            {

                CsvReader csvReader = new CsvReader();

                string smModuleRunName = "Mod_Supermode_" + sm.ToString();

                string fileFullPath = QaMetricsFiles.ReadFileLinkFullPath(smModuleRunName);

                if (File.Exists(fileFullPath))
                {

                    List<string[]> records = csvReader.ReadFile(fileFullPath);
                    for (int record = 9; record < records.Count; record++)
                    {
                        if (records[record].Length > 8)
                        {
                            int longitudeIndex = Convert.ToInt32(records[record][1].Trim());

                            string function = records[record][2].Trim();
                            double minReverse_Mode_Width = 0;
                            double minForward_Mode_Width = 0;
                            double MeanForward_Mode_Width = 0;
                            double MeanReverse_Mode_Width = 0;
                            if (sm == 0)
                            {
                                if (longitudeIndex >= indexStart && string.Compare("Mean", function) == 0)
                                {
                                    MeanForward_Mode_Width = double.Parse(records[record][8]);
                                    MeanReverse_Mode_Width = double.Parse(records[record][9]);

                                    if (MeanReverse_Mode_Width > Mean_reverse_mode_width_up)// read from configer, check reverse LongitudeMode width
                                    {
                                        passFail = false;

                                        message += string.Format("Mean Reverse Mode Width= {3} > {0} at {1} longitude mode in super mode {2}", Mean_reverse_mode_width_up, longitudeIndex, sm, MeanReverse_Mode_Width);

                                        sSm = sm.ToString();
                                        sLongMode = longitudeIndex.ToString();
                                        string sTmp = sSm + "_" + sLongMode;
                                        if (!missingLineMode.Contains(sTmp))
                                        {
                                            missingLineMode.Add(sTmp);
                                        }


                                    }
                                    if (MeanForward_Mode_Width > Mean_forward_mode_width_up) //after reverse width ok, check forward LongitudeMode width
                                    {
                                        passFail = false;

                                        message += string.Format("Mean Forward Mode Width= {3} > {0} at {1} longitude mode in super mode {2}", Mean_forward_mode_width_up, longitudeIndex, sm, MeanForward_Mode_Width);

                                        sSm = sm.ToString();
                                        sLongMode = longitudeIndex.ToString();
                                        string sTmp = sSm + "_" + sLongMode;
                                        if (!missingLineMode.Contains(sTmp))
                                        {
                                            missingLineMode.Add(sTmp);
                                        }

                                        // break;
                                    }
                                }

                                if (longitudeIndex >= indexStart && string.Compare("Minimum", function) == 0)
                                {
                                    minReverse_Mode_Width = double.Parse(records[record][8]);
                                    minForward_Mode_Width = double.Parse(records[record][9]);
                                    //double MinModeWidth = 5;// read from configer
                                    if (minReverse_Mode_Width < Min_forward_mode_width_low)
                                    {
                                        passFail = false;
                                        message += string.Format("Min Forward Mode Width= {3} < {0} at {1} longitude mode in super mode {2}", Min_forward_mode_width_low, longitudeIndex, sm, minReverse_Mode_Width);

                                        sSm = sm.ToString();
                                        sLongMode = longitudeIndex.ToString();
                                        string sTmp = sSm + "_" + sLongMode;
                                        if (!missingLineMode.Contains(sTmp))
                                        {
                                            missingLineMode.Add(sTmp);
                                        }

                                        //break;
                                    }
                                    if (minForward_Mode_Width < Min_reverse_mode_width_low)
                                    {
                                        passFail = false;

                                        message += string.Format("Min Reverse Mode Width= {3} < {0} at {1} longitude mode in super mode {2}", Min_reverse_mode_width_low, longitudeIndex, sm, minForward_Mode_Width);

                                        sSm = sm.ToString();
                                        sLongMode = longitudeIndex.ToString();
                                        string sTmp = sSm + "_" + sLongMode;
                                        if (!missingLineMode.Contains(sTmp))
                                        {
                                            missingLineMode.Add(sTmp);
                                        }

                                        // break;
                                    }

                                }
                            }
                            else
                            {
                                if (string.Compare("Minimum", function) == 0)
                                {
                                    minReverse_Mode_Width = double.Parse(records[record][8]);
                                    minForward_Mode_Width = double.Parse(records[record][9]);

                                    //double MinModeWidth_2 = 5;// read from configer
                                    if (minForward_Mode_Width < Min_forward_mode_width_low)
                                    {
                                        passFail = false;

                                        message += string.Format("Min Forward Mode Width= {3} < {0} at {1} longitude mode in super mode {2}", Min_forward_mode_width_low, longitudeIndex, sm, minForward_Mode_Width);

                                        sSm = sm.ToString();
                                        sLongMode = longitudeIndex.ToString();
                                        string sTmp = sSm + "_" + sLongMode;
                                        if (!missingLineMode.Contains(sTmp))
                                        {
                                            missingLineMode.Add(sTmp);
                                        }
                                        //break;
                                    }
                                    if (minReverse_Mode_Width < Min_reverse_mode_width_low)
                                    {
                                        passFail = false;

                                        message += string.Format("Min Reverse Mode Width= {3} < {0} at {1} longitude mode in super mode {2}", Min_reverse_mode_width_low, longitudeIndex, sm, minReverse_Mode_Width);

                                        sSm = sm.ToString();
                                        sLongMode = longitudeIndex.ToString();
                                        string sTmp = sSm + "_" + sLongMode;
                                        if (!missingLineMode.Contains(sTmp))
                                        {
                                            missingLineMode.Add(sTmp);
                                        }
                                        // break;
                                    }
                                }

                                if ((longitudeIndex == 0) && (string.Compare("Minimum", function) == 0))
                                {
                                    minReverse_Mode_Width = double.Parse(records[record][8]);
                                    minForward_Mode_Width = double.Parse(records[record][9]);

                                    //double MinModeWidth_1 = 13;// read from configer
                                    if (minForward_Mode_Width > Min_forward_mode_width_up)
                                    {
                                        passFail = false;

                                        message += string.Format("Min Forward Mode Width= {3} > {0} at {1} longitude mode in super mode {2}", Min_forward_mode_width_up, longitudeIndex, sm, minForward_Mode_Width);

                                        sSm = sm.ToString();
                                        sLongMode = longitudeIndex.ToString();
                                        string sTmp = sSm + "_" + sLongMode;
                                        if (!missingLineMode.Contains(sTmp))
                                        {
                                            missingLineMode.Add(sTmp);
                                        }

                                        // break;
                                    }
                                    if (minReverse_Mode_Width > Min_reverse_mode_width_up)
                                    {
                                        passFail = false;

                                        message += string.Format("Min Reverse Mode Width= {3} > {0} at {1} longitude mode in super mode {2}", Min_reverse_mode_width_up, longitudeIndex, sm, minReverse_Mode_Width);

                                        sSm = sm.ToString();
                                        sLongMode = longitudeIndex.ToString();
                                        string sTmp = sSm + "_" + sLongMode;
                                        if (!missingLineMode.Contains(sTmp))
                                        {
                                            missingLineMode.Add(sTmp);
                                        }
                                        //break;
                                    }
                                }
                                if ((longitudeIndex > 0) && (string.Compare("Mean", function) == 0))
                                {

                                    MeanForward_Mode_Width = double.Parse(records[record][8]);
                                    MeanReverse_Mode_Width = double.Parse(records[record][9]);
                                    //double MeanModeWidth_1 = 14;//read
                                    if (MeanForward_Mode_Width > Mean_forward_mode_width_up)
                                    {
                                        passFail = false;

                                        message += string.Format("Mean Forward Mode Width= {3} > {0} at {1} longitude mode in super mode {2}", Mean_forward_mode_width_up, longitudeIndex, sm, MeanForward_Mode_Width);

                                        sSm = sm.ToString();
                                        sLongMode = longitudeIndex.ToString();
                                        string sTmp = sSm + "_" + sLongMode;
                                        if (!missingLineMode.Contains(sTmp))
                                        {
                                            missingLineMode.Add(sTmp);
                                        }
                                        //break;
                                    }
                                    if (MeanReverse_Mode_Width > Mean_reverse_mode_width_up)
                                    {
                                        passFail = false;

                                        message += string.Format("Mean Reverse Mode Width= {3} > {0} at {1} longitude mode in super mode {2}", Mean_reverse_mode_width_up, longitudeIndex, sm, MeanReverse_Mode_Width);

                                        sSm = sm.ToString();
                                        sLongMode = longitudeIndex.ToString();
                                        string sTmp = sSm + "_" + sLongMode;
                                        if (!missingLineMode.Contains(sTmp))
                                        {
                                            missingLineMode.Add(sTmp);
                                        }
                                        //break;
                                    }
                                }
                            }

                        }

                    }

                }

                else
                {
                    throw new FileNotFoundException(fileFullPath + "was not found.");
                }
            }
            return passFail;
        }

        /// <summary>
        /// Retrive tcmz_map stage test result form pcas 
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        protected override void ValidateMapStageData(ITestEngineInit engine, DUTObject dutObject)
        {
            if (progInfo.TestParamsConfig.GetBoolParam("IsUsePcasMappingData"))
            {
                IDataRead dataRead = engine.GetDataReader("PCAS_SHENZHEN");

                //2008-3-24: by Ken: Determine whether tcmz_map stage tested.
                StringDictionary mapKeys = new StringDictionary();
                mapKeys.Add("SCHEMA", ILMZSchema);
                mapKeys.Add("SERIAL_NO", dutObject.SerialNumber);
                mapKeys.Add("TEST_STAGE", "final");
                //mapKeys.Add("DEVICE_TYPE", dutObject.PartCode);
                //mapKeys.Add("TEST_STATUS", "Pass");

                StringDictionary cocKeys = new StringDictionary();
                cocKeys.Add("SCHEMA", ILMZSchema);
                cocKeys.Add("SERIAL_NO", dutObject.SerialNumber);
                cocKeys.Add("TEST_STAGE", "package build");

                try
                {
                    this._cocResult = dataRead.GetLatestResults(cocKeys, false);
                    this._mapResult = dataRead.GetLatestResults(mapKeys, true);
                }
                catch (Exception e)
                {
                    this._mapResult = null;
                    this._cocResult = null;
                }

                if (this._mapResult == null || dutObject.Attributes.IsPresent("ExtraStage"))
                {
                    doMapping = true;
                }
                else
                {
                    if (!this._mapResult.IsPresent("TEST_STATUS_DSDBRMAP"))
                    {
                        engine.ShowContinueUserQuery("invalid final(map) data, program will testing mapping first!");
                        doMapping = true;
                    }
                    else
                    {

                        if (this._mapResult.ReadString("TEST_STATUS_DSDBRMAP").ToUpper().Contains("PASS") || dutObject.TestStage.Length < 6)
                        {
                            doMapping = false;
                            // Get data from ITU operating file

                            string ituFile = this._mapResult.ReadFileLinkFullPath("CG_CHAR_ITU_FILE");
                            int node = this._mapResult.ReadSint32("NODE");
                            ituOperationPointfile = CheckItuFilePath(ituFile,node.ToString());
                            //ituOperationPointfile = @"C:\Documents and Settings\mzalign\Desktop\ITUOperatingPoints_DSDBR01_HG100491.001_20110302212612.csv";
                            //string closeGridCharFileName = @"C:\Documents and Settings\mzalign\Desktop\ITUOperatingPoints_DSDBR01_HG100491.001_20110225140915.csv";
                            ////string closeGridCharFileName = @"C:\Documents and Settings\mzalign\Desktop\ITUOperatingPoints_DSDBR01_HG100491.001_20110209045425.csv";
                            ////string closeGridCharFileName = @"C:\Documents and Settings\mzalign\Desktop\ITUOperatingPoints_DSDBR01_HG100493.001_20110208134122.csv";
                            if (ituOperationPointfile == null)
                            {
                                engine.ErrorInProgram("Can't find itu operation file!");
                            }
                            this.closeGridCharData = Bookham.Toolkit.CloseGrid.CsvDataLookup.GetItuOperatingPoints(ituOperationPointfile);
                        }
                        else
                        {
                            doMapping = true;
                        }
                    }
                }
            }
            else
            {
                ManualEntryMappingReults(engine );
            }
        }

        private string CheckItuFilePath(string ituFile,string node)
        {
            if (!ituFile.ToLower().Contains("unknown")) return ituFile; // vlaid path, return directly
            string filename = ituFile.Substring(ituFile.LastIndexOf("\\")+1);
            string strYear = filename.Substring(filename.LastIndexOf('_')+1, 4);
            string strMonth = filename.Substring(filename.LastIndexOf('_') + 5, 2);
            string strYearMonth = filename.Substring(filename.LastIndexOf('_')+1, 6);
            string strYearMonthDay=filename.Substring(filename.LastIndexOf('_')+1, 8);
            string nodeNumber = "Node" + node;
            

            string serverName="";
            switch (strYear)
            {
                case "2011":
                    serverName = "\\\\szn-mcad-01\\Archive2010.08";
                    break;
                case "2009":
                    serverName = "\\\\szn-mcad-01\\Archive2009";
                    break;

                case "2008":
                    serverName = "\\\\szn-mcad-01\\Archive2008";
                    break;
                case "2010":
                    if (int.Parse(strMonth) <= 5) serverName = "\\\\szn-sfl-04\\Archive2010";
                    else
                        if (int.Parse(strMonth) >= 6 && int.Parse(strMonth) <= 8) serverName = "\\\\szn-sfl-04\\Archive2010.06";
                        else serverName = "\\\\szn-mcad-01\\Archive2010.08";
                    break;
            }
            string itufilename = Path.Combine(serverName, strYear);
            itufilename = Path.Combine(itufilename, strYearMonth);
            itufilename = Path.Combine(itufilename, nodeNumber);
            itufilename = Path.Combine(itufilename, strYearMonthDay);
            itufilename = Path.Combine(itufilename, filename);
            if (File.Exists(itufilename))
                return itufilename;
            else
                return null;
        }

        /// <summary>
        /// Validate DSDBR drive instruments - PXI or FCU
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        protected override void ValidateDsdbrDriveInstrs(ITestEngineInit engine, DUTObject dutObject)
        {
            if (dutObject.TestJigID == "FCUTestJigID")
            {
                progInfo.DsdbrInstrsUsed = DsdbrDriveInstruments.FCUInstruments;
            }
            else if (dutObject.TestJigID == "PXITestJigID")
            {
                progInfo.DsdbrInstrsUsed = DsdbrDriveInstruments.PXIInstruments;
            }
            else if (dutObject.TestJigID == "FCU2AsicTestJigId")
            {
                progInfo.DsdbrInstrsUsed = DsdbrDriveInstruments.FCU2AsicInstrument;
            }
            else
            {
                engine.ErrorInProgram("Unknown DSDBR drive instruments used!");
            }
        }

        private void GetSuperModeResultFromPcas(ITestEngineInit engine,DUTObject dutObject)
        {
            IDataRead dataRead = engine.GetDataReader("PCAS_SHENZHEN");

            for (int i = 0; i < 7; i++)
            {
                DatumList superMapResult = null;
                StringDictionary SuperMapKeys = new StringDictionary();
                SuperMapKeys.Add("SCHEMA", ILMZSchema);
                SuperMapKeys.Add("SERIAL_NO", dutObject.SerialNumber);
                SuperMapKeys.Add("TEST_STAGE", "map_sm"+i.ToString());

                try
                {
                    superMapResult = dataRead.GetLatestResults(SuperMapKeys, true);
                }
                catch
                {
                    engine.SendStatusMsg("can't read supermode"+i.ToString()+ " result!");
                }
                if (superMapResult != null)
                {
                    try
                    {
                        Cg_Dut_File_TimeStamp[i] = superMapResult.ReadString("CG_DUT_FILE_TIMESTAMP");
                        Cg_Lm_Lines_Sm_File[i] = superMapResult.ReadFileLinkFullPath("CG_LM_LINES_SM_FILE");
                        Cg_PassFail_Sm_File[i] = superMapResult.ReadFileLinkFullPath("CG_PASSFAIL_SM_FILE");
                        Cg_Qametrics_Sm_File[i] = superMapResult.ReadFileLinkFullPath("CG_QAMETRICS_SM_FILE");
                        Gain_i_sm_map[i] = superMapResult.ReadDouble("GAIN_I_SM_MAP");
                        Soa_i_sm_map[i] = superMapResult.ReadDouble("SOA_I_SM_MAP");
                        RearSoa_i_sm_map[i] = superMapResult.ReadDouble("REARSOA_I_SM_MAP");
                        Matrix_ph1_Fwd_Map_Sm[i] = superMapResult.ReadFileLinkFullPath("MATRIX_PHOTO1_FWD_MAP_SM");
                        Matrix_ph1_Rev_Map_Sm[i] = superMapResult.ReadFileLinkFullPath("MATRIX_PHOTO1_REV_MAP_SM");
                        Matrix_ph2_Fwd_Map_Sm[i] = superMapResult.ReadFileLinkFullPath("MATRIX_PHOTO2_FWD_MAP_SM");
                        Matrix_ph2_Rev_Map_Sm[i] = superMapResult.ReadFileLinkFullPath("MATRIX_PHOTO2_REV_MAP_SM");
                        Matrix_Pratio_Fwd_Map_Sm[i] = superMapResult.ReadFileLinkFullPath("MATRIX_PRATIO_FWD_MAP_SM");
                        Matrix_Pratio_Rev_Map_Sm[i] = superMapResult.ReadFileLinkFullPath("MATRIX_PRATIO_REV_MAP_SM");
                        Modal_Distort_SM[i] = superMapResult.ReadDouble("MODAL_DISTORT_SM");
                        Num_Lm[i] = superMapResult.ReadSint32("NUM_LM");
                        Screen_Result_Sm[i] = superMapResult.ReadSint32("SCREEN_RESULT_SM");
                        Sm_ID[i] = superMapResult.ReadSint32("SM_ID");
                        Test_status[i] = superMapResult.ReadString("TEST_STATUS");
                        //Equip_Id[i] = superMapResult.ReadString("EQUIP_ID");
                        //Node[i] = superMapResult.ReadSint32("NODE");
                        Time_Date[i] = superMapResult.ReadString("TIME_DATE");
                    }
                    catch (Exception e)
                    {
                        string errDesp = e.Message;
                    }
                }
            }

        }

        /// <summary>
        /// Copy data from tcmz_map stage.
        /// </summary>
        /// <param name="mapData"></param>
        /// <param name="engine"></param>
        private void CopyDataFromMap(DatumList mapData, ITestEngineRun engine)
        {
            try
            {
                string[] mapParameter ={
                    "WAFER_SIZE",
                    "SOA_POWER_LEVELING",
                    "MATRIX_PRATIO_SUMMARY_MAP",
                    "MODAL_DISTORT_SM0",
                    "MODAL_DISTORT_SM1",
                    "MODAL_DISTORT_SM2",
                    "MODAL_DISTORT_SM3",
                    "MODAL_DISTORT_SM4",
                    "MODAL_DISTORT_SM5",
                    "MODAL_DISTORT_SM6",
                    "MODAL_DISTORT_SM7",
                    "MILD_MULTIMODE_SM0",
                    "MILD_MULTIMODE_SM1",
                    "MILD_MULTIMODE_SM2",
                    "MILD_MULTIMODE_SM3",
                    "MILD_MULTIMODE_SM4",
                    "MILD_MULTIMODE_SM5",
                    "MILD_MULTIMODE_SM6",
                    "MILD_MULTIMODE_SM7",
                    "CHAR_ITU_OP_COUNT",
                    "CG_CHAR_ITU_FILE",
                    "CG_ITUEST_POINTS_FILE",
                    "CG_PHASE_MODE_ACQ_FILE",
                    "CG_DUT_FILE_TIMESTAMP",
                    "CG_ID",
                    "CG_SM_LINES_FILE",
                    "CG_PRATIO_WL_FILE",
                    "CG_QAMETRICS_FILE",
                    "CG_PASSFAIL_FILE",
                    "IRX_DARK",
                    "ITX_DARK",
                    //"LASER_WAFER_ID",
                    "MZ_MAX_V_BIAS",
                    "MZ_MOD_LEFT_DARK_I",
                    "MZ_MOD_RIGHT_DARK_I",
                    "NUM_SM",
                    "REF_FIBRE_POWER",
                    "REF_FREQ",
                    "REF_LI_FILE",
                    "REF_MZ_IMB_LEFT",
                    "REF_MZ_IMB_RIGHT",
                    "REGISTRY_FILE",
                    "TAP_COMP_DARK_I",
                    "FIBRE_POWER_INITIAL",
                    "LASER_WAFER_SIZE",
                    
                    "TEST_STATUS_DSDBRMAP"
                    
                };

                for (int i = 0; i < mapParameter.Length; i++)
                {
                    if (mapData.IsPresent(mapParameter[i]))
                    {
                        if (!mapData.GetDatum(mapParameter[i]).ValueToString().ToLower().Contains("unknown"))
                        {
                            tcTraceData.Add(mapData.GetDatum(mapParameter[i]));
                        }
                        else
                        {
                            string a = mapData.GetDatum(mapParameter[i]).ValueToString();
                        }
                    }
                }

                #region noused code
                // Feed forward data
                /*if (mapData.IsPresent("WAFER_SIZE"))
                {
                    tcTraceData.Add(mapData.GetDatum("WAFER_SIZE"));
                }
               
                tcTraceData.Add(mapData.GetDatum("SOA_POWER_LEVELING"));
                
                tcTraceData.Add(mapData.GetDatum("MATRIX_PRATIO_SUMMARY_MAP"));

                for (int i = 0; i < 8; i++)
                {
                    tcTraceData.Add(mapData.GetDatum("MODAL_DISTORT_SM" + i.ToString()));
                    tcTraceData.Add(mapData.GetDatum("MILD_MULTIMODE_SM" + i.ToString()));
                }

                //CloseGridCharacterise Module
                
                tcTraceData.Add(mapData.GetDatum("CHAR_ITU_OP_COUNT"));
                tcTraceData.Add(mapData.GetDatum("CG_CHAR_ITU_FILE"));
                tcTraceData.Add(mapData.GetDatum("CG_ITUEST_POINTS_FILE"));
                tcTraceData.Add(mapData.GetDatum("CG_PHASE_MODE_ACQ_FILE"));
                tcTraceData.Add(mapData.GetDatum("CG_DUT_FILE_TIMESTAMP"));
                tcTraceData.Add(mapData.GetDatum("CG_ID"));
                tcTraceData.Add(mapData.GetDatum("CG_SM_LINES_FILE"));
                tcTraceData.Add(mapData.GetDatum("CG_PRATIO_WL_FILE"));
                tcTraceData.Add(mapData.GetDatum("CG_QAMETRICS_FILE"));
                tcTraceData.Add(mapData.GetDatum("CG_PASSFAIL_FILE"));


                //tcTraceData.Add(mapData.GetDatum("I_ETALON_FS_A"));
                //tcTraceData.Add(mapData.GetDatum("I_ETALON_GAIN"));
                //tcTraceData.Add(mapData.GetDatum("I_ETALON_SOA"));

                tcTraceData.Add(mapData.GetDatum("IRX_DARK"));
                tcTraceData.Add(mapData.GetDatum("ITX_DARK"));
                //tcTraceData.Add(mapData.GetDatum("ITU_OP_EST_COUNT"));

                tcTraceData.Add(mapData.GetDatum("LASER_WAFER_ID"));

                tcTraceData.Add(mapData.GetDatum("MZ_MAX_V_BIAS"));
                tcTraceData.Add(mapData.GetDatum("MZ_MOD_LEFT_DARK_I"));
                tcTraceData.Add(mapData.GetDatum("MZ_MOD_RIGHT_DARK_I"));

                tcTraceData.Add(mapData.GetDatum("NUM_SM"));

                tcTraceData.Add(mapData.GetDatum("REF_FIBRE_POWER"));
                tcTraceData.Add(mapData.GetDatum("REF_FREQ"));
                tcTraceData.Add(mapData.GetDatum("REF_LI_FILE"));
                tcTraceData.Add(mapData.GetDatum("REF_MZ_IMB_LEFT"));
                tcTraceData.Add(mapData.GetDatum("REF_MZ_IMB_RIGHT"));
                tcTraceData.Add(mapData.GetDatum("REGISTRY_FILE"));
                tcTraceData.Add(mapData.GetDatum("TAP_COMP_DARK_I"));

              
                //traceDataMapStage.AddString(mapData.GetDatum("LASER_CHIP_ID"));
                tcTraceData.Add(mapData.GetDatum("FIBRE_POWER_INITIAL"));
                tcTraceData.Add(mapData.GetDatum("LASER_WAFER_SIZE"));
                //tcTraceData.Add(mapData.GetDatum("NUM_MISSING_EST_CHAN"));
                tcTraceData.Add(mapData.GetDatum("LASER_WAFER_ID"));
                tcTraceData.Add(mapData.GetDatum("TC_REARSOA_I_OVERALL_MAP"));
                tcTraceData.Add(mapData.GetDatum("TC_REARSOA_MAX"));
                tcTraceData.Add(mapData.GetDatum("TC_REARSOA_MIN"));
                tcTraceData.Add(mapData.GetDatum("TC_TX_TARGET_CURRENT"));
                tcTraceData.Add(mapData.GetDatum("TC_FIBRE_TARGET_POWER_TOL"));

                tcTraceData.Add(mapData.GetDatum("TEST_STATUS_DSDBRMAP"));*/
                #endregion noused code

                // Set trace data
                //progInfo.MainSpec.SetTraceData(tcTraceData);
            }
            catch (Exception e)
            {
                engine.ErrorInProgram("Copy map stage data error!" + e.Message);
            }
        }

        /// <summary>
        /// Run at the end of test to archive results for this device
        /// </summary>
        private string archiveCGfiles(DUTObject dutObject)
        {
            string zipFileName = Util_GenerateFileName.GenWithTimestamp(
                progInfo.TestParamsConfig.GetStringParam("ResultsArchiveDirectory"),
                "CloseGridFiles", dutObject.SerialNumber, "zip");

            if (!Directory.Exists(Path.GetDirectoryName(zipFileName)))
            {
                Directory.CreateDirectory(progInfo.TestParamsConfig.GetStringParam("ResultsArchiveDirectory"));
            }

            if (Directory.Exists(progInfo.TestParamsConfig.GetStringParam("DSDBRFileDirectory")))
            {
                string[] csvFilesToAdd = Directory.GetFiles(
                    progInfo.TestParamsConfig.GetStringParam("DSDBRFileDirectory"),
                    "*" + dutObject.SerialNumber + "*.csv", SearchOption.TopDirectoryOnly);
                string[] zipFilesToAdd = Directory.GetFiles(
                    progInfo.TestParamsConfig.GetStringParam("DSDBRFileDirectory"),
                    "*" + dutObject.SerialNumber + "*.zip", SearchOption.TopDirectoryOnly);
                string[] txtFilesToAdd = Directory.GetFiles(
                    progInfo.TestParamsConfig.GetStringParam("DSDBRFileDirectory"),
                    "*" + dutObject.SerialNumber + "*.txt", SearchOption.TopDirectoryOnly);

                if (csvFilesToAdd.Length > 0 || zipFilesToAdd.Length > 0 || txtFilesToAdd.Length > 0)
                {
                    Util_ZipFile zipFile = new Util_ZipFile(zipFileName);
                    //zipFile.SetCompressionLevel(5); // 5 is reasonably fast with good compression
                    using (zipFile)
                    {
                        if (csvFilesToAdd.Length > 0)
                        {
                            foreach (string fileToAdd in csvFilesToAdd)
                            {
                                zipFile.AddFileToZip(fileToAdd);
                            }
                        }

                        if (zipFilesToAdd.Length > 0)
                        {
                            foreach (string fileToAdd in zipFilesToAdd)
                            {
                                zipFile.AddFileToZip(fileToAdd);
                            }
                        }

                        if (txtFilesToAdd.Length > 0)
                        {
                            foreach (string fileToAdd in txtFilesToAdd)
                            {
                                zipFile.AddFileToZip(fileToAdd);
                            }
                        }
                    }
                    return zipFileName;
                }
            }
            return null;
        }

        /// <summary>
        /// Run at the start of test to delete any old CSV data
        /// </summary>
        protected override void cleanUpCgFiles()
        {
            if (!Directory.Exists(progInfo.TestParamsConfig.GetStringParam("DSDBRFileDirectory")))
                return;

            string[] filesToRemove = Directory.GetFiles(
                    progInfo.TestParamsConfig.GetStringParam("DSDBRFileDirectory"), "*.csv");
            string[] zipFilesToRemove = Directory.GetFiles(
                    progInfo.TestParamsConfig.GetStringParam("DSDBRFileDirectory"), "*.zip");
            string[] txtFilesToRemove = Directory.GetFiles(
                    progInfo.TestParamsConfig.GetStringParam("DSDBRFileDirectory"), "*.txt");

            foreach (string fileToRemove in filesToRemove)
            {
                try
                {
                    File.Delete(fileToRemove);
                }
                catch (Exception)
                {
                    // Ignore exceptions on individual files
                }
            }

            foreach (string fileToRemove in zipFilesToRemove)
            {
                try
                {
                    File.Delete(fileToRemove);
                }
                catch (Exception)
                {
                    // Ignore exceptions on individual files
                }
            }

            foreach (string fileToRemove in txtFilesToRemove)
            {
                try
                {
                    File.Delete(fileToRemove);
                }
                catch (Exception)
                {
                    // Ignore exceptions on individual files
                }
            }
        }

        #endregion

    }
}

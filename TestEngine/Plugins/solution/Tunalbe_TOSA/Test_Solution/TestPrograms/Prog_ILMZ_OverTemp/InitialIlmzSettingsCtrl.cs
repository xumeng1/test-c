using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Bookham.TestSolution.TestPrograms
{
    internal partial class InitialIlmzSettingsCtrl : UserControl
    {
        public InitialIlmzSettingsCtrl(Prog_ILMZ_OverTempGui parent)
        {
            this.parent = parent;
            InitializeComponent();
        }

        private Prog_ILMZ_OverTempGui parent;

        private void buttonFindFile_Click(object sender, EventArgs e)
        {
            DialogResult res = openFileDialog1.ShowDialog(this);
            if (res == DialogResult.OK)
            {
                textBoxTcmzSettingsFile.Text = openFileDialog1.FileName;
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            GuiMsgs.TcmzInitSettingsResponse resp = new GuiMsgs.TcmzInitSettingsResponse(textBoxTcmzSettingsFile.Text);
            parent.SendToWorker(resp);
            parent.CtrlFinished();
        }       

    }
}

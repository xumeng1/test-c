// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestPrograms
//
// Prog_Ilmz_OverTemp.cs
//
// Author: paul.annetts, Mark Fullalove 2006
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.ExternalData;
using Bookham.TestEngine.Framework.Limits;
using System.Collections.Specialized;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestLibrary.Utilities;
using Bookham.TestSolution.IlmzCommonInstrs;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestSolution.IlmzCommonData;
using Bookham.TestSolution.ILMZFinalData;
using Bookham.TestSolution.IlmzCommonUtils;
using Bookham.TestSolution.TestModules;
using Bookham.TestLibrary.Instruments;
using Bookham.ToolKit.Mz;
using Bookham.TestEngine.Config;
using System.Collections;
using Bookham.TestLibrary.BlackBoxes;
using Bookham.Tcmz.Core;
using System.Threading;
using System.IO.Ports;
using Bookham.fcumapping.CommonData;
using Bookham.Solution.Config;
using Bookham.TestLibrary.Algorithms;
using Bookham.TestSolution.Instruments;
using Bookham.TestLibrary.Instruments;

namespace Bookham.TestSolution.TestPrograms
{
    /// <summary>
    /// ILMZ Negtive Chirp chip test program
    /// </summary>
    public class Prog_ILMZ_OverTemp : TcmzProgram, ITestProgram
    {
        #region Private data
        /// <summary>
        /// Remember the specification name we are using in the program
        /// </summary>
        private Prog_ILMZ_OverTempInfo progInfo;
        private DatumList _mapResult;
        private DatumList _cocResult;
        private DateTime testTime_End;
        private double labourTime = 0;
        private IlmzChannels IlmzItuChannels;
        
        private string errorInformation;

        
        private string closeGridCharFileName = "";
        

        private const string externalKeyTuneOpticalPowerToleranceInmW
            = "TC_FIBRE_TARGET_POWER_TOL_MID";
        private const string externalKeyTuneOpticalPowerToleranceIndB
            = "Tune_OpticalPowerTolerance_dB";
        private const string externalKeyTuneISOAPowerSlope
            = "Tune_IsoaPowerSlope";

        private int numberOfSupermodes;
        
        private double imb_left = 0;
        private double imb_right = 0;

        


        
        private string TestTimeStamp;
        private string smSpecName; // supermode spec name
        private double soa_powerLeveling = 0;
        //private int[] MILD_MULTIMODE_SM = { 1, 1, 1, 1, 1, 1, 1 };
        private string CG_ID = "CG no id";
        private string zipFilename;
        private int itu_op_count = 0;// used to store itu point number after characterise module, to determine test status for map stage
        private const string cg_setting_file = @"Configuration\TOSA Final\MapTest\CloseGridSettings.xml";
        private List<CDSDBRSuperMode> supermodes = new List<CDSDBRSuperMode>();
        private ConfigurationManager CGcfgReader = new ConfigurationManager(cg_setting_file);
        private DatumList tcTraceData = new DatumList();
         

        //private string[] Cg_Dut_File_TimeStamp = new string[7];
        //private string[] Cg_Lm_Lines_Sm_File = new string[7];
        //private string[] Cg_PassFail_Sm_File = new string[7];
        //private string[] Cg_Qametrics_Sm_File = new string[7];
        //private double[] Gain_i_sm_map = new double[7];
        //private double[] Soa_i_sm_map = new double[7];
        //private double[] RearSoa_i_sm_map = new double[7];
        //private string[] Matrix_ph1_Fwd_Map_Sm = new string[7];
        //private string[] Matrix_ph1_Rev_Map_Sm = new string[7];
        //private string[] Matrix_ph2_Fwd_Map_Sm = new string[7];
        //private string[] Matrix_ph2_Rev_Map_Sm = new string[7];
        //private string[] Matrix_Pratio_Fwd_Map_Sm = new string[7];
        //private string[] Matrix_Pratio_Rev_Map_Sm = new string[7];
        //private double[] Modal_Distort_SM = { 0, 0, 0, 0, 0, 0, 0 };
        //private int[] Num_Lm = { 0, 0, 0, 0, 0, 0, 0 };
        //private int[] Screen_Result_Sm =  { 0, 0, 0, 0, 0, 0, 0 };
        //private int[] Sm_ID =  { 0, 0, 0, 0, 0, 0, 0 };
        //private string[] Test_status = { "fail", "fail", "fail", "fail", "fail", "fail", "fail" };
        //private string[] Time_Date = { "0", "0", "0", "0", "0", "0", "0" };
        //private int[] Node = new int[7];
        //private string[] Equip_Id = new string[7];
        private bool ituFromLowCost = false;
        private bool moduleRunError = false;
        private bool isOxideWafer = false;



        #endregion

        #region OverTemp test used private data  

        string TestStage = "";// Echoxl.wang@2012-05-16
        private string MzFileDirectory;
        private string Nofilepath = @"Configuration\TOSA_OverTemp\NoDataFile.txt";
        private DatumList finalResult;
        private const string ILMZSchema = "hiberdb";
        private string ituOperationPointfile = "";
        private DsdbrChannelData[] closeGridCharData;
        private ProgIlmzFinalInstruments progInstrs = null;
        private string channelPassFile;
        private ITestEngineInit initTestEngine;
        private double locker_pot_value = 0;

        #endregion

        #region for Light Tower.

        private SerialPort _serialPort;
        /// <summary>
        /// Serial port to control the lighter to indicate test messeage
        /// </summary>
        public SerialPort SerialPort
        {
            get { return _serialPort; }
        }

        /// <summary>
        /// Construct a serial port object from parts of the resource string.
        /// </summary>
        /// <param name="comPort">COMx port portion of resource string.</param>
        /// <param name="rate">bps rate portion of resource string.</param>
        /// <returns>Constructed SerialPort handler instance.</returns>
        private SerialPort buildSerPort(string comPort, string rate)
        {
            return new System.IO.Ports.SerialPort(
                "COM" + comPort,                                /* COMn */
                int.Parse(rate),                                /* bps */
                System.IO.Ports.Parity.None, 8, StopBits.One);  /* 8N1 */
        }
        #endregion



        /// <summary>
        /// Constructor
        /// </summary>
        public Prog_ILMZ_OverTemp()
        {
            this.progInfo = new Prog_ILMZ_OverTempInfo ();
        }
        /// <summary>
        ///  GUI for program 
        /// </summary>
        public override Type UserControl
        {
            get { return typeof(Prog_ILMZ_OverTempGui); }
        }

        /// <summary>
        /// Program initiate phase
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        /// <param name="instrs"></param>
        /// <param name="chassis"></param>
        public override void InitCode(ITestEngineInit engine, DUTObject dutObject, InstrumentCollection instrs, ChassisCollection chassis)
        {
            #region light_tower

            /* Set up Serial Binary Comms */
            // _serialPort = buildSerPort(comPort, rate);
            string lightTower = @"Configuration\TOSA Final\LightTowerParams.xml";
            progInfo.lightTowerConfig = new TestParamConfigAccessor(dutObject,
                lightTower, "_", "LightTowerParams");
            if (progInfo.lightTowerConfig.GetBoolParam("LightExist"))
            {
                try
                {

                    _serialPort = this.buildSerPort(progInfo.lightTowerConfig.GetStringParam("ComNum"), progInfo.lightTowerConfig.GetStringParam("BaudRate"));
                    _serialPort.Close();
                    if (!_serialPort.IsOpen)
                    {
                        _serialPort.Open();
                    }

                    // Add the light tower color.
                    _serialPort.WriteLine(progInfo.lightTowerConfig.GetStringParam("LightGreen"));
                    _serialPort.Close();
                }
                catch { }

            }
            #endregion light_tower...

            initTestEngine = engine;
            base.InitCode(engine, dutObject, instrs, chassis);
            
        }
        /// <summary>
        /// Check Dut's test stage, if it is not in "final" stage, prompt Error in program to stop test
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        protected override void GetTestStageInformation(ITestEngineInit engine, DUTObject dutObject)
        {
            if (!dutObject.TestStage.ToLower().Contains("temptest"))
            {
                engine.ErrorInProgram(string.Format("Un-matched test stage {0}. Expected \"TempTest\"", dutObject.TestStage));
            }
            // Get test stage information - Must be "final"
            TestStage = "TempTest";

        }
        protected override void ValidateMapStageData(ITestEngineInit engine, DUTObject dutObject)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        /// <summary>
        /// verify device internal high temperature...
        /// </summary>
        /// <param name="engine"></param>
        protected override void VerifyJigTemperauture(ITestEngineInit engine)
        {
            //if (VerifyJigTemperautureIsOk) return;
            engine.SendStatusMsg("verify device internal high temperature...");

            //get inst
            Inst_Ke2510 dsdbrTec = progInfo.Instrs.TecDsdbr as Inst_Ke2510;

            Inst_Nt10a jigTec = progInfo.Instrs.TecCase as Inst_Nt10a;

            double JigHighTempValue = progInfo.TestConditions.HighTemp;
            TempTablePoint JigHighTemp = progInfo.TempConfig.GetTemperatureCal(25, JigHighTempValue)[0];
            
            dsdbrTec.OutputEnabled = false;

            dsdbrTec.Set_Protection_Level_Temperature(Inst_Ke2510.LimitType.UPPER, JigHighTempValue + 5);

            //jigTec.Set_Protection_Level_Temperature(Inst_Ke2510.LimitType.UPPER, JigHighTempValue + 5);//Echo remed this line, nt8A can't support this function

            jigTec.SensorTemperatureSetPoint_C = JigHighTempValue + JigHighTemp.OffsetC;
            jigTec.OutputEnabled = true;

            int count = 0;
            double Tolerance = 3;
            do
            {
                Thread.Sleep(2000);
                double dsdbrTemp = dsdbrTec.SensorTemperatureActual_C;
                //double mztemp = mzTec.SensorTemperatureActual_C;
                //&& JigHighTempValue - Tolerance < mztemp && mztemp < JigHighTempValue + Tolerance
                if (JigHighTempValue - Tolerance < dsdbrTemp && dsdbrTemp < JigHighTempValue + Tolerance)
                    break;
                count++;
                if (count > 60)
                {
                    jigTec.SensorTemperatureSetPoint_C = progInfo.TestParamsConfig.GetDoubleParam("TecCase_SensorTemperatureSetPoint_C");
                    Thread.Sleep(2000);
                    jigTec.OutputEnabled = false;
                    //engine.ErrorInModule("");
                    throw new Exception("The Dsdbr and Mz temperature can't reach the CaseTemp:" + JigHighTempValue.ToString());
                }
            } while (true);
            //restore settings
            TEC_LASER_OL_TEMP = dsdbrTec.SensorTemperatureActual_C;
            //TEC_MZ_OL_TEMP = mzTec.SensorTemperatureActual_C;
            TEC_JIG_CL_TEMP = jigTec.SensorTemperatureActual_C;
            jigTec.SensorTemperatureSetPoint_C = progInfo.TestParamsConfig.GetDoubleParam("TecCase_SensorTemperatureSetPoint_C");
            Thread.Sleep(1000);
            jigTec.OutputEnabled = false;
            VerifyJigTemperautureIsOk = true;
            engine.SendStatusMsg("verify is ok!");

        }
        /// <summary>
        /// Initialise test configuration
        /// </summary>
        /// <param name="dutObj">Device under test</param>
        /// <param name="engine">Test engine</param>
        protected override void initConfig(DUTObject dutObj, ITestEngineInit engine)
        {

            progInfo.TestParamsConfig = new TestParamConfigAccessor(dutObj,
                @"Configuration\TOSA_OverTemp\TcmzTestParams.xml", "", "TcmzTestParams");
            bool usepcas = progInfo.TestParamsConfig.GetBoolParam("UseLocalLimitFile");

            progInfo.InstrumentsCalData = new ConfigDataAccessor(@"Configuration\TOSA_OverTemp\InstrsCalibration_new.xml", "Calibration");

            progInfo.TempConfig = new TempTableConfigAccessor(dutObj, 1,
                @"Configuration\TOSA_OverTemp\TempTable.xml");
            progInfo.OpticalSwitchConfig = new TestParamConfigAccessor(dutObj,
                 @"Configuration\TOSA_OverTemp\IlmzOpticalSwitch.xml", "", "IlmzOpticalSwitchParams");

            progInfo.TestSelect = new TestSelection(@"Configuration\TOSA_OverTemp\testSelect_" + dutObj.PartCode + ".xml");

            //progInfo.MapParamsConfig = new TestParamConfigAccessor(dutObj, @"Configuration\TOSA Final\MapTest\MappingTestConfig.xml", "", "MappingTestParams");
            string _MzFileDirectory = progInfo.TestParamsConfig.GetStringParam("MzFileDirectory");
            GetCurrentTimeStamp();
            string _fileDir_subFolder = "\\" + dutObj.SerialNumber + "_" + TestTimeStamp;

            MzFileDirectory = _MzFileDirectory + _fileDir_subFolder;

            if (!Directory.Exists(MzFileDirectory))
            {
                Directory.CreateDirectory(MzFileDirectory);
            }
            string Nofilepath_Temp = MzFileDirectory + Nofilepath.Substring(Nofilepath.LastIndexOf("\\"), Nofilepath.Length - Nofilepath.LastIndexOf("\\") - 4) + "_" + TestTimeStamp + ".txt";
            File.Copy(Nofilepath, Nofilepath_Temp);
            Nofilepath = Nofilepath_Temp;//Jack.zhang fix Nofile load by stream bug.2011-09-06

            ValidateFinalStageData(engine, dutObj);

        }
        protected override void CheckPowerBeforeTesting(ITestEngineInit engine, DUTObject dutObject)
        {
            engine.SendStatusMsg("Check power before testing..");
        }

        /// <summary>
        /// Initialise instruments
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="instrs"></param>   
        /// <param name="chassis"></param>
        protected override void initInstrs(ITestEngineInit engine, InstrumentCollection instrs, ChassisCollection chassis)
        {
            //add for avoidig instrument logging error
            engine.SendStatusMsg("Initialise Instruments");
            progInstrs = new ProgIlmzFinalInstruments();
            progInfo.Instrs = progInstrs;

            //foreach (Chassis var in chassis.Values) var.EnableLogging = false;
            //foreach (Instrument var in instrs.Values) var.EnableLogging = false;

            // Initialise DSDBR drive instruments
            switch (progInfo.DsdbrInstrsUsed)
            {
                case DsdbrDriveInstruments.FCUInstruments:
                    // FCU
                    progInstrs.Fcu = new FCUInstruments();
                    progInstrs.Fcu.FullbandControlUnit = (Instr_FCU)instrs["FullbandControlUnit"];
                    progInstrs.Fcu.SoaCurrentSource =
                                        (InstType_ElectricalSource)instrs["SoaCurrentSource"];
                    // modify for replacing FCU 2400 steven.cui
                    //progInstrs.Fcu.FCU_Source = (InstType_ElectricalSource)instrs["FCU_Source"];
                    break;

                // Alice.Huang add FCU2Asic function    2010-02-01
                case DsdbrDriveInstruments.FCU2AsicInstrument:
                    engine.SendStatusMsg("Configuring FCU2Asic instruments");
                    progInstrs.Fcu2AsicInstrsGroups = new FCU2AsicInstruments();
                    if (instrs.Contains("FCU_Source"))
                        progInstrs.Fcu2AsicInstrsGroups.FcuSource =
                                    (InstType_ElectricalSource)instrs["FCU_Source"];
                    else
                        progInstrs.Fcu2AsicInstrsGroups.FcuSource = null;

                    progInstrs.Fcu2AsicInstrsGroups.AsicVccSource =
                                        (InstType_ElectricalSource)instrs["ASIC_VccSource"];
                    progInstrs.Fcu2AsicInstrsGroups.AsicVeeSource =
                                        (InstType_ElectricalSource)instrs["ASIC_VeeSource"];
                    progInstrs.Fcu2AsicInstrsGroups.Fcu2Asic = (Inst_Fcu2Asic)instrs["Fcu2Asic"];
                    //progInstrs.Fcu2AsicInstrsGroups.FcuSource = (Inst_Ke24xx)instrs["FcuSource"];
                    break;

            }

           
            progInstrs.Mz = new IlMzInstruments();
            progInstrs.Mz.PowerMeter = (IInstType_TriggeredOpticalPowerMeter)instrs["OpmMz"];
            // OpmRef
            progInstrs.OpmReference = (IInstType_OpticalPowerMeter)instrs["OpmRef"];

            progInstrs.Mz.PowerMeter.EnableInputTrigger(false);
            progInstrs.Mz.PowerMeter.SetDefaultState();
            progInstrs.OpmReference.SetDefaultState();

            //wavemeter
            Inst_Ag86122x waveMeter_Ag86122 = (Inst_Ag86122x)instrs["WaveMeter"];
            if (waveMeter_Ag86122.IsOnline)
            {
                waveMeter_Ag86122.ContinueMeasMode = false;
                progInfo.Instrs.Wavemeter = (IInstType_Wavemeter)waveMeter_Ag86122;
                Measurements.Wavemeter = (IInstType_Wavemeter)waveMeter_Ag86122;
            }

            // OSA
            /*  progInstrs.Osa = (IInstType_OSA)instrs["Osa"];

              // WM
              progInstrs.Wavemeter = (IInstType_Wavemeter)instrs["Wavemeter"];*/
            //Echo remed this block

            // Switch path manager
            //Util_SwitchPathManager switchPathManager = new Util_SwitchPathManager
            //   (@"Configuration\TOSA Final\switches.xml", instrs);
            // progInstrs.SwitchPathManager = switchPathManager;

            // Switch between OSA and Opm
            // Switch_Osa_MzOpm switchOsaMzOpm = new Switch_Osa_MzOpm(switchPathManager);
            // progInstrs.SwitchOsaMzOpm = switchOsaMzOpm;

            // TECs
            progInstrs.TecDsdbr = (IInstType_TecController)instrs["TecDsdbr"];
            DsdbrUtils.opticalSwitch = (Inst_Ke2510)instrs["TecDsdbr"];
            DsdbrTuning.OpticalSwitch = (Inst_Ke2510)instrs["TecDsdbr"];
             
            progInstrs.TecCase = (IInstType_TecController)instrs["TecCase"];
            progInstrs.TecCase.SensorTemperatureSetPoint_C = progInfo.TestParamsConfig.GetDoubleParam("TecCase_SensorTemperatureSetPoint_C");

            // Config instruments if not simulation mode
            if (!engine.IsSimulation)
            {
                engine.SendStatusMsg("Configuring Instruments");
                // Configure FCU
                if (progInfo.DsdbrInstrsUsed == DsdbrDriveInstruments.FCUInstruments)
                    ConfigureFCUInstruments(progInstrs.Fcu);

                    // Alice.Huang   2010-02-01
                // add FCU2ASIC instrument initialise for TOSA Test
                else if (progInfo.DsdbrInstrsUsed == DsdbrDriveInstruments.FCU2AsicInstrument)
                    ConfigureFCU2AsicInstruments(progInstrs.Fcu2AsicInstrsGroups);

                // Configure TEC controllers

                ConfigureTecController(progInstrs.TecDsdbr, "TecDsdbr");
                ConfigureTecController(progInstrs.TecCase, "TecCase");

               
                progInstrs.Mz.FCU2Asic = (Inst_Fcu2Asic)instrs["Fcu2Asic"];
                progInstrs.OpmReference.SetDefaultState();
                progInstrs.Mz.PowerMeter.SetDefaultState();
                DsdbrUtils.locker_port = locker_pot_value;
                DsdbrTuning.locker_pot = locker_pot_value;

                progInfo.Instrs.Fcu2AsicInstrsGroups.AsicVccSource.SetDefaultState();
                progInfo.Instrs.Fcu2AsicInstrsGroups.AsicVeeSource.SetDefaultState();
                progInfo.Instrs.Fcu2AsicInstrsGroups.AsicVccSource.OutputEnabled = false;
                progInfo.Instrs.Fcu2AsicInstrsGroups.AsicVeeSource.OutputEnabled = false;
            }
        }

        /// <summary>
        /// Load specifications
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        protected override void loadSpecs(ITestEngineInit engine, DUTObject dutObject)
        {
            engine.SendStatusMsg("Loading Specification");
            ILimitRead limitReader = null;
            StringDictionary mainSpecKeys = new StringDictionary();


            if (progInfo.TestParamsConfig.GetBoolParam("UseLocalLimitFile"))
            {
                // initialise limit file reader
                limitReader = engine.GetLimitReader("PCAS_FILE");
                // main specification
                string final_SpecNameFile = progInfo.TestParamsConfig.GetStringParam("OverTempLocalLimitFileName");
                string finalSpecFullFilename = progInfo.TestParamsConfig.GetStringParam("LocalLimitFileDirectory")
                    + @"\" + final_SpecNameFile;
                mainSpecKeys.Add("Filename", finalSpecFullFilename);

                
            }
            else
            {
                // Use default limit source
                limitReader = engine.GetLimitReader();

                mainSpecKeys.Add("SCHEMA", "HIBERDB");
                mainSpecKeys.Add("TEST_STAGE", "final");
                mainSpecKeys.Add("DEVICE_TYPE", dutObject.PartCode);


            }

            // load main specification

            SpecList tempSpecList = limitReader.GetLimit(mainSpecKeys);
            progInfo.MainSpec = tempSpecList[0];

            Specification dummySpec = new Specification("DummySpec", new StringDictionary(), 1);

            // declare spec list to Test Engine
            SpecList specList = new SpecList();
            specList.Add(progInfo.MainSpec);
            specList.Add(dummySpec);
            engine.SetSpecificationList(specList);

            progInfo.TestConditions = new IlmzOverTempTestConds(progInfo.MainSpec);

        }

        /// <summary>
        /// Initialise utilities
        /// </summary>
        /// <param name="engine"></param>
        protected override void initUtils(ITestEngineInit engine)
        {
            engine.SendStatusMsg("Initialise utilities");
            // Initialise DsdbrUtils and Measurements utilities by testkit hardware info.
            switch (progInfo.DsdbrInstrsUsed)
            {
                // added FCU2ASIC Option by Alice.Huang, for TOSA GB test     2010-02-01
                case DsdbrDriveInstruments.FCU2AsicInstrument:
                    DsdbrUtils.Fcu2AsicInstrumentGroup = progInfo.Instrs.Fcu2AsicInstrsGroups;
                    Measurements.FCU2AsicInstrs = progInfo.Instrs.Fcu2AsicInstrsGroups;
                    SoaShutterMeasurement.Fcu2AsicInstrs = progInfo.Instrs.Fcu2AsicInstrsGroups;
                    break;
                case DsdbrDriveInstruments.FCUInstruments:
                    DsdbrUtils.FcuInstrumentGroup = progInfo.Instrs.Fcu;
                    Measurements.FCUInstrs = progInfo.Instrs.Fcu;
                    SoaShutterMeasurement.FCUInstrs = progInfo.Instrs.Fcu;
                    break;
            }
            //Measurements.OSA = progInfo.Instrs.Osa;
            
            //Measurements.Wavemeter = progInfo.Instrs.Wavemeter;
            Measurements.MzHead = progInfo.Instrs.Mz.PowerMeter;
            Measurements.ExternalHead = progInfo.Instrs.OpmReference;


            //Measurements.SectionIVSweepRawData = null;

            // Initialise TraceToneTest utility by device types
            if (progInfo.TestParamsConfig.GetBoolParam("TraceToneTestEnabled"))
            {
                TraceToneTest.Initialise(
                    progInfo.TestParamsConfig.GetIntParam("TraceToneTest_OrderOfFit"),
                    progInfo.TestConditions.TargetFibrePower_dBm,
                    progInfo.TestConditions.TraceToneTestAgeCond1_dB,
                    progInfo.TestConditions.TraceToneTestAgeCond2_dB,
                    progInfo.TestParamsConfig.GetDoubleParam("TraceToneTest_ModulationIndex"),
                    progInfo.TestParamsConfig.GetDoubleParam("TraceToneTest_SoaStart_mA"),
                    progInfo.TestParamsConfig.GetDoubleParam("TraceToneTest_SoaEnd_mA"),
                    progInfo.TestParamsConfig.GetDoubleParam("TraceToneTest_SoaStep_mA"),
                    progInfo.TestParamsConfig.GetIntParam("TraceToneTest_StepDelay_mS"));
            }
            this.initOpticalSwitchIoLine();

        }


        private void RecordSweepData(ILMZSweepData sweepData, string ChangePartcodeSweepFile)
        {
            Dictionary<ILMZSweepDataType, double[]> sweepDataListTemp = sweepData.Sweeps;
            List<string> dataNameList = new List<string>();
            List<double[]> sweepDataList = new List<double[]>();

            foreach (ILMZSweepDataType dataName in sweepDataListTemp.Keys)
            {
                dataNameList.Add(dataName.ToString());
                sweepDataList.Add(sweepDataListTemp[dataName]);
            }

            double[][] sweepDataArray = sweepDataList.ToArray();

            string[] dataNameArray = dataNameList.ToArray();

            if ((sweepDataArray.Length > 0) && (dataNameArray[0].Length > 0))
            { }
            else
            {
                return;
            }

            string mzSweepDataResultsFile = this.MzFileDirectory + ChangePartcodeSweepFile;
            if (File.Exists(mzSweepDataResultsFile))
            {
                File.Delete(mzSweepDataResultsFile);
            }

            using (StreamWriter writer = new StreamWriter(mzSweepDataResultsFile))
            {
                // file header
                StringBuilder fileHeader = new StringBuilder();

                for (int count = 0; count < dataNameArray.Length; count++)
                {
                    if (count != 0) fileHeader.Append(",");
                    fileHeader.Append(dataNameArray[count]);
                }

                // file contents
                List<string> fileContents = new List<string>();
                for (int irow = 0; irow < sweepDataArray[0].Length; irow++)
                {
                    StringBuilder aLine = new StringBuilder();
                    for (int icol = 0; icol < dataNameArray.Length; icol++)
                    {
                        if (icol != 0) aLine.Append(",");
                        aLine.Append(sweepDataArray[icol][irow].ToString());
                    }
                    fileContents.Add(aLine.ToString());
                }

                // write file header to file
                writer.WriteLine(fileHeader.ToString());

                // writer file contents to file
                foreach (string row in fileContents)
                {
                    writer.WriteLine(row);
                }
            }

        }


        private void initOpticalSwitchIoLine()
        {
            IlmzOpticalSwitchLines.C_Band_DigiIoLine = int.Parse(progInfo.OpticalSwitchConfig.GetStringParam("C_Band_Filter_IO_Line"));
            IlmzOpticalSwitchLines.C_Band_DigiIoLine_State = progInfo.OpticalSwitchConfig.GetBoolParam("C_Band_Filter_IO_Line_State");
            IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine = int.Parse(progInfo.OpticalSwitchConfig.GetStringParam("Dut_Ctap_IO_Line"));
            IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine_State = progInfo.OpticalSwitchConfig.GetBoolParam("Dut_Ctap_IO_Line_State");
            IlmzOpticalSwitchLines.Dut_Rx_DigiIoLine = int.Parse(progInfo.OpticalSwitchConfig.GetStringParam("Dut_Rx_IO_Line"));
            IlmzOpticalSwitchLines.Dut_Rx_DigiIoLine_State = progInfo.OpticalSwitchConfig.GetBoolParam("Dut_Rx_IO_Line_State");
            IlmzOpticalSwitchLines.Dut_Tx_DigiIoLine = int.Parse(progInfo.OpticalSwitchConfig.GetStringParam("Dut_Tx_IO_LIne"));
            IlmzOpticalSwitchLines.Dut_Tx_DigiIoLine_State = progInfo.OpticalSwitchConfig.GetBoolParam("Dut_Tx_IO_LIne_State");
            IlmzOpticalSwitchLines.Etalon_Ref_DigiIoLine = int.Parse(progInfo.OpticalSwitchConfig.GetStringParam("Etalon_Ref_IO_Line"));
            IlmzOpticalSwitchLines.Etalon_Ref_DigiIoLine_State = progInfo.OpticalSwitchConfig.GetBoolParam("Etalon_Ref_IO_Line_State");
            IlmzOpticalSwitchLines.Etalon_Rx_DigiIoLine = int.Parse(progInfo.OpticalSwitchConfig.GetStringParam("Etalon_Rx_IO_Line"));
            IlmzOpticalSwitchLines.Etalon_Rx_DigiIoLine_State = progInfo.OpticalSwitchConfig.GetBoolParam("Etalon_Rx_IO_Line_State");
            IlmzOpticalSwitchLines.Etalon_Tx_DigiIoLine = int.Parse(progInfo.OpticalSwitchConfig.GetStringParam("Etalon_Tx_IO_Line"));
            IlmzOpticalSwitchLines.Etalon_Tx_DigiIoLine_State = progInfo.OpticalSwitchConfig.GetBoolParam("Etalon_Tx_IO_Line_State");
            IlmzOpticalSwitchLines.L_Band_DigiIoLine = int.Parse(progInfo.OpticalSwitchConfig.GetStringParam("L_Band_Filter_IO_Line"));
            IlmzOpticalSwitchLines.L_Band_DigiIoLine_State = progInfo.OpticalSwitchConfig.GetBoolParam("L_Band_Filter_IO_Line_State");
        }

        /// <summary>
        /// Initialise test modules
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        protected override void initModules(ITestEngineInit engine, DUTObject dutObject, InstrumentCollection instrs, ChassisCollection chassis)
        {
            engine.SendStatusMsg("Initialise modules");

            this.IlmzInitialise_InitModule(engine, dutObject);
            this.DeviceTempControl_InitModule(engine, "DsdbrTemperature", progInfo.Instrs.TecDsdbr, "TecDsdbr");
            this.CaseTempControl_InitModule(engine, "CaseTemp_Mid", 25, progInfo.TestConditions.MidTemp);
            this.IlmzPowerUp_InitModule(engine, instrs);
            this.IlmzCrossCalibration_InitModule(engine, instrs);
            this.IlmzPowerHeadCal_InitModule(engine, dutObject);

            if (progInfo.TestParamsConfig.GetBoolParam("LowTempTestEnabled"))
            {
                this.CaseTempControl_InitModule(engine, "CaseTemp_Low", progInfo.TestConditions.MidTemp, progInfo.TestConditions.LowTemp);
                this.CaseTempControl_InitModule(engine, "CaseTemp_High", progInfo.TestConditions.LowTemp, progInfo.TestConditions.HighTemp);
                this.IlmzLowTempTest_InitModule(engine, dutObject);
            }
            else
            {
                this.CaseTempControl_InitModule(engine, "CaseTemp_High", progInfo.TestConditions.MidTemp, progInfo.TestConditions.HighTemp);
            }
            this.IlmzHighTempTest_InitModule(engine, dutObject);
            if (progInfo.TestParamsConfig.GetBoolParam("TraceToneTestEnabled"))
            {
                this.TcmzTraceToneTest_InitModule(engine, dutObject);
            }
            this.IlmzGroupResults_InitModule(engine, dutObject);
            this.CaseTempControl_InitModule(engine, "CaseTemp_Safe", progInfo.TestConditions.HighTemp, 25);   // TODO - tidier method?
            //new added from mapping software


        }

        //
        /// <summary>
        /// Creates an adjusted specifiation for engineering use
        /// </summary>
        /// <param name="engine">reference to test engine</param>
        /// <returns>An adjusted specification</returns>
        private Specification ConstructEngineeringSpec(ITestEngineInit engine)
        {
            engine.SendStatusMsg("Construct Engineering specification");
            GuiMsgs.TcmzEngineeringGuiResponse resp = (GuiMsgs.TcmzEngineeringGuiResponse)engine.ReceiveFromGui().Payload;
            double engAdjustment = (double)resp.FiddleFactor;

            // Calculate power difference
            double upperLimit = Convert.ToDouble(progInfo.MainSpec.GetParamLimit("CH_PEAK_POWER").HighLimit.ValueToString());
            double lowerLimit = Convert.ToDouble(progInfo.MainSpec.GetParamLimit("CH_PEAK_POWER").LowLimit.ValueToString());
            double limitTargetPower = (upperLimit - lowerLimit) / 2 + lowerLimit;
            double adjustment = limitTargetPower - engAdjustment;

            List<String> paramsToAdjust = new List<string>();
            paramsToAdjust.Add("CH_PEAK_POWER");
            paramsToAdjust.Add("CH_PWRCTRL_POWER_MAX_HIGH");
            paramsToAdjust.Add("CH_PWRCTRL_POWER_MAX_LOW");
            paramsToAdjust.Add("CH_PWRCTRL_POWER_MIN_HIGH");
            paramsToAdjust.Add("CH_PWRCTRL_POWER_MIN_LOW");
            paramsToAdjust.Add("TC_FIBRE_TARGET_POWER");
            paramsToAdjust.Add("TC_POWER_CTRL_RANGE_MAX");
            paramsToAdjust.Add("TC_POWER_CTRL_RANGE_MIN");

            Specification powerAdjustedSpec = new Specification(progInfo.MainSpec.Name, progInfo.MainSpec.ActualDatabaseKeys, progInfo.MainSpec.Priority);
            foreach (ParamLimit pl in progInfo.MainSpec)
            {
                Datum highLimit = pl.HighLimit;
                Datum lowLimit = pl.LowLimit;
                if (paramsToAdjust.Contains(pl.InternalName))
                {
                    double oldHighLimit = Convert.ToDouble(pl.HighLimit.ValueToString());
                    double trimmedHighLimit = Math.Truncate(oldHighLimit);
                    if (trimmedHighLimit != 999 && trimmedHighLimit != 9999)
                    {
                        DatumDouble newHighLimit = new DatumDouble(pl.InternalName, oldHighLimit - adjustment);
                        highLimit = (Datum)newHighLimit;
                    }

                    double oldLowLimit = Convert.ToDouble(pl.LowLimit.ValueToString());
                    double trimmedLowLimit = Math.Truncate(oldLowLimit);
                    if (trimmedLowLimit != -999 && trimmedLowLimit != -9999)
                    {
                        DatumDouble newLowLimit = new DatumDouble(pl.InternalName, oldLowLimit - adjustment);
                        lowLimit = (Datum)newLowLimit;
                    }
                }
                RawParamLimit rpl = new RawParamLimit(pl.InternalName, pl.ParamType, lowLimit, highLimit, pl.Operand, pl.Priority, pl.AccuracyFactor, pl.Units);
                powerAdjustedSpec.Add(rpl);
            }
            return powerAdjustedSpec;
        }

        /// <summary>
        /// if no pcas result for mapping, get nessesary information by manual
        /// it is useful for Eng debug
        /// </summary>
        /// <param name="engine"></param>
        public void ManualEntryMappingReults(ITestEngineInit engine)
        {
            engine.GuiShow();
            engine.GuiToFront();
            engine.SendToGui(new GuiMsgs.tosaMappingResultRequst());

            engine.SendStatusMsg("Construct Engineering mapping data");
            GuiMsgs.tosaMappingResponse resp = (GuiMsgs.tosaMappingResponse)engine.ReceiveFromGui().Payload;

            this._mapResult = resp.mapReult;
            if (this._mapResult == null)
            {
                string errorDescription = " No TOSA Mapping Data ,\n" +
                    "Please test this device at Hitt_Map stage first!";

                engine.ErrorInProgram(errorDescription);
            }
            try
            {
                string MAP_TC_NUM_CHAN_REQUIRED = this._mapResult.ReadSint32("TC_NUM_CHAN_REQUIRED").ToString();
                string MAP_TC_OPTICAL_FREQ_START = this._mapResult.ReadDouble("TC_OPTICAL_FREQ_START").ToString();//Jack.Zhang
                //string MAP_TC_OPTICAL_FREQ_START = this._mapResult.ReadDouble("TC_OPTICAL_FREQ_START").ToString();
            }
            catch
            {
                string errorDescription = "Can't load this 2 map parameters: " +
                    "'TC_NUM_CHAN_REQUIRED' & 'TC_OPTICAL_FREQ_START'; please contact Product Engineer!";
                engine.ShowContinueUserQuery(errorDescription);
                engine.ErrorInProgram(errorDescription);
            }
            if (!this._mapResult.ReadString("TEST_STATUS").ToLower().Contains("pass"))
            {
                string errorDescription = "No passed TCMZ_Map stage data in PCAS,\n" +
                    "Please test this device at Hitt_Map stage first!";
                engine.ShowContinueUserQuery(errorDescription);
                engine.ErrorInProgram(errorDescription);
            }

            // Get data from ITU operating file
            string closeGridCharFileName = this._mapResult.ReadFileLinkFullPath("CG_CHAR_ITU_FILE");
            this.closeGridCharData = Bookham.Toolkit.CloseGrid.CsvDataLookup.GetItuOperatingPoints(closeGridCharFileName);


        }

        #region Initialise modules
        /// <summary>
        /// Configure Initialise Device module
        /// </summary>
        private void IlmzInitialise_InitModule(ITestEngineInit engine, DUTObject dutObject)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("IlmzInitialise", "IlmzInitialise", "");

            // Add instruments
            //modRun.Instrs.Add("Osa", (Instrument)progInfo.Instrs.Osa);
            modRun.ConfigData.AddReference("MzInstruments", progInfo.Instrs.Mz);
            switch (progInfo.DsdbrInstrsUsed)
            {
                // added FCU2ASIC Option by Alice.Huang, for TOSA GB test     2010-02-01
                case DsdbrDriveInstruments.FCU2AsicInstrument:
                    modRun.ConfigData.AddReference("FCU2AsicInstruments",
                                            progInfo.Instrs.Fcu2AsicInstrsGroups);
                    break;
                case DsdbrDriveInstruments.FCUInstruments:
                    modRun.ConfigData.AddReference("FcuInstruments", progInfo.Instrs.Fcu);
                    break;
            }

            // Add config data
            TcmzInitialise_Config tcmzInitConfig = new TcmzInitialise_Config();
            DatumList datumsRequired = tcmzInitConfig.GetDatumList();
            ExtractAndAddDatums(modRun, datumsRequired);
            modRun.ConfigData.AddSint32("NumChans", progInfo.TestConditions.NbrChannels);
            modRun.ConfigData.AddDouble("FreqLow_GHz", progInfo.TestConditions.ItuLowFreq_GHz);
            modRun.ConfigData.AddDouble("FreqHigh_GHz", progInfo.TestConditions.ItuHighFreq_GHz);
            modRun.ConfigData.AddDouble("FreqSpace_GHz", progInfo.TestConditions.ItuSpacing_GHz);
            modRun.ConfigData.AddString("DutSerialNbr", dutObject.SerialNumber);
            modRun.ConfigData.AddString("MzFileDirectory", MzFileDirectory);
            modRun.ConfigData.AddEnum("DsdbrInstrsGroup", progInfo.DsdbrInstrsUsed);

        }

        private string GetCurrentTimeStamp()
        {
            if (TestTimeStamp == null)
            {
                System.DateTime currentTime = new System.DateTime();
                currentTime = System.DateTime.Now;

                //assume the year is four digital number
                string year = currentTime.Year.ToString();
                string month = currentTime.Month > 9 ? currentTime.Month.ToString() : "0" + currentTime.Month.ToString();
                string day = currentTime.Day > 9 ? currentTime.Day.ToString() : "0" + currentTime.Day.ToString();
                string hours = currentTime.Hour > 9 ? currentTime.Hour.ToString() : "0" + currentTime.Hour.ToString();
                string minute = currentTime.Minute > 9 ? currentTime.Minute.ToString() : "0" + currentTime.Minute.ToString();
                string second = currentTime.Second > 9 ? currentTime.Second.ToString() : "0" + currentTime.Second.ToString();
                TestTimeStamp = string.Format("{0}{1}{2}{3}{4}{5}",
                    year, month, day,
                    hours, minute, second);
            }
            return TestTimeStamp;
        }

        /// <summary>
        /// Configure a Device Temperature Control module
        /// </summary>
        private void DeviceTempControl_InitModule(ITestEngineInit engine, string moduleName,
            IInstType_SimpleTempControl tecController, string configPrefix)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun(moduleName, "SimpleTempControl", "");

            // Add instrument
            modRun.Instrs.Add("Controller", (Instrument)tecController);

            //load config
            double timeout_S = this.progInfo.TestParamsConfig.GetDoubleParam(configPrefix + "_Timeout_S");
            double stabTime_S = this.progInfo.TestParamsConfig.GetDoubleParam(configPrefix + "_StabiliseTime_S");
            int updateTime_mS = this.progInfo.TestParamsConfig.GetIntParam(string.Format("{0}_UpdateTime_mS", configPrefix));
            double sensorSetPoint_C = this.progInfo.TestParamsConfig.GetDoubleParam(string.Format("{0}_SensorTemperatureSetPoint_C", configPrefix));
            double tolerance_C = this.progInfo.TestParamsConfig.GetDoubleParam(string.Format("{0}_Tolerance_C", configPrefix));

            // Add config data
            DatumList tempConfig = new DatumList();
            tempConfig.AddDouble("datumMaxExpectedTimeForOperation_s", timeout_S);
            tempConfig.AddDouble("RqdStabilisationTime_s", stabTime_S);
            tempConfig.AddSint32("TempTimeBtwReadings_ms", updateTime_mS);
            tempConfig.AddDouble("SetPointTemperature_C", sensorSetPoint_C);
            tempConfig.AddDouble("TemperatureTolerance_C", tolerance_C);

            modRun.ConfigData.AddListItems(tempConfig);
        }

        /// <summary>
        /// Configure Case Temperature module
        /// </summary>
        private void CaseTempControl_InitModule(ITestEngineInit engine,
            string moduleName, Double currentTemp, Double tempSetPoint)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun(moduleName, "SimpleTempControl", "");

            // Add instrument
            modRun.Instrs.Add("Controller", (Instrument)progInfo.Instrs.TecCase);

            // Add config data
            TempTablePoint tempCal = progInfo.TempConfig.GetTemperatureCal(currentTemp, tempSetPoint)[0];
            DatumList tempConfig = new DatumList();
            int guiTimeOut_ms = this.progInfo.TestParamsConfig.GetIntParam("CaseTempGui_UpdateTime_mS");
            // Timeout = 2 * calibrated time, minimum of 60secs
            double timeout_s = Math.Max(tempCal.DelayTime_s * 15, 60);
            tempConfig.AddDouble("datumMaxExpectedTimeForOperation_s", timeout_s);
            // Stabilisation time = 0.5 * calibrated time
            tempConfig.AddDouble("RqdStabilisationTime_s", tempCal.DelayTime_s);
            tempConfig.AddSint32("TempTimeBtwReadings_ms", guiTimeOut_ms);
            // Actual temperature to set
            tempConfig.AddDouble("SetPointTemperature_C", tempSetPoint + tempCal.OffsetC);
            tempConfig.AddDouble("TemperatureTolerance_C", tempCal.Tolerance_degC);
            modRun.ConfigData.AddListItems(tempConfig);
        }

        /// <summary>
        /// Initialise IlmzPowerUp module
        /// </summary>
        /// <param name="engine"></param>
        private void IlmzPowerUp_InitModule(ITestEngineInit engine, InstrumentCollection instrs)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("IlmzPowerUp", "IlmzPowerUp", "");

            // Add instruments
            modRun.Instrs.Add("OpticalPowerMeter", (Instrument)progInfo.Instrs.Mz.PowerMeter);
            modRun.Instrs.Add(instrs);
            modRun.ConfigData.AddReference("MzInstruments", progInfo.Instrs.Mz);

            // Add config data
            modRun.ConfigData.AddDouble("MzTapBias_V", progInfo.TestParamsConfig.GetDoubleParam("MzTapBias_V"));
            modRun.ConfigData.AddDouble("PowerupMinipower_mW", progInfo.TestParamsConfig.GetDoubleParam("PowerupMinipower_mW"));
            modRun.Limits.AddParameter(progInfo.MainSpec, "MZ_MOD_LEFT_DARK_I", "MzLeftModDarkCurrent_mA");
            modRun.Limits.AddParameter(progInfo.MainSpec, "MZ_MOD_RIGHT_DARK_I", "MzRightModDarkCurrent_mA");
            modRun.Limits.AddParameter(progInfo.MainSpec, "TAP_COMP_DARK_I", "MzTapCompDarkCurrent_mA");
            modRun.Limits.AddParameter(progInfo.MainSpec, "ITX_DARK", "TxLockerDarkCurrent_mA");
            modRun.Limits.AddParameter(progInfo.MainSpec, "IRX_DARK", "RxLockerDarkCurrent_mA");

        }

        /// <summary>
        /// Initialise IlmzCrossCalibration module
        /// </summary>
        /// <param name="engine"></param>
        private void IlmzCrossCalibration_InitModule(ITestEngineInit engine, InstrumentCollection instrs)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("IlmzCrossCalibration", "TcmzCrossCalibration", "");
            modRun.ConfigData.AddDouble("FreqDivTolerance_GHz", Convert.ToDouble(
                progInfo.MainSpec.GetParamLimit("XCAL_CH_MID_MAP_GB").HighLimit.ValueToString()));
            // Add instruments

            // Add config data
            modRun.ConfigData.AddSint32("OpticalCal_delay_ms", 2000);

            modRun.Instrs.Add("Optical_switch", (Instrument)progInfo.Instrs.TecDsdbr);
            modRun.Instrs.Add(instrs);

            // Tie up to limits
            modRun.Limits.AddParameter(progInfo.MainSpec, "XCAL_CH_LO_MAP_GB", "XCAL_CH_LO");
            modRun.Limits.AddParameter(progInfo.MainSpec, "XCAL_CH_MID_MAP_GB", "XCAL_CH_MID");
            modRun.Limits.AddParameter(progInfo.MainSpec, "XCAL_CH_HI_MAP_GB", "XCAL_CH_HI");
        }

        /// <summary>
        /// Initialise IlmzPowerHeadCal module
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutobject"></param>
        private void IlmzPowerHeadCal_InitModule(ITestEngineInit engine, DUTObject dutobject)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("IlmzPowerHeadCal", "TcmzPowerHeadCal", "");

            // Add instruments
            modRun.Instrs.Add("OpmReference", (Instrument)progInfo.Instrs.OpmReference);
            modRun.Instrs.Add("Wavemeter", (Instrument)progInfo.Instrs.Wavemeter);
            modRun.Instrs.Add("OpmMZ", (Instrument)progInfo.Instrs.Mz.PowerMeter);
            switch (progInfo.DsdbrInstrsUsed)
            {
                // added FCU2ASIC Option by Alice.Huang, for TOSA GB test     2010-02-01
                case DsdbrDriveInstruments.FCU2AsicInstrument:
                case DsdbrDriveInstruments.FCUInstruments:
                    break;
                case DsdbrDriveInstruments.PXIInstruments:
                    modRun.Instrs.Add("OpmCgDirect", (Instrument)progInfo.Instrs.OpmCgDirect);
                    modRun.Instrs.Add("OpmCgFilter", (Instrument)progInfo.Instrs.OpmCgFilter);
                    break;
            }

            // Add any config data here ...
            modRun.ConfigData.AddString("TestStage", TestStage);
            modRun.ConfigData.AddSint32("OpticalCal_delay_ms", 2000);
            modRun.ConfigData.AddDouble("OpticalCal_MZ_Max", progInfo.TestParamsConfig.GetDoubleParam("OpticalCal_MZ_Max"));
            modRun.ConfigData.AddDouble("OpticalCal_MZ_Min", progInfo.TestParamsConfig.GetDoubleParam("OpticalCal_MZ_Min"));
            modRun.ConfigData.AddString("PlotFileDirectory", MzFileDirectory);
            modRun.ConfigData.AddString("SN", dutobject.SerialNumber);
            switch (progInfo.DsdbrInstrsUsed)
            {
                // added FCU2ASIC Option by Alice.Huang, for TOSA GB test     2010-02-01
                case DsdbrDriveInstruments.FCU2AsicInstrument:

                case DsdbrDriveInstruments.FCUInstruments:
                    modRun.ConfigData.AddBool("IsCalPXIT", false);
                    break;
                case DsdbrDriveInstruments.PXIInstruments:
                    modRun.ConfigData.AddBool("IsCalPXIT", true);
                    modRun.ConfigData.AddDouble("OpticalCal_CgDirect_Max", progInfo.TestParamsConfig.GetDoubleParam("OpticalCal_CgDirect_Max"));
                    modRun.ConfigData.AddDouble("OpticalCal_CgDirect_Min", progInfo.TestParamsConfig.GetDoubleParam("OpticalCal_CgDirect_Min"));
                    break;
            }
        }



        /// <summary>
        /// Initialise IlmzMzCharacterise module
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        private void IlmzMzCharacterise_InitModule(ITestEngineInit engine, DUTObject dutObject, InstrumentCollection instrs)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("IlmzMzCharacterise", "IlmzMzCharacterise_ND", "");

            // Add instruments
            modRun.ConfigData.AddReference("MzInstruments", progInfo.Instrs.Mz);

            modRun.Instrs.Add("FCU", (Instrument)(Inst_Fcu2Asic)instrs["Fcu2Asic"]);
            modRun.Instrs.Add("Optical_switch", (Instrument)progInfo.Instrs.TecDsdbr);


            TcmzMzSweep_Config tcmzConfig = new TcmzMzSweep_Config();
            DatumList datumsRequired = tcmzConfig.GetDatumList();
            ExtractAndAddDatums(modRun, datumsRequired);

            double mzBias1 = progInfo.TestParamsConfig.GetDoubleParam("MzFixedModBias_1_V");
            double mzBias2 = progInfo.TestParamsConfig.GetDoubleParam("MzFixedModBias_2_V");

            //START MODIFICATION, 2007.09.26, BY Ken.Wu ( TCMZ Vpi Correction )
            // Read values needed for TCMZ Vpi calculation from config file.
            double tuneISOAPowerSlope
                = progInfo.TestParamsConfig.GetDoubleParam(externalKeyTuneISOAPowerSlope);

            double tuneOpticalPowerToleranceInmW
                = ((DatumDouble)(progInfo.MainSpec.GetParamLimit(externalKeyTuneOpticalPowerToleranceInmW).HighLimit)).Value;

            double tuneOpticalPowerToleranceIndB
                = progInfo.TestParamsConfig.GetDoubleParam(externalKeyTuneOpticalPowerToleranceIndB);

            //Echoxl.wang 2011-02-18
            //add locker pot value to ensure we can get correct ctap value;
            modRun.ConfigData.AddDouble("locker_tran_pot", locker_pot_value);
            modRun.ConfigData.AddDoubleArray("MZFixedModBias_volt", new double[] { mzBias1, mzBias2 });
            // Alice.Huang   2010-04-27
            // add SOA Current value for convenience
            //modRun.ConfigData.AddDouble("SoaSetupCurrent_mA", 
            //                progInfo.TestParamsConfig.GetDoubleParam("MzChrSetupSoa_mA"));
            modRun.ConfigData.AddDouble("TuneISOAPowerSlope", tuneISOAPowerSlope);
            modRun.ConfigData.AddDouble("TuneOpticalPowerToleranceInmW", tuneOpticalPowerToleranceInmW);
            modRun.ConfigData.AddDouble("TargetFibrePower_dBm",
                            progInfo.TestConditions.TargetFibrePower_dBm);
            modRun.ConfigData.AddDouble("PowerlevelingOffset_dB",
                 ((DatumDouble)progInfo.MainSpec.GetParamLimit("TC_MZ_PWR_CHR_OFFSET").HighLimit).Value);
            //END MODIFICATION, 2007.09.26, By Ken.Wu
            modRun.ConfigData.AddDouble("VcmFactor_FirstChan", progInfo.TestParamsConfig.GetDoubleParam("MzVcmFactor_FirstChan_ForMzChar"));
            modRun.ConfigData.AddDouble("VcmFactor_MiddleChan", progInfo.TestParamsConfig.GetDoubleParam("MzVcmFactor_MiddleChan_ForMzChar"));
            modRun.ConfigData.AddDouble("VcmFactor_LastChan", progInfo.TestParamsConfig.GetDoubleParam("MzVcmFactor_LastChan_ForMzChar"));
            modRun.ConfigData.AddSint32("NumberOfPoints", progInfo.TestParamsConfig.GetIntParam("MzSweepNumberOfPoints"));
            modRun.ConfigData.AddString("DutSerialNbr", dutObject.SerialNumber);
            modRun.ConfigData.AddReference("TestSpec", progInfo.MainSpec);
            modRun.ConfigData.AddString("ResultDir", this.MzFileDirectory);
            modRun.ConfigData.AddDouble("LeftBias_V_LI_Sweep", progInfo.TestParamsConfig.GetDoubleParam("LeftBias_V_LI_Sweep"));
            modRun.ConfigData.AddDouble("RightBias_V_LI_Sweep", progInfo.TestParamsConfig.GetDoubleParam("RightBias_V_LI_Sweep"));
            modRun.ConfigData.AddBool("ArmSourceByAsic", progInfo.TestParamsConfig.GetBoolParam("ArmSourceByAsic"));
            modRun.ConfigData.AddDouble("MidChannelFrequency", progInfo.TestParamsConfig.GetDoubleParam("MidChannelFrequency"));


            #region Add below configdata for Wafer 50

            modRun.ConfigData.AddDouble("LeftBias_V_LI_Sweep_FirstChan", progInfo.TestParamsConfig.GetDoubleParam("LeftBias_V_LI_Sweep_FirstChan"));//
            modRun.ConfigData.AddDouble("RightBias_V_LI_Sweep_FirstChan", progInfo.TestParamsConfig.GetDoubleParam("RightBias_V_LI_Sweep_FirstChan"));
            modRun.ConfigData.AddDouble("LeftBias_V_LI_Sweep_MiddleChan", progInfo.TestParamsConfig.GetDoubleParam("LeftBias_V_LI_Sweep_MiddleChan"));
            modRun.ConfigData.AddDouble("RightBias_V_LI_Sweep_MiddleChan", progInfo.TestParamsConfig.GetDoubleParam("RightBias_V_LI_Sweep_MiddleChan"));
            modRun.ConfigData.AddDouble("LeftBias_V_LI_Sweep_LastChan", progInfo.TestParamsConfig.GetDoubleParam("LeftBias_V_LI_Sweep_LastChan"));            
            modRun.ConfigData.AddDouble("RightBias_V_LI_Sweep_LastChan", progInfo.TestParamsConfig.GetDoubleParam("RightBias_V_LI_Sweep_LastChan"));
            modRun.ConfigData.AddDouble("shiftLVWhenQuadLlessThan", progInfo.TestParamsConfig.GetDoubleParam("shiftLVWhenQuadLlessThan"));//-4,

            double maxPeakArmBias = progInfo.TestParamsConfig.GetDoubleParam("MaxPeakArmBias_Nitride");//-6.3

            //if (this._cocResult != null && _cocResult.IsPresent("WAFER_ID"))
            //{
            //    string wafer_id = this._cocResult.ReadString("WAFER_ID");
            //    string oxideWafer = progInfo.TestParamsConfig.GetStringParam("OxideWafer"); // CIL0050
            //    if (wafer_id.ToUpper().Contains(oxideWafer))
            //    {
            //        this.isOxideWafer = true;
            //        modRun.ConfigData.AddBool("OxideWafer", true);
            //        maxPeakArmBias = progInfo.TestParamsConfig.GetDoubleParam("MaxPeakArmBias_Oxide");//-5
            //    }
            //    else
            //    {
            //        modRun.ConfigData.AddBool("OxideWafer", false);
            //    }
            //}
            //else
            //{
            //    modRun.ConfigData.AddBool("OxideWafer", false);
            //}
            modRun.ConfigData.AddDouble("MaxPeakArmBiasV", maxPeakArmBias);

            #endregion

            //#warning Add MZTapBias_V to config class
            modRun.ConfigData.AddDouble("MZInlineTapBias_V", modRun.ConfigData.ReadDouble("MZTapBias_V"));
            modRun.ConfigData.AddDouble("MzFixedModBias_volt_firstChan", progInfo.TestParamsConfig.GetDoubleParam("MzFixedModBias_volt_FirstChan"));//-4
            modRun.ConfigData.AddDouble("MzFixedModBias_volt_middleChan", progInfo.TestParamsConfig.GetDoubleParam("MzFixedModBias_volt_MiddleChan"));
            modRun.ConfigData.AddDouble("MzFixedModBias_volt_lastChan", progInfo.TestParamsConfig.GetDoubleParam("MzFixedModBias_volt_LastChan"));//-3
            modRun.ConfigData.AddDouble("Vmax_firstChan", progInfo.TestParamsConfig.GetDoubleParam("MzVcmFactor_FirstChan_Vmax"));//-1
            modRun.ConfigData.AddDouble("Vmax_middleChan", progInfo.TestParamsConfig.GetDoubleParam("MzVcmFactor_MiddleChan_Vmax"));
            modRun.ConfigData.AddDouble("Vmax_lastChan", progInfo.TestParamsConfig.GetDoubleParam("MzVcmFactor_LastChan_Vmax"));//-0.5
            modRun.ConfigData.AddDouble("Target_ER", progInfo.TestParamsConfig.GetDoubleParam("Target_ER"));

            // Limits
            modRun.Limits.AddParameter(progInfo.MainSpec, "MZ_INITIAL_CAL_SWEEP_FILE", "MzSweepDataZipFile");
            modRun.Limits.AddParameter(progInfo.MainSpec, "MZ_CAL_EST_FILE", "MzSweepDataResultsFile");
            modRun.Limits.AddParameter(progInfo.MainSpec, "MZ_CAL_EST_FLAG", "AllSweepsPassed");

        }

        /// <summary>
        /// Initialise IlmzMzItuEstimate module
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        private void IlmzMzItuEstimate_InitModule(ITestEngineInit engine, DUTObject dutObject)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("IlmzMzItuEstimate", "IlmzMzItuEstimate_ND", "");

            // Add instruments
            modRun.ConfigData.AddReference("MzInstruments", progInfo.Instrs.Mz);

            // Add config data
            modRun.Instrs.Add("Optical_switch", (Instrument)progInfo.Instrs.TecDsdbr);

            modRun.ConfigData.AddDouble("locker_tran_pot", locker_pot_value);
            modRun.ConfigData.AddReference("Specification", progInfo.MainSpec);
            modRun.ConfigData.AddDouble("MzTapBias_V", progInfo.TestParamsConfig.GetDoubleParam("MzTapBias_V"));
            modRun.ConfigData.AddDouble("MZInlineTapBias_V", progInfo.TestParamsConfig.GetDoubleParam("MzTapBias_V"));
            modRun.ConfigData.AddDouble("CalOffset", progInfo.TestConditions.CalOffset);
            modRun.ConfigData.AddDouble("HighTemp", progInfo.TestConditions.HighTemp);
            modRun.ConfigData.AddDouble("LowTemp", progInfo.TestConditions.LowTemp);
            modRun.ConfigData.AddDouble("FreqLow_GHz", progInfo.TestConditions.ItuLowFreq_GHz);
            modRun.ConfigData.AddDouble("FreqHigh_GHz", progInfo.TestConditions.ItuHighFreq_GHz);
            modRun.ConfigData.AddDouble("FreqSpace_GHz", progInfo.TestConditions.ItuSpacing_GHz);
            //if (isOxideWafer)
            //{
            //    modRun.ConfigData.AddDouble("MzCtrlINominal_mA", progInfo.TestParamsConfig.GetDoubleParam("Oxide_MzCtrlINominal_mA"));
            //}
            //else
            //{
            //    modRun.ConfigData.AddDouble("MzCtrlINominal_mA", progInfo.TestParamsConfig.GetDoubleParam("MzCtrlINominal_mA"));
            //}
            modRun.ConfigData.AddDouble("MzCtrlINominal_mA", progInfo.TestParamsConfig.GetDoubleParam("MzCtrlINominal_mA"));
            modRun.ConfigData.AddString("DUTSerialNbr", dutObject.SerialNumber);
            modRun.ConfigData.AddString("MzFileDirectory", MzFileDirectory);
            modRun.ConfigData.AddDouble("Vmax", progInfo.TestParamsConfig.GetDoubleParam("MzVcmFactor_Vmax"));
            modRun.ConfigData.AddDouble("Vmax_lastChan", progInfo.TestParamsConfig.GetDoubleParam("MzVcmFactor_LastChan_Vmax"));
            modRun.ConfigData.AddBool("ArmSourceByAsic", progInfo.TestParamsConfig.GetBoolParam("ArmSourceByAsic"));
            modRun.ConfigData.AddDouble("MidChannelFrequency", progInfo.TestParamsConfig.GetDoubleParam("MidChannelFrequency"));

            // for Vpi correction
            double VpiCorrectionUpperAdjustDelta = progInfo.TestParamsConfig.GetDoubleParam("VpiCorrectionUpperAdjustDelta");
            double VpiCorrectionLowerAdjustDelta = progInfo.TestParamsConfig.GetDoubleParam("VpiCorrectionLowerAdjustDelta");
            modRun.ConfigData.AddDouble("VpiCorrectionUpperAdjustDelta", VpiCorrectionUpperAdjustDelta);
            modRun.ConfigData.AddDouble("VpiCorrectionLowerAdjustDelta", VpiCorrectionLowerAdjustDelta);
            modRun.ConfigData.AddSint32("NumberOfPoints", progInfo.TestParamsConfig.GetIntParam("MzSweepNumberOfPoints"));
            modRun.ConfigData.AddString("DutSerialNbr", dutObject.SerialNumber);
            // Add limits
            modRun.Limits.AddParameter(progInfo.MainSpec, "MZ_CAL_EST_FILE_FINAL_VCM", "MzAtNewVcmResultsFile");
            modRun.Limits.AddParameter(progInfo.MainSpec, "MZ_CAL_EST_FLAG_FINAL_VCM", "MzAtNewVcmPass");
        }

        /// <summary>
        /// Configure Channel Characterisation module
        /// </summary>
        private void IlmzChannelChar_InitModule(ITestEngineInit engine, DUTObject dutObject)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("IlmzChannelChar", "IlmzChannelChar_ND", "");
            modRun.PreviousTestData.AddString("LASER_WAFER_SIZE", LASER_WAFER_SIZE == null ? string.Empty : LASER_WAFER_SIZE);
            //modRun.PreviousTestData.AddStringArray("SmFiles", SmFiles);
            modRun.PreviousTestData.AddString("BandType", progInfo.TestConditions.FreqBand.ToString());
            //modRun.PreviousTestData.AddStringArray("NUM_LM", NUM_LM);

            // Add instruments
            modRun.ConfigData.AddReference("MzInstruments", progInfo.Instrs.Mz);

            modRun.Instrs.Add("Optical_switch", (Instrument)progInfo.Instrs.TecDsdbr);
            modRun.ConfigData.AddDouble("locker_tran_pot", locker_pot_value);
            modRun.Instrs.Add("OSA", (Instrument)progInfo.Instrs.Osa);
            // modRun.Instrs.Add("MzTec", (Instrument)progInfo.Instrs.TecMz);
            modRun.Instrs.Add("DsdbrTec", (Instrument)progInfo.Instrs.TecDsdbr);

            switch (progInfo.DsdbrInstrsUsed)
            {
                // added FCU2ASIC Option by Alice.Huang, for TOSA GB test     2010-02-01
                case DsdbrDriveInstruments.FCU2AsicInstrument:
                case DsdbrDriveInstruments.FCUInstruments:
                    modRun.Instrs.Add("PowerHead", (Instrument)progInfo.Instrs.Mz.PowerMeter);
                    break;
                case DsdbrDriveInstruments.PXIInstruments:
                    modRun.Instrs.Add("PowerHead", (Instrument)progInfo.Instrs.OpmCgDirect);
                    break;
            }

            // Add config data
            TcmzChannelChar_ND_Config tcmzChanCharConfig = new TcmzChannelChar_ND_Config();
            DatumList datumsRequired = tcmzChanCharConfig.GetDatumList();
            ExtractAndAddDatums(modRun, datumsRequired);

            TcmzMzSweep_Config tcmzConfig = new TcmzMzSweep_Config();
            datumsRequired = tcmzConfig.GetDatumList();
            ExtractAndAddDatums(modRun, datumsRequired);

            //modRun.ConfigData.AddReference("SwitchOsaMzOpm", progInfo.Instrs.SwitchOsaMzOpm);
            modRun.ConfigData.AddString("ResultDir", this.MzFileDirectory);
            modRun.ConfigData.AddReference("TestSelect", progInfo.TestSelect);
            modRun.ConfigData.AddReference("Specification", progInfo.MainSpec);
            modRun.ConfigData.AddReference("FreqBand", progInfo.TestConditions.FreqBand);
            modRun.ConfigData.AddDouble("FreqLow_GHz", progInfo.TestConditions.ItuLowFreq_GHz);
            modRun.ConfigData.AddDouble("FreqHigh_GHz", progInfo.TestConditions.ItuHighFreq_GHz);
            modRun.ConfigData.AddDouble("FreqSpace_GHz", progInfo.TestConditions.ItuSpacing_GHz);
            modRun.ConfigData.AddSint32("NumChans", progInfo.TestConditions.NbrChannels);
            modRun.ConfigData.AddDouble("CaseTempLow_C", progInfo.TestConditions.LowTemp);
            modRun.ConfigData.AddDouble("CaseTempHigh_C", progInfo.TestConditions.HighTemp);
            modRun.ConfigData.AddDouble("TargetFibrePower_dBm", progInfo.TestConditions.TargetFibrePower_dBm);
            modRun.ConfigData.AddDouble("Tune_OpticalPowerTolerance_mW",
                ((DatumDouble)progInfo.MainSpec.GetParamLimit(externalKeyTuneOpticalPowerToleranceInmW).HighLimit).Value);
            modRun.ConfigData.AddDouble("CalOffset", progInfo.TestConditions.CalOffset);
            modRun.ConfigData.AddString("DutSerialNbr", dutObject.SerialNumber);
            modRun.ConfigData.AddString("DeviceType", dutObject.PartCode);
            modRun.ConfigData.AddDouble("MzDcVpiSlopeLimitMin", progInfo.TestConditions.MzDcVpiSlopeLimitMin);
            modRun.ConfigData.AddDouble("MzDcVpiSlopeLimitMax", progInfo.TestConditions.MzDcVpiSlopeLimitMax);
            modRun.ConfigData.AddDouble("LockerSlopeEffAbsLimitMin", progInfo.TestConditions.LockerSlopeEffAbsLimitMin);
            modRun.ConfigData.AddDouble("LockerSlopeEffAbsLimitMax", progInfo.TestConditions.LockerSlopeEffAbsLimitMax);
            //if (isOxideWafer)
            //{
            //    modRun.ConfigData.UpdateDouble("MzCtrlINominal_mA", progInfo.TestParamsConfig.GetDoubleParam("Oxide_MzCtrlINominal_mA"));
            //}
            //else
            //{
            //    modRun.ConfigData.UpdateDouble("MzCtrlINominal_mA", progInfo.TestParamsConfig.GetDoubleParam("MzCtrlINominal_mA"));
            //}
            modRun.ConfigData.UpdateDouble("MzCtrlINominal_mA", progInfo.TestParamsConfig.GetDoubleParam("MzCtrlINominal_mA"));

            try
            {
                modRun.ConfigData.AddDouble("LockerSlope_FreqOffset_GHz",
                    progInfo.TestParamsConfig.GetDoubleParam("LockerSlope_FreqOffset_GHz"));
            }
            catch { }

            modRun.ConfigData.AddDouble("MaxSoaForPwrLeveling", Convert.ToDouble(
                progInfo.MainSpec.GetParamLimit("CH_ITU_SOA_I").HighLimit.ValueToString()));
            bool loggingEnabled = true;
            try
            {
                loggingEnabled = this.progInfo.TestParamsConfig.GetBoolParam("LoggintEnabled");
            }
            catch (Exception /*e*/ ) { }
            modRun.ConfigData.AddBool("LoggingEnabled", loggingEnabled);

#warning Add to config
            modRun.ConfigData.AddBool("AlwaysTuneLaser", true);
            //modRun.ConfigData.AddString("IPhaseModAcqFilesCollection", this._mapResult.ReadFileLinkFullPath("CG_PHASE_MODE_ACQ_FILE"));

            // Tie up limits
            Specification spec = progInfo.MainSpec;
            modRun.Limits.AddParameter(spec, "TCASE_MID_PASS_FAIL_FLAG", "PassFailFlag");
            modRun.Limits.AddParameter(spec, "NUM_CHAN_TEST_TCASE_MID", "NumChannelsTested");
            modRun.Limits.AddParameter(spec, "NUM_CHAN_PASS_TCASE_MID", "NumChannelsPass");
            modRun.Limits.AddParameter(spec, "CHANNEL_RAW_DATA_FILE", "ChannelPlotData");
            modRun.Limits.AddParameter(spec, "FULL_LV_SWEEP_FLAG", "FullLvSweepFlag");
            modRun.Limits.AddParameter(spec, "FULL_LK_SLOPE_FLAG", "FullLkSlopeTest");

        }

        /// <summary>
        /// Initialise IlmzLowTempTest module
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        private void IlmzLowTempTest_InitModule(ITestEngineInit engine, DUTObject dutObject)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("IlmzLowTempTest", "IlmzTempTest_ND", "");

            // Add instruments
            modRun.ConfigData.AddReference("MzInstruments", progInfo.Instrs.Mz);
            //modRun.Instrs.Add("MzTec", (Instrument)progInfo.Instrs.TecMz);
            modRun.Instrs.Add("DsdbrTec", (Instrument)progInfo.Instrs.TecDsdbr);
            modRun.Instrs.Add("Optical_switch", (Instrument)progInfo.Instrs.TecDsdbr);

            modRun.ConfigData.AddDouble("locker_tran_pot", locker_pot_value);
            //modRun.ConfigData.AddReference("SwitchOsaMzOpm", progInfo.Instrs.SwitchOsaMzOpm);

            // Add config data
            TcmzMzSweep_Config tcmzConfig = new TcmzMzSweep_Config();
            DatumList datumsRequired = tcmzConfig.GetDatumList();
            ExtractAndAddDatums(modRun, datumsRequired);
            modRun.ConfigData.AddEnum("Temp", TcmzTestTemp.Low);
            modRun.ConfigData.AddReference("TestSelect", progInfo.TestSelect);
            modRun.ConfigData.AddString("DutSerialNbr", dutObject.SerialNumber);
            //Modify non-rules old statment at 2008-10-10 by tim;
            //modRun.ConfigData.AddBool("DoPwrCtrlRangeTests", dutObject.PartCode.ToLower().Contains("lmt10ncc"));
            modRun.ConfigData.AddBool("DoPwrCtrlRangeTests", progInfo.TestParamsConfig.GetBoolParam("DoPwrCtrlRangeTests"));
            //Modify non-ruless old statment at 2008-10-10;
            //modRun.ConfigData.AddBool("DoTapLevelling", !dutObject.PartCode.ToLower().Contains("lmt10ncb"));
            modRun.ConfigData.AddBool("DoTapLevelling", progInfo.TestParamsConfig.GetBoolParam("DoTapLevelling"));
            modRun.ConfigData.AddDouble("TapPhotoCurrentToleranceRatio",
            progInfo.TestParamsConfig.GetDoubleParam("TapPhotoCurrentToleranceRatio"));
            //modRun.ConfigData.AddDouble("MZModSweepMax_volt", progInfo.TestParamsConfig.GetDoubleParam("MzSeModSweepMax_volt"));
            //modRun.ConfigData.AddDouble("MZFixedModBias_volt", progInfo.TestParamsConfig.GetDoubleParam("MzSeFixedModBias_volt"));
            modRun.ConfigData.AddDouble("PwrControlRangeLow", progInfo.TestConditions.PwrControlRangeLow);
            modRun.ConfigData.AddDouble("PwrControlRangeHigh", progInfo.TestConditions.PwrControlRangeHigh);
            modRun.ConfigData.AddDouble("Tune_OpticalPowerTolerance_dB",
                progInfo.TestParamsConfig.GetDoubleParam("Tune_OpticalPowerTolerance_dB"));
            modRun.ConfigData.AddDouble("Tune_OpticalPowerTolerance_mW",
                ((DatumDouble)progInfo.MainSpec.GetParamLimit(externalKeyTuneOpticalPowerToleranceInmW).HighLimit).Value);
            modRun.ConfigData.AddDouble("Tune_IsoaPowerSlope", progInfo.TestParamsConfig.GetDoubleParam("Tune_IsoaPowerSlope"));
            modRun.ConfigData.AddDouble("MaxSoaForPwrLeveling", Convert.ToDouble(
                progInfo.MainSpec.GetParamLimit("CH_ITU_SOA_I").HighLimit.ValueToString()));
            bool loggingEnabled = true;
            try
            {
                loggingEnabled = this.progInfo.TestParamsConfig.GetBoolParam("LoggintEnabled");
            }
            catch (Exception /*e*/ ) { }
            modRun.ConfigData.AddBool("LoggingEnabled", loggingEnabled);
            modRun.ConfigData.AddBool("TraceToneTestEnabled", progInfo.TestParamsConfig.GetBoolParam("TraceToneTestEnabled"));

            // Tie up limits
            Specification spec = progInfo.MainSpec;
            modRun.Limits.AddParameter(spec, "LOW_DISS_OPTICAL_FREQ", "LOW_DISS_OPTICAL_FREQ");
            modRun.Limits.AddParameter(spec, "LOW_DISS_SOL", "LOW_DISS_SOL");
            modRun.Limits.AddParameter(spec, "PACK_DISS_TCASE_LOW", "PACK_DISS_TCASE_LOW");
            modRun.Limits.AddParameter(spec, "TEC_I_TCASE_LOW", "LASER_TEC_I_TCASE_LOW");
            modRun.Limits.AddParameter(spec, "TEC_V_TCASE_LOW", "LASER_TEC_V_TCASE_LOW");
            //modRun.Limits.AddParameter(spec, "MZ_TEC_I_TCASE_LOW", "MZ_TEC_I_TCASE_LOW");
            //modRun.Limits.AddParameter(spec, "MZ_TEC_V_TCASE_LOW", "MZ_TEC_V_TCASE_LOW");
            modRun.Limits.AddParameter(spec, "NUM_CHAN_TEST_TCASE_LOW", "NumChannelsTested");
            modRun.Limits.AddParameter(spec, "NUM_CHAN_PASS_TCASE_LOW", "NumChannelsPass");
            modRun.Limits.AddParameter(spec, "NUM_LOCK_FAIL_TCASE_LOW", "NumLockFails");
            modRun.Limits.AddParameter(spec, "TCASE_LOW_PASS_FAIL_FLAG", "PassFailFlag");
        }

        /// <summary>
        /// Initialise TcmHighTempTest module
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        private void IlmzHighTempTest_InitModule(ITestEngineInit engine, DUTObject dutObject)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("IlmzHighTempTest", "IlmzTempTest_ND", "");

            // Add instruments
            modRun.ConfigData.AddReference("MzInstruments", progInfo.Instrs.Mz);
            //modRun.Instrs.Add("MzTec", (Instrument)progInfo.Instrs.TecMz);
            modRun.Instrs.Add("DsdbrTec", (Instrument)progInfo.Instrs.TecDsdbr);
            modRun.Instrs.Add("Optical_switch", (Instrument)progInfo.Instrs.TecDsdbr);

            modRun.ConfigData.AddDouble("locker_tran_pot", locker_pot_value);
            //modRun.ConfigData.AddReference("SwitchOsaMzOpm", progInfo.Instrs.SwitchOsaMzOpm);

            // Add config data
            TcmzMzSweep_Config tcmzConfig = new TcmzMzSweep_Config();
            DatumList datumsRequired = tcmzConfig.GetDatumList();
            ExtractAndAddDatums(modRun, datumsRequired);
            modRun.ConfigData.AddEnum("Temp", TcmzTestTemp.High);
            modRun.ConfigData.AddReference("TestSelect", progInfo.TestSelect);
            modRun.ConfigData.AddString("DutSerialNbr", dutObject.SerialNumber);
            //modRun.ConfigData.AddBool("DoPwrCtrlRangeTests", dutObject.PartCode.ToLower().Contains("lmt10ncc"));
            modRun.ConfigData.AddBool("DoPwrCtrlRangeTests", progInfo.TestParamsConfig.GetBoolParam("DoPwrCtrlRangeTests"));
            //Modify non-rules old statment;
            //modRun.ConfigData.AddBool("DoTapLevelling", !dutObject.PartCode.ToLower().Contains("lmt10ncb"));
            modRun.ConfigData.AddBool("DoTapLevelling", progInfo.TestParamsConfig.GetBoolParam("DoTapLevelling"));
            modRun.ConfigData.AddDouble("TapPhotoCurrentToleranceRatio",
                progInfo.TestParamsConfig.GetDoubleParam("TapPhotoCurrentToleranceRatio"));
            modRun.ConfigData.AddDouble("LaserTecEolDeltaT_C", progInfo.TestConditions.LaserTecEolDeltaT_C);
            //modRun.ConfigData.AddDouble("MzTecEolDeltaT_C", progInfo.TestConditions.MzTecEolDeltaT_C);
            modRun.ConfigData.AddDouble("PwrControlRangeLow", Convert.ToDouble(progInfo.MainSpec.GetParamLimit("TC_PWR_CTRL_RANGE_MIN").LowLimit.ValueToString()));
            modRun.ConfigData.AddDouble("PwrControlRangeHigh", Convert.ToDouble(progInfo.MainSpec.GetParamLimit("TC_PWR_CTRL_RANGE_MAX").LowLimit.ValueToString()));
            modRun.ConfigData.AddDouble("Tune_OpticalPowerTolerance_dB",
                progInfo.TestParamsConfig.GetDoubleParam("Tune_OpticalPowerTolerance_dB"));
            modRun.ConfigData.AddDouble("Tune_OpticalPowerTolerance_mW",
                ((DatumDouble)progInfo.MainSpec.GetParamLimit(externalKeyTuneOpticalPowerToleranceInmW).HighLimit).Value);
            modRun.ConfigData.AddDouble("Tune_IsoaPowerSlope",
                progInfo.TestParamsConfig.GetDoubleParam("Tune_IsoaPowerSlope"));
            modRun.ConfigData.AddDouble("MaxSoaForPwrLeveling", Convert.ToDouble(
                progInfo.MainSpec.GetParamLimit("CH_ITU_SOA_I").HighLimit.ValueToString()));
            bool loggingEnabled = true;
            try
            {
                loggingEnabled = this.progInfo.TestParamsConfig.GetBoolParam("LoggintEnabled");
            }
            catch (Exception /*e*/ ) { }
            modRun.ConfigData.AddBool("LoggingEnabled", loggingEnabled);
            modRun.ConfigData.AddBool("TraceToneTestEnabled", progInfo.TestParamsConfig.GetBoolParam("TraceToneTestEnabled"));

            // Tie up limits
            Specification spec = progInfo.MainSpec;
            modRun.Limits.AddParameter(spec, "HIGH_DISS_OPTICAL_FREQ", "HIGH_DISS_OPTICAL_FREQ");
            modRun.Limits.AddParameter(spec, "HIGH_DISS_SOL", "HIGH_DISS_SOL");
            modRun.Limits.AddParameter(spec, "PACK_DISS_TCASE_HIGH", "PACK_DISS_TCASE_HIGH");
            modRun.Limits.AddParameter(spec, "TEC_I_TCASE_HIGH", "LASER_TEC_I_TCASE_HIGH");
            modRun.Limits.AddParameter(spec, "TEC_V_TCASE_HIGH", "LASER_TEC_V_TCASE_HIGH");
            //modRun.Limits.AddParameter(spec, "MZ_TEC_I_TCASE_HIGH", "MZ_TEC_I_TCASE_HIGH");
            //modRun.Limits.AddParameter(spec, "MZ_TEC_V_TCASE_HIGH", "MZ_TEC_V_TCASE_HIGH");
            modRun.Limits.AddParameter(spec, "TEC_I_TCASE_HIGH_EOL", "LASER_TEC_I_TCASE_HIGH_EOL");
            modRun.Limits.AddParameter(spec, "TEC_V_TCASE_HIGH_EOL", "LASER_TEC_V_TCASE_HIGH_EOL");
            //modRun.Limits.AddParameter(spec, "MZ_TEC_I_TCASE_HIGH_EOL", "MZ_TEC_I_TCASE_HIGH_EOL");
            //modRun.Limits.AddParameter(spec, "MZ_TEC_V_TCASE_HIGH_EOL", "MZ_TEC_V_TCASE_HIGH_EOL");
            modRun.Limits.AddParameter(spec, "PACK_DISS_TCASE_HIGH_EOL", "PACK_DISS_TCASE_HIGH_EOL");
            modRun.Limits.AddParameter(spec, "NUM_CHAN_TEST_TCASE_HIGH", "NumChannelsTested");
            modRun.Limits.AddParameter(spec, "NUM_CHAN_PASS_TCASE_HIGH", "NumChannelsPass");
            modRun.Limits.AddParameter(spec, "NUM_LOCK_FAIL_TCASE_HIGH", "NumLockFails");
            modRun.Limits.AddParameter(spec, "TCASE_HIGH_PASS_FAIL_FLAG", "PassFailFlag");
        }

        /// <summary>
        /// Initialise TcmzTraceToneTest module
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        private void TcmzTraceToneTest_InitModule(ITestEngineInit engine, DUTObject dutObject)
        {
            // Initialise module
            ModuleRun modRun = engine.AddModuleRun("TcmzTraceToneTest", "TcmzTraceToneTest", "");
            modRun.ConfigData.AddString("DutSerialNumber", dutObject.SerialNumber);
            modRun.ConfigData.AddString("PlotFileDirectory", MzFileDirectory);

            // Tie up limits when added to spec
            modRun.Limits.AddParameter(progInfo.MainSpec, "I_SOA_1", "I_Soa_1");
            modRun.Limits.AddParameter(progInfo.MainSpec, "MAX_I_SOA_TRACE_SOLOT", "Max_I_Soa_Trace_SOLOT");
            modRun.Limits.AddParameter(progInfo.MainSpec, "MAX_TRACE_I_SOLOT", "Max_Trace_I_SOLOT");
            modRun.Limits.AddParameter(progInfo.MainSpec, "SOA_GRADIENT_SOLOT", "Soa_Gradient_SOLOT");
            modRun.Limits.AddParameter(progInfo.MainSpec, "I_SOA_TRACE_EOLOT1", "I_Soa_Trace_EOLOT1");
            modRun.Limits.AddParameter(progInfo.MainSpec, "TRACE_I_EOLOT1", "Trace_I_EOLOT1");
            modRun.Limits.AddParameter(progInfo.MainSpec, "SOA_GRADIENT_EOLOT1", "Soa_Gradient_EOLOT1");
            modRun.Limits.AddParameter(progInfo.MainSpec, "I_SOA_TRACE_EOLOT2", "I_Soa_Trace_EOLOT2");
            modRun.Limits.AddParameter(progInfo.MainSpec, "TRACE_I_EOLOT2", "Trace_I_EOLOT2");
            modRun.Limits.AddParameter(progInfo.MainSpec, "SOA_GRADIENT_EOLOT2", "Soa_Gradient_EOLOT2");
            modRun.Limits.AddParameter(progInfo.MainSpec, "TRACE_SOA_PLOT", "TraceSoaCurves");
        }

        /// <summary>
        /// Initialise TcmzGroupResults module
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        private void IlmzGroupResults_InitModule(ITestEngineInit engine, DUTObject dutObject)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("IlmzGroupResults", "IlmzGroupResults", "");

            // Add config data
            modRun.ConfigData.AddBool("LowTempTestEnabled", progInfo.TestParamsConfig.GetBoolParam("LowTempTestEnabled"));

            // Tie up limits
            Specification spec = progInfo.MainSpec;
            modRun.Limits.AddParameter(spec, "AVERAGE_LOCK_RATIO", "AVERAGE_LOCK_RATIO");
            modRun.Limits.AddParameter(spec, "OPTICAL_FREQ_FIRST_CHAN_FAIL", "OPTICAL_FREQ_FIRST_CHAN_FAIL");
            modRun.Limits.AddParameter(spec, "FULL_PASS_CHAN_COUNT_OVERALL", "FULL_PASS_CHAN_COUNT_OVERALL");

            modRun.Limits.AddParameter(spec, "FIBRE_POWER_CHAN_TEMP_MIN", "FIBRE_POWER_CHAN_TEMP_MIN");
            modRun.Limits.AddParameter(spec, "FIBRE_POWER_CHAN_TEMP_MAX", "FIBRE_POWER_CHAN_TEMP_MAX");
            modRun.Limits.AddParameter(spec, "FIBRE_POWER_CHANGE_TCASE_HIGH", "FIBRE_POWER_CHANGE_TCASE_HIGH");
            modRun.Limits.AddParameter(spec, "FIBRE_POWER_FAIL_CHAN_TEMP", "FIBRE_POWER_FAIL_CHAN_TEMP");
            modRun.Limits.AddParameter(spec, "FIBRE_POWER_FAIL_OVER_TCASE", "FIBRE_POWER_FAIL_OVER_TCASE");
            modRun.Limits.AddParameter(spec, "TRACK_ERROR_PWR_TCASE", "TRACK_ERROR_PWR_TCASE");

            modRun.Limits.AddParameter(spec, "LOCK_FREQ_CHAN_TEMP_MIN", "LOCK_FREQ_CHAN_TEMP_MIN");
            modRun.Limits.AddParameter(spec, "LOCK_FREQ_CHAN_TEMP_MAX", "LOCK_FREQ_CHAN_TEMP_MAX");
            modRun.Limits.AddParameter(spec, "LOCK_FREQ_CHANGE_TCASE_HIGH", "LOCK_FREQ_CHANGE_TCASE_HIGH");
            modRun.Limits.AddParameter(spec, "LOCK_FREQ_FAIL_CHAN_TEMP", "LOCK_FREQ_FAIL_CHAN_TEMP");
            modRun.Limits.AddParameter(spec, "LOCK_FREQ_FAIL_OVER_TCASE", "LOCK_FREQ_FAIL_OVER_TCASE");
            modRun.Limits.AddParameter(spec, "TRACK_ERROR_FREQ_TCASE", "TRACK_ERROR_FREQ_TCASE");
            modRun.Limits.AddParameter(spec, "TRACK_ERROR_ULFREQ_TCASE", "TRACK_ERROR_ULFREQ_TCASE");

            modRun.Limits.AddParameter(spec, "PHASE_I_CHANGE_TCASE_HIGH", "PHASE_I_CHANGE_TCASE_HIGH");

            if (progInfo.TestParamsConfig.GetBoolParam("LowTempTestEnabled"))
            {
                modRun.Limits.AddParameter(spec, "FIBRE_POWER_CHANGE_TCASE_LOW", "FIBRE_POWER_CHANGE_TCASE_LOW");
                modRun.Limits.AddParameter(spec, "LOCK_FREQ_CHANGE_TCASE_LOW", "LOCK_FREQ_CHANGE_TCASE_LOW");
                modRun.Limits.AddParameter(spec, "PHASE_I_CHANGE_TCASE_LOW", "PHASE_I_CHANGE_TCASE_LOW");
            }
        }

        #endregion

        #region Configure instruments
        /// <summary>
        /// Configure a TEC controller using config data
        /// </summary>
        /// <param name="tecCtlr">Instrument reference</param>
        /// <param name="tecCtlId">Config table data prefix</param>
        private void ConfigureTecController(IInstType_TecController tecCtlr, string tecCtlId)
        {
            SteinhartHartCoefficients stCoeffs = new SteinhartHartCoefficients();

            // Keithley 2510 specific commands (must be done before 'SetDefaultState')
            if (tecCtlr.DriverName.Contains("Ke2510"))
            {
                tecCtlr.HardwareData["MinTemperatureLimit_C"] = progInfo.TestParamsConfig.GetStringParam(tecCtlId + "_MinTemp_C");
                tecCtlr.HardwareData["MaxTemperatureLimit_C"] = progInfo.TestParamsConfig.GetStringParam(tecCtlId + "_MaxTemp_C");
                tecCtlr.Sensor_Type = (InstType_TecController.SensorType)
                Enum.Parse(typeof(InstType_TecController.SensorType), progInfo.TestParamsConfig.GetStringParam(tecCtlId + "_Sensor_Type"));
                tecCtlr.TecVoltageCompliance_volt = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_TecVoltageCompliance_volt");
                tecCtlr.DerivativeGain = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_DerivativeGain");

            }
            // Operating modes

            tecCtlr.SetDefaultState();
            tecCtlr.OperatingMode = (InstType_TecController.ControlMode)
                Enum.Parse(typeof(InstType_TecController.ControlMode), progInfo.TestParamsConfig.GetStringParam(tecCtlId + "_OperatingMode"));

            // Thermistor characteristics
            if (tecCtlr.Sensor_Type == InstType_TecController.SensorType.Thermistor_2wire ||
                tecCtlr.Sensor_Type == InstType_TecController.SensorType.Thermistor_4wire)
            {
                stCoeffs.A = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_stCoeffs_A");
                stCoeffs.B = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_stCoeffs_B");
                stCoeffs.C = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_stCoeffs_C");
                tecCtlr.SteinhartHartConstants = stCoeffs;
            }

            // Additional parameters
            double degree = tecCtlr.SensorTemperatureActual_C;

            tecCtlr.TecCurrentCompliance_amp = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_TecCurrentCompliance_amp");
            tecCtlr.ProportionalGain = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_ProportionalGain");
            tecCtlr.IntegralGain = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_IntegralGain");
            try
            {
                tecCtlr.SensorTemperatureSetPoint_C = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_SensorTemperatureSetPoint_C");
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

        }


        /// <summary>
        /// Configure FCU instruments
        /// </summary>
        /// <param name="fcuInstrs"></param>
        private void ConfigureFCUInstruments(FCUInstruments fcuInstrs)
        {
            fcuInstrs.FullbandControlUnit.ResetCPN();
            fcuInstrs.FullbandControlUnit.EnterProtectMode();
            fcuInstrs.FullbandControlUnit.EnterVendorProtectMode("THURSDAY"); // Need this privilege level
            // in order to use IOIFBkhm interface.
            // This password has to be used.
            // Config FCU calibration data
            progInfo.Instrs.Fcu.FCUCalibrationData = new FCUInstruments.FCUCalData();
            string fcuSerialNumber = fcuInstrs.FullbandControlUnit.GetUnitSerialNumber().Trim();
            double CalFactor;
            double CalOffset;
            // Tx ADC
            readCalData("FCU2Asic", fcuSerialNumber, "Tx ADC", out CalFactor, out CalOffset);
            progInfo.Instrs.Fcu2AsicInstrsGroups.FCUCalibrationData.TxADC_CalFactor = CalFactor;
            progInfo.Instrs.Fcu2AsicInstrsGroups.FCUCalibrationData.TxADC_CalOffset = CalOffset;
            // Rx ADC
            readCalData("FCU2Asic", fcuSerialNumber, "Rx ADC", out CalFactor, out CalOffset);
            progInfo.Instrs.Fcu2AsicInstrsGroups.FCUCalibrationData.RxADC_CalFactor = CalFactor;
            progInfo.Instrs.Fcu2AsicInstrsGroups.FCUCalibrationData.RxADC_CalOffset = CalOffset;
            // TxCoarsePot
            readCalData("FCU2Asic", fcuSerialNumber, "TxCoarsePot", out CalFactor, out CalOffset);
            progInfo.Instrs.Fcu2AsicInstrsGroups.FCUCalibrationData.TxCoarsePot_CalFactor = CalFactor;
            progInfo.Instrs.Fcu2AsicInstrsGroups.FCUCalibrationData.TxCoarsePot_CalOffset = CalOffset;
            // TxFinePot
            readCalData("FCU2Asic", fcuSerialNumber, "TxFinePot", out CalFactor, out CalOffset);
            progInfo.Instrs.Fcu2AsicInstrsGroups.FCUCalibrationData.TxFinePot_CalFactor = CalFactor;
            progInfo.Instrs.Fcu2AsicInstrsGroups.FCUCalibrationData.TxFinePot_CalOffset = CalOffset;
            // Fix ADC
            readCalData("FCU2Asic", fcuSerialNumber, "Fix ADC", out CalFactor, out CalOffset);
            progInfo.Instrs.Fcu2AsicInstrsGroups.FCUCalibrationData.FixADC_CalFactor = CalFactor;
            progInfo.Instrs.Fcu2AsicInstrsGroups.FCUCalibrationData.FixADC_CalOffset = CalOffset;

            // ThermistorResistance
            readCalData("FCU2Asic", fcuSerialNumber, "ThermistorResistance", out CalFactor, out CalOffset);
            progInfo.Instrs.Fcu2AsicInstrsGroups.FCUCalibrationData.ThermistorResistance_CalFactor = CalFactor;
            progInfo.Instrs.Fcu2AsicInstrsGroups.FCUCalibrationData.ThermistorResistance_CalOffset = CalOffset;
        }

        // added FCU2ASIC Option by Alice.Huang, for TOSA GB test     2010-02-01
        /// <summary>
        /// initialise FCU2ASIC INSTRUMENTS 
        /// </summary>
        /// <param name="fcu2AsicInstrs">FCU2ASIC instruments group </param>
        private void ConfigureFCU2AsicInstruments(FCU2AsicInstruments fcu2AsicInstrs)
        {

            string fcu2asic_InstrName = "FCU2Asic";
            //Configure instrument

            fcu2AsicInstrs.AsicVccSource.SetDefaultState();
            double curMax = progInfo.TestParamsConfig.GetDoubleParam("AsicVccSource_CurrentCompliance_A");
            Inst_Ke24xx keSource = fcu2AsicInstrs.AsicVccSource as Inst_Ke24xx;

            //fcu2AsicInstrs .AsicVccSource.U
            //fcu2AsicInstrs.AsicVccSource 
            // Set to voltage source mode & set current compliance
            fcu2AsicInstrs.AsicVccSource.VoltageSetPoint_Volt = progInfo.TestParamsConfig.GetDoubleParam("AsicVccSource_Voltage_V");
            fcu2AsicInstrs.AsicVccSource.CurrentComplianceSetPoint_Amp = curMax;
            fcu2AsicInstrs.AsicVccSource.OutputEnabled = true;
            if (keSource != null)
            {
                keSource.UseFrontTerminals = true;
                keSource.FourWireSense = false;
                keSource.SenseCurrent(curMax, curMax);
            }
            double i = fcu2AsicInstrs.AsicVccSource.CurrentActual_amp;

            fcu2AsicInstrs.AsicVeeSource.SetDefaultState();

            keSource = fcu2AsicInstrs.AsicVccSource as Inst_Ke24xx;
            curMax = progInfo.TestParamsConfig.GetDoubleParam("AsicVeeSource_CurrentCompliance_A");

            // Set to voltage source mode & set current compliance  
            fcu2AsicInstrs.AsicVeeSource.VoltageSetPoint_Volt =
                        progInfo.TestParamsConfig.GetDoubleParam("AsicVeeSource_Voltage_V");
            fcu2AsicInstrs.AsicVeeSource.CurrentComplianceSetPoint_Amp = curMax;
            fcu2AsicInstrs.AsicVeeSource.OutputEnabled = true;
            if (keSource != null)
            {
                keSource.UseFrontTerminals = true;
                keSource.FourWireSense = false;
                keSource.SenseCurrent(curMax, curMax);
            }
            i = fcu2AsicInstrs.AsicVeeSource.CurrentActual_amp;

            // if fcu asic instrs exists, setup fcu source output

            if (fcu2AsicInstrs.FcuSource != null)
            {
                /*fcu2AsicInstrs.FcuSource.SetDefaultState();
                // Set to voltage source mode & set current compliance
                fcu2AsicInstrs.FcuSource.VoltageSetPoint_Volt = 0.0;
                fcu2AsicInstrs.FcuSource.CurrentComplianceSetPoint_Amp =
                            progInfo.TestParamsConfig.GetDoubleParam("FcuSource_CurrentCompliance_A");
                fcu2AsicInstrs.FcuSource.VoltageSetPoint_Volt =
                            progInfo.TestParamsConfig.GetDoubleParam("FcuSource_Voltage_V");
                fcu2AsicInstrs.FcuSource.OutputEnabled = true;
                i = fcu2AsicInstrs.FcuSource.CurrentActual_amp;
                Thread.Sleep(2000);*/
                fcu2AsicInstrs.FcuSource.OutputEnabled = false;

                fcu2AsicInstrs.FcuSource.VoltageSetPoint_Volt = 5.0;
                fcu2AsicInstrs.FcuSource.CurrentComplianceSetPoint_Amp = 1.0500;
                fcu2AsicInstrs.FcuSource.OutputEnabled = true;
                Thread.Sleep(2000);
            }


            double CalFactor;
            double CalOffset;


            // Tx ADC
            readCalData(fcu2asic_InstrName, "all", "Tx ADC", out CalFactor, out CalOffset);
            progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.FcuDac2mACalibration.TxADC_CalFactor = CalFactor;
            progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.FcuDac2mACalibration.TxADC_CalOffset = CalOffset;
            // Rx ADC
            readCalData(fcu2asic_InstrName, "all", "Rx ADC", out CalFactor, out CalOffset);
            progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.FcuDac2mACalibration.RxADC_CalFactor = CalFactor;
            progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.FcuDac2mACalibration.RxADC_CalOffset = CalOffset;
            // TxCoarsePot
            readCalData(fcu2asic_InstrName, "all", "TxCoarsePot", out CalFactor, out CalOffset);
            progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.FcuDac2mACalibration.TxCoarsePot_CalFactor = CalFactor;
            progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.FcuDac2mACalibration.TxCoarsePot_CalOffset = CalOffset;
            // TxCoarsePot
            readCalData(fcu2asic_InstrName, "all", "RxCoarsePot", out CalFactor, out CalOffset);
            locker_pot_value = CalFactor;
            progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.FcuDac2mACalibration.RxCoarsePot_CalFactor = CalFactor;
            progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.FcuDac2mACalibration.RxCoarsePot_CalOffset = CalOffset;
            // TxFinePot
            readCalData(fcu2asic_InstrName, "all", "TxFinePot", out CalFactor, out CalOffset);
            progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.FcuDac2mACalibration.TxFinePot_CalFactor = CalFactor;
            progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.FcuDac2mACalibration.TxFinePot_CalOffset = CalOffset;
            // Fix ADC
            readCalData(fcu2asic_InstrName, "all", "Fix ADC", out CalFactor, out CalOffset);
            progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.FcuDac2mACalibration.FixADC_CalFactor = CalFactor;
            progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.FcuDac2mACalibration.FixADC_CalOffset = CalOffset;
            // ThermistorResistance
            readCalData(fcu2asic_InstrName, "all", "ThermistorResistance", out CalFactor, out CalOffset);
            progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.FcuDac2mACalibration.ThermistorResistance_CalFactor = CalFactor;
            progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.FcuDac2mACalibration.ThermistorResistance_CalOffset = CalOffset;

            // Asic Part
            progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.SetupChannelCalibration();


        }
        /// <summary>
        /// Configure a MZ bias source
        /// </summary>
        /// <param name="biasSrc">Instrument reference</param>
        /// <param name="biasSrcId">Config table data prefix</param>
        private void ConfigureMzBiasSource(IInstType_ElectricalSource biasSrc, string biasSrcId)
        {
            // Keithley 2400 specific commands (must be done before 'SetDefaultState')
            if (biasSrc.DriverName.Contains("Ke24xx"))
            {
                biasSrc.HardwareData["4wireSense"] = "true";
            }

            // Configure instrument
            biasSrc.SetDefaultState();
            // Set to current source mode & set compliance
            biasSrc.CurrentSetPoint_amp = 0.0;
            biasSrc.CurrentComplianceSetPoint_Amp = progInfo.TestParamsConfig.GetDoubleParam(biasSrcId + "_CurrentCompliance_A");
            // Set to voltage source mode & set compliance
            biasSrc.VoltageSetPoint_Volt = 0.0;
            biasSrc.VoltageComplianceSetPoint_Volt = progInfo.TestParamsConfig.GetDoubleParam(biasSrcId + "_VoltageCompliance_volt");
        }

        #endregion

        private MzData[] GetMzDataForEveryChannel()
        {
            if (!File.Exists(channelPassFile))
            {
                string strMsg = "Can't find channel pass file.\nplease do final test for Hit2 GB!";
                initTestEngine.ErrorInProgram(strMsg);
            }
            //read mzdata from channel pass file
            CsvReader cr=new CsvReader ();
            List<string[]> lines = cr.ReadFile(channelPassFile);

            int chanNbr=lines.Count-3;//except for title line ,lowlimit line ,highlimit line
            MzData[] mzDatas = new MzData[chanNbr];

            for (int i = 0; i < chanNbr; i++)
            {
                
                mzDatas[i].LeftArmMod_Peak_V = GetValueFromEnumParam(EnumTosaParam.MzLeftArmModPeak_V, i);

                mzDatas[i].RightArmMod_Peak_V = GetValueFromEnumParam(EnumTosaParam.MzRightArmModPeak_V, i);

                mzDatas[i].LeftArmMod_Quad_V = GetValueFromEnumParam(EnumTosaParam.MzLeftArmModQuad_V, i);

                mzDatas[i].RightArmMod_Quad_V = GetValueFromEnumParam(EnumTosaParam.MzRightArmModQuad_V, i);

                mzDatas[i].LeftArmMod_Min_V = GetValueFromEnumParam(EnumTosaParam.MzLeftArmModMinima_V, i);

                mzDatas[i].RightArmMod_Min_V = GetValueFromEnumParam(EnumTosaParam.MzRightArmModMinima_V, i);

                mzDatas[i].LeftArmImb_mA = GetValueFromEnumParam(EnumTosaParam.MzLeftArmImb_mA, i);

                mzDatas[i].RightArmImb_mA = GetValueFromEnumParam(EnumTosaParam.MzRightArmImb_mA, i);

                mzDatas[i].LeftArmImb_Dac = GetValueFromEnumParam(EnumTosaParam.MzLeftArmImb_Dac, i);
                
                mzDatas[i].RightArmImb_Dac = GetValueFromEnumParam(EnumTosaParam.MzRightArmImb_Dac, i);

            }
            return mzDatas;
        }
        private int FindColumnInStringArray(string columnName, string[] stringArray)
        {
            int returnIndex = -1;

            for (int i = 0; i < stringArray.Length; i++)
            {
                if (stringArray[i].ToUpper() == columnName.ToUpper())
                {
                    returnIndex=i;
                    break;
                }
            }
            if (returnIndex < 0) initTestEngine.ErrorInProgram("No "+columnName+" column exist in channel pass file!");
            return returnIndex;
        }
        private double GetValueFromEnumParam(EnumTosaParam colName,int ituChanIndex)
        {
            string columnName = colName.ToString();

            if (!File.Exists(channelPassFile))
            {
                string strMsg = "Can't find channel pass file.\nplease do final test for Hit2 GB!";
                initTestEngine.ErrorInProgram(strMsg);
            }
            //read mzdata from channel pass file
            CsvReader cr=new CsvReader ();
            List<string[]> lines = cr.ReadFile(channelPassFile);

            int colIndex = FindColumnInStringArray(columnName, lines[0]);
            double dVal = double.Parse(lines[ituChanIndex + 3][colIndex]);

            return dVal;

        }

        private IlmzChannels BuildMidTempTestResult()
        {
            // Build ILMZ channels
            MzData[] mzDataCollection = GetMzDataForEveryChannel();

            IlmzChannelInit[] channelInits = new IlmzChannelInit[mzDataCollection.Length];
            for (int ituChanIndex=0;ituChanIndex<mzDataCollection.Length;ituChanIndex++)
            {
                IlmzChannelInit oneChannelInit = channelInits[ituChanIndex];
                oneChannelInit.Dsdbr = closeGridCharData[ituChanIndex];
                oneChannelInit.Mz = mzDataCollection[ituChanIndex];
            }

            double lowTemperature = progInfo.TestConditions.LowTemp;
            double highTemperature = progInfo.TestConditions.HighTemp;
            Specification spec = progInfo.MainSpec;

            IlmzChannels ilmzItuChannels = new IlmzChannels(channelInits, spec, lowTemperature, highTemperature);

            double startFreq=progInfo.TestConditions.ItuLowFreq_GHz;
            double stopFreq=progInfo.TestConditions.ItuHighFreq_GHz;
            
            for(double ituFreq=startFreq;ituFreq<=stopFreq;ituFreq+=50)
            {
                int ituChanIndex = 0;
                ILMZChannel[] channelsAtItu = ilmzItuChannels.GetChannelsAtItu(ituFreq);
                ILMZChannel oneChannel = channelsAtItu[0];

                foreach (string nameAsStr in Enum.GetNames(typeof(EnumTosaParam)))
                {
                    EnumTosaParam parameter = (EnumTosaParam)Enum.Parse(typeof(EnumTosaParam), nameAsStr);
                    double midResult = GetValueFromEnumParam(parameter, ituChanIndex);
                    oneChannel.MidTempData.SetValueDouble(parameter, midResult);
                }
                ituChanIndex++;
            }
            return IlmzItuChannels;
        }

        #region Program Running

        public override void Run(ITestEngineRun engine, DUTObject dutObject, InstrumentCollection instrs, ChassisCollection chassis)
        {
            // Open the Inst_E36xxA instruments, then load the DUT;
            progInfo.Instrs.Fcu2AsicInstrsGroups.AsicVccSource.OutputEnabled = false;
            progInfo.Instrs.Fcu2AsicInstrsGroups.AsicVeeSource.OutputEnabled = false;

            // Show the message to ask operator insert DUT;
            {
                engine.ShowContinueUserQuery("Please click the continue button to test,���������,��� Continue ����");
            }

            progInfo.Instrs.Fcu2AsicInstrsGroups.AsicVccSource.OutputEnabled = true;
            progInfo.Instrs.Fcu2AsicInstrsGroups.AsicVeeSource.OutputEnabled = true;

            //load etalon wavemeter cal data from file
            //string FrequencyCalibration_file_Path = "L_band_FrequencyCalibration_file_Path";

            //string str = progInfo.TestConditions.FreqBand.ToString();
            //if (str == "C")
               // FrequencyCalibration_file_Path = "C_band_FrequencyCalibration_file_Path";
            //string freqCalFilename = progInfo.MapParamsConfig.GetStringParam(FrequencyCalibration_file_Path);
            //FreqCalByEtalon.LoadFreqlCalArray(freqCalFilename, double.Parse(progInfo.MainSpec.GetParamLimit("TC_CAL_OFFSET").LowLimit.ValueToString()));


            ModuleRun modRun;

            // Set retest count
            this.SetRetestCount(engine, dutObject);

            //this.numberOfSupermodes = _mapResult.ReadSint32("NUM_SM");

            // Initialise 
            // engine.RunModule("IlmzInitialise");

            // Set switches for the filter box
            if (!engine.IsSimulation && progInfo.DsdbrInstrsUsed == DsdbrDriveInstruments.PXIInstruments)
            {
                switch (progInfo.TestConditions.FreqBand)
                {
                    case FreqBand.C:
                        progInfo.Instrs.SwitchPathManager.SetToPosn("FILTERBOX_C");
                        break;
                    case FreqBand.L:
                        progInfo.Instrs.SwitchPathManager.SetToPosn("FILTERBOX_L");
                        break;
                    default:
                        engine.ErrorInProgram("Invalid frequency band: " + progInfo.TestConditions.FreqBand);
                        break;
                }
            }
            //Verify Jig Temperauture first
            VerifyJigTemperauture(engine);

            // Set temperatures
            //  pre set the case temp to save some time
            progInfo.Instrs.TecCase.SensorTemperatureSetPoint_C = progInfo.TestConditions.MidTemp;
            // TODO set MZ & DSDBR according to FF data
            progInfo.Instrs.TecCase.OutputEnabled = true;

            //progInstrs.Fcu2AsicInstrsGroups.Fcu2Asic.Inital_Etalon_WaveMter();//to avoid Var pot large change in overallmap FCUMKI pot leveling Jack.Zhang 2011-01-12
            //System.Threading.Thread.Sleep(10000);
            engine.RunModule("DsdbrTemperature");
            engine.RunModule("CaseTemp_Mid");




            // Power up
            RunIlmzPowerUp(engine);

            // Measure of cross-calibration of frequency between map stage and final stage
            RunCrossCalibrationMeasure(engine);

            // Power head calibration
            RunIlmzPowerHeadCal(engine);//Echo remed for debug,

           /* // MZ Characterise 
            modRun = engine.GetModuleRun("IlmzMzCharacterise");
            modRun.PreviousTestData.AddReference("ItuChannels", closeGridCharData);
            ModuleRunReturn modRunReturn = engine.RunModule("IlmzMzCharacterise");
            bool isTcmzMzCharacteriseError =
                modRunReturn.ModuleRunData.ModuleData.IsPresent("IsError") ? modRunReturn.ModuleRunData.ModuleData.ReadBool("IsError") : false;
            if (isTcmzMzCharacteriseError)
            {
                this.errorInformation +=
                    modRunReturn.ModuleRunData.ModuleData.ReadString("ErrorInformation");
                // Set device to safe case temperature
                engine.RunModule("CaseTemp_Safe");
                return;
            }

            // MZ ITU settings estimate
            Datum mzSweepData = modRunReturn.ModuleRunData.ModuleData.GetDatum("MzSweepData");
            modRun = engine.GetModuleRun("IlmzMzItuEstimate");
            modRun.ConfigData.AddDoubleArray("OptimizedVcmArray", modRunReturn.ModuleRunData.ModuleData.ReadDoubleArray("OptimizedVcmArray"));
            modRun.PreviousTestData.Add(mzSweepData);
            modRun.PreviousTestData.AddReference("ItuChannels", closeGridCharData);
            modRun.PreviousTestData.AddReference("ItuChannelsIn", this.closeGridCharData);
            modRun.PreviousTestData.AddString("MzSweepDataResultsFile", modRunReturn.ModuleRunData.ModuleData.ReadFileLinkFullPath("MzSweepDataResultsFile"));
            ModuleRunReturn ituEstRunReturn = engine.RunModule("IlmzMzItuEstimate", false);

            bool isExit = false;
            try
            {
                if (ituEstRunReturn.ModuleRunData.ModuleData.ReadSint32("MzAtNewVcmPass") == 0)
                {
                    isExit = true;
                }
            }
            catch (Exception e)
            {
                //if program goes here, then module didn't return any data
                isExit = true;
            }

            //if (ituEstRunReturn==null || ituEstRunReturn.ModuleRunData.ModuleData.ReadSint32("MzAtNewVcmPass") == 0)
            if (isExit)
            {
                this.errorInformation += "MZ status at new preferred Vcm failed, test stopped.";
                moduleRunError = true;
                // Set device to safe case temperature
                engine.RunModule("CaseTemp_Safe");
                return;
            }
            IlmzItuChannels = (IlmzChannels)ituEstRunReturn.ModuleRunData.ModuleData.ReadReference("TcmzItuChannels");

            //Channel Characterise
            int breakPowerLevelingCount = progInfo.TestParamsConfig.GetIntParam("BreakPowerLevelingCount");
            modRun = engine.GetModuleRun("IlmzChannelChar");
            modRun.ConfigData.AddReference("IlmzItuChannels", IlmzItuChannels);
            modRun.ConfigData.AddUint32("BreakPowerLevelingCount", breakPowerLevelingCount);
            ModuleRunReturn chanCharRunReturn = null;


            chanCharRunReturn = engine.RunModule("IlmzChannelChar");


            //SelectTestFlag = chanCharRunReturn.ModuleRunData.ModuleData.ReadBool("SelectTestFlag");
            //if ((chanCharRunReturn != null ) &&(chanCharRunReturn.ModuleRunData.ModuleData.ReadSint32("FailAbortFlag") == 1))
            //Echo update above lines to below line,
            isExit = false;
            try
            {
                if (chanCharRunReturn.ModuleRunData.ModuleData.ReadSint32("FailAbortFlag") == 1)
                {
                    isExit = true;
                }
            }
            catch (Exception e)
            {
                isExit = true;
            }
            //if ((chanCharRunReturn == null) || (chanCharRunReturn.ModuleRunData.ModuleData.ReadSint32("FailAbortFlag") == 1))
            if (isExit)
            {
                this.errorInformation = chanCharRunReturn.ModuleRunData.ModuleData.ReadString("FailAbortReason");
                //this.errorInformation = "Testing Fail in Channel Char Test";
                moduleRunError = true;
                // Set device to safe case temperature
                engine.RunModule("CaseTemp_Safe");
                return;
            }*/

            IlmzItuChannels = BuildMidTempTestResult();
            IlmzChannels.ExtremeChannelIndexes extremeChans = IlmzItuChannels.GetItuExtremeChannels();
            // Low case temperature tests
            if (progInfo.TestParamsConfig.GetBoolParam("LowTempTestEnabled"))
            {
                if (IlmzItuChannels.Passed.Length > 0)
                {
                    // Set Case temperature to low 
                    engine.RunModule("CaseTemp_Low");
                    // Low temperature test 
                    modRun = engine.GetModuleRun("IlmzLowTempTest");
                    modRun.ConfigData.AddReference("TcmzItuChannels", IlmzItuChannels);
                    modRun.ConfigData.AddReference("ExtremeChannels", extremeChans);
                    engine.RunModule("IlmzLowTempTest");
                }
            }

            // High case temperature tests
            if (IlmzItuChannels.Passed.Length > 0)
            {
                // Set Case temperature to high 
                engine.RunModule("CaseTemp_High");

                // High temperature test 
                modRun = engine.GetModuleRun("IlmzHighTempTest");
                modRun.ConfigData.AddReference("TcmzItuChannels", IlmzItuChannels);
                modRun.ConfigData.AddReference("ExtremeChannels", extremeChans);
                engine.RunModule("IlmzHighTempTest");
            }

            // Send the case temperature back to ambient whilst we clean up.
            progInfo.Instrs.TecCase.SensorTemperatureSetPoint_C = 25;

            // Trace tone test - make sure over-temp test were done!
            if (progInfo.TestParamsConfig.GetBoolParam("TraceToneTestEnabled"))
            {
                // One-off measurements on individual channels
                engine.RunModule("TcmzTraceToneTest");
            }

            // Consolodate results
            modRun = engine.GetModuleRun("IlmzGroupResults");
            modRun.ConfigData.AddReference("TcmzItuChannels", IlmzItuChannels);
            engine.RunModule("IlmzGroupResults");

            // Set device to safe case temperature                 
            engine.RunModule("CaseTemp_Safe");
        }

        /// <summary>
        /// Run IlmzPowerHeadCal module
        /// </summary>
        /// <param name="engine"></param>
        private void RunIlmzPowerHeadCal(ITestEngineRun engine)
        {
            ModuleRun modRun;
            ButtonId skipPowerCal = ButtonId.No;
            if (progInfo.TestParamsConfig.GetBoolParam("AllowSkipPowerCalibration"))
            {
                skipPowerCal = engine.ShowYesNoUserQuery("Do you want to skip power calibration?");
            }
            if (skipPowerCal == ButtonId.Yes)
            {
                GuiMsgs.TcmzPowerCalDataResponse resp;
                do
                {
                    engine.ShowContinueUserQuery("Select Power Calibration Data file");
                    engine.GuiShow();
                    engine.GuiToFront();
                    engine.SendToGui(new GuiMsgs.TcmzPowerCalDataRequest());
                    resp = (GuiMsgs.TcmzPowerCalDataResponse)engine.ReceiveFromGui().Payload;
                    if (!File.Exists(resp.Filename))
                    {
                        engine.ShowContinueUserQuery("Power Calibration Data file doesn't exist, please try again!");
                    }
                } while (!File.Exists(resp.Filename));

                OpticalPowerCal.Initialise(2);
                using (CsvReader cr = new CsvReader())
                {
                    List<string[]> contents = cr.ReadFile(resp.Filename);
                    for (int ii = 1; ii < contents.Count; ii++)
                    {
                        OpticalPowerCal.AddCalibratedPoint(OpticalPowerHead.MZHead, double.Parse(contents[ii][0]), double.Parse(contents[ii][1]) - double.Parse(contents[ii][2]));
                        if (progInfo.DsdbrInstrsUsed == DsdbrDriveInstruments.PXIInstruments)
                        {
                            OpticalPowerCal.AddCalibratedPoint(OpticalPowerHead.OpmCgDirect, double.Parse(contents[ii][0]), double.Parse(contents[ii][1]) - double.Parse(contents[ii][3]));
                            OpticalPowerCal.AddCalibratedPoint(OpticalPowerHead.OpmCgFiltered, double.Parse(contents[ii][0]), double.Parse(contents[ii][1]) - double.Parse(contents[ii][4]));
                        }
                    }
                }
            }
            else
            {
                modRun = engine.GetModuleRun("IlmzPowerHeadCal");
                modRun.ConfigData.AddReference("CloseGridCharData", this.closeGridCharData);
                modRun.ConfigData.AddSint32("NUM_SM", numberOfSupermodes);
                ModuleRunReturn moduleRunReturn = engine.RunModule("IlmzPowerHeadCal");
                labourTime += moduleRunReturn.ModuleRunData.ModuleData.GetDatumDouble("LabourTime").Value;
            }
        }

        /// <summary>
        /// Run IlmzPowerUp module
        /// </summary>
        /// <param name="engine"></param>
        private void RunIlmzPowerUp(ITestEngineRun engine)
        {
            ModuleRun modRun;
            modRun = engine.GetModuleRun("IlmzPowerUp");
            IlmzChannelInit chanInit = new IlmzChannelInit();

            // MZ data is from TcmzInitMzSweep
            chanInit.Mz.LeftArmImb_mA = 0;
            chanInit.Mz.RightArmImb_mA = 0;
            chanInit.Mz.LeftArmMod_Min_V = 0;// this._mapResult.ReadDouble("REF_MZ_VOFF_LEFT"); //Jack.Zhang need to Get from Map PACS
            chanInit.Mz.RightArmMod_Min_V = 0;// this._mapResult.ReadDouble("REF_MZ_VOFF_RIGHT");
            chanInit.Mz.LeftArmMod_Peak_V = 0;// this._mapResult.ReadDouble("REF_MZ_VON_LEFT");
            chanInit.Mz.RightArmMod_Peak_V = 0;// this._mapResult.ReadDouble("REF_MZ_VON_RIGHT");

            //if (this._mapResult.IsPresent("REF_MZ_VOFF_LEFT"))
            //{
            //    chanInit.Mz.LeftArmMod_Min_V = this._mapResult.ReadDouble("REF_MZ_VOFF_LEFT"); //Jack.Zhang need to Get from Map PACS
            //}
            //if (this._mapResult.IsPresent("REF_MZ_VOFF_RIGHT"))
            //{
            //    chanInit.Mz.RightArmMod_Min_V = this._mapResult.ReadDouble("REF_MZ_VOFF_RIGHT");
            //}
            //if (this._mapResult.IsPresent("REF_MZ_VON_LEFT"))
            //{
            //    chanInit.Mz.LeftArmMod_Peak_V = this._mapResult.ReadDouble("REF_MZ_VON_LEFT");
            //}
            //if (this._mapResult.IsPresent("REF_MZ_VON_RIGHT"))
            //{
            //    chanInit.Mz.RightArmMod_Peak_V = this._mapResult.ReadDouble("REF_MZ_VON_RIGHT");
            //}

            // DSDBR data
            double testFreq_GHz = 193700;

            //Add for "L" band by Tim at 2009-2-13.
            if (progInfo.TestConditions.FreqBand == FreqBand.L)
                testFreq_GHz = 189000;
            foreach (DsdbrChannelData chanData in this.closeGridCharData)
            {
                if (Math.Abs(chanData.ItuFreq_GHz - testFreq_GHz) <= 20)
                {
                    chanInit.Dsdbr = chanData;
                    break;
                }
            }

            // switch the fcu cal data to pot1 to measure lock current
            double CalFactor;
            double CalOffset;
            string fcu2asic_InstrName = "FCU2Asic";

            // Tx ADC
            readCalData(fcu2asic_InstrName, "all", "Tx ADC low", out CalFactor, out CalOffset);
            progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.FcuDac2mACalibration.TxADC_CalFactor = CalFactor;
            progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.FcuDac2mACalibration.TxADC_CalOffset = CalOffset;
            // Rx ADC
            readCalData(fcu2asic_InstrName, "all", "Rx ADC low", out CalFactor, out CalOffset);
            progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.FcuDac2mACalibration.RxADC_CalFactor = CalFactor;
            progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.FcuDac2mACalibration.RxADC_CalOffset = CalOffset;

            if (!modRun.ConfigData.IsPresent("IlmzSettings")) modRun.ConfigData.AddReference("IlmzSettings", chanInit);
            ModuleRunReturn tcmzPowerUpRtn = engine.RunModule("IlmzPowerUp");
            if (tcmzPowerUpRtn.ModuleRunData.ModuleData.ReadBool("ErrorExists") == true)
            {
                engine.RaiseNonParamFail(0, tcmzPowerUpRtn.ModuleRunData.ModuleData.ReadString("ErrorString"));
            }
        }

        /// <summary>
        /// Run IlmzCrossCalibration module
        /// </summary>
        /// <param name="engine"></param>
        private void RunCrossCalibrationMeasure(ITestEngineRun engine)
        {
            ModuleRun modRun;
            modRun = engine.GetModuleRun("IlmzCrossCalibration");
            if (this.closeGridCharData.Length < 3)
            {
                engine.RaiseNonParamFail(1, "Frequency channel less than 3 channels, pls check ITU operation point file!");
            }

            // Get DsdbrChannelData for frequency cross-calibration
            int[] chanIndices = new int[3];
            chanIndices[0] = 0; // low channel
            chanIndices[1] = (progInfo.TestConditions.NbrChannels - 1) / 2; // mid channel 45
            chanIndices[2] = progInfo.TestConditions.NbrChannels - 1; // high channel

            if (this.closeGridCharData[closeGridCharData.Length - 1].ItuChannelIndex < chanIndices[2])
            {
                chanIndices[2] = closeGridCharData[closeGridCharData.Length - 1].ItuChannelIndex;
                chanIndices[1] = (chanIndices[0] + chanIndices[2]) / 2;
            }


            DsdbrChannelData[] dsdbrChansData = new DsdbrChannelData[3];
            for (int ii = 0; ii < chanIndices.Length; ii++)
            {
                foreach (DsdbrChannelData chanData in this.closeGridCharData)
                {
                    if (chanData.ItuChannelIndex == chanIndices[ii])
                    {
                        dsdbrChansData[ii] = chanData;
                        break;
                    }
                }
            }

            // Add config data to test module
            modRun.ConfigData.AddReference("DsdbrChannels", dsdbrChansData);
            // Run test module
            engine.RunModule("IlmzCrossCalibration");
        }

        #endregion


        #region End of Program
        /// <summary>
        /// reset instrument after dut test
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        /// <param name="instrs"></param>
        /// <param name="chassis"></param>
        public override void PostRun(ITestEnginePostRun engine, DUTObject dutObject,
                    InstrumentCollection instrs, ChassisCollection chassis)
        {
            // This takes a few seconds
            string archiveFile = archiveCGfiles(dutObject);

            // Display temperature progress
            engine.GuiShow();
            engine.GuiToFront();

            // Cleanup & power off
            if (!engine.IsSimulation)
            {
                SetCaseTempSafe(engine, 25);

                if (progInfo.DsdbrInstrsUsed == DsdbrDriveInstruments.FCU2AsicInstrument)
                {
                    if (progInfo.Instrs.Fcu2AsicInstrsGroups.FcuSource != null)
                        progInfo.Instrs.Fcu2AsicInstrsGroups.FcuSource.OutputEnabled = false;
                    progInfo.Instrs.Fcu2AsicInstrsGroups.AsicVeeSource.OutputEnabled = false;
                    progInfo.Instrs.Fcu2AsicInstrsGroups.AsicVccSource.OutputEnabled = false;

                }

                else if (progInfo.DsdbrInstrsUsed == DsdbrDriveInstruments.FCUInstruments)
                {
                    // modify for replacing FCU 2400 steven.cui
                    // Shutdown FCU
                    // Call methods on the FCU to switch off the laser.
                    progInfo.Instrs.Fcu.SoaCurrentSource.OutputEnabled = false;

                    progInfo.Instrs.Fcu.FullbandControlUnit.SetIVSource((float)0.0, IVSourceIndexNumbers.Rear);
                    progInfo.Instrs.Fcu.FullbandControlUnit.SetIVSource((float)0.0, IVSourceIndexNumbers.Phase);
                    progInfo.Instrs.Fcu.FullbandControlUnit.SetIVSource((float)0.0, IVSourceIndexNumbers.SOA);
                    progInfo.Instrs.Fcu.FullbandControlUnit.SetIVSource((float)0.0, IVSourceIndexNumbers.Gain);
                    progInfo.Instrs.Fcu.FullbandControlUnit.SetControlRegLaserPSUOn(false);
                    progInfo.Instrs.Fcu.FullbandControlUnit.SetControlRegLaserTecOn(false);

                    progInfo.Instrs.Fcu.FullbandControlUnit.SetControlRegLaserPSUOn(false);
                    TxCommand cmd1 = new TxCommand(0, 0, 0); // TunableModules command
                    cmd1 = progInfo.Instrs.Fcu.FullbandControlUnit.ReadTxCommand();
                    TxCommand cmd2 = new TxCommand(cmd1.Data1, cmd1.Data2,
                        (byte)(cmd1.Data3 | (byte)0x01)); // set bit0 of byte "3"
                    progInfo.Instrs.Fcu.FullbandControlUnit.SetTxCommand(cmd2); // Bit 'LSENABLE' is set for disabling it.
                    progInfo.Instrs.Fcu.FullbandControlUnit.OIFLaserEnable(false);
                    //progInfo.Instrs.Fcu.FCU_Source.OutputEnabled = false;
                }

                progInfo.Instrs.TecCase.OutputEnabled = false;
                progInfo.Instrs.TecDsdbr.OutputEnabled = false;
                progInfo.Instrs.Fcu2AsicInstrsGroups.AsicVccSource.OutputEnabled = false;
                progInfo.Instrs.Fcu2AsicInstrsGroups.AsicVeeSource.OutputEnabled = false;

            }
        }


        /// <summary>
        /// Write Dut test result
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        /// <param name="dutOutcome"></param>
        /// <param name="results"></param>
        /// <param name="userList"></param>
        public override void DataWrite(ITestEngineDataWrite engine, DUTObject dutObject,
                        DUTOutcome dutOutcome, TestData results, UserList userList)
        {
            //DatumList tcTraceData = new DatumList();
            //generate keys for main spec

            StringDictionary finalKeys = new StringDictionary();
            finalKeys.Add("SCHEMA", "HIBERDB");
            finalKeys.Add("DEVICE_TYPE", dutObject.PartCode);
            finalKeys.Add("SERIAL_NO", dutObject.SerialNumber);
            finalKeys.Add("SPECIFICATION", progInfo.
                MainSpec.Name);
            finalKeys.Add("TEST_STAGE", "final");

            this.CopyDataFromMap(this._mapResult, engine);


            //Data_Ignore_Write(engine);

            #region Fill in blank TC and CH results
            // Add 'missing data' for TC_* and CH_* parameters
            foreach (ParamLimit paramLimit in progInfo.MainSpec)
            {
                if (paramLimit.ExternalName.StartsWith("TC_") || paramLimit.ExternalName.StartsWith("CH_"))
                {
                    if (!tcTraceData.IsPresent(paramLimit.ExternalName))
                    {
                        Datum dummyValue = null;
                        switch (paramLimit.ParamType)
                        {
                            case DatumType.Double:
                                dummyValue = new DatumDouble(paramLimit.ExternalName, Convert.ToDouble(paramLimit.HighLimit.ValueToString()));
                                break;
                            case DatumType.Sint32:
                                dummyValue = new DatumSint32(paramLimit.ExternalName, Convert.ToInt32(paramLimit.HighLimit.ValueToString()));
                                break;
                            case DatumType.StringType:
                                dummyValue = new DatumString(paramLimit.ExternalName, "MISSING");
                                break;
                            case DatumType.Uint32:
                                dummyValue = new DatumUint32(paramLimit.ExternalName, Convert.ToUInt32(paramLimit.HighLimit.ValueToString()));
                                break;
                        }
                        if (dummyValue != null)
                            tcTraceData.Add(dummyValue);
                    }
                }
            }
            #endregion

            //write coc information
            if (_cocResult != null)
            {
                if (_cocResult.IsPresent("CHIP_ID"))
                {
                    tcTraceData.AddString("CHIP_ID", _cocResult.ReadString("CHIP_ID"));
                }
                if (_cocResult.IsPresent("SERIAL_NO"))
                {
                    tcTraceData.AddString("COC_SERIAL_NO", _cocResult.ReadString("SERIAL_NO"));
                }
                if (_cocResult.IsPresent("WAFER_ID"))
                {
                    tcTraceData.AddString("WAFER_ID", _cocResult.ReadString("WAFER_ID"));
                }
            }
            else
            {
                tcTraceData.AddString("CHIP_ID", "  ");
                tcTraceData.AddString("COC_SERIAL_NO", "  ");
                tcTraceData.AddString("WAFER_ID", "   ");
            }
            tcTraceData.AddString("WAFER_SIZE", this._mapResult.ReadString("LASER_WAFER_SIZE"));
            tcTraceData.AddSint32("NODE", dutObject.NodeID);
            if (_mapResult.IsPresent("REF_LV_FILE"))
            {
                tcTraceData.AddFileLink("REF_LI_FILE", this._mapResult.ReadFileLinkFullPath("REF_LV_FILE"));
            }
            if (_mapResult.IsPresent("LASER_RTH"))
            {
                tcTraceData.AddDouble("RTH", this._mapResult.ReadDouble("LASER_RTH"));
            }
            
            // Add time date
            tcTraceData.AddString("TIME_DATE", DateTime.Now.ToString("yyyyMMddHHmmss"));
            // Add testTime
            testTime_End = DateTime.Now;
            TimeSpan testTime = testTime_End.Subtract(testTime_Start);
            tcTraceData.AddDouble("TEST_TIME", testTime.TotalMinutes);
            tcTraceData.AddDouble("LABOUR_TIME", labourTime);
            // Add more detail into the main specification
            tcTraceData.AddString("FACTORY_WORKS_FAILMODE", "");
            tcTraceData.AddString("FACTORY_WORKS_LOTID", dutObject.BatchID);
            tcTraceData.AddString("FACTORY_WORKS_PARTID", dutObject.PartCode);
            tcTraceData.AddString("SOFTWARE_ID", dutObject.ProgramPluginName + " SW_version: " + dutObject.ProgramPluginVersion + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString());// + "  " + engine.SpecificTestSolutionId.ToString());
            tcTraceData.AddString("OPERATOR_ID", userList.UserListString);
            tcTraceData.AddString("COMMENTS", engine.GetProgramRunComments());
            tcTraceData.AddString("PRODUCT_CODE", dutObject.PartCode);
            tcTraceData.AddString("FULL_PTR_TEST_FLAG", "0");
            tcTraceData.AddString("PTR_EOL_FREQ_DELTA", "16");
            tcTraceData.AddDouble("TEC_LASER_OL_TEMP", TEC_LASER_OL_TEMP);
            tcTraceData.AddDouble("TEC_JIG_CL_TEMP", TEC_JIG_CL_TEMP);
            tcTraceData.AddString("EQUIP_ID", dutObject.EquipmentID);
            

            #region Write Channel pass/fail files and TTA/TXFP cal files
            if (IlmzItuChannels == null)
            {
                if (engine.GetProgramRunStatus() != ProgramStatus.Success)
                {
                    if (moduleRunError)
                    {
                        tcTraceData.AddString("TEST_STATUS", "Fail");
                    }
                    else
                    {
                        tcTraceData.AddString("TEST_STATUS", engine.GetProgramRunStatus().ToString());
                    }
                }
                else
                {
                    string status = progInfo.MainSpec.Status.Status.ToString();
                    tcTraceData.AddString("TEST_STATUS", status);
                }
                tcTraceData.AddString("FAIL_ABORT_REASON", "Invalid ilmz Itu Channel.");

                engine.SetDataKeysPerSpec(progInfo.MainSpec.Name, finalKeys);
                try
                {
                    engine.SetTraceData(progInfo.MainSpec.Name, tcTraceData);
                }
                catch (Exception e)
                {
                    engine.ShowContinueUserQuery(e.Message + "\n Error occurs, please check pcas drop data!");
                }
            }
            else
            {
                // Write channel files



                string tsffTemplate_LowCost;
                if (ituFromLowCost) //itu file from low cost kit
                {
                    tsffTemplate_LowCost = @"Configuration\TOSA Final\FinalTest\" +
                        progInfo.TestParamsConfig.GetStringParam("TXFP_PARAM_FILE_lowCost");
                }
                else //itu file from hitt-01 test kit
                {
                    tsffTemplate_LowCost = @"Configuration\TOSA Final\FinalTest\" +
                    progInfo.TestParamsConfig.GetStringParam("TXFP_PARAM_FILE");
                }


                string tempFileDirectory = progInfo.TestParamsConfig.GetStringParam("DSDBRFileDirectory");
                if (MzFileDirectory.Substring(MzFileDirectory.Length - 2, 2).Contains("\\"))
                {
                    MzFileDirectory = MzFileDirectory.Substring(0, MzFileDirectory.Length - 1);
                }

                string passFile = Util_GenerateFileName.GenWithTimestamp(MzFileDirectory,
                                            "CHANNEL_PASS", dutObject.SerialNumber, "csv");

                string failFile = Util_GenerateFileName.GenWithTimestamp(MzFileDirectory,
                                            "CHANNEL_FAIL", dutObject.SerialNumber, "csv");
                //string ttaFile = tempFileDirectory + Path.DirectorySeparatorChar + 
                //    dutObject.SerialNumber.Replace(".", "_") +
                //    "_" + DateTime.Now.ToUniversalTime().ToString("yyyyMMddHHmmss") + "_ITU.csv";
                string tsffFile = MzFileDirectory + Path.DirectorySeparatorChar +
                    dutObject.SerialNumber.Replace(".", "_") +
                    "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + "_TITAN.csv";

                string tsff_LowCost_File = MzFileDirectory + Path.DirectorySeparatorChar +
                    dutObject.SerialNumber.Replace(".", "_") +
                    "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + "_TITAN_LowCost.csv";

                ChannelDataFiles.WriteIlmzmzData(IlmzItuChannels.Passed, passFile);
                ChannelDataFiles.WriteIlmzmzData(IlmzItuChannels.Failed, failFile);

                while (!File.Exists(tsffTemplate_LowCost))
                {
                    string prompt = string.Format("Missing file: \"{0}\", Can't write HITT_CAL_FILE.\n" +
                            "Do you want to retry? If press No, this program won't generate HITT_CAL_FILE.",
                            tsffTemplate_LowCost);
                    if (engine.ShowYesNoUserQuery(prompt) == ButtonId.Yes)
                    {
                        continue;
                    }
                    else
                    {
                        break;
                    }
                }

                if (File.Exists(tsffTemplate_LowCost))
                {
                    //ChannelDataFiles.WriteTXFPData(IlmzItuChannels.Passed, tsffTemplate, tsffFile);
                    ChannelDataFiles.LoadMidTempFailedChannelData(IlmzItuChannels.Failed);
                    ChannelDataFiles.WriteTXFPData(IlmzItuChannels.Passed, tsffTemplate_LowCost, tsff_LowCost_File, ituOperationPointfile, progInstrs.Fcu2AsicInstrsGroups.Fcu2Asic);
                }

                tcTraceData.AddFileLink("CHANNEL_PASS_FILE", passFile);
                tcTraceData.AddFileLink("CHANNEL_FAIL_FILE", failFile);
                //tcTraceData.AddFileLink("TTA_CAL_FILE", ttaFile);
                tcTraceData.AddFileLink("HITT_CAL_FILE", tsff_LowCost_File);

            #endregion

                if ((this.errorInformation != null) &&
                    (this.errorInformation.Length != 0))
                {
                    if (this.errorInformation.Length > 80)
                        this.errorInformation = this.errorInformation.Substring(0, 80);
                    tcTraceData.AddString("FAIL_ABORT_REASON", this.errorInformation);
                }
                else
                {
                    tcTraceData.AddString("FAIL_ABORT_REASON", "Success");
                }
                if (engine.GetProgramRunStatus() != ProgramStatus.Success)
                {
                    if (moduleRunError)
                    {
                        tcTraceData.AddString("TEST_STATUS", "Fail");
                    }
                    else
                    {
                        tcTraceData.AddString("TEST_STATUS", engine.GetProgramRunStatus().ToString());
                    }
                }
                else
                {
                    string status = progInfo.MainSpec.Status.Status.ToString();
                    tcTraceData.AddString("TEST_STATUS", status);
                }

                // Set trace data above here
                engine.SetDataKeysPerSpec(progInfo.MainSpec.Name, finalKeys);
                try
                {
                    engine.SetTraceData(progInfo.MainSpec.Name, tcTraceData);
                }
                catch (Exception e)
                {
                    engine.ShowContinueUserQuery(e.Message + " \nError occcurs, please check pcas drop data!");
                }


                #region light_tower

                if (progInfo.lightTowerConfig.GetBoolParam("LightExist"))
                {
                    try
                    {

                        _serialPort.Close();
                        if (!_serialPort.IsOpen)
                        {
                            _serialPort.Open();
                        }

                        // Add the light tower color.
                        _serialPort.WriteLine(progInfo.lightTowerConfig.GetStringParam("LightYellow"));
                        _serialPort.Close();
                    }
                    catch { }
                }
                #endregion light_tower


            }
        }

        /// <summary>
        /// fill in empty ingore parameters
        /// </summary>
        /// <param name="engine"></param>
        private void Data_Ignore_Write(ITestEngineDataWrite engine)
        {
            string[] ignoreParas = {
                
                "RTH",
                //"FIBRE_POWER_INITIAL"
            };
            //DatumList tcTraceData = new DatumList();
            foreach (string para in ignoreParas)
            {
                foreach (ParamLimit paramLimit in progInfo.MainSpec)
                {
                    if (paramLimit.ExternalName == para)
                    {
                        if (!tcTraceData.IsPresent(paramLimit.ExternalName))
                        {
                            Datum dummyValue = null;
                            switch (paramLimit.ParamType)
                            {
                                case DatumType.Double:
                                    dummyValue = new DatumDouble(paramLimit.ExternalName, Convert.ToDouble(paramLimit.HighLimit.ValueToString()));
                                    break;
                                case DatumType.Sint32:
                                    dummyValue = new DatumSint32(paramLimit.ExternalName, Convert.ToInt32(paramLimit.HighLimit.ValueToString()));
                                    break;
                                case DatumType.StringType:
                                    dummyValue = new DatumString(paramLimit.ExternalName, "No Value");
                                    break;
                                case DatumType.Uint32:
                                    dummyValue = new DatumUint32(paramLimit.ExternalName, Convert.ToUInt32(paramLimit.HighLimit.ValueToString()));
                                    break;
                                case DatumType.FileLinkType:
                                    dummyValue = new DatumFileLink(paramLimit.ExternalName, this.Nofilepath);
                                    break;
                            }
                            if (dummyValue != null)
                                //tcTraceData.Add(dummyValue);
                                this.tcTraceData.Add(dummyValue);
                        }//if
                    }//if (paramLimit.ExternalName == para)
                }//foreach (ParamLimit paramLimit in mainSpec)
            }// foreach (string para in ignoreParas)
            //engine.SetTraceData(this.progInfo.MainSpec.Name, tcTraceData);
        }


        /// <summary>
        /// Wait for the case temperature to reach a safe temperature
        /// </summary>
        /// <param name="engine">ITestEnginePostRun</param>
        /// <param name="safeTemperature_C"></param>
        private void SetCaseTempSafe(ITestEnginePostRun engine, double safeTemperature_C)
        {
            // Set case TEC to safe temperature
            progInfo.Instrs.TecCase.SensorTemperatureSetPoint_C = safeTemperature_C;
            double deltaTemp = double.MaxValue;
            do
            {
                double caseTemp = progInfo.Instrs.TecCase.SensorTemperatureActual_C;
                deltaTemp = Math.Abs(safeTemperature_C - caseTemp);
                engine.SendToGui("Case temperature = " + String.Format("{0:f}", caseTemp));
                System.Threading.Thread.Sleep(500);

            } while (deltaTemp > 10);
        }

        #endregion


        #region Private Helper Functions
        /// <summary>
        /// zip files 
        /// </summary>
        /// <param name="files">to be zipped files</param>
        /// <param name="dutObject">dut</param>
        /// <returns></returns>

        private string zipFilesToCalIPhaseAcq(string[] files, DUTObject dutObject)
        {
            string zipFileName = Util_GenerateFileName.GenWithTimestamp(MzFileDirectory,
                "IPhaseModeAcqFiles", dutObject.SerialNumber, "zip");

            if (files.Length > 0)
            {
                Util_ZipFile zipFile = new Util_ZipFile(zipFileName);
                zipFile.SetCompressionLevel(5); // 5 is reasonably fast with good compression
                using (zipFile)
                {
                    foreach (string fileToAdd in files)
                    {
                        zipFile.AddFileToZip(fileToAdd);
                    }
                }
                return zipFileName;

            }

            return null;
        }

        private string[] CollectFilesToCalIPhaseAcq(string SerialNbr)
        {

            string searchPattern = string.Format("ImiddleLower_LM*_DSDBR01_" +
                 SerialNbr + "_" + this.TestTimeStamp + "_SM?.csv");

            return Directory.GetFiles(MzFileDirectory, searchPattern);

        }


        /// <summary>
        /// Get the times of retest for the current device
        /// </summary>
        /// <param name="testEngine"></param>
        /// <param name="dutObject"></param>
        private void SetRetestCount(ITestEngineRun testEngine, DUTObject dutObject)
        {
            IDataRead dataRead = testEngine.GetDataReader("PCAS_SHENZHEN");
            int retestCount = 0;
            try
            {
                //get parameter "retest"
                StringDictionary finalKeys = new StringDictionary();
                finalKeys.Add("SCHEMA", "hiberdb");
                finalKeys.Add("DEVICE_TYPE", dutObject.PartCode);
                finalKeys.Add("TEST_STAGE", "final");
                finalKeys.Add("SERIAL_NO", dutObject.SerialNumber);
                DatumList finalData = dataRead.GetLatestResults(finalKeys, false);
                if (finalData == null)
                {
                    retestCount = 0;
                }
                else
                {
                    if (finalData.IsPresent("RETEST"))
                    {
                        retestCount = int.Parse(finalData.ReadString("RETEST"));
                    }
                    retestCount++;
                }

            }
            catch
            {
            }

            // Set retest count
            DatumList dl = new DatumList();
            dl.AddString("RETEST", retestCount.ToString());
            progInfo.MainSpec.SetTraceData(dl);
        }

        /// <summary>
        /// Retrieves datums from config file via the ConfigAccessor.
        /// </summary>
        /// <param name="modRun">The ModuleRun to add the items to.</param>
        /// <param name="datumsRequired">The datum names in this datumlist specify the names of the datums to be retrieved and added to the ModuleRun</param>
        private void ExtractAndAddDatums(ModuleRun modRun, DatumList datumsRequired)
        {
            foreach (Datum datum in datumsRequired)
            {
                switch (datum.Type)
                {
                    case DatumType.Double:
                        {
                            modRun.ConfigData.Add(new DatumDouble(datum.Name, this.progInfo.TestParamsConfig.GetDoubleParam(datum.Name)));
                        }
                        break;

                    case DatumType.StringType:
                        {
                            modRun.ConfigData.Add(new DatumString(datum.Name, this.progInfo.TestParamsConfig.GetStringParam(datum.Name)));
                        }
                        break;

                    case DatumType.Uint32:
                        {
                            modRun.ConfigData.Add(new DatumUint32(datum.Name, this.progInfo.TestParamsConfig.GetIntParam(datum.Name)));
                        }
                        break;

                    case DatumType.Sint32:
                        {
                            modRun.ConfigData.Add(new DatumSint32(datum.Name, this.progInfo.TestParamsConfig.GetIntParam(datum.Name)));
                        }
                        break;

                    case DatumType.BoolType:
                        {
                            modRun.ConfigData.Add(new DatumBool(datum.Name, this.progInfo.TestParamsConfig.GetBoolParam(datum.Name)));
                        }
                        break;

                    default:
                        {
                            throw new ArgumentException("Invalid DatumType '" + datum.Type + "' of '" + datum.Name + "'");
                        }
                }
            }
        }

        /// <summary>
        /// Read instruments calibration data
        /// </summary>
        /// <param name="InstrumentName"></param>
        /// <param name="InstrumentSerialNo"></param>
        /// <param name="ParameterName"></param>
        /// <param name="CalFactor"></param>
        /// <param name="CalOffset"></param>
        private void readCalData(string InstrumentName, string InstrumentSerialNo,
            string ParameterName, out double CalFactor, out double CalOffset)
        {
            StringDictionary keys = new StringDictionary();
            keys.Add("Instrument_name", InstrumentName);
            keys.Add("Parameter", ParameterName);
            if (InstrumentSerialNo != null)
            {
                keys.Add("Instrument_SerialNo", InstrumentSerialNo);
            }
            DatumList calParams = progInfo.InstrumentsCalData.GetData(keys, false);

            CalFactor = calParams.ReadDouble("CalFactor");
            CalOffset = calParams.ReadDouble("CalOffset");
        }



        /// <summary>
        /// Retrive Ilmz final stage test result of mid temperature test from pcas 
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        private void ValidateFinalStageData(ITestEngineInit engine, DUTObject dutObject)
        {
            if (progInfo.TestParamsConfig.GetBoolParam("IsUsePcasMappingData"))
            {
                IDataRead dataRead = engine.GetDataReader("PCAS_SHENZHEN");

                StringDictionary finalKeys = new StringDictionary();
                finalKeys.Add("SCHEMA", ILMZSchema);
                finalKeys.Add("SERIAL_NO", dutObject.SerialNumber);
                finalKeys.Add("TEST_STAGE", "final");

                try
                {
                    this.finalResult = dataRead.GetLatestResults(finalKeys, true);
                }
                catch (Exception e)
                {
                    engine.ErrorInProgram("Invalid final stage result for " + dutObject.SerialNumber + "\n" + e.Message);

                }
                bool finalPass = true;
                if (finalResult != null)
                {
                    finalPass = finalResult.ReadString("TEST_STATUS").ToLower().Contains("pass") ? true : false;
                }
                if (finalResult == null || !finalPass)
                {
                    engine.ErrorInProgram("Invalid final stage result for " + dutObject.SerialNumber + "\n" + "Test status is fail or test result is null.");
                }

                // Get data from ITU operating file
                string _itufile = this.finalResult.ReadFileLinkFullPath("CG_CHAR_ITU_FILE");
                channelPassFile = this.finalResult.ReadFileLinkFullPath("CHANNEL_PASS_FILE");
                int node = this.finalResult.ReadSint32("NODE");
                ituOperationPointfile = CheckItuFilePath(_itufile, node.ToString());


                if (ituOperationPointfile == null)
                {
                    string errDescription = "Can't find Itu operation point file." +
                            "\n please contact IS to confirm this!";
                    engine.ErrorInProgram(errDescription);
                }

                this.closeGridCharData = Bookham.Toolkit.CloseGrid.CsvDataLookup.GetItuOperatingPoints(ituOperationPointfile);

                List<string[]> lines = new List<string[]>();
                using (CsvReader csvReader = new CsvReader())
                {
                    lines = csvReader.ReadFile(ituOperationPointfile);
                }
                int ituColumnLength = lines[0].Length;

                //judge whether itu file is come from low cost kit or hitt-01 kit
                if (ituColumnLength > 23) ituFromLowCost = true;

            }
            else
            {
                string strMsg = "Can't retrieve final stage data from local Disk.";
                engine.ErrorInProgram(strMsg);
            }
        }
       
        private string CheckItuFilePath(string ituFile, string node)
        {
            if (!ituFile.ToLower().Contains("unknown")) return ituFile; // vlaid path, return directly
            string filename = ituFile.Substring(ituFile.LastIndexOf("\\") + 1);
            string strYear = filename.Substring(filename.LastIndexOf('_') + 1, 4);
            string strMonth = filename.Substring(filename.LastIndexOf('_') + 5, 2);
            string strYearMonth = filename.Substring(filename.LastIndexOf('_') + 1, 6);
            string strYearMonthDay = filename.Substring(filename.LastIndexOf('_') + 1, 8);
            string nodeNumber = "Node" + node;


            string serverName = "";
            switch (strYear)
            {
                case "2011":
                    serverName = "\\\\szn-mcad-01\\Archive2010.08";
                    break;
                case "2009":
                    serverName = "\\\\szn-mcad-01\\Archive2009";
                    break;

                case "2008":
                    serverName = "\\\\szn-mcad-01\\Archive2008";
                    break;
                case "2010":
                    if (int.Parse(strMonth) <= 5) serverName = "\\\\szn-sfl-04\\Archive2010";
                    else
                        if (int.Parse(strMonth) >= 6 && int.Parse(strMonth) <= 8) serverName = "\\\\szn-sfl-04\\Archive2010.06";
                        else serverName = "\\\\szn-mcad-01\\Archive2010.08";
                    break;
            }
            string itufilename = Path.Combine(serverName, strYear);
            itufilename = Path.Combine(itufilename, strYearMonth);
            itufilename = Path.Combine(itufilename, nodeNumber);
            itufilename = Path.Combine(itufilename, strYearMonthDay);
            itufilename = Path.Combine(itufilename, filename);
            if (File.Exists(itufilename))
                return itufilename;
            else
                return null;
        }

        /// <summary>
        /// Validate DSDBR drive instruments - PXI or FCU
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        protected override void ValidateDsdbrDriveInstrs(ITestEngineInit engine, DUTObject dutObject)
        {
            if (dutObject.TestJigID == "FCUTestJigID")
            {
                progInfo.DsdbrInstrsUsed = DsdbrDriveInstruments.FCUInstruments;
            }
            else if (dutObject.TestJigID == "PXITestJigID")
            {
                progInfo.DsdbrInstrsUsed = DsdbrDriveInstruments.PXIInstruments;
            }
            else if (dutObject.TestJigID == "FCU2AsicTestJigId")
            {
                progInfo.DsdbrInstrsUsed = DsdbrDriveInstruments.FCU2AsicInstrument;
            }
            else
            {
                engine.ErrorInProgram("Unknown DSDBR drive instruments used!");
            }
        }

        /// <summary>
        /// Copy data from tcmz_map stage.
        /// </summary>
        /// <param name="mapData"></param>
        /// <param name="engine"></param>
        private void CopyDataFromMap(DatumList mapData, ITestEngineDataWrite engine)
        {
            try
            {
                string[] mapParameter ={
                    //"WAFER_SIZE",
                    "SOA_POWER_LEVELING",
                    "MATRIX_PRATIO_SUMMARY_MAP",
                    //"MODAL_DISTORT_SM0",
                    //"MODAL_DISTORT_SM1",
                    //"MODAL_DISTORT_SM2",
                    //"MODAL_DISTORT_SM3",
                    //"MODAL_DISTORT_SM4",
                    //"MODAL_DISTORT_SM5",
                    //"MODAL_DISTORT_SM6",
                    //"MODAL_DISTORT_SM7",
                    //"MILD_MULTIMODE_SM0",
                    //"MILD_MULTIMODE_SM1",
                    //"MILD_MULTIMODE_SM2",
                    //"MILD_MULTIMODE_SM3",
                    //"MILD_MULTIMODE_SM4",
                    //"MILD_MULTIMODE_SM5",
                    //"MILD_MULTIMODE_SM6",
                    //"MILD_MULTIMODE_SM7",
                    "CHAR_ITU_OP_COUNT",
                    "CG_CHAR_ITU_FILE",
                    //"CG_ITUEST_POINTS_FILE",
                    
                    "CG_DUT_FILE_TIMESTAMP",
                    "CG_ID",
                    "CG_SM_LINES_FILE",
                    "CG_PRATIO_WL_FILE",
                    "CG_QAMETRICS_FILE",
                    ////"CG_PASSFAIL_FILE",
                    ////"IRX_DARK",
                    ////"ITX_DARK",
                    ////"LASER_WAFER_ID",
                    "MZ_MAX_V_BIAS",
                    ////"MZ_MOD_LEFT_DARK_I",
                    ////"MZ_MOD_RIGHT_DARK_I",
                    "NUM_SM",
                    "REF_FIBRE_POWER",
                    "REF_FREQ",
                    
                    "REF_MZ_IMB_LEFT",
                    "REF_MZ_IMB_RIGHT",
                    "REGISTRY_FILE",
                    //"TAP_COMP_DARK_I",
                    "FIBRE_POWER_INITIAL",
                    //"LASER_WAFER_SIZE",
                    
                    //"REF_MZ_LEFT",
                    //"REF_MZ_RIGHT",
                    //"REF_MZ_RATIO",
                    //"REF_MZ_SUM",
                    //"REF_MZ_VOFF_LEFT",
                    //"REF_MZ_VOFF_RIGHT",
                    //"REF_MZ_VON_LEFT",
                    //"REF_MZ_VON_RIGHT",
                   
                    "ITU_OP_EST_COUNT",
                
                     "I_ETALON_FS_A",
                     "I_ETALON_GAIN",
                     "I_ETALON_SOA",
                     "CG_PHASE_MODE_ACQ_FILE",
                     "NUM_MISSING_EST_CHAN",

                    //"REF_LI_FILE"//if map data from hitt_map test stage,
                };

                for (int i = 0; i < mapParameter.Length; i++)
                {
                    if (mapData.IsPresent(mapParameter[i]) && progInfo.MainSpec.ParamLimitExists(mapParameter[i]))
                    {
                        if (!mapData.GetDatum(mapParameter[i]).ValueToString().ToLower().Contains("unknown"))
                        {
                            tcTraceData.Add(mapData.GetDatum(mapParameter[i]));
                        }
                        else
                        {
                            string a = mapData.GetDatum(mapParameter[i]).ValueToString();
                        }
                    }
                }

            }
            catch (Exception e)
            {
                engine.ErrorInProgram("Copy map stage data error!" + e.Message);
            }
        }



        /// <summary>
        /// Run at the end of test to archive results for this device
        /// </summary>
        private string archiveCGfiles(DUTObject dutObject)
        {
            string zipFileName = Util_GenerateFileName.GenWithTimestamp(
                progInfo.TestParamsConfig.GetStringParam("ResultsArchiveDirectory"),
                "CloseGridFiles", dutObject.SerialNumber, "zip");

            if (!Directory.Exists(Path.GetDirectoryName(zipFileName)))
            {
                Directory.CreateDirectory(progInfo.TestParamsConfig.GetStringParam("ResultsArchiveDirectory"));
            }

            if (Directory.Exists(progInfo.TestParamsConfig.GetStringParam("DSDBRFileDirectory")))
            {
                string[] csvFilesToAdd = Directory.GetFiles(
                    progInfo.TestParamsConfig.GetStringParam("DSDBRFileDirectory"),
                    "*" + dutObject.SerialNumber + "*.csv", SearchOption.TopDirectoryOnly);
                string[] zipFilesToAdd = Directory.GetFiles(
                    progInfo.TestParamsConfig.GetStringParam("DSDBRFileDirectory"),
                    "*" + dutObject.SerialNumber + "*.zip", SearchOption.TopDirectoryOnly);
                string[] txtFilesToAdd = Directory.GetFiles(
                    progInfo.TestParamsConfig.GetStringParam("DSDBRFileDirectory"),
                    "*" + dutObject.SerialNumber + "*.txt", SearchOption.TopDirectoryOnly);

                if (csvFilesToAdd.Length > 0 || zipFilesToAdd.Length > 0 || txtFilesToAdd.Length > 0)
                {
                    Util_ZipFile zipFile = new Util_ZipFile(zipFileName);
                    //zipFile.SetCompressionLevel(5); // 5 is reasonably fast with good compression
                    using (zipFile)
                    {
                        if (csvFilesToAdd.Length > 0)
                        {
                            foreach (string fileToAdd in csvFilesToAdd)
                            {
                                zipFile.AddFileToZip(fileToAdd);
                            }
                        }

                        if (zipFilesToAdd.Length > 0)
                        {
                            foreach (string fileToAdd in zipFilesToAdd)
                            {
                                zipFile.AddFileToZip(fileToAdd);
                            }
                        }

                        if (txtFilesToAdd.Length > 0)
                        {
                            foreach (string fileToAdd in txtFilesToAdd)
                            {
                                zipFile.AddFileToZip(fileToAdd);
                            }
                        }
                    }
                    return zipFileName;
                }
            }
            return null;
        }

        /// <summary>
        /// Run at the start of test to delete any old CSV data
        /// </summary>
        protected override void cleanUpCgFiles()
        {
            if (!Directory.Exists(progInfo.TestParamsConfig.GetStringParam("DSDBRFileDirectory")))
                return;

            string[] filesToRemove = Directory.GetFiles(
                    progInfo.TestParamsConfig.GetStringParam("DSDBRFileDirectory"), "*.csv");
            string[] zipFilesToRemove = Directory.GetFiles(
                    progInfo.TestParamsConfig.GetStringParam("DSDBRFileDirectory"), "*.zip");
            string[] txtFilesToRemove = Directory.GetFiles(
                    progInfo.TestParamsConfig.GetStringParam("DSDBRFileDirectory"), "*.txt");

            foreach (string fileToRemove in filesToRemove)
            {
                try
                {
                    File.Delete(fileToRemove);
                }
                catch (Exception)
                {
                    // Ignore exceptions on individual files
                }
            }

            foreach (string fileToRemove in zipFilesToRemove)
            {
                try
                {
                    File.Delete(fileToRemove);
                }
                catch (Exception)
                {
                    // Ignore exceptions on individual files
                }
            }

            foreach (string fileToRemove in txtFilesToRemove)
            {
                try
                {
                    File.Delete(fileToRemove);
                }
                catch (Exception)
                {
                    // Ignore exceptions on individual files
                }
            }
        }

        #endregion

    }
}



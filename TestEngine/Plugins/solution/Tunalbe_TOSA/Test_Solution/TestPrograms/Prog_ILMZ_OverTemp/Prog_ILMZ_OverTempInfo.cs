using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.Framework.Limits;
using Bookham.TestLibrary.Utilities;
using Bookham.TestSolution.ILMZFinalData;
using Bookham.TestSolution.IlmzCommonUtils;
using Bookham.TestEngine.Config;
using Bookham.TestSolution.IlmzCommonData;

namespace Bookham.TestSolution.TestPrograms
{
    internal class Prog_ILMZ_OverTempInfo
    {
        internal const int MaxSupermodes = 8;

        /// <summary>
        /// Remember the specification name we are using in the program
        /// </summary>
        internal Specification MainSpec;
        //internal SpecList SupermodeSpecs;

        internal IlmzOverTempTestConds TestConditions;

        internal ProgIlmzFinalInstruments Instrs;

        internal TestParamConfigAccessor TestParamsConfig;

        internal TestParamConfigAccessor LocalTestParamsConfig;

        internal TestParamConfigAccessor OpticalSwitchConfig;

        internal TestParamConfigAccessor MapParamsConfig;

        internal TempTableConfigAccessor TempConfig;

        internal TestParamConfigAccessor lightTowerConfig;

        internal ConfigDataAccessor InstrumentsCalData;

        internal TestSelection TestSelect;

        internal FinalTestStage TestStage;

        internal DsdbrDriveInstruments DsdbrInstrsUsed;
    }
}

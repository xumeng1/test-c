﻿// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestPrograms
//
// Prog_Ilmz_OverTemp.cs
//
// Author: paul.annetts, Mark Fullalove 2006
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.ExternalData;
using Bookham.TestEngine.Framework.Limits;
using System.Collections.Specialized;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestLibrary.Utilities;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestSolution.ILMZFinalData;
using Bookham.TestSolution.IlmzCommonUtils;
using Bookham.TestSolution.IlmzCommonInstrs;
using Bookham.TestSolution.IlmzCommonData;
using Bookham.TestSolution.TestModules;
using Bookham.TestLibrary.Instruments;
using Bookham.ToolKit.Mz;
using Bookham.TestEngine.Config;
using System.Collections;
using Bookham.TestLibrary.BlackBoxes;
using Bookham.Program.Core;
using System.Threading;
using System.IO.Ports;
using Bookham.fcumapping.CommonData;
using Bookham.Solution.Config;
using Bookham.TestLibrary.Algorithms;
using Bookham.TestSolution.Instruments;
using Bookham.TestLibrary.Instruments;

namespace Bookham.TestSolution.TestPrograms
{
    /// <summary>
    /// ILMZ Negtive Chirp chip test program
    /// </summary>
    public class Prog_ILMZ_OverTemp : TestProgramCore
    {
        #region Private data
        /// <summary>
        /// Remember the specification name we are using in the program
        /// </summary>
        private Prog_ILMZ_OverTempInfo progInfo;
        
        private double labourTime = 0;
        private IlmzChannels IlmzItuChannels;

        IlmzChannels.ExtremeChannelIndexes extremeChansSetup; 

        private bool DO_GRandR = false;

        private string errorInformation;

        List<int> channelsIndexes_TestOT;
        
        private string closeGridCharFileName = "";        

        private const string externalKeyTuneOpticalPowerToleranceInmW
            = "TC_FIBRE_TARGET_POWER_TOL_MID";
        private const string externalKeyTuneOpticalPowerToleranceIndB
            = "Tune_OpticalPowerTolerance_dB";
        private const string externalKeyTuneISOAPowerSlope
            = "Tune_IsoaPowerSlope";

        private int numberOfSupermodes;

        private string eolPhaseScanDropFilePath;
        
        //private int[] MILD_MULTIMODE_SM = { 1, 1, 1, 1, 1, 1, 1 };
        private DatumList tcTraceData = new DatumList();
         

        //private string[] Cg_Dut_File_TimeStamp = new string[7];
        //private string[] Cg_Lm_Lines_Sm_File = new string[7];
        //private string[] Cg_PassFail_Sm_File = new string[7];
        //private string[] Cg_Qametrics_Sm_File = new string[7];
        //private double[] Gain_i_sm_map = new double[7];
        //private double[] Soa_i_sm_map = new double[7];
        //private double[] RearSoa_i_sm_map = new double[7];
        //private string[] Matrix_ph1_Fwd_Map_Sm = new string[7];
        //private string[] Matrix_ph1_Rev_Map_Sm = new string[7];
        //private string[] Matrix_ph2_Fwd_Map_Sm = new string[7];
        //private string[] Matrix_ph2_Rev_Map_Sm = new string[7];
        //private string[] Matrix_Pratio_Fwd_Map_Sm = new string[7];
        //private string[] Matrix_Pratio_Rev_Map_Sm = new string[7];
        //private double[] Modal_Distort_SM = { 0, 0, 0, 0, 0, 0, 0 };
        //private int[] Num_Lm = { 0, 0, 0, 0, 0, 0, 0 };
        //private int[] Screen_Result_Sm =  { 0, 0, 0, 0, 0, 0, 0 };
        //private int[] Sm_ID =  { 0, 0, 0, 0, 0, 0, 0 };
        //private string[] Test_status = { "fail", "fail", "fail", "fail", "fail", "fail", "fail" };
        //private string[] Time_Date = { "0", "0", "0", "0", "0", "0", "0" };
        //private int[] Node = new int[7];
        //private string[] Equip_Id = new string[7];
        //private bool ituFromLowCost = false;
        private bool moduleRunError = false;
        private bool isOxideWafer = false;



        #endregion

        #region OverTemp test used private data  

        string TestStage = "";
        private string Nofilepath = @"Configuration\TOSA Final\MapTest\NoDataFile.txt";
        private DatumList finalResult;
        private DsdbrChannelData[] closeGridCharData;
        private DsdbrChannelData[] ituChanData;
        private ProgIlmzFinalInstruments progInstrs = null;
        private string channelPassFile;
        private ITestEngineInit engine;
        private double locker_pot_value = 0;
        private List<string[]> passFileLines;

        #endregion

        #region for Light Tower.

        private SerialPort _serialPort;
        /// <summary>
        /// Serial port to control the lighter to indicate test messeage
        /// </summary>
        public SerialPort SerialPort
        {
            get { return _serialPort; }
        }

        /// <summary>
        /// Construct a serial port object from parts of the resource string.
        /// </summary>
        /// <param name="comPort">COMx port portion of resource string.</param>
        /// <param name="rate">bps rate portion of resource string.</param>
        /// <returns>Constructed SerialPort handler instance.</returns>
        private SerialPort buildSerPort(string comPort, string rate)
        {
            return new System.IO.Ports.SerialPort(
                "COM" + comPort,                                /* COMn */
                int.Parse(rate),                                /* bps */
                System.IO.Ports.Parity.None, 8, StopBits.One);  /* 8N1 */
        }
        #endregion
        
        /// <summary>
        /// Constructor
        /// </summary>
        public Prog_ILMZ_OverTemp()
        {
            this.progInfo = new Prog_ILMZ_OverTempInfo ();
        }

        /// <summary>
        ///  GUI for program 
        /// </summary>
        public override Type UserControl
        {
            get { return typeof(Prog_ILMZ_OverTempGui); }
        }

        #region Implement InitCode method

        /// <summary>
        /// Initialise test configuration
        /// </summary>
        /// <param name="dutObj">Device under test</param>
        /// <param name="engine">Test engine</param>
        protected override void InitConfig(ITestEngineInit engine, DUTObject dutObject)
        {
            base.InitConfig(engine, dutObject);

            this.engine = engine;

            progInfo.TestParamsConfig = new TestParamConfigAccessor(dutObject,
                @"Configuration\TOSA Final\FinalTest\CommonTestParams.xml", "", "TestParams");
            bool usepcas = progInfo.TestParamsConfig.GetBoolParam("UseLocalLimitFile");

            progInfo.InstrumentsCalData = new ConfigDataAccessor(@"Configuration\TOSA Final\InstrsCalibration_new.xml", "Calibration");

            progInfo.TempConfig = new TempTableConfigAccessor(dutObject, 1,
                @"Configuration\TOSA Final\TempTable.xml");

            progInfo.LocalTestParamsConfig = new TestParamConfigAccessor(dutObject,
                 @"Configuration\TOSA Final\LocalTestParams.xml", "", "TestParams");

            progInfo.OpticalSwitchConfig = new TestParamConfigAccessor(dutObject,
                 @"Configuration\TOSA Final\IlmzOpticalSwitch.xml", "", "IlmzOpticalSwitchParams");

            progInfo.TestSelect = new TestSelection(@"Configuration\TOSA Final\FinalTest\testSelect_" + dutObject.PartCode + ".xml");

            progInfo.MapParamsConfig = new TestParamConfigAccessor(dutObject, @"Configuration\TOSA Final\MapTest\MappingTestConfig.xml", "", "MappingTestParams");

            string Nofilepath_Temp = Path.Combine(this.CommonTestConfig.ResultsSubDirectory, Path.GetFileNameWithoutExtension(Nofilepath) + "_" + TestTimeStamp_Start + Path.GetExtension(Nofilepath));
            File.Copy(Nofilepath, Nofilepath_Temp);
            File.SetAttributes(Nofilepath_Temp, FileAttributes.Normal);
            Nofilepath = Nofilepath_Temp;//Jack.zhang fix Nofile load by stream bug.2011-09-06
        }

        /// <summary>
        /// Load specifications
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        protected override void LoadSpecs(ITestEngineInit engine, DUTObject dutObject)
        {
            engine.SendStatusMsg("Loading Specification");
            ILimitRead limitReader = null;
            StringDictionary mainSpecKeys = new StringDictionary();

            if (progInfo.TestParamsConfig.GetBoolParam("UseLocalLimitFile"))
            {
                // initialise limit file reader
                limitReader = engine.GetLimitReader("PCAS_FILE");
                // main specification
                string final_SpecNameFile = progInfo.TestParamsConfig.GetStringParam("OverTempLocalLimitFileName");
                string finalSpecFullFilename = progInfo.TestParamsConfig.GetStringParam("LocalLimitFileDirectory")
                    + @"\" + final_SpecNameFile;
                mainSpecKeys.Add("Filename", finalSpecFullFilename);


            }
            else
            {
                // Use default limit source
                limitReader = engine.GetLimitReader();

                mainSpecKeys.Add("SCHEMA", "HIBERDB");
                mainSpecKeys.Add("TEST_STAGE", "final_ot");//by tim
                mainSpecKeys.Add("DEVICE_TYPE", dutObject.PartCode);
            }

            // load main specification

            SpecList tempSpecList = limitReader.GetLimit(mainSpecKeys);
            progInfo.MainSpec = tempSpecList[0];
            this.MainSpec = progInfo.MainSpec;

            Specification dummySpec = new Specification("DummySpec", new StringDictionary(), 1);

            // declare spec list to Test Engine
            SpecList specList = new SpecList();
            specList.Add(progInfo.MainSpec);
            specList.Add(dummySpec);
            engine.SetSpecificationList(specList);

            progInfo.TestConditions = new IlmzOverTempTestConds(progInfo.MainSpec);
            
            base.LoadSpecs(engine, dutObject);

            #region no use
            // No judgment the code type at here by tim
            /*
            // Get our specification object (so we can initialise modules with appropriate limits)
            int FINAL_TC_NUM_CHAN_REQUIRED = progInfo.TestConditions.NbrChannels;
            double FINAL_TC_OPTICAL_FREQ_STEP = progInfo.TestConditions.ItuSpacing_GHz;
            double FINAL_TC_OPTICAL_FREQ_START = progInfo.TestConditions.ItuLowFreq_GHz;
            double FINAL_TC_OPTICAL_FREQ_END = progInfo.TestConditions.ItuHighFreq_GHz;

            int MAP_TC_NUM_CHAN_REQUIRED = this._mapResult.ReadSint32("TC_NUM_CHAN_REQUIRED");
            double MAP_TC_OPTICAL_FREQ_START = this._mapResult.ReadDouble("TC_OPTICAL_FREQ_START");
            double MAP_TC_OPTICAL_FREQ_STEP = this._mapResult.ReadDouble("TC_OPTICAL_FREQ_STEP");
            double MAP_TC_OPTICAL_FREQ_END = MAP_TC_OPTICAL_FREQ_START + (MAP_TC_NUM_CHAN_REQUIRED - 1) * MAP_TC_OPTICAL_FREQ_STEP;

            if (FINAL_TC_NUM_CHAN_REQUIRED > MAP_TC_NUM_CHAN_REQUIRED || FINAL_TC_OPTICAL_FREQ_START < MAP_TC_OPTICAL_FREQ_START || FINAL_TC_OPTICAL_FREQ_END > MAP_TC_OPTICAL_FREQ_END)
            {
                string errorDescription = "final & map stage are different Type; please retest MAP! ";
                engine.ShowContinueUserQuery(errorDescription);
                engine.ErrorInProgram(errorDescription);
            }
            */
            #endregion
        }

        /// <summary>
        /// Validate DSDBR drive instruments - PXI or FCU
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        protected override void ValidateDsdbrDriveInstrs(ITestEngineInit engine, DUTObject dutObject)
        {
            if (dutObject.TestJigID == "FCUTestJigID")
            {
                progInfo.DsdbrInstrsUsed = DsdbrDriveInstruments.FCUInstruments;
            }
            else if (dutObject.TestJigID == "PXITestJigID")
            {
                progInfo.DsdbrInstrsUsed = DsdbrDriveInstruments.PXIInstruments;
            }
            else if (dutObject.TestJigID == "FCU2AsicTestJigId")
            {
                progInfo.DsdbrInstrsUsed = DsdbrDriveInstruments.FCU2AsicInstrument;
            }
            else
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "Unknown DSDBR drive instruments used!", FailModeCategory_Enum.HW);
            }
        }

        /// <summary>
        /// Check Dut's test stage, if it is not in "final" stage, prompt Error in program to stop test
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        protected override void ValidateTestStage(ITestEngineInit engine, DUTObject dutObject)
        {
            if (!dutObject.TestStage.ToLower().Contains("final_ot"))
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, string.Format("Un-matched test stage {0}. Expected \"Final_OT\"", dutObject.TestStage), FailModeCategory_Enum.OP);
            }
            // Get test stage information - Must be "final_ot"
            progInfo.TestStage = FinalTestStage.Final_OT;

        }

        protected override void ValidateMapStageData(ITestEngineInit engine, DUTObject dutObject)
        {
            if (progInfo.TestParamsConfig.GetBoolParam("IsUsePcasMappingData"))
            {
                IDataRead dataRead = engine.GetDataReader("PCAS_SHENZHEN");

                StringDictionary mapKeys = new StringDictionary();
                mapKeys.Add("SCHEMA", ILMZSchema);
                mapKeys.Add("SERIAL_NO", dutObject.SerialNumber);
                mapKeys.Add("TEST_STAGE", "hitt_map");

                try
                {
                    this._mapResult = dataRead.GetLatestResults(mapKeys, true);
                }
                catch (Exception e)
                {
                    this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "Invalid map stage result for " + dutObject.SerialNumber + "\n" + e.Message, FailModeCategory_Enum.OP);
                }
                //check this map result is come from low cost test kits or not? Echo, 2011-11-02

                string equip_id = this._mapResult.ReadString("EQUIP_ID");

                //if (IsMapComeFromLowCostKits(equip_id))
                //{
                //    string errorMesg = "This map result is come from low cost kits,\nAnd Can't do final test on this kits,\n\nPlease test this device on other test kits! \n此器件需在其他非LOwCost測試台做Final測試！";
                //    engine.ErrorInProgram(errorMesg);
                //}

                bool mapPass = true;
                if (_mapResult != null
                    && _mapResult.Count > 0)
                {
                    mapPass = _mapResult.ReadString("TEST_STATUS").ToLower().Contains("pass") ? true : false;

                    if (!mapPass)
                    {
                        this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "Invalid map stage result for " + dutObject.SerialNumber + "\n" + "Test status is fail or test result is null.", FailModeCategory_Enum.OP);
                    }
                }
                else
                {
                    this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "Invalid map stage result for " + dutObject.SerialNumber + "\n" + "Test status is fail or test result is null.", FailModeCategory_Enum.OP);
                }

                try
                {
                    string MAP_TC_NUM_CHAN_REQUIRED = this._mapResult.ReadSint32("TC_NUM_CHAN_REQUIRED").ToString();
                    string MAP_TC_OPTICAL_FREQ_START = this._mapResult.ReadDouble("TC_OPTICAL_FREQ_START").ToString();
                }
                catch
                {
                    this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "Can't load this 2 parameters @final test results: 'TC_NUM_CHAN_REQUIRED' & 'TC_OPTICAL_FREQ_START'.", FailModeCategory_Enum.SW);
                }

                #region Comment out by chongjian.liang 2015.7.13
                // Get data from ITU operating file
                //string itufile_temp = this._mapResult.ReadFileLinkFullPath("CG_CHAR_ITU_FILE");
                //int node = this._mapResult.ReadSint32("NODE");
                //string itufile = CheckItuFilePath(itufile_temp, node.ToString());

                //if (!File.Exists(itufile))
                //{
                //    string errDescription = itufile + " does not exist.\n\nPlease contact the test engineer.";

                //    engine.ErrorInProgram(errDescription);
                //} 
                #endregion

                // Get data from ITU operating file - chongjian.liang 2015.7.13
                this.closeGridCharFileName = this._mapResult.ReadFileLinkFullPath("CG_CHAR_ITU_FILE");
                
                this.closeGridCharData = Bookham.Toolkit.CloseGrid.CsvDataLookup.GetItuOperatingPoints(closeGridCharFileName);

                //List<string[]> lines = new List<string[]>();
                //using (CsvReader csvReader = new CsvReader())
                //{
                //    lines = csvReader.ReadFile(closeGridCharFileName);
                //}
                //int ituColumnLength = lines[0].Length;

                ////judge whether itu file is come from low cost kit or hitt-01 kit
                //if (ituColumnLength > 23) ituFromLowCost = true;
            }
            else
            {
                ManualEntryMappingReults(engine);
            }

            if (_mapResult.IsPresent("TC_DSDBR_TEMP"))//temp for add DSDBR_temp in limit file jack.zhang 2012-04-19
                progInfo.TestConditions.DSDBR_TEMP_C = _mapResult.ReadDouble("TC_DSDBR_TEMP");
            else
            {
                progInfo.TestConditions.DSDBR_TEMP_C = progInfo.TestParamsConfig.GetDoubleParam("TecDsdbr_SensorTemperatureSetPoint_C");
                tcTraceData.AddDouble("TC_DSDBR_TEMP", progInfo.TestConditions.DSDBR_TEMP_C);
            }
        }

        /// <summary>
        /// Retrive Ilmz final stage test result of mid temperature test from pcas 
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        protected override void ValidateFinalStageData(ITestEngineInit engine, DUTObject dutObject)
        {
            if (progInfo.TestParamsConfig.GetBoolParam("IsUsePcasMappingData"))
            {
                IDataRead dataRead = engine.GetDataReader("PCAS_SHENZHEN");

                StringDictionary finalKeys = new StringDictionary();
                finalKeys.Add("SCHEMA", ILMZSchema);
                finalKeys.Add("SERIAL_NO", dutObject.SerialNumber);
                finalKeys.Add("DEVICE_TYPE", dutObject.PartCode); // Add by chongjian.liang 2013.6.3
                finalKeys.Add("TEST_STAGE", "final");

                try
                {
                    this.finalResult = dataRead.GetLatestResults(finalKeys, true);
                }
                catch (Exception e)
                {
                    this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "Invalid final stage result for " + dutObject.SerialNumber + "\n" + e.Message, FailModeCategory_Enum.OP);
                }

                // To process the final OT test, the final test must have a passed result for this device type - chongjan.liang 2013.6.3
                if (this.finalResult == null
                    || this.finalResult.Count == 0)
                {
                    this.failModeCheck.RaiseError_Prog_NonParamFail(engine, string.Format("Cannot proceed the final OT test when missing final stage data for {0} {1}", dutObject.PartCode, dutObject.SerialNumber), FailModeCategory_Enum.OP);
                }
                else if (!finalResult.ReadString("TEST_STATUS").ToLower().Contains("pass"))
                {
                    this.failModeCheck.RaiseError_Prog_NonParamFail(engine, string.Format("Cannot proceed the final OT test when final test result failed for {0} {1}", dutObject.PartCode, dutObject.SerialNumber), FailModeCategory_Enum.OP);
                }

                #region Comment out by chongjian.liang 2015.7.13
                //// Get data from Channel paas file

                //channelPassFile = this.finalResult.ReadFileLinkFullPath("CHANNEL_PASS_FILE");

                //int node = this.finalResult.ReadSint32("NODE");

                //channelPassFile = CheckItuFilePath(channelPassFile, node.ToString());

                //if (!File.Exists(channelPassFile))
                //{
                //    string errDescription = channelPassFile + " does not exist.\n\nPlease contact the test engineer.";

                //    engine.ErrorInProgram(errDescription);
                //} 
                #endregion

                // Get data from ITU operating file - chongjian.liang 2015.7.13
                this.channelPassFile = this.finalResult.ReadFileLinkFullPath("CHANNEL_PASS_FILE");

                this.ituChanData = Bookham.Toolkit.CloseGrid.CsvDataLookup.GetChannelPassPoints(channelPassFile, true); //by tim

                //List<string[]> lines = new List<string[]>();
                //using (CsvReader csvReader = new CsvReader())
                //{
                //    lines = csvReader.ReadFile(channelPassFile);
                //}
                //int ituColumnLength = lines[0].Length;

                ////judge whether itu file is come from low cost kit or hitt-01 kit
                //if (ituColumnLength > 23) ituFromLowCost = true;

            }
            else
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "Can't retrieve final stage data from local Disk.", FailModeCategory_Enum.SW);
            }
        }

      

        /// <summary>
        /// verify device internal high temperature...
        /// </summary>
        /// <param name="engine"></param>
        protected override void VerifyJigTemperauture(ITestEngineInit engine)
        {
            //if (VerifyJigTemperautureIsOk) return;
            engine.SendStatusMsg("verify device internal high temperature...");

            //get inst
            Inst_Ke2510 dsdbrTec = progInfo.Instrs.TecDsdbr as Inst_Ke2510;

            IInstType_TecController jigTec = progInfo.Instrs.TecCase as IInstType_TecController;

            double JigHighTempValue = progInfo.TestConditions.HighTemp;
            TempTablePoint JigHighTemp = progInfo.TempConfig.GetTemperatureCal(25, JigHighTempValue)[0];

            dsdbrTec.OutputEnabled = false;

            dsdbrTec.Set_Protection_Level_Temperature(Inst_Ke2510.LimitType.UPPER, JigHighTempValue + 5);

            //jigTec.Set_Protection_Level_Temperature(Inst_Ke2510.LimitType.UPPER, JigHighTempValue + 5);//Echo remed this line, nt8A can't support this function

            jigTec.SensorTemperatureSetPoint_C = JigHighTempValue + JigHighTemp.OffsetC;
            jigTec.OutputEnabled = true;

            int count = 0;
            double Tolerance = 3;
            const int retryvVerifyJigtemperatureTime = 3;
            int actualRetryTime = 0;
            do
            {
                Thread.Sleep(2000);
                double dsdbrTemp = dsdbrTec.SensorTemperatureActual_C;
                //double mztemp = mzTec.SensorTemperatureActual_C;
                //&& JigHighTempValue - Tolerance < mztemp && mztemp < JigHighTempValue + Tolerance
                if (JigHighTempValue - Tolerance < dsdbrTemp && dsdbrTemp < JigHighTempValue + Tolerance)
                    break;
                count++;
                if (count > 90)
                {
                    actualRetryTime++;
                    if (actualRetryTime == retryvVerifyJigtemperatureTime)      //add by Dong.Chen , red
                    {
                        jigTec.SensorTemperatureSetPoint_C = progInfo.TestConditions.MidTemp;
                        Thread.Sleep(2000);
                        jigTec.OutputEnabled = false;
                        
                        
                        this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "The Dsdbr and Mz temperature can't reach the CaseTemp:" + JigHighTempValue.ToString() + dsdbrTemp.ToString("0.00"), FailModeCategory_Enum.UN);
                    }
                    else
                    {

                        jigTec.SensorTemperatureSetPoint_C = progInfo.TestConditions.MidTemp;
                        jigTec.OutputEnabled = true;
                        Thread.Sleep(60000);

                        dsdbrTemp = dsdbrTec.SensorTemperatureActual_C;
                        if (Math.Abs(dsdbrTemp - progInfo.TestConditions.MidTemp) > Tolerance)
                        {
                            Thread.Sleep(30000);
                        }
                        jigTec.OutputEnabled = false;
                        engine.ShowContinueUserQuery("Please reload the device,Thanks(请检查石墨片以及器件的装载位置,请重放器件，谢谢！) ");

                        jigTec.SensorTemperatureSetPoint_C = JigHighTempValue + JigHighTemp.OffsetC;
                        jigTec.OutputEnabled = true;
                        count = 0;
                    }

                }
            } while (true);
            //restore settings
            TEC_LASER_OL_TEMP = dsdbrTec.SensorTemperatureActual_C;
            //TEC_MZ_OL_TEMP = mzTec.SensorTemperatureActual_C;
            TEC_JIG_CL_TEMP = jigTec.SensorTemperatureActual_C;
            jigTec.SensorTemperatureSetPoint_C = progInfo.TestConditions.MidTemp;
            Thread.Sleep(1000);
            jigTec.OutputEnabled = false;
            VerifyJigTemperautureIsOk = true;
            engine.SendStatusMsg("verify is ok!");

        }

        /// <summary>
        /// Initialise instruments
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="instrs"></param>   
        /// <param name="chassis"></param>
        protected override void InitInstrs(ITestEngineInit engine, DUTObject dutObject, InstrumentCollection instrs, ChassisCollection chassis)
        {
            if (engine.IsSimulation)
            {
                return;
            }

            //add for avoidig instrument logging error
            engine.SendStatusMsg("Initialise Instruments");

            #region light_tower

            /* Set up Serial Binary Comms */
            // _serialPort = buildSerPort(comPort, rate);
            string lightTower = @"Configuration\TOSA Final\LightTowerParams.xml";
            progInfo.lightTowerConfig = new TestParamConfigAccessor(dutObject,
                lightTower, "_", "LightTowerParams");
            if (progInfo.lightTowerConfig.GetBoolParam("LightExist"))
            {
                try
                {

                    _serialPort = this.buildSerPort(progInfo.lightTowerConfig.GetStringParam("ComNum"), progInfo.lightTowerConfig.GetStringParam("BaudRate"));
                    _serialPort.Close();
                    if (!_serialPort.IsOpen)
                    {
                        _serialPort.Open();
                    }

                    // Add the light tower color.
                    _serialPort.WriteLine(progInfo.lightTowerConfig.GetStringParam("LightGreen"));
                    _serialPort.Close();
                }
                catch { }

            }
            #endregion light_tower...

            progInstrs = new ProgIlmzFinalInstruments();
            progInfo.Instrs = progInstrs;

            foreach (Chassis var in chassis.Values) var.EnableLogging = false;
            foreach (Instrument var in instrs.Values) var.EnableLogging = false;

            // Initialise DSDBR drive instruments
            switch (progInfo.DsdbrInstrsUsed)
            {
                case DsdbrDriveInstruments.FCUInstruments:
                    // FCU
                    progInstrs.Fcu = new FCUInstruments();
                    progInstrs.Fcu.FullbandControlUnit = (Instr_FCU)instrs["FullbandControlUnit"];
                    progInstrs.Fcu.SoaCurrentSource =
                                        (InstType_ElectricalSource)instrs["SoaCurrentSource"];
                    // modify for replacing FCU 2400 steven.cui
                    //progInstrs.Fcu.FCU_Source = (InstType_ElectricalSource)instrs["FCU_Source"];
                    break;

                // Alice.Huang add FCU2Asic function    2010-02-01
                case DsdbrDriveInstruments.FCU2AsicInstrument:
                    engine.SendStatusMsg("Configuring FCU2Asic instruments");
                    progInstrs.Fcu2AsicInstrsGroups = new FCU2AsicInstruments();
                    if (instrs.Contains("FCUMKI_Source"))
                        progInstrs.Fcu2AsicInstrsGroups.FcuSource =
                                    (InstType_ElectricalSource)instrs["FCUMKI_Source"];
                    else
                        progInstrs.Fcu2AsicInstrsGroups.FcuSource = null;

                    progInstrs.Fcu2AsicInstrsGroups.AsicVccSource =
                                        (InstType_ElectricalSource)instrs["ASIC_VccSource"];
                    progInstrs.Fcu2AsicInstrsGroups.AsicVeeSource =
                                        (InstType_ElectricalSource)instrs["ASIC_VeeSource"];
                    progInstrs.Fcu2AsicInstrsGroups.Fcu2Asic = (Inst_Fcu2Asic)instrs["Fcu2Asic"];
                    //progInstrs.Fcu2AsicInstrsGroups.FcuSource = (Inst_Ke24xx)instrs["FcuSource"];
                    break;

            }


            progInstrs.Mz = new IlMzInstruments();
            progInstrs.Mz.PowerMeter = (IInstType_TriggeredOpticalPowerMeter)instrs["OpmMz"];
            // OpmRef
            progInstrs.OpmReference = (IInstType_OpticalPowerMeter)instrs["OpmRef"];

            progInstrs.Mz.PowerMeter.EnableInputTrigger(false);
            progInstrs.Mz.PowerMeter.SetDefaultState();
            progInstrs.OpmReference.SetDefaultState();

            TestProgramCore.InitInstrs_Wavemeter(this.failModeCheck, engine, dutObject, instrs, progInfo.LocalTestParamsConfig, progInfo.OpticalSwitchConfig);
            
            bool UseWaveMeterDoLowCostMonitor = progInfo.TestParamsConfig.GetBoolParam("UseWaveMeterDoCal");

            if (UseWaveMeterDoLowCostMonitor
                && Measurements.Wavemeter != null)
            {
                Measurements.Wavemeter.IsOnline = false; //use Etalon box cal freq what ever WM is avaliable. jack.zhang 2012-03-05
            }
            // OSA
            /*  progInstrs.Osa = (IInstType_OSA)instrs["Osa"];

              // WM
              progInstrs.Wavemeter = (IInstType_Wavemeter)instrs["Wavemeter"];*/
            //Echo remed this block

            // Switch path manager
            //Util_SwitchPathManager switchPathManager = new Util_SwitchPathManager
            //   (@"Configuration\TOSA Final\switches.xml", instrs);
            // progInstrs.SwitchPathManager = switchPathManager;

            // Switch between OSA and Opm
            // Switch_Osa_MzOpm switchOsaMzOpm = new Switch_Osa_MzOpm(switchPathManager);
            // progInstrs.SwitchOsaMzOpm = switchOsaMzOpm;

            // TECs
            progInstrs.TecDsdbr = (IInstType_TecController)instrs["TecDsdbr"];
            DsdbrUtils.opticalSwitch = (Inst_Ke2510)instrs["TecDsdbr"];
            DsdbrTuning.OpticalSwitch = (Inst_Ke2510)instrs["TecDsdbr"];

            progInstrs.TecCase = (IInstType_TecController)instrs["TecCase"];

            // Config instruments if not simulation mode
            if (!engine.IsSimulation)
            {
                engine.SendStatusMsg("Configuring Instruments");
                // Configure FCU
                if (progInfo.DsdbrInstrsUsed == DsdbrDriveInstruments.FCUInstruments)
                    ConfigureFCUInstruments(progInstrs.Fcu);

                    // Alice.Huang   2010-02-01
                // add FCU2ASIC instrument initialise for TOSA Test
                else if (progInfo.DsdbrInstrsUsed == DsdbrDriveInstruments.FCU2AsicInstrument)
                    ConfigureFCU2AsicInstruments(progInstrs.Fcu2AsicInstrsGroups);

                // Configure TEC controllers

                ConfigureTecController(engine, progInstrs.TecDsdbr, "TecDsdbr", false, progInfo.TestParamsConfig.GetDoubleParam("TecDsdbr_SensorTemperatureSetPoint_C"));
                ConfigureTecController(engine, progInstrs.TecCase, "TecCase", true, progInfo.TestParamsConfig.GetDoubleParam("TecCase_SensorTemperatureSetPoint_C"));


                progInstrs.Mz.FCU2Asic = (Inst_Fcu2Asic)instrs["Fcu2Asic"];
                progInstrs.Mz.FCU2Asic.SwapWirebond = ParamManager.Conditions.IsSwapWirebond;
                progInstrs.OpmReference.SetDefaultState();
                progInstrs.Mz.PowerMeter.SetDefaultState();
                DsdbrUtils.locker_port = locker_pot_value;
                DsdbrTuning.locker_pot = locker_pot_value;
            }
        }

        /// <summary>
        /// Initialise utilities
        /// </summary>
        /// <param name="engine"></param>
        protected override void InitUtils(ITestEngineInit engine)
        {
            if (engine.IsSimulation)
            {
                return;
            }

            engine.SendStatusMsg("Initialise utilities");
            // Initialise DsdbrUtils and Measurements utilities by testkit hardware info.
            switch (progInfo.DsdbrInstrsUsed)
            {
                // added FCU2ASIC Option by Alice.Huang, for TOSA GB test     2010-02-01
                case DsdbrDriveInstruments.FCU2AsicInstrument:
                    DsdbrUtils.Fcu2AsicInstrumentGroup = progInfo.Instrs.Fcu2AsicInstrsGroups;
                    Measurements.FCU2AsicInstrs = progInfo.Instrs.Fcu2AsicInstrsGroups;
                    SoaShutterMeasurement.Fcu2AsicInstrs = progInfo.Instrs.Fcu2AsicInstrsGroups;
                    break;
                case DsdbrDriveInstruments.FCUInstruments:
                    DsdbrUtils.FcuInstrumentGroup = progInfo.Instrs.Fcu;
                    Measurements.FCUInstrs = progInfo.Instrs.Fcu;
                    SoaShutterMeasurement.FCUInstrs = progInfo.Instrs.Fcu;
                    break;
            }
            //Measurements.OSA = progInfo.Instrs.Osa;

            //Measurements.Wavemeter = progInfo.Instrs.Wavemeter;
            Measurements.MzHead = progInfo.Instrs.Mz.PowerMeter;
            Measurements.ExternalHead = progInfo.Instrs.OpmReference;


            //Measurements.SectionIVSweepRawData = null;

            // Initialise TraceToneTest utility by device types
            if (progInfo.TestParamsConfig.GetBoolParam("TraceToneTestEnabled"))
            {
                TraceToneTest.Initialise(
                    progInfo.TestParamsConfig.GetIntParam("TraceToneTest_OrderOfFit"),
                    progInfo.TestConditions.TargetFibrePower_dBm,
                    progInfo.TestConditions.TraceToneTestAgeCond1_dB,
                    progInfo.TestConditions.TraceToneTestAgeCond2_dB,
                    progInfo.TestParamsConfig.GetDoubleParam("TraceToneTest_ModulationIndex"),
                    progInfo.TestParamsConfig.GetDoubleParam("TraceToneTest_SoaStart_mA"),
                    progInfo.TestParamsConfig.GetDoubleParam("TraceToneTest_SoaEnd_mA"),
                    progInfo.TestParamsConfig.GetDoubleParam("TraceToneTest_SoaStep_mA"),
                    progInfo.TestParamsConfig.GetIntParam("TraceToneTest_StepDelay_mS"));
            }
        }

        protected override void InitOpticalSwitchIoLine()
        {
            IlmzOpticalSwitchLines.C_Band_DigiIoLine = int.Parse(progInfo.OpticalSwitchConfig.GetStringParam("C_Band_Filter_IO_Line"));
            IlmzOpticalSwitchLines.C_Band_DigiIoLine_State = progInfo.OpticalSwitchConfig.GetBoolParam("C_Band_Filter_IO_Line_State");
            IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine = int.Parse(progInfo.OpticalSwitchConfig.GetStringParam("Dut_Ctap_IO_Line"));
            IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine_State = progInfo.OpticalSwitchConfig.GetBoolParam("Dut_Ctap_IO_Line_State");
            IlmzOpticalSwitchLines.Dut_Rx_DigiIoLine = int.Parse(progInfo.OpticalSwitchConfig.GetStringParam("Dut_Rx_IO_Line"));
            IlmzOpticalSwitchLines.Dut_Rx_DigiIoLine_State = progInfo.OpticalSwitchConfig.GetBoolParam("Dut_Rx_IO_Line_State");
            IlmzOpticalSwitchLines.Dut_Tx_DigiIoLine = int.Parse(progInfo.OpticalSwitchConfig.GetStringParam("Dut_Tx_IO_LIne"));
            IlmzOpticalSwitchLines.Dut_Tx_DigiIoLine_State = progInfo.OpticalSwitchConfig.GetBoolParam("Dut_Tx_IO_LIne_State");
            IlmzOpticalSwitchLines.Etalon_Ref_DigiIoLine = int.Parse(progInfo.OpticalSwitchConfig.GetStringParam("Etalon_Ref_IO_Line"));
            IlmzOpticalSwitchLines.Etalon_Ref_DigiIoLine_State = progInfo.OpticalSwitchConfig.GetBoolParam("Etalon_Ref_IO_Line_State");
            IlmzOpticalSwitchLines.Etalon_Rx_DigiIoLine = int.Parse(progInfo.OpticalSwitchConfig.GetStringParam("Etalon_Rx_IO_Line"));
            IlmzOpticalSwitchLines.Etalon_Rx_DigiIoLine_State = progInfo.OpticalSwitchConfig.GetBoolParam("Etalon_Rx_IO_Line_State");
            IlmzOpticalSwitchLines.Etalon_Tx_DigiIoLine = int.Parse(progInfo.OpticalSwitchConfig.GetStringParam("Etalon_Tx_IO_Line"));
            IlmzOpticalSwitchLines.Etalon_Tx_DigiIoLine_State = progInfo.OpticalSwitchConfig.GetBoolParam("Etalon_Tx_IO_Line_State");
            IlmzOpticalSwitchLines.L_Band_DigiIoLine = int.Parse(progInfo.OpticalSwitchConfig.GetStringParam("L_Band_Filter_IO_Line"));
            IlmzOpticalSwitchLines.L_Band_DigiIoLine_State = progInfo.OpticalSwitchConfig.GetBoolParam("L_Band_Filter_IO_Line_State");
        }

        /// <summary>
        /// Initialise test modules
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        protected override void InitModules(ITestEngineInit engine, DUTObject dutObject, InstrumentCollection instrs, ChassisCollection chassis)
        {
            engine.SendStatusMsg("Initialise modules");

            this.InitMod_Pin_Check(engine, instrs, chassis, dutObject);
            this.IlmzInitialise_InitModule(engine, dutObject);
            this.DeviceTempControl_InitModule(engine, "DsdbrTemperature", progInfo.Instrs.TecDsdbr, "TecDsdbr");
            this.CaseTempControl_InitModule(engine, "CaseTemp_Mid", 25, progInfo.TestConditions.MidTemp);
            this.IlmzPowerUp_InitModule(engine, instrs);
            this.IlmzCrossCalibration_InitModule(engine, instrs);
            this.IlmzPowerHeadCal_InitModule(engine, dutObject);

            this.IlmzChannelChar_InitModule(engine, dutObject);

            if (progInfo.TestParamsConfig.GetBoolParam("LowTempTestEnabled_OT"))
            {
                this.CaseTempControl_InitModule(engine, "CaseTemp_Low", progInfo.TestConditions.MidTemp, progInfo.TestConditions.LowTemp);
                this.CaseTempControl_InitModule(engine, "CaseTemp_High", progInfo.TestConditions.LowTemp, progInfo.TestConditions.HighTemp);
                this.IlmzLowTempTest_InitModule(engine, dutObject);
            }
            else
            { this.CaseTempControl_InitModule(engine, "CaseTemp_High", progInfo.TestConditions.MidTemp, progInfo.TestConditions.HighTemp); }

            if (progInfo.TestParamsConfig.GetBoolParam("HighTempTestEnabled_OT"))
            {

                this.IlmzHighTempTest_InitModule(engine, dutObject);
                if (progInfo.TestParamsConfig.GetBoolParam("TraceToneTestEnabled"))
                {
                    this.TcmzTraceToneTest_InitModule(engine, dutObject);
                }
            }

            this.IlmzGroupResults_InitModule(engine, dutObject);
            this.CaseTempControl_InitModule(engine, "CaseTemp_Safe", progInfo.TestConditions.HighTemp, 25);   // TODO - tidier method?
            //new added from mapping software


        }

        protected override void CheckPowerBeforeTesting(ITestEngineInit engine, DUTObject dutObject)
        {
            if (progInfo.TestParamsConfig.GetBoolParam("IsCheckPwrBeforeTest"))
            {
                string newDeviceType = progInfo.TestParamsConfig.GetStringParam("AutoChangeDeviceTypeTo");
                //init instruments
                //progInstrs.Mz.TapComplementary.VoltageSetPoint_Volt = -3.5;
                progInstrs.TecCase.SensorTemperatureSetPoint_C = progInfo.TestParamsConfig.GetDoubleParam("TecCase_SensorTemperatureSetPoint_C");//35
                progInstrs.TecCase.OutputEnabled = true;
                progInstrs.TecDsdbr.SensorTemperatureSetPoint_C = progInfo.TestParamsConfig.GetDoubleParam("TecDSDBR_SensorTemperatureSetPoint_C");//45
                progInstrs.TecDsdbr.OutputEnabled = true;
                System.Threading.Thread.Sleep(5000);
                engine.SendStatusMsg("please wait a minute, just adjust laser tec and jig tec......");
                //set dsdbr condition as last channel do li sweep, measure peak power
                DsdbrChannelData lastChannelData = this.closeGridCharData[this.closeGridCharData.Length - 1];
                lastChannelData.Setup.ISoa_mA = 100;
                DsdbrUtils.SetDsdbrCurrents_mA(lastChannelData.Setup);
                //prepare instruments for sweep
                IlMzDriverUtils mzDriverUtils = new IlMzDriverUtils(progInstrs.Mz);//without calibrate power head
                progInstrs.Mz.ImbsSourceType = IlMzInstruments.EnumSourceType.Asic;
                mzDriverUtils.opticalSwitch = (Inst_Ke2510)progInfo.Instrs.TecDsdbr;
                mzDriverUtils.locker_port = int.Parse(locker_pot_value.ToString());

                //double currentCompliance_A = progInfo.TestParamsConfig.GetDoubleParam("MZCurrentCompliance_A");
                //double currentRange_A = progInfo.TestParamsConfig.GetDoubleParam("MZCurrentRange_A");//0.012
                //mzDriverUtils.InitialiseMZArms(currentCompliance_A, currentRange_A);

                //double voltageCompliance_V = progInfo.TestParamsConfig.GetDoubleParam("MZVoltageCompliance_V");//8.0
                //double voltageRange_V = progInfo.TestParamsConfig.GetDoubleParam("MZVoltageRange_V");//8.0
                //mzDriverUtils.InitialiseImbalanceArms(voltageCompliance_V, voltageRange_V);
                //mzDriverUtils.InitialiseTaps(currentCompliance_A, currentRange_A);

                int numberOfAverages = progInfo.TestParamsConfig.GetIntParam("MzInitNumberOfAverages");//0
                //double integrationRate = progInfo.TestParamsConfig.GetDoubleParam("MZIntegrationRate");//0.01
                int MZSweepDelay_ms = progInfo.TestParamsConfig.GetIntParam("MZSourceMeasureDelay_ms");//5ms
                double powerMeterAveragingTime = progInfo.TestParamsConfig.GetDoubleParam("MZPowerMeterAveragingTime_s");//0.005

                engine.SendStatusMsg("begin do LI sweep....");

                //mzDriverUtils.SetMeasurementAccuracy(numberOfAverages, integrationRate, sourceMeasureDelay, powerMeterAveragingTime);
                progInstrs.Mz.PowerMeter.Mode = Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.MeterMode.Absolute_mW;

                // set power meter range to its initial range before any sweep 
                progInstrs.Mz.PowerMeter.Range = progInfo.TestParamsConfig.GetDoubleParam("OpmRangeInit_mW");//1

                mzDriverUtils.isCalibratePower = false;//no need calibrate power,only use uncalibrate power is ok


                ILMZSweepResult diffIsweep = mzDriverUtils.ImbArm_DifferentialSweep
                         (-2.5, -2.5,
                          0.0, 2 * 5 / 1000.0, 161, MZSweepDelay_ms, 0, -3.5, true);
                // analyse the data
                MzAnalysisWrapper.MzAnalysisResults mzAnlyDiffI =
                    MzAnalysisWrapper.DiffImbalance_NegChirp_FindFeaturePointByRef(
                    diffIsweep, 5 / 1000.0, 0,
                    Alg_MZAnalysis.MzFeaturePowerType.Max);

                string ChangePartcodeSweepFile = dutObject.SerialNumber + "_ChangePartCodeLiSweep_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".csv";
                RecordSweepData(diffIsweep, ChangePartcodeSweepFile);

                double CheckPowerLimit_dBm = progInfo.TestParamsConfig.GetDoubleParam("CheckPwrLimit_dBm");
                double pwrOffset = progInfo.TestParamsConfig.GetDoubleParam("CheckPwrOffset_dB");
                double fibre_pwr_dbm = mzAnlyDiffI.PowerAtMax_dBm + pwrOffset;
                engine.SendStatusMsg("initial peak power is :" + fibre_pwr_dbm.ToString() + " dBm....");
                if (fibre_pwr_dbm < CheckPowerLimit_dBm)
                {
                    engine.SendStatusMsg("Automaticaly change to new part code:" + newDeviceType + "......");

                    dutObject.PartCode = newDeviceType;
                    this.InitConfig(engine, dutObject);
                    //this.ValidateMapStageData(engine, dutObject);
                    this.InitUtils(engine);
                }
                progInstrs.Mz.FCU2Asic.CloseAllAsic();
            }
        }

        #endregion

        #region Initialize modules

        /// <summary>
        /// Initial Pin check for hittGB test(check the pins correlation with TCE and Vcc)
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="instrs"></param>
        /// <param name="chassis"></param>
        /// <param name="dutObject"></param>
        private void InitMod_Pin_Check(ITestEngineInit engine, InstrumentCollection instrs, ChassisCollection chassis, DUTObject dutObject)
        {
            double Temperature_LowLimit_Degree = 15.0;
            double Temperature_HighLimit_Degree = 40.0;

            ModuleRun modRun = engine.AddModuleRun("Mod_PinCheck", "Mod_PinCheck", string.Empty);

            modRun.Instrs.Add(instrs);
            modRun.Chassis.Add(chassis);
            modRun.ConfigData.AddString("TIME_STAMP", TestTimeStamp_Start);
            modRun.ConfigData.AddString("LASER_ID", dutObject.SerialNumber);
            modRun.ConfigData.AddDouble("Temperature_LowLimit_Degree", Temperature_LowLimit_Degree);
            modRun.ConfigData.AddDouble("Temperature_HighLimit_Degree", Temperature_HighLimit_Degree);
        }

        /// <summary>
        /// Configure Initialise Device module
        /// </summary>
        private void IlmzInitialise_InitModule(ITestEngineInit engine, DUTObject dutObject)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("IlmzInitialise", "IlmzInitialise", "");

            // Add instruments
            //modRun.Instrs.Add("Osa", (Instrument)progInfo.Instrs.Osa);
            modRun.ConfigData.AddReference("MzInstruments", progInfo.Instrs.Mz);
            switch (progInfo.DsdbrInstrsUsed)
            {
                // added FCU2ASIC Option by Alice.Huang, for TOSA GB test     2010-02-01
                case DsdbrDriveInstruments.FCU2AsicInstrument:
                    modRun.ConfigData.AddReference("FCU2AsicInstruments",
                                            progInfo.Instrs.Fcu2AsicInstrsGroups);
                    break;
                case DsdbrDriveInstruments.FCUInstruments:
                    modRun.ConfigData.AddReference("FcuInstruments", progInfo.Instrs.Fcu);
                    break;
            }

            // Add config data
            TcmzInitialise_Config tcmzInitConfig = new TcmzInitialise_Config();
            DatumList datumsRequired = tcmzInitConfig.GetDatumList();
            ExtractAndAddDatums(modRun, datumsRequired);
            modRun.ConfigData.AddSint32("NumChans", progInfo.TestConditions.NbrChannels);
            modRun.ConfigData.AddDouble("FreqLow_GHz", progInfo.TestConditions.ItuLowFreq_GHz);
            modRun.ConfigData.AddDouble("FreqHigh_GHz", progInfo.TestConditions.ItuHighFreq_GHz);
            modRun.ConfigData.AddDouble("FreqSpace_GHz", progInfo.TestConditions.ItuSpacing_GHz);
            modRun.ConfigData.AddString("DutSerialNbr", dutObject.SerialNumber);
            modRun.ConfigData.AddString("MzFileDirectory", this.CommonTestConfig.ResultsSubDirectory);
            modRun.ConfigData.AddEnum("DsdbrInstrsGroup", progInfo.DsdbrInstrsUsed);

        }

        /// <summary>
        /// Configure a Device Temperature Control module
        /// </summary>
        private void DeviceTempControl_InitModule(ITestEngineInit engine, string moduleName,
            IInstType_SimpleTempControl tecController, string configPrefix)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun(moduleName, "SimpleTempControl", "");

            // Add instrument
            modRun.Instrs.Add("Controller", (Instrument)tecController);

            //load config
            double timeout_S = this.progInfo.TestParamsConfig.GetDoubleParam(configPrefix + "_Timeout_S");
            double stabTime_S = this.progInfo.TestParamsConfig.GetDoubleParam(configPrefix + "_StabiliseTime_S");
            int updateTime_mS = this.progInfo.TestParamsConfig.GetIntParam(string.Format("{0}_UpdateTime_mS", configPrefix));
            double sensorSetPoint_C = this.progInfo.TestConditions.DSDBR_TEMP_C; //this.progInfo.TestParamsConfig.GetDoubleParam(string.Format("{0}_SensorTemperatureSetPoint_C", configPrefix));
            double tolerance_C = this.progInfo.TestParamsConfig.GetDoubleParam(string.Format("{0}_Tolerance_C", configPrefix));

            // Add config data
            DatumList tempConfig = new DatumList();
            tempConfig.AddDouble("datumMaxExpectedTimeForOperation_s", timeout_S);
            tempConfig.AddDouble("RqdStabilisationTime_s", stabTime_S);
            tempConfig.AddSint32("TempTimeBtwReadings_ms", updateTime_mS);
            tempConfig.AddDouble("SetPointTemperature_C", sensorSetPoint_C);
            tempConfig.AddDouble("TemperatureTolerance_C", tolerance_C);

            modRun.ConfigData.AddListItems(tempConfig);
        }

        /// <summary>
        /// Configure Case Temperature module
        /// </summary>
        private void CaseTempControl_InitModule(ITestEngineInit engine,
            string moduleName, Double currentTemp, Double tempSetPoint)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun(moduleName, "SimpleTempControl", "");

            // Add instrument
            modRun.Instrs.Add("Controller", (Instrument)progInfo.Instrs.TecCase);

            // Add config data
            TempTablePoint tempCal = progInfo.TempConfig.GetTemperatureCal(currentTemp, tempSetPoint)[0];
            DatumList tempConfig = new DatumList();
            int guiTimeOut_ms = this.progInfo.TestParamsConfig.GetIntParam("CaseTempGui_UpdateTime_mS");
            // Timeout = 2 * calibrated time, minimum of 60secs
            double timeout_s = Math.Max(tempCal.DelayTime_s * 15, 60);
            tempConfig.AddDouble("datumMaxExpectedTimeForOperation_s", timeout_s);
            // Stabilisation time = 0.5 * calibrated time
            tempConfig.AddDouble("RqdStabilisationTime_s", tempCal.DelayTime_s);
            tempConfig.AddSint32("TempTimeBtwReadings_ms", guiTimeOut_ms);
            // Actual temperature to set
            tempConfig.AddDouble("SetPointTemperature_C", tempSetPoint + tempCal.OffsetC);
            tempConfig.AddDouble("TemperatureTolerance_C", tempCal.Tolerance_degC);
            modRun.ConfigData.AddListItems(tempConfig);
        }

        /// <summary>
        /// Initialise IlmzPowerUp module
        /// </summary>
        /// <param name="engine"></param>
        private void IlmzPowerUp_InitModule(ITestEngineInit engine, InstrumentCollection instrs)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("IlmzPowerUp", "IlmzPowerUp", "");

            // Add instruments
            modRun.Instrs.Add("OpticalPowerMeter", (Instrument)progInfo.Instrs.Mz.PowerMeter);
            modRun.Instrs.Add(instrs);
            modRun.ConfigData.AddReference("MzInstruments", progInfo.Instrs.Mz);

            // Add config data
            modRun.ConfigData.AddDouble("MzTapBias_V", progInfo.TestParamsConfig.GetDoubleParam("MzTapBias_V"));
            modRun.ConfigData.AddDouble("PowerupMinipower_mW", progInfo.TestParamsConfig.GetDoubleParam("PowerupMinipower_mW"));
        }

        /// <summary>
        /// Initialise IlmzCrossCalibration module
        /// </summary>
        /// <param name="engine"></param>
        private void IlmzCrossCalibration_InitModule(ITestEngineInit engine, InstrumentCollection instrs)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("IlmzCrossCalibration", "TcmzCrossCalibration", "");
            modRun.ConfigData.AddDouble("FreqDivTolerance_GHz", Convert.ToDouble(
                progInfo.MainSpec.GetParamLimit("XCAL_CH_MID_MAP_GB").HighLimit.ValueToString()));
            // Add instruments

            // Add config data
            modRun.ConfigData.AddSint32("OpticalCal_delay_ms", 2000);

            modRun.Instrs.Add("Optical_switch", (Instrument)progInfo.Instrs.TecDsdbr);
            modRun.Instrs.Add(instrs);

            // Tie up to limits
            modRun.Limits.AddParameter(progInfo.MainSpec, "XCAL_CH_LO_MAP_GB", "XCAL_CH_LO");
            modRun.Limits.AddParameter(progInfo.MainSpec, "XCAL_CH_MID_MAP_GB", "XCAL_CH_MID");
            modRun.Limits.AddParameter(progInfo.MainSpec, "XCAL_CH_HI_MAP_GB", "XCAL_CH_HI");
        }

        /// <summary>
        /// Initialise IlmzPowerHeadCal module
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutobject"></param>
        private void IlmzPowerHeadCal_InitModule(ITestEngineInit engine, DUTObject dutobject)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("IlmzPowerHeadCal", "TcmzPowerHeadCal", "");

            // Add instruments
            modRun.Instrs.Add("OpmReference", (Instrument)progInfo.Instrs.OpmReference);
            modRun.Instrs.Add("Wavemeter", (Instrument)progInfo.Instrs.Wavemeter);
            modRun.Instrs.Add("OpmMZ", (Instrument)progInfo.Instrs.Mz.PowerMeter);
            switch (progInfo.DsdbrInstrsUsed)
            {
                // added FCU2ASIC Option by Alice.Huang, for TOSA GB test     2010-02-01
                case DsdbrDriveInstruments.FCU2AsicInstrument:
                    modRun.Instrs.Add("Fcu2Asic", (Instrument)progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic);
                    break;
                case DsdbrDriveInstruments.FCUInstruments:
                    break;
                case DsdbrDriveInstruments.PXIInstruments:
                    modRun.Instrs.Add("OpmCgDirect", (Instrument)progInfo.Instrs.OpmCgDirect);
                    modRun.Instrs.Add("OpmCgFilter", (Instrument)progInfo.Instrs.OpmCgFilter);
                    break;
            }

            // Add any config data here ...
            modRun.ConfigData.AddEnum("TestStage", progInfo.TestStage);
            modRun.ConfigData.AddSint32("OpticalCal_delay_ms", 2000);
            modRun.ConfigData.AddDouble("OpticalCal_MZ_Max", progInfo.LocalTestParamsConfig.GetDoubleParam("OpticalCal_MZ_Max"));
            modRun.ConfigData.AddDouble("OpticalCal_MZ_Min", progInfo.LocalTestParamsConfig.GetDoubleParam("OpticalCal_MZ_Min"));
            modRun.ConfigData.AddString("PlotFileDirectory", this.CommonTestConfig.ResultsSubDirectory);
            modRun.ConfigData.AddString("SN", dutobject.SerialNumber);
            modRun.ConfigData.AddDouble("Ref_Mz_IMB_Left_mA", this._mapResult.ReadDouble("REF_MZ_IMB_LEFT"));
            modRun.ConfigData.AddDouble("Ref_Mz_IMB_Right_mA", this._mapResult.ReadDouble("REF_MZ_IMB_RIGHT"));
            modRun.ConfigData.AddString("Frequency_band", progInfo.TestConditions.FreqBand.ToString());
            switch (progInfo.DsdbrInstrsUsed)
            {
                // added FCU2ASIC Option by Alice.Huang, for TOSA GB test     2010-02-01
                case DsdbrDriveInstruments.FCU2AsicInstrument:

                case DsdbrDriveInstruments.FCUInstruments:
                    modRun.ConfigData.AddBool("IsCalPXIT", false);
                    break;
                case DsdbrDriveInstruments.PXIInstruments:
                    modRun.ConfigData.AddBool("IsCalPXIT", true);
                    modRun.ConfigData.AddDouble("OpticalCal_CgDirect_Max", progInfo.LocalTestParamsConfig.GetDoubleParam("OpticalCal_CgDirect_Max"));
                    modRun.ConfigData.AddDouble("OpticalCal_CgDirect_Min", progInfo.LocalTestParamsConfig.GetDoubleParam("OpticalCal_CgDirect_Min"));
                    break;
            }
        }



        /// <summary>
        /// Initialise IlmzMzCharacterise module
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        private void IlmzMzCharacterise_InitModule(ITestEngineInit engine, DUTObject dutObject, InstrumentCollection instrs)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("IlmzMzCharacterise", "IlmzMzCharacterise_ND", "");

            // Add instruments
            modRun.ConfigData.AddReference("MzInstruments", progInfo.Instrs.Mz);

            modRun.Instrs.Add("FCU", (Instrument)(Inst_Fcu2Asic)instrs["Fcu2Asic"]);
            modRun.Instrs.Add("Optical_switch", (Instrument)progInfo.Instrs.TecDsdbr);

            TcmzMzSweep_Config tcmzConfig = new TcmzMzSweep_Config();
            DatumList datumsRequired = tcmzConfig.GetDatumList();
            ExtractAndAddDatums(modRun, datumsRequired);

            double mzBias1 = progInfo.TestParamsConfig.GetDoubleParam("MzFixedModBias_1_V");
            double mzBias2 = progInfo.TestParamsConfig.GetDoubleParam("MzFixedModBias_2_V");

            //START MODIFICATION, 2007.09.26, BY Ken.Wu ( TCMZ Vpi Correction )
            // Read values needed for TCMZ Vpi calculation from config file.
            double tuneISOAPowerSlope
                = progInfo.TestParamsConfig.GetDoubleParam(externalKeyTuneISOAPowerSlope);

            double tuneOpticalPowerToleranceInmW
                = ((DatumDouble)(progInfo.MainSpec.GetParamLimit(externalKeyTuneOpticalPowerToleranceInmW).HighLimit)).Value;

            double tuneOpticalPowerToleranceIndB
                = progInfo.TestParamsConfig.GetDoubleParam(externalKeyTuneOpticalPowerToleranceIndB);

            //Echoxl.wang 2011-02-18
            //add locker pot value to ensure we can get correct ctap value;
            modRun.ConfigData.AddDouble("locker_tran_pot", locker_pot_value);
            modRun.ConfigData.AddDoubleArray("MZFixedModBias_volt", new double[] { mzBias1, mzBias2 });
            modRun.ConfigData.AddReference("FailModeCheck", this.failModeCheck);
            // Alice.Huang   2010-04-27
            // add SOA Current value for convenience
            //modRun.ConfigData.AddDouble("SoaSetupCurrent_mA", 
            //                progInfo.TestParamsConfig.GetDoubleParam("MzChrSetupSoa_mA"));
            modRun.ConfigData.AddDouble("TuneISOAPowerSlope", tuneISOAPowerSlope);
            modRun.ConfigData.AddDouble("TuneOpticalPowerToleranceInmW", tuneOpticalPowerToleranceInmW);
            modRun.ConfigData.AddDouble("TargetFibrePower_dBm",
                            progInfo.TestConditions.TargetFibrePower_dBm);
            modRun.ConfigData.AddDouble("PowerlevelingOffset_dB",
                 ((DatumDouble)progInfo.MainSpec.GetParamLimit("TC_MZ_PWR_CHR_OFFSET").HighLimit).Value);
            //END MODIFICATION, 2007.09.26, By Ken.Wu
            modRun.ConfigData.AddDouble("VcmFactor_FirstChan", progInfo.TestParamsConfig.GetDoubleParam("MzVcmFactor_FirstChan_ForMzChar"));
            modRun.ConfigData.AddDouble("VcmFactor_MiddleChan", progInfo.TestParamsConfig.GetDoubleParam("MzVcmFactor_MiddleChan_ForMzChar"));
            modRun.ConfigData.AddDouble("VcmFactor_LastChan", progInfo.TestParamsConfig.GetDoubleParam("MzVcmFactor_LastChan_ForMzChar"));
            modRun.ConfigData.AddSint32("NumberOfPoints", progInfo.TestParamsConfig.GetIntParam("MzSweepNumberOfPoints"));
            modRun.ConfigData.AddString("DutSerialNbr", dutObject.SerialNumber);
            modRun.ConfigData.AddReference("TestSpec", progInfo.MainSpec);
            modRun.ConfigData.AddString("ResultDir", this.CommonTestConfig.ResultsSubDirectory);
            modRun.ConfigData.AddDouble("LeftBias_V_LI_Sweep", progInfo.TestParamsConfig.GetDoubleParam("LeftBias_V_LI_Sweep"));
            modRun.ConfigData.AddDouble("RightBias_V_LI_Sweep", progInfo.TestParamsConfig.GetDoubleParam("RightBias_V_LI_Sweep"));
            modRun.ConfigData.AddBool("ArmSourceByAsic", progInfo.TestParamsConfig.GetBoolParam("ArmSourceByAsic"));

            if (progInfo.TestConditions.FreqBand.ToString() == "C")
            {
                modRun.ConfigData.AddDouble("MidChannelFrequency", progInfo.TestParamsConfig.GetDoubleParam("MidChannelFrequency_C"));
            }
            else
            {
                modRun.ConfigData.AddDouble("MidChannelFrequency", progInfo.TestParamsConfig.GetDoubleParam("MidChannelFrequency_L"));
            }

            modRun.ConfigData.AddSint32("MZSourceMeasureDelay_ms", progInfo.TestParamsConfig.GetIntParam("MZSourceMeasureDelay_ms"));
            modRun.ConfigData.UpdateDouble("MzCtrlINominal_mA", progInfo.TestParamsConfig.GetDoubleParam("MzCtrlINominal_mA"));
            modRun.ConfigData.AddDouble("MzCtrlINominal_thermal_mA", progInfo.TestParamsConfig.GetDoubleParam("MzCtrlINominal_thermal_mA"));

            #region Add below configdata for Wafer 50

            modRun.ConfigData.AddDouble("LeftBias_V_LI_Sweep_FirstChan", progInfo.TestParamsConfig.GetDoubleParam("LeftBias_V_LI_Sweep_FirstChan"));//
            modRun.ConfigData.AddDouble("RightBias_V_LI_Sweep_FirstChan", progInfo.TestParamsConfig.GetDoubleParam("RightBias_V_LI_Sweep_FirstChan"));
            modRun.ConfigData.AddDouble("LeftBias_V_LI_Sweep_MiddleChan", progInfo.TestParamsConfig.GetDoubleParam("LeftBias_V_LI_Sweep_MiddleChan"));
            modRun.ConfigData.AddDouble("RightBias_V_LI_Sweep_MiddleChan", progInfo.TestParamsConfig.GetDoubleParam("RightBias_V_LI_Sweep_MiddleChan"));
            modRun.ConfigData.AddDouble("LeftBias_V_LI_Sweep_LastChan", progInfo.TestParamsConfig.GetDoubleParam("LeftBias_V_LI_Sweep_LastChan"));
            modRun.ConfigData.AddDouble("RightBias_V_LI_Sweep_LastChan", progInfo.TestParamsConfig.GetDoubleParam("RightBias_V_LI_Sweep_LastChan"));
            modRun.ConfigData.AddDouble("shiftLVWhenQuadLlessThan", progInfo.TestParamsConfig.GetDoubleParam("shiftLVWhenQuadLlessThan"));//-4,

            double maxPeakArmBias = progInfo.TestParamsConfig.GetDoubleParam("MaxPeakArmBias_Nitride");//-6.3

            //if (this._cocResult != null && _cocResult.IsPresent("WAFER_ID"))
            //{
            //    string wafer_id = this._cocResult.ReadString("WAFER_ID");
            //    string oxideWafer = progInfo.TestParamsConfig.GetStringParam("OxideWafer"); // CIL0050
            //    if (wafer_id.ToUpper().Contains(oxideWafer))
            //    {
            //        this.isOxideWafer = true;
            //        modRun.ConfigData.AddBool("OxideWafer", true);
            //        maxPeakArmBias = progInfo.TestParamsConfig.GetDoubleParam("MaxPeakArmBias_Oxide");//-5
            //    }
            //    else
            //    {
            //        modRun.ConfigData.AddBool("OxideWafer", false);
            //    }
            //}
            //else
            //{
            //    modRun.ConfigData.AddBool("OxideWafer", false);
            //}
            modRun.ConfigData.AddDouble("MaxPeakArmBiasV", maxPeakArmBias);

            #endregion

            //#warning Add MZTapBias_V to config class
            modRun.ConfigData.AddDouble("MZInlineTapBias_V", modRun.ConfigData.ReadDouble("MZTapBias_V"));
            modRun.ConfigData.AddDouble("MzFixedModBias_volt_firstChan", progInfo.TestParamsConfig.GetDoubleParam("MzFixedModBias_volt_FirstChan"));//-4
            modRun.ConfigData.AddDouble("MzFixedModBias_volt_middleChan", progInfo.TestParamsConfig.GetDoubleParam("MzFixedModBias_volt_MiddleChan"));
            modRun.ConfigData.AddDouble("MzFixedModBias_volt_lastChan", progInfo.TestParamsConfig.GetDoubleParam("MzFixedModBias_volt_LastChan"));//-3
            modRun.ConfigData.AddDouble("Vmax_firstChan", progInfo.TestParamsConfig.GetDoubleParam("MzVcmFactor_FirstChan_Vmax"));//-1
            modRun.ConfigData.AddDouble("Vmax_middleChan", progInfo.TestParamsConfig.GetDoubleParam("MzVcmFactor_MiddleChan_Vmax"));
            modRun.ConfigData.AddDouble("Vmax_lastChan", progInfo.TestParamsConfig.GetDoubleParam("MzVcmFactor_LastChan_Vmax"));//-0.5
            modRun.ConfigData.AddDouble("Target_ER", progInfo.TestParamsConfig.GetDoubleParam("Target_ER"));
            modRun.ConfigData.AddDouble("Vpi_offset", progInfo.TestParamsConfig.GetDoubleParam("Vpi_offset"));

            // Limits
            modRun.Limits.AddParameter(progInfo.MainSpec, "MZ_INITIAL_CAL_SWEEP_FILE", "MzSweepDataZipFile");
            modRun.Limits.AddParameter(progInfo.MainSpec, "MZ_CAL_EST_FILE", "MzSweepDataResultsFile");
            modRun.Limits.AddParameter(progInfo.MainSpec, "MZ_CAL_EST_FLAG", "AllSweepsPassed");

        }

        /// <summary>
        /// Initialise IlmzMzItuEstimate module
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        private void IlmzMzItuEstimate_InitModule(ITestEngineInit engine, DUTObject dutObject)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("IlmzMzItuEstimate", "IlmzMzItuEstimate_ND", "");

            // Add instruments
            modRun.ConfigData.AddReference("MzInstruments", progInfo.Instrs.Mz);

            // Add config data
            modRun.Instrs.Add("Optical_switch", (Instrument)progInfo.Instrs.TecDsdbr);

            modRun.ConfigData.AddDouble("locker_tran_pot", locker_pot_value);
            modRun.ConfigData.AddReference("Specification", progInfo.MainSpec);
            modRun.ConfigData.AddDouble("MzTapBias_V", progInfo.TestParamsConfig.GetDoubleParam("MzTapBias_V"));
            modRun.ConfigData.AddDouble("MZInlineTapBias_V", progInfo.TestParamsConfig.GetDoubleParam("MzTapBias_V"));
            modRun.ConfigData.AddDouble("CalOffset", progInfo.TestConditions.CalOffset);
            modRun.ConfigData.AddDouble("HighTemp", progInfo.TestConditions.HighTemp);
            modRun.ConfigData.AddDouble("LowTemp", progInfo.TestConditions.LowTemp);
            modRun.ConfigData.AddDouble("FreqLow_GHz", progInfo.TestConditions.ItuLowFreq_GHz);
            modRun.ConfigData.AddDouble("FreqHigh_GHz", progInfo.TestConditions.ItuHighFreq_GHz);
            modRun.ConfigData.AddDouble("FreqSpace_GHz", progInfo.TestConditions.ItuSpacing_GHz);
            modRun.ConfigData.AddSint32("MZSourceMeasureDelay_ms", progInfo.TestParamsConfig.GetIntParam("MZSourceMeasureDelay_ms"));
            //if (isOxideWafer)
            //{
            //    modRun.ConfigData.AddDouble("MzCtrlINominal_mA", progInfo.TestParamsConfig.GetDoubleParam("Oxide_MzCtrlINominal_mA"));
            //}
            //else
            //{
            //    modRun.ConfigData.AddDouble("MzCtrlINominal_mA", progInfo.TestParamsConfig.GetDoubleParam("MzCtrlINominal_mA"));
            //}
            modRun.ConfigData.AddDouble("MzCtrlINominal_mA", progInfo.TestParamsConfig.GetDoubleParam("MzCtrlINominal_mA"));
            modRun.ConfigData.AddString("DUTSerialNbr", dutObject.SerialNumber);
            modRun.ConfigData.AddString("MzFileDirectory", this.CommonTestConfig.ResultsSubDirectory);
            modRun.ConfigData.AddDouble("Vmax", progInfo.TestParamsConfig.GetDoubleParam("MzVcmFactor_Vmax"));
            modRun.ConfigData.AddDouble("Vmax_lastChan", progInfo.TestParamsConfig.GetDoubleParam("MzVcmFactor_LastChan_Vmax"));
            modRun.ConfigData.AddBool("ArmSourceByAsic", progInfo.TestParamsConfig.GetBoolParam("ArmSourceByAsic"));

            // for Vpi correction
            double VpiCorrectionUpperAdjustDelta = progInfo.TestParamsConfig.GetDoubleParam("VpiCorrectionUpperAdjustDelta");
            double VpiCorrectionLowerAdjustDelta = progInfo.TestParamsConfig.GetDoubleParam("VpiCorrectionLowerAdjustDelta");
            modRun.ConfigData.AddDouble("VpiCorrectionUpperAdjustDelta", VpiCorrectionUpperAdjustDelta);
            modRun.ConfigData.AddDouble("VpiCorrectionLowerAdjustDelta", VpiCorrectionLowerAdjustDelta);
            modRun.ConfigData.AddSint32("NumberOfPoints", progInfo.TestParamsConfig.GetIntParam("MzSweepNumberOfPoints"));
            modRun.ConfigData.AddString("DutSerialNbr", dutObject.SerialNumber);
            // Add limits
            modRun.Limits.AddParameter(progInfo.MainSpec, "MZ_CAL_EST_FILE_FINAL_VCM", "MzAtNewVcmResultsFile");
            modRun.Limits.AddParameter(progInfo.MainSpec, "MZ_CAL_EST_FLAG_FINAL_VCM", "MzAtNewVcmPass");
        }

        /// <summary>
        /// Configure Channel Characterisation module
        /// </summary>
        private void IlmzChannelChar_InitModule(ITestEngineInit engine, DUTObject dutObject)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("IlmzChannelChar", "IlmzChannelChar_OverTemp", "");
            // Add instruments
            modRun.ConfigData.AddReference("MzInstruments", progInfo.Instrs.Mz);
            //modRun.Instrs.Add("MzTec", (Instrument)progInfo.Instrs.TecMz);
            modRun.Instrs.Add("DsdbrTec", (Instrument)progInfo.Instrs.TecDsdbr);
            modRun.Instrs.Add("Optical_switch", (Instrument)progInfo.Instrs.TecDsdbr);

            modRun.ConfigData.AddDouble("locker_tran_pot", locker_pot_value);
            //modRun.ConfigData.AddReference("SwitchOsaMzOpm", progInfo.Instrs.SwitchOsaMzOpm);

            // Add config data
            TcmzMzSweep_Config tcmzConfig = new TcmzMzSweep_Config();
            DatumList datumsRequired = tcmzConfig.GetDatumList();
            ExtractAndAddDatums(modRun, datumsRequired);
            modRun.ConfigData.AddEnum("Temp", TcmzTestTemp.Low);
            modRun.ConfigData.AddReference("TestSelect", progInfo.TestSelect);
            modRun.ConfigData.AddString("DutSerialNbr", dutObject.SerialNumber);
            //Modify non-rules old statment at 2008-10-10 by tim;
            //modRun.ConfigData.AddBool("DoPwrCtrlRangeTests", dutObject.PartCode.ToLower().Contains("lmt10ncc"));
            modRun.ConfigData.AddBool("DoPwrCtrlRangeTests", progInfo.TestParamsConfig.GetBoolParam("DoPwrCtrlRangeTests"));
            //Modify non-ruless old statment at 2008-10-10;
            //modRun.ConfigData.AddBool("DoTapLevelling", !dutObject.PartCode.ToLower().Contains("lmt10ncb"));
            modRun.ConfigData.AddBool("DoTapLevelling", progInfo.TestParamsConfig.GetBoolParam("DoTapLevelling"));
            modRun.ConfigData.AddDouble("TapPhotoCurrentToleranceRatio",
            progInfo.TestParamsConfig.GetDoubleParam("TapPhotoCurrentToleranceRatio"));
            //modRun.ConfigData.AddDouble("MZModSweepMax_volt", progInfo.TestParamsConfig.GetDoubleParam("MzSeModSweepMax_volt"));
            //modRun.ConfigData.AddDouble("MZFixedModBias_volt", progInfo.TestParamsConfig.GetDoubleParam("MzSeFixedModBias_volt"));
            modRun.ConfigData.AddDouble("PwrControlRangeLow", progInfo.TestConditions.PwrControlRangeLow);
            modRun.ConfigData.AddDouble("PwrControlRangeHigh", progInfo.TestConditions.PwrControlRangeHigh);
            modRun.ConfigData.AddDouble("Tune_OpticalPowerTolerance_dB",
                progInfo.TestParamsConfig.GetDoubleParam("Tune_OpticalPowerTolerance_dB"));
            modRun.ConfigData.AddDouble("Tune_OpticalPowerTolerance_mW", 0.06); //by tim
            modRun.ConfigData.AddDouble("Tune_IsoaPowerSlope", progInfo.TestParamsConfig.GetDoubleParam("Tune_IsoaPowerSlope"));
            modRun.ConfigData.AddDouble("MaxSoaForPwrLeveling", Convert.ToDouble(
                progInfo.MainSpec.GetParamLimit("CH_ITU_SOA_I").HighLimit.ValueToString()));
            
            double CH_LASER_RTH_Min = Convert.ToDouble(this.progInfo.MainSpec.GetParamLimit("CH_LASER_RTH").LowLimit.ValueToString());
            double CH_LASER_RTH_Max = Convert.ToDouble(this.progInfo.MainSpec.GetParamLimit("CH_LASER_RTH").HighLimit.ValueToString());
            modRun.ConfigData.AddDouble("CH_LASER_RTH_Min", CH_LASER_RTH_Min);
            modRun.ConfigData.AddDouble("CH_LASER_RTH_Max", CH_LASER_RTH_Max);

            bool isTestRippleOfEOL = this.progInfo.TestParamsConfig.GetBoolParam("IsTestRippleOfEOL");
            modRun.ConfigData.AddBool("IsTestRippleOfEOL", isTestRippleOfEOL);

            if (isTestRippleOfEOL)
            {
                this.eolPhaseScanDropFilePath = Util_GenerateFileName.GenWithTimestamp(this.CommonTestConfig.ResultsSubDirectory, "EOLPhaseScanDropFile", dutObject.SerialNumber, "csv");

                File.Create(this.eolPhaseScanDropFilePath).Close();

                modRun.ConfigData.AddBool("UseLowCostSolution", progInfo.MapParamsConfig.GetBoolParam("UseLowCostSolution"));
                modRun.ConfigData.AddString("EOLPhaseScanDropFilePath", this.eolPhaseScanDropFilePath);
                modRun.ConfigData.AddString("PhaseCurrentFile", progInfo.MapParamsConfig.GetStringParam("PhaseCurrentFile"));

                RippleTestOperations.LoadConfigToModule(this._cocResult, modRun.ConfigData);
            }

            bool loggingEnabled = true;
            try
            {
                loggingEnabled = this.progInfo.TestParamsConfig.GetBoolParam("LoggintEnabled");
            }
            catch (Exception /*e*/ ) { }
            modRun.ConfigData.AddBool("LoggingEnabled", loggingEnabled);
            modRun.ConfigData.AddBool("TraceToneTestEnabled", progInfo.TestParamsConfig.GetBoolParam("TraceToneTestEnabled"));
            //modRun.ConfigData.AddSint32("MZSourceMeasureDelay_ms", progInfo.TestParamsConfig.GetIntParam("MZSourceMeasureDelay_ms"));
            modRun.ConfigData.AddBool("DO_GRandR", this.DO_GRandR);

            modRun.ConfigData.AddString("ResultDir", this.CommonTestConfig.ResultsSubDirectory);

#warning Add to config
            modRun.ConfigData.AddBool("AlwaysTuneLaser", true);
            //modRun.ConfigData.AddString("IPhaseModAcqFilesCollection", this._mapResult.ReadFileLinkFullPath("CG_PHASE_MODE_ACQ_FILE"));
            DsdbrTuning.IphaseMin_mA = Convert.ToDouble(progInfo.MainSpec.GetParamLimit("CH_CAL_PHASE_I").LowLimit.ValueToString()); //by tim
            DsdbrTuning.IphaseMax_mA = Convert.ToDouble(progInfo.MainSpec.GetParamLimit("CH_CAL_PHASE_I").HighLimit.ValueToString()); //by tim

            // Tie up limits
            Specification spec = progInfo.MainSpec;

            if (spec.ParamLimitExists("LM_PHASE_SCAN_RIPPLE_MID"))
            {
                modRun.Limits.AddParameter(spec, "LM_PHASE_SCAN_RIPPLE_MID", "LM_PHASE_SCAN_RIPPLE_MID");
            }

            modRun.Limits.AddParameter(spec, "TCASE_MID_PASS_FAIL_FLAG", "PassFailFlag");
            modRun.Limits.AddParameter(spec, "NUM_CHAN_TEST_TCASE_MID", "NumChannelsTested");
            modRun.Limits.AddParameter(spec, "NUM_CHAN_PASS_TCASE_MID", "NumChannelsPass");
        }

        /// <summary>
        /// Initialise IlmzLowTempTest module
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        private void IlmzLowTempTest_InitModule(ITestEngineInit engine, DUTObject dutObject)
        {
            // Initialise module
            TempTablePoint tempCal;
            ModuleRun modRun;
            if (dutObject.Attributes.ReadString("ChirpType").Contains("ND"))
            {
                modRun = engine.AddModuleRun("IlmzLowTempTest", "IlmzTempTest_ND", "");

                //add casetec
                tempCal = progInfo.TempConfig.GetTemperatureCal(progInfo.TestConditions.MidTemp, progInfo.TestConditions.LowTemp)[0];
                modRun.ConfigData.AddDouble("CaseTec_SetPointTemperature_C", progInfo.TestConditions.LowTemp + tempCal.OffsetC);
                modRun.ConfigData.AddDouble("CaseTec_TemperatureTolerance_C", tempCal.Tolerance_degC);
                modRun.Instrs.Add("CaseTec", (Instrument)progInfo.Instrs.TecCase);

                // Add instruments
                modRun.ConfigData.AddReference("MzInstruments", progInfo.Instrs.Mz);
                //modRun.Instrs.Add("MzTec", (Instrument)progInfo.Instrs.TecMz);
                modRun.Instrs.Add("DsdbrTec", (Instrument)progInfo.Instrs.TecDsdbr);
                modRun.Instrs.Add("Optical_switch", (Instrument)progInfo.Instrs.TecDsdbr);

                modRun.ConfigData.AddReference("FailModeCheck", this.failModeCheck);
                modRun.ConfigData.AddDouble("locker_tran_pot", locker_pot_value);
                //modRun.ConfigData.AddReference("SwitchOsaMzOpm", progInfo.Instrs.SwitchOsaMzOpm);

                // Add config data
                TcmzMzSweep_Config tcmzConfig = new TcmzMzSweep_Config();
                DatumList datumsRequired = tcmzConfig.GetDatumList();
                ExtractAndAddDatums(modRun, datumsRequired);
                modRun.ConfigData.AddEnum("Temp", TcmzTestTemp.Low);
                modRun.ConfigData.AddReference("TestSelect", progInfo.TestSelect);
                modRun.ConfigData.AddString("DutSerialNbr", dutObject.SerialNumber);
                //Modify non-rules old statment at 2008-10-10 by tim;
                //modRun.ConfigData.AddBool("DoPwrCtrlRangeTests", dutObject.PartCode.ToLower().Contains("lmt10ncc"));
                modRun.ConfigData.AddBool("DoPwrCtrlRangeTests", progInfo.TestParamsConfig.GetBoolParam("DoPwrCtrlRangeTests"));
                //Modify non-ruless old statment at 2008-10-10;
                //modRun.ConfigData.AddBool("DoTapLevelling", !dutObject.PartCode.ToLower().Contains("lmt10ncb"));
                modRun.ConfigData.AddBool("DoTapLevelling", progInfo.TestParamsConfig.GetBoolParam("DoTapLevelling"));
                modRun.ConfigData.AddDouble("TapPhotoCurrentToleranceRatio",
                progInfo.TestParamsConfig.GetDoubleParam("TapPhotoCurrentToleranceRatio"));
                //modRun.ConfigData.AddDouble("MZModSweepMax_volt", progInfo.TestParamsConfig.GetDoubleParam("MzSeModSweepMax_volt"));
                //modRun.ConfigData.AddDouble("MZFixedModBias_volt", progInfo.TestParamsConfig.GetDoubleParam("MzSeFixedModBias_volt"));
                modRun.ConfigData.AddDouble("PwrControlRangeLow", progInfo.TestConditions.PwrControlRangeLow);
                modRun.ConfigData.AddDouble("PwrControlRangeHigh", progInfo.TestConditions.PwrControlRangeHigh);
                modRun.ConfigData.AddDouble("Tune_OpticalPowerTolerance_dB",
                    progInfo.TestParamsConfig.GetDoubleParam("Tune_OpticalPowerTolerance_dB"));
                modRun.ConfigData.AddDouble("Tune_OpticalPowerTolerance_mW", progInfo.TestParamsConfig.GetDoubleParam("Tune_OpticalPowerTolerance_mW")); //by tim
                modRun.ConfigData.AddDouble("Tune_IsoaPowerSlope", progInfo.TestParamsConfig.GetDoubleParam("Tune_IsoaPowerSlope"));
                modRun.ConfigData.AddDouble("MaxSoaForPwrLeveling", Convert.ToDouble(
                    progInfo.MainSpec.GetParamLimit("CH_ITU_SOA_I").HighLimit.ValueToString()));

                bool loggingEnabled = true;
                try
                {
                    loggingEnabled = this.progInfo.TestParamsConfig.GetBoolParam("LoggintEnabled");
                }
                catch (Exception /*e*/ ) { }
                modRun.ConfigData.AddBool("LoggingEnabled", loggingEnabled);
                modRun.ConfigData.AddBool("TraceToneTestEnabled", progInfo.TestParamsConfig.GetBoolParam("TraceToneTestEnabled"));
                modRun.ConfigData.AddSint32("MZSourceMeasureDelay_ms", progInfo.TestParamsConfig.GetIntParam("MZSourceMeasureDelay_ms"));
                modRun.ConfigData.AddBool("DO_GRandR", this.DO_GRandR);

                modRun.ConfigData.AddString("ResultDir", this.CommonTestConfig.ResultsSubDirectory);
                modRun.ConfigData.AddDouble("TC_MZ_MINIMA_V_LIMIT_MIN", Convert.ToDouble(progInfo.MainSpec.GetParamLimit("TC_MZ_MINIMA_V_LIMIT_MID").LowLimit.ValueToString()));
                modRun.ConfigData.AddReference("Specification", progInfo.MainSpec);

                // Tie up limits
                Specification spec = progInfo.MainSpec;
                modRun.Limits.AddParameter(spec, "LOW_DISS_OPTICAL_FREQ", "LOW_DISS_OPTICAL_FREQ");
                modRun.Limits.AddParameter(spec, "LOW_DISS_SOL", "LOW_DISS_SOL");
                modRun.Limits.AddParameter(spec, "PACK_DISS_TCASE_LOW", "PACK_DISS_TCASE_LOW");
                modRun.Limits.AddParameter(spec, "TEC_I_TCASE_LOW", "LASER_TEC_I_TCASE_LOW");
                modRun.Limits.AddParameter(spec, "TEC_V_TCASE_LOW", "LASER_TEC_V_TCASE_LOW");
                //modRun.Limits.AddParameter(spec, "MZ_TEC_I_TCASE_LOW", "MZ_TEC_I_TCASE_LOW");
                //modRun.Limits.AddParameter(spec, "MZ_TEC_V_TCASE_LOW", "MZ_TEC_V_TCASE_LOW");
                modRun.Limits.AddParameter(spec, "NUM_CHAN_TEST_TCASE_LOW", "NumChannelsTested");
                modRun.Limits.AddParameter(spec, "NUM_CHAN_PASS_TCASE_LOW", "NumChannelsPass");
                modRun.Limits.AddParameter(spec, "NUM_LOCK_FAIL_TCASE_LOW", "NumLockFails");
                modRun.Limits.AddParameter(spec, "TCASE_LOW_PASS_FAIL_FLAG", "PassFailFlag");
            } //by tim
            else if (dutObject.Attributes.ReadString("ChirpType").Contains("ZD"))
            {
                modRun = engine.AddModuleRun("IlmzLowTempTest", "IlmzTempTest_ZD", "");

                tempCal = progInfo.TempConfig.GetTemperatureCal(progInfo.TestConditions.MidTemp, progInfo.TestConditions.LowTemp)[0];
                modRun.ConfigData.AddDouble("CaseTec_SetPointTemperature_C", progInfo.TestConditions.LowTemp + tempCal.OffsetC);
                modRun.ConfigData.AddDouble("CaseTec_TemperatureTolerance_C", tempCal.Tolerance_degC);
                modRun.Instrs.Add("CaseTec", (Instrument)progInfo.Instrs.TecCase);

                // Add instruments
                modRun.ConfigData.AddReference("MzInstruments", progInfo.Instrs.Mz);
                //modRun.Instrs.Add("MzTec", (Instrument)progInfo.Instrs.TecMz);
                modRun.Instrs.Add("DsdbrTec", (Instrument)progInfo.Instrs.TecDsdbr);
                modRun.Instrs.Add("Optical_switch", (Instrument)progInfo.Instrs.TecDsdbr);

                modRun.ConfigData.AddReference("FailModeCheck", this.failModeCheck);
                modRun.ConfigData.AddDouble("locker_tran_pot", locker_pot_value);
                //modRun.ConfigData.AddReference("SwitchOsaMzOpm", progInfo.Instrs.SwitchOsaMzOpm);
                modRun.ConfigData.AddBool("IsUseNegSlopeImbInZC", progInfo.TestParamsConfig.GetBoolParam("IsUseNegSlopeImbInZC"));

                // Add config data
                TcmzMzSweep_Config tcmzConfig = new TcmzMzSweep_Config();
                DatumList datumsRequired = tcmzConfig.GetDatumList();
                ExtractAndAddDatums(modRun, datumsRequired);
                modRun.ConfigData.AddEnum("Temp", TcmzTestTemp.Low);
                modRun.ConfigData.AddReference("TestSelect", progInfo.TestSelect);
                modRun.ConfigData.AddString("DutSerialNbr", dutObject.SerialNumber);
                //Modify non-rules old statment at 2008-10-10 by tim;
                //modRun.ConfigData.AddBool("DoPwrCtrlRangeTests", dutObject.PartCode.ToLower().Contains("lmt10ncc"));
                modRun.ConfigData.AddBool("DoPwrCtrlRangeTests", progInfo.TestParamsConfig.GetBoolParam("DoPwrCtrlRangeTests"));
                //Modify non-ruless old statment at 2008-10-10;
                //modRun.ConfigData.AddBool("DoTapLevelling", !dutObject.PartCode.ToLower().Contains("lmt10ncb"));
                modRun.ConfigData.AddBool("DoTapLevelling", progInfo.TestParamsConfig.GetBoolParam("DoTapLevelling"));
                modRun.ConfigData.AddDouble("TapPhotoCurrentToleranceRatio",
                progInfo.TestParamsConfig.GetDoubleParam("TapPhotoCurrentToleranceRatio"));
                //modRun.ConfigData.AddDouble("MZModSweepMax_volt", progInfo.TestParamsConfig.GetDoubleParam("MzSeModSweepMax_volt"));
                //modRun.ConfigData.AddDouble("MZFixedModBias_volt", progInfo.TestParamsConfig.GetDoubleParam("MzSeFixedModBias_volt"));
                modRun.ConfigData.AddDouble("PwrControlRangeLow", progInfo.TestConditions.PwrControlRangeLow);
                modRun.ConfigData.AddDouble("PwrControlRangeHigh", progInfo.TestConditions.PwrControlRangeHigh);
                modRun.ConfigData.AddDouble("Tune_OpticalPowerTolerance_dB",
                    progInfo.TestParamsConfig.GetDoubleParam("Tune_OpticalPowerTolerance_dB"));
                modRun.ConfigData.AddDouble("Tune_OpticalPowerTolerance_mW", progInfo.TestParamsConfig.GetDoubleParam("Tune_OpticalPowerTolerance_mW")); //by tim
                modRun.ConfigData.AddDouble("Tune_IsoaPowerSlope", progInfo.TestParamsConfig.GetDoubleParam("Tune_IsoaPowerSlope"));
                modRun.ConfigData.AddDouble("MaxSoaForPwrLeveling", Convert.ToDouble(
                    progInfo.MainSpec.GetParamLimit("CH_ITU_SOA_I").HighLimit.ValueToString()));

                double CH_LASER_RTH_Min = Convert.ToDouble(this.progInfo.MainSpec.GetParamLimit("CH_LASER_RTH").LowLimit.ValueToString());
                double CH_LASER_RTH_Max = Convert.ToDouble(this.progInfo.MainSpec.GetParamLimit("CH_LASER_RTH").HighLimit.ValueToString());
                modRun.ConfigData.AddDouble("CH_LASER_RTH_Min", CH_LASER_RTH_Min);
                modRun.ConfigData.AddDouble("CH_LASER_RTH_Max", CH_LASER_RTH_Max);

                bool isTestRippleOfEOL = this.progInfo.TestParamsConfig.GetBoolParam("IsTestRippleOfEOL");
                modRun.ConfigData.AddBool("IsTestRippleOfEOL", isTestRippleOfEOL);

                if (isTestRippleOfEOL)
                {
                    modRun.ConfigData.AddBool("UseLowCostSolution", progInfo.MapParamsConfig.GetBoolParam("UseLowCostSolution"));
                    modRun.ConfigData.AddString("EOLPhaseScanDropFilePath", this.eolPhaseScanDropFilePath);
                    modRun.ConfigData.AddString("PhaseCurrentFile", progInfo.MapParamsConfig.GetStringParam("PhaseCurrentFile"));

                    RippleTestOperations.LoadConfigToModule(this._cocResult, modRun.ConfigData);
                }

                bool loggingEnabled = true;
                try
                {
                    loggingEnabled = this.progInfo.TestParamsConfig.GetBoolParam("LoggintEnabled");
                }
                catch (Exception /*e*/ ) { }
                modRun.ConfigData.AddBool("LoggingEnabled", loggingEnabled);
                modRun.ConfigData.AddBool("TraceToneTestEnabled", progInfo.TestParamsConfig.GetBoolParam("TraceToneTestEnabled"));
                modRun.ConfigData.AddSint32("MZSourceMeasureDelay_ms", progInfo.TestParamsConfig.GetIntParam("MZSourceMeasureDelay_ms"));
                modRun.ConfigData.AddBool("DO_GRandR", this.DO_GRandR);

                modRun.ConfigData.AddString("ResultDir", this.CommonTestConfig.ResultsSubDirectory);
                modRun.ConfigData.AddDouble("TC_MZ_MINIMA_V_LIMIT_MIN", Convert.ToDouble(progInfo.MainSpec.GetParamLimit("TC_MZ_MINIMA_V_LIMIT_MID").LowLimit.ValueToString()));
                modRun.ConfigData.AddReference("Specification", progInfo.MainSpec);

                // Tie up limits
                Specification spec = progInfo.MainSpec;

                if (spec.ParamLimitExists("LM_PHASE_SCAN_RIPPLE_LOW"))
                {
                    modRun.Limits.AddParameter(spec, "LM_PHASE_SCAN_RIPPLE_LOW", "LM_PHASE_SCAN_RIPPLE_LOW");
                }

                modRun.Limits.AddParameter(spec, "LOW_DISS_OPTICAL_FREQ", "LOW_DISS_OPTICAL_FREQ");
                modRun.Limits.AddParameter(spec, "LOW_DISS_SOL", "LOW_DISS_SOL");
                modRun.Limits.AddParameter(spec, "PACK_DISS_TCASE_LOW", "PACK_DISS_TCASE_LOW");
                modRun.Limits.AddParameter(spec, "TEC_I_TCASE_LOW", "LASER_TEC_I_TCASE_LOW");
                modRun.Limits.AddParameter(spec, "TEC_V_TCASE_LOW", "LASER_TEC_V_TCASE_LOW");
                //modRun.Limits.AddParameter(spec, "MZ_TEC_I_TCASE_LOW", "MZ_TEC_I_TCASE_LOW");
                //modRun.Limits.AddParameter(spec, "MZ_TEC_V_TCASE_LOW", "MZ_TEC_V_TCASE_LOW");
                modRun.Limits.AddParameter(spec, "NUM_CHAN_TEST_TCASE_LOW", "NumChannelsTested");
                modRun.Limits.AddParameter(spec, "NUM_CHAN_PASS_TCASE_LOW", "NumChannelsPass");
                modRun.Limits.AddParameter(spec, "NUM_LOCK_FAIL_TCASE_LOW", "NumLockFails");
                modRun.Limits.AddParameter(spec, "TCASE_LOW_PASS_FAIL_FLAG", "PassFailFlag");
            } // by tim
        }

        /// <summary>
        /// Initialise TcmHighTempTest module
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        private void IlmzHighTempTest_InitModule(ITestEngineInit engine, DUTObject dutObject)
        {
            // Initialise module
            TempTablePoint tempCal;
            ModuleRun modRun;
            if (dutObject.Attributes.ReadString("ChirpType").Contains("ND"))
            {
                modRun = engine.AddModuleRun("IlmzHighTempTest", "IlmzTempTest_ND", "");
                tempCal = progInfo.TempConfig.GetTemperatureCal(progInfo.TestConditions.LowTemp, progInfo.TestConditions.HighTemp)[0];
                modRun.ConfigData.AddDouble("CaseTec_SetPointTemperature_C", progInfo.TestConditions.HighTemp + tempCal.OffsetC);
                modRun.ConfigData.AddDouble("CaseTec_TemperatureTolerance_C", tempCal.Tolerance_degC);
                modRun.Instrs.Add("CaseTec", (Instrument)progInfo.Instrs.TecCase);


                // Add instruments
                modRun.ConfigData.AddReference("MzInstruments", progInfo.Instrs.Mz);
                //modRun.Instrs.Add("MzTec", (Instrument)progInfo.Instrs.TecMz);
                modRun.Instrs.Add("DsdbrTec", (Instrument)progInfo.Instrs.TecDsdbr);
                modRun.Instrs.Add("Optical_switch", (Instrument)progInfo.Instrs.TecDsdbr);

                modRun.ConfigData.AddReference("FailModeCheck", this.failModeCheck);
                modRun.ConfigData.AddDouble("locker_tran_pot", locker_pot_value);
                //modRun.ConfigData.AddReference("SwitchOsaMzOpm", progInfo.Instrs.SwitchOsaMzOpm);

                // Add config data
                TcmzMzSweep_Config tcmzConfig = new TcmzMzSweep_Config();
                DatumList datumsRequired = tcmzConfig.GetDatumList();
                ExtractAndAddDatums(modRun, datumsRequired);
                modRun.ConfigData.AddEnum("Temp", TcmzTestTemp.High);
                modRun.ConfigData.AddReference("TestSelect", progInfo.TestSelect);
                modRun.ConfigData.AddString("DutSerialNbr", dutObject.SerialNumber);
                //modRun.ConfigData.AddBool("DoPwrCtrlRangeTests", dutObject.PartCode.ToLower().Contains("lmt10ncc"));
                modRun.ConfigData.AddBool("DoPwrCtrlRangeTests", progInfo.TestParamsConfig.GetBoolParam("DoPwrCtrlRangeTests"));
                //Modify non-rules old statment;
                //modRun.ConfigData.AddBool("DoTapLevelling", !dutObject.PartCode.ToLower().Contains("lmt10ncb"));
                modRun.ConfigData.AddBool("DoTapLevelling", progInfo.TestParamsConfig.GetBoolParam("DoTapLevelling"));
                modRun.ConfigData.AddDouble("TapPhotoCurrentToleranceRatio",
                    progInfo.TestParamsConfig.GetDoubleParam("TapPhotoCurrentToleranceRatio"));
                modRun.ConfigData.AddDouble("LaserTecEolDeltaT_C", progInfo.TestConditions.LaserTecEolDeltaT_C);
                //modRun.ConfigData.AddDouble("MzTecEolDeltaT_C", progInfo.TestConditions.MzTecEolDeltaT_C);
                modRun.ConfigData.AddDouble("PwrControlRangeLow", progInfo.TestConditions.PwrControlRangeLow); //by tim
                modRun.ConfigData.AddDouble("PwrControlRangeHigh", progInfo.TestConditions.PwrControlRangeHigh); // by tim
                modRun.ConfigData.AddDouble("Tune_OpticalPowerTolerance_dB",
                    progInfo.TestParamsConfig.GetDoubleParam("Tune_OpticalPowerTolerance_dB"));
                modRun.ConfigData.AddDouble("Tune_OpticalPowerTolerance_mW", 0.06); //by tim
                modRun.ConfigData.AddDouble("Tune_IsoaPowerSlope",
                    progInfo.TestParamsConfig.GetDoubleParam("Tune_IsoaPowerSlope"));
                modRun.ConfigData.AddDouble("MaxSoaForPwrLeveling", Convert.ToDouble(
                    progInfo.MainSpec.GetParamLimit("CH_ITU_SOA_I").HighLimit.ValueToString()));

                bool loggingEnabled = true;
                try
                {
                    loggingEnabled = this.progInfo.TestParamsConfig.GetBoolParam("LoggintEnabled");
                }
                catch (Exception /*e*/ ) { }
                modRun.ConfigData.AddBool("LoggingEnabled", loggingEnabled);
                modRun.ConfigData.AddBool("TraceToneTestEnabled", progInfo.TestParamsConfig.GetBoolParam("TraceToneTestEnabled"));
                modRun.ConfigData.AddSint32("MZSourceMeasureDelay_ms", progInfo.TestParamsConfig.GetIntParam("MZSourceMeasureDelay_ms"));
                modRun.ConfigData.AddBool("DO_GRandR", this.DO_GRandR);

                modRun.ConfigData.AddString("ResultDir", this.CommonTestConfig.ResultsSubDirectory);
                modRun.ConfigData.AddDouble("TC_MZ_MINIMA_V_LIMIT_MIN", Convert.ToDouble(progInfo.MainSpec.GetParamLimit("TC_MZ_MINIMA_V_LIMIT_MID").LowLimit.ValueToString()));
                modRun.ConfigData.AddReference("Specification", progInfo.MainSpec);

                // Tie up limits
                Specification spec = progInfo.MainSpec;
                modRun.Limits.AddParameter(spec, "HIGH_DISS_OPTICAL_FREQ", "HIGH_DISS_OPTICAL_FREQ");
                modRun.Limits.AddParameter(spec, "HIGH_DISS_SOL", "HIGH_DISS_SOL");
                modRun.Limits.AddParameter(spec, "PACK_DISS_TCASE_HIGH", "PACK_DISS_TCASE_HIGH");
                modRun.Limits.AddParameter(spec, "TEC_I_TCASE_HIGH", "LASER_TEC_I_TCASE_HIGH");
                modRun.Limits.AddParameter(spec, "TEC_V_TCASE_HIGH", "LASER_TEC_V_TCASE_HIGH");
                //modRun.Limits.AddParameter(spec, "MZ_TEC_I_TCASE_HIGH", "MZ_TEC_I_TCASE_HIGH");
                //modRun.Limits.AddParameter(spec, "MZ_TEC_V_TCASE_HIGH", "MZ_TEC_V_TCASE_HIGH");
                modRun.Limits.AddParameter(spec, "TEC_I_TCASE_HIGH_EOL", "LASER_TEC_I_TCASE_HIGH_EOL");
                modRun.Limits.AddParameter(spec, "TEC_V_TCASE_HIGH_EOL", "LASER_TEC_V_TCASE_HIGH_EOL");
                //modRun.Limits.AddParameter(spec, "MZ_TEC_I_TCASE_HIGH_EOL", "MZ_TEC_I_TCASE_HIGH_EOL");
                //modRun.Limits.AddParameter(spec, "MZ_TEC_V_TCASE_HIGH_EOL", "MZ_TEC_V_TCASE_HIGH_EOL");
                modRun.Limits.AddParameter(spec, "PACK_DISS_TCASE_HIGH_EOL", "PACK_DISS_TCASE_HIGH_EOL");
                modRun.Limits.AddParameter(spec, "NUM_CHAN_TEST_TCASE_HIGH", "NumChannelsTested");
                modRun.Limits.AddParameter(spec, "NUM_CHAN_PASS_TCASE_HIGH", "NumChannelsPass");
                modRun.Limits.AddParameter(spec, "NUM_LOCK_FAIL_TCASE_HIGH", "NumLockFails");
                modRun.Limits.AddParameter(spec, "TCASE_HIGH_PASS_FAIL_FLAG", "PassFailFlag");
            }// by tim
            else if (dutObject.Attributes.ReadString("ChirpType").Contains("ZD"))
            {
                modRun = engine.AddModuleRun("IlmzHighTempTest", "IlmzTempTest_ZD", "");

                tempCal = progInfo.TempConfig.GetTemperatureCal(progInfo.TestConditions.LowTemp, progInfo.TestConditions.HighTemp)[0];
                modRun.ConfigData.AddDouble("CaseTec_SetPointTemperature_C", progInfo.TestConditions.HighTemp + tempCal.OffsetC);
                modRun.ConfigData.AddDouble("CaseTec_TemperatureTolerance_C", tempCal.Tolerance_degC);
                modRun.Instrs.Add("CaseTec", (Instrument)progInfo.Instrs.TecCase);

                // Add instruments
                modRun.ConfigData.AddReference("MzInstruments", progInfo.Instrs.Mz);
                //modRun.Instrs.Add("MzTec", (Instrument)progInfo.Instrs.TecMz);
                modRun.Instrs.Add("DsdbrTec", (Instrument)progInfo.Instrs.TecDsdbr);
                modRun.Instrs.Add("Optical_switch", (Instrument)progInfo.Instrs.TecDsdbr);

                modRun.ConfigData.AddReference("FailModeCheck", this.failModeCheck);
                modRun.ConfigData.AddDouble("locker_tran_pot", locker_pot_value);
                //modRun.ConfigData.AddReference("SwitchOsaMzOpm", progInfo.Instrs.SwitchOsaMzOpm);
                modRun.ConfigData.AddBool("IsUseNegSlopeImbInZC", progInfo.TestParamsConfig.GetBoolParam("IsUseNegSlopeImbInZC"));

                // Add config data
                TcmzMzSweep_Config tcmzConfig = new TcmzMzSweep_Config();
                DatumList datumsRequired = tcmzConfig.GetDatumList();
                ExtractAndAddDatums(modRun, datumsRequired);
                modRun.ConfigData.AddEnum("Temp", TcmzTestTemp.High);
                modRun.ConfigData.AddReference("TestSelect", progInfo.TestSelect);
                modRun.ConfigData.AddString("DutSerialNbr", dutObject.SerialNumber);
                //modRun.ConfigData.AddBool("DoPwrCtrlRangeTests", dutObject.PartCode.ToLower().Contains("lmt10ncc"));
                modRun.ConfigData.AddBool("DoPwrCtrlRangeTests", progInfo.TestParamsConfig.GetBoolParam("DoPwrCtrlRangeTests"));
                //Modify non-rules old statment;
                //modRun.ConfigData.AddBool("DoTapLevelling", !dutObject.PartCode.ToLower().Contains("lmt10ncb"));
                modRun.ConfigData.AddBool("DoTapLevelling", progInfo.TestParamsConfig.GetBoolParam("DoTapLevelling"));
                modRun.ConfigData.AddDouble("TapPhotoCurrentToleranceRatio",
                    progInfo.TestParamsConfig.GetDoubleParam("TapPhotoCurrentToleranceRatio"));
                modRun.ConfigData.AddDouble("LaserTecEolDeltaT_C", progInfo.TestConditions.LaserTecEolDeltaT_C);
                //modRun.ConfigData.AddDouble("MzTecEolDeltaT_C", progInfo.TestConditions.MzTecEolDeltaT_C);
                modRun.ConfigData.AddDouble("PwrControlRangeLow", progInfo.TestConditions.PwrControlRangeLow); //by tim
                modRun.ConfigData.AddDouble("PwrControlRangeHigh", progInfo.TestConditions.PwrControlRangeHigh); // by tim
                modRun.ConfigData.AddDouble("Tune_OpticalPowerTolerance_dB",
                    progInfo.TestParamsConfig.GetDoubleParam("Tune_OpticalPowerTolerance_dB"));
                modRun.ConfigData.AddDouble("Tune_OpticalPowerTolerance_mW", 0.06); //by tim
                modRun.ConfigData.AddDouble("Tune_IsoaPowerSlope",
                    progInfo.TestParamsConfig.GetDoubleParam("Tune_IsoaPowerSlope"));
                modRun.ConfigData.AddDouble("MaxSoaForPwrLeveling", Convert.ToDouble(
                    progInfo.MainSpec.GetParamLimit("CH_ITU_SOA_I").HighLimit.ValueToString()));

                double CH_LASER_RTH_Min = Convert.ToDouble(this.progInfo.MainSpec.GetParamLimit("CH_LASER_RTH").LowLimit.ValueToString());
                double CH_LASER_RTH_Max = Convert.ToDouble(this.progInfo.MainSpec.GetParamLimit("CH_LASER_RTH").HighLimit.ValueToString());
                modRun.ConfigData.AddDouble("CH_LASER_RTH_Min", CH_LASER_RTH_Min);
                modRun.ConfigData.AddDouble("CH_LASER_RTH_Max", CH_LASER_RTH_Max);

                bool isTestRippleOfEOL = this.progInfo.TestParamsConfig.GetBoolParam("IsTestRippleOfEOL");
                modRun.ConfigData.AddBool("IsTestRippleOfEOL", isTestRippleOfEOL);

                if (isTestRippleOfEOL)
                {
                    modRun.ConfigData.AddBool("UseLowCostSolution", progInfo.MapParamsConfig.GetBoolParam("UseLowCostSolution"));
                    modRun.ConfigData.AddString("EOLPhaseScanDropFilePath", this.eolPhaseScanDropFilePath);
                    modRun.ConfigData.AddString("PhaseCurrentFile", progInfo.MapParamsConfig.GetStringParam("PhaseCurrentFile"));

                    RippleTestOperations.LoadConfigToModule(this._cocResult, modRun.ConfigData);
                }

                bool loggingEnabled = true;
                try
                {
                    loggingEnabled = this.progInfo.TestParamsConfig.GetBoolParam("LoggintEnabled");
                }
                catch (Exception /*e*/ ) { }
                modRun.ConfigData.AddBool("LoggingEnabled", loggingEnabled);
                modRun.ConfigData.AddBool("TraceToneTestEnabled", progInfo.TestParamsConfig.GetBoolParam("TraceToneTestEnabled"));
                modRun.ConfigData.AddSint32("MZSourceMeasureDelay_ms", progInfo.TestParamsConfig.GetIntParam("MZSourceMeasureDelay_ms"));
                modRun.ConfigData.AddBool("DO_GRandR", this.DO_GRandR);

                modRun.ConfigData.AddString("ResultDir", this.CommonTestConfig.ResultsSubDirectory);
                modRun.ConfigData.AddDouble("TC_MZ_MINIMA_V_LIMIT_MIN", Convert.ToDouble(progInfo.MainSpec.GetParamLimit("TC_MZ_MINIMA_V_LIMIT_MID").LowLimit.ValueToString()));
                modRun.ConfigData.AddReference("Specification", progInfo.MainSpec);

                // Tie up limits
                Specification spec = progInfo.MainSpec;

                if (spec.ParamLimitExists("LM_PHASE_SCAN_RIPPLE_HIGH"))
                {
                    modRun.Limits.AddParameter(spec, "LM_PHASE_SCAN_RIPPLE_HIGH", "LM_PHASE_SCAN_RIPPLE_HIGH");
                }

                if (spec.ParamLimitExists("LM_PHASE_SCAN_FOR_RIPPLE_FILE"))
                {
                    modRun.Limits.AddParameter(spec, "LM_PHASE_SCAN_FOR_RIPPLE_FILE", "LM_PHASE_SCAN_FOR_RIPPLE_FILE");
                }

                modRun.Limits.AddParameter(spec, "HIGH_DISS_OPTICAL_FREQ", "HIGH_DISS_OPTICAL_FREQ");
                modRun.Limits.AddParameter(spec, "HIGH_DISS_SOL", "HIGH_DISS_SOL");
                modRun.Limits.AddParameter(spec, "PACK_DISS_TCASE_HIGH", "PACK_DISS_TCASE_HIGH");
                modRun.Limits.AddParameter(spec, "TEC_I_TCASE_HIGH", "LASER_TEC_I_TCASE_HIGH");
                modRun.Limits.AddParameter(spec, "TEC_V_TCASE_HIGH", "LASER_TEC_V_TCASE_HIGH");
                //modRun.Limits.AddParameter(spec, "MZ_TEC_I_TCASE_HIGH", "MZ_TEC_I_TCASE_HIGH");
                //modRun.Limits.AddParameter(spec, "MZ_TEC_V_TCASE_HIGH", "MZ_TEC_V_TCASE_HIGH");
                modRun.Limits.AddParameter(spec, "TEC_I_TCASE_HIGH_EOL", "LASER_TEC_I_TCASE_HIGH_EOL");
                modRun.Limits.AddParameter(spec, "TEC_V_TCASE_HIGH_EOL", "LASER_TEC_V_TCASE_HIGH_EOL");
                //modRun.Limits.AddParameter(spec, "MZ_TEC_I_TCASE_HIGH_EOL", "MZ_TEC_I_TCASE_HIGH_EOL");
                //modRun.Limits.AddParameter(spec, "MZ_TEC_V_TCASE_HIGH_EOL", "MZ_TEC_V_TCASE_HIGH_EOL");
                modRun.Limits.AddParameter(spec, "PACK_DISS_TCASE_HIGH_EOL", "PACK_DISS_TCASE_HIGH_EOL");
                modRun.Limits.AddParameter(spec, "NUM_CHAN_TEST_TCASE_HIGH", "NumChannelsTested");
                modRun.Limits.AddParameter(spec, "NUM_CHAN_PASS_TCASE_HIGH", "NumChannelsPass");
                modRun.Limits.AddParameter(spec, "NUM_LOCK_FAIL_TCASE_HIGH", "NumLockFails");
                modRun.Limits.AddParameter(spec, "TCASE_HIGH_PASS_FAIL_FLAG", "PassFailFlag");
            }// by tim


        }

        /// <summary>
        /// Initialise TcmzTraceToneTest module
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        private void TcmzTraceToneTest_InitModule(ITestEngineInit engine, DUTObject dutObject)
        {
            // Initialise module
            ModuleRun modRun = engine.AddModuleRun("TcmzTraceToneTest", "TcmzTraceToneTest", "");
            modRun.ConfigData.AddString("DutSerialNumber", dutObject.SerialNumber);
            modRun.ConfigData.AddString("PlotFileDirectory", this.CommonTestConfig.ResultsSubDirectory);

            // Tie up limits when added to spec
            modRun.Limits.AddParameter(progInfo.MainSpec, "I_SOA_1", "I_Soa_1");
            modRun.Limits.AddParameter(progInfo.MainSpec, "MAX_I_SOA_TRACE_SOLOT", "Max_I_Soa_Trace_SOLOT");
            modRun.Limits.AddParameter(progInfo.MainSpec, "MAX_TRACE_I_SOLOT", "Max_Trace_I_SOLOT");
            modRun.Limits.AddParameter(progInfo.MainSpec, "SOA_GRADIENT_SOLOT", "Soa_Gradient_SOLOT");
            modRun.Limits.AddParameter(progInfo.MainSpec, "I_SOA_TRACE_EOLOT1", "I_Soa_Trace_EOLOT1");
            modRun.Limits.AddParameter(progInfo.MainSpec, "TRACE_I_EOLOT1", "Trace_I_EOLOT1");
            modRun.Limits.AddParameter(progInfo.MainSpec, "SOA_GRADIENT_EOLOT1", "Soa_Gradient_EOLOT1");
            modRun.Limits.AddParameter(progInfo.MainSpec, "I_SOA_TRACE_EOLOT2", "I_Soa_Trace_EOLOT2");
            modRun.Limits.AddParameter(progInfo.MainSpec, "TRACE_I_EOLOT2", "Trace_I_EOLOT2");
            modRun.Limits.AddParameter(progInfo.MainSpec, "SOA_GRADIENT_EOLOT2", "Soa_Gradient_EOLOT2");
            modRun.Limits.AddParameter(progInfo.MainSpec, "TRACE_SOA_PLOT", "TraceSoaCurves");
        }

        /// <summary>
        /// Initialise TcmzGroupResults module
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        private void IlmzGroupResults_InitModule(ITestEngineInit engine, DUTObject dutObject)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("IlmzGroupResults", "IlmzGroupResults", "");

            // Add config data
            modRun.ConfigData.AddString("RESULT_DIR", this.CommonTestConfig.ResultsSubDirectory);
            modRun.ConfigData.AddString("TIME_STAMP", this.TestTimeStamp_Start);
            modRun.ConfigData.AddString("LASER_ID", dutObject.SerialNumber);
            modRun.ConfigData.AddBool("LowTempTestEnabled", progInfo.TestParamsConfig.GetBoolParam("LowTempTestEnabled_OT"));
            modRun.ConfigData.AddReference("MainSpec", progInfo.MainSpec);

            // Tie up limits
            Specification spec = progInfo.MainSpec;
            //modRun.Limits.AddParameter(spec, "AVERAGE_LOCK_RATIO", "AVERAGE_LOCK_RATIO"); //by tim
            modRun.Limits.AddParameter(spec, "OPTICAL_FREQ_FIRST_CHAN_FAIL", "OPTICAL_FREQ_FIRST_CHAN_FAIL");
            modRun.Limits.AddParameter(spec, "FULL_PASS_CHAN_COUNT_OVERALL", "FULL_PASS_CHAN_COUNT_OVERALL");

            modRun.Limits.AddParameter(spec, "FIBRE_POWER_CHAN_TEMP_MIN", "FIBRE_POWER_CHAN_TEMP_MIN");
            modRun.Limits.AddParameter(spec, "FIBRE_POWER_CHAN_TEMP_MAX", "FIBRE_POWER_CHAN_TEMP_MAX");
            modRun.Limits.AddParameter(spec, "FIBRE_POWER_CHANGE_TCASE_HIGH", "FIBRE_POWER_CHANGE_TCASE_HIGH");
            modRun.Limits.AddParameter(spec, "FIBRE_POWER_FAIL_CHAN_TEMP", "FIBRE_POWER_FAIL_CHAN_TEMP");
            modRun.Limits.AddParameter(spec, "FIBRE_POWER_FAIL_OVER_TCASE", "FIBRE_POWER_FAIL_OVER_TCASE");
            //modRun.Limits.AddParameter(spec, "TRACK_ERROR_PWR_TCASE", "TRACK_ERROR_PWR_TCASE"); //by tim

            modRun.Limits.AddParameter(spec, "LOCK_FREQ_CHAN_TEMP_MIN", "LOCK_FREQ_CHAN_TEMP_MIN");
            modRun.Limits.AddParameter(spec, "LOCK_FREQ_CHAN_TEMP_MAX", "LOCK_FREQ_CHAN_TEMP_MAX");
            modRun.Limits.AddParameter(spec, "LOCK_FREQ_CHANGE_TCASE_HIGH", "LOCK_FREQ_CHANGE_TCASE_HIGH");
            modRun.Limits.AddParameter(spec, "LOCK_FREQ_FAIL_CHAN_TEMP", "LOCK_FREQ_FAIL_CHAN_TEMP");
            modRun.Limits.AddParameter(spec, "LOCK_FREQ_FAIL_OVER_TCASE", "LOCK_FREQ_FAIL_OVER_TCASE");
            //modRun.Limits.AddParameter(spec, "TRACK_ERROR_FREQ_TCASE", "TRACK_ERROR_FREQ_TCASE"); //by tim
            //modRun.Limits.AddParameter(spec, "TRACK_ERROR_ULFREQ_TCASE", "TRACK_ERROR_ULFREQ_TCASE"); //by tim

            modRun.Limits.AddParameter(spec, "PHASE_I_CHANGE_TCASE_HIGH", "PHASE_I_CHANGE_TCASE_HIGH");

            if (progInfo.TestParamsConfig.GetBoolParam("LowTempTestEnabled_OT"))
            {
                modRun.Limits.AddParameter(spec, "FIBRE_POWER_CHANGE_TCASE_LOW", "FIBRE_POWER_CHANGE_TCASE_LOW");
                modRun.Limits.AddParameter(spec, "LOCK_FREQ_CHANGE_TCASE_LOW", "LOCK_FREQ_CHANGE_TCASE_LOW");
                modRun.Limits.AddParameter(spec, "PHASE_I_CHANGE_TCASE_LOW", "PHASE_I_CHANGE_TCASE_LOW");
            }
        }

        #endregion

        #region Configure instruments

        /// <summary>
        /// Configure a TEC controller using config data
        /// </summary>
        /// <param name="tecCtlr">Instrument reference</param>
        /// <param name="tecCtlId">Config table data prefix</param>
        private void ConfigureTecController(ITestEngineBase engine, IInstType_TecController tecCtlr, string tecCtlId, bool isTecCase, double temperatureSetPoint_C)
        {
            SteinhartHartCoefficients stCoeffs = new SteinhartHartCoefficients();

            if (tecCtlr.DriverName.Contains("Ke2510"))
            {
                // Keithley 2510 specific commands (must be done before 'SetDefaultState')
                tecCtlr.HardwareData["MinTemperatureLimit_C"] = progInfo.TestParamsConfig.GetStringParam(tecCtlId + "_MinTemp_C");
                tecCtlr.HardwareData["MaxTemperatureLimit_C"] = progInfo.TestParamsConfig.GetStringParam(tecCtlId + "_MaxTemp_C");

                tecCtlr.SetDefaultState();

                tecCtlr.Sensor_Type = (InstType_TecController.SensorType)Enum.Parse(typeof(InstType_TecController.SensorType), progInfo.TestParamsConfig.GetStringParam(tecCtlId + "_Sensor_Type"));

                tecCtlr.TecVoltageCompliance_volt = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_TecVoltageCompliance_volt");

                if (isTecCase)
                {
                    tecCtlr.DerivativeGain = progInfo.LocalTestParamsConfig.GetDoubleParam(tecCtlId + "_DerivativeGain");
                }
                else
                {
                    tecCtlr.DerivativeGain = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_DerivativeGain");
                }
            }
            else
            {
                tecCtlr.SetDefaultState();
            }

            tecCtlr.OperatingMode = (InstType_TecController.ControlMode)Enum.Parse(typeof(InstType_TecController.ControlMode), progInfo.TestParamsConfig.GetStringParam(tecCtlId + "_OperatingMode"));

            // Thermistor characteristics
            if (tecCtlr.Sensor_Type == InstType_TecController.SensorType.Thermistor_2wire ||
                tecCtlr.Sensor_Type == InstType_TecController.SensorType.Thermistor_4wire)
            {
                stCoeffs.A = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_stCoeffs_A");
                stCoeffs.B = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_stCoeffs_B");
                stCoeffs.C = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_stCoeffs_C");
                tecCtlr.SteinhartHartConstants = stCoeffs;
            }

            if (isTecCase)
            {
                tecCtlr.TecCurrentCompliance_amp = progInfo.LocalTestParamsConfig.GetDoubleParam(tecCtlId + "_TecCurrentCompliance_amp");
                tecCtlr.ProportionalGain = progInfo.LocalTestParamsConfig.GetDoubleParam(tecCtlId + "_ProportionalGain");
                tecCtlr.IntegralGain = progInfo.LocalTestParamsConfig.GetDoubleParam(tecCtlId + "_IntegralGain");
            }
            else
            {
                tecCtlr.TecCurrentCompliance_amp = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_TecCurrentCompliance_amp");
                tecCtlr.ProportionalGain = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_ProportionalGain");
                tecCtlr.IntegralGain = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_IntegralGain");
            }

            try
            {
                tecCtlr.SensorTemperatureSetPoint_C = temperatureSetPoint_C;
            }
            catch (Exception e)
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, e.Message, FailModeCategory_Enum.UN);
            }
        }

        /// <summary>
        /// Configure FCU instruments
        /// </summary>
        /// <param name="fcuInstrs"></param>
        private void ConfigureFCUInstruments(FCUInstruments fcuInstrs)
        {
            fcuInstrs.FullbandControlUnit.ResetCPN();
            fcuInstrs.FullbandControlUnit.EnterProtectMode();
            fcuInstrs.FullbandControlUnit.EnterVendorProtectMode("THURSDAY"); // Need this privilege level
            // in order to use IOIFBkhm interface.
            // This password has to be used.
            // Config FCU calibration data
            progInfo.Instrs.Fcu.FCUCalibrationData = new FCUInstruments.FCUCalData();
            string fcuSerialNumber = fcuInstrs.FullbandControlUnit.GetUnitSerialNumber().Trim();
            double CalFactor;
            double CalOffset;
            // Tx ADC
            readCalData("FCU2Asic", fcuSerialNumber, "Tx ADC", out CalFactor, out CalOffset);
            progInfo.Instrs.Fcu2AsicInstrsGroups.FCUCalibrationData.TxADC_CalFactor = CalFactor;
            progInfo.Instrs.Fcu2AsicInstrsGroups.FCUCalibrationData.TxADC_CalOffset = CalOffset;
            // Rx ADC
            readCalData("FCU2Asic", fcuSerialNumber, "Rx ADC", out CalFactor, out CalOffset);
            progInfo.Instrs.Fcu2AsicInstrsGroups.FCUCalibrationData.RxADC_CalFactor = CalFactor;
            progInfo.Instrs.Fcu2AsicInstrsGroups.FCUCalibrationData.RxADC_CalOffset = CalOffset;
            // TxCoarsePot
            readCalData("FCU2Asic", fcuSerialNumber, "TxCoarsePot", out CalFactor, out CalOffset);
            progInfo.Instrs.Fcu2AsicInstrsGroups.FCUCalibrationData.TxCoarsePot_CalFactor = CalFactor;
            progInfo.Instrs.Fcu2AsicInstrsGroups.FCUCalibrationData.TxCoarsePot_CalOffset = CalOffset;
            // TxFinePot
            readCalData("FCU2Asic", fcuSerialNumber, "TxFinePot", out CalFactor, out CalOffset);
            progInfo.Instrs.Fcu2AsicInstrsGroups.FCUCalibrationData.TxFinePot_CalFactor = CalFactor;
            progInfo.Instrs.Fcu2AsicInstrsGroups.FCUCalibrationData.TxFinePot_CalOffset = CalOffset;
            // Fix ADC
            readCalData("FCU2Asic", fcuSerialNumber, "Fix ADC", out CalFactor, out CalOffset);
            progInfo.Instrs.Fcu2AsicInstrsGroups.FCUCalibrationData.FixADC_CalFactor = CalFactor;
            progInfo.Instrs.Fcu2AsicInstrsGroups.FCUCalibrationData.FixADC_CalOffset = CalOffset;

            // ThermistorResistance
            readCalData("FCU2Asic", fcuSerialNumber, "ThermistorResistance", out CalFactor, out CalOffset);
            progInfo.Instrs.Fcu2AsicInstrsGroups.FCUCalibrationData.ThermistorResistance_CalFactor = CalFactor;
            progInfo.Instrs.Fcu2AsicInstrsGroups.FCUCalibrationData.ThermistorResistance_CalOffset = CalOffset;
        }

        // added FCU2ASIC Option by Alice.Huang, for TOSA GB test     2010-02-01
        /// <summary>
        /// initialise FCU2ASIC INSTRUMENTS 
        /// </summary>
        /// <param name="fcu2AsicInstrs">FCU2ASIC instruments group </param>
        private void ConfigureFCU2AsicInstruments(FCU2AsicInstruments fcu2AsicInstrs)
        {
            double curMax = progInfo.LocalTestParamsConfig.GetDoubleParam("FcuSource_CurrentCompliance_A");
            if (fcu2AsicInstrs.FcuSource != null)
            {
                // Set to voltage source mode & set current compliance
                fcu2AsicInstrs.FcuSource.SetDefaultState();
                fcu2AsicInstrs.FcuSource.VoltageSetPoint_Volt = progInfo.LocalTestParamsConfig.GetDoubleParam("FcuSource_Voltage_V");
                fcu2AsicInstrs.FcuSource.CurrentComplianceSetPoint_Amp = curMax;
                fcu2AsicInstrs.FcuSource.OutputEnabled = true;
                System.Threading.Thread.Sleep(5000);
            }

            fcu2AsicInstrs.AsicVccSource.SetDefaultState();
            curMax = progInfo.LocalTestParamsConfig.GetDoubleParam("AsicVccSource_CurrentCompliance_A");

            // Set to voltage source mode & set current compliance
            fcu2AsicInstrs.AsicVccSource.VoltageSetPoint_Volt = progInfo.LocalTestParamsConfig.GetDoubleParam("AsicVccSource_Voltage_V");
            fcu2AsicInstrs.AsicVccSource.CurrentComplianceSetPoint_Amp = curMax;
            fcu2AsicInstrs.AsicVccSource.OutputEnabled = true;

            fcu2AsicInstrs.AsicVeeSource.SetDefaultState();
            curMax = progInfo.LocalTestParamsConfig.GetDoubleParam("AsicVeeSource_CurrentCompliance_A");
            // Set to voltage source mode & set current compliance  
            fcu2AsicInstrs.AsicVeeSource.VoltageSetPoint_Volt =
                        progInfo.LocalTestParamsConfig.GetDoubleParam("AsicVeeSource_Voltage_V");
            fcu2AsicInstrs.AsicVeeSource.CurrentComplianceSetPoint_Amp = curMax;
            fcu2AsicInstrs.AsicVeeSource.OutputEnabled = true;
            //load etalon wavemeter cal data from file

            string FrequencyCalibration_file_Path = "L_band_FrequencyCalibration_file_Path";
            string str = progInfo.TestConditions.FreqBand.ToString();
            if (str == "C")
                FrequencyCalibration_file_Path = "C_band_FrequencyCalibration_file_Path";

            string freqCalFilename = progInfo.MapParamsConfig.GetStringParam(FrequencyCalibration_file_Path);
            string FCUMKI_Tx_CalFilename = progInfo.MapParamsConfig.GetStringParam("FCUMKI_Tx_Calibration_file_Path");
            string FCUMKI_Rx_CalFilename = progInfo.MapParamsConfig.GetStringParam("FCUMKI_Rx_Calibration_file_Path");
            string FCUMKI_Var_CalFilename = progInfo.MapParamsConfig.GetStringParam("FCUMKI_Var_Calibration_file_Path");
            string FCUMKI_Fix_CalFilename = progInfo.MapParamsConfig.GetStringParam("FCUMKI_Fix_Calibration_file_Path");

            FreqCalByEtalon.LoadFreqlCalArray(freqCalFilename, 0.0, progInfo.TestConditions.FreqTolerance_GHz, progInfo.TestConditions.ItuSpacing_GHz);
            FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Tx = FreqCalByEtalon.LoadFCUMKICalArray(FCUMKI_Tx_CalFilename);
            FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Rx = FreqCalByEtalon.LoadFCUMKICalArray(FCUMKI_Rx_CalFilename);
            FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Var = FreqCalByEtalon.LoadFCUMKICalArray(FCUMKI_Var_CalFilename);
            FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Fix = FreqCalByEtalon.LoadFCUMKICalArray(FCUMKI_Fix_CalFilename);
        }

        /// <summary>
        /// Configure a MZ bias source
        /// </summary>
        /// <param name="biasSrc">Instrument reference</param>
        /// <param name="biasSrcId">Config table data prefix</param>
        private void ConfigureMzBiasSource(IInstType_ElectricalSource biasSrc, string biasSrcId)
        {
            // Keithley 2400 specific commands (must be done before 'SetDefaultState')
            if (biasSrc.DriverName.Contains("Ke24xx"))
            {
                biasSrc.HardwareData["4wireSense"] = "true";
            }

            // Configure instrument
            biasSrc.SetDefaultState();
            // Set to current source mode & set compliance
            biasSrc.CurrentSetPoint_amp = 0.0;
            biasSrc.CurrentComplianceSetPoint_Amp = progInfo.TestParamsConfig.GetDoubleParam(biasSrcId + "_CurrentCompliance_A");
            // Set to voltage source mode & set compliance
            biasSrc.VoltageSetPoint_Volt = 0.0;
            biasSrc.VoltageComplianceSetPoint_Volt = progInfo.TestParamsConfig.GetDoubleParam(biasSrcId + "_VoltageCompliance_volt");
        }

        #endregion

        #region Program Running

        public override void Run(ITestEngineRun engine, DUTObject dutObject, InstrumentCollection instrs, ChassisCollection chassis)
        {
            InstCommands.CloseInstToLoadDUT(engine, progInfo.Instrs.Fcu2AsicInstrsGroups.FcuSource, progInfo.Instrs.Fcu2AsicInstrsGroups.AsicVccSource, progInfo.Instrs.Fcu2AsicInstrsGroups.AsicVeeSource, progInfo.Instrs.TecDsdbr);


            ///VerifyJigTemperauture(engine);

            progInfo.Instrs.Mz.PowerMeter.ZeroDarkCurrent_Start();

            //engine.ShowContinueUserQuery("Please load the DUT and click Continue to test\n\n请放置器件，点击 Continue 测试");

            #region Pin Check -- chongjian.liang - 2012.9.13

            engine.RunModule("Mod_PinCheck");

            ModRunData Pin_Check_Results = engine.GetModuleRunData("Mod_PinCheck");

            bool isPinCheckPass = Pin_Check_Results.ModuleData.ReadBool("Pin_Check_Pass");

            if (!isPinCheckPass)
            {
                this.errorInformation += Pin_Check_Results.ModuleData.ReadString("ErrorInformation");
                return;
            }
            #endregion

            ModuleRun modRun;

            this.numberOfSupermodes = _mapResult.ReadSint32("NUM_SM");

            // Initialise 
            // engine.RunModule("IlmzInitialise");

            // Set switches for the filter box
            if (!engine.IsSimulation && progInfo.DsdbrInstrsUsed == DsdbrDriveInstruments.PXIInstruments)
            {
                switch (progInfo.TestConditions.FreqBand)
                {
                    case FreqBand.C:
                        progInfo.Instrs.SwitchPathManager.SetToPosn("FILTERBOX_C");
                        break;
                    case FreqBand.L:
                        progInfo.Instrs.SwitchPathManager.SetToPosn("FILTERBOX_L");
                        break;
                    default:
                        this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "Invalid frequency band: " + progInfo.TestConditions.FreqBand, FailModeCategory_Enum.UN);
                        break;
                }
            }

            //close the instrs ,becase we need relod device
            InstCommands.CloseInstToLoadDUT(engine, progInfo.Instrs.Fcu2AsicInstrsGroups.FcuSource, progInfo.Instrs.Fcu2AsicInstrsGroups.AsicVccSource, progInfo.Instrs.Fcu2AsicInstrsGroups.AsicVeeSource, progInfo.Instrs.TecDsdbr);

            ////Verify Jig Temperauture first
            VerifyJigTemperauture(engine);


            #region[waitting for 35C]
            //Thread.Sleep(60000);
            #endregion
            InstCommands.OpenInstToLoadDUT(engine, progInfo.Instrs.Fcu2AsicInstrsGroups.FcuSource, progInfo.Instrs.Fcu2AsicInstrsGroups.AsicVccSource, progInfo.Instrs.Fcu2AsicInstrsGroups.AsicVeeSource, progInfo.Instrs.TecDsdbr);

            // Asic Part
            Thread.Sleep(1000);
            progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.SetupChannelCalibration();

            progInstrs.Fcu2AsicInstrsGroups.Fcu2Asic.Inital_Etalon_WaveMter();//to avoid Var pot large change in overallmap FCUMKI pot leveling Jack.Zhang 2011-01-12


            

            // Set temperatures
            //  pre set the case temp to save some time
            progInfo.Instrs.TecCase.SensorTemperatureSetPoint_C = progInfo.TestConditions.MidTemp;
            // TODO set MZ & DSDBR according to FF data
            progInfo.Instrs.TecCase.OutputEnabled = true;

            #region Temperature control - chongjian.liang 2012.12.3

            // Set and wait for temperatures to stabilise

            ModuleRunReturn caseControlResult = engine.RunModule("CaseTemp_Mid");

            if (caseControlResult.ModuleRunData.ModuleData.IsPresent("ErrorInformation")) {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, caseControlResult.ModuleRunData.ModuleData.ReadString("ErrorInformation"), FailModeCategory_Enum.UN);
            }

            ModuleRunReturn dsdbrControlResult = engine.RunModule("DsdbrTemperature");

            if (dsdbrControlResult.ModuleRunData.ModuleData.IsPresent("ErrorInformation")) {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, dsdbrControlResult.ModuleRunData.ModuleData.ReadString("ErrorInformation"), FailModeCategory_Enum.UN);
            }
            #endregion

            // Power up
            RunIlmzPowerUp(engine);

            // Measure of cross-calibration of frequency between map stage and final stage
            RunCrossCalibrationMeasure(engine);

            // Power head calibration
            RunIlmzPowerHeadCal(engine);//Echo remed for debug,

            IlmzItuChannels = BuildMidTempTestResult();

            #region Setup OT test channels

            IlmzChannels.ExtremeChannelIndexes extremeChans = IlmzItuChannels.GetItuExtremeChannels(IlmzItuChannels.AllOptions);
            extremeChansSetup = extremeChans;

            int eolScanBoundLeft = 0;
            int eolScanBoundRight = 0;

            List<int> extremeChansOfRipple = null;

            if (progInfo.TestParamsConfig.GetBoolParam("IsAddRippleMaxForOT"))
            {
                extremeChansOfRipple = GetChanOfRippleMax(engine, out eolScanBoundLeft, out eolScanBoundRight);
            }

            List<int> extremeChansOfIRear = null;

            if (progInfo.TestParamsConfig.GetBoolParam("IsAddRearMaxPerSMForOT"))
            {
                extremeChansOfIRear = GetChansOfRearMaxPerSM(extremeChans, extremeChansOfRipple);
            } 
            #endregion

            //IlmzItuChannels = (IlmzChannels)ituEstRunReturn.ModuleRunData.ModuleData.ReadReference("TcmzItuChannels");

            //Channel Characterise
            int breakPowerLevelingCount = progInfo.TestParamsConfig.GetIntParam("BreakPowerLevelingCount");
            modRun = engine.GetModuleRun("IlmzChannelChar");
            modRun.ConfigData.AddReference("IlmzItuChannels", IlmzItuChannels);
            modRun.ConfigData.AddReference("ExtremeChannels", extremeChans);
            modRun.ConfigData.AddSint32("EOLScanBoundLeft", eolScanBoundLeft);
            modRun.ConfigData.AddSint32("EOLScanBoundRight", eolScanBoundRight);
            modRun.ConfigData.AddUint32("BreakPowerLevelingCount", breakPowerLevelingCount);
            if (extremeChansOfIRear != null)
            {
                modRun.ConfigData.AddReference("ExtremeChansOfIRear", extremeChansOfIRear);
            }
            if (extremeChansOfRipple != null)
            {
                modRun.ConfigData.AddReference("ExtremeChansOfRipple", extremeChansOfRipple);
            }

            ModuleRunReturn chanCharRunReturn = null;

            chanCharRunReturn = engine.RunModule("IlmzChannelChar");

            //SelectTestFlag = chanCharRunReturn.ModuleRunData.ModuleData.ReadBool("SelectTestFlag");
            //if ((chanCharRunReturn != null ) &&(chanCharRunReturn.ModuleRunData.ModuleData.ReadSint32("FailAbortFlag") == 1))

            bool isExit = false;

            List<bool> passFlags_LastModule = null;

            channelsIndexes_TestOT = null;

            if (progInfo.TestParamsConfig.GetBoolParam("LowTempTestEnabled_OT"))
            {
                if (IlmzItuChannels.AllOptions.Length > 0)
                {
                    // Set Case temperature to low 
                    engine.RunModule("CaseTemp_Low");
                    // Low temperature test 
                    modRun = engine.GetModuleRun("IlmzLowTempTest");
                    modRun.ConfigData.AddReference("TcmzItuChannels", IlmzItuChannels);
                    modRun.ConfigData.AddReference("ExtremeChannels", extremeChans);
                    modRun.ConfigData.AddSint32("EOLScanBoundLeft", eolScanBoundLeft);
                    modRun.ConfigData.AddSint32("EOLScanBoundRight", eolScanBoundRight);
                    if (extremeChansOfIRear != null)
                    {
                        modRun.ConfigData.AddReference("ExtremeChansOfIRear", extremeChansOfIRear);
                    }
                    if (extremeChansOfRipple != null)
                    {
                        modRun.ConfigData.AddReference("ExtremeChansOfRipple", extremeChansOfRipple);
                    } 

                    // Record the pass flags of all channels from last module result - chongjian.liang 2012.12.26
                    passFlags_LastModule = new List<bool>(IlmzItuChannels.AllOptions.Length);
                    for (int index = 0; index < IlmzItuChannels.AllOptions.Length; index++)
                    {
                        passFlags_LastModule.Add(IlmzItuChannels.AllOptions[index].MidTempData.GetValueBool(EnumTosaParam.Pass_Flag));
                    }
                    modRun.ConfigData.AddReference("PassFlags_LastModule", passFlags_LastModule);

                    ModuleRunReturn lowTempTestResult = engine.RunModule("IlmzLowTempTest");

                    channelsIndexes_TestOT = lowTempTestResult.ModuleRunData.ModuleData.ReadReference("ChannelsIndexes_TestOT") as List<int>;
                }
            }

            if (progInfo.TestParamsConfig.GetBoolParam("HighTempTestEnabled_OT"))
            {
                // High case temperature tests
                if (IlmzItuChannels.AllOptions.Length > 0)
                {
                    // Set Case temperature to high 
                    engine.RunModule("CaseTemp_High");

                    // High temperature test 
                    modRun = engine.GetModuleRun("IlmzHighTempTest");
                    modRun.ConfigData.AddReference("TcmzItuChannels", IlmzItuChannels);
                    modRun.ConfigData.AddReference("ExtremeChannels", extremeChans);
                    modRun.ConfigData.AddSint32("EOLScanBoundLeft", eolScanBoundLeft);
                    modRun.ConfigData.AddSint32("EOLScanBoundRight", eolScanBoundRight);
                    if (extremeChansOfIRear != null)
                    {
                        modRun.ConfigData.AddReference("ExtremeChansOfIRear", extremeChansOfIRear);
                    }
                    if (extremeChansOfRipple != null)
                    {
                        modRun.ConfigData.AddReference("ExtremeChansOfRipple", extremeChansOfRipple);
                    } 

                    // Record the pass flags of all channels from last module result - chongjian.liang 2012.12.26
                    if (passFlags_LastModule == null)
                    {
                        passFlags_LastModule = new List<bool>(IlmzItuChannels.AllOptions.Length);
                        for (int index = 0; index < IlmzItuChannels.AllOptions.Length; index++)
                        {
                            passFlags_LastModule.Add(IlmzItuChannels.AllOptions[index].MidTempData.GetValueBool(EnumTosaParam.Pass_Flag));
                        }
                        modRun.ConfigData.AddReference("PassFlags_LastModule", passFlags_LastModule);
                    }
                    else
                    {
                        for (int index = 0; index < IlmzItuChannels.AllOptions.Length; index++)
                        {
                            passFlags_LastModule[index] = IlmzItuChannels.AllOptions[index].MidTempData.GetValueBool(EnumTosaParam.Pass_Flag);
                        }
                        modRun.ConfigData.AddReference("PassFlags_LastModule", passFlags_LastModule);
                    }

                    ModuleRunReturn highTempTestResult = engine.RunModule("IlmzHighTempTest");

                    List<int> channelsIndexes_HighTemp = highTempTestResult.ModuleRunData.ModuleData.ReadReference("ChannelsIndexes_TestOT") as List<int>;

                    foreach (int channelIndex_HighTemp in channelsIndexes_HighTemp)
                    {
                        if (!channelsIndexes_TestOT.Contains(channelIndex_HighTemp))
                        {
                            channelsIndexes_TestOT.Add(channelIndex_HighTemp);
                        }
                    }
                }
            }

            // Send the case temperature back to ambient whilst we clean up.
            progInfo.Instrs.TecCase.SensorTemperatureSetPoint_C = 25;

            // Trace tone test - make sure over-temp test were done!
            if (progInfo.TestParamsConfig.GetBoolParam("TraceToneTestEnabled"))
            {
                // One-off measurements on individual channels
                engine.RunModule("TcmzTraceToneTest");
            }

            // Consolodate results
            modRun = engine.GetModuleRun("IlmzGroupResults");
            modRun.ConfigData.AddReference("TcmzItuChannels", IlmzItuChannels);
            engine.RunModule("IlmzGroupResults");

            // Set device to safe case temperature                 
            engine.RunModule("CaseTemp_Safe");
        }

        /// <summary>
        /// Run IlmzPowerHeadCal module
        /// </summary>
        /// <param name="engine"></param>
        private void RunIlmzPowerHeadCal(ITestEngineRun engine)
        {
            ModuleRun modRun;
            ButtonId skipPowerCal = ButtonId.No;
            if (progInfo.TestParamsConfig.GetBoolParam("AllowSkipPowerCalibration"))
            {
                skipPowerCal = engine.ShowYesNoUserQuery("Do you want to skip power calibration?");
            }
            if (skipPowerCal == ButtonId.Yes)
            {
                GuiMsgs.TcmzPowerCalDataResponse resp;
                do
                {
                    engine.ShowContinueUserQuery("Select Power Calibration Data file");
                    engine.GuiShow();
                    engine.GuiToFront();
                    engine.SendToGui(new GuiMsgs.TcmzPowerCalDataRequest());
                    resp = (GuiMsgs.TcmzPowerCalDataResponse)engine.ReceiveFromGui().Payload;
                    if (!File.Exists(resp.Filename))
                    {
                        engine.ShowContinueUserQuery("Power Calibration Data file doesn't exist, please try again!");
                    }
                } while (!File.Exists(resp.Filename));

                OpticalPowerCal.Initialise(2);
                using (CsvReader cr = new CsvReader())
                {
                    List<string[]> contents = cr.ReadFile(resp.Filename);
                    for (int ii = 1; ii < contents.Count; ii++)
                    {
                        OpticalPowerCal.AddCalibratedPoint(OpticalPowerHead.MZHead, double.Parse(contents[ii][0]), double.Parse(contents[ii][1]) - double.Parse(contents[ii][2]));
                        if (progInfo.DsdbrInstrsUsed == DsdbrDriveInstruments.PXIInstruments)
                        {
                            OpticalPowerCal.AddCalibratedPoint(OpticalPowerHead.OpmCgDirect, double.Parse(contents[ii][0]), double.Parse(contents[ii][1]) - double.Parse(contents[ii][3]));
                            OpticalPowerCal.AddCalibratedPoint(OpticalPowerHead.OpmCgFiltered, double.Parse(contents[ii][0]), double.Parse(contents[ii][1]) - double.Parse(contents[ii][4]));
                        }
                    }
                }
            }
            else
            {
                modRun = engine.GetModuleRun("IlmzPowerHeadCal");
                modRun.ConfigData.AddReference("CloseGridCharData", this.closeGridCharData);
                modRun.ConfigData.AddSint32("NUM_SM", numberOfSupermodes);
                ModuleRunReturn moduleRunReturn = engine.RunModule("IlmzPowerHeadCal");
                labourTime += moduleRunReturn.ModuleRunData.ModuleData.GetDatumDouble("LabourTime").Value;

                if (!moduleRunReturn.ModuleRunData.ModuleData.ReadBool("IsSuccessful") &&
                    !progInfo.TestParamsConfig.GetBoolParam("IsProceedWhenPowerCaliFailed"))
                {
                    // Raise error to end the test and write error to pcas result - chongjian.liang 2013.11.25
                    this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "Power calibration failed. Please clean the fiber head, or ask the technician to recalibrate the power if fiber head is OK.", FailModeCategory_Enum.OP);
                }
            }
        }

        /// <summary>
        /// Run IlmzPowerUp module
        /// </summary>
        /// <param name="engine"></param>
        private void RunIlmzPowerUp(ITestEngineRun engine)
        {
            ModuleRun modRun;
            modRun = engine.GetModuleRun("IlmzPowerUp");
            IlmzChannelInit chanInit = new IlmzChannelInit();

            // MZ data is from TcmzInitMzSweep
            chanInit.Mz.LeftArmImb_mA = this._mapResult.ReadDouble("REF_MZ_IMB_LEFT");
            chanInit.Mz.RightArmImb_mA = this._mapResult.ReadDouble("REF_MZ_IMB_RIGHT");
            chanInit.Mz.LeftArmMod_Min_V = 0;// this._mapResult.ReadDouble("REF_MZ_VOFF_LEFT"); //Jack.Zhang need to Get from Map PACS
            chanInit.Mz.RightArmMod_Min_V = 0;// this._mapResult.ReadDouble("REF_MZ_VOFF_RIGHT");
            chanInit.Mz.LeftArmMod_Peak_V = 0;// this._mapResult.ReadDouble("REF_MZ_VON_LEFT");
            chanInit.Mz.RightArmMod_Peak_V = 0;// this._mapResult.ReadDouble("REF_MZ_VON_RIGHT");

            if (this._mapResult.IsPresent("REF_MZ_VOFF_LEFT"))
            {
                chanInit.Mz.LeftArmMod_Min_V = this._mapResult.ReadDouble("REF_MZ_VOFF_LEFT"); //Jack.Zhang need to Get from Map PACS
            }
            if (this._mapResult.IsPresent("REF_MZ_VOFF_RIGHT"))
            {
                chanInit.Mz.RightArmMod_Min_V = this._mapResult.ReadDouble("REF_MZ_VOFF_RIGHT");
            }
            if (this._mapResult.IsPresent("REF_MZ_VON_LEFT"))
            {
                chanInit.Mz.LeftArmMod_Peak_V = this._mapResult.ReadDouble("REF_MZ_VON_LEFT");
            }
            if (this._mapResult.IsPresent("REF_MZ_VON_RIGHT"))
            {
                chanInit.Mz.RightArmMod_Peak_V = this._mapResult.ReadDouble("REF_MZ_VON_RIGHT");
            }

            // DSDBR data
            double testFreq_GHz = 193700;

            //Add for "L" band by Tim at 2009-2-13.
            if (progInfo.TestConditions.FreqBand == FreqBand.L)
                testFreq_GHz = 189000;
            foreach (DsdbrChannelData chanData in this.closeGridCharData)
            {
                if (Math.Abs(chanData.ItuFreq_GHz - testFreq_GHz) <= 20)
                {
                    chanInit.Dsdbr = chanData;
                    break;
                }
            }

            // switch the fcu cal data to pot1 to measure lock current

            if (!modRun.ConfigData.IsPresent("IlmzSettings")) modRun.ConfigData.AddReference("IlmzSettings", chanInit);
            ModuleRunReturn tcmzPowerUpRtn = engine.RunModule("IlmzPowerUp");
            if (tcmzPowerUpRtn.ModuleRunData.ModuleData.ReadBool("ErrorExists") == true)
            {
                // Raise error to end the test and write error to pcas result - chongjian.liang 2013.11.19
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, tcmzPowerUpRtn.ModuleRunData.ModuleData.ReadString("ErrorString"), FailModeCategory_Enum.UN);
            }
        }

        /// <summary>
        /// Run IlmzCrossCalibration module
        /// </summary>
        /// <param name="engine"></param>
        private void RunCrossCalibrationMeasure(ITestEngineRun engine)
        {
            ModuleRun modRun;
            modRun = engine.GetModuleRun("IlmzCrossCalibration");
            if (this.closeGridCharData.Length < 3)
            {
                engine.RaiseNonParamFail(1, "Frequency channel less than 3 channels, pls check ITU operation point file!");
            }

            // Get DsdbrChannelData for frequency cross-calibration
            int[] chanIndices = new int[3];
            chanIndices[0] = 0; // low channel
            chanIndices[1] = (progInfo.TestConditions.NbrChannels - 1) / 2; // mid channel 45
            chanIndices[2] = progInfo.TestConditions.NbrChannels - 1; // high channel

            if (this.closeGridCharData[closeGridCharData.Length - 1].ItuChannelIndex < chanIndices[2])
            {
                chanIndices[2] = closeGridCharData[closeGridCharData.Length - 1].ItuChannelIndex;
                chanIndices[1] = (chanIndices[0] + chanIndices[2]) / 2;
            }


            DsdbrChannelData[] dsdbrChansData = new DsdbrChannelData[3];
            for (int ii = 0; ii < chanIndices.Length; ii++)
            {
                foreach (DsdbrChannelData chanData in this.closeGridCharData)
                {
                    if (chanData.ItuChannelIndex == chanIndices[ii])
                    {
                        dsdbrChansData[ii] = chanData;
                        break;
                    }
                }
            }

            // Add config data to test module
            modRun.ConfigData.AddReference("DsdbrChannels", dsdbrChansData);
            // Run test module
            engine.RunModule("IlmzCrossCalibration");
        }

        #endregion

        #region End of Program
        /// <summary>
        /// reset instrument after dut test
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        /// <param name="instrs"></param>
        /// <param name="chassis"></param>
        public override void PostRun(ITestEnginePostRun engine, DUTObject dutObject,
                    InstrumentCollection instrs, ChassisCollection chassis)
        {
            // Display temperature progress
            engine.GuiShow();
            engine.GuiToFront();

            // Cleanup & power off
            if (!engine.IsSimulation)
            {
                TestProgramCore.CloseInstrs_Wavemeter();

                if (progInfo.DsdbrInstrsUsed == DsdbrDriveInstruments.FCU2AsicInstrument)
                {
                    InstCommands.CloseInstToUnLoadDUT(engine, progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic, progInfo.Instrs.Fcu2AsicInstrsGroups.FcuSource, progInfo.Instrs.Fcu2AsicInstrsGroups.AsicVccSource, progInfo.Instrs.Fcu2AsicInstrsGroups.AsicVeeSource, progInfo.Instrs.TecDsdbr, progInfo.Instrs.TecCase, 25);
                }

                #region Else(Not used)
                else if (progInfo.DsdbrInstrsUsed == DsdbrDriveInstruments.FCUInstruments)
                {
                    // modify for replacing FCU 2400 steven.cui
                    // Shutdown FCU
                    // Call methods on the FCU to switch off the laser.
                    progInfo.Instrs.Fcu.SoaCurrentSource.OutputEnabled = false;

                    progInfo.Instrs.Fcu.FullbandControlUnit.SetIVSource((float)0.0, IVSourceIndexNumbers.Rear);
                    progInfo.Instrs.Fcu.FullbandControlUnit.SetIVSource((float)0.0, IVSourceIndexNumbers.Phase);
                    progInfo.Instrs.Fcu.FullbandControlUnit.SetIVSource((float)0.0, IVSourceIndexNumbers.SOA);
                    progInfo.Instrs.Fcu.FullbandControlUnit.SetIVSource((float)0.0, IVSourceIndexNumbers.Gain);
                    progInfo.Instrs.Fcu.FullbandControlUnit.SetControlRegLaserPSUOn(false);
                    progInfo.Instrs.Fcu.FullbandControlUnit.SetControlRegLaserTecOn(false);

                    progInfo.Instrs.Fcu.FullbandControlUnit.SetControlRegLaserPSUOn(false);
                    TxCommand cmd1 = new TxCommand(0, 0, 0); // TunableModules command
                    cmd1 = progInfo.Instrs.Fcu.FullbandControlUnit.ReadTxCommand();
                    TxCommand cmd2 = new TxCommand(cmd1.Data1, cmd1.Data2,
                        (byte)(cmd1.Data3 | (byte)0x01)); // set bit0 of byte "3"
                    progInfo.Instrs.Fcu.FullbandControlUnit.SetTxCommand(cmd2); // Bit 'LSENABLE' is set for disabling it.
                    progInfo.Instrs.Fcu.FullbandControlUnit.OIFLaserEnable(false);
                    //progInfo.Instrs.Fcu.FCU_Source.OutputEnabled = false;

                    InstCommands.CloseTecsWithSafeTemperature(engine, progInfo.Instrs.TecDsdbr, progInfo.Instrs.TecCase, 25);
                }
                #endregion
                
                #region light_tower

                if (progInfo.lightTowerConfig.GetBoolParam("LightExist"))
                {
                    try
                    {

                        _serialPort.Close();
                        if (!_serialPort.IsOpen)
                        {
                            _serialPort.Open();
                        }

                        // Add the light tower color.
                        _serialPort.WriteLine(progInfo.lightTowerConfig.GetStringParam("LightYellow"));
                        _serialPort.Close();
                    }
                    catch { }
                }
                #endregion light_tower
            }
        }


        /// <summary>
        /// Write Dut test result
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        /// <param name="dutOutcome"></param>
        /// <param name="results"></param>
        /// <param name="userList"></param>
        public override void DataWrite(ITestEngineDataWrite engine, DUTObject dutObject,
                        DUTOutcome dutOutcome, TestData results, UserList userList)
        {
            //DatumList tcTraceData = new DatumList();
            //generate keys for main spec

            StringDictionary finalKeys = new StringDictionary();
            finalKeys.Add("SCHEMA", "HIBERDB");
            finalKeys.Add("DEVICE_TYPE", dutObject.PartCode);
            finalKeys.Add("SERIAL_NO", dutObject.SerialNumber);
            finalKeys.Add("SPECIFICATION", progInfo.
                MainSpec.Name);
            finalKeys.Add("TEST_STAGE", "final_ot");

            this.CopyDataFromMap(this._mapResult, engine);

            //Data_Ignore_Write(engine);


            //XIAOJIANG 2017.03.20
            if (progInfo.MainSpec.ParamLimitExists("LOT_TYPE"))
                if (dutObject.Attributes.IsPresent("lot_type"))
                    DatumListAddOrUpdate(ref tcTraceData, "LOT_TYPE", dutObject.Attributes.ReadString("lot_type"));
                    //tcTraceData.AddString("LOT_TYPE", dutObject.Attributes.ReadString("lot_type"));
                else
                    DatumListAddOrUpdate(ref tcTraceData, "LOT_TYPE", "Unknown");
                    //tcTraceData.AddString("LOT_TYPE", "Unknown");

            if (progInfo.MainSpec.ParamLimitExists("FACTORY_WORKS_LOTTYPE"))
                if (dutObject.Attributes.IsPresent("lot_type"))
                    DatumListAddOrUpdate(ref tcTraceData, "FACTORY_WORKS_LOTTYPE", dutObject.Attributes.ReadString("lot_type"));
                    //tcTraceData.AddString("FACTORY_WORKS_LOTTYPE", dutObject.Attributes.ReadString("lot_type"));
                else
                    DatumListAddOrUpdate(ref tcTraceData, "FACTORY_WORKS_LOTTYPE", "Unknown");
                    //tcTraceData.AddString("FACTORY_WORKS_LOTTYPE", "Unknown");

            if (progInfo.MainSpec.ParamLimitExists("LOT_ID"))
                DatumListAddOrUpdate(ref tcTraceData, "LOT_ID", dutObject.BatchID);
                //tcTraceData.AddString("LOT_ID", dutObject.BatchID);
            if (progInfo.MainSpec.ParamLimitExists("FACTORY_WORKS_LOTID"))
            {
                DatumListAddOrUpdate(ref tcTraceData, "FACTORY_WORKS_LOTID", dutObject.BatchID);

                    //traceDataList.AddString("FACTORY_WORKS_LOTID", dutObject.BatchID);
                
            }
            //END ADD XIAOJIANG 2017.03.20

            #region Fill in blank TC and CH results

            // Add 'missing data' for TC_* and CH_* parameters
            foreach (ParamLimit paramLimit in progInfo.MainSpec)
            {
                if (paramLimit.ExternalName.StartsWith("TC_") || paramLimit.ExternalName.StartsWith("CH_"))
                {
                    if (!tcTraceData.IsPresent(paramLimit.ExternalName))
                    {
                        Datum dummyValue = null;
                        
                        if (paramLimit.ExternalName.Contains("TC_MZ_IMB_"))
                        {
                            if (ParamManager.Conditions.IsThermalPhase)
                            {
                                dummyValue = new DatumDouble(paramLimit.ExternalName, Convert.ToDouble(paramLimit.HighLimit.ValueToString()));
                            }
                            else
                            {
                                dummyValue = new DatumDouble(paramLimit.ExternalName, Convert.ToDouble(paramLimit.LowLimit.ValueToString()));
                            }
                        }
                        else
                        {
                            switch (paramLimit.ParamType)
                            {
                                case DatumType.Double:
                                    dummyValue = new DatumDouble(paramLimit.ExternalName, Convert.ToDouble(paramLimit.HighLimit.ValueToString()));
                                    break;
                                case DatumType.Sint32:
                                    dummyValue = new DatumSint32(paramLimit.ExternalName, Convert.ToInt32(paramLimit.HighLimit.ValueToString()));
                                    break;
                                case DatumType.StringType:
                                    dummyValue = new DatumString(paramLimit.ExternalName, "MISSING");
                                    break;
                                case DatumType.Uint32:
                                    dummyValue = new DatumUint32(paramLimit.ExternalName, Convert.ToUInt32(paramLimit.HighLimit.ValueToString()));
                                    break;
                            }
                        }

                        if (dummyValue != null)
                        {
                            tcTraceData.Add(dummyValue);
                        }
                    }
                }
            }
            #endregion

            //write coc information
            
            tcTraceData.AddString("CHIP_ID", ParamManager.Conditions.CHIP_ID);
            tcTraceData.AddString("COC_SERIAL_NO", ParamManager.Conditions.COC_SN);
            tcTraceData.AddString("WAFER_ID", ParamManager.Conditions.WAFER_ID);

            DatumType typeOfSWAPWIREBOND = progInfo.MainSpec.GetParamLimit("SWAPWIREBOND").ParamType;

            if (typeOfSWAPWIREBOND == DatumType.Sint32)
            {
                tcTraceData.AddSint32("SWAPWIREBOND", ParamManager.Conditions.IsSwapWirebond ? 1 : 0);
            }
            else
            {
                tcTraceData.AddString("SWAPWIREBOND", ParamManager.Conditions.IsSwapWirebond ? "1" : "0");
            }

            tcTraceData.AddString("WAFER_SIZE", this._mapResult.ReadString("LASER_WAFER_SIZE"));
            tcTraceData.AddSint32("NODE", dutObject.NodeID);
            //if (_mapResult.IsPresent("REF_LV_FILE"))
            //{
            //    tcTraceData.AddFileLink("REF_LI_FILE", this._mapResult.ReadFileLinkFullPath("REF_LV_FILE"));
            //}

            // The RTH limit unit is ohm - chongjian.liang 2013.5.13
            if (progInfo.MainSpec.GetParamLimit("RTH").Units.ToLower().Contains("ohm"))
            {
                double lowLimit = double.Parse(progInfo.MainSpec.GetParamLimit("RTH").LowLimit.ValueToString());
                double highLimit = double.Parse(progInfo.MainSpec.GetParamLimit("RTH").HighLimit.ValueToString());

                if (lowLimit == highLimit)
                {
                    tcTraceData.AddDouble("RTH", lowLimit);
                }
                else
                {
                    this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "Cannot determine the RTH value.", FailModeCategory_Enum.SW);
                }
            }
            else // The RTH limit unit is degree
            {
                if (_mapResult.IsPresent("LASER_RTH"))
                {
                    tcTraceData.AddDouble("RTH", 999); //this._mapResult.ReadDouble("LASER_RTH"));
                }
                else
                {
                    tcTraceData.AddDouble("RTH", 999);
                }
            }

            double testTime = Math.Round(engine.GetTestTime() / 60, 2);
            double processTime = Math.Round(engine.TestProcessTime / 60, 2);
            double laborTime = testTime - processTime;

            tcTraceData.AddString("TIME_DATE", TestTimeStamp_Start);
            tcTraceData.AddDouble("TEST_TIME", testTime);
            tcTraceData.AddDouble("LABOUR_TIME", laborTime);
            // Add more detail into the main specification
            tcTraceData.AddString("FACTORY_WORKS_FAILMODE", " ");

            //tcTraceData.AddString("FACTORY_WORKS_LOTID", dutObject.BatchID);

            DatumListAddOrUpdate(ref tcTraceData, "FACTORY_WORKS_LOTID", dutObject.BatchID);

            tcTraceData.AddString("FACTORY_WORKS_PARTID", dutObject.PartCode);
            tcTraceData.AddString("SOFTWARE_ID", dutObject.ProgramPluginName + " SW_version: " + dutObject.ProgramPluginVersion + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString());// + "  " + engine.SpecificTestSolutionId.ToString());
            tcTraceData.AddString("OPERATOR_ID", userList.InitialUser);
            tcTraceData.AddString("COMMENTS", engine.GetProgramRunComments());
            tcTraceData.AddString("PRODUCT_CODE", dutObject.PartCode);
            tcTraceData.AddString("FULL_PTR_TEST_FLAG", "0");
            //tcTraceData.AddString("PTR_EOL_FREQ_DELTA", "16");
            tcTraceData.AddDouble("TEC_LASER_OL_TEMP", TEC_LASER_OL_TEMP);
            //tcTraceData.AddDouble("TEC_JIG_CL_TEMP", TEC_JIG_CL_TEMP);
            tcTraceData.AddString("EQUIP_ID", dutObject.EquipmentID);

            // Since some files were generated but not zipped, so zip these files to the REGISTRY_FILE - chongjian.liang 2013.5.21
            string finalFilesZipPath = Path.Combine(this.CommonTestConfig.ResultsSubDirectory, "ZipFilesOnFinalOT_" + dutObject.SerialNumber + "_" + TestTimeStamp_Start + ".zip");
            List<string> filesToZip = GetFilesToZip(progInfo.TestParamsConfig.GetStringParam("ZipFileListOnFinalOT"));
            Util_ZipFile.ZipFiles(finalFilesZipPath, filesToZip);

            if (File.Exists(finalFilesZipPath))
            {
                tcTraceData.AddFileLink("REGISTRY_FILE", finalFilesZipPath);
            }

            #region Write Channel pass/fail files and TTA/TXFP cal files
            if (IlmzItuChannels == null)
            {
                if (engine.GetProgramRunStatus() != ProgramStatus.Success)
                {
                    if (moduleRunError)
                    {
                        tcTraceData.AddString("TEST_STATUS", "Fail");
                    }
                    else
                    {
                        tcTraceData.AddString("TEST_STATUS", engine.GetProgramRunStatus().ToString());
                    }
                }
                else
                {
                    string status = progInfo.MainSpec.Status.Status.ToString();
                    tcTraceData.AddString("TEST_STATUS", status);
                }

                if (this.errorInformation != null && (this.errorInformation.Length != 0))
                {
                    if (this.errorInformation.Length > 80)
                        this.errorInformation = this.errorInformation.Substring(0, 80);
                    tcTraceData.AddString("FAIL_ABORT_REASON", this.errorInformation);
                }
                else
                {
                    tcTraceData.AddString("FAIL_ABORT_REASON", engine.ProgramRunStatus.ToString());
                }

                engine.SetDataKeysPerSpec(progInfo.MainSpec.Name, finalKeys);
                try
                {
                    this.failModeCheck.CheckMainSpec(engine, dutObject, this.MainSpec, this.CommonTestConfig.PCASFailMode_FilePath, ref tcTraceData);
                    
                    engine.SetTraceData(progInfo.MainSpec.Name, tcTraceData);
                }
                catch (Exception e)
                {
                    engine.ShowContinueUserQuery(e.Message + "\n Error occurs, please check pcas drop data!");
                }
            }
            else
            {
                // Write channel files

                string passFailFile = Util_GenerateFileName.GenWithTimestamp(this.CommonTestConfig.ResultsSubDirectory,
                                            "CHANNEL_OVER_TEMP_FILE", dutObject.SerialNumber, "csv");

                ChannelDataFiles.WriteFinal_OT_Data(IlmzItuChannels.AllOptions, passFailFile, extremeChansSetup, channelsIndexes_TestOT);

                tcTraceData.AddFileLink("CHANNEL_OVER_TEMP_FILE", passFailFile);

            #endregion

                if ((this.errorInformation != null) &&
                    (this.errorInformation.Length != 0))
                {
                    if (this.errorInformation.Length > 80)
                        this.errorInformation = this.errorInformation.Substring(0, 80);
                    tcTraceData.AddString("FAIL_ABORT_REASON", this.errorInformation);
                }
                else
                {
                    tcTraceData.AddString("FAIL_ABORT_REASON", "Success");
                }
                if (engine.GetProgramRunStatus() != ProgramStatus.Success)
                {
                    if (moduleRunError)
                    {
                        tcTraceData.AddString("TEST_STATUS", "Fail");
                    }
                    else
                    {
                        tcTraceData.AddString("TEST_STATUS", engine.GetProgramRunStatus().ToString());
                    }
                }
                else
                {
                    string status = progInfo.MainSpec.Status.Status.ToString();
                    tcTraceData.AddString("TEST_STATUS", status);
                }

                if (progInfo.MainSpec.ParamLimitExists("RETEST"))
                {
                    tcTraceData.AddString("RETEST", this.retestCount.ToString());
                }

                if (!dutOutcome.DUTData.IsPresent("RETEST"))
                {
                    dutOutcome.DUTData.AddSint32("RETEST", this.retestCount);
                }

                // Set trace data above here
                engine.SetDataKeysPerSpec(progInfo.MainSpec.Name, finalKeys);
                try
                {
                    this.failModeCheck.CheckMainSpec(engine, dutObject, this.MainSpec, this.CommonTestConfig.PCASFailMode_FilePath, ref tcTraceData);

                    engine.SetTraceData(progInfo.MainSpec.Name, tcTraceData);
                }
                catch (Exception e)
                {
                    engine.ShowContinueUserQuery(e.Message + " \nError occcurs, please check pcas drop data!");
                }
            }
        }

        /// <summary>
        /// fill in empty ingore parameters
        /// </summary>
        /// <param name="engine"></param>
        private void Data_Ignore_Write(ITestEngineDataWrite engine)
        {
            string[] ignoreParas = {
                
                "RTH",
                //"FIBRE_POWER_INITIAL"
            };
            //DatumList tcTraceData = new DatumList();
            foreach (string para in ignoreParas)
            {
                foreach (ParamLimit paramLimit in progInfo.MainSpec)
                {
                    if (paramLimit.ExternalName == para)
                    {
                        if (!tcTraceData.IsPresent(paramLimit.ExternalName))
                        {
                            Datum dummyValue = null;
                            switch (paramLimit.ParamType)
                            {
                                case DatumType.Double:
                                    dummyValue = new DatumDouble(paramLimit.ExternalName, Convert.ToDouble(paramLimit.HighLimit.ValueToString()));
                                    break;
                                case DatumType.Sint32:
                                    dummyValue = new DatumSint32(paramLimit.ExternalName, Convert.ToInt32(paramLimit.HighLimit.ValueToString()));
                                    break;
                                case DatumType.StringType:
                                    dummyValue = new DatumString(paramLimit.ExternalName, "No Value");
                                    break;
                                case DatumType.Uint32:
                                    dummyValue = new DatumUint32(paramLimit.ExternalName, Convert.ToUInt32(paramLimit.HighLimit.ValueToString()));
                                    break;
                                case DatumType.FileLinkType:
                                    dummyValue = new DatumFileLink(paramLimit.ExternalName, this.Nofilepath);
                                    break;
                            }
                            if (dummyValue != null)
                                //tcTraceData.Add(dummyValue);
                                this.tcTraceData.Add(dummyValue);
                        }//if
                    }//if (paramLimit.ExternalName == para)
                }//foreach (ParamLimit paramLimit in mainSpec)
            }// foreach (string para in ignoreParas)
            //engine.SetTraceData(this.progInfo.MainSpec.Name, tcTraceData);
        }

        #endregion

        #region Private Helper Functions

        private MzData[] GetMzDataForEveryChannel()
        {
            if (!File.Exists(channelPassFile))
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "Can't find channel pass file, please do final test for Hit2 GB!", FailModeCategory_Enum.OP);
            }
            //read mzdata from channel pass file
            CsvReader cr = new CsvReader();
            passFileLines = cr.ReadFile(channelPassFile);

            int chanNbr = passFileLines.Count - 3;//except for title line ,lowlimit line ,highlimit line
            MzData[] mzDatas = new MzData[chanNbr];

            for (int i = 0; i < chanNbr; i++)
            {

                mzDatas[i].LeftArmMod_Peak_V = GetDoubleValueFromEnumParam(EnumTosaParam.MzLeftArmModPeak_V, i);

                mzDatas[i].RightArmMod_Peak_V = GetDoubleValueFromEnumParam(EnumTosaParam.MzRightArmModPeak_V, i);

                mzDatas[i].LeftArmMod_Quad_V = GetDoubleValueFromEnumParam(EnumTosaParam.MzLeftArmModQuad_V, i);

                mzDatas[i].RightArmMod_Quad_V = GetDoubleValueFromEnumParam(EnumTosaParam.MzRightArmModQuad_V, i);

                mzDatas[i].LeftArmMod_Min_V = GetDoubleValueFromEnumParam(EnumTosaParam.MzLeftArmModMinima_V, i);

                mzDatas[i].RightArmMod_Min_V = GetDoubleValueFromEnumParam(EnumTosaParam.MzRightArmModMinima_V, i);

                mzDatas[i].LeftArmImb_mA = GetDoubleValueFromEnumParam(EnumTosaParam.MzLeftArmImb_mA, i);

                mzDatas[i].RightArmImb_mA = GetDoubleValueFromEnumParam(EnumTosaParam.MzRightArmImb_mA, i);

                mzDatas[i].LeftArmImb_Dac = GetDoubleValueFromEnumParam(EnumTosaParam.MzLeftArmImb_Dac, i);

                mzDatas[i].RightArmImb_Dac = GetDoubleValueFromEnumParam(EnumTosaParam.MzRightArmImb_Dac, i);

            }
            return mzDatas;
        }

        private int FindColumnInStringArray(string columnName, string[] stringArray)
        {
            int returnIndex = -1;

            for (int i = 0; i < stringArray.Length; i++)
            {
                if (stringArray[i].ToUpper() == columnName.ToUpper())
                {
                    returnIndex = i;
                    break;
                }
            }
            if (returnIndex < 0) {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "No " + columnName + " column exist in channel pass file!", FailModeCategory_Enum.PR);
            }

            return returnIndex;
        }
        private double GetDoubleValueFromEnumParam(EnumTosaParam colName, int ituChIndex)
        {
            string columnName = colName.ToString();

            double dVal = 0;
            try
            {

                int colIndex = FindColumnInStringArray(columnName, passFileLines[0]);
                if (passFileLines[ituChIndex + 3][colIndex].Trim().Length > 0)
                {
                    dVal = double.Parse(passFileLines[ituChIndex + 3][colIndex]);
                }

            }
            catch (Exception e)
            {
                string strmsg = e.Message;
            }

            return dVal;

        }
        private string GetStringValueFromEnumParam(EnumTosaParam colName, int ituChanIndex)
        {
            string columnName = colName.ToString();

            string sVal = "";
            try
            {

                int colIndex = FindColumnInStringArray(columnName, passFileLines[0]);
                if (passFileLines[ituChanIndex + 3][colIndex].Trim().Length > 0)
                {
                    sVal = passFileLines[ituChanIndex + 3][colIndex].Trim();
                }

            }
            catch (Exception e)
            {
                string strmsg = e.Message;
            }

            return sVal;

        }

        private bool GetBoolValueFromEnumParam(EnumTosaParam colName, int ituChanIndex)
        {
            string columnName = colName.ToString();

            bool bVal = false;
            try
            {

                int colIndex = FindColumnInStringArray(columnName, passFileLines[0]);
                if (passFileLines[ituChanIndex + 3][colIndex].Trim().ToUpper() == "TRUE")
                {
                    bVal = true;
                }
                if (passFileLines[ituChanIndex + 3][colIndex].Trim().ToUpper() == "FALSE")
                {
                    bVal = false;
                }

            }
            catch (Exception e)
            {
                string strmsg = e.Message;
            }

            return bVal;

        }

        private IlmzChannels BuildMidTempTestResult()
        {
            // Build ILMZ channels
            MzData[] mzDataCollection = GetMzDataForEveryChannel();

            IlmzChannelInit[] channelInits = new IlmzChannelInit[mzDataCollection.Length];

            for (int ituChIndex = 0; ituChIndex < mzDataCollection.Length; ituChIndex++)
            {
                //IlmzChannelInit oneChannelInit = channelInits[ituChanIndex];
                channelInits[ituChIndex].Dsdbr = ituChanData[ituChIndex];
                channelInits[ituChIndex].Mz = mzDataCollection[ituChIndex];
            }

            double lowTemperature = progInfo.TestConditions.LowTemp;
            double highTemperature = progInfo.TestConditions.HighTemp;
            Specification spec = progInfo.MainSpec;

            IlmzChannels ilmzItuChannels = new IlmzChannels(channelInits, spec, lowTemperature, highTemperature, true);

            double startFreq = progInfo.TestConditions.ItuLowFreq_GHz;
            double stopFreq = progInfo.TestConditions.ItuHighFreq_GHz;
            int ituChanIndex = 0;

            for (double ituFreq = startFreq; ituFreq <= stopFreq; ituFreq += progInfo.TestConditions.ItuSpacing_GHz)
            {
                //int ituChanIndex = 0;
                ILMZChannel oneChannel;
                ILMZChannel[] channelsAtItu;
                int chanIndex = 0;
                try
                {
                    //channelsAtItu = ilmzItuChannels.GetChannelsAtItu(ituFreq);
                    chanIndex = ilmzItuChannels.GetChannelIndexAtItu(ituFreq);
                    //oneChannel = channelsAtItu[0];
                }
                catch (Exception e)
                {
                    this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "Can't read " + ituFreq.ToString() + "GHz Channel data !", FailModeCategory_Enum.UN);
                }
                if (chanIndex < 0)
                {

                    continue;
                }

                foreach (string nameAsStr in Enum.GetNames(typeof(EnumTosaParam)))
                {
                    EnumTosaParam parameter = (EnumTosaParam)Enum.Parse(typeof(EnumTosaParam), nameAsStr);
                    if (parameter == EnumTosaParam.TuningOk || parameter == EnumTosaParam.PowerLevellingOk || parameter == EnumTosaParam.Pass_Flag) // Add by tim
                    {
                        bool bResult = GetBoolValueFromEnumParam(parameter, ituChanIndex);

                        ilmzItuChannels.AllOptions[chanIndex].MidTempData.SetValueBool(parameter, bResult);
                    }
                    else
                    {
                        double midResult = GetDoubleValueFromEnumParam(parameter, ituChanIndex);
                        if (parameter == EnumTosaParam.ItuChannelIndex)
                        {
                            continue;
                        }
                        if (parameter == EnumTosaParam.Supermode || parameter == EnumTosaParam.LongitudinalMode || parameter == EnumTosaParam.FrontSectionPair)
                        {
                            ilmzItuChannels.AllOptions[chanIndex].MidTempData.SetValueSint32(parameter, (int)midResult);
                        }
                        else
                        {
                            ilmzItuChannels.AllOptions[chanIndex].MidTempData.SetValueDouble(parameter, midResult);
                        }
                    }
                }
                ituChanIndex++;
            }
            return ilmzItuChannels;
        }

        /// <summary>
        /// Creates an adjusted specifiation for engineering use
        /// </summary>
        /// <param name="engine">reference to test engine</param>
        /// <returns>An adjusted specification</returns>
        private Specification ConstructEngineeringSpec(ITestEngineInit engine)
        {
            engine.SendStatusMsg("Construct Engineering specification");
            GuiMsgs.TcmzEngineeringGuiResponse resp = (GuiMsgs.TcmzEngineeringGuiResponse)engine.ReceiveFromGui().Payload;
            double engAdjustment = (double)resp.FiddleFactor;

            // Calculate power difference
            double upperLimit = Convert.ToDouble(progInfo.MainSpec.GetParamLimit("CH_PEAK_POWER").HighLimit.ValueToString());
            double lowerLimit = Convert.ToDouble(progInfo.MainSpec.GetParamLimit("CH_PEAK_POWER").LowLimit.ValueToString());
            double limitTargetPower = (upperLimit - lowerLimit) / 2 + lowerLimit;
            double adjustment = limitTargetPower - engAdjustment;

            List<String> paramsToAdjust = new List<string>();
            paramsToAdjust.Add("CH_PEAK_POWER");
            paramsToAdjust.Add("CH_PWRCTRL_POWER_MAX_HIGH");
            paramsToAdjust.Add("CH_PWRCTRL_POWER_MAX_LOW");
            paramsToAdjust.Add("CH_PWRCTRL_POWER_MIN_HIGH");
            paramsToAdjust.Add("CH_PWRCTRL_POWER_MIN_LOW");
            paramsToAdjust.Add("TC_FIBRE_TARGET_POWER");
            paramsToAdjust.Add("TC_POWER_CTRL_RANGE_MAX");
            paramsToAdjust.Add("TC_POWER_CTRL_RANGE_MIN");

            Specification powerAdjustedSpec = new Specification(progInfo.MainSpec.Name, progInfo.MainSpec.ActualDatabaseKeys, progInfo.MainSpec.Priority);
            foreach (ParamLimit pl in progInfo.MainSpec)
            {
                Datum highLimit = pl.HighLimit;
                Datum lowLimit = pl.LowLimit;
                if (paramsToAdjust.Contains(pl.InternalName))
                {
                    double oldHighLimit = Convert.ToDouble(pl.HighLimit.ValueToString());
                    double trimmedHighLimit = Math.Truncate(oldHighLimit);
                    if (trimmedHighLimit != 999 && trimmedHighLimit != 9999)
                    {
                        DatumDouble newHighLimit = new DatumDouble(pl.InternalName, oldHighLimit - adjustment);
                        highLimit = (Datum)newHighLimit;
                    }

                    double oldLowLimit = Convert.ToDouble(pl.LowLimit.ValueToString());
                    double trimmedLowLimit = Math.Truncate(oldLowLimit);
                    if (trimmedLowLimit != -999 && trimmedLowLimit != -9999)
                    {
                        DatumDouble newLowLimit = new DatumDouble(pl.InternalName, oldLowLimit - adjustment);
                        lowLimit = (Datum)newLowLimit;
                    }
                }
                RawParamLimit rpl = new RawParamLimit(pl.InternalName, pl.ParamType, lowLimit, highLimit, pl.Operand, pl.Priority, pl.AccuracyFactor, pl.Units);
                powerAdjustedSpec.Add(rpl);
            }
            return powerAdjustedSpec;
        }

        /// <summary>
        /// if no pcas result for mapping, get nessesary information by manual
        /// it is useful for Eng debug
        /// </summary>
        /// <param name="engine"></param>
        public void ManualEntryMappingReults(ITestEngineInit engine)
        {
            engine.GuiShow();
            engine.GuiToFront();
            engine.SendToGui(new GuiMsgs.tosaMappingResultRequst());

            engine.SendStatusMsg("Construct Engineering mapping data");
            GuiMsgs.tosaMappingResponse resp = (GuiMsgs.tosaMappingResponse)engine.ReceiveFromGui().Payload;

            this._mapResult = resp.mapReult;
            if (this._mapResult == null)
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "No TOSA Mapping Data , please test this device at Hitt_Map stage first!", FailModeCategory_Enum.OP);
            }
            try
            {
                string MAP_TC_NUM_CHAN_REQUIRED = this._mapResult.ReadSint32("TC_NUM_CHAN_REQUIRED").ToString();
                string MAP_TC_OPTICAL_FREQ_START = this._mapResult.ReadDouble("TC_OPTICAL_FREQ_START").ToString();//Jack.Zhang
                //string MAP_TC_OPTICAL_FREQ_START = this._mapResult.ReadDouble("TC_OPTICAL_FREQ_START").ToString();
            }
            catch
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "Can't load this 2 map parameters: 'TC_NUM_CHAN_REQUIRED' & 'TC_OPTICAL_FREQ_START'.", FailModeCategory_Enum.OP);
            }
            if (!this._mapResult.ReadString("TEST_STATUS").ToLower().Contains("pass"))
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "No passed TCMZ_Map stage data in PCAS, please test this device at Hitt_Map stage first!", FailModeCategory_Enum.OP);
            }

            // Get data from ITU operating file - chongjian.liang 2015.7.13
            string closeGridCharFileName = this._mapResult.ReadFileLinkFullPath("CG_CHAR_ITU_FILE");
            //string closeGridCharFileName = this._mapResult.ReadFileLinkFullPath("CG_CHAR_ITU_FILE");
            this.closeGridCharData = Bookham.Toolkit.CloseGrid.CsvDataLookup.GetItuOperatingPoints(closeGridCharFileName);


        }

        private void RecordSweepData(ILMZSweepResult sweepData, string ChangePartcodeSweepFile)
        {
            Dictionary<ILMZSweepDataType, double[]> sweepDataListTemp = sweepData.SweepData;
            List<string> dataNameList = new List<string>();
            List<double[]> sweepDataList = new List<double[]>();

            foreach (ILMZSweepDataType dataName in sweepDataListTemp.Keys)
            {
                dataNameList.Add(dataName.ToString());
                sweepDataList.Add(sweepDataListTemp[dataName]);
            }

            double[][] sweepDataArray = sweepDataList.ToArray();

            string[] dataNameArray = dataNameList.ToArray();

            if ((sweepDataArray.Length > 0) && (dataNameArray[0].Length > 0))
            { }
            else
            {
                return;
            }

            string mzSweepDataResultsFile = Path.Combine(this.CommonTestConfig.ResultsSubDirectory, ChangePartcodeSweepFile);
            if (File.Exists(mzSweepDataResultsFile))
            {
                File.Delete(mzSweepDataResultsFile);
            }

            using (StreamWriter writer = new StreamWriter(mzSweepDataResultsFile))
            {
                // file header
                StringBuilder fileHeader = new StringBuilder();

                for (int count = 0; count < dataNameArray.Length; count++)
                {
                    if (count != 0) fileHeader.Append(",");
                    fileHeader.Append(dataNameArray[count]);
                }

                // file contents
                List<string> fileContents = new List<string>();
                for (int irow = 0; irow < sweepDataArray[0].Length; irow++)
                {
                    StringBuilder aLine = new StringBuilder();
                    for (int icol = 0; icol < dataNameArray.Length; icol++)
                    {
                        if (icol != 0) aLine.Append(",");
                        aLine.Append(sweepDataArray[icol][irow].ToString());
                    }
                    fileContents.Add(aLine.ToString());
                }

                // write file header to file
                writer.WriteLine(fileHeader.ToString());

                // writer file contents to file
                foreach (string row in fileContents)
                {
                    writer.WriteLine(row);
                }
            }

        }

        /// <summary>
        /// Add ITU channel with max ripple to do OT test - chongjian.liang 2013.5.2
        /// </summary>
        private List<int> GetChanOfRippleMax(ITestEngineBase engine, out int o_eolScanBoundLeft, out int o_eolScanBoundRight)
        {
            List<int> extremeChansOfRipple = new List<int>();
            int lineIndexOfMaxRipple = 0;
            double maxRipple = 0;
            int ituIndexOfMaxRipple = 0;

            // Get the ITU channel with max ripple from scan file
            using (CsvReader csvReader = new CsvReader())
            {
                // chongjian.liang 2015.8.18
                //List<string[]> phaseScanResult = csvReader.ReadFile(finalResult.ReadFileLinkFullPath("LM_PHASE_SCAN_FOR_RIPPLE_FILE"));
                List<string[]> phaseScanResult = csvReader.ReadFile(finalResult.ReadFileLinkFullPath("LM_PHASE_SCAN_FOR_RIPPLE_FILE"));

                for (int i = 0; i < phaseScanResult.Count; i += 4)
                {
                    double ripple = double.Parse(phaseScanResult[i][2].Split(':')[1]);

                    if (ripple > maxRipple)
                    {
                        maxRipple = ripple;
                        lineIndexOfMaxRipple = i;
                    }
                }

                int ituChannelOfMaxRipple = int.Parse(phaseScanResult[lineIndexOfMaxRipple][0].Split(':')[1]);
                ituIndexOfMaxRipple = IlmzItuChannels.GetChannelIndexAtItu(ituChannelOfMaxRipple);

                int[] phaseScanBound = new int[2];
                string[] phaseScanBoundStrings = phaseScanResult[lineIndexOfMaxRipple][1].Split(':')[1].Split('-');

                for (int i = 0; i < 2; i++)
                {
                    phaseScanBound[i] = int.Parse(phaseScanBoundStrings[i]);
                }

                o_eolScanBoundLeft = phaseScanBound[0];
                o_eolScanBoundRight = phaseScanBound[1];
            }

            // Add channel of max ripple for ripple & OT test
            extremeChansOfRipple.Add(ituIndexOfMaxRipple);

            return extremeChansOfRipple;
        }

        /// <summary>
        ///  Add ITU channel with max irear per supermodes to do OT test - chongjian.liang 2013.5.2
        /// </summary>
        private List<int> GetChansOfRearMaxPerSM(IlmzChannels.ExtremeChannelIndexes extremeChans, List<int> extremeChansOfRipple)
        {
            List<int> extremeChansOfIRear = new List<int>();

            const int superModeCount = 8;

            bool[] isSuperModeHasChannelsForOT = new bool[superModeCount];

            // Dictionary<SuperModeIndex, Dictionary<ITUIndex, RearCurrent>>
            Dictionary<int, List<ILMZChannel>> rearCurrentsOfSuperModes = new Dictionary<int, List<ILMZChannel>>();

            for (int i = 0; i < superModeCount; i++)
            {
                rearCurrentsOfSuperModes.Add(i, new List<ILMZChannel>());
            }

            // When a super mode already has channels to do OT test, don't add the max rear channel
            foreach (ILMZChannel ituChannel in IlmzItuChannels.AllOptions)
            {
                int ituIndex = int.Parse(ituChannel.MidTempData.GetMeasuredData(EnumTosaParam.ItuChannelIndex).ValueToString());
                int superModeIndex = int.Parse(ituChannel.MidTempData.GetMeasuredData(EnumTosaParam.Supermode).ValueToString());

                // Check if the super mode has any channel for OT test
                if (ituIndex == extremeChans.FirstChannelIndex ||
                    ituIndex == extremeChans.LastChannelIndex ||
                    ituIndex == extremeChans.HighestestIsoaIndex ||
                    ituIndex == extremeChans.LowestIsoaIndex ||
                    ituIndex == extremeChans.HighestLockSlopeEffIndex ||
                    ituIndex == extremeChans.LowestLockSlopeEffIndex ||
                    ituIndex == extremeChans.HighestLaserDissIndex ||
                    ituIndex == extremeChans.LowestLaserDissIndex ||
                    extremeChansOfRipple.Contains(ituIndex))
                {
                    isSuperModeHasChannelsForOT[superModeIndex] = true;
                }

                // Collect itu channels per super modes
                rearCurrentsOfSuperModes[superModeIndex].Add(ituChannel);
            }

            /// For the super mode has no channel for OT test, add the ITU channel with max rear current of this supermode to do OT

            for (int i = 0; i < superModeCount; i++)
            {
                if (!isSuperModeHasChannelsForOT[i] && rearCurrentsOfSuperModes[i].Count > 0)
                {
                    double maxRear = 0;
                    int maxRearIndex = 0;

                    for (int j = 0; j < rearCurrentsOfSuperModes[i].Count; j++)
                    {
                        double currentRear = double.Parse(rearCurrentsOfSuperModes[i][j].MidTempData.GetMeasuredData(EnumTosaParam.IRear_mA).ValueToString());

                        if (maxRear < currentRear)
                        {
                            maxRear = currentRear;
                            maxRearIndex = j;
                        }
                    }

                    int ituIndexForOT = int.Parse(rearCurrentsOfSuperModes[i][maxRearIndex].MidTempData.GetMeasuredData(EnumTosaParam.ItuChannelIndex).ValueToString());

                    extremeChansOfIRear.Add(ituIndexForOT);
                }
            }

            return extremeChansOfIRear;
        }

        /// <summary>
        /// Retrieves datums from config file via the ConfigAccessor.
        /// </summary>
        /// <param name="modRun">The ModuleRun to add the items to.</param>
        /// <param name="datumsRequired">The datum names in this datumlist specify the names of the datums to be retrieved and added to the ModuleRun</param>
        private void ExtractAndAddDatums(ModuleRun modRun, DatumList datumsRequired)
        {
            foreach (Datum datum in datumsRequired)
            {
                switch (datum.Type)
                {
                    case DatumType.Double:
                        {
                            modRun.ConfigData.Add(new DatumDouble(datum.Name, this.progInfo.TestParamsConfig.GetDoubleParam(datum.Name)));
                        }
                        break;

                    case DatumType.StringType:
                        {
                            modRun.ConfigData.Add(new DatumString(datum.Name, this.progInfo.TestParamsConfig.GetStringParam(datum.Name)));
                        }
                        break;

                    case DatumType.Uint32:
                        {
                            modRun.ConfigData.Add(new DatumUint32(datum.Name, this.progInfo.TestParamsConfig.GetIntParam(datum.Name)));
                        }
                        break;

                    case DatumType.Sint32:
                        {
                            modRun.ConfigData.Add(new DatumSint32(datum.Name, this.progInfo.TestParamsConfig.GetIntParam(datum.Name)));
                        }
                        break;

                    case DatumType.BoolType:
                        {
                            modRun.ConfigData.Add(new DatumBool(datum.Name, this.progInfo.TestParamsConfig.GetBoolParam(datum.Name)));
                        }
                        break;

                    default:
                        {
                            throw new ArgumentException("Invalid DatumType '" + datum.Type + "' of '" + datum.Name + "'");
                        }
                }
            }
        }

        /// <summary>
        /// Read instruments calibration data
        /// </summary>
        /// <param name="InstrumentName"></param>
        /// <param name="InstrumentSerialNo"></param>
        /// <param name="ParameterName"></param>
        /// <param name="CalFactor"></param>
        /// <param name="CalOffset"></param>
        private void readCalData(string InstrumentName, string InstrumentSerialNo,
            string ParameterName, out double CalFactor, out double CalOffset)
        {
            StringDictionary keys = new StringDictionary();
            keys.Add("Instrument_name", InstrumentName);
            keys.Add("Parameter", ParameterName);
            if (InstrumentSerialNo != null)
            {
                keys.Add("Instrument_SerialNo", InstrumentSerialNo);
            }
            DatumList calParams = progInfo.InstrumentsCalData.GetData(keys, false);

            CalFactor = calParams.ReadDouble("CalFactor");
            CalOffset = calParams.ReadDouble("CalOffset");
        }

        /// <summary>
        /// Copy data from tcmz_map stage.
        /// </summary>
        /// <param name="mapData"></param>
        /// <param name="engine"></param>
        private void CopyDataFromMap(DatumList mapData, ITestEngineDataWrite engine)
        {
            try
            {
                string[] mapParameter ={
                    //"WAFER_SIZE",
                    //"SOA_POWER_LEVELING",
                    //"MATRIX_PRATIO_SUMMARY_MAP",
                    //"MODAL_DISTORT_SM0",
                    //"MODAL_DISTORT_SM1",
                    //"MODAL_DISTORT_SM2",
                    //"MODAL_DISTORT_SM3",
                    //"MODAL_DISTORT_SM4",
                    //"MODAL_DISTORT_SM5",
                    //"MODAL_DISTORT_SM6",
                    //"MODAL_DISTORT_SM7",
                    //"MILD_MULTIMODE_SM0",
                    //"MILD_MULTIMODE_SM1",
                    //"MILD_MULTIMODE_SM2",
                    //"MILD_MULTIMODE_SM3",
                    //"MILD_MULTIMODE_SM4",
                    //"MILD_MULTIMODE_SM5",
                    //"MILD_MULTIMODE_SM6",
                    //"MILD_MULTIMODE_SM7",
                    "CHAR_ITU_OP_COUNT",
                    //"CG_CHAR_ITU_FILE",
                    //"CG_ITUEST_POINTS_FILE",
                    
                    //"CG_DUT_FILE_TIMESTAMP",
                    //"CG_ID",
                    //"CG_SM_LINES_FILE",
                    //"CG_PRATIO_WL_FILE",
                    //"CG_QAMETRICS_FILE",
                    ////"CG_PASSFAIL_FILE",
                    ////"IRX_DARK",
                    ////"ITX_DARK",
                    ////"LASER_WAFER_ID",
                    "MZ_MAX_V_BIAS",
                    ////"MZ_MOD_LEFT_DARK_I",
                    ////"MZ_MOD_RIGHT_DARK_I",
                    //"NUM_SM",
                    //"REF_FIBRE_POWER",
                    //"REF_FREQ",
                    
                    //"REF_MZ_IMB_LEFT",
                    //"REF_MZ_IMB_RIGHT",
                    //"TAP_COMP_DARK_I",
                    "FIBRE_POWER_INITIAL",
                    //"LASER_WAFER_SIZE",
                    
                    //"REF_MZ_LEFT",
                    //"REF_MZ_RIGHT",
                    //"REF_MZ_RATIO",
                    //"REF_MZ_SUM",
                    //"REF_MZ_VOFF_LEFT",
                    //"REF_MZ_VOFF_RIGHT",
                    //"REF_MZ_VON_LEFT",
                    //"REF_MZ_VON_RIGHT",
                   
                    //"ITU_OP_EST_COUNT",
                
                     //"I_ETALON_FS_A",
                     //"I_ETALON_GAIN",
                     //"I_ETALON_SOA",
                     //"CG_PHASE_MODE_ACQ_FILE",
                     "NUM_MISSING_EST_CHAN",
                    "TC_DSDBR_TEMP",
                    //"REF_LI_FILE"//if map data from hitt_map test stage,
                };

                for (int i = 0; i < mapParameter.Length; i++)
                {
                    if (mapData.IsPresent(mapParameter[i]) && progInfo.MainSpec.ParamLimitExists(mapParameter[i]))
                    {
                        if (!mapData.GetDatum(mapParameter[i]).ValueToString().ToLower().Contains("unknown"))
                        {
                            tcTraceData.Add(mapData.GetDatum(mapParameter[i]));
                        }
                        else
                        {
                            string a = mapData.GetDatum(mapParameter[i]).ValueToString();
                        }
                    }
                }

            }
            catch (Exception e)
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "Copy map stage data error!" + e.Message, FailModeCategory_Enum.SW);
            }
        }
        
        #endregion
    }
}



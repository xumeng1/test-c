// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestPrograms
//
// Prog_TCMZ_Final.cs
//
// Author: paul.annetts, Mark Fullalove 2006
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.ExternalData;
using Bookham.TestEngine.Framework.Limits;
using System.Collections.Specialized;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.Toolkit.CloseGrid;
using Bookham.Toolkit.CloseGrid.TEEquipment;
using Bookham.TestLibrary.Utilities;
using Bookham.TestSolution.TcmzCommonInstrs;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestSolution.TcmzCommonData;
using Bookham.TestSolution.TcmzFinalData;
using Bookham.TestSolution.TcmzCommonUtils;
using Bookham.TestSolution.TestModules;
using Bookham.TestLibrary.Instruments;
using Bookham.ToolKit.Mz;
using Bookham.TestEngine.Config;
using System.Collections;
using Bookham.TestLibrary.BlackBoxes;
using Bookham.Tcmz.Core;
using System.Threading;

namespace Bookham.TestSolution.TestPrograms
{
    public class Prog_TCMZ_Final_ND : TcmzProgram, ITestProgram
    {
        #region Private data
        /// <summary>
        /// Remember the specification name we are using in the program
        /// </summary>
        private Prog_TCMZ_FinalInfo progInfo;
        private DatumList _mapResult;
        private DateTime testTime_End;
        private double labourTime = 0;
        private TcmzChannels tcmzItuChannels;
        private const string TCMZSchema = "hiberdb";

        private string errorInformation;

        private DsdbrChannelData[] closeGridCharData;

        private const string externalKeyTuneOpticalPowerToleranceInmW
            = "Tune_OpticalPowerTolerance_mW";
        private const string externalKeyTuneOpticalPowerToleranceIndB
            = "Tune_OpticalPowerTolerance_dB";
        private const string externalKeyTuneISOAPowerSlope
            = "Tune_IsoaPowerSlope";

        private int numberOfSupermodes;

        bool SelectTestFlag;
        private List<SectionIVSweepData> SectionIVCurveData;

        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        public Prog_TCMZ_Final_ND()
        {
            this.progInfo = new Prog_TCMZ_FinalInfo();
        }
        public override Type UserControl
        {
            get { return typeof(Prog_TCMZ_FinalGui); }
        }
        #region Program Initialisation

        public override void InitCode(ITestEngineInit engine, DUTObject dutObject, InstrumentCollection instrs, ChassisCollection chassis)
        {
            // get info for SMSR test
            SMSRMeasurement.strSN = dutObject.SerialNumber;
            //SMSRMeasurement.SMSRzipfilename = "results\\SMSR_Spectrum" + "_" + dutObject.SerialNumber + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".zip";
            //Util_ZipFile zipFile = new Util_ZipFile(SMSRMeasurement.SMSRzipfilename);            

            base.InitCode(engine, dutObject, instrs, chassis);

        }
        protected override void GetTestStageInformation(ITestEngineInit engine, DUTObject dutObject)
        {
            // Get test stage information - Must be "final"
            progInfo.TestStage = FinalTestStage.Final;
            if (dutObject.TestStage.ToLower() != "final")
            {
                engine.ErrorInProgram(string.Format("Un-matched test stage {0}. Expected \"final\"", dutObject.TestStage));
            }
        }
        protected override void VerifyJigTemperauture(ITestEngineInit engine)
        {
            if (VerifyJigTemperautureIsOk) return;
            engine.SendStatusMsg("verify device internal high temperature...");

            //get inst
            Inst_Ke2510 dsdbrTec = progInfo.Instrs.TecDsdbr as Inst_Ke2510;
            Inst_Ke2510 mzTec = progInfo.Instrs.TecMz as Inst_Ke2510;
            Inst_Ke2510 jigTec = progInfo.Instrs.TecCase as Inst_Ke2510;

            double JigHighTempValue = progInfo.TestConditions.HighTemp;
            dsdbrTec.OutputEnabled = false;
            mzTec.OutputEnabled = false;
            mzTec.Set_Protection_Level_Temperature(Inst_Ke2510.LimitType.UPPER, JigHighTempValue + 5);
            dsdbrTec.Set_Protection_Level_Temperature(Inst_Ke2510.LimitType.UPPER, JigHighTempValue + 5);

            jigTec.SensorTemperatureSetPoint_C = JigHighTempValue;
            jigTec.OutputEnabled = true;

            int count = 0;
            double Tolerance = 3;
            do
            {
                Thread.Sleep(2000);
                double dsdbrTemp = dsdbrTec.SensorTemperatureActual_C;
                double mztemp = mzTec.SensorTemperatureActual_C;
                if (JigHighTempValue - Tolerance < dsdbrTemp && dsdbrTemp < JigHighTempValue + Tolerance
                    && JigHighTempValue - Tolerance < mztemp && mztemp < JigHighTempValue + Tolerance)
                    break;
                count++;
                if (count > 60)
                {
                    jigTec.SensorTemperatureSetPoint_C = progInfo.TestParamsConfig.GetDoubleParam("TecCase_SensorTemperatureSetPoint_C");
                    Thread.Sleep(2000);
                    jigTec.OutputEnabled = false;
                    //engine.ErrorInModule("");
                    throw new Exception("The Dsdbr and Mz temperature can't reach the CaseTemp:" + JigHighTempValue.ToString());
                }
            } while (true);
            //restore settings
            TEC_LASER_OL_TEMP = dsdbrTec.SensorTemperatureActual_C;
            TEC_MZ_OL_TEMP = mzTec.SensorTemperatureActual_C;
            TEC_JIG_CL_TEMP = jigTec.SensorTemperatureActual_C;
            jigTec.SensorTemperatureSetPoint_C = progInfo.TestParamsConfig.GetDoubleParam("TecCase_SensorTemperatureSetPoint_C");
            Thread.Sleep(1000);
            jigTec.OutputEnabled = false;
            VerifyJigTemperautureIsOk = true;
            engine.SendStatusMsg("verify is ok!");

        }
        /// <summary>
        /// Initialise test configuration
        /// </summary>
        /// <param name="dutObj">Device under test</param>
        /// <param name="engine">Test engine</param>
        protected override void initConfig(DUTObject dutObj, ITestEngineInit engine)
        {
            progInfo.TestParamsConfig = new TestParamConfigAccessor(dutObj,
                @"Configuration\TcmzFinalConfig\TCMZ_Final\TcmzTestParams.xml", "", "TcmzTestParams");

            progInfo.InstrumentsCalData = new ConfigDataAccessor(@"Configuration\TcmzFinalConfig\InstrsCalibration.xml", "Calibration");

            progInfo.TempConfig = new TempTableConfigAccessor(dutObj, 1,
                @"Configuration\TcmzFinalConfig\TempTable.xml");

            progInfo.TestSelect = new TestSelection(@"Configuration\TcmzFinalConfig\TCMZ_Final\testSelect_" + dutObj.PartCode + ".xml");
        }

        /// <summary>
        /// Initialise instruments
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="instrs"></param>
        /// <param name="chassis"></param>
        protected override void initInstrs(ITestEngineInit engine, InstrumentCollection instrs, ChassisCollection chassis)
        {
            ProgTcmzFinalInstruments progInstrs = new ProgTcmzFinalInstruments();
            progInfo.Instrs = progInstrs;

            // Initialise DSDBR drive instruments
            switch (progInfo.DsdbrInstrsUsed)
            {
                case DsdbrDriveInstruments.FCUInstruments:
                    // FCU
                    progInstrs.Fcu = new FCUInstruments();
                    progInstrs.Fcu.FullbandControlUnit = (Instr_FCU)instrs["FullbandControlUnit"];
                    progInstrs.Fcu.SoaCurrentSource = (InstType_ElectricalSource)instrs["SoaCurrentSource"];
                    progInstrs.Fcu.FCU_Source = (InstType_ElectricalSource)instrs["FCU_Source"];
                    break;
                case DsdbrDriveInstruments.PXIInstruments:
                    // PXI
                    // Get instruments from CloseGrid
                    Chassis_CloseGrid chassisCloseGrid = Chassis_CloseGrid.Singleton;
                    progInstrs.Dsdbr = new DsdbrInstruments();
                    progInstrs.Dsdbr.CurrentSources = chassisCloseGrid.CurrentSources;
                    progInstrs.OpmCgDirect = chassisCloseGrid.OpticalPowerMeters[PowerMeterHead.Reference];
                    progInstrs.OpmCgFilter = chassisCloseGrid.OpticalPowerMeters[PowerMeterHead.Filter];
                    // SOA current actuators            
                    Inst_DigiIoComboSwitch soaRelays = new Inst_DigiIoComboSwitch("SoaRelays", @"Configuration\TcmzFinalConfig\SoaRelayConfig.xml",
                        instrs);
                    progInstrs.DsdbrSoaRelays = soaRelays;
                    break;
            }

            // MZs
            progInstrs.Mz = new MzInstruments();
            bool inlineTapPresent = progInfo.TestParamsConfig.GetBoolParam("InlineTapEnabled");
            progInstrs.Mz.TapComplementary = (InstType_TriggeredElectricalSource)instrs["MzTapComp"];
            if (inlineTapPresent)
                progInstrs.Mz.TapInline = (InstType_TriggeredElectricalSource)instrs["MzTapInline"];
            progInstrs.Mz.LeftArmMod = (InstType_TriggeredElectricalSource)instrs["MzLeftMod"];
            progInstrs.Mz.RightArmMod = (InstType_TriggeredElectricalSource)instrs["MzRightMod"];
            progInstrs.Mz.LeftArmImb = (InstType_TriggeredElectricalSource)instrs["MzLeftImb"];
            progInstrs.Mz.RightArmImb = (InstType_TriggeredElectricalSource)instrs["MzRightImb"];
            progInstrs.Mz.PowerMeter = (IInstType_TriggeredOpticalPowerMeter)instrs["OpmMz"];

            // OpmRef
            progInstrs.OpmReference = (IInstType_OpticalPowerMeter)instrs["OpmRef"];

            // OSA
            progInstrs.Osa = (IInstType_OSA)instrs["Osa"];

            // WM
            progInstrs.Wavemeter = (IInstType_Wavemeter)instrs["Wavemeter"];

            // Switch path manager
            Util_SwitchPathManager switchPathManager = new Util_SwitchPathManager
                (@"Configuration\TcmzFinalConfig\switches.xml", instrs);
            progInstrs.SwitchPathManager = switchPathManager;

            // Switch between OSA and Opm
            Switch_Osa_MzOpm switchOsaMzOpm = new Switch_Osa_MzOpm(switchPathManager);
            progInstrs.SwitchOsaMzOpm = switchOsaMzOpm;

            // TECs
            progInstrs.TecDsdbr = (IInstType_TecController)instrs["TecDsdbr"];
            progInstrs.TecMz = (IInstType_TecController)instrs["TecMz"];
            progInstrs.TecCase = (IInstType_TecController)instrs["TecCase"];

            // Config instruments if not simulation mode
            if (!engine.IsSimulation)
            {
                // Configure FCU
                if (progInfo.DsdbrInstrsUsed == DsdbrDriveInstruments.FCUInstruments)
                    ConfigureFCUInstruments(progInstrs.Fcu);

                // Configure TEC controllers
                ConfigureTecController(progInstrs.TecDsdbr, "TecDsdbr");
                ConfigureTecController(progInstrs.TecMz, "TecMz");
                ConfigureTecController(progInstrs.TecCase, "TecCase");

                // Configure MZ bias sources
                ConfigureMzBiasSource(progInstrs.Mz.LeftArmMod, "MzLeftMod");
                ConfigureMzBiasSource(progInstrs.Mz.RightArmMod, "MzRightMod");
                ConfigureMzBiasSource(progInstrs.Mz.LeftArmImb, "MzLeftImb");
                ConfigureMzBiasSource(progInstrs.Mz.RightArmImb, "MzRightImb");
                ConfigureMzBiasSource(progInstrs.Mz.TapComplementary, "MzTapComp");
                if (inlineTapPresent)
                    ConfigureMzBiasSource(progInstrs.Mz.TapInline, "MzTapInline");

                // Configure OSA 
                progInstrs.Osa.SetDefaultState();
                progInstrs.Osa.ReferenceLevel_dBm = 10.0;
                progInstrs.Osa.WavelengthStart_nm = 1520.0;
                progInstrs.Osa.WavelengthStop_nm = 1620.0;
                if (progInstrs.Osa.DriverName.Contains("Ag8614x"))
                {
                    Inst_Ag8614xA ag8614xOsa = (Inst_Ag8614xA)progInstrs.Osa;
                    ag8614xOsa.Sensitivity_dBm = Convert.ToDouble(progInfo.TestParamsConfig.GetStringParam("Osa_Sensitivity"));
                }

                // Configure power meter
                progInstrs.OpmReference.SetDefaultState();
                progInstrs.Mz.PowerMeter.SetDefaultState();

                // Configure switches
                progInstrs.SwitchOsaMzOpm.SetState(Switch_Osa_MzOpm.State.MzOpm);
            }
        }

        /// <summary>
        /// Load specifications
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        protected override void loadSpecs(ITestEngineInit engine, DUTObject dutObject)
        {
            ILimitRead limitReader = null;
            StringDictionary mainSpecKeys = new StringDictionary();

            if (progInfo.TestParamsConfig.GetBoolParam("UseLocalLimitFile"))
            {
                // initialise limit file reader
                limitReader = engine.GetLimitReader("PCAS_FILE");
                // main specification
                string mainSpecNameFile = progInfo.TestParamsConfig.GetStringParam("PcasLimitFileName");
                string mainSpecFullFilename = progInfo.TestParamsConfig.GetStringParam("PcasLimitFileDirectory")
                    + @"\" + mainSpecNameFile;
                mainSpecKeys.Add("Filename", mainSpecFullFilename);
            }
            else
            {
                // Use default limit source
                limitReader = engine.GetLimitReader();

                mainSpecKeys.Add("SCHEMA", "HIBERDB");
                mainSpecKeys.Add("TEST_STAGE", "final");
                mainSpecKeys.Add("DEVICE_TYPE", dutObject.PartCode);
            }

            // load main specification
            SpecList tempSpecList = limitReader.GetLimit(mainSpecKeys);
            // Get our specification object (so we can initialise modules with appropriate limits)
            progInfo.MainSpec = tempSpecList[0];

            // Manipulate spec
            if (progInfo.MainSpec.Name.Contains("_lp") && dutObject.PartCode.Contains("_lp"))
            {
                engine.GuiShow();
                engine.GuiToFront();
                engine.SendToGui(new GuiMsgs.TcmzEngineeringGuiRequest());

                // Create an 'adjusted' specification
                Specification powerAdjustedSpec = ConstructEngineeringSpec(engine);

                // Apply new spec
                progInfo.MainSpec = powerAdjustedSpec;
            }

            // Create a dummy spec to avoid "All specifications has failed" promotion by Test Engine
            Specification dummySpec = new Specification("DummySpec", new StringDictionary(), 1);

            // declare spec list to Test Engine
            SpecList specList = new SpecList();
            specList.Add(progInfo.MainSpec);
            specList.Add(dummySpec);
            engine.SetSpecificationList(specList);

            progInfo.TestConditions = new TcmzFinalTestConds(progInfo.MainSpec);
        }

        /// <summary>
        /// Initialise utilities
        /// </summary>
        /// <param name="engine"></param>
        protected override void initUtils(ITestEngineInit engine)
        {
            // Initialise DsdbrUtils and Measurements utilities by testkit hardware info.
            switch (progInfo.DsdbrInstrsUsed)
            {
                case DsdbrDriveInstruments.FCUInstruments:
                    DsdbrUtils.FcuInstrumentGroup = progInfo.Instrs.Fcu;
                    Measurements.FCUInstrs = progInfo.Instrs.Fcu;
                    SoaShutterMeasurement.FCUInstrs = progInfo.Instrs.Fcu;
                    break;
                case DsdbrDriveInstruments.PXIInstruments:
                    DsdbrUtils.DsdbrInstrumentGroup = progInfo.Instrs.Dsdbr;
                    Measurements.PXIInstrs = progInfo.Instrs.Dsdbr;
                    Measurements.OpmCgDirect = progInfo.Instrs.OpmCgDirect;
                    Measurements.OpmCgFiltered = progInfo.Instrs.OpmCgFilter;
                    SoaShutterMeasurement.PXIInstrs = progInfo.Instrs.Dsdbr;
                    break;
            }
            Measurements.OSA = progInfo.Instrs.Osa;
            Measurements.Wavemeter = progInfo.Instrs.Wavemeter;
            Measurements.MzHead = progInfo.Instrs.Mz.PowerMeter;
            Measurements.ExternalHead = progInfo.Instrs.OpmReference;
            Measurements.SectionIVSweepRawData = this.SectionIVCurveData.ToArray();
            
            // Initialise TraceToneTest utility by device types
            if (progInfo.TestParamsConfig.GetBoolParam("TraceToneTestEnabled"))
            {
                TraceToneTest.Initialise(
                    progInfo.TestParamsConfig.GetIntParam("TraceToneTest_OrderOfFit"),
                    progInfo.TestConditions.TargetFibrePower_dBm,
                    progInfo.TestConditions.TraceToneTestAgeCond1_dB,
                    progInfo.TestConditions.TraceToneTestAgeCond2_dB,
                    progInfo.TestParamsConfig.GetDoubleParam("TraceToneTest_ModulationIndex"),
                    progInfo.TestParamsConfig.GetDoubleParam("TraceToneTest_SoaStart_mA"),
                    progInfo.TestParamsConfig.GetDoubleParam("TraceToneTest_SoaEnd_mA"),
                    progInfo.TestParamsConfig.GetDoubleParam("TraceToneTest_SoaStep_mA"),
                    progInfo.TestParamsConfig.GetIntParam("TraceToneTest_StepDelay_mS"));
            }
        }

        /// <summary>
        /// Initialise test modules
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        protected override void initModules(ITestEngineInit engine, DUTObject dutObject)
        {
            this.TcmzInitialise_InitModule(engine, dutObject);
            this.DeviceTempControl_InitModule(engine, "DsdbrTemperature", progInfo.Instrs.TecDsdbr, "TecDsdbr");
            this.DeviceTempControl_InitModule(engine, "MzTemperature", progInfo.Instrs.TecMz, "TecMz");
            this.CaseTempControl_InitModule(engine, "CaseTemp_Mid", 25, progInfo.TestConditions.MidTemp);
            this.TcmzPowerUp_InitModule(engine);
            this.TcmzCrossCalibration_InitModule(engine);
            this.TcmzPowerHeadCal_InitModule(engine);
            this.TcmzMzCharacterise_InitModule(engine, dutObject);
            this.TcmzMzItuEstimate_InitModule(engine, dutObject);
            this.TcmzChannelChar_InitModule(engine, dutObject);
            if (progInfo.TestParamsConfig.GetBoolParam("LowTempTestEnabled"))
            {
                this.CaseTempControl_InitModule(engine, "CaseTemp_Low", progInfo.TestConditions.MidTemp, progInfo.TestConditions.LowTemp);
                this.CaseTempControl_InitModule(engine, "CaseTemp_High", progInfo.TestConditions.LowTemp, progInfo.TestConditions.HighTemp);
                this.TcmzLowTempTest_InitModule(engine, dutObject);
            }
            else
            {
                this.CaseTempControl_InitModule(engine, "CaseTemp_High", progInfo.TestConditions.MidTemp, progInfo.TestConditions.HighTemp);
            }
            this.TcmzHighTempTest_InitModule(engine, dutObject);
            if (progInfo.TestParamsConfig.GetBoolParam("TraceToneTestEnabled"))
            {
                this.TcmzTraceToneTest_InitModule(engine, dutObject);
            }
            this.TcmzGroupResults_InitModule(engine, dutObject);
            this.CaseTempControl_InitModule(engine, "CaseTemp_Safe", progInfo.TestConditions.HighTemp, 25);   // TODO - tidier method?
        }

        /// <summary>
        /// Creates an adjusted specifiation for engineering use
        /// </summary>
        /// <param name="engine">reference to test engine</param>
        /// <returns>An adjusted specification</returns>
        private Specification ConstructEngineeringSpec(ITestEngineInit engine)
        {
            GuiMsgs.TcmzEngineeringGuiResponse resp = (GuiMsgs.TcmzEngineeringGuiResponse)engine.ReceiveFromGui().Payload;
            double engAdjustment = (double)resp.FiddleFactor;

            // Calculate power difference
            double upperLimit = Convert.ToDouble(progInfo.MainSpec.GetParamLimit("CH_PEAK_POWER").HighLimit.ValueToString());
            double lowerLimit = Convert.ToDouble(progInfo.MainSpec.GetParamLimit("CH_PEAK_POWER").LowLimit.ValueToString());
            double limitTargetPower = (upperLimit - lowerLimit) / 2 + lowerLimit;
            double adjustment = limitTargetPower - engAdjustment;

            List<String> paramsToAdjust = new List<string>();
            paramsToAdjust.Add("CH_PEAK_POWER");
            paramsToAdjust.Add("CH_PWRCTRL_POWER_MAX_HIGH");
            paramsToAdjust.Add("CH_PWRCTRL_POWER_MAX_LOW");
            paramsToAdjust.Add("CH_PWRCTRL_POWER_MIN_HIGH");
            paramsToAdjust.Add("CH_PWRCTRL_POWER_MIN_LOW");
            paramsToAdjust.Add("TC_FIBRE_TARGET_POWER");
            paramsToAdjust.Add("TC_POWER_CTRL_RANGE_MAX");
            paramsToAdjust.Add("TC_POWER_CTRL_RANGE_MIN");

            Specification powerAdjustedSpec = new Specification(progInfo.MainSpec.Name, progInfo.MainSpec.ActualDatabaseKeys, progInfo.MainSpec.Priority);
            foreach (ParamLimit pl in progInfo.MainSpec)
            {
                Datum highLimit = pl.HighLimit;
                Datum lowLimit = pl.LowLimit;
                if (paramsToAdjust.Contains(pl.InternalName))
                {
                    double oldHighLimit = Convert.ToDouble(pl.HighLimit.ValueToString());
                    double trimmedHighLimit = Math.Truncate(oldHighLimit);
                    if (trimmedHighLimit != 999 && trimmedHighLimit != 9999)
                    {
                        DatumDouble newHighLimit = new DatumDouble(pl.InternalName, oldHighLimit - adjustment);
                        highLimit = (Datum)newHighLimit;
                    }

                    double oldLowLimit = Convert.ToDouble(pl.LowLimit.ValueToString());
                    double trimmedLowLimit = Math.Truncate(oldLowLimit);
                    if (trimmedLowLimit != -999 && trimmedLowLimit != -9999)
                    {
                        DatumDouble newLowLimit = new DatumDouble(pl.InternalName, oldLowLimit - adjustment);
                        lowLimit = (Datum)newLowLimit;
                    }
                }
                RawParamLimit rpl = new RawParamLimit(pl.InternalName, pl.ParamType, lowLimit, highLimit, pl.Operand, pl.Priority, pl.AccuracyFactor, pl.Units);
                powerAdjustedSpec.Add(rpl);
            }
            return powerAdjustedSpec;
        }

        #region Initialise modules
        /// <summary>
        /// Configure Initialise Device module
        /// </summary>
        private void TcmzInitialise_InitModule(ITestEngineInit engine, DUTObject dutObject)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("TcmzInitialise", "TcmzInitialise", "");

            // Add instruments
            modRun.Instrs.Add("Osa", (Instrument)progInfo.Instrs.Osa);
            modRun.ConfigData.AddReference("MzInstruments", progInfo.Instrs.Mz);
            switch (progInfo.DsdbrInstrsUsed)
            {
                case DsdbrDriveInstruments.FCUInstruments:
                    modRun.ConfigData.AddReference("FcuInstruments", progInfo.Instrs.Fcu);
                    break;
                case DsdbrDriveInstruments.PXIInstruments:
                    modRun.Instrs.Add("SoaRelays", progInfo.Instrs.DsdbrSoaRelays);
                    break;
            }

            // Add config data
            TcmzInitialise_Config tcmzInitConfig = new TcmzInitialise_Config();
            DatumList datumsRequired = tcmzInitConfig.GetDatumList();
            ExtractAndAddDatums(modRun, datumsRequired);
            modRun.ConfigData.AddSint32("NumChans", progInfo.TestConditions.NbrChannels);
            modRun.ConfigData.AddDouble("FreqLow_GHz", progInfo.TestConditions.ItuLowFreq_GHz);
            modRun.ConfigData.AddDouble("FreqHigh_GHz", progInfo.TestConditions.ItuHighFreq_GHz);
            modRun.ConfigData.AddDouble("FreqSpace_GHz", progInfo.TestConditions.ItuSpacing_GHz);
            modRun.ConfigData.AddString("DutSerialNbr", dutObject.SerialNumber);
            modRun.ConfigData.AddString("MzFileDirectory", progInfo.TestParamsConfig.GetStringParam("MzFileDirectory"));
            modRun.ConfigData.AddEnum("DsdbrInstrsGroup", progInfo.DsdbrInstrsUsed);
            switch (progInfo.DsdbrInstrsUsed)
            {
                case DsdbrDriveInstruments.FCUInstruments:
                    break;
                case DsdbrDriveInstruments.PXIInstruments:
                    string closeGridRegKey = this.progInfo.TestParamsConfig.GetStringParam("CloseGridRegKey");
                    modRun.ConfigData.AddString("CloseGridRegKey", closeGridRegKey);
                    break;
            }
        }

        /// <summary>
        /// Configure a Device Temperature Control module
        /// </summary>
        private void DeviceTempControl_InitModule(ITestEngineInit engine, string moduleName, IInstType_SimpleTempControl tecController, string configPrefix)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun(moduleName, "SimpleTempControl", "");

            // Add instrument
            modRun.Instrs.Add("Controller", (Instrument)tecController);

            //load config
            double timeout_S = this.progInfo.TestParamsConfig.GetDoubleParam(configPrefix + "_Timeout_S");
            double stabTime_S = this.progInfo.TestParamsConfig.GetDoubleParam(configPrefix + "_StabiliseTime_S");
            int updateTime_mS = this.progInfo.TestParamsConfig.GetIntParam(string.Format("{0}_UpdateTime_mS", configPrefix));
            double sensorSetPoint_C = this.progInfo.TestParamsConfig.GetDoubleParam(string.Format("{0}_SensorTemperatureSetPoint_C", configPrefix));
            double tolerance_C = this.progInfo.TestParamsConfig.GetDoubleParam(string.Format("{0}_Tolerance_C", configPrefix));

            // Add config data
            DatumList tempConfig = new DatumList();
            tempConfig.AddDouble("datumMaxExpectedTimeForOperation_s", timeout_S);
            tempConfig.AddDouble("RqdStabilisationTime_s", stabTime_S);
            tempConfig.AddSint32("TempTimeBtwReadings_ms", updateTime_mS);
            tempConfig.AddDouble("SetPointTemperature_C", sensorSetPoint_C);
            tempConfig.AddDouble("TemperatureTolerance_C", tolerance_C);

            modRun.ConfigData.AddListItems(tempConfig);
        }

        /// <summary>
        /// Configure Case Temperature module
        /// </summary>
        private void CaseTempControl_InitModule(ITestEngineInit engine, string moduleName, Double currentTemp, Double tempSetPoint)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun(moduleName, "SimpleTempControl", "");

            // Add instrument
            modRun.Instrs.Add("Controller", (Instrument)progInfo.Instrs.TecCase);

            // Add config data
            TempTablePoint tempCal = progInfo.TempConfig.GetTemperatureCal(currentTemp, tempSetPoint)[0];
            DatumList tempConfig = new DatumList();
            int guiTimeOut_ms = this.progInfo.TestParamsConfig.GetIntParam("CaseTempGui_UpdateTime_mS");
            // Timeout = 2 * calibrated time, minimum of 60secs
            double timeout_s = Math.Max(tempCal.DelayTime_s * 15, 60);
            tempConfig.AddDouble("datumMaxExpectedTimeForOperation_s", timeout_s);
            // Stabilisation time = 0.5 * calibrated time
            tempConfig.AddDouble("RqdStabilisationTime_s", tempCal.DelayTime_s);
            tempConfig.AddSint32("TempTimeBtwReadings_ms", guiTimeOut_ms);
            // Actual temperature to set
            tempConfig.AddDouble("SetPointTemperature_C", tempSetPoint + tempCal.OffsetC);
            tempConfig.AddDouble("TemperatureTolerance_C", tempCal.Tolerance_degC);
            modRun.ConfigData.AddListItems(tempConfig);
        }

        /// <summary>
        /// Initialise TcmzPowerUp module
        /// </summary>
        /// <param name="engine"></param>
        private void TcmzPowerUp_InitModule(ITestEngineInit engine)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("TcmzPowerUp", "TcmzPowerUp", "");

            // Add instruments
            modRun.Instrs.Add("OpticalPowerMeter", (Instrument)progInfo.Instrs.Mz.PowerMeter);
            modRun.ConfigData.AddReference("MzInstruments", progInfo.Instrs.Mz);

            // Add config data
            modRun.ConfigData.AddDouble("MzTapBias_V", progInfo.TestParamsConfig.GetDoubleParam("MzTapBias_V"));
            modRun.ConfigData.AddDouble("LockerBias_V", progInfo.TestParamsConfig.GetDoubleParam("LockerBias_V"));
        }

        /// <summary>
        /// Initialise TcmzCrossCalibration module
        /// </summary>
        /// <param name="engine"></param>
        private void TcmzCrossCalibration_InitModule(ITestEngineInit engine)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("TcmzCrossCalibration", "TcmzCrossCalibration", "");

            // Add instruments

            // Add config data
            modRun.ConfigData.AddSint32("OpticalCal_delay_ms", 2000);

            // Tie up to limits
            modRun.Limits.AddParameter(progInfo.MainSpec, "XCAL_CH_LO_MAP_GB", "XCAL_CH_LO");
            modRun.Limits.AddParameter(progInfo.MainSpec, "XCAL_CH_MID_MAP_GB", "XCAL_CH_MID");
            modRun.Limits.AddParameter(progInfo.MainSpec, "XCAL_CH_HI_MAP_GB", "XCAL_CH_HI");
        }

        /// <summary>
        /// Initialise TcmzPowerHeadCal module
        /// </summary>
        /// <param name="engine"></param>
        private void TcmzPowerHeadCal_InitModule(ITestEngineInit engine)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("TcmzPowerHeadCal", "TcmzPowerHeadCal", "");

            // Add instruments
            modRun.Instrs.Add("OpmReference", (Instrument)progInfo.Instrs.OpmReference);
            modRun.Instrs.Add("Wavemeter", (Instrument)progInfo.Instrs.Wavemeter);
            modRun.Instrs.Add("OpmMZ", (Instrument)progInfo.Instrs.Mz.PowerMeter);
            switch (progInfo.DsdbrInstrsUsed)
            {
                case DsdbrDriveInstruments.FCUInstruments:
                    break;
                case DsdbrDriveInstruments.PXIInstruments:
                    modRun.Instrs.Add("OpmCgDirect", (Instrument)progInfo.Instrs.OpmCgDirect);
                    modRun.Instrs.Add("OpmCgFilter", (Instrument)progInfo.Instrs.OpmCgFilter);
                    break;
            }

            // Add any config data here ...
            modRun.ConfigData.AddEnum("TestStage", progInfo.TestStage);
            modRun.ConfigData.AddSint32("OpticalCal_delay_ms", 2000);
            modRun.ConfigData.AddDouble("OpticalCal_MZ_Max", progInfo.TestParamsConfig.GetDoubleParam("OpticalCal_MZ_Max"));
            modRun.ConfigData.AddDouble("OpticalCal_MZ_Min", progInfo.TestParamsConfig.GetDoubleParam("OpticalCal_MZ_Min"));
            modRun.ConfigData.AddString("PlotFileDirectory", progInfo.TestParamsConfig.GetStringParam("DSDBRFileDirectory"));
            switch (progInfo.DsdbrInstrsUsed)
            {
                case DsdbrDriveInstruments.FCUInstruments:
                    modRun.ConfigData.AddBool("IsCalPXIT", false);
                    break;
                case DsdbrDriveInstruments.PXIInstruments:
                    modRun.ConfigData.AddBool("IsCalPXIT", true);
                    modRun.ConfigData.AddDouble("OpticalCal_CgDirect_Max", progInfo.TestParamsConfig.GetDoubleParam("OpticalCal_CgDirect_Max"));
                    modRun.ConfigData.AddDouble("OpticalCal_CgDirect_Min", progInfo.TestParamsConfig.GetDoubleParam("OpticalCal_CgDirect_Min"));
                    break;
            }
        }

        /// <summary>
        /// Initialise TcmzMzCharacterise module
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        private void TcmzMzCharacterise_InitModule(ITestEngineInit engine, DUTObject dutObject)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("TcmzMzCharacterise", "TcmzMzCharacterise_ND", "");

            // Add instruments
            modRun.ConfigData.AddReference("MzInstruments", progInfo.Instrs.Mz);

            TcmzMzSweep_Config tcmzConfig = new TcmzMzSweep_Config();
            DatumList datumsRequired = tcmzConfig.GetDatumList();
            ExtractAndAddDatums(modRun, datumsRequired);

            double mzBias1 = progInfo.TestParamsConfig.GetDoubleParam("MzFixedModBias_1_V");
            double mzBias2 = progInfo.TestParamsConfig.GetDoubleParam("MzFixedModBias_2_V");

            //START MODIFICATION, 2007.09.26, BY Ken.Wu ( TCMZ Vpi Correction )
            // Read values needed for TCMZ Vpi calculation from config file.
            double tuneISOAPowerSlope
                = progInfo.TestParamsConfig.GetDoubleParam(externalKeyTuneISOAPowerSlope);

            double tuneOpticalPowerToleranceInmW
                = progInfo.TestParamsConfig.GetDoubleParam(externalKeyTuneOpticalPowerToleranceInmW);

            double tuneOpticalPowerToleranceIndB
                = progInfo.TestParamsConfig.GetDoubleParam(externalKeyTuneOpticalPowerToleranceIndB);

            modRun.ConfigData.AddDoubleArray("MZFixedModBias_volt", new double[] { mzBias1, mzBias2 });

            modRun.ConfigData.AddDouble("TuneISOAPowerSlope", tuneISOAPowerSlope);
            modRun.ConfigData.AddDouble("TuneOpticalPowerToleranceInmW", tuneOpticalPowerToleranceInmW);
            modRun.ConfigData.AddDouble("TargetFibrePower_dBm", progInfo.TestConditions.TargetFibrePower_dBm);
            //END MODIFICATION, 2007.09.26, By Ken.Wu

            modRun.ConfigData.AddSint32("NumberOfPoints", progInfo.TestParamsConfig.GetIntParam("MzSweepNumberOfPoints"));
            modRun.ConfigData.AddString("DutSerialNbr", dutObject.SerialNumber);
            modRun.ConfigData.AddReference("TestSpec", progInfo.MainSpec);
#warning Add MZTapBias_V to config class
            modRun.ConfigData.AddDouble("MZInlineTapBias_V", modRun.ConfigData.ReadDouble("MZTapBias_V"));
            // Limits
            modRun.Limits.AddParameter(progInfo.MainSpec, "MZ_INITIAL_CAL_SWEEP_FILE", "MzSweepDataZipFile");
            modRun.Limits.AddParameter(progInfo.MainSpec, "MZ_CAL_EST_FILE", "MzSweepDataResultsFile");
            modRun.Limits.AddParameter(progInfo.MainSpec, "MZ_CAL_EST_FLAG", "AllSweepsPassed");
        }

        /// <summary>
        /// Initialise TcmzMzItuEstimate module
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        private void TcmzMzItuEstimate_InitModule(ITestEngineInit engine, DUTObject dutObject)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("TcmzMzItuEstimate", "TcmzMzItuEstimate_ND", "");

            // Add instruments
            modRun.ConfigData.AddReference("MzInstruments", progInfo.Instrs.Mz);

            // Add config data
            modRun.ConfigData.AddReference("Specification", progInfo.MainSpec);
            modRun.ConfigData.AddDouble("CalOffset", progInfo.TestConditions.CalOffset);
            modRun.ConfigData.AddDouble("HighTemp", progInfo.TestConditions.HighTemp);
            modRun.ConfigData.AddDouble("LowTemp", progInfo.TestConditions.LowTemp);
            modRun.ConfigData.AddDouble("FreqLow_GHz", progInfo.TestConditions.ItuLowFreq_GHz);
            modRun.ConfigData.AddDouble("FreqHigh_GHz", progInfo.TestConditions.ItuHighFreq_GHz);
            modRun.ConfigData.AddDouble("FreqSpace_GHz", progInfo.TestConditions.ItuSpacing_GHz);
            modRun.ConfigData.AddDouble("MzCtrlINominal_mA", progInfo.TestParamsConfig.GetDoubleParam("MzCtrlINominal_mA"));
            modRun.ConfigData.AddString("DUTSerialNbr", dutObject.SerialNumber);
            modRun.ConfigData.AddString("MzFileDirectory", progInfo.TestParamsConfig.GetStringParam("MzFileDirectory"));
            // for Vpi correction
            double VpiCorrectionUpperAdjustDelta = progInfo.TestParamsConfig.GetDoubleParam("VpiCorrectionUpperAdjustDelta");
            double VpiCorrectionLowerAdjustDelta = progInfo.TestParamsConfig.GetDoubleParam("VpiCorrectionLowerAdjustDelta");
            modRun.ConfigData.AddDouble("VpiCorrectionUpperAdjustDelta", VpiCorrectionUpperAdjustDelta);
            modRun.ConfigData.AddDouble("VpiCorrectionLowerAdjustDelta", VpiCorrectionLowerAdjustDelta);

            // Add limits
            modRun.Limits.AddParameter(progInfo.MainSpec, "MZ_CAL_EST_FILE_FINAL_VCM", "MzAtNewVcmResultsFile");
            modRun.Limits.AddParameter(progInfo.MainSpec, "MZ_CAL_EST_FLAG_FINAL_VCM", "MzAtNewVcmPass");
        }

        /// <summary>
        /// Configure Channel Characterisation module
        /// </summary>
        private void TcmzChannelChar_InitModule(ITestEngineInit engine, DUTObject dutObject)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("TcmzChannelChar", "TcmzChannelChar_ND", "");
            modRun.PreviousTestData.AddString("LASER_WAFER_SIZE", LASER_WAFER_SIZE == null ? string.Empty : LASER_WAFER_SIZE);
            modRun.PreviousTestData.AddStringArray("SmFiles", SmFiles);
            modRun.PreviousTestData.AddString("BandType", progInfo.TestConditions.FreqBand.ToString());
            modRun.PreviousTestData.AddStringArray("NUM_LM", NUM_LM);

            // Add instruments
            modRun.ConfigData.AddReference("MzInstruments", progInfo.Instrs.Mz);
            modRun.Instrs.Add("OSA", (Instrument)progInfo.Instrs.Osa);
            modRun.Instrs.Add("MzTec", (Instrument)progInfo.Instrs.TecMz);
            modRun.Instrs.Add("DsdbrTec", (Instrument)progInfo.Instrs.TecDsdbr);
            switch (progInfo.DsdbrInstrsUsed)
            {
                case DsdbrDriveInstruments.FCUInstruments:
                    modRun.Instrs.Add("PowerHead", (Instrument)progInfo.Instrs.Mz.PowerMeter);
                    break;
                case DsdbrDriveInstruments.PXIInstruments:
                    modRun.Instrs.Add("PowerHead", (Instrument)progInfo.Instrs.OpmCgDirect);
                    break;
            }

            // Add config data
            TcmzChannelChar_ND_Config tcmzChanCharConfig = new TcmzChannelChar_ND_Config();
            DatumList datumsRequired = tcmzChanCharConfig.GetDatumList();
            ExtractAndAddDatums(modRun, datumsRequired);

            TcmzMzSweep_Config tcmzConfig = new TcmzMzSweep_Config();
            datumsRequired = tcmzConfig.GetDatumList();
            ExtractAndAddDatums(modRun, datumsRequired);

            modRun.ConfigData.AddReference("SwitchOsaMzOpm", progInfo.Instrs.SwitchOsaMzOpm);
            modRun.ConfigData.AddReference("TestSelect", progInfo.TestSelect);
            modRun.ConfigData.AddReference("Specification", progInfo.MainSpec);
            modRun.ConfigData.AddReference("FreqBand", progInfo.TestConditions.FreqBand);
            modRun.ConfigData.AddDouble("FreqLow_GHz", progInfo.TestConditions.ItuLowFreq_GHz);
            modRun.ConfigData.AddDouble("FreqHigh_GHz", progInfo.TestConditions.ItuHighFreq_GHz);
            modRun.ConfigData.AddDouble("FreqSpace_GHz", progInfo.TestConditions.ItuSpacing_GHz);
            modRun.ConfigData.AddSint32("NumChans", progInfo.TestConditions.NbrChannels);
            modRun.ConfigData.AddDouble("CaseTempLow_C", progInfo.TestConditions.LowTemp);
            modRun.ConfigData.AddDouble("CaseTempHigh_C", progInfo.TestConditions.HighTemp);
            modRun.ConfigData.AddDouble("TargetFibrePower_dBm", progInfo.TestConditions.TargetFibrePower_dBm);
            modRun.ConfigData.AddDouble("CalOffset", progInfo.TestConditions.CalOffset);
            modRun.ConfigData.AddString("DutSerialNbr", dutObject.SerialNumber);
            modRun.ConfigData.AddString("DeviceType", dutObject.PartCode);
            modRun.ConfigData.AddDouble("MzDcVpiSlopeLimitMin", progInfo.TestConditions.MzDcVpiSlopeLimitMin);
            modRun.ConfigData.AddDouble("MzDcVpiSlopeLimitMax", progInfo.TestConditions.MzDcVpiSlopeLimitMax);
            modRun.ConfigData.AddDouble("LockerSlopeEffAbsLimitMin", progInfo.TestConditions.LockerSlopeEffAbsLimitMin);
            modRun.ConfigData.AddDouble("LockerSlopeEffAbsLimitMax", progInfo.TestConditions.LockerSlopeEffAbsLimitMax);
            bool loggingEnabled = true;
            try
            {
                loggingEnabled = this.progInfo.TestParamsConfig.GetBoolParam("LoggintEnabled");
            }
            catch (Exception /*e*/ ) { }
            modRun.ConfigData.AddBool("LoggingEnabled", loggingEnabled);

#warning Add to config
            modRun.ConfigData.AddBool("AlwaysTuneLaser", false);
            modRun.ConfigData.AddString("IPhaseModAcqFilesCollection", this._mapResult.ReadFileLinkFullPath("CG_PHASE_MODE_ACQ_FILE"));

            // Tie up limits
            Specification spec = progInfo.MainSpec;
            modRun.Limits.AddParameter(spec, "TCASE_MID_PASS_FAIL_FLAG", "PassFailFlag");
            modRun.Limits.AddParameter(spec, "NUM_CHAN_TEST_TCASE_MID", "NumChannelsTested");
            modRun.Limits.AddParameter(spec, "NUM_CHAN_PASS_TCASE_MID", "NumChannelsPass");
            modRun.Limits.AddParameter(spec, "CHANNEL_RAW_DATA_FILE", "ChannelPlotData");
            modRun.Limits.AddParameter(spec, "FULL_LV_SWEEP_FLAG", "FullLvSweepFlag");
            modRun.Limits.AddParameter(spec, "FULL_LK_SLOPE_FLAG", "FullLkSlopeTest");
        }

        /// <summary>
        /// Initialise TcmzLowTempTest module
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        private void TcmzLowTempTest_InitModule(ITestEngineInit engine, DUTObject dutObject)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("TcmzLowTempTest", "TcmzTempTest_ND", "");

            // Add instruments
            modRun.ConfigData.AddReference("MzInstruments", progInfo.Instrs.Mz);
            modRun.Instrs.Add("MzTec", (Instrument)progInfo.Instrs.TecMz);
            modRun.Instrs.Add("DsdbrTec", (Instrument)progInfo.Instrs.TecDsdbr);
            modRun.ConfigData.AddReference("SwitchOsaMzOpm", progInfo.Instrs.SwitchOsaMzOpm);

            // Add config data
            TcmzMzSweep_Config tcmzConfig = new TcmzMzSweep_Config();
            DatumList datumsRequired = tcmzConfig.GetDatumList();
            ExtractAndAddDatums(modRun, datumsRequired);
            modRun.ConfigData.AddEnum("Temp", TcmzTestTemp.Low);
            modRun.ConfigData.AddReference("TestSelect", progInfo.TestSelect);
            modRun.ConfigData.AddString("DutSerialNbr", dutObject.SerialNumber);
            //Modify non-rules old statment at 2008-10-10 by tim;
            //modRun.ConfigData.AddBool("DoPwrCtrlRangeTests", dutObject.PartCode.ToLower().Contains("lmt10ncc"));
            modRun.ConfigData.AddBool("DoPwrCtrlRangeTests", progInfo.TestParamsConfig.GetBoolParam("DoPwrCtrlRangeTests"));
            //Modify non-ruless old statment at 2008-10-10;
            //modRun.ConfigData.AddBool("DoTapLevelling", !dutObject.PartCode.ToLower().Contains("lmt10ncb"));
            modRun.ConfigData.AddBool("DoTapLevelling",progInfo.TestParamsConfig.GetBoolParam("DoTapLevelling"));
            modRun.ConfigData.AddDouble("TapPhotoCurrentToleranceRatio",
            progInfo.TestParamsConfig.GetDoubleParam("TapPhotoCurrentToleranceRatio"));
            //modRun.ConfigData.AddDouble("MZModSweepMax_volt", progInfo.TestParamsConfig.GetDoubleParam("MzSeModSweepMax_volt"));
            //modRun.ConfigData.AddDouble("MZFixedModBias_volt", progInfo.TestParamsConfig.GetDoubleParam("MzSeFixedModBias_volt"));
            modRun.ConfigData.AddDouble("PwrControlRangeLow", progInfo.TestConditions.PwrControlRangeLow);
            modRun.ConfigData.AddDouble("PwrControlRangeHigh", progInfo.TestConditions.PwrControlRangeHigh);
            modRun.ConfigData.AddDouble("Tune_OpticalPowerTolerance_dB", progInfo.TestParamsConfig.GetDoubleParam("Tune_OpticalPowerTolerance_dB"));
            modRun.ConfigData.AddDouble("Tune_OpticalPowerTolerance_mW", progInfo.TestParamsConfig.GetDoubleParam("Tune_OpticalPowerTolerance_mW"));
            modRun.ConfigData.AddDouble("Tune_IsoaPowerSlope", progInfo.TestParamsConfig.GetDoubleParam("Tune_IsoaPowerSlope"));
            bool loggingEnabled = true;
            try
            {
                loggingEnabled = this.progInfo.TestParamsConfig.GetBoolParam("LoggintEnabled");
            }
            catch (Exception /*e*/ ) { }
            modRun.ConfigData.AddBool("LoggingEnabled", loggingEnabled);
            modRun.ConfigData.AddBool("TraceToneTestEnabled", progInfo.TestParamsConfig.GetBoolParam("TraceToneTestEnabled"));

            // Tie up limits
            Specification spec = progInfo.MainSpec;
            modRun.Limits.AddParameter(spec, "LOW_DISS_OPTICAL_FREQ", "LOW_DISS_OPTICAL_FREQ");
            modRun.Limits.AddParameter(spec, "LOW_DISS_SOL", "LOW_DISS_SOL");
            modRun.Limits.AddParameter(spec, "PACK_DISS_TCASE_LOW", "PACK_DISS_TCASE_LOW");
            modRun.Limits.AddParameter(spec, "LASER_TEC_I_TCASE_LOW", "LASER_TEC_I_TCASE_LOW");
            modRun.Limits.AddParameter(spec, "LASER_TEC_V_TCASE_LOW", "LASER_TEC_V_TCASE_LOW");
            modRun.Limits.AddParameter(spec, "MZ_TEC_I_TCASE_LOW", "MZ_TEC_I_TCASE_LOW");
            modRun.Limits.AddParameter(spec, "MZ_TEC_V_TCASE_LOW", "MZ_TEC_V_TCASE_LOW");
            modRun.Limits.AddParameter(spec, "NUM_CHAN_TEST_TCASE_LOW", "NumChannelsTested");
            modRun.Limits.AddParameter(spec, "NUM_CHAN_PASS_TCASE_LOW", "NumChannelsPass");
            modRun.Limits.AddParameter(spec, "NUM_LOCK_FAIL_TCASE_LOW", "NumLockFails");
            modRun.Limits.AddParameter(spec, "TCASE_LOW_PASS_FAIL_FLAG", "PassFailFlag");
        }

        /// <summary>
        /// Initialise TcmHighTempTest module
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        private void TcmzHighTempTest_InitModule(ITestEngineInit engine, DUTObject dutObject)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("TcmzHighTempTest", "TcmzTempTest_ND", "");

            // Add instruments
            modRun.ConfigData.AddReference("MzInstruments", progInfo.Instrs.Mz);
            modRun.Instrs.Add("MzTec", (Instrument)progInfo.Instrs.TecMz);
            modRun.Instrs.Add("DsdbrTec", (Instrument)progInfo.Instrs.TecDsdbr);
            modRun.ConfigData.AddReference("SwitchOsaMzOpm", progInfo.Instrs.SwitchOsaMzOpm);

            // Add config data
            TcmzMzSweep_Config tcmzConfig = new TcmzMzSweep_Config();
            DatumList datumsRequired = tcmzConfig.GetDatumList();
            ExtractAndAddDatums(modRun, datumsRequired);
            modRun.ConfigData.AddEnum("Temp", TcmzTestTemp.High);
            modRun.ConfigData.AddReference("TestSelect", progInfo.TestSelect);
            modRun.ConfigData.AddString("DutSerialNbr", dutObject.SerialNumber);
            //modRun.ConfigData.AddBool("DoPwrCtrlRangeTests", dutObject.PartCode.ToLower().Contains("lmt10ncc"));
            modRun.ConfigData.AddBool("DoPwrCtrlRangeTests", progInfo.TestParamsConfig.GetBoolParam("DoPwrCtrlRangeTests"));
            //Modify non-rules old statment;
            //modRun.ConfigData.AddBool("DoTapLevelling", !dutObject.PartCode.ToLower().Contains("lmt10ncb"));
            modRun.ConfigData.AddBool("DoTapLevelling",progInfo.TestParamsConfig.GetBoolParam("DoTapLevelling"));
            modRun.ConfigData.AddDouble("TapPhotoCurrentToleranceRatio",
                progInfo.TestParamsConfig.GetDoubleParam("TapPhotoCurrentToleranceRatio"));
            modRun.ConfigData.AddDouble("LaserTecEolDeltaT_C", progInfo.TestConditions.LaserTecEolDeltaT_C);
            modRun.ConfigData.AddDouble("MzTecEolDeltaT_C", progInfo.TestConditions.MzTecEolDeltaT_C);
            modRun.ConfigData.AddDouble("PwrControlRangeLow", Convert.ToDouble(progInfo.MainSpec.GetParamLimit("TC_PWR_CTRL_RANGE_MIN").LowLimit.ValueToString()));
            modRun.ConfigData.AddDouble("PwrControlRangeHigh", Convert.ToDouble(progInfo.MainSpec.GetParamLimit("TC_PWR_CTRL_RANGE_MAX").LowLimit.ValueToString()));
            modRun.ConfigData.AddDouble("Tune_OpticalPowerTolerance_dB", progInfo.TestParamsConfig.GetDoubleParam("Tune_OpticalPowerTolerance_dB"));
            modRun.ConfigData.AddDouble("Tune_OpticalPowerTolerance_mW", progInfo.TestParamsConfig.GetDoubleParam("Tune_OpticalPowerTolerance_mW"));
            modRun.ConfigData.AddDouble("Tune_IsoaPowerSlope", progInfo.TestParamsConfig.GetDoubleParam("Tune_IsoaPowerSlope"));
            bool loggingEnabled = true;
            try
            {
                loggingEnabled = this.progInfo.TestParamsConfig.GetBoolParam("LoggintEnabled");
            }
            catch (Exception /*e*/ ) { }
            modRun.ConfigData.AddBool("LoggingEnabled", loggingEnabled);
            modRun.ConfigData.AddBool("TraceToneTestEnabled", progInfo.TestParamsConfig.GetBoolParam("TraceToneTestEnabled"));

            // Tie up limits
            Specification spec = progInfo.MainSpec;
            modRun.Limits.AddParameter(spec, "HIGH_DISS_OPTICAL_FREQ", "HIGH_DISS_OPTICAL_FREQ");
            modRun.Limits.AddParameter(spec, "HIGH_DISS_SOL", "HIGH_DISS_SOL");
            modRun.Limits.AddParameter(spec, "PACK_DISS_TCASE_HIGH", "PACK_DISS_TCASE_HIGH");
            modRun.Limits.AddParameter(spec, "LASER_TEC_I_TCASE_HIGH", "LASER_TEC_I_TCASE_HIGH");
            modRun.Limits.AddParameter(spec, "LASER_TEC_V_TCASE_HIGH", "LASER_TEC_V_TCASE_HIGH");
            modRun.Limits.AddParameter(spec, "MZ_TEC_I_TCASE_HIGH", "MZ_TEC_I_TCASE_HIGH");
            modRun.Limits.AddParameter(spec, "MZ_TEC_V_TCASE_HIGH", "MZ_TEC_V_TCASE_HIGH");
            modRun.Limits.AddParameter(spec, "LASER_TEC_I_TCASE_HIGH_EOL", "LASER_TEC_I_TCASE_HIGH_EOL");
            modRun.Limits.AddParameter(spec, "LASER_TEC_V_TCASE_HIGH_EOL", "LASER_TEC_V_TCASE_HIGH_EOL");
            modRun.Limits.AddParameter(spec, "MZ_TEC_I_TCASE_HIGH_EOL", "MZ_TEC_I_TCASE_HIGH_EOL");
            modRun.Limits.AddParameter(spec, "MZ_TEC_V_TCASE_HIGH_EOL", "MZ_TEC_V_TCASE_HIGH_EOL");
            modRun.Limits.AddParameter(spec, "PACK_DISS_TCASE_HIGH_EOL", "PACK_DISS_TCASE_HIGH_EOL");
            modRun.Limits.AddParameter(spec, "NUM_CHAN_TEST_TCASE_HIGH", "NumChannelsTested");
            modRun.Limits.AddParameter(spec, "NUM_CHAN_PASS_TCASE_HIGH", "NumChannelsPass");
            modRun.Limits.AddParameter(spec, "NUM_LOCK_FAIL_TCASE_HIGH", "NumLockFails");
            modRun.Limits.AddParameter(spec, "TCASE_HIGH_PASS_FAIL_FLAG", "PassFailFlag");
        }

        /// <summary>
        /// Initialise TcmzTraceToneTest module
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        private void TcmzTraceToneTest_InitModule(ITestEngineInit engine, DUTObject dutObject)
        {
            // Initialise module
            ModuleRun modRun = engine.AddModuleRun("TcmzTraceToneTest", "TcmzTraceToneTest", "");
            modRun.ConfigData.AddString("DutSerialNumber", dutObject.SerialNumber);
            modRun.ConfigData.AddString("PlotFileDirectory", progInfo.TestParamsConfig.GetStringParam("MzFileDirectory"));

            // Tie up limits when added to spec
            modRun.Limits.AddParameter(progInfo.MainSpec, "I_SOA_1", "I_Soa_1");
            modRun.Limits.AddParameter(progInfo.MainSpec, "MAX_I_SOA_TRACE_SOLOT", "Max_I_Soa_Trace_SOLOT");
            modRun.Limits.AddParameter(progInfo.MainSpec, "MAX_TRACE_I_SOLOT", "Max_Trace_I_SOLOT");
            modRun.Limits.AddParameter(progInfo.MainSpec, "SOA_GRADIENT_SOLOT", "Soa_Gradient_SOLOT");
            modRun.Limits.AddParameter(progInfo.MainSpec, "I_SOA_TRACE_EOLOT1", "I_Soa_Trace_EOLOT1");
            modRun.Limits.AddParameter(progInfo.MainSpec, "TRACE_I_EOLOT1", "Trace_I_EOLOT1");
            modRun.Limits.AddParameter(progInfo.MainSpec, "SOA_GRADIENT_EOLOT1", "Soa_Gradient_EOLOT1");
            modRun.Limits.AddParameter(progInfo.MainSpec, "I_SOA_TRACE_EOLOT2", "I_Soa_Trace_EOLOT2");
            modRun.Limits.AddParameter(progInfo.MainSpec, "TRACE_I_EOLOT2", "Trace_I_EOLOT2");
            modRun.Limits.AddParameter(progInfo.MainSpec, "SOA_GRADIENT_EOLOT2", "Soa_Gradient_EOLOT2");
            modRun.Limits.AddParameter(progInfo.MainSpec, "TRACE_SOA_PLOT", "TraceSoaCurves");
        }

        /// <summary>
        /// Initialise TcmzGroupResults module
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        private void TcmzGroupResults_InitModule(ITestEngineInit engine, DUTObject dutObject)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("TcmzGroupResults", "TcmzGroupResults", "");

            // Add config data
            modRun.ConfigData.AddBool("LowTempTestEnabled", progInfo.TestParamsConfig.GetBoolParam("LowTempTestEnabled"));

            // Tie up limits
            Specification spec = progInfo.MainSpec;
            modRun.Limits.AddParameter(spec, "AVERAGE_LOCK_RATIO", "AVERAGE_LOCK_RATIO");
            modRun.Limits.AddParameter(spec, "OPTICAL_FREQ_FIRST_CHAN_FAIL", "OPTICAL_FREQ_FIRST_CHAN_FAIL");
            modRun.Limits.AddParameter(spec, "FULL_PASS_CHAN_COUNT_OVERALL", "FULL_PASS_CHAN_COUNT_OVERALL");

            modRun.Limits.AddParameter(spec, "FIBRE_POWER_CHAN_TEMP_MIN", "FIBRE_POWER_CHAN_TEMP_MIN");
            modRun.Limits.AddParameter(spec, "FIBRE_POWER_CHAN_TEMP_MAX", "FIBRE_POWER_CHAN_TEMP_MAX");
            modRun.Limits.AddParameter(spec, "FIBRE_POWER_CHANGE_TCASE_HIGH", "FIBRE_POWER_CHANGE_TCASE_HIGH");
            modRun.Limits.AddParameter(spec, "FIBRE_POWER_FAIL_CHAN_TEMP", "FIBRE_POWER_FAIL_CHAN_TEMP");
            modRun.Limits.AddParameter(spec, "FIBRE_POWER_FAIL_OVER_TCASE", "FIBRE_POWER_FAIL_OVER_TCASE");
            modRun.Limits.AddParameter(spec, "TRACK_ERROR_PWR_TCASE", "TRACK_ERROR_PWR_TCASE");

            modRun.Limits.AddParameter(spec, "LOCK_FREQ_CHAN_TEMP_MIN", "LOCK_FREQ_CHAN_TEMP_MIN");
            modRun.Limits.AddParameter(spec, "LOCK_FREQ_CHAN_TEMP_MAX", "LOCK_FREQ_CHAN_TEMP_MAX");
            modRun.Limits.AddParameter(spec, "LOCK_FREQ_CHANGE_TCASE_HIGH", "LOCK_FREQ_CHANGE_TCASE_HIGH");
            modRun.Limits.AddParameter(spec, "LOCK_FREQ_FAIL_CHAN_TEMP", "LOCK_FREQ_FAIL_CHAN_TEMP");
            modRun.Limits.AddParameter(spec, "LOCK_FREQ_FAIL_OVER_TCASE", "LOCK_FREQ_FAIL_OVER_TCASE");
            modRun.Limits.AddParameter(spec, "TRACK_ERROR_FREQ_TCASE", "TRACK_ERROR_FREQ_TCASE");
            modRun.Limits.AddParameter(spec, "TRACK_ERROR_ULFREQ_TCASE", "TRACK_ERROR_ULFREQ_TCASE");

            modRun.Limits.AddParameter(spec, "PHASE_I_CHANGE_TCASE_HIGH", "PHASE_I_CHANGE_TCASE_HIGH");

            if (progInfo.TestParamsConfig.GetBoolParam("LowTempTestEnabled"))
            {
                modRun.Limits.AddParameter(spec, "FIBRE_POWER_CHANGE_TCASE_LOW", "FIBRE_POWER_CHANGE_TCASE_LOW");
                modRun.Limits.AddParameter(spec, "LOCK_FREQ_CHANGE_TCASE_LOW", "LOCK_FREQ_CHANGE_TCASE_LOW");
                modRun.Limits.AddParameter(spec, "PHASE_I_CHANGE_TCASE_LOW", "PHASE_I_CHANGE_TCASE_LOW");
            }
        }

        #endregion

        #region Configure instruments
        /// <summary>
        /// Configure a TEC controller using config data
        /// </summary>
        /// <param name="tecCtlr">Instrument reference</param>
        /// <param name="tecCtlId">Config table data prefix</param>
        private void ConfigureTecController(IInstType_TecController tecCtlr, string tecCtlId)
        {
            SteinhartHartCoefficients stCoeffs = new SteinhartHartCoefficients();

            // Keithley 2510 specific commands (must be done before 'SetDefaultState')
            if (tecCtlr.DriverName.Contains("Ke2510"))
            {
                tecCtlr.HardwareData["MinTemperatureLimit_C"] = progInfo.TestParamsConfig.GetStringParam(tecCtlId + "_MinTemp_C");
                tecCtlr.HardwareData["MaxTemperatureLimit_C"] = progInfo.TestParamsConfig.GetStringParam(tecCtlId + "_MaxTemp_C");
            }
            // Operating modes

            tecCtlr.SetDefaultState();
            tecCtlr.OperatingMode = (InstType_TecController.ControlMode)
                Enum.Parse(typeof(InstType_TecController.ControlMode), progInfo.TestParamsConfig.GetStringParam(tecCtlId + "_OperatingMode"));
            tecCtlr.Sensor_Type = (InstType_TecController.SensorType)
                Enum.Parse(typeof(InstType_TecController.SensorType), progInfo.TestParamsConfig.GetStringParam(tecCtlId + "_Sensor_Type"));

            // Thermistor characteristics
            if (tecCtlr.Sensor_Type == InstType_TecController.SensorType.Thermistor_2wire ||
                tecCtlr.Sensor_Type == InstType_TecController.SensorType.Thermistor_4wire)
            {
                stCoeffs.A = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_stCoeffs_A");
                stCoeffs.B = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_stCoeffs_B");
                stCoeffs.C = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_stCoeffs_C");
                tecCtlr.SteinhartHartConstants = stCoeffs;
            }

            // Additional parameters
            tecCtlr.TecCurrentCompliance_amp = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_TecCurrentCompliance_amp");
            tecCtlr.TecVoltageCompliance_volt = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_TecVoltageCompliance_volt");
            tecCtlr.ProportionalGain = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_ProportionalGain");
            tecCtlr.IntegralGain = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_IntegralGain");
            tecCtlr.DerivativeGain = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_DerivativeGain");
            tecCtlr.SensorTemperatureSetPoint_C = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_SensorTemperatureSetPoint_C");
        }

        /// <summary>
        /// Configure FCU instruments
        /// </summary>
        /// <param name="fcuInstrs"></param>
        private void ConfigureFCUInstruments(FCUInstruments fcuInstrs)
        {
            // Config FCU source
            // Keithley 2400 specific commands (must be done before 'SetDefaultState')
            InstType_ElectricalSource FcuSource = progInfo.Instrs.Fcu.FCU_Source;
            if (FcuSource.DriverName.Contains("Ke24xx"))
            {
                FcuSource.HardwareData["4wireSense"] = "true";
            }
            // Configure instrument
            FcuSource.SetDefaultState();
            // Set to voltage source mode & set current compliance
            FcuSource.VoltageSetPoint_Volt = 0.0;
            FcuSource.CurrentComplianceSetPoint_Amp = progInfo.TestParamsConfig.GetDoubleParam("FCUSource_CurrentCompliance_A");
            FcuSource.VoltageSetPoint_Volt = progInfo.TestParamsConfig.GetDoubleParam("FCUSource_Voltage_V");
            FcuSource.OutputEnabled = true;
            double i = FcuSource.CurrentActual_amp;

            // The Ixolite Wrapper Chassis should already have been ctor'd with driver name
            // "Ixolite" and resource string "LPT,378,41" {for accessing printer port 1, 
            // I2C address hex 41}.
            // The FCU instrument should already have been ctor'd with driver name
            // "Ixolite", no slots, and a reference to the above chassis instance.
            fcuInstrs.FullbandControlUnit.ResetCPN();
            fcuInstrs.FullbandControlUnit.EnterProtectMode();
            fcuInstrs.FullbandControlUnit.EnterVendorProtectMode("THURSDAY"); // Need this privilege level
                                                                              // in order to use IOIFBkhm interface.
                                                                              // This password has to be used.
            // Config FCU calibration data
            progInfo.Instrs.Fcu.FCUCalibrationData = new FCUInstruments.FCUCalData();
            string fcuSerialNumber = fcuInstrs.FullbandControlUnit.GetUnitSerialNumber().Trim();
            double CalFactor;
            double CalOffset;
            // Tx ADC
            readCalData("FCU", fcuSerialNumber, "Tx ADC", out CalFactor, out CalOffset);
            progInfo.Instrs.Fcu.FCUCalibrationData.TxADC_CalFactor = CalFactor;
            progInfo.Instrs.Fcu.FCUCalibrationData.TxADC_CalOffset = CalOffset;
            // Rx ADC
            readCalData("FCU", fcuSerialNumber, "Rx ADC", out CalFactor, out CalOffset);
            progInfo.Instrs.Fcu.FCUCalibrationData.RxADC_CalFactor = CalFactor;
            progInfo.Instrs.Fcu.FCUCalibrationData.RxADC_CalOffset = CalOffset;
            // TxCoarsePot
            readCalData("FCU", fcuSerialNumber, "TxCoarsePot", out CalFactor, out CalOffset);
            progInfo.Instrs.Fcu.FCUCalibrationData.TxCoarsePot_CalFactor = CalFactor;
            progInfo.Instrs.Fcu.FCUCalibrationData.TxCoarsePot_CalOffset = CalOffset;
            // TxFinePot
            readCalData("FCU", fcuSerialNumber, "TxFinePot", out CalFactor, out CalOffset);
            progInfo.Instrs.Fcu.FCUCalibrationData.TxFinePot_CalFactor = CalFactor;
            progInfo.Instrs.Fcu.FCUCalibrationData.TxFinePot_CalOffset = CalOffset;
            // ThermistorResistance
            readCalData("FCU", fcuSerialNumber, "ThermistorResistance", out CalFactor, out CalOffset);
            progInfo.Instrs.Fcu.FCUCalibrationData.ThermistorResistance_CalFactor = CalFactor;
            progInfo.Instrs.Fcu.FCUCalibrationData.ThermistorResistance_CalOffset = CalOffset;
        }

        /// <summary>
        /// Configure a MZ bias source
        /// </summary>
        /// <param name="biasSrc">Instrument reference</param>
        /// <param name="biasSrcId">Config table data prefix</param>
        private void ConfigureMzBiasSource(IInstType_ElectricalSource biasSrc, string biasSrcId)
        {
            // Keithley 2400 specific commands (must be done before 'SetDefaultState')
            if (biasSrc.DriverName.Contains("Ke24xx"))
            {
                biasSrc.HardwareData["4wireSense"] = "true";
            }

            // Configure instrument
            biasSrc.SetDefaultState();
            // Set to current source mode & set compliance
            biasSrc.CurrentSetPoint_amp = 0.0;
            biasSrc.CurrentComplianceSetPoint_Amp = progInfo.TestParamsConfig.GetDoubleParam(biasSrcId + "_CurrentCompliance_A");
            // Set to voltage source mode & set compliance
            biasSrc.VoltageSetPoint_Volt = 0.0;
            biasSrc.VoltageComplianceSetPoint_Volt = progInfo.TestParamsConfig.GetDoubleParam(biasSrcId + "_VoltageCompliance_volt");
        }

        #endregion

        #endregion


        #region Program Running

        public override void Run(ITestEngineRun engine, DUTObject dutObject, InstrumentCollection instrs, ChassisCollection chassis)
        {
            ModuleRun modRun;

            // Set retest count
            this.SetRetestCount(engine, dutObject);

            // Initialise 
            engine.RunModule("TcmzInitialise");

            // Set switches for the filter box
            if (!engine.IsSimulation && progInfo.DsdbrInstrsUsed == DsdbrDriveInstruments.PXIInstruments)
            {
                switch (progInfo.TestConditions.FreqBand)
                {
                    case FreqBand.C:
                        progInfo.Instrs.SwitchPathManager.SetToPosn("FILTERBOX_C");
                        break;
                    case FreqBand.L:
                        progInfo.Instrs.SwitchPathManager.SetToPosn("FILTERBOX_L");
                        break;
                    default:
                        engine.ErrorInProgram("Invalid frequency band: " + progInfo.TestConditions.FreqBand);
                        break;
                }
            }

            // Set temperatures
            //  pre set the case temp to save some time
            progInfo.Instrs.TecCase.SensorTemperatureSetPoint_C = progInfo.TestConditions.MidTemp;
            // TODO set MZ & DSDBR according to FF data
            progInfo.Instrs.TecCase.OutputEnabled = true;
            progInfo.Instrs.TecMz.OutputEnabled = true;
            // Set and wait for temperatures to stabilise
            engine.RunModule("DsdbrTemperature");
            engine.RunModule("MzTemperature");
            engine.RunModule("CaseTemp_Mid");

            // Get tcmz_map data.!
            this.CopyDataFromMap(this._mapResult, engine);
            this.numberOfSupermodes = _mapResult.ReadSint32("NUM_SM");

            // Switch optical path to MzOpm
            this.progInfo.Instrs.SwitchOsaMzOpm.SetState(Switch_Osa_MzOpm.State.MzOpm);

            //Add by tim for the OSA Disply save time at 2008-09-16;
            //switch on OSA
            Inst_Ag8614xA ins8614 = this.progInfo.Instrs.Osa as Inst_Ag8614xA;
            if ((ins8614 != null) &&
                (ins8614.DisplayFeature))
            {
                ins8614.Display = false;
            }
            //end;
            // Power up
            RunTcmzPowerUp(engine);

            // Measure of cross-calibration of frequency between map stage and final stage
            RunCrossCalibrationMeasure(engine);

            // Power head calibration
            RunTcmzPowerHeadCal(engine);

            // MZ Characterise 
            modRun = engine.GetModuleRun("TcmzMzCharacterise");
            modRun.PreviousTestData.AddReference("ItuChannels", closeGridCharData);
            ModuleRunReturn modRunReturn = engine.RunModule("TcmzMzCharacterise");
            bool isTcmzMzCharacteriseError =
                modRunReturn.ModuleRunData.ModuleData.IsPresent("IsError") ? modRunReturn.ModuleRunData.ModuleData.ReadBool("IsError") : false;
            if (isTcmzMzCharacteriseError)
            {
                this.errorInformation +=
                    modRunReturn.ModuleRunData.ModuleData.ReadString("ErrorInformation");
                // Set device to safe case temperature
                engine.RunModule("CaseTemp_Safe");
                return;
            }

            // MZ ITU settings estimate
            Datum mzSweepData = modRunReturn.ModuleRunData.ModuleData.GetDatum("MzSweepData");
            modRun = engine.GetModuleRun("TcmzMzItuEstimate");
            modRun.PreviousTestData.Add(mzSweepData);
            modRun.PreviousTestData.AddReference("ItuChannelsIn", this.closeGridCharData);
            modRun.PreviousTestData.AddString("MzSweepDataResultsFile", modRunReturn.ModuleRunData.ModuleData.ReadFileLinkFullPath("MzSweepDataResultsFile"));
            ModuleRunReturn ituEstRunReturn = engine.RunModule("TcmzMzItuEstimate");
            tcmzItuChannels = (TcmzChannels)ituEstRunReturn.ModuleRunData.ModuleData.ReadReference("TcmzItuChannels");
            if (ituEstRunReturn.ModuleRunData.ModuleData.ReadSint32("MzAtNewVcmPass") == 0)
            {
                this.errorInformation += "MZ status at new preferred Vcm failed, test stopped.";
                // Set device to safe case temperature
                engine.RunModule("CaseTemp_Safe");
                return;
            }

                // Channel Characterise
                int breakPowerLevelingCount = progInfo.TestParamsConfig.GetIntParam("BreakPowerLevelingCount");
                modRun = engine.GetModuleRun("TcmzChannelChar");
                modRun.ConfigData.AddReference("TcmzItuChannels", tcmzItuChannels);
                modRun.ConfigData.AddUint32("BreakPowerLevelingCount", breakPowerLevelingCount);
                ModuleRunReturn chanCharRunReturn = engine.RunModule("TcmzChannelChar");
            SelectTestFlag = chanCharRunReturn.ModuleRunData.ModuleData.ReadBool("SelectTestFlag");
            if (chanCharRunReturn.ModuleRunData.ModuleData.ReadSint32("FailAbortFlag") == 1)
                {
                    this.errorInformation = chanCharRunReturn.ModuleRunData.ModuleData.ReadString("FailAbortReason");
                // Set device to safe case temperature
                engine.RunModule("CaseTemp_Safe");
                    return;
                }

                // Low case temperature tests
                if (progInfo.TestParamsConfig.GetBoolParam("LowTempTestEnabled"))
                {
                    if (tcmzItuChannels.Passed.Length > 0)
                    {
                        // Set Case temperature to low 
                        engine.RunModule("CaseTemp_Low");

                        // Low temperature test 
                        modRun = engine.GetModuleRun("TcmzLowTempTest");
                        modRun.ConfigData.AddReference("TcmzItuChannels", tcmzItuChannels);
                        engine.RunModule("TcmzLowTempTest");
                    }
                }

                // High case temperature tests
                if (tcmzItuChannels.Passed.Length > 0)
                {
                    // Set Case temperature to high 
                    engine.RunModule("CaseTemp_High");

                    // High temperature test 
                    modRun = engine.GetModuleRun("TcmzHighTempTest");
                    modRun.ConfigData.AddReference("TcmzItuChannels", tcmzItuChannels);
                    engine.RunModule("TcmzHighTempTest");
                }

                // Send the case temperature back to ambient whilst we clean up.
                progInfo.Instrs.TecCase.SensorTemperatureSetPoint_C = 25;

                // Trace tone test - make sure over-temp test were done!
                if (tcmzItuChannels.Passed.Length > 0 && progInfo.TestParamsConfig.GetBoolParam("TraceToneTestEnabled"))
                {
                    // One-off measurements on individual channels
                    engine.RunModule("TcmzTraceToneTest");
                }

                // Consolodate results
                modRun = engine.GetModuleRun("TcmzGroupResults");
                modRun.ConfigData.AddReference("TcmzItuChannels", tcmzItuChannels);
                engine.RunModule("TcmzGroupResults");

            // Set device to safe case temperature
            engine.RunModule("CaseTemp_Safe");
        }

        /// <summary>
        /// Run TcmzPowerHeadCal module
        /// </summary>
        /// <param name="engine"></param>
        private void RunTcmzPowerHeadCal(ITestEngineRun engine)
        {
            ModuleRun modRun;
            ButtonId skipPowerCal = ButtonId.No;
            if (progInfo.TestParamsConfig.GetBoolParam("AllowSkipPowerCalibration"))
            {
                skipPowerCal = engine.ShowYesNoUserQuery("Do you want to skip power calibration?");
            }
            if (skipPowerCal == ButtonId.Yes)
            {
                GuiMsgs.TcmzPowerCalDataResponse resp;
                do
                {
                    engine.ShowContinueUserQuery("Select Power Calibration Data file");
                    engine.GuiShow();
                    engine.GuiToFront();
                    engine.SendToGui(new GuiMsgs.TcmzPowerCalDataRequest());
                    resp = (GuiMsgs.TcmzPowerCalDataResponse)engine.ReceiveFromGui().Payload;
                    if (!File.Exists(resp.Filename))
                    {
                        engine.ShowContinueUserQuery("Power Calibration Data file doesn't exist, please try again!");
                    }
                } while (!File.Exists(resp.Filename));

                OpticalPowerCal.Initialise(2);
                using (CsvReader cr = new CsvReader())
                {
                    List<string[]> contents = cr.ReadFile(resp.Filename);
                    for (int ii = 1; ii < contents.Count; ii++)
                    {
                        OpticalPowerCal.AddCalibratedPoint(OpticalPowerHead.MZHead, double.Parse(contents[ii][0]), double.Parse(contents[ii][1]) - double.Parse(contents[ii][2]));
                        if (progInfo.DsdbrInstrsUsed == DsdbrDriveInstruments.PXIInstruments)
                        {
                            OpticalPowerCal.AddCalibratedPoint(OpticalPowerHead.OpmCgDirect, double.Parse(contents[ii][0]), double.Parse(contents[ii][1]) - double.Parse(contents[ii][3]));
                            OpticalPowerCal.AddCalibratedPoint(OpticalPowerHead.OpmCgFiltered, double.Parse(contents[ii][0]), double.Parse(contents[ii][1]) - double.Parse(contents[ii][4]));
                        }
                    }
                }
            }
            else
            {
                modRun = engine.GetModuleRun("TcmzPowerHeadCal");
                modRun.ConfigData.AddReference("CloseGridCharData", this.closeGridCharData);
                modRun.ConfigData.AddSint32("NUM_SM", numberOfSupermodes);
                ModuleRunReturn moduleRunReturn = engine.RunModule("TcmzPowerHeadCal");
                labourTime += moduleRunReturn.ModuleRunData.ModuleData.GetDatumDouble("LabourTime").Value;
            }
        }

        /// <summary>
        /// Run TcmzPowerUp module
        /// </summary>
        /// <param name="engine"></param>
        private void RunTcmzPowerUp(ITestEngineRun engine)
        {
            ModuleRun modRun;
            modRun = engine.GetModuleRun("TcmzPowerUp");
            TcmzChannelInit chanInit = new TcmzChannelInit();
            // MZ data is from TcmzInitMzSweep
            chanInit.Mz.LeftArmImb_mA = progInfo.TestParamsConfig.GetDoubleParam("MzCtrlINominal_mA");
            chanInit.Mz.RightArmImb_mA = progInfo.TestParamsConfig.GetDoubleParam("MzCtrlINominal_mA");
            chanInit.Mz.LeftArmMod_Min_V = this._mapResult.ReadDouble("REF_MZ_VOFF_LEFT");
            chanInit.Mz.RightArmMod_Min_V = this._mapResult.ReadDouble("REF_MZ_VOFF_RIGHT");
            chanInit.Mz.LeftArmMod_Peak_V = this._mapResult.ReadDouble("REF_MZ_VON_LEFT");
            chanInit.Mz.RightArmMod_Peak_V = this._mapResult.ReadDouble("REF_MZ_VON_RIGHT");
            // DSDBR data
            double testFreq_GHz = 193700;

            //Add for "L" band by Tim at 2009-2-13.
            if (progInfo.TestConditions.FreqBand == FreqBand.L)
                testFreq_GHz = 189000; 
            foreach (DsdbrChannelData chanData in this.closeGridCharData)
            {
                if (Math.Abs(chanData.ItuFreq_GHz - testFreq_GHz) <= 20)
                {
                    chanInit.Dsdbr = chanData;
                    break;
                }
            }
            modRun.ConfigData.AddReference("TcmzSettings", chanInit);
            ModuleRunReturn tcmzPowerUpRtn = engine.RunModule("TcmzPowerUp");
            if (tcmzPowerUpRtn.ModuleRunData.ModuleData.ReadBool("ErrorExists") == true)
            {
                engine.RaiseNonParamFail(0, tcmzPowerUpRtn.ModuleRunData.ModuleData.ReadString("ErrorString"));
            }
        }

        /// <summary>
        /// Run TcmzCrossCalibration module
        /// </summary>
        /// <param name="engine"></param>
        private void RunCrossCalibrationMeasure(ITestEngineRun engine)
        {
            ModuleRun modRun;
            modRun = engine.GetModuleRun("TcmzCrossCalibration");

            // Get DsdbrChannelData for frequency cross-calibration
            int[] chanIndices = new int[3];
            chanIndices[0] = 0; // low channel
            chanIndices[1] = (progInfo.TestConditions.NbrChannels - 1) / 2; // mid channel 45
            chanIndices[2] = progInfo.TestConditions.NbrChannels - 1; // high channel
            DsdbrChannelData[] dsdbrChansData = new DsdbrChannelData[3];
            for (int ii = 0; ii < chanIndices.Length; ii++)
            {
                foreach (DsdbrChannelData chanData in this.closeGridCharData)
                {
                    if (chanData.ItuChannelIndex == chanIndices[ii])
                    {
                        dsdbrChansData[ii] = chanData;
                        break;
                    }
                }
            }

            // Add config data to test module
            modRun.ConfigData.AddReference("DsdbrChannels", dsdbrChansData);
            // Run test module
            engine.RunModule("TcmzCrossCalibration");
        }

        #endregion


        #region End of Program

        public override void PostRun(ITestEnginePostRun engine, DUTObject dutObject, InstrumentCollection instrs, ChassisCollection chassis)
        {
            // This takes a few seconds
            string archiveFile = archiveCGfiles(dutObject);

            // Cleanup & power off
            if (!engine.IsSimulation)
            {
                // Display temperature progress
                engine.GuiShow();
                engine.GuiToFront();

                SetCaseTempSafe(engine, 25);

                MzInstruments mzInstrs = progInfo.Instrs.Mz;
                mzInstrs.LeftArmMod.CleanUpSweep();
                mzInstrs.LeftArmImb.CleanUpSweep();
                mzInstrs.RightArmMod.CleanUpSweep();
                mzInstrs.RightArmImb.CleanUpSweep();
                mzInstrs.TapComplementary.CleanUpSweep();
                if (mzInstrs.InlineTapOnline)
                {
                    mzInstrs.TapInline.CleanUpSweep();
                }

                if (progInfo.DsdbrInstrsUsed == DsdbrDriveInstruments.PXIInstruments)
                {
                    CloseGridWrapper.Singleton.StopAndShutdown();
                }
                else
                {
                    // Shutdown FCU
                    // Call methods on the FCU to switch off the laser.
                    progInfo.Instrs.Fcu.SoaCurrentSource.OutputEnabled = false;

                    progInfo.Instrs.Fcu.FullbandControlUnit.SetControlRegLaserPSUOn(false);
                    TxCommand cmd1 = new TxCommand(0, 0, 0); // TunableModules command
                    cmd1 = progInfo.Instrs.Fcu.FullbandControlUnit.ReadTxCommand();
                    TxCommand cmd2 = new TxCommand(cmd1.Data1, cmd1.Data2,
                        (byte)(cmd1.Data3 | (byte)0x01)); // set bit0 of byte "3"
                    progInfo.Instrs.Fcu.FullbandControlUnit.SetTxCommand(cmd2); // Bit 'LSENABLE' is set for disabling it.
                    progInfo.Instrs.Fcu.FullbandControlUnit.OIFLaserEnable(false);

                    progInfo.Instrs.Fcu.FCU_Source.OutputEnabled = false;
                }

                progInfo.Instrs.TecCase.OutputEnabled = false;
                progInfo.Instrs.TecDsdbr.OutputEnabled = false;
                progInfo.Instrs.TecMz.OutputEnabled = false;
            }
        }

        private string archiveSMSRfiles(string SN)
        {
            const string SMSR_PATH = "results\\smsr";
            string[] files = Directory.GetFiles(SMSR_PATH);
            string SMSRzipfilename = "results\\SMSR_Spectrum" + "_" + SN + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".zip";
            using (Util_ZipFile zipFile = new Util_ZipFile(SMSRzipfilename))
            {
                foreach (string strFileName in files)
                {
                    zipFile.AddFileToZip(strFileName);
                    File.Delete(strFileName);
                }
            }

            return Path.GetFullPath(SMSRzipfilename);
        }

        public override void DataWrite(ITestEngineDataWrite engine, DUTObject dutObject, DUTOutcome dutOutcome, TestData results, UserList userList)
        {
            DatumList tcTraceData = new DatumList();

            #region Fill in blank TC and CH results
            // Add 'missing data' for TC_* and CH_* parameters
            foreach (ParamLimit paramLimit in progInfo.MainSpec)
            {
                if (paramLimit.ExternalName.StartsWith("TC_") || paramLimit.ExternalName.StartsWith("CH_"))
                {
                    if (!tcTraceData.IsPresent(paramLimit.ExternalName))
                    {
                        Datum dummyValue = null;
                        switch (paramLimit.ParamType)
                        {
                            case DatumType.Double:
                                dummyValue = new DatumDouble(paramLimit.ExternalName, Convert.ToDouble(paramLimit.HighLimit.ValueToString()));
                                break;
                            case DatumType.Sint32:
                                dummyValue = new DatumSint32(paramLimit.ExternalName, Convert.ToInt32(paramLimit.HighLimit.ValueToString()));
                                break;
                            case DatumType.StringType:
                                dummyValue = new DatumString(paramLimit.ExternalName, "MISSING");
                                break;
                            case DatumType.Uint32:
                                dummyValue = new DatumUint32(paramLimit.ExternalName, Convert.ToUInt32(paramLimit.HighLimit.ValueToString()));
                                break;
                        }
                        if (dummyValue != null)
                            tcTraceData.Add(dummyValue);
                    }
                }
            }
            #endregion

            ////archive smsr files
            string SMSRfiles = archiveSMSRfiles(dutObject.SerialNumber);

            //pcasc\blobData\1342           
            File.Copy(SMSRfiles, Path.Combine(@"pcasc\blobData\1342", Path.GetFileName(SMSRfiles)));
            //tcTraceData.AddFileLink("SMSR_FILE", "PATH");


            // Add time date
            tcTraceData.AddString("TIME_DATE", DateTime.Now.ToUniversalTime().ToString("yyyyMMddHHmmss"));
            // Add testTime
            testTime_End = DateTime.Now;
            TimeSpan testTime = testTime_End.Subtract(testTime_Start);
            tcTraceData.AddDouble("TEST_TIME", testTime.TotalMinutes);
            // Add labourTime
            tcTraceData.AddDouble("LABOUR_TIME", labourTime);
            // Add more detail into the main specification
            tcTraceData.AddString("FACTORY_WORKS_FAILMODE", "");
            tcTraceData.AddString("FACTORY_WORKS_LOTID", dutObject.BatchID);
            tcTraceData.AddString("FACTORY_WORKS_PARTID", dutObject.PartCode);
            tcTraceData.AddString("SOFTWARE_ID", dutObject.ProgramPluginName + dutObject.ProgramPluginVersion);
            tcTraceData.AddString("OPERATOR_ID", userList.UserListString);
            tcTraceData.AddString("COMMENTS", engine.GetProgramRunComments());
            tcTraceData.AddString("PRODUCT_CODE", dutObject.PartCode);
            //tcTraceData.AddString("CG_ID", "CloseGrid 5.0");
            //tcTraceData.AddString("MZ_WAFER_TYPE", "Oxide");
            //Todo: if full channel tested is "1",else"0".
            tcTraceData.AddString("FULL_PTR_TEST_FLAG", SelectTestFlag ? "0" : "1");
            if (LASER_WAFER_SIZE.Contains("3"))
            {
                tcTraceData.AddString("PTR_EOL_FREQ_DELTA", "16");
            }
            else
            {
                tcTraceData.AddString("PTR_EOL_FREQ_DELTA", "16");
            }
            //tcTraceData.AddString("LASER_WAFER_SIZE", " ");

            tcTraceData.AddDouble("TEC_LASER_OL_TEMP", TEC_LASER_OL_TEMP);
            tcTraceData.AddDouble("TEC_MZ_OL_TEMP", TEC_MZ_OL_TEMP);
            tcTraceData.AddDouble("TEC_JIG_CL_TEMP", TEC_JIG_CL_TEMP);
            
            //tcTraceData.AddString("CG_ID", CloseGridWrapper.Singleton.Version.ToString());
            //// Use the overall status for the main spec
            //tcTraceData.AddString("TEST_STATUS", engine.OverallPassFail.ToString());

            #region Write Channel pass/fail files and TTA/TSFF cal files
            if (tcmzItuChannels != null)
            {
                // Write channel files
                string ttaTemplate = @"Configuration\TcmzFinalConfig\TCMZ_Final\" + progInfo.TestParamsConfig.GetStringParam("TTA_PARAM_FILE");
                string tsffTemplate = @"Configuration\TcmzFinalConfig\TCMZ_Final\" + progInfo.TestParamsConfig.GetStringParam("TSFF_PARAM_FILE");
                string tempFileDirectory = progInfo.TestParamsConfig.GetStringParam("DSDBRFileDirectory");
                string passFile = Util_GenerateFileName.GenWithTimestamp(tempFileDirectory, "CHANNEL_PASS", dutObject.SerialNumber, "csv");
                string failFile = Util_GenerateFileName.GenWithTimestamp(tempFileDirectory, "CHANNEL_FAIL", dutObject.SerialNumber, "csv");
                string ttaFile = tempFileDirectory + Path.DirectorySeparatorChar + dutObject.SerialNumber.Replace(".", "_") +
                    "_" + DateTime.Now.ToUniversalTime().ToString("yyyyMMddHHmmss") + "_ITU.csv";
                string tsffFile = tempFileDirectory + Path.DirectorySeparatorChar + dutObject.SerialNumber.Replace(".", "_") +
                    "_" + DateTime.Now.ToUniversalTime().ToString("yyyyMMddHHmmss") + "_TSFF.csv";
                ChannelDataFiles.WriteTcmzData(tcmzItuChannels.Passed, passFile);
                ChannelDataFiles.WriteTcmzData(tcmzItuChannels.Failed, failFile);

                //2008-1-31: by Ken.Wu: Verify template file existance before write data.
                while (!File.Exists(ttaTemplate))
                {
                    string prompt = string.Format("Missing file: \"{0}\", Can't write TTA_CAL_FILE.\n" +
                            "Do you want to retry? If press No, this program won't generate TTA_CAL_FILE.",
                            ttaTemplate);
                    if (engine.ShowYesNoUserQuery(prompt) == ButtonId.Yes)
                    {
                        continue;
                    }
                    else
                    {
                        break;
                    }
                }

                if (File.Exists(ttaTemplate))
                {
                    ChannelDataFiles.WriteTTAData(tcmzItuChannels.Passed, ttaTemplate, ttaFile, dutObject.SerialNumber, "G4 T1");
                }

                while (!File.Exists(tsffTemplate))
                {
                    string prompt = string.Format("Missing file: \"{0}\", Can't write TSFF_CAL_FILE.\n" +
                            "Do you want to retry? If press No, this program won't generate TSFF_CAL_FILE.",
                            tsffTemplate);
                    if (engine.ShowYesNoUserQuery(prompt) == ButtonId.Yes)
                    {
                        continue;
                    }
                    else
                    {
                        break;
                    }
                }

                if (File.Exists(tsffTemplate))
                {
                    ChannelDataFiles.WriteTSFFData(tcmzItuChannels.Passed, tsffTemplate, tsffFile);
                }

                tcTraceData.AddFileLink("CHANNEL_PASS_FILE", passFile);
                tcTraceData.AddFileLink("CHANNEL_FAIL_FILE", failFile);
                tcTraceData.AddFileLink("TTA_CAL_FILE", ttaFile);
                tcTraceData.AddFileLink("TSFF_CAL_FILE", tsffFile);
            }
            #endregion

            if ((this.errorInformation != null) &&
                (this.errorInformation.Length != 0))
            {
                if (this.errorInformation.Length > 80)
                    this.errorInformation = this.errorInformation.Substring(0, 80);
                tcTraceData.AddString("FAIL_ABORT_REASON", this.errorInformation);
            }
            else
            {
                tcTraceData.AddString("FAIL_ABORT_REASON", "Success");
            }

            // Set trace data above here
            engine.SetTraceData(progInfo.MainSpec.Name, tcTraceData);

            // Test status and data keys
            foreach (Specification spec in results.Specifications)
            {
                if (!spec.Name.ToLower().Contains("dummyspec"))
                {
                    StringDictionary dataKeys = new StringDictionary();
                    dataKeys.Add("SCHEMA", "HIBERDB");
                    dataKeys.Add("DEVICE_TYPE", dutObject.PartCode);
                    dataKeys.Add("SERIAL_NO", dutObject.SerialNumber);
                    dataKeys.Add("TEST_STAGE", "final");
                    dataKeys.Add("SPECIFICATION", spec.Name);

                    // Add any other data required for external data (example below for PCAS)
                    // !(beware, all these parameters MUST exist in the specification)!...
                    DatumList traceData = new DatumList();
                    traceData.AddSint32("NODE", dutObject.NodeID);
                    traceData.AddString("EQUIP_ID", dutObject.EquipmentID);
                    if (engine.GetProgramRunStatus() != ProgramStatus.Success)
                    {
                        string status = engine.GetProgramRunStatus().ToString();
                        traceData.AddString("TEST_STATUS", status.Substring(0, Math.Min(14, status.Length)));
                    }
                    else
                    {
                        string status = spec.Status.Status.ToString();
                        traceData.AddString("TEST_STATUS", status.Substring(0, Math.Min(14, status.Length)));
                    }

                    // pick the specification to add key and trace data to...
                    engine.SetDataKeysPerSpec(spec.Name, dataKeys);
                    engine.SetTraceData(spec.Name, traceData);
                }
            }
        }

        /// <summary>
        /// Wait for the case temperature to reach a safe temperature
        /// </summary>
        /// <param name="engine">ITestEnginePostRun</param>
        private void SetCaseTempSafe(ITestEnginePostRun engine, double safeTemperature_C)
        {
            // Set case TEC to safe temperature
            progInfo.Instrs.TecCase.SensorTemperatureSetPoint_C = safeTemperature_C;
            double deltaTemp = double.MaxValue;
            do
            {
                double caseTemp = progInfo.Instrs.TecCase.SensorTemperatureActual_C;
                deltaTemp = Math.Abs(safeTemperature_C - caseTemp);
                engine.SendToGui("Case temperature = " + String.Format("{0:f}", caseTemp));
                System.Threading.Thread.Sleep(500);

            } while (deltaTemp > 10);
        }

        #endregion


        #region Private Helper Functions

        /// <summary>
        /// Get the times of retest for the current device
        /// </summary>
        /// <param name="testEngine"></param>
        /// <param name="dutObject"></param>
        private void SetRetestCount(ITestEngineRun testEngine, DUTObject dutObject)
        {
            IDataRead dataRead = testEngine.GetDataReader("PCAS_SHENZHEN");
            int retestCount = 0;
            try
            {
                //get parameter "retest"
                StringDictionary finalKeys = new StringDictionary();
                finalKeys.Add("SCHEMA", "hiberdb");
                finalKeys.Add("DEVICE_TYPE", dutObject.PartCode);
                finalKeys.Add("TEST_STAGE", "final");
                finalKeys.Add("SERIAL_NO", dutObject.SerialNumber);
                DatumList finalData = dataRead.GetLatestResults(finalKeys, false);
                if (finalData == null)
                {
                    retestCount = 0;
                }
                else
                {
                    if (finalData.IsPresent("RETEST"))
                    {
                        retestCount = int.Parse(finalData.ReadString("RETEST"));
                    }
                    retestCount++;
                }

            }
            catch
            {
            }

            // Set retest count
            DatumList dl = new DatumList();
            dl.AddString("RETEST", retestCount.ToString());
            progInfo.MainSpec.SetTraceData(dl);
        }

        /// <summary>
        /// Retrieves datums from config file via the ConfigAccessor.
        /// </summary>
        /// <param name="modRun">The ModuleRun to add the items to.</param>
        /// <param name="datumsRequired">The datum names in this datumlist specify the names of the datums to be retrieved and added to the ModuleRun</param>
        private void ExtractAndAddDatums(ModuleRun modRun, DatumList datumsRequired)
        {
            foreach (Datum datum in datumsRequired)
            {
                switch (datum.Type)
                {
                    case DatumType.Double:
                        {
                            modRun.ConfigData.Add(new DatumDouble(datum.Name, this.progInfo.TestParamsConfig.GetDoubleParam(datum.Name)));
                        }
                        break;

                    case DatumType.StringType:
                        {
                            modRun.ConfigData.Add(new DatumString(datum.Name, this.progInfo.TestParamsConfig.GetStringParam(datum.Name)));
                        }
                        break;

                    case DatumType.Uint32:
                        {
                            modRun.ConfigData.Add(new DatumUint32(datum.Name, this.progInfo.TestParamsConfig.GetIntParam(datum.Name)));
                        }
                        break;

                    case DatumType.Sint32:
                        {
                            modRun.ConfigData.Add(new DatumSint32(datum.Name, this.progInfo.TestParamsConfig.GetIntParam(datum.Name)));
                        }
                        break;

                    case DatumType.BoolType:
                        {
                            modRun.ConfigData.Add(new DatumBool(datum.Name, this.progInfo.TestParamsConfig.GetBoolParam(datum.Name)));
                        }
                        break;

                    default:
                        {
                            throw new ArgumentException("Invalid DatumType '" + datum.Type + "' of '" + datum.Name + "'");
                        }
                }
            }
        }

        /// <summary>
        /// Read instruments calibration data
        /// </summary>
        /// <param name="InstrumentName"></param>
        /// <param name="InstrumentSerialNo"></param>
        /// <param name="ParameterName"></param>
        /// <param name="CalFactor"></param>
        /// <param name="CalOffset"></param>
        private void readCalData(string InstrumentName, string InstrumentSerialNo, string ParameterName, out double CalFactor, out double CalOffset)
        {
            StringDictionary keys = new StringDictionary();
            keys.Add("Instrument_name", InstrumentName);
            keys.Add("Parameter", ParameterName);
            if (InstrumentSerialNo != null)
            {
                keys.Add("Instrument_SerialNo", InstrumentSerialNo);
            }
            DatumList calParams = progInfo.InstrumentsCalData.GetData(keys, false);

            CalFactor = calParams.ReadDouble("CalFactor");
            CalOffset = calParams.ReadDouble("CalOffset");
        }

        /// <summary>
        /// Retrive tcmz_map stage test result form pcas 
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        protected override void ValidateMapStageData(ITestEngineInit engine, DUTObject dutObject)
        {
            IDataRead dataRead = engine.GetDataReader("PCAS_SHENZHEN");

            //2008-3-24: by Ken: Determine whether tcmz_map stage tested.
            StringDictionary mapKeys = new StringDictionary();
            mapKeys.Add("SCHEMA", TCMZSchema);
            mapKeys.Add("SERIAL_NO", dutObject.SerialNumber);
            mapKeys.Add("TEST_STAGE", "tcmz_map");
            //mapKeys.Add("TEST_STATUS", "Pass");

            this._mapResult = dataRead.GetLatestResults(mapKeys, true);
            if (this._mapResult == null || !this._mapResult.ReadString("TEST_STATUS").ToLower().Contains("pass"))
            {
                string errorDescription = "No passed TCMZ_Map stage data in PCAS,\n" + "Please test this device at TCMZ_Map stage first!";
                engine.ShowContinueUserQuery(errorDescription);
                engine.ErrorInProgram(errorDescription);
            }

            // Get data from ITU operating file
            string closeGridCharFileName = this._mapResult.ReadFileLinkFullPath("CG_CHAR_ITU_FILE");
            this.closeGridCharData = Bookham.Toolkit.CloseGrid.CsvDataLookup.GetItuOperatingPoints(closeGridCharFileName);

            // Section IV curve data
            this.SectionIVCurveData = new List<SectionIVSweepData>();
            string sectionIVCurveFilename = this._mapResult.ReadFileLinkFullPath("SECTION_IVTEST_RESULTS_FILE");
            // read section IV curve data from file
            using (StreamReader reader = new StreamReader(sectionIVCurveFilename))
            {
                while (!reader.EndOfStream)
                {
                    string[] IDataLine = reader.ReadLine().Split(',');
                    string[] VDataLine = reader.ReadLine().Split(',');
                    if (IDataLine.Length > 0 && VDataLine.Length > 0)
                    {
                        string sectionNameInfo = IDataLine[0]; // "I"+sectionName+"_mA"
                        foreach (string sectionName in Enum.GetNames(typeof(DSDBRSection)))
                        {
                            if (sectionNameInfo.ToLower().Contains(sectionName.ToLower()))
                            {
                                sectionNameInfo = sectionName;
                                break;
                            }
                        }

                        // get section
                        DSDBRSection section = (DSDBRSection)Enum.Parse(typeof(DSDBRSection), sectionNameInfo);
                        double[] Idata = new double[IDataLine.Length - 1];
                        double[] Vdata = new double[VDataLine.Length - 1];
                        for (int ii = 1; ii < IDataLine.Length; ii++)
                        {
                            Idata[ii - 1] = double.Parse(IDataLine[ii]);
                            Vdata[ii - 1] = double.Parse(VDataLine[ii]);
                        }
                        SectionIVSweepData sweepData = new SectionIVSweepData(section, Idata, Vdata);
                        //sweepData.PolynomialFit(5); // Fit data
                        this.SectionIVCurveData.Add(sweepData);
                    }
                }
            }
        }

        /// <summary>
        /// Validate DSDBR drive instruments - PXI or FCU
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        protected override void ValidateDsdbrDriveInstrs(ITestEngineInit engine, DUTObject dutObject)
        {
            if (dutObject.TestJigID == "FCUTestJigID")
            {
                progInfo.DsdbrInstrsUsed = DsdbrDriveInstruments.FCUInstruments;
            }
            else if (dutObject.TestJigID == "PXITestJigID")
            {
                progInfo.DsdbrInstrsUsed = DsdbrDriveInstruments.PXIInstruments;
            }
            else
            {
                engine.ErrorInProgram("Unknown DSDBR drive instruments used!");
            }
        }

        /// <summary>
        /// Copy data from tcmz_map stage.
        /// </summary>
        /// <param name="mapData"></param>
        /// <param name="engine"></param>
        private void CopyDataFromMap(DatumList mapData, ITestEngineRun engine)
        {
            try
            {
                DatumList traceDataMapStage = new DatumList();

                // Feed forward data
                traceDataMapStage.Add(mapData.GetDatum("ETALON1_FS_B"));
                traceDataMapStage.Add(mapData.GetDatum("ETALON1_FS_PAIR"));
                traceDataMapStage.Add(mapData.GetDatum("ETALON1_PHASE"));
                traceDataMapStage.Add(mapData.GetDatum("ETALON1_REAR"));
                traceDataMapStage.Add(mapData.GetDatum("I_ETALON_FS_A"));
                traceDataMapStage.Add(mapData.GetDatum("I_ETALON_GAIN"));
                traceDataMapStage.Add(mapData.GetDatum("I_ETALON_SOA"));

                traceDataMapStage.Add(mapData.GetDatum("MZ_RTH"));
                traceDataMapStage.Add(mapData.GetDatum("LASER_RTH"));
                traceDataMapStage.Add(mapData.GetDatum("ETALON1_FREQ"));

                traceDataMapStage.Add(mapData.GetDatum("LASER_CHIP_ID"));
                traceDataMapStage.Add(mapData.GetDatum("LASER_WAFER_ID"));
                traceDataMapStage.Add(mapData.GetDatum("MZ_CHIP_ID"));
                traceDataMapStage.Add(mapData.GetDatum("MZ_WAFER_ID"));
                traceDataMapStage.Add(mapData.GetDatum("MZ_SERIAL_NO"));
                traceDataMapStage.Add(mapData.GetDatum("LASER_WAFER_SIZE"));
                traceDataMapStage.Add(mapData.GetDatum("CG_ID"));
                traceDataMapStage.Add(mapData.GetDatum("MZ_WAFER_TYPE"));

                // TcmzInitialise
                traceDataMapStage.Add(mapData.GetDatum("CG_DUT_FILE_TIMESTAMP"));
                traceDataMapStage.Add(mapData.GetDatum("REGISTRY_FILE"));

                // TcmzPowerUp
                traceDataMapStage.Add(mapData.GetDatum("ITX_DARK"));
                traceDataMapStage.Add(mapData.GetDatum("IRX_DARK"));
                traceDataMapStage.Add(mapData.GetDatum("TAP_COMP_DARK_I"));
                traceDataMapStage.Add(mapData.GetDatum("MZ_MOD_LEFT_DARK_I"));
                traceDataMapStage.Add(mapData.GetDatum("MZ_MOD_RIGHT_DARK_I"));
                traceDataMapStage.Add(mapData.GetDatum("MZ_IMB_LEFT_DARK_I"));
                traceDataMapStage.Add(mapData.GetDatum("MZ_IMB_RIGHT_DARK_I"));
                traceDataMapStage.Add(mapData.GetDatum("FIBRE_POWER_INITIAL"));

                // TcmzInitMzSweep
                traceDataMapStage.Add(mapData.GetDatum("MZ_MAX_V_BIAS"));
                traceDataMapStage.Add(mapData.GetDatum("REF_LV_FILE"));
                traceDataMapStage.Add(mapData.GetDatum("REF_MZ_VON_LEFT"));
                traceDataMapStage.Add(mapData.GetDatum("REF_MZ_VOFF_LEFT"));
                traceDataMapStage.Add(mapData.GetDatum("REF_MZ_VON_RIGHT"));
                traceDataMapStage.Add(mapData.GetDatum("REF_MZ_VOFF_RIGHT"));
                traceDataMapStage.Add(mapData.GetDatum("REF_FIBRE_POWER"));
                traceDataMapStage.Add(mapData.GetDatum("REF_MZ_LEFT"));
                traceDataMapStage.Add(mapData.GetDatum("REF_MZ_RIGHT"));
                traceDataMapStage.Add(mapData.GetDatum("REF_MZ_SUM"));
                traceDataMapStage.Add(mapData.GetDatum("REF_MZ_RATIO"));
                traceDataMapStage.Add(mapData.GetDatum("REF_FREQ"));

                //CloseGridOverallMap Module
                traceDataMapStage.Add(mapData.GetDatum("NUM_SM"));
                traceDataMapStage.Add(mapData.GetDatum("CG_SM_LINES_FILE"));
                traceDataMapStage.Add(mapData.GetDatum("CG_PRATIO_WL_FILE"));
                traceDataMapStage.Add(mapData.GetDatum("CG_QAMETRICS_FILE"));
                traceDataMapStage.Add(mapData.GetDatum("MATRIX_PRATIO_SUMMARY_MAP"));

                //TcmzPowerHeadCal Module
                //CloseGridSupermode 0 => n Module
                for (int i = 0; i < 8; i++)
                {
                    traceDataMapStage.Add(mapData.GetDatum("MODAL_DISTORT_SM" + i.ToString()));
                    traceDataMapStage.Add(mapData.GetDatum("MILD_MULTIMODE_SM" + i.ToString()));
                }

                //CloseGridCharacterise Module
                traceDataMapStage.Add(mapData.GetDatum("CG_PASSFAIL_FILE"));
                traceDataMapStage.Add(mapData.GetDatum("CG_ITUEST_POINTS_FILE"));
                traceDataMapStage.Add(mapData.GetDatum("ITU_OP_EST_COUNT"));
                traceDataMapStage.Add(mapData.GetDatum("CHAR_ITU_OP_COUNT"));
                traceDataMapStage.Add(mapData.GetDatum("CG_CHAR_ITU_FILE"));
                traceDataMapStage.Add(mapData.GetDatum("NUM_MISSING_EST_CHAN"));

                // Set trace data
                progInfo.MainSpec.SetTraceData(traceDataMapStage);
            }
            catch (Exception e)
            {
                engine.ErrorInProgram("Copy map stage data error!" + e.Message);
            }
        }

        /// <summary>
        /// Run at the end of test to archive results for this device
        /// </summary>
        private string archiveCGfiles(DUTObject dutObject)
        {
            string zipFileName = Util_GenerateFileName.GenWithTimestamp(
                progInfo.TestParamsConfig.GetStringParam("ResultsArchiveDirectory"),
                "CloseGridFiles", dutObject.SerialNumber, "zip");

            if (!Directory.Exists(Path.GetDirectoryName(zipFileName)))
            {
                Directory.CreateDirectory(progInfo.TestParamsConfig.GetStringParam("ResultsArchiveDirectory"));
            }

            if (Directory.Exists(progInfo.TestParamsConfig.GetStringParam("DSDBRFileDirectory")))
            {
                string[] csvFilesToAdd = Directory.GetFiles(
                    progInfo.TestParamsConfig.GetStringParam("DSDBRFileDirectory"),
                    "*" + dutObject.SerialNumber + "*.csv", SearchOption.TopDirectoryOnly);
                string[] zipFilesToAdd = Directory.GetFiles(
                    progInfo.TestParamsConfig.GetStringParam("DSDBRFileDirectory"),
                    "*" + dutObject.SerialNumber + "*.zip", SearchOption.TopDirectoryOnly);
                string[] txtFilesToAdd = Directory.GetFiles(
                    progInfo.TestParamsConfig.GetStringParam("DSDBRFileDirectory"),
                    "*" + dutObject.SerialNumber + "*.txt", SearchOption.TopDirectoryOnly);

                if (csvFilesToAdd.Length > 0 || zipFilesToAdd.Length > 0 || txtFilesToAdd.Length > 0)
                {
                    Util_ZipFile zipFile = new Util_ZipFile(zipFileName);
                    zipFile.SetCompressionLevel(5); // 5 is reasonably fast with good compression
                    using (zipFile)
                    {
                        if (csvFilesToAdd.Length > 0)
                        {
                            foreach (string fileToAdd in csvFilesToAdd)
                            {
                                zipFile.AddFileToZip(fileToAdd);
                            }
                        }

                        if (zipFilesToAdd.Length > 0)
                        {
                            foreach (string fileToAdd in zipFilesToAdd)
                            {
                                zipFile.AddFileToZip(fileToAdd);
                            }
                        }

                        if (txtFilesToAdd.Length > 0)
                        {
                            foreach (string fileToAdd in txtFilesToAdd)
                            {
                                zipFile.AddFileToZip(fileToAdd);
                            }
                        }
                    }
                    return zipFileName;
                }
            }
            return null;
        }

        /// <summary>
        /// Run at the start of test to delete any old CSV data
        /// </summary>
        protected override void cleanUpCgFiles()
        {
            if (!Directory.Exists(progInfo.TestParamsConfig.GetStringParam("DSDBRFileDirectory")))
                return;

            string[] filesToRemove = Directory.GetFiles(
                    progInfo.TestParamsConfig.GetStringParam("DSDBRFileDirectory"), "*.csv");
            string[] zipFilesToRemove = Directory.GetFiles(
                    progInfo.TestParamsConfig.GetStringParam("DSDBRFileDirectory"), "*.zip");
            string[] txtFilesToRemove = Directory.GetFiles(
                    progInfo.TestParamsConfig.GetStringParam("DSDBRFileDirectory"), "*.txt");

            foreach (string fileToRemove in filesToRemove)
            {
                try
                {
                    File.Delete(fileToRemove);
                }
                catch (Exception)
                {
                    // Ignore exceptions on individual files
                }
            }

            foreach (string fileToRemove in zipFilesToRemove)
            {
                try
                {
                    File.Delete(fileToRemove);
                }
                catch (Exception)
                {
                    // Ignore exceptions on individual files
                }
            }

            foreach (string fileToRemove in txtFilesToRemove)
            {
                try
                {
                    File.Delete(fileToRemove);
                }
                catch (Exception)
                {
                    // Ignore exceptions on individual files
                }
            }
        }

        #endregion

    }
}

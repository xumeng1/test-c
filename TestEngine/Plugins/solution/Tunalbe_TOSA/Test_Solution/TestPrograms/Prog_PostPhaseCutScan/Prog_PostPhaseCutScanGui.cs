// [Copyright]
//
// Bookham Test Engine
// TODO - Full name of assembly
//
// Prog_PostPhaseCutScan/ProgramGui.cs
// 
// Author: paul.annetts
// Design: TODO Title of design document

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Bookham.TestSolution.TestPrograms
{
    public partial class Prog_PostPhaseCutScanGui : Bookham.TestEngine.Framework.Messages.ManagedCtlBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public Prog_PostPhaseCutScanGui()
        {
            /* Call designer generated code. */
            InitializeComponent();

            /* TODO - Write constructor code here. */
        }

        /// <summary>
        /// Message Received Event Handler
        /// </summary>
        /// <param name="payload">Message Payload</param>
        /// <param name="inMsgSeq">Incoming message sequence number</param>
        /// <param name="respSeq">Outgoing message sequence number</param>
        private void ProgramGui_MsgReceived(object payload, long inMsgSeq, long respSeq)
        {

        }
    }
}

﻿// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestPrograms
//
// Prog_PostPhaseCutScan.cs
//
// Author: Echoxl.wang, 2011
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.IO.Ports;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.ExternalData;
using Bookham.TestEngine.Framework.Limits;
using System.Collections.Specialized;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestSolution.Instruments;
using Bookham.TestLibrary.Instruments;
using Bookham.TestLibrary.Utilities;
using System.Windows.Forms;
using Bookham.TestLibrary.InstrTypes;
//using Bookham.TestLibrary.FcuCommonUtils;
using Bookham.TestSolution.IlmzCommonInstrs;
using Bookham.TestSolution.IlmzCommonUtils;
using Bookham.ToolKit.Mz;
using Bookham.Program.Core;
using Bookham.TestSolution.IlmzCommonData;

namespace Bookham.TestSolution.TestPrograms
{
    public class Prog_PostPhaseCutScan : TestProgramCore
    {
        #region Private data
        private DatumList traceDataList = null;
        private DatumList PreBurnData = null;

        private const string PhaseScanSettingsPath = "Configuration/TOSA Final/PhaseScan/PhaseScanTestConfig.xml";
        private const string LocalSettingsPath = @"Configuration\TOSA Final\LocalTestParams.xml";
        private const string FinalSettingsPath = "Configuration/TOSA Final/FinalTest/CommonTestParams.xml";
        private const string OpticalSwitchSettingsPath = "Configuration/TOSA Final/IlmzOpticalSwitch.xml";
        private const string MapParamsConfigSettingsPath = "Configuration/TOSA Final/MapTest/MappingTestConfig.xml";
        private string PhaseCurrentSetupFile = "";
        private string RearCurrentSetupFile = "";
        private string FSBCurrentSetupFile = "";
        private string serialNum;
        private string fail_Abort_Reason;

        private Dictionary<string, Specification> SpecDict = new Dictionary<string, Specification>();

        private TestParamConfigAccessor ProgramCfgReader;
        private TestParamConfigAccessor LocalCfgReader;
        private TestParamConfigAccessor TecCaseCfgReader;
        private TestParamConfigAccessor TecDSDBRCfgReader;
        private TestParamConfigAccessor ElecCfgReader;
        private TestParamConfigAccessor FinalCfgReader;
        private TestParamConfigAccessor OpticalSwitchConfig;
        private TestParamConfigAccessor MapParamsConfig;
        private TempTableConfigAccessor TempConfig;
        private TestParamConfigAccessor LightTowerConfig;

        Inst_Ke2510 TecDsdbr;
        //Inst_Ke2510 TecCase;
        //Inst_Nt10a TecCase;
        IInstType_TecController TecCase;
        //Inst_Ke24xx fcuSource;
        //Inst_FCU Fcu2Asic;
        Inst_Fcu2Asic Fcu2Asic;
        //Inst_E36xxA VcckeSource ;
        //Inst_E36xxA VeekeSource ;
        IInstType_ElectricalSource ASIC_VccSource;
        IInstType_ElectricalSource ASIC_VeeSource;
        InstType_ElectricalSource FCUMKI_Source;
        //Inst_Ke24xx CtapSource;
        //Inst_Ke24xx Leftmode ;
        //Inst_Ke24xx Rightmod;
        ITestEngineInit testEngineInit;

        double RTH_Actual_ohm = 0.0;

        float preBurn_constant = 0;
        float preBurn_nonConstant = 0;
        int preBurn_fp = 3;
        float preBurn_Igain_mA = 0;
        float preBurn_Irear_mA = 0;
        float preBurn_Isoa_mA = 0;
        float preBurn_imbLeft_mA = 0;
        float preBurn_imbRight_mA = 0;
        double preBurn_freq_GHz = 0;
        double preBurn_braggFreq_GHz = 0.0;
        double IMBLeft_PRE_Minus_POST_mA = -999;
        int preLastJumpIndex = 0;
        int preLastModeLength = 0;
        int preJumpCount = 0;
        int prelastsecondJumpIndex = 0;
        double TC_LASER_TEMP_C = 0;
        double TC_CASE_MID_C = 0;
        double Pre_ITH = 0;
        double MzPeak_LEFT_IMBForITH = 0;
        double MzPeak_RIGHT_IMBForITH = 0;
        double RTH_Offset_ohm=0;
        int numberPointSweep = 0;
        string MainSpecName = "";

        double ITH_Shift_Ratio = 0;
        double mode_Shift_Ratio = 0;

        #endregion

        #region for Light Tower.

        private SerialPort _serialPort;
        /// <summary>
        /// Serial port to control the lighter to indicate test messeage
        /// </summary>
        public SerialPort SerialPort
        {
            get { return _serialPort; }
        }

        /// <summary>
        /// Construct a serial port object from parts of the resource string.
        /// </summary>
        /// <param name="comPort">COMx port portion of resource string.</param>
        /// <param name="rate">bps rate portion of resource string.</param>
        /// <returns>Constructed SerialPort handler instance.</returns>
        private SerialPort buildSerPort(string comPort, string rate)
        {
            return new System.IO.Ports.SerialPort(
                "COM" + comPort,                                /* COMn */
                int.Parse(rate),                                /* bps */
                System.IO.Ports.Parity.None, 8, StopBits.One);  /* 8N1 */
        }
        #endregion

        public override Type UserControl
        {
            get { return typeof(Prog_PostPhaseCutScanGui); }
        }

        #region Implement InitCode method

        protected override void InitConfig(ITestEngineInit engine, DUTObject dutObject)
        {
            base.InitConfig(engine, dutObject);

            testEngineInit = engine;
            serialNum = dutObject.SerialNumber;

            ProgramCfgReader = new TestParamConfigAccessor(dutObject, PhaseScanSettingsPath, "", "PhaseScanTestParams");
            LocalCfgReader = new TestParamConfigAccessor(dutObject, LocalSettingsPath, "", "TestParams");
            TecCaseCfgReader = new TestParamConfigAccessor(dutObject, FinalSettingsPath, "TecCase", "TestParams");
            TecDSDBRCfgReader = new TestParamConfigAccessor(dutObject, FinalSettingsPath, "TecDsdbr", "TestParams");
            ElecCfgReader = new TestParamConfigAccessor(dutObject, PhaseScanSettingsPath, "CommonElec", "PhaseScanTestParams");
            FinalCfgReader = new TestParamConfigAccessor(dutObject, FinalSettingsPath, "", "TestParams");
            TempConfig = new TempTableConfigAccessor(dutObject, 1, @"Configuration\TOSA Final\TempTable.xml");
            OpticalSwitchConfig = new TestParamConfigAccessor(dutObject, OpticalSwitchSettingsPath, "", "IlmzOpticalSwitchParams");
            MapParamsConfig = new TestParamConfigAccessor(dutObject, MapParamsConfigSettingsPath, "", "MappingTestParams");
            LightTowerConfig = new TestParamConfigAccessor(dutObject, @"Configuration\TOSA Final\LightTowerParams.xml", "", "LightTowerParams");

            PhaseCurrentSetupFile = ProgramCfgReader.GetStringParam("PhaseCurrentFile");
            RearCurrentSetupFile = ProgramCfgReader.GetStringParam("RearCurrentFile");
            FSBCurrentSetupFile = ProgramCfgReader.GetStringParam("FSBCurrentFile");
            numberPointSweep = ProgramCfgReader.GetIntParam("PointNum_MZSweep");
        }

        protected override void LoadSpecs(ITestEngineInit engine, DUTObject dut)
        {
            bool ReadFromPcas = ProgramCfgReader.GetBoolParam("ReadFromPcas");

            StringDictionary mainSpecKeys = new StringDictionary();
            SpecList tempSpecList = null;
            ILimitRead limitReader;
            SpecList specList = new SpecList();

            if (ReadFromPcas)
            {
                limitReader = engine.GetLimitReader();
            }
            else //read limit from local file
            {
                // initialise limit file reader
                limitReader = engine.GetLimitReader("PCAS_FILE");
                // main specification
                string final_SpecNameFile = ProgramCfgReader.GetStringParam(dut.TestStage.ToLower() + "_PcasLimitFileName");
                string finalSpecFullFilename = ProgramCfgReader.GetStringParam("PcasLimitFileDirectory")
                    + @"\" + final_SpecNameFile;
                mainSpecKeys.Add("Filename", finalSpecFullFilename);
            }
            mainSpecKeys.Add("SCHEMA", "HIBERDB");
            mainSpecKeys.Add("TEST_STAGE", dut.TestStage.ToLower());
            mainSpecKeys.Add("DEVICE_TYPE", dut.PartCode);

            tempSpecList = limitReader.GetLimit(mainSpecKeys);
            this.MainSpec = tempSpecList[0];
            engine.SetSpecificationList(tempSpecList);
            this.MainSpecName = this.MainSpec.Name;
            
            base.LoadSpecs(engine, dut);
        }

        protected override void ValidateBurnInStageData(ITestEngineInit engine, DUTObject dutObject)
        {
            IDataRead dataRead = engine.GetDataReader("PCAS_SHENZHEN");
            DatumList BIData = null;

            StringDictionary BIKeys = new StringDictionary();
            BIKeys.Add("SCHEMA", "hiberdb");
            BIKeys.Add("TEST_STAGE", "burn in");
            BIKeys.Add("SERIAL_NO", dutObject.SerialNumber);
            BIData = dataRead.GetLatestResults(BIKeys, false);
            try
            {
                if (BIData != null
                    && BIData.Count > 0)
                {
                    string testStatus = BIData.ReadString("TEST_STATUS");
                    if (!testStatus.ToLower().Contains("pass"))
                    {
                        this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "This DUT is failed at burn in, Terminate testing!", FailModeCategory_Enum.OP);
                    }

                }
                else
                {
                    this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "NO burn in data in PCAS,Terminate mapping testing!", FailModeCategory_Enum.OP);
                }

            }
            catch (Exception e)
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, e.Message, FailModeCategory_Enum.UN);
            }

        }

        protected override void InitInstrs(ITestEngineInit engine, DUTObject dutObject, InstrumentCollection instrs, ChassisCollection chassis)
        {
            if (engine.IsSimulation) return;

            #region light_tower

            /* Set up Serial Binary Comms */
            // _serialPort = buildSerPort(comPort, rate);
            if (LightTowerConfig.GetBoolParam("LightExist"))
            {
                try
                {

                    _serialPort = this.buildSerPort(LightTowerConfig.GetStringParam("ComNum"), LightTowerConfig.GetStringParam("BaudRate"));
                    _serialPort.Close();
                    if (!_serialPort.IsOpen)
                    {
                        _serialPort.Open();
                    }

                    // Add the light tower color.
                    _serialPort.WriteLine(LightTowerConfig.GetStringParam("LightGreen"));
                    _serialPort.Close();
                }
                catch { }

            }
            #endregion

            TecDsdbr = (Inst_Ke2510)instrs["TecDsdbr"];
            //TecCase = (Inst_Ke2510)instrs["TecCase"];
            TecCase = (IInstType_TecController)instrs["TecCase"];
            if (instrs.Contains("FCUMKI_Source"))
            {
                FCUMKI_Source = (InstType_ElectricalSource)instrs["FCUMKI_Source"];
                FCUMKI_Source.VoltageSetPoint_Volt = LocalCfgReader.GetDoubleParam("FcuSource_Voltage_V");
                FCUMKI_Source.CurrentComplianceSetPoint_Amp = LocalCfgReader.GetDoubleParam("FcuSource_CurrentCompliance_A");
                FCUMKI_Source.OutputEnabled = true;
                System.Threading.Thread.Sleep(5000);
            }
            else
                FCUMKI_Source = null;

            ASIC_VccSource = (IInstType_ElectricalSource)instrs["ASIC_VccSource"];
            ASIC_VeeSource = (IInstType_ElectricalSource)instrs["ASIC_VeeSource"];

            if (ASIC_VccSource != null)
            {
                ASIC_VccSource.VoltageSetPoint_Volt = LocalCfgReader.GetDoubleParam("AsicVccSource_Voltage_V");
                ASIC_VccSource.CurrentComplianceSetPoint_Amp = LocalCfgReader.GetDoubleParam("AsicVccSource_CurrentCompliance_A");
                ASIC_VccSource.OutputEnabled = true;
            }
            if (ASIC_VeeSource != null)
            {
                ASIC_VeeSource.VoltageSetPoint_Volt = LocalCfgReader.GetDoubleParam("AsicVeeSource_Voltage_V");
                ASIC_VeeSource.CurrentComplianceSetPoint_Amp = LocalCfgReader.GetDoubleParam("AsicVeeSource_CurrentCompliance_A");
                ASIC_VeeSource.OutputEnabled = true;
            }



            Fcu2Asic = (Inst_Fcu2Asic)instrs["Fcu2Asic"];
            if (dutObject.TestJigID == "FCU2AsicTestJigId")
            {
                DsdbrUtils.Fcu2AsicInstrumentGroup = new FCU2AsicInstruments();
                DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic = Fcu2Asic;
            }
            DsdbrUtils.opticalSwitch = TecDsdbr;
            Measurements.MzHead = (IInstType_OpticalPowerMeter)instrs["OpmMz"];
            Measurements.MzHead.SetDefaultState();
            Fcu2Asic.SwapWirebond = ParamManager.Conditions.IsSwapWirebond;

            //Measurements.MzHead.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;

            TestProgramCore.InitInstrs_Wavemeter(this.failModeCheck, engine, dutObject, instrs, this.LocalCfgReader, this.OpticalSwitchConfig);
            
            ConfigureTecController(engine, TecCase, "TecCase", TecCaseCfgReader, true, this.TC_CASE_MID_C);
            ConfigureTecController(engine, TecDsdbr, "TecDsdbr", TecDSDBRCfgReader, false, this.TC_LASER_TEMP_C);
        }

        protected override void InitOpticalSwitchIoLine()
        {
            IlmzOpticalSwitchLines.C_Band_DigiIoLine = int.Parse(OpticalSwitchConfig.GetStringParam("C_Band_Filter_IO_Line"));
            IlmzOpticalSwitchLines.C_Band_DigiIoLine_State = OpticalSwitchConfig.GetBoolParam("C_Band_Filter_IO_Line_State");
            IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine = int.Parse(OpticalSwitchConfig.GetStringParam("Dut_Ctap_IO_Line"));
            IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine_State = OpticalSwitchConfig.GetBoolParam("Dut_Ctap_IO_Line_State");
            IlmzOpticalSwitchLines.Dut_Rx_DigiIoLine = int.Parse(OpticalSwitchConfig.GetStringParam("Dut_Rx_IO_Line"));
            IlmzOpticalSwitchLines.Dut_Rx_DigiIoLine_State = OpticalSwitchConfig.GetBoolParam("Dut_Rx_IO_Line_State");
            IlmzOpticalSwitchLines.Dut_Tx_DigiIoLine = int.Parse(OpticalSwitchConfig.GetStringParam("Dut_Tx_IO_LIne"));
            IlmzOpticalSwitchLines.Dut_Tx_DigiIoLine_State = OpticalSwitchConfig.GetBoolParam("Dut_Tx_IO_LIne_State");
            IlmzOpticalSwitchLines.Etalon_Ref_DigiIoLine = int.Parse(OpticalSwitchConfig.GetStringParam("Etalon_Ref_IO_Line"));
            IlmzOpticalSwitchLines.Etalon_Ref_DigiIoLine_State = OpticalSwitchConfig.GetBoolParam("Etalon_Ref_IO_Line_State");
            IlmzOpticalSwitchLines.Etalon_Rx_DigiIoLine = int.Parse(OpticalSwitchConfig.GetStringParam("Etalon_Rx_IO_Line"));
            IlmzOpticalSwitchLines.Etalon_Rx_DigiIoLine_State = OpticalSwitchConfig.GetBoolParam("Etalon_Rx_IO_Line_State");
            IlmzOpticalSwitchLines.Etalon_Tx_DigiIoLine = int.Parse(OpticalSwitchConfig.GetStringParam("Etalon_Tx_IO_Line"));
            IlmzOpticalSwitchLines.Etalon_Tx_DigiIoLine_State = OpticalSwitchConfig.GetBoolParam("Etalon_Tx_IO_Line_State");
            IlmzOpticalSwitchLines.L_Band_DigiIoLine = int.Parse(OpticalSwitchConfig.GetStringParam("L_Band_Filter_IO_Line"));
            IlmzOpticalSwitchLines.L_Band_DigiIoLine_State = OpticalSwitchConfig.GetBoolParam("L_Band_Filter_IO_Line_State");
        }

        protected override void InitModules(ITestEngineInit engine, DUTObject dutObject, InstrumentCollection instrs, ChassisCollection chassis)
        {
            this.InitMod_Pin_Check(engine, instrs, chassis, dutObject);
            this.CaseTempControl_InitModule(engine, "CaseTemp_Mid", 25, this.TC_CASE_MID_C);
            this.InitMod_TEC_ACResistance(engine, instrs);
            this.DeviceTempControl_InitModule(engine, "DsdbrTemperature", TecDsdbr, "TecDsdbr");
            this.InitMod_PhaseCutScan(engine, instrs, chassis, dutObject);
            this.InitMod_RearCutScan(engine, instrs, chassis, dutObject);
            this.InitMod_LIVMeasurement(engine, instrs, chassis, dutObject);
            this.InitMod_HittMzLiSweep(engine, instrs, chassis, dutObject);
        }

        #endregion

        #region Initialize modules

        /// <summary>
        /// Initial Pin check for hittGB test(check the pins correlation with TCE and Vcc)
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="instrs"></param>
        /// <param name="chassis"></param>
        /// <param name="dutObject"></param>
        private void InitMod_Pin_Check(ITestEngineInit engine, InstrumentCollection instrs, ChassisCollection chassis, DUTObject dutObject)
        {
            double Temperature_LowLimit_Degree = 15.0;
            double Temperature_HighLimit_Degree = 40.0;

            ModuleRun modRun = engine.AddModuleRun("Mod_PinCheck", "Mod_PinCheck", string.Empty);

            modRun.Instrs.Add(instrs);
            modRun.Chassis.Add(chassis);
            modRun.ConfigData.AddString("TIME_STAMP", TestTimeStamp_Start);
            modRun.ConfigData.AddString("LASER_ID", dutObject.SerialNumber);
            modRun.ConfigData.AddDouble("Temperature_LowLimit_Degree", Temperature_LowLimit_Degree);
            modRun.ConfigData.AddDouble("Temperature_HighLimit_Degree", Temperature_HighLimit_Degree);
        }

        /// <summary>
        /// Check DUT TEC AC Resistance - chongjian.liang 2013.3.20
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="instrs"></param>
        private void InitMod_TEC_ACResistance(ITestEngineInit engine, InstrumentCollection instrs)
        {
            ModuleRun modRun = engine.AddModuleRun("Mod_TEC_ACResistance", "Mod_TEC_ACResistance", string.Empty);

            modRun.Instrs.Add(instrs);

            modRun.Limits.AddParameter(this.MainSpec, "DUT_TEC_ACR", "DUT_TEC_ACR");
        }

        /// <summary>
        /// Do MZ LI sweep to compare the difference of IMB between PRE & POST stage - chongjian.liang 2013.3.20
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="instrs"></param>
        /// <param name="chassis"></param>
        /// <param name="dutObject"></param>
        private void InitMod_HittMzLiSweep(ITestEngineInit engine, InstrumentCollection instrs, ChassisCollection chassis, DUTObject dutObject)
        {
            ModuleRun modRun = engine.AddModuleRun("DoHittMzLiSweep", "Mod_HittMzLiSweep", string.Empty);
            modRun.Instrs.Add(instrs);
            modRun.Chassis.Add(chassis);

            modRun.ConfigData.AddReference("FailModeCheck", this.failModeCheck);
            modRun.ConfigData.AddString("TIME_STAMP", TestTimeStamp_Start);
            modRun.ConfigData.AddString("LASER_ID", serialNum);
            modRun.ConfigData.AddString("RESULT_DIR", this.CommonTestConfig.ResultsSubDirectory);
            modRun.ConfigData.AddDouble("Iphase_mA", this.ProgramCfgReader.GetDoubleParam("IphaseForPhaseScan_mA"));
            modRun.ConfigData.AddDouble("Irear_mA", this.ProgramCfgReader.GetDoubleParam("IrearForPhaseScan_mA"));
            modRun.ConfigData.AddDouble("IfsCon_mA", this.ProgramCfgReader.GetDoubleParam("IfsaForPhaseScan_mA"));
            modRun.ConfigData.AddDouble("IfsNonCon_mA", this.ProgramCfgReader.GetDoubleParam("IfsbForPhaseScan_mA"));
            modRun.ConfigData.AddDouble("Igain_mA", this.ProgramCfgReader.GetDoubleParam("IGainForPhaseScan_mA"));
            modRun.ConfigData.AddDouble("Isoa_mA", this.ProgramCfgReader.GetDoubleParam("ISoaForPhaseScan_mA"));
            modRun.ConfigData.AddDouble("IrearSoa_mA", this.ProgramCfgReader.GetDoubleParam("IRearSoaForPhaseScan_mA"));
            modRun.ConfigData.AddSint32("FrontPair", this.ProgramCfgReader.GetIntParam("cut_scan_fp"));
            modRun.ConfigData.AddReference("MzInstruments", new IlMzInstruments());
            modRun.ConfigData.AddDouble("MaxCurrentMzSweep_mA", ParamManager.Conditions.IsThermalPhase ? FinalCfgReader.GetDoubleParam("MzCtrlINominal_thermal_mA") : FinalCfgReader.GetDoubleParam("MzCtrlINominal_mA"));
            modRun.ConfigData.AddSint32("MzSweepNumber", this.numberPointSweep);

            modRun.ConfigData.AddDouble("IMBLeft_PRE_mA", this.preBurn_imbLeft_mA);
            modRun.ConfigData.AddDouble("IMBRight_PRE_mA", this.preBurn_imbRight_mA);
            modRun.ConfigData.AddDouble("FREQ_PRE_GHz", this.preBurn_freq_GHz);
            modRun.ConfigData.AddBool("IsToCheckSNMixUp", this.ProgramCfgReader.GetBoolParam("IsToCheckSNMixUp"));
            modRun.ConfigData.AddDouble("FreqToleranceForIMB_GHz", this.ProgramCfgReader.GetDoubleParam("FreqToleranceForIMB_GHz"));

            if (this.MainSpec.ParamLimitExists("SN_MIXUP_CHECK_LI_SWEEP_FILE"))
            {
                modRun.Limits.AddParameter(this.MainSpec, "SN_MIXUP_CHECK_LI_SWEEP_FILE", "SN_MIXUP_CHECK_LI_SWEEP_FILE");
            }
        }

        private void InitMod_LIVMeasurement(ITestEngineInit engine, InstrumentCollection instrs, ChassisCollection chassis, DUTObject dutObject)
        {
            ModuleRun modRun = engine.AddModuleRun("LIVMeasurement", "Mod_LIVMeasurement", string.Empty);
            modRun.Instrs.Add(instrs);
            modRun.Chassis.Add(chassis);

            modRun.ConfigData.AddString("TIME_STAMP", TestTimeStamp_Start);

            //modRun.Instrs.Add("dutTxfp", equipment.dutTxfp);
            //modRun.Instrs.Add("powerMeter", equipment.powerMeterSwitched);

            modRun.ConfigData.AddString("SweepFilePath", this.CommonTestConfig.ResultsSubDirectory);
            modRun.ConfigData.AddDouble("StartCurrent_mA", ProgramCfgReader.GetDoubleParam("LIV_Start_mA"));
            modRun.ConfigData.AddDouble("StopCurrent_mA", ProgramCfgReader.GetDoubleParam("LIV_Stop_mA"));
            modRun.ConfigData.AddSint32("NbrPoints", ProgramCfgReader.GetIntParam("LIV_NumOfPoints"));
            modRun.ConfigData.AddSint32("StepDelay_mS", ProgramCfgReader.GetIntParam("LIV_StepDelay_mS"));
            modRun.ConfigData.AddSint32("NumSmoothPoints_dLdI", ProgramCfgReader.GetIntParam("LIV_1stDiff_Smooth_NumPoints"));
            modRun.ConfigData.AddSint32("NumSmoothPoints_d2LdI2", ProgramCfgReader.GetIntParam("LIV_2ndDiff_Smooth_NumPoints"));
            modRun.ConfigData.AddString("SerialNumber", serialNum);

            modRun.ConfigData.AddDouble("PowerFixer", ProgramCfgReader.GetDoubleParam("PowerFixer"));
            modRun.ConfigData.AddDouble("minPwr_mW", ProgramCfgReader.GetDoubleParam("MinPwr_mW"));
            modRun.ConfigData.AddDouble("minD2ldi2", ProgramCfgReader.GetDoubleParam("MinD2ldi2"));

            //new params
            //ParamLimit plLASER_THRESHHOLD_CURRENT = spec.GetParamLimit("LASER_THRESHHOLD_CURRENT");
            //modRun.ConfigData.AddDouble("StartCurrent_mA", Convert.ToDouble(plLASER_THRESHHOLD_CURRENT.LowLimit.ValueToString()));
            //modRun.ConfigData.AddDouble("StopCurrent_mA", Convert.ToDouble(plLASER_THRESHHOLD_CURRENT.HighLimit.ValueToString()));

            modRun.ConfigData.AddDouble("TC_FRONT_PAIR", ProgramCfgReader.GetDoubleParam("TC_FRONT_PAIR"));
            modRun.ConfigData.AddDouble("TC_FSA", ProgramCfgReader.GetDoubleParam("TC_FSA"));
            modRun.ConfigData.AddDouble("TC_FSB", ProgramCfgReader.GetDoubleParam("TC_FSB"));
            modRun.ConfigData.AddDouble("TC_LEFT_BIAS", ProgramCfgReader.GetDoubleParam("TC_LEFT_BIAS"));
            modRun.ConfigData.AddDouble("TC_RIGHT_BIAS", ProgramCfgReader.GetDoubleParam("TC_RIGHT_BIAS"));
            modRun.ConfigData.AddDouble("TC_PHASE", ProgramCfgReader.GetDoubleParam("TC_PHASE"));
            modRun.ConfigData.AddDouble("TC_REAR", ProgramCfgReader.GetDoubleParam("TC_REAR"));
            modRun.ConfigData.AddDouble("TC_SOA", ProgramCfgReader.GetDoubleParam("TC_SOA"));
            modRun.ConfigData.AddDouble("TC_REARSOA", ProgramCfgReader.GetDoubleParam("TC_REARSOA"));
            modRun.ConfigData.AddDouble("MzPeak_LEFT_IMBForITH", this.MzPeak_LEFT_IMBForITH);
            modRun.ConfigData.AddDouble("MzPeak_RIGHT_IMBForITH", this.MzPeak_RIGHT_IMBForITH);
            modRun.ConfigData.AddDouble("PRE_ITH", this.Pre_ITH);

            #region Tie up limits

            Specification spec = this.MainSpec;

            if (MainSpec.ParamLimitExists("ITH"))
            {
                modRun.Limits.AddParameter(spec, "ITH", "ITH");
            }

            if (MainSpec.ParamLimitExists("LI_GAIN_SWEEP_FILE"))
            {
                modRun.Limits.AddParameter(spec, "LI_GAIN_SWEEP_FILE", "PLOT_LIV");
            }

            if (MainSpec.ParamLimitExists("ITH_SHIFT_RATIO"))
            {
                modRun.Limits.AddParameter(spec, "ITH_SHIFT_RATIO", "ITH_SHIFT_RATIO");
            }

            #endregion
        }

        private void InitMod_PhaseCutScan(ITestEngineInit engine, InstrumentCollection instrs, ChassisCollection chassis, DUTObject dutObject)
        {
            ModuleRun modRun = engine.AddModuleRun("DoPhaseCutScan", "Mod_PhaseScan", string.Empty);
            modRun.Instrs.Add(instrs);
            modRun.Chassis.Add(chassis);

            modRun.ConfigData.AddString("TIME_STAMP", TestTimeStamp_Start);
            modRun.ConfigData.AddString("LASER_ID", serialNum);
            modRun.ConfigData.AddString("RESULT_DIR", this.CommonTestConfig.ResultsSubDirectory);
            modRun.ConfigData.AddString("PhaseSetupFile_Path", this.PhaseCurrentSetupFile);

            modRun.ConfigData.AddDouble("Iphase_mA", this.ProgramCfgReader.GetDoubleParam("IphaseForPhaseScan_mA"));
            modRun.ConfigData.AddDouble("Irear_mA", this.preBurn_Irear_mA);
            modRun.ConfigData.AddDouble("IfsCon_mA", this.preBurn_constant);
            modRun.ConfigData.AddDouble("IfsNonCon_mA", this.preBurn_nonConstant);
            modRun.ConfigData.AddDouble("Igain_mA", this.preBurn_Igain_mA);
            modRun.ConfigData.AddDouble("Isoa_mA", this.preBurn_Isoa_mA);
            modRun.ConfigData.AddDouble("IrearSoa_mA", this.ProgramCfgReader.GetDoubleParam("IRearSoaForPhaseScan_mA"));
            modRun.ConfigData.AddSint32("FrontPair", this.preBurn_fp);
            modRun.ConfigData.AddDouble("ImbLeft_mA", this.preBurn_imbLeft_mA);
            modRun.ConfigData.AddDouble("ImbRight_mA", this.preBurn_imbRight_mA);
            modRun.ConfigData.AddDouble("RTH_Offset_ohm", this.RTH_Offset_ohm);//Jack.zhang for post limit file RTH update. 2012-05-09

            modRun.ConfigData.AddBool("IsPostBurn", true);
            modRun.ConfigData.AddBool("ReturnNotTestedResult", false);
            modRun.ConfigData.AddDouble("preBurn_braggFreq_GHz", this.preBurn_braggFreq_GHz);
            modRun.ConfigData.AddSint32("preLastJumpIndex", this.preLastJumpIndex);
            modRun.ConfigData.AddSint32("preLastModeLength", this.preLastModeLength);
            modRun.ConfigData.AddSint32("preJumpCount", this.preJumpCount);
            modRun.ConfigData.AddSint32("prelastsecondJumpIndex", this.prelastsecondJumpIndex);
            modRun.ConfigData.AddString("FSBCurrentSetupFile_Path", this.FSBCurrentSetupFile);

            double rth_Limit_Min_ohm = Convert.ToDouble(this.MainSpec.GetParamLimit("RTH_MIN").LowLimit.ValueToString());
            double rth_Limit_Max_ohm = Convert.ToDouble(this.MainSpec.GetParamLimit("RTH_MAX").HighLimit.ValueToString());

            modRun.ConfigData.AddDouble("RTH_LIMIT_MIN_ohm", rth_Limit_Min_ohm);
            modRun.ConfigData.AddDouble("RTH_LIMIT_MAX_ohm", rth_Limit_Max_ohm);

            double rth_Min_ohm = (rth_Limit_Min_ohm + rth_Limit_Max_ohm) / 2;
            double rth_Max_ohm = rth_Min_ohm;

            modRun.ConfigData.AddDouble("RTH_MIN_ohm", rth_Min_ohm);
            modRun.ConfigData.AddDouble("RTH_MAX_ohm", rth_Max_ohm);

            modRun.ConfigData.AddDouble("StepWidthToReadRTH", this.ProgramCfgReader.GetDoubleParam("StepWidthToReadRTH"));

            #region Tie up limits

            Specification spec = this.MainSpec;

            modRun.Limits.AddParameter(spec, "PHASE_SCAN_FREQ_JUMP_OK", "PHASE_SCAN_FREQ_JUMP_OK");
            modRun.Limits.AddParameter(spec, "BRAGG_FREQ", "Bragg_Freq_GHz");
            modRun.Limits.AddParameter(spec, "LAST_JUMP_INDEX", "Last_Jump_IndexPoint");
            modRun.Limits.AddParameter(spec, "DELTA_BRAGG_FREQ", "Delta_Bragg_Freq_GHz");
            //modRun.Limits.AddParameter(spec, "MODE_SHIFT_RATIO", "Mode_Shift_Ratio_Percent");
            modRun.Limits.AddParameter(spec, "PHASE_CUT_POSTBURN_FILE", "Mz_Phase_Cut_file");
            modRun.Limits.AddParameter(spec, "IASIC_PHASESCAN", "Iasic_PhaseScan");
            #endregion Tie up limits
        }

        /// <summary>
        ///  Initalzie the rear cut scan to get the sweep data.
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="instrs"></param>
        /// <param name="chassis"></param>
        /// <param name="dutObject"></param>
        private void InitMod_RearCutScan(ITestEngineInit engine, InstrumentCollection instrs, ChassisCollection chassis, DUTObject dutObject)
        {
            ModuleRun modRun = engine.AddModuleRun("DoRearCutScan", "Mod_RearCutScan", string.Empty);
            modRun.Instrs.Add(instrs);
            modRun.Chassis.Add(chassis);

            modRun.ConfigData.AddString("TIME_STAMP", TestTimeStamp_Start);
            modRun.ConfigData.AddString("LASER_ID", serialNum);
            modRun.ConfigData.AddString("RESULT_DIR", this.CommonTestConfig.ResultsSubDirectory);
            modRun.ConfigData.AddString("RearSetupFile_Path", this.RearCurrentSetupFile);
            modRun.ConfigData.AddString("Iphase_mA", this.ProgramCfgReader.GetStringParam("IphaseForPhaseScan_mA"));
            modRun.ConfigData.AddString("Irear_mA", this.preBurn_Irear_mA.ToString());
            modRun.ConfigData.AddString("IfsCon_mA", this.preBurn_constant.ToString());
            modRun.ConfigData.AddString("IfsNonCon_mA", this.preBurn_nonConstant.ToString());
            modRun.ConfigData.AddString("Igain_mA", this.preBurn_Igain_mA.ToString());
            modRun.ConfigData.AddString("Isoa_mA", this.preBurn_Isoa_mA.ToString());
            modRun.ConfigData.AddString("IrearSoa_mA", this.ProgramCfgReader.GetStringParam("IRearSoaForPhaseScan_mA"));
            modRun.ConfigData.AddString("FrontPair", this.preBurn_fp.ToString());
            modRun.ConfigData.AddDouble("ImbLeft_mA", double.Parse(preBurn_imbLeft_mA.ToString()));
            modRun.ConfigData.AddDouble("ImbRight_mA", double.Parse(preBurn_imbRight_mA.ToString()));
            modRun.ConfigData.AddDouble("RTH_Offset_ohm", this.RTH_Offset_ohm);//Jack.zhang for post limit file RTH update. 2012-05-09

            modRun.ConfigData.AddBool("IsPostBurn", false);
            modRun.ConfigData.AddBool("ReturnNotTestedResult", false);

            double rth_Limit_Min_ohm = Convert.ToDouble(this.MainSpec.GetParamLimit("RTH_MIN").LowLimit.ValueToString());
            double rth_Limit_Max_ohm = Convert.ToDouble(this.MainSpec.GetParamLimit("RTH_MAX").HighLimit.ValueToString());

            modRun.ConfigData.AddDouble("RTH_LIMIT_MIN_ohm", rth_Limit_Min_ohm);
            modRun.ConfigData.AddDouble("RTH_LIMIT_MAX_ohm", rth_Limit_Max_ohm);

            modRun.ConfigData.AddDouble("StepWidthToReadRTH", this.ProgramCfgReader.GetDoubleParam("StepWidthToReadRTH"));

            // Add conditions to check if POST & PRE are the same device - chongjian.liang 2013.3.20
            modRun.ConfigData.AddBool("IsToCheckSNMixUp", this.ProgramCfgReader.GetBoolParam("IsToCheckSNMixUp"));
            modRun.ConfigData.AddDouble("FREQ_PRE_GHz", this.preBurn_freq_GHz);

            #region Tie up limits

            Specification spec = this.MainSpec;

            modRun.Limits.AddParameter(spec, "RTH_MIN", "RTH_MIN_ohm");
            modRun.Limits.AddParameter(spec, "RTH_MAX", "RTH_MAX_ohm");
            modRun.Limits.AddParameter(spec, "REAR_CUT_POSTBURN_FILE", "Mz_Rear_Cut_file");
            modRun.Limits.AddParameter(spec, "IASIC_REARSCAN", "Iasic_RearScan");
            #endregion
        }

        /// <summary>
        /// Configure Case Temperature module
        /// </summary>
        private void CaseTempControl_InitModule(ITestEngineInit engine, string moduleName, Double currentTemp, Double tempSetPoint)
        {
            // Initialise module
            ModuleRun modRun = engine.AddModuleRun("TecCaseControl", "SimpleTempControl", string.Empty);
            //modRun = engine.AddModuleRun(moduleName, "SimpleTempControl", "");

            // Add instrument
            modRun.Instrs.Add("Controller", (Instrument)TecCase);

            // Add config data
            TempTablePoint tempCal = TempConfig.GetTemperatureCal(currentTemp, tempSetPoint)[0];
            DatumList tempConfig = new DatumList();
            int guiTimeOut_ms = int.Parse(FinalCfgReader.GetStringParam("CaseTempGui_UpdateTime_mS"));

            // Timeout = 2 * calibrated time, minimum of 60secs
            double timeout_s = Math.Max(tempCal.DelayTime_s * 15, 60);
            tempConfig.AddDouble("datumMaxExpectedTimeForOperation_s", timeout_s);
            // Stabilisation time = 0.5 * calibrated time
            tempConfig.AddDouble("RqdStabilisationTime_s", tempCal.DelayTime_s);
            tempConfig.AddSint32("TempTimeBtwReadings_ms", guiTimeOut_ms);
            // Actual temperature to set
            tempConfig.AddDouble("SetPointTemperature_C", tempSetPoint + tempCal.OffsetC);
            tempConfig.AddDouble("TemperatureTolerance_C", tempCal.Tolerance_degC);
            modRun.ConfigData.AddListItems(tempConfig);
        }

        /// <summary>
        /// Configure a Device Temperature Control module
        /// </summary>
        private void DeviceTempControl_InitModule(ITestEngineInit engine, string moduleName, IInstType_SimpleTempControl tecController, string configPrefix)
        {
            // Initialise module
            ModuleRun modRun = engine.AddModuleRun("TecDsdbrControl", "SimpleTempControl", string.Empty);

            // Add instrument
            modRun.Instrs.Add("Controller", (Instrument)tecController);

            //load config
            double timeout_S = FinalCfgReader.GetDoubleParam(configPrefix + "_Timeout_S");
            double stabTime_S = FinalCfgReader.GetDoubleParam(configPrefix + "_StabiliseTime_S");
            int updateTime_mS = FinalCfgReader.GetIntParam(string.Format("{0}_UpdateTime_mS", configPrefix));
            double sensorSetPoint_C = this.TC_LASER_TEMP_C;
            double tolerance_C = FinalCfgReader.GetDoubleParam(string.Format("{0}_Tolerance_C", configPrefix));

            // Add config data
            DatumList tempConfig = new DatumList();
            tempConfig.AddDouble("datumMaxExpectedTimeForOperation_s", timeout_S);
            tempConfig.AddDouble("RqdStabilisationTime_s", stabTime_S);
            tempConfig.AddSint32("TempTimeBtwReadings_ms", updateTime_mS);
            tempConfig.AddDouble("SetPointTemperature_C", sensorSetPoint_C);
            tempConfig.AddDouble("TemperatureTolerance_C", tolerance_C);

            modRun.ConfigData.AddListItems(tempConfig);
        }

        private void InitTecDsdbr(ITestEngineInit engine, InstrumentCollection instrs, ChassisCollection chassis, DUTObject dutObject)
        {
            ModuleRun modRun = engine.AddModuleRun("TecDsdbrControl", "SimpleTempControl", string.Empty);

            DatumList tempConfig = new DatumList();
            tempConfig.AddDouble("SetPointTemperature_C", TecDSDBRCfgReader.GetDoubleParam("SensorTemperatureSetPoint_C"));
            tempConfig.AddDouble("TemperatureTolerance_C", TecDSDBRCfgReader.GetDoubleParam("Temperature_Tolerance"));
            tempConfig.AddDouble("RqdStabilisationTime_s", TecDSDBRCfgReader.GetIntParam("StabilisationTime_s"));
            tempConfig.AddDouble("datumMaxExpectedTimeForOperation_s", TecDSDBRCfgReader.GetIntParam("TotalSettleTime_s"));
            tempConfig.AddSint32("TempTimeBtwReadings_ms", 50);

            modRun.ConfigData.AddListItems(tempConfig);

            modRun.Instrs.Add("Controller", (Instrument)instrs["TecDsdbr"]);

            modRun.Chassis.Add(chassis);
        }

        private void InitTecCase(ITestEngineInit engine, InstrumentCollection instrs, ChassisCollection chassis, DUTObject dutObject)
        {
            ModuleRun modRun = engine.AddModuleRun("TecCaseControl", "SimpleTempControl", string.Empty);

            DatumList tempConfig = new DatumList();

            tempConfig.AddDouble("SetPointTemperature_C", TecCaseCfgReader.GetDoubleParam("TecCase_SensorTemperatureSetPoint_C"));
            tempConfig.AddDouble("TemperatureTolerance_C", TecCaseCfgReader.GetDoubleParam("TecCase_Temperature_Tolerance"));
            tempConfig.AddDouble("RqdStabilisationTime_s", TecCaseCfgReader.GetIntParam("StabilisationTime_s"));
            tempConfig.AddDouble("datumMaxExpectedTimeForOperation_s", TecCaseCfgReader.GetIntParam("TotalSettleTime_s"));
            tempConfig.AddSint32("TempTimeBtwReadings_ms", 50);

            modRun.Instrs.Add("Controller", (Instrument)instrs["TecCase"]);
            modRun.Chassis.Add(chassis);

            modRun.ConfigData.AddListItems(tempConfig);
        }

        #endregion

        #region Run program

        public override void Run(ITestEngineRun engine, DUTObject dutObject, InstrumentCollection instrs, ChassisCollection chassis)
        {
            InstCommands.CloseInstToLoadDUT(engine, FCUMKI_Source, ASIC_VccSource, ASIC_VeeSource, TecDsdbr);

            //engine.ShowContinueUserQuery("Please load the DUT and click Continue to test\n\n请放置器件，点击 Continue 测试");

            int rerunCount = 0;

            bool isRTHFailed = false;

            do
            {
                this.fail_Abort_Reason = "";

                #region Pin Check

                engine.RunModule("Mod_PinCheck");

                ModRunData Pin_Check_Results = engine.GetModuleRunData("Mod_PinCheck");

                bool isPinCheckPass = Pin_Check_Results.ModuleData.ReadBool("Pin_Check_Pass");

                if (!isPinCheckPass)
                {
                    this.fail_Abort_Reason += Pin_Check_Results.ModuleData.ReadString("ErrorInformation");
                    return;
                }
                #endregion

                // Asic Part
                //bool Is_ASIC5112 = FinalCfgReader.GetBoolParam("Is_ASIC5112");
                Fcu2Asic.SetupChannelCalibration();
                ConfigureFCU2AsicInstruments();

                // Close DUT TEC to prepare to read DUT TEC AC resistance - chongjian.liang 2013.4.8

                this.TecDsdbr.OutputEnabled = false;

                #region Set and wait for DUT case temperature to stabilise

                ModuleRunReturn caseControlResult = engine.RunModule("TecCaseControl");

                if (caseControlResult.ModuleRunData.ModuleData.IsPresent("ErrorInformation")) {
                    this.failModeCheck.RaiseError_Prog_NonParamFail(engine, caseControlResult.ModuleRunData.ModuleData.ReadString("ErrorInformation"), FailModeCategory_Enum.UN);
                }
                #endregion

                #region Check DUT TEC AC Resistance - chongjian.liang 2013.3.20

                ModuleRunReturn ACRResult = engine.RunModule("Mod_TEC_ACResistance");

                if (ACRResult.ModuleRunStatus.Status == MultiSpecPassFail.AllFail)
                {
                    this.fail_Abort_Reason += "Test aborted due to DUT_TEC_ACR failed. Please contact product engineer.";

                    return;
                }

                #endregion

                #region Set and wait for DUT chip temperature to stabilise

                ModuleRunReturn dsdbrControlResult = engine.RunModule("TecDsdbrControl");

                if (dsdbrControlResult.ModuleRunData.ModuleData.IsPresent("ErrorInformation")) {
                    this.failModeCheck.RaiseError_Prog_NonParamFail(engine, dsdbrControlResult.ModuleRunData.ModuleData.ReadString("ErrorInformation"), FailModeCategory_Enum.UN);
                }
                #endregion

                #region LIVMeasurement

                ModuleRunReturn LIVMeasurementResults = engine.RunModule("LIVMeasurement");

                // Gain sweeping range enlarged to 60mA when ITH = 0. - chongjian.liang, requested by James Zhan
                if (LIVMeasurementResults.ModuleRunData.ModuleData.ReadDouble("ITH") < 1)
                {
                    ModuleRun mrLIVMeasurement = engine.GetModuleRun("LIVMeasurement");

                    mrLIVMeasurement.ConfigData.UpdateDouble("StopCurrent_mA", 60);

                    LIVMeasurementResults = engine.RunModule("LIVMeasurement");
                }
                #endregion

                #region Run DoPhaseCutScan -- chongjian.liang - 2012.8.21

                // Pass fsb current to PhaseCutScan
                ModuleRun modrun = engine.GetModuleRun("DoPhaseCutScan");

                #region Update ModuleRun config

                bool isReturnDummyResult;

                if (rerunCount < 2) // Run DoPhaseCutScan the first 2 times
                {
                    isReturnDummyResult = true;
                }
                else // Run DoPhaseCutScan the 3rd time
                {
                    isReturnDummyResult = false;
                }

                if (!modrun.ConfigData.IsPresent("IsReturnDummyResult"))
                {
                    modrun.ConfigData.AddBool("IsReturnDummyResult", isReturnDummyResult);
                }
                else
                {
                    modrun.ConfigData.UpdateBool("IsReturnDummyResult", isReturnDummyResult);
                }
                #endregion

                // If DoPhaseCutScan failed, the operator will continue the program manually
                ModuleRunReturn phaseScanResult = engine.RunModule("DoPhaseCutScan");

                if (phaseScanResult.ModuleRunData.ModuleData.IsPresent("IsRTHFailed"))
                {
                    isRTHFailed = phaseScanResult.ModuleRunData.ModuleData.ReadBool("IsRTHFailed");

                    // Rerun the all modules if RTH failed within 3 times
                    if (isRTHFailed && ++rerunCount < 3)
                    {
                        engine.ShowContinueUserQuery("RTH failed; Please reload the DUT, then click the Continue to proceed. 请重新装载器件到夹具上，然后点 Continue 继续.");

                        engine.SendStatusMsg("RTH failed, start over all modules.");

                        continue;
                    }
                }

                if (phaseScanResult.ModuleRunData.ModuleData.IsPresent("FAIL_ABORT_REASON"))
                {
                    this.fail_Abort_Reason += phaseScanResult.ModuleRunData.ModuleData.ReadString("FAIL_ABORT_REASON");
                }

                this.mode_Shift_Ratio = phaseScanResult.ModuleRunData.ModuleData.ReadDouble("Mode_Shift_Ratio_Percent");

                #endregion

                #region Run DoRearCutScan -- chongjian.liang - 2012.8.21

                modrun = engine.GetModuleRun("DoRearCutScan");

                #region Update ModuleRun config

                // If RTH failed, not really test the module to save time.
                if (isRTHFailed)
                {
                    modrun.ConfigData.UpdateBool("ReturnNotTestedResult", true);
                }

                double rth_Min_ohm = phaseScanResult.ModuleRunData.ModuleData.ReadDouble("RTH_MIN_ohm");
                double rth_Max_ohm = phaseScanResult.ModuleRunData.ModuleData.ReadDouble("RTH_MAX_ohm");

                if (!modrun.ConfigData.IsPresent("RTH_MIN_ohm"))
                {
                    modrun.ConfigData.AddDouble("RTH_MIN_ohm", rth_Min_ohm);
                }
                else
                {
                    modrun.ConfigData.UpdateDouble("RTH_MIN_ohm", rth_Min_ohm);
                }

                if (!modrun.ConfigData.IsPresent("RTH_MAX_ohm"))
                {
                    modrun.ConfigData.AddDouble("RTH_MAX_ohm", rth_Max_ohm);
                }
                else
                {
                    modrun.ConfigData.UpdateDouble("RTH_MAX_ohm", rth_Max_ohm);
                }

                if (rerunCount < 2) // Run DoRearCutScan the first 2 times
                {
                    isReturnDummyResult = true;
                }
                else // Run DoRearCutScan the 3rd time
                {
                    isReturnDummyResult = false;
                }

                if (!modrun.ConfigData.IsPresent("IsReturnDummyResult"))
                {
                    modrun.ConfigData.AddBool("IsReturnDummyResult", isReturnDummyResult);
                }
                else
                {
                    modrun.ConfigData.UpdateBool("IsReturnDummyResult", isReturnDummyResult);
                }
                #endregion

                // If DoRearCutScan failed, the operator will continue the program manually
                ModuleRunReturn rearScanResult = engine.RunModule("DoRearCutScan");

                if (rearScanResult.ModuleRunData.ModuleData.IsPresent("IsRTHFailed"))
                {
                    isRTHFailed = rearScanResult.ModuleRunData.ModuleData.ReadBool("IsRTHFailed");

                    // Rerun the all modules if RTH failed within 3 times
                    if (isRTHFailed && ++rerunCount < 3)
                    {
                        engine.ShowContinueUserQuery("RTH failed; Please reload the DUT, then click the Continue to proceed. 请重新装载器件到夹具上，然后点 Continue 继续.");

                        engine.SendStatusMsg("RTH failed, start over all modules.");

                        continue;
                    }
                }

                if (rearScanResult.ModuleRunData.ModuleData.IsPresent("FAIL_ABORT_REASON"))
                {
                    this.fail_Abort_Reason += rearScanResult.ModuleRunData.ModuleData.ReadString("FAIL_ABORT_REASON");
                }

                #endregion

                #region Check if POST & PRE is the same device by comparing IMB under the most similar conditions  - chongjian.liang 2013.3.20

                modrun = engine.GetModuleRun("DoHittMzLiSweep");

                modrun.ConfigData.AddDouble("FreqClosestToPREs_RearScan", rearScanResult.ModuleRunData.ModuleData.ReadDouble("FreqClosestToPREs"));

                modrun.ConfigData.AddReference("ISetup_FreqClosestToPREs_RearScan", rearScanResult.ModuleRunData.ModuleData.ReadReference("ISetup_FreqClosestToPREs"));

                ModuleRunReturn liSweepResults = engine.RunModule("DoHittMzLiSweep");

                if (liSweepResults.ModuleRunData.ModuleData.IsPresent("IMBLeft_PRE_Minus_POST_mA"))
                {
                    this.IMBLeft_PRE_Minus_POST_mA = liSweepResults.ModuleRunData.ModuleData.ReadDouble("IMBLeft_PRE_Minus_POST_mA");
                }

                //bool isSNMixUp = liSweepResults.ModuleRunData.ModuleData.ReadBool("IsSNMixUp");

                //if (isSNMixUp)
                //{
                //    ButtonId response = engine.ShowContinueCancelUserQuery("Please check if you have inputted the correct SN, if it does, click Continue, otherwise, click Cancel and retest the DUT.请检查输入的器件序列号是否正确, 如果正确, 点Continue, 否则, 点Cancel然后重测.");

                //    if (response == ButtonId.Cancel)
                //    {
                //        engine.ErrorInProgram("Test is cancelled due to SN wrong inputted.");
                //    }
                //}

                #endregion
            }
            while (isRTHFailed && rerunCount < 3);
        }

        public override void PostRun(ITestEnginePostRun engine, DUTObject dutObject, InstrumentCollection instrs, ChassisCollection chassis)
        {
            InstCommands.CloseInstToUnLoadDUT(engine, Fcu2Asic, FCUMKI_Source, ASIC_VccSource, ASIC_VeeSource, TecDsdbr, TecCase, 25);

            TestProgramCore.CloseInstrs_Wavemeter();

            #region light_tower

            /* Set up Serial Binary Comms */
            // _serialPort = buildSerPort(comPort, rate);
            if (LightTowerConfig.GetBoolParam("LightExist"))
            {
                try
                {

                    _serialPort = this.buildSerPort(LightTowerConfig.GetStringParam("ComNum"), LightTowerConfig.GetStringParam("BaudRate"));
                    _serialPort.Close();
                    if (!_serialPort.IsOpen)
                    {
                        _serialPort.Open();
                    }

                    // Add the light tower color.
                    _serialPort.WriteLine(LightTowerConfig.GetStringParam("LightYellow"));
                    _serialPort.Close();
                }
                catch { }

            }
            #endregion
        }

        public override void DataWrite(ITestEngineDataWrite engine, DUTObject dutObject, DUTOutcome dutOutcome, TestData results, UserList userList)
        {
            //Output main teststage
            StringDictionary mainkeys = new StringDictionary();
            mainkeys.Add("SCHEMA", "hiberdb");
            mainkeys.Add("TEST_STAGE", dutObject.TestStage.ToLower());
            mainkeys.Add("DEVICE_TYPE", dutObject.PartCode);
            mainkeys.Add("SPECIFICATION", this.MainSpecName);
            mainkeys.Add("SERIAL_NO", dutObject.SerialNumber);
            // Tell Test Engine about it...

            engine.SetDataKeysPerSpec(MainSpecName, mainkeys);

            this.traceDataList = new DatumList();

            this.traceDataList.AddSint32("NODE", dutObject.NodeID);
            traceDataList.AddString("EQUIP_ID", dutObject.EquipmentID);

            traceDataList.AddString("OPERATOR_ID", userList.InitialUser);

            double testTime = Math.Round(engine.GetTestTime() / 60, 2);
            double processTime = Math.Round(engine.TestProcessTime / 60, 2);
            double laborTime = testTime - processTime;

            traceDataList.AddDouble("TEST_TIME", testTime);

            if (this.MainSpec.ParamLimitExists("LABOUR_TIME"))
            {
                traceDataList.AddDouble("LABOUR_TIME", laborTime);
            }
            traceDataList.AddString("SOFTWARE_ID", dutObject.ProgramPluginName + " Version: " + dutObject.ProgramPluginVersion + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString());
            traceDataList.AddString("PRODUCT_CODE", dutObject.PartCode.ToString());
            traceDataList.AddString("TIME_DATE", TestTimeStamp_Start);
            traceDataList.AddDouble("IFSA_CUTSCAN", preBurn_constant);
            traceDataList.AddDouble("IFSB_CUTSCAN", preBurn_nonConstant);
            traceDataList.AddDouble("FP_CUTSCAN", double.Parse(this.preBurn_fp.ToString()));
            traceDataList.AddDouble("IGAIN_CUTSCAN", preBurn_Igain_mA);
            traceDataList.AddDouble("IREAR_CUTSCAN", preBurn_Irear_mA);
            traceDataList.AddDouble("ISOA_CUTSCAN", preBurn_Isoa_mA);
            traceDataList.AddDouble("IPHASE_CUTSCAN", this.ProgramCfgReader.GetDoubleParam("IphaseForPhaseScan_mA"));

            traceDataList.AddString("CHIP_ID", dutObject.Attributes.ReadString("x_chip_id"));
            traceDataList.AddString("WAFER_ID", dutObject.Attributes.ReadString("x_wafer_id"));
            traceDataList.AddString("COC_SN", dutObject.Attributes.ReadString("x_coc_sn"));
            /* traceDataList.AddString("COC_SN", ParamManager.Conditions.COC_SN);
             traceDataList.AddString("WAFER_ID", ParamManager.Conditions.WAFER_ID);
             traceDataList.AddString("CHIP_ID", ParamManager.Conditions.CHIP_ID);
             */
            //traceDataList.AddDouble("LASER_RTH", RTH_Actual_ohm);//this.TecDsdbr.SensorResistanceActual_ohm);
            traceDataList.AddDouble("MZ_IMB_L_TRANSMI_MA", this.preBurn_imbLeft_mA);
            traceDataList.AddDouble("MZ_IMB_R_TRANSMI_MA", this.preBurn_imbRight_mA);
            traceDataList.AddDouble("IMBLEFT_PRE_MINUS_POST", this.IMBLeft_PRE_Minus_POST_mA);

            //XIAOJIANG 2017.03.20
            if (MainSpec.ParamLimitExists("LOT_TYPE"))
                if (dutObject.Attributes.IsPresent("lot_type"))
                    DatumListAddOrUpdate(ref traceDataList, "LOT_TYPE", dutObject.Attributes.ReadString("lot_type"));
                //traceDataList.AddString("LOT_TYPE", dutObject.Attributes.ReadString("lot_type"));
                else
                    DatumListAddOrUpdate(ref traceDataList, "LOT_TYPE", "Unknown");
            //traceDataList.AddString("LOT_TYPE", "Unknown");

            if (MainSpec.ParamLimitExists("FACTORY_WORKS_LOTTYPE"))
                if (dutObject.Attributes.IsPresent("lot_type"))
                    DatumListAddOrUpdate(ref traceDataList, "FACTORY_WORKS_LOTTYPE", dutObject.Attributes.ReadString("lot_type"));
                //traceDataList.AddString("FACTORY_WORKS_LOTTYPE", dutObject.Attributes.ReadString("lot_type"));
                else
                    DatumListAddOrUpdate(ref traceDataList, "FACTORY_WORKS_LOTTYPE", "Unknown");
            //traceDataList.AddString("FACTORY_WORKS_LOTTYPE", "Unknown");

            if (MainSpec.ParamLimitExists("LOT_ID"))
                DatumListAddOrUpdate(ref traceDataList, "LOT_ID", dutObject.BatchID);
            //traceDataList.AddString("LOT_ID", dutObject.BatchID);
            if (MainSpec.ParamLimitExists("FACTORY_WORKS_LOTID"))
                DatumListAddOrUpdate(ref traceDataList, "FACTORY_WORKS_LOTID", dutObject.BatchID);
            //traceDataList.AddString("FACTORY_WORKS_LOTID", dutObject.BatchID);
            //END ADD XIAOJIANG 2017.03.20

            if (this.fail_Abort_Reason != null && this.fail_Abort_Reason.Length != 0)
            {
                if (this.fail_Abort_Reason.Length > 80)
                    this.fail_Abort_Reason = this.fail_Abort_Reason.Substring(0, 80);
                traceDataList.AddString("COMMENTS", this.fail_Abort_Reason);
            }
            else
            {
                traceDataList.AddString("COMMENTS", engine.GetProgramRunComments());
            }

            if (engine.GetProgramRunStatus() != ProgramStatus.Success)
            {
                traceDataList.AddString("TEST_STATUS", engine.GetProgramRunStatus().ToString());
            }
            else
            {
                traceDataList.AddString("TEST_STATUS", this.MainSpec.Status.Status.ToString());
            }

            if (MainSpec.ParamLimitExists("TC_DSDBR_TEMP"))
                traceDataList.AddDouble("TC_DSDBR_TEMP", this.TC_LASER_TEMP_C);
            if (MainSpec.ParamLimitExists("TC_T_CASE_MID"))
                traceDataList.AddDouble("TC_T_CASE_MID", this.TC_CASE_MID_C);

            if (MainSpec.ParamLimitExists("MODE_SHIFT_RATIO"))
                traceDataList.AddDouble("MODE_SHIFT_RATIO", this.mode_Shift_Ratio);

            if (MainSpec.ParamLimitExists("RETEST"))
            {
                traceDataList.AddString("RETEST", this.retestCount.ToString());
            }

            if (!dutOutcome.DUTData.IsPresent("RETEST"))
            {
                dutOutcome.DUTData.AddSint32("RETEST", this.retestCount);
            }

            this.failModeCheck.CheckMainSpec(engine, dutObject, this.MainSpec, this.CommonTestConfig.PCASFailMode_FilePath, ref traceDataList);

            // pick the specification to add this data to...
            engine.SetTraceData(MainSpecName, traceDataList);
        }

        #endregion

        #region Private functions

        protected override void GetPreBurnStageData(ITestEngineInit engine, DUTObject dutObject)
        {
            base.GetPreBurnStageData(engine, dutObject);

            IDataRead dataRead = engine.GetDataReader("PCAS_SHENZHEN");
            DatumList PreBurnData = null;

            StringDictionary PreBurnKeys = new StringDictionary();
            PreBurnKeys.Add("SCHEMA", "hiberdb");
            PreBurnKeys.Add("TEST_STAGE", "pre burn");
            PreBurnKeys.Add("SERIAL_NO", dutObject.SerialNumber);
            PreBurnData = dataRead.GetLatestResults(PreBurnKeys, false);

            try
            {
                if (PreBurnData != null
                    && PreBurnData.Count > 0)
                {
                    this.preBurn_constant = float.Parse((PreBurnData.ReadDouble("IFSA_CUTSCAN")).ToString());
                    this.preBurn_nonConstant = float.Parse((PreBurnData.ReadDouble("IFSB_CUTSCAN")).ToString());
                    this.preBurn_fp = int.Parse((PreBurnData.ReadDouble("FP_CUTSCAN")).ToString());
                    this.preBurn_Igain_mA = float.Parse((PreBurnData.ReadDouble("IGAIN_CUTSCAN")).ToString());
                    this.preBurn_Irear_mA = float.Parse((PreBurnData.ReadDouble("IREAR_CUTSCAN")).ToString());
                    this.preBurn_Isoa_mA = float.Parse((PreBurnData.ReadDouble("ISOA_CUTSCAN")).ToString());
                    this.preBurn_imbLeft_mA = float.Parse((PreBurnData.ReadDouble("MZ_IMB_L_TRANSMI_MA")).ToString());
                    this.preBurn_imbRight_mA = float.Parse((PreBurnData.ReadDouble("MZ_IMB_R_TRANSMI_MA")).ToString());
                    this.preBurn_freq_GHz = PreBurnData.ReadDouble("FREQ_MZ_SWEEP");
                    this.preBurn_braggFreq_GHz = PreBurnData.ReadDouble("BRAGG_FREQ");
                    this.preLastJumpIndex = PreBurnData.ReadSint32("LAST_JUMP_INDEX");
                    this.preLastModeLength = PreBurnData.ReadSint32("LAST_MODE_LENGTH");
                    this.preJumpCount = PreBurnData.ReadSint32("COUNT_JUMPPOINT");
                    this.prelastsecondJumpIndex = PreBurnData.ReadSint32("PRE_LAST_JUMP_INDEX");

                    if (PreBurnData.IsPresent("TC_DSDBR_TEMP"))//temp for add DSDBR_temp in limit file jack.zhang 2012-04-19
                    {
                        this.TC_LASER_TEMP_C = PreBurnData.ReadDouble("TC_DSDBR_TEMP");
                        this.RTH_Offset_ohm = 0;
                    }
                    else
                    {
                        this.TC_LASER_TEMP_C = FinalCfgReader.GetDoubleParam("TecDsdbr_SensorTemperatureSetPoint_C");
                        this.RTH_Offset_ohm = this.ProgramCfgReader.GetDoubleParam("RTH_Offset_ohm");
                    }
                    if (PreBurnData.IsPresent("TC_T_CASE_MID"))//temp for add DSDBR_temp in limit file jack.zhang 2012-04-19
                        this.TC_CASE_MID_C = PreBurnData.ReadDouble("TC_T_CASE_MID");
                    else
                        this.TC_CASE_MID_C = FinalCfgReader.GetDoubleParam("TecCase_SensorTemperatureSetPoint_C");

                    if (PreBurnData.IsPresent("MzPeak_LEFT_IMBForITH"))//temp for add DSDBR_temp in limit file jack.zhang 2012-04-19
                        this.MzPeak_LEFT_IMBForITH = PreBurnData.ReadDouble("MzPeak_LEFT_IMBForITH");
                    else
                        this.MzPeak_LEFT_IMBForITH = PreBurnData.ReadDouble("MZ_IMB_L_TRANSMI_MA");

                    if (PreBurnData.IsPresent("MzPeak_RIGHT_IMBForITH"))//temp for add DSDBR_temp in limit file jack.zhang 2012-04-19
                        this.MzPeak_RIGHT_IMBForITH = PreBurnData.ReadDouble("MzPeak_RIGHT_IMBForITH");
                    else
                        this.MzPeak_RIGHT_IMBForITH = PreBurnData.ReadDouble("MZ_IMB_R_TRANSMI_MA");

                    if (PreBurnData.IsPresent("ITH"))//temp for add DSDBR_temp in limit file jack.zhang 2012-04-19
                        this.Pre_ITH = PreBurnData.ReadDouble("ITH");
                    else
                        this.Pre_ITH = 0;
                }
                else
                {
                    this.failModeCheck.RaiseError_Prog_NonParamFail(engine, string.Format("No valid Pre burn phase cut scan Data for {0}\n Terminating this test!", dutObject.SerialNumber), FailModeCategory_Enum.OP);
                }
            }
            catch (Exception e)
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, e.Message, FailModeCategory_Enum.UN);
            }

        }

        private void InitialiseFcu(Inst_Fcu2Asic Fcu)
        {
            Fcu.SetDefaultState();
        }

        /// <summary>
        /// Configure a TEC controller using config data
        /// </summary>
        /// <param name="tecCtlr">Instrument reference</param>
        /// <param name="tecCtlId">Config table data prefix</param>
        private void ConfigureTecController(ITestEngineBase engine, IInstType_TecController tecCtlr, string tecCtlId, TestParamConfigAccessor cfgReader, bool isTecCase, double temperatureSetPoint_C)
        {
            SteinhartHartCoefficients stCoeffs = new SteinhartHartCoefficients();

            if (tecCtlr.DriverName.Contains("Ke2510"))
            {
                // Keithley 2510 specific commands (must be done before 'SetDefaultState')
                tecCtlr.HardwareData["MinTemperatureLimit_C"] = cfgReader.GetStringParam(tecCtlId + "_MinTemp_C");
                tecCtlr.HardwareData["MaxTemperatureLimit_C"] = cfgReader.GetStringParam(tecCtlId + "_MaxTemp_C");

                tecCtlr.SetDefaultState();

                tecCtlr.Sensor_Type = (InstType_TecController.SensorType)Enum.Parse(typeof(InstType_TecController.SensorType), cfgReader.GetStringParam(tecCtlId + "_Sensor_Type"));

                tecCtlr.TecVoltageCompliance_volt = cfgReader.GetDoubleParam(tecCtlId + "_TecVoltageCompliance_volt");

                if (isTecCase)
                {
                    tecCtlr.DerivativeGain = LocalCfgReader.GetDoubleParam(tecCtlId + "_DerivativeGain");
                }
                else
                {
                    tecCtlr.DerivativeGain = cfgReader.GetDoubleParam(tecCtlId + "_DerivativeGain");
                }
            }
            else
            {
                tecCtlr.SetDefaultState();
            }

            tecCtlr.OperatingMode = (InstType_TecController.ControlMode)Enum.Parse(typeof(InstType_TecController.ControlMode), cfgReader.GetStringParam(tecCtlId + "_OperatingMode"));

            // Thermistor characteristics
            if (tecCtlr.Sensor_Type == InstType_TecController.SensorType.Thermistor_2wire ||
                tecCtlr.Sensor_Type == InstType_TecController.SensorType.Thermistor_4wire)
            {
                stCoeffs.A = cfgReader.GetDoubleParam(tecCtlId + "_stCoeffs_A");
                stCoeffs.B = cfgReader.GetDoubleParam(tecCtlId + "_stCoeffs_B");
                stCoeffs.C = cfgReader.GetDoubleParam(tecCtlId + "_stCoeffs_C");
                tecCtlr.SteinhartHartConstants = stCoeffs;
            }

            if (isTecCase)
            {
                tecCtlr.TecCurrentCompliance_amp = LocalCfgReader.GetDoubleParam(tecCtlId + "_TecCurrentCompliance_amp");
                tecCtlr.ProportionalGain = LocalCfgReader.GetDoubleParam(tecCtlId + "_ProportionalGain");
                tecCtlr.IntegralGain = LocalCfgReader.GetDoubleParam(tecCtlId + "_IntegralGain");
            }
            else
            {
                tecCtlr.TecCurrentCompliance_amp = cfgReader.GetDoubleParam(tecCtlId + "_TecCurrentCompliance_amp");
                tecCtlr.ProportionalGain = cfgReader.GetDoubleParam(tecCtlId + "_ProportionalGain");
                tecCtlr.IntegralGain = cfgReader.GetDoubleParam(tecCtlId + "_IntegralGain");
            }

            try
            {
                tecCtlr.SensorTemperatureSetPoint_C = temperatureSetPoint_C;
            }
            catch (Exception e)
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, e.Message, FailModeCategory_Enum.UN);
            }

        }

        private void InitialiseTec(Inst_Ke2510 Tec, string TecName, TestParamConfigAccessor cfgReader)
        {
            //NameValueCollection TecSettings = (NameValueCollection)cfgReader.
            //GetSection("InstrumentSettings/TemperatureSettings/" + TecName);
            Tec.SetDefaultState();
            Tec.OutputEnabled = false;
            //取消ground connect 
            // Tec.GroundConnect = bool.Parse(TecSettings["GroundConnect"]);
            //使用温度控制模式
            Tec.OperatingMode = (InstType_TecController.ControlMode)Enum.
                Parse(typeof(InstType_TecController.ControlMode), cfgReader.GetStringParam("OperatingMode"));
            //启用4线，RTD温度感应(用户模式，需要指定α,β,δ,R0)，A=1.2073e-3,B=2.20813e-4,C=1.4494e-7
            InstType_TecController.SensorType st = (InstType_TecController.SensorType)Enum.
                Parse(typeof(InstType_TecController.SensorType), cfgReader.GetStringParam("SensorType"));
            Tec.Sensor_Type = st;
            if (st == InstType_TecController.SensorType.Thermistor_2wire
                || st == InstType_TecController.SensorType.Thermistor_4wire)
            {
                SteinhartHartCoefficients stCoeffs = new SteinhartHartCoefficients();
                stCoeffs.A = cfgReader.GetDoubleParam("stCoeffs_A");
                stCoeffs.B = cfgReader.GetDoubleParam("stCoeffs_B");
                stCoeffs.C = cfgReader.GetDoubleParam("stCoeffs_C");
                Tec.SteinhartHartConstants = stCoeffs;
            }
            else if (st == InstType_TecController.SensorType.RTD_2wire
                || st == InstType_TecController.SensorType.RTD_4wire)
            {
                CallendarVanDusenCoefficients cvCoeffs = new CallendarVanDusenCoefficients();

            }
            else
            {

            }
            //ProportionalGain=80, IntegrallGain=5.0, DerivativeGain=0 
            Tec.ProportionalGain = cfgReader.GetDoubleParam("ProportionalGain");   //80;
            Tec.IntegralGain = cfgReader.GetDoubleParam("IntegralGain");          //5.0;
            Tec.DerivativeGain = cfgReader.GetDoubleParam("DerivativeGain");      //0;
            //保护电流=1.6A,保护电压=5V 
            Tec.TecCurrentCompliance_amp = cfgReader.GetDoubleParam("TecCurrentCompliance_amp");   //1.6;
            Tec.TecVoltageCompliance_volt = cfgReader.GetDoubleParam("TecVoltageCompliance_volt"); //5.0; //5.0;
            //最低温度=0, 最高温度=80 
            //Tec.Set_Protection_Level_Temperature(Inst_Ke2510.LimitType.LOWER, double.Parse(TecSettings["MinTemperatureLimit_C"]));//0
            //Tec.Set_Protection_Level_Temperature(Inst_Ke2510.LimitType.UPPER, double.Parse(TecSettings["MaxTemperatureLimit_C"]));//60
            //Tec.Set_Protection(Inst_Ke2510.ProtectionType.ptTEMPERATURE, Inst_Ke2510.OnOrOff.ON);

            //normal operate
            Tec.SensorTemperatureSetPoint_C = cfgReader.GetDoubleParam("SensorTemperatureSetPoint_C");//25;
            Tec.OutputEnabled = cfgReader.GetBoolParam("OutputEnabled");
        }

        /// <summary>
        /// initialise FCU2ASIC INSTRUMENTS 
        /// </summary>
        /// <param name="fcu2AsicInstrs">FCU2ASIC instruments group </param>
        private void ConfigureFCU2AsicInstruments()
        {
            string FCUMKI_Tx_CalFilename = MapParamsConfig.GetStringParam("FCUMKI_Tx_Calibration_file_Path");
            string FCUMKI_Rx_CalFilename = MapParamsConfig.GetStringParam("FCUMKI_Rx_Calibration_file_Path");
            string FCUMKI_Var_CalFilename = MapParamsConfig.GetStringParam("FCUMKI_Var_Calibration_file_Path");
            string FCUMKI_Fix_CalFilename = MapParamsConfig.GetStringParam("FCUMKI_Fix_Calibration_file_Path");

            FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Tx = FreqCalByEtalon.LoadFCUMKICalArray(FCUMKI_Tx_CalFilename);
            FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Rx = FreqCalByEtalon.LoadFCUMKICalArray(FCUMKI_Rx_CalFilename);
            FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Var = FreqCalByEtalon.LoadFCUMKICalArray(FCUMKI_Var_CalFilename);
            FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Fix = FreqCalByEtalon.LoadFCUMKICalArray(FCUMKI_Fix_CalFilename);
            //initial FCUMKI pot value in FCUMKI cal file list. Jack.zhang 2012-04-12
            DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic.Locker_tran_pot = FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Tx[FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Tx.Count - 1].Pot;
            DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic.Locker_refi_pot = FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Rx[FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Rx.Count - 1].Pot;
            DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic.Locker_fine_pot = 255;
        } 

        #endregion
    }
}


// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestPrograms
//
// Prog_TCMZ_Final.cs
//
// Author: paul.annetts, Mark Fullalove 2006
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.ExternalData;
using Bookham.TestEngine.Framework.Limits;
using System.Collections.Specialized;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestLibrary.Utilities;
using Bookham.TestSolution.IlmzCommonInstrs;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestSolution.IlmzCommonData;
using Bookham.TestSolution.ILMZFinalData;
using Bookham.TestSolution.IlmzCommonUtils;
using Bookham.TestSolution.TestModules;
using Bookham.TestLibrary.Instruments;
using Bookham.ToolKit.Mz;
using Bookham.TestEngine.Config;
using System.Collections;
using Bookham.TestLibrary.BlackBoxes;
using Bookham.Program.Core;
using System.Threading;
using System.IO.Ports;
using Bookham.TestSolution.Instruments;

namespace Bookham.TestSolution.TestPrograms
{
    /// <summary>
    /// ILMZ Negtive Chirp chip test program
    /// </summary>
    public class Prog_ILMZ_Final_SPC : TestProgramCore
    {
        #region Private data
        /// <summary>
        /// Remember the specification name we are using in the program
        /// </summary>
        private Prog_ILMZ_SPCInfo progInfo;
        private DatumList tcTraceData = new DatumList();
        private double labourTime = 0;
        private BaseLineData[] baseLineChannels;
        private double TestCondition_MidTemp;
        private double TestCondition_HighTemp;
        private double TestCondition_LowTemp;
        private double[] powerOffsetArray;
        //verify Mz/Laser Temperature
        private bool VerifyJigTemperautureIsOk = false;

        private double TEC_LASER_OL_TEMP = 0.0;
        private double TEC_MZ_OL_TEMP = 0.0;
        private double TEC_JIG_CL_TEMP = 0.0;
        private double TC_LASER_TEMP_C = 0;
        private string Nofilepath = @"Configuration\TOSA Final\MapTest\NoDataFile.txt";

        ProgIlmzFinalInstruments progInstrs;


        //private string errorInformation;

        /*private DsdbrChannelData[] closeGridCharData;

        private const string externalKeyTuneOpticalPowerToleranceInmW
            = "TC_FIBRE_TARGET_POWER_TOL_MID";
        private const string externalKeyTuneOpticalPowerToleranceIndB
            = "Tune_OpticalPowerTolerance_dB";
        private const string externalKeyTuneISOAPowerSlope
            = "Tune_IsoaPowerSlope";

        private int numberOfSupermodes;

        //bool SelectTestFlag;
        private List<SectionIVSweepData> SectionIVCurveData;*/

        #endregion

        #region for Light Tower.

        private SerialPort _serialPort;
        /// <summary>
        /// Serial port to control the lighter to indicate test messeage
        /// </summary>
        public SerialPort SerialPort
        {
            get { return _serialPort; }
        }

        /// <summary>
        /// Construct a serial port object from parts of the resource string.
        /// </summary>
        /// <param name="comPort">COMx port portion of resource string.</param>
        /// <param name="rate">bps rate portion of resource string.</param>
        /// <returns>Constructed SerialPort handler instance.</returns>
        private SerialPort buildSerPort(string comPort, string rate)
        {
            return new System.IO.Ports.SerialPort(
                "COM" + comPort,                                /* COMn */
                int.Parse(rate),                                /* bps */
                System.IO.Ports.Parity.None, 8, StopBits.One);  /* 8N1 */
        }
        #endregion
        
        /// <summary>
        /// Constructor
        /// </summary>
        public Prog_ILMZ_Final_SPC()
        {
            this.progInfo = new Prog_ILMZ_SPCInfo();
        }
       
        /// <summary>
        ///  GUI for program 
        /// </summary>
        public override Type UserControl
        {
            get { return typeof(Prog_ILMZ_Final_SPCGui); }
        }

        #region Implement InitCode method

        /// <summary>
        /// Initialise test configuration
        /// </summary>
        /// <param name="dutObj">Device under test</param>
        /// <param name="engine">Test engine</param>
        protected override void InitConfig(ITestEngineInit engine, DUTObject dutObject)
        {
            base.InitConfig(engine, dutObject);

            progInfo.TestParamsConfig = new TestParamConfigAccessor(dutObject,
                @"Configuration\TOSA Final\SPC\HittTestParams_SPC.xml", "", "TcmzTestParams");

            progInfo.FinalTestParamsConfig = new TestParamConfigAccessor(dutObject,
                 @"Configuration\TOSA Final\FinalTest\CommonTestParams.xml", "", "TestParams");

            //progInfo.InstrumentsCalData = new ConfigDataAccessor(@"Configuration\TOSA Final\InstrsCalibration_new.xml", "Calibration");

            progInfo.TempConfig = new TempTableConfigAccessor(dutObject, 1,
                @"Configuration\TOSA Final\TempTable.xml");

            progInfo.LocalTestParamsConfig = new TestParamConfigAccessor(dutObject,
                 @"Configuration\TOSA Final\LocalTestParams.xml", "", "TestParams");

            progInfo.OpticalSwitchConfig = new TestParamConfigAccessor(dutObject,
                 @"Configuration\TOSA Final\IlmzOpticalSwitch.xml", "", "IlmzOpticalSwitchParams");

            progInfo.MapParamsConfig = new TestParamConfigAccessor(dutObject, @"Configuration\TOSA Final\MapTest\MappingTestConfig.xml", "", "MappingTestParams");

            //progInfo.TestSelect = new TestSelection(@"Configuration\TOSA Final\FinalTest\testSelect_" + dutObj.PartCode + ".xml");
            string dutSN = dutObject.SerialNumber.ToString().Replace('.', '_');
            progInfo.baseLineFile = @"Configuration\TOSA Final\SPC\BaseLineTemplate_" + dutSN + ".csv";
            progInfo.baseLineReferenceFile = @"Configuration\TOSA Final\SPC\BaseLineReference_" + dutSN + ".csv";

            if (dutObject.TestStage.Contains("base"))
            {
                if (!File.Exists(progInfo.baseLineFile))
                {
                    this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "Hitt SPC baseline file didn't found. It should be:  \n" + progInfo.baseLineFile.ToString(), FailModeCategory_Enum.SW);
                }
            }
            else
            {
                if (!File.Exists(progInfo.baseLineReferenceFile))
                {
                    this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "Hitt SPC reference  file didn't found. It should be:  \n" + progInfo.baseLineReferenceFile.ToString(), FailModeCategory_Enum.SW);
                }
            }

            GetBaseLineChannels(engine);
        }

        /// <summary>
        /// Load specifications
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        protected override void LoadSpecs(ITestEngineInit engine, DUTObject dutObject)
        {
            engine.SendStatusMsg("Loading Specification");
            ILimitRead limitReader = null;
            StringDictionary mainSpecKeys = new StringDictionary();

            if (progInfo.TestParamsConfig.GetBoolParam("UseLocalLimitFile"))
            {
                // initialise limit file reader
                limitReader = engine.GetLimitReader("PCAS_FILE");
                // main specification
                string mainSpecNameFile = progInfo.TestParamsConfig.GetStringParam("PcasSPCLimitFileName");
                string mainSpecFullFilename = progInfo.TestParamsConfig.GetStringParam("PcasLimitFileDirectory")
                    + @"\" + mainSpecNameFile;
                mainSpecKeys.Add("Filename", mainSpecFullFilename);
            }
            else
            {
                // Use default limit source
                limitReader = engine.GetLimitReader();

                mainSpecKeys.Add("SCHEMA", "HIBERDB");
                mainSpecKeys.Add("TEST_STAGE", dutObject.TestStage);
                mainSpecKeys.Add("DEVICE_TYPE", dutObject.PartCode);
            }

            // load main specification

            SpecList tempSpecList = limitReader.GetLimit(mainSpecKeys);
            engine.SetSpecificationList(tempSpecList);

            // Get our specification object (so we can initialise modules with appropriate limits)
            progInfo.MainSpec = tempSpecList[0];
            this.MainSpec = progInfo.MainSpec;

            progInfo.TestConditions = new IlmzFinalTestConds(progInfo.MainSpec);
            GetTestCondition(engine, dutObject);

            base.LoadSpecs(engine, dutObject);
        }

        /// <summary>
        /// Validate DSDBR drive instruments - PXI or FCU
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        protected override void ValidateDsdbrDriveInstrs(ITestEngineInit engine, DUTObject dutObject)
        {
            if (dutObject.TestJigID == "FCUTestJigID")
            {
                progInfo.DsdbrInstrsUsed = DsdbrDriveInstruments.FCUInstruments;
            }
            else if (dutObject.TestJigID == "PXITestJigID")
            {
                progInfo.DsdbrInstrsUsed = DsdbrDriveInstruments.PXIInstruments;
            }
            else if (dutObject.TestJigID == "FCU2AsicTestJigId")
            {
                progInfo.DsdbrInstrsUsed = DsdbrDriveInstruments.FCU2AsicInstrument;
            }
            else
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "Unknown DSDBR drive instruments used!", FailModeCategory_Enum.HW);
            }
        }

        /// <summary>
        /// Check Dut's test stage, if it is not in "final" stage, prompt Error in program to stop test
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        protected override void ValidateTestStage(ITestEngineInit engine, DUTObject dutObject)
        {
            // Get test stage information - Must be "spc"
            progInfo.TestStage = FinalTestStage.SPC;
        }

        /// <summary>
        /// verify device internal high temperature...
        /// </summary>
        /// <param name="engine"></param>
        protected override void VerifyJigTemperauture(ITestEngineInit engine)
        {
            //if (VerifyJigTemperautureIsOk) return;
            engine.SendStatusMsg("verify device internal high temperature...");


            //GetTestCondition(engine, dutobject);

            //get inst
            Inst_Ke2510 dsdbrTec = progInfo.Instrs.TecDsdbr as Inst_Ke2510;
            ////Inst_Ke2510 mzTec = progInfo.Instrs.TecMz as Inst_Ke2510;
            //jig temp controlled by NT8A edit by Rosa
            //Inst_Ke2510 jigTec = progInfo.Instrs.TecCase as Inst_Ke2510;
            IInstType_TecController jigTec = progInfo.Instrs.TecCase as IInstType_TecController;
            //double JigHighTempValue = progInfo.TestConditions.HighTemp;

            double JigHighTempValue = this.TestCondition_HighTemp;
            TempTablePoint tempCal = progInfo.TempConfig.GetTemperatureCal(25, JigHighTempValue)[0];
            double CalJigHighTemValue = JigHighTempValue + tempCal.OffsetC;

            jigTec.SensorTemperatureSetPoint_C = CalJigHighTemValue;
            dsdbrTec.OutputEnabled = false;
            //mzTec.OutputEnabled = false;
            //mzTec.Set_Protection_Level_Temperature(Inst_Ke2510.LimitType.UPPER, JigHighTempValue + 5);
            dsdbrTec.Set_Protection_Level_Temperature(Inst_Ke2510.LimitType.UPPER, JigHighTempValue + 5);
            //jigTec.Set_Protection_Level_Temperature(Inst_Ke2510.LimitType.UPPER, JigHighTempValue + 5);//Rosa:NT10A do not support this

            jigTec.OutputEnabled = true;

            /*
            int count = 0;
            double Tolerance = 3;
            do
            {
                Thread.Sleep(2000);
                double dsdbrTemp = dsdbrTec.SensorTemperatureActual_C;
                //double mztemp = mzTec.SensorTemperatureActual_C;
                //&& JigHighTempValue - Tolerance < mztemp && mztemp < JigHighTempValue + Tolerance
                if (JigHighTempValue - Tolerance < dsdbrTemp && dsdbrTemp < JigHighTempValue + Tolerance+5 )
                    break;
                count++;
                if (count > 60)
                {
                    jigTec.SensorTemperatureSetPoint_C = progInfo.TestParamsConfig.GetDoubleParam("TecCase_SensorTemperatureSetPoint_C");
                    Thread.Sleep(2000);
                    jigTec.OutputEnabled = false;
                    this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "The Dsdbr and Mz temperature can't reach the CaseTemp:" + JigHighTempValue.ToString(), FailModeCategory_Enum.UN);
                }
            } while (true);
            */

            //should let jig temperature stablize for 30 seconds,then read and record device temperatuer

            double gap = 3;
            double Tolerance = 0.5;
            do
            {
                Thread.Sleep(2000);

                double mztemp = jigTec.SensorTemperatureActual_C;

                if (CalJigHighTemValue - Tolerance < mztemp && mztemp < CalJigHighTemValue + Tolerance)
                    break;

            } while (true);


            //Thread.Sleep(120000);
            //double dsdbrTemp = dsdbrTec.SensorTemperatureActual_C;
            //if (!(JigHighTempValue - gap < dsdbrTemp)) {
            //    this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "The Dsdbr temperature can't reach the CaseTemp:" + JigHighTempValue.ToString(), FailModeCategory_Enum.UN);
            //}
            int cycleCount = 0;
            do
            {
                Thread.Sleep(2000);

                double dsdbrTemp = dsdbrTec.SensorTemperatureActual_C;

                if (Math.Abs(JigHighTempValue - dsdbrTemp) > gap)
                {
                    if (cycleCount > 90)
                    {
                        this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "The Dsdbr temperature can't reach the CaseTemp:" + JigHighTempValue.ToString(), FailModeCategory_Enum.UN);
                        break;
                    }
                }
                else 
                {
                    break;
                }
                cycleCount++;

            } while (true);

            //restore settings
            TEC_LASER_OL_TEMP = dsdbrTec.SensorTemperatureActual_C;
            //TEC_MZ_OL_TEMP = mzTec.SensorTemperatureActual_C;
            TEC_JIG_CL_TEMP = jigTec.SensorTemperatureActual_C;
            jigTec.SensorTemperatureSetPoint_C = this.TestCondition_MidTemp;
            Thread.Sleep(1000);
            jigTec.OutputEnabled = false;
            VerifyJigTemperautureIsOk = true;
            engine.SendStatusMsg("verify is ok!");

        }

        /// <summary>
        /// Initialise instruments
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="instrs"></param>   
        /// <param name="chassis"></param>
        protected override void InitInstrs(ITestEngineInit engine, DUTObject dutObject, InstrumentCollection instrs, ChassisCollection chassis)
        {
            //add for avoidig instrument logging error
            engine.SendStatusMsg("Initialise Instruments");

            #region light_tower

            /* Set up Serial Binary Comms */
            // _serialPort = buildSerPort(comPort, rate);
            string lightTower = @"Configuration\TOSA Final\LightTowerParams.xml";
            progInfo.lightTowerConfig = new TestParamConfigAccessor(dutObject,
                lightTower, "_", "LightTowerParams");
            if (progInfo.lightTowerConfig.GetBoolParam("LightExist"))
            {
                try
                {

                    _serialPort = this.buildSerPort(progInfo.lightTowerConfig.GetStringParam("ComNum"), progInfo.lightTowerConfig.GetStringParam("BaudRate"));
                    _serialPort.Close();
                    if (!_serialPort.IsOpen)
                    {
                        _serialPort.Open();
                    }

                    // Add the light tower color.
                    _serialPort.WriteLine(progInfo.lightTowerConfig.GetStringParam("LightGreen"));
                    _serialPort.Close();
                }
                catch { }

            }
            #endregion

            // alice: add osa share option & configuration
            #region OSA share Config

            TcmzOSAshare.GetOSAShareConfig(instrs,
                @"Configuration\InstrumentShareConfiguration.xml", "OsaShareConfiguration/node" +
                                                    dutObject.NodeID.ToString());
            if (TcmzOSAshare.OsaShareFlag && (TcmzOSAshare.InstrumentInUsedFileName.Trim() == ""))
                TcmzOSAshare.InstrumentInUsedFileName = dutObject.NodeID.ToString() + ".txt";
            #endregion
            // get info for SMSR test
            //SMSRMeasurement.strSN = dutObject.SerialNumber;
            //SMSRMeasurement.SMSRzipfilename = "results\\SMSR_Spectrum" + "_" + dutObject.SerialNumber + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".zip";
            //Util_ZipFile zipFile = new Util_ZipFile(SMSRMeasurement.SMSRzipfilename);            

            progInstrs = new ProgIlmzFinalInstruments();
            progInfo.Instrs = progInstrs;

            foreach (Chassis var in chassis.Values) var.EnableLogging = false;
            foreach (Instrument var in instrs.Values) var.EnableLogging = false;

            // Initialise DSDBR drive instruments
            switch (progInfo.DsdbrInstrsUsed)
            {
                case DsdbrDriveInstruments.FCUInstruments:
                    // FCU
                    progInstrs.Fcu = new FCUInstruments();
                    progInstrs.Fcu.FullbandControlUnit = (Instr_FCU)instrs["FullbandControlUnit"];
                    progInstrs.Fcu.SoaCurrentSource =
                                        (InstType_ElectricalSource)instrs["SoaCurrentSource"];
                    // modify for replacing FCU 2400 steven.cui
                    //progInstrs.Fcu.FCU_Source = (InstType_ElectricalSource)instrs["FCU_Source"];
                    break;

                // Alice.Huang add FCU2Asic function    2010-02-01
                case DsdbrDriveInstruments.FCU2AsicInstrument:
                    engine.SendStatusMsg("Configuring FCU2Asic instruments");
                    progInstrs.Fcu2AsicInstrsGroups = new FCU2AsicInstruments();
                    if (instrs.Contains("FCUMKI_Source"))
                        progInstrs.Fcu2AsicInstrsGroups.FcuSource =
                                    (InstType_ElectricalSource)instrs["FCUMKI_Source"];
                    else
                        progInstrs.Fcu2AsicInstrsGroups.FcuSource = null;

                    progInstrs.Fcu2AsicInstrsGroups.AsicVccSource =
                                        (InstType_ElectricalSource)instrs["ASIC_VccSource"];
                    progInstrs.Fcu2AsicInstrsGroups.AsicVeeSource =
                                        (InstType_ElectricalSource)instrs["ASIC_VeeSource"];
                    progInstrs.Fcu2AsicInstrsGroups.Fcu2Asic = (Inst_Fcu2Asic)instrs["Fcu2Asic"];

                    //progInstrs.Fcu2AsicInstrsGroups.FcuSource = (Inst_Ke24xx)instrs["FcuSource"];
                    break;

            }

            progInstrs.Mz = new IlMzInstruments();
            progInstrs.Mz.FCU2Asic = (Inst_Fcu2Asic)instrs["Fcu2Asic"];
            progInstrs.Mz.FCU2Asic.SwapWirebond = ParamManager.Conditions.IsSwapWirebond;
            progInstrs.Mz.PowerMeter = (IInstType_TriggeredOpticalPowerMeter)instrs["OpmMz"];
            // OpmRef
            progInstrs.OpmReference = (IInstType_OpticalPowerMeter)instrs["OpmRef"];

            progInstrs.Mz.PowerMeter.EnableInputTrigger(false);
            progInstrs.Mz.PowerMeter.SetDefaultState();
            progInstrs.OpmReference.SetDefaultState();

            TestProgramCore.InitInstrs_Wavemeter(this.failModeCheck, engine, dutObject, instrs, progInfo.LocalTestParamsConfig, progInfo.OpticalSwitchConfig);
            
            progInstrs.TecDsdbr = (IInstType_TecController)instrs["TecDsdbr"];
            DsdbrUtils.opticalSwitch = (Inst_Ke2510)instrs["TecDsdbr"];
            DsdbrTuning.OpticalSwitch = (Inst_Ke2510)instrs["TecDsdbr"];

            // Initialize OSA - chongjian.liang 2013.7.23
            if (instrs.Contains("Osa"))
            {
                progInstrs.Osa = (IInstType_OSA)instrs["Osa"];
            }

            ////progInstrs.TecMz = (IInstType_TecController)instrs["TecMz"];
            progInstrs.TecCase = (IInstType_TecController)instrs["TecCase"];

            // Config instruments if not simulation mode
            if (!engine.IsSimulation)
            {
                engine.SendStatusMsg("Configuring Instruments");
                // Configure FCU
                ConfigureFCU2AsicInstruments(progInstrs.Fcu2AsicInstrsGroups);

                // Configure TEC controllers

                ConfigureTecController(engine, progInstrs.TecDsdbr, "TecDsdbr", false, this.TC_LASER_TEMP_C);
                //ConfigureTecController(progInstrs.TecMz, "TecMz");
                ConfigureTecController(engine, progInstrs.TecCase, "TecCase", true, this.TestCondition_MidTemp);
                progInstrs.OpmReference.SetDefaultState();
                progInstrs.Mz.PowerMeter.SetDefaultState();
            }
        }

        /// <summary>
        /// Initialise utilities
        /// </summary>
        /// <param name="engine"></param>
        protected override void InitUtils(ITestEngineInit engine)
        {
            engine.SendStatusMsg("Initialise utilities");

            // Initialise DsdbrUtils and Measurements utilities by testkit hardware info.
            switch (progInfo.DsdbrInstrsUsed)
            {
                // added FCU2ASIC Option by Alice.Huang, for TOSA GB test     2010-02-01
                case DsdbrDriveInstruments.FCU2AsicInstrument:
                    DsdbrUtils.Fcu2AsicInstrumentGroup = progInfo.Instrs.Fcu2AsicInstrsGroups;
                    Measurements.FCU2AsicInstrs = progInfo.Instrs.Fcu2AsicInstrsGroups;
                    SoaShutterMeasurement.Fcu2AsicInstrs = progInfo.Instrs.Fcu2AsicInstrsGroups;
                    break;
                case DsdbrDriveInstruments.FCUInstruments:
                    DsdbrUtils.FcuInstrumentGroup = progInfo.Instrs.Fcu;
                    Measurements.FCUInstrs = progInfo.Instrs.Fcu;
                    SoaShutterMeasurement.FCUInstrs = progInfo.Instrs.Fcu;
                    break;
            }
            //Measurements.OSA = progInfo.Instrs.Osa;
            //Measurements.Wavemeter = progInfo.Instrs.Wavemeter;
            Measurements.MzHead = progInfo.Instrs.Mz.PowerMeter;
            Measurements.ExternalHead = progInfo.Instrs.OpmReference;

            // Initialise TraceToneTest utility by device types
            if (progInfo.TestParamsConfig.GetBoolParam("TraceToneTestEnabled"))
            {
                TraceToneTest.Initialise(
                    progInfo.TestParamsConfig.GetIntParam("TraceToneTest_OrderOfFit"),
                    progInfo.TestConditions.TargetFibrePower_dBm,
                    progInfo.TestConditions.TraceToneTestAgeCond1_dB,
                    progInfo.TestConditions.TraceToneTestAgeCond2_dB,
                    progInfo.TestParamsConfig.GetDoubleParam("TraceToneTest_ModulationIndex"),
                    progInfo.TestParamsConfig.GetDoubleParam("TraceToneTest_SoaStart_mA"),
                    progInfo.TestParamsConfig.GetDoubleParam("TraceToneTest_SoaEnd_mA"),
                    progInfo.TestParamsConfig.GetDoubleParam("TraceToneTest_SoaStep_mA"),
                    progInfo.TestParamsConfig.GetIntParam("TraceToneTest_StepDelay_mS"));
            }
        }

        protected override void InitOpticalSwitchIoLine()
        {
            IlmzOpticalSwitchLines.C_Band_DigiIoLine = int.Parse(progInfo.OpticalSwitchConfig.GetStringParam("C_Band_Filter_IO_Line"));
            IlmzOpticalSwitchLines.C_Band_DigiIoLine_State = progInfo.OpticalSwitchConfig.GetBoolParam("C_Band_Filter_IO_Line_State");
            IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine = int.Parse(progInfo.OpticalSwitchConfig.GetStringParam("Dut_Ctap_IO_Line"));
            IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine_State = progInfo.OpticalSwitchConfig.GetBoolParam("Dut_Ctap_IO_Line_State");
            IlmzOpticalSwitchLines.Dut_Rx_DigiIoLine = int.Parse(progInfo.OpticalSwitchConfig.GetStringParam("Dut_Rx_IO_Line"));
            IlmzOpticalSwitchLines.Dut_Rx_DigiIoLine_State = progInfo.OpticalSwitchConfig.GetBoolParam("Dut_Rx_IO_Line_State");
            IlmzOpticalSwitchLines.Dut_Tx_DigiIoLine = int.Parse(progInfo.OpticalSwitchConfig.GetStringParam("Dut_Tx_IO_LIne"));
            IlmzOpticalSwitchLines.Dut_Tx_DigiIoLine_State = progInfo.OpticalSwitchConfig.GetBoolParam("Dut_Tx_IO_LIne_State");
            IlmzOpticalSwitchLines.Etalon_Ref_DigiIoLine = int.Parse(progInfo.OpticalSwitchConfig.GetStringParam("Etalon_Ref_IO_Line"));
            IlmzOpticalSwitchLines.Etalon_Ref_DigiIoLine_State = progInfo.OpticalSwitchConfig.GetBoolParam("Etalon_Ref_IO_Line_State");
            IlmzOpticalSwitchLines.Etalon_Rx_DigiIoLine = int.Parse(progInfo.OpticalSwitchConfig.GetStringParam("Etalon_Rx_IO_Line"));
            IlmzOpticalSwitchLines.Etalon_Rx_DigiIoLine_State = progInfo.OpticalSwitchConfig.GetBoolParam("Etalon_Rx_IO_Line_State");
            IlmzOpticalSwitchLines.Etalon_Tx_DigiIoLine = int.Parse(progInfo.OpticalSwitchConfig.GetStringParam("Etalon_Tx_IO_Line"));
            IlmzOpticalSwitchLines.Etalon_Tx_DigiIoLine_State = progInfo.OpticalSwitchConfig.GetBoolParam("Etalon_Tx_IO_Line_State");
            IlmzOpticalSwitchLines.L_Band_DigiIoLine = int.Parse(progInfo.OpticalSwitchConfig.GetStringParam("L_Band_Filter_IO_Line"));
            IlmzOpticalSwitchLines.L_Band_DigiIoLine_State = progInfo.OpticalSwitchConfig.GetBoolParam("L_Band_Filter_IO_Line_State");
        }

        /// <summary>
        /// Initialise test modules
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        protected override void InitModules(ITestEngineInit engine, DUTObject dutObject, InstrumentCollection instrs, ChassisCollection chassis)
        {
            engine.SendStatusMsg("Initialise modules");

            this.InitMod_Pin_Check(engine, instrs, chassis, dutObject);
            //this.IlmzInitialise_InitModule(engine, dutObject);
            this.DeviceTempControl_InitModule(engine, "DsdbrTemperature", progInfo.Instrs.TecDsdbr, "TecDsdbr");
            this.CaseTempControl_InitModule(engine, "CaseTemp_Mid", 25, TestCondition_MidTemp);
            this.IlmzChannelChar_InitModule(engine, dutObject);
            if (progInfo.TestParamsConfig.GetBoolParam("LowTempTestEnabled"))
            {
                this.CaseTempControl_InitModule(engine, "CaseTemp_Low", TestCondition_MidTemp, TestCondition_LowTemp);
                this.CaseTempControl_InitModule(engine, "CaseTemp_High", TestCondition_LowTemp, TestCondition_HighTemp);
                this.IlmzLowTempTest_InitModule(engine, dutObject);
            }
            else
            {
                this.CaseTempControl_InitModule(engine, "CaseTemp_High", TestCondition_MidTemp, TestCondition_HighTemp);
            }
            this.IlmzHighTempTest_InitModule(engine, dutObject);
            //this.IlmzChannelChar_InitModule(engine, dutObject);
            this.IlmzPowerHeadCal_InitModule(engine, dutObject);
            //this.CaseTempControl_InitModule(engine, "CaseTemp_Safe", progInfo.TestConditions.HighTemp, 25);   // TODO - tidier method?
        }

        #endregion

        #region Initialize modules

        /// <summary>
        /// Initial Pin check for hittGB test(check the pins correlation with TCE and Vcc)
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="instrs"></param>
        /// <param name="chassis"></param>
        /// <param name="dutObject"></param>
        private void InitMod_Pin_Check(ITestEngineInit engine, InstrumentCollection instrs, ChassisCollection chassis, DUTObject dutObject)
        {
            double Temperature_LowLimit_Degree = 15.0;
            double Temperature_HighLimit_Degree = 40.0;


            ModuleRun modRun = engine.AddModuleRun("Mod_PinCheck", "Mod_PinCheck", string.Empty);

            modRun.Instrs.Add(instrs);
            modRun.Chassis.Add(chassis);
            //modRun.ConfigData.AddString("TIME_STAMP", TestTimeStamp_Start);
            //modRun.ConfigData.AddString("LASER_ID", dutObject.SerialNumber);
            modRun.ConfigData.AddDouble("Temperature_LowLimit_Degree", Temperature_LowLimit_Degree);
            modRun.ConfigData.AddDouble("Temperature_HighLimit_Degree", Temperature_HighLimit_Degree);
        }
       
        /// <summary>
        /// Configure a Device Temperature Control module
        /// </summary>
        private void DeviceTempControl_InitModule(ITestEngineInit engine, string moduleName,
            IInstType_SimpleTempControl tecController, string configPrefix)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun(moduleName, "SimpleTempControl", "");

            // Add instrument
            modRun.Instrs.Add("Controller", (Instrument)tecController);

            //load config
            double timeout_S = this.progInfo.FinalTestParamsConfig.GetDoubleParam(configPrefix + "_Timeout_S");
            double stabTime_S = this.progInfo.FinalTestParamsConfig.GetDoubleParam(configPrefix + "_StabiliseTime_S");
            int updateTime_mS = this.progInfo.FinalTestParamsConfig.GetIntParam(string.Format("{0}_UpdateTime_mS", configPrefix));
            //read Dsdbr settemperature from SPC template file, not config file. Rosa comment on June 07, 2011
            //double sensorSetPoint_C = this.progInfo.TestParamsConfig.GetDoubleParam(string.Format("{0}_SensorTemperatureSetPoint_C", configPrefix));
            double sensorSetPoint_C = baseLineChannels[0].Dsdbr_Temp_C;
            double tolerance_C = this.progInfo.FinalTestParamsConfig.GetDoubleParam(string.Format("{0}_Tolerance_C", configPrefix));

            // Add config data
            DatumList tempConfig = new DatumList();
            tempConfig.AddDouble("datumMaxExpectedTimeForOperation_s", timeout_S);
            tempConfig.AddDouble("RqdStabilisationTime_s", stabTime_S);
            tempConfig.AddSint32("TempTimeBtwReadings_ms", updateTime_mS);
            tempConfig.AddDouble("SetPointTemperature_C", sensorSetPoint_C);
            tempConfig.AddDouble("TemperatureTolerance_C", tolerance_C);

            modRun.ConfigData.AddListItems(tempConfig);
        }

        /// <summary>
        /// Configure Case Temperature module
        /// </summary>
        private void CaseTempControl_InitModule(ITestEngineInit engine,
            string moduleName, Double currentTemp, Double tempSetPoint)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun(moduleName, "SimpleTempControl", "");

            // Add instrument
            modRun.Instrs.Add("Controller", (Instrument)progInfo.Instrs.TecCase);

            // Add config data
            TempTablePoint tempCal = progInfo.TempConfig.GetTemperatureCal(currentTemp, tempSetPoint)[0];
            DatumList tempConfig = new DatumList();
            int guiTimeOut_ms = this.progInfo.FinalTestParamsConfig.GetIntParam("CaseTempGui_UpdateTime_mS");
            // Timeout = 2 * calibrated time, minimum of 60secs
            double timeout_s = Math.Max(tempCal.DelayTime_s * 15, 60);
            tempConfig.AddDouble("datumMaxExpectedTimeForOperation_s", timeout_s);
            // Stabilisation time = 0.5 * calibrated time
            tempConfig.AddDouble("RqdStabilisationTime_s", tempCal.DelayTime_s);
            tempConfig.AddSint32("TempTimeBtwReadings_ms", guiTimeOut_ms);
            // Actual temperature to set
            tempConfig.AddDouble("SetPointTemperature_C", tempSetPoint + tempCal.OffsetC);
            tempConfig.AddDouble("TemperatureTolerance_C", tempCal.Tolerance_degC);
            modRun.ConfigData.AddListItems(tempConfig);
        }

        private void IlmzPowerHeadCal_InitModule(ITestEngineInit engine, DUTObject dutobject)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("IlmzPowerHeadCal", "HittSPCPowerHeadCal", "");

            // Add instruments
            modRun.Instrs.Add("OpmReference", (Instrument)progInfo.Instrs.OpmReference);
            //modRun.Instrs.Add("Wavemeter", (Instrument)progInfo.Instrs.Wavemeter);
            modRun.Instrs.Add("OpmMZ", (Instrument)progInfo.Instrs.Mz.PowerMeter);
            switch (progInfo.DsdbrInstrsUsed)
            {
                // added FCU2ASIC Option by Alice.Huang, for TOSA GB test     2010-02-01
                case DsdbrDriveInstruments.FCU2AsicInstrument:
                case DsdbrDriveInstruments.FCUInstruments:
                    break;
                case DsdbrDriveInstruments.PXIInstruments:
                    modRun.Instrs.Add("OpmCgDirect", (Instrument)progInfo.Instrs.OpmCgDirect);
                    modRun.Instrs.Add("OpmCgFilter", (Instrument)progInfo.Instrs.OpmCgFilter);
                    break;
            }

            // Add any config data here ...
            modRun.ConfigData.AddEnum("TestStage", progInfo.TestStage);
            modRun.ConfigData.AddSint32("OpticalCal_delay_ms", 2000);
            modRun.ConfigData.AddDouble("OpticalCal_MZ_Max", progInfo.LocalTestParamsConfig.GetDoubleParam("OpticalCal_MZ_Max"));
            modRun.ConfigData.AddDouble("OpticalCal_MZ_Min", progInfo.LocalTestParamsConfig.GetDoubleParam("OpticalCal_MZ_Min"));
            modRun.ConfigData.AddString("PlotFileDirectory", this.CommonTestConfig.ResultsArchiveDirectory);
            modRun.ConfigData.AddString("SN", dutobject.SerialNumber);
            switch (progInfo.DsdbrInstrsUsed)
            {
                // added FCU2ASIC Option by Alice.Huang, for TOSA GB test     2010-02-01
                case DsdbrDriveInstruments.FCU2AsicInstrument:

                case DsdbrDriveInstruments.FCUInstruments:
                    modRun.ConfigData.AddBool("IsCalPXIT", false);
                    break;
                case DsdbrDriveInstruments.PXIInstruments:
                    modRun.ConfigData.AddBool("IsCalPXIT", true);
                    modRun.ConfigData.AddDouble("OpticalCal_CgDirect_Max", progInfo.LocalTestParamsConfig.GetDoubleParam("OpticalCal_CgDirect_Max"));
                    modRun.ConfigData.AddDouble("OpticalCal_CgDirect_Min", progInfo.LocalTestParamsConfig.GetDoubleParam("OpticalCal_CgDirect_Min"));
                    break;
            }
        }
        
        /// <summary>
        /// Configure Channel Characterisation module
        /// </summary>
        private void IlmzChannelChar_InitModule(ITestEngineInit engine, DUTObject dutObject)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("IlmzChannelSPC", "IlmzChannelSPC", "");

            modRun.ConfigData.AddString("BandType", progInfo.TestConditions.FreqBand.ToString());

            // Add instruments
            modRun.ConfigData.AddReference("MzInstruments", progInfo.Instrs.Mz);
            IlMzDriverUtils.MaxModBias_V = double.Parse(progInfo.TestParamsConfig.GetDoubleParam("MZMaxBias_V").ToString());

            if (progInfo.Instrs.Osa != null)
            {
                modRun.Instrs.Add("OSA", (Instrument)progInfo.Instrs.Osa);
                modRun.ConfigData.AddDouble("SMSRMeasure_Span_nm", progInfo.TestParamsConfig.GetDoubleParam("SMSRMeasure_Span_nm"));
                modRun.ConfigData.AddDouble("SMSRMeasure_RBW_nm", progInfo.TestParamsConfig.GetDoubleParam("SMSRMeasure_RBW_nm"));
                modRun.ConfigData.AddDouble("SMSRMeasure_OSASensitivity_dBm", progInfo.TestParamsConfig.GetDoubleParam("SMSRMeasure_OSASensitivity_dBm"));
                modRun.ConfigData.AddSint32("SMSRMeasure_NumPts", progInfo.TestParamsConfig.GetIntParam("SMSRMeasure_NumPts"));
            }

            modRun.Limits.AddParameter(progInfo.MainSpec, "SMSR_CH_FST", "Smsr_ch_fst");
            modRun.Limits.AddParameter(progInfo.MainSpec, "SMSR_CH_MID", "Smsr_ch_mid");
            modRun.Limits.AddParameter(progInfo.MainSpec, "SMSR_CH_LST", "Smsr_ch_lst");

            if (!dutObject.TestStage.ToString().Contains("base"))
            {
                modRun.Limits.AddParameter(progInfo.MainSpec, "SMSR_CH_FST_DELTA", "Smsr_ch_fst_delta");
                modRun.Limits.AddParameter(progInfo.MainSpec, "SMSR_CH_MID_DELTA", "Smsr_ch_mid_delta");
                modRun.Limits.AddParameter(progInfo.MainSpec, "SMSR_CH_LST_DELTA", "Smsr_ch_lst_delta");
            }

            //}
            //modRun.Instrs.Add("MzTec", (Instrument)progInfo.Instrs.TecMz);
            modRun.Instrs.Add("DsdbrTec", (Instrument)progInfo.Instrs.TecDsdbr);
            modRun.Instrs.Add("Optical_switch", (Instrument)progInfo.Instrs.TecDsdbr);
            //modRun.Instrs.Add("FcuSource",(InstType_ElectricalSource)progInfo.Instrs.Fcu2AsicInstrsGroups.FcuSource);
            switch (progInfo.DsdbrInstrsUsed)
            {
                // added FCU2ASIC Option by Alice.Huang, for TOSA GB test     2010-02-01
                case DsdbrDriveInstruments.FCU2AsicInstrument:
                case DsdbrDriveInstruments.FCUInstruments:
                    modRun.Instrs.Add("PowerHead", (Instrument)progInfo.Instrs.Mz.PowerMeter);
                    break;
                case DsdbrDriveInstruments.PXIInstruments:
                    modRun.Instrs.Add("PowerHead", (Instrument)progInfo.Instrs.OpmCgDirect);
                    break;
            }

            //do we need below? Rosa comment on June 14,2011
            //modRun.ConfigData.AddReference("SwitchOsaMzOpm", progInfo.Instrs.SwitchOsaMzOpm);
            modRun.ConfigData.AddReference("Specification", progInfo.MainSpec);
            modRun.ConfigData.AddReference("FailModeCheck", this.failModeCheck);
            modRun.ConfigData.AddDouble("CaseTempMid_C", TestCondition_MidTemp);
            modRun.ConfigData.AddDouble("CaseTempHigh_C", TestCondition_HighTemp);


            modRun.ConfigData.AddString("DutSerialNbr", dutObject.SerialNumber);
            modRun.ConfigData.AddString("TestStage", dutObject.TestStage);

            modRun.ConfigData.AddString("BaseLineReferenceFile", progInfo.baseLineReferenceFile);

            //modRun.ConfigData.AddDouble("Tcase_temp_cl", TEC_LASER_OL_TEMP);//dsdbr temperature when close loop
            modRun.ConfigData.AddReference("BaseLineChannels", baseLineChannels);

            modRun.ConfigData.AddDouble("MZInitSweepStepSize_mV", progInfo.TestParamsConfig.GetDoubleParam("MZInitSweepStepSize_mV"));
            modRun.ConfigData.AddDouble("MZTapBias_V", progInfo.TestParamsConfig.GetDoubleParam("MZTapBias_V"));
            modRun.ConfigData.AddString("MzFileDirectory", this.CommonTestConfig.ResultsSubDirectory);
            modRun.ConfigData.AddSint32("MZSourceMeasureDelay_ms", progInfo.FinalTestParamsConfig.GetIntParam("MZSourceMeasureDelay_ms"));
            modRun.ConfigData.AddDouble("OpmRangeInit_mW", progInfo.TestParamsConfig.GetDoubleParam("OpmRangeInit_mW"));
            // Tie up limits
            Specification spec = progInfo.MainSpec;

            if (!dutObject.TestStage.ToString().Contains("base"))
            {
                modRun.Limits.AddParameter(spec, "FREQ_CH_FST_DELTA", "Freq_ch_fst_delta");
                modRun.Limits.AddParameter(spec, "FREQ_CH_MID_DELTA", "Freq_ch_mid_delta");
                modRun.Limits.AddParameter(spec, "FREQ_CH_LST_DELTA", "Freq_ch_lst_delta");

                modRun.Limits.AddParameter(spec, "IRX_CH_FST_DELTA", "Irx_ch_fst_delta");
                modRun.Limits.AddParameter(spec, "IRX_CH_MID_DELTA", "Irx_ch_mid_delta");
                modRun.Limits.AddParameter(spec, "IRX_CH_LST_DELTA", "Irx_ch_lst_delta");

                modRun.Limits.AddParameter(spec, "ITX_CH_FST_DELTA", "Itx_ch_fst_delta");
                modRun.Limits.AddParameter(spec, "ITX_CH_MID_DELTA", "Itx_ch_mid_delta");
                modRun.Limits.AddParameter(spec, "ITX_CH_LST_DELTA", "Itx_ch_lst_delta");

                modRun.Limits.AddParameter(spec, "LRATIO_CH_FST_DELTA", "LRatio_ch_fst_delta");
                modRun.Limits.AddParameter(spec, "LRATIO_CH_MID_DELTA", "LRatio_ch_mid_delta");
                modRun.Limits.AddParameter(spec, "LRATIO_CH_LST_DELTA", "LRatio_ch_lst_delta");

                modRun.Limits.AddParameter(spec, "PRATIO_CH_FST_DELTA", "PRatio_ch_fst_delta");
                modRun.Limits.AddParameter(spec, "PRATIO_CH_MID_DELTA", "PRatio_ch_mid_delta");
                modRun.Limits.AddParameter(spec, "PRATIO_CH_LST_DELTA", "PRatio_ch_lst_delta");

                modRun.Limits.AddParameter(spec, "MZ_CTAP_QUAD_CH_FST_DELTA", "Ctap_quad_ch_fst_delta");
                modRun.Limits.AddParameter(spec, "MZ_CTAP_QUAD_CH_MID_DELTA", "Ctap_quad_ch_mid_delta");
                modRun.Limits.AddParameter(spec, "MZ_CTAP_QUAD_CH_LST_DELTA", "Ctap_quad_ch_lst_delta");

                modRun.Limits.AddParameter(spec, "MZ_DCER_CH_FST_DELTA", "Mz_DcEr_ch_fst_delta");
                modRun.Limits.AddParameter(spec, "MZ_DCER_CH_MID_DELTA", "Mz_DcEr_ch_mid_delta");
                modRun.Limits.AddParameter(spec, "MZ_DCER_CH_LST_DELTA", "Mz_DcEr_ch_lst_delta");

                modRun.Limits.AddParameter(spec, "MZ_VPEAK_LEFT_CH_FST_DELTA", "Mz_vpeak_left_ch_fst_delta");
                modRun.Limits.AddParameter(spec, "MZ_VPEAK_LEFT_CH_MID_DELTA", "Mz_vpeak_left_ch_mid_delta");
                modRun.Limits.AddParameter(spec, "MZ_VPEAK_LEFT_CH_LST_DELTA", "Mz_vpeak_left_ch_lst_delta");

                modRun.Limits.AddParameter(spec, "MZ_VPEAK_RIGHT_CH_FST_DELTA", "Mz_vpeak_right_ch_fst_delta");
                modRun.Limits.AddParameter(spec, "MZ_VPEAK_RIGHT_CH_MID_DELTA", "Mz_vpeak_right_ch_mid_delta");
                modRun.Limits.AddParameter(spec, "MZ_VPEAK_RIGHT_CH_LST_DELTA", "Mz_vpeak_right_ch_lst_delta");

                modRun.Limits.AddParameter(spec, "MZ_VPI_CH_FST_DELTA", "Mz_vpi_ch_fst_delta");
                modRun.Limits.AddParameter(spec, "MZ_VPI_CH_MID_DELTA", "Mz_vpi_ch_mid_delta");
                modRun.Limits.AddParameter(spec, "MZ_VPI_CH_LST_DELTA", "Mz_vpi_ch_lst_delta");

                modRun.Limits.AddParameter(spec, "PWR_CH_FST_DELTA", "Pwr_ch_fst_delta");
                modRun.Limits.AddParameter(spec, "PWR_CH_MID_DELTA", "Pwr_ch_mid_delta");
                modRun.Limits.AddParameter(spec, "PWR_CH_LST_DELTA", "Pwr_ch_lst_delta");

                modRun.Limits.AddParameter(spec, "TCASE_HIGHT_CL_DELTA", "Tcase_hight_cl_delta");
            }

            modRun.Limits.AddParameter(spec, "FREQ_CH_FST", "Freq_ch_fst");
            modRun.Limits.AddParameter(spec, "FREQ_CH_MID", "Freq_ch_mid");
            modRun.Limits.AddParameter(spec, "FREQ_CH_LST", "Freq_ch_lst");

            modRun.Limits.AddParameter(spec, "IRX_CH_FST", "Irx_ch_fst");
            modRun.Limits.AddParameter(spec, "IRX_CH_MID", "Irx_ch_mid");
            modRun.Limits.AddParameter(spec, "IRX_CH_LST", "Irx_ch_lst");

            modRun.Limits.AddParameter(spec, "ITX_CH_FST", "Itx_ch_fst");
            modRun.Limits.AddParameter(spec, "ITX_CH_MID", "Itx_ch_mid");
            modRun.Limits.AddParameter(spec, "ITX_CH_LST", "Itx_ch_lst");

            modRun.Limits.AddParameter(spec, "LRATIO_CH_FST", "LRatio_ch_fst");
            modRun.Limits.AddParameter(spec, "LRATIO_CH_MID", "LRatio_ch_mid");
            modRun.Limits.AddParameter(spec, "LRATIO_CH_LST", "LRatio_ch_lst");

            modRun.Limits.AddParameter(spec, "PRATIO_CH_FST", "PRatio_ch_fst");
            modRun.Limits.AddParameter(spec, "PRATIO_CH_MID", "PRatio_ch_mid");
            modRun.Limits.AddParameter(spec, "PRATIO_CH_LST", "PRatio_ch_lst");

            modRun.Limits.AddParameter(spec, "MZ_CTAP_QUAD_CH_FST", "Ctap_quad_ch_fst");
            modRun.Limits.AddParameter(spec, "MZ_CTAP_QUAD_CH_MID", "Ctap_quad_ch_mid");
            modRun.Limits.AddParameter(spec, "MZ_CTAP_QUAD_CH_LST", "Ctap_quad_ch_lst");

            modRun.Limits.AddParameter(spec, "MZ_DCER_CH_FST", "Mz_DcEr_ch_fst");
            modRun.Limits.AddParameter(spec, "MZ_DCER_CH_MID", "Mz_DcEr_ch_mid");
            modRun.Limits.AddParameter(spec, "MZ_DCER_CH_LST", "Mz_DcEr_ch_lst");

            modRun.Limits.AddParameter(spec, "MZ_VPEAK_LEFT_CH_FST", "Mz_vpeak_left_ch_fst");
            modRun.Limits.AddParameter(spec, "MZ_VPEAK_LEFT_CH_MID", "Mz_vpeak_left_ch_mid");
            modRun.Limits.AddParameter(spec, "MZ_VPEAK_LEFT_CH_LST", "Mz_vpeak_left_ch_lst");

            modRun.Limits.AddParameter(spec, "MZ_VPEAK_RIGHT_CH_FST", "Mz_vpeak_right_ch_fst");
            modRun.Limits.AddParameter(spec, "MZ_VPEAK_RIGHT_CH_MID", "Mz_vpeak_right_ch_mid");
            modRun.Limits.AddParameter(spec, "MZ_VPEAK_RIGHT_CH_LST", "Mz_vpeak_right_ch_lst");

            modRun.Limits.AddParameter(spec, "MZ_VPI_CH_FST", "Mz_vpi_ch_fst");
            modRun.Limits.AddParameter(spec, "MZ_VPI_CH_MID", "Mz_vpi_ch_mid");
            modRun.Limits.AddParameter(spec, "MZ_VPI_CH_LST", "Mz_vpi_ch_lst");

            modRun.Limits.AddParameter(spec, "PWR_CH_FST", "Pwr_ch_fst");
            modRun.Limits.AddParameter(spec, "PWR_CH_MID", "Pwr_ch_mid");
            modRun.Limits.AddParameter(spec, "PWR_CH_LST", "Pwr_ch_lst");


            modRun.Limits.AddParameter(spec, "MZ_LV_SWEEP_FILE_CH_FST", "Mz_sweep_file_ch_fst");
            modRun.Limits.AddParameter(spec, "MZ_LV_SWEEP_FILE_CH_MID", "Mz_sweep_file_ch_mid");
            modRun.Limits.AddParameter(spec, "MZ_LV_SWEEP_FILE_CH_LST", "Mz_sweep_file_ch_lst");

            modRun.Limits.AddParameter(spec, "TCASE_HIGHT_CL", "Dsdbr_temp_cl");
        }
        /// <summary>
        /// Initialise IlmzLowTempTest module
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        private void IlmzLowTempTest_InitModule(ITestEngineInit engine, DUTObject dutObject)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("IlmzLowTempTest", "IlmzTempTest_SPC", "");

            // Add instruments
            modRun.ConfigData.AddReference("MzInstruments", progInfo.Instrs.Mz);
            IlMzDriverUtils.MaxModBias_V = double.Parse(progInfo.TestParamsConfig.GetDoubleParam("MZMaxBias_V").ToString());

            //modRun.Instrs.Add("MzTec", (Instrument)progInfo.Instrs.TecMz);
            modRun.Instrs.Add("DsdbrTec", (Instrument)progInfo.Instrs.TecDsdbr);
            modRun.Instrs.Add("Optical_switch", (Instrument)progInfo.Instrs.TecDsdbr);
            //modRun.Instrs.Add("OSA", (Instrument)progInfo.Instrs.Osa);
            //modRun.ConfigData.AddDouble("SMSRMeasure_Span_nm", progInfo.TestParamsConfig.GetDoubleParam("SMSRMeasure_Span_nm"));
            //modRun.ConfigData.AddDouble("SMSRMeasure_RBW_nm", progInfo.TestParamsConfig.GetDoubleParam("SMSRMeasure_RBW_nm"));
            //modRun.ConfigData.AddDouble("SMSRMeasure_OSASensitivity_dBm", progInfo.TestParamsConfig.GetDoubleParam("SMSRMeasure_OSASensitivity_dBm"));
            //modRun.ConfigData.AddSint32("SMSRMeasure_NumPts", progInfo.TestParamsConfig.GetIntParam("SMSRMeasure_NumPts"));

            switch (progInfo.DsdbrInstrsUsed)
            {
                // added FCU2ASIC Option by Alice.Huang, for TOSA GB test     2010-02-01
                case DsdbrDriveInstruments.FCU2AsicInstrument:
                case DsdbrDriveInstruments.FCUInstruments:
                    modRun.Instrs.Add("PowerHead", (Instrument)progInfo.Instrs.Mz.PowerMeter);
                    break;
                case DsdbrDriveInstruments.PXIInstruments:
                    modRun.Instrs.Add("PowerHead", (Instrument)progInfo.Instrs.OpmCgDirect);
                    break;
            }
            modRun.ConfigData.AddReference("FailModeCheck", this.failModeCheck);
            modRun.ConfigData.AddReference("Specification", progInfo.MainSpec);
            modRun.ConfigData.AddString("TestStage", dutObject.TestStage);
            //modRun.ConfigData.AddReference("SwitchOsaMzOpm", progInfo.Instrs.SwitchOsaMzOpm);
            modRun.ConfigData.AddString("BaseLineReferenceFile", progInfo.baseLineReferenceFile);
            modRun.ConfigData.AddReference("BaseLineChannels", baseLineChannels);

            modRun.ConfigData.AddDouble("MZInitSweepStepSize_mV", progInfo.TestParamsConfig.GetDoubleParam("MZInitSweepStepSize_mV"));
            modRun.ConfigData.AddDouble("MZTapBias_V", progInfo.TestParamsConfig.GetDoubleParam("MZTapBias_V"));
            modRun.ConfigData.AddString("MzFileDirectory", this.CommonTestConfig.ResultsSubDirectory);

            modRun.ConfigData.AddDouble("OpmRangeInit_mW", progInfo.TestParamsConfig.GetDoubleParam("OpmRangeInit_mW"));
            modRun.ConfigData.AddSint32("MZSourceMeasureDelay_ms", progInfo.FinalTestParamsConfig.GetIntParam("MZSourceMeasureDelay_ms"));
            modRun.ConfigData.AddDouble("CaseTempLow_C", TestCondition_LowTemp);
            // Add config data
            Specification spec = progInfo.MainSpec;

            if (!dutObject.TestStage.ToString().Contains("base"))
            {
                modRun.Limits.AddParameter(spec, "FREQ_CH_FST_LOWTEMP_DELTA", "Freq_ch_fst_lowtemp_delta");
                modRun.Limits.AddParameter(spec, "FREQ_CH_MID_LOWTEMP_DELTA", "Freq_ch_mid_lowtemp_delta");
                modRun.Limits.AddParameter(spec, "FREQ_CH_LST_LOWTEMP_DELTA", "Freq_ch_lst_lowtemp_delta");

                modRun.Limits.AddParameter(spec, "PWR_CH_FST_LOWTEMP_DELTA", "Pwr_ch_fst_lowtemp_delta");
                modRun.Limits.AddParameter(spec, "PWR_CH_MID_LOWTEMP_DELTA", "Pwr_ch_mid_lowtemp_delta");
                modRun.Limits.AddParameter(spec, "PWR_CH_LST_LOWTEMP_DELTA", "Pwr_ch_lst_lowtemp_delta");
            }
            modRun.Limits.AddParameter(spec, "FREQ_CH_FST_LOWTEMP", "Freq_ch_fst_Low");
            modRun.Limits.AddParameter(spec, "FREQ_CH_MID_LOWTEMP", "Freq_ch_mid_Low");
            modRun.Limits.AddParameter(spec, "FREQ_CH_LST_LOWTEMP", "Freq_ch_lst_Low");

            modRun.Limits.AddParameter(spec, "PWR_CH_FST_LOWTEMP", "Pwr_ch_fst_Low");
            modRun.Limits.AddParameter(spec, "PWR_CH_MID_LOWTEMP", "Pwr_ch_mid_Low");
            modRun.Limits.AddParameter(spec, "PWR_CH_LST_LOWTEMP", "Pwr_ch_lst_Low");
        }

        /// <summary>
        /// Initialise TcmHighTempTest module
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        private void IlmzHighTempTest_InitModule(ITestEngineInit engine, DUTObject dutObject)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("IlmzHighTempTest", "IlmzTempTest_SPC", "");

            //modRun.ConfigData.AddString("current_condition", "High_temp");
            // Add instruments
            modRun.ConfigData.AddReference("MzInstruments", progInfo.Instrs.Mz);
            IlMzDriverUtils.MaxModBias_V = double.Parse(progInfo.TestParamsConfig.GetDoubleParam("MZMaxBias_V").ToString());

            //modRun.Instrs.Add("MzTec", (Instrument)progInfo.Instrs.TecMz);
            modRun.Instrs.Add("DsdbrTec", (Instrument)progInfo.Instrs.TecDsdbr);
            modRun.Instrs.Add("Optical_switch", (Instrument)progInfo.Instrs.TecDsdbr);
            //modRun.Instrs.Add("OSA", (Instrument)progInfo.Instrs.Osa);
            //modRun.ConfigData.AddDouble("SMSRMeasure_Span_nm", progInfo.TestParamsConfig.GetDoubleParam("SMSRMeasure_Span_nm"));
            //modRun.ConfigData.AddDouble("SMSRMeasure_RBW_nm", progInfo.TestParamsConfig.GetDoubleParam("SMSRMeasure_RBW_nm"));
            //modRun.ConfigData.AddDouble("SMSRMeasure_OSASensitivity_dBm", progInfo.TestParamsConfig.GetDoubleParam("SMSRMeasure_OSASensitivity_dBm"));
            //modRun.ConfigData.AddSint32("SMSRMeasure_NumPts", progInfo.TestParamsConfig.GetIntParam("SMSRMeasure_NumPts"));

            switch (progInfo.DsdbrInstrsUsed)
            {
                // added FCU2ASIC Option by Alice.Huang, for TOSA GB test     2010-02-01
                case DsdbrDriveInstruments.FCU2AsicInstrument:
                case DsdbrDriveInstruments.FCUInstruments:
                    modRun.Instrs.Add("PowerHead", (Instrument)progInfo.Instrs.Mz.PowerMeter);
                    break;
                case DsdbrDriveInstruments.PXIInstruments:
                    modRun.Instrs.Add("PowerHead", (Instrument)progInfo.Instrs.OpmCgDirect);
                    break;
            }
            modRun.ConfigData.AddReference("FailModeCheck", this.failModeCheck);
            modRun.ConfigData.AddReference("Specification", progInfo.MainSpec);
            modRun.ConfigData.AddString("TestStage", dutObject.TestStage);
            //modRun.ConfigData.AddReference("SwitchOsaMzOpm", progInfo.Instrs.SwitchOsaMzOpm);
            modRun.ConfigData.AddString("BaseLineReferenceFile", progInfo.baseLineReferenceFile);
            modRun.ConfigData.AddReference("BaseLineChannels", baseLineChannels);

            modRun.ConfigData.AddDouble("MZInitSweepStepSize_mV", progInfo.TestParamsConfig.GetDoubleParam("MZInitSweepStepSize_mV"));
            modRun.ConfigData.AddDouble("MZTapBias_V", progInfo.TestParamsConfig.GetDoubleParam("MZTapBias_V"));
            modRun.ConfigData.AddString("MzFileDirectory", this.CommonTestConfig.ResultsSubDirectory);

            modRun.ConfigData.AddDouble("OpmRangeInit_mW", progInfo.TestParamsConfig.GetDoubleParam("OpmRangeInit_mW"));
            modRun.ConfigData.AddSint32("MZSourceMeasureDelay_ms", progInfo.FinalTestParamsConfig.GetIntParam("MZSourceMeasureDelay_ms"));
            // Add config data
            Specification spec = progInfo.MainSpec;
            if (!dutObject.TestStage.ToString().Contains("base"))
            {
                modRun.Limits.AddParameter(spec, "FREQ_CH_FST_HIGHTEMP_DELTA", "Freq_ch_fst_hightemp_delta");
                modRun.Limits.AddParameter(spec, "FREQ_CH_MID_HIGHTEMP_DELTA", "Freq_ch_mid_hightemp_delta");
                modRun.Limits.AddParameter(spec, "FREQ_CH_LST_HIGHTEMP_DELTA", "Freq_ch_lst_hightemp_delta");

                modRun.Limits.AddParameter(spec, "PWR_CH_FST_HIGHTEMP_DELTA", "Pwr_ch_fst_hightemp_delta");
                modRun.Limits.AddParameter(spec, "PWR_CH_MID_HIGHTEMP_DELTA", "Pwr_ch_mid_hightemp_delta");
                modRun.Limits.AddParameter(spec, "PWR_CH_LST_HIGHTEMP_DELTA", "Pwr_ch_lst_hightemp_delta");

                //raul add power dissipation
                //modRun.Limits.AddParameter(spec, "TECDSDBRCURRENT_CH_FST_H", "TecDsdbrCurrent_ch_fst_hightemp");
                //modRun.Limits.AddParameter(spec, "TECDSDBRCURRENT_CH_MID_H", "TecDsdbrCurrent_ch_mid_hightemp");
                //modRun.Limits.AddParameter(spec, "TECDSDBRCURRENT_CH_LST_H", "TecDsdbrCurrent_ch_lst_hightemp");

                //modRun.Limits.AddParameter(spec, "TECDSDBRVOLTAGE_CH_FST_H", "TecDsdbrVoltage_ch_fst_hightemp");
                //modRun.Limits.AddParameter(spec, "TECDSDBRVOLTAGE_CH_MID_H", "TecDsdbrVoltage_ch_mid_hightemp");
                //modRun.Limits.AddParameter(spec, "TECDSDBRVOLTAGE_CH_LST_H", "TecDsdbrVoltage_ch_lst_hightemp");

                modRun.Limits.AddParameter(spec, "PACKAGEPOWER_CH_FST_H_DELTA", "PackagePower_ch_fst_hightemp_delta");
                modRun.Limits.AddParameter(spec, "PACKAGEPOWER_CH_MID_H_DELTA", "PackagePower_ch_mid_hightemp_delta");
                modRun.Limits.AddParameter(spec, "PACKAGEPOWER_CH_LST_H_DELTA", "PackagePower_ch_lst_hightemp_delta");
            }
            modRun.Limits.AddParameter(spec, "FREQ_CH_FST_HIGHTEMP", "Freq_ch_fst_High");
            modRun.Limits.AddParameter(spec, "FREQ_CH_MID_HIGHTEMP", "Freq_ch_mid_High");
            modRun.Limits.AddParameter(spec, "FREQ_CH_LST_HIGHTEMP", "Freq_ch_lst_High");

            modRun.Limits.AddParameter(spec, "PWR_CH_FST_HIGHTEMP", "Pwr_ch_fst_High");
            modRun.Limits.AddParameter(spec, "PWR_CH_MID_HIGHTEMP", "Pwr_ch_mid_High");
            modRun.Limits.AddParameter(spec, "PWR_CH_LST_HIGHTEMP", "Pwr_ch_lst_High");

            //raul added power dissipation
            modRun.Limits.AddParameter(spec, "PACKAGEPOWER_CH_FST_H", "PackagePower_ch_fst_High");
            modRun.Limits.AddParameter(spec, "PACKAGEPOWER_CH_MID_H", "PackagePower_ch_mid_High");
            modRun.Limits.AddParameter(spec, "PACKAGEPOWER_CH_LST_H", "PackagePower_ch_lst_High");
        }

        #endregion

        #region Configure instruments
        /// <summary>
        /// Configure a TEC controller using config data
        /// </summary>
        /// <param name="tecCtlr">Instrument reference</param>
        /// <param name="tecCtlId">Config table data prefix</param>
        private void ConfigureTecController(ITestEngineBase engine, IInstType_TecController tecCtlr, string tecCtlId, bool isTecCase, double temperatureSetPoint_C)
        {
            SteinhartHartCoefficients stCoeffs = new SteinhartHartCoefficients();

            if (tecCtlr.DriverName.Contains("Ke2510"))
            {
                // Keithley 2510 specific commands (must be done before 'SetDefaultState')
                tecCtlr.HardwareData["MinTemperatureLimit_C"] = progInfo.FinalTestParamsConfig.GetStringParam(tecCtlId + "_MinTemp_C");
                tecCtlr.HardwareData["MaxTemperatureLimit_C"] = progInfo.FinalTestParamsConfig.GetStringParam(tecCtlId + "_MaxTemp_C");

                tecCtlr.SetDefaultState();

                tecCtlr.Sensor_Type = (InstType_TecController.SensorType)Enum.Parse(typeof(InstType_TecController.SensorType), progInfo.FinalTestParamsConfig.GetStringParam(tecCtlId + "_Sensor_Type"));
                
                tecCtlr.TecVoltageCompliance_volt = progInfo.FinalTestParamsConfig.GetDoubleParam(tecCtlId + "_TecVoltageCompliance_volt");

                if (isTecCase)
                {
                    tecCtlr.DerivativeGain = progInfo.LocalTestParamsConfig.GetDoubleParam(tecCtlId + "_DerivativeGain");
                }
                else
                {
                    tecCtlr.DerivativeGain = progInfo.FinalTestParamsConfig.GetDoubleParam(tecCtlId + "_DerivativeGain");
                }
            }
            else
            {
                tecCtlr.SetDefaultState();
            }

            tecCtlr.OperatingMode = (InstType_TecController.ControlMode)Enum.Parse(typeof(InstType_TecController.ControlMode), progInfo.FinalTestParamsConfig.GetStringParam(tecCtlId + "_OperatingMode"));
            
            // Thermistor characteristics
            if (tecCtlr.Sensor_Type == InstType_TecController.SensorType.Thermistor_2wire ||
                tecCtlr.Sensor_Type == InstType_TecController.SensorType.Thermistor_4wire)
            {
                stCoeffs.A = progInfo.FinalTestParamsConfig.GetDoubleParam(tecCtlId + "_stCoeffs_A");
                stCoeffs.B = progInfo.FinalTestParamsConfig.GetDoubleParam(tecCtlId + "_stCoeffs_B");
                stCoeffs.C = progInfo.FinalTestParamsConfig.GetDoubleParam(tecCtlId + "_stCoeffs_C");
                tecCtlr.SteinhartHartConstants = stCoeffs;
            }

            // Additional parameters
            if (isTecCase)
            {
                tecCtlr.TecCurrentCompliance_amp = progInfo.LocalTestParamsConfig.GetDoubleParam(tecCtlId + "_TecCurrentCompliance_amp");
                //NT10A do not support below fuction, Rosa comment on June 13,2011
                //tecCtlr.TecVoltageCompliance_volt = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_TecVoltageCompliance_volt");
                tecCtlr.ProportionalGain = progInfo.LocalTestParamsConfig.GetDoubleParam(tecCtlId + "_ProportionalGain");
                tecCtlr.IntegralGain = progInfo.LocalTestParamsConfig.GetDoubleParam(tecCtlId + "_IntegralGain");
                //NT10A do not support below fuction, Rosa comment on June 13,2011
                //tecCtlr.DerivativeGain = progInfo.LocalTestParamsConfig.GetDoubleParam(tecCtlId + "_DerivativeGain");
            }
            else
            {
                tecCtlr.TecCurrentCompliance_amp = progInfo.FinalTestParamsConfig.GetDoubleParam(tecCtlId + "_TecCurrentCompliance_amp");
                //NT10A do not support below fuction, Rosa comment on June 13,2011
                //tecCtlr.TecVoltageCompliance_volt = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_TecVoltageCompliance_volt");
                tecCtlr.ProportionalGain = progInfo.FinalTestParamsConfig.GetDoubleParam(tecCtlId + "_ProportionalGain");
                tecCtlr.IntegralGain = progInfo.FinalTestParamsConfig.GetDoubleParam(tecCtlId + "_IntegralGain");
                //NT10A do not support below fuction, Rosa comment on June 13,2011
                //tecCtlr.DerivativeGain = progInfo.FinalTestParamsConfig.GetDoubleParam(tecCtlId + "_DerivativeGain");
            }

            try
            {
                tecCtlr.SensorTemperatureSetPoint_C = temperatureSetPoint_C;
            }
            catch (Exception e)
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, e.Message, FailModeCategory_Enum.UN);
            }
        }

        /// <summary>
        /// Read instruments calibration data
        /// </summary>
        /// <param name="InstrumentName"></param>
        /// <param name="InstrumentSerialNo"></param>
        /// <param name="ParameterName"></param>
        /// <param name="CalFactor"></param>
        /// <param name="CalOffset"></param>
        private void readCalData(string InstrumentName, string InstrumentSerialNo,
            string ParameterName, out double CalFactor, out double CalOffset)
        {
            StringDictionary keys = new StringDictionary();
            keys.Add("Instrument_name", InstrumentName);
            keys.Add("Parameter", ParameterName);
            if (InstrumentSerialNo != null)
            {
                keys.Add("Instrument_SerialNo", InstrumentSerialNo);
            }
            DatumList calParams = progInfo.InstrumentsCalData.GetData(keys, false);

            CalFactor = calParams.ReadDouble("CalFactor");
            CalOffset = calParams.ReadDouble("CalOffset");
        }

        // added FCU2ASIC Option by Alice.Huang, for TOSA GB test     2010-02-01
        /// <summary>
        /// initialise FCU2ASIC INSTRUMENTS 
        /// </summary>
        /// <param name="fcu2AsicInstrs">FCU2ASIC instruments group </param>
        private void ConfigureFCU2AsicInstruments(FCU2AsicInstruments fcu2AsicInstrs)
        {
            string fcu2asic_InstrName = "FCU2Asic";
            //Configure instrument

            fcu2AsicInstrs.AsicVccSource.SetDefaultState();
            double curMax = progInfo.LocalTestParamsConfig.GetDoubleParam("AsicVccSource_CurrentCompliance_A");
            Inst_Ke24xx keSource = fcu2AsicInstrs.AsicVccSource as Inst_Ke24xx;

            //fcu2AsicInstrs .AsicVccSource.U
            //fcu2AsicInstrs.AsicVccSource 
            // Set to voltage source mode & set current compliance
            double voltSetPoint = progInfo.LocalTestParamsConfig.GetDoubleParam("AsicVccSource_Voltage_V");
            fcu2AsicInstrs.AsicVccSource.VoltageComplianceSetPoint_Volt = voltSetPoint;
            fcu2AsicInstrs.AsicVccSource.CurrentComplianceSetPoint_Amp = curMax;
            fcu2AsicInstrs.AsicVccSource.VoltageSetPoint_Volt = voltSetPoint;
            fcu2AsicInstrs.AsicVccSource.OutputEnabled = true;
            if (keSource != null)
            {
                keSource.UseFrontTerminals = true;
                keSource.FourWireSense = false;
                keSource.SenseCurrent(curMax, curMax);
            }

            fcu2AsicInstrs.AsicVeeSource.SetDefaultState();
            keSource = fcu2AsicInstrs.AsicVccSource as Inst_Ke24xx;
            curMax = progInfo.LocalTestParamsConfig.GetDoubleParam("AsicVeeSource_CurrentCompliance_A");

            // Set to voltage source mode & set current compliance 
            voltSetPoint = progInfo.LocalTestParamsConfig.GetDoubleParam("AsicVeeSource_Voltage_V");
            fcu2AsicInstrs.AsicVeeSource.VoltageComplianceSetPoint_Volt = voltSetPoint;
            fcu2AsicInstrs.AsicVeeSource.CurrentComplianceSetPoint_Amp = curMax;
            fcu2AsicInstrs.AsicVeeSource.VoltageSetPoint_Volt = voltSetPoint;
            fcu2AsicInstrs.AsicVeeSource.OutputEnabled = true;
            if (keSource != null)
            {
                keSource.UseFrontTerminals = true;
                keSource.FourWireSense = false;
                keSource.SenseCurrent(curMax, curMax);
            }

            // if fcu asic instrs exists, setup fcu source output

            if (fcu2AsicInstrs.FcuSource != null)
            {

                fcu2AsicInstrs.FcuSource.OutputEnabled = false;

                fcu2AsicInstrs.FcuSource.VoltageSetPoint_Volt = progInfo.LocalTestParamsConfig.GetDoubleParam("FcuSource_Voltage_V");
                fcu2AsicInstrs.FcuSource.CurrentComplianceSetPoint_Amp = progInfo.LocalTestParamsConfig.GetDoubleParam("FcuSource_CurrentCompliance_A");
                fcu2AsicInstrs.FcuSource.OutputEnabled = true;
                Thread.Sleep(2000);
            }


            double CalFactor;
            double CalOffset;
            double Tc_CalOffset = progInfo.TestParamsConfig.GetDoubleParam("Tc_CalOffset");
            double FreqTolerance_GHz = progInfo.TestParamsConfig.GetDoubleParam("FreqTolerance_GHz");
            double ItuSpacing_GHz = progInfo.TestParamsConfig.GetDoubleParam("ItuSpacing_GHz");

            string FrequencyCalibration_file_Path = "L_band_FrequencyCalibration_file_Path";
            string str = progInfo.TestConditions.FreqBand.ToString();
            if (str == "C")
                FrequencyCalibration_file_Path = "C_band_FrequencyCalibration_file_Path";

            string freqCalFilename = progInfo.MapParamsConfig.GetStringParam(FrequencyCalibration_file_Path);
            string FCUMKI_Tx_CalFilename = progInfo.MapParamsConfig.GetStringParam("FCUMKI_Tx_Calibration_file_Path");
            string FCUMKI_Rx_CalFilename = progInfo.MapParamsConfig.GetStringParam("FCUMKI_Rx_Calibration_file_Path");
            string FCUMKI_Var_CalFilename = progInfo.MapParamsConfig.GetStringParam("FCUMKI_Var_Calibration_file_Path");
            string FCUMKI_Fix_CalFilename = progInfo.MapParamsConfig.GetStringParam("FCUMKI_Fix_Calibration_file_Path");

            FreqCalByEtalon.LoadFreqlCalArray(freqCalFilename, Tc_CalOffset, FreqTolerance_GHz, ItuSpacing_GHz);
            FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Tx = FreqCalByEtalon.LoadFCUMKICalArray(FCUMKI_Tx_CalFilename);
            FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Rx = FreqCalByEtalon.LoadFCUMKICalArray(FCUMKI_Rx_CalFilename);
            FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Var = FreqCalByEtalon.LoadFCUMKICalArray(FCUMKI_Var_CalFilename);
            FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Fix = FreqCalByEtalon.LoadFCUMKICalArray(FCUMKI_Fix_CalFilename);

            int dac_trans_mon = 0;
            int dac_ref_mon = 0;
            int dac_Reference_mon = 0;
            int dac_Filter_mon = 0;
            double average_count = 5.0;

            for (int j = 0; j < average_count; j++)
            {
                dac_ref_mon = dac_ref_mon + progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.Locker_refi_mon;
                dac_trans_mon = dac_trans_mon + progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.Locker_tran_mon;
                dac_Filter_mon = dac_Filter_mon + progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.Var_port_Dac;
                dac_Reference_mon = dac_Reference_mon + progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.Fix_Port_Dac;
            }
            dac_trans_mon = (int)Math.Round(dac_trans_mon / average_count);
            dac_ref_mon = (int)Math.Round(dac_ref_mon / average_count);
            dac_Reference_mon = (int)Math.Round(dac_Reference_mon / average_count);
            dac_Filter_mon = (int)Math.Round(dac_Filter_mon / average_count);


            progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic._Fix_Port_Dark_Current_Dac = dac_Reference_mon;
            progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic._Var_Port_Dark_Current_Dac = dac_Filter_mon;
            progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic._Locker_Tx_Dark_Current_Dac = dac_trans_mon;
            progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic._Locker_Rx_Dark_Current_Dac = dac_ref_mon;

        }
        /// <summary>
        /// Configure a MZ bias source
        /// </summary>
        /// <param name="biasSrc">Instrument reference</param>
        /// <param name="biasSrcId">Config table data prefix</param>
        private void ConfigureMzBiasSource(IInstType_ElectricalSource biasSrc, string biasSrcId)
        {
            // Keithley 2400 specific commands (must be done before 'SetDefaultState')
            if (biasSrc.DriverName.Contains("Ke24xx"))
            {
                biasSrc.HardwareData["4wireSense"] = "true";
            }

            // Configure instrument
            biasSrc.SetDefaultState();
            // Set to current source mode & set compliance
            biasSrc.CurrentSetPoint_amp = 0.0;
            biasSrc.CurrentComplianceSetPoint_Amp = progInfo.TestParamsConfig.GetDoubleParam(biasSrcId + "_CurrentCompliance_A");
            // Set to voltage source mode & set compliance
            biasSrc.VoltageSetPoint_Volt = 0.0;
            biasSrc.VoltageComplianceSetPoint_Volt = progInfo.TestParamsConfig.GetDoubleParam(biasSrcId + "_VoltageCompliance_volt");
        }

        #endregion config instruments
        
        #region Program Running

        public override void Run(ITestEngineRun engine, DUTObject dutObject, InstrumentCollection instrs, ChassisCollection chassis)
        {
            InstCommands.CloseInstToLoadDUT(engine, progInfo.Instrs.Fcu2AsicInstrsGroups.FcuSource, progInfo.Instrs.Fcu2AsicInstrsGroups.AsicVccSource, progInfo.Instrs.Fcu2AsicInstrsGroups.AsicVeeSource, progInfo.Instrs.TecDsdbr);

            //engine.ShowContinueUserQuery("Please click the continue button to test,���������,��� Continue ����");

            //First do pin check
            engine.RunModule("Mod_PinCheck");
            ModRunData Pin_Check_Results = engine.GetModuleRunData("Mod_PinCheck");
            bool Pin_Check_Pass = Pin_Check_Results.ModuleData.ReadBool("Pin_Check_Pass");
            if (!Pin_Check_Pass)
            {
                return;
            }

            ModuleRun modRun;

            //load etalon wavemeter cal data from file


            string FrequencyCalibration_file_Path = "L_band_FrequencyCalibration_file_Path";

            string str = progInfo.TestConditions.FreqBand.ToString();
            if (str == "C")
                FrequencyCalibration_file_Path = "C_band_FrequencyCalibration_file_Path";
            string freqCalFilename = progInfo.TestParamsConfig.GetStringParam(FrequencyCalibration_file_Path);
            //double cal_offset=double.Parse(progInfo.MainSpec.GetParamLimit("TC_CAL_OFFSET").LowLimit.ValueToString());

            // Asic Part
            progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.SetupChannelCalibration();
            double cal_offset = 0;
            FreqCalByEtalon.LoadFreqlCalArray(freqCalFilename, cal_offset);

            progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic.Inital_Etalon_WaveMter();//to avoid Var pot large change in overallmap FCUMKI pot leveling Jack.Zhang 2011-01-12

            this.VerifyJigTemperauture(engine);
            // Set temperatures
            //  pre set the case temp to save some time
            progInfo.Instrs.TecCase.SensorTemperatureSetPoint_C = this.TestCondition_MidTemp;
            // TODO set MZ & DSDBR according to FF data
            progInfo.Instrs.TecCase.OutputEnabled = true;
            //progInfo.Instrs.TecMz.OutputEnabled = true;

            #region Temperature control - chongjian.liang 2012.12.3

            // Set and wait for temperatures to stabilise

            ModuleRunReturn caseControlResult = engine.RunModule("CaseTemp_Mid");

            if (caseControlResult.ModuleRunData.ModuleData.IsPresent("ErrorInformation")) {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, caseControlResult.ModuleRunData.ModuleData.ReadString("ErrorInformation"), FailModeCategory_Enum.UN);
            }

            ModuleRunReturn dsdbrControlResult = engine.RunModule("DsdbrTemperature");

            if (dsdbrControlResult.ModuleRunData.ModuleData.IsPresent("ErrorInformation")) {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, dsdbrControlResult.ModuleRunData.ModuleData.ReadString("ErrorInformation"), FailModeCategory_Enum.UN);
            }
            #endregion

            // Switch optical path to MzOpm
            //no need for low cost HITT test kit, Rosa added on May 31, 2011
            //this.progInfo.Instrs.SwitchOsaMzOpm.SetState(Switch_Osa_MzOpm.State.MzOpm);




            // Power up
            //RunIlmzPowerUp(engine);
            RunIlmzPowerHeadCal(engine);


            modRun = engine.GetModuleRun("IlmzChannelSPC");
            //modRun.ConfigData.AddReference("BaseLineChannels", baseLineChannels);
            modRun.ConfigData.AddDoubleArray("PowerOffsetArray", powerOffsetArray);
            modRun.ConfigData.AddDouble("Tcase_temp_cl", TEC_LASER_OL_TEMP);//dsdbr temperature when close loop

            engine.RunModule("IlmzChannelSPC");

            // Low case temperature tests
            if (progInfo.TestParamsConfig.GetBoolParam("LowTempTestEnabled"))
            {
                // low temperature test
                engine.RunModule("CaseTemp_Low");
                modRun = engine.GetModuleRun("IlmzLowTempTest");
                modRun.ConfigData.AddDoubleArray("PowerOffsetArray", powerOffsetArray);
                modRun.ConfigData.AddString("current_condition", "Low_temp");
                engine.RunModule("IlmzLowTempTest");

                // high temperature test
                engine.RunModule("CaseTemp_High");
                modRun = engine.GetModuleRun("IlmzHighTempTest");
                modRun.ConfigData.AddDoubleArray("PowerOffsetArray", powerOffsetArray);
                modRun.ConfigData.AddString("current_condition", "High_temp");
                engine.RunModule("IlmzHighTempTest");
            }
            else
            {
                // high temperature test
                engine.RunModule("CaseTemp_High");
                modRun = engine.GetModuleRun("IlmzHighTempTest");
                modRun.ConfigData.AddDoubleArray("PowerOffsetArray", powerOffsetArray);
                modRun.ConfigData.AddString("current_condition", "High_temp");
                engine.RunModule("IlmzHighTempTest");
            }

            // Send the case temperature back to ambient whilst we clean up.
            progInfo.Instrs.TecCase.SensorTemperatureSetPoint_C = 25;


            #region[add power change to spc  2016-12-19];
            ModRunData Mid_Temp_Results = engine.GetModuleRunData("IlmzChannelSPC");

            double Mid_Temp_CH_FST_PWR = Mid_Temp_Results.ModuleData.ReadDouble("Pwr_ch_fst");
            double Mid_Temp_CH_MID_PWR = Mid_Temp_Results.ModuleData.ReadDouble("Pwr_ch_mid");
            double Mid_Temp_CH_LST_PWR = Mid_Temp_Results.ModuleData.ReadDouble("Pwr_ch_lst");


            ModRunData High_Temp_Results = engine.GetModuleRunData("IlmzHighTempTest");


            double High_Temp_CH_FST_PWR = High_Temp_Results.ModuleData.ReadDouble("Pwr_ch_fst_High");
            double High_Temp_CH_MID_PWR = High_Temp_Results.ModuleData.ReadDouble("Pwr_ch_mid_High");
            double High_Temp_CH_LST_PWR = High_Temp_Results.ModuleData.ReadDouble("Pwr_ch_lst_High");


            engine.AddModuleRun("Mod_CH_FST_HIGHTEMP_PWR_Delta", "Mod_CalcDeltas", "0.0.0.1");
            modRun = engine.GetModuleRun("Mod_CH_FST_HIGHTEMP_PWR_Delta");
            modRun.ConfigData.AddSint32("DELTA_TYPE", 0);
            modRun.ConfigData.AddDouble("INPUT_VALUE_1", High_Temp_CH_FST_PWR);
            modRun.ConfigData.AddDouble("INPUT_VALUE_2", Mid_Temp_CH_FST_PWR);

            if (MainSpec.ParamLimitExists("PWR_CH_FST_HIGH_TEMP_DELTA"))
            {
                modRun.Limits.AddParameter(MainSpec, "PWR_CH_FST_HIGH_TEMP_DELTA", "CALC_DELTA");
            }
            engine.RunModule("Mod_CH_FST_HIGHTEMP_PWR_Delta");

            engine.AddModuleRun("Mod_CH_MID_HIGHTEMP_PWR_Delta", "Mod_CalcDeltas", "0.0.0.1");
            modRun = engine.GetModuleRun("Mod_CH_MID_HIGHTEMP_PWR_Delta");
            modRun.ConfigData.AddSint32("DELTA_TYPE", 0);
            modRun.ConfigData.AddDouble("INPUT_VALUE_1", High_Temp_CH_MID_PWR);
            modRun.ConfigData.AddDouble("INPUT_VALUE_2", Mid_Temp_CH_MID_PWR);

            if (MainSpec.ParamLimitExists("PWR_CH_MID_HIGH_TEMP_DELTA"))
            {
                modRun.Limits.AddParameter(MainSpec, "PWR_CH_MID_HIGH_TEMP_DELTA", "CALC_DELTA");
            }

            engine.RunModule("Mod_CH_MID_HIGHTEMP_PWR_Delta");


            engine.AddModuleRun("Mod_CH_LST_HIGHTEMP_PWR_Delta", "Mod_CalcDeltas", "0.0.0.1");
            modRun = engine.GetModuleRun("Mod_CH_LST_HIGHTEMP_PWR_Delta");
            modRun.ConfigData.AddSint32("DELTA_TYPE", 0);
            modRun.ConfigData.AddDouble("INPUT_VALUE_1", High_Temp_CH_FST_PWR);
            modRun.ConfigData.AddDouble("INPUT_VALUE_2", Mid_Temp_CH_LST_PWR);


            if (MainSpec.ParamLimitExists("PWR_CH_LST_HIGH_TEMP_DELTA"))
            {
                modRun.Limits.AddParameter(MainSpec, "PWR_CH_LST_HIGH_TEMP_DELTA", "CALC_DELTA");
            }
            engine.RunModule("Mod_CH_LST_HIGHTEMP_PWR_Delta");

            #endregion

            // Set device to safe case temperature                 
            //engine.RunModule("CaseTemp_Safe");
        }

        /// <summary>
        /// Run IlmzPowerHeadCal module
        /// </summary>
        /// <param name="engine"></param>
        private void RunIlmzPowerHeadCal(ITestEngineRun engine)
        {
            ModuleRun modRun;
            ButtonId skipPowerCal = ButtonId.No;
            if (progInfo.TestParamsConfig.GetBoolParam("AllowSkipPowerCalibration"))
            {
                skipPowerCal = engine.ShowYesNoUserQuery("Do you want to skip power calibration?");
            }
            if (skipPowerCal == ButtonId.Yes)
            {
                GuiMsgs.TcmzPowerCalDataResponse resp;
                do
                {
                    engine.ShowContinueUserQuery("Select Power Calibration Data file");
                    engine.GuiShow();
                    engine.GuiToFront();
                    engine.SendToGui(new GuiMsgs.TcmzPowerCalDataRequest());
                    resp = (GuiMsgs.TcmzPowerCalDataResponse)engine.ReceiveFromGui().Payload;
                    if (!File.Exists(resp.Filename))
                    {
                        engine.ShowContinueUserQuery("Power Calibration Data file doesn't exist, please try again!");
                    }
                } while (!File.Exists(resp.Filename));

                OpticalPowerCal.Initialise(2);
                using (CsvReader cr = new CsvReader())
                {
                    List<string[]> contents = cr.ReadFile(resp.Filename);
                    for (int ii = 1; ii < contents.Count; ii++)
                    {
                        OpticalPowerCal.AddCalibratedPoint(OpticalPowerHead.MZHead, double.Parse(contents[ii][0]), double.Parse(contents[ii][1]) - double.Parse(contents[ii][2]));
                        if (progInfo.DsdbrInstrsUsed == DsdbrDriveInstruments.PXIInstruments)
                        {
                            OpticalPowerCal.AddCalibratedPoint(OpticalPowerHead.OpmCgDirect, double.Parse(contents[ii][0]), double.Parse(contents[ii][1]) - double.Parse(contents[ii][3]));
                            OpticalPowerCal.AddCalibratedPoint(OpticalPowerHead.OpmCgFiltered, double.Parse(contents[ii][0]), double.Parse(contents[ii][1]) - double.Parse(contents[ii][4]));
                        }
                    }
                }
            }
            else
            {
                modRun = engine.GetModuleRun("IlmzPowerHeadCal");
                modRun.ConfigData.AddReference("BaseLineChannels", this.baseLineChannels);
                //modRun.ConfigData.AddSint32("NUM_SM", numberOfSupermodes);
                ModuleRunReturn moduleRunReturn = engine.RunModule("IlmzPowerHeadCal");
                //labourTime += moduleRunReturn.ModuleRunData.ModuleData.GetDatumDouble("LabourTime").Value;
                powerOffsetArray = moduleRunReturn.ModuleRunData.ModuleData.ReadDoubleArray("PowerOffsetArray");

                if (!moduleRunReturn.ModuleRunData.ModuleData.ReadBool("IsSuccessful"))
                {
                    this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "Power calibration failed. Please clean the fiber head, or ask the technician to recalibrate the power if fiber head is OK.", FailModeCategory_Enum.OP);
                }
            }
        }
        
        private void GetTestCondition(ITestEngineInit engine, DUTObject dutObject)
        {

            string[] testCondNames = new string[]
            {
                "TC_CASE_TEMP_HIGH",
                "TC_CASE_TEMP_LOW",
                "TC_CASE_TEMP_MID",
                "TC_DSDBR_TEMP"
                
            };
            DatumList testConds = SpecValuesToDatumList.GetTestConditions(progInfo.MainSpec, testCondNames);

            foreach (Datum d in testConds)
            {
                string datumName = d.Name;
                switch (datumName)
                {
                    case "TC_CASE_TEMP_HIGH":
                        TestCondition_HighTemp = ((DatumDouble)d).Value;
                        break;
                    case "TC_CASE_TEMP_LOW":
                        TestCondition_LowTemp = ((DatumDouble)d).Value;
                        break;
                    //get case mid temperature from SPC template file
                    case "TC_CASE_TEMP_MID":
                        TestCondition_MidTemp = ((DatumDouble)d).Value;
                        break;
                    case "TC_DSDBR_TEMP":
                        this.TC_LASER_TEMP_C = ((DatumDouble)d).Value;
                        break;
                }
            }
            TestCondition_MidTemp = baseLineChannels[0].Case_Temp_Mid_C;
            this.TC_LASER_TEMP_C = baseLineChannels[0].Dsdbr_Temp_C;
        }
        
        #endregion
        
        #region End of Program
        /// <summary>
        /// reset instrument after dut test
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        /// <param name="instrs"></param>
        /// <param name="chassis"></param>
        public override void PostRun(ITestEnginePostRun engine, DUTObject dutObject,
                    InstrumentCollection instrs, ChassisCollection chassis)
        {
            // Display temperature progress
            engine.GuiShow();
            engine.GuiToFront();

            // Cleanup & power off
            if (!engine.IsSimulation)
            {
                TestProgramCore.CloseInstrs_Wavemeter();

                if (progInfo.DsdbrInstrsUsed == DsdbrDriveInstruments.FCU2AsicInstrument)
                {
                    InstCommands.CloseInstToUnLoadDUT(engine, progInfo.Instrs.Fcu2AsicInstrsGroups.Fcu2Asic, progInfo.Instrs.Fcu2AsicInstrsGroups.FcuSource, progInfo.Instrs.Fcu2AsicInstrsGroups.AsicVccSource, progInfo.Instrs.Fcu2AsicInstrsGroups.AsicVeeSource, progInfo.Instrs.TecDsdbr, progInfo.Instrs.TecCase, 25);
                }

                #region Else(Not used)
                else if (progInfo.DsdbrInstrsUsed == DsdbrDriveInstruments.FCUInstruments)
                {
                    // modify for replacing FCU 2400 steven.cui
                    // Shutdown FCU
                    // Call methods on the FCU to switch off the laser.
                    progInfo.Instrs.Fcu.SoaCurrentSource.OutputEnabled = false;

                    progInfo.Instrs.Fcu.FullbandControlUnit.SetIVSource((float)0.0, IVSourceIndexNumbers.Rear);
                    progInfo.Instrs.Fcu.FullbandControlUnit.SetIVSource((float)0.0, IVSourceIndexNumbers.Phase);
                    progInfo.Instrs.Fcu.FullbandControlUnit.SetIVSource((float)0.0, IVSourceIndexNumbers.SOA);
                    progInfo.Instrs.Fcu.FullbandControlUnit.SetIVSource((float)0.0, IVSourceIndexNumbers.Gain);
                    progInfo.Instrs.Fcu.FullbandControlUnit.SetControlRegLaserPSUOn(false);
                    progInfo.Instrs.Fcu.FullbandControlUnit.SetControlRegLaserTecOn(false);

                    progInfo.Instrs.Fcu.FullbandControlUnit.SetControlRegLaserPSUOn(false);
                    TxCommand cmd1 = new TxCommand(0, 0, 0); // TunableModules command
                    cmd1 = progInfo.Instrs.Fcu.FullbandControlUnit.ReadTxCommand();
                    TxCommand cmd2 = new TxCommand(cmd1.Data1, cmd1.Data2,
                        (byte)(cmd1.Data3 | (byte)0x01)); // set bit0 of byte "3"
                    progInfo.Instrs.Fcu.FullbandControlUnit.SetTxCommand(cmd2); // Bit 'LSENABLE' is set for disabling it.
                    progInfo.Instrs.Fcu.FullbandControlUnit.OIFLaserEnable(false);
                    //progInfo.Instrs.Fcu.FCU_Source.OutputEnabled = false;

                    InstCommands.CloseTecsWithSafeTemperature(engine, progInfo.Instrs.TecDsdbr, progInfo.Instrs.TecCase, 25);
                } 
                #endregion

                #region light_tower

                /* Set up Serial Binary Comms */
                // _serialPort = buildSerPort(comPort, rate);
                string lightTower = @"Configuration\TOSA Final\LightTowerParams.xml";
                progInfo.lightTowerConfig = new TestParamConfigAccessor(dutObject,
                    lightTower, "_", "LightTowerParams");
                if (progInfo.lightTowerConfig.GetBoolParam("LightExist"))
                {
                    try
                    {

                        _serialPort = this.buildSerPort(progInfo.lightTowerConfig.GetStringParam("ComNum"), progInfo.lightTowerConfig.GetStringParam("BaudRate"));
                        _serialPort.Close();
                        if (!_serialPort.IsOpen)
                        {
                            _serialPort.Open();
                        }

                        // Add the light tower color.
                        _serialPort.WriteLine(progInfo.lightTowerConfig.GetStringParam("LightYellow"));
                        _serialPort.Close();
                    }
                    catch { }

                }
                #endregion
            }
        }

        /// <summary>
        /// Write Dut test result
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        /// <param name="dutOutcome"></param>
        /// <param name="results"></param>
        /// <param name="userList"></param>
        public override void DataWrite(ITestEngineDataWrite engine, DUTObject dutObject,
                        DUTOutcome dutOutcome, TestData results, UserList userList)
        {
            StringDictionary keys = new StringDictionary();
            keys.Add("SCHEMA", "HIBERDB");
            keys.Add("DEVICE_TYPE", dutObject.PartCode);
            keys.Add("TEST_STAGE", dutObject.TestStage);
            keys.Add("SERIAL_NO", dutObject.SerialNumber);
            keys.Add("SPECIFICATION", progInfo.MainSpec.Name);

            engine.SetDataKeys(keys);

            Data_Ignore_Write(engine);

            //XIAOJIANG 2017.03.20
            if (progInfo.MainSpec.ParamLimitExists("LOT_TYPE"))
                if (dutObject.Attributes.IsPresent("lot_type"))
                    tcTraceData.AddString("LOT_TYPE", dutObject.Attributes.ReadString("lot_type"));
                else
                    tcTraceData.AddString("LOT_TYPE", "Unknown");

            if (progInfo.MainSpec.ParamLimitExists("FACTORY_WORKS_LOTTYPE"))
                if (dutObject.Attributes.IsPresent("lot_type"))
                    tcTraceData.AddString("FACTORY_WORKS_LOTTYPE", dutObject.Attributes.ReadString("lot_type"));
                else
                    tcTraceData.AddString("FACTORY_WORKS_LOTTYPE", "Unknown");

            if (progInfo.MainSpec.ParamLimitExists("LOT_ID"))
                tcTraceData.AddString("LOT_ID", dutObject.BatchID);
            if (progInfo.MainSpec.ParamLimitExists("FACTORY_WORKS_LOTID"))
                //tcTraceData.AddString("FACTORY_WORKS_LOTID", dutObject.BatchID);
                DatumListAddOrUpdate(ref tcTraceData, "FACTORY_WORKS_LOTID", dutObject.BatchID);
            
            //END ADD XIAOJIANG 2017.03.20

            #region Fill in blank TC and CH results
            // Add 'missing data' for TC_* and CH_* parameters
            foreach (ParamLimit paramLimit in progInfo.MainSpec)
            {
                if (paramLimit.ExternalName.StartsWith("TC_") || paramLimit.ExternalName.StartsWith("CH_"))
                {
                    if (!tcTraceData.IsPresent(paramLimit.ExternalName))
                    {
                        Datum dummyValue = null;
                        switch (paramLimit.ParamType)
                        {
                            case DatumType.Double:
                                dummyValue = new DatumDouble(paramLimit.ExternalName, Convert.ToDouble(paramLimit.HighLimit.ValueToString()));
                                break;
                            case DatumType.Sint32:
                                dummyValue = new DatumSint32(paramLimit.ExternalName, Convert.ToInt32(paramLimit.HighLimit.ValueToString()));
                                break;
                            case DatumType.StringType:
                                dummyValue = new DatumString(paramLimit.ExternalName, "MISSING");
                                break;
                            case DatumType.Uint32:
                                dummyValue = new DatumUint32(paramLimit.ExternalName, Convert.ToUInt32(paramLimit.HighLimit.ValueToString()));
                                break;
                        }
                        if (dummyValue != null)
                            tcTraceData.Add(dummyValue);
                    }
                }
            }
            #endregion Fill in blank TC and CH results
            
            double testTime = Math.Round(engine.GetTestTime() / 60, 2);

            tcTraceData.AddString("TIME_DATE", TestTimeStamp_Start);
            // Add testTime
            tcTraceData.AddDouble("TEST_TIME", testTime);

            tcTraceData.AddSint32("NODE", dutObject.NodeID);

            tcTraceData.AddString("EQUIP_ID", dutObject.EquipmentID);

            if (engine.GetProgramRunStatus() != ProgramStatus.Success)
            {
                string status = engine.GetProgramRunStatus().ToString();
                tcTraceData.AddString("TEST_STATUS", status.Substring(0, Math.Min(14, status.Length)));
                
                string emailSubject = "SPC_Failed_" + dutObject.SerialNumber +"_" + TestTime_Start.ToString() + "_" + dutObject.EquipmentID;
                string emailBody = "SPC data is wrong. pls check and fix it as soon as possible. Thanks!";
                engine.SendEmail("LowCost_SPC", emailSubject, emailBody);
            }
            else
            {
                string status = progInfo.MainSpec.Status.Status.ToString();
                tcTraceData.AddString("TEST_STATUS", status.Substring(0, Math.Min(14, status.Length)));
            }

            this.failModeCheck.CheckMainSpec(engine, dutObject, this.MainSpec, this.CommonTestConfig.PCASFailMode_FilePath, ref tcTraceData);

            // Set trace data above here
            engine.SetTraceData(progInfo.MainSpec.Name, tcTraceData);
        }

        /// <summary>
        /// fill in empty ingore parameters
        /// you can put the parameter will be removed from new limit. jack.zhang 2012-04-19
        /// </summary>
        /// <param name="engine"></param>
        private void Data_Ignore_Write(ITestEngineDataWrite engine)
        {
            string[] ignoreParas = {
                
                //"SMSR_CH_MID",//the three SMSR have been add in limit check in IlmzChannelChar_InitModule
                //"SMSR_CH_FST",
                //"SMSR_CH_LST"
                //"FIBRE_POWER_INITIAL"
            };
            //DatumList tcTraceData = new DatumList();
            foreach (string para in ignoreParas)
            {
                foreach (ParamLimit paramLimit in progInfo.MainSpec)
                {
                    if (paramLimit.ExternalName == para)
                    {
                        if (!tcTraceData.IsPresent(paramLimit.ExternalName))
                        {
                            Datum dummyValue = null;
                            switch (paramLimit.ParamType)
                            {
                                case DatumType.Double:
                                    dummyValue = new DatumDouble(paramLimit.ExternalName, Convert.ToDouble(paramLimit.HighLimit.ValueToString()));
                                    break;
                                case DatumType.Sint32:
                                    dummyValue = new DatumSint32(paramLimit.ExternalName, Convert.ToInt32(paramLimit.HighLimit.ValueToString()));
                                    break;
                                case DatumType.StringType:
                                    dummyValue = new DatumString(paramLimit.ExternalName, "No Value");
                                    break;
                                case DatumType.Uint32:
                                    dummyValue = new DatumUint32(paramLimit.ExternalName, Convert.ToUInt32(paramLimit.HighLimit.ValueToString()));
                                    break;
                                case DatumType.FileLinkType:
                                    dummyValue = new DatumFileLink(paramLimit.ExternalName, this.Nofilepath);
                                    break;
                            }
                            if (dummyValue != null)
                                //tcTraceData.Add(dummyValue);
                                this.tcTraceData.Add(dummyValue);
                        }//if
                    }//if (paramLimit.ExternalName == para)
                }//foreach (ParamLimit paramLimit in mainSpec)
            }// foreach (string para in ignoreParas)
            //engine.SetTraceData(this.progInfo.MainSpec.Name, tcTraceData);
        }

        #endregion  End of Program
        
        #region Private Helper Functions

        /// <summary>
        /// get base line channels from base line file
        /// </summary>
        /// <param name="p"></param>
        private void GetBaseLineChannels(ITestEngineBase engine)
        {
            CsvReader reader = new CsvReader();
            List<string[]> lines = new List<string[]>();
            lines = reader.ReadFile(progInfo.baseLineFile);
            if (lines.Count < 4)
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "invalid base line data, pls confirm BaseLineTemplate.csv is correct!", FailModeCategory_Enum.SW);
            }
            baseLineChannels = new BaseLineData[lines.Count - 1];
            for (int i = 1; i < lines.Count; i++)
            {
                baseLineChannels[i - 1].Dsdbr.Setup.IPhase_mA = Double.Parse(lines[i][1]);
                baseLineChannels[i - 1].Dsdbr.Setup.IRear_mA = Double.Parse(lines[i][2]);
                baseLineChannels[i - 1].Dsdbr.Setup.IGain_mA = Double.Parse(lines[i][3]);
                baseLineChannels[i - 1].Dsdbr.Setup.ISoa_mA = Double.Parse(lines[i][4]);
                baseLineChannels[i - 1].Dsdbr.Setup.IRearSoa_mA = Double.Parse(lines[i][5]);
                baseLineChannels[i - 1].Dsdbr.Setup.FrontPair = Int32.Parse(lines[i][6]);
                baseLineChannels[i - 1].Dsdbr.Setup.IFsFirst_mA = Double.Parse(lines[i][7]);
                baseLineChannels[i - 1].Dsdbr.Setup.IFsSecond_mA = Double.Parse(lines[i][8]);
                baseLineChannels[i - 1].MzLeftArmImb_mA = Double.Parse(lines[i][16]);
                baseLineChannels[i - 1].MzRightArmImb_mA = Double.Parse(lines[i][17]);
                baseLineChannels[i - 1].vcm = Double.Parse(lines[i][18]);
                baseLineChannels[i - 1].Dsdbr.ItuFreq_GHz = double.Parse(lines[i][19]);
                //add device Dsdbr temperature and case mid temperature information. Rosa added on June 07,2011
                baseLineChannels[i - 1].Dsdbr_Temp_C = Double.Parse(lines[i][20]);
                baseLineChannels[i - 1].Case_Temp_Mid_C = Double.Parse(lines[i][21]);

            }

        }

        /// <summary>
        /// Creates an adjusted specifiation for engineering use
        /// </summary>
        /// <param name="engine">reference to test engine</param>
        /// <returns>An adjusted specification</returns>
        private Specification ConstructEngineeringSpec(ITestEngineInit engine)
        {
            engine.SendStatusMsg("Construct Engineering specification");
            GuiMsgs.TcmzEngineeringGuiResponse resp = (GuiMsgs.TcmzEngineeringGuiResponse)engine.ReceiveFromGui().Payload;
            double engAdjustment = (double)resp.FiddleFactor;

            // Calculate power difference
            double upperLimit = Convert.ToDouble(progInfo.MainSpec.GetParamLimit("CH_PEAK_POWER").HighLimit.ValueToString());
            double lowerLimit = Convert.ToDouble(progInfo.MainSpec.GetParamLimit("CH_PEAK_POWER").LowLimit.ValueToString());
            double limitTargetPower = (upperLimit - lowerLimit) / 2 + lowerLimit;
            double adjustment = limitTargetPower - engAdjustment;

            List<String> paramsToAdjust = new List<string>();
            paramsToAdjust.Add("CH_PEAK_POWER");
            paramsToAdjust.Add("CH_PWRCTRL_POWER_MAX_HIGH");
            paramsToAdjust.Add("CH_PWRCTRL_POWER_MAX_LOW");
            paramsToAdjust.Add("CH_PWRCTRL_POWER_MIN_HIGH");
            paramsToAdjust.Add("CH_PWRCTRL_POWER_MIN_LOW");
            paramsToAdjust.Add("TC_FIBRE_TARGET_POWER");
            paramsToAdjust.Add("TC_POWER_CTRL_RANGE_MAX");
            paramsToAdjust.Add("TC_POWER_CTRL_RANGE_MIN");

            Specification powerAdjustedSpec = new Specification(progInfo.MainSpec.Name, progInfo.MainSpec.ActualDatabaseKeys, progInfo.MainSpec.Priority);
            foreach (ParamLimit pl in progInfo.MainSpec)
            {
                Datum highLimit = pl.HighLimit;
                Datum lowLimit = pl.LowLimit;
                if (paramsToAdjust.Contains(pl.InternalName))
                {
                    double oldHighLimit = Convert.ToDouble(pl.HighLimit.ValueToString());
                    double trimmedHighLimit = Math.Truncate(oldHighLimit);
                    if (trimmedHighLimit != 999 && trimmedHighLimit != 9999)
                    {
                        DatumDouble newHighLimit = new DatumDouble(pl.InternalName, oldHighLimit - adjustment);
                        highLimit = (Datum)newHighLimit;
                    }

                    double oldLowLimit = Convert.ToDouble(pl.LowLimit.ValueToString());
                    double trimmedLowLimit = Math.Truncate(oldLowLimit);
                    if (trimmedLowLimit != -999 && trimmedLowLimit != -9999)
                    {
                        DatumDouble newLowLimit = new DatumDouble(pl.InternalName, oldLowLimit - adjustment);
                        lowLimit = (Datum)newLowLimit;
                    }
                }
                RawParamLimit rpl = new RawParamLimit(pl.InternalName, pl.ParamType, lowLimit, highLimit, pl.Operand, pl.Priority, pl.AccuracyFactor, pl.Units);
                powerAdjustedSpec.Add(rpl);
            }
            return powerAdjustedSpec;
        }
        
        /// <summary>
        /// Retrieves datums from config file via the ConfigAccessor.
        /// </summary>
        /// <param name="modRun">The ModuleRun to add the items to.</param>
        /// <param name="datumsRequired">The datum names in this datumlist specify the names of the datums to be retrieved and added to the ModuleRun</param>
        private void ExtractAndAddDatums(ModuleRun modRun, DatumList datumsRequired)
        {
            foreach (Datum datum in datumsRequired)
            {
                switch (datum.Type)
                {
                    case DatumType.Double:
                        {
                            modRun.ConfigData.Add(new DatumDouble(datum.Name, this.progInfo.TestParamsConfig.GetDoubleParam(datum.Name)));
                        }
                        break;

                    case DatumType.StringType:
                        {
                            modRun.ConfigData.Add(new DatumString(datum.Name, this.progInfo.TestParamsConfig.GetStringParam(datum.Name)));
                        }
                        break;

                    case DatumType.Uint32:
                        {
                            modRun.ConfigData.Add(new DatumUint32(datum.Name, this.progInfo.TestParamsConfig.GetIntParam(datum.Name)));
                        }
                        break;

                    case DatumType.Sint32:
                        {
                            modRun.ConfigData.Add(new DatumSint32(datum.Name, this.progInfo.TestParamsConfig.GetIntParam(datum.Name)));
                        }
                        break;

                    case DatumType.BoolType:
                        {
                            modRun.ConfigData.Add(new DatumBool(datum.Name, this.progInfo.TestParamsConfig.GetBoolParam(datum.Name)));
                        }
                        break;

                    default:
                        {
                            throw new ArgumentException("Invalid DatumType '" + datum.Type + "' of '" + datum.Name + "'");
                        }
                }
            }
        }
        
        /// <summary>
        /// Copy data from tcmz_map stage.
        /// </summary>
        /// <param name="mapData"></param>
        /// <param name="engine"></param>
        private void CopyDataFromMap(DatumList mapData, ITestEngineRun engine)
        {
            try
            {
                DatumList traceDataMapStage = new DatumList();

                // Feed forward data
                traceDataMapStage.Add(mapData.GetDatum("ETALON1_FS_B"));
                traceDataMapStage.Add(mapData.GetDatum("ETALON1_FS_PAIR"));
                traceDataMapStage.Add(mapData.GetDatum("ETALON1_PHASE"));
                traceDataMapStage.Add(mapData.GetDatum("ETALON1_REAR"));
                traceDataMapStage.Add(mapData.GetDatum("I_ETALON_FS_A"));
                traceDataMapStage.Add(mapData.GetDatum("I_ETALON_GAIN"));
                traceDataMapStage.Add(mapData.GetDatum("I_ETALON_SOA"));

                //traceDataMapStage.Add(mapData.GetDatum("MZ_RTH"));
                traceDataMapStage.Add(mapData.GetDatum("LASER_RTH"));
                traceDataMapStage.Add(mapData.GetDatum("ETALON1_FREQ"));

                //traceDataMapStage.Add(mapData.GetDatum("LASER_CHIP_ID"));
                //traceDataMapStage.Add(mapData.GetDatum("LASER_WAFER_ID"));
                //traceDataMapStage.Add(mapData.GetDatum("MZ_CHIP_ID"));
                //traceDataMapStage.Add(mapData.GetDatum("MZ_WAFER_ID"));
                //traceDataMapStage.Add(mapData.GetDatum("MZ_SERIAL_NO"));
                //traceDataMapStage.Add(mapData.GetDatum("LASER_WAFER_SIZE"));
                //traceDataMapStage.Add(mapData.GetDatum("CG_ID"));
                //traceDataMapStage.Add(mapData.GetDatum("MZ_WAFER_TYPE"));

                // IlmzInitialise
                traceDataMapStage.Add(mapData.GetDatum("CG_DUT_FILE_TIMESTAMP"));
                //traceDataMapStage.Add(mapData.GetDatum("REGISTRY_FILE"));

                // IlmzPowerUp
                traceDataMapStage.Add(mapData.GetDatum("ITX_DARK"));
                traceDataMapStage.Add(mapData.GetDatum("IRX_DARK"));
                traceDataMapStage.Add(mapData.GetDatum("TAP_COMP_DARK_I"));
                traceDataMapStage.Add(mapData.GetDatum("MZ_MOD_LEFT_DARK_I"));
                traceDataMapStage.Add(mapData.GetDatum("MZ_MOD_RIGHT_DARK_I"));
                //traceDataMapStage.Add(mapData.GetDatum("MZ_IMB_LEFT_DARK_I"));
                //traceDataMapStage.Add(mapData.GetDatum("MZ_IMB_RIGHT_DARK_I"));
                //traceDataMapStage.Add(mapData.GetDatum("FIBRE_POWER_INITIAL"));

                // TcmzInitMzSweep
                traceDataMapStage.Add(mapData.GetDatum("MZ_MAX_V_BIAS"));
                //traceDataMapStage.Add(mapData.GetDatum("REF_LV_FILE"));
                traceDataMapStage.Add(mapData.GetDatum("REF_MZ_VON_LEFT"));
                traceDataMapStage.Add(mapData.GetDatum("REF_MZ_VOFF_LEFT"));
                traceDataMapStage.Add(mapData.GetDatum("REF_MZ_VON_RIGHT"));
                traceDataMapStage.Add(mapData.GetDatum("REF_MZ_VOFF_RIGHT"));
                traceDataMapStage.Add(mapData.GetDatum("REF_FIBRE_POWER"));
                traceDataMapStage.Add(mapData.GetDatum("REF_MZ_IMB_LEFT"));
                traceDataMapStage.Add(mapData.GetDatum("REF_MZ_IMB_RIGHT"));
                //traceDataMapStage.Add(mapData.GetDatum("REF_MZ_LEFT"));
                //traceDataMapStage.Add(mapData.GetDatum("REF_MZ_RIGHT"));
                traceDataMapStage.Add(mapData.GetDatum("REF_MZ_SUM"));
                traceDataMapStage.Add(mapData.GetDatum("REF_MZ_RATIO"));
                traceDataMapStage.Add(mapData.GetDatum("REF_FREQ"));

                //CloseGridOverallMap Module
                traceDataMapStage.Add(mapData.GetDatum("NUM_SM"));
                traceDataMapStage.Add(mapData.GetDatum("CG_SM_LINES_FILE"));
                traceDataMapStage.Add(mapData.GetDatum("CG_PRATIO_WL_FILE"));
                traceDataMapStage.Add(mapData.GetDatum("CG_QAMETRICS_FILE"));
                traceDataMapStage.Add(mapData.GetDatum("MATRIX_PRATIO_SUMMARY_MAP"));

                //IlmzPowerHeadCal Module
                //CloseGridSupermode 0 => n Module
                //for (int i = 0; i < 8; i++)
                //{
                //    traceDataMapStage.Add(mapData.GetDatum("MODAL_DISTORT_SM" + i.ToString()));
                //    traceDataMapStage.Add(mapData.GetDatum("MILD_MULTIMODE_SM" + i.ToString()));
                //}

                //CloseGridCharacterise Module

                //traceDataMapStage.Add(mapData.GetDatum("CG_ITUEST_POINTS_FILE"));
                traceDataMapStage.Add(mapData.GetDatum("ITU_OP_EST_COUNT"));
                traceDataMapStage.Add(mapData.GetDatum("CHAR_ITU_OP_COUNT"));
                traceDataMapStage.Add(mapData.GetDatum("CG_CHAR_ITU_FILE"));
                //traceDataMapStage.Add(mapData.GetDatum("NUM_MISSING_EST_CHAN"));


                // Add by Alice    2010-03-11
                //traceDataMapStage.Add(mapData.GetDatum("TC_NUM_CHAN_REQUIRED"));
                //traceDataMapStage.Add(mapData.GetDatum("TC_OPTICAL_FREQ_START"));
                //traceDataMapStage.Add(mapData.GetDatum("SECTION_IVTEST_RESULTS_FILE"));


                // Set trace data
                progInfo.MainSpec.SetTraceData(traceDataMapStage);
            }
            catch (Exception e)
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "Copy map stage data error!" + e.Message, FailModeCategory_Enum.SW);
            }
        }

        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Bookham.TestSolution.TestPrograms
{
    internal partial class PowerCalDataCtrl : UserControl
    {
        public PowerCalDataCtrl(Prog_ILMZ_Final_SPCGui parent)
        {
            this.parent = parent;
            InitializeComponent();
        }

        private Prog_ILMZ_Final_SPCGui parent;

        private void buttonFindFile_Click(object sender, EventArgs e)
        {
            DialogResult res = openFileDialog1.ShowDialog(this);
            if (res == DialogResult.OK)
            {
                textBoxTcmzSettingsFile.Text = openFileDialog1.FileName;
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            GuiMsgs.TcmzPowerCalDataResponse resp = new GuiMsgs.TcmzPowerCalDataResponse(textBoxTcmzSettingsFile.Text);
            parent.SendToWorker(resp);
            parent.CtrlFinished();
        }       

    }
}

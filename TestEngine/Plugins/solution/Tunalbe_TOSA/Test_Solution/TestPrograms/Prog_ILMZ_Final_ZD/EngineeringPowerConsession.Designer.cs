namespace Bookham.TestSolution.TestPrograms
{
    partial class EngineeringPowerConsession
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.buttonOk = new System.Windows.Forms.Button();
            this.comboPower = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label1.Location = new System.Drawing.Point(26, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(170, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Reduced power test";
            // 
            // buttonOk
            // 
            this.buttonOk.Location = new System.Drawing.Point(314, 79);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(52, 25);
            this.buttonOk.TabIndex = 1;
            this.buttonOk.Text = "Ok";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // comboPower
            // 
            this.comboPower.FormattingEnabled = true;
            this.comboPower.Items.AddRange(new object[] {
            "5.0 dBm",
            "4.0 dBm",
            "3.0 dBm",
            "2.0 dBm",
            "1.0 dBm",
            "0 dBm",
            "-1.0 dBm",
            "-2.0 dBm",
            "You must be joking !"});
            this.comboPower.Location = new System.Drawing.Point(30, 79);
            this.comboPower.Name = "comboPower";
            this.comboPower.Size = new System.Drawing.Size(106, 21);
            this.comboPower.TabIndex = 3;
            this.comboPower.Text = "6.0 dBm";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(27, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(218, 16);
            this.label2.TabIndex = 4;
            this.label2.Text = "Please select the new target power.";
            // 
            // EngineeringPowerConsession
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboPower);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.label1);
            this.Name = "EngineeringPowerConsession";
            this.Size = new System.Drawing.Size(427, 140);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.ComboBox comboPower;
        private System.Windows.Forms.Label label2;
    }
}

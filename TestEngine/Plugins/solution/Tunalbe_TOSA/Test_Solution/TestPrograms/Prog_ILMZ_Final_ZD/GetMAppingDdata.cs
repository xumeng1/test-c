using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.IO;
using System.Windows.Forms;
using Bookham.TestEngine.Framework.InternalData;

namespace Bookham.TestSolution.TestPrograms
{
    public partial class GetMAppingDdata : UserControl
    {
        private Prog_ILMZ_FinalGui parent;

        public GetMAppingDdata(Prog_ILMZ_FinalGui parent)
        {
            this.parent = parent;
            InitializeComponent();

            if (openFileDialog1.FileName.Length > 0)
            {
                string directoryName = Path.GetDirectoryName(openFileDialog1.FileName);
                if (directoryName.Length > 0)
                {
                    string[] ituFiles = Directory.GetFiles(directoryName, "ITUOperatingPoints*.csv");
                    if (ituFiles.Length > 0)
                        txtItuFileName.Text = ituFiles[0];
                }
            }

        }

        private void btnGetItuFile_Click(object sender, EventArgs e)
        {
            //openFileDialog1.Filter = "|.csv";
            openFileDialog1.Title = "Get Itu File";
            DialogResult res = openFileDialog1.ShowDialog(this);
            if (res == DialogResult.OK)
            {
                txtItuFileName.Text  = openFileDialog1.FileName;
            }


            if (openFileDialog1.FileName.Length > 0)
            {
                string directoryName = Path.GetDirectoryName(openFileDialog1.FileName);
                string[] zipFiles = Directory.GetFiles(directoryName, "*.zip");
                if (zipFiles.Length > 0)
                    txtPhaseFile.Text = zipFiles[0];

                string[] mzTuningFiles = Directory.GetFiles(directoryName, "*MZTuning*");
                if (mzTuningFiles.Length > 0)
                {
                    using (StreamReader reader = new StreamReader(mzTuningFiles[0]))
                    {
                        while (!reader.EndOfStream)
                        {
                            string line = reader.ReadLine();
                            if (line.Contains("Max left imbalance"))
                            {
                                string[] tokens = line.Split(':');
                                if (tokens.Length > 0 && tokens[1].Length > 4)
                                {
                                    string current = tokens[1].Substring(0, 4);
                                    txtImbLeft.Text = current;
                                }
                            }
                            if (line.Contains("Max right imbalance"))
                            {
                                string[] tokens = line.Split(':');
                                if (tokens.Length > 0 && tokens[1].Length > 4)
                                {
                                    string current = tokens[1].Substring(0, 4);
                                    txtImbRight.Text = current;
                                }
                            }
                        }
                    }
                }
            }
        }

        //private void btnGetIVFile_Click(object sender, EventArgs e)
        //{
        //    openFileDialog1.Title = "Get IV Sweep File";
        //    //openFileDialog1.Filter = "|.csv";
        //    DialogResult res = openFileDialog1.ShowDialog(this);
        //    if (res == DialogResult.OK)
        //    {
        //        txtIVFile .Text = openFileDialog1.FileName;
        //    }
        //}

        private void btnSet_Click(object sender, EventArgs e)
        {
            int channels = int.Parse(txtChannels.Text);
            if (channels == 0)
            {
                MessageBox.Show(" Please re-entry the totals channels count", "Total channels can't be 0");
                return;
            }
            int smNumber = int.Parse(txsmNumber .Text .Trim ());
            if (smNumber <6)
            {
                MessageBox.Show(" Please re-entry the totals SM count", "Total supper mode numbers should be more than 6");
                return;
            }
            double startFreq = double.Parse(cbStartFreq.Text);
            if (startFreq < 191500)
            {
#warning - Mark - edited this 
                //MessageBox.Show("Please re-entry the start frequency ", "Frequency should start greater than 191500");
                //return;
            }
            string ituFile = txtItuFileName.Text.Trim();
            if (!File.Exists(ituFile ))
            {
                MessageBox.Show("ITU file doesn't exist at " + ituFile + " ,Plese re entry the CG Itu file", " Can't find file");
                return;
            }

            //string ivFile = txtIVFile.Text;
            //if (!File.Exists(ivFile))
            //{
            //    MessageBox.Show("IV Sweep file doesn't exist at " + ivFile + " ,Plese re entry the Section IV Sweep file", " Can't find file");
            //    return;
            //}
            string phaseFile = txtPhaseFile.Text;
            if (!File.Exists(phaseFile))
            {
                MessageBox.Show("Phase Mode Acq File doesn't exist at " + phaseFile + " ,Plese re entry the phase mode acq file", " Can't find file");
                return;
            }
            double vLeft = Convert.ToDouble(txtVleft.Text);
            double vRight = Convert.ToDouble(txtVright.Text);
            double iLeft = Convert.ToDouble(txtImbLeft.Text);
            double iRight = Convert.ToDouble(txtImbRight.Text);
                                  
            DatumList mapResult = new DatumList();
            mapResult .Add (new DatumString ("TEST_STATUS","Pass"));
            mapResult.Add ( new DatumSint32 ("TC_NUM_CHAN_REQUIRED",channels ));
            mapResult .Add (new DatumDouble("TC_OPTICAL_FREQ_START",startFreq) );
            mapResult.Add(new DatumFileLink("CG_CHAR_ITU_FILE", ituFile));
            //mapResult.Add(new DatumFileLink("SECTION_IVTEST_RESULTS_FILE", ivFile));
            mapResult .Add (new DatumSint32("NUM_SM",smNumber));
            mapResult.Add(new DatumFileLink("CG_PHASE_MODE_ACQ_FILE", phaseFile));
            mapResult.Add(new DatumDouble("REF_MZ_VOFF_LEFT", 0));//Jack.Zhang
            mapResult.Add(new DatumDouble("REF_MZ_VOFF_RIGHT", 0));
            mapResult.Add(new DatumDouble("REF_MZ_VON_LEFT", vLeft ));
            mapResult.Add(new DatumDouble("REF_MZ_VON_RIGHT",vRight));
            mapResult.Add(new DatumDouble("REF_MZ_IMB_LEFT", iLeft));
            mapResult.Add(new DatumDouble("REF_MZ_IMB_RIGHT", iRight));
            //mapResult.Add(new DatumStringArray("SM_FILES", smFiles));

            GuiMsgs.tosaMappingResponse rsp = new GuiMsgs.tosaMappingResponse(mapResult);

            parent.SendToWorker(rsp);

            parent.CtrlFinished();
        }

        //private void btnGetSMFile_Click(object sender, EventArgs e)
        //{
        //    openFileDialog1.Multiselect = true;
        //    openFileDialog1 .Title = "Get Phase Mode ACQ File";
        //    DialogResult res = openFileDialog1.ShowDialog(this);
        //    if (res == DialogResult.OK)
        //    {
        //        cbSmFiles.Items.Clear();
        //        string[] files = openFileDialog1.FileNames;
                
        //        string filelist = "";
        //        foreach (string str in files)
        //        {
        //            filelist += str;
        //            filelist  += "\n";
        //            cbSmFiles.Items.Add(str);
                  
        //        }
        //        filelist.TrimEnd('\n');
        //        txtVright.Text = filelist;
        //        txtVright.Refresh();
        //        cbSmFiles.Refresh();
        //    }
        //    openFileDialog1.Multiselect = false;
        //}

        private void btnGetPhaseFile_Click(object sender, EventArgs e)
        {


            DialogResult res = openFileDialog1.ShowDialog(this);
            if (res == DialogResult.OK)
            {
                txtPhaseFile .Text  = openFileDialog1.FileName;
                txtPhaseFile.Refresh();
            }
        }

        

       

        
        


    }
}

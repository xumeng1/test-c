namespace Bookham.TestSolution.TestPrograms
{
    partial class GetMAppingDdata
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtItuFileName = new System.Windows.Forms.TextBox();
            this.cbStartFreq = new System.Windows.Forms.ComboBox();
            this.txtChannels = new System.Windows.Forms.TextBox();
            this.btnGetItuFile = new System.Windows.Forms.Button();
            this.btnSet = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.txsmNumber = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtPhaseFile = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtVright = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtImbLeft = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtImbRight = new System.Windows.Forms.TextBox();
            this.txtVleft = new System.Windows.Forms.TextBox();
            this.btnGetPhaseFile = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(43, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Channels";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(43, 154);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(166, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Start Frequency(GHz)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(43, 211);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(122, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "ITU Channel file";
            // 
            // txtItuFileName
            // 
            this.txtItuFileName.Location = new System.Drawing.Point(209, 211);
            this.txtItuFileName.Name = "txtItuFileName";
            this.txtItuFileName.Size = new System.Drawing.Size(291, 20);
            this.txtItuFileName.TabIndex = 4;
            // 
            // cbStartFreq
            // 
            this.cbStartFreq.FormattingEnabled = true;
            this.cbStartFreq.Items.AddRange(new object[] {
            "191150",
            "191350",
            "191500",
            "191600",
            "191700",
            "191800",
            "192000"});
            this.cbStartFreq.Location = new System.Drawing.Point(209, 153);
            this.cbStartFreq.Name = "cbStartFreq";
            this.cbStartFreq.Size = new System.Drawing.Size(162, 21);
            this.cbStartFreq.Sorted = true;
            this.cbStartFreq.TabIndex = 6;
            this.cbStartFreq.Text = "191150";
            // 
            // txtChannels
            // 
            this.txtChannels.Location = new System.Drawing.Point(187, 28);
            this.txtChannels.Name = "txtChannels";
            this.txtChannels.Size = new System.Drawing.Size(59, 20);
            this.txtChannels.TabIndex = 7;
            this.txtChannels.Text = "96";
            // 
            // btnGetItuFile
            // 
            this.btnGetItuFile.Location = new System.Drawing.Point(506, 209);
            this.btnGetItuFile.Name = "btnGetItuFile";
            this.btnGetItuFile.Size = new System.Drawing.Size(51, 22);
            this.btnGetItuFile.TabIndex = 9;
            this.btnGetItuFile.Text = "Browse";
            this.btnGetItuFile.UseVisualStyleBackColor = true;
            this.btnGetItuFile.Click += new System.EventHandler(this.btnGetItuFile_Click);
            // 
            // btnSet
            // 
            this.btnSet.Location = new System.Drawing.Point(576, 396);
            this.btnSet.Name = "btnSet";
            this.btnSet.Size = new System.Drawing.Size(95, 38);
            this.btnSet.TabIndex = 10;
            this.btnSet.Text = "Set";
            this.btnSet.UseVisualStyleBackColor = true;
            this.btnSet.Click += new System.EventHandler(this.btnSet_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.RestoreDirectory = true;
            // 
            // txsmNumber
            // 
            this.txsmNumber.Location = new System.Drawing.Point(187, 83);
            this.txsmNumber.Name = "txsmNumber";
            this.txsmNumber.Size = new System.Drawing.Size(59, 20);
            this.txsmNumber.TabIndex = 12;
            this.txsmNumber.Text = "7";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(43, 83);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(93, 20);
            this.label5.TabIndex = 11;
            this.label5.Text = "SM Number";
            // 
            // txtPhaseFile
            // 
            this.txtPhaseFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhaseFile.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtPhaseFile.Location = new System.Drawing.Point(209, 263);
            this.txtPhaseFile.Name = "txtPhaseFile";
            this.txtPhaseFile.Size = new System.Drawing.Size(291, 22);
            this.txtPhaseFile.TabIndex = 18;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(21, 33);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(126, 20);
            this.label7.TabIndex = 17;
            this.label7.Text = "Von_LeftMod_V";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(266, 33);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(136, 20);
            this.label6.TabIndex = 13;
            this.label6.Text = "Von_RightMod_V";
            // 
            // txtVright
            // 
            this.txtVright.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVright.ForeColor = System.Drawing.Color.Black;
            this.txtVright.Location = new System.Drawing.Point(408, 33);
            this.txtVright.Multiline = true;
            this.txtVright.Name = "txtVright";
            this.txtVright.Size = new System.Drawing.Size(79, 20);
            this.txtVright.TabIndex = 14;
            this.txtVright.Text = "0";
            this.txtVright.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.txtImbLeft);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.txtImbRight);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txtVleft);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtVright);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(47, 316);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(510, 131);
            this.groupBox1.TabIndex = 19;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "MZ Settings";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(21, 80);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(129, 20);
            this.label8.TabIndex = 24;
            this.label8.Text = "Ion_LeftImb_mA";
            // 
            // txtImbLeft
            // 
            this.txtImbLeft.Location = new System.Drawing.Point(140, 80);
            this.txtImbLeft.Name = "txtImbLeft";
            this.txtImbLeft.Size = new System.Drawing.Size(79, 22);
            this.txtImbLeft.TabIndex = 25;
            this.txtImbLeft.Text = "0";
            this.txtImbLeft.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(266, 80);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(139, 20);
            this.label9.TabIndex = 22;
            this.label9.Text = "Ion_RightImb_mA";
            // 
            // txtImbRight
            // 
            this.txtImbRight.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtImbRight.ForeColor = System.Drawing.Color.Black;
            this.txtImbRight.Location = new System.Drawing.Point(408, 80);
            this.txtImbRight.Multiline = true;
            this.txtImbRight.Name = "txtImbRight";
            this.txtImbRight.Size = new System.Drawing.Size(79, 20);
            this.txtImbRight.TabIndex = 23;
            this.txtImbRight.Text = "0";
            this.txtImbRight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtVleft
            // 
            this.txtVleft.Location = new System.Drawing.Point(140, 33);
            this.txtVleft.Name = "txtVleft";
            this.txtVleft.Size = new System.Drawing.Size(79, 22);
            this.txtVleft.TabIndex = 21;
            this.txtVleft.Text = "0";
            this.txtVleft.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btnGetPhaseFile
            // 
            this.btnGetPhaseFile.Location = new System.Drawing.Point(506, 263);
            this.btnGetPhaseFile.Name = "btnGetPhaseFile";
            this.btnGetPhaseFile.Size = new System.Drawing.Size(51, 22);
            this.btnGetPhaseFile.TabIndex = 22;
            this.btnGetPhaseFile.Text = "Browse";
            this.btnGetPhaseFile.UseVisualStyleBackColor = true;
            this.btnGetPhaseFile.Click += new System.EventHandler(this.btnGetPhaseFile_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(43, 265);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(159, 20);
            this.label4.TabIndex = 20;
            this.label4.Text = "Phase Mode Acq File";
            // 
            // GetMAppingDdata
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnGetPhaseFile);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtPhaseFile);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txsmNumber);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnSet);
            this.Controls.Add(this.btnGetItuFile);
            this.Controls.Add(this.txtChannels);
            this.Controls.Add(this.cbStartFreq);
            this.Controls.Add(this.txtItuFileName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "GetMAppingDdata";
            this.Size = new System.Drawing.Size(715, 458);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtItuFileName;
        private System.Windows.Forms.ComboBox cbStartFreq;
        private System.Windows.Forms.TextBox txtChannels;
        private System.Windows.Forms.Button btnGetItuFile;
        private System.Windows.Forms.Button btnSet;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TextBox txsmNumber;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtPhaseFile;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtVright;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnGetPhaseFile;
        private System.Windows.Forms.TextBox txtVleft;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtImbLeft;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtImbRight;
    }
}

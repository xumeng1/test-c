// [Copyright]
//
// Bookham Test Engine
// MyTestProgram1
//
// MyTestProgram1/ProgramGui
// 
// Author: paul.annetts
// Design: TODO

namespace Bookham.TestSolution.TestPrograms
{
    partial class Prog_PrePhaseCutScanGui
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textLable = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textLable
            // 
            this.textLable.AutoSize = true;
            this.textLable.Location = new System.Drawing.Point(128, 90);
            this.textLable.Name = "textLable";
            this.textLable.Size = new System.Drawing.Size(0, 13);
            this.textLable.TabIndex = 0;
            // 
            // Prog_FcuMapGui
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.textLable);
            this.Name = "Prog_FcuMapGui";
            this.MsgReceived += new Bookham.TestEngine.Framework.Messages.ManagedCtlBase.MsgReceivedDlg(this.ProgramGui_MsgReceived);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label textLable;
    }
}

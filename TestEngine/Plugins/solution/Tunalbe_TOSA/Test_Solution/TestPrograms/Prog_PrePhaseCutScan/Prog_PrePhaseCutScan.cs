﻿// [Copyright]
// Bookham [Full Project Name]
// Bookham.TestSolution.TestPrograms
// Prog_PrePhaseCutScan.cs
// Author: Echoxl.wang,2011
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.Specialized;
using System.Data;
using System.IO;
using System.IO.Ports;
using System.Windows.Forms;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.ExternalData;
using Bookham.TestEngine.Framework.Limits;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.Solution.Config;
using Bookham.TestLibrary.Instruments;
using Bookham.TestSolution.Instruments;
using Bookham.TestLibrary.InstrTypes;
using Bookham.fcumapping.CommonData;
//using Bookham.TestLibrary.FcuCommonUtils;
using Bookham.TestSolution.IlmzCommonInstrs;
using Bookham.TestSolution.IlmzCommonUtils;
using Bookham.TestLibrary.Utilities;
using Bookham.ToolKit.Mz;
using Bookham.Program.Core;
using Bookham.TestSolution.IlmzCommonData;

namespace Bookham.TestSolution.TestPrograms
{
    public class Prog_PrePhaseCutScan : TestProgramCore, ITestProgram
    {
        #region Private data
        private DatumList traceDataList = null;
        private List<string> mzSweepFilesToZip = new List<string>();

        private const string PhaseScanSettingsPath = "Configuration/TOSA Final/PhaseScan/PhaseScanTestConfig.xml";
        private const string LocalSettingsPath = @"Configuration\TOSA Final\LocalTestParams.xml";
        private const string FinalSettingsPath = "Configuration/TOSA Final/FinalTest/CommonTestParams.xml";
        private const string OpticalSwitchSettingsPath = "Configuration/TOSA Final/IlmzOpticalSwitch.xml";
        private const string MapParamsConfigSettingsPath = "Configuration/TOSA Final/MapTest/MappingTestConfig.xml";
        private const string ASICTypeCollocationPath = @"Configuration\TOSA Final\ASICTypeCollocation.csv";
        private string PhaseCurrentSetupFile = "";
        private string RearCurrentSetupFile = "";
        private string FSBCurrentSetupFile = "";
        private string serialNum;
        private string fail_Abort_Reason;
        private double TC_LASER_TEMP_C = 0;
        private double TC_CASE_MID_C = 0;
        private double RTH_Offset_ohm = 0;
        private double imb_Left_mA_for_Ith = 0.0;
        private double imb_Right_mA_for_Ith = 0.0;
        private double imb_Left_mA_for_IScan = 0.0;
        private double imb_Right_mA_for_IScan = 0.0;
        private Dictionary<string, Specification> SpecDict = new Dictionary<string, Specification>();

        private TestParamConfigAccessor ProgramCfgReader;
        private TestParamConfigAccessor LocalCfgReader;
        private TestParamConfigAccessor TecCaseCfgReader;
        private TestParamConfigAccessor TecDSDBRCfgReader;
        private TestParamConfigAccessor ElecCfgReader;
        private TestParamConfigAccessor FinalCfgReader;
        private TestParamConfigAccessor OpticalSwitchConfig;
        private TestParamConfigAccessor MapParamsConfig;
        private TempTableConfigAccessor TempConfig;
        private TestParamConfigAccessor LightTowerConfig;

        Inst_Ke2510 TecDsdbr;
        //Inst_Ke2510 TecCase;
        //Inst_Nt10a TecCase;
        IInstType_TecController TecCase;
        //Inst_Ke24xx fcuSource;
        //Inst_FCU Fcu2Asic;
        Inst_Fcu2Asic Fcu2Asic;
        //Inst_E36xxA VcckeSource ;
        //Inst_E36xxA VeekeSource ;
        IInstType_ElectricalSource ASIC_VccSource;
        IInstType_ElectricalSource ASIC_VeeSource;
        InstType_ElectricalSource FCUMKI_Source;
        //Inst_Ke24xx CtapSource;
        //Inst_Ke24xx Leftmode ;
        //Inst_Ke24xx Rightmod;
        ITestEngineInit testEngineInit;

        float cut_scan_constant = 0;
        float cut_scan_nonConstant = 0;
        float cut_scan_Iphase_mA = 0;
        float cut_scan_Irear_mA = 0;
        int cut_scan_fp = 3;
        int numberPointSweep = 0;
        IlMzInstruments MzInstr = new IlMzInstruments();
        string MainSpecName = "";
        List<int> smIndexes = null;
        List<ModuleRunReturn> smPhaseScanResults;
        bool isCanScanForRipple = false;

        double[] phaseScanPoints;
        double[] rearScanPoints;

        float I_ETALON_FS_A;
        float I_ETALON_FS_B;
        float I_ETALON_GAIN;
        float I_ETALON_PHASE;
        float I_ETALON_REAR;
        float I_ETALON_REAR_SOA;
        float I_ETALON_SOA;
        int ETALON_FS_PAIR;
        int LM_MID_LINE_INTER;
        double LM_MID_LINE_SLOPE;
        List<string[]> LM_MID_LINE_SWEEP_FILE = null;

        #endregion

        #region for Light Tower.

        private SerialPort _serialPort;
        /// <summary>
        /// Serial port to control the lighter to indicate test messeage
        /// </summary>
        public SerialPort SerialPort
        {
            get { return _serialPort; }
        }

        /// <summary>
        /// Construct a serial port object from parts of the resource string.
        /// </summary>
        /// <param name="comPort">COMx port portion of resource string.</param>
        /// <param name="rate">bps rate portion of resource string.</param>
        /// <returns>Constructed SerialPort handler instance.</returns>
        private SerialPort buildSerPort(string comPort, string rate)
        {
            return new System.IO.Ports.SerialPort(
                "COM" + comPort,                                /* COMn */
                int.Parse(rate),                                /* bps */
                System.IO.Ports.Parity.None, 8, StopBits.One);  /* 8N1 */
        }
        #endregion

        public override Type UserControl
        {
            get { return typeof(Prog_PrePhaseCutScanGui); }
        }

        #region Implement InitCode method

        protected override void InitConfig(ITestEngineInit engine, DUTObject dutObject)
        {
            base.InitConfig(engine, dutObject);

            testEngineInit = engine;
            serialNum = dutObject.SerialNumber;

            ProgramCfgReader = new TestParamConfigAccessor(dutObject, PhaseScanSettingsPath, "", "PhaseScanTestParams");
            LocalCfgReader = new TestParamConfigAccessor(dutObject, LocalSettingsPath, "", "TestParams");
            TecCaseCfgReader = new TestParamConfigAccessor(dutObject, FinalSettingsPath, "TecCase", "TestParams");
            TecDSDBRCfgReader = new TestParamConfigAccessor(dutObject, FinalSettingsPath, "TecDsdbr", "TestParams");
            ElecCfgReader = new TestParamConfigAccessor(dutObject, PhaseScanSettingsPath, "CommonElec", "PhaseScanTestParams");
            FinalCfgReader = new TestParamConfigAccessor(dutObject, FinalSettingsPath, "", "TestParams");
            TempConfig = new TempTableConfigAccessor(dutObject, 1, @"Configuration\TOSA Final\TempTable.xml");
            OpticalSwitchConfig = new TestParamConfigAccessor(dutObject, OpticalSwitchSettingsPath, "", "IlmzOpticalSwitchParams");
            MapParamsConfig = new TestParamConfigAccessor(dutObject, MapParamsConfigSettingsPath, "", "MappingTestParams");
            LightTowerConfig = new TestParamConfigAccessor(dutObject, @"Configuration\TOSA Final\LightTowerParams.xml", "", "LightTowerParams");

            PhaseCurrentSetupFile = ProgramCfgReader.GetStringParam("PhaseCurrentFile");
            RearCurrentSetupFile = ProgramCfgReader.GetStringParam("RearCurrentFile");
            FSBCurrentSetupFile = ProgramCfgReader.GetStringParam("FSBCurrentFile");
            numberPointSweep = ProgramCfgReader.GetIntParam("PointNum_MZSweep");

            this.phaseScanPoints = this.ReadScanPointsFile(engine, this.PhaseCurrentSetupFile);
            this.rearScanPoints = this.ReadScanPointsFile(engine, this.RearCurrentSetupFile);
        }

        /// <summary>
        /// Rear ft, pt, or rt files content - chongjian.liang 2016.11.04
        /// </summary>
        /// <param name="filePath"></param>
        private double[] ReadScanPointsFile(ITestEngineInit engine, string filePath)
        {
            if (string.IsNullOrEmpty(filePath))
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "ScanPointsFile path is empty", FailModeCategory_Enum.SW);
            }
            else if (!File.Exists(filePath))
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, string.Format("{0} does not exist.", filePath), FailModeCategory_Enum.SW);
            }

            using (CsvReader csvReader = new CsvReader())
            {
                List<string[]> fileContext = csvReader.ReadFile(filePath);
                {
                    if (fileContext == null)
                    {
                        this.failModeCheck.RaiseError_Prog_NonParamFail(engine, string.Format("Empty content in file {0}.", filePath), FailModeCategory_Enum.SW);
                    }
                }

                List<double> fileValues = new List<double>();

                foreach (string[] line in fileContext)
                {
                    if (line.Length < 1)
                    {
                        this.failModeCheck.RaiseError_Prog_NonParamFail(engine, string.Format("Line {0} is empty in file {1}.", fileContext.IndexOf(line), filePath), FailModeCategory_Enum.SW);
                    }

                    double valueOfLine;
                    {
                        if (!double.TryParse(line[0], out valueOfLine))
                        {
                            this.failModeCheck.RaiseError_Prog_NonParamFail(engine, string.Format("Illegal format of line {0} in file {1}.", fileContext.IndexOf(line), filePath), FailModeCategory_Enum.SW);
                        }
                    }

                    fileValues.Add(valueOfLine);
                }

                return fileValues.ToArray();
            }
        }

        protected override void LoadSpecs(ITestEngineInit engine, DUTObject dut)
        {
            bool ReadFromPcas = ProgramCfgReader.GetBoolParam("ReadFromPcas");

            StringDictionary mainSpecKeys = new StringDictionary();
            SpecList tempSpecList = null;
            ILimitRead limitReader;
            SpecList specList = new SpecList();

            if (ReadFromPcas)
            {
                limitReader = engine.GetLimitReader();
            }
            else //read limit from local file
            {
                // initialise limit file reader
                limitReader = engine.GetLimitReader("PCAS_FILE");
                // main specification
                string aa = dut.TestStage.ToLower();
                string final_SpecNameFile = ProgramCfgReader.GetStringParam(dut.TestStage.ToLower() + "_PcasLimitFileName");
                string finalSpecFullFilename = ProgramCfgReader.GetStringParam("PcasLimitFileDirectory")
                    + @"\" + final_SpecNameFile;
                mainSpecKeys.Add("Filename", finalSpecFullFilename);
            }
            mainSpecKeys.Add("SCHEMA", "HIBERDB");
            mainSpecKeys.Add("TEST_STAGE", dut.TestStage.ToLower());
            mainSpecKeys.Add("DEVICE_TYPE", dut.PartCode);
            tempSpecList = limitReader.GetLimit(mainSpecKeys);
            this.MainSpec = tempSpecList[0];
            engine.SetSpecificationList(tempSpecList);
            this.MainSpecName = this.MainSpec.Name;

            GetTestCondition();
            if (this.TC_LASER_TEMP_C == 0)//temp for add DSDBR_temp in limit file jack.zhang 2012-04-19
                this.TC_LASER_TEMP_C = FinalCfgReader.GetDoubleParam("TecDsdbr_SensorTemperatureSetPoint_C");
            if (this.TC_CASE_MID_C == 0)//temp for add DSDBR_temp in limit file jack.zhang 2012-04-19
                this.TC_CASE_MID_C = FinalCfgReader.GetDoubleParam("TecCase_SensorTemperatureSetPoint_C");
            
            base.LoadSpecs(engine, dut);
        }

        /// <summary>
        /// Validate CHIP_ID & WAFER_ID - chongjian.liang 2016.08.17
        /// </summary>
        protected override void ValidatePackageBuildStageData(ITestEngineInit engine, DUTObject dutObject)
        {
            base.ValidatePackageBuildStageData(engine, dutObject);

            // CHIP_ID
            {
                int CHIP_ID_LENGTH = 4;

                if (ParamManager.Conditions.CHIP_ID.Length != CHIP_ID_LENGTH)
                {
                    this.failModeCheck.RaiseError_Prog_NonParamFail(engine, string.Format("The length of CHIP_ID from package build stage does not equal to {0}.", CHIP_ID_LENGTH), FailModeCategory_Enum.OP);
                }
            }

            // WAFER_ID
            {
                if (!ParamManager.Conditions.WAFER_ID.Contains("."))
                {
                    this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "Missing '.' in WAFER_ID from package build stage", FailModeCategory_Enum.OP);
                }

                string[] segmentsOf_WAFER_ID = ParamManager.Conditions.WAFER_ID.Split('.');

                int WAFER_ID_LENGTH_SEG1_CASE1 = 8;
                int WAFER_ID_LENGTH_SEG1_CASE2 = 9;
                int WAFER_ID_LENGTH_SEG2 = 1;

                if (segmentsOf_WAFER_ID.Length != 2
                    || (segmentsOf_WAFER_ID[0].Length != WAFER_ID_LENGTH_SEG1_CASE1 && segmentsOf_WAFER_ID[0].Length != WAFER_ID_LENGTH_SEG1_CASE2)
                    || segmentsOf_WAFER_ID[1].Length != WAFER_ID_LENGTH_SEG2)
                {
                    this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "The format of WAFER_ID from package build stage is not correct.", FailModeCategory_Enum.OP);
                }
            }
        }

        /// <summary>
        /// Validate previous test stage's status - chongjian.liang 2016.08.03
        /// </summary>
        protected override void ValidatePreACRStageData(ITestEngineInit engine, DUTObject dutObject)
        {
            this.ValidatePreviousTestStage(engine, dutObject, "pre_ac_r");
        }

        /// <summary>
        /// Validate previous test stage's status - chongjian.liang 2016.08.03
        /// </summary>
        protected override void ValidateLensAlignStageData(ITestEngineInit engine, DUTObject dutObject)
        {
            this.ValidatePreviousTestStage(engine, dutObject, "lens align");
        }

        /// <summary>
        /// Validate previous test stage's status - chongjian.liang 2016.08.03
        /// </summary>
        protected override void ValidateLensPostBakeTestStageData(ITestEngineInit engine, DUTObject dutObject)
        {
            this.ValidatePreviousTestStage(engine, dutObject, "lens post bake test");
        }

        /// <summary>
        /// Validate previous test stage's status - chongjian.liang 2016.08.03
        /// </summary>
        protected override void ValidateReceptacleAlignStageData(ITestEngineInit engine, DUTObject dutObject)
        {
            this.ValidatePreviousTestStage(engine, dutObject, "receptacle align");
        }

        /// <summary>
        /// Validate previous test stage's status - chongjian.liang 2016.08.03
        /// </summary>
        protected override void ValidatePostTempCycleStageData(ITestEngineInit engine, DUTObject dutObject)
        {
            this.ValidatePreviousTestStage(engine, dutObject, "post temp cycle");
        }

        protected override void ValidateBurnInStageData(ITestEngineInit engine, DUTObject dutObject)
        {
            IDataRead dataRead = engine.GetDataReader("PCAS_SHENZHEN");
            DatumList BIData = null;

            StringDictionary BIKeys = new StringDictionary();
            BIKeys.Add("SCHEMA", "hiberdb");
            BIKeys.Add("TEST_STAGE", "burn in");
            BIKeys.Add("SERIAL_NO", dutObject.SerialNumber);
            BIData = dataRead.GetLatestResults(BIKeys, false);
            try
            {
                if (BIData != null
                    && BIData.Count > 0)
                {
                    string testStatus = BIData.ReadString("TEST_STATUS");
                    if (!testStatus.ToLower().Contains("pass"))
                    {
                        this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "This DUT has been failed at burn in, Terminate testing!", FailModeCategory_Enum.OP);
                    }
                    else
                    {
                        this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "This DUT has pass burn in, can not retest pre again!", FailModeCategory_Enum.OP);
                    }

                }

            }
            catch (Exception e)
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, e.Message, FailModeCategory_Enum.UN);
            }

        }

        protected override void InitInstrs(ITestEngineInit engine, DUTObject dutObject, InstrumentCollection instrs,ChassisCollection chassis)
        {
            if (engine.IsSimulation) return;
            
            #region light_tower

            /* Set up Serial Binary Comms */
            // _serialPort = buildSerPort(comPort, rate);
            if (LightTowerConfig.GetBoolParam("LightExist"))
            {
                try
                {

                    _serialPort = this.buildSerPort(LightTowerConfig.GetStringParam("ComNum"), LightTowerConfig.GetStringParam("BaudRate"));
                    _serialPort.Close();
                    if (!_serialPort.IsOpen)
                    {
                        _serialPort.Open();
                    }

                    // Add the light tower color.
                    _serialPort.WriteLine(LightTowerConfig.GetStringParam("LightGreen"));
                    _serialPort.Close();
                }
                catch { }

            }
            #endregion

            TecDsdbr = (Inst_Ke2510)instrs["TecDsdbr"];
            TecCase = (IInstType_TecController)instrs["TecCase"];

            ASIC_VccSource = (IInstType_ElectricalSource)instrs["ASIC_VccSource"];
            ASIC_VeeSource = (IInstType_ElectricalSource)instrs["ASIC_VeeSource"];
            //CtapSource = (Inst_Ke24xx)instrs["Ctap"];
            //Leftmode = (Inst_Ke24xx)instrs["LVBias"];
            //Rightmod = (Inst_Ke24xx)instrs["RVBias"];
            //fcuSource = (Inst_Ke24xx)instrs["FCUMKI_Source"];
            if (instrs.Contains("FCUMKI_Source"))
            {
                FCUMKI_Source = (InstType_ElectricalSource)instrs["FCUMKI_Source"];
                FCUMKI_Source.VoltageSetPoint_Volt = LocalCfgReader.GetDoubleParam("FcuSource_Voltage_V");
                FCUMKI_Source.CurrentComplianceSetPoint_Amp = LocalCfgReader.GetDoubleParam("FcuSource_CurrentCompliance_A");
                FCUMKI_Source.OutputEnabled = true;
                System.Threading.Thread.Sleep(5000);
            }
            else
                FCUMKI_Source = null;
            if (ASIC_VccSource != null)
            {
                ASIC_VccSource.VoltageSetPoint_Volt = LocalCfgReader.GetDoubleParam("AsicVccSource_Voltage_V");
                ASIC_VccSource.CurrentComplianceSetPoint_Amp = LocalCfgReader.GetDoubleParam("AsicVccSource_CurrentCompliance_A");
                ASIC_VccSource.OutputEnabled = true;
            }
            if (ASIC_VeeSource != null)
            {
                ASIC_VeeSource.VoltageSetPoint_Volt = LocalCfgReader.GetDoubleParam("AsicVeeSource_Voltage_V");
                ASIC_VeeSource.CurrentComplianceSetPoint_Amp = LocalCfgReader.GetDoubleParam("AsicVeeSource_CurrentCompliance_A");
                ASIC_VeeSource.OutputEnabled = true;
            }

            Fcu2Asic = (Inst_Fcu2Asic)instrs["Fcu2Asic"];
            if (dutObject.TestJigID == "FCU2AsicTestJigId")
            {
                DsdbrUtils.Fcu2AsicInstrumentGroup = new FCU2AsicInstruments();
                DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic = Fcu2Asic;
            }
            DsdbrUtils.opticalSwitch = TecDsdbr;
            Measurements.MzHead = (IInstType_OpticalPowerMeter)instrs["OpmMz"];
            Measurements.MzHead.SetDefaultState();
            Fcu2Asic.SwapWirebond = ParamManager.Conditions.IsSwapWirebond;

            //Measurements.MzHead.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;

            TestProgramCore.InitInstrs_Wavemeter(this.failModeCheck, engine, dutObject, instrs, this.LocalCfgReader, this.OpticalSwitchConfig);

            ConfigureTecController(engine, TecCase, "TecCase", TecCaseCfgReader, true, this.TC_CASE_MID_C);
            ConfigureTecController(engine, TecDsdbr, "TecDsdbr", TecDSDBRCfgReader, false, this.TC_LASER_TEMP_C);
        }

        protected override void InitOpticalSwitchIoLine()
        {
            IlmzOpticalSwitchLines.C_Band_DigiIoLine = int.Parse(OpticalSwitchConfig.GetStringParam("C_Band_Filter_IO_Line"));
            IlmzOpticalSwitchLines.C_Band_DigiIoLine_State = OpticalSwitchConfig.GetBoolParam("C_Band_Filter_IO_Line_State");
            IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine = int.Parse(OpticalSwitchConfig.GetStringParam("Dut_Ctap_IO_Line"));
            IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine_State = OpticalSwitchConfig.GetBoolParam("Dut_Ctap_IO_Line_State");
            IlmzOpticalSwitchLines.Dut_Rx_DigiIoLine = int.Parse(OpticalSwitchConfig.GetStringParam("Dut_Rx_IO_Line"));
            IlmzOpticalSwitchLines.Dut_Rx_DigiIoLine_State = OpticalSwitchConfig.GetBoolParam("Dut_Rx_IO_Line_State");
            IlmzOpticalSwitchLines.Dut_Tx_DigiIoLine = int.Parse(OpticalSwitchConfig.GetStringParam("Dut_Tx_IO_LIne"));
            IlmzOpticalSwitchLines.Dut_Tx_DigiIoLine_State = OpticalSwitchConfig.GetBoolParam("Dut_Tx_IO_LIne_State");
            IlmzOpticalSwitchLines.Etalon_Ref_DigiIoLine = int.Parse(OpticalSwitchConfig.GetStringParam("Etalon_Ref_IO_Line"));
            IlmzOpticalSwitchLines.Etalon_Ref_DigiIoLine_State = OpticalSwitchConfig.GetBoolParam("Etalon_Ref_IO_Line_State");
            IlmzOpticalSwitchLines.Etalon_Rx_DigiIoLine = int.Parse(OpticalSwitchConfig.GetStringParam("Etalon_Rx_IO_Line"));
            IlmzOpticalSwitchLines.Etalon_Rx_DigiIoLine_State = OpticalSwitchConfig.GetBoolParam("Etalon_Rx_IO_Line_State");
            IlmzOpticalSwitchLines.Etalon_Tx_DigiIoLine = int.Parse(OpticalSwitchConfig.GetStringParam("Etalon_Tx_IO_Line"));
            IlmzOpticalSwitchLines.Etalon_Tx_DigiIoLine_State = OpticalSwitchConfig.GetBoolParam("Etalon_Tx_IO_Line_State");
            IlmzOpticalSwitchLines.L_Band_DigiIoLine = int.Parse(OpticalSwitchConfig.GetStringParam("L_Band_Filter_IO_Line"));
            IlmzOpticalSwitchLines.L_Band_DigiIoLine_State = OpticalSwitchConfig.GetBoolParam("L_Band_Filter_IO_Line_State");
        }

        protected override void InitModules(ITestEngineInit engine, DUTObject dutObject, InstrumentCollection instrs, ChassisCollection chassis)
        {
            this.InitMod_Pin_Check(engine, instrs, chassis, dutObject);
            this.CaseTempControl_InitModule(engine, "CaseTemp_Mid", 25, this.TC_CASE_MID_C);

            this.DeviceTempControl_InitModule(engine, "DsdbrTemperature", TecDsdbr, "TecDsdbr");
            
            InitMod_HittMzLiSweep(engine, instrs, chassis, dutObject);
            InitMod_PhaseCutScan(engine, instrs, chassis, dutObject);
            InitMod_RearCutScan(engine, instrs, chassis, dutObject);
            InitMod_LIVMeasurement(engine, instrs, chassis, dutObject);
            InitMod_HittMzLiSweepForITH(engine, instrs, chassis, dutObject);
            InitModulesForRippleScan(engine, instrs, chassis);
            InitMod_EtalonScan(engine, instrs, chassis, dutObject);
        }
      
        #endregion

        #region Initialize modules

        /// <summary>
        /// Initial Pin check for hittGB test(check the pins correlation with TCE and Vcc)
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="instrs"></param>
        /// <param name="chassis"></param>
        /// <param name="dutObject"></param>
        private void InitMod_Pin_Check(ITestEngineInit engine, InstrumentCollection instrs, ChassisCollection chassis, DUTObject dutObject)
        {
            double Temperature_LowLimit_Degree = 15.0;
            double Temperature_HighLimit_Degree = 40.0;

            ModuleRun modRun = engine.AddModuleRun("Mod_PinCheck", "Mod_PinCheck", string.Empty);

            modRun.Instrs.Add(instrs);
            modRun.Chassis.Add(chassis);
            modRun.ConfigData.AddString("TIME_STAMP", TestTimeStamp_Start);
            modRun.ConfigData.AddString("LASER_ID", dutObject.SerialNumber);
            modRun.ConfigData.AddDouble("Temperature_LowLimit_Degree", Temperature_LowLimit_Degree);
            modRun.ConfigData.AddDouble("Temperature_HighLimit_Degree", Temperature_HighLimit_Degree);
        }

        /// <summary>
        /// Configure Case Temperature module
        /// </summary>
        private void CaseTempControl_InitModule(ITestEngineInit engine, string moduleName, Double currentTemp, Double tempSetPoint)
        {
            // Initialise module
            ModuleRun modRun = engine.AddModuleRun("TecCaseControl", "SimpleTempControl", string.Empty);
            //modRun = engine.AddModuleRun(moduleName, "SimpleTempControl", "");

            // Add instrument
            modRun.Instrs.Add("Controller", (Instrument)TecCase);

            // Add config data
            TempTablePoint tempCal = TempConfig.GetTemperatureCal(currentTemp, tempSetPoint)[0];
            DatumList tempConfig = new DatumList();
            int guiTimeOut_ms = int.Parse(FinalCfgReader.GetStringParam("CaseTempGui_UpdateTime_mS"));

            // Timeout = 2 * calibrated time, minimum of 60secs
            double timeout_s = Math.Max(tempCal.DelayTime_s * 15, 60);
            tempConfig.AddDouble("datumMaxExpectedTimeForOperation_s", timeout_s);
            // Stabilisation time = 0.5 * calibrated time
            tempConfig.AddDouble("RqdStabilisationTime_s", tempCal.DelayTime_s);
            tempConfig.AddSint32("TempTimeBtwReadings_ms", guiTimeOut_ms);
            // Actual temperature to set
            tempConfig.AddDouble("SetPointTemperature_C", tempSetPoint + tempCal.OffsetC);
            tempConfig.AddDouble("TemperatureTolerance_C", tempCal.Tolerance_degC);
            modRun.ConfigData.AddListItems(tempConfig);
        }

        /// <summary>
        /// Configure a Device Temperature Control module
        /// </summary>
        private void DeviceTempControl_InitModule(ITestEngineInit engine, string moduleName, IInstType_SimpleTempControl tecController, string configPrefix)
        {
            // Initialise module
            ModuleRun modRun = engine.AddModuleRun("TecDsdbrControl", "SimpleTempControl", string.Empty);

            // Add instrument
            modRun.Instrs.Add("Controller", (Instrument)tecController);

            //load config
            double timeout_S = FinalCfgReader.GetDoubleParam(configPrefix + "_Timeout_S");
            double stabTime_S = FinalCfgReader.GetDoubleParam(configPrefix + "_StabiliseTime_S");
            int updateTime_mS = FinalCfgReader.GetIntParam(string.Format("{0}_UpdateTime_mS", configPrefix));
            double sensorSetPoint_C = this.TC_LASER_TEMP_C;
            double tolerance_C = FinalCfgReader.GetDoubleParam(string.Format("{0}_Tolerance_C", configPrefix));

            // Add config data
            DatumList tempConfig = new DatumList();
            tempConfig.AddDouble("datumMaxExpectedTimeForOperation_s", timeout_S);
            tempConfig.AddDouble("RqdStabilisationTime_s", stabTime_S);
            tempConfig.AddSint32("TempTimeBtwReadings_ms", updateTime_mS);
            tempConfig.AddDouble("SetPointTemperature_C", sensorSetPoint_C);
            tempConfig.AddDouble("TemperatureTolerance_C", tolerance_C);

            modRun.ConfigData.AddListItems(tempConfig);
        }

        /// <summary>
        /// Initialize modules for phase scan for ripple
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="instrs"></param>
        /// <param name="chassis"></param>
        private void InitModulesForRippleScan(ITestEngineInit engine, InstrumentCollection instrs, ChassisCollection chassis)
        {
            string[] smIndexesChars = this.ProgramCfgReader.GetStringParam("SMListForRippleTest").Split(',');

            smIndexes = new List<int>(smIndexesChars.Length);

            foreach (string smIndexChar in smIndexesChars)
            {
                smIndexes.Add(int.Parse(smIndexChar));
            }

            Random random = new Random();

            isCanScanForRipple = true;

            // Do phase ripple scan for 1 DUT in 10 - chongjian.liang 2013.4.5
            if (random.Next(10) == 0)
            {
                double iRear_mA = 0;
                int frontPair = 0;
                double iFS_NonCons_mA = 0;
                double iSOA_mA = 0;

                foreach (int smIndex in smIndexes)
                {
                    // Get ripple scan conditions & result from COC stage
                    GetParamsFromCOCResult(engine, smIndex, ref isCanScanForRipple, out iRear_mA, out frontPair, out iFS_NonCons_mA, out iSOA_mA);

                    // Initialize modules
                    InitMod_HittMzLiSweepForRipple(engine, instrs, chassis, smIndex, iRear_mA, frontPair, iFS_NonCons_mA, iSOA_mA);
                    InitMod_PhaseCutScan_ForRipple(engine, instrs, chassis, smIndex, iRear_mA, frontPair, iFS_NonCons_mA, iSOA_mA);
                }
            }
            else
            {
                isCanScanForRipple = false;

                // Even cannot do phase scan to calc ripples, this module still needs to run to fill the limits
                foreach (int smIndex in smIndexes)
                {
                    InitMod_PhaseCutScan_ForRipple(engine, instrs, chassis, smIndex, 0, 0, 0, 0);
                }
            }
        }

        private void InitMod_HittMzLiSweepForITH(ITestEngineInit engine, InstrumentCollection instrs, ChassisCollection chassis, DUTObject dutObject)
        {
            ModuleRun modRun = engine.AddModuleRun("DoHittMzLiSweepForITH", "Mod_HittMzLiSweep", string.Empty);
            modRun.Instrs.Add(instrs);
            modRun.Chassis.Add(chassis);

            modRun.ConfigData.AddReference("FailModeCheck", this.failModeCheck);
            modRun.ConfigData.AddString("TIME_STAMP", TestTimeStamp_Start);
            modRun.ConfigData.AddString("LASER_ID", serialNum + "_Ith");
            modRun.ConfigData.AddString("RESULT_DIR", this.CommonTestConfig.ResultsSubDirectory);
            //modRun.ConfigData.AddString("Iphase_mA", this.cut_scan_Iphase_mA.ToString());
            //modRun.ConfigData.AddString("Irear_mA", this.cut_scan_Irear_mA.ToString());
            //modRun.ConfigData.AddString("IfsCon_mA", this.cut_scan_constant.ToString());
            //modRun.ConfigData.AddString("IfsNonCon_mA", this.cut_scan_nonConstant.ToString());
            modRun.ConfigData.AddDouble("Iphase_mA", this.ProgramCfgReader.GetDoubleParam("TC_PHASE"));
            modRun.ConfigData.AddDouble("Irear_mA", this.ProgramCfgReader.GetDoubleParam("TC_REAR"));
            modRun.ConfigData.AddDouble("IfsCon_mA", this.ProgramCfgReader.GetDoubleParam("TC_FSA"));
            modRun.ConfigData.AddDouble("IfsNonCon_mA", this.ProgramCfgReader.GetDoubleParam("TC_FSB"));
            modRun.ConfigData.AddDouble("Igain_mA", this.ProgramCfgReader.GetDoubleParam("LIV_Stop_mA"));
            modRun.ConfigData.AddDouble("Isoa_mA", this.ProgramCfgReader.GetDoubleParam("TC_SOA"));
            modRun.ConfigData.AddDouble("IrearSoa_mA", this.ProgramCfgReader.GetDoubleParam("TC_REARSOA"));
            //modRun.ConfigData.AddString("FrontPair", this.cut_scan_fp.ToString());
            modRun.ConfigData.AddSint32("FrontPair", this.ProgramCfgReader.GetIntParam("TC_FRONT_PAIR"));
            modRun.ConfigData.AddReference("MzInstruments", MzInstr);
            modRun.ConfigData.AddDouble("MaxCurrentMzSweep_mA", ParamManager.Conditions.IsThermalPhase ? FinalCfgReader.GetDoubleParam("MzCtrlINominal_thermal_mA") : FinalCfgReader.GetDoubleParam("MzCtrlINominal_mA"));
            modRun.ConfigData.AddSint32("MzSweepNumber", this.numberPointSweep);
            modRun.ConfigData.AddReference("MZSweepFilesToZip", this.mzSweepFilesToZip);

            #region Tie up limits


            //Specification spec = this.MainSpec;
            //if (MainSpec.ParamLimitExists("MZPEAK_LEFT_IMBFORITH"))
            //{
            //    modRun.Limits.AddParameter(spec, "MZPEAK_LEFT_IMBFORITH", "ImbLeft_peak_mA");
            //}
            //if (MainSpec.ParamLimitExists("MZPEAK_RIGHT_IMBFORITH"))
            //{
            //    modRun.Limits.AddParameter(spec, "MZPEAK_RIGHT_IMBFORITH", "ImbRight_peak_mA");
            //}


            #endregion
        }

        private void InitMod_LIVMeasurement(ITestEngineInit engine, InstrumentCollection instrs, ChassisCollection chassis, DUTObject dutObject)
        {
            ModuleRun modRun = engine.AddModuleRun("LIVMeasurement", "Mod_LIVMeasurement", string.Empty);
            modRun.Instrs.Add(instrs);
            modRun.Chassis.Add(chassis);

            modRun.ConfigData.AddString("TIME_STAMP", TestTimeStamp_Start);

            //modRun.Instrs.Add("dutTxfp", equipment.dutTxfp);
            //modRun.Instrs.Add("powerMeter", equipment.powerMeterSwitched);

            modRun.ConfigData.AddString("SweepFilePath", this.CommonTestConfig.ResultsSubDirectory);
            modRun.ConfigData.AddDouble("StartCurrent_mA", ProgramCfgReader.GetDoubleParam("LIV_Start_mA"));
            modRun.ConfigData.AddDouble("StopCurrent_mA", ProgramCfgReader.GetDoubleParam("LIV_Stop_mA"));
            modRun.ConfigData.AddSint32("NbrPoints", ProgramCfgReader.GetIntParam("LIV_NumOfPoints"));
            modRun.ConfigData.AddSint32("StepDelay_mS", ProgramCfgReader.GetIntParam("LIV_StepDelay_mS"));
            modRun.ConfigData.AddSint32("NumSmoothPoints_dLdI", ProgramCfgReader.GetIntParam("LIV_1stDiff_Smooth_NumPoints"));
            modRun.ConfigData.AddSint32("NumSmoothPoints_d2LdI2", ProgramCfgReader.GetIntParam("LIV_2ndDiff_Smooth_NumPoints"));
            modRun.ConfigData.AddString("SerialNumber", serialNum);

            modRun.ConfigData.AddDouble("PowerFixer", ProgramCfgReader.GetDoubleParam("PowerFixer"));
            modRun.ConfigData.AddDouble("minPwr_mW", ProgramCfgReader.GetDoubleParam("MinPwr_mW"));
            modRun.ConfigData.AddDouble("minD2ldi2", ProgramCfgReader.GetDoubleParam("MinD2ldi2"));
            modRun.ConfigData.AddDouble("TC_FRONT_PAIR", ProgramCfgReader.GetDoubleParam("TC_FRONT_PAIR"));
            modRun.ConfigData.AddDouble("TC_FSA", ProgramCfgReader.GetDoubleParam("TC_FSA"));
            modRun.ConfigData.AddDouble("TC_FSB", ProgramCfgReader.GetDoubleParam("TC_FSB"));
            modRun.ConfigData.AddDouble("TC_LEFT_BIAS", ProgramCfgReader.GetDoubleParam("TC_LEFT_BIAS"));
            modRun.ConfigData.AddDouble("TC_RIGHT_BIAS", ProgramCfgReader.GetDoubleParam("TC_RIGHT_BIAS"));
            modRun.ConfigData.AddDouble("TC_PHASE", ProgramCfgReader.GetDoubleParam("TC_PHASE"));
            modRun.ConfigData.AddDouble("TC_REAR", ProgramCfgReader.GetDoubleParam("TC_REAR"));
            modRun.ConfigData.AddDouble("TC_SOA", ProgramCfgReader.GetDoubleParam("TC_SOA"));
            modRun.ConfigData.AddDouble("TC_REARSOA", ProgramCfgReader.GetDoubleParam("TC_REARSOA"));

            #region Tie up limits

            Specification spec = this.MainSpec;

            if (MainSpec.ParamLimitExists("ITH"))
            {
                modRun.Limits.AddParameter(spec, "ITH", "ITH");
            }
            if (MainSpec.ParamLimitExists("LI_GAIN_SWEEP_FILE"))
            {
                modRun.Limits.AddParameter(spec, "LI_GAIN_SWEEP_FILE", "PLOT_LIV");
            }

            #endregion
        }

        private void InitMod_PhaseCutScan(ITestEngineInit engine, InstrumentCollection instrs, ChassisCollection chassis, DUTObject dutObject)
        {
            ModuleRun modRun = engine.AddModuleRun("DoPhaseCutScan", "Mod_PhaseScan", string.Empty);
            modRun.Instrs.Add(instrs);
            modRun.Chassis.Add(chassis);

            modRun.ConfigData.AddString("TIME_STAMP", TestTimeStamp_Start);
            modRun.ConfigData.AddString("LASER_ID", serialNum);
            modRun.ConfigData.AddString("RESULT_DIR", this.CommonTestConfig.ResultsSubDirectory);
            modRun.ConfigData.AddString("PhaseSetupFile_Path", this.PhaseCurrentSetupFile);

            modRun.ConfigData.AddDouble("Iphase_mA", this.ProgramCfgReader.GetDoubleParam("IphaseForPhaseScan_mA"));
            modRun.ConfigData.AddDouble("Irear_mA", this.ProgramCfgReader.GetDoubleParam("IrearForPhaseScan_mA"));
            modRun.ConfigData.AddDouble("IfsCon_mA", this.ProgramCfgReader.GetDoubleParam("IfsaForPhaseScan_mA"));
            modRun.ConfigData.AddDouble("IfsNonCon_mA", this.ProgramCfgReader.GetDoubleParam("IfsbForPhaseScan_mA"));
            modRun.ConfigData.AddDouble("Igain_mA", this.ProgramCfgReader.GetDoubleParam("IGainForPhaseScan_mA"));
            modRun.ConfigData.AddDouble("Isoa_mA", this.ProgramCfgReader.GetDoubleParam("ISoaForPhaseScan_mA"));
            modRun.ConfigData.AddDouble("IrearSoa_mA", this.ProgramCfgReader.GetDoubleParam("IRearSoaForPhaseScan_mA"));
            modRun.ConfigData.AddSint32("FrontPair", this.ProgramCfgReader.GetIntParam("cut_scan_fp"));
            modRun.ConfigData.AddString("FSBCurrentSetupFile_Path", this.FSBCurrentSetupFile);
            modRun.ConfigData.AddDouble("RTH_Offset_ohm", this.RTH_Offset_ohm);//Jack.zhang for post limit file RTH update. 2012-05-09

            modRun.ConfigData.AddBool("IsPostBurn", false);
            modRun.ConfigData.AddBool("ReturnNotTestedResult", false);

            double rth_Limit_MIN_ohm = Convert.ToDouble(this.MainSpec.GetParamLimit("RTH_MIN").LowLimit.ValueToString());
            double rth_Limit_MAX_ohm = Convert.ToDouble(this.MainSpec.GetParamLimit("RTH_MAX").HighLimit.ValueToString());

            modRun.ConfigData.AddDouble("RTH_LIMIT_MIN_ohm", rth_Limit_MIN_ohm);
            modRun.ConfigData.AddDouble("RTH_LIMIT_MAX_ohm", rth_Limit_MAX_ohm);

            double rth_Min_ohm = (rth_Limit_MIN_ohm + rth_Limit_MAX_ohm) / 2;
            double rth_Max_ohm = rth_Min_ohm;

            modRun.ConfigData.AddDouble("RTH_MIN_ohm", rth_Min_ohm);
            modRun.ConfigData.AddDouble("RTH_MAX_ohm", rth_Max_ohm);

            modRun.ConfigData.AddDouble("StepWidthToReadRTH", this.ProgramCfgReader.GetDoubleParam("StepWidthToReadRTH"));

            if (this.LM_MID_LINE_SWEEP_FILE != null)
            {
                modRun.ConfigData.AddReference("LM_MID_LINE_SWEEP_FILE", this.LM_MID_LINE_SWEEP_FILE);
            }

            #region Tie up limits

            Specification spec = this.MainSpec;

            modRun.Limits.AddParameter(spec, "BRAGG_FREQ", "Bragg_Freq_GHz");
            modRun.Limits.AddParameter(spec, "LAST_JUMP_INDEX", "Last_Jump_IndexPoint");
            modRun.Limits.AddParameter(spec, "LAST_MODE_LENGTH", "Last_Mode_length");
            modRun.Limits.AddParameter(spec, "COUNT_JUMPPOINT", "Count_JumpPoint");
            modRun.Limits.AddParameter(spec, "PRE_LAST_JUMP_INDEX", "Pre_Last_Jump_Index");

            modRun.Limits.AddParameter(spec, "PHASE_SCAN_FREQ_JUMP_OK", "PHASE_SCAN_FREQ_JUMP_OK");
            modRun.Limits.AddParameter(spec, "PHASE_CUT_PREBURN_FILE", "Mz_Phase_Cut_file");
            modRun.Limits.AddParameter(spec, "FSB_PREBURN_FILE", "FSB_Cut_file");
            modRun.Limits.AddParameter(spec, "IFSB_CUTSCAN", "Ifsb_forphase_mA");
            modRun.Limits.AddParameter(spec, "IASIC_PHASESCAN", "Iasic_PhaseScan");

            if (spec.ParamLimitExists("LK_KEY_POINTS_OFFSET"))
            {
                modRun.Limits.AddParameter(spec, "LK_KEY_POINTS_OFFSET", "LK_KEY_POINTS_OFFSET");
            }
            if (spec.ParamLimitExists("LK_RANGE_OFFSET"))
            {
                modRun.Limits.AddParameter(spec, "LK_RANGE_OFFSET", "LK_RANGE_OFFSET");
            }
            #endregion
        }

        private void InitMod_PhaseCutScan_ForRipple(ITestEngineInit engine, InstrumentCollection instrs, ChassisCollection chassis,
            int smIndex, double iRear_mA, int frontPair, double iFS_NonCons_mA, double iSOA_mA)
        {
            ModuleRun modRun = engine.AddModuleRun("Mod_PhaseScan_ForRipple_SM" + smIndex, "Mod_PhaseScan", string.Empty);

            modRun.Instrs.Add(instrs);
            modRun.Chassis.Add(chassis);

            modRun.ConfigData.AddString("TIME_STAMP", TestTimeStamp_Start);
            modRun.ConfigData.AddString("LASER_ID", serialNum + "_SM" + smIndex);
            modRun.ConfigData.AddString("RESULT_DIR", this.CommonTestConfig.ResultsSubDirectory);
            modRun.ConfigData.AddString("PhaseSetupFile_Path", this.PhaseCurrentSetupFile);

            modRun.ConfigData.AddSint32("SM_INDEX", smIndex);
            modRun.ConfigData.AddBool("IsPostBurn", false);
            modRun.ConfigData.AddBool("IsScanForRipple", true);
            modRun.ConfigData.AddBool("IsCanScanForRipple", isCanScanForRipple);

            modRun.ConfigData.AddBool("ReturnNotTestedResult", false);

            double rth_Limit_Min_ohm = Convert.ToDouble(this.MainSpec.GetParamLimit("RTH_MIN").LowLimit.ValueToString());
            double rth_Limit_Max_ohm = Convert.ToDouble(this.MainSpec.GetParamLimit("RTH_MAX").HighLimit.ValueToString());

            modRun.ConfigData.AddDouble("RTH_LIMIT_MIN_ohm", rth_Limit_Min_ohm);
            modRun.ConfigData.AddDouble("RTH_LIMIT_MAX_ohm", rth_Limit_Max_ohm);

            modRun.ConfigData.AddDouble("StepWidthToReadRTH", this.ProgramCfgReader.GetDoubleParam("StepWidthToReadRTH"));

            modRun.ConfigData.AddDouble("IfsNonCon_mA", iFS_NonCons_mA);
            modRun.ConfigData.AddDouble("Irear_mA", iRear_mA);
            modRun.ConfigData.AddDouble("Isoa_mA", iSOA_mA);
            modRun.ConfigData.AddSint32("FrontPair", frontPair);

            modRun.ConfigData.AddDouble("Igain_mA", this.ProgramCfgReader.GetDoubleParam("IGainForPhaseScan_mA"));
            modRun.ConfigData.AddDouble("IfsCon_mA", this.ProgramCfgReader.GetDoubleParam("IfsaForPhaseScan_mA"));
            modRun.ConfigData.AddDouble("IrearSoa_mA", this.ProgramCfgReader.GetDoubleParam("IRearSoaForPhaseScan_mA"));

            RippleTestOperations.LoadConfigToModule(this._cocResult, smIndex, modRun.ConfigData);
        }

        /// <summary>
        ///  Initalzie the rear cut scan to get the sweep data.
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="instrs"></param>
        /// <param name="chassis"></param>
        /// <param name="dutObject"></param>
        private void InitMod_RearCutScan(ITestEngineInit engine, InstrumentCollection instrs, ChassisCollection chassis, DUTObject dutObject)
        {
            ModuleRun modRun = engine.AddModuleRun("DoRearCutScan", "Mod_RearCutScan", string.Empty);
            modRun.Instrs.Add(instrs);
            modRun.Chassis.Add(chassis);

            modRun.ConfigData.AddString("TIME_STAMP", TestTimeStamp_Start);
            modRun.ConfigData.AddString("LASER_ID", serialNum);
            modRun.ConfigData.AddString("RESULT_DIR", this.CommonTestConfig.ResultsSubDirectory);
            modRun.ConfigData.AddString("RearSetupFile_Path", this.RearCurrentSetupFile);
            //modRun.ConfigData.AddString("Iphase_mA", this.cut_scan_Iphase_mA.ToString());
            //modRun.ConfigData.AddString("IfsCon_mA", this.cut_scan_constant.ToString());
            //modRun.ConfigData.AddString("IfsNonCon_mA", this.cut_scan_nonConstant.ToString());//fsb current need to get from phase scan
            modRun.ConfigData.AddString("Iphase_mA", this.ProgramCfgReader.GetStringParam("IphaseForPhaseScan_mA"));
            modRun.ConfigData.AddString("IfsCon_mA", this.ProgramCfgReader.GetStringParam("IfsaForPhaseScan_mA"));
            modRun.ConfigData.AddString("Igain_mA", this.ProgramCfgReader.GetStringParam("IGainForPhaseScan_mA"));
            modRun.ConfigData.AddString("Isoa_mA", this.ProgramCfgReader.GetStringParam("ISoaForPhaseScan_mA"));
            modRun.ConfigData.AddString("IrearSoa_mA", this.ProgramCfgReader.GetStringParam("IRearSoaForPhaseScan_mA"));
            //modRun.ConfigData.AddString("FrontPair", this.cut_scan_fp.ToString());
            modRun.ConfigData.AddString("FrontPair", this.ProgramCfgReader.GetStringParam("cut_scan_fp"));
            modRun.ConfigData.AddDouble("RTH_Offset_ohm", this.RTH_Offset_ohm);//Jack.zhang for post limit file RTH update. 2012-05-09

            modRun.ConfigData.AddBool("IsPostBurn", false);
            modRun.ConfigData.AddBool("ReturnNotTestedResult", false);

            double rth_Limit_Min_ohm = Convert.ToDouble(this.MainSpec.GetParamLimit("RTH_MIN").LowLimit.ValueToString());
            double rth_Limit_Max_ohm = Convert.ToDouble(this.MainSpec.GetParamLimit("RTH_MAX").HighLimit.ValueToString());

            modRun.ConfigData.AddDouble("RTH_LIMIT_MIN_ohm", rth_Limit_Min_ohm);
            modRun.ConfigData.AddDouble("RTH_LIMIT_MAX_ohm", rth_Limit_Max_ohm);

            modRun.ConfigData.AddDouble("StepWidthToReadRTH", this.ProgramCfgReader.GetDoubleParam("StepWidthToReadRTH"));

            #region Tie up limits

            Specification spec = this.MainSpec;

            modRun.Limits.AddParameter(spec, "RTH_MIN", "RTH_MIN_ohm");
            modRun.Limits.AddParameter(spec, "RTH_MAX", "RTH_MAX_ohm");
            modRun.Limits.AddParameter(spec, "REAR_CUT_PREBURN_FILE", "Mz_Rear_Cut_file");
            modRun.Limits.AddParameter(spec, "IASIC_REARSCAN", "Iasic_RearScan");
            #endregion
        }

        private void InitMod_EtalonScan(ITestEngineInit engine, InstrumentCollection instrs, ChassisCollection chassis, DUTObject dutObject)
        {
            ModuleRun modRun = engine.AddModuleRun("Mod_EtalonScan", "Mod_EtalonScan", string.Empty);
            modRun.Instrs.Add(instrs);
            modRun.Chassis.Add(chassis);

            modRun.ConfigData.AddString("TIME_STAMP", TestTimeStamp_Start);
            modRun.ConfigData.AddString("LASER_ID", serialNum);
            modRun.ConfigData.AddString("RESULT_DIR", this.CommonTestConfig.ResultsSubDirectory);
            modRun.ConfigData.AddReference("FailModeCheck", this.failModeCheck);

            modRun.ConfigData.AddDouble("I_ETALON_FS_A", this.I_ETALON_FS_A);
            modRun.ConfigData.AddDouble("I_ETALON_FS_B", this.I_ETALON_FS_B);
            modRun.ConfigData.AddDouble("I_ETALON_GAIN", this.I_ETALON_GAIN);
            modRun.ConfigData.AddDouble("I_ETALON_PHASE", this.I_ETALON_PHASE);
            modRun.ConfigData.AddDouble("I_ETALON_REAR", this.I_ETALON_REAR);
            modRun.ConfigData.AddDouble("I_ETALON_REAR_SOA", this.I_ETALON_REAR_SOA);
            modRun.ConfigData.AddDouble("I_ETALON_SOA", this.I_ETALON_SOA);
            modRun.ConfigData.AddSint32("ETALON_FS_PAIR", this.ETALON_FS_PAIR);
            modRun.ConfigData.AddSint32("LM_MID_LINE_INTER", this.LM_MID_LINE_INTER);
            modRun.ConfigData.AddDouble("LM_MID_LINE_SLOPE", this.LM_MID_LINE_SLOPE);

            modRun.ConfigData.AddDoubleArray("PhaseScanPoints", this.phaseScanPoints);
            modRun.ConfigData.AddDoubleArray("RearScanPoints", this.rearScanPoints);

            string FIRST_50_LOCKERSLOPEEFF = "FIRST_50_LOCKERSLOPEEFF";
            string FIRST_00_LOCKERSLOPEEFF = "FIRST_00_LOCKERSLOPEEFF";
            string FIRST_50_LOCKERSLOPE_QUAD = "FIRST_50_LOCKERSLOPE_QUAD";
            string FIRST_00_LOCKERSLOPE_QUAD = "FIRST_00_LOCKERSLOPE_QUAD";
            string ETALON_CURVE_SWEEP_FILE = "ETALON_CURVE_SWEEP_FILE";

            if (this.MainSpec.ParamLimitExists(FIRST_50_LOCKERSLOPEEFF))
            {
                modRun.Limits.AddParameter(this.MainSpec, FIRST_50_LOCKERSLOPEEFF, FIRST_50_LOCKERSLOPEEFF);
            }
            if (this.MainSpec.ParamLimitExists(FIRST_00_LOCKERSLOPEEFF))
            {
                modRun.Limits.AddParameter(this.MainSpec, FIRST_00_LOCKERSLOPEEFF, FIRST_00_LOCKERSLOPEEFF);
            }
            if (this.MainSpec.ParamLimitExists(FIRST_50_LOCKERSLOPE_QUAD))
            {
                modRun.Limits.AddParameter(this.MainSpec, FIRST_50_LOCKERSLOPE_QUAD, FIRST_50_LOCKERSLOPE_QUAD);
            }
            if (this.MainSpec.ParamLimitExists(FIRST_00_LOCKERSLOPE_QUAD))
            {
                modRun.Limits.AddParameter(this.MainSpec, FIRST_00_LOCKERSLOPE_QUAD, FIRST_00_LOCKERSLOPE_QUAD);
            }
            if (this.MainSpec.ParamLimitExists(ETALON_CURVE_SWEEP_FILE))
            {
                modRun.Limits.AddParameter(this.MainSpec, ETALON_CURVE_SWEEP_FILE, ETALON_CURVE_SWEEP_FILE);
            }
        }

        private void InitMod_HittMzLiSweep(ITestEngineInit engine, InstrumentCollection instrs, ChassisCollection chassis, DUTObject dutObject)
        {
            ModuleRun modRun = engine.AddModuleRun("DoHittMzLiSweep", "Mod_HittMzLiSweep", string.Empty);
            modRun.Instrs.Add(instrs);
            modRun.Chassis.Add(chassis);

            modRun.ConfigData.AddReference("FailModeCheck", this.failModeCheck);
            modRun.ConfigData.AddString("TIME_STAMP", TestTimeStamp_Start);
            modRun.ConfigData.AddString("LASER_ID", serialNum);
            modRun.ConfigData.AddString("RESULT_DIR", this.CommonTestConfig.ResultsSubDirectory);
            modRun.ConfigData.AddDouble("Iphase_mA", this.ProgramCfgReader.GetDoubleParam("IphaseForPhaseScan_mA"));
            modRun.ConfigData.AddDouble("Irear_mA", this.ProgramCfgReader.GetDoubleParam("IrearForPhaseScan_mA"));
            modRun.ConfigData.AddDouble("IfsCon_mA", this.ProgramCfgReader.GetDoubleParam("IfsaForPhaseScan_mA"));
            modRun.ConfigData.AddDouble("IfsNonCon_mA", this.ProgramCfgReader.GetDoubleParam("IfsbForPhaseScan_mA"));
            modRun.ConfigData.AddDouble("Igain_mA", this.ProgramCfgReader.GetDoubleParam("IGainForPhaseScan_mA"));
            modRun.ConfigData.AddDouble("Isoa_mA", this.ProgramCfgReader.GetDoubleParam("ISoaForPhaseScan_mA"));
            modRun.ConfigData.AddDouble("IrearSoa_mA", this.ProgramCfgReader.GetDoubleParam("IRearSoaForPhaseScan_mA"));
            modRun.ConfigData.AddSint32("FrontPair", this.ProgramCfgReader.GetIntParam("cut_scan_fp"));
            modRun.ConfigData.AddReference("MzInstruments", MzInstr);
            modRun.ConfigData.AddDouble("MaxCurrentMzSweep_mA", ParamManager.Conditions.IsThermalPhase ? FinalCfgReader.GetDoubleParam("MzCtrlINominal_thermal_mA") : FinalCfgReader.GetDoubleParam("MzCtrlINominal_mA"));
            modRun.ConfigData.AddSint32("MzSweepNumber", this.numberPointSweep);
            modRun.ConfigData.AddReference("MZSweepFilesToZip", this.mzSweepFilesToZip);

            #region Tie up limits

            Specification spec = this.MainSpec;
            //modRun.Limits.AddParameter(spec, "MZ_IMB_L_TRANSMI_MA", "ImbLeft_peak_mA");
            //modRun.Limits.AddParameter(spec, "MZ_IMB_R_TRANSMI_MA", "ImbRight_peak_mA");
            modRun.Limits.AddParameter(spec, "FREQ_MZ_SWEEP", "Freq_mz_sweep_Ghz");

            #endregion
        }

        private void InitMod_HittMzLiSweepForRipple(ITestEngineInit engine, InstrumentCollection instrs, ChassisCollection chassis,
            int smIndex, double iRear_mA, int frontPair, double iFS_NonCons_mA, double iSOA_mA)
        {
            ModuleRun modRun = engine.AddModuleRun("DoHittMzLiSweep_SM" + smIndex, "Mod_HittMzLiSweep", string.Empty);
            modRun.Instrs.Add(instrs);
            modRun.Chassis.Add(chassis);

            modRun.ConfigData.AddReference("FailModeCheck", this.failModeCheck);
            modRun.ConfigData.AddString("TIME_STAMP", TestTimeStamp_Start);
            modRun.ConfigData.AddString("LASER_ID", serialNum + "_SM" + smIndex);
            modRun.ConfigData.AddString("RESULT_DIR", this.CommonTestConfig.ResultsSubDirectory);
            modRun.ConfigData.AddDouble("MaxCurrentMzSweep_mA", ParamManager.Conditions.IsThermalPhase ? FinalCfgReader.GetDoubleParam("MzCtrlINominal_thermal_mA") : FinalCfgReader.GetDoubleParam("MzCtrlINominal_mA"));
            modRun.ConfigData.AddSint32("MzSweepNumber", this.numberPointSweep);
            modRun.ConfigData.AddDouble("Iphase_mA", this.ProgramCfgReader.GetDoubleParam("IphaseForPhaseScan_mA"));

            bool isReadLocalConfig = true;

            if (this._cocResult.IsPresent("PLOT_PHASE_CUT_SCAN_SM" + smIndex))
            {
                // chongjian.liang 2015.8.18
                //string phaseScanFilePath_COC = this._cocResult.ReadFileLinkFullPath("PLOT_PHASE_CUT_SCAN_SM" + smIndex);
                string phaseScanFilePath_COC = this._cocResult.ReadFileLinkFullPath("PLOT_PHASE_CUT_SCAN_SM" + smIndex);

                if (File.Exists(phaseScanFilePath_COC))
                {
                    using (CsvReader reader = new CsvReader())
                    {
                        reader.OpenFile(phaseScanFilePath_COC);

                        string[] names = reader.GetLine();
                        string[] values = reader.GetLine();

                        // Read coc config if those params are appended to the this file
                        if (values.Length > 15)
                        {
                            isReadLocalConfig = false;

                            modRun.ConfigData.AddDouble("Igain_mA", double.Parse(values[7]));
                            modRun.ConfigData.AddDouble("IfsCon_mA", double.Parse(values[9]));
                            modRun.ConfigData.AddDouble("IrearSoa_mA", double.Parse(values[13]));
                        }
                    }
                }
            }

            if (isReadLocalConfig)
            {
                modRun.ConfigData.AddDouble("Igain_mA", this.ProgramCfgReader.GetDoubleParam("IGainForPhaseScan_mA"));
                modRun.ConfigData.AddDouble("IfsCon_mA", this.ProgramCfgReader.GetDoubleParam("IfsaForPhaseScan_mA"));
                modRun.ConfigData.AddDouble("IrearSoa_mA", this.ProgramCfgReader.GetDoubleParam("IRearSoaForPhaseScan_mA"));
            }

            modRun.ConfigData.AddDouble("IfsNonCon_mA", iFS_NonCons_mA);
            modRun.ConfigData.AddDouble("Irear_mA", iRear_mA);
            modRun.ConfigData.AddDouble("Isoa_mA", iSOA_mA);
            modRun.ConfigData.AddSint32("FrontPair", frontPair);
            modRun.ConfigData.AddReference("MzInstruments", MzInstr);
            modRun.ConfigData.AddReference("MZSweepFilesToZip", this.mzSweepFilesToZip);
        }

        private void InitTecDsdbr(ITestEngineInit engine, InstrumentCollection instrs, ChassisCollection chassis, DUTObject dutObject)
        {
            ModuleRun modRun = engine.AddModuleRun("TecDsdbrControl", "SimpleTempControl", string.Empty);

            DatumList tempConfig = new DatumList();
            tempConfig.AddDouble("SetPointTemperature_C", TecDSDBRCfgReader.GetDoubleParam("SensorTemperatureSetPoint_C"));
            tempConfig.AddDouble("TemperatureTolerance_C", TecDSDBRCfgReader.GetDoubleParam("Temperature_Tolerance"));
            tempConfig.AddDouble("RqdStabilisationTime_s", TecDSDBRCfgReader.GetIntParam("StabilisationTime_s"));
            tempConfig.AddDouble("datumMaxExpectedTimeForOperation_s", TecDSDBRCfgReader.GetIntParam("TotalSettleTime_s"));
            tempConfig.AddSint32("TempTimeBtwReadings_ms", 50);

            modRun.ConfigData.AddListItems(tempConfig);

            modRun.Instrs.Add("Controller", (Instrument)instrs["TecDsdbr"]);

            modRun.Chassis.Add(chassis);


        }

        private void InitTecCase(ITestEngineInit engine, InstrumentCollection instrs, ChassisCollection chassis, DUTObject dutObject)
        {
            ModuleRun modRun = engine.AddModuleRun("TecCaseControl", "SimpleTempControl", string.Empty);

            DatumList tempConfig = new DatumList();

            tempConfig.AddDouble("SetPointTemperature_C", TecCaseCfgReader.GetDoubleParam("SensorTemperatureSetPoint_C"));
            tempConfig.AddDouble("TemperatureTolerance_C", TecCaseCfgReader.GetDoubleParam("Temperature_Tolerance"));
            tempConfig.AddDouble("RqdStabilisationTime_s", TecCaseCfgReader.GetIntParam("StabilisationTime_s"));
            tempConfig.AddDouble("datumMaxExpectedTimeForOperation_s", TecCaseCfgReader.GetIntParam("TotalSettleTime_s"));
            tempConfig.AddSint32("TempTimeBtwReadings_ms", 50);

            modRun.Instrs.Add("Controller", (Instrument)instrs["TecCase"]);
            modRun.Chassis.Add(chassis);

            modRun.ConfigData.AddListItems(tempConfig);
        }

        #endregion
        
        #region Run program

        public override void Run(ITestEngineRun engine, DUTObject dutObject, InstrumentCollection instrs, ChassisCollection chassis)
        {
            InstCommands.CloseInstToLoadDUT(engine, FCUMKI_Source, ASIC_VccSource, ASIC_VeeSource, TecDsdbr);

            //engine.ShowContinueUserQuery("Please load the DUT and click Continue to test\n\n请放置器件，点击 Continue 测试");

            int rerunCount = 0;

            bool isRTHFailed = false;

            do
            {
                this.fail_Abort_Reason = "";

                #region Pin check

                engine.RunModule("Mod_PinCheck");

                ModRunData Pin_Check_Results = engine.GetModuleRunData("Mod_PinCheck");

                bool Pin_Check_Pass = Pin_Check_Results.ModuleData.ReadBool("Pin_Check_Pass");

                if (!Pin_Check_Pass)
                {
                    this.fail_Abort_Reason += Pin_Check_Results.ModuleData.ReadString("ErrorInformation");
                    return;
                }
                #endregion

                ValidateASICType(engine, dutObject); // chongjian.liang add to validate type by FCUMKI and local config file 2013.3.22

                // Asic Part
                //bool Is_ASIC5112 = FinalCfgReader.GetBoolParam("Is_ASIC5112");
                Fcu2Asic.SetupChannelCalibration();
                ConfigureFCU2AsicInstruments();

                #region Temperature control - chongjian.liang 2012.12.3

                // Set and wait for temperatures to stabilise

                ModuleRunReturn caseControlResult = engine.RunModule("TecCaseControl");

                if (caseControlResult.ModuleRunData.ModuleData.IsPresent("ErrorInformation")) {
                    this.failModeCheck.RaiseError_Prog_NonParamFail(engine, caseControlResult.ModuleRunData.ModuleData.ReadString("ErrorInformation"), FailModeCategory_Enum.UN);
                }

                ModuleRunReturn dsdbrControlResult = engine.RunModule("TecDsdbrControl");

                if (dsdbrControlResult.ModuleRunData.ModuleData.IsPresent("ErrorInformation")) {
                    this.failModeCheck.RaiseError_Prog_NonParamFail(engine, dsdbrControlResult.ModuleRunData.ModuleData.ReadString("ErrorInformation"), FailModeCategory_Enum.UN);
                }
                #endregion

                #region DoHittMzLiSweepForITH

                ModuleRunReturn LiSweepForITHResults = engine.RunModule("DoHittMzLiSweepForITH");

                MzAnalysisWrapper.MzAnalysisResults mzAnlyDiffI = (MzAnalysisWrapper.MzAnalysisResults)LiSweepForITHResults.ModuleRunData.ModuleData.ReadReference("MzAnlyResult");

                imb_Left_mA_for_Ith = mzAnlyDiffI.Max_SrcL * 1000;

                imb_Right_mA_for_Ith = mzAnlyDiffI.Max_SrcR * 1000;
                #endregion

                #region LIVMeasurement

                ModuleRun mrLIVMeasurement = engine.GetModuleRun("LIVMeasurement");

                if (!mrLIVMeasurement.ConfigData.IsPresent("MzPeak_LEFT_IMBForITH"))
                {
                    mrLIVMeasurement.ConfigData.AddDouble("MzPeak_LEFT_IMBForITH", imb_Left_mA_for_Ith);
                }
                else
                {
                    mrLIVMeasurement.ConfigData.UpdateDouble("MzPeak_LEFT_IMBForITH", imb_Left_mA_for_Ith);
                }

                if (!mrLIVMeasurement.ConfigData.IsPresent("MzPeak_RIGHT_IMBForITH"))
                {
                    mrLIVMeasurement.ConfigData.AddDouble("MzPeak_RIGHT_IMBForITH", imb_Right_mA_for_Ith);
                }
                else
                {
                    mrLIVMeasurement.ConfigData.UpdateDouble("MzPeak_RIGHT_IMBForITH", imb_Right_mA_for_Ith);
                }

                ModuleRunReturn LIVMeasurementResults = engine.RunModule("LIVMeasurement");

                // Gain sweeping range enlarged to 60mA when ITH = 0. - chongjian.liang, requested by James Zhan
                if (LIVMeasurementResults.ModuleRunData.ModuleData.ReadDouble("ITH") < 1)
                {
                    mrLIVMeasurement.ConfigData.UpdateDouble("StopCurrent_mA", 60);

                    LIVMeasurementResults = engine.RunModule("LIVMeasurement");
                }

                #endregion

                #region DoHittMzLiSweep

                ModuleRunReturn LiSweepResults = engine.RunModule("DoHittMzLiSweep");

                mzAnlyDiffI = (MzAnalysisWrapper.MzAnalysisResults)LiSweepResults.ModuleRunData.ModuleData.ReadReference("MzAnlyResult");

                imb_Left_mA_for_IScan = mzAnlyDiffI.Quad_SrcL * 1000;

                imb_Right_mA_for_IScan = mzAnlyDiffI.Quad_SrcR * 1000;

                #endregion

                Measurements.MzHead.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;

                #region Run DoPhaseCutScan -- chongjian.liang - 2012.8.21

                // Pass fsb current to PhaseCutScan
                ModuleRun modrun = engine.GetModuleRun("DoPhaseCutScan");

                #region Update ModuleRun config

                if (!modrun.ConfigData.IsPresent("ImbLeft_mA"))
                {
                    modrun.ConfigData.AddDouble("ImbLeft_mA", imb_Left_mA_for_IScan);
                }
                else
                {
                    modrun.ConfigData.UpdateDouble("ImbLeft_mA", imb_Left_mA_for_IScan);
                }

                if (!modrun.ConfigData.IsPresent("ImbRight_mA"))
                {
                    modrun.ConfigData.AddDouble("ImbRight_mA", imb_Right_mA_for_IScan);
                }
                else
                {
                    modrun.ConfigData.UpdateDouble("ImbRight_mA", imb_Right_mA_for_IScan);
                }

                bool isReturnDummyResult;

                if (rerunCount < 2) // Run DoPhaseCutScan the first 2 times
                {
                    isReturnDummyResult = true;
                }
                else // Run DoPhaseCutScan the 3rd time
                {
                    isReturnDummyResult = false;
                }

                if (!modrun.ConfigData.IsPresent("IsReturnDummyResult"))
                {
                    modrun.ConfigData.AddBool("IsReturnDummyResult", isReturnDummyResult);
                }
                else
                {
                    modrun.ConfigData.UpdateBool("IsReturnDummyResult", isReturnDummyResult);
                }
                #endregion

                // If DoPhaseCutScan failed, the operator will continue the program manually
                ModuleRunReturn phaseScanResult = engine.RunModule("DoPhaseCutScan");

                if (phaseScanResult.ModuleRunData.ModuleData.IsPresent("IsRTHFailed"))
                {
                    isRTHFailed = phaseScanResult.ModuleRunData.ModuleData.ReadBool("IsRTHFailed");

                    // Rerun the all modules if RTH failed within 3 times
                    if (isRTHFailed && ++rerunCount < 3)
                    {
                        engine.ShowContinueUserQuery("RTH failed; Please reload the DUT, then click the Continue to proceed. 请重新装载器件到夹具上，然后点 Continue 继续.");

                        engine.SendStatusMsg("RTH failed, start over all modules.");

                        continue;
                    }
                }

                if (phaseScanResult.ModuleRunData.ModuleData.IsPresent("FAIL_ABORT_REASON"))
                {
                    this.fail_Abort_Reason += phaseScanResult.ModuleRunData.ModuleData.ReadString("FAIL_ABORT_REASON");
                }
                #endregion

                double rth_Min_ohm = phaseScanResult.ModuleRunData.ModuleData.ReadDouble("RTH_MIN_ohm");
                double rth_Max_ohm = phaseScanResult.ModuleRunData.ModuleData.ReadDouble("RTH_MAX_ohm");

                smPhaseScanResults = new List<ModuleRunReturn>();

                for (int i = 0; i < smIndexes.Count; i++)
                {
                    #region DoHittMzLiSweep

                    if (isCanScanForRipple && !isRTHFailed)
                    {
                        ModuleRunReturn liSweepResults = engine.RunModule("DoHittMzLiSweep_SM" + smIndexes[i]);

                        mzAnlyDiffI = (MzAnalysisWrapper.MzAnalysisResults)liSweepResults.ModuleRunData.ModuleData.ReadReference("MzAnlyResult");
                    }
                    #endregion

                    #region Run Mod_PhaseScan_ForRipple

                    modrun = engine.GetModuleRun("Mod_PhaseScan_ForRipple_SM" + smIndexes[i]);

                    #region Update ModuleRun config

                    // If RTH fail, not really test the module to save time.
                    if (isRTHFailed)
                    {
                        modrun.ConfigData.UpdateBool("ReturnNotTestedResult", true);
                    }

                    if (!modrun.ConfigData.IsPresent("RTH_MIN_ohm"))
                    {
                        modrun.ConfigData.AddDouble("RTH_MIN_ohm", rth_Min_ohm);
                    }
                    else
                    {
                        modrun.ConfigData.UpdateDouble("RTH_MIN_ohm", rth_Min_ohm);
                    }

                    if (!modrun.ConfigData.IsPresent("RTH_MAX_ohm"))
                    {
                        modrun.ConfigData.AddDouble("RTH_MAX_ohm", rth_Max_ohm);
                    }
                    else
                    {
                        modrun.ConfigData.UpdateDouble("RTH_MAX_ohm", rth_Max_ohm);
                    }

                    if (!modrun.ConfigData.IsPresent("ImbLeft_mA"))
                    {
                        modrun.ConfigData.AddDouble("ImbLeft_mA", mzAnlyDiffI.Max_SrcL * 1000);
                    }
                    else
                    {
                        modrun.ConfigData.UpdateDouble("ImbLeft_mA", mzAnlyDiffI.Max_SrcL * 1000);
                    }

                    if (!modrun.ConfigData.IsPresent("ImbRight_mA"))
                    {
                        modrun.ConfigData.AddDouble("ImbRight_mA", mzAnlyDiffI.Max_SrcR * 1000);
                    }
                    else
                    {
                        modrun.ConfigData.UpdateDouble("ImbRight_mA", mzAnlyDiffI.Max_SrcR * 1000);
                    }

                    if (rerunCount < 2) // Run DoRearCutScan the first 2 times
                    {
                        isReturnDummyResult = true;
                    }
                    else // Run DoRearCutScan the 3rd time
                    {
                        isReturnDummyResult = false;
                    }

                    if (!modrun.ConfigData.IsPresent("IsReturnDummyResult"))
                    {
                        modrun.ConfigData.AddBool("IsReturnDummyResult", isReturnDummyResult);
                    }
                    else
                    {
                        modrun.ConfigData.UpdateBool("IsReturnDummyResult", isReturnDummyResult);
                    }
                    #endregion

                    smPhaseScanResults.Add(engine.RunModule("Mod_PhaseScan_ForRipple_SM" + smIndexes[i]));

                    rth_Min_ohm = smPhaseScanResults[i].ModuleRunData.ModuleData.ReadDouble("RTH_MIN_ohm");
                    rth_Max_ohm = smPhaseScanResults[i].ModuleRunData.ModuleData.ReadDouble("RTH_MAX_ohm");

                    if (smPhaseScanResults[i].ModuleRunData.ModuleData.IsPresent("IsRTHFailed"))
                    {
                        isRTHFailed = smPhaseScanResults[i].ModuleRunData.ModuleData.ReadBool("IsRTHFailed");

                        // Rerun the all modules if RTH failed within 3 times
                        if (isRTHFailed && ++rerunCount < 3)
                        {
                            engine.ShowContinueUserQuery("RTH failed; Please reload the DUT, then click the Continue to proceed. 请重新装载器件到夹具上，然后点 Continue 继续.");

                            engine.SendStatusMsg("RTH failed, start over all modules.");

                            continue;
                        }
                    }

                    if (smPhaseScanResults[i].ModuleRunData.ModuleData.IsPresent("FAIL_ABORT_REASON"))
                    {
                        this.fail_Abort_Reason += smPhaseScanResults[i].ModuleRunData.ModuleData.ReadString("FAIL_ABORT_REASON");
                    }
                    #endregion
                }

                #region Run DoRearCutScan -- chongjian.liang - 2012.8.21

                modrun = engine.GetModuleRun("DoRearCutScan");

                #region Update ModuleRun config

                // If RTH fail, not really test the module to save time.
                if (isRTHFailed)
                {
                    modrun.ConfigData.UpdateBool("ReturnNotTestedResult", true);
                }

                rth_Min_ohm = phaseScanResult.ModuleRunData.ModuleData.ReadDouble("RTH_MIN_ohm");
                rth_Max_ohm = phaseScanResult.ModuleRunData.ModuleData.ReadDouble("RTH_MAX_ohm");

                if (!modrun.ConfigData.IsPresent("RTH_MIN_ohm"))
                {
                    modrun.ConfigData.AddDouble("RTH_MIN_ohm", rth_Min_ohm);
                }
                else
                {
                    modrun.ConfigData.UpdateDouble("RTH_MIN_ohm", rth_Min_ohm);
                }

                if (!modrun.ConfigData.IsPresent("RTH_MAX_ohm"))
                {
                    modrun.ConfigData.AddDouble("RTH_MAX_ohm", rth_Max_ohm);
                }
                else
                {
                    modrun.ConfigData.UpdateDouble("RTH_MAX_ohm", rth_Max_ohm);
                }

                double Ifsb_mA = phaseScanResult.ModuleRunData.ModuleData.ReadDouble("Ifsb_forphase_mA");

                if (!modrun.ConfigData.IsPresent("IfsNonCon_mA"))
                {
                    modrun.ConfigData.AddString("IfsNonCon_mA", Ifsb_mA.ToString());
                }
                else
                {
                    modrun.ConfigData.UpdateString("IfsNonCon_mA", Ifsb_mA.ToString());
                }

                if (!modrun.ConfigData.IsPresent("ImbLeft_mA"))
                {
                    modrun.ConfigData.AddDouble("ImbLeft_mA", imb_Left_mA_for_IScan);
                }
                else
                {
                    modrun.ConfigData.UpdateDouble("ImbLeft_mA", imb_Left_mA_for_IScan);
                }

                if (!modrun.ConfigData.IsPresent("ImbRight_mA"))
                {
                    modrun.ConfigData.AddDouble("ImbRight_mA", imb_Right_mA_for_IScan);
                }
                else
                {
                    modrun.ConfigData.UpdateDouble("ImbRight_mA", imb_Right_mA_for_IScan);
                }

                if (rerunCount < 2) // Run DoRearCutScan the first 2 times
                {
                    isReturnDummyResult = true;
                }
                else // Run DoRearCutScan the 3rd time
                {
                    isReturnDummyResult = false;
                }

                if (!modrun.ConfigData.IsPresent("IsReturnDummyResult"))
                {
                    modrun.ConfigData.AddBool("IsReturnDummyResult", isReturnDummyResult);
                }
                else
                {
                    modrun.ConfigData.UpdateBool("IsReturnDummyResult", isReturnDummyResult);
                }
                #endregion

                // If DoRearCutScan failed, the operator will continue the program manually
                ModuleRunReturn rearScanResult = engine.RunModule("DoRearCutScan");

                if (rearScanResult.ModuleRunData.ModuleData.IsPresent("IsRTHFailed"))
                {
                    isRTHFailed = rearScanResult.ModuleRunData.ModuleData.ReadBool("IsRTHFailed");

                    // Rerun the all modules if RTH failed within 3 times
                    if (isRTHFailed && ++rerunCount < 3)
                    {
                        engine.ShowContinueUserQuery("RTH failed; Please reload the DUT, then click the Continue to proceed. 请重新装载器件到夹具上，然后点 Continue 继续.");

                        engine.SendStatusMsg("RTH failed, start over all modules.");

                        continue;
                    }
                }

                if (rearScanResult.ModuleRunData.ModuleData.IsPresent("FAIL_ABORT_REASON"))
                {
                    this.fail_Abort_Reason += rearScanResult.ModuleRunData.ModuleData.ReadString("FAIL_ABORT_REASON");
                }
                #endregion

                #region Run Mod_EtalonScan

                modrun = engine.GetModuleRun("Mod_EtalonScan");

                if (!modrun.ConfigData.IsPresent("ImbLeft_mA"))
                {
                    modrun.ConfigData.AddDouble("ImbLeft_mA", imb_Left_mA_for_IScan);
                }
                else
                {
                    modrun.ConfigData.UpdateDouble("ImbLeft_mA", imb_Left_mA_for_IScan);
                }

                if (!modrun.ConfigData.IsPresent("ImbRight_mA"))
                {
                    modrun.ConfigData.AddDouble("ImbRight_mA", imb_Right_mA_for_IScan);
                }
                else
                {
                    modrun.ConfigData.UpdateDouble("ImbRight_mA", imb_Right_mA_for_IScan);
                }

                engine.RunModule("Mod_EtalonScan"); 

                #endregion
            }
            while (isRTHFailed && rerunCount < 3);
        }

        public override void PostRun(ITestEnginePostRun engine, DUTObject dutObject, InstrumentCollection instrs, ChassisCollection chassis)
        {
            InstCommands.CloseInstToUnLoadDUT(engine, Fcu2Asic, FCUMKI_Source, ASIC_VccSource, ASIC_VeeSource, TecDsdbr, TecCase, 25);

            TestProgramCore.CloseInstrs_Wavemeter();

            #region light_tower

            /* Set up Serial Binary Comms */
            // _serialPort = buildSerPort(comPort, rate);
            if (LightTowerConfig.GetBoolParam("LightExist"))
            {
                try
                {

                    _serialPort = this.buildSerPort(LightTowerConfig.GetStringParam("ComNum"), LightTowerConfig.GetStringParam("BaudRate"));
                    _serialPort.Close();
                    if (!_serialPort.IsOpen)
                    {
                        _serialPort.Open();
                    }

                    // Add the light tower color.
                    _serialPort.WriteLine(LightTowerConfig.GetStringParam("LightYellow"));
                    _serialPort.Close();
                }
                catch { }

            }
            #endregion

            #region Commented out lines

            //CtapSource.SetDefaultState();
            //CtapSource.OutputEnabled = false;
            //Leftmode.SetDefaultState();
            //Leftmode.OutputEnabled = false;
            //Rightmod.SetDefaultState();
            //Rightmod.OutputEnabled = false;

            //fcuSource.OutputEnabled = false;
            //System.Threading.Thread.Sleep(2000);
            //fcuSource.OutputEnabled = true;

            /*Fcu2Asic.IGain_mA = 30;
            Fcu2Asic.IGain_mA = 0;
            Fcu2Asic.ISoa_mA = 10;
            Fcu2Asic.ISoa_mA = 0;
            Fcu2Asic.IPhase_mA = 0;
            Fcu2Asic.IrearSoa_mA = 0;
            Fcu2Asic.IRear_mA = 0;
            Fcu2Asic.SetFrontPairCurrent_mA(1, (float)0, (float)0);
            Fcu2Asic.SetFrontPairCurrent_mA(2, (float)0, (float)0);
            Fcu2Asic.SetFrontPairCurrent_mA(3, (float)0, (float)0);
            Fcu2Asic.SetFrontPairCurrent_mA(4, (float)0, (float)0);
            Fcu2Asic.SetFrontPairCurrent_mA(5, (float)0, (float)0);
            Fcu2Asic.SetFrontPairCurrent_mA(6, (float)0, (float)0);
            Fcu2Asic.SetFrontPairCurrent_mA(7, (float)0, (float)0);
            Fcu2Asic.Close();*/
            #endregion
        }

        public override void DataWrite(ITestEngineDataWrite engine, DUTObject dutObject, DUTOutcome dutOutcome, TestData results, UserList userList)
        {
            Data_TC_CH_Write(engine);
            //Output main teststage
            StringDictionary mainkeys = new StringDictionary();
            mainkeys.Add("SCHEMA", "hiberdb");
            mainkeys.Add("TEST_STAGE", dutObject.TestStage.ToLower());
            mainkeys.Add("DEVICE_TYPE", dutObject.PartCode);
            mainkeys.Add("SPECIFICATION", this.MainSpecName);
            mainkeys.Add("SERIAL_NO", dutObject.SerialNumber);
            // Tell Test Engine about it...

            engine.SetDataKeysPerSpec(MainSpecName, mainkeys);

            this.traceDataList = new DatumList();

            this.traceDataList.AddSint32("NODE", dutObject.NodeID);
            traceDataList.AddString("EQUIP_ID", dutObject.EquipmentID);

            traceDataList.AddString("OPERATOR_ID", userList.InitialUser);

            double testTime = Math.Round(engine.GetTestTime() / 60, 2);
            double processTime = Math.Round(engine.TestProcessTime / 60, 2);
            double laborTime = testTime - processTime;

            traceDataList.AddDouble("TEST_TIME", testTime);

            if (this.MainSpec.ParamLimitExists("LABOUR_TIME"))
            {
                traceDataList.AddDouble("LABOUR_TIME", laborTime);
            }
            traceDataList.AddString("SOFTWARE_ID", dutObject.ProgramPluginName + " Version: " + dutObject.ProgramPluginVersion + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString());
            traceDataList.AddString("PRODUCT_CODE", dutObject.PartCode.ToString());
            traceDataList.AddString("TIME_DATE", TestTimeStamp_Start);
            //traceDataList.AddDouble("IFSA_CUTSCAN",cut_scan_constant);
            traceDataList.AddDouble("IFSA_CUTSCAN", this.ProgramCfgReader.GetDoubleParam("IfsaForPhaseScan_mA"));
            //traceDataList.AddDouble("IFSB_CUTSCAN",cut_scan_nonConstant); //remed by raul,record in phase scan module
            //traceDataList.AddDouble("FP_CUTSCAN",double.Parse(cut_scan_fp.ToString()));
            traceDataList.AddDouble("FP_CUTSCAN", this.ProgramCfgReader.GetDoubleParam("cut_scan_fp"));
            traceDataList.AddDouble("IGAIN_CUTSCAN", this.ProgramCfgReader.GetDoubleParam("IGainForPhaseScan_mA"));
            traceDataList.AddDouble("IREAR_CUTSCAN", this.ProgramCfgReader.GetDoubleParam("IrearForPhaseScan_mA"));
            traceDataList.AddDouble("ISOA_CUTSCAN", this.ProgramCfgReader.GetDoubleParam("ISoaForPhaseScan_mA"));
            traceDataList.AddDouble("IPHASE_CUTSCAN", this.ProgramCfgReader.GetDoubleParam("IphaseForPhaseScan_mA"));

            traceDataList.AddString("CHIP_ID", dutObject.Attributes.ReadString("x_chip_id"));
            traceDataList.AddString("WAFER_ID", dutObject.Attributes.ReadString("x_wafer_id"));
            traceDataList.AddString("COC_SN", dutObject.Attributes.ReadString("x_coc_sn"));
            /* traceDataList.AddString("COC_SN", ParamManager.Conditions.COC_SN);
             traceDataList.AddString("WAFER_ID", ParamManager.Conditions.WAFER_ID);
             traceDataList.AddString("CHIP_ID", ParamManager.Conditions.CHIP_ID);
             */
            if (MainSpec.ParamLimitExists("MZPEAK_LEFT_IMBFORITH"))
            {
                traceDataList.AddDouble("MZPEAK_LEFT_IMBFORITH", imb_Left_mA_for_Ith);
            }
            if (MainSpec.ParamLimitExists("MZPEAK_RIGHT_IMBFORITH"))
            {
                traceDataList.AddDouble("MZPEAK_RIGHT_IMBFORITH", imb_Right_mA_for_Ith);
            }
            if (MainSpec.ParamLimitExists("MZ_IMB_L_TRANSMI_MA"))
            {
                traceDataList.AddDouble("MZ_IMB_L_TRANSMI_MA", imb_Left_mA_for_IScan);
            }
            if (MainSpec.ParamLimitExists("MZ_IMB_R_TRANSMI_MA"))
            {
                traceDataList.AddDouble("MZ_IMB_R_TRANSMI_MA", imb_Right_mA_for_IScan);
            }


            //XIAOJIANG 2017.03.20
            if (MainSpec.ParamLimitExists("LOT_TYPE"))
                if (dutObject.Attributes.IsPresent("lot_type"))
                    DatumListAddOrUpdate(ref traceDataList,"LOT_TYPE",dutObject.Attributes.ReadString("lot_type"));
                    //traceDataList.AddString("LOT_TYPE", dutObject.Attributes.ReadString("lot_type"));
                else
                    DatumListAddOrUpdate(ref traceDataList, "LOT_TYPE", "Unknown");
                    //traceDataList.AddString("LOT_TYPE", "Unknown");

            if (MainSpec.ParamLimitExists("FACTORY_WORKS_LOTTYPE"))
                if (dutObject.Attributes.IsPresent("lot_type"))
                    DatumListAddOrUpdate(ref traceDataList, "FACTORY_WORKS_LOTTYPE", dutObject.Attributes.ReadString("lot_type"));
                    //traceDataList.AddString("FACTORY_WORKS_LOTTYPE", dutObject.Attributes.ReadString("lot_type"));
                else
                    DatumListAddOrUpdate(ref traceDataList, "FACTORY_WORKS_LOTTYPE", "Unknown");
                    //traceDataList.AddString("FACTORY_WORKS_LOTTYPE", "Unknown");

            if (MainSpec.ParamLimitExists("LOT_ID"))
                DatumListAddOrUpdate(ref traceDataList, "LOT_ID", dutObject.BatchID);
                //traceDataList.AddString("LOT_ID", dutObject.BatchID);
            if (MainSpec.ParamLimitExists("FACTORY_WORKS_LOTID"))
                DatumListAddOrUpdate(ref traceDataList, "FACTORY_WORKS_LOTID", dutObject.BatchID);
                //traceDataList.AddString("FACTORY_WORKS_LOTID", dutObject.BatchID);
            //END ADD XIAOJIANG 2017.03.20


            // Zip MZ sweep files and store in MZ_INITIAL_SWEEP_FILE - chongjian.liang 2013.5.22
            string zipFilePath = Path.Combine(this.CommonTestConfig.ResultsSubDirectory, "MZSweepFiles_" + dutObject.SerialNumber + "_" + TestTimeStamp_Start + ".zip");
            Util_ZipFile.ZipFiles(zipFilePath, this.mzSweepFilesToZip);

            if (File.Exists(zipFilePath))
            {
                traceDataList.AddFileLink("MZ_INITIAL_SWEEP_FILE", zipFilePath);
            }

            if (this.fail_Abort_Reason != null && (this.fail_Abort_Reason.Length != 0))
            {
                if (this.fail_Abort_Reason.Length > 80)
                    this.fail_Abort_Reason = this.fail_Abort_Reason.Substring(0, 80);
                traceDataList.AddString("COMMENTS", this.fail_Abort_Reason);
            }
            else
            {
                traceDataList.AddString("COMMENTS", engine.GetProgramRunComments());
            }

            if (engine.GetProgramRunStatus() != ProgramStatus.Success)
            {
                traceDataList.AddString("TEST_STATUS", engine.GetProgramRunStatus().ToString());
            }
            else
            {
                traceDataList.AddString("TEST_STATUS", this.MainSpec.Status.Status.ToString());
            }

            if (smPhaseScanResults == null
                || smPhaseScanResults.Count == 0)
            {
                string emptyFilePath = Path.Combine(this.CommonTestConfig.ResultsSubDirectory, string.Format("{0}_EmptyFile_{1}.csv", dutObject.SerialNumber, this.TestTimeStamp_Start));

                if (!File.Exists(emptyFilePath))
                {
                    File.Create(emptyFilePath).Close();
                }

                for (int i = 0; i < smIndexes.Count; i++)
                {
                    traceDataList.AddDouble("PHASE_SCAN_RIPPLE_SM" + smIndexes[i], -999);
                    traceDataList.AddSint32("PHASE_RIPPLE_SM" + smIndexes[i] + "_JUMPS", -999);

                    traceDataList.AddFileLink("PLOT_PHASE_CUT_SCAN_SM" + smIndexes[i], emptyFilePath);
                }
            }
            else
            {
                for (int i = 0; i < smIndexes.Count; i++)
                {
                    if (MainSpec.ParamLimitExists("PHASE_SCAN_RIPPLE_SM" + smIndexes[i]))
                    {
                        try
                        {
                            traceDataList.AddDouble("PHASE_SCAN_RIPPLE_SM" + smIndexes[i], smPhaseScanResults[i].ModuleRunData.ModuleData.ReadDouble("RippleOfSM" + smIndexes[i]));
                        }
                        catch
                        {
                            traceDataList.AddDouble("PHASE_SCAN_RIPPLE_SM" + smIndexes[i], -999);
                        }
                    }

                    if (MainSpec.ParamLimitExists("PHASE_RIPPLE_SM" + smIndexes[i] + "_JUMPS"))
                    {
                        try
                        {
                            traceDataList.AddSint32("PHASE_RIPPLE_SM" + smIndexes[i] + "_JUMPS", smPhaseScanResults[i].ModuleRunData.ModuleData.ReadSint32("RippleOfSM" + smIndexes[i] + "_Jumps"));
                        }
                        catch
                        {
                            traceDataList.AddSint32("PHASE_RIPPLE_SM" + smIndexes[i] + "_JUMPS", -999);
                        }
                    }

                    if (MainSpec.ParamLimitExists("PLOT_PHASE_CUT_SCAN_SM" + smIndexes[i]))
                    {
                        try
                        {
                            traceDataList.AddFileLink("PLOT_PHASE_CUT_SCAN_SM" + smIndexes[i], smPhaseScanResults[i].ModuleRunData.ModuleData.ReadFileLinkFullPath("PhaseScanFileSM" + smIndexes[i]));
                        }
                        catch
                        {
                            string emptyFilePath = Path.Combine(this.CommonTestConfig.ResultsSubDirectory, string.Format("{0}_EmptyFile_{1}.csv", dutObject.SerialNumber, this.TestTimeStamp_Start));

                            if (!File.Exists(emptyFilePath))
                            {
                                File.Create(emptyFilePath).Close();
                            }

                            traceDataList.AddFileLink("PLOT_PHASE_CUT_SCAN_SM" + smIndexes[i], emptyFilePath);
                        }
                    }
                }
            }

            if (MainSpec.ParamLimitExists("RETEST"))
            {
                traceDataList.AddString("RETEST", this.retestCount.ToString());
            }

            if (!dutOutcome.DUTData.IsPresent("RETEST"))
            {
                dutOutcome.DUTData.AddSint32("RETEST", this.retestCount);
            }

            this.failModeCheck.CheckMainSpec(engine, dutObject, this.MainSpec, this.CommonTestConfig.PCASFailMode_FilePath, ref traceDataList);

            // pick the specification to add this data to...
            engine.SetTraceData(MainSpecName, traceDataList);
        }

        #endregion

        #region Private functions

        private void traceFactoryWork(ITestEngineInit engine, DUTObject dutobject)
        {
            //DatumList factorywork = new DatumList();
            traceDataList.AddString("FACTORY_WORKS_FAILMODE", "");
            //traceDataList.AddString("FACTORY_WORKS_LOTID", dutobject.SerialNumber.ToString());
            DatumListAddOrUpdate(ref traceDataList, "FACTORY_WORKS_LOTID", dutobject.SerialNumber.ToString());
            traceDataList.AddString("FACTORY_WORKS_PARTID", dutobject.PartCode.ToString());
            //this.mainSpec.SetTraceData(factorywork);
        } 

        /// <summary>
        /// Validate ASIC type by FCUMKI and local config file - chongjian.liang 2013.3.22
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        private void ValidateASICType(ITestEngineRun engine, DUTObject dutObject)
        {
            #region Get the ASIC type, ThermalPhase type & SwapWirebond type info from file for the FCU read ASIC code

            //0140:ASIC5112
            //0110:ASIC5111 
            string ASICCodeFromFCUReading = null;

            int FCUReadCount = 0;

            // Try to read ASIC code by FCU within 3 times
            while (FCUReadCount++ < 3)
            {
                FCUMKI_Source.OutputEnabled = true;
                ASIC_VccSource.OutputEnabled = true;
                ASIC_VeeSource.OutputEnabled = true;

                int waitCount = 0;

                // Let electric outputs be stabilized
                do
                {
                    System.Threading.Thread.Sleep(100);

                    ASICCodeFromFCUReading = Fcu2Asic.AsicTypeCheck();

                    // FCU Reading missing "getasicstatus " when FCU source is not ready; FCU Reading comes with "error" when VCC & VEE sources are not ready - chongjian.liang 2013.4.3
                } while ((ASICCodeFromFCUReading == null || !ASICCodeFromFCUReading.ToLower().Contains("getasicstatus ") || ASICCodeFromFCUReading.ToLower().Contains("error")) && waitCount++ < 100);

                // Check if the FCU read ASIC code is OK
                // Or if the FCU read ASIC code containing error message - temperary condition since "ok" doesn't exist currently - chongjian.liang 2013.4.3
                if (ASICCodeFromFCUReading.ToLower().Contains("error"))
                {
                    FCUMKI_Source.OutputEnabled = false;
                    ASIC_VccSource.OutputEnabled = false;
                    ASIC_VeeSource.OutputEnabled = false;

                    // Abort the test when FCU cannot read ASIC code after 3 attemps.
                    if (FCUReadCount == 3)
                    {
                        this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "Cannot read the ASIC code by FCU.", FailModeCategory_Enum.PR);
                    }

                    // Ask the operator the reload the DUT to read ASIC within 3 attemps
                    engine.ShowContinueUserQuery("Cannot read the ASIC code by FCU, please reload the DUT and click the Continue. FCU无法读取ASIC码,请取出并重新放置器件后,点Continue继续测试.");
                }
                else
                {
                    ASICCodeFromFCUReading = ASICCodeFromFCUReading.Replace("getasicstatus ", "").Replace("ok", "").Trim().ToUpper();

                    break;
                }
            }

            List<string[]> ASICTypeCollocation;

            int indexOfLine = 1;

            using (CsvReader reader = new CsvReader())
            {
                ASICTypeCollocation = reader.ReadFile(ASICTypeCollocationPath);

                // Enumerate all ASIC Code in ASICTypeCollocation
                for (int index = indexOfLine; index < ASICTypeCollocation.Count; index++)
                {
                    // Get the index of the line that contains all info for the FCU read ASIC Code
                    if (ASICCodeFromFCUReading == ASICTypeCollocation[index][0].Trim().ToUpper())
                    {
                        indexOfLine = index;
                        break;
                    }
                    // Catch exception for no matching FCU read ASIC Code in ASICTypeCollocation
                    else if (index == ASICTypeCollocation.Count - 1)
                    {
                        string errorString_EN = string.Format("Missing ASIC code \"{0}\" in \"{1}\", please contact software engineer.", ASICCodeFromFCUReading, ASICTypeCollocationPath);
                        string errorString_CN = string.Format("文件\"{0}\"没有ASIC码\"{1}\", 请联系软件工程师. ", ASICTypeCollocationPath, ASICCodeFromFCUReading);

                        this.fail_Abort_Reason += errorString_EN;

                        this.failModeCheck.RaiseError_Prog_NonParamFail(engine, errorString_EN + errorString_CN, FailModeCategory_Enum.SW);
                    }
                }
            }
            #endregion

            bool isErrorInProgram = false;
            string errorString = "";

            #region Check if ASICType value was correctly inputted in PackageBuild result.

            if (ParamManager.Conditions.AsicType.ToUpper() != ASICTypeCollocation[indexOfLine][1].Trim().ToUpper())
            {
                isErrorInProgram = true;

                string errorString_EN = string.Format("The AsicType from PackageBuild result does not match the DUT ASIC type, please contact product engineer to retest PackageBuild. ");
                string errorString_CN = string.Format("PackageBuild测试结果的AsicType值与器件ASIC型号不一致, 请联系产品工程师重测PackageBuild. ");

                errorString += errorString_EN + errorString_CN;

                this.fail_Abort_Reason += errorString_EN;
            }
            #endregion

            #region Check if the ThermalPhase type can match this ASIC type

            // Check if this ASIC type is limited to support ThermalPhase or non ThermalPhase
            if (ASICTypeCollocation[indexOfLine].Length >= 3)
            {
                bool isThermalPhase = false;

                // Compare the ThermalPhase type from COC result with this ASIC's ThermalPhase type.
                if (bool.TryParse(ASICTypeCollocation[indexOfLine][2], out isThermalPhase))
                {
                    if (ParamManager.Conditions.IsThermalPhase != isThermalPhase)
                    {
                        isErrorInProgram = true;

                        string errorString_EN = string.Format("The ThermalPhase type does not match this ASIC type, please contact product engineer. ");
                        string errorString_CN = string.Format("ThermalPhase类型与ASIC型号不匹配, 请联系产品工程师. ");

                        errorString += errorString_EN + errorString_CN;

                        this.fail_Abort_Reason += errorString_EN;
                    }
                }
                else
                {
                    string errorString_EN = string.Format("Failed to parse Thermal Phase \"{0}\" from \"{1}\" into bool, please contact software engineer. ", ASICTypeCollocation[indexOfLine][2], ASICTypeCollocationPath);
                    string errorString_CN = string.Format("无法将从文件\"{0}\"读到的Thermal Phase值\"{1}\"转换成布尔值, 请联系软件工程师. ", ASICTypeCollocationPath, ASICTypeCollocation[indexOfLine][2]);

                    this.fail_Abort_Reason += errorString_EN;

                    this.failModeCheck.RaiseError_Prog_NonParamFail(engine, errorString_EN + errorString_CN, FailModeCategory_Enum.SW);
                }
            }

            #endregion

            #region Check if the SwapWirebond type can match this ASIC type

            // Check if this ASIC type is limited to support SwapWirebond or Wirebond
            if (ASICTypeCollocation[indexOfLine].Length >= 4)
            {
                bool isSwapWirebond = false;

                // Compare the SwapWirebond type from COC result with this ASIC's SwapWirebond type.
                if (bool.TryParse(ASICTypeCollocation[indexOfLine][3], out isSwapWirebond))
                {
                    if (ParamManager.Conditions.IsSwapWirebond != isSwapWirebond)
                    {
                        isErrorInProgram = true;

                        string errorString_EN = string.Format("The SwapWirebond type does not match this ASIC type, please contact product engineer. ");
                        string errorString_CN = string.Format("SwapWirebond类型与ASIC型号不匹配, 请联系产品工程师. ");

                        errorString += errorString_EN + errorString_CN;

                        this.fail_Abort_Reason += errorString_EN;
                    }
                }
                else
                {
                    string errorString_EN = string.Format("Failed to parse Swap Wirebond \"{0}\" from \"{1}\" into bool, please contact software engineer. ", ASICTypeCollocation[indexOfLine][3], ASICTypeCollocationPath);
                    string errorString_CN = string.Format("无法将从文件\"{0}\"读到的Swap Wirebond值\"{1}\"转换成布尔值, 请联系软件工程师. ", ASICTypeCollocationPath, ASICTypeCollocation[indexOfLine][3]);

                    this.fail_Abort_Reason += errorString_EN;

                    this.failModeCheck.RaiseError_Prog_NonParamFail(engine, errorString_EN + errorString_CN, FailModeCategory_Enum.SW);
                }
            }

            #endregion

            // Abort the test if ASICType in package build result is different from this DUT ASIC type
            // Abort the test if ThermalPhase & SwapWirebond type doesn't match this DUT ASIC type
            if (isErrorInProgram)
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, errorString, FailModeCategory_Enum.PR);
            }
        }

        private void GetTestCondition()
        {
            string[] testCondNames = new string[]
            {
                "TC_T_CASE_MID",
                "TC_DSDBR_TEMP"                
            };
            DatumList testConds = SpecValuesToDatumList.GetTestConditions(this.MainSpec, testCondNames, false);

            foreach (Datum d in testConds)
            {
                string datumName = d.Name;
                switch (datumName)
                {
                    case "TC_T_CASE_MID":
                        this.TC_CASE_MID_C = ((DatumDouble)d).Value;
                        break;
                    case "TC_DSDBR_TEMP":
                        this.TC_LASER_TEMP_C = ((DatumDouble)d).Value;
                        break;
                }
            }
        }

        /// <summary>
        /// Author: chongjian.liang 2013.12.01
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        protected override void GetEtalonAlignStageData(ITestEngineInit engine, DUTObject dutObject)
        {
            base.GetEtalonAlignStageData(engine, dutObject);

            if (!string.IsNullOrEmpty(ParamManager.Conditions.COC_SN))
            {
                StringDictionary keys = new StringDictionary();
                keys.Add("SCHEMA", ILMZSchema);
                keys.Add("SERIAL_NO", ParamManager.Conditions.COC_SN);
                keys.Add("TEST_STAGE", "etalon align");

                this._etalonAlignResult = engine.GetDataReader("PCAS_SHENZHEN").GetLatestResults(keys, true);

                if (this._etalonAlignResult != null
                    && this._etalonAlignResult.Count > 0)
                {
                    I_ETALON_FS_A = (float)this._etalonAlignResult.ReadDouble("I_ETALON_FS_A");
                    I_ETALON_FS_B = (float)this._etalonAlignResult.ReadDouble("I_ETALON_FS_B");
                    I_ETALON_GAIN = (float)this._etalonAlignResult.ReadDouble("I_ETALON_GAIN");
                    I_ETALON_PHASE = (float)this._etalonAlignResult.ReadDouble("I_ETALON_PHASE");
                    I_ETALON_REAR = (float)this._etalonAlignResult.ReadDouble("I_ETALON_REAR");
                    I_ETALON_REAR_SOA = (float)this._etalonAlignResult.ReadDouble("I_ETALON_REAR_SOA");
                    I_ETALON_SOA = (float)this._etalonAlignResult.ReadDouble("I_ETALON_SOA");
                    ETALON_FS_PAIR = this._etalonAlignResult.ReadSint32("ETALON_FS_PAIR");
                    LM_MID_LINE_INTER = this._etalonAlignResult.ReadSint32("LM_MID_LINE_INTER");
                    LM_MID_LINE_SLOPE = this._etalonAlignResult.ReadDouble("LM_MID_LINE_SLOPE");
                }

                if (this._etalonAlignResult != null
                    && this._etalonAlignResult.Count > 0
                    && this._etalonAlignResult.IsPresent("LM_MID_LINE_SWEEP_FILE"))
                {
                    // Since the LM_MID_LINE_SWEEP_FILE cannot return the full path, we have to get it from the server directly - chongjian.liang 2013.12.2
                    string fileName = this._etalonAlignResult.ReadFileLinkName("LM_MID_LINE_SWEEP_FILE");

                    string node = this._etalonAlignResult.ReadSint32("NODE").ToString();
                    string year_month = this._etalonAlignResult.ReadString("TIME_DATE").Substring(0, 6);

                    string filePath = Path.Combine(@"\\szn-sfl-clst-01\RESULTS\node" + node + "\\" + year_month, fileName);

                    if (File.Exists(filePath))
                    {
                        using (CsvReader csvReader = new CsvReader())
                        {
                            this.LM_MID_LINE_SWEEP_FILE = csvReader.ReadFile(filePath);

                            // Remove the header
                            this.LM_MID_LINE_SWEEP_FILE.RemoveAt(0);

                            // Remove the rear part
                            for (int i = 0; i < this.LM_MID_LINE_SWEEP_FILE.Count; i++)
                            {
                                if (string.IsNullOrEmpty(this.LM_MID_LINE_SWEEP_FILE[i][0]))
                                {
                                    this.LM_MID_LINE_SWEEP_FILE.RemoveRange(i, this.LM_MID_LINE_SWEEP_FILE.Count - i);
                                }
                            }
                        }
                    }
                }
            }
        }

        private void InitialiseFcu(Inst_Fcu2Asic Fcu)
        {
            Fcu.SetDefaultState();
        }

        /// <summary>
        /// Configure a TEC controller using config data
        /// </summary>
        /// <param name="tecCtlr">Instrument reference</param>
        /// <param name="tecCtlId">Config table data prefix</param>
        private void ConfigureTecController(ITestEngineBase engine, IInstType_TecController tecCtlr, string tecCtlId, TestParamConfigAccessor cfgReader, bool isTecCase, double temperatureSetPoint_C)
        {
            SteinhartHartCoefficients stCoeffs = new SteinhartHartCoefficients();

            if (tecCtlr.DriverName.Contains("Ke2510"))
            {
                // Keithley 2510 specific commands (must be done before 'SetDefaultState')
                tecCtlr.HardwareData["MinTemperatureLimit_C"] = cfgReader.GetStringParam(tecCtlId + "_MinTemp_C");
                tecCtlr.HardwareData["MaxTemperatureLimit_C"] = cfgReader.GetStringParam(tecCtlId + "_MaxTemp_C");

                tecCtlr.SetDefaultState();

                tecCtlr.Sensor_Type = (InstType_TecController.SensorType)Enum.Parse(typeof(InstType_TecController.SensorType), cfgReader.GetStringParam(tecCtlId + "_Sensor_Type"));

                tecCtlr.TecVoltageCompliance_volt = cfgReader.GetDoubleParam(tecCtlId + "_TecVoltageCompliance_volt");

                if (isTecCase)
                {
                    tecCtlr.DerivativeGain = LocalCfgReader.GetDoubleParam(tecCtlId + "_DerivativeGain");
                }
                else
                {
                    tecCtlr.DerivativeGain = cfgReader.GetDoubleParam(tecCtlId + "_DerivativeGain");
                }
            }
            else
            {
                tecCtlr.SetDefaultState();
            }

            tecCtlr.OperatingMode = (InstType_TecController.ControlMode)Enum.Parse(typeof(InstType_TecController.ControlMode), cfgReader.GetStringParam(tecCtlId + "_OperatingMode"));

            // Thermistor characteristics
            if (tecCtlr.Sensor_Type == InstType_TecController.SensorType.Thermistor_2wire ||
                tecCtlr.Sensor_Type == InstType_TecController.SensorType.Thermistor_4wire)
            {
                stCoeffs.A = cfgReader.GetDoubleParam(tecCtlId + "_stCoeffs_A");
                stCoeffs.B = cfgReader.GetDoubleParam(tecCtlId + "_stCoeffs_B");
                stCoeffs.C = cfgReader.GetDoubleParam(tecCtlId + "_stCoeffs_C");
                tecCtlr.SteinhartHartConstants = stCoeffs;
            }

            // Additional parameters
            if (isTecCase)
            {
                tecCtlr.TecCurrentCompliance_amp = LocalCfgReader.GetDoubleParam(tecCtlId + "_TecCurrentCompliance_amp");
                tecCtlr.ProportionalGain = LocalCfgReader.GetDoubleParam(tecCtlId + "_ProportionalGain");
                tecCtlr.IntegralGain = LocalCfgReader.GetDoubleParam(tecCtlId + "_IntegralGain");
            }
            else
            {
                tecCtlr.TecCurrentCompliance_amp = cfgReader.GetDoubleParam(tecCtlId + "_TecCurrentCompliance_amp");
                tecCtlr.ProportionalGain = cfgReader.GetDoubleParam(tecCtlId + "_ProportionalGain");
                tecCtlr.IntegralGain = cfgReader.GetDoubleParam(tecCtlId + "_IntegralGain");
            }

            try
            {
                tecCtlr.SensorTemperatureSetPoint_C = temperatureSetPoint_C;
            }
            catch (Exception e)
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, e.Message, FailModeCategory_Enum.UN);
            }

        }

        private void InitialiseTec(Inst_Ke2510 Tec, string TecName, TestParamConfigAccessor cfgReader)
        {
            //NameValueCollection TecSettings = (NameValueCollection)cfgReader.
            //GetSection("InstrumentSettings/TemperatureSettings/" + TecName);
            Tec.SetDefaultState();
            Tec.OutputEnabled = false;
            //取消ground connect 
            // Tec.GroundConnect = bool.Parse(TecSettings["GroundConnect"]);
            //使用温度控制模式
            Tec.OperatingMode = (InstType_TecController.ControlMode)Enum.
                Parse(typeof(InstType_TecController.ControlMode), cfgReader.GetStringParam("OperatingMode"));
            //启用4线，RTD温度感应(用户模式，需要指定α,β,δ,R0)，A=1.2073e-3,B=2.20813e-4,C=1.4494e-7
            InstType_TecController.SensorType st = (InstType_TecController.SensorType)Enum.
                Parse(typeof(InstType_TecController.SensorType), cfgReader.GetStringParam("SensorType"));
            Tec.Sensor_Type = st;
            if (st == InstType_TecController.SensorType.Thermistor_2wire
                || st == InstType_TecController.SensorType.Thermistor_4wire)
            {
                SteinhartHartCoefficients stCoeffs = new SteinhartHartCoefficients();
                stCoeffs.A = cfgReader.GetDoubleParam("stCoeffs_A");
                stCoeffs.B = cfgReader.GetDoubleParam("stCoeffs_B");
                stCoeffs.C = cfgReader.GetDoubleParam("stCoeffs_C");
                Tec.SteinhartHartConstants = stCoeffs;
            }
            else if (st == InstType_TecController.SensorType.RTD_2wire
                || st == InstType_TecController.SensorType.RTD_4wire)
            {
                CallendarVanDusenCoefficients cvCoeffs = new CallendarVanDusenCoefficients();

            }
            else
            {

            }
            //ProportionalGain=80, IntegrallGain=5.0, DerivativeGain=0 
            Tec.ProportionalGain = cfgReader.GetDoubleParam("ProportionalGain");   //80;
            Tec.IntegralGain = cfgReader.GetDoubleParam("IntegralGain");          //5.0;
            Tec.DerivativeGain = cfgReader.GetDoubleParam("DerivativeGain");      //0;
            //保护电流=1.6A,保护电压=5V 
            Tec.TecCurrentCompliance_amp = cfgReader.GetDoubleParam("TecCurrentCompliance_amp");   //1.6;
            Tec.TecVoltageCompliance_volt = cfgReader.GetDoubleParam("TecVoltageCompliance_volt"); //5.0; //5.0;
            //最低温度=0, 最高温度=80 
            //Tec.Set_Protection_Level_Temperature(Inst_Ke2510.LimitType.LOWER, double.Parse(TecSettings["MinTemperatureLimit_C"]));//0
            //Tec.Set_Protection_Level_Temperature(Inst_Ke2510.LimitType.UPPER, double.Parse(TecSettings["MaxTemperatureLimit_C"]));//60
            //Tec.Set_Protection(Inst_Ke2510.ProtectionType.ptTEMPERATURE, Inst_Ke2510.OnOrOff.ON);

            //normal operate
            Tec.SensorTemperatureSetPoint_C = cfgReader.GetDoubleParam("SensorTemperatureSetPoint_C");//25;
            Tec.OutputEnabled = cfgReader.GetBoolParam("OutputEnabled");
        }

        private void GetParamsFromCOCResult(ITestEngineInit engine, int sm_Index, ref bool canScanForRipple, out double iRear_mA, out int frontPair, out double iFS_NonCons_mA, out double iSOA_mA)
        {
            /// STEP 1: Read coc test result from local server

            string timeStamp = this._cocResult.ReadString("CG_DUT_FILE_TIMESTAMP");
            int node = this._cocResult.ReadSint32("NODE");
            string serialNum = this._cocResult.ReadString("SERIAL_NO");

            string directory = @"\\Szn-sfl-clst-01\Results\node" + node + "\\" + timeStamp.Substring(0, 8) + "\\";
            string fileName = "Im_DSDBR01_" + serialNum + "_" + timeStamp + "_SM" + sm_Index + ".csv";

            if (!File.Exists(directory + fileName) || !this._cocResult.IsPresent("PHASE_SCAN_RIPPLE_SM" + sm_Index) || this._cocResult.ReadDouble("PHASE_SCAN_RIPPLE_SM" + sm_Index) == double.NaN)
            {
                iRear_mA = 0;
                frontPair = 0;
                iFS_NonCons_mA = 0;
                iSOA_mA = 0;
                canScanForRipple = false;
                return;
            }

            using (CsvReader reader = new CsvReader())
            {
                reader.OpenFile(directory + fileName);

                List<string[]> sm_SweepData = reader.ReadFile(directory + fileName);

                string[] sm_Data = sm_SweepData[sm_SweepData.Count - 3];

                iRear_mA = double.Parse(sm_Data[0]);
                frontPair = int.Parse(sm_Data[1]);
                iFS_NonCons_mA = double.Parse(sm_Data[3]);
            }

            /// STEP 2: Read the coc test result refers to local file

            double power = this._cocResult.ReadDouble("P_AT_XMA");

            List<string[]> soa_Data = new List<string[]>();

            using (CsvReader reader = new CsvReader())
            {
                soa_Data = reader.ReadFile(@"Configuration\TOSA Final\PhaseScan\PhaseCutScanSoaCurrent.csv");
                soa_Data.Remove(soa_Data[0]);

                int index = -1;

                foreach (string[] soa_line in soa_Data)
                {
                    if (power >= double.Parse(soa_line[0]))
                    {
                        index++;
                    }
                    else
                    {
                        break;
                    }
                }

                iSOA_mA = 0;

                if (index == -1)
                {
                    this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "P_AT_XMA from COC result is less than " + soa_Data[0][0] + "mW.", FailModeCategory_Enum.PR);
                }
                else
                {
                    if (sm_Index == 0)
                    {
                        iSOA_mA = double.Parse(soa_Data[index][2]);
                    }
                    else if (sm_Index == 3)
                    {
                        iSOA_mA = double.Parse(soa_Data[index][3]);
                    }
                    else
                    {
                        this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "SM index must be 0 or 3.", FailModeCategory_Enum.SW);
                    }
                }
            }
        }

        /// <summary>
        /// Fill in blank TC and CH results
        /// </summary>
        private void Data_TC_CH_Write(ITestEngineDataWrite engine)
        {
            DatumList TraceData = new DatumList();
            foreach (ParamLimit paramLimit in MainSpec)
            {
                if (paramLimit.ExternalName.StartsWith("TC_") || paramLimit.ExternalName.StartsWith("CH_"))
                {
                    if (!TraceData.IsPresent(paramLimit.ExternalName))
                    {
                        Datum dummyValue = null;
                        switch (paramLimit.ParamType)
                        {
                            case DatumType.Double:
                                dummyValue = new DatumDouble(paramLimit.ExternalName, Convert.ToDouble(paramLimit.HighLimit.ValueToString()));
                                break;
                            case DatumType.Sint32:
                                dummyValue = new DatumSint32(paramLimit.ExternalName, Convert.ToInt32(paramLimit.HighLimit.ValueToString()));
                                break;
                            case DatumType.StringType:
                                dummyValue = new DatumString(paramLimit.ExternalName, "MISSING");
                                break;
                            case DatumType.Uint32:
                                dummyValue = new DatumUint32(paramLimit.ExternalName, Convert.ToUInt32(paramLimit.HighLimit.ValueToString()));
                                break;
                        }

                        if (dummyValue != null)
                            TraceData.Add(dummyValue);
                    }
                }
            }
            engine.SetTraceData(this.MainSpecName, TraceData);

        }

        /// <summary>
        /// initialise FCU2ASIC INSTRUMENTS 
        /// </summary>
        /// <param name="fcu2AsicInstrs">FCU2ASIC instruments group </param>
        private void ConfigureFCU2AsicInstruments()
        {
            string FCUMKI_Tx_CalFilename = MapParamsConfig.GetStringParam("FCUMKI_Tx_Calibration_file_Path");
            string FCUMKI_Rx_CalFilename = MapParamsConfig.GetStringParam("FCUMKI_Rx_Calibration_file_Path");
            string FCUMKI_Var_CalFilename = MapParamsConfig.GetStringParam("FCUMKI_Var_Calibration_file_Path");
            string FCUMKI_Fix_CalFilename = MapParamsConfig.GetStringParam("FCUMKI_Fix_Calibration_file_Path");

            FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Tx = FreqCalByEtalon.LoadFCUMKICalArray(FCUMKI_Tx_CalFilename);
            FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Rx = FreqCalByEtalon.LoadFCUMKICalArray(FCUMKI_Rx_CalFilename);
            FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Var = FreqCalByEtalon.LoadFCUMKICalArray(FCUMKI_Var_CalFilename);
            FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Fix = FreqCalByEtalon.LoadFCUMKICalArray(FCUMKI_Fix_CalFilename);
            //initial FCUMKI pot value in FCUMKI cal file list. Jack.zhang 2012-04-12
            DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic.Locker_tran_pot = FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Tx[FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Tx.Count - 1].Pot;
            DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic.Locker_refi_pot = FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Rx[FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Rx.Count - 1].Pot;
            DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic.Locker_fine_pot = 255;
        }

        #endregion
    }
}
using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestSolution.IlmzCommonData;
using Bookham.TestLibrary.Utilities;
using Bookham.TestEngine.Framework.Limits;

namespace Bookham.TestSolution.TestPrograms
{

    internal class IlmzMapTestConds
    {
        internal const int MaxNbrSupermodes = 8;

        internal IlmzMapTestConds(Specification spec)
        {
            // get test conditions
            string[] testCondNames = new string[]
            {
                "TC_NUM_CHAN_REQUIRED",
                "TC_OPTICAL_FREQ_START",
                "TC_FIBRE_TARGET_POWER",
                "TC_TX_TARGET_CURRENT",
                "TC_REARSOA_MIN",
                "TC_REARSOA_MAX",
                "TC_CAL_OFFSET",
                "TC_T_CASE_MID"
            };
            DatumList testConds = SpecValuesToDatumList.GetTestConditions(spec, testCondNames);

            foreach (Datum d in testConds)
            {
                string datumName = d.Name;
                switch (datumName)
                {
                    case "TC_NUM_CHAN_REQUIRED":
                        this.NbrChannels = ((DatumSint32)d).Value;
                        break;
                    case "TC_OPTICAL_FREQ_START":
                        this.ItuLowFreq_GHz = ((DatumDouble)d).Value;
                        break;
                    case "TC_FIBRE_TARGET_POWER":
                        this.TargetFibrePower_dBm = ((DatumDouble)d).Value;
                        break;
                    case "TC_TX_TARGET_CURRENT":
                        this.RxTargetCurrent = ((DatumDouble)d).Value; ;
                        break;
                    case "TC_REARSOA_MIN":
                        this.RearsoaMin = ((DatumDouble)d).Value;
                        break;
                    case "TC_REARSOA_MAX":
                        this.RearsoaMax = ((DatumDouble)d).Value;
                        break;
                    case "TC_CAL_OFFSET":
                        this.CalOffset = ((DatumDouble)d).Value;
                        break;
                    case "TC_T_CASE_MID":
                        this.MidTemp = ((DatumDouble)d).Value;
                        break;

                }
            }
        }

        internal double CalOffset;
        internal double OverallMapIGain_mA;
        internal double LaserTecEolDeltaT_C;
        internal double LockerBias_V;
        internal double MzTecEolDeltaT_C;
        internal double MzVcmCalLimitMax_V;
        internal double MzVcmCalLimitMin_V;
        internal double MzVpiCalLimitMax_V;
        internal double MzVpiCalLimitMin_V;
        internal double MzImbLeftLimitMin_mA;
        internal double MzImbLeftLimitMax_mA;
        internal double MzImbRightLimitMin_mA;
        internal double MzImbRightLimitMax_mA;
        internal double MzPeakVLimitMin_V;
        internal double MzPeakVLimitMax_V;
        internal double MzMinVLimitMin_V;
        internal double MzMinVLimitMax_V;
        internal double MzQuadVLimitMin_V;
        internal double MzQuadVLimitMax_V;
        internal int NbrChannels;
        internal FreqBand FreqBand;
        internal double ItuLowFreq_GHz;
        internal double ItuHighFreq_GHz;
        internal double ItuSpacing_GHz;
        internal double OverallMapISoa_mA;
        internal double OverallMapIPhase_mA;
        internal double TargetFibrePower_dBm;
        internal double TargetFibrePowerTol_dB;
        internal double MidTemp;
        internal double LowTemp;
        internal double HighTemp;
        internal double PwrControlRangeLow;
        internal double PwrControlRangeHigh;
        internal double TraceToneTestAgeCond1_dB;
        internal double TraceToneTestAgeCond2_dB;
        internal double MzDcVpiSlopeLimitMax;
        internal double MzDcVpiSlopeLimitMin;
        internal double LockerSlopeEffAbsLimitMax;
        internal double LockerSlopeEffAbsLimitMin;
        internal double RearsoaMap;
        internal double RearsoaMax;
        internal double RearsoaMin;
        internal double RxTargetCurrent;
    }
}

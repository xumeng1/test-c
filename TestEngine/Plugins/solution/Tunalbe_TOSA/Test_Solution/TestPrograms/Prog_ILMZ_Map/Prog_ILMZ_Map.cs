// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestPrograms
//
// Prog_TCMZ_Final.cs
//
// Author: paul.annetts, Mark Fullalove 2006
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.ExternalData;
using Bookham.TestEngine.Framework.Limits;
using System.Collections.Specialized;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestLibrary.Utilities;
using Bookham.TestSolution.IlmzCommonInstrs;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestSolution.IlmzCommonData;
using Bookham.TestSolution.ILMZFinalData;
using Bookham.TestSolution.IlmzCommonUtils;
using Bookham.TestSolution.TestModules;
using Bookham.TestLibrary.Instruments;
using Bookham.ToolKit.Mz;
using Bookham.TestEngine.Config;
using System.Collections;
using Bookham.TestLibrary.BlackBoxes;
using Bookham.Program.Core;
using System.Threading;
using System.IO.Ports;
using Bookham.fcumapping.CommonData;
using Bookham.Solution.Config;
using Bookham.TestLibrary.Algorithms;
using Bookham.TestSolution.Instruments;
using Bookham.Program.Core;

namespace Bookham.TestSolution.TestPrograms
{
    /// <summary>
    /// ILMZ Negtive Chirp chip test program
    /// </summary>
    public class Prog_ILMZ_Map : TestProgramCore
    {
        #region Private data
        /// <summary>
        /// Remember the specification name we are using in the program
        /// </summary>
        //private Prog_ILMZ_FinalInfo progInfo;

        const int FP_LOWER_LIMIT = 1;
        const int FP_UPPER_LIMIT = 7;

        private DateTime testTime_End;

        private double labourTime = 0;

        private string errorInformation = "";
        private string CommentInformation = "";

        private double locker_pot_value = 0;
        private double soa_powerLeveling = 0;
        private bool useDUTEtalon = false;
        private bool UseLowCostSolution = false;
        private double Max_LockerCurrent_mA = 0;
        private double Min_LockerCurrent_mA = 0;
        private List<double> Isoa_SM = new List<double>();
        private List<double> Mz_Imb_Left_mA_SM = new List<double>();
        private List<double> Mz_Imb_Right_mA_SM = new List<double>();
        private List<int> Var_Pot_SM = new List<int>();
        private List<int> Tx_Pot_SM = new List<int>();
        private List<int> Rx_Pot_SM = new List<int>();
        private double[,] slope_rear_rearsoa;
        private string CG_ID = "CG no id";
        private string IPhaseModeAcqZipFilename;
        private string LowCostMapDataZipFilename;

        private const string cg_setting_file_const = @"Configuration\TOSA Final\MapTest\CloseGridSettings.xml";
        private string Nofilepath = @"Configuration\TOSA Final\MapTest\NoDataFile.txt";
        private string MappinSettingsPath = @"Configuration\TOSA Final\MapTest\MappingTestConfig.xml";
        private string FinalSettingsPath = @"Configuration\TOSA Final\FinalTest\CommonTestParams.xml"; //just for same DUT and jig temp_control setting

        private string cg_setting_file = cg_setting_file_const;
        private List<CDSDBRSuperMode> supermodes = new List<CDSDBRSuperMode>();
        private ConfigurationManager CGcfgReader = null;


        private TestParamConfigAccessor TestParameterConfig_Map = null;
        private TestParamConfigAccessor OpticalSwitchConfig = null;
        private TestParamConfigAccessor TestParameterConfig_Final = null;
        private TestParamConfigAccessor LocalTestParamsConfig = null;
        private TempTableConfigAccessor TempConfig = null;
        private TestParamConfigAccessor LightTowerConfig = null;

        Inst_Ke2510 TecDsdbr;
        IInstType_TecController TecCase;

        Inst_Fcu2Asic Fcu;
        InstType_ElectricalSource FCUMKISource;
        InstType_ElectricalSource VcckeSource;
        InstType_ElectricalSource VeekeSource;
        Inst_Ke24xx CtapSource;
        ITestEngineInit testEngineInit;

        /// <summary>
        /// Remember the specification name we are using in the program
        /// </summary>
        private string MainSpecName;
        private string MainTestStage;
        private int numberOfSupermodes;

        private TestConditionType TestCondition = new TestConditionType();

        private int[] MILD_MULTIMODE_SM = new int[8];


        private double[] Modal_Distort_SM = new double[8];

        internal SpecList SupermodeSpecs;
        internal string smSpecName;
        DatumList traceDataList = null;

        string LaserWaferSize = "3";

        ITestEngineInit testEngine = null;

        Dictionary<string, double> cocResults_UPPER_FP_CROSSING = new Dictionary<string, double>();

        #endregion

        #region for Light Tower.

        private SerialPort _serialPort;
        /// <summary>
        /// Serial port to control the lighter to indicate test messeage
        /// </summary>
        public SerialPort SerialPort
        {
            get { return _serialPort; }
        }

        /// <summary>
        /// Construct a serial port object from parts of the resource string.
        /// </summary>
        /// <param name="comPort">COMx port portion of resource string.</param>
        /// <param name="rate">bps rate portion of resource string.</param>
        /// <returns>Constructed SerialPort handler instance.</returns>
        private SerialPort buildSerPort(string comPort, string rate)
        {
            return new System.IO.Ports.SerialPort(
                "COM" + comPort,                                /* COMn */
                int.Parse(rate),                                /* bps */
                System.IO.Ports.Parity.None, 8, StopBits.One);  /* 8N1 */
        }
        #endregion
        
        /// <summary>
        ///  GUI for program 
        /// </summary>
        public override Type UserControl
        {
            get { return typeof(Prog_ILMZ_MapGui); }
        }

        #region Implement InitCode method

        /// <summary>
        /// Initialise test configuration
        /// </summary>
        /// <param name="dutObj">Device under test</param>
        /// <param name="engine">Test engine</param>
        protected override void InitConfig(ITestEngineInit engine, DUTObject dutObject)
        {
            base.InitConfig(engine, dutObject);

            testEngineInit = engine;

            TempConfig = new TempTableConfigAccessor(dutObject, 1, @"Configuration\TOSA Final\TempTable.xml");

            CGcfgReader = new ConfigurationManager(cg_setting_file_const);

            TestParameterConfig_Map = new TestParamConfigAccessor(dutObject, MappinSettingsPath, "", "MappingTestParams");

            TestParameterConfig_Final = new TestParamConfigAccessor(dutObject, FinalSettingsPath, "", "TestParams");

            LocalTestParamsConfig = new TestParamConfigAccessor(dutObject, @"Configuration\TOSA Final\LocalTestParams.xml", "", "TestParams");

            OpticalSwitchConfig = new TestParamConfigAccessor(dutObject,
                 @"Configuration\TOSA Final\IlmzOpticalSwitch.xml", "", "IlmzOpticalSwitchParams");

            LightTowerConfig = new TestParamConfigAccessor(dutObject,
                @"Configuration\TOSA Final\LightTowerParams.xml", "", "LightTowerParams");

            string cg_setting_file_Temp = Path.Combine(this.CommonTestConfig.ResultsSubDirectory, Path.GetFileNameWithoutExtension(cg_setting_file) + "_" + TestTimeStamp_Start + Path.GetExtension(cg_setting_file));
            File.Copy(cg_setting_file, cg_setting_file_Temp);
            File.SetAttributes(cg_setting_file_Temp, FileAttributes.Normal); // Set file to normal stage to resolve the readonly file can not be updated to server problem - chongjian.liang 2013.4.18
            cg_setting_file = cg_setting_file_Temp;

            string Nofilepath_Temp = Path.Combine(this.CommonTestConfig.ResultsSubDirectory, Path.GetFileNameWithoutExtension(Nofilepath) + "_" + TestTimeStamp_Start + Path.GetExtension(Nofilepath));
            File.Copy(Nofilepath, Nofilepath_Temp, true);
            File.SetAttributes(Nofilepath_Temp, FileAttributes.Normal);
            Nofilepath = Nofilepath_Temp;//Jack.zhang fix Nofile load by stream bug.2011-09-06

            IPhaseModeAcqZipFilename = this.Nofilepath;
        }

        protected override void LoadSpecs(ITestEngineInit engine, DUTObject dutObject)
        {
            bool ReadPcasFromLocal = TestParameterConfig_Map.GetBoolParam("UseLocalLimitFile");

            StringDictionary mainSpecKeys = new StringDictionary();
            StringDictionary smSpecKeys = new StringDictionary();
            SpecList tempSpecList;
            ILimitRead limitReader;
            SpecList specList = new SpecList();

            string smSpecNameStub = "sm";
            this.SupermodeSpecs = new SpecList();

            mainSpecKeys.Add("SCHEMA", "HIBERDB");
            mainSpecKeys.Add("DEVICE_TYPE", dutObject.PartCode);
            if (dutObject.TestStage.ToLower() == "maptest")   //raul added to switch if use MES.
            {
                mainSpecKeys.Add("TEST_STAGE", "hitt_map");
            }
            else
            {
                mainSpecKeys.Add("TEST_STAGE", dutObject.TestStage.ToLower());
            }
            smSpecKeys.Add("SCHEMA", "hiberdb");
            smSpecKeys.Add("DEVICE_TYPE", dutObject.PartCode);
            smSpecKeys.Add("TEST_STAGE", "map_sm0");
            dutObject.Attributes.AddString("SupermodeSpecNameStub", smSpecNameStub);

            if (!ReadPcasFromLocal)
            {
                //load main specification
                limitReader = engine.GetLimitReader();
                tempSpecList = limitReader.GetLimit(mainSpecKeys);
                this.MainSpec = tempSpecList[0];
                this.MainSpecName = this.MainSpec.Name;
                tempSpecList = limitReader.GetLimit(smSpecKeys);
                Specification smSpecMaster = tempSpecList[0];
                smSpecName = smSpecMaster.Name;
                foreach (ParamLimit testParam in this.MainSpec)
                {
                    if (testParam.ExternalName == "NUM_SM")
                    {
                        numberOfSupermodes = int.Parse(testParam.HighLimit.ValueToString());
                    }
                }
                for (int ii = 0; ii < numberOfSupermodes; ii++)
                {
                    Specification smSpec_ii = new Specification(smSpecNameStub + "_" + ii.ToString(), smSpecMaster);
                    this.SupermodeSpecs.Add(smSpec_ii);
                }
            }
            else
            {
                //read master limit file from local pc
                limitReader = engine.GetLimitReader();
                tempSpecList = limitReader.GetLimit(mainSpecKeys);
                this.MainSpecName = tempSpecList[0].Name;//get the current PCAS name from server. jack.zhang 2012-06-13
                tempSpecList = limitReader.GetLimit(smSpecKeys);
                smSpecName = tempSpecList[0].Name;//get the current PCAS name from server. jack.zhang 2012-06-13
                this.CommentInformation += ":Note:the test with local limit file!";

                limitReader = engine.GetLimitReader("PCAS_FILE");
                string LimitFile = TestParameterConfig_Map.GetStringParam("LocalLimitFile");
                LimitFile = Path.Combine(Path.GetDirectoryName(MappinSettingsPath), LimitFile);
                string TempLimitFile = LimitFile.Substring(0, LimitFile.LastIndexOf("\\") + 1) + this.MainSpecName + ".csv"; ;
                File.Copy(LimitFile, TempLimitFile, true);
                StringDictionary keys = new StringDictionary();
                keys.Add("Filename", TempLimitFile);
                tempSpecList = limitReader.GetLimit(keys);
                this.MainSpec = tempSpecList[0];
                File.Delete(TempLimitFile);
                //read 8 supermode limit file from local pc
                string Sub_LimitFile = LimitFile.Substring(0, LimitFile.Length - 4) + "_SM_0";
                Specification smSpecMaster = tempSpecList[0];

                for (int i = 0; i < 8; i++)
                {
                    LimitFile = Sub_LimitFile + i.ToString() + ".csv";

                    if (keys.ContainsKey("Filename"))
                    {
                        keys.Remove("Filename");
                        keys.Add("Filename", LimitFile);
                    }
                    tempSpecList = limitReader.GetLimit(keys);
                    smSpecMaster = tempSpecList[0];
                    Specification smSpec_ii = new Specification(smSpecNameStub + "_" + i.ToString(), smSpecMaster);
                    this.SupermodeSpecs.Add(smSpec_ii);
                }
            }
            specList.Add(this.MainSpec);
            foreach (Specification smSpec in this.SupermodeSpecs)
            {
                specList.Add(smSpec);
            }
            engine.SetSpecificationList(specList);

            GetTestCondition();

            if (TestCondition.DSDBR_TEMP_C == 0)//temp for add DSDBR_temp in limit file jack.zhang 2012-04-19
                TestCondition.DSDBR_TEMP_C = TestParameterConfig_Final.GetDoubleParam("TecDsdbr_SensorTemperatureSetPoint_C");
            
            base.LoadSpecs(engine, dutObject);
        }

        protected override void ValidatePostBurnStageData(ITestEngineInit engine, DUTObject dutObject)
        {
            base.ValidatePostBurnStageData(engine, dutObject);

            if (TestParameterConfig_Map.GetBoolParam("validatePostBurn"))
            {
                IDataRead dataRead = engine.GetDataReader("PCAS_SHENZHEN");

                // read dsdbr data from etalon test

                StringDictionary postburnKeys = new StringDictionary();
                postburnKeys.Add("SCHEMA", "hiberdb");
                postburnKeys.Add("TEST_STAGE", "post burn");
                postburnKeys.Add("SERIAL_NO", dutObject.SerialNumber);
                DatumList postburnData = dataRead.GetLatestResults(postburnKeys, false);

                try
                {
                    if (postburnData != null
                        && postburnData.Count > 0)
                    {
                        string testStatus = postburnData.ReadString("TEST_STATUS");

                        if (!testStatus.ToLower().Contains("pass"))
                        {
                            this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "This DUT is failed at post burn, Terminate mapping testing!", FailModeCategory_Enum.OP);
                        }
                    }
                    else
                    {
                        this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "NO post burn data in PCAS,Terminate mapping testing!", FailModeCategory_Enum.OP);
                    }
                }
                catch (Exception e)
                {
                    this.failModeCheck.RaiseError_Prog_NonParamFail(engine, e.Message, FailModeCategory_Enum.UN);
                }
            }
        }

        protected override void InitInstrs(ITestEngineInit engine, DUTObject dutObject, InstrumentCollection instrs, ChassisCollection chassis)
        {
            foreach (Chassis var in chassis.Values) var.EnableLogging = false;
            foreach (Instrument var in instrs.Values) var.EnableLogging = false;

            if (engine.IsSimulation) return;

            #region light_tower

            /* Set up Serial Binary Comms */
            // _serialPort = buildSerPort(comPort, rate);
            if (LightTowerConfig.GetBoolParam("LightExist"))
            {
                try
                {

                    _serialPort = this.buildSerPort(LightTowerConfig.GetStringParam("ComNum"), LightTowerConfig.GetStringParam("BaudRate"));
                    _serialPort.Close();
                    if (!_serialPort.IsOpen)
                    {
                        _serialPort.Open();
                    }

                    // Add the light tower color.
                    _serialPort.WriteLine(LightTowerConfig.GetStringParam("LightGreen"));
                    _serialPort.Close();
                }
                catch { }

            }
            #endregion

            TecDsdbr = (Inst_Ke2510)instrs["TecDsdbr"];
            TecCase = (IInstType_TecController)instrs["TecCase"];

            if (instrs.Contains("FCUMKI_Source"))
                FCUMKISource = (InstType_ElectricalSource)instrs["FCUMKI_Source"];
            else
                FCUMKISource = null;

            //FCUMKISource = (InstType_ElectricalSource)instrs["FCUMKI_Source"];
            VcckeSource = (InstType_ElectricalSource)instrs["ASIC_VccSource"];
            VeekeSource = (InstType_ElectricalSource)instrs["ASIC_VeeSource"];

            double curMax = LocalTestParamsConfig.GetDoubleParam("FcuSource_CurrentCompliance_A");

            if (FCUMKISource != null)
            {
                FCUMKISource.SetDefaultState();
                FCUMKISource.VoltageSetPoint_Volt = LocalTestParamsConfig.GetDoubleParam("FcuSource_Voltage_V");
                FCUMKISource.CurrentComplianceSetPoint_Amp = LocalTestParamsConfig.GetDoubleParam("FcuSource_CurrentCompliance_A");
                FCUMKISource.CurrentComplianceSetPoint_Amp = curMax;
                FCUMKISource.OutputEnabled = true;
                System.Threading.Thread.Sleep(5000);
            }

            curMax = LocalTestParamsConfig.GetDoubleParam("AsicVccSource_CurrentCompliance_A");
            if (VcckeSource != null)
            {
                VcckeSource.SetDefaultState();
                VcckeSource.VoltageSetPoint_Volt = LocalTestParamsConfig.GetDoubleParam("AsicVccSource_Voltage_V");
                VcckeSource.CurrentComplianceSetPoint_Amp = LocalTestParamsConfig.GetDoubleParam("AsicVccSource_CurrentCompliance_A");
                VcckeSource.CurrentComplianceSetPoint_Amp = curMax;
                VcckeSource.OutputEnabled = true;
            }
            if (VeekeSource != null)
            {
                curMax = LocalTestParamsConfig.GetDoubleParam("AsicVeeSource_CurrentCompliance_A");

                VeekeSource.SetDefaultState();
                VeekeSource.VoltageSetPoint_Volt = LocalTestParamsConfig.GetDoubleParam("AsicVeeSource_Voltage_V");
                VeekeSource.CurrentComplianceSetPoint_Amp = LocalTestParamsConfig.GetDoubleParam("AsicVeeSource_CurrentCompliance_A");
                VeekeSource.CurrentComplianceSetPoint_Amp = curMax;
                VeekeSource.OutputEnabled = true;
            }

            Fcu = (Inst_Fcu2Asic)instrs["Fcu2Asic"];
            Fcu.EnableLogging = false;
            DsdbrUtils.Fcu2AsicInstrumentGroup = new FCU2AsicInstruments();
            DsdbrUtils.Fcu2AsicInstrumentGroup.FcuSource = FCUMKISource;
            DsdbrUtils.Fcu2AsicInstrumentGroup.AsicVccSource = VcckeSource;
            DsdbrUtils.Fcu2AsicInstrumentGroup.AsicVeeSource = VeekeSource;
            DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic = Fcu;
            DsdbrUtils.opticalSwitch = (Inst_Ke2510)instrs["TecDsdbr"];
            DsdbrTuning.OpticalSwitch = (Inst_Ke2510)instrs["TecDsdbr"];
            Measurements.FCU2AsicInstrs = new FCU2AsicInstruments();
            Measurements.FCU2AsicInstrs.Fcu2Asic = Fcu;
            Fcu.SwapWirebond = ParamManager.Conditions.IsSwapWirebond;

            Measurements.MzHead = (IInstType_OpticalPowerMeter)instrs["OpmMz"];

            IInstType_TriggeredOpticalPowerMeter PowerMeter = (IInstType_TriggeredOpticalPowerMeter)instrs["OpmMz"];
            // OpmRef
            //IInstType_TriggeredOpticalPowerMeter OpmReference = (IInstType_OpticalPowerMeter)instrs["OpmRef"];

            PowerMeter.EnableInputTrigger(false);
            PowerMeter.SetDefaultState();
            //progInstrs.OpmReference.SetDefaultState();

            if (!ConfigureFCU2AsicInstruments()) {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "Low Cost Frequency Cal Data can not meet test requirment.", FailModeCategory_Enum.HW);
            }

            TestProgramCore.InitInstrs_Wavemeter(this.failModeCheck, engine, dutObject, instrs, this.LocalTestParamsConfig, this.OpticalSwitchConfig);

            UseLowCostSolution = TestParameterConfig_Map.GetBoolParam("UseLowCostSolution");
            
            ConfigureTecController(engine, TecDsdbr, "TecDsdbr", false, this.TestCondition.DSDBR_TEMP_C);

            ConfigureTecController(engine, TecCase, "TecCase", true, this.TestCondition.CaseTemp);
            //ConfigureFCU2AsicInstruments();
        }

        protected override void InitOpticalSwitchIoLine()
        {
            //if (engine.IsSimulation) return;
            IlmzOpticalSwitchLines.C_Band_DigiIoLine = int.Parse(OpticalSwitchConfig.GetStringParam("C_Band_Filter_IO_Line"));
            IlmzOpticalSwitchLines.C_Band_DigiIoLine_State = OpticalSwitchConfig.GetBoolParam("C_Band_Filter_IO_Line_State");
            IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine = int.Parse(OpticalSwitchConfig.GetStringParam("Dut_Ctap_IO_Line"));
            IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine_State = OpticalSwitchConfig.GetBoolParam("Dut_Ctap_IO_Line_State");
            IlmzOpticalSwitchLines.Dut_Rx_DigiIoLine = int.Parse(OpticalSwitchConfig.GetStringParam("Dut_Rx_IO_Line"));
            IlmzOpticalSwitchLines.Dut_Rx_DigiIoLine_State = OpticalSwitchConfig.GetBoolParam("Dut_Rx_IO_Line_State");
            IlmzOpticalSwitchLines.Dut_Tx_DigiIoLine = int.Parse(OpticalSwitchConfig.GetStringParam("Dut_Tx_IO_LIne"));
            IlmzOpticalSwitchLines.Dut_Tx_DigiIoLine_State = OpticalSwitchConfig.GetBoolParam("Dut_Tx_IO_LIne_State");
            IlmzOpticalSwitchLines.Etalon_Ref_DigiIoLine = int.Parse(OpticalSwitchConfig.GetStringParam("Etalon_Ref_IO_Line"));
            IlmzOpticalSwitchLines.Etalon_Ref_DigiIoLine_State = OpticalSwitchConfig.GetBoolParam("Etalon_Ref_IO_Line_State");
            IlmzOpticalSwitchLines.Etalon_Rx_DigiIoLine = int.Parse(OpticalSwitchConfig.GetStringParam("Etalon_Rx_IO_Line"));
            IlmzOpticalSwitchLines.Etalon_Rx_DigiIoLine_State = OpticalSwitchConfig.GetBoolParam("Etalon_Rx_IO_Line_State");
            IlmzOpticalSwitchLines.Etalon_Tx_DigiIoLine = int.Parse(OpticalSwitchConfig.GetStringParam("Etalon_Tx_IO_Line"));
            IlmzOpticalSwitchLines.Etalon_Tx_DigiIoLine_State = OpticalSwitchConfig.GetBoolParam("Etalon_Tx_IO_Line_State");
            IlmzOpticalSwitchLines.L_Band_DigiIoLine = int.Parse(OpticalSwitchConfig.GetStringParam("L_Band_Filter_IO_Line"));
            IlmzOpticalSwitchLines.L_Band_DigiIoLine_State = OpticalSwitchConfig.GetBoolParam("L_Band_Filter_IO_Line_State");
        }

        /// <summary>
        /// Initialise test modules
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        protected override void InitModules(ITestEngineInit engine, DUTObject dutObject, InstrumentCollection instrs, ChassisCollection chassis)
        {
            engine.SendStatusMsg("Initialise modules");
            this.CaseTempControl_InitModule(engine, "CaseTemp_Mid", 25, TestCondition.CaseTemp);

            this.DeviceTempControl_InitModule(engine, "DsdbrTemperature", TecDsdbr, "TecDsdbr");
            InitMod_Pin_Check(engine, instrs, chassis, dutObject);
            InitMod_OverallMap(engine, instrs, chassis, dutObject);
            InitMod_Supermode(engine, instrs, chassis, dutObject);
            initMod_LockerLeveling(engine, instrs, chassis, dutObject);
            InitMod_Characterise(engine, instrs, chassis, dutObject);
            InitMod_GetDUTLockerRatioData(engine, instrs, chassis, dutObject);
        }

        #endregion

        #region Initialize modules

        /// <summary>
        /// Initial Pin check for hittGB test(check the pins correlation with TCE and Vcc)
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="instrs"></param>
        /// <param name="chassis"></param>
        /// <param name="dutObject"></param>
        private void InitMod_Pin_Check(ITestEngineInit engine, InstrumentCollection instrs, ChassisCollection chassis, DUTObject dutObject)
        {
            double Temperature_LowLimit_Degree = 15.0;
            double Temperature_HighLimit_Degree = 40.0;

            ModuleRun modRun = engine.AddModuleRun("Mod_PinCheck", "Mod_PinCheck", string.Empty);

            modRun.Instrs.Add(instrs);
            modRun.Chassis.Add(chassis);
            modRun.ConfigData.AddString("TIME_STAMP", TestTimeStamp_Start);
            modRun.ConfigData.AddString("LASER_ID", dutObject.SerialNumber);
            modRun.ConfigData.AddDouble("Temperature_LowLimit_Degree", Temperature_LowLimit_Degree);
            modRun.ConfigData.AddDouble("Temperature_HighLimit_Degree", Temperature_HighLimit_Degree);
        }

        private void initMod_LockerLeveling(ITestEngineInit engine, InstrumentCollection instrs, ChassisCollection chassis, DUTObject dutObject)
        {
            ModuleRun modRun = engine.AddModuleRun("LockerLeveling", "LockerLeveling", string.Empty);
            modRun.ConfigData.AddReference("Mapping_Settings", TestParameterConfig_Map);
            modRun.Instrs.Add(instrs);
            modRun.Chassis.Add(chassis);
            modRun.ConfigData.AddString("TIME_STAMP", TestTimeStamp_Start);
            modRun.ConfigData.AddString("LASER_ID", dutObject.SerialNumber);
            modRun.ConfigData.AddString("RESULT_DIR", this.CommonTestConfig.ResultsSubDirectory);
            modRun.ConfigData.AddDouble("RX_TARGET_CURRENT", TestCondition.TxTargetCurrent);
            modRun.ConfigData.AddDouble("MIN_REARSOA", TestCondition.MinRearSoa);
            modRun.ConfigData.AddDouble("MAX_REARSOA", TestCondition.MaxRearSoa);
            modRun.Instrs.Add("Optical_switch", (Instrument)TecDsdbr);

            foreach (ParamLimit testParam in this.MainSpec)
            {
                if (testParam.ExternalName == "TC_TX_TARGET_CURRENT")
                {
                    Max_LockerCurrent_mA = double.Parse(testParam.HighLimit.ValueToString());
                    Min_LockerCurrent_mA = double.Parse(testParam.LowLimit.ValueToString());
                }
            }
            modRun.ConfigData.AddDouble("Max_LockerCurrent_mA", Max_LockerCurrent_mA);
            modRun.ConfigData.AddDouble("Min_LockerCurrent_mA", Min_LockerCurrent_mA);
        }

        //private void InitMod_Characterise(ITestEngineInit engine, object instrs, object chassis, DUTObject dutObject)
        private void InitMod_Characterise(ITestEngineInit engine, InstrumentCollection instrs, ChassisCollection chassis, DUTObject dutObject)
        {
            ModuleRun modRun = engine.AddModuleRun("Mod_Characterise", "Mod_Characterise", string.Empty);


            #region add configure data

            #region Get RTH limit from final limit file - chongjian.liang 2013.4.25

            ILimitRead limitReader = engine.GetLimitReader();
            StringDictionary finalSpecKey = new StringDictionary();
            finalSpecKey.Add("SCHEMA", "HIBERDB");
            finalSpecKey.Add("TEST_STAGE", "final");
            finalSpecKey.Add("DEVICE_TYPE", dutObject.PartCode);

            Specification finalSpec = limitReader.GetLimit(finalSpecKey)[0];

            ParamLimit CH_LASER_RTH = finalSpec.GetParamLimit("CH_LASER_RTH");

            modRun.ConfigData.AddDouble("CH_LASER_RTH_Min", double.Parse(CH_LASER_RTH.LowLimit.ValueToString()));
            modRun.ConfigData.AddDouble("CH_LASER_RTH_Max", double.Parse(CH_LASER_RTH.HighLimit.ValueToString()));
            #endregion

            //bool isDelItuFile_When_MissingLine = TestParameterConfig_Map.GetBoolParam("DelItuFile_When_MissingLine");
            modRun.ConfigData.AddString("TIME_STAMP", TestTimeStamp_Start);
            modRun.ConfigData.AddString("RESULT_DIR", this.CommonTestConfig.ResultsSubDirectory);
            modRun.ConfigData.AddString("LASERID", dutObject.SerialNumber);
            modRun.ConfigData.AddSint32("ChanNbr", TestCondition.ChannelNbr);
            modRun.ConfigData.AddSint32("FrequencyStep", (int)TestCondition.FrequencyStep_GHz);
            modRun.ConfigData.AddDouble("StartFrequency", TestCondition.StartFrequency);
            modRun.ConfigData.AddDouble("FrequencyTolerance_GHz", TestCondition.FrequencyTolerance_GHz);
            modRun.ConfigData.AddString("EQUIP_ID", dutObject.EquipmentID);
            modRun.ConfigData.AddReference("Characterization_Settings", TestParameterConfig_Map);
            modRun.ConfigData.AddBool("USEDUTETALON", useDUTEtalon);
            modRun.ConfigData.AddBool("UseLowCostSolution", UseLowCostSolution);
            modRun.ConfigData.AddDouble("MIN_REARSOA", TestCondition.MinRearSoa);
            modRun.ConfigData.AddDouble("MAX_REARSOA", TestCondition.MaxRearSoa);
            modRun.ConfigData.AddDouble("Target_LockerCurrent", TestCondition.TxTargetCurrent);
            modRun.ConfigData.AddDouble("Max_LockerCurrent_mA", Max_LockerCurrent_mA);
            modRun.ConfigData.AddDouble("Min_LockerCurrent_mA", Min_LockerCurrent_mA);
            modRun.Instrs.Add(instrs);
            modRun.Chassis.Add(chassis);

            //link limits
            Specification spec = MainSpec;
            modRun.Limits.AddParameter(spec, "CG_CHAR_ITU_FILE", "CG_CHAR_ITU_FILE");
            modRun.Limits.AddParameter(spec, "CHAR_ITU_OP_COUNT", "CHAR_ITU_OP_COUNT");
            //modRun.Limits.AddParameter(spec, "CG_ITUEST_POINTS_FILE", "GuessItuFile");
            modRun.Limits.AddParameter(spec, "LASER_RTH", "Laser_Rth");
            #endregion
        }

        private void InitMod_Supermode(ITestEngineInit engine, InstrumentCollection instrs, ChassisCollection chassis, DUTObject dutObject)
        {
            for (int i = 0; i < numberOfSupermodes; i++)
            {
                ModuleRun modRun = engine.AddModuleRun("Mod_Supermode_" + i.ToString(), "Mod_Supermode", string.Empty);
                //modRun.ConfigData.AddReference("Overall_Map_Settings", cfgReader.GetSection("TestModule/MappingSettings/Overall_Map_Settings"));
                string FileName = "Im_DSDBR01_" + dutObject.SerialNumber + "_" + TestTimeStamp_Start + "_SM" + i.ToString() + ".csv";
                modRun.ConfigData.AddSint32("NUM_SUPERMODE", i);
                modRun.ConfigData.AddString("MIDDLE_LINE_PATH", Path.Combine(this.CommonTestConfig.ResultsSubDirectory, FileName));
                modRun.ConfigData.AddString("PHASE_SETUP_FILE_PATH", TestParameterConfig_Map.GetStringParam("PhaseCurrentFile"));
                //modRun.ConfigData.AddReference("Mapping_Settings", TestParameterConfig_Map);
                modRun.ConfigData.AddString("RESULT_DIR", this.CommonTestConfig.ResultsSubDirectory);
                modRun.ConfigData.AddString("TIME_STAMP", TestTimeStamp_Start);
                modRun.ConfigData.AddString("LASER_ID", dutObject.SerialNumber);
                modRun.ConfigData.AddUint32("SUPERMODE_NUM", (long)i);
                modRun.ConfigData.AddString("EQUIP_ID", dutObject.EquipmentID);
                modRun.ConfigData.AddString("SPEC_ID", smSpecName);
                modRun.ConfigData.AddSint32("NODE", dutObject.NodeID);
                modRun.ConfigData.AddBool("USELockerRatioForSM", TestParameterConfig_Map.GetBoolParam("UseLockerRatioFindJumpPoint"));
                modRun.ConfigData.AddBool("USEDUTETALON", useDUTEtalon);
                modRun.ConfigData.AddString("NoFilePath", Nofilepath);
                modRun.ConfigData.AddDouble("locker_tran_pot", locker_pot_value);
                modRun.ConfigData.AddDouble("IgainForMapping_mA", TestCondition.IGAIN_OVERALL_MAP);
                modRun.ConfigData.AddDouble("IrearsoaForMapping_mA", TestCondition.IREARSOA_OVERALL_MAP);
                modRun.Instrs.Add("FCU", Fcu);
                modRun.Instrs.Add("Optical_switch", TecDsdbr);
                modRun.Chassis.Add(chassis);
                if (ParamManager.Conditions.IsSwapWirebond)
                {
                    modRun.ConfigData.AddString("Rear_RearSoa_Switch", "rearsoa");
                }
                else
                {
                    modRun.ConfigData.AddString("Rear_RearSoa_Switch", "rear");
                }
                #region Tie up limits
                //Specification spec=SpecDict["map_sm" + i.ToString()];
                Specification spec = this.SupermodeSpecs[i];

                modRun.Limits.AddParameter(spec, "CG_DUT_FILE_TIMESTAMP", "timestamp");
                modRun.Limits.AddParameter(spec, "CG_LM_LINES_SM_FILE", "cg_lm_lines_sm_file");

                modRun.Limits.AddParameter(spec, "EQUIP_ID", "equipId");

                modRun.Limits.AddParameter(spec, "GAIN_I_SM_MAP", "gain_i_sm_map");

                modRun.Limits.AddParameter(spec, "MATRIX_PRATIO_FWD_MAP_SM", "pRatio_fwd");
                modRun.Limits.AddParameter(spec, "MATRIX_PRATIO_REV_MAP_SM", "pRatio_rev");

                modRun.Limits.AddParameter(spec, "MATRIX_PHOTO1_FWD_MAP_SM", "photo1_fwd");
                modRun.Limits.AddParameter(spec, "MATRIX_PHOTO1_REV_MAP_SM", "photo1_rev");

                modRun.Limits.AddParameter(spec, "MATRIX_PHOTO2_FWD_MAP_SM", "photo2_fwd");
                modRun.Limits.AddParameter(spec, "MATRIX_PHOTO2_REV_MAP_SM", "photo2_rev");

                modRun.Limits.AddParameter(spec, "MODAL_DISTORT_SM", "modal_distort");

                modRun.Limits.AddParameter(spec, "NUM_LM", "num_lm");
                modRun.Limits.AddParameter(spec, "SM_ID", "smID");
                modRun.Limits.AddParameter(spec, "SOA_I_SM_MAP", "sm_i_soa");
                modRun.Limits.AddParameter(spec, "TIME_DATE", "testTimeStamp");
                modRun.Limits.AddParameter(spec, "CG_PASSFAIL_SM_FILE", "cg_passfail_sm_file");
                modRun.Limits.AddParameter(spec, "CG_QAMETRICS_SM_FILE", "cg_qametrics_sm_file");
                modRun.Limits.AddParameter(spec, "SCREEN_RESULT_SM", "screen_result_sm");
                modRun.Limits.AddParameter(spec, "REARSOA_I_SM_MAP", "rearsoa_i_sm");

                #endregion

            }
        }

        //private void InitMod_OverallMap(ITestEngineInit engine, object instrs, object chassis, DUTObject dutObject)
        private void InitMod_OverallMap(ITestEngineInit engine, InstrumentCollection instrs, ChassisCollection chassis, DUTObject dutObject)
        {
            NameValueCollection SupperModeMapDataSource_Etalon = (NameValueCollection)CGcfgReader.GetSection("CloseGridSetting/SuperMap/BoundaryDetection");
            useDUTEtalon = TestParameterConfig_Map.GetBoolParam("UseDUTEtalonData");
            if (Measurements.Wavemeter == null)
            {
                useDUTEtalon = false;
            }
            else
            {
                if (!Measurements.Wavemeter.IsOnline)
                {
                    useDUTEtalon = false; //if no WM force to use optical box but not DUT as Locker ratio data source. jack.zhang 2013-04-15
                }
            }
            ModuleRun modRun = engine.AddModuleRun("Mod_OverallMap", "Mod_OverallMap", string.Empty);
            //modRun.ConfigData.AddReference("Overall_Map_Settings", CGcfgReader.GetSection("CloseGridSetting/OverAllMap/BoundaryDetection"));
            modRun.ConfigData.AddReference("FailModeCheck", this.failModeCheck);
            modRun.ConfigData.AddReference("Mapping_Settings", TestParameterConfig_Map);
            modRun.ConfigData.AddReference("Mzsweep_Settings", TestParameterConfig_Final);
            modRun.ConfigData.AddReference("OpticalCalConfig", LocalTestParamsConfig);
            modRun.ConfigData.AddString("CG_Setting_File", cg_setting_file);
            modRun.ConfigData.AddString("NoFilePath", Nofilepath);
            modRun.ConfigData.AddBool("USEDUTETALON", useDUTEtalon);
            modRun.Instrs.Add(instrs);
            modRun.Chassis.Add(chassis);
            modRun.ConfigData.AddString("TIME_STAMP", TestTimeStamp_Start);
            modRun.ConfigData.AddString("LASER_ID", dutObject.SerialNumber);
            modRun.ConfigData.AddString("RESULT_DIR", this.CommonTestConfig.ResultsSubDirectory);
            modRun.ConfigData.AddDouble("TARGET_POWER_dBm", TestCondition.FiberTargetPower);
            modRun.ConfigData.AddDouble("TARGET_POWER_Tollerance_dB", TestCondition.FiberTargetPowerTollerance);
            modRun.ConfigData.AddDouble("IgainForMapping_mA", TestCondition.IGAIN_OVERALL_MAP);
            modRun.ConfigData.AddDouble("IsoaForMapping_mA", TestCondition.ISOA_OVERALL_MAP);
            modRun.ConfigData.AddDouble("IphaseForMapping_mA", TestCondition.IPHASE_OVERALL_MAP);
            modRun.ConfigData.AddDouble("IrearsoaForMapping_mA", TestCondition.IREARSOA_OVERALL_MAP);
            modRun.ConfigData.AddString("FreqBand", TestCondition.FrequencyBand == 0 ? "C" : "L");

            modRun.ConfigData.AddString("LaserWaferID", ParamManager.Conditions.WAFER_ID);
            if (ParamManager.Conditions.IsSwapWirebond)
            {
                modRun.ConfigData.AddString("Rear_RearSoa_Switch", "rearsoa");
            }
            else
            {
                modRun.ConfigData.AddString("Rear_RearSoa_Switch", "rear");
            }

            modRun.Instrs.Add("Optical_switch", TecDsdbr);
            modRun.Instrs.Add("FCU", Fcu);

            #region Add params to test UPPER_FP_CROSSING & DELTA_FP_CROSSING - chongjian.liang 2013.9.10

            using (CsvReader csvReader = new CsvReader())
            {
                string frontSweepFilePath = this.TestParameterConfig_Map.GetStringParam("FrontCurrentFile");

                int sweepPointsCount = csvReader.ReadFile(frontSweepFilePath).Count;

                modRun.ConfigData.AddSint32("FrontSweepPointsCount", sweepPointsCount);
            }

            modRun.ConfigData.AddSint32("FP_LOWER_LIMIT", FP_LOWER_LIMIT);
            modRun.ConfigData.AddSint32("FP_UPPER_LIMIT", FP_UPPER_LIMIT);

            modRun.ConfigData.AddReference("CocResults_UPPER_FP_CROSSING", this.cocResults_UPPER_FP_CROSSING);

            modRun.ConfigData.AddReference("MainSpec", this.MainSpec);
            #endregion

            #region Add limits to test UPPER_FP_CROSSING & DELTA_FP_CROSSING - chongjian.liang 2013.9.10

            for (int fpIndex = FP_LOWER_LIMIT; fpIndex <= FP_UPPER_LIMIT; fpIndex++)
            {
                string limitName_UPPER_FP_CROSSING = string.Format("UPPER_FP{0}_CROSSING", fpIndex);
                string limitName_DELTA_FP_CROSSING = string.Format("DELTA_FP{0}_CROSSING", fpIndex);

                if (this.MainSpec.ParamLimitExists(limitName_UPPER_FP_CROSSING))
                {
                    modRun.Limits.AddParameter(this.MainSpec, limitName_UPPER_FP_CROSSING, limitName_UPPER_FP_CROSSING);
                }
                if (this.MainSpec.ParamLimitExists(limitName_DELTA_FP_CROSSING))
                {
                    modRun.Limits.AddParameter(this.MainSpec, limitName_DELTA_FP_CROSSING, limitName_DELTA_FP_CROSSING);
                }
            }

            #endregion

            // Getting proper pot value for SM scan. - chongjian.liang 2016.08.29
            {
                int PHASE_POINTS_SUM_FOR_SM_SCAN_POT_RECKONING = 6;

                List<float> phasePointsmAList_SMScanPotReckoning = new List<float>(PHASE_POINTS_SUM_FOR_SM_SCAN_POT_RECKONING);

                using (CsvReader csvReader = new CsvReader())
                {
                    string phaseCurrentFilePath = this.TestParameterConfig_Map.GetStringParam("PhaseCurrentFile");

                    List<string[]> phaseCurrentFileContext = csvReader.ReadFile(phaseCurrentFilePath);

                    if (phaseCurrentFileContext.Count < 7)
                    {
                        this.failModeCheck.RaiseError_Prog_NonParamFail(engine, string.Format("The file {0} doesn't have enough data.", phaseCurrentFilePath), FailModeCategory_Enum.SW);
                    }

                    int phasePointStep = phaseCurrentFileContext.Count / (PHASE_POINTS_SUM_FOR_SM_SCAN_POT_RECKONING - 1);

                    for (int i = 0; i < PHASE_POINTS_SUM_FOR_SM_SCAN_POT_RECKONING - 1; i++)
                    {
                        phasePointsmAList_SMScanPotReckoning.Add(float.Parse(phaseCurrentFileContext[i * phasePointStep][0]));
                    }

                    phasePointsmAList_SMScanPotReckoning.Add(float.Parse(phaseCurrentFileContext[phaseCurrentFileContext.Count - 1][0]));
                }

                modRun.ConfigData.AddReference("PhasePointsmAList_SMScanPotReckoning", phasePointsmAList_SMScanPotReckoning);
            }

            Specification spec = MainSpec;
            modRun.Limits.AddParameter(spec, "NUM_SM", "SM_NUM");
            modRun.Limits.AddParameter(spec, "REF_FIBRE_POWER", "Ref_fiber_power");
            modRun.Limits.AddParameter(spec, "REF_FREQ", "calFreq_Ghz");
            modRun.Limits.AddParameter(spec, "REF_MZ_VON_LEFT", "von_left");
            modRun.Limits.AddParameter(spec, "REF_MZ_VON_RIGHT", "von_right");
            modRun.Limits.AddParameter(spec, "REF_MZ_VOFF_LEFT", "voff_left");
            modRun.Limits.AddParameter(spec, "REF_MZ_VOFF_RIGHT", "voff_right");
            modRun.Limits.AddParameter(spec, "REF_MZ_SUM", "ref_mz_sum");
            modRun.Limits.AddParameter(spec, "REF_MZ_RATIO", "ref_mz_ratio");
            modRun.Limits.AddParameter(spec, "MZ_MAX_V_BIAS", "max_V_Bias");
            modRun.Limits.AddParameter(spec, "REF_LV_FILE", "fileFullPath");
            modRun.Limits.AddParameter(spec, "ITX_DARK", "ITX_DARK_mA");
            modRun.Limits.AddParameter(spec, "IRX_DARK", "IRX_DARK_mA");
            modRun.Limits.AddParameter(spec, "MZ_MOD_LEFT_DARK_I", "L_dark_mA");
            modRun.Limits.AddParameter(spec, "MZ_MOD_RIGHT_DARK_I", "R_dark_mA");
            modRun.Limits.AddParameter(spec, "TAP_COMP_DARK_I", "tap_comp_dark_i");
            modRun.Limits.AddParameter(spec, "CG_SM_LINES_FILE", "cg_sm_lines_file");
            modRun.Limits.AddParameter(spec, "CG_PRATIO_WL_FILE", "cg_pratio_wl_file");
            modRun.Limits.AddParameter(spec, "CG_PASSFAIL_FILE", "cg_passfail_file");
            modRun.Limits.AddParameter(spec, "CG_QAMETRICS_FILE", "cg_qametrics_file");
            modRun.Limits.AddParameter(spec, "MATRIX_PRATIO_SUMMARY_MAP", "matrix_pratio_summary_map");
            modRun.Limits.AddParameter(spec, "REGISTRY_FILE", "registry_file");
            modRun.Limits.AddParameter(spec, "REF_MZ_IMB_LEFT", "imb_left");
            modRun.Limits.AddParameter(spec, "REF_MZ_IMB_RIGHT", "imb_right");
            modRun.Limits.AddParameter(spec, "SOA_POWER_LEVELING", "SOA_PowerLevel");

            double Max_Isoa_mA = 0;
            double Min_Isoa_mA = 0;
            foreach (ParamLimit testParam in this.MainSpec)
            {
                if (testParam.ExternalName == "SOA_POWER_LEVELING")
                {
                    Max_Isoa_mA = double.Parse(testParam.HighLimit.ValueToString());
                    Min_Isoa_mA = double.Parse(testParam.LowLimit.ValueToString());
                }
            }
            modRun.ConfigData.AddDouble("Max_Isoa_mA", Max_Isoa_mA);
            modRun.ConfigData.AddDouble("Min_Isoa_mA", Min_Isoa_mA);
        }

        private void InitMod_GetDUTLockerRatioData(ITestEngineInit engine, InstrumentCollection instrs, ChassisCollection chassis, DUTObject dutObject)
        {
            NameValueCollection characterizationSitting = (NameValueCollection)CGcfgReader.GetSection("CloseGridSetting/Characterization");
            double FrequencyCheckRange = double.Parse(characterizationSitting["LineartyLastDeltFrequencyGHzJump"]);
            string ResultFileName = Path.Combine(this.CommonTestConfig.ResultsSubDirectory, "FreqToDUTLockerRatio_" + dutObject.SerialNumber + "_" + TestTimeStamp_Start + ".csv");
            ModuleRun modRun = engine.AddModuleRun("Mod_GetDUTLockRatio", "Mod_GetDUTLockRatio", string.Empty);
            modRun.ConfigData.AddString("ResultFileName", ResultFileName);
            modRun.ConfigData.AddDouble("FrequencyCheckRange", FrequencyCheckRange);
            modRun.ConfigData.AddReference("FailModeCheck", this.failModeCheck);
        }

        /// <summary>
        /// Configure a Device Temperature Control module
        /// </summary>
        private void DeviceTempControl_InitModule(ITestEngineInit engine, string moduleName,
            IInstType_SimpleTempControl tecController, string configPrefix)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun(moduleName, "SimpleTempControl", "");

            // Add instrument
            modRun.Instrs.Add("Controller", (Instrument)tecController);

            //load config
            double timeout_S = TestParameterConfig_Final.GetDoubleParam(configPrefix + "_Timeout_S");
            double stabTime_S = TestParameterConfig_Final.GetDoubleParam(configPrefix + "_StabiliseTime_S");
            int updateTime_mS = TestParameterConfig_Final.GetIntParam(string.Format("{0}_UpdateTime_mS", configPrefix));
            double sensorSetPoint_C = TestCondition.DSDBR_TEMP_C;
            double tolerance_C = TestParameterConfig_Final.GetDoubleParam(string.Format("{0}_Tolerance_C", configPrefix));

            // Add config data
            DatumList tempConfig = new DatumList();
            tempConfig.AddDouble("datumMaxExpectedTimeForOperation_s", timeout_S);
            tempConfig.AddDouble("RqdStabilisationTime_s", stabTime_S);
            tempConfig.AddSint32("TempTimeBtwReadings_ms", updateTime_mS);
            tempConfig.AddDouble("SetPointTemperature_C", sensorSetPoint_C);
            tempConfig.AddDouble("TemperatureTolerance_C", tolerance_C);

            modRun.ConfigData.AddListItems(tempConfig);
        }

        /// <summary>
        /// Configure Case Temperature module
        /// </summary>
        private void CaseTempControl_InitModule(ITestEngineInit engine,
            string moduleName, Double currentTemp, Double tempSetPoint)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun(moduleName, "SimpleTempControl", "");

            // Add instrument
            modRun.Instrs.Add("Controller", (Instrument)TecCase);

            // Add config data
            TempTablePoint tempCal = TempConfig.GetTemperatureCal(currentTemp, tempSetPoint)[0];
            DatumList tempConfig = new DatumList();
            int guiTimeOut_ms = int.Parse(TestParameterConfig_Final.GetStringParam("CaseTempGui_UpdateTime_mS"));

            // Timeout = 2 * calibrated time, minimum of 60secs
            double timeout_s = Math.Max(tempCal.DelayTime_s * 15, 60);
            tempConfig.AddDouble("datumMaxExpectedTimeForOperation_s", timeout_s);
            // Stabilisation time = 0.5 * calibrated time
            tempConfig.AddDouble("RqdStabilisationTime_s", tempCal.DelayTime_s);
            tempConfig.AddSint32("TempTimeBtwReadings_ms", guiTimeOut_ms);
            // Actual temperature to set
            tempConfig.AddDouble("SetPointTemperature_C", tempSetPoint + tempCal.OffsetC);
            tempConfig.AddDouble("TemperatureTolerance_C", tempCal.Tolerance_degC);
            modRun.ConfigData.AddListItems(tempConfig);
        }

        #endregion

        #region Configure instruments

        /// <summary>
        /// Configure a TEC controller using config data
        /// </summary>
        /// <param name="tecCtlr">Instrument reference</param>
        /// <param name="tecCtlId">Config table data prefix</param>
        private void ConfigureTecController(ITestEngineBase engine, IInstType_TecController tecCtlr, string tecCtlId, bool isTecCase, double temperatureSetPoint_C)
        {
            SteinhartHartCoefficients stCoeffs = new SteinhartHartCoefficients();

            if (tecCtlr.DriverName.Contains("Ke2510"))
            {
                // Keithley 2510 specific commands (must be done before 'SetDefaultState')
                tecCtlr.HardwareData["MinTemperatureLimit_C"] = TestParameterConfig_Final.GetStringParam(tecCtlId + "_MinTemp_C");
                tecCtlr.HardwareData["MaxTemperatureLimit_C"] = TestParameterConfig_Final.GetStringParam(tecCtlId + "_MaxTemp_C");

                tecCtlr.SetDefaultState();

                tecCtlr.Sensor_Type = (InstType_TecController.SensorType)Enum.Parse(typeof(InstType_TecController.SensorType), TestParameterConfig_Final.GetStringParam(tecCtlId + "_Sensor_Type"));
                
                tecCtlr.TecVoltageCompliance_volt = TestParameterConfig_Final.GetDoubleParam(tecCtlId + "_TecVoltageCompliance_volt");

                if (isTecCase)
                {
                    tecCtlr.DerivativeGain = LocalTestParamsConfig.GetDoubleParam(tecCtlId + "_DerivativeGain");
                }
                else
                {
                    tecCtlr.DerivativeGain = TestParameterConfig_Final.GetDoubleParam(tecCtlId + "_DerivativeGain");
                }
            }
            else
            {
                tecCtlr.SetDefaultState();
            }
			
            tecCtlr.OperatingMode = (InstType_TecController.ControlMode)Enum.Parse(typeof(InstType_TecController.ControlMode), TestParameterConfig_Final.GetStringParam(tecCtlId + "_OperatingMode"));
            
            // Thermistor characteristics
            if (tecCtlr.Sensor_Type == InstType_TecController.SensorType.Thermistor_2wire ||
                tecCtlr.Sensor_Type == InstType_TecController.SensorType.Thermistor_4wire)
            {
                stCoeffs.A = TestParameterConfig_Final.GetDoubleParam(tecCtlId + "_stCoeffs_A");
                stCoeffs.B = TestParameterConfig_Final.GetDoubleParam(tecCtlId + "_stCoeffs_B");
                stCoeffs.C = TestParameterConfig_Final.GetDoubleParam(tecCtlId + "_stCoeffs_C");
                tecCtlr.SteinhartHartConstants = stCoeffs;
            }

            // Additional parameters
            
            //if (tecCtlr.DriverName.Contains("Ke2510"))
            //{
            //    tecCtlr.Sensor_Type = (InstType_TecController.SensorType)
            //    Enum.Parse(typeof(InstType_TecController.SensorType), TestParameterConfig_Final.GetStringParam(tecCtlId + "_Sensor_Type"));
            //    //tecCtlr.SensorCurrent_amp = 0.0025;//Jack.zhang for min the negtive impactiom about hitt coc common ground 2011-10-07
            //}

            // Additional parameters
            if (isTecCase)
            {
                tecCtlr.TecCurrentCompliance_amp = LocalTestParamsConfig.GetDoubleParam(tecCtlId + "_TecCurrentCompliance_amp");
                tecCtlr.ProportionalGain = LocalTestParamsConfig.GetDoubleParam(tecCtlId + "_ProportionalGain");
                tecCtlr.IntegralGain = LocalTestParamsConfig.GetDoubleParam(tecCtlId + "_IntegralGain");
            }
            else
            {
                tecCtlr.TecCurrentCompliance_amp = TestParameterConfig_Final.GetDoubleParam(tecCtlId + "_TecCurrentCompliance_amp");
                tecCtlr.ProportionalGain = TestParameterConfig_Final.GetDoubleParam(tecCtlId + "_ProportionalGain");
                tecCtlr.IntegralGain = TestParameterConfig_Final.GetDoubleParam(tecCtlId + "_IntegralGain");
            }

            try
            {
                tecCtlr.SensorTemperatureSetPoint_C = temperatureSetPoint_C;
            }
            catch (Exception e)
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, e.Message, FailModeCategory_Enum.UN);
            }

        }
        
        // added FCU2ASIC Option by Alice.Huang, for TOSA GB test     2010-02-01
        /// <summary>
        /// initialise FCU2ASIC INSTRUMENTS 
        /// </summary>
        /// <param name="fcu2AsicInstrs">FCU2ASIC instruments group </param>
        private bool ConfigureFCU2AsicInstruments()
        {
    

            bool IsFrequencyCalInTestRange = true;
            string FrequencyCalibration_file_Path = "L_band_FrequencyCalibration_file_Path";
            int band = TestCondition.FrequencyBand;
            if (band == 0)//C band
                FrequencyCalibration_file_Path = "C_band_FrequencyCalibration_file_Path";


            string freqCalFilename = TestParameterConfig_Map.GetStringParam(FrequencyCalibration_file_Path);
            string FCUMKI_Tx_CalFilename = TestParameterConfig_Map.GetStringParam("FCUMKI_Tx_Calibration_file_Path");
            string FCUMKI_Rx_CalFilename = TestParameterConfig_Map.GetStringParam("FCUMKI_Rx_Calibration_file_Path");
            string FCUMKI_Var_CalFilename = TestParameterConfig_Map.GetStringParam("FCUMKI_Var_Calibration_file_Path");
            string FCUMKI_Fix_CalFilename = TestParameterConfig_Map.GetStringParam("FCUMKI_Fix_Calibration_file_Path");

            FreqCalByEtalon.LoadFreqlCalArray(freqCalFilename, TestCondition.CalOffset, TestCondition.FrequencyTolerance_GHz, TestCondition.FrequencyStep_GHz);
            FreqCalByEtalon.UseDUTLockerRatio = TestParameterConfig_Map.GetBoolParam("UseDUTEtalonData"); // chongjian.liang 2016.08.22
            //FreqCalByEtalon.UseDUTLockerRatio = false;//init DUT LR as Optical box data but not Device data for Lowcost backup method. jack.zhang 2012-12-14 // Commented out - chongjian.liang 2016.08.22
            FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Tx = FreqCalByEtalon.LoadFCUMKICalArray(FCUMKI_Tx_CalFilename);
            FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Rx = FreqCalByEtalon.LoadFCUMKICalArray(FCUMKI_Rx_CalFilename);
            FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Var = FreqCalByEtalon.LoadFCUMKICalArray(FCUMKI_Var_CalFilename);
            FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Fix = FreqCalByEtalon.LoadFCUMKICalArray(FCUMKI_Fix_CalFilename);

            double Freq_Cal_Start = FreqCalByEtalon.Filter_Etalon_Data[0][FreqCalByEtalon.Freq_GHz_List_Index];
            double Freq_Cal_End = FreqCalByEtalon.Filter_Etalon_Data[FreqCalByEtalon.Filter_Etalon_Data.Count - 1][FreqCalByEtalon.Freq_GHz_List_Index];
            double Freq_Target_Start = TestCondition.StartFrequency;
            double Freq_Target_End = TestCondition.StartFrequency + TestCondition.FrequencyStep_GHz * (TestCondition.ChannelNbr - 1);
            if (Freq_Target_Start < Freq_Cal_Start || Freq_Target_End > Freq_Cal_End)
                IsFrequencyCalInTestRange = false;
            return IsFrequencyCalInTestRange;

        }
        
        #endregion
        
        #region Program Running

        public override void Run(ITestEngineRun engine, DUTObject dutObject, InstrumentCollection instrs, ChassisCollection chassis)
        {
            InstCommands.CloseInstToLoadDUT(engine, DsdbrUtils.Fcu2AsicInstrumentGroup.FcuSource, DsdbrUtils.Fcu2AsicInstrumentGroup.AsicVccSource, DsdbrUtils.Fcu2AsicInstrumentGroup.AsicVeeSource, this.TecDsdbr);
            
            //engine.ShowContinueUserQuery("Please click the continue button to test,请放置器件,点击 Continue 测试");

            //First do pin check
            engine.RunModule("Mod_PinCheck");
            ModRunData Pin_Check_Results = engine.GetModuleRunData("Mod_PinCheck");
            bool Pin_Check_Pass = Pin_Check_Results.ModuleData.ReadBool("Pin_Check_Pass");
            if (!Pin_Check_Pass)
            {
                this.errorInformation += Pin_Check_Results.ModuleData.ReadString("ErrorInformation");
                return;
            }

            ModuleRun modRun;

            // Asic Part
            //bool Is_ASIC5112 = TestParameterConfig_Final.GetBoolParam("Is_ASIC5112");
            Fcu.SetupChannelCalibration();

            TecCase.SensorTemperatureSetPoint_C = TestCondition.CaseTemp;
            TecCase.OutputEnabled = true;

            Fcu.Inital_Etalon_WaveMter();//to avoid Var pot large change in overallmap FCUMKI pot leveling Jack.Zhang 2011-01-12

            bool GetDataFileOK=getDataFromPreAndPostBurn(engine, dutObject);// Jack.zhang add for LowCost solution update 2012-05-28
            
            #region Temperature control - chongjian.liang 2012.12.3

            // Set and wait for temperatures to stabilise

            ModuleRunReturn caseControlResult = engine.RunModule("CaseTemp_Mid");

            if (caseControlResult.ModuleRunData.ModuleData.IsPresent("ErrorInformation")) {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, caseControlResult.ModuleRunData.ModuleData.ReadString("ErrorInformation"), FailModeCategory_Enum.UN);
            }

            ModuleRunReturn dsdbrControlResult = engine.RunModule("DsdbrTemperature");

            if (dsdbrControlResult.ModuleRunData.ModuleData.IsPresent("ErrorInformation")) {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, dsdbrControlResult.ModuleRunData.ModuleData.ReadString("ErrorInformation"), FailModeCategory_Enum.UN);
            }
            #endregion

            ModRunData overallMapResults;
            do
            {
                ModuleRunReturn moduleRunReturn = engine.RunModule("Mod_OverallMap");

                if (!moduleRunReturn.ModuleRunOk) // Catch the module error - chongjian.liang 2014.11.25
                {
                    this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "Module 'Mod_OverallMap' runs into an error.", FailModeCategory_Enum.UN);
                }

                overallMapResults = engine.GetModuleRunData("Mod_OverallMap");
            }
            while (overallMapResults.ModuleData.ReadBool("RetryModule"));

            this.numberOfSupermodes = overallMapResults.ModuleData.ReadSint32("SM_NUM");
            this.soa_powerLeveling = overallMapResults.ModuleData.ReadDouble("SOA_PowerLevel");

            this.Isoa_SM = (List<double>)overallMapResults.ModuleData.ReadReference("SuperMode_Isoa_mA_List");
            this.Mz_Imb_Left_mA_SM = (List<double>)overallMapResults.ModuleData.ReadReference("SuperMode_Mz_Imb_Left_mA_List");
            this.Mz_Imb_Right_mA_SM = (List<double>)overallMapResults.ModuleData.ReadReference("SuperMode_Mz_Imb_Right_mA_List");
            this.Var_Pot_SM = (List<int>)overallMapResults.ModuleData.ReadReference("SuperMode_Var_Pot_List");
            this.Tx_Pot_SM = (List<int>)overallMapResults.ModuleData.ReadReference("SuperMode_Tx_Pot_List");
            this.Rx_Pot_SM = (List<int>)overallMapResults.ModuleData.ReadReference("SuperMode_Rx_Pot_List");
            dutObject.Attributes.AddSint32("supermodeNumber", numberOfSupermodes);

            if (numberOfSupermodes <= 0)
            {
                this.CommentInformation+="No valid super mode";
                this.errorInformation += overallMapResults.ModuleData.ReadString("ErrorInformation");
                return;
            }
            DatumList QaMetricsFiles = new DatumList();

            for (int i = 0; i < numberOfSupermodes; i++)
            {

                // Specification smSpec = SpecDict["map_sm" + i.ToString()];
                string moduleRunName = "Mod_Supermode_" + i.ToString();
                Specification smSpec = this.SupermodeSpecs[i];
                Fcu.Var_pot = Var_Pot_SM[i];
                Fcu.Locker_tran_pot = Tx_Pot_SM[i];
                Fcu.Locker_refi_pot = Rx_Pot_SM[i];
                modRun = engine.GetModuleRun(moduleRunName);

                modRun.ConfigData.AddDouble("Soa_powerLevel", Isoa_SM[i]);
                modRun.ConfigData.AddDouble("Mz_Imb_Left_mA", Mz_Imb_Left_mA_SM[i]);
                modRun.ConfigData.AddDouble("Mz_Imb_Right_mA", Mz_Imb_Right_mA_SM[i]);

                ModuleRunReturn mrr = engine.RunModule(moduleRunName);
                string ErrorInformation = mrr.ModuleRunData.ModuleData.ReadString("ErrorInformation");
                if (ErrorInformation.Length == 0)
                {
                    CDSDBRSuperMode sm = (CDSDBRSuperMode)mrr.ModuleRunData.ModuleData.ReadReference("SUMPERMODE" + i.ToString());
                    supermodes.Add(sm);

                //get returndata for future data write
                this.CG_ID = mrr.ModuleRunData.ModuleData.ReadString("CGid");
                QaMetricsFiles.AddFileLink(moduleRunName, mrr.ModuleRunData.ModuleData.ReadFileLinkFullPath("cg_qametrics_sm_file"));
                switch (i)
                {
                    case 0: this.MILD_MULTIMODE_SM[i] = mrr.ModuleRunData.ModuleData.ReadSint32("MILD_MULTIMODE_SM0");
                        this.Modal_Distort_SM[i] = mrr.ModuleRunData.ModuleData.ReadDouble("MODAL_DISTORT_SM0");
                        break;
                    case 1: this.MILD_MULTIMODE_SM[i] = mrr.ModuleRunData.ModuleData.ReadSint32("MILD_MULTIMODE_SM1");
                        this.Modal_Distort_SM[i] = mrr.ModuleRunData.ModuleData.ReadDouble("MODAL_DISTORT_SM1");
                        break;
                    case 2: this.MILD_MULTIMODE_SM[i] = mrr.ModuleRunData.ModuleData.ReadSint32("MILD_MULTIMODE_SM2");
                        this.Modal_Distort_SM[i] = mrr.ModuleRunData.ModuleData.ReadDouble("MODAL_DISTORT_SM2");

                            break;
                        case 3: this.MILD_MULTIMODE_SM[i] = mrr.ModuleRunData.ModuleData.ReadSint32("MILD_MULTIMODE_SM3");
                            this.Modal_Distort_SM[i] = mrr.ModuleRunData.ModuleData.ReadDouble("MODAL_DISTORT_SM3");
                            break;
                        case 4:
                            this.MILD_MULTIMODE_SM[i] = mrr.ModuleRunData.ModuleData.ReadSint32("MILD_MULTIMODE_SM4");
                            this.Modal_Distort_SM[i] = mrr.ModuleRunData.ModuleData.ReadDouble("MODAL_DISTORT_SM4");
                            break;
                        case 5:
                            this.MILD_MULTIMODE_SM[i] = mrr.ModuleRunData.ModuleData.ReadSint32("MILD_MULTIMODE_SM5");
                            this.Modal_Distort_SM[i] = mrr.ModuleRunData.ModuleData.ReadDouble("MODAL_DISTORT_SM5");
                            break;
                        case 6:
                            this.MILD_MULTIMODE_SM[i] = mrr.ModuleRunData.ModuleData.ReadSint32("MILD_MULTIMODE_SM6");
                            this.Modal_Distort_SM[i] = mrr.ModuleRunData.ModuleData.ReadDouble("MODAL_DISTORT_SM6");
                            break;
                        case 7:
                            this.MILD_MULTIMODE_SM[i] = mrr.ModuleRunData.ModuleData.ReadSint32("MILD_MULTIMODE_SM7");
                            this.Modal_Distort_SM[i] = mrr.ModuleRunData.ModuleData.ReadDouble("MODAL_DISTORT_SM7");
                            break;
                    }
                }
                else
                {
                    this.CommentInformation += "No valid super mode";
                    this.errorInformation += ErrorInformation;
                    return;
                }
            }
            #region check LM missingline
            //List<string> MissingLineList = new List<string>();
            //string message = "Check Longitude Mode missing line.....";
            //engine.SendToGui(message);
            //bool LongiMode_ok = this.CheckLongitudeMode(QaMetricsFiles, out message, ref MissingLineList);
            //engine.SendToGui(message);
            #endregion check lm missing line
            modRun = engine.GetModuleRun("LockerLeveling");
            modRun.ConfigData.AddSint32("supermodeNumber", numberOfSupermodes);
            modRun.ConfigData.AddBool("GetPrePostDataFileOK", GetDataFileOK);
            
            ModuleRunReturn LockerLevelResults = engine.RunModule("LockerLeveling");
            if (!LockerLevelResults.ModuleRunOk)
            {
                this.errorInformation += "locker leveling fail";
                return;
            }
            slope_rear_rearsoa = new double[numberOfSupermodes, 2];
            slope_rear_rearsoa = (double[,])LockerLevelResults.ModuleRunData.ModuleData.ReadReference("slope_rear_rearsoa");

            modRun = engine.GetModuleRun("Mod_Characterise");
            modRun.ConfigData.AddReference("SUPERMODES", supermodes);
            modRun.ConfigData.AddReference("slope_rear_rearsoa", slope_rear_rearsoa);
            modRun.ConfigData.AddReference("Mz_Imb_Left_mA_List", Mz_Imb_Left_mA_SM);
            modRun.ConfigData.AddReference("Mz_Imb_Right_mA_List", Mz_Imb_Right_mA_SM);
            //modRun.ConfigData.AddStringArray("MissingLineArray", MissingLineList.ToArray());

            ModuleRunReturn modCharacterizeResult = engine.RunModule("Mod_Characterise");

            // Check RTH by final limit file - chongjian.liang 2013.4.25
            if (modCharacterizeResult.ModuleRunData.ModuleData.ReadBool("IsRTHFailed"))
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "RTH kept out of limit in 5 seconds.", FailModeCategory_Enum.PR);
            }
        }

        #endregion
        
        #region End of Program
        /// <summary>
        /// reset instrument after dut test
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        /// <param name="instrs"></param>
        /// <param name="chassis"></param>
        public override void PostRun(ITestEnginePostRun engine, DUTObject dutObject,
                    InstrumentCollection instrs, ChassisCollection chassis)
        {
            // Display temperature progress
            engine.GuiShow();
            engine.GuiToFront();

            // Cleanup & power off
            if (!engine.IsSimulation)
            {
                TestProgramCore.CloseInstrs_Wavemeter();

                InstCommands.CloseInstToUnLoadDUT(engine, this.Fcu, DsdbrUtils.Fcu2AsicInstrumentGroup.FcuSource, this.VcckeSource, this.VeekeSource, this.TecDsdbr, this.TecCase, 25);
               
                #region light_tower

                /* Set up Serial Binary Comms */
                // _serialPort = buildSerPort(comPort, rate);
                if (LightTowerConfig.GetBoolParam("LightExist"))
                {
                    try
                    {

                        _serialPort = this.buildSerPort(LightTowerConfig.GetStringParam("ComNum"), LightTowerConfig.GetStringParam("BaudRate"));
                        _serialPort.Close();
                        if (!_serialPort.IsOpen)
                        {
                            _serialPort.Open();
                        }

                        // Add the light tower color.
                        _serialPort.WriteLine(LightTowerConfig.GetStringParam("LightYellow"));
                        _serialPort.Close();
                    }
                    catch { }

                }
                #endregion
            }
        }

        /// <summary>
        /// Fill in blank TC and CH results
        /// </summary>
        private void Data_TC_CH_Write(ITestEngineDataWrite engine)
        {
            DatumList TraceData = new DatumList();
            foreach (ParamLimit paramLimit in MainSpec)
            {
                if (paramLimit.ExternalName.StartsWith("TC_") || paramLimit.ExternalName.StartsWith("CH_"))
                {
                    if (!TraceData.IsPresent(paramLimit.ExternalName))
                    {
                        Datum dummyValue = null;
                        switch (paramLimit.ParamType)
                        {
                            case DatumType.Double:
                                dummyValue = new DatumDouble(paramLimit.ExternalName, Convert.ToDouble(paramLimit.HighLimit.ValueToString()));
                                break;
                            case DatumType.Sint32:
                                dummyValue = new DatumSint32(paramLimit.ExternalName, Convert.ToInt32(paramLimit.HighLimit.ValueToString()));
                                break;
                            case DatumType.StringType:
                                dummyValue = new DatumString(paramLimit.ExternalName, "MISSING");
                                break;
                            case DatumType.Uint32:
                                dummyValue = new DatumUint32(paramLimit.ExternalName, Convert.ToUInt32(paramLimit.HighLimit.ValueToString()));
                                break;
                        }

                        if (dummyValue != null)
                            TraceData.Add(dummyValue);
                    }
                }
            }
            engine.SetTraceData(this.MainSpecName, TraceData);

        }
        
        /// <summary>
        /// Write Dut test result
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        /// <param name="dutOutcome"></param>
        /// <param name="results"></param>
        /// <param name="userList"></param>
        public override void DataWrite(ITestEngineDataWrite engine, DUTObject dutObject,
                        DUTOutcome dutOutcome, TestData results, UserList userList)
        {
            Data_Ignore_Write(engine);

            Data_TC_CH_Write(engine);

            //Output main teststage
            StringDictionary mainkeys = new StringDictionary();
            mainkeys.Add("SCHEMA", "hiberdb");
            mainkeys.Add("DEVICE_TYPE", dutObject.PartCode);
            if (dutObject.TestStage.ToLower() == "maptest")   //raul added to switch if use MES.
            {
                mainkeys.Add("TEST_STAGE", "hitt_map");
            }
            else
            {
                mainkeys.Add("TEST_STAGE", dutObject.TestStage.ToLower());
            }
            mainkeys.Add("SPECIFICATION", this.MainSpecName);
            mainkeys.Add("SERIAL_NO", dutObject.SerialNumber);
            // Tell Test Engine about it...

            engine.SetDataKeysPerSpec(MainSpecName, mainkeys);


            this.traceDataList = new DatumList();

            this.traceDataList.AddSint32("NODE", dutObject.NodeID);
            traceDataList.AddString("EQUIP_ID", dutObject.EquipmentID);

            double testTime = Math.Round(engine.GetTestTime() / 60, 2);
            double processTime = Math.Round(engine.TestProcessTime / 60, 2);
            double laborTime = testTime - processTime;

            traceDataList.AddString("OPERATOR_ID", userList.InitialUser);
            traceDataList.AddString("COMMENTS", engine.GetProgramRunComments() +this.CommentInformation);
            traceDataList.AddDouble("TEST_TIME", testTime);
            traceDataList.AddDouble("LABOUR_TIME", laborTime);
            traceDataList.AddString("SOFTWARE_ID", dutObject.ProgramPluginName + " SW_version: " + dutObject.ProgramPluginVersion + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + "  " + engine.SpecificTestSolutionId.ToString());
            traceDataList.AddString("PRODUCT_CODE", dutObject.PartCode.ToString());
            traceDataList.AddString("TIME_DATE", TestTimeStamp_Start);
            
            // Zip IPhaseModeAcq files
            IPhaseModeAcqZipFilename = Path.Combine(this.CommonTestConfig.ResultsSubDirectory, "IPhaseModeAcqFiles_" + dutObject.SerialNumber + "_" + TestTimeStamp_Start + ".zip");
            List<string> ForwardMiddleLineFile = CollectFilesToCalIPhaseAcq(dutObject.SerialNumber);
            Util_ZipFile.ZipFiles(IPhaseModeAcqZipFilename, ForwardMiddleLineFile);

            if (File.Exists(IPhaseModeAcqZipFilename))
            {
                traceDataList.AddFileLink("CG_PHASE_MODE_ACQ_FILE", IPhaseModeAcqZipFilename);
            }

            // Zip Map test files
            LowCostMapDataZipFilename = Path.Combine(this.CommonTestConfig.ResultsSubDirectory, "LowCostMapData_" + dutObject.SerialNumber + "_" + TestTimeStamp_Start + ".zip");
            List<string> lowcostfiles = GetFilesToZip(TestParameterConfig_Final.GetStringParam("ZipFileListOnMap"));
            Util_ZipFile.ZipFiles(LowCostMapDataZipFilename, lowcostfiles);
            
            if (File.Exists(LowCostMapDataZipFilename))
            {
                traceDataList.AddFileLink("CG_ITUEST_POINTS_FILE", LowCostMapDataZipFilename);
            }

            if (this.errorInformation != null && (this.errorInformation.Length != 0))
            {
                if (this.errorInformation.Length > 256)
                    this.errorInformation = this.errorInformation.Substring(0, 256);
                traceDataList.AddString("FAIL_ABORT_REASON", this.errorInformation);
            }
            else
            {
                traceDataList.AddString("FAIL_ABORT_REASON", engine.ProgramRunStatus.ToString());
            }

            traceDataList.AddString("CG_ID", this.CG_ID);
            traceDataList.AddString("CG_DUT_FILE_TIMESTAMP", TestTimeStamp_Start);
            traceDataList.AddSint32("MILD_MULTIMODE_SM0", this.MILD_MULTIMODE_SM[0]);
            traceDataList.AddDouble("MODAL_DISTORT_SM0", this.Modal_Distort_SM[0]);
            traceDataList.AddSint32("MILD_MULTIMODE_SM1", this.MILD_MULTIMODE_SM[1]);
            traceDataList.AddDouble("MODAL_DISTORT_SM1", this.Modal_Distort_SM[1]);
            traceDataList.AddSint32("MILD_MULTIMODE_SM2", this.MILD_MULTIMODE_SM[2]);
            traceDataList.AddDouble("MODAL_DISTORT_SM2", this.Modal_Distort_SM[2]);
            traceDataList.AddSint32("MILD_MULTIMODE_SM3", this.MILD_MULTIMODE_SM[3]);
            traceDataList.AddDouble("MODAL_DISTORT_SM3", this.Modal_Distort_SM[3]);
            traceDataList.AddSint32("MILD_MULTIMODE_SM4", this.MILD_MULTIMODE_SM[4]);
            traceDataList.AddDouble("MODAL_DISTORT_SM4", this.Modal_Distort_SM[4]);
            traceDataList.AddSint32("MILD_MULTIMODE_SM5", this.MILD_MULTIMODE_SM[5]);
            traceDataList.AddDouble("MODAL_DISTORT_SM5", this.Modal_Distort_SM[5]);
            traceDataList.AddSint32("MILD_MULTIMODE_SM6", this.MILD_MULTIMODE_SM[6]);
            traceDataList.AddDouble("MODAL_DISTORT_SM6", this.Modal_Distort_SM[6]);
            traceDataList.AddSint32("MILD_MULTIMODE_SM7", this.MILD_MULTIMODE_SM[7]);
            traceDataList.AddDouble("MODAL_DISTORT_SM7", this.Modal_Distort_SM[7]);
            traceDataList.AddString("FACTORY_WORKS_PARTID", dutObject.PartCode);
            //traceDataList.AddString("FACTORY_WORKS_LOTID", dutObject.SerialNumber);
            DatumListAddOrUpdate(ref traceDataList, "FACTORY_WORKS_LOTID", dutObject.BatchID);
            traceDataList.AddString("FACTORY_WORKS_FAILMODE", " ");
            traceDataList.AddString("LASER_WAFER_SIZE", LaserWaferSize);


            //XIAOJIANG 2017.03.20
            if (MainSpec.ParamLimitExists("LOT_TYPE"))
                if (dutObject.Attributes.IsPresent("lot_type"))
                   DatumListAddOrUpdate(ref traceDataList,"LOT_TYPE",dutObject.Attributes.ReadString("lot_type"));
                    //traceDataList.AddString("LOT_TYPE", dutObject.Attributes.ReadString("lot_type"));
                else
                   DatumListAddOrUpdate(ref traceDataList, "LOT_TYPE", "Unknown");
                    //traceDataList.AddString("LOT_TYPE", "Unknown");

            if (MainSpec.ParamLimitExists("FACTORY_WORKS_LOTTYPE"))
                if (dutObject.Attributes.IsPresent("lot_type"))
                    DatumListAddOrUpdate(ref traceDataList, "FACTORY_WORKS_LOTTYPE", dutObject.Attributes.ReadString("lot_type"));
                    //traceDataList.AddString("FACTORY_WORKS_LOTTYPE", dutObject.Attributes.ReadString("lot_type"));
                else
                    DatumListAddOrUpdate(ref traceDataList, "FACTORY_WORKS_LOTTYPE", "Unknown");
                    //traceDataList.AddString("FACTORY_WORKS_LOTTYPE", "Unknown");

            if (MainSpec.ParamLimitExists("LOT_ID"))
                DatumListAddOrUpdate(ref traceDataList, "LOT_ID", dutObject.BatchID);
                //traceDataList.AddString("LOT_ID", dutObject.BatchID);
            if (MainSpec.ParamLimitExists("FACTORY_WORKS_LOTID"))
                DatumListAddOrUpdate(ref traceDataList, "FACTORY_WORKS_LOTID", dutObject.BatchID);
                //traceDataList.AddString("FACTORY_WORKS_LOTID", dutObject.BatchID);
            //END ADD XIAOJIANG 2017.03.20
            if (MainSpec.ParamLimitExists("WAFER_ID"))
            {
                traceDataList.AddString("WAFER_ID", dutObject.Attributes.ReadString("x_wafer_id"));


            }
            if (MainSpec.ParamLimitExists("CHIP_ID"))
            {
                traceDataList.AddString("CHIP_ID", dutObject.Attributes.ReadString("x_chip_id"));

            }
            if (MainSpec.ParamLimitExists("COC_SN"))
            {

                traceDataList.AddString("COC_SN", dutObject.Attributes.ReadString("x_coc_sn"));
            }
            /*
            if (MainSpec.ParamLimitExists("WAFER_ID"))
            {
                traceDataList.AddString("WAFER_ID", ParamManager.Conditions.WAFER_ID);
            }
            if (MainSpec.ParamLimitExists("CHIP_ID"))
            {
                traceDataList.AddString("CHIP_ID", ParamManager.Conditions.CHIP_ID);
            }
            if (MainSpec.ParamLimitExists("COC_SN"))
            {
                traceDataList.AddString("COC_SN", ParamManager.Conditions.COC_SN);
            }*/

            if (engine.GetProgramRunStatus() != ProgramStatus.Success)
            {
                traceDataList.AddString("TEST_STATUS", engine.GetProgramRunStatus().ToString());
            }
            else
            {
                traceDataList.AddString("TEST_STATUS", this.MainSpec.Status.Status.ToString());
                //traceDataList.AddString("TEST_STATUS", engine.GetOverallPassFail().ToString());
            }

            if (MainSpec.ParamLimitExists("RETEST"))
            {
                traceDataList.AddString("RETEST", this.retestCount.ToString());
            }

            if (!dutOutcome.DUTData.IsPresent("RETEST"))
            {
                dutOutcome.DUTData.AddSint32("RETEST", this.retestCount);
            }

            this.failModeCheck.CheckMainSpec(engine, dutObject, this.MainSpec, this.CommonTestConfig.PCASFailMode_FilePath, ref traceDataList);

            // pick the specification to add this data to...
            engine.SetTraceData(MainSpecName, traceDataList);

            string smSpecNameStub = null;
            if (dutObject.Attributes.IsPresent("SupermodeSpecNameStub"))
                smSpecNameStub = dutObject.Attributes.ReadString("SupermodeSpecNameStub");

            foreach (Specification spec in results.Specifications)
            {
                if (spec.Name == this.MainSpecName) continue;
                StringDictionary smKeys = new StringDictionary();

                smKeys.Add("SCHEMA", "HIBERDB");
                smKeys.Add("DEVICE_TYPE", dutObject.PartCode);
                smKeys.Add("SERIAL_NO", dutObject.SerialNumber);

                DatumList traceData = new DatumList();
                traceData.AddSint32("NODE", dutObject.NodeID);


                if (smSpecNameStub.Length > 0 && spec.Name.Contains(smSpecNameStub))
                {
                    int lastUnderscore = spec.Name.LastIndexOf("_");

                    int smNumber = int.Parse(spec.Name.Substring(lastUnderscore + 1, spec.Name.Length - lastUnderscore - 1));

                    smKeys.Add("TEST_STAGE", "map_sm" + smNumber.ToString());
                    smKeys.Add("SPECIFICATION", smSpecName);

                    if (engine.GetProgramRunStatus() != ProgramStatus.Success)
                    {
                        traceData.AddString("TEST_STATUS", engine.GetProgramRunStatus().ToString());
                    }
                    else
                    {
                        traceData.AddString("TEST_STATUS", engine.GetOverallPassFail().ToString());
                    }

                    this.failModeCheck.CheckSubSpec(engine, spec, this.CommonTestConfig.PCASFailMode_FilePath, ref traceData);
                }

                engine.SetDataKeysPerSpec(spec.Name, smKeys);
                engine.SetTraceData(spec.Name, traceData);
            }
        }

        /// <summary>
        /// fill in empty ingore parameters
        /// </summary>
        /// <param name="engine"></param>
        private void Data_Ignore_Write(ITestEngineDataWrite engine)
        {
            string[] ignoreParas = {
                "ITU_OP_EST_COUNT",
                "LASER_AC_RESISTANCE_CHANGE",
                "LASER_TEC_AC_RESISTANCE",
                ////////The belowed items was read from coc in the future
                "ETALON1_FREQ",
                "ETALON1_FS_B",
                "ETALON1_FS_PAIR",
                "ETALON1_PHASE",
                "ETALON1_REAR",
                "ETALON1_REARSOA",
                "I_ETALON_FS_A",
                "I_ETALON_GAIN",
                "I_ETALON_SOA",
                //"LABOUR_TIME",
                "FIBRE_POWER_INITIAL",
                "NUM_MISSING_EST_CHAN",
                //"CG_ITUEST_POINTS_FILE",
                //"LASER_RTH",//temp for add DSDBR_temp in limit file jack.zhang 2012-04-19

            };
            DatumList TraceData = new DatumList();
            foreach (string para in ignoreParas)
            {
                foreach (ParamLimit paramLimit in MainSpec)
                {
                    if (paramLimit.ExternalName == para)
                    {
                        if (!TraceData.IsPresent(paramLimit.ExternalName))
                        {
                            Datum dummyValue = null;
                            switch (paramLimit.ParamType)
                            {
                                case DatumType.Double:
                                    dummyValue = new DatumDouble(paramLimit.ExternalName, Convert.ToDouble(paramLimit.HighLimit.ValueToString()));
                                    break;
                                case DatumType.Sint32:
                                    dummyValue = new DatumSint32(paramLimit.ExternalName, Convert.ToInt32(paramLimit.HighLimit.ValueToString()));
                                    break;
                                case DatumType.StringType:
                                    dummyValue = new DatumString(paramLimit.ExternalName, "No Value");
                                    break;
                                case DatumType.Uint32:
                                    dummyValue = new DatumUint32(paramLimit.ExternalName, Convert.ToUInt32(paramLimit.HighLimit.ValueToString()));
                                    break;
                                case DatumType.FileLinkType:
                                    dummyValue = new DatumFileLink(paramLimit.ExternalName, this.Nofilepath);
                                    break;
                            }
                            if (dummyValue != null)
                                TraceData.Add(dummyValue);
                        }//if
                    }//if (paramLimit.ExternalName == para)
                }//foreach (ParamLimit paramLimit in mainSpec)
            }// foreach (string para in ignoreParas)

            engine.SetTraceData(this.MainSpecName, TraceData);
        }

        #endregion

        protected override void GetCocStageData(ITestEngineInit engine, DUTObject dutObject)
        {
            base.GetCocStageData(engine, dutObject);

            if (this._cocResult != null)
            {
                for (int fpIndex = FP_LOWER_LIMIT; fpIndex <= FP_UPPER_LIMIT; fpIndex++)
                {
                    string limitName_UPPER_FP_CROSSING = string.Format("UPPER_FP{0}_CROSSING", fpIndex);

                    if (this._cocResult.IsPresent(limitName_UPPER_FP_CROSSING))
                    {
                        double limitValue_UPPER_FP_CROSSING = this._cocResult.ReadDouble(limitName_UPPER_FP_CROSSING);

                        this.cocResults_UPPER_FP_CROSSING.Add(limitName_UPPER_FP_CROSSING, limitValue_UPPER_FP_CROSSING);
                    }
                }
            }
        }

        #region Private Helper Functions

        private bool getDataFromPreAndPostBurn(ITestEngineRun engine, DUTObject dutObject)
        {
            bool GetDataFileOK = false;
            double PostBurnTestTemp_C;
            IDataRead dataRead = engine.GetDataReader("PCAS_SHENZHEN");
            DatumList testStatus = null;
            StringDictionary PCASRecorderKeys = new StringDictionary();
            ModuleRun modRun = engine.GetModuleRun("Mod_GetDUTLockRatio");
            try
            {
                PCASRecorderKeys.Add("SCHEMA", "hiberdb");
                PCASRecorderKeys.Add("TEST_STAGE", "post burn");
                PCASRecorderKeys.Add("SERIAL_NO", dutObject.SerialNumber);
                testStatus = dataRead.GetLatestResults(PCASRecorderKeys, true);
                PostBurnTestTemp_C = testStatus.ReadDouble("TC_DSDBR_TEMP");

                if (testStatus != null
                    && testStatus.Count > 0
                    && testStatus.ReadString("TEST_STATUS").ToLower().Contains("pass"))
                {
                    modRun.ConfigData.AddListDatum("PostBurn_testStatus", testStatus);

                    PCASRecorderKeys = new StringDictionary();
                    PCASRecorderKeys.Add("SCHEMA", "hiberdb");
                    PCASRecorderKeys.Add("TEST_STAGE", "pre burn");
                    PCASRecorderKeys.Add("SERIAL_NO", dutObject.SerialNumber);
                    testStatus = dataRead.GetLatestResults(PCASRecorderKeys, true);
                    modRun.ConfigData.AddListDatum("PreBurn_testStatus", testStatus);
                    double Target_Start_Freq_GHz = TestCondition.StartFrequency;
                    double Target_End_Freq_GHz = TestCondition.StartFrequency + TestCondition.FrequencyStep_GHz * (TestCondition.ChannelNbr - 1);
                    modRun.ConfigData.AddDouble("Target_Start_Freq_GHz", Target_Start_Freq_GHz);
                    modRun.ConfigData.AddDouble("Target_End_Freq_GHz", Target_End_Freq_GHz);
                    ModuleRunReturn moduleRunReturn = engine.RunModule("Mod_GetDUTLockRatio");

                    GetDataFileOK = moduleRunReturn.ModuleRunData.ModuleData.ReadBool("GetDataFileOK");
                    if (!GetDataFileOK)
                        this.errorInformation += moduleRunReturn.ModuleRunData.ModuleData.ReadString("ErrorInformation");
                    else
                    {
                        // Commented out - chongjian.liang 2016.08.22
                        //if (useDUTEtalon && TestCondition.DSDBR_TEMP_C == PostBurnTestTemp_C)
                        //    FreqCalByEtalon.UseDUTLockerRatio = true;
                        //else
                        //    FreqCalByEtalon.UseDUTLockerRatio = false;

                        FreqCalByEtalon.DUTLockerRatioSlope_50GHz = (LinearLeastSquaresFit)moduleRunReturn.ModuleRunData.ModuleData.ReadReference("DUTLockerRatioSlope_50GHz");
                        FreqCalByEtalon.DUTLockerRatioSlope_100GHz = (LinearLeastSquaresFit)moduleRunReturn.ModuleRunData.ModuleData.ReadReference("DUTLockerRatioSlope_100GHz");
                    }
                }
                else
                {
                    this.failModeCheck.RaiseError_Prog_NonParamFail(engine, string.Format("No valid post burn scan Data for {0}\n Terminating this test!", dutObject.SerialNumber), FailModeCategory_Enum.OP);
                }
            }
            catch (Exception e)
            {
                //do not use DUT locker ratio if no avilable post and pre buring data(ex. diff DUT temp between pre/post and mapping; old pre/post data)jack.zhang 2012-11-23
                //FreqCalByEtalon.UseDUTLockerRatio = false;// Commented out - chongjian.liang 2016.08.22
            }
            return GetDataFileOK;
        }

        private void ValidateASICType(ITestEngineInit engine, DUTObject dutObject)
        {
            string aaa = Fcu.AsicTypeCheck();
        }

        private void GetTestCondition()
        {
            string[] testCondNames = new string[]
            {
                "TC_NUM_CHAN_REQUIRED",
                "TC_OPTICAL_FREQ_START",
                "TC_FIBRE_TARGET_POWER",
                "TC_FIBRE_TARGET_POWER_TOL",
                "TC_TX_TARGET_CURRENT",
                "TC_REARSOA_MIN",
                "TC_REARSOA_MAX",
                "TC_CAL_OFFSET",
                "TC_T_CASE_MID",
                "TC_FREQ_BAND",
                "TC_OPTICAL_FREQ_STEP",
                "TC_OPTICAL_FREQ_TOL",
                "TC_DSDBR_TEMP",
                "TC_GAIN_I_OVERALL_MAP",
                "TC_SOA_I_OVERALL_MAP",
                "TC_PHASE_I_OVERALL_MAP",
                "TC_REARSOA_I_OVERALL_MAP"
            };
            DatumList testConds = SpecValuesToDatumList.GetTestConditions(this.MainSpec, testCondNames, false);

            foreach (Datum d in testConds)
            {
                string datumName = d.Name;
                switch (datumName)
                {
                    case "TC_NUM_CHAN_REQUIRED":
                        TestCondition.ChannelNbr = ((DatumSint32)d).Value;
                        break;
                    case "TC_OPTICAL_FREQ_START":
                        TestCondition.StartFrequency = ((DatumDouble)d).Value;
                        break;
                    case "TC_FIBRE_TARGET_POWER":
                        TestCondition.FiberTargetPower = ((DatumDouble)d).Value;
                        break;
                    case "TC_FIBRE_TARGET_POWER_TOL":
                        TestCondition.FiberTargetPowerTollerance = ((DatumDouble)d).Value;
                        break;
                    case "TC_TX_TARGET_CURRENT":
                        TestCondition.TxTargetCurrent = ((DatumDouble)d).Value;
                        break;
                    case "TC_REARSOA_MIN":
                        TestCondition.MinRearSoa = ((DatumDouble)d).Value;
                        break;
                    case "TC_REARSOA_MAX":
                        TestCondition.MaxRearSoa = ((DatumDouble)d).Value;
                        break;
                    case "TC_CAL_OFFSET":
                        TestCondition.CalOffset = ((DatumDouble)d).Value;
                        break;
                    case "TC_T_CASE_MID":
                        TestCondition.CaseTemp = ((DatumDouble)d).Value;
                        break;
                    case "TC_FREQ_BAND":
                        TestCondition.FrequencyBand = ((DatumSint32)d).Value;
                        break;
                    case "TC_OPTICAL_FREQ_STEP":
                        TestCondition.FrequencyStep_GHz = ((DatumDouble)d).Value;
                        break;
                    case "TC_OPTICAL_FREQ_TOL":
                        TestCondition.FrequencyTolerance_GHz = ((DatumDouble)d).Value;
                        break;
                    case "TC_DSDBR_TEMP":
                        TestCondition.DSDBR_TEMP_C = ((DatumDouble)d).Value;
                        break;
                    case "TC_GAIN_I_OVERALL_MAP":
                        TestCondition.IGAIN_OVERALL_MAP = ((DatumDouble)d).Value;
                        break;
                    case "TC_SOA_I_OVERALL_MAP":
                        TestCondition.ISOA_OVERALL_MAP = ((DatumDouble)d).Value;
                        break;
                    case "TC_PHASE_I_OVERALL_MAP":
                        TestCondition.IPHASE_OVERALL_MAP = ((DatumDouble)d).Value;
                        break;
                    case "TC_REARSOA_I_OVERALL_MAP":
                        TestCondition.IREARSOA_OVERALL_MAP = ((DatumDouble)d).Value;
                        break;
                }
            }
            TestCondition.StartFrequency = TestCondition.StartFrequency + TestCondition.CalOffset;
        }

        private List<string> CollectFilesToCalIPhaseAcq(string SerialNbr)
        {

            string searchPattern = string.Format("ImiddleLower_LM*_DSDBR01_" +
                 SerialNbr + "_" + this.TestTimeStamp_Start + "_SM?.csv");
            List<string> AbsZipFileList = new List<string>();
            string[] TempZipFiles = Directory.GetFiles(this.CommonTestConfig.ResultsSubDirectory, searchPattern);
            foreach (string AbsFile in TempZipFiles)
            {
                AbsZipFileList.Add(AbsFile);
            }
            return AbsZipFileList;

        }

        private List<string> CollectFilesToZip(DUTObject dutObject)
        {
            string filename = TestParameterConfig_Final.GetStringParam("ZipFileListOnMap");
            
            List<string> ZipFileList=new List<string> ();
            List<string> AbsZipFileList = new List<string>();

            if (File.Exists(filename))
            {
                using (CsvReader csvReader = new CsvReader())
                {
                    List<string[]> lines = csvReader.ReadFile(filename);

                    for (int i = 1; i < lines.Count; i++)
                    {
                        ZipFileList.Add(string.Format(lines[i][0] + dutObject.SerialNumber + "_" + this.TestTimeStamp_Start +lines[i][1]));
                    }
                }

                foreach(string FileSample in ZipFileList)
                {
                    string[] TempZipFiles = Directory.GetFiles(this.CommonTestConfig.ResultsSubDirectory, FileSample);
                  
                    foreach(string AbsFile in TempZipFiles)
                    {
                        AbsZipFileList.Add(AbsFile);
                    }                    
                }
            }

            return AbsZipFileList;
        }

        /* // by tim to combine solution
        /// <summary>
        /// Read instruments calibration data
        /// </summary>
        /// <param name="InstrumentName"></param>
        /// <param name="InstrumentSerialNo"></param>
        /// <param name="ParameterName"></param>
        /// <param name="CalFactor"></param>
        /// <param name="CalOffset"></param>
        private void readCalData(string InstrumentName, string InstrumentSerialNo,
            string ParameterName, out double CalFactor, out double CalOffset)
        {
            StringDictionary keys = new StringDictionary();
            keys.Add("Instrument_name", InstrumentName);
            keys.Add("Parameter", ParameterName);
            if (InstrumentSerialNo != null)
            {
                keys.Add("Instrument_SerialNo", InstrumentSerialNo);
            }
            DatumList calParams = InstrumentsCalData.GetData(keys, false);

            CalFactor = calParams.ReadDouble("CalFactor");
            CalOffset = calParams.ReadDouble("CalOffset");
        }
        */


        private bool CheckLongitudeMode(DatumList QaMetricsFiles, out string message, ref List<string> missingLineMode)
        {   // this module was set forth by roger.li

            bool passFail = true;

            int sm = 0;

            message = "";


            //string[] tmp = new string[2];
            string sSm = "";
            string sLongMode = "";

            double Min_forward_mode_width_low = TestParameterConfig_Map.GetDoubleParam("Min_forward_mode_width_low");//5
            double Min_reverse_mode_width_low = TestParameterConfig_Map.GetDoubleParam("Min_reverse_mode_width_low");//5
            double Min_reverse_mode_width_up = TestParameterConfig_Map.GetDoubleParam("Min_reverse_mode_width_up");//13
            double Min_forward_mode_width_up = TestParameterConfig_Map.GetDoubleParam("Min_forward_mode_width_up");//13
            double Mean_reverse_mode_width_up = TestParameterConfig_Map.GetDoubleParam("Mean_reverse_mode_width_up");//14
            double Mean_forward_mode_width_up = TestParameterConfig_Map.GetDoubleParam("Mean_forward_mode_width_up");//14

            int indexStart = 5;

            for (int i = 0; i < QaMetricsFiles.Count - 1 && sm < numberOfSupermodes - 1; i++, sm++)
            {

                CsvReader csvReader = new CsvReader();

                string smModuleRunName = "Mod_Supermode_" + sm.ToString();

                string fileFullPath = QaMetricsFiles.ReadFileLinkFullPath(smModuleRunName);

                if (File.Exists(fileFullPath))
                {

                    List<string[]> records = csvReader.ReadFile(fileFullPath);
                    for (int record = 9; record < records.Count; record++)
                    {
                        if (records[record].Length > 8)
                        {
                            int longitudeIndex = Convert.ToInt32(records[record][1].Trim());

                            string function = records[record][2].Trim();
                            double minReverse_Mode_Width = 0;
                            double minForward_Mode_Width = 0;
                            double MeanForward_Mode_Width = 0;
                            double MeanReverse_Mode_Width = 0;
                            if (sm == 0)
                            {
                                if (longitudeIndex >= indexStart && string.Compare("Mean", function) == 0)
                                {
                                    MeanForward_Mode_Width = double.Parse(records[record][8]);
                                    MeanReverse_Mode_Width = double.Parse(records[record][9]);

                                    if (MeanReverse_Mode_Width > Mean_reverse_mode_width_up)// read from configer, check reverse LongitudeMode width
                                    {
                                        passFail = false;

                                        message += string.Format("Mean Reverse Mode Width= {3} > {0} at {1} longitude mode in super mode {2}", Mean_reverse_mode_width_up, longitudeIndex, sm, MeanReverse_Mode_Width);

                                        sSm = sm.ToString();
                                        sLongMode = longitudeIndex.ToString();
                                        string sTmp = sSm + "_" + sLongMode;
                                        if (!missingLineMode.Contains(sTmp))
                                        {
                                            missingLineMode.Add(sTmp);
                                        }


                                    }
                                    if (MeanForward_Mode_Width > Mean_forward_mode_width_up) //after reverse width ok, check forward LongitudeMode width
                                    {
                                        passFail = false;

                                        message += string.Format("Mean Forward Mode Width= {3} > {0} at {1} longitude mode in super mode {2}", Mean_forward_mode_width_up, longitudeIndex, sm, MeanForward_Mode_Width);

                                        sSm = sm.ToString();
                                        sLongMode = longitudeIndex.ToString();
                                        string sTmp = sSm + "_" + sLongMode;
                                        if (!missingLineMode.Contains(sTmp))
                                        {
                                            missingLineMode.Add(sTmp);
                                        }

                                        // break;
                                    }
                                }

                                if (longitudeIndex >= indexStart && string.Compare("Minimum", function) == 0)
                                {
                                    minReverse_Mode_Width = double.Parse(records[record][8]);
                                    minForward_Mode_Width = double.Parse(records[record][9]);
                                    //double MinModeWidth = 5;// read from configer
                                    if (minReverse_Mode_Width < Min_forward_mode_width_low)
                                    {
                                        passFail = false;
                                        message += string.Format("Min Forward Mode Width= {3} < {0} at {1} longitude mode in super mode {2}", Min_forward_mode_width_low, longitudeIndex, sm, minReverse_Mode_Width);

                                        sSm = sm.ToString();
                                        sLongMode = longitudeIndex.ToString();
                                        string sTmp = sSm + "_" + sLongMode;
                                        if (!missingLineMode.Contains(sTmp))
                                        {
                                            missingLineMode.Add(sTmp);
                                        }

                                        //break;
                                    }
                                    if (minForward_Mode_Width < Min_reverse_mode_width_low)
                                    {
                                        passFail = false;

                                        message += string.Format("Min Reverse Mode Width= {3} < {0} at {1} longitude mode in super mode {2}", Min_reverse_mode_width_low, longitudeIndex, sm, minForward_Mode_Width);

                                        sSm = sm.ToString();
                                        sLongMode = longitudeIndex.ToString();
                                        string sTmp = sSm + "_" + sLongMode;
                                        if (!missingLineMode.Contains(sTmp))
                                        {
                                            missingLineMode.Add(sTmp);
                                        }

                                        // break;
                                    }

                                }
                            }
                            else
                            {
                                if (string.Compare("Minimum", function) == 0)
                                {
                                    minReverse_Mode_Width = double.Parse(records[record][8]);
                                    minForward_Mode_Width = double.Parse(records[record][9]);

                                    //double MinModeWidth_2 = 5;// read from configer
                                    if (minForward_Mode_Width < Min_forward_mode_width_low)
                                    {
                                        passFail = false;

                                        message += string.Format("Min Forward Mode Width= {3} < {0} at {1} longitude mode in super mode {2}", Min_forward_mode_width_low, longitudeIndex, sm, minForward_Mode_Width);

                                        sSm = sm.ToString();
                                        sLongMode = longitudeIndex.ToString();
                                        string sTmp = sSm + "_" + sLongMode;
                                        if (!missingLineMode.Contains(sTmp))
                                        {
                                            missingLineMode.Add(sTmp);
                                        }
                                        //break;
                                    }
                                    if (minReverse_Mode_Width < Min_reverse_mode_width_low)
                                    {
                                        passFail = false;

                                        message += string.Format("Min Reverse Mode Width= {3} < {0} at {1} longitude mode in super mode {2}", Min_reverse_mode_width_low, longitudeIndex, sm, minReverse_Mode_Width);

                                        sSm = sm.ToString();
                                        sLongMode = longitudeIndex.ToString();
                                        string sTmp = sSm + "_" + sLongMode;
                                        if (!missingLineMode.Contains(sTmp))
                                        {
                                            missingLineMode.Add(sTmp);
                                        }
                                        // break;
                                    }
                                }

                                if ((longitudeIndex == 0) && (string.Compare("Minimum", function) == 0))
                                {
                                    minReverse_Mode_Width = double.Parse(records[record][8]);
                                    minForward_Mode_Width = double.Parse(records[record][9]);

                                    //double MinModeWidth_1 = 13;// read from configer
                                    if (minForward_Mode_Width > Min_forward_mode_width_up)
                                    {
                                        passFail = false;

                                        message += string.Format("Min Forward Mode Width= {3} > {0} at {1} longitude mode in super mode {2}", Min_forward_mode_width_up, longitudeIndex, sm, minForward_Mode_Width);

                                        sSm = sm.ToString();
                                        sLongMode = longitudeIndex.ToString();
                                        string sTmp = sSm + "_" + sLongMode;
                                        if (!missingLineMode.Contains(sTmp))
                                        {
                                            missingLineMode.Add(sTmp);
                                        }

                                        // break;
                                    }
                                    if (minReverse_Mode_Width > Min_reverse_mode_width_up)
                                    {
                                        passFail = false;

                                        message += string.Format("Min Reverse Mode Width= {3} > {0} at {1} longitude mode in super mode {2}", Min_reverse_mode_width_up, longitudeIndex, sm, minReverse_Mode_Width);

                                        sSm = sm.ToString();
                                        sLongMode = longitudeIndex.ToString();
                                        string sTmp = sSm + "_" + sLongMode;
                                        if (!missingLineMode.Contains(sTmp))
                                        {
                                            missingLineMode.Add(sTmp);
                                        }
                                        //break;
                                    }
                                }
                                if ((longitudeIndex > 0) && (string.Compare("Mean", function) == 0))
                                {

                                    MeanForward_Mode_Width = double.Parse(records[record][8]);
                                    MeanReverse_Mode_Width = double.Parse(records[record][9]);
                                    //double MeanModeWidth_1 = 14;//read
                                    if (MeanForward_Mode_Width > Mean_forward_mode_width_up)
                                    {
                                        passFail = false;

                                        message += string.Format("Mean Forward Mode Width= {3} > {0} at {1} longitude mode in super mode {2}", Mean_forward_mode_width_up, longitudeIndex, sm, MeanForward_Mode_Width);

                                        sSm = sm.ToString();
                                        sLongMode = longitudeIndex.ToString();
                                        string sTmp = sSm + "_" + sLongMode;
                                        if (!missingLineMode.Contains(sTmp))
                                        {
                                            missingLineMode.Add(sTmp);
                                        }
                                        //break;
                                    }
                                    if (MeanReverse_Mode_Width > Mean_reverse_mode_width_up)
                                    {
                                        passFail = false;

                                        message += string.Format("Mean Reverse Mode Width= {3} > {0} at {1} longitude mode in super mode {2}", Mean_reverse_mode_width_up, longitudeIndex, sm, MeanReverse_Mode_Width);

                                        sSm = sm.ToString();
                                        sLongMode = longitudeIndex.ToString();
                                        string sTmp = sSm + "_" + sLongMode;
                                        if (!missingLineMode.Contains(sTmp))
                                        {
                                            missingLineMode.Add(sTmp);
                                        }
                                        //break;
                                    }
                                }
                            }

                        }

                    }

                }

                else
                {
                    throw new FileNotFoundException(fileFullPath + "was not found.");
                }
            }
            return passFail;
        }

        private struct TestConditionType
        {
            public int ChannelNbr;
            public double StartFrequency;
            public double FiberTargetPower;
            public double FiberTargetPowerTollerance;
            public double TxTargetCurrent;
            public double MinRearSoa;
            public double MaxRearSoa;
            public double CalOffset;
            public double CaseTemp;
            public int FrequencyBand;
            public double FrequencyStep_GHz;
            public double FrequencyTolerance_GHz;
            public double DSDBR_TEMP_C;
            public double IGAIN_OVERALL_MAP;
            public double ISOA_OVERALL_MAP;
            public double IPHASE_OVERALL_MAP;
            public double IREARSOA_OVERALL_MAP;
            

        }

        private string CheckItuFilePath(string ituFile, string node)
        {
            if (!ituFile.ToLower().Contains("unknown")) return ituFile; // vlaid path, return directly
            string filename = ituFile.Substring(ituFile.LastIndexOf("\\") + 1);
            string strYear = filename.Substring(filename.LastIndexOf('_') + 1, 4);
            string strMonth = filename.Substring(filename.LastIndexOf('_') + 5, 2);
            string strYearMonth = filename.Substring(filename.LastIndexOf('_') + 1, 6);
            string strYearMonthDay = filename.Substring(filename.LastIndexOf('_') + 1, 8);
            string nodeNumber = "Node" + node;


            string serverName = "";
            switch (strYear)
            {
                case "2011":
                    serverName = "\\\\szn-mcad-01\\Archive2010.08";
                    break;
                case "2009":
                    serverName = "\\\\szn-mcad-01\\Archive2009";
                    break;

                case "2008":
                    serverName = "\\\\szn-mcad-01\\Archive2008";
                    break;
                case "2010":
                    if (int.Parse(strMonth) <= 5) serverName = "\\\\szn-sfl-04\\Archive2010";
                    else
                        if (int.Parse(strMonth) >= 6 && int.Parse(strMonth) <= 8) serverName = "\\\\szn-sfl-04\\Archive2010.06";
                        else serverName = "\\\\szn-mcad-01\\Archive2010.08";
                    break;
            }
            string itufilename = Path.Combine(serverName, strYear);
            itufilename = Path.Combine(itufilename, strYearMonth);
            itufilename = Path.Combine(itufilename, nodeNumber);
            itufilename = Path.Combine(itufilename, strYearMonthDay);
            itufilename = Path.Combine(itufilename, filename);
            if (File.Exists(itufilename))
                return itufilename;
            else
                return null;
        }

        #endregion
    }
}


using System;
using System.Collections.Generic;
using System.Text;

namespace Prog_ILMZ_Qual_ND
{
    internal class progIlmzQualInstruments
    {
        
        /// <summary>
        /// FCU
        /// </summary>
        internal FCUInstruments Fcu;

        /// <summary>
        /// FCU2Asic instrument groups
        /// </summary>
        internal FCU2AsicInstruments Fcu2AsicInstrsGroups;

        /// <summary>
        /// MZs
        /// </summary>
        internal IlMzInstruments Mz;

        /// <summary>
        /// OpmRef
        /// </summary>
        internal IInstType_OpticalPowerMeter OpmReference;

        /// <summary>
        /// OSA
        /// </summary>
        internal IInstType_OSA Osa;

        /// <summary>
        /// WM
        /// </summary>
        internal IInstType_Wavemeter Wavemeter;

        /// <summary>
        /// Switch path manager
        /// </summary>
        internal Util_SwitchPathManager SwitchPathManager;

        /// <summary>
        /// Switch between OSA and Opm
        /// </summary>
        internal Switch_Osa_MzOpm SwitchOsaMzOpm;

        /// <summary>
        /// DSDBR Tec
        /// </summary>
        internal IInstType_TecController TecDsdbr;
        /// <summary>
        /// Mz Tec
        /// </summary>
        internal IInstType_TecController TecMz;
        /// <summary>
        /// Case Tec
        /// </summary>
        internal IInstType_TecController TecCase;
    }
}

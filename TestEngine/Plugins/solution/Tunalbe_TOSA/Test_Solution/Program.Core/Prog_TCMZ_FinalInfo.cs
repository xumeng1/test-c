using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.Framework.Limits;
using Bookham.TestLibrary.Utilities;
using Bookham.TestSolution.TcmzFinalData;
using Bookham.TestSolution.TcmzCommonUtils;
using Bookham.TestEngine.Config;
using Bookham.TestSolution.TcmzCommonData;

namespace Bookham.TestSolution.TestPrograms
{
    public class Prog_TCMZ_FinalInfo
    {
        public const int MaxSupermodes = 8;

        /// <summary>
        /// Remember the specification name we are using in the program
        /// </summary>
        public Specification MainSpec;
        //internal SpecList SupermodeSpecs;

        internal TcmzFinalTestConds TestConditions;

        internal ProgTcmzFinalInstruments Instrs;

        internal TestParamConfigAccessor TestParamsConfig;

        internal TempTableConfigAccessor TempConfig;

        internal ConfigDataAccessor InstrumentsCalData;

        internal TestSelection TestSelect;

        internal FinalTestStage TestStage;

        internal DsdbrDriveInstruments DsdbrInstrsUsed;
    }
}
using System;
using System.IO;
using Bookham.TestLibrary.Utilities;
using Bookham.TestEngine.PluginInterfaces.Program;

namespace Bookham.Program.Core
{
    public class CommonTestConfig
    {
        public string ResultsSubDirectory
        {
            get
            {
                return this.resultsSubDirectory;
            }
        }

        public string ResultsDirectory
        {
            get
            {
                return this.resultsDirectory;
            }
        }

        private string resultsArchiveDirectory;

        public string ResultsArchiveDirectory
        {
            get
            {
                return this.resultsArchiveDirectory;
            }
        }

        public string PCASFailMode_FilePath
        {
            get
            {
                return this.pcasFailMode_FilePath;
            }
        }

        public CommonTestConfig(DUTObject dutObject, string testTimeStamp_Start)
        {
            TestParamConfigAccessor configAccessor = new TestParamConfigAccessor(dutObject, CONFIG_PATH, "", "TestConfig");

            this.resultsArchiveDirectory = configAccessor.GetStringParam("ResultsArchiveDirectory");
            this.resultsDirectory = configAccessor.GetStringParam("ResultsDirectory");
            this.resultsSubDirectory = this.resultsDirectory + "\\" + dutObject.SerialNumber + "_" + testTimeStamp_Start;
            this.pcasFailMode_FilePath = configAccessor.GetStringParam("PCASFailMode_FilePath");

            Directory.CreateDirectory(this.resultsSubDirectory);
        }

        private const string CONFIG_PATH = @"Configuration\TOSA Final\CommonTestConfig.xml";

        private string resultsSubDirectory;
        private string resultsDirectory;
        private string pcasFailMode_FilePath;
    }
}

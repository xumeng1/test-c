using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.IO;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.PluginInterfaces.ExternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.Framework.Limits;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Config;
using Bookham.TestLibrary.Utilities;
using Bookham.TestLibrary.Instruments;
using Bookham.TestSolution.IlmzCommonData;
using Bookham.TestSolution.IlmzCommonUtils;

namespace Bookham.Program.Core
{
    public abstract class TestProgramCore : ITestProgram
    {
        #region ITestProgram Members

        public void InitCode(ITestEngineInit engine, DUTObject dutObject, InstrumentCollection instrs, ChassisCollection chassis)
        {
            this.InitConfig(engine, dutObject);

            this.GetPackagBuildStageData(engine, dutObject);
            this.GetCocStageData(engine, dutObject);
            this.GetEtalonAlignStageData(engine, dutObject);
            this.GetPreBurnStageData(engine, dutObject);

            string testStage = dutObject.TestStage.ToLower();

            bool isValidateMapStage = false;

            if (testStage.Contains("final") || testStage.Contains("hitt_spc") || testStage.Contains("qual") || testStage.Contains("hitt debug01"))
            {
                isValidateMapStage = true;
            }

            if (isValidateMapStage)
            {
                this.GetMapStageData(engine, dutObject);
            }

            this.LoadSpecs(engine, dutObject);

            this.ValidateDsdbrDriveInstrs(engine, dutObject);
            this.ValidateTestStatus(engine, dutObject);
            this.ValidateTestStage(engine, dutObject);
            this.ValidateCocStageData(engine, dutObject);
            this.ValidatePackageBuildStageData(engine, dutObject);
            this.ValidatePreACRStageData(engine, dutObject);
            this.ValidateLensAlignStageData(engine, dutObject);
            this.ValidateLensPostBakeTestStageData(engine, dutObject);
            this.ValidateReceptacleAlignStageData(engine, dutObject);
            this.ValidatePostTempCycleStageData(engine, dutObject);
            this.ValidatePreBurnstageData(engine, dutObject);
            this.ValidateBurnInStageData(engine, dutObject);
            this.ValidatePostBurnStageData(engine, dutObject);
            if (isValidateMapStage)
            {
                this.ValidateMapStageData(engine, dutObject);
            }
            this.ValidateFinalStageData(engine, dutObject);
            //this.VerifyJigTemperauture(engine);
            this.InitInstrs(engine, dutObject, instrs, chassis);
            this.InitUtils(engine);
            this.InitOpticalSwitchIoLine();
            this.InitModules(engine, dutObject, instrs, chassis);
            this.CheckPowerBeforeTesting(engine, dutObject);

            this.retestCount = this.RetestCount(engine, dutObject);
        }

        public abstract void DataWrite(ITestEngineDataWrite engine, DUTObject dutObject, DUTOutcome dutOutcome, TestData results, UserList userList);

        public abstract void PostRun(ITestEnginePostRun engine, DUTObject dutObject, InstrumentCollection instrs, ChassisCollection chassis);

        public abstract void Run(ITestEngineRun engine, DUTObject dutObject, InstrumentCollection instrs, ChassisCollection chassis);

        public abstract Type UserControl { get;}

        #endregion

        #region Virtual methods

        protected virtual void DatumListAddOrUpdate(ref DatumList datumList, string strName,string strValue)
        {
            if (datumList.IsPresent(strName))
            {
                datumList.UpdateString(strName, strValue);
            }
            else
            {
                datumList.AddString(strName, strValue);
            }
        }

        protected virtual void InitConfig(ITestEngineInit engine, DUTObject dutObject)
        {
            this.TestTime_Start = DateTime.Now;
            this.TestTimeStamp_Start = this.TestTime_Start.ToString("yyyyMMddHHmmss");

            this.CommonTestConfig = new CommonTestConfig(dutObject, this.TestTimeStamp_Start);

            // Delete the local results - chongjian.liang 2013.5.20
            Directory.Delete(this.CommonTestConfig.ResultsDirectory, true);
            Directory.CreateDirectory(this.CommonTestConfig.ResultsSubDirectory);

            // Read EQUIP_ID from local config - chongjian.liang 2015.9.6
            dutObject.EquipmentID = this.ReadEquipID(engine, @"Configuration\TOSA Final\EQUIP_ID_Config.xml", dutObject.NodeID.ToString());

            engine.SetMainFormTitle(string.Format("EQUIP_ID: {0}, NODE_ID: {1}", dutObject.EquipmentID, dutObject.NodeID));
        }
        /// <summary>
        /// Created by chongjian.liang 2013.10.16
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        protected virtual void GetPackagBuildStageData(ITestEngineInit engine, DUTObject dutObject)
        {
            IDataRead dataRead = engine.GetDataReader("PCAS_SHENZHEN");

            StringDictionary packageBuildKeys = new StringDictionary();
            packageBuildKeys.Add("SCHEMA", ILMZSchema);
            packageBuildKeys.Add("SERIAL_NO", dutObject.SerialNumber);
            packageBuildKeys.Add("TEST_STAGE", "package build");

            this._packageBuildResult = dataRead.GetLatestResults(packageBuildKeys, false);

            if (this._packageBuildResult != null
                && this._packageBuildResult.Count > 0)
            {
                if (this._packageBuildResult.IsPresent("CHIP_ID"))
                {
                    ParamManager.Conditions.CHIP_ID = this._packageBuildResult.ReadString("CHIP_ID").Trim();
                }
                else
                {
                    ParamManager.Conditions.CHIP_ID = " ";
                }

                if (this._packageBuildResult.IsPresent("WAFER_ID"))
                {
                    ParamManager.Conditions.WAFER_ID = this._packageBuildResult.ReadString("WAFER_ID").Trim();
                }
                else
                {
                    ParamManager.Conditions.WAFER_ID = " ";
                }

                if (this._packageBuildResult.IsPresent("COC_SN"))
                {
                    ParamManager.Conditions.COC_SN = this._packageBuildResult.ReadString("COC_SN").Trim();
                }
                else
                {
                    this.failModeCheck.RaiseError_Prog_NonParamFail(engine, string.Format("No COC_SN in package build stage for " + dutObject.SerialNumber), FailModeCategory_Enum.OP);
                }

                if (this._packageBuildResult.IsPresent("ASIC_TYPE"))
                {
                    ParamManager.Conditions.AsicType = this._packageBuildResult.ReadString("ASIC_TYPE").Trim();
                }
                else
                {
                    this.failModeCheck.RaiseError_Prog_NonParamFail(engine, string.Format("No ASIC_TYPE in package build stage for " + dutObject.SerialNumber), FailModeCategory_Enum.OP);
                }
            }
            else
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, string.Format("No valid package build stage data for " + dutObject.SerialNumber), FailModeCategory_Enum.OP);
            }
        }
        /// <summary>
        /// Created by chongjian.liang 2013.10.16
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        protected virtual void GetCocStageData(ITestEngineInit engine, DUTObject dutObject)
        {
            if (this._packageBuildResult != null)
            {
                if (!string.IsNullOrEmpty(ParamManager.Conditions.COC_SN))
                {
                    IDataRead dataRead = engine.GetDataReader("PCAS_SHENZHEN");

                    StringDictionary cocKeys = new StringDictionary();
                    cocKeys.Add("SCHEMA", "COC");
                    cocKeys.Add("SERIAL_NO", ParamManager.Conditions.COC_SN);
                    cocKeys.Add("TEST_STAGE", "coc_final");

                    this._cocResult = dataRead.GetLatestResults(cocKeys, false);

                    if (this._cocResult != null
                        && this._cocResult.Count > 0)
                    {
                        if (_cocResult.IsPresent("THERMALPHASE"))
                        {
                            ParamManager.Conditions.IsThermalPhase = this._cocResult.ReadString("THERMALPHASE") == "1" ? true : false;
                        }
                        else
                        {
                            ParamManager.Conditions.IsThermalPhase = false;
                        }

                        if (_cocResult.IsPresent("SWAPWIREBOND"))
                        {
                            ParamManager.Conditions.IsSwapWirebond = this._cocResult.ReadString("SWAPWIREBOND") == "1" ? true : false;
                        }
                        else
                        {
                            //engine.ErrorInProgram(string.Format("No SWAPWIREBOND in coc stage for " + dutObject.SerialNumber));
                        }
                    }
                    else
                    {
                        this.failModeCheck.RaiseError_Prog_NonParamFail(engine, string.Format("No valid coc stage data for " + dutObject.SerialNumber), FailModeCategory_Enum.OP);
                    }
                }
            }
        }
        /// <summary>
        /// Created by chongjian.liang 2013.12.01
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        protected virtual void GetEtalonAlignStageData(ITestEngineInit engine, DUTObject dutObject) { }
        /// <summary>
        /// Created by chongjian.liang 2013.10.16
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        protected virtual void GetPreBurnStageData(ITestEngineInit engine, DUTObject dutObject){ }
        /// <summary>
        /// Created by chongjian.liang 2013.10.16
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        protected virtual void GetMapStageData(ITestEngineInit engine, DUTObject dutObject)
        {
            IDataRead dataRead = engine.GetDataReader("PCAS_SHENZHEN");

            StringDictionary mapKeys = new StringDictionary();
            mapKeys.Add("SCHEMA", ILMZSchema);
            mapKeys.Add("SERIAL_NO", dutObject.SerialNumber);
            mapKeys.Add("TEST_STAGE", "hitt_map");

            try
            {
                this._mapResult = dataRead.GetLatestResults(mapKeys, true);
            }
            catch (Exception e)
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "Invalid map stage result for " + dutObject.SerialNumber + "\n" + e.Message, FailModeCategory_Enum.OP);
            }
        }
        /// <summary>
        /// Created by chongjian.liang 2013.10.16
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        protected virtual void LoadSpecs(ITestEngineInit engine, DUTObject dutObject)
        {
            if (this.MainSpec.ParamLimitExists("CH_MZ_CTRL_L_I")
                && this.MainSpec.ParamLimitExists("CH_MZ_CTRL_R_I"))
            {
                // Apply limit from TC_MZ_IMB* to CH_MZ_CTRL_L/R_I
                if (this.MainSpec.ParamLimitExists("TC_MZ_IMB_LEFT_LIMIT_MIN")
                    && this.MainSpec.ParamLimitExists("TC_MZ_IMB_LEFT_LIMIT_MAX")
                    && this.MainSpec.ParamLimitExists("TC_MZ_IMB_RIGHT_LIMIT_MIN")
                    && this.MainSpec.ParamLimitExists("TC_MZ_IMB_RIGHT_LIMIT_MAX"))
                {
                    if (ParamManager.Conditions.IsThermalPhase)
                    {
                        ParamManager.Spec.CH_MZ_CTRL_L_I = new SpecLimit(double.Parse(this.MainSpec.GetParamLimit("TC_MZ_IMB_LEFT_LIMIT_MIN").HighLimit.ValueToString()), double.Parse(this.MainSpec.GetParamLimit("TC_MZ_IMB_LEFT_LIMIT_MAX").HighLimit.ValueToString()));
                        ParamManager.Spec.CH_MZ_CTRL_R_I = new SpecLimit(double.Parse(this.MainSpec.GetParamLimit("TC_MZ_IMB_RIGHT_LIMIT_MIN").HighLimit.ValueToString()), double.Parse(this.MainSpec.GetParamLimit("TC_MZ_IMB_RIGHT_LIMIT_MAX").HighLimit.ValueToString()));
                    }
                    else
                    {
                        ParamManager.Spec.CH_MZ_CTRL_L_I = new SpecLimit(double.Parse(this.MainSpec.GetParamLimit("TC_MZ_IMB_LEFT_LIMIT_MIN").LowLimit.ValueToString()), double.Parse(this.MainSpec.GetParamLimit("TC_MZ_IMB_LEFT_LIMIT_MAX").LowLimit.ValueToString()));
                        ParamManager.Spec.CH_MZ_CTRL_R_I = new SpecLimit(double.Parse(this.MainSpec.GetParamLimit("TC_MZ_IMB_RIGHT_LIMIT_MIN").LowLimit.ValueToString()), double.Parse(this.MainSpec.GetParamLimit("TC_MZ_IMB_RIGHT_LIMIT_MAX").LowLimit.ValueToString()));
                    }
                }
                else // Apply limit of CH_MZ_CTRL_L/R_I to itself
                {
                    ParamManager.Spec.CH_MZ_CTRL_L_I = new SpecLimit(this.MainSpec, "CH_MZ_CTRL_L_I");
                    ParamManager.Spec.CH_MZ_CTRL_R_I = new SpecLimit(this.MainSpec, "CH_MZ_CTRL_R_I");
                }
            }
        }

        /// <summary>
        /// A common function to validate previous test stage's status - chongjian.liang 2016.08.03
        /// </summary>
        protected void ValidatePreviousTestStage(ITestEngineInit engine, DUTObject dutObject, string previousStage)
        {
            StringDictionary key = new StringDictionary();
            key.Add("SCHEMA", ILMZSchema);
            key.Add("SERIAL_NO", dutObject.SerialNumber);
            key.Add("TEST_STAGE", previousStage);

            DatumList result = engine.GetDataReader("PCAS_SHENZHEN").GetLatestResults(key, false);

            if (result == null
                || result.Count == 0)
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, string.Format("Missing {0} stage result.", previousStage), FailModeCategory_Enum.OP);
            }
            else if (!result.ReadString("TEST_STATUS").ToUpper().Contains("PASS"))
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, string.Format("The last {0} stage result is failed.", previousStage), FailModeCategory_Enum.OP);
            }
        }

        protected virtual void ValidateDsdbrDriveInstrs(ITestEngineInit engine, DUTObject dutObject) { }
        protected virtual void ValidateTestStage(ITestEngineInit engine, DUTObject dutObject) { }
        protected virtual void ValidateTestStatus(ITestEngineInit engine, DUTObject dutObject)
        {
            IDataRead dataRead = engine.GetDataReader("PCAS_SHENZHEN");

            StringDictionary resultKeys = new StringDictionary();
            resultKeys.Add("SCHEMA", ILMZSchema);
            resultKeys.Add("SERIAL_NO", dutObject.SerialNumber);
            resultKeys.Add("TEST_STAGE", dutObject.TestStage);

            DatumList result = null;

            try
            {
                result = dataRead.GetLatestResults(resultKeys, false);

            }
            catch { }

            if (result != null
                && result.Count > 0
                && result.IsPresent("TEST_STATUS")
                && result.ReadString("TEST_STATUS").ToLower().Contains("pass")
                && !dutObject.TestStage.ToLower().Contains("spc"))
            {
                ButtonId respond = engine.ShowYesNoUserQuery(string.Format("The {0} test is already passed for {1}, do you want to retest and override the passed result? 序列号为{1}的器件在{0} stage已经passed，是否要重测并覆盖之前passed的结果？", dutObject.TestStage, dutObject.SerialNumber));

                if (respond == ButtonId.No)
                {
                    this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "Test aborted due to the test has already passed!", FailModeCategory_Enum.OP);
                }
            }
        }
        protected virtual void ValidateCocStageData(ITestEngineInit engine, DUTObject dutObject)
        {
            #region Check THERMALPHASE before test - chongjian.liang 2013.6.3

            if (this.MainSpec.ParamLimitExists("THERMALPHASE"))
            {
                ParamLimit limitOfTHERMALPHASE = this.MainSpec.GetParamLimit("THERMALPHASE");

                if (limitOfTHERMALPHASE != null && limitOfTHERMALPHASE.LowLimit != null && limitOfTHERMALPHASE.HighLimit != null)
                {
                    int thermalPhaseCode = ParamManager.Conditions.IsThermalPhase ? 1 : 0;

                    if (thermalPhaseCode < int.Parse(limitOfTHERMALPHASE.LowLimit.ValueToString()) || thermalPhaseCode > int.Parse(limitOfTHERMALPHASE.HighLimit.ValueToString()))
                    {
                        this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "The DUT THERMALPHASE is out of the test limit", FailModeCategory_Enum.PR);
                    }
                }
            }
            #endregion

            #region Check SWAPWIREBOND before test - chongjian.liang 2013.6.3

            if (this.MainSpec.ParamLimitExists("SWAPWIREBOND"))
            {
                ParamLimit limitOfSWAPWIREBOND = this.MainSpec.GetParamLimit("SWAPWIREBOND");

                if (limitOfSWAPWIREBOND != null && limitOfSWAPWIREBOND.LowLimit != null && limitOfSWAPWIREBOND.HighLimit != null)
                {
                    int swapWirebondCode = ParamManager.Conditions.IsSwapWirebond ? 1 : 0;

                    if (swapWirebondCode < int.Parse(limitOfSWAPWIREBOND.LowLimit.ValueToString()) || swapWirebondCode > int.Parse(limitOfSWAPWIREBOND.HighLimit.ValueToString()))
                    {
                        this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "The DUT SWAPWIREBOND is out of the test limit", FailModeCategory_Enum.PR);
                    }
                } 
            }
            #endregion
        }
        protected virtual void ValidatePackageBuildStageData(ITestEngineInit engine, DUTObject dutObject)
        {
            if (this._packageBuildResult != null)
            {
                if (this._packageBuildResult.IsPresent("ASIC_TYPE"))
                {
                    #region Check ASIC_TYPE before test - chongjian.liang 2013.6.3

                    if (this.MainSpec.ParamLimitExists("ASIC_TYPE"))
                    {
                        ParamLimit limitOfASIC_TYPE = this.MainSpec.GetParamLimit("ASIC_TYPE");

                        if (limitOfASIC_TYPE != null && limitOfASIC_TYPE.LowLimit != null && limitOfASIC_TYPE.HighLimit != null)
                        {
                            int asicTypeCode = int.Parse(ParamManager.Conditions.AsicType.Substring(ParamManager.Conditions.AsicType.Length - 4, 4));

                            if (asicTypeCode < int.Parse(limitOfASIC_TYPE.LowLimit.ValueToString()) || asicTypeCode > int.Parse(limitOfASIC_TYPE.HighLimit.ValueToString()))
                            {
                                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, "The DUT ASIC_TYPE is out of the test limit", FailModeCategory_Enum.PR);
                            }
                        } 
                    }
                    #endregion
                }
            }
            else
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, string.Format("No valid package build stage data for " + dutObject.SerialNumber), FailModeCategory_Enum.OP);
            }
        }
        protected virtual void ValidatePreACRStageData(ITestEngineInit engine, DUTObject dutObject) { }
        protected virtual void ValidateLensAlignStageData(ITestEngineInit engine, DUTObject dutObject) { }
        protected virtual void ValidateLensPostBakeTestStageData(ITestEngineInit engine, DUTObject dutObject) { }
        protected virtual void ValidateReceptacleAlignStageData(ITestEngineInit engine, DUTObject dutObject) { }
        protected virtual void ValidatePostTempCycleStageData(ITestEngineInit engine, DUTObject dutObject) { }
        protected virtual void ValidatePreBurnstageData(ITestEngineInit engine, DUTObject dutObject) { }
        protected virtual void ValidateBurnInStageData(ITestEngineInit engine, DUTObject dutObject) { }
        protected virtual void ValidatePostBurnStageData(ITestEngineInit engine, DUTObject dutObject) { }
        protected virtual void ValidateMapStageData(ITestEngineInit engine, DUTObject dutObject) { }
        protected virtual void ValidateFinalStageData(ITestEngineInit engine, DUTObject dutObject) { }
        protected virtual void VerifyJigTemperauture(ITestEngineInit engine) { }
        protected virtual void InitInstrs(ITestEngineInit engine, DUTObject dutObj, InstrumentCollection instrs, ChassisCollection chassis) { }
        protected virtual void InitUtils(ITestEngineInit engine) { }
        protected virtual void InitOpticalSwitchIoLine() { }
        protected virtual void InitModules(ITestEngineInit engine, DUTObject dutObject, InstrumentCollection instrs, ChassisCollection chassis) { }
        protected virtual void CheckPowerBeforeTesting(ITestEngineInit engine, DUTObject dutObject) { }

        #endregion

        #region ReadPcasData

        //private void GetMapStageData(ITestEngineInit engine, DUTObject dutObject)
        //{
        //    //Add read sm file method
        //    string[] PcasResults = GetPcasResult_tcmz_map(engine, dutObject.SerialNumber.Trim());
        //    int smNumber = int.Parse(PcasResults[1]);
        //    int Num_Lm = smNumber;
        //    string[] PcasSmResults = GetSmFile(engine, dutObject.SerialNumber.Trim(), smNumber);
        //    LASER_WAFER_SIZE = PcasResults[2];
        //    SmFiles = GetSmFile(engine, dutObject.SerialNumber.Trim(), smNumber);
        //    NUM_LM = GetNum_LM(engine, dutObject.SerialNumber.Trim(), smNumber, Num_Lm);
        //    //ssNUM_LM = GetNum_LM(engine, dutObject.SerialNumber.Trim(), smNumber, NUM_LM);
        //    foreach (string var in SmFiles)
        //    {
        //        if (!File.Exists(var))
        //            throw new Exception("Can't find the file '" + var + "'");
        //    }
        //}
        
        //public string[] GetSmFile(ITestEngineInit engine,
        //    string SERIAL_NO, int smNumber)
        //{
        //    string[] PcasResults;
        //    bool IsOk;
        //    string[] SmFiles = new string[smNumber];
        //    //string[] NUM_LM = new string[smNumber];

        //    for (int i = 0; i < smNumber; i++)
        //    {
        //        string TEST_STAGE = "final_sm" + i.ToString();
        //        IsOk = ReadPcasResult.TryGetPcasData(engine, "HIBERDB", TEST_STAGE, SERIAL_NO, true,
        //            new string[] { "TEST_STATUS", "CG_QAMETRICS_SM_FILE", "NUM_LM" }, out PcasResults);
        //        if (!IsOk)
        //        {
        //            string errmsg = string.Format("Try Get PcasData Fail. SCHEMA='HIBERDB',TEST_STAGE='{1}',SERIAL_NO={0}", SERIAL_NO, TEST_STAGE);
        //            engine.ErrorInProgram(errmsg);
        //            throw new Exception(errmsg);
        //        }
        //        if (!PcasResults[0].Equals("Pass", StringComparison.InvariantCultureIgnoreCase))
        //        {
        //            //string errmsg = string.Format("Test Result Fail. SCHEMA='HIBERDB',TEST_STAGE='{1}',SERIAL_NO={0}", SERIAL_NO, TEST_STAGE);
        //            //engine.ErrorInProgram(errmsg);
        //            //throw new Exception(errmsg);
        //        }
        //        SmFiles[i] = PcasResults[1];
        //        //NUM_LM[i] = PcasResults[2];
        //    }
        //    return SmFiles;
        //}

        //public string[] GetNum_LM(ITestEngineInit engine,
        //    string SERIAL_NO, int smNumber, int num_Lm)
        //{
        //    string[] PcasResults;
        //    bool IsOk;
        //    string[] NUM_LM = new string[smNumber];

        //    for (int i = 0; i < smNumber; i++)
        //    {
        //        string TEST_STAGE = "final_sm" + i.ToString();
        //        IsOk = ReadPcasResult.TryGetPcasData(engine, "HIBERDB", TEST_STAGE, SERIAL_NO, true,
        //            new string[] { "TEST_STATUS", "CG_QAMETRICS_SM_FILE", "NUM_LM" }, out PcasResults);
        //        if (!IsOk)
        //        {
        //            string errmsg = string.Format("Try Get PcasData Fail. SCHEMA='HIBERDB',TEST_STAGE='{1}',SERIAL_NO={0}", SERIAL_NO, TEST_STAGE);
        //            engine.ErrorInProgram(errmsg);
        //            throw new Exception(errmsg);
        //        }
        //        if (!PcasResults[0].Equals("Pass", StringComparison.InvariantCultureIgnoreCase))
        //        {
        //            //string errmsg = string.Format("Test Result Fail. SCHEMA='HIBERDB',TEST_STAGE='{1}',SERIAL_NO={0}", SERIAL_NO, TEST_STAGE);
        //            //engine.ErrorInProgram(errmsg);
        //            //throw new Exception(errmsg);
        //        }
        //        NUM_LM[i] = PcasResults[2];
        //    }
        //    return NUM_LM;
        //}

        //public string[] GetPcasResult_tcmz_map(ITestEngineInit engine, string SERIAL_NO)
        //{
        //    string[] PcasResults;
        //    bool IsOk = ReadPcasResult.TryGetPcasData(engine, "HIBERDB", "tcmz_map", SERIAL_NO, false,
        //          new string[] { "TEST_STATUS", "NUM_SM", "LASER_WAFER_SIZE" }, out PcasResults);
        //    if (!IsOk)
        //    {
        //        string errmsg = string.Format("Try Get PcasData Fail. SCHEMA='HIBERDB',TEST_STAGE='tcmz_map',SERIAL_NO={0}", SERIAL_NO);
        //        engine.ErrorInProgram(errmsg);
        //        throw new Exception(errmsg);
        //    }
        //    if (!PcasResults[0].Equals("Pass", StringComparison.InvariantCultureIgnoreCase))
        //    {
        //        string errmsg = string.Format("Test Result Fail. SCHEMA='HIBERDB',TEST_STAGE='tcmz_map',SERIAL_NO={0}", SERIAL_NO);
        //        engine.ErrorInProgram(errmsg);
        //        throw new Exception(errmsg);
        //    }
        //    return PcasResults;
        //}
        
        #endregion

        #region Helper functions

        /// <summary>
        /// InitInstrs_Wavemeter - chongjian.liang 2016.08.18
        /// </summary>
        public static void InitInstrs_Wavemeter(FailModeCheck failModeCheck, ITestEngineInit engine, DUTObject dutObject, InstrumentCollection instrs, TestParamConfigAccessor localTestParamsConfig, TestParamConfigAccessor opticalSwitchConfig)
        {
            if (instrs.Contains("WaveMeter"))
            {
                Measurements.Wavemeter = instrs["WaveMeter"] as InstType_SharedWaveMeter;
            }

            if (Measurements.Wavemeter != null)
            {
                bool instShare_IsSharingWaveMeter = localTestParamsConfig.GetBoolParam("InstShare_IsSharingWaveMeter");

                if (instShare_IsSharingWaveMeter)
                {
                    bool instShare_IsMaster = localTestParamsConfig.GetBoolParam("InstShare_IsMaster");
                    Enum_LockSchema instShare_LockSchema = (Enum_LockSchema)Enum.Parse(typeof(Enum_LockSchema), localTestParamsConfig.GetStringParam("InstShare_LockSchema"));
                    string wavemeter_ID = TestProgramCore.Read_Wavemeter_ID(failModeCheck, engine, @"Configuration\TOSA Final\Wavemeter_ID_Config.xml", dutObject.NodeID);

                    if (instShare_IsMaster)
                    {
                        Enum_LineState instShare_LineState_MasterOn = (Enum_LineState)Enum.Parse(typeof(Enum_LineState), opticalSwitchConfig.GetStringParam("InstShare_LineState_MasterOn"));
                        Enum_LineState instShare_LineState_SlaveOn = (Enum_LineState)Enum.Parse(typeof(Enum_LineState), opticalSwitchConfig.GetStringParam("InstShare_LineState_SlaveOn"));
                        int instShare_OpticalSwitch_SwitchSpeed_ms = localTestParamsConfig.GetIntParam("InstShare_OpticalSwitch_SwitchSpeed_ms");

                        if (instShare_LockSchema == Enum_LockSchema.MemoryLock)
                        {
                            Measurements.Wavemeter.TurnOnShare_Master(instrs["TecCase"] as Inst_Ke2510, instShare_LineState_MasterOn, instShare_LineState_SlaveOn, instShare_OpticalSwitch_SwitchSpeed_ms);
                        }
                        else if (instShare_LockSchema == Enum_LockSchema.FileLock)
                        {
                            Measurements.Wavemeter.TurnOnShare_Master(wavemeter_ID, instrs["TecCase"] as Inst_Ke2510, instShare_LineState_MasterOn, instShare_LineState_SlaveOn, instShare_OpticalSwitch_SwitchSpeed_ms);
                        }
                    }
                    else
                    {
                        if (instShare_LockSchema == Enum_LockSchema.MemoryLock)
                        {
                            Measurements.Wavemeter.TurnOnShare_Slave();
                        }
                        else if (instShare_LockSchema == Enum_LockSchema.FileLock)
                        {
                            Measurements.Wavemeter.TurnOnShare_Slave(wavemeter_ID);
                        }
                    }
                }

                Measurements.Wavemeter.IsOnline = true;
                Measurements.Wavemeter.SetDefaultState();
            }
        }

        /// <summary>
        /// CloseInstrs_Wavemeter - chongjian.liang 2016.08.18
        /// </summary>
        public static void CloseInstrs_Wavemeter()
        {
            if (Measurements.Wavemeter != null)
            {
                Measurements.Wavemeter.TurnOffShare();
            }
        }

        /// <summary>
        /// Get RETEST from PCAS result for current stage, and accumulate the RETEST count.
        /// 0 is returned when missing current stage results.
        /// - chongjian.liang 2016.06.14
        /// </summary>
        protected int RetestCount(ITestEngineInit engine, DUTObject dutObject)
        {
            IDataRead dataRead = engine.GetDataReader("PCAS_SHENZHEN");

            StringDictionary keys = new StringDictionary();
            keys.Add("SCHEMA", "HIBERDB");
            keys.Add("TEST_STAGE", dutObject.TestStage);
            keys.Add("SERIAL_NO", dutObject.SerialNumber);

            DatumList pcasResult = dataRead.GetLatestResults(keys, false);

            if (pcasResult == null
                || pcasResult.Count == 0)
            {
                return 0;
            }

            if (!pcasResult.IsPresent("RETEST"))
            {
                this.failModeCheck.RaiseMsg_Prog_NonParamFail(engine, "Missing item RETEST in test specification.", FailModeCategory_Enum.PR);

                return 0;
            }

            int testCount = 0;

            try
            {
                testCount = int.Parse(pcasResult.ReadString("RETEST"));
            }
            catch
            {
                testCount = pcasResult.ReadSint32("RETEST");
            }

            return ++testCount;
        }

        /// <summary>
        /// Read Wavemeter_ID from config file - chongjian.liang 2016.08.26
        /// </summary>
        private static string Read_Wavemeter_ID(FailModeCheck failModeCheck, ITestEngineBase engine, string filePath, int node)
        {
            ConfigDataAccessor config = new ConfigDataAccessor(filePath, "Table_Wavemeter_ID");

            try
            {
                StringDictionary configKeys = new StringDictionary();
                configKeys["Node"] = node.ToString();

                return config.GetData(configKeys, true).ReadString("Wavemeter_ID");
            }
            catch
            {
                failModeCheck.RaiseError_Prog_NonParamFail(engine, string.Format("Couldn't find Wavemeter_ID by [Node: {0}] from file {1}", node, filePath), FailModeCategory_Enum.SW);
            }

            return null;
        }

        /// <summary>
        /// Read EquipID from config file - chongjian.liang 2015.8.27
        /// </summary>
        protected string ReadEquipID(ITestEngineBase engine, string filePath, string nodeID, string jigID)
        {
            ConfigDataAccessor config = new ConfigDataAccessor(filePath, "Table_EquipID");

            try
            {
                StringDictionary configKeys = new StringDictionary();
                configKeys["NodeID"] = nodeID;
                configKeys["JigID"] = jigID;

                return config.GetData(configKeys, true).ReadString("EquipID");
            }
            catch
            {
                this.failModeCheck.RaiseError_Prog_NonParamFail(engine, string.Format("Couldn't find EquipID by [NodeID: {0}, JigID: {1}] from file {2}", nodeID, jigID, filePath), FailModeCategory_Enum.SW);
            }

            return null;
        }

        /// <summary>
        /// Read EquipID from config file - chongjian.liang 2015.8.27
        /// </summary>
        protected string ReadEquipID(ITestEngineBase engine, string filePath, string nodeID)
        {
            return this.ReadEquipID(engine, filePath, nodeID, "N/A");
        }

        /// <summary>
        /// Move this function here for public using. - chongjian.liang 2014.11.18
        /// </summary>
        protected string CheckItuFilePath(string filePath, string node)
        {
            if (!filePath.ToLower().Contains("unknown"))
            {
                return filePath; // valid path, return directly
            }

            string filename = filePath.Substring(filePath.LastIndexOf("\\") + 1);
            string strYear = filename.Substring(filename.LastIndexOf('_') + 1, 4);
            string strMonth = filename.Substring(filename.LastIndexOf('_') + 5, 2);
            string strYearMonth = filename.Substring(filename.LastIndexOf('_') + 1, 6);
            string strYearMonthDay = filename.Substring(filename.LastIndexOf('_') + 1, 8);
            string nodeNumber = "Node" + node;

            string serverName = "\\\\szn-sfl-04\\";

            #region Handle the chaotic archive's addresses. - chongjian.liang 2014.11.18

            string[] ArchiveRootNames ={ string.Format("Archive{0}", strYear), string.Format("Archive{0}", strYearMonth), string.Format("Archive{0}.{1}", strYear, strMonth) };

            string itufilename = null;

            foreach (string archiveRootName in ArchiveRootNames)
            {
                itufilename = Path.Combine(serverName, archiveRootName);
                itufilename = Path.Combine(itufilename, strYear);
                itufilename = Path.Combine(itufilename, strYearMonth);
                itufilename = Path.Combine(itufilename, nodeNumber);
                itufilename = Path.Combine(itufilename, strYearMonthDay);
                itufilename = Path.Combine(itufilename, filename);

                if (File.Exists(itufilename))
                {
                    break;
                }
            }

            // Handle for more chaotic archive's addresses - chongjian.liang 2015.5.21
            if (!File.Exists(itufilename))
            {
                for (int i = 0; i < 100; i++)
                {
                    itufilename = Path.Combine(serverName, string.Format("Archive{0}.{1:D2}", strYear, i));
                    itufilename = Path.Combine(itufilename, strYear);
                    itufilename = Path.Combine(itufilename, strYearMonth);
                    itufilename = Path.Combine(itufilename, nodeNumber);
                    itufilename = Path.Combine(itufilename, strYearMonthDay);
                    itufilename = Path.Combine(itufilename, filename);

                    if (File.Exists(itufilename))
                    {
                        break;
                    }
                }
            }

            #endregion

            return itufilename;
        }

        protected List<string> GetFilesToZip(string filesPatternListPath)
        {
            List<string> filesPatternList = new List<string>();
            List<string> filesToZip = new List<string>();

            if (File.Exists(filesPatternListPath))
            {
                using (CsvReader csvReader = new CsvReader())
                {
                    List<string[]> lines = csvReader.ReadFile(filesPatternListPath);

                    for (int i = 0; i < lines.Count; i++)
                    {
                        filesPatternList.Add(lines[i][0]);
                    }
                }

                foreach (string filesPattern in filesPatternList)
                {
                    filesToZip.AddRange(Directory.GetFiles(this.CommonTestConfig.ResultsDirectory, filesPattern, SearchOption.AllDirectories));
                }
            }
            else
            {
                throw new Exception(string.Format("Cannot get the file {0}, please contact software engineer.", filesPatternListPath));
            }

            return filesToZip;
        }

        #endregion

        protected const string ILMZSchema = "hiberdb";
        protected static bool VerifyJigTemperautureIsOk = false;
        protected static double TEC_LASER_OL_TEMP, TEC_MZ_OL_TEMP, TEC_JIG_CL_TEMP;
        protected string LASER_WAFER_SIZE;
        protected string[] SmFiles;
        protected string[] NUM_LM;
        protected DateTime TestTime_Start;
        protected string TestTimeStamp_Start;
        protected CommonTestConfig CommonTestConfig;
        protected Specification MainSpec;
        protected DatumList _mapResult;
        protected DatumList _cocResult;
        protected DatumList _etalonAlignResult;
        protected DatumList _packageBuildResult;
        protected int retestCount = 0;
        protected FailModeCheck failModeCheck = new FailModeCheck();
    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace Pcas
{
    class ResultTableGroup
    {
        private PcasSchema _schema;
        private string _groupName;

        public PcasSchema Schema
        {
            get
            {
                return _schema;
            }
            set
            {
                _schema = value;
            }
        }

        public string GroupName
        {
            get
            {
                return _groupName;
            }
            set
            {
                _groupName = value;
            }
        }
    }
}

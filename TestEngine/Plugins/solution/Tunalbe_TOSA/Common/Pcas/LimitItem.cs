using System;
using System.Collections.Generic;
using System.Text;
using DbAccess;

namespace Pcas
{
    public enum LimitDataType
    {
        Integer = 1,
        Float = 2,
        String256 = 3,
        String80 = 4,
        String20 = 5,
        String14 = 6,
    }

    public enum PcasResultStatus
    {
        Opened = 0,
        Passed,
        Failed,
        NullValue,
        NotInSpec,
    }

    public class LimitItem 
        //: DbObjectBase
        : DbObjectWithColumn
    {
        #region Private members.
        private string _errorInfo;
        private string _warningInfo;
        #endregion

        #region Key definition
        const string key_Minumum = "MINIMUM";
        const string key_Maximum = "MAXIMUM";
        const string key_Units = "UNITS";
        const string key_Format = "FORMAT";
        const string key_OnFail = "ON_FAIL";
        const string key_DataType = "STORE";
        const string key_Prority = "PRIORITY";
        const string key_Name = "SHORT_DESC";
        const string key_Description = "LONG_DESC";
        const string key_Regrade = "REGRADE";

        static TableSchema _csvSchema = null;
        #endregion

        #region Constructors
        /// <summary>
        /// Static constructor.
        /// </summary>
        static LimitItem()
        {
            TableSchema schema = new TableSchema();
            schema.Add(new TableColumn(0, "SHORT_DESC", typeof(string)));
            schema.Add(new TableColumn(1, "LONG_DESC", typeof(string)));
            schema.Add(new TableColumn(2, "MINIMUM", typeof(decimal)));
            schema.Add(new TableColumn(3, "MAXIMUM", typeof(decimal)));
            schema.Add(new TableColumn(4, "UNITS", typeof(string)));
            schema.Add(new TableColumn(5, "FORMAT", typeof(string)));
            schema.Add(new TableColumn(6, "ON_FAIL", typeof(decimal)));
            schema.Add(new TableColumn(7, "STORE", typeof(decimal)));
            schema.Add(new TableColumn(8, "PRIORITY", typeof(decimal)));
            LimitItem._csvSchema = schema;
        }

        public LimitItem()
        {
        }

        /// <summary>
        /// Construct a LimitItem with item name(short_desc) and datatype(store).
        /// </summary>
        /// <param name="name"></param>
        /// <param name="type"></param>
        public LimitItem(string name, LimitDataType type)
        {
            this.Name = name;
            this.DataType = type;
        }

        /// <summary>
        /// Construct a LimitItem with item name(short_desc);
        /// </summary>
        /// <param name="name">LimitItem name.</param>
        public LimitItem(string name)
        {
            this.Name = name;
            this.DataType = LimitDataType.String256;
        }

        #endregion

        static public string ReNameForTcmzFinalChannel(string originalName)
        {
            string destName = originalName;
            if (destName.Contains("_-5C"))
            {
                string name = destName.Replace("_-5C", "");
                destName = "TLO_" + name;
            }
            else if (destName.Contains("_72C"))
            {
                string name = destName.Replace("_72C", "");
                destName = "THI_" + name;
            }
            else if (destName.Contains("_75C"))
            {
                string name = destName.Replace("_75C", "");
                destName = "THI_" + name;
            }

            if (destName.Contains("_GHz"))
            {
                string name = destName.Replace("_GHz", "");
                destName = name;
            }
            else if (destName.Contains("_mA"))
            {
                string name = destName.Replace("_mA", "");
                destName = name;
            }
            else if (destName.Contains("_dBm"))
            {
                string name = destName.Replace("_dBm", "");
                destName = name;
            }
            else if (destName.Contains("_ohm"))
            {
                string name = destName.Replace("_ohm", "");
                destName = name;
            }
            else if (destName.Contains("_dB"))
            {
                string name = destName.Replace("_dB", "");
                destName = name;
            }
            else if (destName.Contains("_A"))
            {
                string name = destName.Replace("_A", "");
                destName = name;
            }
            else if (destName.Contains("_W"))
            {
                string name = destName.Replace("_W", "");
                destName = name;
            }

            //Abbreviate..
            //Replace 'PhotoCurrent' As '_I_'
            if (destName.Contains("PhotoCurrent"))
            {
                string name = destName.Replace("PhotoCurrent", "_I_");
                destName = name;
            }

            return destName;
        }

        static string[] lookupTable = new string[]
        {
            "GHz",
            "dBm",
            "ohm",
            "mA",
            "dB",
            "A",
            "W",
        };

        public static string GetLimitUnitFromChannelCSVName(string originalName)
        {

            foreach (string item in LimitItem.lookupTable)
            {
                if (originalName.Contains("_" + item))
                {
                    return item;
                }
            }
            return "none";
        }

        #region Public properties
        /// <summary>
        /// Returns default csv file schema;
        /// </summary>
        public static TableSchema CSVSchema
        {
            get
            {
                return LimitItem._csvSchema;
            }
        }

        public LimitDataType DataType
        {
            get
            {
                return (LimitDataType)base.GetDecimal(key_DataType);
            }
            set
            {
                base[key_DataType] = (int)value;
            }
        }

        public string ErrorInfo
        {
            get
            {
                return this._errorInfo;
            }
        }

        public bool HasError
        {
            get
            {
                return !((this._errorInfo == null) || (this._errorInfo.Length == 0));
            }
        }

        public bool HasWarning
        {
            get
            {
                return !((this._warningInfo == null) || (this._warningInfo.Length == 0));
            }
        }

        public string WarningInfo
        {
            get
            {
                return this._warningInfo;
            }
        }

        /// <summary>
        /// Regrade.
        /// </summary>
        public bool Regrade
        {
            get
            {
                object obj = base[key_Regrade];
                if (obj is decimal)
                {
                    return ((int)(decimal)obj) != 0;
                }
                else
                {
                    return false;
                }
            }
            set
            {
                base[key_Regrade] = value ? 1M : 0M;
            }
        }

        public int Priority
        {
            get
            {
                //return (int)(decimal)base[key_Prority];
                return base.GetInt(key_Prority);
            }
            set
            {
                base[key_Prority] = (decimal)value;
            }
        }

        /// <summary>
        /// Onfail
        /// </summary>
        public int OnFail
        {
            get
            {
                return (int)(decimal)(double)base[key_OnFail];
            }
            set
            {
                base[key_OnFail] = (decimal)value;
            }
        }

        public string Format
        {
            get
            {
                if (base[key_Format] == null)
                {
                    switch (this.DataType )
                    {
                        case LimitDataType.Float:
                            base[key_Format] = "#0.0000";
                            break;

                        case LimitDataType.Integer:
                            base[key_Format] = "00";
                            break;

                        case LimitDataType.String14:
                            base[key_Format] = "14A";
                            break;

                        case LimitDataType.String20:
                            base[key_Format] = "20A";
                            break;

                        case LimitDataType.String256:
                            base[key_Format] = "256A";
                            break;

                        case LimitDataType.String80:
                            base[key_Format] = "80A";
                            break;
                    }
                }

                return base[key_Format] as string ;
            }
            set
            {
                base[key_Format] = value;
            }
        }

        /// <summary>
        /// Item unit.
        /// </summary>
        public  string Unit
        {
            get
            {
                string units = base[key_Units] as string;

                if ((units == null) || (units.Length == 0))
                {
                    units = "none";
                }

                return units;
            }

            set
            {
                base[key_Units] = value;
            }
        }

        /// <summary>
        /// Item description
        /// </summary>
        public string Description
        {
            get
            {
                return base[key_Description] as string; ;
            }
            set
            {
                base[key_Description] = value;
            }
        }

        /// <summary>
        /// Item name ( short_desc).
        /// </summary>
        public string Name
        {
            get
            {
                return base[key_Name] as string;
            }
            set
            {
                base[key_Name] = value;
            }
        }

        /// <summary>
        /// Item low limit.
        /// </summary>
        public decimal LowLimit
        {
            get
            {
                //return (decimal)base[key_Minumum];
                return base.GetDecimal(key_Minumum);
            }
            set
            {
                base[key_Minumum] = value;
            }
        }

        /// <summary>
        /// Item high limit.
        /// </summary>
        public decimal HighLimit
        {
            get
            {
                return base.GetDecimal(key_Maximum);
            }
            set
            {
                base[key_Maximum] = value;
            }
        }
        #endregion

        public string ToCSV()
        {
            //string str = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}",
            //    this.Name, this.LowLimitString, this.HighLimitString,
            //    this._unit, this._format, this._onfail, (int)this.DataType,
            //    this._priority, this._regrade, this.Description);
            string str = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8}",
                this.Name, this.Description, this.LowLimit.ToString(), this.HighLimit.ToString(),
                this.Unit, this.Format, this.OnFail, (int)this.DataType, this.Priority);
            return str;
        }

        public bool HasLowLimit
        {
            get
            {
                return (this.LowLimit != -999) && (this.LowLimit != -9999) && (this.LowLimit != -99999);
            }
        }

        public bool HasHighLimit
        {
            get
            {
                return (this.HighLimit != 999) && (this.HighLimit != 9999) && (this.HighLimit != 99999);
            }
        }

        public PcasResultStatus  CheckValue(object value)
        {
            switch ( this.DataType )
            {
                case LimitDataType.Float:
                case LimitDataType.Integer:
                    {
                        if (value == null)
                        {
                            return PcasResultStatus.NullValue;
                        }

                        decimal v = 0;
                        if (value is IConvertible)
                        {
                            IConvertible ic = value as IConvertible;
                            try
                            {
                                v = ic.ToDecimal(null);
                            }
                            catch (InvalidCastException /*e*/)
                            {
                                v = decimal.MinValue;
                            }
                            catch (OverflowException /*e*/)
                            {
                                v = decimal.MinValue;
                            }
                        }

                        if (this.HasLowLimit)
                        {
                            if (this.HasHighLimit)
                            {
                                return (this.LowLimit <= v) && (v <= this.HighLimit) ? PcasResultStatus.Passed : PcasResultStatus.Failed;
                            }
                            else
                            {
                                return (this.LowLimit <= v) ? PcasResultStatus.Passed : PcasResultStatus.Failed;
                            }
                        }
                        else
                        {
                            if (this.HasHighLimit)
                            {
                                return (v <= this.HighLimit) ? PcasResultStatus.Passed : PcasResultStatus.Failed;
                            }
                            else
                            {
                                return PcasResultStatus.Opened;
                            }
                        }
                    }
                    //break;

                default:
                    {
                        return PcasResultStatus.Opened;
                    }
                    //break;
            }
        }

        public void CheckForError()
        {
            this._errorInfo = "";
            this._warningInfo = "";

            if (this.Name.Contains("-"))
            {
                this._errorInfo += "Limit item name contains invalid character '-'.\n";
            }

            if (this.Name.Length > 30)
            {
                this._errorInfo += "Limit item name exceeded 30 characters.\n";
            }

            switch (this.DataType)
            {
                case LimitDataType.Float:
                    {
                        int minDigits = 0;
                        int minTailZero = 0;
                        int maxDigits = 0;
                        int maxTailZero = 0;
                        bool startCountDigit = false;
                        string min = this.LowLimit.ToString();
                        string max = this.HighLimit.ToString();

                        for(int idx = 0 ; idx < min.Length; idx ++ )
                        {
                            char c = min[idx];

                            if( startCountDigit )
                            {
                                minDigits ++;
                            }

                            if ( c == '.' )
                            {
                                startCountDigit = true;
                            }

                            if (c == '0')
                            {
                                minTailZero++;
                            }
                            else
                            {
                                minTailZero = 0;
                            }
                        }
                        minDigits -= minTailZero;

                        startCountDigit = false;
                        for(int idx = 0 ; idx < max.Length; idx ++ )
                        {
                            char c = max[idx];

                            if( startCountDigit )
                            {
                                maxDigits ++;
                            }

                            if ( c == '.' )
                            {
                                startCountDigit = true;
                            }

                            if (c == '0')
                            {
                                maxTailZero++;
                            }
                            else
                            {
                                maxTailZero = 0;
                            }
                        }
                        maxDigits -= maxTailZero;

                        startCountDigit = false;
                        int formatDigit = 0;
                        for( int idx = 0; idx < this.Format.Length; idx ++ )
                        {
                            char c = this.Format[idx];

                            if ( startCountDigit )
                            {
                                formatDigit ++;
                            }

                            if (c == '.')
                            {
                                if (startCountDigit)
                                {
                                    this._errorInfo += "Limit Format Error!\n";
                                    break;
                                }
                                else
                                {
                                    startCountDigit = true;
                                }
                            }
                        }

                        if (( formatDigit == 0 ) ||
                            (formatDigit <= Math.Max(maxDigits, minDigits)))
                        {
                            this._warningInfo += string.Format("Results may truncated by FORMAT[{0}].\n", this.Format);
                        }
                    }
                    break;
            }
        }

        public override void Bind(DbObjectAdapter adapter)
        {
            //adapter.Bind("MINIMUM", ref this._minimum);
            //adapter.Bind("MAXIMUM", ref this._maximum);
            //adapter.Bind("UNITS", ref this._unit);
            //adapter.Bind("FORMAT", ref this._format);
            //adapter.Bind("ON_FAIL", ref this._onfail);
            //decimal ival = (decimal)this._dateType;
            //adapter.Bind("STORE", ref ival);
            //this._dateType = (LimitDataType)ival;
            //adapter.Bind("PRIORITY", ref this._priority);
            //adapter.Bind("SHORT_DESC", ref this._name);
            //adapter.Bind("REGRADE", ref this._regrade);

            //adapter.Bind("long_desc", ref this._description);
            base.Bind(adapter);
        }

        public void IncreaseFormatDecimal()
        {
            string format = base[key_Format] as string;

            bool hasDecimal = false;
            bool isDecimalFormat = true;
            foreach (char c in format)
            {
                if ( char.IsDigit( c) ||
                    (c == '#') )
                {
                    continue;
                }
                
                if( c == '.' )
                {
                    hasDecimal = true;
                    continue;
                }

                isDecimalFormat = false;
                break;
            }

            if (isDecimalFormat == true)
            {
                format += hasDecimal ? "0" : ".0";
                base[key_Format] = format;
            }
        }

        public void GuessFromString(string strMin, string strMax, List<string> values)
        {
            decimal min = decimal.MinValue;
            decimal max = decimal.MaxValue;

            if (decimal.TryParse(strMin, out min))
            {
                this.LowLimit = min;
            }
            else
            {
                this.LowLimit = -9999;
            }

            if (decimal.TryParse(strMax, out max))
            {
                this.HighLimit = max;
            }
            else
            {
                this.HighLimit = 9999;
            }

            bool mayInt = true;
            bool mayFlt = true;
            bool mayS14 = true;
            bool mayS20 = true;
            bool mayS80 = true;
            bool mayS256 = true;
            foreach (string item in values)
            {
                if (string.IsNullOrEmpty(item))
                    continue;

                int resi = 0;
                decimal resd = 0;
                bool resb = false;

                if (mayInt && 
                    !int.TryParse(item, out resi) &&
                    !bool.TryParse( item, out resb) )
                    mayInt = false;

                if (mayFlt && !decimal.TryParse(item, out resd))
                    mayFlt = false;

                if (mayS14 && item.Length > 14)
                    mayS14 = false;

                if (mayS20 && item.Length > 20)
                    mayS20 = false;

                if (mayS80 && item.Length > 80)
                    mayS80 = false;

                if (mayS256 && item.Length > 256)
                    mayS256 = false;
            }

            if (mayInt)
            {
                this.DataType = LimitDataType.Integer;
                this.Format = "#0";
            }
            else if (mayFlt)
            {
                this.DataType = LimitDataType.Float;
                this.Format = "#0.0000";
            }
            else if (mayS14)
            {
                this.DataType = LimitDataType.String14;
                this.Format = "14A";
            }
            else if (mayS20)
            {
                this.DataType = LimitDataType.String20;
                this.Format = "20A";
            }
            else if (mayS80)
            {
                this.DataType = LimitDataType.String80;
                this.Format = "80A";
            }
            else if (mayS256)
            {
                this.DataType = LimitDataType.String256;
                this.Format = "256A";
            }
            else
            {
                throw new InvalidOperationException("string value exceeded 256!");
            }
        }
    }
}

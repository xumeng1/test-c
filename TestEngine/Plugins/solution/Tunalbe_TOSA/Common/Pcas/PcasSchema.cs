using System;
using System.Collections.Generic;
using System.Text;

namespace Pcas
{
    public enum PcasSchema
    {
        OC_PRODDB,
        AMCO,
        CATVDB,
        COC,
        HBR10DB,
        HIBERDB,
        OAL,
    };
}

using System;
using System.Collections.Generic;
using System.Text;
using DbAccess;
using System.Data.Common;

namespace Pcas
{
    public class Result
        : DbObjectWithColumn
    {
        #region Field Definitions
        public const string key_SpecificationId = "SPEC_ID";
        #endregion

        #region Properties
        public string SpecificationId
        {
            get
            {
                return base[key_SpecificationId] as string;
            }
        }
        #endregion

        public static Result GetResult(TestLink link)
        {
            return PCASAdapter.GetResult(link);
        }

        public static List<Result> GetResults( List<TestLink> links )
        {
            return PCASAdapter.GetResults(links);
        }

        public string GetFileLinkFullPath(string key )
        {
            string file = base[key] as string;

            if (string.IsNullOrEmpty(file))
            {
                return null;
            }
            else
            {
                string node = this["NODE"].ToString();
                //string date = this["TIME_DATE"].ToString().Substring(0, 8);
                string [] parts = file.Split(new char[] { '_' });
                string date = parts[parts.Length - 1];

                if (string.IsNullOrEmpty(node) ||
                    string.IsNullOrEmpty(date))
                {
                    return null;
                }
                else
                {
                    return string.Format("\\\\Szn-sfl-clst-01\\Results\\NODE{0}\\{1}\\{2}",
                    node, date.Substring(0,8), file);
                }
            }
        }

        public int ItemCount
        {
            get
            {
                return base._values.Count;
            }
        }
    }
}

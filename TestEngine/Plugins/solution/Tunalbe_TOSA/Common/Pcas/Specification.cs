using System;
using System.Collections.Generic;
using System.Text;
using DbAccess;
using System.IO;
using System.Collections;

namespace Pcas
{
    public class Specification
        : DbObjectWithColumn
        , IEnumerable<LimitItem>
    {
        #region Fields Definition
        const string key_SpecId = "SPEC_ID";
        const string key_DeviceType = "DEVICE_TYPE";
        const string key_TestStage = "TEST_STAGE";
        const string key_Description = "DESCRIPTION";
        const string key_ResultsTableId = "RESULTS_TABLE_ID";
        const string key_TmSeq = "TM_SEQ";
        const string key_RepResults = "REP_RESULTS";
        const string key_RepSpec = "REP_SPEC";
        #endregion

        //#region Nested Types
        //public class LimitItemEnumerator : IEnumerator<LimitItem>
        //{
        //    private Specification _owner;
        //    private int _index = -1;

        //    internal LimitItemEnumerator(Specification owner)
        //    {
        //        this._owner = owner;
        //        this._index = -1;
        //    }

        //    #region IEnumerator<LimitItem> Members

        //    public LimitItem Current
        //    {
        //        get
        //        {
        //            return this._owner[this._index];
        //        }
        //    }

        //    #endregion

        //    #region IDisposable Members

        //    public void Dispose()
        //    {
        //        this._owner = null;
        //    }

        //    #endregion

        //    #region IEnumerator Members

        //    object System.Collections.IEnumerator.Current
        //    {
        //        get
        //        {
        //            return this._owner[this._index];
        //        }
        //    }

        //    public bool MoveNext()
        //    {
        //        this._index++;

        //        if (_index < this._owner.Count)
        //        {
        //            return true;
        //        }
        //        else
        //        {
        //            this._index = this._owner.Count - 1;
        //            return false;
        //        }
        //    }

        //    public void Reset()
        //    {
        //        this._index = -1;
        //    }

        //    #endregion
        //}
        //#endregion

        private PcasSchema _schema;
        internal List<LimitItem> _list = new List<LimitItem>();
        private bool _isLoadLimits = false;

        #region Constructors
        /// <summary>
        /// Default constructor.
        /// </summary>
        public Specification()
        {
            this._isLoadLimits = true;
        }
        #endregion

        #region Public Properties and Indexers

        /// <summary>
        /// Gets or sets the device type of this specification.
        /// </summary>
        public string DeviceType
        {
            get
            {
                return base[key_DeviceType] as string;
            }
        }

        /// <summary>
        /// Return/set the value of specification name(SPEC_ID in database).
        /// </summary>
        public string Name
        {
            get
            {
                return base[key_SpecId] as string;
            }
            set
            {
                base[key_SpecId] = value;
            }
        }

        public string TimeSequence
        {
            get
            {
                //return this._tmSeq;
                return base[key_TmSeq] as string;
            }
        }

        public string TestStage
        {
            get
            {
                return base[key_TestStage] as string;
            }
        }

        public PcasSchema Schema
        {
            get
            {
                return _schema;
            }
            internal set
            {
                _schema = value;
            }
        }
        public LimitItem this[int index]
        {
            get
            {
                this.SyncSpecs();
                return this._list[index];
            }
        }

        public new LimitItem this[string index]
        {
            get
            {
                this.SyncSpecs();

                foreach (LimitItem item in this._list)
                {
                    if (item.Name == index)
                    {
                        return item;
                    }
                }
                return null;
            }
        }
        #endregion

        public int Find(string limitName)
        {
            //foreach( LimitItem limit in this._list )
            for (int i = 0; i < this._list.Count; i++)
            {
                LimitItem limit = this._list[i];

                if (string.Compare(limit.Name, limitName) == 0)
                {
                    return i;
                }
            }
            return -1;
        }

        public static int SortByName(LimitItem x, LimitItem y)
        {
            return string.Compare(x.Name, y.Name);
        }

        public void Sort(Comparison<LimitItem> cmp)
        {
            if (cmp == null)
            {
                this._list.Sort(Specification.SortByName);
            }
            else
            {
                this._list.Sort(cmp);
            }
        }

        public override bool IsModified
        {
            get
            {
                foreach (LimitItem item in this._list)
                {
                    if (item.IsModified == true)
                    {
                        return true;
                    }
                }

                return base.IsModified;
            }
        }

        public int Count
        {
            get
            {
                this.SyncSpecs();
                return this._list.Count;
            }
        }

        public void Add(LimitItem item)
        {
            this._list.Add(item);
            this.MarkModified();
        }

        public void AddRange(ICollection<LimitItem> col)
        {
            foreach (LimitItem item in col)
            {
                this._list.Add(item);
            }
            this.MarkModified();
        }

        #region Iterator Implementation

        public new IEnumerator<LimitItem> GetEnumerator()
        {
            this.SyncSpecs();

            for (int i = 0; i < this._list.Count; i++)
            {
                yield return _list[i];
            }
        }
        
        IEnumerator IEnumerable.GetEnumerator()
        {
            this.SyncSpecs();

            for (int i = 0; i < this._list.Count; i++)
            {
                yield return _list[i];
            }
        }

        #endregion

        public override void Bind(DbObjectAdapter adapter)
        {
            base.Bind(adapter);

            //adapter.Bind(key_SpecId, ref this._specificationId);
            //adapter.Bind(key_DeviceType, ref this._deviceType);
            //adapter.Bind(key_TestStage, ref this._testStage);
            //adapter.Bind(key_Description, ref this._description);
            //adapter.Bind(key_ResultsTableId, ref this._resultsTableId);
            //adapter.Bind(key_TmSeq, ref this._tmSeq);
            //adapter.Bind(key_RepResults, ref this._repResults);
            //adapter.Bind(key_RepSpec, ref this._repSpec);

            this._isLoadLimits = false;
        }

        private void SyncSpecs()
        {
            if (this._isLoadLimits == false)
            {
                string specId = this.Name;

                this._list = PCASAdapter.GetLimitItems(_schema, specId);

                this._isLoadLimits = true;
            }
        }

        public SpecificationWithValue FillValues(PcasResult values)
        {
            this.SyncSpecs();

            SpecificationWithValue spec = new SpecificationWithValue(this);
            //for (int i = 0; i < values.Count; i++)
            //{
            //    object val = values[i];
            //    LimitItem limit = null;
            //    foreach( LimitItem item in this._list )
            //    {
            //    }
            //}
            foreach (LimitItem item in spec._list)
            {
                spec.AddValue(item, values[item.Name]);
            }

            return spec;
        }

        public void WritePcasDropWithResults(DbObjectWithColumn item, string fileName, string serialNo)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }

            StreamWriter writer = new StreamWriter(fileName);

            writer.WriteLine("~DEVICE TYPE");
            //writer.WriteLine(this._deviceType);
            writer.WriteLine(this.DeviceType);
            writer.WriteLine("~STAGE");
            writer.WriteLine(this.TestStage);
            writer.WriteLine("~SPECIFICATION");
            writer.WriteLine(this.Name);
            writer.WriteLine("~SERIAL NUMBER");
            writer.WriteLine(serialNo);

            foreach (LimitItem limit in this)
            {
                //if (item[limit.Name] != null)
                object value = item.GetValue(limit.Name, true);
                if (value != null)
                {
                    string valueStr = value.ToString();

                    if (valueStr.Length != 0)
                    {
                        writer.WriteLine(string.Format("${0}${1}", limit.Name, (int)limit.DataType));

                        switch (limit.DataType)
                        {
                            case LimitDataType.Float:
                                {
                                    double val;
                                    if (!double.TryParse(valueStr, out val))
                                    {
                                        bool bval;
                                        if (bool.TryParse(valueStr, out bval))
                                        {
                                            valueStr = bval ? "1" : "0";
                                        }
                                    }
                                }
                                break;

                            case LimitDataType.Integer:
                                {
                                    int val;
                                    if (!int.TryParse(valueStr, out val))
                                    {
                                        bool bval;
                                        if (bool.TryParse(valueStr, out bval))
                                        {
                                            valueStr = bval ? "1" : "0";
                                        }
                                    }
                                }
                                break;

                            default:
                                {
                                }
                                break;
                        }
                        writer.WriteLine(valueStr);
                    }
                }
                else // value == null;
                {
                    string upperLimitName = limit.Name.ToUpper();

                    //if (string.Compare(limit.Name, "SPEC_ID", true) == 0)
                    switch (upperLimitName)
                    {
                        case "SPEC_ID":
                            {
                                writer.WriteLine("${0}${1}", limit.Name, (int)limit.DataType);
                                writer.WriteLine(this.Name);
                            }
                            break;

                        case "OPERATOR_ID":
                            {
                                writer.WriteLine("${0}${1}", limit.Name, (int)limit.DataType);
                                writer.WriteLine("TCMZ Mage");
                            }
                            break;
                    }
                }
            }
            writer.Close();
        }

        public static TableSchema GetTableSchemaForCSV()
        {
            TableSchema schema = new TableSchema();
            schema.Add(new TableColumn(0, "SHORT_DESC", typeof(string) ));
            schema.Add(new TableColumn(1, "LONG_DESC", typeof(string)));
            schema.Add(new TableColumn(2, "MINIMUM", typeof(decimal)));
            schema.Add(new TableColumn(3, "MAXIMUM", typeof(decimal)));
            schema.Add(new TableColumn(4, "UNITS", typeof(string)));
            schema.Add(new TableColumn(5, "FORMAT", typeof(string)));
            schema.Add(new TableColumn(6, "ON_FAIL", typeof(decimal)));
            schema.Add(new TableColumn(7, "STORE", typeof(decimal)));
            schema.Add(new TableColumn(8, "PRIORITY", typeof(decimal)));
            return schema;
        }

        public static TableColumnMapping GetTableMappingForCSV()
        {
            TableColumnMapping map = new TableColumnMapping();

            map.Add("SHORT_DESC", "0");
            map.Add("LONG_DESC", "1");
            map.Add("MINIMUM","2");
            map.Add("MAXIMUM","3");
            map.Add("UNITS", "4");
            map.Add("FORMAT", "5");
            map.Add("ON_FAIL", "6");
            map.Add("STORE", "7");
            map.Add("PRIORITY", "8");

            return map;
        }

        public PcasResultStatus Check(KeyValuePair<string, object> item)
        {
            LimitItem limit = this[item.Key];

            if (limit == null)
                return PcasResultStatus.NotInSpec;
            else
                return limit.CheckValue(item.Value);
        }

        public PcasResultStatus Check(string key, object value)
        {
            LimitItem limit = this[key];

            if (limit == null)
                return PcasResultStatus.NotInSpec;
            else
                return limit.CheckValue(value);
        }
    }
}

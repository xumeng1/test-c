using System;
using System.Collections.Generic;
using System.Text;
using DbAccess;
using System.Data.Common;

namespace Pcas
{
    public class PCASAdapter
    {
        private static Dictionary<PcasSchema, string> _lookupTblClusterOwner;
        private static DbObjectAdapter _adapter;

        static PCASAdapter()
        {
            _lookupTblClusterOwner = new Dictionary<PcasSchema, string>();

            _lookupTblClusterOwner.Add(PcasSchema.AMCO, "AMCO");
            _lookupTblClusterOwner.Add(PcasSchema.CATVDB, "CATVDB");
            _lookupTblClusterOwner.Add(PcasSchema.COC, "");
            _lookupTblClusterOwner.Add(PcasSchema.HBR10DB, "HBR10DB");
            _lookupTblClusterOwner.Add(PcasSchema.HIBERDB, "HIBERDB");
            _lookupTblClusterOwner.Add(PcasSchema.OAL, "OAL");
            _lookupTblClusterOwner.Add(PcasSchema.OC_PRODDB, "OC_PRODDB");
        }

        public static DbObjectAdapter Adapter
        {
            get
            {
                return _adapter;
            }
            set
            {
                _adapter = value;
            }
        }

        static string GetClusterOwner(PcasSchema schema)
        {
            return _lookupTblClusterOwner[schema];
        }

        public static List<Result> GetResults( List<TestLink> links )
        {
            List<Result> results = new List<Result> ();
            foreach( TestLink link in links )
            {
                try
                {
                    results.Add(GetResult(link));
                }
                catch (DbException /*e*/ )
                {
                }
            }
            return results;
        }

        public static Result GetResult(TestLink link)
        {
            string clusterOwner = GetClusterOwner(link.Schema);
            if (!string.IsNullOrEmpty(clusterOwner))
            {
                clusterOwner += ".";
            }

            string sql;

            if ( string.IsNullOrEmpty( clusterOwner ))
            {
                sql = string.Format(
                     @" SELECT UNIQUE table_name FROM all_tables WHERE table_name LIKE '{0}_%' and owner IS NULL ",
                     link.ResultTable);
            }
            else
            {
                sql = string.Format(
                     @" SELECT UNIQUE table_name FROM all_tables WHERE table_name LIKE '{0}_%' and owner = '{1}' ",
                     link.ResultTable, link.Schema.ToString());
            }

            List<string> tables = new List<string>();
            DbDataReader reader = _adapter.CreateReader(sql);
            while (reader.Read())
            {
                tables.Add(reader["table_name"] as string);
            }
            reader.Close();

            if (tables.Count == 0)
            {
                return null;
            }

            StringBuilder sqlSelect = new StringBuilder(" SELECT ");
            StringBuilder sqlFrom = new StringBuilder();
            sqlFrom.AppendFormat(" FROM {0}.test_link", link.Schema.ToString());

            StringBuilder sqlWhere = new StringBuilder();
            sqlWhere.AppendFormat(
                @"WHERE {2}.test_link.serial_no = '{0}' and {2}.test_link.time_date = '{1}'",
                link.SerialNo, link.TimeDateString, link.Schema );
            for (int i = 0; i < tables.Count; i++)
            {
                sqlSelect.AppendFormat(i > 0 ? ", {0}{1}.*" : "{0}{1}.*", clusterOwner, tables[i]);
                sqlFrom.AppendFormat(@", {0}{1}", clusterOwner, tables[i]);
                sqlWhere.AppendFormat(@" AND {0}{1}.serial_no = test_link.serial_no AND {0}{1}.time_date = test_link.time_date",
                    clusterOwner, tables[i]);
            }
            sql = string.Format(@" {0} {1} {2} ", sqlSelect.ToString(), sqlFrom.ToString(), sqlWhere.ToString());

            List<Result> list = _adapter.Query<Result>(sql);
            if (list.Count > 0)
            {
                return list[0];
            }
            else
            {
                return null;
            }
        }

        public static List<TestLink> GetTestLinks( TestLinkQueryStruct query )
        {
            if (query.Schema == null)
            {
                List<TestLink> list = new List<TestLink>();

                foreach (PcasSchema schema in Enum.GetValues(typeof(PcasSchema)))
                {
                    List<TestLink> subList = null;
                    try
                    {
                        subList = GetTestLinks(schema, query.SerialNo, query.TestStage, query.DeviceType, query.StartTime, query.EndTime);
                    }
                    catch (DbException /*e*/ )
                    {
                    }

                    if (subList!= null)
                    {
                        list.AddRange(subList);
                    }
                }

                return list;
            }
            else
            {
                return GetTestLinks(query.Schema.Value, query.SerialNo, query.TestStage, query.DeviceType, query.StartTime, query.EndTime);
            }
        }

        public static List<TestLink> GetTestLinks(PcasSchema schema, string serialNo, string testStage, string deviceType, DateTime? dtStart, DateTime? dtEnd)
        {
            string clusterOwner = GetClusterOwner(schema);
            StringBuilder whereClause = new StringBuilder();

            if (!string.IsNullOrEmpty(serialNo))
            {
                if (serialNo.Contains("*") || serialNo.Contains("%"))
                {
                    whereClause.AppendFormat("serial_no LIKE '{0}'", serialNo.Replace('*', '%'));
                }
                else
                {
                    whereClause.AppendFormat("serial_no = '{0}'", serialNo);
                }
            }

            if (!string.IsNullOrEmpty(testStage))
            {
                string correctedTestStage = testStage.Replace('*', '%');
                string opr = (testStage.Contains("*") || testStage.Contains("%")) ? "LIKE" : "=";

                whereClause.AppendFormat(
                    (whereClause.Length == 0) ? "test_stage {0} '{1}'" : " AND test_stage {0} '{1}'", opr, correctedTestStage);
            }

            if (dtStart != null)
            {
                whereClause.AppendFormat(
                    (whereClause.Length == 0)?"time_date >= '{0}'":" AND time_date >= '{0}'", 
                    dtStart.Value.ToString("yyyyMMddHHmmss"));
            }

            if (dtEnd != null)
            {
                whereClause.AppendFormat((whereClause.Length == 0) ? "time_date <= '{0}'" : " AND time_date <= '{0}'", 
                    dtEnd.Value.ToString("yyyyMMddHHmmss"));
            }

            if (!string.IsNullOrEmpty(deviceType))
            {
                string correctedDeviceType = deviceType.Replace('*', '%');
                string opr = (deviceType.Contains("*") || deviceType.Contains("%")) ? "LIKE" : "=";

                whereClause.AppendFormat(
                    (whereClause.Length == 0) ? " device_type {0} '{1}' " : " AND device_type {0} '{1}' ",
                    opr, correctedDeviceType);
            }

            string sql = null;
            if (whereClause.Length == 0)
            {
                sql = string.Format(" SELECT * FROM {0}.test_link ORDER BY time_date DESC ", schema.ToString());
            }
            else
            {
                sql = string.Format(
                    " SELECT * FROM {0}.test_link WHERE {1} ORDER BY time_date DESC ",
                    schema.ToString(),
                    whereClause.ToString());
            }

            List<TestLink> links = _adapter.Query<TestLink>(sql);

            foreach (TestLink link in links)
            {
                link.Schema = schema;
            }
            return links;
        }

        public static Specification GetLastLiveSepcification(PcasSchema schema, string deviceType, string testStage, string specId)
        {
            List<Specification> list = GetLiveSpecifications( schema, deviceType, testStage, specId);

            return (list.Count > 0) ? list[0] : null;
        }

        public static Specification GetLastSepcification(PcasSchema schema, string deviceType, string testStage, string specId)
        {
            List<Specification> list = GetSpecifications(schema, deviceType, testStage, specId, false);

            return (list.Count > 0) ? list[0] : null;
        }

        public static List<Specification> GetLiveSpecifications(PcasSchema schema, string deviceType, string testStage, string specId)
        {
            return GetSpecifications(schema, deviceType, testStage, specId, true);
        }

        public static List<Specification> GetSpecifications(PcasSchema? schema, string deviceType, string testStage, string specId, bool liveOnly)
        {
            if (schema == null)
            {
                List<Specification> results = new List<Specification>();

                Array schemas = Enum.GetValues(typeof(PcasSchema));
                foreach (PcasSchema item in schemas)
                {
                    List<Specification> subList = GetSpecifications(item, deviceType, testStage, specId, liveOnly);
                    results.AddRange(subList);
                }

                return results;
            }
            else
            {
                return GetSpecifications(schema, deviceType, testStage, specId, liveOnly);
            }
        }

        public static List<Specification> GetSpecifications(PcasSchema schema, string deviceType, string testStage, string specId, bool liveOnly)
        {
            string clusterOwner = GetClusterOwner(schema);

            StringBuilder whereClause = new StringBuilder();

            if (!string.IsNullOrEmpty(deviceType))
            {
                whereClause.AppendFormat("device_type = '{0}'", deviceType);
            }

            if (!string.IsNullOrEmpty(testStage))
            {
                if (whereClause.Length == 0)
                {
                    whereClause.AppendFormat("test_stage = '{0}'", testStage);
                }
                else
                {
                    whereClause.AppendFormat(" AND test_stage = '{0}'", testStage);
                }
            }

            if (!string.IsNullOrEmpty(specId))
            {
                if (whereClause.Length == 0)
                {
                    whereClause.AppendFormat("spec_id = '{0}'", specId);
                }
                else
                {
                    whereClause.AppendFormat(" AND spec_id = '{0}'", specId);
                }
            }

            if (liveOnly)
            {
                if (whereClause.Length == 0)
                {
                    whereClause.Append(" tm_seq <> 0 ");
                }
                else
                {
                    whereClause.Append("AND tm_seq <> 0 ");
                }
            }

            string sql = null;
            if (whereClause.Length > 0)
            {
                sql = string.Format(
                     "SELECT * FROM {0}.test_master WHERE {1} ORDER BY tm_seq DESC",
                     schema.ToString(),
                     whereClause.ToString());
            }
            else
            {
                sql = string.Format(@"SELECT * FROM {0}.test_master ORDER BY tm_seq DESC",schema.ToString() );
            }

            List<Specification> list = _adapter.Query<Specification>(sql);

            foreach (Specification spec in list)
            {
                spec.Schema = schema;
            }

            return list;
        }

        public static List<string> GetTestStages(PcasSchema? schema)
        {
            List<string> stages = new List<string>();

            if (schema == null)
            {
                foreach (PcasSchema item in Enum.GetValues(typeof(PcasSchema)))
                {
                    stages.AddRange(
                        GetTestStages(item)
                    );
                }
            }
            else
            {
                try
                {
                    string sql = string.Format("SELECT UNIQUE test_stage FROM {0}.test_master WHERE tm_seq <> 0",
                        schema.ToString());
                    DbDataReader reader = _adapter.CreateReader(sql);

                    while (reader.Read())
                    {
                        stages.Add(reader["test_stage"] as string);
                    }
                    reader.Close();
                }
                catch (Exception /*e*/)
                {
                }
            }

            return stages;
        }

        public static List<string> GetDeviceTypes(PcasSchema? schema)
        {
            List<string> deviceTypes = new List<string>();

            if (schema == null)
            {
                Array schemas = Enum.GetValues(typeof(PcasSchema));

                foreach (PcasSchema item in schemas)
                {
                    List<string> subTypes = GetDeviceTypes(item);
                    deviceTypes.AddRange(subTypes);
                }
            }
            else
            {
                try
                {
                    string sql = string.Format("SELECT UNIQUE device_type FROM {0}.test_master WHERE tm_seq <> 0",
                        schema.ToString());
                    DbDataReader reader = _adapter.CreateReader(sql);

                    while (reader.Read())
                    {
                        deviceTypes.Add(reader["device_type"] as string);
                    }
                    reader.Close();
                }
                catch (Exception /*e*/)
                {
                }
            }

            return deviceTypes;
        }

        public static List<LimitItem> GetLimitItems(PcasSchema schema, string specId)
        {
            List<LimitItem> list = _adapter.Query<LimitItem>(
                    string.Format("select s.*, d.long_desc from {0}.{1} s, {0}.descriptors d WHERE s.short_desc = d.short_desc(+) order by s.short_desc", 
                    schema.ToString(), specId ));
            return list;
        }
    }
}

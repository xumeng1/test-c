using System;
using System.Collections.Generic;
using System.Text;
using DbAccess;
using System.Data.OleDb;

namespace Pcas
{
    public struct TestLinkQueryStruct
    {
        public PcasSchema? Schema;
        public string SerialNo;
        public string TestStage;
        public DateTime? StartTime;
        public DateTime? EndTime;
        public string DeviceType;
    };

    public class TestLink 
        : DbAccess.DbObjectWithColumn
    {
        public const string key_SerialNo = "SERIAL_NO";
        public const string key_ResultTable = "RESULTS_TABLE_ID";
        public const string key_DateTime = "TIME_DATE";
        public const string key_TestStatus = "TEST_STATUS";
        public const string key_DeviceType = "DEVICE_TYPE";
        public const string key_TestStage = "TEST_STAGE";
        public const string key_SpecId = "SPEC_ID";

        private Result _result = null;
        private Specification _spec = null;
        private PcasSchema _schema;

        public PcasSchema Schema
        {
            get
            {
                return this._schema;
            }
            set
            {
                this._schema = value;
            }
        }

        public string SerialNo
        {
            get
            {
                return base[key_SerialNo] as string ;
            }
        }

        public string ResultTable
        {
            get
            {
                string table = base[key_ResultTable] as string ;
                return table.ToUpper();
            }
        }

        public string TimeDateString
        {
            get
            {
                return base[key_DateTime] as string ;
            }
        }

        public DateTime DateTime
        {
            get
            {
                string str = base[key_DateTime] as string ;
                int year = int.Parse(str.Substring(0, 4));
                int month = int.Parse(str.Substring(4, 2));
                int day = int.Parse(str.Substring(6, 2));
                int hour = int.Parse( str.Substring(8,2));
                int minute = int.Parse(str.Substring(10, 2));
                int second = int.Parse(str.Substring(12, 2));
                DateTime ret = new DateTime(year, month, day, hour, minute, second);
                return ret;
            }
        }

        public string TestStatus
        {
            get
            {
                return base[key_TestStatus] as string ;
            }
        }

        public string SpecificationId
        {
            get
            {
                return base[key_SpecId] as string ;
            }
        }

        public string DeviceType
        {
            get
            {
                return base[key_DeviceType] as string;
            }
        }

        public string TestStage
        {
            get
            {
                return base[key_TestStage] as string;
            }
        }

        public Result Result
        {
            get
            {
                if (this._result == null)
                {
                    this._result = this.GetResult();
                }
                return this._result;
            }
        }

        public Result GetResult()
        {
            return PCASAdapter.GetResult( this);
        }

        public Specification Specification
        {
            get
            {
                if (_spec == null)
                {
                    _spec = this.GetSpecification();
                }

                return _spec;
            }
        }

        public Specification GetSpecification( )
        {
            return PCASAdapter.GetLastSepcification(this._schema, this.DeviceType, this.TestStage, this.SpecificationId);
        }

        public static TestLink GetLastTestLink(DbObjectAdapter adapter, string serialno)
        {
            TestLinkQueryStruct query = new TestLinkQueryStruct();
            query.SerialNo = serialno;
            List<TestLink> list = PCASAdapter.GetTestLinks(query);

            return list.Count > 0 ? list[0] : null;
        }

        public static List<TestLink> GetTestLinks( string serialno)
        {
            TestLinkQueryStruct query = new TestLinkQueryStruct();
            query.SerialNo = serialno;
            return PCASAdapter.GetTestLinks(query);
        }

        public static List<TestLink> GetTestLinks(DbObjectAdapter adapter, string serialno, string testStage)
        {
            TestLinkQueryStruct query = new TestLinkQueryStruct();
            query.SerialNo = serialno;
            query.TestStage = testStage;
            return PCASAdapter.GetTestLinks(query);
        }

        public static List<TestLink> GetTestLinks(TestLinkQueryStruct queryStruct)
        {
            return PCASAdapter.GetTestLinks(queryStruct);
        }
    }
}

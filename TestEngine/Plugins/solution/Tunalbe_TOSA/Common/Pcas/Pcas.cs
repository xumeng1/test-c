using System;
using System.Collections.Generic;
using System.Text;
using System.Data.OleDb;
using System.Data.OracleClient;
using System.Data.Common;
using System.Data;
using DbAccess;

namespace Pcas
{
    public class PcasResult : DbObjectBase
    {

        private List<KeyValuePair<string, Object>> _items = new List<KeyValuePair<string,object>> ();

        public PcasResult()
        {
        }

        public int Count
        {
            get
            {
                return this._items.Count;
            }
        }

        public object this[int index]
        {
            get
            {
                return this._items[index].Value;
            }
        }

        public object this[string index]
        {
            get
            {
                for (int i = 0; i < this._items.Count; i++)
                {
                    KeyValuePair<string,object> pair = this._items[i];
                    if (pair.Key == index)
                    {
                        return pair.Value;
                    }
                }
                return null;
            }
        }

        public Type GetType(int index)
        {
            return this._items[index].Value.GetType();
        }

        public string GetName(int index)
        {
            return this._items[index].Key ;
        }

        public override void Bind(DbObjectAdapter adapter)
        {
            for (int i = 0; i < adapter.FieldCount; i++)
            {
                string name = adapter.GetName(i);
                object value = null;
                adapter.Bind(name, ref value);
                this._items.Add(new KeyValuePair<string, object>(name, value));
            }
        }

        public string GetFileLinkFullPath(string p)
        {
            string node = this["NODE"].ToString();
            string date = this["TIME_DATE"].ToString().Substring(0,8);
            string file = this[p].ToString();

            string fullPath = string.Format("\\\\Szn-sfl-clst-01\\Results\\NODE{0}\\{1}\\{2}",
                node, date, file);
            return fullPath;
        }
    }

    public class PcasResultEntry : DbObjectBase
    {
        public string SerialNo;
        public string ResultTable;
        //public DateTime TimeDate;
        public string TimeDateString;
        public string TestStatus;
        public string DeviceType;
        public string TestStage;
        public string Specification;
        //public string Schema;
        private DateTime ?_datetime;

        public DateTime TimeDate
        {
            get
            {
                if (this._datetime == null)
                {
                    int year = int.Parse(this.TimeDateString.Substring(0, 4));
                    int month = int.Parse(this.TimeDateString.Substring(4, 2));
                    int day = int.Parse(this.TimeDateString.Substring(6, 2));
                    int hour = int.Parse(this.TimeDateString.Substring(8, 2));
                    int minute = int.Parse(this.TimeDateString.Substring(10, 2));
                    int second = int.Parse(this.TimeDateString.Substring(12, 2));
                    this._datetime= new DateTime(year, month, day, hour, minute, second);
                }

                return this._datetime.Value;
            }
        }

        //public PcasResultEntry(DbDataReader reader, string schema)
        //{
        //    this.Schema = schema;
        //    this.SerialNo = reader["SERIAL_NO"] as string;
        //    this.ResultTable = reader["RESULTS_TABLE_ID"] as string;
        //    this.TimeDateString = reader["TIME_DATE"] as string;
        //    int year = int.Parse(this.TimeDateString.Substring(0, 4));
        //    int month = int.Parse(this.TimeDateString.Substring(4, 2));
        //    int day = int.Parse(this.TimeDateString.Substring(6, 2));
        //    int hour = int.Parse(this.TimeDateString.Substring(8, 2));
        //    int minute = int.Parse(this.TimeDateString.Substring(10, 2));
        //    int second = int.Parse(this.TimeDateString.Substring(12, 2));
        //    this.TimeDate = new DateTime(year, month, day, hour, minute, second);
        //    this.TestStatus = reader["TEST_STATUS"] as string;
        //    this.DeviceType = reader["DEVICE_TYPE"] as string;
        //    this.TestStage = reader["TEST_STAGE"] as string;
        //    this.Specification = reader["SPEC_ID"] as string;
        //}

        public override void Bind(DbObjectAdapter adapter)
        {
            adapter.Bind("SERIAL_NO", ref this.SerialNo);
            adapter.Bind("RESULTS_TABLE_ID", ref this.ResultTable );
            adapter.Bind("TIME_DATE", ref this.TimeDateString );
            adapter.Bind("TEST_STAGE", ref this.TestStage);
            adapter.Bind("TEST_STATUS", ref this.TestStatus);
            adapter.Bind("DEVICE_TYPE", ref this.DeviceType);
            adapter.Bind("SPEC_ID", ref this.Specification);
        }
    }

    public class PcasResultQueryer : IDisposable 
    {
        private string _deviceType;
        private string _specification;
        private string _schema;
        private string _testStage;
        private string _serialNo;

        private string _dataSource;
        private OracleConnection _connection;

        public PcasResultQueryer(string dataSource)
        {
            this._dataSource = dataSource;

            OracleConnectionStringBuilder builder = new OracleConnectionStringBuilder ();
            builder.DataSource = dataSource;
            builder.UserID = "pcas";
            builder.Password = "test";

            this._connection = new OracleConnection(builder.ConnectionString);

            this._connection.Open();
        }

        public void Dispose()
        {
            if (this._connection != null)
            {
                this._connection.Close();
                this._connection = null;
            }
        }

        public string DeviceType
        {
            get
            {
                return this._deviceType;
            }
            set
            {
                this._deviceType = value;
            }
        }

        public string Specification
        {
            get
            {
                return this._specification;
            }
            set
            {
                this._specification = value;
            }
        }

        public string Schema
        {
            get
            {
                return this._schema;
            }
            set
            {
                this._schema = value;
            }
        }

        public string TestStage
        {
            get
            {
                return this._testStage;
            }
            set
            {
                this._testStage = value;
            }
        }

        public string SerialNo
        {
            get
            {
                return this._serialNo;
            }
            set
            {
                this._serialNo = value;
            }
        }

        private string GenerateWhereClause()
        {
            StringBuilder whereClause = new StringBuilder ();

            if ( (this._serialNo != null) && (this._serialNo.Length != 0 ))
            {
                whereClause.AppendFormat(
                    whereClause.Length == 0 ? " serial_no='{0}' " : " and serial_no='{0}' ",
                    this._serialNo);
            }
            if ((this._testStage != null) && (this._testStage.Length != 0))
            {
                whereClause.AppendFormat(
                    whereClause.Length == 0 ? " test_stage='{0}' " : " and test_stage='{0}' ",
                    this._testStage);
            }
            if ((this._deviceType != null) && (this._deviceType.Length != 0))
            {
                whereClause.AppendFormat(
                    whereClause.Length == 0 ? " device_type='{0}' " : " and device_type='{0}' ",
                    this._deviceType);
            }
            if ((this._specification != null) && (this._specification.Length != 0))
            {
                whereClause.AppendFormat(
                    whereClause.Length == 0 ? " spec_id='{0}' " : " and spec_id='{0}' ",
                    this._specification);
            }

            return whereClause.ToString();
        }
    }
}

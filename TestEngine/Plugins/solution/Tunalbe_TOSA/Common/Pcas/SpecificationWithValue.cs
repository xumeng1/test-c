using System;
using System.Collections.Generic;
using System.Text;

namespace Pcas
{
    public class SpecificationWithValue : Specification
    {
        private Dictionary<LimitItem, object> _limitValues = new Dictionary<LimitItem, object>();

        public SpecificationWithValue(Specification spec)
        {
            this._list.AddRange(spec._list);
        }

        public void AddValue(LimitItem limit, object value)
        {
            this._limitValues.Add(limit, value);
        }

        public object GetValue(LimitItem limit)
        {
            return this._limitValues[limit];
        }
    }
}

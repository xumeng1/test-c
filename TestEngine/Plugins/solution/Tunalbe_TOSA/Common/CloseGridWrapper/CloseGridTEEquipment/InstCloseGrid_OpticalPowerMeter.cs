using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestLibrary.Algorithms;

namespace Bookham.Toolkit.CloseGrid.TEEquipment
{
    /// <summary>
    /// Optical Power Meter as represented by CloseGrid
    /// </summary>
    public class InstCloseGrid_OpticalPowerMeter : InstType_OpticalPowerMeter
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="powerMeter">Which CloseGrid Power Meter</param>
        /// <param name="chassis">Chassis</param>
        internal InstCloseGrid_OpticalPowerMeter(PowerMeterHead powerMeter, Chassis_CloseGrid chassis)
            : base("CloseGrid_" + powerMeter, "InstCloseGrid_OpticalPowerMeter", powerMeter.ToString(),
              "", chassis)
        {
            closeGridWrapper = CloseGridWrapper.Singleton;
            chassisCloseGrid = chassis;
            this.PowerMeter = powerMeter;
            cachedCalOffset_dB = 0.0;
            SetDefaultState();
        }

        #region Private data
        private CloseGridWrapper closeGridWrapper; //Jack.Zhang
        private Chassis_CloseGrid chassisCloseGrid;
        private InstType_OpticalPowerMeter.MeterMode cachedMode;
        private double cachedWavelength_nm;
        private double cachedCalOffset_dB;
        #endregion

        /// <summary>
        /// Power Meter within CloseGrid that this represents
        /// </summary>
        public readonly PowerMeterHead PowerMeter;


        /// <summary>
        /// Firmware version - just return as for Chassis, as CloseGrid doesn't
        /// make it easy to get this specific info
        /// </summary>
        public override string FirmwareVersion
        {
            get { return chassisCloseGrid.FirmwareVersion; }
        }

        /// <summary>
        /// Hardware identity - just return as for Chassis, as CloseGrid doesn't
        /// make it easy to get this specific info
        /// </summary>
        public override string HardwareIdentity
        {
            get { return chassisCloseGrid.HardwareIdentity; }
        }

        /// <summary>
        /// Set to mW mode and a cal wavelength of 1550.0 nm and 0.0 dB
        /// </summary>
        public override void SetDefaultState()
        {
            this.CalOffset_dB = 0.0;
            this.Wavelength_nm = 1550.0;
            this.Mode = MeterMode.Absolute_mW;
        }

        /// <summary>
        /// Averaging Time - not possible
        /// </summary>
        public override double AveragingTime_s
        {
            get
            {
                throw new InstrumentException("Get of AveragingTime_s is not possible.");
            }
            set
            {
                throw new InstrumentException("Set of AveragingTime_s is not possible.");
            }
        }

        /// <summary>
        /// Cal offset
        /// </summary>
        public override double CalOffset_dB
        {
            get
            {
                return cachedCalOffset_dB;
            }
            set
            {
                cachedCalOffset_dB = value;
            }
        }

        /// <summary>
        /// Power meter mode
        /// </summary>
        public override InstType_OpticalPowerMeter.MeterMode Mode
        {
            get
            {
                return cachedMode;
            }
            set
            {
                // TODO - support calibration dB and other modes via PowerCorrectedMeasure!
                switch (value)
                {
                    case MeterMode.Absolute_mW: break;
                    default: throw new InstrumentException("Invalid Meter Mode: " + cachedMode);
                }
                cachedMode = value;
            }
        }

        /// <summary>
        /// Range - only AutoRange
        /// </summary>
        public override double Range
        {
            get
            {
                return InstType_OpticalPowerMeter.AutoRange;
            }
            set
            {
                if (IsAutoRange(value)) return;
                throw new InstrumentException("Only allowed to AutoRange this power meter"); 
            }
        }        

        /// <summary>
        /// Reference power - not possible
        /// </summary>
        public override double ReferencePower
        {
            get
            {
                throw new InstrumentException("Get of ReferencePower is not possible.");
            }
            set
            {
                throw new InstrumentException("Set of ReferencePower is not possible.");
            }
        }

        /// <summary>
        /// Power Meter calibration wavelength nm
        /// </summary>
        public override double Wavelength_nm
        {
            get
            {
                return cachedWavelength_nm;
            }
            set
            {
                cachedWavelength_nm = value;
            }
        }

        /// <summary>
        /// Zero Dark Current cal isn't possible
        /// </summary>
        public override void ZeroDarkCurrent_End()
        {
            throw new InstrumentException("ZeroDarkCurrent_End is not possible.");
        }

        /// <summary>
        /// Zero Dark Current cal isn't possible
        /// </summary>
        public override void ZeroDarkCurrent_Start()
        {
            throw new InstrumentException("ZeroDarkCurrent_Start is not possible.");
        }

        /// <summary>
        /// Read power
        /// </summary>
        /// <returns>Power according to the mode</returns>
        public override double ReadPower()
        {
            double calFreqGHz = Alg_FreqWlConvert.Wave_nm_TO_Freq_GHz(cachedWavelength_nm);
            double rawPow_mW = closeGridWrapper.ReadPower_mW(PowerMeter, calFreqGHz);
            double rawPow_dBm = Alg_PowConvert_dB.Convert_mWtodBm(rawPow_mW);
            double corrPow_dBm = rawPow_dBm - cachedCalOffset_dB;

            double pow = Double.NaN;
            switch (cachedMode)
            {
                case MeterMode.Absolute_dBm:
                    pow = corrPow_dBm;
                    break;
                case MeterMode.Absolute_mW:
                    pow = Alg_PowConvert_dB.Convert_dBmtomW(corrPow_dBm);
                    break;                
                default: throw new InstrumentException("Invalid Meter Mode: " + cachedMode);
            }
            return pow;            
        }

        /// <summary>
        /// Override of MaxMinOn, not yet implemented.
        /// </summary>
        public override bool MaxMinOn
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        /// <summary>
        /// Override of InstType_OpticalPowerMeter.ReadPowerMaxMin, not implemented.
        /// </summary>
        /// <returns>PowerMaxMin Info.</returns>
        public override InstType_OpticalPowerMeter.PowerMaxMin ReadPowerMaxMin()
        {
            throw new Exception("The method or operation is not implemented.");
        }
    }
}

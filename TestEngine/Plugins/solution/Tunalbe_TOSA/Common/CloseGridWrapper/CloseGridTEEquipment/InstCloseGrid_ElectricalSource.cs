using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestSolution.IlmzCommonData;

namespace Bookham.Toolkit.CloseGrid.TEEquipment
{
    /// <summary>
    /// Electrical source as represented by CloseGrid
    /// </summary>
    public class InstCloseGrid_ElectricalSource : InstType_ElectricalSource
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="section">Which DSDBR section</param>
        /// <param name="chassis">Chassis</param>
        internal InstCloseGrid_ElectricalSource(DSDBRSection section, Chassis_CloseGrid chassis)
            :base("CloseGrid_"+section, "InstCloseGrid_ElectricalSource", section.ToString(),
              "", chassis)
        {
            closeGridWrapper = CloseGridWrapper.Singleton;
            chassisCloseGrid = chassis;
            DsdbrSection = section;
        }

        #region Private data
        private CloseGridWrapper closeGridWrapper;//Jack.Zhang
        private Chassis_CloseGrid chassisCloseGrid;
        private double currentSetPointCache_mA;
        #endregion

        /// <summary>
        /// DSDBR section this electrical source object represents
        /// </summary>
        public readonly DSDBRSection DsdbrSection;

        /// <summary>
        /// Firmware version - just return as for Chassis, as CloseGrid doesn't
        /// make it easy to get this specific info
        /// </summary>
        public override string FirmwareVersion
        {
            get { return chassisCloseGrid.FirmwareVersion; }
        }

        /// <summary>
        /// Hardware identity - just return as for Chassis, as CloseGrid doesn't
        /// make it easy to get this specific info
        /// </summary>
        public override string HardwareIdentity
        {
            get { return chassisCloseGrid.HardwareIdentity; }
        }

        /// <summary>
        /// Set default state
        /// </summary>
        public override void SetDefaultState()
        {
            DSDBRSectionCurrent current = new DSDBRSectionCurrent(false, 0.0);
            closeGridWrapper.SetSectionCurrent(DsdbrSection, current);
        }

        /// <summary>
        /// Get actual current
        /// </summary>
        public override double CurrentActual_amp
        {
            get 
            {
                DSDBRSectionCurrent current = getSectionCurrent();
                return current.Current_mA / 1000.0;
            }
        }

        /// <summary>
        /// Set compliance current - not possible
        /// </summary>
        public override double CurrentComplianceSetPoint_Amp
        {
            get
            {
                throw new InstrumentException("Get of CurrentComplianceSetPoint_Amp is not possible.");
            }
            set
            {
                throw new InstrumentException("Set of CurrentComplianceSetPoint_Amp is not possible.");
            }
        }

        /// <summary>
        /// Current source set-point
        /// </summary>
        public override double CurrentSetPoint_amp
        {
            get
            {
                // setpoint not available via CloseGrid - have to state-cache it in the driver!
                return currentSetPointCache_mA / 1000.0;
            }
            set
            {
                // find out if this source is enabled
                DSDBRSectionCurrent current = new DSDBRSectionCurrent(this.OutputEnabled, value * 1000.0);
                setSectionCurrent(current);
                currentSetPointCache_mA = value * 1000.0;
            }
        }

        
        /// <summary>
        /// Is Output enabled?
        /// </summary>
        public override bool OutputEnabled
        {
            get
            {
                DSDBRSectionCurrent current = getSectionCurrent();
                return current.Enabled;
            }
            set
            {
                DSDBRSectionCurrent current = new DSDBRSectionCurrent(value, 0.0);
                setSectionCurrent(current);
            }
        }

        /// <summary>
        /// Get actual voltage
        /// </summary>
        public override double VoltageActual_Volt
        {
            get 
            {
                double volt_V = closeGridWrapper.ReadVoltage_V(DsdbrSection);
                return volt_V;
            }
        }

        /// <summary>
        /// Get/Set compliance voltage
        /// </summary>
        public override double VoltageComplianceSetPoint_Volt
        {
            get
            {
                double volt_V = closeGridWrapper.GetComplianceVoltage_V(DsdbrSection);
                return volt_V;
            }
            set
            {
                closeGridWrapper.SetComplianceVoltage_V(DsdbrSection, value);
            }
        }

        /// <summary>
        /// Voltage Set Point - not possible
        /// </summary>
        public override double VoltageSetPoint_Volt
        {
            get
            {
                throw new InstrumentException("Get of VoltageSetPoint_Volt is not possible.");
            }
            set
            {
                throw new InstrumentException("Set of VoltageSetPoint_Volt is not possible.");
            }
        }

        #region Private Helper Functions
        private DSDBRSectionCurrent getSectionCurrent()
        {
            DSDBRSectionCurrent current =
                    closeGridWrapper.GetSectionCurrent(DsdbrSection);
            return current;
        }

        private void setSectionCurrent(DSDBRSectionCurrent current)
        {
            closeGridWrapper.SetSectionCurrent(DsdbrSection, current);           
        }
        #endregion

    }
}

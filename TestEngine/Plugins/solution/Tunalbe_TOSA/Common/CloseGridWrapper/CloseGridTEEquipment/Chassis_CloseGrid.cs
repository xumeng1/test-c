using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestSolution.IlmzCommonData;
using Bookham.TestLibrary.InstrTypes;

namespace Bookham.Toolkit.CloseGrid.TEEquipment
{
    /// <summary>
    /// Test Engine Equipment Chassis object for CloseGrid instruments.
    /// Not designed to be created by normal means - use static Singleton 
    /// property to get it from any where!
    /// </summary>
    public sealed class Chassis_CloseGrid : Chassis
    {
        #region Singleton Implementation
        /// <summary>
        /// Singleton object accessor
        /// </summary>
        public static Chassis_CloseGrid Singleton
        {
            get
            {
                return instance;
            }
        }

        /// <summary>
        /// Private constructor
        /// </summary>
        private Chassis_CloseGrid()
            :base("CloseGrid", "Chassis_CloseGrid", "NA")
        {
            // get the closegridwrapper object
            closeGridWrapper = CloseGridWrapper.Singleton;
            // create current source instruments and store in Dictionary
            currentSources = 
                new Dictionary<DSDBRSection, InstType_ElectricalSource>();
            currentSources.Add(DSDBRSection.Gain, 
                new InstCloseGrid_ElectricalSource(DSDBRSection.Gain, this));
            currentSources.Add(DSDBRSection.SOA, 
                new InstCloseGrid_ElectricalSource(DSDBRSection.SOA, this));
            currentSources.Add(DSDBRSection.Rear, 
                new InstCloseGrid_ElectricalSource(DSDBRSection.Rear, this));
            currentSources.Add(DSDBRSection.Phase, 
                new InstCloseGrid_ElectricalSource(DSDBRSection.Phase, this));
            currentSources.Add(DSDBRSection.Front1, 
                new InstCloseGrid_ElectricalSource(DSDBRSection.Front1, this));
            currentSources.Add(DSDBRSection.Front2, 
                new InstCloseGrid_ElectricalSource(DSDBRSection.Front2, this));
            currentSources.Add(DSDBRSection.Front3, 
                new InstCloseGrid_ElectricalSource(DSDBRSection.Front3, this));
            currentSources.Add(DSDBRSection.Front4, 
                new InstCloseGrid_ElectricalSource(DSDBRSection.Front4, this));
            currentSources.Add(DSDBRSection.Front5, 
                new InstCloseGrid_ElectricalSource(DSDBRSection.Front5, this));
            currentSources.Add(DSDBRSection.Front6, 
                new InstCloseGrid_ElectricalSource(DSDBRSection.Front6, this));
            currentSources.Add(DSDBRSection.Front7, 
                new InstCloseGrid_ElectricalSource(DSDBRSection.Front7, this));
            currentSources.Add(DSDBRSection.Front8, 
                new InstCloseGrid_ElectricalSource(DSDBRSection.Front8, this));

            // create power meter instruments and store in Dictionary
            opticalPowerMeters = 
                new Dictionary<PowerMeterHead, InstType_OpticalPowerMeter>();
            opticalPowerMeters.Add(PowerMeterHead.Reference, 
                new InstCloseGrid_OpticalPowerMeter(PowerMeterHead.Reference, this));
            opticalPowerMeters.Add(PowerMeterHead.Filter, 
                new InstCloseGrid_OpticalPowerMeter(PowerMeterHead.Filter, this));
        }

        /// <summary>
        /// The only instance of this object
        /// </summary>
        private static Chassis_CloseGrid instance = new Chassis_CloseGrid();
        #endregion

        #region Private fields
        private CloseGridWrapper closeGridWrapper;//Jack.Zhang
        private Dictionary<DSDBRSection, InstType_ElectricalSource> currentSources;
        private Dictionary<PowerMeterHead, InstType_OpticalPowerMeter> opticalPowerMeters; 
        #endregion

        /// <summary>
        /// Get firmware version
        /// </summary>
        public override string FirmwareVersion
        {
            get 
            {
                CloseGridVersion ver = closeGridWrapper.Version;
                return ver.ReleaseNo;
            }
        }

        /// <summary>
        /// Get hardware identity
        /// </summary>
        public override string HardwareIdentity
        {
            get { return "CloseGrid"; }
        }

        /// <summary>
        /// Current sources
        /// </summary>
        public Dictionary<DSDBRSection, InstType_ElectricalSource> CurrentSources
        {
            get { return currentSources; }
        }

        /// <summary>
        /// Optical power meters
        /// </summary>
        public Dictionary<PowerMeterHead, InstType_OpticalPowerMeter> OpticalPowerMeters
        {
            get { return opticalPowerMeters; }
        }
    }
}

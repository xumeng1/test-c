using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.Toolkit.CloseGrid
{
    /// <summary>
    /// Ramp Direction used for Overall Map
    /// </summary>
    public enum OverallRampDir
    {
        /// <summary>Rear Current Ramped</summary>
        Rear,
        /// <summary>Front Current Ramped</summary>
        Front,
    }

    /// <summary>
    /// Ramp Direction used for Supermode Map
    /// </summary>
    public enum SupermodeRampDir
    {
        /// <summary>Phase Current Ramped</summary>
        Phase,
        /// <summary>Middle-Line (Combination of Front+Rear) Ramped</summary>
        Middle
    }

    internal sealed class RampDirConvert
    {
        private RampDirConvert() { }

        internal static string ToString(OverallRampDir rampDir)
        {
            string rampDirName;
            switch (rampDir)
            {
                case OverallRampDir.Rear: rampDirName = "R"; break;
                case OverallRampDir.Front: rampDirName = "F"; break;
                default:
                    throw new ArgumentException("Invalid RampDir: " + rampDir);
            }
            return rampDirName;
        }

        internal static string ToString(SupermodeRampDir rampDir)
        {
            string rampDirName;
            switch (rampDir)
            {
                case SupermodeRampDir.Phase: rampDirName = "P"; break;
                case SupermodeRampDir.Middle: rampDirName = "M"; break;
                default:
                    throw new ArgumentException("Invalid RampDir: " + rampDir);
            }
            return rampDirName;
        }

        /// <summary>
        /// Method to convert ramp direction string to enum
        /// </summary>
        /// <param name="rampDirString">Ramp direction string</param>
        /// <returns>Ramp direction enum</returns>
        internal static OverallRampDir ToOverallRampDir(string rampDirString)
        {
            if (rampDirString == "R")
            {
                return OverallRampDir.Rear;
            }
            else if (rampDirString == "F")
            {
                return OverallRampDir.Front;
            }            
            else
            {
                throw new ArgumentException("Invalid Ramp Direction: " + rampDirString);
            }
        }

        /// <summary>
        /// Method to convert ramp direction string to enum
        /// </summary>
        /// <param name="rampDirString">Ramp direction string</param>
        /// <returns>Ramp direction enum</returns>
        internal static SupermodeRampDir ToSupermodeRampDir(string rampDirString)
        {
            if (rampDirString == "P")
            {
                return SupermodeRampDir.Phase;
            }
            else if (rampDirString == "M")
            {
                return SupermodeRampDir.Middle;
            }
            else
            {
                throw new ArgumentException("Invalid Ramp Direction: " + rampDirString);
            }
        }
    }
}
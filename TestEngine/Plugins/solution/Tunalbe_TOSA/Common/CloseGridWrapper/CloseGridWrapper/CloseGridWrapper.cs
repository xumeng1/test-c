using System;
using System.Collections.Generic;
using System.Text;
//using AxCGSystemAXCLib;
using System.IO;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestSolution.IlmzCommonData;
using Bookham.TestLibrary.Utilities;
using Bookham.TestLibrary.Algorithms;

namespace Bookham.Toolkit.CloseGrid
{
    // Alice.Huang     2010-04-20 
    // Comment all operation related with CloseGrid ActiveX OCX to avoid solution to depend on CloseGrid

    /// <summary>
    /// Close Grid Wrapper
    /// </summary>
    /// <remarks>Only one object of this type can exist - singleton pattern</remarks>
    public sealed class CloseGridWrapper : IDisposable
    {
        #region Constants
        const string registryBase = @"HKEY_LOCAL_MACHINE\SOFTWARE\PXIT\CLose-Grid 5.0\";
        const int swPosnSoaPositive = 1;
        const int swPosnSoaNegative = 2;
        #endregion

        #region Singleton Pattern
        /// <summary>
        ///  Access the Close Grid class as a singleton.
        /// </summary>
        public static CloseGridWrapper Singleton
        {
            get
            {
                return singletonInstance;                
            }
        }

        /// <summary>
        /// This is the singleton instance
        /// </summary>
        static private CloseGridWrapper singletonInstance = new CloseGridWrapper();

        /// <summary>
        /// Private constructor
        /// </summary>
        private CloseGridWrapper()
        {
            // startup the wrapper thread
            cgWrapThread = CGWrapThread.Singleton;
            cgWrapThread.Start();
            // get the OCX object
            //closeGrid = cgWrapThread.CloseGridOCX;

            // read the registry to find simulation flag
            // will also do a basic check that the Registry tree exists OK
            string keyPath = registryBase + @"\Software Configuration\DSDBR01";
            string keySimMode = "Stub hardware calls";

            // Kits has not regedit the CloseGrid, so set a normal value "0" or "1" by tim
            string simModeStr = "0";  //(string)Microsoft.Win32.Registry.GetValue(keyPath, keySimMode, ""); 

            bool isSimMode;
            if (simModeStr == "1") isSimMode = true;
            else if (simModeStr == "0") isSimMode = false;
            else throw new ArgumentException("Invalid Close Grid registry simulation flag: " + simModeStr +
                ". Please check that the CloseGrid registry is correctly installed as per the Close Grid manual");
            closeGridHardwareSimulation = isSimMode;
        }        
        #endregion

        #region Private data
        /// <summary>
        /// Close Grid Active X OCX (private)
        /// </summary>
        //private AxCGSystemAXC closeGrid;

        /// <summary>
        /// Wrapper thread
        /// </summary>
        private CGWrapThread cgWrapThread;

        /// <summary>
        /// Switch Path Manager
        /// </summary>
        private Inst_DigiIoComboSwitch soaRelays;       

        /// <summary>
        /// Cache of the close grid test info - set-up as part of the setup operation.
        /// </summary>
        private CloseGridTestInfo closeGridTestInfo;

        /// <summary>
        /// Close Grid simulation flag cache (read from registry at startup)
        /// </summary>
        private bool closeGridHardwareSimulation;
        #endregion

        ///// <summary>
        ///// Version info of CloseGrid
        ///// </summary>
        ///// <returns>Version class</returns>
        //public CloseGridVersion Version
        //{
        //    get
        //    {
        //        CloseGridVersion cgv = new CloseGridVersion(closeGrid);
        //        return cgv;
        //    }
        //}

        ///// <summary>
        ///// Status 
        ///// </summary>
        ///// <returns>Status string</returns>
        //public CloseGridState Status()
        //{
        //    string stateStr;
        //    short sh1, sh2, sh3, sh4, sh5;
        //    closeGrid.getState(out stateStr, out sh1, out sh2, out sh3, out sh4,
        //        out sh5);
        //    CloseGridState state = (CloseGridState)Enum.Parse(typeof(CloseGridState), stateStr);
        //    return state;
        //}

        ///// <summary>
        ///// Get percent complete
        ///// </summary>
        ///// <returns>Percentage complete</returns>
        //public short GetPercentComplete()
        //{
        //    short pcComplete;
        //    closeGrid.getPercentComplete(out pcComplete);
        //    return pcComplete;
        //}

        ///// <summary>
        ///// Startup the Close Grid ActiveX
        ///// </summary>
        ///// <param name="soaRelays">Soa Relays as a switch object 
        ///// - position 1= SOA positive
        ///// - position 2= SOA negative</param>
        //public void Startup(Inst_DigiIoComboSwitch soaRelays)
        //{
        //    closeGridTestInfo = null;
        //    closeGrid.startup();
        //    this.soaRelays = soaRelays;
        //}

        /// <summary>
        /// Setup CloseGrid
        /// </summary>
        /// <param name="laserType">Laser Type</param>
        /// <param name="laserId">Laser ID</param>
        public CloseGridTestInfo Setup(string laserType, string laserId)
        {            
            // set relays to positive position
            //this.soaRelays.SwitchState = swPosnSoaPositive;

            // Alice.Huang     2010-04-20
            // Comment all operation related with CloseGrid ActiveX OCX to avoid solution to depend on CloseGrid
            // [y][M][d][H][min][s]
            string dateTimeStamp= DateTime .Now.ToString ("yyyyMMddHHmmss");
            //closeGrid.setup(laserType, laserId, out dateTimeStamp);

            // read the registry to find where to write the ITU grid files
            // both estimate and main ITU frequency
            string keyPath = registryBase + @"\Software Configuration\" + laserType;
            string keyResultsDir = "Results Directory";
            string resultsDir = (string) Microsoft.Win32.Registry.GetValue(keyPath, keyResultsDir, "");
            
            // check read registry OK
            if (resultsDir == "") throw new ArgumentException("Results directory not specified in Close Grid Registry!");
            
            // create our test info object
            CloseGridTestInfo testInfo = new CloseGridTestInfo(resultsDir, laserType, laserId, dateTimeStamp);
            // cache it!
            this.closeGridTestInfo = testInfo;
            // return it
            return testInfo;
        }

        /// <summary>
        /// Get the Close Grid Test Info
        /// </summary>
        public CloseGridTestInfo CloseGridTestInfo
        {
            get
            {
                return this.closeGridTestInfo;
            }
        }

        ///// <summary>
        ///// Simulation mode
        ///// </summary>
        //public bool SimulationMode
        //{
        //    get
        //    {
        //        return closeGridHardwareSimulation;
        //    }
        //}

        ///// <summary>
        ///// Read optical frequency
        ///// </summary>
        ///// <returns>Frequency in GHz</returns>
        //public double ReadFrequency_GHz()
        //{
        //    double freqGHz;
        //    closeGrid.readFrequency(out freqGHz);
        //    return freqGHz;
        //}

        ///// <summary>
        ///// Read raw optical power
        ///// </summary>
        ///// <param name="head">Which power meter head</param>
        ///// <returns>Power in mW</returns>
        //public double ReadPower_mW(PowerMeterHead head)
        //{
        //    string powerMeterModule = "PowerMeterII";
        //    short powerMeterChannel=0;
        //    switch (head)
        //    {
        //        case PowerMeterHead.Reference:
        //            powerMeterChannel = 1;
        //            break;
        //        case PowerMeterHead.Filter:
        //            powerMeterChannel = 2;
        //            break;
        //        default:
        //            throw new ArgumentException("Invalid PowerMeterHead: " + head);

        //    }
        //    double pow_mW;
        //    closeGrid.readOpticalPower(powerMeterModule, powerMeterChannel, out pow_mW);
        //    return pow_mW;
        //}

        ///// <summary>
        ///// Read raw optical power
        ///// </summary>
        ///// <param name="head">Which power meter head</param>
        ///// <param name="calFreqGHz">Which frequency in GHz is the signal (for power meter cal)</param>
        ///// <returns>Power in mW</returns>
        //public double ReadPower_mW(PowerMeterHead head, double calFreqGHz)
        //{
        //    string powerMeterModule = "PowerMeterII";
        //    short powerMeterChannel = 0;
        //    switch (head)
        //    {
        //        case PowerMeterHead.Reference:
        //            powerMeterChannel = 1;
        //            break;
        //        case PowerMeterHead.Filter:
        //            powerMeterChannel = 2;
        //            break;
        //        default:
        //            throw new ArgumentException("Invalid PowerMeterHead: " + head);

        //    }
        //    double pow_mW;
        //    closeGrid.readOpticalPowerAtFreq(powerMeterModule, powerMeterChannel, 
        //        calFreqGHz, out pow_mW);
        //    return pow_mW;
        //}

        ///// <summary>
        ///// Read locker photocurrent
        ///// </summary>
        ///// <param name="head">Which locker current</param>
        ///// <returns>Current in mA</returns>
        //public double ReadLockerCurrent_mA(LockerCurrentHead head)
        //{
        //    string photoIMeterModule = "PowerMeterEE";
        //    short photoIMeterChannel = 0;
        //    switch (head)
        //    {
        //        case LockerCurrentHead.Transmit:
        //            photoIMeterChannel = 1;
        //            break;
        //        case LockerCurrentHead.Reflect:
        //            photoIMeterChannel = 2;
        //            break;
        //        default:
        //            throw new ArgumentException("Invalid PowerMeterHead: " + head);

        //    }
        //    double curr_mA;
        //    closeGrid.readPhotodiodeCurrent(photoIMeterModule, photoIMeterChannel, out curr_mA);
        //    return curr_mA;
        //}

        ///// <summary>
        ///// Read TEC Temperature in degrees C
        ///// </summary>
        ///// <returns></returns>
        //public double ReadTECTemp_degC()
        //{
        //    double temp_degC;
        //    closeGrid.readTemperature(out temp_degC);
        //    return temp_degC;
        //}

        ///// <summary>
        ///// Get all DSDBR section currents
        ///// </summary>
        ///// <returns>DSDBR Sections structure</returns>
        //public DSDBRSectionCurrents GetCurrents()
        //{
        //    DSDBRSectionCurrents d;
        //    d.Gain = GetSectionCurrent(DSDBRSection.Gain);
        //    d.SOA = GetSectionCurrent(DSDBRSection.SOA);
        //    d.Rear = GetSectionCurrent(DSDBRSection.Rear);
        //    d.Phase = GetSectionCurrent(DSDBRSection.Phase);
        //    d.Front1 = GetSectionCurrent(DSDBRSection.Front1);
        //    d.Front2 = GetSectionCurrent(DSDBRSection.Front2);
        //    d.Front3 = GetSectionCurrent(DSDBRSection.Front3);
        //    d.Front4 = GetSectionCurrent(DSDBRSection.Front4);
        //    d.Front5 = GetSectionCurrent(DSDBRSection.Front5);
        //    d.Front6 = GetSectionCurrent(DSDBRSection.Front6);
        //    d.Front7 = GetSectionCurrent(DSDBRSection.Front7);
        //    d.Front8 = GetSectionCurrent(DSDBRSection.Front8);
        //    return d;
        //}

        ///// <summary>
        ///// Set all DSDBR section currents - use clockwise tuning rules
        ///// </summary>
        ///// <param name="d">Section currents to apply.</param>
        //public void SetCurrents(DSDBRSectionCurrents d)
        //{
        //    SetSectionCurrent(DSDBRSection.Gain, d.Gain);
        //    SetSectionCurrent(DSDBRSection.Front1, d.Front1);
        //    SetSectionCurrent(DSDBRSection.Front2, d.Front2);
        //    SetSectionCurrent(DSDBRSection.Front3, d.Front3);
        //    SetSectionCurrent(DSDBRSection.Front4, d.Front4);
        //    SetSectionCurrent(DSDBRSection.Front5, d.Front5);
        //    SetSectionCurrent(DSDBRSection.Front6, d.Front6);
        //    SetSectionCurrent(DSDBRSection.Front7, d.Front7);
        //    SetSectionCurrent(DSDBRSection.Front8, d.Front8);
        //    SetSectionCurrent(DSDBRSection.Rear, d.Rear);
        //    SetSectionCurrent(DSDBRSection.Phase, d.Phase);
        //    SetSectionCurrent(DSDBRSection.SOA, d.SOA);            
        //}

        ///// <summary>
        ///// Get Section Current
        ///// </summary>
        ///// <param name="section">Which section?</param>
        ///// <returns>Current source status</returns>
        //public DSDBRSectionCurrent GetSectionCurrent(DSDBRSection section)
        //{
        //    // special rules for setting SOA
        //    // tf if (section == DSDBRSection.SOA) return getSoaCurrent();

        //    string sectionName = sectionCloseGridName(section);            
        //    DSDBRSectionCurrent sectionCurrent;
        //    short shortVal;
        //    closeGrid.getOutputConnection(sectionName, out shortVal);
        //    closeGrid.getCurrent(sectionName, out sectionCurrent.Current_mA);
        //    sectionCurrent.Enabled = (shortVal != 0);
        //    return sectionCurrent;
        //}

        ///// <summary>
        ///// Read voltage across a DSDBR section
        ///// </summary>
        ///// <param name="section">Which section?</param>
        ///// <returns>Volts across the section (V)</returns>
        //public double ReadVoltage_V(DSDBRSection section)
        //{
        //    // TODO - do we need to check polarity of SOA before reading (and show negative V for negative I?)
        //    string sectionName = sectionCloseGridName(section);            
        //    double volts_V;
        //    closeGrid.readVoltage(sectionName, out volts_V);
        //    return volts_V;
        //}

        ///// <summary>
        ///// Get compliance voltage of a DSDBR section current source
        ///// </summary>
        ///// <param name="section">Which section?</param>
        ///// <returns>Compliance voltage (V)</returns>
        //public double GetComplianceVoltage_V(DSDBRSection section)
        //{
        //    string sectionName = sectionCloseGridName(section);
        //    double volts_V;
        //    closeGrid.getComplianceVoltage(sectionName, out volts_V);
        //    return volts_V;            
        //}

        ///// <summary>
        ///// Set compliance voltage of a DSDBR section current source
        ///// </summary>
        ///// <param name="section">Which section?</param>
        ///// <param name="complianceVoltage_V">Compliance voltage (V)</param>
        //public void SetComplianceVoltage_V(DSDBRSection section, double complianceVoltage_V)
        //{
        //    string sectionName = sectionCloseGridName(section);
        //    closeGrid.setComplianceVoltage(sectionName, complianceVoltage_V);            
        //}

        ///// <summary>
        ///// Set Section Current
        ///// </summary>
        ///// <param name="section">Which section</param>
        ///// <param name="sectionCurrent">Required Section Current Status</param>
        //public void SetSectionCurrent(DSDBRSection section, DSDBRSectionCurrent sectionCurrent)
        //{
        //    // special rules for setting SOA
        //    if (section == DSDBRSection.SOA) 
        //        setSoaCurrent(sectionCurrent);
        //    else
        //    {
        //        string sectionName = sectionCloseGridName(section);
        //        short shortVal;
        //        if (sectionCurrent.Enabled)
        //        {
        //            shortVal = 1;
        //        }
        //        else
        //        {
        //            shortVal = 0;
        //        }
        //        closeGrid.setOutputConnection(sectionName, shortVal);
        //        closeGrid.setCurrent(sectionName, sectionCurrent.Current_mA);
        //    }
        //}

        ///// <summary>
        ///// Start screening overall map
        ///// </summary>
        //public void StartLaserScreeningOverallMap()
        //{
        //    closeGrid.startLaserScreeningOverallMap();
        //}

        ///// <summary>
        ///// Get overall map results
        ///// </summary>
        ///// <returns>Overall map results object</returns>
        //public OverallMapResults GetScreeningOverallMapResults()
        //{
        //    string resultsDir, laserType, laserId, dateTimeStamp, rampDir;
        //    short mapPassed, supermodeCount;
        //    object avgPowers;

        //    closeGrid.getScreeningOverallMapResults(out resultsDir, out laserType,
        //        out laserId, out dateTimeStamp, out rampDir, out mapPassed, out supermodeCount,
        //        out avgPowers);

        //    string collatedDFSFile = collateDFSMaps(this.closeGridTestInfo, rampDir);

        //    OverallMapResults mapRes = new OverallMapResults(this.closeGridTestInfo, rampDir, mapPassed, supermodeCount,
        //        avgPowers, collatedDFSFile);
        //    return mapRes;
        //}

        ///// <summary>
        ///// Load overall map  - call GetScreeningOverallMapResults to return data!
        ///// </summary>
        ///// <param name="info">Info describing what to load</param>
        ///// <param name="rampDir">Ramp direction</param>
        //public void LoadOverallMap(CloseGridTestInfo info, OverallRampDir rampDir)
        //{
        //    string rampDirStr = RampDirConvert.ToString(rampDir);
        //    closeGrid.loadOverallMap(info.ResultsDir, info.LaserType, info.LaserId,
        //        info.DateTimeStamp, rampDirStr);
        //    // update cached test info
        //    this.closeGridTestInfo = info;            
        //}
       
        ///// <summary>
        ///// Start Supermode screening
        ///// </summary>
        ///// <param name="supermode"></param>
        //public void StartLaserScreeningSMMap(short supermode)
        //{
        //    closeGrid.startLaserScreeningSMMap(supermode);
        //}

        ///// <summary>
        ///// Get Supermode Map results
        ///// </summary>
        ///// <param name="supermode"></param>
        //public SMMapResults GetScreeningSMMapResults(short supermode)
        //{
        //    string resultsDir, laserType, laserId, dateTimeStamp, rampDir;
        //    short mapPassed, longitudinalModeCount;            

        //    closeGrid.getScreeningSMMapResults(supermode, out resultsDir, out laserType,
        //        out laserId, out dateTimeStamp, out rampDir, out mapPassed, out longitudinalModeCount);

        //    SMMapResults mapRes = new SMMapResults(supermode, rampDir, this.closeGridTestInfo, mapPassed, longitudinalModeCount);
        //    return mapRes;
        //}

        ///// <summary>
        ///// Load Supermode Map results from file - call GetScreeningSMMapResults to
        ///// get the data back again!
        ///// </summary>
        ///// <param name="info">Test information describing the test</param>
        ///// <param name="supermode">Supermode number 0 to N-1</param>
        ///// <param name="rampDir">Ramp direction</param>
        //public void LoadSMMap(short supermode, CloseGridTestInfo info, SupermodeRampDir rampDir)
        //{
        //    string rampDirStr = RampDirConvert.ToString(rampDir);
        //    closeGrid.loadSMMap(info.ResultsDir, info.LaserType, info.LaserId,
        //        info.DateTimeStamp, supermode, rampDirStr);            
        //}

        /// <summary>
        /// Reads the first entry from each of the midline supermode files.
        /// </summary>
        /// <param name="numberOfSupermodes">The number of supermodes</param>
        /// <returns>An array containing setup conditions</returns>
        public MidChannelSetup[] GetImSetupForChannelCal(int numberOfSupermodes)
        {
            MidChannelSetup[] midChannelData = new MidChannelSetup[numberOfSupermodes + 1];
            ImSetupData imSetupData = new ImSetupData(closeGridTestInfo);

            for (int i = 0; i < numberOfSupermodes; i++)
            {
                imSetupData.ReadImSetup(i);
                midChannelData[i] = imSetupData.FirstEntry;
            }
            midChannelData[numberOfSupermodes] = imSetupData.LastEntry;

            return midChannelData;
        }

        ///// <summary>
        ///// Collate Pass/Fail results
        ///// </summary>
        ///// <returns>Collated results</returns>
        //public CollatedPassFailResults CollatePassFailResults()
        //{
        //    string resultsDir, dateTimeStamp;
        //    short overallMapPassed, overallPass;
        //    object mapsPassedArray;

        //    closeGrid.collatePassFailResults(out resultsDir,
        //        out dateTimeStamp, out overallMapPassed, out mapsPassedArray,
        //        out overallPass);
        //    CollatedPassFailResults res = new CollatedPassFailResults(this.closeGridTestInfo,
        //        overallMapPassed, mapsPassedArray, overallPass);
        //    return res;
        //}

        ///// <summary>
        ///// Replaces the currents of a middle line of a supermode (on Overall Map) 
        ///// with currents from an �Im_*?file.
        ///// </summary>
        ///// <param name="supermode">Supermode number</param>
        ///// <param name="imFilename">Filename to load</param>
        //public void LoadImFromFile(short supermode, string imFilename)
        //{
        //    closeGrid.loadImFromFile(supermode, imFilename);
        //}

        ///// <summary>
        ///// Replaces the currents of a middle line of frequency of a longitudinal mode 
        ///// (on Supermode Map) with currents from an �ImiddleFreq_*?file.
        ///// </summary>
        ///// <param name="supermode">Supermode number</param>
        ///// <param name="longitudinalModeNbr">Longitudinal mode number</param>
        ///// <param name="imiddleFreqFilename">Filename to load</param>
        //public void LoadImiddleFreqFromFile(short supermode, short longitudinalModeNbr,
        //    string imiddleFreqFilename)
        //{
        //    closeGrid.loadImiddleFreqFromFile(supermode, longitudinalModeNbr, imiddleFreqFilename);
        //}

        ///// <summary>
        ///// Generate Polynomial Coefficients File for CloseGrid to use
        ///// </summary>
        ///// <param name="sourceFile">Polynomial Coefficients Source File</param>
        //public void GeneratePolyCoeffFile(string sourceFile)
        //{
        //    // Read the registry to find the polynomial coefficients file path
        //    string keyPath = registryBase + @"\Software Configuration\"
        //        + closeGridTestInfo.LaserType + @"\Coarse Frequency";
        //    string keyNamePolyCoeffs = "Polynomial Coefficients Filepath";
        //    string filePathPolyCoeffs = (string)Microsoft.Win32.Registry.GetValue(keyPath, keyNamePolyCoeffs, "");

        //    // Copy the source file to destination
        //    File.Copy(sourceFile, filePathPolyCoeffs, true);

        //    // Need to set coeffs as this default coeffs were read when startup CG?
        //    List<string> coeffsListStr = CsvDataLookup.GetColumn(filePathPolyCoeffs, 1);
        //    double[] coeffsArrayDouble = new double[coeffsListStr.Count];
        //    for (int i = 0; i < coeffsListStr.Count; i++)
        //    {
        //        coeffsArrayDouble[i] = double.Parse(coeffsListStr[i]);
        //    }
        //    this.SetCoarseFreqPolyCoeffs(coeffsArrayDouble);
        //}

        /// <summary>
        /// Generate ITU Grid file for CloseGrid to use
        /// </summary>
        /// <param name="freqsGHz">Array of frequencies in GHz in the grid</param>
        public void GenerateITUGrid(double[] freqsGHz)
        {
            // read the registry to find where to write the ITU grid files
            // both estimate and main ITU frequency
            string keyPath = registryBase + @"\Software Configuration\" 
                + closeGridTestInfo.LaserType + @"\ITU Grid";
            string keyNameEst = "Estimates ITU Grid Filepath";
            string keyNameItu = "ITU Grid Filepath";
            string filenameEst = (string) Microsoft.Win32.Registry.GetValue(keyPath, keyNameEst, "");
            string filenameItu = (string) Microsoft.Win32.Registry.GetValue(keyPath, keyNameItu, "");

            // write the itu file
            using (StreamWriter sw = new StreamWriter(filenameEst))
            {
                foreach (double d in freqsGHz)
                {
                    string str = d.ToString();
                    sw.WriteLine(str);
                }
            }
            
            if (filenameEst != filenameItu)
            {
            // copy estimates file to the proper ITU file
            File.Copy(filenameEst, filenameItu, true);
            }
        }

        /// <summary>
        /// Generate ITU Grid file for CloseGrid to use
        /// </summary>
        /// <param name="startGHz">Start frequency in GHz</param>
        /// <param name="spacingGHz">Frequency spacing in GHz</param>
        /// <param name="nbrChannels">Number of ITU channels</param>
        public void GenerateITUGrid(double startGHz, double spacingGHz, int nbrChannels)
        {
            // generate frequency array
            double[] freqsGHz = new double[nbrChannels];
            double currentFreq = startGHz;
            for (int ii = 0; ii < nbrChannels; ii++)
            {
                freqsGHz[ii] = currentFreq;
                currentFreq += spacingGHz;
            }

            // now generate the grid file (use the array overload)
            this.GenerateITUGrid(freqsGHz);
        }

        ///// <summary>
        ///// Calculate coarse frequency polynomial fit coefficients by
        ///// using Tunable Laser and comparing WaveMeter and Power Ratio.
        ///// </summary>
        ///// <param name="nbrSamplePts">Number of sample points</param>
        ///// <param name="nbrPolyCoeffs">Number of coefficients to use</param>
        ///// <returns>Results: coefficients and chi squared</returns>
        //public CoarseFreqPolyCoeffResults CalcCoarseFreqPolyCoeffs(short nbrSamplePts, short nbrPolyCoeffs)
        //{
        //    object polyCoeffs;
        //    double chiSquared;
        //    closeGrid.calcCoarseFreqPolyCoeffs(nbrSamplePts, nbrPolyCoeffs,
        //        out polyCoeffs, out chiSquared);
            
        //    CoarseFreqPolyCoeffResults coeffs = new CoarseFreqPolyCoeffResults(polyCoeffs, chiSquared);
        //    return coeffs;
        //}

        ///// <summary>
        ///// Get current coarse frequency polynomial fit coefficients
        ///// </summary>
        ///// <returns>Polynomial coeffs</returns>
        //public double[] GetCoarseFreqPolyCoeffs()
        //{
        //    object polyCoeffsObj;
        //    closeGrid.getCoarseFreqPolyCoeffs(out polyCoeffsObj);
        //    double[] polyCoeffs = (double[])polyCoeffsObj;
        //    return polyCoeffs;
        //}

        ///// <summary>
        ///// Set coarse frequency polynomial fit coefficients
        ///// </summary>
        ///// <param name="polyCoeffs">Polynomial coeffs</param>
        //public void SetCoarseFreqPolyCoeffs(double[] polyCoeffs)
        //{
        //    object obj = polyCoeffs;
        //    closeGrid.setCoarseFreqPolyCoeffs(ref obj);
        //}

        ///// <summary>
        ///// Estimate ITU Operating points
        ///// </summary>
        ///// <returns>ITU Operating points</returns>
        //public EstimateITUPointsResults EstimateITUPoints()
        //{
        //    string resultsDir, dateTimeStamp;
        //    short nbrITUEstimates;

        //    closeGrid.estimateITUOPs(out resultsDir,
        //        out dateTimeStamp, out nbrITUEstimates);
            
        //    EstimateITUPointsResults res =
        //        new EstimateITUPointsResults(this.closeGridTestInfo, nbrITUEstimates);
        //    return res;
        //}

        ///// <summary>
        ///// Start Laser Characterisation
        ///// </summary>
        //public void StartLaserCharacterisation()
        //{
        //    closeGrid.startLaserCharacterisation();
        //}

        ///// <summary>
        ///// Get Laser Characterisation results
        ///// </summary>
        ///// <returns>Laser Characterisation results</returns>
        //public GetCharResults GetCharacterisationResults()
        //{
        //    string resultsDir, dateTimeStamp;
        //    short nbrITUFound;
        //    short nbrITUDuff;

        //    closeGrid.getCharacterisationResults(out resultsDir,
        //        out dateTimeStamp, out nbrITUFound, out nbrITUDuff);
           
        //    GetCharResults res = new GetCharResults(closeGridTestInfo, nbrITUFound,
        //        nbrITUDuff);
        //    return res;
        //}

        /// <summary>
        /// Get Laser Characterisation results
        /// </summary>
        /// <returns>Laser Characterisation results</returns>
        public DsdbrChannelData[] LoadCharacterisationResults(string filename)
        {
            // lookup the ITU channel data
            DsdbrChannelData[] dsdbrChanData = CsvDataLookup.GetItuOperatingPoints(filename);
            return dsdbrChanData;
        }

        ///// <summary>
        ///// Applies the currents found at a given index along the middle line of 
        ///// frequency of a specified longitudinal mode in a supermode. 
        ///// Have to specify whether to leave ISoa and IGain as they are now, 
        ///// or reapply from the Supermode map.
        ///// </summary>
        ///// <param name="supermode">Supermode number</param>
        ///// <param name="longitudinalModeNbr">Longitudinal mode number</param>
        ///// <param name="index">Index along the longitudinal mode</param>
        ///// <param name="useMapSoaGain">Use ISOA and IGain from map (true) or leave as is (false)</param>
        //public void ApplyStableOP(short supermode, short longitudinalModeNbr,
        //    double index, bool useMapSoaGain)
        //{
        //    short useMapSoaGainShort;
        //    if (useMapSoaGain) useMapSoaGainShort = 1;
        //    else useMapSoaGainShort = 0;

        //    closeGrid.applyStableOP(supermode, longitudinalModeNbr, index, useMapSoaGainShort);
        //}

        ///// <summary>
        ///// Shutdown CloseGrid
        ///// </summary>
        //public void Shutdown()
        //{
        //    closeGrid.shutdown();
        //    closeGridTestInfo = null;
        //}

        ///// <summary>
        ///// Stop everything and Shutdown CloseGrid
        ///// </summary>
        //public void StopAndShutdown()
        //{
        //    closeGrid.stopAndShutdown();
        //    closeGridTestInfo = null;
        //}

        /// <summary>
        /// Shutdown Wrapper and dispose of the thread
        /// </summary>
        public void Dispose()
        {
            cgWrapThread.Dispose();
        }

        /// <summary>
        /// Calculate mode aquisition phase current
        /// </summary>
        /// <param name="supermode">Supermode number</param>
        /// <param name="longitudinalModeNbr">Longitudinal mode number</param>
        /// <param name="rearCurrent">Rear current</param>
        /// <returns>Mode acquisition phase current</returns>
        public double IPhaseAcquisition(short supermode, short longitudinalModeNbr, double rearCurrent)
        {
            // Generate filename
            string filename = this.CloseGridTestInfo.ResultsDir + "\\ImiddleLower_LM" + longitudinalModeNbr + "_" + this.CloseGridTestInfo.LaserType + "_" +
                this.CloseGridTestInfo.LaserId + "_" + this.CloseGridTestInfo.DateTimeStamp + "_SM" + supermode + ".csv";

            return CsvDataLookup.IPhaseAcquisition(rearCurrent, filename);
        }

        /// <summary>
        /// Return files for calculating Iphase mode acquisition current
        /// </summary>
        /// <returns></returns>
        public string[] CollectFilesToCalIPhaseAcq()
        {
            string searchPattern = string.Format("ImiddleLower_LM*_" + this.CloseGridTestInfo.LaserType + "_" +
                this.CloseGridTestInfo.LaserId + "_" + this.CloseGridTestInfo.DateTimeStamp + "_SM?.csv");
            return Directory.GetFiles(this.CloseGridTestInfo.ResultsDir, searchPattern);
        }

        /// <summary>
        /// Mild MultiModing Screen
        /// </summary>
        /// <param name="smasString">Sm number as string</param>
        /// <param name="mmmVal">MMM value</param>
        /// <param name="mmmValRearIndex">MMM value rear index</param>
        /// <param name="mmmValPhaseIndex">MMM value phase index</param>
        public void MildMultiModingScreen(string smasString, out double mmmVal, out int mmmValRearIndex, out int mmmValPhaseIndex)
        {
            string filename = this.CloseGridTestInfo.ResultsDir +
                @"\MATRIX_forwardPowerRatio_" + this.closeGridTestInfo.GetFileNameInfoStr() + "_SM" + smasString + "_P.csv";
            int colCount = 0;
            double maxNegStep = 0;
            int minValleyIndex = 0;
            int mmmRearColumn = 0;
            int thisRearCutColumn = 0;
            do
            {
                thisRearCutColumn = mildMultiModeStartPhIndex + colCount * mildMultiModeSampleSpacing;
                List<string> columnStringValues = CsvDataLookup.GetColumn(filename, thisRearCutColumn);
                int upperBound = columnStringValues.Count - 1;

                double[] columnValues = new double[columnStringValues.Count];
                for (int i = 0; i < columnStringValues.Count; i++)
                {
                    columnValues[i] = double.Parse(columnStringValues[i]);
                }

                int[] valleyIndexes = Alg_FindFeature.FindIndexesForAllValleys(columnValues, 1);

                int count = 0;

                do
                {
                    double thisValleyStep = columnValues[valleyIndexes[count]] - columnValues[valleyIndexes[count] - 1];
                    if (thisValleyStep < maxNegStep)
                    {
                        maxNegStep = thisValleyStep;
                        minValleyIndex = valleyIndexes[count];
                        mmmRearColumn = thisRearCutColumn;
                    }
                    count++;

                } while (count < valleyIndexes.GetUpperBound(0));

                colCount++;
            } while (thisRearCutColumn + mildMultiModeSampleSpacing <= rearStepsSize);

            mmmVal = maxNegStep;
            mmmValRearIndex = minValleyIndex;
            mmmValPhaseIndex = mmmRearColumn;

        }

        /// <summary>
        /// Mild MultiModing Screen
        /// </summary>
        /// <param name="smasString">Sm number as string</param>
        /// <param name="numOfFwdFail">Number of failures in forward map</param>
        /// <param name="numOfRevFail">Number of failures in reverse map</param>
        /// <param name="mmmPassFailStatus">MMM Metric, True:PASS, False:FAIL</param>
        public void MildMultiModingScreen(string smasString, out int numOfFwdFail, out int numOfRevFail, out int mmmPassFailStatus)
        {
            // mmmStatus[0]: Forward
            // mmmStatus[1]: Reverse
            bool[] mmmStatus = new bool[2];
            int[] numOfFail = new int[2];

            string[] filesToCheck = new string[2];
            filesToCheck[0] = this.CloseGridTestInfo.ResultsDir +
                @"\MATRIX_forwardPowerRatio_" + this.closeGridTestInfo.GetFileNameInfoStr() + "_SM" + smasString + "_P.csv";
            filesToCheck[1] = this.CloseGridTestInfo.ResultsDir +
                @"\MATRIX_reversePowerRatio_" + this.closeGridTestInfo.GetFileNameInfoStr() + "_SM" + smasString + "_P.csv";

            // Check forward file and reverse file...
            for (int ii = 0; ii < filesToCheck.Length; ii++)
            {
                string filename = filesToCheck[ii];

                // Read file in
                CsvReader Cr = new CsvReader();
                string[][] strData = Cr.ReadFile(filename).ToArray();
                if (strData == null)
                    throw new Exception(string.Format("No data in the file {0}.", filename));

                int rearRows = strData.GetLength(0);
                int phaseColumns = strData[0].GetLength(0);

                // Rear cut differential
                double[,] dblData = new double[rearRows, phaseColumns];
                byte[,] Flag = new byte[rearRows, phaseColumns];
                
                //Modify for MMM by tim at 2008-09-24;
                for (int j = 0; j < phaseColumns; j++)
                {
                    int i = 0;
                    Flag[i, j] = 0;                   
                    for (i = 0; i <= rearRows - 1; i++)
                        dblData[i, j] = Convert.ToDouble(strData[i][j]);
                    double r0 = dblData[0, j];
                    double r1 = dblData[1, j];
                    double dblTemp = 0;
                    //For the Mid line
                    for (i = 1; i <= rearRows - 1; i++)
                    {
                        //For Second and last line  
                        if (i == 1 || i == rearRows - 1)
                        {
                            //Rear Cut Difference
                            dblData[i, j] = (i == 1 ? r0 : r1) + 0.5 * dblData[i, j];
                            dblData[i, j] /= 1.5;
                            dblData[i, j] -= (i == 1 ? r0 : r1);
                            //MMM threshold
                            Flag[i, j] = dblData[i, j] < rearCutDiffThreshold ? (byte)1 : (byte)0;
                            //sum += Flag[i, j];
                            continue;
                        }
                        dblTemp = dblData[i, j];
                        //Rear Cut Difference
                        dblData[i, j] = (dblData[i - 1, j] + dblData[i, j] + dblData[i + 1, j])
                            - (dblData[i, j] + dblData[i - 1, j] + r0);
                        dblData[i, j] /= 3;
                        //MMM threshold
                        Flag[i, j] = dblData[i, j] < rearCutDiffThreshold ? (byte)1 : (byte)0;
                        //sum += Flag[i, j];

                        r0 = r1;
                        r1 = dblTemp;
                    }
                }
                //End Modify;

                // 3 point threshold
                // Finds three or more MMM threshold fail points in a 3x3 box.
                int actualFails = 0;
                for (int i = 2; i < rearRows - 1; i++)
                {
                    for (int j = 1; j < phaseColumns - 1; j++)
                    {
                        byte btmp = 0;
                        for (int x = -1; x <= 1; x++)
                        {
                            for (int y = -1; y <= 1; y++)
                            {
                                btmp += Flag[i + x, j + y];
                            }
                        }
                        actualFails += btmp >= mmmThresholdPoint ? 1 : 0;
                    }
                }

                // Set the status
                numOfFail[ii] = actualFails;
                mmmStatus[ii] = actualFails > allowedFails ? false : true;
            }

            numOfFwdFail = numOfFail[0];
            numOfRevFail = numOfFail[1];
            mmmPassFailStatus = (mmmStatus[0] && mmmStatus[1]) ? 1 : 0;
        }

        /// <summary>
        /// Calculate Overall Map Supermode Status
        /// </summary>
        /// <returns>Status as a bool, true = Pass</returns>
        public bool OverallMapSmStatus()
        {
            bool isPass = true;

            using (CsvReader csvReader = new CsvReader())
            {
                // Generate filename
                string filename = this.CloseGridTestInfo.ResultsDir + "\\collatedPassFailResults_" + this.CloseGridTestInfo.LaserType + "_" +
                    this.CloseGridTestInfo.LaserId + "_" + this.CloseGridTestInfo.DateTimeStamp + ".csv";

                // Return fail if the file is not available
                List<string[]> lines;
                try
                {
                    lines = csvReader.ReadFile(filename);
                }
                catch
                {
                    return false;
                }

                // Find 'Index' text on line before supermode summary results lines 
                //  should be between lines 20 and 35
                int lineNum;
                for (lineNum = overallSmStatusFirstLine; lineNum < overallSmStatusLastLine; lineNum++)
                {
                    if (lines[lineNum][0].ToLower().Contains("index")) break;
                }
                if (lineNum >= overallSmStatusLastLine) return false;

                // Look through lines for any value (which indicates a fail)
                int smLineNum = 0;
                lineNum++;
                while (lines[lineNum + smLineNum][0] != "" && isPass)
                {
                    for (int ii = 1; ii < lines[lineNum + smLineNum].Length; ii++)
                    {
                        if (lines[lineNum + smLineNum][ii].Length > 0)
                        {
                            isPass = false;
                            break;
                        }
                    }
                    smLineNum++;
                }
            }              

            // Return result
            return isPass;
        }

        #region Private Helper Functions
        /// <summary>
        /// Convert our externally visible enum to internal CloseGrid name
        /// </summary>
        /// <param name="section">DSDBRSection enum</param>
        /// <returns>CloseGrid string</returns>
        private static string sectionCloseGridName(DSDBRSection section)
        {
            string sectionName;
            switch (section)
            {
                case DSDBRSection.Gain: sectionName = "Gain"; break;
                case DSDBRSection.SOA: sectionName = "SOA"; break;
                case DSDBRSection.Rear: sectionName = "Rear"; break;
                case DSDBRSection.Phase: sectionName = "Phase"; break;
                case DSDBRSection.Front1: sectionName = "LFS1"; break;
                case DSDBRSection.Front2: sectionName = "LFS2"; break;
                case DSDBRSection.Front3: sectionName = "LFS3"; break;
                case DSDBRSection.Front4: sectionName = "LFS4"; break;
                case DSDBRSection.Front5: sectionName = "LFS5"; break;
                case DSDBRSection.Front6: sectionName = "LFS6"; break;
                case DSDBRSection.Front7: sectionName = "LFS7"; break;
                case DSDBRSection.Front8: sectionName = "LFS8"; break;
                default:
                    throw new ArgumentException("Invalid Section: " + section);
            }
            return sectionName;
        }

        ///// <summary>
        ///// Set SOA current, including setting actuator
        ///// </summary>
        ///// <param name="newSoaCurrent">Current object</param>
        //private void setSoaCurrent(DSDBRSectionCurrent newSoaCurrent)
        //{
        //    string soaModuleName = sectionCloseGridName(DSDBRSection.SOA);
        //    // don't care about direction of relays if we want to disconnect!
            //if (!newSoaCurrent.Enabled)
        //    {
        //        closeGrid.setCurrent(soaModuleName, 0.0);
        //        closeGrid.setOutputConnection(soaModuleName, 0);
        //        return;
        //    }

        //    // if not we'll need to check what's being done now
        //    DSDBRSectionCurrent initSoaCurrent = getSoaCurrent();
        //    // work out the current directions 1=positive, -1=negative, 0=zero
        //    int initCurrentDir = Math.Sign(initSoaCurrent.Current_mA);
        //    int newCurrentDir = Math.Sign(newSoaCurrent.Current_mA);


        //    // Set relays if current is currently off, or there's a change of direction
        //    if ((!initSoaCurrent.Enabled) || (newCurrentDir!=initCurrentDir))
        //    {
        //        // turn down and short the current source
                //closeGrid.setCurrent(soaModuleName, 0.0);
        //        closeGrid.setOutputConnection(soaModuleName, 0);
                
        //        // set SOA relays
        //        setSoaRelays(newCurrentDir);
        //    }

        //    // now set turn on output, and then setup current
        //    closeGrid.setOutputConnection(soaModuleName, 1); 
        //    double newCurrentAbs_mA = Math.Abs(newSoaCurrent.Current_mA);
        //    closeGrid.setCurrent(soaModuleName, newCurrentAbs_mA);
            
        //}       

        ///// <summary>
        ///// Get SOA current, including setting actuator
        ///// </summary>
        ///// <returns>SOA current status</returns>
        //private DSDBRSectionCurrent getSoaCurrent()
        //{
        //    string soaModuleName = sectionCloseGridName(DSDBRSection.SOA);
        //    DSDBRSectionCurrent sectionCurrent;
        //    short shortVal;
        //    closeGrid.getOutputConnection(soaModuleName, out shortVal);
        //    closeGrid.getCurrent(soaModuleName, out sectionCurrent.Current_mA);
        //    sectionCurrent.Enabled = (shortVal != 0);
            
        //    // if the section current is enabled, then we have to check which 
        //    // way around the relays are
        //    if (sectionCurrent.Enabled)
        //    {
        //         // check if we're in a positive or negative position
        //        int switchState = soaRelays.SwitchState;
        //        if (switchState == -1)
        //        {
        //            throw new ArgumentException("Invalid relay position");
        //        }

        //        // if we're in negative configuration, invert the current reading
        //        if (switchState == swPosnSoaNegative)
        //        {
        //            sectionCurrent.Current_mA = -sectionCurrent.Current_mA;
        //        }
        //    }
        //    return sectionCurrent;
        //}

        /// <summary>
        /// Set SOA relays to positive or negative position
        /// </summary>
        /// <param name="newCurrentDir">New SOA current direction as integer: 0, or +1 positive, -1 negative. These
        /// values were chosen as they can directly be pumped from the Math.Sign() method.</param>
        private void setSoaRelays(int newCurrentDir)
        {
            // do nothing in simulation mode
            if (closeGridHardwareSimulation) return;

            // which relay positions to use - positive or negative
            if ((newCurrentDir == 0) || (newCurrentDir == 1))
            {
                soaRelays.SwitchState = swPosnSoaPositive;
            }
            else if (newCurrentDir == -1)
            {
                soaRelays.SwitchState = swPosnSoaNegative;
            }
            else throw new ArgumentException("Invalid current direction: " + newCurrentDir);            
        }

        /// <summary>
        /// Collate the DFS (overall front section maps)
        /// </summary>
        /// <param name="testInfo">Info describing what to load</param>
        /// <param name="rampDirStr">Ramp direction</param>
        /// <returns>Filename of collated map</returns>
        private string collateDFSMaps(CloseGridTestInfo testInfo, string rampDirStr)
        {
            string collatedDFSFile = string.Format("{0}\\MATRIX_PowerRatio_{1}_DFS_{2}.csv",
                            testInfo.ResultsDir, testInfo.GetFileNameInfoStr(),
                            rampDirStr);

            using (StreamWriter writer = new StreamWriter(collatedDFSFile))
            {
                for (int frontSectionPair = 1; frontSectionPair <= 7; frontSectionPair++)
                {
                    string mapFileName = string.Format("{0}\\MATRIX_PowerRatio_{1}_DFS{2}_{3}.csv",
                        testInfo.ResultsDir, testInfo.GetFileNameInfoStr(),
                        frontSectionPair, rampDirStr);
                    using (StreamReader reader = new StreamReader(mapFileName))
                    {
                        string line;
                        while ((line = reader.ReadLine()) != null)
                        {
                            writer.WriteLine(line);
                        }
                    }
                }
            }
            return collatedDFSFile;
        }

        #endregion

        #region Private data
        private const int overallSmStatusFirstLine = 20;
        private const int overallSmStatusLastLine = 35;
        private const int mildMultiModeStartPhIndex = 10;// in teststand sequence, its value is 9. but it will calculate the 10th column, as the array index is from 0.
        // But in our program,the function GetColumn can get the exact column indicated by mildMultiModeStartPhIndex
        private const int mildMultiModeSampleSpacing = 20;
        private const int rearStepsSize = 101;

        #region New MMM screen consts
        private const double rearCutDiffThreshold = -0.003;
        private const int allowedFails = 0;
        private const int mmmThresholdPoint = 3;
        #endregion

        #endregion

    }
}

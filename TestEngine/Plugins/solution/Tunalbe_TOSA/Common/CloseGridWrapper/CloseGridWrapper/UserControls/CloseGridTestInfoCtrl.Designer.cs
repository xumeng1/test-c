namespace Bookham.Toolkit.CloseGrid
{
    partial class CloseGridTestInfoCtrl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxResDir = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSelectResDir = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxLaserType = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxLaserId = new System.Windows.Forms.TextBox();
            this.textBoxDateTimeStamp = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBoxResDir
            // 
            this.textBoxResDir.Location = new System.Drawing.Point(3, 66);
            this.textBoxResDir.Name = "textBoxResDir";
            this.textBoxResDir.Size = new System.Drawing.Size(277, 20);
            this.textBoxResDir.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Results Directory";
            // 
            // btnSelectResDir
            // 
            this.btnSelectResDir.Location = new System.Drawing.Point(286, 65);
            this.btnSelectResDir.Name = "btnSelectResDir";
            this.btnSelectResDir.Size = new System.Drawing.Size(24, 20);
            this.btnSelectResDir.TabIndex = 2;
            this.btnSelectResDir.Text = "...";
            this.btnSelectResDir.UseVisualStyleBackColor = true;
            this.btnSelectResDir.Click += new System.EventHandler(this.btnSelectResDir_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Laser Type";
            // 
            // textBoxLaserType
            // 
            this.textBoxLaserType.Location = new System.Drawing.Point(3, 23);
            this.textBoxLaserType.Name = "textBoxLaserType";
            this.textBoxLaserType.Size = new System.Drawing.Size(88, 20);
            this.textBoxLaserType.TabIndex = 3;
            this.textBoxLaserType.Text = "DSDBR01";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(98, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Laser ID";
            // 
            // textBoxLaserId
            // 
            this.textBoxLaserId.Location = new System.Drawing.Point(97, 23);
            this.textBoxLaserId.Name = "textBoxLaserId";
            this.textBoxLaserId.Size = new System.Drawing.Size(88, 20);
            this.textBoxLaserId.TabIndex = 5;
            this.textBoxLaserId.Text = "TRY";
            // 
            // textBoxDateTimeStamp
            // 
            this.textBoxDateTimeStamp.Location = new System.Drawing.Point(191, 23);
            this.textBoxDateTimeStamp.Name = "textBoxDateTimeStamp";
            this.textBoxDateTimeStamp.Size = new System.Drawing.Size(119, 20);
            this.textBoxDateTimeStamp.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(192, 7);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "DateTimeStamp";
            // 
            // CloseGridTestInfoCtrl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxLaserId);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxDateTimeStamp);
            this.Controls.Add(this.textBoxLaserType);
            this.Controls.Add(this.btnSelectResDir);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxResDir);
            this.Name = "CloseGridTestInfoCtrl";
            this.Size = new System.Drawing.Size(317, 100);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxResDir;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSelectResDir;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxLaserType;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxLaserId;
        private System.Windows.Forms.TextBox textBoxDateTimeStamp;
        private System.Windows.Forms.Label label5;
    }
}

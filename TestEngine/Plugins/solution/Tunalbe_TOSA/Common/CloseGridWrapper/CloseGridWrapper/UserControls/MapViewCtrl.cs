using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using NPlot;

namespace Bookham.Toolkit.CloseGrid
{
    /// <summary>
    /// User control to view a DSDBR map
    /// </summary>
    /// <remarks>Uses NPlot internally to do the drawing</remarks>
    public partial class MapViewCtrl : UserControl
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public MapViewCtrl()
        {
            InitializeComponent();
        }
        
        /// <summary>
        /// Set the map data
        /// </summary>
        /// <param name="mapData">2D array of double containing map data</param>
        /// <param name="title">Title of the Map</param>
        /// <param name="xAxis">X-axis label</param>
        /// <param name="yAxis">Y-axis label</param>
        public void SetMap(double[,] mapData, string title, string xAxis, string yAxis)
        {
            plotSurface2D1.Clear();
            plotSurface2D1.Title = title;
            ImagePlot imagePlot = new NPlot.ImagePlot(mapData);
            imagePlot.Gradient = new LinearGradient(Color.Red, Color.Blue);
            plotSurface2D1.Add(imagePlot);
            plotSurface2D1.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            plotSurface2D1.XAxis1.Label = xAxis;
            plotSurface2D1.YAxis1.Label = yAxis;
            plotSurface2D1.Refresh();
        }
    }
}

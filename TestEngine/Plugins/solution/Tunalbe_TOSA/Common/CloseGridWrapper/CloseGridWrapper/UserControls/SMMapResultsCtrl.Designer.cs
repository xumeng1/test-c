namespace Bookham.Toolkit.CloseGrid.UserControls
{
    partial class SMMapResultsCtrl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.labelNbrLongModes = new System.Windows.Forms.Label();
            this.checkBoxMapOK = new System.Windows.Forms.CheckBox();
            this.listBoxRampDir = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(0, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 39);
            this.label2.TabIndex = 6;
            this.label2.Text = "Nbr \r\nLongitudinal\r\nModes";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelNbrLongModes
            // 
            this.labelNbrLongModes.BackColor = System.Drawing.SystemColors.Window;
            this.labelNbrLongModes.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelNbrLongModes.Location = new System.Drawing.Point(16, 81);
            this.labelNbrLongModes.Name = "labelNbrLongModes";
            this.labelNbrLongModes.Size = new System.Drawing.Size(23, 22);
            this.labelNbrLongModes.TabIndex = 5;
            // 
            // checkBoxMapOK
            // 
            this.checkBoxMapOK.AutoSize = true;
            this.checkBoxMapOK.CheckAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.checkBoxMapOK.Enabled = false;
            this.checkBoxMapOK.Location = new System.Drawing.Point(3, 8);
            this.checkBoxMapOK.Name = "checkBoxMapOK";
            this.checkBoxMapOK.Size = new System.Drawing.Size(50, 31);
            this.checkBoxMapOK.TabIndex = 4;
            this.checkBoxMapOK.Text = "Map OK";
            this.checkBoxMapOK.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBoxMapOK.UseVisualStyleBackColor = true;
            // 
            // listBoxRampDir
            // 
            this.listBoxRampDir.FormattingEnabled = true;
            this.listBoxRampDir.Items.AddRange(new object[] {
            "Phase",
            "Middle"});
            this.listBoxRampDir.Location = new System.Drawing.Point(70, 25);
            this.listBoxRampDir.Name = "listBoxRampDir";
            this.listBoxRampDir.Size = new System.Drawing.Size(38, 30);
            this.listBoxRampDir.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(59, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Ramp Dir";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // SMMapResultsCtrl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.listBoxRampDir);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.labelNbrLongModes);
            this.Controls.Add(this.checkBoxMapOK);
            this.Name = "SMMapResultsCtrl";
            this.Size = new System.Drawing.Size(112, 110);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelNbrLongModes;
        private System.Windows.Forms.CheckBox checkBoxMapOK;
        private System.Windows.Forms.ListBox listBoxRampDir;
        private System.Windows.Forms.Label label1;
    }
}

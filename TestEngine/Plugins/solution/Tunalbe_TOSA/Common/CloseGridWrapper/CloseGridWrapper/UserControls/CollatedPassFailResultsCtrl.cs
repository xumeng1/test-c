using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Bookham.Toolkit.CloseGrid.UserControls
{
    /// <summary>
    /// User Control to show CollatedPassFail results
    /// </summary>
    public partial class CollatedPassFailResultsCtrl : UserControl
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public CollatedPassFailResultsCtrl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Set from CollatedPassFailResults
        /// </summary>
        public CollatedPassFailResults Value
        {
            set
            {
                checkBoxOverallMapPassed.Checked = value.OverallMapPassed;
                checkBoxOverallPass.Checked = value.OverallPass;
                listViewSupermodes.Items.Clear();
                for (int ii = 0; ii < value.MapsPassedArray.Length; ii++)
                {
                    bool pf = value.MapsPassedArray[ii];
                    ListViewItem lvi = listViewSupermodes.Items.Add(ii.ToString());
                    lvi.SubItems.Add(pf.ToString());                    
                }
            }
        }
        }
}

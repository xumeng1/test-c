namespace Bookham.Toolkit.CloseGrid.UserControls
{
    partial class CollatedPassFailResultsCtrl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.checkBoxOverallMapPassed = new System.Windows.Forms.CheckBox();
            this.checkBoxOverallPass = new System.Windows.Forms.CheckBox();
            this.listViewSupermodes = new System.Windows.Forms.ListView();
            this.colHeaderSupermode = new System.Windows.Forms.ColumnHeader();
            this.colHeaderPass = new System.Windows.Forms.ColumnHeader();
            this.SuspendLayout();
            // 
            // checkBoxOverallMapPassed
            // 
            this.checkBoxOverallMapPassed.AutoSize = true;
            this.checkBoxOverallMapPassed.Enabled = false;
            this.checkBoxOverallMapPassed.Location = new System.Drawing.Point(4, 4);
            this.checkBoxOverallMapPassed.Name = "checkBoxOverallMapPassed";
            this.checkBoxOverallMapPassed.Size = new System.Drawing.Size(121, 17);
            this.checkBoxOverallMapPassed.TabIndex = 0;
            this.checkBoxOverallMapPassed.Text = "Overall Map Passed";
            this.checkBoxOverallMapPassed.UseVisualStyleBackColor = true;
            // 
            // checkBoxOverallPass
            // 
            this.checkBoxOverallPass.AutoSize = true;
            this.checkBoxOverallPass.Enabled = false;
            this.checkBoxOverallPass.Location = new System.Drawing.Point(4, 28);
            this.checkBoxOverallPass.Name = "checkBoxOverallPass";
            this.checkBoxOverallPass.Size = new System.Drawing.Size(85, 17);
            this.checkBoxOverallPass.TabIndex = 1;
            this.checkBoxOverallPass.Text = "Overall Pass";
            this.checkBoxOverallPass.UseVisualStyleBackColor = true;
            // 
            // listViewSupermodes
            // 
            this.listViewSupermodes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.listViewSupermodes.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colHeaderSupermode,
            this.colHeaderPass});
            this.listViewSupermodes.Location = new System.Drawing.Point(4, 53);
            this.listViewSupermodes.Name = "listViewSupermodes";
            this.listViewSupermodes.Size = new System.Drawing.Size(143, 122);
            this.listViewSupermodes.TabIndex = 2;
            this.listViewSupermodes.UseCompatibleStateImageBehavior = false;
            this.listViewSupermodes.View = System.Windows.Forms.View.Details;
            // 
            // colHeaderSupermode
            // 
            this.colHeaderSupermode.Text = "Supermode";
            this.colHeaderSupermode.Width = 72;
            // 
            // colHeaderPass
            // 
            this.colHeaderPass.Text = "Pass";
            // 
            // CollatedPassFailResultsCtrl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.listViewSupermodes);
            this.Controls.Add(this.checkBoxOverallPass);
            this.Controls.Add(this.checkBoxOverallMapPassed);
            this.Name = "CollatedPassFailResultsCtrl";
            this.Size = new System.Drawing.Size(152, 175);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox checkBoxOverallMapPassed;
        private System.Windows.Forms.CheckBox checkBoxOverallPass;
        private System.Windows.Forms.ListView listViewSupermodes;
        private System.Windows.Forms.ColumnHeader colHeaderSupermode;
        private System.Windows.Forms.ColumnHeader colHeaderPass;
    }
}

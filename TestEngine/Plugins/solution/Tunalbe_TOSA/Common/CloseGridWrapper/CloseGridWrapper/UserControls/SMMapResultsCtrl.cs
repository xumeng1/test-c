using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Bookham.Toolkit.CloseGrid.UserControls
{
    /// <summary>
    /// User control to show results of SuperMode Map
    /// </summary>
    public partial class SMMapResultsCtrl : UserControl
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public SMMapResultsCtrl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialise control from SMMapResults class
        /// </summary>
        public SMMapResults Value
        {
            set
            {
                checkBoxMapOK.Checked = value.ScreeningPassed;
                labelNbrLongModes.Text = value.LongitudinalModeCount.ToString();
                this.RampDir = value.RampDir;
            }
        }

        /// <summary>
        /// Get/Set Ramp direction
        /// </summary>
        public SupermodeRampDir RampDir
        {
            get
            {
                if (listBoxRampDir.SelectedIndex == -1)
                {
                    listBoxRampDir.SelectedIndex = 0;
                }
                int selIndex = listBoxRampDir.SelectedIndex;
                if (selIndex == 0)
                { 
                    return SupermodeRampDir.Phase; 
                }
                else if (selIndex == 1)
                { 
                    return SupermodeRampDir.Middle; 
                }
                else
                { 
                    throw new ArgumentException("Bad Ramp Dir: " + selIndex); 
                }
            }
            set
            {
                switch (value)
                {
                    case SupermodeRampDir.Phase: 
                        listBoxRampDir.SelectedIndex = 0;
                        break;
                    case SupermodeRampDir.Middle:
                        listBoxRampDir.SelectedIndex = 1;
                        break;
                    default:
                        throw new ArgumentException("Bad Ramp Dir: " + value); 
                }
            }
        }
    }
}

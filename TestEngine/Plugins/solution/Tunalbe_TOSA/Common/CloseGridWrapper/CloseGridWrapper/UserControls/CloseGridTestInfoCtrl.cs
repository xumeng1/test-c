using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Bookham.Toolkit.CloseGrid
{
    /// <summary>
    /// User control to show Close Grid Test Information (Results directory, laser type, id etc...)
    /// </summary>    
    public partial class CloseGridTestInfoCtrl : UserControl
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public CloseGridTestInfoCtrl()
        {
            InitializeComponent();
        }

        private void btnSelectResDir_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.ShowNewFolderButton = false;
            DialogResult res = folderBrowserDialog1.ShowDialog();
            if (res == DialogResult.OK)
            {
                textBoxResDir.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        /// <summary>
        /// Laser Type
        /// </summary>
        public string LaserType
        {
            get
            {
                return textBoxLaserType.Text;
            }
            set
            {
                textBoxLaserType.Text = value;
            }
        }

        /// <summary>
        /// Laser ID
        /// </summary>
        public string LaserId
        {
            get
            {
                return textBoxLaserId.Text;
            }
            set
            {
                textBoxLaserId.Text = value;
            }
        }

        /// <summary>
        /// Date and Time Stamp
        /// </summary>        
        public string DateTimeStamp
        {
            get
            {
                return textBoxDateTimeStamp.Text;
            }
            set
            {
                textBoxDateTimeStamp.Text = value;
            }
        }

        /// <summary>
        /// All the information in this control rolled up in a CloseGridTestInfo object
        /// </summary>
        public CloseGridTestInfo Value
        {
            get
            {                
                string resultsDir = textBoxResDir.Text;
                string laserType = textBoxLaserType.Text;
                string laserId = textBoxLaserId.Text;
                string dateTimeStamp = textBoxDateTimeStamp.Text;
                // check that all are specified!
                if ((resultsDir == "") || (laserType == "") || (laserId == "")
                     || (dateTimeStamp == ""))
                {
                    throw new ArgumentException("All of ResultsDir, LaserType, LaserId and DateTimeStamp must be specified!");
                }

                CloseGridTestInfo info = new CloseGridTestInfo(resultsDir,
                    laserType, laserId, dateTimeStamp);
                return info;
            }
            set
            {
                textBoxResDir.Text = value.ResultsDir;
                textBoxLaserType.Text = value.LaserType;
                textBoxLaserId.Text = value.LaserId;
                textBoxDateTimeStamp.Text = value.DateTimeStamp;                
            }
        }
    }
}

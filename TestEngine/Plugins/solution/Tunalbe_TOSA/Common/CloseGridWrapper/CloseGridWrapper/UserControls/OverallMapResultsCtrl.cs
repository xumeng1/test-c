using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Bookham.Toolkit.CloseGrid
{
    /// <summary>
    /// User control to display overall map results
    /// </summary>
    public partial class OverallMapResultsCtrl : UserControl
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public OverallMapResultsCtrl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Set value
        /// </summary>
        public OverallMapResults Value
        {
            set
            {
                checkBoxMapOK.Checked = value.ScreeningPassed;
                labelNbrSupermodes.Text = value.SupermodeCount.ToString();
                listBoxAvgPowers.Items.Clear();
                foreach (double val in value.AvgPowers_mW)
                {
                    listBoxAvgPowers.Items.Add(val);
                }
                RampDir = value.RampDir;
            }
        }

        /// <summary>
        /// Get/Set Ramp direction
        /// </summary>
        public OverallRampDir RampDir
        {
            get
            {
                if (listBoxRampDir.SelectedIndex == -1)
                {
                    listBoxRampDir.SelectedIndex = 0;
                }
                int selIndex = listBoxRampDir.SelectedIndex;
                if (selIndex == 0)
                {
                    return OverallRampDir.Front;
                }
                else if (selIndex == 1)
                {
                    return OverallRampDir.Rear;
                }
                else
                {
                    throw new ArgumentException("Bad Ramp Dir: " + selIndex);
                }
            }
            set
            {
                switch (value)
                {
                    case OverallRampDir.Front:
                        listBoxRampDir.SelectedIndex = 0;
                        break;
                    case OverallRampDir.Rear:
                        listBoxRampDir.SelectedIndex = 1;
                        break;
                    default:
                        throw new ArgumentException("Bad Ramp Dir: " + value);
                }
            }
        }
    }
}

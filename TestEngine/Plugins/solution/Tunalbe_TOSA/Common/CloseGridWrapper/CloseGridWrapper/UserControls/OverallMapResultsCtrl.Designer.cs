namespace Bookham.Toolkit.CloseGrid
{
    partial class OverallMapResultsCtrl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.checkBoxMapOK = new System.Windows.Forms.CheckBox();
            this.labelNbrSupermodes = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.listBoxAvgPowers = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.listBoxRampDir = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // checkBoxMapOK
            // 
            this.checkBoxMapOK.AutoSize = true;
            this.checkBoxMapOK.CheckAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.checkBoxMapOK.Enabled = false;
            this.checkBoxMapOK.Location = new System.Drawing.Point(4, 3);
            this.checkBoxMapOK.Name = "checkBoxMapOK";
            this.checkBoxMapOK.Size = new System.Drawing.Size(50, 31);
            this.checkBoxMapOK.TabIndex = 1;
            this.checkBoxMapOK.Text = "Map OK";
            this.checkBoxMapOK.UseVisualStyleBackColor = true;
            // 
            // labelNbrSupermodes
            // 
            this.labelNbrSupermodes.BackColor = System.Drawing.SystemColors.Window;
            this.labelNbrSupermodes.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelNbrSupermodes.Location = new System.Drawing.Point(17, 67);
            this.labelNbrSupermodes.Name = "labelNbrSupermodes";
            this.labelNbrSupermodes.Size = new System.Drawing.Size(23, 22);
            this.labelNbrSupermodes.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(1, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 26);
            this.label2.TabIndex = 3;
            this.label2.Text = "Nbr \r\nSupermodes";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // listBoxAvgPowers
            // 
            this.listBoxAvgPowers.Enabled = false;
            this.listBoxAvgPowers.FormattingEnabled = true;
            this.listBoxAvgPowers.Location = new System.Drawing.Point(66, 19);
            this.listBoxAvgPowers.Name = "listBoxAvgPowers";
            this.listBoxAvgPowers.Size = new System.Drawing.Size(61, 121);
            this.listBoxAvgPowers.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(65, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Avg Powers";
            // 
            // listBoxRampDir
            // 
            this.listBoxRampDir.FormattingEnabled = true;
            this.listBoxRampDir.Items.AddRange(new object[] {
            "Front",
            "Rear"});
            this.listBoxRampDir.Location = new System.Drawing.Point(7, 112);
            this.listBoxRampDir.Name = "listBoxRampDir";
            this.listBoxRampDir.Size = new System.Drawing.Size(47, 30);
            this.listBoxRampDir.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 96);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Ramp Dir";
            // 
            // OverallMapResultsCtrl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.listBoxRampDir);
            this.Controls.Add(this.listBoxAvgPowers);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.labelNbrSupermodes);
            this.Controls.Add(this.checkBoxMapOK);
            this.Name = "OverallMapResultsCtrl";
            this.Size = new System.Drawing.Size(137, 150);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox checkBoxMapOK;
        private System.Windows.Forms.Label labelNbrSupermodes;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox listBoxAvgPowers;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox listBoxRampDir;
        private System.Windows.Forms.Label label1;

    }
}

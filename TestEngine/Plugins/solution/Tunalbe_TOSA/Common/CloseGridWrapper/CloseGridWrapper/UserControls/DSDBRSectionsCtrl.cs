using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Bookham.Toolkit.CloseGrid
{
    /// <summary>
    /// DSDBR Section custom control - shows all the laser section current values!
    /// </summary>
    public partial class DSDBRSectionsCtrl : UserControl
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public DSDBRSectionsCtrl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Readback / Apply the DSDBR currents
        /// </summary>
        public DSDBRSectionCurrents Value
        {
            get
            {
                DSDBRSectionCurrents d;
                d.Gain.Enabled = GainEnabled.Checked;
                d.Gain.Current_mA = (double) GainCurrent.Value;
                d.SOA.Enabled = SoaEnabled.Checked;
                d.SOA.Current_mA = (double) SoaCurrent.Value;
                d.Rear.Enabled = RearEnabled.Checked;
                d.Rear.Current_mA = (double)RearCurrent.Value;
                d.Phase.Enabled = PhaseEnabled.Checked;
                d.Phase.Current_mA = (double)PhaseCurrent.Value;

                d.Front1.Enabled = Front1Enabled.Checked;
                d.Front1.Current_mA = (double)Front1Current.Value;
                d.Front2.Enabled = Front2Enabled.Checked;
                d.Front2.Current_mA = (double)Front2Current.Value;
                d.Front3.Enabled = Front3Enabled.Checked;
                d.Front3.Current_mA = (double)Front3Current.Value;
                d.Front4.Enabled = Front4Enabled.Checked;
                d.Front4.Current_mA = (double)Front4Current.Value;
                d.Front5.Enabled = Front5Enabled.Checked;
                d.Front5.Current_mA = (double)Front5Current.Value;
                d.Front6.Enabled = Front6Enabled.Checked;
                d.Front6.Current_mA = (double)Front6Current.Value;
                d.Front7.Enabled = Front7Enabled.Checked;
                d.Front7.Current_mA = (double)Front7Current.Value;
                d.Front8.Enabled = Front8Enabled.Checked;
                d.Front8.Current_mA = (double)Front8Current.Value;

                return d;
            }
            set
            {
                GainEnabled.Checked = value.Gain.Enabled;
                GainCurrent.Value = (decimal) value.Gain.Current_mA;
                SoaEnabled.Checked = value.SOA.Enabled;
                SoaCurrent.Value = (decimal)value.SOA.Current_mA;
                RearEnabled.Checked = value.Rear.Enabled;
                RearCurrent.Value = (decimal)value.Rear.Current_mA;
                PhaseEnabled.Checked = value.Phase.Enabled;
                PhaseCurrent.Value = (decimal)value.Phase.Current_mA;

                Front1Enabled.Checked = value.Front1.Enabled;
                Front1Current.Value = (decimal)value.Front1.Current_mA;
                Front2Enabled.Checked = value.Front2.Enabled;
                Front2Current.Value = (decimal)value.Front2.Current_mA;
                Front3Enabled.Checked = value.Front3.Enabled;
                Front3Current.Value = (decimal)value.Front3.Current_mA;
                Front4Enabled.Checked = value.Front4.Enabled;
                Front4Current.Value = (decimal)value.Front4.Current_mA;
                Front5Enabled.Checked = value.Front5.Enabled;
                Front5Current.Value = (decimal)value.Front5.Current_mA;
                Front6Enabled.Checked = value.Front6.Enabled;
                Front6Current.Value = (decimal)value.Front6.Current_mA;
                Front7Enabled.Checked = value.Front7.Enabled;
                Front7Current.Value = (decimal)value.Front7.Current_mA;
                Front8Enabled.Checked = value.Front8.Enabled;
                Front8Current.Value = (decimal)value.Front8.Current_mA;
            }
        }
        
    }
}

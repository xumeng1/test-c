using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Bookham.Toolkit.CloseGrid
{
    internal partial class InvisibleForm : Form
    {
        internal InvisibleForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Spooky - when this form is activated in the GUI - it will 
        /// instantly disappear!
        /// </summary>
        private void InvisibleForm_Activated(object sender, EventArgs e)
        {
            this.Visible = false;
        }           
    }
}
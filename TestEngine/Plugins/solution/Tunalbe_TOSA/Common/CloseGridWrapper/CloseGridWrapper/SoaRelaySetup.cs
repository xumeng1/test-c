using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.InstrTypes;

namespace Bookham.Toolkit.CloseGrid
{
    /// <summary>
    /// SOA Relay setup to allow to switch from positive to negative current
    /// </summary>
    public class SoaRelaySetup
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="soaRelays">Which relays</param>
        /// <param name="soaPosRelayPosns">Relay posns for positive current</param>
        /// <param name="soaNegRelayPosns">Relay posns for negative current</param>
        public SoaRelaySetup(InstType_DigitalIO[] soaRelays, bool[] soaPosRelayPosns,
            bool[] soaNegRelayPosns)
        {
            this.soaRelays = soaRelays;
            this.soaPosRelayPosns = soaPosRelayPosns;
            this.soaNegRelayPosns = soaNegRelayPosns;
        }

        /// <summary>
        /// Which relays
        /// </summary>
        public InstType_DigitalIO[] SoaRelays
        {
            get { return soaRelays; }
        }
        
        /// <summary>
        /// Relay posns for positive current
        /// </summary>
        public bool[] SoaPosRelayPosns
        {
            get { return soaPosRelayPosns; }
        }

        /// <summary>
        /// Relay posns for negative current
        /// </summary>
        public bool[] SoaNegRelayPosns
        {
            get { return soaNegRelayPosns; }
        }
        

        private InstType_DigitalIO[] soaRelays;
        private bool[] soaPosRelayPosns;
        private bool[] soaNegRelayPosns;
    }
}

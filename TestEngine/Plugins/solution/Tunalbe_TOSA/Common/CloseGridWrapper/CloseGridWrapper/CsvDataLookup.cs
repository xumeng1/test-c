using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.Utilities;
using System.IO;
using Bookham.TestSolution.IlmzCommonData;

namespace Bookham.Toolkit.CloseGrid
{
    /// <summary>
    ///  Helper class for extracting data from a CloseGrid CSV file 
    /// </summary>
    public sealed class CsvDataLookup
    {
        private static int _ituColumnLength;
        /// <summary>
        /// Private constructor
        /// </summary>
        private CsvDataLookup() { }

        /// <summary>
        /// Get the value of a single element in a CSV file
        /// </summary>
        /// <param name="filename">File to open</param>
        /// <param name="line">Line number (start from 1)</param>
        /// <param name="column">Column number (start from 1)</param>
        /// <returns>Element string - throws exception on bad line/column number or missing file</returns>
        internal static string GetElement(string filename, int line, int column)
        {
            string filenameNoPath = Path.GetFileName(filename);
            string elementStr;
            // using statement will auto-shutdown the file even in presence of exceptions
            using (CsvReader csvReader = new CsvReader())
            {
                // open the file
                csvReader.OpenFile(filename);
                string[] lineElems = null;
                int lineCounter = 1;
                // find the right line
                while ((lineElems = csvReader.GetLine()) != null)
                {
                    if (lineCounter == line) break;
                    lineCounter++;
                }
                // if got to the end of the file, we'll have a null line reference
                if (lineElems == null)
                {
                    string errStr = string.Format("Couldn't find line {0} in CloseGrid CSV file {1}",
                        line, filenameNoPath);
                    throw new ArgumentException(errStr);
                }
                // OK - we've got our line
                // sanity check the column value
                if ((column < 0) || (column > lineElems.Length - 1))
                {
                    string errStr = string.Format("Couldn't find column {0}, line {1} in CloseGrid CSV file {2}",
                        column, line, filenameNoPath);
                    throw new ArgumentException(errStr);
                }
                // get the element
                elementStr = lineElems[column - 1];
            }
            // return the value
            return elementStr;
        }

        public static int GetItuColumnLength()
        {
            return _ituColumnLength;
        }

        /// <summary>
        /// Get column from a CSV file
        /// </summary>
        /// <param name="filename">filename</param>
        /// <param name="column">Column number (start at 1)</param>
        /// <returns>List of strings</returns>
        internal static List<string> GetColumn(string filename, int column)
        {
            if (column < 1) throw new ArgumentException("Invalid column number (starts at 1): " + column);

            string filenameNoPath = Path.GetFileName(filename);
            List<string[]> lines;
            // using statement will auto-shutdown the file even in presence of exceptions
            using (CsvReader csvReader = new CsvReader())
            {
                lines = csvReader.ReadFile(filename);
            }

            int colIndexMinus1 = column - 1;
            List<string> columnStrs = new List<string>();
            foreach (string[] line in lines)
            {
                // if this line doesn't have a value at this column level - add a null string
                string elemStr = null;
                if (line.Length >= column) elemStr = line[colIndexMinus1];
                columnStrs.Add(elemStr);
            }
            return columnStrs;
        }

        /// <summary>
        /// Calculate Modal Distortion figure
        /// </summary>
        /// <param name="smQaAnalysisFile">Path to a supermode QA analysis</param>
        /// <param name="nbrLongitudinalModes">Number of longitudinal modes</param>
        /// <param name="ignoreFirstThreeLMs">True if ignore the first 3 LMs when calculate MD</param>
        /// <returns>Modal Distortion figure</returns>
        internal static double ModalDistortion(string smQaAnalysisFile, int nbrLongitudinalModes, bool ignoreFirstThreeLMs)
        {
            int startLM = 0;
            if (ignoreFirstThreeLMs)
            {
                // ignore the first 3 LMs so we start from LM3 ( As the first LM is LM0)
                startLM = 3;
            }

            double avg;
            if (ignoreFirstThreeLMs && nbrLongitudinalModes <= 3)
            {
                avg = 999;
            }
            else
            {
                // get 8th column of the file 
                List<string> columns = GetColumn(smQaAnalysisFile, 8);
                double total = 0.0;
                for (int ii = startLM; ii < nbrLongitudinalModes; ii++)
                {
                    // Get max and min values of "Width for Stable Area" for each longitudinal mode
                    string maxStr = columns[ii * 9 + 9];  //lines 10, 19, 28 etc
                    string minStr = columns[ii * 9 + 10]; //lines 11, 20, 28 etc

                    double max = Double.Parse(maxStr);
                    double min = Double.Parse(minStr);
                    total += max - min;
                }
                avg = total / (nbrLongitudinalModes - startLM);
            }

            return avg;
        }

        /// <summary>
        /// Loads the ITU operating points data structure from the operating points file
        /// </summary>
        /// <param name="ituOpPointsFile">ITU Operating points file</param>
        /// <returns>Array - one per channel</returns>
        public static DsdbrChannelData[] GetItuOperatingPoints(string ituOpPointsFile)
        {
            List<string[]> lines;
            // using statement will auto-shutdown the file even in presence of exceptions
            using (CsvReader csvReader = new CsvReader())
            {
                lines = csvReader.ReadFile(ituOpPointsFile);
            }
            _ituColumnLength = lines[0].Length;
            // create a new list with appropriate capacity
            List<DsdbrChannelData> ituOpList = new List<DsdbrChannelData>(lines.Count - 1);
            // go through the lines of the file - zero based, so therefore starting at the 2nd line:
            // skipping the header line
            for (int lineNbr = 1; lineNbr < lines.Count; lineNbr++)
            {
                string[] line = lines[lineNbr];
                // skip any empty lines - as detected by lack of commas
                if (line.Length <= 1) continue;

                // get one channel's info
                DsdbrChannelData chan= new DsdbrChannelData ();
                DsdbrChannelSetup chanSetup;
                //ITU Channel Number,
                chan.ItuChannelIndex = int.Parse(line[0]);
                //ITU Channel Frequency,
                chan.ItuFreq_GHz = double.Parse(line[1]);
                //Supermode Number,
                chan.Supermode = int.Parse(line[2]);
                //Longitudinal Mode Number,
                chan.LongitudinalMode = int.Parse(line[3]);
                //Index on Middle Line Of Frequency,
                chan.MiddleLineIndex = double.Parse(line[4]);
                //Phase [mA],
                chanSetup.IPhase_mA = double.Parse(line[5]);
                //Rear [mA],
                chanSetup.IRear_mA = double.Parse(line[6]);
                //Gain [mA],
                chanSetup.IGain_mA = double.Parse(line[7]);
                //SOA [mA],
                chanSetup.ISoa_mA = double.Parse(line[8]);
                //Front Pair Number,
                chanSetup.FrontPair = int.Parse(line[9]);
                //Constant Front [mA],
                chanSetup.IFsFirst_mA = double.Parse(line[10]);
                //Non-constant Front [mA],
                chanSetup.IFsSecond_mA = double.Parse(line[11]);
                               
                ////Frequency [GHz],
                chan.Freq_GHz = double.Parse(line[12]);
                ////Coarse Frequency [GHz],
                //chan.CoarseFreq_GHz = double.Parse(line[15]);
                ////Optical Power Ratio [0:1],
                chan.OptPowerRatio = double.Parse(line[15]);
                //Optical Power [mW],
                //chan.OptPower_mW = double.Parse(line[15]);
                //Echo new added below if clause
                //when itu point is find by simple wavemeter, then we will record 4 exra information (lockratio,powerratio)
                //so, we have 4 columnOffset when read itu point file .
                int columnOffset = 0;

                if (line.Length > 23)
                {
                    columnOffset = 4;
                }
                //X coordinate of boundary above,
                chan.BoundAboveX = double.Parse(line[13 + columnOffset]);
                //Y coordinate of boundary above,
                chan.BoundAboveY = double.Parse(line[14 + columnOffset]);
                //X coordinate of boundary below,
                chan.BoundBelowX = double.Parse(line[15 + columnOffset]);
                //Y coordinate of boundary below,
                chan.BoundBelowY = double.Parse(line[16 + columnOffset]);
                //X coordinate of boundary to left,
                chan.BoundLeftX = double.Parse(line[17 + columnOffset]);
                //Y coordinate of boundary to left,
                chan.BoundLeftY = double.Parse(line[18 + columnOffset]);
                //X coordinate of boundary to right,
                chan.BoundRightX = double.Parse(line[19 + columnOffset]);
                //Y coordinate of boundary to right,
                chan.BoundRightY = double.Parse(line[20 + columnOffset]);

                chanSetup.IRearSoa_mA = double.Parse(line[22 + columnOffset]);


                // add setup data to the channel object (NB: struct here, so copies!)
                chan.Setup = chanSetup;
                // add it to the list
                ituOpList.Add(chan);
            }

            // sort the list by ItuChannelIndex
            ituOpList.Sort();

            // return as a normal array
            return ituOpList.ToArray();
        }

        public static DsdbrChannelData[] GetChannelPassPoints(string ChassPassFile)
        {
            List<string[]> lines;
            // using statement will auto-shutdown the file even in presence of exceptions
            using (CsvReader csvReader = new CsvReader())
            {
                lines = csvReader.ReadFile(ChassPassFile);
            }
            _ituColumnLength = lines[0].Length;
            // create a new list with appropriate capacity
            List<DsdbrChannelData> ituOpList = new List<DsdbrChannelData>(lines.Count - 1);
            // go through the lines of the file - zero based, so therefore starting at the 2nd line:
            // skipping the header line
            for (int lineNbr = 3; lineNbr < lines.Count; lineNbr++)
            {
                string[] line = lines[lineNbr];
                // skip any empty lines - as detected by lack of commas
                if (line.Length <= 1) continue;

                // get one channel's info
                DsdbrChannelData chan = new DsdbrChannelData();
                DsdbrChannelSetup chanSetup;
                //ITU Channel Number,
                chan.ItuChannelIndex = int.Parse(line[0]);
                //ITU Channel Frequency,
                chan.ItuFreq_GHz = double.Parse(line[1]);
                //Supermode Number,
                chan.Supermode = int.Parse(line[2]);
                //Longitudinal Mode Number,
                chan.LongitudinalMode = int.Parse(line[3]);
                //Index on Middle Line Of Frequency,
                chan.MiddleLineIndex = double.Parse(line[4]);
                //Phase [mA],
                chanSetup.IPhase_mA = double.Parse(line[5]);
                //Rear [mA],
                chanSetup.IRear_mA = double.Parse(line[6]);
                //Gain [mA],
                chanSetup.IGain_mA = double.Parse(line[7]);
                //SOA [mA],
                chanSetup.ISoa_mA = double.Parse(line[8]);
                //Front Pair Number,
                chanSetup.FrontPair = int.Parse(line[9]);
                //Constant Front [mA],
                chanSetup.IFsFirst_mA = double.Parse(line[10]);
                //Non-constant Front [mA],
                chanSetup.IFsSecond_mA = double.Parse(line[11]);

                ////Frequency [GHz],
                chan.Freq_GHz = double.Parse(line[15]);
                ////Coarse Frequency [GHz],
                //chan.CoarseFreq_GHz = double.Parse(line[15]);
                ////Optical Power Ratio [0:1],
                chan.OptPowerRatio = double.Parse(line[17]);
                //Optical Power [mW],
                //chan.OptPower_mW = double.Parse(line[15]);
                //Echo new added below if clause
                //when itu point is find by simple wavemeter, then we will record 4 exra information (lockratio,powerratio)
                //so, we have 4 columnOffset when read itu point file .
                int columnOffset = 0;

                if (line.Length > 23)
                {
                    columnOffset = 4;
                }
                //X coordinate of boundary above,
                chan.BoundAboveX = double.Parse(line[18]);
                //Y coordinate of boundary above,
                chan.BoundAboveY = double.Parse(line[19]);
                //X coordinate of boundary below,
                chan.BoundBelowX = double.Parse(line[20]);
                //Y coordinate of boundary below,
                chan.BoundBelowY = double.Parse(line[21]);
                //X coordinate of boundary to left,
                chan.BoundLeftX = double.Parse(line[22]);
                //Y coordinate of boundary to left,
                chan.BoundLeftY = double.Parse(line[23]);
                //X coordinate of boundary to right,
                chan.BoundRightX = double.Parse(line[24]);
                //Y coordinate of boundary to right,
                chan.BoundRightY = double.Parse(line[25]);

                chanSetup.IRearSoa_mA = double.Parse(line[11]);


                // add setup data to the channel object (NB: struct here, so copies!)
                chan.Setup = chanSetup;
                // add it to the list
                ituOpList.Add(chan);
            }

            // sort the list by ItuChannelIndex
            ituOpList.Sort();

            // return as a normal array
            return ituOpList.ToArray();
        }


        public static DsdbrChannelData[] GetChannelPassPoints(string ChassPassFile, bool FinalPassFile)
        {
            List<string[]> lines;
            // using statement will auto-shutdown the file even in presence of exceptions
            using (CsvReader csvReader = new CsvReader())
            {
                lines = csvReader.ReadFile(ChassPassFile);
            }
            _ituColumnLength = lines[0].Length;
            // create a new list with appropriate capacity
            List<DsdbrChannelData> ituOpList = new List<DsdbrChannelData>(lines.Count - 1);
            // go through the lines of the file - zero based, so therefore starting at the 2nd line:
            // skipping the header line
            for (int lineNbr = 3; lineNbr < lines.Count; lineNbr++)
            {
                string[] line = lines[lineNbr];
                // skip any empty lines - as detected by lack of commas
                if (line.Length <= 1) continue;

                // get one channel's info
                DsdbrChannelData chan = new DsdbrChannelData();
                DsdbrChannelSetup chanSetup;
                //ITU Channel Number,
                chan.ItuChannelIndex = int.Parse(line[0]);
                //ITU Channel Frequency,
                chan.ItuFreq_GHz = double.Parse(line[1]);
                //Supermode Number,
                chan.Supermode = int.Parse(line[3]);
                //Longitudinal Mode Number,
                chan.LongitudinalMode = int.Parse(line[4]);
                //Index on Middle Line Of Frequency,
                chan.MiddleLineIndex = double.Parse(line[5]);
                //Phase [mA],
                chanSetup.IPhase_mA = double.Parse(line[7]);
                //Rear [mA],
                chanSetup.IRear_mA = double.Parse(line[9]);
                //Gain [mA],
                chanSetup.IGain_mA = double.Parse(line[10]);
                //SOA [mA],
                chanSetup.ISoa_mA = double.Parse(line[11]);
                //Front Pair Number,
                chanSetup.FrontPair = int.Parse(line[13]);
                //Constant Front [mA],
                chanSetup.IFsFirst_mA = double.Parse(line[14]);
                //Non-constant Front [mA],
                chanSetup.IFsSecond_mA = double.Parse(line[15]);

                ////Frequency [GHz],
                chan.Freq_GHz = double.Parse(line[16]);
                ////Coarse Frequency [GHz],
                //chan.CoarseFreq_GHz = double.Parse(line[15]);
                ////Optical Power Ratio [0:1],
                chan.OptPowerRatio = double.Parse(line[18]);
                //Optical Power [mW],
                //chan.OptPower_mW = double.Parse(line[15]);
                //Echo new added below if clause
                //when itu point is find by simple wavemeter, then we will record 4 exra information (lockratio,powerratio)
                //so, we have 4 columnOffset when read itu point file .
                int columnOffset = 0;

                if (line.Length > 23)
                {
                    columnOffset = 4;
                }
                //X coordinate of boundary above,
                chan.BoundAboveX = double.Parse(line[19]);
                //Y coordinate of boundary above,
                chan.BoundAboveY = double.Parse(line[20]);
                //X coordinate of boundary below,
                chan.BoundBelowX = double.Parse(line[21]);
                //Y coordinate of boundary below,
                chan.BoundBelowY = double.Parse(line[22]);
                //X coordinate of boundary to left,
                chan.BoundLeftX = double.Parse(line[23]);
                //Y coordinate of boundary to left,
                chan.BoundLeftY = double.Parse(line[24]);
                //X coordinate of boundary to right,
                chan.BoundRightX = double.Parse(line[25]);
                //Y coordinate of boundary to right,
                chan.BoundRightY = double.Parse(line[26]);

                chanSetup.IRearSoa_mA = double.Parse(line[12]);


                // add setup data to the channel object (NB: struct here, so copies!)
                chan.Setup = chanSetup;
                // add it to the list
                ituOpList.Add(chan);
            }

            // sort the list by ItuChannelIndex
            ituOpList.Sort();

            // return as a normal array
            return ituOpList.ToArray();
        }


        /// <summary>
        /// Loads the ITU operating points data structure from the operating points file which in frequency range
        /// </summary>
        /// <param name="ituOpPointsFile">ITU Operating points file</param>
        /// /// <param name="lowFreq_Ghz">low frequency</param>
        /// /// <param name="highFreq_Ghz">high frequency</param>
        /// <returns>Array - one per channel</returns>
        public static DsdbrChannelData[] GetItuOperatingPointsInFreqRange(string ituOpPointsFile, double lowFreq_Ghz, double highFreq_Ghz)
        {
            List<string[]> lines;
            // using statement will auto-shutdown the file even in presence of exceptions
            using (CsvReader csvReader = new CsvReader())
            {
                lines = csvReader.ReadFile(ituOpPointsFile);
            }

            // create a new list with appropriate capacity
            List<DsdbrChannelData> ituOpList = new List<DsdbrChannelData>(lines.Count - 1);
            // go through the lines of the file - zero based, so therefore starting at the 2nd line:
            // skipping the header line
            for (int lineNbr = 1; lineNbr < lines.Count; lineNbr++)
            {
                string[] line = lines[lineNbr];
                // skip any empty lines - as detected by lack of commas
                if (line.Length <= 1) continue;



                // get one channel's info
                DsdbrChannelData chan = new DsdbrChannelData();
                DsdbrChannelSetup chanSetup;
                //ITU Channel Number,
                chan.ItuChannelIndex = int.Parse(line[0]);
                //ITU Channel Frequency,
                chan.ItuFreq_GHz = double.Parse(line[1]);

                if (chan.ItuFreq_GHz < lowFreq_Ghz || chan.ItuFreq_GHz > highFreq_Ghz) continue; //Echo new added for PA009181 81channels ZC device 
                //Supermode Number,
                chan.Supermode = int.Parse(line[2]);
                //Longitudinal Mode Number,
                chan.LongitudinalMode = int.Parse(line[3]);
                //Index on Middle Line Of Frequency,
                chan.MiddleLineIndex = double.Parse(line[4]);
                //Phase [mA],
                chanSetup.IPhase_mA = double.Parse(line[5]);
                //Rear [mA],
                chanSetup.IRear_mA = double.Parse(line[6]);
                //Gain [mA],
                chanSetup.IGain_mA = double.Parse(line[7]);
                //SOA [mA],
                chanSetup.ISoa_mA = double.Parse(line[8]);
                //Front Pair Number,
                chanSetup.FrontPair = int.Parse(line[9]);
                //Constant Front [mA],
                chanSetup.IFsFirst_mA = double.Parse(line[10]);
                //Non-constant Front [mA],
                chanSetup.IFsSecond_mA = double.Parse(line[11]);


                ////Frequency [GHz],
                chan.Freq_GHz = double.Parse(line[12]);
                ////Coarse Frequency [GHz],
                //chan.CoarseFreq_GHz = double.Parse(line[13]);
                ////Optical Power Ratio [0:1],
                //chan.OptPowerRatio = double.Parse(line[14]);
                ////Optical Power [mW],
                chan.OptPower_mW = double.Parse(line[15]);

                int columnOffset = 0;

                if (line.Length > 23)
                {
                    columnOffset = 4;
                }
                //X coordinate of boundary above,
                chan.BoundAboveX = double.Parse(line[13 + columnOffset]);
                //Y coordinate of boundary above,
                chan.BoundAboveY = double.Parse(line[14 + columnOffset]);
                //X coordinate of boundary below,
                chan.BoundBelowX = double.Parse(line[15 + columnOffset]);
                //Y coordinate of boundary below,
                chan.BoundBelowY = double.Parse(line[16 + columnOffset]);
                //X coordinate of boundary to left,
                chan.BoundLeftX = double.Parse(line[17 + columnOffset]);
                //Y coordinate of boundary to left,
                chan.BoundLeftY = double.Parse(line[18 + columnOffset]);
                //X coordinate of boundary to right,
                chan.BoundRightX = double.Parse(line[19 + columnOffset]);
                //Y coordinate of boundary to right,
                chan.BoundRightY = double.Parse(line[20 + columnOffset]);

                chanSetup.IRearSoa_mA = double.Parse(line[22 + columnOffset]);


                // add setup data to the channel object (NB: struct here, so copies!)
                chan.Setup = chanSetup;
                // add it to the list
                ituOpList.Add(chan);
            }

            // sort the list by ItuChannelIndex
            ituOpList.Sort();

            // return as a normal array
            return ituOpList.ToArray();
        }

        /// <summary>
        /// Calculate mode aquisition phase current
        /// </summary>
        /// <param name="rearCurrent">Rear current</param>
        /// <param name="ImiddleLowerLMFile">Path to ImiddleLower_LM file</param>
        /// <returns>Mode acquisition phase current</returns>
        public static double IPhaseAcquisition(double rearCurrent, string ImiddleLowerLMFile)
        {
            // Phase Acquisition Current consts
            const int iRearCol = 0;
            const int iPhaseCol = 4;
            const double iRearGap = 0.0001;

            double IPhaseAcq = 0;
            int IPhaseCount = 0;
            string[] previousLine;
            string[] currentLine;

            using (CsvReader csvReader = new CsvReader())
            {
                // Generate filename
                string filename = ImiddleLowerLMFile;

                // Return -999 if the file is not available
                List<string[]> lines;
                try
                {
                    lines = csvReader.ReadFile(filename);
                }
                catch
                {
                    return 999;
                }

                // Look up rear current in file
                int nbrRows = lines.Count;
                int nbrCols = lines[0].Length;
                previousLine = new string[nbrCols];
                currentLine = new string[nbrCols];

                if (nbrRows <= 1)
                {
                    IPhaseAcq = -999;
                    return IPhaseAcq;
                }
                else if (double.Parse(lines[1][iRearCol]) >= rearCurrent + iRearGap)
                {
                    IPhaseAcq = -999;
                    return IPhaseAcq;
                }//This should not occur... 

                for (int ii = 1; ii < nbrRows; ii++)
                {
                    currentLine = lines[ii];
                    if (Math.Abs(rearCurrent - double.Parse(currentLine[iRearCol])) < iRearGap)
                    {
                        IPhaseAcq += double.Parse(currentLine[iPhaseCol]);
                        IPhaseCount++;
                    }
                    if (double.Parse(currentLine[iRearCol]) > rearCurrent && IPhaseCount == 0)
                    {
                        double deltaIRear1 = double.Parse(currentLine[iRearCol]) - double.Parse(previousLine[iRearCol]);
                        double deltaIRear2 = rearCurrent - double.Parse(previousLine[iRearCol]);
                        double deltaIPhase1 = double.Parse(currentLine[iPhaseCol]) - double.Parse(previousLine[iPhaseCol]);
                        double deltaIPhase2 = (deltaIRear2 / deltaIRear1) * deltaIPhase1;
                        IPhaseAcq = double.Parse(previousLine[iPhaseCol]) + deltaIPhase2;
                        IPhaseCount = 1;
                        break;
                    }
                    previousLine = currentLine;
                }
            }

            if (IPhaseCount > 0)
            {
                IPhaseAcq = IPhaseAcq / IPhaseCount;
            }
            else
            {
                IPhaseAcq = -999;//When rearCurrent>=double.Parse(lines[nbrCols-1][iRearCol])+iRearGap
            }

            return IPhaseAcq;
        }
    }
}

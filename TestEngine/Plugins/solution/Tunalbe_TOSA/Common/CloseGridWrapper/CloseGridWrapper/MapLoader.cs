using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.Utilities;

namespace Bookham.Toolkit.CloseGrid
{
    /// <summary>
    /// Helper class to load CSV map files into a 2D double array
    /// </summary>
    /// <remarks>All methods static, don't construct!</remarks>
    public sealed class MapLoader
    {
        /// <summary>
        /// Private constructor
        /// </summary>
        private MapLoader() { }

        /// <summary>
        /// Load map file into memory
        /// </summary>
        /// <param name="file">Map file to load</param>
        /// <returns>2D array of double</returns>
        public static double[,] LoadMap(string file)
        {
            double[,] map = null;
            using (CsvReader csvReader = new CsvReader())
            {
                List<string[]> lines = csvReader.ReadFile(file);
                int nbrRows = lines.Count;
                int nbrColumns = lines[0].Length;

                map = new double[nbrRows, nbrColumns];
                string str;
                for (int ii=0; ii<nbrRows; ii++)
                {
                    for (int jj = 0; jj < nbrColumns; jj++)
                    {
                        str = lines[ii][jj];
                        map[ii, jj] = double.Parse(str);
                    }
                }
            }
            return map;
        }
    }
}

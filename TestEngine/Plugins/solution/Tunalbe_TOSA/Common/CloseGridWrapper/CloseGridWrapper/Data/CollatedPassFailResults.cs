using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.Toolkit.CloseGrid
{
    /// <summary>
    /// Class containing the collated pass/fail results of all scans
    /// </summary>
    public class CollatedPassFailResults
    {
        /// <summary>
        /// Constructor
        /// </summary>
        internal CollatedPassFailResults(CloseGridTestInfo info,
            short overallMapPassed, object mapsPassedArray, short overallPass)
        {
            if (overallMapPassed == 1) this.OverallMapPassed = true;
            else this.OverallMapPassed = false;
            if (overallPass == 1) this.OverallPass = true;
            else this.OverallPass = false;
            
            short[] shortArray = (short[]) mapsPassedArray;
            this.MapsPassedArray = new bool[shortArray.Length];
            for (int ii=0; ii<shortArray.Length; ii++)
            {
                bool pf;
                if (shortArray[ii] == 1) pf=true;
                else pf=false;

                MapsPassedArray[ii] = pf;
            }

            this.CollatedPassFailResultsFile = string.Format("{0}\\collatedPassFailResults_{1}.csv",
                info.ResultsDir, info.GetFileNameInfoStr());
        }
       
        /// <summary>Overall Map passed</summary>
        public readonly bool OverallMapPassed;
        /// <summary>Is the device an overall pass?</summary>
        public readonly bool OverallPass;
        /// <summary>Array of whether each supermode passed</summary>
        public readonly bool[] MapsPassedArray;
        /// <summary>Collated Pass/Fail Results file</summary>
        public readonly string CollatedPassFailResultsFile;

        
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestSolution.IlmzCommonData;

namespace Bookham.Toolkit.CloseGrid
{
    /// <summary>
    /// Results of ITU Characterisation
    /// </summary>
    public class GetCharResults
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="info">Test Info</param>
        /// <param name="nbrITUFound">Number of ITU points found</param>
        /// <param name="nbrITUDuff">Number of Duff (bad) ITU points</param>
        internal GetCharResults(CloseGridTestInfo info,
            short nbrITUFound, short nbrITUDuff)
        {
            this.NbrITUFound = nbrITUFound;
            this.NbrITUDuff = nbrITUDuff;
            this.ItuOperatingPointsFile = string.Format("{0}\\ITUOperatingPoints_{1}.csv",
                info.ResultsDir, info.GetFileNameInfoStr());
            // lookup the ITU channel data
            this.ItuChannelData = CsvDataLookup.GetItuOperatingPoints(this.ItuOperatingPointsFile);
        }

        /// <summary>
        /// Nbr of ITU points found
        /// </summary>
        public readonly short NbrITUFound;

        /// <summary>
        /// Nbr of ITU points that were duff
        /// </summary>
        public readonly short NbrITUDuff;

        /// <summary>
        /// ITU Operating points file
        /// </summary>
        public readonly string ItuOperatingPointsFile;

        /// <summary>
        /// ITU Operating points (channel) data
        /// </summary>
        public readonly DsdbrChannelData[] ItuChannelData;
    }
}

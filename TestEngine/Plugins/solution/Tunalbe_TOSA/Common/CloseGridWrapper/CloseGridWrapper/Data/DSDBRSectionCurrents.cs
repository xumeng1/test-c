using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.Toolkit.CloseGrid
{
    /// <summary>
    /// DSDBR Section currents
    /// </summary>
    public struct DSDBRSectionCurrents
    {
        /// <summary>Gain section</summary>
        public DSDBRSectionCurrent Gain;
        /// <summary>SOA section</summary>
        public DSDBRSectionCurrent SOA;
        /// <summary>Rear section</summary>
        public DSDBRSectionCurrent Rear;
        /// <summary>Phase section</summary>
        public DSDBRSectionCurrent Phase;
        /// <summary>Front1 section</summary>
        public DSDBRSectionCurrent Front1;
        /// <summary>Front2 section</summary>
        public DSDBRSectionCurrent Front2;
        /// <summary>Front3 section</summary>
        public DSDBRSectionCurrent Front3;
        /// <summary>Front4 section</summary>
        public DSDBRSectionCurrent Front4;
        /// <summary>Front5 section</summary>
        public DSDBRSectionCurrent Front5;
        /// <summary>Front6 section</summary>
        public DSDBRSectionCurrent Front6;
        /// <summary>Front7 section</summary>
        public DSDBRSectionCurrent Front7;
        /// <summary>Front8 section</summary>
        public DSDBRSectionCurrent Front8;
    }
}

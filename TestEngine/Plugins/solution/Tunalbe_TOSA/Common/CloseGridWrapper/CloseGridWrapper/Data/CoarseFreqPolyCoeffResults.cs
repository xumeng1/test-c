using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.Toolkit.CloseGrid
{
    /// <summary>
    /// Class containing the results of a polynomial coefficient calculation
    /// </summary>
    public class CoarseFreqPolyCoeffResults
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="polyCoeffs">Coarse Frequency polynomial coefficients</param>
        /// <param name="chiSquared">Chi squared metric on fit</param>
        internal CoarseFreqPolyCoeffResults(object polyCoeffs, double chiSquared)
        {
            this.PolyCoeffs = (double[])polyCoeffs;
            this.ChiSquared = chiSquared;
        }

        /// <summary>Coarse Frequency polynomial coefficients</summary>
        public readonly double[] PolyCoeffs;

        /// <summary>Chi squared metric on fit</summary>
        public readonly double ChiSquared;
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using AxCGSystemAXCLib;

namespace Bookham.Toolkit.CloseGrid
{
    /// <summary>
    /// Close Grid version information class
    /// </summary>
    public class CloseGridVersion
    {
        internal CloseGridVersion(AxCGSystemAXC closeGrid)
        {
            string copyright, label, labelInfo, labelComment;            
            closeGrid.getVersionInfo(out ProductName, out ReleaseNo, out copyright,
                out label, out labelInfo, out labelComment, out BuildConfig, out BuildDateTime,
                out DriverVerInfo);
        }

        /// <summary>Close Grid Product Name</summary>
        public readonly string ProductName;
        /// <summary>Release Number</summary>
        public readonly string ReleaseNo;
        /// <summary>Build Configuration info</summary>
        public readonly string BuildConfig;
        /// <summary>Build Date and Time</summary>
        public readonly string BuildDateTime;
        /// <summary>Driver Version Info</summary>
        public readonly string DriverVerInfo;

        /// <summary>
        /// ToString override
        /// </summary>
        /// <returns>String representation</returns>
        public override string ToString()
        {
            string ver = String.Format("{0} v {1}\nBuild={2}, {3}\nDrivers:\n{4}",
                ProductName, ReleaseNo, BuildConfig, BuildDateTime, DriverVerInfo);
            return ver;
        }
    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.Toolkit.CloseGrid
{
    /// <summary>
    /// Class containing the results of a Overall Map scan
    /// </summary>
    public class OverallMapResults
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="info">CloseGrid Test Info (results dir, laser type, id etc)</param>
        /// <param name="rampDir">Ramp direction</param>
        /// <param name="screeningPassed">Screening passed</param>
        /// <param name="supermodeCount">Supermode count</param>
        /// <param name="avgPowers_mW">Average power in each supermode</param>
        /// <param name="combinedDFSMapFile">Collated Front Section summary map filename</param>
        internal OverallMapResults(CloseGridTestInfo info, string rampDir, short screeningPassed,
            int supermodeCount, object avgPowers_mW, string combinedDFSMapFile)
        {
            this.RampDir = RampDirConvert.ToOverallRampDir(rampDir);
            if (screeningPassed == 1) ScreeningPassed = true;
            else ScreeningPassed = false;
            this.SupermodeCount = supermodeCount;

            this.AvgPowers_mW = (double[])avgPowers_mW;
            this.SupermodeLinesFile =
                string.Format("{0}\\lines_OverallMap_{1}.csv",
                info.ResultsDir, info.GetFileNameInfoStr());
            this.PowerRatioSmFile =
                string.Format("{0}\\PowerRatio_Continuity_{1}.csv",
                info.ResultsDir, info.GetFileNameInfoStr());
            this.PassFailFile =
               string.Format("{0}\\passFailMetrics_OverallMap_{1}.csv",
               info.ResultsDir, info.GetFileNameInfoStr());
            this.QaMetricsFile =
                string.Format("{0}\\qaMetrics_OverallMap_{1}.csv",
                info.ResultsDir, info.GetFileNameInfoStr());

            this.CombinedDFSMapFile = combinedDFSMapFile;
        }

        /// <summary>Ramp direction</summary>
        public readonly OverallRampDir RampDir;
        /// <summary>Screening passed</summary>
        public readonly bool ScreeningPassed;
        /// <summary>Supermode count</summary>
        public readonly int SupermodeCount;
        /// <summary>Average power in each supermode</summary>
        public readonly double[] AvgPowers_mW;
        /// <summary>Supermode line file</summary>        
        public readonly string SupermodeLinesFile;
        /// <summary>Power ratio (pseudo-frequency) vs supermode file</summary>                
        public readonly string PowerRatioSmFile;
        /// <summary>Pass/Fail file</summary>
        public readonly string PassFailFile;
        /// <summary>QA metrics file</summary>                        
        public readonly string QaMetricsFile;
        /// <summary>Collated Front Section summary map</summary>                                
        public readonly string CombinedDFSMapFile;
    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.Toolkit.CloseGrid
{
    /// <summary>
    /// Results of ITU Estimation
    /// </summary>
    public class EstimateITUPointsResults
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="info">Test Info</param>
        /// <param name="nbrITUEstimates">Number of ITU Estimate points found</param>
        internal EstimateITUPointsResults(CloseGridTestInfo info,
            short nbrITUEstimates)
        {
            this.NbrITUEstimates = nbrITUEstimates;

            this.ItuEstimatesFile = string.Format("{0}\\ITUOperatingPointEstimates_{1}.csv",
                info.ResultsDir, info.GetFileNameInfoStr());
        }

        /// <summary>
        /// Nbr of ITU Estimate points found
        /// </summary>
        public readonly short NbrITUEstimates;

        /// <summary>
        /// ITU estimates file
        /// </summary>
        public readonly string ItuEstimatesFile;
    }
}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// ImSetupData.cs
//
// Author: Mark Fullalove, 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.Utilities;
using Bookham.TestEngine.PluginInterfaces;

namespace Bookham.Toolkit.CloseGrid
{
    internal class ImSetupData
    {
        internal ImSetupData(CloseGridTestInfo testInfo)
        {
            this.testInfo = testInfo;
        }

        internal void ReadImSetup(int supermodeNumber)
        {
            string fileName = string.Format(testInfo.ResultsDir + "\\Im_{0}_SM{1}.csv", testInfo.GetFileNameInfoStr(), supermodeNumber);
            using (CsvReader reader = new CsvReader())
            {
                try
                {
                    List<string[]> lines = reader.ReadFile(fileName);
                    // all data in the second line
                    string[] firstLine = lines[1];
                    string[] finalLine = lines[lines.Count-1];

                    firstEntry.RearCurrent_mA = double.Parse(firstLine[0]);
                    lastEntry.RearCurrent_mA = double.Parse(finalLine[0]);

                    firstEntry.FrontPairNumber = int.Parse(firstLine[1]);
                    lastEntry.FrontPairNumber = int.Parse(finalLine[1]);

                    firstEntry.ConstantFrontCurrent_mA = double.Parse(firstLine[2]);
                    lastEntry.ConstantFrontCurrent_mA = double.Parse(finalLine[2]);

                    firstEntry.NonConstantFrontCurrent_mA = double.Parse(firstLine[3]);
                    lastEntry.NonConstantFrontCurrent_mA = double.Parse(finalLine[3]);
                }
                catch (Exception e)
                {
                    throw new ArgumentException("Invalid Supermode file", e);
                }

            }
        }

        internal MidChannelSetup FirstEntry
        {
            get { return firstEntry; }
        }

        internal MidChannelSetup LastEntry
        {
            get { return lastEntry; }
        }

        private static MidChannelSetup firstEntry;
        private static MidChannelSetup lastEntry;
        private CloseGridTestInfo testInfo;
    }

    /// <summary>
    /// Setup data for switching between channels in mid-channel file ( Im_..._.csv )
    /// </summary>
    public struct MidChannelSetup
    {
        /// <summary>
        /// The rear current
        /// </summary>
        public double RearCurrent_mA;
        /// <summary>
        /// The front pair number
        /// </summary>
        public int FrontPairNumber;
        /// <summary>
        /// The constant front current
        /// </summary>
        public double ConstantFrontCurrent_mA;
        /// <summary>
        /// The non-constant front current
        /// </summary>
        public double NonConstantFrontCurrent_mA;
    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.Toolkit.CloseGrid
{
    /// <summary>
    /// Class containing the results of a supermode scan
    /// </summary>
    public class SMMapResults
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="supermode">Supermode number (0 to N-1)</param>
        /// <param name="rampDir">Ramp direction</param>
        /// <param name="testInfo">CloseGrid Test Info (results dir, laser type, id etc)</param>
        /// <param name="screeningPassed">Screening passed</param>
        /// <param name="longitudinalModeCount">Longitudinal mode count</param>
        internal SMMapResults(short supermode, string rampDir, CloseGridTestInfo testInfo, short screeningPassed,
            int longitudinalModeCount)
        {
            this.Supermode = supermode;
            this.RampDir = RampDirConvert.ToSupermodeRampDir(rampDir);
            if (screeningPassed == 1) ScreeningPassed = true;
            else ScreeningPassed = false;
            this.LongitudinalModeCount = longitudinalModeCount;

            // common file ending - calculate it once!
            string filePostfix1 = string.Format("{0}_SM{1}.csv",
                        testInfo.GetFileNameInfoStr(), supermode);

            this.PassFailFile = string.Format("{0}\\passFailMetrics_{1}",
                        testInfo.ResultsDir, filePostfix1);
            this.QaMetricsFile = string.Format("{0}\\qaMetrics_{1}",
                        testInfo.ResultsDir, filePostfix1);
            this.LongitudinalModeLinesFile = string.Format("{0}\\lines_{1}",
                        testInfo.ResultsDir, filePostfix1);
            
            string filePostfix2 = string.Format("{0}_SM{1}_{2}.csv",
                        testInfo.GetFileNameInfoStr(), supermode, rampDir);
            
            this.MatrixPowerForwardFile = string.Format("{0}\\MATRIX_forwardPower_{1}",
                        testInfo.ResultsDir, filePostfix2);

            this.MatrixPowerRatioForwardFile = string.Format("{0}\\MATRIX_forwardPowerRatio_{1}",
                        testInfo.ResultsDir, filePostfix2);
            this.MatrixPowerRatioReverseFile = string.Format("{0}\\MATRIX_reversePowerRatio_{1}",
                        testInfo.ResultsDir, filePostfix2);

            this.MatrixPD1CurrentForwardFile = string.Format("{0}\\MATRIX_forwardPD1Current_{1}",
                        testInfo.ResultsDir, filePostfix2);
            this.MatrixPD1CurrentReverseFile = string.Format("{0}\\MATRIX_reversePD1Current_{1}",
                        testInfo.ResultsDir, filePostfix2);

            this.MatrixPD2CurrentForwardFile = string.Format("{0}\\MATRIX_forwardPD2Current_{1}",
                        testInfo.ResultsDir, filePostfix2);
            this.MatrixPD2CurrentReverseFile = string.Format("{0}\\MATRIX_reversePD2Current_{1}",
                        testInfo.ResultsDir, filePostfix2);      
      
            // lookup the hysteresis percentage
            string elemStr = CsvDataLookup.GetElement(this.QaMetricsFile, 9, 1);
            this.HysteresisPercent = Double.Parse(elemStr);
            
            // calculate the Modal Distortion
            double modalDist = CsvDataLookup.ModalDistortion
                (this.QaMetricsFile, this.LongitudinalModeCount, true);
            this.ModalDistortion = modalDist;
        }

        /// <summary>Supermode number (0 to N-1)</summary>
        public readonly short Supermode;
        /// <summary>Ramp direction</summary>
        public readonly SupermodeRampDir RampDir;
        /// <summary>Screening passed</summary>
        public readonly bool ScreeningPassed;
        /// <summary>Longitudinal mode count</summary>
        public readonly int LongitudinalModeCount;

        /// <summary>Hysteresis Percentage</summary>
        public readonly double HysteresisPercent;
        /// <summary>Modal Distortion</summary>
        public readonly double ModalDistortion;

        /// <summary>Pass/Fail file</summary>
        public readonly string PassFailFile;
        /// <summary>QA metrics file</summary>
        public readonly string QaMetricsFile;
        /// <summary>Longitudinal Mode lines file</summary>
        public readonly string LongitudinalModeLinesFile;

        /// <summary>Matrix file for power on forward sweep</summary>
        public readonly string MatrixPowerForwardFile;

        /// <summary>Matrix file for power ratio on forward sweep</summary>
        public readonly string MatrixPowerRatioForwardFile;
        /// <summary>Matrix file for power ratio on reverse sweep</summary>
        public readonly string MatrixPowerRatioReverseFile;

        /// <summary>Matrix file for photocurrent 1 on forward sweep</summary>
        public readonly string MatrixPD1CurrentForwardFile;
        /// <summary>Matrix file for photocurrent 1 on reverse sweep</summary>
        public readonly string MatrixPD1CurrentReverseFile;
        /// <summary>Matrix file for photocurrent 2 on forward sweep</summary>
        public readonly string MatrixPD2CurrentForwardFile;
        /// <summary>Matrix file for photocurrent 2 on reverse sweep</summary>
        public readonly string MatrixPD2CurrentReverseFile;

    }
}

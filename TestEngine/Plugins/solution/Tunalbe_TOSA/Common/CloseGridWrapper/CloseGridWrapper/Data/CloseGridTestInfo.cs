using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.Toolkit.CloseGrid
{    
    /// <summary>
    /// Close Grid Test Information: this information is used a lot by CloseGrid, especially in naming
    /// output files.
    /// </summary>
    public class CloseGridTestInfo
    {
        /// <summary>
        /// Public Constructor - uses RampDir enum
        /// </summary>
        /// <param name="resultsDir">Results Directory</param>
        /// <param name="laserType">Laser Type</param>
        /// <param name="laserId">Laser ID</param>
        /// <param name="dateTimeStamp">Date Time Stamp</param>
        public CloseGridTestInfo(string resultsDir, string laserType, string laserId,
            string dateTimeStamp)            
        {
            this.ResultsDir = resultsDir;
            this.DateTimeStamp = dateTimeStamp;
            this.LaserType = laserType;
            this.LaserId = laserId;            
        }

        /// <summary>Results Directory</summary>
        public readonly string ResultsDir;
        /// <summary>Date Time Stamp</summary>
        public readonly string DateTimeStamp; 
        /// <summary>Laser Type</summary>
        public readonly string LaserType;
        /// <summary>Laser ID</summary>
        public readonly string LaserId;
        
        /// <summary>
        /// Get the string that is commonly used in file names based on this structure.
        /// Combination of laser type, id and date-time stamp
        /// </summary>
        /// <returns>info string</returns>
        public string GetFileNameInfoStr()
        {
            string infoStr = string.Format("{0}_{1}_{2}",
                this.LaserType, this.LaserId, this.DateTimeStamp);
            return infoStr;
        }

    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.Toolkit.CloseGrid
{
    /// <summary>
    /// DSDBR Section Current Status
    /// </summary>
    public struct DSDBRSectionCurrent
    {
        /// <summary>
        /// Convenience constructor
        /// </summary>
        /// <param name="enabled">Current source enabled</param>
        /// <param name="current_mA">Current</param>
        public DSDBRSectionCurrent(bool enabled, double current_mA)
        {
            this.Enabled = enabled;
            this.Current_mA = current_mA;
        }

        /// <summary>
        /// Section Current Enabled
        /// </summary>
        public bool Enabled;

        /// <summary>
        /// Section current in mA
        /// </summary>
        public double Current_mA;

        /// <summary>
        /// Override of ToString() - aids Debug
        /// </summary>
        /// <returns>String representation</returns>
        public override string ToString()
        {
            if (!Enabled) return "Disabled";
            else
            {
                string str = string.Format("{1} mA", Current_mA);
                return str;
            }
        }
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace Bookham.Toolkit.CloseGrid
{
    // Alice.Huang     2010-04-20
    // Comment all operation related with CloseGrid ActiveX OCX to avoid solution to depend on CloseGrid

    /// <summary>
    /// The CLoseGrid OCX (AxCGSystemAXCLib.AxCGSystemAXC) has to live
    /// in a special thread inside of a user control. 
    /// This thread has to be of STA Apartment state, and be running a Windows message pump.
    /// Looks like after all this is done, any thread can use the OCX object!
    /// </summary>
    /// <remarks>Only one object of this type can exist - singleton pattern</remarks>
    internal sealed class CGWrapThread
    {
        #region Singleton Pattern
        /// <summary>
        ///  Access the class as a singleton.
        /// </summary>
        internal static CGWrapThread Singleton
        {
            get
            {
                return singletonInstance;                
            }
        }

        /// <summary>
        /// This is the singleton instance
        /// </summary>
        static private CGWrapThread singletonInstance = new CGWrapThread();
        #endregion

        /// <summary>
        /// Private constructor
        /// </summary>
        private CGWrapThread()
        {
            thread = new Thread(new ThreadStart(threadCode));
            thread.SetApartmentState(ApartmentState.STA);
            thread.Name = "CloseGridWrapper";
            form = null;
            ctrlInitialised = false;

        }
        
        /// <summary>
        /// Method to start our thread
        /// </summary>
        internal void Start()
        {
            thread.Start();
            // wait for our dummy thread to startup before returning
            while (!ctrlInitialised) Thread.Sleep(100);
        }

        ///// <summary>
        ///// Accessor for the CloseGrid ActiveX OCX
        ///// </summary>
        //internal AxCGSystemAXCLib.AxCGSystemAXC CloseGridOCX
        //{
        //    get
        //    {
        //        return form.axCGSystemAXC1;
        //    }
        //}

        /// <summary>
        /// Delegate definition needed for Dispose
        /// </summary>
        internal delegate void myDelegate();

        /// <summary>
        /// Dispose of the thread
        /// </summary>
        internal void Dispose()
        {
            // need to close the form, which will in-turn kill the thread!
            if (form.InvokeRequired)
            {
                myDelegate md= new myDelegate(form.Close);
                // have to use a delegate - use Invoke to perform the cross-threaded comms!
                form.Invoke(md);
            }            
        }

        /// <summary>
        /// This method contains what will run in our thread
        /// </summary>
        private void threadCode()
        {
            // create our dummy user control, just to host the CLoseGrid OCX
            form = new InvisibleForm();
            form.Visible = false;
            // set flag to say that we're ready...
            ctrlInitialised = true;
            // run a Windows Event loop on this invisible form
            Application.Run(form);
        }

        #region Private data
        /// <summary>
        /// The thread object
        /// </summary>
        private Thread thread;

        /// <summary>
        /// Our dummy user control to host the CLoseGrid OCX
        /// </summary>
        private InvisibleForm form;

        private bool ctrlInitialised;
        #endregion
    }
}

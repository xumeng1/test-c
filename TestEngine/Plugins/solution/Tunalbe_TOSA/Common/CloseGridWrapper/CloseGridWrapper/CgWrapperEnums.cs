using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.Toolkit.CloseGrid
{
    /// <summary>
    /// Close Grid state machine state names
    /// </summary>
    public enum CloseGridState
    {
        /// <summary>Instantiated State</summary>
        Instantiated,
        /// <summary>Startup State</summary>
        Startup,
        /// <summary>Setup State</summary>
        Setup,
        /// <summary>Busy State</summary>
        Busy,
        /// <summary>Complete State</summary>
        Complete
    }

    /// <summary>
    /// Optical Power meter head
    /// </summary>
    public enum PowerMeterHead
    {
        /// <summary>Reference meter (tap of fibre output)</summary>
        Reference,
        /// <summary>Filter meter (via optical filter to generate pseudo-freq)</summary>
        Filter
    }

    /// <summary>
    /// Locker Photo-Current Meter head
    /// </summary>
    public enum LockerCurrentHead
    {
        /// <summary>Transmit photodiode</summary>        
        Transmit,
        /// <summary>Reflect photodiode</summary>        
        Reflect
    }
}
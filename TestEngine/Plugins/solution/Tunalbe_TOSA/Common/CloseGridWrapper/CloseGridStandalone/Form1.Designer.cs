namespace CloseGridStandalone
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnStartup = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.labelStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.btnSetup = new System.Windows.Forms.Button();
            this.btnShutdown = new System.Windows.Forms.Button();
            this.btnStopAndShut = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemAboutCloseGrid = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemAboutCGApp = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageInstr = new System.Windows.Forms.TabPage();
            this.btnSoaCompSet = new System.Windows.Forms.Button();
            this.btnSoaCompGet = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.numSoaCompliance = new System.Windows.Forms.NumericUpDown();
            this.btnSetSections = new System.Windows.Forms.Button();
            this.btnGetSections = new System.Windows.Forms.Button();
            this.dsdbrSectionsCtrl = new Bookham.Toolkit.CloseGrid.DSDBRSectionsCtrl();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.labelTemperature = new System.Windows.Forms.Label();
            this.btnGetReadings = new System.Windows.Forms.Button();
            this.labelFreqReading = new System.Windows.Forms.Label();
            this.labelPowerReading = new System.Windows.Forms.Label();
            this.tabPageOverallMap = new System.Windows.Forms.TabPage();
            this.mapViewOverall = new Bookham.Toolkit.CloseGrid.MapViewCtrl();
            this.overallMapResultsCtrl1 = new Bookham.Toolkit.CloseGrid.OverallMapResultsCtrl();
            this.btnLoadOverallMap = new System.Windows.Forms.Button();
            this.btnGetOverallMapResults = new System.Windows.Forms.Button();
            this.btnStartOverallMap = new System.Windows.Forms.Button();
            this.tabPageSMMap = new System.Windows.Forms.TabPage();
            this.mapViewSM = new Bookham.Toolkit.CloseGrid.MapViewCtrl();
            this.label1 = new System.Windows.Forms.Label();
            this.numSuperMode = new System.Windows.Forms.NumericUpDown();
            this.btnLoadSMMap = new System.Windows.Forms.Button();
            this.btnGetSMResults = new System.Windows.Forms.Button();
            this.btnDoSMMap = new System.Windows.Forms.Button();
            this.smMapResultsCtrl1 = new Bookham.Toolkit.CloseGrid.UserControls.SMMapResultsCtrl();
            this.tabPageCharacterisation = new System.Windows.Forms.TabPage();
            this.btnGetCharResults = new System.Windows.Forms.Button();
            this.btnStartLaserChar = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.labelNbrITUDuff = new System.Windows.Forms.Label();
            this.labelNbrITUFound = new System.Windows.Forms.Label();
            this.labelNbrITUEst = new System.Windows.Forms.Label();
            this.btnGetITUEstimates = new System.Windows.Forms.Button();
            this.collatedPassFailResultsCtrl1 = new Bookham.Toolkit.CloseGrid.UserControls.CollatedPassFailResultsCtrl();
            this.btnCollatePFResults = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.closeGridTestInfoCtrl1 = new Bookham.Toolkit.CloseGrid.CloseGridTestInfoCtrl();
            this.button3 = new System.Windows.Forms.Button();
            this.btnGetScreeningResults = new System.Windows.Forms.Button();
            this.mapViewCtrl1 = new Bookham.Toolkit.CloseGrid.MapViewCtrl();
            this.textBoxSoaActuatorAddress = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.checkBoxSimulation = new System.Windows.Forms.CheckBox();
            this.statusStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPageInstr.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSoaCompliance)).BeginInit();
            this.tabPageOverallMap.SuspendLayout();
            this.tabPageSMMap.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSuperMode)).BeginInit();
            this.tabPageCharacterisation.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnStartup
            // 
            this.btnStartup.Location = new System.Drawing.Point(188, 48);
            this.btnStartup.Name = "btnStartup";
            this.btnStartup.Size = new System.Drawing.Size(75, 23);
            this.btnStartup.TabIndex = 1;
            this.btnStartup.Text = "Startup";
            this.btnStartup.UseVisualStyleBackColor = true;
            this.btnStartup.Click += new System.EventHandler(this.btnStartup_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.labelStatus,
            this.toolStripProgressBar1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 508);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(835, 22);
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // labelStatus
            // 
            this.labelStatus.AutoSize = false;
            this.labelStatus.BackColor = System.Drawing.SystemColors.Window;
            this.labelStatus.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(80, 17);
            this.labelStatus.Text = "...";
            // 
            // toolStripProgressBar1
            // 
            this.toolStripProgressBar1.Name = "toolStripProgressBar1";
            this.toolStripProgressBar1.Size = new System.Drawing.Size(200, 16);
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // btnSetup
            // 
            this.btnSetup.Location = new System.Drawing.Point(269, 47);
            this.btnSetup.Name = "btnSetup";
            this.btnSetup.Size = new System.Drawing.Size(75, 23);
            this.btnSetup.TabIndex = 4;
            this.btnSetup.Text = "Setup";
            this.btnSetup.UseVisualStyleBackColor = true;
            this.btnSetup.Click += new System.EventHandler(this.btnSetup_Click);
            // 
            // btnShutdown
            // 
            this.btnShutdown.Location = new System.Drawing.Point(12, 85);
            this.btnShutdown.Name = "btnShutdown";
            this.btnShutdown.Size = new System.Drawing.Size(75, 23);
            this.btnShutdown.TabIndex = 4;
            this.btnShutdown.Text = "Shutdown";
            this.btnShutdown.UseVisualStyleBackColor = true;
            this.btnShutdown.Click += new System.EventHandler(this.btnShutdown_Click);
            // 
            // btnStopAndShut
            // 
            this.btnStopAndShut.Location = new System.Drawing.Point(93, 85);
            this.btnStopAndShut.Name = "btnStopAndShut";
            this.btnStopAndShut.Size = new System.Drawing.Size(97, 23);
            this.btnStopAndShut.TabIndex = 4;
            this.btnStopAndShut.Text = "Stop && Shutdown";
            this.btnStopAndShut.UseVisualStyleBackColor = true;
            this.btnStopAndShut.Click += new System.EventHandler(this.btnStopAndShut_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(835, 24);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItemAboutCloseGrid,
            this.menuItemAboutCGApp});
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // menuItemAboutCloseGrid
            // 
            this.menuItemAboutCloseGrid.Name = "menuItemAboutCloseGrid";
            this.menuItemAboutCloseGrid.Size = new System.Drawing.Size(212, 22);
            this.menuItemAboutCloseGrid.Text = "Close Grid";
            this.menuItemAboutCloseGrid.Click += new System.EventHandler(this.menuItemAboutCloseGrid_Click);
            // 
            // menuItemAboutCGApp
            // 
            this.menuItemAboutCGApp.Name = "menuItemAboutCGApp";
            this.menuItemAboutCGApp.Size = new System.Drawing.Size(212, 22);
            this.menuItemAboutCGApp.Text = "Close Grid Standalone App";
            this.menuItemAboutCGApp.Click += new System.EventHandler(this.menuItemAboutCGApp_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPageInstr);
            this.tabControl1.Controls.Add(this.tabPageOverallMap);
            this.tabControl1.Controls.Add(this.tabPageSMMap);
            this.tabControl1.Controls.Add(this.tabPageCharacterisation);
            this.tabControl1.Location = new System.Drawing.Point(12, 134);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(811, 371);
            this.tabControl1.TabIndex = 12;
            // 
            // tabPageInstr
            // 
            this.tabPageInstr.Controls.Add(this.btnSoaCompSet);
            this.tabPageInstr.Controls.Add(this.btnSoaCompGet);
            this.tabPageInstr.Controls.Add(this.label8);
            this.tabPageInstr.Controls.Add(this.numSoaCompliance);
            this.tabPageInstr.Controls.Add(this.btnSetSections);
            this.tabPageInstr.Controls.Add(this.btnGetSections);
            this.tabPageInstr.Controls.Add(this.dsdbrSectionsCtrl);
            this.tabPageInstr.Controls.Add(this.label5);
            this.tabPageInstr.Controls.Add(this.label4);
            this.tabPageInstr.Controls.Add(this.label3);
            this.tabPageInstr.Controls.Add(this.labelTemperature);
            this.tabPageInstr.Controls.Add(this.btnGetReadings);
            this.tabPageInstr.Controls.Add(this.labelFreqReading);
            this.tabPageInstr.Controls.Add(this.labelPowerReading);
            this.tabPageInstr.Location = new System.Drawing.Point(4, 22);
            this.tabPageInstr.Name = "tabPageInstr";
            this.tabPageInstr.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageInstr.Size = new System.Drawing.Size(803, 345);
            this.tabPageInstr.TabIndex = 2;
            this.tabPageInstr.Text = "Instruments";
            this.tabPageInstr.UseVisualStyleBackColor = true;
            // 
            // btnSoaCompSet
            // 
            this.btnSoaCompSet.Location = new System.Drawing.Point(478, 85);
            this.btnSoaCompSet.Name = "btnSoaCompSet";
            this.btnSoaCompSet.Size = new System.Drawing.Size(75, 23);
            this.btnSoaCompSet.TabIndex = 11;
            this.btnSoaCompSet.Text = "Set";
            this.btnSoaCompSet.UseVisualStyleBackColor = true;
            this.btnSoaCompSet.Click += new System.EventHandler(this.btnSoaCompSet_Click);
            // 
            // btnSoaCompGet
            // 
            this.btnSoaCompGet.Location = new System.Drawing.Point(478, 59);
            this.btnSoaCompGet.Name = "btnSoaCompGet";
            this.btnSoaCompGet.Size = new System.Drawing.Size(75, 23);
            this.btnSoaCompGet.TabIndex = 10;
            this.btnSoaCompGet.Text = "Get";
            this.btnSoaCompGet.UseVisualStyleBackColor = true;
            this.btnSoaCompGet.Click += new System.EventHandler(this.btnSoaCompGet_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(348, 43);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(87, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "SOA Compliance";
            // 
            // numSoaCompliance
            // 
            this.numSoaCompliance.DecimalPlaces = 4;
            this.numSoaCompliance.Location = new System.Drawing.Point(351, 61);
            this.numSoaCompliance.Name = "numSoaCompliance";
            this.numSoaCompliance.Size = new System.Drawing.Size(120, 20);
            this.numSoaCompliance.TabIndex = 8;
            // 
            // btnSetSections
            // 
            this.btnSetSections.Location = new System.Drawing.Point(211, 35);
            this.btnSetSections.Name = "btnSetSections";
            this.btnSetSections.Size = new System.Drawing.Size(103, 23);
            this.btnSetSections.TabIndex = 7;
            this.btnSetSections.Text = "Set Sections";
            this.btnSetSections.UseVisualStyleBackColor = true;
            this.btnSetSections.Click += new System.EventHandler(this.btnSetSections_Click);
            // 
            // btnGetSections
            // 
            this.btnGetSections.Location = new System.Drawing.Point(211, 6);
            this.btnGetSections.Name = "btnGetSections";
            this.btnGetSections.Size = new System.Drawing.Size(103, 23);
            this.btnGetSections.TabIndex = 7;
            this.btnGetSections.Text = "Get Sections";
            this.btnGetSections.UseVisualStyleBackColor = true;
            this.btnGetSections.Click += new System.EventHandler(this.btnGetSections_Click);
            // 
            // dsdbrSectionsCtrl
            // 
            this.dsdbrSectionsCtrl.Location = new System.Drawing.Point(6, 3);
            this.dsdbrSectionsCtrl.Name = "dsdbrSectionsCtrl";
            this.dsdbrSectionsCtrl.Size = new System.Drawing.Size(199, 321);
            this.dsdbrSectionsCtrl.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(620, 90);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(101, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Temperature (degC)";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(620, 43);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Frequency (GHz)";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(624, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Power (mW)";
            // 
            // labelTemperature
            // 
            this.labelTemperature.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTemperature.BackColor = System.Drawing.SystemColors.Window;
            this.labelTemperature.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelTemperature.Location = new System.Drawing.Point(623, 106);
            this.labelTemperature.Name = "labelTemperature";
            this.labelTemperature.Size = new System.Drawing.Size(100, 23);
            this.labelTemperature.TabIndex = 3;
            // 
            // btnGetReadings
            // 
            this.btnGetReadings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGetReadings.Location = new System.Drawing.Point(641, 141);
            this.btnGetReadings.Name = "btnGetReadings";
            this.btnGetReadings.Size = new System.Drawing.Size(80, 23);
            this.btnGetReadings.TabIndex = 2;
            this.btnGetReadings.Text = "Get Readings";
            this.btnGetReadings.UseVisualStyleBackColor = true;
            this.btnGetReadings.Click += new System.EventHandler(this.btnGetReadings_Click);
            // 
            // labelFreqReading
            // 
            this.labelFreqReading.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelFreqReading.BackColor = System.Drawing.SystemColors.Window;
            this.labelFreqReading.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelFreqReading.Location = new System.Drawing.Point(623, 59);
            this.labelFreqReading.Name = "labelFreqReading";
            this.labelFreqReading.Size = new System.Drawing.Size(100, 23);
            this.labelFreqReading.TabIndex = 1;
            // 
            // labelPowerReading
            // 
            this.labelPowerReading.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelPowerReading.BackColor = System.Drawing.SystemColors.Window;
            this.labelPowerReading.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelPowerReading.Location = new System.Drawing.Point(623, 19);
            this.labelPowerReading.Name = "labelPowerReading";
            this.labelPowerReading.Size = new System.Drawing.Size(100, 23);
            this.labelPowerReading.TabIndex = 0;
            // 
            // tabPageOverallMap
            // 
            this.tabPageOverallMap.Controls.Add(this.mapViewOverall);
            this.tabPageOverallMap.Controls.Add(this.overallMapResultsCtrl1);
            this.tabPageOverallMap.Controls.Add(this.btnLoadOverallMap);
            this.tabPageOverallMap.Controls.Add(this.btnGetOverallMapResults);
            this.tabPageOverallMap.Controls.Add(this.btnStartOverallMap);
            this.tabPageOverallMap.Location = new System.Drawing.Point(4, 22);
            this.tabPageOverallMap.Name = "tabPageOverallMap";
            this.tabPageOverallMap.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageOverallMap.Size = new System.Drawing.Size(803, 345);
            this.tabPageOverallMap.TabIndex = 0;
            this.tabPageOverallMap.Text = "Overall Map";
            this.tabPageOverallMap.UseVisualStyleBackColor = true;
            // 
            // mapViewOverall
            // 
            this.mapViewOverall.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.mapViewOverall.Location = new System.Drawing.Point(197, 7);
            this.mapViewOverall.Name = "mapViewOverall";
            this.mapViewOverall.Size = new System.Drawing.Size(591, 332);
            this.mapViewOverall.TabIndex = 3;
            // 
            // overallMapResultsCtrl1
            // 
            this.overallMapResultsCtrl1.Location = new System.Drawing.Point(6, 93);
            this.overallMapResultsCtrl1.Name = "overallMapResultsCtrl1";
            this.overallMapResultsCtrl1.RampDir = Bookham.Toolkit.CloseGrid.OverallRampDir.Front;
            this.overallMapResultsCtrl1.Size = new System.Drawing.Size(130, 151);
            this.overallMapResultsCtrl1.TabIndex = 2;
            // 
            // btnLoadOverallMap
            // 
            this.btnLoadOverallMap.Location = new System.Drawing.Point(7, 63);
            this.btnLoadOverallMap.Name = "btnLoadOverallMap";
            this.btnLoadOverallMap.Size = new System.Drawing.Size(129, 24);
            this.btnLoadOverallMap.TabIndex = 0;
            this.btnLoadOverallMap.Text = "Load Overall Map";
            this.btnLoadOverallMap.UseVisualStyleBackColor = true;
            this.btnLoadOverallMap.Click += new System.EventHandler(this.btnLoadOverallMap_Click);
            // 
            // btnGetOverallMapResults
            // 
            this.btnGetOverallMapResults.Location = new System.Drawing.Point(7, 36);
            this.btnGetOverallMapResults.Name = "btnGetOverallMapResults";
            this.btnGetOverallMapResults.Size = new System.Drawing.Size(129, 21);
            this.btnGetOverallMapResults.TabIndex = 0;
            this.btnGetOverallMapResults.Text = "Get Screening Results";
            this.btnGetOverallMapResults.UseVisualStyleBackColor = true;
            this.btnGetOverallMapResults.Click += new System.EventHandler(this.btnGetScreeningResults_Click);
            // 
            // btnStartOverallMap
            // 
            this.btnStartOverallMap.Location = new System.Drawing.Point(7, 7);
            this.btnStartOverallMap.Name = "btnStartOverallMap";
            this.btnStartOverallMap.Size = new System.Drawing.Size(129, 23);
            this.btnStartOverallMap.TabIndex = 0;
            this.btnStartOverallMap.Text = "Do Overall Map";
            this.btnStartOverallMap.UseVisualStyleBackColor = true;
            this.btnStartOverallMap.Click += new System.EventHandler(this.btnStartOverallMap_Click);
            // 
            // tabPageSMMap
            // 
            this.tabPageSMMap.Controls.Add(this.mapViewSM);
            this.tabPageSMMap.Controls.Add(this.label1);
            this.tabPageSMMap.Controls.Add(this.numSuperMode);
            this.tabPageSMMap.Controls.Add(this.btnLoadSMMap);
            this.tabPageSMMap.Controls.Add(this.btnGetSMResults);
            this.tabPageSMMap.Controls.Add(this.btnDoSMMap);
            this.tabPageSMMap.Controls.Add(this.smMapResultsCtrl1);
            this.tabPageSMMap.Location = new System.Drawing.Point(4, 22);
            this.tabPageSMMap.Name = "tabPageSMMap";
            this.tabPageSMMap.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageSMMap.Size = new System.Drawing.Size(803, 345);
            this.tabPageSMMap.TabIndex = 1;
            this.tabPageSMMap.Text = "Supermode Maps";
            this.tabPageSMMap.UseVisualStyleBackColor = true;
            // 
            // mapViewSM
            // 
            this.mapViewSM.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.mapViewSM.Location = new System.Drawing.Point(206, 6);
            this.mapViewSM.Name = "mapViewSM";
            this.mapViewSM.Size = new System.Drawing.Size(591, 332);
            this.mapViewSM.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Supermode Number";
            // 
            // numSuperMode
            // 
            this.numSuperMode.Location = new System.Drawing.Point(6, 19);
            this.numSuperMode.Name = "numSuperMode";
            this.numSuperMode.Size = new System.Drawing.Size(120, 20);
            this.numSuperMode.TabIndex = 4;
            // 
            // btnLoadSMMap
            // 
            this.btnLoadSMMap.Location = new System.Drawing.Point(6, 101);
            this.btnLoadSMMap.Name = "btnLoadSMMap";
            this.btnLoadSMMap.Size = new System.Drawing.Size(120, 24);
            this.btnLoadSMMap.TabIndex = 3;
            this.btnLoadSMMap.Text = "Load SM Map";
            this.btnLoadSMMap.UseVisualStyleBackColor = true;
            this.btnLoadSMMap.Click += new System.EventHandler(this.btnLoadSMMap_Click);
            // 
            // btnGetSMResults
            // 
            this.btnGetSMResults.Location = new System.Drawing.Point(6, 74);
            this.btnGetSMResults.Name = "btnGetSMResults";
            this.btnGetSMResults.Size = new System.Drawing.Size(120, 21);
            this.btnGetSMResults.TabIndex = 2;
            this.btnGetSMResults.Text = "Get SM Results";
            this.btnGetSMResults.UseVisualStyleBackColor = true;
            this.btnGetSMResults.Click += new System.EventHandler(this.btnGetSMResults_Click);
            // 
            // btnDoSMMap
            // 
            this.btnDoSMMap.Location = new System.Drawing.Point(6, 45);
            this.btnDoSMMap.Name = "btnDoSMMap";
            this.btnDoSMMap.Size = new System.Drawing.Size(120, 23);
            this.btnDoSMMap.TabIndex = 1;
            this.btnDoSMMap.Text = "Do SM Map";
            this.btnDoSMMap.UseVisualStyleBackColor = true;
            this.btnDoSMMap.Click += new System.EventHandler(this.btnDoSMMap_Click);
            // 
            // smMapResultsCtrl1
            // 
            this.smMapResultsCtrl1.Location = new System.Drawing.Point(9, 131);
            this.smMapResultsCtrl1.Name = "smMapResultsCtrl1";
            this.smMapResultsCtrl1.RampDir = Bookham.Toolkit.CloseGrid.SupermodeRampDir.Phase;
            this.smMapResultsCtrl1.Size = new System.Drawing.Size(117, 110);
            this.smMapResultsCtrl1.TabIndex = 0;
            // 
            // tabPageCharacterisation
            // 
            this.tabPageCharacterisation.Controls.Add(this.btnGetCharResults);
            this.tabPageCharacterisation.Controls.Add(this.btnStartLaserChar);
            this.tabPageCharacterisation.Controls.Add(this.label2);
            this.tabPageCharacterisation.Controls.Add(this.label6);
            this.tabPageCharacterisation.Controls.Add(this.labelNbrITUDuff);
            this.tabPageCharacterisation.Controls.Add(this.labelNbrITUFound);
            this.tabPageCharacterisation.Controls.Add(this.labelNbrITUEst);
            this.tabPageCharacterisation.Controls.Add(this.btnGetITUEstimates);
            this.tabPageCharacterisation.Controls.Add(this.collatedPassFailResultsCtrl1);
            this.tabPageCharacterisation.Controls.Add(this.btnCollatePFResults);
            this.tabPageCharacterisation.Location = new System.Drawing.Point(4, 22);
            this.tabPageCharacterisation.Name = "tabPageCharacterisation";
            this.tabPageCharacterisation.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageCharacterisation.Size = new System.Drawing.Size(803, 345);
            this.tabPageCharacterisation.TabIndex = 3;
            this.tabPageCharacterisation.Text = "Characterisation";
            this.tabPageCharacterisation.UseVisualStyleBackColor = true;
            // 
            // btnGetCharResults
            // 
            this.btnGetCharResults.Location = new System.Drawing.Point(221, 111);
            this.btnGetCharResults.Name = "btnGetCharResults";
            this.btnGetCharResults.Size = new System.Drawing.Size(106, 23);
            this.btnGetCharResults.TabIndex = 10;
            this.btnGetCharResults.Text = "Get Char Results";
            this.btnGetCharResults.UseVisualStyleBackColor = true;
            this.btnGetCharResults.Click += new System.EventHandler(this.btnGetCharResults_Click);
            // 
            // btnStartLaserChar
            // 
            this.btnStartLaserChar.Location = new System.Drawing.Point(221, 66);
            this.btnStartLaserChar.Name = "btnStartLaserChar";
            this.btnStartLaserChar.Size = new System.Drawing.Size(106, 38);
            this.btnStartLaserChar.TabIndex = 9;
            this.btnStartLaserChar.Text = "Start Laser Characterisation";
            this.btnStartLaserChar.UseVisualStyleBackColor = true;
            this.btnStartLaserChar.Click += new System.EventHandler(this.btnStartLaserChar_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(378, 98);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Duff";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(334, 23);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Found";
            // 
            // labelNbrITUDuff
            // 
            this.labelNbrITUDuff.BackColor = System.Drawing.SystemColors.Window;
            this.labelNbrITUDuff.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelNbrITUDuff.Location = new System.Drawing.Point(381, 111);
            this.labelNbrITUDuff.Name = "labelNbrITUDuff";
            this.labelNbrITUDuff.Size = new System.Drawing.Size(28, 23);
            this.labelNbrITUDuff.TabIndex = 7;
            // 
            // labelNbrITUFound
            // 
            this.labelNbrITUFound.BackColor = System.Drawing.SystemColors.Window;
            this.labelNbrITUFound.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelNbrITUFound.Location = new System.Drawing.Point(337, 111);
            this.labelNbrITUFound.Name = "labelNbrITUFound";
            this.labelNbrITUFound.Size = new System.Drawing.Size(28, 23);
            this.labelNbrITUFound.TabIndex = 7;
            // 
            // labelNbrITUEst
            // 
            this.labelNbrITUEst.BackColor = System.Drawing.SystemColors.Window;
            this.labelNbrITUEst.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelNbrITUEst.Location = new System.Drawing.Point(337, 36);
            this.labelNbrITUEst.Name = "labelNbrITUEst";
            this.labelNbrITUEst.Size = new System.Drawing.Size(28, 23);
            this.labelNbrITUEst.TabIndex = 7;
            // 
            // btnGetITUEstimates
            // 
            this.btnGetITUEstimates.Location = new System.Drawing.Point(221, 36);
            this.btnGetITUEstimates.Name = "btnGetITUEstimates";
            this.btnGetITUEstimates.Size = new System.Drawing.Size(106, 23);
            this.btnGetITUEstimates.TabIndex = 6;
            this.btnGetITUEstimates.Text = "Get ITU Estimates";
            this.btnGetITUEstimates.UseVisualStyleBackColor = true;
            this.btnGetITUEstimates.Click += new System.EventHandler(this.btnGetITUEstimates_Click);
            // 
            // collatedPassFailResultsCtrl1
            // 
            this.collatedPassFailResultsCtrl1.Location = new System.Drawing.Point(3, 36);
            this.collatedPassFailResultsCtrl1.Name = "collatedPassFailResultsCtrl1";
            this.collatedPassFailResultsCtrl1.Size = new System.Drawing.Size(149, 209);
            this.collatedPassFailResultsCtrl1.TabIndex = 5;
            // 
            // btnCollatePFResults
            // 
            this.btnCollatePFResults.Location = new System.Drawing.Point(6, 6);
            this.btnCollatePFResults.Name = "btnCollatePFResults";
            this.btnCollatePFResults.Size = new System.Drawing.Size(146, 24);
            this.btnCollatePFResults.TabIndex = 4;
            this.btnCollatePFResults.Text = "Collate Pass Fail Results";
            this.btnCollatePFResults.UseVisualStyleBackColor = true;
            this.btnCollatePFResults.Click += new System.EventHandler(this.btnCollatePFResults_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 400;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // closeGridTestInfoCtrl1
            // 
            this.closeGridTestInfoCtrl1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.closeGridTestInfoCtrl1.DateTimeStamp = "";
            this.closeGridTestInfoCtrl1.LaserId = "TRY";
            this.closeGridTestInfoCtrl1.LaserType = "DSDBR01";
            this.closeGridTestInfoCtrl1.Location = new System.Drawing.Point(507, 27);
            this.closeGridTestInfoCtrl1.Name = "closeGridTestInfoCtrl1";
            this.closeGridTestInfoCtrl1.Size = new System.Drawing.Size(316, 104);
            this.closeGridTestInfoCtrl1.TabIndex = 13;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(6, 45);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(120, 23);
            this.button3.TabIndex = 1;
            this.button3.Text = "Do Overall Map";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // btnGetScreeningResults
            // 
            this.btnGetScreeningResults.Location = new System.Drawing.Point(7, 36);
            this.btnGetScreeningResults.Name = "btnGetScreeningResults";
            this.btnGetScreeningResults.Size = new System.Drawing.Size(129, 21);
            this.btnGetScreeningResults.TabIndex = 0;
            this.btnGetScreeningResults.Text = "Get Screening Results";
            this.btnGetScreeningResults.UseVisualStyleBackColor = true;
            this.btnGetScreeningResults.Click += new System.EventHandler(this.btnGetScreeningResults_Click);
            // 
            // mapViewCtrl1
            // 
            this.mapViewCtrl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.mapViewCtrl1.Location = new System.Drawing.Point(197, 7);
            this.mapViewCtrl1.Name = "mapViewCtrl1";
            this.mapViewCtrl1.Size = new System.Drawing.Size(591, 332);
            this.mapViewCtrl1.TabIndex = 3;
            // 
            // textBoxSoaActuatorAddress
            // 
            this.textBoxSoaActuatorAddress.Location = new System.Drawing.Point(12, 50);
            this.textBoxSoaActuatorAddress.Name = "textBoxSoaActuatorAddress";
            this.textBoxSoaActuatorAddress.Size = new System.Drawing.Size(170, 20);
            this.textBoxSoaActuatorAddress.TabIndex = 14;
            this.textBoxSoaActuatorAddress.Text = "PXI2::15::INSTR";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 34);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(173, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "SOA Actuator Card VISA Resource";
            // 
            // checkBoxSimulation
            // 
            this.checkBoxSimulation.AutoSize = true;
            this.checkBoxSimulation.Enabled = false;
            this.checkBoxSimulation.Location = new System.Drawing.Point(367, 40);
            this.checkBoxSimulation.Name = "checkBoxSimulation";
            this.checkBoxSimulation.Size = new System.Drawing.Size(104, 30);
            this.checkBoxSimulation.TabIndex = 16;
            this.checkBoxSimulation.Text = "CloseGrid \r\nSimulation Mode";
            this.checkBoxSimulation.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(835, 530);
            this.Controls.Add(this.checkBoxSimulation);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.textBoxSoaActuatorAddress);
            this.Controls.Add(this.closeGridTestInfoCtrl1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.btnStopAndShut);
            this.Controls.Add(this.btnShutdown);
            this.Controls.Add(this.btnSetup);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.btnStartup);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Close Grid Standalone App";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPageInstr.ResumeLayout(false);
            this.tabPageInstr.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSoaCompliance)).EndInit();
            this.tabPageOverallMap.ResumeLayout(false);
            this.tabPageSMMap.ResumeLayout(false);
            this.tabPageSMMap.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSuperMode)).EndInit();
            this.tabPageCharacterisation.ResumeLayout(false);
            this.tabPageCharacterisation.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnStartup;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Button btnSetup;
        private System.Windows.Forms.Button btnShutdown;
        private System.Windows.Forms.Button btnStopAndShut;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuItemAboutCloseGrid;
        private System.Windows.Forms.ToolStripMenuItem menuItemAboutCGApp;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPageOverallMap;
        private System.Windows.Forms.TabPage tabPageSMMap;
        private System.Windows.Forms.ToolStripStatusLabel labelStatus;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar1;
        private System.Windows.Forms.Button btnStartOverallMap;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TabPage tabPageInstr;
        private System.Windows.Forms.Label labelPowerReading;
        private System.Windows.Forms.Button btnGetReadings;
        private System.Windows.Forms.Label labelFreqReading;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelTemperature;
        private System.Windows.Forms.Button btnSetSections;
        private System.Windows.Forms.Button btnGetSections;
        private Bookham.Toolkit.CloseGrid.DSDBRSectionsCtrl dsdbrSectionsCtrl;
        private System.Windows.Forms.Button btnLoadOverallMap;
        private System.Windows.Forms.Button btnGetOverallMapResults;
        private Bookham.Toolkit.CloseGrid.OverallMapResultsCtrl overallMapResultsCtrl1;
        private Bookham.Toolkit.CloseGrid.CloseGridTestInfoCtrl closeGridTestInfoCtrl1;
        private Bookham.Toolkit.CloseGrid.MapViewCtrl mapViewOverall;
        private System.Windows.Forms.Button btnLoadSMMap;
        private System.Windows.Forms.Button btnGetSMResults;
        private System.Windows.Forms.Button btnDoSMMap;
        private Bookham.Toolkit.CloseGrid.UserControls.SMMapResultsCtrl smMapResultsCtrl1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numSuperMode;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btnGetScreeningResults;
        private Bookham.Toolkit.CloseGrid.MapViewCtrl mapViewCtrl1;
        private Bookham.Toolkit.CloseGrid.MapViewCtrl mapViewSM;
        private System.Windows.Forms.TabPage tabPageCharacterisation;
        private Bookham.Toolkit.CloseGrid.UserControls.CollatedPassFailResultsCtrl collatedPassFailResultsCtrl1;
        private System.Windows.Forms.Button btnCollatePFResults;
        private System.Windows.Forms.Button btnGetCharResults;
        private System.Windows.Forms.Button btnStartLaserChar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label labelNbrITUDuff;
        private System.Windows.Forms.Label labelNbrITUFound;
        private System.Windows.Forms.Label labelNbrITUEst;
        private System.Windows.Forms.Button btnGetITUEstimates;
        private System.Windows.Forms.TextBox textBoxSoaActuatorAddress;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox checkBoxSimulation;
        private System.Windows.Forms.Button btnSoaCompSet;
        private System.Windows.Forms.Button btnSoaCompGet;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown numSoaCompliance;
    }
}


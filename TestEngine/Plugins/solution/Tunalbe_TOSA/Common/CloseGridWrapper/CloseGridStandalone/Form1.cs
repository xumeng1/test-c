using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Bookham.Toolkit.CloseGrid;
using System.Threading;
using Bookham.TestLibrary.ChassisNS;
using Bookham.TestLibrary.Instruments;
using Bookham.TestEngine.Equipment;
using Bookham.TestLibrary.Utilities;
using Bookham.TestSolution.TcmzCommonData;

namespace CloseGridStandalone
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            closeGridWrapper = CloseGridWrapper.Singleton;
            bool simMode = closeGridWrapper.SimulationMode;
            this.checkBoxSimulation.Checked = simMode;
            updateStatus();

            
        }

        private CloseGridWrapper closeGridWrapper;

        private Inst_DigiIoComboSwitch soaRelays;

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            // close down the closeGridWrapper!
            closeGridWrapper.Dispose();
        }

        private void updateStatus()
        {
            CloseGridState state = closeGridWrapper.Status();
            string stateStr = state.ToString();
            if (stateStr != labelStatus.Text)
            {
                labelStatus.Text = stateStr;
                defaultCtrlState();
                if (state == CloseGridState.Instantiated)
                {
                    btnStartup.Enabled = true;
                }
                else if (state == CloseGridState.Startup)
                {
                    btnSetup.Enabled = true;
                }
                else if ((state == CloseGridState.Setup) || (state == CloseGridState.Complete))
                {
                    btnGetReadings.Enabled = true;
                    btnStartOverallMap.Enabled = true;
                    dsdbrSectionsCtrl.Enabled = true;
                    btnGetSections.Enabled = true;
                    btnSetSections.Enabled = true;
                }
                else if (state == CloseGridState.Complete)
                {
                    btnSetup.Enabled = true;
                }
                else if (state == CloseGridState.Busy)
                {
                    timer1.Enabled = true;
                }
            }            
        }

        private void defaultCtrlState()
        {
            btnStartup.Enabled = false;
            btnSetup.Enabled = false;
            btnGetReadings.Enabled = false;
            dsdbrSectionsCtrl.Enabled = false;
            btnGetSections.Enabled = false;
            btnSetSections.Enabled = false;
            btnStartOverallMap.Enabled = false;
            btnStopAndShut.Enabled = true;
            btnShutdown.Enabled = true;
            closeGridTestInfoCtrl1.Enabled = true;
            timer1.Enabled = true;
            toolStripProgressBar1.Value = 0;
        }

        private void disableCtrls()
        {
            btnStartup.Enabled = false;
            btnSetup.Enabled = false;
            btnGetReadings.Enabled = false;
            dsdbrSectionsCtrl.Enabled = false; 
            btnGetSections.Enabled = false;
            btnSetSections.Enabled = false;
            btnStartOverallMap.Enabled = false;
            btnStopAndShut.Enabled = false;
            btnShutdown.Enabled = false;
            closeGridTestInfoCtrl1.Enabled = false;
            timer1.Enabled = false;
        }

        private void updateWorking()
        {
            labelStatus.Text = "...";
            disableCtrls();
        }

        private void btnStartup_Click(object sender, EventArgs e)
        {
            updateWorking();

            // initialise test engine logging
            Bookham.TestEngine.Framework.Logging.Initializer.Init();
            string soaActuatorVisaResource = textBoxSoaActuatorAddress.Text;

            // create the Actuator chassis - hardwire the PXIT variant
            //Chassis_PXIT105 soaActuatorCard =
            //    new Chassis_PXIT105("SoaRelayCard",
            //    "Chassis_PXIT105", soaActuatorVisaResource);

            // create relays
            //Inst_PXIT105 pxit105 =
            //    new Inst_PXIT105("PXIT105", "Inst_PXIT105", "", "",
            //    soaActuatorCard);

            if (!checkBoxSimulation.Checked)
            {
                //soaActuatorCard.IsOnline = true;
                //pxit105.IsOnline = true;
            }            

            InstrumentCollection instrCollection = new InstrumentCollection();
            //instrCollection.Add(pxit105);

            //this.soaRelays = new Inst_DigiIoComboSwitch("SoaRelaySwitch", @"..\..\soaRelayConfig.xml", instrCollection);

            backgroundWorker1.RunWorkerAsync();                      
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                errFromStartup = null;
                // initialise close grid wrapper
                closeGridWrapper.Startup(soaRelays);
            }
            catch (Exception ex)
            {
                errFromStartup = ex.ToString();                
            }
        }

        private string errFromStartup;

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (errFromStartup != null)
            {
                MessageBox.Show(errFromStartup, "Exception!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            updateStatus();
        }

        private void menuItemAboutCloseGrid_Click(object sender, EventArgs e)
        {
            string ver = closeGridWrapper.Version.ToString();
            MessageBox.Show(ver, "Close Grid Info");
        }

        private void menuItemAboutCGApp_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Close Grid Standalone application. (c) Bookham Inc 2006");
        }

        private void btnSetup_Click(object sender, EventArgs e)
        {            
            CloseGridTestInfo testInfo = closeGridWrapper.Setup(closeGridTestInfoCtrl1.LaserType, 
                closeGridTestInfoCtrl1.LaserId);
            closeGridTestInfoCtrl1.Value = testInfo;
            updateStatus();
        }

        private void btnShutdown_Click(object sender, EventArgs e)
        {
            closeGridWrapper.Shutdown();
            updateStatus();
        }

        private void btnStopAndShut_Click(object sender, EventArgs e)
        {
            closeGridWrapper.StopAndShutdown();
            updateStatus();
        }

        private void btnStartOverallMap_Click(object sender, EventArgs e)
        {
            closeGridWrapper.StartLaserScreeningOverallMap();
            updateStatus();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            updateStatus();
            if (labelStatus.Text == "Busy")
            {
                short pcComplete = closeGridWrapper.GetPercentComplete();
                toolStripProgressBar1.Value = pcComplete;
            }
        }
        
        private void btnGetReadings_Click(object sender, EventArgs e)
        {
            double pow = closeGridWrapper.ReadPower_mW(PowerMeterHead.Reference);
            labelPowerReading.Text = pow.ToString("0.000e+00");
            double temp = closeGridWrapper.ReadTECTemp_degC();
            labelTemperature.Text = temp.ToString("00.00");
            double freq = closeGridWrapper.ReadFrequency_GHz();
            labelFreqReading.Text = freq.ToString("00.00");            
        }

        private void btnGetSections_Click(object sender, EventArgs e)
        {
            DSDBRSectionCurrents d = closeGridWrapper.GetCurrents();
            dsdbrSectionsCtrl.Value = d;
        }

        private void btnSetSections_Click(object sender, EventArgs e)
        {
            DSDBRSectionCurrents d = dsdbrSectionsCtrl.Value;
            closeGridWrapper.SetCurrents(d);

            // temp


            do
            {
                double m1 = closeGridWrapper.ReadLockerCurrent_mA(LockerCurrentHead.Transmit);
                double m2 = closeGridWrapper.ReadLockerCurrent_mA(LockerCurrentHead.Reflect);
                System.Threading.Thread.Sleep(100);
            } while (false);
        }

        private void btnGetScreeningResults_Click(object sender, EventArgs e)
        {
            OverallMapResults mapRes = closeGridWrapper.GetScreeningOverallMapResults();
            overallMapResultsCtrl1.Value = mapRes;
            double[,] map = MapLoader.LoadMap(mapRes.CombinedDFSMapFile);
            mapViewOverall.SetMap(map, "Overall Map", "Section1", "Section2");
        }

        private void btnLoadOverallMap_Click(object sender, EventArgs e)
        {
            CloseGridTestInfo testInfo = closeGridTestInfoCtrl1.Value;
            OverallRampDir rampDir = overallMapResultsCtrl1.RampDir;
            closeGridWrapper.LoadOverallMap(testInfo, rampDir);
            
            // load the data back
            btnGetScreeningResults_Click(sender, e);
        }       

        private void btnDoSMMap_Click(object sender, EventArgs e)
        {
            short supermode = (short)numSuperMode.Value;
            closeGridWrapper.StartLaserScreeningSMMap(supermode);
            updateStatus();
        }

        private void btnGetSMResults_Click(object sender, EventArgs e)
        {
            short supermode = (short)numSuperMode.Value;
            SMMapResults mapRes = closeGridWrapper.GetScreeningSMMapResults(supermode);
            smMapResultsCtrl1.Value = mapRes;
            double[,] map = MapLoader.LoadMap(mapRes.MatrixPowerRatioForwardFile);
            mapViewSM.SetMap(map, "SM Map #" + supermode, "Section1", "Section2");
        }        

        private void btnLoadSMMap_Click(object sender, EventArgs e)
        {
            short supermode = (short)numSuperMode.Value;
            CloseGridTestInfo testInfo = closeGridTestInfoCtrl1.Value;
            SupermodeRampDir rampDir = smMapResultsCtrl1.RampDir;            
            closeGridWrapper.LoadSMMap(supermode, testInfo, rampDir);

            // load the data back
            btnGetSMResults_Click(sender, e);
        }        

        private void btnCollatePFResults_Click(object sender, EventArgs e)
        {
            CollatedPassFailResults pfRes = closeGridWrapper.CollatePassFailResults();
            collatedPassFailResultsCtrl1.Value = pfRes;
        }

        private void btnGetITUEstimates_Click(object sender, EventArgs e)
        {
            EstimateITUPointsResults res = closeGridWrapper.EstimateITUPoints();
            labelNbrITUEst.Text = res.NbrITUEstimates.ToString();
        }

        private void btnStartLaserChar_Click(object sender, EventArgs e)
        {
            closeGridWrapper.StartLaserCharacterisation();
            updateStatus();
        }

        private void btnGetCharResults_Click(object sender, EventArgs e)
        {
            GetCharResults res = closeGridWrapper.GetCharacterisationResults();
            labelNbrITUFound.Text = res.NbrITUFound.ToString();
            labelNbrITUDuff.Text = res.NbrITUDuff.ToString();
        }

        private void btnSoaCompGet_Click(object sender, EventArgs e)
        {
            double v = closeGridWrapper.GetComplianceVoltage_V(DSDBRSection.SOA);
            numSoaCompliance.Value = (decimal) v;
        }

        private void btnSoaCompSet_Click(object sender, EventArgs e)
        {
            double v = (double) numSoaCompliance.Value;
            closeGridWrapper.SetComplianceVoltage_V(DSDBRSection.SOA, v);
        }        


    }
}
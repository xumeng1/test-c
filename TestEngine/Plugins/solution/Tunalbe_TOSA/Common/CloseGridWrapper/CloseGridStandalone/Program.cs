using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Threading;

namespace CloseGridStandalone
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {            
            Application.ThreadException += // Windows Forms 
                new ThreadExceptionEventHandler(
                OnGuiUnhandledException);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
            Environment.Exit(0);
            
        }        

        static void OnGuiUnhandledException(object obj, ThreadExceptionEventArgs args)
        {
            string errMsg = args.Exception.Message;
            MessageBox.Show(errMsg, "Exception!", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

    }
}
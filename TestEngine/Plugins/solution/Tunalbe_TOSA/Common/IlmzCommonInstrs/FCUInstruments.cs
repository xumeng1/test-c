using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.Instruments;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestSolution.IlmzCommonData;

namespace Bookham.TestSolution.IlmzCommonInstrs
{
    /// <summary>
    /// Common DSDBR control instruments
    /// </summary>
    public class FCUInstruments
    {
        /// <summary>
        /// Transmission Locker and DSDBR current sources for each laser section except SOA
        /// </summary>
        public Instr_FCU FullbandControlUnit;

        /// <summary>
        /// Current source for SOA
        /// </summary>
        public InstType_ElectricalSource SoaCurrentSource;

        /// <summary>
        /// Voltage source for FCU
        /// </summary>
        //public InstType_ElectricalSource FCU_Source;

        /// <summary>
        /// DSDBR current sources for each laser section
        /// </summary>
        public Dictionary<DSDBRSection, InstType_ElectricalSource> CurrentSources;
        /// <summary>
        /// FCU calibration data
        /// </summary>
        public FCUCalData FCUCalibrationData;

        /// <summary> Structure for FCU calibration data </summary>
        public struct FCUCalData
        {
            /// <summary> Cal offset for TxADC </summary>
            public double TxADC_CalOffset;
            /// <summary> Cal factor for TxADC </summary>
            public double TxADC_CalFactor;

            /// <summary> Cal offset for RxADC </summary>
            public double RxADC_CalOffset;
            /// <summary> Cal factor for RxADC </summary>
            public double RxADC_CalFactor;

            /// <summary> Cal offset for TxCoarsePot </summary>
            public double TxCoarsePot_CalOffset;
            /// <summary> Cal factor for TxCoarsePot </summary>
            public double TxCoarsePot_CalFactor;

            /// <summary> Cal offset for TxFinePot </summary>
            public double TxFinePot_CalOffset;
            /// <summary> Cal factor for TxFinePot </summary>
            public double TxFinePot_CalFactor;

            /// <summary> Cal offset for ThermistorResistance </summary>
            public double ThermistorResistance_CalOffset;
            /// <summary> Cal factor for ThermistorResistance </summary>
            public double ThermistorResistance_CalFactor;
        }
    }
}

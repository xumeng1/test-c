using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestSolution.IlmzCommonData;

namespace Bookham.TestSolution.IlmzCommonInstrs
{
    /// <summary>
    /// Common DSDBR control instruments
    /// </summary>
    public class DsdbrInstruments
    {
        /// <summary>
        /// DSDBR current sources for each laser section
        /// </summary>
        public Dictionary<DSDBRSection, InstType_ElectricalSource> CurrentSources;

        /// <summary>Transmission Locker photodiode + bias</summary>
        public IInstType_ElectricalSource LockerTx;
        /// <summary>Reflection Locker photodiode + bias</summary>
        public IInstType_ElectricalSource LockerRx;
    }
}

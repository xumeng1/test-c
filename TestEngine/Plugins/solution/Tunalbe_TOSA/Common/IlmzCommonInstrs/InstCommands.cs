﻿// Copyright (C), 1988-1999, Oclaro. Co., Ltd.
// Bookham.TestSolution.IlmzCommonInstrs
// InstCommands.cs
// Author: chongjian.liang 2013.1.19

using System;
using System.Threading;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.Instruments;

namespace Bookham.TestSolution.IlmzCommonInstrs
{
    /// <summary>
    /// Common instrument commands
    /// </summary>
    public class InstCommands
    {
        /// <summary>
        /// Close FcuSource, VccSource, VeeSource.
        /// Close TecDsdbr.
        /// </summary>
        public static void CloseInstToLoadDUT(ITestEngineBase engine, IInstType_ElectricalSource fcuSource, IInstType_ElectricalSource vccSource, IInstType_ElectricalSource veeSource,
            IInstType_SimpleTempControl tecDsdbr)
        {
            if (fcuSource != null)
            {
                fcuSource.OutputEnabled = false;
            }

            if (vccSource != null)
            {
                vccSource.OutputEnabled = false;
            }

            if (veeSource != null)
            {
                veeSource.OutputEnabled = false;
            }

            if (tecDsdbr != null)
            {
                tecDsdbr.OutputEnabled = false;
            }
        }


        /// <summary>
        /// Close FcuSource, VccSource, VeeSource.
        /// Close TecDsdbr.
        /// </summary>
        public static void OpenInstToLoadDUT(ITestEngineBase engine, IInstType_ElectricalSource fcuSource, IInstType_ElectricalSource vccSource, IInstType_ElectricalSource veeSource,
            IInstType_SimpleTempControl tecDsdbr)
        {
            if (fcuSource != null)
            {
                fcuSource.OutputEnabled = true;
            }

            if (vccSource != null)
            {
                vccSource.OutputEnabled = true;
            }

            if (veeSource != null)
            {
                veeSource.OutputEnabled = true;
            }

            if (tecDsdbr != null)
            {
                tecDsdbr.OutputEnabled = false;
            }
        }

        /// <summary>
        /// Close Asic current, FcuSource, VccSource, VeeSource.
        /// Close TecDsdbr, set TecCase to safe temperature, close TecCase.
        /// </summary>
        public static void CloseInstToUnLoadDUT(ITestEngineBase engine, Inst_Fcu2Asic fcu, 
            IInstType_ElectricalSource fcuSource, IInstType_ElectricalSource vccSource, IInstType_ElectricalSource veeSource,
            IInstType_SimpleTempControl tecDsdbr, IInstType_SimpleTempControl tecCase, double safeTemperature_C)
        {
            CloseFcu(engine, fcu);
            ClosePowerSources(engine, fcuSource, vccSource, veeSource);
            CloseTecsWithSafeTemperature(engine, tecDsdbr, tecCase, safeTemperature_C);
        }

        /// <summary>
        /// Close Asic and non Asic currents privided by Fcu
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="fcu"></param>
        public static void CloseFcu(ITestEngineBase engine, Inst_Fcu2Asic fcu)
        {
            if (fcu != null && fcu.IsOnline)
            {
                CommandInstrument(delegate
                {
                    // Close Asic currents privided by Fcu
                    fcu.CloseAllAsic();

                    // Close Non Asic currents privided by Fcu
                    fcu.VLeftModBias_Volt = 0;
                    fcu.VRightModBias_Volt = 0;
                }, "FCUMKI", CommandType.Close, engine, false);
            }
        }

        /// <summary>
        /// Close Asic current, FcuSource, VccSource, VeeSource.
        /// </summary>
        public static void ClosePowerSources(ITestEngineBase engine, IInstType_ElectricalSource fcuSource, IInstType_ElectricalSource vccSource, IInstType_ElectricalSource veeSource)
        {
            if (fcuSource != null)
            {
                if (fcuSource.IsOnline)
                {
                    CommandInstrument(delegate
                    {
                        fcuSource.OutputEnabled = false;
                    }, "FcuSource", CommandType.Close, engine, true);
                }
                else
                {
                    engine.ShowContinueUserQuery("Warning: Close Fcu source failed! After test ended, please ask the technician to close this instrument manually before take away the DUT.\n\n注意: 无法关闭Fcu电源. 测试完毕后, 请叫技术员手动关掉这个设备后, 再取出器件.");
                }
            }

            if (vccSource != null)
            {
                if (vccSource.IsOnline)
                {
                    CommandInstrument(delegate
                    {
                        vccSource.OutputEnabled = false;
                    }, "VccSource", CommandType.Close, engine, true);

                }
                else
                {
                    engine.ShowContinueUserQuery("Warning: Close Vcc source failed! After test ended, please ask the technician to close this instrument manually before take away the DUT.\n\n注意: 无法关闭Vcc电源. 测试完毕后, 请叫技术员手动关掉这个设备后, 再取出器件.");
                }
            }

            if (veeSource != null)
            {
                if (veeSource.IsOnline)
                {
                    CommandInstrument(delegate
                    {
                        veeSource.OutputEnabled = false;
                    }, "VeeSource", CommandType.Close, engine, true);
                }
                else
                {
                    engine.ShowContinueUserQuery("Warning: Close Vee source failed! After test ended, please ask the technician to close this instrument manually before take away the DUT.\n\n注意: 无法关闭Vee电源. 测试完毕后, 请叫技术员手动关掉这个设备后, 再取出器件.");
                }
            }
        }

        /// <summary>
        /// Close TecDsdbr, set TecCase to safe temperature, close TecCase.
        /// </summary>
        public static void CloseTecsWithSafeTemperature(ITestEngineBase engine, IInstType_SimpleTempControl tecDsdbr, IInstType_SimpleTempControl tecCase, double safeTemperature_C)
        {
            if (tecDsdbr != null)
            {
                if (tecDsdbr.IsOnline)
                {
                    CommandInstrument(delegate
                    {
                        tecDsdbr.OutputEnabled = false;
                    }, "TecDsdbr", CommandType.Close, engine, false);
                }
                else
                {
                    engine.ShowContinueUserQuery("Failed to close TecDsdbr!\n\n无法关闭器件控温设备!");
                }
            }

            if (tecCase != null)
            {
                if (tecCase.IsOnline)
                {
                    SetTecCaseToSafeTemperature(engine, tecCase, safeTemperature_C);

                    CommandInstrument(delegate
                    {
                        tecCase.OutputEnabled = false;
                    }, "TecCase", CommandType.Close, engine, false);
                }
                else
                {
                    engine.ShowContinueUserQuery("Failed to close TecCase!\n\n无法关闭夹具控温设备!");
                }
            }
        }

        /// <summary>
        /// Set and wait for the case temperature to reach a safe temperature
        /// </summary>
        private static void SetTecCaseToSafeTemperature(ITestEngineBase engine, IInstType_SimpleTempControl tecCase, double safeTemperature_C)
        {
            CommandInstrument(delegate
            {
                tecCase.SensorTemperatureSetPoint_C = safeTemperature_C;
            }, "TecCase", CommandType.Set, engine, false);

            double deltaTemperature_C = double.MaxValue;

            int count = 0;

            do
            {
                if (count++ > 360)
                {
                    engine.ShowContinueUserQuery("Case temperature control failed, after test ended, do not take away the DUT before temperature read from TecCase is too high or low.\n\n夹具控温失败. 测试完毕后, 留意夹具控温设备的温度, 请不要在温度过高或过低的情况下取出器件.");

                    break;
                }

                double currentTemperature_C = tecCase.SensorTemperatureActual_C;

                engine.SendToGui("Case temperature = " + String.Format("{0:f}", currentTemperature_C));

                deltaTemperature_C = Math.Abs(safeTemperature_C - currentTemperature_C);

                System.Threading.Thread.Sleep(500);

            } while (deltaTemperature_C > 10);
        }

        /// <summary>
        /// Set or close an instrument
        /// </summary>
        private static void CommandInstrument(Action<object> commands, string instrumentName, CommandType commandType, ITestEngineBase engine, bool isPowerSource)
        {
            switch (commandType)
            {
                case CommandType.Open:
                    engine.SendToGui("Opening " + instrumentName + "...");
                    break;
                case CommandType.Close:
                    engine.SendToGui("Closing " + instrumentName + "...");
                    break;
                case CommandType.Set:
                    engine.SendToGui("Setting " + instrumentName + " value.");
                    break;
            }

            bool commandSucceeded = false;

            Thread commandThread = new Thread(new ThreadStart(delegate
            {
                commands(null);

                commandSucceeded = true;
            }));

            commandThread.IsBackground = true;
            commandThread.Start();

            int timeCount = 0;

            while (timeCount++ < 20)
            {
                if (commandSucceeded)
                {
                    break;
                }
                else
                {
                    Thread.Sleep(500);
                }
            }

            if (commandSucceeded)
            {
                switch (commandType)
                {
                    case CommandType.Open:
                        engine.SendToGui("Open " + instrumentName + " succeeded!");
                        break;
                    case CommandType.Close:
                        engine.SendToGui("Close " + instrumentName + " succeeded!");
                        break;
                    case CommandType.Set:
                        engine.SendToGui("Set " + instrumentName + " value succeeded!");
                        break;
                }
            }
            else
            {
                switch (commandType)
                {
                    case CommandType.Open:
                        {
                            engine.SendToGui("Open " + instrumentName + " failed!");

                            engine.ShowContinueUserQuery("Failed to open " + instrumentName + " !\n\n无法打开" + instrumentName + "!");
                        }
                        break;
                    case CommandType.Close:
                        {
                            engine.SendToGui("Close " + instrumentName + " failed!");

                            if (isPowerSource)
                            {
                                engine.ShowContinueUserQuery("Warning: Close " + instrumentName + " failed! After test ended, please ask the technician to close this instrument manually before take away the DUT.\n\n注意: 无法关闭" + instrumentName + ". 测试完毕后, 请叫技术员手动关掉这个设备后, 再取出器件.");
                            }
                            else
                            {
                                engine.ShowContinueUserQuery("Failed to close " + instrumentName + " !\n\n无法关闭" + instrumentName + "!");
                            }
                        }
                        break;
                    case CommandType.Set:
                        {
                            engine.SendToGui("Set value to " + instrumentName + " failed!");

                            engine.ShowContinueUserQuery("Failed to set value to " + instrumentName + "!\n\n无法改变" + instrumentName + "的值!");
                        }
                        break;
                }

                engine.ShowContinueUserQuery("After test completed, please restart the test software(TestEngine).\n\n测试完毕后,请重新启动测试软件(TestEngine).");
            }
        }

        /// <summary>
        /// CommandType
        /// </summary>
        private enum CommandType
        {
            Open, Close, Set
        }
    }
}

// alice.Huang 
// created on 2010-01-27
// for TOSA GB test with DSDBR Source prided by ASIC and control by FCU


using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.Instruments;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestSolution.IlmzCommonData;

namespace Bookham.TestSolution.IlmzCommonInstrs
{
    /// <summary>
    /// Transmission Locker and DSDBR current sources for each laser section 
    /// and IMBs sources in MZ
    /// </summary>
    public class FCU2AsicInstruments
    {
        /// <summary>
        /// asic controled by FCU
        /// </summary>
        
        public Inst_Fcu2Asic  Fcu2Asic;

        
        /// <summary>
        /// Vcc source for Asic
        /// </summary>
        public InstType_ElectricalSource AsicVccSource;
        /// <summary>
        /// Vee source for Asic
        /// </summary>
        public InstType_ElectricalSource AsicVeeSource;

        /// <summary>
        /// FCU Source
        /// </summary>
        public InstType_ElectricalSource FcuSource;

        /// <summary>
        /// FCU calibration data
        /// </summary>

        public Inst_Fcu2Asic.FCUCalData FCUCalibrationData;

        /*/// <summary> Structure for FCU calibration data </summary>
        public struct FCUCalData
        {
            /// <summary> Cal offset for TxADC </summary>
            public double TxADC_CalOffset;
            /// <summary> Cal factor for TxADC </summary>
            public double TxADC_CalFactor;

            /// <summary> Cal offset for RxADC </summary>
            public double RxADC_CalOffset;
            /// <summary> Cal factor for RxADC </summary>
            public double RxADC_CalFactor;

            /// <summary> Cal offset for TxCoarsePot </summary>
            public double TxCoarsePot_CalOffset;
            /// <summary> Cal factor for TxCoarsePot </summary>
            public double TxCoarsePot_CalFactor;

            /// <summary> Cal offset for TxCoarsePot </summary>
            public double RxCoarsePot_CalOffset;
            /// <summary> Cal factor for TxCoarsePot </summary>
            public double RxCoarsePot_CalFactor;

            /// <summary> Cal offset for TxFinePot </summary>
            public double TxFinePot_CalOffset;
            /// <summary> Cal factor for TxFinePot </summary>
            public double TxFinePot_CalFactor;

            /// <summary> Cal offset for FixPot </summary>
            public double FixADC_CalOffset;
            /// <summary> Cal factor for FixPot </summary>
            public double FixADC_CalFactor;

            /// <summary> Cal offset for ThermistorResistance </summary>
            public double ThermistorResistance_CalOffset;
            /// <summary> Cal factor for ThermistorResistance </summary>
            public double ThermistorResistance_CalFactor;
        }
          */
    }
}

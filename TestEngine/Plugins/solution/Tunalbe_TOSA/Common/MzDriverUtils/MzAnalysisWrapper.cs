using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.Algorithms;
using Bookham.TestSolution.ILMZFinalData;
using Bookham.TestLibrary.Algorithms.DoubleSlope;
using System.IO;
namespace Bookham.ToolKit.Mz
{
    /// <summary>
    /// Utility class to simplify calls to analyse MZ data
    /// </summary>
    public sealed class MzAnalysisWrapper
    {
        /// <summary>
        /// Private constructor 
        /// </summary>
        private MzAnalysisWrapper() { }

        /// <summary>
        /// Calculates ER, VPI and VQuad from a zero chirp MZ characteristic.
        /// </summary>
        /// <remarks>
        /// This method joins data from two single ended sweeps before analysis.
        /// </remarks>
        /// <param name="leftArmSweep">Left arm sweep data</param>
        /// <param name="rightArmSweep">Right arm sweep data</param>
        /// <param name="fixedLeftSrc">Left arm fixed level</param>
        /// <param name="fixedRightSrc">Right arm fixed level</param>
        /// <returns>A structure containing all of the analysis data results</returns>
        public static MzAnalysisResults ZeroChirp_SingleEndedSweeps(ILMZSweepResult leftArmSweep,
            ILMZSweepResult rightArmSweep, double fixedLeftSrc, double fixedRightSrc)
        {
            // check the sweeps are sensible!
            if ((leftArmSweep.SrcMeter != SourceMeter.LeftModArm) ||
                (leftArmSweep.Type != SweepType.SingleEndVoltage) ||
                (rightArmSweep.SrcMeter != SourceMeter.RightModArm) ||
                (rightArmSweep.Type != SweepType.SingleEndVoltage))
            {
                string errMsg = string.Format("Invalid sweep data for Zero Chirp Analysis\n" +
                    "L:{0}-{1}\n" +
                    "R:{2}-{3}",
                    leftArmSweep.SrcMeter, leftArmSweep.Type,
                    rightArmSweep.SrcMeter, rightArmSweep.Type);
                throw new ArgumentException(errMsg);
            }
            // extract the sweep data
            double[] leftArmSrc = leftArmSweep.SweepData[ILMZSweepDataType.LeftArmModBias_V];
            double[] leftArmPower = leftArmSweep.SweepData[ILMZSweepDataType.FibrePower_mW];
            double[] rightArmSrc = rightArmSweep.SweepData[ILMZSweepDataType.RightArmModBias_V];
            double[] rightArmPower = rightArmSweep.SweepData[ILMZSweepDataType.FibrePower_mW];
            // sanity check fixed source levels to 2 sig figs to avoid major problems
            double fixedLeftSrcCheck = rightArmSweep.SweepData[ILMZSweepDataType.LeftArmModBias_V][0];
            if (Math.Abs(fixedLeftSrc - fixedLeftSrcCheck) > 0.1)
            {
                string errMsg = string.Format("Left fixed source doesn't match data: Specified={0}, From Sweep={1}",
                    fixedLeftSrc, fixedLeftSrcCheck);
                throw new ArgumentException(errMsg);
            }
            double fixedRightSrcCheck = leftArmSweep.SweepData[ILMZSweepDataType.RightArmModBias_V][0];
            if (Math.Abs(fixedRightSrc - fixedRightSrcCheck) > 0.1)
            {
                string errMsg = string.Format("Right fixed source doesn't match data: Specified={0}, From Sweep={1}",
                    fixedRightSrc, fixedRightSrcCheck);
                throw new ArgumentException(errMsg);
            }

            // get the fixed bias levels from the opposite arm of the sweep
            // basic MZ sweep analysis
            Alg_MZAnalysis.MZAnalysis mzAnlyRaw =
                Alg_MZAnalysis.ZeroChirpAnalysis(leftArmSrc, rightArmSrc, leftArmPower,
                    rightArmPower, false, false, 0);

            MzAnalysisResults mzAnly = new MzAnalysisResults(mzAnlyRaw);

            // find the appropriate left and right arm source values
            // MIN
            CalcSingleEndedArmValues(mzAnlyRaw.VoltageAtMin, fixedLeftSrc, fixedRightSrc,
                out mzAnly.Min_SrcL, out mzAnly.Min_SrcR);
            // MAX
            CalcSingleEndedArmValues(mzAnlyRaw.VoltageAtMax, fixedLeftSrc, fixedRightSrc,
                out mzAnly.Max_SrcL, out mzAnly.Max_SrcR);
            // IMB
            CalcSingleEndedArmValues(mzAnlyRaw.VImb, fixedLeftSrc, fixedRightSrc,
                out mzAnly.Imb_SrcL, out mzAnly.Imb_SrcR);
            // QUAD
            CalcSingleEndedArmValues(mzAnlyRaw.VQuad, fixedLeftSrc, fixedRightSrc,
                out mzAnly.Quad_SrcL, out mzAnly.Quad_SrcR);

            return mzAnly;
        }

        /// <summary>
        /// Calculates ER, VPI and VQuad from a negative chirp MZ characteristic.
        /// </summary>
        /// <remarks>
        /// This method joins data from two single ended sweeps before analysis.
        /// </remarks>
        /// <param name="leftArmSweep">Left arm sweep data</param>
        /// <param name="rightArmSweep">Right arm sweep data</param>
        /// <param name="fixedLeftSrc">Left arm fixed level</param>
        /// <param name="fixedRightSrc">Right arm fixed level</param>
        /// <returns>A structure containing all of the analysis data results</returns>
        public static MzAnalysisResults Differential_NegativeChirp_SingleEndedSweeps(ILMZSweepResult leftArmSweep,
            ILMZSweepResult rightArmSweep, double fixedLeftSrc, double fixedRightSrc)
        {
            // check the sweeps are sensible!
            if ((leftArmSweep.SrcMeter != SourceMeter.LeftModArm) ||
                (leftArmSweep.Type != SweepType.SingleEndVoltage) ||
                (rightArmSweep.SrcMeter != SourceMeter.RightModArm) ||
                (rightArmSweep.Type != SweepType.SingleEndVoltage))
            {
                string errMsg = string.Format("Invalid sweep data for Zero Chirp Analysis\n" +
                    "L:{0}-{1}\n" +
                    "R:{2}-{3}",
                    leftArmSweep.SrcMeter, leftArmSweep.Type,
                    rightArmSweep.SrcMeter, rightArmSweep.Type);
                throw new ArgumentException(errMsg);
            }
            // extract the sweeps
            double[] leftArmSrc = leftArmSweep.SweepData[ILMZSweepDataType.LeftArmModBias_V];
            double[] leftArmPower = leftArmSweep.SweepData[ILMZSweepDataType.FibrePower_mW];
            double[] rightArmSrc = rightArmSweep.SweepData[ILMZSweepDataType.RightArmModBias_V];
            double[] rightArmPower = rightArmSweep.SweepData[ILMZSweepDataType.FibrePower_mW];
            // sanity check fixed source levels to 2 sig figs to avoid major problems
            double fixedLeftSrcCheck = rightArmSweep.SweepData[ILMZSweepDataType.LeftArmModBias_V][0];
            if (Math.Abs(fixedLeftSrc - fixedLeftSrcCheck) > 0.1)
            {
                string errMsg = string.Format("Left fixed source doesn't match data: Specified={0}, From Sweep={1}",
                    fixedLeftSrc, fixedLeftSrcCheck);
                throw new ArgumentException(errMsg);
            }
            double fixedRightSrcCheck = leftArmSweep.SweepData[ILMZSweepDataType.RightArmModBias_V][0];
            if (Math.Abs(fixedRightSrc - fixedRightSrcCheck) > 0.1)
            {
                string errMsg = string.Format("Right fixed source doesn't match data: Specified={0}, From Sweep={1}",
                    fixedRightSrc, fixedRightSrcCheck);
                throw new ArgumentException(errMsg);
            }

            // get the fixed bias levels from the opposite arm of the sweep
            // basic MZ sweep analysis, but analyse the NEGATIVE slope
            Alg_MZAnalysis.MZAnalysis mzAnlyRaw =
                Alg_MZAnalysis.ZeroChirpAnalysis(leftArmSrc, rightArmSrc, leftArmPower,
                    rightArmPower, false, false, 0);

            MzAnalysisResults mzAnly = new MzAnalysisResults(mzAnlyRaw);

            // find the appropriate left and right arm source values
            // MIN
            CalcSingleEndedArmValues(mzAnlyRaw.VoltageAtMin, fixedLeftSrc, fixedRightSrc,
                out mzAnly.Min_SrcL, out mzAnly.Min_SrcR);
            // MAX
            CalcSingleEndedArmValues(mzAnlyRaw.VoltageAtMax, fixedLeftSrc, fixedRightSrc,
                out mzAnly.Max_SrcL, out mzAnly.Max_SrcR);
            // IMB
            CalcSingleEndedArmValues(mzAnlyRaw.VImb, fixedLeftSrc, fixedRightSrc,
                out mzAnly.Imb_SrcL, out mzAnly.Imb_SrcR);
            // QUAD
            CalcSingleEndedArmValues(mzAnlyRaw.VQuad, fixedLeftSrc, fixedRightSrc,
                out mzAnly.Quad_SrcL, out mzAnly.Quad_SrcR);

            return mzAnly;
        }

        /// <summary>
        /// Performs analysis of single ended sweep data
        /// </summary>
        /// <param name="singleArmSrc">Modulator arm data</param>
        /// <param name="singleArmPower">Optical power</param>
        /// <returns>Single ended results</returns>
        public static MzSingleEndedAnalysisResults SingleEnded_NegChirp_ModulatorSweep(double[] singleArmSrc, double[] singleArmPower)
        {
            // perform basic (zero chirp) MZ sweep analysis
            Alg_MZAnalysis.MZAnalysis mzAnlyRaw =
                Alg_MZAnalysis.ZeroChirpAnalysis(singleArmSrc, singleArmPower, false, false, 0);

            #region Try to analysis on positive slope
            if (mzAnlyRaw.ExtinctionRatio_dB.ToString() == "NaN" ||
                mzAnlyRaw.PowerAtMax_dBm.ToString() == "NaN" ||
                mzAnlyRaw.PowerAtMin_dBm.ToString() == "NaN" ||
                mzAnlyRaw.VImb.ToString() == "NaN" ||
                mzAnlyRaw.VoltageAtMax.ToString() == "NaN" ||
                mzAnlyRaw.VoltageAtMin.ToString() == "NaN" ||
                mzAnlyRaw.Vpi.ToString() == "NaN" ||
                mzAnlyRaw.VQuad.ToString() == "NaN")
            {
                mzAnlyRaw = Alg_MZAnalysis.ZeroChirpAnalysis(singleArmSrc, singleArmPower, false, true, 0);
            }
            #endregion

            // Translate zero chirp results into the appropriate neg chirp data
            MzSingleEndedAnalysisResults mzAnly = new MzSingleEndedAnalysisResults(mzAnlyRaw);
            mzAnly.Max_Src = mzAnlyRaw.VoltageAtMax;
            mzAnly.Min_Src = mzAnlyRaw.VoltageAtMin;
            // Use the point of inflection for Quadrature
            mzAnly.Quad_Src = mzAnlyRaw.VImb;
            // Neg chirp imbalance point is at MIN
            mzAnly.Imb_Src = mzAnlyRaw.VoltageAtMin;

            return mzAnly;
        }

        //--------------------------------   Echo new added static method  -------------------------------------


        /// <summary>
        /// Perform analysis of negative chirp right modulator arm single ended sweep data
        /// </summary>
        /// <param name="singleArmSrc">Modulator arm data</param>
        /// <param name="singleArmPower">Optical power</param>
        /// <returns>Single ended results</returns>
        public static MzSingleEndedAnalysisResults SingleEnded_NegChirp_RightArmModulatorSweep(double[] singleArmSrc, double[] singleArmPower, out bool isError)
        {
            isError = false;
            // Mz analysis raw results
            Alg_MZAnalysis.MZAnalysis mzAnlyRaw = new Alg_MZAnalysis.MZAnalysis();

            // Find turning points by Alg_MZLVMinMax.LVMinMax
            Alg_MZLVMinMax.LVMinMax lvMinMax = Alg_MZLVMinMax.MZLVMinMax(singleArmSrc, singleArmPower, 3, false);
            Alg_MZLVMinMax.DataPoint[] turningPointsPeak = lvMinMax.PeakData;
            double[] turningPointsPeak_Src = new double[turningPointsPeak.Length];
            for (int ii = 0; ii < turningPointsPeak.Length; ii++)
            {
                turningPointsPeak_Src[ii] = turningPointsPeak[ii].Voltage;
            }

            Alg_MZLVMinMax.DataPoint[] turningPointsValley = lvMinMax.ValleyData;
            double[] turningPointsValley_Src = new double[turningPointsValley.Length];
            for (int ii = 0; ii < turningPointsValley.Length; ii++)
            {
                turningPointsValley_Src[ii] = turningPointsValley[ii].Voltage;
            }
            // Check if can't find turning points
            if (turningPointsPeak.Length == 0)
            {
                isError = true;
                string errorFile = @"D:\FinalError\turingPointError_Peak.csv";
                if (File.Exists(errorFile))
                {
                    File.Delete(errorFile);
                }
                StreamWriter sw = new StreamWriter(errorFile);
                {
                    sw.WriteLine("singleArm Src, Power");
                    for (int i = 0; i < singleArmSrc.Length; i++)
                    {
                        sw.WriteLine(singleArmSrc[i].ToString() + "," + singleArmPower[i].ToString());
                        sw.Flush();
                    }
                }
                sw.Close();
                //throw new Exception("Can't find any peak turning point on the curve!");
                return null;
            }
            if (turningPointsValley.Length == 0)
            {
                isError = true;
                string errorFile = @"D:\FinalError\turingPointError_valley.csv";
                if (File.Exists(errorFile))
                {
                    File.Delete(errorFile);
                }
                StreamWriter sw = new StreamWriter(errorFile);
                {
                    sw.WriteLine("singleArm Src, Power");
                    for (int i = 0; i < singleArmSrc.Length; i++)
                    {
                        sw.WriteLine(singleArmSrc[i].ToString() + "," + singleArmPower[i].ToString());
                        sw.Flush();
                    }
                }
                sw.Close();

                //throw new Exception("Can't find any valley turning point on the curve!");
                return null;
            }

            // Find the minimum closest to 0
            int closestMinToZero = Alg_ArrayFunctions.FindIndexOfNearestElement(turningPointsValley_Src, 0);
            // Vmin
            mzAnlyRaw.VoltageAtMin = turningPointsValley_Src[closestMinToZero];
            // Pmin
            mzAnlyRaw.PowerAtMin_dBm = turningPointsValley[closestMinToZero].Power_dBm; // The index should be the same
            // Find the maximum closest to 0
            int closestMaxToZero = Alg_ArrayFunctions.FindIndexOfNearestElement(turningPointsPeak_Src, 0);
            // Vmax
            mzAnlyRaw.VoltageAtMax = turningPointsPeak_Src[closestMaxToZero];
            // Pmax
            mzAnlyRaw.PowerAtMax_dBm = turningPointsPeak[closestMaxToZero].Power_dBm; // The index should be the same
            // ER
            mzAnlyRaw.ExtinctionRatio_dB = mzAnlyRaw.PowerAtMax_dBm - mzAnlyRaw.PowerAtMin_dBm;
            // Vpi
            mzAnlyRaw.Vpi = Math.Abs(mzAnlyRaw.VoltageAtMax - mzAnlyRaw.VoltageAtMin);
            // Vquad
            Alg_MZLVMinMax.DataPoint QuadPoint3dB = new Alg_MZLVMinMax.DataPoint();
            QuadPoint3dB.Power_dBm = mzAnlyRaw.PowerAtMax_dBm - 3;
            int fromIndex = Math.Min(turningPointsValley[closestMinToZero].Index, turningPointsPeak[closestMaxToZero].Index);
            int toIndex = Math.Max(turningPointsValley[closestMinToZero].Index, turningPointsPeak[closestMaxToZero].Index);
            double[] subArray_Pwr = Alg_ArrayFunctions.ExtractSubArray(singleArmPower, fromIndex, toIndex);
            double[] subArray_V = Alg_ArrayFunctions.ExtractSubArray(singleArmSrc, fromIndex, toIndex);
            // False peaks at the extremes of the sweep may not have a 3dB point
            double minPwr = Alg_PointSearch.FindMinValueInArray(subArray_Pwr);
            double maxPwr = Alg_PointSearch.FindMaxValueInArray(subArray_Pwr);
            if (QuadPoint3dB.Power_dBm < minPwr || QuadPoint3dB.Power_dBm > maxPwr)
            {
                // Not found within data
                QuadPoint3dB.Voltage = Double.NaN;
            }
            else
            {
                QuadPoint3dB.Voltage = XatYAlgorithm.Calculate(subArray_V, subArray_Pwr, QuadPoint3dB.Power_dBm);
            }
            mzAnlyRaw.VQuad = QuadPoint3dB.Voltage;
            // Vimb
            Alg_FindPointOfInflection.PointOfInflection imbPoint =
                Alg_FindPointOfInflection.FindPointOfInflection(singleArmSrc, singleArmPower, fromIndex, toIndex);
            if (imbPoint.Found)
            {
                mzAnlyRaw.VImb = imbPoint.XValue;
            }
            else
            {
                mzAnlyRaw.VImb = double.NaN;
            }

            // Single ended results
            MzSingleEndedAnalysisResults mzAnly = new MzSingleEndedAnalysisResults(mzAnlyRaw);
            mzAnly.Max_Src = mzAnlyRaw.VoltageAtMax;
            mzAnly.Min_Src = mzAnlyRaw.VoltageAtMin;
            // Use the point of inflection for Quadrature
            mzAnly.Quad_Src = mzAnlyRaw.VImb;
            // Neg chirp imbalance point is at MIN
            mzAnly.Imb_Src = mzAnlyRaw.VoltageAtMin;
            return mzAnly;
        }




        //--------------------------------   End of add method    -----------------------------------------------
        /// <summary>
        /// Perform analysis of negative chirp right modulator arm single ended sweep data
        /// </summary>
        /// <param name="singleArmSrc">Modulator arm data</param>
        /// <param name="singleArmPower">Optical power</param>
        /// <returns>Single ended results</returns>
        public static MzSingleEndedAnalysisResults SingleEnded_NegChirp_RightArmModulatorSweep(double[] singleArmSrc, double[] singleArmPower)
        {
            // Mz analysis raw results
            Alg_MZAnalysis.MZAnalysis mzAnlyRaw = new Alg_MZAnalysis.MZAnalysis();

            // Find turning points by Alg_MZLVMinMax.LVMinMax
            Alg_MZLVMinMax.LVMinMax lvMinMax = Alg_MZLVMinMax.MZLVMinMax(singleArmSrc, singleArmPower, 3, false);
            Alg_MZLVMinMax.DataPoint[] turningPointsPeak = lvMinMax.PeakData;
            double[] turningPointsPeak_Src = new double[turningPointsPeak.Length];
            for (int ii = 0; ii < turningPointsPeak.Length; ii++)
            {
                turningPointsPeak_Src[ii] = turningPointsPeak[ii].Voltage;
            }

            Alg_MZLVMinMax.DataPoint[] turningPointsValley = lvMinMax.ValleyData;
            double[] turningPointsValley_Src = new double[turningPointsValley.Length];
            for (int ii = 0; ii < turningPointsValley.Length; ii++)
            {
                turningPointsValley_Src[ii] = turningPointsValley[ii].Voltage;
            }
            // Check if can't find turning points
            if (turningPointsPeak.Length == 0)
            {
                string errorFile = @"D:\FinalError\turingPointError_Peak.csv";
                if (File.Exists(errorFile))
                {
                    File.Delete(errorFile);
                }
                StreamWriter sw = new StreamWriter(errorFile);
                {
                    sw.WriteLine("singleArm Src, Power");
                    for (int i = 0; i < singleArmSrc.Length; i++)
                    {
                        sw.WriteLine(singleArmSrc[i].ToString() + "," + singleArmPower[i].ToString());
                        sw.Flush();
                    }
                }
                sw.Close();
                throw new Exception("Can't find any peak turning point on the curve!");
            }
            if (turningPointsValley.Length == 0)
            {
                string errorFile = @"D:\FinalError\turingPointError_valley.csv";
                if (File.Exists(errorFile))
                {
                    File.Delete(errorFile);
                }
                StreamWriter sw = new StreamWriter(errorFile);
                {
                    sw.WriteLine("singleArm Src, Power");
                    for (int i = 0; i < singleArmSrc.Length; i++)
                    {
                        sw.WriteLine(singleArmSrc[i].ToString() + "," + singleArmPower[i].ToString());
                        sw.Flush();
                    }
                }
                sw.Close();

                throw new Exception("Can't find any valley turning point on the curve!");
            }

            // Find the minimum closest to 0
            int closestMinToZero = Alg_ArrayFunctions.FindIndexOfNearestElement(turningPointsValley_Src, 0);
            // Vmin
            mzAnlyRaw.VoltageAtMin = turningPointsValley_Src[closestMinToZero];
            // Pmin
            mzAnlyRaw.PowerAtMin_dBm = turningPointsValley[closestMinToZero].Power_dBm; // The index should be the same
            // Find the maximum closest to 0
            int closestMaxToZero = Alg_ArrayFunctions.FindIndexOfNearestElement(turningPointsPeak_Src, 0);
            // Vmax
            mzAnlyRaw.VoltageAtMax = turningPointsPeak_Src[closestMaxToZero];
            // Pmax
            mzAnlyRaw.PowerAtMax_dBm = turningPointsPeak[closestMaxToZero].Power_dBm; // The index should be the same
            // ER
            mzAnlyRaw.ExtinctionRatio_dB = mzAnlyRaw.PowerAtMax_dBm - mzAnlyRaw.PowerAtMin_dBm;
            // Vpi
            mzAnlyRaw.Vpi = Math.Abs(mzAnlyRaw.VoltageAtMax - mzAnlyRaw.VoltageAtMin);
            // Vquad
            Alg_MZLVMinMax.DataPoint QuadPoint3dB = new Alg_MZLVMinMax.DataPoint();
            QuadPoint3dB.Power_dBm = mzAnlyRaw.PowerAtMax_dBm - 3;
            int fromIndex = Math.Min(turningPointsValley[closestMinToZero].Index, turningPointsPeak[closestMaxToZero].Index);
            int toIndex = Math.Max(turningPointsValley[closestMinToZero].Index, turningPointsPeak[closestMaxToZero].Index);
            double[] subArray_Pwr = Alg_ArrayFunctions.ExtractSubArray(singleArmPower, fromIndex, toIndex);
            double[] subArray_V = Alg_ArrayFunctions.ExtractSubArray(singleArmSrc, fromIndex, toIndex);
            // False peaks at the extremes of the sweep may not have a 3dB point
            double minPwr = Alg_PointSearch.FindMinValueInArray(subArray_Pwr);
            double maxPwr = Alg_PointSearch.FindMaxValueInArray(subArray_Pwr);
            if (QuadPoint3dB.Power_dBm < minPwr || QuadPoint3dB.Power_dBm > maxPwr)
            {
                // Not found within data
                QuadPoint3dB.Voltage = Double.NaN;
            }
            else
            {
                QuadPoint3dB.Voltage = XatYAlgorithm.Calculate(subArray_V, subArray_Pwr, QuadPoint3dB.Power_dBm);
            }
            mzAnlyRaw.VQuad = QuadPoint3dB.Voltage;
            // Vimb
            Alg_FindPointOfInflection.PointOfInflection imbPoint =
                Alg_FindPointOfInflection.FindPointOfInflection(singleArmSrc, singleArmPower, fromIndex, toIndex);
            if (imbPoint.Found)
            {
                mzAnlyRaw.VImb = imbPoint.XValue;
            }
            else
            {
                mzAnlyRaw.VImb = double.NaN;
            }

            // Single ended results
            MzSingleEndedAnalysisResults mzAnly = new MzSingleEndedAnalysisResults(mzAnlyRaw);
            mzAnly.Max_Src = mzAnlyRaw.VoltageAtMax;
            mzAnly.Min_Src = mzAnlyRaw.VoltageAtMin;
            // Use the point of inflection for Quadrature
            mzAnly.Quad_Src = mzAnlyRaw.VImb;
            // Neg chirp imbalance point is at MIN
            mzAnly.Imb_Src = mzAnlyRaw.VoltageAtMin;
            return mzAnly;
        }

        /// <summary>
        /// Analysis of differential sweep data from a Negative chirp device.
        /// Points of interest are to be found on the negative slope
        /// </summary>
        /// <param name="differentialSrc">Modulator arm data</param>
        /// <param name="differentialPower">Optical power</param>
        /// <returns>Analysis results</returns>
        public static MzSingleEndedAnalysisResults Generic_NegChirp_SingleEndedSweep(double[] differentialSrc, double[] differentialPower)
        {
            // Analyse as for zero chirp, but look on negative slope
            Alg_MZAnalysis.MZAnalysis mzAnlyRaw =
                Alg_MZAnalysis.ZeroChirpAnalysis(differentialSrc, differentialPower, false, false, 0);

            // Translate results into the appropriate neg chirp data
            MzSingleEndedAnalysisResults mzAnly = new MzSingleEndedAnalysisResults(mzAnlyRaw);
            mzAnly.Max_Src = mzAnlyRaw.VoltageAtMax;
            mzAnly.Min_Src = mzAnlyRaw.VoltageAtMin;
            mzAnly.Quad_Src = mzAnlyRaw.VQuad;
            // Differential Neg chirp imbalance point is at imbalance on neg slope
            mzAnly.Imb_Src = mzAnlyRaw.VImb;
            return mzAnly;
        }



        /// <summary>
        /// Calculates ER, VPI and VQuad from a zero chirp MZ differential sweep.
        /// </summary>
        /// <param name="sweepData">Sweep data</param>
        /// <param name="nominalSourceLevel"></param>
        /// <returns>A structure containing all of the analysis data</returns>
        public static MzAnalysisResults ZeroChirp_DifferentialSweep
            (ILMZSweepResult sweepData, double nominalSourceLevel)
        {


            return ZeroChirp_DifferentialSweep(sweepData, nominalSourceLevel, 0);
        }

        /// <summary>
        /// Calculates ER, VPI and VQuad from a zero chirp MZ differential sweep.
        /// </summary>
        /// <param name="sweepData">Sweep data</param>
        /// <param name="nominalSourceLevel">Vcm</param>
        /// <param name="modBiasOffset_V">Select slope with quadrature point closest to this offset</param>
        /// <returns>A structure containing all of the analysis data</returns>
        public static MzAnalysisResults ZeroChirp_DifferentialSweep
            (ILMZSweepResult sweepData, double nominalSourceLevel, double modBiasOffset_V)
        { 
            // generate the differential data to analyse - left arm data minus right arm data
            double[] diffDriveArrayPlus;
            double[] diffDriveArrayMinus;

            if (sweepData.Type == SweepType.DifferentialVoltage)
            {
                diffDriveArrayPlus = sweepData.SweepData[ILMZSweepDataType.LeftArmModBias_V];
                diffDriveArrayMinus = sweepData.SweepData[ILMZSweepDataType.RightArmModBias_V];
            }
            else if (sweepData.Type == SweepType.DifferentialCurrent)
            {
                diffDriveArrayPlus = sweepData.SweepData[ILMZSweepDataType.LeftArmImb_A];
                diffDriveArrayMinus = sweepData.SweepData[ILMZSweepDataType.RightArmImb_A];
            }
            else
            {
                throw new ArgumentException("Sweep type isn't differential: " + sweepData.Type);
            }
            // sanity check nominal source level to 2 sig figs to avoid major problems
            double nominalSourceLevelCalc = (diffDriveArrayMinus[0] + diffDriveArrayPlus[0]) / 2;
            if (Math.Abs(nominalSourceLevel - nominalSourceLevelCalc) > 0.1)
            {
                string errMsg = string.Format("Nominal source level doesn't match data: Specified={0}, From Sweep={1}",
                    nominalSourceLevel, nominalSourceLevelCalc);
                throw new ArgumentException(errMsg);
            }
            // subtract the arrays for analysis
            double[] diffDriveArray =
                Alg_ArrayFunctions.SubtractArrays(diffDriveArrayPlus, diffDriveArrayMinus);
            double[] powerArray = sweepData.SweepData[ILMZSweepDataType.FibrePower_mW];
            // do the analysis
            Alg_MZAnalysis.MZAnalysis mzAnlyRaw =
                Alg_MZAnalysis.ZeroChirpAnalysis(diffDriveArray, powerArray, false, true, 0);

            // find the appropriate left and right arm source values
            MzAnalysisResults mzAnly = new MzAnalysisResults(mzAnlyRaw);
            // MIN
            CalcDifferentialArmValues(mzAnlyRaw.VoltageAtMin, nominalSourceLevel,
                out mzAnly.Min_SrcL, out mzAnly.Min_SrcR);
            // MAX
            CalcDifferentialArmValues(mzAnlyRaw.VoltageAtMax, nominalSourceLevel,
                out mzAnly.Max_SrcL, out mzAnly.Max_SrcR);
            // IMB
            CalcDifferentialArmValues(mzAnlyRaw.VImb, nominalSourceLevel,
                out mzAnly.Imb_SrcL, out mzAnly.Imb_SrcR);
            // QUAD
            CalcDifferentialArmValues(mzAnlyRaw.VQuad, nominalSourceLevel,
                out mzAnly.Quad_SrcL, out mzAnly.Quad_SrcR);

            return mzAnly;
        }

        /// <summary>
        /// Calculates ER, VPI and VQuad from a zero chirp MZ differential sweep.
        /// </summary>
        /// <param name="sweepData">Sweep data</param>
        /// <param name="nominalSourceLevel"></param>
        public static MzAnalysisResults ZeroChirp_LSingleSweep
            (ILMZSweepResult sweepData, double nominalSourceLevel)
        {
            // generate the differential data to analyse - left arm data minus right arm data
            double[] diffDriveArrayPlus;
            //double[] diffDriveArrayMinus;

            if (sweepData.Type == SweepType.SingleEndVoltage)
            {
                diffDriveArrayPlus = sweepData.SweepData[ILMZSweepDataType.LeftArmModBias_V];
                //diffDriveArrayMinus = sweepResult.SweepData[ILMZSweepDataType.RightArmModBias_V];
            }
            else if (sweepData.Type == SweepType.SingleEndCurrent)
            {
                diffDriveArrayPlus = sweepData.SweepData[ILMZSweepDataType.LeftArmImb_A];
                //diffDriveArrayMinus = sweepResult.SweepData[ILMZSweepDataType.RightArmImb_A];
            }
            else
            {
                throw new ArgumentException("Sweep type isn't single: " + sweepData.Type);
            }
            // sanity check nominal source level to 2 sig figs to avoid major problems
            double nominalSourceLevelCalc = diffDriveArrayPlus[0];
            if (Math.Abs(nominalSourceLevel - nominalSourceLevelCalc) > 0.1)
            {
                string errMsg = string.Format("Nominal source level doesn't match data: Specified={0}, From Sweep={1}",
                    nominalSourceLevel, nominalSourceLevelCalc);
                throw new ArgumentException(errMsg);
            }
            // subtract the arrays for analysis
            double[] diffDriveArray =
                Alg_ArrayFunctions.ReverseArray(diffDriveArrayPlus);
            double[] powerArray = sweepData.SweepData[ILMZSweepDataType.FibrePower_mW];
            // do the analysis
            Alg_MZAnalysis.MZAnalysis mzAnlyRaw =
                Alg_MZAnalysis.ZeroChirpAnalysis(diffDriveArray, powerArray, false, true, 0);

            // find the appropriate left and right arm source values
            MzAnalysisResults mzAnly = new MzAnalysisResults(mzAnlyRaw);
            // MIN
            CalcDifferentialArmValues(mzAnlyRaw.VoltageAtMin, nominalSourceLevel,
                out mzAnly.Min_SrcL, out mzAnly.Min_SrcR);

            int i = 0;
            double Min = 5;
            int MinIndex = 0;
            for (i = 0; i < 200; i++)
            {
                if (powerArray[i] < Min)
                {
                    Min = powerArray[i];
                    MinIndex = i;
                }
            }
            mzAnly.LMinCurrent = diffDriveArrayPlus[MinIndex];
            // MAX
            CalcDifferentialArmValues(mzAnlyRaw.VoltageAtMax, nominalSourceLevel,
                out mzAnly.Max_SrcL, out mzAnly.Max_SrcR);
            // IMB
            CalcDifferentialArmValues(mzAnlyRaw.VImb, nominalSourceLevel,
                out mzAnly.Imb_SrcL, out mzAnly.Imb_SrcR);
            // QUAD
            CalcDifferentialArmValues(mzAnlyRaw.VQuad, nominalSourceLevel,
                out mzAnly.Quad_SrcL, out mzAnly.Quad_SrcR);

            return mzAnly;
        }

        /// <summary>
        /// Calculates ER, VPI and VQuad from a zero chirp MZ differential sweep.
        /// </summary>
        /// <param name="sweepData">Sweep data</param>
        /// <param name="nominalSourceLevel"></param>
        /// <returns>A structure containing all of the analysis data</returns>
        public static MzAnalysisResults ZeroChirp_RSingleSweep
            (ILMZSweepResult sweepData, double nominalSourceLevel)
        {
            // generate the differential data to analyse - left arm data minus right arm data
            double[] diffDriveArrayPlus;
            //double[] diffDriveArrayMinus;

            if (sweepData.Type == SweepType.SingleEndVoltage)
            {
                diffDriveArrayPlus = sweepData.SweepData[ILMZSweepDataType.RightArmModBias_V];
                //diffDriveArrayMinus = sweepResult.SweepData[ILMZSweepDataType.RightArmModBias_V];
            }
            else if (sweepData.Type == SweepType.SingleEndCurrent)
            {
                diffDriveArrayPlus = sweepData.SweepData[ILMZSweepDataType.RightArmImb_A];
                //diffDriveArrayMinus = sweepResult.SweepData[ILMZSweepDataType.RightArmImb_A];
            }
            else
            {
                throw new ArgumentException("Sweep type isn't single: " + sweepData.Type);
            }
            // sanity check nominal source level to 2 sig figs to avoid major problems
            double nominalSourceLevelCalc = diffDriveArrayPlus[0];
            if (Math.Abs(nominalSourceLevel - nominalSourceLevelCalc) > 0.1)
            {
                string errMsg = string.Format("Nominal source level doesn't match data: Specified={0}, From Sweep={1}",
                    nominalSourceLevel, nominalSourceLevelCalc);
                throw new ArgumentException(errMsg);
            }
            // subtract the arrays for analysis
            double[] diffDriveArray =
                Alg_ArrayFunctions.ReverseArray(diffDriveArrayPlus);
            double[] powerArray = sweepData.SweepData[ILMZSweepDataType.FibrePower_mW];
            // do the analysis
            Alg_MZAnalysis.MZAnalysis mzAnlyRaw =
                Alg_MZAnalysis.ZeroChirpAnalysis(diffDriveArray, powerArray, false, true, 0);

            // find the appropriate left and right arm source values
            MzAnalysisResults mzAnly = new MzAnalysisResults(mzAnlyRaw);
            // MIN
            CalcDifferentialArmValues(mzAnlyRaw.VoltageAtMin, nominalSourceLevel,
                out mzAnly.Min_SrcL, out mzAnly.Min_SrcR);

            int i = 0;
            double Min = 5;
            int MinIndex = 0;
            for (i = 0; i < 200; i++)
            {
                if (powerArray[i] < Min)
                {
                    Min = powerArray[i];
                    MinIndex = i;
                }
            }
            mzAnly.RMinCurrent = diffDriveArrayPlus[MinIndex];
            // MAX
            CalcDifferentialArmValues(mzAnlyRaw.VoltageAtMax, nominalSourceLevel,
                out mzAnly.Max_SrcL, out mzAnly.Max_SrcR);
            // IMB
            CalcDifferentialArmValues(mzAnlyRaw.VImb, nominalSourceLevel,
                out mzAnly.Imb_SrcL, out mzAnly.Imb_SrcR);
            // QUAD
            CalcDifferentialArmValues(mzAnlyRaw.VQuad, nominalSourceLevel,
                out mzAnly.Quad_SrcL, out mzAnly.Quad_SrcR);

            return mzAnly;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sweepData"></param>
        /// <param name="nominalSourceLevel"></param>
        /// <param name="usePositiveSlope"></param>
        /// <returns></returns>
        public static MzAnalysisResults ZeroChirp_DifferentialSweep_DoubleSlope
            (ILMZSweepResult sweepData, double nominalSourceLevel, bool usePositiveSlope)
        {
            // generate the differential data to analyse - left arm data minus right arm data
            double[] diffDriveArrayPlus;
            double[] diffDriveArrayMinus;

            if (sweepData.Type == SweepType.DifferentialVoltage)
            {
                diffDriveArrayPlus = sweepData.SweepData[ILMZSweepDataType.LeftArmModBias_V];
                diffDriveArrayMinus = sweepData.SweepData[ILMZSweepDataType.RightArmModBias_V];
            }
            else if (sweepData.Type == SweepType.DifferentialCurrent)
            {
                diffDriveArrayPlus = sweepData.SweepData[ILMZSweepDataType.LeftArmImb_A];
                diffDriveArrayMinus = sweepData.SweepData[ILMZSweepDataType.RightArmImb_A];
            }
            else
            {
                throw new ArgumentException("Sweep type isn't differential: " + sweepData.Type);
            }
            // sanity check nominal source level to 2 sig figs to avoid major problems
            double nominalSourceLevelCalc = (diffDriveArrayMinus[0] + diffDriveArrayPlus[0]) / 2;
            if (Math.Abs(nominalSourceLevel - nominalSourceLevelCalc) > 0.1)
            {
                string errMsg = string.Format("Nominal source level doesn't match data: Specified={0}, From Sweep={1}",
                    nominalSourceLevel, nominalSourceLevelCalc);
                throw new ArgumentException(errMsg);
            }
            // subtract the arrays for analysis
            double[] diffDriveArray =
                Alg_ArrayFunctions.SubtractArrays(diffDriveArrayPlus, diffDriveArrayMinus);
            double[] powerArray = sweepData.SweepData[ILMZSweepDataType.FibrePower_mW];
            // do the analysis
            Alg_MZAnalysis.MZAnalysis mzAnlyRaw =
                Alg_Doubleslope.TCMZDoubleSlope(diffDriveArray, powerArray, false, usePositiveSlope, 0);

            // find the appropriate left and right arm source values
            MzAnalysisResults mzAnly = new MzAnalysisResults(mzAnlyRaw);
            // MIN
            CalcDifferentialArmValues(mzAnlyRaw.VoltageAtMin, nominalSourceLevel,
                out mzAnly.Min_SrcL, out mzAnly.Min_SrcR);
            // MAX
            CalcDifferentialArmValues(mzAnlyRaw.VoltageAtMax, nominalSourceLevel,
                out mzAnly.Max_SrcL, out mzAnly.Max_SrcR);
            // IMB
            CalcDifferentialArmValues(mzAnlyRaw.VImb, nominalSourceLevel,
                out mzAnly.Imb_SrcL, out mzAnly.Imb_SrcR);
            // QUAD
            CalcDifferentialArmValues(mzAnlyRaw.VQuad, nominalSourceLevel,
                out mzAnly.Quad_SrcL, out mzAnly.Quad_SrcR);
            mzAnly.usePositiveSlope = usePositiveSlope;
            return mzAnly;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sweepData"></param>
        /// <param name="nominalSourceLevel"></param>
        /// <param name="usePositiveSlope"></param>
        /// <returns></returns>
        public static MzAnalysisResults ZeroChirp_LSingleSweep_DoubleSlope
            (ILMZSweepResult sweepData, double nominalSourceLevel, bool usePositiveSlope)
        {
            // generate the differential data to analyse - left arm data minus right arm data
            double[] diffDriveArrayPlus;
            //double[] diffDriveArrayMinus;

            if (sweepData.Type == SweepType.SingleEndVoltage)
            {
                diffDriveArrayPlus = sweepData.SweepData[ILMZSweepDataType.LeftArmModBias_V];
                //diffDriveArrayMinus = sweepResult.SweepData[ILMZSweepDataType.RightArmModBias_V];
            }
            else if (sweepData.Type == SweepType.SingleEndCurrent)
            {
                diffDriveArrayPlus = sweepData.SweepData[ILMZSweepDataType.LeftArmImb_A];
                //diffDriveArrayMinus = sweepResult.SweepData[ILMZSweepDataType.RightArmImb_A];
            }
            else
            {
                throw new ArgumentException("Sweep type isn't single: " + sweepData.Type);
            }
            // sanity check nominal source level to 2 sig figs to avoid major problems
            double nominalSourceLevelCalc = diffDriveArrayPlus[0];
            if (Math.Abs(nominalSourceLevel - nominalSourceLevelCalc) > 0.1)
            {
                string errMsg = string.Format("Nominal source level doesn't match data: Specified={0}, From Sweep={1}",
                    nominalSourceLevel, nominalSourceLevelCalc);
                throw new ArgumentException(errMsg);
            }
            // subtract the arrays for analysis
            double[] diffDriveArray =
                Alg_ArrayFunctions.ReverseArray(diffDriveArrayPlus);
            double[] powerArray = sweepData.SweepData[ILMZSweepDataType.FibrePower_mW];
            // do the analysis
            Alg_MZAnalysis.MZAnalysis mzAnlyRaw =
                Alg_Doubleslope.TCMZDoubleSlope(diffDriveArray, powerArray, false, usePositiveSlope, 0);

            // find the appropriate left and right arm source values
            MzAnalysisResults mzAnly = new MzAnalysisResults(mzAnlyRaw);
            // MIN
            CalcDifferentialArmValues(mzAnlyRaw.VoltageAtMin, nominalSourceLevel,
                out mzAnly.Min_SrcL, out mzAnly.Min_SrcR);

            int i = 0;
            double Min = 5;
            int MinIndex = 0;
            for (i = 0; i < 200; i++)
            {
                if (powerArray[i] < Min)
                {
                    Min = powerArray[i];
                    MinIndex = i;
                }
            }
            mzAnly.LMinCurrent = diffDriveArrayPlus[MinIndex];

            // MAX
            CalcDifferentialArmValues(mzAnlyRaw.VoltageAtMax, nominalSourceLevel,
                out mzAnly.Max_SrcL, out mzAnly.Max_SrcR);
            // IMB
            CalcDifferentialArmValues(mzAnlyRaw.VImb, nominalSourceLevel,
                out mzAnly.Imb_SrcL, out mzAnly.Imb_SrcR);
            // QUAD
            CalcDifferentialArmValues(mzAnlyRaw.VQuad, nominalSourceLevel,
                out mzAnly.Quad_SrcL, out mzAnly.Quad_SrcR);
            mzAnly.usePositiveSlope = usePositiveSlope;
            return mzAnly;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sweepData"></param>
        /// <param name="nominalSourceLevel"></param>
        /// <param name="usePositiveSlope"></param>
        /// <returns></returns>
        public static MzAnalysisResults ZeroChirp_RSingleSweep_DoubleSlope
            (ILMZSweepResult sweepData, double nominalSourceLevel, bool usePositiveSlope)
        {
            // generate the differential data to analyse - left arm data minus right arm data
            double[] diffDriveArrayPlus;
            //double[] diffDriveArrayMinus;

            if (sweepData.Type == SweepType.SingleEndVoltage)
            {
                diffDriveArrayPlus = sweepData.SweepData[ILMZSweepDataType.RightArmModBias_V];
                //diffDriveArrayMinus = sweepResult.SweepData[ILMZSweepDataType.RightArmModBias_V];
            }
            else if (sweepData.Type == SweepType.SingleEndCurrent)
            {
                diffDriveArrayPlus = sweepData.SweepData[ILMZSweepDataType.RightArmImb_A];
                //diffDriveArrayMinus = sweepResult.SweepData[ILMZSweepDataType.RightArmImb_A];
            }
            else
            {
                throw new ArgumentException("Sweep type isn't single: " + sweepData.Type);
            }
            // sanity check nominal source level to 2 sig figs to avoid major problems
            double nominalSourceLevelCalc = diffDriveArrayPlus[0];
            if (Math.Abs(nominalSourceLevel - nominalSourceLevelCalc) > 0.1)
            {
                string errMsg = string.Format("Nominal source level doesn't match data: Specified={0}, From Sweep={1}",
                    nominalSourceLevel, nominalSourceLevelCalc);
                throw new ArgumentException(errMsg);
            }
            // subtract the arrays for analysis
            double[] diffDriveArray =
                Alg_ArrayFunctions.ReverseArray(diffDriveArrayPlus);
            double[] powerArray = sweepData.SweepData[ILMZSweepDataType.FibrePower_mW];
            // do the analysis
            Alg_MZAnalysis.MZAnalysis mzAnlyRaw =
                Alg_Doubleslope.TCMZDoubleSlope(diffDriveArray, powerArray, false, usePositiveSlope, 0);

            // find the appropriate left and right arm source values
            MzAnalysisResults mzAnly = new MzAnalysisResults(mzAnlyRaw);
            // MIN
            CalcDifferentialArmValues(mzAnlyRaw.VoltageAtMin, nominalSourceLevel,
                out mzAnly.Min_SrcL, out mzAnly.Min_SrcR);
            int i = 0;
            double Min = 5;
            int MinIndex = 0;
            for (i = 0; i < 200; i++)
            {
                if (powerArray[i] < Min)
                {
                    Min = powerArray[i];
                    MinIndex = i;
                }
            }
            mzAnly.RMinCurrent = diffDriveArrayPlus[MinIndex];

            // MAX
            CalcDifferentialArmValues(mzAnlyRaw.VoltageAtMax, nominalSourceLevel,
                out mzAnly.Max_SrcL, out mzAnly.Max_SrcR);
            // IMB
            CalcDifferentialArmValues(mzAnlyRaw.VImb, nominalSourceLevel,
                out mzAnly.Imb_SrcL, out mzAnly.Imb_SrcR);
            // QUAD
            CalcDifferentialArmValues(mzAnlyRaw.VQuad, nominalSourceLevel,
                out mzAnly.Quad_SrcL, out mzAnly.Quad_SrcR);
            mzAnly.usePositiveSlope = usePositiveSlope;
            return mzAnly;
        }
        /// <summary>
        /// Calculates ER, VPI and VQuad from a negative chirp MZ differential sweep.
        /// </summary>
        /// <param name="sweepData">Sweep data</param>
        /// <param name="nominalSourceLevel">Nominal source level in A or V</param>
        /// <param name="vcmOffset"></param>
        /// <returns>A structure containing all of the analysis data</returns>
        public static MzAnalysisResults Differential_NegChirp_DifferentialSweep
            (ILMZSweepResult sweepData, double nominalSourceLevel, double vcmOffset)
        {
            // generate the differential data to analyse - left arm data minus right arm data
            double[] diffDriveArrayPlus;
            double[] diffDriveArrayMinus;

            if (sweepData.Type == SweepType.DifferentialVoltage)
            {
                diffDriveArrayPlus = sweepData.SweepData[ILMZSweepDataType.LeftArmModBias_V];
                diffDriveArrayMinus = sweepData.SweepData[ILMZSweepDataType.RightArmModBias_V];
            }
            else if (sweepData.Type == SweepType.DifferentialCurrent)
            {
                diffDriveArrayPlus = sweepData.SweepData[ILMZSweepDataType.LeftArmImb_A];
                diffDriveArrayMinus = sweepData.SweepData[ILMZSweepDataType.RightArmImb_A];
            }
            else
            {
                throw new ArgumentException("Sweep type isn't differential: " + sweepData.Type);
            }
            // sanity check nominal source level to 2 sig figs to avoid major problems
            double nominalSourceLevelCalc = (diffDriveArrayMinus[0] + diffDriveArrayPlus[0]) / 2;//jack.zhang
            if (Math.Abs(nominalSourceLevel - nominalSourceLevelCalc) > 0.1)
            {
                string errMsg = string.Format("Nominal source level doesn't match data: Specified={0}, From Sweep={1}",
                    nominalSourceLevel, nominalSourceLevelCalc);
                throw new ArgumentException(errMsg);
            }
            // subtract the arrays for analysis
            double[] diffDriveArray =
                Alg_ArrayFunctions.SubtractArrays(diffDriveArrayPlus, diffDriveArrayMinus);
            double[] powerArray = sweepData.SweepData[ILMZSweepDataType.FibrePower_mW];
            // do the analysis
            Alg_MZAnalysis.MZAnalysis mzAnlyRaw =
                Alg_MZAnalysis.ZeroChirpAnalysis(diffDriveArray, powerArray, false, false, vcmOffset);

            // find the appropriate left and right arm source values
            MzAnalysisResults mzAnly = new MzAnalysisResults(mzAnlyRaw);
            // MIN
            CalcDifferentialArmValues(mzAnlyRaw.VoltageAtMin, nominalSourceLevel,
                out mzAnly.Min_SrcL, out mzAnly.Min_SrcR);
            // MAX
            CalcDifferentialArmValues(mzAnlyRaw.VoltageAtMax, nominalSourceLevel,
                out mzAnly.Max_SrcL, out mzAnly.Max_SrcR);
            // IMB
            CalcDifferentialArmValues(mzAnlyRaw.VImb, nominalSourceLevel,
                out mzAnly.Imb_SrcL, out mzAnly.Imb_SrcR);
            // QUAD
            CalcDifferentialArmValues(mzAnlyRaw.VQuad, nominalSourceLevel,
                out mzAnly.Quad_SrcL, out mzAnly.Quad_SrcR);

            return mzAnly;
        }

        // Alice.Huang     2010-04-29
        // add this fuction for txfp negative chirp analysis
        // this fuction is derives from Differential_NegChirp_DifferentialSweep

        /// <summary>
        /// Calculates ER, VPI and VQuad from a negative chirp MZ Imbalance differential sweep 
        /// by make reference to the specified sourve level coresponding to featurePower
        /// which might be Min,Max,Quardure or Max-3dB power.
        /// </summary>
        /// <param name="sweepData">Sweep data</param>
        /// <param name="nominalSourceLevel">Nominal source level in A or V</param>
        /// <param name="featurePowerType"> feafure power point to make reference during the analysis </param>
        /// <param name="vcmOffset"></param>
        /// <returns>A structure containing all of the analysis data</returns>
        public static MzAnalysisResults DiffImbalance_NegChirp_FindFeaturePointByRef
            (ILMZSweepResult sweepData, double nominalSourceLevel, double refSourceLevel,
            Alg_MZAnalysis.MzFeaturePowerType featurePower)
        {
            // generate the differential data to analyse - left arm data minus right arm data
            double[] diffDriveArrayPlus;
            double[] diffDriveArrayMinus;


            if (sweepData.Type == SweepType.DifferentialCurrent)
            {
                diffDriveArrayPlus = sweepData.SweepData[ILMZSweepDataType.LeftArmImb_A];
                diffDriveArrayMinus = sweepData.SweepData[ILMZSweepDataType.RightArmImb_A];
            }
            else
            {
                throw new ArgumentException("Sweep type isn't differential Imb data : " + sweepData.Type);
            }
            // sanity check nominal source level to 2 sig figs to avoid major problems
            double nominalSourceLevelCalc = (diffDriveArrayMinus[0] + diffDriveArrayPlus[0]) / 2;//jack.zhang
            if (Math.Abs(nominalSourceLevel - nominalSourceLevelCalc) > 0.1)
            {
                string errMsg = string.Format("Nominal source level doesn't match data: Specified={0}, From Sweep={1}",
                    nominalSourceLevel, nominalSourceLevelCalc);
                throw new ArgumentException(errMsg);
            }
            // subtract the arrays for analysis
            double[] diffDriveArray =
                Alg_ArrayFunctions.SubtractArrays(diffDriveArrayPlus, diffDriveArrayMinus);
            double[] powerArray = sweepData.SweepData[ILMZSweepDataType.FibrePower_mW];
            // do the analysis
            Alg_MZAnalysis.MZAnalysis mzAnlyRaw = Alg_MZAnalysis.FindFeaturePointClosestToOffset
                (diffDriveArray, powerArray, false, refSourceLevel, featurePower);

            // find the appropriate left and right arm source values
            MzAnalysisResults mzAnly = new MzAnalysisResults(mzAnlyRaw);
            // MIN
            CalcDifferentialArmValues(mzAnlyRaw.VoltageAtMin, nominalSourceLevel,
                out mzAnly.Min_SrcL, out mzAnly.Min_SrcR);
            // MAX
            CalcDifferentialArmValues(mzAnlyRaw.VoltageAtMax, nominalSourceLevel,
                out mzAnly.Max_SrcL, out mzAnly.Max_SrcR);
            // IMB
            CalcDifferentialArmValues(mzAnlyRaw.VImb, nominalSourceLevel,
                out mzAnly.Imb_SrcL, out mzAnly.Imb_SrcR);
            // QUAD
            CalcDifferentialArmValues(mzAnlyRaw.VQuad, nominalSourceLevel,
                out mzAnly.Quad_SrcL, out mzAnly.Quad_SrcR);

            return mzAnly;
        }
        //Echoxl.wang  2011-03-24
        //add this overwrite function to ensure we can find a trough or peak point by reference
        /// <summary>
        /// Calculates ER, VPI and VQuad from a negative chirp MZ Imbalance differential sweep 
        /// by make reference to the specified sourve level coresponding to featurePower
        /// which might be Min,Max,Quardure or Max-3dB power.
        /// </summary>
        /// <param name="sweepData">Sweep data</param>
        /// <param name="nominalSourceLevel">Nominal source level in A or V</param>
        /// <param name="minMaxRefSourceLevel">array hold min point voltage value and max point voltage value as reference</param>
        /// <param name="featurePowerType"> feafure power point to make reference during the analysis </param>
        /// <param name="vcmOffset"></param>
        /// <returns>A structure containing all of the analysis data</returns>
        public static MzAnalysisResults DiffImbalance_NegChirp_FindFeaturePointByRef
            (ILMZSweepResult sweepData, double nominalSourceLevel, double[] minMaxRefSourceLevel,
            Alg_MZAnalysis.MzFeaturePowerType featurePower)
        {
            // generate the differential data to analyse - left arm data minus right arm data
            double[] diffDriveArrayPlus;
            double[] diffDriveArrayMinus;


            if (sweepData.Type == SweepType.DifferentialCurrent)
            {
                diffDriveArrayPlus = sweepData.SweepData[ILMZSweepDataType.LeftArmImb_A];
                diffDriveArrayMinus = sweepData.SweepData[ILMZSweepDataType.RightArmImb_A];
            }
            else
            {
                throw new ArgumentException("Sweep type isn't differential Imb data : " + sweepData.Type);
            }
            // sanity check nominal source level to 2 sig figs to avoid major problems
            double nominalSourceLevelCalc = (diffDriveArrayMinus[0] + diffDriveArrayPlus[0]) / 2;//jack.zhang
            if (Math.Abs(nominalSourceLevel - nominalSourceLevelCalc) > 0.1)
            {
                string errMsg = string.Format("Nominal source level doesn't match data: Specified={0}, From Sweep={1}",
                    nominalSourceLevel, nominalSourceLevelCalc);
                throw new ArgumentException(errMsg);
            }
            // subtract the arrays for analysis
            double[] diffDriveArray =
                Alg_ArrayFunctions.SubtractArrays(diffDriveArrayPlus, diffDriveArrayMinus);
            double[] powerArray = sweepData.SweepData[ILMZSweepDataType.FibrePower_mW];
            // do the analysis
            Alg_MZAnalysis.MZAnalysis mzAnlyRaw = Alg_MZAnalysis.FindFeaturePointClosestToOffset
                (diffDriveArray, powerArray, false, minMaxRefSourceLevel, featurePower);

            // find the appropriate left and right arm source values
            MzAnalysisResults mzAnly = new MzAnalysisResults(mzAnlyRaw);
            // MIN
            CalcDifferentialArmValues(mzAnlyRaw.VoltageAtMin, nominalSourceLevel,
                out mzAnly.Min_SrcL, out mzAnly.Min_SrcR);
            // MAX
            CalcDifferentialArmValues(mzAnlyRaw.VoltageAtMax, nominalSourceLevel,
                out mzAnly.Max_SrcL, out mzAnly.Max_SrcR);
            // IMB
            CalcDifferentialArmValues(mzAnlyRaw.VImb, nominalSourceLevel,
                out mzAnly.Imb_SrcL, out mzAnly.Imb_SrcR);
            // QUAD
            CalcDifferentialArmValues(mzAnlyRaw.VQuad, nominalSourceLevel,
                out mzAnly.Quad_SrcL, out mzAnly.Quad_SrcR);

            return mzAnly;
        }
        /// <summary>
        /// Analysis for a differential imbalance control sweep performed on a device 
        /// intended for use in a Single-Ended Negative Chirp application.
        /// The only parameter of real interest at final test is the imbalance point.
        /// </summary>
        /// <param name="sweepData">Sweep data</param>
        /// <param name="nominalSourceLevel">Nominal control current</param>
        /// <returns>Imbalance settings</returns>
        public static MzAnalysisResults SingleEnded_NegChirp_DifferentialImbalanceSweep(ILMZSweepResult sweepData, double nominalSourceLevel)
        {
            // generate the differential data to analyse - left arm data minus right arm data
            double[] diffDriveArrayPlus;
            double[] diffDriveArrayMinus;

            if (sweepData.Type == SweepType.DifferentialVoltage)
            {
                diffDriveArrayPlus = sweepData.SweepData[ILMZSweepDataType.LeftArmModBias_V];
                diffDriveArrayMinus = sweepData.SweepData[ILMZSweepDataType.RightArmModBias_V];
            }
            else if (sweepData.Type == SweepType.DifferentialCurrent)
            {
                diffDriveArrayPlus = sweepData.SweepData[ILMZSweepDataType.LeftArmImb_A];
                diffDriveArrayMinus = sweepData.SweepData[ILMZSweepDataType.RightArmImb_A];
            }
            else
            {
                throw new ArgumentException("Sweep type isn't differential: " + sweepData.Type);
            }
            // sanity check nominal source level to 2 sig figs to avoid major problems
            double nominalSourceLevelCalc = (diffDriveArrayMinus[0] + diffDriveArrayPlus[0]) / 2;
            if (Math.Round(nominalSourceLevel, 1) != Math.Round(nominalSourceLevelCalc, 1))
            {
                string errMsg = string.Format("Nominal source level doesn't match data: Specified={0}, From Sweep={1}",
                    nominalSourceLevel, nominalSourceLevelCalc);
                throw new ArgumentException(errMsg);
            }
            // subtract the arrays for analysis
            double[] diffDriveArray =
                Alg_ArrayFunctions.SubtractArrays(diffDriveArrayPlus, diffDriveArrayMinus);
            double[] powerArray = sweepData.SweepData[ILMZSweepDataType.FibrePower_mW];
            // do the analysis
            Alg_MZAnalysis.MZAnalysis mzAnlyRaw =
                Alg_MZAnalysis.NegChirpImbalanceCtrlAnalysis(diffDriveArray, powerArray, false);

            // find the appropriate left and right arm source values
            MzAnalysisResults mzAnly = new MzAnalysisResults(mzAnlyRaw);
            // MIN
            CalcDifferentialArmValues(mzAnlyRaw.VoltageAtMin, nominalSourceLevel,
                out mzAnly.Min_SrcL, out mzAnly.Min_SrcR);
            // MAX
            CalcDifferentialArmValues(mzAnlyRaw.VoltageAtMax, nominalSourceLevel,
                out mzAnly.Max_SrcL, out mzAnly.Max_SrcR);
            // IMB
            CalcDifferentialArmValues(mzAnlyRaw.VImb, nominalSourceLevel,
                out mzAnly.Imb_SrcL, out mzAnly.Imb_SrcR);
            // QUAD
            CalcDifferentialArmValues(mzAnlyRaw.VQuad, nominalSourceLevel,
                out mzAnly.Quad_SrcL, out mzAnly.Quad_SrcR);

            return mzAnly;
        }

        /// <summary>
        /// Calculate the arm source volt/current in a stitched single ended scan. 
        /// Assumes that left arm source values are correct, right arm source values have been
        /// multiplied by -1.
        /// </summary>
        /// <param name="stitchedValue">The stitched value from the stitched data: left </param>
        /// <param name="fixedLeft">Fixed value of the left arm</param>
        /// <param name="fixedRight">Fixed value of the right arm</param>
        /// <param name="leftArmValue">Calculated actual left arm value</param>
        /// <param name="rightArmValue">Calculated actual right arm value</param>
        public static void CalcSingleEndedArmValues(double stitchedValue, double fixedLeft, double fixedRight,
            out double leftArmValue, out double rightArmValue)
        {
            if (stitchedValue <= 0)
            {
                leftArmValue = stitchedValue;
                rightArmValue = fixedRight;
            }
            else
            {
                leftArmValue = fixedLeft;
                rightArmValue = -stitchedValue;
            }
        }
        //Mark.F add below function at 2011-04-22
        /// <summary>
        /// Calculates new Vcm and recalculates Vpi, Voffset and Icm at this level.
        /// </summary>
        /// <param name="mzChannel">Data from two MZ sweeps at different common mode voltages</param>
        /// <returns>Recalculated MZ settings</returns>
        public static MzChannelData CalculateMzSetupAtMaxVpi(MzSweepDataItuChannel mzChannel)
        {
            if (mzChannel.VcmData.Count < 2)
            {
                throw new ArgumentException("MZ channel characterisation data is incorrect. Need 2 Vcm measurements, have " + mzChannel.VcmData.Count.ToString());
            }
            if (mzChannel.VcmData[0].Vcm_V == mzChannel.VcmData[1].Vcm_V)
            {
                throw new ArgumentException("MZ channel characterisation data is incorrect. Need 2 Vcm measurements at different values of Vcm, have two at " + mzChannel.VcmData[0].Vcm_V.ToString());
            }

            MzRawData mzRawData_Vcm1 = new MzRawData();
            MzRawData mzRawData_Vcm2 = new MzRawData();

            MzChannelData mzChannelData = new MzChannelData();
            mzChannelData.ItuChannelIndex = mzChannel.ItuChannelIndex;

            //
            // Get data
            //
            // Data from Vcm point 1
            mzRawData_Vcm1.Vpi_V = mzChannel.VcmData[0].GetValueDouble(MzItuAtVcmMeasurements.Vpi_V);
            mzRawData_Vcm1.AbsVimb_V = Math.Abs(CalculateVoffset(mzChannel.VcmData[0]));
            mzRawData_Vcm1.Vimb_V = CalculateVoffset(mzChannel.VcmData[0]);
            mzRawData_Vcm1.Vcm_V = mzChannel.VcmData[0].Vcm_V;
            mzRawData_Vcm1.Iimb_mA = (mzChannel.VcmData[0].GetValueDouble(MzItuAtVcmMeasurements.IImbQuadL_mA)
                - mzChannel.VcmData[0].GetValueDouble(MzItuAtVcmMeasurements.IImbQuadR_mA));

            // Data from Vcm point 2
            mzRawData_Vcm2.Vpi_V = mzChannel.VcmData[1].GetValueDouble(MzItuAtVcmMeasurements.Vpi_V);
            mzRawData_Vcm2.AbsVimb_V = Math.Abs(CalculateVoffset(mzChannel.VcmData[1]));
            mzRawData_Vcm2.Vimb_V = CalculateVoffset(mzChannel.VcmData[1]);
            mzRawData_Vcm2.Vcm_V = mzChannel.VcmData[1].Vcm_V;
            mzRawData_Vcm2.Iimb_mA = (mzChannel.VcmData[1].GetValueDouble(MzItuAtVcmMeasurements.IImbQuadL_mA)
                - mzChannel.VcmData[1].GetValueDouble(MzItuAtVcmMeasurements.IImbQuadR_mA));


            //
            // Calculate straight line fits for Vpi , Voffset & Iimb
            //
            // Vpi vs Vcm
            mzChannelData.VpiFit.Gradient = (mzRawData_Vcm1.Vpi_V - mzRawData_Vcm2.Vpi_V) / (mzRawData_Vcm1.Vcm_V - mzRawData_Vcm2.Vcm_V);
            // Offset required is Y at X=0, so swap X & Y and invert gradient
            mzChannelData.VpiFit.Offset = EstimateXAtY.Calculate(mzRawData_Vcm1.Vpi_V, mzRawData_Vcm1.Vcm_V, 1 / mzChannelData.VpiFit.Gradient, 0);

            // Voffset vs Vcm
            mzChannelData.VoffsetFit.Gradient = (mzRawData_Vcm1.Vimb_V - mzRawData_Vcm2.Vimb_V) / (mzRawData_Vcm1.Vcm_V - mzRawData_Vcm2.Vcm_V);
            // Offset required is Y at X=0, so swap X & Y and invert gradient
            mzChannelData.VoffsetFit.Offset = EstimateXAtY.Calculate(mzRawData_Vcm1.Vimb_V, mzRawData_Vcm1.Vcm_V, 1 / mzChannelData.VoffsetFit.Gradient, 0);

            // AbsVoffset vs Vcm
            mzChannelData.AbsVoffsetFit.Gradient = (mzRawData_Vcm1.AbsVimb_V - mzRawData_Vcm2.AbsVimb_V) / (mzRawData_Vcm1.Vcm_V - mzRawData_Vcm2.Vcm_V);
            // Offset required is Y at X=0, so swap X & Y and invert gradient
            mzChannelData.AbsVoffsetFit.Offset = EstimateXAtY.Calculate(mzRawData_Vcm1.AbsVimb_V, mzRawData_Vcm1.Vcm_V, 1 / mzChannelData.AbsVoffsetFit.Gradient, 0);

            // Iimb vs Vcm
            mzChannelData.IimbFit.Gradient = (mzRawData_Vcm1.Iimb_mA - mzRawData_Vcm2.Iimb_mA) / (mzRawData_Vcm1.Vcm_V - mzRawData_Vcm2.Vcm_V);
            // Offset required is Y at X=0, so swap X & Y and invert gradient
            mzChannelData.IimbFit.Offset = EstimateXAtY.Calculate(mzRawData_Vcm1.Iimb_mA, mzRawData_Vcm1.Vcm_V, 1 / mzChannelData.IimbFit.Gradient, 0);

            //
            // Calculate new Vcm
            //
            // This targets Vpi max
            mzChannelData.Vcm_V = (vpiMaxLimit - mzChannelData.VpiFit.Offset) / mzChannelData.VpiFit.Gradient;


            // Predict Vpi at the new Vcm
            mzChannelData.Vpi_V = mzChannelData.VpiFit.Gradient * mzChannelData.Vcm_V + mzChannelData.VpiFit.Offset;

            // Don't attempt to correct Vpi by selecting a new Vcm here.
            // This will prevent us from using imbalance to shift the characteristic.

            // To avoid driving the device too hard we should clamp the calculated Vcm against the limits
            mzChannelData.Vcm_V = Math.Max(mzChannelData.Vcm_V, vcmMinLimit);
            mzChannelData.Vcm_V = Math.Min(mzChannelData.Vcm_V, vcmMaxLimit);

            // ... and just in case we changed anything, recalculate Vpi. Not much we can do if this now fails Vpi

            mzChannelData.Vcm_V = Math.Min(mzChannelData.Vcm_V, margin - mzChannelData.Vpi_V / 2);
            mzChannelData.Vpi_V = mzChannelData.VpiFit.Gradient * mzChannelData.Vcm_V + mzChannelData.VpiFit.Offset;

            //  ... and Voffset at new Vcm
            mzChannelData.Voffset_V = mzChannelData.VoffsetFit.Gradient * mzChannelData.Vcm_V + mzChannelData.VoffsetFit.Offset;

            // ... and Icm at new Vcm
            mzChannelData.Icm_mA = mzChannelData.IimbFit.Gradient * mzChannelData.Vcm_V + mzChannelData.IimbFit.Offset;


            // Return channel data 
            return mzChannelData;

        }


        /// <summary>
        /// Three sets of data are fitted using the formula aVL^2 + bVR^2 + C = 1/Vpi.
        /// The coefficients a, c, and c are then used to calculate VR for a given VL at a target Vpi.
        /// </summary>
        /// <param name="mzVlVrVpiData1">dataset 1</param>
        /// <param name="mzVlVrVpiData2">dataset 2</param>
        /// <param name="mzVlVrVpiData3">dataset 3</param>
        /// <param name="vpiTarget_V">target Vpi</param>
        /// <param name="leftArmBiasTarget_V">target left bias V</param>
        /// <returns>Vcm</returns>
        public static double CalculateVcmForVpi(MzVlVrVpiData mzVlVrVpiData1, MzVlVrVpiData mzVlVrVpiData2, MzVlVrVpiData mzVlVrVpiData3, double vpiTarget_V, double leftArmBiasTarget_V)
        {
            // Calculate coefficients a, b, c

            /* A */
            double a_Numerator = ((1 / mzVlVrVpiData2.Vpi_V) - (1 / mzVlVrVpiData3.Vpi_V))
                - ((1 / mzVlVrVpiData1.Vpi_V) - (1 / mzVlVrVpiData2.Vpi_V))
                * (mzVlVrVpiData2.RightArmBias_V_Squared - mzVlVrVpiData3.RightArmBias_V_Squared)
                / (mzVlVrVpiData1.RightArmBias_V_Squared - mzVlVrVpiData2.RightArmBias_V_Squared);

            double a_Denominator = (mzVlVrVpiData2.LeftArmBias_V_Squared - mzVlVrVpiData3.LeftArmBias_V_Squared)
                - (mzVlVrVpiData2.RightArmBias_V_Squared - mzVlVrVpiData3.RightArmBias_V_Squared)
                / (mzVlVrVpiData1.RightArmBias_V_Squared - mzVlVrVpiData2.RightArmBias_V_Squared)
                * (mzVlVrVpiData1.LeftArmBias_V_Squared - mzVlVrVpiData2.LeftArmBias_V_Squared);

            double a = a_Numerator / a_Denominator;

            /* B */
            double b = (((1 / mzVlVrVpiData1.Vpi_V) - (1 / mzVlVrVpiData2.Vpi_V))
                - ((mzVlVrVpiData1.LeftArmBias_V_Squared - mzVlVrVpiData2.LeftArmBias_V_Squared) * a))
                / (mzVlVrVpiData1.LeftArmBias_V_Squared - mzVlVrVpiData2.LeftArmBias_V_Squared);

            /* C */
            double c = (1 / mzVlVrVpiData1.Vpi_V)
                - (a * mzVlVrVpiData1.LeftArmBias_V_Squared)
                - (b * mzVlVrVpiData1.RightArmBias_V_Squared);

            // Use the coefficients to solve the formula for right arm bias
            double rightArmBias_Squared = ((1 / vpiTarget_V)
                - c
                - (a * leftArmBiasTarget_V * leftArmBiasTarget_V))
                / b;

            double rightArmBias = -Math.Sqrt(Math.Abs(rightArmBias_Squared));

            double vcm_V = (leftArmBiasTarget_V + rightArmBias) / 2;
            return vcm_V;
        }

        public struct MzVlVrVpiData
        {
            public double Vpi_V;

            public double LeftArmBias_V;

            public double RightArmBias_V;

            public double LeftArmBias_V_Squared
            {
                get { return LeftArmBias_V * LeftArmBias_V; }
            }

            public double RightArmBias_V_Squared
            {
                get { return RightArmBias_V * RightArmBias_V; }
            }

            public double Vcm_V
            {
                get { return (LeftArmBias_V + RightArmBias_V) / 2; }
            }
        }


        /// <summary>
        /// Method that calculates the source value on the left arm and right arm
        /// calculated from the left-arm minus right-arm difference of a differential sweep
        /// </summary>
        /// <param name="diffValue">Left-right value</param>
        /// <param name="nominalValue">Nominal value(constant average of values)</param>
        /// <param name="leftArmVal">Calculated left arm value</param>
        /// <param name="rightArmVal">Calculated right arm value</param>
        public static void CalcDifferentialArmValues(double diffValue, double nominalValue,
            out double leftArmVal, out double rightArmVal)
        {
            leftArmVal = (diffValue / 2) + nominalValue;
            rightArmVal = nominalValue - (diffValue / 2);
        }

        /// <summary>
        /// Method that get all peak and valley points array,raul added on 2012.6.26
        /// </summary>
        /// <param name="sweepResult">Diff imb sweep data</param>
        /// <param name="peakArray">all peak points array</param>
        /// <param name="valleyArray">all trough points array</param>
        public static void GetPeakAndValleyPointArray(ILMZSweepResult sweepResult, out Alg_MZLVMinMax.DataPoint[] peakArray, out Alg_MZLVMinMax.DataPoint[] valleyArray)
        {
            double[] diffDriveArrayPlus;
            double[] diffDriveArrayMinus;


            if (sweepResult.Type == SweepType.DifferentialCurrent)
            {
                diffDriveArrayPlus = sweepResult.SweepData[ILMZSweepDataType.LeftArmImb_A];
                diffDriveArrayMinus = sweepResult.SweepData[ILMZSweepDataType.RightArmImb_A];
            }
            else
            {
                throw new ArgumentException("Sweep type isn't differential Imb data : " + sweepResult.Type);
            }

            // subtract the arrays for analysis
            double[] diffDriveArray = Alg_ArrayFunctions.SubtractArrays(diffDriveArrayPlus, diffDriveArrayMinus);

            double[] powerArray = sweepResult.SweepData[ILMZSweepDataType.FibrePower_mW];

            Alg_MZLVMinMax.LVMinMax turningPoints = Alg_MZLVMinMax.MZLVMinMax(diffDriveArray, powerArray, 5, false);

            turningPoints = Alg_MZAnalysis.AddArrayEndPoints(diffDriveArray, powerArray, turningPoints);

            peakArray = turningPoints.PeakData;
            valleyArray = turningPoints.ValleyData;
        }

        /// <summary>
        /// Class to give MZ analysis results, including calculation of raw left and right
        /// source levels (units depend on whether this is a V or L scan).
        /// Values are calculated according to whether the data comes combined single-ended
        /// or a differential sweep.
        /// </summary>
        public class MzAnalysisResults
        {
            internal MzAnalysisResults() { }

            internal MzAnalysisResults(Alg_MZAnalysis.MZAnalysis mzAnalysis)
            {
                this.MzAnalysis = mzAnalysis;
            }

            /// <summary>
            /// Raw MZ analysis results
            /// </summary>
            public readonly Alg_MZAnalysis.MZAnalysis MzAnalysis;

            /// <summary>
            /// Left source level correction. This will be similar to the quadrature voltage
            /// for some device families.
            /// </summary>
            public double Imb_SrcL;
            /// <summary>
            /// Right source level correction. This will be similar to the quadrature voltage
            /// for some device families.
            /// </summary>
            public double Imb_SrcR;
            /// <summary>
            /// Left source level at maximum power.
            /// </summary>
            public double Max_SrcL;
            /// <summary>
            /// Right source level at maximum power.
            /// </summary>
            public double Max_SrcR;
            /// <summary>
            /// Left source level at minimum power.
            /// </summary>
            public double Min_SrcL;
            /// <summary>
            /// Right source level at maximum power.
            /// </summary>
            public double Min_SrcR;
            /// <summary>
            /// Left source level at quadrature (half) power.
            /// </summary>
            public double Quad_SrcL;
            /// <summary>
            /// Right source level at quadrature (half) power.
            /// </summary>
            public double Quad_SrcR;
            /// <summary>
            /// Peak power value
            /// </summary>
            public double PowerAtMax_dBm
            {
                get { return MzAnalysis.PowerAtMax_dBm; }
            }
            /// <summary>
            /// Minimum power value
            /// </summary>
            public double PowerAtMin_dBm
            {
                get { return MzAnalysis.PowerAtMin_dBm; }
            }
            /// <summary>
            /// VPi
            /// </summary>
            public double Vpi
            {
                get { return MzAnalysis.Vpi; }
            }

            /// <summary>
            /// Extinction Ratio
            /// </summary>
            public double ExtinctionRatio_dB
            {
                get { return MzAnalysis.ExtinctionRatio_dB; }
            }

            public double Vimb
            {
                get { return MzAnalysis.VImb; }
            }
            /// <summary>
            /// whether  use positive Slope here
            /// </summary>
            public bool usePositiveSlope;
            /// <summary>
            /// LMinCurrent
            /// </summary>
            public double LMinCurrent;

            /// <summary>
            ///RMinCurrent
            /// </summary>
            public double RMinCurrent;
        }

        /// <summary>
        /// Class to give MZ analysis results, including calculation of raw left and right
        /// source levels (units depend on whether this is a V or L scan).
        /// Values are calculated according to whether the data comes combined single-ended
        /// or a differential sweep.
        /// </summary>
        public class MzSingleEndedAnalysisResults
        {
            private MzSingleEndedAnalysisResults() { }

            internal MzSingleEndedAnalysisResults(Alg_MZAnalysis.MZAnalysis mzAnalysis)
            {
                this.MzAnalysis = mzAnalysis;
            }

            /// <summary>
            /// Raw MZ analysis results
            /// </summary>
            public readonly Alg_MZAnalysis.MZAnalysis MzAnalysis;

            /// <summary>
            /// Source level correction. This will be similar to the quadrature voltage
            /// for zero chirp devices, but will the the minima for neg chirp device families.
            /// </summary>
            public double Imb_Src;
            /// <summary>
            /// Source level at maximum power.
            /// </summary>
            public double Max_Src;
            /// <summary>
            /// Source level at minimum power.
            /// </summary>
            public double Min_Src;
            /// <summary>
            /// Source level at quadrature (half) power.
            /// </summary>
            public double Quad_Src;
            /// <summary>
            /// Peak power value
            /// </summary>
            public double PowerAtMax_dBm
            {
                get { return MzAnalysis.PowerAtMax_dBm; }
            }
            /// <summary>
            /// Minimum power value
            /// </summary>
            public double PowerAtMin_dBm
            {
                get { return MzAnalysis.PowerAtMin_dBm; }
            }
            /// <summary>
            /// VPi
            /// </summary>
            public double Vpi
            {
                get { return MzAnalysis.Vpi; }
            }
            /// <summary>
            /// Extinction Ratio
            /// </summary>
            public double ExtinctionRatio_dB
            {
                get { return MzAnalysis.ExtinctionRatio_dB; }
            }
        }


        /// <summary>
        /// Check the power data to see whether the meter range is too high
        /// </summary>
        /// <param name="power_mW">Power data in mW</param>
        /// <returns>TRUE if data contains something close to 0mW</returns>
        public static bool CheckForUnderrange(double[] power_mW)
        {
            double min = Alg_FindFeature.FindAbsoluteMinWithSign(power_mW);
            return Math.Abs(min) < 0.000001;
        }

        /// <summary>
        /// Check the power data to see whether the meter range is too low
        /// </summary>
        /// <param name="power_mW">Power data in mW</param>
        /// <returns>TRUE if data contains something silly</returns>
        public static bool CheckForOverrange(double[] power_mW)
        {
            double max = Alg_FindFeature.FindAbsoluteMaxWithSign(power_mW);
            return Math.Abs(max) > 100;
        }

        /// <summary>
        /// Limits any data element below min level.
        /// </summary>
        /// <param name="power_mW">Power data in mW</param>        
        /// <param name="minLevel_dBm">Lower power limit</param>
        /// <returns>An array of data</returns>
        public static double[] FixUnderRangeData(double[] power_mW, double minLevel_dBm)
        {
            double minLevel_mW = Alg_PowConvert_dB.Convert_dBmtomW(minLevel_dBm);
            double[] fixedData = power_mW;

            for (int i = 0; i < power_mW.Length; i++)
            {
                if (power_mW[i] < minLevel_mW)
                {
                    fixedData[i] = minLevel_mW;
                }
                else
                {
                    fixedData[i] = power_mW[i];
                }
            }
            return fixedData;
        }

        /// <summary>
        /// Calculates new Vcm and recalculates Vpi, Voffset and Icm at this level.
        /// </summary>
        /// <param name="mzChannel">Data from two MZ sweeps at different common mode voltages</param>
        /// <returns>Recalculated MZ settings</returns>
        public static MzChannelData CalculateMzSetupAtIdealVcm(MzSweepDataItuChannel mzChannel)
        {
            if (mzChannel.VcmData.Count < 2)
            {
                throw new ArgumentException("MZ channel characterisation data is incorrect. Need 2 Vcm measurements, have " + mzChannel.VcmData.Count.ToString());
            }
            if (mzChannel.VcmData[0].Vcm_V == mzChannel.VcmData[1].Vcm_V)
            {
                throw new ArgumentException("MZ channel characterisation data is incorrect. Need 2 Vcm measurements at different values of Vcm, have two at " + mzChannel.VcmData[0].Vcm_V.ToString());
            }

            MzRawData mzRawData_Vcm1 = new MzRawData();
            MzRawData mzRawData_Vcm2 = new MzRawData();

            MzChannelData mzChannelData = new MzChannelData();
            mzChannelData.ItuChannelIndex = mzChannel.ItuChannelIndex;

            //
            // Get data
            //
            // Data from Vcm point 1
            mzRawData_Vcm1.Vpi_V = mzChannel.VcmData[0].GetValueDouble(MzItuAtVcmMeasurements.Vpi_V);
            mzRawData_Vcm1.AbsVimb_V = Math.Abs(CalculateVoffset(mzChannel.VcmData[0]));
            mzRawData_Vcm1.Vimb_V = CalculateVoffset(mzChannel.VcmData[0]);
            mzRawData_Vcm1.Vcm_V = mzChannel.VcmData[0].Vcm_V;
            mzRawData_Vcm1.Iimb_mA = (mzChannel.VcmData[0].GetValueDouble(MzItuAtVcmMeasurements.IImbQuadL_mA)
                - mzChannel.VcmData[0].GetValueDouble(MzItuAtVcmMeasurements.IImbQuadR_mA));

            // Data from Vcm point 2
            mzRawData_Vcm2.Vpi_V = mzChannel.VcmData[1].GetValueDouble(MzItuAtVcmMeasurements.Vpi_V);
            mzRawData_Vcm2.AbsVimb_V = Math.Abs(CalculateVoffset(mzChannel.VcmData[1]));
            mzRawData_Vcm2.Vimb_V = CalculateVoffset(mzChannel.VcmData[1]);
            mzRawData_Vcm2.Vcm_V = mzChannel.VcmData[1].Vcm_V;
            mzRawData_Vcm2.Iimb_mA = (mzChannel.VcmData[1].GetValueDouble(MzItuAtVcmMeasurements.IImbQuadL_mA)
                - mzChannel.VcmData[1].GetValueDouble(MzItuAtVcmMeasurements.IImbQuadR_mA));


            //
            // Calculate straight line fits for Vpi , Voffset & Iimb
            //
            // Vpi vs Vcm
            mzChannelData.VpiFit.Gradient = (mzRawData_Vcm1.Vpi_V - mzRawData_Vcm2.Vpi_V) / (mzRawData_Vcm1.Vcm_V - mzRawData_Vcm2.Vcm_V);
            // Offset required is Y at X=0, so swap X & Y and invert gradient
            mzChannelData.VpiFit.Offset = EstimateXAtY.Calculate(mzRawData_Vcm1.Vpi_V, mzRawData_Vcm1.Vcm_V, 1 / mzChannelData.VpiFit.Gradient, 0);

            // Voffset vs Vcm
            mzChannelData.VoffsetFit.Gradient = (mzRawData_Vcm1.Vimb_V - mzRawData_Vcm2.Vimb_V) / (mzRawData_Vcm1.Vcm_V - mzRawData_Vcm2.Vcm_V);
            // Offset required is Y at X=0, so swap X & Y and invert gradient
            mzChannelData.VoffsetFit.Offset = EstimateXAtY.Calculate(mzRawData_Vcm1.Vimb_V, mzRawData_Vcm1.Vcm_V, 1 / mzChannelData.VoffsetFit.Gradient, 0);

            // AbsVoffset vs Vcm
            mzChannelData.AbsVoffsetFit.Gradient = (mzRawData_Vcm1.AbsVimb_V - mzRawData_Vcm2.AbsVimb_V) / (mzRawData_Vcm1.Vcm_V - mzRawData_Vcm2.Vcm_V);
            // Offset required is Y at X=0, so swap X & Y and invert gradient
            mzChannelData.AbsVoffsetFit.Offset = EstimateXAtY.Calculate(mzRawData_Vcm1.AbsVimb_V, mzRawData_Vcm1.Vcm_V, 1 / mzChannelData.AbsVoffsetFit.Gradient, 0);

            // Iimb vs Vcm
            mzChannelData.IimbFit.Gradient = (mzRawData_Vcm1.Iimb_mA - mzRawData_Vcm2.Iimb_mA) / (mzRawData_Vcm1.Vcm_V - mzRawData_Vcm2.Vcm_V);
            // Offset required is Y at X=0, so swap X & Y and invert gradient
            mzChannelData.IimbFit.Offset = EstimateXAtY.Calculate(mzRawData_Vcm1.Iimb_mA, mzRawData_Vcm1.Vcm_V, 1 / mzChannelData.IimbFit.Gradient, 0);



            //
            // Calculate new Vcm
            //
            double Vmax = -0.55;    // -0.5 is specified in the XS. This is the closest that we want an MZ arm to get to 0v
            //
            // Latest formula from Roberto :
            // Vcm = (- 0.5 -Cvpi/4 + Coff/2) / (1 + Mvpi/4 - Moff/2) 
            //
            // Corrected formula from Dave :
            // Vcm = (- 0.5 -Cvpi/4 - Coff/2) / (1 + Mvpi/4 + Moff/2) 
            //
            mzChannelData.Vcm_V =
                (Vmax - (mzChannelData.VpiFit.Offset / 4) - (mzChannelData.AbsVoffsetFit.Offset / 2))
                / (1 + (mzChannelData.VpiFit.Gradient / 4) + (mzChannelData.AbsVoffsetFit.Gradient / 2));


            // Predict Vpi at the new Vcm
            mzChannelData.Vpi_V = mzChannelData.VpiFit.Gradient * mzChannelData.Vcm_V + mzChannelData.VpiFit.Offset;

            // Don't attempt to correct Vpi by selecting a new Vcm here.
            // This will prevent us from using imbalance to shift the characteristic.

            // To avoid driving the device too hard we should clamp the calculated Vcm against the limits
            mzChannelData.Vcm_V = Math.Max(mzChannelData.Vcm_V, vcmMinLimit);
            mzChannelData.Vcm_V = Math.Min(mzChannelData.Vcm_V, vcmMaxLimit);

            // ... and just in case we changed anything, recalculate Vpi. Not much we can do if this now fails Vpi
            mzChannelData.Vpi_V = mzChannelData.VpiFit.Gradient * mzChannelData.Vcm_V + mzChannelData.VpiFit.Offset;

            //  ... and Voffset at new Vcm
            mzChannelData.Voffset_V = mzChannelData.VoffsetFit.Gradient * mzChannelData.Vcm_V + mzChannelData.VoffsetFit.Offset;

            // ... and Icm at new Vcm
            mzChannelData.Icm_mA = mzChannelData.IimbFit.Gradient * mzChannelData.Vcm_V + mzChannelData.IimbFit.Offset;


            // Return channel data 
            return mzChannelData;

        }

        /// <summary>
        /// Calculates the trough point close to zero Jack.Zhang 2010-04-28.
        /// </summary>
        /// <param name="sweepData"></param>
        /// <param name="nominalSourceLevel"></param>
        /// <param name="vcmOffset"></param>
        /// <returns></returns>
        public static MzAnalysisResults Min_point_ClosetoZero(ILMZSweepResult sweepData, double nominalSourceLevel, double vcmOffset)
        {
            double[] diffDriveArrayPlus;
            double[] diffDriveArrayMinus;
            double MinVoltageCloseToZero = 999;

            MzAnalysisResults mzAnalysis = new MzAnalysisResults();

            if (sweepData.Type == SweepType.DifferentialVoltage)
            {
                diffDriveArrayPlus = sweepData.SweepData[ILMZSweepDataType.LeftArmModBias_V];
                diffDriveArrayMinus = sweepData.SweepData[ILMZSweepDataType.RightArmModBias_V];
            }
            else if (sweepData.Type == SweepType.DifferentialCurrent)
            {
                diffDriveArrayPlus = sweepData.SweepData[ILMZSweepDataType.LeftArmImb_A];
                diffDriveArrayMinus = sweepData.SweepData[ILMZSweepDataType.RightArmImb_A];
            }
            else
            {
                throw new ArgumentException("Sweep type isn't differential: " + sweepData.Type);
            }
            // sanity check nominal source level to 2 sig figs to avoid major problems
            double nominalSourceLevelCalc = (diffDriveArrayMinus[0] + diffDriveArrayPlus[0]) / 2;//jack.zhang
            if (Math.Abs(nominalSourceLevel - nominalSourceLevelCalc) > 0.1)
            {
                string errMsg = string.Format("Nominal source level doesn't match data: Specified={0}, From Sweep={1}",
                    nominalSourceLevel, nominalSourceLevelCalc);
                throw new ArgumentException(errMsg);
            }
            // subtract the arrays for analysis
            double[] diffDriveArray =
                Alg_ArrayFunctions.SubtractArrays(diffDriveArrayPlus, diffDriveArrayMinus);
            double[] powerArray = sweepData.SweepData[ILMZSweepDataType.FibrePower_mW];
            Bookham.TestLibrary.Algorithms.Alg_MZLVMinMax.LVMinMax abc = Alg_MZLVMinMax.MZLVMinMax(diffDriveArray, powerArray, false);
            foreach (double index in abc.VoltagesForAllValleys())
            {
                MinVoltageCloseToZero = Math.Abs(index) / index * Math.Min(Math.Abs(MinVoltageCloseToZero), Math.Abs(index));
            }
            CalcDifferentialArmValues(MinVoltageCloseToZero, nominalSourceLevel, out mzAnalysis.Min_SrcL, out mzAnalysis.Min_SrcR);
            return mzAnalysis;
        }
        /// <summary>
        /// A container for MZ data which may be useful for interpolation across ITU channels
        /// </summary>
        public struct MzChannelData
        {
            /// <summary>
            /// The ITU channel index
            /// </summary>
            public int ItuChannelIndex;
            /// <summary>
            /// VPI calculated at this channel
            /// </summary>
            public double Vpi_V;
            /// <summary>
            /// Differential Iimbalance calculated at this channel
            /// </summary>      
            public double Icm_mA;
            /// <summary>
            /// Voffset calculated at this channel
            /// </summary>
            public double Voffset_V;
            /// <summary>
            /// recalculated VCM
            /// </summary>
            public double Vcm_V;
            /// <summary>
            /// Gradient and offset for VPI vs VCM
            /// </summary>
            public StraightLineFit VpiFit;
            /// <summary>
            /// Gradient and offset for Voffset vs VCM 
            /// </summary>
            public StraightLineFit VoffsetFit;
            /// <summary>
            /// Coefficients for fit of the absolute magnitude of Voffset vs VCM
            /// </summary>
            public StraightLineFit AbsVoffsetFit;
            /// <summary>
            /// Gradient and offset for Iimb vs VCM 
            /// </summary>
            public StraightLineFit IimbFit;
        }

        /// <summary>
        /// Container for measured MZ data
        /// </summary>
        public struct MzRawData
        {
            /// <summary>
            /// Vpi
            /// </summary>
            public double Vpi_V;
            /// <summary>
            /// Vcm
            /// </summary>
            public double Vcm_V;
            /// <summary>
            /// Voffset
            /// </summary>
            public double Vimb_V;
            /// <summary>
            /// Absolute magnitude of Voffset
            /// </summary>
            public double AbsVimb_V;
            /// <summary>
            /// Iimbalance
            /// </summary>
            public double Iimb_mA;
        }

        /// <summary>
        /// Container to hold the characteristics of a straight line
        /// </summary>
        public struct StraightLineFit
        {
            /// <summary>
            /// Gradient
            /// </summary>
            public double Gradient;
            /// <summary>
            /// Intercept at X=0
            /// </summary>
            public double Offset;
        }

        /// <summary>
        /// Calculate the differential Voffset given the individual L and R bias voltages
        /// </summary>
        /// <param name="mzItuAtVcmData">A structure containing MZ sweep data</param>
        /// <returns>Differential voffset</returns>
        private static double CalculateVoffset(MzItuAtVcmData mzItuAtVcmData)
        {
            double left_V = mzItuAtVcmData.GetValueDouble(MzItuAtVcmMeasurements.VquadL_V);
            double right_V = mzItuAtVcmData.GetValueDouble(MzItuAtVcmMeasurements.VquadR_V);

            return left_V - right_V;
        }

        #region Public Properties

        /// <summary>
        /// Lower limit of VCM
        /// </summary>
        public static double vcmMinLimit;
        /// <summary>
        /// Upper limit of Vcm
        /// </summary>
        public static double vcmMaxLimit;
        /// <summary>
        /// Lower limit of Vpi
        /// </summary>
        public static double vpiMinLimit;
        /// <summary>
        /// Upper limit of Vpi
        /// </summary>
        public static double vpiMaxLimit;
        /// <summary>
        /// Common mode current in mA
        /// </summary>
        public static double commonModeCurrent_mA;
        public static double margin;

        #endregion


    }
}

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Bookham.ToolKit.Mz
{
    /// <summary>
    /// ILMZSweepDataType
    /// </summary>
    public enum ILMZSweepDataType
    {
        /// <summary>Left-arm mod bias voltage</summary>
        LeftArmModBias_V,
        /// <summary>Left-arm mod bias current</summary>
        LeftArmModBias_mA,
        /// <summary>Right-arm mod bias voltage</summary>
        RightArmModBias_V,
        /// <summary>Right-arm mod bias current</summary>
        RightArmModBias_mA,
        /// <summary>Left-arm imbalance voltage</summary>
        LeftArmImb_V,
        /// <summary>Left-arm imbalance current</summary>
        LeftArmImb_A,
        /// <summary>Right-arm imbalance voltage</summary>
        RightArmImb_V,
        /// <summary>Right-arm imbalance current</summary>
        RightArmImb_A,
        /// <summary>Inline tap current</summary>
        TapInline_mA,
        /// <summary>Inline tap voltage</summary>
        TapInline_V,
        /// <summary>Complementary tap current</summary>
        TapComplementary_mA,
        /// <summary>Complementary tap voltage</summary>
        TapComplementary_V,
        LeftMinusRight,//Left minus Right, If do LV sweep, this parameter denote left bias volt-right bias volt
                                           //If do Li sweep, this parameter denote left imb current-right imb current
        /// <summary>Optical Fibre Power</summary>
        FibrePower_mW,

        // I add the follow to record DAC Value for alpha build
        /// <summary>
        /// Left imbalance current dac value
        /// </summary>
        LeftArmImb_I_Dac,
        /// <summary>
        /// Right imbalance current dac value
        /// </summary>
        RightArmImb_I_dac,

    }

    /// <summary>
    /// Sweep Type
    /// </summary>
    public enum SweepType
    {
        /// <summary>Single-ended current sweep</summary>
        SingleEndCurrent,
        /// <summary>Single-ended voltage sweep</summary>
        SingleEndVoltage,
        /// <summary>Differential current sweep</summary>
        DifferentialCurrent,
        /// <summary>Differential voltage sweep</summary>
        DifferentialVoltage
    }

    /// <summary>
    /// Source meter instruments
    /// </summary>
    public enum SourceMeter
    {
        /// <summary>The left imbalance arm.</summary>
        LeftImbArm,
        /// <summary>The right imbalance arm.</summary>
        RightImbArm,
        /// <summary>The left modulator arm</summary>
        LeftModArm,
        /// <summary>The right modulator arm.</summary>
        RightModArm,
        /// <summary>The complementary tap.</summary>
        ComplementaryTap,
        /// <summary>The inline tap.</summary>
        InlineTap
    }   

    /// <summary>
    /// A structure to contain all of the data collected from the instruments connected to the MZ
    /// </summary>
    public class ILMZSweepResult
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public ILMZSweepResult()
        {
            this.sweepData = new Dictionary<ILMZSweepDataType, double[]>();
        }

        /// <summary>
        /// Which meter (left arm only for differential)
        /// </summary>
        public SourceMeter SrcMeter;

        /// <summary>
        /// What Sweep type is this?
        /// </summary>
        public SweepType Type;

        /// <summary>
        /// Private dictionary - protects us from losing the entire dictionary!
        /// </summary>
        private Dictionary<ILMZSweepDataType, double[]> sweepData;

        /// <summary>
        /// SweepData dictionary object
        /// </summary>
        public Dictionary<ILMZSweepDataType, double[]> SweepData
        {
            get { return sweepData; }
        }

        /// <summary>
        /// MzRaw file data
        /// </summary>
        public struct MzRawData
        {
            public string[] header;
            public ArrayList[] plotData;
        }
    }                          
}

using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.ToolKit.Mz
{
    public class VcmCalculator
    {
        /// <summary>
        /// Helper class that calculates the common mode bias depth required to achieve a Vpi target within the biassing constraints.
        /// </summary>
        /// <param name="vpi1">Measured Vpi at the first condition</param>
        /// <param name="vcm1">The common mode bias at the first condition</param>
        /// <param name="vpi2">Measured Vpi at the second condition</param>
        /// <param name="vcm2">The common mode bias at the first condition</param>
        /// <param name="absMinBias_V">Apply at least this much bias at the turning points at the ends of the sweep</param>
        /// <param name="absMaxBias_V">Apply no more than this much bias at the turning points at the ends of the sweep</param>
        public VcmCalculator(double vpi1, double vcm1, double vpi2, double vcm2, double absMinBias_V, double absMaxBias_V)
        {
            this.AddData(vpi1, vcm1);
            this.AddData(vpi2, vcm2);
            this.vAbsMinBias = Math.Abs(absMinBias_V);
            this.vAbsMaxBias = Math.Abs(absMaxBias_V);
        }

        public void AddData(double vpi, double vcm)
        {
            measuredData.Add(new VcmVpi(vpi, vcm));
        }

        public double CalculateVcm(double vpiTarget)
        {
            //double bestDeltaVpi = double.MaxValue;
            double bestDeltaVpi = 999.0;
            VcmVpi point1 = new VcmVpi();
            VcmVpi point2 = new VcmVpi();

            // Find the closest two points to target
            foreach (VcmVpi dataPoint in measuredData)
            {
                double delta = Math.Abs(dataPoint.Vpi - vpiTarget);
                if (delta < bestDeltaVpi)
                {
                    bestDeltaVpi = delta;
                    point1 = dataPoint;
                }
            }
            //bestDeltaVpi = double.MaxValue;
            bestDeltaVpi = 999.0;
            foreach (VcmVpi dataPoint in measuredData)
            {
                double delta = Math.Abs(dataPoint.Vpi - vpiTarget);
                if (delta < bestDeltaVpi && !dataPoint.Equals(point1))
                {
                    bestDeltaVpi = delta;
                    point2 = dataPoint;
                }
            }

            // Calculate a linear gradient and offset of Vcm against Vpi
            double Mvpi = (point1.Vpi - point2.Vpi) / (point1.Vcm - point2.Vcm);
            // The offset that we require is Y at X=0, so swap X & Y and invert gradient
            double Cvpi = Bookham.TestLibrary.Algorithms.EstimateXAtY.Calculate(point1.Vpi, point1.Vcm, 1 / Mvpi, 0);

            // Calculate the minimum magnitude Vcm that will ensure a bias of at least VMinBias at the peak and trough bias turning points
            double minVcm = (-vAbsMinBias - (Cvpi / 4)) / (1 + (Mvpi / 4));

            // Calculate the level of Vcm that gives the target Vpi
            double targetVcm = (vpiTarget - Cvpi) / Mvpi;

            // Clamp Vcm within the constraints
            targetVcm = Math.Min(minVcm, targetVcm);
            targetVcm = Math.Max(-vAbsMaxBias, targetVcm);

            return targetVcm;
        }

        public double AbsMinBias_V
        {
            get { return vAbsMinBias; }
            set { vAbsMinBias = value; }
        }

        public double AbsMaxBias_V
        {
            get { return vAbsMaxBias; }
            set { vAbsMaxBias = value; }
        }

        private double vAbsMinBias;
        private double vAbsMaxBias;

        private List<VcmVpi> measuredData = new List<VcmVpi>();

        private struct VcmVpi
        {
            public VcmVpi(double vpi, double vcm)
            {
                this.Vpi = vpi;
                this.Vcm = vcm;
            }
            public double Vpi;
            public double Vcm;
        }
    }

}

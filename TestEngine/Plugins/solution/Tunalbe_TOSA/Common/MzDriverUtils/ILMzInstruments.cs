using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.Instruments ;

namespace Bookham.ToolKit.Mz
{
    /// <summary>
    /// Common Mach-Zehnder instruments
    /// </summary>
    public class IlMzInstruments
    {
        /// <summary>Left Arm Modulation Voltage Source</summary>
        public InstType_TriggeredElectricalSource LeftArmMod;
        /// <summary>Right Arm Modulation Voltage Source</summary>
        public InstType_TriggeredElectricalSource RightArmMod;

        /// <summary>Left Arm Imbalance Current Source</summary>
        // Alice.Huang  2010-02-02
        // initilise it to null to allow flexiable application for Asic Source
        public InstType_TriggeredElectricalSource LeftArmImb=null;
        /// <summary>Right Arm Imbalance Current Source</summary>
        // Alice.Huang  2010-02-02
        // initilise it to null to allow flexiable application for Asic Source
        public InstType_TriggeredElectricalSource RightArmImb;

        /// <summary>
        /// FCU TO Asic intrtrument , 
        /// Fcu control Asic, 
        /// Asic source dsdbr sections        
        /// </summary>
        // added by Alice.Huang, add FCU2ASIC option for TOSA GB test
        // on 2010-02-02
        private Inst_Fcu2Asic fcu2Asic = null;
        
        /// <summary>
        ///  Identify which type of source will be used on Imbs Section;
        /// </summary>
        public EnumSourceType ImbsSourceType= EnumSourceType.ElectricalSource;

        /// <summary>Complementary Tap Photodiode + bias</summary>
        public InstType_TriggeredElectricalSource TapComplementary;
        /// <summary>Inline Tap Photodiode + bias - sometimes not present 
        /// (will be null is those circumstances)</summary>
        public InstType_TriggeredElectricalSource TapInline;

        /// <summary>Triggered power-meter used for MZ measurements</summary>
        /// 
        // Alice.Huang     2010-02-26
        // redefine   IInstType_TriggeredOpticalPowerMeter   with  InstType_OpticalPowerMeter 
        public IInstType_TriggeredOpticalPowerMeter PowerMeter;   //IInstType_TriggeredOpticalPowerMeter

        /// <summary>A boolean to select whether the inline tap is to be used. 
        /// If this is false no attempt will be made to communicate with the 
        /// inline tap instrument, all returned inline tap data will be null.</summary>
        public bool InlineTapOnline
        {
            get
            {
                return (TapInline != null);
            }
        }

        // Alice.Huang  2010-02-02
        /// <summary>
        /// FCU2ASIC Instrument, <see cref="fcu2Asic"/>
        /// </summary>
        public Inst_Fcu2Asic FCU2Asic
        {
            get { return fcu2Asic; }
            set 
            { 
                fcu2Asic = value;
                ImbsSourceType = EnumSourceType.Asic;
            }
        }

        // add this sourcetype to indetify which type of source should be used 
        /// <summary>
        ///  Enum which type of source should be implemented
        /// </summary>
        public enum EnumSourceType
        {
            /// <summary>
            /// Pin bias by sourcemeter
            /// </summary>
            ElectricalSource,
            /// <summary>
            /// pin bias by Asic
            /// </summary>
            Asic
        }
    }
}

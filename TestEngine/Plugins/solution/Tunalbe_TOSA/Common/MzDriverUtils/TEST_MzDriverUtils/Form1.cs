using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Bookham.TestLibrary.Utilities;
using Bookham.ToolKit.Mz;

namespace TEST_MzDriverUtils
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            test();
        }

        private void ReadCSV(int xIndex, int yIndex, out double[] xData, out double[] yData, string fileName)
        {
            // Read CSV

            CsvReader reader = new CsvReader();
            string[] lineElems;
            List<double> colA = new List<double>();
            List<double> colB = new List<double>();

            reader.OpenFile(fileName);
            lineElems = reader.GetLine();

            while (lineElems != null)
            {
                // read the next line
                lineElems = reader.GetLine();
                // check for end of file
                if (lineElems == null) break;
                double result;
                if (Double.TryParse(lineElems[xIndex], out result) && Double.TryParse(lineElems[yIndex], out result))
                {
                    // Looks ok. Process the line
                    colA.Add(Convert.ToDouble(lineElems[xIndex]));
                    colB.Add(Convert.ToDouble(lineElems[yIndex]));
                }
            }

            reader.CloseFile();

            xData = colA.ToArray();
            yData = colB.ToArray();
        }

        private void test()
        {
            string testDataFolder =
                @"F:\TestEngine\TCMZ_NS_Development_1\TestEngine\Plugins\solution\TuneGoldBox\Common\MzDriverUtils\TEST_MzDriverUtils\TestData\";
            string testDataFilename = "RawDataFile.csv";

            double[] xData;
            double[] yData;
            ReadCSV(0, 1, out xData, out yData, testDataFolder + testDataFilename);

            try
            {
                MzAnalysisWrapper.MzSingleEndedAnalysisResults res = MzAnalysisWrapper.SingleEnded_NegChirp_RightArmModulatorSweep(xData, yData);
            }
            catch
            {
            }




        }
    }
}
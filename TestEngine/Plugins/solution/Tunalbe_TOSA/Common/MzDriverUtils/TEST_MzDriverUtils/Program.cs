using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Bookham.ToolKit.Mz;
using Bookham.TestLibrary.Utilities;

namespace TEST_MzDriverUtils
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
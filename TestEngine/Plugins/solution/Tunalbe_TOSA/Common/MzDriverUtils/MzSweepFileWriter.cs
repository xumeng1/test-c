using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Bookham.TestLibrary.Algorithms;
using System.Collections;
using Bookham.TestLibrary.Utilities;

namespace Bookham.ToolKit.Mz
{
    /// <summary>
    /// Utility class to write sweep MZ CSV files
    /// </summary>
    public sealed class MzSweepFileWriter
    {
        /// <summary>
        /// Private constructor 
        /// </summary>
        private MzSweepFileWriter() { }

        /// <summary>
        /// Write sweep data
        /// </summary>
        /// <param name="filename">Filename</param>
        /// <param name="sweepData">Sweep data</param>
        /// <param name="sweepsToWrite">What sweep data to write to file</param>
        public static void WriteSweepData(string filename, ILMZSweepResult sweepData,
            ILMZSweepDataType[] sweepsToWrite)
        {
            SourceMeter sourceMeter = sweepData.SrcMeter;
            int nbrColumns = sweepsToWrite.Length;
            if (sweepsToWrite == null || nbrColumns <= 0)
            {
                throw new ArgumentException("No columns to write - null or zero length!");
            }

            // sweep data to write - extract it into a list to avoid having
            // to look this up in the Dictionary (hashtable) all the time
            List<double[]> sweepDataArrays = new List<double[]>(nbrColumns);

            int nbrPoints = sweepData.SweepData[ILMZSweepDataType.FibrePower_mW].Length;
            using (StreamWriter writer = new StreamWriter(filename))
            {
                // write header and lookup the data we need to write!
                bool initColumn = true;
                foreach (ILMZSweepDataType sweep in sweepsToWrite)
                {
                    if (!initColumn)
                    {
                        writer.Write(',');
                        initColumn = false;
                    }
                    writer.Write(sweep.ToString());
                    writer.Write(',');
                    // find the array for this column and add it to our list
                    double[] array = sweepData.SweepData[sweep];
                    sweepDataArrays.Add(array);
                }
                writer.WriteLine();
                
                // write data
                for (int ii = 0; ii < nbrPoints; ii++)
                {
                    for (int jj = 0; jj < nbrColumns; jj++)
                    {
                        if (jj!=0)
                        {
                            writer.Write(',');                            
                        }
                        double point = sweepDataArrays[jj][ii];
                        writer.Write(point); 
                    }
                    writer.WriteLine();
                }
            }
        }       

        /// <summary>
        /// Write sweep data
        /// </summary>
        /// <param name="filename">Filename</param>
        /// <param name="leftArmSweep">Left arm sweep data</param>
        /// <param name="rightArmSweep">Right arm sweep data</param>
        /// <param name="otherSweepsToWrite">Other sweeps to write to file, apart from the 
        /// modulation voltage</param>
        public static void WriteStitchedSingleEndLVData(string filename,
            ILMZSweepResult leftArmSweep, ILMZSweepResult rightArmSweep,
            ILMZSweepDataType[] otherSweepsToWrite)
        {
            double[] leftArmVoltageArray = leftArmSweep.SweepData[ILMZSweepDataType.LeftArmModBias_V];
            double[] rightArmVoltageArray = rightArmSweep.SweepData[ILMZSweepDataType.RightArmModBias_V];
            // Stitch the single ended sweeps together
            double[] reversedRightArmVoltage = Alg_ArrayFunctions.ReverseArray(rightArmVoltageArray);
            double[] positiveReversedRightArmVoltage = Alg_ArrayFunctions.MultiplyEachArrayElement(reversedRightArmVoltage, -1);
            double[] bothArmsVoltage = Alg_ArrayFunctions.JoinArrays(leftArmVoltageArray, positiveReversedRightArmVoltage);

            // now deal with the other data that's requested
            List<string> otherSweepDataColumnNames = new List<string>();
            List<double[]> otherSweepData = new List<double[]>();
            foreach (ILMZSweepDataType sweepType in otherSweepsToWrite)
            {
                string sweepDataColumnName;
                double[] sweepDataLeft;
                double[] sweepDataRight;
                switch (sweepType)
                {
                    case ILMZSweepDataType.LeftArmModBias_mA:
                    case ILMZSweepDataType.RightArmModBias_mA:
                        sweepDataColumnName = "MZBias_mA";
                        sweepDataLeft = leftArmSweep.SweepData[ILMZSweepDataType.LeftArmModBias_mA];
                        sweepDataRight = rightArmSweep.SweepData[ILMZSweepDataType.RightArmModBias_mA];
                        break;
                    case ILMZSweepDataType.FibrePower_mW:
                    case ILMZSweepDataType.TapComplementary_mA:
                    case ILMZSweepDataType.TapComplementary_V:
                    case ILMZSweepDataType.TapInline_mA:
                    case ILMZSweepDataType.TapInline_V:
                        sweepDataColumnName = sweepType.ToString();
                        sweepDataLeft = leftArmSweep.SweepData[sweepType];
                        sweepDataRight = rightArmSweep.SweepData[sweepType];
                        break;
                    default:
                        throw new ArgumentException("Can't stitch single ended LV data of type: " + sweepType);
                }
                // do the join
                double[] sweepDataRightRev = Alg_ArrayFunctions.ReverseArray(sweepDataRight);
                //sweepDataRightRev = Alg_ArrayFunctions.MultiplyEachArrayElement(sweepDataRightRev, -1);
                double[] stitchedSweepData = Alg_ArrayFunctions.JoinArrays(sweepDataLeft, sweepDataRightRev);
                // add it to our collection
                otherSweepDataColumnNames.Add(sweepDataColumnName);
                otherSweepData.Add(stitchedSweepData);
            }

            int nbrOtherColumns = otherSweepDataColumnNames.Count;
            int dataLen = bothArmsVoltage.Length;

            using (StreamWriter writer = new StreamWriter(filename))
            {
                // write headings
                writer.Write("MZBias_V");
                int ii,jj;
                for (jj = 0; jj < nbrOtherColumns; jj++)
                {
                    writer.Write(',');
                    writer.Write(otherSweepDataColumnNames[jj]);
                }
                writer.WriteLine();
                
                // write data
                for (ii = 0; ii < dataLen; ii++)
                {
                    // write voltage data
                    writer.Write(bothArmsVoltage[ii]);
                    // write the other data
                    for (jj = 0; jj < nbrOtherColumns; jj++)
                    {
                        writer.Write(',');
                        writer.Write(otherSweepData[jj][ii]);
                    }
                    writer.WriteLine();
                }
            }
        }

        
        public static ILMZSweepResult.MzRawData ReadPlotData(string fileName)
        {
            string[] header;
            ArrayList[] plotData;

            using (CsvReader reader = new CsvReader())
            {
                // Read header line
                List<string[]> lines = reader.ReadFile(fileName);
                // First line is header
                header = lines[0];
                int columnCount = header[header.Length - 1].Length == 0 ? header.Length - 1 : header.Length;

                plotData = new System.Collections.ArrayList[columnCount];
                for (int column = 0; column < columnCount; column++)
                {
                    plotData[column] = new System.Collections.ArrayList(lines.Count);
                }

                // Read numeric data
                for (int lineNo = 1; lineNo < lines.Count; lineNo++)
                {
                    string[] dataLine = lines[lineNo];
                    for (int column = 0; column < plotData.Length; column++)
                    {
                        string colValue = dataLine[column];
                        double colDbl = double.Parse(colValue);
                        plotData[column].Add(double.Parse(dataLine[column]));
                    }
                }
            }

            ILMZSweepResult.MzRawData rawData = new ILMZSweepResult.MzRawData();
            rawData.header = header;
            rawData.plotData = plotData;

            return rawData;
        }
    }
}

/*
 * this dll imeplements double slope algorithm 
 * for tcmz zero chirp LI Scan
 * 
 * 
 * 
 * 
 * 
 * 
 * developed by bob.lv 
 * 2008-8-28
 *
 */

using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using Bookham.TestLibrary.Algorithms;

namespace Bookham.TestLibrary.Algorithms.DoubleSlope
{
    /// <summary>
    /// Remember the specification name we are using in the program
    /// </summary>

    public class Alg_Doubleslope
    {
        /// <summary>
        /// Initialise instruments
        /// </summary>
        /// <param name="currentArray"></param>
        /// <param name="powerArray"></param>
        /// <param name="power_in_dBm"></param>
        /// <param name="modBiasOffset_V"></param>
        /// /// <param name="positive"></param>
        public static Alg_MZAnalysis.MZAnalysis TCMZDoubleSlope(double[] currentArray, double[] powerArray, bool power_in_dBm, bool positive, double modBiasOffset_V)
        {
            Alg_MZLVMinMax.LVMinMax turningPoints = Alg_MZLVMinMax.MZLVMinMax(currentArray, powerArray, 5, power_in_dBm);
            return TCMZInternalDoubleSlope(currentArray, powerArray, turningPoints, power_in_dBm, modBiasOffset_V, positive);

        }

        /// <summary>
        /// Initialise instruments
        /// </summary>
        /// <param name="voltageArray"></param>
        /// <param name="powerArray"></param>
        /// <param name="turning_Points"></param>
        /// <param name="power_in_dBm"></param>
        /// <param name="modBiasOffset_V"></param>
        /// /// <param name="positive"></param>

        public static Alg_MZAnalysis.MZAnalysis TCMZInternalDoubleSlope(double[] voltageArray, double[] powerArray, Alg_MZLVMinMax.LVMinMax turning_Points, bool power_in_dBm, double modBiasOffset_V, bool positive)
        {
            // PRECONDITIONS
            if (voltageArray.Length != powerArray.Length)
            {
                throw new AlgorithmException("Voltage and power data must contain the same number of elements. Voltage data contains " + voltageArray.Length + " and power data contains " + powerArray.Length);
            }
            if (voltageArray.Length == 0)
            {
                throw new AlgorithmException("Voltage and power arrays are empty");
            }
            if (voltageArray[0] > voltageArray[voltageArray.Length - 1])
            {
                throw new AlgorithmException("Voltage data must be in ascending order");
            }

            // Convert power to dBm, if necessary
            double[] powerArray_dBm = null;
            if (power_in_dBm)
            {
                powerArray_dBm = (double[])powerArray.Clone();
            }
            else
            {
                powerArray_dBm = Alg_PowConvert_dB.Convert_mWtodBm(powerArray);
            }
            // max or a min at the ends of the array are not true turning points, but
            // if the scan has not collected sufficient data it is valid to use one of them for VQuad
            // however it is a bad idea to add these points when 0v is the end point of the scan
            Alg_MZLVMinMax.LVMinMax turningPoints;
            //if (voltageArray[0] != 0 && voltageArray[voltageArray.Length - 1] != 0)
            //{
            turningPoints = addArrayEndPoints(voltageArray, powerArray_dBm, turning_Points);
            //}
            //else
            //{
            //    turningPoints = turning_Points;
            //}


            Alg_MZAnalysis.MZAnalysis results = new Alg_MZAnalysis.MZAnalysis();

            double[] peakTPs = turningPoints.VoltagesForAllPeaks();
            double[] valleyTPs = turningPoints.VoltagesForAllValleys();
            if (peakTPs.Length == 0)
            {
                throw new AlgorithmException("Unable to analyse MZ data. No peaks found in data.");
            }
            if (valleyTPs.Length == 0)
            {
                throw new AlgorithmException("Unable to analyse MZ data. No valleys found in data.");
            }



            // Calculate all potential VQuad points
            ArrayList quadPointsbyInflection = new ArrayList();
            ArrayList quadPointsByPower = new ArrayList();

            if (positive)
            {

                foreach (Alg_MZLVMinMax.DataPoint minPoint in turningPoints.ValleyData)
                {
                    bool found = false;
                    int maxIndex = 0;
                    double maxPower = Double.MinValue;

                    foreach (Alg_MZLVMinMax.DataPoint maxPoint in turningPoints.PeakData)
                    {
                        // Search for a positive slope
                        if (maxPoint.Voltage > minPoint.Voltage)
                        {
                            maxIndex = maxPoint.Index;
                            maxPower = maxPoint.Power_dBm;
                            found = true;
                            break;
                        }
                    }

                    // If we found a positive slope that uses this MIN we can analyse it.
                    if (found)
                    {
                        // METHOD 1
                        // Use point of inflection to find the point at which the gradient is at a MAX
                        // Power must be in mW
                        double[] power_mW = Alg_PowConvert_dB.Convert_dBmtomW(powerArray_dBm);
                        Alg_FindPointOfInflection.PointOfInflection potentialQuadPoint = Alg_FindPointOfInflection.FindPointOfInflection(voltageArray, power_mW, minPoint.Index, maxIndex);
                        if (potentialQuadPoint.Found)
                        {
                            quadPointsbyInflection.Add(potentialQuadPoint);
                        }

                        // METHOD 2
                        // Use half power point, 3dB down from Peak
                        Alg_MZLVMinMax.DataPoint QuadPoint3dB = new Alg_MZLVMinMax.DataPoint();
                        QuadPoint3dB.Power_dBm = maxPower - 3;
                        double[] subArray_Pwr = Alg_ArrayFunctions.ExtractSubArray(powerArray_dBm, minPoint.Index, maxIndex);
                        double[] subArray_V = Alg_ArrayFunctions.ExtractSubArray(voltageArray, minPoint.Index, maxIndex);
                        // False peaks at the extremes of the sweep may not have a 3dB point
                        double minPwr = Alg_PointSearch.FindMinValueInArray(subArray_Pwr);
                        double maxPwr = Alg_PointSearch.FindMaxValueInArray(subArray_Pwr);
                        if (QuadPoint3dB.Power_dBm < minPwr || QuadPoint3dB.Power_dBm > maxPwr)
                        {
                            // Not found within data
                            QuadPoint3dB.Voltage = Double.NaN;
                        }
                        else
                        {
                            QuadPoint3dB.Voltage = XatYAlgorithm.Calculate(subArray_V, subArray_Pwr, QuadPoint3dB.Power_dBm);
                        }
                        quadPointsByPower.Add(QuadPoint3dB);
                    }
                }
            }
            else
            {

                //search for the max negative slopes
                foreach (Alg_MZLVMinMax.DataPoint maxPoint in turningPoints.PeakData)
                {
                    bool found = false;
                    int minIndex = 0;
                    double maxPower = maxPoint.Power_dBm;
                    foreach (Alg_MZLVMinMax.DataPoint minPoint in turningPoints.ValleyData)
                    {
                        if (minPoint.Voltage > maxPoint.Voltage)
                        {
                            minIndex = minPoint.Index;
                           // minPower = minPoint.Power_dBm;
                            found = true;
                            break;
                        }
                    }

                    if (found)
                    {
                        double[] power_mW = Alg_PowConvert_dB.Convert_dBmtomW(powerArray_dBm);
                        Alg_FindPointOfInflection.PointOfInflection potentialQuadPoint =
                            Alg_FindPointOfInflection.FindPointOfInflection(voltageArray, power_mW, maxPoint.Index, minIndex);
                        if (potentialQuadPoint.Found)
                        {
                            quadPointsbyInflection.Add(potentialQuadPoint);
                        }

                        // METHOD 2
                        // Use half power point, 3dB down from Peak
                        Alg_MZLVMinMax.DataPoint QuadPoint3dB = new Alg_MZLVMinMax.DataPoint();
                        QuadPoint3dB.Power_dBm = maxPower - 3;
                        double[] subArray_Pwr = Alg_ArrayFunctions.ExtractSubArray(powerArray_dBm, maxPoint.Index, minIndex);
                        double[] subArray_V = Alg_ArrayFunctions.ExtractSubArray(voltageArray, maxPoint.Index, minIndex);
                        // False peaks at the extremes of the sweep may not have a 3dB point
                        double minPwr = Alg_PointSearch.FindMinValueInArray(subArray_Pwr);
                        double maxPwr = Alg_PointSearch.FindMaxValueInArray(subArray_Pwr);
                        if (QuadPoint3dB.Power_dBm < minPwr || QuadPoint3dB.Power_dBm > maxPwr)
                        {
                            // Not found within data
                            QuadPoint3dB.Voltage = Double.NaN;
                        }
                        else
                        {
                            QuadPoint3dB.Voltage = XatYAlgorithm.Calculate(subArray_V, subArray_Pwr, QuadPoint3dB.Power_dBm);
                        }
                        quadPointsByPower.Add(QuadPoint3dB);
                    }
                }
            }

            double[] quadPointVoltages = new double[quadPointsbyInflection.Count];
            for (int i = 0; i < quadPointsbyInflection.Count; i++)
            {
                Alg_FindPointOfInflection.PointOfInflection poi = (Alg_FindPointOfInflection.PointOfInflection)quadPointsbyInflection[i];
                quadPointVoltages[i] = poi.XValue;
            }
            int closestQUADToOffsetV = Alg_ArrayFunctions.FindIndexOfNearestElement(quadPointVoltages, modBiasOffset_V);

            // Get details of the closest one to 0v
            Alg_FindPointOfInflection.PointOfInflection quadPoint = (Alg_FindPointOfInflection.PointOfInflection)quadPointsbyInflection[closestQUADToOffsetV];
            results.VImb = quadPoint.XValue;

            // Repeat for method 2 data
            // Copy the candidate quad points into an array.
            double[] quadPointVoltagesByPower = new double[quadPointsByPower.Count];
            for (int i = 0; i < quadPointsByPower.Count; i++)
            {
                Alg_MZLVMinMax.DataPoint poi = (Alg_MZLVMinMax.DataPoint)quadPointsByPower[i];
                quadPointVoltagesByPower[i] = poi.Voltage;
            }
            int closestQUADToOffsetV_byPower = Alg_ArrayFunctions.FindIndexOfNearestElement(quadPointVoltagesByPower, modBiasOffset_V);

            // Get details of the closest one to 0v
            Alg_MZLVMinMax.DataPoint quadPointByPower = (Alg_MZLVMinMax.DataPoint)quadPointsByPower[closestQUADToOffsetV_byPower];
            results.VQuad = quadPointByPower.Voltage;

            #region exceptions to the rule
            if (Double.IsNaN(results.VImb))
            {
                // if not found try another way !
                Alg_MZLVMinMax.DataPoint negSlopeMaxPoint = new Alg_MZLVMinMax.DataPoint();
                Alg_MZLVMinMax.DataPoint negSlopeMinPoint = new Alg_MZLVMinMax.DataPoint();
                Alg_MZLVMinMax.DataPoint posSlopeMaxPoint = new Alg_MZLVMinMax.DataPoint();
                double maxPower = Double.MinValue;
                bool found = false;
                foreach (Alg_MZLVMinMax.DataPoint maxPoint in turningPoints.PeakData)
                {
                    foreach (Alg_MZLVMinMax.DataPoint minPoint in turningPoints.ValleyData)
                    {
                        // Search for a negative slope
                        if (maxPoint.Voltage < minPoint.Voltage)
                        {
                            maxPower = maxPoint.Power_dBm;
                            found = true;
                            negSlopeMinPoint = minPoint;
                            negSlopeMaxPoint = maxPoint;
                            break;
                        }

                    }
                    if (found)
                        break;
                }
                // find max after this min
                if (found&&positive)
                {
                    found = false;
                    foreach (Alg_MZLVMinMax.DataPoint maxPoint in turningPoints.PeakData)
                    {
                        // Search for a next max ( positive slope )
                        if (maxPoint.Voltage > negSlopeMinPoint.Voltage)
                        {
                            found = true;
                            posSlopeMaxPoint = maxPoint;
                            break;
                        }
                    }
                }
                if (found)
                {
                    Alg_MZLVMinMax.DataPoint QuadPoint3dB = new Alg_MZLVMinMax.DataPoint();
                    QuadPoint3dB.Power_dBm = maxPower - 3;

                    double[] subArray_Pwr;
                    double[] subArray_V;
                    if (positive)
                    {
                        subArray_Pwr = Alg_ArrayFunctions.ExtractSubArray(powerArray_dBm, negSlopeMinPoint.Index, posSlopeMaxPoint.Index);
                        subArray_V = Alg_ArrayFunctions.ExtractSubArray(voltageArray, negSlopeMinPoint.Index, posSlopeMaxPoint.Index);
                    }
                    else
                    {
                        subArray_Pwr = Alg_ArrayFunctions.ExtractSubArray(powerArray_dBm, posSlopeMaxPoint.Index, negSlopeMinPoint.Index);
                        subArray_V = Alg_ArrayFunctions.ExtractSubArray(voltageArray, posSlopeMaxPoint.Index, negSlopeMinPoint.Index);
                    }
                    // Make sure there's enough power to give a 3dB point
                    double minPwr = Alg_PointSearch.FindMinValueInArray(subArray_Pwr);
                    double maxPwr = Alg_PointSearch.FindMaxValueInArray(subArray_Pwr);
                    if (QuadPoint3dB.Power_dBm < minPwr || QuadPoint3dB.Power_dBm > maxPwr)
                    {
                        // Not found within data
                        QuadPoint3dB.Voltage = Double.NaN;
                    }
                    else
                    {
                        results.VQuad = XatYAlgorithm.Calculate(subArray_V, subArray_Pwr, QuadPoint3dB.Power_dBm);
                        results.VImb = results.VQuad;
                        // The distance Max->Min = Vpi
                        // The distance Min-> 3dB = Vpi / 2
                        // Max -> 3dB = 3/2 Vpi , so we can calculate Vpi by multiplying by 2/3
                        // Waveform may stretch as we move away from 3dB, so don't just use Max - Min
                        results.Vpi = Math.Abs(results.VQuad - negSlopeMaxPoint.Voltage) * 2 / 3;
                    }

                }
            }
            #endregion

            // Find the MAX and MIN closest to Quadrature ( Point of inflection )
            if (results.VImb != Double.NaN)
            {
                Alg_MZLVMinMax.DataPoint realMax, realMin;
                realMax.Power_dBm = Double.NaN;
                realMax.Voltage = Double.NaN;
                realMin.Power_dBm = Double.NaN;
                realMin.Voltage = Double.NaN;

                double deltaV = Double.MaxValue;
                if (positive)
                {
                    foreach (Alg_MZLVMinMax.DataPoint maxPoint in turningPoints.PeakData)
                    {
                        if (maxPoint.Voltage > results.VImb && Math.Abs(maxPoint.Voltage - results.VImb) < deltaV)
                        {
                            // Pos gradient
                            deltaV = Math.Abs(maxPoint.Voltage - results.VImb);
                            realMax = maxPoint;
                        }
                    }
                    deltaV = Double.MaxValue;
                    foreach (Alg_MZLVMinMax.DataPoint minPoint in turningPoints.ValleyData)
                    {
                        if (minPoint.Voltage < results.VImb && Math.Abs(results.VImb - minPoint.Voltage) < deltaV)
                        {
                            // Pos gradient
                            deltaV = Math.Abs(minPoint.Voltage - results.VImb);
                            realMin = minPoint;
                        }
                    }
                }
                else
                {
                    foreach (Alg_MZLVMinMax.DataPoint maxPoint in turningPoints.PeakData)
                    {
                        if (maxPoint.Voltage < results.VImb && Math.Abs(results.VImb - maxPoint.Voltage) < deltaV)
                        {
                            deltaV = Math.Abs(results.VImb - maxPoint.Voltage);
                            realMax = maxPoint;
                        }
                    }
                    deltaV = Double.MaxValue;
                    foreach (Alg_MZLVMinMax.DataPoint minPoint in turningPoints.ValleyData)
                    {
                        if (minPoint.Voltage > results.VImb && Math.Abs(minPoint.Voltage - results.VImb) < deltaV)
                        {
                            deltaV = Math.Abs(minPoint.Voltage - results.VImb);
                            realMin = minPoint;
                        }
                    }
                }

                // ER & VPi
                results.ExtinctionRatio_dB = realMax.Power_dBm - realMin.Power_dBm;
                results.Vpi = Math.Abs(realMax.Voltage - realMin.Voltage);
                results.VoltageAtMax = realMax.Voltage;
                results.PowerAtMax_dBm = realMax.Power_dBm;
                results.VoltageAtMin = realMin.Voltage;
                results.PowerAtMin_dBm = realMin.Power_dBm;
            }
            else
            {
                // Just in case !
                int closestMAXToOffsetV = Alg_ArrayFunctions.FindIndexOfNearestElement(peakTPs, modBiasOffset_V);
                int closestMINToOffsetV = Alg_ArrayFunctions.FindIndexOfNearestElement(valleyTPs, modBiasOffset_V);
                // ER & VPi
                results.ExtinctionRatio_dB = turningPoints.PeakData[closestMAXToOffsetV].Power_dBm - turningPoints.ValleyData[closestMINToOffsetV].Power_dBm;
                results.Vpi = Math.Abs(turningPoints.PeakData[closestMAXToOffsetV].Voltage - turningPoints.ValleyData[closestMINToOffsetV].Voltage);
                results.VoltageAtMax = turningPoints.PeakData[closestMAXToOffsetV].Voltage;
                results.PowerAtMax_dBm = turningPoints.PeakData[closestMAXToOffsetV].Power_dBm;
                results.VoltageAtMin = turningPoints.ValleyData[closestMINToOffsetV].Voltage;
                results.PowerAtMin_dBm = turningPoints.ValleyData[closestMINToOffsetV].Power_dBm;
            }
            return results;
        }

        private static Alg_MZLVMinMax.LVMinMax addArrayEndPoints(double[] voltageArray, double[] powerArray_dBm, Alg_MZLVMinMax.LVMinMax turningPoints)
        {
            ArrayList peaks = new ArrayList(turningPoints.PeakData.Length);
            foreach (Alg_MZLVMinMax.DataPoint dataPoint in turningPoints.PeakData)
            {
                peaks.Add(dataPoint);
            }

            ArrayList valleys = new ArrayList(turningPoints.ValleyData.Length);
            foreach (Alg_MZLVMinMax.DataPoint dataPoint in turningPoints.ValleyData)
            {
                valleys.Add(dataPoint);
            }

            int lastElement = voltageArray.Length - 1;

            // End point
            Alg_MZLVMinMax.DataPoint endPoint = new Alg_MZLVMinMax.DataPoint();
            endPoint.Index = lastElement;
            endPoint.Power_dBm = powerArray_dBm[lastElement];
            endPoint.Voltage = voltageArray[lastElement];

            // Start point
            Alg_MZLVMinMax.DataPoint startPoint = new Alg_MZLVMinMax.DataPoint();
            startPoint.Index = 0;
            startPoint.Power_dBm = powerArray_dBm[0];
            startPoint.Voltage = voltageArray[0];

            // Determine gradient at each end of the data
            bool positiveSlopeAtStart = false; ;
            bool positiveSlopeAtEnd = false;

            // Check whether there are sufficient turning points detected
            if (turningPoints.PeakData.Length < 1 && turningPoints.ValleyData.Length < 1)
            {   // No peaks or valleys
                if (endPoint.Power_dBm > startPoint.Power_dBm)
                {
                    positiveSlopeAtEnd = true;
                    positiveSlopeAtStart = true;
                }
            }
            else
            {
                if (turningPoints.PeakData.Length < 1)
                {   // No peaks with a valley between means that data is "U" shaped.
                    positiveSlopeAtEnd = true;
                }
                if (turningPoints.ValleyData.Length < 1)
                {    // No valleys with a peak in the middle means that data is "^" shaped.
                    positiveSlopeAtStart = true;
                }
                // This should be the "usual" case
                if (turningPoints.PeakData.Length >= 1 && turningPoints.ValleyData.Length >= 1)
                {   // To find the sign of the gradient we can draw a straight line from the end of the array to the nearest turning point.
                    double firstTurningPoint_V = turningPoints.PeakData[0].Voltage < turningPoints.ValleyData[0].Voltage ? turningPoints.PeakData[0].Voltage : turningPoints.ValleyData[0].Voltage;
                    double lastTurningPoint_V = turningPoints.PeakData[turningPoints.PeakData.Length - 1].Voltage > turningPoints.ValleyData[turningPoints.ValleyData.Length - 1].Voltage ? turningPoints.PeakData[turningPoints.PeakData.Length - 1].Voltage : turningPoints.ValleyData[turningPoints.ValleyData.Length - 1].Voltage;

                    // Due to the way that the LeastSquaresFit works we need to check that the data point above 
                    // the fit start and stop points will result in a different point.
                    LinearLeastSquaresFit endFitCoeffs = null;
                    LinearLeastSquaresFit startFitCoeffs = null;
                    if (firstTurningPoint_V > voltageArray[0])              // This should be ok.
                        startFitCoeffs = LinearLeastSquaresFitAlgorithm.Calculate_ByXValue(voltageArray, powerArray_dBm, voltageArray[0], firstTurningPoint_V);
                    if (lastTurningPoint_V < voltageArray[lastElement - 1]) // This needs to be checked.
                        endFitCoeffs = LinearLeastSquaresFitAlgorithm.Calculate_ByXValue(voltageArray, powerArray_dBm, lastTurningPoint_V, voltageArray[lastElement]);
                    // Check gradient
                    if (endFitCoeffs != null && endFitCoeffs.Slope > 0)
                    {
                        positiveSlopeAtEnd = true;
                    }
                    if (startFitCoeffs != null && startFitCoeffs.Slope > 0)
                    {
                        positiveSlopeAtStart = true;
                    }
                }
            }



            // Deal with end point
            if (positiveSlopeAtEnd)
            {
                peaks.Add(endPoint);            // Positive gradient near end (peak)
            }
            else
            {
                valleys.Add(endPoint);          // Negative gradient near end (valley)
            }

            // Deal with start point
            if (positiveSlopeAtStart)
            {
                valleys.Insert(0, startPoint);     // Positive gradient near start. (valley)
            }
            else
            {
                peaks.Insert(0, startPoint);   // Negative gradient near start (peak)
            }

            // Create return structure
            Alg_MZLVMinMax.DataPoint[] peakData = (Alg_MZLVMinMax.DataPoint[])peaks.ToArray(typeof(Alg_MZLVMinMax.DataPoint));
            Alg_MZLVMinMax.DataPoint[] valleyData = (Alg_MZLVMinMax.DataPoint[])valleys.ToArray(typeof(Alg_MZLVMinMax.DataPoint));

            Alg_MZLVMinMax.LVMinMax returnData = new Alg_MZLVMinMax.LVMinMax();
            returnData.PeakData = peakData;
            returnData.ValleyData = valleyData;

            return returnData;
        }
    }
}

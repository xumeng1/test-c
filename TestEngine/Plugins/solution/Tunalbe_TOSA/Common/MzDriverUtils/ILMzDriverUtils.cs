using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestSolution.IlmzCommonInstrs;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestSolution.IlmzCommonData;
using Bookham.TestSolution.IlmzCommonUtils;
using Bookham.TestLibrary.Algorithms;
using Bookham.TestLibrary.Instruments;
using System.ComponentModel;

namespace Bookham.ToolKit.Mz
{
    /// <summary>
    /// This class contains methods that help to setup and collect data from the instrumentation connected to the MZ
    /// </summary>
    public class IlMzDriverUtils
    {
        #region Static functions
        /// <summary>
        /// Setup the MZ to bias and imbalance settings to give Peak transmission
        /// as specified in the MzData structure.
        /// Assumes all the current and voltages compliance levels have been setup correctly
        /// </summary>
        /// <param name="mzInstrs">MZ instruments collection</param>
        /// <param name="mzData">MZ setup data</param>
        public static void SetupMzToPeak(IlMzInstruments mzInstrs, MzData mzData)
        {
            GroundArmOutputs(mzInstrs);

            double senseRange = mzInstrs.LeftArmMod.CurrentComplianceSetPoint_Amp;
            mzInstrs.LeftArmMod.SenseCurrent(senseRange, senseRange);
            mzInstrs.RightArmMod.SenseCurrent(senseRange, senseRange);

            mzInstrs.LeftArmMod.VoltageSetPoint_Volt = mzData.LeftArmMod_Peak_V;
            mzInstrs.LeftArmMod.SourceFixedVoltage();
            mzInstrs.RightArmMod.VoltageSetPoint_Volt = mzData.RightArmMod_Peak_V;
            mzInstrs.RightArmMod.SourceFixedVoltage();
            // Alice.Huang   2010-02-02
            // add FCU2ASIC operation
            if (mzInstrs.ImbsSourceType == IlMzInstruments.EnumSourceType.ElectricalSource)
            {
                mzInstrs.LeftArmImb.CurrentSetPoint_amp = mzData.LeftArmImb_mA / 1000;
                mzInstrs.RightArmImb.CurrentSetPoint_amp = mzData.RightArmImb_mA / 1000;
            }
            else
            {
                mzInstrs.FCU2Asic.IimbLeft_mA =(float ) mzData.LeftArmImb_mA;
                mzInstrs.FCU2Asic.IimbRight_mA =(float ) mzData.RightArmImb_mA;
            }
            mzInstrs.RightArmMod.DisableTriggering();
            mzInstrs.LeftArmMod.DisableTriggering();
            mzInstrs.TapComplementary.DisableTriggering();
            mzInstrs.TapComplementary.OutputEnabled = true;
            if (mzInstrs.InlineTapOnline)
            {
                mzInstrs.TapInline.DisableTriggering();
                mzInstrs.TapInline.OutputEnabled = true;
            }

        }
        /// <summary>
        /// Setup Mz to peak via fcu2Asic , not electronic source
        /// </summary>
        /// <param name="Inst_Fcu2Asic"></param>
        /// <param name="mzData"></param>
        public static void SetupMzToPeak(Inst_Fcu2Asic Fcu2Asic , MzData mzData)
        {

            Fcu2Asic.VLeftModBias_Volt = (float)mzData.LeftArmMod_Peak_V;
            Fcu2Asic.VRightModBias_Volt=(float)mzData.RightArmMod_Peak_V;
            Fcu2Asic.IimbLeft_mA = (float)mzData.LeftArmImb_mA;
            Fcu2Asic.IimbRight_mA=(float)mzData.RightArmImb_mA;
            
        }
        /// <summary>
        /// Setup Mz to Quad via fcu2Asic , not electronic source, Echo new added at 12-21-2010
        /// </summary>
        /// <param name="Inst_Fcu2Asic"></param>
        /// <param name="mzData"></param> 
        public static void SetupMzToQuad(Inst_Fcu2Asic Fcu2Asic, MzData mzData)
        {

            Fcu2Asic.VLeftModBias_Volt = (float)mzData.LeftArmMod_Quad_V;
            Fcu2Asic.VRightModBias_Volt = (float)mzData.RightArmMod_Quad_V;
            Fcu2Asic.IimbLeft_mA = (float)mzData.LeftArmImb_mA;
            Fcu2Asic.IimbRight_mA = (float)mzData.RightArmImb_mA;

        }

        public static void SetupMzToTrough(Inst_Fcu2Asic Fcu2Asic, MzData mzData)
        {

            Fcu2Asic.VLeftModBias_Volt = (float)mzData.LeftArmMod_Min_V;
            Fcu2Asic.VRightModBias_Volt = (float)mzData.RightArmMod_Min_V;
            Fcu2Asic.IimbLeft_mA = (float)mzData.LeftArmImb_mA;
            Fcu2Asic.IimbRight_mA = (float)mzData.RightArmImb_mA;

        }


        /// <summary>
        /// Setup the MZ to bias and imbalance settings to give Minima transmission
        /// as specified in the MzData structure.
        /// Assumes all the current and voltages compliance levels have been setup correctly
        /// </summary>
        /// <param name="mzInstrs">MZ instruments collection</param>
        /// <param name="mzData">MZ setup data</param>
        public static void SetupMzToTrough(IlMzInstruments mzInstrs, MzData mzData)
        {
            GroundArmOutputs(mzInstrs);

            double senseRange = mzInstrs.LeftArmMod.CurrentComplianceSetPoint_Amp;
            mzInstrs.LeftArmMod.SenseCurrent(senseRange, senseRange);
            mzInstrs.RightArmMod.SenseCurrent(senseRange, senseRange);

            mzInstrs.LeftArmMod.VoltageSetPoint_Volt = mzData.LeftArmMod_Min_V;
            mzInstrs.RightArmMod.VoltageSetPoint_Volt = mzData.RightArmMod_Min_V;
            // Alice.Huang   2010-02-02
            // add FCU2ASIC operation
            if (mzInstrs.ImbsSourceType == IlMzInstruments.EnumSourceType.ElectricalSource)
            {
                mzInstrs.LeftArmImb.CurrentSetPoint_amp = mzData.LeftArmImb_mA / 1000;
                mzInstrs.RightArmImb.CurrentSetPoint_amp = mzData.RightArmImb_mA / 1000;
            }
            else
            {
                mzInstrs.FCU2Asic.IimbLeft_mA = (float ) mzData.LeftArmImb_mA;
                mzInstrs.FCU2Asic.IimbRight_mA =(float ) mzData.RightArmImb_mA;
            }
            mzInstrs.RightArmMod.DisableTriggering();
            mzInstrs.LeftArmMod.DisableTriggering();
            mzInstrs.TapComplementary.DisableTriggering();
            mzInstrs.TapComplementary.OutputEnabled = true;
            if (mzInstrs.InlineTapOnline)
            {
                mzInstrs.TapInline.DisableTriggering();
                mzInstrs.TapInline.OutputEnabled = true;
            }
        }

        /// <summary>
        /// Setup the MZ to bias and imbalance settings to give Quadrature point
        /// as specified in the MzData structure.
        /// Assumes all the current and voltages compliance levels have been setup correctly
        /// </summary>
        /// <param name="mzInstrs">MZ instruments collection</param>
        /// <param name="mzData">MZ setup data</param>
        public static void SetupMzToQuad(IlMzInstruments mzInstrs, MzData mzData)
        {
            GroundArmOutputs(mzInstrs);

            double senseRange = mzInstrs.LeftArmMod.CurrentComplianceSetPoint_Amp;
            mzInstrs.LeftArmMod.SenseCurrent(senseRange, senseRange);
            mzInstrs.RightArmMod.SenseCurrent(senseRange, senseRange);

            mzInstrs.LeftArmMod.VoltageSetPoint_Volt = mzData.LeftArmMod_Quad_V;
            mzInstrs.RightArmMod.VoltageSetPoint_Volt = mzData.RightArmMod_Quad_V;
            // Alice.Huang   2010-02-02
            // add FCU2ASIC operation
            if (mzInstrs.ImbsSourceType == IlMzInstruments.EnumSourceType.ElectricalSource)
            {
                mzInstrs.LeftArmImb.CurrentSetPoint_amp = mzData.LeftArmImb_mA / 1000;
                mzInstrs.RightArmImb.CurrentSetPoint_amp = mzData.RightArmImb_mA / 1000;
            }
            else
            {
                mzInstrs.FCU2Asic.IimbLeft_mA = (float ) mzData.LeftArmImb_mA;
                mzInstrs.FCU2Asic.IimbRight_mA = (float ) mzData.RightArmImb_mA;
            }
            mzInstrs.TapComplementary.DisableTriggering();
            mzInstrs.TapComplementary.OutputEnabled = true;
            if (mzInstrs.InlineTapOnline)
            {
                mzInstrs.TapInline.DisableTriggering();
                mzInstrs.TapInline.OutputEnabled = true;
            }
        }

        /// <summary>
        /// Setup the MZ to bias and imbalance settings
        /// Assumes all the current and voltages compliance levels have been setup correctly
        /// </summary>
        /// <param name="mzInstrs">MZ instruments collection</param>
        /// <param name="leftArmMod_V">MZ setup data - leftArmMod_V</param>
        /// <param name="rightArmMod_V">MZ setup data - rightArmMod_V</param>
        /// <param name="leftArmImb_mA">MZ setup data - leftArmImb_mA</param>
        /// <param name="rightArmImb_mA">MZ setup data - rightArmImb_mA</param>
        public static void SetupMz(IlMzInstruments mzInstrs, double leftArmMod_V, double rightArmMod_V,
            double leftArmImb_mA, double rightArmImb_mA)
        {
            GroundArmOutputs(mzInstrs);

            double senseRange = mzInstrs.LeftArmMod.CurrentComplianceSetPoint_Amp;
            mzInstrs.LeftArmMod.SenseCurrent(senseRange, senseRange);
            mzInstrs.RightArmMod.SenseCurrent(senseRange, senseRange);

            mzInstrs.LeftArmMod.VoltageSetPoint_Volt = leftArmMod_V;
            mzInstrs.RightArmMod.VoltageSetPoint_Volt = rightArmMod_V;
            // Alice.Huang   2010-02-02
            // add FCU2ASIC operation
            if (mzInstrs.ImbsSourceType == IlMzInstruments.EnumSourceType.ElectricalSource)
            {
                mzInstrs.LeftArmImb.CurrentSetPoint_amp = leftArmImb_mA / 1000;
                mzInstrs.RightArmImb.CurrentSetPoint_amp = rightArmImb_mA / 1000;
            }
            else
            {
                mzInstrs.FCU2Asic.IimbRight_mA = (float ) rightArmImb_mA;
                mzInstrs.FCU2Asic.IimbLeft_mA =(float ) leftArmImb_mA;
            }
            mzInstrs.TapComplementary.DisableTriggering();
            mzInstrs.TapComplementary.OutputEnabled = true;
            if (mzInstrs.InlineTapOnline)
            {
                mzInstrs.TapInline.DisableTriggering();
                mzInstrs.TapInline.OutputEnabled = true;
            }
        }

        #endregion

        /// <summary>
        /// Class Constructor.
        /// </summary>
        /// <param name="mzInstruments">A collection of MZ instruments</param>
        public IlMzDriverUtils(IlMzInstruments mzInstruments)
        {
            this.MzInstrs = mzInstruments;

            this.numberOfAverages = 0;
            this.integrationRate = 1;
            this.averagingTime = 0.01;
            this.voltageCompliance = 1;
            this.voltageRange = 1;

            this.triggerLineOut = 1;
            this.triggerLineIn = 2;
            this.triggerLineUnused = 3;
        }
        /// <summary>
        /// Setup the MZ to bias and imbalance settings to give Peak transmission
        /// as specified in the MzData structure.
        /// Assumes all the current and voltages compliance levels have been setup correctly
        /// </summary>
        /// <param name="mzData">MZ setup data</param>
        public void SetupMzToPeak(MzData mzData)
        {
            IlMzDriverUtils.SetupMzToPeak(MzInstrs, mzData);
        }

        /// <summary>
        /// Setup the MZ to bias and imbalance settings to give Minima transmission
        /// as specified in the MzData structure.
        /// Assumes all the current and voltages compliance levels have been setup correctly
        /// </summary>
        /// <param name="mzData">MZ setup data</param>
        public void SetupMzToTrough(MzData mzData)
        {
            IlMzDriverUtils.SetupMzToTrough(MzInstrs, mzData);
        }

        /// <summary>
        /// Setup the MZ to bias and imbalance settings to give Quadrature point
        /// as specified in the MzData structure.
        /// Assumes all the current and voltages compliance levels have been setup correctly
        /// </summary>
        /// <param name="mzData">MZ setup data</param>
        public void SetupMzToQuad(MzData mzData)
        {
            IlMzDriverUtils.SetupMzToQuad(MzInstrs, mzData);
        }

        /// <summary>
        /// Setup the MZ to bias and imbalance settings
        /// Assumes all the current and voltages compliance levels have been setup correctly
        /// </summary>
        /// <param name="leftArmMod_V">MZ setup data - leftArmMod_V</param>
        /// <param name="rightArmMod_V">MZ setup data - rightArmMod_V</param>
        /// <param name="leftArmImb_mA">MZ setup data - leftArmImb_mA</param>
        /// <param name="rightArmImb_mA">MZ setup data - rightArmImb_mA</param>
        public void SetupMz(double leftArmMod_V, double rightArmMod_V,
            double leftArmImb_mA, double rightArmImb_mA)
        {
            IlMzDriverUtils.SetupMz(MzInstrs, leftArmMod_V, rightArmMod_V, leftArmImb_mA, rightArmImb_mA);
        }

        /// <summary>
        /// Set or get the input trigger line used by all sourcemeters
        /// </summary>
        public int TriggerLineIn
        {
            get { return triggerLineIn; }
            set { triggerLineIn = value; }
        }

        /// <summary>
        /// Set or get the output trigger line used by all sourcemeters
        /// </summary>
        public int TriggerLineOut
        {
            get { return triggerLineOut; }
            set { triggerLineOut = value; }
        }

        /// <summary>
        /// Set or get the value of an unused trigger line used by all sourcemeters
        /// </summary>
        public int TriggerLineUnused
        {
            get { return triggerLineUnused; }
            set { triggerLineUnused = value; }
        }

        /// <summary>
        /// Sets parameters controlling sweep rates on all instruments.
        /// </summary>
        /// <param name="numberOfAverages">The number of averages for the sourcemeters.</param>
        /// <param name="integrationRate">The integration rate for the sourcemeters.</param>
        /// <param name="sourceMeasureDelay">The delay between sourcing and measuring.</param>
        /// <param name="powerMeterAveragingTime">The averaging time for the optical power meter.</param>
        public void SetMeasurementAccuracy(int numberOfAverages, double integrationRate, 
                            double sourceMeasureDelay, double powerMeterAveragingTime)
        {
            this.numberOfAverages = numberOfAverages;
            this.integrationRate = integrationRate;
            this.averagingTime = powerMeterAveragingTime; // This is also used by the power meter driver
            double shortDelay = sourceMeasureDelay * 0.75;
            // Reduced source-measure delay
            this.MzInstrs.LeftArmMod.SetMeasurementAccuracy(false, shortDelay, numberOfAverages, integrationRate);
            this.MzInstrs.RightArmMod.SetMeasurementAccuracy(false, shortDelay, numberOfAverages, integrationRate);

            // Alice.Huang  2010-02-02
            // if FCU2ASIC, it don't need to initialise the Imbs sources
            if (this.MzInstrs.ImbsSourceType == IlMzInstruments.EnumSourceType.ElectricalSource)
            {
                this.MzInstrs.LeftArmImb.SetMeasurementAccuracy(false, shortDelay, numberOfAverages, integrationRate);
                this.MzInstrs.RightArmImb.SetMeasurementAccuracy(false, shortDelay, numberOfAverages, integrationRate);
            }
            if (this.MzInstrs.InlineTapOnline)
                this.MzInstrs.TapInline.SetMeasurementAccuracy(false, shortDelay, numberOfAverages, integrationRate);

            // Full source-measure delay
            this.MzInstrs.TapComplementary.SetMeasurementAccuracy(false, sourceMeasureDelay, numberOfAverages, integrationRate);
        }

        /// <summary>
        /// Sets all output levels to zero and enables outputs.
        /// </summary>
        public static void GroundAllOutputs(IlMzInstruments mzInstrs)
        {
            // Current sources

            // Alice.Huang  2010-02-02
            // Add FCU2ASIC OPERATION
            if (mzInstrs.ImbsSourceType == IlMzInstruments.EnumSourceType.ElectricalSource)
            {
                SetCurrent(mzInstrs.LeftArmImb, 0);
                SetCurrent(mzInstrs.RightArmImb, 0);
            }
            else
            {
                mzInstrs.FCU2Asic.IimbLeft_mA = 0;
                mzInstrs.FCU2Asic.IimbRight_mA = 0;
            }
            // Voltage sources
            SetVoltage(mzInstrs.LeftArmMod, 0);
            SetVoltage(mzInstrs.RightArmMod, 0);
            //mzInstrs.TapComplementary.OutputEnabled = false;
            //SetVoltage(mzInstrs.TapComplementary, 0);
            if (mzInstrs.InlineTapOnline)
                SetVoltage(mzInstrs.TapInline, 0);
        }

        /// <summary>
        /// Sets all MZ arm output levels to zero and enables outputs.
        /// MZ taps are not changed.
        /// </summary>
        public static void GroundArmOutputs(IlMzInstruments mzInstrs)
        {
            // Current sources
            
            // Alice.Huang  2010-02-02
            // Add FCU2ASIC OPERATION
            if (mzInstrs.ImbsSourceType == IlMzInstruments.EnumSourceType.ElectricalSource)
            {
                SetCurrent(mzInstrs.LeftArmImb, 0);
                SetCurrent(mzInstrs.RightArmImb, 0);
                SetVoltage(mzInstrs.LeftArmMod, 0);
                SetVoltage(mzInstrs.RightArmMod, 0);
            }
            else
            {
                mzInstrs.FCU2Asic.IimbLeft_mA = 0;
                mzInstrs.FCU2Asic.IimbRight_mA = 0;
               
            }
            // Voltage sources
            
        }

        /// <summary>
        /// Enables the outputs on all of the sourcemeters
        /// </summary>
        /// <param name="outputOn">Set TRUE to turn the outputs on.</param>
        public void EnableAllOutputs(bool outputOn)
        {
            // Alice.Huang  2010-02-02
            // Add FCU2ASIC OPERATION
            if (this.MzInstrs.ImbsSourceType == IlMzInstruments.EnumSourceType.ElectricalSource)
            {
                MzInstrs.LeftArmImb.OutputEnabled = outputOn;
                MzInstrs.RightArmImb.OutputEnabled = outputOn;
            }
            
            MzInstrs.LeftArmMod.OutputEnabled = outputOn;
            MzInstrs.RightArmMod.OutputEnabled = outputOn;
            MzInstrs.TapComplementary.OutputEnabled = outputOn;
            if (MzInstrs.InlineTapOnline)
                MzInstrs.TapInline.OutputEnabled = outputOn;
        }
        /// <summary>
        /// set source meter autoZero status
        /// </summary>
        /// <param name="sourcemeter"></param>
        /// <param name="autoZeroOn"></param>
        public void SetAutoZero(InstType_ElectricalSource sourcemeter, bool autoZeroOn)
        {
            Inst_Ke24xx i24xx = sourcemeter as Inst_Ke24xx;
            if (i24xx != null)
                i24xx.AutoZero = autoZeroOn;
        }

        /// <summary>
        /// Reset instruments after sweep
        /// </summary>
    public void CleanupAfterSweep()
        {
            // Alice.Huang  2010-02-02
            // Add FCU2ASIC OPERATION
            if (this.MzInstrs.ImbsSourceType == IlMzInstruments.EnumSourceType.ElectricalSource)
            {
                MzInstrs.LeftArmImb.ClearSweepTriggering();
                MzInstrs.RightArmImb.ClearSweepTriggering();
            }
        
            MzInstrs.LeftArmMod.ClearSweepTriggering();
            MzInstrs.RightArmMod.ClearSweepTriggering();
            MzInstrs.TapComplementary.ClearSweepTriggering();
            if (MzInstrs.InlineTapOnline)
                MzInstrs.TapInline.ClearSweepTriggering();
        }

        /// <summary>
        /// Sets a voltage on a sourcemeter and enables the output.
        /// </summary>
        /// <param name="sourceMeter">The sourcemeter to be set.</param>
        /// <param name="voltage_V">The voltage level to set.</param>
        public void SetVoltage(SourceMeter sourceMeter, double voltage_V)
        {
            switch (sourceMeter)
            {
                case SourceMeter.LeftImbArm:
                    // Alice.Huang  2010-02-02
                    // fcu2asic can't support this fuction
                    if (this.MzInstrs.ImbsSourceType ==
                        IlMzInstruments.EnumSourceType.ElectricalSource)            
                        SetVoltage(MzInstrs.LeftArmImb, voltage_V);
                    break;
                case SourceMeter.RightImbArm:
                    // Alice.Huang  2010-02-02
                    // Add FCU2ASIC OPERATION
                    if (this.MzInstrs.ImbsSourceType == 
                        IlMzInstruments.EnumSourceType.ElectricalSource)            
                        SetVoltage(MzInstrs.RightArmImb, voltage_V);
                    break;
                case SourceMeter.LeftModArm:
                    SetVoltage(MzInstrs.LeftArmMod, voltage_V);
                    break;
                case SourceMeter.RightModArm:
                    SetVoltage(MzInstrs.RightArmMod, voltage_V);
                    break;
                case SourceMeter.ComplementaryTap:
                    SetVoltage(MzInstrs.TapComplementary, voltage_V);
                    break;
                case SourceMeter.InlineTap:
                    if (MzInstrs.InlineTapOnline)
                        SetVoltage(MzInstrs.TapInline, voltage_V);
                    break;
                default:
                    throw new InstrumentException("Unhandled sourcemeter type : " + sourceMeter.ToString());
            }
        }

        /// <summary>
        /// Sets a current on a sourcemeter and enables the output.
        /// </summary>
        /// <param name="sourceMeter">The sourcemeter to be set.</param>
        /// <param name="current_A">The current level to set</param>
        public void SetCurrent(SourceMeter sourceMeter, double current_A)
        {
            switch (sourceMeter)
            {
                case SourceMeter.LeftImbArm:
                    // Alice.Huang  2010-02-02
                    // Add FCU2ASIC OPERATION
                    if (this.MzInstrs.ImbsSourceType ==
                        IlMzInstruments.EnumSourceType.ElectricalSource)
                        SetCurrent(MzInstrs.LeftArmImb, current_A);
                    else
                        MzInstrs.FCU2Asic.IimbLeft_mA =(float) current_A * 1000;
                    break;
                case SourceMeter.RightImbArm:
                    // Alice.Huang  2010-02-02
                    // Add FCU2ASIC OPERATION
                    if (this.MzInstrs.ImbsSourceType ==
                        IlMzInstruments.EnumSourceType.ElectricalSource)
                        SetCurrent(MzInstrs.RightArmImb, current_A);
                    else
                        MzInstrs.FCU2Asic.IimbRight_mA =(float ) current_A * 1000;
                    break;
                /*case SourceMeter.LeftModArm:
                    SetCurrent(MzInstrs.LeftArmMod, current_A);
                    break;
                case SourceMeter.RightModArm:
                    SetCurrent(MzInstrs.RightArmMod, current_A);
                    break;
                case SourceMeter.ComplementaryTap:
                    SetCurrent(MzInstrs.TapComplementary, current_A);
                    break;
                case SourceMeter.InlineTap:
                    if (MzInstrs.InlineTapOnline)
                        SetCurrent(MzInstrs.TapInline, current_A);
                    break;*/ //Echo remed this block
                default:
                    throw new InstrumentException("Unhandled sourcemeter type : " + sourceMeter.ToString());
            }
        }

        /// <summary>
        /// Sets the current sense range on a sourcemeter
        /// </summary>
        /// <param name="sourceMeter">The sourcemeter to be set</param>
        /// <param name="currentRange_A">The current sense range to be set.</param>
        public void SetCurrentSenseRange(SourceMeter sourceMeter, double currentRange_A)
        {
            // This is a workaround until the driver supports SetCurrentSenseRange()
            double IsenseCompliance_A = currentRange_A;

            switch (sourceMeter)
            {
                case SourceMeter.LeftImbArm:
                    // Alice.Huang  2010-02-02
                    // Add FCU2ASIC OPERATION
                    if (this.MzInstrs.ImbsSourceType == IlMzInstruments.EnumSourceType.ElectricalSource)
                    {
                        IsenseCompliance_A = MzInstrs.LeftArmImb.CurrentComplianceSetPoint_Amp;
                        MzInstrs.LeftArmImb.SenseCurrent(IsenseCompliance_A, currentRange_A);
                    }
                    break;
                case SourceMeter.RightImbArm:
                    // Alice.Huang  2010-02-02
                    // Add FCU2ASIC OPERATION
                    if (this.MzInstrs.ImbsSourceType == IlMzInstruments.EnumSourceType.ElectricalSource)
                    {
                        IsenseCompliance_A = MzInstrs.RightArmImb.CurrentComplianceSetPoint_Amp;
                        MzInstrs.RightArmImb.SenseCurrent(IsenseCompliance_A, currentRange_A);
                    }
                    break;
                case SourceMeter.LeftModArm:
                    IsenseCompliance_A = MzInstrs.LeftArmMod.CurrentComplianceSetPoint_Amp;
                    MzInstrs.LeftArmMod.SenseCurrent(IsenseCompliance_A, currentRange_A);
                    break;
                case SourceMeter.RightModArm:
                    IsenseCompliance_A = MzInstrs.RightArmMod.CurrentComplianceSetPoint_Amp;
                    MzInstrs.RightArmMod.SenseCurrent(IsenseCompliance_A, currentRange_A);
                    break;
                case SourceMeter.ComplementaryTap:
                    IsenseCompliance_A = MzInstrs.TapComplementary.CurrentComplianceSetPoint_Amp;
                    MzInstrs.TapComplementary.SenseCurrent(IsenseCompliance_A, currentRange_A);
                    break;
                case SourceMeter.InlineTap:
                    if (MzInstrs.InlineTapOnline)
                    {
                        IsenseCompliance_A = MzInstrs.TapInline.CurrentComplianceSetPoint_Amp;
                        MzInstrs.TapInline.SenseCurrent(IsenseCompliance_A, currentRange_A);
                    }
                    break;
                default:
                    throw new InstrumentException("Unhandled sourcemeter type : " + sourceMeter.ToString());
            }
        }

        /// <summary>
        /// Sets the voltage sense range on a sourcemeter.
        /// </summary>
        /// <param name="sourceMeter">The sourcemeter to set.</param>
        /// <param name="voltageRange_V">The voltage sense range to be set.</param>
        public void SetVoltageSenseRange(SourceMeter sourceMeter, double voltageRange_V)
        {
            // TODO - implement in driver
            //switch (sourceMeter)
            //{
            //    case SourceMeter.LeftImbArm:
            //        this.mzInstrs.LeftArmImb.senseVoltageRange(voltageRange_V);
            //        break;
            //    case SourceMeter.RightImbArm:
            //        this.mzInstrs.RightArmImb.senseVoltageRange(voltageRange_V);
            //        break;
            //    case SourceMeter.LeftModArm:
            //        this.mzInstrs.LeftArmMod.senseVoltageRange(voltageRange_V);
            //        break;
            //    case SourceMeter.RightModArm:
            //        this.mzInstrs.RightArmMod.senseVoltageRange(voltageRange_V);
            //        break;
            //    case SourceMeter.ComplementaryTap:
            //        this.mzInstrs.TapComplementary.senseVoltageRange(voltageRange_V);
            //        break;
            //    case SourceMeter.InlineTap:
            //        if (mzInstrs.InlineTapOnline)
            //            mzInstrs.TapInline.senseVoltageRange(voltageRange_V);
            //        break;
            //    default:
            //        throw new InstrumentException("Unhandled sourcemeter type : " + sourceMeter.ToString());
            //}
        }
        public void AutoZeroSource(InstType_ElectricalSource sourceMeter, bool autoZeroState)
        {
            Inst_Ke24xx ke2400 = sourceMeter as Inst_Ke24xx;
            if (ke2400 != null)
            {
                ke2400.AutoZero = autoZeroState;
            }
        }
        /// <summary>
        /// Sets current compliance and sense range on all MZ arms.
        /// Sets the operating mode to source volts and measure current.
        /// </summary>
        /// <param name="currentCompliance_A">The current limit.</param>
        /// <param name="currentRange_A">The current range.</param>
        public void InitialiseMZArms(double currentCompliance_A, double currentRange_A)
        {
            this.MzInstrs.LeftArmMod.SetDefaultState();
            this.MzInstrs.RightArmMod.SetDefaultState();
            // Alice.Huang   2010-03-06
            // to insure the ke2400 to souce voltage
            
            //MzInstrs.RightArmMod.SourceFixedVoltage();
            //MzInstrs.LeftArmMod.SourceFixedVoltage();

            
           
            this.MzInstrs.LeftArmMod.InitSourceVMeasureI_MeasurementAccuracy(currentCompliance_A, currentRange_A, true, 0, numberOfAverages, integrationRate, true);
            this.MzInstrs.RightArmMod.InitSourceVMeasureI_MeasurementAccuracy(currentCompliance_A, currentRange_A, true, 0, numberOfAverages, integrationRate, true);
           
            //MzInstrs.LeftArmMod.VoltageSetPoint_Volt = 0;            
            //MzInstrs.RightArmMod.VoltageSetPoint_Volt = 0;

            // Alice.Huang  2010-02-02
            // FCU2ASIC can't take these actions
            if (this.MzInstrs.ImbsSourceType == IlMzInstruments.EnumSourceType.ElectricalSource)
            {
               
                this.MzInstrs.LeftArmImb.SetDefaultState();
                this.MzInstrs.RightArmImb.SetDefaultState();
                this.MzInstrs.LeftArmImb.InitSourceVMeasureI_MeasurementAccuracy(currentCompliance_A, currentRange_A, true, 0, numberOfAverages, integrationRate, true);
                this.MzInstrs.RightArmImb.InitSourceVMeasureI_MeasurementAccuracy(currentCompliance_A, currentRange_A, true, 0, numberOfAverages, integrationRate, true);
            }
            //else
            //{

            //    MzInstrs.FCU2Asic.IimbRight_mA = 0;
            //    MzInstrs.FCU2Asic.IimbLeft_mA = 0;
            //}
        }

        /// <summary>
        /// Sets voltage compliance and sense range on the MZ imbalance arms.
        /// Sets the operating mode to source current and measure voltage.
        /// </summary>
        /// <param name="voltageCompliance_V">The voltage compliance limit.</param>
        /// <param name="voltageRange_V">The voltage range.</param>
        public void InitialiseImbalanceArms(double voltageCompliance_V, double voltageRange_V)
        {

            // Alice.Huang  2010-02-02
            // FCU2ASIC can't take these actions
            if (MzInstrs.ImbsSourceType == IlMzInstruments.EnumSourceType.Asic) return;

            this.voltageCompliance = voltageCompliance_V;
            this.voltageRange = voltageRange_V;

            
            this.MzInstrs.LeftArmImb.SetDefaultState();
            this.MzInstrs.RightArmImb.SetDefaultState();

            this.MzInstrs.LeftArmImb.InitSourceIMeasureV_MeasurementAccuracy(
                voltageCompliance_V, voltageRange_V, true, 0,
                numberOfAverages, integrationRate, true);
            this.MzInstrs.RightArmImb.InitSourceIMeasureV_MeasurementAccuracy(
                voltageCompliance_V, voltageRange_V, true, 0,
                numberOfAverages, integrationRate, true);
            
            
        }

        //public void InitialiseImbalanceArms(double voltageCompliance_V, double voltageRange_V, double currentCompliance_A, double currentRange_A)
        //{
        //    this.voltageCompliance = voltageCompliance_V;
        //    this.voltageRange = voltageRange_V;

        //    this.MzInstrs.LeftArmImb.SetDefaultState();
        //    this.MzInstrs.RightArmImb.SetDefaultState();

        //    this.MzInstrs.LeftArmImb.CurrentComplianceSetPoint_Amp = currentCompliance_A;
        //    this.MzInstrs.RightArmImb.CurrentComplianceSetPoint_Amp = currentCompliance_A;
        //    this.MzInstrs.LeftArmImb.InitSourceVMeasureI_MeasurementAccuracy(currentCompliance_A, currentRange_A, true, 0, numberOfAverages, integrationRate, true);
        //    this.MzInstrs.RightArmImb.InitSourceVMeasureI_MeasurementAccuracy(currentCompliance_A, currentRange_A, true, 0, numberOfAverages, integrationRate, true);

        //    this.MzInstrs.LeftArmImb.InitSourceIMeasureV_MeasurementAccuracy(voltageCompliance_V, voltageRange_V, true, 0, numberOfAverages, integrationRate, true);
        //    this.MzInstrs.RightArmImb.InitSourceIMeasureV_MeasurementAccuracy(voltageCompliance_V, voltageRange_V, true, 0, numberOfAverages, integrationRate, true);
        //}

        /// <summary>
        /// Initialises the power tap instruments.
        /// </summary>
        /// <param name="currentCompliance_A">The current limit.</param>
        /// <param name="currentRange_A">The current range.</param>
        public void InitialiseTaps(double currentCompliance_A, double currentRange_A)
        {
            // Alice.Huang     2010-03-04   
            // Commented  to avoid no current was read in 2 wire mode

            //MzInstrs.TapComplementary.SetDefaultState();
            
            MzInstrs.TapComplementary.InitSourceVMeasureI_MeasurementAccuracy(
                currentCompliance_A, currentRange_A, true, 0, 
                numberOfAverages, integrationRate, true);

            if (MzInstrs.InlineTapOnline)
            {
                MzInstrs.TapInline.SetDefaultState();
                MzInstrs.TapInline.InitSourceVMeasureI_MeasurementAccuracy(
                    currentCompliance_A, currentRange_A, true, 0, 
                    numberOfAverages, integrationRate, true);
            }
        }

        /// <summary>
        /// Reads a voltage from an instrument.
        /// </summary>
        /// <param name="sourceMeter">The sourcemeter to read from.</param>
        /// <returns>The voltage value in volts</returns>
        public double ReadVoltage(SourceMeter sourceMeter)
        {
            switch (sourceMeter)
            {
                case SourceMeter.LeftImbArm:
                    // Alice.Huang   2010-02-02
                    // add FCU2ASIC operation
                    if (MzInstrs.ImbsSourceType == IlMzInstruments.EnumSourceType.ElectricalSource)
                        return MzInstrs.LeftArmImb.VoltageActual_Volt;
                    else
                        throw new InstrumentException("FCU2ASIC Can't monitor the voltage");
                case SourceMeter.RightImbArm:
                    if (MzInstrs.ImbsSourceType ==IlMzInstruments.EnumSourceType .ElectricalSource )
                        return MzInstrs.RightArmImb.VoltageActual_Volt;
                    else
                        throw new InstrumentException("FCU2ASIC Can't monitor the voltage");
                case SourceMeter.LeftModArm:
                    return MzInstrs.LeftArmMod.VoltageActual_Volt;
                case SourceMeter.RightModArm:
                    return MzInstrs.RightArmMod.VoltageActual_Volt;
                case SourceMeter.ComplementaryTap:
                    return MzInstrs.TapComplementary.VoltageActual_Volt;
                case SourceMeter.InlineTap:
                    if (MzInstrs.InlineTapOnline)
                    {
                        return MzInstrs.TapInline.VoltageActual_Volt;
                    }
                    else
                    {
                        return double.NaN;
                    }
                default:
                    throw new InstrumentException("Unhandled sourcemeter type : " + sourceMeter.ToString());
            }
        }

        /// <summary>
        /// Reads a current from an instrument.
        /// </summary>
        /// <param name="sourceMeter">The sourcemeter to read from.</param>
        /// <returns>A current in amps.</returns>
        public double ReadCurrent(SourceMeter sourceMeter)
        {
            switch (sourceMeter)
            {
                case SourceMeter.LeftImbArm:
                    // Alice.Huang  2010-02-02
                    // Add FCU2ASIC OPERATION
                    if (this.MzInstrs.ImbsSourceType ==
                        IlMzInstruments.EnumSourceType.ElectricalSource)
                        return MzInstrs.LeftArmImb.CurrentActual_amp;
                    else
                        return ((double)MzInstrs.FCU2Asic.IimbLeft_mA/1000);
                case SourceMeter.RightImbArm:
                    // Alice.Huang  2010-02-02
                    // Add FCU2ASIC OPERATION
                    if (this.MzInstrs.ImbsSourceType ==
                        IlMzInstruments.EnumSourceType.ElectricalSource)
                        return MzInstrs.RightArmImb.CurrentActual_amp;
                    else
                        return ((double)MzInstrs.FCU2Asic.IimbRight_mA/1000);
                case SourceMeter.LeftModArm:
                    return MzInstrs.LeftArmMod.CurrentActual_amp;
                case SourceMeter.RightModArm:
                    return MzInstrs.RightArmMod.CurrentActual_amp;
                case SourceMeter.ComplementaryTap:
                    return MzInstrs.TapComplementary.CurrentActual_amp;
                case SourceMeter.InlineTap:
                    if (MzInstrs.InlineTapOnline)
                    {
                        return MzInstrs.TapInline.CurrentActual_amp;
                    }
                    else
                    {
                        return double.NaN;
                    }
                default:
                    throw new InstrumentException("Unhandled sourcemeter type : " + sourceMeter.ToString());
            }
        }

        /// <summary>
        /// Reads a dark current from an instrument.
        /// </summary>
        /// <param name="sourceMeter">The sourcemeter to read from.</param>
        /// <returns>A current in amps.</returns>
        public double ReadDarkCurrent(SourceMeter sourceMeter)
        {
            InstType_TriggeredElectricalSource src;
            double iDark=0;

            switch (sourceMeter)
            {
                case SourceMeter.LeftImbArm:
                    // Alice.Huang  2010-02-02
                    // Add FCU2ASIC OPERATION
                    if (this.MzInstrs.ImbsSourceType ==
                        IlMzInstruments.EnumSourceType.ElectricalSource)
                        src = MzInstrs.LeftArmImb;
                    else
                    {
                        src = null;
                        iDark = (double)MzInstrs.FCU2Asic.IimbLeft_mA/1000;
                    }
                    break;
                case SourceMeter.RightImbArm:
                    // Alice.Huang  2010-02-02
                    // Add FCU2ASIC OPERATION
                    if (this.MzInstrs.ImbsSourceType ==
                        IlMzInstruments.EnumSourceType.ElectricalSource)
                        src = MzInstrs.RightArmImb;
                    else
                    {
                        src = null;
                        iDark = (double)MzInstrs.FCU2Asic.IimbRight_mA/1000;
                    }
                    break;
                case SourceMeter.LeftModArm:
                    src = MzInstrs.LeftArmMod;
                    break;
                case SourceMeter.RightModArm:
                    src = MzInstrs.RightArmMod;
                    break;
                case SourceMeter.ComplementaryTap:
                    src = MzInstrs.TapComplementary;
                    break;
                case SourceMeter.InlineTap:
                    if (MzInstrs.InlineTapOnline)
                    {
                        src = MzInstrs.TapInline;
                        break;
                    }
                    else
                    {
                        return double.NaN;
                    }
                default:
                    throw new InstrumentException("Unhandled sourcemeter type : " + sourceMeter.ToString());
            }

            if (src != null)
            {
                // Set measurement accuracy
                src.SetMeasurementAccuracy(true, 0, this.iDarkNumAverages, this.integrationRate, true);

                // Read current
                iDark = src.CurrentActual_amp;

                // Return instrument to how we found it
                //  Note that auto delay is left enabled as we cannot query the delay time
                src.SetMeasurementAccuracy(true, 0, this.numberOfAverages, this.integrationRate, true);
            }
            // Return current
            return iDark;
        }

        /// <summary>
        /// Performs a voltage sweep on the left modulator arm.
        /// </summary>
        /// <param name="rightModArmBias_V">Fixed right modulator arm bias voltage.</param>
        /// <param name="leftImbArmCurrent_A">Fixed left imbalance arm current.</param>
        /// <param name="rightImbArmCurrent_A">Fixed right imbalange arm current.</param>
        /// <param name="startBias_V">Start level for the sweep.</param>
        /// <param name="stopBias_V">Stop level for the sweep.</param>
        /// <param name="numberOfPoints">Number of points in the sweep.</param>
        /// <param name="inlineTap_V">Fixed inline tap bias level.</param>
        /// <param name="complementaryTap_V">Fixed complementary tap bias level.</param>
        /// <returns>The optical power data and all voltage and current data from each of the sourcemeters.</returns>
        public ILMZSweepResult LeftModArm_SingleEndedSweep(double rightModArmBias_V,
                            double leftImbArmCurrent_A, double rightImbArmCurrent_A,
                            double startBias_V, double stopBias_V, int numberOfPoints,
                            double inlineTap_V, double complementaryTap_V,bool ModArmSourceByAsic)
        {
            // Alice.Huang  2010-02-04
            // Add FCU2ASIC OPERATION
            if (this.MzInstrs.ImbsSourceType ==
                IlMzInstruments.EnumSourceType.ElectricalSource)
            {
                // Setup
                SetImbalanceVSense();
                SetupTapsForSweep(numberOfPoints, inlineTap_V, complementaryTap_V);

                // L + R imb
                MzInstrs.LeftArmImb.InitSourceIMeasureV_TriggeredFixedCurrent(leftImbArmCurrent_A, numberOfPoints, triggerLineIn, triggerLineUnused);
                MzInstrs.RightArmImb.InitSourceIMeasureV_TriggeredFixedCurrent(rightImbArmCurrent_A, numberOfPoints, triggerLineIn, triggerLineUnused);

                // Right arm
                MzInstrs.RightArmMod.InitSourceVMeasureI_TriggeredFixedVoltage(rightModArmBias_V, numberOfPoints, triggerLineIn, triggerLineUnused);

                // Drive the sweep using the left arm
                MzInstrs.LeftArmMod.VoltageSetPoint_Volt = startBias_V;
                MzInstrs.LeftArmMod.InitSourceVMeasureI_VoltageSweep(startBias_V, stopBias_V, numberOfPoints, triggerLineIn, triggerLineOut);

                return DoTheSweep(MzInstrs.LeftArmMod, SourceMeter.LeftModArm, SweepType.SingleEndVoltage,
                    numberOfPoints);
            }
            else
            {
                //because Asic can't do LV sweep, so we add  parameter "ModArmSourceByAsic" to recognize FCU source is electrical or asic
                if (!ModArmSourceByAsic) 
                // Alice.Huang  2010-02-04
                // if imb s source by FCU2ASIC, then we don't need trigger box,
                // or we can't get the imb sweep data
                //SetupTapsForSweep(1, inlineTap_V, complementaryTap_V);
                {
                    double iComplianceComp = MzInstrs.TapComplementary.CurrentComplianceSetPoint_Amp;
                    MzInstrs.TapComplementary.SenseCurrent(iComplianceComp, iComplianceComp);
                    MzInstrs.TapComplementary.InitSourceVMeasureI_TriggeredFixedVoltage(complementaryTap_V, numberOfPoints, triggerLineIn, triggerLineOut);

                    MzInstrs.FCU2Asic.IimbLeft_mA = (float)leftImbArmCurrent_A * 1000;
                    MzInstrs.FCU2Asic.IimbRight_mA = (float)rightImbArmCurrent_A * 1000;

                    //// Right arm
                    MzInstrs.RightArmMod.InitSourceVMeasureI_TriggeredFixedVoltage(rightModArmBias_V, numberOfPoints, triggerLineIn, triggerLineUnused);

                    // Drive the sweep using the left arm
                    MzInstrs.LeftArmMod.VoltageSetPoint_Volt = startBias_V;
                    MzInstrs.LeftArmMod.InitSourceVMeasureI_VoltageSweep(startBias_V, stopBias_V, numberOfPoints, triggerLineIn, triggerLineOut);

                    ILMZSweepResult sweepData = SweepAsic(MzInstrs.LeftArmMod, SourceMeter.LeftModArm, SweepType.SingleEndVoltage, numberOfPoints);
                    // add imb current reading array to sweep dat to use common fucntion to write sweep data to file
                    double[] leftImbCurs = new double[numberOfPoints];
                    double[] rightImbCurs = new double[numberOfPoints];
                    double leftCurReading = (double)MzInstrs.FCU2Asic.IimbLeft_mA / 1000;
                    double rightCurReading = (double)MzInstrs.FCU2Asic.IimbRight_mA / 1000;

                    for (int i = 0; i < numberOfPoints; i++)
                    {
                        leftImbCurs[i] = leftCurReading;
                        rightImbCurs[i] = rightCurReading;
                    }
                    sweepData.SweepData[ILMZSweepDataType.LeftArmImb_A] = leftImbCurs;
                    sweepData.SweepData[ILMZSweepDataType.RightArmImb_A] = rightImbCurs;

                    return sweepData;
                }
                else
                {// do left mod sweep via asic totally , Echo 2010-12-20
                    
                    MzInstrs.FCU2Asic.IimbLeft_mA = (float)leftImbArmCurrent_A * 1000;
                    MzInstrs.FCU2Asic.IimbRight_mA = (float)rightImbArmCurrent_A * 1000;
                    MzInstrs.FCU2Asic.VRightModBias_Volt = (float)rightModArmBias_V;
                    MzInstrs.FCU2Asic.VLeftModBias_Volt = (float)startBias_V;

                    double step_V =(double) (stopBias_V - startBias_V) / numberOfPoints;
                    MzInstrs.PowerMeter.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_mW;
                    MzInstrs.PowerMeter.EnableInputTrigger(false);
                    MzInstrs.PowerMeter.EnableOutputTrigger(false);

                    double[] power_mW = new double[numberOfPoints]; 

                    double[] right_Bias_V = new double[numberOfPoints];
                    double[] right_Bias_mA = new double[numberOfPoints];
                    double[] left_Bias_V = new double[numberOfPoints];
                    double[] left_Bias_mA = new double[numberOfPoints];
                    double[] leftImbCurs = new double[numberOfPoints];
                    double[] rightImbCurs = new double[numberOfPoints];
                    double leftCurReading = (double)MzInstrs.FCU2Asic.IimbLeft_mA / 1000;
                    double rightCurReading = (double)MzInstrs.FCU2Asic.IimbRight_mA / 1000;

                    //verify fcu is apply related current on pins.---only for debug
                    double vright = MzInstrs.FCU2Asic.VRightModBias_Volt;
                    double vleft = MzInstrs.FCU2Asic.VLeftModBias_Volt;
                    double Iimbleft = MzInstrs.FCU2Asic.IimbLeft_mA;
                    double iimbRight = MzInstrs.FCU2Asic.IimbRight_mA;
                    //end of verify fcu
                    int delaytime_ms = 1;
                    for (int i = 0; i < numberOfPoints; i++)
                    {
                        left_Bias_V[i] = startBias_V + i * step_V;
                        //MzInstrs.FCU2Asic.VLeftModBias_Volt = startBias_V + i * step_V;
                        //System.Threading.Thread.Sleep(delaytime_ms);
                        //power_mW[i] = MzInstrs.PowerMeter.ReadPower();

                        //left_Bias_V[i] = MzInstrs.FCU2Asic.VLeftModBias_Volt;

                        right_Bias_mA[i] = 0;
                        left_Bias_mA[i] = 0;
                        right_Bias_V[i] = MzInstrs.FCU2Asic.VRightModBias_Volt;
                        leftImbCurs[i] = leftCurReading;
                        rightImbCurs[i] = rightCurReading;
                    }
                    bool exchangeVolt = false;
                    if (Math.Abs(stopBias_V) < Math.Abs(startBias_V))
                    {
                        //exchange stop and start volt
                        exchangeVolt = true;
                        double temp = startBias_V;
                        startBias_V = stopBias_V;
                        stopBias_V = temp;
                    }
                    //echo new added below if clause to ensure we can get correct ctap value. 2011-02-18
                    if (opticalSwitch != null)
                    {
                        IInstType_DigitalIO ctapLine = opticalSwitch.GetDigiIoLine(IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine);//line 3
                        ctapLine.LineState = IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine_State;// true
                    }

                    ILMZSweepResult sweepData = new ILMZSweepResult();
                    sweepData.Type = SweepType.SingleEndVoltage;

                    sweepData.SweepData[ILMZSweepDataType.LeftArmModBias_V] = left_Bias_V;
                    sweepData.SweepData[ILMZSweepDataType.RightArmModBias_V] = right_Bias_V;
                  
                    sweepData.SweepData[ILMZSweepDataType.LeftArmModBias_mA] = left_Bias_mA;
                    
                    sweepData.SweepData[ILMZSweepDataType.RightArmModBias_mA] = right_Bias_mA;
                    

                    sweepData.SweepData[ILMZSweepDataType.LeftArmImb_A] = leftImbCurs;
                    sweepData.SweepData[ILMZSweepDataType.RightArmImb_A] = rightImbCurs;

                    //sweepResult.SweepData[ILMZSweepDataType.TapComplementary_mA] = right_Bias_mA;
                    sweepData.SweepData[ILMZSweepDataType.TapComplementary_V] = right_Bias_mA;

                    sweepData = SweepAsicDeviceByFCUI(LvSweepType.Left, startBias_V, stopBias_V, delaytime_ms, SweepType.DifferentialVoltage, numberOfPoints, exchangeVolt,sweepData);


                    return sweepData;
                    

                }
            }

        }


        public ILMZSweepResult LeftModArm_SingleEndedSweep_ForZCHit2(double rightModArmBias_V,
                         double leftImbArmCurrent_A, double rightImbArmCurrent_A,
                         double startBias_V, double stopBias_V, int numberOfPoints,
                         double inlineTap_V, double complementaryTap_V,bool ModArmSourceByAsic)
        {
            // Alice.Huang  2010-02-04
            // Add FCU2ASIC OPERATION
            if (this.MzInstrs.ImbsSourceType ==
                IlMzInstruments.EnumSourceType.ElectricalSource)
            {
                // Setup
                SetImbalanceVSense();
                SetupTapsForSweep(numberOfPoints, inlineTap_V, complementaryTap_V);

                // L + R imb
                MzInstrs.LeftArmImb.InitSourceIMeasureV_TriggeredFixedCurrent(leftImbArmCurrent_A, numberOfPoints, triggerLineIn, triggerLineUnused);
                MzInstrs.RightArmImb.InitSourceIMeasureV_TriggeredFixedCurrent(rightImbArmCurrent_A, numberOfPoints, triggerLineIn, triggerLineUnused);

                // Right arm
                MzInstrs.RightArmMod.InitSourceVMeasureI_TriggeredFixedVoltage(rightModArmBias_V, numberOfPoints, triggerLineIn, triggerLineUnused);

                // Drive the sweep using the left arm
                MzInstrs.LeftArmMod.VoltageSetPoint_Volt = startBias_V;
                MzInstrs.LeftArmMod.InitSourceVMeasureI_VoltageSweep(startBias_V, stopBias_V, numberOfPoints, triggerLineIn, triggerLineOut);

                return DoTheSweep(MzInstrs.LeftArmMod, SourceMeter.LeftModArm, SweepType.SingleEndVoltage,
                    numberOfPoints);
            }
            else
            {
                if (!ModArmSourceByAsic)
                {
                    // Alice.Huang  2010-02-04
                    // if imb s source by FCU2ASIC, then we don't need trigger box,
                    // or we can't get the imb sweep data
                    //SetupTapsForSweep(1, inlineTap_V, complementaryTap_V);
                    double iComplianceComp = MzInstrs.TapComplementary.CurrentComplianceSetPoint_Amp;
                    MzInstrs.TapComplementary.SenseCurrent(iComplianceComp, iComplianceComp);
                    MzInstrs.TapComplementary.InitSourceVMeasureI_TriggeredFixedVoltage(complementaryTap_V, numberOfPoints, triggerLineIn, triggerLineOut);

                    MzInstrs.FCU2Asic.IimbLeft_mA = (float)leftImbArmCurrent_A * 1000;
                    MzInstrs.FCU2Asic.IimbRight_mA = (float)rightImbArmCurrent_A * 1000;

                    //**************************
                    //MzInstrs.LeftArmMod.InitSourceVMeasureI_TriggeredFixedVoltage(leftModArmBias_V, numberOfPoints, triggerLineIn, triggerLineUnused);

                    //// Drive the sweep using the right arm
                    //MzInstrs.RightArmMod.VoltageSetPoint_Volt = startBias_V;
                    //MzInstrs.RightArmMod.InitSourceVMeasureI_VoltageSweep(startBias_V, stopBias_V, numberOfPoints, triggerLineIn, triggerLineOut);

                    //ILMZSweepResult sweepResult = SweepAsic(MzInstrs.RightArmMod, SourceMeter.RightModArm, SweepType.SingleEndVoltage, numberOfPoints);


                    //**************************


                    MzInstrs.RightArmMod.InitSourceVMeasureI_TriggeredFixedVoltage(rightModArmBias_V, numberOfPoints, triggerLineIn, triggerLineOut);

                    // Drive the sweep using the left arm
                    MzInstrs.LeftArmMod.VoltageSetPoint_Volt = startBias_V;
                    MzInstrs.LeftArmMod.InitSourceVMeasureI_VoltageSweep(startBias_V, stopBias_V, numberOfPoints, triggerLineIn, triggerLineUnused);

                    ILMZSweepResult sweepData = SweepAsic(MzInstrs.LeftArmMod, SourceMeter.LeftModArm, SweepType.SingleEndVoltage, numberOfPoints);
                    // add imb current reading array to sweep dat to use common fucntion to write sweep data to file
                    double[] leftImbCurs = new double[numberOfPoints];
                    double[] rightImbCurs = new double[numberOfPoints];
                    double leftCurReading = (double)MzInstrs.FCU2Asic.IimbLeft_mA / 1000;
                    double rightCurReading = (double)MzInstrs.FCU2Asic.IimbRight_mA / 1000;

                    for (int i = 0; i < numberOfPoints; i++)
                    {
                        leftImbCurs[i] = leftCurReading;
                        rightImbCurs[i] = rightCurReading;
                    }
                    sweepData.SweepData[ILMZSweepDataType.LeftArmImb_A] = leftImbCurs;
                    sweepData.SweepData[ILMZSweepDataType.RightArmImb_A] = rightImbCurs;

                    return sweepData;
                }
                else
                {
                    //Arm source by asic

                    MzInstrs.FCU2Asic.IimbLeft_mA = (float)leftImbArmCurrent_A * 1000;
                    MzInstrs.FCU2Asic.IimbRight_mA = (float)rightImbArmCurrent_A * 1000;
                   
                    // Drive the sweep using the left arm
                    MzInstrs.FCU2Asic.VRightModBias_Volt = (float)rightModArmBias_V;
                    MzInstrs.FCU2Asic.VLeftModBias_Volt = (float)startBias_V;


                    double[] left_Bias_V = new double[numberOfPoints];
                    double[] left_Bias_mA = new double[numberOfPoints];

                    double[] right_Bias_V = new double[numberOfPoints];
                    double[] right_Bias_mA = new double[numberOfPoints];

                    double[] L_R_Bias_V = new double[numberOfPoints]; //Left-right

                    double[] leftImbCurs = new double[numberOfPoints];
                    double[] rightImbCurs = new double[numberOfPoints];

                    double leftCurReading = (double)MzInstrs.FCU2Asic.IimbLeft_mA / 1000;
                    double rightCurReading = (double)MzInstrs.FCU2Asic.IimbRight_mA / 1000;

                    MzInstrs.PowerMeter.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_mW;
                    MzInstrs.PowerMeter.EnableInputTrigger(false);
                    MzInstrs.PowerMeter.EnableOutputTrigger(false);
                    

                    if (startBias_V < MaxModBias_V)
                    {
                        double offset_v = startBias_V - MaxModBias_V;
                        startBias_V = MaxModBias_V;
                        stopBias_V = offset_v;

                    }
                    double step_V = (stopBias_V - startBias_V) / numberOfPoints;

                    for (int i = 0; i < numberOfPoints; i++)
                    {

                        left_Bias_V[i] = startBias_V + i * step_V;
                        right_Bias_V[i] = rightModArmBias_V;

                        //MzInstrs.FCU2Asic.VLeftModBias_Volt = startBias_V + i * step_V;
                        //System.Threading.Thread.Sleep(30);
                        //MzInstrs.FCU2Asic.VRightModBias_Volt = stopBias_V - i * step_V;
                        //System.Threading.Thread.Sleep(30);
                        //power_mW[i] = MzInstrs.PowerMeter.ReadPower();

                        right_Bias_mA[i] = 0;
                        left_Bias_mA[i] = 0;

                        L_R_Bias_V[i] = left_Bias_V[i] - right_Bias_V[i];

                        leftImbCurs[i] = leftCurReading;
                        rightImbCurs[i] = rightCurReading;

                    }

                    int delaytime_ms = 5;
                    bool exchangeVolt = false;
                    if (Math.Abs(stopBias_V) < Math.Abs(startBias_V))
                    {
                        //exchange stop and start volt
                        exchangeVolt = true;
                        double temp = startBias_V;
                        startBias_V = stopBias_V;
                        stopBias_V = temp;
                    }

                    //set locker port to ensure correct ctap value
                    if (opticalSwitch != null)
                    {
                        IInstType_DigitalIO ctapLine = opticalSwitch.GetDigiIoLine(IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine);
                        ctapLine.LineState = IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine_State;
                    }

                    ILMZSweepResult sweepData = new ILMZSweepResult();
                    sweepData.Type = SweepType.SingleEndVoltage;

                    sweepData.SweepData[ILMZSweepDataType.LeftArmModBias_mA] = left_Bias_mA;
                    sweepData.SweepData[ILMZSweepDataType.LeftArmModBias_V] = left_Bias_V;

                    sweepData.SweepData[ILMZSweepDataType.RightArmModBias_mA] = right_Bias_mA;
                    sweepData.SweepData[ILMZSweepDataType.RightArmModBias_V] = right_Bias_V;

                    sweepData.SweepData[ILMZSweepDataType.LeftArmImb_A] = leftImbCurs;
                    sweepData.SweepData[ILMZSweepDataType.RightArmImb_A] = rightImbCurs;

                    //sweepResult.SweepData[ILMZSweepDataType.TapComplementary_mA] = right_Bias_mA;
                    sweepData.SweepData[ILMZSweepDataType.TapComplementary_V] = right_Bias_mA;

                    sweepData.SweepData[ILMZSweepDataType.LeftMinusRight] = L_R_Bias_V;

                    sweepData = SweepAsicDeviceByFCUI(LvSweepType.Left, startBias_V, stopBias_V, delaytime_ms, SweepType.DifferentialVoltage, numberOfPoints, exchangeVolt, sweepData);

                    return sweepData;
                }
            }

        }

        
        
        
        
        /// <summary>
        /// Performs a voltage sweep on the right modulator arm.
        /// </summary>
        /// <param name="leftModArmBias_V">Fixed left modulator arm bias voltage.</param>
        /// <param name="leftImbArmCurrent_A">Fixed left imbalance arm current.</param>
        /// <param name="rightImbArmCurrent_A">Fixed right imbalange arm current.</param>
        /// <param name="startBias_V">Start level for the sweep.</param>
        /// <param name="stopBias_V">Stop level for the sweep.</param>
        /// <param name="numberOfPoints">Number of points in the sweep.</param>
        /// <param name="inlineTap_V">Fixed inline tap bias level.</param>
        /// <param name="complementaryTap_V">Fixed complementary tap bias level.</param>
        /// <returns>The optical power data and all voltage and current data from each of the sourcemeters.</returns>
        public ILMZSweepResult RightModArm_SingleEndedSweep(double leftModArmBias_V,
            double leftImbArmCurrent_A, double rightImbArmCurrent_A,
            double startBias_V, double stopBias_V, int numberOfPoints,
            double inlineTap_V, double complementaryTap_V,bool ModArmSourceByAsic)
        {

            // Alice.Huang  2010-02-04
            // Add FCU2ASIC OPERATION
            if (this.MzInstrs.ImbsSourceType ==
                IlMzInstruments.EnumSourceType.ElectricalSource)
            {
                // Setup
                SetImbalanceVSense();
                SetupTapsForSweep(numberOfPoints, inlineTap_V, complementaryTap_V);

                // L + R imb
                MzInstrs.LeftArmImb.InitSourceIMeasureV_TriggeredFixedCurrent(leftImbArmCurrent_A, numberOfPoints, triggerLineIn, triggerLineUnused);
                MzInstrs.RightArmImb.InitSourceIMeasureV_TriggeredFixedCurrent(rightImbArmCurrent_A, numberOfPoints, triggerLineIn, triggerLineUnused);

                // Left arm
                MzInstrs.LeftArmMod.InitSourceVMeasureI_TriggeredFixedVoltage(leftModArmBias_V, numberOfPoints, triggerLineIn, triggerLineUnused);

                // Drive the sweep using the right arm
                MzInstrs.RightArmMod.VoltageSetPoint_Volt = startBias_V;
                MzInstrs.RightArmMod.InitSourceVMeasureI_VoltageSweep(startBias_V, stopBias_V, numberOfPoints, triggerLineIn, triggerLineOut);

                return DoTheSweep(MzInstrs.RightArmMod, SourceMeter.RightModArm, SweepType.SingleEndVoltage,
                    numberOfPoints);
            }
            else
            {
                if (!ModArmSourceByAsic) // In this case, right bias was sourced by 2400
                {
                    // Alice.Huang  2010-02-04
                    // if imb s source by FCU2ASIC, then we don't need trigger box,
                    // or we can't get the imb sweep data
                    //SetupTapsForSweep(1, inlineTap_V, complementaryTap_V);
                    double iComplianceComp = MzInstrs.TapComplementary.CurrentComplianceSetPoint_Amp;
                    MzInstrs.TapComplementary.SenseCurrent(iComplianceComp, iComplianceComp);
                    MzInstrs.TapComplementary.InitSourceVMeasureI_TriggeredFixedVoltage(complementaryTap_V, numberOfPoints, triggerLineIn, triggerLineOut);
                    MzInstrs.FCU2Asic.IimbLeft_mA = (float)leftImbArmCurrent_A * 1000;
                    MzInstrs.FCU2Asic.IimbRight_mA = (float)rightImbArmCurrent_A * 1000;

                    //SetVoltage(MzInstrs.LeftArmMod, leftModArmBias_V);
                    //// Drive the sweep using the right arm     
                    ////System.Threading.Thread.Sleep(100);
                    //SetVoltage(MzInstrs.RightArmMod, startBias_V);
                    //return rfSingleEndedSweepWithAsic(SourceMeter.RightModArm,
                    //    startBias_V, stopBias_V, numberOfPoints, 2);
                    //// Left arm
                    MzInstrs.LeftArmMod.InitSourceVMeasureI_TriggeredFixedVoltage(leftModArmBias_V, numberOfPoints, triggerLineIn, triggerLineUnused);

                    // Drive the sweep using the right arm
                    MzInstrs.RightArmMod.VoltageSetPoint_Volt = startBias_V;
                    MzInstrs.RightArmMod.InitSourceVMeasureI_VoltageSweep(startBias_V, stopBias_V, numberOfPoints, triggerLineIn, triggerLineOut);

                    ILMZSweepResult sweepData = SweepAsic(MzInstrs.RightArmMod, SourceMeter.RightModArm, SweepType.SingleEndVoltage, numberOfPoints);
                    // add imb current reading array to sweep dat to use common fucntion to write sweep data to file
                    double[] leftImbCurs = new double[numberOfPoints];
                    double[] rightImbCurs = new double[numberOfPoints];
                    double leftCurReading = (double)MzInstrs.FCU2Asic.IimbLeft_mA / 1000;
                    double rightCurReading = (double)MzInstrs.FCU2Asic.IimbRight_mA / 1000;

                    for (int i = 0; i < numberOfPoints; i++)
                    {
                        leftImbCurs[i] = leftCurReading;
                        rightImbCurs[i] = rightCurReading;
                    }
                    sweepData.SweepData[ILMZSweepDataType.LeftArmImb_A] = leftImbCurs;
                    sweepData.SweepData[ILMZSweepDataType.RightArmImb_A] = rightImbCurs;
                    return sweepData;
                }
                else
                {
                    //In this case, right bias was sourced by Asic, Echo new added this parameter 12-21-2010
                    MzInstrs.FCU2Asic.IimbLeft_mA = (float)leftImbArmCurrent_A * 1000;
                    MzInstrs.FCU2Asic.IimbRight_mA = (float)rightImbArmCurrent_A * 1000;
                    MzInstrs.FCU2Asic.VLeftModBias_Volt = (float)-3.5;

                    double step_V = (stopBias_V - startBias_V) / numberOfPoints;
                    MzInstrs.PowerMeter.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_mW;
                    MzInstrs.PowerMeter.EnableInputTrigger(false);
                    MzInstrs.PowerMeter.EnableOutputTrigger(false);
                    
                    //double[]  power_mW= new double[numberOfPoints];
                    
                    double[] right_Bias_V = new double[numberOfPoints];
                    double[] right_Bias_mA = new double[numberOfPoints];

                    double[] left_Bias_V = new double[numberOfPoints];
                    double[] left_Bias_mA = new double[numberOfPoints];

                    double[] leftImbCurs = new double[numberOfPoints];
                    double[] rightImbCurs = new double[numberOfPoints];

                    double leftCurReading = (double)MzInstrs.FCU2Asic.IimbLeft_mA / 1000;
                    double rightCurReading = (double)MzInstrs.FCU2Asic.IimbRight_mA / 1000;

                    for (int i = 0; i < numberOfPoints; i++)
                    {
                        right_Bias_V[i] = startBias_V + i * step_V;

                        //MzInstrs.FCU2Asic.VRightModBias_Volt = startBias_V + i * step_V;
                        //System.Threading.Thread.Sleep(30);
                        //power_mW[i] = MzInstrs.PowerMeter.ReadPower();

                        right_Bias_mA[i] = 0;
                        left_Bias_mA[i] = 0;
                        left_Bias_V[i] = -3.5;
                        leftImbCurs[i] = leftCurReading;
                        rightImbCurs[i] = rightCurReading;
                    }
                    int delaytime_ms=5;
                    bool exchangeVolt = false;
                    if (Math.Abs(stopBias_V) < Math.Abs(startBias_V))
                    {
                        //exchange stop and start volt
                        exchangeVolt = true;
                        double temp = startBias_V;
                        stopBias_V = startBias_V;
                        startBias_V = temp;
                    }
                    //echo new added below if clause to ensure we can get correct ctap value. 2011-02-18
                    if (opticalSwitch != null)
                    {
                        IInstType_DigitalIO ctapLine = opticalSwitch.GetDigiIoLine(IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine);
                        ctapLine.LineState = IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine_State;
                    }

                    ILMZSweepResult sweepData = new ILMZSweepResult();
                    sweepData.Type = SweepType.SingleEndVoltage;
                    
                    sweepData.SweepData[ILMZSweepDataType.LeftArmModBias_mA] = left_Bias_mA;
                    sweepData.SweepData[ILMZSweepDataType.LeftArmModBias_V] = left_Bias_V;

                    sweepData.SweepData[ILMZSweepDataType.RightArmModBias_mA] = right_Bias_mA;
                    sweepData.SweepData[ILMZSweepDataType.RightArmModBias_V] = right_Bias_V;

                    sweepData.SweepData[ILMZSweepDataType.LeftArmImb_A] = leftImbCurs;
                    sweepData.SweepData[ILMZSweepDataType.RightArmImb_A] = rightImbCurs;

                    sweepData.SweepData[ILMZSweepDataType.TapComplementary_mA] = right_Bias_mA;
                    sweepData.SweepData[ILMZSweepDataType.TapComplementary_V] = right_Bias_mA;

                    sweepData = SweepAsicDeviceByFCUI(LvSweepType.Right, startBias_V, stopBias_V, delaytime_ms, SweepType.SingleEndVoltage, numberOfPoints, exchangeVolt,sweepData);

                    return sweepData;
                }
            }
        }

        /// <summary>
        /// Performs a differential voltage sweep on the modulator arms.
        /// </summary>
        /// <param name="leftImbArmCurrent_A">Fixed left imbalance arm current.</param>
        /// <param name="rightImbArmCurrent_A">Fixed right imbalance arm current.</param>
        /// <param name="startBias_V">Start level for the left mod arm, stop level for the right mod arm.</param>
        /// <param name="stopBias_V">Stop level for the left mod arm, start level for the right mod arm.</param>
        /// <param name="numberOfPoints">Number of points in the sweep.</param>
        /// <param name="inlineTap_V">Fixed inline tap bias level.</param>
        /// <param name="complementaryTap_V">Fixed complementary tap bias level.</param>
        /// <returns>The optical power data and all voltage and current data from each of the sourcemeters.</returns>
        public ILMZSweepResult ModArm_DifferentialSweepByTrigger(double leftImbArmCurrent_A,
                    double rightImbArmCurrent_A, double startBias_V,
                    double stopBias_V, int numberOfPoints, int delaytime_ms,
                    double inlineTap_V, double complementaryTap_V,bool ModArmSourceByAsic)
        {

            // Alice.Huang  2010-02-02
            // Add FCU2ASIC OPERATION
            if (this.MzInstrs.ImbsSourceType ==
                IlMzInstruments.EnumSourceType.ElectricalSource)
            {
                // Setup
                SetImbalanceVSense();
                SetupTapsForSweep(numberOfPoints, inlineTap_V, complementaryTap_V);

                // L + R imb
                MzInstrs.LeftArmImb.InitSourceIMeasureV_TriggeredFixedCurrent(leftImbArmCurrent_A, numberOfPoints, triggerLineIn, triggerLineUnused);
                MzInstrs.RightArmImb.InitSourceIMeasureV_TriggeredFixedCurrent(rightImbArmCurrent_A, numberOfPoints, triggerLineIn, triggerLineUnused);

                // left Mod
                MzInstrs.LeftArmMod.InitSourceVMeasureI_VoltageSweep(startBias_V, stopBias_V, numberOfPoints, triggerLineIn, triggerLineUnused);	// trigger in only

                // Drive the sweep using the right arm
                MzInstrs.RightArmMod.VoltageSetPoint_Volt = startBias_V;
                MzInstrs.RightArmMod.InitSourceVMeasureI_VoltageSweep(stopBias_V, startBias_V, numberOfPoints, triggerLineIn, triggerLineOut);

                MzInstrs.LeftArmMod.VoltageSetPoint_Volt = stopBias_V;
                return DoTheSweep(MzInstrs.RightArmMod, SourceMeter.LeftModArm, SweepType.DifferentialVoltage,
                    numberOfPoints);
            }
            else
            {
                if (!ModArmSourceByAsic)
                {
                    // Alice.Huang  2010-02-04
                    // if imb s source by FCU2ASIC, then we don't need trigger box,
                    // or we can't get the imb sweep data
                    //SetupTapsForSweep(1, inlineTap_V, complementaryTap_V);
                    double iComplianceComp = MzInstrs.TapComplementary.CurrentComplianceSetPoint_Amp;
                    MzInstrs.TapComplementary.SenseCurrent(iComplianceComp, iComplianceComp);
                    MzInstrs.TapComplementary.InitSourceVMeasureI_TriggeredFixedVoltage(complementaryTap_V,
                        numberOfPoints, triggerLineIn, triggerLineOut);
                    MzInstrs.FCU2Asic.IimbLeft_mA = (float)leftImbArmCurrent_A * 1000;
                    MzInstrs.FCU2Asic.IimbRight_mA = (float)rightImbArmCurrent_A * 1000;
                    if (startBias_V < MaxModBias_V)
                    {
                        double offset_v = startBias_V - MaxModBias_V;
                        startBias_V = MaxModBias_V;
                        stopBias_V = offset_v;
                    }

                    //SetVoltage(MzInstrs.LeftArmMod, startBias_V);
                    //SetVoltage(MzInstrs.RightArmMod, stopBias_V);
                    //EnableAllOutputs(true);
                    //return rfDefferentialSweepWithAsic(startBias_V, stopBias_V, numberOfPoints, 2);

                    //// left Mod
                    MzInstrs.LeftArmMod.InitSourceVMeasureI_VoltageSweep(startBias_V, stopBias_V, numberOfPoints, triggerLineIn, triggerLineUnused);	// trigger in only

                    // Drive the sweep using the right arm
                    MzInstrs.RightArmMod.VoltageSetPoint_Volt = stopBias_V;  //  alice for debug  startBias_V;
                    MzInstrs.RightArmMod.InitSourceVMeasureI_VoltageSweep(stopBias_V, startBias_V, numberOfPoints, triggerLineIn, triggerLineOut);

                    MzInstrs.LeftArmMod.VoltageSetPoint_Volt = stopBias_V;
                    ILMZSweepResult sweepData = SweepAsic(MzInstrs.RightArmMod, SourceMeter.LeftModArm, SweepType.DifferentialVoltage, numberOfPoints);
                    // add imb current reading array to sweep dat to use common fucntion to write sweep data to file
                    double[] leftImbCurs = new double[numberOfPoints];
                    double[] rightImbCurs = new double[numberOfPoints];
                    double leftCurReading = (double)MzInstrs.FCU2Asic.IimbLeft_mA / 1000;
                    double rightCurReading = (double)MzInstrs.FCU2Asic.IimbRight_mA / 1000;

                    for (int i = 0; i < numberOfPoints; i++)
                    {
                        leftImbCurs[i] = leftCurReading;
                        rightImbCurs[i] = rightCurReading;
                    }
                    sweepData.SweepData[ILMZSweepDataType.LeftArmImb_A] = leftImbCurs;
                    sweepData.SweepData[ILMZSweepDataType.RightArmImb_A] = rightImbCurs;
                    return sweepData;
                }
                else
                {
                    //mod arm source by asic,
                    MzInstrs.FCU2Asic.IimbLeft_mA = (float)leftImbArmCurrent_A * 1000;
                    MzInstrs.FCU2Asic.IimbRight_mA = (float)rightImbArmCurrent_A * 1000;

                    //double[] power_mW = new double[numberOfPoints];

                    double[] left_Bias_V = new double[numberOfPoints];
                    double[] left_Bias_mA = new double[numberOfPoints];

                    double[] right_Bias_V = new double[numberOfPoints];
                    double[] right_Bias_mA = new double[numberOfPoints];

                    double[] L_R_Bias_V = new double[numberOfPoints]; //Left-right

                    double[] leftImbCurs = new double[numberOfPoints];
                    double[] rightImbCurs = new double[numberOfPoints];

                    double leftCurReading = (double)MzInstrs.FCU2Asic.IimbLeft_mA / 1000;
                    double rightCurReading = (double)MzInstrs.FCU2Asic.IimbRight_mA / 1000;

                    MzInstrs.PowerMeter.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_mW;
                    MzInstrs.PowerMeter.EnableInputTrigger(false);
                    MzInstrs.PowerMeter.EnableOutputTrigger(false);
                    bool exchangeVolt = false;

                    if (startBias_V < MaxModBias_V)
                    {
                        double offset_v = startBias_V - MaxModBias_V;
                        startBias_V = MaxModBias_V;
                        stopBias_V = offset_v;
                        
                    }
                    double step_V = (stopBias_V - startBias_V) / numberOfPoints;

                    for (int i = 0; i < numberOfPoints; i++)
                    {
                        
                        left_Bias_V[i] = startBias_V + i * step_V;
                        right_Bias_V[i] = stopBias_V - i * step_V;

                        //MzInstrs.FCU2Asic.VLeftModBias_Volt = startBias_V + i * step_V;
                        //System.Threading.Thread.Sleep(30);
                        //MzInstrs.FCU2Asic.VRightModBias_Volt = stopBias_V - i * step_V;
                        //System.Threading.Thread.Sleep(30);
                        //power_mW[i] = MzInstrs.PowerMeter.ReadPower();
                       
                        right_Bias_mA[i] = 0;
                        left_Bias_mA[i] = 0;

                        L_R_Bias_V[i] = left_Bias_V[i] - right_Bias_V[i];
                        
                        leftImbCurs[i] = leftCurReading;
                        rightImbCurs[i] = rightCurReading;

                    }

                    //set locker port to ensure correct ctap value
                    if (opticalSwitch != null)
                    {
                        IInstType_DigitalIO ctapLine = opticalSwitch.GetDigiIoLine(IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine);
                        ctapLine.LineState = IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine_State;
                    }

                    ILMZSweepResult sweepData = new ILMZSweepResult();
                    sweepData.Type = SweepType.DifferentialVoltage;

                    sweepData.SweepData[ILMZSweepDataType.LeftArmModBias_mA] = left_Bias_mA;
                    sweepData.SweepData[ILMZSweepDataType.LeftArmModBias_V] = left_Bias_V;

                    sweepData.SweepData[ILMZSweepDataType.RightArmModBias_mA] = right_Bias_mA;
                    sweepData.SweepData[ILMZSweepDataType.RightArmModBias_V] = right_Bias_V;

                    sweepData.SweepData[ILMZSweepDataType.LeftArmImb_A] = leftImbCurs;
                    sweepData.SweepData[ILMZSweepDataType.RightArmImb_A] = rightImbCurs;

                    sweepData.SweepData[ILMZSweepDataType.LeftMinusRight] = L_R_Bias_V;
                    double minLevel_dBm = -50;
                    bool dataOk = false;
                    do
                    {
                        sweepData = SweepAsicDeviceByFCUI(LvSweepType.Diff, startBias_V, stopBias_V, delaytime_ms, SweepType.DifferentialVoltage, numberOfPoints, false, sweepData);
                        if (MzAnalysisWrapper.CheckForOverrange(sweepData.SweepData[ILMZSweepDataType.FibrePower_mW]))
                        {
                            dataOk = false;
                            //modify by tim at2008-09-04 for Ag8163 NaN;
                            MzInstrs.PowerMeter.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_mW;
                            MzInstrs.PowerMeter.Range = MzInstrs.PowerMeter.Range * 10;

                            if (double.IsNaN(MzInstrs.PowerMeter.Range))
                            {
                                MzInstrs.PowerMeter.Range = 1;
                            }
                        }
                        else
                        {
                            // if underrange fix the data and continue
                            dataOk = true;
                            if (MzAnalysisWrapper.CheckForUnderrange(sweepData.SweepData[ILMZSweepDataType.FibrePower_mW]))
                            {
                                sweepData.SweepData[ILMZSweepDataType.FibrePower_mW] =
                                    MzAnalysisWrapper.FixUnderRangeData(sweepData.SweepData[ILMZSweepDataType.FibrePower_mW]
                                    , minLevel_dBm);
                            }
                        }
                    }
                    while (!dataOk);
                    return sweepData;

                }
            }

        }

        /// <summary>
        /// Do LV diff Slow sweep by FCUMKI to comparer with the fast sweep by trigger. Jack.zhang 2012-05-05
        /// </summary>
        ///  <param name="leftImbArmCurrent_A">Fixed left imbalance arm current.</param>
        /// <param name="rightImbArmCurrent_A">Fixed right imbalance arm current.</param>
        /// <param name="startBias_V">Start level for the left mod arm, stop level for the right mod arm.</param>
        /// <param name="stopBias_V">Stop level for the left mod arm, start level for the right mod arm.</param>
        /// <param name="numberOfPoints">Number of points in the sweep.</param>
        /// <param name="inlineTap_V">Fixed inline tap bias level.</param>
        /// <param name="complementaryTap_V">Fixed complementary tap bias level.</param>
        /// <returns>The optical power data and all voltage and current data from each of the sourcemeters.</returns>
        public ILMZSweepResult ModArm_Differential_ForLoop_SweepbyFCUMKI(double leftImbArmCurrent_A,
            double rightImbArmCurrent_A, double startBias_V,
            double stopBias_V, int numberOfPoints, int delaytime_ms,
            double inlineTap_V, double complementaryTap_V, bool ModArmSourceByAsic)
        {
            //mod arm source by asic,
            MzInstrs.FCU2Asic.IimbLeft_mA = (float)leftImbArmCurrent_A * 1000;
            MzInstrs.FCU2Asic.IimbRight_mA = (float)rightImbArmCurrent_A * 1000;

            double[] left_Bias_V = new double[numberOfPoints];
            double[] left_Bias_mA = new double[numberOfPoints];

            double[] right_Bias_V = new double[numberOfPoints];
            double[] right_Bias_mA = new double[numberOfPoints];

            double[] L_R_Bias_V = new double[numberOfPoints]; //Left-right

            double[] leftImbCurs = new double[numberOfPoints];
            double[] rightImbCurs = new double[numberOfPoints];

            double[] power_mW = new double[numberOfPoints];
            double[] Ctap_mA = new double[numberOfPoints];

            double leftCurReading = (double)MzInstrs.FCU2Asic.IimbLeft_mA / 1000;
            double rightCurReading = (double)MzInstrs.FCU2Asic.IimbRight_mA / 1000;

            MzInstrs.PowerMeter.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_mW;
            MzInstrs.PowerMeter.Range = InstType_OpticalPowerMeter.AutoRange;

            //set locker port to ensure correct ctap value
            if (opticalSwitch != null)
            {
                IInstType_DigitalIO ctapLine = opticalSwitch.GetDigiIoLine(IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine);
                ctapLine.LineState = IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine_State;
            }

            if (startBias_V < MaxModBias_V)
            {
                double offset_v = startBias_V - MaxModBias_V;
                startBias_V = MaxModBias_V;
                stopBias_V = offset_v;

            }
            double step_V = (stopBias_V - startBias_V) / numberOfPoints;

            for (int i = 0; i < numberOfPoints; i++)
            {

                left_Bias_V[i] = startBias_V + i * step_V;
                right_Bias_V[i] = stopBias_V - i * step_V;

                MzInstrs.FCU2Asic.VLeftModBias_Volt = (float)left_Bias_V[i];
                MzInstrs.FCU2Asic.VRightModBias_Volt = (float)right_Bias_V[i];
                power_mW[i] = MzInstrs.PowerMeter.ReadPower();
                Ctap_mA[i] = MzInstrs.FCU2Asic.ICtap_mA;
                right_Bias_mA[i] = 0;
                left_Bias_mA[i] = 0;

                L_R_Bias_V[i] = left_Bias_V[i] - right_Bias_V[i];

                leftImbCurs[i] = leftCurReading;
                rightImbCurs[i] = rightCurReading;

            }

            ILMZSweepResult sweepData = new ILMZSweepResult();
            sweepData.Type = SweepType.DifferentialVoltage;

            sweepData.SweepData[ILMZSweepDataType.LeftArmModBias_mA] = left_Bias_mA;
            sweepData.SweepData[ILMZSweepDataType.LeftArmModBias_V] = left_Bias_V;

            sweepData.SweepData[ILMZSweepDataType.RightArmModBias_mA] = right_Bias_mA;
            sweepData.SweepData[ILMZSweepDataType.RightArmModBias_V] = right_Bias_V;

            sweepData.SweepData[ILMZSweepDataType.LeftArmImb_A] = leftImbCurs;
            sweepData.SweepData[ILMZSweepDataType.RightArmImb_A] = rightImbCurs;

            //sweepResult.SweepData[ILMZSweepDataType.TapComplementary_mA] = right_Bias_mA;
            sweepData.SweepData[ILMZSweepDataType.TapComplementary_V] = right_Bias_mA;

            sweepData.SweepData[ILMZSweepDataType.LeftMinusRight] = L_R_Bias_V;

            sweepData.SweepData[ILMZSweepDataType.FibrePower_mW] = power_mW;

            sweepData.SweepData[ILMZSweepDataType.TapComplementary_mA] = Ctap_mA;
            return sweepData;
        }
        /// <summary>
        /// </summary>
        /// <param name="leftModArmVoltage_V"></param>
        /// <param name="rightModArmVoltage_V"></param>
        /// <param name="startCurrent_A">Start level for the left imb arm, stop level for the right imb arm.</param>
        /// <param name="stopCurrent_A">Stop level for the left imb arm, start level for the right imb arm.</param>
        /// <param name="numberOfPoints">Number of points in the sweep.</param>
        /// <param name="inlineTap_V">Fixed inline tap bias level.</param>
        /// <param name="complementaryTap_V">Fixed complementary tap bias level.</param>
        /// <returns>The optical power data and all voltage and current data from each of the sourcemeters.</returns>
        public ILMZSweepResult LeftImbArm_SingleEndedSweep(
                    double rightModArmVoltage_V, double leftModArmVoltage_V, 
                    double startCurrent_A, double stopCurrent_A, int numberOfPoints, 
                    double inlineTap_V, double complementaryTap_V)
        {
            // Alice.Huang  2010-02-03
            // add FCU2Asic instrument functions
            //Setup Left imb
            if (MzInstrs.ImbsSourceType == IlMzInstruments.EnumSourceType.ElectricalSource)
            {
                //Setup
                SetImbalanceVSense();
                SetupTapsForSweep(numberOfPoints, inlineTap_V, complementaryTap_V);

                EnableAllOutputs(true);

                //Left +Right mod arms
                MzInstrs.LeftArmMod.InitSourceVMeasureI_TriggeredFixedVoltage(leftModArmVoltage_V, numberOfPoints, triggerLineIn, triggerLineUnused);
                MzInstrs.RightArmMod.InitSourceVMeasureI_TriggeredFixedVoltage(rightModArmVoltage_V, numberOfPoints, triggerLineIn, triggerLineUnused);

                //Setup Left imb
                //   It's safest to explicitly set the instruments to sense V here, 
                //   as any spot-measurements may have reset the instrument to read A
                MzInstrs.LeftArmImb.SenseVoltage(voltageCompliance, voltageRange);

                //Right imb
                //MzInstrs.RightArmImb.InitSourceIMeasureV_CurrentSweep(stopCurrent_A, startCurrent_A, numberOfPoints, triggerLineIn, triggerLineUnused);

                // Drive the sweep using leftImb arm
                MzInstrs.LeftArmImb.CurrentComplianceSetPoint_Amp = startCurrent_A;
                MzInstrs.LeftArmImb.InitSourceIMeasureV_CurrentSweep(startCurrent_A, stopCurrent_A, numberOfPoints, triggerLineIn, triggerLineOut);

                MzInstrs.LeftArmImb.CurrentSetPoint_amp = stopCurrent_A;
                return DoTheSweep(MzInstrs.LeftArmImb, SourceMeter.LeftImbArm, SweepType.SingleEndCurrent,
                    numberOfPoints);
            }
            else
            {
                SetupTapsForSweep(1, inlineTap_V, complementaryTap_V);
                
                MzInstrs.LeftArmMod.VoltageSetPoint_Volt = leftModArmVoltage_V;
                MzInstrs.LeftArmMod.SourceFixedVoltage();
                MzInstrs.RightArmMod.VoltageSetPoint_Volt = rightModArmVoltage_V;
                MzInstrs.RightArmMod.SourceFixedVoltage();
                MzInstrs.FCU2Asic.IimbLeft_mA =(float) stopCurrent_A * 1000;
                EnableAllOutputs(true);
                
                return ImbSingledEndedSweepWithAsic(SourceMeter.LeftImbArm,
                    startCurrent_A * 1000, stopCurrent_A * 1000, numberOfPoints, 20);
            }
        }
        /// <summary>
        /// Performs a single current sweep on the MZ imbalance control Left/Right arms.
        /// </summary>
        /// <param name="leftModArmVoltage_V">Fixed left modulator arm voltage.</param>
        /// <param name="rightModArmVoltage_V">Fixed right modulator arm voltage.</param>
        /// <param name="startCurrent_A">Start level for the left imb arm, stop level for the right imb arm.</param>
        /// <param name="stopCurrent_A">Stop level for the left imb arm, start level for the right imb arm.</param>
        /// <param name="numberOfPoints">Number of points in the sweep.</param>
        /// <param name="inlineTap_V">Fixed inline tap bias level.</param>
        /// <param name="complementaryTap_V">Fixed complementary tap bias level.</param>
        /// <returns>The optical power data and all voltage and current data from each of the sourcemeters.</returns>
        public ILMZSweepResult RightImbArm_SingleEndedSweep(
                    double leftModArmVoltage_V, double rightModArmVoltage_V,
                    double startCurrent_A, double stopCurrent_A, int numberOfPoints, 
                    double inlineTap_V, double complementaryTap_V)
        {
            
            // Alice.Huang  2010-02-03
            // add FCU2Asic instrument functions

            //Setup Right imb
            if (MzInstrs.ImbsSourceType == IlMzInstruments.EnumSourceType.ElectricalSource)
            {
                //Setup
                SetImbalanceVSense();
                SetupTapsForSweep(numberOfPoints, inlineTap_V, complementaryTap_V);
                //Left +Right mod arms
                MzInstrs.LeftArmMod.InitSourceVMeasureI_TriggeredFixedVoltage(leftModArmVoltage_V, numberOfPoints, triggerLineIn, triggerLineUnused);
                MzInstrs.RightArmMod.InitSourceVMeasureI_TriggeredFixedVoltage(rightModArmVoltage_V, numberOfPoints, triggerLineIn, triggerLineUnused);

                //Setup Right imb
                //   It's safest to explicitly set the instruments to sense V here, 
                //   as any spot-measurements may have reset the instrument to read A
                MzInstrs.RightArmImb.SenseVoltage(voltageCompliance, voltageRange);

                //Left imb
                //MzInstrs.LeftArmImb.InitSourceIMeasureV_CurrentSweep(stopCurrent_A, startCurrent_A, numberOfPoints, triggerLineIn, triggerLineUnused);

                // Drive the sweep using rightImb arm
                MzInstrs.RightArmImb.CurrentSetPoint_amp = startCurrent_A;
                MzInstrs.RightArmImb.InitSourceIMeasureV_CurrentSweep(startCurrent_A, stopCurrent_A, numberOfPoints, triggerLineIn, triggerLineOut);

                MzInstrs.RightArmImb.CurrentSetPoint_amp = stopCurrent_A;

                EnableAllOutputs(true);

                return DoTheSweep(MzInstrs.RightArmImb, SourceMeter.RightImbArm, SweepType.SingleEndCurrent,
                    numberOfPoints);
            }
            else
            {
                // Alice.Huang  2010-02-04
                // if imb s source by FCU2ASIC, then we don't need trigger box,
                // or we can't get the imb sweep data
                SetupTapsForSweep(1, inlineTap_V, complementaryTap_V);
                EnableAllOutputs(true);
                
                MzInstrs.LeftArmMod.VoltageSetPoint_Volt = leftModArmVoltage_V;
                MzInstrs.LeftArmMod.SourceFixedVoltage();
                MzInstrs.RightArmMod.VoltageSetPoint_Volt = rightModArmVoltage_V;
                MzInstrs.RightArmMod.SourceFixedVoltage();

                MzInstrs.FCU2Asic.IimbRight_mA =(float ) stopCurrent_A * 1000;
                return ImbSingledEndedSweepWithAsic(SourceMeter.RightImbArm,
                            startCurrent_A * 1000, stopCurrent_A * 1000, numberOfPoints, 20);

            }
        }
        /// Performs a differential current sweep on the MZ imbalance control arms.
        /// </summary>
        /// <param name="leftModArmVoltage_V">Fixed left modulator arm voltage.</param>
        /// <param name="rightModArmVoltage_V">Fixed right modulator arm voltage.</param>
        /// <param name="startCurrent_A">Start level for the left imb arm, stop level for the right imb arm.</param>
        /// <param name="stopCurrent_A">Stop level for the left imb arm, start level for the right imb arm.</param>
        /// <param name="numberOfPoints">Number of points in the sweep.</param>
        /// <param name="inlineTap_V">Fixed inline tap bias level.</param>
        /// <param name="complementaryTap_V">Fixed complementary tap bias level.</param>
        /// <returns>The optical power data and all voltage and current data from each of the sourcemeters.</returns>
        public ILMZSweepResult ImbArm_DifferentialSweep(
                        double leftModArmVoltage_V, double rightModArmVoltage_V,
                        double startCurrent_A, double stopCurrent_A, int numberOfPoints,int MZSourceMeasureDelay_ms,
                        double inlineTap_V, double complementaryTap_V,bool ImbArmSourceByAsic)
        {
            // Alice.Huang  2010-02-03
            // add FCU2Asic instrument functions

            // Setup L + R imb
            if (MzInstrs.ImbsSourceType == IlMzInstruments.EnumSourceType.ElectricalSource)
            {
                // Setup
                SetupTapsForSweep(numberOfPoints, inlineTap_V, complementaryTap_V);

                EnableAllOutputs(true);

                // Left + Right mod arms
                MzInstrs.LeftArmMod.InitSourceVMeasureI_TriggeredFixedVoltage(leftModArmVoltage_V, numberOfPoints, triggerLineIn, triggerLineUnused);
                MzInstrs.RightArmMod.InitSourceVMeasureI_TriggeredFixedVoltage(rightModArmVoltage_V, numberOfPoints, triggerLineIn, triggerLineUnused);

                // Setup L + R imb
                //   It's safest to explicitly set the instruments to sense V here, 
                //   as any spot-measurements may have reset the instrument to read A
                MzInstrs.LeftArmImb.SenseVoltage(voltageCompliance, voltageRange);
                MzInstrs.RightArmImb.SenseVoltage(voltageCompliance, voltageRange);

                // right imb
                MzInstrs.RightArmImb.InitSourceIMeasureV_CurrentSweep(stopCurrent_A, startCurrent_A, numberOfPoints, triggerLineIn, triggerLineUnused);

                // Drive the sweep using the leftImb arm
                MzInstrs.LeftArmImb.CurrentSetPoint_amp = startCurrent_A;
                MzInstrs.LeftArmImb.InitSourceIMeasureV_CurrentSweep(startCurrent_A, stopCurrent_A, numberOfPoints, triggerLineIn, triggerLineOut);

                MzInstrs.RightArmImb.CurrentSetPoint_amp = stopCurrent_A;
                return DoTheSweep(MzInstrs.LeftArmImb, SourceMeter.LeftImbArm, SweepType.DifferentialCurrent,
                    numberOfPoints);
            }
            else
            {
                if (!ImbArmSourceByAsic)
                #region Ke2400 as source
                {
                    // Alice.Huang  2010-02-04
                    // if imb s source by FCU2ASIC, then we don't need trigger box,
                    // or we can't get the imb sweep data
                    SetupTapsForSweep(1, inlineTap_V, complementaryTap_V);
                    try
                    {
                        MzInstrs.LeftArmMod.InitSourceVMeasureI_UntriggeredSpotMeas();
                        MzInstrs.RightArmMod.InitSourceVMeasureI_UntriggeredSpotMeas();
                    }
                    catch (Exception ex)
                    {

                        MzInstrs.RightArmMod.CleanUpSweep();
                        MzInstrs.LeftArmMod.CleanUpSweep();
                    }
                    SetVoltage(MzInstrs.LeftArmMod, leftModArmVoltage_V);
                    SetVoltage(MzInstrs.RightArmMod, rightModArmVoltage_V);
                    //MzInstrs.LeftArmMod.VoltageSetPoint_Volt = leftModArmVoltage_V;
                    //MzInstrs.LeftArmMod.SourceFixedVoltage();
                    //MzInstrs.RightArmMod.VoltageSetPoint_Volt = rightModArmVoltage_V;
                    //MzInstrs.RightArmMod.SourceFixedVoltage();

                    MzInstrs.FCU2Asic.IimbLeft_mA = (float)startCurrent_A * 1000;
                    MzInstrs.FCU2Asic.IimbRight_mA = (float)stopCurrent_A * 1000;

                    EnableAllOutputs(true);

                    return ImbDifferentialSweepWithAsic(startCurrent_A * 1000, stopCurrent_A * 1000,
                                                numberOfPoints, 20,ImbArmSourceByAsic);
                }
                #endregion Ke2400 as source
                else
                {
                    MzInstrs.FCU2Asic.IimbLeft_mA = (float)startCurrent_A * 1000;
                    MzInstrs.FCU2Asic.IimbRight_mA = (float)stopCurrent_A * 1000;

                    MzInstrs.FCU2Asic.VLeftModBias_Volt = (float)leftModArmVoltage_V;
                    MzInstrs.FCU2Asic.VRightModBias_Volt = (float)rightModArmVoltage_V;

                    //echo new added below if clause to ensure we can get correct ctap value. 2011-02-18
                    if (opticalSwitch != null)
                    {
                        IInstType_DigitalIO ctapLine = opticalSwitch.GetDigiIoLine(IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine);
                        ctapLine.LineState = IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine_State;
                    }
                    return ImbDifferentialSweepWithAsic(startCurrent_A * 1000, stopCurrent_A * 1000,
                                                numberOfPoints, MZSourceMeasureDelay_ms, ImbArmSourceByAsic);

                }

            }
        }

        /// <summary>
        ///  Create this ImbArm by tim to combine solution
        /// </summary>
        /// <param name="leftModArmVoltage_V"></param>
        /// <param name="rightModArmVoltage_V"></param>
        /// <param name="startCurrent_A"></param>
        /// <param name="stopCurrent_A"></param>
        /// <param name="numberOfPoints"></param>
        /// <param name="inlineTap_V"></param>
        /// <param name="complementaryTap_V"></param>
        /// <param name="ImbArmSourceByAsic"></param>
        /// <param name="isCalibratePower"></param>
        /// <returns></returns>
        public ILMZSweepResult ImbArm_DifferentialSweep(
                        double leftModArmVoltage_V, double rightModArmVoltage_V,
                        double startCurrent_A, double stopCurrent_A, int numberOfPoints,
                        double inlineTap_V, double complementaryTap_V, bool ImbArmSourceByAsic, bool isCalibratePower)
        {
            // Alice.Huang  2010-02-03
            // add FCU2Asic instrument functions

            // Setup L + R imb
            if (MzInstrs.ImbsSourceType == IlMzInstruments.EnumSourceType.ElectricalSource)
            {
                // Setup
                SetupTapsForSweep(numberOfPoints, inlineTap_V, complementaryTap_V);

                EnableAllOutputs(true);

                // Left + Right mod arms
                MzInstrs.LeftArmMod.InitSourceVMeasureI_TriggeredFixedVoltage(leftModArmVoltage_V, numberOfPoints, triggerLineIn, triggerLineUnused);
                MzInstrs.RightArmMod.InitSourceVMeasureI_TriggeredFixedVoltage(rightModArmVoltage_V, numberOfPoints, triggerLineIn, triggerLineUnused);

                // Setup L + R imb
                //   It's safest to explicitly set the instruments to sense V here, 
                //   as any spot-measurements may have reset the instrument to read A
                MzInstrs.LeftArmImb.SenseVoltage(voltageCompliance, voltageRange);
                MzInstrs.RightArmImb.SenseVoltage(voltageCompliance, voltageRange);

                // right imb
                MzInstrs.RightArmImb.InitSourceIMeasureV_CurrentSweep(stopCurrent_A, startCurrent_A, numberOfPoints, triggerLineIn, triggerLineUnused);

                // Drive the sweep using the leftImb arm
                MzInstrs.LeftArmImb.CurrentSetPoint_amp = startCurrent_A;
                MzInstrs.LeftArmImb.InitSourceIMeasureV_CurrentSweep(startCurrent_A, stopCurrent_A, numberOfPoints, triggerLineIn, triggerLineOut);

                MzInstrs.RightArmImb.CurrentSetPoint_amp = stopCurrent_A;
                return DoTheSweep(MzInstrs.LeftArmImb, SourceMeter.LeftImbArm, SweepType.DifferentialCurrent,
                    numberOfPoints);
            }
            else
            {
                if (!ImbArmSourceByAsic)
                #region Ke2400 as source
                {
                    // Alice.Huang  2010-02-04
                    // if imb s source by FCU2ASIC, then we don't need trigger box,
                    // or we can't get the imb sweep data
                    SetupTapsForSweep(1, inlineTap_V, complementaryTap_V);
                    try
                    {
                        MzInstrs.LeftArmMod.InitSourceVMeasureI_UntriggeredSpotMeas();
                        MzInstrs.RightArmMod.InitSourceVMeasureI_UntriggeredSpotMeas();
                    }
                    catch (Exception ex)
                    {

                        MzInstrs.RightArmMod.CleanUpSweep();
                        MzInstrs.LeftArmMod.CleanUpSweep();
                    }
                    SetVoltage(MzInstrs.LeftArmMod, leftModArmVoltage_V);
                    SetVoltage(MzInstrs.RightArmMod, rightModArmVoltage_V);
                    //MzInstrs.LeftArmMod.VoltageSetPoint_Volt = leftModArmVoltage_V;
                    //MzInstrs.LeftArmMod.SourceFixedVoltage();
                    //MzInstrs.RightArmMod.VoltageSetPoint_Volt = rightModArmVoltage_V;
                    //MzInstrs.RightArmMod.SourceFixedVoltage();

                    MzInstrs.FCU2Asic.IimbLeft_mA = (float)startCurrent_A * 1000;
                    MzInstrs.FCU2Asic.IimbRight_mA = (float)stopCurrent_A * 1000;

                    EnableAllOutputs(true);

                    return ImbDifferentialSweepWithAsic(startCurrent_A * 1000, stopCurrent_A * 1000,
                                                numberOfPoints, 20, ImbArmSourceByAsic);
                }
                #endregion Ke2400 as source
                else
                {
                    MzInstrs.FCU2Asic.IimbLeft_mA = (float)startCurrent_A * 1000;
                    MzInstrs.FCU2Asic.IimbRight_mA = (float)stopCurrent_A * 1000;

                    MzInstrs.FCU2Asic.VLeftModBias_Volt = (float)leftModArmVoltage_V;
                    MzInstrs.FCU2Asic.VRightModBias_Volt = (float)rightModArmVoltage_V;

                    //echo new added below if clause to ensure we can get correct ctap value. 2011-02-18
                    if (opticalSwitch != null)
                    {
                        IInstType_DigitalIO ctapLine = opticalSwitch.GetDigiIoLine(IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine);
                        ctapLine.LineState = IlmzOpticalSwitchLines.Dut_Ctap_DigiIoLine_State;
                    }
                    return ImbDifferentialSweepWithAsic(startCurrent_A * 1000, stopCurrent_A * 1000,
                                                numberOfPoints, 20, ImbArmSourceByAsic, isCalibratePower);
                }

            }
        }

        /// <summary>
        /// Performs a voltage sweep on the inline tap arm.
        /// If the inline tap instrument is not configured as online an empty structure will be returned.
        /// </summary>
        /// <param name="leftModArmVoltage_V">Fixed left modulator arm voltage.</param>
        /// <param name="rightModArmVoltage_V">Fixed right modulator arm voltage.</param>
        /// <param name="leftImbArmCurrent_A">Fixed left imbalance arm current.</param>
        /// <param name="rightImbArmCurrent_A">Fixed right imbalance arm current.</param>
        /// <param name="startVoltage_V">Start level for the left imb arm, stop level for the right imb arm.</param>
        /// <param name="stopVoltage_V">Stop level for the left imb arm, start level for the right imb arm.</param>
        /// <param name="numberOfPoints">Number of points in the sweep.</param>
        /// <param name="complementaryTap_V">Fixed complementary tap bias level.</param>
        /// <returns>The optical power data and all voltage and current data from each of the sourcemeters.</returns>
        public ILMZSweepResult InlineTapSweep(
                    double leftModArmVoltage_V, double rightModArmVoltage_V,
                    double leftImbArmCurrent_A, double rightImbArmCurrent_A,
                    double startVoltage_V, double stopVoltage_V, int numberOfPoints,
                    double complementaryTap_V)
        {
            if (!MzInstrs.InlineTapOnline)
            {
                return new ILMZSweepResult();
            }
            
            // cTap
            MzInstrs.TapComplementary.InitSourceVMeasureI_TriggeredFixedVoltage(complementaryTap_V,
                                        numberOfPoints, triggerLineIn, triggerLineOut);

            // L + R imb
            // Alice.Huang  2010-02-02
            // Add FCU2ASIC OPERATION
            if (this.MzInstrs.ImbsSourceType ==
                IlMzInstruments.EnumSourceType.ElectricalSource)
            {
                // Setup
                SetImbalanceVSense();
                MzInstrs.LeftArmImb.InitSourceIMeasureV_TriggeredFixedCurrent(leftImbArmCurrent_A,
                                            numberOfPoints, triggerLineIn, triggerLineUnused);
                MzInstrs.RightArmImb.InitSourceIMeasureV_TriggeredFixedCurrent(rightImbArmCurrent_A,
                                            numberOfPoints, triggerLineIn, triggerLineUnused);
            }
            else
            {
                MzInstrs.FCU2Asic.IimbLeft_mA = (float)leftImbArmCurrent_A * 1000;
                MzInstrs.FCU2Asic.IimbRight_mA = (float)rightImbArmCurrent_A * 1000;
            }
            // L + R arms
            MzInstrs.LeftArmMod.InitSourceVMeasureI_TriggeredFixedVoltage(leftModArmVoltage_V,
                                numberOfPoints, triggerLineIn, triggerLineUnused);
            MzInstrs.RightArmMod.InitSourceVMeasureI_TriggeredFixedVoltage(rightModArmVoltage_V, 
                                numberOfPoints, triggerLineIn, triggerLineUnused);

            // Drive the sweep using the inline tap
            MzInstrs.TapInline.VoltageSetPoint_Volt = startVoltage_V;
            MzInstrs.TapInline.InitSourceVMeasureI_VoltageSweep(startVoltage_V, stopVoltage_V, 
                                numberOfPoints, triggerLineIn, triggerLineOut);

            return DoTheSweep(MzInstrs.TapInline, SourceMeter.InlineTap, 
                SweepType.SingleEndVoltage, numberOfPoints);
        }

       
        /// <summary>
        /// Performs a voltage sweep on the complementary tap arm.
        /// </summary>
        /// <param name="leftModArmVoltage_V">Fixed left modulator arm voltage.</param>
        /// <param name="rightModArmVoltage_V">Fixed right modulator arm voltage.</param>
        /// <param name="leftImbArmCurrent_A">Fixed left imbalance arm current.</param>
        /// <param name="rightImbArmCurrent_A">Fixed right imbalance arm current.</param>
        /// <param name="startVoltage_V">Start level for the left imb arm, stop level for the right imb arm.</param>
        /// <param name="stopVoltage_V">Stop level for the left imb arm, start level for the right imb arm.</param>
        /// <param name="numberOfPoints">Number of points in the sweep.</param>
        /// <param name="inlineTap_V">Fixed inline tap bias level.</param>
        /// <returns>The optical power data and all voltage and current data from each of the sourcemeters.</returns>
        public ILMZSweepResult ComplementaryTapSweep(double leftModArmVoltage_V,
            double rightModArmVoltage_V, double leftImbArmCurrent_A, double rightImbArmCurrent_A,
            double startVoltage_V, double stopVoltage_V, int numberOfPoints, double inlineTap_V)
        {
            if (MzInstrs.InlineTapOnline)
            {
                MzInstrs.TapInline.InitSourceVMeasureI_TriggeredFixedVoltage(inlineTap_V,
                                            numberOfPoints, triggerLineIn, triggerLineUnused);
            }

            // Setup
            // Alice.Huang  2010-02-02
            // Add FCU2ASIC OPERATION
            if (this.MzInstrs.ImbsSourceType ==
                IlMzInstruments.EnumSourceType.ElectricalSource)
            {
                SetImbalanceVSense();

                // L + R imb
                MzInstrs.LeftArmImb.InitSourceIMeasureV_TriggeredFixedCurrent(leftImbArmCurrent_A, numberOfPoints, triggerLineIn, triggerLineUnused);
                MzInstrs.RightArmImb.InitSourceIMeasureV_TriggeredFixedCurrent(rightImbArmCurrent_A, numberOfPoints, triggerLineIn, triggerLineUnused);
            }
            else
            {
                MzInstrs.FCU2Asic.IimbLeft_mA =(float ) leftImbArmCurrent_A * 1000;
                MzInstrs.FCU2Asic.IimbRight_mA = (float)rightImbArmCurrent_A * 1000;
            }
            // L + R arms
            // Use LeftArm as the trigger source
            MzInstrs.LeftArmMod.InitSourceVMeasureI_TriggeredFixedVoltage(leftModArmVoltage_V, numberOfPoints, triggerLineIn, triggerLineOut);
            MzInstrs.RightArmMod.InitSourceVMeasureI_TriggeredFixedVoltage(rightModArmVoltage_V, numberOfPoints, triggerLineIn, triggerLineUnused);

            // Drive the sweep using the complementary tap
            MzInstrs.TapComplementary.VoltageSetPoint_Volt = startVoltage_V;
            MzInstrs.TapComplementary.InitSourceVMeasureI_VoltageSweep(startVoltage_V, stopVoltage_V, numberOfPoints, triggerLineIn, triggerLineOut);

            return DoTheSweep(MzInstrs.TapComplementary, SourceMeter.ComplementaryTap, SweepType.SingleEndVoltage,
                numberOfPoints);
        }




        #region private members
        /// <summary>
        /// Performs the steps necessary to apply a voltage on the specified instrument.
        /// </summary>
        /// <param name="sourceMeter">The source meter to be set</param>
        /// <param name="voltage_V">The voltage level to apply.</param>
        private static void SetVoltage(InstType_TriggeredElectricalSource sourceMeter, double voltage_V)
        {
            sourceMeter.InitSourceVMeasureI_UntriggeredSpotMeas();
            sourceMeter.VoltageSetPoint_Volt = voltage_V;
            sourceMeter.SourceFixedVoltage();
            //if (Math .Abs (voltage_V )> 0)  sourceMeter.SenseVoltage(Math.Abs(voltage_V), Math.Abs(voltage_V));
            sourceMeter.OutputEnabled = true;
        }

        /// <summary>
        /// Performs the steps necessary to apply a current on the specified instrument.
        /// </summary>
        /// <param name="sourceMeter">The source meter to be set</param>
        /// <param name="current_A">The current level to apply.</param>
        private static void SetCurrent(InstType_TriggeredElectricalSource sourceMeter, double current_A)
        {
            sourceMeter.InitSourceIMeasureV_UntriggeredSpotMeas();
            sourceMeter.CurrentSetPoint_amp = current_A;
            sourceMeter.SourceFixedCurrent();
            sourceMeter.OutputEnabled = true;
        }

        /// <summary>
        /// Trigger the sweep, wait for completion, return all data.
        /// </summary>
        /// <param name="sourceMeter">The instrument that we should wait for to determine whether the sweep is complete.</param>
        /// <param name="sourceMeterEnum">Which source meter to use</param>
        /// <param name="type">What type of sweep is this?</param>
        /// <param name="nbrPoints">Number of points</param>
        /// <returns>The optical power data and all voltage and current data from each of the sourcemeters.</returns>
        private ILMZSweepResult DoTheSweep(InstType_TriggeredElectricalSource sourceMeter,
            SourceMeter sourceMeterEnum, SweepType type, int nbrPoints)
        {
            ILMZSweepResult sweepData = new ILMZSweepResult();
            sweepData.SrcMeter = sourceMeterEnum;
            sweepData.Type = type;

            // Clear traces
            MzInstrs.LeftArmMod.ClearSweepTraceData();
            MzInstrs.RightArmMod.ClearSweepTraceData();            
            MzInstrs.LeftArmImb.ClearSweepTraceData();
            MzInstrs.RightArmImb.ClearSweepTraceData();
            
            MzInstrs.TapComplementary.ClearSweepTraceData();
            if (MzInstrs.InlineTapOnline)
                MzInstrs.TapInline.ClearSweepTraceData();

            // Outputs on
            EnableAllOutputs(true);

            // Wake up OPM
            //MzInstrs.PowerMeter.EnableInputTrigger(false);

            // Configure OPM for triggered sweep

            // Alice.Huang     2010-02-26
            //commented all methjod related with   IInstType_TriggeredOpticalPowerMeter   with  InstType_OpticalPowerMeter 

            //MzInstrs.PowerMeter.StopDataLogging();
            System.Threading.Thread.Sleep(500);

            //MzInstrs.PowerMeter.ConfigureDataLogging(nbrPoints, this.averagingTime);
            //MzInstrs.PowerMeter.EnableInputTrigger(true);
            //MzInstrs.PowerMeter.StartDataLogging();

            // Set triggers        
            MzInstrs.LeftArmImb.Trigger();
            MzInstrs.RightArmImb.Trigger();            
            MzInstrs.LeftArmMod.Trigger();
            MzInstrs.RightArmMod.Trigger();
            if (MzInstrs.InlineTapOnline)
                MzInstrs.TapInline.Trigger();

            // Need a small delay here if sweep time is below 100ms
            System.Threading.Thread.Sleep(500);

            // Start the sweep running
            //mzInstrs.TapComplementary.Trigger();
            MzInstrs.TapComplementary.StartSweep();

            // Wait for finish
            bool status = sourceMeter.WaitForSweepToFinish();
            if (!status)
            {
                throw new InstrumentException("Error waiting for sweep");
            }

            // get data
            //
            double[] uncalibratedPower_mW=null ;// = MzInstrs.PowerMeter.GetSweepData();
            sweepData.SweepData[ILMZSweepDataType.FibrePower_mW] = OpticalPowerCal.GetCorrectedPower_mW(OpticalPowerHead.MZHead, uncalibratedPower_mW, Measurements.FrequencyWithoutMeter);

            double[] voltsArray = null;
            double[] currentArray = null;
            
            MzInstrs.LeftArmImb.GetSweepDataSet(out voltsArray, out currentArray);
            sweepData.SweepData[ILMZSweepDataType.LeftArmImb_V] = voltsArray;
            sweepData.SweepData[ILMZSweepDataType.LeftArmImb_A] = currentArray;

            MzInstrs.RightArmImb.GetSweepDataSet(out voltsArray, out currentArray);
            sweepData.SweepData[ILMZSweepDataType.RightArmImb_V] = voltsArray;
            sweepData.SweepData[ILMZSweepDataType.RightArmImb_A] = currentArray;
            
            MzInstrs.LeftArmMod.GetSweepDataSet(out voltsArray, out currentArray);
            sweepData.SweepData[ILMZSweepDataType.LeftArmModBias_V] = voltsArray;
            sweepData.SweepData[ILMZSweepDataType.LeftArmModBias_mA] = Alg_ArrayFunctions.MultiplyEachArrayElement(currentArray, 1000.0);// The current is in mA

            MzInstrs.RightArmMod.GetSweepDataSet(out voltsArray, out currentArray);
            sweepData.SweepData[ILMZSweepDataType.RightArmModBias_V] = voltsArray;
            sweepData.SweepData[ILMZSweepDataType.RightArmModBias_mA] = Alg_ArrayFunctions.MultiplyEachArrayElement(currentArray, 1000.0);// The current is in mA

            MzInstrs.TapComplementary.GetSweepDataSet(out voltsArray, out currentArray);
            sweepData.SweepData[ILMZSweepDataType.TapComplementary_V] = voltsArray;
            sweepData.SweepData[ILMZSweepDataType.TapComplementary_mA] = Alg_ArrayFunctions.MultiplyEachArrayElement(currentArray, 1000.0);// The current is in mA

            if (MzInstrs.TapInline != null)
            {
                MzInstrs.TapInline.GetSweepDataSet(out voltsArray, out currentArray);
                sweepData.SweepData[ILMZSweepDataType.TapInline_V] = voltsArray;
                sweepData.SweepData[ILMZSweepDataType.TapInline_mA] = Alg_ArrayFunctions.MultiplyEachArrayElement(currentArray, 1000.0);// The current is in mA
            }

            // Turn outputs off
            if (MzInstrs.ImbsSourceType == IlMzInstruments.EnumSourceType.Asic)
            {
                MzInstrs.FCU2Asic.IimbLeft_mA = 0;
                MzInstrs.FCU2Asic.IimbRight_mA = 0;
            }
            else
                EnableAllOutputs(false);

            // If we don't do this the power meter won't play any more
            //MzInstrs.PowerMeter.StopDataLogging();
            //MzInstrs.PowerMeter.EnableInputTrigger(false);

            // For each meter
            // :FORM ASC
            // :SYST:AZER:STAT ON
            CleanupAfterSweep();

            return sweepData;
        }
        //Echo new added this function 2011-1-7
        /// <summary>
        /// sweep asic device via fcu mk I
        /// </summary>
        /// <param name="lvsweepType"></param>
        /// <param name="start"></param>
        /// <param name="stop"></param>
        /// <param name="delaytime"></param>
        /// <param name="type"></param>
        /// <param name="nbrPoints"></param>
        /// <returns></returns>
        private ILMZSweepResult SweepAsicDeviceByFCUI(LvSweepType lvsweepType, double start, double stop, int delaytime, SweepType type, int nbrPoints, bool exchangeVolt, ILMZSweepResult sweepData)
        {

            // Wake up OPM
            //MzInstrs.PowerMeter.SetDefaultState();

            MzInstrs.PowerMeter.EnableInputTrigger(false);

            // Configure OPM for triggered sweep

            //MzInstrs.PowerMeter.StopDataLogging();
            System.Threading.Thread.Sleep(500);

            MzInstrs.PowerMeter.ConfigureDataLogging(nbrPoints, delaytime/1000.0);
            MzInstrs.PowerMeter.EnableInputTrigger(true);
            MzInstrs.PowerMeter.StartDataLogging();
            System.Threading.Thread.Sleep(1500);

            //check start stop value is valid?
             start = Math.Round(start, 5);//there is error when V_bias change to DAC with long digital number for FCUMKI . jack.zhang 2012-05-08
             stop = Math.Round(stop, 5);
            // Set triggers 
            if(type==SweepType.SingleEndCurrent || type==SweepType.DifferentialCurrent)
            {
                throw new Exception("invalid sweep type ");
            }
            if(lvsweepType==LvSweepType.Left)
            {
                MzInstrs.FCU2Asic.FcuLeftBiasSingleEndedTrig(start,stop,nbrPoints,delaytime);
            }
            if (lvsweepType == LvSweepType.Right)
            {
                MzInstrs.FCU2Asic.FcuRightBiasSingleEndedTrig(start,stop,nbrPoints,delaytime);
            }
            if (lvsweepType == LvSweepType.Diff)
            {
                if (Math.Abs(start) > Math.Abs(stop))
                {
                    //switch start and stop
                    double temp = start;
                    start = stop;
                    stop = temp;
                }
                MzInstrs.FCU2Asic.FcuBiasDiffTrig(start,stop,nbrPoints,delaytime);
            }
                        
            // Need longer than all need time 500ms. jack.zhang 2012-04-30
            System.Threading.Thread.Sleep(nbrPoints * delaytime+500);
            

            // get data
            double[] Ctap_mA = MzInstrs.FCU2Asic.GetCtapAfterMZsweep();
            double[] uncalibratedPower_mW = MzInstrs.PowerMeter.GetSweepData();
            //if (lvsweepType != LvSweepType.Diff && exchangeVolt)
            {
                uncalibratedPower_mW = Alg_ArrayFunctions.ReverseArray(uncalibratedPower_mW);
                Ctap_mA = Alg_ArrayFunctions.ReverseArray(Ctap_mA);
            }
            //sweepResult.SweepData[ILMZSweepDataType.FibrePower_mW] = OpticalPowerCal.GetCorrectedPower_mW(OpticalPowerHead.MZHead, uncalibratedPower_mW, Measurements.FrequencyWithoutMeter);
            sweepData.SweepData[ILMZSweepDataType.FibrePower_mW] =uncalibratedPower_mW;

            sweepData.SweepData[ILMZSweepDataType.TapComplementary_mA] = Ctap_mA;
           
            // If we don't do this the power meter won't play any more
            MzInstrs.PowerMeter.StopDataLogging();
            MzInstrs.PowerMeter.EnableInputTrigger(false);
          
            return sweepData;
        }

        //private double[] GetCtapAfterMZsweep()
        //{
        //    throw new Exception("The method or operation is not implemented.");
        //}


        // Alice.Huang  2010-02-04
        // Add RF sweep functions with FCU2ASIC
        /// <summary>
        /// Trigger the RF source to  sweep, wait for completion, return all data.
        /// </summary>
        /// <param name="sourceMeter">The instrument that we should wait for to determine whether the sweep is complete.</param>
        /// <param name="sourceMeterEnum">Which source meter to use</param>
        /// <param name="type">What type of sweep is this?</param>
        /// <param name="nbrPoints">Number of points</param>
        /// <returns>The optical power data and all voltage and current data from each of the sourcemeters.</returns>
        private ILMZSweepResult SweepAsic(InstType_TriggeredElectricalSource sourceMeter,
            SourceMeter sourceMeterEnum, SweepType type, int nbrPoints)
        {
            ILMZSweepResult sweepData = new ILMZSweepResult();
            sweepData.SrcMeter = sourceMeterEnum;
            sweepData.Type = type;

            // Clear traces
            MzInstrs.LeftArmMod.ClearSweepTraceData();
            MzInstrs.RightArmMod.ClearSweepTraceData();


            MzInstrs.TapComplementary.ClearSweepTraceData();
            if (MzInstrs.InlineTapOnline)
                MzInstrs.TapInline.ClearSweepTraceData();

            // Outputs on
            EnableAllOutputs(true);

            // Wake up OPM
            MzInstrs.PowerMeter.EnableInputTrigger(false);

            // Configure OPM for triggered sweep

            MzInstrs.PowerMeter.StopDataLogging();
            System.Threading.Thread.Sleep(500);

            MzInstrs.PowerMeter.ConfigureDataLogging(nbrPoints, this.averagingTime);
            MzInstrs.PowerMeter.EnableInputTrigger(true);
            MzInstrs.PowerMeter.StartDataLogging();

            // Set triggers      
            MzInstrs.LeftArmMod.Trigger();
            MzInstrs.RightArmMod.Trigger();
            if (MzInstrs.InlineTapOnline)
                MzInstrs.TapInline.Trigger();

            // Need a small delay here if sweep time is below 100ms
            System.Threading.Thread.Sleep(500);

            // Start the sweep running
            MzInstrs.TapComplementary.Trigger();
           
            // Wait for finish
            bool status = sourceMeter.WaitForSweepToFinish();
            if (!status)
            {
                throw new InstrumentException("Error waiting for sweep");
            }

            // get data
            //
            double[] uncalibratedPower_mW = MzInstrs.PowerMeter.GetSweepData();
            sweepData.SweepData[ILMZSweepDataType.FibrePower_mW] = OpticalPowerCal.GetCorrectedPower_mW(OpticalPowerHead.MZHead, uncalibratedPower_mW, Measurements.FrequencyWithoutMeter);

            double[] voltsArray = null;
            double[] currentArray = null;

            
            double curtemp = MzInstrs.FCU2Asic.IimbLeft_mA / 1000;

            MzInstrs.LeftArmMod.GetSweepDataSet(out voltsArray, out currentArray);
            sweepData.SweepData[ILMZSweepDataType.LeftArmModBias_V] = voltsArray;
            sweepData.SweepData[ILMZSweepDataType.LeftArmModBias_mA] = Alg_ArrayFunctions.MultiplyEachArrayElement(currentArray, 1000.0);// The current is in mA

            MzInstrs.RightArmMod.GetSweepDataSet(out voltsArray, out currentArray);
            sweepData.SweepData[ILMZSweepDataType.RightArmModBias_V] = voltsArray;
            sweepData.SweepData[ILMZSweepDataType.RightArmModBias_mA] = Alg_ArrayFunctions.MultiplyEachArrayElement(currentArray, 1000.0);// The current is in mA

            MzInstrs.TapComplementary.GetSweepDataSet(out voltsArray, out currentArray);
            sweepData.SweepData[ILMZSweepDataType.TapComplementary_V] = voltsArray;
            sweepData.SweepData[ILMZSweepDataType.TapComplementary_mA] = Alg_ArrayFunctions.MultiplyEachArrayElement(currentArray, 1000.0);// The current is in mA

            if (MzInstrs.TapInline != null)
            {
                MzInstrs.TapInline.GetSweepDataSet(out voltsArray, out currentArray);
                sweepData.SweepData[ILMZSweepDataType.TapInline_V] = voltsArray;
                sweepData.SweepData[ILMZSweepDataType.TapInline_mA] = Alg_ArrayFunctions.MultiplyEachArrayElement(currentArray, 1000.0);// The current is in mA
            }
           
            // If we don't do this the power meter won't play any more
            MzInstrs.PowerMeter.StopDataLogging();
            MzInstrs.PowerMeter.EnableInputTrigger(false);
            
            // For each meter
            // :FORM ASC
            // :SYST:AZER:STAT ON
            CleanupAfterSweep();

            return sweepData;
        }
        /// <summary>
        /// RF defferential sweep, 
        /// Left RF starts from iStart and end at vStop
        /// Right RF starts from iStop and end at vStart
        /// </summary>
        /// <param name="vStart">RF sweep start Voltage</param>
        /// <param name="vStop">RF sweep stop voltage</param>
        /// <param name="nbrPoints">Number of points</param>
        /// <param name="currentSettledTime_mS">time has to wait to get a stable reading from instrument 
        /// after a new volt apply on RF </param>
        /// <returns>
        /// Left imb current & DAC, .
        /// Right imb current & Dac
        /// left & Right RF Voltage
        /// Ctap & Tap ( if source exists) currents
        /// opotical power in mW   
        /// </returns>
        private ILMZSweepResult rfDefferentialSweepWithAsic(double vStart, double vStop,
                                        int nbrPoints, int currentSettledTime_mS)
        {
            if (nbrPoints < 2) throw new ArgumentException(
                 " can't carry a sweep with data points with " + nbrPoints.ToString());

            ILMZSweepResult sweepData = new ILMZSweepResult();
            sweepData.Type = SweepType.DifferentialVoltage;
            // Clear traces
            //MzInstrs.LeftArmMod.ClearSweepTraceData();
            //MzInstrs.RightArmMod.ClearSweepTraceData();

            //MzInstrs.TapComplementary.ClearSweepTraceData();
            //if (MzInstrs.InlineTapOnline)
            //    MzInstrs.TapInline.ClearSweepTraceData();

            // Outputs on
            EnableAllOutputs(true);

            // Wake up OPM
            MzInstrs.PowerMeter.EnableInputTrigger(false);
            MzInstrs.PowerMeter.EnableOutputTrigger(false);
            //Configure OPM for triggered sweep
            //MzInstrs.PowerMeter.StopDataLogging();

            System.Threading.Thread.Sleep(50);

            // Alice.Huang    2010-03-07
            // Commented for 8153 run error in debug
            //MzInstrs.PowerMeter.AveragingTime_s = this.averagingTime;
            SetAutoZero(MzInstrs.TapComplementary, true);
            SetAutoZero(MzInstrs.LeftArmMod, true);
            SetAutoZero(MzInstrs.RightArmMod, true);

            MzInstrs.PowerMeter.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_mW;

            double[] uncalibratedPower_mW = new double[nbrPoints];
            double[] cur_L_A = new double[nbrPoints];   // Left Imb current 
            double[] cur_R_A = new double[nbrPoints];   // Right Imb current
            double[] Vol_L_V = new double[nbrPoints];   // Left RF voltage
            double[] Vol_R_V = new double[nbrPoints];   // Right RF Voltage

            double[] dac_L = new double[nbrPoints];   // Left imb Current Dac
            double[] dac_R = new double[nbrPoints];   // Right imb current Dac
            double[] cur_CTap_A = new double[nbrPoints];   // compliance Tap Current
            double[] cur_Tap_A = new double[nbrPoints];    // inline Tap Current 

            // sweep imb arms

            double vStep = (vStop - vStart) / (nbrPoints - 1);
            for (int i = 0; i < nbrPoints; i++)
            {
                MzInstrs.LeftArmMod .VoltageSetPoint_Volt  = (vStart + i * vStep);
                MzInstrs.RightArmMod.VoltageSetPoint_Volt   = (vStop - i * vStep);

                System.Threading.Thread.Sleep(currentSettledTime_mS);
                cur_L_A[i] = MzInstrs.FCU2Asic.IimbLeft_mA / 1000;
                dac_L[i] = MzInstrs.FCU2Asic.IimbLeft_Dac;
                cur_R_A[i] = MzInstrs.FCU2Asic.IimbRight_mA / 1000;
                dac_R[i] = MzInstrs.FCU2Asic.IimbRight_Dac;

                Vol_L_V[i] = MzInstrs.LeftArmMod.VoltageActual_Volt;
                Vol_R_V[i] = MzInstrs.RightArmMod.VoltageActual_Volt;
                cur_CTap_A[i] = MzInstrs.TapComplementary.CurrentActual_amp;
                if (MzInstrs.TapInline != null)
                    cur_Tap_A[i] = MzInstrs.TapInline.CurrentActual_amp;
                uncalibratedPower_mW[i] = MzInstrs.PowerMeter.ReadPower();
            }

            //sweepResult.SweepData[ILMZSweepDataType.FibrePower_mW] = OpticalPowerCal.GetCorrectedPower_mW(OpticalPowerHead.MZHead, uncalibratedPower_mW, Measurements.CurrentFrequency_GHz);
            //sweepResult.SweepData[ILMZSweepDataType.LeftArmImb_A] = cur_L_A ;
            //sweepResult.SweepData[ILMZSweepDataType.RightArmImb_A] = cur_R_A ;

            sweepData.SweepData.Add(ILMZSweepDataType.FibrePower_mW,
                OpticalPowerCal.GetCorrectedPower_mW(OpticalPowerHead.MZHead,
                uncalibratedPower_mW, Measurements.FrequencyWithoutMeter));
            sweepData.SweepData.Add(ILMZSweepDataType.LeftArmImb_A, cur_L_A);
            sweepData.SweepData.Add(ILMZSweepDataType.LeftArmImb_I_Dac, dac_L);
            sweepData.SweepData.Add(ILMZSweepDataType.RightArmImb_A, cur_R_A);
            sweepData.SweepData.Add(ILMZSweepDataType.RightArmImb_I_dac, dac_R);

            sweepData.SweepData.Add(ILMZSweepDataType.LeftArmModBias_V, Vol_L_V);
            sweepData.SweepData.Add(ILMZSweepDataType.RightArmModBias_V, Vol_R_V);
            sweepData.SweepData.Add(ILMZSweepDataType.TapComplementary_mA, cur_CTap_A);

            if (MzInstrs.TapInline != null)
                sweepData.SweepData.Add(ILMZSweepDataType.TapInline_mA, cur_Tap_A);

            // Turn outputs off
            EnableAllOutputs(false);
            MzInstrs.FCU2Asic.IimbLeft_mA = 0;
            MzInstrs.FCU2Asic.IimbRight_mA = 0;

            // For each meter
            // :FORM ASC
            // :SYST:AZER:STAT ON
            //CleanupAfterSweep();

            return sweepData;
        }
        /// <summary>
        /// RF DATA single ended sweep, sweep will start from VStart and end with VStop
        /// </summary>
        /// <param name="sourceMeterEnum">to indentify which RF DATA Arm that this sweep carry on</param>
        /// <param name="vStart">RF Arm sweep from voltage vStart</param>
        /// <param name="vStop">RF Arm sweep to voltage vStop </param>
        /// <param name="nbrPoints">Number of points</param>
        /// <param name="voltageSettledTime_mS">time has to wait to get a stable reading from instrument 
        /// after a new voltage apply on RF Data arm </param>
        /// <returns>
        /// Left imb current & DAC, .
        /// Right imb current & Dac
        /// left & Right RF Voltage
        /// Ctap & Tap ( if source exists) currents
        /// opotical power in mW   
        /// </returns>
        private ILMZSweepResult rfSingleEndedSweepWithAsic(SourceMeter sourceMeterEnum,
            double vStart, double vStop, int nbrPoints, int voltageSettledTime_mS)
        {
            ILMZSweepResult sweepData = new ILMZSweepResult();
            sweepData.SrcMeter = sourceMeterEnum;
            sweepData.Type = SweepType.SingleEndVoltage;

            SetAutoZero(MzInstrs.LeftArmMod, true);//Mark fullalove
            SetAutoZero(MzInstrs.RightArmMod, true);
            SetAutoZero(MzInstrs.TapComplementary, true);

            // Clear traces
            //MzInstrs.LeftArmMod.ClearSweepTraceData();
            //MzInstrs.RightArmMod.ClearSweepTraceData();   
            
            //MzInstrs.TapComplementary.ClearSweepTraceData();
            //if (MzInstrs.InlineTapOnline)
            //    MzInstrs.TapInline.ClearSweepTraceData();

            // Outputs on

            //double vMax = (Math.Abs(vStart) > Math.Abs(vStop)) ?vStart : vStop;
             
            //EnableAllOutputs(false);
            //MzInstrs.RightArmMod.SenseVoltage(vMax,Math .Abs (vMax));
            //MzInstrs.LeftArmMod.SenseVoltage(vMax,Math .Abs (vMax ));
            EnableAllOutputs(true);

            // Wake up OPM
            //MzInstrs.PowerMeter.EnableInputTrigger(false);

            // Configure OPM for triggered sweep
            //MzInstrs.PowerMeter.StopDataLogging();
            System.Threading.Thread.Sleep(50);

            // Alice.Huang   2010-03-04
            // in sz, the powermeter  is 8153 , which doesn/t support the average time , so it's commented

            //MzInstrs.PowerMeter.AveragingTime_s = this.averagingTime;
            MzInstrs.PowerMeter.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_mW;

            double[] uncalibratedPower_mW = new double[nbrPoints];
            double[] cur_L_A = new double[nbrPoints];   // Left Imb current 
            double[] cur_R_A = new double[nbrPoints];   // Right Imb current
            double[] Vol_L_V = new double[nbrPoints];   // Left RF voltage
            double[] Vol_R_V = new double[nbrPoints];   // Right RF Voltage

            double[] dac_L = new double[nbrPoints];   // Left imb Current Dac
            double[] dac_R = new double[nbrPoints];   // Right imb current Dac
            double[] cur_CTap_A = new double[nbrPoints];   // compliance Tap Current
            double[] cur_Tap_A = new double[nbrPoints];    // inline Tap Current 

            // sweep imb arms

            double vstep = (vStop - vStart) / (nbrPoints - 1);
           

            for (int i = 0; i < nbrPoints; i++)
            {
                if (sourceMeterEnum == SourceMeter.LeftModArm)
                {
                    SetVoltage (MzInstrs .LeftArmMod , vStart + i * vstep);                   

                }
                else
                {
                    SetVoltage(MzInstrs.RightArmMod, vStart + i * vstep);
                }
                System.Threading.Thread.Sleep(voltageSettledTime_mS);
                cur_L_A[i] = MzInstrs.FCU2Asic.IimbLeft_mA/1000;
                dac_L[i] = MzInstrs.FCU2Asic.IimbLeft_Dac;
                cur_R_A[i] = MzInstrs.FCU2Asic.IimbLeft_mA/1000;
                dac_R[i] = MzInstrs.FCU2Asic.IimbRight_Dac;

                Vol_L_V[i] = MzInstrs.LeftArmMod.VoltageActual_Volt;
                Vol_R_V[i] = MzInstrs.RightArmMod.VoltageActual_Volt;

                cur_CTap_A[i] = MzInstrs.TapComplementary.CurrentActual_amp ;
                if (MzInstrs.TapInline != null)
                    cur_Tap_A[i] = MzInstrs.TapInline.CurrentActual_amp;
                uncalibratedPower_mW[i] = MzInstrs.PowerMeter.ReadPower();
            }

            sweepData.SweepData[ILMZSweepDataType.FibrePower_mW] = OpticalPowerCal.GetCorrectedPower_mW(
                                        OpticalPowerHead.MZHead, uncalibratedPower_mW, Measurements.FrequencyWithoutMeter);
            //sweepResult.SweepData[ILMZSweepDataType.LeftArmImb_A] = cur_L_A;
            //sweepResult.SweepData[ILMZSweepDataType.RightArmImb_A] = cur_R_A ;

            //sweepResult.SweepData.Add(ILMZSweepDataType.FibrePower_mW,
            //    OpticalPowerCal.GetCorrectedPower_mW(OpticalPowerHead.MZHead,
            //    uncalibratedPower_mW, Measurements.CurrentFrequency_GHz));
            sweepData.SweepData.Add(ILMZSweepDataType.LeftArmImb_A, cur_L_A);
            sweepData.SweepData.Add(ILMZSweepDataType.LeftArmImb_I_Dac, dac_L);
            sweepData.SweepData.Add(ILMZSweepDataType.RightArmImb_A, cur_R_A);
            sweepData.SweepData.Add(ILMZSweepDataType.RightArmImb_I_dac, dac_R);

            sweepData.SweepData.Add(ILMZSweepDataType.LeftArmModBias_V, Vol_L_V);
            sweepData.SweepData.Add(ILMZSweepDataType.RightArmModBias_V, Vol_R_V);
            sweepData.SweepData.Add(ILMZSweepDataType.TapComplementary_mA, cur_CTap_A);

            if (MzInstrs.TapInline != null)
                sweepData.SweepData.Add(ILMZSweepDataType.TapInline_mA, cur_Tap_A);         
       

            
            // Turn outputs off            
            MzInstrs.FCU2Asic.IimbLeft_mA = 0;
            MzInstrs.FCU2Asic.IimbRight_mA = 0;           
            EnableAllOutputs(false);

            // If we don't do this the power meter won't play any more
            
            // For each meter            // :FORM ASC            // :SYST:AZER:STAT ON
            CleanupAfterSweep();

            return sweepData;
        }
        
        
        // Alice.Huang  2010-02-03
        // Add imb sweep functions with FCU2ASIC

        /// <summary>
        /// imb single ended sweep, sweep will start from iStart and end with iStop
        /// </summary>
        /// <param name="sourceMeterEnum"> to indentify which imb that this sweep carry on</param>
        /// <param name="iStart_mA">Imb sweep start current </param>
        /// <param name="iStop_mA">Imb sweep stop current </param>
        /// <param name="nbrPoints">Number of points </param>
        /// <param name="currentSettledTime_mS"> time has to wait to get a stable reading from instrument 
        /// after a new current apply on imb </param>
        /// <returns>
        /// Left imb current & DAC, .
        /// Right imb current & Dac
        /// left & Right RF Voltage
        /// Ctap & Tap ( if source exists) currents
        /// opotical power in mW       
        /// </returns>
        private ILMZSweepResult ImbSingledEndedSweepWithAsic(SourceMeter sourceMeterEnum,  
            double iStart_mA, double iStop_mA, int nbrPoints, int currentSettledTime_mS)
        {
            if (nbrPoints <2) throw new ArgumentException( 
                " can't carry a sweep with data points with " + nbrPoints.ToString());
            if (sourceMeterEnum != SourceMeter.RightImbArm &&
                sourceMeterEnum != SourceMeter.LeftImbArm) throw new ArgumentException(
                  " can't do imb single ended sweep on " + sourceMeterEnum.ToString());
            ILMZSweepResult sweepData = new ILMZSweepResult();
            sweepData.SrcMeter = sourceMeterEnum;
            sweepData.Type = SweepType.SingleEndCurrent;
            // Clear traces
            //MzInstrs.LeftArmMod.ClearSweepTraceData();
            //MzInstrs.RightArmMod.ClearSweepTraceData();            
            
            //MzInstrs.TapComplementary.ClearSweepTraceData();
            //if (MzInstrs.InlineTapOnline)
            //    MzInstrs.TapInline.ClearSweepTraceData();
           
            // Outputs on
            EnableAllOutputs(true );

            // Wake up OPM
            //MzInstrs.PowerMeter.EnableInputTrigger(false);
            // Configure OPM for triggered sweep
            //MzInstrs.PowerMeter.StopDataLogging();
            System.Threading.Thread.Sleep(500);

            //MzInstrs.PowerMeter.AveragingTime_s = this.averagingTime;
            SetAutoZero(MzInstrs.RightArmMod, true);
            SetAutoZero(MzInstrs.LeftArmMod, true);
            SetAutoZero(MzInstrs.TapComplementary, true);

            MzInstrs .PowerMeter .Mode = InstType_OpticalPowerMeter.MeterMode .Absolute_mW ;

            double[] uncalibratedPower_mW = new double[nbrPoints];
            double[] cur_L_A = new double[nbrPoints];   // Left Imb current 
            double[] cur_R_A = new double[nbrPoints];   // Right Imb current
            double[] Vol_L_V = new double[nbrPoints];   // Left RF voltage
            double[] Vol_R_V = new double[nbrPoints];   // Right RF Voltage

            double[] dac_L = new double[nbrPoints];   // Left imb Current Dac
            double[] dac_R = new double[nbrPoints];   // Right imb current Dac
            double[] cur_CTap_A = new double[nbrPoints];   // compliance Tap Current
            double[] cur_Tap_A = new double[nbrPoints];    // inline Tap Current 

            // sweep imb arms

            double iStep = (iStop_mA - iStart_mA )/(nbrPoints -1) ;
            for (int i = 0; i < nbrPoints; i++)
            {
                if (sourceMeterEnum == SourceMeter.LeftImbArm)
                {
                    MzInstrs.FCU2Asic.IimbLeft_mA = (float )(iStart_mA + i * iStep);
                    
                }
                else 
                {
                    MzInstrs.FCU2Asic.IimbRight_mA = (float)(iStart_mA + i * iStep);
                }
                System .Threading .Thread .Sleep(currentSettledTime_mS );
                cur_L_A[i] = MzInstrs.FCU2Asic.IimbLeft_mA/1000;
                dac_L[i] = MzInstrs.FCU2Asic.IimbLeft_Dac;
                cur_R_A[i] = MzInstrs.FCU2Asic.IimbLeft_mA/1000;
                dac_R[i] = MzInstrs.FCU2Asic.IimbRight_Dac;

                Vol_L_V[i] = MzInstrs.LeftArmMod.VoltageActual_Volt;
                Vol_R_V[i] = MzInstrs.RightArmMod.VoltageActual_Volt;

                cur_CTap_A[i] = MzInstrs.TapComplementary.CurrentActual_amp ;
                if (MzInstrs.TapInline != null) 
                    cur_Tap_A[i] = MzInstrs.TapInline.CurrentActual_amp;
                uncalibratedPower_mW[i] = MzInstrs.PowerMeter.ReadPower();
            }           
            
            //sweepResult.SweepData[ILMZSweepDataType.FibrePower_mW] = OpticalPowerCal.GetCorrectedPower_mW(OpticalPowerHead.MZHead, uncalibratedPower_mW, Measurements.CurrentFrequency_GHz);            
            //sweepResult.SweepData[ILMZSweepDataType.LeftArmImb_A] = cur_L_A ;                
            //sweepResult.SweepData[ILMZSweepDataType.RightArmImb_A] = cur_R_A;

            sweepData .SweepData .Add(ILMZSweepDataType.FibrePower_mW , 
                OpticalPowerCal.GetCorrectedPower_mW(OpticalPowerHead.MZHead ,
                uncalibratedPower_mW ,Measurements .CurrentFrequency_GHz ));
            sweepData.SweepData.Add(ILMZSweepDataType .LeftArmImb_A ,cur_L_A );
            sweepData.SweepData.Add(ILMZSweepDataType.LeftArmImb_I_Dac, dac_L);
            sweepData.SweepData.Add(ILMZSweepDataType.RightArmImb_A, cur_R_A);
            sweepData.SweepData.Add(ILMZSweepDataType.RightArmImb_I_dac, dac_R);

            sweepData.SweepData.Add(ILMZSweepDataType.LeftArmModBias_V, Vol_L_V);
            sweepData.SweepData.Add(ILMZSweepDataType.RightArmModBias_V, Vol_R_V);
            sweepData.SweepData.Add(ILMZSweepDataType.TapComplementary_mA, cur_CTap_A);

            if (MzInstrs.TapInline != null) 
                sweepData.SweepData.Add(ILMZSweepDataType.TapInline_mA, cur_Tap_A);         

            // Turn outputs off
            EnableAllOutputs(false);
            MzInstrs.FCU2Asic.IimbLeft_mA = 0;
            MzInstrs.FCU2Asic.IimbRight_mA = 0;
           
            // For each meter
            // :FORM ASC
            // :SYST:AZER:STAT ON
            CleanupAfterSweep();

            return sweepData;
        }
        /// <summary>
        /// imb defferential sweep, 
        /// Left imb starts from iStart and end at iStop
        /// Right imb starts from iStop and end at iStart
        /// </summary>
        /// <param name="iStart">Imb sweep start current</param>
        /// <param name="iStop">Imb sweep stop current</param>
        /// <param name="nbrPoints">Number of points</param>
        /// <param name="currentSettledTime_mS">time has to wait to get a stable reading from instrument 
        /// after a new current apply on imb</param>
        /// <returns>
        /// Left imb current and DAC, .
        /// Right imb current and Dac
        /// left & Right RF Voltage
        /// Ctap & Tap ( if source exists) currents
        /// opotical power in mW   
        /// </returns>
        private ILMZSweepResult ImbDifferentialSweepWithAsic(double iStart, double iStop,
                                        int nbrPoints, int currentSettledTime_mS, bool ImbArmSourceByAsic)
        {
            if (nbrPoints < 2) throw new ArgumentException(
                 " can't carry a sweep with data points with " + nbrPoints.ToString());

            ILMZSweepResult sweepData = new ILMZSweepResult();
            sweepData.Type = SweepType.DifferentialCurrent;
            // Clear traces
            //MzInstrs.LeftArmMod.ClearSweepTraceData();
            //MzInstrs.RightArmMod.ClearSweepTraceData();

            //MzInstrs.TapComplementary.ClearSweepTraceData();
            //if (MzInstrs.InlineTapOnline)
            //    MzInstrs.TapInline.ClearSweepTraceData();

            // Outputs on
            if (!ImbArmSourceByAsic)
            {
                EnableAllOutputs(true);
            }

            // Wake up OPM
            MzInstrs.PowerMeter.EnableInputTrigger(false);
            MzInstrs.PowerMeter.EnableOutputTrigger(false);
             //Configure OPM for triggered sweep
            //MzInstrs.PowerMeter.StopDataLogging();
           
            System.Threading.Thread.Sleep(500);

            
            /*SetAutoZero(MzInstrs.TapComplementary, true);
            SetAutoZero(MzInstrs.LeftArmMod, true);
            SetAutoZero(MzInstrs.RightArmMod, true);*/ //Echo remed this block

            MzInstrs.PowerMeter.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_mW;

            double[] uncalibratedPower_mW = new double[nbrPoints];
            double[] cur_L_A = new double[nbrPoints];   // Left Imb current 
            double[] cur_R_A = new double[nbrPoints];   // Right Imb current
            double[] Vol_L_V = new double[nbrPoints];   // Left RF voltage
            double[] Vol_R_V = new double[nbrPoints];   // Right RF Voltage

            double[] cur_L_R_A = new double[nbrPoints]; 

            double[] dac_L = new double[nbrPoints];   // Left imb Current Dac
            double[] dac_R = new double[nbrPoints];   // Right imb current Dac
            double[] cur_CTap_A = new double[nbrPoints];   // compliance Tap Current
            double[] cur_Tap_A = new double[nbrPoints];    // inline Tap Current 

            // sweep imb arms

            double iStep = (iStop - iStart) / (nbrPoints - 1);
            for (int i = 0; i < nbrPoints; i++)
            {
                MzInstrs.FCU2Asic.IimbLeft_mA = (float) (iStart + i * iStep);
                MzInstrs.FCU2Asic.IimbRight_mA = (float) (iStop - i * iStep);
                
                System.Threading.Thread.Sleep(currentSettledTime_mS);
                cur_L_A[i] = MzInstrs.FCU2Asic.IimbLeft_mA/1000;
                dac_L[i] = MzInstrs.FCU2Asic.IimbLeft_Dac;
                cur_R_A[i] = MzInstrs.FCU2Asic.IimbRight_mA / 1000;
                dac_R[i] = MzInstrs.FCU2Asic.IimbRight_Dac;

                cur_L_R_A[i] = cur_L_A[i] - cur_R_A[i];

                Vol_L_V[i] = MzInstrs.FCU2Asic.VLeftModBias_Volt;
                Vol_R_V[i] = MzInstrs.FCU2Asic.VRightModBias_Volt;
                cur_CTap_A[i] = MzInstrs.FCU2Asic.ICtap_mA; // by tim
                if (MzInstrs.TapInline != null)
                    cur_Tap_A[i] = MzInstrs.TapInline.CurrentActual_amp;
                uncalibratedPower_mW[i] = MzInstrs.PowerMeter.ReadPower();

                if (ImbSweepProgressChanged != null)
                {
                    int percentage =(int)Math.Round( i / (nbrPoints*1.0 - 1) * 100);
                    ProgressChangedEventArgs arg = new ProgressChangedEventArgs(percentage, null);
                    ImbSweepProgressChanged(MzInstrs.FCU2Asic, arg);
                }                
            }

            sweepData.SweepData.Add(ILMZSweepDataType.LeftArmImb_A, cur_L_A);
            sweepData.SweepData.Add(ILMZSweepDataType.RightArmImb_A, cur_R_A);            
            sweepData.SweepData.Add(ILMZSweepDataType.LeftArmImb_I_Dac, dac_L);            
            sweepData.SweepData.Add(ILMZSweepDataType.RightArmImb_I_dac, dac_R);
            sweepData.SweepData.Add(ILMZSweepDataType.LeftArmModBias_V, Vol_L_V);
            sweepData.SweepData.Add(ILMZSweepDataType.RightArmModBias_V, Vol_R_V);
            sweepData.SweepData.Add(ILMZSweepDataType.LeftMinusRight, cur_L_R_A);
            if (isCalibratePower)
            {
                sweepData.SweepData.Add(ILMZSweepDataType.FibrePower_mW,
                    OpticalPowerCal.GetCorrectedPower_mW(OpticalPowerHead.MZHead,
                    uncalibratedPower_mW, Measurements.FrequencyWithoutMeter));
            }
            else
            {
                sweepData.SweepData.Add(ILMZSweepDataType.FibrePower_mW, uncalibratedPower_mW);
            }

            sweepData.SweepData.Add(ILMZSweepDataType.TapComplementary_mA, cur_CTap_A);

            if (MzInstrs.TapInline != null)
                sweepData.SweepData.Add(ILMZSweepDataType.TapInline_mA, cur_Tap_A);         

            // Turn outputs off
            if (!ImbArmSourceByAsic)
            {
                EnableAllOutputs(false);
            }
            else
            {
                //MzInstrs.FCU2Asic.Close();
            }

            return sweepData;
        }

        /// <summary>
        /// Create imbDiff by tim to combine solution
        /// </summary>
        /// <param name="iStart"></param>
        /// <param name="iStop"></param>
        /// <param name="nbrPoints"></param>
        /// <param name="currentSettledTime_mS"></param>
        /// <param name="ImbArmSourceByAsic"></param>
        /// <param name="isCalibratePower"></param>
        /// <returns></returns>
        private ILMZSweepResult ImbDifferentialSweepWithAsic(double iStart, double iStop,
                                     int nbrPoints, int currentSettledTime_mS, bool ImbArmSourceByAsic, bool isCalibratePower)
        {
            if (nbrPoints < 2) throw new ArgumentException(
                 " can't carry a sweep with data points with " + nbrPoints.ToString());

            ILMZSweepResult sweepData = new ILMZSweepResult();
            sweepData.Type = SweepType.DifferentialCurrent;
            // Clear traces
            //MzInstrs.LeftArmMod.ClearSweepTraceData();
            //MzInstrs.RightArmMod.ClearSweepTraceData();

            //MzInstrs.TapComplementary.ClearSweepTraceData();
            //if (MzInstrs.InlineTapOnline)
            //    MzInstrs.TapInline.ClearSweepTraceData();

            // Outputs on
            if (!ImbArmSourceByAsic)
            {
                EnableAllOutputs(true);
            }

            // Wake up OPM
            MzInstrs.PowerMeter.EnableInputTrigger(false);
            MzInstrs.PowerMeter.EnableOutputTrigger(false);
            //Configure OPM for triggered sweep
            //MzInstrs.PowerMeter.StopDataLogging();

            System.Threading.Thread.Sleep(500);


            /*SetAutoZero(MzInstrs.TapComplementary, true);
            SetAutoZero(MzInstrs.LeftArmMod, true);
            SetAutoZero(MzInstrs.RightArmMod, true);*/
            //Echo remed this block

            MzInstrs.PowerMeter.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_mW;

            double[] uncalibratedPower_mW = new double[nbrPoints];
            double[] cur_L_A = new double[nbrPoints];   // Left Imb current 
            double[] cur_R_A = new double[nbrPoints];   // Right Imb current
            double[] Vol_L_V = new double[nbrPoints];   // Left RF voltage
            double[] Vol_R_V = new double[nbrPoints];   // Right RF Voltage

            double[] cur_L_R_A = new double[nbrPoints];

            double[] dac_L = new double[nbrPoints];   // Left imb Current Dac
            double[] dac_R = new double[nbrPoints];   // Right imb current Dac
            double[] cur_CTap_A = new double[nbrPoints];   // compliance Tap Current
            double[] cur_Tap_A = new double[nbrPoints];    // inline Tap Current 

            // sweep imb arms

            double iStep = (iStop - iStart) / (nbrPoints - 1);
            for (int i = 0; i < nbrPoints; i++)
            {
                MzInstrs.FCU2Asic.IimbLeft_mA = (float)(iStart + i * iStep);
                MzInstrs.FCU2Asic.IimbRight_mA = (float)(iStop - i * iStep);

                System.Threading.Thread.Sleep(currentSettledTime_mS);
                cur_L_A[i] = MzInstrs.FCU2Asic.IimbLeft_mA / 1000;
                dac_L[i] = MzInstrs.FCU2Asic.IimbLeft_Dac;
                cur_R_A[i] = MzInstrs.FCU2Asic.IimbRight_mA / 1000;
                dac_R[i] = MzInstrs.FCU2Asic.IimbRight_Dac;

                cur_L_R_A[i] = cur_L_A[i] - cur_R_A[i];

                Vol_L_V[i] = MzInstrs.FCU2Asic.VLeftModBias_Volt;
                Vol_R_V[i] = MzInstrs.FCU2Asic.VRightModBias_Volt;
                cur_CTap_A[i] = MzInstrs.FCU2Asic.ICtap_mA; // by tim
                if (MzInstrs.TapInline != null)
                    cur_Tap_A[i] = MzInstrs.TapInline.CurrentActual_amp;
                uncalibratedPower_mW[i] = MzInstrs.PowerMeter.ReadPower();
            }

            //sweepResult.SweepData[ILMZSweepDataType.FibrePower_mW] = OpticalPowerCal.GetCorrectedPower_mW(OpticalPowerHead.MZHead, uncalibratedPower_mW, Measurements.CurrentFrequency_GHz);
            //sweepResult.SweepData[ILMZSweepDataType.LeftArmImb_A] = cur_L_A ;
            //sweepResult.SweepData[ILMZSweepDataType.RightArmImb_A] = cur_R_A ;

            sweepData.SweepData.Add(ILMZSweepDataType.LeftArmImb_A, cur_L_A);
            sweepData.SweepData.Add(ILMZSweepDataType.RightArmImb_A, cur_R_A);
            if (isCalibratePower)
            {
                sweepData.SweepData.Add(ILMZSweepDataType.FibrePower_mW,
                    OpticalPowerCal.GetCorrectedPower_mW(OpticalPowerHead.MZHead,
                    uncalibratedPower_mW, Measurements.FrequencyWithoutMeter));
            }
            else
            {
                sweepData.SweepData.Add(ILMZSweepDataType.FibrePower_mW, uncalibratedPower_mW);
            }

            sweepData.SweepData.Add(ILMZSweepDataType.LeftArmImb_I_Dac, dac_L);

            sweepData.SweepData.Add(ILMZSweepDataType.RightArmImb_I_dac, dac_R);

            sweepData.SweepData.Add(ILMZSweepDataType.LeftArmModBias_V, Vol_L_V);
            sweepData.SweepData.Add(ILMZSweepDataType.RightArmModBias_V, Vol_R_V);
            sweepData.SweepData.Add(ILMZSweepDataType.TapComplementary_mA, cur_CTap_A);
            sweepData.SweepData.Add(ILMZSweepDataType.LeftMinusRight, cur_L_R_A);


            if (MzInstrs.TapInline != null)
                sweepData.SweepData.Add(ILMZSweepDataType.TapInline_mA, cur_Tap_A);

            // Turn outputs off
            if (!ImbArmSourceByAsic)
            {
                EnableAllOutputs(false);
            }
            else
            {
                //MzInstrs.FCU2Asic.Close();
            }

            //MzInstrs.FCU2Asic.IimbLeft_mA = 0;
            //MzInstrs.FCU2Asic.IimbRight_mA = 0;

            // For each meter
            // :FORM ASC
            // :SYST:AZER:STAT ON
            //CleanupAfterSweep();

            return sweepData;
        }

        /// <summary>
        /// Create imbDiff by Jack.zhang to use FCUMKI trigger PM
        /// </summary>
        /// <param name="iStart"></param>
        /// <param name="iStop"></param>
        /// <param name="nbrPoints"></param>
        /// <param name="currentSettledTime_mS"></param>
        /// <param name="ImbArmSourceByAsic"></param>
        /// <param name="isCalibratePower"></param>
        /// <returns></returns>
        public ILMZSweepResult ImbDifferentialSweepWithAsicByTrigger(double leftModArmVoltage_V, double rightModArmVoltage_V,
                                                                    double iStart, double iStop,int nbrPoints, 
                                                                    double inlineTap_V, double complementaryTap_V,
                                                                    bool ImbArmSourceByAsic, bool isCalibratePower)
        {
            if (nbrPoints < 2) throw new ArgumentException(
                 " can't carry a sweep with data points with " + nbrPoints.ToString());

            double[] cur_L_mA = new double[nbrPoints];   // Left Imb current 
            double[] cur_R_mA = new double[nbrPoints];   // Right Imb current
            double[] Vol_L_V = new double[nbrPoints];   // Left RF voltage
            double[] Vol_R_V = new double[nbrPoints];   // Right RF Voltage

            double[] cur_L_R_mA = new double[nbrPoints];

            double[] dac_L = new double[nbrPoints];   // Left imb Current Dac
            double[] dac_R = new double[nbrPoints];   // Right imb current Dac

            MzInstrs.FCU2Asic.VLeftModBias_Volt = (float)leftModArmVoltage_V;
            MzInstrs.FCU2Asic.VRightModBias_Volt = (float)rightModArmVoltage_V;

            int iStartDAC = MzInstrs.FCU2Asic.mAToDacForLeftImb_Asic((float)iStart*1000);
            int iStopDAC = MzInstrs.FCU2Asic.mAToDacForRightImb_Asic((float)iStop*1000);
            int currentSettledTime_mS = 5;
            ILMZSweepResult sweepData = new ILMZSweepResult();
            sweepData.Type = SweepType.DifferentialCurrent;

            // Wake up OPM
            MzInstrs.PowerMeter.EnableInputTrigger(false);
            MzInstrs.PowerMeter.ConfigureDataLogging(nbrPoints, currentSettledTime_mS / 1000.0);
            MzInstrs.PowerMeter.EnableInputTrigger(true);

            MzInstrs.PowerMeter.StartDataLogging();
            System.Threading.Thread.Sleep(1000);

            MzInstrs.FCU2Asic.AsicImbDiffTrig(iStartDAC, iStopDAC, nbrPoints, currentSettledTime_mS);
            System.Threading.Thread.Sleep(nbrPoints * currentSettledTime_mS + 500);
            double[] uncalibratedPower_mW = MzInstrs.PowerMeter.GetSweepData();
            double[] cur_CTap_A = MzInstrs.FCU2Asic.GetReferenceAfterMZsweep();
            MzInstrs.PowerMeter.StopDataLogging();
            MzInstrs.PowerMeter.EnableInputTrigger(false);

            MzInstrs.PowerMeter.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_mW;

            // sweep imb arms

            double iStep = (iStop - iStart) / (nbrPoints - 1);
            for (int i = 0; i < nbrPoints; i++)
            {
                cur_L_mA[i] =iStart + i * iStep;
                dac_L[i] = MzInstrs.FCU2Asic.mAToDacForLeftImb_Asic((float)cur_L_mA[i]);
                cur_R_mA[i] = iStop - i * iStep;
                dac_R[i] = MzInstrs.FCU2Asic.mAToDacForRightImb_Asic((float)cur_R_mA[i]);

                cur_L_R_mA[i] = cur_L_mA[i] - cur_R_mA[i];

                Vol_L_V[i] = leftModArmVoltage_V;
                Vol_R_V[i] = rightModArmVoltage_V;
            }

            sweepData.SweepData.Add(ILMZSweepDataType.LeftArmImb_A, cur_L_mA);
            sweepData.SweepData.Add(ILMZSweepDataType.RightArmImb_A, cur_R_mA);
            sweepData.SweepData.Add(ILMZSweepDataType.LeftArmImb_I_Dac, dac_L);
            sweepData.SweepData.Add(ILMZSweepDataType.RightArmImb_I_dac, dac_R);
            sweepData.SweepData.Add(ILMZSweepDataType.LeftArmModBias_V, Vol_L_V);
            sweepData.SweepData.Add(ILMZSweepDataType.RightArmModBias_V, Vol_R_V);
            sweepData.SweepData.Add(ILMZSweepDataType.LeftMinusRight, cur_L_R_mA);
            sweepData.SweepData.Add(ILMZSweepDataType.FibrePower_mW, uncalibratedPower_mW);
            sweepData.SweepData.Add(ILMZSweepDataType.TapComplementary_mA, cur_CTap_A);
            return sweepData;
        }

        /// <summary>
        /// imb defferential sweep, 
        /// Left imb starts from iStart and end at iStop
        /// Right imb starts from iStop and end at iStart
        /// </summary>
        /// <param name="iStartDAC">Imb sweep start current</param>
        /// <param name="iStopDAC">Imb sweep stop current</param>
        /// <param name="nbrPoints">Number of points</param>
        /// <param name="currentSettledTime_mS">time has to wait to get a stable reading from instrument 
        /// after a new current apply on imb</param>
        /// <returns>
        /// Left imb current and DAC, .
        /// Right imb current and Dac
        /// left & Right RF Voltage
        /// Ctap & Tap ( if source exists) currents
        /// opotical power in mW   
        /// </returns>
        private ILMZSweepResult ImbDiffSweepWithAsicFcu(int iStartDAC, int iStopDAC,
                                        int nbrPoints, int currentSettledTime_mS, bool ImbArmSourceByAsic)
        {
            if (nbrPoints < 2) throw new ArgumentException(
                 " can't carry a sweep with data points with " + nbrPoints.ToString());

            ILMZSweepResult sweepData = new ILMZSweepResult();
            sweepData.Type = SweepType.DifferentialCurrent;
           
            // Outputs on
            if (!ImbArmSourceByAsic)
            {
                EnableAllOutputs(true);
            }

            MzInstrs.PowerMeter.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_mW;

            // Wake up OPM
            MzInstrs.PowerMeter.EnableInputTrigger(false);
           // MzInstrs.PowerMeter.EnableOutputTrigger(false);


            System.Threading.Thread.Sleep(500);
            int delaytime = 5;

            MzInstrs.PowerMeter.ConfigureDataLogging(nbrPoints, delaytime / 1000.0);
            MzInstrs.PowerMeter.EnableInputTrigger(true);
            MzInstrs.PowerMeter.StartDataLogging();
                    


            MzInstrs.FCU2Asic.AsicImbDiffTrig(iStartDAC, iStopDAC, nbrPoints, delaytime);
            System.Threading.Thread.Sleep(1500);


            double[] uncalibratedPower_mW = MzInstrs.PowerMeter.GetSweepData();
            double[] Ctap_mA = MzInstrs.FCU2Asic.GetCtapAfterMZsweep();
            ////if (lvsweepType != LvSweepType.Diff && exchangeVolt)
            //{
            //    uncalibratedPower_mW = Alg_ArrayFunctions.ReverseArray(uncalibratedPower_mW);
            //    Ctap_mA = Alg_ArrayFunctions.ReverseArray(Ctap_mA);
            //}
            sweepData.SweepData[ILMZSweepDataType.FibrePower_mW] = OpticalPowerCal.GetCorrectedPower_mW(OpticalPowerHead.MZHead, uncalibratedPower_mW, Measurements.FrequencyWithoutMeter);

            sweepData.SweepData[ILMZSweepDataType.TapComplementary_mA] = Ctap_mA;

            // If we don't do this the power meter won't play any more
            MzInstrs.PowerMeter.StopDataLogging();
            MzInstrs.PowerMeter.EnableInputTrigger(false);


            //double[] uncalibratedPower_mW = new double[nbrPoints];
            double[] cur_L_A = new double[nbrPoints];   // Left Imb current 
            double[] cur_R_A = new double[nbrPoints];   // Right Imb current
            double[] Vol_L_V = new double[nbrPoints];   // Left RF voltage
            double[] Vol_R_V = new double[nbrPoints];   // Right RF Voltage

            //double[] cur_L_R_A = new double[nbrPoints];

            double[] dac_L = new double[nbrPoints];   // Left imb Current Dac
            double[] dac_R = new double[nbrPoints];   // Right imb current Dac
            //double[] cur_CTap_A = new double[nbrPoints];   // compliance Tap Current
            double[] cur_Tap_A = new double[nbrPoints];    // inline Tap Current 

            // sweep imb arms

            float iStop = MzInstrs.FCU2Asic.Dac2mAForLeftImb_Asic(iStopDAC);
            float iStart = MzInstrs.FCU2Asic.Dac2mAForLeftImb_Asic(iStartDAC);
            
            double iStep = (iStop - iStart) / (nbrPoints - 1);
            int iStepDac = (iStopDAC - iStartDAC) / (nbrPoints - 1);

            for (int i = 0; i < nbrPoints; i++)
            {
                //MzInstrs.FCU2Asic.IimbLeft_mA = (float)(iStart + i * iStep);
                //MzInstrs.FCU2Asic.IimbRight_mA = (float)(iStop - i * iStep);

                //System.Threading.Thread.Sleep(currentSettledTime_mS);

                cur_L_A[i] = (iStart + i * iStep)/1000;
                dac_L[i] = iStartDAC + i * iStepDac;
                cur_R_A[i] = (iStop - i * iStep)/1000;
                dac_R[i] = iStopDAC - i * iStepDac;
                                
                Vol_L_V[i] = MzInstrs.FCU2Asic.VLeftModBias_Volt;
                Vol_R_V[i] = MzInstrs.FCU2Asic.VRightModBias_Volt;
                
                if (MzInstrs.TapInline != null)
                    cur_Tap_A[i] = MzInstrs.TapInline.CurrentActual_amp;                
            }

           
            sweepData.SweepData.Add(ILMZSweepDataType.LeftArmImb_A, cur_L_A);
            sweepData.SweepData.Add(ILMZSweepDataType.RightArmImb_A, cur_R_A);
            //if (isCalibratePower)
            //{
            //    sweepResult.SweepData.Add(ILMZSweepDataType.FibrePower_mW,
            //        OpticalPowerCal.GetCorrectedPower_mW(OpticalPowerHead.MZHead,
            //        uncalibratedPower_mW, Measurements.FrequencyWithoutMeter));
            //}
            //else
            //{
            //    sweepResult.SweepData.Add(ILMZSweepDataType.FibrePower_mW, uncalibratedPower_mW);
            //}

            sweepData.SweepData.Add(ILMZSweepDataType.LeftArmImb_I_Dac, dac_L);

            sweepData.SweepData.Add(ILMZSweepDataType.RightArmImb_I_dac, dac_R);

            sweepData.SweepData.Add(ILMZSweepDataType.LeftArmModBias_V, Vol_L_V);
            sweepData.SweepData.Add(ILMZSweepDataType.RightArmModBias_V, Vol_R_V);
            //sweepResult.SweepData.Add(ILMZSweepDataType.TapComplementary_mA, Ctap_mA);
            

            if (MzInstrs.TapInline != null)
                sweepData.SweepData.Add(ILMZSweepDataType.TapInline_mA, cur_Tap_A);

            // Turn outputs off
            if (!ImbArmSourceByAsic)
            {
                EnableAllOutputs(false);
            }
            else
            {
                //MzInstrs.FCU2Asic.Close();
            }
           
            return sweepData;
        }
        /// <summary>
        /// Sets the mode of the imbalance contol arms to sense voltage.
        /// </summary>
        private void SetImbalanceVSense()
        {
            MzInstrs.LeftArmImb.SenseVoltage(voltageCompliance, voltageRange);	// It's safest to explicitly set the instrument to sense V here, as any spot-measurements may have reset the instrument to read A
            MzInstrs.RightArmImb.SenseVoltage(voltageCompliance, voltageRange);
        }

        /// <summary>
        /// Initialise the tap instruments for the sweep.
        /// </summary>
        /// <param name="numberOfPoints">Number of points in the sweep.</param>
        /// <param name="inlineTap_V">Fixed inline tap voltage.</param>
        /// <param name="complementaryTap_V">Fixed complementary tap voltage</param>
        private void SetupTapsForSweep(int numberOfPoints, double inlineTap_V, double complementaryTap_V)
        {
            // iTap
            if (MzInstrs.InlineTapOnline)
            {
                // set the current sense range
                double iComplianceInline = MzInstrs.TapInline.CurrentComplianceSetPoint_Amp;
                MzInstrs.TapInline.SenseCurrent(iComplianceInline, iComplianceInline);

                MzInstrs.TapInline.InitSourceVMeasureI_TriggeredFixedVoltage(inlineTap_V, numberOfPoints, triggerLineIn, triggerLineUnused);
            }

            // cTap
            // set the current sense range
            double iComplianceComp = MzInstrs.TapComplementary.CurrentComplianceSetPoint_Amp;
            MzInstrs.TapComplementary.SenseCurrent(iComplianceComp, iComplianceComp);
            if (DsdbrUtils.DsdbrDriverInstrsToUse != DsdbrDriveInstruments.FCU2AsicInstrument)
                MzInstrs.TapComplementary.InitSourceVMeasureI_TriggeredFixedVoltage(complementaryTap_V, numberOfPoints, triggerLineIn, triggerLineOut);
            else
            {
                //MzInstrs.TapComplementary.VoltageSetPoint_Volt = complementaryTap_V;
                //MzInstrs.TapComplementary.SourceFixedVoltage();
                MzInstrs.TapComplementary.InitSourceVMeasureI_UntriggeredSpotMeas();
                SetVoltage(MzInstrs.TapComplementary, complementaryTap_V);
            }
        }

        // Data
        /// <summary>
        /// MZ instruments
        /// </summary>
        public readonly IlMzInstruments MzInstrs;
        private int numberOfAverages;
        private double averagingTime;
        private double integrationRate;
        private double voltageCompliance;
        private double voltageRange;

        private int triggerLineOut = 1;
        private int triggerLineIn = 2;
        private int triggerLineUnused = 3;

        private int iDarkNumAverages = 50;

        /// <summary>
        /// 
        /// </summary>
        public static double MaxModBias_V;
        public event ProgressChangedEventHandler ImbSweepProgressChanged;
        public IInstType_DigiIOCollection opticalSwitch = null;
        public  int locker_port;
        public bool isCalibratePower = true;
        public enum LvSweepType
        {
            /// <summary>
            /// Left mod arm single ended sweep
            /// </summary>
            Left,
            /// <summary>
            /// right mod arm single ended sweep
            /// </summary>
            Right,
            /// <summary>
            /// mod arm differential sweep
            /// </summary>
            Diff
        }
        #endregion
    }
}

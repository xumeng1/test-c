using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.Algorithms;

namespace Bookham.TestLibrary.Utilities
{
    /// <summary>
    /// Class to measure SMSR from an OSA and return value in dBm.
    /// </summary>
    public class Util_SMSRMeasure
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="osa">OSA</param>
        /// <param name="smsrCentreOpticalFreq_GHz">Centre optical frequency of signal peak</param>
        /// <param name="smsrSpan_nm">Span - narrow span around ITU</param>
        /// <param name="smsrRBW_nm">Resolution bandwidth</param>
        /// <param name="smsrOSASensitivity_dBm">OSA sensitivity setting - set to resolve lower peaks</param>
        /// <param name="smsrNumPts">Number of trace points</param>
        /// <param name="smsrSweepTime_s">Sweep time - use OSA automatic setting</param>
        private Util_SMSRMeasure(InstType_OSA osa, double smsrCentreOpticalFreq_GHz, 
            double smsrSpan_nm, double smsrRBW_nm, double smsrOSASensitivity_dBm, int smsrNumPts, double smsrSweepTime_s)
        {
            this.osa = osa;
            this.smsrCentreOpticalFreq_GHz = smsrCentreOpticalFreq_GHz;
            this.smsrSpan_nm = smsrSpan_nm;
            this.smsrRBW_nm = smsrRBW_nm;
            this.smsrOSASensitivity_dBm = smsrOSASensitivity_dBm;
            this.smsrNumPts = smsrNumPts;
            this.smsrSweepTime_s = smsrSweepTime_s;
        }

        /// <summary>
        /// Measure SMSR, return value in dBm.
        /// </summary>
        /// <param name="osa">OSA</param>
        /// <param name="smsrCentreOpticalFreq_GHz">Centre optical frequency of signal peak</param>
        /// <param name="smsrSpan_nm">Span - narrow span around ITU</param>
        /// <param name="smsrRBW_nm">Resolution bandwidth</param>
        /// <param name="smsrOSASensitivity_dBm">OSA sensitivity setting - set to resolve lower peaks</param>
        /// <param name="smsrNumPts">Number of trace points</param>
        /// <param name="smsrSweepTime_s">Sweep time - use OSA automatic setting</param>
        /// <returns>SMSR in dBm</returns>
        public static double MeasureSMSR(InstType_OSA osa, double smsrCentreOpticalFreq_GHz,
            double smsrSpan_nm, double smsrRBW_nm, double smsrOSASensitivity_dBm, int smsrNumPts, double smsrSweepTime_s)
        {
            Util_SMSRMeasure smsrMeasure = new Util_SMSRMeasure(osa, smsrCentreOpticalFreq_GHz, smsrSpan_nm, smsrRBW_nm, smsrOSASensitivity_dBm, smsrNumPts, smsrSweepTime_s);

            smsrMeasure.osa.SweepStop();
            //check/set OSA sweepmode to single
            if (smsrMeasure.osa.SweepMode != InstType_OSA.SweepModes.Single)
            {
                smsrMeasure.osa.SweepMode = InstType_OSA.SweepModes.Single;
            }

            double centreWL = Alg_FreqWlConvert.Freq_GHz_TO_Wave_nm(smsrMeasure.smsrCentreOpticalFreq_GHz);
            smsrMeasure.osa.WavelengthStart_nm = centreWL - smsrMeasure.smsrSpan_nm;
            smsrMeasure.osa.WavelengthStop_nm = centreWL + smsrMeasure.smsrSpan_nm;
            smsrMeasure.osa.ResolutionBandwidth = smsrMeasure.smsrRBW_nm;
            //OSA sensitivity...
            //OSA sweep time...
            smsrMeasure.osa.TracePoints = smsrMeasure.smsrNumPts;


            double peakPower_dBm;
            double peakFrequency_GHz;
            double nextPeakPower_dBm;
            double nextPeakFrequency_GHz;
            double smsr_dBm;
            int sweepCount = 0;
            do
            {
                smsrMeasure.osa.SweepStart();
                do
                {
                    System.Threading.Thread.Sleep(100);
                } while (smsrMeasure.osa.Status != InstType_OSA.ChannelState.DataReady);

                smsrMeasure.osa.MarkerToPeak();
                peakPower_dBm = smsrMeasure.osa.MarkerAmplitude_dBm;
                peakFrequency_GHz = Alg_FreqWlConvert.Wave_nm_TO_Freq_GHz(smsrMeasure.osa.MarkerWavelength_nm);

                smsrMeasure.osa.MarkerToNextPeak(InstType_OSA.Direction.Unspecified);
                nextPeakPower_dBm = smsrMeasure.osa.MarkerAmplitude_dBm;
                nextPeakFrequency_GHz = Alg_FreqWlConvert.Wave_nm_TO_Freq_GHz(smsrMeasure.osa.MarkerWavelength_nm);

                smsr_dBm = peakPower_dBm - nextPeakPower_dBm;
                sweepCount++;
            } while (sweepCount < 3 && smsr_dBm < 10);//If smsr_dBm<10,try again unless reaching the max iterations 3

            if (smsr_dBm < 10)
            {
                if (Math.Abs(peakFrequency_GHz - nextPeakFrequency_GHz) < 15)
                {
                    smsrMeasure.osa.MarkerWavelength_nm = Alg_FreqWlConvert.Freq_GHz_TO_Wave_nm(peakFrequency_GHz + 60);//The value 60 may need to be determined from setup file---One case I don't know how to handle.
                    nextPeakPower_dBm = smsrMeasure.osa.MarkerAmplitude_dBm;
                    smsrMeasure.osa.MarkerWavelength_nm = Alg_FreqWlConvert.Freq_GHz_TO_Wave_nm(peakFrequency_GHz - 60);//The value 60 may need to be determined from setup file
                    nextPeakPower_dBm = Math.Max(nextPeakPower_dBm, smsrMeasure.osa.MarkerAmplitude_dBm);

                    smsr_dBm = peakPower_dBm - nextPeakPower_dBm;

                    if (smsr_dBm == 0)//OSA error
                    {
                        smsr_dBm = 999999;
                    }
                }//else. multimode
            }//else. correct value

            return smsr_dBm;
        }

        #region Private data
        private InstType_OSA osa;
        private double smsrCentreOpticalFreq_GHz;
        private double smsrSpan_nm;
        private double smsrRBW_nm;
        private double smsrOSASensitivity_dBm;
        private int smsrNumPts;
        private double smsrSweepTime_s;      
        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Bookham.TestSolution.TcmzCommonUtils
{
    public partial class TesterForm : Form
    {
        public TesterForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // Initialise data
            ts = new TestSelection(@"..\..\..\..\..\..\..\..\Configuration\TCMZ\TestSelect.xml");

            // Show data
            ShowTable(ts.TestSelectTable);

            // Populate combo boxes
            Frequency.Items.Add("Select Frequency");
            Frequency.SelectedIndex = 0;
            TestName.Items.Add("Select Test Name");
            TestName.SelectedIndex = 0;

            DataColumn freqColumn = new DataColumn();

            foreach (DataColumn col in ts.TestSelectTable.Columns)
            {
                if (col.Caption == "ITU Frequency")
                {
                    freqColumn = col;
                }
                else
                {
                    TestName.Items.Add(col.Caption);
                }
            }
            
            foreach (DataRow row in ts.TestSelectTable.Rows)
            {
                Frequency.Items.Add(row[freqColumn]);
            }
        }

        private void ShowTable(DataTable dataTable)
        {
            dataGridView1.DataSource = dataTable;
        }

        private void Frequency_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetStatus();
        }

        private void TestName_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetStatus();
        }

        private void SetStatus()
        {
            if (Frequency.SelectedIndex > 0 && TestName.SelectedIndex > 0)
            {
                double freq = Convert.ToDouble(Frequency.Text);
                string testName = TestName.Text;
                SelectedStatus.Text = ts.IsTestSelected(freq, testName) ? "True" : "False";
            }
        }

        private TestSelection ts;
    }
}
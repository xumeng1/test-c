using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestSolution.IlmzCommonUtils
{
        /// <summary>
        /// Channel failure code enumerations
        /// </summary>
        [Flags]
        public enum ChannelFailCode
        {
            /// <summary>
            /// No fails
            /// </summary>
            NoFail = 0,
            /// <summary>
            /// Power levelling fail
            /// </summary>
            PowerLevel = 1,
            /// <summary>
            /// ITU tuning fail
            /// </summary>
            ItuTuning = 2,
            /// <summary>
            /// SMSR fail
            /// </summary>
            SMSR = 4,
            /// <summary>
            /// Supermode SR fail
            /// </summary>
            SupermodeSR = 8,
            /// <summary>
            /// SOA shutter test fail
            /// </summary>
            SoaShutter = 16,
            /// <summary>
            /// Locker measurement out of spec
            /// </summary>
            LockerCurrent = 32,
            /// <summary>
            /// Locker slope test fail
            /// </summary>
            LockerSlope = 64,
            /// <summary>
            /// Phase tuning range fail
            /// </summary>
            PhaseTuneRange = 128,
            /// <summary>
            /// Calibration frequency measurement out of spec
            /// </summary>
            CalIphIrearOutOfSpec = 256,
            /// <summary>
            /// Over temperature frequency change out of spec
            /// </summary>
            FreqChange = 512,
            /// <summary>
            /// Over temperature fibre power change out of spec
            /// </summary>
            FibrePwrChange = 1024,
            /// <summary>
            /// SOA control range test fail
            /// </summary>
            SoaControlRange = 2048,
            /// <summary>
            /// Measured parameter out of spec
            /// </summary>
            ParamOutOfSpec = 4096
    }
    /// <summary>
    /// Channel failure Eol enumerations
    /// </summary>
    [Flags]
    public enum Re_Tune_Reason
    {
        /// <summary>
        /// NoReTune
        /// </summary>
        NoReTune = 0,
        /// <summary>
        /// PhaseTunePosFail
        /// </summary>
        PhaseTunePosFail = 1,
        /// <summary>
        /// PhaseTuneNegFail
        /// </summary>
        PhaseTuneNegFail = 2,
        /// <summary>
        /// SMSREOLPosFail
        /// </summary>
        SMSREOLPosFail = 4,
        /// <summary>
        /// SMSREOLNegFail
        /// </summary>
        SMSREOLNegFail = 8,
        /// <summary>
        /// Othercause
        /// </summary>
        Othercause = 16,

    }
}

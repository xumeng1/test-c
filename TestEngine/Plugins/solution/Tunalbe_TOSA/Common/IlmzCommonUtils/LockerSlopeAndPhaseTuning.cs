// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// LockerSlopeAndPhaseTuning.cs
//
// Author: Mark Fullalove, 2007
// Design: [Reference design documentation]

using System;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestSolution.TcmzCommonData;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestSolution.TcmzCommonInstrs;
using Bookham.TestLibrary.Utilities;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Static class to perform locker slope and phase tuning measurements.
    /// </summary>
    public class LockerSlopeAndPhaseTuning
    {
        // Empty constructor
        private LockerSlopeAndPhaseTuning()
        {
        }

        // Backup method !
        //public DatumList MeasureAtSingleChannel(double phaseStepSize_mA, int phaseStepDelay_mS, double maxPhaseCurrentLimit_mA, double minPhaseCurrentLimit_mA, DsdbrInstruments Instruments, IInstType_Wavemeter Wavemeter)
        /// <summary>
        /// Record locker slope and phase tuning efficiency
        /// </summary>
        /// <param name="refFreqAccuracy">The required tolerance between frequency setpoints and the measured frequency.</param>
        /// <param name="modeHopDetect_GHz">The size of frequency jump which is considered to be a mode hop.</param>
        /// <param name="freqSlope_p_GHz">An initial estimate for the slope of frequency against phase current.</param>
        /// <param name="timeout_s">The maximum time allowed to tune to frequency</param>
        /// <param name="instruments">A collection of instruments.</param>
        /// <param name="wavemeter">A reference to the wavemeters</param>
        /// <returns>A DatumList containing the parametric measurements.</returns>
        public DatumList MeasureAtSingleChannel(double refFreqAccuracy, double modeHopDetect_GHz, double freqSlope_p_GHz, double timeout_s, DsdbrInstruments instruments, IInstType_Wavemeter wavemeter)
        {
            // Get instruments
            phaseCurrentSource = instruments.CurrentSources[DSDBRSection.Phase];
            rxLockerSource = instruments.LockerRx;
            txLockerSource = instruments.LockerTx;
            this.wavemeter = wavemeter;
            
            // Get initial conditions
            LockerReadings measurements_ITU = GetReadings();

            // Tune to lower freq
            TuneFrequency(wavemeter, refFreqAccuracy, measurements_ITU.freq_gHz - 1000, modeHopDetect_GHz, timeout_s, freqSlope_p_GHz);
            //TuneToFrequency(measurements_ITU.freq_gHz - 1000, phaseStepSize_mA, phaseStepDelay_mS, maxPhaseCurrentLimit_mA, minPhaseCurrentLimit_mA);
            
            // Read at lower frequency
            LockerReadings measurements_lower = GetReadings();

            // Jump to upper freq. ( assuming that the phase offset is symmetrical )
            double deltaPhaseCurrent_lower = measurements_ITU.phase_mA - phaseCurrentSource.CurrentSetPoint_amp;
            double phaseCurrentForUpperFrequency = measurements_ITU.phase_mA + deltaPhaseCurrent_lower;
            // Sanity check
            //phaseCurrentForUpperFrequency = phaseCurrentForUpperFrequency > maxPhaseCurrentLimit_mA ? maxPhaseCurrentLimit_mA : phaseCurrentForUpperFrequency;
            phaseCurrentSource.CurrentSetPoint_amp = phaseCurrentForUpperFrequency;

            // Fine tune to the frequency
            TuneFrequency(wavemeter, refFreqAccuracy, measurements_ITU.freq_gHz + 1000, modeHopDetect_GHz, timeout_s, freqSlope_p_GHz);
            //TuneToFrequency(measurements_ITU.freq_gHz + 1000, phaseStepSize_mA, phaseStepDelay_mS, maxPhaseCurrentLimit_mA, minPhaseCurrentLimit_mA);

            // Read at higher frequency
            LockerReadings measurements_upper = GetReadings();

            double slopeEfficiency = double.NaN;
            double phaseTuningEfficiency = double.NaN;
            double phaseRatioSlopeEfficiency = double.NaN;

            // Calculate
            if (measurements_lower.freq_gHz - measurements_upper.freq_gHz != 0)
            {
                if (measurements_ITU.txLocker_mA + measurements_ITU.rxLocker_mA != 0)
                {
                    slopeEfficiency = measurements_lower.txLocker_mA - measurements_lower.rxLocker_mA;
                    slopeEfficiency -= measurements_upper.txLocker_mA - measurements_upper.rxLocker_mA;
                    slopeEfficiency /= 0.5 * (measurements_ITU.txLocker_mA + measurements_ITU.rxLocker_mA);
                    slopeEfficiency /= measurements_lower.freq_gHz - measurements_upper.freq_gHz;
                    slopeEfficiency *= 100;
                }

                phaseTuningEfficiency = measurements_lower.phase_mA - measurements_upper.phase_mA;
                phaseTuningEfficiency /= measurements_lower.freq_gHz - measurements_upper.phase_mA;
            }

            if ( measurements_lower.rxLocker_mA != 0 )
            {
                double lockRatio_lower = measurements_lower.txLocker_mA / measurements_lower.rxLocker_mA;
                double lockRatio_upper = measurements_upper.txLocker_mA / measurements_upper.rxLocker_mA;

                if (lockRatio_lower - lockRatio_upper != 0)
                {
                    phaseRatioSlopeEfficiency = measurements_lower.phase_mA - measurements_upper.phase_mA;
                    phaseRatioSlopeEfficiency /= lockRatio_lower - lockRatio_upper;
                }
            }

            // Create results container
            DatumList SingleChannelResults = new DatumList();

            // Store results
            SingleChannelResults.AddDouble("LockerSlopeEff", slopeEfficiency);
            SingleChannelResults.AddDouble("PhaseTuningEff", phaseTuningEfficiency);
            SingleChannelResults.AddDouble("PhaseRatioSlopeEff", phaseRatioSlopeEfficiency);

            return SingleChannelResults;
        }

        /// <summary>
        /// Tune to frequency by adjusting phase current.
        /// </summary>
        /// <param name="wavemeter">The wavemeter with which to measure frequency.</param>
        /// <param name="reqFreqAccuracy">Defines how close we need to get to the target frequency.</param>
        /// <param name="freqTarget_GHz">The target frequency to tune to.</param>
        /// <param name="modeHopDetect_GHz">A change in frequency greater than this will result in a FreqTunerModeHopException</param>
        /// <param name="freqSlope_p_GHz">An initial estimate for the slope of frequency against phase current.</param>
        /// <param name="timeout_s">The maximum time allowed to tune to frequency</param>
        private void TuneFrequency(IInstType_Wavemeter wavemeter, double reqFreqAccuracy, double freqTarget_GHz, double modeHopDetect_GHz, double timeout_s, double freqSlope_p_GHz)
        {
            Util_FrequencyTuner tuner = new Util_FrequencyTuner((InstType_Wavemeter)wavemeter, reqFreqAccuracy, freqTarget_GHz, modeHopDetect_GHz);
            tuner.SetupTuning(freqSlope_p_GHz, modeHopDetect_GHz);
            // get current
            double phaseCurrent = phaseCurrentSource.CurrentSetPoint_amp;
            double actualFreq_GHz;
            DateTime startTime = DateTime.Now;

            while ( ! tuner.MeasFreqAndCheckInRange(out actualFreq_GHz) )
            {        
                tuner.UpdateSlopeAndCheckModeHops(actualFreq_GHz);
                phaseCurrent = tuner.CalcNewTuningPoint(phaseCurrent, actualFreq_GHz);

                phaseCurrentSource.CurrentSetPoint_amp = phaseCurrent;
                if ((DateTime.Compare(DateTime.Now, startTime.AddSeconds(timeout_s)) > 0))
                    break;
            }
        }

        #region Backup method
        /// <summary>
        /// Iterative process to set frequency by adjusting phase currrent.
        /// </summary>
        /// <param name="targetFrequency"></param>
        /// <param name="phaseStepSize"></param>
        /// <param name="phaseStepDelay_mS"></param>
        /// <param name="maxPhaseCurrentLimit_mA"></param>
        /// <param name="minPhaseCurrentLimit_mA"></param>
        private void TuneToFrequency(double targetFrequency, double phaseStepSize, int phaseStepDelay_mS, double maxPhaseCurrentLimit_mA, double minPhaseCurrentLimit_mA)
        {
            int direction = 0;
            int loops = 0;
            double frequencyTolerance = 100;    // Tune to within 100 GHz of target
            double measuredFrequency = wavemeter.Frequency_GHz;
            double deltaFrequency = targetFrequency - measuredFrequency;
            double phaseCurrent = phaseCurrentSource.CurrentActual_amp;

            while (Math.Abs(deltaFrequency) > frequencyTolerance && loops++ < 10)
            {
                if (measuredFrequency < targetFrequency)
                {
                    phaseCurrent += phaseStepSize;
                    direction = 1;
                }
                else
                {
                    phaseCurrent -= phaseStepSize;
                    direction = -1;
                }

                // Check that the phase current is safe
                if (phaseCurrent > maxPhaseCurrentLimit_mA || phaseCurrent > minPhaseCurrentLimit_mA)
                {   // give up.
                    return;
                    // TODO - Should we throw an exception here ?
                }

                // Set Current
                phaseCurrentSource.CurrentSetPoint_amp = phaseCurrent;
                System.Threading.Thread.Sleep(phaseStepDelay_mS);

                // Check frequency
                measuredFrequency = wavemeter.Frequency_GHz;
                deltaFrequency = targetFrequency - measuredFrequency;

                // Halve the step size if changing direction
                if (direction == 1 && deltaFrequency < 0)
                {
                    deltaFrequency /= 2;
                }
                if (direction == -1 && deltaFrequency > 0)
                {
                    deltaFrequency /= 2;
                }
            }

        }
        #endregion

        private LockerReadings GetReadings()
        {
            LockerReadings readings = new LockerReadings();

            readings.rxLocker_mA = rxLockerSource.CurrentActual_amp;
            readings.txLocker_mA = rxLockerSource.CurrentActual_amp;
            readings.phase_mA = phaseCurrentSource.CurrentActual_amp;
            readings.freq_gHz = wavemeter.Frequency_GHz;

            return readings;
        }

        private struct LockerReadings
        {
            public double rxLocker_mA;
            public double txLocker_mA;
            public double phase_mA;
            public double freq_gHz;
        }

        private IInstType_Wavemeter wavemeter;
        private IInstType_ElectricalSource phaseCurrentSource;
        private IInstType_ElectricalSource rxLockerSource;
        private IInstType_ElectricalSource txLockerSource;
    }
}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// SoaShutterMeasurement.cs
//
// Author: Mark Fullalove, 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestSolution.IlmzCommonData;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestSolution.IlmzCommonInstrs;
using Bookham.TestSolution.IlmzCommonUtils;
using Bookham.TestLibrary.BlackBoxes;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Static class to perform SOA shutter measurements
    /// </summary>
    public static class SoaShutterMeasurement
    {
        /// <summary>
        /// Perform SOA shutter measurements at the current channel,using PXI instruments
        /// </summary>
        /// <param name="maxSoaCurrent_mA">Max allowed SOA current</param>
        /// <param name="maxSoaVoltage_V">SOA voltage compliance limit (SOA_SHUTTER_V)</param>
        /// <param name="maxSoaPower_mW">Maximum safe electrical power dissipation in the SOA section</param>
        /// <param name="voltageComplianceOffset_V">SOA voltage compliance limit offset</param>
        /// <param name="instruments">DsdbrInstruments collection</param>
        /// <param name="powerHead">Optical power meter</param>
        /// <returns>Datumlist containing the parametric results.</returns>
        public static DatumList MeasureAtSingleChannel(double maxSoaCurrent_mA, double maxSoaVoltage_V, double maxSoaPower_mW,
            double voltageComplianceOffset_V, DsdbrInstruments instruments, IInstType_OpticalPowerMeter powerHead)
        {
            // Get instruments
            IInstType_ElectricalSource soaCurrentSource = instruments.CurrentSources[DSDBRSection.SOA];
            IInstType_ElectricalSource gainCurrentSource = instruments.CurrentSources[DSDBRSection.Gain];
            IInstType_ElectricalSource rxLockerSource = instruments.LockerRx;
            IInstType_ElectricalSource txLockerSource = instruments.LockerTx;

            // Get initial conditions
            double initialSoaCurrent = soaCurrentSource.CurrentSetPoint_amp;
            double initialGainCurrent = gainCurrentSource.CurrentSetPoint_amp;
            double initialSoaCompliance_V = soaCurrentSource.VoltageComplianceSetPoint_Volt;

            // Setup instruments ready to test
            gainCurrentSource.CurrentSetPoint_amp = 0;
            soaCurrentSource.CurrentSetPoint_amp = 0;
            soaCurrentSource.VoltageComplianceSetPoint_Volt = maxSoaVoltage_V + voltageComplianceOffset_V;
            powerHead.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_mW;

            // Apply small negative current to set SOA reverse current relays
            soaCurrentSource.CurrentSetPoint_amp = -0.001;
            System.Threading.Thread.Sleep(50);
            gainCurrentSource.CurrentSetPoint_amp = initialGainCurrent;
            System.Threading.Thread.Sleep(100);

            // Test
            double measuredSoaVoltage_V = 0;
            double appliedSoaCurrent_mA = 0;
            double electricalPower_mW = 0;
            for (appliedSoaCurrent_mA = -soaCurrentStepSize_mA; Math.Abs(appliedSoaCurrent_mA) < maxSoaCurrent_mA; appliedSoaCurrent_mA -= soaCurrentStepSize_mA)
            {
                soaCurrentSource.CurrentSetPoint_amp = appliedSoaCurrent_mA / 1000;

                System.Threading.Thread.Sleep(200);

                measuredSoaVoltage_V = soaCurrentSource.VoltageActual_Volt;

                // Stop if compliance is reached ( normal exit point )
                if (Math.Round(measuredSoaVoltage_V, 1) >= Math.Round(maxSoaVoltage_V, 1))
                    break;

                // Check electrical power dissipation just in case we manage to hit the safe
                //  limit without reaching either voltage compliance or the max safe current.
                electricalPower_mW = Math.Abs(appliedSoaCurrent_mA * measuredSoaVoltage_V);
                if (electricalPower_mW >= maxSoaPower_mW)
                    break;
            }

            // Create results container
            DatumList SingleChannelResults = new DatumList();

            // Read optical power & monitors
            SingleChannelResults.AddDouble("ShutteredPower_dBm",
                Measurements.ReadOpticalPower(OpticalPowerHead.OpmCgDirect, Measurements.PowerUnits.dBm));
            DsdbrUtils.LockerCurrents lc = DsdbrUtils.ReadLockerCurrents();
            SingleChannelResults.AddDouble("IrxShuttered", lc.RxCurrent_mA);
            SingleChannelResults.AddDouble("ItxShuttered", lc.TxCurrent_mA);

            // Read the actual current rather than relying on the set level
            appliedSoaCurrent_mA = soaCurrentSource.CurrentActual_amp * 1000;
            SingleChannelResults.AddDouble("IsoaShutter", appliedSoaCurrent_mA);
            SingleChannelResults.AddDouble("VsoaShutter", -1 * measuredSoaVoltage_V);
            SingleChannelResults.AddDouble("SoaPwrShuttered", electricalPower_mW);

            // Restore original conditions
            // NOTE: The SOA cannot be in compliance when changing from negative to 0 or positive SOA current
            //       otherwise Closegrid will crash. Therefore, set Igain to 0 to stop photocurrent in the SOA
            //       then a short wait before setting Isoa to 0. 
            gainCurrentSource.CurrentSetPoint_amp = 0;
            gainCurrentSource.OutputEnabled = true;
            System.Threading.Thread.Sleep(1000);
            // Set small negative Isoa to reduce Isoa but not switch relays
            soaCurrentSource.CurrentSetPoint_amp = -0.001;
            soaCurrentSource.VoltageComplianceSetPoint_Volt = initialSoaCompliance_V;
            System.Threading.Thread.Sleep(500);
            // Set Isoa to 0 to switch relays
            soaCurrentSource.CurrentSetPoint_amp = 0;
            System.Threading.Thread.Sleep(1000);
            soaCurrentSource.CurrentSetPoint_amp = initialSoaCurrent;
            gainCurrentSource.CurrentSetPoint_amp = initialGainCurrent;

            return SingleChannelResults;
        }

        /// <summary>
        /// Perform SOA shutter measurements at the current channel,using FCU instruments
        /// </summary>
        /// <param name="maxSoaCurrent_mA"></param>
        /// <param name="maxSoaVoltage_V"></param>
        /// <param name="maxSoaPower_mW"></param>
        /// <param name="voltageComplianceOffset_V"></param>
        /// <param name="instruments"></param>
        /// <param name="powerHead"></param>
        /// <returns></returns>
        public static DatumList MeasureAtSingleChannel(double maxSoaCurrent_mA, double maxSoaVoltage_V, double maxSoaPower_mW,
            double voltageComplianceOffset_V, FCUInstruments instruments, IInstType_OpticalPowerMeter powerHead)
        {
            // Get initial conditions
            double initialSoaCurrent_A = instruments.SoaCurrentSource.CurrentActual_amp;
            double initialGainCurrent_mA = instruments.FullbandControlUnit.GetIVSource(IVSourceIndexNumbers.Gain);
            double initialSoaCompliance_V = instruments.SoaCurrentSource.VoltageComplianceSetPoint_Volt;

            // Setup instruments ready to test
            instruments.FullbandControlUnit.SetIVSource(0, IVSourceIndexNumbers.Gain);
            instruments.SoaCurrentSource.CurrentSetPoint_amp = 0;
            instruments.SoaCurrentSource.VoltageComplianceSetPoint_Volt = maxSoaVoltage_V + voltageComplianceOffset_V; // Positive voltage compliance is always set.
            powerHead.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_mW;

            // Apply small negative current to set SOA reverse current relays
            instruments.SoaCurrentSource.CurrentSetPoint_amp = -0.001;
            System.Threading.Thread.Sleep(50);
            instruments.FullbandControlUnit.SetIVSource((float)initialGainCurrent_mA, IVSourceIndexNumbers.Gain);
            System.Threading.Thread.Sleep(100);

            // Test
            double measuredSoaVoltage_V = 0;
            double appliedSoaCurrent_mA = 0;
            double electricalPower_mW = 0;
            for (appliedSoaCurrent_mA = -soaCurrentStepSize_mA; Math.Abs(appliedSoaCurrent_mA) < maxSoaCurrent_mA; appliedSoaCurrent_mA -= soaCurrentStepSize_mA)
            {
                instruments.SoaCurrentSource.CurrentSetPoint_amp = appliedSoaCurrent_mA / 1000;

                System.Threading.Thread.Sleep(200);

                measuredSoaVoltage_V = instruments.SoaCurrentSource.VoltageActual_Volt;

                // Stop if compliance is reached ( normal exit point )
                if (Math.Abs(Math.Round(measuredSoaVoltage_V, 1)) >= Math.Abs(Math.Round(maxSoaVoltage_V, 1)))
                    break;

                // Check electrical power dissipation just in case we manage to hit the safe
                //  limit without reaching either voltage compliance or the max safe current.
                electricalPower_mW = Math.Abs(appliedSoaCurrent_mA * measuredSoaVoltage_V);
                if (electricalPower_mW >= maxSoaPower_mW)
                    break;
            }

            // Create results container
            DatumList SingleChannelResults = new DatumList();

            // Read optical power & monitors
            double power_dBm = Measurements.ReadOpticalPower(OpticalPowerHead.MZHead, Measurements.PowerUnits.dBm);
            if (double.IsInfinity(power_dBm))
            {
                power_dBm = -50;
            }
            SingleChannelResults.AddDouble("ShutteredPower_dBm", power_dBm);
            DsdbrUtils.LockerCurrents lc = DsdbrUtils.ReadLockerCurrents();
            SingleChannelResults.AddDouble("IrxShuttered", lc.RxCurrent_mA);
            SingleChannelResults.AddDouble("ItxShuttered", lc.TxCurrent_mA);

            // Read the actual current rather than relying on the set level
            appliedSoaCurrent_mA = instruments.SoaCurrentSource.CurrentActual_amp * 1000;
            SingleChannelResults.AddDouble("IsoaShutter", -1 * appliedSoaCurrent_mA);// Keep it positive for compatibility with PXI testkits
            SingleChannelResults.AddDouble("VsoaShutter", measuredSoaVoltage_V);// Keep it negative for compatibility with PXI testkits
            SingleChannelResults.AddDouble("SoaPwrShuttered", electricalPower_mW);

            // Restore original conditions
            // NOTE: The SOA cannot be in compliance when changing from negative to 0 or positive SOA current
            //       otherwise Closegrid will crash. Therefore, set Igain to 0 to stop photocurrent in the SOA
            //       then a short wait before setting Isoa to 0. 
            instruments.FullbandControlUnit.SetIVSource(0, IVSourceIndexNumbers.Gain);
            System.Threading.Thread.Sleep(1000);
            // Set small negative Isoa to reduce Isoa but not switch relays
            instruments.SoaCurrentSource.CurrentSetPoint_amp = -0.001;
            instruments.SoaCurrentSource.VoltageComplianceSetPoint_Volt = initialSoaCompliance_V;
            System.Threading.Thread.Sleep(500);
            // Set Isoa to 0 to switch relays
            instruments.SoaCurrentSource.CurrentSetPoint_amp = 0;
            System.Threading.Thread.Sleep(1000);
            instruments.SoaCurrentSource.CurrentSetPoint_amp = initialSoaCurrent_A;
            instruments.FullbandControlUnit.SetIVSource((float)initialGainCurrent_mA, IVSourceIndexNumbers.Gain);

            return SingleChannelResults;
        }
        
        // Alice.Huang   2010-02-02
        // add FCU2ASIC operation
        
        /// <summary>
        /// Perform SOA shutter measurements at the current channel,using FCU2Asic instruments
        /// </summary>
        /// <param name="maxSoaCurrent_mA"></param>        
        /// <param name="maxSoaPower_mW"></param>        
        /// <param name="instruments"></param>
        /// <param name="powerHead"></param>
        /// <returns></returns>
        public static DatumList MeasureAtSingleChannel(double maxSoaCurrent_mA, double maxSoaPower_mW,
            FCU2AsicInstruments instruments, IInstType_OpticalPowerMeter powerHead)
        {
            // Get initial conditions
            double initialSoaCurrent_mA = instruments.Fcu2Asic.ISoa_mA;
            double initialGainCurrent_mA = instruments.Fcu2Asic .IGain_mA;

            /*Bookham.TestLibrary.Instruments.Inst_Ke24xx keSource = null;
            try
            {
                keSource  = (Bookham.TestLibrary.Instruments.Inst_Ke24xx)instruments.AsicVeeSource;
            }
            catch
            { }
            if (keSource != null) keSource.SenseCurrent(maxSoaCurrent_mA/1000 +0.001, maxSoaCurrent_mA/1000 + 0.001);*/
            //Echo remed aboved block, 13-12-2010

            // Setup instruments ready to test
            instruments.Fcu2Asic .IGain_mA =0;
            instruments .Fcu2Asic .ISoa_mA =0;
            // Commented by Alice.Huang  :   2010-02-02
            // Fcu2Asic can't monitor Voltage
            //instruments.SoaCurrentSource.VoltageComplianceSetPoint_Volt = maxSoaVoltage_V + voltageComplianceOffset_V; // Positive voltage compliance is always set.
            powerHead.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;
            powerHead.Range = InstType_OpticalPowerMeter.AutoRange;
            // Apply small negative current to set SOA reverse current relays
            instruments .Fcu2Asic .ISoa_mA = -1;            
            System.Threading.Thread.Sleep(50);
            instruments .Fcu2Asic .IGain_mA = (float ) initialGainCurrent_mA;
            System.Threading.Thread.Sleep(100);

            // Test            
            double appliedSoaCurrent_mA = 0;
            double electricalPower_mW = 0;
            double measuredSoaVoltage_V = 0;
            double measuredCurrent;

            measuredSoaVoltage_V = 0;
            
            for (appliedSoaCurrent_mA = -soaCurrentStepSize_mA;
                Math .Abs (appliedSoaCurrent_mA ) <= maxSoaCurrent_mA;
                appliedSoaCurrent_mA -= soaCurrentStepSize_mA)
            {
                instruments .Fcu2Asic .ISoa_mA = (float)appliedSoaCurrent_mA;

                System.Threading.Thread.Sleep(200);

                // Alice.Huang 2010-02-22
                // for Asic can't monitor the voltage
               
                if (appliedSoaCurrent_mA >= 0)
                    measuredSoaVoltage_V = instruments.AsicVccSource.VoltageActual_Volt;
                else
                {
                    measuredSoaVoltage_V = instruments.AsicVeeSource.VoltageActual_Volt;
                    if (measuredSoaVoltage_V > 0) measuredSoaVoltage_V = 0 - measuredSoaVoltage_V;
                    
                    measuredCurrent = instruments.AsicVeeSource.CurrentActual_amp;
                    if ( Math.Abs(measuredCurrent) <= (Math.Abs(appliedSoaCurrent_mA / 1000) - 0.0001))
                    {
                        /*if (keSource != null)
                        {
                            // if Vee Source current reading less than -appliedSoaCurrent_mA, it should meet the 
                            // compliant current then the curent actualy applied on soa should be Icompliance,
                            // we should change the current conpliance and rang to a litttle more than -appliedSoaCurrent_mA;

                            keSource.SenseCurrent(maxSoaCurrent_mA/1000 +0.001 , maxSoaCurrent_mA / 1000 + 0.001);
                            System.Threading.Thread.Sleep(100);
                            instruments.Fcu2Asic.ISoa_mA = (float)appliedSoaCurrent_mA;
                            System.Threading.Thread.Sleep(500);
                        }*/ // echo remed aboved block, 13-12-2010
                    }
                }

                //// Stop if compliance is reached ( normal exit point )
                //if (Math.Abs(Math.Round(measuredSoaVoltage_V, 1)) >= Math.Abs(Math.Round(maxSoaVoltage_V, 1)))
                //    break;

                // Check electrical power dissipation just in case we manage to hit the safe
                //  limit without reaching either voltage compliance or the max safe current.
                electricalPower_mW = Math.Abs(appliedSoaCurrent_mA * measuredSoaVoltage_V);
                if (electricalPower_mW >= maxSoaPower_mW)
                    break;
            }

            // Create results container
            DatumList SingleChannelResults = new DatumList();
            measuredCurrent = instruments.AsicVeeSource.CurrentActual_amp;
            // Read optical power & monitors
            double power_dBm = Measurements.ReadOpticalPower(OpticalPowerHead.MZHead, Measurements.PowerUnits.dBm);
            
            if (double.IsInfinity(power_dBm))
            {
                power_dBm = -50;
            }
            SingleChannelResults.AddDouble("ShutteredPower_dBm", power_dBm);
            DsdbrUtils.LockerCurrents lc = DsdbrUtils.ReadLockerCurrents();
            SingleChannelResults.AddDouble("IrxShuttered", lc.RxCurrent_mA);
            SingleChannelResults.AddDouble("ItxShuttered", lc.TxCurrent_mA);

            // Read the actual current rather than relying on the set level
            //appliedSoaCurrent_mA = instruments.Fcu2Asic .ISoa_mA;
            SingleChannelResults.AddDouble("IsoaShutter", -1 * measuredCurrent * 1000);// Keep it positive for compatibility with PXI testkits
            //SingleChannelResults.AddDouble("VsoaShutter", measuredSoaVoltage_V);// Keep it negative for compatibility with PXI testkits
            SingleChannelResults.AddDouble("SoaPwrShuttered", electricalPower_mW);

            // Restore original conditions
            // NOTE: The SOA cannot be in compliance when changing from negative to 0 or positive SOA current
            //       otherwise Closegrid will crash. Therefore, set Igain to 0 to stop photocurrent in the SOA
            //       then a short wait before setting Isoa to 0.
            instruments .Fcu2Asic .IGain_mA =0;            
            System.Threading.Thread.Sleep(1000);
            // Set small negative Isoa to reduce Isoa but not switch relays
            instruments .Fcu2Asic .ISoa_mA = -1;
            //instruments.SoaCurrentSource.VoltageComplianceSetPoint_Volt = initialSoaCompliance_V;
            System.Threading.Thread.Sleep(500);
            // Set Isoa to 0 to switch relays
            instruments.Fcu2Asic .ISoa_mA = 0;
            System.Threading.Thread.Sleep(1000);
            instruments.Fcu2Asic .ISoa_mA = (float) initialSoaCurrent_mA;
            instruments.Fcu2Asic .IGain_mA = (float)initialGainCurrent_mA;

            return SingleChannelResults;
        }
        // Jack.Zhang   2010-05-02
        // add FCU2ASIC operation

        /// <summary>
        /// Perform SOA shutter measurements at the current channel,using FCU2Asic instruments
        /// </summary>
        /// <param name="maxSoaCurrent_mA"></param>        
        /// <param name="maxSoaPower_mW"></param>        
        /// <param name="instruments"></param>
        /// <param name="powerHead"></param>
        /// <returns></returns>
        public static DatumList MeasureAtSingleChannel_New(double maxSoaCurrent_mA, double maxSoaPower_mW,
            FCU2AsicInstruments instruments, IInstType_OpticalPowerMeter powerHead)
        {
            // Get initial conditions
            double initialSoaCurrent_mA = instruments.Fcu2Asic.ISoa_mA;
            double initialGainCurrent_mA = instruments.Fcu2Asic.IGain_mA;

            // Setup instruments ready to test
            instruments.Fcu2Asic.IGain_mA = 0;
            instruments.Fcu2Asic.ISoa_mA = 0;
            // Commented by Alice.Huang  :   2010-02-02
            // Fcu2Asic can't monitor Voltage
            //instruments.SoaCurrentSource.VoltageComplianceSetPoint_Volt = maxSoaVoltage_V + voltageComplianceOffset_V; // Positive voltage compliance is always set.

           /* Bookham.TestLibrary.Instruments.Inst_Ke24xx keSource = null;
            try
            {
                keSource = (Bookham.TestLibrary.Instruments.Inst_Ke24xx)instruments.AsicVeeSource;
            }
            catch
            { }
            if (keSource != null) keSource.SenseCurrent(maxSoaCurrent_mA / 1000 + 0.001, maxSoaCurrent_mA / 1000 + 0.001);*/ 
            //Echo remed above block, 13-12-2010

            Measurements.MzHead.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;
            Measurements.MzHead.Range = InstType_OpticalPowerMeter.AutoRange;
            // Apply small negative current to set SOA reverse current relays
            instruments.Fcu2Asic.ISoa_mA = -1;
            //System.Threading.Thread.Sleep(50);
            instruments.Fcu2Asic.IGain_mA = (float)initialGainCurrent_mA;
            System.Threading.Thread.Sleep(100);

            // Test            
            double appliedSoaCurrent_mA = 0;
            double electricalPower_mW = 0;
            double measuredSoaVoltage_V = 0;
            measuredSoaVoltage_V = 0;

               instruments.Fcu2Asic.ISoa_mA = (float)(-maxSoaCurrent_mA);

                System.Threading.Thread.Sleep(500);

                
            // Create results container
            DatumList SingleChannelResults = new DatumList();
            double measuredCurrent = instruments.AsicVeeSource.CurrentActual_amp;
            // Read optical power & monitors
            double power_dBm = Measurements.ReadOpticalPower(OpticalPowerHead.MZHead, Measurements.PowerUnits.dBm);
            if (double.IsInfinity(power_dBm))
            {
                power_dBm = -50;
            }
            SingleChannelResults.AddDouble("ShutteredPower_dBm", power_dBm);
            //DsdbrUtils.LockerCurrents lc = DsdbrUtils.ReadLockerCurrents();
            SingleChannelResults.AddDouble("IrxShuttered", 0);
            SingleChannelResults.AddDouble("ItxShuttered", 0);

            // Read the actual current rather than relying on the set level
            //appliedSoaCurrent_mA = instruments.Fcu2Asic.ISoa_mA;
            SingleChannelResults.AddDouble("IsoaShutter", -1*measuredCurrent *1000);// Keep it positive for compatibility with PXI testkits
            //SingleChannelResults.AddDouble("VsoaShutter", measuredSoaVoltage_V);// Keep it negative for compatibility with PXI testkits
            SingleChannelResults.AddDouble("SoaPwrShuttered", electricalPower_mW);

            // Restore original conditions
            // NOTE: The SOA cannot be in compliance when changing from negative to 0 or positive SOA current
            //       otherwise Closegrid will crash. Therefore, set Igain to 0 to stop photocurrent in the SOA
            //       then a short wait before setting Isoa to 0.
            //instruments.Fcu2Asic.IGain_mA = 0;
            //System.Threading.Thread.Sleep(1000);
            //// Set small negative Isoa to reduce Isoa but not switch relays
            //instruments.Fcu2Asic.ISoa_mA = -1;
            ////instruments.SoaCurrentSource.VoltageComplianceSetPoint_Volt = initialSoaCompliance_V;
            //System.Threading.Thread.Sleep(500);
            // Set Isoa to 0 to switch relays
            instruments.Fcu2Asic.ISoa_mA = 0;
            System.Threading.Thread.Sleep(100);
            instruments.Fcu2Asic.ISoa_mA = (float)initialSoaCurrent_mA;
            instruments.Fcu2Asic.IGain_mA = (float)initialGainCurrent_mA;

            return SingleChannelResults;
        }
        /// <summary>
        /// Perform SOA shutter measurements at the current channel.
        /// </summary>
        /// <param name="maxSoaCurrent_mA"></param>
        /// <param name="maxSoaVoltage_V"></param>
        /// <param name="maxSoaPower_mW"></param>
        /// <param name="voltageComplianceOffset_V"></param>
        /// <param name="powerHead"></param>
        /// <returns></returns>
        public static DatumList MeasureAtSingleChannel(double maxSoaCurrent_mA, double maxSoaVoltage_V, double maxSoaPower_mW,
            double voltageComplianceOffset_V, IInstType_OpticalPowerMeter powerHead)
        {
            if (SoaShutterMeasurement.instrsToUse == DsdbrDriveInstruments.FCUInstruments)
            {
                return SoaShutterMeasurement.MeasureAtSingleChannel(
                        maxSoaCurrent_mA,
                        maxSoaVoltage_V,
                        maxSoaPower_mW,
                        voltageComplianceOffset_V,
                        SoaShutterMeasurement.fcuInstrs,
                        powerHead);
            }

            // Alice.Huang   2010-02-02
            // add FCU2ASIC operation
            else if (SoaShutterMeasurement.instrsToUse == DsdbrDriveInstruments.FCU2AsicInstrument)
            {

                //return SoaShutterMeasurement.MeasureAtSingleChannel_New(maxSoaCurrent_mA, 
                //    maxSoaPower_mW,SoaShutterMeasurement.fcu2AsicInstrs, powerHead);
                return SoaShutterMeasurement.MeasureAtSingleChannel(maxSoaCurrent_mA,
                    maxSoaPower_mW, SoaShutterMeasurement.fcu2AsicInstrs, powerHead);
            }
            else
            {
                return SoaShutterMeasurement.MeasureAtSingleChannel(
                        maxSoaCurrent_mA,
                        maxSoaVoltage_V,
                        maxSoaPower_mW,
                        voltageComplianceOffset_V,
                        SoaShutterMeasurement.pxiInstrs,
                        powerHead);
            }
        }


        /// <summary>
        /// Fullband control unit instruments group
        /// </summary>
        public static FCUInstruments FCUInstrs
        {
            set { fcuInstrs = value; instrsToUse = DsdbrDriveInstruments.FCUInstruments; }
            get { return fcuInstrs; }
        }

        /// <summary>
        /// PXI instruments group
        /// </summary>
        public static DsdbrInstruments PXIInstrs
        {
            set { pxiInstrs = value; instrsToUse = DsdbrDriveInstruments.PXIInstruments; }
            get { return pxiInstrs; }
        }

        /// <summary>
        /// Set/Get FCU2Asic Instrument Group. see <paramref name="fcu2AsicInstrs"/>
        /// </summary>
        public static FCU2AsicInstruments Fcu2AsicInstrs
        {
            get { 
                return SoaShutterMeasurement.fcu2AsicInstrs;
            }
            set 
            { 
                SoaShutterMeasurement.fcu2AsicInstrs = value; 
                instrsToUse = DsdbrDriveInstruments.FCU2AsicInstrument;
            }
        }

        #region Private Data
        private const double soaCurrentStepSize_mA = 1;
        private static FCUInstruments fcuInstrs;
        private static DsdbrInstruments pxiInstrs;
        // alice.Huang  add FCU2ASIC OPTION
        /// <summary>
        /// FCU TO Asic intrtrument groups, 
        /// Fcu control Asic, 
        /// Asic source dsdbr sections 
        /// Asic has an power supply
        /// </summary>
        private static FCU2AsicInstruments fcu2AsicInstrs;

        

        private static DsdbrDriveInstruments instrsToUse;
        #endregion

        //public static DatumList MeasureAtSingleChannel(double p, double p_2, double p_3, double p_4, IInstType_OpticalPowerMeter PowerHead)
        //{
        //    throw new Exception("The method or operation is not implemented.");
        //}
    }


}

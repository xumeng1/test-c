// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// SoaPowerLevel.cs
//
// Author: Tony Foster, 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Threading;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.Toolkit.CloseGrid;//jack.Zhang
using Bookham.TestSolution.IlmzCommonData;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestSolution.IlmzCommonInstrs;
using Bookham.TestLibrary.Algorithms;
using Bookham.TestLibrary.Instruments;


namespace Bookham.TestSolution.IlmzCommonUtils
{
    /// <summary>
    /// Parameters structure for powerleveling.
    /// </summary>
    /// <remarks>
    /// 2007-11-20: Ken.Wu: Add structure.
    /// </remarks>
    public struct PowerLevelParameters
    {
        /// <summary>
        /// Target power in micro-watt.
        /// </summary>
        /// <seealso cref="TargetPower_dBm"/>
        public double TargetPower_mW;
        /// <summary>
        /// Power tolerance in micro-watt.
        /// </summary>
        /// <seealso cref="PowerTolerance_dB"/>
        public double PowerTolerance_mW;
        /// <summary>
        /// SOAPower Slope
        /// </summary>
        public double SOAPowerSlope;
        
        /// <summary>
        /// Target power in dBm.
        /// </summary>
        /// <seealso cref="TargetPower_mW"/>
        public double TargetPower_dBm
        {
            get
            {
                return Alg_PowConvert_dB.Convert_mWtodBm(this.TargetPower_mW);
            }
            set
            {
                this.TargetPower_mW = Alg_PowConvert_dB.Convert_dBmtomW(value);
            }
        }

        /// <summary>
        /// Power tolerance in dBm.
        /// </summary>
        /// <seealso cref="PowerTolerance_mW"/>
        public double PowerTolerance_dB
        {
            get
            {
                return Alg_PowConvert_dB.Convert_mWtodBm(this.TargetPower_mW + this.PowerTolerance_mW)
                    - Alg_PowConvert_dB.Convert_mWtodBm(this.TargetPower_mW);
            }
            set
            {
                this.PowerTolerance_mW = Alg_PowConvert_dB.Convert_dBmtomW(this.TargetPower_dBm + value)
                                            - Alg_PowConvert_dB.Convert_dBmtomW(this.TargetPower_dBm);
            }
        }
    }

    /// <summary>
    /// Static class to set fibre power with the SOA
    /// </summary>
    public static class SoaPowerLevel
    {

        #region Power levelling

        /// <summary>
        /// Power level using SOA with Adaptive SOA current(mA) vs Power(mW) slope
        /// </summary>
        /// <param name="TargetPower_mW">Target power dBm</param>
        /// <param name="PowerTolerance_mW">Power level tolerance mW</param>
        /// <param name="IsoaPowerSlope">Initial soa current(mA) vs power(mW) slope( Soa curve may be a lot more linear using mA vs mW )</param>
        /// <returns>Power levelling results</returns>
        public static DatumList PowerLevelWithAdaptiveSlope(double TargetPower_mW,
                                 double PowerTolerance_mW,  double IsoaPowerSlope)
        {
            // Local constants
            const int maxIterations = 10;
            const int loopDelay_ms = 500;
            const double maxISoaDelta_mA = 20;
            const double maxIsoa_mA = 150; ;

            // Assume tune failed until target reached
            bool targetReachedOk = false;

            // Get initial conditions
            //double frequency_GHz = Measurements.ReadFrequency_GHz();
            double frequency_GHz = Measurements.FrequencyWithoutMeter;
            double initialPower_mW = Measurements.ReadOpticalPower(OpticalPowerHead.MZHead, Measurements.PowerUnits.mW);
            double power_mW = initialPower_mW;
            double initialSoaCurrent;

            initialSoaCurrent = DsdbrUtils.ReadSectionCurrent_mA(DSDBRSection.SOA);

            double iSoa = initialSoaCurrent;
            int loopCount = 0;
            double isoaDelta = 0;
            double adaptiveIsoaPowerSlope = IsoaPowerSlope;
            double lastPower_mW;
            double lastIsoa_mA;

            // Adjust power
            while (Math.Abs(TargetPower_mW - power_mW) > PowerTolerance_mW && loopCount < maxIterations)
            {
                //Record lastPower_mW and lastIsoa_mA
                lastPower_mW = power_mW;
                lastIsoa_mA = iSoa;

                // Calculate new SOA current & limit delta and absolute value
                isoaDelta = (TargetPower_mW - power_mW) * adaptiveIsoaPowerSlope;
                isoaDelta = Math.Sign(isoaDelta) * Math.Min(Math.Abs(isoaDelta), maxISoaDelta_mA);
                iSoa += isoaDelta;
                iSoa = Math.Min(iSoa, maxIsoa_mA);
                iSoa = Math.Max(iSoa, 0);

                // Apply new Isoa
                DsdbrUtils.SetSectionCurrent_mA(DSDBRSection.SOA, iSoa);

                // Settling time
                Thread.Sleep(loopDelay_ms);

                // Read new values
                power_mW = Measurements.ReadOpticalPower(OpticalPowerHead.MZHead, Measurements.PowerUnits.mW);
                iSoa = DsdbrUtils.ReadSectionCurrent_mA(DSDBRSection.SOA);

                //Calculate new adaptiveIsoaPowerSlope
                if (power_mW - lastPower_mW != 0)
                {
                    adaptiveIsoaPowerSlope = (iSoa - lastIsoa_mA) / (power_mW - lastPower_mW);
                }

                loopCount++;

                if (iSoa == maxIsoa_mA)
                    break;
            }

            double lastTunedIsoa = DsdbrUtils.ReadSectionCurrent_mA(DSDBRSection.SOA);
            double lastTunedPower_dBm = Measurements.ReadOpticalPower(OpticalPowerHead.MZHead, Measurements.PowerUnits.dBm);
            double powerAtMaxIsoa_dBm = 0;

            // If tuned OK then set return flag else reset Isoa back to original
            if (loopCount < maxIterations && iSoa < maxIsoa_mA)
            {
                targetReachedOk = true;
            }
            else
            {
                DsdbrUtils.SetSectionCurrent_mA(DSDBRSection.SOA, maxIsoa_mA);
                // Settling time
                Thread.Sleep(loopDelay_ms);
                powerAtMaxIsoa_dBm = Measurements.ReadOpticalPower(OpticalPowerHead.MZHead, Measurements.PowerUnits.dBm);

                //DsdbrUtils.SetSectionCurrent_mA(DSDBRSection.SOA, initialSoaCurrent);
            }

            //Return power levelling results
            DatumList results = new DatumList();
            results.AddBool("PowerLevelOk", targetReachedOk);
            results.AddDouble("lastTunedIsoa", lastTunedIsoa);
            results.AddDouble("lastTunedPower_dBm", lastTunedPower_dBm);
            results.AddDouble("maxIsoa_mA", maxIsoa_mA);
            results.AddDouble("powerAtMaxIsoa_dBm", powerAtMaxIsoa_dBm);

            if (DsdbrUtils.DsdbrDriverInstrsToUse == DsdbrDriveInstruments.FCU2AsicInstrument)
            {
                results.AddDouble("lastTunedIsoaDac", (double)DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic.mAToDacForFrontSoa_Asic((float)lastTunedIsoa));
                results.AddDouble("maxIsoa_Dac", (double)DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic.mAToDacForFrontSoa_Asic((float)maxIsoa_mA));
            }
            return results;
        }
        /// <summary>
        /// Power level using SOA with Adaptive SOA current(mA) vs Power(mW) slope
        /// </summary>
        /// <param name="TargetPower_mW">Target power dBm</param>
        /// <param name="PowerTolerance_mW">Power level tolerance mW</param>
        /// <param name="IsoaPowerSlope">Initial soa current(mA) vs power(mW) slope( Soa curve may be a lot more linear using mA vs mW )</param>
        /// <param name="maxIsoa_mA"> max Soa current available for SOA tuning  </param>
        /// <returns>Power levelling results</returns>
        public static DatumList PowerLevelWithAdaptiveSlope(double TargetPower_mW,
                        double PowerTolerance_dBm,double IsoaPowerSlope, double maxIsoa_mA)
        {
            // Local constants
            const int maxIterations = 10;
            const int loopDelay_ms = 10;
            const double maxISoaDelta_mA = 20;
            // Assume tune failed until target reached
            bool targetReachedOk = false;

            // Get initial conditions
            //double frequency_GHz = Measurements.ReadFrequency_GHz();
            double power_mW = Measurements.ReadOpticalPower(OpticalPowerHead.MZHead, Measurements.PowerUnits.mW);
            
            double initialSoaCurrent;

            initialSoaCurrent = DsdbrUtils.ReadSectionCurrent_mA(DSDBRSection.SOA);

            double iSoa = initialSoaCurrent;
            int loopCount = 0;
            double isoaDelta = 0;
            double adaptiveIsoaPowerSlope = IsoaPowerSlope;
            double lastPower_mW;
            double lastIsoa_mA;
            double Power_AbsDiff_dB=Math.Abs(Alg_PowConvert_dB.Convert_mWtodBm(TargetPower_mW) - Alg_PowConvert_dB.Convert_mWtodBm(power_mW));
            // Alice.Huang     2010-07-20
            // Add list to trace the SOA, Power, SOA Delta and soa slope
            List<double> iSoaList = new List<double>();
            List<double> isoaDeltaList = new List<double>();
            List<double> soaSlopeList = new List<double>();
            List<double> powerList = new List<double>();

            // Adjust power
            while ( Power_AbsDiff_dB> PowerTolerance_dBm && loopCount < maxIterations)
            {
                //Record lastPower_mW and lastIsoa_mA
                lastPower_mW = power_mW;
                lastIsoa_mA = iSoa;

                // Calculate new SOA current & limit delta and absolute value
                isoaDelta = (TargetPower_mW - power_mW) * adaptiveIsoaPowerSlope;
                isoaDelta = Math.Sign(isoaDelta) * Math.Min(Math.Abs(isoaDelta), maxISoaDelta_mA);
                iSoa += isoaDelta;
                iSoa = Math.Min(iSoa, maxIsoa_mA);
                iSoa = Math.Max(iSoa, 0);

                // Apply new Isoa
                DsdbrUtils.SetSectionCurrent_mA(DSDBRSection.SOA, iSoa);

                // Settling time
                Thread.Sleep(loopDelay_ms);

                // Read new values
                power_mW = Measurements.ReadOpticalPower(OpticalPowerHead.MZHead, Measurements.PowerUnits.mW);
                iSoa = DsdbrUtils.ReadSectionCurrent_mA(DSDBRSection.SOA);
                Power_AbsDiff_dB = Math.Abs(Alg_PowConvert_dB.Convert_mWtodBm(TargetPower_mW) - Alg_PowConvert_dB.Convert_mWtodBm(power_mW));

                //Calculate new adaptiveIsoaPowerSlope
                if (power_mW - lastPower_mW != 0)
                {
                    adaptiveIsoaPowerSlope = (iSoa - lastIsoa_mA) / (power_mW - lastPower_mW);
                }

                // Alice.Huang     2010-07-20
                // Add this to trace power leveling error
                iSoaList.Add(iSoa);
                isoaDeltaList.Add(isoaDelta);
                soaSlopeList.Add(adaptiveIsoaPowerSlope);
                powerList.Add(power_mW);

                loopCount++;

                if (iSoa == maxIsoa_mA)
                    break;
            }

            double lastTunedIsoa = DsdbrUtils.ReadSectionCurrent_mA(DSDBRSection.SOA);            
            double lastTunedPower_dBm = Measurements.ReadOpticalPower(OpticalPowerHead.MZHead, Measurements.PowerUnits.dBm);
            double powerAtMaxIsoa_dBm = 0;

            // If tuned OK then set return flag else reset Isoa back to original
            if (loopCount < maxIterations && iSoa < maxIsoa_mA)
            {
                targetReachedOk = true;
            }
            else
            {
                DsdbrUtils.SetSectionCurrent_mA(DSDBRSection.SOA, maxIsoa_mA);
                // Settling time
                Thread.Sleep(loopDelay_ms);
                powerAtMaxIsoa_dBm = Measurements.ReadOpticalPower(OpticalPowerHead.MZHead, Measurements.PowerUnits.dBm);

                // Jack.zhang     if target power can't achieve, set the ISOA as maximum
                //DsdbrUtils.SetSectionCurrent_mA(DSDBRSection.SOA, initialSoaCurrent);
            }

            //Return power levelling results
            DatumList results = new DatumList();
            results.AddBool("PowerLevelOk", targetReachedOk);
            results.AddDouble("lastTunedIsoa", lastTunedIsoa);
            results.AddDouble("lastTunedPower_dBm", lastTunedPower_dBm);
            results.AddDouble("maxIsoa_mA", maxIsoa_mA);
            results.AddDouble("powerAtMaxIsoa_dBm", powerAtMaxIsoa_dBm);

            if (DsdbrUtils.DsdbrDriverInstrsToUse == DsdbrDriveInstruments.FCU2AsicInstrument)
            {
                results.AddDouble("lastTunedIsoaDac", (double)DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic.mAToDacForFrontSoa_Asic((float)lastTunedIsoa));
                results.AddDouble("maxIsoa_Dac", (double)DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic.mAToDacForFrontSoa_Asic((float)maxIsoa_mA));
            }

            return results;
        }

        /// <summary>
        /// Power level using SOA with Adaptive SOA current(mA) vs Power(mW) slope.
        /// </summary>
        /// <param name="parameters">all powerleveling parameters.</param>
        /// <returns></returns>
        public static DatumList PowerLevelWithAdaptiveSlope(PowerLevelParameters parameters)
        {
            return PowerLevelWithAdaptiveSlope(parameters.TargetPower_mW, parameters.PowerTolerance_mW, parameters.SOAPowerSlope);
        }

        /// <summary>
        /// Power level using SOA
        /// </summary>
        /// <param name="TargetPower_dBm">Target power dBm</param>
        /// <param name="PowerTolerance_dB">Power level tolerance dB</param>
        /// <param name="IsoaPowerSlope">SOA currrent vs Power in dBm slope</param>
        /// <returns>Tuning status, true if target power reached</returns>
        public static bool PowerLevel(double TargetPower_dBm, double PowerTolerance_dB, double IsoaPowerSlope)
        {
            // Local constants
            const int maxIterations = 10;               
            const int loopDelay_ms = 500;
            const double maxISoaDelta_mA = 20;
            const double maxIsoa_mA = 200;
      
            // Assume tune failed until target reached
            bool targetReachedOk = false;

            // Get initial conditions
            //double frequency_GHz = Measurements.ReadFrequency_GHz();
            double frequency_GHz = Measurements.FrequencyWithoutMeter;
            double initialPower_dBm = Measurements.ReadOpticalPower(OpticalPowerHead.MZHead, Measurements.PowerUnits.dBm);
            double power_dBm = initialPower_dBm;
            double initialSoaCurrent = DsdbrUtils.ReadSectionCurrent_mA(DSDBRSection.SOA);
            double iSoa = initialSoaCurrent;
            int loopCount = 0;
            double isoaDelta = 0;

            // Adjust power
            while (Math.Abs(TargetPower_dBm - power_dBm) > PowerTolerance_dB && loopCount < maxIterations)
            {
                // Calculate new SOA current & limit delta and absolute value
                isoaDelta = (TargetPower_dBm - power_dBm) * IsoaPowerSlope;
                isoaDelta = Math.Sign(isoaDelta) * Math.Min(Math.Abs(isoaDelta), maxISoaDelta_mA);
                iSoa += isoaDelta;
                iSoa = Math.Min(iSoa, maxIsoa_mA);
                iSoa = Math.Max(iSoa, 0);

                // Apply new Isoa
                DsdbrUtils.SetSectionCurrent_mA(DSDBRSection.SOA, iSoa);

                // Settling time
                Thread.Sleep(loopDelay_ms);

                // Read new values
                power_dBm = Measurements.ReadOpticalPower(OpticalPowerHead.MZHead, Measurements.PowerUnits.dBm);
                iSoa = DsdbrUtils.ReadSectionCurrent_mA(DSDBRSection.SOA);  
       
                loopCount++;
            }

            // If tuned OK then set return flag else reset Isoa back to original
            if (loopCount < maxIterations)
            {
                targetReachedOk = true;
            }

            // Return status
            return targetReachedOk;
        }

        #endregion

        #region Tap levelling
        /// <summary>
        /// Tap Levelling results
        /// </summary>
        public struct TapLevelRes
        {
            /// <summary>
            /// Was it successful?
            /// </summary>
            public bool LevelOk;
            /// <summary>
            /// SOA Current
            /// </summary>
            public double SoaCurrent_mA;
            /// <summary>
            /// Fibre power
            /// </summary>
            public double FibrePower_dBm;

            /// <summary>
            /// SOA Dac
            /// </summary>
            public int SoaCurrent_Dac;
        }

        /// <summary>
        /// Adjust Power level using SOA with adaptive slope until a predefined Tap photocurrent is achieved
        /// </summary>
        /// <param name="tap">Tap object</param>
        /// <param name="targetPhotoCurrent_A">Target tap photocurrent (A)</param>
        /// <param name="photoCurrentToleranceRatio">Tolerance Ratio of photocurrent (e.g. within a 1/10th of target would be 0.1)</param>
        /// <returns>Tuning results structure</returns>
        public static TapLevelRes TapLevelWithAdaptiveSlope(Inst_Fcu2Asic Fcu2Asic, double targetPhotoCurrent_A,
            double photoCurrentToleranceRatio)
        {
            // Local constants
            const int maxIterations = 10;
            const int loopDelay_ms = 500;
            const double maxISoaDelta_mA = 20;
            const double maxIsoa_mA = 200;

            // Get initial conditions
            double photoCurrentTolerance_A = Math.Abs(targetPhotoCurrent_A / photoCurrentToleranceRatio);
            double initPhotoCurrent_A = Fcu2Asic.ICtap_mA/1000.0;
            double photoCurrent_A = initPhotoCurrent_A;
            double initialSoaCurrent_mA = DsdbrUtils.ReadSectionCurrent_mA(DSDBRSection.SOA);
            double iSoa_mA = initialSoaCurrent_mA;
            double iSoaCurrentSlope = initialSoaCurrent_mA / initPhotoCurrent_A;
            int loopCount = 0;
            double isoaDelta = 0;
            double adaptiveIsoaCurrentSlope = iSoaCurrentSlope;
            double lastPhotocurrent_A;
            double lastIsoa_mA;

            // Adjust power
            while (Math.Abs(targetPhotoCurrent_A - photoCurrent_A) > photoCurrentTolerance_A && loopCount < maxIterations)
            {
                //record last photo current and last soa current
                lastPhotocurrent_A = photoCurrent_A;
                lastIsoa_mA = iSoa_mA;

                // Calculate new SOA current & limit delta and absolute value
                isoaDelta = (targetPhotoCurrent_A - photoCurrent_A) * adaptiveIsoaCurrentSlope;
                isoaDelta = Math.Sign(isoaDelta) * Math.Min(Math.Abs(isoaDelta), maxISoaDelta_mA);
                iSoa_mA += isoaDelta;
                iSoa_mA = Math.Min(iSoa_mA, maxIsoa_mA);
                iSoa_mA = Math.Max(iSoa_mA, 0);

                // Apply new Isoa
                DsdbrUtils.SetSectionCurrent_mA(DSDBRSection.SOA, iSoa_mA);

                // Settling time
                Thread.Sleep(loopDelay_ms);

                // Read new values
                photoCurrent_A = Fcu2Asic.ICtap_mA / 1000.0;
                iSoa_mA = DsdbrUtils.ReadSectionCurrent_mA(DSDBRSection.SOA);

                //Calculate new slope
                if (lastPhotocurrent_A - photoCurrent_A != 0)
                {
                    adaptiveIsoaCurrentSlope = (lastIsoa_mA - iSoa_mA) / (lastPhotocurrent_A - photoCurrent_A);
                }

                loopCount++;

                if (iSoa_mA == maxIsoa_mA || iSoa_mA == 0)
                    break;
            }

            TapLevelRes results = new TapLevelRes();
            // If tuned OK then set return flag else reset Isoa back to original
            if (loopCount < maxIterations && iSoa_mA < maxIsoa_mA && iSoa_mA > 0)
            {
                results.LevelOk = true;
                results.FibrePower_dBm = Measurements.ReadOpticalPower(OpticalPowerHead.MZHead, Measurements.PowerUnits.dBm);
                results.SoaCurrent_mA = iSoa_mA;
                if (DsdbrUtils.DsdbrDriverInstrsToUse == DsdbrDriveInstruments.FCU2AsicInstrument)
                    results.SoaCurrent_Dac = DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic.mAToDacForFrontSoa_Asic((float)iSoa_mA);
            }
            else
            {
                results.LevelOk = false;
                results.SoaCurrent_mA = initialSoaCurrent_mA;
                DsdbrUtils.SetSectionCurrent_mA(DSDBRSection.SOA, initialSoaCurrent_mA);
            }

            // Return status
            return results;
        }


        /// <summary>
        /// Adjust Power level using SOA with adaptive slope until a predefined Tap photocurrent is achieved
        /// </summary>
        /// <param name="tap">Tap object</param>
        /// <param name="targetPhotoCurrent_A">Target tap photocurrent (A)</param>
        /// <param name="photoCurrentToleranceRatio">Tolerance Ratio of photocurrent (e.g. within a 1/10th of target would be 0.1)</param>
        /// <returns>Tuning results structure</returns>
        public static TapLevelRes TapLevelWithAdaptiveSlope(IInstType_ElectricalSource tap, double targetPhotoCurrent_A,
            double photoCurrentToleranceRatio)
        {
            // Local constants
            const int maxIterations = 10;
            const int loopDelay_ms = 500;
            const double maxISoaDelta_mA = 20;
            const double maxIsoa_mA = 200;

            // Get initial conditions
            double photoCurrentTolerance_A = Math.Abs(targetPhotoCurrent_A / photoCurrentToleranceRatio);
            double initPhotoCurrent_A = tap.CurrentActual_amp;
            double photoCurrent_A = initPhotoCurrent_A;
            double initialSoaCurrent_mA = DsdbrUtils.ReadSectionCurrent_mA(DSDBRSection.SOA);
            double iSoa_mA = initialSoaCurrent_mA;
            double iSoaCurrentSlope = initialSoaCurrent_mA / initPhotoCurrent_A;
            int loopCount = 0;
            double isoaDelta = 0;
            double adaptiveIsoaCurrentSlope = iSoaCurrentSlope;
            double lastPhotocurrent_A;
            double lastIsoa_mA;

            // Adjust power
            while (Math.Abs(targetPhotoCurrent_A - photoCurrent_A) > photoCurrentTolerance_A && loopCount < maxIterations)
            {
                //record last photo current and last soa current
                lastPhotocurrent_A = photoCurrent_A;
                lastIsoa_mA = iSoa_mA;

                // Calculate new SOA current & limit delta and absolute value
                isoaDelta = (targetPhotoCurrent_A - photoCurrent_A) * adaptiveIsoaCurrentSlope;
                isoaDelta = Math.Sign(isoaDelta) * Math.Min(Math.Abs(isoaDelta), maxISoaDelta_mA);
                iSoa_mA += isoaDelta;
                iSoa_mA = Math.Min(iSoa_mA, maxIsoa_mA);
                iSoa_mA = Math.Max(iSoa_mA, 0);

                // Apply new Isoa
                DsdbrUtils.SetSectionCurrent_mA(DSDBRSection.SOA, iSoa_mA);

                // Settling time
                Thread.Sleep(loopDelay_ms);

                // Read new values
                photoCurrent_A = tap.CurrentActual_amp;
                iSoa_mA = DsdbrUtils.ReadSectionCurrent_mA(DSDBRSection.SOA);

                //Calculate new slope
                if (lastPhotocurrent_A - photoCurrent_A != 0)
                {
                    adaptiveIsoaCurrentSlope = (lastIsoa_mA - iSoa_mA) / (lastPhotocurrent_A - photoCurrent_A);
                }

                loopCount++;

                if (iSoa_mA == maxIsoa_mA || iSoa_mA == 0)
                    break;
            }

            TapLevelRes results = new TapLevelRes();
            // If tuned OK then set return flag else reset Isoa back to original
            if (loopCount < maxIterations && iSoa_mA < maxIsoa_mA && iSoa_mA > 0)
            {
                results.LevelOk = true;
                results.FibrePower_dBm = Measurements.ReadOpticalPower(OpticalPowerHead.MZHead, Measurements.PowerUnits.dBm);
                results.SoaCurrent_mA = iSoa_mA;
                if (DsdbrUtils.DsdbrDriverInstrsToUse == DsdbrDriveInstruments.FCU2AsicInstrument)
                    results.SoaCurrent_Dac = DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic.mAToDacForFrontSoa_Asic((float)iSoa_mA);
            }
            else
            {
                results.LevelOk = false;
                DsdbrUtils.SetSectionCurrent_mA(DSDBRSection.SOA, initialSoaCurrent_mA);
            }

            // Return status
            return results;
        }

        /// <summary>
        /// Adjust Power level using SOA until a predefined Tap photocurrent is achieved
        /// </summary>
        /// <param name="targetPhotoCurrent_A">Target tap photocurrent (A)</param>
        /// <param name="photoCurrentToleranceRatio">Tolerance Ratio of photocurrent (e.g. within a 1/10th of target would be 0.1)</param>
        /// <param name="tap">Tap object</param>
        /// <returns>Tuning results structure</returns>
        public static TapLevelRes TapLevel(IInstType_ElectricalSource tap, double targetPhotoCurrent_A, 
            double photoCurrentToleranceRatio)
        {
            // Local constants
            const int maxIterations = 10;
            const int loopDelay_ms = 500;
            const double maxISoaDelta_mA = 20;
            const double maxIsoa_mA = 200;
            
            // Get initial conditions
            double photoCurrentTolerance_A = Math.Abs(targetPhotoCurrent_A / photoCurrentToleranceRatio);
            double initPhotoCurrent_A = tap.CurrentActual_amp;
            double photoCurrent_A = initPhotoCurrent_A;            
            double initialSoaCurrent_mA = DsdbrUtils.ReadSectionCurrent_mA(DSDBRSection.SOA);
            double iSoa_mA = initialSoaCurrent_mA;
            double iSoaCurrentSlope = initialSoaCurrent_mA / initPhotoCurrent_A;
            int loopCount = 0;
            double isoaDelta = 0;
            
            // Adjust power
            while (Math.Abs(targetPhotoCurrent_A - photoCurrent_A) > photoCurrentTolerance_A &&
                                                        loopCount < maxIterations)
            {
                // Calculate new SOA current & limit delta and absolute value
                isoaDelta = (targetPhotoCurrent_A - photoCurrent_A) * iSoaCurrentSlope;
                isoaDelta = Math.Sign(isoaDelta) * Math.Min(Math.Abs(isoaDelta), maxISoaDelta_mA);
                iSoa_mA += isoaDelta;
                iSoa_mA = Math.Min(iSoa_mA, maxIsoa_mA);
                iSoa_mA = Math.Max(iSoa_mA, 0);

                // Apply new Isoa
                DsdbrUtils.SetSectionCurrent_mA(DSDBRSection.SOA, iSoa_mA);

                // Settling time
                Thread.Sleep(loopDelay_ms);

                // Read new values
                photoCurrent_A = tap.CurrentActual_amp;
                iSoa_mA = DsdbrUtils.ReadSectionCurrent_mA(DSDBRSection.SOA);

                loopCount++;
            }

            TapLevelRes results = new TapLevelRes();
            // If tuned OK then set return flag else reset Isoa back to original
            if (loopCount < maxIterations)
            {
                results.LevelOk = true;
                results.FibrePower_dBm = Measurements.ReadOpticalPower(OpticalPowerHead.MZHead, Measurements.PowerUnits.dBm);
                results.SoaCurrent_mA = iSoa_mA;
                if (DsdbrUtils.DsdbrDriverInstrsToUse == DsdbrDriveInstruments.FCU2AsicInstrument)
                    results.SoaCurrent_Dac = DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic.mAToDacForFrontSoa_Asic((float)iSoa_mA);
            }
            else
            {
                results.LevelOk = false;
                DsdbrUtils.SetSectionCurrent_mA(DSDBRSection.SOA, initialSoaCurrent_mA);
            }

            // Return status
            return results;
        }
        #endregion

    }
}

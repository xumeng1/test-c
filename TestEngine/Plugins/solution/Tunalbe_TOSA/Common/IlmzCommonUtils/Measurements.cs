// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// Measurements.cs
//
// Author: Tony Foster, 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Threading;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestSolution.IlmzCommonInstrs;
using Bookham.TestLibrary.Algorithms;
using Bookham.TestSolution.IlmzCommonData;
using Bookham.TestLibrary.Instruments;
using Bookham.TestLibrary.BlackBoxes;

namespace Bookham.TestSolution.IlmzCommonUtils
{
    /// <summary>
    /// General Measurements
    /// </summary>
    public static class Measurements
    {
        #region Instruments

        /// <summary>
        /// Wavemeter instrument reference
        /// </summary>
        public static InstType_SharedWaveMeter Wavemeter
        {
            set { wavemeter = value; }
            get { return wavemeter; }
        }

        /// <summary>
        /// OSA instrument reference
        /// </summary>
        public static IInstType_OSA OSA
        {
            set { osa = value; }
            get { return osa; }
        }

        /// <summary>
        /// PXIT Reference power detector
        /// </summary>
        public static IInstType_OpticalPowerMeter OpmCgDirect
        {
            set { opmCgDirect = value; }
            get { return opmCgDirect; }
        }

        /// <summary>
        /// PXIT Filter power detector
        /// </summary>
        public static IInstType_OpticalPowerMeter OpmCgFiltered
        {
            set { opmCgFiltered = value; }
            get { return opmCgFiltered; }
        }

        /// <summary>
        /// Ag8163 internal power detector
        /// </summary>
        public static IInstType_OpticalPowerMeter MzHead
        {
            set { mzHead = value; }
            get { return mzHead; }
        }

       
        /// <summary>
        /// Ag8163 external power detector
        /// </summary>
        public static IInstType_OpticalPowerMeter ExternalHead
        {
            set { externalHead = value; }
            get { return externalHead; }
        }

        /// <summary>
        /// Fullband control unit instruments group
        /// </summary>
        public static FCUInstruments FCUInstrs
        {
            set { fcuInstrs = value; instrsToUse = DsdbrDriveInstruments.FCUInstruments; }
            get { return fcuInstrs; }
        }

        /// <summary>
        /// PXI instruments group
        /// </summary>
        public static DsdbrInstruments PXIInstrs
        {
            set { pxiInstrs = value; instrsToUse = DsdbrDriveInstruments.PXIInstruments; }
            get { return pxiInstrs; }
        }
        // added by Alice.Huang, for TOSA GB test 
        // with DSDBR Source is ASIC on chip, and control by FCU
        // on 2010-01-27
        /// <summary>
        ///  get/set FCU2ASIC instruments group. <see cref="fcu2AsicInstrs"/>
        /// </summary>
        public static FCU2AsicInstruments FCU2AsicInstrs
        {
            set
            {
                fcu2AsicInstrs = value;
                instrsToUse = DsdbrDriveInstruments.FCU2AsicInstrument;
            }
            get
            {
                return fcu2AsicInstrs;
            }
        }
        #endregion

        #region Measurement functions
        /// <summary>
        /// get or set frequency, this value isn't from wavemeter, It just come itu point file.
        /// Echo added this function in this class. to support record sweep data and calibrated power data
        /// </summary>
        public static double FrequencyWithoutMeter
        {
            get
            {
                return freq_Ghz_without_test;
            }
            set
            {
                freq_Ghz_without_test = value;
                CurrentFrequency_GHz = value;
                currentWavelength_nm = Alg_FreqWlConvert.Freq_GHz_TO_Wave_nm(value);
            }
        }


        /// <summary>
        /// Current frequency 
        /// </summary>
        public static double CurrentFrequency_GHz
        {
            // Return local value. Read frequency if not available
            get
            {
                if (currentFrequency_GHz < 180000 || currentFrequency_GHz > 200000)
                    currentFrequency_GHz = ReadFrequency_GHz();
                return currentFrequency_GHz;
            }
            // Set local frequency & wavelength
            set
            {
                // Check that value is sensible
                if (value > 180000 && value < 200000)
                {
                    currentFrequency_GHz = value;
                    currentWavelength_nm = Alg_FreqWlConvert.Freq_GHz_TO_Wave_nm(value);
                }
                else
                {
                    currentWavelength_nm = 0;
                }
            }
        }

        /// <summary>
        /// Read frequency on wavemeter
        /// </summary>
        /// <returns>frequency in GHz</returns>
        public static double ReadFrequency_GHz()
        {
            // Read frequency
          //  wavemeter.SetDefaultState();
            double freq = wavemeter.Frequency_GHz;
            // Update cached values
            CurrentFrequency_GHz = freq;
            // Return frequency
            return freq;
        }

        /// <summary>
        /// Read wavelength on wavemeter
        /// </summary>
        /// <returns>frequency in GHz</returns>
        public static double ReadWavelength_nm()
        {
            // Read frequency
            double freq = ReadFrequency_GHz();

            // Return wavelength (currentWavelength_nm is cached from readFrequency_GHz)
            return currentWavelength_nm;
        }

        /// <summary>
        /// Read Optical Power
        /// </summary>
        /// <param name="head">Power detector to use</param>
        /// <param name="units">Power units</param>
        /// <returns>Power in specified units</returns>
        public static double ReadOpticalPower(OpticalPowerHead head, PowerUnits units)
        {
            IInstType_OpticalPowerMeter detectorHead;
            switch (head)
            {
                // External Head
                case OpticalPowerHead.ExternalHead:
                    detectorHead = externalHead;
                    break;
                // MZ head
                case OpticalPowerHead.MZHead:
                    detectorHead = mzHead;
                    break;
                // Closegrid Direct detector
                case OpticalPowerHead.OpmCgDirect:
                    detectorHead = opmCgDirect;
                    break;
                // Closegrid Filtered detector
                case OpticalPowerHead.OpmCgFiltered:
                    detectorHead = opmCgFiltered;
                    break;
                default:
                    throw new ArgumentException("Unknown optical power detector '" + head.ToString() + "' in Measurements.ReadOpticalPower'");
            }

            double power = 0;

            // Set head to correct mode. Note that CG heads only work in mW.
            if ((units == PowerUnits.dBm || units == PowerUnits.dBm_uncal)
                && head != OpticalPowerHead.OpmCgDirect && head != OpticalPowerHead.OpmCgFiltered)
                detectorHead.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;
            else
                detectorHead.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_mW;

            // Set wavelength
            currentWavelength_nm = Alg_FreqWlConvert.Freq_GHz_TO_Wave_nm(Measurements.FrequencyWithoutMeter);
            detectorHead.Wavelength_nm = currentWavelength_nm;

            // Read power and check under range...
            int maxTryCount = 3;
            int triedCount = 0;
            do
            {
                power = detectorHead.ReadPower();
                
                if (!double.IsInfinity(power))
                    break;
                else
                    detectorHead.SetDefaultState();       //Add by tim
            }
            while (++triedCount <= maxTryCount);

            if (units == PowerUnits.dBm)
                if (head == OpticalPowerHead.OpmCgDirect || head == OpticalPowerHead.OpmCgFiltered)
                {
                    power = OpticalPowerCal.GetCorrectedPower_mW(head, power, currentFrequency_GHz);
                    power = Alg_PowConvert_dB.Convert_mWtodBm(power);
                }
                else
                {
                    power = OpticalPowerCal.GetCorrectedPower_dBm(head, power, currentFrequency_GHz);
                }
            else if (units == PowerUnits.mW)
                power = OpticalPowerCal.GetCorrectedPower_mW(head, power, currentFrequency_GHz);

            // Return power
            return power;
        }

        /// <summary>
        /// Calculate TCMZ Electrical power dissipation.
        /// </summary>
        /// <param name="frontSectionPair">Front section pair</param>
        /// <param name="dsdbrTec">DSDBR TEC</param>
        /// <param name="mzTec">MZ TEC</param>
        /// <returns></returns>
        public static PowerDissResults ElecPowerDissipation(int frontSectionPair, 
            IInstType_TecController dsdbrTec, IInstType_TecController mzTec)
        {
            // Check section IV sweep data
            if (sectionIVSweepRawData == null && 
                instrsToUse == DsdbrDriveInstruments.FCUInstruments) 
            {
                throw new ArgumentNullException("FCU testkits need raw section IV sweep data for voltage calculation!");
            }

            if (instrsToUse == DsdbrDriveInstruments.PXIInstruments) // PXI testkits
            {
                return elecPowerDissipation(pxiInstrs, frontSectionPair, dsdbrTec, mzTec);
            }
            else if (instrsToUse == DsdbrDriveInstruments.FCU2AsicInstrument)
            {
                return elecPowerDissipation(fcu2AsicInstrs, dsdbrTec);
            }
            else // FCU testkits
            {
                return elecPowerDissipation(fcuInstrs, frontSectionPair, dsdbrTec, mzTec);
            }
        }

        #endregion

        #region Set Properties
        /// <summary>
        /// Section IV sweep raw data
        /// </summary>
        public static SectionIVSweepData[] SectionIVSweepRawData
        {
            set { sectionIVSweepRawData = value; }
        }
        #endregion

        #region Enums
        /// <summary>
        /// Optical power measurement units
        /// </summary>
        public enum PowerUnits
        {
            /// <summary>
            /// dBm
            /// </summary>
            dBm,
            /// <summary>
            /// mW
            /// </summary>
            mW,
            /// <summary>
            /// Uncalibrated dBm
            /// </summary>
            dBm_uncal,
            /// <summary>
            /// Uncalibrated mW 
            /// </summary>
            mW_uncal
        }
        #endregion

        #region Structures
        /// <summary>
        /// TCMZ Electrical Dissipation results
        /// </summary>
        public struct PowerDissResults
        {
            /// <summary>
            /// Dsdbr TEC current A
            /// </summary>
            public double TecDsdbr_A;
            /// <summary>
            /// Dsdbr TEC voltage V
            /// </summary>
            public double TecDsdbr_V;
            /// <summary>
            /// MZ TEC current A
            /// </summary>
            public double TecMz_A;
            /// <summary>
            /// MZ TEC voltage V
            /// </summary>
            public double TecMz_V;
            /// <summary>
            /// Dsdbr Gain section current A
            /// </summary>
            public double Gain_A;
            /// <summary>
            /// Dsdbr Gain section voltage V
            /// </summary>
            public double Gain_V;
            /// <summary>
            /// Dsdbr Phase section current A
            /// </summary>
            public double Phase_A;
            /// <summary>
            /// Dsdbr Phase section voltage V
            /// </summary>
            public double Phase_V;
            /// <summary>
            /// Dsdbr Rear section current A
            /// </summary>
            public double Rear_A;
            /// <summary>
            /// Dsdbr Rear section voltage V
            /// </summary>
            public double Rear_V;
            /// <summary>
            /// Dsdbr SOA section current A
            /// </summary>
            public double SOA_A;
            /// <summary>
            /// Dsdbr SOA section voltage V
            /// </summary>
            public double SOA_V;
            /// <summary>
            /// Dsdbr first front section current A
            /// </summary>
            public double FrontFirst_A;
            /// <summary>
            /// Dsdbr first front section voltage V
            /// </summary>
            public double FrontFirst_V;
            /// <summary>
            /// Dsdbr second front section current A
            /// </summary>
            public double FrontSecond_A;
            /// <summary>
            /// Dsdbr second front section voltage V
            /// </summary>
            public double FrontSecond_V;

            /// <summary>
            /// Laser total current
            /// </summary>
            public double LaserTotalCurrent_A;
            /// <summary>
            /// Laser power W
            /// </summary>
            public double LaserPower_W;
            /// <summary>
            /// Package power W
            /// </summary>
            public double PackagePower_W;

            /// <summary>
            /// Dsdbr SOA section current A
            /// </summary>
            public double RearSOA_A;
            /// <summary>
            /// Dsdbr SOA section voltage V
            /// </summary>
            public double RearSOA_V;
        }

        #endregion

        #region Private Data

        private static InstType_SharedWaveMeter wavemeter;
        private static IInstType_OSA osa;
        private static IInstType_OpticalPowerMeter opmCgDirect;
        private static IInstType_OpticalPowerMeter opmCgFiltered;
        private static IInstType_OpticalPowerMeter mzHead;
        private static IInstType_OpticalPowerMeter externalHead;

        private static FCUInstruments fcuInstrs;
        private static DsdbrInstruments pxiInstrs;

        // added by Alice.Huang, add FCU2ASIC OPTION for TOSA GB test  on 2010-01-27
        
        /// <summary>
        /// FCU TO Asic intrtrument groups, 
        /// Fcu control Asic, 
        /// Asic source dsdbr sections 
        /// Asic has an power supply
        /// </summary>
        /// 
        private static FCU2AsicInstruments fcu2AsicInstrs;
        private static DsdbrDriveInstruments instrsToUse;

        private static SectionIVSweepData[] sectionIVSweepRawData = null;

        private static double currentFrequency_GHz;
        private static double currentWavelength_nm;
        private static double freq_Ghz_without_test;//frequency from low cost test set
        #endregion

        #region Private methods

        /// <summary>
        /// Calculate TCMZ Electrical power dissipation using PXI instruments
        /// </summary>
        /// <param name="dsdbrInstrs">DSDBR instruments class</param>
        /// <param name="frontSectionPair">Front section pair</param>
        /// <param name="dsdbrTec">DSDBR TEC</param>
        /// <param name="mzTec">MZ TEC</param>
        /// <returns></returns>
        private static PowerDissResults elecPowerDissipation(DsdbrInstruments dsdbrInstrs,
            int frontSectionPair, IInstType_TecController dsdbrTec, IInstType_TecController mzTec)
        {
            PowerDissResults results = new PowerDissResults();
            DSDBRSection frontSection1 = (DSDBRSection)Enum.Parse(typeof(DSDBRSection), "Front" + frontSectionPair);
            DSDBRSection frontSection2 = (DSDBRSection)Enum.Parse(typeof(DSDBRSection), "Front" + (frontSectionPair + 1));

            results.TecDsdbr_A = dsdbrTec.TecCurrentActual_amp;
            results.TecDsdbr_V = dsdbrTec.TecVoltageActual_volt;
            results.TecMz_A = mzTec.TecCurrentActual_amp;
            results.TecMz_V = mzTec.TecVoltageActual_volt;

            results.Gain_A = dsdbrInstrs.CurrentSources[DSDBRSection.Gain].CurrentActual_amp;
            results.Gain_V = dsdbrInstrs.CurrentSources[DSDBRSection.Gain].VoltageActual_Volt;
            results.Phase_A = dsdbrInstrs.CurrentSources[DSDBRSection.Phase].CurrentActual_amp;
            results.Phase_V = dsdbrInstrs.CurrentSources[DSDBRSection.Phase].VoltageActual_Volt;
            results.Rear_A = dsdbrInstrs.CurrentSources[DSDBRSection.Rear].CurrentActual_amp;
            results.Rear_V = dsdbrInstrs.CurrentSources[DSDBRSection.Rear].VoltageActual_Volt;
            results.SOA_A = dsdbrInstrs.CurrentSources[DSDBRSection.SOA].CurrentActual_amp;
            results.SOA_V = dsdbrInstrs.CurrentSources[DSDBRSection.SOA].VoltageActual_Volt;

            results.FrontFirst_A = dsdbrInstrs.CurrentSources[frontSection1].CurrentActual_amp;
            results.FrontFirst_V = dsdbrInstrs.CurrentSources[frontSection1].VoltageActual_Volt;
            results.FrontSecond_A = dsdbrInstrs.CurrentSources[frontSection2].CurrentActual_amp;
            results.FrontSecond_V = dsdbrInstrs.CurrentSources[frontSection2].VoltageActual_Volt;

            results.LaserTotalCurrent_A = results.Gain_A + results.Phase_A + results.Rear_A +
                results.SOA_A + results.FrontFirst_A + results.FrontSecond_A;

            results.LaserPower_W =
                (results.Gain_A * results.Gain_V) +
                (results.Phase_A * results.Phase_V) +
                (results.Rear_A * results.Rear_V) +
                (results.SOA_A * results.SOA_V) +
                (results.FrontFirst_A * results.FrontFirst_V) +
                (results.FrontSecond_A * results.FrontSecond_V);

            results.PackagePower_W =
                (results.TecDsdbr_A * results.TecDsdbr_V) +
                (results.TecMz_A * results.TecMz_V) +
                results.LaserPower_W;

            return results;
        }

        /// <summary>
        /// Calculate TCMZ Electrical power dissipation using FCU instruments
        /// </summary>
        /// <param name="fcuInstrs">FCU instruments group</param>
        /// <param name="frontSectionPair">Front section pair</param>
        /// <param name="dsdbrTec">DSDBR TEC</param>
        /// <param name="mzTec">MZ TEC</param>
        /// <returns></returns>
        private static PowerDissResults elecPowerDissipation(FCUInstruments fcuInstrs,
            int frontSectionPair, IInstType_TecController dsdbrTec, IInstType_TecController mzTec)
        {
            // NOTE: here we just calculate electrical power dissipation from SOA,DsdbrTec,MzTec
            // as voltage of some sections can't be measured.
            // NOTE2: We can extract section voltage data from sectionIVSweepRawData

            PowerDissResults results = new PowerDissResults();

            // Dsdbr and Mz tecs
            results.TecDsdbr_A = dsdbrTec.TecCurrentActual_amp;
            results.TecDsdbr_V = dsdbrTec.TecVoltageActual_volt;
            results.TecMz_A = mzTec.TecCurrentActual_amp;
            results.TecMz_V = mzTec.TecVoltageActual_volt;

            // Dsdbr
            results.Gain_A = fcuInstrs.FullbandControlUnit.GetIVSource(IVSourceIndexNumbers.Gain) / 1000;
            results.Phase_A = fcuInstrs.FullbandControlUnit.GetIVSource(IVSourceIndexNumbers.Phase) / 1000;
            results.Rear_A = fcuInstrs.FullbandControlUnit.GetIVSource(IVSourceIndexNumbers.Rear) / 1000;
            results.SOA_A = fcuInstrs.SoaCurrentSource.CurrentActual_amp;
            results.Gain_V = getSectionVoltage(DSDBRSection.Gain, results.Gain_A * 1000);
            results.Phase_V = getSectionVoltage(DSDBRSection.Phase, results.Phase_A * 1000);
            results.Rear_V = getSectionVoltage(DSDBRSection.Rear, results.Rear_A * 1000);
            results.SOA_V = fcuInstrs.SoaCurrentSource.VoltageActual_Volt;

            fcuInstrs.FullbandControlUnit.SetFrontSectionPair(frontSectionPair);
            if (frontSectionPair % 2 == 0)
            {
                // "first" is even
                results.FrontFirst_A = fcuInstrs.FullbandControlUnit.GetIVSource(IVSourceIndexNumbers.FrontEven) / 1000;
                results.FrontSecond_A = fcuInstrs.FullbandControlUnit.GetIVSource(IVSourceIndexNumbers.FrontOdd) / 1000;
                if(results.FrontSecond_A < 0)
                {
                    results.FrontSecond_A = 0;
                }
            }
            else
            {
                // "first" is odd
                results.FrontFirst_A = fcuInstrs.FullbandControlUnit.GetIVSource(IVSourceIndexNumbers.FrontOdd) / 1000;
                results.FrontSecond_A = fcuInstrs.FullbandControlUnit.GetIVSource(IVSourceIndexNumbers.FrontEven) / 1000;
                if (results.FrontSecond_A < 0)
                {
                    results.FrontSecond_A = 0;
                }
            }
            DSDBRSection frontSection1 = (DSDBRSection)Enum.Parse(typeof(DSDBRSection), "Front" + frontSectionPair);
            DSDBRSection frontSection2 = (DSDBRSection)Enum.Parse(typeof(DSDBRSection), "Front" + (frontSectionPair + 1));
            results.FrontFirst_V = getSectionVoltage(frontSection1, results.FrontFirst_A * 1000);
            results.FrontSecond_V = getSectionVoltage(frontSection2, results.FrontSecond_A * 1000);

            // Laser total current
            results.LaserTotalCurrent_A = results.Gain_A + results.Phase_A + results.Rear_A +
                results.SOA_A + results.FrontFirst_A + results.FrontSecond_A;

            // Laser power dissipation
            results.LaserPower_W =
                (results.Gain_A * results.Gain_V) +
                (results.Phase_A * results.Phase_V) +
                (results.Rear_A * results.Rear_V) +
                (results.SOA_A * results.SOA_V) +
                (results.FrontFirst_A * results.FrontFirst_V) +
                (results.FrontSecond_A * results.FrontSecond_V);

            // Package power dissipation
            results.PackagePower_W =
                (results.TecDsdbr_A * results.TecDsdbr_V) +
                (results.TecMz_A * results.TecMz_V) +
                results.LaserPower_W;

            return results;
        }

        /// <summary>
        /// Calculate TCMZ Electrical power dissipation using FCU2Asic instruments
        /// </summary>
        /// <param name="fcu2AsicInstrs">FCU2Asic instruments group</param>        
        /// <param name="dsdbrTec">DSDBR TEC</param>
        /// <param name="mzTec">MZ TEC</param>
        /// <returns></returns>
        private static PowerDissResults elecPowerDissipation(FCU2AsicInstruments fcu2AsicInstrs, IInstType_TecController dsdbrTec)
        {
            // NOTE: here we just calculate electrical power dissipation from SOA,DsdbrTec,MzTec
            // as voltage of some sections can't be measured.
            
            // Read imbs current to recover latter
            //double i_imbL = fcu2AsicInstrs.Fcu2Asic.IimbLeft_mA;
            //double i_imbR = fcu2AsicInstrs.Fcu2Asic.IimbRight_mA;

            // Set imbs current to be 0 to avoid power dissipation on them 
            // but take accout to be DSDBR's dissipation
            //fcu2AsicInstrs.Fcu2Asic.IimbRight_mA = 0;
            //fcu2AsicInstrs.Fcu2Asic.IimbLeft_mA = 0;
            //System.Threading.Thread.Sleep(500);

            PowerDissResults results = new PowerDissResults();

            // Dsdbr and Mz tecs
            results.TecDsdbr_A = dsdbrTec.TecCurrentActual_amp;
            results.TecDsdbr_V = dsdbrTec.TecVoltageActual_volt;
            results.TecMz_A = 0; //mzTec.TecCurrentActual_amp;
            results.TecMz_V = 0; // mzTec.TecVoltageActual_volt;

            // Dsdbr
            results.RearSOA_A = fcu2AsicInstrs.Fcu2Asic.IRearSoa_mA / 1000;
            results.Gain_A = fcu2AsicInstrs.Fcu2Asic.IGain_mA / 1000;
            results.Phase_A = fcu2AsicInstrs .Fcu2Asic .IPhase_mA  / 1000;
            results.Rear_A = fcu2AsicInstrs .Fcu2Asic .IRear_mA / 1000;
            results.SOA_A = fcu2AsicInstrs .Fcu2Asic .ISoa_mA /1000;


            int frontSectionPair = fcu2AsicInstrs.Fcu2Asic.GetFrontPairNumber();
            if (frontSectionPair > 0)
            {
                if (frontSectionPair % 2 == 0)
                {
                    // "first" is even
                    results.FrontFirst_A = fcu2AsicInstrs .Fcu2Asic .IEven_mA  / 1000;
                    results.FrontSecond_A =fcu2AsicInstrs .Fcu2Asic .IOdd_mA  / 1000;                    
                }
                else
                {
                    // "first" is odd
                    results.FrontFirst_A = fcu2AsicInstrs .Fcu2Asic .IOdd_mA  / 1000;
                    results.FrontSecond_A = fcu2AsicInstrs .Fcu2Asic .IEven_mA  / 1000;                    
                }
            }
            else
            {
                results.FrontFirst_A = 0;
                results.FrontSecond_A = 0;
            }

            // to be decide
            // Laser total current
            results.LaserTotalCurrent_A = fcu2AsicInstrs.AsicVccSource.CurrentActual_amp;
            double vccTemp = fcu2AsicInstrs.AsicVccSource.VoltageActual_Volt;
            double veeTemp = fcu2AsicInstrs.AsicVeeSource.VoltageActual_Volt;
            if (veeTemp > 0) veeTemp = 0 - veeTemp;

            results.Gain_V = vccTemp ;
            results.Phase_V = vccTemp;
            results.Rear_V = vccTemp;
            results.SOA_V = (results.SOA_A >= 0) ? vccTemp : veeTemp;
            results.RearSOA_V = vccTemp;
           
            results.FrontFirst_V = vccTemp;
            results.FrontSecond_V = vccTemp; 
            
            // Laser power dissipation
            //results.LaserPower_W = fcu2AsicInstrs.AsicVccSource.CurrentActual_amp * fcu2AsicInstrs.AsicVccSource.VoltageActual_Volt +

            //                        fcu2AsicInstrs.AsicVeeSource.CurrentActual_amp * 
            //                        Math .Abs (fcu2AsicInstrs.AsicVeeSource.VoltageActual_Volt);
            results.LaserPower_W = results.LaserTotalCurrent_A * vccTemp + Math.Abs(fcu2AsicInstrs.AsicVeeSource.CurrentActual_amp * veeTemp);
            // Package power dissipation
            results.PackagePower_W = (results.TecDsdbr_A * results.TecDsdbr_V) +  results.LaserPower_W;

            //fcu2AsicInstrs.Fcu2Asic.IimbLeft_mA =(float ) i_imbL;
            //fcu2AsicInstrs.Fcu2Asic.IimbRight_mA =(float ) i_imbR;
            //System.Threading.Thread.Sleep(20);
            return results;
        }
        /// <summary>
        /// Get section voltage in V at a given section current in mA
        /// </summary>
        /// <param name="section">DSDBR section</param>
        /// <param name="sectionCurrent_mA">Section current in mA</param>
        /// <returns>Fitted section voltage</returns>
        private static double getSectionVoltage(DSDBRSection section, double sectionCurrent_mA)
        {
            // Get the correct section IV sweep data firstly
            SectionIVSweepData sectionIVSweepData = null;
            foreach (SectionIVSweepData sweepData in sectionIVSweepRawData)
            {
                if (sweepData.Section == section)
                {
                    sectionIVSweepData = sweepData;
                    break;
                }
            }

            // Get fitted voltage
            return sectionIVSweepData.GetFitVoltage_V(sectionCurrent_mA);
        }

        #endregion

    }
}

using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.Algorithms;
using Bookham.TestLibrary.Instruments;
using Bookham.TestLibrary.Utilities;
//using Bookham.TestSolution.Instruments;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;

namespace Bookham.TestSolution.IlmzCommonUtils
{
    /// <summary>
    /// Class to measure SMSR from an OSA and return value in dBm.
    /// </summary>
    public static class SMSRMeasurement
    {
        /// <summary>
        /// Measure SMSR, return value in dBm.
        /// </summary>
        /// <param name="smsrCentreOpticalFreq_GHz">Centre optical frequency of signal peak</param>
        /// <returns>SMSR in dBm</returns>
        public static double MeasureSMSR(double smsrCentreOpticalFreq_GHz)
        {
            
            Inst_Ag8614xA.OptPowerPoint[] tracedata;
            if (traceDataPath == "" || !Directory.Exists(traceDataPath))
                traceDataPath = @"results\";
            //datumlist RtData=new DataumList();
            double smsr_dBm = 0;
            //alice : add osa option and put it in a try-catch block to insure instrument will
           // be unlock Event when an exception happen
            try
               
            {
                //alice  add this part to lock instrument and switch optical path if osa shared
                if (TcmzOSAshare.OsaShareFlag)
                {
                    // if the instrument can't lock in share application, throw an exception
                    bool lockflag;
                    int lockCount = 0;
                    do
                    {
                        lockflag = TcmzOSAshare.LockInstruments();
                        lockCount++;
                    }
                    while ((!lockflag) && (lockCount < 3));
                    if (!lockflag)
                    {
                        throw new InstrumentException("can't lock instrument" +
                            "\nplease check if shared instrument used by other test kit without release" +
                            " or instrument lock file is exist in the setting path");
                    }
                    TcmzOSAshare.SetDioToMeasurement();
                    // OSW min switch time : 10~ 15ms
                    // + DIO output stable time
                    System.Threading.Thread.Sleep(50);
                }
            // Set OSA sweepmode to single
            SMSRMeasurement.osa.SweepMode = InstType_OSA.SweepModes.Single;
            strCentreFreq = smsrCentreOpticalFreq_GHz.ToString();

            // Set OSA sensitivity if Agilent Ag8164x
            Inst_Ag8614xA Ag8616xOsa = null;
            if (SMSRMeasurement.osa.DriverName.ToLower().Contains("ag8614"))
            {
                Ag8616xOsa = (Inst_Ag8614xA)SMSRMeasurement.osa;
                Ag8616xOsa.Sensitivity_dBm = SMSRMeasurement.smsrOSASensitivity_dBm;
                Ag8616xOsa.ScreenTitle = "Measuring SMSR";
            }
			// Add by chongjian.liang 2013.6.9
            else if (SMSRMeasurement.osa.DriverName.ToUpper().Contains("YKAQ6370"))
            {
                Instr_YKAQ6370 YKAQ6370 = (Instr_YKAQ6370)SMSRMeasurement.osa;
                YKAQ6370.SensorSensitivity = Instr_YKAQ6370.SensitivityEnum.MID;
                YKAQ6370.ScreenTitle = "Measuring SMSR";
            }

            // Configure OSA
            double centreWL = Alg_FreqWlConvert.Freq_GHz_TO_Wave_nm(smsrCentreOpticalFreq_GHz);
            SMSRMeasurement.osa.WavelengthStart_nm = centreWL - SMSRMeasurement.smsrSpan_nm / 2;
            SMSRMeasurement.osa.WavelengthStop_nm = centreWL + SMSRMeasurement.smsrSpan_nm / 2;
            SMSRMeasurement.osa.ResolutionBandwidth = SMSRMeasurement.smsrRBW_nm;
            SMSRMeasurement.osa.TracePoints = SMSRMeasurement.smsrNumPts;

            // Initialise measurements
            double peakPower_dBm = -999;
            double peakFrequency_GHz = 0;
            double nextPeakPower_dBm = -999;
            double nextPeakFrequency_GHz = 0;

            int sweepCount = 0;
            int PeakFlag = 0;
            bool Iserror = false;
            bool Is6747 = false;
            // Make measurements
            // Steven: first get the Peak power&freq , then get the NextPeak power&freq
            do
            {
                SMSRMeasurement.osa.SweepStart();
                do
                {
                    System.Threading.Thread.Sleep(100);
                } while (SMSRMeasurement.osa.Status != InstType_OSA.ChannelState.DataReady && SMSRMeasurement.osa.Status != InstType_OSA.ChannelState.Idle);
                
                // Steven: Get the Peak Power and Freq
                try
                {
                    SMSRMeasurement.osa.MarkerToPeak();
                    peakPower_dBm = SMSRMeasurement.osa.MarkerAmplitude_dBm;
                    peakFrequency_GHz = Alg_FreqWlConvert.Wave_nm_TO_Freq_GHz(SMSRMeasurement.osa.MarkerWavelength_nm);
                    PeakFlag = 1;
                }
                catch
                {
                    PeakFlag = 0;
                }                                
                sweepCount++;
            } while (sweepCount < 3 && PeakFlag == 0);
            
            // Steven: Get the NextPeak Power and Freq  
               try
                {
                    do
                    {
                    SMSRMeasurement.osa.MarkerToNextPeak(InstType_OSA.Direction.Unspecified);
                    nextPeakPower_dBm = SMSRMeasurement.osa.MarkerAmplitude_dBm;
                    nextPeakFrequency_GHz = Alg_FreqWlConvert.Wave_nm_TO_Freq_GHz(SMSRMeasurement.osa.MarkerWavelength_nm);
                    
                    smsr_dBm = peakPower_dBm - nextPeakPower_dBm;
                    } while (Math.Abs(peakFrequency_GHz - nextPeakFrequency_GHz) < 15);
                }
                catch (Exception e)
                {
                    Iserror = true;
                    string error = e.Message;
                    System.Text.RegularExpressions.Regex rex = new System.Text.RegularExpressions.Regex("6747");
                    if (rex.Match(error).Success)
                    {
                        Is6747 = true;
                    }
                    else
                        Is6747 = false;
                }
                // Steven: If no sensible measurement then measure SMSR at set wavelength from peak
                if (Iserror==true)
                {                
                     if (Is6747 == true)
                     {
                         SMSRMeasurement.osa.MarkerWavelength_nm = Alg_FreqWlConvert.Freq_GHz_TO_Wave_nm(peakFrequency_GHz + freqOffsetWhenNoSecondPeak_GHz);//The value 60 may need to be determined from setup file
                         nextPeakPower_dBm = SMSRMeasurement.osa.MarkerAmplitude_dBm;
                         SMSRMeasurement.osa.MarkerWavelength_nm = Alg_FreqWlConvert.Freq_GHz_TO_Wave_nm(peakFrequency_GHz - freqOffsetWhenNoSecondPeak_GHz);//The value 60 may need to be determined from setup file
                         nextPeakPower_dBm = Math.Max(nextPeakPower_dBm, SMSRMeasurement.osa.MarkerAmplitude_dBm);
                         
                         smsr_dBm = peakPower_dBm - nextPeakPower_dBm;
                     }
                     else
                         smsr_dBm = -999;                
                }
 
            // Steven: Save abnormal SMSR data

            if ((smsr_dBm <= 0) || (smsr_dBm > 80) || (Iserror == true))
            {
                tracedata = SMSRMeasurement.OSA.GetDisplayTrace;
                    if ((smsr_dBm <=0) && (Iserror == false))
                    {
                        strAbnormalReason = "Neg_min";
                    }
                    else if ((smsr_dBm > 80) && (Iserror == false))
                    {
                        strAbnormalReason = "Pos_max";
                    }
                    else if ((Iserror == true) && (Is6747 == true))
                    {
                        strAbnormalReason = "Error_6747";
                    }
                    else if ((Iserror == true) && (Is6747 == false))
                    {
                        strAbnormalReason = "Error_not6747";
                    }
                    strFileName = traceDataPath + "SMSR_Spectrum_" + strSN + "_" + strCentreFreq + "_" +
                        DateTime.Now.ToString("yyyyMMddHHmmssfff") + "_"+ strAbnormalReason + ".csv";
                    writeTraceFile(tracedata,
                                Alg_FreqWlConvert.Freq_GHz_TO_Wave_nm(peakFrequency_GHz),
                                Alg_FreqWlConvert.Freq_GHz_TO_Wave_nm(nextPeakFrequency_GHz), strFileName);
                    
             }

             // Steven: Zip smsr file
             if (!string.IsNullOrEmpty(strFileName) && !string.IsNullOrEmpty(SMSRzipfilename))
             {                 
                 Util_ZipFile zipFile = new Util_ZipFile(SMSRzipfilename);
                 zipFile.AddFileToZip(strFileName);
                 //zipFile.Dispose();//Echo remed this line
             }
             
            if (Ag8616xOsa != null) Ag8616xOsa.ScreenTitle = "SMSR " + smsr_dBm.ToString("0.0") + "dB";
        }
        catch (DirectoryNotFoundException e)
        {
            throw e;
        }
        catch (InstrumentException e)
        {
            throw e;
        }
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            if (TcmzOSAshare.OsaShareFlag)
            {
                if (TcmzOSAshare.DioRecoverFlag) TcmzOSAshare.RecoverDIO();
                TcmzOSAshare.UnlockInstruments();
            }

        }
            return smsr_dBm;
        }
        /// <summary>
        /// write OSA trace form data to file
        /// </summary>
        /// <param name="tracedata"> Osa trace data to be record </param>
        /// <param name="peakWavelen"> peak wave wavelen in trace data </param>
        /// <param name="nextpeakWavelen"> the second peak wave wavelen in trace data </param>
        /// <param name="strFileName"> file to record trace data </param>
        private static void writeTraceFile(Inst_Ag8614xA.OptPowerPoint[] tracedata,
            double peakWavelen, double nextpeakWavelen, string strFileName)
        {

            using (StreamWriter writer = new StreamWriter(strFileName))
            {
                writer.WriteLine("Wavelength,Power");
                writer.WriteLine("Peak Wavelen," + peakWavelen.ToString());
                writer.WriteLine("NextPeakWavelen," + nextpeakWavelen.ToString());

                for (int i = tracedata.GetLowerBound(0); i <= tracedata.GetUpperBound(0); i++)
                {
                    writer.WriteLine(tracedata[i].wavelength_nm.ToString() + "," +
                        tracedata[i].power_dB.ToString());
                }
                writer.Close();

            }

        }



       

        #region Private data
        private static IInstType_OSA osa;
        private static double smsrSpan_nm;
        private static double smsrRBW_nm;
        private static double smsrOSASensitivity_dBm;
        private static int smsrNumPts;
        private static string strFileName;
        
        //private static double smsrSweepTime_s; 

        // file name elements, to record OSA Trace data when getting abnormal SMSR
        private static string strAbnormalReason = "";
        /// <summary>
        /// 
        /// </summary>
        public static string traceDataPath = "";
        /// <summary>
        /// 
        /// </summary>
        public static string strSN = "";
        /// <summary>
        /// 
        /// </summary>
        public static string strCentreFreq;
        /// <summary>
        /// 
        /// </summary>
        public static string SMSRzipfilename;

        

        private const double freqOffsetWhenNoSecondPeak_GHz = 60;
        #endregion

        #region Public properties
        /// <summary>
        /// OSA
        /// </summary>
        public static IInstType_OSA OSA
        {
            get { return osa; }
            set { osa = value; }
        }

        /// <summary>
        /// Narrow span around ITU
        /// </summary>
        public static double SMSRSpan_nm
        {
            get { return smsrSpan_nm; }
            set { smsrSpan_nm = value; }
        }

        /// <summary>
        /// Resolution bandwidth
        /// </summary>
        public static double SMSRRBW_nm
        {
            get { return smsrRBW_nm; }
            set { smsrRBW_nm = value; }
        }

        /// <summary>
        /// OSA sensitivity setting - set to resolve lower peaks
        /// </summary>
        public static double SMSROSASensitivity_dBm
        {
            get { return smsrOSASensitivity_dBm; }
            set { smsrOSASensitivity_dBm = value; }
        }

        /// <summary>
        /// Number of trace points
        /// </summary>
        public static int SMSRNumPts
        {
            get { return smsrNumPts; }
            set { smsrNumPts = value; }
        }
        #endregion

    }
}

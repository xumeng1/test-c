using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.Framework.Limits;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestSolution.IlmzCommonData;

namespace Bookham.TestLibrary.Utilities
{
    /// <summary>
    /// Class that is used to contain double-precision floating point data which can be pass/failed
    /// or missing data. Uses a Specification object internally to manage this, but this class
    /// encapsulates this and hides it from the user.
    /// </summary>
    /// <typeparam name="T">An enum that is used to index your data</typeparam>
    public class LimitCheckedDataStore<T>
    {

        #region Constructor
        /// <summary>
        /// Constructor - must call InitLimits() to initialise specification
        /// </summary>
        public LimitCheckedDataStore()
        {
            privateSpec = new Specification("Internal Spec", null, 0);
        }

        /// <summary>
        /// Initialise limits lookup
        /// </summary>
        /// <param name="spec">Specification to use</param>
        /// <param name="specNameLookup">Spec name lookup dictionary</param>
        public void InitLimits(Specification spec, Dictionary<T, string> specNameLookup)
        {
            InitLimits(spec, specNameLookup, new List<RawParamLimit>());
        }

        /// <summary>
        /// Initialise limits lookup
        /// </summary>
        /// <param name="spec">Specification to use</param>
        /// <param name="specNameLookup">Spec name lookup dictionary</param>
        /// <param name="localLimits">Array of local limits to also be used. 
        /// NB: limit names must match those in the Enum</param>
        public void InitLimits(Specification spec, Dictionary<T, string> specNameLookup,
            List<RawParamLimit> localLimits)
        {
            //setup the specification
            foreach (object obj in Enum.GetValues(typeof(T)))
            {
                // convert it back to our known enum!
                T enumVal = (T)obj;
                string enumValName = enumVal.ToString();
                if (enumValName.Contains("MzVcmCal_V"))
                {
                    string a = "ok";
                }

                // is this in local limits list?
                RawParamLimit localLimitFound = null;
                foreach (RawParamLimit ll in localLimits)
                {
                    if (ll.Name == enumValName)
                    {
                        localLimitFound = ll;
                        break;
                    }
                }

                // is this limit in specification?
                RawParamLimit rpl = null;
                string pcasName;
                bool foundPcasName = specNameLookup.TryGetValue(enumVal, out pcasName);

                // check that it is not both in the spec and locally defined
                if (foundPcasName && (localLimitFound != null))
                {
                    string errStr = String.Format(
                                "LimitsCheckedDataStore<{0}>[{1}]: Defined both as local limit and in spec as '{2}'",
                                typeof(T).Name, enumValName, pcasName);
                    throw new ArgumentException(errStr);
                }

                // if local
                if (localLimitFound != null)
                {
                    rpl = localLimitFound;
                }
                else if (foundPcasName)
                {
                    // check if we found the limit!
                    if (!spec.ParamLimitExists(pcasName))
                    {
                        string errStr = String.Format(
                            "LimitsCheckedDataStore<{0}>[{1}]: Couldn't find linked Spec Limit '{2}'",
                                typeof(T).Name, enumValName, pcasName);
                        throw new ArgumentException(errStr);
                    }

                    ParamLimit lim = spec.GetParamLimit(pcasName);
                    
                    //** Load IMB limits from TC_MZ_IMB_LEFT/RIGHT_LIMIT_MIN/MAX to apply to CH_MZ_CTRL_L_I - chongjian.liang 2013.10.11

                    Datum lowLimit = lim.LowLimit;
                    Datum highLimit = lim.HighLimit;

                    if (pcasName.ToUpper().Contains("CH_MZ_CTRL_L_I"))
                    {
                        lowLimit = new DatumDouble(lim.LowLimit.Name, ParamManager.Spec.CH_MZ_CTRL_L_I.Low);
                        highLimit = new DatumDouble(lim.HighLimit.Name, ParamManager.Spec.CH_MZ_CTRL_L_I.High);
                    }
                    else if (pcasName.ToUpper().Contains("CH_MZ_CTRL_R_I"))
                    {
                        lowLimit = new DatumDouble(lim.LowLimit.Name, ParamManager.Spec.CH_MZ_CTRL_R_I.Low);
                        highLimit = new DatumDouble(lim.HighLimit.Name, ParamManager.Spec.CH_MZ_CTRL_R_I.High);
                    }

                    rpl = new RawParamLimit(enumValName, lim.ParamType, lowLimit, highLimit, lim.Operand, lim.Priority, lim.AccuracyFactor, lim.Units);
                }
                else
                {
                    // no PCAS limit or local limit defined         
                    // we assume that this is NoLimit, of Double type           
                    rpl = new RawParamLimit(enumValName, DatumType.Double);
                }
                // add it to the channel specification
                privateSpec.Add(rpl);
            }
        }
        #endregion

        #region Private Data
        private Specification privateSpec;
        #endregion

        #region Public members
        /// <summary>
        /// Get overall status - will return Pass if there are no failed parameters
        /// </summary>
        public PassFail OverallStatus
        {
            get
            {
                return privateSpec.Status.Status;
            }
        }

        /// <summary>
        /// Get overall completeness - No data, Incomplete, Complete as an enum
        /// </summary>
        public SpecComplete OverallCompleteness
        {
            get
            {
                return privateSpec.Status.IsComplete;
            }
        }

        /// <summary>
        /// Is a parameter tested?
        /// </summary>
        /// <param name="param">parameter</param>
        /// <returns>true if tested, false if not</returns>
        public bool IsTested(T param)
        {
            ParamLimit pl = getParamLimit(param);
            bool isTested = pl.Tested;
            return isTested;
        }

        /// <summary>
        /// Pass/Fail status
        /// </summary>
        /// <param name="param">parameter</param>
        /// <returns>true if tested, false if not</returns>
        public PassFail ParamStatus(T param)
        {
            ParamLimit pl = getParamLimit(param);
            PassFail pf = pl.TestResult;
            return pf;
        }

        /// <summary>
        /// Get value as a datum
        /// </summary>
        /// <param name="param">parameter</param>
        /// <returns>value datum - throws if not tested</returns>
        public Datum GetMeasuredData(T param)
        {
            ParamLimit pl = getParamLimit(param);
            if (!pl.Tested) throw new ArgumentException("Param not tested: " + param);
            // get the unrounded data
            return pl.MeasuredData;
        }

        /// <summary>
        /// Get parameter low limit
        /// </summary>
        /// <param name="param">parameter</param>
        /// <returns>low limit</returns>
        public double GetMeasuredDataLowLimit(T param)
        {
            ParamLimit pl = getParamLimit(param);
            double lowLimit;
            if (pl.LowLimit == null)
            {
                lowLimit = -999;
            }
            else
            {
                if (pl.ParamType == DatumType.Double
                    || pl.ParamType == DatumType.Sint32
                    || pl.ParamType == DatumType.Uint32)
                {
                    lowLimit = double.Parse(pl.LowLimit.ValueToString());
                }
                else
                {
                    // Deal with internal boolean limit type
                    lowLimit = -999;
                }
            }

            return lowLimit;
        }

        /// <summary>
        /// Get parameter high limit
        /// </summary>
        /// <param name="param">parameter</param>
        /// <returns>high limit</returns>
        public double GetMeasuredDataHighLimit(T param)
        {
            ParamLimit pl = getParamLimit(param);
            double highLimit;
            if (pl.HighLimit == null)
            {
                highLimit = 999;
            }
            else
            {
                if (pl.ParamType == DatumType.Double
                    || pl.ParamType == DatumType.Sint32
                    || pl.ParamType == DatumType.Uint32)
                {
                    highLimit = double.Parse(pl.HighLimit.ValueToString());
                }
                else
                {
                    // Deal with internal boolean limit type
                    highLimit = 999;
                }
            }

            return highLimit;
        }


        /// <summary>
        /// Get double value
        /// </summary>
        /// <param name="param">parameter</param>
        /// <returns>value - throws if not tested</returns>
        public double GetValueDouble(T param)
        {
            Datum dtm = GetMeasuredData(param);
            DatumDouble dd = (DatumDouble)dtm;
            double val = dd.Value;
            return val;
        }

        /// <summary>
        /// Get signed 32-bit integer value
        /// </summary>
        /// <param name="param">parameter</param>
        /// <returns>value - throws if not tested</returns>
        public int GetValueSint32(T param)
        {
            Datum dtm = GetMeasuredData(param);
            DatumSint32 dd = (DatumSint32)dtm;
            int val = dd.Value;
            return val;
        }

        /// <summary>
        /// Get boolean value
        /// </summary>
        /// <param name="param">parameter</param>
        /// <returns>value - throws if not tested</returns>
        public bool GetValueBool(T param)
        {
            Datum dtm = GetMeasuredData(param);
            DatumBool dd = (DatumBool)dtm;
            bool val = dd.Value;
            return val;
        }

        /// <summary>
        /// Set measured data - will throw if there is a Datum type mismatch
        /// </summary>
        /// <param name="param">parameter</param>
        /// <param name="val">Datum - name is ignored here, it is only the value that is transferred</param>
        public void SetMeasuredData(T param, Datum val)
        {
            ParamLimit pl = getParamLimit(param);
            pl.SetMeasuredDataIgnoreName(val);
        }

        /// <summary>
        /// Set value
        /// </summary>
        /// <param name="param">parameter</param>
        /// <param name="val">value</param>
        public void SetValueBool(T param, bool val)
        {
            DatumBool dd = new DatumBool("NewValue", val);
            SetMeasuredData(param, dd);
        }

        /// <summary>
        /// Set value
        /// </summary>
        /// <param name="param">parameter</param>
        /// <param name="val">value</param>
        public void SetValueSint32(T param, int val)
        {
            DatumSint32 dd = new DatumSint32("NewValue", val);
            SetMeasuredData(param, dd);
        }

        /// <summary>
        /// Set value
        /// </summary>
        /// <param name="param">parameter</param>
        /// <param name="val">value</param>
        public void SetValueDouble(T param, double val)
        {
            DatumDouble dd = new DatumDouble("NewValue", Math.Round(val, 4));
            SetMeasuredData(param, dd);
        }

        /// <summary>
        /// Get failed parameters
        /// </summary>
        public T[] FailedParams
        {
            get
            {
                List<T> failedParamsList = new List<T>();
                foreach (ParamLimit p in privateSpec)
                {
                    if (p.TestResult == PassFail.Fail)
                    {
                        T param = (T)Enum.Parse(typeof(T), p.ExternalName);
                        failedParamsList.Add(param);
                    }
                }
                T[] failedParams = failedParamsList.ToArray();
                return failedParams;
            }
        }

        /// <summary>
        /// Get names of failed params in a string
        /// </summary>
        public string FailedParamNames
        {
            get
            {
                string failedParamNames = "";
                foreach (T fp in this.FailedParams) { failedParamNames += fp.ToString() + "; "; }
                return failedParamNames;
            }
        }

        #endregion

        #region Protected Functions
        #endregion

        #region Private Helper Functions
        private ParamLimit getParamLimit(T param)
        {
            string paramName = param.ToString();
            ParamLimit pl = privateSpec.GetParamLimit(paramName);
            return pl;
        }
        #endregion


    }
}

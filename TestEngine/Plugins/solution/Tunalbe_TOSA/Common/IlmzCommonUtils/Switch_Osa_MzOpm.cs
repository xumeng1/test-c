using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.Utilities;

namespace Bookham.TestSolution.IlmzCommonUtils
{
    /// <summary>
    /// OSA / MZ Optical Power Meter switch wrapper utility for TCMZ
    /// </summary>
    public class Switch_Osa_MzOpm
    {
        /// <summary>
        /// Switch position
        /// </summary>
        public enum State
        {
            /// <summary>OSA</summary>
            Osa,
            /// <summary>MZ Optical Power Meter</summary>
            MzOpm
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="swMan"></param>
        public Switch_Osa_MzOpm(Util_SwitchPathManager swMan)
        {
            this.swMan = swMan;
        }

        /// <summary>
        /// Underlying Switch Path Manager utility
        /// </summary>
        private Util_SwitchPathManager swMan;

        /// <summary>
        /// Switch state
        /// </summary>
        /// <param name="state"></param>
        public void SetState(State state)
        {            
            switch (state)
            {
                case State.MzOpm:
                    swMan.SetToPosn("MZ_OPM");
                    break;
                case State.Osa:
                    swMan.SetToPosn("OSA");
                    break;
                default:
                    throw new ArgumentException("Invalid state: " + state.ToString());
            }
            
        }
    }
}

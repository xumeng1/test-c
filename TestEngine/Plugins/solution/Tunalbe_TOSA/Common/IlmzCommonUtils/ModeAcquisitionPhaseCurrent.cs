using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.Utilities;

namespace Bookham.TestSolution.TcmzCommonUtils
{
    public class ModeAcquisitionPhaseCurrent
    {
        /// <summary>
        /// constructor
        /// </summary>
        private ModeAcquisitionPhaseCurrent()
        {
        }

        /// <summary>
        /// Calculating mode aquisition phase current
        /// </summary>
        /// <param name="file">File of forward phase sweep longitudinal mode centre line</param>
        /// <param name="rearCurrent">Rear current at that ITU operating point</param>
        /// <returns>Mode acquisition phase current</returns>
        public static double IPhaseAcquisition(string file, double rearCurrent)
        {
            double IPhaseAcq = 0;
            int IPhaseCount = 0;
            string[] previousLine;
            string[] currentLine;

            using (CsvReader csvReader = new CsvReader())
            {
                List<string[]> lines = csvReader.ReadFile(file);
                int nbrRows = lines.Count;
                int nbrCols = lines[0].Length;
                previousLine = new string[nbrCols];
                currentLine = new string[nbrCols];

                if (double.Parse(lines[1][iRearCol]) >= rearCurrent + iRearGap)
                {
                    IPhaseAcq = -999;
                    return IPhaseAcq;
                }//This should not occur... 

                for (int ii = 1; ii < nbrRows; ii++)
                {
                    currentLine = lines[ii];
                    if (Math.Abs(rearCurrent - double.Parse(currentLine[iRearCol])) < iRearGap)
                    {
                        IPhaseAcq += double.Parse(currentLine[iPhaseCol]);
                        IPhaseCount++;
                    }
                    if (double.Parse(currentLine[iRearCol]) > rearCurrent && IPhaseCount == 0)
                    {
                        double deltaIRear1 = double.Parse(currentLine[iRearCol]) - double.Parse(previousLine[iRearCol]);
                        double deltaIRear2 = rearCurrent - double.Parse(previousLine[iRearCol]);
                        double deltaIPhase1 = double.Parse(currentLine[iPhaseCol]) - double.Parse(previousLine[iPhaseCol]);
                        double deltaIPhase2 = (deltaIRear2 / deltaIRear1) * deltaIPhase1;
                        IPhaseAcq = double.Parse(previousLine[iPhaseCol]) + deltaIPhase2;
                        IPhaseCount = 1;
                        break;
                    }
                    previousLine = currentLine;
                }
            }

            if (IPhaseCount > 0)
            {
                IPhaseAcq = IPhaseAcq / IPhaseCount;
            }
            else
            {
                IPhaseAcq = -999;//When rearCurrent>=double.Parse(lines[nbrCols-1][iRearCol])+iRearGap
            }

            return IPhaseAcq;
        }


        #region Private data
        private const int iRearCol = 0;
        private const int iPhaseCol = 4;
        private const double iRearGap = 0.0001;
        #endregion
    }
}

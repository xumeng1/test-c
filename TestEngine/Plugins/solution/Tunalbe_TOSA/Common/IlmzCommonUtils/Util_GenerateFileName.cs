using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using NUnit.Framework;

namespace Bookham.TestLibrary.Utilities
{
    /// <summary>
    /// Generate File name (e.g. for a BLOB file)
    /// </summary>
    public sealed class Util_GenerateFileName
    {
        private Util_GenerateFileName() { }

        /// <summary>
        /// Generate file name with 17 digit time stamp in UTC.
        /// 
        /// Timestamp format:[y][M][d][h][min][s][ms]
        /// y=4-digit Year
        /// M=month number
        /// d=day of month
        /// h=hour
        /// min=minute
        /// s=second
        /// ms=millisecond
        /// </summary>
        /// <param name="directory">Directory to put results into</param>
        /// <param name="filenameStem">Filename stem</param>
        /// <param name="dutSerial">Serial number of DUT</param>
        /// <param name="extension">Filename extension</param>
        /// <returns></returns>
        public static string GenWithTimestamp(string directory, string filenameStem, 
            string dutSerial, string extension)
        {
            string timeStamp = DateTime.Now.ToString("yyyyMMddHHmmssfff");

            string filename = string.Format("{0}_{1}_{2}.{3}", filenameStem, dutSerial, timeStamp, extension);

            return Path.Combine(directory, filename);
        }
    }

    /// <exclude/>
    /// <summary>Test fixture class to test filename generator in nunit</summary>
    [TestFixture]
    public class Test_UtilGenFileName
    {
        /// <exclude/>
        [Test]
        public void Test1()
        {
            string filename = Util_GenerateFileName.GenWithTimestamp("a", "aFile", "1234", "csv");
        }
    }
}

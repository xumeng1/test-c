// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestLibrary.Utilities
//
// Util_RegistryAccess.cs
//
// Author: Mark Fullalove, 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;
using System.Security.AccessControl;
using NUnit.Framework;

namespace Bookham.TestLibrary.Utilities
{
    /// <summary>
    /// Helper class to open the Win32 registry
    /// </summary>
    public class Util_RegistryAccess
    {
        /// <summary>
        /// Empty private constructor
        /// </summary>
        private Util_RegistryAccess()
        {
        }

        /// <summary>
        /// Extracta the registry branch in REGEDIT4 format
        /// </summary>
        /// <param name="startKey">Starting key ( e.g. "Software\\Bookham" )</param>
        /// <param name="registryKey">The registry branch</param>
        /// <returns>REGEDIT4 format registry dump</returns>
        public static string DumpTree(string startKey, RegistryKey registryKey)
        {
            baseKey = registryKey;
            registryKey = registryKey.OpenSubKey(startKey, false);
            
            sb = new StringBuilder();
            sb.Append("REGEDIT4");

            GetSubKeys(registryKey);
            
            return sb.ToString();
        }

        /// <summary>
        /// Recursively extracts keys and values from the registry
        /// </summary>
        /// <param name="subKey">Subkey name</param>
        private static void GetSubKeys(RegistryKey subKey)
        {
            string CrLf = "\r\n";
            sb.Append( CrLf + CrLf + "[" + subKey.Name + "]");

            if (subKey.ValueCount > 0)
            {                
                foreach ( string keyName in subKey.GetValueNames() )
                {
                    string keyValue = subKey.GetValue(keyName).ToString();
                    sb.Append( CrLf +"\"" + keyName + "\"=\"" + keyValue.Replace(@"\",@"\\") + "\"");
                }
            }
            foreach (string subKeyName in subKey.GetSubKeyNames())
            {
                RegistryKey localKey = baseKey;
                localKey = subKey.OpenSubKey(subKeyName, false);
                GetSubKeys(localKey); // Recursive call
            }
            subKey.Close();
        }

        /// <summary>
        /// Holds the registry data accumulated so far
        /// </summary>
        private static StringBuilder sb;
        /// <summary>
        /// The base registry key ( e.g. Registry.LocalMachine )
        /// </summary>
        private static RegistryKey baseKey;
    }

    /// <summary>
    /// Quick test harness
    /// </summary>
    [TestFixture]
    public class Util_RegistryAccess_TEST
    {
        /// <summary>
        /// Test...
        /// </summary>
        [Test]
        public void Test1()
        {
            string regFile = Util_RegistryAccess.DumpTree(@"Software\ORACLE", Registry.LocalMachine);
        }
    }
    
}

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestSolution.Instruments;
using Bookham.TestLibrary.Instruments;
using System.IO;

namespace Bookham.TestSolution.IlmzCommonUtils
{
    public static class IlmzOpticalSwitchLines
    {
       public static int Etalon_Tx_DigiIoLine
       {
           get
           {
               return etalon_tx_line;
           }
           set
           {
               etalon_tx_line = value;
           }
       }
        public static int Etalon_Rx_DigiIoLine
        {
            get
            {
                return etalon_rx_line;
            }
            set
            {
                etalon_rx_line = value;
            }
        }
        public static int Dut_Tx_DigiIoLine
        {
            get
            {
                return dut_tx_line;
            }
            set
            {
                dut_tx_line = value;
            }
        }
        public static int Dut_Rx_DigiIoLine
        {
            get
            {
                return dut_rx_line;
            }
            set
            {
                dut_rx_line = value;
            }
        }
        public static int Etalon_Ref_DigiIoLine
        {
            get
            {
                return etalon_ref_line;
            }
            set
            {
                etalon_ref_line = value;
            }
        }
        public static int Dut_Ctap_DigiIoLine
        {
            get
            {
                return dut_ctap_line;
            }
            set
            {
                dut_ctap_line = value;
            }
        }
        public static int C_Band_DigiIoLine
        {
            get
            {
                return c_band_filter;
            }
            set
            {
                c_band_filter = value;
            }
        }
        public static int L_Band_DigiIoLine
        {
            get
            {
                return l_band_filter;
            }
            set
            {
                l_band_filter = value;
            }
        }



        public static bool Etalon_Tx_DigiIoLine_State
        {
            get
            {
                return etalon_tx_line_state;
            }
            set
            {
                etalon_tx_line_state = value;
            }
        }
        public static bool Etalon_Rx_DigiIoLine_State
        {
            get
            {
                return etalon_rx_line_state;
            }
            set
            {
                etalon_rx_line_state = value;
            }
        }
        public static bool Dut_Tx_DigiIoLine_State
        {
            get
            {
                return dut_tx_line_state;
            }
            set
            {
                dut_tx_line_state = value;
            }
        }
        public static bool Dut_Rx_DigiIoLine_State
        {
            get
            {
                return dut_rx_line_state;
            }
            set
            {
                dut_rx_line_state = value;
            }
        }
        public static bool Etalon_Ref_DigiIoLine_State
        {
            get
            {
                return etalon_ref_line_state;
            }
            set
            {
                etalon_ref_line_state = value;
            }
        }
        public static bool Dut_Ctap_DigiIoLine_State
        {
            get
            {
                return dut_ctap_line_state;
            }
            set
            {
                dut_ctap_line_state = value;
            }
        }
        public static bool C_Band_DigiIoLine_State
        {
            get
            {
                return c_band_filter_state;
            }
            set
            {
                c_band_filter_state = value;
            }
        }
        public static bool L_Band_DigiIoLine_State
        {
            get
            {
                return l_band_filter_state;
            }
            set
            {
                l_band_filter_state = value;
            }
        }

       static int etalon_tx_line;
       static int etalon_rx_line;
       static int dut_tx_line;
       static int dut_rx_line;
        static int etalon_ref_line;
        static int dut_ctap_line;
        static int c_band_filter;
        static int l_band_filter;

        static bool etalon_tx_line_state;
        static bool etalon_rx_line_state;
        static bool dut_tx_line_state;
        static bool dut_rx_line_state;
        static bool etalon_ref_line_state;
        static bool dut_ctap_line_state;
        static bool c_band_filter_state;
        static bool l_band_filter_state;
    }
}

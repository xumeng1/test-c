using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using Bookham.TestLibrary.Algorithms;
using Bookham.TestLibrary.Utilities;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestSolution.IlmzCommonData;

namespace Bookham.TestSolution.IlmzCommonUtils
{
    /// <summary>
    /// Class to perform trace tone test
    /// </summary>
    public class TraceToneTest
    {
        /// <summary>
        /// Private constructor
        /// </summary>
        private TraceToneTest()
        {
        }

        #region Public functions
        /// <summary>
        /// Setup the collections of TraceSoaSweep data classes
        /// </summary>
        /// <param name="orderOfFit"></param>
        /// <param name="fibreTargetPwr_dBm"></param>
        /// <param name="ageAmount1_dB"></param>
        /// <param name="ageAmout2_dB"></param>
        /// <param name="modulationIndex"></param>
        /// <param name="startSoaCurrent_mA"></param>
        /// <param name="endSoaCurrent_mA"></param>
        /// <param name="stepSoaCurrent_mA"></param>
        /// <param name="stepDelay_mS"></param>
        public static void Initialise(int orderOfFit, double fibreTargetPwr_dBm, double ageAmount1_dB, double ageAmout2_dB, double modulationIndex,
            double startSoaCurrent_mA, double endSoaCurrent_mA, double stepSoaCurrent_mA, int stepDelay_mS)
        {
            TraceToneTest.fitOrder = orderOfFit;

            TraceToneTest.traceSoaSweepData = null;

            TraceToneTest.fibreTargetPower_dBm = fibreTargetPwr_dBm;
            TraceToneTest.age_Cond_1_dB = ageAmount1_dB;
            TraceToneTest.age_Cond_2_dB = ageAmout2_dB;
            TraceToneTest.modulationIndex = modulationIndex;
            // calculate trace tone power
            TraceToneTest.traceTonePower_dB = 10 * Math.Log10(1 + modulationIndex);

            TraceToneTest.soaSweepStartCurr_mA = startSoaCurrent_mA;
            TraceToneTest.soaSweepEndCurr_mA = endSoaCurrent_mA;
            TraceToneTest.soaSweepStepCurr_mA = stepSoaCurrent_mA;
            TraceToneTest.soaSweepStepDelay_mS = stepDelay_mS;

            TraceToneTest.tcmzTestTemp = TcmzTestTemp.Mid;
        }

        /// <summary>
        /// Perform Soa sweep and measure power
        /// </summary>
        /// <param name="testTemp"></param>
        public static void PerformTraceSoaSweep(TcmzTestTemp testTemp)
        {
            TraceToneTest.tcmzTestTemp = testTemp;

            DatumList sweepData = SoaSweep.TraceSoaSweep(soaSweepStartCurr_mA, 
                soaSweepEndCurr_mA, soaSweepStepCurr_mA, soaSweepStepDelay_mS);
            double[] soaCurrent_mA = sweepData.ReadDoubleArray("SoaCurrentArray_mA");
            double[] power_dBm = sweepData.ReadDoubleArray("MeasuredPowerArray_dBm");
            double[] soaDacs = null;
            if (DsdbrUtils.DsdbrDriverInstrsToUse == DsdbrDriveInstruments.FCU2AsicInstrument)
                soaDacs = sweepData.ReadDoubleArray("SoaCurrentArray_Dac");

            if (TraceToneTest.traceSoaSweepData == null)
            {
                TraceToneTest.SetupTraceSoaSweepDataContainer();

                if (DsdbrUtils.DsdbrDriverInstrsToUse == DsdbrDriveInstruments.FCU2AsicInstrument)
                    TraceToneTest.AddTraceData(soaCurrent_mA, soaDacs, power_dBm);
                else 
                    TraceToneTest.AddTraceData(soaCurrent_mA, power_dBm);
            }
            else
            {
                double[] oldPowerData_dBm = TraceToneTest.traceSoaSweepData[TraceDataType.SOLOT].rawPowerData_dBm;
                if (power_dBm[power_dBm.Length - 1] < oldPowerData_dBm[oldPowerData_dBm.Length - 1])
                {
                    // Use new data
                    TraceToneTest.SetupTraceSoaSweepDataContainer();
                    if (DsdbrUtils.DsdbrDriverInstrsToUse == DsdbrDriveInstruments.FCU2AsicInstrument)
                        TraceToneTest.AddTraceData(soaCurrent_mA, soaDacs, power_dBm);
                    else
                        TraceToneTest.AddTraceData(soaCurrent_mA, power_dBm);
                }
                else
                {
                    // Use old data.
                }
            }
        }

        /// <summary>
        /// Pefrom trace tone test and analysis
        /// </summary>
        /// <param name="plotFileName"></param>
        /// <returns></returns>
        public static DatumList PerformTraceToneTest(string plotFileName)
        {
            // Return data
            DatumList traceToneResults = new DatumList();

            // SOLOT
            double I_Soa_1 = TraceToneTest.GetFittedSoaByPowerIndBm_mA(TraceDataType.SOLOT, TraceToneTest.fibreTargetPower_dBm);
            double Max_I_Soa_Trace_SOLOT = TraceToneTest.GetFittedSoaByPowerIndBm_mA(TraceDataType.SOLOT, TraceToneTest.fibreTargetPower_dBm + TraceToneTest.traceTonePower_dB);
            double Soa_Gradient_SOLOT = TraceToneTest.GetFittedSoaGradient(TraceDataType.SOLOT, I_Soa_1);
            double Max_Trace_I_SOLOT = Max_I_Soa_Trace_SOLOT - I_Soa_1;

            // EOLOT_1
            double I_Soa_EOL_1 = TraceToneTest.GetFittedSoaByPowerIndBm_mA(TraceDataType.EOLOT_1, TraceToneTest.fibreTargetPower_dBm);
            double I_Soa_Trace_EOLOT1 = TraceToneTest.GetFittedSoaByPowerIndBm_mA(TraceDataType.EOLOT_1, TraceToneTest.fibreTargetPower_dBm + TraceToneTest.traceTonePower_dB);
            double Soa_Gradient_EOLOT1 = TraceToneTest.GetFittedSoaGradient(TraceDataType.EOLOT_1, I_Soa_EOL_1);
            double Trace_I_EOLOT1 = I_Soa_Trace_EOLOT1 - I_Soa_EOL_1;

            // EOLOT_2
            double I_Soa_EOL_2 = TraceToneTest.GetFittedSoaByPowerIndBm_mA(TraceDataType.EOLOT_2, TraceToneTest.fibreTargetPower_dBm);
            double I_Soa_Trace_EOLOT2 = TraceToneTest.GetFittedSoaByPowerIndBm_mA(TraceDataType.EOLOT_2, TraceToneTest.fibreTargetPower_dBm + TraceToneTest.traceTonePower_dB);
            double Soa_Gradient_EOLOT2 = TraceToneTest.GetFittedSoaGradient(TraceDataType.EOLOT_2, I_Soa_EOL_2);
            double Trace_I_EOLOT2 = I_Soa_Trace_EOLOT2 - I_Soa_EOL_2;

            // Trace Soa curves file
            TraceToneTest.WriteTraceSoaCurveFile(plotFileName);

            traceToneResults.AddDouble("I_Soa_1", I_Soa_1);
            traceToneResults.AddDouble("Max_I_Soa_Trace_SOLOT", Max_I_Soa_Trace_SOLOT);
            traceToneResults.AddDouble("Soa_Gradient_SOLOT", Soa_Gradient_SOLOT);
            traceToneResults.AddDouble("Max_Trace_I_SOLOT", Max_Trace_I_SOLOT);
            //traceToneResults.AddDouble("I_Soa_EOL_1", I_Soa_EOL_1);
            traceToneResults.AddDouble("I_Soa_Trace_EOLOT1", I_Soa_Trace_EOLOT1);
            traceToneResults.AddDouble("Soa_Gradient_EOLOT1", Soa_Gradient_EOLOT1);
            traceToneResults.AddDouble("Trace_I_EOLOT1", Trace_I_EOLOT1);
            //traceToneResults.AddDouble("I_Soa_EOL_2", I_Soa_EOL_2);
            traceToneResults.AddDouble("I_Soa_Trace_EOLOT2", I_Soa_Trace_EOLOT2);
            traceToneResults.AddDouble("Soa_Gradient_EOLOT2", Soa_Gradient_EOLOT2);
            traceToneResults.AddDouble("Trace_I_EOLOT2", Trace_I_EOLOT2);

            if (DsdbrUtils .DsdbrDriverInstrsToUse == DsdbrDriveInstruments .FCU2AsicInstrument)
            {
                traceToneResults.AddSint32("I_SoaDac_1", DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic.mAToDacForFrontSoa_Asic((float)I_Soa_1));
                traceToneResults.AddSint32("Max_I_SoaDac_Trace_SOLOT", DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic.mAToDacForFrontSoa_Asic((float)Max_I_Soa_Trace_SOLOT));

                //traceToneResults.AddSint32("I_SoaDac_EOL_1", I_Soa_EOL_1);
                traceToneResults.AddSint32("I_SoaDac_Trace_EOLOT1", DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic.mAToDacForFrontSoa_Asic((float)I_Soa_Trace_EOLOT1));

                //traceToneResults.AddSint32("I_Soa_EOL_2", I_Soa_EOL_2);
                traceToneResults.AddSint32("I_SoaDac_Trace_EOLOT2", DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic.mAToDacForFrontSoa_Asic((float)I_Soa_Trace_EOLOT2));
                
            }
            return traceToneResults;
        }

        #endregion


        #region Private functions
        /// <summary>
        /// Setup trace Soa sweep data container
        /// </summary>
        private static void SetupTraceSoaSweepDataContainer()
        {
            TraceToneTest.traceSoaSweepData = new Dictionary<TraceDataType, TraceSoaSweep>();
            TraceToneTest.traceSoaSweepData.Add(TraceDataType.SOLOT, new TraceSoaSweep(TraceToneTest.fitOrder));
            TraceToneTest.traceSoaSweepData.Add(TraceDataType.EOLOT_1, new TraceSoaSweep(TraceToneTest.fitOrder));
            TraceToneTest.traceSoaSweepData.Add(TraceDataType.EOLOT_2, new TraceSoaSweep(TraceToneTest.fitOrder));
        }

        /// <summary>
        /// Add all trace data points
        /// </summary>
        /// <param name="soaCurrent_mA"></param>
        /// <param name="power_dBm"></param>
        private static void AddTraceData(double[] soaCurrent_mA, double[] power_dBm)
        {
            if (soaCurrent_mA.Length != power_dBm.Length)
            {
                throw new AlgorithmException("Mismatched SOA current and fibre power arrays");
            }
            if (soaCurrent_mA.Length == 0)
            {
                throw new AlgorithmException("Empty SOA current and fibre power arrays");
            }

            for (int ii = 0; ii < soaCurrent_mA.Length; ii++)
            {
                TraceToneTest.AddTracePoint(soaCurrent_mA[ii], power_dBm[ii]);
            }
        }

        
        /// <summary>
        /// Add a trace data point
        /// </summary>
        /// <param name="soaCurrent_mA"></param>
        /// <param name="power_dBm"></param>
        private static void AddTracePoint(double soaCurrent_mA, double power_dBm)
        {
            TraceToneTest.traceSoaSweepData[TraceDataType.SOLOT].AddPoint(soaCurrent_mA, power_dBm);
            TraceToneTest.traceSoaSweepData[TraceDataType.EOLOT_1].AddPoint(soaCurrent_mA, power_dBm - Math.Abs(TraceToneTest.age_Cond_1_dB));
            TraceToneTest.traceSoaSweepData[TraceDataType.EOLOT_2].AddPoint(soaCurrent_mA, power_dBm - Math.Abs(TraceToneTest.age_Cond_2_dB));
        }


        // Alice.Huang  2010-02-20
        // Override these tow function to add SOA DAC trace for Tosa

        /// <summary>
        /// Add all trace data points
        /// </summary>
        /// <param name="soaCurrent_mA"></param>
        /// <param name="soaDacs"> </param>
        /// <param name="power_dBm"></param>
        private static void AddTraceData(double[] soaCurrent_mA,double[] soaDacs, double[] power_dBm)
        {
            if ((soaCurrent_mA.Length != power_dBm.Length) ||
                (soaCurrent_mA.Length != soaDacs.Length))
            {
                throw new AlgorithmException("Mismatched SOA current and fibre power array or Soa Dac's array");
            }
            if (soaCurrent_mA.Length == 0)
            {
                throw new AlgorithmException("Empty SOA current and fibre power array");
            }

            for (int ii = 0; ii < soaCurrent_mA.Length; ii++)
            {
                TraceToneTest.AddTracePoint(soaCurrent_mA[ii], soaDacs[ii], power_dBm[ii]);
            }
        }


        /// <summary>
        /// Add a trace data point
        /// </summary>
        /// <param name="soaCurrent_mA"> soa current in mA</param>
        /// <param name="soaDac"> soa current in Dac </param>
        /// <param name="power_dBm">power reading in dBm </param>
        private static void AddTracePoint(double soaCurrent_mA, double soaDac, double power_dBm)
        {
            TraceToneTest.traceSoaSweepData[TraceDataType.SOLOT].AddPoint(soaCurrent_mA, soaDac, power_dBm);
            TraceToneTest.traceSoaSweepData[TraceDataType.EOLOT_1].AddPoint(soaCurrent_mA, soaDac, power_dBm - Math.Abs(TraceToneTest.age_Cond_1_dB));
            TraceToneTest.traceSoaSweepData[TraceDataType.EOLOT_2].AddPoint(soaCurrent_mA, soaDac, power_dBm - Math.Abs(TraceToneTest.age_Cond_2_dB));
        }

        /// <summary>
        /// Write SOA measured and aged curves data into csv file
        /// </summary>
        /// <param name="filename"></param>
        private static void WriteTraceSoaCurveFile(string filename)
        {
            List<string> fileLines = new List<string>();

            // Trace SOA curves part
            StringBuilder header = new StringBuilder();
            header.Append("SOACurrent_mA");
            if (DsdbrUtils.DsdbrDriverInstrsToUse == DsdbrDriveInstruments.FCU2AsicInstrument)
                header.Append(",SoaCurrent_Dac");

            double[] soaCurrent_mA = null;
            double[] powerSOLOT_dBm = null;
            double[] powerEOLOT1_dBm = null;
            double[] powerEOLOT2_dBm = null;

            // Alice.Huang   2010-02-20  
            // Add SOA Dac Trce for TOSA

            double[] soaCurrent_Dac = null;

            foreach (string nameAsStr in Enum.GetNames(typeof(TraceDataType)))
            {
                header.Append(",Power_" + nameAsStr + "_dBm");
                header.Append(",Power_" + nameAsStr + "_mW");

                TraceDataType parameter = (TraceDataType)Enum.Parse(typeof(TraceDataType), nameAsStr);
                if (parameter == TraceDataType.SOLOT)
                {
                    soaCurrent_mA = TraceToneTest.traceSoaSweepData[parameter].rawSoaCurrentData_mA;
                    powerSOLOT_dBm = TraceToneTest.traceSoaSweepData[parameter].rawPowerData_dBm;

                    if (DsdbrUtils.DsdbrDriverInstrsToUse == DsdbrDriveInstruments.FCU2AsicInstrument)
                        soaCurrent_Dac = TraceToneTest.traceSoaSweepData[parameter].rawSoaCurrentData_Dac;
                }
                else if (parameter == TraceDataType.EOLOT_1)
                {
                    powerEOLOT1_dBm = TraceToneTest.traceSoaSweepData[parameter].rawPowerData_dBm;
                }
                else
                {
                    powerEOLOT2_dBm = TraceToneTest.traceSoaSweepData[parameter].rawPowerData_dBm;
                }
            } 

            fileLines.Add(header.ToString());
            for (int ii = 0; ii < soaCurrent_mA.Length; ii++)
            {
                StringBuilder aLine = new StringBuilder();
                aLine.Append(soaCurrent_mA[ii].ToString());
                aLine.Append(",");
                if (DsdbrUtils.DsdbrDriverInstrsToUse == DsdbrDriveInstruments.FCU2AsicInstrument)
                {
                    aLine.Append(String.Format("#", soaCurrent_Dac[ii]));
                    aLine.Append(",");
                }
                aLine.Append(powerSOLOT_dBm[ii].ToString());
                aLine.Append(",");
                aLine.Append(Math.Pow(10, powerSOLOT_dBm[ii] / 10).ToString());
                aLine.Append(",");
                aLine.Append(powerEOLOT1_dBm[ii].ToString());
                aLine.Append(",");
                aLine.Append(Math.Pow(10, powerEOLOT1_dBm[ii] / 10).ToString());
                aLine.Append(",");
                aLine.Append(powerEOLOT2_dBm[ii].ToString());
                aLine.Append(",");
                aLine.Append(Math.Pow(10, powerEOLOT2_dBm[ii] / 10).ToString());

                fileLines.Add(aLine.ToString());
            }

            // Polynomial coefficients part
            fileLines.Add("");
            fileLines.Add(
                string.Format("{0} order polynomial fit is used and it's based on data from {1} temperature test",
                TraceToneTest.fitOrder,
                Enum.GetName(typeof(TcmzTestTemp), TraceToneTest.tcmzTestTemp)));
            fileLines.Add("Coeff_* below means the coefficient of item x^*");
            // table header
            string tableHeader = "TestConds";
            for (int ii = 0; ii <= TraceToneTest.fitOrder; ii++)
            {
                tableHeader += ",Coeff_" + ii;
            }
            fileLines.Add(tableHeader);
            // table data
            foreach (string nameAsStr in Enum.GetNames(typeof(TraceDataType)))
            {
                StringBuilder aLine = new StringBuilder();
                aLine.Append(nameAsStr);

                TraceDataType parameter = (TraceDataType)Enum.Parse(typeof(TraceDataType), nameAsStr);
                for (int ii = 0; ii <= TraceToneTest.fitOrder; ii++)
                {
                    aLine.Append(",");
                    aLine.Append(TraceToneTest.traceSoaSweepData[parameter].Coeffs_mWVsmA[ii]);
                }

                fileLines.Add(aLine.ToString());
            }

            // Write .csv file
            using (CsvWriter writer = new CsvWriter())
            {
                writer.WriteFile(filename, fileLines);
            }
        }

        /// <summary>
        /// Get fitted power in dBm at a given SOA current in mA
        /// </summary>
        /// <param name="traceDataType"></param>
        /// <param name="soaCurrent_mA"></param>
        /// <returns></returns>
        private static double GetFittedPower_dBm(TraceDataType traceDataType, double soaCurrent_mA)
        {
            double fittedPower_dBm = TraceToneTest.traceSoaSweepData[traceDataType].GetPower_dBm(soaCurrent_mA);
            return fittedPower_dBm;
        }

        /// <summary>
        /// Get fitted power in mW at a given SOA current in mA
        /// </summary>
        /// <param name="traceDataType"></param>
        /// <param name="soaCurrent_mA"></param>
        /// <returns></returns>
        private static double GetFittedPower_mW(TraceDataType traceDataType, double soaCurrent_mA)
        {
            double fittedPower_mW = TraceToneTest.traceSoaSweepData[traceDataType].GetPower_mW(soaCurrent_mA);
            return fittedPower_mW;
        }

        /// <summary>
        /// Get fitted SOA current in mA at a given power in mW
        /// </summary>
        /// <param name="traceDataType"></param>
        /// <param name="power_mW"></param>
        /// <returns></returns>
        private static double GetFittedSoaByPowerInmW_mA(TraceDataType traceDataType, double power_mW)
        {
            double fittedSoa_mA = TraceToneTest.traceSoaSweepData[traceDataType].GetSOACurrentByPowerInmW_mA(power_mW);
            return fittedSoa_mA;
        }

        /// <summary>
        /// Get fitted SOA current in mA at a given power in dBm
        /// </summary>
        /// <param name="traceDataType"></param>
        /// <param name="power_dBm"></param>
        /// <returns></returns>
        private static double GetFittedSoaByPowerIndBm_mA(TraceDataType traceDataType, double power_dBm)
        {
            double fittedSoa_mA = TraceToneTest.traceSoaSweepData[traceDataType].GetSOACurrentByPowerIndBm_mA(power_dBm);
            return fittedSoa_mA;
        }

        /// <summary>
        /// Get SOA slope in mW/mA at a given SOA current in mA
        /// </summary>
        /// <param name="traceDataType"></param>
        /// <param name="soaCurrent_mA"></param>
        /// <returns></returns>
        private static double GetFittedSoaGradient(TraceDataType traceDataType, double soaCurrent_mA)
        {
            double fittedSoaGradient = TraceToneTest.traceSoaSweepData[traceDataType].GetSOAGradient(soaCurrent_mA);
            return fittedSoaGradient;
        }

        
        #endregion

        #region Private data
        private static Dictionary<TraceDataType, TraceSoaSweep> traceSoaSweepData;
        private static int fitOrder;
        private static double fibreTargetPower_dBm;
        private static double age_Cond_1_dB;
        private static double age_Cond_2_dB;
        private static double modulationIndex;
        private static double traceTonePower_dB;

        private static double soaSweepStartCurr_mA;
        private static double soaSweepEndCurr_mA;
        private static double soaSweepStepCurr_mA;
        private static int soaSweepStepDelay_mS;

        private static TcmzTestTemp tcmzTestTemp;
        #endregion
    }


    /// <summary>
    /// Class containing the sweep data of fibre power VS. SOA current
    /// </summary>
    public class TraceSoaSweep
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="fitOrder">The order of fit to apply to the data</param>
        public TraceSoaSweep(int fitOrder)
        {
            this.fitOrder = fitOrder;
            this.soaCurrent_mA = new List<double>();
            this.power_dBm = new List<double>();
            this.power_mW = new List<double>();
            this.fitComplete = false;

            // Alice.Huang   2010-02-18
            // for FCU2ASIC soa dac trace
            this.soaCurrent_Dac = new List<double>();

        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="fitOrder">The order of fit to apply to the dat</param>
        /// <param name="soaCurrentData_mA">An array of SOA current in mA</param>
        /// <param name="fibrePowerData_dBm">An array of fibre power in dBm</param>
        public TraceSoaSweep(int fitOrder, double[] soaCurrentData_mA, double[] fibrePowerData_dBm)
        {
            if (soaCurrentData_mA.Length != fibrePowerData_dBm.Length)
            {
                throw new AlgorithmException("Mismatched SOA current and fibre power arrays");
            }
            if (soaCurrentData_mA.Length == 0)
            {
                throw new AlgorithmException("Empty SOA current and fibre power arrays");
            }

            this.fitOrder = fitOrder;
            this.soaCurrent_mA = new List<double>();
            this.power_dBm = new List<double>();
            this.power_mW = new List<double>();
            this.fitComplete = false;

            foreach (double soaCurrent in soaCurrentData_mA)
            {
                this.soaCurrent_mA.Add(soaCurrent);
            }
            foreach (double fibrePower in fibrePowerData_dBm)
            {
                this.power_dBm.Add(fibrePower);
                this.power_mW.Add(Alg_PowConvert_dB.Convert_dBmtomW(fibrePower));
            }

            if (this.soaCurrent_mA.Count > fitOrder)
            {
                this.fitToData();
            }
            
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="fitOrder">The order of fit to apply to the dat</param>
        /// <param name="soaCurrentData_mA">An array of SOA current in mA</param>
        /// <param name="fibrePowerData_dBm">An array of fibre power in dBm</param>
        /// <param name="soaCurrentData_Dac"  An array of SOA current Dac data</param>
        public TraceSoaSweep(int fitOrder, double[] soaCurrentData_mA, 
                                double[] fibrePowerData_dBm,double[] soaCurrentData_Dac)
        {
            if (soaCurrentData_mA.Length != fibrePowerData_dBm.Length)
            {
                throw new AlgorithmException("Mismatched SOA current and fibre power arrays");
            }
            if (soaCurrentData_mA.Length == 0)
            {
                throw new AlgorithmException("Empty SOA current and fibre power arrays");
            }
            if (soaCurrentData_Dac.Length != fibrePowerData_dBm.Length)
            {
                throw new AlgorithmException("Mismatched SOA current Dac and fibre power arrays");
            }
            this.fitOrder = fitOrder;
            this.soaCurrent_mA = new List<double>();
            this.power_dBm = new List<double>();
            this.power_mW = new List<double>();
            this.fitComplete = false;
            this.soaCurrent_Dac = new List<double>();
            foreach (double soaCurrent in soaCurrentData_mA)
            {
                this.soaCurrent_mA.Add(soaCurrent);
            }
            foreach (double fibrePower in fibrePowerData_dBm)
            {
                this.power_dBm.Add(fibrePower);
                this.power_mW.Add(Alg_PowConvert_dB.Convert_dBmtomW(fibrePower));
            }
            
            foreach (double soaDac in soaCurrent_Dac)
            {
                this.soaCurrent_Dac.Add(soaDac);
            }
            
            if (this.soaCurrent_mA.Count > fitOrder)
            {
                this.fitToData();
            }
            
        }
        /// <summary>
        /// Add a single SOA current in mA and fibre power in dBm pair
        /// </summary>
        /// <param name="soaCurrent_mA"></param>
        /// <param name="fibrePower_dBm"></param>
        public void AddPoint(double soaCurrent_mA, double fibrePower_dBm)
        {
            this.soaCurrent_mA.Add(soaCurrent_mA);
            this.power_dBm.Add(fibrePower_dBm);
            this.power_mW.Add(Alg_PowConvert_dB.Convert_dBmtomW(fibrePower_dBm));
            
            if (this.soaCurrent_mA.Count > this.fitOrder)
            {
                this.fitToData();
            }
        }

        /// <summary>
        /// Add a single SOA current in mA and fibre power in dBm pair
        /// </summary>
        /// <param name="soaCurrent_mA"> soa current in mA</param>
        /// <param name="fibrePower_dBm"> fibre power reading in dBm</param>
        /// <param name="soaDac"> Soa current in Dac </param>
        public void AddPoint(double soaCurrent_mA, double soaDac, double fibrePower_dBm)
        {
            this.soaCurrent_mA.Add(soaCurrent_mA);
            this.power_dBm.Add(fibrePower_dBm);
            this.power_mW.Add(Alg_PowConvert_dB.Convert_dBmtomW(fibrePower_dBm));
            this.soaCurrent_Dac.Add(soaDac);

            if (this.soaCurrent_mA.Count > this.fitOrder)
            {
                this.fitToData();
            }
        }

        /// <summary>
        /// Calculate power in mW at a given SOA current in mA
        /// Check FitComplete before using this function.
        /// </summary>
        /// <param name="soaCurrent_mA"></param>
        /// <returns></returns>
        public double GetPower_mW(double soaCurrent_mA)
        {
            if (coeffs_mWVsmA == null)
            {
                throw new AlgorithmException("Insufficient data to perform fit");
            }

            // Apply coefficients to data point.
            double power_mW = 0;
            for (int powerOfX = 0; powerOfX <= fitOrder; powerOfX++)
            {
                power_mW += coeffs_mWVsmA[powerOfX] * Math.Pow(soaCurrent_mA, powerOfX);
            }

            return power_mW;
        }

        /// <summary>
        /// Calculate power in dBm at a given SOA current in mA. Check FitComplete before using this function.
        /// </summary>
        /// <param name="soaCurrent_mA"></param>
        /// <returns></returns>
        public double GetPower_dBm(double soaCurrent_mA)
        {
            double power_mW = this.GetPower_mW(soaCurrent_mA);
            return Alg_PowConvert_dB.Convert_mWtodBm(power_mW);
        }

        /// <summary>
        /// Locate SOA current in mA for a given power in dBm
        /// </summary>
        /// <param name="power_dBm"></param>
        /// <returns></returns>
        public double GetSOACurrentByPowerIndBm_mA(double power_dBm)
        {
            double power_mW = Alg_PowConvert_dB.Convert_dBmtomW(power_dBm);
            return this.GetSOACurrentByPowerInmW_mA(power_mW);
        }

        /// <summary>
        /// Locate SOA current in mA for a given power in mW
        /// </summary>
        /// <param name="power_mW"></param>
        /// <returns></returns>
        public double GetSOACurrentByPowerInmW_mA(double power_mW)
        {
            if (coeffs_mWVsmA == null)
            {
                throw new AlgorithmException("Insufficient data to perform fit");
            }

            double[] soaData_mA = this.soaCurrent_mA.ToArray();
            double[] powerData_mW = this.power_mW.ToArray();

            PolyFit polyFit = Alg_PolyFit.PolynomialFit(powerData_mW, soaData_mA, this.fitOrder);
            double[] coeffs_mAVsmW = polyFit.Coeffs;

            // Apply coefficients to data point.
            double soaCurr_mA = 0;
            for (int powerOfX = 0; powerOfX <= fitOrder; powerOfX++)
            {
                soaCurr_mA += coeffs_mAVsmW[powerOfX] * Math.Pow(power_mW, powerOfX);
            }

            return soaCurr_mA;
        }

        /// <summary>
        /// Get SOA slope in mW/mA at a given SOA current in mA
        /// </summary>
        /// <param name="soaCurrent_mA"></param>
        /// <returns></returns>
        public double GetSOAGradient(double soaCurrent_mA)
        {
            if (coeffs_mWVsmA == null)
            {
                throw new AlgorithmException("Insufficient data to perform fit");
            }

            // Apply coefficients to data point.
            double soaGradient = 0;
            for (int powerOfX = 1; powerOfX <= fitOrder; powerOfX++)
            {
                soaGradient += powerOfX * coeffs_mWVsmA[powerOfX] * Math.Pow(soaCurrent_mA, powerOfX - 1);
            }

            return soaGradient;
        }

        /// <summary>
        /// True if poly fit finished
        /// </summary>
        public bool FitComplete
        {
            get
            {
                return fitComplete;
            }
        }

        /// <summary>
        /// Poly coeffs after poly fit. Check FitComplete before using it.
        /// </summary>
        public double[] Coeffs_mWVsmA
        {
            get
            {
                return coeffs_mWVsmA;
            }
        }

        /// <summary>
        /// Raw SOA current in mA
        /// </summary>
        public double[] rawSoaCurrentData_mA
        {
            get
            {
                return this.soaCurrent_mA.ToArray();
            }
        }

        /// <summary>
        /// Raw power in dBm
        /// </summary>
        public double[] rawPowerData_dBm
        {
            get
            {
                return this.power_dBm.ToArray();
            }
        }

        /// <summary>
        /// Perform polynomial fit
        /// </summary>
        private void fitToData()
        {
            double[] soa_mA = this.soaCurrent_mA.ToArray();
            double[] power_mW = this.power_mW.ToArray();

            PolyFit polyFit = Alg_PolyFit.PolynomialFit(soa_mA, power_mW, this.fitOrder);
            this.coeffs_mWVsmA = polyFit.Coeffs;

            this.fitComplete = true;
        }


        // Private data
        private int fitOrder;
        private double[] coeffs_mWVsmA;
        private List<double> soaCurrent_mA;
        private List<double> power_dBm;
        private List<double> power_mW;
        private bool fitComplete;

        // Alice.Huang   2010-02-18
        private List<double> soaCurrent_Dac;
        /// <summary>
        /// Get Soa current dac array
        /// </summary>
        public double[] rawSoaCurrentData_Dac
        {
            get
            {
                return this.soaCurrent_Dac.ToArray();
            }
        }
    }

    /// <summary>
    /// Specify which trace data to access
    /// </summary>
    public enum TraceDataType
    {
        /// <summary>Start of life over temperature</summary>
        SOLOT,
        /// <summary>End of life1 over temperature</summary>
        EOLOT_1,
        /// <summary>End of life2 over temperature</summary>
        EOLOT_2
    }
}

/// alice.H 
///add this class for OSA share function to lock instrument 
///throught lock a common file in remote server
/// when locking the common file is found in idle state, change its name to the name that indicate 
/// the shared instrument is under used by this test kit.
///when unlocking the file, check if the file is in the name that that indicate 
/// the shared instrument is under used by this test kit or not
/// if it is, changes it to indle state's name,so that other test kit can found it in idle state
///
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
using System.Windows.Forms;
using Bookham.TestEngine.Equipment ;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestEngine.PluginInterfaces.Instrument;

namespace Bookham.TestSolution.IlmzCommonUtils
{
    /// <summary>
    /// for OSA share in TCMZ Final test
    /// this class include 4 part:
    /// 1. configure setting of tcmz share function: setting includes: dio shource, 
    ///     dio line used to control the switch,dio line in measurement state 
    ///     and dio recover state after measurement finished
    /// 2. lock intrument to be used by this test set only
    /// 3. unlock instrument to allow used by other test set
    /// 4. set dio line to change osa optical path to this test set for measurement
    /// 5. set dio line output to recover state if necessary.
    /// </summary>
    public static class TcmzOSAshare
    {
        #region private data
        static string configXmlFilePath;
        static bool osaShareFlag;       
        static string dioSourceName;
        static int dioLineNumber;        
        static bool dioStateInMeasure;
        static bool dioRecoverFlag;        
        static bool dioStateRecover;
        static string LockInstrumentFilePath;
        static string instrumentInUsedFileName;        
        static string instrumantIdleFileName;
        static double instrumentLockTimeOut;

        static IInstType_DigitalIO osaShareDIO=null ;
       
        #endregion
                
        
        /// <summary>
        /// 
        /// </summary>
        public static string InstrumentInUsedFileName
        {
            get { return TcmzOSAshare.instrumentInUsedFileName; }
            set { TcmzOSAshare.instrumentInUsedFileName = value; }
        }
        
        /// <summary>
        /// 
        /// </summary>
        public static bool OsaShareFlag
        {
            get { return TcmzOSAshare.osaShareFlag; }
            //set { TcmzOSAshare.osaShareFlag = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public static bool DioRecoverFlag
        {
            get { return TcmzOSAshare.dioRecoverFlag; }
            //set { TcmzOSAshare.dioRecoverFlag = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="instrs"></param>
        /// <param name="configXmlPath"></param>
        /// <param name="xmlSectionPath"></param>
        /// <returns></returns>
        public static bool   GetOSAShareConfig(InstrumentCollection instrs, 
            string configXmlPath,string xmlSectionPath)
        {
            bool flagSetupDIO = false;

            if (File.Exists(configXmlPath ))
            {
                XmlDocument instrumentFile = new XmlDocument();                
                #region Read osa share setting from xml file

                try
                {                    
                    instrumentFile.Load(configXmlPath);
                    configXmlFilePath = configXmlPath;

                    XmlNode tempNode;
                    string strSetting;

                    tempNode = instrumentFile.SelectSingleNode(xmlSectionPath + "/OSAShareOption");
                    if (tempNode != null)
                    {
                        strSetting = tempNode.InnerText.Trim();
                        osaShareFlag = bool.Parse(strSetting);
                    }

                    if (osaShareFlag)
                    {
                        tempNode = instrumentFile.SelectSingleNode(xmlSectionPath + "/DioSourceName");
                        if (tempNode != null)
                        {
                            strSetting = tempNode.InnerText.Trim();
                            IInstType_DigiIOCollection dioCollection = (IInstType_DigiIOCollection)instrs[strSetting];
                            if (dioCollection != null)
                            {
                                dioSourceName = strSetting;
                                tempNode = instrumentFile.SelectSingleNode(xmlSectionPath + "/DioLineNumber");
                                if (tempNode != null)
                                {
                                    strSetting = tempNode.InnerText.Trim();
                                    dioLineNumber = int.Parse(strSetting);
                                    osaShareDIO = (IInstType_DigitalIO )dioCollection.GetDigiIoLine(dioLineNumber);
                                    flagSetupDIO = true;
                                }
                            }
                            else
                            {
                                MessageBox.Show(@" instrument """ + strSetting + "\" doesn't contains DIO functions" +
                                    "\n optical path can't switch,osa can't share !");
                                osaShareFlag = false;
                                osaShareDIO = null;
                            }

                        }
                    }
                    if (osaShareFlag && flagSetupDIO && (osaShareDIO != null))
                    {
                        tempNode = instrumentFile.SelectSingleNode(xmlSectionPath + "/DioStateInMeasure");
                        if (tempNode != null)
                        {
                            strSetting = tempNode.InnerText.Trim();
                            dioStateInMeasure = bool.Parse(strSetting);

                        }
                        else
                        {
                            MessageBox.Show(@" can't get available DIO State setting for OSA share" +
                                "\n optical path can't switch,osa can't share !");
                            osaShareFlag = false;
                            flagSetupDIO = false;
                            osaShareDIO = null;
                        }

                        tempNode = instrumentFile.SelectSingleNode(xmlSectionPath + "/DioRevoverOptionAfterMeasure");
                        if (tempNode != null)
                        {
                            strSetting = tempNode.InnerText.Trim();
                            dioRecoverFlag = bool.Parse(strSetting);

                        }
                        else
                        {
                            dioRecoverFlag = false;
                        }
                        if (dioRecoverFlag)
                        {
                            tempNode = instrumentFile.SelectSingleNode(xmlSectionPath + "/DioStateRevover");
                            if (tempNode != null)
                            {
                                strSetting = tempNode.InnerText.Trim();
                                dioStateRecover = bool.Parse(strSetting);

                            }
                            else
                            {
                                MessageBox.Show(" can't find DIO Setting Node @ " +
                                    xmlSectionPath + "/DioStateRevover" +
                                    " DIO recover state is set to be TRUE");
                                dioStateRecover = true;
                            }
                        }
                        tempNode = instrumentFile.SelectSingleNode(xmlSectionPath + "/LockInstrumentFilePath");
                        if (tempNode != null)
                        {
                            strSetting = tempNode.InnerText.Trim();
                            if (Directory.Exists(strSetting))
                            {
                                LockInstrumentFilePath = strSetting;
                            }
                            else
                            {
                                MessageBox.Show(@"can't find instrument lock file path @" + strSetting +
                                    "\n instrument can't be lock,osa can't share !");
                                osaShareFlag = false;
                                flagSetupDIO = false;
                                osaShareDIO = null;
                            }

                        }
                        tempNode = instrumentFile.SelectSingleNode(xmlSectionPath + "/InstrumentInUsedFileName");
                        if (tempNode != null)
                        {
                            strSetting = tempNode.InnerText.Trim();
                            instrumentInUsedFileName = strSetting;

                        }
                        else
                        {
                            instrumentInUsedFileName = "";
                        }
                        tempNode = instrumentFile.SelectSingleNode(xmlSectionPath + "/InstrumentIdleFileName");
                        if (tempNode != null)
                        {
                            strSetting = tempNode.InnerText.Trim();
                            instrumantIdleFileName = strSetting;
                        }
                        else
                        {
                            MessageBox.Show(" Can't find a file name seeting to indicate OSA available @ " +
                                xmlSectionPath + "/InstrumentIdleFileName" +
                                "\n Test can't go on!");
                            osaShareDIO = null;
                            flagSetupDIO = false;
                            osaShareFlag = false;
                        }
                        tempNode = instrumentFile.SelectSingleNode(xmlSectionPath + "/InstrumentLockTimeOut");
                        if (tempNode != null)
                        {
                            strSetting = tempNode.InnerText.Trim();
                            // in seconds
                            instrumentLockTimeOut = double.Parse(strSetting);
                        }
                        else
                        {
                            instrumentLockTimeOut = 60;
                        }

                    }
                    else
                    {
                        osaShareDIO = null;
                        flagSetupDIO = true;
                    }
                }               
                
                catch (XmlException e)
                {
                    osaShareFlag = false;
                    flagSetupDIO = false;
                    osaShareDIO = null;
                    MessageBox.Show(" Can't load osa share configuration file, osa can't be shared " +
                        "\n if you want to share the OSA, please check if the config file is in the right path" + configXmlPath + e .Message );
                }
                catch (System.Xml.XPath.XPathException e)
                {
                    MessageBox.Show("can't find some osa share setting node,so osa share is set to be not share" +
                        "\n if you want to share the OSA, please check all OSA share setting Node Name in " +
                        configXmlPath + "in section /" + xmlSectionPath + e .Message );
                    osaShareFlag = false;
                    flagSetupDIO = false;
                    osaShareDIO = null;
                }
                #endregion
            }
            else 
            {
                //MessageBox.Show("Can't find OSA share config file @ " + configXmlPath +
                //    " \ntect won't share the OSA for SMSR measurement!");//Jack.Zhang
                osaShareFlag = false;
                dioSourceName = "";
                dioLineNumber = 0;
                dioStateInMeasure = false;
                dioRecoverFlag = false;
                dioStateRecover = false;
                LockInstrumentFilePath = "";
                instrumantIdleFileName = "";
                instrumentInUsedFileName = "";
                instrumentLockTimeOut = 0.0f;
                osaShareDIO = null;
                flagSetupDIO = false;
            }
            return flagSetupDIO;
        }

        /// <summary>
        /// if the instruemnt is in idle state 
        /// and can change to a file name available for this test kit's in measurement file name, 
        /// return true; else return false
        /// </summary>
        /// <returns>  </returns>
        /// exception: DirectoryNotFoundException, InstrumentException
        public static bool LockInstruments()
        {
            bool lockflag = false;

            if (osaShareFlag)
            {
                if (Directory.Exists(LockInstrumentFilePath))
                {
                    DateTime startTime = DateTime.Now;
                    do
                    {
                        if (((TimeSpan)(DateTime.Now - startTime)).TotalSeconds  > instrumentLockTimeOut)
                        {
                            MessageBox.Show(" instrument lock time out!\n please check the other test kit status that share the common osa!");
                            throw (new InstrumentException(" can't find the instrument idle file at " +
                                LockInstrumentFilePath + instrumantIdleFileName +
                                " please chek the file state or maybe the osa is occupate by the other test kits without release it by changing th file name to idle status"));
                            //break ;
                        }
                        else
                        {
                            if (File.Exists(LockInstrumentFilePath + instrumantIdleFileName))
                            {
                                // if instrument file is in idle name, 
                                // try to change the file name available for this test kit's use
                                try
                                {
                                    File.Move(LockInstrumentFilePath + instrumantIdleFileName,
                                            LockInstrumentFilePath + instrumentInUsedFileName);
                                }
                                catch (FileNotFoundException)
                                {
                                    continue;
                                }
                                // wait for file name changed
                                System.Threading.Thread.Sleep(5);
                                // check if file name chaged successfully or not
                                // if success, intrument can be available for this test kit, end waiting
                                // or it is used by other test kit so have to wait
                                if (File.Exists(LockInstrumentFilePath + instrumentInUsedFileName))
                                {
                                    lockflag = true;
                                    break;
                                }
                            }
                        }
                    }
                    while (true);
                }
                else   // cna't find path to get osa share lock file, promote a error 
                {
                    throw (new DirectoryNotFoundException(" can't find path to get instrument lock file @ " +
                        LockInstrumentFilePath + "\n please check the net work connecting and the path"));
                }
            }
            else
            {
                lockflag = true;
            }
            return lockflag;
        }

        /// <summary>
        ///  if the file name  can be change from this test kit's in measurement state to idle state,
        /// or finally find the file name is in idle status, return true,
        /// else, return false;
        /// </summary>
        /// <returns></returns>
        /// exception: DirectoryNotFoundException
        public static  bool  UnlockInstruments()
        {
            bool unlockflag = false;
            if (osaShareFlag)
            {
                if (Directory.Exists(LockInstrumentFilePath))
                {
                    // if the intrument is used by this test kit, change the file name to idle
                    if (File.Exists(LockInstrumentFilePath + instrumentInUsedFileName))
                    {
                        File.Move(LockInstrumentFilePath + instrumentInUsedFileName,
                                    LockInstrumentFilePath + instrumantIdleFileName);

                        System.Threading.Thread.Sleep(5);

                        // if the file name is in idle, the unlock sucessfully
                        // or it is used by other test kit , can't unlock the instruments
                        if (File.Exists(LockInstrumentFilePath + instrumantIdleFileName))
                        {
                            unlockflag = true;
                        }
                    }
                        // the istrument is used by other test kit , prompt the message.
                    else
                    {
                        MessageBox.Show(" the instrument doesn't lock by this test kit" +
                            "\n so the unlock can't be handle by this test kit");
                        unlockflag = false;
                    }
                }
                else
                {
                    throw (new DirectoryNotFoundException(" can't find path to get instrument lock file @ " +
                        LockInstrumentFilePath + "\n please check the net work connecting and the path"));
                }
            }
            else
            {
                unlockflag = true;
            }

            return unlockflag;
        }
        /// <summary>
        /// set DIO line output to let osa optical path swtich to this test set
        /// </summary>
        public static  void SetDioToMeasurement()
        {

            if (osaShareDIO != null)
            {
                bool state;
                DateTime startTime = DateTime.Now;
                do
                {
                    osaShareDIO.LineState = dioStateInMeasure;
                    System.Threading.Thread.Sleep(10);
                    state = osaShareDIO.LineState;

                }
                while ((state != dioStateInMeasure) && ((TimeSpan)(DateTime.Now - startTime)).TotalSeconds < 60);

                if (state != dioStateInMeasure) throw new InstrumentException("can't swith optical path to osa " +
                    dioSourceName + " DIO out of control. please chek the instrument DIO");
            }
            
        }
        /// <summary>
        /// set DIO line output to default sate
        /// </summary>
        public static  void RecoverDIO()
        {
            if (osaShareDIO !=null ) osaShareDIO.LineState = dioStateRecover;
        }

    }
}

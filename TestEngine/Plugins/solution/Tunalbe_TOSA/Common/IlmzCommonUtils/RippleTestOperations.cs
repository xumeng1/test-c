// [Copyright]
// Bookham.TestSolution.IlmzCommonUtils
// RippleTest.cs
// Author: chongjian.liang 2013.5.6
// Design: [XS documentation]

using System;
using System.IO;
using System.Collections.Generic;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestLibrary.Utilities;
using Bookham.TestLibrary.InstrTypes;

namespace Bookham.TestSolution.IlmzCommonUtils
{
    public class RippleTestOperations
    {
        /// <summary>
        /// Add ripple test config to module
        /// </summary>
        /// <param name="cocResult"></param>
        /// <param name="moduleConfigData"></param>
        public static void LoadConfigToModule(DatumList cocResult, DatumList moduleConfigData)
        {
            // Load config from coc result only once
            if (isHasLoadConfig == false)
            {
                __LoadConfig(cocResult, false);

                isHasLoadConfig = true;
            }

            moduleConfigData.AddSint32("AfterJumpIgnoreCount", afterJumpIgnoreCount);
            moduleConfigData.AddSint32("BeforeJumpIgnoreCount", beforeJumpIgnoreCount);
            moduleConfigData.AddDouble("Multiple", multiple);
            moduleConfigData.AddString("MultipleFilterString", multipleFilterString);
            moduleConfigData.AddDouble("Ripple_MIN", ripple_Min);
            moduleConfigData.AddDouble("Ripple_MAX", ripple_Max);
        }

        /// <summary>
        /// Add ripple test config to module
        /// Add/update the Igain_mA, IfsCon_mA and IrearSoa_mA for phase scan (for ripple analysis) from COC result to module
        /// </summary>
        /// <param name="cocResult"></param>
        /// <param name="smIndex">The SM number for the ripple scan from coc result</param>
        /// <param name="moduleConfigData"></param>
        public static void LoadConfigToModule(DatumList cocResult, int smIndex, DatumList moduleConfigData)
        {
            // Load config from coc result only once
            if (isHasLoadConfig == false)
            {
                __LoadConfig(cocResult, smIndex, true);

                isHasLoadConfig = true;
            }

            moduleConfigData.AddSint32("AfterJumpIgnoreCount", afterJumpIgnoreCount);
            moduleConfigData.AddSint32("BeforeJumpIgnoreCount", beforeJumpIgnoreCount);
            moduleConfigData.AddDouble("Multiple", multiple);
            moduleConfigData.AddString("MultipleFilterString", multipleFilterString);
            moduleConfigData.AddDouble("Ripple_MIN", ripple_Min);
            moduleConfigData.AddDouble("Ripple_MAX", ripple_Max);

            if (moduleConfigData.IsPresent("Igain_mA"))
            {
                moduleConfigData.UpdateDouble("Igain_mA", gain_mA);
            }
            else
            {
                moduleConfigData.AddDouble("Igain_mA", gain_mA);
            }

            if (moduleConfigData.IsPresent("IfsCon_mA"))
            {
                moduleConfigData.UpdateDouble("IfsCon_mA", fsCon_mA);
            }
            else
            {
                moduleConfigData.AddDouble("IfsCon_mA", fsCon_mA);
            }

            if (moduleConfigData.IsPresent("IrearSoa_mA"))
            {
                moduleConfigData.UpdateDouble("IrearSoa_mA", rearSoa_mA);
            }
            else
            {
                moduleConfigData.AddDouble("IrearSoa_mA", rearSoa_mA);
            }
        }

        /// <summary>
        /// Analyse the ripple by a list of frequencies - chongjian.liang 2013.5.6
        /// </summary>
        /// <param name="freqArray_GHz"></param>
        /// <param name="ripple_Min"></param>
        /// <param name="ripple_Max"></param>
        /// <param name="afterJumpIgnoreCount"></param>
        /// <param name="beforeJumpIgnoreCount"></param>
        /// <param name="multiple"></param>
        /// <param name="multipleFilterString"></param>
        /// <param name="o_ripple"></param>
        /// <param name="o_rippleCount"></param>
        public static void AnalyseRipple(double[] freqArray_GHz, double ripple_Min, double ripple_Max, int afterJumpIgnoreCount, int beforeJumpIgnoreCount, double multiple, string multipleFilterString, out List<double> o_ripple, out List<int> o_rippleCount)
        {
            o_ripple = new List<double>();
            o_rippleCount = new List<int>();

            List<int> backIndexList;
            List<int> jumpIndexList;
            List<int> noiseIndexList;
            List<double> outRangDeltaList;

            string[] multipleFilterStringList = multipleFilterString.Trim().Split(';');

            List<double[]> multipleFilter = new List<double[]>();

            foreach (string mf in multipleFilterStringList)
            {
                if (mf.Trim().Length > 0)
                {
                    string[] mfStrings = mf.Trim().Split(',');
                    if (mfStrings.Length >= 3)
                    {
                        multipleFilter.Add(new double[] { Convert.ToDouble(mfStrings[0].Trim()), Convert.ToDouble(mfStrings[1].Trim())
                                                , Convert.ToDouble(mfStrings[2].Trim()) });
                    }
                }
            }

            int outOfRang = __AnalysisData(freqArray_GHz, ripple_Min, ripple_Max, multiple, 1d / 4d, 3d / 4d, out backIndexList, out jumpIndexList, out noiseIndexList, out outRangDeltaList, multipleFilter, afterJumpIgnoreCount, beforeJumpIgnoreCount);

            outRangDeltaList.Sort();

            if (backIndexList.Count > 0)// deltaFreq[0] < -1)//if it turn down more than 1GHz, then fail
            {
                o_ripple.Add(outRangDeltaList[0]);//deltaFreq[0]); //the min value 
            }
            else if (outRangDeltaList[outRangDeltaList.Count - 1] > 20)//(deltaFreq[deltaFreq.Count - 1] > 20)//if max turn up delta value more than 20GHz, then fail
            {
                o_ripple.Add(outRangDeltaList[outRangDeltaList.Count - 1]);// (deltaFreq[deltaFreq.Count - 1]);//the delta value > 20GHz
            }
            else if (outOfRang >= 1)//outOfRang > 2)//if outOfRang number is more then 2, then fail//Jim @ 2012.04.18
            {
                o_ripple.Add(outRangDeltaList[outRangDeltaList.Count - 1]);//the max delta value
                //RippleOfSm.Add(deltaFreq[deltaFreq.Count - 1]); 
            }
            else//it is ok
            {
                //get the last valid value in delta list(ignore the jump and noise point)
                //RippleOfSm.Add(deltaFreq[deltaFreq.Count - 1 - (jumpIndexList.Count + noiseIndexList.Count)]);
                o_ripple.Add(outRangDeltaList[outRangDeltaList.Count - 1 - outOfRang]);//(jumpIndexList.Count + noiseIndexList.Count)]);
            }

            o_rippleCount.Add(jumpIndexList.Count);
        }

        /// <summary>
        /// Do phase scan, analyse ripple and Save result to file
        /// </summary>
        public static double DoRippleTest(bool isUseOpticalBoxEtalon, string testStage, double chanFreq_GHz, string phaseScanSequenceFilePath, string rippleTestDropFilePath, int scanStartIndex, int scanEndIndex,
            IInstType_TecController dsdbrTec, double CH_LASER_RTH_Min, double CH_LASER_RTH_Max,
            double ripple_Min, double ripple_Max, int afterJumpIgnoreCount, int beforeJumpIgnoreCount, double multiple, string multipleFilterString)
        {
            double[] phaseArray_mA;
            double[] lockerRatioArray;
            double[] freqArray_GHz;

            PhaseScanOperations.PhaseScanByIndex(isUseOpticalBoxEtalon, phaseScanSequenceFilePath, scanStartIndex, scanEndIndex,
                dsdbrTec, CH_LASER_RTH_Min, CH_LASER_RTH_Max,
                out phaseArray_mA, out lockerRatioArray, out freqArray_GHz);

            List<double> ripple;
            List<int> rippleCount;

            AnalyseRipple(freqArray_GHz, ripple_Min, ripple_Max, afterJumpIgnoreCount, beforeJumpIgnoreCount, multiple, multipleFilterString,
                out ripple, out rippleCount);

            __SaveTestResultToFile(rippleTestDropFilePath, testStage, chanFreq_GHz, scanStartIndex, scanEndIndex, ripple[0], phaseArray_mA, lockerRatioArray, freqArray_GHz);

            return ripple[0];
        }

        /// <summary>
        /// Do phase scan, analyse ripple and Save result to file
        /// </summary>
        public static double DoRippleTest(bool isUseOpticalBoxEtalon, string testStage, double chanFreq_GHz, string phaseScanSequenceFilePath, string rippleTestDropFilePath, int scanStartIndex, int scanEndIndex,
            IInstType_TecController dsdbrTec, double CH_LASER_RTH_Min, double CH_LASER_RTH_Max,
            double ripple_Min, double ripple_Max, int afterJumpIgnoreCount, int beforeJumpIgnoreCount, double multiple, string multipleFilterString,
            out double o_readLockerRatioTime, out double o_readWaveMeterTime)
        {
            double[] phaseArray_mA;
            double[] lockerRatioArray;
            double[] freqArray_GHz;

            PhaseScanOperations.PhaseScanByIndex(isUseOpticalBoxEtalon, phaseScanSequenceFilePath, scanStartIndex, scanEndIndex,
                dsdbrTec, CH_LASER_RTH_Min, CH_LASER_RTH_Max,
                out phaseArray_mA, out lockerRatioArray, out freqArray_GHz,
                out o_readLockerRatioTime, out o_readWaveMeterTime);

            List<double> ripple;
            List<int> rippleCount;

            AnalyseRipple(freqArray_GHz, ripple_Min, ripple_Max, afterJumpIgnoreCount, beforeJumpIgnoreCount, multiple, multipleFilterString,
                out ripple, out rippleCount);

            __SaveTestResultToFile(rippleTestDropFilePath, testStage, chanFreq_GHz, scanStartIndex, scanEndIndex, ripple[0], phaseArray_mA, lockerRatioArray, freqArray_GHz);

            return ripple[0];
        }

        private static void __SaveTestResultToFile(string rippleTestDropFilePath, string testStage, double chanFreq_GHz, double scanStartIndex, double scanEndIndex, double ripple, double[] iPhaseArray_mA, double[] lockerRatioArray, double[] freqArray_GHz)
        {
            using (StreamWriter writer = new StreamWriter(rippleTestDropFilePath, true))
            {
                // Write the first line
                if (!string.IsNullOrEmpty(testStage))
                {
                    writer.Write("Test Stage:{0},", testStage);
                }
                writer.Write("ITU Channel[GHz]:{0},", chanFreq_GHz);
                writer.Write("Scan bound:{0}-{1},", scanStartIndex, scanEndIndex);
                writer.Write("Ripple:{0},", ripple);
                writer.WriteLine();

                // Write the second line
                writer.Write("Phase[mA],");
                foreach (double iPhase in iPhaseArray_mA)
                {
                    writer.Write(iPhase.ToString() + ",");
                }
                writer.WriteLine();

                // Write the third line
                writer.Write("LockerRatio,");
                foreach (double lockerRatio in lockerRatioArray)
                {
                    writer.Write(lockerRatio.ToString() + ",");
                }
                writer.WriteLine();

                // Write the four line
                writer.Write("Frequency[GHz],");
                foreach (double frequency in freqArray_GHz)
                {
                    writer.Write(frequency.ToString() + ",");
                }
                writer.WriteLine();
            }
        }

        private static void __LoadConfig(DatumList cocResult, bool isLoadDSDBRSetting)
        {
            const int smCount = 7;

            // Find a sm ripple test config as the current ripple test config
            for (int smIndex = 0; smIndex < smCount; smIndex++)
            {
                string cocPhaseScanConfigFileTitle = "PLOT_PHASE_CUT_SCAN_SM" + smIndex;

                if (cocResult.IsPresent(cocPhaseScanConfigFileTitle))
                {
                    string cocPhaseScanConfigFilePath = cocResult.ReadFileLinkFullPath(cocPhaseScanConfigFileTitle);

                    // Make sure the file exists even a link exists in PCAS result
                    if (File.Exists(cocPhaseScanConfigFilePath))
                    {
                        using (CsvReader reader = new CsvReader())
                        {
                            reader.OpenFile(cocPhaseScanConfigFilePath);

                            string[] names = reader.GetLine();
                            string[] values = reader.GetLine();

                            // Read coc config if those params are appended to the this file
                            if (values.Length > 15)
                            {
                                if (isLoadDSDBRSetting)
                                {
                                    gain_mA = double.Parse(values[7]);
                                    fsCon_mA = double.Parse(values[9]);
                                    rearSoa_mA = double.Parse(values[13]);
                                }

                                afterJumpIgnoreCount = int.Parse(values[16]);
                                beforeJumpIgnoreCount = int.Parse(values[17]);
                                multiple = double.Parse(values[19]);
                                multipleFilterString = values[20].Replace('#', ',');
                                ripple_Min = double.Parse(values[21]);
                                ripple_Max = double.Parse(values[22]);
                            }
                        }
                    }

                    break;
                }
            }
        }

        private static void __LoadConfig(DatumList cocResult, int smIndex, bool isLoadDSDBRSetting)
        {
            string cocPhaseScanConfigFileTitle = "PLOT_PHASE_CUT_SCAN_SM" + smIndex;

            if (cocResult.IsPresent(cocPhaseScanConfigFileTitle))
            {
                string cocPhaseScanConfigFilePath = cocResult.ReadFileLinkFullPath(cocPhaseScanConfigFileTitle);

                // Make sure the file exists even a link exists in PCAS result
                if (File.Exists(cocPhaseScanConfigFilePath))
                {
                    using (CsvReader reader = new CsvReader())
                    {
                        reader.OpenFile(cocPhaseScanConfigFilePath);

                        string[] names = reader.GetLine();
                        string[] values = reader.GetLine();

                        // Read coc config if those params are appended to the this file
                        if (values.Length > 15)
                        {
                            if (isLoadDSDBRSetting)
                            {
                                gain_mA = double.Parse(values[7]);
                                fsCon_mA = double.Parse(values[9]);
                                rearSoa_mA = double.Parse(values[13]);
                            }

                            afterJumpIgnoreCount = int.Parse(values[16]);
                            beforeJumpIgnoreCount = int.Parse(values[17]);
                            multiple = double.Parse(values[19]);
                            multipleFilterString = values[20].Replace('#', ',');
                            ripple_Min = double.Parse(values[21]);
                            ripple_Max = double.Parse(values[22]);
                        }
                    }
                }
            }
        }

        private static int __AnalysisData(double[] freqListSoa, double rippleLimit_MIN, double rippleLimit_MAX, double multiple, double rangeBegin, double rangeEnd
                       , out List<int> backIndexList, out List<int> jumpIndexList, out List<int> noiseIndexList, out List<double> outRangDeltaList
                       , List<double[]> multipleFilter, int afterJumpIgnoreCount, int beforeJumpIgnoreCount)
        {
            int currentIndex = 0;
            int outOfRang = 0;
            backIndexList = new List<int>();
            jumpIndexList = new List<int>();
            noiseIndexList = new List<int>();
            outRangDeltaList = new List<double>();

            while (true)
            {
                if (currentIndex >= freqListSoa.Length)
                {
                    break;
                }
                List<double> dataListSub = new List<double>();
                List<double> dataListSubABS = new List<double>();
                bool isFirst = true;
                //find line
                for (; currentIndex < freqListSoa.Length; currentIndex++)
                {
                    if (!isFirst)
                    {
                        double addedValue = freqListSoa[currentIndex] - freqListSoa[currentIndex - 1];
                        //Convert.ToDouble(datas[currentIndex].Trim()) - Convert.ToDouble(datas[currentIndex - 1].Trim());
                        if (addedValue <= -20)
                        {
                            break;
                        }
                        dataListSub.Add(addedValue);
                        dataListSubABS.Add(Math.Abs(addedValue));
                    }
                    else
                    {
                        isFirst = false;
                    }
                }
                //get avg value
                dataListSubABS.Sort();
                double sumValue = 0;
                double count = 0;
                for (int i = (int)(dataListSubABS.Count * rangeBegin); i < dataListSubABS.Count * rangeEnd; i++)
                {
                    sumValue += dataListSubABS[i];
                    count++;
                }
                double avgValue = sumValue / count;
                //get fixed multiple value from multipleFilter array. By Jim @ 2012.04.18
                double multipleFixed = multiple;
                if (multipleFilter.Count > 0)
                {
                    foreach (double[] mfArray in multipleFilter)
                    {
                        if (avgValue >= mfArray[0] && avgValue < mfArray[1])
                        {
                            multipleFixed = mfArray[2];
                            break;
                        }
                    }
                }
                int minIndex = 0 + afterJumpIgnoreCount;
                int maxIndex = dataListSub.Count - 1 - beforeJumpIgnoreCount;
                if (currentIndex - dataListSub.Count - 1 == 0)
                {
                    //ignore first point
                    minIndex = 0;
                }
                if (currentIndex == freqListSoa.Length)
                {
                    //ignore last point
                    maxIndex = dataListSub.Count - 1 - 1;
                }

                double avgValueTemp = avgValue * multipleFixed;
                //analysis
                for (int i = minIndex; i <= maxIndex; )
                {
                    int Index = currentIndex - dataListSub.Count + i;
                    if (dataListSub[i] < rippleLimit_MIN)
                    {
                        //error1
                        backIndexList.Add(Index);
                        outRangDeltaList.Add(dataListSub[i]);
                    }
                    else if (dataListSub[i] > rippleLimit_MAX)
                    {
                        //error2
                        if ((jumpIndexList.Count > 0 ? jumpIndexList[jumpIndexList.Count - 1] : -1) != Index - 1
                            && (noiseIndexList.Count > 0 ? noiseIndexList[noiseIndexList.Count - 1] : -1) != Index - 1)
                        {
                            outOfRang++;
                            outRangDeltaList.Add(Math.Abs(dataListSub[i]));
                        }
                        jumpIndexList.Add(Index);
                    }
                    else
                    {
                        //error2.1
                        if (Math.Abs(dataListSub[i]) > avgValueTemp)
                        {
                            if (i + 1 < dataListSub.Count)
                            {
                                if (Math.Abs(dataListSub[i + 1]) > avgValueTemp)
                                {
                                    if (dataListSub[i] + dataListSub[i + 1] > rippleLimit_MAX)
                                    {
                                        if ((jumpIndexList.Count > 0 ? jumpIndexList[jumpIndexList.Count - 1] : -1) != Index - 1
                                            && (noiseIndexList.Count > 0 ? noiseIndexList[noiseIndexList.Count - 1] : -1) != Index - 1)
                                        {
                                            outOfRang++;
                                            outRangDeltaList.Add(Math.Abs(dataListSub[i] + dataListSub[i + 1]));
                                        }
                                        noiseIndexList.Add(Index);
                                        i += 1;
                                        continue;
                                    }
                                }
                            }
                        }
                        else//no error
                        {
                            outRangDeltaList.Add(dataListSub[i]);
                        }
                    }
                    i++;
                }


            }

            int noiseCount = noiseIndexList.Count;
            return outOfRang;
        }

        private static int afterJumpIgnoreCount = 3;
        private static int beforeJumpIgnoreCount = 3;
        private static double multiple = 2.5;
        private static string multipleFilterString = "0,2,2.5;2,99999,2.5";
        private static double ripple_Min = -1;
        private static double ripple_Max = 4;

        private static double gain_mA;
        private static double fsCon_mA;
        private static double rearSoa_mA;
        private static bool isHasLoadConfig = false;
    }
}
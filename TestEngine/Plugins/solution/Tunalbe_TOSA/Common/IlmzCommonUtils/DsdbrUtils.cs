using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Bookham.TestSolution.IlmzCommonInstrs;
using Bookham.TestSolution.IlmzCommonData;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestEngine.PluginInterfaces;
using Bookham.Toolkit.CloseGrid;//jack.Zhang
using Bookham.TestLibrary.BlackBoxes;
using Bookham.TestLibrary.Instruments;
using Bookham.TestEngine.Framework.Logging;

namespace Bookham.TestSolution.IlmzCommonUtils
{
    /// <summary>
    /// DSDBR utilities
    /// </summary>
    public sealed class DsdbrUtils
    {
        private DsdbrUtils() { }

        /// <summary>
        /// DSDBR instruments property
        /// </summary>
        public static DsdbrInstruments DsdbrInstrumentGroup
        {
            set
            {
                pxiInstrs = value;
                dsdbrDriverInstrsToUse = DsdbrDriveInstruments.PXIInstruments;
            }
            get
            {
                return pxiInstrs;
            }
        }

        /// <summary>
        /// FCU instruments property
        /// </summary>
        public static FCUInstruments FcuInstrumentGroup
        {
            set
            {
                fcuInstrs = value;
                dsdbrDriverInstrsToUse = DsdbrDriveInstruments.FCUInstruments;
            }
            get
            {
                return fcuInstrs;
            }
        }

        // added by Alice.Huang, for TOSA GB test 
        // with DSDBR Source is ASIC on chip, and control by FCU
        // on 2010-01-27
        /// <summary>
        /// get/set FCU2Asic instruments group. <see cref="fcu2AsicInstrs"/>
        /// </summary>
        public static FCU2AsicInstruments Fcu2AsicInstrumentGroup
        {
            set
            {
                fcu2AsicInstrs = value;
                dsdbrDriverInstrsToUse = DsdbrDriveInstruments.FCU2AsicInstrument;
            }
            get
            {
                return fcu2AsicInstrs;
            }
        }
        /// <summary>
        /// DsdbrDriverInstr to be used
        /// </summary>
        public static DsdbrDriveInstruments DsdbrDriverInstrsToUse
        {
            get { return dsdbrDriverInstrsToUse; }
        }

        /// <summary>
        /// Set DSDBR currents
        /// </summary>
        /// <param name="setup">DSDBR Channel setup</param>
        public static void SetDsdbrCurrents_mA(DsdbrChannelSetup setup)
        {
            SetDsdbrCurrents_mA(setup, dsdbrDriverInstrsToUse);
        }

        // Alice.Huang   2010-02-19
        // add this soa 2 dac to allow flexialbe convertion application

        /// <summary>
        /// convert Soa current to Dac 
        /// </summary>
        /// <param name="soa_mA"> soa current value in mA </param>
        /// <returns>conresponding SOA Dac value for Asic </returns>
        //public static int Soa2Dac(double soa_mA)
        //{
        //    double calFactor = 0;
        //    double calOffset = 0;

        //    if (soa_mA >= 0)
        //    {
        //        calFactor = fcu2AsicInstrs.Fcu2Asic.SectionsDacCalibration.FrontSoaDac_Pos_CalFactor;
        //        calOffset = fcu2AsicInstrs.Fcu2Asic.SectionsDacCalibration.FrontSoaDac_Pos_CalOffset;
        //    }
        //    else
        //    {
        //        calOffset = fcu2AsicInstrs.Fcu2Asic.SectionsDacCalibration.FrontSoaDac_Neg_CalOffset;
        //        calFactor = fcu2AsicInstrs.Fcu2Asic.SectionsDacCalibration.FrontSoaDac_Neg_CalFactor;
        //    }
        //    int dac = (int)(soa_mA * calFactor + calOffset) + fcu2AsicInstrs .Fcu2Asic .ZeroSoaDac; 
        //    return dac;

        //}
        /// <summary>
        /// Set DSDBR currents
        /// </summary>
        /// <param name="setup"></param>
        /// <param name="instrsToUse"></param>
        private static void SetDsdbrCurrents_mA(DsdbrChannelSetup setup, DsdbrDriveInstruments instrsToUse)
        {
            if (instrsToUse == DsdbrDriveInstruments.PXIInstruments)
            {
                // get Dsdbr section drivers
                InstType_ElectricalSource gain = (InstType_ElectricalSource)pxiInstrs.CurrentSources[DSDBRSection.Gain];
                InstType_ElectricalSource phase = (InstType_ElectricalSource)pxiInstrs.CurrentSources[DSDBRSection.Phase];
                InstType_ElectricalSource rear = (InstType_ElectricalSource)pxiInstrs.CurrentSources[DSDBRSection.Rear];
                InstType_ElectricalSource soa = (InstType_ElectricalSource)pxiInstrs.CurrentSources[DSDBRSection.SOA];

                int secondPair = setup.FrontPair + 1;
                DSDBRSection firstFsId = (DSDBRSection)Enum.Parse(typeof(DSDBRSection), "Front" + setup.FrontPair.ToString().Trim());
                DSDBRSection secondFsId = (DSDBRSection)Enum.Parse(typeof(DSDBRSection), "Front" + secondPair.ToString().Trim());
                InstType_ElectricalSource firstFs = (InstType_ElectricalSource)pxiInstrs.CurrentSources[firstFsId];
                InstType_ElectricalSource secondFs = (InstType_ElectricalSource)pxiInstrs.CurrentSources[secondFsId];

                // Set current to 0 on all unused front sections
                DSDBRSection fsId;
                InstType_ElectricalSource fs;
                for (int fsNum = 1; fsNum < 9; fsNum++)
                {
                    fsId = (DSDBRSection)Enum.Parse(typeof(DSDBRSection), "Front" + fsNum.ToString().Trim());
                    fs = (InstType_ElectricalSource)pxiInstrs.CurrentSources[fsId];

                    if (fsNum != setup.FrontPair && fsNum != secondPair)
                    {
                        fs.CurrentSetPoint_amp = 0.0;
                        fs.OutputEnabled = false;
                    }
                }

                // Apply currents
                //gain.CurrentSetPoint_amp = setup.IGain_mA;
                //phase.CurrentSetPoint_amp = setup.IPhase_mA;
                //rear.CurrentSetPoint_amp = setup.IRear_mA;
                //soa.CurrentSetPoint_amp = setup.ISoa_mA;
                firstFs.CurrentSetPoint_amp = setup.IFsFirst_mA / 1000.0;
                secondFs.CurrentSetPoint_amp = setup.IFsSecond_mA / 1000.0;

                // Make sure sections are enabled and unused front sections are disabled
                EnableDsdbr(true);

                // Really apply currents !
                gain.CurrentSetPoint_amp = setup.IGain_mA / 1000.0;
                firstFs.CurrentSetPoint_amp = setup.IFsFirst_mA / 1000.0;
                secondFs.CurrentSetPoint_amp = setup.IFsSecond_mA / 1000.0;
                rear.CurrentSetPoint_amp = setup.IRear_mA / 1000.0;
                phase.CurrentSetPoint_amp = setup.IPhase_mA / 1000.0;
                soa.CurrentSetPoint_amp = setup.ISoa_mA / 1000.0;
            }

            // Added by Alice.Huang, for TOSA GB test 
            // with DSDBR Source is ASIC on chip, and control by FCU
            // on 2010-01-27
            else if (instrsToUse == DsdbrDriveInstruments.FCU2AsicInstrument )
            {
                // Enable fcu & ASIC OUTPUT 
                EnableDsdbr(true);

                //fcu2AsicInstrs.Fcu2Asic.ZeroCurrent();
                //fcu2AsicInstrs.Fcu2Asic.CloseAllAsic();
                System.Threading.Thread.Sleep(5);
                // Set gain current
                fcu2AsicInstrs.Fcu2Asic.IGain_mA = (float)setup.IGain_mA;
                //SetCurrentToFcu("gain", (float)setup.IGain_mA);
                System.Threading.Thread.Sleep(5);
                // Set soa current
                fcu2AsicInstrs.Fcu2Asic.ISoa_mA = (float)setup.ISoa_mA;
                //SetCurrentToFcu("soa", (float)setup.ISoa_mA);
                System.Threading.Thread.Sleep(5);
                // Set front section pair number & front section currents
                fcu2AsicInstrs.Fcu2Asic.SetFrontSectionPairCurrent(setup.FrontPair,
                   (float)setup.IFsFirst_mA, (float)setup.IFsSecond_mA);
                System.Threading.Thread.Sleep(5);
                // Set rear current
                fcu2AsicInstrs.Fcu2Asic.IRear_mA = (float)setup.IRear_mA;
                System.Threading.Thread.Sleep(5);
                //SetCurrentToFcu("rear", (float)setup.IRear_mA);
                
                // Set phase current
                fcu2AsicInstrs.Fcu2Asic.IPhase_mA = (float)setup.IPhase_mA;
                System.Threading.Thread.Sleep(5);
                //SetCurrentToFcu("phase", (float)setup.IPhase_mA);


                fcu2AsicInstrs.Fcu2Asic.IRearSoa_mA = (float)setup.IRearSoa_mA;
                //SetCurrentToFcu("reso", (float)setup.IRearSoa_mA);
                // Set gain current
                //fcu2AsicInstrs.Fcu2Asic.IGain_mA = (float)setup.IGain_mA;
                System.Threading.Thread.Sleep(5);

            }  // end else if fcu2asic
            else
            {
                // Enable FCU
                EnableDsdbr(true);

                // Set zero rear current
                fcuInstrs.FullbandControlUnit.SetIVSource(0, IVSourceIndexNumbers.Rear);
                // Set zero phase current
                fcuInstrs.FullbandControlUnit.SetIVSource(0, IVSourceIndexNumbers.Phase);

                // Set gain current
                fcuInstrs.FullbandControlUnit.SetIVSource((float)setup.IGain_mA, IVSourceIndexNumbers.Gain);

                // Set front section pair number
                fcuInstrs.FullbandControlUnit.SetFrontSectionPair(setup.FrontPair);
                // Set front section currents
                if (setup.FrontPair % 2 == 0)
                {
                    // "first" is even
                    fcuInstrs.FullbandControlUnit.SetIVSource((float)setup.IFsFirst_mA, IVSourceIndexNumbers.FrontEven);
                    fcuInstrs.FullbandControlUnit.SetIVSource((float)setup.IFsSecond_mA, IVSourceIndexNumbers.FrontOdd);
                }
                else
                {
                    // "first" is odd
                    fcuInstrs.FullbandControlUnit.SetIVSource((float)setup.IFsFirst_mA, IVSourceIndexNumbers.FrontOdd);
                    fcuInstrs.FullbandControlUnit.SetIVSource((float)setup.IFsSecond_mA, IVSourceIndexNumbers.FrontEven);
                }
                // Set rear current
                fcuInstrs.FullbandControlUnit.SetIVSource((float)setup.IRear_mA, IVSourceIndexNumbers.Rear);
                // Set phase current
                fcuInstrs.FullbandControlUnit.SetIVSource((float)setup.IPhase_mA, IVSourceIndexNumbers.Phase);
                // Set soa current
                fcuInstrs.SoaCurrentSource.CurrentSetPoint_amp = setup.ISoa_mA / 1000;
            }
        }

        /// <summary>
        /// Set current to Fcu to avoid asic error
        /// </summary>
        /// <param name="pinName"></param>
        /// <param name="current_mA"></param>
        private static void SetCurrentToFcu(string pinName, float current_mA)
        {
            bool fcuError = false;
            int fcuErrorTimes = 3;
            while (fcuErrorTimes > 0)
            {
                try
                {
                    fcuError = false;
                    if (pinName.Contains("gain"))
                    {
                        fcu2AsicInstrs.Fcu2Asic.IGain_mA = current_mA;
                    }
                    if (pinName.Contains("phase"))
                    {
                        fcu2AsicInstrs.Fcu2Asic.IPhase_mA = current_mA;
                    }
                    if (pinName.Contains("rear"))
                    {
                        fcu2AsicInstrs.Fcu2Asic.IRear_mA = current_mA;
                    }
                    if (pinName.Contains("soa"))
                    {
                        fcu2AsicInstrs.Fcu2Asic.ISoa_mA = current_mA;
                    }
                    if (pinName.Contains("reso"))
                    {
                        fcu2AsicInstrs.Fcu2Asic.IRearSoa_mA = current_mA;
                    }
                    if (pinName.Contains("leftimb"))
                    {
                        fcu2AsicInstrs.Fcu2Asic.IimbLeft_mA = current_mA;
                    }
                    if (pinName.Contains("rightimb"))
                    {
                        fcu2AsicInstrs.Fcu2Asic.IimbRight_mA = current_mA;
                    }
                }
                catch (Exception e)
                {
                    fcuError = true;
                    fcu2AsicInstrs.FcuSource.OutputEnabled = false;
                    Fcu2AsicInstrumentGroup.AsicVccSource.OutputEnabled = false;
                    Fcu2AsicInstrumentGroup.AsicVeeSource.OutputEnabled = false;
                }
                if (fcuError)
                {
                    fcuErrorTimes--;
                    fcu2AsicInstrs.FcuSource.OutputEnabled = true;
                    Fcu2AsicInstrumentGroup.AsicVccSource.OutputEnabled = true;
                    Fcu2AsicInstrumentGroup.AsicVeeSource.OutputEnabled = true;
                    
                }
                else
                {
                    fcuErrorTimes = 0;
                }
            }
            if (fcuError)
            {
                throw new Exception("Fcu Fatal error occurs!");
            }
        }

        /// <summary>
        /// Static class to set output state of all DSDBR sources.
        /// </summary>
        /// <param name="enableState">Output state, true for enabled</param>
        public static void EnableDsdbr(bool enableState)
        {
            EnableDsdbr(enableState, dsdbrDriverInstrsToUse);
        }

        /// <summary>
        /// Static class to set output state of all DSDBR sources.
        /// </summary>
        /// <param name="enableState">Output state, true for enabled</param>
        /// <param name="instrsToUse">Instruments to be used</param>
        private static void EnableDsdbr(bool enableState, DsdbrDriveInstruments instrsToUse)
        {
            if (instrsToUse == DsdbrDriveInstruments.PXIInstruments)
            {
                // Get Dsdbr section drivers
                InstType_ElectricalSource gain = (InstType_ElectricalSource)pxiInstrs.CurrentSources[DSDBRSection.Gain];
                InstType_ElectricalSource phase = (InstType_ElectricalSource)pxiInstrs.CurrentSources[DSDBRSection.Phase];
                InstType_ElectricalSource rear = (InstType_ElectricalSource)pxiInstrs.CurrentSources[DSDBRSection.Rear];
                InstType_ElectricalSource soa = (InstType_ElectricalSource)pxiInstrs.CurrentSources[DSDBRSection.SOA];
                InstType_ElectricalSource firstFs = (InstType_ElectricalSource)pxiInstrs.CurrentSources[DSDBRSection.Front1];
                InstType_ElectricalSource secondFs = (InstType_ElectricalSource)pxiInstrs.CurrentSources[DSDBRSection.Front2];

                // Set front pair sections
                int fsPair = -1;
                int fsActiveCount = 0;
                DSDBRSection fsId;
                InstType_ElectricalSource fs;

                for (int fsNum = 1; fsNum < 9; fsNum++)
                {
                    fsId = (DSDBRSection)Enum.Parse(typeof(DSDBRSection), "Front" + fsNum.ToString().Trim());
                    fs = (InstType_ElectricalSource)pxiInstrs.CurrentSources[fsId];

                    if (enableState && fs.CurrentSetPoint_amp > 0 && fsPair == -1)
                    {
                        fsPair = fsNum;
                        fsActiveCount++;
                        firstFs = fs;
                    }
                    else if (enableState && fsNum == fsPair + 1)
                    {
                        fsActiveCount++;
                        secondFs = fs;
                    }
                    else if (enableState && fs.CurrentSetPoint_amp > 0)
                    {
                        fsActiveCount++;
                        fs.OutputEnabled = false;
                    }
                    else
                    {
                        fs.OutputEnabled = false;
                    }

                }

                // Check that exactly two sections are active if enabling
                if (fsActiveCount != 2 && enableState)
                {
                    enableState = false;
                    throw new Exception("Invalid front section configuration");
                }

                // Set sections (order gain-front sections-rear-phase-soa for clockwise startup)
                gain.OutputEnabled = enableState;
                firstFs.OutputEnabled = enableState;
                secondFs.OutputEnabled = enableState;
                rear.OutputEnabled = enableState;
                phase.OutputEnabled = enableState;
                soa.OutputEnabled = enableState;
            }

            // Added by Alice.Huang, for TOSA GB test 
            // with DSDBR Source is ASIC on chip, and control by FCU
            // on 2010-01-27
            else if (instrsToUse == DsdbrDriveInstruments.FCU2AsicInstrument )
            {
                //if (enableState == true)
                //{
                //    fcu2AsicInstrs.Fcu2Asic.IsOnline = true;
                //    // Enable power up
                //    //fcu2AsicInstrs.Fcu2Asic.LaserEnable = Inst_Fcu2Asic.OnOff.on;                
                //}
                //else
                //{
                //    //fcu2AsicInstrs.Fcu2Asic.LaserEnable = Inst_Fcu2Asic.OnOff.off;
                //}

                // Set sections (order gain-front sections-rear-phase-soa for clockwise startup)

                // Alice.Huang     2010-03-04
                // commented gain & soa to ensure instable current being apply on then

                if (!enableState) fcu2AsicInstrs.Fcu2Asic.IGain_mA = 0;
                if (fcu2AsicInstrs.Fcu2Asic.GetFrontPairNumber() != 0)
                {
                    fcu2AsicInstrs.Fcu2Asic.IEven_mA = 0;
                    fcu2AsicInstrs.Fcu2Asic.IOdd_mA = 0;
                }
                fcu2AsicInstrs.Fcu2Asic.IRear_mA = 0;
                fcu2AsicInstrs.Fcu2Asic.IPhase_mA = 0;

                // Alice.Huang  2010-03-07
                // commented to ensure soa is stable to avoid ke2400 can't source enough current because of current range

                //if (!enableState) fcu2AsicInstrs.Fcu2Asic.ISoa_mA = 0;   //

                
            }  // End if FCU2Asic
            else
            {
                if (enableState == true)
                {
                    // Enable power up
                    fcuInstrs.FullbandControlUnit.OIFLaserEnable(true);

                    TxCommand cmd = new TxCommand(0, 0, 0);
                    cmd = fcuInstrs.FullbandControlUnit.ReadTxCommand();
                    TxCommand cmd2 = new TxCommand(cmd.Data1, cmd.Data2,
                        (byte)(cmd.Data3 & (byte)254));
                    fcuInstrs.FullbandControlUnit.SetTxCommand(cmd2);

                    do
                    {
                        try
                        {
                            fcuInstrs.FullbandControlUnit.SetControlRegLaserPSUOn(true);
                            break;
                        }
                        catch (Bookham.TestLibrary.BlackBoxes.TunableModuleException)
                        {
                            // This sometimes needs two attempts !
                            //Log.InstrumentWrite("FCU error when attempting SetControlRegLaserPSUOn");
                            System.Threading.Thread.Sleep(500);
                        }
                    } while (true);

                    // Ensure that the locker circuit is off, because this will 
                    // supply phase current in addition to the phase current source setting.
                    if (fcuInstrs.FullbandControlUnit.GetControlRegLockerOn() != false)
                        fcuInstrs.FullbandControlUnit.SetControlRegLockerOn(false);

                    // SOA section
                    fcuInstrs.SoaCurrentSource.OutputEnabled = true;
                }
                else
                {
                    fcuInstrs.FullbandControlUnit.SetControlRegLaserPSUOn(false);
                    fcuInstrs.SoaCurrentSource.OutputEnabled = false;
                }

                // Set sections (order gain-front sections-rear-phase-soa for clockwise startup)
                fcuInstrs.FullbandControlUnit.SetIVSource(0, IVSourceIndexNumbers.Gain);
                fcuInstrs.FullbandControlUnit.SetIVSource(0, IVSourceIndexNumbers.FrontEven);
                fcuInstrs.FullbandControlUnit.SetIVSource(0, IVSourceIndexNumbers.FrontOdd);
                fcuInstrs.FullbandControlUnit.SetIVSource(0, IVSourceIndexNumbers.Rear);
                fcuInstrs.FullbandControlUnit.SetIVSource(0, IVSourceIndexNumbers.Phase);
                // SOA section
                fcuInstrs.SoaCurrentSource.CurrentSetPoint_amp = 0.0;
            }
        }

        /// <summary>
        /// Clockwise start of phase and rear sections.
        /// </summary>
        /// <param name="Iphase_mA">Phase current</param>
        /// <param name="Irear_mA">Rear current</param>
        public static void ClockwiseStart(double Iphase_mA, double Irear_mA)
        {
            ClockwiseStart(Iphase_mA, Irear_mA, dsdbrDriverInstrsToUse);
        }

        /// <summary>
        /// Clockwise start of phase and rear sections.
        /// </summary>
        /// <param name="Iphase_mA">Phase current</param>
        /// <param name="Irear_mA">Rear current</param>
        /// <param name="instrsToUse">Instruments to be used</param>
        private static void ClockwiseStart(double Iphase_mA, double Irear_mA, DsdbrDriveInstruments instrsToUse)
        {
            if (instrsToUse == DsdbrDriveInstruments.PXIInstruments)
            {
                // Get Dsdbr section drivers
                InstType_ElectricalSource phase = (InstType_ElectricalSource)pxiInstrs.CurrentSources[DSDBRSection.Phase];
                InstType_ElectricalSource rear = (InstType_ElectricalSource)pxiInstrs.CurrentSources[DSDBRSection.Rear];

                // Do clockwise start
                rear.CurrentSetPoint_amp = 0.0;
                phase.CurrentSetPoint_amp = 0.0;
                rear.CurrentSetPoint_amp = Irear_mA / 1000;
                phase.CurrentSetPoint_amp = Iphase_mA / 1000;
            }
            // added by Alice.Huang, for TOSA GB test 
            // with DSDBR Source is ASIC on chip, and control by FCU
            // on 2010-01-27
            else if (instrsToUse == DsdbrDriveInstruments .FCU2AsicInstrument)
            {
                fcu2AsicInstrs.Fcu2Asic.IRear_mA = 0;
                fcu2AsicInstrs.Fcu2Asic.IPhase_mA = 0;
                fcu2AsicInstrs.Fcu2Asic.IRear_mA = (float)Irear_mA;
                fcu2AsicInstrs.Fcu2Asic.IPhase_mA = (float)Iphase_mA;
                
            }
            else
            {
                fcuInstrs.FullbandControlUnit.SetIVSource(0, IVSourceIndexNumbers.Rear);
                fcuInstrs.FullbandControlUnit.SetIVSource(0, IVSourceIndexNumbers.Phase);
                fcuInstrs.FullbandControlUnit.SetIVSource((float)Irear_mA, IVSourceIndexNumbers.Rear);
                fcuInstrs.FullbandControlUnit.SetIVSource((float)Iphase_mA, IVSourceIndexNumbers.Phase);
            }
        }

        /// <summary>
        /// Set a DSDBR section current
        /// </summary>
        /// <param name="Section">DSDBR Section</param>
        /// <param name="SectionCurrent_mA">Section Current</param>
        public static void SetSectionCurrent_mA(DSDBRSection Section, double SectionCurrent_mA)
        {
            SetSectionCurrent_mA(Section, SectionCurrent_mA, dsdbrDriverInstrsToUse);
        }

        /// <summary>
        /// Set a DSDBR section current, if is FCU2Asic source, the FrontSection + i  
        /// function is disable to avoid error
        /// </summary>
        /// <param name="Section">DSDBR Section</param>
        /// <param name="SectionCurrent_mA">Section current</param>
        /// <param name="instrsToUse">Instruments to be used</param>
        private static void SetSectionCurrent_mA(DSDBRSection Section, double SectionCurrent_mA, DsdbrDriveInstruments instrsToUse)
        {
            if (instrsToUse == DsdbrDriveInstruments.PXIInstruments)
            {
                // Get section
                InstType_ElectricalSource source = (InstType_ElectricalSource)pxiInstrs.CurrentSources[Section];

                // Set current
                source.CurrentSetPoint_amp = SectionCurrent_mA / 1000;
            }
            // added by Alice.Huang, for TOSA GB test 
            // with DSDBR Source is ASIC on chip, and control by FCU
            // on 2010-01-27
            else if (instrsToUse == DsdbrDriveInstruments .FCU2AsicInstrument )
            {
                switch (Section)
                {
                    case DSDBRSection.Gain:
                        fcu2AsicInstrs.Fcu2Asic.IGain_mA = (float)SectionCurrent_mA;
                        break;
                    case DSDBRSection.SOA:
                        fcu2AsicInstrs.Fcu2Asic.ISoa_mA = (float)SectionCurrent_mA;
                        break;
                    case DSDBRSection.Rear:
                        fcu2AsicInstrs.Fcu2Asic.IRear_mA = (float)SectionCurrent_mA;
                        break;
                    case DSDBRSection.Phase:
                        fcu2AsicInstrs.Fcu2Asic.IPhase_mA = (float)SectionCurrent_mA;
                        break;
                    case DSDBRSection .RearSoa :
                        fcu2AsicInstrs.Fcu2Asic.IRearSoa_mA = (float)SectionCurrent_mA;
                        break;
                    case DSDBRSection.Front1:
                    case DSDBRSection.Front2:
                    case DSDBRSection.Front3:
                    case DSDBRSection.Front4:
                    case DSDBRSection.Front5:
                    case DSDBRSection.Front6:
                    case DSDBRSection.Front7:
                    case DSDBRSection.Front8:

                    // alice.Huang :               2010-01-27
                        //disable the front section's function because i can't get the 
                        // FrontSectionPir in use, So set a radom front section might cause error

                        int frontSection = int.Parse(Section.ToString().Replace("Front", ""));
                        int frontPairUsed = (Section - DSDBRSection.Front1) + 1;

                        if (frontSection == frontPairUsed || frontSection == frontPairUsed + 1)
                        {
                            if (frontSection % 2 == 0)
                            {
                                //fcuInstrs.FullbandControlUnit.SetIVSource((float)SectionCurrent_mA, IVSourceIndexNumbers.FrontEven);
                                fcu2AsicInstrs.Fcu2Asic.IEven_mA =(float ) SectionCurrent_mA;
                            }
                            else
                            {
                                //fcuInstrs.FullbandControlUnit.SetIVSource((float)SectionCurrent_mA, IVSourceIndexNumbers.FrontOdd);
                                fcu2AsicInstrs.Fcu2Asic.IOdd_mA = (float)SectionCurrent_mA;
                            }
                        }
                        else
                        {
                            throw new ArgumentException(string.Format(" FCU2Asic Can't set current to front section {0} without given fronsetion pair",
                                Section.ToString()));
                        }
                        break;
                    default:
                        throw new ArgumentException("Unknown DSDBR section!");
                }   // End switch
            }  // End if fcu2Asic
            else
            {
                switch (Section)
                {
                    case DSDBRSection.Gain:
                        fcuInstrs.FullbandControlUnit.SetIVSource((float)SectionCurrent_mA, IVSourceIndexNumbers.Gain);
                        break;
                    case DSDBRSection.SOA:
                        fcuInstrs.SoaCurrentSource.CurrentSetPoint_amp = SectionCurrent_mA / 1000;
                        break;
                    case DSDBRSection.Rear:
                        fcuInstrs.FullbandControlUnit.SetIVSource((float)SectionCurrent_mA, IVSourceIndexNumbers.Rear);
                        break;
                    case DSDBRSection.Phase:
                        fcuInstrs.FullbandControlUnit.SetIVSource((float)SectionCurrent_mA, IVSourceIndexNumbers.Phase);
                        break;
                    case DSDBRSection.Front1:
                    case DSDBRSection.Front2:
                    case DSDBRSection.Front3:
                    case DSDBRSection.Front4:
                    case DSDBRSection.Front5:
                    case DSDBRSection.Front6:
                    case DSDBRSection.Front7:
                    case DSDBRSection.Front8:
                        int frontSection = int.Parse(Section.ToString().Replace("Front", ""));
                        int frontPairUsed = fcuInstrs.FullbandControlUnit.GetFrontSectionPair();
                        if (frontSection == frontPairUsed || frontSection == frontPairUsed + 1)
                        {
                            if (frontSection % 2 == 0)
                            {
                                fcuInstrs.FullbandControlUnit.SetIVSource((float)SectionCurrent_mA, IVSourceIndexNumbers.FrontEven);
                            }
                            else
                            {
                                fcuInstrs.FullbandControlUnit.SetIVSource((float)SectionCurrent_mA, IVSourceIndexNumbers.FrontOdd);
                            }
                        }
                        else
                        {
                            throw new ArgumentException(string.Format("Can't set current to front section {0}", frontSection));
                        }
                        break;
                    default:
                        throw new ArgumentException("Unknown DSDBR section!");
                }
            }
        }

        /// <summary>
        /// Read a DSDBR section current
        /// </summary>
        /// <param name="Section">DSDBR Section</param>
        /// <returns>Section current in mA</returns>
        public static double ReadSectionCurrent_mA(DSDBRSection Section)
        {
            return ReadSectionCurrent_mA(Section, dsdbrDriverInstrsToUse);
        }

        /// <summary>
        /// Read a DSDBR section current
        /// </summary>
        /// <param name="Section">DSDBR Section</param>
        /// <param name="instrsToUse">Instruments to be used</param>
        /// <returns>Section current in mA</returns>
        private static double ReadSectionCurrent_mA(DSDBRSection Section, DsdbrDriveInstruments instrsToUse)
        {
            if (instrsToUse == DsdbrDriveInstruments.PXIInstruments)
            {
                // Get section
                InstType_ElectricalSource source = (InstType_ElectricalSource)pxiInstrs.CurrentSources[Section];

                // Read current
                return source.CurrentActual_amp * 1000;
            }

            // added by Alice.Huang, for TOSA GB test 
            // with DSDBR Source is ASIC on chip, and control by FCU
            // on 2010-01-27                
            else  if (instrsToUse == DsdbrDriveInstruments .FCU2AsicInstrument )
            {
                double sectionCurrent_mA = 0;
                switch (Section)
                {
                    case DSDBRSection.Gain:
                        sectionCurrent_mA = (double) fcu2AsicInstrs .Fcu2Asic.IGain_mA ;
                        break;
                    case DSDBRSection.SOA:
                        sectionCurrent_mA = (double)fcu2AsicInstrs.Fcu2Asic .ISoa_mA;
                        break;
                    case DSDBRSection.Rear:
                        sectionCurrent_mA = (double) fcu2AsicInstrs.Fcu2Asic .IRear_mA;
                        break;
                    case DSDBRSection.Phase:
                        sectionCurrent_mA = (double)fcu2AsicInstrs.Fcu2Asic .IPhase_mA;
                        break;
                    case DSDBRSection .RearSoa :
                        sectionCurrent_mA = (double)fcu2AsicInstrs.Fcu2Asic.IRearSoa_mA;
                        break;
                    //case DSDBRSection .FrontEven:
                    //    sectionCurrent_mA = (double)fcu2AsicInstrs.Fcu2Asic.IEven_mA;
                    //    break;
                    //case DSDBRSection .FrontOdd :
                    //    sectionCurrent_mA = (double)fcu2AsicInstrs.Fcu2Asic.IOdd_mA;
                    //    break;
                    case DSDBRSection.Front1:
                    case DSDBRSection.Front2:
                    case DSDBRSection.Front3:
                    case DSDBRSection.Front4:
                    case DSDBRSection.Front5:
                    case DSDBRSection.Front6:
                    case DSDBRSection.Front7:
                    case DSDBRSection.Front8:

                        // alice.Huang :               2010-01-27
                        //disable the front section's function because i can't get the 
                        // FrontSectionPir in use, So get a radom front section might cause error

                        int frontSection = int.Parse(Section.ToString().Replace("Front", ""));
                        int frontPairUsed = fcu2AsicInstrs.Fcu2Asic.GetFrontPairNumber();
                        if (frontSection == frontPairUsed || frontSection == frontPairUsed + 1)
                        {
                            if (frontSection % 2 == 0)
                            {
                                //sectionCurrent_mA = (double)fcuInstrs.FullbandControlUnit.GetIVSource(IVSourceIndexNumbers.FrontEven);
                                sectionCurrent_mA = (double)fcu2AsicInstrs.Fcu2Asic.IEven_mA;
                                //Add by tim for FrontSection negtive at 2008-08-22
                                if (sectionCurrent_mA < 0)
                                {
                                    sectionCurrent_mA = 0;
                                }
                                //add by jack.zhang for IF=5.0006mA 2011-10-19
                                if (sectionCurrent_mA >5)
                                {
                                    sectionCurrent_mA = 5;
                                }
                                //end
                            }
                            else
                            {
                                //sectionCurrent_mA = (double)fcuInstrs.FullbandControlUnit.GetIVSource(IVSourceIndexNumbers.FrontOdd);
                                sectionCurrent_mA = (double)fcu2AsicInstrs.Fcu2Asic.IOdd_mA;
                                //Add by tim for FrontSection negtive at 2008-08-22
                                if (sectionCurrent_mA < 0)
                                {
                                    sectionCurrent_mA = 0;
                                }
                                //add by jack.zhang for IF=5.0006mA 2011-10-19
                                if (sectionCurrent_mA > 5)
                                {
                                    sectionCurrent_mA = 5;
                                }
                                //end
                            }
                        }
                        else
                        {
                            sectionCurrent_mA = 0;
                        }
                        break;
                    default:
                        throw new ArgumentException("Unknown DSDBR section!");
                }
                return sectionCurrent_mA;
            }  // End if fcu2asic
            else
            {
                double sectionCurrent_mA = 0;
                switch (Section)
                {
                    case DSDBRSection.Gain:
                        sectionCurrent_mA = (double)fcuInstrs.FullbandControlUnit.GetIVSource(IVSourceIndexNumbers.Gain);
                        break;
                    case DSDBRSection.SOA:
                        sectionCurrent_mA = fcuInstrs.SoaCurrentSource.CurrentActual_amp * 1000;
                        break;
                    case DSDBRSection.Rear:
                        sectionCurrent_mA = (double)fcuInstrs.FullbandControlUnit.GetIVSource(IVSourceIndexNumbers.Rear);
                        break;
                    case DSDBRSection.Phase:
                        sectionCurrent_mA = (double)fcuInstrs.FullbandControlUnit.GetIVSource(IVSourceIndexNumbers.Phase);
                        break;
                    case DSDBRSection.Front1:
                    case DSDBRSection.Front2:
                    case DSDBRSection.Front3:
                    case DSDBRSection.Front4:
                    case DSDBRSection.Front5:
                    case DSDBRSection.Front6:
                    case DSDBRSection.Front7:
                    case DSDBRSection.Front8:
                        int frontSection = int.Parse(Section.ToString().Replace("Front", ""));
                        int frontPairUsed = fcuInstrs.FullbandControlUnit.GetFrontSectionPair();
                        if (frontSection == frontPairUsed || frontSection == frontPairUsed + 1)
                        {
                            if (frontSection % 2 == 0)
                            {
                                sectionCurrent_mA = (double)fcuInstrs.FullbandControlUnit.GetIVSource(IVSourceIndexNumbers.FrontEven);
                                //Add by tim for FrontSection negtive at 2008-08-22
                                if (sectionCurrent_mA < 0)
                                {
                                    sectionCurrent_mA = 0;
                                }
                                //end
                            }
                            else
                            {
                                sectionCurrent_mA = (double)fcuInstrs.FullbandControlUnit.GetIVSource(IVSourceIndexNumbers.FrontOdd);
                                //Add by tim for FrontSection negtive at 2008-08-22
                                if (sectionCurrent_mA < 0)
                                {
                                    sectionCurrent_mA = 0;
                                }
                                //end
                            }
                        }
                        else
                        {
                            sectionCurrent_mA = 0;
                        }
                        break;
                    default:
                        throw new ArgumentException("Unknown DSDBR section!");
                }
                return sectionCurrent_mA;
            }
        }

        /// <summary>
        /// Read the var & fix, return the power ratio - chongjian.liang 2013.4.22 
        /// </summary>
        /// <param name="isTunePotValue"></param>
        /// <param name="isRestoreToPotValueBeforeTune"></param>
        /// <param name="isAverageValue"></param>
        /// <returns></returns>
        public static double ReadPowerRatio(bool isSwitchIOLine, bool isTunePotValue, bool isRestoreToPotValueBeforeTune, bool isAverageValue)
        {
            // Switch IO line
            if (isSwitchIOLine)
            {
                opticalSwitch.GetDigiIoLine(IlmzOpticalSwitchLines.Etalon_Ref_DigiIoLine).LineState = IlmzOpticalSwitchLines.Etalon_Ref_DigiIoLine_State;
            }

            double var_mA = 0;
            double fix_mA = 0;

            if (DsdbrUtils.dsdbrDriverInstrsToUse == DsdbrDriveInstruments.FCU2AsicInstrument)
            {
                var_mA = fcu2AsicInstrs.Fcu2Asic.Var_Port_mA(isTunePotValue, isRestoreToPotValueBeforeTune, isAverageValue);
                fix_mA = fcu2AsicInstrs.Fcu2Asic.Fix_Port_mA(isAverageValue);
            }

            return fix_mA == 0 ? 0 : var_mA / fix_mA;
        }

        /// <summary>
        /// The default ReadLockerCurrents function: Tune pot values before reading dut locker currents, resume the pot values after reading dut locker currents. - chongjian.liang 2013.6.11
        /// </summary>
        /// <returns>Locker currents in mA and lock ratio</returns>
        public static LockerCurrents ReadLockerCurrents()
        {
            return ReadLockerCurrents(true, false, true, true, true);
        }

        /// <summary>
        /// Read and average the locker currents, return the locker ratio - chongjian.liang 2013.4.22
        /// </summary>
        /// <param name="isSwitchIOLine">If needs to switch to DUT or optical box Tx & Rx line</param>
        /// <param name="isUseOpticalBoxEtalon">If use the optical box, the locker ratio is Rx/Tx</param>
        /// <param name="isTunePotValue">Tune pot value before reading locker current, store the pot value before tuning</param>
        /// <param name="isRestoreToPotValueBeforeTune">After reading locker current, restore the pot value to the last pot values before tuning</param>
        /// <returns>Locker currents in mA and lock ratio</returns>
        public static LockerCurrents ReadLockerCurrents(bool isSwitchIOLine, bool isUseOpticalBoxEtalon, bool isTunePotValue, bool isRestoreToPotValueBeforeTune, bool isAverageValue)
        {
            if (DsdbrUtils.dsdbrDriverInstrsToUse == DsdbrDriveInstruments.FCU2AsicInstrument)
            {
                // Switch IO line
                if (isSwitchIOLine && DsdbrUtils.opticalSwitch != null)
                {
                    if (isUseOpticalBoxEtalon)
                    {
                        opticalSwitch.GetDigiIoLine(IlmzOpticalSwitchLines.Etalon_Tx_DigiIoLine).LineState = IlmzOpticalSwitchLines.Etalon_Tx_DigiIoLine_State;
                        opticalSwitch.GetDigiIoLine(IlmzOpticalSwitchLines.Etalon_Rx_DigiIoLine).LineState = IlmzOpticalSwitchLines.Etalon_Rx_DigiIoLine_State;
                    }
                    else
                    {
                        opticalSwitch.GetDigiIoLine(IlmzOpticalSwitchLines.Dut_Tx_DigiIoLine).LineState = IlmzOpticalSwitchLines.Dut_Tx_DigiIoLine_State;
                        opticalSwitch.GetDigiIoLine(IlmzOpticalSwitchLines.Dut_Rx_DigiIoLine).LineState = IlmzOpticalSwitchLines.Dut_Rx_DigiIoLine_State;
                    }
                }

                // Read Tx & Rx currents

                LockerCurrents lockerCurrents = new LockerCurrents();

                lockerCurrents.TxCurrent_mA = fcu2AsicInstrs.Fcu2Asic.Locker_Tx_mA(isTunePotValue, isRestoreToPotValueBeforeTune, isAverageValue);
                lockerCurrents.RxCurrent_mA = fcu2AsicInstrs.Fcu2Asic.Locker_Rx_mA(isTunePotValue, isRestoreToPotValueBeforeTune, isAverageValue);

                // Calc LockRatio
                if (!isUseOpticalBoxEtalon)
                {
                    lockerCurrents.LockRatio = lockerCurrents.RxCurrent_mA == 0 ? 0 : lockerCurrents.TxCurrent_mA / lockerCurrents.RxCurrent_mA;
                }
                else
                {
                    lockerCurrents.LockRatio = lockerCurrents.TxCurrent_mA == 0 ? 0 : lockerCurrents.RxCurrent_mA / lockerCurrents.TxCurrent_mA;
                }

                return lockerCurrents;
            }
            else if (DsdbrUtils.dsdbrDriverInstrsToUse == DsdbrDriveInstruments.PXIInstruments)
            {
                // Get locker
                CloseGridWrapper cgw = CloseGridWrapper.Singleton;//Jack.Zhang
                LockerCurrents lc = new LockerCurrents();
                lc.TxCurrent_mA = 1;
                lc.RxCurrent_mA = 1;

                // Read and accumulate current measurements 
                for (int ii = 0; ii < lockerAvgCount; ii++)
                {
                    //lc.TxCurrent_mA += cgw.ReadLockerCurrent_mA(LockerCurrentHead.Transmit);
                    //lc.RxCurrent_mA += cgw.ReadLockerCurrent_mA(LockerCurrentHead.Reflect);
                    Thread.Sleep(lockerDelay_mS);
                }

                // Return averaged count. Invert to return positive current.
                lc.TxCurrent_mA = -lc.TxCurrent_mA / lockerAvgCount;
                lc.RxCurrent_mA = -lc.RxCurrent_mA / lockerAvgCount;
                lc.LockRatio = lc.TxCurrent_mA / lc.RxCurrent_mA;
                return lc;
            }
            else
            {
                LockerCurrents lc = new LockerCurrents();
                lc.TxCurrent_mA = 0;
                lc.RxCurrent_mA = 0;
                double txCurrent_mA = 0;
                double rxCurrent_mA = 0;

                // Read and accumulate current measurements 
                for (int ii = 0; ii < lockerAvgCount; ii++)
                {
                    rxCurrent_mA += (fcuInstrs.FullbandControlUnit.GetLockerRefMon_ADC() - fcuInstrs.FCUCalibrationData.RxADC_CalOffset) / fcuInstrs.FCUCalibrationData.RxADC_CalFactor;
                    txCurrent_mA += (fcuInstrs.FullbandControlUnit.GetLockerTxMon_ADC() - fcuInstrs.FCUCalibrationData.TxADC_CalOffset) / fcuInstrs.FCUCalibrationData.TxADC_CalFactor;
                    Thread.Sleep(lockerDelay_mS);
                }

                // Return averaged count. Invert to return positive current.
                lc.TxCurrent_mA = txCurrent_mA;
                lc.RxCurrent_mA = rxCurrent_mA;
                lc.TxCurrent_mA = lc.TxCurrent_mA / lockerAvgCount;
                lc.RxCurrent_mA = lc.RxCurrent_mA / lockerAvgCount;
                lc.LockRatio = lc.TxCurrent_mA / lc.RxCurrent_mA;
                return lc;
            }
        }
       
        /// <summary>
        /// Set locker bias voltage
        /// </summary>
        /// <param name="locker">Locker name</param>
        /// <param name="bias_V">Locker bias voltage</param>
        /// <returns>Locker current in mA</returns>
        public static void SetLockerBiasVoltage_V(LockerId locker, double bias_V)
        {
            // Get locker
            InstType_ElectricalSource source;
            switch (locker)
            {
                case LockerId.TX:
                    source = (InstType_ElectricalSource)pxiInstrs.LockerTx;
                    break;
                case LockerId.RX:
                    source = (InstType_ElectricalSource)pxiInstrs.LockerRx;
                    break;
                default:
                    throw new Exception("Unknown locker enum: '" + locker.ToString() + "'");
            }

            // Set bias voltage
            source.VoltageSetPoint_Volt = bias_V;
        }

        #region Public Data Structures
        /// <summary>
        /// Locker currents
        /// </summary>
        public struct LockerCurrents
        {
            /// <summary>
            /// TX locker current mA
            /// </summary>
            public double TxCurrent_mA;
            /// <summary>
            /// RX locker current mA
            /// </summary>
            public double RxCurrent_mA;
            /// <summary>
            /// Locker ratio
            /// </summary>
            public double LockRatio;
        }
        #endregion

        #region Private Data

        private static DsdbrInstruments pxiInstrs;
        private static FCUInstruments fcuInstrs;

        /// <summary>
        /// FCU TO Asic intrtrument groups, 
        /// Fcu control Asic, 
        /// Asic source dsdbr sections 
        /// Asic has an power supply
        /// </summary>
        // added by Alice.Huang, add FCU2ASIC option for TOSA GB test
        // on 2010-01-27

        private static FCU2AsicInstruments fcu2AsicInstrs;
        private static DsdbrDriveInstruments dsdbrDriverInstrsToUse;

        private const int lockerAvgCount = 5;
        private const int lockerDelay_mS = 2;
        private const double C = 299792458;
        public static IInstType_DigiIOCollection opticalSwitch = null;
        public static double locker_port = 0;
        #endregion


    }
}

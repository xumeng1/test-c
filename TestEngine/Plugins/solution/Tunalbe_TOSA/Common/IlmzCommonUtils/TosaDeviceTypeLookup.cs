using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.IO;

namespace Bookham.TestSolution.IlmzCommonUtils
{
    /// <summary>
    /// Struct for device type lookup item
    /// </summary>
    public struct DeviceTypeLookupItem
    {
        private string chirpType;

        public string ChirpType
        {
            get { return this.chirpType; }
            set { this.chirpType = value; }
        }

        private string deviceType;

        public string DeviceType
        {
            get { return this.deviceType; }
            set { this.deviceType = value; }
        }
    }

    /// <summary>
    /// Class for utility of device type lookup
    /// </summary>
    public class TosaDeviceTypeConfig
    {
        /// <summary>
        /// Load device type lookup file
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static Dictionary<string,DeviceTypeLookupItem> LoadDeviceTypeLookup(string filename)
        {
            StreamReader reader = new StreamReader(filename);
            Dictionary<string, DeviceTypeLookupItem> list = new Dictionary<string, DeviceTypeLookupItem>();

            // skip the header line.
            reader.ReadLine();

            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine();

                string[] fields = line.Split(new char [] {','});

                if ( fields.Length >= 2 )
                {
                    DeviceTypeLookupItem item = new DeviceTypeLookupItem();
                    item.DeviceType = fields[0].ToUpper();
                    item.ChirpType = fields[1].ToUpper();
                    list.Add(item.DeviceType, item);
                }
            }
            reader.Close();
            return list;
        }
    }

    /// <summary>
    /// Read string from *.txt file
    /// </summary>
    public class ReadTxtFile
    {

        /// <summary>
        ///  Read text context
        /// </summary>
        /// <param name="filePathName"></param>
        /// <returns></returns>
        public static string ReadTxt(string filePathName)
        {

            string titleString = null;

            if (File.Exists(filePathName))
            {
                using (StreamReader sr = File.OpenText(filePathName))
                {
                    string tempString;
                    while ((tempString = sr.ReadLine()) != null)
                    {
                        titleString = tempString;
                    }
                }
            }
            return titleString;
        }

    }
}

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.Algorithms;
using Bookham.TestLibrary.Instruments;
using Bookham.TestEngine.PluginInterfaces.Instrument;

namespace Bookham.TestSolution.IlmzCommonUtils
{
    /// <summary>
    /// Class to measure SupermodeSR from an OSA and return value in dBm.
    /// </summary>
    public static class SupermodeSRMeasurement
    {
        /// <summary>
        /// Measure supermodeSR, return value in dBm.
        /// </summary>
        /// <param name="supermodeSRCentreOpticalFreq_GHz">Centre optical frequency</param>
        /// <returns>supermodeSR in dBm</returns>
        public static double MeasureSupermodeSR(double supermodeSRCentreOpticalFreq_GHz)
        {
            double supermodeSR_dBm;
            try
            {
                //alice  add this part to lock instrument and switch optical path if osa shared
                if (TcmzOSAshare.OsaShareFlag)
                {
                    // if the instrument can't lock in share application, throw an exception
                    bool lockflag;
                    int lockCount = 0;
                    do
                    {
                        lockflag = TcmzOSAshare.LockInstruments();
                        lockCount++;
                    }
                    while ((!lockflag) && (lockCount < 3));

                    if (!lockflag)
                    {
                        throw new InstrumentException("can't lock instrument" +
                            "\nplease check if shared instrument used by other test kit without release" +
                            " or instrument lock file is exist in the setting path");
                    }
                    TcmzOSAshare.SetDioToMeasurement();
                    // OSW min switch time : 10~ 15ms
                    // + DIO output stable time
                    System.Threading.Thread.Sleep(50);
                }
            //check/set OSA sweepmode to single
            if (SupermodeSRMeasurement.osa.SweepMode != InstType_OSA.SweepModes.Single)
            {
                SupermodeSRMeasurement.osa.SweepMode = InstType_OSA.SweepModes.Single;
            }

            // Set OSA sensitivity if Agilent Ag8164x
            Inst_Ag8614xA Ag8616xOsa = null;
            if (SupermodeSRMeasurement.osa.DriverName.ToLower().Contains("ag8614"))
            {
                Ag8616xOsa = (Inst_Ag8614xA)SupermodeSRMeasurement.osa;
                Ag8616xOsa.Sensitivity_dBm = SupermodeSRMeasurement.supermodeSROSASensitivity_dBm;
                Ag8616xOsa.ScreenTitle = "Measuring Supermode SR";
            }

            double centreWL = Alg_FreqWlConvert.Freq_GHz_TO_Wave_nm(supermodeSRCentreOpticalFreq_GHz);
            SupermodeSRMeasurement.osa.WavelengthStart_nm = centreWL - SupermodeSRMeasurement.supermodeSRSpan_nm/2;
            SupermodeSRMeasurement.osa.WavelengthStop_nm = centreWL + SupermodeSRMeasurement.supermodeSRSpan_nm/2;
            SupermodeSRMeasurement.osa.ResolutionBandwidth = SupermodeSRMeasurement.supermodeSRRBW_nm;
            SupermodeSRMeasurement.osa.TracePoints = SupermodeSRMeasurement.supermodeSRNumPts;

            double peakPower_dBm;
            double nextPeakPower_dBm;
            double peakWL_nm;
            double nextPeakWL_nm;
            int nextPeakCount = 0;
            SupermodeSRMeasurement.osa.SweepStart();
            do
            {
                System.Threading.Thread.Sleep(100);
            } while (SupermodeSRMeasurement.osa.Status != InstType_OSA.ChannelState.DataReady && SupermodeSRMeasurement.osa.Status != InstType_OSA.ChannelState.Idle);

            SupermodeSRMeasurement.osa.MarkerToPeak();
            peakPower_dBm = SupermodeSRMeasurement.osa.MarkerAmplitude_dBm;
            peakWL_nm = SupermodeSRMeasurement.osa.MarkerWavelength_nm;

            do
            {
                SupermodeSRMeasurement.osa.MarkerToNextPeak(InstType_OSA.Direction.Unspecified);
                nextPeakWL_nm = SupermodeSRMeasurement.osa.MarkerWavelength_nm;
                nextPeakCount++;
            } while (nextPeakCount < 3 && (Math.Abs(nextPeakWL_nm - peakWL_nm) < SupermodeSRMeasurement.supermodeSRExclude_nm));

            //if more than 3 peaks in supermodeSR exclusion?
            //...

            nextPeakPower_dBm = SupermodeSRMeasurement.osa.MarkerAmplitude_dBm;

            supermodeSR_dBm = peakPower_dBm - nextPeakPower_dBm;

            if (Ag8616xOsa != null) 
                Ag8616xOsa.ScreenTitle = "Supermode SR " + supermodeSR_dBm.ToString("0.0") + "dB";
        }
        catch (DirectoryNotFoundException e)
        {
            throw e;
        }
        catch (InstrumentException e)
        {
            throw e;
        }
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            if (TcmzOSAshare.OsaShareFlag)
            {
                if (TcmzOSAshare.DioRecoverFlag) TcmzOSAshare.RecoverDIO();
                TcmzOSAshare.UnlockInstruments();
            }

        }
            return supermodeSR_dBm;
        }

        #region Private data
        private static IInstType_OSA osa;
        private static double supermodeSRSpan_nm;
        private static double supermodeSRRBW_nm;
        private static double supermodeSROSASensitivity_dBm;
        private static int supermodeSRNumPts;
        //private double supermodeSRSweepTime_s;
        private static double supermodeSRExclude_nm;
        #endregion

        #region Public properties
        /// <summary>
        /// OSA
        /// </summary>
        public static IInstType_OSA OSA
        {
            get { return osa; }
            set { osa = value; }
        }

        /// <summary>
        /// Span - wide span to observe supermodes
        /// </summary>
        public static double SupermodeSRSpan_nm
        {
            get { return supermodeSRSpan_nm; }
            set { supermodeSRSpan_nm = value; }
        }

        /// <summary>
        /// Resolution bandwidth
        /// </summary>
        public static double SupermodeSRRBW_nm
        {
            get { return supermodeSRRBW_nm; }
            set { supermodeSRRBW_nm = value; }
        }

        /// <summary>
        /// OSA sensitivity setting - set to resolve lower peaks
        /// </summary>
        public static double SupermodeSROSASensitivity_dBm
        {
            get { return supermodeSROSASensitivity_dBm; }
            set { supermodeSROSASensitivity_dBm = value; }
        }

        /// <summary>
        /// Number of trace points
        /// </summary>
        public static int SupermodeSRNumPts
        {
            get { return supermodeSRNumPts; }
            set { supermodeSRNumPts = value; }
        }

        /// <summary>
        /// Exclusion region around carrier
        /// </summary>
        public static double SupermodeSRExclude_nm
        {
            get { return supermodeSRExclude_nm; }
            set { supermodeSRExclude_nm = value; }
        }
        #endregion
    }
}
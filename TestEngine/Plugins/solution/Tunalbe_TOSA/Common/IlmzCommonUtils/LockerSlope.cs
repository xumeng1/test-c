// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// LockerSlope.cs
//
// Author: Tony Foster, 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestSolution.IlmzCommonData;

namespace Bookham.TestSolution.IlmzCommonUtils
{
    /// <summary>
    /// Locker slope
    /// </summary>
    public static class LockerSlope
    {
        /// <summary>
        /// Locker slope measurement with +/-3GHz offset from centre frequency
        /// </summary>
        /// <returns>Locker slope</returns>
        public static DatumList MeasureLockerSlope()
        {
            DatumList rtn = new DatumList();
            rtn = MeasureLockerSlope(freqDeltaFromCentre_GHz);
            return rtn;
        }

        /// <summary>
        /// measure locker slope with special frequency offset from centre frequency 
        /// </summary>
        /// <param name="freqOffset_GHz"></param>
        /// <returns></returns>
        public static DatumList MeasureLockerSlope(double freqOffset_GHz)
        {
            // Measurements at center frequency
            //double cntFreq_GHz = Measurements.ReadFrequency_GHz();
            double cntFreq_GHz = Measurements.FrequencyWithoutMeter;
            double cntIphase = DsdbrUtils.ReadSectionCurrent_mA(DSDBRSection.Phase);
            DsdbrUtils.LockerCurrents cntLockers = DsdbrUtils.ReadLockerCurrents();

            bool phaseTuneOk_Low = false;
            bool phaseTuneOk_High = false;
            double slopeEfficiency = 0;
            double phaseTuningEfficiency = 0;
            double phaseRatioSlopeEfficiency = 0;
            double RatioSlope = 0;  //raul added for temp
            double pte = 0;
            double lowIphase = 0;
            double lowFreq_GHz = 0;
            double highIphase = 0;
            double highFreq_GHz = 0;
            string PTE_Information="";
            string ITU3GHzOffset_Info = null;

            #region  Measurements at low frequency

            DatumList phaseResultLow = DsdbrTuning.PhaseTune(cntFreq_GHz - freqOffset_GHz, pte);
            lowFreq_GHz = phaseResultLow.ReadDouble("Freq_GHz");
            lowIphase = phaseResultLow.ReadDouble("PhaseCurrent_mA");
            phaseTuneOk_Low = phaseResultLow.ReadBool("TuneOK");
            DsdbrUtils.LockerCurrents lowLockers = DsdbrUtils.ReadLockerCurrents();
            PTE_Information = PTE_Information + "-" + freqOffset_GHz.ToString() + "GHz_" + phaseResultLow.ReadString("EOL_Testing_Info");
            ITU3GHzOffset_Info = string.Format("[-3GHz: TX_mA:{0} RX_mA:{1}]", lowLockers.TxCurrent_mA, lowLockers.RxCurrent_mA);
            #endregion

            DatumList phaseResultHigh=new DatumList();

            if (phaseTuneOk_Low)
            {
                DsdbrUtils.SetSectionCurrent_mA(DSDBRSection.Phase, 2 * cntIphase - lowIphase);

                #region Measurements at high frequency

                phaseResultHigh = DsdbrTuning.PhaseTune(cntFreq_GHz + freqOffset_GHz, pte);
                highFreq_GHz = phaseResultHigh.ReadDouble("Freq_GHz");
                highIphase = phaseResultHigh.ReadDouble("PhaseCurrent_mA");
                phaseTuneOk_High = phaseResultHigh.ReadBool("TuneOK");
                DsdbrUtils.LockerCurrents highLockers = DsdbrUtils.ReadLockerCurrents();
                PTE_Information = PTE_Information + "+" + freqOffset_GHz.ToString() + "GHz_" + phaseResultHigh.ReadString("EOL_Testing_Info");
                ITU3GHzOffset_Info += string.Format("[+3GHz: TX_mA:{0} RX_mA:{1}]", highLockers.TxCurrent_mA, highLockers.RxCurrent_mA);
                #endregion

                // Calculate return params
                if ((highFreq_GHz - lowFreq_GHz != 0) || (lowIphase - highIphase != 0))
                {
                    if (cntLockers.TxCurrent_mA + cntLockers.RxCurrent_mA != 0)
                    {
                        #region Comment below old formulas for calculating Locker Slope - chongjian.liang 6.6
                        //RatioSlope = lowLockers.TxCurrent_mA - lowLockers.RxCurrent_mA;
                        //RatioSlope -= highLockers.TxCurrent_mA - highLockers.RxCurrent_mA;
                        //RatioSlope /= 0.5 * (cntLockers.TxCurrent_mA + cntLockers.RxCurrent_mA);
                        //RatioSlope /= lowFreq_GHz - highFreq_GHz;
                        //RatioSlope *= 100;

                        //added for temp
                        //slopeEfficiency = lowLockers.LockRatio - highLockers.LockRatio;
                        //slopeEfficiency /= lowLockers.LockRatio + highLockers.LockRatio;
                        //slopeEfficiency /= lowFreq_GHz - highFreq_GHz;
                        //slopeEfficiency *= 100; 
                        #endregion

                        // Use new Locker Slope formula to be consistant with Black box formula - chongjian.liang 2013.6.6
                        double highLockErr = (highLockers.RxCurrent_mA - highLockers.TxCurrent_mA) / (highLockers.RxCurrent_mA + highLockers.TxCurrent_mA);
                        double lowLockErr = (lowLockers.RxCurrent_mA - lowLockers.TxCurrent_mA) / (lowLockers.RxCurrent_mA + lowLockers.TxCurrent_mA);
                        slopeEfficiency = 100 * ((highLockErr - lowLockErr) / (highFreq_GHz - lowFreq_GHz));
                    }

                    phaseTuningEfficiency = (lowIphase - highIphase) / (lowFreq_GHz - highFreq_GHz);
                }

                if (lowLockers.RxCurrent_mA != 0 && lowLockers.LockRatio - highLockers.LockRatio != 0)
                {
                    phaseRatioSlopeEfficiency = (lowIphase - highIphase) / (lowLockers.LockRatio - highLockers.LockRatio);
                }
            }

            // Return to center frequency
            DsdbrUtils.SetSectionCurrent_mA(DSDBRSection.Phase, cntIphase);

            // Return results
            DatumList rtn = new DatumList();
            // No need to record the MZ_CSR_POWER_RATIO, per Stream's request - chongjian.liang 2013.5.10
            //rtn.AddDouble("MZ_CSR_POWER_RATIO", RatioSlope);
            rtn.AddDouble("LockerSlopeEff", slopeEfficiency);
            rtn.AddDouble("PhaseTuningEff", phaseTuningEfficiency);
            rtn.AddDouble("PhaseRatioSlopeEff", phaseRatioSlopeEfficiency);
            rtn.AddBool("LockSlopeTuneOk", phaseTuneOk_High && phaseTuneOk_Low);
            rtn.AddDouble("LowIphaseForLockerSlope", lowIphase);
            rtn.AddDouble("HighIphaseForLockerSlope", highIphase);
            rtn.AddDouble("LowFreqForLockerSlope", lowFreq_GHz);
            rtn.AddDouble("HighFreqForLockerSlope", highFreq_GHz);
            rtn.AddString("EOL_Testing_Info", PTE_Information);
            rtn.AddString("ITU3GHzOffset_Info", ITU3GHzOffset_Info);

            return rtn;
        }

        #region Private Data
        private const double freqDeltaFromCentre_GHz = 3.0;
        #endregion

    }
}

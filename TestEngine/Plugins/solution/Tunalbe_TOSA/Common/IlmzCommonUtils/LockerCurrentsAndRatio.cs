// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// LockerCurrentsAndRatio.cs
//
// Author: Mark Fullalove, 2007
// Design: [Reference design documentation]

using System;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestSolution.TcmzCommonInstrs;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Static class to perform locker currents and ratio measurements
    /// </summary>
    public static class LockerCurrentsAndRatio
    {
        /// <summary>
        /// Measure locker currents and calculate their ratio.
        /// </summary>
        /// <param name="lockerBias_V">The bias to be applied to both lockers.</param>
        /// <param name="instruments">The collection of instruments</param>
        /// <returns>A datumlist containing locker currents and locker ratio</returns>
        public static DatumList MeasureAtSingleChannel(double lockerBias_V, DsdbrInstruments instruments )
        {
            // Setup
            instruments.LockerRx.VoltageSetPoint_Volt = lockerBias_V;
            instruments.LockerTx.VoltageSetPoint_Volt = lockerBias_V;

            // Measure
            double TxLock_mA = instruments.LockerTx.CurrentActual_amp;
            double RxLock_mA = instruments.LockerRx.CurrentActual_amp;
            double lockRatio = RxLock_mA != 0 ? TxLock_mA / RxLock_mA : double.PositiveInfinity;

            // Create results container
            DatumList SingleChannelResults = new DatumList();

            // Store results
            SingleChannelResults.AddDouble("ItxLock", TxLock_mA);
            SingleChannelResults.AddDouble("IrxLock", RxLock_mA);
            SingleChannelResults.AddDouble("LockRatio", lockRatio);

            return SingleChannelResults;
        }
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using ICSharpCode.SharpZipLib.Zip;
using System.IO;

namespace Bookham.TestLibrary.Utilities
{
    /// <summary>
    /// Class to create and add to a zip file.
    /// </summary>
    public class Util_ZipFile : IDisposable
    {
        private ZipOutputStream zipStream;
        byte[] buffer;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="zipFileName">The name of the zip archive file.</param>
        public Util_ZipFile(string zipFileName)
        {
            // Open or Create
            zipStream = new ZipOutputStream(File.Open(zipFileName, FileMode.OpenOrCreate));
            zipStream.SetLevel(9); // 0 - store only to 9 - means best compression
            buffer = new byte[4096];
        }

        /// <summary>
        /// Sets the compression level ( the default is 9 )
        /// </summary>
        /// <param name="compressionLevel">0=fast, store only to 9=slow, maximum compression</param>
        public void SetCompressionLevel(int compressionLevel)
        {
            zipStream.SetLevel(compressionLevel);
        }

        /// <summary>
        /// Adds a file to our zip file
        /// </summary>
        /// <param name="fileToZip">file name to add (relative path)</param>
        public void AddFileToZip(string fileToZip)
        {
            if (!File.Exists(fileToZip))
                return;
            ZipEntry entry = new ZipEntry(Path.GetFileName(fileToZip));
            zipStream.PutNextEntry(entry);
            using (FileStream fs = File.OpenRead(fileToZip))
            {
                // Using a fixed size buffer here makes no noticeable difference for output
                // but keeps a lid on memory usage.
                int sourceBytes;
                do
                {
                    sourceBytes = fs.Read(buffer, 0, buffer.Length);
                    zipStream.Write(buffer, 0, sourceBytes);
                } while (sourceBytes > 0);
            }
        }

        /// <summary>
        /// Add files to the zip file
        /// </summary>
        /// <param name="filesToZip"></param>
        public void AddFilesToZip(List<string> filesToZip)
        {
            foreach (string fileToZip in filesToZip)
            {
                AddFileToZip(fileToZip);
            }
        }

        /// <summary>
        /// Zip files
        /// </summary>
        public static void ZipFiles(string zipFilePath, List<string> filesToZipPath, int compressionLevel)
        {
            if (filesToZipPath.Count > 0)
            {
                using (Util_ZipFile zipFile = new Util_ZipFile(zipFilePath))
                {
                    zipFile.SetCompressionLevel(compressionLevel);

                    foreach (string filePathOfFileToAdd in filesToZipPath)
                    {
                        zipFile.AddFileToZip(filePathOfFileToAdd);
                    }
                }
            }
        }

        /// <summary>
        /// Zip files with compression level as 5
        /// </summary>
        public static void ZipFiles(string zipFilePath, List<string> filesToZipPath)
        {
            // 5 is reasonably fast with good compression
            ZipFiles(zipFilePath, filesToZipPath, 5);
        }

        #region IDisposable Members

        void IDisposable.Dispose()
        {
            if (zipStream != null)
            {
                zipStream.Finish();
                zipStream.Close();
            }
        }

        #endregion
    }

    /// <summary>
    /// Class to de-compress a zip file
    /// </summary>
    public class Util_UnZipFile : IDisposable
    {
        private ZipInputStream zipInput;
        byte[] buffer;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="zipFilename"></param>
        public Util_UnZipFile(string zipFilename)
        {
            zipInput = new ZipInputStream(File.OpenRead(zipFilename));
            buffer = new byte[4096];
        }

        /// <summary>
        /// De-compress the zip file to the destination folder
        /// </summary>
        /// <param name="desPath"></param>
        public void UnZipFile(string desPath)
        {
            ZipEntry theEntry;
            while ((theEntry = zipInput.GetNextEntry()) != null)
            {
                string directoryName = desPath + "\\";
                string fileName = Path.GetFileName(theEntry.Name);

                if (!Directory.Exists(directoryName))
                    Directory.CreateDirectory(directoryName);

                if (fileName != String.Empty)
                {
                    string curPath = directoryName + theEntry.Name;
                    string curDir = Path.GetDirectoryName(curPath);
                    if (!Directory.Exists(curDir))
                        Directory.CreateDirectory(curDir);

                    FileStream streamWriter = File.Create(curPath);

                    int size = 2048;
                    while (true)
                    {
                        size = zipInput.Read(buffer, 0, buffer.Length);
                        if (size > 0)
                        {
                            streamWriter.Write(buffer, 0, size);
                        }
                        else
                        {
                            break;
                        }
                    }

                    streamWriter.Close();
                }
            }
            zipInput.Close();
        }

        #region IDisposable Members

        /// <summary>
        /// Dispose function
        /// </summary>
        public void Dispose()
        {
            if (zipInput != null)
            {
                zipInput.Close();
            }
        }

        #endregion
    }
}

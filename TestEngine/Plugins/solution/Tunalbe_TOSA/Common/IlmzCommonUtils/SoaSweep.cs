// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// SoaSweep.cs
//
// Author: Mark Fullalove, 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.Toolkit.CloseGrid;//jack.Zhang
using Bookham.TestSolution.IlmzCommonData;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestSolution.IlmzCommonInstrs;
using Bookham.TestLibrary.Algorithms;
using Bookham.TestLibrary.Utilities;
using System.IO;

namespace Bookham.TestSolution.IlmzCommonUtils
{
    /// <summary>
    /// Static class to perform SOA sweep test
    /// </summary>
    public static class SoaSweep
    {
        #region Private help functions
        /// <summary>
        /// Measure the fibre power at a fixed SOA current.
        /// Default power meter head is OpmCgDirect
        /// </summary>
        /// <param name="soaCurrent_mA">SOA current in mA</param>
        /// <param name="delayTime_mS">Delay time between setting soa current and measuring fibre power</param>
        /// <returns>Fibre power in dBm</returns>
        private static double MeasurePowerAtSinglePoint(double soaCurrent_mA, int delayTime_mS)
        {
            DsdbrUtils.SetSectionCurrent_mA(DSDBRSection.SOA, soaCurrent_mA);
            System.Threading.Thread.Sleep(delayTime_mS);

            double fibrePwr_dBm;
            if (DsdbrUtils.DsdbrDriverInstrsToUse == DsdbrDriveInstruments.PXIInstruments)
            {
                fibrePwr_dBm = Measurements.ReadOpticalPower(OpticalPowerHead.OpmCgDirect, Measurements.PowerUnits.dBm);
            }
            else
            {
                fibrePwr_dBm = Measurements.ReadOpticalPower(OpticalPowerHead.MZHead, Measurements.PowerUnits.dBm);
            }

            return fibrePwr_dBm;
        }

        /// <summary>
        /// Writes the plot data to a file
        /// </summary>
        /// <param name="power">Array of power</param>
        /// <param name="soaCurrent">Array of SOA current</param>
        /// <param name="plotFileDirectory">The directory in which to write the file</param>
        /// <param name="plotFileQuantifier">Identifies this file from those of other channels or devices</param>
        /// <returns>The name of the file written</returns>
        private static string writeCSVdata(double[] power, double[] soaCurrent,
                            string plotFileDirectory, string plotFileQuantifier)
        {
            int nbrPoints = power.Length;

            string plotFileName = Util_GenerateFileName.GenWithTimestamp(plotFileDirectory, "SoaSweep", plotFileQuantifier, "csv");
            using (StreamWriter writer = new StreamWriter(plotFileName))
            {
                writer.WriteLine("SOA Current mA,Optical Power dBm");
                for (int i = 0; i < nbrPoints; i++)
                {
                    writer.WriteLine(soaCurrent[i] + "," + power[i]);
                }
            }
            return plotFileName;
        }
        /// <summary>
        /// Writes the plot data to a file
        /// </summary>
        /// <param name="power">Array of power</param>
        /// <param name="soaCurrent">Array of SOA current</param>
        /// <param name="soaDac"> Array of Soa Dac </param>
        /// <param name="plotFileDirectory">The directory in which to write the file</param>
        /// <param name="plotFileQuantifier">Identifies this file from those of other channels or devices</param>
        /// <returns>The name of the file written</returns>
        private static string writeCSVdata(double[] power, double[] soaCurrent, double[] soaDac,
                            string plotFileDirectory, string plotFileQuantifier)
        {
            int nbrPoints = power.Length;

            string plotFileName = Util_GenerateFileName.GenWithTimestamp(plotFileDirectory, "SoaSweep", plotFileQuantifier, "csv");
            using (StreamWriter writer = new StreamWriter(plotFileName))
            {
                writer.WriteLine("SOA Current mA,Optical Power dBm,Soa Dac");
                for (int i = 0; i < nbrPoints; i++)
                {
                    writer.WriteLine(soaCurrent[i] + "," + power[i] + "," + soaDac[i]);
                }
            }
            return plotFileName;
        }

        #endregion


        #region Public functions

        /// <summary>
        /// Measure the fibre power at the maximum SOA current
        /// </summary>
        /// <param name="maxSoaCurrent_mA">The maximum SOA current in mA</param>
        /// <param name="delayTime_mS">Delay time between setting soa current and measuring fibre power</param>
        /// <returns>Fibre power in dBm at max SOA current</returns>
        public static double MeasurePowerAtMaxSoa(double maxSoaCurrent_mA, int delayTime_mS)
        {
            // Record initial soa current
            double initialSoaCurrent = DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic.ISoa_mA;

            // Measure power at max soa
            double rtnPwr_dBm = MeasurePowerAtSinglePoint(maxSoaCurrent_mA, delayTime_mS);

            // Set soa to initial
            DsdbrUtils.SetSectionCurrent_mA(DSDBRSection.SOA, initialSoaCurrent);

            return rtnPwr_dBm;
        }

        /// <summary>
        /// Measure the fibre power at the minimum SOA current
        /// </summary>
        /// <param name="minSoaCurrent_mA">The minimum SOA current in mA</param>
        /// <param name="delayTime_mS">Delay time between setting soa current and measuring fibre power</param>
        /// <returns>Fibre power in dBm at min SOA current</returns>
        public static double MeasurePowerAtMinSoa(double minSoaCurrent_mA, int delayTime_mS)
        {
            // Record initial soa current
            double initialSoaCurrent = DsdbrUtils.ReadSectionCurrent_mA(DSDBRSection.SOA);

            // Measure power at min soa
            double rtnPwr_dBm = MeasurePowerAtSinglePoint(minSoaCurrent_mA, delayTime_mS);

            // Set soa to initial
            DsdbrUtils.SetSectionCurrent_mA(DSDBRSection.SOA, initialSoaCurrent);

            return rtnPwr_dBm;
        }

        /// <summary>
        /// Measure the fibre power at the minimum SOA current and maximum SOA current
        /// </summary>
        /// <param name="minSoaCurrent_mA">The minimum SOA current in mA</param>
        /// <param name="maxSoaCurrent_mA">The maximum SOA current in mA</param>
        /// <param name="delayTime_mS">Delay time between setting soa current and measuring fibre power</param>
        /// <returns>Fibre power in dBm at min and max SOA current</returns>
        public static DatumList MeasurePowerAtMinAndMaxSoa(double minSoaCurrent_mA,
                                                double maxSoaCurrent_mA, int delayTime_mS)
        {
            // Record initial soa current
            double initialSoaCurrent = DsdbrUtils.ReadSectionCurrent_mA(DSDBRSection.SOA);

            // Measure power at min/max soa
            DatumList rtnData = new DatumList();
            rtnData.AddDouble("SOA_MinPwr_dBm", MeasurePowerAtSinglePoint(minSoaCurrent_mA, delayTime_mS));
            rtnData.AddDouble("SOA_MaxPwr_dBm", MeasurePowerAtSinglePoint(maxSoaCurrent_mA, delayTime_mS));

            // Set soa to initial
            DsdbrUtils.SetSectionCurrent_mA(DSDBRSection.SOA, initialSoaCurrent);

            return rtnData;
        }

        /// <summary>
        /// Performs SOA sweep between 0 mA and maxSoaCurrent_mA in steps of 10 mA.
        /// </summary>
        /// <param name="minSoaCurrent_mA">Start level for fit</param>
        /// <param name="maxSoaCurrent_mA">Max current for sweep (MAX_SOA_I)</param>
        /// <param name="refSoaCurrent_mA">Reference current for fit</param>
        /// <param name="stepDelay_mS">Delay between each point of the sweep</param>
        /// <param name="plotFileDirectory">Directory in which to store plot files</param>
        /// <param name="plotFileQuantifier">Identifies this file from those of other channels or devices</param>
        /// <returns>Datumlist containing parametric results and sweep data.</returns>
        public static DatumList SweepAtSingleChannel(double minSoaCurrent_mA, 
                    double maxSoaCurrent_mA, double refSoaCurrent_mA, int stepDelay_mS,
                    string plotFileDirectory, string plotFileQuantifier)
        {
            OpticalPowerHead powerMeterHead;
            if (DsdbrUtils.DsdbrDriverInstrsToUse == DsdbrDriveInstruments.PXIInstruments)
            {
                powerMeterHead = OpticalPowerHead.OpmCgDirect;
            }
            else
            {
                powerMeterHead = OpticalPowerHead.MZHead;
            }

            // Setup
            double initialSoaCurrent = DsdbrUtils.ReadSectionCurrent_mA(DSDBRSection.SOA);
            ArrayList soaCurrentList = new ArrayList();
            ArrayList opticalPowerList = new ArrayList();
            double soaCurrent = 0;

            // Alice.Huang    add on 2010-02-18
            // add soa DAC trace
            int soaDac = 0;
            ArrayList soaDacList = new ArrayList();

            // Sweep
            for (soaCurrent = minSoaCurrent_mA; soaCurrent <= maxSoaCurrent_mA; soaCurrent += 10)
            {
                DsdbrUtils.SetSectionCurrent_mA(DSDBRSection.SOA, soaCurrent);
                System.Threading.Thread.Sleep(stepDelay_mS);

                soaCurrentList.Add(DsdbrUtils.ReadSectionCurrent_mA(DSDBRSection.SOA));
                opticalPowerList.Add(Measurements.ReadOpticalPower(powerMeterHead, Measurements.PowerUnits.dBm));

                if (DsdbrUtils.DsdbrDriverInstrsToUse == DsdbrDriveInstruments.FCU2AsicInstrument)
                {
                    soaDac = DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic.ISoa_Dac;
                    soaDacList.Add(soaDac);
                }
            }

            // Read at last point if maxSoaCurrent_mA is not divisible by 10
            if (soaCurrent < maxSoaCurrent_mA)
            {
                DsdbrUtils.SetSectionCurrent_mA(DSDBRSection.SOA, maxSoaCurrent_mA);
                System.Threading.Thread.Sleep(stepDelay_mS);

                soaCurrentList.Add(DsdbrUtils.ReadSectionCurrent_mA(DSDBRSection.SOA));
                opticalPowerList.Add(Measurements.ReadOpticalPower(powerMeterHead, Measurements.PowerUnits.dBm));

                if (DsdbrUtils.DsdbrDriverInstrsToUse == DsdbrDriveInstruments.FCU2AsicInstrument)
                {
                    soaDac = DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic.ISoa_Dac;
                    soaDacList.Add(soaDac);
                }
            }

            double[] soaCurrentArray = (double[])soaCurrentList.ToArray(typeof(Double));
            double[] powerArray = (double[])opticalPowerList.ToArray(typeof(Double));

            // Write plot data
            string plotFileName;

            // Alice.Huang  2010-02-18
            // If FCU2ASIC, record the SOA dac to csv file
            double[] soaDacArray=null;
            if (DsdbrUtils.DsdbrDriverInstrsToUse == DsdbrDriveInstruments.FCU2AsicInstrument)
            {
                soaDacArray = (double[])soaDacList.ToArray(typeof(double));
                plotFileName = writeCSVdata(powerArray, soaCurrentArray, soaDacArray,
                                            plotFileDirectory, plotFileQuantifier);
            }
            else             
            plotFileName = writeCSVdata(powerArray, soaCurrentArray, 
                                        plotFileDirectory, plotFileQuantifier);

            // Analyse
            double refSoaPower = XatYAlgorithm.Calculate(powerArray, soaCurrentArray, refSoaCurrent_mA);
#warning Awaiting clarification of fit calculation;
            double deltaI = 0;
            double deltaP = 0;
            double fitQuality = 0;

            // Create results container
            DatumList SingleChannelResults = new DatumList();

            // Store results
            SingleChannelResults.AddDouble("SOA_MinPwr_dBm", powerArray[0]);
            SingleChannelResults.AddDouble("SOA_MaxPwr_dBm", powerArray[powerArray.Length - 1]);
            SingleChannelResults.AddDoubleArray("SOA_mA", soaCurrentArray);
            SingleChannelResults.AddDoubleArray("FibrePower_dBm", powerArray);
            SingleChannelResults.AddDouble("SOA_Iref_mA", refSoaCurrent_mA);
            SingleChannelResults.AddDouble("SOA_Pref_dBm", refSoaPower);
            // Fit coefficients
            SingleChannelResults.AddDouble("SOA_DeltaI_mA", deltaI);
            SingleChannelResults.AddDouble("SOA_DeltaP_dB", deltaP);
            SingleChannelResults.AddDouble("SOA_fit_quality", fitQuality);
            // Plot data file
            SingleChannelResults.AddString("SOA_sweep_plot", plotFileName);
            if (DsdbrUtils.DsdbrDriverInstrsToUse == DsdbrDriveInstruments.FCU2AsicInstrument)
            {
                SingleChannelResults.AddSint32("SOA_Iref_Dac", DsdbrUtils.Fcu2AsicInstrumentGroup.Fcu2Asic.mAToDacForFrontSoa_Asic((float)refSoaCurrent_mA));
                SingleChannelResults.AddDoubleArray("SOA_DAC", soaDacArray);
                //SingleChannelResults.AddDouble("SOA_DeltaI_Dac", deltaI);
            }

            // Restore original conditions
            DsdbrUtils.SetSectionCurrent_mA(DSDBRSection.SOA, initialSoaCurrent);

            return SingleChannelResults;
        }

        /// <summary>
        /// Performs SOA sweep and measure fibre power.
        /// </summary>
        /// <param name="minSoaCurrent_mA">Start soa current in mA</param>
        /// <param name="maxSoaCurrent_mA">End soa current in mA</param>
        /// <param name="stepSoaCurrent_mA">Soa current step in mA</param>
        /// <param name="stepDelay_mS">Step delay time in mS</param>
        /// <returns>Soa current array and measured fibre power array</returns>
        public static DatumList TraceSoaSweep(double minSoaCurrent_mA, double maxSoaCurrent_mA, double stepSoaCurrent_mA, int stepDelay_mS)
        {
            OpticalPowerHead powerMeterHead;
            if (DsdbrUtils.DsdbrDriverInstrsToUse == DsdbrDriveInstruments.PXIInstruments)
            {
                powerMeterHead = OpticalPowerHead.OpmCgDirect;
            }
            else
            {
                powerMeterHead = OpticalPowerHead.MZHead;
            }

            // Setup
            double initialSoaCurrent = DsdbrUtils.ReadSectionCurrent_mA(DSDBRSection.SOA);
            ArrayList soaCurrentList = new ArrayList();
            ArrayList opticalPowerList = new ArrayList();
            double soaCurrent = 0;

            // Alice.Huang    add on 2010-02-18
            // add soa DAC trace
            int soaDac = 0;
            ArrayList soaDacList = new ArrayList();

            // Sweep
            for (soaCurrent = minSoaCurrent_mA; soaCurrent <= maxSoaCurrent_mA; soaCurrent += stepSoaCurrent_mA)
            {
                DsdbrUtils.SetSectionCurrent_mA(DSDBRSection.SOA, soaCurrent);
                System.Threading.Thread.Sleep(stepDelay_mS);

                soaCurrentList.Add(DsdbrUtils.ReadSectionCurrent_mA(DSDBRSection.SOA));
                opticalPowerList.Add(Measurements.ReadOpticalPower(powerMeterHead, Measurements.PowerUnits.dBm));

                if (DsdbrUtils.DsdbrDriverInstrsToUse == DsdbrDriveInstruments.FCU2AsicInstrument)
                {                   
                    soaDac = DsdbrUtils.Fcu2AsicInstrumentGroup .Fcu2Asic .ISoa_Dac;                   
                    soaDacList.Add(soaDac );
                }
            }

            // Read at last point if maxSoaCurrent_mA is not divisible by 10
            if (soaCurrent < maxSoaCurrent_mA)
            {
                DsdbrUtils.SetSectionCurrent_mA(DSDBRSection.SOA, maxSoaCurrent_mA);
                System.Threading.Thread.Sleep(stepDelay_mS);

                soaCurrentList.Add(DsdbrUtils.ReadSectionCurrent_mA(DSDBRSection.SOA));
                opticalPowerList.Add(Measurements.ReadOpticalPower(powerMeterHead, Measurements.PowerUnits.dBm));

                if (DsdbrUtils.DsdbrDriverInstrsToUse == DsdbrDriveInstruments.FCU2AsicInstrument)
                {
                    soaDac = DsdbrUtils.Fcu2AsicInstrumentGroup .Fcu2Asic .ISoa_Dac;                   
                    soaDacList.Add(soaDac );
                }
            }

            double[] soaCurrentArray = (double[])soaCurrentList.ToArray(typeof(Double));
            double[] powerArray = (double[])opticalPowerList.ToArray(typeof(Double));
            DatumList sweepDataToReturn = new DatumList();
            sweepDataToReturn.AddDoubleArray("SoaCurrentArray_mA", soaCurrentArray);
            sweepDataToReturn.AddDoubleArray("MeasuredPowerArray_dBm", powerArray);

            if (DsdbrUtils.DsdbrDriverInstrsToUse == DsdbrDriveInstruments.FCU2AsicInstrument)
            {
                double[] soaDacArray= (double[]) soaDacList .ToArray(typeof(double));
                sweepDataToReturn.AddDoubleArray("SoaCurrentArray_Dac", soaDacArray);
            }
            // Restore original conditions
            DsdbrUtils.SetSectionCurrent_mA(DSDBRSection.SOA, initialSoaCurrent);

            return sweepDataToReturn;
        }

        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestSolution.IlmzCommonData;
using Bookham.TestSolution.IlmzCommonInstrs;
using Bookham.TestLibrary.Algorithms;

namespace Bookham.TestSolution.IlmzCommonUtils
{
    /// <summary>
    /// Class to perform DSDBR section IV sweep test
    /// </summary>
    public sealed class SectionIVSweep
    {
        #region Private data
        /// <summary> DSDBR instruments property </summary>
        private static DsdbrInstruments _dsdbrInstrumentGroup;
        #endregion

        /// <summary>
        /// Private constructor
        /// </summary>
        private SectionIVSweep()
        {
        }

        /// <summary>
        /// IV sweep
        /// </summary>
        /// <param name="section">DSDBR section</param>
        /// <param name="sectionSweepSettings">Section IV sweep settings</param>
        /// <returns>Section IV sweep data</returns>
        public static SectionIVSweepData PerformSectionIVSweep(DSDBRSection section, SectionIVSweepSetting sectionSweepSettings)
        {
            // Setup
            double startCurrent_mA=sectionSweepSettings.StartI_mA;
            double stopCurrent_mA = sectionSweepSettings.StopI_mA;
            double stepCurrent_mA = sectionSweepSettings.StepI_mA;
            int delayTime_mS = sectionSweepSettings.DelayTime_mS;

            DsdbrUtils.SetDsdbrCurrents_mA(sectionSweepSettings.OtherSectionsCurrents);

            // Sweep results
            List<double> Idata_mA = new List<double>();
            List<double> Vdata_V = new List<double>();
            double curr_mA = 0.0;

            // Section IV sweep test
            _dsdbrInstrumentGroup.CurrentSources[section].CurrentSetPoint_amp = 0.0;
            _dsdbrInstrumentGroup.CurrentSources[section].OutputEnabled = true;
            for (curr_mA = startCurrent_mA; curr_mA <= stopCurrent_mA; curr_mA += stepCurrent_mA)
            {
                _dsdbrInstrumentGroup.CurrentSources[section].CurrentSetPoint_amp = curr_mA / 1000;
                System.Threading.Thread.Sleep(delayTime_mS);

                Idata_mA.Add(_dsdbrInstrumentGroup.CurrentSources[section].CurrentActual_amp * 1000);
                Vdata_V.Add(_dsdbrInstrumentGroup.CurrentSources[section].VoltageActual_Volt);
            }
            // Read at last point
            if (curr_mA < stopCurrent_mA)
            {
                _dsdbrInstrumentGroup.CurrentSources[section].CurrentSetPoint_amp = stopCurrent_mA / 1000;
                System.Threading.Thread.Sleep(delayTime_mS);

                Idata_mA.Add(_dsdbrInstrumentGroup.CurrentSources[section].CurrentActual_amp * 1000);
                Vdata_V.Add(_dsdbrInstrumentGroup.CurrentSources[section].VoltageActual_Volt);
            }

            // Return data
            SectionIVSweepData sweepData = new SectionIVSweepData(section, Idata_mA.ToArray(), Vdata_V.ToArray());
            return sweepData;
        }

        /// <summary>
        /// DSDBR instruments property
        /// </summary>
        public static DsdbrInstruments DsdbrInstrumentGroup
        {
            set
            {
                _dsdbrInstrumentGroup = value;
            }
        }
    }

    /// <summary>
    /// Class to contain DSDBR section IV sweep data
    /// </summary>
    public class SectionIVSweepData
    {
        #region Private data
        private DSDBRSection _section;
        private double[] _Idata_mA;
        private double[] _Vdata_V;
        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="section"></param>
        /// <param name="Idata_mA"></param>
        /// <param name="Vdata_V"></param>
        public SectionIVSweepData(DSDBRSection section, double[] Idata_mA, double[] Vdata_V)
        {
            this._section = section;
            this._Idata_mA = Idata_mA;
            this._Vdata_V = Vdata_V;
        }

        /// <summary>
        /// Section IV sweep data - Currents
        /// </summary>
        public double[] Idata_mA
        {
            get { return this._Idata_mA; }
        }

        /// <summary>
        /// Section IV sweep data - Voltages
        /// </summary>
        public double[] Vdata_V
        {
            get { return this._Vdata_V; }
        }

        /// <summary>
        /// DSDBR section
        /// </summary>
        public DSDBRSection Section
        {
            get { return this._section; }
        }

        /// <summary>
        /// Get fit voltage in V at a given current in mA
        /// </summary>
        /// <param name="current_mA"></param>
        /// <returns></returns>
        public double GetFitVoltage_V(double current_mA)
        {
            /* LinearLeastSquaresFit is used here based on adjacent 4 points 
             * around the given current */

            // Get the fromIndex and toIndex
            int linearFitAdjPoints = 2;
            int index = Alg_ArrayFunctions.FindIndexOfNearestElement(this._Idata_mA, current_mA);
            int startIndex = Math.Max(0, index + 1 - linearFitAdjPoints);
            int stopIndex = Math.Min(this._Idata_mA.Length - 1, index + linearFitAdjPoints);

            // Linear least squares fit
            LinearLeastSquaresFit linearFitRes =
                LinearLeastSquaresFitAlgorithm.Calculate(this._Idata_mA, this._Vdata_V, startIndex, stopIndex);

            // Calculate fitted voltage
            double voltage_V = linearFitRes.Slope * current_mA + linearFitRes.YIntercept;
            return voltage_V;
        }
    }

    /// <summary>
    /// Structure to contain configuration data for DSDBR section IV sweep
    /// </summary>
    public struct SectionIVSweepSetting
    {
        /// <summary> Section </summary>
        public DSDBRSection Section;
        /// <summary> Start current in mA </summary>
        public double StartI_mA;
        /// <summary> Stop current in mA </summary>
        public double StopI_mA;
        /// <summary> Step current in mA </summary>
        public double StepI_mA;
        /// <summary> Delay time in mS </summary>
        public int DelayTime_mS;
        /// <summary> Other sections currents when do IV sweep </summary>
        public DsdbrChannelSetup OtherSectionsCurrents;
    }
}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// PowerCal.cs
//
// Author: Mark Fullalove, 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Threading;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.Toolkit.CloseGrid;//jack.Zhang
using Bookham.TestSolution.IlmzCommonData;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestSolution.IlmzCommonInstrs;
using Bookham.TestLibrary.Algorithms;

namespace Bookham.TestSolution.IlmzCommonUtils
{
    /// <summary>
    /// Class containing power cal data for a single channel
    /// </summary>
    public class PowerCal
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="fitOrder">The order of fit to apply to the data</param>
        public PowerCal(int fitOrder)
        {
            this.fitOrder = fitOrder;
            this.frequency = new ArrayList();
            this.offset_dB = new ArrayList();
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="frequencyData">An array of frequencies</param>
        /// <param name="offsetData">An array of offsets</param>
        /// <param name="fitOrder">The order of fit to be applied</param>
        public PowerCal(double[] frequencyData, double[] offsetData, int fitOrder)
        {
            if (frequencyData.Length != offsetData.Length)
            {
                throw new AlgorithmException("Mismatched power and offset arrays");
            }
            if (frequencyData.Length == 0)
            {
                throw new AlgorithmException("Empty power and offset arrays");
            }

            this.frequency = new ArrayList();
            this.offset_dB = new ArrayList();

            foreach (double freq in frequencyData)
            {
                frequency.Add(freq);
            }
            foreach (double offset in offsetData)
            {
                offset_dB.Add(offset);
            }

            if (this.frequency.Count > fitOrder)
                fitToData();
        }

        /// <summary>
        /// Adds a single frequency and offset pair.
        /// </summary>
        /// <param name="frequency">Measured frequency</param>
        /// <param name="offset">Measured offset</param>
        public void AddPoint(double frequency, double offset)
        {
            this.frequency.Add(frequency);
            this.offset_dB.Add(offset);

            if ( this.frequency.Count > fitOrder )
                fitToData();
        }

        /// <summary>
        /// Calculate the Offset at a given frequency
        /// </summary>
        /// <param name="frequencyGHz">The frequency for which the offset is required.</param>
        /// <returns>An offset figure in dB</returns>
        public double GetOffset_dB(double frequencyGHz)
        {
            if (coeffs == null)
            {
                throw new AlgorithmException("Insufficient data to perform fit");
            }

            // Apply coefficients to data point.
            double offset = 0;
            for (int powerOfX = 0; powerOfX <= fitOrder; powerOfX++)
            {
                offset += coeffs[powerOfX] * Math.Pow(frequencyGHz, powerOfX);
            }


                //double yValue = 0;
                //for (int powerOfX = 0; powerOfX <= Order; powerOfX++)
                //{
                //    yValue += Coefficients[powerOfX] * Math.Pow(xArray[i], powerOfX);
                //}
                //fittedY[i] = yValue;


            return offset;
        }

        private void fitToData()
        {
            double[] frequencyArray = (double[])frequency.ToArray(typeof(Double));
            double[] offsetArray = (double[])offset_dB.ToArray(typeof(Double));

            PolyFit polyFit = Alg_PolyFit.PolynomialFit(frequencyArray, offsetArray, fitOrder);
            //PolyFit polyFit = Alg_PolyFit.PolynomialFit(offsetArray, frequencyArray, fitOrder);
            coeffs = polyFit.Coeffs;
            calibrationComplete = true;
        }

        /// <summary>
        /// Calibration complete flag
        /// </summary>
        /// <returns>True if calibration is complete</returns>
        public bool CalibrationComplete()
        {
            return calibrationComplete;
        }

        private int fitOrder;
        private double[] coeffs;
        private ArrayList frequency;
        private ArrayList offset_dB;
        private bool calibrationComplete;
    }


}

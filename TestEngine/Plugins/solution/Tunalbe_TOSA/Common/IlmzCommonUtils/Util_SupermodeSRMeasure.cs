using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.Algorithms;

namespace Bookham.TestLibrary.Utilities
{
    /// <summary>
    /// Class to measure SupermodeSR from an OSA and return value in dBm.
    /// </summary>
    public class Util_SupermodeSRMeasure
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="osa">OSA</param>
        /// <param name="supermodeSRCentreOpticalFreq_GHz">Centre optical frequency</param>
        /// <param name="supermodeSRSpan_nm">Span - wide span to observe supermodes</param>
        /// <param name="supermodeSRRBW_nm">Resolution bandwidth</param>
        /// <param name="supermodeSROSASensitivity_dBm">OSA sensitivity setting - set to resolve lower peaks</param>
        /// <param name="supermodeSRNumPts">Number of trace points</param>
        /// <param name="supermodeSRExclude_nm">Exclusion region around carrier</param>
        /// <param name="supermodeSRSweepTime_s">Sweep time - use OSA automatic setting</param>
        private Util_SupermodeSRMeasure(InstType_OSA osa, double supermodeSRCentreOpticalFreq_GHz, 
            double supermodeSRSpan_nm, double supermodeSRRBW_nm, double supermodeSROSASensitivity_dBm, 
            int supermodeSRNumPts, double supermodeSRExclude_nm, double supermodeSRSweepTime_s)
        {
            this.osa = osa;
            this.supermodeSRCentreOpticalFreq_GHz = supermodeSRCentreOpticalFreq_GHz;
            this.supermodeSRSpan_nm = supermodeSRSpan_nm;
            this.supermodeSRRBW_nm = supermodeSRRBW_nm;
            this.supermodeSROSASensitivity_dBm = supermodeSROSASensitivity_dBm;
            this.supermodeSRNumPts = supermodeSRNumPts;
            this.supermodeSRExclude_nm = supermodeSRExclude_nm;
            this.supermodeSRSweepTime_s = supermodeSRSweepTime_s;
        }

        /// <summary>
        /// Measure supermodeSR, return value in dBm.
        /// </summary>
        /// <param name="osa">OSA</param>
        /// <param name="supermodeSRCentreOpticalFreq_GHz">Centre optical frequency</param>
        /// <param name="supermodeSRSpan_nm">Span - wide span to observe supermodes</param>
        /// <param name="supermodeSRRBW_nm">Resolution bandwidth</param>
        /// <param name="supermodeSROSASensitivity_dBm">OSA sensitivity setting - set to resolve lower peaks</param>
        /// <param name="supermodeSRNumPts">Number of trace points</param>
        /// <param name="supermodeSRExclude_nm">Exclusion region around carrier</param>
        /// <param name="supermodeSRSweepTime_s">Sweep time - use OSA automatic setting</param>
        /// <returns>supermodeSR in dBm</returns>
        public double MeasureSupermodeSR(InstType_OSA osa, double supermodeSRCentreOpticalFreq_GHz,
            double supermodeSRSpan_nm, double supermodeSRRBW_nm, double supermodeSROSASensitivity_dBm,
            int supermodeSRNumPts, double supermodeSRExclude_nm, double supermodeSRSweepTime_s)
        {
            Util_SupermodeSRMeasure supermodeSRMeasure = new Util_SupermodeSRMeasure(osa, supermodeSRCentreOpticalFreq_GHz, 
                supermodeSRSpan_nm, supermodeSRRBW_nm, supermodeSROSASensitivity_dBm, supermodeSRNumPts, supermodeSRExclude_nm, supermodeSRSweepTime_s);

            supermodeSRMeasure.osa.SweepStop();
            //check/set OSA sweepmode to single
            if (supermodeSRMeasure.osa.SweepMode != InstType_OSA.SweepModes.Single)
            {
                supermodeSRMeasure.osa.SweepMode = InstType_OSA.SweepModes.Single;
            }

            double centreWL = Alg_FreqWlConvert.Freq_GHz_TO_Wave_nm(supermodeSRMeasure.supermodeSRCentreOpticalFreq_GHz);
            supermodeSRMeasure.osa.WavelengthStart_nm = centreWL - supermodeSRMeasure.supermodeSRSpan_nm;
            supermodeSRMeasure.osa.WavelengthStop_nm = centreWL + supermodeSRMeasure.supermodeSRSpan_nm;
            supermodeSRMeasure.osa.ResolutionBandwidth = supermodeSRMeasure.supermodeSRRBW_nm;
            //sensitivity...
            //sweep time...
            supermodeSRMeasure.osa.TracePoints = supermodeSRMeasure.supermodeSRNumPts;

            double peakPower_dBm;
            double nextPeakPower_dBm;
            double peakWL_nm;
            double nextPeakWL_nm;
            int nextPeakCount = 0;
            supermodeSRMeasure.osa.SweepStart();
            do
            {
                System.Threading.Thread.Sleep(100);
            } while (supermodeSRMeasure.osa.Status != InstType_OSA.ChannelState.DataReady);

            osa.MarkerToPeak();
            peakPower_dBm = supermodeSRMeasure.osa.MarkerAmplitude_dBm;
            peakWL_nm = supermodeSRMeasure.osa.MarkerWavelength_nm;

            do
            {
                supermodeSRMeasure.osa.MarkerToNextPeak(InstType_OSA.Direction.Unspecified);
                nextPeakWL_nm = supermodeSRMeasure.osa.MarkerWavelength_nm;
                nextPeakCount++;
            } while (nextPeakCount < 3 && (Math.Abs(nextPeakWL_nm - peakWL_nm) < supermodeSRMeasure.supermodeSRExclude_nm));

            //if more than 3 peaks in supermodeSR exclusion?-------The other case I don't know how to handle.
            //...

            nextPeakPower_dBm = supermodeSRMeasure.osa.MarkerAmplitude_dBm;

            double supermodeSR_dBm = peakPower_dBm - nextPeakPower_dBm;
            return supermodeSR_dBm;
        }

        #region Private data
        private InstType_OSA osa;
        private double supermodeSRCentreOpticalFreq_GHz;
        private double supermodeSRSpan_nm;
        private double supermodeSRRBW_nm;
        private double supermodeSROSASensitivity_dBm;
        private int supermodeSRNumPts;
        private double supermodeSRSweepTime_s;
        private double supermodeSRExclude_nm;
        #endregion
    }
}
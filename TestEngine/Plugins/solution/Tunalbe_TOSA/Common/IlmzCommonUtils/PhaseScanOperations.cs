// [Copyright]
// Bookham.TestSolution.IlmzCommonUtils
// RippleTest.cs
// Author: chongjian.liang 2013.5.6
// Design: [XS documentation]

using System;
using System.IO;
using System.Collections.Generic;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.Utilities;
using Bookham.TestSolution.IlmzCommonData;
using Bookham.TestSolution.IlmzCommonInstrs;

namespace Bookham.TestSolution.IlmzCommonUtils
{
    public class PhaseScanOperations
    {
        public static List<double> PhaseScanSequence_mA = null;

        /// <summary>
        /// Intialize the phase scan sequence the same as Map test from file "PhaseCurrentFile" - chongjian.liang 2013.4.10
        /// </summary>
        public static void IntializePhaseScanSequence(string phaseScanSequenceFilePath)
        {
            PhaseScanSequence_mA = new List<double>();

            // Add IPhase=0 equal IPhase index=0;
            // So the _phaseScanCurrents[IPhaseIndex] is the phase current of this index
            PhaseScanSequence_mA.Add(0);

            List<string[]> lines = null;

            using (CsvReader csvReader = new CsvReader())
            {
                lines = csvReader.ReadFile(phaseScanSequenceFilePath);
            }

            // Add all phase currents from phase sweep file
            foreach (string[] line in lines)
            {
                PhaseScanSequence_mA.Add(double.Parse(line[0]));
            }
        }

        /// <summary>
        /// Scan phase current by scan start and end index in phaseScanSequenceFile
        /// </summary>
        public static void PhaseScanByIndex(bool isUseOpticalBoxEtalon, string phaseScanSequenceFilePath, int scanStartIndex, int scanEndIndex, IInstType_TecController dsdbrTec, double CH_LASER_RTH_Min, double CH_LASER_RTH_Max, out double[] phaseArray_mA, out double[] lockerRatioArray, out double[] freqArray_GHz)
        {
            // Intialize phase scan sequence if it's not been initialized
            if (PhaseScanSequence_mA == null)
            {
                IntializePhaseScanSequence(phaseScanSequenceFilePath);
            }

            int scanCount = scanEndIndex - scanStartIndex + 1;

            phaseArray_mA = new double[scanCount];
            lockerRatioArray = new double[scanCount];
            freqArray_GHz = new double[scanCount];
            
            for (int i = scanStartIndex; i <= scanEndIndex; i++)
            {
                DsdbrUtils.SetSectionCurrent_mA(DSDBRSection.Phase, PhaseScanSequence_mA[i]);

                /// Check RTH at scanStartIndex

                if (i == scanStartIndex)
                {
                    double RTH_ohm = dsdbrTec.SensorResistanceActual_ohm;

                    while (RTH_ohm < CH_LASER_RTH_Min || RTH_ohm > CH_LASER_RTH_Max)
                    {
                        // Check RTH of TecDsdbr every 0.01s
                        System.Threading.Thread.Sleep(10);

                        RTH_ohm = dsdbrTec.SensorResistanceActual_ohm;
                    }
                }

                /// Read phase current, locker ratio & frequency

                phaseArray_mA[i - scanStartIndex] = PhaseScanSequence_mA[i];

                lockerRatioArray[i - scanStartIndex] = DsdbrUtils.ReadLockerCurrents(i == scanStartIndex, isUseOpticalBoxEtalon, i == scanStartIndex, i == scanEndIndex, true).LockRatio;

                freqArray_GHz[i - scanStartIndex] = Measurements.ReadFrequency_GHz();
            }
        }

        /// <summary>
        /// Scan phase current by scan start and end index in phaseScanSequenceFile
        /// </summary>
        public static void PhaseScanByIndex(bool isUseOpticalBoxEtalon, string phaseScanSequenceFilePath, int scanStartIndex, int scanEndIndex, IInstType_TecController dsdbrTec, double CH_LASER_RTH_Min, double CH_LASER_RTH_Max, out double[] phaseArray_mA, out double[] lockerRatioArray, out double[] freqArray_GHz, out double readLockerRatioTime, out double readWaveMeterTime)
        {
            // Intialize phase scan sequence if it's not been initialized
            if (PhaseScanSequence_mA == null)
            {
                IntializePhaseScanSequence(phaseScanSequenceFilePath);
            }

            int scanCount = scanEndIndex - scanStartIndex + 1;

            phaseArray_mA = new double[scanCount];
            lockerRatioArray = new double[scanCount];
            freqArray_GHz = new double[scanCount];

            readLockerRatioTime = 0;
            readWaveMeterTime = 0;

            for (int i = scanStartIndex; i <= scanEndIndex; i++)
            {
                DsdbrUtils.SetSectionCurrent_mA(DSDBRSection.Phase, PhaseScanSequence_mA[i]);

                /// Check RTH at scanStartIndex

                if (i == scanStartIndex)
                {
                    double RTH_ohm = dsdbrTec.SensorResistanceActual_ohm;

                    while (RTH_ohm < CH_LASER_RTH_Min || RTH_ohm > CH_LASER_RTH_Max)
                    {
                        // Check RTH of TecDsdbr every 0.01s
                        System.Threading.Thread.Sleep(10);

                        RTH_ohm = dsdbrTec.SensorResistanceActual_ohm;
                    }
                }

                /// Read phase current, locker ratio & frequency

                phaseArray_mA[i - scanStartIndex] = PhaseScanSequence_mA[i];

                DateTime startTime = DateTime.Now;

                lockerRatioArray[i - scanStartIndex] = DsdbrUtils.ReadLockerCurrents(i == scanStartIndex, isUseOpticalBoxEtalon, i == scanStartIndex, i == scanEndIndex, true).LockRatio;
                
                readLockerRatioTime += (DateTime.Now - startTime).TotalSeconds;

                startTime = DateTime.Now;

                freqArray_GHz[i - scanStartIndex] = Measurements.ReadFrequency_GHz();

                readWaveMeterTime += (DateTime.Now - startTime).TotalSeconds;
            }
        }
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;

namespace DbAccess
{
    /// <summary>
    /// Data-object base class
    /// </summary>
    public abstract class DbObjectBase  
    {
        //[Browsable(false)]
        //public DataTable Schema
        //{
        //    get
        //    {
        //        return this._schemaTable;
        //    }
        //    internal set
        //    {
        //        this._schemaTable = value;
        //    }
        //}
        /// <summary>
        /// Bind members to database fields.
        /// </summary>
        /// <param name="adapter">Data-object adapter</param>
        public abstract void Bind(DbObjectAdapter adapter);
    }
}

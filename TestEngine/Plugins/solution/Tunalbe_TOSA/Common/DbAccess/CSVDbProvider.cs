using System;
using System.Collections.Generic;
using System.Text;

namespace DbAccess
{
    public class CSVDbProvider : IDbProvider
    {
        private bool _hasHeader = true;
        private TableColumnMapping _map;
        private TableSchema _tableSchema;

        #region IDbProvider Members

        public bool HasHeader
        {
            get 
            {
                return this._hasHeader;
            }
            set
            {
                this._hasHeader = value;
            }
        }

        public TableColumnMapping DefaultMapping
        {
            get
            {
                return this._map;
            }
            set
            {
                this._map = value;
            }
        }

        public TableSchema DefaultTableSchema
        {
            get
            {
                return this._tableSchema;
            }
            set
            {
                this._tableSchema = value;
            }
        }

        public string Schema
        {
            get
            {
                return null;
            }
        }

        public void EnsureConnect()
        {
            return;
        }

        public System.Data.Common.DbCommand CreateCommand()
        {
            return new CSVCommand( this );
        }

        public System.Data.Common.DbCommand CreateCommand(string statement)
        {
            return new CSVCommand( statement, this );
        }

        public System.Data.Common.DbTransaction BeginTransaction()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion
    }
}

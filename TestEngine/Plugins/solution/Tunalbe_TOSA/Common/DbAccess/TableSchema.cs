using System;
using System.Collections.Generic;
using System.Text;

namespace DbAccess
{
    public class TableSchema
        : IEnumerable<TableColumn>
    {
        #region Nested types
        public class Enumerator
            : IEnumerator<TableColumn>
        {
            private TableSchema _owner;
            private int _index;

            internal Enumerator( TableSchema owner )
            {
                this._owner = owner;
                this._index = -1;
            }

            #region IEnumerator<TableColumn> Members

            public TableColumn Current
            {
                get 
                {
                    return this._owner._list[this._index];
                }
            }

            #endregion

            #region IDisposable Members

            public void Dispose()
            {
                this._owner = null;
                this._index = 0;
            }

            #endregion

            #region IEnumerator Members

            object System.Collections.IEnumerator.Current
            {
                get 
                {
                    return this._owner._list[this._index];
                }
            }

            public bool MoveNext()
            {
                this._index++;
                return (this._index < this._owner._list.Count);
            }

            public void Reset()
            {
                this._index = -1;
            }

            #endregion
        }
        #endregion

        private List<TableColumn> _list = new List<TableColumn>();
        /// <summary>
        /// Table schema name.
        /// </summary>
        private string _name;

        /// <summary>
        /// Returns table schema name.
        /// </summary>
        public string Name
        {
            get
            {
                return this._name;
            }
            set
            {
                this._name = value;
            }
        }

        /// <summary>
        /// Number of columns.
        /// </summary>
        public int Count
        {
            get
            {
                return this._list.Count;
            }
        }
        public TableColumn this[int index]
        {
            get
            {
                return this._list[index];
            }
        }

        public void Add(TableColumn col)
        {
            this._list.Add(col);
        }


        #region IEnumerable<TableColumn> Members

        public IEnumerator<TableColumn> GetEnumerator()
        {
            return new Enumerator(this);
        }

        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return new Enumerator(this);
        }

        #endregion
    }

    public class TableColumn
    {
        public int Ordinal;
        //public TableColumnDataType Type;
        public Type Type;
        public string ColumnName;

        public TableColumn(int ordinal, string columnName, Type type)
        {
            this.Ordinal = ordinal;
            this.Type = type;
            this.ColumnName = columnName;
        }
    }

    public class TableColumnMapping
    {
        private Dictionary<string, string> _map = new Dictionary<string, string>();

        public void Add(string keyFrom, string keyTo)
        {
            this._map.Add(keyFrom, keyTo);
        }

        public string this[string index]
        {
            get
            {
                return this._map[index];
            }
        }
    }

}

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.Specialized;
using System.Xml.Serialization;
using System.Xml.Schema;

namespace DbAccess
{
    /// <summary>
    /// Data-object will read/write fields automaticlly from/into database.
    /// When querying, field-names will automaticlly generated from 'select' clause.
    /// </summary>
    public class DbObjectWithColumn 
        : DbObjectBase
        , IEnumerable<KeyValuePair<string,object>>
        , ICloneable
    {
        /// <summary>
        /// Dictionary which holds all FieldName -- FieldValue pairs.
        /// </summary>
        protected Dictionary<string, object> _values = new Dictionary<string, object>();
        //protected StringDictionary _values = new StringDictionary();
        private bool _modified = true;

        public virtual bool IsModified
        {
            get
            {
                return this._modified;
            }
        }
        /// <summary>
        /// indexer.
        /// </summary>
        /// <param name="key">Database object key name( field name ).</param>
        /// <returns>Returns database object value.</returns>
        public virtual object this[string key]
        {
            get
            {
                if (_values.ContainsKey(key))
                {
                    return this._values[key];
                }
                else
                {
                    return null;
                }
            }
            set
            {
                if (this._values.ContainsKey(key))
                {
                    if (this._values[key] != value)
                    {
                        this._values[key] = value;
                        this._modified = true;
                    }
                }
                else
                {
                    this._values.Add(key, value);
                    this._modified = true;
                }
            }
        }
    
        /// <summary>
        /// Binds all members to database.
        /// </summary>
        /// <param name="adapter">database - object mapping adapter</param>
        public override void Bind(DbObjectAdapter adapter)
        {
            for (int i = 0; i < adapter.FieldCount; i++)
            {
                string key = adapter.GetName(i);
                object value = null;

                if (string.Compare(key, "ROWID", true) != 0)
                {
                    adapter.Bind(key, ref value);
                    //this._values.Add(key, value);
                    this._values[key] = value;
                }
            }

            this._modified = false;
        }

        /// <summary>
        /// Return value of specified key( field name ).
        /// </summary>
        /// <param name="key">key/field name</param>
        /// <param name="ignoreCase">ture for ignore case.</param>
        /// <returns>returns corresponding value for specified key.</returns>
        public object GetValue(string key, bool ignoreCase)
        {
            foreach (string ikey in this._values.Keys)
            {
                if (string.Compare(key, ikey, ignoreCase) == 0)
                {
                    return _values[ikey];
                }
            }
            return null;
        }

        /// <summary>
        /// Get specified field value as decimal type.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public decimal GetDecimal(string key)
        {
            if (_values.ContainsKey(key))
            {
                return Convert.ToDecimal(this._values[key]);
            }
            else
            {
                return decimal.Zero;
            }
        }

        public int GetInt(string key)
        {
            if (_values.ContainsKey(key))
            {
                return Convert.ToInt32(this._values[key]);
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Mark this object as MODIFIED.
        /// </summary>
        protected void MarkModified()
        {
            this._modified = true;
        }

        /// <summary>
        /// Mark this object as UNMODIFIED.
        /// </summary>
        public void MarkUnmodified()
        {
            this._modified = false;
        }

        #region IEnumerable<KeyValuePair<string,object>> Members

        public IEnumerator<KeyValuePair<string, object>> GetEnumerator()
        {
            return this._values.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this._values.GetEnumerator();
        }

        #endregion

        public void Add(KeyValuePair<string, object> item)
        {
            this._values.Add(item.Key, item.Value);
            this._modified = true;
        }

        #region ICloneable Members

        public object Clone()
        {
            Type type = this.GetType();
            DbObjectWithColumn obj = System.Activator.CreateInstance(type) as DbObjectWithColumn;
            
            foreach( KeyValuePair<string,object> pair in this._values )
            {
                obj._values.Add(pair.Key, pair.Value);
            }

            //obj.Schema = this.Schema;
            obj._modified = this._modified;

            return obj;
        }

        #endregion
    }
}

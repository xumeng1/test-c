//Revision History:
//2008-4-11: specify schema in DbObjectAdapter.Query instead of here, 
//          in order to support multi-schema query.

using System.Data.Common;
namespace DbAccess
{
    public interface IDbProvider
    {
        //string Schema
        //{
        //    get;
        //}
    
        void EnsureConnect();

        DbCommand CreateCommand();

        DbCommand CreateCommand(string statement);

        DbTransaction BeginTransaction();
    }
}

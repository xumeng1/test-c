using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Data.Common;
using System.Collections;
using System.Data;

namespace DbAccess
{
    /// <summary>
    /// CSV file paser.
    /// </summary>
    public class CSVDbReader : DbDataReader
    {
        public class CSVReaderEnumerator : IEnumerator
        {
            private CSVDbReader _owner;
            private int _index;

            public CSVReaderEnumerator(CSVDbReader owner)
            {
                this._owner = owner;
                this._index = 0;
            }

            #region IEnumerator Members

            public object Current
            {
                get 
                {
                    return this._owner._line[this._index];
                }
            }

            public bool MoveNext()
            {
                if (_index < _owner._line.Length - 1)
                {
                    _index++;
                    return true;
                }
                else
                {
                    return false;
                }
            }

            public void Reset()
            {
                this._index = 0;
            }

            #endregion
        }

        #region private members
        private StreamReader _reader;
        private List<string> _fields = new List<string> ();
        private bool _hasHeader = true;
        private TableColumnMapping _map;
        private TableSchema _schema;
        /// <summary>
        /// current parsing line.
        /// </summary>
        private string[] _line;
        #endregion

        public bool HasHeader
        {
            get
            {
                return this._hasHeader;
            }
        }

        /// <summary>
        /// Construct CSVDbReader from a file.
        /// </summary>
        /// <param name="filename"></param>
        public CSVDbReader(string filename)
        {
            this._reader = new StreamReader(filename);

            string strline = this._reader.ReadLine();

            string [] fields = strline.Split( new char[] { ',' } );

            //this._fieldIndecies = new Dictionary<string,int>();
            this._fields.AddRange(fields);
        }

        /// <summary>
        /// Construct DSVDbReader with column mapping parameters.
        /// </summary>
        /// <param name="filename">CSV filename</param>
        /// <param name="hasHeader">Indicate whether this file has a header line or not.</param>
        /// <param name="map">column mapping</param>
        public CSVDbReader(string filename, bool hasHeader, TableColumnMapping map)
        {
            this._reader = new StreamReader(filename);

            this._hasHeader = hasHeader;
            if (this._hasHeader)
            {
                string strline = this._reader.ReadLine();

                string[] fields = strline.Split(new char[] { ',' });
                this._fields.AddRange(fields);
            }
            else
            {
                this._map = map;
            }

            //this._fieldIndecies = new Dictionary<string,int>();
            //this._fields.AddRange(fields);
        }

        /// <summary>
        /// Construct a CSVDbReader.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="hasHeader"></param>
        /// <param name="schema"></param>
        public CSVDbReader(string filename, bool hasHeader, TableSchema schema)
        {
            this._reader = new StreamReader(filename);

            this._hasHeader = hasHeader;
            if (this._hasHeader)
            {
                string strline = this._reader.ReadLine();

                if (string.IsNullOrEmpty(strline))
                {
                    this._fields.Clear();
                }
                else
                {
                    string[] fields = strline.Split(new char[] { ',' });
                    this._fields.AddRange(fields);
                }
            }
            else
            {
                this._schema = schema;
            }
        }

        /// <summary>
        /// Read next record from csv file.
        /// </summary>
        /// <returns> true if record exist, false for end-of-file.</returns>
        public override bool Read()
        {
            if (this._reader.EndOfStream)
            {
                return false;
            }
            else
            {
                string line = this._reader.ReadLine();
                this._line = line.Split(new char[] { ',' });
                return true;
            }
        }

        /// <summary>
        /// Reset CSVDbReader.
        /// </summary>
        public void Reset()
        {
            this._reader.BaseStream.Seek(0, SeekOrigin.Begin);
        }

        /// <summary>
        /// Skip records/lines.
        /// </summary>
        /// <param name="nSkip"> number of records/lines to skip</param>
        public void Skip(Int32 nSkip)
        {
            for (int idx = 0; idx < nSkip; idx++)
            {
                if (!this.Read())
                {
                    break;
                }
            }
        }

        /// <summary>
        /// Close the reader.
        /// </summary>
        public override void Close()
        {
            this._reader.Close();
            this._reader = null;
        }

        /// <summary>
        /// Returns double value of specified field.
        /// </summary>
        /// <param name="fieldName">Specified field name</param>
        /// <returns>return double value.</returns>
        public double GetDouble(string fieldName)
        {
            int idx = this.FindIndex(fieldName);

            if (idx == -1)
            {
                throw new ArgumentOutOfRangeException("fieldName", "Field not found!");
            }

            return this.GetDouble(idx);
        }

        /// <summary>
        /// Return nullable double value of specified field.
        /// </summary>
        /// <param name="index">Specified field index.</param>
        /// <returns>returns nullable double value of the field.</returns>
        public double? GetNullableDouble(int index)
        {
            string field = this._line[index];
            string trimmed = field.Trim();

            if (trimmed.Length == 0)
            {
                return null;
            }
            else
            {
                return double.Parse(trimmed);
            }
        }

        /// <summary>
        /// Returns nullable double value of specified value.
        /// </summary>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        public double? GetNullableDouble(string fieldName)
        {
            int idx = this.FindIndex(fieldName);

            if (idx == -1)
            {
                throw new ArgumentOutOfRangeException("fieldName", "Field not found!");
            }

            return this.GetNullableDouble(idx);
        }

        /// <summary>
        /// Returns double value of specified field.
        /// </summary>
        /// <param name="index">Specified field index.</param>
        /// <returns>Return double value of specified value.</returns>
        public override double GetDouble(int index)
        {
            return double.Parse( this._line[index] );
        }

        /// <summary>
        /// Returns string value of specified field.
        /// </summary>
        /// <param name="fieldName">Specified field name.</param>
        /// <returns>Return string value.</returns>
        public string GetString(string fieldName)
        {
            int idx = this.FindIndex(fieldName);

            if (idx == -1)
            {
                throw new ArgumentOutOfRangeException("fieldName", "Field not found!");
            }

            return this.GetString(idx);
        }

        /// <summary>
        /// Returns strinf value of specified field.
        /// </summary>
        /// <param name="index">Specified field index.</param>
        /// <returns>Return string value</returns>
        public override string GetString(int index)
        {
            string val = this._line[index];
            return val.Trim( new char[]{ ' ', '"' } );
        }

        /// <summary>
        /// Returns value of specified field as unsigned integer.
        /// </summary>
        /// <param name="fieldName">Specified field name.</param>
        /// <returns>returned uint value.</returns>
        public uint GetUInt(string fieldName)
        {
            int idx = this.FindIndex(fieldName);

            if ( idx == -1 )
            {
                throw new ArgumentOutOfRangeException("fieldName", "Field not found!");
            }

            return this.GetUInt(idx);
        }

        /// <summary>
        /// Returns value of specified field as unsigned integer.
        /// </summary>
        /// <param name="index">Field index.</param>
        /// <returns>Returned uint value.</returns>
        public uint GetUInt(int index)
        {
            return uint.Parse(this._line[index]);
        }

        /// <summary>
        /// Find index for corresponding field.
        /// </summary>
        /// <param name="fieldName">Field name.</param>
        /// <returns>Field index.</returns>
        public int FindIndex(string fieldName)
        {
            for (int idx = 0; idx < this._fields.Count; idx++)
            {
                if (string.Compare(fieldName, this._fields[idx], true) == 0)
                {
                    return idx;
                }
            }

            return -1;
        }

        /// <summary>
        /// Returns value of specified field as int.
        /// </summary>
        /// <param name="fieldName">Field name.</param>
        /// <returns>Field value as int</returns>
        public int GetInt(string fieldName)
        {
            int idx = this.FindIndex(fieldName);

            if (idx == -1)
            {
                throw new ArgumentOutOfRangeException("fieldName", "Field not found!");
            }

            return this.GetInt(idx);
        }

        /// <summary>
        /// Returns value of specified field as int
        /// </summary>
        /// <param name="index">Field index</param>
        /// <returns>Field value as int</returns>
        public int GetInt(int index)
        {
            return int.Parse(this._line[index]);
        }

        /// <summary>
        /// Returns field name for indicated item.
        /// </summary>
        /// <param name="i">field index</param>
        /// <returns></returns>
        public override  string GetName(int i)
        {
            if (this._schema != null)
            {
                foreach (TableColumn col in this._schema)
                {
                    if (col.Ordinal == i)
                    {
                        return col.ColumnName;
                    }
                }
                return i.ToString();
            }
            else
            {
                if (this._hasHeader)
                {
                    return this._fields[i];
                }
                else
                {
                    return i.ToString();
                }
            }
        }

        /// <summary>
        /// Returns field count for this reader.
        /// </summary>
        public override  int FieldCount
        {
            get
            {
                if (this._schema != null)
                {
                    return this._schema.Count;
                }
                else
                {
                    return this._fields.Count;
                }
            }
        }

        /// <summary>
        /// indexer..
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public override  object  this[int index]
        {
            get
            {
                if (index < this._line.Length)
                {
                    return this._line[index];
                }
                else
                {
                    return "";
                }
            }
        }

        public override int Depth
        {
            get 
            {
                return 1;
            }
        }

        /// <summary>
        /// Return indicated file value as Boolean type.
        /// </summary>
        /// <param name="ordinal">Field ordinal</param>
        /// <returns>Boolean value for this field.</returns>
        public override bool GetBoolean(int ordinal)
        {
            return bool.Parse(this._line[ordinal]);
        }

        /// <summary>
        /// Returns value of corresponding field as byte type.
        /// </summary>
        /// <param name="ordinal"></param>
        /// <returns></returns>
        public override byte GetByte(int ordinal)
        {
            return byte.Parse(this._line[ordinal]);
        }

        public override long GetBytes(int ordinal, long dataOffset, byte[] buffer, int bufferOffset, int length)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override char GetChar(int ordinal)
        {
            return char.Parse(this._line[ordinal]);
        }

        public override long GetChars(int ordinal, long dataOffset, char[] buffer, int bufferOffset, int length)
        {
            this._line[ordinal].CopyTo((int)dataOffset, buffer, bufferOffset, length);
            return length;
        }

        public override string GetDataTypeName(int ordinal)
        {
            return typeof(string).Name;
        }

        public override DateTime GetDateTime(int ordinal)
        {
            return DateTime.Parse(this._line[ordinal]);
        }

        public override decimal GetDecimal(int ordinal)
        {
            return decimal.Parse(this._line[ordinal]);
        }

        public override System.Collections.IEnumerator GetEnumerator()
        {
            return new CSVReaderEnumerator(this);
        }

        public override Type GetFieldType(int ordinal)
        {
            if (this._schema != null)
            {
                foreach (TableColumn col in this._schema)
                {
                    if (col.Ordinal == ordinal)
                    {
                        return col.Type;
                    }
                }
                return typeof(DBNull);
            }
            else
            {
                return typeof(string);
            }
        }

        public override float GetFloat(int ordinal)
        {
            return float.Parse(this._line[ordinal]);
        }

        public override Guid GetGuid(int ordinal)
        {
            return new Guid(this._line[ordinal]);
        }

        public override short GetInt16(int ordinal)
        {
            return short.Parse(this._line[ordinal]);
        }

        public override int GetInt32(int ordinal)
        {
            return int.Parse(this._line[ordinal]);
        }

        public override long GetInt64(int ordinal)
        {
            return long.Parse(this._line[ordinal]);
        }

        /// <summary>
        /// Returns the Ordinal of specified fieldname.
        /// </summary>
        /// <param name="name">Field name.</param>
        /// <returns>Field ordinal</returns>
        public override int GetOrdinal(string name)
        {
            if (this._hasHeader)
            {
                string mappedKey = 
                    (this._map != null) ? this._map[name] : name;

                for (int i = 0; i < this._fields.Count; i++)
                {
                    string key = this._fields[i];

                    if (string.Compare(mappedKey, key) == 0)
                    {
                        return i;
                    }
                }

                return -1;
            }
            else
            {
                if (this._schema != null)
                {
                    foreach (TableColumn col in this._schema)
                    {
                        if (string.Compare(col.ColumnName, name, true) == 0)
                        {
                            return col.Ordinal;
                        }
                    }
                }

                int ordinal = -1;

                if( this._map != null )
                {
                    string mappedKey = this._map[name];
                    ordinal = int.Parse(mappedKey);
                }
                else
                {
                    ordinal = int.Parse(name);
                }

                return ordinal;
            }
        }

        public override System.Data.DataTable GetSchemaTable()
        {
            DataTable table = new DataTable("CSV Table Schema");

            DataColumn colName = table.Columns.Add("ColumnName", typeof(string ));
            DataColumn colOridinal = table.Columns.Add("ColumnOridinal", typeof(int));

            for ( int idx = 0; idx < this._fields.Count; idx ++  )
            {
                DataRow row = table.NewRow();
                row[colName] = this._fields[idx];
                row[colOridinal] = idx;
                table.Rows.Add(row);
            }

            return table;
        }

        public override object GetValue(int ordinal)
        {
            return this._line[ordinal];
        }

        public override int GetValues(object[] values)
        {
            for (int i = 0; i < values.Length; i++)
            {
                if (i < this._line.Length)
                {
                    values[i] = this._line[i];
                }
            }

            return Math.Min(values.Length, this._fields.Count);
        }

        public override bool HasRows
        {
            get 
            {
                return !this._reader.EndOfStream;
            }
        }

        public override bool IsClosed
        {
            get
            {
                return (this._reader == null);
            }
        }

        public override bool IsDBNull(int ordinal)
        {
            return this._line[ordinal].Length == 0;
        }

        public override bool NextResult()
        {
            return false;
        }

        public override int RecordsAffected
        {
            get 
            {
                return -1;
            }
        }

        public override object this[string name]
        {
            get 
            {
                int idx = this.GetOrdinal(name);
                if ( ( 0 <= idx ) && (idx < this._line.Length ))
                {
                    string str = this._line[idx];
                    Type type = typeof(string);

                    if (this._schema != null)
                    {
                        foreach (TableColumn col in this._schema)
                        {
                            if (col.Ordinal == idx)
                            {
                                type = col.Type;
                                break;
                            }
                        }
                    }

                    if (type == typeof(string))
                    {
                        return str;
                    }
                    else
                    {
                        return Convert.ChangeType(str, type);
                    }
                }
                else
                {
                    return null;
                }
            }
        }
    }
}

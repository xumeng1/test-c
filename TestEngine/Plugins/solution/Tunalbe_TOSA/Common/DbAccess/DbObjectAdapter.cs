//Revision History
//2008.4.15: Ken.Wu: DbAccess module get no responsibility to known anything about PCAS schema;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;
using System.Data;

namespace DbAccess
{
    public enum DbAction
    {
        Unknown,
        Query,
        Update,
    }

    public class DbObjectAdapter
    {
        private enum AdapterSource
        {
            Reader,
            Table,
        }

        private DbAction _action = DbAction.Unknown;
        private IDbProvider _provider;
        private DbDataReader _reader;
        private DataTable _table;
        private DataRow _row;
        private AdapterSource _source = AdapterSource.Reader;
        private string _schema;

        public IDbProvider Provider
        {
            get
            {
                return _provider;
            }
            set
            {
                _provider = value;
            }
        }

        public string Schema
        {
            get
            {
                return this._schema;
            }
            set
            {
                this._schema = value;
            }
        }

        public T QueryLastOne<T>(string selectedFields, string tableName, string whereKeys, string orderFields) where T: DbObjectBase 
        {
            if ((selectedFields == null) ||
                (selectedFields.Length == 0))
            {
                throw new ArgumentException("Selected keys can't be empty!");
            }

            if ((tableName == null) ||
                (tableName.Length == 0))
            {
                throw new ArgumentException(" TableName can't be empty!");
            }

            StringBuilder stmt = new StringBuilder();

            string[] fields = selectedFields.Split(new char[] { ',' });
            stmt.Append(" SELECT ");

            bool isFirst = true;
            for (int i = 0; i < fields.Length; i++)
            {
                string field = fields[i];
                if (field.Length > 0)
                {
                    if (!isFirst)
                    {
                        stmt.Append(", ");
                    }
                    else
                    {

                        stmt.Append(" ");
                        isFirst = false;
                    }

                    stmt.Append(field);
                }
            }

            stmt.AppendFormat (" FROM {0}", tableName );

            if ((whereKeys != null) &&
                (whereKeys.Length > 0))
            {
                stmt.AppendFormat(" WHERE ( {0} ) AND ( ROWNUM <= 1 ) ", whereKeys);
            }
            else
            {
                stmt.AppendFormat(" WHERE ROWNUM <= 1 ");
            }

            if ((orderFields != null) &&
                (orderFields.Length > 0))
            {
                stmt.AppendFormat(" ORDER BY {0}", orderFields);
            }

            return this.QueryLastOne<T>(stmt.ToString());
        }

        public List<string> QuerySingleField(string statement)
        {
            string sql = null;

            //if (string.Compare(this._schema, "COC") == 0)
            //{
            //    sql = statement.Replace(@"$schema$.", "");
            //}
            //else
            //{
            //    sql = statement.Replace(@"$schema$", this._schema);
            //} 
            
            DbCommand cmd = _provider.CreateCommand(sql);
            DbDataReader reader = cmd.ExecuteReader();
            List<string> result = new List<string>();

            while (reader.Read())
            {
                result.Add(reader[0].ToString());
            }
            reader.Close();

            return result;
        }

        public List<DbObjectBase> Query(string statement, Type typeid)
        {
            Type baseType = typeid;

            while (typeid != typeof(object))
            {
                if (baseType == typeof(DbObjectBase))
                {
                    break;
                }

                baseType = baseType.BaseType;
            }

            if (typeid != typeof(DbObjectBase))
            {
                string prompt = string.Format("Can't cast type {0} into {1}.",
                    typeid.Name, typeof(DbObjectBase).Name);

                throw new InvalidCastException(prompt);
            }

            this._action = DbAction.Query;
            DbCommand cmd = _provider.CreateCommand(statement);

            DbDataReader reader = cmd.ExecuteReader(CommandBehavior.KeyInfo);
            //DataTable schemaTable = reader.GetSchemaTable();
            this._reader = reader;
            List<DbObjectBase> list = new List<DbObjectBase>();
            while (reader.Read())
            {
                DbObjectBase item = (DbObjectBase)System.Activator.CreateInstance(typeid);
                //item.Schema = schemaTable;
                item.Bind(this);
                list.Add(item);
            }
            reader.Close();

            return list;
        }
        
        public List<T> Query<T>(string statement) where T:DbObjectBase
        {
            //string sql;

            //if ((DbObjectAdapter._provider.Schema == null) ||
            //    (DbObjectAdapter._provider.Schema.Length == 0))
            //{
            //    sql = statement;
            //}
            //else
            //{
            //if ( string.Compare( this._schema, "COC" ) == 0 )
            //{
            //    sql = statement.Replace(@"$schema$.", "" );
            //}
            //else
            //{                 
            //    sql = statement.Replace(@"$schema$", this._schema);
            //}
            //}

            this._action = DbAction.Query;
            DbCommand cmd = _provider.CreateCommand(statement);

            DbDataReader reader = cmd.ExecuteReader(CommandBehavior.KeyInfo);
            //reader.GetSchemaTable().WriteXml("c:\\a.xml");
            this._reader = reader;
            List<T> list = new List<T>();
            Type typeid = typeof(T);
            while (reader.Read())
            {
                //reader.GetSchemaTable().WriteXml("c:\\sp.xml");
                T item = (T)System.Activator.CreateInstance(typeid);
                item.Bind(this);
                list.Add(item);
            }
            reader.Close();

            return list;
        }

        //public List<T> Query<T>(string statement, string schema) where T : DbObjectBase
        //{
        //    string sql;

        //    if (string.Compare(this._schema, "COC") == 0)
        //    {
        //        sql = statement.Replace(@"$schema$.", "");
        //    }
        //    else
        //    {
        //        sql = statement.Replace(@"$schema$", schema);
        //    }

        //    this._action = DbAction.Query;
        //    DbCommand cmd = DbObjectAdapter._provider.CreateCommand(sql);

        //    DbDataReader reader = cmd.ExecuteReader(CommandBehavior.KeyInfo);
        //    this._reader = reader;
        //    List<T> list = new List<T>();
        //    Type typeid = typeof(T);
        //    while (reader.Read())
        //    {
        //        //reader.GetSchemaTable().WriteXml("c:\\sp.xml");
        //        T item = (T)System.Activator.CreateInstance(typeid);
        //        item.Bind(this);
        //        list.Add(item);
        //    }
        //    reader.Close();

        //    return list;
        //}

        public List<T> QueryAll<T>(string tableName) where T : DbObjectBase
        {
            this._action = DbAction.Query;
            string statement = string.Format("SELECT * FROM {0} ", tableName);
            DbCommand cmd = _provider.CreateCommand(statement);

            DbDataReader reader = cmd.ExecuteReader(CommandBehavior.KeyInfo);
            this._reader = reader;
            List<T> list = new List<T>();
            Type typeid = typeof(T);
            while (reader.Read())
            {
                T item = (T)System.Activator.CreateInstance(typeid);
                item.Bind(this);
                list.Add(item);
            }
            reader.Close();

            return list;
        }

        public List<T> Query<T>(string statement, out DataTable schema) where T : DbObjectBase
        {
            this._action = DbAction.Query;
            DbCommand cmd = _provider.CreateCommand(statement);

            DbDataReader reader = cmd.ExecuteReader(CommandBehavior.KeyInfo);
            schema = reader.GetSchemaTable();

            this._reader = reader;
            List<T> list = new List<T>();
            Type typeid = typeof(T);
            while (reader.Read())
            {
                T item = (T)System.Activator.CreateInstance(typeid);
                item.Bind(this);
                list.Add(item);
            }
            reader.Close();

            return list;
        }

        public List<T> Query<T>(string statement, IDbProvider provider, out DataTable schema) where T : DbObjectBase
        {
            if (provider == null)
            {
                throw new ArgumentNullException("provider", "Database provider can't be null!");
            }

            this._action = DbAction.Query;
            DbCommand cmd = provider.CreateCommand(statement);

            DbDataReader reader = cmd.ExecuteReader(CommandBehavior.KeyInfo);
            schema = reader.GetSchemaTable();

            this._reader = reader;
            List<T> list = new List<T>();
            while (reader.Read())
            {
                T item = (T)System.Activator.CreateInstance<T>();
                item.Bind(this);
                list.Add(item);
            }
            reader.Close();

            return list;
        }

        public List<T> Query<T>(string statement, IDbProvider provider) where T : DbObjectBase
        {
            if (provider == null)
            {
                throw new ArgumentNullException("provider", "Database provider can't be null!");
            }

            this._action = DbAction.Query;
            DbCommand cmd = provider.CreateCommand(statement);

            DbDataReader reader = cmd.ExecuteReader(CommandBehavior.KeyInfo);

            this._reader = reader;
            List<T> list = new List<T>();
            while (reader.Read())
            {
                T item = (T)System.Activator.CreateInstance<T>();
                item.Bind(this);
                list.Add(item);
            }
            reader.Close();

            return list;
        }

        public List<T> Query<T>(DataTable table) where T : DbObjectBase
        {
            if (table == null)
            {
                throw new ArgumentNullException("table");
            }

            List<T> list = new List<T>();

            this._action = DbAction.Query;
            this._source = AdapterSource.Table;
            this._table = table;
            foreach (DataRow row in table.Rows )
            {
                this._row = row;

                T item = (T)System.Activator.CreateInstance<T>();
                item.Bind( this );
                list.Add( item );
            }
            this._source = AdapterSource.Reader;

            return list;
        }

        public int FieldCount
        {
            get
            {
                switch (this._source)
                {
                    case AdapterSource.Reader:
                        return _reader.FieldCount;

                    case AdapterSource.Table:
                        return _table.Columns.Count;

                    default:
                        return 0;
                }
            }
        }

        public void Bind( string fieldName, ref byte member)
        {
            switch (this._source)
            {
                case AdapterSource.Reader:
                    {
                        member = (byte)this._reader[fieldName];
                    }
                    break;

                case AdapterSource.Table:
                    {
                        member = (byte)this._row[fieldName];
                    }
                    break;
            }
        }

        public void Bind(string fieldName, ref string member)
        {
            switch (this._source)
            {
                case AdapterSource.Reader:
                    {
                        member = this._reader[fieldName] as string ;
                    }
                    break;

                case AdapterSource.Table:
                    {
                        member = this._row[fieldName] as string ;
                    }
                    break;
            }
        }

        public void Bind(string fieldName, ref uint member)
        {
            switch (this._source)
            {
                case AdapterSource.Reader:
                    {
                        member = (uint)this._reader[fieldName];
                    }
                    break;

                case AdapterSource.Table:
                    {
                        member = (uint)this._row[fieldName]; ;
                    }
                    break;
            }
        }

        public void Bind(string fieldName, ref int member)
        {
            switch (this._source)
            {
                case AdapterSource.Reader:
                    {
                        member = (int)this._reader[fieldName];
                    }
                    break;

                case AdapterSource.Table:
                    {
                        member = (int)this._row[fieldName]; ;
                    }
                    break;
            }
        }

        public void Bind(string fieldName, ref bool member)
        {
            switch (this._source)
            {
                case AdapterSource.Reader:
                    {
                        member = (bool)this._reader[fieldName];
                    }
                    break;

                case AdapterSource.Table:
                    {
                        member = (bool)this._row[fieldName]; ;
                    }
                    break;
            }
        }

        public void Bind(string fieldName, ref decimal member)
        {
            switch (this._source)
            {
                case AdapterSource.Reader:
                    {
                        //member = (decimal)this._reader[fieldName];
                        int ordinal = this._reader.GetOrdinal(fieldName);
                        member = this._reader.GetDecimal(ordinal);
                    }
                    break;

                case AdapterSource.Table:
                    {
                        member = (decimal)this._row[fieldName];
                    }
                    break;
            }
        }

        public void Bind(string fieldName, ref double member)
        {
            switch (this._source)
            {
                case AdapterSource.Reader:
                    {
                        member = (double)this._reader[fieldName];
                    }
                    break;

                case AdapterSource.Table:
                    {
                        member = (double)this._row[fieldName]; ;
                    }
                    break;
            }
        }

        public void Bind(string fieldName, ref object member)
        {
            switch (this._action)
            {
                case DbAction.Query:
                    {
                        switch (this._source)
                        {
                            case AdapterSource.Reader:
                                {
                                    member = this._reader[fieldName];
                                }
                                break;

                            case AdapterSource.Table:
                                {
                                    member = this._row[fieldName]; ;
                                }
                                break;
                        }
                    }
                    break;

                case DbAction.Update:
                    {
                        if (!this._table.Columns.Contains(fieldName))
                        {
                            this._table.Columns.Add(fieldName, member.GetType());
                        }
                        _row[fieldName] = member;
                    }
                    break;
            }
        }

        public string GetName(int i)
        {
            switch (this._source)
            {
                case AdapterSource.Reader:
                        return this._reader.GetName(i);

                case AdapterSource.Table:
                        return this._table.Columns[i].ColumnName;
            }
            return null;
        }

        public delegate int WSSaveProc(DataTable table);

        public int Update(List<DbObjectWithColumn> list, WSSaveProc proc )
        {
            int cnt = 0;
            foreach (DbObjectWithColumn item in list)
            {
                DataTable table = new DataTable();
                cnt += this.Update(item, table);
                return proc.Invoke(table);
            }
            return cnt;
        }

        public int Update(DbObjectWithColumn item, DataTable table)
        {
            this._action = DbAction.Update;
            this._source = AdapterSource.Table;
            this._table = table;

            //DataTable
            this._row = this._table.NewRow();
            item.Bind(this);

            this._action = DbAction.Unknown;
            this._source = AdapterSource.Reader;
            this._table = null;

            return 1;
        }

        public DbDataReader CreateReader( string statement)
        {
            DbCommand cmd = _provider.CreateCommand(statement);
            return cmd.ExecuteReader();
        }

        public T QueryLastOne<T>(string stmt) where T: DbObjectBase
        {
            DbCommand cmd = _provider.CreateCommand(stmt.ToString());

            Type typeid = typeof(T);
            DbDataReader reader = cmd.ExecuteReader(CommandBehavior.KeyInfo);
            this._reader = reader;
            T obj = null;
            this._source = AdapterSource.Reader;
            this._action = DbAction.Query;
            if (reader.Read())
            {
                obj = System.Activator.CreateInstance<T>();
                obj.Bind(this);
            }
            this._action = DbAction.Unknown;
            reader.Close();

            return obj;
        }

        public void Execute(string sql)
        {
            DbCommand cmd = _provider.CreateCommand(sql);

            cmd.ExecuteNonQuery();
        }
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;
using System.Data.OleDb;

namespace DbAccess
{
    public class OleDbProvider : IDbProvider
    {
        #region Private members
        private string _schema;
        private OleDbConnection _connection;
        #endregion

        public OleDbProvider( string dataSource, string userName, string password )
        {
            this._connection = new OleDbConnection();
            this._connection.ConnectionString = string.Format(
                "Provider=MSDAORA; Data Source='{0}'; User ID='{1}'; Password='{2}';",
                dataSource, userName, password);
        }

        #region IDbProvider Members

        public string Schema
        {
            get
            {
                return _schema;
            }
            set
            {
                this._schema = value;
            }
        }

        public void EnsureConnect()
        {
            if (this._connection.State == System.Data.ConnectionState.Closed)
            {
                this._connection.Open();
            }

            if ((this._connection.State & System.Data.ConnectionState.Broken) ==
                System.Data.ConnectionState.Broken)
            {
                this._connection.Close();
                this._connection.Open();
            }
        }

        public System.Data.Common.DbCommand CreateCommand()
        {
            if (this._connection == null)
            {
                throw new NullReferenceException("Refer to null OracleConnection");
            }
            this.EnsureConnect();

            return this._connection.CreateCommand();
        }

        public System.Data.Common.DbCommand CreateCommand(string statement)
        {
            if (this._connection == null)
            {
                throw new NullReferenceException("Refer to null OracleConnection");
            }

            this.EnsureConnect();

            string stmt = statement.Replace("{schema}", this._schema);

            DbCommand cmd = this._connection.CreateCommand();
            cmd.CommandText = stmt;
            return cmd;
        }

        /// <summary>
        /// Begins a transaction on this connection.
        /// </summary>
        /// <returns></returns>
        public System.Data.Common.DbTransaction BeginTransaction()
        {
            if (this._connection == null)
            {
                throw new NullReferenceException("Refer to null OracleConnection");
            }
            this.EnsureConnect();

            return this._connection.BeginTransaction();
        }

        #endregion
    }
}

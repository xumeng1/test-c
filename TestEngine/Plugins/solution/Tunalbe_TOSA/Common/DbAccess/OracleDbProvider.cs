using System;
using System.Collections.Generic;
using System.Text;
using System.Data.OracleClient;
using System.Data.Common;

namespace DbAccess
{
    public class OracleDbProvider : IDbProvider
    {
        #region Private members
        private string _schema;
        private OracleConnection _connection;
        #endregion

        public OracleDbProvider(string dataSource, string userName, string password)
        {
            OracleConnectionStringBuilder builder = new OracleConnectionStringBuilder();
            builder.DataSource = dataSource;
            builder.UserID = userName;
            builder.Password = password;

            this._connection = new OracleConnection(builder.ConnectionString);
        }

        #region IDbProvider Members

        public string Schema
        {
            get 
            {
                return _schema;
            }
            set
            {
                this._schema = value;
            }
        }

        public void EnsureConnect()
        {
            if ( this._connection.State == System.Data.ConnectionState.Closed )
            {
                this._connection.Open();
            }

            if ((this._connection.State & System.Data.ConnectionState.Broken) == 
                System.Data.ConnectionState.Broken )
            {
                this._connection.Close();
                this._connection.Open();
            }
        }

        public DbCommand CreateCommand()
        {
            if (this._connection == null)
            {
                throw new NullReferenceException("Refer to null OracleConnection");
            }
            this.EnsureConnect();

            return this._connection.CreateCommand();
        }

        public DbTransaction BeginTransaction()
        {
            if (this._connection == null)
            {
                throw new NullReferenceException("Refer to null OracleConnection");
            }
            this.EnsureConnect();

            return this._connection.BeginTransaction();
        }

        public DbCommand CreateCommand(string statement)
        {
            if (this._connection == null)
            {
                throw new NullReferenceException("Refer to null OracleConnection");
            }

            this.EnsureConnect();

            string stmt = statement.Replace("{schema}", this._schema);

            DbCommand cmd = this._connection.CreateCommand();
            cmd.CommandText = stmt;
            return cmd;
        }
        #endregion
    }
}

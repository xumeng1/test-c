using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;

namespace DbAccess
{
    public class SqlDbProvider : IDbProvider
    {
        private SqlConnection _connection;

        public SqlDbProvider( string connectionString )
        {
            this._connection = new SqlConnection(connectionString);
        }

        #region IDbProvider Members

        public void EnsureConnect()
        {
            if (this._connection.State == System.Data.ConnectionState.Closed)
            {
                this._connection.Open();
            }

            if ((this._connection.State & System.Data.ConnectionState.Broken) ==
                System.Data.ConnectionState.Broken)
            {
                this._connection.Close();
                this._connection.Open();
            }
        }

        public System.Data.Common.DbCommand CreateCommand()
        {
            this.EnsureConnect();
            return this._connection.CreateCommand();
        }

        public System.Data.Common.DbCommand CreateCommand(string statement)
        {
            this.EnsureConnect();
            SqlCommand cmd = this._connection.CreateCommand();
            cmd.CommandText = statement;
            return cmd;
        }

        public System.Data.Common.DbTransaction BeginTransaction()
        {
            this.EnsureConnect();
            return this._connection.BeginTransaction();
        }
        #endregion
    }
}

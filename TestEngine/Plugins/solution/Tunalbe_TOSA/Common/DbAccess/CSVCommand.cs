using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;

namespace DbAccess
{
    public class CSVCommand : DbCommand
    {
        private string _fileName;
        private CSVDbProvider _provider;

        public CSVCommand(string commandText)
        {
            this._fileName = commandText;
        }

        public CSVCommand(string commandText, CSVDbProvider provider)
        {
            this._fileName = commandText;
            this._provider = provider;
        }

        public CSVCommand(CSVDbProvider provider)
        {
            this._provider = provider;
        }

        public CSVCommand()
        {
        }

        public override void Cancel()
        {
            return;
        }

        public override string CommandText
        {
            get
            {
                return this._fileName;
            }
            set
            {
                this._fileName = value;
            }
        }

        public override int CommandTimeout
        {
            get
            {
                return 0;
            }
            set
            {
                return;             
            }
        }

        public override System.Data.CommandType CommandType
        {
            get
            {
                return System.Data.CommandType.Text;
            }
            set
            {
            }
        }

        protected override DbParameter CreateDbParameter()
        {
            return null;
        }

        protected override DbConnection DbConnection
        {
            get
            {
                return null;
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        protected override DbParameterCollection DbParameterCollection
        {
            get 
            {
                return null;
            }
        }

        protected override DbTransaction DbTransaction
        {
            get
            {
                return null;
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        public override bool DesignTimeVisible
        {
            get
            {
                return false;
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        /// <summary>
        /// Open csv file and returns a CSVDbReader to access the file.
        /// </summary>
        /// <param name="behavior"> currently not used.</param>
        /// <returns>DbDataReader, actually a CSVDbReader.</returns>
        protected override DbDataReader ExecuteDbDataReader(System.Data.CommandBehavior behavior)
        {
            CSVDbReader reader = new CSVDbReader(this._fileName, this._provider.HasHeader, this._provider.DefaultTableSchema);
            return reader;
        }

        public override int ExecuteNonQuery()
        {
            return 0;
        }

        public override object ExecuteScalar()
        {
            CSVDbReader reader = new CSVDbReader(this._fileName);
            if (reader.Read())
            {
                if (reader.FieldCount > 0)
                {
                    return reader[0];
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public override void Prepare()
        {
            return;
        }

        public override System.Data.UpdateRowSource UpdatedRowSource
        {
            get
            {
                return System.Data.UpdateRowSource.None;
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }
    }
}

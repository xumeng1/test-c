using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestSolution.IlmzCommonData
{
    /// <summary>
    /// DSDBR channel setup information
    /// </summary>
    public struct DsdbrChannelSetup
    {
        

        /// <summary>Phase current</summary>
        public double IPhase_mA;
        /// <summary>Rear current</summary>
        public double IRear_mA;
        /// <summary>Gain current</summary>
        public double IGain_mA;
        /// <summary>SOA current (positive or negative)</summary>
        public double ISoa_mA;
        /// <summary>Front section pair</summary>        
        public int FrontPair;
        /// <summary>Current of the 1st front section (typically constant)</summary>        
        public double IFsFirst_mA;
        /// <summary>Current of the 2nd front section (typically non-constant)</summary>        
        public double IFsSecond_mA;
        /// <summary> Rear soa current </summary>
        public double IRearSoa_mA;
    }
}
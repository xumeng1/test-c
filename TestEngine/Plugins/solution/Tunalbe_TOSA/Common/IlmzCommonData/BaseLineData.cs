using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestSolution.IlmzCommonData;

namespace Bookham.TestSolution.IlmzCommonData
{
    public struct BaseLineData
    {
        /// <summary>
        /// DSDBR setup
        /// </summary>
        public DsdbrChannelData Dsdbr;
        /// <summary>
        /// ImbLeft Dac
        /// </summary>
        public double MzLeftArmImb_mA;
        /// <summary>
        /// ImbRight Dac
        /// </summary>
        public double MzRightArmImb_mA;
        /// <summary>
        /// vcm value
        /// </summary>
        public double vcm;

        public double Dsdbr_Temp_C;

        public double Case_Temp_Mid_C;


    }
}

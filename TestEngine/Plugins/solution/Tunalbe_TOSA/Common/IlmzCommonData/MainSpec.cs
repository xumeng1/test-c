// Created by chongjian.liang 2013.10.14
using System;
using Bookham.TestEngine.Framework.Limits;

namespace Bookham.TestSolution.IlmzCommonData
{
    public struct MainSpec
    {
        public SpecLimit CH_MZ_CTRL_L_I;
        public SpecLimit CH_MZ_CTRL_R_I;
    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestSolution.IlmzCommonData
{
    /// <summary>
    /// Tcmz final test stage
    /// </summary>
    public enum FinalTestStage
    {
        /// <summary>Map stage</summary>
        Map,
        /// <summary>Final stage</summary>
        Final,
        /// <summary> Qual Test  pre-Bake</summary>
        Qual0,
        /// <summary> Qual Test after bake </summary>
        Qual,
        /// <summary> SPC </summary> // by tim to combine solution
        SPC,
        /// <summary> PreTest </summary> // by tim to combine solution
        PreTest,
        /// <summary> PostTest </summary> // by tim to combine solution
        PostTest,
        /// <summary> Final_OT </summary> // by tim to combine solution
        Final_OT,
        /// <summary> hitt_spc</summary>
        hitt_spc,
        /// <summary> hitt_debug01</summary>
        hitt_debug01,
    }
}

// Created by chongjian.liang 2013.10.11

using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestSolution.IlmzCommonData
{
    public struct GlobalConditions
    {
        public bool IsThermalPhase;
        public bool IsSwapWirebond;
        public string AsicType;
        public string CHIP_ID;
        public string WAFER_ID;
        public string COC_SN;
    }
}

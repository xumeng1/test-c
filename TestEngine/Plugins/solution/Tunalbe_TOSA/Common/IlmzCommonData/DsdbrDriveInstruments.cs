using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestSolution.IlmzCommonData
{
    /// <summary>
    /// Instruments group to drive DSDBR
    /// </summary>
    public enum DsdbrDriveInstruments
    {
        /// <summary>
        /// PXI instruments
        /// </summary>
        PXIInstruments,
        /// <summary>
        /// FCU instruments
        /// </summary>
        FCUInstruments,
        /// <summary>
        /// FCU control Asic
        /// </summary>
        FCU2AsicInstrument,
    }
}

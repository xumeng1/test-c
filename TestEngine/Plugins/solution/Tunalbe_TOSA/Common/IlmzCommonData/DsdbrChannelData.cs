using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestSolution.IlmzCommonData;

namespace Bookham.TestSolution.IlmzCommonData
{
    /// <summary>
    /// Dsdbr Channel data, as calculated by Close Grid application
    /// </summary>
    public struct DsdbrChannelData : IComparable
    {
        /// <summary>DSDBR setup</summary>        
        public DsdbrChannelSetup Setup;
        /// <summary>Itu channel index</summary>        
        public int ItuChannelIndex;
        /// <summary>Itu frequency in GHz</summary>        
        public double ItuFreq_GHz;
        /// <summary>Supermode number (starts at 0)</summary>        
        public int Supermode;
        /// <summary>Longitudinal mode number</summary>        
        public int LongitudinalMode;
        /// <summary>Middle line index</summary>        
        public double MiddleLineIndex;
        /// <summary>Frequency in GHz</summary>        
        public double Freq_GHz;
        /// <summary>Coarse Frequency in GHz</summary>        
        public double CoarseFreq_GHz;
        /// <summary>Optical Power Ratio used for coarse frequency calc ("Pseudo-frequency")</summary>        
        public double OptPowerRatio;
        /// <summary>Optical Power in mW</summary>        
        public double OptPower_mW;

        /// <summary>Bound above X</summary>        
        public double BoundAboveX;
        /// <summary>Bound above Y</summary>        
        public double BoundAboveY;
        /// <summary>Bound below X</summary>        
        public double BoundBelowX;
        /// <summary>Bound below Y</summary>        
        public double BoundBelowY;
        /// <summary>Bound left X</summary>        
        public double BoundLeftX;
        /// <summary>Bound left Y</summary>        
        public double BoundLeftY;
        /// <summary>Bound right X</summary>        
        public double BoundRightX;
        /// <summary>Bound right X</summary>        
        public double BoundRightY;

        #region IComparable Members

        /// <summary>
        /// Compare function using ItuChannelIndex
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int CompareTo(object obj)
        {
            DsdbrChannelData ch = (DsdbrChannelData)obj;

            int compareResult = this.ItuChannelIndex.CompareTo(ch.ItuChannelIndex);
            return compareResult;
        }

        #endregion
    }
}

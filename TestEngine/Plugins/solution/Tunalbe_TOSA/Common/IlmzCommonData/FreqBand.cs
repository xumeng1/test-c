using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestSolution.IlmzCommonData
{
    /// <summary>
    /// Frequency Band
    /// </summary>
    public enum FreqBand
    {
        /// <summary>C band</summary>
        C = 0,
        /// <summary>L band</summary>
        L = 1
    }
}

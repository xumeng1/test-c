using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestSolution.IlmzCommonData
{
    /// <summary>
    /// Mach-Zehnder bias data
    /// </summary>
    public struct MzData
    {
        /// <summary>Data arm bias for peak power</summary>
        public double LeftArmMod_Peak_V;
        /// <summary>Data-bar arm bias for peak power</summary>
        public double RightArmMod_Peak_V;
        /// <summary>Data arm bias for quadrature</summary>
        public double LeftArmMod_Quad_V;
        /// <summary>Data-bar arm bias for quadrature</summary>
        public double RightArmMod_Quad_V;
        /// <summary>Data arm bias for min power</summary>
        public double LeftArmMod_Min_V;
        /// <summary>Data-bar arm bias for min power</summary>
        public double RightArmMod_Min_V;
        /// <summary>Data control bias in V</summary>
        public double LeftArmImb_mA;
        /// <summary>Data-bar control bias in V</summary>
        public double RightArmImb_mA;
        /// <summary>Data control bias in DAC</summary>
        public double LeftArmImb_Dac;
        /// <summary>Data-bar control bias in DAC</summary>
        public double RightArmImb_Dac;
    }
}

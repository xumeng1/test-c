using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestSolution.IlmzCommonData
{
    /// <summary>
    /// Temperature to test TCMZ at
    /// </summary>
    public enum TcmzTestTemp
    {
        /// <summary>High</summary>
        High,
        /// <summary>Mid</summary>
        Mid,
        /// <summary>Low</summary>
        Low
    }
}

// Created by chongjian.liang 2013.10.14
using System;
using Bookham.TestEngine.Framework.Limits;

namespace Bookham.TestSolution.IlmzCommonData
{
    public class SpecLimit
    {
        private double low = double.NaN;

        public double Low
        {
            get
            {
                if (low == double.NaN)
                {
                    throw new Exception("SpecLimit.Low is not set before use, please contact software engineer.");
                }

                return low;
            }
            set { low = value; }
        }

        private double high = double.NaN;

        public double High
        {
            get
            {
                if (high == double.NaN)
                {
                    throw new Exception("SpecLimit.High is not set before use, please contact software engineer.");
                }
                
                return high;
            }
            set { high = value; }
        }

        public SpecLimit(double low, double high)
        {
            this.Low = low;
            this.High = high;
        }

        public SpecLimit(Specification spec, string limit)
        {
            if (spec.ParamLimitExists(limit))
            {
                ParamLimit specLimit = spec.GetParamLimit(limit);

                if (specLimit.LowLimit != null)
                {
                    this.Low = Convert.ToDouble(specLimit.LowLimit.ValueToString());
                }
                else
                {
                    // null means unlimited.
                    this.Low = -99999;
                }

                if (specLimit.HighLimit != null)
                {
                    this.High = Convert.ToDouble(specLimit.HighLimit.ValueToString());
                }
                else
                {
                    // null means unlimited.
                    this.High = 99999;
                }
            }
            else
            {
                throw new Exception(string.Format("The limit {0} doesn't exist in the limit file.", limit));
            }
        }
    }
}
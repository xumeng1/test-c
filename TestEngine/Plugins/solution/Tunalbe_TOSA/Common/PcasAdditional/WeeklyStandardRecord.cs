using System;
using System.Collections.Generic;
using System.Text;

namespace PcasAdditional
{
    public class WeeklyStandardRecord
        : DbAccess.DbObjectWithColumn
    {
        const string key_Id = "Id";
        const string key_Guid = "Guid";
        const string key_CreateTime = "CreateTime";
        const string key_TestStage = "TestStage";
        const string key_SerialNo = "SerialNo";

        /// <summary>
        /// Sequential Id of current object;
        /// </summary>
        public Int64 Id
        {
            get
            {
                return (Int64)base[key_Id];
            }
        }

        public Guid Guid
        {
            get
            {
                return (Guid)base[key_Guid];
            }
        }

        public DateTime CreateTime
        {
            get
            {
                return (DateTime)base[key_CreateTime];
            }
        }

        public string SerialNo
        {
            get
            {
                return base[key_SerialNo] as string;
            }
        }
    }
}

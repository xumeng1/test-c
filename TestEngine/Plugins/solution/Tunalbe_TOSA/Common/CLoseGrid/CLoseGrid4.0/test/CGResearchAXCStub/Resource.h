//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by CGResearch.rc
//
#define IDS_CGRESEARCH                  1
#define IDD_ABOUTBOX_CGRESEARCH         1
#define IDB_CGRESEARCH                  1
#define IDI_ABOUTDLL                    1
#define IDS_CGRESEARCH_PPG              2
#define IDS_CGRESEARCH_PPG_CAPTION      200
#define IDD_PROPPAGE_CGRESEARCH         200

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        202
#define _APS_NEXT_COMMAND_VALUE         32768
#define _APS_NEXT_CONTROL_VALUE         201
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif

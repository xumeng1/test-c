// Copyright (c) 2004 PXIT. All Rights Reserved
//
// FileName    : CT_CGSystemAXC.h
// Description : CT_CGSystemAXC class declaration
//               Component Test of CGSystemAXC ActiveX Control
//               Covers tests of basic operation with stubbed HW,
//               tests of the internal state machine,
//               tests for NULL pointer arguments
//               and tests for bad VARIANT arguments
//
// Rev#  | Date          | Author               | Description of change
// --------------------------------------------------------------
// 0.1   | 13 July 2004  | Frank D'Arcy         | Initial Draft
// 0.2   | 29 July 2004  | Frank D'Arcy         | Upgraded to test full API
// 0.3   | 30 July 2004  | Frank D'Arcy         | Added state m/c tests
//



#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

class CT_CGSystemAXC :
	public CppUnit::TestFixture
{

private:

public:

  void setUp();
  void tearDown();

  void test_getVersionInfo();
  void test_getVersionInfo_null_args();
  void test_startup();
  void test_setup();
  void test_shutdown();
  void test_rampdown();
  void test_getCurrent();
  void test_setCurrent();
  void test_setOutputConnection();
  void test_getOutputConnection();
  void test_get2DData();
  void test_get3DData();
  void test_startLaserScreeningOverallMap();
  void test_startLaserScreeningSMMap();
  void test_estimateITUOPs();
  void test_startLaserCharacterisation();
  void test_getPercentComplete();
  void test_getState();
  void test_getScreeningOverallMapResults();
  void test_getScreeningSMMapResults();
  void test_getCharacterisationResults();
  void test_collatePassFailResults();
  void test_loadOverallMap();
  void test_loadSMMap();
  void test_readVoltage();
  void test_readFrequency();
  void test_alignPowerRatioToFrequency();
  void test_readCoarseFrequency();
  void test_readPhotodiodeCurrent();
  void test_readOpticalPower();
  void test_readOpticalPowerAtFreq();
  void test_readTemperature();
  void test_getTECTempSetpoint();
  void test_setTECTempSetpoint();
  //void test_getTECPIDvalues();
  //void test_setTECPIDvalues();

  void test_states_st_sh();
  void test_states_st_se_sh();
  void test_states_runthru();


  CPPUNIT_TEST_SUITE( CT_CGSystemAXC );


  CPPUNIT_TEST( test_getVersionInfo );
  CPPUNIT_TEST( test_getVersionInfo_null_args );

#ifdef STUB_CGSYSTEM_API

  CPPUNIT_TEST( test_startup );
  CPPUNIT_TEST( test_setup );
  CPPUNIT_TEST( test_shutdown );
  CPPUNIT_TEST( test_rampdown );
  CPPUNIT_TEST( test_getCurrent );
  CPPUNIT_TEST( test_setCurrent );
  CPPUNIT_TEST( test_setOutputConnection );
  CPPUNIT_TEST( test_getOutputConnection );
  CPPUNIT_TEST( test_get2DData );
  CPPUNIT_TEST( test_get3DData );
  CPPUNIT_TEST( test_startLaserScreeningOverallMap );
  CPPUNIT_TEST( test_startLaserScreeningSMMap );
  CPPUNIT_TEST( test_estimateITUOPs );
  CPPUNIT_TEST( test_startLaserCharacterisation );
  CPPUNIT_TEST( test_getPercentComplete );
  CPPUNIT_TEST( test_getState );
  CPPUNIT_TEST( test_getScreeningOverallMapResults );
  CPPUNIT_TEST( test_getScreeningSMMapResults );
  CPPUNIT_TEST( test_getCharacterisationResults );
  CPPUNIT_TEST( test_collatePassFailResults );
  CPPUNIT_TEST( test_loadOverallMap );
  CPPUNIT_TEST( test_loadSMMap );
  CPPUNIT_TEST( test_readVoltage );
  CPPUNIT_TEST( test_readFrequency );
  CPPUNIT_TEST( test_alignPowerRatioToFrequency );
  CPPUNIT_TEST( test_readCoarseFrequency );
  CPPUNIT_TEST( test_readPhotodiodeCurrent );
  CPPUNIT_TEST( test_readOpticalPower );
  CPPUNIT_TEST( test_readOpticalPowerAtFreq );
  CPPUNIT_TEST( test_readTemperature );
  CPPUNIT_TEST( test_getTECTempSetpoint );
  CPPUNIT_TEST( test_setTECTempSetpoint );
  //CPPUNIT_TEST( test_getTECPIDvalues );
  //CPPUNIT_TEST( test_setTECPIDvalues );

  CPPUNIT_TEST( test_states_st_sh );
  CPPUNIT_TEST( test_states_st_se_sh );
  CPPUNIT_TEST( test_states_runthru );

#endif // STUB_CGSYSTEM_API


  CPPUNIT_TEST_SUITE_END();
};

CPPUNIT_TEST_SUITE_REGISTRATION( CT_CGSystemAXC );


// Copyright (c) 2004 PXIT. All Rights Reserved
//
// FileName    : ST_CGSystem.h
// Description : ST_CGSystem class declaration
//               System Test of PXIT CLose-Grid 4.0
//
// Rev#  | Date          | Author               | Description of change
// --------------------------------------------------------------
// 0.1   | 05 Aug 2004   | Frank D'Arcy         | Initial Draft
//


#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

class ST_CGSystem :
	public CppUnit::TestFixture
{

private:

public:

  void setUp();
  void tearDown();

  void test_loadOverallMap_p32();


  CPPUNIT_TEST_SUITE( ST_CGSystem );

  CPPUNIT_TEST( test_loadOverallMap_p32 );

  CPPUNIT_TEST_SUITE_END();
};

CPPUNIT_TEST_SUITE_REGISTRATION( ST_CGSystem );


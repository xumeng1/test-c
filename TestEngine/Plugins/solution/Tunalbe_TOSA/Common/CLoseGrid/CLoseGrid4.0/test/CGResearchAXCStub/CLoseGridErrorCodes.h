// Copyright (c) 2002 Tsunami Photonics Ltd. All Rights Reserved
///////////////////////////////////////////////////////////////////////////////
///
//	Contents	:	Defintion of HRESULT error codes which the CLose-Grid COM 
//					interface returns
//
//	Author		:	Richard Ashe
//
//	Date		:	17/07/2002
///
///////////////////////////////////////////////////////////////////////////////
///
//
#include <winerror.h>

////////////////////////////////////////////////
///
//	General Errors
// 
#define CG_ERROR_SYSTEM_ERROR						_HRESULT_TYPEDEF_(0x800F0250L)
#define CG_ERROR_INVALID_STATE						_HRESULT_TYPEDEF_(0x800F0251L)

#define CG_ERROR_WAVEMETER_OFFLINE					_HRESULT_TYPEDEF_(0x800F0252L)

#define CG_ERROR_REGISTRY_KEY						_HRESULT_TYPEDEF_(0x800F0253L)
#define CG_ERROR_REGISTRY_DATA						_HRESULT_TYPEDEF_(0x800F0254L)

#define CG_ERROR_LASER_ALIGN_LOWPOWER				_HRESULT_TYPEDEF_(0x800F0255L)
#define CG_ERROR_LASER_ALIGN_MAXPOWER				_HRESULT_TYPEDEF_(0x800F0256L)

#define CG_ERROR_PXIT_SETCURRENT					_HRESULT_TYPEDEF_(0x800F0257L)
#define CG_ERROR_PXIT_SEQUENCE						_HRESULT_TYPEDEF_(0x800F0258L)
#define CG_ERROR_PXIT_READPOWER						_HRESULT_TYPEDEF_(0x800F0259L)

#define CG_ERROR_LOGFILE_DISK_FULL					_HRESULT_TYPEDEF_(0x800F025AL)
#define	CG_ERROR_RESULTS_DISK_FULL					_HRESULT_TYPEDEF_(0x800F025BL)

#define CG_ERROR_PXIT_GETCURRENT					_HRESULT_TYPEDEF_(0x800F025CL)
//#define CG_ERROR_REFLECTOR_CURRENT				_HRESULT_TYPEDEF_(0x800F025DL)
//#define CG_ERROR_PHASE_CURRENT					_HRESULT_TYPEDEF_(0x800F025EL)

//#define CG_ERROR_GAIN_CURRENT_NOT_RAMPED			_HRESULT_TYPEDEF_(0x800F025FL)

//
////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////
///
//	Startup
//
/// Errors exclusive to Startup
#define CG_ERROR_LOGFILE_OPEN						_HRESULT_TYPEDEF_(0x800F0260L)
#define	CG_ERROR_LOGFILEPATHNAME_REGISTRY_KEY		_HRESULT_TYPEDEF_(0x800F0261L)
#define	CG_ERROR_LOGFILEPATHNAME_REGISTRY_DATA		_HRESULT_TYPEDEF_(0x800F0262L)
#define	CG_ERROR_LOGGINGLEVEL_REGISTRY_KEY			_HRESULT_TYPEDEF_(0x800F0263L)
#define	CG_ERROR_LOGGINGLEVEL_REGISTRY_DATA			_HRESULT_TYPEDEF_(0x800F0264L)
#define	CG_ERROR_LOGFILEMAXSIZE_REGISTRY_KEY		_HRESULT_TYPEDEF_(0x800F0265L)
#define	CG_ERROR_LOGFILEMAXSIZE_REGISTRY_DATA		_HRESULT_TYPEDEF_(0x800F0266L)

#define CG_ERROR_PXIT_MODULES_UNINITIALISED			_HRESULT_TYPEDEF_(0x800F0267L)
#define CG_ERROR_PXIT_MODULE_MISSING				_HRESULT_TYPEDEF_(0x800F0268L)
#define CG_ERROR_POWER_METER_MISSING				_HRESULT_TYPEDEF_(0x800F0269L)
#define CG_ERROR_NO_PXIT_MODULES_FOUND				_HRESULT_TYPEDEF_(0x800F026AL)

#define CG_ERROR_SET_OUTPUT_RANGE					_HRESULT_TYPEDEF_(0x800F026BL)
#define CG_ERROR_SET_COMPLIANCE_VOLTAGE				_HRESULT_TYPEDEF_(0x800F026CL)
#define CG_ERROR_SET_OUTPUT_MODE					_HRESULT_TYPEDEF_(0x800F026DL)
#define CG_ERROR_SET_MAX_CURRENT					_HRESULT_TYPEDEF_(0x800F026EL)

/// General errors also returned from startup
//	CG_ERROR_SYSTEM_ERROR
//	CG_ERROR_WAVEMETER_OFFLINE
//	CG_ERROR_INVALID_STATE
//	CG_ERROR_REGISTRY_KEY
//	CG_ERROR_REGISTRY_DATA
//	CG_ERROR_PXIT_SETCURRENT
//	CG_ERROR_LOGFILE_DISK_FULL
//	CG_ERROR_RESULTS_DISK_FULL
//	CG_ERROR_PXIT_SEQUENCE

//
////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////
///
//	Setup
//
/// Errors exclusive to Setup
#define	CG_ERROR_INVALID_LASER_ID					_HRESULT_TYPEDEF_(0x800F0277L)


//#define CG_ERROR_ALL_FAILED_CONNECTIVITY			_HRESULT_TYPEDEF_(0x800F026BL)
//#define CG_ERROR_G_R_FAILED_CONNECTIVITY			_HRESULT_TYPEDEF_(0x800F026CL)
//#define CG_ERROR_G_P_FAILED_CONNECTIVITY			_HRESULT_TYPEDEF_(0x800F026DL)
//#define CG_ERROR_R_P_FAILED_CONNECTIVITY			_HRESULT_TYPEDEF_(0x800F026EL)
//#define CG_ERROR_G_FAILED_CONNECTIVITY			_HRESULT_TYPEDEF_(0x800F026FL)
//#define CG_ERROR_R_FAILED_CONNECTIVITY				_HRESULT_TYPEDEF_(0x800F0270L)
//#define CG_ERROR_P_FAILED_CONNECTIVITY				_HRESULT_TYPEDEF_(0x800F0271L)



/// General errors also returned from setup
//	CG_ERROR_INVALID_STATE
//	CG_ERROR_LOGFILE_DISK_FULL
//	CG_ERROR_RESULTS_DISK_FULL
//	CG_ERROR_GAIN_CURRENT
	

//
////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////
///
//	Shutdown
//
/// Errors exclusive to shutdown
#define	CG_ERROR_LOGFILE_CLOSE						_HRESULT_TYPEDEF_(0x800F0278L)
#define CG_ERROR_PXIT_SHUTDOWN						_HRESULT_TYPEDEF_(0x800F0279L)

/// General errors also returned from Shutdown
//	CG_ERROR_INVALID_STATE
//	CG_ERROR_LOGFILE_DISK_FULL
//	CG_ERROR_RESULTS_DISK_FULL
//	CG_ERROR_SYSTEM_ERROR

//
////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////
///
//	getSystemStatus	
//
/// Errors exclusive to getSystemStatus

/// General errors also returned from getSystemStatus
//	CG_ERROR_LOGFILE_DISK_FULL
//	CG_ERROR_RESULTS_DISK_FULL

//
////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////
///
//	setCurrent	
//
/// Errors exclusive to setCurrent

/// General errors also returned from setCurrent
//	CG_ERROR_INVALID_STATE
//	CG_ERROR_LOGFILE_DISK_FULL
//	CG_ERROR_RESULTS_DISK_FULL
//	CG_ERROR_SYSTEM_ERROR
//	CG_ERROR_PXIT_MODULES_UNINITIALISED

//
////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////
///
//	rampDown	
//
/// Errors exclusive to rampDown

/// General errors also returned from rampDown
//	CG_ERROR_INVALID_STATE
//	CG_ERROR_LOGFILE_DISK_FULL
//	CG_ERROR_RESULTS_DISK_FULL
//	CG_ERROR_SYSTEM_ERROR
//	CG_ERROR_PXIT_MODULES_UNINITIALISED

//
////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////
///
//	getFrequency	
//
/// Errors exclusive to getFrequency

/// General errors also returned from getFrequency
//	CG_ERROR_INVALID_STATE
//	CG_ERROR_LOGFILE_DISK_FULL
//	CG_ERROR_RESULTS_DISK_FULL
//	CG_ERROR_SYSTEM_ERROR
//	CG_ERROR_PXIT_MODULES_UNINITIALISED
//	CG_ERROR_WAVEMETER_OFFLINE

//
////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////
///
//	getPower	
//
/// Errors exclusive to getPower

/// General errors also returned from getPower
//	CG_ERROR_INVALID_STATE
//	CG_ERROR_LOGFILE_DISK_FULL
//	CG_ERROR_RESULTS_DISK_FULL
//	CG_ERROR_SYSTEM_ERROR
//	CG_ERROR_PXIT_MODULES_UNINITIALISED
//	CG_ERROR_PXIT_READPOWER	

//
////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////
///
// Run LIV Test
//
/// Errors exclusive to livtest
//#define CG_ERROR_LIV_GAIN_SETTING_MISMATCH	_HRESULT_TYPEDEF_(0x800F027DL)
//#define CG_ERROR_LIV_STEPS_RANGE			_HRESULT_TYPEDEF_(0x800F027EL)
//#define CG_ERROR_LIV_STEP_DURATION_RANGE	_HRESULT_TYPEDEF_(0x800F027FL)

/// General errors also returned from liv test
//S_OK (operation successful)
//CG_ERROR_INVALID_STATE
//CG_ERROR_LOGFILE_DISK_FULL
//CG_ERROR_RESULTS_DISK_FULL
//CG_ERROR_SYSTEM_ERROR

//
////////////////////////////////////////////////////////////////////////////////////

//#define CG_ERROR_FUNCTION_NOT_SUPPORTED	    _HRESULT_TYPEDEF_(0x800F0280L)

/////////////////////////////////////////////////////////////////////////////////////
#define CG_ERROR_CONNECTIVITY_FAILED		_HRESULT_TYPEDEF_(0x800F0281L)

////////////////////////////////////////////////////////////////////////////////////
//
///
// getTemperature and setTemperature common errors
//CG_ERROR_INVALID_STATE
//CG_ERROR_RESULTS_DISK_FULL
//CG_ERROR_LOGFILE_DISK_FULL
//CG_ERROR_SYSTEM_ERROR
//CG_ERROR_PXIT_MODULES_UNINITIALISED
#define CG_ERROR_TEC_OFFLINE					_HRESULT_TYPEDEF_(0x800F0282L)
#define CG_ERROR_PXIT_READVOLTAGE				_HRESULT_TYPEDEF_(0x800F0283L)
#define CG_ERROR_ENABLED_SLOTS_NOT_CONTIGUOUS	_HRESULT_TYPEDEF_(0x800F0284L)

//
#define CG_ERROR_PXIT_STEP_SIZE					_HRESULT_TYPEDEF_(0x800F0285L)
#define CG_ERROR_OUTPUT_NOT_CONNECTED			_HRESULT_TYPEDEF_(0x800F0286L)
#define CG_ERROR_DUPLICATE_DEVICE_NAMES			_HRESULT_TYPEDEF_(0x800F0287L)

#define CG_ERROR_EMPTY_SLOTS_IN_SEQUENCE		_HRESULT_TYPEDEF_(0x800F0288L)


////////////////////////////////////////////////////////////////////////////////////
// Warnings

#define CG_WARNING_NONE								0
#define CG_WARNING_FLI_NOT_ALIGNED					1
#define CG_WARNING_FLI_CALIBRATION_LIMIT_EXCEEDED	2

//
////////////////////////////////////////////////////////////////////////////////////

// Copyright (c) 2004 PXIT. All Rights Reserved
//
// FileName    : CT_CGSystemAXC.cpp
// Description : CT_CGSystemAXC class function implementation
//               Component Test of CGSystemAXC ActiveX Control
//               Covers tests of basic operation with stubbed HW,
//               tests of the internal state machine,
//               tests for NULL pointer arguments
//               and tests for bad VARIANT arguments
//
// Rev#  | Date          | Author               | Description of change
// --------------------------------------------------------------
// 0.1   | 13 July 2004  | Frank D'Arcy         | Initial Draft
//



#include "stdafx.h"
#include "CT_CGSystemAXC.h"
#include "..\closegridsystemactrl1.h"
#include "productinfo.h"
#include "defaults.h"
#include "CGSystemErrors.h"


// Global instance of the LED Test Platform ActiveX Control
extern CClosegridsystemactrl1 g_test_cgsystem_axc;



// Macro to FAIL and print out an OLE Dispatch Exception if caught when it shouldn't
#define ASSERT_OLEDE_FAIL( EX ) \
		std::string errmsgstr = "OLE Dispatch Exception thrown"; \
		errmsgstr = errmsgstr + "\n  error code: "; \
		char buffer[20]; \
		_itoa( EX->m_wCode, buffer, 10 ); \
		errmsgstr = errmsgstr + buffer; \
		TCHAR errmsg[1000]; \
		EX->GetErrorMessage( errmsg, 1000 ); \
		errmsgstr = errmsgstr + "\n  error message: " + std::string(errmsg); \
		CPPUNIT_FAIL( errmsgstr );

// Macro to check the system state
#define ASSERT_SYSTEM_STATE( STATE ) \
	{ BSTR state_BSTR; \
	TRY { \
		g_led_test_platform_axc.getState( &state_BSTR ); \
	} CATCH( COleDispatchException, pEx ) { \
		ASSERT_OLEDE_FAIL( pEx ) \
	} END_CATCH \
	CPPUNIT_ASSERT( CString( state_BSTR ) == CString( STATE ) ); \
	SysFreeString( state_BSTR ); }


///////////////////////////////////////////////
// setUp - called before each test
//
void CT_CGSystemAXC::setUp()
{
	// call shutdown, it does nothing yet 
	TRY
	{
		g_test_cgsystem_axc.shutdown();
	}
	CATCH( COleDispatchException, pEx )
	{
	}
	END_CATCH
}

///////////////////////////////////////////////
// tearDown - called after each test
//
void CT_CGSystemAXC::tearDown() 
{
	// call shutdown, it does nothing yet 
	TRY
	{
		g_test_cgsystem_axc.shutdown();
	}
	CATCH( COleDispatchException, pEx )
	{
	}
	END_CATCH
}

///////////////////////////////////////////////
// test_getVersionInfo
//
// tests object.getVersionInfo()
//
void CT_CGSystemAXC::test_getVersionInfo() 
{
	BSTR product_name_BSTR;
	BSTR release_no_BSTR;
	BSTR copyright_notice_BSTR;
	BSTR label_BSTR;
	BSTR label_info_BSTR;
	BSTR label_comment_BSTR;
	BSTR build_configuration_BSTR;
	BSTR build_datetime_BSTR;

	TRY
	{
		g_test_cgsystem_axc.getVersionInfo( &product_name_BSTR,
											&release_no_BSTR,
											&copyright_notice_BSTR,
											&label_BSTR,
											&label_info_BSTR,
											&label_comment_BSTR,
											&build_configuration_BSTR,
											&build_datetime_BSTR);
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	CPPUNIT_ASSERT( CString( product_name_BSTR ) == CString( CGSYSTEM_PRODUCTNAME ) );
	CPPUNIT_ASSERT( CString( release_no_BSTR ) == CString( CGSYSTEM_RELEASENO ) );
	CPPUNIT_ASSERT( CString( copyright_notice_BSTR ) == CString( CGSYSTEM_COPYRIGHTNOTICE ) );

	SysFreeString( product_name_BSTR );
	SysFreeString( release_no_BSTR );
	SysFreeString( copyright_notice_BSTR );
	SysFreeString( label_BSTR );
	SysFreeString( label_info_BSTR );
	SysFreeString( label_comment_BSTR );
	SysFreeString( build_configuration_BSTR );
	SysFreeString( build_datetime_BSTR );

}






///////////////////////////////////////////////
// test_getVersionInfo_null_args
//
// tests object.getVersionInfo() blocks NULL pointer arguments
//
void CT_CGSystemAXC::test_getVersionInfo_null_args()
{
	BSTR product_name_BSTR;
	BSTR release_no_BSTR;
	BSTR copyright_notice_BSTR;
	BSTR label_BSTR;
	BSTR label_info_BSTR;
	BSTR label_comment_BSTR;
	BSTR build_configuration_BSTR;
	BSTR build_datetime_BSTR;


	// test NULL argument for product_name_BSTR

	WORD w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.getVersionInfo( NULL,
											&release_no_BSTR,
											&copyright_notice_BSTR,
											&label_BSTR,
											&label_info_BSTR,
											&label_comment_BSTR,
											&build_configuration_BSTR,
											&build_datetime_BSTR);
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );


	// test NULL argument for release_no_BSTR

	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.getVersionInfo( &product_name_BSTR,
											NULL,
											&copyright_notice_BSTR,
											&label_BSTR,
											&label_info_BSTR,
											&label_comment_BSTR,
											&build_configuration_BSTR,
											&build_datetime_BSTR);
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );


	// test NULL argument for copyright_notice_BSTR

	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.getVersionInfo( &product_name_BSTR,
											&release_no_BSTR,
											NULL,
											&label_BSTR,
											&label_info_BSTR,
											&label_comment_BSTR,
											&build_configuration_BSTR,
											&build_datetime_BSTR);
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );


	// test NULL argument for label_BSTR

	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.getVersionInfo( &product_name_BSTR,
											&release_no_BSTR,
											&copyright_notice_BSTR,
											NULL,
											&label_info_BSTR,
											&label_comment_BSTR,
											&build_configuration_BSTR,
											&build_datetime_BSTR);
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );


	// test NULL argument for label_info_BSTR

	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.getVersionInfo( &product_name_BSTR,
											&release_no_BSTR,
											&copyright_notice_BSTR,
											&label_BSTR,
											NULL,
											&label_comment_BSTR,
											&build_configuration_BSTR,
											&build_datetime_BSTR);
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );


	// test NULL argument for label_comment_BSTR

	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.getVersionInfo( &product_name_BSTR,
											&release_no_BSTR,
											&copyright_notice_BSTR,
											&label_BSTR,
											&label_info_BSTR,
											NULL,
											&build_configuration_BSTR,
											&build_datetime_BSTR);
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );


	// test NULL argument for build_configuration_BSTR

	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.getVersionInfo( &product_name_BSTR,
											&release_no_BSTR,
											&copyright_notice_BSTR,
											&label_BSTR,
											&label_info_BSTR,
											&label_comment_BSTR,
											NULL,
											&build_datetime_BSTR);
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );


	// test NULL argument for build_datetime_BSTR

	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.getVersionInfo( &product_name_BSTR,
											&release_no_BSTR,
											&copyright_notice_BSTR,
											&label_BSTR,
											&label_info_BSTR,
											&label_comment_BSTR,
											&build_configuration_BSTR,
											NULL);
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );

}




///////////////////////////////////////////////
// test_startup
//
// tests object.startup()
//
void CT_CGSystemAXC::test_startup()
{

	// call startup, it does nothing yet 
	TRY
	{
		g_test_cgsystem_axc.startup();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

}




///////////////////////////////////////////////
// test_setup
//
// tests object.setup()
//
void CT_CGSystemAXC::test_setup()
{

	// call startup
	TRY
	{
		g_test_cgsystem_axc.startup();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH


	char laser_type[] = "DSDBR01";
	char laser_id[] = "test_laser_id";
	BSTR date_time_stamp1;
	BSTR date_time_stamp2;

	// call setup 
	TRY
	{
		g_test_cgsystem_axc.setup(
			laser_type,
			laser_id,
			&date_time_stamp1 );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH


	CString date_time_stamp1_CString( date_time_stamp1 );
	CPPUNIT_ASSERT( date_time_stamp1_CString.GetLength() > 0 );


	// call setup 
	TRY
	{
		g_test_cgsystem_axc.setup(
			laser_type,
			laser_id,
			&date_time_stamp2 );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH


	CString date_time_stamp2_CString( date_time_stamp2 );
	CPPUNIT_ASSERT( date_time_stamp2_CString.GetLength() > 0 );

	// test date/time stamp is always unique
	CPPUNIT_ASSERT( date_time_stamp1_CString != date_time_stamp2_CString );




	// call setup with laser_type as NULL
	WORD w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.setup(
			NULL,
			laser_id,
			&date_time_stamp2 );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );


	// call setup with laser_id as NULL
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.setup(
			laser_type,
			NULL,
			&date_time_stamp2 );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );


	// call setup with p_date_time_stamp as NULL
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.setup(
			laser_type,
			laser_id,
			NULL );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );





	// call setup with laser_type as ""
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.setup(
			"",
			laser_id,
			&date_time_stamp2 );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_EMPTY_STRING_ARG_ERROR ) );


	// call setup with laser_id as ""
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.setup(
			laser_type,
			"",
			&date_time_stamp2 );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_EMPTY_STRING_ARG_ERROR ) );



	// call setup with laser_type as invalid
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.setup(
			"INVALIDLASERTYPE",
			laser_id,
			&date_time_stamp2 );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_LASERTYPE_ERROR ) );


	// call setup with laser_id as invalid
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.setup(
			laser_type,
			"invalid laser id",
			&date_time_stamp2 );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_LASER_ID_ERROR ) );
}




///////////////////////////////////////////////
// test_shutdown
//
// tests object.shutdown()
//
void CT_CGSystemAXC::test_shutdown()
{
	// call startup
	TRY
	{
		g_test_cgsystem_axc.startup();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH


	// call shutdown, it does nothing yet 
	TRY
	{
		g_test_cgsystem_axc.shutdown();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

}



///////////////////////////////////////////////
// test_rampdown
//
// tests object.rampdown()
//
void CT_CGSystemAXC::test_rampdown()
{
	// call startup
	TRY
	{
		g_test_cgsystem_axc.startup();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	BSTR date_time_stamp;

	// call setup
	TRY
	{
		g_test_cgsystem_axc.setup("DSDBR01","test_laser_id",&date_time_stamp);
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	// call rampdown, it does nothing yet 
	TRY
	{
		g_test_cgsystem_axc.rampdown();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

}






///////////////////////////////////////////////
// test_getCurrent
//
// tests object.getCurrent()
//
void CT_CGSystemAXC::test_getCurrent()
{
	// call startup
	TRY
	{
		g_test_cgsystem_axc.startup();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	BSTR date_time_stamp;

	// call setup
	TRY
	{
		g_test_cgsystem_axc.setup("DSDBR01","test_laser_id",&date_time_stamp);
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH


	double current;

	// call getCurrent with module_name as a null terminated char array

	TRY
	{
		g_test_cgsystem_axc.getCurrent( "test", &current );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH


	// call getCurrent with module_name as a BSTR
	CString module_name_CString("test");
	BSTR module_name_BSTR = module_name_CString.AllocSysString();

	TRY
	{
		g_test_cgsystem_axc.getCurrent( COLE2T(module_name_BSTR), &current );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH



	// call getCurrent with invalid module name
	WORD w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.getCurrent( "te st", &current );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_INVALID_MODULE_NAME_ERROR ) );


	// call getCurrent with NULL module name
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.getCurrent( NULL, &current );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );


	// call getCurrent with NULL p_current
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.getCurrent( "test", NULL );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );
}




///////////////////////////////////////////////
// test_setCurrent
//
// tests object.setCurrent()
//
void CT_CGSystemAXC::test_setCurrent()
{
	// call startup
	TRY
	{
		g_test_cgsystem_axc.startup();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	BSTR date_time_stamp;

	// call setup
	TRY
	{
		g_test_cgsystem_axc.setup("DSDBR01","test_laser_id",&date_time_stamp);
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	// call setCurrent with module_name as a null terminated char array
	TRY
	{
		g_test_cgsystem_axc.setCurrent( "test", 0 );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH


	// call setCurrent with module_name as a BSTR
	CString module_name_CString("test");
	BSTR module_name_BSTR = module_name_CString.AllocSysString();

	TRY
	{
		g_test_cgsystem_axc.setCurrent( COLE2T(module_name_BSTR), 0 );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH



	// call setCurrent with invalid module name
	WORD w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.setCurrent( "te st", 0 );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_INVALID_MODULE_NAME_ERROR ) );


	// call setCurrent with NULL module name
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.setCurrent( NULL, 0 );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );
}







///////////////////////////////////////////////
// test_setOutputConnection
//
// tests object.setOutputConnection()
//
void CT_CGSystemAXC::test_setOutputConnection()
{
	// call startup
	TRY
	{
		g_test_cgsystem_axc.startup();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	BSTR date_time_stamp;

	// call setup
	TRY
	{
		g_test_cgsystem_axc.setup("DSDBR01","test_laser_id",&date_time_stamp);
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	// call setOutputConnection with module_name as a null terminated char array
	TRY
	{
		g_test_cgsystem_axc.setOutputConnection( "test", 0 );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH


	// call setOutputConnection with invalid module name
	WORD w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.setOutputConnection( "te st", 0 );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_INVALID_MODULE_NAME_ERROR ) );


	// call setOutputConnection with NULL module name
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.setOutputConnection( NULL, 0 );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );
}





///////////////////////////////////////////////
// test_getOutputConnection
//
// tests object.getOutputConnection()
//
void CT_CGSystemAXC::test_getOutputConnection()
{
	// call startup
	TRY
	{
		g_test_cgsystem_axc.startup();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	BSTR date_time_stamp;

	// call setup
	TRY
	{
		g_test_cgsystem_axc.setup("DSDBR01","test_laser_id",&date_time_stamp);
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	short output_connection;

	// call getOutputConnection with module_name as a null terminated char array
	TRY
	{
		g_test_cgsystem_axc.getOutputConnection( "test", &output_connection );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH


	// call getOutputConnection with invalid module name
	WORD w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.getOutputConnection( "te st", &output_connection );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_INVALID_MODULE_NAME_ERROR ) );


	// call getOutputConnection with NULL pointer for module_name
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.getOutputConnection( NULL, &output_connection );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );


	// call getOutputConnection with NULL pointer for p_output_connection
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.getOutputConnection( "test", NULL );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );
}





///////////////////////////////////////////////
// test_get2DData
//
// tests object.get2DData()
//
void CT_CGSystemAXC::test_get2DData()
{
	// call startup
	TRY
	{
		g_test_cgsystem_axc.startup();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	BSTR date_time_stamp;

	// call setup
	TRY
	{
		g_test_cgsystem_axc.setup("DSDBR01","test_laser_id",&date_time_stamp);
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	LPCTSTR module_name = "test";
	VARIANT currents;
	short microseconds_per_point = 0;
	BSTR results_dir;


	//create an array bound
	SAFEARRAYBOUND rgsabound[1];
	//Set bounds of array
	rgsabound[0].lLbound	= 0;		//Lower bound
	rgsabound[0].cElements	= 100;		//Number of elements

	VariantInit(&currents);

	//Assigns the Variant to hold an array of doubles (real 8-byte numbers)
	currents.vt = VT_ARRAY | VT_R8;

	//Create the safe array of doubles, with the specified bounds, and only 1 dimension
	currents.parray = SafeArrayCreate( VT_R8, 1, rgsabound );
	//SafeArrayAllocData( variantArray.parray );

	double* p_data;
	//Fill in the buffer
	SafeArrayAccessData( currents.parray, (void**)&p_data );
	
	for(ULONG i = 0; i < rgsabound[0].cElements ; i++ )
	{
		// Fill safe array with double values between -1 and +1
		if( i == 0 )
			p_data[i] = (double)-1;
		else
			p_data[i] = ((double)-1) + ((double)2)*( ((double)i) / ((double)(rgsabound[0].cElements)) );
	}

	SafeArrayUnaccessData( currents.parray );


	// call get2DData with module_name as a null terminated char array
	TRY
	{
		g_test_cgsystem_axc.get2DData( module_name,
									   &currents,
									   microseconds_per_point,
									   &results_dir,
									   &date_time_stamp );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	CString results_dir_CString( results_dir );
	CString date_time_stamp_CString( date_time_stamp );

	CPPUNIT_ASSERT( results_dir_CString.GetLength() > 0 );
	CPPUNIT_ASSERT( date_time_stamp_CString.GetLength() > 0 );



	// call get2DData with module_name as NULL
	WORD w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.get2DData( NULL,
									   &currents,
									   microseconds_per_point,
									   &results_dir,
									   &date_time_stamp );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );



	// call get2DData with currents as NULL
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.get2DData( module_name,
									   NULL,
									   microseconds_per_point,
									   &results_dir,
									   &date_time_stamp );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );



	// call get2DData with results_dir as NULL
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.get2DData( module_name,
									   &currents,
									   microseconds_per_point,
									   NULL,
									   &date_time_stamp );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );




	// call get2DData with date_time_stamp as NULL
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.get2DData( module_name,
									   &currents,
									   microseconds_per_point,
									   &results_dir,
									   NULL );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );




	// negative tests with bad VARIANTs



	VARIANT null_array_currents;

	//create an array bound
	SAFEARRAYBOUND null_array_rgsabound[1];
	//Set bounds of array
	null_array_rgsabound[0].lLbound = 0;		//Lower bound
	null_array_rgsabound[0].cElements = 100;	//Number of elements

	VariantInit(&null_array_currents);

	//Assigns the Variant to hold an array of doubles (real 8-byte numbers)
	null_array_currents.vt = VT_ARRAY | VT_R8;
	null_array_currents.parray = NULL;

	// call get2DData with currents containing an empty array
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.get2DData( module_name,
									   &null_array_currents,
									   microseconds_per_point,
									   &results_dir,
									   &date_time_stamp );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_VARIANT_ARG_ERROR ) );





	VARIANT not1dArrayOfDoubles_currents;

	//create an array bound
	SAFEARRAYBOUND not1dArrayOfDoubles_rgsabound[1];
	//Set bounds of array
	not1dArrayOfDoubles_rgsabound[0].lLbound = 0;		//Lower bound
	not1dArrayOfDoubles_rgsabound[0].cElements = 100;	//Number of elements

	VariantInit(&not1dArrayOfDoubles_currents);

	//Assigns the Variant to hold an array of floats (real 4-byte numbers)
	not1dArrayOfDoubles_currents.vt = VT_ARRAY | VT_R4;

	//Create the safe array of doubles, with the specified bounds, and only 1 dimension
	not1dArrayOfDoubles_currents.parray = SafeArrayCreate( VT_R4, 1, not1dArrayOfDoubles_rgsabound );

	float* p_fdata;
	//Fill in the buffer
	SafeArrayAccessData( not1dArrayOfDoubles_currents.parray, (void**)&p_fdata );
	
	for(ULONG i = 0; i < not1dArrayOfDoubles_rgsabound[0].cElements ; i++ )
	{
		// Fill safe array with double values between -1 and +1
		if( i == 0 )
			p_fdata[i] = (float)-1;
		else
			p_fdata[i] = ((float)-1) + ((float)2)*( ((float)i) / ((float)(not1dArrayOfDoubles_rgsabound[0].cElements)) );
	}

	SafeArrayUnaccessData( not1dArrayOfDoubles_currents.parray );

	// call get2DData with currents containing an array that is not 1D array of doubles
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.get2DData( module_name,
									   &not1dArrayOfDoubles_currents,
									   microseconds_per_point,
									   &results_dir,
									   &date_time_stamp );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NOT1DDOUBLEARRAY_VARIANT_ARG_ERROR ) );






	VARIANT empty_currents;

	//create an array bound
	SAFEARRAYBOUND empty_rgsabound[1];
	//Set bounds of array
	empty_rgsabound[0].lLbound = 0;		//Lower bound
	empty_rgsabound[0].cElements = 0;	//Number of elements

	VariantInit(&empty_currents);

	//Assigns the Variant to hold an array of doubles (real 8-byte numbers)
	empty_currents.vt = VT_ARRAY | VT_R8;

	//Create the safe array of doubles, with the specified bounds, and only 1 dimension
	empty_currents.parray = SafeArrayCreate( VT_R8, 1, empty_rgsabound );


	// call get2DData with currents containing an empty array
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.get2DData( module_name,
									   &empty_currents,
									   microseconds_per_point,
									   &results_dir,
									   &date_time_stamp );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_EMPTY_VARIANT_ARG_ERROR ) );


	SysFreeString( results_dir );
	SysFreeString( date_time_stamp );

}


///////////////////////////////////////////////
// test_get3DData
//
// tests object.get3DData()
//
void CT_CGSystemAXC::test_get3DData()
{
	// call startup
	TRY
	{
		g_test_cgsystem_axc.startup();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	BSTR date_time_stamp;

	// call setup
	TRY
	{
		g_test_cgsystem_axc.setup("DSDBR01","test_laser_id",&date_time_stamp);
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	LPCTSTR module_name_x = "test_x";
	VARIANT currents_x;
	LPCTSTR module_name_y = "test_y";
	VARIANT currents_y;
	short microseconds_per_point = 0;
	short map_type = 1;
	BSTR results_dir;



	//create an array bound
	SAFEARRAYBOUND rgsabound_x[1];
	rgsabound_x[0].lLbound = 0;		//The first (and only) column of our array starts at 0.
	rgsabound_x[0].cElements = 100;	//and has 100 elements.

	//create the array
	SAFEARRAY FAR* p_test_safe_array_x;
	p_test_safe_array_x = SafeArrayCreate(VT_R8, 1, rgsabound_x); //create the array of doubles, one dimension
															  //with the bounds stored in rgsabound.
	//now access the safe array's data.
	double* p_data_x;
	HRESULT hr_x = SafeArrayAccessData(  p_test_safe_array_x, (void**)&p_data_x); //Get a pointer to the data.
	
	for(ULONG i = 0; i < rgsabound_x[0].cElements ; i++ )
	{
		// Fill safe array with double values between -1 and +1
		if( i == 0 )
			p_data_x[i] = (double)-1;
		else
			p_data_x[i] = ((double)-1) + ((double)2)*( ((double)i) / ((double)(rgsabound_x[0].cElements)) );
	}

	SafeArrayUnaccessData(p_test_safe_array_x);

	//To put the SafeArray in a Variant
	currents_x.parray = p_test_safe_array_x;
	currents_x.vt = VT_ARRAY|VT_R8; // state it's an array and what type it holds.



	//create an array bound
	SAFEARRAYBOUND rgsabound_y[1];
	rgsabound_y[0].lLbound = 0;		//The first (and only) column of our array starts at 0.
	rgsabound_y[0].cElements = 1000;	//and has 1000 elements.

	//create the array
	SAFEARRAY FAR* p_test_safe_array_y;
	p_test_safe_array_y = SafeArrayCreate(VT_R8, 1, rgsabound_y); //create the array of doubles, one dimension
															  //with the bounds stored in rgsabound.
	//now access the safe array's data.
	double* p_data_y;
	HRESULT hr_y = SafeArrayAccessData(  p_test_safe_array_y, (void**)&p_data_y); //Get a pointer to the data.
	
	for(ULONG i = 0; i < rgsabound_y[0].cElements ; i++ )
	{
		// Fill safe array with double values between -1 and +1
		if( i == 0 )
			p_data_y[i] = (double)-1;
		else
			p_data_y[i] = ((double)-1) + ((double)2)*( ((double)i) / ((double)(rgsabound_y[0].cElements)) );
	}

	SafeArrayUnaccessData(p_test_safe_array_y);

	//To put the SafeArray in a Variant
	currents_y.parray = p_test_safe_array_y;
	currents_y.vt = VT_ARRAY|VT_R8; // state it's an array and what type it holds.



	// call get3DData with module_name as a null terminated char array
	TRY
	{
		g_test_cgsystem_axc.get3DData( module_name_x,
									   &currents_x,
									   module_name_y,
									   &currents_y,
									   map_type,
									   microseconds_per_point,
									   &results_dir,
									   &date_time_stamp );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH


	CString results_dir_CString( results_dir );
	CString date_time_stamp_CString( date_time_stamp );

	CPPUNIT_ASSERT( results_dir_CString.GetLength() > 0 );
	CPPUNIT_ASSERT( date_time_stamp_CString.GetLength() > 0 );




	// call get3DData with module_name_x as NULL
	WORD w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.get3DData( NULL,
									   &currents_x,
									   module_name_y,
									   &currents_y,
									   map_type,
									   microseconds_per_point,
									   &results_dir,
									   &date_time_stamp );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );


	// call get3DData with module_name_y as NULL
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.get3DData( module_name_x,
									   &currents_x,
									   NULL,
									   &currents_y,
									   map_type,
									   microseconds_per_point,
									   &results_dir,
									   &date_time_stamp );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );





	// call get3DData with currents_x as NULL
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.get3DData( module_name_x,
									   NULL,
									   module_name_y,
									   &currents_y,
									   map_type,
									   microseconds_per_point,
									   &results_dir,
									   &date_time_stamp );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );




	// call get3DData with currents_y as NULL
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.get3DData( module_name_x,
									   &currents_x,
									   module_name_y,
									   NULL,
									   map_type,
									   microseconds_per_point,
									   &results_dir,
									   &date_time_stamp );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );



	// call get3DData with results_dir as NULL
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.get3DData( module_name_x,
									   &currents_x,
									   module_name_y,
									   &currents_y,
									   map_type,
									   microseconds_per_point,
									   NULL,
									   &date_time_stamp );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );




	// call get3DData with currents as date_time_stamp as NULL
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.get3DData( module_name_x,
									   &currents_x,
									   module_name_y,
									   &currents_y,
									   map_type,
									   microseconds_per_point,
									   &results_dir,
									   NULL );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );






	// negative tests with bad VARIANTs



	VARIANT null_array_currents;

	//create an array bound
	SAFEARRAYBOUND null_array_rgsabound[1];
	//Set bounds of array
	null_array_rgsabound[0].lLbound = 0;		//Lower bound
	null_array_rgsabound[0].cElements = 100;	//Number of elements

	VariantInit(&null_array_currents);

	//Assigns the Variant to hold an array of doubles (real 8-byte numbers)
	null_array_currents.vt = VT_ARRAY | VT_R8;
	null_array_currents.parray = NULL;

	// call get3DData with currents_x containing a NULL array
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.get3DData( module_name_x,
									   &null_array_currents,
									   module_name_y,
									   &currents_y,
									   map_type,
									   microseconds_per_point,
									   &results_dir,
									   &date_time_stamp );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_VARIANT_ARG_ERROR ) );


	// call get3DData with currents_y containing a NULL array
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.get3DData( module_name_x,
									   &currents_x,
									   module_name_y,
									   &null_array_currents,
									   map_type,
									   microseconds_per_point,
									   &results_dir,
									   &date_time_stamp );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_VARIANT_ARG_ERROR ) );



	VARIANT not1dArrayOfDoubles_currents;

	//create an array bound
	SAFEARRAYBOUND not1dArrayOfDoubles_rgsabound[1];
	//Set bounds of array
	not1dArrayOfDoubles_rgsabound[0].lLbound = 0;		//Lower bound
	not1dArrayOfDoubles_rgsabound[0].cElements = 100;	//Number of elements

	VariantInit(&not1dArrayOfDoubles_currents);

	//Assigns the Variant to hold an array of floats (real 4-byte numbers)
	not1dArrayOfDoubles_currents.vt = VT_ARRAY | VT_R4;

	//Create the safe array of doubles, with the specified bounds, and only 1 dimension
	not1dArrayOfDoubles_currents.parray = SafeArrayCreate( VT_R4, 1, not1dArrayOfDoubles_rgsabound );

	float* p_fdata;
	//Fill in the buffer
	SafeArrayAccessData( not1dArrayOfDoubles_currents.parray, (void**)&p_fdata );
	
	for(ULONG i = 0; i < not1dArrayOfDoubles_rgsabound[0].cElements ; i++ )
	{
		// Fill safe array with double values between -1 and +1
		if( i == 0 )
			p_fdata[i] = (float)-1;
		else
			p_fdata[i] = ((float)-1) + ((float)2)*( ((float)i) / ((float)(not1dArrayOfDoubles_rgsabound[0].cElements)) );
	}

	SafeArrayUnaccessData( not1dArrayOfDoubles_currents.parray );


	// call get3DData with currents_x containing an array that is not 1D array of doubles
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.get3DData( module_name_x,
									   &not1dArrayOfDoubles_currents,
									   module_name_y,
									   &currents_y,
									   map_type,
									   microseconds_per_point,
									   &results_dir,
									   &date_time_stamp );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NOT1DDOUBLEARRAY_VARIANT_ARG_ERROR ) );


	// call get3DData with currents_y containing an array that is not 1D array of doubles
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.get3DData( module_name_x,
									   &currents_x,
									   module_name_y,
									   &not1dArrayOfDoubles_currents,
									   map_type,
									   microseconds_per_point,
									   &results_dir,
									   &date_time_stamp );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NOT1DDOUBLEARRAY_VARIANT_ARG_ERROR ) );





	VARIANT empty_currents;

	//create an array bound
	SAFEARRAYBOUND empty_rgsabound[1];
	//Set bounds of array
	empty_rgsabound[0].lLbound = 0;		//Lower bound
	empty_rgsabound[0].cElements = 0;	//Number of elements

	VariantInit(&empty_currents);

	//Assigns the Variant to hold an array of doubles (real 8-byte numbers)
	empty_currents.vt = VT_ARRAY | VT_R8;

	//Create the safe array of doubles, with the specified bounds, and only 1 dimension
	empty_currents.parray = SafeArrayCreate( VT_R8, 1, empty_rgsabound );




	// call get3DData with currents_x containing an empty array
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.get3DData( module_name_x,
									   &empty_currents,
									   module_name_y,
									   &currents_y,
									   map_type,
									   microseconds_per_point,
									   &results_dir,
									   &date_time_stamp );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_EMPTY_VARIANT_ARG_ERROR ) );


	// call get3DData with currents_y containing an empty array
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.get3DData( module_name_x,
									   &currents_x,
									   module_name_y,
									   &empty_currents,
									   map_type,
									   microseconds_per_point,
									   &results_dir,
									   &date_time_stamp );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_EMPTY_VARIANT_ARG_ERROR ) );







	SysFreeString( results_dir );
	SysFreeString( date_time_stamp );
}



///////////////////////////////////////////////
// test_startLaserScreeningOverallMap
//
// tests object.startLaserScreeningOverallMap()
//
void CT_CGSystemAXC::test_startLaserScreeningOverallMap()
{
	// call startup
	TRY
	{
		g_test_cgsystem_axc.startup();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	BSTR date_time_stamp;

	// call setup
	TRY
	{
		g_test_cgsystem_axc.setup("DSDBR01","test_laser_id",&date_time_stamp);
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	// call startLaserScreeningOverallMap normally
	TRY
	{
		g_test_cgsystem_axc.startLaserScreeningOverallMap();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	BSTR system_state;
	short overall_map_loaded;
	short supermodes_found_count;
	short supermode_map_loaded_count;
	short ITUOP_estimate_count;
	short ITUOP_count;

	// call getState
	TRY
	{
		g_test_cgsystem_axc.getState(
			&system_state,
			&overall_map_loaded,
			&supermodes_found_count,
			&supermode_map_loaded_count,
			&ITUOP_estimate_count,
			&ITUOP_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	CPPUNIT_ASSERT( CString( system_state ) == BUSY_STATE );

	// wait for screening to complete
	short percent_complete = 0;
	while( percent_complete < 100 )
	{
		Sleep(100);

		// call getPercentComplete
		TRY
		{
			g_test_cgsystem_axc.getPercentComplete( &percent_complete );
		}
		CATCH( COleDispatchException, pEx )
		{
			ASSERT_OLEDE_FAIL( pEx )
		}
		END_CATCH

		CPPUNIT_ASSERT( percent_complete >= 0 && percent_complete <= 100 );
	}

	CPPUNIT_ASSERT( percent_complete == 100 );

	// call getState
	TRY
	{
		g_test_cgsystem_axc.getState(
			&system_state,
			&overall_map_loaded,
			&supermodes_found_count,
			&supermode_map_loaded_count,
			&ITUOP_estimate_count,
			&ITUOP_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	CPPUNIT_ASSERT( CString( system_state ) == COMPLETE_STATE );
}

///////////////////////////////////////////////
// test_startLaserScreeningSMMap
//
// tests object.startLaserScreeningSMMap()
//
void CT_CGSystemAXC::test_startLaserScreeningSMMap()
{
	// call startup
	TRY
	{
		g_test_cgsystem_axc.startup();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	BSTR date_time_stamp;

	// call setup
	TRY
	{
		g_test_cgsystem_axc.setup("DSDBR01","test_laser_id",&date_time_stamp);
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	// call startLaserScreeningOverallMap normally
	TRY
	{
		g_test_cgsystem_axc.startLaserScreeningOverallMap();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH


	// wait for screening to complete
	short percent_complete = 0;
	while( percent_complete < 100 )
	{
		Sleep(100);

		// call getPercentComplete
		TRY
		{
			g_test_cgsystem_axc.getPercentComplete( &percent_complete );
		}
		CATCH( COleDispatchException, pEx )
		{
			ASSERT_OLEDE_FAIL( pEx )
		}
		END_CATCH

		CPPUNIT_ASSERT( percent_complete >= 0 && percent_complete <= 100 );
	}

	CPPUNIT_ASSERT( percent_complete == 100 );



	short supermode_number = 1;

	// call startLaserScreeningSMMap normally
	TRY
	{
		g_test_cgsystem_axc.startLaserScreeningSMMap( supermode_number );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH


	BSTR system_state;
	short overall_map_loaded;
	short supermodes_found_count;
	short supermode_map_loaded_count;
	short ITUOP_estimate_count;
	short ITUOP_count;

	// call getState
	TRY
	{
		g_test_cgsystem_axc.getState(
			&system_state,
			&overall_map_loaded,
			&supermodes_found_count,
			&supermode_map_loaded_count,
			&ITUOP_estimate_count,
			&ITUOP_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	CPPUNIT_ASSERT( CString( system_state ) == BUSY_STATE );


	// wait for screening to complete
	percent_complete = 0;
	while( percent_complete < 100 )
	{
		Sleep(100);

		// call getPercentComplete
		TRY
		{
			g_test_cgsystem_axc.getPercentComplete( &percent_complete );
		}
		CATCH( COleDispatchException, pEx )
		{
			ASSERT_OLEDE_FAIL( pEx )
		}
		END_CATCH

		CPPUNIT_ASSERT( percent_complete >= 0 && percent_complete <= 100 );
	}

	CPPUNIT_ASSERT( percent_complete == 100 );




	// call getState
	TRY
	{
		g_test_cgsystem_axc.getState(
			&system_state,
			&overall_map_loaded,
			&supermodes_found_count,
			&supermode_map_loaded_count,
			&ITUOP_estimate_count,
			&ITUOP_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	CPPUNIT_ASSERT( CString( system_state ) == COMPLETE_STATE );
}

///////////////////////////////////////////////
// test_estimateITUOPs
//
// tests object.estimateITUOPs()
//
void CT_CGSystemAXC::test_estimateITUOPs()
{
	// call startup
	TRY
	{
		g_test_cgsystem_axc.startup();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	BSTR date_time_stamp;

	// call setup
	TRY
	{
		g_test_cgsystem_axc.setup("DSDBR01","test_laser_id",&date_time_stamp);
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	// call startLaserScreeningOverallMap normally
	TRY
	{
		g_test_cgsystem_axc.startLaserScreeningOverallMap();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH



	// wait for screening to complete
	short percent_complete = 0;
	while( percent_complete < 100 )
	{
		Sleep(100);

		// call getPercentComplete
		TRY
		{
			g_test_cgsystem_axc.getPercentComplete( &percent_complete );
		}
		CATCH( COleDispatchException, pEx )
		{
			ASSERT_OLEDE_FAIL( pEx )
		}
		END_CATCH

		CPPUNIT_ASSERT( percent_complete >= 0 && percent_complete <= 100 );
	}

	CPPUNIT_ASSERT( percent_complete == 100 );




	BSTR results_dir;
	short ITUOP_estimates_count;

	// call estimateITUOPs normally
	TRY
	{
		g_test_cgsystem_axc.estimateITUOPs(
			&results_dir,
			&date_time_stamp,
			&ITUOP_estimates_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	SysFreeString( results_dir );
	SysFreeString( date_time_stamp );


	// call estimateITUOPs with p_results_dir as NULL
	WORD w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.estimateITUOPs(
			NULL,
			&date_time_stamp,
			&ITUOP_estimates_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );


	// call estimateITUOPs with p_date_time_stamp as NULL
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.estimateITUOPs(
			&results_dir,
			NULL,
			&ITUOP_estimates_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );


	// call estimateITUOPs with p_ITUOP_estimates_count as NULL
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.estimateITUOPs(
			&results_dir,
			&date_time_stamp,
			NULL );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );

}

///////////////////////////////////////////////
// test_startLaserCharacterisation
//
// tests object.startLaserCharacterisation()
//
void CT_CGSystemAXC::test_startLaserCharacterisation()
{
	// call startup
	TRY
	{
		g_test_cgsystem_axc.startup();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	BSTR date_time_stamp;

	// call setup
	TRY
	{
		g_test_cgsystem_axc.setup("DSDBR01","test_laser_id",&date_time_stamp);
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	// call startLaserScreeningOverallMap normally
	TRY
	{
		g_test_cgsystem_axc.startLaserScreeningOverallMap();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH



	// wait for screening to complete
	short percent_complete = 0;
	while( percent_complete < 100 )
	{
		Sleep(100);

		// call getPercentComplete
		TRY
		{
			g_test_cgsystem_axc.getPercentComplete( &percent_complete );
		}
		CATCH( COleDispatchException, pEx )
		{
			ASSERT_OLEDE_FAIL( pEx )
		}
		END_CATCH

		CPPUNIT_ASSERT( percent_complete >= 0 && percent_complete <= 100 );
	}

	CPPUNIT_ASSERT( percent_complete == 100 );


	// call startLaserCharacterisation normally
	TRY
	{
		g_test_cgsystem_axc.startLaserCharacterisation();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	BSTR system_state;
	short overall_map_loaded;
	short supermodes_found_count;
	short supermode_map_loaded_count;
	short ITUOP_estimate_count;
	short ITUOP_count;

	// call getState
	TRY
	{
		g_test_cgsystem_axc.getState(
			&system_state,
			&overall_map_loaded,
			&supermodes_found_count,
			&supermode_map_loaded_count,
			&ITUOP_estimate_count,
			&ITUOP_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	CPPUNIT_ASSERT( CString( system_state ) == BUSY_STATE );

	// wait for characterisation to complete
	percent_complete = 0;
	while( percent_complete < 100 )
	{
		Sleep(100);

		// call getPercentComplete
		TRY
		{
			g_test_cgsystem_axc.getPercentComplete( &percent_complete );
		}
		CATCH( COleDispatchException, pEx )
		{
			ASSERT_OLEDE_FAIL( pEx )
		}
		END_CATCH

		CPPUNIT_ASSERT( percent_complete >= 0 && percent_complete <= 100 );
	}

	CPPUNIT_ASSERT( percent_complete == 100 );



	// call getState
	TRY
	{
		g_test_cgsystem_axc.getState(
			&system_state,
			&overall_map_loaded,
			&supermodes_found_count,
			&supermode_map_loaded_count,
			&ITUOP_estimate_count,
			&ITUOP_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	CPPUNIT_ASSERT( CString( system_state ) == COMPLETE_STATE );
}


///////////////////////////////////////////////
// test_getPercentComplete
//
// tests object.getPercentComplete()
//
void CT_CGSystemAXC::test_getPercentComplete()
{
	// call startup
	TRY
	{
		g_test_cgsystem_axc.startup();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	BSTR date_time_stamp;

	// call setup
	TRY
	{
		g_test_cgsystem_axc.setup("DSDBR01","test_laser_id",&date_time_stamp);
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH


	short percent_complete;

	// call getPercentComplete normally
	TRY
	{
		g_test_cgsystem_axc.getPercentComplete( &percent_complete );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	CPPUNIT_ASSERT( percent_complete >= 0 && percent_complete <= 100 );


	// call getPercentComplete with p_percent_complete as NULL
	WORD w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.getPercentComplete( NULL );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );


}

///////////////////////////////////////////////
// test_getState
//
// tests object.getState()
//
void CT_CGSystemAXC::test_getState()
{
	BSTR system_state;
	short overall_map_loaded;
	short supermodes_found_count;
	short supermode_map_loaded_count;
	short ITUOP_estimate_count;
	short ITUOP_count;

	// call getState normally
	TRY
	{
		g_test_cgsystem_axc.getState(
			&system_state,
			&overall_map_loaded,
			&supermodes_found_count,
			&supermode_map_loaded_count,
			&ITUOP_estimate_count,
			&ITUOP_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	CString system_state_CString( system_state );

	CPPUNIT_ASSERT( system_state_CString.GetLength() > 0 );

	// call getState with p_state as NULL
	WORD w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.getState(
			NULL,
			&overall_map_loaded,
			&supermodes_found_count,
			&supermode_map_loaded_count,
			&ITUOP_estimate_count,
			&ITUOP_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );

	// call getState with p_overall_map_loaded as NULL
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.getState(
			&system_state,
			NULL,
			&supermodes_found_count,
			&supermode_map_loaded_count,
			&ITUOP_estimate_count,
			&ITUOP_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );


	// call getState with p_supermodes_found_count as NULL
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.getState(
			&system_state,
			&overall_map_loaded,
			NULL,
			&supermode_map_loaded_count,
			&ITUOP_estimate_count,
			&ITUOP_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );


	// call getState with p_supermode_map_loaded_count as NULL
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.getState(
			&system_state,
			&overall_map_loaded,
			&supermodes_found_count,
			NULL,
			&ITUOP_estimate_count,
			&ITUOP_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );


	// call getState with p_ITUOP_estimate_count as NULL
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.getState(
			&system_state,
			&overall_map_loaded,
			&supermodes_found_count,
			&supermode_map_loaded_count,
			NULL,
			&ITUOP_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );


	// call getState with p_ITUOP_count as NULL
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.getState(
			&system_state,
			&overall_map_loaded,
			&supermodes_found_count,
			&supermode_map_loaded_count,
			&ITUOP_estimate_count,
			NULL );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );

	SysFreeString( system_state );
}



///////////////////////////////////////////////
// test_getScreeningOverallMapResults
//
// tests object.getScreeningOverallMapResults() arguments only
//
void CT_CGSystemAXC::test_getScreeningOverallMapResults()
{
	// call startup
	TRY
	{
		g_test_cgsystem_axc.startup();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	BSTR date_time_stamp;

	// call setup
	TRY
	{
		g_test_cgsystem_axc.setup("DSDBR01","test_laser_id",&date_time_stamp);
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	// call startLaserScreeningOverallMap normally
	TRY
	{
		g_test_cgsystem_axc.startLaserScreeningOverallMap();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	BSTR system_state;
	short overall_map_loaded;
	short supermodes_found_count;
	short supermode_map_loaded_count;
	short ITUOP_estimate_count;
	short ITUOP_count;

	// call getState
	TRY
	{
		g_test_cgsystem_axc.getState(
			&system_state,
			&overall_map_loaded,
			&supermodes_found_count,
			&supermode_map_loaded_count,
			&ITUOP_estimate_count,
			&ITUOP_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	CPPUNIT_ASSERT( CString( system_state ) == BUSY_STATE );

	// wait for screening to complete
	short percent_complete = 0;
	while( percent_complete < 100 )
	{
		Sleep(100);

		// call getPercentComplete
		TRY
		{
			g_test_cgsystem_axc.getPercentComplete( &percent_complete );
		}
		CATCH( COleDispatchException, pEx )
		{
			ASSERT_OLEDE_FAIL( pEx )
		}
		END_CATCH

		CPPUNIT_ASSERT( percent_complete >= 0 && percent_complete <= 100 );
	}

	CPPUNIT_ASSERT( percent_complete == 100 );


	// call getState
	TRY
	{
		g_test_cgsystem_axc.getState(
			&system_state,
			&overall_map_loaded,
			&supermodes_found_count,
			&supermode_map_loaded_count,
			&ITUOP_estimate_count,
			&ITUOP_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	CPPUNIT_ASSERT( CString( system_state ) == COMPLETE_STATE );


	BSTR results_dir;
	short screening_overall_map_passed = FALSE;
	short supermode_count = -1;

	// call getScreeningOverallMapResults normally
	TRY
	{
		g_test_cgsystem_axc.getScreeningOverallMapResults(
			&results_dir,
			&date_time_stamp,
			&screening_overall_map_passed,
			&supermode_count);
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	CString results_dir_CString( results_dir );
	CString date_time_stamp_CString( date_time_stamp );

	CPPUNIT_ASSERT( results_dir_CString.GetLength() > 0 );
	CPPUNIT_ASSERT( date_time_stamp_CString.GetLength() > 0 );
	CPPUNIT_ASSERT( screening_overall_map_passed == TRUE );
	CPPUNIT_ASSERT( supermode_count >= 0 );


	// call getScreeningOverallMapResults with p_results_dir as NULL
	WORD w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.getScreeningOverallMapResults(
			NULL,
			&date_time_stamp,
			&screening_overall_map_passed,
			&supermode_count);
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );


	// call getScreeningOverallMapResults with date_time_stamp as NULL
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.getScreeningOverallMapResults(
			&results_dir,
			NULL,
			&screening_overall_map_passed,
			&supermode_count);
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );



	// call getScreeningOverallMapResults with screening_overall_map_passed as NULL
		
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.getScreeningOverallMapResults(
			&results_dir,
			&date_time_stamp,
			NULL,
			&supermode_count);
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );




	// call getScreeningOverallMapResults with supermode_count as NULL
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.getScreeningOverallMapResults(
			&results_dir,
			&date_time_stamp,
			&screening_overall_map_passed,
			NULL);
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );



	SysFreeString( results_dir );
	SysFreeString( date_time_stamp );
}




///////////////////////////////////////////////
// test_getScreeningSMMapResults
//
// tests object.getScreeningSMMapResults()
//
void CT_CGSystemAXC::test_getScreeningSMMapResults()
{
	// call startup
	TRY
	{
		g_test_cgsystem_axc.startup();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	BSTR date_time_stamp;

	// call setup
	TRY
	{
		g_test_cgsystem_axc.setup("DSDBR01","test_laser_id",&date_time_stamp);
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	// call startLaserScreeningOverallMap normally
	TRY
	{
		g_test_cgsystem_axc.startLaserScreeningOverallMap();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH


	BSTR system_state;
	short overall_map_loaded;
	short supermodes_found_count;
	short supermode_map_loaded_count;
	short ITUOP_estimate_count;
	short ITUOP_count;

	// call getState
	TRY
	{
		g_test_cgsystem_axc.getState(
			&system_state,
			&overall_map_loaded,
			&supermodes_found_count,
			&supermode_map_loaded_count,
			&ITUOP_estimate_count,
			&ITUOP_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	CPPUNIT_ASSERT( CString( system_state ) == BUSY_STATE );

	// wait for screening to complete
	short percent_complete = 0;
	while( percent_complete < 100 )
	{
		Sleep(100);

		// call getPercentComplete
		TRY
		{
			g_test_cgsystem_axc.getPercentComplete( &percent_complete );
		}
		CATCH( COleDispatchException, pEx )
		{
			ASSERT_OLEDE_FAIL( pEx )
		}
		END_CATCH

		CPPUNIT_ASSERT( percent_complete >= 0 && percent_complete <= 100 );
	}

	CPPUNIT_ASSERT( percent_complete == 100 );


	// call getState
	TRY
	{
		g_test_cgsystem_axc.getState(
			&system_state,
			&overall_map_loaded,
			&supermodes_found_count,
			&supermode_map_loaded_count,
			&ITUOP_estimate_count,
			&ITUOP_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	CPPUNIT_ASSERT( CString( system_state ) == COMPLETE_STATE );


	short supermode_number = 1;
	BSTR results_dir;
	short screening_sm_map_passed = FALSE;
	short longitudinal_mode_count = 0;

	// call getScreeningSMMapResults normally
	TRY
	{
		g_test_cgsystem_axc.getScreeningSMMapResults(
			supermode_number,
			&results_dir,
			&date_time_stamp,
			&screening_sm_map_passed,
			&longitudinal_mode_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	CString results_dir_CString( results_dir );
	CString date_time_stamp_CString( date_time_stamp );

	CPPUNIT_ASSERT( results_dir_CString.GetLength() > 0 );
	CPPUNIT_ASSERT( date_time_stamp_CString.GetLength() > 0 );
	CPPUNIT_ASSERT( screening_sm_map_passed == TRUE );

	SysFreeString( results_dir );
	SysFreeString( date_time_stamp );


	// call getScreeningSMMapResults with p_results_dir as NULL
	WORD w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.getScreeningSMMapResults(
			supermode_number,
			NULL,
			&date_time_stamp,
			&screening_sm_map_passed,
			&longitudinal_mode_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );


	// call getScreeningSMMapResults with date_time_stamp as NULL
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.getScreeningSMMapResults(
			supermode_number,
			&results_dir,
			NULL,
			&screening_sm_map_passed,
			&longitudinal_mode_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );


	// call getScreeningSMMapResults with screening_sm_map_passed as NULL
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.getScreeningSMMapResults(
			supermode_number,
			&results_dir,
			&date_time_stamp,
			NULL,
			&longitudinal_mode_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );

	// call getScreeningSMMapResults with longitudinal_mode_count as NULL
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.getScreeningSMMapResults(
			supermode_number,
			&results_dir,
			&date_time_stamp,
			&screening_sm_map_passed,
			NULL );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );
}


///////////////////////////////////////////////
// test_getCharacterisationResults
//
// tests object.getCharacterisationResults()
//
void CT_CGSystemAXC::test_getCharacterisationResults()
{
	// call startup
	TRY
	{
		g_test_cgsystem_axc.startup();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	BSTR date_time_stamp;

	// call setup
	TRY
	{
		g_test_cgsystem_axc.setup("DSDBR01","test_laser_id",&date_time_stamp);
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	// call startLaserScreeningOverallMap normally
	TRY
	{
		g_test_cgsystem_axc.startLaserScreeningOverallMap();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	BSTR system_state;
	short overall_map_loaded;
	short supermodes_found_count;
	short supermode_map_loaded_count;
	short ITUOP_estimate_count;
	short ITUOP_count;

	// call getState
	TRY
	{
		g_test_cgsystem_axc.getState(
			&system_state,
			&overall_map_loaded,
			&supermodes_found_count,
			&supermode_map_loaded_count,
			&ITUOP_estimate_count,
			&ITUOP_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	CPPUNIT_ASSERT( CString( system_state ) == BUSY_STATE );

	// wait for screening to complete
	short percent_complete = 0;
	while( percent_complete < 100 )
	{
		Sleep(100);

		// call getPercentComplete
		TRY
		{
			g_test_cgsystem_axc.getPercentComplete( &percent_complete );
		}
		CATCH( COleDispatchException, pEx )
		{
			ASSERT_OLEDE_FAIL( pEx )
		}
		END_CATCH

		CPPUNIT_ASSERT( percent_complete >= 0 && percent_complete <= 100 );
	}

	CPPUNIT_ASSERT( percent_complete == 100 );



	// call getState
	TRY
	{
		g_test_cgsystem_axc.getState(
			&system_state,
			&overall_map_loaded,
			&supermodes_found_count,
			&supermode_map_loaded_count,
			&ITUOP_estimate_count,
			&ITUOP_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	CPPUNIT_ASSERT( CString( system_state ) == COMPLETE_STATE );

	BSTR results_dir;
	short ITUOP__count;

	// call getCharacterisationResults normally
	TRY
	{
		g_test_cgsystem_axc.getCharacterisationResults(
			&results_dir,
			&date_time_stamp,
			&ITUOP__count );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	CString results_dir_CString( results_dir );
	CString date_time_stamp_CString( date_time_stamp );

	CPPUNIT_ASSERT( results_dir_CString.GetLength() > 0 );
	CPPUNIT_ASSERT( date_time_stamp_CString.GetLength() > 0 );

	SysFreeString( results_dir );
	SysFreeString( date_time_stamp );



	// call getCharacterisationResults with p_results_dir as NULL
	WORD w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.getCharacterisationResults(
			NULL,
			&date_time_stamp,
			&ITUOP__count );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );


	// call getCharacterisationResults with date_time_stamp as NULL
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.getCharacterisationResults(
			&results_dir,
			NULL,
			&ITUOP__count );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );


	// call getCharacterisationResults with ITUOP__count as NULL
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.getCharacterisationResults(
			&results_dir,
			&date_time_stamp,
			NULL );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );
}



///////////////////////////////////////////////
// test_collatePassFailResults
//
// tests object.collatePassFailResults()
//
void CT_CGSystemAXC::test_collatePassFailResults()
{
	// call startup
	TRY
	{
		g_test_cgsystem_axc.startup();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	BSTR date_time_stamp;

	// call setup
	TRY
	{
		g_test_cgsystem_axc.setup("DSDBR01","test_laser_id",&date_time_stamp);
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	// call startLaserScreeningOverallMap normally
	TRY
	{
		g_test_cgsystem_axc.startLaserScreeningOverallMap();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH


	BSTR system_state;
	short overall_map_loaded;
	short supermodes_found_count;
	short supermode_map_loaded_count;
	short ITUOP_estimate_count;
	short ITUOP_count;

	// call getState
	TRY
	{
		g_test_cgsystem_axc.getState(
			&system_state,
			&overall_map_loaded,
			&supermodes_found_count,
			&supermode_map_loaded_count,
			&ITUOP_estimate_count,
			&ITUOP_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	CPPUNIT_ASSERT( CString( system_state ) == BUSY_STATE );

	// wait for screening to complete
	short percent_complete = 0;
	while( percent_complete < 100 )
	{
		Sleep(100);

		// call getPercentComplete
		TRY
		{
			g_test_cgsystem_axc.getPercentComplete( &percent_complete );
		}
		CATCH( COleDispatchException, pEx )
		{
			ASSERT_OLEDE_FAIL( pEx )
		}
		END_CATCH

		CPPUNIT_ASSERT( percent_complete >= 0 && percent_complete <= 100 );
	}

	CPPUNIT_ASSERT( percent_complete == 100 );


	// call getState
	TRY
	{
		g_test_cgsystem_axc.getState(
			&system_state,
			&overall_map_loaded,
			&supermodes_found_count,
			&supermode_map_loaded_count,
			&ITUOP_estimate_count,
			&ITUOP_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	CPPUNIT_ASSERT( CString( system_state ) == COMPLETE_STATE );

	BSTR results_dir;
	short screening_overall_map_passed;
	VARIANT screening_sm_maps_passed;
	short total_passed;

	// call collatePassFailResults normally
	TRY
	{
		g_test_cgsystem_axc.collatePassFailResults(
			&results_dir,
			&date_time_stamp,
			&screening_overall_map_passed,
			&screening_sm_maps_passed,
			&total_passed );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	CString results_dir_CString( results_dir );
	CString date_time_stamp_CString( date_time_stamp );

	CPPUNIT_ASSERT( results_dir_CString.GetLength() > 0 );
	CPPUNIT_ASSERT( date_time_stamp_CString.GetLength() > 0 );

	CPPUNIT_ASSERT( screening_sm_maps_passed.vt == (VT_ARRAY | VT_I2) );

	SysFreeString( results_dir );
	SysFreeString( date_time_stamp );
}


///////////////////////////////////////////////
// test_loadOverallMap
//
// tests object.loadOverallMap()
//
void CT_CGSystemAXC::test_loadOverallMap()
{
	// call startup
	TRY
	{
		g_test_cgsystem_axc.startup();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	BSTR date_time_stmp;

	// call setup
	TRY
	{
		g_test_cgsystem_axc.setup("DSDBR01","test_laser_id",&date_time_stmp);
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH


	char results_dir[] = "C:\\";
	char laser_type[] = "DSDBR01";
	char laser_id[] = "test_laser_id";
	char date_time_stamp[] = "20040720181756";
	char map_ramp_direction[] = "R";

	// call loadOverallMap (files don't exist)
	WORD w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.loadOverallMap(
			results_dir,
			laser_type,
			laser_id,
			date_time_stamp,
			map_ramp_direction );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_FILE_DOES_NOT_EXIST_ERROR ) );


	// call setup
	TRY
	{
		g_test_cgsystem_axc.setup("DSDBR01","test_laser_id",&date_time_stmp);
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	// call loadOverallMap with results_dir as NULL
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.loadOverallMap(
			NULL,
			laser_type,
			laser_id,
			date_time_stamp,
			map_ramp_direction );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );



	// call loadOverallMap with laser_type as NULL
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.loadOverallMap(
			results_dir,
			NULL,
			laser_id,
			date_time_stamp,
			map_ramp_direction );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );


	// call loadOverallMap with laser_id as NULL
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.loadOverallMap(
			results_dir,
			laser_type,
			NULL,
			date_time_stamp,
			map_ramp_direction );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );



	// call loadOverallMap with date_time_stamp as NULL
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.loadOverallMap(
			results_dir,
			laser_type,
			laser_id,
			NULL,
			map_ramp_direction );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );


	// call loadOverallMap with map_ramp_direction as NULL
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.loadOverallMap(
			results_dir,
			laser_type,
			laser_id,
			date_time_stamp,
			NULL );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );


	// call loadOverallMap with results_dir as empty ""
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.loadOverallMap(
			"",
			laser_type,
			laser_id,
			date_time_stamp,
			map_ramp_direction );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_EMPTY_STRING_ARG_ERROR ) );



	// call loadOverallMap with laser_type as empty ""
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.loadOverallMap(
			results_dir,
			"",
			laser_id,
			date_time_stamp,
			map_ramp_direction );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_EMPTY_STRING_ARG_ERROR ) );



	// call loadOverallMap with laser_id as empty ""
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.loadOverallMap(
			results_dir,
			laser_type,
			"",
			date_time_stamp,
			map_ramp_direction );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_EMPTY_STRING_ARG_ERROR ) );


	// call loadOverallMap with date_time_stamp as empty ""
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.loadOverallMap(
			results_dir,
			laser_type,
			laser_id,
			"",
			map_ramp_direction );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_EMPTY_STRING_ARG_ERROR ) );


	// call loadOverallMap with map_ramp_direction as empty ""
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.loadOverallMap(
			results_dir,
			laser_type,
			laser_id,
			date_time_stamp,
			"" );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_EMPTY_STRING_ARG_ERROR ) );


	// call loadOverallMap with map_ramp_direction as "F". (Files don't exist)
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.loadOverallMap(
			results_dir,
			laser_type,
			laser_id,
			date_time_stamp,
			"F" );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_FILE_DOES_NOT_EXIST_ERROR ) );


	// call setup
	TRY
	{
		g_test_cgsystem_axc.setup("DSDBR01","test_laser_id",&date_time_stmp);
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	// call loadOverallMap with map_ramp_direction as "P". Should be rejected.
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.loadOverallMap(
			results_dir,
			laser_type,
			laser_id,
			date_time_stamp,
			"P" );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_INVALID_MAP_RAMP_DIRECTION_ERROR ) );

	// call loadOverallMap with map_ramp_direction as "M". Should be rejected.
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.loadOverallMap(
			results_dir,
			laser_type,
			laser_id,
			date_time_stamp,
			"M" );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_INVALID_MAP_RAMP_DIRECTION_ERROR ) );
}





///////////////////////////////////////////////
// test_loadSMMap
//
// tests object.loadSMMap()
//
void CT_CGSystemAXC::test_loadSMMap()
{
	// call startup
	TRY
	{
		g_test_cgsystem_axc.startup();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	BSTR date_time_stmp;

	// call setup
	TRY
	{
		g_test_cgsystem_axc.setup("DSDBR01","test_laser_id",&date_time_stmp);
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	// call startLaserScreeningOverallMap normally
	TRY
	{
		g_test_cgsystem_axc.startLaserScreeningOverallMap();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH


	BSTR system_state;
	short overall_map_loaded;
	short supermodes_found_count;
	short supermode_map_loaded_count;
	short ITUOP_estimate_count;
	short ITUOP_count;

	// call getState
	TRY
	{
		g_test_cgsystem_axc.getState(
			&system_state,
			&overall_map_loaded,
			&supermodes_found_count,
			&supermode_map_loaded_count,
			&ITUOP_estimate_count,
			&ITUOP_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	CPPUNIT_ASSERT( CString( system_state ) == BUSY_STATE );

	// wait for screening to complete
	short percent_complete = 0;
	while( percent_complete < 100 )
	{
		Sleep(100);

		// call getPercentComplete
		TRY
		{
			g_test_cgsystem_axc.getPercentComplete( &percent_complete );
		}
		CATCH( COleDispatchException, pEx )
		{
			ASSERT_OLEDE_FAIL( pEx )
		}
		END_CATCH

		CPPUNIT_ASSERT( percent_complete >= 0 && percent_complete <= 100 );
	}

	CPPUNIT_ASSERT( percent_complete == 100 );




	// call getState
	TRY
	{
		g_test_cgsystem_axc.getState(
			&system_state,
			&overall_map_loaded,
			&supermodes_found_count,
			&supermode_map_loaded_count,
			&ITUOP_estimate_count,
			&ITUOP_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	CPPUNIT_ASSERT( CString( system_state ) == COMPLETE_STATE );

	char results_dir[] = "C:\\";
	char laser_type[] = "DSDBR01";
	char laser_id[] = "test_laser_id";
	char date_time_stamp[] = "20040720181756";
	short supermode_number = 1;
	char map_ramp_direction[] = "M";

	// call loadSMMap (files don't exist)
	WORD w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.loadSMMap(
			results_dir,
			laser_type,
			laser_id,
			date_time_stamp,
			supermode_number,
			map_ramp_direction );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_FILE_DOES_NOT_EXIST_ERROR ) );



	// call loadSMMap with results_dir as NULL
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.loadSMMap(
			NULL,
			laser_type,
			laser_id,
			date_time_stamp,
			supermode_number,
			map_ramp_direction );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );



	// call loadSMMap with laser_type as NULL
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.loadSMMap(
			results_dir,
			NULL,
			laser_id,
			date_time_stamp,
			supermode_number,
			map_ramp_direction );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );


	// call loadSMMap with laser_id as NULL
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.loadSMMap(
			results_dir,
			laser_type,
			NULL,
			date_time_stamp,
			supermode_number,
			map_ramp_direction );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );



	// call loadSMMap with date_time_stamp as NULL
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.loadSMMap(
			results_dir,
			laser_type,
			laser_id,
			NULL,
			supermode_number,
			map_ramp_direction );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );


	// call loadSMMap with map_ramp_direction as NULL
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.loadSMMap(
			results_dir,
			laser_type,
			laser_id,
			date_time_stamp,
			supermode_number,
			NULL );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );


	// call loadSMMap with results_dir as empty ""
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.loadSMMap(
			"",
			laser_type,
			laser_id,
			date_time_stamp,
			supermode_number,
			map_ramp_direction );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_EMPTY_STRING_ARG_ERROR ) );



	// call loadSMMap with laser_type as empty ""
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.loadSMMap(
			results_dir,
			"",
			laser_id,
			date_time_stamp,
			supermode_number,
			map_ramp_direction );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_EMPTY_STRING_ARG_ERROR ) );



	// call loadSMMap with laser_id as empty ""
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.loadSMMap(
			results_dir,
			laser_type,
			"",
			date_time_stamp,
			supermode_number,
			map_ramp_direction );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_EMPTY_STRING_ARG_ERROR ) );


	// call loadSMMap with date_time_stamp as empty ""
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.loadSMMap(
			results_dir,
			laser_type,
			laser_id,
			"",
			supermode_number,
			map_ramp_direction );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_EMPTY_STRING_ARG_ERROR ) );


	// call loadSMMap with map_ramp_direction as empty ""
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.loadSMMap(
			results_dir,
			laser_type,
			laser_id,
			date_time_stamp,
			supermode_number,
			"" );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_EMPTY_STRING_ARG_ERROR ) );


	// call loadSMMap with map_ramp_direction as "P". Files don't exist.
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.loadSMMap(
			results_dir,
			laser_type,
			laser_id,
			date_time_stamp,
			supermode_number,
			"P" );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_FILE_DOES_NOT_EXIST_ERROR ) );


	// call loadSMMap with map_ramp_direction as "F". Should be rejected.
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.loadSMMap(
			results_dir,
			laser_type,
			laser_id,
			date_time_stamp,
			supermode_number,
			"F" );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_INVALID_MAP_RAMP_DIRECTION_ERROR ) );

	// call loadSMMap with map_ramp_direction as "R". Should be rejected.
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.loadSMMap(
			results_dir,
			laser_type,
			laser_id,
			date_time_stamp,
			supermode_number,
			"R" );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_INVALID_MAP_RAMP_DIRECTION_ERROR ) );
}



///////////////////////////////////////////////
// test_readVoltage
//
// tests object.readVoltage()
//
void CT_CGSystemAXC::test_readVoltage()
{
	// call startup
	TRY
	{
		g_test_cgsystem_axc.startup();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	BSTR date_time_stamp;

	// call setup
	TRY
	{
		g_test_cgsystem_axc.setup("DSDBR01","test_laser_id",&date_time_stamp);
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH



	double voltage;

	// call readVoltage with module_name as a null terminated char array

	TRY
	{
		g_test_cgsystem_axc.readVoltage( "test", &voltage );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH


	// call readVoltage with module_name as a BSTR
	CString module_name_CString("test");
	BSTR module_name_BSTR = module_name_CString.AllocSysString();

	TRY
	{
		g_test_cgsystem_axc.readVoltage( COLE2T(module_name_BSTR), &voltage );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH



	// call readVoltage with invalid module name
	WORD w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.readVoltage( "te st", &voltage );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_INVALID_MODULE_NAME_ERROR ) );


	// call readVoltage with NULL module name
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.readVoltage( NULL, &voltage );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );


	// call readVoltage with NULL p_voltage
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.readVoltage( "test", NULL );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );
}

///////////////////////////////////////////////
// test_readFrequency
//
// tests object.readFrequency()
//
void CT_CGSystemAXC::test_readFrequency()
{
	// call startup
	TRY
	{
		g_test_cgsystem_axc.startup();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	BSTR date_time_stamp;

	// call setup
	TRY
	{
		g_test_cgsystem_axc.setup("DSDBR01","test_laser_id",&date_time_stamp);
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH



	double frequency;

	// call readFrequency

	TRY
	{
		g_test_cgsystem_axc.readFrequency( &frequency );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	// call readFrequency with NULL p_frequency
	WORD w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.readFrequency( NULL );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );
}

///////////////////////////////////////////////
// test_alignPowerRatioToFrequency
//
// tests object.alignPowerRatioToFrequency()
//
void CT_CGSystemAXC::test_alignPowerRatioToFrequency()
{
	// call startup
	TRY
	{
		g_test_cgsystem_axc.startup();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	BSTR date_time_stamp;

	// call setup
	TRY
	{
		g_test_cgsystem_axc.setup("DSDBR01","test_laser_id",&date_time_stamp);
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH


	double power_ratio = 0;
	double frequency = 0;

	// call alignPowerRatioToFrequency

	TRY
	{
		g_test_cgsystem_axc.alignPowerRatioToFrequency( power_ratio, frequency );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH
}

///////////////////////////////////////////////
// test_readCoarseFrequency
//
// tests object.readCoarseFrequency()
//
void CT_CGSystemAXC::test_readCoarseFrequency()
{
	// call startup
	TRY
	{
		g_test_cgsystem_axc.startup();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	BSTR date_time_stamp;

	// call setup
	TRY
	{
		g_test_cgsystem_axc.setup("DSDBR01","test_laser_id",&date_time_stamp);
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH



	double coarse_frequency;

	// call readCoarseFrequency

	TRY
	{
		g_test_cgsystem_axc.readCoarseFrequency( &coarse_frequency );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	// call readCoarseFrequency with NULL p_coarse_frequency
	WORD w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.readCoarseFrequency( NULL );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );
}

///////////////////////////////////////////////
// test_readPhotodiodeCurrent
//
// tests object.readPhotodiodeCurrent()
//
void CT_CGSystemAXC::test_readPhotodiodeCurrent()
{
	// call startup
	TRY
	{
		g_test_cgsystem_axc.startup();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	BSTR date_time_stamp;

	// call setup
	TRY
	{
		g_test_cgsystem_axc.setup("DSDBR01","test_laser_id",&date_time_stamp);
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH



	short channel = 1;
	double current;

	// call readPhotodiodeCurrent with module_name as a null terminated char array

	TRY
	{
		g_test_cgsystem_axc.readPhotodiodeCurrent(
			"test",
			channel,
			&current );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH


	// call readPhotodiodeCurrent with module_name as a BSTR
	CString module_name_CString("test");
	BSTR module_name_BSTR = module_name_CString.AllocSysString();

	TRY
	{
		g_test_cgsystem_axc.readPhotodiodeCurrent(
			COLE2T(module_name_BSTR),
			channel,
			&current );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH



	// call readPhotodiodeCurrent with invalid module name
	WORD w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.readPhotodiodeCurrent(
			"te st",
			channel,
			&current );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_INVALID_MODULE_NAME_ERROR ) );


	// call readPhotodiodeCurrent with NULL module name
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.readPhotodiodeCurrent(
			NULL,
			channel,
			&current );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );


	// call readPhotodiodeCurrent with NULL p_current
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.readPhotodiodeCurrent(
			"test",
			channel,
			NULL );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );


	// call readPhotodiodeCurrent with invalid channel = 0
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.readPhotodiodeCurrent(
			"test",
			0,
			&current );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_INVALID_CHANNEL_ERROR ) );

	// call readPhotodiodeCurrent with invalid channel = 3
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.readPhotodiodeCurrent(
			"test",
			3,
			&current );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_INVALID_CHANNEL_ERROR ) );


	// call readPhotodiodeCurrent with valid channel = 2

	TRY
	{
		g_test_cgsystem_axc.readPhotodiodeCurrent(
			"test",
			2,
			&current );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

}

///////////////////////////////////////////////
// test_readOpticalPower
//
// tests object.readOpticalPower()
//
void CT_CGSystemAXC::test_readOpticalPower()
{
	// call startup
	TRY
	{
		g_test_cgsystem_axc.startup();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	BSTR date_time_stamp;

	// call setup
	TRY
	{
		g_test_cgsystem_axc.setup("DSDBR01","test_laser_id",&date_time_stamp);
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH



	short channel = 1;
	double optical_power;

	// call readOpticalPower with module_name as a null terminated char array

	TRY
	{
		g_test_cgsystem_axc.readOpticalPower(
			"test",
			channel,
			&optical_power );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH


	// call readOpticalPower with module_name as a BSTR
	CString module_name_CString("test");
	BSTR module_name_BSTR = module_name_CString.AllocSysString();

	TRY
	{
		g_test_cgsystem_axc.readOpticalPower(
			COLE2T(module_name_BSTR),
			channel,
			&optical_power );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH



	// call readOpticalPower with invalid module name
	WORD w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.readOpticalPower(
			"te st",
			channel,
			&optical_power );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_INVALID_MODULE_NAME_ERROR ) );


	// call readOpticalPower with NULL module name
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.readOpticalPower(
			NULL,
			channel,
			&optical_power );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );


	// call readOpticalPower with NULL p_optical_power
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.readOpticalPower(
			"test",
			channel,
			NULL );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );


	// call readOpticalPower with invalid channel = 0
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.readOpticalPower(
			"test",
			0,
			&optical_power );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_INVALID_CHANNEL_ERROR ) );

	// call readOpticalPower with invalid channel = 3
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.readOpticalPower(
			"test",
			3,
			&optical_power );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_INVALID_CHANNEL_ERROR ) );



	// call readOpticalPower with valid channel = 2

	TRY
	{
		g_test_cgsystem_axc.readOpticalPower(
			"test",
			2,
			&optical_power );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH
}

///////////////////////////////////////////////
// test_readOpticalPowerAtFreq
//
// tests object.readOpticalPowerAtFreq()
//
void CT_CGSystemAXC::test_readOpticalPowerAtFreq()
{
	// call startup
	TRY
	{
		g_test_cgsystem_axc.startup();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	BSTR date_time_stamp;

	// call setup
	TRY
	{
		g_test_cgsystem_axc.setup("DSDBR01","test_laser_id",&date_time_stamp);
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH



	short channel = 1;
	double optical_power;
	double frequency = 1543;

	// call readOpticalPowerAtFreq with module_name as a null terminated char array

	TRY
	{
		g_test_cgsystem_axc.readOpticalPowerAtFreq(
			"test",
			channel,
			frequency,
			&optical_power );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH


	// call readOpticalPowerAtFreq with module_name as a BSTR
	CString module_name_CString("test");
	BSTR module_name_BSTR = module_name_CString.AllocSysString();

	TRY
	{
		g_test_cgsystem_axc.readOpticalPowerAtFreq(
			COLE2T(module_name_BSTR),
			channel,
			frequency,
			&optical_power );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH



	// call readOpticalPowerAtFreq with invalid module name
	WORD w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.readOpticalPowerAtFreq(
			"te st",
			channel,
			frequency,
			&optical_power );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_INVALID_MODULE_NAME_ERROR ) );


	// call readOpticalPowerAtFreq with NULL module name
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.readOpticalPowerAtFreq(
			NULL,
			channel,
			frequency,
			&optical_power );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );


	// call readOpticalPowerAtFreq with NULL p_optical_power
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.readOpticalPowerAtFreq(
			"test",
			channel,
			frequency,
			NULL );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );


	// call readOpticalPowerAtFreq with invalid channel = 0
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.readOpticalPowerAtFreq(
			"test",
			0,
			frequency,
			&optical_power );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_INVALID_CHANNEL_ERROR ) );

	// call readOpticalPowerAtFreq with invalid channel = 3
	w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.readOpticalPowerAtFreq(
			"test",
			3,
			frequency,
			&optical_power );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_INVALID_CHANNEL_ERROR ) );



	// call readOpticalPowerAtFreq with valid channel = 2

	TRY
	{
		g_test_cgsystem_axc.readOpticalPowerAtFreq(
			"test",
			2,
			frequency,
			&optical_power );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH
}

///////////////////////////////////////////////
// test_readTemperature
//
// tests object.readTemperature()
//
void CT_CGSystemAXC::test_readTemperature()
{
	// call startup
	TRY
	{
		g_test_cgsystem_axc.startup();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	BSTR date_time_stamp;

	// call setup
	TRY
	{
		g_test_cgsystem_axc.setup("DSDBR01","test_laser_id",&date_time_stamp);
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH



	double temperature;

	// call readTemperature
	TRY
	{
		g_test_cgsystem_axc.readTemperature( &temperature );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	// call readTemperature with NULL p_temperature
	WORD w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.readTemperature( NULL );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );
}

///////////////////////////////////////////////
// test_getTECTempSetpoint
//
// tests object.getTECTempSetpoint()
//
void CT_CGSystemAXC::test_getTECTempSetpoint()
{
	// call startup
	TRY
	{
		g_test_cgsystem_axc.startup();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH




	double temp_setpoint;

	// call getTECTempSetpoint
	TRY
	{
		g_test_cgsystem_axc.getTECTempSetpoint( &temp_setpoint );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	// call getTECTempSetpoint with NULL p_temp_setpoint
	WORD w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.getTECTempSetpoint( NULL );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );
}

///////////////////////////////////////////////
// test_setTECTempSetpoint
//
// tests object.setTECTempSetpoint()
//
void CT_CGSystemAXC::test_setTECTempSetpoint()
{
	// call startup
	TRY
	{
		g_test_cgsystem_axc.startup();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH


	double temp_setpoint = 25;

	// call setTECTempSetpoint
	TRY
	{
		g_test_cgsystem_axc.setTECTempSetpoint( temp_setpoint );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH


}

/////////////////////////////////////////////////
//// test_getTECPIDvalues
////
//// tests object.getTECPIDvalues()
////
//void CT_CGSystemAXC::test_getTECPIDvalues()
//{
//	// call startup
//	TRY
//	{
//		g_test_cgsystem_axc.startup();
//	}
//	CATCH( COleDispatchException, pEx )
//	{
//		ASSERT_OLEDE_FAIL( pEx )
//	}
//	END_CATCH
//
//	double P_value;
//	double I_value;
//	double D_value;
//
//	// call getTECPIDvalues
//	TRY
//	{
//		g_test_cgsystem_axc.getTECPIDvalues( &P_value, &I_value, &D_value );
//	}
//	CATCH( COleDispatchException, pEx )
//	{
//		ASSERT_OLEDE_FAIL( pEx )
//	}
//	END_CATCH
//
//
//	// call getTECPIDvalues with NULL p_P_value
//	WORD w_code = 0;
//	TRY
//	{
//		g_test_cgsystem_axc.getTECPIDvalues( NULL, &I_value, &D_value );
//	}
//	CATCH( COleDispatchException, pEx )
//	{
//		w_code = pEx->m_wCode;
//	}
//	END_CATCH
//	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );
//
//
//	// call getTECPIDvalues with NULL p_I_value
//	w_code = 0;
//	TRY
//	{
//		g_test_cgsystem_axc.getTECPIDvalues( &P_value, NULL, &D_value );
//	}
//	CATCH( COleDispatchException, pEx )
//	{
//		w_code = pEx->m_wCode;
//	}
//	END_CATCH
//	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );
//
//
//	// call getTECPIDvalues with NULL p_D_value
//	w_code = 0;
//	TRY
//	{
//		g_test_cgsystem_axc.getTECPIDvalues( &P_value, &I_value, NULL );
//	}
//	CATCH( COleDispatchException, pEx )
//	{
//		w_code = pEx->m_wCode;
//	}
//	END_CATCH
//	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_NULL_PTR_ERROR ) );
//}
//
/////////////////////////////////////////////////
//// test_setTECPIDvalues
////
//// tests object.setTECPIDvalues()
////
//void CT_CGSystemAXC::test_setTECPIDvalues()
//{
//	// call startup
//	TRY
//	{
//		g_test_cgsystem_axc.startup();
//	}
//	CATCH( COleDispatchException, pEx )
//	{
//		ASSERT_OLEDE_FAIL( pEx )
//	}
//	END_CATCH
//
//
//	double P_value = 1;
//	double I_value = 2;
//	double D_value = 3;
//
//	// call setTECPIDvalues
//	TRY
//	{
//		g_test_cgsystem_axc.setTECPIDvalues( P_value, I_value, D_value );
//	}
//	CATCH( COleDispatchException, pEx )
//	{
//		ASSERT_OLEDE_FAIL( pEx )
//	}
//	END_CATCH
//}



///////////////////////////////////////////////
// test_states_st_sh
//
// tests state m/c for
// object.startup()
// object.shutdown()
//
void CT_CGSystemAXC::test_states_st_sh()
{
	BSTR system_state;
	short overall_map_loaded;
	short supermodes_found_count;
	short supermode_map_loaded_count;
	short ITUOP_estimate_count;
	short ITUOP_count;

	// call getState before startup
	TRY
	{
		g_test_cgsystem_axc.getState(
			&system_state,
			&overall_map_loaded,
			&supermodes_found_count,
			&supermode_map_loaded_count,
			&ITUOP_estimate_count,
			&ITUOP_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	CString system_state_CString( system_state );

	CPPUNIT_ASSERT( system_state_CString == INSTANTIATED_STATE );


//#define INSTANTIATED_STATE CString("Instantiated")
//#define STARTUP_STATE CString("Startup")
//#define SETUP_STATE CString("Setup")
//#define BUSY_STATE CString("Busy")
//#define COMPLETE_STATE CString("Complete")



	// call startup 
	TRY
	{
		g_test_cgsystem_axc.startup();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH


	// call getState after startup
	TRY
	{
		g_test_cgsystem_axc.getState(
			&system_state,
			&overall_map_loaded,
			&supermodes_found_count,
			&supermode_map_loaded_count,
			&ITUOP_estimate_count,
			&ITUOP_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	system_state_CString = CString( system_state );

	CPPUNIT_ASSERT( system_state_CString == STARTUP_STATE );




	// call shutdown 
	TRY
	{
		g_test_cgsystem_axc.shutdown();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH



	// call getState after shutdown
	TRY
	{
		g_test_cgsystem_axc.getState(
			&system_state,
			&overall_map_loaded,
			&supermodes_found_count,
			&supermode_map_loaded_count,
			&ITUOP_estimate_count,
			&ITUOP_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	system_state_CString = CString( system_state );

	CPPUNIT_ASSERT( system_state_CString == INSTANTIATED_STATE );


	// let's go around again


	// call startup 
	TRY
	{
		g_test_cgsystem_axc.startup();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH


	// call getState after startup
	TRY
	{
		g_test_cgsystem_axc.getState(
			&system_state,
			&overall_map_loaded,
			&supermodes_found_count,
			&supermode_map_loaded_count,
			&ITUOP_estimate_count,
			&ITUOP_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	system_state_CString = CString( system_state );

	CPPUNIT_ASSERT( system_state_CString == STARTUP_STATE );




	// call shutdown 
	TRY
	{
		g_test_cgsystem_axc.shutdown();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH



	// call getState after shutdown
	TRY
	{
		g_test_cgsystem_axc.getState(
			&system_state,
			&overall_map_loaded,
			&supermodes_found_count,
			&supermode_map_loaded_count,
			&ITUOP_estimate_count,
			&ITUOP_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	system_state_CString = CString( system_state );

	CPPUNIT_ASSERT( system_state_CString == INSTANTIATED_STATE );

}


///////////////////////////////////////////////
// test_states_st_se_sh
//
// tests state m/c for
// object.startup()
// object.setup()
// object.shutdown()
//
void CT_CGSystemAXC::test_states_st_se_sh()
{
	BSTR system_state;
	short overall_map_loaded;
	short supermodes_found_count;
	short supermode_map_loaded_count;
	short ITUOP_estimate_count;
	short ITUOP_count;

	// call getState before startup
	TRY
	{
		g_test_cgsystem_axc.getState(
			&system_state,
			&overall_map_loaded,
			&supermodes_found_count,
			&supermode_map_loaded_count,
			&ITUOP_estimate_count,
			&ITUOP_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	CString system_state_CString( system_state );

	CPPUNIT_ASSERT( system_state_CString == INSTANTIATED_STATE );



	// call startup 
	TRY
	{
		g_test_cgsystem_axc.startup();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH


	// call getState after startup
	TRY
	{
		g_test_cgsystem_axc.getState(
			&system_state,
			&overall_map_loaded,
			&supermodes_found_count,
			&supermode_map_loaded_count,
			&ITUOP_estimate_count,
			&ITUOP_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	system_state_CString = CString( system_state );

	CPPUNIT_ASSERT( system_state_CString == STARTUP_STATE );



	char laser_type[] = "DSDBR01";
	char laser_id[] = "test_laser_id";
	BSTR date_time_stamp;

	// call setup 
	TRY
	{
		g_test_cgsystem_axc.setup(
			laser_type,
			laser_id,
			&date_time_stamp );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH


	// call getState after setup
	TRY
	{
		g_test_cgsystem_axc.getState(
			&system_state,
			&overall_map_loaded,
			&supermodes_found_count,
			&supermode_map_loaded_count,
			&ITUOP_estimate_count,
			&ITUOP_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	system_state_CString = CString( system_state );

	CPPUNIT_ASSERT( system_state_CString == SETUP_STATE );




	// call setup with invalid arg
	WORD w_code = 0;
	TRY
	{
		g_test_cgsystem_axc.setup(
			"INVALIDLASERTYPE",
			laser_id,
			&date_time_stamp );
	}
	CATCH( COleDispatchException, pEx )
	{
		w_code = pEx->m_wCode;
	}
	END_CATCH
	CPPUNIT_ASSERT( w_code == CGSYSTEM_ERROR_CODE( CGSYSTEM_LASERTYPE_ERROR ) );




	// call getState after setup with invalid arg
	TRY
	{
		g_test_cgsystem_axc.getState(
			&system_state,
			&overall_map_loaded,
			&supermodes_found_count,
			&supermode_map_loaded_count,
			&ITUOP_estimate_count,
			&ITUOP_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	system_state_CString = CString( system_state );

	CPPUNIT_ASSERT( system_state_CString == STARTUP_STATE );


	// call setup again
	TRY
	{
		g_test_cgsystem_axc.setup(
			laser_type,
			laser_id,
			&date_time_stamp );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH


	// call getState after setup
	TRY
	{
		g_test_cgsystem_axc.getState(
			&system_state,
			&overall_map_loaded,
			&supermodes_found_count,
			&supermode_map_loaded_count,
			&ITUOP_estimate_count,
			&ITUOP_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	system_state_CString = CString( system_state );

	CPPUNIT_ASSERT( system_state_CString == SETUP_STATE );



	// call shutdown 
	TRY
	{
		g_test_cgsystem_axc.shutdown();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH



	// call getState after shutdown
	TRY
	{
		g_test_cgsystem_axc.getState(
			&system_state,
			&overall_map_loaded,
			&supermodes_found_count,
			&supermode_map_loaded_count,
			&ITUOP_estimate_count,
			&ITUOP_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	system_state_CString = CString( system_state );

	CPPUNIT_ASSERT( system_state_CString == INSTANTIATED_STATE );

}



///////////////////////////////////////////////
// test_states_runthru
//
// tests state m/c for
// object.startup()
// object.setup()
// object.startLaserScreeningOverallMap()
// object.shutdown()
//
void CT_CGSystemAXC::test_states_runthru()
{
	BSTR system_state;
	short overall_map_loaded;
	short supermodes_found_count;
	short supermode_map_loaded_count;
	short ITUOP_estimate_count;
	short ITUOP_count;

	// call getState before startup
	TRY
	{
		g_test_cgsystem_axc.getState(
			&system_state,
			&overall_map_loaded,
			&supermodes_found_count,
			&supermode_map_loaded_count,
			&ITUOP_estimate_count,
			&ITUOP_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	CString system_state_CString( system_state );

	CPPUNIT_ASSERT( system_state_CString == INSTANTIATED_STATE );

	//printf("\n system state = %s\n", system_state_CString.GetBuffer() );


	// call startup 
	TRY
	{
		g_test_cgsystem_axc.startup();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH


	// call getState after startup
	TRY
	{
		g_test_cgsystem_axc.getState(
			&system_state,
			&overall_map_loaded,
			&supermodes_found_count,
			&supermode_map_loaded_count,
			&ITUOP_estimate_count,
			&ITUOP_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	system_state_CString = CString( system_state );

	CPPUNIT_ASSERT( system_state_CString == STARTUP_STATE );

	//printf("\n system state = %s\n", system_state_CString.GetBuffer() );


	char laser_type[] = "DSDBR01";
	char laser_id[] = "test_laser_id";
	BSTR date_time_stamp;

	// call setup 
	TRY
	{
		g_test_cgsystem_axc.setup(
			laser_type,
			laser_id,
			&date_time_stamp );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH


	// call getState after setup
	TRY
	{
		g_test_cgsystem_axc.getState(
			&system_state,
			&overall_map_loaded,
			&supermodes_found_count,
			&supermode_map_loaded_count,
			&ITUOP_estimate_count,
			&ITUOP_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	system_state_CString = CString( system_state );

	CPPUNIT_ASSERT( system_state_CString == SETUP_STATE );

	//printf("\n system state = %s\n", system_state_CString.GetBuffer() );



	// call startLaserScreeningOverallMap
	TRY
	{
		g_test_cgsystem_axc.startLaserScreeningOverallMap();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH



	// call getState
	TRY
	{
		g_test_cgsystem_axc.getState(
			&system_state,
			&overall_map_loaded,
			&supermodes_found_count,
			&supermode_map_loaded_count,
			&ITUOP_estimate_count,
			&ITUOP_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	CPPUNIT_ASSERT( CString( system_state ) == BUSY_STATE );

	//printf("\n system state = %s\n", system_state_CString.GetBuffer() );

	// wait for screening to complete
	short percent_complete = 0;
	while( percent_complete < 100 )
	{
		Sleep(100);

		// call getPercentComplete
		TRY
		{
			g_test_cgsystem_axc.getPercentComplete( &percent_complete );
		}
		CATCH( COleDispatchException, pEx )
		{
			ASSERT_OLEDE_FAIL( pEx )
		}
		END_CATCH

		CPPUNIT_ASSERT( percent_complete >= 0 && percent_complete <= 100 );

		//printf("\n system percent_complete = %d\n", percent_complete );
	}

	CPPUNIT_ASSERT( percent_complete == 100 );

	// call getState
	TRY
	{
		g_test_cgsystem_axc.getState(
			&system_state,
			&overall_map_loaded,
			&supermodes_found_count,
			&supermode_map_loaded_count,
			&ITUOP_estimate_count,
			&ITUOP_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	system_state_CString = CString( system_state );
	CPPUNIT_ASSERT(system_state_CString == COMPLETE_STATE );

	//printf("\n system state = %s", system_state_CString.GetBuffer() );
	//printf("\n overall_map_loaded = %d", overall_map_loaded );
	//printf("\n supermodes_found_count = %d", supermodes_found_count );
	//printf("\n supermode_map_loaded_count = %d", supermode_map_loaded_count );
	//printf("\n ITUOP_estimate_count = %d", ITUOP_estimate_count );
	//printf("\n ITUOP_count = %d\n", ITUOP_count );



	// iterate through each supermode found

	for( short sm = 1; sm <= supermodes_found_count ; sm++ )
	{

		// call startLaserScreeningSMMap
		TRY
		{
			g_test_cgsystem_axc.startLaserScreeningSMMap(sm);
		}
		CATCH( COleDispatchException, pEx )
		{
			ASSERT_OLEDE_FAIL( pEx )
		}
		END_CATCH



		// call getState
		TRY
		{
			g_test_cgsystem_axc.getState(
				&system_state,
				&overall_map_loaded,
				&supermodes_found_count,
				&supermode_map_loaded_count,
				&ITUOP_estimate_count,
				&ITUOP_count );
		}
		CATCH( COleDispatchException, pEx )
		{
			ASSERT_OLEDE_FAIL( pEx )
		}
		END_CATCH



		system_state_CString = CString( system_state );
		CPPUNIT_ASSERT(system_state_CString == BUSY_STATE );

		//printf("\n sm = %d, system state = %s", sm, system_state_CString.GetBuffer() );
		//printf("\n   overall_map_loaded = %d", overall_map_loaded );
		//printf("\n   supermodes_found_count = %d", supermodes_found_count );
		//printf("\n   supermode_map_loaded_count = %d", supermode_map_loaded_count );
		//printf("\n   ITUOP_estimate_count = %d", ITUOP_estimate_count );
		//printf("\n   ITUOP_count = %d\n", ITUOP_count );

		// wait for screening to complete
		percent_complete = 0;
		while( percent_complete < 100 )
		{
			Sleep(100);

			// call getPercentComplete
			TRY
			{
				g_test_cgsystem_axc.getPercentComplete( &percent_complete );
			}
			CATCH( COleDispatchException, pEx )
			{
				ASSERT_OLEDE_FAIL( pEx )
			}
			END_CATCH

			CPPUNIT_ASSERT( percent_complete >= 0 && percent_complete <= 100 );

			//printf("\n system percent_complete = %d\n", percent_complete );
		}

		CPPUNIT_ASSERT( percent_complete == 100 );

		// call getState
		TRY
		{
			g_test_cgsystem_axc.getState(
				&system_state,
				&overall_map_loaded,
				&supermodes_found_count,
				&supermode_map_loaded_count,
				&ITUOP_estimate_count,
				&ITUOP_count );
		}
		CATCH( COleDispatchException, pEx )
		{
			ASSERT_OLEDE_FAIL( pEx )
		}
		END_CATCH

		system_state_CString = CString( system_state );
		CPPUNIT_ASSERT(system_state_CString == COMPLETE_STATE );

		//printf("\n sm = %d, system state = %s", sm, system_state_CString.GetBuffer() );
		//printf("\n   overall_map_loaded = %d", overall_map_loaded );
		//printf("\n   supermodes_found_count = %d", supermodes_found_count );
		//printf("\n   supermode_map_loaded_count = %d", supermode_map_loaded_count );
		//printf("\n   ITUOP_estimate_count = %d", ITUOP_estimate_count );
		//printf("\n   ITUOP_count = %d\n", ITUOP_count );

	}




	BSTR results_dir;
	short ITUOP_estimates_count;

	// call estimateITUOPs normally
	TRY
	{
		g_test_cgsystem_axc.estimateITUOPs(
			&results_dir,
			&date_time_stamp,
			&ITUOP_estimates_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	SysFreeString( results_dir );
	SysFreeString( date_time_stamp );



	// call getState after estimateITUOPs
	TRY
	{
		g_test_cgsystem_axc.getState(
			&system_state,
			&overall_map_loaded,
			&supermodes_found_count,
			&supermode_map_loaded_count,
			&ITUOP_estimate_count,
			&ITUOP_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	system_state_CString = CString( system_state );
	CPPUNIT_ASSERT(system_state_CString == COMPLETE_STATE );

	//printf("\n system state = %s", system_state_CString.GetBuffer() );
	//printf("\n overall_map_loaded = %d", overall_map_loaded );
	//printf("\n supermodes_found_count = %d", supermodes_found_count );
	//printf("\n supermode_map_loaded_count = %d", supermode_map_loaded_count );
	//printf("\n ITUOP_estimate_count = %d", ITUOP_estimate_count );
	//printf("\n ITUOP_count = %d\n", ITUOP_count );



	// call startLaserCharacterisation
	TRY
	{
		g_test_cgsystem_axc.startLaserCharacterisation();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH



	// call getState after startLaserCharacterisation
	TRY
	{
		g_test_cgsystem_axc.getState(
			&system_state,
			&overall_map_loaded,
			&supermodes_found_count,
			&supermode_map_loaded_count,
			&ITUOP_estimate_count,
			&ITUOP_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	system_state_CString = CString( system_state );
	CPPUNIT_ASSERT(system_state_CString == BUSY_STATE );



	//printf("\n system state = %s\n", system_state_CString.GetBuffer() );

	// wait for screening to complete
	percent_complete = 0;
	while( percent_complete < 100 )
	{
		Sleep(100);

		// call getPercentComplete
		TRY
		{
			g_test_cgsystem_axc.getPercentComplete( &percent_complete );
		}
		CATCH( COleDispatchException, pEx )
		{
			ASSERT_OLEDE_FAIL( pEx )
		}
		END_CATCH

		CPPUNIT_ASSERT( percent_complete >= 0 && percent_complete <= 100 );

		//printf("\n system percent_complete = %d\n", percent_complete );
	}

	CPPUNIT_ASSERT( percent_complete == 100 );

	// call getState
	TRY
	{
		g_test_cgsystem_axc.getState(
			&system_state,
			&overall_map_loaded,
			&supermodes_found_count,
			&supermode_map_loaded_count,
			&ITUOP_estimate_count,
			&ITUOP_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	system_state_CString = CString( system_state );
	CPPUNIT_ASSERT(system_state_CString == COMPLETE_STATE );

	//printf("\n system state = %s", system_state_CString.GetBuffer() );
	//printf("\n overall_map_loaded = %d", overall_map_loaded );
	//printf("\n supermodes_found_count = %d", supermodes_found_count );
	//printf("\n supermode_map_loaded_count = %d", supermode_map_loaded_count );
	//printf("\n ITUOP_estimate_count = %d", ITUOP_estimate_count );
	//printf("\n ITUOP_count = %d\n", ITUOP_count );



	// call shutdown 
	TRY
	{
		g_test_cgsystem_axc.shutdown();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH



	// call getState after shutdown
	TRY
	{
		g_test_cgsystem_axc.getState(
			&system_state,
			&overall_map_loaded,
			&supermodes_found_count,
			&supermode_map_loaded_count,
			&ITUOP_estimate_count,
			&ITUOP_count );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	system_state_CString = CString( system_state );

	CPPUNIT_ASSERT( system_state_CString == INSTANTIATED_STATE );

	//printf("\n system state = %s", system_state_CString.GetBuffer() );
	//printf("\n overall_map_loaded = %d", overall_map_loaded );
	//printf("\n supermodes_found_count = %d", supermodes_found_count );
	//printf("\n supermode_map_loaded_count = %d", supermode_map_loaded_count );
	//printf("\n ITUOP_estimate_count = %d", ITUOP_estimate_count );
	//printf("\n ITUOP_count = %d\n", ITUOP_count );

}

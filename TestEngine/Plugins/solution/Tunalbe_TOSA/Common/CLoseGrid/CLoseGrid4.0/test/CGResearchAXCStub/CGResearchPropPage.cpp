// CGResearchPropPage.cpp : Implementation of the CCGResearchPropPage property page class.

#include "stdafx.h"
#include "CGResearch.h"
#include "CGResearchPropPage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


IMPLEMENT_DYNCREATE(CCGResearchPropPage, COlePropertyPage)



// Message map

BEGIN_MESSAGE_MAP(CCGResearchPropPage, COlePropertyPage)
END_MESSAGE_MAP()



// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CCGResearchPropPage, "CGRESEARCH.CGResearchPropPage.1",
	0x7d14d008, 0x68b4, 0x4f99, 0x9b, 0x4a, 0x65, 0x43, 0x13, 0x5d, 0xe4, 0xbd)



// CCGResearchPropPage::CCGResearchPropPageFactory::UpdateRegistry -
// Adds or removes system registry entries for CCGResearchPropPage

BOOL CCGResearchPropPage::CCGResearchPropPageFactory::UpdateRegistry(BOOL bRegister)
{
	if (bRegister)
		return AfxOleRegisterPropertyPageClass(AfxGetInstanceHandle(),
			m_clsid, IDS_CGRESEARCH_PPG);
	else
		return AfxOleUnregisterClass(m_clsid, NULL);
}



// CCGResearchPropPage::CCGResearchPropPage - Constructor

CCGResearchPropPage::CCGResearchPropPage() :
	COlePropertyPage(IDD, IDS_CGRESEARCH_PPG_CAPTION)
{
}



// CCGResearchPropPage::DoDataExchange - Moves data between page and properties

void CCGResearchPropPage::DoDataExchange(CDataExchange* pDX)
{
	DDP_PostProcessing(pDX);
}



// CCGResearchPropPage message handlers

// CGResearch.cpp : Implementation of CCGResearchApp and DLL registration.

#include "stdafx.h"
#include "CGResearch.h"
#include "CGResearch_i.c"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


CCGResearchApp NEAR theApp;

const GUID CDECL BASED_CODE _tlid =
		{ 0x8357BF3E, 0x8BCB, 0x451A, { 0x9C, 0xAB, 0x89, 0xA3, 0xF0, 0x2E, 0x10, 0xE9 } };
const WORD _wVerMajor = 1;
const WORD _wVerMinor = 0;



// CCGResearchApp::InitInstance - DLL initialization

BOOL CCGResearchApp::InitInstance()
{
	BOOL bInit = COleControlModule::InitInstance();

	if (bInit)
	{
		// TODO: Add your own module initialization code here.

	    // DUAL_SUPPORT_START
	    //    make sure the type library is registered. Otherwise dual interface won't work!
	    AfxOleRegisterTypeLib(AfxGetInstanceHandle(), LIBID_CGResearchLib, _T("CGResearch.tlb"));
	    // DUAL_SUPPORT_END
	}

	return bInit;
}



// CCGResearchApp::ExitInstance - DLL termination

int CCGResearchApp::ExitInstance()
{
	// TODO: Add your own module termination code here.

	return COleControlModule::ExitInstance();
}



// DllRegisterServer - Adds entries to the system registry

STDAPI DllRegisterServer(void)
{
	AFX_MANAGE_STATE(_afxModuleAddrThis);

	if (!AfxOleRegisterTypeLib(AfxGetInstanceHandle(), _tlid))
		return ResultFromScode(SELFREG_E_TYPELIB);

	if (!COleObjectFactoryEx::UpdateRegistryAll(TRUE))
		return ResultFromScode(SELFREG_E_CLASS);

	return NOERROR;
}



// DllUnregisterServer - Removes entries from the system registry

STDAPI DllUnregisterServer(void)
{
	AFX_MANAGE_STATE(_afxModuleAddrThis);

	if (!AfxOleUnregisterTypeLib(_tlid, _wVerMajor, _wVerMinor))
		return ResultFromScode(SELFREG_E_TYPELIB);

	if (!COleObjectFactoryEx::UpdateRegistryAll(FALSE))
		return ResultFromScode(SELFREG_E_CLASS);

	return NOERROR;
}

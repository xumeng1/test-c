// Copyright (c) 2004 PXIT. All Rights Reserved
//
// FileName    : ST_CGSystem.cpp
// Description : ST_CGSystem class function implementation
//               System Test of PXIT CLose-Grid 4.0
//
// Rev#  | Date          | Author               | Description of change
// --------------------------------------------------------------
// 0.1   | 05 Aug 2004   | Frank D'Arcy         | Initial Draft
//


#include "stdafx.h"
#include "ST_CGSystem.h"
#include "..\closegridsystemactrl1.h"
#include "productinfo.h"
#include "defaults.h"
#include "CGSystemErrors.h"


// Global instance of the LED Test Platform ActiveX Control
extern CClosegridsystemactrl1 g_test_cgsystem_axc;



// Macro to FAIL and print out an OLE Dispatch Exception if caught when it shouldn't
#define ASSERT_OLEDE_FAIL( EX ) \
		std::string errmsgstr = "OLE Dispatch Exception thrown"; \
		errmsgstr = errmsgstr + "\n  error code: "; \
		char buffer[20]; \
		_itoa( EX->m_wCode, buffer, 10 ); \
		errmsgstr = errmsgstr + buffer; \
		TCHAR errmsg[1000]; \
		EX->GetErrorMessage( errmsg, 1000 ); \
		errmsgstr = errmsgstr + "\n  error message: " + std::string(errmsg); \
		CPPUNIT_FAIL( errmsgstr );

// Macro to check the system state
#define ASSERT_SYSTEM_STATE( STATE ) \
	{ BSTR state_BSTR; \
	TRY { \
		g_led_test_platform_axc.getState( &state_BSTR ); \
	} CATCH( COleDispatchException, pEx ) { \
		ASSERT_OLEDE_FAIL( pEx ) \
	} END_CATCH \
	CPPUNIT_ASSERT( CString( state_BSTR ) == CString( STATE ) ); \
	SysFreeString( state_BSTR ); }


///////////////////////////////////////////////
// setUp - called before each test
//
void ST_CGSystem::setUp()
{
	// call shutdown, it does nothing yet 
	TRY
	{
		g_test_cgsystem_axc.shutdown();
	}
	CATCH( COleDispatchException, pEx )
	{
	}
	END_CATCH
}

///////////////////////////////////////////////
// tearDown - called after each test
//
void ST_CGSystem::tearDown() 
{
	// call shutdown, it does nothing yet 
	TRY
	{
		g_test_cgsystem_axc.shutdown();
	}
	CATCH( COleDispatchException, pEx )
	{
	}
	END_CATCH
}



///////////////////////////////////////////////
// test_loadOverallMapdutA
//
// tests object.loadOverallMap() with data for p32
//
void ST_CGSystem::test_loadOverallMap_p32()
{
	// call startup
	TRY
	{
		g_test_cgsystem_axc.startup();
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

	BSTR date_time_stamp_BSTR;

	// call setup
	TRY
	{
		g_test_cgsystem_axc.setup("DSDBR01","test_laser_id",&date_time_stamp_BSTR);
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH


	char results_dir[] = "C:\\CLoseGrid\\CLoseGrid4.0\\test\\testdata";
	char laser_type[] = "DSDBR01";
	//char laser_id[] = "dutA";
	//char date_time_stamp[] = "20040723135355";
	char laser_id[] = "p32";
	char date_time_stamp[] = "20040805150014";
	char map_ramp_direction[] = "R";

	// call loadOverallMap normally
	TRY
	{
		g_test_cgsystem_axc.loadOverallMap(
			results_dir,
			laser_type,
			laser_id,
			date_time_stamp,
			map_ramp_direction );
	}
	CATCH( COleDispatchException, pEx )
	{
		ASSERT_OLEDE_FAIL( pEx )
	}
	END_CATCH

}


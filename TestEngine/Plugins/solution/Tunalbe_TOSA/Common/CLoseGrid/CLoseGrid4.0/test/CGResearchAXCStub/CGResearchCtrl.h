#include <afx.h>
using namespace std;

#include <vector>

#pragma once

// CGResearchCtrl.h : Declaration of the CCGResearchCtrl ActiveX Control class.


// CCGResearchCtrl : See CGResearchCtrl.cpp for implementation.

class CCGResearchCtrl : public COleControl
{
	DECLARE_DYNCREATE(CCGResearchCtrl)

// Constructor
public:
	CCGResearchCtrl();

// Overrides
public:
	virtual void OnDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid);
	virtual void DoPropExchange(CPropExchange* pPX);
	virtual void OnResetState();
	virtual DWORD GetControlFlags();

// Implementation
protected:
	~CCGResearchCtrl();

	DECLARE_OLECREATE_EX(CCGResearchCtrl)    // Class factory and guid
	DECLARE_OLETYPELIB(CCGResearchCtrl)      // GetTypeInfo
	DECLARE_PROPPAGEIDS(CCGResearchCtrl)     // Property page IDs
	DECLARE_OLECTLTYPE(CCGResearchCtrl)		// Type name and misc status

// Message maps
	DECLARE_MESSAGE_MAP()

// Dispatch maps
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()

	afx_msg void AboutBox();
	afx_msg void startup();
	afx_msg void shutdown();
    afx_msg void setCurrent( BSTR moduleName, double current );
    afx_msg void getCurrent( BSTR moduleName, double* current );
    afx_msg void readVoltage( BSTR moduleName, double* voltage );
    afx_msg void getState( BSTR* current_state );
    afx_msg void livtest(
        short powermeter_slot_number,
        short powermeter_channel_number,
        short currentsource_slot_number,
        double start_current,
        double stop_current,
        long number_of_steps,
        long step_duration,
        VARIANT* current_array,
        VARIANT* voltage_array,
        VARIANT* power_array,
        VARIANT* dPdI_array,
        VARIANT* d2PdI2_array,
        BSTR* results_dir );
	afx_msg void setup( BSTR laser_id );
	afx_msg void rampdown();
    afx_msg void readPower(
        short slot_number,
        short channel_number,
        double* optical_power );
    afx_msg void readFrequency( double* frequency );
    afx_msg void readTemperature( double* temperature );
    afx_msg void setTECSetpoint( double temp_setpoint );
    afx_msg void getTECSetpoint( double* temp_setpoint );
    afx_msg void get2DData(
        short currentsource_slot_number,
        double min_current,
        double max_current,
        long number_of_steps,
        long step_duration,
 		VARIANT_BOOL frequencyUnits,
	    BSTR* results_dir );
    afx_msg void get3DData(
        short Ix_slot_number,
        double Ix_min_current,
        double Ix_max_current,
        long Ix_number_of_steps,
        long Ix_step_duration,
        short Iy_slot_number,
        double Iy_min_current,
        double Iy_max_current,
        long Iy_number_of_steps,
        VARIANT_BOOL hysteresis,
		VARIANT_BOOL frequencyUnits,
        BSTR* results_dir );
    afx_msg void getVersionInfo(BSTR* productName,
								BSTR* versionName,
								BSTR* label,
								BSTR* labelInfo,
								BSTR* labelComment,
								BSTR* buildConfig);
    afx_msg void setOutputConnection(BSTR moduleName, 
									short outputConnection);
    afx_msg void getOutputConnection(BSTR moduleName, 
									short* outputConnection);
    afx_msg void get3DData2(BSTR moduleNameX,
            VARIANT* currentsX,
            BSTR moduleNameY,
            VARIANT* currentsY,
            long stepDuration,
            VARIANT_BOOL hysteresis,
			VARIANT_BOOL frequencyUnits,
            BSTR* results_dir);
	afx_msg void resetSequences();
	afx_msg void load305Sequence(BSTR		moduleName,
								VARIANT*	currents,
								short		sourceDelay,
								short		measureDelay);
	afx_msg void load306Sequence(BSTR		moduleName,
								short		numPoints,
								short		channel,
								short		sourceDelay,
								short		measureDelay);
    afx_msg void getSequenceCount(BSTR moduleName, 
									short* sequenceCount);
    afx_msg void getCurrentSequenceNumber(BSTR moduleName, 
									short* currentSequenceNumber);
	afx_msg void runSequences();

    afx_msg void setInSequence(BSTR moduleName, 
							short inSequence);

    afx_msg void setCoarseFrequencyOffset(double powerRatio, 
										double	frequency);
    afx_msg void readCoarseFrequency( double* frequency );
    afx_msg void readOpticalPower(BSTR	moduleName,
								short	channel,
								double* opticalPower );
    afx_msg void readPhotodiodeCurrent(BSTR	moduleName,
									short	channel,
									double* current );
    afx_msg void readOpticalPowerAtFreq(BSTR	moduleName,
										short	channel,
										double	frequency,
										double* opticalPower );
    afx_msg void setTECPIDValues(double pValue, 
								double	iValue,
								double	dValue);

    afx_msg void getTECPIDValues(double* pValue, 
								double*	iValue,
								double*	dValue);
    afx_msg void get2DData2(BSTR moduleName,
            VARIANT* currents,
            long stepDuration,
			VARIANT_BOOL frequencyUnits,
            BSTR* results_dir);
    afx_msg void get306SequenceMeasurements(BSTR moduleName,
											VARIANT* channel1Measurements,
											VARIANT* channel2Measurements);

	// DUAL_SUPPORT_START
	//    add declaration of IDualAClick implementation
	//    You need one entry here for each entry in the
	//    interface statement of the ODL, plus the entries for a
	//    dispatch interface. The BEGIN_DUAL_INTERFACE_PART
	//    macro in mfcdual.h automatically generates the IDispatch
	//    entries for you.
	//    Each entry with the "propput" attribute needs a function
	//    named "put_<property name>". Each entry with the "propget"
	//    attribute needs a function named "get_<property name>".
	//    You can pull these function prototypes from the header file
	//    generated by MKTYPLIB.
	BEGIN_DUAL_INTERFACE_PART(Dual_DCGResearch, IDual_DCGResearch)
	//    STDMETHOD(AboutBox)(THIS);
	    STDMETHOD(startup)(THIS);
	    STDMETHOD(shutdown)(THIS);
        STDMETHOD(setCurrent)(THIS_ BSTR moduleName, double current );
        STDMETHOD(getCurrent)(THIS_ BSTR moduleName, double* current );
        STDMETHOD(readVoltage)(THIS_ BSTR moduleName, double* voltage );
        STDMETHOD(getState)(THIS_ BSTR* current_state);
        STDMETHOD(livtest)(THIS_ short powermeter_slot_number,
            short powermeter_channel_number,
            short currentsource_slot_number,
            double start_current,
            double stop_current,
            long number_of_steps,
            long step_duration,
            VARIANT* current_array,
            VARIANT* voltage_array,
            VARIANT* power_array,
            VARIANT* dPdI_array,
            VARIANT* d2PdI2_array,
            BSTR* results_dir );
	    STDMETHOD(setup)(THIS_ BSTR laser_id );
	    STDMETHOD(rampdown)(THIS);
        STDMETHOD(readPower)( THIS_
            short slot_number,
            short channel_number,
            double* optical_power );
        STDMETHOD(readFrequency)( THIS_ double* frequency );
        STDMETHOD(readTemperature)( THIS_ double* temperature );
        STDMETHOD(setTECSetpoint)( THIS_ double temp_setpoint );
        STDMETHOD(getTECSetpoint)( THIS_ double* temp_setpoint );
        STDMETHOD(get2DData)( THIS_
            short currentsource_slot_number,
            double min_current,
            double max_current,
            long number_of_steps,
            long step_duration,
			VARIANT_BOOL frequencyUnits,
            BSTR* results_dir );
        STDMETHOD(get3DData)( THIS_
            short Ix_slot_number,
            double Ix_min_current,
            double Ix_max_current,
            long Ix_number_of_steps,
            long Ix_step_duration,
            short Iy_slot_number,
            double Iy_min_current,
            double Iy_max_current,
            long Iy_number_of_steps,
            VARIANT_BOOL hysteresis,
			VARIANT_BOOL frequencyUnits,
            BSTR* results_dir );
		STDMETHOD(getVersionInfo)( THIS_ 
								BSTR* productName,
								BSTR* versionName,
								BSTR* label,
								BSTR* labelInfo,
								BSTR* labelComment,
								BSTR* buildConfig);
        STDMETHOD(setOutputConnection)(THIS_ 
									BSTR moduleName, 
									short outputConnection);
        STDMETHOD(getOutputConnection)(THIS_ 
									BSTR moduleName, 
									short* outputConnection);
        STDMETHOD(get3DData2)( THIS_
            BSTR moduleNameX,
            VARIANT* currentsX,
            BSTR moduleNameY,
            VARIANT* currentsY,
            long Ix_step_duration,
            VARIANT_BOOL hysteresis,
			VARIANT_BOOL frequencyUnits,
            BSTR* results_dir);
	    STDMETHOD(resetSequences)(THIS);
        STDMETHOD(load305Sequence)( THIS_
            BSTR		moduleName,
            VARIANT*	currents,
            short		sourceDelay,
			short		measureDelay);
        STDMETHOD(load306Sequence)( THIS_
            BSTR		moduleName,
            short		numPoints,
			short		channel,
            short		sourceDelay,
			short		measureDelay);
        STDMETHOD(getSequenceCount)(THIS_ 
									BSTR moduleName, 
									short* sequenceCount);
        STDMETHOD(getCurrentSequenceNumber)(THIS_ 
									BSTR moduleName, 
									short* currentSequenceNumber);
	    STDMETHOD(runSequences)(THIS);

        STDMETHOD(setInSequence)(THIS_ 
								BSTR moduleName, 
								short inSequence);

        STDMETHOD(setCoarseFrequencyOffset)(THIS_ 
								double	powerRatio, 
								double	frequency);

        STDMETHOD(readCoarseFrequency)( THIS_ double* frequency );

        STDMETHOD(readOpticalPower)( THIS_
            BSTR	moduleName,
            short	channel,
            double* opticalPower );

        STDMETHOD(readPhotodiodeCurrent)( THIS_
            BSTR	moduleName,
            short	channel,
            double* current );

        STDMETHOD(readOpticalPowerAtFreq)( THIS_
            BSTR	moduleName,
            short	channel,
			double	frequency,
            double* opticalPower );

        STDMETHOD(setTECPIDValues)(THIS_ 
								double	pValue, 
								double	iValue,
								double	dValue);

        STDMETHOD(getTECPIDValues)(THIS_ 
								double*	pValue, 
								double*	iValue,
								double*	dValue);

        STDMETHOD(get2DData2)( THIS_
            BSTR moduleName,
            VARIANT* currents,
            long stepDuration,
			VARIANT_BOOL frequencyUnits,
            BSTR* results_dir);

        STDMETHOD(get306SequenceMeasurements)( THIS_
								            BSTR moduleName,
											VARIANT* channel1Measurements,
											VARIANT* channel2Measurements);


	END_DUAL_INTERFACE_PART(Dual_DCGResearch)

	//     add declaration of ISupportErrorInfo implementation
	//     to indicate we support the OLE Automation error object
	DECLARE_DUAL_ERRORINFO()
	// DUAL_SUPPORT_END

// Event maps
	DECLARE_EVENT_MAP()

// Dispatch and event IDs
public:
	enum {
	};

private:

    bool	_diskFullLogged;
	CString		_logFileDrive;
	CString		_resultsDrive;
	CRITICAL_SECTION	criticalSection;	

	//void	setLogFileDrive();	
	//void	setResultsDrive();	
	//bool	validFunctionCall(CString functionName);
	//bool	validLaserId(CString laserId);
	//bool	diskFull(CString diskDrive);

	void			convertVectorToVariant(vector<double>& array, VARIANT& variantArray);
	vector<double>	convertVariantToVector(VARIANT&	varArray);

};


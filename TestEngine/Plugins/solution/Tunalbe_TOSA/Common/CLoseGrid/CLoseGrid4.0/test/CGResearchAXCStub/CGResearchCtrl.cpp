// CGResearchCtrl.cpp : Implementation of the CCGResearchCtrl ActiveX Control class.

#include "stdafx.h"
#include "CGResearch.h"
#include "CGResearchCtrl.h"
#include "CGResearchPropPage.h"
//#include "CLoseGridSystem.h"
#include "CLoseGridErrorCodes.h"
//#include "LogFile.h"
//#include "CLoseGridConf.h"
//#include "CLoseGridConf.h"
#include <comdef.h>
#include "versioncontrol.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


IMPLEMENT_DYNCREATE(CCGResearchCtrl, COleControl)



// Message map

BEGIN_MESSAGE_MAP(CCGResearchCtrl, COleControl)
	//ON_OLEVERB(AFX_IDS_VERB_PROPERTIES, OnProperties)
END_MESSAGE_MAP()



// Dispatch map

BEGIN_DISPATCH_MAP(CCGResearchCtrl, COleControl)
	//DISP_FUNCTION_ID(CCGResearchCtrl, "AboutBox", DISPID_ABOUTBOX, AboutBox, VT_EMPTY, VTS_NONE)
	DISP_FUNCTION_ID(CCGResearchCtrl, "startup", 1, startup, VT_EMPTY, VTS_NONE)
	DISP_FUNCTION_ID(CCGResearchCtrl, "shutdown", 2, shutdown, VT_EMPTY, VTS_NONE)
	DISP_FUNCTION_ID(CCGResearchCtrl, "setCurrent", 3, setCurrent, VT_EMPTY, VTS_BSTR VTS_R8)
	DISP_FUNCTION_ID(CCGResearchCtrl, "getCurrent", 4, getCurrent, VT_EMPTY, VTS_BSTR VTS_PR8)
	DISP_FUNCTION_ID(CCGResearchCtrl, "readVoltage", 5, readVoltage, VT_EMPTY, VTS_I2 VTS_PR8)
	DISP_FUNCTION_ID(CCGResearchCtrl, "getState", 6, getState, VT_EMPTY, VTS_PBSTR)
    DISP_FUNCTION_ID(CCGResearchCtrl, "livtest", 7, livtest, VT_EMPTY, VTS_I2 VTS_I2 VTS_I2 VTS_R8 VTS_R8 VTS_I4 VTS_I4 VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PBSTR )
	DISP_FUNCTION_ID(CCGResearchCtrl, "setup", 8, setup, VT_EMPTY, VTS_BSTR)
	DISP_FUNCTION_ID(CCGResearchCtrl, "rampdown", 9, rampdown, VT_EMPTY, VTS_NONE)
	DISP_FUNCTION_ID(CCGResearchCtrl, "readPower", 10, readPower, VT_EMPTY, VTS_I2 VTS_I2 VTS_PR8 )
	DISP_FUNCTION_ID(CCGResearchCtrl, "readFrequency", 11, readFrequency, VT_EMPTY, VTS_PR8 )
	DISP_FUNCTION_ID(CCGResearchCtrl, "readTemperature", 12, readTemperature, VT_EMPTY, VTS_PR8 )
	DISP_FUNCTION_ID(CCGResearchCtrl, "setTECSetpoint", 13, setTECSetpoint, VT_EMPTY, VTS_R8 )
	DISP_FUNCTION_ID(CCGResearchCtrl, "getTECSetpoint", 14, getTECSetpoint, VT_EMPTY, VTS_PR8 )
	DISP_FUNCTION_ID(CCGResearchCtrl, "get2DData", 15, get2DData, VT_EMPTY, VTS_I2 VTS_R8 VTS_R8 VTS_I4 VTS_I4 VTS_BOOL VTS_PBSTR VTS_PI2 )
	DISP_FUNCTION_ID(CCGResearchCtrl, "get3DData", 16, get3DData, VT_EMPTY, VTS_I2 VTS_R8 VTS_R8 VTS_I4 VTS_I4 VTS_I2 VTS_R8 VTS_R8 VTS_I4 VTS_BOOL VTS_BOOL VTS_PBSTR VTS_PI2 )
	DISP_FUNCTION_ID(CCGResearchCtrl, "getVersionInfo", 17, getVersionInfo, VT_EMPTY, VTS_PBSTR VTS_PBSTR VTS_PBSTR VTS_PBSTR VTS_PBSTR VTS_PBSTR)
	DISP_FUNCTION_ID(CCGResearchCtrl, "setOutputConnection", 18, setOutputConnection, VT_EMPTY, VTS_PBSTR VTS_I2)
	DISP_FUNCTION_ID(CCGResearchCtrl, "getOutputConnection", 19, getOutputConnection, VT_EMPTY, VTS_PBSTR VTS_PI2)
	DISP_FUNCTION_ID(CCGResearchCtrl, "get3DData2", 20, get3DData2, VT_EMPTY, VTS_PBSTR VTS_PVARIANT VTS_PBSTR VTS_PVARIANT VTS_I4 VTS_BOOL VTS_BOOL VTS_PBSTR )
	DISP_FUNCTION_ID(CCGResearchCtrl, "resetSequences", 21, resetSequences, VT_EMPTY, VTS_NONE)
	DISP_FUNCTION_ID(CCGResearchCtrl, "load305Sequence", 22, load305Sequence, VT_EMPTY, VTS_PBSTR VTS_PVARIANT VTS_I2 VTS_I2 )
	DISP_FUNCTION_ID(CCGResearchCtrl, "load306Sequence", 23, load306Sequence, VT_EMPTY, VTS_PBSTR VTS_I2 VTS_I2 VTS_I2 VTS_I2 )
	DISP_FUNCTION_ID(CCGResearchCtrl, "getSequenceCount", 24, getSequenceCount, VT_EMPTY, VTS_PBSTR VTS_PI2)
	DISP_FUNCTION_ID(CCGResearchCtrl, "getCurrentSequenceNumber", 25, getCurrentSequenceNumber, VT_EMPTY, VTS_PBSTR VTS_PI2)
	DISP_FUNCTION_ID(CCGResearchCtrl, "runSequences", 26, runSequences, VT_EMPTY, VTS_NONE)
	DISP_FUNCTION_ID(CCGResearchCtrl, "setInSequence", 27, setInSequence, VT_EMPTY, VTS_PBSTR VTS_I2)
	DISP_FUNCTION_ID(CCGResearchCtrl, "setCoarseFrequencyOffset", 28, setCoarseFrequencyOffset, VT_EMPTY, VTS_R8 VTS_R8)
	DISP_FUNCTION_ID(CCGResearchCtrl, "readCoarseFrequency", 29, readCoarseFrequency, VT_EMPTY, VTS_PR8 )
	DISP_FUNCTION_ID(CCGResearchCtrl, "readOpticalPower", 30, readOpticalPower, VT_EMPTY, VTS_PBSTR VTS_I2 VTS_PR8 )
	DISP_FUNCTION_ID(CCGResearchCtrl, "readPhotodiodeCurrent", 31, readPhotodiodeCurrent, VT_EMPTY, VTS_PBSTR VTS_I2 VTS_PR8 )
	DISP_FUNCTION_ID(CCGResearchCtrl, "readOpticalPowerAtFreq", 32, readOpticalPowerAtFreq, VT_EMPTY, VTS_PBSTR VTS_I2 VTS_R8 VTS_PR8 )
	DISP_FUNCTION_ID(CCGResearchCtrl, "setTECPIDValues", 33, setTECPIDValues, VT_EMPTY, VTS_R8 VTS_R8 VTS_R8)
	DISP_FUNCTION_ID(CCGResearchCtrl, "getTECPIDValues", 34, getTECPIDValues, VT_EMPTY, VTS_PR8 VTS_PR8 VTS_PR8)
END_DISPATCH_MAP()

// DUAL_SUPPORT_START 
// Note: we add support for DIID__DCGResearch to support typesafe binding
// from VBA.  We add support for IID_IDual_DCGResearch to support our dual
// interface. See CGResearchidl.h for the definition of DIID__DCGResearch and
// IID_IDual_DCGResearch.
//
// DUAL_ERRORINFO_PART indicates we support OLE Automation
// error handling.
//
BEGIN_INTERFACE_MAP(CCGResearchCtrl, COleControl)
	INTERFACE_PART(CCGResearchCtrl, DIID__DCGResearch, Dispatch)
	INTERFACE_PART(CCGResearchCtrl, IID_IDual_DCGResearch, Dual_DCGResearch)
	DUAL_ERRORINFO_PART(CCGResearchCtrl)
END_INTERFACE_MAP()

// DUAL_SUPPORT_END

// Event map

BEGIN_EVENT_MAP(CCGResearchCtrl, COleControl)
END_EVENT_MAP()



// Property pages

// TODO: Add more property pages as needed.  Remember to increase the count!
BEGIN_PROPPAGEIDS(CCGResearchCtrl, 1)
	PROPPAGEID(CCGResearchPropPage::guid)
END_PROPPAGEIDS(CCGResearchCtrl)



// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CCGResearchCtrl, "CGRESEARCH.CGResearchCtrl.1",
	0x8b09736b, 0x6334, 0x4eb7, 0xb3, 0xd5, 0x63, 0xbb, 0xad, 0x7b, 0x77, 0x7)



// Type library ID and version

IMPLEMENT_OLETYPELIB(CCGResearchCtrl, _tlid, _wVerMajor, _wVerMinor)



// Interface IDs

const IID BASED_CODE IID_DCGResearch =
		{ 0x96B7E9F2, 0xD1B2, 0x449D, { 0x8B, 0xA, 0x71, 0x1C, 0x52, 0x74, 0xB1, 0x9A } };
const IID BASED_CODE IID_DCGResearchEvents =
		{ 0x27B4CC7F, 0x30B0, 0x4CD6, { 0xB6, 0x6, 0xA6, 0x39, 0xF3, 0xBE, 0x6, 0x96 } };



// Control type information

static const DWORD BASED_CODE _dwCGResearchOleMisc =
	OLEMISC_ACTIVATEWHENVISIBLE |
	OLEMISC_SETCLIENTSITEFIRST |
	OLEMISC_INSIDEOUT |
	OLEMISC_CANTLINKINSIDE |
	OLEMISC_RECOMPOSEONRESIZE;

IMPLEMENT_OLECTLTYPE(CCGResearchCtrl, IDS_CGRESEARCH, _dwCGResearchOleMisc)



// CCGResearchCtrl::CCGResearchCtrlFactory::UpdateRegistry -
// Adds or removes system registry entries for CCGResearchCtrl

BOOL CCGResearchCtrl::CCGResearchCtrlFactory::UpdateRegistry(BOOL bRegister)
{
	// TODO: Verify that your control follows apartment-model threading rules.
	// Refer to MFC TechNote 64 for more information.
	// If your control does not conform to the apartment-model rules, then
	// you must modify the code below, changing the 6th parameter from
	// afxRegApartmentThreading to 0.

	if (bRegister)
		return AfxOleRegisterControlClass(
			AfxGetInstanceHandle(),
			m_clsid,
			m_lpszProgID,
			IDS_CGRESEARCH,
			IDB_CGRESEARCH,
			afxRegApartmentThreading,
			_dwCGResearchOleMisc,
			_tlid,
			_wVerMajor,
			_wVerMinor);
	else
		return AfxOleUnregisterClass(m_clsid, m_lpszProgID);
}



// CCGResearchCtrl::CCGResearchCtrl - Constructor

CCGResearchCtrl::CCGResearchCtrl()
{
	InitializeIIDs(&IID_DCGResearch, &IID_DCGResearchEvents);
	// TODO: Initialize your control's instance data here.

	InitializeCriticalSection(&criticalSection);

	_diskFullLogged = false;
	_logFileDrive = "";
	_resultsDrive = "";

    // Instantiate CLoseGridSystem singleton
    //CLoseGridSystem::instance();
}



// CCGResearchCtrl::~CCGResearchCtrl - Destructor

CCGResearchCtrl::~CCGResearchCtrl()
{
	// TODO: Cleanup your control's instance data here.
	DeleteCriticalSection(&criticalSection);
}



// CCGResearchCtrl::OnDraw - Drawing function

void CCGResearchCtrl::OnDraw(
			CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid)
{
	// TODO: Replace the following code with your own drawing code.
	pdc->FillRect(rcBounds, CBrush::FromHandle((HBRUSH)GetStockObject(WHITE_BRUSH)));
	pdc->Ellipse(rcBounds);
}



// CCGResearchCtrl::DoPropExchange - Persistence support

void CCGResearchCtrl::DoPropExchange(CPropExchange* pPX)
{
	ExchangeVersion(pPX, MAKELONG(_wVerMinor, _wVerMajor));
	COleControl::DoPropExchange(pPX);

	// TODO: Call PX_ functions for each persistent custom property.
}



// CCGResearchCtrl::GetControlFlags -
// Flags to customize MFC's implementation of ActiveX controls.
//
// For information on using these flags, please see MFC technical note
// #nnn, "Optimizing an ActiveX Control".
DWORD CCGResearchCtrl::GetControlFlags()
{
	DWORD dwFlags = COleControl::GetControlFlags();


	// The control can activate without creating a window.
	// TODO: when writing the control's message handlers, avoid using
	//		the m_hWnd member variable without first checking that its
	//		value is non-NULL.
	dwFlags |= windowlessActivate;
	return dwFlags;
}



// CCGResearchCtrl::OnResetState - Reset control to default state

void CCGResearchCtrl::OnResetState()
{
	COleControl::OnResetState();  // Resets defaults found in DoPropExchange

	// TODO: Reset any other control state here.
}



// CCGResearchCtrl::AboutBox - Display an "About" box to the user

void CCGResearchCtrl::AboutBox()
{
	CDialog dlgAbout(IDD_ABOUTBOX_CGRESEARCH);
	dlgAbout.DoModal();
}


void CCGResearchCtrl::startup()
{
	HRESULT hr = S_OK;

	//if(_logFileDrive=="")
	//	setLogFileDrive();

	//if(diskFull(_logFileDrive))
	//{
	//	hr = CG_ERROR_LOGFILE_DISK_FULL;
	//}

 //   if( SUCCEEDED(hr) )
 //   {

	//    CG_LOG("CCGResearchCtrl::startup() : Entered", DIAGNOSTIC_LEVEL)

	//    if(!validFunctionCall("startup"))
	//    {
	//	    hr = CG_ERROR_INVALID_STATE;
	//    }
	//    else
	//    {
	//	    if(CLoseGridSystem::instance()!=NULL)
	//	    {
	//		    hr = CLoseGridSystem::instance()->startup();

	//		    if(SUCCEEDED(hr))
 //                   CLoseGridSystem::instance()->setSystemState(CLoseGridSystem::State::Startup);

	//	    }
	//	    else
	//	    {
	//		    hr = CG_ERROR_SYSTEM_ERROR;
	//	    }
	//    }
 //   }

	//// trace
	//CG_LOG("CCGResearchCtrl::startup() : Exiting", DIAGNOSTIC_LEVEL)

    if(FAILED(hr)) AfxThrowOleException(hr);
    return;
}

void CCGResearchCtrl::shutdown()
{
	//CG_LOG("Shutting down CLoseGrid....", INFO_LEVEL)

    //HRESULT hr = S_OK;

	//if(_resultsDrive=="")
	//	setResultsDrive();
	//if(diskFull(_resultsDrive))
	//{
	//	hr = CG_ERROR_RESULTS_DISK_FULL;
	//}

	//if( SUCCEEDED(hr) && _logFileDrive=="")
	//	setLogFileDrive();
	//if(diskFull(_logFileDrive))
	//{
	//	hr = CG_ERROR_LOGFILE_DISK_FULL;
	//}

 //   if( SUCCEEDED(hr) )
 //   {
	//    if( !validFunctionCall("shutdown"))
	//    {
	//	    hr = CG_ERROR_INVALID_STATE;
	//    }
	//    else
	//    {
	//	    hr = CLoseGridSystem::instance()->shutdown();
	//	    if(SUCCEEDED(hr))
	//	    {
	//	        CLoseGridSystem::instance()->setSystemState(
 //                   CLoseGridSystem::State::Instantiated);
	//	    }
	//    }
 //   }

 //   if(FAILED(hr)) AfxThrowOleException(hr);
    return;
}


void CCGResearchCtrl::setCurrent( BSTR moduleName, double current )
{
	//CString moduleNameString(moduleName);

	//char infoStr[BUF_SIZE];
	//memset(infoStr, '\0', sizeof(infoStr));
	//sprintf(infoStr, 
	//	"Setting module %s current to %g", 
	//	moduleNameString, current);
	//CG_LOG(infoStr, INFO_LEVEL)

 //   HRESULT hr = S_OK;

	//if(_resultsDrive=="")
	//	setResultsDrive();
	//if(diskFull(_resultsDrive))
	//{
	//	hr = CG_ERROR_RESULTS_DISK_FULL;
	//}

 //   if( SUCCEEDED(hr) )
 //   {
	//    if(_logFileDrive=="")
	//	    setLogFileDrive();
	//    if(diskFull(_logFileDrive))
	//    {
	//	    hr = CG_ERROR_LOGFILE_DISK_FULL;
	//    }
 //   }

 //   if( SUCCEEDED(hr) )
 //   {
 //   	CG_LOG("CCGResearchCtrl::setCurrent() : Entered\n", DIAGNOSTIC_LEVEL)

	//    // we can only perform this operation
	//    // if the system is ready and a laser
	//    // is installed
	//    if(!validFunctionCall("setCurrent"))
	//    {
	//	    hr = CG_ERROR_INVALID_STATE;
	//    }
	//    else
	//    {
	//	    hr = CLoseGridSystem::instance()->setCurrent(moduleNameString, current);
	//    }

	//    CG_LOG("CCGResearchCtrl::setCurrent( ) : Exiting", DIAGNOSTIC_LEVEL)
 //   }

 //   if(FAILED(hr)) AfxThrowOleException(hr);
    return;
}


void CCGResearchCtrl::getCurrent( BSTR moduleName, double* current )
{
 //   HRESULT hr = S_OK;

	//CString moduleNameString(moduleName);

	//if(_resultsDrive=="")
	//	setResultsDrive();
	//if(diskFull(_resultsDrive))
	//{
	//	hr = CG_ERROR_RESULTS_DISK_FULL;
	//}

 //   if( SUCCEEDED(hr) )
 //   {
	//    if(_logFileDrive=="")
	//	    setLogFileDrive();
	//    if(diskFull(_logFileDrive))
	//    {
	//	    hr = CG_ERROR_LOGFILE_DISK_FULL;
	//    }
 //   }

 //   if( SUCCEEDED(hr) )
 //   {
 //   	CG_LOG("CCGResearchCtrl::getCurrent() : Entered\n", DIAGNOSTIC_LEVEL)

	//    // we can only perform this operation
	//    // if the system is ready and a laser
	//    // is installed
	//    if(!validFunctionCall("getCurrent"))
	//    {
	//	    hr = CG_ERROR_INVALID_STATE;
	//    }
	//    else
	//    {
 //           CLoseGridSystem::instance()->shelfInUse->getCurrent(
 //               moduleNameString, *current);
	//    }

	//    CG_LOG("CCGResearchCtrl::getCurrent() : Exiting", DIAGNOSTIC_LEVEL)
 //   }

 //   if(FAILED(hr)) AfxThrowOleException(hr);
    return;
}

void 
CCGResearchCtrl::
setOutputConnection(BSTR moduleName,
					short outputConnection)
{

	//CString moduleNameString(moduleName);

	//char infoStr[BUF_SIZE];
	//memset(infoStr, '\0', sizeof(infoStr));
	//sprintf(infoStr, 
	//	"Setting output connection %d  on module %s", 
	//	outputConnection, 
	//	moduleNameString);
	//CG_LOG(infoStr, INFO_LEVEL)

 //   HRESULT hr = S_OK;

	//if(_resultsDrive=="")
	//	setResultsDrive();
	//if(diskFull(_resultsDrive))
	//{
	//	hr = CG_ERROR_RESULTS_DISK_FULL;
	//}

 //   if( SUCCEEDED(hr) )
 //   {
	//    if(_logFileDrive=="")
	//	    setLogFileDrive();
	//    if(diskFull(_logFileDrive))
	//    {
	//	    hr = CG_ERROR_LOGFILE_DISK_FULL;
	//    }
 //   }

 //   if( SUCCEEDED(hr) )
 //   {
 //   	CG_LOG("CGResearchCtrl::setOutputConnection() : Entered\n", DIAGNOSTIC_LEVEL)

	//    // we can only perform this operation
	//    // if the system is ready and a laser
	//    // is installed
	//    if(!validFunctionCall("setOutputConnection"))
	//    {
	//	    hr = CG_ERROR_INVALID_STATE;
	//    }
	//    else
	//    {

	//		hr = (HRESULT)(CLoseGridSystem::instance()->setOutputConnection(moduleNameString, outputConnection));
	//    }

	//    CG_LOG("CGResearchCtrl::setOutputConnection( ) : Exiting", DIAGNOSTIC_LEVEL)
 //   }

 //   if(FAILED(hr)) AfxThrowOleException(hr);
    return;
}

void 
CCGResearchCtrl::
getOutputConnection(BSTR moduleName,
					short* outputConnection)
{
	//CString moduleNameString(moduleName);

	//char infoStr[BUF_SIZE];
	//memset(infoStr, '\0', sizeof(infoStr));
	//sprintf(infoStr, 
	//	"Getting output connection on module %s", 
	//	moduleNameString);
	//CG_LOG(infoStr, INFO_LEVEL)

 //   HRESULT hr = S_OK;

	//if(_resultsDrive=="")
	//	setResultsDrive();
	//if(diskFull(_resultsDrive))
	//{
	//	hr = CG_ERROR_RESULTS_DISK_FULL;
	//}

 //   if( SUCCEEDED(hr) )
 //   {
	//    if(_logFileDrive=="")
	//	    setLogFileDrive();
	//    if(diskFull(_logFileDrive))
	//    {
	//	    hr = CG_ERROR_LOGFILE_DISK_FULL;
	//    }
 //   }

 //   if( SUCCEEDED(hr) )
 //   {
 //   	CG_LOG("CGResearchCtrl::getOutputConnection() : Entered\n", DIAGNOSTIC_LEVEL)

	//    // we can only perform this operation
	//    // if the system is ready and a laser
	//    // is installed
	//    if(!validFunctionCall("getOutputConnection"))
	//    {
	//	    hr = CG_ERROR_INVALID_STATE;
	//    }
	//    else
	//    {

	//		hr = (HRESULT)(CLoseGridSystem::instance()->getOutputConnection(moduleNameString, *outputConnection));
	//    }

	//    CG_LOG("CGResearchCtrl::setOutputConnection( ) : Exiting", DIAGNOSTIC_LEVEL)
 //   }

 //   if(FAILED(hr)) AfxThrowOleException(hr);
    return;
}

void 
CCGResearchCtrl::
setInSequence(BSTR moduleName,
			short inSequence)
{

	//CString moduleNameString(moduleName);

	//char infoStr[BUF_SIZE];
	//memset(infoStr, '\0', sizeof(infoStr));
	//sprintf(infoStr, 
	//	"Setting module %s sequence setting to %d", 
	//	moduleNameString,
	//	inSequence);
	//CG_LOG(infoStr, INFO_LEVEL)

 //   HRESULT hr = S_OK;

	//if(_resultsDrive=="")
	//	setResultsDrive();
	//if(diskFull(_resultsDrive))
	//{
	//	hr = CG_ERROR_RESULTS_DISK_FULL;
	//}

 //   if( SUCCEEDED(hr) )
 //   {
	//    if(_logFileDrive=="")
	//	    setLogFileDrive();
	//    if(diskFull(_logFileDrive))
	//    {
	//	    hr = CG_ERROR_LOGFILE_DISK_FULL;
	//    }
 //   }

 //   if( SUCCEEDED(hr) )
 //   {
 //   	CG_LOG("CGResearchCtrl::setInSequence() : Entered\n", DIAGNOSTIC_LEVEL)

	//    // we can only perform this operation
	//    // if the system is ready and a laser
	//    // is installed
	//    if(!validFunctionCall("setInSequence"))
	//    {
	//	    hr = CG_ERROR_INVALID_STATE;
	//    }
	//    else
	//    {

	//		hr = (HRESULT)(CLoseGridSystem::instance()->setInSequence(moduleNameString, inSequence));
	//    }

	//    CG_LOG("CGResearchCtrl::setInSequence( ) : Exiting", DIAGNOSTIC_LEVEL)
 //   }

 //   if(FAILED(hr)) AfxThrowOleException(hr);
    return;
}

void 
CCGResearchCtrl::
setCoarseFrequencyOffset(double powerRatio,
						double frequency)
{

	//char infoStr[BUF_SIZE];
	//memset(infoStr, '\0', sizeof(infoStr));
	//sprintf(infoStr, 
	//	"Setting coarse frequency offset: power ratio = %f, frequency = %f", 
	//	powerRatio,
	//	frequency);
	//CG_LOG(infoStr, INFO_LEVEL)

 //   HRESULT hr = S_OK;

	//if(_resultsDrive=="")
	//	setResultsDrive();
	//if(diskFull(_resultsDrive))
	//{
	//	hr = CG_ERROR_RESULTS_DISK_FULL;
	//}

 //   if( SUCCEEDED(hr) )
 //   {
	//    if(_logFileDrive=="")
	//	    setLogFileDrive();
	//    if(diskFull(_logFileDrive))
	//    {
	//	    hr = CG_ERROR_LOGFILE_DISK_FULL;
	//    }
 //   }

 //   if( SUCCEEDED(hr) )
 //   {
 //   	CG_LOG("CGResearchCtrl::setCoarseFrequencyOffset() : Entered\n", DIAGNOSTIC_LEVEL)

	//    // we can only perform this operation
	//    // if the system is ready and a laser
	//    // is installed
	//    if(!validFunctionCall("setCoarseFrequencyOffset"))
	//    {
	//	    hr = CG_ERROR_INVALID_STATE;
	//    }
	//    else
	//    {

	//		hr = (HRESULT)(CLoseGridSystem::instance()->setCoarseFrequencyOffset(powerRatio, frequency));
	//    }

	//    CG_LOG("CGResearchCtrl::setCoarseFrequencyOffset( ) : Exiting", DIAGNOSTIC_LEVEL)
 //   }

 //   if(FAILED(hr)) AfxThrowOleException(hr);
    return;
}

void 
CCGResearchCtrl::
setTECPIDValues(double pValue,
				double iValue,
				double dValue)
{

	//char infoStr[BUF_SIZE];
	//memset(infoStr, '\0', sizeof(infoStr));
	//sprintf(infoStr, 
	//	"Setting TEC PID Values: p value = %f, i value = %f, d value = %f", 
	//	pValue,
	//	iValue,
	//	dValue);
	//CG_LOG(infoStr, INFO_LEVEL)

 //   HRESULT hr = S_OK;

	//if(_resultsDrive=="")
	//	setResultsDrive();
	//if(diskFull(_resultsDrive))
	//{
	//	hr = CG_ERROR_RESULTS_DISK_FULL;
	//}

 //   if( SUCCEEDED(hr) )
 //   {
	//    if(_logFileDrive=="")
	//	    setLogFileDrive();
	//    if(diskFull(_logFileDrive))
	//    {
	//	    hr = CG_ERROR_LOGFILE_DISK_FULL;
	//    }
 //   }

 //   if( SUCCEEDED(hr) )
 //   {
 //   	CG_LOG("CGResearchCtrl::setTECPIDValues() : Entered\n", DIAGNOSTIC_LEVEL)

	//    // we can only perform this operation
	//    // if the system is ready and a laser
	//    // is installed
	//    if(!validFunctionCall("setTECPIDValues"))
	//    {
	//	    hr = CG_ERROR_INVALID_STATE;
	//    }
	//    else
	//    {

	//		hr = (HRESULT)(CLoseGridSystem::instance()->setTECPIDValues(pValue, iValue, dValue));
	//    }

	//    CG_LOG("CGResearchCtrl::setTECPIDValues( ) : Exiting", DIAGNOSTIC_LEVEL)
 //   }

 //   if(FAILED(hr)) AfxThrowOleException(hr);
    return;
}

void 
CCGResearchCtrl::
getTECPIDValues(double* pValue,
				double* iValue,
				double* dValue)
{

	//char infoStr[BUF_SIZE];
	//memset(infoStr, '\0', sizeof(infoStr));
	//sprintf(infoStr, "Getting TEC PID Values");
	//CG_LOG(infoStr, INFO_LEVEL)

 //   HRESULT hr = S_OK;

	//if(_resultsDrive=="")
	//	setResultsDrive();
	//if(diskFull(_resultsDrive))
	//{
	//	hr = CG_ERROR_RESULTS_DISK_FULL;
	//}

 //   if( SUCCEEDED(hr) )
 //   {
	//    if(_logFileDrive=="")
	//	    setLogFileDrive();
	//    if(diskFull(_logFileDrive))
	//    {
	//	    hr = CG_ERROR_LOGFILE_DISK_FULL;
	//    }
 //   }

 //   if( SUCCEEDED(hr) )
 //   {
 //   	CG_LOG("CGResearchCtrl::getTECPIDValues() : Entered\n", DIAGNOSTIC_LEVEL)

	//    // we can only perform this operation
	//    // if the system is ready and a laser
	//    // is installed
	//    if(!validFunctionCall("getTECPIDValues"))
	//    {
	//	    hr = CG_ERROR_INVALID_STATE;
	//    }
	//    else
	//    {

	//		hr = (HRESULT)(CLoseGridSystem::instance()->getTECPIDValues(*pValue, *iValue, *dValue));
	//    }

	//    CG_LOG("CGResearchCtrl::getTECPIDValues( ) : Exiting", DIAGNOSTIC_LEVEL)
 //   }

 //   if(FAILED(hr)) AfxThrowOleException(hr);
    return;
}

void 
CCGResearchCtrl::
getSequenceCount(BSTR moduleName,
				short* sequenceCount)
{
	//CString moduleNameString(moduleName);

	//char infoStr[BUF_SIZE];
	//memset(infoStr, '\0', sizeof(infoStr));
	//sprintf(infoStr, 
	//	"Getting sequence count on module %s", 
	//	moduleNameString);
	//CG_LOG(infoStr, INFO_LEVEL)

 //   HRESULT hr = S_OK;

	//if(_resultsDrive=="")
	//	setResultsDrive();
	//if(diskFull(_resultsDrive))
	//{
	//	hr = CG_ERROR_RESULTS_DISK_FULL;
	//}

 //   if( SUCCEEDED(hr) )
 //   {
	//    if(_logFileDrive=="")
	//	    setLogFileDrive();
	//    if(diskFull(_logFileDrive))
	//    {
	//	    hr = CG_ERROR_LOGFILE_DISK_FULL;
	//    }
 //   }

 //   if( SUCCEEDED(hr) )
 //   {
 //   	CG_LOG("CGResearchCtrl::getSequenceCount() : Entered\n", DIAGNOSTIC_LEVEL)

	//    // we can only perform this operation
	//    // if the system is ready and a laser
	//    // is installed
	//    if(!validFunctionCall("getSequenceCount"))
	//    {
	//	    hr = CG_ERROR_INVALID_STATE;
	//    }
	//    else
	//    {

	//		hr = (HRESULT)(CLoseGridSystem::instance()->getSequenceCount(moduleNameString, *sequenceCount));
	//    }

	//    CG_LOG("CGResearchCtrl::getSequenceCount( ) : Exiting", DIAGNOSTIC_LEVEL)
 //   }

 //   if(FAILED(hr)) AfxThrowOleException(hr);
    return;
}

void 
CCGResearchCtrl::
getCurrentSequenceNumber(BSTR moduleName,
				short* currentSequenceNumber)
{
	//CString moduleNameString(moduleName);

	//char infoStr[BUF_SIZE];
	//memset(infoStr, '\0', sizeof(infoStr));
	//sprintf(infoStr, 
	//	"Getting current sequence number on module %s", 
	//	moduleNameString);
	//CG_LOG(infoStr, INFO_LEVEL)

 //   HRESULT hr = S_OK;

	//if(_resultsDrive=="")
	//	setResultsDrive();
	//if(diskFull(_resultsDrive))
	//{
	//	hr = CG_ERROR_RESULTS_DISK_FULL;
	//}

 //   if( SUCCEEDED(hr) )
 //   {
	//    if(_logFileDrive=="")
	//	    setLogFileDrive();
	//    if(diskFull(_logFileDrive))
	//    {
	//	    hr = CG_ERROR_LOGFILE_DISK_FULL;
	//    }
 //   }

 //   if( SUCCEEDED(hr) )
 //   {
 //   	CG_LOG("CGResearchCtrl::getCurrentSequenceNumber() : Entered\n", DIAGNOSTIC_LEVEL)

	//    // we can only perform this operation
	//    // if the system is ready and a laser
	//    // is installed
	//    if(!validFunctionCall("getCurrentSequenceNumber"))
	//    {
	//	    hr = CG_ERROR_INVALID_STATE;
	//    }
	//    else
	//    {

	//		hr = (HRESULT)(CLoseGridSystem::instance()->getCurrentSequenceNumber(moduleNameString, *currentSequenceNumber));
	//    }

	//    CG_LOG("CGResearchCtrl::getCurrentSequenceNumber( ) : Exiting", DIAGNOSTIC_LEVEL)
 //   }

 //   if(FAILED(hr)) AfxThrowOleException(hr);
    return;
}

void CCGResearchCtrl::readVoltage( BSTR moduleName, double* voltage )
{
 //   HRESULT hr = S_OK;

	//if(_resultsDrive=="")
	//	setResultsDrive();
	//if(diskFull(_resultsDrive))
	//{
	//	hr = CG_ERROR_RESULTS_DISK_FULL;
	//}

 //   if( SUCCEEDED(hr) )
 //   {
	//    if(_logFileDrive=="")
	//	    setLogFileDrive();
	//    if(diskFull(_logFileDrive))
	//    {
	//	    hr = CG_ERROR_LOGFILE_DISK_FULL;
	//    }
 //   }

 //   if( SUCCEEDED(hr) )
 //   {
	//    CG_LOG("CCGResearchCtrl::readVoltage() : Entering", DIAGNOSTIC_LEVEL)

	//	CString moduleNameStr(moduleName);
 //   	
	//    if(!validFunctionCall("readVoltage"))
	//    {
	//	    hr = CG_ERROR_INVALID_STATE;
	//    }
	//    else
	//    {
	//	    hr = CLoseGridSystem::instance()->readVoltage(moduleNameStr, *voltage);
	//    }

	//    char infoStr[BUF_SIZE];
	//    memset(infoStr, '\0', sizeof(infoStr));
	//    sprintf(infoStr, 
 //           "Device %s: Voltage read returned %g",
	//	    moduleNameStr, voltage);
	//    CG_LOG(infoStr, INFO_LEVEL)
 //   }


	//CG_LOG("CCGResearchCtrl::readVoltage() : Exiting", DIAGNOSTIC_LEVEL)

 //   if(FAILED(hr)) AfxThrowOleException(hr);
    return;
}

void CCGResearchCtrl::getState( BSTR* current_state )
{
 //   HRESULT hr = S_OK;

	//if(_resultsDrive=="")
	//	setResultsDrive();
	//if(diskFull(_resultsDrive))
	//{
	//	hr = CG_ERROR_RESULTS_DISK_FULL;
	//}

 //   if(SUCCEEDED(hr))
 //   {
	//    if(_logFileDrive=="")
	//	    setLogFileDrive();
	//    if(diskFull(_logFileDrive))
	//    {
	//	    hr = CG_ERROR_LOGFILE_DISK_FULL;
	//    }
 //   }

 //   if(SUCCEEDED(hr))
 //   {
	//    if(!validFunctionCall("getState"))
	//    {
	//	    hr = CG_ERROR_INVALID_STATE;
	//    }
	//    else
	//    {
	//        try
	//        {
	//	        // get state
	//	        CString stateString = CLoseGridSystem::instance()->stateAsString(
 //                   CLoseGridSystem::instance()->getSystemState());
	//	        *current_state = stateString.AllocSysString();


	//	        char infoStr[BUF_SIZE];
	//	        memset(infoStr, '\0', sizeof(infoStr));
	//	        sprintf(infoStr, 
	//		        "getState returned state=%s", 
	//		        stateString);
	//	        CG_LOG(infoStr, INFO_LEVEL)

	//        }
	//        catch(...)
	//        {
	//	        CG_LOG("CCGResearchCtrl::getState() : Exception thrown", CRITICAL_LEVEL)
	//	        hr = CG_ERROR_SYSTEM_ERROR;
	//        }
 //       }
 //   }

 //   if(FAILED(hr)) AfxThrowOleException(hr);
    return;
}

void 
CCGResearchCtrl::
livtest(short powermeter_slot_number,
    short powermeter_channel_number,
    short currentsource_slot_number,
    double start_current,
    double stop_current,
    long number_of_steps,
    long step_duration,
    VARIANT* current_array,
    VARIANT* voltage_array,
    VARIANT* power_array,
    VARIANT* dPdI_array,
    VARIANT* d2PdI2_array,
    BSTR* results_dir )
{
 //   HRESULT hr = S_OK;

	//if(_resultsDrive=="")
	//	setResultsDrive();
	//if(diskFull(_resultsDrive))
	//{
	//	hr = CG_ERROR_RESULTS_DISK_FULL;
	//}

 //   if(SUCCEEDED(hr))
 //   {
	//    if(_logFileDrive=="")
	//	    setLogFileDrive();
	//    if(diskFull(_logFileDrive))
	//    {
	//	    hr = CG_ERROR_LOGFILE_DISK_FULL;
	//    }
 //   }

	//vector<double>	currentArray;
	//vector<double>	voltageArray;
	//vector<double>	powerArray;
	//vector<double>	dPdIArray;
	//vector<double>	dPdI2Array;

	//CString resultsDir;

	//int		slotNumberPowerMeter	= (int)powermeter_slot_number;
	//int		channelNumberPowerMeter	= (int)powermeter_channel_number;
	//int		slotNumberCurrentSource = (int)currentsource_slot_number;
	//double	minCurrent				= (double)start_current;
	//double	maxCurrent				= (double)stop_current;
	//int		numSteps				= (int)number_of_steps;
	//int		stepDuration			= (int)step_duration;

 //   if(SUCCEEDED(hr))
 //   {
	//    if(!validFunctionCall("livtest"))
	//    {
	//	    hr = CG_ERROR_INVALID_STATE;
	//    }
	//    else
	//    {

	//		///////////////////////////////////////////////////////////////////////////////////////
	//		///
	//		//	

	//		hr = CLoseGridSystem::instance()->livTest(slotNumberPowerMeter,
	//												channelNumberPowerMeter,
	//												slotNumberCurrentSource,
	//												minCurrent,
	//												maxCurrent,
	//												numSteps,
	//												stepDuration,
	//												currentArray,
	//												powerArray,
	//												voltageArray,
	//												dPdIArray,
	//												dPdI2Array,
	//												resultsDir);
	//		//
	//		///////////////////////////////////////////////////////////////////////////////////////

	//	}
	//}

	//if(SUCCEEDED(hr))
	//{
	//	convertVectorToVariant(currentArray, *current_array);
	//	convertVectorToVariant(voltageArray, *voltage_array);
	//	convertVectorToVariant(powerArray, *power_array);
	//	convertVectorToVariant(dPdIArray, *dPdI_array);
	//	convertVectorToVariant(dPdI2Array, *d2PdI2_array);

	//	//
	//	///
	//	///////////////////////////////////////////////////////////////////////////////////////////////
	//	///
	//	//


	//	*results_dir = resultsDir.AllocSysString(); 

 //   }

 //   if(FAILED(hr)) AfxThrowOleException(hr);
    return;
}


void CCGResearchCtrl::setup( BSTR laser_id )
{
 //   HRESULT hr = S_OK;

	//if(_resultsDrive=="")
	//	setResultsDrive();
	//if(diskFull(_resultsDrive))
	//{
	//	hr = CG_ERROR_RESULTS_DISK_FULL;
	//}

 //   if(SUCCEEDED(hr))
 //   {
	//    if(_logFileDrive=="")
	//	    setLogFileDrive();
	//    if(diskFull(_logFileDrive))
	//    {
	//	    hr = CG_ERROR_LOGFILE_DISK_FULL;
	//    }
 //   }

 //   if(SUCCEEDED(hr))
 //   {
	//    CG_LOG("CCGResearchCtrl::setup() : Entered", DIAGNOSTIC_LEVEL)

	//    char infoStr[BUF_SIZE];
	//    memset(infoStr, '\0', sizeof(infoStr));

	//    if(!validFunctionCall("setup"))
	//    {
	//	    memset(infoStr, '\0', sizeof(infoStr));
	//	    sprintf(infoStr, "CCGResearchCtrl::setup() : Setup failed as system is not in a valid state. Current state = %s",
 //               CLoseGridSystem::instance()->stateAsString(CLoseGridSystem::instance()->getSystemState()));
	//	    CG_LOG(infoStr, ERROR_LEVEL)

	//	    hr = CG_ERROR_INVALID_STATE;
	//    }
	//    else
	//    {
	//	    // convert the BSTR to a CString 
	//	    CString laserId(laser_id);

	//	    sprintf(infoStr, "Attempting to setup laser. Laser ID = %s....", laserId);
	//	    CG_LOG(infoStr, INFO_LEVEL)

	//	    // in order to setup laser CloseGrid must just have been instantiated i.e Startup
	//	    // or it can also have been setup - i.e if a screening was just cancelled

	//	    if(!validLaserId(laserId))
	//	    {
	//		    hr = CG_ERROR_INVALID_LASER_ID;

	//		    memset(infoStr, '\0', sizeof(infoStr));
	//		    sprintf(infoStr, "CCloseGrid::setup() : Setup failed due to invalid Laser ID. Input Laser ID = %s", laserId);
	//		    CG_LOG(infoStr, ERROR_LEVEL)

	//		    // set back to startup state
 //               CLoseGridSystem::instance()->setSystemState(CLoseGridSystem::State::Startup);
	//	    }
	//	    else
	//	    {
	//		    // now call setup - this will take all parameters for screening and will do a small  amount of laser checking 
	//		    // i.e. are the bond wires all connected
	//		    hr = CLoseGridSystem::instance()->setup(laserId);

	//		    if(SUCCEEDED(hr))
	//		    {
	//			    memset(infoStr, '\0', sizeof(infoStr));
	//			    sprintf(infoStr, "CCloseGrid::setup() : Laser %s setup successfully", laserId);
	//			    CG_LOG(infoStr, INFO_LEVEL)

	//			    // set to setup state
 //                   CLoseGridSystem::instance()->setSystemState(CLoseGridSystem::State::Setup);
	//		    }
	//		    else
	//		    {
	//			    memset(infoStr, '\0', sizeof(infoStr));
	//			    sprintf(infoStr, "CCGResearchCtrl::setup() : Laser %s setup failed", laserId);
	//			    CG_LOG(infoStr, ERROR_LEVEL)

	//			    // set back to startup state
	//			    CLoseGridSystem::instance()->setSystemState(CLoseGridSystem::State::Startup);
	//		    }
	//	    }
	//    }
 //   }

	//// trace
	//CG_LOG("CCGResearchCtrl::setup() : Exiting", DIAGNOSTIC_LEVEL)

 //   if(FAILED(hr)) AfxThrowOleException(hr);
    return;
}

void CCGResearchCtrl::rampdown()
{
	//CG_LOG("Ramping currents down to zero.....", INFO_LEVEL)

 //   HRESULT hr = S_OK;

	//if(_resultsDrive=="")
	//	setResultsDrive();
	//if(diskFull(_resultsDrive))
	//{
	//	hr = CG_ERROR_RESULTS_DISK_FULL;
	//}

 //   if(SUCCEEDED(hr))
 //   {
	//    if(_logFileDrive=="")
	//	    setLogFileDrive();
	//    if(diskFull(_logFileDrive))
	//    {
	//	    hr = CG_ERROR_LOGFILE_DISK_FULL;
	//    }
 //   }


 //   if(SUCCEEDED(hr))
 //   {
	//    CG_LOG("CCGResearchCtrl::rampDown() : Entering", DIAGNOSTIC_LEVEL)

	//    // we can only perform this operation
	//    // if the system is ready and a laser
	//    // is installed
	//    if(!validFunctionCall("rampDown"))
	//    {
	//	    hr = CG_ERROR_INVALID_STATE;
	//    }
	//    else
	//    {
	//	    hr = CLoseGridSystem::instance()->rampDown();
	//    }

	//    CG_LOG("CCGResearchCtrl::rampDown() : Exiting\n", DIAGNOSTIC_LEVEL)
 //   }

 //   if(FAILED(hr)) AfxThrowOleException(hr);
    return;
}

void CCGResearchCtrl::readPower(
    short slot_number,
    short channel_number,
    double* optical_power )
{
 ///*   HRESULT hr = S_OK;

	//if(_resultsDrive=="")
	//	setResultsDrive();
	//if(diskFull(_resultsDrive))
	//{
	//	hr = CG_ERROR_RESULTS_DISK_FULL;
	//}

 //   if(SUCCEEDED(hr))
 //   {
	//    if(_logFileDrive=="")
	//	    setLogFileDrive();
	//    if(diskFull(_logFileDrive))
	//    {
	//	    hr = CG_ERROR_LOGFILE_DISK_FULL;
	//    }
 //   }

 //   if(SUCCEEDED(hr))
 //   {
	//    CG_LOG("CCGResearchCtrl::readPower() : Entered", DIAGNOSTIC_LEVEL)

	//    if(!validFunctionCall("readPower"))
	//    {
	//	    hr = CG_ERROR_INVALID_STATE;
	//    }
	//    else
	//    {
	//	    hr = CLoseGridSystem::instance()->getPower((int)slot_number,
	//													    (int)channel_number,
	//													    *optical_power);
	//    }

	//    char infoStr[BUF_SIZE];
	//    memset(infoStr, '\0', sizeof(infoStr));
	//    sprintf(infoStr, "Slot %d, channel %d, power read returned %g",
 //           slot_number, channel_number, *optical_power);
	//    CG_LOG(infoStr, INFO_LEVEL)

	//    CG_LOG("CCGResearchCtrl::readPower() : Exiting", DIAGNOSTIC_LEVEL)
 //   }

 //   if(FAILED(hr)) AfxThrowOleException(hr);*/
    return;
}

void CCGResearchCtrl::readOpticalPower(
    BSTR	moduleName,
    short	channel,
    double* opticalPower )
{
 //   HRESULT hr = S_OK;

	//if(_resultsDrive=="")
	//	setResultsDrive();
	//if(diskFull(_resultsDrive))
	//{
	//	hr = CG_ERROR_RESULTS_DISK_FULL;
	//}

 //   if(SUCCEEDED(hr))
 //   {
	//    if(_logFileDrive=="")
	//	    setLogFileDrive();
	//    if(diskFull(_logFileDrive))
	//    {
	//	    hr = CG_ERROR_LOGFILE_DISK_FULL;
	//    }
 //   }

	//CString moduleNameStr(moduleName); 

 //   if(SUCCEEDED(hr))
 //   {
	//    CG_LOG("CCGResearchCtrl::readOpticalPower() : Entered", DIAGNOSTIC_LEVEL)

	//    if(!validFunctionCall("readOpticalPower"))
	//    {
	//	    hr = CG_ERROR_INVALID_STATE;
	//    }
	//    else
	//    {

	//	    hr = CLoseGridSystem::instance()->readOpticalPower(moduleNameStr,
	//														channel,
	//													    *opticalPower);
	//    }

	//    char infoStr[BUF_SIZE];
	//    memset(infoStr, '\0', sizeof(infoStr));
	//    sprintf(infoStr, "Module %s, channel %d, optical power read returned %g",
 //           moduleNameStr, channel, *opticalPower);
	//    CG_LOG(infoStr, INFO_LEVEL)

	//    CG_LOG("CCGResearchCtrl::readOpticalPower() : Exiting", DIAGNOSTIC_LEVEL)
 //   }

 //   if(FAILED(hr)) AfxThrowOleException(hr);
    return;
}

void CCGResearchCtrl::readOpticalPowerAtFreq(
    BSTR	moduleName,
    short	channel,
	double	frequency,
    double* opticalPower )
{
 //   HRESULT hr = S_OK;

	//if(_resultsDrive=="")
	//	setResultsDrive();
	//if(diskFull(_resultsDrive))
	//{
	//	hr = CG_ERROR_RESULTS_DISK_FULL;
	//}

 //   if(SUCCEEDED(hr))
 //   {
	//    if(_logFileDrive=="")
	//	    setLogFileDrive();
	//    if(diskFull(_logFileDrive))
	//    {
	//	    hr = CG_ERROR_LOGFILE_DISK_FULL;
	//    }
 //   }

	//CString moduleNameStr(moduleName); 

 //   if(SUCCEEDED(hr))
 //   {
	//    CG_LOG("CCGResearchCtrl::readOpticalPowerAtFreq() : Entered", DIAGNOSTIC_LEVEL)

	//    if(!validFunctionCall("readOpticalPowerAtFreq"))
	//    {
	//	    hr = CG_ERROR_INVALID_STATE;
	//    }
	//    else
	//    {

	//	    hr = CLoseGridSystem::instance()->readOpticalPowerAtFreq(moduleNameStr,
	//																channel,
	//																frequency,
	//																*opticalPower);
	//    }

	//    char infoStr[BUF_SIZE];
	//    memset(infoStr, '\0', sizeof(infoStr));
	//    sprintf(infoStr, "Module %s, channel %d, optical power read at frequency %f returned %g",
 //           moduleNameStr, channel, frequency, *opticalPower);
	//    CG_LOG(infoStr, INFO_LEVEL)

	//    CG_LOG("CCGResearchCtrl::readOpticalPowerAtFreq() : Exiting", DIAGNOSTIC_LEVEL)
 //   }

 //   if(FAILED(hr)) AfxThrowOleException(hr);
    return;
}

void 
CCGResearchCtrl::
readPhotodiodeCurrent(
    BSTR	moduleName,
    short	channel,
    double* current )
{
 ///*   HRESULT hr = S_OK;

	//if(_resultsDrive=="")
	//	setResultsDrive();
	//if(diskFull(_resultsDrive))
	//{
	//	hr = CG_ERROR_RESULTS_DISK_FULL;
	//}

 //   if(SUCCEEDED(hr))
 //   {
	//    if(_logFileDrive=="")
	//	    setLogFileDrive();
	//    if(diskFull(_logFileDrive))
	//    {
	//	    hr = CG_ERROR_LOGFILE_DISK_FULL;
	//    }
 //   }

	//CString moduleNameStr(moduleName); 

 //   if(SUCCEEDED(hr))
 //   {
	//    CG_LOG("CCGResearchCtrl::readPhotodiodeCurrent() : Entered", DIAGNOSTIC_LEVEL)

	//    if(!validFunctionCall("readPhotodiodeCurrent"))
	//    {
	//	    hr = CG_ERROR_INVALID_STATE;
	//    }
	//    else
	//    {

	//	    hr = CLoseGridSystem::instance()->readPhotodiodeCurrent(moduleNameStr,
	//																channel,
	//																*current);
	//    }

	//    char infoStr[BUF_SIZE];
	//    memset(infoStr, '\0', sizeof(infoStr));
	//    sprintf(infoStr, "Module %s, channel %d, photdiode current read returned %g",
 //           moduleNameStr, channel, *current);
	//    CG_LOG(infoStr, INFO_LEVEL)

	//    CG_LOG("CCGResearchCtrl::readPhotodiodeCurrent() : Exiting", DIAGNOSTIC_LEVEL)
 //   }

 //   if(FAILED(hr)) AfxThrowOleException(hr);*/
    return;
}

void 
CCGResearchCtrl::
readCoarseFrequency(double* frequency)
{
 ///*   HRESULT hr = S_OK;

	//if(_resultsDrive=="")
	//	setResultsDrive();
	//if(diskFull(_resultsDrive))
	//{
	//	hr = CG_ERROR_RESULTS_DISK_FULL;
	//}

 //   if(SUCCEEDED(hr))
 //   {
	//    if(_logFileDrive=="")
	//	    setLogFileDrive();
	//    if(diskFull(_logFileDrive))
	//    {
	//	    hr = CG_ERROR_LOGFILE_DISK_FULL;
	//    }
 //   }

 //   if(SUCCEEDED(hr))
 //   {
	//    CG_LOG("CCGResearchCtrl::readCoarseFrequency() : Entered", DIAGNOSTIC_LEVEL)

	//    if(!validFunctionCall("readCoarseFrequency"))
	//    {
	//	    hr = CG_ERROR_INVALID_STATE;
	//    }
	//    else
	//    {
	//	    hr = CLoseGridSystem::instance()->readCoarseFrequency(*frequency);
	//    }

	//    char infoStr[BUF_SIZE];
	//    memset(infoStr, '\0', sizeof(infoStr));
	//    sprintf(infoStr, "Coarse frequency read returned %g", *frequency);
	//    CG_LOG(infoStr, INFO_LEVEL)

	//    CG_LOG("CCGResearchCtrl::readCoarseFrequency() : Exiting", DIAGNOSTIC_LEVEL)
 //   }

 //   if(FAILED(hr)) AfxThrowOleException(hr);*/
    return;
}

void CCGResearchCtrl::readFrequency( double* frequency )
{
 //   HRESULT hr = S_OK;

	//if(_resultsDrive=="")
	//	setResultsDrive();
	//if(diskFull(_resultsDrive))
	//{
	//	hr = CG_ERROR_RESULTS_DISK_FULL;
	//}

 //   if(SUCCEEDED(hr))
 //   {
	//    if(_logFileDrive=="")
	//	    setLogFileDrive();
	//    if(diskFull(_logFileDrive))
	//    {
	//	    hr = CG_ERROR_LOGFILE_DISK_FULL;
	//    }
 //   }

 //   if(SUCCEEDED(hr))
 //   {
	//    CG_LOG("CCGResearchCtrl::readFrequency() : Entered", DIAGNOSTIC_LEVEL)

	//    if(!validFunctionCall("readFrequency"))
	//    {
	//	    hr = CG_ERROR_INVALID_STATE;
	//    }
	//    else
	//    {
	//	    hr = CLoseGridSystem::instance()->getFrequency(*frequency);
	//    }

	//    char infoStr[BUF_SIZE];
	//    memset(infoStr, '\0', sizeof(infoStr));
	//    sprintf(infoStr, "Frequency read = %g", *frequency);
	//    CG_LOG(infoStr, INFO_LEVEL)

	//    CG_LOG("CCGResearchCtrl::readFrequency() : Exiting", DIAGNOSTIC_LEVEL)
 //   }

 //   if(FAILED(hr)) AfxThrowOleException(hr);
    return;
}

// readTemperature contains no debug as it is
// called from a continuous thread which would
// fill the log file
void CCGResearchCtrl::readTemperature( double* temperature )
{
 //   HRESULT hr = S_OK;

	//if(_resultsDrive=="")
	//	setResultsDrive();
	//if(diskFull(_resultsDrive))
	//{
	//	hr = CG_ERROR_RESULTS_DISK_FULL;
	//}

 //   if(SUCCEEDED(hr))
 //   {
	//    if(_logFileDrive=="")
	//	    setLogFileDrive();
	//    if(diskFull(_logFileDrive))
	//    {
	//	    hr = CG_ERROR_LOGFILE_DISK_FULL;
	//    }
 //   }

 //   if(SUCCEEDED(hr))
 //   {

	//    if(!validFunctionCall("readTemperature"))
	//    {
	//		*temperature = 0;
	//	    hr = CG_ERROR_INVALID_STATE;
	//    }
	//    else
	//    {
	//	    float temp = 0;
	//	    hr = CLoseGridSystem::instance()->readTemperature(temp);
	//	    *temperature = (double)temp;
	//    }

 //   }

 //   if(FAILED(hr)) AfxThrowOleException(hr);
    return;
}

void CCGResearchCtrl::setTECSetpoint( double temp_setpoint )
{
 ///*   HRESULT hr = S_OK;

	//if(_resultsDrive=="")
	//	setResultsDrive();
	//if(diskFull(_resultsDrive))
	//{
	//	hr = CG_ERROR_RESULTS_DISK_FULL;
	//}

 //   if(SUCCEEDED(hr))
 //   {
	//    if(_logFileDrive=="")
	//	    setLogFileDrive();
	//    if(diskFull(_logFileDrive))
	//    {
	//	    hr = CG_ERROR_LOGFILE_DISK_FULL;
	//    }
 //   }

 //   if(SUCCEEDED(hr))
 //   {
	//    CG_LOG("CCGResearchCtrl::setTECSetpoint() : Entering", DIAGNOSTIC_LEVEL)

	//    if(!validFunctionCall("setTECSetpoint"))
	//    {
	//	    hr = CG_ERROR_INVALID_STATE;
	//    }
	//    else
	//    {
	//	    float temp = 0;
	//	    temp = (float)temp_setpoint;
	//	    hr = CLoseGridSystem::instance()->setTECSetpoint(temp);
	//    }

	//    char infoStr[BUF_SIZE];
	//    memset(infoStr, '\0', sizeof(infoStr));
	//    sprintf(infoStr, "Temperature setpoint applied %g", temp_setpoint);
	//    CG_LOG(infoStr, INFO_LEVEL)

	//    CG_LOG("CCGResearchCtrl::setTECSetpoint() : Exiting", DIAGNOSTIC_LEVEL)
 //   }

 //   if(FAILED(hr)) AfxThrowOleException(hr);*/
    return;
}

void CCGResearchCtrl::getTECSetpoint( double* temp_setpoint )
{
 ///*   HRESULT hr = S_OK;

	//if(_resultsDrive=="")
	//	setResultsDrive();
	//if(diskFull(_resultsDrive))
	//{
	//	hr = CG_ERROR_RESULTS_DISK_FULL;
	//}

 //   if(SUCCEEDED(hr))
 //   {
	//    if(_logFileDrive=="")
	//	    setLogFileDrive();
	//    if(diskFull(_logFileDrive))
	//    {
	//	    hr = CG_ERROR_LOGFILE_DISK_FULL;
	//    }
 //   }

 //   if(SUCCEEDED(hr))
 //   {
	//    CG_LOG("CCGResearchCtrl::getTECSetpoint() : Entering", DIAGNOSTIC_LEVEL)

	//    float temp = 0;
	//    if(!validFunctionCall("getTECSetpoint"))
	//    {
	//	    hr = CG_ERROR_INVALID_STATE;
	//    }
	//    else
	//    {
	//	    hr = CLoseGridSystem::instance()->getTECSetpoint(temp);
	//	    *temp_setpoint = (double)temp;
	//    }

	//    char infoStr[BUF_SIZE];
	//    memset(infoStr, '\0', sizeof(infoStr));
	//    sprintf(infoStr, "Temperature setpoint retrieved %.2f", temp);
	//    CG_LOG(infoStr, INFO_LEVEL)

	//    CG_LOG("CCGResearchCtrl::getTECSetpoint() : Exiting", DIAGNOSTIC_LEVEL)
 //   }

 //   if(FAILED(hr)) AfxThrowOleException(hr);*/
    return;
}

void CCGResearchCtrl::get2DData(
    short currentsource_slot_number,
    double min_current,
    double max_current,
    long number_of_steps,
    long step_duration,
    VARIANT_BOOL frequencyUnits,
    BSTR* results_dir )
{
	//HRESULT hr = S_OK;

	//if(_resultsDrive=="")
	//	setResultsDrive();
	//if(diskFull(_resultsDrive))
	//{
	//	hr =  CG_ERROR_RESULTS_DISK_FULL;
	//}

	//if(SUCCEEDED(hr))
	//{
	//	if(_logFileDrive=="")
	//		setLogFileDrive();
	//	if(diskFull(_logFileDrive))
	//	{
	//		hr = CG_ERROR_LOGFILE_DISK_FULL;
	//	}
	//}

	//if(SUCCEEDED(hr))
	//{
	//    CG_LOG("CCGResearchCtrl::get2DData() : Entering", DIAGNOSTIC_LEVEL)
	//
	//	CString results_dir_CString;

	//	if(!validFunctionCall("get2DData"))
	//	{
	//		hr = CG_ERROR_INVALID_STATE;
	//	}
	//	else
	//	{

	//		hr = CLoseGridSystem::instance()->get2DData((int)currentsource_slot_number,
	//													min_current,
	//													max_current,
	//													(int)number_of_steps,
	//													(int)step_duration,
	//													frequencyUnits>0,
	//													results_dir_CString);

	//		*results_dir = results_dir_CString.AllocSysString();
	//	}

	//	char infoStr[BUF_SIZE];
	//	memset(infoStr, '\0', sizeof(infoStr));
	//	sprintf(infoStr, "get2DData returned results dir %s", results_dir_CString);
	//	CG_LOG(infoStr, INFO_LEVEL)

	//	CG_LOG("CCGResearchCtrl::get2DData() : Exiting", DIAGNOSTIC_LEVEL)
	//}

 //   if(FAILED(hr)) AfxThrowOleException(hr);
    return;
}

void 
CCGResearchCtrl::
get306SequenceMeasurements( BSTR			moduleName,
							VARIANT*		channel1Measurements,
							VARIANT*		channel2Measurements)
{
	//HRESULT hr = S_OK;

	//if(_resultsDrive=="")
	//	setResultsDrive();
	//if(diskFull(_resultsDrive))
	//{
	//	hr =  CG_ERROR_RESULTS_DISK_FULL;
	//}

	//if(SUCCEEDED(hr))
	//{
	//	if(_logFileDrive=="")
	//		setLogFileDrive();
	//	if(diskFull(_logFileDrive))
	//	{
	//		hr = CG_ERROR_LOGFILE_DISK_FULL;
	//	}
	//}

	//if(SUCCEEDED(hr))
	//{
	//    CG_LOG("CCGResearchCtrl::get306SequenceMeasurements() : Entering", DIAGNOSTIC_LEVEL)
	//
	//	if(!validFunctionCall("get306SequenceMeasurements"))
	//	{
	//		hr = CG_ERROR_INVALID_STATE;
	//	}
	//	else
	//	{

	//		CString moduleNameString(moduleName);
	//		vector<double> vectChannel1Measurements;
	//		vector<double> vectChannel2Measurements;


	//		hr = CLoseGridSystem::instance()->get306SequenceMeasurements(moduleNameString,
	//																vectChannel1Measurements,
	//																vectChannel2Measurements);

	//		convertVectorToVariant(vectChannel1Measurements, *channel1Measurements);
	//		convertVectorToVariant(vectChannel2Measurements, *channel2Measurements);
	//	}


	//	CG_LOG("CCGResearchCtrl::get306SequenceMeasurements() : Exiting", DIAGNOSTIC_LEVEL)
	//}

 //   if(FAILED(hr)) AfxThrowOleException(hr);
    return;
}

void CCGResearchCtrl::get2DData2(
    BSTR			moduleName,
	VARIANT*		currents,
    long			stepDuration,
    VARIANT_BOOL	frequencyUnits,
    BSTR*			results_dir)
{
	//HRESULT hr = S_OK;

	//if(_resultsDrive=="")
	//	setResultsDrive();
	//if(diskFull(_resultsDrive))
	//{
	//	hr =  CG_ERROR_RESULTS_DISK_FULL;
	//}

	//if(SUCCEEDED(hr))
	//{
	//	if(_logFileDrive=="")
	//		setLogFileDrive();
	//	if(diskFull(_logFileDrive))
	//	{
	//		hr = CG_ERROR_LOGFILE_DISK_FULL;
	//	}
	//}

	//if(SUCCEEDED(hr))
	//{
	//    CG_LOG("CCGResearchCtrl::get2DData2() : Entering", DIAGNOSTIC_LEVEL)
	//
	//	CString results_dir_CString;

	//	if(!validFunctionCall("get2DData2"))
	//	{
	//		hr = CG_ERROR_INVALID_STATE;
	//	}
	//	else
	//	{

	//		CString moduleNameString(moduleName);
	//		vector<double> currentsVector;

	//		currentsVector = convertVariantToVector(*currents);

	//		hr = CLoseGridSystem::instance()->get2DData(moduleNameString,
	//													currentsVector,
	//													(short)stepDuration,
	//													frequencyUnits>0,
	//													results_dir_CString);

	//		*results_dir = results_dir_CString.AllocSysString();
	//	}

	//	char infoStr[BUF_SIZE];
	//	memset(infoStr, '\0', sizeof(infoStr));
	//	sprintf(infoStr, "get2DData2 returned results dir %s", results_dir_CString);
	//	CG_LOG(infoStr, INFO_LEVEL)

	//	CG_LOG("CCGResearchCtrl::get2DData() : Exiting", DIAGNOSTIC_LEVEL)
	//}

 //   if(FAILED(hr)) AfxThrowOleException(hr);
    return;
}

void CCGResearchCtrl::get3DData(
    short Ix_slot_number,
    double Ix_min_current,
    double Ix_max_current,
    long Ix_number_of_steps,
    long Ix_step_duration,
    short Iy_slot_number,
    double Iy_min_current,
    double Iy_max_current,
    long Iy_number_of_steps,
    VARIANT_BOOL hysteresis,
    VARIANT_BOOL frequencyUnits,
    BSTR* results_dir )
{
	//HRESULT hr = S_OK;

	//if(_resultsDrive=="")
	//	setResultsDrive();
	//if(diskFull(_resultsDrive))
	//{
	//	hr =  CG_ERROR_RESULTS_DISK_FULL;
	//}

	//if(SUCCEEDED(hr))
	//{
	//	if(_logFileDrive=="")
	//		setLogFileDrive();
	//	if(diskFull(_logFileDrive))
	//	{
	//		hr = CG_ERROR_LOGFILE_DISK_FULL;
	//	}
	//}

	//if(SUCCEEDED(hr))
	//{
	//    CG_LOG("CCGResearchCtrl::get3DData() : Entering", DIAGNOSTIC_LEVEL)
	//
	//	CString results_dir_CString;

	//	if(!validFunctionCall("get3DData"))
	//	{
	//		hr = CG_ERROR_INVALID_STATE;
	//	}
	//	else
	//	{

	//		hr = CLoseGridSystem::instance()->get3DData(
 //               (int)Ix_slot_number,
	//			Ix_min_current,
	//			Ix_max_current,
	//			(int)Ix_number_of_steps,
	//			(int)Iy_slot_number,
	//			Iy_min_current,
	//			Iy_max_current,
	//			(int)Iy_number_of_steps,
	//			(int)Ix_step_duration,
	//			hysteresis>0,
	//			frequencyUnits>0,
	//			results_dir_CString);

	//		*results_dir = results_dir_CString.AllocSysString();
	//	}

	//	char infoStr[BUF_SIZE];
	//	memset(infoStr, '\0', sizeof(infoStr));
	//	sprintf(infoStr, "get3DData returned results dir %s", results_dir_CString);
	//	CG_LOG(infoStr, INFO_LEVEL)
	//	CG_LOG("CCGResearchCtrl::get3DData() : Exiting", DIAGNOSTIC_LEVEL)
	//}

 //   if(FAILED(hr)) AfxThrowOleException(hr);
    return;
}

void CCGResearchCtrl::get3DData2(
    BSTR			moduleNameX,
	VARIANT*		currentsX,
    BSTR			moduleNameY,
	VARIANT*		currentsY,
    long			stepDuration,
    VARIANT_BOOL	hysteresis,
    VARIANT_BOOL	frequencyUnits,
    BSTR*			results_dir)
{
	//HRESULT hr = S_OK;

	//if(_resultsDrive=="")
	//	setResultsDrive();
	//if(diskFull(_resultsDrive))
	//{
	//	hr =  CG_ERROR_RESULTS_DISK_FULL;
	//}

	//if(SUCCEEDED(hr))
	//{
	//	if(_logFileDrive=="")
	//		setLogFileDrive();
	//	if(diskFull(_logFileDrive))
	//	{
	//		hr = CG_ERROR_LOGFILE_DISK_FULL;
	//	}
	//}

	//if(SUCCEEDED(hr))
	//{
	//    CG_LOG("CCGResearchCtrl::get3DData2() : Entering", DIAGNOSTIC_LEVEL)
	//
	//	CString results_dir_CString;

	//	if(!validFunctionCall("get3DData2"))
	//	{
	//		hr = CG_ERROR_INVALID_STATE;
	//	}
	//	else
	//	{

	//		CString moduleNameXStr(moduleNameX); 
	//		CString moduleNameYStr(moduleNameY); 

	//		vector<double> currentsXVector;
	//		vector<double> currentsYVector;

	//		currentsXVector = convertVariantToVector(*currentsX);
	//		currentsYVector = convertVariantToVector(*currentsY);


	//		hr = CLoseGridSystem::instance()->get3DData(
 //               moduleNameXStr,
	//			currentsXVector,
	//			moduleNameYStr,
	//			currentsYVector,
	//			(int)stepDuration,
	//			hysteresis>0,
	//			frequencyUnits>0,
	//			results_dir_CString);

	//		*results_dir = results_dir_CString.AllocSysString();
	//	}

	//	char infoStr[BUF_SIZE];
	//	memset(infoStr, '\0', sizeof(infoStr));
	//	sprintf(infoStr, "get3DData2 returned results dir %s", results_dir_CString);
	//	CG_LOG(infoStr, INFO_LEVEL)
	//	CG_LOG("CCGResearchCtrl::get3DData() : Exiting", DIAGNOSTIC_LEVEL)
	//}

 //   if(FAILED(hr)) AfxThrowOleException(hr);
    return;
}

void 
CCGResearchCtrl::
load305Sequence(
    BSTR			moduleName,
	VARIANT*		currents,
	short			sourceDelay,
	short			measureDelay)
{
	//HRESULT hr = S_OK;

	//if(_resultsDrive=="")
	//	setResultsDrive();
	//if(diskFull(_resultsDrive))
	//{
	//	hr =  CG_ERROR_RESULTS_DISK_FULL;
	//}

	//if(SUCCEEDED(hr))
	//{
	//	if(_logFileDrive=="")
	//		setLogFileDrive();
	//	if(diskFull(_logFileDrive))
	//	{
	//		hr = CG_ERROR_LOGFILE_DISK_FULL;
	//	}
	//}

	//if(SUCCEEDED(hr))
	//{
	//    CG_LOG("CCGResearchCtrl::load305Sequence() : Entering", DIAGNOSTIC_LEVEL)
	//
	//	CString results_dir_CString;

	//	if(!validFunctionCall("load305Sequence"))
	//	{
	//		hr = CG_ERROR_INVALID_STATE;
	//	}
	//	else
	//	{

	//		CString moduleNameStr(moduleName); 

	//		vector<double> currentsVector;
	//		currentsVector = convertVariantToVector(*currents);


	//		hr = CLoseGridSystem::instance()->load305Sequence(moduleNameStr,
	//														currentsVector,
	//														(int)sourceDelay,
	//														(int)measureDelay);
	//	}

	//	CG_LOG("CCGResearchCtrl::load305Sequence() : Exiting", DIAGNOSTIC_LEVEL)
	//}

 //   if(FAILED(hr)) AfxThrowOleException(hr);
    return;
}

void 
CCGResearchCtrl::
load306Sequence(
    BSTR			moduleName,
	short			numPoints,
	short			channel,
	short			sourceDelay,
	short			measureDelay)
{
	//HRESULT hr = S_OK;

	//if(_resultsDrive=="")
	//	setResultsDrive();
	//if(diskFull(_resultsDrive))
	//{
	//	hr =  CG_ERROR_RESULTS_DISK_FULL;
	//}

	//if(SUCCEEDED(hr))
	//{
	//	if(_logFileDrive=="")
	//		setLogFileDrive();
	//	if(diskFull(_logFileDrive))
	//	{
	//		hr = CG_ERROR_LOGFILE_DISK_FULL;
	//	}
	//}

	//if(SUCCEEDED(hr))
	//{
	//    CG_LOG("CCGResearchCtrl::load306Sequence() : Entering", DIAGNOSTIC_LEVEL)
	//
	//	CString results_dir_CString;

	//	if(!validFunctionCall("load306Sequence"))
	//	{
	//		hr = CG_ERROR_INVALID_STATE;
	//	}
	//	else
	//	{

	//		CString moduleNameStr(moduleName); 

	//		hr = CLoseGridSystem::instance()->load306Sequence(moduleNameStr,
	//														numPoints,
	//														channel,
	//														(int)sourceDelay,
	//														(int)measureDelay);
	//	}

	//	CG_LOG("CCGResearchCtrl::load306Sequence() : Exiting", DIAGNOSTIC_LEVEL)
	//}

 //   if(FAILED(hr)) AfxThrowOleException(hr);
    return;
}

// Get version control info
void 
CCGResearchCtrl::
getVersionInfo(BSTR* productName,
			BSTR* versionName,
			BSTR* label,
			BSTR* labelInfo,
			BSTR* labelComment,
			BSTR* buildConfig)
{
//	CG_LOG("CCGResearchCtrl::getVersionInfo( ) : Entered", DIAGNOSTIC_LEVEL)
//
//	CString productNameStr			= "";
//	CString versionStr				= "";
//	CString labelStr				= "";
//	CString labelInfoStr			= "";
//	CString labelCommentStr			= "";
//	CString buildConfigStr			= "";
//
//	// Product info
//	productNameStr	= PRODUCT_NAME;
//	versionStr		= VERSION;
//
//	// Label info
//#ifdef LABEL
//
//	labelStr = LABEL;
//
//	#ifdef LABELSET
//		labelInfoStr = LABELSET;
//	#endif
//
//	#ifdef LABELCOMMENT
//		labelCommentStr = LABELCOMMENT;
//	#endif
//
//#else // no LABEL
//	labelStr = "";
//
//#endif // LABEL
//
//#ifdef HARDWARE_BUILD
//	#ifdef NDEBUG
//		buildConfigStr = "Release";
//	#else
//		buildConfigStr = "Debug";
//	#endif
//#else
//	buildConfigStr = "SoftwareOnlyDebug";
//#endif
//
//
//	*productName		= productNameStr.AllocSysString();
//	*versionName		= versionStr.AllocSysString();
//	*label				= labelStr.AllocSysString();
//	*labelInfo			= labelInfoStr.AllocSysString();
//	*labelComment		= labelCommentStr.AllocSysString();
//	*buildConfig		= buildConfigStr.AllocSysString();
//
//	CG_LOG("CCGResearchCtrl::getVersionInfo( ) : Exiting", DIAGNOSTIC_LEVEL)

    return;
}

void 
CCGResearchCtrl::
resetSequences()
{
	//CG_LOG("resetSequences called", INFO_LEVEL)

 //   HRESULT hr = S_OK;

	//if(_resultsDrive=="")
	//	setResultsDrive();
	//if(diskFull(_resultsDrive))
	//{
	//	hr = CG_ERROR_RESULTS_DISK_FULL;
	//}

 //   if(SUCCEEDED(hr))
 //   {
	//    if(_logFileDrive=="")
	//	    setLogFileDrive();
	//    if(diskFull(_logFileDrive))
	//    {
	//	    hr = CG_ERROR_LOGFILE_DISK_FULL;
	//    }
 //   }


 //   if(SUCCEEDED(hr))
 //   {
	//    CG_LOG("CCGResearchCtrl::resetSequences() : Entering", DIAGNOSTIC_LEVEL)

	//    // we can only perform this operation
	//    // if the system is in a startup state
	//    if(!validFunctionCall("resetSequences"))
	//    {
	//	    hr = CG_ERROR_INVALID_STATE;
	//    }
	//    else
	//    {
	//	    hr = CLoseGridSystem::instance()->resetSequences();
	//    }

	//    CG_LOG("CCGResearchCtrl::resetSequences() : Exiting\n", DIAGNOSTIC_LEVEL)
 //   }

 //   if(FAILED(hr)) AfxThrowOleException(hr);
    return;
}

void 
CCGResearchCtrl::
runSequences()
{
	//CG_LOG("runSequences called", INFO_LEVEL)

 //   HRESULT hr = S_OK;

	//if(_resultsDrive=="")
	//	setResultsDrive();
	//if(diskFull(_resultsDrive))
	//{
	//	hr = CG_ERROR_RESULTS_DISK_FULL;
	//}

 //   if(SUCCEEDED(hr))
 //   {
	//    if(_logFileDrive=="")
	//	    setLogFileDrive();
	//    if(diskFull(_logFileDrive))
	//    {
	//	    hr = CG_ERROR_LOGFILE_DISK_FULL;
	//    }
 //   }


 //   if(SUCCEEDED(hr))
 //   {
	//    CG_LOG("CCGResearchCtrl::runSequences() : Entering", DIAGNOSTIC_LEVEL)

	//    // we can only perform this operation
	//    // if the system is in a startup state
	//    if(!validFunctionCall("runSequences"))
	//    {
	//	    hr = CG_ERROR_INVALID_STATE;
	//    }
	//    else
	//    {
	//	    hr = CLoseGridSystem::instance()->runSequences();

	//		if(SUCCEEDED(hr))
	//			CLoseGridSystem::instance()->setSystemState(CLoseGridSystem::State::Complete);
	//    }

	//    CG_LOG("CCGResearchCtrl::runSequences() : Exiting\n", DIAGNOSTIC_LEVEL)
 //   }

 //   if(FAILED(hr)) AfxThrowOleException(hr);
    return;
}

//
///
///////////////////////////////////////////////////////////////////////////////////////////////////////
///
//

// CCGResearchCtrl message handlers



// DUAL_SUPPORT_START

// delegate standard IDispatch methods to MFC IDispatch implementation
DELEGATE_DUAL_INTERFACE(CCGResearchCtrl, Dual_DCGResearch)

// Our method and property functions can generally just
// delegate back to the methods we generated using
// ClassWizard. However, if we set up properties to
// access variables directly, we will need to write the
//  code to get/put the value into the variable.

STDMETHODIMP CCGResearchCtrl::XDual_DCGResearch::startup()
{
	METHOD_PROLOGUE(CCGResearchCtrl, Dual_DCGResearch)

	TRY_DUAL(IID_IDual_DCGResearch)
	{
		pThis->startup();
		return NOERROR;
	}
	CATCH_ALL_DUAL
}

STDMETHODIMP CCGResearchCtrl::XDual_DCGResearch::shutdown()
{
	METHOD_PROLOGUE(CCGResearchCtrl, Dual_DCGResearch)

	TRY_DUAL(IID_IDual_DCGResearch)
	{
		pThis->shutdown();
		return NOERROR;
	}
	CATCH_ALL_DUAL
}

STDMETHODIMP CCGResearchCtrl::XDual_DCGResearch::setCurrent( BSTR moduleName, double current )
{
	METHOD_PROLOGUE(CCGResearchCtrl, Dual_DCGResearch)

	TRY_DUAL(IID_IDual_DCGResearch)
	{
		pThis->setCurrent( moduleName, current );
		return NOERROR;
	}
	CATCH_ALL_DUAL
}

STDMETHODIMP CCGResearchCtrl::XDual_DCGResearch::getCurrent( BSTR moduleName, double* current )
{
	METHOD_PROLOGUE(CCGResearchCtrl, Dual_DCGResearch)

	TRY_DUAL(IID_IDual_DCGResearch)
	{
		pThis->getCurrent( moduleName, current );
		return NOERROR;
	}
	CATCH_ALL_DUAL
}

STDMETHODIMP CCGResearchCtrl::XDual_DCGResearch::setOutputConnection( BSTR moduleName, short outputConnection )
{
	METHOD_PROLOGUE(CCGResearchCtrl, Dual_DCGResearch)

	TRY_DUAL(IID_IDual_DCGResearch)
	{
		pThis->setOutputConnection( moduleName, outputConnection );
		return NOERROR;
	}
	CATCH_ALL_DUAL
}

STDMETHODIMP CCGResearchCtrl::XDual_DCGResearch::getOutputConnection( BSTR moduleName, short* outputConnection )
{
	METHOD_PROLOGUE(CCGResearchCtrl, Dual_DCGResearch)

	TRY_DUAL(IID_IDual_DCGResearch)
	{
		pThis->getOutputConnection( moduleName, outputConnection );
		return NOERROR;
	}
	CATCH_ALL_DUAL
}

STDMETHODIMP CCGResearchCtrl::XDual_DCGResearch::setInSequence( BSTR moduleName, short inSequence )
{
	METHOD_PROLOGUE(CCGResearchCtrl, Dual_DCGResearch)

	TRY_DUAL(IID_IDual_DCGResearch)
	{
		pThis->setInSequence( moduleName, inSequence );
		return NOERROR;
	}
	CATCH_ALL_DUAL
}

STDMETHODIMP CCGResearchCtrl::XDual_DCGResearch::setCoarseFrequencyOffset( double powerRatio, double frequency )
{
	METHOD_PROLOGUE(CCGResearchCtrl, Dual_DCGResearch)

	TRY_DUAL(IID_IDual_DCGResearch)
	{
		pThis->setCoarseFrequencyOffset( powerRatio, frequency );
		return NOERROR;
	}
	CATCH_ALL_DUAL
}

STDMETHODIMP CCGResearchCtrl::XDual_DCGResearch::setTECPIDValues( double pValue, double iValue, double dValue )
{
	METHOD_PROLOGUE(CCGResearchCtrl, Dual_DCGResearch)

	TRY_DUAL(IID_IDual_DCGResearch)
	{
		pThis->setTECPIDValues( pValue, iValue, dValue );
		return NOERROR;
	}
	CATCH_ALL_DUAL
}

STDMETHODIMP CCGResearchCtrl::XDual_DCGResearch::getTECPIDValues( double* pValue, double* iValue, double* dValue )
{
	METHOD_PROLOGUE(CCGResearchCtrl, Dual_DCGResearch)

	TRY_DUAL(IID_IDual_DCGResearch)
	{
		pThis->getTECPIDValues( pValue, iValue, dValue );
		return NOERROR;
	}
	CATCH_ALL_DUAL
}

STDMETHODIMP CCGResearchCtrl::XDual_DCGResearch::getSequenceCount( BSTR moduleName, short* sequenceCount )
{
	METHOD_PROLOGUE(CCGResearchCtrl, Dual_DCGResearch)

	TRY_DUAL(IID_IDual_DCGResearch)
	{
		pThis->getSequenceCount( moduleName, sequenceCount );
		return NOERROR;
	}
	CATCH_ALL_DUAL
}

STDMETHODIMP CCGResearchCtrl::XDual_DCGResearch::getCurrentSequenceNumber( BSTR moduleName, short* currentSequenceNumber )
{
	METHOD_PROLOGUE(CCGResearchCtrl, Dual_DCGResearch)

	TRY_DUAL(IID_IDual_DCGResearch)
	{
		pThis->getCurrentSequenceNumber( moduleName, currentSequenceNumber );
		return NOERROR;
	}
	CATCH_ALL_DUAL
}

STDMETHODIMP CCGResearchCtrl::XDual_DCGResearch::readVoltage( BSTR moduleName, double* voltage )
{
	METHOD_PROLOGUE(CCGResearchCtrl, Dual_DCGResearch)

	TRY_DUAL(IID_IDual_DCGResearch)
	{
		pThis->readVoltage( moduleName, voltage );
		return NOERROR;
	}
	CATCH_ALL_DUAL
}

STDMETHODIMP CCGResearchCtrl::XDual_DCGResearch::getState( BSTR* current_state )
{
	METHOD_PROLOGUE(CCGResearchCtrl, Dual_DCGResearch)

	TRY_DUAL(IID_IDual_DCGResearch)
	{
		pThis->getState( current_state );
		return NOERROR;
	}

	CATCH_ALL_DUAL
}

STDMETHODIMP CCGResearchCtrl::XDual_DCGResearch::livtest(
    short powermeter_slot_number,
    short powermeter_channel_number,
    short currentsource_slot_number,
    double start_current,
    double stop_current,
    long number_of_steps,
    long step_duration,
    VARIANT* current_array,
    VARIANT* voltage_array,
    VARIANT* power_array,
    VARIANT* dPdI_array,
    VARIANT* d2PdI2_array,
    BSTR* results_dir )
{
	METHOD_PROLOGUE(CCGResearchCtrl, Dual_DCGResearch)

	TRY_DUAL(IID_IDual_DCGResearch)
	{
		pThis->livtest(
            powermeter_slot_number,
            powermeter_channel_number,
            currentsource_slot_number,
            start_current,
            stop_current,
            number_of_steps,
            step_duration,
            current_array,
            voltage_array,
            power_array,
            dPdI_array,
            d2PdI2_array,
            results_dir );
		return NOERROR;
	}

	CATCH_ALL_DUAL
}


STDMETHODIMP CCGResearchCtrl::XDual_DCGResearch::setup( BSTR laser_id )
{
	METHOD_PROLOGUE(CCGResearchCtrl, Dual_DCGResearch)

	TRY_DUAL(IID_IDual_DCGResearch)
	{
		pThis->setup( laser_id );
		return NOERROR;
	}

	CATCH_ALL_DUAL
}

STDMETHODIMP CCGResearchCtrl::XDual_DCGResearch::rampdown()
{
	METHOD_PROLOGUE(CCGResearchCtrl, Dual_DCGResearch)

	TRY_DUAL(IID_IDual_DCGResearch)
	{
		pThis->rampdown( );
		return NOERROR;
	}

	CATCH_ALL_DUAL
}

STDMETHODIMP CCGResearchCtrl::XDual_DCGResearch::readPower(
    short slot_number,
    short channel_number,
    double* optical_power )
{
	METHOD_PROLOGUE(CCGResearchCtrl, Dual_DCGResearch)

	TRY_DUAL(IID_IDual_DCGResearch)
	{
		pThis->readPower( 
            slot_number,
            channel_number,
            optical_power );
		return NOERROR;
	}

	CATCH_ALL_DUAL
}

STDMETHODIMP CCGResearchCtrl::XDual_DCGResearch::readOpticalPower(
    BSTR moduleName,
    short channel,
    double* opticalPower )
{
	METHOD_PROLOGUE(CCGResearchCtrl, Dual_DCGResearch)

	TRY_DUAL(IID_IDual_DCGResearch)
	{
		pThis->readOpticalPower( 
            moduleName,
            channel,
            opticalPower );
		return NOERROR;
	}

	CATCH_ALL_DUAL
}

STDMETHODIMP CCGResearchCtrl::XDual_DCGResearch::readOpticalPowerAtFreq(
    BSTR moduleName,
    short channel,
	double	frequency,
    double* opticalPower )
{
	METHOD_PROLOGUE(CCGResearchCtrl, Dual_DCGResearch)

	TRY_DUAL(IID_IDual_DCGResearch)
	{
		pThis->readOpticalPowerAtFreq( 
            moduleName,
            channel,
			frequency,
            opticalPower );
		return NOERROR;
	}

	CATCH_ALL_DUAL
}

STDMETHODIMP CCGResearchCtrl::XDual_DCGResearch::readPhotodiodeCurrent(
    BSTR moduleName,
    short channel,
    double* current )
{
	METHOD_PROLOGUE(CCGResearchCtrl, Dual_DCGResearch)

	TRY_DUAL(IID_IDual_DCGResearch)
	{
		pThis->readPhotodiodeCurrent( 
            moduleName,
            channel,
            current );
		return NOERROR;
	}

	CATCH_ALL_DUAL
}

STDMETHODIMP CCGResearchCtrl::XDual_DCGResearch::readCoarseFrequency( double* frequency )
{
	METHOD_PROLOGUE(CCGResearchCtrl, Dual_DCGResearch)

	TRY_DUAL(IID_IDual_DCGResearch)
	{
		pThis->readCoarseFrequency(
            frequency );
		return NOERROR;
	}

	CATCH_ALL_DUAL
}

STDMETHODIMP CCGResearchCtrl::XDual_DCGResearch::readFrequency( double* frequency )
{
	METHOD_PROLOGUE(CCGResearchCtrl, Dual_DCGResearch)

	TRY_DUAL(IID_IDual_DCGResearch)
	{
		pThis->readFrequency(
            frequency );
		return NOERROR;
	}

	CATCH_ALL_DUAL
}

STDMETHODIMP CCGResearchCtrl::XDual_DCGResearch::readTemperature( double* temperature )
{
	METHOD_PROLOGUE(CCGResearchCtrl, Dual_DCGResearch)

	TRY_DUAL(IID_IDual_DCGResearch)
	{
		pThis->readTemperature(
            temperature );
		return NOERROR;
	}

	CATCH_ALL_DUAL
}

STDMETHODIMP CCGResearchCtrl::XDual_DCGResearch::setTECSetpoint( double temp_setpoint )
{
	METHOD_PROLOGUE(CCGResearchCtrl, Dual_DCGResearch)

	TRY_DUAL(IID_IDual_DCGResearch)
	{
		pThis->setTECSetpoint(
            temp_setpoint );
		return NOERROR;
	}

	CATCH_ALL_DUAL
}

STDMETHODIMP CCGResearchCtrl::XDual_DCGResearch::getTECSetpoint( double* temp_setpoint )
{
	METHOD_PROLOGUE(CCGResearchCtrl, Dual_DCGResearch)

	TRY_DUAL(IID_IDual_DCGResearch)
	{
		pThis->getTECSetpoint(
            temp_setpoint );
		return NOERROR;
	}

	CATCH_ALL_DUAL
}

STDMETHODIMP CCGResearchCtrl::XDual_DCGResearch::get2DData(
    short currentsource_slot_number,
    double min_current,
    double max_current,
    long number_of_steps,
    long step_duration,
	VARIANT_BOOL frequencyUnits,
    BSTR* results_dir )
{
	METHOD_PROLOGUE(CCGResearchCtrl, Dual_DCGResearch)

	TRY_DUAL(IID_IDual_DCGResearch)
	{
		pThis->get2DData(
            currentsource_slot_number,
            min_current,
            max_current,
            number_of_steps,
            step_duration,
			frequencyUnits,
            results_dir );
		return NOERROR;
	}

	CATCH_ALL_DUAL
}

STDMETHODIMP CCGResearchCtrl::XDual_DCGResearch::get2DData2(
    BSTR			moduleName,
	VARIANT*		currents,
    long			stepDuration,
    VARIANT_BOOL	frequencyUnits,
    BSTR*			results_dir)
{
	METHOD_PROLOGUE(CCGResearchCtrl, Dual_DCGResearch)

	TRY_DUAL(IID_IDual_DCGResearch)
	{
		pThis->get2DData2(
            moduleName,
            currents,
            stepDuration,
			frequencyUnits,
            results_dir);
		return NOERROR;
	}

	CATCH_ALL_DUAL
}
STDMETHODIMP CCGResearchCtrl::XDual_DCGResearch::get306SequenceMeasurements(
    BSTR			moduleName,
	VARIANT*		channel1Measurements,
	VARIANT*		channel2Measurements)
{
	METHOD_PROLOGUE(CCGResearchCtrl, Dual_DCGResearch)

	TRY_DUAL(IID_IDual_DCGResearch)
	{
		pThis->get306SequenceMeasurements(
            moduleName,
			channel1Measurements,
			channel2Measurements);
		return NOERROR;
	}

	CATCH_ALL_DUAL
}
STDMETHODIMP CCGResearchCtrl::XDual_DCGResearch::get3DData(
    short Ix_slot_number,
    double Ix_min_current,
    double Ix_max_current,
    long Ix_number_of_steps,
    long Ix_step_duration,
    short Iy_slot_number,
    double Iy_min_current,
    double Iy_max_current,
    long Iy_number_of_steps,
    VARIANT_BOOL hysteresis,
	VARIANT_BOOL frequencyUnits,
    BSTR* results_dir )
{
	METHOD_PROLOGUE(CCGResearchCtrl, Dual_DCGResearch)

	TRY_DUAL(IID_IDual_DCGResearch)
	{
		pThis->get3DData(
            Ix_slot_number,
            Ix_min_current,
            Ix_max_current,
            Ix_number_of_steps,
            Ix_step_duration,
            Iy_slot_number,
            Iy_min_current,
            Iy_max_current,
            Iy_number_of_steps,
            hysteresis,
			frequencyUnits,
            results_dir );
		return NOERROR;
	}

	CATCH_ALL_DUAL
}

STDMETHODIMP 
CCGResearchCtrl::
XDual_DCGResearch::
get3DData2(
    BSTR			moduleNameX,
	VARIANT*		currentsX,
    BSTR			moduleNameY,
	VARIANT*		currentsY,
    long			stepDuration,
    VARIANT_BOOL	hysteresis,
    VARIANT_BOOL	frequencyUnits,
    BSTR*			results_dir)
{
	METHOD_PROLOGUE(CCGResearchCtrl, Dual_DCGResearch)

	TRY_DUAL(IID_IDual_DCGResearch)
	{
		pThis->get3DData2(
            moduleNameX,
            currentsX,
            moduleNameY,
            currentsY,
            stepDuration,
            hysteresis,
			frequencyUnits,
            results_dir);
		return NOERROR;
	}

	CATCH_ALL_DUAL
}

STDMETHODIMP 
CCGResearchCtrl::
XDual_DCGResearch::
load305Sequence(
    BSTR			moduleName,
	VARIANT*		currents,
    short			sourceDelay,
	short			measureDelay)
{
	METHOD_PROLOGUE(CCGResearchCtrl, Dual_DCGResearch)

	TRY_DUAL(IID_IDual_DCGResearch)
	{
		pThis->load305Sequence(
            moduleName,
            currents,
			sourceDelay,
			measureDelay);
		return NOERROR;
	}

	CATCH_ALL_DUAL
}

STDMETHODIMP 
CCGResearchCtrl::
XDual_DCGResearch::
load306Sequence(
    BSTR			moduleName,
	short			numPoints,
	short			channel,
    short			sourceDelay,
	short			measureDelay)
{
	METHOD_PROLOGUE(CCGResearchCtrl, Dual_DCGResearch)

	TRY_DUAL(IID_IDual_DCGResearch)
	{
		pThis->load306Sequence(
            moduleName,
            numPoints,
			channel,
			sourceDelay,
			measureDelay);
		return NOERROR;
	}

	CATCH_ALL_DUAL
}

STDMETHODIMP 
CCGResearchCtrl::
XDual_DCGResearch::
getVersionInfo(BSTR* productName,
			BSTR* versionName,
			BSTR* label,
			BSTR* labelInfo,
			BSTR* labelComment,
			BSTR* buildConfig)
{
	METHOD_PROLOGUE(CCGResearchCtrl, Dual_DCGResearch)

	TRY_DUAL(IID_IDual_DCGResearch)
	{
		pThis->getVersionInfo(productName,
							versionName,
							label,
							labelInfo,
							labelComment,
							buildConfig );
		return NOERROR;
	}

	CATCH_ALL_DUAL
}

STDMETHODIMP 
CCGResearchCtrl::
XDual_DCGResearch::resetSequences()
{
	METHOD_PROLOGUE(CCGResearchCtrl, Dual_DCGResearch)

	TRY_DUAL(IID_IDual_DCGResearch)
	{
		pThis->resetSequences( );
		return NOERROR;
	}

	CATCH_ALL_DUAL
}

STDMETHODIMP 
CCGResearchCtrl::
XDual_DCGResearch::runSequences()
{
	METHOD_PROLOGUE(CCGResearchCtrl, Dual_DCGResearch)

	TRY_DUAL(IID_IDual_DCGResearch)
	{
		pThis->runSequences( );
		return NOERROR;
	}

	CATCH_ALL_DUAL
}


// Implement ISupportErrorInfo to indicate we support the
// OLE Automation error handler.
IMPLEMENT_DUAL_ERRORINFO(CCGResearchCtrl, IID_IDual_DCGResearch)

// DUAL_SUPPORT_END

//
//// private functions
//
//bool
//CCGResearchCtrl::
//validFunctionCall(CString functionName)
//{
//	EnterCriticalSection(&criticalSection);
//
//	bool valid = false;
//
//    CLoseGridSystem::State currentState = CLoseGridSystem::instance()->getSystemState();
//
//	if(functionName=="startup")
//	{
//        if(currentState == CLoseGridSystem::State::Instantiated)
//			valid = true;
//	}
//	else if(functionName=="shutdown")
//	{
//		if(	(currentState == CLoseGridSystem::State::Startup)||
//			(currentState == CLoseGridSystem::State::Setup)||
//			(currentState == CLoseGridSystem::State::Complete))
//		{
//			valid = true;
//		}
//	}
//	else if(functionName=="getState")
//	{
//			valid = true;
//	}
//	else if(functionName=="setup")
//	{
//		if(	(currentState == CLoseGridSystem::State::Startup)||
//			(currentState == CLoseGridSystem::State::Setup)||
//			(currentState == CLoseGridSystem::State::Complete))
//		{
//			valid = true;
//		}
//	}
//	else if(functionName=="getCurrent")
//	{
//		if(	(currentState == CLoseGridSystem::State::Setup)||
//			(currentState == CLoseGridSystem::State::Complete))
//		{
//			valid = true;
//		}
//	}
//	else if(functionName=="setCurrent")
//	{
//		if(	(currentState == CLoseGridSystem::State::Setup)||
//			(currentState == CLoseGridSystem::State::Complete))
//		{
//			valid = true;
//		}
//	}
//	else if(functionName=="rampDown")
//	{
//		if(	(currentState == CLoseGridSystem::State::Setup)||
//			(currentState == CLoseGridSystem::State::Complete))
//		{
//			valid = true;
//		}
//	}
//	else if(functionName=="readFrequency")
//	{
//		if(	(currentState == CLoseGridSystem::State::Setup)||
//			(currentState == CLoseGridSystem::State::Complete))
//		{
//			valid = true;
//		}
//	}
//	else if(functionName=="readTemperature")
//	{
//		if(	(currentState == CLoseGridSystem::State::Startup)||
//			(currentState == CLoseGridSystem::State::Setup)||
//			(currentState == CLoseGridSystem::State::Complete))
//		{
//			valid = true;
//		}
//	}
//	else if(functionName=="setTECSetpoint")
//	{
//		if(	(currentState == CLoseGridSystem::State::Startup)||
//			(currentState == CLoseGridSystem::State::Setup)||
//			(currentState == CLoseGridSystem::State::Complete))
//		{
//			valid = true;
//		}
//	}
//	else if(functionName=="getTECSetpoint")
//	{
//		if(	(currentState == CLoseGridSystem::State::Startup)||
//			(currentState == CLoseGridSystem::State::Setup)||
//			(currentState == CLoseGridSystem::State::Complete))
//		{
//			valid = true;
//		}
//	}
//	else if(functionName=="readPower")
//	{
//		if(	(currentState == CLoseGridSystem::State::Setup)||
//			(currentState == CLoseGridSystem::State::Complete))
//		{
//			valid = true;
//		}
//	}
//	else if(functionName=="readVoltage")
//	{
//		if(	(currentState == CLoseGridSystem::State::Setup)||
//			(currentState == CLoseGridSystem::State::Complete))
//		{
//			valid = true;
//		}
//	}
//	else if(functionName=="livtest")
//	{
//		if(	(currentState == CLoseGridSystem::State::Setup)||
//			(currentState == CLoseGridSystem::State::Complete))
//		{
//			valid = true;
//		}
//	}
//	else if(functionName=="get2DData")
//	{
//		if(	(currentState == CLoseGridSystem::State::Setup)||
//			(currentState == CLoseGridSystem::State::Complete))
//		{
//			valid = true;
//		}
//	}
//	else if(functionName=="get3DData")
//	{
//		if(	(currentState == CLoseGridSystem::State::Setup)||
//			(currentState == CLoseGridSystem::State::Complete))
//		{
//			valid = true;
//		}
//	}
//	else if(functionName=="get3DData2")
//	{
//		if(	(currentState == CLoseGridSystem::State::Setup)||
//			(currentState == CLoseGridSystem::State::Complete))
//		{
//			valid = true;
//		}
//	}
// 	else if(functionName=="setOutputConnection")
//	{
//		if(	(currentState == CLoseGridSystem::State::Setup)||
//			(currentState == CLoseGridSystem::State::Complete))
//		{
//			valid = true;
//		}
//	}
//  	else if(functionName=="getOutputConnection")
//	{
//		valid = true;
//	}
//	else if(functionName=="resetSequences")
//	{
//		if(	(currentState == CLoseGridSystem::State::Setup)||
//			(currentState == CLoseGridSystem::State::Complete))
//		{
//			valid = true;
//		}
//	}
//	else if(functionName=="runSequences")
//	{
//		if(	(currentState == CLoseGridSystem::State::Setup)||
//			(currentState == CLoseGridSystem::State::Complete))
//		{
//			valid = true;
//		}
//	}
//	else if(functionName=="load305Sequence")
//	{
//		if(	(currentState == CLoseGridSystem::State::Setup)||
//			(currentState == CLoseGridSystem::State::Complete))
//		{
//			valid = true;
//		}
//	}
//	else if(functionName=="load306Sequence")
//	{
//		if(	(currentState == CLoseGridSystem::State::Setup)||
//			(currentState == CLoseGridSystem::State::Complete))
//		{
//			valid = true;
//		}
//	}
//	else if(functionName=="getSequenceCount")
//	{
//		if(	(currentState == CLoseGridSystem::State::Setup)||
//			(currentState == CLoseGridSystem::State::Complete))
//		{
//			valid = true;
//		}
//	}
//  	else if(functionName=="getCurrentSequenceNumber")
//	{
//		if(	(currentState == CLoseGridSystem::State::Setup)||
//			(currentState == CLoseGridSystem::State::Complete))
//		{
//			valid = true;
//		}
//	}
//  	else if(functionName=="setInSequence")
//	{
//		if(	(currentState == CLoseGridSystem::State::Setup)||
//			(currentState == CLoseGridSystem::State::Complete))
//		{
//			valid = true;
//		}
//	}
//  	else if(functionName=="get306SequenceMeasurements")
//	{
//		if(currentState == CLoseGridSystem::State::Complete)
//		{
//			valid = true;
//		}
//	}
//
//
//    if(!valid)
//    {
//        CString logmsg = "CCloseGrid::validFunctionCall() : Invalid function call ";
//        logmsg = logmsg + functionName + CString("() while in ");
//        if(currentState == CLoseGridSystem::State::Instantiated) logmsg = logmsg + CString("Instantiated");
//        if(currentState == CLoseGridSystem::State::Startup) logmsg = logmsg + CString("Startup");
//        if(currentState == CLoseGridSystem::State::Setup) logmsg = logmsg + CString("Setup");
//        if(currentState == CLoseGridSystem::State::Busy) logmsg = logmsg + CString("Busy");
//        if(currentState == CLoseGridSystem::State::Complete) logmsg = logmsg + CString("Complete");
//        logmsg = logmsg + CString(" state.");
//        CG_LOG(logmsg.GetBuffer(), WARNING_LEVEL)
//    }
//
//	LeaveCriticalSection(&criticalSection);
//
//	return valid;
//}
//
//
//void
//CCGResearchCtrl::
//setLogFileDrive()
//{
//	//CLoseGridSystem::instance()->getLogFileDrive(_logFileDrive);
//}
//
//void
//CCGResearchCtrl::
//setResultsDrive()
//{
//	if(CLoseGridSystem::instance())
//	{
//		if(CLoseGridSystem::instance()->shelfInUse)
//		{
//			CString resultsPath = CLoseGridSystem::instance()->resultsFilePath_get();
//			_resultsDrive = resultsPath.SpanExcluding(":");
//			_resultsDrive = _resultsDrive + ":\\";
//		}
//		else
//			_resultsDrive = "";
//	}
//	else
//	{
//		_resultsDrive = "";
//	}
//}
//
//bool
//CCGResearchCtrl::
//diskFull(CString diskDrive)
//{
//	bool	diskFull = false;
//	BOOL	fResult = true;
//
//	__int64		numberOfFreeBytes = 0;
//
//	DWORD	dwSectPerClust; 
//	DWORD	dwBytesPerSect;
//	DWORD	dwFreeClusters; 
//	DWORD	dwTotalClusters;
//
//	if(diskDrive!="")
//	{
//		///////////////////////////////////////////
//		// Check disk the logfile is running
//		// on first for disk space
//
//		fResult = GetDiskFreeSpace (diskDrive, 
//						&dwSectPerClust, 
//						&dwBytesPerSect,
//						&dwFreeClusters, 
//						&dwTotalClusters);
//
//		// convert to megabytes
//		numberOfFreeBytes = dwSectPerClust*dwBytesPerSect;
//		numberOfFreeBytes = numberOfFreeBytes*dwFreeClusters;
//
//		if(fResult)
//		{
//			if(numberOfFreeBytes < MIN_DISK_SPACE_ALLOWED)
//			{
//				diskFull = true;
//			}
//			else
//			{
//				diskFull = false;
//				_diskFullLogged = false;
//			}
//		}
//		else
//		{
//			// error getting disk space
//			// available
//			diskFull = true;
//		}
//	}
//	else
//	{
//		diskFull = false;
//	}
//
//	if((diskFull)&&(!_diskFullLogged))
//	{
//		char errMsg[BUF_SIZE];
//		memset(errMsg, '\0', sizeof(errMsg));
//		sprintf(errMsg, "CCloseGrid::diskFull() : Disk %s is Full", diskDrive);
//		CG_LOG(errMsg, CRITICAL_LEVEL)
//
//		_diskFullLogged = true;
//	}
//
//	return diskFull;
//}
//
//bool
//CCGResearchCtrl::
//validLaserId(CString laserId)
//{
//	bool valid = true;
//
//	if((laserId=="")||(laserId=="0"))
//		valid = false;
//
//	// check for illegal characters
//	int foundInvalidChar = -1;
//	foundInvalidChar = laserId.FindOneOf("\\:*?\"<>�/");
//
//	if(foundInvalidChar>=0)
//		valid = false;
//
//	return valid;
//}
//
//void
//CCGResearchCtrl::
//convertVectorToVariant(vector<double>& array, VARIANT& variantArray)
//{
//	SAFEARRAYBOUND	rgsabound[1];
//	//VARIANT			variantArray;
//
//	//Set bounds of array
//	rgsabound[0].lLbound	= 0;					//Lower bound
//	rgsabound[0].cElements	= (ULONG)array.size();	//Number of elements
//
//	VariantInit(&variantArray);
//
//	//Assigns the Variant to hold an array of doubles (real 8-byte numbers)
//	variantArray.vt = VT_ARRAY | VT_R8;
//
//	//Create the safe array of doubles, with the specified bounds, and only 1 dimension
//	variantArray.parray = SafeArrayCreate( VT_R8, 1, rgsabound );
//	//SafeArrayAllocData( variantArray.parray );
//
//
//	double* lpusBuffer;
//	//Fill in the buffer
//	SafeArrayAccessData( variantArray.parray, (void**)&lpusBuffer );
//
//	for( int i=0; i<(int)(rgsabound[0].cElements); i++ )
//	{
//		*(lpusBuffer + i ) = array[i];
//	}
//
//	SafeArrayUnaccessData( variantArray.parray );
//
//	///////////////////////////////////////////////////////////////////////////////////////////////
//}
//
//vector<double>
//CCGResearchCtrl::
//convertVariantToVector(VARIANT&	variantArray)
//{
//	vector<double> array;
//
//	VariantInit(&variantArray);
//
//	if(variantArray.vt == (VT_ARRAY & VT_R8))
//	{
//		double* lpusBuffer;
//		SafeArrayAccessData( variantArray.parray, (void**)&lpusBuffer );
//
//		if(variantArray.parray->cDims == 1) //1D array
//		{
//			ULONG num = variantArray.parray->rgsabound[0].cElements;
//			for(ULONG i=0; i<num; i++)
//			{
//				array.push_back(lpusBuffer[i]);
//			}
//		}
//
//		SafeArrayUnaccessData( variantArray.parray );
//	}
//
//	return array;
//}


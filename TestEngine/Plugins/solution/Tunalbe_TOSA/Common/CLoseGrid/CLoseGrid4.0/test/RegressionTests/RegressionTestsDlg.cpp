// RegressionTestsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "RegressionTests.h"
#include "RegressionTestsDlg.h"

#include "closegridsystemactrl1.h"

#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include <iostream>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CRegressionTestsDlg dialog

// Global instance of the LED Test Platform ActiveX Control
CClosegridsystemactrl1 g_test_cgsystem_axc;


CRegressionTestsDlg::CRegressionTestsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CRegressionTestsDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CRegressionTestsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CLOSEGRIDSYSTEMACTRL1, g_test_cgsystem_axc);
}

BEGIN_MESSAGE_MAP(CRegressionTestsDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


// CRegressionTestsDlg message handlers

BOOL CRegressionTestsDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	runTests();

	EndDialog(5); // value doesn't matter

	return TRUE;  // return TRUE  unless you set the focus to a control
}



// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CRegressionTestsDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CRegressionTestsDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

bool CRegressionTestsDlg::runTests()
{
	bool wasSucessful = true;

	CppUnit::TestFactoryRegistry &registry
		= CppUnit::TestFactoryRegistry::getRegistry();
	CppUnit::TextUi::TestRunner runner;
	runner.addTest( registry.makeTest() );
	wasSucessful = runner.run();

	return wasSucessful;
}


#pragma once

// CGResearchPropPage.h : Declaration of the CCGResearchPropPage property page class.


// CCGResearchPropPage : See CGResearchPropPage.cpp.cpp for implementation.

class CCGResearchPropPage : public COlePropertyPage
{
	DECLARE_DYNCREATE(CCGResearchPropPage)
	DECLARE_OLECREATE_EX(CCGResearchPropPage)

// Constructor
public:
	CCGResearchPropPage();

// Dialog Data
	enum { IDD = IDD_PROPPAGE_CGRESEARCH };

// Implementation
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Message maps
protected:
	DECLARE_MESSAGE_MAP()
};


#pragma once
#include "afxwin.h"


// CCGRegSetCString dialog

class CCGRegSetCString : public CDialog
{
	DECLARE_DYNAMIC(CCGRegSetCString)

public:
	CCGRegSetCString(
		CString* p_entry_name,
		CString* p_value,
		CWnd* pParent = NULL);   // standard constructor
	virtual ~CCGRegSetCString();

// Dialog Data
	enum { IDD = IDD_DIALOG3 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
public:
	CEdit _reg_entry;
	CEdit _CString_value;
private:
	CString* _p_entry_name;
	CString* _p_value;
public:
	afx_msg void OnBnClickedOk();
};

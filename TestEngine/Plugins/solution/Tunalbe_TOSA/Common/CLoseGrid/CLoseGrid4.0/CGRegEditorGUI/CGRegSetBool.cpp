// CGRegSetBool.cpp : implementation file
//

#include "stdafx.h"
#include "CGRegEditorGUI.h"
#include "CGRegSetBool.h"


// CCGRegSetBool dialog

IMPLEMENT_DYNAMIC(CCGRegSetBool, CDialog)
CCGRegSetBool::CCGRegSetBool(
		CString* p_entry_name,
		bool* p_value,
		CWnd* pParent /*=NULL*/ )
	: CDialog(CCGRegSetBool::IDD, pParent)
{
	_p_entry_name = p_entry_name;
	_p_value = p_value;
}

CCGRegSetBool::~CCGRegSetBool()
{
}

void CCGRegSetBool::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT1, _reg_entry);
	DDX_Control(pDX, IDC_CHECK1, _bool_value);
}


BEGIN_MESSAGE_MAP(CCGRegSetBool, CDialog)
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
END_MESSAGE_MAP()


// CCGRegSetBool message handlers


BOOL CCGRegSetBool::OnInitDialog()
{
    CDialog::OnInitDialog();

	if( _p_entry_name != NULL )
	{
		_reg_entry.SetWindowText(*_p_entry_name);
	}

	if( _p_value != NULL )
	{
		if(*_p_value)
			_bool_value.SetCheck(TRUE);
		else
			_bool_value.SetCheck(FALSE);
	}

    // Return TRUE unless you set the focus to a control.
    return TRUE;
}


void CCGRegSetBool::OnBnClickedOk()
{

	if( _p_value != NULL )
	{
		if( _bool_value.GetCheck() == TRUE )
			*_p_value = true;
		else
			*_p_value = false;
	}

	OnOK();
}

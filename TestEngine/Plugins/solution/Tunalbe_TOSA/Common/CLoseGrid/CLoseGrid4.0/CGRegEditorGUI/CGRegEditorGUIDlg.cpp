// CGRegEditorGUIDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CGRegEditorGUI.h"
#include "CGRegEditorGUIDlg.h"
#include "CGRegSWConfigDialog.h"

#include <process.h>
#include <errno.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// Use HW Config GUI as a separate executable
#define CG_HW_CONFIG_EXE "CGConfigEditor.exe"


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CCGRegEditorGUIDlg dialog



CCGRegEditorGUIDlg::CCGRegEditorGUIDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCGRegEditorGUIDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CCGRegEditorGUIDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CCGRegEditorGUIDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_EDIT_HARDWARE_CONFIG_BUTTON, OnBnClickedEditHardwareConfigButton)
	ON_BN_CLICKED(IDC_EDIT_SOFTWARE_CONFIG_BUTTON, OnBnClickedEditSWConfigButton)
END_MESSAGE_MAP()


// CCGRegEditorGUIDlg message handlers

BOOL CCGRegEditorGUIDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CCGRegEditorGUIDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CCGRegEditorGUIDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CCGRegEditorGUIDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CCGRegEditorGUIDlg::OnBnClickedEditHardwareConfigButton()
{
	intptr_t exitcode = _spawnlp( _P_WAIT, CG_HW_CONFIG_EXE, CG_HW_CONFIG_EXE, NULL );

	if(exitcode==-1)
	{
		int test_errno = errno;
		CString err_msg = CG_HW_CONFIG_EXE;
		err_msg += " : Error ";

		switch(test_errno)
		{
		case E2BIG :
			err_msg += ": Argument list exceeds 1024 bytes";
			break;
		case EINVAL :
			err_msg += ": Mode argument is invalid";
			break;
		case ENOENT :
			err_msg += ": File or path is not found";
			break;
		case ENOEXEC :
			err_msg += ": Specified file is not executable or has invalid executable-file format";
			break;
		case ENOMEM :
			err_msg += ": Not enough memory is available to execute new process";
			break;
		}

		AfxMessageBox( err_msg );
	}

}

void CCGRegEditorGUIDlg::OnBnClickedEditSWConfigButton()
{

    CCGRegSWConfigDialog dlg;
    INT_PTR nResponse = dlg.DoModal();
    if (nResponse == IDOK)
    {
        // TODO: Place code here to handle when the dialog is dismissed with OK
    }
    else if (nResponse == IDCANCEL)
    {
        // TODO: Place code here to handle when the dialog is dismissed with Cancel
    }


}

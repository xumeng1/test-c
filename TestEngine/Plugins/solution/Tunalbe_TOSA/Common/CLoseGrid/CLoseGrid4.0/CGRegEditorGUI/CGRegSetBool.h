#pragma once
#include "afxwin.h"


// CCGRegSetBool dialog

class CCGRegSetBool : public CDialog
{
	DECLARE_DYNAMIC(CCGRegSetBool)

public:
	CCGRegSetBool(
		CString* p_entry_name,
		bool* p_value,
		CWnd* pParent = NULL
		);
	virtual ~CCGRegSetBool();

// Dialog Data
	enum { IDD = IDD_DIALOG2 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
public:
	CEdit _reg_entry;
	CButton _bool_value;
private:
	CString* _p_entry_name;
	bool* _p_value;
public:
	afx_msg void OnBnClickedOk();
};

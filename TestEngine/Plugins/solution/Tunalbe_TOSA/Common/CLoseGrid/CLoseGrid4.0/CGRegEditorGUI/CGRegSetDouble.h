#pragma once
#include "afxwin.h"


// CCGRegSetDouble dialog

class CCGRegSetDouble : public CDialog
{
	DECLARE_DYNAMIC(CCGRegSetDouble)

public:
	CCGRegSetDouble(
		CString* p_entry_name,
		double* p_value,
		CWnd* pParent = NULL
		);
	virtual ~CCGRegSetDouble();

// Dialog Data
	enum { IDD = IDD_DIALOG5 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
public:
	CEdit _reg_entry;
	CEdit _double_value;
private:
	CString* _p_entry_name;
	double* _p_value;
public:
	afx_msg void OnBnClickedOk();
};

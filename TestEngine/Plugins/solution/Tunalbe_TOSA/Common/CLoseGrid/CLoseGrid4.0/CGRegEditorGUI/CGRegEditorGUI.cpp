// CGRegEditorGUI.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "CGRegEditorGUI.h"
#include "CGRegEditorGUIDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CCGRegEditorGUIApp

BEGIN_MESSAGE_MAP(CCGRegEditorGUIApp, CWinApp)
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()


// CCGRegEditorGUIApp construction

CCGRegEditorGUIApp::CCGRegEditorGUIApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only CCGRegEditorGUIApp object

CCGRegEditorGUIApp theApp;


// CCGRegEditorGUIApp initialization

BOOL CCGRegEditorGUIApp::InitInstance()
{
	CWinApp::InitInstance();


	CCGRegEditorGUIDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}

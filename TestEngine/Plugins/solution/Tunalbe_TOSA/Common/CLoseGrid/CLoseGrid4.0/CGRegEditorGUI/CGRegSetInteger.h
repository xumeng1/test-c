#pragma once
#include "afxwin.h"


// CCGRegSetInteger dialog

class CCGRegSetInteger : public CDialog
{
	DECLARE_DYNAMIC(CCGRegSetInteger)

public:
	CCGRegSetInteger(
		CString* p_entry_name,
		int* p_value,
		CWnd* pParent = NULL
		);
	virtual ~CCGRegSetInteger();

// Dialog Data
	enum { IDD = IDD_DIALOG4 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
public:
	CEdit _reg_entry;
	CEdit _int_value;
private:
	CString* _p_entry_name;
	int* _p_value;
public:
	afx_msg void OnBnClickedOk();
};

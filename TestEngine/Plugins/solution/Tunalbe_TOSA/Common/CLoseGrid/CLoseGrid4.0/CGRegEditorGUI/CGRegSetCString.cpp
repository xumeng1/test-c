// CGRegSetCString.cpp : implementation file
//

#include "stdafx.h"
#include "CGRegEditorGUI.h"
#include "CGRegSetCString.h"


// CCGRegSetCString dialog

IMPLEMENT_DYNAMIC(CCGRegSetCString, CDialog)
CCGRegSetCString::CCGRegSetCString(
		CString* p_entry_name,
		CString* p_value,
		CWnd* pParent /*=NULL*/)
	: CDialog(CCGRegSetCString::IDD, pParent)
{
	_p_entry_name = p_entry_name;
	_p_value = p_value;
}

CCGRegSetCString::~CCGRegSetCString()
{
}

void CCGRegSetCString::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT1, _reg_entry);
	DDX_Control(pDX, IDC_EDIT2, _CString_value);
}


BEGIN_MESSAGE_MAP(CCGRegSetCString, CDialog)
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
END_MESSAGE_MAP()


// CCGRegSetCString message handlers


BOOL CCGRegSetCString::OnInitDialog()
{
    CDialog::OnInitDialog();

	if( _p_entry_name != NULL )
	{
		_reg_entry.SetWindowText(*_p_entry_name);
	}

	if( _p_value != NULL )
	{
		_CString_value.SetWindowText(*_p_value);
	}

    // Return TRUE unless you set the focus to a control.
    return TRUE;
}



void CCGRegSetCString::OnBnClickedOk()
{

	if( _p_value != NULL )
	{
		_CString_value.GetWindowText(*_p_value);
	}

	OnOK();
}

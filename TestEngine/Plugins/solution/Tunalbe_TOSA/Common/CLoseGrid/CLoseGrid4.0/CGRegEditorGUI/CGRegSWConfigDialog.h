#pragma once
#include "afxcmn.h"
#include <vector>
#include "CGRegValues.h"

// CCGRegSWConfigDialog dialog

class CCGRegSWConfigDialog : public CDialog
{
	DECLARE_DYNAMIC(CCGRegSWConfigDialog)

public:
	CCGRegSWConfigDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CCGRegSWConfigDialog();

// Dialog Data
	enum { IDD = IDD_DIALOG1 };

protected:
    virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CTreeCtrl _reg_values_CTreeCtrl;
	afx_msg void OnNMDblclkTree1(NMHDR *pNMHDR, LRESULT *pResult);

private:
	void updateTextOfAllItems();

	std::vector<CString> _reg_key_tree_items_CStrings;
	std::vector<HTREEITEM> _reg_key_tree_items;
	std::vector<HTREEITEM> _reg_entry_tree_items;

	std::vector<std::vector<CCGRegEntry>::iterator> _i_r_vector;


};

// CGRegSWConfigDialog.cpp : implementation file
//

#include "stdafx.h"
#include "CGRegEditorGUI.h"
#include "CGRegSWConfigDialog.h"
#include "CGRegSetBool.h"
#include "CGRegSetCString.h"
#include "CGRegSetInteger.h"
#include "CGRegSetDouble.h"


// CCGRegSWConfigDialog dialog

IMPLEMENT_DYNAMIC(CCGRegSWConfigDialog, CDialog)
CCGRegSWConfigDialog::CCGRegSWConfigDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CCGRegSWConfigDialog::IDD, pParent)
{
}

CCGRegSWConfigDialog::~CCGRegSWConfigDialog()
{
}

void CCGRegSWConfigDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TREE1, _reg_values_CTreeCtrl);
}


BEGIN_MESSAGE_MAP(CCGRegSWConfigDialog, CDialog)
	ON_NOTIFY(NM_DBLCLK, IDC_TREE1, OnNMDblclkTree1)
END_MESSAGE_MAP()


// CCGRegSWConfigDialog message handlers


BOOL CCGRegSWConfigDialog::OnInitDialog()
{
    CDialog::OnInitDialog();


	CG_REG_RTYPE cgreg_rval = CG_REG->retrieveAllFromRegistry();

	if( cgreg_rval != CG_REG_RTYPE::ok )
	{
		// save values already in Registry and those missing as defaults
		CG_REG->saveAllToRegistry();
	}


	_reg_values_CTreeCtrl.DeleteAllItems();

	_reg_key_tree_items_CStrings.clear();
	_reg_key_tree_items.clear();
	_reg_entry_tree_items.clear();
	_i_r_vector.clear();

	SetWindowLong(
		_reg_values_CTreeCtrl.m_hWnd ,
		GWL_STYLE , 
		GetWindowLong( _reg_values_CTreeCtrl.m_hWnd , GWL_STYLE) | TVS_HASBUTTONS | TVS_LINESATROOT | TVS_HASLINES );

	CString root("Software Configuration");

	TVINSERTSTRUCT tvInsert;
	tvInsert.hParent = NULL;
	tvInsert.hInsertAfter = NULL;
	tvInsert.item.mask = TVIF_TEXT;
	tvInsert.item.pszText = root.GetBuffer();

	HTREEITEM h_root = _reg_values_CTreeCtrl.InsertItem(&tvInsert);

	std::vector<CCGRegEntry>::iterator i_r;

	for( i_r = CG_REG->_reg_entries.begin() ; i_r != CG_REG->_reg_entries.end() ; i_r++ )
	{
		CString reg_key_name = (*i_r).reg_key_name;
		CString reg_entry_name = (*i_r).reg_entry_name;

		// Tokenize the reg key and ensure each part (after root) is an item in GUI
		CString token;
		CString after_root;
		int curPos = 0;

		// First, find root
		token = reg_key_name.Tokenize("\\",curPos);
		while (token != root) token = reg_key_name.Tokenize("\\",curPos);

		// Next, check each token after root
		HTREEITEM h_last = h_root;
		token = reg_key_name.Tokenize("\\",curPos);
		CString from_root = token;
		while (token != "")
		{
			int i_from_root = 0;

			for( i_from_root = 0 ;
				 i_from_root < (int)_reg_key_tree_items_CStrings.size() ;
				 i_from_root++ )
			{
				if( from_root == _reg_key_tree_items_CStrings[i_from_root] )
				{
					// regkey already in GUI
					h_last = _reg_key_tree_items[i_from_root];
					break;
				}
			}
			if( i_from_root >= (int)_reg_key_tree_items_CStrings.size() )
			{
				// regkey is not in GUI, create item and store its handle

				HTREEITEM h_new = _reg_values_CTreeCtrl.InsertItem(
					TVIF_TEXT,token, 0, 0, 0, 0, 0, h_last, NULL);

				_reg_key_tree_items.push_back(h_new);
				_reg_key_tree_items_CStrings.push_back(from_root);
				h_last = h_new;
			}

			token = reg_key_name.Tokenize("\\",curPos);
			from_root += _T("\\");
			from_root += token;
		}

		// Insert each reg value as item in GUI and store its handle
		HTREEITEM h_new = _reg_values_CTreeCtrl.InsertItem(
			TVIF_TEXT,CG_REG->getText(*i_r), 0, 0, 0, 0, 0, h_last, NULL);

		_reg_entry_tree_items.push_back(h_new);
		_i_r_vector.push_back(i_r);

	}



    // Return TRUE unless you set the focus to a control.
    return TRUE;
}


void CCGRegSWConfigDialog::OnNMDblclkTree1(NMHDR *pNMHDR, LRESULT *pResult)
{
	HTREEITEM h_selection = _reg_values_CTreeCtrl.GetSelectedItem();

	CCGRegValues::rtype rval = CCGRegValues::rtype::ok;

	// At this point I need to search a list of reg items
	// determine is it a string, bool or int value
	// create an appropriate dialog box
	// if value is okay, update the registry and text in tree control

	int i;
	for( i = 0; i < (int)_reg_entry_tree_items.size() ; i++ )
	{
		if( h_selection == _reg_entry_tree_items[i] )
		{
			// found which item was selected
			break;
		}
	}

	if( i < (int)_reg_entry_tree_items.size() )
	{
		std::vector<CCGRegEntry>::iterator i_r = _i_r_vector[i];

		if( (*i_r).reg_entry_datatype == CCGRegEntry::datatype::_bool )
		{
			CString text = CG_REG->getText(*i_r);

			int index_of_value_start = text.ReverseFind('=') + 2;

			CString value_as_text;
			for( int ch = index_of_value_start ; ch < text.GetLength() ; ch++ )
			{
				value_as_text += text[ch];
			}
			bool b;
			if(value_as_text == REG_BOOL_TRUE_VALUE) b = true;
			else b = false;

			CString reg_entry = (*i_r).reg_entry_name;

			CCGRegSetBool dlg_bool(
				&reg_entry,
				&b);

			INT_PTR nResponse = dlg_bool.DoModal();
			if (nResponse == IDOK)
			{
				rval = CG_REG->setBool(*i_r, b);

				if( rval != CCGRegValues::rtype::ok )
				{
					AfxMessageBox(_T("Error"),  MB_SYSTEMMODAL);
				}
			}
			else if (nResponse == IDCANCEL)
			{
				// Do nothing
			}
		}

		if( (*i_r).reg_entry_datatype == CCGRegEntry::datatype::_CString )
		{

			CString text = CG_REG->getText(*i_r);

			int index_of_value_start = text.ReverseFind('=') + 2;

			CString value_as_text;
			for( int ch = index_of_value_start ; ch < text.GetLength() ; ch++ )
			{
				value_as_text += text[ch];
			}

			CString reg_entry = (*i_r).reg_entry_name;

			CCGRegSetCString dlg_bool(
				&reg_entry,
				&value_as_text);

			INT_PTR nResponse = dlg_bool.DoModal();
			if (nResponse == IDOK)
			{
				rval = CG_REG->setCString(*i_r, value_as_text);

				if( rval != CCGRegValues::rtype::ok )
				{
					AfxMessageBox(_T("Error"),  MB_SYSTEMMODAL);
				}
			}
			else if (nResponse == IDCANCEL)
			{
				// Do nothing
			}
		}

		if( (*i_r).reg_entry_datatype == CCGRegEntry::datatype::_double )
		{
			CString text = CG_REG->getText(*i_r);

			int index_of_value_start = text.ReverseFind('=') + 2;

			CString value_as_text;
			for( int ch = index_of_value_start ; ch < text.GetLength() ; ch++ )
			{
				value_as_text += text[ch];
			}
			double d = atof(value_as_text);

			CString reg_entry = (*i_r).reg_entry_name;

			CCGRegSetDouble dlg_double(
				&reg_entry,
				&d);

			INT_PTR nResponse = dlg_double.DoModal();
			if (nResponse == IDOK)
			{
				rval = CG_REG->setDouble(*i_r, d);

				if( rval != CCGRegValues::rtype::ok )
				{
					AfxMessageBox(_T("Error"),  MB_SYSTEMMODAL);
				}
			}
			else if (nResponse == IDCANCEL)
			{
				// Do nothing
			}
		}

		if( (*i_r).reg_entry_datatype == CCGRegEntry::datatype::_int )
		{

			CString text = CG_REG->getText(*i_r);

			int index_of_value_start = text.ReverseFind('=') + 2;

			CString value_as_text;
			for( int ch = index_of_value_start ; ch < text.GetLength() ; ch++ )
			{
				value_as_text += text[ch];
			}
			int i = atoi(value_as_text);

			CString reg_entry = (*i_r).reg_entry_name;

			CCGRegSetInteger dlg_int(
				&reg_entry,
				&i);

			INT_PTR nResponse = dlg_int.DoModal();
			if (nResponse == IDOK)
			{
				rval = CG_REG->setInt(*i_r, i);

				if( rval != CCGRegValues::rtype::ok )
				{
					AfxMessageBox(_T("Error"),  MB_SYSTEMMODAL);
				}
			}
			else if (nResponse == IDCANCEL)
			{
				// Do nothing
			}
		}

		if( rval == CCGRegValues::rtype::ok )
		{
			CG_REG->saveAllToRegistry();

			updateTextOfAllItems();
			//_reg_values_CTreeCtrl.SetItemText( h_selection, CG_REG->getText(*i_r) );
		}

	}

	*pResult = 0;
}

void CCGRegSWConfigDialog::updateTextOfAllItems()
{
	for( int i = 0 ; i < (int)_reg_entry_tree_items.size() ; i++ )
	{
		_reg_values_CTreeCtrl.SetItemText(
			_reg_entry_tree_items[i],
			CG_REG->getText( *(_i_r_vector[i]) ) );
	}
}



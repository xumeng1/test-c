// CGRegEditorGUIDlg.h : header file
//

#pragma once


// CCGRegEditorGUIDlg dialog
class CCGRegEditorGUIDlg : public CDialog
{
// Construction
public:
	CCGRegEditorGUIDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_CGREGEDITORGUI_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedEditHardwareConfigButton();
	afx_msg void OnBnClickedEditSWConfigButton();
};

// CGRegSetDouble.cpp : implementation file
//

#include "stdafx.h"
#include "CGRegEditorGUI.h"
#include "CGRegSetDouble.h"


// CCGRegSetDouble dialog

IMPLEMENT_DYNAMIC(CCGRegSetDouble, CDialog)
CCGRegSetDouble::CCGRegSetDouble(
		CString* p_entry_name,
		double* p_value,
		CWnd* pParent /*=NULL*/ )
	: CDialog(CCGRegSetDouble::IDD, pParent)
{
	_p_entry_name = p_entry_name;
	_p_value = p_value;
}

CCGRegSetDouble::~CCGRegSetDouble()
{
}

void CCGRegSetDouble::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT1, _reg_entry);
	DDX_Control(pDX, IDC_EDIT2, _double_value);
}


BEGIN_MESSAGE_MAP(CCGRegSetDouble, CDialog)
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
END_MESSAGE_MAP()


// CCGRegSetDouble message handlers


BOOL CCGRegSetDouble::OnInitDialog()
{
    CDialog::OnInitDialog();

	if( _p_entry_name != NULL )
	{
		_reg_entry.SetWindowText(*_p_entry_name);
	}

	if( _p_value != NULL )
	{
		CString text_value;
		text_value.AppendFormat("%g",*_p_value);
		_double_value.SetWindowText(text_value);
	}

    // Return TRUE unless you set the focus to a control.
    return TRUE;
}


void CCGRegSetDouble::OnBnClickedOk()
{

	if( _p_value != NULL )
	{
		CString text_value;
		_double_value.GetWindowText(text_value);
		*_p_value = atof(text_value);
	}

	OnOK();
}

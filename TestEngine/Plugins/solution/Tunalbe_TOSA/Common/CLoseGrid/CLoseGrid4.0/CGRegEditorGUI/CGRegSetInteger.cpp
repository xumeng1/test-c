// CGRegSetInteger.cpp : implementation file
//

#include "stdafx.h"
#include "CGRegEditorGUI.h"
#include "CGRegSetInteger.h"


// CCGRegSetInteger dialog

IMPLEMENT_DYNAMIC(CCGRegSetInteger, CDialog)
CCGRegSetInteger::CCGRegSetInteger(
		CString* p_entry_name,
		int* p_value,
		CWnd* pParent /*=NULL*/ )
	: CDialog(CCGRegSetInteger::IDD, pParent)
{
	_p_entry_name = p_entry_name;
	_p_value = p_value;
}

CCGRegSetInteger::~CCGRegSetInteger()
{
}

void CCGRegSetInteger::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT1, _reg_entry);
	DDX_Control(pDX, IDC_EDIT2, _int_value);
}


BEGIN_MESSAGE_MAP(CCGRegSetInteger, CDialog)
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
END_MESSAGE_MAP()


// CCGRegSetInteger message handlers


BOOL CCGRegSetInteger::OnInitDialog()
{
    CDialog::OnInitDialog();

	if( _p_entry_name != NULL )
	{
		_reg_entry.SetWindowText(*_p_entry_name);
	}

	if( _p_value != NULL )
	{
		CString text_value;
		text_value.AppendFormat("%d",*_p_value);
		_int_value.SetWindowText(text_value);
	}

    // Return TRUE unless you set the focus to a control.
    return TRUE;
}


void CCGRegSetInteger::OnBnClickedOk()
{

	if( _p_value != NULL )
	{
		CString text_value;
		_int_value.GetWindowText(text_value);
		*_p_value = atoi(text_value);
	}

	OnOK();
}

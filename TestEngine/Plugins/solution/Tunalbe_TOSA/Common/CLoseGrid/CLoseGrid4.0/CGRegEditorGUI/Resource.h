//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by CGRegEditorGUI.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_CGREGEDITORGUI_DIALOG       102
#define IDR_HTML_CGREGSETBOOL           103
#define IDR_MAINFRAME                   128
#define IDD_DIALOG1                     129
#define IDD_DIALOG2                     131
#define IDD_DIALOG3                     132
#define IDD_DIALOG4                     133
#define IDD_DIALOG5                     134
#define IDC_EDIT_SW_CONFIG_BUTTON       1000
#define IDC_EDIT_SOFTWARE_CONFIG_BUTTON 1000
#define IDC_EDIT_HARDWARE_CONFIG_BUTTON 1001
#define IDC_TREE1                       1001
#define IDC_EDIT1                       1002
#define IDC_CHECK1                      1003
#define IDC_EDIT2                       1004

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        133
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1005
#define _APS_NEXT_SYMED_VALUE           104
#endif
#endif

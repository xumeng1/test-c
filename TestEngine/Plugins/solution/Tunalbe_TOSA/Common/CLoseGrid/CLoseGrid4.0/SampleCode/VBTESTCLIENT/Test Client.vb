Public Class Form1
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents StartupButton As System.Windows.Forms.Button
    Friend WithEvents AxCGSystemAXC As AxCGSystemAXCLib.AxCGSystemAXC
    Friend WithEvents SetupButton As System.Windows.Forms.Button
    Friend WithEvents LaserTypeTextBox As System.Windows.Forms.TextBox
    Friend WithEvents LaserIDTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DateTimeStampTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents ShutdownButton As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents SystemStateTextBox As System.Windows.Forms.TextBox
    Friend WithEvents InitialGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents GetOutputConnectionButton As System.Windows.Forms.Button
    Friend WithEvents SetOutputConnectionButton As System.Windows.Forms.Button
    Friend WithEvents OutputConnectionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SetOutputConnectionUpDown As System.Windows.Forms.NumericUpDown
    Friend WithEvents CurrentSourceComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents SetCurrentButton As System.Windows.Forms.Button
    Friend WithEvents GetCurrentButton As System.Windows.Forms.Button
    Friend WithEvents SetCurrentNumericUpDown As System.Windows.Forms.NumericUpDown
    Friend WithEvents GetCurrentTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TabControl As System.Windows.Forms.TabControl
    Friend WithEvents CurrentSourcesTabPage As System.Windows.Forms.TabPage
    Friend WithEvents Get2DDataTabPage As System.Windows.Forms.TabPage
    Friend WithEvents Get2DDataButton As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Get2DDataCurrentSourceComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Get2DDataCurrentsFileTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Get2DDataMinCurrentTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Get2DDataMaxCurrentTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Get2DDataNumSetpointsTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Get2DDataOpenCurrentsFileButton As System.Windows.Forms.Button
    Friend WithEvents Get2DDataOpenCurrentsFileDialog As System.Windows.Forms.OpenFileDialog
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(Form1))
        Me.StartupButton = New System.Windows.Forms.Button()
        Me.AxCGSystemAXC = New AxCGSystemAXCLib.AxCGSystemAXC()
        Me.SetupButton = New System.Windows.Forms.Button()
        Me.LaserTypeTextBox = New System.Windows.Forms.TextBox()
        Me.LaserIDTextBox = New System.Windows.Forms.TextBox()
        Me.DateTimeStampTextBox = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.ShutdownButton = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.SystemStateTextBox = New System.Windows.Forms.TextBox()
        Me.InitialGroupBox = New System.Windows.Forms.GroupBox()
        Me.GetOutputConnectionButton = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.CurrentSourceComboBox = New System.Windows.Forms.ComboBox()
        Me.SetOutputConnectionButton = New System.Windows.Forms.Button()
        Me.OutputConnectionTextBox = New System.Windows.Forms.TextBox()
        Me.SetOutputConnectionUpDown = New System.Windows.Forms.NumericUpDown()
        Me.SetCurrentButton = New System.Windows.Forms.Button()
        Me.GetCurrentTextBox = New System.Windows.Forms.TextBox()
        Me.GetCurrentButton = New System.Windows.Forms.Button()
        Me.SetCurrentNumericUpDown = New System.Windows.Forms.NumericUpDown()
        Me.TabControl = New System.Windows.Forms.TabControl()
        Me.CurrentSourcesTabPage = New System.Windows.Forms.TabPage()
        Me.Get2DDataTabPage = New System.Windows.Forms.TabPage()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Get2DDataNumSetpointsTextBox = New System.Windows.Forms.TextBox()
        Me.Get2DDataMinCurrentTextBox = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Get2DDataMaxCurrentTextBox = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Get2DDataOpenCurrentsFileButton = New System.Windows.Forms.Button()
        Me.Get2DDataCurrentsFileTextBox = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Get2DDataCurrentSourceComboBox = New System.Windows.Forms.ComboBox()
        Me.Get2DDataButton = New System.Windows.Forms.Button()
        Me.Get2DDataOpenCurrentsFileDialog = New System.Windows.Forms.OpenFileDialog()
        CType(Me.AxCGSystemAXC, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.InitialGroupBox.SuspendLayout()
        CType(Me.SetOutputConnectionUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SetCurrentNumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl.SuspendLayout()
        Me.CurrentSourcesTabPage.SuspendLayout()
        Me.Get2DDataTabPage.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'StartupButton
        '
        Me.StartupButton.Location = New System.Drawing.Point(32, 19)
        Me.StartupButton.Name = "StartupButton"
        Me.StartupButton.TabIndex = 0
        Me.StartupButton.Text = "Startup"
        '
        'AxCGSystemAXC
        '
        Me.AxCGSystemAXC.Enabled = True
        Me.AxCGSystemAXC.Location = New System.Drawing.Point(440, 8)
        Me.AxCGSystemAXC.Name = "AxCGSystemAXC"
        Me.AxCGSystemAXC.OcxState = CType(resources.GetObject("AxCGSystemAXC.OcxState"), System.Windows.Forms.AxHost.State)
        Me.AxCGSystemAXC.Size = New System.Drawing.Size(104, 32)
        Me.AxCGSystemAXC.TabIndex = 1
        '
        'SetupButton
        '
        Me.SetupButton.Location = New System.Drawing.Point(32, 51)
        Me.SetupButton.Name = "SetupButton"
        Me.SetupButton.TabIndex = 0
        Me.SetupButton.Text = "Setup"
        '
        'LaserTypeTextBox
        '
        Me.LaserTypeTextBox.Location = New System.Drawing.Point(120, 59)
        Me.LaserTypeTextBox.Name = "LaserTypeTextBox"
        Me.LaserTypeTextBox.Size = New System.Drawing.Size(120, 20)
        Me.LaserTypeTextBox.TabIndex = 2
        Me.LaserTypeTextBox.Text = "DSDBR01"
        '
        'LaserIDTextBox
        '
        Me.LaserIDTextBox.Location = New System.Drawing.Point(248, 59)
        Me.LaserIDTextBox.Name = "LaserIDTextBox"
        Me.LaserIDTextBox.Size = New System.Drawing.Size(120, 20)
        Me.LaserIDTextBox.TabIndex = 2
        Me.LaserIDTextBox.Text = "stub"
        '
        'DateTimeStampTextBox
        '
        Me.DateTimeStampTextBox.Location = New System.Drawing.Point(376, 59)
        Me.DateTimeStampTextBox.Name = "DateTimeStampTextBox"
        Me.DateTimeStampTextBox.Size = New System.Drawing.Size(120, 20)
        Me.DateTimeStampTextBox.TabIndex = 2
        Me.DateTimeStampTextBox.Text = "20040816225938"
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(120, 35)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(96, 16)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Laser Type"
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(248, 35)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(96, 16)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Laser ID"
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(376, 35)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(96, 16)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Date/Time Stamp"
        '
        'ShutdownButton
        '
        Me.ShutdownButton.Location = New System.Drawing.Point(32, 83)
        Me.ShutdownButton.Name = "ShutdownButton"
        Me.ShutdownButton.TabIndex = 0
        Me.ShutdownButton.Text = "Shutdown"
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(32, 24)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(96, 16)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "System State"
        '
        'SystemStateTextBox
        '
        Me.SystemStateTextBox.Location = New System.Drawing.Point(128, 21)
        Me.SystemStateTextBox.Name = "SystemStateTextBox"
        Me.SystemStateTextBox.ReadOnly = True
        Me.SystemStateTextBox.Size = New System.Drawing.Size(120, 20)
        Me.SystemStateTextBox.TabIndex = 2
        Me.SystemStateTextBox.Text = "Instantiated"
        '
        'InitialGroupBox
        '
        Me.InitialGroupBox.Controls.AddRange(New System.Windows.Forms.Control() {Me.LaserIDTextBox, Me.DateTimeStampTextBox, Me.Label2, Me.Label3, Me.Label1, Me.SetupButton, Me.ShutdownButton, Me.StartupButton, Me.LaserTypeTextBox})
        Me.InitialGroupBox.Location = New System.Drawing.Point(16, 56)
        Me.InitialGroupBox.Name = "InitialGroupBox"
        Me.InitialGroupBox.Size = New System.Drawing.Size(528, 120)
        Me.InitialGroupBox.TabIndex = 4
        Me.InitialGroupBox.TabStop = False
        '
        'GetOutputConnectionButton
        '
        Me.GetOutputConnectionButton.Location = New System.Drawing.Point(16, 104)
        Me.GetOutputConnectionButton.Name = "GetOutputConnectionButton"
        Me.GetOutputConnectionButton.Size = New System.Drawing.Size(160, 23)
        Me.GetOutputConnectionButton.TabIndex = 2
        Me.GetOutputConnectionButton.Text = "Get Output Connection"
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(24, 52)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(80, 16)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "Module Name"
        '
        'CurrentSourceComboBox
        '
        Me.CurrentSourceComboBox.Items.AddRange(New Object() {"Gain", "SOA", "Phase", "Rear", "LFS1", "LFS2", "LFS3", "LFS4", "LFS5", "LFS6", "LFS7", "LFS8"})
        Me.CurrentSourceComboBox.Location = New System.Drawing.Point(112, 48)
        Me.CurrentSourceComboBox.Name = "CurrentSourceComboBox"
        Me.CurrentSourceComboBox.Size = New System.Drawing.Size(128, 21)
        Me.CurrentSourceComboBox.TabIndex = 0
        '
        'SetOutputConnectionButton
        '
        Me.SetOutputConnectionButton.Location = New System.Drawing.Point(16, 136)
        Me.SetOutputConnectionButton.Name = "SetOutputConnectionButton"
        Me.SetOutputConnectionButton.Size = New System.Drawing.Size(160, 24)
        Me.SetOutputConnectionButton.TabIndex = 2
        Me.SetOutputConnectionButton.Text = "Set Output Connection"
        '
        'OutputConnectionTextBox
        '
        Me.OutputConnectionTextBox.Location = New System.Drawing.Point(192, 104)
        Me.OutputConnectionTextBox.Name = "OutputConnectionTextBox"
        Me.OutputConnectionTextBox.Size = New System.Drawing.Size(32, 20)
        Me.OutputConnectionTextBox.TabIndex = 3
        Me.OutputConnectionTextBox.Text = ""
        '
        'SetOutputConnectionUpDown
        '
        Me.SetOutputConnectionUpDown.Location = New System.Drawing.Point(192, 136)
        Me.SetOutputConnectionUpDown.Maximum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.SetOutputConnectionUpDown.Name = "SetOutputConnectionUpDown"
        Me.SetOutputConnectionUpDown.Size = New System.Drawing.Size(48, 20)
        Me.SetOutputConnectionUpDown.TabIndex = 4
        '
        'SetCurrentButton
        '
        Me.SetCurrentButton.Location = New System.Drawing.Point(264, 136)
        Me.SetCurrentButton.Name = "SetCurrentButton"
        Me.SetCurrentButton.Size = New System.Drawing.Size(160, 24)
        Me.SetCurrentButton.TabIndex = 2
        Me.SetCurrentButton.Text = "Set Current"
        '
        'GetCurrentTextBox
        '
        Me.GetCurrentTextBox.Location = New System.Drawing.Point(440, 104)
        Me.GetCurrentTextBox.Name = "GetCurrentTextBox"
        Me.GetCurrentTextBox.Size = New System.Drawing.Size(32, 20)
        Me.GetCurrentTextBox.TabIndex = 3
        Me.GetCurrentTextBox.Text = ""
        '
        'GetCurrentButton
        '
        Me.GetCurrentButton.Location = New System.Drawing.Point(264, 104)
        Me.GetCurrentButton.Name = "GetCurrentButton"
        Me.GetCurrentButton.Size = New System.Drawing.Size(160, 23)
        Me.GetCurrentButton.TabIndex = 2
        Me.GetCurrentButton.Text = "Get Current"
        '
        'SetCurrentNumericUpDown
        '
        Me.SetCurrentNumericUpDown.Location = New System.Drawing.Point(440, 136)
        Me.SetCurrentNumericUpDown.Maximum = New Decimal(New Integer() {300, 0, 0, 0})
        Me.SetCurrentNumericUpDown.Name = "SetCurrentNumericUpDown"
        Me.SetCurrentNumericUpDown.Size = New System.Drawing.Size(48, 20)
        Me.SetCurrentNumericUpDown.TabIndex = 4
        '
        'TabControl
        '
        Me.TabControl.Controls.AddRange(New System.Windows.Forms.Control() {Me.CurrentSourcesTabPage, Me.Get2DDataTabPage})
        Me.TabControl.Location = New System.Drawing.Point(16, 192)
        Me.TabControl.Name = "TabControl"
        Me.TabControl.SelectedIndex = 0
        Me.TabControl.Size = New System.Drawing.Size(536, 304)
        Me.TabControl.TabIndex = 6
        '
        'CurrentSourcesTabPage
        '
        Me.CurrentSourcesTabPage.Controls.AddRange(New System.Windows.Forms.Control() {Me.SetOutputConnectionButton, Me.SetCurrentNumericUpDown, Me.Label5, Me.GetCurrentTextBox, Me.CurrentSourceComboBox, Me.OutputConnectionTextBox, Me.SetCurrentButton, Me.GetCurrentButton, Me.GetOutputConnectionButton, Me.SetOutputConnectionUpDown})
        Me.CurrentSourcesTabPage.Location = New System.Drawing.Point(4, 22)
        Me.CurrentSourcesTabPage.Name = "CurrentSourcesTabPage"
        Me.CurrentSourcesTabPage.Size = New System.Drawing.Size(528, 278)
        Me.CurrentSourcesTabPage.TabIndex = 0
        Me.CurrentSourcesTabPage.Text = "Current Sources"
        '
        'Get2DDataTabPage
        '
        Me.Get2DDataTabPage.Controls.AddRange(New System.Windows.Forms.Control() {Me.GroupBox1, Me.Get2DDataOpenCurrentsFileButton, Me.Get2DDataCurrentsFileTextBox, Me.Label7, Me.Label6, Me.Get2DDataCurrentSourceComboBox, Me.Get2DDataButton})
        Me.Get2DDataTabPage.Location = New System.Drawing.Point(4, 22)
        Me.Get2DDataTabPage.Name = "Get2DDataTabPage"
        Me.Get2DDataTabPage.Size = New System.Drawing.Size(528, 278)
        Me.Get2DDataTabPage.TabIndex = 1
        Me.Get2DDataTabPage.Text = "Get2DData"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.AddRange(New System.Windows.Forms.Control() {Me.Label12, Me.Label9, Me.Get2DDataNumSetpointsTextBox, Me.Get2DDataMinCurrentTextBox, Me.Label8, Me.Get2DDataMaxCurrentTextBox, Me.Label10, Me.Label11})
        Me.GroupBox1.Location = New System.Drawing.Point(32, 96)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(472, 100)
        Me.GroupBox1.TabIndex = 10
        Me.GroupBox1.TabStop = False
        '
        'Label12
        '
        Me.Label12.Location = New System.Drawing.Point(32, 32)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(80, 16)
        Me.Label12.TabIndex = 4
        Me.Label12.Text = "Num Setpoints"
        '
        'Label9
        '
        Me.Label9.Location = New System.Drawing.Point(216, 64)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(80, 16)
        Me.Label9.TabIndex = 4
        Me.Label9.Text = "Max Current"
        '
        'Get2DDataNumSetpointsTextBox
        '
        Me.Get2DDataNumSetpointsTextBox.Location = New System.Drawing.Point(112, 32)
        Me.Get2DDataNumSetpointsTextBox.Name = "Get2DDataNumSetpointsTextBox"
        Me.Get2DDataNumSetpointsTextBox.ReadOnly = True
        Me.Get2DDataNumSetpointsTextBox.Size = New System.Drawing.Size(40, 20)
        Me.Get2DDataNumSetpointsTextBox.TabIndex = 8
        Me.Get2DDataNumSetpointsTextBox.Text = ""
        '
        'Get2DDataMinCurrentTextBox
        '
        Me.Get2DDataMinCurrentTextBox.Location = New System.Drawing.Point(112, 62)
        Me.Get2DDataMinCurrentTextBox.Name = "Get2DDataMinCurrentTextBox"
        Me.Get2DDataMinCurrentTextBox.ReadOnly = True
        Me.Get2DDataMinCurrentTextBox.Size = New System.Drawing.Size(40, 20)
        Me.Get2DDataMinCurrentTextBox.TabIndex = 8
        Me.Get2DDataMinCurrentTextBox.Text = ""
        '
        'Label8
        '
        Me.Label8.Location = New System.Drawing.Point(32, 64)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(80, 16)
        Me.Label8.TabIndex = 4
        Me.Label8.Text = "Min Current"
        '
        'Get2DDataMaxCurrentTextBox
        '
        Me.Get2DDataMaxCurrentTextBox.Location = New System.Drawing.Point(296, 62)
        Me.Get2DDataMaxCurrentTextBox.Name = "Get2DDataMaxCurrentTextBox"
        Me.Get2DDataMaxCurrentTextBox.ReadOnly = True
        Me.Get2DDataMaxCurrentTextBox.Size = New System.Drawing.Size(40, 20)
        Me.Get2DDataMaxCurrentTextBox.TabIndex = 8
        Me.Get2DDataMaxCurrentTextBox.Text = ""
        '
        'Label10
        '
        Me.Label10.Location = New System.Drawing.Point(152, 64)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(24, 16)
        Me.Label10.TabIndex = 9
        Me.Label10.Text = "mA"
        '
        'Label11
        '
        Me.Label11.Location = New System.Drawing.Point(336, 64)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(24, 16)
        Me.Label11.TabIndex = 9
        Me.Label11.Text = "mA"
        '
        'Get2DDataOpenCurrentsFileButton
        '
        Me.Get2DDataOpenCurrentsFileButton.Location = New System.Drawing.Point(475, 58)
        Me.Get2DDataOpenCurrentsFileButton.Name = "Get2DDataOpenCurrentsFileButton"
        Me.Get2DDataOpenCurrentsFileButton.Size = New System.Drawing.Size(24, 18)
        Me.Get2DDataOpenCurrentsFileButton.TabIndex = 6
        Me.Get2DDataOpenCurrentsFileButton.Text = "..."
        '
        'Get2DDataCurrentsFileTextBox
        '
        Me.Get2DDataCurrentsFileTextBox.Location = New System.Drawing.Point(120, 56)
        Me.Get2DDataCurrentsFileTextBox.Name = "Get2DDataCurrentsFileTextBox"
        Me.Get2DDataCurrentsFileTextBox.Size = New System.Drawing.Size(352, 20)
        Me.Get2DDataCurrentsFileTextBox.TabIndex = 5
        Me.Get2DDataCurrentsFileTextBox.Text = ""
        '
        'Label7
        '
        Me.Label7.Location = New System.Drawing.Point(40, 58)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(80, 16)
        Me.Label7.TabIndex = 4
        Me.Label7.Text = "Currents File"
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(40, 26)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(80, 16)
        Me.Label6.TabIndex = 3
        Me.Label6.Text = "Module Name"
        '
        'Get2DDataCurrentSourceComboBox
        '
        Me.Get2DDataCurrentSourceComboBox.Items.AddRange(New Object() {"Gain", "SOA", "Phase", "Rear", "LFS1", "LFS2", "LFS3", "LFS4", "LFS5", "LFS6", "LFS7", "LFS8"})
        Me.Get2DDataCurrentSourceComboBox.Location = New System.Drawing.Point(120, 24)
        Me.Get2DDataCurrentSourceComboBox.Name = "Get2DDataCurrentSourceComboBox"
        Me.Get2DDataCurrentSourceComboBox.Size = New System.Drawing.Size(128, 21)
        Me.Get2DDataCurrentSourceComboBox.TabIndex = 2
        '
        'Get2DDataButton
        '
        Me.Get2DDataButton.Location = New System.Drawing.Point(216, 232)
        Me.Get2DDataButton.Name = "Get2DDataButton"
        Me.Get2DDataButton.TabIndex = 0
        Me.Get2DDataButton.Text = "Get 2D Data"
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(568, 509)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.TabControl, Me.InitialGroupBox, Me.AxCGSystemAXC, Me.Label4, Me.SystemStateTextBox})
        Me.Name = "Form1"
        Me.Text = "Test Client"
        CType(Me.AxCGSystemAXC, System.ComponentModel.ISupportInitialize).EndInit()
        Me.InitialGroupBox.ResumeLayout(False)
        CType(Me.SetOutputConnectionUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SetCurrentNumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl.ResumeLayout(False)
        Me.CurrentSourcesTabPage.ResumeLayout(False)
        Me.Get2DDataTabPage.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    'Globals
    Dim connectedModulesList() As String

    Dim get2DDataCurrents() As Double

#Region "Window Events"

    Private Sub StartupButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles StartupButton.Click
        Try
            AxCGSystemAXC.startup()

        Catch ex As Exception
            MsgBox(ex.Message.ToString)
        End Try

        stateChanged()
    End Sub

    Private Sub SetupButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SetupButton.Click

        Dim laserType As String = LaserTypeTextBox.Text.ToString
        Dim laserID As String = LaserIDTextBox.Text.ToString
        Dim dateTimeStamp As String = DateTimeStampTextBox.Text.ToString

        Try
            AxCGSystemAXC.setup(laserType, laserID, dateTimeStamp)
        Catch ex As Exception
            MsgBox(ex.Message.ToString)
        End Try

        stateChanged()
    End Sub

    Private Sub ShutdownButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ShutdownButton.Click
        Try
            AxCGSystemAXC.shutdown()
        Catch ex As Exception
            MsgBox(ex.Message.ToString)
        End Try

        stateChanged()
    End Sub

    Private Sub GetOutputConnectionButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GetOutputConnectionButton.Click
        updateOutputConnection()
    End Sub

    Private Sub SetOutputConnectionButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SetOutputConnectionButton.Click
        Dim outputConnection As Short

        Try
            AxCGSystemAXC.setOutputConnection(CurrentSourceComboBox.Text, SetOutputConnectionUpDown.Text)

        Catch ex As Exception
            MsgBox(ex.Message.ToString)
        End Try

    End Sub

    Private Sub CurrentSourceComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CurrentSourceComboBox.SelectedIndexChanged
        updateOutputConnection()
        updateCurrentSettings()
    End Sub

    Private Sub GetCurrentButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GetCurrentButton.Click
        updateCurrent()
    End Sub

    Private Sub SetCurrentButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SetCurrentButton.Click
        Try

            Dim current As Double = SetCurrentNumericUpDown.Text

            AxCGSystemAXC.setCurrent(CurrentSourceComboBox.Text, current)

            GetCurrentTextBox.Text = SetCurrentNumericUpDown.Text
        Catch ex As Exception
            MsgBox(ex.Message.ToString)
        End Try
    End Sub

    Private Sub Get2DDataButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Get2DDataButton.Click

        Try

            Dim moduleName As String = Get2DDataCurrentSourceComboBox.Text
            Dim msPerPoint As Short = 0
            Dim dateTimeStamp As String = DateTimeStampTextBox.Text
            Dim resultsDir As String = ""

            Dim currents() As Object

            ReDim currents(get2DDataCurrents.GetLength(0))

            Dim i As Integer
            For i = 0 To get2DDataCurrents.GetLength(0) - 1
                currents(i) = get2DDataCurrents.GetValue(i)
            Next i


            'currents.VT_Type = VariantType.Array

            ' convert currents to variant array
            'AxCGSystemAXC.get2DData(moduleName, get2DDataCurrents, msPerPoint, resultsDir, dateTimeStamp)
            AxCGSystemAXC.get2DData(moduleName, currents, msPerPoint, 1000, resultsDir, dateTimeStamp)

        Catch ex As Exception
            MsgBox(ex.Message.ToString)
        End Try

        stateChanged()

    End Sub

    Private Sub Get2DDataOpenCurrentsFileButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Get2DDataOpenCurrentsFileButton.Click

        Get2DDataOpenCurrentsFileDialog().Filter = "csv files (*.CSV)|*.CSV"

        Dim ret As DialogResult = DialogResult.None
        ret = Get2DDataOpenCurrentsFileDialog.ShowDialog()

        If ret = DialogResult.OK Then
            Dim fileName As String = Get2DDataOpenCurrentsFileDialog.FileName

            get2DDataCurrents = extractCurrentsFromFile(fileName)

            Get2DDataCurrentsFileTextBox.Text = fileName
            Get2DDataNumSetpointsTextBox.Text = get2DDataCurrents.GetLength(0)
            Get2DDataMinCurrentTextBox.Text = minOf(get2DDataCurrents)
            Get2DDataMaxCurrentTextBox.Text = maxOf(get2DDataCurrents)


        End If
    End Sub

#End Region

    Private Sub stateChanged()
        Dim state As String = "" ' type mismatch on getState call if this is not initialised to ""
        Dim omLoaded As Short
        Dim smFoundCount As Short
        Dim smLoadedCount As Short
        Dim ituOPEstimateCount As Short
        Dim ituOPCount As Short

        Try
            AxCGSystemAXC.getState(state, omLoaded, smFoundCount, smLoadedCount, ituOPEstimateCount, ituOPCount)

            SystemStateTextBox.Text = state

        Catch ex As Exception
            MsgBox(ex.Message.ToString)
        End Try
    End Sub

    Private Sub updateOutputConnection()

        If (SystemStateTextBox.Text = "Setup" Or SystemStateTextBox.Text = "Complete") Then

            Dim outputConnection As Short

            Try
                AxCGSystemAXC.getOutputConnection(CurrentSourceComboBox.Text, outputConnection)

                OutputConnectionTextBox.Text = outputConnection.ToString

            Catch ex As Exception
                MsgBox(ex.Message.ToString)
            End Try

        End If
    End Sub

    Private Sub updateCurrentSettings()

        If (SystemStateTextBox.Text = "Setup" Or SystemStateTextBox.Text = "Complete") Then
            updateCurrent()
            SetCurrentNumericUpDown.Text = GetCurrentTextBox.Text
        End If
    End Sub

    Private Sub updateCurrent()

        If (SystemStateTextBox.Text = "Setup" Or SystemStateTextBox.Text = "Complete") Then

            Dim current As Double = 0

            Try
                AxCGSystemAXC.getCurrent(CurrentSourceComboBox.Text, current)

                GetCurrentTextBox.Text = current.ToString

            Catch ex As Exception
                MsgBox(ex.Message.ToString)
            End Try

        End If
    End Sub

    Private Function extractCurrentsFromFile(ByVal fileName As String) As Double()
        Dim currents As Double()

        Try
            FileOpen(1, fileName, OpenMode.Input)
        Catch fileEx As Exception
            MsgBox(fileEx.Message.ToString)
        End Try

        Dim fileLine As String = ""

        Do Until EOF(1)
            fileLine = LineInput(1)
            Dim fileLineArray() As String = fileLine.Split(",")

            Dim currentsLength As Integer = 0
            If Not currents Is Nothing Then
                currentsLength = currents.GetLength(0)
            End If

            'Re Dimension the array
            ReDim Preserve currents(currentsLength + (fileLineArray.GetLength(0) - 1))

            Dim i As Integer = 0
            For i = 0 To fileLineArray.GetLength(0) - 1
                currents(currentsLength + i) = Val(fileLineArray.GetValue(i))
            Next
        Loop

        Return currents

    End Function

    Private Function maxOf(ByVal doubleValues() As Double) As Double

        Dim maxValue As Double = 0

        If Not doubleValues Is Nothing Then
            Dim i As Integer
            For i = 0 To doubleValues.GetLength(0) - 1
                If (doubleValues(i) > maxValue) Then
                    maxValue = doubleValues(i)
                End If
            Next
        End If

        Return maxValue
    End Function

    Private Function minOf(ByVal doubleValues() As Double) As Double

        Dim minValue As Double = 0

        If Not doubleValues Is Nothing Then
            Dim i As Integer
            For i = 0 To doubleValues.GetLength(0) - 1
                If (doubleValues(i) < minValue) Then
                    minValue = doubleValues(i)
                End If
            Next
        End If

        Return minValue
    End Function

End Class

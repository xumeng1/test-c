// CGVersionInfoDlg.cpp : implementation file
//

#include "stdafx.h"
#include "VersionInfoDialog.h"
#include "CGVersionInfoDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CCGVersionInfoDlg dialog



CCGVersionInfoDlg::CCGVersionInfoDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCGVersionInfoDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CCGVersionInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CLOSEGRIDSYSTEMACTRL1, closegridsystem_axc);
	DDX_Control(pDX, IDC_EDIT1, _product_name);
	DDX_Control(pDX, IDC_EDIT2, _release_no);
	DDX_Control(pDX, IDC_EDIT3, _copyright_notice);
	DDX_Control(pDX, IDC_EDIT4, _label);
	DDX_Control(pDX, IDC_EDIT5, _label_info);
	DDX_Control(pDX, IDC_EDIT6, _label_comment);
	DDX_Control(pDX, IDC_EDIT7, _build_configuration);
	DDX_Control(pDX, IDC_EDIT8, _build_datetime);
	DDX_Control(pDX, IDC_EDIT9, _driver_version_info);
	DDX_Control(pDX, IDC_LOOPCOUNT_EDIT, _loop_count_CEdit);
}

BEGIN_MESSAGE_MAP(CCGVersionInfoDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_TESTBUTTON, OnBnClickedTestButton)
END_MESSAGE_MAP()


// CCGVersionInfoDlg message handlers

BOOL CCGVersionInfoDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon


	
	BSTR product_name_BSTR;
	BSTR release_no_BSTR;
	BSTR copyright_notice_BSTR;
	BSTR label_BSTR;
	BSTR label_info_BSTR;
	BSTR label_comment_BSTR;
	BSTR build_configuration_BSTR;
	BSTR build_datetime_BSTR;
	BSTR driver_version_info_BSTR;

	closegridsystem_axc.getVersionInfo( &product_name_BSTR,
										&release_no_BSTR,
										&copyright_notice_BSTR,
										&label_BSTR,
										&label_info_BSTR,
										&label_comment_BSTR,
										&build_configuration_BSTR,
										&build_datetime_BSTR,
										&driver_version_info_BSTR);

	CString product_name_CString( product_name_BSTR );
	CString release_no_CString( release_no_BSTR );
	CString copyright_notice_CString( copyright_notice_BSTR );
	CString label_CString( label_BSTR );
	CString label_info_CString( label_info_BSTR );
	CString label_comment_CString( label_comment_BSTR );
	label_comment_CString.Replace("\n","\r\n");
	CString build_configuration_CString( build_configuration_BSTR );
	CString build_datetime_CString( build_datetime_BSTR );
	CString driver_version_info_CString( driver_version_info_BSTR );
	driver_version_info_CString.Replace("\n","\r\n");

	_product_name.SetWindowText( product_name_CString );
	_release_no.SetWindowText( release_no_CString );
	_copyright_notice.SetWindowText( copyright_notice_CString );
	_label.SetWindowText( label_CString );
	_label_info.SetWindowText( label_info_CString );
	_label_comment.SetWindowText( label_comment_CString );
	_build_configuration.SetWindowText( build_configuration_CString );
	_build_datetime.SetWindowText( build_datetime_CString );
	_driver_version_info.SetWindowText( driver_version_info_CString );

	SysFreeString( product_name_BSTR );
	SysFreeString( release_no_BSTR );
	SysFreeString( copyright_notice_BSTR );
	SysFreeString( label_BSTR );
	SysFreeString( label_info_BSTR );
	SysFreeString( label_comment_BSTR );
	SysFreeString( build_configuration_BSTR );
	SysFreeString( build_datetime_BSTR );
	SysFreeString( driver_version_info_BSTR );


	_loop_count = 0;
	char numbuf[100];
	sprintf( numbuf, "%d", _loop_count );
	_loop_count_CEdit.SetWindowText(numbuf);

	//closegridsystem_axc.startup();

	//BSTR date_time_stamp_BSTR;
	//closegridsystem_axc.setup("DSDBR01","stub",&date_time_stamp_BSTR);
	//SysFreeString( date_time_stamp_BSTR );


	//closegridsystem_axc.loadOverallMap(
	//	"C:\\Program Files\\PXIT\\CLose-Grid 4.0\\SampleResults",
	//	"DSDBR01",
	//	"p32",
	//	"20041028115910",
	//	"R" );

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CCGVersionInfoDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CCGVersionInfoDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CCGVersionInfoDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

//
//void CCGVersionInfoDlg::OnBnClickedTestButton()
//{
//	if( _loop_count == 0 )
//	{
//		closegridsystem_axc.startup();
//		_loop_count = 1;
//	}
//	else
//	{
//		closegridsystem_axc.shutdown();
//		_loop_count = 0;
//	}
//
//}


void CCGVersionInfoDlg::waitWhileBusy()
{
	CString system_state;
	BSTR system_state_BSTR;
	short overall_map_loaded;
	short supermodes_found_count;
	short supermode_map_loaded_count;
	short ITUOP_estimate_count;
	short ITUOP_count;

	do
	{
		TRY
		{
			closegridsystem_axc.getState(
				&system_state_BSTR,
				&overall_map_loaded,
				&supermodes_found_count,
				&supermode_map_loaded_count,
				&ITUOP_estimate_count,
				&ITUOP_count );

			system_state = system_state_BSTR;
			SysFreeString( system_state_BSTR );

		}
		CATCH( COleDispatchException, pEx )
		{
			system_state = system_state_BSTR;
			SysFreeString( system_state_BSTR );
		}
		END_CATCH

		Sleep(1000);
	}
	while( system_state == CString("Busy") );
}

void CCGVersionInfoDlg::OnBnClickedTestButton()
{
	_loop_count = 0;


	//#ifdef _DEBUG
	//_CrtMemState s1, s2, s3;
	//#endif


	while( _loop_count < 1000 ) 
	{

		CString results_dir("C:\\DSDBR_results");
		if( results_dir.Right(1) != _T("\\") )
			results_dir += _T("\\");

		// build a string with wildcards
		CString str_wildcard = results_dir + _T("*.*");

		// start working for files
		CFileFind file_finder;
		BOOL bWorking = file_finder.FindFile(str_wildcard);

		while (bWorking)
		{
			bWorking = file_finder.FindNextFile();
			CString file_path = file_finder.GetFilePath();

			remove(file_path.GetBuffer());
		}

		file_finder.Close();


		closegridsystem_axc.startup();

		BSTR date_time_stamp_BSTR;
		closegridsystem_axc.setup("DSDBR01","stub",&date_time_stamp_BSTR);
		SysFreeString( date_time_stamp_BSTR );

		closegridsystem_axc.startLaserScreeningOverallMap();

		waitWhileBusy();

		//TRACE("\nsystem_state = %s\n",system_state.GetBuffer());
		//TRACE("\noverall_map_loaded = %d\n",overall_map_loaded);
		//TRACE("\nsupermodes_found_count = %d\n",supermodes_found_count);
		//TRACE("\nsupermode_map_loaded_count = %d\n",supermode_map_loaded_count);
		//TRACE("\nITUOP_estimate_count = %d\n",ITUOP_estimate_count);
		//TRACE("\nITUOP_count = %d\n",ITUOP_count);

		closegridsystem_axc.loadOverallMap(
			"C:\\Program Files\\PXIT\\CLose-Grid 4.0\\SampleResults",
			"DSDBR01",
			"p32",
			"20041028115910",
			"R" );

		closegridsystem_axc.startLaserScreeningSMMap(0);

		waitWhileBusy();

		closegridsystem_axc.loadSMMap(
			"C:\\Program Files\\PXIT\\CLose-Grid 4.0\\SampleResults",
			"DSDBR01",
			"p32",
			"20041028115910",
			0,
			"P" );

		closegridsystem_axc.startLaserScreeningSMMap(1);

		waitWhileBusy();

		closegridsystem_axc.loadSMMap(
			"C:\\Program Files\\PXIT\\CLose-Grid 4.0\\SampleResults",
			"DSDBR01",
			"p32",
			"20041028115910",
			1,
			"P" );


		closegridsystem_axc.startLaserScreeningSMMap(2);

		waitWhileBusy();

		closegridsystem_axc.loadSMMap(
			"C:\\Program Files\\PXIT\\CLose-Grid 4.0\\SampleResults",
			"DSDBR01",
			"p32",
			"20041028115910",
			2,
			"P" );


		closegridsystem_axc.startLaserScreeningSMMap(3);

		waitWhileBusy();

		closegridsystem_axc.loadSMMap(
			"C:\\Program Files\\PXIT\\CLose-Grid 4.0\\SampleResults",
			"DSDBR01",
			"p32",
			"20041028115910",
			3,
			"P" );


		closegridsystem_axc.startLaserScreeningSMMap(4);

		waitWhileBusy();

		closegridsystem_axc.loadSMMap(
			"C:\\Program Files\\PXIT\\CLose-Grid 4.0\\SampleResults",
			"DSDBR01",
			"p32",
			"20041028115910",
			4,
			"P" );


		closegridsystem_axc.startLaserScreeningSMMap(5);

		waitWhileBusy();

		closegridsystem_axc.loadSMMap(
			"C:\\Program Files\\PXIT\\CLose-Grid 4.0\\SampleResults",
			"DSDBR01",
			"p32",
			"20041028115910",
			5,
			"P" );


		closegridsystem_axc.shutdown();


		////#ifdef _DEBUG

		////_CrtMemCheckpoint( &s2 );
		//////_CrtMemDumpStatistics( &s2 );

		////if ( _loop_count > 0 && _CrtMemDifference( &s3, &s1, &s2) )
		////{
		////	TRACE("\n_loop_count = %d\n",_loop_count);
		////	_CrtMemDumpStatistics( &s3 );
		////}

		////_CrtMemCheckpoint( &s1 );

		////#endif

		char numbuf[100];
		_loop_count++;
		sprintf( numbuf, "%d", _loop_count );
		_loop_count_CEdit.SetWindowText(numbuf);

		// Invalidate window so entire client area 
		// is redrawn when UpdateWindow is called.
		Invalidate();   

		// Update Window to cause View to redraw.
		UpdateWindow();

		Sleep(2000);

	}

}
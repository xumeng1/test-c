// CGVersionInfoDlg.h : header file
//

#pragma once
#include "closegridsystemactrl1.h"
#include "afxwin.h"

// CCGVersionInfoDlg dialog
class CCGVersionInfoDlg : public CDialog
{
// Construction
public:
	CCGVersionInfoDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_VERSIONINFODIALOG_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CClosegridsystemactrl1 closegridsystem_axc;
	CEdit _product_name;
	CEdit _release_no;
	CEdit _copyright_notice;
	CEdit _label;
	CEdit _label_info;
	CEdit _label_comment;
	CEdit _build_configuration;
	CEdit _build_datetime;
	CEdit _driver_version_info;

	afx_msg void OnBnClickedTestButton();
	afx_msg void waitWhileBusy();
	CEdit _loop_count_CEdit;
	int _loop_count;
};

// Copyright (c) 2004 PXIT. All Rights Reserved
//
// FileName    : DSDBRFrontCurrents.cpp
// Description : Definition of CDSDBRFrontCurrents class
//               Contains DSDBR Front Currents
//               and utility routines
//
// Rev#  | Date          | Author               | Description of change
// --------------------------------------------------------------
// 0.1   | 09 Sept 2004  | Frank D'Arcy         | Initial Draft
//

#include "StdAfx.h"
#include "DSDBRFrontCurrents.h"
#include "defaults.h"
#include <fstream>
#include <direct.h>
#include "CGRegValues.h"



CDSDBRFrontCurrents::CDSDBRFrontCurrents(void)
{
	clear();
}

CDSDBRFrontCurrents::~CDSDBRFrontCurrents(void)
{
	clear();
}

void
CDSDBRFrontCurrents::
clear()
{
	_total_length = 0;
	_submap_length.clear();
	_submap_start_index.clear();
	_currents.clear();
	_abs_filepath.Empty();
}

short
CDSDBRFrontCurrents::
pair_number( long index )
{
	short front_pair_number = 0;

	if( index >= _submap_start_index[0] && index < _submap_start_index[1] )
		front_pair_number = 1;
	else
	if( index >= _submap_start_index[1] && index < _submap_start_index[2] )
		front_pair_number = 2;
	else
	if( index >= _submap_start_index[2] && index < _submap_start_index[3] )
		front_pair_number = 3;
	else
	if( index >= _submap_start_index[3] && index < _submap_start_index[4] )
		front_pair_number = 4;
	else
	if( index >= _submap_start_index[4] && index < _submap_start_index[5] )
		front_pair_number = 5;
	else
	if( index >= _submap_start_index[5] && index < _submap_start_index[6] )
		front_pair_number = 6;
	else
	if( index >= _submap_start_index[6] && index < _total_length )
		front_pair_number = 7;

	return front_pair_number;
}

long
CDSDBRFrontCurrents::
index_in_submap( long index )
{
	return index - _submap_start_index[ pair_number( index ) - 1 ];
}

CString
CDSDBRFrontCurrents::getConstantModuleName( short pair_num )
{
	CString constant_module_name;

	switch( pair_num )
	{
	case 1:
		constant_module_name = CG_REG->get_DSDBR01_CMDC_front1_module_name();
		break;
	case 2:
		constant_module_name = CG_REG->get_DSDBR01_CMDC_front2_module_name();
		break;
	case 3:
		constant_module_name = CG_REG->get_DSDBR01_CMDC_front3_module_name();
		break;
	case 4:
		constant_module_name = CG_REG->get_DSDBR01_CMDC_front4_module_name();
		break;
	case 5:
		constant_module_name = CG_REG->get_DSDBR01_CMDC_front5_module_name();
		break;
	case 6:
		constant_module_name = CG_REG->get_DSDBR01_CMDC_front6_module_name();
		break;
	case 7:
		constant_module_name = CG_REG->get_DSDBR01_CMDC_front7_module_name();
		break;
	}

	return constant_module_name;
}

CString
CDSDBRFrontCurrents::getNonConstantModuleName( short pair_num )
{
	CString constant_module_name;

	switch( pair_num )
	{
	case 1:
		constant_module_name = CG_REG->get_DSDBR01_CMDC_front2_module_name();
		break;
	case 2:
		constant_module_name = CG_REG->get_DSDBR01_CMDC_front3_module_name();
		break;
	case 3:
		constant_module_name = CG_REG->get_DSDBR01_CMDC_front4_module_name();
		break;
	case 4:
		constant_module_name = CG_REG->get_DSDBR01_CMDC_front5_module_name();
		break;
	case 5:
		constant_module_name = CG_REG->get_DSDBR01_CMDC_front6_module_name();
		break;
	case 6:
		constant_module_name = CG_REG->get_DSDBR01_CMDC_front7_module_name();
		break;
	case 7:
		constant_module_name = CG_REG->get_DSDBR01_CMDC_front8_module_name();
		break;
	}

	return constant_module_name;
}


double
CDSDBRFrontCurrents::getConstantCurrent( short pair_num, long sub_index )
{
	double constant_current = 0;
	long index = _submap_start_index[pair_num-1] + sub_index;

	switch( pair_num )
	{
	case 1:
		constant_current = _currents[index].I_front_1;
		break;
	case 2:
		constant_current = _currents[index].I_front_2;
		break;
	case 3:
		constant_current = _currents[index].I_front_3;
		break;
	case 4:
		constant_current = _currents[index].I_front_4;
		break;
	case 5:
		constant_current = _currents[index].I_front_5;
		break;
	case 6:
		constant_current = _currents[index].I_front_6;
		break;
	case 7:
		constant_current = _currents[index].I_front_7;
		break;
	}

	return constant_current;
}

double
CDSDBRFrontCurrents::getNonConstantCurrent( short pair_num, long sub_index )
{
	double non_constant_current = 0;
	long index = _submap_start_index[pair_num-1] + sub_index;

	switch( pair_num )
	{
	case 1:
		non_constant_current = _currents[index].I_front_2;
		break;
	case 2:
		non_constant_current = _currents[index].I_front_3;
		break;
	case 3:
		non_constant_current = _currents[index].I_front_4;
		break;
	case 4:
		non_constant_current = _currents[index].I_front_5;
		break;
	case 5:
		non_constant_current = _currents[index].I_front_6;
		break;
	case 6:
		non_constant_current = _currents[index].I_front_7;
		break;
	case 7:
		non_constant_current = _currents[index].I_front_8;
		break;
	}

	return non_constant_current;
}


std::vector<double>
CDSDBRFrontCurrents::
getNonConstantCurrentVector( short pair_num )
{
	std::vector<double> non_constant_current;
	non_constant_current.clear();


	for( long i = 0 ; i < _submap_length[pair_num-1]  ; i++ )
	{
		non_constant_current.push_back( getNonConstantCurrent( pair_num, i ) );
	}

	return non_constant_current;
}


bool
CDSDBRFrontCurrents::
isEmpty()
{
	bool rval = false;

	if( _currents.size() == 0 )
	{
		rval = true;
	}

	return rval;
}


CDSDBRFrontCurrents::rcode
CDSDBRFrontCurrents::
writeToFile( CString abs_filepath, bool overwrite )
{
	rcode rval = rcode::ok;
    long row_count = 0;

	CFileStatus status;

	// Check if file already exists and overwrite not specified

	if( CFile::GetStatus( abs_filepath, status ) && overwrite == false )
	{
		rval = rcode::file_already_exists;
	}
	else
	{

        // Ensure the directory exists
		int index_of_last_dirslash = abs_filepath.ReverseFind('\\');
		if( index_of_last_dirslash == -1 )
		{
			// '\\' not found => not an absolute path
			rval = rcode::could_not_open_file;
		}
		else
		{
			CString dir_name = abs_filepath;
			dir_name.Truncate(index_of_last_dirslash);

			int i = 0;
			CString token = dir_name.Tokenize( "\\/", i );
			CString new_dir = "";
			while ( token != "" )
			{
				if( new_dir.GetLength() == 0 )
				{
					new_dir = token;
				}
				else
				{
					new_dir = new_dir + "\\" + token;
				}
				CFileStatus status;
				//printf("\n %s  = %d", new_dir, CFile::GetStatus( new_dir, status ) );
				if( new_dir.GetLength() > 2 // skip drive name
				&& !CFile::GetStatus( new_dir, status ) )
				{ // directory does not exists => make it
					_mkdir( new_dir );
				}
				else
				{ // directory exists
				}
				//printf("\n %s  = %d", new_dir, CFile::GetStatus( new_dir, status ) );
	            
				token = dir_name.Tokenize( "\\/", i );
			}

			try
			{
				std::ofstream file_stream( abs_filepath );

				if( file_stream )
				{

					// First write header row
					file_stream << "Front Pair Number";
					file_stream << COMMA_CHAR;
					file_stream << "Constant Front Current [mA]";
					file_stream << COMMA_CHAR;
					file_stream << "Non-constant Front Current [mA]";
					file_stream << std::endl;

					row_count = 0;
					for( long i = 0 ; i < (long)(_currents.size()) ; i++ )
					{
						// First write data rows
						file_stream << _currents[i].front_pair_number;
						file_stream << COMMA_CHAR;


						switch( _currents[i].front_pair_number )
						{
						case 1:
							file_stream << _currents[i].I_front_1;
							file_stream << COMMA_CHAR;
							file_stream << _currents[i].I_front_2;
							break;
						case 2:
							file_stream << _currents[i].I_front_2;
							file_stream << COMMA_CHAR;
							file_stream << _currents[i].I_front_3;
							break;
						case 3:
							file_stream << _currents[i].I_front_3;
							file_stream << COMMA_CHAR;
							file_stream << _currents[i].I_front_4;
							break;
						case 4:
							file_stream << _currents[i].I_front_4;
							file_stream << COMMA_CHAR;
							file_stream << _currents[i].I_front_5;
							break;
						case 5:
							file_stream << _currents[i].I_front_5;
							file_stream << COMMA_CHAR;
							file_stream << _currents[i].I_front_6;
							break;
						case 6:
							file_stream << _currents[i].I_front_6;
							file_stream << COMMA_CHAR;
							file_stream << _currents[i].I_front_7;
							break;
						case 7:
							file_stream << _currents[i].I_front_7;
							file_stream << COMMA_CHAR;
							file_stream << _currents[i].I_front_8;
							break;
						}

						file_stream << std::endl;
						row_count++;
					}

					_abs_filepath = abs_filepath;

				}
				else
				{
					rval = rcode::could_not_open_file;
				}
			}
			catch(...)
			{
				rval = rcode::could_not_open_file;
			}
		}
	}

	return rval;
}

CDSDBRFrontCurrents::rcode
CDSDBRFrontCurrents::
readFromFile( CString abs_filepath )
{
	rcode rval = rcode::ok;

	clear();

    long row_count = 0;
    long first_line_col_count = 0;

	CFileStatus status;

    // Check file

	if( CFile::GetStatus( abs_filepath, status ) )
	{
		// File exists => attempt to read it's contents
		try
		{
			std::ifstream file_stream( abs_filepath );

			if( file_stream )
			{
				short current_submap = 0;
				long submap_length = 0;

				// File stream is open, read in header line, then ignore it!
				char map_buffer[MAX_LINE_LENGTH];
				*map_buffer = '\0';
				file_stream.getline( map_buffer, MAX_LINE_LENGTH );

				row_count = 0;
				while( !file_stream.eof() )
				{
					*map_buffer = '\0';
					file_stream.getline( map_buffer, MAX_LINE_LENGTH );

					int map_buffer_lenght = (int)strlen(map_buffer);
					if( map_buffer_lenght > 0 ) // there is something on the line
					{
						char *fp = strtok(map_buffer,DELIMITERS);

						short front_pair_number = 0;
						double constant_If = -1;
						double nonconstant_If = -1;

						if(fp) // read front_pair_number
						{
							front_pair_number = atoi(fp);
							fp = strtok(NULL,DELIMITERS);

							if( current_submap != front_pair_number )
							{
								if( current_submap+1 == front_pair_number )
								{
									// found transition between pairs of front sections

									if( current_submap != 0 )
									{
										_submap_length.push_back( submap_length );
									}
									submap_length = 0;
									current_submap = front_pair_number;
									_submap_start_index.push_back( row_count );

								}
								else
								{
									rval = rcode::file_format_invalid;
									break;
								}
							}
						}

						if(fp) // read constant_If
						{
							constant_If = atof(fp);
							fp = strtok(NULL,DELIMITERS);
						}

						if(fp) // read nonconstant_If
						{
							nonconstant_If = atof(fp);
							fp = strtok(NULL,DELIMITERS);
						}

						if( front_pair_number > 7 || front_pair_number < 1
						 || constant_If < 0 || nonconstant_If < 0 )
						{
							rval = rcode::file_format_invalid;
							break;
						}

						If_point new_If_point;
						new_If_point.front_pair_number = front_pair_number;
						new_If_point.I_front_1 = 0;
						new_If_point.I_front_2 = 0;
						new_If_point.I_front_3 = 0;
						new_If_point.I_front_4 = 0;
						new_If_point.I_front_5 = 0;
						new_If_point.I_front_6 = 0;
						new_If_point.I_front_7 = 0;
						new_If_point.I_front_8 = 0;

						switch( front_pair_number )
						{
						case 1:
							new_If_point.I_front_1 = constant_If;
							new_If_point.I_front_2 = nonconstant_If;
							break;
						case 2:
							new_If_point.I_front_2 = constant_If;
							new_If_point.I_front_3 = nonconstant_If;
							break;
						case 3:
							new_If_point.I_front_3 = constant_If;
							new_If_point.I_front_4 = nonconstant_If;
							break;
						case 4:
							new_If_point.I_front_4 = constant_If;
							new_If_point.I_front_5 = nonconstant_If;
							break;
						case 5:
							new_If_point.I_front_5 = constant_If;
							new_If_point.I_front_6 = nonconstant_If;
							break;
						case 6:
							new_If_point.I_front_6 = constant_If;
							new_If_point.I_front_7 = nonconstant_If;
							break;
						case 7:
							new_If_point.I_front_7 = constant_If;
							new_If_point.I_front_8 = nonconstant_If;
							break;
						}

						_currents.push_back( new_If_point );

						row_count++;
						submap_length++;
					}
				}

				// record length of final submap
				_submap_length.push_back( submap_length );
			}
			else
			{
        		rval = rcode::could_not_open_file;
			}
		}
		catch(...)
		{
			rval = rcode::could_not_open_file;
		}

	}
	else
	{
		rval = rcode::file_does_not_exist;
	}

    if( rval == rcode::ok
	 && ( row_count == 0
	   || _currents.size() != row_count
	   || _submap_length.size() != 7
	   || _submap_start_index.size() != 7 ) )
	{
		rval = rcode::file_format_invalid;
	}

	if( rval == rcode::ok )
	{
		_abs_filepath = abs_filepath;
		_total_length = row_count;
	}
	else
	{
		// something is wrong, clear everything!
		clear();
	}

	return rval;
}


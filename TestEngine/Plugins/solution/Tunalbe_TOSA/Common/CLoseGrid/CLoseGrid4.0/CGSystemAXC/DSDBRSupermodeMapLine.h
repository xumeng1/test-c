// Copyright (c) 2004 Tsunami Photonics Ltd. All Rights Reserved
//
// FileName    : DSDBRSupermodeMapLine.h
// Description : Declaration of CDSDBRSupermodeMapLine class
//               Represents a set of point on DSDBRSupermodeMapLine
//               eg. Longitudinal mode boundaries
//               or Longitudinal mode middle lines of frequency
//
// Rev#  | Date          | Author               | Description of change
// --------------------------------------------------------------
// 0.1   | 12 Oct 2004   | Frank D'Arcy         | Initial Draft
//

#pragma once

#include "defaults.h"
#include <vector>

class CDSDBRSupermodeMapLine {

public:

	typedef enum rcode
	{
		ok = 0,
		file_does_not_exist,
		file_already_exists,
		could_not_open_file,
		file_format_invalid,
		//files_not_matching_sizes,
		//corrupt_map_not_written_to_file,
		//existing_file_not_overwritten,
		//maps_not_loaded,
		//outside_map_dimensions,
		//next_pt_not_found,
		no_data,
		invalid_front_pair_number
		//no_supermodes_found
	};

	typedef struct point_type
	{
		long row;
		long col;
		double real_row;
		double real_col;
		short front_pair_number;
		double I_rear;
		double I_front_1;
		double I_front_2;
		double I_front_3;
		double I_front_4;
		double I_front_5;
		double I_front_6;
		double I_front_7;
		double I_front_8;
		double I_phase;
	};

	// constructor
	CDSDBRSupermodeMapLine();

	// destructor
    ~CDSDBRSupermodeMapLine();

	// clear all contents
	void clear();

	bool hasChangeOfFrontPairs();

	rcode getFrontPairNumbers(
		std::vector<short> &front_pair_numbers );

	rcode writeRowColToFile(
		CString abs_filepath,
		bool overwrite );

	rcode readRowColFromFile(
		CString abs_filepath );

	rcode writeCurrentsToFile(
		CString abs_filepath,
		bool overwrite );

	rcode readCurrentsFromFile(
		CString abs_filepath );

	std::vector<double> getRows();
	std::vector<double> getCols();

	std::vector<point_type> _points;

};


// Copyright (c) 2004 PXIT. All Rights Reserved
//
// FileName    : CGSystemErrors.h
// Description : Declaration of the error codes and  
//               error messages of the CGSystemAXC
//
// Rev#  | Date          | Author               | Description of change
// --------------------------------------------------------------
// 0.1   | 12 July 2004  | Frank D'Arcy         | Initial Draft
// 0.2   | 28 Sept 2004  | Frank D'Arcy         | Adding CGSYSTEM_FUNCTION_NOT_IMPLEMENTED
//

#ifndef __CGSYSTEM_ERRORS__H__
#define __CGSYSTEM_ERRORS__H__

#include <utility>
#include <string>

typedef std::pair<short, std::string> error_std_pair;

// Use these macros to get the error code or error message
// eg.  CGSYSTEM_ERROR_CODE( CGSYSTEM_UNKNOWN_ERROR )
// eg.  CGSYSTEM_ERROR_MESSAGE( CGSYSTEM_UNKNOWN_ERROR )
//
#define CGSYSTEM_ERROR_CODE( A ) error_std_pair(A).first
#define CGSYSTEM_ERROR_MESSAGE( A ) error_std_pair(A).second.c_str()

#define CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( A, B ) \
	A.first = error_std_pair(B).first; \
	A.second = error_std_pair(B).second;


// Use this macro to throw OLE Dispatch Exceptions
#define CGSYSTEM_THROW_EXCEPTION( A ) \
		SERVERINTERFACE->closeLogFile(); \
		AfxThrowOleDispatchException( \
			(WORD)(error_std_pair(A).first), \
			(LPCTSTR)(error_std_pair(A).second.c_str()) );

// or this one
#define CGSYSTEM_THROW_EXCEPTION_FROM_PAIR( A ) \
		SERVERINTERFACE->closeLogFile(); \
		AfxThrowOleDispatchException( \
			(WORD)(A.first), \
			(LPCTSTR)(A.second.c_str()) );


// Add errors to list below
#define CGSYSTEM_SUCCESS	0,	"Success"
#define CGSYSTEM_UNKNOWN_ERROR	1001,	"Unknown Error"
#define CGSYSTEM_INVALID_STATE_ERROR	1002,	"Function called is not allowed in the system's current state"
#define CGSYSTEM_NULL_PTR_ERROR	1003,	"A pointer argument passed a NULL pointer"
#define CGSYSTEM_INVALID_MODULE_NAME_ERROR	1004,	"Module name argument is invalid"
#define CGSYSTEM_NULL_VARIANT_ARG_ERROR	1005,	"A VARIANT argument contains NULL"
#define CGSYSTEM_EMPTY_VARIANT_ARG_ERROR	1006,	"A VARIANT argument contains an empty array"
#define CGSYSTEM_NOT1DDOUBLEARRAY_VARIANT_ARG_ERROR	1007,	"A VARIANT argument does not contain a 1-D array of doubles"
#define CGSYSTEM_UNKNOWN_STATE_ERROR	1008,	"The system is in an unknown state"
#define CGSYSTEM_EMPTY_STRING_ARG_ERROR	1009,	"A string argument is empty"
#define CGSYSTEM_DATETIMESTAMP_ERROR	1010,	"Date-Time Stamp argument is not of the correct format or valid date-time"
#define CGSYSTEM_DIRECTORY_ERROR	1011,	"Directory path argument does not exist or is not a directory"
#define CGSYSTEM_LASERTYPE_ERROR	1012,	"Laser Type argument is invalid"
#define CGSYSTEM_LASER_ID_ERROR	1013,	"Laser ID argument is invalid"
#define CGSYSTEM_FILE_DOES_NOT_EXIST_ERROR	1014,	"File(s) to open could not be found, check input arguments"
#define CGSYSTEM_FILEOPEN_ERROR	1015,	"An error occurred trying to open a file"
#define CGSYSTEM_INVALID_FILE_FORMAT_ERROR	1016,	"A file opened for reading was not in the correct format"
#define CGSYSTEM_FILES_NOT_MATCHING_SIZES_ERROR	1017,	"Files opened for reading are not the same size"
#define CGSYSTEM_VARIANT_CREATION_ERROR	1018,	"An error occurred trying to create a VARIANT to return, check VARIANT* argument"
#define CGSYSTEM_INVALID_MAP_RAMP_DIRECTION_ERROR	1019,	"The map ramp direction argument is an invalid value"
#define CGSYSTEM_INVALID_CHANNEL_ERROR	1020,	"The channel argument is an invalid value"
#define CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR	1021,	"The function called cannot be used in the current system state"
#define CGSYSTEM_NO_VALID_SUPERMODES_FOUND	1022,	"No valid supermodes were found in the overall map"
#define CGSYSTEM_SPECIFIED_SUPERMODE_NOT_FOUND	1023,	"The supermode number specified as an argument is not available"
#define CGSYSTEM_SPECIFIED_SUPERMODE_NOT_LOADED	1024,	"The supermode number specified as an argument is not loaded"
#define CGSYSTEM_FUNCTION_NOT_IMPLEMENTED	1025,	"The function called is not yet implemented"
#define CGSYSTEM_ERROR_WRITING_RESULTS_TO_FILE	1026,	"An error occurred when trying to write results to file"
#define CGSYSTEM_REGISTRY_ERROR	1027,	"Registry data is invalid. See log file for details."
#define CGSYSTEM_RESULTS_DISK_FULL_ERROR	1028,	"Error writing results to file. Disk is full."
#define CGSYSTEM_SEQUENCING_STEP_SIZE_ERROR	1029,	"Error running sequence. Current increment in sequence is too big."
#define CGSYSTEM_HARDWARE_ERROR	1030,	"Error encountered with hardware. See log for details."
#define CGSYSTEM_CONNECTIVITY_ERROR	1031,	"Connectivity test failed."
#define CGSYSTEM_POWER_METER_NOT_FOUND_ERROR	1032,	"Could not find power meter in system."
#define CGSYSTEM_WAVEMETER_NOT_FOUND_ERROR	1033,	"Could not find wavemeter in system."
#define CGSYSTEM_TEC_NOT_FOUND_ERROR	1034,	"Could not find TEC in system."
#define CGSYSTEM_POWER_READ_ERROR	1035,	"Error reading from Power Meter."
#define CGSYSTEM_DUPLICATE_DEVICE_NAME_ERROR	1036,	"Duplicate device names found in system."
#define CGSYSTEM_EMPTY_SEQUENCE_SLOTS_ERROR	1037,	"Error running sequence. Empty slots were found between sequencing devices."
#define CGSYSTEM_PXIT_MODULE_ERROR	1038,	"Error detected on device. Please see log for details."
#define CGSYSTEM_LASER_ALIGNMENT_ERROR	1039,	"Laser failed alignment test."
#define CGSYSTEM_SYSTEM_ERROR	1040,	"System Error detected. Please see log for details."
#define CGSYSTEM_LOGFILE_ERROR	1041,	"Error detected with log file."
#define CGSYSTEM_SET_CURRENT_ERROR	1042,	"Error setting current on device."
#define CGSYSTEM_SEQUENCING_ERROR	1043,	"Error running sequence. See log for details."
#define CGSYSTEM_DEVICE_NAME_NOT_ON_CARD_ERROR	1044,	"Registry device name conflicts with device name stored on module."
#define CGSYSTEM_306_FIRMWARE_OBSOLETE_ERROR	1045,	"Firmware version on 306 device is obsolete."
#define CGSYSTEM_COARSEFREQSAMPLENUM_ARG_ERROR	1046,	"The argument num_of_sample_pts must be greater than num_of_poly_coeffs"
#define CGSYSTEM_COARSEFREQMAXSAMPLENUM_ERROR	1047,	"The maximum number of sample pts has been exceeded"
#define CGSYSTEM_COARSEFREQCOEFFS_ARG_ERROR	1048,	"The argument num_of_poly_coeffs is outside the range of valid values"
#define CGSYSTEM_POWERRATIO_ARG_ERROR	1049,	"The argument power_ratio is outside the range of valid values [0:1]"
#define CGSYSTEM_COARSEFREQPOLYNOTLOADED_ERROR	1050,	"No valid coarse frequency polynomial is loaded"
#define CGSYSTEM_COARSEFREQPOLYINVALID_ERROR	1051,	"The argument frequency polynomial is not within valid value limits"
#define CGSYSTEM_MODULE_NAME_NOT_FOUND_ERROR	1052,	"The module name argument was not found"
#define CGSYSTEM_POWER_RATIO_OUT_OF_RANGE_ERROR	1053,	"The measured power ratio is not in the valid range [0:1]"
#define CGSYSTEM_COARSEFREQPOLYDATACOLLECTION_ERROR	1054,	"Required data not collected for coarse frequency polynomial calculation"
#define CGSYSTEM_WAVEMETER_MEASUREMENT_ERROR	1055,	"Could not read valid frequency from wavemeter."
#define CGSYSTEM_OPERATION_POINT_APPLY_ERROR	1056,	"Error occurred trying to apply an operating point."
#define CGSYSTEM_UNSTABLE_OPERATION_POINTS_ERROR	1057,	"Unstable operating points were encountered"
#define CGSYSTEM_ITUGRID_NOT_ALL_CHANNELS_FOUND_ERROR	1058,	"Not all defined ITU grid channels were found"
#define CGSYSTEM_MAPS_NOT_LOADED_ERROR	1059,	"Valid maps have not been loaded"
#define CGSYSTEM_CURRENTS_NOT_LOADED_ERROR	1060,	"Valid current vectors have not been loaded"
#define CGSYSTEM_CORRUPT_MAP_ERROR	1061,	"An internal map is corrupt"
#define CGSYSTEM_BOUNDARY_DETECTION_ERROR	1062,	"A boundary detection error was detected"
#define CGSYSTEM_NO_DATA_ERROR	1063,	"Internal data is missing"
#define CGSYSTEM_ERROR_READING_FROM_FILE	1064,	"Error occurred while attempting to read a file"
#define CGSYSTEM_NO_VALID_LONGITUDINAL_MODES_FOUND	1065,	"No valid longitudinal modes were found in the supermode map"
#define CGSYSTEM_PXITRELAY_ERROR	1066,	"Error calling PXITRelay dll function"
#define CGSYSTEM_INVALID_ARGUMENT	1067,	"Invalid argument encountered"
#define CGSYSTEM_POWER_READING_TOO_LOW_ERROR	1068,	"Power reading deemed to be too low - possibly DUT is not lasing."
#define CGSYSTEM_VARIANT_ARG_TOO_LARGE_ERROR	1069,	"A VARIANT argument contains an array that is too large"
#define CGSYSTEM_WORKER_THREAD_CREATION_ERROR	1070,	"An error occured while trying to create a worker thread"
#define CGSYSTEM_INVALID_MAP_TYPE_ERROR	1071,	"A map type argument was passed with a bad value"
#define CGSYSTEM_SM_NOT_FOUND_ERROR	1072,	"Requested supermode not found"
#define CGSYSTEM_LM_NOT_FOUND_ERROR	1073,	"Requested longitudinal mode not found"
#define CGSYSTEM_INDEX_NOT_FOUND_ERROR	1074,	"Requested index not found"
#define CGSYSTEM_GET_OUTPUT_CONNECTION_ERROR	1075,	"An error occurred while trying to assess the output state of a current source"
#define CGSYSTEM_REQUIRED_OUTPUTS_NOT_CONNECTED_ERROR	1076,	"The outputs required to represent a point on a map are not connected or don't have a front pair chosen"
#define CGSYSTEM_GET_CURRENT_ERROR	1077,	"An error occurred while trying to assess the output current of a current source"


#endif // __CGSYSTEM_ERRORS__H__

// Copyright (c) 2002 Tsunami Photonics Ltd. All Rights Reserved

///////////////////////////////////////////////////////////////////////////////
///
//	Class	:
//
//	Contents:
//
//	Author	:
//
///
///////////////////////////////////////////////////////////////////////////////
///
//
#include "stdafx.h"

#include "SequenceRunSettingsList.h"

#define new DEBUG_NEW

/////////////////////////////////////////////////////////////////////

SequenceRunSettingsList::
SequenceRunSettingsList()
{
}

SequenceRunSettingsList::
~SequenceRunSettingsList(void)
{
	emptyTheList();
}

/////////////////////////////////////////////////////////////////////

SequenceRunSettings* 
SequenceRunSettingsList::
findObjectByIndex(unsigned int index)
{
	SequenceRunSettings* pSequenceRunSettings = NULL;

	if (!IsEmpty())
	{
		POSITION pos;

		int i=0;
		for(pos = this->GetHeadPosition(); pos !=NULL;)
		{
			pSequenceRunSettings = reinterpret_cast<SequenceRunSettings*>(this->GetNext(pos));
			if(pSequenceRunSettings!=NULL)
			{
				if (i == index)
				{
					return pSequenceRunSettings;
				}
			}

			i++;
		}
	}

	return NULL;
}

/////////////////////////////////////////////////////////////////////

SequenceRunSettings* 
SequenceRunSettingsList::
findObject(int runNumber)
{
	SequenceRunSettings* pSequenceRunSettings = NULL;

	if (!IsEmpty())
	{
		POSITION pos;

		for(pos = this->GetTailPosition(); pos !=NULL;)
		{
			pSequenceRunSettings = reinterpret_cast<SequenceRunSettings*>(this->GetPrev(pos));
			if(pSequenceRunSettings!=NULL)
			{
				if (pSequenceRunSettings->_runNumber == runNumber)
				{	
					return pSequenceRunSettings;
				}
			}
		}
	}

	return NULL;
}

/////////////////////////////////////////////////////////////////////

bool 
SequenceRunSettingsList::
addToList(SequenceRunSettings* pSequenceRunSettings)
{
	bool success = FALSE;

	// Check that we don't have a
	// deviceCard with the same ID already
	if(!inList(pSequenceRunSettings))
	{
		this->AddHead(pSequenceRunSettings);
		success = TRUE;
	}
	else
	{
		success = FALSE;
	}

	return success;
}

/////////////////////////////////////////////////////////////////////

bool 
SequenceRunSettingsList::
removeFromList(SequenceRunSettings* pSequenceRunSettingsIn)
{
	bool success = FALSE;

	if(inList(pSequenceRunSettingsIn))
	{
		POSITION pos;

		for(pos = this->GetTailPosition(); pos !=NULL;)
		{
			SequenceRunSettings* pSequenceRunSettings = NULL;
			pSequenceRunSettings = reinterpret_cast<SequenceRunSettings*>(this->GetPrev(pos));
			if(pSequenceRunSettings!=NULL)
			{
				if (pSequenceRunSettings->_runNumber ==  pSequenceRunSettingsIn->_runNumber)
				{
					this->RemoveAt(pos);
					success = TRUE;
				}
			}
		}
	}
	else
	{
		success = FALSE;
	}

	return success;
}

/////////////////////////////////////////////////////////////////////

bool 
SequenceRunSettingsList::
inList(SequenceRunSettings *pSequenceRunSettingsIn)
{
	SequenceRunSettings *pSequenceRunSettings = NULL;

	if (!IsEmpty())
	{
		POSITION pos;

		for(pos = this->GetTailPosition(); pos !=NULL;)
		{
			pSequenceRunSettings = reinterpret_cast<SequenceRunSettings*>(this->GetPrev(pos));
			if(pSequenceRunSettings!=NULL)
			{
				if (pSequenceRunSettings->_runNumber ==  pSequenceRunSettingsIn->_runNumber)
				{
					return TRUE;
				}
			}
		}
	}

	return FALSE;
}

void 
SequenceRunSettingsList::
emptyTheList()
{
	SequenceRunSettings* pSequenceRunSettings;

	if (!IsEmpty())
	{
		POSITION pos;

		for (pos = this->GetTailPosition(); pos != NULL;)	//Start searching at the tail
		{
			pSequenceRunSettings = reinterpret_cast <SequenceRunSettings*> (this->GetPrev(pos)); //Select a pointer to this extension and move pos back by one position

			if(pSequenceRunSettings!=NULL)
			{
				delete pSequenceRunSettings;
				pSequenceRunSettings = 0;
			}

		}
	}

	RemoveAll();
} 
//
///////////////////////////////////////////////////////////////////////////////

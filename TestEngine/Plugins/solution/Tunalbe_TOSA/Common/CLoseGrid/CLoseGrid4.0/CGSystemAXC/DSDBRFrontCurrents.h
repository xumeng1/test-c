// Copyright (c) 2004 PXIT. All Rights Reserved
//
// FileName    : DSDBRFrontCurrents.h
// Description : Declaration of CDSDBRFrontCurrents class
//               Contains DSDBR Front Currents
//               and utility routines
//
// Rev#  | Date          | Author               | Description of change
// --------------------------------------------------------------
// 0.1   | 09 Sept 2004  | Frank D'Arcy         | Initial Draft
//

#pragma once

#include <vector>

class CDSDBRFrontCurrents
{
public:

	CDSDBRFrontCurrents(void);

	~CDSDBRFrontCurrents(void);

	typedef enum rcode
	{
		ok = 0,
		vector_size_rows_cols_do_not_match,
		could_not_open_file,
		file_does_not_exist,
		file_already_exists,
		file_format_invalid
	};

	typedef struct If_point
	{
		short front_pair_number;
		double I_front_1;
		double I_front_2;
		double I_front_3;
		double I_front_4;
		double I_front_5;
		double I_front_6;
		double I_front_7;
		double I_front_8;
	};

	rcode writeToFile( CString abs_filepath, bool overwrite = false );

	rcode readFromFile( CString abs_filepath );

	void clear();

	bool isEmpty();

	short pair_number( long index );

	long index_in_submap( long index );

	CString getConstantModuleName( short pair_num );
	CString getNonConstantModuleName( short pair_num );

	double getConstantCurrent( short pair_num, long sub_index );

	double getNonConstantCurrent( short pair_num, long sub_index );

	std::vector<double> getNonConstantCurrentVector( short pair_num );

	long _total_length;
	std::vector<long> _submap_length;
	std::vector<long> _submap_start_index;
	std::vector<If_point> _currents;
	CString _abs_filepath;
};

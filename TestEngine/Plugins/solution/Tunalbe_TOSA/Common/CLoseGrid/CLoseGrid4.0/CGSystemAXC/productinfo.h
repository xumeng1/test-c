// Copyright (c) 2004 PXIT. All Rights Reserved
//
// FileName    : productinfo.h
// Description : Defines product information
//
// Rev#  | Date          | Author               | Description of change
// --------------------------------------------------------------
// 0.1   | 12 July 2004   | Frank D'Arcy         | Initial Draft
//

#define CGSYSTEM_PRODUCTNAME "CLose-Grid"

#define CGSYSTEM_RELEASENO "4.0"

#define CGSYSTEM_COPYRIGHTNOTICE "2005 - no owner"


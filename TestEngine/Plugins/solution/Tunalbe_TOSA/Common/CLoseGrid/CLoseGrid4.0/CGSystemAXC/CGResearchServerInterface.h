// Copyright (c) 2004 Tsunami Photonics Ltd. All Rights Reserved

///////////////////////////////////////////////////////////////////////////////
///
//	Class	:
//
//	Contents:
//
//	Author	:
//
///
///////////////////////////////////////////////////////////////////////////////
///
//
#ifndef __CGRESEARCHSERVERINTERFACE_H_
#define __CGRESEARCHSERVERINTERFACE_H_


// COM interface
#include "CGResearchidl.h"
#include <vector>
//

#define OUTPUT_ON	1
#define	OUTPUT_OFF	0


#define SERVERINTERFACE CGResearchServerInterface::instance()

// logging levels
#define DIAGNOSTIC_CGR_LOG 4
#define INFO_CGR_LOG       3
#define WARNING_CGR_LOG    2
#define ERROR_CGR_LOG      1
#define CRITICAL_CGR_LOG   0

// macro to shorten the logging
#define CGR_LOG(STR,LEV) CGResearchServerInterface::instance()->log(STR, LEV);


using namespace std;

/////////////////////////////////////////////////////////////////////

class CGResearchServerInterface
{
public:
	// Singleton class
	static CGResearchServerInterface* instance();

				// set the interface
	void	setPointerToServer(IDual_DCGResearch* closeGridServerPtr);

	///////////////////////////////////////////////////////
	///
	//	User interface fxns
	//
	HRESULT	startup();

	HRESULT	setup(CString laserId,
				CString	laserType,
				CString	timeStamp,
				CString &err_msg);

	HRESULT	getVersionInfo(CString* productName,
						CString* versionName,
						CString* label,
						CString* labelInfo,
						CString* labelComment,
						CString* buildConfig);

	HRESULT	shutdown();

	HRESULT	rampdown();

	HRESULT	disconnect();

	HRESULT	getState(CString* state);

	/////////////////////////////////////
	///	Testing Interface

	HRESULT	livTest(int				powerMeterSlotNumber,
					int				powerMeterChannelNumber,
					int				currentSourceSlotNumber,
					double			minCurrent,
					double			maxCurrent,
					int				num_of_steps,
					int				step_duration,
					std::vector<double>* current_array,
					std::vector<double>* power_array,
					std::vector<double>* voltage_array,
					std::vector<double>* dPdI_array,
					std::vector<double>* d2PdI2_array,
					CString*		resultsDir);


	HRESULT	get2DData(int		slotNumber,
					double		minCurrent,
					double		maxCurrent,
					int			numSteps,
					int			stepDuration,
					bool		frequencyUnits,
					CString*	resultsDir);

	HRESULT	get2DData(CString		moduleName,
					std::vector<double>	currents,
					short			stepDuration,
					bool			frequencyUnits,
					CString*		resultsDir);


	HRESULT	get3DData(CString		moduleNameX,
					std::vector<double>	currentsX,
					CString			moduleNameY,
					std::vector<double>	currentsY,
					short			stepDuration,
					bool			hysteresis,
					bool			frequencyUnits,
					CString*		resultsDir);

	HRESULT	get3DData(int		slotNumberX,
					double		minCurrentX,
					double		maxCurrentX,
					int			numStepsX,
					int			slotNumberY,
					double		minCurrentY,
					double		maxCurrentY,
					int			numStepsY,
					int			stepDuration,
					bool		hysteresis,
					bool		frequencyUnits,
					CString*	resultsDir);

	HRESULT	setCurrent(CString	moduleName, 
						double	current);

	HRESULT	getCurrent(CString	moduleName, 
					double*	currentOut);


	HRESULT	readVoltage(CString	moduleName, 
					double*	voltageOut);

	HRESULT	setComplianceVoltage(CString	moduleName, 
						double	voltage);

	HRESULT	getComplianceVoltage(CString	moduleName, 
					double*	voltage);

	HRESULT	setOutputConnection(CString moduleName,
								short	outputConnection);

	HRESULT	getOutputConnection(CString moduleName,
								short*	outputConnection);

	HRESULT	readFrequency(double* frequency);

	HRESULT	readTemperature(double* temperature);

	HRESULT	setTecSetpoint(double temperature);

	HRESULT	getTecSetpoint(double* setpoint);

	HRESULT	readPower(int	slotNumber,
					int		channelNumber,
					double*	power);

	HRESULT	readOpticalPower(CString	moduleName,
							int			channel,
							double*		opticalPower);

	HRESULT	readOpticalPowers(CString	moduleName,
							double*		opticalPowerCh1,
							double*		opticalPowerCh2);

	HRESULT	readOpticalPowerAtFreq(CString	moduleName,
								int			channel,
								double		frequency,
								double*		opticalPower);

	HRESULT	readPhotodiodeCurrent(CString	moduleName,
								int			channel,
								double*		current);


	HRESULT	load305Sequence(CString	moduleName,
					std::vector<double>&	currents,
					int				sourceDelay,
					int				measureDelay);

	HRESULT	load306Sequence(CString	moduleName,
					short			numPoints,
					short			channel,
					int				sourceDelay,
					int				measureDelay);

	HRESULT	getSequenceCount(CString moduleName,
							short*	sequenceCount);

	HRESULT	setInSequence(CString moduleName,
						short	inSequence);

	HRESULT	resetSequences();

	HRESULT	reset305Sequences();

	HRESULT	reset306Sequences();

	HRESULT	runSequences();

	HRESULT	setCoarseFrequencyOffset(double powerRatio,
									double	frequency);

	HRESULT	readCoarseFrequency(double* frequency);

	HRESULT	setTECPIDValues(double pValue,
							double iValue,
							double dValue);

	HRESULT	getTECPIDValues(double* pValue,
							double* iValue,
							double* dValue);

	HRESULT	get306SequenceMeasurements(CString		moduleName,
									vector<double>*	channel1Measurements,
									vector<double>*	channel2Measurements);

	HRESULT	log(CString	message, short level );

	HRESULT	openLogFile();

	HRESULT	closeLogFile();

	HRESULT	setLogFileName(CString laserId,
				CString	laserType,
				CString	timeStamp);

	HRESULT	markThreadToKill( long thread_id );
	
	HRESULT getMarkedThreadID( long* p_thread_id);

	HRESULT initWavemeter();

	//
	///////////////////////////////////////////////////////

	///////////////////////////////////
	///
	//	Get and set functions
	//

	//
	///////////////////////////////////////////////////////

protected:
	CGResearchServerInterface(void);
	CGResearchServerInterface(const CGResearchServerInterface&);
	~CGResearchServerInterface(void);

	// = operator for returning the instance
	CGResearchServerInterface& operator= (const CGResearchServerInterface&);

private:
					// the instance of this class
	static	CGResearchServerInterface*	theInstance;


	void convertVariantToVector(VARIANT&	variantArray, vector<double>& array);

	void convertVectorToVariant(vector<double>& array, VARIANT &variantArray);

	///////////////////////////////////////////////////////
	///
	//	Client-Server interaction fxns
	//
						// ptr to server
	IDual_DCGResearch*		_closeGridServerPtr;


	CRITICAL_SECTION _safe_access; // protects access to research, only one thread at a time

	//
	/////////////////////////////////////////////////////
};
//
///////////////////////////////////////////////////////////////////////////////

#endif // __CGRESEARCHSERVERINTERFACE_H_

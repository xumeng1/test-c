// Copyright (c) 2004 Tsunami Photonics Ltd. All Rights Reserved
//
// FileName    : DSDBRSuperMode.cpp
// Description : Definition of CDSDBRSuperMode class
//
// Rev#  | Date          | Author               | Description of change
// --------------------------------------------------------------
// 0.1   | 10 Aug 2004   | Frank D'Arcy         | Initial Placeholder
// 0.2   | 10 Aug 2004   | Frank D'Arcy         | Initial Draft
// 0.3   | 11 Oct 2004   | Frank D'Arcy         | Adding Boundary Detection
//

#include "stdafx.h"
#include "DSDBRSuperMode.h"
#include "CGRegValues.h"
#include "CGResearchServerInterface.h"
#include "CLoseGridErrorCodes.h"
#include "CGRegValues.h"
#include "PassFailCollated.h"
#include "CGSystemErrors.h"

#include <fstream>
#include <direct.h>

#include "VectorAnalysis.h"
#include <math.h>

#include <algorithm>

// constructor
CDSDBRSuperMode::CDSDBRSuperMode(short	superModeNumber,
								int		xAxisLength,
								int		yAxisLength)
{
	_p_data_protect = NULL;
	_p_percent_complete = NULL;

	_sm_number = superModeNumber;
	_xAxisLength = xAxisLength;
	_yAxisLength = yAxisLength;

	_qaAnalysisComplete = false;

	clear();
}

CDSDBRSuperMode::
CDSDBRSuperMode()
{
}

// destructor
CDSDBRSuperMode::~CDSDBRSuperMode()
{
	clear();
}



std::pair<short, std::string>
CDSDBRSuperMode::get_error_pair( rcode error_in )
{
	std::pair<short, std::string> return_error_pair;

	switch( error_in )
	{
		case file_does_not_exist:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_FILE_DOES_NOT_EXIST_ERROR )
			break;
		case file_already_exists:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_FILEOPEN_ERROR )
			break;
		case could_not_open_file:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_FILEOPEN_ERROR )
			break;
		case file_format_invalid:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_INVALID_FILE_FORMAT_ERROR )
			break;
		case files_not_matching_sizes:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_FILES_NOT_MATCHING_SIZES_ERROR )
			break;
		//case corrupt_map_not_written_to_file:
		//	CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_ERROR_WRITING_RESULTS_TO_FILE )
		//	break;
		//case existing_file_not_overwritten:
		//	CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_ERROR_WRITING_RESULTS_TO_FILE )
		//	break;
		case file_read_error:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_ERROR_READING_FROM_FILE )
			break;
		case maps_not_loaded:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_MAPS_NOT_LOADED_ERROR )
			break;
		//case currents_not_loaded:
		//	CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_CURRENTS_NOT_LOADED_ERROR )
		//	break;
		case invalid_front_pair_number:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_CORRUPT_MAP_ERROR )
			break;
		//case outside_map_dimensions:
		//	CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_CORRUPT_MAP_ERROR )
		//	break;
		//case corrupt_map_dimensions:
		//	CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_CORRUPT_MAP_ERROR )
		//	break;
		case no_data:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_NO_DATA_ERROR )
			break;
		//case submap_indexing_error:
		//	CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_CORRUPT_MAP_ERROR )
		//	break;
		case sequencing_error:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_SEQUENCING_ERROR )
			break;
		//case sequence_data_format_error:
		//	CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_SEQUENCING_ERROR )
		//	break;
		case sequencing_step_size_error:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_SEQUENCING_STEP_SIZE_ERROR )
			break;
		case hardware_error:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_HARDWARE_ERROR )
			break;
		case system_error:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_SYSTEM_ERROR )
			break;
		case set_current:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_SET_CURRENT_ERROR )
			break;
		case registry_error:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_REGISTRY_ERROR )
			break;
		case log_file_error:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_LOGFILE_ERROR )
			break;
		case results_disk_full:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_RESULTS_DISK_FULL_ERROR )
			break;
		case connectivity_error:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_CONNECTIVITY_ERROR )
			break;
		case power_meter_not_found:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_POWER_METER_NOT_FOUND_ERROR )
			break;
		case wavemeter_not_found:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_WAVEMETER_NOT_FOUND_ERROR )
			break;
		case tec_not_found:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_TEC_NOT_FOUND_ERROR )
			break;
		case power_read_error:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_POWER_READ_ERROR )
			break;
		case duplicate_device_name:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_DUPLICATE_DEVICE_NAME_ERROR )
			break;
		case pxit_module_error:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_PXIT_MODULE_ERROR )
			break;
		case device_name_not_on_card:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_DEVICE_NAME_NOT_ON_CARD_ERROR )
			break;
		case device_306_firmware_obsolete:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_306_FIRMWARE_OBSOLETE_ERROR )
			break;
		case laser_alignment_error:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_LASER_ALIGNMENT_ERROR )
			break;
		case empty_sequence_slots:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_EMPTY_SEQUENCE_SLOTS_ERROR )
			break;
		case module_name_not_found_error:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_MODULE_NAME_NOT_FOUND_ERROR )
			break;
		case min_number_of_lines_not_reached:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_BOUNDARY_DETECTION_ERROR )
			break;
		case max_number_of_lines_exceeded:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_BOUNDARY_DETECTION_ERROR )
			break;
		case mismatched_lines:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_BOUNDARY_DETECTION_ERROR )
			break;
		case screening_failed:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_BOUNDARY_DETECTION_ERROR )
			break;
		default:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_UNKNOWN_ERROR )
	}

	return return_error_pair;
}




void
CDSDBRSuperMode::
clear()
{
	_upper_line.clear();
	_lower_line.clear();
	_middle_line.clear();

	_phase_current.clear();
	_filtered_middle_line.clear();

	_boundary_detection_ramp.clear();

	clearMapsAndLines();

	_maps_loaded = false;
	_results_dir_CString = "";
	_date_time_stamp_CString = "";
	_laser_id_CString = "";

	_Im_ramp = false;
	_rows = 0;
	_cols = 0;
	_sourceDelay = 0;
	_measureDelay = 0;
	_sm_number = 0;
	_maps_loaded = false;
	_row_of_midpoint_of_filtered_middle_line = 0;
	_I_gain = 0;
	_I_soa = 0;

	_middleLineRMSValue = 0;
	_middleLineSlope	= 0;

	_modeAnalysis.init();
}


void
CDSDBRSuperMode::
clearMapsAndLines()
{
	_map_forward_direct_power.clear();
	_map_reverse_direct_power.clear();
	_map_hysteresis_direct_power.clear();

	_map_forward_filtered_power.clear();
	_map_reverse_filtered_power.clear();
	_map_hysteresis_filtered_power.clear();

	_map_forward_power_ratio.clear();
	_map_reverse_power_ratio.clear();
	_map_hysteresis_power_ratio.clear();

	_map_forward_power_ratio_median_filtered.clear();
	_map_reverse_power_ratio_median_filtered.clear();
	_map_hysteresis_power_ratio_median_filtered.clear();

	_map_forward_photodiode1_current.clear();
	_map_reverse_photodiode1_current.clear();
	_map_forward_photodiode2_current.clear();
	_map_reverse_photodiode2_current.clear();

	//_phase_current.clear();

	//_filtered_middle_line.clear();

	for( int i = 0 ; i < (int)(_lm_upper_lines.size()) ; i++ )
	{
		_lm_upper_lines[i].clear();
	}
	_lm_upper_lines.clear();

	for( int i = 0 ; i < (int)(_lm_lower_lines.size()) ; i++ )
	{
		_lm_lower_lines[i].clear();
	}
	_lm_lower_lines.clear();

	for( int i = 0 ; i < (int)(_lm_middle_lines.size()) ; i++ )
	{
		_lm_middle_lines[i].clear();
	}
	_lm_middle_lines.clear();

	for( int i = 0 ; i < (int)(_lm_filtered_middle_lines.size()) ; i++ )
	{
		_lm_filtered_middle_lines[i].clear();
	}
	_lm_filtered_middle_lines.clear();

	for( int i = 0 ; i < (int)(_lm_middle_lines_of_frequency.size()) ; i++ )
	{
		_lm_middle_lines_of_frequency[i].clear();
	}
	_lm_middle_lines_of_frequency.clear();

	_average_widths.clear();
	_matched_line_numbers.clear();

	_lm_upper_lines_removed = 0;
	_lm_lower_lines_removed = 0;
	_lm_middle_lines_of_frequency_removed = 0;
}

short
CDSDBRSuperMode::getLMCount()
{
	short lm_count = 0;

	for( int i = 0 ; i < (int)(_lm_middle_lines_of_frequency.size()) ; i++ )
	{
		if( _lm_middle_lines_of_frequency[i]._points.size() > 0 )
		{
			lm_count++;
		}
	}

	return lm_count;
}

CDSDBRSuperMode::rcode
CDSDBRSuperMode::
collectMapsFromLaser()
{
	rcode rval = rcode::ok;
	HRESULT err = S_OK;

	//////////////////////////////////////////////////////////////////////////////////
	///
	//	Retrieve registry parameters for gathering supermode plots
	//
	bool hysteresis = true;
	if( CG_REG->get_DSDBR01_SMDC_ramp_direction() == CString(SMDC_RAMP_MIDDLE_LINE) )
		_Im_ramp = true;
	else
		_Im_ramp = false;

	_sourceDelay	= CG_REG->get_DSDBR01_SMDC_source_delay();
	_measureDelay	= CG_REG->get_DSDBR01_SMDC_measure_delay();


	// Find the Gain and SOA current settings
	HRESULT get_Igain_rval = SERVERINTERFACE->getCurrent( CG_REG->get_DSDBR01_CMDC_gain_module_name(), &_I_gain );
	HRESULT get_Isoa_rval = SERVERINTERFACE->getCurrent( CG_REG->get_DSDBR01_CMDC_soa_module_name(), &_I_soa );

	if( get_Igain_rval != S_OK )
	{
		rval = mapHResultToRCode(get_Igain_rval);
	}
	if( rval == rcode::ok && get_Isoa_rval != S_OK )
	{
		rval = mapHResultToRCode(get_Isoa_rval);
	}

	int numPointsPerCycle = 0;
	int numCycles = 0;
	SequenceRunSettingsList* pSequenceRunSettingsList = new SequenceRunSettingsList();
	if(rval == rcode::ok)
	{
		//
		////////////////////////////////////////////////////////////////////////////////////
		///
		//	Extract sequences to run
		//

		////////////////////////////////////////////////////////////////////////////////////////////

		rval = extractSequenceRunSettings(pSequenceRunSettingsList, 
										CG_REG->get_DSDBR01_SMDC_phase_currents_abspath(),
										hysteresis,
										numPointsPerCycle,
										numCycles);

		if( rval != rcode::ok )
		{
			CString log_err_msg("CDSDBRSuperMode::collectMapsFromLaser() error returned from extractSequenceRunSettings");
			CGR_LOG(log_err_msg,ERROR_CGR_LOG)
		}
	}

	//
	////////////////////////////////////////////////////////////////////////////////////////////
	///
	//	Run sequences

	vector<double>	powerCh1;
	vector<double>	powerCh2;
	vector<double>	photodiodeCurrentCh1;
	vector<double>	photodiodeCurrentCh2;

	powerCh1.clear();
	powerCh2.clear();
	photodiodeCurrentCh1.clear();
	photodiodeCurrentCh2.clear();

	if(rval == rcode::ok)
	{
		rval = runSequences(pSequenceRunSettingsList,
							numPointsPerCycle,
							numCycles,
							&powerCh1,
							&powerCh2,
							&photodiodeCurrentCh1,
							&photodiodeCurrentCh2);


		if( rval != rcode::ok )
		{
			CString log_err_msg("CDSDBRSuperMode::collectMapsFromLaser() error returned from runSequences");
			CGR_LOG(log_err_msg,ERROR_CGR_LOG)
		}

	}

	//
	////////////////////////////////////////////////////////////////////////////////////////////
	///
	//	Format results

	vector<double> forwardDirectPower;
	vector<double> reverseDirectPower;
	vector<double> hysteresisDirectPower;

	vector<double> forwardFilteredPower;
	vector<double> reverseFilteredPower;
	vector<double> hysteresisFilteredPower;

	vector<double> forwardPowerRatio;
	vector<double> reversePowerRatio;
	vector<double> hysteresisPowerRatio;


	if(rval==rcode::ok)
	{
		if( CG_REG->get_DSDBR01_CMDC_306II_direct_channel() == CHANNEL_1 )
		{
			rval = extractPowerData(hysteresis,
								numPointsPerCycle,
								powerCh1,
								powerCh2,
								forwardDirectPower,
								reverseDirectPower,
								hysteresisDirectPower,
								forwardFilteredPower,
								reverseFilteredPower,
								hysteresisFilteredPower,
								forwardPowerRatio,
								reversePowerRatio,
								hysteresisPowerRatio);
		}
		else // direct channel is CHANNEL_2
		{
			rval = extractPowerData(hysteresis,
								numPointsPerCycle,
								powerCh2,
								powerCh1,
								forwardDirectPower,
								reverseDirectPower,
								hysteresisDirectPower,
								forwardFilteredPower,
								reverseFilteredPower,
								hysteresisFilteredPower,
								forwardPowerRatio,
								reversePowerRatio,
								hysteresisPowerRatio);
		}

		if( rval != rcode::ok )
		{
			CString log_err_msg("CDSDBRSuperMode::collectMapsFromLaser() error returned from extractPowerData");
			CGR_LOG(log_err_msg,ERROR_CGR_LOG)
		}
	}



	vector<double> forward_photodiode1_current;
	vector<double> reverse_photodiode1_current;
	vector<double> forward_photodiode2_current;
	vector<double> reverse_photodiode2_current;

	if( rval == rcode::ok
	 && CG_REG->get_DSDBR01_SMDC_measure_photodiode_currents() )
	{
		// extract photodiode1 currents

		if( CG_REG->get_DSDBR01_CMDC_306EE_photodiode1_channel() == CHANNEL_1 )
		{
			if(hysteresis)
			{
				rval = splitHysteresisPowerArray(
					photodiodeCurrentCh1,
					numPointsPerCycle,
					forward_photodiode1_current,
					reverse_photodiode1_current);
			}
			else
			{
				for(int jkl = 0; jkl < (int)photodiodeCurrentCh1.size() ; jkl++ )
					forward_photodiode1_current.push_back(photodiodeCurrentCh1[jkl]);
			}
		}
		else
		{
			if(hysteresis)
			{
				rval = splitHysteresisPowerArray(
					photodiodeCurrentCh2,
					numPointsPerCycle,
					forward_photodiode1_current,
					reverse_photodiode1_current);
			}
			else
			{
				for(int jkl = 0; jkl < (int)photodiodeCurrentCh2.size() ; jkl++ )
					forward_photodiode1_current.push_back(photodiodeCurrentCh2[jkl]);
			}
		}

		if( rval != rcode::ok )
		{
			CString log_err_msg("CDSDBRSuperMode::collectMapsFromLaser() : error returned from splitHysteresisPowerArray");
			CGR_LOG(log_err_msg,ERROR_CGR_LOG)
		}
	}

	if( rval == rcode::ok
	 && CG_REG->get_DSDBR01_SMDC_measure_photodiode_currents() )
	{
		// extract photodiode2 currents

		if( CG_REG->get_DSDBR01_CMDC_306EE_photodiode2_channel() == CHANNEL_1 )
		{
			if(hysteresis)
			{
				rval = splitHysteresisPowerArray(
					photodiodeCurrentCh1,
					numPointsPerCycle,
					forward_photodiode2_current,
					reverse_photodiode2_current);
			}
			else
			{
				for(int jkl = 0; jkl < (int)photodiodeCurrentCh1.size() ; jkl++ )
					forward_photodiode2_current.push_back(photodiodeCurrentCh1[jkl]);
			}
		}
		else
		{
			if(hysteresis)
			{
				rval = splitHysteresisPowerArray(
					photodiodeCurrentCh2,
					numPointsPerCycle,
					forward_photodiode2_current,
					reverse_photodiode2_current);
			}
			else
			{
				for(int jkl = 0; jkl < (int)photodiodeCurrentCh2.size() ; jkl++ )
					forward_photodiode2_current.push_back(photodiodeCurrentCh2[jkl]);
			}
		}

		if( rval != rcode::ok )
		{
			CString log_err_msg("CDSDBRSuperMode::collectMapsFromLaser() : error returned from splitHysteresisPowerArray");
			CGR_LOG(log_err_msg,ERROR_CGR_LOG)
		}
	}




	/////////////////////////////////////////////////////////////////////
	///
	//	Store the results
	//
	if(rval == rcode::ok)
	{
		long numRows = (long)numCycles;
		long numCols = (long)numPointsPerCycle;

		_map_forward_direct_power.setMap(forwardDirectPower, numRows, numCols);
		_map_reverse_direct_power.setMap(reverseDirectPower, numRows, numCols);
		_map_hysteresis_direct_power.setMap(hysteresisDirectPower, numRows, numCols);

		_map_forward_filtered_power.setMap(forwardFilteredPower, numRows, numCols);
		_map_reverse_filtered_power.setMap(reverseFilteredPower, numRows, numCols);
		_map_hysteresis_filtered_power.setMap(hysteresisFilteredPower, numRows, numCols);

		_map_forward_power_ratio.setMap(forwardPowerRatio, numRows, numCols);
		_map_reverse_power_ratio.setMap(reversePowerRatio, numRows, numCols);
		_map_hysteresis_power_ratio.setMap(hysteresisPowerRatio, numRows, numCols);

		if( CG_REG->get_DSDBR01_SMDC_measure_photodiode_currents() )
		{
			_map_forward_photodiode1_current.setMap(forward_photodiode1_current, numRows, numCols);
			_map_reverse_photodiode1_current.setMap(reverse_photodiode1_current, numRows, numCols);
			_map_forward_photodiode2_current.setMap(forward_photodiode2_current, numRows, numCols);
			_map_reverse_photodiode2_current.setMap(reverse_photodiode2_current, numRows, numCols);
		}

	}

	delete pSequenceRunSettingsList;

	///////////////////////////////////////////////////////////////////////////////////

	if( rval == rcode::ok )
	{
		// have successfully collected data from laser
		_rows = (long)(_filtered_middle_line._points.size());
		_cols = _phase_current._length;
		_maps_loaded = true;

		_xAxisLength = (int)_cols;
		_yAxisLength = (int)_rows;
	}
	else
	{
		clearMapsAndLines();
		_maps_loaded = false;
	}


	return rval;
}

CDSDBRSuperMode::rcode
CDSDBRSuperMode::
extractSequenceRunSettings(SequenceRunSettingsList* sequenceRunSettingsList,
						   CString					phaseCurrentsAbsFilePath,
						   bool						hysteresis,
						   int&						numPointsPerCycle,
						   int&						numCycles)
{
	rcode rval = rcode::ok;

	// clear it out
	sequenceRunSettingsList->emptyTheList();

	///////////////////////////////////////////////////////////////////

	CCurrentsVector::rcode readIp_rval =  _phase_current.readFromFile( phaseCurrentsAbsFilePath );

	if( readIp_rval != CCurrentsVector::rcode::ok )
	{
		CString log_err_msg("CDSDBRSuperMode::extractSequenceRunSettings() error reading phase currents file: ");
		log_err_msg += phaseCurrentsAbsFilePath;
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)
	}

	///////////////////////////////////////////////////////////////////

	if(rval == rcode::ok && readIp_rval == CCurrentsVector::rcode::ok )
	{
		if(_Im_ramp)
		{
			rval = extractSequenceSettingsWithMiddleRamp(sequenceRunSettingsList,
												_phase_current._currents,
												hysteresis,
												numPointsPerCycle,
												numCycles);

			if( rval != rcode::ok )
			{
				CString log_err_msg("CDSDBRSuperMode::extractSequenceRunSettings() error returned from extractSequenceSettingsWithMiddleRamp");
				CGR_LOG(log_err_msg,ERROR_CGR_LOG)
			}

		}
		else
		{
			rval = extractSequenceSettingsWithPhaseRamp(sequenceRunSettingsList,
												_phase_current._currents,
												hysteresis,
												numPointsPerCycle,
												numCycles);
			if( rval != rcode::ok )
			{
				CString log_err_msg("CDSDBRSuperMode::extractSequenceRunSettings() error returned from extractSequenceSettingsWithPhaseRamp");
				CGR_LOG(log_err_msg,ERROR_CGR_LOG)
			}

		}
	}
	//////////////////////////////////////////////////////////////////


	return rval;
}

CDSDBRSuperMode::rcode
CDSDBRSuperMode::
extractSequenceSettingsWithMiddleRamp(SequenceRunSettingsList*	sequenceRunSettingsList,
									vector<double>				phaseSequence,
									bool						hysteresis,
									int&						numPointsPerCycle,
									int&						numCycles)
{
	rcode rval = rcode::ok;

	///////////////////////////////////////////////////////

	vector<double> rearSequence;
	vector<double> frontSequence;

	vector<double> nullVector;
	nullVector.clear();

	vector<short> frontPairs;

	CString disconnectConstantFrontModuleName = "";
	CString disconnectSequencedFrontModuleName = "";

	CString constantFrontModuleName = "";
	CString sequencedFrontModuleName = "";
	
	///////////////////////////////////////////////////////

	_filtered_middle_line.getFrontPairNumbers(frontPairs);
	short numFrontPairs = (short)frontPairs.size();

	numCycles = (int)phaseSequence.size();
	numPointsPerCycle = 0;
	//numPointsPerCycle = middleLineFile->numRows() - 1; // num rear sequence points

	int sequenceRunSettingsCounter = 0;
	SequenceRunSettings* runSettings = NULL;

	for(int cycleNumber=0; cycleNumber<numCycles; cycleNumber++)
	{
		numPointsPerCycle = 0;

		vector<short>::iterator frontPairIter;
		for(frontPairIter = frontPairs.begin(); frontPairIter != frontPairs.end(); frontPairIter++)
		{
			short frontPairNumber = *frontPairIter;

			//////////////////////////////////////////////////////////

			double constantCurrent = 0;
			_filtered_middle_line.getCurrentsInOneFrontPair(frontPairNumber,
												constantCurrent,
												frontSequence,
												rearSequence);

			////////////////////////////////////////////////////////////

			numPointsPerCycle = numPointsPerCycle + (int)rearSequence.size();

			////////////////////////////////////////////////////////////

			if(frontPairNumber==frontPairs[((int)frontPairs.size()-1)]) // we've reached the apex
			{
				if(hysteresis)
				{
					applyHysteresisToSequence(rearSequence);
					applyHysteresisToSequence(frontSequence);
				}
			}


			////////////////////////////////////////////////////////////
			/// Check for sequences which are too long and
			// need to be split
			//
			vector<CCurrentsVector> frontSeqs;
			vector<CCurrentsVector> rearSeqs;

			rearSeqs.clear();
			frontSeqs.clear();

			splitSequence(MAX_SEQUENCE_SIZE,
					frontSequence,
					frontSeqs);
					

			splitSequence(MAX_SEQUENCE_SIZE,
					rearSequence,
					rearSeqs);

			int numSplitSequences = (int)(rearSeqs.size());

			for(int i=0; i<numSplitSequences; i++)
			{
				runSettings = NULL;
				runSettings = new SequenceRunSettings(sequenceRunSettingsCounter);		
				sequenceRunSettingsCounter++;


				////////////////////////////////////////////////////////////

				mapFrontPairNumberToModuleNames(frontPairNumber,
												constantFrontModuleName,
												sequencedFrontModuleName);

				////////////////////////////////////////////////////////////

				PXIT306SequenceSettings* pxit306SequenceSettings = NULL;
				PXIT305SequenceSettings* pxit305SequenceSettings = NULL;

				////////////////////////////////////////////////////////////
					
				//disconnect the previous constant front
				//if this is not the first sequence
				if(	(disconnectConstantFrontModuleName != "")&&
					(disconnectConstantFrontModuleName != constantFrontModuleName)&&
					(disconnectConstantFrontModuleName != sequencedFrontModuleName))
				{

					pxit305SequenceSettings = new PXIT305SequenceSettings(disconnectConstantFrontModuleName,
																		nullVector,
																		OUTPUT_OFF,
																		0,
																		NOT_IN_SEQUENCE,
																		_sourceDelay,
																		_measureDelay);
					runSettings->_PXIT305SequenceSettingsList->addToList(pxit305SequenceSettings);

				}
				disconnectConstantFrontModuleName = constantFrontModuleName;


				if(	(disconnectSequencedFrontModuleName != "")&&
					(disconnectSequencedFrontModuleName != constantFrontModuleName)&&
					(disconnectSequencedFrontModuleName != sequencedFrontModuleName))
				{

					pxit305SequenceSettings = new PXIT305SequenceSettings(disconnectSequencedFrontModuleName,
																		nullVector,
																		OUTPUT_OFF,
																		0,
																		NOT_IN_SEQUENCE,
																		_sourceDelay,
																		_measureDelay);
					runSettings->_PXIT305SequenceSettingsList->addToList(pxit305SequenceSettings);

					disconnectSequencedFrontModuleName = "";
				}

				//
				///////////////////////////////////////////////////////////////////////////////////////
				//

				pxit305SequenceSettings = new PXIT305SequenceSettings(CG_REG->get_DSDBR01_CMDC_rear_module_name(),
																	rearSeqs[i]._currents,
																	OUTPUT_ON,
																	0,
																	IN_SEQUENCE,
																	_sourceDelay,
																	_measureDelay);
				runSettings->_PXIT305SequenceSettingsList->addToList(pxit305SequenceSettings);


				//
				///
				//
				pxit305SequenceSettings = new PXIT305SequenceSettings(CG_REG->get_DSDBR01_CMDC_phase_module_name(),
																	nullVector,
																	OUTPUT_ON,
																	phaseSequence[cycleNumber],
																	NOT_IN_SEQUENCE,
																	_sourceDelay,
																	_measureDelay);
				runSettings->_PXIT305SequenceSettingsList->addToList(pxit305SequenceSettings);
				///////////////////////////////////////////////////////////////////////////////

				//
				///
				//

				CString moduleName = "";

				pxit305SequenceSettings = new PXIT305SequenceSettings(constantFrontModuleName,
																	nullVector,
																	OUTPUT_ON,
																	constantCurrent,
																	NOT_IN_SEQUENCE,
																	_sourceDelay,
																	_measureDelay);
				runSettings->_PXIT305SequenceSettingsList->addToList(pxit305SequenceSettings);

				//
				///
				//
				pxit305SequenceSettings = new PXIT305SequenceSettings(sequencedFrontModuleName,
																	frontSeqs[i]._currents,
																	OUTPUT_ON,
																	0,
																	IN_SEQUENCE,
																	_sourceDelay,
																	_measureDelay);
				runSettings->_PXIT305SequenceSettingsList->addToList(pxit305SequenceSettings);


				//
				///
				//
				pxit306SequenceSettings = new PXIT306SequenceSettings(CG_REG->get_DSDBR01_CMDC_306II_module_name(),
																	IN_SEQUENCE,
																	(int)(rearSeqs[i]._currents.size()),
																	_sourceDelay,
																	_measureDelay);
				runSettings->_PXIT306SequenceSettingsList->addToList(pxit306SequenceSettings);

				///////////////////////////////////////////////////////////

				if( CG_REG->get_DSDBR01_SMDC_measure_photodiode_currents() )
				{
					pxit306SequenceSettings = new PXIT306SequenceSettings(CG_REG->get_DSDBR01_CMDC_306EE_module_name(),
																		IN_SEQUENCE,
																		(int)(rearSeqs[i]._currents.size()),
																		_sourceDelay,
																		_measureDelay);
					runSettings->_PXIT306SequenceSettingsList->addToList(pxit306SequenceSettings);
				}

				sequenceRunSettingsList->addToList(runSettings);
			}

			for(int i_seq = 0 ; i_seq < (int)rearSeqs.size() ; i_seq++ )
				rearSeqs[i_seq].clear();
			for(int i_seq = 0 ; i_seq < (int)frontSeqs.size() ; i_seq++ )
				frontSeqs[i_seq].clear();
			rearSeqs.clear();
			frontSeqs.clear();

		}

		// for next run
		disconnectConstantFrontModuleName = constantFrontModuleName;
		disconnectSequencedFrontModuleName = sequencedFrontModuleName;


		if(hysteresis)
		{
			disconnectSequencedFrontModuleName = sequencedFrontModuleName;

			//TODO tidy up
			frontPairIter = frontPairs.end();
			frontPairIter--;
			frontPairIter--;
			for(frontPairIter = frontPairIter; frontPairIter >= frontPairs.begin(); frontPairIter--)
			{
				short frontPairNumber = *frontPairIter;
				double constantCurrent = 0;

				//////////////////////////////////////////////////////////

				_filtered_middle_line.getCurrentsInOneFrontPair(frontPairNumber,
													constantCurrent,
													frontSequence,
													rearSequence);

				////////////////////////////////////////////////////////////

				// reverse the sequences
				reverse(frontSequence.begin(), frontSequence.end());
				reverse(rearSequence.begin(), rearSequence.end());

				////////////////////////////////////////////////////////////

				CString constantFrontModuleName = "";
				CString sequencedFrontModuleName = "";
				mapFrontPairNumberToModuleNames(frontPairNumber,
												constantFrontModuleName,
												sequencedFrontModuleName);

				////////////////////////////////////////////////////////////
				////////////////////////////////////////////////////////////
				/// Check for sequences which are too long and
				// need to be split
				//
				vector<CCurrentsVector> frontSeqs;
				vector<CCurrentsVector> rearSeqs;

				rearSeqs.clear();
				frontSeqs.clear();

				splitSequence(MAX_SEQUENCE_SIZE,
						frontSequence,
						frontSeqs);
						

				splitSequence(MAX_SEQUENCE_SIZE,
						rearSequence,
						rearSeqs);

				int numSplitSequences = (int)(rearSeqs.size());

				for(int i=0; i<numSplitSequences; i++)
				{
					runSettings = NULL;
					runSettings = new SequenceRunSettings(sequenceRunSettingsCounter);		
					sequenceRunSettingsCounter++;
				
					PXIT306SequenceSettings* pxit306SequenceSettings = NULL;
					PXIT305SequenceSettings* pxit305SequenceSettings = NULL;
					pxit305SequenceSettings = new PXIT305SequenceSettings(CG_REG->get_DSDBR01_CMDC_rear_module_name(),
																		rearSeqs[i]._currents,
																		OUTPUT_ON,
																		0,
																		IN_SEQUENCE,
																		_sourceDelay,
																		_measureDelay);
					runSettings->_PXIT305SequenceSettingsList->addToList(pxit305SequenceSettings);


					//
					///
					//
					pxit305SequenceSettings = new PXIT305SequenceSettings(CG_REG->get_DSDBR01_CMDC_phase_module_name(),
																		nullVector,
																		OUTPUT_ON,
																		phaseSequence[cycleNumber],
																		NOT_IN_SEQUENCE,
																		_sourceDelay,
																		_measureDelay);
					runSettings->_PXIT305SequenceSettingsList->addToList(pxit305SequenceSettings);
					///////////////////////////////////////////////////////////////////////////////

					//
					///
					//

					CString moduleName = "";

					//disconnect the previous constant front
					//if this is not the first sequence
					if(disconnectSequencedFrontModuleName != "")
					{

						pxit305SequenceSettings = new PXIT305SequenceSettings(disconnectSequencedFrontModuleName,
																			nullVector,
																			OUTPUT_OFF,
																			0,
																			NOT_IN_SEQUENCE,
																			_sourceDelay,
																			_measureDelay);
						runSettings->_PXIT305SequenceSettingsList->addToList(pxit305SequenceSettings);

					}
					disconnectSequencedFrontModuleName = sequencedFrontModuleName;


					//
					///
					//

					pxit305SequenceSettings = new PXIT305SequenceSettings(constantFrontModuleName,
																		nullVector,
																		OUTPUT_ON,
																		constantCurrent,
																		NOT_IN_SEQUENCE,
																		_sourceDelay,
																		_measureDelay);
					runSettings->_PXIT305SequenceSettingsList->addToList(pxit305SequenceSettings);

					//
					///
					//
					pxit305SequenceSettings = new PXIT305SequenceSettings(sequencedFrontModuleName,
																		frontSeqs[i]._currents,
																		OUTPUT_ON,
																		0,
																		IN_SEQUENCE,
																		_sourceDelay,
																		_measureDelay);
					runSettings->_PXIT305SequenceSettingsList->addToList(pxit305SequenceSettings);


					//
					///
					//
					pxit306SequenceSettings = new PXIT306SequenceSettings(CG_REG->get_DSDBR01_CMDC_306II_module_name(),
																		IN_SEQUENCE,
																		(int)(rearSeqs[i]._currents.size()),
																		_sourceDelay,
																		_measureDelay);
					runSettings->_PXIT306SequenceSettingsList->addToList(pxit306SequenceSettings);

					///////////////////////////////////////////////////////////


					if( CG_REG->get_DSDBR01_SMDC_measure_photodiode_currents() )
					{
						pxit306SequenceSettings = new PXIT306SequenceSettings(CG_REG->get_DSDBR01_CMDC_306EE_module_name(),
																			IN_SEQUENCE,
																			(int)(rearSeqs[i]._currents.size()),
																			_sourceDelay,
																			_measureDelay);
						runSettings->_PXIT306SequenceSettingsList->addToList(pxit306SequenceSettings);
					}

					sequenceRunSettingsList->addToList(runSettings);
				}

				for(int i_seq = 0 ; i_seq < (int)rearSeqs.size() ; i_seq++ )
					rearSeqs[i_seq].clear();
				for(int i_seq = 0 ; i_seq < (int)frontSeqs.size() ; i_seq++ )
					frontSeqs[i_seq].clear();
				rearSeqs.clear();
				frontSeqs.clear();

			}

			// for next run
			disconnectSequencedFrontModuleName = "";
			disconnectConstantFrontModuleName = "";
		}

	}

	//////////////////////////////////////////////////////////////////


	return rval;
}

CDSDBRSuperMode::rcode
CDSDBRSuperMode::
extractSequenceSettingsWithPhaseRamp(SequenceRunSettingsList*	sequenceRunSettingsList,
								   vector<double>				phaseSequence,
								   bool							hysteresis,
								   int&							numPointsPerCycle,
								   int&							numCycles)
{
	rcode rval = rcode::ok;

	//numCycles = middleLineFile->numRows() - 1; // num rear sequence points
	numCycles = 0;
	numPointsPerCycle = (int)phaseSequence.size();

	////////////////////////////////////////////////////////////////////

	if(hysteresis)
	{
		applyHysteresisToSequence(phaseSequence);
	}

	////////////////////////////////////////////////////////////////////

	vector<double> nullVector;
	nullVector.clear();

	vector<double> frontSequence;
	CString previousConstantFrontModuleName = "";


	vector<short> frontPairs;
	_filtered_middle_line.getFrontPairNumbers(frontPairs);

	short numFrontPairs = (short)frontPairs.size();

	int sequenceRunCounter = 0;

	//TODO check [0]
	vector<short>::iterator frontPairIter;
	for(frontPairIter = frontPairs.begin(); frontPairIter != frontPairs.end(); frontPairIter++)
	{
		short frontPairNumber = *frontPairIter;

		vector<double> frontSeq;
		vector<double> rearSeq;
		double frontConstant	= 0;

		////////////////////////////////////////////////////////////

		_filtered_middle_line.getCurrentsInOneFrontPair(frontPairNumber,
											frontConstant,
											frontSeq,
											rearSeq);

		////////////////////////////////////////////////////////////
		
		numCycles = numCycles + (int)(rearSeq.size());

		////////////////////////////////////////////////////////////

		vector<double> multiplePhaseSequence;
		for(int j=0; j<(int)rearSeq.size(); j++)
		{
			vector<double>::iterator iter;
			for(iter=phaseSequence.begin();iter!=phaseSequence.end();iter++)
				multiplePhaseSequence.push_back(*iter);
		}

		matchToPhaseSequence(phaseSequence,
							frontSeq);

		matchToPhaseSequence(phaseSequence,
							rearSeq);


		////////////////////////////////////////////////////////////

		CString constantFrontModuleName = "";
		CString sequencedFrontModuleName = "";
		mapFrontPairNumberToModuleNames(frontPairNumber,
										constantFrontModuleName,
										sequencedFrontModuleName);

		////////////////////////////////////////////////////////////

		////////////////////////////////////////////////////////////
		/// Check for sequences which are too long and
		// need to be split
		//
		vector<CCurrentsVector> frontSeqs;
		vector<CCurrentsVector> rearSeqs;
		vector<CCurrentsVector> phaseSeqs;

		rearSeqs.clear();
		frontSeqs.clear();
		phaseSeqs.clear();

		splitSequence(MAX_SEQUENCE_SIZE,
				frontSeq,
				frontSeqs);				

		splitSequence(MAX_SEQUENCE_SIZE,
				rearSeq,
				rearSeqs);

		splitSequence(MAX_SEQUENCE_SIZE,
				multiplePhaseSequence,
				phaseSeqs);

		int numSplitSequences = (int)(rearSeqs.size());
		for(int i=0; i<numSplitSequences; i++)
		{

			SequenceRunSettings* runSettings = new SequenceRunSettings(sequenceRunCounter);
			sequenceRunCounter++;

			PXIT306SequenceSettings* pxit306SequenceSettings = NULL;
			PXIT305SequenceSettings* pxit305SequenceSettings = NULL;


			///////////////////////////////////////////////////////////////////////////////


			if(previousConstantFrontModuleName != "")
			{
				pxit305SequenceSettings = new PXIT305SequenceSettings(previousConstantFrontModuleName,
																	nullVector,
																	OUTPUT_OFF,
																	0,
																	NOT_IN_SEQUENCE,
																	_sourceDelay,
																	_measureDelay);
				runSettings->_PXIT305SequenceSettingsList->addToList(pxit305SequenceSettings);
				
			}

			pxit305SequenceSettings = new PXIT305SequenceSettings(constantFrontModuleName,
																nullVector,
																OUTPUT_ON,
																frontConstant,
																NOT_IN_SEQUENCE,
																_sourceDelay,
																_measureDelay);
			runSettings->_PXIT305SequenceSettingsList->addToList(pxit305SequenceSettings);

			pxit305SequenceSettings = new PXIT305SequenceSettings(sequencedFrontModuleName,
																frontSeqs[i]._currents,
																OUTPUT_ON,
																0,
																IN_SEQUENCE,
																_sourceDelay,
																_measureDelay);
			runSettings->_PXIT305SequenceSettingsList->addToList(pxit305SequenceSettings);

			pxit305SequenceSettings = new PXIT305SequenceSettings(CG_REG->get_DSDBR01_CMDC_rear_module_name(),
																rearSeqs[i]._currents,
																OUTPUT_ON,
																0,
																IN_SEQUENCE,
																_sourceDelay,
																_measureDelay);
			runSettings->_PXIT305SequenceSettingsList->addToList(pxit305SequenceSettings);

			pxit305SequenceSettings = new PXIT305SequenceSettings(CG_REG->get_DSDBR01_CMDC_phase_module_name(),
																phaseSeqs[i]._currents,
																OUTPUT_ON,
																0,
																IN_SEQUENCE,
																_sourceDelay,
																_measureDelay);
			runSettings->_PXIT305SequenceSettingsList->addToList(pxit305SequenceSettings);

			pxit306SequenceSettings = new PXIT306SequenceSettings(CG_REG->get_DSDBR01_CMDC_306II_module_name(),
																IN_SEQUENCE,
																(int)(rearSeqs[i]._currents.size()),
																_sourceDelay,
																_measureDelay);
			runSettings->_PXIT306SequenceSettingsList->addToList(pxit306SequenceSettings);

			///////////////////////////////////////////////////////////

			if( CG_REG->get_DSDBR01_SMDC_measure_photodiode_currents() )
			{
				pxit306SequenceSettings = new PXIT306SequenceSettings(CG_REG->get_DSDBR01_CMDC_306EE_module_name(),
																	IN_SEQUENCE,
																	(int)(rearSeqs[i]._currents.size()),
																	_sourceDelay,
																	_measureDelay);
				runSettings->_PXIT306SequenceSettingsList->addToList(pxit306SequenceSettings);
			}

			sequenceRunSettingsList->addToList(runSettings);
		}
		previousConstantFrontModuleName = constantFrontModuleName;

		for(int i_seq = 0 ; i_seq < (int)rearSeqs.size() ; i_seq++ )
			rearSeqs[i_seq].clear();
		for(int i_seq = 0 ; i_seq < (int)frontSeqs.size() ; i_seq++ )
			frontSeqs[i_seq].clear();
		for(int i_seq = 0 ; i_seq < (int)phaseSeqs.size() ; i_seq++ )
			phaseSeqs[i_seq].clear();
		rearSeqs.clear();
		frontSeqs.clear();
		phaseSeqs.clear();
	}

	//////////////////////////////////////////////////////////////////

	return rval;
}


void
CDSDBRSuperMode::
applyHysteresisToSequence(vector<double>& sequence)
{
	vector<double> sequenceReversed;
	vector<double>::iterator iter;
	for(iter=sequence.end()-1; iter!=sequence.begin(); iter--)
	{
		sequenceReversed.push_back(*iter);
	}
	// add last element
	sequenceReversed.push_back(*iter);

	for(iter=sequenceReversed.begin(); iter!=sequenceReversed.end(); iter++)
	{
		sequence.push_back(*iter);
	}
}

CDSDBRSuperMode::rcode
CDSDBRSuperMode::
mapFrontPairNumberToModuleNames(short		frontPairNumber,
								CString&	constantFrontModuleName,
								CString&	sequencedFrontModuleName)
{
	rcode rval = rcode::ok;

	switch(frontPairNumber)
	{
		case 1:
		{
			constantFrontModuleName		= CG_REG->get_DSDBR01_CMDC_front1_module_name();;
			sequencedFrontModuleName	= CG_REG->get_DSDBR01_CMDC_front2_module_name();;
		}
		break;
		case 2:
		{
			constantFrontModuleName		= CG_REG->get_DSDBR01_CMDC_front2_module_name();;
			sequencedFrontModuleName	= CG_REG->get_DSDBR01_CMDC_front3_module_name();;
		}
		break;
		case 3:
		{
			constantFrontModuleName		= CG_REG->get_DSDBR01_CMDC_front3_module_name();;
			sequencedFrontModuleName	= CG_REG->get_DSDBR01_CMDC_front4_module_name();;
		}
		break;
		case 4:
		{
			constantFrontModuleName		= CG_REG->get_DSDBR01_CMDC_front4_module_name();;
			sequencedFrontModuleName	= CG_REG->get_DSDBR01_CMDC_front5_module_name();;
		}
		break;
		case 5:
		{
			constantFrontModuleName		= CG_REG->get_DSDBR01_CMDC_front5_module_name();;
			sequencedFrontModuleName	= CG_REG->get_DSDBR01_CMDC_front6_module_name();;
		}
		break;
		case 6:
		{
			constantFrontModuleName		= CG_REG->get_DSDBR01_CMDC_front6_module_name();;
			sequencedFrontModuleName	= CG_REG->get_DSDBR01_CMDC_front7_module_name();;
		}
		break;
		case 7:
		{
			constantFrontModuleName		= CG_REG->get_DSDBR01_CMDC_front7_module_name();;
			sequencedFrontModuleName	= CG_REG->get_DSDBR01_CMDC_front8_module_name();;
		}
		break;
		default:
		{
			constantFrontModuleName		= "";
			sequencedFrontModuleName	= "";

			rval = rcode::invalid_front_pair_number;

			CGR_LOG("CDSDBRSuperMode::mapFrontPairNumberToModuleNames() invalid_front_pair_number", ERROR_CGR_LOG)
		}
		break;

	}

	return rval;
}

void
CDSDBRSuperMode::
matchToPhaseSequence(vector<double>		phaseSequence,
					 vector<double>&	sequence)
{
	int numPhasePoints = (int)phaseSequence.size();

	vector<double> adjustedSequence;

	vector<double>::iterator iter;
	for(iter=sequence.begin(); iter!=sequence.end(); iter++)
	{
		for(int i=0; i<numPhasePoints; i++)
			adjustedSequence.push_back(*iter);
	}

	sequence = adjustedSequence;
}

//////////////////////////////////////////////////////////////////////////////////////////////////

CDSDBRSuperMode::rcode
CDSDBRSuperMode::
runSequences(SequenceRunSettingsList*	runSettingsList,
			 int						numPointsPerCycle,
			 int						numCycles,
			 vector<double>*			total306IIPowerCh1,
			 vector<double>*			total306IIPowerCh2,
			 vector<double>*			total306EECurrentCh1,
			 vector<double>*			total306EECurrentCh2)
{
	rcode rval = rcode::ok;
	HRESULT err = S_OK;

	err = SERVERINTERFACE->resetSequences();

	double delta_percent = (double)90/(double)(runSettingsList->GetCount());
	double percent_complete = 0;


	if(err == S_OK)
	{
		for(int j=0; j<(int)runSettingsList->GetCount();j++)
		{
			percent_complete += delta_percent;
			setPercentComplete( (short)percent_complete );

			SequenceRunSettings* runSettings = NULL;
			runSettings = runSettingsList->findObject(j);
			if(runSettings)
			{
				if(err == S_OK)
					err = SERVERINTERFACE->resetSequences();

				if(err == S_OK)
					err = loadSequences(runSettingsList, j);

				if(err == S_OK)
					err = SERVERINTERFACE->resetSequences();

				if(err == S_OK)
					err = runSequence(runSettings);

				if(err == S_OK)
					err = SERVERINTERFACE->resetSequences();


				///////////////////////////////////////////////////////////////////////////

				vector<double> powerCh1;
				vector<double> powerCh2;
				if(err == S_OK)
				{
					// gather power
					err = SERVERINTERFACE->get306SequenceMeasurements(CG_REG->get_DSDBR01_CMDC_306II_module_name(),
																	&powerCh1,
																	&powerCh2);

					//for(int ijk = 0; ijk < 2*numPointsPerCycle*numCycles ; ijk++ )
					//{
					//	powerCh1.push_back(0);
					//	powerCh2.push_back(0);
					//}

				}

				if(err == S_OK)
				{
					vector<double>::iterator iter;
					for(iter = powerCh1.begin(); iter!= powerCh1.end(); iter++)
						total306IIPowerCh1->push_back(*iter);
					for(iter = powerCh2.begin(); iter!= powerCh2.end(); iter++)
						total306IIPowerCh2->push_back(*iter);
				}


				if(err == S_OK)
					err = SERVERINTERFACE->resetSequences();

				if( CG_REG->get_DSDBR01_SMDC_measure_photodiode_currents() )
				{
					vector<double> photoICh1;
					vector<double> photoICh2;
					if(err == S_OK)
					{
						//gather power
						err = SERVERINTERFACE->get306SequenceMeasurements(CG_REG->get_DSDBR01_CMDC_306EE_module_name(),
																		&photoICh1,
																		&photoICh2);
					}

					if(err == S_OK)
					{
						vector<double>::iterator iter;
						for(iter = photoICh1.begin(); iter!= photoICh1.end(); iter++)
							total306EECurrentCh1->push_back(*iter);
						for(iter = photoICh2.begin(); iter!= photoICh2.end(); iter++)
							total306EECurrentCh2->push_back(*iter);
					}
				}

				////////////////////////////////////////////////////////////////////////////////
			}	

			if(err)
				break;
		}

	}

	// reset everything back to 0
	for(int i=0; i<(int)(runSettingsList->GetCount()); i++)
	{
		SequenceRunSettings* runSettings = NULL;
		runSettings = runSettingsList->findObject(i);
		if(runSettings)
		{
			resetCurrentsAndDisconnect(runSettings);
			setNotInSequence(runSettings);
		}

	}

	if(err != S_OK)
		rval = mapHResultToRCode(err);


	return rval;
}

CDSDBRSuperMode::rcode
CDSDBRSuperMode::
loadSequences(SequenceRunSettingsList*	runSettingsList,
			  int	runNumber)
{
	rcode rval = rcode::ok;

	SequenceRunSettings* runSettings = NULL;
	runSettings = runSettingsList->findObject(runNumber);
	if(runSettings)
		rval = loadSequence(runSettings);

	return rval;
}

CDSDBRSuperMode::rcode
CDSDBRSuperMode::
loadSequence(SequenceRunSettings*	runSettings)
{
	rcode rval = rcode::ok;

	if(runSettings!=NULL)
	{
		if(rval == S_OK)
		{
			for(int i=0; i<(int)(runSettings->_PXIT305SequenceSettingsList->GetCount()); i++)
			{
				PXIT305SequenceSettings* sequenceSettings = NULL;
				sequenceSettings = runSettings->_PXIT305SequenceSettingsList->findObjectByIndex(i);
				if(sequenceSettings)
				{
					if(sequenceSettings->_inSequence == IN_SEQUENCE)
					{
						rval = load305Sequence(sequenceSettings->_moduleName,
											sequenceSettings->_currents,
											sequenceSettings->_sourceDelay,
											sequenceSettings->_measureDelay);
					}
				}

				if(rval)
					break;
			}
		}

		if(rval == S_OK)
		{
			for(int i=0; i<(int)(runSettings->_PXIT306SequenceSettingsList->GetCount()); i++)
			{
				PXIT306SequenceSettings* sequenceSettings = NULL;
				sequenceSettings = runSettings->_PXIT306SequenceSettingsList->findObjectByIndex(i);
				if(sequenceSettings)
				{

					if(sequenceSettings->_inSequence == IN_SEQUENCE)
					{
						rval = load306Sequence(sequenceSettings->_moduleName,
											sequenceSettings->_numPoints,
											sequenceSettings->_sourceDelay,
											sequenceSettings->_measureDelay);
					}
				}

				if(rval)
					break;
			}
		}
	}

	return rval;
}

CDSDBRSuperMode::rcode
CDSDBRSuperMode::
runSequence(SequenceRunSettings*	runSettings)
{
	rcode rval = rcode::ok;

	for(int i=0; i<(int)(runSettings->_PXIT305SequenceSettingsList->GetCount()); i++)
	{
		PXIT305SequenceSettings* sequenceSettings = NULL;
		sequenceSettings = runSettings->_PXIT305SequenceSettingsList->findObjectByIndex(i);
		if(sequenceSettings)
		{
			rval = setup305DeviceForRun(sequenceSettings);
		}

		if(rval)
			break;
	}
		
	for(int i=0; i<(int)(runSettings->_PXIT306SequenceSettingsList->GetCount()); i++)
	{
		PXIT306SequenceSettings* sequenceSettings = NULL;
		sequenceSettings = runSettings->_PXIT306SequenceSettingsList->findObjectByIndex(i);
		if(sequenceSettings)
		{
			rval = setup306DeviceForRun(sequenceSettings);
		}

		if(rval)
			break;
	}

	if(rval == rcode::ok)
	{
		HRESULT err = SERVERINTERFACE->runSequences();
		if(err != S_OK)
			rval = mapHResultToRCode(err);
	}


	return rval;
}

CDSDBRSuperMode::rcode
CDSDBRSuperMode::
resetCurrentsAndDisconnect(SequenceRunSettings* runSettings)
{
	rcode rval = rcode::ok;
	HRESULT err = S_OK;

	for(int i=0; i<(int)(runSettings->_PXIT305SequenceSettingsList->GetCount()); i++)
	{
		PXIT305SequenceSettings* sequenceSettings = NULL;
		sequenceSettings = runSettings->_PXIT305SequenceSettingsList->findObjectByIndex(i);
		if(sequenceSettings)
		{
			err = SERVERINTERFACE->setCurrent(sequenceSettings->_moduleName, 0);

			if(err == S_OK)
			{
				err = SERVERINTERFACE->setOutputConnection(sequenceSettings->_moduleName, OUTPUT_OFF);
			}
			if(err == S_OK)
			{
				err = SERVERINTERFACE->setInSequence(sequenceSettings->_moduleName, NOT_IN_SEQUENCE);
			}
		}

		if(err)
			break;
	}
		
	if(err != S_OK)
		rval = mapHResultToRCode(err);

	return rval;
}

CDSDBRSuperMode::rcode
CDSDBRSuperMode::
setNotInSequence(SequenceRunSettings* runSettings)
{
	rcode rval = rcode::ok;
	HRESULT err = S_OK;

	for(int i=0; i<(int)(runSettings->_PXIT305SequenceSettingsList->GetCount()); i++)
	{
		PXIT305SequenceSettings* sequenceSettings = NULL;
		sequenceSettings = runSettings->_PXIT305SequenceSettingsList->findObjectByIndex(i);
		if(sequenceSettings)
			err = SERVERINTERFACE->setInSequence(sequenceSettings->_moduleName, NOT_IN_SEQUENCE);

		if(err)
			break;
	}
		
	for(int i=0; i<(int)(runSettings->_PXIT306SequenceSettingsList->GetCount()); i++)
	{
		PXIT306SequenceSettings* sequenceSettings = NULL;
		sequenceSettings = runSettings->_PXIT306SequenceSettingsList->findObjectByIndex(i);
		if(sequenceSettings)
			err = SERVERINTERFACE->setInSequence(sequenceSettings->_moduleName, NOT_IN_SEQUENCE);

		if(err)
			break;
	}

	if(err != S_OK)
		rval = mapHResultToRCode(err);

	return rval;
}

CDSDBRSuperMode::rcode
CDSDBRSuperMode::
setup305DeviceForRun(PXIT305SequenceSettings* runSettings)
{
	rcode rval = rcode::ok;
	HRESULT err = S_OK;

	//TODO error checking

	if(runSettings)
	{
		// set output on or off
		if(err == S_OK)
		{
			err = SERVERINTERFACE->setOutputConnection(runSettings->_moduleName, 
													runSettings->_outputConnection);


		}

		// set in or outside sequence
		if(err == S_OK)
		{			
			err = SERVERINTERFACE->setInSequence(runSettings->_moduleName, 
												runSettings->_inSequence);

		}

		// if connected and not in sequence, set a constant current (may be zero)
		if(err == S_OK)
		{			
			if(runSettings->_inSequence == NOT_IN_SEQUENCE)
			{
				if(runSettings->_outputConnection == OUTPUT_ON)
				{
					err = SERVERINTERFACE->setCurrent(runSettings->_moduleName,
													runSettings->_constantCurrent);
				}
			}
		}

	}

	if(err != S_OK)
		rval = mapHResultToRCode(err);

	return rval;
}

CDSDBRSuperMode::rcode
CDSDBRSuperMode::
setup306DeviceForRun(PXIT306SequenceSettings* runSettings)
{
	rcode rval = rcode::ok;
	HRESULT err = S_OK;

	//TODO error checking

	if(runSettings)
	{
		// set in or outside sequence
		if(err == S_OK)
		{			
			err = SERVERINTERFACE->setInSequence(runSettings->_moduleName, 
												runSettings->_inSequence);
		}

	}

	if(err != S_OK)
		rval = mapHResultToRCode(err);

	return rval;
}

CDSDBRSuperMode::rcode
CDSDBRSuperMode::
load305Sequence(CString			moduleName,
				vector<double>	sequence,
				int				sourceDelay,
				int				measureDelay)
{
	rcode rval = rcode::ok;
	HRESULT err = S_OK;

	err = SERVERINTERFACE->load305Sequence(moduleName,
										sequence,
										sourceDelay,
										measureDelay);


	if(err != S_OK)
		rval = mapHResultToRCode(err);

	return rval;
}

CDSDBRSuperMode::rcode
CDSDBRSuperMode::
load306Sequence(CString		moduleName,
				int			numPoints,
				int			sourceDelay,
				int			measureDelay)
{
	rcode rval = rcode::ok;
	HRESULT err = S_OK;

	err = SERVERINTERFACE->load306Sequence(moduleName,
											numPoints,
											ALL_CHANNELS,
											sourceDelay,
											measureDelay);

	if(err != S_OK)
		rval = mapHResultToRCode(err);

	return rval;
}

CDSDBRSuperMode::rcode
CDSDBRSuperMode::
extractPowerData(bool				hysteresis,
				 int				numPointsPerCycle,
				 vector<double>		directPower,
				 vector<double>		filteredPower,
				 vector<double>&	forwardDirectPower,
				 vector<double>&	reverseDirectPower,
				 vector<double>&	hysteresisDirectPower,
				 vector<double>&	forwardFilteredPower,
				 vector<double>&	reverseFilteredPower,
				 vector<double>&	hysteresisFilteredPower,
				 vector<double>&	forwardPowerRatio,
				 vector<double>&	reversePowerRatio,
				 vector<double>&	hysteresisPowerRatio)
{
	rcode rval = rcode::ok;

	try
	{

		//////////////////////////////////////////////////////////////////

		forwardDirectPower.clear();
		reverseDirectPower.clear();
		hysteresisDirectPower.clear();
		
		forwardFilteredPower.clear();
		reverseFilteredPower.clear();
		hysteresisFilteredPower.clear();

		forwardPowerRatio.clear();
		reversePowerRatio.clear();
		hysteresisPowerRatio.clear();
		
		//////////////////////////////////////////////////////////////////

		int totalNumPowerPoints = (int)directPower.size();
		if(totalNumPowerPoints != (int)filteredPower.size())
		{
			rval = rcode::sequencing_error;
			CGR_LOG("CDSDBRSuperMode::extractPowerData() sequencing_error", ERROR_CGR_LOG)
		}

		////////////////////////////////////////////////////////////////////

		if(rval == rcode::ok)
		{
			//////////////////////////////////////////////////////////////////////////////////////////////////
			///
			//	Hysteresis
			//
			if(hysteresis==true)
			{
				rval = splitHysteresisPowerArray(directPower,
												numPointsPerCycle,
												forwardDirectPower,
												reverseDirectPower);

				if(rval == rcode::ok)
				{
					rval = splitHysteresisPowerArray(filteredPower,
													numPointsPerCycle,
													forwardFilteredPower,
													reverseFilteredPower);
				}	


				if(rval == rcode::ok)
				{
					// while it not possible to have more reverse points
					// than forward points, it is possible to have more 
					// forward points than reverse points. Therefore use
					// th enumber of reverse points for ratios
					int numReversePowerPoints = (int)reverseDirectPower.size();
					for(int i=0; i<numReversePowerPoints; i++)
					{
						hysteresisDirectPower.push_back(forwardDirectPower[i] - reverseDirectPower[i]);
						hysteresisFilteredPower.push_back(forwardFilteredPower[i] - reverseFilteredPower[i]);
					}
				}
			
				if(rval == rcode::ok)
				{
					int numPoints = (int)forwardFilteredPower.size();
					for(int i=0; i<numPoints; i++)
					{
						double p_ratio = forwardFilteredPower[i]/forwardDirectPower[i];

						if( p_ratio < 0 || p_ratio > 1 ) // invalid value for ratio
						{
							CString err_message("CDSDBRSuperMode::extractPowerData : ");
							err_message.AppendFormat("calculated forward p_ratio = %g",p_ratio);
							CGR_LOG(err_message,ERROR_CGR_LOG);

							rval = rcode::laser_alignment_error;
							break;
						}

						forwardPowerRatio.push_back( p_ratio );
					}
				}

				if(rval == rcode::ok)
				{
					int numReversePowerPoints = (int)reverseDirectPower.size();
					for(int i=0; i<numReversePowerPoints; i++)
					{

						double p_ratio = reverseFilteredPower[i]/reverseDirectPower[i];

						if( p_ratio < 0 || p_ratio > 1 ) // invalid value for ratio
						{
							CString err_message("CDSDBRSuperMode::extractPowerData : ");
							err_message.AppendFormat("calculated reverse p_ratio = %g",p_ratio);
							CGR_LOG(err_message,ERROR_CGR_LOG);

							rval = rcode::laser_alignment_error;
							break;
						}

						reversePowerRatio.push_back(p_ratio);
						hysteresisPowerRatio.push_back(forwardPowerRatio[i] - reversePowerRatio[i]);
					}
				}

			}

			//////////////////////////////////////////////////////////////////////////////////////////////////
			///
			//	No Hysteresis
			//
			if(hysteresis==false)
			{
				forwardDirectPower		= directPower;
				forwardFilteredPower	= filteredPower;

				for(int i=0; i<totalNumPowerPoints; i++)
				{
					double p_ratio = forwardFilteredPower[i]/forwardDirectPower[i];

					if( p_ratio < 0 || p_ratio > 1 ) // invalid value for ratio
					{
						CString err_message("CDSDBRSuperMode::extractPowerData : ");
						err_message.AppendFormat("calculated forward p_ratio = %g",p_ratio);
						CGR_LOG(err_message,ERROR_CGR_LOG);

						rval = rcode::laser_alignment_error;
						break;
					}

					forwardPowerRatio.push_back(p_ratio);
				}

			}

			//////////////////////////////////////////////////////////////////////////////////////////////////
		}	
		
		////////////////////////////////////////////////////////////////////////////////////////////////
	}
	catch(...)
	{
		rval = rcode::no_data;
		CGR_LOG("CDSDBRSuperMode::extractPowerData() no_data", ERROR_CGR_LOG)
	}

	return rval;
}

CDSDBRSuperMode::rcode
CDSDBRSuperMode::
splitHysteresisPowerArray(vector<double>	&powerVector,
						int					numPointsPerCycle,
						vector<double>		&forwardPower,
						vector<double>		&reversePower)
{
	rcode rval = rcode::ok;

	forwardPower.clear();
	reversePower.clear();

	try
	{
		///////////////////////////////////////////

		int				i			= 0;
		int				j			= 0;
		int				flipIndex	= 0;
		vector<double>	tempArray;

		int numElements = (int)(powerVector.size());
		while(i<numElements)
		{
			////////////////////////////////////////////////////

			j=0;
			while(j<numPointsPerCycle)
			{
				if(i<numElements)
					forwardPower.push_back(powerVector[i]);
				else
					break;

				j++;
				i++;
			}

			////////////////////////////////////////////////////

			j=0;
			tempArray.clear();
			while(j<numPointsPerCycle)
			{
				if(i<numElements)
					tempArray.push_back(powerVector[i]);
				else
					break;

				j++;
				i++;
			}

			if(tempArray.size() > 0)
			{
				// flip the temp array
				reverse(tempArray.begin(), tempArray.end());
				vector<double>::iterator iter;
				for(iter=tempArray.begin(); iter!=tempArray.end(); iter++)
					reversePower.push_back(*iter);
			}
			////////////////////////////////////////////////////

		}
	}
	catch(...)
	{
		rval = rcode::sequencing_error;
		CGR_LOG("CDSDBRSuperMode::splitHysteresisPowerArray() sequencing_error", ERROR_CGR_LOG)
	}

	return rval;
}

void
CDSDBRSuperMode::
setPercentComplete( short percent_complete )
{
	EnterCriticalSection(_p_data_protect);

	*_p_percent_complete = percent_complete;

	LeaveCriticalSection(_p_data_protect);

	return;
}

CDSDBRSuperMode::rcode
CDSDBRSuperMode::
writeMapsToFile()
{
	rcode rval = rcode::ok;

	// Write the supermode maps to file
	// The supermode maps are stored in files named
	// MATRIX_forwardPowerRatio_DSDBR[##]_[laser_ID]_[YYYYMMDDHHMMSS]_SM[i]_[P|M].csv
	// MATRIX_reversePowerRatio_DSDBR[##]_[laser_ID]_[YYYYMMDDHHMMSS]_SM[i]_[P|M].csv
	// MATRIX_forwardPowerRatio_DSDBR[##]_[laser_ID]_[YYYYMMDDHHMMSS]_SM[i]_[P|M].csv
	// MATRIX_forwardPower_DSDBR[##]_[laser_ID]_[YYYYMMDDHHMMSS]_SM[i]_[P|M].csv
	// MATRIX_reversePower_DSDBR[##]_[laser_ID]_[YYYYMMDDHHMMSS]_SM[i]_[P|M].csv
	// MATRIX_forwardPower_DSDBR[##]_[laser_ID]_[YYYYMMDDHHMMSS]_SM[i]_[P|M].csv
	// where i is the supermode number, indexed from 0
	//
	// The currents used to collect these maps are stored in 2 files
	// Im_OverallMap_DSDBR[##]_[laser_ID]_[YYYYMMDDHHMMSS]_SM[i].csv
	// Ip_OverallMap_DSDBR[##]_[laser_ID]_[YYYYMMDDHHMMSS]_SM[i].csv
	// where Im is the middle line current (combination of front and rear current)
	// and Ip is phase current

	//////////////////////////////////////////////////////////////////////////////

	

	CString file_path = _results_dir_CString;
	// Check for directory without trailing '\\' character
	if( file_path.GetAt( file_path.GetLength() - 1 ) != '\\' )
	{
		// Add trailing directory backslash '\\'
		file_path += CString( "\\" );
	}

	//////////////////////////////////////////////////////////////////////////////

	CString map_ramp_direction_CString;
	if( _Im_ramp )
		map_ramp_direction_CString = RAMP_DIRECTION_MIDDLE_LINE;
	else
		map_ramp_direction_CString = RAMP_DIRECTION_PHASE;

	CString smNumberAsCString = "";
	smNumberAsCString.Format("%d", _sm_number);

	//////////////////////////////////////////////////////

	CString fileNameString = "";
	fileNameString += USCORE;
	fileNameString += LASER_TYPE_ID_DSDBR01_CSTRING;
	fileNameString += USCORE;
	fileNameString += _laser_id_CString;
	fileNameString += USCORE;
	fileNameString += _date_time_stamp_CString;
	fileNameString += USCORE;
	fileNameString += CString("SM");
	fileNameString += smNumberAsCString;

	CString file_path_phase_currents = file_path + PHASE_CURRENT_CSTRING + fileNameString + CSV;
	CString file_path_middle_line_currents = file_path + MIDDLELINE_CURRENT_CSTRING + fileNameString + CSV;

	fileNameString += USCORE;
	fileNameString += map_ramp_direction_CString;
	fileNameString += CSV;

	//////////////////////////////////////////////////////////////////////////////

	CString file_path_forward_power = file_path + MATRIX_ + MEAS_TYPE_ID_FORWARDPOWER_CSTRING + fileNameString;
	CString file_path_reverse_power = file_path + MATRIX_ + MEAS_TYPE_ID_REVERSEPOWER_CSTRING + fileNameString;
	CString file_path_hysteresis_power = file_path + MATRIX_ + MEAS_TYPE_ID_HYSTERESISPOWER_CSTRING + fileNameString;

	CString file_path_forward_power_ratio = file_path + MATRIX_ + MEAS_TYPE_ID_FORWARDPOWERRATIO_CSTRING + fileNameString;
	CString file_path_reverse_power_ratio = file_path + MATRIX_ + MEAS_TYPE_ID_REVERSEPOWERRATIO_CSTRING + fileNameString;
	CString file_path_hysteresis_power_ratio = file_path + MATRIX_ + MEAS_TYPE_ID_HYSTERESISPOWERRATIO_CSTRING + fileNameString;

	CString file_path_forward_photodiode1_current = file_path + MATRIX_ + MEAS_TYPE_ID_FORWARDPD1_CSTRING + fileNameString;
	CString file_path_reverse_photodiode1_current = file_path + MATRIX_ + MEAS_TYPE_ID_REVERSEPD1_CSTRING + fileNameString;
	CString file_path_forward_photodiode2_current = file_path + MATRIX_ + MEAS_TYPE_ID_FORWARDPD2_CSTRING + fileNameString;
	CString file_path_reverse_photodiode2_current = file_path + MATRIX_ + MEAS_TYPE_ID_REVERSEPD2_CSTRING + fileNameString;

	CString file_path_forward_filtered_power = file_path + MATRIX_ + MEAS_TYPE_ID_FORWARDFILTEREDPOWER_CSTRING + fileNameString;
	CString file_path_reverse_filtered_power = file_path + MATRIX_ + MEAS_TYPE_ID_REVERSEFILTEREDPOWER_CSTRING + fileNameString;
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// write to file
	_map_forward_direct_power.writeToFile(
		file_path_forward_power,
		CG_REG->get_DSDBR01_SMDC_overwrite_existing_files() );

	_map_reverse_direct_power.writeToFile(
		file_path_reverse_power,
		CG_REG->get_DSDBR01_SMDC_overwrite_existing_files());

	_map_hysteresis_direct_power.writeToFile(
		file_path_hysteresis_power,
		CG_REG->get_DSDBR01_SMDC_overwrite_existing_files());

	_map_forward_power_ratio.writeToFile(
		file_path_forward_power_ratio,
		CG_REG->get_DSDBR01_SMDC_overwrite_existing_files());

	_map_reverse_power_ratio.writeToFile(
		file_path_reverse_power_ratio,
		CG_REG->get_DSDBR01_SMDC_overwrite_existing_files());

	_map_hysteresis_power_ratio.writeToFile(
		file_path_hysteresis_power_ratio,
		CG_REG->get_DSDBR01_SMDC_overwrite_existing_files());

	//write phase currents to file

	_phase_current.writeToFile(
		file_path_phase_currents,
		CG_REG->get_DSDBR01_SMDC_overwrite_existing_files());

	_filtered_middle_line.writeCurrentsToFile(
		file_path_middle_line_currents,
		CG_REG->get_DSDBR01_SMDC_overwrite_existing_files());

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	if( CG_REG->get_DSDBR01_SMDC_measure_photodiode_currents() )
	{
		_map_forward_photodiode1_current.writeToFile(
			file_path_forward_photodiode1_current,
			CG_REG->get_DSDBR01_SMDC_overwrite_existing_files());

		_map_reverse_photodiode1_current.writeToFile(
			file_path_reverse_photodiode1_current,
			CG_REG->get_DSDBR01_SMDC_overwrite_existing_files());

		_map_forward_photodiode2_current.writeToFile(
			file_path_forward_photodiode2_current,
			CG_REG->get_DSDBR01_SMDC_overwrite_existing_files());

		_map_reverse_photodiode2_current.writeToFile(
			file_path_reverse_photodiode2_current,
			CG_REG->get_DSDBR01_SMDC_overwrite_existing_files());
	}


	if( CG_REG->get_DSDBR01_SMDC_write_filtered_power_maps() )
	{
		_map_forward_filtered_power.writeToFile(
			file_path_forward_filtered_power,
			CG_REG->get_DSDBR01_SMDC_overwrite_existing_files());

		_map_reverse_filtered_power.writeToFile(
			file_path_reverse_filtered_power,
			CG_REG->get_DSDBR01_SMDC_overwrite_existing_files());
	}

	return rval;
}


CDSDBRSuperMode::rcode
CDSDBRSuperMode::loadMapsFromFile(
	CString results_dir_CString,
	CString laser_id_CString,
	CString date_time_stamp_CString,
	CString map_ramp_direction_CString )
{
	// Read the supermode maps from file.
	// The supermode maps are stored in files named
	// MATRIX_forwardPowerRatio_DSDBR[##]_[laser_ID]_[YYYYMMDDHHMMSS]_SM[i]_[P|M].csv
	// MATRIX_reversePowerRatio_DSDBR[##]_[laser_ID]_[YYYYMMDDHHMMSS]_SM[i]_[P|M].csv
	// MATRIX_forwardPowerRatio_DSDBR[##]_[laser_ID]_[YYYYMMDDHHMMSS]_SM[i]_[P|M].csv
	// MATRIX_forwardPower_DSDBR[##]_[laser_ID]_[YYYYMMDDHHMMSS]_SM[i]_[P|M].csv
	// MATRIX_reversePower_DSDBR[##]_[laser_ID]_[YYYYMMDDHHMMSS]_SM[i]_[P|M].csv
	// MATRIX_forwardPower_DSDBR[##]_[laser_ID]_[YYYYMMDDHHMMSS]_SM[i]_[P|M].csv
	// where i is the supermode number, indexed from 0
	//
	// The currents used to collect these maps are stored in 2 files
	// Im_OverallMap_DSDBR[##]_[laser_ID]_[YYYYMMDDHHMMSS]_SM[i].csv
	// Ip_OverallMap_DSDBR[##]_[laser_ID]_[YYYYMMDDHHMMSS]_SM[i].csv
	// where Im is the middle line current (combination of front and rear current)
	// and Ip is phase current

	CGR_LOG("CDSDBRSuperMode::loadMapsFromFile() entered", DIAGNOSTIC_CGR_LOG)

	rcode rval = rcode::ok;

	CString file_path = results_dir_CString;

	// Check for directory without trailing '\\' character
	if( file_path.GetAt( file_path.GetLength() - 1 ) != '\\' )
	{
		// Add trailing directory backslash '\\'
		file_path += CString( "\\" );
	}

	CString smNumberAsCString = "";
	smNumberAsCString.Format("%d", _sm_number);

	//////////////////////////////////////////////////////

	CString fileNameString = "";
	fileNameString += USCORE;
	fileNameString += LASER_TYPE_ID_DSDBR01_CSTRING;
	fileNameString += USCORE;
	fileNameString += laser_id_CString;
	fileNameString += USCORE;
	fileNameString += date_time_stamp_CString;
	fileNameString += USCORE;
	fileNameString += CString("SM");
	fileNameString += smNumberAsCString;

	CString file_path_phase_currents = file_path + PHASE_CURRENT_CSTRING + fileNameString + CSV;
	CString file_path_middle_line_currents = file_path + MIDDLELINE_CURRENT_CSTRING + fileNameString + CSV;

	fileNameString += USCORE;
	fileNameString += map_ramp_direction_CString;
	fileNameString += CSV;

	CString file_path_forward_power = file_path + MATRIX_ + MEAS_TYPE_ID_FORWARDPOWER_CSTRING + fileNameString;
	CString file_path_reverse_power = file_path + MATRIX_ + MEAS_TYPE_ID_REVERSEPOWER_CSTRING + fileNameString;
	CString file_path_hysteresis_power = file_path + MATRIX_ + MEAS_TYPE_ID_HYSTERESISPOWER_CSTRING + fileNameString;

	CString file_path_forward_power_ratio = file_path + MATRIX_ + MEAS_TYPE_ID_FORWARDPOWERRATIO_CSTRING + fileNameString;
	CString file_path_reverse_power_ratio = file_path + MATRIX_ + MEAS_TYPE_ID_REVERSEPOWERRATIO_CSTRING + fileNameString;
	CString file_path_hysteresis_power_ratio = file_path + MATRIX_ + MEAS_TYPE_ID_HYSTERESISPOWERRATIO_CSTRING + fileNameString;


	// have file names, now try to read them

	CLaserModeMap::rcode load_map_from_file_rval = CLaserModeMap::rcode::ok;
	CCurrentsVector::rcode load_Ip_from_file_rval = CCurrentsVector::rcode::ok;
	CDSDBROverallModeMapLine::rcode load_Im_from_file_rval = CDSDBROverallModeMapLine::rcode::ok;

	if( load_map_from_file_rval == CLaserModeMap::rcode::ok )
		_map_forward_direct_power.readFromFile( file_path_forward_power );

	if( load_map_from_file_rval == CLaserModeMap::rcode::ok )
		_map_reverse_direct_power.readFromFile( file_path_reverse_power );

	if( load_map_from_file_rval == CLaserModeMap::rcode::ok )
		_map_hysteresis_direct_power.readFromFile( file_path_hysteresis_power );

	if( load_map_from_file_rval == CLaserModeMap::rcode::ok )
		load_map_from_file_rval = _map_forward_power_ratio.readFromFile( file_path_forward_power_ratio );

	if( load_map_from_file_rval == CLaserModeMap::rcode::ok )
	{
		_map_hysteresis_power_ratio.readFromFile( file_path_hysteresis_power_ratio );
		load_map_from_file_rval = _map_reverse_power_ratio.readFromFile( file_path_reverse_power_ratio );

		if( load_map_from_file_rval != CLaserModeMap::rcode::ok )
		{
			if( load_map_from_file_rval != CLaserModeMap::rcode::ok
				&& _map_forward_power_ratio._map.size() == _map_hysteresis_power_ratio._map.size() )
			{ // Found _map_hysteresis_power_ratio _map_forward_power_ratio
			// Need to calculate _map_reverse_power_ratio
				
				vector<double> reversePowerRatio;
				reversePowerRatio.clear();

				for( unsigned long i = 0 ; i < _map_forward_power_ratio._map.size() ; i++ )
				{
					reversePowerRatio.push_back( 
						_map_forward_power_ratio._map[i] - _map_hysteresis_power_ratio._map[i] );
				}

				_map_reverse_power_ratio.clear();
				_map_reverse_power_ratio.setMap( reversePowerRatio,
					_map_forward_power_ratio._rows, _map_forward_power_ratio._cols );

				load_map_from_file_rval = CLaserModeMap::rcode::ok; // calculated instead of read from file
			}
		}
	}

	if( load_map_from_file_rval == CLaserModeMap::rcode::ok )
	{
		load_Ip_from_file_rval = _phase_current.readFromFile( file_path_phase_currents );

		load_Im_from_file_rval = _filtered_middle_line.readCurrentsFromFile( file_path_middle_line_currents );
	}

	// map error codes

	if( load_map_from_file_rval == CLaserModeMap::rcode::file_does_not_exist
	 || load_Ip_from_file_rval == CCurrentsVector::rcode::file_does_not_exist
	 || load_Im_from_file_rval == CDSDBROverallModeMapLine::rcode::file_does_not_exist )
	{
		rval = rcode::file_does_not_exist;
		CGR_LOG("CDSDBRSuperMode::loadMapsFromFile() file_does_not_exist", ERROR_CGR_LOG)
	}
	else
	if( load_map_from_file_rval == CLaserModeMap::rcode::could_not_open_file
	 || load_Ip_from_file_rval == CCurrentsVector::rcode::could_not_open_file
	 || load_Im_from_file_rval == CDSDBROverallModeMapLine::rcode::could_not_open_file )
	{
		rval = rcode::could_not_open_file;
		CGR_LOG("CDSDBRSuperMode::loadMapsFromFile() could_not_open_file", ERROR_CGR_LOG)
	}
	else
	if( load_map_from_file_rval == CLaserModeMap::rcode::file_format_invalid
	 || load_map_from_file_rval == CLaserModeMap::rcode::vector_size_rows_cols_do_not_match
	 || load_Ip_from_file_rval == CCurrentsVector::rcode::file_format_invalid
	 || load_Ip_from_file_rval == CCurrentsVector::rcode::vector_size_rows_cols_do_not_match
	 || load_Im_from_file_rval == CDSDBROverallModeMapLine::rcode::file_format_invalid )
	{
		rval = rcode::file_format_invalid;
		CGR_LOG("CDSDBRSuperMode::loadMapsFromFile() file_format_invalid", ERROR_CGR_LOG)
	}

	if( rval == rcode::ok )
	{
		// have read in files
		// check maps are of matching size

		if( map_ramp_direction_CString == RAMP_DIRECTION_MIDDLE_LINE )
			_Im_ramp = true;
		else
			_Im_ramp = false;

		if((_Im_ramp && ( ( _phase_current._length != _map_forward_power_ratio._rows )
					   || ( _phase_current._length != _map_reverse_power_ratio._rows )
					   || ( (long)(_filtered_middle_line._points.size()) != _map_forward_power_ratio._cols )
					   || ( (long)(_filtered_middle_line._points.size()) != _map_reverse_power_ratio._cols )))
		||(!_Im_ramp && ( ( _phase_current._length != _map_forward_power_ratio._cols )
					   || ( _phase_current._length != _map_reverse_power_ratio._cols )
					   || ( (long)(_filtered_middle_line._points.size()) != _map_forward_power_ratio._rows )
					   || ( (long)(_filtered_middle_line._points.size()) != _map_reverse_power_ratio._rows ))))
		{
			// maps are not of matching size
			rval = rcode::files_not_matching_sizes;
			CGR_LOG("CDSDBRSuperMode::loadMapsFromFile() files_not_matching_sizes", ERROR_CGR_LOG)
		}
	}

	if( rval == rcode::ok )
	{
		// have successfully loaded the essential files

		_results_dir_CString = results_dir_CString;
		_laser_id_CString = laser_id_CString;
		_date_time_stamp_CString = date_time_stamp_CString;

		_rows = (long)(_filtered_middle_line._points.size());
		_cols = _phase_current._length;

		_xAxisLength = (int)_cols;
		_yAxisLength = (int)_rows;

		_maps_loaded = true;
	}
	else
	{
		clearMapsAndLines();
		_maps_loaded = false;
		CGR_LOG("CDSDBRSuperMode::loadMapsFromFile() maps not loaded", ERROR_CGR_LOG)
	}

	CGR_LOG("CDSDBRSuperMode::loadMapsFromFile() exiting", DIAGNOSTIC_CGR_LOG)

	return rval;
}

CDSDBRSuperMode::rcode
CDSDBRSuperMode::
splitSequence(int			maxSequenceSize,
	vector<double>&			sequence,
	vector<CCurrentsVector>&	splitSequences)
{
	rcode rval = rcode::ok;

	int numSequences = 0;

	div_t result = div(((int)sequence.size()), maxSequenceSize);
	if(result.rem > 0)
		numSequences  = result.quot + 1;
	else
		numSequences  = result.quot;


	int sequenceIndex = 0;
	for(int sequenceNumber=0; sequenceNumber<numSequences; sequenceNumber++)
	{
		CCurrentsVector splitSequence;
		splitSequences.push_back(splitSequence);	

		for(int i=0; i<maxSequenceSize; i++)
		{
			if(sequenceIndex<(int)(sequence.size()))
				splitSequences[sequenceNumber]._currents.push_back(sequence[sequenceIndex]);
			else
				break;

			sequenceIndex++;
		}

	}


	return rval;
}

CDSDBRSuperMode::rcode
CDSDBRSuperMode::
mapHResultToRCode(HRESULT err)
{
	rcode rval = rcode::ok;

	switch(err)
	{
		case S_OK:
			rval = rcode::ok;
			break;
		case CG_ERROR_SYSTEM_ERROR:
			rval = rcode::system_error;
			break;			
		case CG_ERROR_CONNECTIVITY_FAILED:
			rval = rcode::connectivity_error;
			break;
		case CG_ERROR_POWER_METER_MISSING:
			rval = rcode::power_meter_not_found;
			break;
		case CG_ERROR_WAVEMETER_OFFLINE:
			rval = rcode::wavemeter_not_found;
			break;
		case CG_ERROR_TEC_OFFLINE:
			rval = rcode::tec_not_found;
			break;
		case CG_ERROR_PXIT_READPOWER:
			rval = rcode::power_read_error;
			break;
		case CG_ERROR_DUPLICATE_DEVICE_NAMES:
			rval = rcode::duplicate_device_name;
			break;
		case CG_ERROR_EMPTY_SLOTS_IN_SEQUENCE:
			rval = rcode::empty_sequence_slots;
			break;
		case CG_ERROR_PXIT_MODULES_UNINITIALISED:
		case CG_ERROR_PXIT_MODULE_MISSING:
		case CG_ERROR_NO_PXIT_MODULES_FOUND:
			rval = rcode::pxit_module_error;
			break;
		case CG_ERROR_DEVICE_NAME_NOT_ON_CARD:
			rval = rcode::device_name_not_on_card;
			break;
		case CG_ERROR_306_FIRMWARE_OBSOLETE:
			rval = rcode::device_306_firmware_obsolete;
			break;
		case CG_ERROR_LASER_ALIGN_LOWPOWER:
		case CG_ERROR_LASER_ALIGN_MAXPOWER:
			rval = rcode::laser_alignment_error;
			break;
		case CG_ERROR_OUTPUT_NOT_CONNECTED:
		case CG_ERROR_SET_OUTPUT_RANGE:
		case CG_ERROR_SET_COMPLIANCE_VOLTAGE:
		case CG_ERROR_SET_OUTPUT_MODE:
		case CG_ERROR_SET_MAX_CURRENT:
			rval = rcode::hardware_error;
			break;
		case CG_ERROR_LOGFILE_OPEN:
		case CG_ERROR_LOGFILE_DISK_FULL:
		case CG_ERROR_LOGFILE_CLOSE:
		case CG_ERROR_LOGFILEPATHNAME_REGISTRY_KEY:
		case CG_ERROR_LOGFILEPATHNAME_REGISTRY_DATA:
		case CG_ERROR_LOGGINGLEVEL_REGISTRY_KEY:
		case CG_ERROR_LOGGINGLEVEL_REGISTRY_DATA:
		case CG_ERROR_LOGFILEMAXSIZE_REGISTRY_KEY:
		case CG_ERROR_LOGFILEMAXSIZE_REGISTRY_DATA:
			rval = rcode::log_file_error;
			break;
		case CG_ERROR_PXIT_SETCURRENT:
			rval = rcode::set_current;
			break;
		case CG_ERROR_REGISTRY_DATA:
		case CG_ERROR_REGISTRY_KEY:
			rval = rcode::registry_error;
			break;
		case CG_ERROR_RESULTS_DISK_FULL:
			rval = rcode::results_disk_full;
			break;
		case CG_ERROR_PXIT_STEP_SIZE:
			rval = rcode::sequencing_step_size_error;
		break;
		case CG_ERROR_PXIT_SEQUENCE:
			rval = rcode::sequencing_error;
		break;
		case CG_ERROR_MODULE_NAME_NOT_FOUND:
			rval = rcode::module_name_not_found_error;
		break;
		default:
			rval = rcode::unknown_error;
			break;
	}

	return rval;
}


CDSDBRSuperMode::rcode
CDSDBRSuperMode::screen()
{
	rcode rval = rcode::ok;

	CGR_LOG("CDSDBRSuperMode::screen() entered",DIAGNOSTIC_CGR_LOG)

	if( _Im_ramp )
	{ // not yet supported

		CGR_LOG("CDSDBRSuperMode::screen() screening of middle-line ramped data is not yet supported",ERROR_CGR_LOG)
			
		rval = rcode::screening_failed;
	}


	if( rval == rcode::ok )
	{
		if( CG_REG->get_DSDBR01_SMBD_read_boundaries_from_file() )
		{
			// Step 1: Read in boundaries from file
			rval = readBoundariesFromFile();
		}
		else
		{
			// Step 1: Find boundaries
			rval = findLMBoundaries();
		}
	}

	_lm_upper_lines_removed = (short)_lm_upper_lines.size();
	_lm_lower_lines_removed = (short)_lm_lower_lines.size();

	// Step 2: Find middle lines of frequency in longitudinal modes
	if( rval == CDSDBRSuperMode::rcode::ok )
		rval = findLMMiddleLines();

	//_lm_middle_lines_of_frequency_removed -= (short)_lm_middle_lines.size();

	// Step 3: Filter middle lines of frequency in longitudinal modes
	if( rval == CDSDBRSuperMode::rcode::ok )
		rval = filterLMMiddleLines();

	// Step 5: Shift middle lines of frequency in longitudinal modes
	if( rval == CDSDBRSuperMode::rcode::ok )
		rval = calcLMMiddleLinesOfFreq();

	// Step 6: Check each middle line to ensure
	// it is in one longitudinal mode,
	// if not, remove the longitudinal mode
	if( rval == CDSDBRSuperMode::rcode::ok )
		rval = checkLMMiddleLinesOfFreq();

	_lm_upper_lines_removed -= (short)_lm_upper_lines.size();
	_lm_lower_lines_removed -= (short)_lm_lower_lines.size();
	//_lm_middle_lines_of_frequency_removed -= (short)_lm_middle_lines_of_frequency.size();


	if( rval == CDSDBRSuperMode::rcode::ok )
	{
		//////////////////////////////////////////////////////////////////////////////
		/////
		////
		//setContinuityValues();
		
		vector<double> xPoints;
		vector<double> yPoints;
		bool hasChangeOfFrontPairs;

		vector<ModeBoundaryLine> upperLines;
		vector<ModeBoundaryLine> lowerLines;
		vector<ModeBoundaryLine> middleLines;

		for(int i=0; i<(int)_lm_upper_lines.size(); i++)
		{
			xPoints.clear();
			yPoints.clear();

			xPoints = _lm_upper_lines[i].getCols();
			yPoints = _lm_upper_lines[i].getRows();
			hasChangeOfFrontPairs = _lm_upper_lines[i].hasChangeOfFrontPairs();

			ModeBoundaryLine upperLine(xPoints, yPoints, hasChangeOfFrontPairs);
			upperLines.push_back(upperLine);
		}

		for(int i=0; i<(int)_lm_lower_lines.size(); i++)
		{
			xPoints.clear();
			yPoints.clear();

			xPoints = _lm_lower_lines[i].getCols();
			yPoints = _lm_lower_lines[i].getRows();
			hasChangeOfFrontPairs = _lm_lower_lines[i].hasChangeOfFrontPairs();

			ModeBoundaryLine lowerLine(xPoints, yPoints, hasChangeOfFrontPairs);
			lowerLines.push_back(lowerLine);
		}

		for(int i=0; i<(int)_lm_middle_lines_of_frequency.size(); i++)
		{
			xPoints.clear();
			yPoints.clear();

			xPoints = _lm_middle_lines_of_frequency[i].getCols();
			yPoints = _lm_middle_lines_of_frequency[i].getRows();
			hasChangeOfFrontPairs = _lm_lower_lines[i].hasChangeOfFrontPairs();

			ModeBoundaryLine middleLine(xPoints, yPoints, hasChangeOfFrontPairs);
			middleLines.push_back(middleLine);
		}

		vector<double> _continuityValues;
		_superModeMapAnalysis.init();

		SuperModeMapAnalysis::rcode rval_sm_qa = 
			_superModeMapAnalysis.runAnalysis(_sm_number,
										upperLines,
										lowerLines,
										middleLines,
										_lm_upper_lines_removed,
										_lm_lower_lines_removed,
										_lm_middle_lines_of_frequency_removed,
										_xAxisLength,
										_yAxisLength,
										_continuityValues,
										_results_dir_CString,
										_laser_id_CString,
										_date_time_stamp_CString);

		if( rval_sm_qa != SuperModeMapAnalysis::rcode::ok )
			rval = rcode::screening_failed;

		PassFailCollated::instance()->addAnalysedSuperMode(this);

		////
		///////////////////////////////////////////////////////////////////////////////
	}

	if( rval != rcode::ok )
	{
		CString log_err_msg("CDSDBRSuperMode::screen(): error = ");
		log_err_msg += CString( get_error_pair(rval).second.c_str() );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		rval = rcode::screening_failed;
	}

	CGR_LOG("CDSDBRSuperMode::screen() exiting",DIAGNOSTIC_CGR_LOG)

	return rval;
}


CDSDBRSuperMode::rcode
CDSDBRSuperMode::findLMBoundaries()
{
	CGR_LOG("CDSDBRSuperMode::findLMBoundaries() entered",DIAGNOSTIC_CGR_LOG)

	rcode rval = rcode::ok;

	if( !_maps_loaded )
	{
		rval = rcode::maps_not_loaded;
	}

	if( rval == rcode::ok )
	{
		rval = applyMedianFilter(
			_map_forward_power_ratio,
			_map_forward_power_ratio_median_filtered,
			(unsigned int)CG_REG->get_DSDBR01_SMBD_median_filter_rank() );
	}

	if( rval == rcode::ok )
	{
		rval = applyMedianFilter(
			_map_reverse_power_ratio,
			_map_reverse_power_ratio_median_filtered,
			(unsigned int)CG_REG->get_DSDBR01_SMBD_median_filter_rank() );
	}

	if( rval == rcode::ok )
	{
		// find the mode jumps on each forward ramp
		rval = findJumps(_map_forward_power_ratio_median_filtered, true );

		CString log_msg("CDSDBRSuperMode::findLMBoundaries() ");
		log_msg.AppendFormat("found %d jumps on forward map",
			_map_forward_power_ratio_median_filtered._boundary_points.size() );
		CGR_LOG(log_msg,INFO_CGR_LOG)
	}

	if( rval == rcode::ok )
	{
		// find the mode jumps on each reverse ramp
		rval = findJumps(_map_reverse_power_ratio_median_filtered, false );

		CString log_msg("CDSDBRSuperMode::findLMBoundaries() ");
		log_msg.AppendFormat("found %d jumps on reverse map",
			_map_reverse_power_ratio_median_filtered._boundary_points.size() );
		CGR_LOG(log_msg,INFO_CGR_LOG)
	}

	if( rval == rcode::ok )
	{
		rval = findZDPRCBoundaries();
	}

	if( rval == rcode::ok )
	{
		rval = linkJumps();
	}

	if( rval == rcode::ok && CG_REG->get_DSDBR01_SMBD_extrapolate_to_map_edges() )
	{
		rval = insertExtrapolatedMapBorderPoints();
	}

	if( rval == rcode::ok )
	{
		rval = createLMBoundaryLines();
	}

	CGR_LOG("CDSDBRSuperMode::findLMBoundaries() exiting",DIAGNOSTIC_CGR_LOG)

	return rval;
}


CDSDBRSuperMode::rcode
CDSDBRSuperMode::applyMedianFilter(
	CLaserModeMap &map_in,
	CLaserModeMap &filtered_map_out,
	unsigned int rank )
{
	CGR_LOG("CDSDBRSuperMode::applyMedianFilter() entered",DIAGNOSTIC_CGR_LOG)

	rcode rval = rcode::ok;

	VectorAnalysis::medianFilter(
		map_in._map,
		filtered_map_out._map,
		rank,
		(unsigned long)(map_in._cols) );

	// update the rows and cols

	filtered_map_out._rows = map_in._rows;
	filtered_map_out._cols = map_in._cols;

	if( CG_REG->get_DSDBR01_SMBD_write_debug_files() )
	{
		// Assume _abs_filepath is set from loading data from file
		// or from writing to file after gathering data from laser
		CString abs_filepath = map_in._abs_filepath;
		abs_filepath.Replace( MATRIX_ , MATRIX_FILTERED_ );

		// Write filtered map to file
		filtered_map_out.writeToFile( abs_filepath, 
			true );
	}

	CGR_LOG("CDSDBRSuperMode::applyMedianFilter() exiting",DIAGNOSTIC_CGR_LOG)

	return rval;
}


CDSDBRSuperMode::rcode
CDSDBRSuperMode::findJumps(
	CLaserModeMap &map_in,
	bool forward_map )
{
	CGR_LOG("CDSDBRSuperMode::findJumps() entered",DIAGNOSTIC_CGR_LOG)

	rcode rval = rcode::ok;
	std::vector<double>::iterator i_Pr_start = map_in._map.begin();
	map_in._boundary_points.clear();

	std::vector<double> I_ramp;
	unsigned long rows = 0;
	unsigned long cols = 0;

	if( _Im_ramp ) // middle line ramped
	{
		rows =(unsigned long)(_phase_current._currents.size());
		cols =(unsigned long)(_filtered_middle_line._points.size());

		for( unsigned long i = 0; i < cols; i++ )
			I_ramp.push_back( _filtered_middle_line._points[i].I_rear );
	}
	else // phase ramped
	{
		rows =(unsigned long)(_filtered_middle_line._points.size());
		cols =(unsigned long)(_phase_current._currents.size());

		for( unsigned long i = 0; i < cols; i++ )
			I_ramp.push_back( _phase_current._currents[i] );
	}

	CCurrentsVector::rcode read_ramp_rval = _boundary_detection_ramp.readFromFile( 
		CG_REG->get_DSDBR01_SMBD_ramp_abspath() );

	if( read_ramp_rval == CCurrentsVector::rcode::ok )
	{
		if( (_boundary_detection_ramp._currents.size()) != cols )
		{
			CGR_LOG("CDSDBRSuperMode::findJumps() : _boundary_detection_ramp does not match current ramp",DIAGNOSTIC_CGR_LOG)
			CGR_LOG("CDSDBRSuperMode::findJumps() : using current ramp for boundary detection",DIAGNOSTIC_CGR_LOG)
		}
		else
		{
			CGR_LOG("CDSDBRSuperMode::findJumps() : _boundary_detection_ramp loaded from file",DIAGNOSTIC_CGR_LOG)
			CGR_LOG("CDSDBRSuperMode::findJumps() : using _boundary_detection_ramp for boundary detection",DIAGNOSTIC_CGR_LOG)

			I_ramp.clear();
			for( unsigned long i = 0; i < cols; i++ )
				I_ramp.push_back( _boundary_detection_ramp._currents[i] );
		}
	}
	else
	{
		CGR_LOG("CDSDBRSuperMode::findJumps() : _boundary_detection_ramp not loaded from file",DIAGNOSTIC_CGR_LOG)
		CGR_LOG("CDSDBRSuperMode::findJumps() : using current ramp for boundary detection",DIAGNOSTIC_CGR_LOG)
	}



	// iterate though each line of the map

	for( unsigned long row = 0; row < rows ; row++ )
	{
		unsigned long index_of_1st_point_on_line = row*cols;

		// find boundary points on a forward ramped line and store them

		modeBoundaries(
			i_Pr_start,					// starting point on Pr vector
			I_ramp.begin(),				// starting point on I vector
			index_of_1st_point_on_line,	// index of first point within a map
			1,							// increment on vectors to next point
			cols,						// number of points in the line
			map_in._boundary_points,	// structure to store boundary points
			forward_map );				// forward map (true) or reverse map (false)  

		i_Pr_start += map_in._cols; // iterate to start of next line
	}

	if( CG_REG->get_DSDBR01_SMBD_write_debug_files() )
	{
		// Assume _abs_filepath is set from loading data from file
		// or from writing to file after gathering data from laser
		CString abs_filepath = map_in._abs_filepath;
		abs_filepath.Replace( MATRIX_ , JUMPS_ );

		// Write jumps detected in filtered map to file
		map_in.writeJumpsToFile( abs_filepath );
	}

	CGR_LOG("CDSDBRSuperMode::findJumps() exiting",DIAGNOSTIC_CGR_LOG)

	return rval;
}

#define FITTED_LINE_LENGHT 8
#define MIN_NO_OF_POINTS_TO_DETECT_JUMP 4

void
CDSDBRSuperMode::modeBoundaries(
    std::vector<double>::iterator i_Pr_start,	// starting point on Pr vector
    std::vector<double>::iterator i_I_start,	// starting point on I vector
    unsigned long start_point_index,			// index of first point within a map
    unsigned long index_increment,				// increment on vectors to next point
    unsigned long line_length,					// number of points in the line
    boundary_multimap_typedef &boundary_map,	// map of boundary jumps found
	bool forward_map )							// forward map (true) or reverse map (false)
{
    unsigned int I_ramp_boundaries_max = 0;
    long I_ramp_boundaries_min_separation = 20;
    double min_d2Pr_dI2_zero_cross_jump_size = 0;
    double max_sum_of_squared_distances_from_Pr_I_line = 0;
    double min_slope_change_of_Pr_I_lines = 0;

	if( forward_map )
	{
		I_ramp_boundaries_max = CG_REG->get_DSDBR01_SMBDFWD_I_ramp_boundaries_max();
		I_ramp_boundaries_min_separation = CG_REG->get_DSDBR01_SMBDFWD_boundaries_min_separation();
		min_d2Pr_dI2_zero_cross_jump_size = CG_REG->get_DSDBR01_SMBDFWD_min_d2Pr_dI2_zero_cross_jump_size();
		max_sum_of_squared_distances_from_Pr_I_line = CG_REG->get_DSDBR01_SMBDFWD_max_sum_of_squared_distances_from_Pr_I_line();
		min_slope_change_of_Pr_I_lines = CG_REG->get_DSDBR01_SMBDFWD_min_slope_change_of_Pr_I_lines();
	}
	else // reverse map
	{
		I_ramp_boundaries_max = CG_REG->get_DSDBR01_SMBDREV_I_ramp_boundaries_max();
		I_ramp_boundaries_min_separation = CG_REG->get_DSDBR01_SMBDREV_boundaries_min_separation();
		min_d2Pr_dI2_zero_cross_jump_size = CG_REG->get_DSDBR01_SMBDREV_min_d2Pr_dI2_zero_cross_jump_size();
		max_sum_of_squared_distances_from_Pr_I_line = CG_REG->get_DSDBR01_SMBDREV_max_sum_of_squared_distances_from_Pr_I_line();
		min_slope_change_of_Pr_I_lines = CG_REG->get_DSDBR01_SMBDREV_min_slope_change_of_Pr_I_lines();
	}


    //if(start_point_index == 38500) echo_on = true;
    //else echo_on = false;
        
    //if(echo_on)
    //{
    //    std::vector<double>::iterator i_Pr_print = i_Pr_start;
    //    std::vector<double>::iterator i_I_print = i_I_start;
    //    printf("\nstart_point_index = %d",start_point_index);
    //    for( unsigned int k = 0; k < line_length; k++ )
    //    {
    //        printf("\n I[%d], Pr[%d] = [%g, %g] ",k,k,*(i_I_print),*(i_Pr_print));
    //        for( unsigned int l = 0; l < index_increment ; l++ )
    //        {
    //            i_I_print++;
    //            i_Pr_print++;
    //        }
    //    }
    //}


    // keep a count of how many jumps are detected
    unsigned long boundary_count = 0;

    // store detected jumps in a map
    boundary_multimap_typedef jumps;

	// store d2Pr_dI2 at each jump detected
	std::map<unsigned long, double> d2Pr_dI2_at_jumps;

    // keep iterators to the windowed values
    std::vector<double>::iterator i_I[MIN_NO_OF_POINTS_TO_DETECT_JUMP];
    std::vector<double>::iterator i_Pr[MIN_NO_OF_POINTS_TO_DETECT_JUMP];

    // keep slopes of lines fitted to data of MIN_NO_OF_POINTS_TO_DETECT_JUMP windows
    double slopes[FITTED_LINE_LENGHT];
    for( int i = 0; i < FITTED_LINE_LENGHT; i++ )
    {
        slopes[i] = 0;
    }
    double oldest_slope = 0;

    // keep sum_of_squared_distances from lines fitted to data of MIN_NO_OF_POINTS_TO_DETECT_JUMP windows
    double sum_of_squared_distances[FITTED_LINE_LENGHT];
    for( int i = 0; i < FITTED_LINE_LENGHT; i++ )
    {
        sum_of_squared_distances[i] = 0;
    }
    double oldest_sum_of_squared_distance = 0;

    // find the iterators pointing to the values in the first window
    unsigned long point_count = 0;
    for( int i = 0; i < MIN_NO_OF_POINTS_TO_DETECT_JUMP; i++ )
    {
        i_I[i] = i_I_start;
        i_Pr[i] = i_Pr_start;
    }
    // first point in first window is now correct, find subsequent points
    for( int i = 1; i < MIN_NO_OF_POINTS_TO_DETECT_JUMP; i++ )
    {
        for( int j = i; j < MIN_NO_OF_POINTS_TO_DETECT_JUMP; j++ )
        {
            while( point_count < index_increment*i  )
            {
                i_I[j]++;
                i_Pr[j]++;
                point_count++;
            }
        }
    }
    double oldest_I = 0;
    double oldest_Pr = 0;

    // store values in vectors
    std::vector<double> x;
    std::vector<double> y;
    for( int i = 0; i < FITTED_LINE_LENGHT - MIN_NO_OF_POINTS_TO_DETECT_JUMP; i++ )
    { // fill initial points of line with zeros
        x.push_back(0);
        y.push_back(0);
    }
    for( int i = 0; i < MIN_NO_OF_POINTS_TO_DETECT_JUMP; i++ )
    { // fill remaining points of line with initial values
        x.push_back(*(i_I[i]));
        y.push_back(*(i_Pr[i]));
    }

    int jump_found = 0;
    point_count = 0;
    
    while( point_count <= (line_length - MIN_NO_OF_POINTS_TO_DETECT_JUMP + 1)  )
    {
        // iterate through each window on a line

        if( jump_found <= 0 ) // check primary jump criterion
        { 
            // check if zero has been crossed between adjacent d2Pr_dIp2 values

            double d2Pr_dI2_A = VectorAnalysis::doubleDerivative(
                *(i_I[0]), *(i_I[1]), *(i_I[2]),
                *(i_Pr[0]), *(i_Pr[1]), *(i_Pr[2]) );
            double d2Pr_dI2_B = VectorAnalysis::doubleDerivative(
                *(i_I[1]), *(i_I[2]), *(i_I[3]),
                *(i_Pr[1]), *(i_Pr[2]), *(i_Pr[3]) );

            if( ( d2Pr_dI2_A >  min_d2Pr_dI2_zero_cross_jump_size
                && d2Pr_dI2_B < -min_d2Pr_dI2_zero_cross_jump_size )
                || ( d2Pr_dI2_B >  min_d2Pr_dI2_zero_cross_jump_size
                && d2Pr_dI2_A < -min_d2Pr_dI2_zero_cross_jump_size ) )
            {
                // boundary jump detected!!
                // add each detected boundary to boundary_multimap
                jumps.insert(
                    boundary_multimap_typedef::value_type(
                        start_point_index + (point_count - 1)*index_increment,
                        start_point_index + point_count*index_increment )
                    );


				double d2Pr_dI2_largest = d2Pr_dI2_A > d2Pr_dI2_B ? d2Pr_dI2_A : d2Pr_dI2_B;

				d2Pr_dI2_at_jumps.insert(
                    std::map<unsigned long, double>::value_type(
                        start_point_index + (point_count - 1)*index_increment,
                        d2Pr_dI2_largest )
                    );

                //if(echo_on)
                //{
				//	CString jump1_details("CDSDBRSuperMode::modeBoundaries() ");
				//  jump1_details.AppendFormat("\nI = %g,%g,%g,%g, ", *(i_I[0]), *(i_I[1]), *(i_I[2]), *(i_I[3]) );
				//  jump1_details.AppendFormat("Pr = %g,%g,%g,%g",*(i_Pr[0]),*(i_Pr[1]),*(i_Pr[2]),*(i_Pr[3]) );
				//  jump1_details.AppendFormat("\nJump1 start_point_index = %d, point_count = %d, index_increment = %d, in_map = %d>\n",
				//      start_point_index, point_count, index_increment,
				//      start_point_index + point_count*index_increment );
				//CGR_LOG(jump1_details.GetBuffer(),DIAGNOSTIC_CGR_LOG)
                //}
                jump_found = MIN_NO_OF_POINTS_TO_DETECT_JUMP;
            }
        }

        if( jump_found <= 0 && point_count > 0 ) // check for second jump criteria
        {
            // A case we're looking for is where there was not a clean jump
            // in optical power. Sometimes a power reading between two modes
            // is recorded that doesn't seem to belong to either mode,
            // so we wish to ignore such points.

            // recalculate double derivatives ignoring middle point
            //double d2Pr_dI2_A = VectorAnalysis::doubleDerivative(
            //    oldest_I, *(i_I[0]), *(i_I[1]),
            //    oldest_Pr, *(i_Pr[0]), *(i_Pr[2]) );
            //double d2Pr_dI2_B = VectorAnalysis::doubleDerivative(
            //    *(i_I[1]), *(i_I[2]), *(i_I[3]),
            //    *(i_Pr[0]), *(i_Pr[2]), *(i_Pr[3]) );
            double d2Pr_dI2_A = VectorAnalysis::doubleDerivative(
                oldest_I, *(i_I[0]), *(i_I[2]),
                oldest_Pr, *(i_Pr[0]), *(i_Pr[2]) );
            double d2Pr_dI2_B = VectorAnalysis::doubleDerivative(
                *(i_I[0]), *(i_I[2]), *(i_I[3]),
                *(i_Pr[0]), *(i_Pr[2]), *(i_Pr[3]) );

            if( ( d2Pr_dI2_A >  min_d2Pr_dI2_zero_cross_jump_size
                && d2Pr_dI2_B < -min_d2Pr_dI2_zero_cross_jump_size )
                || ( d2Pr_dI2_B >  min_d2Pr_dI2_zero_cross_jump_size
                && d2Pr_dI2_A < -min_d2Pr_dI2_zero_cross_jump_size ) )
            {
                // boundary jump detected!!
                // add each detected boundary to boundary_multimap
                jumps.insert(
                    boundary_multimap_typedef::value_type(
                        start_point_index + (point_count - 2)*index_increment,
                        start_point_index + point_count*index_increment )
                    );

				d2Pr_dI2_at_jumps.insert(
                    std::map<unsigned long, double>::value_type(
                        start_point_index + (point_count - 2)*index_increment,
                        0 )
                    );

                //if(echo_on)
                //{
				//	CString jump2_details("CDSDBRSuperMode::modeBoundaries() ");
				//	jump2_details.AppendFormat("\nI = %g,%g,%g,%g,%g, ", oldest_I, *(i_I[0]), *(i_I[1]), *(i_I[2]), *(i_I[3]) );
                //  jump2_details.AppendFormat("Pr = %g,%g,%g,%g,%g", oldest_Pr, *(i_Pr[0]),*(i_Pr[1]),*(i_Pr[2]),*(i_Pr[3]) );
                //  jump2_details.AppendFormat("\nJump2 <%d, %d>\n",
                //      start_point_index + (point_count - 2)*index_increment,
                //      start_point_index + point_count*index_increment);
				//	CGR_LOG(jump2_details.GetBuffer(),DIAGNOSTIC_CGR_LOG)
                //}
                jump_found = MIN_NO_OF_POINTS_TO_DETECT_JUMP;
            }
        }

        double slope = 0;
        double oldest_slope = 0;
        double sum_of_squared_distance = 0;
        double oldest_sum_of_squared_distance = 0;
        if(max_sum_of_squared_distances_from_Pr_I_line > 0)
        {
            // Find the slope of a line fitted to the windowed data
            double intercept = 0;
            VectorAnalysis::linearFit( x, y, slope, intercept );

            // calculate sum_of_squared_distance of points' distances from the fitted line
            std::vector<double> distances;
            VectorAnalysis::distancesFromPointsToLine( x, y, slope, intercept, distances );
            for( std::vector<double>::iterator i_d = distances.begin();
                    i_d != distances.end(); i_d++ )
            { // calculate sum of squared distances
                sum_of_squared_distance += (*i_d)*(*i_d);
            }

            // Convert slope from y/x to angle in degrees
            slope = (360/(2*M_PI))*atan(slope);

            // shift old slopes along
            oldest_slope = slopes[0];
            for( int i = 0; i < FITTED_LINE_LENGHT-1; i++ )
            {
                slopes[i] = slopes[i+1];
            }
            slopes[FITTED_LINE_LENGHT-1] = slope;

            // shift old sum_of_squared_distances along
            oldest_sum_of_squared_distance = sum_of_squared_distances[0];
            for( int i = 0; i < FITTED_LINE_LENGHT-1; i++ )
            {
                sum_of_squared_distances[i] = sum_of_squared_distances[i+1];
            }
            sum_of_squared_distances[FITTED_LINE_LENGHT-1] = sum_of_squared_distance;
        }

        if( jump_found <= MIN_NO_OF_POINTS_TO_DETECT_JUMP-2*FITTED_LINE_LENGHT
            && point_count >= 2*FITTED_LINE_LENGHT-1 )
        { // check for third jump criteria
            // In this case we wish to identify stark changes in slope.
            // Sometimes a mode boundary results in no change in optical power
            // but the direction optical power is heading can change.
            // If we have two lines fitted to data with the current point in common,
            // a low sum_of_squared_distance on both lines indicates that no jump in power occured,
            // then comparing the slope tells us if there was a rapid change in direction
            if( ( slope - oldest_slope >  min_slope_change_of_Pr_I_lines
                || slope - oldest_slope < -min_slope_change_of_Pr_I_lines )
                && sum_of_squared_distance < max_sum_of_squared_distances_from_Pr_I_line
                && oldest_sum_of_squared_distance < max_sum_of_squared_distances_from_Pr_I_line )
            {
                // boundary jump detected!!
                // add each detected boundary to boundary_multimap
                //printf("\n <%d>, slopes <%g,%g>, sumsqdist <%g,%g>",
                //    start_point_index + (point_count - FITTED_LINE_LENGHT)*index_increment,
                //    oldest_slope, slope,
                //    oldest_sum_of_squared_distance, sum_of_squared_distance );
                //printf("\n x[%d] = [%g,%g,%g,%g,%g,%g,%g,%g]", x.size(), x[0], x[1], x[2], x[3], x[4], x[5], x[6], x[7]); 
                //printf("\n y[%d] = [%g,%g,%g,%g,%g,%g,%g,%g]", y.size(), y[0], y[1], y[2], y[3], y[4], y[5], y[6], y[7]); 

                jumps.insert(
                    boundary_multimap_typedef::value_type(
                        start_point_index + (point_count - FITTED_LINE_LENGHT)*index_increment,
                        start_point_index + (point_count - FITTED_LINE_LENGHT)*index_increment )
                    );

				d2Pr_dI2_at_jumps.insert(
                    std::map<unsigned long, double>::value_type(
                        start_point_index + (point_count - 2)*index_increment,
                        0 )
                    );

                //if(echo_on)
				//	CString jump3_details("CDSDBRSuperMode::modeBoundaries() ");
				//	jump3_details.AppendFormat("\nJump3 <%d, %d>\n",
				//		start_point_index + (point_count - FITTED_LINE_LENGHT)*index_increment,
				//		start_point_index + (point_count - FITTED_LINE_LENGHT)*index_increment);
				//	CGR_LOG(jump3_details.GetBuffer(),DIAGNOSTIC_CGR_LOG)

                jump_found = FITTED_LINE_LENGHT;
            }
        }

        // ensure only one jump is found within a region
        jump_found--;

        // shift the window along by one point
        x.erase(x.begin());
        y.erase(y.begin());
        oldest_I = *(i_I[0]);
        oldest_Pr = *(i_Pr[0]);
        for( int i = 0; i < MIN_NO_OF_POINTS_TO_DETECT_JUMP-1; i++ )
        {
            i_I[i] = i_I[i+1];
            i_Pr[i] = i_Pr[i+1];
        }
        for(unsigned long j = 0; j<index_increment; j++)
        { // find next new point
            i_I[MIN_NO_OF_POINTS_TO_DETECT_JUMP-1]++;
            i_Pr[MIN_NO_OF_POINTS_TO_DETECT_JUMP-1]++;
        }
        x.push_back(*(i_I[MIN_NO_OF_POINTS_TO_DETECT_JUMP-1]));
        y.push_back(*(i_Pr[MIN_NO_OF_POINTS_TO_DETECT_JUMP-1]));

        point_count++;
    }

    if( boundary_count <= I_ramp_boundaries_max )
    {
        boundary_multimap_typedef::iterator i_jumps = jumps.begin();
        while( i_jumps != jumps.end() )
        {
	        boundary_multimap_typedef::iterator i_next = i_jumps;
			i_next++;

	        if( i_next != jumps.end() )
			{
				long separation = 0;
				if( (*i_next).first > (*i_jumps).first )
					separation = (*i_next).first - (*i_jumps).first;
				else
					separation = (*i_jumps).first - (*i_next).first;

				if( separation < I_ramp_boundaries_min_separation )
				{
					// jumps are too close, remove one
					double jump_val = (*d2Pr_dI2_at_jumps.find((*i_jumps).first)).second;
					double next_val = (*d2Pr_dI2_at_jumps.find((*i_next).first)).second;
					if( next_val > jump_val )
					{
						jumps.erase(i_jumps);
						i_jumps = jumps.begin();
					}
					else
					{
						jumps.erase(i_next);
						i_jumps = jumps.begin();
					}
				}
				else
				{
					i_jumps++;
				}
			}
			else
			{
				break;
			}
        }
		
		i_jumps = jumps.begin();
        while( i_jumps != jumps.end() )
        {
            boundary_map.insert(*i_jumps);
            i_jumps++;
        }
    }
    else
    { // too many boundaries detected => something is wrong
    }

}

//#define HYSTERESIS_PERCENTAGE_UPPER_THRESHOLD 50
//#define HYSTERESIS_PERCENTAGE_LOWER_THRESHOLD 5
//#define MIN_NO_OF_POINTS_TO_DETECT_ZPC_JUMP 20
//#define MAX_NO_OF_ZPCS_PER_LINE 3
//#define MIN_DISTANCE_FROM_ZPC_TO_REVERSE_JUMP 2
#define MIN_DISTANCE_FROM_EDGE_TO_ZPC 4
//#define MAX_NO_OF_HYSTERESIS_POINTS 40
//#define MAX_PERCENTAGE_RISE_IN_HYSTERESIS_REGION 0

//
//#define MAX_DISTANCE_BETWEEN_LINKED_JUMPS 55
////#define MIN_SHARPNESS 0.5
////#define MAX_SHARPNESS 2
//#define MIN_SHARPNESS 2
//#define MAX_SHARPNESS 6
//#define MAX_GAP_BETWEEN_DOUBLE_LINES 30
//#define MIN_POINTS_OF_DOUBLE_LINES 4
//#define NEAR_BOTTOM 10
//#define SHARPNESS_NEAR_BOTTOM 0.1
//#define MAX_DISTANCE_BETWEEN_LINKS_NEAR_BOTTOM 40
//#define MAX_DISTANCE_TO_LINK_SINGLE_UNLINKED_JUMP 30
//#define MAX_Y_DISTANCE_TO_LINK 10


#define MIN_NUMBER_OF_JUMPS_PER_MAP 200
#define MAX_NUMBER_OF_JUMPS_PER_MAP 1000
#define MIN_NUMBER_OF_LINES_PER_MAP 2
#define MAX_NUMBER_OF_LINES_PER_MAP 30
#define MIN_NUMBER_OF_JUMPS_TO_FORM_A_BOUNDARY_LINE 4

//#define MIN_MODE_GAP 7

#define MAX_DISTANCE_TO_PAD_VERTICALLY 2

CDSDBRSuperMode::rcode
CDSDBRSuperMode::findZDPRCBoundaries()
{
    CGR_LOG("CDSDBRSuperMode::findZDPRCBoundaries() entered",DIAGNOSTIC_CGR_LOG)

	rcode rval = ok;

    unsigned long ramp_length;
    unsigned long steps_count;
	if( _Im_ramp ) // middle line ramped
	{
		steps_count =(unsigned long)(_phase_current._currents.size());
		ramp_length =(unsigned long)(_filtered_middle_line._points.size());
	}
	else // phase ramped
	{
		steps_count =(unsigned long)(_filtered_middle_line._points.size());
		ramp_length =(unsigned long)(_phase_current._currents.size());
	}


    // first, calculated filtered hysteresis map
    // and find max hysteresis value

    double max_hysteresis = 0;
    int line_counter = 0;
	_map_hysteresis_power_ratio_median_filtered.clear();
    for( unsigned long y = 0; y < steps_count; y++ )
    {
        for( unsigned long x = 0; x < ramp_length; x++ )
        {
            unsigned long fp = x+y*ramp_length;
            //unsigned long rp = ((ramp_length-1)-x)+y*ramp_length;
			double hysteresis = _map_forward_power_ratio_median_filtered._map[fp]
							  - _map_reverse_power_ratio_median_filtered._map[fp];
			_map_hysteresis_power_ratio_median_filtered._map.push_back( hysteresis );
            if( hysteresis > max_hysteresis ) max_hysteresis = hysteresis;
        }
        line_counter++;
    }
	_map_hysteresis_power_ratio_median_filtered._cols = (long)ramp_length;
	_map_hysteresis_power_ratio_median_filtered._rows = (long)steps_count;


	if( CG_REG->get_DSDBR01_SMBD_write_debug_files() )
	{
		// Assume _abs_filepath is set from loading data from file
		// or from writing to file after gathering data from laser
		CString abs_filepath = _map_forward_power_ratio_median_filtered._abs_filepath;
		abs_filepath.Replace( MEAS_TYPE_ID_FORWARDPOWERRATIO_CSTRING , MEAS_TYPE_ID_HYSTERESISPOWERRATIO_CSTRING );

		// Write filtered map to file
		_map_hysteresis_power_ratio_median_filtered.writeToFile( abs_filepath, true );
	}

    // find the jump thresholds as percentages of the max value
	double jump_upper_threshold = CG_REG->get_DSDBR01_SMBD_zprc_hysteresis_upper_threshold();
    double jump_lower_threshold = CG_REG->get_DSDBR01_SMBD_zprc_hysteresis_lower_threshold();
	double max_rise_in_hysteresis = CG_REG->get_DSDBR01_SMBD_zprc_max_hysteresis_rise();

    //double jump_upper_threshold = max_hysteresis*HYSTERESIS_PERCENTAGE_UPPER_THRESHOLD/100;
    //double jump_lower_threshold = max_hysteresis*HYSTERESIS_PERCENTAGE_LOWER_THRESHOLD/100;
    //double max_rise_in_hysteresis = max_hysteresis*MAX_PERCENTAGE_RISE_IN_HYSTERESIS_REGION/100;

    boundary_multimap_typedef hysteresis_forward_jumps;
    std::vector<boundary_multimap_typedef::iterator> forward_jumps;
    std::vector<boundary_multimap_typedef::iterator> reverse_jumps;

    // iterate through each line and identify jumps
    for( unsigned long y = 0; y < steps_count; y++ )
    {
        // find existing forward jumps on the current phase line
        forward_jumps.clear();
		for( boundary_multimap_typedef::iterator i_map1_jump = _map_forward_power_ratio_median_filtered._boundary_points.begin();
             i_map1_jump != _map_forward_power_ratio_median_filtered._boundary_points.end();
			 i_map1_jump++ )
        {
            if( (unsigned long)(((*i_map1_jump).second)/ramp_length) == y )
            { // found jump on current line
                forward_jumps.push_back( i_map1_jump );
            }
        }

        // find existing reverse jumps on the current phase line
        reverse_jumps.clear();
		for( boundary_multimap_typedef::iterator i_map2_jump = _map_reverse_power_ratio_median_filtered._boundary_points.begin();
			 i_map2_jump != _map_reverse_power_ratio_median_filtered._boundary_points.end();
			 i_map2_jump++ )
        {
            if( (unsigned long)(((*i_map2_jump).first)/ramp_length) == y )
            { // found jump on current line
                reverse_jumps.push_back( i_map2_jump );
            }
        }

        // find hysteresis jumps on the current phase difference line
        hysteresis_forward_jumps.clear();
        bool in_hysteresis_region = false;
        int counter = 0;
        int hysteresis_counter = 0;
        double lowest_hysteresis = 0;
        for( unsigned long x = 1; x < ramp_length; x++ )
        {
			double hysteresis = _map_hysteresis_power_ratio_median_filtered._map[x+y*ramp_length];

            if( hysteresis > jump_upper_threshold )
            { // must be in a hysteresis region if hysteresis is this high
                in_hysteresis_region = true;
                lowest_hysteresis = hysteresis;
            }

            if( in_hysteresis_region ) hysteresis_counter++;
            else hysteresis_counter = 0;

            //if( hysteresis_counter > MAX_NO_OF_HYSTERESIS_POINTS )
			if( hysteresis_counter > CG_REG->get_DSDBR01_SMBD_zprc_max_hysteresis_points() )
            {
                in_hysteresis_region = false;
                hysteresis_counter = 0;
            }

            if( in_hysteresis_region && hysteresis < lowest_hysteresis )
                lowest_hysteresis = hysteresis;

            if( in_hysteresis_region
             && hysteresis - lowest_hysteresis > max_rise_in_hysteresis )
            { // jump up while hysteresis should be decreasing => exit region
                in_hysteresis_region = false;
                hysteresis_counter = 0;
            }

            if( hysteresis < jump_lower_threshold )
            {
                if( in_hysteresis_region )
                { // the lower threshold has been crossed exiting a hysteresis region

                    if( x >= MIN_DISTANCE_FROM_EDGE_TO_ZPC
                     && x <= ramp_length-MIN_DISTANCE_FROM_EDGE_TO_ZPC
                     && counter <= 0 )
                    {
                        // record a forward jump
                        hysteresis_forward_jumps.insert(
                            boundary_multimap_typedef::value_type(
                                x-1+y*ramp_length,
                                x+y*ramp_length )
                            );

                        // ensure next jump found is far away
                        //counter = MIN_NO_OF_POINTS_TO_DETECT_ZPC_JUMP;
						counter = CG_REG->get_DSDBR01_SMBD_zprc_min_points_separation();
                    }
                }
                in_hysteresis_region = false;
                hysteresis_counter = 0;
            }

            counter--;
        }

    CString msg = "";

        // remove hysteresis jumps within a small range of known reverse jumps
        std::vector<boundary_multimap_typedef::iterator>::iterator i_reverse_jump;
        for( i_reverse_jump = reverse_jumps.begin();
            i_reverse_jump != reverse_jumps.end(); i_reverse_jump++ )
        {
            int reverse_y = ((*(*i_reverse_jump)).first)/ramp_length;
            int reverse_x = (ramp_length-1) - ((((*(*i_reverse_jump)).first) - reverse_y*ramp_length));
            boundary_multimap_typedef::iterator i_hysteresis_jump
                = hysteresis_forward_jumps.begin();
            while( i_hysteresis_jump != hysteresis_forward_jumps.end() )
            {
                int hysteresis_y = ((*i_hysteresis_jump).second)/ramp_length;
                int hysteresis_x = ((*i_hysteresis_jump).second) - hysteresis_y*ramp_length;

    //msg.AppendFormat("\n reverse_jump = <%d,%d> , hysteresis_jump = <%d,%d>",
    //    (*(*i_reverse_jump)).first, (*(*i_reverse_jump)).second,
    //    (*i_hysteresis_jump).first, (*i_hysteresis_jump).second );

                //if( abs(reverse_x - hysteresis_x) <= MIN_DISTANCE_FROM_ZPC_TO_REVERSE_JUMP )
				if( abs(reverse_x - hysteresis_x) <= CG_REG->get_DSDBR01_SMBD_zprc_max_points_to_reverse_jump() )
                { // hysteresis and reverse jump too near
                    // => remove this hysteresis jump
                    hysteresis_forward_jumps.erase(i_hysteresis_jump);
                    i_hysteresis_jump = hysteresis_forward_jumps.begin();
                }
                else
                {
                    i_hysteresis_jump++;
                }
            }
        }

    //CGR_LOG(msg.GetBuffer(),INFO_LEVEL)

        //if( hysteresis_forward_jumps.size() - forward_jumps.size() <= MAX_NO_OF_ZPCS_PER_LINE )
		if( (int)(hysteresis_forward_jumps.size() - forward_jumps.size()) <= CG_REG->get_DSDBR01_SMBDFWD_I_ramp_boundaries_max() )
        {
            // hysteresis zero power crossings detected

            if( forward_jumps.size() == 0 )
            { // no existing forward jumps
              // add zpcs before the next line
                boundary_multimap_typedef::iterator i_map1_jump;
				for( i_map1_jump = _map_forward_power_ratio_median_filtered._boundary_points.begin();
                     i_map1_jump != _map_forward_power_ratio_median_filtered._boundary_points.end();
					 i_map1_jump++ )
                {
                    if( ((*i_map1_jump).second)/ramp_length > y )
                    { // found jump on line above current line
                      // insert hysteresis jump before it

    int msg_count = 0;
    msg = "CDSDBRSuperMode::findZDPRCBoundaries()";
    msg.Append(" adding jumps on line with no jumps");

                        for( boundary_multimap_typedef::iterator i_hysteresis_jump
                                = hysteresis_forward_jumps.begin();
                            i_hysteresis_jump != hysteresis_forward_jumps.end();
                            i_hysteresis_jump++ )
                        {
                            _map_forward_power_ratio_median_filtered._boundary_points.insert( i_map1_jump, *i_hysteresis_jump );

    msg_count++;
    msg.AppendFormat("\n inserting <%d,%d> before <%d,%d>",
        (*i_hysteresis_jump).first, (*i_hysteresis_jump).second,
        (*i_map1_jump).first, (*i_map1_jump).second );

                        }

    if(msg_count>0) CGR_LOG(msg.GetBuffer(),INFO_CGR_LOG)

                        break;
                    }
                }
                if( i_map1_jump == _map_forward_power_ratio_median_filtered._boundary_points.end() )
                { // insert hysteresis jump at the end

    int msg_count = 0;
    msg = "CDSDBRSuperMode::findZDPRCBoundaries()";
    msg.Append(" adding jumps on line with no jumps");

                    for( boundary_multimap_typedef::iterator i_hysteresis_jump
                            = hysteresis_forward_jumps.begin();
                        i_hysteresis_jump != hysteresis_forward_jumps.end();
                        i_hysteresis_jump++ )
                    {
                        _map_forward_power_ratio_median_filtered._boundary_points.insert(
							_map_forward_power_ratio_median_filtered._boundary_points.end(),
							*i_hysteresis_jump );

    msg_count++;
    msg.AppendFormat("\n inserting <%d,%d> at the end",
        (*i_hysteresis_jump).first, (*i_hysteresis_jump).second );

                    }

    if(msg_count>0) CGR_LOG(msg.GetBuffer(),INFO_CGR_LOG)

                }
            }
            else
            {   // At least one forward jump already exists on the current line
                // match existing forward jumps to hysteresis jumps
                // and insert the remaining hysteresis jump (zpcs) into
                // the map of all forward jumps

                // first, remove hysteresis jumps within a range of known forward jumps
                std::vector<boundary_multimap_typedef::iterator>::iterator i_forward_jump;
                for( i_forward_jump = forward_jumps.begin();
                    i_forward_jump != forward_jumps.end(); i_forward_jump++ )
                {
                    int forward_y = ((*(*i_forward_jump)).second)/ramp_length;
                    int forward_x = ((*(*i_forward_jump)).second) - forward_y*ramp_length;
                    boundary_multimap_typedef::iterator i_hysteresis_jump
                        = hysteresis_forward_jumps.begin();
                    while( i_hysteresis_jump != hysteresis_forward_jumps.end() )
                    {
                        int hysteresis_y = ((*i_hysteresis_jump).second)/ramp_length;
                        int hysteresis_x = ((*i_hysteresis_jump).second) - hysteresis_y*ramp_length;

                        //if( abs(forward_x - hysteresis_x) < MIN_NO_OF_POINTS_TO_DETECT_ZPC_JUMP )
						if( abs(forward_x - hysteresis_x) < CG_REG->get_DSDBR01_SMBD_zprc_min_points_separation() )
                        { // hysteresis and forward jump too near
                          // => remove this hysteresis jump
                            hysteresis_forward_jumps.erase(i_hysteresis_jump);
                            i_hysteresis_jump = hysteresis_forward_jumps.begin();
                        }
                        else
                        {
                            i_hysteresis_jump++;
                        }
                    }
                }

                // At this stage, if forward and hysteresis jumps are matched correctly
                // then there could be some hysteresis jumps left unmatched if far
                // enough away from all known forward jumps on the current line
                //if( hysteresis_forward_jumps.size()  <= MAX_NO_OF_ZPCS_PER_LINE )
				if( (int)(hysteresis_forward_jumps.size()) <= CG_REG->get_DSDBR01_SMBDFWD_I_ramp_boundaries_max() )
                { // there are hysteresis jumps unaccounted for, assumed them to be
                  // zero-power crossings and add them as new
                  // jumps in the correct place in the map of all forward jumps

    int msg_count = 0;
    msg = "CDSDBRSuperMode::findZDPRCBoundaries()";
    msg.Append(" adding jump on line with existing jumps");

                    for( boundary_multimap_typedef::iterator i_hysteresis_jump
                            = hysteresis_forward_jumps.begin();
                        i_hysteresis_jump != hysteresis_forward_jumps.end();
                        i_hysteresis_jump++ )
                    {
                        boundary_multimap_typedef::iterator i_map1_jump;
                        unsigned long previous_jump = 0;
                        for( i_map1_jump = *(forward_jumps.begin());
                            i_map1_jump != _map_forward_power_ratio_median_filtered._boundary_points.end();
							i_map1_jump++ )
                        {
                            if( (*i_map1_jump).second > (*i_hysteresis_jump).second
                            && previous_jump < (*i_hysteresis_jump).second)
                            { // found location to put new jump

    msg_count++;
    msg.AppendFormat("\n inserting <%d,%d> before <%d,%d>",
        (*i_hysteresis_jump).first, (*i_hysteresis_jump).second,
        (*i_map1_jump).first, (*i_map1_jump).second );

                                _map_forward_power_ratio_median_filtered._boundary_points.insert(
									i_map1_jump, *i_hysteresis_jump );
                                break;
                            }
                            previous_jump = (*i_map1_jump).second;
                        }
                        if( i_map1_jump == _map_forward_power_ratio_median_filtered._boundary_points.end() )
                        { // insert hysteresis jump at the end

    msg_count++;
    msg.AppendFormat("\n inserting <%d,%d> at the end",
        (*i_hysteresis_jump).first, (*i_hysteresis_jump).second );

                            _map_forward_power_ratio_median_filtered._boundary_points.insert(
								_map_forward_power_ratio_median_filtered._boundary_points.end(), *i_hysteresis_jump );
                        }

                    }

    if(msg_count>0) CGR_LOG(msg.GetBuffer(),INFO_CGR_LOG)

                }

            }

        }

    }

    CGR_LOG("CDSDBRSuperMode::findZDPRCBoundaries() exiting",DIAGNOSTIC_CGR_LOG)

	return rval;
}

CDSDBRSuperMode::rcode
CDSDBRSuperMode::insertExtrapolatedMapBorderPoints()
{
	CDSDBRSuperMode::rcode rval = ok;
	CGR_LOG("CDSDBRSuperMode::insertExtrapolatedMapBorderPoints() entered",DIAGNOSTIC_CGR_LOG)

	insertExtrapolatedMapBorderPointsInMap(
		_map_forward_power_ratio_median_filtered._boundary_points,
		_map_forward_power_ratio_median_filtered._linked_boundary_points);

	insertExtrapolatedMapBorderPointsInMap(
		_map_reverse_power_ratio_median_filtered._boundary_points,
		_map_reverse_power_ratio_median_filtered._linked_boundary_points);


	if( CG_REG->get_DSDBR01_SMBD_write_debug_files() )
	{
		if( rval == ok )
		{
			// Assume _abs_filepath is set from loading data from file
			// or from writing to file after gathering data from laser
			CString abs_filepath = _map_forward_power_ratio_median_filtered._abs_filepath;
			abs_filepath.Replace( MATRIX_ , "JUMPS_LE_" );

			// Write jumps detected in filtered map to file
			_map_forward_power_ratio_median_filtered.writeLinkedJumpsToFile( abs_filepath );

			// Assume _abs_filepath is set from loading data from file
			// or from writing to file after gathering data from laser
			abs_filepath = _map_reverse_power_ratio_median_filtered._abs_filepath;
			abs_filepath.Replace( MATRIX_ , "JUMPS_LE_" );

			// Write jumps detected in filtered map to file
			_map_reverse_power_ratio_median_filtered.writeLinkedJumpsToFile( abs_filepath );
		}
	}


    CGR_LOG("CDSDBRSuperMode::insertExtrapolatedMapBorderPoints() exiting",DIAGNOSTIC_CGR_LOG)
	return rval;
}

CDSDBRSuperMode::rcode
CDSDBRSuperMode::insertExtrapolatedMapBorderPointsInMap(
	boundary_multimap_typedef &boundary_points,
	boundary_multimap_typedef &linked_boundary_points)
{
	CDSDBRSuperMode::rcode rval = ok;
	CGR_LOG("CDSDBRSuperMode::insertExtrapolatedMapBorderPointsInMap() entered",DIAGNOSTIC_CGR_LOG)

    int top_line_num = -1;
	for( boundary_multimap_typedef::iterator i_f = linked_boundary_points.begin();
        i_f != linked_boundary_points.end() ; i_f++ )
    { // find the number of lines identified
        if( (int)((*i_f).second) > top_line_num ) top_line_num = (*i_f).second;
	}

    std::vector<double> x_position;
    std::vector<double> y_position;
	int ramp_length =(int)(_phase_current._currents.size());
	int steps_count =(int)(_filtered_middle_line._points.size());

	for( int line_number = 0 ; line_number <= top_line_num; line_number++ )
	{
		x_position.clear();
		y_position.clear();

		boundary_multimap_typedef::iterator i_end = linked_boundary_points.end();
		boundary_multimap_typedef::iterator i_first_in_line = i_end;
		boundary_multimap_typedef::iterator i_last_in_line = i_end;

		for( boundary_multimap_typedef::iterator i_f = linked_boundary_points.begin();
			i_f != i_end ; i_f++ )
		{ // find the number of lines identified
			if( (int)((*i_f).second) == line_number )
			{
				if( i_first_in_line == i_end )
				{
					i_first_in_line = i_f;
				}
				i_last_in_line = i_f;

		        int y_pos = (int)(((*i_f).first)/ramp_length);
				int x_pos = (int)((*i_f).first) - y_pos*ramp_length;

				x_position.push_back((double)x_pos);
				y_position.push_back((double)y_pos);

			}
		}
		// At this point, x_position and y_position contain the coordinates
		// for point on one line, defined by line_number

		if( x_position.size() > 3 )
		{
			double slope;
			double intercept;
			VectorAnalysis::linearFit(
				x_position,
				y_position,
				slope,
				intercept);

			//y = slope*x+intercept;
			// calculated extrapolated point to the left of a line
			double y_left_point = intercept;
			double x_left_point = 0;

			//if( y_left_point < 0 )
			//{
			//	y_left_point = 0;
			//	x_left_point = -intercept/slope;
			//}

			if( y_left_point >= 0 )
			{
				// insert into boundary points before first found in line
				//i_first_in_line;
				unsigned long indexnum = (unsigned long)y_left_point*(unsigned long)ramp_length + (unsigned long)x_left_point;

				boundary_points.insert(
					boundary_points.end(),
					boundary_multimap_typedef::value_type(indexnum, indexnum) );

				linked_boundary_points.insert(
					i_first_in_line,
					boundary_multimap_typedef::value_type(
						indexnum,
						(unsigned long)line_number ) );

			}

			// calculated extrapolated point to the right of a line
			double x_right_point = ramp_length-1;
			double y_right_point = slope*(ramp_length-1)+intercept;
			//if( y_right_point > (steps_count-1) )
			//{
			//	y_right_point = steps_count-1;
			//	x_right_point = ((steps_count-1)-intercept)/slope;
			//}

			if( y_right_point <= (steps_count-1) )
			{
				// insert into boundary points after last found in line
				unsigned long indexnum = (unsigned long)y_right_point*(unsigned long)ramp_length + (unsigned long)x_right_point;

				boundary_points.insert(
					boundary_points.end(),
					boundary_multimap_typedef::value_type(indexnum, indexnum) );

				i_last_in_line++;
				linked_boundary_points.insert(
					i_last_in_line,
					boundary_multimap_typedef::value_type(
						indexnum,
						(unsigned long)line_number ) );
			}
		}
	}

	CGR_LOG("CDSDBRSuperMode::insertExtrapolatedMapBorderPointsInMap() exiting",DIAGNOSTIC_CGR_LOG)
	return rval;
}


CDSDBRSuperMode::rcode
CDSDBRSuperMode::linkJumps()
{
	CGR_LOG("CDSDBRSuperMode::linkJumps() entered",DIAGNOSTIC_CGR_LOG)

	CDSDBRSuperMode::rcode rval = ok;

	linkJumpsByIndex(_map_forward_power_ratio_median_filtered, true);

    int mbl_count = -1;
	for( boundary_multimap_typedef::iterator i_f = _map_forward_power_ratio_median_filtered._linked_boundary_points.begin();
        i_f != _map_forward_power_ratio_median_filtered._linked_boundary_points.end() ; i_f++ )
    { // find the number of lines identified
        if( (int)((*i_f).second) > mbl_count ) mbl_count = (*i_f).second;
	}
    if( mbl_count < MIN_NUMBER_OF_LINES_PER_MAP )
    {
		rval = min_number_of_lines_not_reached;
		CGR_LOG("CDSDBRSuperMode::linkJumps() min_number_of_lines_not_reached",ERROR_CGR_LOG)
	}
    else if( mbl_count > MAX_NUMBER_OF_LINES_PER_MAP )
    {
		rval = max_number_of_lines_exceeded;
		CGR_LOG("CDSDBRSuperMode::linkJumps() max_number_of_lines_exceeded",ERROR_CGR_LOG)
	}
	else
	{
		CString log_msg("CDSDBRSuperMode::linkJumps() ");
		log_msg.AppendFormat("found %d mode boundary lines on forward map", mbl_count );
		CGR_LOG(log_msg,INFO_CGR_LOG)
	}

	if( CG_REG->get_DSDBR01_SMBD_write_debug_files() )
	{
		if( rval == ok )
		{
			// Assume _abs_filepath is set from loading data from file
			// or from writing to file after gathering data from laser
			CString abs_filepath = _map_forward_power_ratio_median_filtered._abs_filepath;
			abs_filepath.Replace( MATRIX_ , LINKED_JUMPS_ );

			// Write jumps detected in filtered map to file
			_map_forward_power_ratio_median_filtered.writeLinkedJumpsToFile( abs_filepath );
		}
	}

	if( rval == ok )
	{
		linkJumpsByIndex(_map_reverse_power_ratio_median_filtered, false);

		mbl_count = -1;
		for( boundary_multimap_typedef::iterator i_f = _map_reverse_power_ratio_median_filtered._linked_boundary_points.begin();
			i_f != _map_reverse_power_ratio_median_filtered._linked_boundary_points.end() ; i_f++ )
		{ // find the number of lines identified
			if( (int)((*i_f).second) > mbl_count ) mbl_count = (*i_f).second;
		}
		if( mbl_count < MIN_NUMBER_OF_LINES_PER_MAP )
		{
			rval = min_number_of_lines_not_reached;
			CGR_LOG("CDSDBRSuperMode::linkJumps() min_number_of_lines_not_reached",ERROR_CGR_LOG)
		}
		else if( mbl_count > MAX_NUMBER_OF_LINES_PER_MAP )
		{
			rval = max_number_of_lines_exceeded;
			CGR_LOG("CDSDBRSuperMode::linkJumps() max_number_of_lines_exceeded",ERROR_CGR_LOG)
		}
		else
		{
			CString log_msg("CDSDBRSuperMode::linkJumps() ");
			log_msg.AppendFormat("found %d mode boundary lines on reverse map", mbl_count );
			CGR_LOG(log_msg,INFO_CGR_LOG)
		}
	}

	if( CG_REG->get_DSDBR01_SMBD_write_debug_files() )
	{
		if( rval == ok )
		{
			// Assume _abs_filepath is set from loading data from file
			// or from writing to file after gathering data from laser
			CString abs_filepath = _map_reverse_power_ratio_median_filtered._abs_filepath;
			abs_filepath.Replace( MATRIX_ , LINKED_JUMPS_ );

			// Write jumps detected in filtered map to file
			_map_reverse_power_ratio_median_filtered.writeLinkedJumpsToFile( abs_filepath );
		}
	}

	CGR_LOG("CDSDBRSuperMode::linkJumps() exiting",DIAGNOSTIC_CGR_LOG)

	return rval;
}

// links boundary jumps marked <point_before,point_after> which are 
// nearest and mark them <point_before,boundary_line_number>
// using index method
void
CDSDBRSuperMode::linkJumpsByIndex(
	CLaserModeMap &map_in, // input map
	bool forward_map )							// forward map (true) or reverse map (false)
{
    //unsigned long ramp_length, // length of each ramp within the optical power map
    //unsigned long steps_count, // number of steps within the optical power map
    //boundary_multimap_typedef &boundary_jumps, // jumps detected within the optical power map
    //boundary_multimap_typedef &map_in._linked_boundary_points, // returns containing linked jumps
    //bool forward

    int linking_max_distance_between_linked_jumps;
    double linking_min_sharpness;
    double linking_max_sharpness;
    int linking_max_gap_between_double_lines;
    int linking_min_gap_between_double_lines;
    int linking_near_bottom_row;
    double linking_sharpness_near_bottom_row;
    int linking_max_distance_near_bottom_row;
    int linking_max_distance_to_single_unlinked_jump;
    int linking_max_y_distance_to_link;
    double linking_sharpness_ramp_split;

    double min_slope_change_of_Pr_I_lines = 0;

	if( forward_map )
	{
		linking_max_distance_between_linked_jumps = CG_REG->get_DSDBR01_SMBDFWD_linking_max_distance_between_linked_jumps();
		linking_min_sharpness = CG_REG->get_DSDBR01_SMBDFWD_linking_min_sharpness();
		linking_max_sharpness = CG_REG->get_DSDBR01_SMBDFWD_linking_max_sharpness();
		linking_max_gap_between_double_lines = CG_REG->get_DSDBR01_SMBDFWD_linking_max_gap_between_double_lines();
		linking_min_gap_between_double_lines = CG_REG->get_DSDBR01_SMBDFWD_linking_min_gap_between_double_lines();
		linking_near_bottom_row = CG_REG->get_DSDBR01_SMBDFWD_linking_near_bottom_row();
		linking_sharpness_near_bottom_row = CG_REG->get_DSDBR01_SMBDFWD_linking_sharpness_near_bottom_row();
		linking_max_distance_near_bottom_row = CG_REG->get_DSDBR01_SMBDFWD_linking_max_distance_near_bottom_row();
		linking_max_distance_to_single_unlinked_jump = CG_REG->get_DSDBR01_SMBDFWD_linking_max_distance_to_single_unlinked_jump();
		linking_max_y_distance_to_link = CG_REG->get_DSDBR01_SMBDFWD_linking_max_y_distance_to_link();
		linking_sharpness_ramp_split = CG_REG->get_DSDBR01_SMBDFWD_linking_sharpness_ramp_split();
	}
	else // reverse map
	{
		linking_max_distance_between_linked_jumps = CG_REG->get_DSDBR01_SMBDREV_linking_max_distance_between_linked_jumps();
		linking_min_sharpness = CG_REG->get_DSDBR01_SMBDREV_linking_min_sharpness();
		linking_max_sharpness = CG_REG->get_DSDBR01_SMBDREV_linking_max_sharpness();
		linking_max_gap_between_double_lines = CG_REG->get_DSDBR01_SMBDREV_linking_max_gap_between_double_lines();
		linking_min_gap_between_double_lines = CG_REG->get_DSDBR01_SMBDREV_linking_min_gap_between_double_lines();
		linking_near_bottom_row = CG_REG->get_DSDBR01_SMBDREV_linking_near_bottom_row();
		linking_sharpness_near_bottom_row = CG_REG->get_DSDBR01_SMBDREV_linking_sharpness_near_bottom_row();
		linking_max_distance_near_bottom_row = CG_REG->get_DSDBR01_SMBDREV_linking_max_distance_near_bottom_row();
		linking_max_distance_to_single_unlinked_jump = CG_REG->get_DSDBR01_SMBDREV_linking_max_distance_to_single_unlinked_jump();
		linking_max_y_distance_to_link = CG_REG->get_DSDBR01_SMBDREV_linking_max_y_distance_to_link();
		linking_sharpness_ramp_split = CG_REG->get_DSDBR01_SMBDREV_linking_sharpness_ramp_split();
	}



	bool forward = true;
    unsigned long ramp_length;
    unsigned long steps_count;
	if( _Im_ramp ) // middle line ramped
	{
		steps_count =(unsigned long)(_phase_current._currents.size());
		ramp_length =(unsigned long)(_filtered_middle_line._points.size());
	}
	else // phase ramped
	{
		steps_count =(unsigned long)(_filtered_middle_line._points.size());
		ramp_length =(unsigned long)(_phase_current._currents.size());
	}

	CGR_LOG("CDSDBRSuperMode::linkJumpsByIndex() entered",DIAGNOSTIC_CGR_LOG)

    // empty the map that will store linked jumps as <point_before,boundary_line_number>
	map_in._linked_boundary_points.clear();

    // define a window size before each jump to search in 
    double d_window = 20;

    // keep a count of how many lines are identified
    unsigned long boundary_line_count = 0;

    // record the first jump as in line 0
	boundary_multimap_typedef::iterator i_i = map_in._boundary_points.begin();
    map_in._linked_boundary_points.insert( boundary_multimap_typedef::value_type( (*i_i).first, 0 ) );
    boundary_line_count++;

    boundary_multimap_typedef unlinked_boundary_points;

    i_i++; // move to second jump
    // iterate through each jump and find the distance to other jumps in it's vicinity
    for( ; i_i != map_in._boundary_points.end(); i_i++ )
    { // iterate through each jump

        // find jump at start of window
        boundary_multimap_typedef::iterator i_h = i_i;
        unsigned long count = 0;
        while( i_h != map_in._boundary_points.begin() && (double)count < d_window )
        {
            count++;
            i_h--;
        }

        // find jump at end of window
        // i_i is one past the last jump in the window
        boundary_multimap_typedef::iterator i_j = i_i;

        unsigned long y_i = (unsigned long)(boundaryPoint(i_i)/ramp_length);
        unsigned long x_i = boundaryPoint(i_i) - y_i*ramp_length;
        double nearest_distance = ramp_length*ramp_length; // initial value only
        boundary_multimap_typedef::iterator nearest_jump = i_i; // initial value only

        double sharpness;
        //double max_distance_to_link = MAX_DISTANCE_BETWEEN_LINKED_JUMPS*((double)ramp_length)/100;
        double max_distance_to_link = linking_max_distance_between_linked_jumps*((double)ramp_length)/100;
        double x_position;

        if(forward)
			x_position = (double)x_i;
        else x_position = ((double)ramp_length - 1) - (double)x_i;

        if( x_position >= ((double)ramp_length-1)*linking_sharpness_ramp_split)
        {
            sharpness = linking_max_sharpness;
        }
        else
        {
            sharpness = x_position/(linking_sharpness_ramp_split*((double)(ramp_length-1)))
                *(linking_max_sharpness-linking_min_sharpness) +linking_min_sharpness;
        }

        if( (int)y_i < linking_near_bottom_row )
        {
            sharpness = linking_sharpness_near_bottom_row;
            max_distance_to_link = linking_max_distance_near_bottom_row*((double)ramp_length)/100;
        }
        
//CString msg;
//msg.AppendFormat("sharpness = %g, x_i = %d",sharpness,x_i);
//CGR_LOG(msg.GetBuffer(),INFO_LEVEL)

        for( boundary_multimap_typedef::iterator i_k = i_h; i_k != i_j; i_k++ )
        { // iterate through jumps in the window

            // calculate distance from i_i to each other jump in the window
            if( i_k != i_i )
            {
                unsigned long y_k = (unsigned long)(boundaryPoint(i_k)/ramp_length);
                unsigned long x_k = boundaryPoint(i_k) - y_k*ramp_length;
                double distance_i_to_k;

                if( (x_i > x_k && forward &&
                    ((double)x_i-(double)x_k)/sharpness > (double)y_i-(double)y_k )
                 || (x_i < x_k && !forward &&
                    ((double)x_k-(double)x_i)/sharpness > (double)y_i-(double)y_k ) )
                { // inside sharp angle, calculate distance correctly
                    distance_i_to_k = sqrt((double)((y_i-y_k)*(y_i-y_k)*(y_i-y_k) + (x_i-x_k)*(x_i-x_k)));
                } else {
                  // outside sharp angle, skew distance measurement to look bigger
                    distance_i_to_k = (y_i-y_k)*(y_i-y_k)*(y_i-y_k) + (x_i-x_k)*(x_i-x_k);
                    if(  (x_i < x_k && forward) || (x_i > x_k && !forward) )
                    { // looking down and right then make distance look even bigger
                        distance_i_to_k = 2*distance_i_to_k;
                    }
                }


                //if( y_k != y_i && abs((int)(y_k)-(int)(y_i)) <= MAX_Y_DISTANCE_TO_LINK )
                if( y_k != y_i && abs((int)(y_k)-(int)(y_i)) <= linking_max_y_distance_to_link )
                { // only consider points on different horizontal lines

////printf("\n i_i = <%d,%d>, i_k = <%d,%d>",(*i_i).first,(*i_i).second,(*i_k).first,(*i_k).second);
////printf(", dist = %g", distance_i_to_k );

//msg = "";
//msg.AppendFormat("i_i = <%d,%d>, i_k = <%d,%d>, dist = %g",(*i_i).first,(*i_i).second,(*i_k).first,(*i_k).second, distance_i_to_k);
//CGR_LOG(msg.GetBuffer(),INFO_LEVEL)


                    if( distance_i_to_k < nearest_distance &&
                      ( distance_i_to_k < max_distance_to_link
                     // || change_in_Po_i_to_k < (change_in_Po_across_jump / 10)
                      ) )
                    {
                        nearest_distance = distance_i_to_k;
                        nearest_jump = i_k;
                    }

                }
            }
        }

        if( nearest_jump != i_i )
        { // a jump has been found
            unsigned long line_number;

            // find current jump in linked jumps map
            boundary_multimap_typedef::iterator i_f1 =
                map_in._linked_boundary_points.find( (*i_i).first );

            // find nearest jump in linked jumps map
            boundary_multimap_typedef::iterator i_f2 =
                map_in._linked_boundary_points.find( (*nearest_jump).first );

            if( i_f1 == map_in._linked_boundary_points.end() && i_f2 == map_in._linked_boundary_points.end() )
            { // neither jump has been assigned a line number before now
              // assign both to a new line
                line_number = boundary_line_count;
                boundary_line_count++;
                map_in._linked_boundary_points.insert( boundary_multimap_typedef::value_type(
                    (*i_i).first, line_number ) );
//printf("\n insert <%d,%d>", (*i_i).first, line_number );
                map_in._linked_boundary_points.insert( boundary_multimap_typedef::value_type(
                    (*nearest_jump).first, line_number ) );
//msg = "";
//msg.AppendFormat("insert <%d,%d> insert <%d,%d>", (*i_i).first, line_number, (*nearest_jump).first, line_number);
//CGR_LOG(msg.GetBuffer(),INFO_LEVEL)

//printf(" insert <%d,%d>", (*nearest_jump).first, line_number );

                // check if jumps are in unlinked jumps map, is so => remove them
                boundary_multimap_typedef::iterator i_uf1 =
                    unlinked_boundary_points.find( (*i_i).first );
                if(i_uf1 != unlinked_boundary_points.end())
                    unlinked_boundary_points.erase(i_uf1);
                boundary_multimap_typedef::iterator i_uf2 =
                    unlinked_boundary_points.find( (*nearest_jump).first );
                if(i_uf2 != unlinked_boundary_points.end())
                    unlinked_boundary_points.erase(i_uf2);

            }
            else
            if( i_f1 != map_in._linked_boundary_points.end() && i_f2 == map_in._linked_boundary_points.end() )
            { // the current jump is already assigned a line number
              // assign the nearest jump to the line the current jump is on
                line_number = (*i_f1).second;
                map_in._linked_boundary_points.insert( boundary_multimap_typedef::value_type(
                    (*nearest_jump).first, line_number ) );
//printf("\n                insert <%d,%d>", (*nearest_jump).first, line_number );

//msg = "";
//msg.AppendFormat("                insert <%d,%d>", (*nearest_jump).first, line_number);
//CGR_LOG(msg.GetBuffer(),INFO_LEVEL)

                // check if jump is in unlinked jumps map, is so => remove it
                boundary_multimap_typedef::iterator i_uf2 =
                    unlinked_boundary_points.find( (*nearest_jump).first );
                if(i_uf2 != unlinked_boundary_points.end())
                    unlinked_boundary_points.erase(i_uf2);
            }
            else
            if( i_f1 == map_in._linked_boundary_points.end() && i_f2 != map_in._linked_boundary_points.end() )
            { // the nearest jump is already assigned a line number
              // assign the current jump to the line the nearest jump is on
                line_number = (*i_f2).second;
                map_in._linked_boundary_points.insert( boundary_multimap_typedef::value_type(
                    (*i_i).first, line_number ) );
//printf("\n insert <%d,%d>", (*i_i).first, line_number );

//msg = "";
//msg.AppendFormat("insert <%d,%d>", (*i_i).first, line_number);
//CGR_LOG(msg.GetBuffer(),INFO_LEVEL)

                // check if jump is in unlinked jumps map, is so => remove it
                boundary_multimap_typedef::iterator i_uf1 =
                    unlinked_boundary_points.find( (*i_i).first );
                if(i_uf1 != unlinked_boundary_points.end())
                    unlinked_boundary_points.erase(i_uf1);
            }
            else
            { // both jumps are already assigned to lines, leave them as they are
            }

        }
        else
        { // no jump was found with Po near enough to i_i's Po
            // => ignore this point, treat it as an isolated point not on a line
//printf("\n no jump was found with Po near enough to i_i's Po " );

            unlinked_boundary_points.insert( *i_i );
        }
    }

    // find two adjacent linked jumps on the same phase line
    // remove the right-most jump, add it with the unlinked jumps
    boundary_multimap_typedef::iterator i_linked_jump_a = map_in._linked_boundary_points.begin();
    boundary_multimap_typedef::iterator i_linked_jump_b = i_linked_jump_a;
    i_linked_jump_b++;
    while( i_linked_jump_b != map_in._linked_boundary_points.end())
    {
        int y_a = (int)(((*i_linked_jump_a).first)/ramp_length);
        int y_b = (int)(((*i_linked_jump_b).first)/ramp_length);
        if( y_a == y_b && (*i_linked_jump_a).second == (*i_linked_jump_b).second )
        {
            // found linked jumps on the same phase line
            int x_a = (int)((*i_linked_jump_a).first) - y_a*(int)ramp_length;
            int x_b = (int)((*i_linked_jump_b).first) - y_a*(int)ramp_length;

             // unlink right-most jump
            if( (forward && x_a>x_b) || (!forward && x_a<x_b) )
            {
//CString msg = "";
//msg.AppendFormat("unlinking <%d,%d>", (*i_linked_jump_a).first, (*i_linked_jump_a).second );
//CGR_LOG(msg.GetBuffer(),INFO_LEVEL)

                boundary_multimap_typedef::iterator i_fj
					= map_in._boundary_points.find( (*i_linked_jump_a).first );
                unlinked_boundary_points.insert( *i_fj );
                map_in._linked_boundary_points.erase(i_linked_jump_a);
            }
            else
            {
//CString msg = "";
//msg.AppendFormat("unlinking <%d,%d>", (*i_linked_jump_b).first, (*i_linked_jump_b).second );
//CGR_LOG(msg.GetBuffer(),INFO_LEVEL)

                boundary_multimap_typedef::iterator i_fj
					= map_in._boundary_points.find( (*i_linked_jump_b).first );
                unlinked_boundary_points.insert( *i_fj );
                map_in._linked_boundary_points.erase(i_linked_jump_b);
            }

            // go back to start
            i_linked_jump_a = map_in._linked_boundary_points.begin();
            i_linked_jump_b = i_linked_jump_a;
            i_linked_jump_b++;
        }
        else
        {   // go to next pair
            i_linked_jump_a++;
            i_linked_jump_b++;
        }
    }



    // find groups of linked jumps and check if two groups belong together
    bool jumps_found = true;
    boundary_multimap_typedef boundary_line_points;
    boundary_multimap_typedef alt_boundary_line_points;
    unsigned long boundary_line_number = 0;
    unsigned long alt_boundary_line_number = 1;
    while(jumps_found)
    {
        boundary_line_points.clear();
        alt_boundary_line_points.clear();
        for( boundary_multimap_typedef::iterator i_linked_jumps = map_in._linked_boundary_points.begin() ;
            i_linked_jumps != map_in._linked_boundary_points.end() ; i_linked_jumps++ )
        {
            if( (*i_linked_jumps).second == boundary_line_number )
            {
                // found a jump on the current boundary line
                boundary_line_points.insert( *i_linked_jumps );
            }
            if( (*i_linked_jumps).second == alt_boundary_line_number )
            {
                // found a jump on the alternate boundary line
                alt_boundary_line_points.insert( *i_linked_jumps );
            }
        }

        if( boundary_line_points.size() == 0 )
        { // no jumps found for this line => gone past last line
            jumps_found = false; // this will end the while loop
        }
        else
        {
            // check each jump of the alternate line to see if it belongs to the same line
            bool same_phase_line_found = false;
            bool upper_adjacent_phase_line_found = false;
            bool lower_adjacent_phase_line_found = false;
            int max_x_distance = 0;
            int min_y = steps_count;
            int max_y = 0;
            int alt_min_y = steps_count;
            int alt_max_y = 0;
            int double_points_count = 0;
            for( boundary_multimap_typedef::iterator i_al = alt_boundary_line_points.begin();
                i_al != alt_boundary_line_points.end() ; i_al++ )
            {
                int y_al = (int)(((*i_al).first)/ramp_length);
                int x_al = (int)((*i_al).first) - y_al*(int)ramp_length;

                if(y_al < alt_min_y) alt_min_y = y_al;
                if(y_al > alt_max_y) alt_max_y = y_al;

                for( boundary_multimap_typedef::iterator i_l = boundary_line_points.begin();
                    i_l != boundary_line_points.end() ; i_l++ )
                {
                    int y_l = (int)(((*i_l).first)/ramp_length);
                    int x_l = (int)((*i_l).first) - y_l*(int)ramp_length;

                    if(y_l < min_y) min_y = y_l;
                    if(y_l > max_y) max_y = y_l;

                    if( y_al == y_l )
                    { // same phase line, the two lines are not the same
                        same_phase_line_found = true;
                        //break;
                        int x_gap = abs(x_l-x_al);
                        if( x_gap > max_x_distance ) max_x_distance = x_gap;
                        double_points_count++;
                    }

                    //if( y_al+1 == y_l && abs(x_l-x_al) <= MAX_DISTANCE_BETWEEN_LINKED_JUMPS )
                    if( y_al+1 == y_l && abs(x_l-x_al) <= linking_max_distance_between_linked_jumps )
                    { // found a linked jump adjacent and above the previous line
                        upper_adjacent_phase_line_found = true;
                    }

                    //if( y_al-1 == y_l && abs(x_l-x_al) <= MAX_DISTANCE_BETWEEN_LINKED_JUMPS )
                    if( y_al-1 == y_l && abs(x_l-x_al) <= linking_max_distance_between_linked_jumps )
                    { // found a linked jump adjacent and above the previous line
                        lower_adjacent_phase_line_found = true;
                    }
                }

                //if(same_phase_line_found) break;
            }

CString msg = "";
//msg.AppendFormat("\n line_no = %d, min_y = %d, max_y = %d, size = %d",
//                 boundary_line_number, min_y, max_y, boundary_line_points.size() );
//msg.AppendFormat("\n alt_line_no = %d, alt_min_y = %d, alt_max_y = %d, alt_size = %d",
//                 alt_boundary_line_number, alt_min_y, alt_max_y, alt_boundary_line_points.size() );
//msg.AppendFormat("\n max_x_distance = %d \n", max_x_distance );

            if( !same_phase_line_found
                && upper_adjacent_phase_line_found
                && lower_adjacent_phase_line_found )
            { // found two sets of jumps that appear to be the same line
              // merge the two sets of jumps

                unsigned long line_number_to_change;
                unsigned long line_number_not_to_change;
                if(boundary_line_number < alt_boundary_line_number)
                {
                    line_number_not_to_change = boundary_line_number;
                    line_number_to_change = alt_boundary_line_number;
                }
                else
                {
                    line_number_not_to_change = alt_boundary_line_number;
                    line_number_to_change = boundary_line_number;
                }

//if(forward) msg.Append("Forward Mode Map: ");
//else msg.Append("Reverse Mode Map: ");
//msg.AppendFormat("Merging line %d into %d", line_number_to_change, line_number_not_to_change );

                boundary_multimap_typedef new_linked_boundary_points;
                for( boundary_multimap_typedef::iterator i_linked_jumps = map_in._linked_boundary_points.begin() ;
                    i_linked_jumps != map_in._linked_boundary_points.end() ; i_linked_jumps++ )
                {
                    if( (*i_linked_jumps).second <= line_number_not_to_change )
                    {
                        new_linked_boundary_points.insert( *i_linked_jumps );
                    }
                    else if( (*i_linked_jumps).second == line_number_to_change )
                    {
                        new_linked_boundary_points.insert( 
                            boundary_multimap_typedef::value_type(
                                (*i_linked_jumps).first,
                                line_number_not_to_change ) );
                    }
                    else
                    {
                        new_linked_boundary_points.insert(
                            boundary_multimap_typedef::value_type(
                                (*i_linked_jumps).first,
                                ((*i_linked_jumps).second )-1 ) );
                    }
                }
                map_in._linked_boundary_points = new_linked_boundary_points;

                if(boundary_line_number < alt_boundary_line_number)
                {
                    if(alt_boundary_line_number>0) alt_boundary_line_number--;
                }
                else
                {
                    if(boundary_line_number>0) boundary_line_number--;
                }
            }
            else
            if( (  (min_y >= alt_min_y-1 && max_y <= alt_max_y+1)
                 ||(alt_min_y >= min_y-1 && alt_max_y <= max_y+1))
                && same_phase_line_found
                && (double)max_x_distance < linking_max_gap_between_double_lines*((double)ramp_length)/100
                && (int)alt_boundary_line_points.size() >= linking_min_gap_between_double_lines
                && (int)boundary_line_points.size() >= linking_min_gap_between_double_lines )
                //&& (double)max_x_distance < MAX_GAP_BETWEEN_DOUBLE_LINES*((double)ramp_length)/100
                //&& alt_boundary_line_points.size() >= MIN_POINTS_OF_DOUBLE_LINES
                //&& boundary_line_points.size() >= MIN_POINTS_OF_DOUBLE_LINES )
            {                
                // double line found, delete group with least number of jumps

                unsigned long line_to_delete;
                boundary_multimap_typedef backup_boundary_line_points;
                if(alt_boundary_line_points.size() < boundary_line_points.size())
                {
                    line_to_delete = alt_boundary_line_number;
                }
                else
                {
                    line_to_delete = boundary_line_number;
                }

if(forward) msg.Append("Forward Mode Map: ");
else msg.Append("Reverse Mode Map: ");
msg.AppendFormat("Double lines %d and %d detected", alt_boundary_line_number, boundary_line_number );

                boundary_multimap_typedef new_linked_boundary_points;
                boundary_line_points.clear();
                for( boundary_multimap_typedef::iterator i_linked_jumps = map_in._linked_boundary_points.begin() ;
                    i_linked_jumps != map_in._linked_boundary_points.end() ; i_linked_jumps++ )
                {
                    if( (*i_linked_jumps).second < line_to_delete )
                    {
                        new_linked_boundary_points.insert( *i_linked_jumps );
                    }

                    if( (*i_linked_jumps).second > line_to_delete )
                    {
                        new_linked_boundary_points.insert(
                            boundary_multimap_typedef::value_type(
                                (*i_linked_jumps).first,
                                ((*i_linked_jumps).second )-1 ) );
                    }

                }
                map_in._linked_boundary_points = new_linked_boundary_points;

                if(alt_boundary_line_points.size() < boundary_line_points.size())
                {
                    if(alt_boundary_line_number>0) alt_boundary_line_number--;
                }
                else
                {
                    if(boundary_line_number>0) boundary_line_number--;
                }
            }

if(msg != "")CGR_LOG(msg.GetBuffer(),INFO_CGR_LOG)


        }

        if( alt_boundary_line_points.size() == 0 )
        {
            boundary_line_number++;
            alt_boundary_line_number = 0;
        }
        else if( boundary_line_number == alt_boundary_line_number+1 )
        {
            alt_boundary_line_number = alt_boundary_line_number+2;
        }
        else
        {
            alt_boundary_line_number++;
        }
    }



//    for( boundary_multimap_typedef::iterator i_unlinked_jumps = unlinked_boundary_points.begin() ;
//        i_unlinked_jumps != unlinked_boundary_points.end() ; i_unlinked_jumps++ )
//    {
//
////CString msg = "";
////msg.AppendFormat("unlinked <%d,%d>", (*i_unlinked_jumps).first, (*i_unlinked_jumps).second);
////CGR_LOG(msg.GetBuffer(),INFO_LEVEL)
//    }


    // Test each unlinked jump against each group of linked points
    // to see if a link can be established
    jumps_found = true;
    boundary_line_number = 0;
    boundary_multimap_typedef::iterator i_unlinked_jumps = unlinked_boundary_points.begin();
    while( jumps_found && unlinked_boundary_points.size() > 0 )
    {
        unsigned long unlinked_point = (*i_unlinked_jumps).first;
        int y_ul = (int)(unlinked_point/ramp_length);
        int x_ul = (int)(unlinked_point) - y_ul*(int)ramp_length;
        bool same_phase_line = false;
        bool adjacent_phase_line = false;
        int max_x_distance = 0;
        int min_y = steps_count;
        int max_y = 0;
        boundary_line_points.clear();

        for( boundary_multimap_typedef::iterator i_linked_jumps = map_in._linked_boundary_points.begin() ;
            i_linked_jumps != map_in._linked_boundary_points.end() ; i_linked_jumps++ )
        {
            if( (*i_linked_jumps).second == boundary_line_number )
            {
                // found a jump on the current boundary line
                boundary_line_points.insert( *i_linked_jumps );

                int y_l = (int)(((*i_linked_jumps).first)/ramp_length);
                int x_l = (int)((*i_linked_jumps).first) - y_l*(int)ramp_length;

                if(y_l < min_y) min_y = y_l;
                if(y_l > max_y) max_y = y_l;

                int diff_y_ul_y_l = abs(y_ul-y_l);
                if(diff_y_ul_y_l == 0) same_phase_line = true;

                if(diff_y_ul_y_l == 1)
                {
                    adjacent_phase_line = true;
                    if(diff_y_ul_y_l > max_x_distance) max_x_distance = abs(x_ul-x_l);
                }
            }
        }

        bool link_found = false;
        if( boundary_line_points.size() == 0 )
        { // no jumps found for this line => gone past last line
            jumps_found = false; // this will end the while loop
        }
        else
        { // Compare current unlinked jump to current line
            if(   !same_phase_line
                && adjacent_phase_line
                && (double)max_x_distance < linking_max_distance_to_single_unlinked_jump*((double)ramp_length)/100 )
                //&& (double)max_x_distance < MAX_DISTANCE_TO_LINK_SINGLE_UNLINKED_JUMP*((double)ramp_length)/100 )
            {
                // link jump to group
                boundary_multimap_typedef::iterator i_linked_jumps = map_in._linked_boundary_points.begin();
                while( i_linked_jumps != map_in._linked_boundary_points.end() )
                { // find correct spot in map to insert newly linked jump
                    if( (*i_linked_jumps).first > (*i_unlinked_jumps).first )
                    {
                        map_in._linked_boundary_points.insert( i_linked_jumps, 
                            boundary_multimap_typedef::value_type(
                                (*i_unlinked_jumps).first,
                                boundary_line_number ) );

//CString msg = "";
//msg.AppendFormat("insert unlinked jump <%d,%d>", (*i_unlinked_jumps).first, boundary_line_number);
//CGR_LOG(msg.GetBuffer(),INFO_LEVEL)

                        unlinked_boundary_points.erase( i_unlinked_jumps );
                        link_found = true;
                        break;
                    }
                    else
                    {
                        i_linked_jumps++;
                    }
                }
            }
        }

        if( link_found )
		{
			// are there any more unlinked points or not?
			if (unlinked_boundary_points.size() == 0 )
			{ // NO - finished
				i_unlinked_jumps = unlinked_boundary_points.end();
			}
			else
			{ // YES - go back to beginning of unlinked jumps
				i_unlinked_jumps = unlinked_boundary_points.begin();
			}
		}
        else if(i_unlinked_jumps != unlinked_boundary_points.end())
        {
            i_unlinked_jumps++;
        }

        if( i_unlinked_jumps == unlinked_boundary_points.end() )
        { // reached the end of unlinked jumps, go to next group of linked jumps
            boundary_line_number++;
            i_unlinked_jumps = unlinked_boundary_points.begin();
        }
    }

	CGR_LOG("CDSDBRSuperMode::linkJumpsByIndex() exiting",DIAGNOSTIC_CGR_LOG)
}


unsigned long
CDSDBRSuperMode::boundaryPoint(boundary_multimap_typedef::iterator iter_b)
{
    unsigned long first = (*iter_b).first;
    unsigned long second = (*iter_b).second;
    unsigned long boundary;
    if( (int)second - (int)first == 2 ) boundary = second - 1;
    else if( (int)first - (int)second == 2 ) boundary = first - 1;
    else boundary = second;
    return boundary;
}


CDSDBRSuperMode::rcode
CDSDBRSuperMode::createLMBoundaryLines()
{
	CGR_LOG("CDSDBRSuperMode::createLMBoundaryLines() entered",DIAGNOSTIC_CGR_LOG)

	rcode rval = ok;

    // Create lines from the linked jumps, filling in
    // the gaps between jumps, and padding to LHS and RHS
    // except where lines meet the top or bottom.

    // create lines for forward map
    createModeBoundaryLines(
        _map_forward_power_ratio_median_filtered,
		_lm_lower_lines );

    // create lines for reverse map
    createModeBoundaryLines(
        _map_reverse_power_ratio_median_filtered,
		_lm_upper_lines );

	//_lm_upper_lines_removed = (short)_lm_upper_lines.size();
	//_lm_lower_lines_removed = (short)_lm_lower_lines.size();

	CString log_msg1("CDSDBRSuperMode::createLMBoundaryLines() ");
	log_msg1.AppendFormat("created %d lower boundary lines", _lm_lower_lines.size() );
	CGR_LOG(log_msg1,INFO_CGR_LOG)

	CString log_msg2("CDSDBRSuperMode::createLMBoundaryLines() ");
	log_msg2.AppendFormat("created %d upper boundary lines", _lm_upper_lines.size() );
	CGR_LOG(log_msg2,INFO_CGR_LOG)

	if( CG_REG->get_DSDBR01_SMBD_write_debug_files() )
	{
		for( int i = 0 ; i < (int)(_lm_lower_lines.size()) ; i++ )
		{
			// Assume _abs_filepath is set from loading data from file
			// or from writing to file after gathering data from laser
			CString abs_filepath = _map_forward_power_ratio_median_filtered._abs_filepath;
			char buf[20];
			sprintf(buf,"lower_A_lm%d_",i);
			abs_filepath.Replace( MATRIX_ , _T(buf) );

			// write boundary line to file
			_lm_lower_lines[i].writeRowColToFile( abs_filepath, true );
		}

		for( int i = 0 ; i < (int)(_lm_upper_lines.size()) ; i++ )
		{
			// Assume _abs_filepath is set from loading data from file
			// or from writing to file after gathering data from laser
			CString abs_filepath = _map_reverse_power_ratio_median_filtered._abs_filepath;
			char buf[20];
			sprintf(buf,"upper_A_lm%d_",i);
			abs_filepath.Replace( MATRIX_ , _T(buf) );

			// write boundary line to file
			_lm_upper_lines[i].writeRowColToFile( abs_filepath, true );
		}
	}

    // check each set of lines (forward and reverse) independently
    checkForwardAndReverseLines( );

	CString log_msg3("CDSDBRSuperMode::createLMBoundaryLines() ");
	log_msg3.AppendFormat("have %d lower boundary lines after checking", _lm_lower_lines.size() );
	CGR_LOG(log_msg3,INFO_CGR_LOG)

	CString log_msg4("CDSDBRSuperMode::createLMBoundaryLines() ");
	log_msg4.AppendFormat("have %d upper boundary lines after checking", _lm_upper_lines.size() );
	CGR_LOG(log_msg4,INFO_CGR_LOG)

	if( CG_REG->get_DSDBR01_SMBD_write_debug_files() )
	{
		for( int i = 0 ; i < (int)(_lm_lower_lines.size()) ; i++ )
		{
			// Assume _abs_filepath is set from loading data from file
			// or from writing to file after gathering data from laser
			CString abs_filepath = _map_forward_power_ratio_median_filtered._abs_filepath;
			char buf[20];
			sprintf(buf,"lower_B_lm%d_",i);
			abs_filepath.Replace( MATRIX_ , _T(buf) );

			// write boundary line to file
			_lm_lower_lines[i].writeRowColToFile( abs_filepath, true );
		}

		for( int i = 0 ; i < (int)(_lm_upper_lines.size()) ; i++ )
		{
			// Assume _abs_filepath is set from loading data from file
			// or from writing to file after gathering data from laser
			CString abs_filepath = _map_reverse_power_ratio_median_filtered._abs_filepath;
			char buf[20];
			sprintf(buf,"upper_B_lm%d_",i);
			abs_filepath.Replace( MATRIX_ , _T(buf) );

			// write boundary line to file
			_lm_upper_lines[i].writeRowColToFile( abs_filepath, true );
		}
	}

    // associate forward lines and reverse lines
    associateForwardAndReverseLines( );

	CString log_msg5("CDSDBRSuperMode::createLMBoundaryLines() ");
	log_msg5.AppendFormat("have %d matched pairs of upper and lower boundary lines", _matched_line_numbers.size() );
	CGR_LOG(log_msg5,INFO_CGR_LOG)

	if( CG_REG->get_DSDBR01_SMBD_write_debug_files() )
	{
		int mcount = 0;
		for( matchedlines_typedef::iterator i_m = _matched_line_numbers.begin();
				i_m != _matched_line_numbers.end() ; i_m++ )
		{
			// Assume _abs_filepath is set from loading data from file
			// or from writing to file after gathering data from laser
			CString abs_filepath = _map_forward_power_ratio_median_filtered._abs_filepath;
			char buf[20];
			sprintf(buf,"lower_C_lm%d_m%d_",(*i_m).first,mcount);
			abs_filepath.Replace( MATRIX_ , _T(buf) );

			// write boundary line to file
			_lm_lower_lines[(*i_m).first].writeRowColToFile( abs_filepath, true );


			// Assume _abs_filepath is set from loading data from file
			// or from writing to file after gathering data from laser
			short upper_line_num = (short)((*i_m).second) - (short)(_lm_lower_lines.size());
			abs_filepath = _map_reverse_power_ratio_median_filtered._abs_filepath;
			sprintf(buf,"upper_C_lm%d_m%d_",upper_line_num,mcount);
			abs_filepath.Replace( MATRIX_ , _T(buf) );

			// write boundary line to file
			_lm_upper_lines[upper_line_num].writeRowColToFile( abs_filepath, true );

			mcount++;
		}
	}

   // remove forward or reverse lines that have not been associated
    removeUnassociatedLines( );

	if( CG_REG->get_DSDBR01_SMBD_write_debug_files() )
	{
		int mcount = 0;
		for( matchedlines_typedef::iterator i_m = _matched_line_numbers.begin();
				i_m != _matched_line_numbers.end() ; i_m++ )
		{
			// Assume _abs_filepath is set from loading data from file
			// or from writing to file after gathering data from laser
			CString abs_filepath = _map_forward_power_ratio_median_filtered._abs_filepath;
			char buf[20];
			sprintf(buf,"lower_D_lm%d_m%d_",(*i_m).first,mcount);
			abs_filepath.Replace( MATRIX_ , _T(buf) );

			// write boundary line to file
			_lm_lower_lines[(*i_m).first].writeRowColToFile( abs_filepath, true );


			// Assume _abs_filepath is set from loading data from file
			// or from writing to file after gathering data from laser
			short upper_line_num = (short)((*i_m).second) - (short)(_lm_lower_lines.size());
			abs_filepath = _map_reverse_power_ratio_median_filtered._abs_filepath;
			sprintf(buf,"upper_D_lm%d_m%d_",upper_line_num,mcount);
			abs_filepath.Replace( MATRIX_ , _T(buf) );

			// write boundary line to file
			_lm_upper_lines[upper_line_num].writeRowColToFile( abs_filepath, true );

			mcount++;
		}

		// Assume _abs_filepath is set from loading data from file
		// or from writing to file after gathering data from laser
		CString fwd_abs_filepath = _map_forward_power_ratio_median_filtered._abs_filepath;
		fwd_abs_filepath.Replace( MATRIX_ , _T("lowermatchedlines_") );

		CString rev_abs_filepath = _map_reverse_power_ratio_median_filtered._abs_filepath;
		rev_abs_filepath.Replace( MATRIX_ , _T("uppermatchedlines_") );

		writeMatchedLinesToFile( fwd_abs_filepath, rev_abs_filepath );
	}

	CGR_LOG("CDSDBRSuperMode::createLMBoundaryLines() exiting",DIAGNOSTIC_CGR_LOG)
 
	return rval;
}



void
CDSDBRSuperMode::createModeBoundaryLines(
	CLaserModeMap &map_in,
	std::vector<CDSDBRSupermodeMapLine> &lines_in )
{
	CGR_LOG("CDSDBRSuperMode::createModeBoundaryLines() entered",DIAGNOSTIC_CGR_LOG)

	// clear any existing lines
	for( int i = 0 ; i < (int)(lines_in.size()) ; i++ )
	{
		lines_in[i].clear();
	}
	lines_in.clear();

	// determine ramp/step lengths
    unsigned long map_ramp_line_length;
    unsigned long steps_count;
	if( _Im_ramp ) // middle line ramped
	{
		steps_count =(unsigned long)(_phase_current._currents.size());
		map_ramp_line_length =(unsigned long)(_filtered_middle_line._points.size());
	}
	else // phase ramped
	{
		steps_count =(unsigned long)(_filtered_middle_line._points.size());
		map_ramp_line_length =(unsigned long)(_phase_current._currents.size());
	}

    // Iterate through the map of jumps and for each marked boundary
    // copy all it's jump points into a new boundary line object

    bool forward_map = true;
    unsigned long boundary_line_number = 0;
    std::vector<unsigned long> boundary_line_points;
    std::vector<unsigned long> boundary_line_points_on_existing_line;
    std::vector<unsigned long> all_boundary_points;

    bool jumps_found = true;
    while(jumps_found)
    {
		CGR_LOG("CDSDBRSuperMode::createModeBoundaryLines() while jumps_found start",DIAGNOSTIC_CGR_LOG)

		boundary_line_points.clear();
        boundary_line_points_on_existing_line.clear();
        int ymax = -1;

		for( boundary_multimap_typedef::iterator i_linked_jumps = map_in._linked_boundary_points.begin() ;
            i_linked_jumps != map_in._linked_boundary_points.end() ; i_linked_jumps++ )
        {
            if( (*i_linked_jumps).second == boundary_line_number )
            {
				CGR_LOG("CDSDBRSuperMode::createModeBoundaryLines() found a jump on the current boundary line",DIAGNOSTIC_CGR_LOG)

                // found a jump on the current boundary line
                // find the exact boundary point and add it to the vector
				boundary_multimap_typedef::iterator i_jump = map_in._boundary_points.find( (*i_linked_jumps).first );
//if( i_jump == map_in._boundary_points.end() ) printf("\n %d not found ", (*i_linked_jumps).first );
//else printf("\n %d found <%d,%d>", (*i_linked_jumps).first, (*i_jump).first, (*i_jump).second );
                unsigned long new_point = boundaryPoint( i_jump );
//printf("\n insert %d into %d", new_point, boundary_line_number );


                int ynew = (int)(new_point/map_ramp_line_length);
                int xnew = new_point - ynew*map_ramp_line_length;
                if( ynew > ymax )
                { // jump on new line found
                    boundary_line_points.push_back( new_point );
                    ymax = ynew;
                }
                else
                { // jump on existing line found
                    boundary_line_points_on_existing_line.push_back( new_point );
                }

            }
        }

        if( boundary_line_points.size() + boundary_line_points_on_existing_line.size() == 0 )
        { // no jumps found for this line => gone past last line
//printf("\n no jumps found for this line => gone past last line %d", boundary_line_number );

			CGR_LOG("CDSDBRSuperMode::createModeBoundaryLines() no jumps found for this line => gone past last line",DIAGNOSTIC_CGR_LOG)

            jumps_found = false; // this will end the while loop
        }
        else if( (int)(boundary_line_points.size())
            + (int)(boundary_line_points_on_existing_line.size())
            < MIN_NUMBER_OF_JUMPS_TO_FORM_A_BOUNDARY_LINE )
        { // not enough jumps found => ignore these isolated points

			CGR_LOG("CDSDBRSuperMode::createModeBoundaryLines() not enough jumps found => ignore these isolated points",DIAGNOSTIC_CGR_LOG)

			//printf("\n not enough jumps found => ignore these isolated points line %d", boundary_line_number );
        }
        else
        { // more than one jump found on the current boundary line
            // => fill in missing parts and create new boundary line object
//printf("\n more than one jump found on the boundary line %d", boundary_line_number );

			CGR_LOG("CDSDBRSuperMode::createModeBoundaryLines() more than one jump found on the current boundary line",DIAGNOSTIC_CGR_LOG)
			CGR_LOG("CDSDBRSuperMode::createModeBoundaryLines() entering boundary_line_points_on_existing_line loop",DIAGNOSTIC_CGR_LOG)

            for( int i = 0; i < (int)(boundary_line_points_on_existing_line.size()) ; i++ )
            {
                unsigned long pi = boundary_line_points_on_existing_line[i];
                int yi = (int)(pi/map_ramp_line_length);
                int xi = pi - yi*map_ramp_line_length;
                if( !forward_map ) xi = (map_ramp_line_length-1)-xi;

                for( int j = 0; j < (int)(boundary_line_points.size()) ; j++ )
                {
                    unsigned long pj = boundary_line_points[j];
                    int yj = (int)(pj/map_ramp_line_length);
                    int xj = pj - yj*map_ramp_line_length;
                    if( !forward_map ) xj = (map_ramp_line_length-1)-xj;

                    if( yj == yi && xj > xi )
                    { // swap points on the same line so that left-most point is stored in main map
                        boundary_line_points[j] = pi;
                        boundary_line_points_on_existing_line[i] = pj;
                    }
                }
            }

			CGR_LOG("CDSDBRSuperMode::createModeBoundaryLines() exiting boundary_line_points_on_existing_line loop",DIAGNOSTIC_CGR_LOG)

            std::vector<unsigned long>::iterator i_point;

			CGR_LOG("CDSDBRSuperMode::createModeBoundaryLines() entering loop to fill each gap in the line choosing the nearest points to a straight line",DIAGNOSTIC_CGR_LOG)

            // fill each gap in the line choosing the nearest points to a straight line
            for( unsigned long i = 1;
                 i < (unsigned long)(boundary_line_points.size());
                 i++ )
            {
                unsigned long previous_point = boundary_line_points[i-1];
                unsigned long current_point = boundary_line_points[i];
                unsigned long previous_point_y = (unsigned long)(previous_point/map_ramp_line_length);
                unsigned long current_point_y = (unsigned long)(current_point/map_ramp_line_length);
                unsigned long previous_point_x = previous_point - previous_point_y*map_ramp_line_length;
                unsigned long current_point_x = current_point - current_point_y*map_ramp_line_length;
                unsigned long y_gap = ( current_point_y > previous_point_y ?
                    current_point_y - previous_point_y : previous_point_y - current_point_y );
                unsigned long x_gap = ( current_point_x > previous_point_x ?
                    current_point_x - previous_point_x : previous_point_x - current_point_x );
                int x_step = ( current_point_x > previous_point_x ? 1 : -1 );;
                int y_step = ( current_point_y > previous_point_y ? 1 : -1 );;

//printf("\n previous_point = boundary_line_points[%d] = %d", i-1, previous_point );
//printf("\n current_point = boundary_line_points[%d] = %d", i, current_point );
//printf("\n previous_point_x,previous_point_y = %d,%d", previous_point_x,previous_point_y );
//printf("\n current_point_x,current_point_y = %d,%d", current_point_x,current_point_y );
//printf("\n x_gap,y_gap = %d,%d", x_gap,y_gap );
//printf("\n x_step,y_step = %d,%d", x_step,y_step );

                // test for a gap
                if( x_gap > 1 || y_gap > 1 )
                { // a gap exists, find points on a line that fills the gap

					CGR_LOG("CDSDBRSuperMode::createModeBoundaryLines() a gap exists, find points on a line that fills the gap",DIAGNOSTIC_CGR_LOG)

                    if( current_point_y == previous_point_y )
                    { // add horizontal line between previous_point and current_point
//printf("\n add horizontal line " );
						CGR_LOG("CDSDBRSuperMode::createModeBoundaryLines() add horizontal line between previous_point and current_point",DIAGNOSTIC_CGR_LOG)

                        for( unsigned long new_point = previous_point_x + x_step ;
                             new_point != current_point_x ;
                             new_point = new_point + x_step )
                        {
                            // find the iterator pointing to current_point
                            i_point = std::find(
                                boundary_line_points.begin(),
                                boundary_line_points.end(),
                                current_point );
                            // insert new point before current_point
                            boundary_line_points.insert( i_point, current_point_y*map_ramp_line_length+new_point );
                            i++;
//printf("\n insert %d", current_point_y*map_ramp_line_length+new_point );
                        }
                    }
                    else if( current_point_x == previous_point_x )
                    { // add vertical line between previous_point and current_point
//printf("\n add vertical line ");

						CGR_LOG("CDSDBRSuperMode::createModeBoundaryLines() add vertical line between previous_point and current_point",DIAGNOSTIC_CGR_LOG)

                        for( unsigned long new_point = previous_point_y + y_step ;
                             new_point != current_point_y ;
                             new_point = new_point + y_step )
                        {
                            // find the iterator pointing to current_point
                            i_point = std::find(
                                boundary_line_points.begin(),
                                boundary_line_points.end(),
                                current_point );
                            // insert new point before current_point
                            boundary_line_points.insert( i_point, new_point*map_ramp_line_length+current_point_x );
                            i++;
//printf("\n insert %d", new_point*map_ramp_line_length+current_point_x );
                        }
                    }
                    else
                    { // find the line between previous_point and current_point

						CGR_LOG("CDSDBRSuperMode::createModeBoundaryLines() find the line between previous_point and current_point",DIAGNOSTIC_CGR_LOG)

                        double slope = ((double)current_point_y - (double)previous_point_y)
                            /((double)current_point_x - (double)previous_point_x);
                        double intersect = ( ((double)previous_point_y)*((double)current_point_x)
                            - ((double)current_point_y)*((double)previous_point_x) )
                            /((double)current_point_x - (double)previous_point_x);
//printf("\n add line slope = %g, intersect = %g", slope, intersect);
                        std::vector<double> distances;

                        if( x_gap >= y_gap )
                        { // search each vertical line between previous_point and current_point
                          // for the point nearest a straight line between those points
                          // and add those points to the boundary line
//printf(", search each vertical line");

						CGR_LOG("CDSDBRSuperMode::createModeBoundaryLines() search each vertical line between previous_point and current_point",DIAGNOSTIC_CGR_LOG)

                            std::vector<double> x_values;
                            std::vector<double> y_values;
                            for( unsigned long new_x_point = previous_point_x + x_step ;
                                new_x_point != current_point_x ;
                                new_x_point = new_x_point + x_step )
                            {
                                x_values.clear();
                                y_values.clear();
                                for( unsigned long new_y_point = previous_point_y ;
                                    new_y_point != current_point_y + y_step ;
                                    new_y_point = new_y_point + y_step )
                                {
                                    x_values.push_back((double)new_x_point);
                                    y_values.push_back((double)new_y_point);
                                }

                                // find the distances of each point on a vertical line to the fitted line
                                distances.clear();
                                VectorAnalysis::distancesFromPointsToLine(
                                    x_values,
                                    y_values,
                                    slope,
                                    intersect,
                                    distances );

                                // find the point nearest the fitted line
                                double shortest_distance = distances[0];
                                unsigned long nearest_y_point = (unsigned long)(y_values[0]);
//printf("\n x_values[0], y_values[0], distances[0] = %g, %g, %g",x_values[0], y_values[0], distances[0]);
                                for( unsigned long id = 1 ; id < distances.size() ; id++ )
                                {
                                    if( distances[id] < shortest_distance )
                                    {
                                        shortest_distance = distances[id];
                                        nearest_y_point = (unsigned long)(y_values[id]);
                                    }
//printf("\n x_values[%d], y_values[%d], distances[%d] = %g, %g, %g",id,id,id,x_values[id], y_values[id], distances[id]);
                                }
//printf("\n shortest_distance = %g, nearest_y_point = %d",shortest_distance,nearest_y_point);

                                // find the iterator pointing to current_point
                                i_point = std::find(
                                    boundary_line_points.begin(),
                                    boundary_line_points.end(),
                                    current_point );

                                // insert new point before current_point
                                boundary_line_points.insert( i_point, nearest_y_point*map_ramp_line_length+new_x_point );
                                i++;
//printf("\n insert %d", nearest_y_point*map_ramp_line_length+new_x_point );

                            }
                        }
                        else
                        { // search each horizontal line between previous_point and current_point
                          // for the point nearest a straight line between those points
                          // and add those points to the boundary line
//printf(", search each horizontal line");

							CGR_LOG("CDSDBRSuperMode::createModeBoundaryLines() search each horizontal line between previous_point and current_point",DIAGNOSTIC_CGR_LOG)

							std::vector<double> x_values;
                            std::vector<double> y_values;
                            for( unsigned long new_y_point = previous_point_y + y_step ;
                                new_y_point != current_point_y ;
                                new_y_point = new_y_point + y_step )
                            {
                                x_values.clear();
                                y_values.clear();
                                for( unsigned long new_x_point = previous_point_x ;
                                    new_x_point != current_point_x + x_step ;
                                    new_x_point = new_x_point + x_step )
                                {
                                    x_values.push_back((double)new_x_point);
                                    y_values.push_back((double)new_y_point);
                                }

                                // find the distances of each point on a vertical line to the fitted line
                                distances.clear();
                                VectorAnalysis::distancesFromPointsToLine(
                                    x_values,
                                    y_values,
                                    slope,
                                    intersect,
                                    distances );

                                // find the point nearest the fitted line
                                double shortest_distance = distances[0];
                                unsigned long nearest_x_point = (unsigned long)(x_values[0]);
//printf("\n x_values[0], y_values[0], distances[0] = %g, %g, %g",x_values[0], y_values[0], distances[0]);
                                for( unsigned long id = 1 ; id < distances.size() ; id++ )
                                {
                                    if( distances[id] < shortest_distance )
                                    {
                                        shortest_distance = distances[id];
                                        nearest_x_point = (unsigned long)(x_values[id]);
                                    }
//printf("\n x_values[%d], y_values[%d], distances[%d] = %g, %g, %g",id,id,id,x_values[id], y_values[id], distances[id]);
                                }
//printf("\n shortest_distance = %g, nearest_y_point = %d",shortest_distance,nearest_x_point);

                                // find the iterator pointing to current_point
                                i_point = std::find(
                                    boundary_line_points.begin(),
                                    boundary_line_points.end(),
                                    current_point );

                                // insert new point before current_point
                                boundary_line_points.insert( i_point, new_y_point*map_ramp_line_length+nearest_x_point );
                                i++;
//printf("\n insert %d", new_y_point*map_ramp_line_length+nearest_x_point );
							}
                        }
                    }
                }
            }

			CGR_LOG("CDSDBRSuperMode::createModeBoundaryLines() exiting loop to fill each gap in the line choosing the nearest points to a straight line",DIAGNOSTIC_CGR_LOG)
			CGR_LOG("CDSDBRSuperMode::createModeBoundaryLines() check if the bottom needs to be padded the left-hand-side",DIAGNOSTIC_CGR_LOG)

            std::vector<unsigned long> points_to_pad_to_lhs;
            i_point = boundary_line_points.begin();
            // check if the bottom needs to be padded the left-hand-side
            unsigned long first_ypos = (unsigned long)((*i_point)/map_ramp_line_length);
            unsigned long first_xpos = (*i_point)-first_ypos*map_ramp_line_length;
            if( first_ypos > MAX_DISTANCE_TO_PAD_VERTICALLY
             || (first_xpos < 2*MAX_DISTANCE_TO_PAD_VERTICALLY && first_ypos > 0)
             || (first_xpos > map_ramp_line_length-2*MAX_DISTANCE_TO_PAD_VERTICALLY && first_ypos > 0) )
            { // the first point is not near enough to the bottom line => pad to left-hand-side
                unsigned long first_point = *i_point;
                unsigned long lhs_point;
                if( forward_map )
                { // find left-hand-side of forward map
                    lhs_point = (((unsigned long)(first_point/map_ramp_line_length))-1)*map_ramp_line_length;
                    for( unsigned long pad_point = lhs_point;
                        pad_point < first_point-map_ramp_line_length;
                        pad_point++ )
                    {
                        points_to_pad_to_lhs.push_back( pad_point );
                    }
                }
                else
                { // find left-hand-side of reverse map
                    lhs_point = (((unsigned long)(first_point/map_ramp_line_length))*map_ramp_line_length)-1;
                    for( unsigned long pad_point = lhs_point;
                        pad_point > first_point-map_ramp_line_length;
                        pad_point-- )
                    {
                        points_to_pad_to_lhs.push_back( pad_point );
                    }
                }

                bool padding_okay = true;
                // check that points do not cross any existing line
                for( std::vector<unsigned long>::iterator i_pplhs = points_to_pad_to_lhs.begin();
                    i_pplhs != points_to_pad_to_lhs.end() ;
                    i_pplhs++ )
                {
                    std::vector<unsigned long>::iterator i_fabp =
                        std::find(all_boundary_points.begin(),all_boundary_points.end(),*i_pplhs);
                    if( i_fabp != all_boundary_points.end() )
                    { // a pad point already exists on another lines! => do not pad
                        padding_okay = false;
                        break;
                    }
                }

                if( padding_okay )
                { // add all padded points to map containing points of whole line
                    boundary_line_points.insert(
                        boundary_line_points.begin(),
                        points_to_pad_to_lhs.begin(),
                        points_to_pad_to_lhs.end() );
                }
            }
            else if(first_ypos > 0)
            { // pad from the first point vertically to the bottom
                long pad_point = (*i_point)-map_ramp_line_length;
                while(pad_point >= 0)
                {
                    boundary_line_points.insert(
                        boundary_line_points.begin(),
                        (unsigned long)pad_point);
                    pad_point = pad_point - (long)map_ramp_line_length;
                }
            }

			CGR_LOG("CDSDBRSuperMode::createModeBoundaryLines() check the last point needs to be padded to the right-hand-side",DIAGNOSTIC_CGR_LOG)

            std::vector<unsigned long> points_to_pad_to_rhs;
            i_point = (boundary_line_points.end())-1;
            // check the last point needs to be padded to the right-hand-side
            unsigned long last_ypos = (unsigned long)((*i_point)/map_ramp_line_length);
            unsigned long last_xpos = (*i_point)-last_ypos*map_ramp_line_length;
            if( last_ypos < (steps_count-1) - MAX_DISTANCE_TO_PAD_VERTICALLY
             || (last_xpos < 2*MAX_DISTANCE_TO_PAD_VERTICALLY && last_ypos < (steps_count-1))
             || (last_xpos > map_ramp_line_length-2*MAX_DISTANCE_TO_PAD_VERTICALLY && last_ypos < (steps_count-1)) )
            { // the last point is not near enough to the top line => pad to right-hand-side

				CGR_LOG("CDSDBRSuperMode::createModeBoundaryLines() the last point is not near enough to the top line => pad to right-hand-side",DIAGNOSTIC_CGR_LOG)

				unsigned long last_point = *i_point;
                unsigned long rhs_point;
                if( forward_map )
                { // find right-hand-side of forward map
                    rhs_point = ((((unsigned long)(last_point/map_ramp_line_length))+1)*map_ramp_line_length)-1;
                    for( unsigned long pad_point = last_point+1;
                        pad_point <= rhs_point;
                        pad_point++ )
                    {
                        points_to_pad_to_rhs.push_back( pad_point );
                    }
                }
                else
                { // find right-hand-side of reverse map
                    rhs_point = ((unsigned long)(last_point/map_ramp_line_length))*map_ramp_line_length;
                    for( unsigned long pad_point = last_point-1;
                        pad_point >= rhs_point;
                        pad_point-- )
                    {
                        points_to_pad_to_rhs.push_back( pad_point );
                    }
                }

                bool padding_okay = true;
                // check that points do not cross any existing line
                for( std::vector<unsigned long>::iterator i_pprhs = points_to_pad_to_rhs.begin();
                    i_pprhs != points_to_pad_to_rhs.end() ;
                    i_pprhs++ )
                {
                    std::vector<unsigned long>::iterator i_fabp =
                        std::find(all_boundary_points.begin(),all_boundary_points.end(),*i_pprhs);
                    if( i_fabp != all_boundary_points.end() )
                    { // a pad point already exists on another lines! => do not pad
                        padding_okay = false;
                        break;
                    }
                }

                if( padding_okay )
                { // add all padded points to map containing points of whole line
                    boundary_line_points.insert(
                        boundary_line_points.end(),
                        points_to_pad_to_rhs.begin(),
                        points_to_pad_to_rhs.end() );
                }
            }
            else if(last_ypos < (steps_count-1))
            { // pad from the last point vertically to the top
				CGR_LOG("CDSDBRSuperMode::createModeBoundaryLines() pad from the last point vertically to the top",DIAGNOSTIC_CGR_LOG)

                unsigned long pad_point = (*i_point)+map_ramp_line_length;
                while(pad_point < map_ramp_line_length*steps_count)
                {
                    boundary_line_points.insert(
                        boundary_line_points.end(),
                        pad_point);
                    pad_point = pad_point + map_ramp_line_length;
                }
            }


            // Check the line's first point and last point are on edge of mode map
            unsigned long first_point = *(boundary_line_points.begin());
            unsigned long last_point = *((boundary_line_points.end())-1);
            unsigned long y_first_point = (unsigned long)(first_point/map_ramp_line_length);
            unsigned long x_first_point = first_point - y_first_point*map_ramp_line_length;
            unsigned long y_last_point = (unsigned long)(last_point/map_ramp_line_length);
            unsigned long x_last_point = last_point - y_last_point*map_ramp_line_length;
            if( !forward_map ) // reverse map
            {
                x_first_point = (map_ramp_line_length-1)-x_first_point;
                x_last_point = (map_ramp_line_length-1)-x_last_point;
            }

            if( ( y_first_point == 0   // first point is on bottom line
               || x_first_point == 0 ) // OR first point is on LHS
             && ( y_last_point == steps_count-1 // AND last point is on top line
               || x_last_point == (map_ramp_line_length-1))) // OR last point is on RHS
            {
                // the line detected is complete (starts and ends on an edge of the mode map)

				CGR_LOG("CDSDBRSuperMode::createModeBoundaryLines() the line detected is complete (starts and ends on an edge of the mode map)",DIAGNOSTIC_CGR_LOG)

                // add points to vector of points of all lines found so far
                for( std::vector<unsigned long>::iterator i_blp = boundary_line_points.begin();
                    i_blp != boundary_line_points.end() ;
                    i_blp++ )
                {
                    all_boundary_points.push_back( *i_blp );
                }

                // At this point we have a std::vector<unsigned long> containing the indices of a line
                // Now we want to convert those into real x-y values,
                // store them in a CDSDBRSupermodeMapLine object, and then add it to the results.

				CDSDBRSupermodeMapLine new_boundary_line;

                for( std::vector<unsigned long>::iterator i_blp = boundary_line_points.begin();
                    i_blp != boundary_line_points.end() ;
                    i_blp++ )
                {
					long row = (long)((*i_blp)/map_ramp_line_length);
					long col = (long)((*i_blp) - ((unsigned long)row)*map_ramp_line_length);

					new_boundary_line._points.push_back( convertRowCol2LinePoint( row, col ) );
				}


				if( lines_in.empty() )
					lines_in.push_back(new_boundary_line);
				else
				{
					long new_line_pos_on_bottom_lhs = y_first_point + ((map_ramp_line_length-1)-x_first_point);

					// Need to find where to put the line in amongst neighbouring lines
					bool foundIt = false;
					for( std::vector<CDSDBRSupermodeMapLine>::iterator i_line = lines_in.begin();
						i_line != lines_in.end() ;
						i_line++ )
					{
						long pos_on_bottom_lhs = (*i_line)._points[0].row +
							(((long)map_ramp_line_length-1)-(*i_line)._points[0].col);

						if( pos_on_bottom_lhs > new_line_pos_on_bottom_lhs )
						{
							// the line is ahead of the one we wish to insert
							foundIt = true;
							lines_in.insert( i_line, new_boundary_line );
							break;
						}
					}
					if( !foundIt )
						lines_in.push_back(new_boundary_line);
				}

            }
            else
            { // increment counter of lines removed
                //results._number_of_lines_removed++;
            }
        }

        boundary_line_number++;
    }

    CGR_LOG("CDSDBRSuperMode::createModeBoundaryLines() exiting",DIAGNOSTIC_CGR_LOG)
}


void
CDSDBRSuperMode::
checkForwardAndReverseLines()
{
	CGR_LOG("CDSDBRSuperMode::checkForwardAndReverseLines() entered",DIAGNOSTIC_CGR_LOG)

    unsigned long ramp_length;
    unsigned long steps_count;

	if( _Im_ramp ) // middle line ramped
	{
		steps_count =(unsigned long)(_phase_current._currents.size());
		ramp_length =(unsigned long)(_filtered_middle_line._points.size());
	}
	else // phase ramped
	{
		steps_count =(unsigned long)(_filtered_middle_line._points.size());
		ramp_length =(unsigned long)(_phase_current._currents.size());
	}

    int not_a_value = -100;

	int prev_forward_line = not_a_value;
    int prev_forward_bottom_rhs_first_pos = not_a_value;
    int prev_forward_lhs_top_last_pos = not_a_value;

    int prev_reverse_line = not_a_value;
    int prev_reverse_bottom_rhs_first_pos = not_a_value;
    int prev_reverse_lhs_top_last_pos = not_a_value;

    unsigned long cl = 0 ;
	while( cl < (unsigned long)(_lm_lower_lines.size() + _lm_upper_lines.size()) )
    { // iterate through lines

        //ModeBoundaryLine* mbl = results.getLine( cl );

		std::vector<CDSDBRSupermodeMapLine>::iterator i_lm_boundary_line;
		bool forward_lower = true;
		unsigned long line_num = 0;

		if( cl < (unsigned long)(_lm_lower_lines.size()) )
		{
			i_lm_boundary_line = _lm_lower_lines.begin();
			line_num = cl;
			i_lm_boundary_line += line_num;
			forward_lower = true;
		}
		else
		{
			i_lm_boundary_line = _lm_upper_lines.begin();
			line_num = (cl - (unsigned long)(_lm_lower_lines.size()));
			i_lm_boundary_line += line_num;
			forward_lower = false;
		}

        if( forward_lower )
        {
            // find y and x values of first point in line

            //std::vector<unsigned long>::iterator i_first_point = mbl->getX()->getIndices()->begin();
            //int first_y = (*i_first_point)/map1_ramp_line_length;
            //int first_x = (*i_first_point) - first_y*map1_ramp_line_length;
			int first_y = i_lm_boundary_line->_points[0].row;
			int first_x = i_lm_boundary_line->_points[0].col;

            int bottom_rhs_first_pos;
            if( first_y == 0 ) bottom_rhs_first_pos = (ramp_length-1)-first_x;
            else bottom_rhs_first_pos = first_y+ramp_length;

            // find y and x values of last point in line

            //std::vector<unsigned long>::iterator i_last_point = mbl->getX()->getIndices()->end();
            //i_last_point--;
            //int last_y = (*i_last_point)/ramp_length;
            //int last_x = (*i_last_point) - last_y*ramp_length;
			int index_last = (int)(i_lm_boundary_line->_points.size()) - 1;
            int last_y = i_lm_boundary_line->_points[index_last].row;
			int last_x = i_lm_boundary_line->_points[index_last].col;
            int lhs_top_last_pos;
            if( last_x == ramp_length-1) lhs_top_last_pos = last_y;
            else lhs_top_last_pos = last_y + (ramp_length-1) - last_x;


			if( prev_forward_line != not_a_value && line_num > 0 && _lm_lower_lines.size() > 1 &&
              ( bottom_rhs_first_pos == prev_forward_bottom_rhs_first_pos
             || lhs_top_last_pos == prev_forward_lhs_top_last_pos
             || (bottom_rhs_first_pos-prev_forward_bottom_rhs_first_pos)
             *(lhs_top_last_pos-prev_forward_lhs_top_last_pos) < 0
			 || CG_REG->get_DSDBR01_SMBD_min_points_width_of_lm() > abs(bottom_rhs_first_pos-prev_forward_bottom_rhs_first_pos)
             || CG_REG->get_DSDBR01_SMBD_min_points_width_of_lm() > abs(lhs_top_last_pos-prev_forward_lhs_top_last_pos) ) )
            { 
                // lines cross each other => remove both
    CGR_LOG("CDSDBRSuperMode::checkForwardAndReverseLines() removing two forward lines that cross",INFO_CGR_LOG)
                //results.deleteLine(cl);
                //results._number_of_lines_removed++;
                //results.deleteLine(prev_forward_line);
                //results._number_of_lines_removed++;
				std::vector<CDSDBRSupermodeMapLine>::iterator i_prev_lm_boundary_line = i_lm_boundary_line;
				i_prev_lm_boundary_line--;
				_lm_lower_lines.erase(i_lm_boundary_line);
				_lm_lower_lines.erase(i_prev_lm_boundary_line);
                prev_forward_line = not_a_value;
				prev_forward_bottom_rhs_first_pos = not_a_value;
				prev_forward_lhs_top_last_pos = not_a_value;
                cl=0;
            }
            else
            {
                prev_forward_line = line_num;
                prev_forward_bottom_rhs_first_pos = bottom_rhs_first_pos;
                prev_forward_lhs_top_last_pos = lhs_top_last_pos;
                cl++;
            }

        }
        else // reverse_upper
        {

            //// find y and x values of first point in line
            //std::vector<unsigned long>::iterator i_first_point = mbl->getX()->getIndices()->begin();
            //int first_y = (*i_first_point)/ramp_length;
            //int first_x = (*i_first_point) - first_y*ramp_length;
			int first_y = i_lm_boundary_line->_points[0].row;
			int first_x = i_lm_boundary_line->_points[0].col;
            //first_x = (ramp_length-1)-first_x;

            int bottom_rhs_first_pos;
            if( first_y == 0 ) bottom_rhs_first_pos = (ramp_length-1)-first_x;
            else bottom_rhs_first_pos = first_y+ramp_length;


            //std::vector<unsigned long>::iterator i_last_point = mbl->getX()->getIndices()->end();
            //i_last_point--;
            //// find y and x values of last point in line
            //int last_y = (*i_last_point)/ramp_length;
            //int last_x = (*i_last_point) - last_y*ramp_length;
			int index_last = (int)(i_lm_boundary_line->_points.size()) - 1;
            int last_y = i_lm_boundary_line->_points[index_last].row;
			int last_x = i_lm_boundary_line->_points[index_last].col;
            //last_x = (ramp_length-1)-last_x;

            int lhs_top_last_pos;
            if( last_x == ramp_length-1 ) lhs_top_last_pos = last_y;
            else lhs_top_last_pos = last_y + (ramp_length-1)-last_x;


            if( prev_reverse_line != not_a_value && line_num > 0 && _lm_upper_lines.size() > 1 &&
              ( bottom_rhs_first_pos == prev_reverse_bottom_rhs_first_pos
             || lhs_top_last_pos == prev_reverse_lhs_top_last_pos
             || (bottom_rhs_first_pos-prev_reverse_bottom_rhs_first_pos)
             *(lhs_top_last_pos-prev_reverse_lhs_top_last_pos) < 0
             || CG_REG->get_DSDBR01_SMBD_min_points_width_of_lm() > abs(bottom_rhs_first_pos-prev_reverse_bottom_rhs_first_pos)
             || CG_REG->get_DSDBR01_SMBD_min_points_width_of_lm() > abs(lhs_top_last_pos-prev_reverse_lhs_top_last_pos) ) )
            { 
                // lines cross each other => remove both
    CGR_LOG("CDSDBRSuperMode::checkForwardAndReverseLines() removing two reverse lines that cross",INFO_CGR_LOG)
                //results.deleteLine(cl);
                //results._number_of_lines_removed++;
                //results.deleteLine(prev_reverse_line);
                //results._number_of_lines_removed++;
				std::vector<CDSDBRSupermodeMapLine>::iterator i_prev_lm_boundary_line = i_lm_boundary_line;
				i_prev_lm_boundary_line--;
				_lm_upper_lines.erase(i_lm_boundary_line);
				_lm_upper_lines.erase(i_prev_lm_boundary_line);
                prev_reverse_line = not_a_value;
				prev_reverse_bottom_rhs_first_pos = not_a_value;
				prev_reverse_lhs_top_last_pos = not_a_value;
                cl=0;
            }
            else
            {
                prev_reverse_line = line_num;
                prev_reverse_bottom_rhs_first_pos = bottom_rhs_first_pos;
                prev_reverse_lhs_top_last_pos = lhs_top_last_pos;
                cl++;
            }
        }

    }

    CGR_LOG("CDSDBRSuperMode::checkForwardAndReverseLines() exiting",DIAGNOSTIC_CGR_LOG)
}


void
CDSDBRSuperMode::
associateForwardAndReverseLines()
{
    CGR_LOG("CDSDBRSuperMode::associateForwardAndReverseLines() entered",DIAGNOSTIC_CGR_LOG)


    unsigned long ramp_length;
    unsigned long steps_count;

	if( _Im_ramp ) // middle line ramped
	{
		steps_count =(unsigned long)(_phase_current._currents.size());
		ramp_length =(unsigned long)(_filtered_middle_line._points.size());
	}
	else // phase ramped
	{
		steps_count =(unsigned long)(_filtered_middle_line._points.size());
		ramp_length =(unsigned long)(_phase_current._currents.size());
	}

    // match up the lines on both forward and reverse maps

    // store first and last point in each line in these maps
    boundary_multimap_typedef forward_firstpoints;
    boundary_multimap_typedef forward_lastpoints;
    boundary_multimap_typedef reverse_firstpoints;
    boundary_multimap_typedef reverse_lastpoints;

    for(unsigned long cl = 0 ; cl < (unsigned long)(_lm_lower_lines.size() + _lm_upper_lines.size()) ; cl++ )
    { // iterate through lines

        //ModeBoundaryLine* mbl = results.getLine( cl );

		std::vector<CDSDBRSupermodeMapLine>::iterator i_lm_boundary_line;
		bool forward_lower = true;
		unsigned long line_num = 0;

		if( cl < (unsigned long)(_lm_lower_lines.size()) )
		{
			i_lm_boundary_line = _lm_lower_lines.begin();
			line_num = cl;
			i_lm_boundary_line += line_num;
			forward_lower = true;
		}
		else
		{
			i_lm_boundary_line = _lm_upper_lines.begin();
			line_num = (cl - (unsigned long)(_lm_lower_lines.size()));
			i_lm_boundary_line += line_num;
			forward_lower = false;
		}

        if( forward_lower )
        {
			long row_first_pt = i_lm_boundary_line->_points.begin()->row;
			long col_first_pt = i_lm_boundary_line->_points.begin()->col;
			unsigned long index_first_pt = col_first_pt + row_first_pt*ramp_length;

            forward_firstpoints.insert(
                boundary_multimap_typedef::value_type( index_first_pt , cl )
                );
            //printf("\n forward_firstpoints %d",*(mbl->getX()->getIndices()->begin()));

            //std::vector<unsigned long>::iterator i_last_point = mbl->getX()->getIndices()->end();
            //i_last_point--;

			std::vector<CDSDBRSupermodeMapLine::point_type>::iterator i_last_pt = i_lm_boundary_line->_points.end();
			i_last_pt--;
			long row_last_pt = (*i_last_pt).row;
			long col_last_pt = (*i_last_pt).col;
			unsigned long index_last_pt = col_last_pt + row_last_pt*ramp_length;

            forward_lastpoints.insert(
                boundary_multimap_typedef::value_type( index_last_pt , cl )
                );
            //printf("\n forward_lastpoints %d",*i_last_point);
		}
		else // reverse_upper
		{
			long row_first_pt = i_lm_boundary_line->_points.begin()->row;
			long col_first_pt = i_lm_boundary_line->_points.begin()->col;
			unsigned long index_first_pt = col_first_pt + row_first_pt*ramp_length;

            reverse_firstpoints.insert(
                boundary_multimap_typedef::value_type( index_first_pt , cl )
                );

			std::vector<CDSDBRSupermodeMapLine::point_type>::iterator i_last_pt = i_lm_boundary_line->_points.end();
			i_last_pt--;
			long row_last_pt = (*i_last_pt).row;
			long col_last_pt = (*i_last_pt).col;
			unsigned long index_last_pt = col_last_pt + row_last_pt*ramp_length;

            //printf("\n reverse_firstpoints %d",*(mbl->getX()->getIndices()->begin()));
            //std::vector<unsigned long>::iterator i_last_point = mbl->getX()->getIndices()->end();
            //i_last_point--;
            reverse_lastpoints.insert(
                boundary_multimap_typedef::value_type( index_last_pt , cl )
                );
            //printf("\n reverse_lastpoints %d",*i_last_point);
		}

    }

    // store rhs and lhs points in the mode below each line in these maps
    boundary_multimap_typedef forward_rhs_points;
    boundary_multimap_typedef forward_lhs_points;
    boundary_multimap_typedef reverse_rhs_points;
    boundary_multimap_typedef reverse_lhs_points;

    // find points between forward lines' last points along RHS
    unsigned long prev_y = 0;             // start at bottom-right corner
    unsigned long prev_x = ramp_length-1; // start at bottom-right corner

    for( boundary_multimap_typedef::iterator i_flp = forward_lastpoints.begin() ;
         i_flp != forward_lastpoints.end() ; i_flp++ )
    {
        unsigned long last_point = (*i_flp).first;
        unsigned long line_number = (*i_flp).second;
        //printf("\n <%d,%d>",last_point,line_number);

        unsigned long y = (unsigned long)(last_point/ramp_length);
        unsigned long x = last_point-y*ramp_length;

        if( x == ramp_length-1 )
        { // last point is on RHS

            unsigned long prev_last_point = ramp_length-1; // start at bottom-right corner
            for( int i = last_point-ramp_length; i > 0; i-- )
            { // find last point of next line down
                boundary_multimap_typedef::iterator fi = forward_lastpoints.find(i);
                if( fi != forward_lastpoints.end() )
                { // found last point of next line down
                    //printf("\n found prev_last_point = %d", (*fi).first);
                    prev_last_point = (*fi).first;
                    break;
                }
            }
            unsigned long prev_y = (unsigned long)(prev_last_point/ramp_length);

            //printf("\nforward_rhs %d ",line_number);

            for(unsigned long mode_point_y = prev_y+1; mode_point_y <= y ; mode_point_y++ )
            { // iterate through points from previous line to current line on RHS
                unsigned long mode_point =
                    (ramp_length-1) + mode_point_y*ramp_length;
                forward_rhs_points.insert(
                    boundary_multimap_typedef::value_type( mode_point , line_number ) );
                //printf(" <%d,%d>",mode_point,line_number);
            }
        }
    }

    // find points between forward lines' first points along LHS
    prev_y = 0; // start at bottom-left corner
    prev_x = 0; // start at bottom-left corner

    for( boundary_multimap_typedef::iterator i_ffp = forward_firstpoints.begin() ;
         i_ffp != forward_firstpoints.end() ; i_ffp++ )
    {
        unsigned long first_point = (*i_ffp).first;
        unsigned long line_number = (*i_ffp).second;
        //printf("\n <%d,%d>",first_point,line_number);

        unsigned long y = (unsigned long)(first_point/ramp_length);
        unsigned long x = first_point-y*ramp_length;

        if( x == 0 )
        { // first point is on LHS

            unsigned long prev_first_point = 0; // start at bottom-left corner
            for( int i = first_point-ramp_length; i > 0; i-- )
            { // find first point of next line down
                boundary_multimap_typedef::iterator fi = forward_firstpoints.find(i);
                if( fi != forward_firstpoints.end() )
                { // found first point of next line down
                    //printf("\n found prev_first_point = %d", (*fi).first);
                    prev_first_point = (*fi).first;
                    break;
                }
            }
            unsigned long prev_y = (unsigned long)(prev_first_point/ramp_length);

            //printf("\nforward_lhs %d ",line_number);

            for(unsigned long mode_point_y = prev_y+1; mode_point_y <= y ; mode_point_y++ )
            { // iterate through points from previous line to current line on LHS
                unsigned long mode_point = mode_point_y*ramp_length;
                forward_lhs_points.insert(
                    boundary_multimap_typedef::value_type( mode_point , line_number ) );
                //printf(" <%d,%d>",mode_point,line_number);
            }
        }
    }

    // find points between reverse lines' last points along RHS
    prev_y = 0;             // start at bottom-right corner
    prev_x = ramp_length-1; // start at bottom-right corner

    for( boundary_multimap_typedef::iterator i_rlp = reverse_lastpoints.begin() ;
         i_rlp != reverse_lastpoints.end() ; i_rlp++ )
    {
        unsigned long last_point = (*i_rlp).first;
        unsigned long line_number = (*i_rlp).second;
        //printf("\n <%d,%d>",last_point,line_number);

        unsigned long y = (unsigned long)(last_point/ramp_length);
        unsigned long x = last_point-y*ramp_length;

        if( x == ramp_length-1 ) //0 )
        { // last point is on RHS

            unsigned long prev_last_point = 0; // start at bottom-right corner
            for( int i = last_point-ramp_length; i > 0; i-- )
            { // find last point of next line down
                boundary_multimap_typedef::iterator fi = reverse_lastpoints.find(i);
                if( fi != reverse_lastpoints.end() )
                { // found last point of next line down
                    //printf("\n found prev_last_point = %d", (*fi).first);
                    prev_last_point = (*fi).first;
                    break;
                }
            }
            unsigned long prev_y = (unsigned long)(prev_last_point/ramp_length);

            //printf("\nreverse_rhs %d ",line_number);

            for(unsigned long mode_point_y = prev_y+1; mode_point_y <= y ; mode_point_y++ )
            { // iterate through points from previous line to current line on RHS
                // also, convert from reverse indexing to forward indexing
                unsigned long mode_point =
                    (ramp_length-1) + mode_point_y*ramp_length;
                reverse_rhs_points.insert(
                    boundary_multimap_typedef::value_type( mode_point , line_number ) );
                //printf(" <%d,%d>",mode_point,line_number);
            }
        }
    }


    // find points between reverse lines' first points along LHS
    prev_y = 0; // start at bottom-left corner
    prev_x = 0; // start at bottom-left corner

    for( boundary_multimap_typedef::iterator i_rfp = reverse_firstpoints.begin() ;
         i_rfp != reverse_firstpoints.end() ; i_rfp++ )
    {
        unsigned long first_point = (*i_rfp).first;
        unsigned long line_number = (*i_rfp).second;
        //printf("\n <%d,%d>",first_point,line_number);

        unsigned long y = (unsigned long)(first_point/ramp_length);
        unsigned long x = first_point-y*ramp_length;

        if( x == 0 ) //ramp_length-1 )
        { // first point is on LHS

            unsigned long prev_first_point = ramp_length-1; // start at bottom-left corner
            for( int i = first_point-ramp_length; i > 0; i-- )
            { // find first point of next line down
                boundary_multimap_typedef::iterator fi = reverse_firstpoints.find(i);
                if( fi != reverse_firstpoints.end() )
                { // found first point of next line down
                    //printf("\n found prev_first_point = %d", (*fi).first);
                    prev_first_point = (*fi).first;
                    break;
                }
            }
            unsigned long prev_y = (unsigned long)(prev_first_point/ramp_length);

            //printf("\nreverse_lhs %d ",line_number);

            for(unsigned long mode_point_y = prev_y+1; mode_point_y <= y ; mode_point_y++ )
            { // iterate through points from previous line to current line on LHS
                // also, convert from reverse indexing to forward indexing
                unsigned long mode_point = mode_point_y*ramp_length;
                reverse_lhs_points.insert(
                    boundary_multimap_typedef::value_type( mode_point , line_number ) );
                //printf(" <%d,%d>",mode_point,line_number);
            }
        }
    }

    // iterate through the forward mode lines
    // for each line, find the points on the forward LHS and RHS
    // then for each of these points, find their line numbers
    // on the reverse LHS and RHS
    // find the lines with the most points in common
    // if it's more than a percentage calculated below, consider the modes matched

    //for(unsigned long cl = 0 ; cl < results.countLines() ; cl++ )
    //{ // iterate through forward lines only
    //    if( results.getLine( cl )->getLineType() & MBL_FORWARD )
    //    {

    for(unsigned long cl = 0 ; cl < (unsigned long)(_lm_lower_lines.size() + _lm_upper_lines.size()) ; cl++ )
    { // iterate through lines

        //ModeBoundaryLine* mbl = results.getLine( cl );

		std::vector<CDSDBRSupermodeMapLine>::iterator i_lm_boundary_line;
		bool forward_lower = true;
		unsigned long line_num = 0;

		if( cl < (unsigned long)(_lm_lower_lines.size()) )
		{
			i_lm_boundary_line = _lm_lower_lines.begin();
			line_num = cl;
			i_lm_boundary_line += line_num;
			forward_lower = true;
		}
		else
		{
			i_lm_boundary_line = _lm_upper_lines.begin();
			line_num = (cl - (unsigned long)(_lm_lower_lines.size()));
			i_lm_boundary_line += line_num;
			forward_lower = false;
		}

        if( forward_lower )
        {

            // build up a vector of containing the reverse line numbers for the same mode points
            std::vector<unsigned long> reverse_line_numbers;

            // find reverse line numbers for the same mode points on RHS
            //printf("\n find reverse line numbers for the same mode points on RHS");
            for( boundary_multimap_typedef::iterator i_frhs = forward_rhs_points.begin();
                 i_frhs != forward_rhs_points.end() ; i_frhs++ )
            {
                if( (*i_frhs).second == cl )
                { // found point associated with this line
                  // find the same point's line number on the reverse map
                    boundary_multimap_typedef::iterator i_rrhs
                        = reverse_rhs_points.find( (*i_frhs).first );
                    if( i_rrhs != reverse_rhs_points.end() )
                    { // found the same point in reverse RHS
                        reverse_line_numbers.push_back( (*i_rrhs).second );
                        //printf(" %d", (*i_rrhs).second );
                    }
                }
            }

            // find reverse line numbers for the same mode points on LHS
            //printf("\n find reverse line numbers for the same mode points on LHS");
            for( boundary_multimap_typedef::iterator i_flhs = forward_lhs_points.begin();
                 i_flhs != forward_lhs_points.end() ; i_flhs++ )
            {
                if( (*i_flhs).second == cl )
                { // found point associated with this line
                  // find the same point's line number on the reverse map
                    boundary_multimap_typedef::iterator i_rlhs
                        = reverse_lhs_points.find( (*i_flhs).first );
                    if( i_rlhs != reverse_lhs_points.end() )
                    { // found the same point in reverse LHS
                        reverse_line_numbers.push_back( (*i_rlhs).second );
                        //printf(" %d", (*i_rlhs).second );
                    }
                }
            }

            // find the most commonly occuring reverse line number 
            unsigned long most_common_number = 0;
            unsigned long most_common_number_count = 0;
            for(unsigned long rln = 0 ; rln < (unsigned long)(_lm_lower_lines.size() + _lm_upper_lines.size()) ; rln++ )
            {
                // count the occurances of rln
                unsigned long rln_count = 0;
                for( std::vector<unsigned long>::iterator i_rln = reverse_line_numbers.begin();
                    i_rln != reverse_line_numbers.end(); i_rln++ )
                    if( *(i_rln) == rln ) rln_count++;

                if( rln_count > most_common_number_count )
                { // rln is the most common number found so far
                    most_common_number = rln;
                    most_common_number_count = rln_count;
                }
            }

            if( reverse_line_numbers.size() > 1 &&
                100*most_common_number_count/((unsigned long)(reverse_line_numbers.size())) >= 50 )
            { // 50% of points matched => treat the lines as matched

                // check this has been matched already, if so, ignore it
				matchedlines_typedef::iterator i_m;
                for( i_m = _matched_line_numbers.begin();
                     i_m != _matched_line_numbers.end() ; i_m++ )
                {
                    if( (*i_m).second == most_common_number ) break;
                }
                
                if( i_m == _matched_line_numbers.end() )
                { // the line found ahs not already been matched to another line
                    _matched_line_numbers.insert(
                        boundary_multimap_typedef::value_type( cl , most_common_number ) );
                    // printf("\n <%d,%d>",cl , most_common_number );
                }
            }

        }
    }

    CGR_LOG("CDSDBRSuperMode::associateForwardAndReverseLines() exiting",DIAGNOSTIC_CGR_LOG)
}


void
CDSDBRSuperMode::
removeUnassociatedLines()
{
	CGR_LOG("CDSDBRSuperMode::removeUnassociatedLines() entered",DIAGNOSTIC_CGR_LOG)

	// erase lines from vectors that have
	// not successfully associate forward with matching reverse

	std::vector<unsigned long> lower_line_numbers;
	std::vector<unsigned long> upper_line_numbers;

	unsigned long _lm_lower_lines_count = (unsigned long)(_lm_lower_lines.size());

	// find all line numbers to begin with

	for( unsigned long i = 0; i < _lm_lower_lines_count ; i++ )
	{
		lower_line_numbers.push_back( i );
	}

	for( unsigned long i = 0; i < (unsigned long)(_lm_upper_lines.size()) ; i++ )
	{
		upper_line_numbers.push_back( i + _lm_lower_lines_count );
	}

	matchedlines_typedef::iterator i_f;
	for( i_f = _matched_line_numbers.begin() ;
		i_f != _matched_line_numbers.end() ; i_f++ )
	{
		// erase known matched line numbers from lists
		// of all forward and reverse line numbers

		std::vector<unsigned long>::iterator i_lower_m
			= std::find( lower_line_numbers.begin(), lower_line_numbers.end(), (*i_f).first );

		std::vector<unsigned long>::iterator i_upper_m
			= std::find( upper_line_numbers.begin(), upper_line_numbers.end(), (*i_f).second );

		if( i_lower_m != lower_line_numbers.end() )
			lower_line_numbers.erase(i_lower_m);

		if( i_upper_m != upper_line_numbers.end() )
			upper_line_numbers.erase(i_upper_m);
	}


	// Any line numbers remaining are unmatched -> erase them.
	// Erase in reverse order so indexing doesn't move things around.

	for( long i = (long)(lower_line_numbers.size()) - 1 ; i >=0 ; i-- )
	{
		std::vector<CDSDBRSupermodeMapLine>::iterator i_ll = _lm_lower_lines.begin();
		long num = lower_line_numbers[i];
		i_ll += lower_line_numbers[i];

		i_ll->clear();
		_lm_lower_lines.erase( i_ll );

		// need to move indexing in _matched_line_numbers accordingly
		matchedlines_typedef new_matched_line_numbers;
		matchedlines_typedef::iterator i_m;
		new_matched_line_numbers.clear();
		for( i_m = _matched_line_numbers.begin() ;
			i_m != _matched_line_numbers.end() ; i_m++ )
		{
			if( (*i_m).first > lower_line_numbers[i] )
			{
				new_matched_line_numbers.insert(
					boundary_multimap_typedef::value_type( (*i_m).first - 1, (*i_m).second ) );
			}
			else
			{
				new_matched_line_numbers.insert(*i_m);
			}
		}
		_matched_line_numbers.clear();
		for( i_m = new_matched_line_numbers.begin() ;
			i_m != new_matched_line_numbers.end() ; i_m++ )
		{
			_matched_line_numbers.insert(*i_m);
		}
	}

	for( long i = (long)(upper_line_numbers.size()) - 1 ; i >=0 ; i-- )
	{
		std::vector<CDSDBRSupermodeMapLine>::iterator i_ul = _lm_upper_lines.begin();
		long num = upper_line_numbers[i] - (long)(_lm_lower_lines_count);
		i_ul += upper_line_numbers[i] - (long)(_lm_lower_lines_count);

		i_ul->clear();
		_lm_upper_lines.erase( i_ul );


		// need to move indexing in _matched_line_numbers accordingly
		matchedlines_typedef new_matched_line_numbers;
		matchedlines_typedef::iterator i_m;
		new_matched_line_numbers.clear();
		for( i_m = _matched_line_numbers.begin() ;
			i_m != _matched_line_numbers.end() ; i_m++ )
		{
			if( (*i_m).second > upper_line_numbers[i] )
			{
				new_matched_line_numbers.insert(
					boundary_multimap_typedef::value_type( (*i_m).first, (*i_m).second - 1 ) );
			}
			else
			{
				new_matched_line_numbers.insert(*i_m);
			}
		}
		_matched_line_numbers.clear();
		for( i_m = new_matched_line_numbers.begin() ;
			i_m != new_matched_line_numbers.end() ; i_m++ )
		{
			_matched_line_numbers.insert(*i_m);
		}
	}

	CGR_LOG("CDSDBRSuperMode::removeUnassociatedLines() exiting",DIAGNOSTIC_CGR_LOG)

	return;
}

CDSDBRSupermodeMapLine::point_type
CDSDBRSuperMode::convertRowCol2LinePoint(
	long row,
	long col )
{
	CDSDBRSupermodeMapLine::point_type point_on_map;

	point_on_map.row = row;
	point_on_map.col = col;
	point_on_map.real_row = (double)row;
	point_on_map.real_col = (double)col;

	if( _Im_ramp )
	{
		point_on_map.front_pair_number = _filtered_middle_line._points[col].front_pair_number;
		point_on_map.I_rear = _filtered_middle_line._points[col].I_rear;
		point_on_map.I_front_1 = _filtered_middle_line._points[col].I_front_1;
		point_on_map.I_front_2 = _filtered_middle_line._points[col].I_front_2;
		point_on_map.I_front_3 = _filtered_middle_line._points[col].I_front_3;
		point_on_map.I_front_4 = _filtered_middle_line._points[col].I_front_4;
		point_on_map.I_front_5 = _filtered_middle_line._points[col].I_front_5;
		point_on_map.I_front_6 = _filtered_middle_line._points[col].I_front_6;
		point_on_map.I_front_7 = _filtered_middle_line._points[col].I_front_7;
		point_on_map.I_front_8 = _filtered_middle_line._points[col].I_front_8;

		point_on_map.I_phase = _phase_current._currents[row];
	}
	else
	{
		point_on_map.front_pair_number = _filtered_middle_line._points[row].front_pair_number;
		point_on_map.I_rear = _filtered_middle_line._points[row].I_rear;
		point_on_map.I_front_1 = _filtered_middle_line._points[row].I_front_1;
		point_on_map.I_front_2 = _filtered_middle_line._points[row].I_front_2;
		point_on_map.I_front_3 = _filtered_middle_line._points[row].I_front_3;
		point_on_map.I_front_4 = _filtered_middle_line._points[row].I_front_4;
		point_on_map.I_front_5 = _filtered_middle_line._points[row].I_front_5;
		point_on_map.I_front_6 = _filtered_middle_line._points[row].I_front_6;
		point_on_map.I_front_7 = _filtered_middle_line._points[row].I_front_7;
		point_on_map.I_front_8 = _filtered_middle_line._points[row].I_front_8;

		point_on_map.I_phase = _phase_current._currents[col];
	}

	return point_on_map;
}


CDSDBRSuperMode::rcode
CDSDBRSuperMode::writeMatchedLinesToFile(
	CString forward_abs_filepath,
	CString reverse_abs_filepath )
{
    //EstimationOf3SecModeBoundariesInput &input,
    //EstimationOf3SecModeBoundariesResults &results,
    //CString forward_file_name, // filename without postfix,  ***.txt
    //CString reverse_file_name, // filename without postfix,  ***.txt
    //unsigned long ramp_length )

    CGR_LOG("CDSDBRSuperMode::writeMatchedLinesToFile() entered",DIAGNOSTIC_CGR_LOG)

	rcode rval = rcode::ok;

    unsigned long ramp_length;
    unsigned long steps_count;

	if( _Im_ramp ) // middle line ramped
	{
		steps_count =(unsigned long)(_phase_current._currents.size());
		ramp_length =(unsigned long)(_filtered_middle_line._points.size());
	}
	else // phase ramped
	{
		steps_count =(unsigned long)(_filtered_middle_line._points.size());
		ramp_length =(unsigned long)(_phase_current._currents.size());
	}

	// File does not exist => okay to create it.
	std::ofstream f_strm( forward_abs_filepath );
	std::ofstream r_strm( reverse_abs_filepath );
	if( f_strm && r_strm )
	{ // streams are open

		unsigned int line_count = (unsigned long)(_lm_lower_lines.size() + _lm_upper_lines.size());
		for( unsigned int i = 0 ; i < line_count ; i++ )
		{
			std::vector<CDSDBRSupermodeMapLine>::iterator i_lm_boundary_line;
			bool forward_lower = true;
			unsigned long line_num = 0;

			if( i < (unsigned long)(_lm_lower_lines.size()) )
			{
				i_lm_boundary_line = _lm_lower_lines.begin();
				line_num = i;
				i_lm_boundary_line += line_num;
				forward_lower = true;
			}
			else
			{
				i_lm_boundary_line = _lm_upper_lines.begin();
				line_num = (i - (unsigned long)(_lm_lower_lines.size()));
				i_lm_boundary_line += line_num;
				forward_lower = false;
			}

			//ModeBoundaryLine* p_mbl = results.getLine(i);
			//std::vector<unsigned long>* p_indices = p_mbl->getX()->getIndices();
			unsigned long line_length = (unsigned long)(i_lm_boundary_line->_points.size());

			if( forward_lower) //p_mbl->getLineType() & MBL_FORWARD ) // line is on forward mode map
			{
				for( unsigned long j = 0 ; j < line_length ; j++ )
				{
					long row = i_lm_boundary_line->_points[j].row;
					long col = i_lm_boundary_line->_points[j].col;
					long index = col + row*ramp_length;
					f_strm << index << ", " << i << "\n";
				}
			}
			else // line is on reverse mode map
			{
				// find number of the matching forward mode line
				matchedlines_typedef::iterator i_f;
				for( i_f = _matched_line_numbers.begin() ;
					i_f != _matched_line_numbers.end() ; i_f++ )
				{
					if( (*i_f).second == i )
					{
						break;
					}
				}
				for( unsigned long j = 0 ; j < line_length ; j++ )
				{
					//unsigned long y_index = (unsigned long)(((*p_indices)[j])/ramp_length);
					//unsigned long x_index = (*p_indices)[j] - y_index*ramp_length;
					//unsigned long reversed_index = (ramp_length-1) - x_index + y_index*ramp_length;
					//r_strm << reversed_index;
					////r_strm << (*p_indices)[j];
					//if( i_f != results.matched_line_numbers.end() ) // found matching line
					//	r_strm << "\t" << (*i_f).first << "\n";
					//else // no matching line found
					//	r_strm << "\t" << i << "\n";
					long row = i_lm_boundary_line->_points[j].row;
					long col = i_lm_boundary_line->_points[j].col;
					long index = col + row*ramp_length;
					r_strm << index;
					if( i_f != _matched_line_numbers.end() ) // found matching line
						r_strm << ", " << (*i_f).first << "\n";
					else // no matching line found
						r_strm << ", " << i << "\n";

				}
			}

		}
	}

    CGR_LOG("CDSDBRSuperMode::writeMatchedLinesToFile() exiting",DIAGNOSTIC_CGR_LOG)

	return rval;
}


CDSDBRSuperMode::rcode
CDSDBRSuperMode::
findLMMiddleLines()
{
	CGR_LOG("CDSDBRSuperMode::findLMMiddleLines() entered",DIAGNOSTIC_CGR_LOG)

	rcode rval = rcode::ok;

	// clear all LM middle lines,
	// filtered middle lines and
	// middle lines of frequency

	for( int i = 0 ; i < (int)(_lm_middle_lines.size()) ; i++ )
	{
		_lm_middle_lines[i].clear();
	}
	_lm_middle_lines.clear();

	for( int i = 0 ; i < (int)(_lm_filtered_middle_lines.size()) ; i++ )
	{
		_lm_filtered_middle_lines[i].clear();
	}
	_lm_filtered_middle_lines.clear();

	for( int i = 0 ; i < (int)(_lm_middle_lines_of_frequency.size()) ; i++ )
	{
		_lm_middle_lines_of_frequency[i].clear();
	}
	_lm_middle_lines_of_frequency.clear();

	_average_widths.clear();

	long count_lm_lines = (long)(_lm_lower_lines.size());

	if( count_lm_lines == (long)(_lm_upper_lines.size()) )
	{

		for( long lm = 0 ; lm < count_lm_lines-1 ; lm++ )
		{
			// iterate through the boundary lines of each longitudinal mode

			std::vector<CDSDBRSupermodeMapLine>::iterator i_lower_line = _lm_upper_lines.begin();
			i_lower_line += lm;

			std::vector<CDSDBRSupermodeMapLine>::iterator i_upper_line = _lm_lower_lines.begin();
			i_upper_line += (lm+1);

			CDSDBRSupermodeMapLine new_lm_middle_line;
			_lm_middle_lines.push_back(new_lm_middle_line);

			bool middle_point_found_at_col_zero = false;
			bool middle_point_found_at_last_col = false;

			// if right-most column covered is not last column in map
			//   find midpoint on rhs-top border
			//   plot line from right-most point found to midpoint
			//   add points along line to middle line
			// if left-most column covered is not first column in map
			//   find midpoint on lhs_bottom border
			//   plot line from left-most point found to lhs_bottom border
			//   add points along line to middle line

			long widths_total = 0;
			long widths_count = 0;

			for( long i = 0; i < _cols ; i++ )
			{
				// for each column

				long highest_row_at_i_on_lower_line = -1;
				long index_of_highest_row_at_i_on_lower_line = -1;
				long lowest_row_at_i_on_lower_line = _rows;
				long index_of_lowest_row_at_i_on_lower_line = -1;

				// find highest and lowest points on lower line at column i

				for( long j = 0; j < (long)(i_lower_line->_points.size()) ; j++ )
				{
					if( i_lower_line->_points[j].col == i
					&& i_lower_line->_points[j].row > highest_row_at_i_on_lower_line )
					{
						index_of_highest_row_at_i_on_lower_line = j;
						highest_row_at_i_on_lower_line = i_lower_line->_points[j].row;
					}
					if( i_lower_line->_points[j].col == i
					&& i_lower_line->_points[j].row < lowest_row_at_i_on_lower_line )
					{
						index_of_lowest_row_at_i_on_lower_line = j;
						lowest_row_at_i_on_lower_line = i_lower_line->_points[j].row;
					}
				}

				long highest_row_at_i_on_upper_line = -1;
				long index_of_highest_row_at_i_on_upper_line = -1;
				long lowest_row_at_i_on_upper_line = _rows;
				long index_of_lowest_row_at_i_on_upper_line = -1;

				// find highest and lowest point on upper line at column i

				for( long j = 0; j < (long)(i_upper_line->_points.size()) ; j++ )
				{
					if( i_upper_line->_points[j].col == i
					&& i_upper_line->_points[j].row > highest_row_at_i_on_upper_line )
					{
						index_of_highest_row_at_i_on_upper_line = j;
						highest_row_at_i_on_upper_line = i_upper_line->_points[j].row;
					}
					if( i_upper_line->_points[j].col == i
					&& i_upper_line->_points[j].row < lowest_row_at_i_on_upper_line )
					{
						index_of_lowest_row_at_i_on_upper_line = j;
						lowest_row_at_i_on_upper_line = i_upper_line->_points[j].row;
					}
				}

				if( index_of_highest_row_at_i_on_lower_line != -1
				&& index_of_lowest_row_at_i_on_upper_line != -1 )
				{
					if( i == 0 ) middle_point_found_at_col_zero = true;
					if( i == _cols-1 ) middle_point_found_at_last_col = true;

					// if both found, check min and max separation distances
					long current_min_width = lowest_row_at_i_on_upper_line - highest_row_at_i_on_lower_line;
					long current_max_width = highest_row_at_i_on_upper_line - lowest_row_at_i_on_lower_line;

					if( current_min_width >= (long)(CG_REG->get_DSDBR01_SMBD_min_points_width_of_lm())
					 && current_max_width <= (long)(CG_REG->get_DSDBR01_SMBD_max_points_width_of_lm()) )
					{

						widths_total += current_min_width;
						widths_count++;

						// find vertical midpoint

						long row_of_midpoint = highest_row_at_i_on_lower_line + (long)(((double)current_min_width)/2);

						//     add to middle line
						_lm_middle_lines[lm]._points.push_back(
							convertRowCol2LinePoint(
								row_of_midpoint,
								i ) );
					}
					else
					{
						// supermode min width not met, remove all points
						_lm_middle_lines[lm].clear();
						break;
					}
				}
			}

			if( _lm_middle_lines[lm]._points.size() > 0 )
				_average_widths.push_back( (double)widths_total/(double)widths_count );
			else
				_average_widths.push_back( 0 );


			if( !middle_point_found_at_col_zero
			&& !(_lm_middle_lines[lm]._points.empty()) )
			{
				// need to fill in the start of the middle line to the lhs-bottom border
				// find middle start point
				// calculate points on straight line fit first known middle point
				// and add to start of middle line

				std::vector<CDSDBRSupermodeMapLine::point_type>::iterator lower_border_pt = i_lower_line->_points.end();

				for( long j = 0; j < (long)(i_lower_line->_points.size()) ; j++ )
				{
					if( (i_lower_line->_points[j].col == 0
					|| i_lower_line->_points[j].row == 0)
					&& lower_border_pt == i_lower_line->_points.end() )
					{
						// found point on lhs-bottom border
						lower_border_pt = i_lower_line->_points.begin();
						lower_border_pt += j;
						break;
					}
				}

				std::vector<CDSDBRSupermodeMapLine::point_type>::iterator upper_border_pt = i_upper_line->_points.end();

				for( long j = 0; j < (long)(i_upper_line->_points.size()) ; j++ )
				{
					if( (i_upper_line->_points[j].col == 0
					|| i_upper_line->_points[j].row == 0)
					&& upper_border_pt == i_upper_line->_points.end() )
					{
						// found point on lhs-bottom border
						upper_border_pt = i_upper_line->_points.begin();
						upper_border_pt += j;
						break;
					}
				}

				if( lower_border_pt != i_lower_line->_points.end()
				&& upper_border_pt != i_upper_line->_points.end() )
				{
					// Have upper and lower border points, find midpoint

					long lower_dist_from_origin;
					if( (*lower_border_pt).col == 0 ) lower_dist_from_origin = -(*lower_border_pt).row;
					else lower_dist_from_origin = (*lower_border_pt).col;

					long upper_dist_from_origin;
					if( (*upper_border_pt).col == 0 ) upper_dist_from_origin = -(*upper_border_pt).row;
					else upper_dist_from_origin = (*upper_border_pt).col;

					long border_midpoint_row;
					long border_midpoint_col;
					long border_midpoint_dist_from_origin = lower_dist_from_origin - (lower_dist_from_origin - upper_dist_from_origin)/2;
					if( border_midpoint_dist_from_origin > 0 )
					{
						border_midpoint_row = 0;
						border_midpoint_col = border_midpoint_dist_from_origin;
					}
					else
					{
						border_midpoint_row = -border_midpoint_dist_from_origin;
						border_midpoint_col = 0;
					}

					std::vector<CDSDBRSupermodeMapLine::point_type>::iterator first_middle_pt = _lm_middle_lines[lm]._points.begin();
					long first_middle_pt_col = (*first_middle_pt).col;
					long first_middle_pt_row = (*first_middle_pt).row;

					double slope = ((double)(first_middle_pt_row - border_midpoint_row))/((double)(first_middle_pt_col - border_midpoint_col));

					// starting at the first existing point,
					// insert one new point in to middle line
					// for each column between the first existing point and the border point

					for( long i = first_middle_pt_col-1 ; i >= border_midpoint_col ; i-- )
					{
						double row_of_midpoint_double = (double)border_midpoint_row + slope*((double)(i-border_midpoint_col));
						long row_of_midpoint = (long)row_of_midpoint_double;
						if( row_of_midpoint_double - (double)row_of_midpoint > 0.5 ) row_of_midpoint++;
						if( row_of_midpoint < 0 ) row_of_midpoint = 0;

						// add to middle line
						_lm_middle_lines[lm]._points.insert(
							_lm_middle_lines[lm]._points.begin(),
							convertRowCol2LinePoint(
								row_of_midpoint,
								i ) );
					}
				}

			}



			if( !middle_point_found_at_last_col
			&& !(_lm_middle_lines[lm]._points.empty()) )
			{
				// need to fill in the end of the middle line to the rhs-top border
				// find middle end point
				// calculate points on straight line fit from last known middle point
				// and add to end of middle line

				//std::vector<CDSDBRSupermodeMapLine::point_type>::iterator last_middle_pt = _lm_middle_lines[lm]._points.end();
				//last_middle_pt--;

				std::vector<CDSDBRSupermodeMapLine::point_type>::iterator lower_border_pt = i_lower_line->_points.end();

				for( long j = 0; j < (long)(i_lower_line->_points.size()) ; j++ )
				{
					if( (i_lower_line->_points[j].col == _cols-1
					|| i_lower_line->_points[j].row == _rows-1)
					&& lower_border_pt == i_lower_line->_points.end() )
					{
						// found point on rhs-top border
						lower_border_pt = i_lower_line->_points.begin();
						lower_border_pt += j;
						break;
					}
				}


				std::vector<CDSDBRSupermodeMapLine::point_type>::iterator upper_border_pt = i_upper_line->_points.end();

				for( long j = 0; j < (long)(i_upper_line->_points.size()) ; j++ )
				{
					if( (i_upper_line->_points[j].col == _cols-1
					|| i_upper_line->_points[j].row == _rows-1)
					&& upper_border_pt == i_upper_line->_points.end() )
					{
						// found point on rhs-top border
						upper_border_pt = i_upper_line->_points.begin();
						upper_border_pt += j;
						break;
					}
				}

				if( lower_border_pt != i_lower_line->_points.end()
				&& upper_border_pt != i_upper_line->_points.end() )
				{
					// Have upper and lower border points, find midpoint


					long lower_dist_from_toprightcorner;
					if( (*lower_border_pt).col == _cols-1 ) lower_dist_from_toprightcorner = (_rows-1)-(*lower_border_pt).row;
					else lower_dist_from_toprightcorner = (*lower_border_pt).col - (_cols-1);

					long upper_dist_from_toprightcorner;
					if( (*upper_border_pt).col == _cols-1 ) upper_dist_from_toprightcorner = (_rows-1)-(*upper_border_pt).row;
					else upper_dist_from_toprightcorner = (*upper_border_pt).col - (_cols-1);

					long border_midpoint_row;
					long border_midpoint_col;
					long border_midpoint_dist_from_toprightcorner;
					long last_midpoint_row = (*(_lm_middle_lines[lm]._points.rbegin())).row;

					if( (*lower_border_pt).row < last_midpoint_row )
					{
						double average_width = _average_widths[lm];
						long pts_to_add = (long)(average_width/2);
						border_midpoint_dist_from_toprightcorner = lower_dist_from_toprightcorner - pts_to_add;
						if( (_rows-1) - last_midpoint_row  < border_midpoint_dist_from_toprightcorner )
							border_midpoint_dist_from_toprightcorner = (_rows-1) - last_midpoint_row;
					}
					else
					{
						border_midpoint_dist_from_toprightcorner = lower_dist_from_toprightcorner - (lower_dist_from_toprightcorner - upper_dist_from_toprightcorner)/2;
					}

					if( border_midpoint_dist_from_toprightcorner > 0 )
					{
						border_midpoint_row = (_rows-1)-border_midpoint_dist_from_toprightcorner;
						border_midpoint_col = _cols-1;
					}
					else
					{
						border_midpoint_row = _rows-1;
						border_midpoint_col = (_cols-1)+border_midpoint_dist_from_toprightcorner;
					}


					std::vector<CDSDBRSupermodeMapLine::point_type>::iterator last_middle_pt = _lm_middle_lines[lm]._points.end();
					last_middle_pt--;
					long last_middle_pt_col = (*last_middle_pt).col;
					long last_middle_pt_row = (*last_middle_pt).row;

					double slope = ((double)(border_midpoint_row - last_middle_pt_row))/((double)(border_midpoint_col - last_middle_pt_col));

					// starting at the end,
					// insert one new point in to middle line
					// for each column between the last existing point and the border point

					for( long i = last_middle_pt_col+1 ; i <= border_midpoint_col ; i++ )
					{
						double row_of_midpoint_double = (double)last_middle_pt_row + slope*((double)(i-last_middle_pt_col));
						long row_of_midpoint = (long)row_of_midpoint_double;
						if( row_of_midpoint_double - (double)row_of_midpoint > 0.5 ) row_of_midpoint++;
						if( row_of_midpoint > _rows-1 ) row_of_midpoint = _rows-1;

						// add to middle line
						_lm_middle_lines[lm]._points.push_back(
							convertRowCol2LinePoint(
								row_of_midpoint,
								i ) );
					}
				}

			}

		}
	}
	else
	{
		rval = rcode::mismatched_lines;
		CGR_LOG("CDSDBRSuperMode::findLMMiddleLines() mismatched_lines", ERROR_CGR_LOG)
	}


	if( CG_REG->get_DSDBR01_SMBD_write_debug_files() )
	{

		for( long lm = 0 ; lm < (long)_lm_middle_lines.size() ; lm++ )
		{
			// iterate through the boundary lines of each longitudinal mode

			CString middle_line_abs_filepath = _map_forward_power_ratio_median_filtered._abs_filepath;
			char buf[100];
			sprintf(buf,"middle_F_lm%d_",lm);
			middle_line_abs_filepath.Replace( MATRIX_ , _T(buf) );

			_lm_middle_lines[lm].writeRowColToFile(
				middle_line_abs_filepath,
				true );

			middle_line_abs_filepath = _map_forward_power_ratio_median_filtered._abs_filepath;
			sprintf(buf,"middleFcurrents_lm%d_",lm);
			middle_line_abs_filepath.Replace( MATRIX_ , _T(buf) );

			_lm_middle_lines[lm].writeCurrentsToFile(
				middle_line_abs_filepath,
				true );
		}

		for( long lm = 0 ; lm < (long)_lm_lower_lines.size() ; lm++ )
		{
			// Assume _abs_filepath is set from loading data from file
			// or from writing to file after gathering data from laser
			CString abs_filepath = _map_forward_power_ratio_median_filtered._abs_filepath;
			char buf[100];
			sprintf(buf,"lower_F_lm%d_",lm);
			abs_filepath.Replace( MATRIX_ , _T(buf) );

			// write boundary line to file
			_lm_lower_lines[lm].writeRowColToFile( abs_filepath, true );
		}

		for( long lm = 0 ; lm < (long)_lm_upper_lines.size() ; lm++ )
		{
			// Assume _abs_filepath is set from loading data from file
			// or from writing to file after gathering data from laser
			CString abs_filepath = _map_reverse_power_ratio_median_filtered._abs_filepath;
			char buf[100];
			sprintf(buf,"upper_F_lm%d_",lm);
			abs_filepath.Replace( MATRIX_ , _T(buf) );

			// write boundary line to file
			_lm_upper_lines[lm].writeRowColToFile( abs_filepath, true );
		}
	}


	CString log_msg("CDSDBRSuperMode::findLMMiddleLines() ");
	log_msg.AppendFormat("found %d middle lines", _lm_middle_lines.size() );
	CGR_LOG(log_msg,INFO_CGR_LOG)

	CGR_LOG("CDSDBRSuperMode::findLMMiddleLines() exiting",DIAGNOSTIC_CGR_LOG)

	return rval;
}

CDSDBRSuperMode::rcode
CDSDBRSuperMode::
findMiddleLines(
	std::vector<CDSDBRSupermodeMapLine> &lines,
	std::vector<CDSDBRSupermodeMapLine> &middle_lines )
{
	CGR_LOG("CDSDBRSuperMode::findMiddleLines() entered",DIAGNOSTIC_CGR_LOG)
	rcode rval = rcode::ok;

	std::vector<double> average_widths;

	for( int i = 0 ; i < (int)(middle_lines.size()) ; i++ )
	{
		middle_lines[i].clear();
	}
	middle_lines.clear();

	long count_lm_lines = (long)(lines.size());

	if( count_lm_lines > 1 )
	{
		for( long lm = 0 ; lm < count_lm_lines-1 ; lm++ )
		{
			// iterate through the boundary lines of each longitudinal mode

			std::vector<CDSDBRSupermodeMapLine>::iterator i_lower_line = lines.begin();
			i_lower_line += lm;
			std::vector<CDSDBRSupermodeMapLine>::iterator i_upper_line = i_lower_line;
			i_upper_line++;

			CDSDBRSupermodeMapLine new_lm_middle_line;
			middle_lines.push_back(new_lm_middle_line);

			bool middle_point_found_at_col_zero = false;
			bool middle_point_found_at_last_col = false;

			// if right-most column covered is not last column in map
			//   find midpoint on rhs-top border
			//   plot line from right-most point found to midpoint
			//   add points along line to middle line
			// if left-most column covered is not first column in map
			//   find midpoint on lhs_bottom border
			//   plot line from left-most point found to lhs_bottom border
			//   add points along line to middle line

			long widths_total = 0;
			long widths_count = 0;

			for( long i = 0; i < _cols ; i++ )
			{
				// for each column

				long highest_row_at_i_on_lower_line = -1;
				long index_of_highest_row_at_i_on_lower_line = -1;
				long lowest_row_at_i_on_lower_line = _rows;
				long index_of_lowest_row_at_i_on_lower_line = -1;

				// find highest and lowest points on lower line at column i

				for( long j = 0; j < (long)(i_lower_line->_points.size()) ; j++ )
				{
					if( i_lower_line->_points[j].col == i
					&& i_lower_line->_points[j].row > highest_row_at_i_on_lower_line )
					{
						index_of_highest_row_at_i_on_lower_line = j;
						highest_row_at_i_on_lower_line = i_lower_line->_points[j].row;
					}
					if( i_lower_line->_points[j].col == i
					&& i_lower_line->_points[j].row < lowest_row_at_i_on_lower_line )
					{
						index_of_lowest_row_at_i_on_lower_line = j;
						lowest_row_at_i_on_lower_line = i_lower_line->_points[j].row;
					}
				}

				long highest_row_at_i_on_upper_line = -1;
				long index_of_highest_row_at_i_on_upper_line = -1;
				long lowest_row_at_i_on_upper_line = _rows;
				long index_of_lowest_row_at_i_on_upper_line = -1;

				// find highest and lowest point on upper line at column i

				for( long j = 0; j < (long)(i_upper_line->_points.size()) ; j++ )
				{
					if( i_upper_line->_points[j].col == i
					&& i_upper_line->_points[j].row > highest_row_at_i_on_upper_line )
					{
						index_of_highest_row_at_i_on_upper_line = j;
						highest_row_at_i_on_upper_line = i_upper_line->_points[j].row;
					}
					if( i_upper_line->_points[j].col == i
					&& i_upper_line->_points[j].row < lowest_row_at_i_on_upper_line )
					{
						index_of_lowest_row_at_i_on_upper_line = j;
						lowest_row_at_i_on_upper_line = i_upper_line->_points[j].row;
					}
				}

				if( index_of_highest_row_at_i_on_lower_line != -1
				&& index_of_lowest_row_at_i_on_upper_line != -1 )
				{
					if( i == 0 ) middle_point_found_at_col_zero = true;
					if( i == _cols-1 ) middle_point_found_at_last_col = true;

					// if both found, check min and max separation distances
					long current_min_width = lowest_row_at_i_on_upper_line - highest_row_at_i_on_lower_line;

					widths_total += current_min_width;
					widths_count++;

					// find vertical midpoint

					long row_of_midpoint = highest_row_at_i_on_lower_line + (long)(((double)current_min_width)/2);

					//     add to middle line
					middle_lines[lm]._points.push_back(
						convertRowCol2LinePoint(
							row_of_midpoint,
							i ) );
				}
			}

			if( middle_lines[lm]._points.size() > 0 )
				average_widths.push_back( (double)widths_total/(double)widths_count );
			else
				average_widths.push_back( 0 );

			if( !middle_point_found_at_col_zero
			&& !(middle_lines[lm]._points.empty()) )
			{
				// need to fill in the start of the middle line to the lhs-bottom border
				// find middle start point
				// calculate points on straight line fit first known middle point
				// and add to start of middle line

				std::vector<CDSDBRSupermodeMapLine::point_type>::iterator lower_border_pt = i_lower_line->_points.end();

				for( long j = 0; j < (long)(i_lower_line->_points.size()) ; j++ )
				{
					if( (i_lower_line->_points[j].col == 0
					|| i_lower_line->_points[j].row == 0)
					&& lower_border_pt == i_lower_line->_points.end() )
					{
						// found point on lhs-bottom border
						lower_border_pt = i_lower_line->_points.begin();
						lower_border_pt += j;
						break;
					}
				}

				std::vector<CDSDBRSupermodeMapLine::point_type>::iterator upper_border_pt = i_upper_line->_points.end();

				for( long j = 0; j < (long)(i_upper_line->_points.size()) ; j++ )
				{
					if( (i_upper_line->_points[j].col == 0
					|| i_upper_line->_points[j].row == 0)
					&& upper_border_pt == i_upper_line->_points.end() )
					{
						// found point on lhs-bottom border
						upper_border_pt = i_upper_line->_points.begin();
						upper_border_pt += j;
						break;
					}
				}

				if( lower_border_pt != i_lower_line->_points.end()
				&& upper_border_pt != i_upper_line->_points.end() )
				{
					// Have upper and lower border points, find midpoint

					long lower_dist_from_origin;
					if( (*lower_border_pt).col == 0 ) lower_dist_from_origin = -(*lower_border_pt).row;
					else lower_dist_from_origin = (*lower_border_pt).col;

					long upper_dist_from_origin;
					if( (*upper_border_pt).col == 0 ) upper_dist_from_origin = -(*upper_border_pt).row;
					else upper_dist_from_origin = (*upper_border_pt).col;

					long border_midpoint_row;
					long border_midpoint_col;
					long border_midpoint_dist_from_origin = lower_dist_from_origin - (lower_dist_from_origin - upper_dist_from_origin)/2;
					if( border_midpoint_dist_from_origin > 0 )
					{
						border_midpoint_row = 0;
						border_midpoint_col = border_midpoint_dist_from_origin;
					}
					else
					{
						border_midpoint_row = -border_midpoint_dist_from_origin;
						border_midpoint_col = 0;
					}

					std::vector<CDSDBRSupermodeMapLine::point_type>::iterator first_middle_pt = middle_lines[lm]._points.begin();
					long first_middle_pt_col = (*first_middle_pt).col;
					long first_middle_pt_row = (*first_middle_pt).row;

					double slope = ((double)(first_middle_pt_row - border_midpoint_row))/((double)(first_middle_pt_col - border_midpoint_col));

					// starting at the first existing point,
					// insert one new point in to middle line
					// for each column between the first existing point and the border point

					for( long i = first_middle_pt_col-1 ; i >= border_midpoint_col ; i-- )
					{
						double row_of_midpoint_double = (double)border_midpoint_row + slope*((double)(i-border_midpoint_col));
						long row_of_midpoint = (long)row_of_midpoint_double;
						if( row_of_midpoint_double - (double)row_of_midpoint > 0.5 ) row_of_midpoint++;
						if( row_of_midpoint < 0 ) row_of_midpoint = 0;

						// add to middle line
						middle_lines[lm]._points.insert(
							middle_lines[lm]._points.begin(),
							convertRowCol2LinePoint(
								row_of_midpoint,
								i ) );
					}
				}

			}

			if( !middle_point_found_at_last_col
			&& !(middle_lines[lm]._points.empty()) )
			{
				// need to fill in the end of the middle line to the rhs-top border
				// find middle end point
				// calculate points on straight line fit from last known middle point
				// and add to end of middle line

				//std::vector<CDSDBRSupermodeMapLine::point_type>::iterator last_middle_pt = middle_lines[lm]._points.end();
				//last_middle_pt--;

				std::vector<CDSDBRSupermodeMapLine::point_type>::iterator lower_border_pt = i_lower_line->_points.end();

				for( long j = 0; j < (long)(i_lower_line->_points.size()) ; j++ )
				{
					if( (i_lower_line->_points[j].col == _cols-1
					|| i_lower_line->_points[j].row == _rows-1)
					&& lower_border_pt == i_lower_line->_points.end() )
					{
						// found point on rhs-top border
						lower_border_pt = i_lower_line->_points.begin();
						lower_border_pt += j;
						break;
					}
				}


				std::vector<CDSDBRSupermodeMapLine::point_type>::iterator upper_border_pt = i_upper_line->_points.end();

				for( long j = 0; j < (long)(i_upper_line->_points.size()) ; j++ )
				{
					if( (i_upper_line->_points[j].col == _cols-1
					|| i_upper_line->_points[j].row == _rows-1)
					&& upper_border_pt == i_upper_line->_points.end() )
					{
						// found point on rhs-top border
						upper_border_pt = i_upper_line->_points.begin();
						upper_border_pt += j;
						break;
					}
				}

				if( lower_border_pt != i_lower_line->_points.end()
				&& upper_border_pt != i_upper_line->_points.end() )
				{
					// Have upper and lower border points, find midpoint


					long lower_dist_from_toprightcorner;
					if( (*lower_border_pt).col == _cols-1 ) lower_dist_from_toprightcorner = (_rows-1)-(*lower_border_pt).row;
					else lower_dist_from_toprightcorner = (*lower_border_pt).col - (_cols-1);

					long upper_dist_from_toprightcorner;
					if( (*upper_border_pt).col == _cols-1 ) upper_dist_from_toprightcorner = (_rows-1)-(*upper_border_pt).row;
					else upper_dist_from_toprightcorner = (*upper_border_pt).col - (_cols-1);

					long border_midpoint_row;
					long border_midpoint_col;
					long border_midpoint_dist_from_toprightcorner;
					long last_midpoint_row = (*(middle_lines[lm]._points.rbegin())).row;

					if( (*lower_border_pt).row < last_midpoint_row )
					{
						double average_width = average_widths[lm];
						long pts_to_add = (long)(average_width/2);
						border_midpoint_dist_from_toprightcorner = lower_dist_from_toprightcorner - pts_to_add;
						if( (_rows-1) - last_midpoint_row  < border_midpoint_dist_from_toprightcorner )
							border_midpoint_dist_from_toprightcorner = (_rows-1) - last_midpoint_row;
					}
					else
					{
						border_midpoint_dist_from_toprightcorner = lower_dist_from_toprightcorner - (lower_dist_from_toprightcorner - upper_dist_from_toprightcorner)/2;
					}

					if( border_midpoint_dist_from_toprightcorner > 0 )
					{
						border_midpoint_row = (_rows-1)-border_midpoint_dist_from_toprightcorner;
						border_midpoint_col = _cols-1;
					}
					else
					{
						border_midpoint_row = _rows-1;
						border_midpoint_col = (_cols-1)+border_midpoint_dist_from_toprightcorner;
					}


					std::vector<CDSDBRSupermodeMapLine::point_type>::iterator last_middle_pt = middle_lines[lm]._points.end();
					last_middle_pt--;
					long last_middle_pt_col = (*last_middle_pt).col;
					long last_middle_pt_row = (*last_middle_pt).row;

					double slope = ((double)(border_midpoint_row - last_middle_pt_row))/((double)(border_midpoint_col - last_middle_pt_col));

					// starting at the end,
					// insert one new point in to middle line
					// for each column between the last existing point and the border point

					for( long i = last_middle_pt_col+1 ; i <= border_midpoint_col ; i++ )
					{
						double row_of_midpoint_double = (double)last_middle_pt_row + slope*((double)(i-last_middle_pt_col));
						long row_of_midpoint = (long)row_of_midpoint_double;
						if( row_of_midpoint_double - (double)row_of_midpoint > 0.5 ) row_of_midpoint++;
						if( row_of_midpoint > _rows-1 ) row_of_midpoint = _rows-1;

						// add to middle line
						middle_lines[lm]._points.push_back(
							convertRowCol2LinePoint(
								row_of_midpoint,
								i ) );
					}
				}
			}
		}
	}
	else
	{
		rval = rcode::no_data;
		CGR_LOG("CDSDBRSuperMode::findMiddleLines() not enough lines", ERROR_CGR_LOG)
	}

	CGR_LOG("CDSDBRSuperMode::findMiddleLines() exiting",DIAGNOSTIC_CGR_LOG)

	return rval;
}

CDSDBRSuperMode::rcode
CDSDBRSuperMode::
filterLMMiddleLines()
{
	CGR_LOG("CDSDBRSuperMode::filterLMMiddleLines() entered",DIAGNOSTIC_CGR_LOG)

	rcode rval = rcode::ok;

	// clear filtered middle lines and freq middle lines

	for( int i = 0 ; i < (int)(_lm_filtered_middle_lines.size()) ; i++ )
	{
		_lm_filtered_middle_lines[i].clear();
	}
	_lm_filtered_middle_lines.clear();

	for( int i = 0 ; i < (int)(_lm_middle_lines_of_frequency.size()) ; i++ )
	{
		_lm_middle_lines_of_frequency[i].clear();
	}
	_lm_middle_lines_of_frequency.clear();



	long filter_n = (long)(CG_REG->get_DSDBR01_SMBD_ml_moving_ave_filter_n());

	for( short lm = 0 ; lm < (short)(_lm_middle_lines.size()) ; lm++ )
	{
		CDSDBRSupermodeMapLine new_filtered_lm_middle_line;

		_lm_filtered_middle_lines.push_back(new_filtered_lm_middle_line);

		// Perform averaging in the middle line direction only
		// which is a combination of If and Ir


		for( long i = 0; i < (long)(_lm_middle_lines[lm]._points.size()) ; i++ )
		{
			long window_size = 0;
			long sum_of_rows_of_windowed_points = 0;
			for( long j = i - filter_n ; j <= i + filter_n ; j++ )
			{
				if( j >=0 && j < (long)(_lm_middle_lines[lm]._points.size()) )
				{
					// sum rows in window, without exceeding start or end
					window_size++;
					sum_of_rows_of_windowed_points += _lm_middle_lines[lm]._points[j].row;
				}
			}

			// the structure CDSDBROverallModeMapLine::point_type
			// contains both the row, col position on a map and
			// the currents at that point.
			// For the average, we need to interpolate the If current
			// while finding the nearest row (just so it can be plotted on a map)

			CDSDBRSupermodeMapLine::point_type averaged_pt
				= interpolateLinePoint(
					(double)sum_of_rows_of_windowed_points / (double)window_size,
					(double)(_lm_middle_lines[lm]._points[i].col) );

			// Add averaged point
			_lm_filtered_middle_lines[lm]._points.push_back( averaged_pt );
		}

	}

	CGR_LOG("CDSDBRSuperMode::filterLMMiddleLines() exiting",DIAGNOSTIC_CGR_LOG)

	return rval;
}


bool g_prevent_front_section_switch = false;
short g_current_front_pair_number = 0;


CDSDBRSupermodeMapLine::point_type
CDSDBRSuperMode::interpolateLinePoint(
	double row,
	double col )
{
	CDSDBRSupermodeMapLine::point_type point_on_map;

	point_on_map.real_row = row;
	point_on_map.real_col = col;

	// find nearest row
	point_on_map.row = (long)row;
	if( row - (double)((long)row) > 0.5 ) point_on_map.row++;

	// find nearest col
	point_on_map.col = (long)col;
	if( col - (double)((long)col) > 0.5 ) point_on_map.col++;

	if( col > (double)((long)col) )
	{
		// interpolate Ip linearly
		double Ip_a = _phase_current._currents[(long)col];
		double Ip_b = _phase_current._currents[((long)col)+1];

		double Ip_fraction = col - (double)((long)col);

		point_on_map.I_phase = Ip_a + Ip_fraction*( Ip_b - Ip_a );
	}
	else
	{
		// no need to interpolate
		point_on_map.I_phase = _phase_current._currents[(long)col];
	}

	point_on_map.I_front_1 = 0;
	point_on_map.I_front_2 = 0;
	point_on_map.I_front_3 = 0;
	point_on_map.I_front_4 = 0;
	point_on_map.I_front_5 = 0;
	point_on_map.I_front_6 = 0;
	point_on_map.I_front_7 = 0;
	point_on_map.I_front_8 = 0;

	// use the nearest point to determine which map to use
	point_on_map.front_pair_number = _filtered_middle_line.pair_number( point_on_map.row );

	short index_of_nearest = (short)(_filtered_middle_line.index_in_submap( point_on_map.row ));

	// find out how far the interpolated point is from the nearest point
	double If_fraction = row - (double)(point_on_map.row);
	long top_row = _filtered_middle_line._submap_length[ point_on_map.front_pair_number-1 ];

	if( ( index_of_nearest == 0 && If_fraction < 0 )
	 || ( index_of_nearest == (short)top_row && If_fraction > 0 ) )
	{
		// cannot interpolate between maps!
		// use nearest point for currents
		If_fraction = 0;
	}

	double If_value;

	if( If_fraction != 0 )
	{
		// need to interpolate
		short index_of_nextpt;
		long next_row;
		if( If_fraction > 0 )
		{
			index_of_nextpt = index_of_nearest+1;
			next_row = point_on_map.row + 1;
			//Ir_fraction = If_fraction;
		}
		else
		{
			index_of_nextpt = index_of_nearest-1;
			next_row = point_on_map.row - 1;
			If_fraction = -If_fraction; // ensure positive fraction
		}

		// interpolate If linearly
		double If_a = _filtered_middle_line.getNonConstantIf( point_on_map.front_pair_number, index_of_nearest );
		double If_b = _filtered_middle_line.getNonConstantIf( point_on_map.front_pair_number, index_of_nextpt );
		If_value = If_a + If_fraction*( If_b - If_a );


		// interpolate Ir linearly
		double Ir_a = _filtered_middle_line._points[point_on_map.row].I_rear;
		double Ir_b = _filtered_middle_line._points[next_row].I_rear;

		point_on_map.I_rear = Ir_a + If_fraction*( Ir_b - Ir_a );
	}
	else
	{
		// no need to interpolate
		If_value = _filtered_middle_line.getNonConstantIf( point_on_map.front_pair_number, index_of_nearest );

		point_on_map.I_rear = _filtered_middle_line._points[(long)row].I_rear;

		point_on_map.real_row = (double)((long)row);
	}


	if( g_prevent_front_section_switch )
	{
		if( point_on_map.front_pair_number > g_current_front_pair_number )
		{
			point_on_map.front_pair_number = g_current_front_pair_number;
			top_row = _filtered_middle_line._submap_length[ point_on_map.front_pair_number-1 ]-1;
			If_value = _filtered_middle_line.getNonConstantIf( point_on_map.front_pair_number, top_row );
			index_of_nearest = (short)top_row;
		}
		else if( point_on_map.front_pair_number < g_current_front_pair_number )
		{
			point_on_map.front_pair_number = g_current_front_pair_number;
			If_value = _filtered_middle_line.getNonConstantIf( point_on_map.front_pair_number, 0 );
			index_of_nearest = 0;
		}
	}
	else
	{
		g_current_front_pair_number = point_on_map.front_pair_number;
	}

	switch( point_on_map.front_pair_number )
	{
	case 1:
		point_on_map.I_front_1 = _filtered_middle_line.getConstantIf( point_on_map.front_pair_number, index_of_nearest );
		point_on_map.I_front_2 = If_value;
		break;
	case 2:
		point_on_map.I_front_2 = _filtered_middle_line.getConstantIf( point_on_map.front_pair_number, index_of_nearest );
		point_on_map.I_front_3 = If_value;
		break;
	case 3:
		point_on_map.I_front_3 = _filtered_middle_line.getConstantIf( point_on_map.front_pair_number, index_of_nearest );
		point_on_map.I_front_4 = If_value;
		break;
	case 4:
		point_on_map.I_front_4 = _filtered_middle_line.getConstantIf( point_on_map.front_pair_number, index_of_nearest );
		point_on_map.I_front_5 = If_value;
		break;
	case 5:
		point_on_map.I_front_5 = _filtered_middle_line.getConstantIf( point_on_map.front_pair_number, index_of_nearest );
		point_on_map.I_front_6 = If_value;
		break;
	case 6:
		point_on_map.I_front_6 = _filtered_middle_line.getConstantIf( point_on_map.front_pair_number, index_of_nearest );
		point_on_map.I_front_7 = If_value;
		break;
	case 7:
		point_on_map.I_front_7 = _filtered_middle_line.getConstantIf( point_on_map.front_pair_number, index_of_nearest );
		point_on_map.I_front_8 = If_value;
		break;
	}

	return point_on_map;
}


CDSDBRSuperMode::rcode
CDSDBRSuperMode::
calcLMMiddleLinesOfFreq()
{
	CGR_LOG("CDSDBRSuperMode::calcLMMiddleLinesOfFreq() entered",DIAGNOSTIC_CGR_LOG)

	rcode rval = rcode::ok;

	// clear freq middle lines

	for( int i = 0 ; i < (int)(_lm_middle_lines_of_frequency.size()) ; i++ )
	{
		_lm_middle_lines_of_frequency[i].clear();
	}
	_lm_middle_lines_of_frequency.clear();


	double vertical_percent_shift = CG_REG->get_DSDBR01_SMBD_vertical_percent_shift(); // set this per supermode, range +-50%

	for( short lm = 0 ; lm < (short)(_lm_filtered_middle_lines.size()) ; lm++ )
	{
		CDSDBRSupermodeMapLine new_lm_middle_line_of_frequency;

		_lm_middle_lines_of_frequency.push_back( new_lm_middle_line_of_frequency );

		double average_width = _average_widths[lm];

		if( average_width > 0 )
		{
			double points_to_shift = average_width*(vertical_percent_shift/100);

			for( long i = 0 ; i < (long)(_lm_filtered_middle_lines[lm]._points.size()) ; i++ )
			{
				double row = (double)(_lm_filtered_middle_lines[lm]._points[i].row);
				double col = (double)(_lm_filtered_middle_lines[lm]._points[i].col);

				row += points_to_shift;

				// ensure point is still on the map, otherwise ignore it
				if( row >= 0 && row <= (double)(_rows-1) )
				{
					CDSDBRSupermodeMapLine::point_type shifted_pt
						= interpolateLinePoint(	row, col );

					_lm_middle_lines_of_frequency[lm]._points.push_back( shifted_pt );
				}
			}

		}

	}

	//_lm_middle_lines_of_frequency_removed = (short)_lm_middle_lines_of_frequency.size();

	CGR_LOG("CDSDBRSuperMode::calcLMMiddleLinesOfFreq() exiting",DIAGNOSTIC_CGR_LOG)

	return rval;
}

CDSDBRSuperMode::rcode
CDSDBRSuperMode::
checkLMMiddleLinesOfFreq()
{
	CGR_LOG("CDSDBRSuperMode::checkLMMiddleLinesOfFreq() entered",DIAGNOSTIC_CGR_LOG)

	rcode rval = rcode::ok;

	long lm_count = (long)(_lm_middle_lines_of_frequency.size());

	if( lm_count > 0 )
	{
		for( long lm = lm_count-1 ; lm >= 0; lm -- )
		{
			std::vector<CDSDBRSupermodeMapLine>::iterator i_middle = _lm_middle_lines_of_frequency.begin();
			i_middle += lm;

			// check each lm middle line is within one longitudinal mode
			for( long i = 0; i < (long)(i_middle->_points.size()) - 1 ; i++ )
			{
				long rowA = 0;
				long colA = 0;
				long rowB = 0;
				long colB = 0;

				if( _Im_ramp )
				{
					rowA = i_middle->_points[i].col;
					colA = i_middle->_points[i].row;
					rowB = i_middle->_points[i+1].col;
					colB = i_middle->_points[i+1].row;
				}
				else
				{
					rowA = i_middle->_points[i].row;
					colA = i_middle->_points[i].col;
					rowB = i_middle->_points[i+1].row;
					colB = i_middle->_points[i+1].col;
				}

				double PrA_fwd = *(_map_forward_power_ratio_median_filtered.iPt( rowA , colA ));
				double PrB_fwd = *(_map_forward_power_ratio_median_filtered.iPt( rowB , colB ));
				double PrA_rev = *(_map_reverse_power_ratio_median_filtered.iPt( rowA , colA ));
				double PrB_rev = *(_map_reverse_power_ratio_median_filtered.iPt( rowB , colB ));

				double delta_Pr_fwd = fabs( PrA_fwd - PrB_fwd );
				double delta_Pr_rev = fabs( PrA_rev - PrB_rev );

				if( delta_Pr_fwd > CG_REG->get_DSDBR01_SMBD_max_deltaPr_in_lm()
				|| delta_Pr_rev > CG_REG->get_DSDBR01_SMBD_max_deltaPr_in_lm() )
				{
					// middle line is not in a single longitudinal mode
					// clear it!

					(*i_middle).clear();
					_lm_middle_lines_of_frequency_removed++;

					CString log_msg("CDSDBRSuperMode::checkLMMiddleLinesOfFreq() ");
					log_msg.AppendFormat("removed lm%d middle line of frequency, ", lm );
					log_msg.AppendFormat("at i=%d; delta_Pr_fwd=%g, delta_Pr_rev=%g,  max_delta_Pr_in_lm=%g ",
						i,delta_Pr_fwd,delta_Pr_rev,CG_REG->get_DSDBR01_SMBD_max_deltaPr_in_lm() );
					CGR_LOG(log_msg,INFO_CGR_LOG)

					break;
				}
			}
		}

		// need to remove erasing of first and last lines
		// as they are needed to calculate stable area

		//// always remove the upper line above the last middle line, it is not part of a longitudinal mode
		//std::vector<CDSDBRSupermodeMapLine>::iterator i_last_upper = _lm_upper_lines.begin();
		//i_last_upper += lm_count;
		//(*i_last_upper).clear();
		//_lm_upper_lines.erase(i_last_upper);

		//// always remove the first lower line, it is not part of a longitudinal mode
		//std::vector<CDSDBRSupermodeMapLine>::iterator i_first_lower = _lm_lower_lines.begin();
		//(*i_first_lower).clear();
		//_lm_lower_lines.erase(i_first_lower);

		// having removed the first lower line,
		// the indices match upper, lower and middle lines of the same longitudinal mode
		// not any more, had to remove erasing of first and last lines above

		//// Instead of removing lowest lower line,
		//// I'll insert empty middle and upper lines at start
		//// so that indexing starts at 1
		//// and lower line 0 is kept
		//CDSDBRSupermodeMapLine empty_line;
		//_lm_upper_lines.insert(_lm_upper_lines.begin(),empty_line);
		//_lm_middle_lines_of_frequency.insert(_lm_middle_lines_of_frequency.begin(),empty_line);


		//for( long lm = lm_count ; lm > 0; lm-- )
		//{
		//	std::vector<CDSDBRSupermodeMapLine>::iterator i_middle = _lm_middle_lines_of_frequency.begin();
		//	std::vector<CDSDBRSupermodeMapLine>::iterator i_lower = _lm_lower_lines.begin();
		//	std::vector<CDSDBRSupermodeMapLine>::iterator i_upper = _lm_upper_lines.begin();
		//	i_middle += lm;
		//	i_lower += lm;
		//	i_upper += lm;

		//	if( (*i_middle)._points.empty()
		//	|| (*i_lower)._points.empty()
		//	|| (*i_upper)._points.empty() )
		//	{
		//		// erase all 3 lines if any 1 is empty

		//		(*i_middle).clear();
		//		(*i_lower).clear();
		//		(*i_upper).clear();

		//		_lm_middle_lines_of_frequency.erase( i_middle );
		//		_lm_lower_lines.erase( i_lower );
		//		_lm_upper_lines.erase( i_upper );
		//	}
		//}


		//// erase any extra lower lines (that don't have an equivalent middle line)
		//for( long lm = (long)(_lm_lower_lines.size()-1) ; lm > lm_count; lm-- )
		//{
		//	std::vector<CDSDBRSupermodeMapLine>::iterator i_lower = _lm_lower_lines.begin();
		//	i_lower += lm;
		//	(*i_lower).clear();
		//	_lm_lower_lines.erase( i_lower );
		//}

		//// erase any extra upper lines (that don't have an equivalent middle line)
		////except 1 at top needed for stable width
		//for( long lm = (long)(_lm_upper_lines.size()-1) ; lm > lm_count+1; lm-- )
		//{
		//	std::vector<CDSDBRSupermodeMapLine>::iterator i_upper = _lm_upper_lines.begin();
		//	i_upper += lm;
		//	(*i_upper).clear();
		//	_lm_upper_lines.erase( i_upper );
		//}


		////// note how many lines were removed due to flaws found.
		////// decrment _lm_upper_lines_removed and _lm_middle_lines_of_frequency_removed
		////// as these have 1 empty line put into each to get indexing right
		////_lm_upper_lines_removed -= (short)(_lm_upper_lines.size()-1);
		////_lm_lower_lines_removed -= (short)_lm_lower_lines.size();
		////_lm_middle_lines_of_frequency_removed -= (short)(_lm_middle_lines_of_frequency.size()-1);

		//CString log_msg("CDSDBRSuperMode::checkLMMiddleLinesOfFreq() ");
		//log_msg.AppendFormat("have %d middle lines of frequency ", _lm_middle_lines_of_frequency.size()-1 );
		//log_msg += _T("after filtering, shifting and checking");
		//CGR_LOG(log_msg,INFO_CGR_LOG)
	}
	else
	{
		rval = rcode::min_number_of_lines_not_reached;
		CGR_LOG("CDSDBRSuperMode::checkLMMiddleLinesOfFreq() error: min_number_of_lines_not_reached",ERROR_CGR_LOG)
	}


	if( CG_REG->get_DSDBR01_SMBD_write_debug_files() )
	{

		for( long lm = 0 ; lm < (long)_lm_middle_lines_of_frequency.size() ; lm++ )
		{
			// iterate through the boundary lines of each longitudinal mode

			CString middle_line_abs_filepath = _map_forward_power_ratio_median_filtered._abs_filepath;
			char buf[100];
			sprintf(buf,"middleFreq_G_lm%d_",lm);
			middle_line_abs_filepath.Replace( MATRIX_ , _T(buf) );

			_lm_middle_lines_of_frequency[lm].writeRowColToFile(
				middle_line_abs_filepath,
				true );

			middle_line_abs_filepath = _map_forward_power_ratio_median_filtered._abs_filepath;
			sprintf(buf,"middleFreqGcurrents_lm%d_",lm);
			middle_line_abs_filepath.Replace( MATRIX_ , _T(buf) );

			_lm_middle_lines_of_frequency[lm].writeCurrentsToFile(
				middle_line_abs_filepath,
				true );
		}

		for( long lm = 0 ; lm < (long)_lm_lower_lines.size() ; lm++ )
		{
			// Assume _abs_filepath is set from loading data from file
			// or from writing to file after gathering data from laser
			CString abs_filepath = _map_forward_power_ratio_median_filtered._abs_filepath;
			char buf[100];
			sprintf(buf,"lower_G_lm%d_",lm);
			abs_filepath.Replace( MATRIX_ , _T(buf) );

			// write boundary line to file
			_lm_lower_lines[lm].writeRowColToFile( abs_filepath, true );
		}

		for( long lm = 0 ; lm < (long)_lm_upper_lines.size() ; lm++ )
		{
			// Assume _abs_filepath is set from loading data from file
			// or from writing to file after gathering data from laser
			CString abs_filepath = _map_reverse_power_ratio_median_filtered._abs_filepath;
			char buf[100];
			sprintf(buf,"upper_G_lm%d_",lm);
			abs_filepath.Replace( MATRIX_ , _T(buf) );

			// write boundary line to file
			_lm_upper_lines[lm].writeRowColToFile( abs_filepath, true );
		}
	}


	CGR_LOG("CDSDBRSuperMode::checkLMMiddleLinesOfFreq() exiting",DIAGNOSTIC_CGR_LOG)

	return rval;
}




CDSDBRSuperMode::rcode
CDSDBRSuperMode::readBoundariesFromFile()
{
	// first, clear existing lm lines

	CGR_LOG("CDSDBRSuperMode::readBoundariesFromFile() entered",DIAGNOSTIC_CGR_LOG)

	for( int i = 0 ; i < (int)(_lm_upper_lines.size()) ; i++ )
	{
		_lm_upper_lines[i].clear();
	}
	_lm_upper_lines.clear();

	for( int i = 0 ; i < (int)(_lm_lower_lines.size()) ; i++ )
	{
		_lm_lower_lines[i].clear();
	}
	_lm_lower_lines.clear();

	for( int i = 0 ; i < (int)(_lm_middle_lines.size()) ; i++ )
	{
		_lm_middle_lines[i].clear();
	}
	_lm_middle_lines.clear();

	for( int i = 0 ; i < (int)(_lm_filtered_middle_lines.size()) ; i++ )
	{
		_lm_filtered_middle_lines[i].clear();
	}
	_lm_filtered_middle_lines.clear();

	for( int i = 0 ; i < (int)(_lm_middle_lines_of_frequency.size()) ; i++ )
	{
		_lm_middle_lines_of_frequency[i].clear();
	}
	_lm_middle_lines_of_frequency.clear();

	_average_widths.clear();


	rcode rval = rcode::ok;
	std::vector<CString> lower_filepaths;
	std::vector<CString> upper_filepaths;

	CString results_dir = _results_dir_CString;
	if( results_dir.Right(1) != _T("\\") )
		results_dir += _T("\\");

	// build a string with wildcards
	CString lower_strWildcard = results_dir + _T("lower_lm_line_*.txt");

	// start working for files
	CFileFind lower_finder;
	BOOL bWorking = lower_finder.FindFile(lower_strWildcard);

	while (bWorking)
	{
		bWorking = lower_finder.FindNextFile();
		CString str = lower_finder.GetFilePath();
		lower_filepaths.push_back( str );
	}

	lower_finder.Close();

	// build a string with wildcards
	CString upper_strWildcard = results_dir + _T("upper_lm_line_*.txt");

	// start working for files
	CFileFind upper_finder;
	bWorking = upper_finder.FindFile(upper_strWildcard);

	while (bWorking)
	{
		bWorking = upper_finder.FindNextFile();
		CString str = upper_finder.GetFilePath();
		upper_filepaths.push_back( str );
	}

	upper_finder.Close();

	short lm_count = (short)(lower_filepaths.size());

	if( lm_count > 0
	 && lm_count == (short)(upper_filepaths.size()) )
	{
		for(short i = 0; i < lm_count ; i++ )
		{
			CDSDBRSupermodeMapLine lower_line;
			CDSDBRSupermodeMapLine upper_line;

			CDSDBRSupermodeMapLine::rcode read_lowerline_rcode;
			read_lowerline_rcode = lower_line.readRowColFromFile(lower_filepaths[i]);

			CDSDBRSupermodeMapLine::rcode read_upperline_rcode;
			read_upperline_rcode = upper_line.readRowColFromFile(upper_filepaths[i]);

			if( read_lowerline_rcode == CDSDBRSupermodeMapLine::rcode::ok 
			 && read_upperline_rcode == CDSDBRSupermodeMapLine::rcode::ok )
			{
				_lm_lower_lines.push_back(lower_line);
				_lm_upper_lines.push_back(upper_line);
			}
			else
			{
				CString log_err_msg("CDSDBRSuperMode::readBoundariesFromFile() Error reading ");
				if( read_lowerline_rcode == CDSDBRSupermodeMapLine::rcode::ok )
					log_err_msg += lower_filepaths[i];
				else
					log_err_msg += upper_filepaths[i];

				CGR_LOG( log_err_msg, ERROR_CGR_LOG )
			}
		}
	}

	if( _lm_lower_lines.size() > 0
	 && _lm_lower_lines.size() == _lm_upper_lines.size() )
	{
		CString log_msg("CDSDBRSuperMode::readBoundariesFromFile() read in ");
		log_msg.AppendFormat("%d upper amd lower lines", _lm_lower_lines.size() );
		CGR_LOG(log_msg,INFO_CGR_LOG)
	}
	else
	{
		rval = rcode::file_read_error;
		CGR_LOG("CDSDBRSuperMode::readBoundariesFromFile() file_read_error",ERROR_CGR_LOG)
	}

	CGR_LOG("CDSDBRSuperMode::readBoundariesFromFile() exiting",DIAGNOSTIC_CGR_LOG)

	return rval;
}


CDSDBRSuperMode::rcode
CDSDBRSuperMode::writeScreeningResultsToFile()
{
	rcode rval = rcode::ok;

	char sm_number_str[10];
	sprintf( sm_number_str, "%d", _sm_number );

	CString lines_abs_filepath = _results_dir_CString;
	if(lines_abs_filepath.Right(1) != _T("\\") )
		lines_abs_filepath += _T("\\");
	lines_abs_filepath += ("lines_");
	lines_abs_filepath += LASER_TYPE_ID_DSDBR01_CSTRING;
	lines_abs_filepath += USCORE;
	lines_abs_filepath += _laser_id_CString;
	lines_abs_filepath += USCORE;
	lines_abs_filepath += _date_time_stamp_CString;
	lines_abs_filepath += USCORE;
	lines_abs_filepath += CString("SM");
	lines_abs_filepath += CString( sm_number_str );
	lines_abs_filepath += CSV;

	// write 1 file containing row/col coordinates of
	// upper boundary line, lower boundary line and middle line
	// of all supermodes found
	rval = writeLMLinesToFile(
		lines_abs_filepath,
		true ); //CG_REG->get_DSDBR01_OMDC_overwrite_existing_files() );


	for( long lm = 0 ; lm < (long)_lm_middle_lines_of_frequency.size() ; lm++ )
	{
		CString middle_line_abs_filepath = _results_dir_CString;
		if(middle_line_abs_filepath.Right(1) != _T("\\") )
			middle_line_abs_filepath += _T("\\");

		char lm_number_str[10];
		sprintf( lm_number_str, "%d", lm );

		middle_line_abs_filepath += ("ImiddleFreq_LM");
		middle_line_abs_filepath += CString( lm_number_str );
		middle_line_abs_filepath += USCORE;
		middle_line_abs_filepath += LASER_TYPE_ID_DSDBR01_CSTRING;
		middle_line_abs_filepath += USCORE;
		middle_line_abs_filepath += _laser_id_CString;
		middle_line_abs_filepath += USCORE;
		middle_line_abs_filepath += _date_time_stamp_CString;
		middle_line_abs_filepath += USCORE;
		middle_line_abs_filepath += CString("SM");
		middle_line_abs_filepath += CString( sm_number_str );
		middle_line_abs_filepath += CSV;

		_lm_middle_lines_of_frequency[lm].writeCurrentsToFile(
			middle_line_abs_filepath,
			true );
	}



	if( CG_REG->get_DSDBR01_SMBD_write_middle_of_lower_lines_to_file() )
	{
		std::vector<CDSDBRSupermodeMapLine> middle_of_lower_lines;

		rval = findMiddleLines(
			_lm_lower_lines,
			middle_of_lower_lines );

		if( rval == rcode::ok )
		{
			lines_abs_filepath = _results_dir_CString;
			if(lines_abs_filepath.Right(1) != _T("\\") )
				lines_abs_filepath += _T("\\");
			lines_abs_filepath += ("middleOfLowerLines_");
			lines_abs_filepath += LASER_TYPE_ID_DSDBR01_CSTRING;
			lines_abs_filepath += USCORE;
			lines_abs_filepath += _laser_id_CString;
			lines_abs_filepath += USCORE;
			lines_abs_filepath += _date_time_stamp_CString;
			lines_abs_filepath += USCORE;
			lines_abs_filepath += CString("SM");
			lines_abs_filepath += CString( sm_number_str );
			lines_abs_filepath += CSV;

			writeLinesToFile(
				lines_abs_filepath,
				CString("middle of lower lines"),
				middle_of_lower_lines,
				true );

			for( long lm = 0 ; lm < (long)middle_of_lower_lines.size() ; lm++ )
			{
				CString middle_line_abs_filepath = _results_dir_CString;
				if(middle_line_abs_filepath.Right(1) != _T("\\") )
					middle_line_abs_filepath += _T("\\");

				char lm_number_str[10];
				sprintf( lm_number_str, "%d", lm );

				middle_line_abs_filepath += ("ImiddleLower_LM");
				middle_line_abs_filepath += CString( lm_number_str );
				middle_line_abs_filepath += USCORE;
				middle_line_abs_filepath += LASER_TYPE_ID_DSDBR01_CSTRING;
				middle_line_abs_filepath += USCORE;
				middle_line_abs_filepath += _laser_id_CString;
				middle_line_abs_filepath += USCORE;
				middle_line_abs_filepath += _date_time_stamp_CString;
				middle_line_abs_filepath += USCORE;
				middle_line_abs_filepath += CString("SM");
				middle_line_abs_filepath += CString( sm_number_str );
				middle_line_abs_filepath += CSV;

				middle_of_lower_lines[lm].writeCurrentsToFile(
					middle_line_abs_filepath,
					true );
			}

		}
	}

	if( CG_REG->get_DSDBR01_SMBD_write_middle_of_upper_lines_to_file() )
	{
		std::vector<CDSDBRSupermodeMapLine> middle_of_upper_lines;

		rval = findMiddleLines(
			_lm_upper_lines,
			middle_of_upper_lines );

		if( rval == rcode::ok )
		{
			lines_abs_filepath = _results_dir_CString;
			if(lines_abs_filepath.Right(1) != _T("\\") )
				lines_abs_filepath += _T("\\");
			lines_abs_filepath += ("middleOfUpperLines_");
			lines_abs_filepath += LASER_TYPE_ID_DSDBR01_CSTRING;
			lines_abs_filepath += USCORE;
			lines_abs_filepath += _laser_id_CString;
			lines_abs_filepath += USCORE;
			lines_abs_filepath += _date_time_stamp_CString;
			lines_abs_filepath += USCORE;
			lines_abs_filepath += CString("SM");
			lines_abs_filepath += CString( sm_number_str );
			lines_abs_filepath += CSV;

			writeLinesToFile(
				lines_abs_filepath,
				CString("middle of upper lines"),
				middle_of_upper_lines,
				true );

			for( long lm = 0 ; lm < (long)middle_of_upper_lines.size() ; lm++ )
			{
				CString middle_line_abs_filepath = _results_dir_CString;
				if(middle_line_abs_filepath.Right(1) != _T("\\") )
					middle_line_abs_filepath += _T("\\");

				char lm_number_str[10];
				sprintf( lm_number_str, "%d", lm );

				middle_line_abs_filepath += ("ImiddleUpper_LM");
				middle_line_abs_filepath += CString( lm_number_str );
				middle_line_abs_filepath += USCORE;
				middle_line_abs_filepath += LASER_TYPE_ID_DSDBR01_CSTRING;
				middle_line_abs_filepath += USCORE;
				middle_line_abs_filepath += _laser_id_CString;
				middle_line_abs_filepath += USCORE;
				middle_line_abs_filepath += _date_time_stamp_CString;
				middle_line_abs_filepath += USCORE;
				middle_line_abs_filepath += CString("SM");
				middle_line_abs_filepath += CString( sm_number_str );
				middle_line_abs_filepath += CSV;

				middle_of_upper_lines[lm].writeCurrentsToFile(
					middle_line_abs_filepath,
					true );
			}
		}
	}


	if( CG_REG->get_DSDBR01_SMBD_write_boundaries_to_file() )
	{

		for( short lm_number = 0; lm_number < (short)(_lm_lower_lines.size()) ; lm_number++ )
		{
			char lm_number_str[10];
			sprintf( lm_number_str, "%d", lm_number );

			CString lm_lower_line_abs_filepath = _results_dir_CString;
			if(lm_lower_line_abs_filepath.Right(1) != _T("\\") )
				lm_lower_line_abs_filepath += _T("\\");
			lm_lower_line_abs_filepath += CString("lower_lm_line_");
			lm_lower_line_abs_filepath += CString( lm_number_str );
			lm_lower_line_abs_filepath += CString( ".txt" );

			_lm_lower_lines[lm_number].writeRowColToFile(
				lm_lower_line_abs_filepath,
				CG_REG->get_DSDBR01_SMDC_overwrite_existing_files() );
		}

		for( short lm_number = 0; lm_number < (short)(_lm_upper_lines.size()) ; lm_number++ )
		{
			char lm_number_str[10];
			sprintf( lm_number_str, "%d", lm_number );

			CString lm_upper_line_abs_filepath = _results_dir_CString;
			if(lm_upper_line_abs_filepath.Right(1) != _T("\\") )
				lm_upper_line_abs_filepath += _T("\\");
			lm_upper_line_abs_filepath += CString("upper_lm_line_");
			lm_upper_line_abs_filepath += CString( lm_number_str );
			lm_upper_line_abs_filepath += CString( ".txt" );

			_lm_upper_lines[lm_number].writeRowColToFile(
				lm_upper_line_abs_filepath,
				CG_REG->get_DSDBR01_SMDC_overwrite_existing_files() );
		}
	}

	return rval;
}



CDSDBRSuperMode::rcode
CDSDBRSuperMode::writeLMLinesToFile(
	CString abs_filepath,
	bool overwrite )
{
	rcode rval = rcode::ok;
    long row_count = 0;
    long col_count = 0;


	if( _lm_upper_lines.empty()
	 || _lm_lower_lines.empty()
	 || _lm_middle_lines_of_frequency.empty() )
	{
		rval = rcode::no_data;
		CGR_LOG("CDSDBRSuperMode::writeLMLinesToFile() no_data", ERROR_CGR_LOG)
	}
	else
	{

		CFileStatus status;

		// Check if file already exists and overwrite not specified

		if( CFile::GetStatus( abs_filepath, status ) && overwrite == false )
		{
			rval = rcode::file_already_exists;
			CGR_LOG("CDSDBRSuperMode::writeLMLinesToFile() file_already_exists", ERROR_CGR_LOG)
		}
		else
		{

			// Ensure the directory exists
			int index_of_last_dirslash = abs_filepath.ReverseFind('\\');
			if( index_of_last_dirslash == -1 )
			{
				// '\\' not found => not an absolute path
				rval = rcode::could_not_open_file;
				CGR_LOG("CDSDBRSuperMode::writeLMLinesToFile() could_not_open_file", ERROR_CGR_LOG)
			}
			else
			{
				CString dir_name = abs_filepath;
				dir_name.Truncate(index_of_last_dirslash);

				int i = 0;
				CString token = dir_name.Tokenize( "\\/", i );
				CString new_dir = "";
				while ( token != "" )
				{
					if( new_dir.GetLength() == 0 )
					{
						new_dir = token;
					}
					else
					{
						new_dir = new_dir + "\\" + token;
					}
					CFileStatus status;
					//printf("\n %s  = %d", new_dir, CFile::GetStatus( new_dir, status ) );
					if( new_dir.GetLength() > 2 // skip drive name
					&& !CFile::GetStatus( new_dir, status ) )
					{ // directory does not exists => make it
						_mkdir( new_dir );
					}
					else
					{ // directory exists
					}
					//printf("\n %s  = %d", new_dir, CFile::GetStatus( new_dir, status ) );
		            
					token = dir_name.Tokenize( "\\/", i );
				}

				try
				{
					std::ofstream file_stream( abs_filepath );

					if( file_stream )
					{
						// First write header row
						file_stream << "LM#";
						file_stream << COMMA_CHAR;
						file_stream << "LineType";
						file_stream << COMMA_CHAR;
						file_stream << "Row";
						file_stream << COMMA_CHAR;
						file_stream << "Col";
						file_stream << std::endl;

						for( short lm = 0; lm < (short)(_lm_middle_lines_of_frequency.size()) ; lm++ )
						{
							std::vector<CDSDBRSupermodeMapLine::point_type> *p_freq_middle_points = &(_lm_middle_lines_of_frequency[lm]._points);

							if( p_freq_middle_points->empty() )
							{
								CString log_err_msg("CDSDBRSuperMode::writeLMLinesToFile(): ");
								log_err_msg.AppendFormat("no middle line data for lm%d",lm);
								CGR_LOG(log_err_msg, ERROR_CGR_LOG)
							}
							else
							{
								for( long i = 0 ; i < (long)(p_freq_middle_points->size()) ; i++ )
								{
									file_stream << lm;
									file_stream << COMMA_CHAR;
									file_stream << "FrequencyMiddle";
									file_stream << COMMA_CHAR;
									file_stream << ((*p_freq_middle_points)[i]).row;
									file_stream << COMMA_CHAR;
									file_stream << ((*p_freq_middle_points)[i]).col;
									file_stream << std::endl;
								}
							}
						}

						for( short lm = 0; lm < (short)(_lm_upper_lines.size()) ; lm++ )
						{
							std::vector<CDSDBRSupermodeMapLine::point_type> *p_lower_points = &(_lm_upper_lines[lm]._points);

							if( p_lower_points->empty() )
							{
								rval = rcode::no_data;
								CString log_err_msg("CDSDBRSuperMode::writeLMLinesToFile(): ");
								log_err_msg.AppendFormat("no lower line data for lm%d",lm);
								CGR_LOG(log_err_msg, ERROR_CGR_LOG)
							}
							else
							{
								for( long i = 0 ; i < (long)(p_lower_points->size()) ; i++ )
								{
									file_stream << lm;
									file_stream << COMMA_CHAR;
									file_stream << "Lower";
									file_stream << COMMA_CHAR;
									file_stream << ((*p_lower_points)[i]).row;
									file_stream << COMMA_CHAR;
									file_stream << ((*p_lower_points)[i]).col;
									file_stream << std::endl;
								}
							}
						}

						for( short lm = 0; lm < (short)(_lm_lower_lines.size()) ; lm++ )
						{

							std::vector<CDSDBRSupermodeMapLine::point_type> *p_upper_points = &(_lm_lower_lines[lm]._points);

							if( p_upper_points->empty() )
							{
								rval = rcode::no_data;
								CString log_err_msg("CDSDBRSuperMode::writeLMLinesToFile(): ");
								log_err_msg.AppendFormat("no upper line data for lm%d",lm-1);
								CGR_LOG(log_err_msg, ERROR_CGR_LOG)
							}
							else
							{
								for( long i = 0 ; i < (long)(p_upper_points->size()) ; i++ )
								{
									file_stream << lm-1;
									file_stream << COMMA_CHAR;
									file_stream << "Upper";
									file_stream << COMMA_CHAR;
									file_stream << ((*p_upper_points)[i]).row;
									file_stream << COMMA_CHAR;
									file_stream << ((*p_upper_points)[i]).col;
									file_stream << std::endl;
								}
							}
						}

					}
					else
					{
						rval = rcode::could_not_open_file;
						CGR_LOG("CDSDBRSuperMode::writeLMLinesToFile() could not open file stream", ERROR_CGR_LOG)
					}
				}
				catch(...)
				{
					rval = rcode::could_not_open_file;
					CGR_LOG("CDSDBRSuperMode::writeLMLinesToFile() file stream threw exception", ERROR_CGR_LOG)
				}
			}
		}
	}

	return rval;
}


CDSDBRSuperMode::rcode
CDSDBRSuperMode::writeLinesToFile(
	CString abs_filepath,
	CString col2_line_type,
	std::vector<CDSDBRSupermodeMapLine> &lines,
	bool overwrite )
{
	rcode rval = rcode::ok;
    long row_count = 0;
    long col_count = 0;


	if( lines.empty() )
	{
		rval = rcode::no_data;
		CGR_LOG("CDSDBRSuperMode::writeLinesToFile() no_data", ERROR_CGR_LOG)
	}
	else
	{

		CFileStatus status;

		// Check if file already exists and overwrite not specified

		if( CFile::GetStatus( abs_filepath, status ) && overwrite == false )
		{
			rval = rcode::file_already_exists;
			CGR_LOG("CDSDBRSuperMode::writeLinesToFile() file_already_exists", ERROR_CGR_LOG)
		}
		else
		{

			// Ensure the directory exists
			int index_of_last_dirslash = abs_filepath.ReverseFind('\\');
			if( index_of_last_dirslash == -1 )
			{
				// '\\' not found => not an absolute path
				rval = rcode::could_not_open_file;
				CGR_LOG("CDSDBRSuperMode::writeLinesToFile() could_not_open_file", ERROR_CGR_LOG)
			}
			else
			{
				CString dir_name = abs_filepath;
				dir_name.Truncate(index_of_last_dirslash);

				int i = 0;
				CString token = dir_name.Tokenize( "\\/", i );
				CString new_dir = "";
				while ( token != "" )
				{
					if( new_dir.GetLength() == 0 )
					{
						new_dir = token;
					}
					else
					{
						new_dir = new_dir + "\\" + token;
					}
					CFileStatus status;
					//printf("\n %s  = %d", new_dir, CFile::GetStatus( new_dir, status ) );
					if( new_dir.GetLength() > 2 // skip drive name
					&& !CFile::GetStatus( new_dir, status ) )
					{ // directory does not exists => make it
						_mkdir( new_dir );
					}
					else
					{ // directory exists
					}
					//printf("\n %s  = %d", new_dir, CFile::GetStatus( new_dir, status ) );
		            
					token = dir_name.Tokenize( "\\/", i );
				}

				try
				{
					std::ofstream file_stream( abs_filepath );

					if( file_stream )
					{
						// First write header row
						file_stream << "LM#";
						file_stream << COMMA_CHAR;
						file_stream << "LineType";
						file_stream << COMMA_CHAR;
						file_stream << "Row";
						file_stream << COMMA_CHAR;
						file_stream << "Col";
						file_stream << std::endl;

						for( short lm = 0; lm < (short)(lines.size()) ; lm++ )
						{

							std::vector<CDSDBRSupermodeMapLine::point_type> *p_points = &(lines[lm]._points);

							if( p_points->empty() )
							{
								rval = rcode::no_data;
								CString log_err_msg("CDSDBRSuperMode::writeLinesToFile(): ");
								log_err_msg.AppendFormat("no line data for lm%d",lm-1);
								CGR_LOG(log_err_msg, ERROR_CGR_LOG)
							}
							else
							{
								for( long i = 0 ; i < (long)(p_points->size()) ; i++ )
								{
									file_stream << lm;
									file_stream << COMMA_CHAR;
									file_stream << col2_line_type.GetBuffer();
									file_stream << COMMA_CHAR;
									file_stream << ((*p_points)[i]).row;
									file_stream << COMMA_CHAR;
									file_stream << ((*p_points)[i]).col;
									file_stream << std::endl;
								}
							}
						}

					}
					else
					{
						rval = rcode::could_not_open_file;
						CGR_LOG("CDSDBRSuperMode::writeLinesToFile() could not open file stream", ERROR_CGR_LOG)
					}
				}
				catch(...)
				{
					rval = rcode::could_not_open_file;
					CGR_LOG("CDSDBRSuperMode::writeLinesToFile() file stream threw exception", ERROR_CGR_LOG)
				}
			}
		}
	}

	return rval;
}



//
//////////////////////////////////////////////////////////////////
///
//	Quantative Analysis
//
CDSDBRSuperMode::rcode
CDSDBRSuperMode::
runModeAnalysis()
{
	rcode rval = rcode::ok;

	loadModeAnalysisBoundaryLines();

	_modeAnalysis.runAnalysis();

	_qaAnalysisComplete = true;

	return rval;
}

void
CDSDBRSuperMode::
loadModeAnalysisBoundaryLines()
{
	ModeBoundaryLine upperModeBoundaryLine(_upper_line.getCols(), _upper_line.getRows());
	ModeBoundaryLine lowerModeBoundaryLine(_lower_line.getCols(), _lower_line.getRows());

	_modeAnalysis.loadBoundaryLines(_sm_number, _xAxisLength, _yAxisLength, upperModeBoundaryLine, lowerModeBoundaryLine);

	getOMQAThresholds();
	
	_qaThresholds.setThresholdsToDefault();

	_qaThresholds._slopeWindow			= _omqaSlopeWindow;
	_qaThresholds._modalDistortionMinX = _omqaModalDistortionMinX;
	_qaThresholds._modalDistortionMaxX = _omqaModalDistortionMaxX;
	_qaThresholds._modalDistortionMinY = _omqaModalDistortionMinY;
	//GDM 31/10/06 flag that this is the OM qa to the analysis sub
	_qaThresholds._SMMapQA = 0;
	_modeAnalysis.setQAThresholds(&_qaThresholds);

}

void
CDSDBRSuperMode::
getOMQAThresholds()
{
	_omqaSlopeWindow			= CG_REG->get_DSDBR01_OMQA_slope_window_size();
	_omqaModalDistortionMinX	= CG_REG->get_DSDBR01_OMQA_modal_distortion_min_x();
	_omqaModalDistortionMaxX	= CG_REG->get_DSDBR01_OMQA_modal_distortion_max_x();
	_omqaModalDistortionMinY	= CG_REG->get_DSDBR01_OMQA_modal_distortion_min_y();
}

void
CDSDBRSuperMode::
getMiddleLineRMSValue(double& rmsValue)
{
	rmsValue = 0;

	/////////////////////////////////////////////////////////////////////
	/// Use VectorAnalysis to get RMS value of this supermodes middle line
	//
	if(_middleLineRMSValue == 0)
	{
		if(_filtered_middle_line._total_length > 0)
		{
			vector<double> middleLineXPoints;
			vector<double> middleLineYPoints;

			middleLineXPoints = _filtered_middle_line.getCols();
			middleLineYPoints = _filtered_middle_line.getRows();

			double	linearFitLineSlope = 0;
			double	linearFitLineIntercept = 0;

			// find linear fit line
			VectorAnalysis::linearFit(middleLineXPoints, 
									middleLineYPoints,
									linearFitLineSlope,
									linearFitLineIntercept);

			_middleLineSlope	= linearFitLineSlope;

			// find distances from points to linear fit line
			vector<double> distances;
			VectorAnalysis::distancesFromPointsToLine(middleLineXPoints,
													middleLineYPoints,
													linearFitLineSlope,
													linearFitLineIntercept,
													distances);

			// square each distance
			for(int i = 0 ; i < (int)distances.size() ; i ++ )
			{
				distances[i] = distances[i]*distances[i];
			}

			// get mean of squared distances
			double meanSqDistance = 0;
			meanSqDistance = VectorAnalysis::meanOfValues(distances);

			// get root of mean of squared distances
			_middleLineRMSValue =  sqrt( meanSqDistance );			
		}
	}

	rmsValue = _middleLineRMSValue;
}

void
CDSDBRSuperMode::
getMiddleLineSlope(double& slope)
{
	slope = 0;

	if(_middleLineSlope == 0)
	{
			vector<double> middleLineXPoints;
			vector<double> middleLineYPoints;

			middleLineXPoints = _filtered_middle_line.getCols();
			middleLineYPoints = _filtered_middle_line.getRows();

			double	linearFitLineSlope = 0;
			double	linearFitLineIntercept = 0;

			// find linear fit line
			VectorAnalysis::linearFit(middleLineXPoints, 
									middleLineYPoints,
									linearFitLineSlope,
									linearFitLineIntercept);

			_middleLineSlope = linearFitLineSlope;
	}

	slope = _middleLineSlope;
}

short
CDSDBRSuperMode::
numLMs()
{
	short numLMs = (short)(_lm_upper_lines.size());

	return numLMs;
}
//
///
//////////////////////////////////////////////////////////////////
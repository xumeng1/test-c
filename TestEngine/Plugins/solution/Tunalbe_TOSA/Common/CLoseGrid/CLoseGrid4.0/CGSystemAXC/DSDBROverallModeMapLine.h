// Copyright (c) 2004 Tsunami Photonics Ltd. All Rights Reserved
//
// FileName    : DSDBROverallModeMapLine.h
// Description : Declaration of CDSDBROverallModeMapLine class
//               Represents a set of point on DSDBROverallModeMap
//               eg. Supermode boundaries or supermode middle lines
//
// Rev#  | Date          | Author               | Description of change
// --------------------------------------------------------------
// 0.1   | 10 Aug 2004   | Frank D'Arcy         | Initial Placeholder
// 0.2   | 10 Aug 2004   | Frank D'Arcy         | Initial Draft
//

#pragma once

#include "defaults.h"
#include <vector>

class CDSDBROverallModeMapLine {

public:

	typedef enum rcode
	{
		ok = 0,
		file_does_not_exist,
		file_already_exists,
		could_not_open_file,
		file_format_invalid,
		//files_not_matching_sizes,
		//corrupt_map_not_written_to_file,
		//existing_file_not_overwritten,
		//maps_not_loaded,
		//outside_map_dimensions,
		//next_pt_not_found,
		no_data,
		invalid_front_pair_number,
		submap_indexing_error
		//no_supermodes_found
	};

	typedef struct point_type
	{
		long row;
		long col;
		short front_pair_number;
		double I_rear;
		double I_front_1;
		double I_front_2;
		double I_front_3;
		double I_front_4;
		double I_front_5;
		double I_front_6;
		double I_front_7;
		double I_front_8;
	};

	// constructor
	CDSDBROverallModeMapLine();

	// destructor
    ~CDSDBROverallModeMapLine();

	// clear all contents
	void clear();

	rcode getFrontPairNumbers(
		std::vector<short> &front_pair_numbers );

	rcode getCurrentsInOneFrontPair(
		short front_pair_number,
		double &constant_front_current,
		std::vector<double> &non_constant_front_current,
		std::vector<double> &rear_current );

	rcode updateSubmapTracking();

	double getConstantIf( short pair_num, long sub_index );

	double getNonConstantIf( short pair_num, long sub_index );

	rcode writeRowColToFile(
		CString abs_filepath,
		bool overwrite );

	rcode readRowColFromFile(
		CString abs_filepath );

	rcode writeCurrentsToFile(
		CString abs_filepath,
		bool overwrite );

	rcode readCurrentsFromFile(
		CString abs_filepath );

	std::vector<double> getRows();
	std::vector<double> getCols();

	short pair_number( long index );

	long index_in_submap( long index );

	long _total_length;
	std::vector<long> _submap_length;
	std::vector<long> _submap_start_index;
	std::vector<point_type> _points;
	CString _abs_filepath;
};


// Copyright (c) 2004 PXIT. All Rights Reserved
//
// FileName    : DSDBRSuperMode.h
// Description : Declaration of CDSDBRSuperMode class
//
// Rev#  | Date          | Author               | Description of change
// --------------------------------------------------------------
// 0.1   | 10 Aug 2004   | Frank D'Arcy         | Initial Placeholder
// 0.2   | 10 Aug 2004   | Frank D'Arcy         | Initial Draft
// 0.3   | 11 Oct 2004   | Frank D'Arcy         | Adding Boundary Detection
//

#pragma once

#include "DSDBROverallModeMapLine.h"
#include "DSDBRSupermodeMapLine.h"
#include "SequenceRunSettingsList.h"
#include "LaserModeMap.h"
#include "CurrentsVector.h"
#include "ModeAnalysis.h"
#include "SuperModeMapAnalysis.h"
#include "PassFailThresholds.h"

// matched lines stored as <forward_line_number, reverse_line_number>
typedef std::multimap<unsigned long,unsigned long> matchedlines_typedef;


class CDSDBRSuperMode 
{
public:

	typedef enum rcode
	{
		ok = 0,
		no_data,
		invalid_front_pair_number,
		sequencing_error,
		file_does_not_exist,
		file_already_exists,
		could_not_open_file,
		file_format_invalid,
		files_not_matching_sizes,
		file_read_error,
		maps_not_loaded,
		system_error,
		log_file_error,
		set_current,
		registry_error,
		results_disk_full,
		sequencing_step_size_error,
		hardware_error,
		connectivity_error,
		power_meter_not_found,
		wavemeter_not_found,
		tec_not_found,
		power_read_error,
		duplicate_device_name,
		empty_sequence_slots,
		pxit_module_error,
		device_name_not_on_card,
		device_306_firmware_obsolete,
		laser_alignment_error,
		unknown_error,
		min_number_of_lines_not_reached,
		max_number_of_lines_exceeded,
		mismatched_lines,
		module_name_not_found_error,
		screening_failed
	};


	std::pair<short, std::string> get_error_pair( rcode error_in );

	// constructor
	CDSDBRSuperMode(short	superModeNumber,
					int		xAxisLength,
					int		yAxisLength);

	CDSDBRSuperMode();

	// destructor
    ~CDSDBRSuperMode();

	void clear();
	void clearMapsAndLines();

	short getLMCount();

	rcode screen();

	rcode findLMBoundaries();

	rcode applyMedianFilter(
		CLaserModeMap &map_in,
		CLaserModeMap &filtered_map_out,
		unsigned int rank );

	rcode findJumps(
		CLaserModeMap &map_in,
		bool forward_map );			// forward map (true) or reverse map (false)

	void modeBoundaries(
		std::vector<double>::iterator i_Po_start,	// starting point on Po vector
		std::vector<double>::iterator i_I_start,	// starting point on I vector
		unsigned long start_point_index,			// index of first point within a map
		unsigned long index_increment,				// increment on vectors to next point
		unsigned long line_length,					// number of points in the line
		boundary_multimap_typedef &boundary_map,	// map of boundary jumps found
		bool forward_map );							// forward map (true) or reverse map (false)

	rcode findZDPRCBoundaries();

	rcode linkJumps();

	rcode insertExtrapolatedMapBorderPoints();

	rcode insertExtrapolatedMapBorderPointsInMap(
		boundary_multimap_typedef &boundary_points,
		boundary_multimap_typedef &linked_boundary_points);

	void linkJumpsByIndex(
		CLaserModeMap &map_in,
		bool forward_map );							// forward map (true) or reverse map (false)

	unsigned long boundaryPoint(boundary_multimap_typedef::iterator iter_b);

	rcode createLMBoundaryLines();


	void createModeBoundaryLines(
		CLaserModeMap &map_in,
		std::vector<CDSDBRSupermodeMapLine> &lines_in );

	void checkForwardAndReverseLines();

    void associateForwardAndReverseLines();

	void removeUnassociatedLines();

	CDSDBRSupermodeMapLine::point_type convertRowCol2LinePoint( long row, long col );

	rcode writeMatchedLinesToFile(
		CString forward_abs_filepath,
		CString reverse_abs_filepath );

	rcode findLMMiddleLines();

	rcode findMiddleLines(
		std::vector<CDSDBRSupermodeMapLine> &lines,
		std::vector<CDSDBRSupermodeMapLine> &middle_lines );

	rcode filterLMMiddleLines();

	CDSDBRSupermodeMapLine::point_type interpolateLinePoint(
		double row,
		double col );

	rcode calcLMMiddleLinesOfFreq();

	rcode checkLMMiddleLinesOfFreq();

	rcode readBoundariesFromFile();

	////////////////////////////////////////////////////////////////////////////////////////////////////



	rcode collectMapsFromLaser();



	rcode	extractSequenceRunSettings(SequenceRunSettingsList* sequenceRunSettingsList,
									   CString					phaseCurrentsAbsFilePath,
									   bool						hysteresis,
									   int&						numPointsPerCycle,
									   int&						numCycles);


	rcode	extractSequenceSettingsWithMiddleRamp(SequenceRunSettingsList* sequenceRunSettingsList,
											   vector<double>			phaseSequence,
											   bool						hysteresis,
											   int&						numPointsPerCycle,
											   int&						numCycles);

	rcode	extractSequenceSettingsWithPhaseRamp(SequenceRunSettingsList* sequenceRunSettingsList,
											   vector<double>			phaseSequence,
											   bool						hysteresis,
											   int&						numPointsPerCycle,
											   int&						numCycles);

	void	applyHysteresisToSequence(vector<double>& sequence);

	rcode	mapFrontPairNumberToModuleNames(short		frontPairNumber,
											CString&	constantFrontModuleName,
											CString&	sequencedFrontModuleName);

	void	matchToPhaseSequence(vector<double>	phaseSequence,
								vector<double>&	sequence);

	rcode	splitSequence(int						maxSequenceSize,
						vector<double>&				sequence,
						vector<CCurrentsVector>&	splitSequences);

	////////////////////////////////////////////////////////////////////////////////////////////////////

	rcode	runSequences(SequenceRunSettingsList*	runSettingsList,
						 int						numPointsPerCycle,
						 int						numCycles,
						 vector<double>*			total306IIPowerCh1,
						 vector<double>*			total306IIPowerCh2,
						 vector<double>*			total306EECurrentCh1,
						 vector<double>*			total306EECurrentCh2);

	rcode	loadSequences(SequenceRunSettingsList*	runSettingsList,
						int	runNumber);

	rcode	loadSequence(SequenceRunSettings*	runSettings);

	rcode	runSequence(SequenceRunSettings*	runSettings);

	rcode	resetCurrentsAndDisconnect(SequenceRunSettings* runSettings);

	rcode	setNotInSequence(SequenceRunSettings* runSettings);

	rcode	load305Sequence(CString			moduleName,
							vector<double>	sequence,
							int				sourceDelay,
							int				measureDelay);

	rcode	load306Sequence(CString		moduleName,
							int			numPoints,
							int			sourceDelay,
							int			measureDelay);

	rcode	setup305DeviceForRun(PXIT305SequenceSettings* runSettings);
	rcode	setup306DeviceForRun(PXIT306SequenceSettings* runSettings);

	////////////////////////////////////////////////////////////////////////////////////////////////////

	rcode	extractPowerData(bool			hysteresis,
							int				numPointsPerCycle,
							vector<double>	directPower,
							vector<double>	filteredPower,
							vector<double>&	forwardDirectPower,
							vector<double>&	reverseDirectPower,
							vector<double>&	hysteresisDirectPower,
							vector<double>&	forwardFilteredPower,
							vector<double>&	reverseFilteredPower,
							vector<double>&	hysteresisFilteredPower,
							vector<double>&	forwardPowerRatio,
							vector<double>&	reversePowerRatio,
							vector<double>&	hysteresisPowerRatio);

	rcode splitHysteresisPowerArray(vector<double>	&powerVector,
									int				numPointsPerCycle,
									vector<double>	&forwardPower,
									vector<double>	&reversePower);

	////////////////////////////////////////////////////////////////////////////////////////////////////

	

	void setPercentComplete( short percent_complete );
	CRITICAL_SECTION *_p_data_protect;
	short *_p_percent_complete;
	short _sm_number;
	bool _maps_loaded;

	CString _results_dir_CString;
	CString _laser_id_CString;
	CString _date_time_stamp_CString;

	///////////////////////////////////////////

	rcode	writeMapsToFile();

	rcode loadMapsFromFile(
		CString results_dir_CString,
		CString laser_id_CString,
		CString date_time_stamp_CString,
		CString map_ramp_direction_CString );


	rcode writeScreeningResultsToFile();

	rcode writeLMLinesToFile(
		CString abs_filepath,
		bool overwrite );

	rcode writeLinesToFile(
		CString abs_filepath,
		CString col2_line_type,
		std::vector<CDSDBRSupermodeMapLine> &lines,
		bool overwrite );

	////////////////////////////////////////////

	rcode mapHResultToRCode(HRESULT err);



	////////////////////////////////////////////
	////////////////////////////////////////////
	///////////                  ///////////////
	///////////     The Data     ///////////////
	///////////                  ///////////////
	////////////////////////////////////////////
	////////////////////////////////////////////

	CDSDBROverallModeMapLine _upper_line;
	CDSDBROverallModeMapLine _lower_line;
	CDSDBROverallModeMapLine _middle_line;
	CDSDBROverallModeMapLine _filtered_middle_line;
	long _row_of_midpoint_of_filtered_middle_line;


	std::vector<CDSDBRSupermodeMapLine> _lm_upper_lines;
	std::vector<CDSDBRSupermodeMapLine> _lm_lower_lines;
	std::vector<CDSDBRSupermodeMapLine> _lm_middle_lines;
	std::vector<CDSDBRSupermodeMapLine> _lm_filtered_middle_lines;
	std::vector<CDSDBRSupermodeMapLine> _lm_middle_lines_of_frequency;
	matchedlines_typedef _matched_line_numbers;

	// count of the number of supermodes removed because of flaws encountered
	short _lm_upper_lines_removed;
	short _lm_lower_lines_removed;
	short _lm_middle_lines_of_frequency_removed;

	std::vector<double> _average_widths;

	CCurrentsVector _phase_current;

	CCurrentsVector _boundary_detection_ramp;

	// record the Gain and SOA currents at the time of collecting a supermode map
	double _I_gain;
	double _I_soa;

	bool _Im_ramp; // true for middle line ramp, false for phase ramp


	int _sourceDelay;
	int _measureDelay;

	CLaserModeMap _map_forward_direct_power;
	CLaserModeMap _map_reverse_direct_power;
	CLaserModeMap _map_hysteresis_direct_power;

	CLaserModeMap _map_forward_filtered_power;
	CLaserModeMap _map_reverse_filtered_power;
	CLaserModeMap _map_hysteresis_filtered_power;

	CLaserModeMap _map_forward_power_ratio;
	CLaserModeMap _map_reverse_power_ratio;
	CLaserModeMap _map_hysteresis_power_ratio;

	CLaserModeMap _map_forward_power_ratio_median_filtered;
	CLaserModeMap _map_reverse_power_ratio_median_filtered;
	CLaserModeMap _map_hysteresis_power_ratio_median_filtered;

	CLaserModeMap _map_forward_photodiode1_current;
	CLaserModeMap _map_reverse_photodiode1_current;
	CLaserModeMap _map_forward_photodiode2_current;
	CLaserModeMap _map_reverse_photodiode2_current;

	// The above CLaserModeMap objects can contain
	// phase ramped or middle line ramped maps and the members
	// _rows and _cols in those objects change accordingly.
	// However, for the members _rows and _cols in this object
	// _rows always refers to the number of middle line points and
	// _cols always refers to the number of phase current points.
	// As such, the supermode map is always viewed the same way
	// independent of which ramp direction was used to gather the maps.
	long _rows;
	long _cols;

	short numLMs();

	////////////////////////////////////////////
	////////////////////////////////////////////
	////////////////////////////////////////////
	////////////////////////////////////////////
	
	//////////////////////////////////////
	///
	//	Quantative Analysis
	//
	ModeAnalysis	_modeAnalysis;

	rcode	runModeAnalysis();
	void	loadModeAnalysisBoundaryLines();
	void	getOMQAThresholds();

	void	getMiddleLineRMSValue(double& rmsValue);
	void	getMiddleLineSlope(double& slope);

	int _xAxisLength;
	int _yAxisLength;

	QAThresholds	_qaThresholds;

	int		_omqaSlopeWindow;
	double	_omqaModalDistortionMinX;
	double	_omqaModalDistortionMaxX;
	double	_omqaModalDistortionMinY;

	double	_middleLineRMSValue;
	double	_middleLineSlope;

	bool	_qaAnalysisComplete;


	//
	////////////////////////////////////////
	///	Super mode analysis
	//
	SuperModeMapAnalysis	_superModeMapAnalysis;

	//
	//////////////////////////////////////

};


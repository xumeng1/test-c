// Copyright (c) 2006
//
// FileName    : ITUGrid.cpp
// Description : Definition of CITUGrid class
//               Contains currents vector
//               and utility routines
//
// Rev#  | Date          | Author               | Description of change
// --------------------------------------------------------------
// 0.1   | 04 Mar 2006   | Frank D'Arcy         | Initial Draft
//

#include "StdAfx.h"
#include "ITUGrid.h"
#include "defaults.h"
#include "CGRegValues.h"
#include "CGResearchServerInterface.h"
#include <fstream>
#include <direct.h>



CITUGrid::CITUGrid(void)
{
	clear();
}

CITUGrid::~CITUGrid(void)
{
	clear();
}

void
CITUGrid::
clear()
{
	_ITU_frequencies.clear();
	_length = 0;
	_abs_filepath.Empty();
}

bool
CITUGrid::
isEmpty()
{
	bool rval = false;

	if( _ITU_frequencies.size() == 0 )
	{
		rval = true;
	}

	return rval;
}

CITUGrid::rcode
CITUGrid::
setup()
{
	rcode rval = rcode::ok;

	CString log_msg_start("CITUGrid::setup(): ");
	CString log_msg = log_msg_start + CString("entered");
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	clear();

	// Read Registry values and set up ITU Grid

	// first, try file abspath
	rval = readFromFile( CG_REG->get_DSDBR01_ITUGRID_abspath() );

	if( rval == rcode::ok && _length > 0 )
	{
		CGR_LOG("CITUGrid::setup() Successfully read ITU Grid from file",INFO_CGR_LOG)
	}
	else
	{
		// Problem with file approach, try original setup method
		CGR_LOG("CITUGrid::setup() ITU Grid not successfully read from file",INFO_CGR_LOG)
		CGR_LOG("CITUGrid::setup() Setting up ITU Grid from registry entries ",INFO_CGR_LOG)
		clear();
		rval = rcode::ok;

		double itu_op_freq = CG_REG->get_DSDBR01_ITUGRID_centre_freq();
		itu_op_freq -=  CG_REG->get_DSDBR01_ITUGRID_step_freq()*
			(double)((short)(CG_REG->get_DSDBR01_ITUGRID_channel_count()/2));

		for( short itu_channel_num = 0 ;
			itu_channel_num < (short)(CG_REG->get_DSDBR01_ITUGRID_channel_count()) ;
			itu_channel_num++ )
		{
			_ITU_frequencies.push_back(itu_op_freq);
			
			itu_op_freq += CG_REG->get_DSDBR01_ITUGRID_step_freq();
		}
	}

	_length = (long)_ITU_frequencies.size();

	if( _length > 0 )
	{
		rval = rcode::ok;
	}
	else
	{
		CGR_LOG("CITUGrid::setup() Failed to set up ITU Grid",ERROR_CGR_LOG)
		clear();
		rval = rcode::no_itu_grid;
	}

	log_msg = log_msg_start + CString("exiting");
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	return rval;
}

CITUGrid::rcode
CITUGrid::
setupEx(CString abs_filepath)
{
	rcode rval = rcode::ok;

	CString log_msg_start("CITUGrid::setup(): ");
	CString log_msg = log_msg_start + CString("entered");
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	clear();

	// Read Registry values and set up ITU Grid

	// first, try file abspath
	rval = readFromFile(abs_filepath);
	//old call from setup rval = readFromFile( CG_REG->get_DSDBR01_ITUGRID_abspath() );


	if( rval == rcode::ok && _length > 0 )
	{
		CGR_LOG("CITUGrid::setup() Successfully read ITU Grid from file",INFO_CGR_LOG)
	}
	else
	{
		// Problem with file approach, try original setup method
		CGR_LOG("CITUGrid::setup() ITU Grid not successfully read from file",INFO_CGR_LOG)
		CGR_LOG("CITUGrid::setup() Setting up ITU Grid from registry entries ",INFO_CGR_LOG)
		clear();
		rval = rcode::ok;

		double itu_op_freq = CG_REG->get_DSDBR01_ITUGRID_centre_freq();
		itu_op_freq -=  CG_REG->get_DSDBR01_ITUGRID_step_freq()*
			(double)((short)(CG_REG->get_DSDBR01_ITUGRID_channel_count()/2));

		for( short itu_channel_num = 0 ;
			itu_channel_num < (short)(CG_REG->get_DSDBR01_ITUGRID_channel_count()) ;
			itu_channel_num++ )
		{
			_ITU_frequencies.push_back(itu_op_freq);
			
			itu_op_freq += CG_REG->get_DSDBR01_ITUGRID_step_freq();
		}
	}

	_length = (long)_ITU_frequencies.size();

	if( _length > 0 )
	{
		rval = rcode::ok;
	}
	else
	{
		CGR_LOG("CITUGrid::setup() Failed to set up ITU Grid",ERROR_CGR_LOG)
		clear();
		rval = rcode::no_itu_grid;
	}

	log_msg = log_msg_start + CString("exiting");
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	return rval;
}

long
CITUGrid::
channel_count()
{
	return _length;
}

CITUGrid::rcode
CITUGrid::
findITUPtsInRange(
	std::vector<short> &channel_num_of_itu_pts_in_lm,
	std::vector<double> &freq_of_itu_pts_in_lm,
	double min_freq,
	double max_freq )
{
	rcode rval = rcode::ok;

	CString log_msg_start("CITUGrid::findITUPtsInRange(): ");
	CString log_msg = log_msg_start + CString("entered");
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	channel_num_of_itu_pts_in_lm.clear();
	freq_of_itu_pts_in_lm.clear();

	for( short itu_channel_num = 0 ;
		itu_channel_num < (short)(channel_count()) ;
		itu_channel_num++ )
	{
		if( _ITU_frequencies[itu_channel_num] >= min_freq
		&& _ITU_frequencies[itu_channel_num] <= max_freq )
		{
			channel_num_of_itu_pts_in_lm.push_back(itu_channel_num);
			freq_of_itu_pts_in_lm.push_back(_ITU_frequencies[itu_channel_num]);

			log_msg = log_msg_start + CString("Adding ");
			log_msg.AppendFormat("ITU Channel Number = %d, ",itu_channel_num);
			log_msg.AppendFormat("ITU Frequency = %g [GHz], ",_ITU_frequencies[itu_channel_num]);
			log_msg += CString(" to list of ITU points in range");
			CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
		}
	}

	log_msg = log_msg_start + CString("exiting");
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	return rval;
}


CITUGrid::rcode
CITUGrid::
writeToFile( CString abs_filepath, bool overwrite )
{
	rcode rval = rcode::ok;
    long row_count = 0;

	CFileStatus status;

	// Check if file already exists and overwrite not specified

	if( CFile::GetStatus( abs_filepath, status ) && overwrite == false )
	{
		rval = rcode::file_already_exists;
	}
	else
	{

        // Ensure the directory exists
		int index_of_last_dirslash = abs_filepath.ReverseFind('\\');
		if( index_of_last_dirslash == -1 )
		{
			// '\\' not found => not an absolute path
			rval = rcode::could_not_open_file;
		}
		else
		{
			CString dir_name = abs_filepath;
			dir_name.Truncate(index_of_last_dirslash);

			int i = 0;
			CString token = dir_name.Tokenize( "\\/", i );
			CString new_dir = "";
			while ( token != "" )
			{
				if( new_dir.GetLength() == 0 )
				{
					new_dir = token;
				}
				else
				{
					new_dir = new_dir + "\\" + token;
				}
				CFileStatus status;
				//printf("\n %s  = %d", new_dir, CFile::GetStatus( new_dir, status ) );
				if( new_dir.GetLength() > 2 // skip drive name
				&& !CFile::GetStatus( new_dir, status ) )
				{ // directory does not exists => make it
					_mkdir( new_dir );
				}
				else
				{ // directory exists
				}
				//printf("\n %s  = %d", new_dir, CFile::GetStatus( new_dir, status ) );
	            
				token = dir_name.Tokenize( "\\/", i );
			}

			try
			{
				std::ofstream file_stream( abs_filepath );

				if( file_stream )
				{
					row_count = 0;
					while( row_count < (long)(_ITU_frequencies.size()) )
					{
						char buf[NUMBER_STR_LENGTH];
						sprintf( buf, "%.8g", _ITU_frequencies[ row_count ] );
						file_stream << buf ;
						file_stream << std::endl;
		
						row_count++;
					}

					_abs_filepath = abs_filepath;

				}
				else
				{
					rval = rcode::could_not_open_file;
				}
			}
			catch(...)
			{
				rval = rcode::could_not_open_file;
			}
		}
	}

	return rval;
}

CITUGrid::rcode
CITUGrid::
readFromFile( CString abs_filepath )
{
	rcode rval = rcode::ok;

    long row_count = 0;
    long first_line_col_count = 0;
    long col_count = 0;

	clear();
	long count_entries = 0;

	CFileStatus status;

    // Check file

	if( CFile::GetStatus( abs_filepath, status ) )
	{
		// File exists => attempt to read it's contents
		try
		{
			std::ifstream file_stream( abs_filepath );

			if( file_stream )
			{
				row_count = 0;

				while( !file_stream.eof() )
				{
					// File stream is open, read each line
					char map_buffer[MAX_LINE_LENGTH];
					*map_buffer = '\0';
					file_stream.getline( map_buffer, MAX_LINE_LENGTH );

					int map_buffer_lenght = (int)strlen(map_buffer);
					if( map_buffer_lenght > 0 ) // there is something on the line
					{
						// Search for two or more commas together,
						// if found, insert a zero between them
						// a comma at the start or end of a line also
						// means there is a zero to be inserted
						// This is to cope with CSV map files produced
						// from Matlab (Giacinto Busico uses this).

						char temp_buffer[MAX_LINE_LENGTH];
						int temp_chars_i = 0;
						for(int map_chars_i = 0; map_chars_i <= map_buffer_lenght ; map_chars_i++ )
						{
							if(map_chars_i == 0 && map_buffer[map_chars_i] == COMMA_CHAR)
							{ // insert zero at start
								temp_buffer[temp_chars_i] = '0';
								temp_chars_i++;
							}

							temp_buffer[temp_chars_i] = map_buffer[map_chars_i];
							temp_chars_i++;

							if( map_buffer[map_chars_i] == COMMA_CHAR
								&& map_buffer[map_chars_i+1] == COMMA_CHAR )
							{ // insert zero between two commas
								temp_buffer[temp_chars_i] = '0';
								temp_chars_i++;
							}
							else if( map_buffer[map_chars_i] == COMMA_CHAR
								&& map_buffer[map_chars_i+1] == '\0' )
							{ // insert zero at end
								temp_buffer[temp_chars_i] = '0';
								temp_chars_i++;
							}
						}

        				col_count = 0;
						char *fp = strtok(temp_buffer,DELIMITERS);
						while(fp)
						{
							double map_value = atof(fp);

							_ITU_frequencies.push_back(map_value);
							count_entries++;
							
							fp = strtok(NULL,DELIMITERS);
							col_count++;
						}

						if(row_count == 0) first_line_col_count = col_count;

						row_count++;
					}

					if( row_count > 0 && col_count != first_line_col_count )
					{
						rval = rcode::file_format_invalid;
						break;
					}
				}
			}
			else
			{
        		rval = rcode::could_not_open_file;
			}
		}
		catch(...)
		{
			rval = rcode::could_not_open_file;
		}

	}
	else
	{
		rval = rcode::file_does_not_exist;
	}

    if( rval == rcode::ok
	 && ( row_count == 0 || col_count != 1 || _ITU_frequencies.size() != row_count*col_count ) )
	{
		rval = rcode::file_format_invalid;
	}

	if( rval == rcode::ok )
	{
		_abs_filepath = abs_filepath;
		_length = row_count;
	}
	else
	{
		// something is wrong, clear everything!
		clear();
	}

	return rval;
}


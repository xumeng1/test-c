// Copyright (c) 2004 PXIT. All Rights Reserved
//
// FileName    : CLoseGridSystemAXCCtrl.cpp
// Description : Implementation of the CCLoseGridSystemAXCCtrl ActiveX Control class.
//
// Rev#  | Date          | Author               | Description of change
// --------------------------------------------------------------
// 0.1   | 12 July 2004   | Frank D'Arcy         | Initial Draft
//

#include "stdafx.h"
#include "CGSystemAXC.h"
#include "CLoseGridSystemAXCCtrl.h"
#include "CLoseGridSystemAXCPpg.h"
#include "CGResearchServerInterface.h"
#include "CLoseGridErrorCodes.h"
#include "PassFailCollated.h"

#include "CGSystemErrors.h"
#include "versioncontrol.h"
#include "productinfo.h"
#include "CGRegValues.h"

#include "CGResearch_i.c"

#include <sys/types.h>
#include <sys/stat.h>

#include <visa.h>
#include "pxit3XX.h"
#include "pxit305.h"
#include "pxit306.h"
#include "pxitrelay.h"



#ifdef _DEBUG
#define new DEBUG_NEW
#endif


typedef struct
{
	CCLoseGridSystemAXCCtrl* p_axc;
	void* p_other;
} two_ptrs;

UINT screenOverallMapProc( LPVOID pParam );
UINT screenSMMapProc( LPVOID pParam );
UINT characteriseProc( LPVOID pParam );



IMPLEMENT_DYNCREATE(CCLoseGridSystemAXCCtrl, COleControl)



// Message map

BEGIN_MESSAGE_MAP(CCLoseGridSystemAXCCtrl, COleControl)
	ON_OLEVERB(AFX_IDS_VERB_PROPERTIES, OnProperties)
END_MESSAGE_MAP()


// Dispatch map

BEGIN_DISPATCH_MAP(CCLoseGridSystemAXCCtrl, COleControl)
	DISP_FUNCTION_ID(CCLoseGridSystemAXCCtrl, "getVersionInfo", 1, getVersionInfo, VT_EMPTY, VTS_PBSTR VTS_PBSTR VTS_PBSTR VTS_PBSTR VTS_PBSTR VTS_PBSTR VTS_PBSTR VTS_PBSTR VTS_PBSTR )
	DISP_FUNCTION_ID(CCLoseGridSystemAXCCtrl, "startup", 2, startup, VT_EMPTY, VTS_NONE )
	DISP_FUNCTION_ID(CCLoseGridSystemAXCCtrl, "setup", 3, setup, VT_EMPTY, VTS_BSTR VTS_BSTR VTS_PBSTR )
	DISP_FUNCTION_ID(CCLoseGridSystemAXCCtrl, "shutdown", 4, shutdown, VT_EMPTY, VTS_NONE )
	DISP_FUNCTION_ID(CCLoseGridSystemAXCCtrl, "rampdown", 5, rampdown, VT_EMPTY, VTS_NONE )
	DISP_FUNCTION_ID(CCLoseGridSystemAXCCtrl, "getCurrent", 6, getCurrent, VT_EMPTY, VTS_BSTR VTS_PR8 )
	DISP_FUNCTION_ID(CCLoseGridSystemAXCCtrl, "setCurrent", 7, setCurrent, VT_EMPTY, VTS_BSTR VTS_R8 )
	DISP_FUNCTION_ID(CCLoseGridSystemAXCCtrl, "getOutputConnection", 8, getOutputConnection, VT_EMPTY, VTS_BSTR VTS_PI2 )
	DISP_FUNCTION_ID(CCLoseGridSystemAXCCtrl, "setOutputConnection", 9, setOutputConnection, VT_EMPTY, VTS_BSTR VTS_I2 )
	DISP_FUNCTION_ID(CCLoseGridSystemAXCCtrl, "get2DData", 10, get2DData, VT_EMPTY, VTS_BSTR VTS_PVARIANT VTS_I2 VTS_I2 VTS_PBSTR VTS_PBSTR )
	DISP_FUNCTION_ID(CCLoseGridSystemAXCCtrl, "get3DData", 11, get3DData, VT_EMPTY, VTS_BSTR VTS_PVARIANT VTS_BSTR VTS_PVARIANT VTS_I2 VTS_I2 VTS_I2 VTS_PBSTR VTS_PBSTR )
	DISP_FUNCTION_ID(CCLoseGridSystemAXCCtrl, "startLaserScreeningOverallMap", 12, startLaserScreeningOverallMap, VT_EMPTY, VTS_NONE )
	DISP_FUNCTION_ID(CCLoseGridSystemAXCCtrl, "startLaserScreeningSMMap", 13, startLaserScreeningSMMap, VT_EMPTY, VTS_I2 )
	DISP_FUNCTION_ID(CCLoseGridSystemAXCCtrl, "estimateITUOPs", 14, estimateITUOPs, VT_EMPTY, VTS_PBSTR VTS_PBSTR VTS_PI2 )
	DISP_FUNCTION_ID(CCLoseGridSystemAXCCtrl, "startLaserCharacterisation", 15, startLaserCharacterisation, VT_EMPTY, VTS_NONE )
	DISP_FUNCTION_ID(CCLoseGridSystemAXCCtrl, "getPercentComplete", 16, getPercentComplete, VT_EMPTY, VTS_PI2 )
	DISP_FUNCTION_ID(CCLoseGridSystemAXCCtrl, "getState", 17, getState, VT_EMPTY, VTS_PBSTR VTS_PI2 VTS_PI2 VTS_PI2 VTS_PI2 VTS_PI2 )
	DISP_FUNCTION_ID(CCLoseGridSystemAXCCtrl, "getScreeningOverallMapResults", 18, getScreeningOverallMapResults, VT_EMPTY, VTS_PBSTR VTS_PBSTR VTS_PBSTR VTS_PBSTR VTS_PBSTR VTS_PI2 VTS_PI2 VTS_PVARIANT )
	DISP_FUNCTION_ID(CCLoseGridSystemAXCCtrl, "getScreeningSMMapResults", 19, getScreeningSMMapResults, VT_EMPTY, VTS_I2 VTS_PBSTR VTS_PBSTR VTS_PBSTR VTS_PBSTR VTS_PBSTR VTS_PI2 VTS_PI2 )
	DISP_FUNCTION_ID(CCLoseGridSystemAXCCtrl, "getCharacterisationResults", 20, getCharacterisationResults, VT_EMPTY, VTS_PBSTR VTS_PBSTR VTS_PI2 VTS_PI2 )
	DISP_FUNCTION_ID(CCLoseGridSystemAXCCtrl, "collatePassFailResults", 21, collatePassFailResults, VT_EMPTY, VTS_PBSTR VTS_PBSTR VTS_PI2 VTS_PVARIANT VTS_PI2 )
	DISP_FUNCTION_ID(CCLoseGridSystemAXCCtrl, "loadOverallMap", 22, loadOverallMap, VT_EMPTY, VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR )
	DISP_FUNCTION_ID(CCLoseGridSystemAXCCtrl, "loadSMMap", 23, loadSMMap, VT_EMPTY, VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_I2 VTS_BSTR )
	DISP_FUNCTION_ID(CCLoseGridSystemAXCCtrl, "readVoltage", 24, readVoltage, VT_EMPTY, VTS_BSTR VTS_PR8 )
	DISP_FUNCTION_ID(CCLoseGridSystemAXCCtrl, "readFrequency", 25, readFrequency, VT_EMPTY, VTS_PR8 )
	DISP_FUNCTION_ID(CCLoseGridSystemAXCCtrl, "readCoarseFrequency", 26, readCoarseFrequency, VT_EMPTY, VTS_PR8 )
	DISP_FUNCTION_ID(CCLoseGridSystemAXCCtrl, "readPhotodiodeCurrent", 27, readPhotodiodeCurrent, VT_EMPTY, VTS_BSTR VTS_I2 VTS_PR8 )
	DISP_FUNCTION_ID(CCLoseGridSystemAXCCtrl, "readOpticalPower", 28, readOpticalPower, VT_EMPTY, VTS_BSTR VTS_I2 VTS_PR8 )
	DISP_FUNCTION_ID(CCLoseGridSystemAXCCtrl, "readOpticalPowerAtFreq", 29, readOpticalPowerAtFreq, VT_EMPTY, VTS_BSTR VTS_I2 VTS_R8 VTS_PR8 )
	DISP_FUNCTION_ID(CCLoseGridSystemAXCCtrl, "readTemperature", 30, readTemperature, VT_EMPTY, VTS_PR8 )
	DISP_FUNCTION_ID(CCLoseGridSystemAXCCtrl, "getTECTempSetpoint", 31, getTECTempSetpoint, VT_EMPTY, VTS_PR8 )
	DISP_FUNCTION_ID(CCLoseGridSystemAXCCtrl, "setTECTempSetpoint", 32, setTECTempSetpoint, VT_EMPTY, VTS_R8 )
	DISP_FUNCTION_ID(CCLoseGridSystemAXCCtrl, "calcCoarseFreqPolyCoeffs", 33, calcCoarseFreqPolyCoeffs, VT_EMPTY, VTS_I2 VTS_I2 VTS_PVARIANT VTS_PR8 )
	DISP_FUNCTION_ID(CCLoseGridSystemAXCCtrl, "getCoarseFreqPolyCoeffs", 34, getCoarseFreqPolyCoeffs, VT_EMPTY, VTS_PVARIANT )
	DISP_FUNCTION_ID(CCLoseGridSystemAXCCtrl, "setCoarseFreqPolyCoeffs", 35, setCoarseFreqPolyCoeffs, VT_EMPTY, VTS_PVARIANT )
	DISP_FUNCTION_ID(CCLoseGridSystemAXCCtrl, "getCoarseFrequency", 36, getCoarseFrequency, VT_EMPTY, VTS_R8 VTS_PR8 )
	DISP_FUNCTION_ID(CCLoseGridSystemAXCCtrl, "getComplianceVoltage", 37, getComplianceVoltage, VT_EMPTY, VTS_BSTR VTS_PR8 )
	DISP_FUNCTION_ID(CCLoseGridSystemAXCCtrl, "setComplianceVoltage", 38, setComplianceVoltage, VT_EMPTY, VTS_BSTR VTS_R8 )
	DISP_FUNCTION_ID(CCLoseGridSystemAXCCtrl, "rampdownAndDisconnect", 39, rampdownAndDisconnect, VT_EMPTY, VTS_NONE )
	DISP_FUNCTION_ID(CCLoseGridSystemAXCCtrl, "stopAndShutdown", 40, stopAndShutdown, VT_EMPTY, VTS_NONE )
	DISP_FUNCTION_ID(CCLoseGridSystemAXCCtrl, "applyStableOP", 41, applyStableOP, VT_EMPTY, VTS_I2 VTS_I2 VTS_R8 VTS_I2 )
	DISP_FUNCTION_ID(CCLoseGridSystemAXCCtrl, "applyPointOnSMMap", 42, applyPointOnSMMap, VT_EMPTY, VTS_I2 VTS_R8 VTS_R8 VTS_I2 )
	DISP_FUNCTION_ID(CCLoseGridSystemAXCCtrl, "getPointOnSMMap", 43, getPointOnSMMap, VT_EMPTY, VTS_I2 VTS_PR8 VTS_PR8 )
	DISP_FUNCTION_ID(CCLoseGridSystemAXCCtrl, "initWavemeter", 44, initWavemeter, VT_EMPTY, VTS_NONE )
	DISP_FUNCTION_ID(CCLoseGridSystemAXCCtrl, "loadImFromFile", 45, loadImFromFile, VT_EMPTY, VTS_I2 VTS_BSTR )
	DISP_FUNCTION_ID(CCLoseGridSystemAXCCtrl, "loadImiddleFreqFromFile", 46, loadImiddleFreqFromFile, VT_EMPTY, VTS_I2 VTS_I2 VTS_BSTR )
END_DISPATCH_MAP()


// DUAL_SUPPORT_START 
// Note: we add support for DIID__DCGSystemAXC to support typesafe binding
// from VBA.  We add support for IID_IDual_DCGSystemAXC to support our dual
// interface. See CGSystemAXCidl.h for the definition of DIID__DCGSystemAXC and
// IID_IDual_DCGSystemAXC.
//
// DUAL_ERRORINFO_PART indicates we support OLE Automation
// error handling.
//
BEGIN_INTERFACE_MAP(CCLoseGridSystemAXCCtrl, COleControl)
	INTERFACE_PART(CCLoseGridSystemAXCCtrl, DIID__DCGSystemAXC, Dispatch)
	INTERFACE_PART(CCLoseGridSystemAXCCtrl, IID_IDual_DCGSystemAXC, Dual_DCGSystemAXC)
	DUAL_ERRORINFO_PART(CCLoseGridSystemAXCCtrl)
END_INTERFACE_MAP()

// DUAL_SUPPORT_END


// Event map

BEGIN_EVENT_MAP(CCLoseGridSystemAXCCtrl, COleControl)
END_EVENT_MAP()



// Property pages

// TODO: Add more property pages as needed.  Remember to increase the count!
BEGIN_PROPPAGEIDS(CCLoseGridSystemAXCCtrl, 1)
	PROPPAGEID(CCLoseGridSystemAXCPropPage::guid)
END_PROPPAGEIDS(CCLoseGridSystemAXCCtrl)



// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CCLoseGridSystemAXCCtrl, "CGSYSTEMAXC.CLoseGridSystemACtrl.1",
	0xaa902865, 0x1021, 0x4226, 0xb4, 0x60, 0x89, 0xd9, 0xad, 0x98, 0xfa, 0x1)



// Type library ID and version

IMPLEMENT_OLETYPELIB(CCLoseGridSystemAXCCtrl, _tlid, _wVerMajor, _wVerMinor)



// Interface IDs

const IID BASED_CODE IID_DCGSystemAXC =
		{ 0xEC20368, 0xED60, 0x4A51, { 0xB2, 0x93, 0xB5, 0xCE, 0x5, 0x77, 0x7E, 0x6D } };
const IID BASED_CODE IID_DCGSystemAXCEvents =
		{ 0xAFB2F1C1, 0xE8AD, 0x409C, { 0x97, 0xBF, 0x8F, 0xC7, 0xAD, 0x47, 0xDE, 0x9F } };



// Control type information

static const DWORD BASED_CODE _dwCGSystemAXCOleMisc =
	OLEMISC_INVISIBLEATRUNTIME |
	OLEMISC_SETCLIENTSITEFIRST |
	OLEMISC_INSIDEOUT |
	OLEMISC_CANTLINKINSIDE |
	OLEMISC_RECOMPOSEONRESIZE;

IMPLEMENT_OLECTLTYPE(CCLoseGridSystemAXCCtrl, IDS_CGSYSTEMAXC, _dwCGSystemAXCOleMisc)



// CCLoseGridSystemAXCCtrl::CCLoseGridSystemAXCCtrlFactory::UpdateRegistry -
// Adds or removes system registry entries for CCLoseGridSystemAXCCtrl

BOOL CCLoseGridSystemAXCCtrl::CCLoseGridSystemAXCCtrlFactory::UpdateRegistry(BOOL bRegister)
{
	// TODO: Verify that your control follows apartment-model threading rules.
	// Refer to MFC TechNote 64 for more information.
	// If your control does not conform to the apartment-model rules, then
	// you must modify the code below, changing the 6th parameter from
	// afxRegApartmentThreading to 0.

	if (bRegister)
		return AfxOleRegisterControlClass(
			AfxGetInstanceHandle(),
			m_clsid,
			m_lpszProgID,
			IDS_CGSYSTEMAXC,
			IDB_CGSYSTEMAXC,
			afxRegApartmentThreading,
			_dwCGSystemAXCOleMisc,
			_tlid,
			_wVerMajor,
			_wVerMinor);
	else
		return AfxOleUnregisterClass(m_clsid, m_lpszProgID);
}



// CCLoseGridSystemAXCCtrl::CCLoseGridSystemAXCCtrl - Constructor

CCLoseGridSystemAXCCtrl::CCLoseGridSystemAXCCtrl()
{
	_CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );

	InitializeIIDs(&IID_DCGSystemAXC, &IID_DCGSystemAXCEvents);
	// TODO: Initialize your control's instance data here.


	// instantiate the COM object
	CoInitialize(NULL);
	_p_cgresearch = NULL;

	HRESULT hr = CoCreateInstance(CLSID_CGResearch, NULL, CLSCTX_ALL,
				IID_IDual_DCGResearch, (void **)&_p_cgresearch );

	if (FAILED(hr))
	{
		//do nothing
	}

	InitializeCriticalSection(&_data_protect);

	// clear all data
	clear();

	// set initial state
	setSystemState( state_type::instantiated );

	CGResearchServerInterface::instance()->setPointerToServer( _p_cgresearch );

	_overall_mode_map._p_data_protect = &_data_protect;
	_overall_mode_map._p_percent_complete = &_percent_complete;
	_ops._p_supermodes = &(_overall_mode_map._supermodes);
	_ops._p_data_protect = &_data_protect;
	_ops._p_percent_complete = &_percent_complete;
	_ops._p_ITUOP_count = &_ITUOP_count;
	_ops._p_duff_ITUOP_count = &_duff_ITUOP_count;
	_worker_thread_id = NULL;
}



// CCLoseGridSystemAXCCtrl::~CCLoseGridSystemAXCCtrl - Destructor

CCLoseGridSystemAXCCtrl::~CCLoseGridSystemAXCCtrl()
{
	// TODO: Cleanup your control's instance data here.

	_p_cgresearch->Release();

	DeleteCriticalSection(&_data_protect);
}



// CCLoseGridSystemAXCCtrl::OnDraw - Drawing function

void CCLoseGridSystemAXCCtrl::OnDraw(
			CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid)
{
	CString display_text = CString( CGSYSTEM_PRODUCTNAME )
						 + CString( " " )
						 + CString( CGSYSTEM_RELEASENO )
						 + CString( "\nActiveX Control" );

	pdc->SetTextColor( RGB(200, 0, 0) );
	CRect rcb = rcBounds;
	pdc->DrawText( display_text, rcb, DT_LEFT );
}



// CCLoseGridSystemAXCCtrl::DoPropExchange - Persistence support

void CCLoseGridSystemAXCCtrl::DoPropExchange(CPropExchange* pPX)
{
	ExchangeVersion(pPX, MAKELONG(_wVerMinor, _wVerMajor));
	COleControl::DoPropExchange(pPX);

	// TODO: Call PX_ functions for each persistent custom property.
}



// CCLoseGridSystemAXCCtrl::GetControlFlags -
// Flags to customize MFC's implementation of ActiveX controls.
//
// For information on using these flags, please see MFC technical note
// #nnn, "Optimizing an ActiveX Control".
DWORD CCLoseGridSystemAXCCtrl::GetControlFlags()
{
	DWORD dwFlags = COleControl::GetControlFlags();


	// The control can activate without creating a window.
	// TODO: when writing the control's message handlers, avoid using
	//		the m_hWnd member variable without first checking that its
	//		value is non-NULL.
	dwFlags |= windowlessActivate;
	return dwFlags;
}



// CCLoseGridSystemAXCCtrl::OnResetState - Reset control to default state

void CCLoseGridSystemAXCCtrl::OnResetState()
{
	COleControl::OnResetState();  // Resets defaults found in DoPropExchange

	// TODO: Reset any other control state here.
}


// Get version control info
//
// Unless an exception is thrown, memory for BSTR arguments are allocated
// and it is the calling code's reponsibility to free the memory.
// If an exception is thrown, no memory is allocated
// and no memory is to be freed by the calling code.
//
void 
CCLoseGridSystemAXCCtrl::
getVersionInfo(
	BSTR* p_product_name,
	BSTR* p_release_no,
	BSTR* p_copyright_notice,
	BSTR* p_label,
	BSTR* p_label_info,
	BSTR* p_label_comment,
	BSTR* p_build_config,
	BSTR* p_build_datetime,
	BSTR* p_driver_version_info )
{
	if( p_product_name == NULL
	 || p_release_no == NULL
	 || p_copyright_notice == NULL
	 || p_label == NULL
	 || p_label_info == NULL
	 || p_label_comment == NULL
	 || p_build_config == NULL
	 || p_build_datetime == NULL
	 || p_driver_version_info == NULL )
	{
		// NULL pointer passed in argument
		CString log_err_msg("CCLoseGridSystemAXCCtrl::getVersionInfo() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_NULL_PTR_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_NULL_PTR_ERROR )
	}

	CString product_name_cstring( CGSYSTEM_PRODUCTNAME );
	CString release_no_cstring( CGSYSTEM_RELEASENO );
	CString copyright_notice_cstring( CGSYSTEM_COPYRIGHTNOTICE );

	CString label_cstring;
	CString label_info_cstring;
	CString label_comment_cstring;
	CString build_config_cstring;
	CString driver_version_info_cstring;

	CString build_datetime_cstring(__DATE__);
	build_datetime_cstring += CString(" ") + CString(__TIME__);

	#ifdef NDEBUG
		build_config_cstring = CString( "Release" );
	#else

		#ifdef _DEBUG

			#ifdef HARDWARE_BUILD
				build_config_cstring = CString( "Debug" );
			#else
				build_config_cstring = CString( "SoftwareOnlyDebug" );
			#endif

		#else
			build_config_cstring = CString( "Unknown" );
		#endif

	#endif

	// Label info
	#ifdef LABEL

		label_cstring = LABEL;

		#ifdef LABELSET
			label_info_cstring = LABELSET;
		#endif

		#ifdef LABELCOMMENT
			label_comment_cstring = LABELCOMMENT;
		#endif

	#else // no LABEL

		build_config_cstring	   += CString( " - Uncontrolled Version" );
		label_cstring				= CString( "no label" );
		label_info_cstring			= CString( "no label info" );
		label_comment_cstring		= CString( "no comment" );

	#endif // LABEL

	double PXIT305Version_double = PXIT305Version();
	double PXIT306Version_double = PXIT306Version();
	double PXIT3XXVersion_double = PXIT3XXVersion();
	double PXITRelayVersion_double = PXITRelayVersion();

	driver_version_info_cstring = _T("");
	char numbuf[100];
	sprintf( numbuf, "PXIT305Version = %#.7g\n", PXIT305Version_double );
	driver_version_info_cstring += _T(numbuf);
	sprintf( numbuf, "PXIT306Version = %#.7g\n", PXIT306Version_double );
	driver_version_info_cstring += _T(numbuf);
	sprintf( numbuf, "PXIT3XXVersion = %#.7g\n", PXIT3XXVersion_double );
	driver_version_info_cstring += _T(numbuf);
	sprintf( numbuf, "PXITRelayVersion = %#.7g\n", PXITRelayVersion_double );
	driver_version_info_cstring += _T(numbuf);

	*p_product_name = product_name_cstring.AllocSysString();
	*p_release_no = release_no_cstring.AllocSysString();
	*p_copyright_notice = copyright_notice_cstring.AllocSysString();
	*p_label = label_cstring.AllocSysString();
	*p_label_info = label_info_cstring.AllocSysString();
	*p_label_comment = label_comment_cstring.AllocSysString();
	*p_build_config = build_config_cstring.AllocSysString();
	*p_build_datetime = build_datetime_cstring.AllocSysString();
	*p_driver_version_info = driver_version_info_cstring.AllocSysString();

    return;
}

void 
CCLoseGridSystemAXCCtrl::
startup()
{
	if( getSystemState() != state_type::instantiated )
	{
		// function cannot be called in the current system state
		CString log_err_msg("CCLoseGridSystemAXCCtrl::startup() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR )
	}


	CG_REG_RTYPE cgreg_rval = CG_REG->retrieveAllFromRegistry();

	if( cgreg_rval != CG_REG_RTYPE::ok )
	{
		// save values already in Registry and those missing as defaults
		CG_REG->saveAllToRegistry();
	}


	CCGOperatingPoints::rtype rval_read_cfreq_poly = _ops.loadDefaultCoarseFreqPoly();
	if( rval_read_cfreq_poly != CCGOperatingPoints::rtype::ok )
	{
		// could not load polynomial, use default, not a reason to fail startup
		CString log_err_msg("CCLoseGridSystemAXCCtrl::startup(): ");
		log_err_msg.Append( "could not load coarse frequency polynomial, using default" );
		CGR_LOG(log_err_msg,WARNING_CGR_LOG)
	}


	HRESULT hres = S_OK;

	if( !CG_REG->get_DSDBR01_BASE_stub_hardware_calls() )
	{
		// Check system hardware is okay
		hres = SERVERINTERFACE->startup();
	}
	else
	{
		// just open log file, no HW initialisation
		hres = SERVERINTERFACE->openLogFile();

		Sleep(2000);
	}

	BSTR product_name_BSTR;
	BSTR release_no_BSTR;
	BSTR copyright_notice_BSTR;
	BSTR label_BSTR;
	BSTR label_info_BSTR;
	BSTR label_comment_BSTR;
	BSTR build_configuration_BSTR;
	BSTR build_datetime_BSTR;
	BSTR driver_version_info_BSTR;

	getVersionInfo( &product_name_BSTR,
					&release_no_BSTR,
					&copyright_notice_BSTR,
					&label_BSTR,
					&label_info_BSTR,
					&label_comment_BSTR,
					&build_configuration_BSTR,
					&build_datetime_BSTR,
					&driver_version_info_BSTR);

	CString product_name_CString( product_name_BSTR );
	CString release_no_CString( release_no_BSTR );
	CString copyright_notice_CString( copyright_notice_BSTR );
	CString label_CString( label_BSTR );
	CString label_info_CString( label_info_BSTR );
	CString label_comment_CString( label_comment_BSTR );
	CString build_configuration_CString( build_configuration_BSTR );
	CString build_datetime_CString( build_datetime_BSTR );
	CString driver_version_info_CString( driver_version_info_BSTR );

	// Write version info to logfile
	CGR_LOG( product_name_CString, CRITICAL_CGR_LOG )
	CGR_LOG( release_no_CString, CRITICAL_CGR_LOG )
	CGR_LOG( copyright_notice_CString, CRITICAL_CGR_LOG )
	CGR_LOG( label_CString, CRITICAL_CGR_LOG )
	CGR_LOG( label_info_CString, CRITICAL_CGR_LOG )
	CGR_LOG( label_comment_CString, CRITICAL_CGR_LOG )
	CGR_LOG( build_configuration_CString, CRITICAL_CGR_LOG )
	CGR_LOG( build_datetime_CString, CRITICAL_CGR_LOG )
	CGR_LOG( driver_version_info_CString, CRITICAL_CGR_LOG )

	SysFreeString( product_name_BSTR );
	SysFreeString( release_no_BSTR );
	SysFreeString( copyright_notice_BSTR );
	SysFreeString( label_BSTR );
	SysFreeString( label_info_BSTR );
	SysFreeString( label_comment_BSTR );
	SysFreeString( build_configuration_BSTR );
	SysFreeString( build_datetime_BSTR );
	SysFreeString( driver_version_info_BSTR );

	if( FAILED(hres) )
	{
		handleCGResearchError( _T("CCLoseGridSystemAXCCtrl::startup()"), hres);
	}

	// change state
	setSystemState( state_type::start_up );

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::startup() exiting"),DIAGNOSTIC_CGR_LOG)

	SERVERINTERFACE->closeLogFile();

	return;
}


void 
CCLoseGridSystemAXCCtrl::
setup(
	LPCTSTR laser_type,
	LPCTSTR laser_id,
	BSTR* p_date_time_stamp )
{
	if( getSystemState() != state_type::start_up
	 && getSystemState() != state_type::set_up
	 && getSystemState() != state_type::complete )
	{
		// function cannot be called in the current system state
		CString log_err_msg("CCLoseGridSystemAXCCtrl::setup() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR )
	}

	// Check for NULL pointer
	if( laser_type == NULL
	 || laser_id == NULL
	 || p_date_time_stamp == NULL )
	{
		// if there is any problem encountered
		// change state down to startup
		CString log_err_msg("CCLoseGridSystemAXCCtrl::setup() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_NULL_PTR_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		setSystemState( state_type::start_up );

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_NULL_PTR_ERROR )
	}

	CString laser_type_CString( laser_type );
	CString laser_id_CString( laser_id );

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::setup() entered"),DIAGNOSTIC_CGR_LOG)
	CString log_msg("CCLoseGridSystemAXCCtrl::setup() laser_type = ");
	log_msg += laser_type_CString;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::setup() laser_id = ");
	log_msg += laser_id;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	// test strings are not empty
	if( laser_type_CString.GetLength() <= 0
	 || laser_id_CString.GetLength() <= 0 )
	{
		// if there is any problem encountered
		// change state down to startup
		CString log_err_msg("CCLoseGridSystemAXCCtrl::setup() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_EMPTY_STRING_ARG_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		setSystemState( state_type::start_up );

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_EMPTY_STRING_ARG_ERROR )
	}

	// test laser_type is valid
	if( !validLaserType( laser_type_CString ) )
	{
		// if there is any problem encountered
		// change state down to startup
		CString log_err_msg("CCLoseGridSystemAXCCtrl::setup() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_LASERTYPE_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		setSystemState( state_type::start_up );

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_LASERTYPE_ERROR )
	}

	// test laser_id is valid
	if( !validLaserID( laser_id_CString ) )
	{
		// if there is any problem encountered
		// change state down to startup
		CString log_err_msg("CCLoseGridSystemAXCCtrl::setup() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_LASER_ID_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		setSystemState( state_type::start_up );

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_LASER_ID_ERROR )
	}

	// clear all system data
	clear();


	CString system_date_time_stamp_CString = getSystemDateTimeStamp();

	CString new_date_time_stamp_CString;
	getNewDateTimeStamp( new_date_time_stamp_CString );
	while( new_date_time_stamp_CString == system_date_time_stamp_CString )
	{
		// to ensure all date/time stamps are unique.
		Sleep(100);
		getNewDateTimeStamp( new_date_time_stamp_CString );
	}


	HRESULT hres = S_OK;

	SERVERINTERFACE->setLogFileName(	laser_id_CString,
									laser_type_CString,
									new_date_time_stamp_CString);

	CString err_msg("");

	if( !CG_REG->get_DSDBR01_BASE_stub_hardware_calls() )
	{
		// Check laser is connected to hardware okay
		hres = SERVERINTERFACE->setup(laser_id_CString,
									laser_type_CString,
									new_date_time_stamp_CString,
									err_msg);
	}

	if( FAILED(hres) )
	{
		// if there is any problem encountered
		// change state down to startup
		setSystemState( state_type::start_up );

		CString return_msg(CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_CONNECTIVITY_ERROR));
		return_msg.Append( err_msg );

		CString log_err_msg("CCLoseGridSystemAXCCtrl::setup() : ");
		log_err_msg += return_msg;
		CGR_LOG(log_err_msg,ERROR_CGR_LOG);

		std::pair<short, std::string> return_error_pair;
		return_error_pair.first = CGSYSTEM_ERROR_CODE(CGSYSTEM_CONNECTIVITY_ERROR);
		return_error_pair.second = std::string( return_msg );

		CGSYSTEM_THROW_EXCEPTION_FROM_PAIR( return_error_pair );

	}


	// set new date/time stamp
	setSystemDateTimeStamp( new_date_time_stamp_CString );

	// set new laser ID
	setSystemLaserID( laser_id_CString );

	*p_date_time_stamp = new_date_time_stamp_CString.AllocSysString();

	// change state
	setSystemState( state_type::set_up );

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::setup() exiting"),DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::setup() p_date_time_stamp = ");
	log_msg += new_date_time_stamp_CString;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	SERVERINTERFACE->closeLogFile();

	return;
}

void 
CCLoseGridSystemAXCCtrl::
shutdown()
{
	if( getSystemState() != state_type::instantiated
	 && getSystemState() != state_type::start_up
	 && getSystemState() != state_type::set_up
	 && getSystemState() != state_type::complete )
	{
		// function cannot be called in the current system state
		CString log_err_msg("CCLoseGridSystemAXCCtrl::shutdown() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR )
	}


	HRESULT hres = S_OK;

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::shutdown() entered"),DIAGNOSTIC_CGR_LOG)




	// change state to instantiated no matter what other error occurs
	setSystemState( state_type::instantiated );

	// clear all system data
	clear();
	_heapmin();


	CString laser_type_CString( "" );
	CString laser_id_CString( "" );
	CString date_time_stamp_CString( "" );

	SERVERINTERFACE->setLogFileName(laser_id_CString,laser_type_CString,date_time_stamp_CString);

	if( !CG_REG->get_DSDBR01_BASE_stub_hardware_calls() )
	{
		// ramp down and turn off all current sources
		hres = SERVERINTERFACE->shutdown();
	}

	if( FAILED(hres) )
	{
		handleCGResearchError( _T("CCLoseGridSystemAXCCtrl::shutdown()"), hres);
	}

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::shutdown() exiting"),DIAGNOSTIC_CGR_LOG)


	SERVERINTERFACE->closeLogFile();

	return;
}

void 
CCLoseGridSystemAXCCtrl::
rampdown()
{
	if( getSystemState() != state_type::set_up
	 && getSystemState() != state_type::complete )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::rampdown() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// function cannot be called in the current system state
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR )
	}

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::rampdown() entered"),DIAGNOSTIC_CGR_LOG)

	HRESULT hres = S_OK;

	if( !CG_REG->get_DSDBR01_BASE_stub_hardware_calls() )
	{
		// ramp down to zero all current sources that are turned on
		hres = SERVERINTERFACE->rampdown();
	}

	if( FAILED(hres) )
	{
		handleCGResearchError( _T("CCLoseGridSystemAXCCtrl::rampdown()"), hres);
	}

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::rampdown() exiting"),DIAGNOSTIC_CGR_LOG)

	SERVERINTERFACE->closeLogFile();

	return;
}


void 
CCLoseGridSystemAXCCtrl::
getCurrent(
	LPCTSTR module_name,
	double* p_current )
{
	if( getSystemState() != state_type::set_up
	 && getSystemState() != state_type::complete )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::getCurrent() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// function cannot be called in the current system state
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR )
	}

	// Check for NULL pointer
	if( module_name == NULL
	 || p_current == NULL )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::getCurrent() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_NULL_PTR_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_NULL_PTR_ERROR )
	}

	CString module_name_CString( module_name );

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::getCurrent() entered"),DIAGNOSTIC_CGR_LOG)
	CString log_msg("CCLoseGridSystemAXCCtrl::getCurrent() module_name = ");
	log_msg += module_name_CString;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	// check the incoming module_name is okay
	if( !validModuleName( module_name_CString ) )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::getCurrent() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_INVALID_MODULE_NAME_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_INVALID_MODULE_NAME_ERROR )
	}

	HRESULT hres = S_OK;


	if( !CG_REG->get_DSDBR01_BASE_stub_hardware_calls() )
	{
		// Get the current being delivered by a chosen current source
		hres = SERVERINTERFACE->getCurrent( module_name_CString, p_current );
	}


	if( FAILED(hres) )
	{
		handleCGResearchError( _T("CCLoseGridSystemAXCCtrl::getCurrent()"), hres);
	}

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::getCurrent() exiting"),DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::getCurrent() p_current = ");
	log_msg.AppendFormat("%g",*p_current);
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	SERVERINTERFACE->closeLogFile();

	return;
}


void 
CCLoseGridSystemAXCCtrl::
setCurrent(
	LPCTSTR module_name,
	double current )
{
	if( getSystemState() != state_type::set_up
	 && getSystemState() != state_type::complete )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::setCurrent() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// function cannot be called in the current system state
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR )
	}

	// Check for NULL pointer
	if( module_name == NULL )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::setCurrent() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_NULL_PTR_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_NULL_PTR_ERROR )
	}

	CString module_name_CString( module_name );

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::setCurrent() entered"),DIAGNOSTIC_CGR_LOG)
	CString log_msg("CCLoseGridSystemAXCCtrl::setCurrent() module_name = ");
	log_msg += module_name_CString;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::setCurrent() current = ");
	log_msg.AppendFormat("%g",current);
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	// check the incoming module_name is okay
	if( !validModuleName( module_name_CString ) )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::setCurrent() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_INVALID_MODULE_NAME_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_INVALID_MODULE_NAME_ERROR )
	}


	HRESULT hres = S_OK;

	if( !CG_REG->get_DSDBR01_BASE_stub_hardware_calls() )
	{
			// Set the current to be delivered by a chosen current source
			hres = SERVERINTERFACE->setCurrent( module_name_CString, current );
	}

	if( FAILED(hres) )
	{
		handleCGResearchError( _T("CCLoseGridSystemAXCCtrl::setCurrent()"), hres);
	}

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::setCurrent() exiting"),DIAGNOSTIC_CGR_LOG)

	SERVERINTERFACE->closeLogFile();

	return;
}

void 
CCLoseGridSystemAXCCtrl::
getOutputConnection(
	LPCTSTR module_name,
	short* p_output_connection )
{
	if( getSystemState() != state_type::set_up
	 && getSystemState() != state_type::complete )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::getOutputConnection() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// function cannot be called in the current system state
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR )
	}

	// Check for NULL pointer
	if( module_name == NULL
	 || p_output_connection == NULL )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::getOutputConnection() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_NULL_PTR_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_NULL_PTR_ERROR )
	}

	CString module_name_CString( module_name );


	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::getOutputConnection() entered"),DIAGNOSTIC_CGR_LOG)
	CString log_msg("CCLoseGridSystemAXCCtrl::getOutputConnection() module_name = ");
	log_msg += module_name_CString;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	// check the incoming module_name is okay
	if( !validModuleName( module_name_CString ) )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::getOutputConnection() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_INVALID_MODULE_NAME_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_INVALID_MODULE_NAME_ERROR )
	}


	HRESULT hres = S_OK;

	if( !CG_REG->get_DSDBR01_BASE_stub_hardware_calls() )
	{
		// get the output connection state of the chosen current source
		hres = SERVERINTERFACE->getOutputConnection( module_name_CString, p_output_connection );
	}

	if( FAILED(hres) )
	{
		handleCGResearchError( _T("CCLoseGridSystemAXCCtrl::getOutputConnection()"), hres);
	}


	log_msg = CString("CCLoseGridSystemAXCCtrl::getOutputConnection() p_output_connection = ");
	log_msg.AppendFormat("%d",*p_output_connection);
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::getOutputConnection() exiting"),DIAGNOSTIC_CGR_LOG)

	SERVERINTERFACE->closeLogFile();

	return;
}


void 
CCLoseGridSystemAXCCtrl::
setOutputConnection(
	LPCTSTR module_name,
	short output_connection )
{
	if( getSystemState() != state_type::set_up
	 && getSystemState() != state_type::complete )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::setOutputConnection() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// function cannot be called in the current system state
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR )
	}

	// Check for NULL pointer
	if( module_name == NULL )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::setOutputConnection() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_NULL_PTR_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_NULL_PTR_ERROR )
	}

	CString module_name_CString( module_name );

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::setOutputConnection() entered"),DIAGNOSTIC_CGR_LOG)
	CString log_msg("CCLoseGridSystemAXCCtrl::setOutputConnection() module_name = ");
	log_msg += module_name_CString;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::setOutputConnection() output_connection = ");
	log_msg.AppendFormat("%d",output_connection);
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	// check the incoming module_name is okay
	if( !validModuleName( module_name_CString ) )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::setOutputConnection() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_INVALID_MODULE_NAME_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_INVALID_MODULE_NAME_ERROR )
	}

	HRESULT hres = S_OK;

	if( !CG_REG->get_DSDBR01_BASE_stub_hardware_calls() )
	{
		// set the output connection state of the chosen current source
		hres = SERVERINTERFACE->setOutputConnection( module_name_CString, output_connection );
	}

	if( FAILED(hres) )
	{
		handleCGResearchError( _T("CCLoseGridSystemAXCCtrl::setOutputConnection()"), hres);
	}

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::setOutputConnection() exiting"),DIAGNOSTIC_CGR_LOG)

	SERVERINTERFACE->closeLogFile();

	return;
}

void 
CCLoseGridSystemAXCCtrl::
get2DData(
	LPCTSTR module_name,
	VARIANT* p_currents,
	short source_delay,
	short measure_delay,
	BSTR* p_results_dir,
	BSTR* p_date_time_stamp )
{

	if( getSystemState() != state_type::set_up
	 && getSystemState() != state_type::complete )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::get2DData() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// function cannot be called in the current system state
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR )
	}

	// Check for NULL pointer
	if( module_name == NULL
	 || p_currents == NULL
	 || p_results_dir == NULL
	 || p_date_time_stamp == NULL  )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::get2DData() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_NULL_PTR_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_NULL_PTR_ERROR )
	}

	CString module_name_CString( module_name );

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::get2DData() entered"),DIAGNOSTIC_CGR_LOG)
	CString log_msg("CCLoseGridSystemAXCCtrl::get2DData() module_name = ");
	log_msg += module_name_CString;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::get2DData() source_delay = ");
	log_msg.AppendFormat("%d", source_delay);
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::get2DData() measure_delay = ");
	log_msg.AppendFormat("%d", measure_delay);
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)



	// check the incoming module_name is okay
	if( !validModuleName( module_name_CString ) )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::get2DData() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_INVALID_MODULE_NAME_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_INVALID_MODULE_NAME_ERROR )
	}


	std::vector<double> currents;
	rcode_type rcode = variantR8ArrayToVectorOfDoubles( p_currents, &currents );

	// check the return code to determine if a VARIANT is invalid
	if( rcode == rcode_type::null_arg )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::get2DData() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_NULL_VARIANT_ARG_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_NULL_VARIANT_ARG_ERROR )
	}
	if( rcode == rcode_type::not_1D_array_of_doubles )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::get2DData() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_NOT1DDOUBLEARRAY_VARIANT_ARG_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_NOT1DDOUBLEARRAY_VARIANT_ARG_ERROR )
	}
	if( rcode == rcode_type::empty_array )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::get2DData() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_EMPTY_VARIANT_ARG_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_EMPTY_VARIANT_ARG_ERROR )
	}
	if( currents.size() >= MAX_SEQUENCE_SIZE )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::get2DData() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_VARIANT_ARG_TOO_LARGE_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_EMPTY_VARIANT_ARG_ERROR )
	}


	bool use_CMDC_306EE = false;
	if( validModuleName(CG_REG->get_DSDBR01_CMDC_306EE_module_name())
	 && CG_REG->get_DSDBR01_CMDC_306EE_module_name() != CG_REG->get_DSDBR01_CMDC_306II_module_name() )
	{
		use_CMDC_306EE = true;
		CGR_LOG("CCLoseGridSystemAXCCtrl::get2DData() Using CMDC_306EE",INFO_CGR_LOG)
	}
	else
	{
		use_CMDC_306EE = false;
		CGR_LOG("CCLoseGridSystemAXCCtrl::get2DData() Not using CMDC_306EE",INFO_CGR_LOG)
	}

	std::vector<double> power306IICh1;
	std::vector<double> power306IICh2;
	std::vector<double> power306EECh1;
	std::vector<double> power306EECh2;

	power306IICh1.clear();
	power306IICh2.clear();
	power306EECh1.clear();
	power306EECh2.clear();

	HRESULT hres = S_OK;

	if( !CG_REG->get_DSDBR01_BASE_stub_hardware_calls() )
	{
		if( SUCCEEDED(hres) ) hres = SERVERINTERFACE->resetSequences();

		if( SUCCEEDED(hres)
		 && module_name_CString != CG_REG->get_DSDBR01_CMDC_gain_module_name() )
		 hres = SERVERINTERFACE->setInSequence( CG_REG->get_DSDBR01_CMDC_gain_module_name(), NOT_IN_SEQUENCE );
		if( SUCCEEDED(hres)
		 && module_name_CString != CG_REG->get_DSDBR01_CMDC_soa_module_name() )
		 hres = SERVERINTERFACE->setInSequence( CG_REG->get_DSDBR01_CMDC_soa_module_name(), NOT_IN_SEQUENCE );
		if( SUCCEEDED(hres)
		 && module_name_CString != CG_REG->get_DSDBR01_CMDC_rear_module_name() )
		 hres = SERVERINTERFACE->setInSequence( CG_REG->get_DSDBR01_CMDC_rear_module_name(), NOT_IN_SEQUENCE );
		if( SUCCEEDED(hres)
		 && module_name_CString != CG_REG->get_DSDBR01_CMDC_phase_module_name() )
		 hres = SERVERINTERFACE->setInSequence( CG_REG->get_DSDBR01_CMDC_phase_module_name(), NOT_IN_SEQUENCE );
		if( SUCCEEDED(hres)
		 && module_name_CString != CG_REG->get_DSDBR01_CMDC_front1_module_name() )
		 hres = SERVERINTERFACE->setInSequence( CG_REG->get_DSDBR01_CMDC_front1_module_name(), NOT_IN_SEQUENCE );
		if( SUCCEEDED(hres)
		 && module_name_CString != CG_REG->get_DSDBR01_CMDC_front2_module_name() )
		 hres = SERVERINTERFACE->setInSequence( CG_REG->get_DSDBR01_CMDC_front2_module_name(), NOT_IN_SEQUENCE );
		if( SUCCEEDED(hres)
		 && module_name_CString != CG_REG->get_DSDBR01_CMDC_front3_module_name() )
		 hres = SERVERINTERFACE->setInSequence( CG_REG->get_DSDBR01_CMDC_front3_module_name(), NOT_IN_SEQUENCE );
		if( SUCCEEDED(hres)
		 && module_name_CString != CG_REG->get_DSDBR01_CMDC_front4_module_name() )
		 hres = SERVERINTERFACE->setInSequence( CG_REG->get_DSDBR01_CMDC_front4_module_name(), NOT_IN_SEQUENCE );
		if( SUCCEEDED(hres)
		 && module_name_CString != CG_REG->get_DSDBR01_CMDC_front5_module_name() )
		 hres = SERVERINTERFACE->setInSequence( CG_REG->get_DSDBR01_CMDC_front5_module_name(), NOT_IN_SEQUENCE );
		if( SUCCEEDED(hres)
		 && module_name_CString != CG_REG->get_DSDBR01_CMDC_front6_module_name() )
		 hres = SERVERINTERFACE->setInSequence( CG_REG->get_DSDBR01_CMDC_front6_module_name(), NOT_IN_SEQUENCE );
		if( SUCCEEDED(hres)
		 && module_name_CString != CG_REG->get_DSDBR01_CMDC_front7_module_name() )
		 hres = SERVERINTERFACE->setInSequence( CG_REG->get_DSDBR01_CMDC_front7_module_name(), NOT_IN_SEQUENCE );
		if( SUCCEEDED(hres)
		 && module_name_CString != CG_REG->get_DSDBR01_CMDC_front8_module_name() )
		 hres = SERVERINTERFACE->setInSequence( CG_REG->get_DSDBR01_CMDC_front8_module_name(), NOT_IN_SEQUENCE );
		//if( SUCCEEDED(hres) ) hres = SERVERINTERFACE->setInSequence( CG_REG->get_DSDBR01_CMDC_306II_module_name(), NOT_IN_SEQUENCE );
		//if( SUCCEEDED(hres) ) SERVERINTERFACE->setInSequence( CG_REG->get_DSDBR01_CMDC_306EE_module_name(), NOT_IN_SEQUENCE );

		if( SUCCEEDED(hres) ) hres = SERVERINTERFACE->resetSequences();

		// set 305 output ON
		if( SUCCEEDED(hres) ) hres = SERVERINTERFACE->setOutputConnection( module_name_CString, CONNECT_OUTPUT );

		// set 305, 306s in sequence
		if( SUCCEEDED(hres) ) hres = SERVERINTERFACE->setInSequence( module_name_CString, IN_SEQUENCE );
		if( SUCCEEDED(hres) ) hres = SERVERINTERFACE->setInSequence( CG_REG->get_DSDBR01_CMDC_306II_module_name(), IN_SEQUENCE );
		if( SUCCEEDED(hres) )
		{
			if( use_CMDC_306EE ) hres = SERVERINTERFACE->setInSequence( CG_REG->get_DSDBR01_CMDC_306EE_module_name(), IN_SEQUENCE );
			else hres = SERVERINTERFACE->setInSequence( CG_REG->get_DSDBR01_CMDC_306EE_module_name(), NOT_IN_SEQUENCE );
		}

		// reset all
		if( SUCCEEDED(hres) ) hres = SERVERINTERFACE->resetSequences();

		// load 305 sequence
		if( SUCCEEDED(hres) ) hres = SERVERINTERFACE->load305Sequence( module_name_CString, currents, source_delay, measure_delay );

		// load 306s sequence
		if( SUCCEEDED(hres) ) hres = SERVERINTERFACE->load306Sequence( CG_REG->get_DSDBR01_CMDC_306II_module_name(), (short)currents.size(), ALL_CHANNELS, source_delay, measure_delay );
		if( SUCCEEDED(hres) && use_CMDC_306EE ) hres = SERVERINTERFACE->load306Sequence( CG_REG->get_DSDBR01_CMDC_306EE_module_name(), (short)currents.size(), ALL_CHANNELS, source_delay, measure_delay );

		// reset all
		if( SUCCEEDED(hres) ) hres = SERVERINTERFACE->resetSequences();

		// run all
		if( SUCCEEDED(hres) ) hres = SERVERINTERFACE->runSequences();

		// reset all
		if( SUCCEEDED(hres) ) hres = SERVERINTERFACE->resetSequences();

		// get 306 measurements
		if( SUCCEEDED(hres) ) hres = SERVERINTERFACE->get306SequenceMeasurements( CG_REG->get_DSDBR01_CMDC_306II_module_name(), &power306IICh1, &power306IICh2 );
		if( SUCCEEDED(hres) && use_CMDC_306EE ) hres = SERVERINTERFACE->get306SequenceMeasurements( CG_REG->get_DSDBR01_CMDC_306EE_module_name(), &power306EECh1, &power306EECh2 );


		CGR_LOG(CString("CCLoseGridSystemAXCCtrl::get2DData() returned from SERVERINTERFACE->get306SequenceMeasurements"),DIAGNOSTIC_CGR_LOG)

		// reset all
		if( SUCCEEDED(hres) ) hres = SERVERINTERFACE->resetSequences();

		// set 305 current = 0
		if( SUCCEEDED(hres) ) hres = SERVERINTERFACE->setCurrent( module_name_CString, 0 );
		// set 305 output OFF
		if( SUCCEEDED(hres) ) hres = SERVERINTERFACE->setOutputConnection( module_name_CString, DISCONNECT_OUTPUT );

		// set 305,306 not in sequence
		if( SUCCEEDED(hres) ) hres = SERVERINTERFACE->setInSequence( module_name_CString, NOT_IN_SEQUENCE );
		if( SUCCEEDED(hres) ) hres = SERVERINTERFACE->setInSequence( CG_REG->get_DSDBR01_CMDC_306II_module_name(), NOT_IN_SEQUENCE );
		if( SUCCEEDED(hres) && use_CMDC_306EE ) hres = SERVERINTERFACE->setInSequence( CG_REG->get_DSDBR01_CMDC_306EE_module_name(), NOT_IN_SEQUENCE );

		if( SUCCEEDED(hres) ) hres = SERVERINTERFACE->resetSequences();

	}

	if( FAILED(hres) )
	{
		handleCGResearchError( _T("CCLoseGridSystemAXCCtrl::get2DData()"), hres);
	}



	// push currents and results into one vector
	std::vector<double> results;
	long rows = (long)currents.size();
	long cols = 3;
	if( use_CMDC_306EE ) cols = 5;

	for( long i = 0 ; i < (long)currents.size() ; i++ )
	{
		results.push_back(currents[i]);

		if( i < (long)power306IICh1.size() )
			results.push_back(power306IICh1[i]);
		else
			results.push_back(0);

		if( i < (long)power306IICh2.size() )
			results.push_back(power306IICh2[i]);
		else
			results.push_back(0);			

		if( use_CMDC_306EE )
		{
			if( i < (long)power306EECh1.size() )
				results.push_back(power306EECh1[i]);
			else
				results.push_back(0);			

			if( i < (long)power306EECh2.size() )
				results.push_back(power306EECh2[i]);
			else
				results.push_back(0);			
		}
	}

	CLaserModeMap results_CLaserModeMap(results, rows, cols);

	CString results_dir_CString = CG_REG->get_DSDBR01_BASE_results_directory();
	CString laser_id_CString = getSystemLaserID();
	CString date_time_stamp_CString = getSystemDateTimeStamp();

	CString results_filename = results_dir_CString;
	if( results_filename.Right(1) != _T("\\") )
		results_filename += _T("\\");
	results_filename += _T("Result2D_");
	results_filename += LASER_TYPE_ID_DSDBR01_CSTRING;
	results_filename += USCORE;
	results_filename += laser_id_CString;
	results_filename += USCORE;
	results_filename += date_time_stamp_CString;
	results_filename += CSV;

	log_msg = CString("CCLoseGridSystemAXCCtrl::get2DData() results_filename = ");
	log_msg += results_filename;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	*p_results_dir = results_dir_CString.AllocSysString();
	*p_date_time_stamp = date_time_stamp_CString.AllocSysString();


	log_msg = CString("CCLoseGridSystemAXCCtrl::get2DData() p_results_dir = ");
	log_msg += results_dir_CString;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::get2DData() p_date_time_stamp = ");
	log_msg += date_time_stamp_CString;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	CLaserModeMap::rcode rcode_map = CLaserModeMap::rcode::ok;
	rcode_map = results_CLaserModeMap.writeToFile(results_filename,true);

	if( rcode_map != CLaserModeMap::rcode::ok )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::get2DData() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_ERROR_WRITING_RESULTS_TO_FILE) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// function cannot be called in the current system state
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_ERROR_WRITING_RESULTS_TO_FILE )
	}

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::get2DData() exiting"),DIAGNOSTIC_CGR_LOG)

	SERVERINTERFACE->closeLogFile();
}

void 
CCLoseGridSystemAXCCtrl::
get3DData(
	LPCTSTR module_name_x,
	VARIANT* p_currents_x,
	LPCTSTR module_name_y,
	VARIANT* p_currents_y,
	short map_type,
	short source_delay,
	short measure_delay,
	BSTR* p_results_dir,
	BSTR* p_date_time_stamp )
{

	if( getSystemState() != state_type::set_up
	 && getSystemState() != state_type::complete )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::get3DData() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// function cannot be called in the current system state
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR )
	}

	// Check for NULL pointer
	if( module_name_x == NULL
	 || p_currents_x == NULL
	 || module_name_y == NULL
	 || p_currents_y == NULL
	 || p_results_dir == NULL
	 || p_date_time_stamp == NULL )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::get3DData() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_NULL_PTR_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_NULL_PTR_ERROR )
	}

	CString module_name_x_CString( module_name_x );
	CString module_name_y_CString( module_name_y );


	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::get3DData() entered"),DIAGNOSTIC_CGR_LOG)
	CString log_msg("CCLoseGridSystemAXCCtrl::get3DData() module_name_x = ");
	log_msg += module_name_x_CString;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::get3DData() module_name_y = ");
	log_msg += module_name_y_CString;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::get3DData() map_type = ");
	log_msg.AppendFormat("%d", map_type);
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::get3DData() source_delay = ");
	log_msg.AppendFormat("%d", source_delay);
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::get3DData() measure_delay = ");
	log_msg.AppendFormat("%d", measure_delay);
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)


	// check the incoming module names is okay
	if( !validModuleName( module_name_x_CString )
	 || !validModuleName( module_name_y_CString ) )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::get3DData() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_INVALID_MODULE_NAME_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_INVALID_MODULE_NAME_ERROR )
	}

	if( map_type != MAP_TYPE_SAWTOOTH && map_type != MAP_TYPE_TRIANGLE )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::get3DData() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_INVALID_MAP_TYPE_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_INVALID_MAP_TYPE_ERROR )
	}


	std::vector<double> currents_x;
	rcode_type rcode = variantR8ArrayToVectorOfDoubles( p_currents_x, &currents_x );

	// check the return code to determine if a VARIANT is invalid
	if( rcode == rcode_type::null_arg )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::get3DData() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_NULL_VARIANT_ARG_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_NULL_VARIANT_ARG_ERROR )
	}
	if( rcode == rcode_type::not_1D_array_of_doubles )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::get3DData() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_NOT1DDOUBLEARRAY_VARIANT_ARG_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_NOT1DDOUBLEARRAY_VARIANT_ARG_ERROR )
	}
	if( rcode == rcode_type::empty_array )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::get3DData() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_EMPTY_VARIANT_ARG_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_EMPTY_VARIANT_ARG_ERROR )
	}

	std::vector<double> currents_y;
	rcode = variantR8ArrayToVectorOfDoubles( p_currents_y, &currents_y );

	// check the return code to determine if a VARIANT is invalid
	if( rcode == rcode_type::null_arg )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::get3DData() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_NULL_VARIANT_ARG_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_NULL_VARIANT_ARG_ERROR )
	}
	if( rcode == rcode_type::not_1D_array_of_doubles )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::get3DData() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_NOT1DDOUBLEARRAY_VARIANT_ARG_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_NOT1DDOUBLEARRAY_VARIANT_ARG_ERROR )
	}
	if( rcode == rcode_type::empty_array )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::get3DData() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_EMPTY_VARIANT_ARG_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_EMPTY_VARIANT_ARG_ERROR )
	}


	bool use_CMDC_306EE = false;
	if( validModuleName(CG_REG->get_DSDBR01_CMDC_306EE_module_name())
	 && CG_REG->get_DSDBR01_CMDC_306EE_module_name() != CG_REG->get_DSDBR01_CMDC_306II_module_name() )
	{
		use_CMDC_306EE = true;
		CGR_LOG("CCLoseGridSystemAXCCtrl::get3DData() Using CMDC_306EE",INFO_CGR_LOG)
	}
	else
	{
		use_CMDC_306EE = false;
		CGR_LOG("CCLoseGridSystemAXCCtrl::get3DData() Not using CMDC_306EE",INFO_CGR_LOG)
	}


	std::vector<double> power306IICh1;
	std::vector<double> power306IICh2;
	std::vector<double> power306EECh1;
	std::vector<double> power306EECh2;

	power306IICh1.clear();
	power306IICh2.clear();
	power306EECh1.clear();
	power306EECh2.clear();

	HRESULT hres = S_OK;

	if( !CG_REG->get_DSDBR01_BASE_stub_hardware_calls() )
	{

		// set 305 output ON
		if( SUCCEEDED(hres) ) hres = SERVERINTERFACE->setOutputConnection( module_name_x_CString, CONNECT_OUTPUT );
		if( SUCCEEDED(hres) ) hres = SERVERINTERFACE->setOutputConnection( module_name_y_CString, CONNECT_OUTPUT );

		// set 305,306s in sequence, all other not in sequence

		if( SUCCEEDED(hres)
		 && module_name_x_CString != CG_REG->get_DSDBR01_CMDC_gain_module_name()
		 && module_name_y_CString != CG_REG->get_DSDBR01_CMDC_gain_module_name() )
		 hres = SERVERINTERFACE->setInSequence( CG_REG->get_DSDBR01_CMDC_gain_module_name(), NOT_IN_SEQUENCE );
		if( SUCCEEDED(hres)
		 && module_name_x_CString != CG_REG->get_DSDBR01_CMDC_soa_module_name()
		 && module_name_y_CString != CG_REG->get_DSDBR01_CMDC_soa_module_name() )
		 hres = SERVERINTERFACE->setInSequence( CG_REG->get_DSDBR01_CMDC_soa_module_name(), NOT_IN_SEQUENCE );
		if( SUCCEEDED(hres)
		 && module_name_x_CString != CG_REG->get_DSDBR01_CMDC_rear_module_name()
		 && module_name_y_CString != CG_REG->get_DSDBR01_CMDC_rear_module_name() )
		 hres = SERVERINTERFACE->setInSequence( CG_REG->get_DSDBR01_CMDC_rear_module_name(), NOT_IN_SEQUENCE );
		if( SUCCEEDED(hres)
		 && module_name_x_CString != CG_REG->get_DSDBR01_CMDC_phase_module_name()
		 && module_name_y_CString != CG_REG->get_DSDBR01_CMDC_phase_module_name() )
		 hres = SERVERINTERFACE->setInSequence( CG_REG->get_DSDBR01_CMDC_phase_module_name(), NOT_IN_SEQUENCE );
		if( SUCCEEDED(hres)
		 && module_name_x_CString != CG_REG->get_DSDBR01_CMDC_front1_module_name()
		 && module_name_y_CString != CG_REG->get_DSDBR01_CMDC_front1_module_name() )
		 hres = SERVERINTERFACE->setInSequence( CG_REG->get_DSDBR01_CMDC_front1_module_name(), NOT_IN_SEQUENCE );
		if( SUCCEEDED(hres)
		 && module_name_x_CString != CG_REG->get_DSDBR01_CMDC_front2_module_name()
		 && module_name_y_CString != CG_REG->get_DSDBR01_CMDC_front2_module_name() )
		 hres = SERVERINTERFACE->setInSequence( CG_REG->get_DSDBR01_CMDC_front2_module_name(), NOT_IN_SEQUENCE );
		if( SUCCEEDED(hres)
		 && module_name_x_CString != CG_REG->get_DSDBR01_CMDC_front3_module_name()
		 && module_name_y_CString != CG_REG->get_DSDBR01_CMDC_front3_module_name() )
		 hres = SERVERINTERFACE->setInSequence( CG_REG->get_DSDBR01_CMDC_front3_module_name(), NOT_IN_SEQUENCE );
		if( SUCCEEDED(hres)
		 && module_name_x_CString != CG_REG->get_DSDBR01_CMDC_front4_module_name()
		 && module_name_y_CString != CG_REG->get_DSDBR01_CMDC_front4_module_name() )
		 hres = SERVERINTERFACE->setInSequence( CG_REG->get_DSDBR01_CMDC_front4_module_name(), NOT_IN_SEQUENCE );
		if( SUCCEEDED(hres)
		 && module_name_x_CString != CG_REG->get_DSDBR01_CMDC_front5_module_name()
		 && module_name_y_CString != CG_REG->get_DSDBR01_CMDC_front5_module_name() )
		 hres = SERVERINTERFACE->setInSequence( CG_REG->get_DSDBR01_CMDC_front5_module_name(), NOT_IN_SEQUENCE );
		if( SUCCEEDED(hres)
		 && module_name_x_CString != CG_REG->get_DSDBR01_CMDC_front6_module_name()
		 && module_name_y_CString != CG_REG->get_DSDBR01_CMDC_front6_module_name() )
		 hres = SERVERINTERFACE->setInSequence( CG_REG->get_DSDBR01_CMDC_front6_module_name(), NOT_IN_SEQUENCE );
		if( SUCCEEDED(hres)
		 && module_name_x_CString != CG_REG->get_DSDBR01_CMDC_front7_module_name()
		 && module_name_y_CString != CG_REG->get_DSDBR01_CMDC_front7_module_name() )
		 hres = SERVERINTERFACE->setInSequence( CG_REG->get_DSDBR01_CMDC_front7_module_name(), NOT_IN_SEQUENCE );
		if( SUCCEEDED(hres)
		 && module_name_x_CString != CG_REG->get_DSDBR01_CMDC_front8_module_name()
		 && module_name_y_CString != CG_REG->get_DSDBR01_CMDC_front8_module_name() )
		 hres = SERVERINTERFACE->setInSequence( CG_REG->get_DSDBR01_CMDC_front8_module_name(), NOT_IN_SEQUENCE );

		if( SUCCEEDED(hres) ) hres = SERVERINTERFACE->setInSequence( module_name_x_CString, IN_SEQUENCE );
		if( SUCCEEDED(hres) ) hres = SERVERINTERFACE->setInSequence( module_name_y_CString, IN_SEQUENCE );
		if( SUCCEEDED(hres) ) hres = SERVERINTERFACE->setInSequence( CG_REG->get_DSDBR01_CMDC_306II_module_name(), IN_SEQUENCE );
		if( SUCCEEDED(hres) )
		{
			if( use_CMDC_306EE )
			{
				hres = SERVERINTERFACE->setInSequence( CG_REG->get_DSDBR01_CMDC_306EE_module_name(), IN_SEQUENCE );
			}
			else
			{	// Ignore any error, module may not exist
				SERVERINTERFACE->setInSequence( CG_REG->get_DSDBR01_CMDC_306EE_module_name(), NOT_IN_SEQUENCE );
			}
		}

		// reset all
		if( SUCCEEDED(hres) ) hres = SERVERINTERFACE->resetSequences();


		// define the sequences to gather a 3D map
		std::vector<double> currents_module_x_sequence;
		std::vector<double> currents_module_y_sequence;

		long num_of_points_per_row = (long)currents_x.size();
		if( map_type == MAP_TYPE_TRIANGLE ) num_of_points_per_row *= 2;

		long max_number_of_rows = (long)currents_y.size();
		long sequence_size = max_number_of_rows * num_of_points_per_row;
		long number_of_sequences = 1;

		if( sequence_size > MAX_SEQUENCE_SIZE )
		{
			// need to split up the sequence

			long number_of_rows = sequence_size / num_of_points_per_row;

			max_number_of_rows = MAX_SEQUENCE_SIZE / num_of_points_per_row;

			number_of_sequences = MAX_SEQUENCE_SIZE / (max_number_of_rows * num_of_points_per_row);

			if( sequence_size > max_number_of_rows * number_of_sequences * num_of_points_per_row )
				number_of_sequences++;
		}

		for( long seq_num = 0 ; seq_num < number_of_sequences ; seq_num++ )
		{
			currents_module_x_sequence.clear();
			currents_module_y_sequence.clear();

			for( long rows = seq_num*max_number_of_rows ;
				rows < (seq_num+1)*max_number_of_rows && rows < (long)currents_y.size() ;
				rows++ )
			{
				for( long cols = 0 ; cols < (long)currents_x.size() ; cols++ )
				{
					currents_module_y_sequence.push_back(currents_y[rows]);
					currents_module_x_sequence.push_back(currents_x[cols]);
				}

				if( map_type == MAP_TYPE_TRIANGLE )
				{
					for( long cols = (long)currents_x.size() - 1  ; cols >=0  ; cols-- )
					{
						currents_module_y_sequence.push_back(currents_y[rows]);
						currents_module_x_sequence.push_back(currents_x[cols]);
					}
				}
			}

			// load 305 x and y sequences
			if( SUCCEEDED(hres) ) hres = SERVERINTERFACE->load305Sequence( module_name_x_CString, currents_module_x_sequence, source_delay, measure_delay );
			if( SUCCEEDED(hres) ) hres = SERVERINTERFACE->load305Sequence( module_name_y_CString, currents_module_y_sequence, source_delay, measure_delay );

			short num_of_points = (short)currents_module_x_sequence.size();
			// load 306s sequence
			if( SUCCEEDED(hres) ) hres = SERVERINTERFACE->load306Sequence( CG_REG->get_DSDBR01_CMDC_306II_module_name(), num_of_points, ALL_CHANNELS, source_delay, measure_delay );
			if( SUCCEEDED(hres) && use_CMDC_306EE ) hres = SERVERINTERFACE->load306Sequence( CG_REG->get_DSDBR01_CMDC_306EE_module_name(), num_of_points, ALL_CHANNELS, source_delay, measure_delay );

			// reset all
			if( SUCCEEDED(hres) ) hres = SERVERINTERFACE->resetSequences();

			// run all
			if( SUCCEEDED(hres) ) hres = SERVERINTERFACE->runSequences();

			// reset all
			if( SUCCEEDED(hres) ) hres = SERVERINTERFACE->resetSequences();


			std::vector<double> power306IICh1_sequence;
			std::vector<double> power306IICh2_sequence;
			power306IICh1_sequence.clear();
			power306IICh2_sequence.clear();

			// get 306 measurements
			if( SUCCEEDED(hres) ) hres = SERVERINTERFACE->get306SequenceMeasurements( CG_REG->get_DSDBR01_CMDC_306II_module_name(), &power306IICh1_sequence, &power306IICh2_sequence );

			if( SUCCEEDED(hres) )
			{
				power306IICh1.insert( power306IICh1.end(),power306IICh1_sequence.begin(),power306IICh1_sequence.end());
				power306IICh2.insert( power306IICh2.end(),power306IICh2_sequence.begin(),power306IICh2_sequence.end());
			}

			if( use_CMDC_306EE )
			{
				std::vector<double> power306EECh1_sequence;
				std::vector<double> power306EECh2_sequence;
				power306EECh1_sequence.clear();
				power306EECh2_sequence.clear();

				if( SUCCEEDED(hres) ) hres = SERVERINTERFACE->get306SequenceMeasurements( CG_REG->get_DSDBR01_CMDC_306EE_module_name(), &power306EECh1_sequence, &power306EECh2_sequence );

				if( SUCCEEDED(hres) )
				{
					power306EECh1.insert( power306EECh1.end(),power306EECh1_sequence.begin(),power306EECh1_sequence.end());
					power306EECh2.insert( power306EECh2.end(),power306EECh2_sequence.begin(),power306EECh2_sequence.end());
				}
			}

			// reset all
			if( SUCCEEDED(hres) ) hres = SERVERINTERFACE->resetSequences();
		}

		// set 305s current = 0
		if( SUCCEEDED(hres) ) hres = SERVERINTERFACE->setCurrent( module_name_x_CString, 0 );
		if( SUCCEEDED(hres) ) hres = SERVERINTERFACE->setCurrent( module_name_y_CString, 0 );
		// set 305s output OFF
		if( SUCCEEDED(hres) ) hres = SERVERINTERFACE->setOutputConnection( module_name_x_CString, DISCONNECT_OUTPUT );
		if( SUCCEEDED(hres) ) hres = SERVERINTERFACE->setOutputConnection( module_name_y_CString, DISCONNECT_OUTPUT );

		// set 305,306 not in sequence
		if( SUCCEEDED(hres) ) hres = SERVERINTERFACE->setInSequence( module_name_x_CString, NOT_IN_SEQUENCE );
		if( SUCCEEDED(hres) ) hres = SERVERINTERFACE->setInSequence( module_name_y_CString, NOT_IN_SEQUENCE );
		if( SUCCEEDED(hres) ) hres = SERVERINTERFACE->setInSequence( CG_REG->get_DSDBR01_CMDC_306II_module_name(), NOT_IN_SEQUENCE );
		if( SUCCEEDED(hres) && use_CMDC_306EE ) hres = SERVERINTERFACE->setInSequence( CG_REG->get_DSDBR01_CMDC_306EE_module_name(), NOT_IN_SEQUENCE );

	}

	if( FAILED(hres) )
	{
		handleCGResearchError( _T("CCLoseGridSystemAXCCtrl::get3DData()"), hres);
	}


	long cols = (long)currents_x.size();
	if( map_type == MAP_TYPE_TRIANGLE ) cols *= 2;
	long rows = (long)currents_y.size();

	CLaserModeMap power306IICh1_CLaserModeMap( power306IICh1, rows, cols );
	CLaserModeMap power306IICh2_CLaserModeMap( power306IICh2, rows, cols );
	CLaserModeMap power306EECh1_CLaserModeMap( power306EECh1, rows, cols );
	CLaserModeMap power306EECh2_CLaserModeMap( power306EECh2, rows, cols );

	//if(rval == rcode_type::ok) rval = vectorOfDoublesToVariantR8Array( &power306IICh1, p_306II_Ch1 );
	//if(rval == rcode_type::ok) rval = vectorOfDoublesToVariantR8Array( &power306IICh2, p_306II_Ch2 );
	//if(rval == rcode_type::ok) rval = vectorOfDoublesToVariantR8Array( &power306EECh1, p_306EE_Ch1 );
	//if(rval == rcode_type::ok) rval = vectorOfDoublesToVariantR8Array( &power306EECh2, p_306EE_Ch2 );

	//if( rval != rcode_type::ok )
	//{
	//	CString log_err_msg("CCLoseGridSystemAXCCtrl::get3DData() error: ");
	//	log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_VARIANT_CREATION_ERROR) );
	//	CGR_LOG(log_err_msg,ERROR_CGR_LOG)

	//	CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_VARIANT_CREATION_ERROR )
	//}

	CString results_dir_CString = CG_REG->get_DSDBR01_BASE_results_directory();
	CString laser_id_CString = getSystemLaserID();
	CString date_time_stamp_CString = getSystemDateTimeStamp();

	CString power306IICh1_filename = results_dir_CString;
	if( power306IICh1_filename.Right(1) != _T("\\") )
		power306IICh1_filename += _T("\\");
	power306IICh1_filename += _T("MATRIX_power306IICh1_");
	power306IICh1_filename += LASER_TYPE_ID_DSDBR01_CSTRING;
	power306IICh1_filename += USCORE;
	power306IICh1_filename += laser_id_CString;
	power306IICh1_filename += USCORE;
	power306IICh1_filename += date_time_stamp_CString;
	power306IICh1_filename += CSV;

	CString power306IICh2_filename = power306IICh1_filename;
	CString power306EECh1_filename = power306IICh1_filename;
	CString power306EECh2_filename = power306IICh1_filename;

	power306IICh2_filename.Replace("MATRIX_power306IICh1_","MATRIX_power306IICh2_");
	power306EECh1_filename.Replace("MATRIX_power306IICh1_","MATRIX_power306EECh1_");
	power306EECh2_filename.Replace("MATRIX_power306IICh1_","MATRIX_power306EECh2_");

	*p_results_dir = results_dir_CString.AllocSysString();
	*p_date_time_stamp = date_time_stamp_CString.AllocSysString();


	log_msg = CString("CCLoseGridSystemAXCCtrl::get3DData() p_results_dir = ");
	log_msg += results_dir_CString;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	log_msg = CString("CCLoseGridSystemAXCCtrl::get3DData() p_date_time_stamp = ");
	log_msg += date_time_stamp_CString;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	log_msg = CString("CCLoseGridSystemAXCCtrl::get3DData() power306IICh1 filename = ");
	log_msg += power306IICh1_filename;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::get3DData() power306IICh2 filename = ");
	log_msg += power306IICh2_filename;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	if( use_CMDC_306EE )
	{
		log_msg = CString("CCLoseGridSystemAXCCtrl::get3DData() power306EECh1 filename = ");
		log_msg += power306IICh1_filename;
		CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
		log_msg = CString("CCLoseGridSystemAXCCtrl::get3DData() power306EECh2 filename = ");
		log_msg += power306IICh2_filename;
		CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	}

	CLaserModeMap::rcode rcode_map = CLaserModeMap::rcode::ok;

	if( rcode_map == CLaserModeMap::rcode::ok )
		rcode_map = power306IICh1_CLaserModeMap.writeToFile(power306IICh1_filename,true);
	if( rcode_map == CLaserModeMap::rcode::ok )
		rcode_map = power306IICh2_CLaserModeMap.writeToFile(power306IICh2_filename,true);
	if( rcode_map == CLaserModeMap::rcode::ok )
		rcode_map = power306EECh1_CLaserModeMap.writeToFile(power306EECh1_filename,true);
	if( rcode_map == CLaserModeMap::rcode::ok )
		rcode_map = power306EECh2_CLaserModeMap.writeToFile(power306EECh2_filename,true);

	if( rcode_map != CLaserModeMap::rcode::ok )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::get3DData() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_ERROR_WRITING_RESULTS_TO_FILE) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// function cannot be called in the current system state
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_ERROR_WRITING_RESULTS_TO_FILE )
	}

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::get3DData() exiting"),DIAGNOSTIC_CGR_LOG)

	SERVERINTERFACE->closeLogFile();

	return;
}




void 
CCLoseGridSystemAXCCtrl::
startLaserScreeningOverallMap()
{
	if( getSystemState() != state_type::set_up
	 && getSystemState() != state_type::complete )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::startLaserScreeningOverallMap() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// function cannot be called in the current system state
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR )
	}

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::startLaserScreeningOverallMap() entered"),DIAGNOSTIC_CGR_LOG)

	two_ptrs ptrs;
	ptrs.p_axc = this;
	ptrs.p_other = &_overall_mode_map;


	_overall_mode_map.clear();

	// set results dir, laser_id and date_time_stamp
	_overall_mode_map._results_dir_CString = CG_REG->get_DSDBR01_BASE_results_directory();
	_overall_mode_map._laser_id_CString = getSystemLaserID();
	_overall_mode_map._date_time_stamp_CString = getSystemDateTimeStamp();

	// kick off thread to gather data from a live laser
	// and produce screening results
	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::startLaserScreeningOverallMap() kicking off new thread for screenOverallMapProc"),DIAGNOSTIC_CGR_LOG)

	CWinThread* p_thread = AfxBeginThread( screenOverallMapProc , (LPVOID)(&ptrs), THREAD_PRIORITY_HIGHEST );

	if( p_thread == NULL )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::startLaserScreeningOverallMap() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_WORKER_THREAD_CREATION_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		setWorkerThreadID( NULL );

		// the worker thread was not created
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_WORKER_THREAD_CREATION_ERROR )
	}
	else
	{
		CString logmsg("CCLoseGridSystemAXCCtrl::startLaserScreeningOverallMap() : ");
		logmsg.AppendFormat( "started new thread, thread ID = %d", p_thread->m_nThreadID );
		CGR_LOG(logmsg,DIAGNOSTIC_CGR_LOG)

		setWorkerThreadID( p_thread->m_nThreadID );
	}

	setSystemState( CCLoseGridSystemAXCCtrl::state_type::busy );

	// Allow new thread time to change system data
	Sleep(100);

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::startLaserScreeningOverallMap() exiting"),DIAGNOSTIC_CGR_LOG)

	return;
}





void 
CCLoseGridSystemAXCCtrl::
startLaserScreeningSMMap( short supermode_number )
{
	if( getSystemState() != state_type::complete )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::startLaserScreeningSMMap() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// function cannot be called in the current system state
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR )
	}

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::startLaserScreeningSMMap() entered"),DIAGNOSTIC_CGR_LOG)
	CString log_msg("CCLoseGridSystemAXCCtrl::startLaserScreeningSMMap() supermode_number = ");
	log_msg.AppendFormat("%d",supermode_number);
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)


	if( ( supermode_number > (getSMFoundCount() - 1) )
	 || ( supermode_number < 0 ) )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::startLaserScreeningSMMap() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_SPECIFIED_SUPERMODE_NOT_FOUND) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// supermode number requested is not available
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_SPECIFIED_SUPERMODE_NOT_FOUND )
	}


	two_ptrs ptrs;
	ptrs.p_axc = this;
	// use this to point to the object representing SM Map to be screened
	ptrs.p_other = (void*)(&(_overall_mode_map._supermodes[supermode_number]));

	_overall_mode_map._supermodes[supermode_number].clearMapsAndLines();

	// set results dir, laser_id and date_time_stamp

	_overall_mode_map._supermodes[supermode_number]._results_dir_CString
		= CG_REG->get_DSDBR01_BASE_results_directory();

	_overall_mode_map._supermodes[supermode_number]._laser_id_CString
		= getSystemLaserID();

	_overall_mode_map._supermodes[supermode_number]._date_time_stamp_CString
		= getSystemDateTimeStamp();

	// kick off thread to gather data from a live laser
	// and produce screening results
	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::startLaserScreeningSMMap() kicking off new thread for screenSMMapProc"),DIAGNOSTIC_CGR_LOG)

	CWinThread* p_thread = AfxBeginThread( screenSMMapProc , (LPVOID)(&ptrs), THREAD_PRIORITY_HIGHEST );

	if( p_thread == NULL )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::startLaserScreeningSMMap() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_WORKER_THREAD_CREATION_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		setWorkerThreadID( NULL );

		// the worker thread was not created
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_WORKER_THREAD_CREATION_ERROR )
	}
	else
	{
		CString logmsg("CCLoseGridSystemAXCCtrl::startLaserScreeningSMMap() : ");
		logmsg.AppendFormat( "started new thread, thread ID = %d", p_thread->m_nThreadID );
		CGR_LOG(logmsg,DIAGNOSTIC_CGR_LOG)

		setWorkerThreadID( p_thread->m_nThreadID );
	}

	setSystemState( CCLoseGridSystemAXCCtrl::state_type::busy );

	// Allow new thread time to change system data
	Sleep(100);

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::startLaserScreeningSMMap() exiting"),DIAGNOSTIC_CGR_LOG)

	return;
}

void
CCLoseGridSystemAXCCtrl::
estimateITUOPs(
	BSTR* p_results_dir,
	BSTR* p_date_time_stamp,
	short* p_ITUOP_estimates_count )
{
	if( getSystemState() != state_type::complete )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::estimateITUOPs() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// function cannot be called in the current system state
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR )
	}

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::estimateITUOPs() entered"),DIAGNOSTIC_CGR_LOG)

	// Check for NULL pointer
	if( p_results_dir == NULL
	 || p_date_time_stamp == NULL
	 || p_ITUOP_estimates_count == NULL )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::estimateITUOPs() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_NULL_PTR_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_NULL_PTR_ERROR )
	}


	// use same directory, laser_id and time_stamp as in _overall_mode_map
	_ops._results_dir_CString = _overall_mode_map._results_dir_CString;
	_ops._laser_id_CString = _overall_mode_map._laser_id_CString;
	_ops._date_time_stamp_CString = _overall_mode_map._date_time_stamp_CString;

	// Collect samples and calculate polynomial
	CCGOperatingPoints::rtype estimate_ituop_rval = CCGOperatingPoints::rtype::ok;

	estimate_ituop_rval = _ops.estimateITUOperatingPoints();

	if( estimate_ituop_rval != CCGOperatingPoints::rtype::ok )
	{
		setEstimateCount( 0 );

		CString log_err_msg("CCLoseGridSystemAXCCtrl::estimateITUOPs() error: ");
		log_err_msg += CString(_ops.get_error_pair( estimate_ituop_rval ).second.c_str());
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION_FROM_PAIR( _ops.get_error_pair( estimate_ituop_rval ) )

	}
	else
	{

		*p_ITUOP_estimates_count = _ops.countITUOPEstimatesFound();
		setEstimateCount( *p_ITUOP_estimates_count );

		*p_results_dir = _ops._results_dir_CString.AllocSysString();
		*p_date_time_stamp = _ops._date_time_stamp_CString.AllocSysString();
	}

	CString log_msg("CCLoseGridSystemAXCCtrl::estimateITUOPs() p_results_dir = ");
	log_msg += _ops._results_dir_CString;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::estimateITUOPs() p_date_time_stamp = ");
	log_msg += _ops._date_time_stamp_CString;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::estimateITUOPs() p_ITUOP_estimates_count = ");
	log_msg.AppendFormat("%d",*p_ITUOP_estimates_count);
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::estimateITUOPs() exiting"),DIAGNOSTIC_CGR_LOG)

	SERVERINTERFACE->closeLogFile();

	return;
}


void 
CCLoseGridSystemAXCCtrl::
startLaserCharacterisation()
{
	if( getSystemState() != state_type::complete )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::startLaserCharacterisation() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// function cannot be called in the current system state
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR )
	}

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::startLaserCharacterisation() entered"),DIAGNOSTIC_CGR_LOG)

	two_ptrs ptrs;
	ptrs.p_axc = this;
	ptrs.p_other = (void*)&_ops; // use this to point to the object for characterisation

	// use same directory, laser_id and time_stamp as in _overall_mode_map
	_ops._results_dir_CString = _overall_mode_map._results_dir_CString;
	_ops._laser_id_CString = _overall_mode_map._laser_id_CString;
	_ops._date_time_stamp_CString = _overall_mode_map._date_time_stamp_CString;

	// kick off thread to gather data from a live laser
	// and produce screening results
	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::startLaserCharacterisation() kicking off new thread for characteriseProc"),DIAGNOSTIC_CGR_LOG)

	CWinThread* p_thread = AfxBeginThread( characteriseProc , (LPVOID)(&ptrs), THREAD_PRIORITY_HIGHEST );

	if( p_thread == NULL )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::startLaserCharacterisation() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_WORKER_THREAD_CREATION_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// the worker thread was not created
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_WORKER_THREAD_CREATION_ERROR )

		setWorkerThreadID( NULL );
	}
	else
	{		CString logmsg("CCLoseGridSystemAXCCtrl::startLaserCharacterisation() : ");
		logmsg.AppendFormat( "started new thread, thread ID = %d", p_thread->m_nThreadID );
		CGR_LOG(logmsg,DIAGNOSTIC_CGR_LOG)

		setWorkerThreadID( p_thread->m_nThreadID );
	}

	setSystemState( CCLoseGridSystemAXCCtrl::state_type::busy );

	// Allow new thread time to change system data
	Sleep(100);

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::startLaserCharacterisation() exiting"),DIAGNOSTIC_CGR_LOG)

	return;
}

void 
CCLoseGridSystemAXCCtrl::
getPercentComplete( short* p_percent_complete )
{
	if( getSystemState() != state_type::set_up
	 && getSystemState() != state_type::busy
	 && getSystemState() != state_type::complete )
	{
		// function cannot be called in the current system state
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR )
	}

	// Check for NULL pointer
	if( p_percent_complete == NULL )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::getPercentComplete() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_NULL_PTR_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		SERVERINTERFACE->closeLogFile();

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_NULL_PTR_ERROR )
	}

	if( getSystemState() == state_type::set_up )
	{
		*p_percent_complete = 0;
	}
	else
	if( getSystemState() == state_type::busy )
	{

		EnterCriticalSection(&_data_protect);

		*p_percent_complete = _percent_complete;

		LeaveCriticalSection(&_data_protect);

	}
	else
	if( getSystemState() == state_type::complete )
	{
		*p_percent_complete = 100;
	}

	return;
}

void 
CCLoseGridSystemAXCCtrl::
getState( 
	BSTR* p_state,
	short* p_overall_map_loaded,
	short* p_supermodes_found_count,
	short* p_supermode_map_loaded_count,
	short* p_ITUOP_estimate_count,
	short* p_ITUOP_count )
{
	// Check for NULL pointer
	if( p_state == NULL
	 || p_overall_map_loaded == NULL
	 || p_supermodes_found_count == NULL
	 || p_supermode_map_loaded_count == NULL
	 || p_ITUOP_estimate_count == NULL
	 || p_ITUOP_count == NULL )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::getState() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_NULL_PTR_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		SERVERINTERFACE->closeLogFile();

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_NULL_PTR_ERROR )
	}

	CString state_CString;

	state_type current_system_state = getSystemState();

	if( current_system_state == instantiated )
		state_CString = INSTANTIATED_STATE;
	else if( current_system_state == start_up )
		state_CString = STARTUP_STATE;
	else if( current_system_state == set_up )
		state_CString = SETUP_STATE;
	else if( current_system_state == busy )
		state_CString = BUSY_STATE;
	else if( current_system_state == complete )
		state_CString = COMPLETE_STATE;
	else
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::getState() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_UNKNOWN_STATE_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		SERVERINTERFACE->closeLogFile();

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_UNKNOWN_STATE_ERROR )
	}

	*p_overall_map_loaded = getOverallMapLoaded();
	*p_supermodes_found_count = getSMFoundCount();
	*p_supermode_map_loaded_count = getSMMapLoadedCount();
	*p_ITUOP_estimate_count = getEstimateCount();
	*p_ITUOP_count = getITUOPCount();

	*p_state = state_CString.AllocSysString();

	error_std_pair last_error = getLastError();
	
	if( last_error.first != error_std_pair(CGSYSTEM_SUCCESS).first )
	{
		// have an error from a non-blocking call, deal with it

		setLastError(error_std_pair(CGSYSTEM_SUCCESS));

		CGSYSTEM_THROW_EXCEPTION_FROM_PAIR(last_error)
	}

	return;
}

CCLoseGridSystemAXCCtrl::state_type
CCLoseGridSystemAXCCtrl::
getSystemState()
{
	EnterCriticalSection(&_data_protect);

	state_type current_system_state = _system_state;

	LeaveCriticalSection(&_data_protect);

	return current_system_state;
}


void 
CCLoseGridSystemAXCCtrl::
setSystemState( state_type new_system_state )
{
	EnterCriticalSection(&_data_protect);

	_system_state = new_system_state;

	if( new_system_state == state_type::busy )
		_percent_complete = 0;

	if( new_system_state == state_type::instantiated )
		_percent_complete = 0;

	if( new_system_state == state_type::set_up )
		_percent_complete = 0;

	if( new_system_state == state_type::start_up )
		_percent_complete = 0;

	if( new_system_state == state_type::complete )
		_percent_complete = 100;

	LeaveCriticalSection(&_data_protect);

	return;
}

void
CCLoseGridSystemAXCCtrl::
setPercentComplete( short percent_complete )
{
	EnterCriticalSection(&_data_protect);

	_percent_complete = percent_complete;

	LeaveCriticalSection(&_data_protect);

	return;
}


void 
CCLoseGridSystemAXCCtrl::
getScreeningOverallMapResults(
	BSTR* p_results_dir,
	BSTR* p_laser_type,
	BSTR* p_laser_id,
	BSTR* p_date_time_stamp,
	BSTR* p_map_ramp_direction,
	short* p_screening_passed,
	short* p_supermode_count,
	VARIANT* p_average_powers_on_middle_lines )
{
	if( getSystemState() != state_type::complete )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::getScreeningOverallMapResults() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// function cannot be called in the current system state
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR )
	}

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::getScreeningOverallMapResults() entered"),DIAGNOSTIC_CGR_LOG)

	// Check for NULL pointer
	if( p_results_dir == NULL 
	 || p_laser_type == NULL 
	 || p_laser_id == NULL 
	 || p_date_time_stamp == NULL 
	 || p_map_ramp_direction == NULL 
	 || p_screening_passed == NULL
	 || p_supermode_count == NULL
	 || p_average_powers_on_middle_lines == NULL )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::getScreeningOverallMapResults() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_NULL_PTR_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_NULL_PTR_ERROR )
	}

	std::vector<double> average_powers_on_middle_lines;
	CDSDBROverallModeMap::rcode get_avg_powers_rval = CDSDBROverallModeMap::rcode::ok;

	get_avg_powers_rval = _overall_mode_map.getAveragePowersOnMiddleLines( average_powers_on_middle_lines );

	if( get_avg_powers_rval == CDSDBROverallModeMapLine::rcode::ok )
	{
		rcode_type rval = vectorOfDoublesToVariantR8Array(
			&average_powers_on_middle_lines,
			p_average_powers_on_middle_lines );

		if( rval != rcode_type::ok )
		{
			CString log_err_msg("CCLoseGridSystemAXCCtrl::getScreeningOverallMapResults() error: ");
			log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_VARIANT_CREATION_ERROR) );
			CGR_LOG(log_err_msg,ERROR_CGR_LOG)

			CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_VARIANT_CREATION_ERROR )
		}
	}

	CString date_time_stamp_CString = getSystemDateTimeStamp();

	*p_results_dir = _overall_mode_map._results_dir_CString.AllocSysString();
	*p_date_time_stamp = _overall_mode_map._date_time_stamp_CString.AllocSysString();

	*p_screening_passed = getOMScreeningPassed();
	*p_supermode_count = getSMFoundCount();

	CString laser_type_CString("DSDBR01");
	*p_laser_type = laser_type_CString.AllocSysString();

	*p_laser_id = _overall_mode_map._laser_id_CString.AllocSysString();

	CString ramp_direction_CString;
	if( _overall_mode_map._Ir_ramp )
		ramp_direction_CString = RAMP_DIRECTION_REAR;
	else
		ramp_direction_CString = RAMP_DIRECTION_FRONT;
	*p_map_ramp_direction = ramp_direction_CString.AllocSysString();


	CString log_msg("CCLoseGridSystemAXCCtrl::getScreeningOverallMapResults() p_results_dir = ");
	log_msg += _overall_mode_map._results_dir_CString;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::getScreeningOverallMapResults() p_laser_type = ");
	log_msg += laser_type_CString;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::getScreeningOverallMapResults() p_laser_id = ");
	log_msg += _overall_mode_map._laser_id_CString;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::getScreeningOverallMapResults() p_date_time_stamp = ");
	log_msg += _overall_mode_map._date_time_stamp_CString;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::getScreeningOverallMapResults() p_map_ramp_direction = ");
	log_msg += ramp_direction_CString;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::getScreeningOverallMapResults() p_screening_passed = ");
	log_msg.AppendFormat("%d",*p_screening_passed);
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::getScreeningOverallMapResults() p_supermode_count = ");
	log_msg.AppendFormat("%d",*p_supermode_count);
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::getScreeningOverallMapResults() exiting"),DIAGNOSTIC_CGR_LOG)

	SERVERINTERFACE->closeLogFile();

	return;
}

void 
CCLoseGridSystemAXCCtrl::
getScreeningSMMapResults(
	short supermode_number,
	BSTR* p_results_dir,
	BSTR* p_laser_type,
	BSTR* p_laser_id,
	BSTR* p_date_time_stamp,
	BSTR* p_map_ramp_direction,
	short* p_screening_sm_map_passed,
	short* p_longitudinal_mode_count )
{
	if( getSystemState() != state_type::complete )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::getScreeningSMMapResults() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// function cannot be called in the current system state
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR )
	}

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::getScreeningSMMapResults() entered"),DIAGNOSTIC_CGR_LOG)
	CString log_msg("CCLoseGridSystemAXCCtrl::setCurrent() supermode_number = ");
	log_msg.AppendFormat("%d",supermode_number);
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	// Check for NULL pointer
	if( p_results_dir == NULL 
	 || p_laser_type == NULL 
	 || p_laser_id == NULL 
	 || p_date_time_stamp == NULL 
	 || p_map_ramp_direction == NULL 
	 || p_screening_sm_map_passed == NULL
	 || p_longitudinal_mode_count == NULL )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::getScreeningSMMapResults() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_NULL_PTR_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_NULL_PTR_ERROR )
	}

	if( ( supermode_number > (getSMFoundCount() - 1) )
	 || ( supermode_number < 0 ) )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::getScreeningSMMapResults() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_SPECIFIED_SUPERMODE_NOT_FOUND) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// supermode number requested is not available
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_SPECIFIED_SUPERMODE_NOT_FOUND )
	}

	if( !(_overall_mode_map._supermodes[supermode_number]._maps_loaded) )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::getScreeningSMMapResults() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_SPECIFIED_SUPERMODE_NOT_LOADED) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// supermode number requested is not loaded
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_SPECIFIED_SUPERMODE_NOT_LOADED )
	}

	// get results dir, laser_id and date_time_stamp


	*p_results_dir = _overall_mode_map._supermodes[supermode_number]._results_dir_CString.AllocSysString();

	CString laser_type_CString("DSDBR01");
	*p_laser_type = laser_type_CString.AllocSysString();

	*p_laser_id = _overall_mode_map._supermodes[supermode_number]._laser_id_CString.AllocSysString();

	*p_date_time_stamp = _overall_mode_map._supermodes[supermode_number]._date_time_stamp_CString.AllocSysString();

	CString ramp_direction_CString;
	if( _overall_mode_map._supermodes[supermode_number]._Im_ramp )
		ramp_direction_CString = RAMP_DIRECTION_MIDDLE_LINE;
	else
		ramp_direction_CString = RAMP_DIRECTION_PHASE;	
	*p_map_ramp_direction = ramp_direction_CString.AllocSysString();

	*p_longitudinal_mode_count = _overall_mode_map._supermodes[supermode_number].getLMCount();

	*p_screening_sm_map_passed = getSMScreeningPassed( supermode_number );

	log_msg = CString("CCLoseGridSystemAXCCtrl::getScreeningSMMapResults() p_results_dir = ");
	log_msg += _overall_mode_map._supermodes[supermode_number]._results_dir_CString;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::getScreeningSMMapResults() p_laser_type = ");
	log_msg += laser_type_CString;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::getScreeningSMMapResults() p_laser_id = ");
	log_msg += _overall_mode_map._supermodes[supermode_number]._laser_id_CString;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::getScreeningSMMapResults() p_date_time_stamp = ");
	log_msg += _overall_mode_map._supermodes[supermode_number]._date_time_stamp_CString;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::getScreeningSMMapResults() p_map_ramp_direction = ");
	log_msg += ramp_direction_CString;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::getScreeningSMMapResults() p_screening_sm_map_passed = ");
	log_msg.AppendFormat("%d",*p_screening_sm_map_passed);
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::getScreeningSMMapResults() p_longitudinal_mode_count = ");
	log_msg.AppendFormat("%d",*p_longitudinal_mode_count);
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::getScreeningSMMapResults() exiting"),DIAGNOSTIC_CGR_LOG)

	SERVERINTERFACE->closeLogFile();

	return;
}


void 
CCLoseGridSystemAXCCtrl::
getCharacterisationResults(
	BSTR* p_results_dir,
	BSTR* p_date_time_stamp,
	short* p_ITUOP_count,
	short* p_duff_ITUOP_count )
{
	if( getSystemState() != state_type::complete )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::getCharacterisationResults() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// function cannot be called in the current system state
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR )
	}

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::getCharacterisationResults() entered"),DIAGNOSTIC_CGR_LOG)

	// Check for NULL pointer
	if( p_results_dir == NULL 
	 || p_date_time_stamp == NULL 
	 || p_ITUOP_count == NULL
	 || p_duff_ITUOP_count == NULL )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::getCharacterisationResults() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_NULL_PTR_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_NULL_PTR_ERROR )
	}

	*p_results_dir = _ops._results_dir_CString.AllocSysString();
	*p_date_time_stamp = _ops._date_time_stamp_CString.AllocSysString();
	*p_ITUOP_count = getITUOPCount();
	*p_duff_ITUOP_count = getDuffITUOPCount();


	CString log_msg("CCLoseGridSystemAXCCtrl::getCharacterisationResults() p_results_dir = ");
	log_msg += _ops._results_dir_CString;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::getCharacterisationResults() p_date_time_stamp = ");
	log_msg += _ops._date_time_stamp_CString;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::getCharacterisationResults() p_ITUOP_count = ");
	log_msg.AppendFormat("%d",*p_ITUOP_count);
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::getCharacterisationResults() p_duff_ITUOP_count = ");
	log_msg.AppendFormat("%d",*p_duff_ITUOP_count);
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::getCharacterisationResults() exiting"),DIAGNOSTIC_CGR_LOG)

	SERVERINTERFACE->closeLogFile();

	return;
}

void 
CCLoseGridSystemAXCCtrl::
collatePassFailResults(
	BSTR* p_results_dir,
	BSTR* p_date_time_stamp,
	short* p_screening_overall_map_passed,
	VARIANT* p_screening_sm_maps_passed,
	short* p_total_passed )
{

	if( getSystemState() != state_type::complete )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::collatePassFailResults() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// function cannot be called in the current system state
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR )
	}

	// Check for NULL pointer
	if( p_results_dir == NULL 
	 || p_date_time_stamp == NULL 
	 || p_screening_overall_map_passed == NULL 
	 || p_screening_sm_maps_passed == NULL 
	 || p_total_passed == NULL )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::collatePassFailResults() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_NULL_PTR_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_NULL_PTR_ERROR )
	}

	/////////////////////////////////////////////////////////////////////////////////

	PassFailCollated::instance()->writeResultsToFile();

	/////////////////////////////////////////////////////////////////////////////////

	std::vector<short> supermode_map_screening_passed;

	for( short sm_number = 0 ; sm_number < getSMFoundCount() ; sm_number ++ )
	{
		supermode_map_screening_passed.push_back( getSMScreeningPassed( sm_number ) );
	}

	rcode_type rval = vectorOfShortsToVariantI2Array(
		&supermode_map_screening_passed,
		p_screening_sm_maps_passed );

	//////////////////////////////////////////////////////////////////////////////////


	*p_screening_overall_map_passed = getOMScreeningPassed();


	//////////////////////////////////////////////////////////////////////////////////

	if( PassFailCollated::instance()->_totalPassed )
		*p_total_passed = TRUE;
	else
		*p_total_passed = FALSE;

	//////////////////////////////////////////////////////////////////////////////////


	if( rval != rcode_type::ok )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::collatePassFailResults() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_VARIANT_CREATION_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_VARIANT_CREATION_ERROR )
	}


	*p_results_dir = _overall_mode_map._results_dir_CString.AllocSysString();
	*p_date_time_stamp = _overall_mode_map._date_time_stamp_CString.AllocSysString();




	CString LMmiddleFreqCurrents_abs_filepath = _overall_mode_map._results_dir_CString;
	if(LMmiddleFreqCurrents_abs_filepath.Right(1) != _T("\\") )
		LMmiddleFreqCurrents_abs_filepath += _T("\\");
	LMmiddleFreqCurrents_abs_filepath += ("LMMiddleFreqCurrents_");
	LMmiddleFreqCurrents_abs_filepath += LASER_TYPE_ID_DSDBR01_CSTRING;
	LMmiddleFreqCurrents_abs_filepath += USCORE;
	LMmiddleFreqCurrents_abs_filepath += _laser_id_CString;
	LMmiddleFreqCurrents_abs_filepath += USCORE;
	LMmiddleFreqCurrents_abs_filepath += _date_time_stamp_CString;
	LMmiddleFreqCurrents_abs_filepath += CSV;

	CDSDBROverallModeMap::rcode rvl = _overall_mode_map.writeLMMiddleFreqCurrentsToFile(
		LMmiddleFreqCurrents_abs_filepath,
		CG_REG->get_DSDBR01_OMDC_overwrite_existing_files() );

	if( rvl != CDSDBROverallModeMap::rcode::ok )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::collatePassFailResults() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_ERROR_WRITING_RESULTS_TO_FILE) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_ERROR_WRITING_RESULTS_TO_FILE )
	}



	CString log_msg("CCLoseGridSystemAXCCtrl::collatePassFailResults() p_results_dir = ");
	log_msg += _overall_mode_map._results_dir_CString;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::collatePassFailResults() p_date_time_stamp = ");
	log_msg += _overall_mode_map._date_time_stamp_CString;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::collatePassFailResults() p_screening_overall_map_passed = ");
	log_msg.AppendFormat("%d",*p_screening_overall_map_passed);
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::collatePassFailResults() exiting"),DIAGNOSTIC_CGR_LOG)

	SERVERINTERFACE->closeLogFile();

	return;
}


void 
CCLoseGridSystemAXCCtrl::
loadOverallMap(
	LPCTSTR results_dir,
	LPCTSTR laser_type,
	LPCTSTR laser_id,
	LPCTSTR date_time_stamp,
	LPCTSTR map_ramp_direction )
{
	//CMemoryState msOld;
	//msOld.Checkpoint();

	//_CrtMemState s1, s2, s3;

	//_CrtMemCheckpoint( &s1 );

	if( getSystemState() != state_type::set_up
	 && getSystemState() != state_type::complete )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::loadOverallMap() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// function cannot be called in the current system state
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR )
	}

	// Check for NULL pointer
	if( results_dir == NULL
	 || laser_type == NULL
	 || laser_id == NULL
	 || date_time_stamp == NULL
	 || map_ramp_direction == NULL )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::loadOverallMap() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_NULL_PTR_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_NULL_PTR_ERROR )
	}

	CString results_dir_CString( results_dir );
	CString laser_type_CString( laser_type );
	CString laser_id_CString( laser_id );
	CString date_time_stamp_CString( date_time_stamp );
	CString map_ramp_direction_CString( map_ramp_direction );


	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::loadOverallMap() entered"),DIAGNOSTIC_CGR_LOG)
	CString log_msg("CCLoseGridSystemAXCCtrl::loadOverallMap() results_dir = ");
	log_msg += results_dir_CString;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::loadOverallMap() laser_type = ");
	log_msg += laser_type_CString;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::loadOverallMap() laser_id = ");
	log_msg += laser_id_CString;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::loadOverallMap() date_time_stamp = ");
	log_msg += date_time_stamp_CString;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::loadOverallMap() map_ramp_direction = ");
	log_msg += map_ramp_direction_CString;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	// test strings are not empty
	if( results_dir_CString.GetLength() <= 0
	 || laser_type_CString.GetLength() <= 0
	 || laser_id_CString.GetLength() <= 0
	 || date_time_stamp_CString.GetLength() <= 0
	 || map_ramp_direction_CString.GetLength() <= 0 )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::loadOverallMap() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_EMPTY_STRING_ARG_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_EMPTY_STRING_ARG_ERROR )
	}

	// test laser_type is valid
	if( !validLaserType( laser_type_CString ) )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::loadOverallMap() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_LASERTYPE_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_LASERTYPE_ERROR )
	}

	// test laser_id is valid
	if( !validLaserID( laser_id_CString ) )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::loadOverallMap() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_LASER_ID_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_LASER_ID_ERROR )
	}

	// test date time stamp is of correct format
	if( !validDateTimeStamp( date_time_stamp_CString ) )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::loadOverallMap() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_DATETIMESTAMP_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_DATETIMESTAMP_ERROR )
	}

	// test results directory is valid
	if( !validDirectoryPath( results_dir_CString ) )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::loadOverallMap() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_DIRECTORY_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_DIRECTORY_ERROR )
	}

	if( map_ramp_direction_CString != RAMP_DIRECTION_REAR
	 && map_ramp_direction_CString != RAMP_DIRECTION_FRONT )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::loadOverallMap() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_INVALID_MAP_RAMP_DIRECTION_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_INVALID_MAP_RAMP_DIRECTION_ERROR )
	}

	if( laser_type_CString == LASER_TYPE_ID_DSDBR01_CSTRING )
	{
		// laser type is DSDBR01
		// load and screen DSDBR01 laser data

		_overall_mode_map.clear();
		setOverallMapLoaded( FALSE );
		setSMFoundCount( 0 );
		setSMMapLoadedCount( 0 );
		setEstimateCount( 0 );
		setITUOPCount( 0 );
		setDuffITUOPCount( 0 );

		CDSDBROverallModeMap::rcode load_from_files_rval = CDSDBROverallModeMap::rcode::ok;

		// load overall map from files

		load_from_files_rval = _overall_mode_map.loadMapsFromFile( 
			results_dir_CString,
			laser_id_CString,
			date_time_stamp_CString,
			map_ramp_direction_CString );


		if( load_from_files_rval != CDSDBROverallModeMap::rcode::ok )
		{
			CString log_err_msg("CCLoseGridSystemAXCCtrl::loadOverallMap() error: ");
			log_err_msg += CString(_overall_mode_map.get_error_pair( load_from_files_rval ).second.c_str());
			CGR_LOG(log_err_msg,ERROR_CGR_LOG)

			CGSYSTEM_THROW_EXCEPTION_FROM_PAIR( _overall_mode_map.get_error_pair( load_from_files_rval ) )
		}

		// got this far without throwing an exception => all okay
		setOverallMapLoaded( TRUE );

		// reset results dir, laser_id and date_time_stamp
		_overall_mode_map._results_dir_CString = CG_REG->get_DSDBR01_BASE_results_directory();
		_overall_mode_map._laser_id_CString = getSystemLaserID();
		_overall_mode_map._date_time_stamp_CString = getSystemDateTimeStamp();

		CDSDBROverallModeMap::rcode rval_writedata = CDSDBROverallModeMap::rcode::ok;

		if( load_from_files_rval == CDSDBROverallModeMap::rcode::ok )
		{
			// write data to files
			rval_writedata = _overall_mode_map.writeMapsToFile();
		}

		CDSDBROverallModeMap::rcode screening_rval = CDSDBROverallModeMap::rcode::ok;

		if( load_from_files_rval == CDSDBROverallModeMap::rcode::ok )
		{
			// okay so far,
			// screen the data


			screening_rval = _overall_mode_map.screen();

		}


		if( screening_rval == CDSDBROverallModeMap::rcode::ok )
		{

			// okay so far,
			if( _overall_mode_map._overallModeMapAnalysis._screeningPassed )
				setOMScreeningPassed( TRUE );
			else
				setOMScreeningPassed( FALSE );

			// write screening results to file

			CDSDBROverallModeMap::rcode write_rval = CDSDBROverallModeMap::rcode::ok;

			write_rval = _overall_mode_map.writeScreeningResultsToFile();

			short num_of_supermodes = _overall_mode_map.supermode_count();
			if( !(num_of_supermodes > 0) )
			{
				setSMFoundCount( 0 );

				CString log_err_msg("CCLoseGridSystemAXCCtrl::loadOverallMap() error: ");
				log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_NO_VALID_SUPERMODES_FOUND) );
				CGR_LOG(log_err_msg,ERROR_CGR_LOG)

				CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_NO_VALID_SUPERMODES_FOUND )
			}
			else
			{
				setSMFoundCount( num_of_supermodes );
			}

			if( write_rval != CDSDBROverallModeMap::rcode::ok )
			{
				CString log_err_msg("CCLoseGridSystemAXCCtrl::loadOverallMap() error: ");
				log_err_msg += CString(_overall_mode_map.get_error_pair( write_rval ).second.c_str());
				CGR_LOG(log_err_msg,ERROR_CGR_LOG)

				CGSYSTEM_THROW_EXCEPTION_FROM_PAIR( _overall_mode_map.get_error_pair( write_rval ) )
			}
		}

		setSystemState( CCLoseGridSystemAXCCtrl::state_type::complete );

		if( screening_rval != CDSDBROverallModeMap::rcode::ok )
		{
			CString log_err_msg("CCLoseGridSystemAXCCtrl::loadOverallMap() error: ");
			log_err_msg += CString(_overall_mode_map.get_error_pair( screening_rval ).second.c_str());
			CGR_LOG(log_err_msg,ERROR_CGR_LOG)

			CGSYSTEM_THROW_EXCEPTION_FROM_PAIR( _overall_mode_map.get_error_pair( screening_rval ) )
		}


		if( rval_writedata != CDSDBROverallModeMap::rcode::ok )
		{
			CString log_err_msg("CCLoseGridSystemAXCCtrl::loadOverallMap() error: ");
			log_err_msg += CString(_overall_mode_map.get_error_pair( rval_writedata ).second.c_str());
			CGR_LOG(log_err_msg,ERROR_CGR_LOG)

			CGSYSTEM_THROW_EXCEPTION_FROM_PAIR( _overall_mode_map.get_error_pair( rval_writedata ) )
		}

	}

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::loadOverallMap() exiting"),DIAGNOSTIC_CGR_LOG)

	//_CrtMemCheckpoint( &s2 );

	//if ( _CrtMemDifference( &s3, &s1, &s2) )
	//	_CrtMemDumpStatistics( &s3 );

	//msOld.DumpAllObjectsSince();

	SERVERINTERFACE->closeLogFile();

	return;
}


void 
CCLoseGridSystemAXCCtrl::
loadSMMap(
	LPCTSTR results_dir,
	LPCTSTR laser_type,
	LPCTSTR laser_id,
	LPCTSTR date_time_stamp,
	short supermode_number,
	LPCTSTR map_ramp_direction )
{
	if( getSystemState() != state_type::complete )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::loadSMMap() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// function cannot be called in the current system state
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR )
	}

	// Check for NULL pointer
	if( results_dir == NULL
	 || laser_type == NULL
	 || laser_id == NULL
	 || date_time_stamp == NULL
	 || map_ramp_direction == NULL )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::loadSMMap() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_NULL_PTR_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_NULL_PTR_ERROR )
	}

	CString results_dir_CString( results_dir );
	CString laser_type_CString( laser_type );
	CString laser_id_CString( laser_id );
	CString date_time_stamp_CString( date_time_stamp );
	CString map_ramp_direction_CString( map_ramp_direction );


	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::loadSMMap() entered"),DIAGNOSTIC_CGR_LOG)
	CString log_msg("CCLoseGridSystemAXCCtrl::loadSMMap() results_dir = ");
	log_msg += results_dir_CString;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::loadSMMap() laser_type = ");
	log_msg += laser_type_CString;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::loadSMMap() laser_id = ");
	log_msg += laser_id_CString;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::loadSMMap() date_time_stamp = ");
	log_msg += date_time_stamp_CString;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::loadSMMap() supermode_number = ");
	log_msg.AppendFormat("%d",supermode_number);
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::loadSMMap() map_ramp_direction = ");
	log_msg += map_ramp_direction_CString;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	// test strings are not empty
	if( results_dir_CString.GetLength() <= 0
	 || laser_type_CString.GetLength() <= 0
	 || laser_id_CString.GetLength() <= 0
	 || date_time_stamp_CString.GetLength() <= 0
	 || map_ramp_direction_CString.GetLength() <= 0 )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::loadSMMap() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_EMPTY_STRING_ARG_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_EMPTY_STRING_ARG_ERROR )
	}

	// test laser_type is valid
	if( !validLaserType( laser_type_CString ) )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::loadSMMap() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_LASERTYPE_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_LASERTYPE_ERROR )
	}

	// test laser_id is valid
	if( !validLaserID( laser_id_CString ) )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::loadSMMap() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_LASER_ID_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_LASER_ID_ERROR )
	}

	// test date time stamp is of correct format
	if( !validDateTimeStamp( date_time_stamp_CString ) )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::loadSMMap() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_DATETIMESTAMP_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_DATETIMESTAMP_ERROR )
	}

	// test results directory is valid
	if( !validDirectoryPath( results_dir_CString ) )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::loadSMMap() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_DIRECTORY_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_DIRECTORY_ERROR )
	}

	if( map_ramp_direction_CString != RAMP_DIRECTION_PHASE
	 && map_ramp_direction_CString != RAMP_DIRECTION_MIDDLE_LINE )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::loadSMMap() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_INVALID_MAP_RAMP_DIRECTION_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_INVALID_MAP_RAMP_DIRECTION_ERROR )
	}

	if( ( supermode_number > (getSMFoundCount() - 1) )
	 || ( supermode_number < 0 ) )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::loadSMMap() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_SPECIFIED_SUPERMODE_NOT_FOUND) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// supermode number requested is not available
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_SPECIFIED_SUPERMODE_NOT_FOUND )
	}

	CDSDBRSuperMode::rcode load_from_files_rval = CDSDBRSuperMode::rcode::ok;

	if( laser_type_CString == LASER_TYPE_ID_DSDBR01_CSTRING )
	{
		// laser type is DSDBR01
		// load and screen DSDBR01 laser supermode data

		_overall_mode_map._supermodes[supermode_number].clearMapsAndLines();
		_overall_mode_map._supermodes[supermode_number]._superModeMapAnalysis.init();

		// load overall map from files

		load_from_files_rval = _overall_mode_map._supermodes[supermode_number].loadMapsFromFile( 
			results_dir_CString,
			laser_id_CString,
			date_time_stamp_CString,
			map_ramp_direction_CString );


		if( load_from_files_rval != CDSDBRSuperMode::rcode::ok )
		{
			CString log_err_msg("CCLoseGridSystemAXCCtrl::loadSMMap() error: ");
			log_err_msg += CString(_overall_mode_map._supermodes[supermode_number].get_error_pair( load_from_files_rval ).second.c_str());
			CGR_LOG(log_err_msg,ERROR_CGR_LOG)

			CGSYSTEM_THROW_EXCEPTION_FROM_PAIR( _overall_mode_map._supermodes[supermode_number].get_error_pair( load_from_files_rval ) )
		}

	}

	short sm_map_loaded_count = 0;
	for( int i = 0; i < (int)(_overall_mode_map._supermodes.size()) ; i++ )
	{
		if( _overall_mode_map._supermodes[i]._maps_loaded )
			sm_map_loaded_count++;
	}
	setSMMapLoadedCount( sm_map_loaded_count );


	// reset results dir, laser_id and date_time_stamp
	_overall_mode_map._supermodes[supermode_number]._results_dir_CString = CG_REG->get_DSDBR01_BASE_results_directory();
	_overall_mode_map._supermodes[supermode_number]._laser_id_CString = getSystemLaserID();
	_overall_mode_map._supermodes[supermode_number]._date_time_stamp_CString = getSystemDateTimeStamp();


	CDSDBRSuperMode::rcode rval_writedata = CDSDBRSuperMode::rcode::ok;

	if( load_from_files_rval == CDSDBRSuperMode::rcode::ok )
	{
		// write data to files
		rval_writedata = _overall_mode_map._supermodes[supermode_number].writeMapsToFile();
	}

	CDSDBRSuperMode::rcode screening_rval = CDSDBRSuperMode::rcode::ok;
	short longitudinal_mode_count = 0;

	if( load_from_files_rval == CDSDBRSuperMode::rcode::ok )
	{
		// okay so far,
		// screen the data

		screening_rval = _overall_mode_map._supermodes[supermode_number].screen();

		if( _overall_mode_map._supermodes[supermode_number]._superModeMapAnalysis._screeningPassed )
			setSMScreeningPassed( supermode_number, TRUE );
		else
			setSMScreeningPassed( supermode_number, FALSE );
	}

	if( screening_rval != CDSDBRSuperMode::rcode::ok )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::loadSMMap() error: ");
		log_err_msg += CString(_overall_mode_map._supermodes[supermode_number].get_error_pair( screening_rval ).second.c_str());
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION_FROM_PAIR( _overall_mode_map._supermodes[supermode_number].get_error_pair( screening_rval ) )
	}

	// okay so far,
	// write screening results to file

	CDSDBRSuperMode::rcode write_rval = CDSDBRSuperMode::rcode::ok;

	write_rval = _overall_mode_map._supermodes[supermode_number].writeScreeningResultsToFile();

	if( write_rval != CDSDBRSuperMode::rcode::ok )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::loadSMMap() error: ");
		log_err_msg += CString(_overall_mode_map._supermodes[supermode_number].get_error_pair( write_rval ).second.c_str());
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION_FROM_PAIR( _overall_mode_map._supermodes[supermode_number].get_error_pair( write_rval ) )
	}

	longitudinal_mode_count = _overall_mode_map._supermodes[supermode_number].getLMCount();

	if( !(longitudinal_mode_count > 0) )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::loadSMMap() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_NO_VALID_LONGITUDINAL_MODES_FOUND) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_NO_VALID_LONGITUDINAL_MODES_FOUND )
	}

	setSystemState( state_type::complete );

	if( rval_writedata != CDSDBRSuperMode::rcode::ok )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::loadSMMap() error: ");
		log_err_msg += CString(_overall_mode_map._supermodes[supermode_number].get_error_pair( rval_writedata ).second.c_str());
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION_FROM_PAIR( _overall_mode_map._supermodes[supermode_number].get_error_pair( rval_writedata ) )
	}

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::loadSMMap() exiting"),DIAGNOSTIC_CGR_LOG)

	SERVERINTERFACE->closeLogFile();

	return;
}


void 
CCLoseGridSystemAXCCtrl::
readVoltage(
	LPCTSTR module_name,
	double* p_voltage )
{
	if( getSystemState() != state_type::set_up
	 && getSystemState() != state_type::complete )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::readVoltage() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// function cannot be called in the current system state
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR )
	}

	// Check for NULL pointer
	if( module_name == NULL 
	 || p_voltage == NULL )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::readVoltage() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_NULL_PTR_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_NULL_PTR_ERROR )
	}

	CString module_name_CString( module_name );

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::readVoltage() entered"),DIAGNOSTIC_CGR_LOG)
	CString log_msg("CCLoseGridSystemAXCCtrl::readVoltage() module_name = ");
	log_msg += module_name_CString;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	// check the incoming module_name is okay
	if( !validModuleName( module_name_CString ) )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::readVoltage() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_INVALID_MODULE_NAME_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_INVALID_MODULE_NAME_ERROR )
	}

	HRESULT hres = S_OK;

	if( !CG_REG->get_DSDBR01_BASE_stub_hardware_calls() )
	{
		// read Voltage from 305 module 
		hres = SERVERINTERFACE->readVoltage( module_name_CString, p_voltage );
	}

	if( FAILED(hres) )
	{
		handleCGResearchError( _T("CCLoseGridSystemAXCCtrl::readVoltage()"), hres);
	}

	log_msg = CString("CCLoseGridSystemAXCCtrl::readVoltage() p_voltage = ");
	log_msg.AppendFormat("%g",*p_voltage);
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::readVoltage() exiting"),DIAGNOSTIC_CGR_LOG)

	SERVERINTERFACE->closeLogFile();

	return;
}

void 
CCLoseGridSystemAXCCtrl::
readFrequency(
	double* p_frequency )
{
	if( getSystemState() != state_type::start_up
	 && getSystemState() != state_type::set_up
	 && getSystemState() != state_type::complete )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::readFrequency() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// function cannot be called in the current system state
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR )
	}


	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::readFrequency() entered"),DIAGNOSTIC_CGR_LOG)

	// Check for NULL pointer
	if( p_frequency == NULL )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::readFrequency() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_NULL_PTR_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_NULL_PTR_ERROR )
	}

	HRESULT hres = S_OK;

	if( !CG_REG->get_DSDBR01_BASE_stub_hardware_calls() )
	{
		// read Frequency from Wavemeter
		hres = SERVERINTERFACE->readFrequency( p_frequency );
	}

	if( FAILED(hres) )
	{
		handleCGResearchError( _T("CCLoseGridSystemAXCCtrl::readFrequency()"), hres);
	}

	if( *p_frequency < MIN_VALID_FREQ_MEAS
	 || *p_frequency > MAX_VALID_FREQ_MEAS )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::readFrequency() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_WAVEMETER_MEASUREMENT_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// function cannot be called in the current system state
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_WAVEMETER_MEASUREMENT_ERROR )
	}


	CString log_msg("CCLoseGridSystemAXCCtrl::readFrequency() p_frequency = ");
	log_msg.AppendFormat("%g",*p_frequency);
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::readFrequency() exiting"),DIAGNOSTIC_CGR_LOG)

	SERVERINTERFACE->closeLogFile();

	return;
}


void 
CCLoseGridSystemAXCCtrl::
readCoarseFrequency(
	double* p_coarse_frequency )
{
	if( getSystemState() != state_type::start_up
	 && getSystemState() != state_type::set_up
	 && getSystemState() != state_type::complete )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::readCoarseFrequency() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// function cannot be called in the current system state
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR )
	}

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::readCoarseFrequency() entered"),DIAGNOSTIC_CGR_LOG)

	// Check for NULL pointer
	if( p_coarse_frequency == NULL )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::readCoarseFrequency() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_NULL_PTR_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_NULL_PTR_ERROR )
	}

	HRESULT hres = S_OK;

	if( !CG_REG->get_DSDBR01_BASE_stub_hardware_calls() )
	{
		// get the coarse frequency from power ratio using a polyomial fit
		CCGOperatingPoints::rtype rval_ops = _ops.readCoarseFreq( *p_coarse_frequency );

		if( rval_ops != CCGOperatingPoints::rtype::ok )
		{
			CString log_err_msg("CCLoseGridSystemAXCCtrl::readCoarseFrequency() error: ");
			log_err_msg += CString(_ops.get_error_pair( rval_ops ).second.c_str());
			CGR_LOG(log_err_msg,ERROR_CGR_LOG)

			CGSYSTEM_THROW_EXCEPTION_FROM_PAIR( _ops.get_error_pair( rval_ops ) )
		}

	}

	if( FAILED(hres) )
	{
		handleCGResearchError( _T("CCLoseGridSystemAXCCtrl::readCoarseFrequency()"), hres);
	}


	CString log_msg("CCLoseGridSystemAXCCtrl::readCoarseFrequency() p_coarse_frequency = ");
	log_msg.AppendFormat("%g",*p_coarse_frequency);
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::readCoarseFrequency() exiting"),DIAGNOSTIC_CGR_LOG)

	SERVERINTERFACE->closeLogFile();

	return;
}

void 
CCLoseGridSystemAXCCtrl::
readPhotodiodeCurrent(
	LPCTSTR module_name,
	short channel,
	double* p_current )
{
	if( getSystemState() != state_type::start_up
	 && getSystemState() != state_type::set_up
	 && getSystemState() != state_type::complete )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::readPhotodiodeCurrent() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// function cannot be called in the current system state
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR )
	}

	// Check for NULL pointer
	if( module_name == NULL 
	 || p_current == NULL )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::readPhotodiodeCurrent() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_NULL_PTR_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_NULL_PTR_ERROR )
	}

	CString module_name_CString( module_name );

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::readPhotodiodeCurrent() entered"),DIAGNOSTIC_CGR_LOG)
	CString log_msg("CCLoseGridSystemAXCCtrl::readPhotodiodeCurrent() module_name = ");
	log_msg += module_name_CString;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::readPhotodiodeCurrent() channel = ");
	log_msg.AppendFormat("%d",channel);
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)



	// check the incoming module_name is okay
	if( !validModuleName( module_name_CString ) )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::readPhotodiodeCurrent() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_INVALID_MODULE_NAME_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_INVALID_MODULE_NAME_ERROR )
	}

	if( channel != CHANNEL_1_ON_306 
	 && channel != CHANNEL_2_ON_306 )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::readPhotodiodeCurrent() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_INVALID_CHANNEL_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_INVALID_CHANNEL_ERROR )
	}


	HRESULT hres = S_OK;

	if( !CG_REG->get_DSDBR01_BASE_stub_hardware_calls() )
	{
		hres = SERVERINTERFACE->readPhotodiodeCurrent( module_name_CString, channel, p_current );
	}

	if( FAILED(hres) )
	{
		handleCGResearchError( _T("CCLoseGridSystemAXCCtrl::readPhotodiodeCurrent()"), hres);
	}

	log_msg = CString("CCLoseGridSystemAXCCtrl::readPhotodiodeCurrent() p_current = ");
	log_msg.AppendFormat("%g",*p_current);
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::readPhotodiodeCurrent() exiting"),DIAGNOSTIC_CGR_LOG)

	SERVERINTERFACE->closeLogFile();

	return;
}

void 
CCLoseGridSystemAXCCtrl::
readOpticalPower(
	LPCTSTR module_name,
	short channel,
	double* p_optical_power )
{
	if( getSystemState() != state_type::start_up
	 && getSystemState() != state_type::set_up
	 && getSystemState() != state_type::complete )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::readOpticalPower() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// function cannot be called in the current system state
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR )
	}

	// Check for NULL pointer
	if( module_name == NULL 
	 || p_optical_power == NULL )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::readOpticalPower() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_NULL_PTR_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_NULL_PTR_ERROR )
	}

	CString module_name_CString( module_name );


	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::readOpticalPower() entered"),DIAGNOSTIC_CGR_LOG)
	CString log_msg("CCLoseGridSystemAXCCtrl::readOpticalPower() module_name = ");
	log_msg += module_name_CString;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::readOpticalPower() channel = ");
	log_msg.AppendFormat("%d",channel);
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)


	// check the incoming module_name is okay
	if( !validModuleName( module_name_CString ) )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::readOpticalPower() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_INVALID_MODULE_NAME_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_INVALID_MODULE_NAME_ERROR )
	}

	if( channel != CHANNEL_1_ON_306 
	 && channel != CHANNEL_2_ON_306 )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::readOpticalPower() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_INVALID_CHANNEL_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_INVALID_CHANNEL_ERROR )
	}


	HRESULT hres = S_OK;

	if( !CG_REG->get_DSDBR01_BASE_stub_hardware_calls() )
	{
		// read Optical Power from 306 module 
		hres = SERVERINTERFACE->readOpticalPower( module_name_CString, channel, p_optical_power );
	}

	if( FAILED(hres) )
	{
		handleCGResearchError( _T("CCLoseGridSystemAXCCtrl::readOpticalPower()"), hres);
	}


	log_msg = CString("CCLoseGridSystemAXCCtrl::readOpticalPower() p_optical_power = ");
	log_msg.AppendFormat("%g",*p_optical_power);
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::readOpticalPower() exiting"),DIAGNOSTIC_CGR_LOG)

	SERVERINTERFACE->closeLogFile();

	return;
}

void 
CCLoseGridSystemAXCCtrl::
readOpticalPowerAtFreq(
	LPCTSTR module_name,
	short channel,
	double frequency,
	double* p_optical_power )
{
	if( getSystemState() != state_type::start_up
	 && getSystemState() != state_type::set_up
	 && getSystemState() != state_type::complete )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::readOpticalPowerAtFreq() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// function cannot be called in the current system state
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR )
	}

	// Check for NULL pointer
	if( module_name == NULL 
	 || p_optical_power == NULL )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::readOpticalPowerAtFreq() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_NULL_PTR_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_NULL_PTR_ERROR )
	}

	CString module_name_CString( module_name );


	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::readOpticalPowerAtFreq() entered"),DIAGNOSTIC_CGR_LOG)
	CString log_msg("CCLoseGridSystemAXCCtrl::readOpticalPowerAtFreq() module_name = ");
	log_msg += module_name_CString;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::readOpticalPowerAtFreq() channel = ");
	log_msg.AppendFormat("%d",channel);
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::readOpticalPowerAtFreq() frequency = ");
	log_msg.AppendFormat("%g",frequency);
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)


	// check the incoming module_name is okay
	if( !validModuleName( module_name_CString ) )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::readOpticalPowerAtFreq() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_INVALID_MODULE_NAME_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_INVALID_MODULE_NAME_ERROR )
	}

	if( channel != CHANNEL_1_ON_306 
	 && channel != CHANNEL_2_ON_306 )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::readOpticalPowerAtFreq() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_INVALID_CHANNEL_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_INVALID_CHANNEL_ERROR )
	}

	HRESULT hres = S_OK;

	if( !CG_REG->get_DSDBR01_BASE_stub_hardware_calls() )
	{
		// read Optical Power at a known frequency from 306 module 
		hres = SERVERINTERFACE->readOpticalPowerAtFreq( module_name_CString, channel, frequency, p_optical_power );
	}

	if( FAILED(hres) )
	{
		handleCGResearchError( _T("CCLoseGridSystemAXCCtrl::readOpticalPowerAtFreq()"), hres);
	}


	log_msg = CString("CCLoseGridSystemAXCCtrl::readOpticalPowerAtFreq() p_optical_power = ");
	log_msg.AppendFormat("%g",*p_optical_power);
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::readOpticalPowerAtFreq() exiting"),DIAGNOSTIC_CGR_LOG)

	SERVERINTERFACE->closeLogFile();

	return;
}

void 
CCLoseGridSystemAXCCtrl::
readTemperature(
	double* p_temperature )
{
	if( getSystemState() != state_type::set_up
	 && getSystemState() != state_type::complete )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::readTemperature() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// function cannot be called in the current system state
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR )
	}

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::readTemperature() entered"),DIAGNOSTIC_CGR_LOG)

	// Check for NULL pointer
	if( p_temperature == NULL )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::readTemperature() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_NULL_PTR_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_NULL_PTR_ERROR )
	}


	HRESULT hres = S_OK;

	if( !CG_REG->get_DSDBR01_BASE_stub_hardware_calls() )
	{
		// Read Temperature from TEC controller
		hres = SERVERINTERFACE->readTemperature( p_temperature );
	}

	if( FAILED(hres) )
	{
		handleCGResearchError( _T("CCLoseGridSystemAXCCtrl::readTemperature()"), hres);
	}


	CString log_msg("CCLoseGridSystemAXCCtrl::readTemperature() p_temperature = ");
	log_msg.AppendFormat("%g",*p_temperature);
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::readTemperature() exiting"),DIAGNOSTIC_CGR_LOG)

	SERVERINTERFACE->closeLogFile();

	return;
}

void 
CCLoseGridSystemAXCCtrl::
getTECTempSetpoint(
	double* p_temp_setpoint )
{
	if( getSystemState() != state_type::start_up
	 && getSystemState() != state_type::set_up
	 && getSystemState() != state_type::complete )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::getTECTempSetpoint() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// function cannot be called in the current system state
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR )
	}

	// Check for NULL pointer
	if( p_temp_setpoint == NULL )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::getTECTempSetpoint() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_NULL_PTR_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_NULL_PTR_ERROR )
	}

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::getTECTempSetpoint() entered"),DIAGNOSTIC_CGR_LOG)

	HRESULT hres = S_OK;

	if( !CG_REG->get_DSDBR01_BASE_stub_hardware_calls() )
	{
		// call CGResearch getTECSetpoint 
		hres = SERVERINTERFACE->getTecSetpoint( p_temp_setpoint );
	}

	if( FAILED(hres) )
	{
		handleCGResearchError( _T("CCLoseGridSystemAXCCtrl::getTECTempSetpoint()"), hres);
	}

	CString log_msg("CCLoseGridSystemAXCCtrl::getTECTempSetpoint() p_temp_setpoint = ");
	log_msg.AppendFormat("%g",*p_temp_setpoint);
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::getTECTempSetpoint() exiting"),DIAGNOSTIC_CGR_LOG)

	SERVERINTERFACE->closeLogFile();

	return;
}

void 
CCLoseGridSystemAXCCtrl::
setTECTempSetpoint(
	double temp_setpoint )
{
	if( getSystemState() != state_type::start_up
	 && getSystemState() != state_type::set_up
	 && getSystemState() != state_type::complete )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::setTECTempSetpoint() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// function cannot be called in the current system state
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR )
	}

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::setTECTempSetpoint() entered"),DIAGNOSTIC_CGR_LOG)
	CString log_msg("CCLoseGridSystemAXCCtrl::setTECTempSetpoint() temp_setpoint = ");
	log_msg.AppendFormat("%g",temp_setpoint);
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	HRESULT hres = S_OK;

	if( !CG_REG->get_DSDBR01_BASE_stub_hardware_calls() )
	{
		// call CGResearch setTECSetpoint 
		hres = SERVERINTERFACE->setTecSetpoint( temp_setpoint );
	}

	if( FAILED(hres) )
	{
		handleCGResearchError( _T("CCLoseGridSystemAXCCtrl::setTECTempSetpoint()"), hres);
	}

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::setTECTempSetpoint() exiting"),DIAGNOSTIC_CGR_LOG)

	SERVERINTERFACE->closeLogFile();

	return;
}

void 
CCLoseGridSystemAXCCtrl::
calcCoarseFreqPolyCoeffs(
	short num_of_sample_pts, 
	short num_of_poly_coeffs,
	VARIANT* p_poly_coeffs,
	double* p_chi_squared )
{
	if( getSystemState() != state_type::complete )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::calcCoarseFreqPolyCoeffs() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// function cannot be called in the current system state
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR )
	}

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::calcCoarseFreqPolyCoeffs() entered"),DIAGNOSTIC_CGR_LOG)
	CString log_msg("CCLoseGridSystemAXCCtrl::calcCoarseFreqPolyCoeffs() num_of_sample_pts = ");
	log_msg.AppendFormat("%d",num_of_sample_pts);
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::calcCoarseFreqPolyCoeffs() num_of_poly_coeffs = ");
	log_msg.AppendFormat("%d",num_of_poly_coeffs);
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	// Check for NULL pointer
	if( p_poly_coeffs == NULL )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::calcCoarseFreqPolyCoeffs() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_NULL_PTR_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_NULL_PTR_ERROR )
	}


	// Check number of polynomial coefficients is within range
	if( num_of_poly_coeffs < MIN_COARSEFREQPOLYCOEFFS 
		|| num_of_poly_coeffs > MAX_COARSEFREQPOLYCOEFFS )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::calcCoarseFreqPolyCoeffs() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_COARSEFREQCOEFFS_ARG_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_COARSEFREQCOEFFS_ARG_ERROR )
	}

	// Check number of sample points is more than
	// the number of polynomial coefficients
	if( num_of_sample_pts <= num_of_poly_coeffs )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::calcCoarseFreqPolyCoeffs() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_COARSEFREQSAMPLENUM_ARG_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_COARSEFREQSAMPLENUM_ARG_ERROR )
	}

	// use same directory, laser_id and time_stamp as in _overall_mode_map
	_ops._results_dir_CString = _overall_mode_map._results_dir_CString;
	_ops._laser_id_CString = _overall_mode_map._laser_id_CString;
	_ops._date_time_stamp_CString = _overall_mode_map._date_time_stamp_CString;

	// Collect samples and calculate polynomial
	CCGOperatingPoints::rtype calc_poly_coeffs_rval = CCGOperatingPoints::rtype::ok;
	std::vector<double> poly_coefficients;

	calc_poly_coeffs_rval = _ops.calcCoarseFreqPoly(
		num_of_sample_pts,
		num_of_poly_coeffs,
		poly_coefficients,
		p_chi_squared );

	if( calc_poly_coeffs_rval != CCGOperatingPoints::rtype::ok )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::calcCoarseFreqPolyCoeffs() error: ");
		log_err_msg += CString(_ops.get_error_pair( calc_poly_coeffs_rval ).second.c_str());
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION_FROM_PAIR( _ops.get_error_pair( calc_poly_coeffs_rval ) )
	}

	rcode_type rval = vectorOfDoublesToVariantR8Array(
		&poly_coefficients,
		p_poly_coeffs );

	if( rval != rcode_type::ok )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::calcCoarseFreqPolyCoeffs() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_VARIANT_CREATION_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_VARIANT_CREATION_ERROR )
	}

	log_msg = CString("CCLoseGridSystemAXCCtrl::calcCoarseFreqPolyCoeffs() p_chi_squared = ");
	log_msg.AppendFormat("%d",*p_chi_squared);
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::calcCoarseFreqPolyCoeffs() exiting"),DIAGNOSTIC_CGR_LOG)

	SERVERINTERFACE->closeLogFile();

	return;
}


void 
CCLoseGridSystemAXCCtrl::
getCoarseFreqPolyCoeffs(
	VARIANT* p_poly_coeffs )
{
	if( getSystemState() != state_type::start_up
	 && getSystemState() != state_type::set_up
	 && getSystemState() != state_type::complete )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::getCoarseFreqPolyCoeffs() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// function cannot be called in the current system state
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR )
	}

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::getCoarseFreqPolyCoeffs() entered"),DIAGNOSTIC_CGR_LOG)

	// Check for NULL pointer
	if( p_poly_coeffs == NULL )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::getCoarseFreqPolyCoeffs() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_NULL_PTR_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_NULL_PTR_ERROR )
	}

	std::vector<double> poly_coefficients;
	CCGOperatingPoints::rtype get_poly_coeffs_rval = CCGOperatingPoints::rtype::ok;

	get_poly_coeffs_rval = _ops.getCoarseFreqPoly( poly_coefficients );

	if( get_poly_coeffs_rval == CCGOperatingPoints::rtype::ok )
	{
		rcode_type rval = vectorOfDoublesToVariantR8Array(
			&poly_coefficients,
			p_poly_coeffs );

		if( rval != rcode_type::ok )
		{
			CString log_err_msg("CCLoseGridSystemAXCCtrl::getCoarseFreqPolyCoeffs() error: ");
			log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_VARIANT_CREATION_ERROR) );
			CGR_LOG(log_err_msg,ERROR_CGR_LOG)

			CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_VARIANT_CREATION_ERROR )
		}
	}
	else
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::getCoarseFreqPolyCoeffs() error: ");
		log_err_msg += CString(_ops.get_error_pair( get_poly_coeffs_rval ).second.c_str());
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION_FROM_PAIR( _ops.get_error_pair( get_poly_coeffs_rval ) )
	}


	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::getCoarseFreqPolyCoeffs() exiting"),DIAGNOSTIC_CGR_LOG)

	SERVERINTERFACE->closeLogFile();

	return;
}


void 
CCLoseGridSystemAXCCtrl::
setCoarseFreqPolyCoeffs(
	VARIANT* p_poly_coeffs )
{
	if( getSystemState() != state_type::start_up
	 && getSystemState() != state_type::set_up
	 && getSystemState() != state_type::complete )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::setCoarseFreqPolyCoeffs() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// function cannot be called in the current system state
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR )
	}


	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::setCoarseFreqPolyCoeffs() entered"),DIAGNOSTIC_CGR_LOG)

	// Check for NULL pointer
	if( p_poly_coeffs == NULL )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::setCoarseFreqPolyCoeffs() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_NULL_PTR_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_NULL_PTR_ERROR )
	}

	std::vector<double> poly_coefficients;
	rcode_type rcode = variantR8ArrayToVectorOfDoubles( p_poly_coeffs, &poly_coefficients );

	// check the return code to determine if a VARIANT is invalid
	if( rcode == rcode_type::null_arg )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::setCoarseFreqPolyCoeffs() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_NULL_VARIANT_ARG_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_NULL_VARIANT_ARG_ERROR )
	}
	if( rcode == rcode_type::not_1D_array_of_doubles )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::setCoarseFreqPolyCoeffs() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_NOT1DDOUBLEARRAY_VARIANT_ARG_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_NOT1DDOUBLEARRAY_VARIANT_ARG_ERROR )
	}
	if( rcode == rcode_type::empty_array )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::setCoarseFreqPolyCoeffs() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_EMPTY_VARIANT_ARG_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_EMPTY_VARIANT_ARG_ERROR )
	}

	CCGOperatingPoints::rtype set_poly_coeffs_rval = CCGOperatingPoints::rtype::ok;

	set_poly_coeffs_rval = _ops.setCoarseFreqPoly( poly_coefficients );

	if( set_poly_coeffs_rval != CCGOperatingPoints::rtype::ok )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::setCoarseFreqPolyCoeffs() error: ");
		log_err_msg += CString(_ops.get_error_pair( set_poly_coeffs_rval ).second.c_str());
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION_FROM_PAIR( _ops.get_error_pair( set_poly_coeffs_rval ) )
	}


	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::setCoarseFreqPolyCoeffs() exiting"),DIAGNOSTIC_CGR_LOG)

	SERVERINTERFACE->closeLogFile();

	return;
}


void 
CCLoseGridSystemAXCCtrl::
getCoarseFrequency(
	double power_ratio,
	double* p_coarse_frequency )
{
	if( getSystemState() != state_type::start_up
	 && getSystemState() != state_type::set_up
	 && getSystemState() != state_type::complete )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::getCoarseFrequency() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// function cannot be called in the current system state
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR )
	}


	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::getCoarseFrequency() entered"),DIAGNOSTIC_CGR_LOG)
	CString log_msg("CCLoseGridSystemAXCCtrl::getCoarseFrequency() power_ratio = ");
	log_msg.AppendFormat("%g",power_ratio);
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	// Check for NULL pointer
	if( p_coarse_frequency == NULL )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::getCoarseFrequency() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_NULL_PTR_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_NULL_PTR_ERROR )
	}

	if( power_ratio >= 0 && power_ratio <=1 )
	{
		*p_coarse_frequency = _ops.getCoarseFreq(power_ratio);
	}
	else
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::getCoarseFrequency() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_POWERRATIO_ARG_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_POWERRATIO_ARG_ERROR )
	}

	log_msg = CString("CCLoseGridSystemAXCCtrl::getCoarseFrequency() p_coarse_frequency = ");
	log_msg.AppendFormat("%g",*p_coarse_frequency);
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::getCoarseFrequency() exiting"),DIAGNOSTIC_CGR_LOG)

	SERVERINTERFACE->closeLogFile();

	return;
}


void 
CCLoseGridSystemAXCCtrl::
getComplianceVoltage(
	LPCTSTR module_name,
	double* p_compliance_voltage )
{
	if( getSystemState() != state_type::set_up
	 && getSystemState() != state_type::complete )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::getComplianceVoltage() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// function cannot be called in the current system state
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR )
	}

	// Check for NULL pointer
	if( module_name == NULL
	 || p_compliance_voltage == NULL )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::getComplianceVoltage() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_NULL_PTR_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_NULL_PTR_ERROR )
	}

	CString module_name_CString( module_name );

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::getComplianceVoltage() entered"),DIAGNOSTIC_CGR_LOG)
	CString log_msg("CCLoseGridSystemAXCCtrl::getComplianceVoltage() module_name = ");
	log_msg += module_name_CString;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	// check the incoming module_name is okay
	if( !validModuleName( module_name_CString ) )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::getComplianceVoltage() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_INVALID_MODULE_NAME_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_INVALID_MODULE_NAME_ERROR )
	}

	HRESULT hres = S_OK;

	if( !CG_REG->get_DSDBR01_BASE_stub_hardware_calls() )
	{
		// Get the current being delivered by a chosen current source
		hres = SERVERINTERFACE->getComplianceVoltage( module_name_CString, p_compliance_voltage );
	}

	if( FAILED(hres) )
	{
		handleCGResearchError( _T("CCLoseGridSystemAXCCtrl::getComplianceVoltage()"), hres);
	}

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::getComplianceVoltage() exiting"),DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::getComplianceVoltage() p_compliance_voltage = ");
	log_msg.AppendFormat("%g",*p_compliance_voltage);
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	SERVERINTERFACE->closeLogFile();

	return;
}


void 
CCLoseGridSystemAXCCtrl::
setComplianceVoltage(
	LPCTSTR module_name,
	double compliance_voltage )
{
	if( getSystemState() != state_type::set_up
	 && getSystemState() != state_type::complete )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::setComplianceVoltage() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// function cannot be called in the current system state
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR )
	}

	// Check for NULL pointer
	if( module_name == NULL )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::setComplianceVoltage() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_NULL_PTR_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_NULL_PTR_ERROR )
	}

	CString module_name_CString( module_name );

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::setComplianceVoltage() entered"),DIAGNOSTIC_CGR_LOG)
	CString log_msg("CCLoseGridSystemAXCCtrl::setComplianceVoltage() module_name = ");
	log_msg += module_name_CString;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::setComplianceVoltage() compliance_voltage = ");
	log_msg.AppendFormat("%g",compliance_voltage);
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	// check the incoming module_name is okay
	if( !validModuleName( module_name_CString ) )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::setComplianceVoltage() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_INVALID_MODULE_NAME_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_INVALID_MODULE_NAME_ERROR )
	}


	HRESULT hres = S_OK;

	if( !CG_REG->get_DSDBR01_BASE_stub_hardware_calls() )
	{
			// Set the current to be delivered by a chosen current source
			hres = SERVERINTERFACE->setComplianceVoltage( module_name_CString, compliance_voltage );
	}

	if( FAILED(hres) )
	{
		handleCGResearchError( _T("CCLoseGridSystemAXCCtrl::setComplianceVoltage()"), hres);
	}

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::setComplianceVoltage() exiting"),DIAGNOSTIC_CGR_LOG)

	SERVERINTERFACE->closeLogFile();

	return;
}



void 
CCLoseGridSystemAXCCtrl::
rampdownAndDisconnect()
{
	if( getSystemState() != state_type::set_up
	 && getSystemState() != state_type::complete )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::rampdownAndDisconnect() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// function cannot be called in the current system state
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR )
	}

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::rampdownAndDisconnect() entered"),DIAGNOSTIC_CGR_LOG)

	HRESULT hres = S_OK;

	if( !CG_REG->get_DSDBR01_BASE_stub_hardware_calls() )
	{
		// disconnect all current sources that are turned on
		hres = SERVERINTERFACE->disconnect();
	}

	if( FAILED(hres) )
	{
		handleCGResearchError( _T("CCLoseGridSystemAXCCtrl::rampdownAndDisconnect()"), hres);
	}

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::rampdownAndDisconnect() exiting"),DIAGNOSTIC_CGR_LOG)

	SERVERINTERFACE->closeLogFile();

	return;
}



void 
CCLoseGridSystemAXCCtrl::
stopAndShutdown()
{
	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::stopAndShutdown() entered"),DIAGNOSTIC_CGR_LOG)

	HRESULT hres = S_OK;


	if( getSystemState() == state_type::busy )
	{	
		// this sets a flag which the thread uses to terminate itself

		hres = SERVERINTERFACE->markThreadToKill( getWorkerThreadID() );

		DWORD sleep_time_ms = 100;	// 0.1 sec
		int wait_cycles = 10;		// 1 sec

		int wait_cycle_count = 0;
		long marked_thread_id = NULL;
		do
		{
			Sleep(100);
			wait_cycle_count++;

			hres = SERVERINTERFACE->getMarkedThreadID( &marked_thread_id );
		}
		while( marked_thread_id != NULL && wait_cycle_count < wait_cycles );

		if( marked_thread_id == NULL )
		{
			// successfully killed the thread
			// change state to complete
			setSystemState( state_type::complete );
		}
	}

	shutdown();

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::stopAndShutdown() exiting"),DIAGNOSTIC_CGR_LOG)

	SERVERINTERFACE->closeLogFile();

	return;
}




void 
CCLoseGridSystemAXCCtrl::
applyStableOP(
	short sm,
	short lm,
	double index,
	short apply_Gain_SOA_of_SM_map )
{

	if( getSystemState() != state_type::complete )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::applyStableOP() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// function cannot be called in the current system state
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR )
	}

	if( apply_Gain_SOA_of_SM_map != TRUE && apply_Gain_SOA_of_SM_map != FALSE )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::applyStableOP() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_INVALID_ARGUMENT) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// apply_Gain_SOA_of_SM_map must be 0 (FALSE) or 1 (TRUE)
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_INVALID_ARGUMENT )
	}

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::applyStableOP() entered"),DIAGNOSTIC_CGR_LOG)



	// Apply stable operating point
	CCGOperatingPoints::rtype op_rval = CCGOperatingPoints::rtype::ok;

	op_rval = _ops.applyStableOP(
		sm,
		lm,
		index,
		apply_Gain_SOA_of_SM_map );

	if( op_rval != CCGOperatingPoints::rtype::ok )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::applyStableOP() error: ");
		log_err_msg += CString(_ops.get_error_pair( op_rval ).second.c_str());
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION_FROM_PAIR( _ops.get_error_pair( op_rval ) )
	}

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::applyStableOP() exiting"),DIAGNOSTIC_CGR_LOG)

	SERVERINTERFACE->closeLogFile();

	return;
}



void 
CCLoseGridSystemAXCCtrl::
applyPointOnSMMap(
	short sm,
	double Im_index,
	double Ip_index,
	short apply_Gain_SOA_of_SM_map )
{

	if( getSystemState() != state_type::complete )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::applyPointOnSMMap() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// function cannot be called in the current system state
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR )
	}

	if( apply_Gain_SOA_of_SM_map != TRUE && apply_Gain_SOA_of_SM_map != FALSE )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::applyPointOnSMMap() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_INVALID_ARGUMENT) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// apply_Gain_SOA_of_SM_map must be 0 (FALSE) or 1 (TRUE)
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_INVALID_ARGUMENT )
	}

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::applyPointOnSMMap() entered"),DIAGNOSTIC_CGR_LOG)




	// Apply operating point
	CCGOperatingPoints::rtype op_rval = CCGOperatingPoints::rtype::ok;

	op_rval = _ops.applyPointOnSMMap(
		sm,
		Im_index,
		Ip_index,
		apply_Gain_SOA_of_SM_map );

	if( op_rval != CCGOperatingPoints::rtype::ok )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::applyPointOnSMMap() error: ");
		log_err_msg += CString(_ops.get_error_pair( op_rval ).second.c_str());
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION_FROM_PAIR( _ops.get_error_pair( op_rval ) )
	}


	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::applyPointOnSMMap() exiting"),DIAGNOSTIC_CGR_LOG)

	SERVERINTERFACE->closeLogFile();

	return;
}


void 
CCLoseGridSystemAXCCtrl::
getPointOnSMMap(
	short sm,
	double* p_Im_index,
	double* p_Ip_index )
{

	if( getSystemState() != state_type::complete )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::getPointOnSMMap() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// function cannot be called in the current system state
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR )
	}


	// Check for NULL pointer
	if( p_Im_index == NULL
	 || p_Ip_index == NULL )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::getPointOnSMMap() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_NULL_PTR_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_NULL_PTR_ERROR )
	}

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::getPointOnSMMap() entered"),DIAGNOSTIC_CGR_LOG)



	// Get current operating point
	CCGOperatingPoints::rtype op_rval = CCGOperatingPoints::rtype::ok;

	op_rval = _ops.getPointOnSMMap(
		sm,
		p_Im_index,
		p_Ip_index );

	if( op_rval != CCGOperatingPoints::rtype::ok )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::applyPointOnSMMap() error: ");
		log_err_msg += CString(_ops.get_error_pair( op_rval ).second.c_str());
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION_FROM_PAIR( _ops.get_error_pair( op_rval ) )
	}


	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::getPointOnSMMap() exiting"),DIAGNOSTIC_CGR_LOG)

	SERVERINTERFACE->closeLogFile();

	return;
}


void 
CCLoseGridSystemAXCCtrl::
initWavemeter()
{

	if( getSystemState() != state_type::complete
	 && getSystemState() != state_type::start_up
	 && getSystemState() != state_type::set_up )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::initWavemeter() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// function cannot be called in the current system state
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR )
	}


	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::initWavemeter() entered"),DIAGNOSTIC_CGR_LOG)

	HRESULT hres = S_OK;

	if( !CG_REG->get_DSDBR01_BASE_stub_hardware_calls() )
	{
		// initialise wavemeter
		hres = SERVERINTERFACE->initWavemeter();
	}

	if( FAILED(hres) )
	{
		handleCGResearchError( _T("CCLoseGridSystemAXCCtrl::initWavemeter()"), hres);
	}

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::initWavemeter() exiting"),DIAGNOSTIC_CGR_LOG)

	SERVERINTERFACE->closeLogFile();

	return;
}


void 
CCLoseGridSystemAXCCtrl::
loadImFromFile(
	short supermode_number,
	BSTR Im_filename )
{
	if( getSystemState() != state_type::complete )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::loadImFromFile() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// function cannot be called in the current system state
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR )
	}

	// Check for NULL pointer
	if( Im_filename == NULL )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::loadImFromFile() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_NULL_PTR_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_NULL_PTR_ERROR )
	}

	CString Im_filename_CString( Im_filename );

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::loadImFromFile() entered"),DIAGNOSTIC_CGR_LOG)

	CString log_msg("CCLoseGridSystemAXCCtrl::loadImFromFile() Im_filename = ");
	log_msg += Im_filename_CString;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::loadImFromFile() supermode_number = ");
	log_msg.AppendFormat("%d",supermode_number);
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	// test strings are not empty
	if( Im_filename_CString.GetLength() <= 0 )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::loadImFromFile() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_EMPTY_STRING_ARG_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_EMPTY_STRING_ARG_ERROR )
	}

	if( ( supermode_number > (getSMFoundCount() - 1) )
	 || ( supermode_number < 0 ) )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::loadImFromFile() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_SPECIFIED_SUPERMODE_NOT_FOUND) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// supermode number requested is not available
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_SPECIFIED_SUPERMODE_NOT_FOUND )
	}


	char absPath[256];
	if( _fullpath( absPath, Im_filename_CString.GetBuffer(), 256 ) == NULL )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::loadImFromFile() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_EMPTY_STRING_ARG_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_FILE_DOES_NOT_EXIST_ERROR )
	}

	CString absPath_CString( absPath );
	log_msg = CString("CCLoseGridSystemAXCCtrl::loadImFromFile() absolute path Im filename = ");
	log_msg += absPath_CString;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)


	CDSDBROverallModeMapLine::rcode load_from_file_rval = CDSDBROverallModeMapLine::rcode::ok;


	// load Im currents from file

	load_from_file_rval = _overall_mode_map._supermodes[supermode_number]._filtered_middle_line.readCurrentsFromFile( absPath_CString ); 

	if( load_from_file_rval != CDSDBROverallModeMapLine::rcode::ok )
	{
		std::pair<short, std::string> return_error_pair;

		switch( load_from_file_rval )
		{
			case CDSDBROverallModeMapLine::rcode::file_format_invalid:
				CGR_LOG("CCLoseGridSystemAXCCtrl::loadImFromFile() error: _filtered_middle_line.readCurrentsFromFile() returned file_format_invalid",ERROR_CGR_LOG)
				CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_INVALID_FILE_FORMAT_ERROR )
				break;
			case CDSDBROverallModeMapLine::rcode::file_does_not_exist:
				CGR_LOG("CCLoseGridSystemAXCCtrl::loadImFromFile() error: _filtered_middle_line.readCurrentsFromFile() returned file_does_not_exist",ERROR_CGR_LOG)
				CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_FILE_DOES_NOT_EXIST_ERROR )
				break;
			case CDSDBROverallModeMapLine::rcode::could_not_open_file:
				CGR_LOG("CCLoseGridSystemAXCCtrl::loadImFromFile() error: _filtered_middle_line.readCurrentsFromFile() returned could_not_open_file",ERROR_CGR_LOG)
				CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_FILEOPEN_ERROR )
				break;
			case CDSDBROverallModeMapLine::rcode::no_data:
				CGR_LOG("CCLoseGridSystemAXCCtrl::loadImFromFile() error: _filtered_middle_line.readCurrentsFromFile() returned no_data",ERROR_CGR_LOG)
				CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_NO_DATA_ERROR )
				break;
			case CDSDBROverallModeMapLine::rcode::submap_indexing_error:
				CGR_LOG("CCLoseGridSystemAXCCtrl::loadImFromFile() error: _filtered_middle_line.readCurrentsFromFile() returned submap_indexing_error",ERROR_CGR_LOG)
				CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_INVALID_FILE_FORMAT_ERROR )
				break;
		}

		CString log_err_msg("CCLoseGridSystemAXCCtrl::loadImFromFile() error: ");
		log_err_msg += CString( return_error_pair.second.c_str() );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION_FROM_PAIR( return_error_pair )
	}

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::loadImFromFile() exiting"),DIAGNOSTIC_CGR_LOG)

	SERVERINTERFACE->closeLogFile();

	return;
}



void 
CCLoseGridSystemAXCCtrl::loadImiddleFreqFromFile(
	short supermode_number,
	short longitudinalmode_number,
	BSTR ImiddleFreq_filename )
{
	if( getSystemState() != state_type::complete )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::loadImiddleFreqFromFile() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// function cannot be called in the current system state
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR )
	}

	// Check for NULL pointer
	if( ImiddleFreq_filename == NULL )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::loadImiddleFreqFromFile() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_NULL_PTR_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_NULL_PTR_ERROR )
	}

	CString ImiddleFreq_filename_CString( ImiddleFreq_filename );

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::loadImiddleFreqFromFile() entered"),DIAGNOSTIC_CGR_LOG)

	CString log_msg("CCLoseGridSystemAXCCtrl::loadImiddleFreqFromFile() ImiddleFreq_filename = ");
	log_msg += ImiddleFreq_filename_CString;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::loadImiddleFreqFromFile() supermode_number = ");
	log_msg.AppendFormat("%d",supermode_number);
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	log_msg = CString("CCLoseGridSystemAXCCtrl::loadImiddleFreqFromFile() longitudinalmode_number = ");
	log_msg.AppendFormat("%d",longitudinalmode_number);
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	// test strings are not empty
	if( ImiddleFreq_filename_CString.GetLength() <= 0 )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::loadImiddleFreqFromFile() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_EMPTY_STRING_ARG_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_EMPTY_STRING_ARG_ERROR )
	}

	if( ( supermode_number > (getSMFoundCount() - 1) )
	 || ( supermode_number < 0 ) )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::loadImiddleFreqFromFile() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_SPECIFIED_SUPERMODE_NOT_FOUND) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// supermode number requested is not available
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_SPECIFIED_SUPERMODE_NOT_FOUND )
	}

	if( ( longitudinalmode_number > ( _overall_mode_map._supermodes[supermode_number].getLMCount() - 1) )
	 || ( longitudinalmode_number < 0 ) )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::loadImiddleFreqFromFile() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_LM_NOT_FOUND_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		// supermode number requested is not available
		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_LM_NOT_FOUND_ERROR )
	}

	char absPath[256];
	if( _fullpath( absPath, ImiddleFreq_filename_CString.GetBuffer(), 256 ) == NULL )
	{
		CString log_err_msg("CCLoseGridSystemAXCCtrl::loadImiddleFreqFromFile() error: ");
		log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_EMPTY_STRING_ARG_ERROR) );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION( CGSYSTEM_FILE_DOES_NOT_EXIST_ERROR )
	}

	CString absPath_CString( absPath );
	log_msg = CString("CCLoseGridSystemAXCCtrl::loadImiddleFreqFromFile() absolute path ImiddleFreq filename = ");
	log_msg += absPath_CString;
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)


	CDSDBRSupermodeMapLine::rcode load_from_file_rval = CDSDBRSupermodeMapLine::rcode::ok;


	// load Im currents from file

	load_from_file_rval = _overall_mode_map._supermodes[supermode_number]._lm_middle_lines_of_frequency[longitudinalmode_number].readCurrentsFromFile(absPath_CString);

	if( load_from_file_rval != CDSDBRSupermodeMapLine::rcode::ok )
	{
		std::pair<short, std::string> return_error_pair;

		switch( load_from_file_rval )
		{
			case CDSDBRSupermodeMapLine::rcode::file_format_invalid:
				CGR_LOG("CCLoseGridSystemAXCCtrl::loadImiddleFreqFromFile() error: _lm_middle_lines_of_frequency readCurrentsFromFile() returned file_format_invalid",ERROR_CGR_LOG)
				CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_INVALID_FILE_FORMAT_ERROR )
				break;
			case CDSDBRSupermodeMapLine::rcode::file_does_not_exist:
				CGR_LOG("CCLoseGridSystemAXCCtrl::loadImiddleFreqFromFile() error: _lm_middle_lines_of_frequency readCurrentsFromFile() returned file_does_not_exist",ERROR_CGR_LOG)
				CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_FILE_DOES_NOT_EXIST_ERROR )
				break;
			case CDSDBRSupermodeMapLine::rcode::could_not_open_file:
				CGR_LOG("CCLoseGridSystemAXCCtrl::loadImiddleFreqFromFile() error: _lm_middle_lines_of_frequency readCurrentsFromFile() returned could_not_open_file",ERROR_CGR_LOG)
				CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_FILEOPEN_ERROR )
				break;
			case CDSDBRSupermodeMapLine::rcode::no_data:
				CGR_LOG("CCLoseGridSystemAXCCtrl::loadImiddleFreqFromFile() error: _lm_middle_lines_of_frequency readCurrentsFromFile() returned no_data",ERROR_CGR_LOG)
				CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_NO_DATA_ERROR )
				break;
		}

		CString log_err_msg("CCLoseGridSystemAXCCtrl::loadImiddleFreqFromFile() error: ");
		log_err_msg += CString( return_error_pair.second.c_str() );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)

		CGSYSTEM_THROW_EXCEPTION_FROM_PAIR( return_error_pair )
	}

	CGR_LOG(CString("CCLoseGridSystemAXCCtrl::loadImiddleFreqFromFile() exiting"),DIAGNOSTIC_CGR_LOG)

	SERVERINTERFACE->closeLogFile();

	return;
}





bool
CCLoseGridSystemAXCCtrl::
validLaserType( CString laser_type_CString )
{
	bool result = false;

	// test against supported laser type
	if( laser_type_CString == LASER_TYPE_ID_DSDBR01_CSTRING )
		result = true;

	return result;
}

bool
CCLoseGridSystemAXCCtrl::
validModuleName( CString module_name_CString )
{
	bool result = true;

	// string must not contain the characters checked below
	if( module_name_CString.FindOneOf( MODULE_ID_BAD_CHARS ) != -1 )
		result = false;

	// string length must be greater than zero
	if( module_name_CString.GetLength() <= 0 )
		result = false;

	// string length must be less than max
	if( module_name_CString.GetLength() > ID_MAX_LENGTH )
		result = false;

	return result;
}


bool
CCLoseGridSystemAXCCtrl::
validLaserID( CString laser_id_CString )
{
	bool result = true;

	// string must not contain the characters checked below
	if( laser_id_CString.FindOneOf( LASER_ID_BAD_CHARS ) != -1 )
		result = false;

	// string length must be greater than zero
	if( laser_id_CString.GetLength() <= 0 )
		result = false;

	// string length must be less than max
	if( laser_id_CString.GetLength() > ID_MAX_LENGTH )
		result = false;

	return result;
}



void
CCLoseGridSystemAXCCtrl::
getNewDateTimeStamp( CString &date_time_stamp_CString )
{
    // Get date_time stamp
    char tmpbuf[128];
	time_t ltime;
	struct tm *struct_tm_time;

    _tzset();
	time( &ltime );
	struct_tm_time = localtime( &ltime );
	strftime( tmpbuf, 128, "%Y%m%d%H%M%S", struct_tm_time );

	date_time_stamp_CString = CString(tmpbuf);

	return;
}


void
CCLoseGridSystemAXCCtrl::
setSystemLaserID( CString laser_id_CString )
{
	EnterCriticalSection(&_data_protect);

	_laser_id_CString = laser_id_CString;

	LeaveCriticalSection(&_data_protect);

	return;
}


void
CCLoseGridSystemAXCCtrl::
setSystemDateTimeStamp( CString date_time_stamp_CString )
{
	EnterCriticalSection(&_data_protect);

	_date_time_stamp_CString = date_time_stamp_CString;

	LeaveCriticalSection(&_data_protect);

	return;
}


CString
CCLoseGridSystemAXCCtrl::
getSystemLaserID()
{
	EnterCriticalSection(&_data_protect);

	CString laser_id_CString = _laser_id_CString;

	LeaveCriticalSection(&_data_protect);

	return laser_id_CString;
}

CString
CCLoseGridSystemAXCCtrl::
getSystemDateTimeStamp()
{
	EnterCriticalSection(&_data_protect);

	CString date_time_stamp_CString = _date_time_stamp_CString;

	LeaveCriticalSection(&_data_protect);

	return date_time_stamp_CString;
}


bool
CCLoseGridSystemAXCCtrl::
validDateTimeStamp( CString date_time_stamp_CString )
{
	bool valid = true;

	// Check date_time_stamp is correct length
	// must be of form YYYYMMDDHHMMSS => 14 characters
	if( date_time_stamp_CString.GetLength() != 14 )
		valid = false;

	if( valid )
	{
		char date_time_stamp_chars[14];
		strncpy( date_time_stamp_chars, date_time_stamp_CString.GetBuffer(), 14 );

		// Check every character is a digit 0-9
		for( int i = 0; i < 14 ; i++ )
		{
			if( !isdigit( date_time_stamp_chars[i] ) )
			{
				valid = false;
				break;
			}
		}

		if( valid )
		{
			char year_chars[5];
			char month_chars[3];
			char date_chars[3];
			char hour_chars[3];
			char minute_chars[3];
			char second_chars[3];

			// extract each portion of the date time stamp
			strncpy( year_chars, date_time_stamp_chars, 4 );
			strncpy( month_chars, &(date_time_stamp_chars[4]), 2 );
			strncpy( date_chars, &(date_time_stamp_chars[6]), 2 );
			strncpy( hour_chars, &(date_time_stamp_chars[8]), 2 );
			strncpy( minute_chars, &(date_time_stamp_chars[10]), 2 );
			strncpy( second_chars, &(date_time_stamp_chars[12]), 2 );

			year_chars[4] = '\0';
			month_chars[2] = '\0';
			date_chars[2] = '\0';
			hour_chars[2] = '\0';
			minute_chars[2] = '\0';
			second_chars[2] = '\0';

			int year = atoi( year_chars );
			int month = atoi( month_chars );
			int date = atoi( date_chars );
			int hour = atoi( hour_chars );
			int minute = atoi( minute_chars );
			int second = atoi( second_chars );

			bool leapyear = false;
			if( year%4 == 0 ) leapyear = true;

			// test year is in valid range
			if( year < 0 || year > 9999 )
				valid = false;

			// test month is in valid range
			if( valid && ( month < 1 || month > 12 ) )
				valid = false;

			// test date is in valid range
			if( valid && ( date < 1 || date > 31 ) )
				valid = false;

			// test date is in valid range for months April, June, September, November 
			if( valid
				&& ( month == 4 || month == 6 || month == 9 || month == 11 )
				&& ( date < 1 || date > 30 ) )
				valid = false;

			// test date is in valid range for month of February 
			if( valid
				&& ( ( !leapyear && month == 2 && ( date < 1 || date > 28 ) )
				|| ( leapyear && month == 2 && ( date < 1 || date > 29  ) ) ) )
				valid = false;

			// test hour is in valid range
			if( valid && ( hour < 0 || hour > 23 ) )
				valid = false;

			// test minute is in valid range
			if( valid && ( minute < 0 || minute > 59 ) )
				valid = false;

			// test second is in valid range
			if( valid && ( second < 0 || second > 59 ) )
				valid = false;
		}
	}

	return valid;
}


bool
CCLoseGridSystemAXCCtrl::
validDirectoryPath( CString directory_path_CString )
{
	bool valid = true;
	struct _stat stat_buffer;

	// Find directories with trailing '\\' character, other than a drive eg. "C:\"
	if( directory_path_CString.GetLength() > 3
	 && directory_path_CString.GetAt( directory_path_CString.GetLength() - 1 ) == '\\' )
	{
		// Remove trailing directory backslash '\\'
		directory_path_CString.TrimRight( "\\" );
	}

	if( _stat( directory_path_CString.GetBuffer(), &stat_buffer ) == -1 )
	{
		// path does not exist
		valid = false;
	}
	else
	{
		if( !( stat_buffer.st_mode & _S_IFDIR ) )
		{
			// path is not a directory
			valid = false;
		}
	}


	return valid;
}



CCLoseGridSystemAXCCtrl::rcode_type
CCLoseGridSystemAXCCtrl::
variantR8ArrayToVectorOfDoubles(
	VARIANT *p_array_VARIANT,
	std::vector<double> *p_vector_of_doubles )
{
	rcode_type rcode = rcode_type::ok;

	if(p_array_VARIANT->vt == (VT_ARRAY | VT_R8))
	{
		double* lpusBuffer;
		SafeArrayAccessData( p_array_VARIANT->parray, (void**)&lpusBuffer );

		if( p_array_VARIANT->parray != NULL )
		{

			if( p_array_VARIANT->parray->cDims == 1) //1D array
			{

				ULONG num = p_array_VARIANT->parray->rgsabound[0].cElements;
				if( num > 0)
				{
					p_vector_of_doubles->clear();

					for(ULONG i=0; i<num; i++)
					{
						p_vector_of_doubles->push_back(lpusBuffer[i]);
					}
				}
				else
				{
					rcode = rcode_type::empty_array;
				}
			}
			else
			{
				rcode = rcode_type::not_1D_array_of_doubles;
			}

		}
		else
		{
			rcode = rcode_type::null_arg;
		}

		SafeArrayUnaccessData( p_array_VARIANT->parray );
	}
	else
	{
		rcode = rcode_type::not_1D_array_of_doubles;
	}


	//SafeArrayDestroyData(p_array_VARIANT->parray);
	//SafeArrayDestroyDescriptor(p_array_VARIANT->parray);

	return rcode;
}



CCLoseGridSystemAXCCtrl::rcode_type
CCLoseGridSystemAXCCtrl::vectorOfDoublesToVariantR8Array(
	std::vector<double> *p_vector_of_doubles,
	VARIANT *p_array_VARIANT )
{
	rcode_type rval = rcode_type::ok;

	if( p_vector_of_doubles == NULL
	 || p_array_VARIANT == NULL )
	{
		// null argument passed
		rval = rcode_type::null_arg;
	}
	else
	{
		VariantInit( p_array_VARIANT );

		if( VariantClear( p_array_VARIANT ) != S_OK )
		{
			// cannot clear the variant
			rval = rcode_type::variant_clear_error;
		}
		else
		{
			bool empty_vector = false;
			if( p_vector_of_doubles->size() <=0 )
			{
				// empty array, insert test data
				empty_vector = true;
				p_vector_of_doubles->insert(p_vector_of_doubles->begin(),10,0);
			}

			//create an array bound
			SAFEARRAYBOUND rgsabound[1];
			//Set bounds of array
			rgsabound[0].lLbound	= 0; //Lower bound
			rgsabound[0].cElements	= (ULONG)(p_vector_of_doubles->size()); //Number of elements

			//Assigns the Variant to hold an array of doubles
			p_array_VARIANT->vt = VT_ARRAY | VT_R8;

			//Create the safe array of doubles, with the specified bounds, and only 1 dimension
			try
			{
				p_array_VARIANT->parray = SafeArrayCreate( VT_R8, 1, rgsabound );
			}
			catch(...)
			{
				rval = rcode_type::variant_array_creation_error;
				return rval;
			}
			
			SafeArrayAllocData( p_array_VARIANT->parray );

			double* p_data;
			//Fill in the buffer
			SafeArrayAccessData( p_array_VARIANT->parray, (void**)&p_data );
			
			for(ULONG i = 0; i < rgsabound[0].cElements ; i++ )
			{
				// Fill safe array with doubles values
				p_data[i] = (*p_vector_of_doubles)[i];
			}

			SafeArrayUnaccessData( p_array_VARIANT->parray );

			if(empty_vector) p_vector_of_doubles->clear();
		}
	}

	return rval;
}



CCLoseGridSystemAXCCtrl::rcode_type
CCLoseGridSystemAXCCtrl::vectorOfShortsToVariantI2Array(
	std::vector<short> *p_vector_of_shorts,
	VARIANT *p_array_VARIANT )
{
	rcode_type rval = rcode_type::ok;

	if( p_vector_of_shorts == NULL
	 || p_array_VARIANT == NULL )
	{
		// null argument passed
		rval = rcode_type::null_arg;
	}
	else
	{
		VariantInit( p_array_VARIANT );

		if( VariantClear( p_array_VARIANT ) != S_OK )
		{
			// cannot clear the variant
			rval = rcode_type::variant_clear_error;
		}
		else
		{

			//create an array bound
			SAFEARRAYBOUND rgsabound[1];
			//Set bounds of array
			rgsabound[0].lLbound	= 0; //Lower bound
			rgsabound[0].cElements	= (ULONG)(p_vector_of_shorts->size()); //Number of elements

			//Assigns the Variant to hold an array of shorts (2-byte signed integers)
			p_array_VARIANT->vt = VT_ARRAY | VT_I2;

			//Create the safe array of shorts, with the specified bounds, and only 1 dimension
			try
			{
				p_array_VARIANT->parray = SafeArrayCreate( VT_I2, 1, rgsabound );
			}
			catch(...)
			{
				rval = rcode_type::variant_array_creation_error;
				return rval;
			}

			SafeArrayAllocData( p_array_VARIANT->parray );

			short* p_data;
			//Fill in the buffer
			SafeArrayAccessData( p_array_VARIANT->parray, (void**)&p_data );
			
			for(ULONG i = 0; i < rgsabound[0].cElements ; i++ )
			{
				// Fill safe array with short values
				p_data[i] = (*p_vector_of_shorts)[i];
			}

			SafeArrayUnaccessData( p_array_VARIANT->parray );

		}
	}

	return rval;
}



short
CCLoseGridSystemAXCCtrl::getOverallMapLoaded()
{
	EnterCriticalSection(&_data_protect);

	short overall_map_loaded = _overall_map_loaded;

	LeaveCriticalSection(&_data_protect);

	return overall_map_loaded;
}

void
CCLoseGridSystemAXCCtrl::setOverallMapLoaded( short overall_map_loaded )
{
	EnterCriticalSection(&_data_protect);

	_overall_map_loaded = overall_map_loaded;

	LeaveCriticalSection(&_data_protect);

	return;
}






short
CCLoseGridSystemAXCCtrl::getSMFoundCount()
{
	EnterCriticalSection(&_data_protect);

	short supermodes_found_count = _supermodes_found_count;

	LeaveCriticalSection(&_data_protect);

	return supermodes_found_count;
}

void
CCLoseGridSystemAXCCtrl::setSMFoundCount( short supermodes_found_count )
{
	EnterCriticalSection(&_data_protect);

	_supermodes_found_count = supermodes_found_count;

	_supermode_map_screening_passed.clear();
	for( short i = 0 ; i < supermodes_found_count ; i++ )
		_supermode_map_screening_passed.push_back( SM_NOT_LOADED );

	LeaveCriticalSection(&_data_protect);

	return;
}


short
CCLoseGridSystemAXCCtrl::getSMMapLoadedCount()
{
	EnterCriticalSection(&_data_protect);

	short supermode_map_loaded_count = _supermode_map_loaded_count;

	LeaveCriticalSection(&_data_protect);

	return supermode_map_loaded_count;
}


void
CCLoseGridSystemAXCCtrl::setOMScreeningPassed( short overall_map_screening_passed )
{
	EnterCriticalSection(&_data_protect);

	_overall_map_screening_passed = overall_map_screening_passed;

	LeaveCriticalSection(&_data_protect);

	return;
}


short
CCLoseGridSystemAXCCtrl::getOMScreeningPassed()
{
	EnterCriticalSection(&_data_protect);

	short overall_map_screening_passed = _overall_map_screening_passed;

	LeaveCriticalSection(&_data_protect);

	return overall_map_screening_passed;
}


void
CCLoseGridSystemAXCCtrl::setSMScreeningPassed( short sm_number, short supermode_map_screening_passed )
{
	EnterCriticalSection(&_data_protect);

	if( sm_number < (short)_supermode_map_screening_passed.size() )
		_supermode_map_screening_passed[sm_number] = supermode_map_screening_passed;

	LeaveCriticalSection(&_data_protect);

	return;
}


short
CCLoseGridSystemAXCCtrl::getSMScreeningPassed( short sm_number )
{
	short supermode_map_screening_passed;

	EnterCriticalSection(&_data_protect);

	if( sm_number < (short)_supermode_map_screening_passed.size() )
		supermode_map_screening_passed = _supermode_map_screening_passed[sm_number];

	LeaveCriticalSection(&_data_protect);

	return supermode_map_screening_passed;
}

void
CCLoseGridSystemAXCCtrl::setSMMapLoadedCount( short supermode_map_loaded_count )
{
	EnterCriticalSection(&_data_protect);

	_supermode_map_loaded_count = supermode_map_loaded_count;

	LeaveCriticalSection(&_data_protect);

	return;
}

short
CCLoseGridSystemAXCCtrl::getEstimateCount()
{
	EnterCriticalSection(&_data_protect);

	short estimate_count = _estimate_count;

	LeaveCriticalSection(&_data_protect);

	return estimate_count;
}

void
CCLoseGridSystemAXCCtrl::setEstimateCount( short estimate_count )
{
	EnterCriticalSection(&_data_protect);

	_estimate_count = estimate_count;

	LeaveCriticalSection(&_data_protect);

	return;
}

short
CCLoseGridSystemAXCCtrl::getITUOPCount()
{
	EnterCriticalSection(&_data_protect);

	short ITUOP_count = _ITUOP_count;

	LeaveCriticalSection(&_data_protect);

	return ITUOP_count;
}

void
CCLoseGridSystemAXCCtrl::setITUOPCount( short ITUOP_count )
{
	EnterCriticalSection(&_data_protect);

	_ITUOP_count = ITUOP_count;

	LeaveCriticalSection(&_data_protect);

	return;
}


short
CCLoseGridSystemAXCCtrl::getDuffITUOPCount()
{
	EnterCriticalSection(&_data_protect);

	short duff_ITUOP_count = _duff_ITUOP_count;

	LeaveCriticalSection(&_data_protect);

	return duff_ITUOP_count;
}

void
CCLoseGridSystemAXCCtrl::setDuffITUOPCount( short duff_ITUOP_count )
{
	EnterCriticalSection(&_data_protect);

	_duff_ITUOP_count = duff_ITUOP_count;

	LeaveCriticalSection(&_data_protect);

	return;
}


DWORD
CCLoseGridSystemAXCCtrl::getWorkerThreadID()
{
	EnterCriticalSection(&_data_protect);

	DWORD worker_thread_id = _worker_thread_id;

	LeaveCriticalSection(&_data_protect);

	return worker_thread_id;
}

void
CCLoseGridSystemAXCCtrl::setWorkerThreadID( DWORD worker_thread_id )
{
	EnterCriticalSection(&_data_protect);

	_worker_thread_id = worker_thread_id;

	LeaveCriticalSection(&_data_protect);

	return;
}


std::pair<short, std::string>
CCLoseGridSystemAXCCtrl::getLastError()
{
	std::pair<short, std::string> last_error;

	EnterCriticalSection(&_data_protect);

	CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( last_error, _last_error );

	LeaveCriticalSection(&_data_protect);

	return last_error;
}

void
CCLoseGridSystemAXCCtrl::setLastError( std::pair<short, std::string> last_error )
{
	if( last_error.first != error_std_pair(CGSYSTEM_SUCCESS).first )
	{
		CString log_err_msg( "CCLoseGridSystemAXCCtrl::setLastError() : setting error : " );
		log_err_msg.AppendFormat( " %d : %s ", last_error.first, last_error.second.c_str() );
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)
	}

	EnterCriticalSection(&_data_protect);

	CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( _last_error, last_error );

	LeaveCriticalSection(&_data_protect);

	return;
}

void
CCLoseGridSystemAXCCtrl::clear()
{
	// clear all data
	setPercentComplete( 0 );
	setOverallMapLoaded( FALSE );
	setOMScreeningPassed( FALSE );
	setSMFoundCount( 0 );
	setSMMapLoadedCount( 0 );
	setEstimateCount( 0 );
	setITUOPCount( 0 );
	setDuffITUOPCount( 0 );
	setLastError( error_std_pair(CGSYSTEM_SUCCESS) );

	_overall_mode_map.clear();

	_ops.clear();
}



// CCLoseGridSystemAXCCtrl message handlers


// static functions


UINT
screenOverallMapProc( LPVOID pParam )
{
	// call this function to run in it's own thread
	// using AfxBeginThread
	CGR_LOG(CString("screenOverallMapProc() entered"),DIAGNOSTIC_CGR_LOG)

	two_ptrs* p_two_ptrs =  (two_ptrs*)pParam;

	CCLoseGridSystemAXCCtrl* p_cg_system = p_two_ptrs->p_axc;
	CDSDBROverallModeMap* p_overall_map = (CDSDBROverallModeMap*)(p_two_ptrs->p_other);

	p_cg_system->setOverallMapLoaded( FALSE );
	p_cg_system->setSMFoundCount( 0 );
	p_cg_system->setSMMapLoadedCount( 0 );
	p_cg_system->setEstimateCount( 0 );
	p_cg_system->setITUOPCount( 0 );
	p_cg_system->setDuffITUOPCount( 0 );

	CDSDBROverallModeMap::rcode rval_collection = CDSDBROverallModeMap::rcode::ok;
	CDSDBROverallModeMap::rcode rval_writedata = CDSDBROverallModeMap::rcode::ok;
	CDSDBROverallModeMap::rcode rval_screening = CDSDBROverallModeMap::rcode::ok;
	CDSDBROverallModeMap::rcode rval_writeresults = CDSDBROverallModeMap::rcode::ok;

	// collect data from live laser
	rval_collection = p_overall_map->collectMapsFromLaser();

	if( rval_collection == CDSDBROverallModeMap::rcode::ok )
	{
		// write data to files
		rval_writedata = p_overall_map->writeMapsToFile();
	}


	if( rval_collection == CDSDBROverallModeMap::rcode::ok )
	{
		// call screening procedure
		rval_screening = p_overall_map->screen();
	}


	if( rval_screening == CDSDBROverallModeMap::rcode::ok )
	{
		// okay so far,
		// write screening results to file

		rval_writeresults = p_overall_map->writeScreeningResultsToFile();
	}

#ifdef HARDWARE_BUILD

#else // HARDWARE_BUILD

	// put a wait in that increments the percent complete
	for( int i = 10; i < 90; i+=10 )
	{
		p_cg_system->setPercentComplete( i );
		Sleep(100);
	}

#endif // HARDWARE_BUILD


	if( rval_collection == CDSDBROverallModeMap::rcode::ok )
	{
		p_cg_system->setOverallMapLoaded( TRUE );
	}
	else
	{
		p_cg_system->setOverallMapLoaded( FALSE );
	}


	short num_of_supermodes = p_overall_map->supermode_count();
	if( !(num_of_supermodes > 0) ) 
	{
		p_cg_system->setSMFoundCount( 0 );	
	}
	else
	{
		p_cg_system->setSMFoundCount( num_of_supermodes );
	}


	if( p_overall_map->_overallModeMapAnalysis._screeningPassed )
		p_cg_system->setOMScreeningPassed( TRUE );
	else
		p_cg_system->setOMScreeningPassed( FALSE );

	p_cg_system->setSystemState( CCLoseGridSystemAXCCtrl::state_type::complete );


	if( rval_collection != CDSDBROverallModeMap::rcode::ok )
	{
		CGR_LOG(CString("screenOverallMapProc() calling CCLoseGridSystemAXCCtrl::setLastError"),DIAGNOSTIC_CGR_LOG)
		p_cg_system->setLastError( p_overall_map->get_error_pair( rval_collection ) );
	}
	else
	if( rval_writedata != CDSDBROverallModeMap::rcode::ok )
	{
		CGR_LOG(CString("screenOverallMapProc() calling CCLoseGridSystemAXCCtrl::setLastError"),DIAGNOSTIC_CGR_LOG)
		p_cg_system->setLastError( p_overall_map->get_error_pair( rval_writedata ) );
	}
	else
	if( rval_screening != CDSDBROverallModeMap::rcode::ok )
	{
		CGR_LOG(CString("screenOverallMapProc() calling CCLoseGridSystemAXCCtrl::setLastError"),DIAGNOSTIC_CGR_LOG)
		p_cg_system->setLastError( p_overall_map->get_error_pair( rval_screening ) );
	}
	else
	if( rval_writeresults != CDSDBROverallModeMap::rcode::ok )
	{
		CGR_LOG(CString("screenOverallMapProc() calling CCLoseGridSystemAXCCtrl::setLastError"),DIAGNOSTIC_CGR_LOG)
		p_cg_system->setLastError( p_overall_map->get_error_pair( rval_writeresults ) );
	}
	else
	if( !(num_of_supermodes > 0) )
	{
		CGR_LOG(CString("screenOverallMapProc() calling CCLoseGridSystemAXCCtrl::setLastError"),DIAGNOSTIC_CGR_LOG)
		p_cg_system->setLastError( error_std_pair( CGSYSTEM_NO_VALID_SUPERMODES_FOUND ) );
	}

	CGR_LOG(CString("screenOverallMapProc() exiting"),DIAGNOSTIC_CGR_LOG)

	SERVERINTERFACE->closeLogFile();

	p_cg_system->setWorkerThreadID( NULL );

	AfxEndThread(0,TRUE);  // thread completed successfully

	return 0;
}


UINT screenSMMapProc( LPVOID pParam )
{
	// call this function to run in it's own thread
	// using AfxBeginThread
	CGR_LOG(CString("screenSMMapProc() entered"),DIAGNOSTIC_CGR_LOG)

	two_ptrs* p_two_ptrs =  (two_ptrs*)pParam;

	CCLoseGridSystemAXCCtrl* p_cg_system = p_two_ptrs->p_axc;
	CDSDBRSuperMode* p_sm_map = (CDSDBRSuperMode*)(p_two_ptrs->p_other);

	p_cg_system->setEstimateCount( 0 );
	p_cg_system->setITUOPCount( 0 );
	p_cg_system->setDuffITUOPCount( 0 );
	p_sm_map->_superModeMapAnalysis.init();


	CDSDBRSuperMode::rcode rval_collection = CDSDBRSuperMode::rcode::ok;
	CDSDBRSuperMode::rcode rval_writedata = CDSDBRSuperMode::rcode::ok;
	CDSDBRSuperMode::rcode rval_screening = CDSDBRSuperMode::rcode::ok;
	CDSDBRSuperMode::rcode rval_writeresults = CDSDBRSuperMode::rcode::ok;


	// collect data from live laser
	rval_collection = p_sm_map->collectMapsFromLaser();

	// removing, should already be set in loadSMMap function
	//// set laser_id and date_time_stamp
	//p_sm_map->_laser_id_CString = p_cg_system->getSystemLaserID();
	//p_sm_map->_date_time_stamp_CString = p_cg_system->getSystemDateTimeStamp();

	if( rval_collection == CDSDBRSuperMode::rcode::ok )
	{
		// write data to files
		rval_writedata = p_sm_map->writeMapsToFile();
	}


	short longitudinal_mode_count = 0;

#ifdef HARDWARE_BUILD

	if( rval_collection == CDSDBRSuperMode::rcode::ok )
	{
		// call screening procedure
		rval_screening = p_sm_map->screen();
	}

	if( p_sm_map->_superModeMapAnalysis._screeningPassed )
		p_cg_system->setSMScreeningPassed( p_sm_map->_sm_number, TRUE );
	else
		p_cg_system->setSMScreeningPassed( p_sm_map->_sm_number, FALSE );

	rval_writeresults = p_sm_map->writeScreeningResultsToFile();

	longitudinal_mode_count = p_sm_map->getLMCount();

#else // HARDWARE_BUILD

	// put a wait in that increments the percent complete
	for( int i = 10; i < 90; i+=10 )
	{
		p_cg_system->setPercentComplete( i );
		Sleep(100);
	}

	longitudinal_mode_count = 20;
	p_cg_system->setSMScreeningPassed( p_sm_map->_sm_number, TRUE );

#endif // HARDWARE_BUILD


	short sm_map_loaded_count = 0;
	for( int i = 0; i < (int)(p_cg_system->_overall_mode_map._supermodes.size()) ; i++ )
	{
		if( p_cg_system->_overall_mode_map._supermodes[i]._maps_loaded )
			sm_map_loaded_count++;
	}
	p_cg_system->setSMMapLoadedCount( sm_map_loaded_count );

	p_cg_system->setSystemState( CCLoseGridSystemAXCCtrl::state_type::complete );

	if( rval_screening != CDSDBRSuperMode::rcode::ok )
	{
		CString log_err_msg("screenSMMapProc() : ");
		log_err_msg += CString(p_sm_map->get_error_pair( rval_screening ).second.c_str());
		CGR_LOG(log_err_msg,WARNING_CGR_LOG)
	}

	if( rval_writeresults != CDSDBRSuperMode::rcode::ok )
	{
		CString log_err_msg("screenSMMapProc() : ");
		log_err_msg += CString(p_sm_map->get_error_pair( rval_writeresults ).second.c_str());
		CGR_LOG(log_err_msg,WARNING_CGR_LOG)
	}

	if( rval_collection != CDSDBRSuperMode::rcode::ok )
	{
		CGR_LOG(CString("screenSMMapProc() calling CCLoseGridSystemAXCCtrl::setLastError"),DIAGNOSTIC_CGR_LOG)
		p_cg_system->setLastError( p_sm_map->get_error_pair( rval_collection ) );
	}
	else
	if( rval_writedata != CDSDBRSuperMode::rcode::ok )
	{
		CGR_LOG(CString("screenSMMapProc() calling CCLoseGridSystemAXCCtrl::setLastError"),DIAGNOSTIC_CGR_LOG)
		p_cg_system->setLastError( p_sm_map->get_error_pair( rval_writedata ) );
	}
	else
	if( !(longitudinal_mode_count > 0) )
	{
		CGR_LOG(CString("screenSMMapProc() calling CCLoseGridSystemAXCCtrl::setLastError"),DIAGNOSTIC_CGR_LOG)
		p_cg_system->setLastError( error_std_pair( CGSYSTEM_NO_VALID_LONGITUDINAL_MODES_FOUND ) );
	}

	CGR_LOG(CString("screenSMMapProc() exiting"),DIAGNOSTIC_CGR_LOG)

	SERVERINTERFACE->closeLogFile();

	p_cg_system->setWorkerThreadID( NULL );

	AfxEndThread(0,TRUE);  // thread completed successfully

	return 0;
}

UINT characteriseProc( LPVOID pParam )
{
	// call this function to run in it's own thread
	// using AfxBeginThread
	CGR_LOG(CString("characteriseProc() entered"),DIAGNOSTIC_CGR_LOG)

	two_ptrs* p_two_ptrs =  (two_ptrs*)pParam;

	CCLoseGridSystemAXCCtrl* p_cg_system = p_two_ptrs->p_axc;


	CCGOperatingPoints* p_ops = (CCGOperatingPoints*)(p_two_ptrs->p_other);

	p_cg_system->setITUOPCount( 0 );
	p_cg_system->setDuffITUOPCount( 0 );

	CCGOperatingPoints::rtype rval = CCGOperatingPoints::rtype::ok;

//#ifdef HARDWARE_BUILD

	if( !CG_REG->get_DSDBR01_BASE_stub_hardware_calls() )
	{
		rval = p_ops->findITUOperatingPointsUsingWavemeter();
	}

	p_cg_system->setITUOPCount( p_ops->countITUOPsFound() );

//#else // HARDWARE_BUILD

	// put a wait in that increments the percent complete
//	for( int i = 10; i < 90; i+=10 )
//	{
//		p_cg_system->setPercentComplete( i );
//		Sleep(100);
//	}
//	p_cg_system->setITUOPCount( 160 );

//#endif // HARDWARE_BUILD


	p_cg_system->setSystemState( CCLoseGridSystemAXCCtrl::state_type::complete );

	if( rval != CCGOperatingPoints::rtype::ok )
	{
		CGR_LOG(CString("characteriseProc() calling CCLoseGridSystemAXCCtrl::setLastError"),DIAGNOSTIC_CGR_LOG)
		p_cg_system->setLastError( p_ops->get_error_pair( rval ) );
	}

	CGR_LOG(CString("characteriseProc() exiting"),DIAGNOSTIC_CGR_LOG)

	SERVERINTERFACE->closeLogFile();

	p_cg_system->setWorkerThreadID( NULL );

	AfxEndThread(0,TRUE);  // thread completed successfully

	return 0;
}

void
CCLoseGridSystemAXCCtrl::
handleCGResearchError(CString calling_function_name,HRESULT err)
{
	CString log_err_msg;
	log_err_msg = calling_function_name;
	log_err_msg += _T(" error: ");

	switch(err)
	{
		case CG_ERROR_SYSTEM_ERROR:
			log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_SYSTEM_ERROR) );
			CGR_LOG(log_err_msg,ERROR_CGR_LOG)
			CGSYSTEM_THROW_EXCEPTION(CGSYSTEM_SYSTEM_ERROR)
			break;			
		case CG_ERROR_CONNECTIVITY_FAILED:
			log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_CONNECTIVITY_ERROR) );
			CGR_LOG(log_err_msg,ERROR_CGR_LOG)
			CGSYSTEM_THROW_EXCEPTION(CGSYSTEM_CONNECTIVITY_ERROR)
			break;
		case CG_ERROR_POWER_METER_MISSING:
			log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_POWER_METER_NOT_FOUND_ERROR) );
			CGR_LOG(log_err_msg,ERROR_CGR_LOG)
			CGSYSTEM_THROW_EXCEPTION(CGSYSTEM_POWER_METER_NOT_FOUND_ERROR)
			break;
		case CG_ERROR_WAVEMETER_OFFLINE:
			log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_WAVEMETER_NOT_FOUND_ERROR) );
			CGR_LOG(log_err_msg,ERROR_CGR_LOG)
			CGSYSTEM_THROW_EXCEPTION(CGSYSTEM_WAVEMETER_NOT_FOUND_ERROR)
			break;
		case CG_ERROR_TEC_OFFLINE:
			log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_TEC_NOT_FOUND_ERROR) );
			CGR_LOG(log_err_msg,ERROR_CGR_LOG)
			CGSYSTEM_THROW_EXCEPTION(CGSYSTEM_TEC_NOT_FOUND_ERROR)
			break;
		case CG_ERROR_PXIT_READPOWER:
			log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_POWER_READ_ERROR) );
			CGR_LOG(log_err_msg,ERROR_CGR_LOG)
			CGSYSTEM_THROW_EXCEPTION(CGSYSTEM_POWER_READ_ERROR)
			break;
		case CG_ERROR_DUPLICATE_DEVICE_NAMES:
			log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_DUPLICATE_DEVICE_NAME_ERROR) );
			CGR_LOG(log_err_msg,ERROR_CGR_LOG)
			CGSYSTEM_THROW_EXCEPTION(CGSYSTEM_DUPLICATE_DEVICE_NAME_ERROR)
			break;
		case CG_ERROR_EMPTY_SLOTS_IN_SEQUENCE:
			log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_EMPTY_SEQUENCE_SLOTS_ERROR) );
			CGR_LOG(log_err_msg,ERROR_CGR_LOG)
			CGSYSTEM_THROW_EXCEPTION(CGSYSTEM_EMPTY_SEQUENCE_SLOTS_ERROR)
			break;
		case CG_ERROR_PXIT_MODULES_UNINITIALISED:
		case CG_ERROR_PXIT_MODULE_MISSING:
		case CG_ERROR_NO_PXIT_MODULES_FOUND:
			log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_PXIT_MODULE_ERROR) );
			CGR_LOG(log_err_msg,ERROR_CGR_LOG)
			CGSYSTEM_THROW_EXCEPTION(CGSYSTEM_PXIT_MODULE_ERROR)
			break;
		case CG_ERROR_DEVICE_NAME_NOT_ON_CARD:
			log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_DEVICE_NAME_NOT_ON_CARD_ERROR) );
			CGR_LOG(log_err_msg,ERROR_CGR_LOG)
			CGSYSTEM_THROW_EXCEPTION(CGSYSTEM_DEVICE_NAME_NOT_ON_CARD_ERROR)
			break;
		case CG_ERROR_306_FIRMWARE_OBSOLETE:
			log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_306_FIRMWARE_OBSOLETE_ERROR) );
			CGR_LOG(log_err_msg,ERROR_CGR_LOG)
			CGSYSTEM_THROW_EXCEPTION(CGSYSTEM_306_FIRMWARE_OBSOLETE_ERROR)
			break;
		case CG_ERROR_LASER_ALIGN_LOWPOWER:
		case CG_ERROR_LASER_ALIGN_MAXPOWER:
			log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_LASER_ALIGNMENT_ERROR) );
			CGR_LOG(log_err_msg,ERROR_CGR_LOG)
			CGSYSTEM_THROW_EXCEPTION(CGSYSTEM_LASER_ALIGNMENT_ERROR)
			break;
		case CG_ERROR_OUTPUT_NOT_CONNECTED:
		case CG_ERROR_SET_OUTPUT_RANGE:
		case CG_ERROR_SET_COMPLIANCE_VOLTAGE:
		case CG_ERROR_SET_OUTPUT_MODE:
		case CG_ERROR_SET_MAX_CURRENT:
			log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_HARDWARE_ERROR) );
			CGR_LOG(log_err_msg,ERROR_CGR_LOG)
			CGSYSTEM_THROW_EXCEPTION(CGSYSTEM_HARDWARE_ERROR)
			break;
		case CG_ERROR_LOGFILE_OPEN:
		case CG_ERROR_LOGFILE_DISK_FULL:
		case CG_ERROR_LOGFILE_CLOSE:
		case CG_ERROR_LOGFILEPATHNAME_REGISTRY_KEY:
		case CG_ERROR_LOGFILEPATHNAME_REGISTRY_DATA:
		case CG_ERROR_LOGGINGLEVEL_REGISTRY_KEY:
		case CG_ERROR_LOGGINGLEVEL_REGISTRY_DATA:
		case CG_ERROR_LOGFILEMAXSIZE_REGISTRY_KEY:
		case CG_ERROR_LOGFILEMAXSIZE_REGISTRY_DATA:
			log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_LOGFILE_ERROR) );
			CGR_LOG(log_err_msg,ERROR_CGR_LOG)
			CGSYSTEM_THROW_EXCEPTION(CGSYSTEM_LOGFILE_ERROR)
			break;
		case CG_ERROR_PXIT_SETCURRENT:
			log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_SET_CURRENT_ERROR) );
			CGR_LOG(log_err_msg,ERROR_CGR_LOG)
			CGSYSTEM_THROW_EXCEPTION(CGSYSTEM_SET_CURRENT_ERROR)
			break;
		case CG_ERROR_REGISTRY_DATA:
		case CG_ERROR_REGISTRY_KEY:
			log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_REGISTRY_ERROR) );
			CGR_LOG(log_err_msg,ERROR_CGR_LOG)
			CGSYSTEM_THROW_EXCEPTION(CGSYSTEM_REGISTRY_ERROR)
			break;
		case CG_ERROR_RESULTS_DISK_FULL:
			log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_RESULTS_DISK_FULL_ERROR) );
			CGR_LOG(log_err_msg,ERROR_CGR_LOG)
			CGSYSTEM_THROW_EXCEPTION(CGSYSTEM_RESULTS_DISK_FULL_ERROR)
			break;
		case CG_ERROR_PXIT_STEP_SIZE:
			log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_SEQUENCING_STEP_SIZE_ERROR) );
			CGR_LOG(log_err_msg,ERROR_CGR_LOG)
			CGSYSTEM_THROW_EXCEPTION(CGSYSTEM_SEQUENCING_STEP_SIZE_ERROR)
		break;
		case CG_ERROR_PXIT_SEQUENCE:
			log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_SEQUENCING_ERROR) );
			CGR_LOG(log_err_msg,ERROR_CGR_LOG)
			CGSYSTEM_THROW_EXCEPTION(CGSYSTEM_SEQUENCING_ERROR)
		break;
		case CG_ERROR_MODULE_NAME_NOT_FOUND:
			log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_MODULE_NAME_NOT_FOUND_ERROR) );
			CGR_LOG(log_err_msg,ERROR_CGR_LOG)
			CGSYSTEM_THROW_EXCEPTION(CGSYSTEM_MODULE_NAME_NOT_FOUND_ERROR)
		break;
		case CG_ERROR_PXITRELAY_ERROR:
			log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_PXITRELAY_ERROR) );
			CGR_LOG(log_err_msg,ERROR_CGR_LOG)
			CGSYSTEM_THROW_EXCEPTION(CGSYSTEM_PXITRELAY_ERROR)
		break;
		case CG_ERROR_INVALID_ARGUMENT:
			log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_INVALID_ARGUMENT) );
			CGR_LOG(log_err_msg,ERROR_CGR_LOG)
			CGSYSTEM_THROW_EXCEPTION(CGSYSTEM_INVALID_ARGUMENT)
		break;
		default:
			log_err_msg.Append( CGSYSTEM_ERROR_MESSAGE(CGSYSTEM_UNKNOWN_ERROR) );
			CGR_LOG(log_err_msg,ERROR_CGR_LOG)
			CGSYSTEM_THROW_EXCEPTION(CGSYSTEM_UNKNOWN_ERROR)
			break;
	}
}




// delegate standard IDispatch methods to MFC IDispatch implementation
DELEGATE_DUAL_INTERFACE(CCLoseGridSystemAXCCtrl, Dual_DCGSystemAXC)

// Our method and property functions can generally just
// delegate back to the methods we generated using
// ClassWizard. However, if we set up properties to
// access variables directly, we will need to write the
//  code to get/put the value into the variable.


STDMETHODIMP 
CCLoseGridSystemAXCCtrl::
XDual_DCGSystemAXC::
getVersionInfo(
			BSTR* p_product_name,
			BSTR* p_release_no,
			BSTR* p_copyright_notice,
			BSTR* p_label,
			BSTR* p_label_info,
			BSTR* p_label_comment,
			BSTR* p_build_configuration,
			BSTR* p_build_datetime,
			BSTR* p_driver_version_info )
{
	METHOD_PROLOGUE(CCLoseGridSystemAXCCtrl, Dual_DCGSystemAXC)

	TRY_DUAL(IID_IDual_DCGSystemAXC)
	{
		pThis->getVersionInfo(
			p_product_name,
			p_release_no,
			p_copyright_notice,
			p_label,
			p_label_info,
			p_label_comment,
			p_build_configuration,
			p_build_datetime,
			p_driver_version_info );

		return NOERROR;
	}

	CATCH_ALL_DUAL
}

STDMETHODIMP CCLoseGridSystemAXCCtrl::XDual_DCGSystemAXC::startup()
{
	METHOD_PROLOGUE(CCLoseGridSystemAXCCtrl, Dual_DCGSystemAXC)

	TRY_DUAL(IID_IDual_DCGSystemAXC)
	{
		pThis->startup();
		return NOERROR;
	}
	CATCH_ALL_DUAL
}

STDMETHODIMP CCLoseGridSystemAXCCtrl::XDual_DCGSystemAXC::setup(
			BSTR laser_type,
			BSTR laser_id,
			BSTR* p_date_time_stamp )
{
	METHOD_PROLOGUE(CCLoseGridSystemAXCCtrl, Dual_DCGSystemAXC)

	TRY_DUAL(IID_IDual_DCGSystemAXC)
	{
		pThis->setup(
			COLE2T( laser_type ),
			COLE2T( laser_id ),
			p_date_time_stamp );
		return NOERROR;
	}
	CATCH_ALL_DUAL
}

STDMETHODIMP CCLoseGridSystemAXCCtrl::XDual_DCGSystemAXC::shutdown()
{
	METHOD_PROLOGUE(CCLoseGridSystemAXCCtrl, Dual_DCGSystemAXC)

	TRY_DUAL(IID_IDual_DCGSystemAXC)
	{
		pThis->shutdown();
		return NOERROR;
	}
	CATCH_ALL_DUAL
}

STDMETHODIMP CCLoseGridSystemAXCCtrl::XDual_DCGSystemAXC::rampdown()
{
	METHOD_PROLOGUE(CCLoseGridSystemAXCCtrl, Dual_DCGSystemAXC)

	TRY_DUAL(IID_IDual_DCGSystemAXC)
	{
		pThis->rampdown();
		return NOERROR;
	}
	CATCH_ALL_DUAL
}


STDMETHODIMP CCLoseGridSystemAXCCtrl::XDual_DCGSystemAXC::getCurrent(
			BSTR module_name,
			double* p_current)
{
	METHOD_PROLOGUE(CCLoseGridSystemAXCCtrl, Dual_DCGSystemAXC)

	TRY_DUAL(IID_IDual_DCGSystemAXC)
	{
		pThis->getCurrent(
			COLE2T( module_name ),
			p_current);
		return NOERROR;
	}
	CATCH_ALL_DUAL
}



STDMETHODIMP CCLoseGridSystemAXCCtrl::XDual_DCGSystemAXC::setCurrent(
			BSTR module_name,
			double current )
{
	METHOD_PROLOGUE(CCLoseGridSystemAXCCtrl, Dual_DCGSystemAXC)

	TRY_DUAL(IID_IDual_DCGSystemAXC)
	{
		pThis->setCurrent(
			COLE2T( module_name ),
			current );
		return NOERROR;
	}
	CATCH_ALL_DUAL
}



STDMETHODIMP CCLoseGridSystemAXCCtrl::XDual_DCGSystemAXC::getOutputConnection(
			BSTR module_name,
			short* p_output_connection )
{
	METHOD_PROLOGUE(CCLoseGridSystemAXCCtrl, Dual_DCGSystemAXC)

	TRY_DUAL(IID_IDual_DCGSystemAXC)
	{
		pThis->getOutputConnection(
			COLE2T( module_name ),
			p_output_connection );
		return NOERROR;
	}
	CATCH_ALL_DUAL
}


STDMETHODIMP CCLoseGridSystemAXCCtrl::XDual_DCGSystemAXC::setOutputConnection(
			BSTR module_name,
			short output_connection )
{
	METHOD_PROLOGUE(CCLoseGridSystemAXCCtrl, Dual_DCGSystemAXC)

	TRY_DUAL(IID_IDual_DCGSystemAXC)
	{
		pThis->setOutputConnection(
			COLE2T( module_name ),
			output_connection );
		return NOERROR;
	}
	CATCH_ALL_DUAL
}

STDMETHODIMP CCLoseGridSystemAXCCtrl::XDual_DCGSystemAXC::get2DData(
			BSTR module_name,
			VARIANT* p_currents,
			short source_delay,
			short measure_delay,
			BSTR* p_results_dir,
			BSTR* p_date_time_stamp )
{
	METHOD_PROLOGUE(CCLoseGridSystemAXCCtrl, Dual_DCGSystemAXC)

	TRY_DUAL(IID_IDual_DCGSystemAXC)
	{
		pThis->get2DData(
			COLE2T( module_name ),
			p_currents,
			source_delay,
			measure_delay,
			p_results_dir,
			p_date_time_stamp );
		return NOERROR;
	}
	CATCH_ALL_DUAL
}


STDMETHODIMP CCLoseGridSystemAXCCtrl::XDual_DCGSystemAXC::get3DData(
			BSTR module_name_x,
			VARIANT* p_currents_x,
			BSTR module_name_y,
			VARIANT* p_currents_y,
			short map_type,
			short source_delay,
			short measure_delay,
			BSTR* p_results_dir,
			BSTR* p_date_time_stamp )
{
	METHOD_PROLOGUE(CCLoseGridSystemAXCCtrl, Dual_DCGSystemAXC)

	TRY_DUAL(IID_IDual_DCGSystemAXC)
	{
		pThis->get3DData(
			COLE2T( module_name_x ),
			p_currents_x,
			COLE2T( module_name_y ),
			p_currents_y,
			map_type,
			source_delay,
			measure_delay,
			p_results_dir,
			p_date_time_stamp );
		return NOERROR;
	}
	CATCH_ALL_DUAL
}



STDMETHODIMP CCLoseGridSystemAXCCtrl::XDual_DCGSystemAXC::startLaserScreeningOverallMap()
{
	METHOD_PROLOGUE(CCLoseGridSystemAXCCtrl, Dual_DCGSystemAXC)

	TRY_DUAL(IID_IDual_DCGSystemAXC)
	{
		pThis->startLaserScreeningOverallMap();
		return NOERROR;
	}
	CATCH_ALL_DUAL
}


STDMETHODIMP CCLoseGridSystemAXCCtrl::XDual_DCGSystemAXC::startLaserScreeningSMMap(
			short supermode_number )
{
	METHOD_PROLOGUE(CCLoseGridSystemAXCCtrl, Dual_DCGSystemAXC)

	TRY_DUAL(IID_IDual_DCGSystemAXC)
	{
		pThis->startLaserScreeningSMMap(
			supermode_number );
		return NOERROR;
	}
	CATCH_ALL_DUAL
}

STDMETHODIMP CCLoseGridSystemAXCCtrl::XDual_DCGSystemAXC::estimateITUOPs(
			BSTR* p_results_dir,
			BSTR* p_date_time_stamp,
			short* p_ITUOP_estimates_count )
{
	METHOD_PROLOGUE(CCLoseGridSystemAXCCtrl, Dual_DCGSystemAXC)

	TRY_DUAL(IID_IDual_DCGSystemAXC)
	{
		pThis->estimateITUOPs(
			p_results_dir,
			p_date_time_stamp,
			p_ITUOP_estimates_count );
		return NOERROR;
	}
	CATCH_ALL_DUAL
}



STDMETHODIMP CCLoseGridSystemAXCCtrl::XDual_DCGSystemAXC::startLaserCharacterisation()
{
	METHOD_PROLOGUE(CCLoseGridSystemAXCCtrl, Dual_DCGSystemAXC)

	TRY_DUAL(IID_IDual_DCGSystemAXC)
	{
		pThis->startLaserCharacterisation();
		return NOERROR;
	}
	CATCH_ALL_DUAL
}




STDMETHODIMP CCLoseGridSystemAXCCtrl::XDual_DCGSystemAXC::getPercentComplete(
			short* p_percent_complete )
{
	METHOD_PROLOGUE(CCLoseGridSystemAXCCtrl, Dual_DCGSystemAXC)

	TRY_DUAL(IID_IDual_DCGSystemAXC)
	{
		pThis->getPercentComplete(
			p_percent_complete );
		return NOERROR;
	}
	CATCH_ALL_DUAL
}


STDMETHODIMP CCLoseGridSystemAXCCtrl::XDual_DCGSystemAXC::getState(
			BSTR* p_state,
			short* p_overall_map_loaded,
			short* p_supermodes_found_count,
			short* p_supermode_map_loaded_count,
			short* p_ITUOP_estimate_count,
			short* p_ITUOP_count )
{
	METHOD_PROLOGUE(CCLoseGridSystemAXCCtrl, Dual_DCGSystemAXC)

	TRY_DUAL(IID_IDual_DCGSystemAXC)
	{
		pThis->getState(
			p_state,
			p_overall_map_loaded,
			p_supermodes_found_count,
			p_supermode_map_loaded_count,
			p_ITUOP_estimate_count,
			p_ITUOP_count );
		return NOERROR;
	}
	CATCH_ALL_DUAL
}


STDMETHODIMP CCLoseGridSystemAXCCtrl::XDual_DCGSystemAXC::getScreeningOverallMapResults(
			BSTR* p_results_dir,
			BSTR* p_laser_type,
			BSTR* p_laser_id,
			BSTR* p_date_time_stamp,
			BSTR* p_map_ramp_direction,
			short* p_screening_overall_map_passed,
			short* p_supermode_count,
			VARIANT* p_average_powers_on_middle_lines )
{
	METHOD_PROLOGUE(CCLoseGridSystemAXCCtrl, Dual_DCGSystemAXC)

	TRY_DUAL(IID_IDual_DCGSystemAXC)
	{
		pThis->getScreeningOverallMapResults(
			p_results_dir,
			p_laser_type,
			p_laser_id,
			p_date_time_stamp,
			p_map_ramp_direction,
			p_screening_overall_map_passed,
			p_supermode_count,
			p_average_powers_on_middle_lines );
		return NOERROR;
	}
	CATCH_ALL_DUAL
}


STDMETHODIMP CCLoseGridSystemAXCCtrl::XDual_DCGSystemAXC::getScreeningSMMapResults(
			short supermode_number,
			BSTR* p_results_dir,
			BSTR* p_laser_type,
			BSTR* p_laser_id,
			BSTR* p_date_time_stamp,
			BSTR* p_map_ramp_direction,
			short* p_screening_sm_map_passed,
			short* p_longitudinal_mode_count )
{
	METHOD_PROLOGUE(CCLoseGridSystemAXCCtrl, Dual_DCGSystemAXC)

	TRY_DUAL(IID_IDual_DCGSystemAXC)
	{
		pThis->getScreeningSMMapResults(
			supermode_number,
			p_results_dir,
			p_laser_type,
			p_laser_id,
			p_date_time_stamp,
			p_map_ramp_direction,
			p_screening_sm_map_passed,
			p_longitudinal_mode_count );
		return NOERROR;
	}
	CATCH_ALL_DUAL
}


STDMETHODIMP CCLoseGridSystemAXCCtrl::XDual_DCGSystemAXC::getCharacterisationResults(
			BSTR* p_results_dir,
			BSTR* p_date_time_stamp,
			short* p_ITUOP_count,
			short* p_duff_ITUOP_count )
{
	METHOD_PROLOGUE(CCLoseGridSystemAXCCtrl, Dual_DCGSystemAXC)

	TRY_DUAL(IID_IDual_DCGSystemAXC)
	{
		pThis->getCharacterisationResults(
			p_results_dir,
			p_date_time_stamp,
			p_ITUOP_count,
			p_duff_ITUOP_count );
		return NOERROR;
	}
	CATCH_ALL_DUAL
}


STDMETHODIMP CCLoseGridSystemAXCCtrl::XDual_DCGSystemAXC::collatePassFailResults(
			BSTR* p_results_dir,
			BSTR* p_date_time_stamp,
			short* p_screening_overall_map_passed,
			VARIANT* p_screening_sm_maps_passed,
			short* p_total_passed )
{
	METHOD_PROLOGUE(CCLoseGridSystemAXCCtrl, Dual_DCGSystemAXC)

	TRY_DUAL(IID_IDual_DCGSystemAXC)
	{
		pThis->collatePassFailResults(
			p_results_dir,
			p_date_time_stamp,
			p_screening_overall_map_passed,
			p_screening_sm_maps_passed,
			p_total_passed );
		return NOERROR;
	}
	CATCH_ALL_DUAL
}


STDMETHODIMP CCLoseGridSystemAXCCtrl::XDual_DCGSystemAXC::loadOverallMap(
			BSTR results_dir,
			BSTR laser_type,
			BSTR laser_id,
			BSTR date_time_stamp,
			BSTR map_ramp_direction )
{
	METHOD_PROLOGUE(CCLoseGridSystemAXCCtrl, Dual_DCGSystemAXC)

	TRY_DUAL(IID_IDual_DCGSystemAXC)
	{
		pThis->loadOverallMap(
			COLE2T( results_dir ),
			COLE2T( laser_type ),
			COLE2T( laser_id ),
			COLE2T( date_time_stamp ),
			COLE2T( map_ramp_direction ) );
		return NOERROR;
	}
	CATCH_ALL_DUAL
}


STDMETHODIMP CCLoseGridSystemAXCCtrl::XDual_DCGSystemAXC::loadSMMap(
			BSTR results_dir,
			BSTR laser_type,
			BSTR laser_id,
			BSTR date_time_stamp,
			short supermode_number,
			BSTR map_ramp_direction )
{
	METHOD_PROLOGUE(CCLoseGridSystemAXCCtrl, Dual_DCGSystemAXC)

	TRY_DUAL(IID_IDual_DCGSystemAXC)
	{
		pThis->loadSMMap(
			COLE2T( results_dir ),
			COLE2T( laser_type ),
			COLE2T( laser_id ),
			COLE2T( date_time_stamp ),
			supermode_number,
			COLE2T( map_ramp_direction ) );
		return NOERROR;
	}
	CATCH_ALL_DUAL
}


STDMETHODIMP CCLoseGridSystemAXCCtrl::XDual_DCGSystemAXC::readVoltage(
			BSTR module_name,
			double* p_voltage )
{
	METHOD_PROLOGUE(CCLoseGridSystemAXCCtrl, Dual_DCGSystemAXC)

	TRY_DUAL(IID_IDual_DCGSystemAXC)
	{
		pThis->readVoltage(
			COLE2T( module_name ),
			p_voltage );
		return NOERROR;
	}
	CATCH_ALL_DUAL
}


STDMETHODIMP CCLoseGridSystemAXCCtrl::XDual_DCGSystemAXC::readFrequency(
			double* p_frequency )
{
	METHOD_PROLOGUE(CCLoseGridSystemAXCCtrl, Dual_DCGSystemAXC)

	TRY_DUAL(IID_IDual_DCGSystemAXC)
	{
		pThis->readFrequency(
			p_frequency );
		return NOERROR;
	}
	CATCH_ALL_DUAL
}




STDMETHODIMP CCLoseGridSystemAXCCtrl::XDual_DCGSystemAXC::readCoarseFrequency( 
			double* p_coarse_frequency )
{
	METHOD_PROLOGUE(CCLoseGridSystemAXCCtrl, Dual_DCGSystemAXC)

	TRY_DUAL(IID_IDual_DCGSystemAXC)
	{
		pThis->readCoarseFrequency( 
			p_coarse_frequency );
		return NOERROR;
	}
	CATCH_ALL_DUAL
}



STDMETHODIMP CCLoseGridSystemAXCCtrl::XDual_DCGSystemAXC::readPhotodiodeCurrent( 
			BSTR module_name,
			short channel,
			double* p_current )
{
	METHOD_PROLOGUE(CCLoseGridSystemAXCCtrl, Dual_DCGSystemAXC)

	TRY_DUAL(IID_IDual_DCGSystemAXC)
	{
		pThis->readPhotodiodeCurrent( 
			COLE2T( module_name ),
			channel,
			p_current );
		return NOERROR;
	}
	CATCH_ALL_DUAL
}



STDMETHODIMP CCLoseGridSystemAXCCtrl::XDual_DCGSystemAXC::readOpticalPower( 
			BSTR module_name,
			short channel,
			double* p_optical_power )
{
	METHOD_PROLOGUE(CCLoseGridSystemAXCCtrl, Dual_DCGSystemAXC)

	TRY_DUAL(IID_IDual_DCGSystemAXC)
	{
		pThis->readOpticalPower( 
			COLE2T( module_name ),
			channel,
			p_optical_power );
		return NOERROR;
	}
	CATCH_ALL_DUAL
}




STDMETHODIMP CCLoseGridSystemAXCCtrl::XDual_DCGSystemAXC::readOpticalPowerAtFreq( 
			BSTR module_name,
			short channel,
			double frequency,
			double* p_optical_power )
{
	METHOD_PROLOGUE(CCLoseGridSystemAXCCtrl, Dual_DCGSystemAXC)

	TRY_DUAL(IID_IDual_DCGSystemAXC)
	{
		pThis->readOpticalPowerAtFreq( 
			COLE2T( module_name ),
			channel,
			frequency,
			p_optical_power );
		return NOERROR;
	}
	CATCH_ALL_DUAL
}




STDMETHODIMP CCLoseGridSystemAXCCtrl::XDual_DCGSystemAXC::readTemperature(
			double* p_temperature )
{
	METHOD_PROLOGUE(CCLoseGridSystemAXCCtrl, Dual_DCGSystemAXC)

	TRY_DUAL(IID_IDual_DCGSystemAXC)
	{
		pThis->readTemperature( 
			p_temperature );
		return NOERROR;
	}
	CATCH_ALL_DUAL
}



STDMETHODIMP CCLoseGridSystemAXCCtrl::XDual_DCGSystemAXC::getTECTempSetpoint( 
			double* p_temp_setpoint )
{
	METHOD_PROLOGUE(CCLoseGridSystemAXCCtrl, Dual_DCGSystemAXC)

	TRY_DUAL(IID_IDual_DCGSystemAXC)
	{
		pThis->getTECTempSetpoint( 
			p_temp_setpoint );
		return NOERROR;
	}
	CATCH_ALL_DUAL
}


STDMETHODIMP CCLoseGridSystemAXCCtrl::XDual_DCGSystemAXC::setTECTempSetpoint(
			double temp_setpoint )
{
	METHOD_PROLOGUE(CCLoseGridSystemAXCCtrl, Dual_DCGSystemAXC)

	TRY_DUAL(IID_IDual_DCGSystemAXC)
	{
		pThis->setTECTempSetpoint( 
			temp_setpoint );
		return NOERROR;
	}
	CATCH_ALL_DUAL
}


STDMETHODIMP CCLoseGridSystemAXCCtrl::XDual_DCGSystemAXC::calcCoarseFreqPolyCoeffs(
		short num_of_sample_pts, 
		short num_of_poly_coeffs,
		VARIANT* p_poly_coeffs,
		double* p_chi_squared  )
{
	METHOD_PROLOGUE(CCLoseGridSystemAXCCtrl, Dual_DCGSystemAXC)

	TRY_DUAL(IID_IDual_DCGSystemAXC)
	{
		pThis->calcCoarseFreqPolyCoeffs(
			num_of_sample_pts, 
			num_of_poly_coeffs,
			p_poly_coeffs,
			p_chi_squared );
		return NOERROR;
	}
	CATCH_ALL_DUAL
}


STDMETHODIMP CCLoseGridSystemAXCCtrl::XDual_DCGSystemAXC::getCoarseFreqPolyCoeffs(
		VARIANT* p_poly_coeffs )
{
	METHOD_PROLOGUE(CCLoseGridSystemAXCCtrl, Dual_DCGSystemAXC)

	TRY_DUAL(IID_IDual_DCGSystemAXC)
	{
		pThis->getCoarseFreqPolyCoeffs(
			p_poly_coeffs );
		return NOERROR;
	}
	CATCH_ALL_DUAL
}


STDMETHODIMP CCLoseGridSystemAXCCtrl::XDual_DCGSystemAXC::setCoarseFreqPolyCoeffs(
		VARIANT* p_poly_coeffs )
{
	METHOD_PROLOGUE(CCLoseGridSystemAXCCtrl, Dual_DCGSystemAXC)

	TRY_DUAL(IID_IDual_DCGSystemAXC)
	{
		pThis->setCoarseFreqPolyCoeffs(
			p_poly_coeffs );
		return NOERROR;
	}
	CATCH_ALL_DUAL
}


STDMETHODIMP CCLoseGridSystemAXCCtrl::XDual_DCGSystemAXC::getCoarseFrequency(
	double power_ratio,
	double* p_coarse_frequency )
{
	METHOD_PROLOGUE(CCLoseGridSystemAXCCtrl, Dual_DCGSystemAXC)

	TRY_DUAL(IID_IDual_DCGSystemAXC)
	{
		pThis->getCoarseFrequency(
			power_ratio,
			p_coarse_frequency );
		return NOERROR;
	}
	CATCH_ALL_DUAL
}



STDMETHODIMP CCLoseGridSystemAXCCtrl::XDual_DCGSystemAXC::getComplianceVoltage(
	BSTR module_name,
	double* p_compliance_voltage )
{
	METHOD_PROLOGUE(CCLoseGridSystemAXCCtrl, Dual_DCGSystemAXC)

	TRY_DUAL(IID_IDual_DCGSystemAXC)
	{
		pThis->getComplianceVoltage(
			COLE2T( module_name ),
			p_compliance_voltage );
		return NOERROR;
	}
	CATCH_ALL_DUAL
}


STDMETHODIMP CCLoseGridSystemAXCCtrl::XDual_DCGSystemAXC::setComplianceVoltage(
	BSTR module_name,
	double compliance_voltage )
{
	METHOD_PROLOGUE(CCLoseGridSystemAXCCtrl, Dual_DCGSystemAXC)

	TRY_DUAL(IID_IDual_DCGSystemAXC)
	{
		pThis->setComplianceVoltage(
			COLE2T( module_name ),
			compliance_voltage );
		return NOERROR;
	}
	CATCH_ALL_DUAL
}


STDMETHODIMP CCLoseGridSystemAXCCtrl::XDual_DCGSystemAXC::rampdownAndDisconnect()
{
	METHOD_PROLOGUE(CCLoseGridSystemAXCCtrl, Dual_DCGSystemAXC)

	TRY_DUAL(IID_IDual_DCGSystemAXC)
	{
		pThis->rampdownAndDisconnect();
		return NOERROR;
	}
	CATCH_ALL_DUAL
}


STDMETHODIMP CCLoseGridSystemAXCCtrl::XDual_DCGSystemAXC::stopAndShutdown()
{
	METHOD_PROLOGUE(CCLoseGridSystemAXCCtrl, Dual_DCGSystemAXC)

	TRY_DUAL(IID_IDual_DCGSystemAXC)
	{
		pThis->stopAndShutdown();
		return NOERROR;
	}
	CATCH_ALL_DUAL
}



STDMETHODIMP CCLoseGridSystemAXCCtrl::XDual_DCGSystemAXC::applyStableOP(
	short sm,
	short lm,
	double index,
	short apply_Gain_SOA_of_SM_map)
{
	METHOD_PROLOGUE(CCLoseGridSystemAXCCtrl, Dual_DCGSystemAXC)

	TRY_DUAL(IID_IDual_DCGSystemAXC)
	{
		pThis->applyStableOP(
			sm,
			lm,
			index,
			apply_Gain_SOA_of_SM_map);
		return NOERROR;
	}
	CATCH_ALL_DUAL
}




STDMETHODIMP CCLoseGridSystemAXCCtrl::XDual_DCGSystemAXC::applyPointOnSMMap(
	short sm,
	double Im_index,
	double Ip_index,
	short apply_Gain_SOA_of_SM_map )
{
	METHOD_PROLOGUE(CCLoseGridSystemAXCCtrl, Dual_DCGSystemAXC)

	TRY_DUAL(IID_IDual_DCGSystemAXC)
	{
		pThis->applyPointOnSMMap(
			sm,
			Im_index,
			Ip_index,
			apply_Gain_SOA_of_SM_map );
		return NOERROR;
	}
	CATCH_ALL_DUAL
}


STDMETHODIMP CCLoseGridSystemAXCCtrl::XDual_DCGSystemAXC::getPointOnSMMap(
	short sm,
	double* p_Im_index,
	double* p_Ip_index )
{
	METHOD_PROLOGUE(CCLoseGridSystemAXCCtrl, Dual_DCGSystemAXC)

	TRY_DUAL(IID_IDual_DCGSystemAXC)
	{
		pThis->getPointOnSMMap(
			sm,
			p_Im_index,
			p_Ip_index );
		return NOERROR;
	}
	CATCH_ALL_DUAL
}


STDMETHODIMP CCLoseGridSystemAXCCtrl::XDual_DCGSystemAXC::initWavemeter()
{
	METHOD_PROLOGUE(CCLoseGridSystemAXCCtrl, Dual_DCGSystemAXC)

	TRY_DUAL(IID_IDual_DCGSystemAXC)
	{
		pThis->initWavemeter();
		return NOERROR;
	}
	CATCH_ALL_DUAL
}


STDMETHODIMP CCLoseGridSystemAXCCtrl::XDual_DCGSystemAXC::loadImFromFile(
	short supermode_number,
	BSTR Im_filename )
{
	METHOD_PROLOGUE(CCLoseGridSystemAXCCtrl, Dual_DCGSystemAXC)

	TRY_DUAL(IID_IDual_DCGSystemAXC)
	{
		pThis->loadImFromFile(
			supermode_number,
			Im_filename );
		return NOERROR;
	}
	CATCH_ALL_DUAL
}



STDMETHODIMP CCLoseGridSystemAXCCtrl::XDual_DCGSystemAXC::loadImiddleFreqFromFile(
	short supermode_number,
	short longitudinalmode_number,
	BSTR ImiddleFreq_filename )
{
	METHOD_PROLOGUE(CCLoseGridSystemAXCCtrl, Dual_DCGSystemAXC)

	TRY_DUAL(IID_IDual_DCGSystemAXC)
	{
		pThis->loadImiddleFreqFromFile(
			supermode_number,
			longitudinalmode_number,
			ImiddleFreq_filename );
		return NOERROR;
	}
	CATCH_ALL_DUAL
}




// Implement ISupportErrorInfo to indicate we support the
// OLE Automation error handler.
IMPLEMENT_DUAL_ERRORINFO(CCLoseGridSystemAXCCtrl, IID_IDual_DCGSystemAXC)

// DUAL_SUPPORT_END

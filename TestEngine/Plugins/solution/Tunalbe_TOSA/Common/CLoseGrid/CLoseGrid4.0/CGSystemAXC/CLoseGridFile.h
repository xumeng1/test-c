
#ifndef __CLOSEGRIDFILE__H__
#define __CLOSEGRIDFILE__H__

#pragma once

#include <afx.h>
#include <fstream>
#include <vector>

using namespace std;

///////////////////////////////////////////////////////////////////////////////

#define	TAB_DELIMITOR		"	"
#define	CSV_DELIMITOR		","
#define	MAX_ROW_LENGTH		100000
#define	MAX_COL_LENGTH		100000
#define ENDLINE_CHAR		'\n'
#define	EXT_SEPERATOR		"."
#define	FOLDER_SEPERATOR	"\\"
#define	DELIMITORS			",\t\r\b\f\n"

#define	TEXTFILE_EXTENSION	"txt"
#define	CSVFILE_EXTENSION	"csv"

#define	HEADER_ROW	1

// Errors
#define	CG_NO_ERROR			0
#define	ERROR_FILEEXISTS	-1
#define	ERROR_FILEOPEN		-2
#define	ERROR_FILENOTFOUND	-3
#define	ERROR_FILECLOSED	-4
#define	ERROR_ENDOFFILE		-5

///////////////////////////////////////////////////////////////////////////////

class CLoseGridFile : public CFile
{
public:
	CLoseGridFile(CString filePath);	// e.g. "C:\\PXIT\theFile.csv"

	CLoseGridFile(CString folderPath, 
				CString fileNameWithExtension);	

	CLoseGridFile(CString folderPath, 
				CString fileNameWithExtension,
				CString	delimitor);	

	CLoseGridFile(CString folderPath,	// e.g. "C:\\PXIT"
				CString	fileName,		// e.g.	"results"
				CString	extension,
				CString	delimitor);		// e.g. ","

	CLoseGridFile();
	~CLoseGridFile();

	int		createFolderAndFile();
	int		openFileForReading();
	int		openFileForWriting();

	int		addPage(vector<double>&	array,
					int	rowSize);
	int		addRow(CStringArray& rowArray);
	int		addEmptyLine();

	int		readFile(CStringArray& fileArray);
	int		readFile(vector<double>& fileContents);
	int		readRow(int	rowNumber,
					CStringArray&	rowArray);
	int		readColumn(int			columnNumber,
					bool			includeHeader,
					vector<double>&	columnArray);
	int		readColumn(int			columnNumber,
					bool			includeHeader,
					vector<int>&	columnArray);
	int		readColumn(int			columnNumber,
					bool			includeHeader,
					CStringArray&	columnArray);

				// return number of rows in file
	int		numRows();

	int		_numRows;

protected:

	// example of a file = C:\\PXIT\\Laser Run Results\\478\\2003_06_28_14_55_59\\CLoseGridConfigFile.csv

	CString			_fileName;	// CLoseGridConfigFile

	CString			_fileNameWithExtension;	// CLoseGridConfigFile.csv

	CString			_filePath;	// C:\\PXIT\\Laser Run Results\\478\\2003_06_28_14_55_59\\CLoseGridConfigFile.csv

	CString			_fileExtension;	// csv, txt etc

	CString			_folder;	// 2003_06_28_14_55_59

	CString			_folderPath; // C:\\PXIT\\Laser Run Results\\478\\2003_06_28_14_55_59

	CString			_drive;		// C:

    CFileStatus		_fileStatus;

						// the handle to our file
	std::ofstream*	_writeFileStream;

						// the handle to our file
	std::ifstream*	_readFileStream;

						// delimitor to use when reading from
						// writing to the file e.g. , . tab etc
	CString			_delimitor;


	//
	///
	//////////////////////////////////////////////////////////////////////
	///
	//

	void	setFileExtension();
	void	setFileName();
	void	setFolder();
	void	setDrive();

	void	splitIntoSeperateStrings(CString		theString,
									CStringArray&	stringArray,
									CString			seperator);

	void	splitIntoSeperateStrings(CString		theString,
									CStringArray&	stringArray);

};
#endif	// __CLoseGridFile__H__
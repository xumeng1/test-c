// Copyright (c) 2004 Tsunami Photonics Ltd. All Rights Reserved

///////////////////////////////////////////////////////////////////////////////
///
//	Class	:
//
//	Contents:
//
//	Author	:
//
///
///////////////////////////////////////////////////////////////////////////////
///
//
#include "stdafx.h"

#include "CGResearchServerInterface.h"
#include "CGRegValues.h"
#include "defaults.h"
#include <fstream>
#include <map>


#ifndef HARDWARE_BUILD 
// only using fake frequencies for testing in software-only builds
static bool Iramp_vs_fakefreq_table = false;
static std::map<double,double> i_vs_FakeFreq;
static double last_i_setting = 0;
int loadIvsFakeFreqFromFile();
#endif // HARDWARE_BUILD

#define new DEBUG_NEW

/////////////////////////////////////////////////////////////////////
// Only instance of this class
CGResearchServerInterface* CGResearchServerInterface::theInstance = NULL;

/////////////////////////////////////////////////////////////////////

//
///	ctor and dtor
//
CGResearchServerInterface::
CGResearchServerInterface()
{
		// CGResearchServerInterface is a singleton class
	theInstance = this;

	InitializeCriticalSection(&_safe_access);

	_closeGridServerPtr = NULL;
}

CGResearchServerInterface::
~CGResearchServerInterface(void)
{
	DeleteCriticalSection(&_safe_access);
}

/////////////////////////////////////////////////////////////////////
///
//	Function	:
//
//	Returns		:		
//	Parameters	:	
//	Description	:
//	Updates		:	
//
CGResearchServerInterface* 
CGResearchServerInterface::instance()
{
	// If we're calling this for
	// the first time, create our
	// instance, otherwise disallow
	// any more being created
	if(theInstance == NULL)
		static CGResearchServerInterface theInstance;
	return theInstance;
}

//
/////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
///
//	User exposed functions (public functions)
//
void
CGResearchServerInterface::
setPointerToServer(IDual_DCGResearch* closeGridServerPtr)
{
	//EnterCriticalSection(&_safe_access);

	_closeGridServerPtr = closeGridServerPtr;

	//LeaveCriticalSection(&_safe_access);
}

//
///	Configuration Interface ///////////////////////////////////////////////////
//


//
/////////////////////////////////////////////////////////////////////

//
///	Testing Interface /////////////////////////////////////////////////////////
//

/////////////////////////////////////////////////////////////////////
///
//	Function	:	generateLivPlot
//
//	Returns		:	void
//
//	Parameters	:	void
//
//	Description	:	
//
HRESULT
CGResearchServerInterface::
startup()
{
	//EnterCriticalSection(&_safe_access);

	HRESULT err = S_OK;

	err = _closeGridServerPtr->startup();

	//LeaveCriticalSection(&_safe_access);

#ifndef HARDWARE_BUILD
// only using fake frequencies for testing in software-only builds

	// Try to read in table of Iramp Vs fake frequency once

	if( loadIvsFakeFreqFromFile() == 0 )
		Iramp_vs_fakefreq_table = true;
	else
		Iramp_vs_fakefreq_table = false;

#endif // HARDWARE_BUILD

	return err;
}
//
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
///
//	Function	:	
//
//	Returns		:	void
//
//	Parameters	:	void
//
//	Description	:	
//
HRESULT
CGResearchServerInterface::
setup(CString laserIdIn,
	  CString laserTypeIn,
	  CString timeStampIn,
	  CString &err_msg)
{
	//EnterCriticalSection(&_safe_access);

	HRESULT err = S_OK;

	BSTR laserId	= laserIdIn.AllocSysString();
	BSTR laserType	= laserTypeIn.AllocSysString();
	BSTR timeStamp	= timeStampIn.AllocSysString();
	BSTR err_msg_BSTR = NULL;

	err = _closeGridServerPtr->setup(laserId, laserType, timeStamp, &err_msg_BSTR);

	if(err_msg_BSTR != NULL) err_msg = CString(err_msg_BSTR);

	SysFreeString(laserId);
	SysFreeString(laserType);
	SysFreeString(timeStamp);
	SysFreeString(err_msg_BSTR);

	//LeaveCriticalSection(&_safe_access);

	return err;
}
//
/////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////
///
//	Function	:	
//
//	Returns		:	void
//
//	Parameters	:	void
//
//	Description	:	
//
HRESULT
CGResearchServerInterface::
setLogFileName(CString laserIdIn,
	  CString laserTypeIn,
	  CString timeStampIn)
{
	//EnterCriticalSection(&_safe_access);

	HRESULT err = S_OK;

	BSTR laserId	= laserIdIn.AllocSysString();
	BSTR laserType	= laserTypeIn.AllocSysString();
	BSTR timeStamp	= timeStampIn.AllocSysString();

	err = _closeGridServerPtr->setLogFileName(laserId, laserType, timeStamp);

	SysFreeString(laserId);
	SysFreeString(laserType);
	SysFreeString(timeStamp);

	//LeaveCriticalSection(&_safe_access);

	return err;
}
//
/////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////
///
//	Function	:	
//
//	Returns		:	void
//
//	Parameters	:	void
//
//	Description	:	
//
HRESULT
CGResearchServerInterface::
getVersionInfo(CString* productNameStr,
			CString* versionNameStr,
			CString* labelStr,
			CString* labelInfoStr,
			CString* labelCommentStr,
			CString* buildConfigStr)
{
	//EnterCriticalSection(&_safe_access);

	HRESULT err = S_OK;

	BSTR productName	= 0;
	BSTR versionName	= 0;
	BSTR label			= 0;
	BSTR labelInfo		= 0;
	BSTR labelComment	= 0;
	BSTR buildConfig	= 0;	
	
	err = _closeGridServerPtr->getVersionInfo(&productName,
											&versionName,
											&label,
											&labelInfo,
											&labelComment,
											&buildConfig);

	if(SUCCEEDED(err))
	{
		if(productName!=0)
			*productNameStr = CString(productName);
		if(versionName!=0)
			*versionNameStr = CString(versionName);
		if(label!=0)
			*labelStr = CString(label);
		if(labelInfo!=0)
			*labelInfoStr = CString(labelInfo);
		if(labelComment!=0)
			*labelCommentStr = CString(labelComment);
		if(buildConfig!=0)
			*buildConfigStr = CString(buildConfig);
	}
						
	SysFreeString(productName);
	SysFreeString(versionName);
	SysFreeString(label);
	SysFreeString(labelInfo);
	SysFreeString(labelComment);
	SysFreeString(buildConfig);

	//LeaveCriticalSection(&_safe_access);

	return err;
}
//
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
///
//	Function	:	
//
//	Returns		:	void
//
//	Parameters	:	void
//
//	Description	:	
//
HRESULT
CGResearchServerInterface::
getState(CString* state)
{
	//EnterCriticalSection(&_safe_access);

	HRESULT err = S_OK;

	BSTR currentState;

	err = _closeGridServerPtr->getState(&currentState);
		
	if(SUCCEEDED(err))
	{
		*state = CString(currentState);
	}

    SysFreeString(currentState);

	//LeaveCriticalSection(&_safe_access);

	return err;
}
//
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
///
//	Function	:	
//
//	Returns		:	void
//
//	Parameters	:	void
//
//	Description	:	
//
HRESULT
CGResearchServerInterface::
setCurrent(CString moduleNameIn,
		   double currentIn)
{
#ifdef HARDWARE_BUILD 
	//EnterCriticalSection(&_safe_access);

	HRESULT err = S_OK;

	BSTR moduleName = moduleNameIn.AllocSysString();

	err = _closeGridServerPtr->setCurrent(moduleName, currentIn);
		
	SysFreeString(moduleName);


#else // HARDWARE_BUILD 
// only using fake frequencies for testing in software-only builds
	if( moduleNameIn == CG_REG->get_DSDBR01_CHAR_fakefreq_Iramp_module_name() )
	{
		last_i_setting = currentIn;
	}
	HRESULT err = S_OK;
#endif // HARDWARE_BUILD


	//LeaveCriticalSection(&_safe_access);

	return err;
}

HRESULT
CGResearchServerInterface::
getCurrent(CString moduleNameIn, 
			double*	currentOut)
{
	//EnterCriticalSection(&_safe_access);

	HRESULT err = S_OK;

	BSTR moduleName = moduleNameIn.AllocSysString();

	err = _closeGridServerPtr->getCurrent(moduleName, currentOut);

	SysFreeString(moduleName);

	//LeaveCriticalSection(&_safe_access);

	return err;
}


HRESULT
CGResearchServerInterface::
readVoltage(CString moduleNameIn, 
			double*	voltageOut)
{
	//EnterCriticalSection(&_safe_access);

	HRESULT err = S_OK;

	BSTR moduleName = moduleNameIn.AllocSysString();

	err = _closeGridServerPtr->readVoltage(moduleName, voltageOut);

	SysFreeString(moduleName);

	//LeaveCriticalSection(&_safe_access);

	return err;
}


/////////////////////////////////////////////////////////////////////
///
//	Function	:	
//
//	Returns		:	void
//
//	Parameters	:	void
//
//	Description	:	
//
HRESULT
CGResearchServerInterface::
setComplianceVoltage(CString moduleNameIn,
		   double voltageIn)
{
	//EnterCriticalSection(&_safe_access);

	HRESULT err = S_OK;

	BSTR moduleName = moduleNameIn.AllocSysString();

	
	err = _closeGridServerPtr->setComplianceVoltage(moduleName, voltageIn);
	
	
	SysFreeString(moduleName);

	//LeaveCriticalSection(&_safe_access);

	return err;
}

HRESULT
CGResearchServerInterface::
getComplianceVoltage(CString moduleNameIn, 
			double*	voltageOut)
{
	//EnterCriticalSection(&_safe_access);

	HRESULT err = S_OK;

	BSTR moduleName = moduleNameIn.AllocSysString();

	err = _closeGridServerPtr->getComplianceVoltage(moduleName, voltageOut);

	SysFreeString(moduleName);

	//LeaveCriticalSection(&_safe_access);

	return err;
}

//
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
///
//	Function	:	
//
//	Returns		:	void
//
//	Parameters	:	void
//
//	Description	:	
//
HRESULT
CGResearchServerInterface::
setOutputConnection(CString moduleNameIn,
					short	outputConnection)
{
#ifdef HARDWARE_BUILD
	//EnterCriticalSection(&_safe_access);

	HRESULT err = S_OK;

	BSTR moduleName = moduleNameIn.AllocSysString();

	err = _closeGridServerPtr->setOutputConnection(moduleName, outputConnection);
		
	SysFreeString(moduleName);

	//LeaveCriticalSection(&_safe_access);

#else // HARDWARE_BUILD
	HRESULT err = S_OK;
#endif // HARDWARE_BUILD

	return err;
}

//
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
///
//	Function	:	
//
//	Returns		:	void
//
//	Parameters	:	void
//
//	Description	:	
//
HRESULT
CGResearchServerInterface::
setInSequence(CString moduleNameIn,
			short	inSequence)
{
	//EnterCriticalSection(&_safe_access);

	HRESULT err = S_OK;

	BSTR moduleName = moduleNameIn.AllocSysString();

	err = _closeGridServerPtr->setInSequence(moduleName, inSequence);
		
	SysFreeString(moduleName);

	//LeaveCriticalSection(&_safe_access);

	return err;
}

//
/////////////////////////////////////////////////////////////////////

HRESULT
CGResearchServerInterface::
load305Sequence(CString			moduleNameIn,
				vector<double>&	currents,
				int				sourceDelay,
				int				measureDelay)
{
	//EnterCriticalSection(&_safe_access);

	HRESULT err = S_OK;

	BSTR		moduleName = moduleNameIn.AllocSysString();
	VARIANT		varCurrents;
	convertVectorToVariant(currents,varCurrents);

	err = _closeGridServerPtr->load305Sequence(moduleName, &varCurrents, sourceDelay, measureDelay);


	//SafeArrayDestroyData(varCurrents.parray);
	//VariantClear(&varCurrents);
	SysFreeString(moduleName);

	//LeaveCriticalSection(&_safe_access);

	return err;
}

HRESULT	
CGResearchServerInterface::
load306Sequence(CString	moduleNameIn,
				short	numPoints,
				short	channel,
				int		sourceDelay,
				int		measureDelay)
{
	//EnterCriticalSection(&_safe_access);

	HRESULT err = S_OK;

	BSTR moduleName = moduleNameIn.AllocSysString();

	err = _closeGridServerPtr->load306Sequence(moduleName, 
											numPoints,
											channel,
											sourceDelay,
											measureDelay);

	SysFreeString(moduleName);

	//LeaveCriticalSection(&_safe_access);

	return err;
}

HRESULT	
CGResearchServerInterface::
resetSequences()
{
	//EnterCriticalSection(&_safe_access);

	HRESULT err = S_OK;

	err = _closeGridServerPtr->resetSequences();

	//LeaveCriticalSection(&_safe_access);

	return err;
}

HRESULT	
CGResearchServerInterface::
reset305Sequences()
{
	//EnterCriticalSection(&_safe_access);

	HRESULT err = S_OK;

	err = _closeGridServerPtr->reset305Sequences();

	//LeaveCriticalSection(&_safe_access);

	return err;
}

HRESULT	
CGResearchServerInterface::
reset306Sequences()
{
	//EnterCriticalSection(&_safe_access);

	HRESULT err = S_OK;

	err = _closeGridServerPtr->reset306Sequences();

	//LeaveCriticalSection(&_safe_access);

	return err;
}

HRESULT	
CGResearchServerInterface::
runSequences()
{
	//EnterCriticalSection(&_safe_access);

	HRESULT err = S_OK;

	err = _closeGridServerPtr->runSequences();

	//LeaveCriticalSection(&_safe_access);

	return err;
}

HRESULT	
CGResearchServerInterface::
setCoarseFrequencyOffset(double powerRatio,
						double	frequency)
{
	//EnterCriticalSection(&_safe_access);

	HRESULT err = S_OK;

	err = _closeGridServerPtr->setCoarseFrequencyOffset(powerRatio, frequency);

	//LeaveCriticalSection(&_safe_access);

	return err;
}


HRESULT	
CGResearchServerInterface::
setTECPIDValues(double	pValue,
				double	iValue,
				double	dValue)
{
	//EnterCriticalSection(&_safe_access);

	HRESULT err = S_OK;

	err = _closeGridServerPtr->setTECPIDValues(pValue, iValue, dValue);

	//LeaveCriticalSection(&_safe_access);

	return err;
}

HRESULT	
CGResearchServerInterface::
getTECPIDValues(double*	pValue,
				double*	iValue,
				double*	dValue)
{
	//EnterCriticalSection(&_safe_access);

	HRESULT err = S_OK;

	err = _closeGridServerPtr->getTECPIDValues(pValue, iValue, dValue);

	//LeaveCriticalSection(&_safe_access);

	return err;
}

/////////////////////////////////////////////////////////////////////
///
//	Function	:	
//
//	Returns		:	void
//
//	Parameters	:	void
//
//	Description	:	
//
HRESULT
CGResearchServerInterface::
getOutputConnection(CString moduleNameIn,
					short*	outputConnection)
{
#ifdef HARDWARE_BUILD

	//EnterCriticalSection(&_safe_access);

	HRESULT err = S_OK;

	BSTR moduleName = moduleNameIn.AllocSysString();

	err = _closeGridServerPtr->getOutputConnection(moduleName, outputConnection);
		
	SysFreeString(moduleName);

	//LeaveCriticalSection(&_safe_access);

#else // HARDWARE_BUILD
	HRESULT err = S_OK;
	*outputConnection = CONNECT_OUTPUT;
#endif // HARDWARE_BUILD

	return err;
}

//
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
///
//	Function	:	
//
//	Returns		:	void
//
//	Parameters	:	void
//
//	Description	:	
//
HRESULT
CGResearchServerInterface::
getSequenceCount(CString moduleNameIn,
				short*	sequenceCount)
{
	//EnterCriticalSection(&_safe_access);

	HRESULT err = S_OK;

	BSTR moduleName = moduleNameIn.AllocSysString();

	err = _closeGridServerPtr->getSequenceCount(moduleName, sequenceCount);
		
	SysFreeString(moduleName);

	//LeaveCriticalSection(&_safe_access);

	return err;
}

//
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
///
//	Function	:	readFrequency
//
//	Returns		:	HRESULT
//
//	Parameters	:	double* freq
//
//	Description	:	
//
HRESULT
CGResearchServerInterface::
readFrequency(double* freq)
{
	//EnterCriticalSection(&_safe_access);

	HRESULT err = S_OK;

	err = _closeGridServerPtr->readFrequency(freq);

	if( err != S_OK
	 || *freq < MIN_VALID_FREQ_MEAS
	 || *freq > MAX_VALID_FREQ_MEAS )
	{
		// a problem occurred
		// try to reset and read frequency again

		for( int i = 0 ;
			 i < CG_REG->get_DSDBR01_CMDC_wavemeter_retries() ;
			 i++ )
		{
			// reinitialize wavemeter
			err = _closeGridServerPtr->initWavemeter();
			if( err != S_OK )
			{ // can't reinitialize without error, then exit
				break;
			}

			// have reinitialized okay, reread frequency
			err = _closeGridServerPtr->readFrequency(freq);
			if( err == S_OK
			 && *freq >= MIN_VALID_FREQ_MEAS
			 && *freq <= MAX_VALID_FREQ_MEAS )
			{ // read successfully
				break;
			}
		}

	}


#ifndef HARDWARE_BUILD
if( Iramp_vs_fakefreq_table )
{
	// Table exists => find frequency to return
	// last_i_setting is set in setCurrent(Iramp_module_name...)

	double fakeFreq_found = 0;
	double fakeFreq = 0;
	double prev_fakeFreq = 0;
	double Iramp_value = 0;
	double prev_Iramp_value = 0;

	for( std::map<double,double>::iterator map_iter = i_vs_FakeFreq.begin();
		map_iter != i_vs_FakeFreq.end();
		map_iter++ )
	{
		prev_fakeFreq = fakeFreq;
		fakeFreq = (*map_iter).second;

		prev_Iramp_value = Iramp_value;
		Iramp_value = (*map_iter).first;

		if( last_i_setting == Iramp_value )
		{
			// found exact entry in table for Iramp
			fakeFreq_found = fakeFreq;
			break;
		}

		if( prev_Iramp_value > 0 && last_i_setting > prev_Iramp_value && last_i_setting < Iramp_value )
		{
			// found Iramp between two entries in table
			fakeFreq_found = prev_fakeFreq + (fakeFreq-prev_fakeFreq)*(last_i_setting-prev_Iramp_value)/(Iramp_value-prev_Iramp_value);
			break;
		}

	}
	if( fakeFreq_found > 0 )
	{
		double fakefreq_error = CG_REG->get_DSDBR01_CHAR_fakefreq_max_error();
		if( fakefreq_error > 0 )
		{
			fakeFreq_found += fakefreq_error*((2*(double)rand()/(double)RAND_MAX)-1);
		}

		*freq = fakeFreq_found;
		err = S_OK;
	}
}
#endif // HARDWARE_BUILD


	//LeaveCriticalSection(&_safe_access);

	return err;
}
//
/////////////////////////////////////////////////////////////////////




#ifndef HARDWARE_BUILD
// only using fake frequencies for testing in software-only builds

int loadIvsFakeFreqFromFile()
{
	int rval = 0;
    long row_count = 0;
    long first_line_col_count = 0;
    long col_count = 0;
	std::vector<double> I_values;
	std::vector<double> F_values;
	CFileStatus status;

	i_vs_FakeFreq.clear();

    // Check file

	CString abs_filepath = CG_REG->get_DSDBR01_CHAR_Iramp_vs_fakefreq_abspath();

	if( CFile::GetStatus( abs_filepath, status ) )
	{
		// File exists => attempt to read it's contents
		try
		{
			std::ifstream file_stream( abs_filepath );

			if( file_stream )
			{
				row_count = 0;

				while( !file_stream.eof() )
				{
					// File stream is open, read each line
					char temp_buffer[MAX_LINE_LENGTH];
					*temp_buffer = '\0';
					file_stream.getline( temp_buffer, MAX_LINE_LENGTH );

					int temp_buffer_lenght = (int)strlen(temp_buffer);
					if( temp_buffer_lenght > 0 ) // there is something on the line
					{
        				col_count = 0;
						char *fp = strtok(temp_buffer,DELIMITERS);
						while(fp)
						{
							double I_value = atof(fp);
							I_values.push_back(I_value);
							col_count++;
							
							fp = strtok(NULL,DELIMITERS);

							double F_value = atof(fp);
							F_values.push_back(F_value);
							col_count++;

							fp = strtok(NULL,DELIMITERS);

						}

						if( col_count != 2 )
						{
							rval = 1; // file_format_invalid;
							break;
						}

						if(row_count == 0) first_line_col_count = col_count;

						row_count++;
					}

					if( row_count > 0 && col_count != first_line_col_count )
					{
						rval = 1; // file_format_invalid;
						break;
					}
				}
			}
			else
			{
        		rval = 1; // could_not_open_file;
			}
		}
		catch(...)
		{
			rval = 1; // could_not_open_file;
		}

	}
	else
	{
		rval = 1; // file_does_not_exist;
	}

    if( rval == 0 && col_count != 2 )
	{
		rval = 1; // file_format_invalid;
	}

    if( rval == 0 && ( I_values.size() != F_values.size() ) )
	{
		rval = 1; // file_format_invalid;
	}

	if( rval == 0 )
	{
		for(int i = 0; i < (int)(I_values.size()) ; i++ )
		{
			i_vs_FakeFreq.insert( std::map<double,double>::value_type( I_values[i], F_values[i] ) );
		}
	}

	return rval;
}


#endif // HARDWARE_BUILD






















/////////////////////////////////////////////////////////////////////
///
//	Function	:	
//
//	Returns		:	void
//
//	Parameters	:	void
//
//	Description	:	
//
HRESULT
CGResearchServerInterface::
readTemperature(double* temperature)
{
	//EnterCriticalSection(&_safe_access);

	HRESULT err = S_OK;

	err = _closeGridServerPtr->readTemperature(temperature);
										
	//LeaveCriticalSection(&_safe_access);

	return err;
}
//
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
///
//	Function	:	
//
//	Returns		:	void
//
//	Parameters	:	void
//
//	Description	:	
//
HRESULT
CGResearchServerInterface::
readPower(int		slotNumber,
		  int		channelNumber,
		  double*	power)
{
#ifdef HARDWARE_BUILD
	//EnterCriticalSection(&_safe_access);

	HRESULT err = S_OK;

	err = _closeGridServerPtr->readPower(slotNumber,
										channelNumber,
										power);
										
	//LeaveCriticalSection(&_safe_access);

#else // HARDWARE_BUILD
	HRESULT err = S_OK;
	*power = MIN_DIRECT_OPTICAL_POWER;
#endif // HARDWARE_BUILD
	return err;
}
//
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
///
//	Function	:	
//
//	Returns		:	void
//
//	Parameters	:	void
//
//	Description	:	
//
HRESULT
CGResearchServerInterface::
readOpticalPower(CString	moduleName,
				int			channel,
				double*		opticalPower)
{
#ifdef HARDWARE_BUILD
	//EnterCriticalSection(&_safe_access);

	HRESULT err = S_OK;

	BSTR varModuleName = moduleName.AllocSysString();

	err = _closeGridServerPtr->readOpticalPower(varModuleName,
												channel,
												opticalPower);
					
    SysFreeString(varModuleName);

	//LeaveCriticalSection(&_safe_access);

#else // HARDWARE_BUILD
	HRESULT err = S_OK;
	*opticalPower = MIN_DIRECT_OPTICAL_POWER;
#endif // HARDWARE_BUILD

	return err;
}
//
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
///
//	Function	:	
//
//	Returns		:	void
//
//	Parameters	:	void
//
//	Description	:	
//
HRESULT
CGResearchServerInterface::
readOpticalPowers(CString	moduleName,
				double*		opticalPowerCh1,
				double*		opticalPowerCh2)
{
#ifdef HARDWARE_BUILD
	//EnterCriticalSection(&_safe_access);

	HRESULT err = S_OK;

	BSTR varModuleName = moduleName.AllocSysString();

	err = _closeGridServerPtr->readOpticalPowers(varModuleName,
												opticalPowerCh1,
												opticalPowerCh2);
					
    SysFreeString(varModuleName);

	//LeaveCriticalSection(&_safe_access);

#else // HARDWARE_BUILD
	HRESULT err = S_OK;
	*opticalPowerCh1 = 3*MIN_DIRECT_OPTICAL_POWER;
	*opticalPowerCh2 = 2*MIN_DIRECT_OPTICAL_POWER;
#endif // HARDWARE_BUILD

	return err;
}
//
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
///
//	Function	:	
//
//	Returns		:	void
//
//	Parameters	:	void
//
//	Description	:	
//
HRESULT
CGResearchServerInterface::
readOpticalPowerAtFreq(CString	moduleName,
					int			channel,
					double		frequency,
					double*		opticalPower)
{
#ifdef HARDWARE_BUILD
	//EnterCriticalSection(&_safe_access);

	HRESULT err = S_OK;

	BSTR varModuleName = moduleName.AllocSysString();

	err = _closeGridServerPtr->readOpticalPowerAtFreq(varModuleName,
													channel,
													frequency,
													opticalPower);
					
    SysFreeString(varModuleName);

	//LeaveCriticalSection(&_safe_access);

#else // HARDWARE_BUILD
	HRESULT err = S_OK;
	*opticalPower = MIN_DIRECT_OPTICAL_POWER;
#endif // HARDWARE_BUILD

	return err;
}
//
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
///
//	Function	:	
//
//	Returns		:	void
//
//	Parameters	:	void
//
//	Description	:	
//
HRESULT
CGResearchServerInterface::
readPhotodiodeCurrent(CString	moduleName,
					int			channel,
					double*		current)
{
#ifdef HARDWARE_BUILD

	//EnterCriticalSection(&_safe_access);

	HRESULT err = S_OK;

	BSTR varModuleName = moduleName.AllocSysString();

	err = _closeGridServerPtr->readPhotodiodeCurrent(varModuleName,
													channel,
													current);
					
    SysFreeString(varModuleName);

	//LeaveCriticalSection(&_safe_access);

#else // HARDWARE_BUILD
	HRESULT err = S_OK;
	*current = MIN_DIRECT_OPTICAL_POWER;
#endif // HARDWARE_BUILD

	return err;
}
//
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
///
//	Function	:	
//
//	Returns		:	void
//
//	Parameters	:	void
//
//	Description	:	
//
HRESULT
CGResearchServerInterface::
readCoarseFrequency(double*	frequency)
{
	//EnterCriticalSection(&_safe_access);

	HRESULT err = S_OK;

	err = _closeGridServerPtr->readCoarseFrequency(frequency);
										
	//LeaveCriticalSection(&_safe_access);

	return err;
}
//
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
///
//	Function	:	
//
//	Returns		:	void
//
//	Parameters	:	void
//
//	Description	:	
//
HRESULT
CGResearchServerInterface::
setTecSetpoint(double temperature)
{
	//EnterCriticalSection(&_safe_access);

	HRESULT err = S_OK;

	err = _closeGridServerPtr->setTECSetpoint(temperature);
										
	//LeaveCriticalSection(&_safe_access);

	return err;
}
//
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
///
//	Function	:	
//
//	Returns		:	void
//
//	Parameters	:	void
//
//	Description	:	
//
HRESULT
CGResearchServerInterface::
getTecSetpoint(double* setpoint)
{
	//EnterCriticalSection(&_safe_access);

	HRESULT err = S_OK;

	err = _closeGridServerPtr->getTECSetpoint(setpoint);
										
	//LeaveCriticalSection(&_safe_access);

	return err;
}
//
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
///
//	Function	:	
//
//	Returns		:	void
//
//	Parameters	:	void
//
//	Description	:	
//
HRESULT
CGResearchServerInterface::
get2DData(int		slotNumber,
		double		minCurrent,
		double		maxCurrent,
		int			numSteps,
		int			stepDuration,
		bool		frequencyUnits,
		CString*	resultsDir)
{
	//EnterCriticalSection(&_safe_access);

	HRESULT err = S_OK;

    BSTR results_dir = 0;

	err = _closeGridServerPtr->get2DData(
        (short)slotNumber,
		minCurrent,
		maxCurrent,
		(long)numSteps,
        (long)stepDuration,
		(VARIANT_BOOL)frequencyUnits,
		&results_dir);

	if(SUCCEEDED(err))
	{
		*resultsDir = CString(results_dir);
	}

    SysFreeString(results_dir);

	//LeaveCriticalSection(&_safe_access);

	return err;
}
//
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
///
//	Function	:	
//
//	Returns		:	void
//
//	Parameters	:	void
//
//	Description	:	
//
HRESULT
CGResearchServerInterface::
get2DData(CString		moduleName,
		vector<double>	currents,
		short			stepDuration,
		bool			frequencyUnits,
		CString*		resultsDir)
{
	//EnterCriticalSection(&_safe_access);

	HRESULT err = S_OK;

	BSTR		varModuleName = moduleName.AllocSysString();
	VARIANT		varCurrents;
    BSTR		varResultsDir = 0;


	convertVectorToVariant(currents,varCurrents);


	err = _closeGridServerPtr->get2DData2(
        varModuleName,
		&varCurrents,
        (long)stepDuration,
		(VARIANT_BOOL)frequencyUnits,
		&varResultsDir);

	if(SUCCEEDED(err))
	{
	    *resultsDir = CString(varResultsDir);
	}

    SysFreeString(varModuleName);
    SysFreeString(varResultsDir);
	//SafeArrayDestroyData(varCurrents.parray);
	//VariantClear(&varCurrents);

	//LeaveCriticalSection(&_safe_access);

	return err;
}
//
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
///
//	Function	:	
//
//	Returns		:	void
//
//	Parameters	:	void
//
//	Description	:	
//
HRESULT
CGResearchServerInterface::
get3DData(int		slotNumberX,
		double		minCurrentX,
		double		maxCurrentX,
		int			numStepsX,
		int			slotNumberY,
		double		minCurrentY,
		double		maxCurrentY,
		int			numStepsY,
		int			stepDuration,
		bool		hysteresis,
		bool		frequencyUnits,
		CString*	resultsDir)
{
	//EnterCriticalSection(&_safe_access);

	HRESULT err = S_OK;

    BSTR results_dir = 0;

	err = _closeGridServerPtr->get3DData(
        (short)slotNumberX,
		minCurrentX,
		maxCurrentX,
		(long)numStepsX,
        (long)stepDuration,
		(short)slotNumberY,
		minCurrentY,
		maxCurrentY,
		(long)numStepsY,
		(VARIANT_BOOL)hysteresis,
		(VARIANT_BOOL)frequencyUnits,
		&results_dir);

	if(SUCCEEDED(err))
	{
	    *resultsDir = CString(results_dir);
	}

    SysFreeString(results_dir);

	//LeaveCriticalSection(&_safe_access);

	return err;
}
//
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
///
//	Function	:	
//
//	Returns		:	void
//
//	Parameters	:	void
//
//	Description	:	
//
HRESULT
CGResearchServerInterface::
get3DData(CString		moduleNameX,
		vector<double>	currentsX,
		CString			moduleNameY,
		vector<double>	currentsY,
		short			stepDuration,
		bool			hysteresis,
		bool			frequencyUnits,
		CString*		resultsDir)
{
	//EnterCriticalSection(&_safe_access);

	HRESULT err = S_OK;

	BSTR		varModuleNameX = moduleNameX.AllocSysString();
	VARIANT		varCurrentsX;
	BSTR		varModuleNameY = moduleNameY.AllocSysString();
	VARIANT		varCurrentsY;
    BSTR		varResultsDir = 0;


	convertVectorToVariant(currentsX,varCurrentsX);
	convertVectorToVariant(currentsY,varCurrentsY);


	err = _closeGridServerPtr->get3DData2(
        varModuleNameX,
		&varCurrentsX,
        varModuleNameY,
		&varCurrentsY,
        (long)stepDuration,
		(VARIANT_BOOL)hysteresis,
		(VARIANT_BOOL)frequencyUnits,
		&varResultsDir);

	if(SUCCEEDED(err))
	{
	    *resultsDir = CString(varResultsDir);
	}

    SysFreeString(varModuleNameX);
    SysFreeString(varModuleNameY);
    SysFreeString(varResultsDir);
	//SafeArrayDestroyData(varCurrentsX.parray);
	//VariantClear(&varCurrentsX);
	//SafeArrayDestroyData(varCurrentsY.parray);
	//VariantClear(&varCurrentsY);

	//LeaveCriticalSection(&_safe_access);

	return err;
}
//
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
///
//	Function	:	
//
//	Returns		:	void
//
//	Parameters	:	void
//
//	Description	:	
//
HRESULT
CGResearchServerInterface::
livTest(int				powerMeterSlotNumber,
		int				powerMeterChannelNumber,
		int				currentSourceSlotNumber,
		double			minCurrent,
		double			maxCurrent,
		int				num_of_steps,
		int				step_duration,
		vector<double>* current_array,
		vector<double>* power_array,
		vector<double>* voltage_array,
		vector<double>* dPdI_array,
		vector<double>* d2PdI2_array,
		CString*		resultsDir)
{
	//EnterCriticalSection(&_safe_access);

	HRESULT err = S_OK;

    BSTR results_dir = 0;

	VARIANT	varCurrentArray;
	VARIANT	varPowerArray;
	VARIANT	varVoltageArray;
	VARIANT	varDPdIArray;
	VARIANT	varD2PdI2Array;

	err = _closeGridServerPtr->livtest(powerMeterSlotNumber,
									powerMeterChannelNumber,
									currentSourceSlotNumber,
									minCurrent,
									maxCurrent,
									num_of_steps,
									step_duration,
									&varCurrentArray,
									&varVoltageArray,
									&varPowerArray,
									&varDPdIArray,
									&varD2PdI2Array,
									&results_dir);

	if(SUCCEEDED(err))
	{
		convertVariantToVector(varCurrentArray,*current_array);
		convertVariantToVector(varPowerArray,*power_array);
		convertVariantToVector(varVoltageArray,*voltage_array);
		convertVariantToVector(varDPdIArray,*dPdI_array);
		convertVariantToVector(varD2PdI2Array,*d2PdI2_array);

		*resultsDir		= CString(results_dir);
	}

	SysFreeString(results_dir);
	//SafeArrayDestroyData(varCurrentArray.parray);
	//VariantClear(&varCurrentArray);
	//SafeArrayDestroyData(varPowerArray.parray);
	//VariantClear(&varPowerArray);
	//SafeArrayDestroyData(varVoltageArray.parray);
	//VariantClear(&varVoltageArray);
	//SafeArrayDestroyData(varDPdIArray.parray);
	//VariantClear(&varDPdIArray);
	//SafeArrayDestroyData(varD2PdI2Array.parray);
	//VariantClear(&varD2PdI2Array);

	//LeaveCriticalSection(&_safe_access);

	return err;
}
//
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
///
//	Function	:	
//
//	Returns		:	void
//
//	Parameters	:	void
//
//	Description	:	
//
HRESULT
CGResearchServerInterface::
get306SequenceMeasurements(CString		moduleName,
						vector<double>*	channel1Measurements,
						vector<double>*	channel2Measurements)
{
	//EnterCriticalSection(&_safe_access);

	HRESULT err = S_OK;

	BSTR		varModuleName = moduleName.AllocSysString();
	VARIANT		varChannel1;
	VARIANT		varChannel2;

	err = _closeGridServerPtr->get306SequenceMeasurements(varModuleName,
														&varChannel1,
														&varChannel2);

	if(SUCCEEDED(err))
	{
		convertVariantToVector(varChannel1,*channel1Measurements);
		convertVariantToVector(varChannel2,*channel2Measurements);
	}

    SysFreeString(varModuleName);
	//SafeArrayDestroyData(varChannel1.parray);
	//VariantClear(&varChannel1);
	//SafeArrayDestroyData(varChannel2.parray);
	//VariantClear(&varChannel2);

	//LeaveCriticalSection(&_safe_access);

	return err;
}
//
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
///
//	Function	:	log
//
//	Returns		:	HRESULT
//
//	Parameters	:	CString	message
//					short level
//
//	Description	:	
//
HRESULT
CGResearchServerInterface::
log(CString	message, short level )
{
	//EnterCriticalSection(&_safe_access);

	HRESULT err = S_OK;

	BSTR message_BSTR = message.AllocSysString();

	err = _closeGridServerPtr->log( message_BSTR,level);

    SysFreeString( message_BSTR );

	//LeaveCriticalSection(&_safe_access);

	return err;
}


/////////////////////////////////////////////////////////////////////
///
//	Function	:	openLogFile
//
//	Returns		:	HRESULT
//
//	Parameters	:	
//
//	Description	:	
//
HRESULT
CGResearchServerInterface::
openLogFile()
{
	//EnterCriticalSection(&_safe_access);

	HRESULT err = S_OK;

	err = _closeGridServerPtr->openLogFile();

	//LeaveCriticalSection(&_safe_access);

	return err;
}


/////////////////////////////////////////////////////////////////////
///
//	Function	:	closeLogFile
//
//	Returns		:	HRESULT
//
//	Parameters	:	
//
//	Description	:	
//
HRESULT
CGResearchServerInterface::
closeLogFile()
{
	//EnterCriticalSection(&_safe_access);

	HRESULT err = S_OK;

	err = _closeGridServerPtr->closeLogFile();

	//LeaveCriticalSection(&_safe_access);

	return err;
}

//
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
///
//	Function	:	shutdown
//
//	Returns		:	void
//
//	Parameters	:	void
//
//	Description	:	
//
HRESULT
CGResearchServerInterface::
shutdown()
{
	//EnterCriticalSection(&_safe_access);

	HRESULT err = S_OK;

	err = _closeGridServerPtr->shutdown();

	//LeaveCriticalSection(&_safe_access);

	return err;
}
//
/////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////
///
//	Function	:	rampdown
//
//	Returns		:	void
//
//	Parameters	:	void
//
//	Description	:	
//
HRESULT
CGResearchServerInterface::
rampdown()
{
	//EnterCriticalSection(&_safe_access);

	HRESULT err = S_OK;

	err = _closeGridServerPtr->rampdown();

	//LeaveCriticalSection(&_safe_access);

	return err;
}
//
/////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////
///
//	Function	:	disconnect
//
//	Returns		:	void
//
//	Parameters	:	void
//
//	Description	:	
//
HRESULT
CGResearchServerInterface::
disconnect()
{
	//EnterCriticalSection(&_safe_access);

	HRESULT err = S_OK;

	err = _closeGridServerPtr->disconnect();

	//LeaveCriticalSection(&_safe_access);

	return err;
}
//
/////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////
///
//	Function	:	markThreadToKill
//
//	Returns		:	void
//
//	Parameters	:	void
//
//	Description	:	
//
HRESULT
CGResearchServerInterface::
markThreadToKill( long thread_id )
{
	HRESULT err = S_OK;

	err = _closeGridServerPtr->markThreadToKill( thread_id );

	return err;
}
//
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
///
//	Function	:	getMarkedThreadID
//
//	Returns		:	void
//
//	Parameters	:	void
//
//	Description	:	
//
HRESULT
CGResearchServerInterface::
getMarkedThreadID( long* p_thread_id )
{
	HRESULT err = S_OK;

	err = _closeGridServerPtr->getMarkedThreadID( p_thread_id );

	return err;
}
//
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
///
//	Function	:	initWavemeter
//
//	Returns		:	void
//
//	Parameters	:	void
//
//	Description	:	
//
HRESULT
CGResearchServerInterface::
initWavemeter()
{
	HRESULT err = S_OK;

	err = _closeGridServerPtr->initWavemeter();

	return err;
}
//
/////////////////////////////////////////////////////////////////////

//
///	End of user interface functions
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
///
//	Private functions used for client server interaction
//

void
CGResearchServerInterface::
convertVariantToVector(VARIANT&	variantArray, vector<double>& array)
{
	CString log_msg("CGResearchServerInterface::convertVariantToVector entered");
	BSTR message_BSTR = log_msg.AllocSysString();
	_closeGridServerPtr->log(message_BSTR,DIAGNOSTIC_CGR_LOG);
    SysFreeString( message_BSTR );

	VariantInit(&variantArray);

	log_msg = CString("CGResearchServerInterface::convertVariantToVector returned from VariantInit");
	message_BSTR = log_msg.AllocSysString();
	_closeGridServerPtr->log(message_BSTR,DIAGNOSTIC_CGR_LOG);
	SysFreeString( message_BSTR );

	if(variantArray.vt == (VT_ARRAY & VT_R8))
	{
		double* lpusBuffer;

		log_msg = CString("CGResearchServerInterface::convertVariantToVector calling SafeArrayAccessData");
		BSTR message_BSTR = log_msg.AllocSysString();
		_closeGridServerPtr->log(message_BSTR,DIAGNOSTIC_CGR_LOG);
		SysFreeString( message_BSTR );

		SafeArrayAccessData( variantArray.parray, (void**)&lpusBuffer );


		log_msg = CString("CGResearchServerInterface::convertVariantToVector returned from SafeArrayAccessData");
		message_BSTR = log_msg.AllocSysString();
		_closeGridServerPtr->log(message_BSTR,DIAGNOSTIC_CGR_LOG);
		SysFreeString( message_BSTR );

		if(variantArray.parray->cDims == 1) //1D array
		{
			ULONG num = variantArray.parray->rgsabound[0].cElements;

			log_msg = CString("CGResearchServerInterface::convertVariantToVector");
			log_msg.AppendFormat( " : number of elements = %d", num );
			BSTR message_BSTR = log_msg.AllocSysString();
			_closeGridServerPtr->log(message_BSTR,DIAGNOSTIC_CGR_LOG);
			SysFreeString( message_BSTR );

			for(ULONG i=0; i<num; i++)
			{
				array.push_back(lpusBuffer[i]);
			}
		}

		log_msg = CString("CGResearchServerInterface::convertVariantToVector calling SafeArrayUnaccessData");
		message_BSTR = log_msg.AllocSysString();
		_closeGridServerPtr->log(message_BSTR,DIAGNOSTIC_CGR_LOG);
		SysFreeString( message_BSTR );

		SafeArrayUnaccessData( variantArray.parray );


		log_msg = CString("CGResearchServerInterface::convertVariantToVector returned from SafeArrayUnaccessData");
		message_BSTR = log_msg.AllocSysString();
		_closeGridServerPtr->log(message_BSTR,DIAGNOSTIC_CGR_LOG);
		SysFreeString( message_BSTR );
	}


	log_msg = CString("CGResearchServerInterface::convertVariantToVector destroying variant");
	message_BSTR = log_msg.AllocSysString();
	_closeGridServerPtr->log(message_BSTR,DIAGNOSTIC_CGR_LOG);
    SysFreeString( message_BSTR );

	if( variantArray.parray != NULL )
		SafeArrayDestroyData(variantArray.parray);
	else
	{
		log_msg = CString("CGResearchServerInterface::convertVariantToVector :: found NULL pointer before calling SafeArrayDestroyData");
		message_BSTR = log_msg.AllocSysString();
		_closeGridServerPtr->log(message_BSTR,DIAGNOSTIC_CGR_LOG);
		SysFreeString( message_BSTR );
	}

	if( variantArray.parray != NULL )
		SafeArrayDestroyDescriptor(variantArray.parray);
	else
	{
		log_msg = CString("CGResearchServerInterface::convertVariantToVector :: found NULL pointer before calling SafeArrayDestroyDescriptor");
		message_BSTR = log_msg.AllocSysString();
		_closeGridServerPtr->log(message_BSTR,DIAGNOSTIC_CGR_LOG);
		SysFreeString( message_BSTR );
	}

	log_msg = CString("CGResearchServerInterface::convertVariantToVector exiting");
	message_BSTR = log_msg.AllocSysString();
	_closeGridServerPtr->log(message_BSTR,DIAGNOSTIC_CGR_LOG);
    SysFreeString( message_BSTR );

	return;
}

void
CGResearchServerInterface::
convertVectorToVariant(vector<double>& array, VARIANT &variantArray)
{
	CString log_msg("CGResearchServerInterface::convertVectorToVariant entered");
	BSTR message_BSTR = log_msg.AllocSysString();
	_closeGridServerPtr->log(message_BSTR,DIAGNOSTIC_CGR_LOG);
    SysFreeString( message_BSTR );


	int arraySize = (int)(array.size());
	SAFEARRAYBOUND rgb [] = { arraySize, 0 };
	variantArray.vt = VT_ARRAY | VT_R8; // Array of doubles

	// Now call SafeArrayCreate with type, dimension,
	//  and pointer to vector of dimension descriptors 
	try
	{
		variantArray.parray = SafeArrayCreate(VT_R8, 1, rgb);
	}
	catch(...)
	{
		return;
	}

	log_msg = CString("CGResearchServerInterface::convertVectorToVariant returned from SafeArrayCreate");
	log_msg.AppendFormat(" : arraySize = %d",arraySize);
	message_BSTR = log_msg.AllocSysString();
	_closeGridServerPtr->log(message_BSTR,DIAGNOSTIC_CGR_LOG);
    SysFreeString( message_BSTR );

	double *rgelems; // Pointer to double elements

	// Now lock the array for editing and get a pointer
	// to the raw elements
	SafeArrayAccessData(variantArray.parray, (void**)&rgelems);

	log_msg = CString("CGResearchServerInterface::convertVectorToVariant returned from SafeArrayAccessData");
	message_BSTR = log_msg.AllocSysString();
	_closeGridServerPtr->log(message_BSTR,DIAGNOSTIC_CGR_LOG);
    SysFreeString( message_BSTR );

	// Loop through setting the elements
	for(int i=0; i<arraySize; i++)
	{
		rgelems[i] = array[i];
	}


	log_msg = CString("CGResearchServerInterface::convertVectorToVariant calling SafeArrayUnaccessData");
	message_BSTR = log_msg.AllocSysString();
	_closeGridServerPtr->log(message_BSTR,DIAGNOSTIC_CGR_LOG);
    SysFreeString( message_BSTR );

	// Release the lock on the array
	// (which also invalidates the element pointer)
	SafeArrayUnaccessData(variantArray.parray);


	// Clear the array releasing the memory
	//VariantClear(&variantArray);

	log_msg = CString("CGResearchServerInterface::convertVectorToVariant exiting");
	message_BSTR = log_msg.AllocSysString();
	_closeGridServerPtr->log(message_BSTR,DIAGNOSTIC_CGR_LOG);
    SysFreeString( message_BSTR );

	return;
}

//
/// End Client-Server functions
///////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
///
//	Get and set functions
//


//
//
///	End of CGResearchServerInterface.cpp
///////////////////////////////////////////////////////////////////////////////
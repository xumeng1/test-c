// Copyright (c) 2004 Tsunami Photonics Ltd. All Rights Reserved
//
// FileName    : VectorAnalysis.h
// Description : Declaration of VectorAnalysis class
//
// Rev#  | Date          | Author               | Description of change
// --------------------------------------------------------------
// 0.1   | 11 Oct 2002   | Frank D'Arcy         | Initial Draft
// 0.2   | 26 Jul 2004   | Frank D'Arcy         | Ported to CG 4.0
//

#pragma once

#include <vector>

#ifndef M_PI
#define M_PI       3.14159265358979323846
#endif

// PI/2
#ifndef M_PI_2
#define M_PI_2     1.57079632679489661923
#endif



class VectorAnalysis {

public:


    static double derivative(
        double x1,
        double x2,
        double y1,
        double y2 );

    static double doubleDerivative(
        double x1,
        double x2,
        double x3,
        double y1,
        double y2,
        double y3 );

    static void medianFilter(
        std::vector<double> &x_in,
        std::vector<double> &filtered_x_out,
        unsigned int rank,
        unsigned long line_length
    );

	static void linearFit(
		std::vector<double> &x_in,
		std::vector<double> &y_in,
		double &slope,
		double &intercept
	);

    static void distancesFromPointsToLine(
        std::vector<double> &x_in,
        std::vector<double> &y_in,
        double &slope,
        double &intercept,
        std::vector<double> &distances
    );

	static double meanOfValues(
		std::vector<double> &values );
};


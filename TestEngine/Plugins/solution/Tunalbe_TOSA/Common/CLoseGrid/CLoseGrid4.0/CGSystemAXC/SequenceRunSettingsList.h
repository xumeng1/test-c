// Copyright (c) 2003 Tsunami Photonics Ltd. All Rights Reserved

///////////////////////////////////////////////////////////////////////////////
///
//	Class	:
//
//	Contents:
//
//	Author	:
//
///
///////////////////////////////////////////////////////////////////////////////
///
//
#ifndef __SEQUENCERUNSETTINGSLIST_H_
#define __SEQUENCERUNSETTINGSLIST_H_

#pragma once

#include "afxcoll.h"

#include "SequenceRunSettings.h"

/////////////////////////////////////////////////////////////////////

class SequenceRunSettingsList 
	: public CPtrList
{
public:
	SequenceRunSettingsList(void);
	~SequenceRunSettingsList(void);


									// Find a device given the handle
	SequenceRunSettings*	findObjectByIndex(unsigned int index);

									// Find a device given the module name
	SequenceRunSettings*	findObject(int runNumber);

					// Insert at the head of the List
	bool	addToList(SequenceRunSettings* pSequenceRunSettings);

	bool	removeFromList(SequenceRunSettings* pSequenceRunSettings);

					// Is the device ptr in our list
	bool	inList(SequenceRunSettings* pSequenceRunSettings);


					// Destroy the List
					// by deleting all objects - 
					// Called on destruction
	void	emptyTheList();			

};
//
///////////////////////////////////////////////////////////////////////////////
#endif

// Copyright (c) 2003 Tsunami Photonics Ltd. All Rights Reserved

///////////////////////////////////////////////////////////////////////////////
///
//	Class	:
//
//	Contents:
//
//	Author	:
//
///
///////////////////////////////////////////////////////////////////////////////
///
//
#include "stdafx.h"

#include "PXIT305SequenceSettings.h"

#define new DEBUG_NEW

/////////////////////////////////////////////////////////////////////

//
///	ctor and dtor
//
PXIT305SequenceSettings::
PXIT305SequenceSettings(CString			moduleName,
						vector<double>&	currents,
						short			outputConnection,
						double			constantCurrent,
						short			inSequence,
						short			sourceDelay,
						short			measureDelay)
{
	_moduleName			= moduleName;
	_currents			= currents;
	_outputConnection	= outputConnection;
	_constantCurrent	= constantCurrent;
	_inSequence			= inSequence;
	_sourceDelay		= sourceDelay;
	_measureDelay		= measureDelay;

}

PXIT305SequenceSettings::
~PXIT305SequenceSettings(void)
{
	_currents.clear();
}

/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
///
//	Get and set functions
//

//
//
///	End of PXIT305SequenceSettings.cpp
///////////////////////////////////////////////////////////////////////////////
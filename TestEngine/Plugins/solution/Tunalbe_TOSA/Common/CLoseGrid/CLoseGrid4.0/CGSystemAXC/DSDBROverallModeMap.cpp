// Copyright (c) 2004 PXIT. All Rights Reserved
//
// FileName    : DSDBROverallModeMap.cpp
// Description : Definition of CDSDBROverallModeMap class
//               Contains data for a DSDBR Overall mode map
//               including its seven parts as separate mode maps 
//               and utility routines
//
// Rev#  | Date          | Author               | Description of change
// --------------------------------------------------------------
// 0.1   | 23 July 2004   | Frank D'Arcy         | Initial Draft
//

#include "StdAfx.h"

#include <math.h>
#include "DSDBROverallModeMap.h"
#include "CGResearchServerInterface.h"
#include "CLoseGridErrorCodes.h"

#include "CGSystemErrors.h"
#include "defaults.h"
#include "CGRegValues.h"
#include "VectorAnalysis.h"
#include <algorithm>
#include <fstream>
#include <direct.h>

CDSDBROverallModeMap::CDSDBROverallModeMap()
{
	_p_data_protect = NULL;
	_p_percent_complete = NULL;

	clear();
}

CDSDBROverallModeMap::~CDSDBROverallModeMap(void)
{
	clear();
}



CDSDBROverallModeMap::rcode
CDSDBROverallModeMap::loadMapsFromFile(
	CString results_dir_CString,
	CString laser_id_CString,
	CString date_time_stamp_CString,
	CString map_ramp_direction_CString )
{
	// Read the overall map from file.
	// The overall map is stored in 7 files named
	// MATRIX_PowerRatio_DSDBR[##]_[laser_ID]_[YYYYMMDDHHMMSS]_DFS[i]_[R|F].csv
	// where i is from 1 to 7
	//
	// The currents used to collect these maps are stored in 2 files
	// If_OverallMap_DSDBR[##]_[laser_ID]_[YYYYMMDDHHMMSS].csv
	// Ir_OverallMap_DSDBR[##]_[laser_ID]_[YYYYMMDDHHMMSS].csv
	// where If is front sections' current
	// and Ir is rear (reflector section current)

	CGR_LOG("CDSDBROverallModeMap::loadMapsFromFile() entered",DIAGNOSTIC_CGR_LOG)

	rcode rval = rcode::ok;

	// first, clear anything previously collected/loaded
	clear();

	CString file_path = results_dir_CString;
	
	// Check for directory without trailing '\\' character
	if( file_path.GetAt( file_path.GetLength() - 1 ) != '\\' )
	{
		// Add trailing directory backslash '\\'
		file_path += CString( "\\" );
	}

	CString If_file_path = file_path;
	If_file_path += FRONT_CURRENT_CSTRING;
	If_file_path += CString( "_OverallMap_" );
	If_file_path += LASER_TYPE_ID_DSDBR01_CSTRING;
	If_file_path += USCORE;
	If_file_path += laser_id_CString;
	If_file_path += USCORE;
	If_file_path += date_time_stamp_CString;
	If_file_path += CSV;

	CString Ir_file_path = file_path;
	Ir_file_path += REAR_CURRENT_CSTRING;
	Ir_file_path += CString( "_OverallMap_" );
	Ir_file_path += LASER_TYPE_ID_DSDBR01_CSTRING;
	Ir_file_path += USCORE;
	Ir_file_path += laser_id_CString;
	Ir_file_path += USCORE;
	Ir_file_path += date_time_stamp_CString;
	Ir_file_path += CSV;

	file_path += MATRIX_;
	file_path += MEAS_TYPE_ID_POWERRATIO_CSTRING;
	file_path += USCORE;
	file_path += LASER_TYPE_ID_DSDBR01_CSTRING;
	file_path += USCORE;
	file_path += laser_id_CString;
	file_path += USCORE;
	file_path += date_time_stamp_CString;
	file_path += CString("_DFS");

	// create absolute file path for each of the seven files

	CString file_path_DFS1 = file_path + CString("1") + USCORE + map_ramp_direction_CString + CSV;
	CString file_path_DFS2 = file_path + CString("2") + USCORE + map_ramp_direction_CString + CSV;
	CString file_path_DFS3 = file_path + CString("3") + USCORE + map_ramp_direction_CString + CSV;
	CString file_path_DFS4 = file_path + CString("4") + USCORE + map_ramp_direction_CString + CSV;
	CString file_path_DFS5 = file_path + CString("5") + USCORE + map_ramp_direction_CString + CSV;
	CString file_path_DFS6 = file_path + CString("6") + USCORE + map_ramp_direction_CString + CSV;
	CString file_path_DFS7 = file_path + CString("7") + USCORE + map_ramp_direction_CString + CSV;


	// load data from files

	CCurrentsVector::rcode load_Ir_from_file_rval = CCurrentsVector::rcode::ok;
	load_Ir_from_file_rval = _vector_Ir.readFromFile( Ir_file_path );

	if( load_Ir_from_file_rval != CCurrentsVector::rcode::ok )
	{
		CString log_err_msg("CDSDBROverallModeMap::loadMapsFromFile() error reading rear currents file: ");
		log_err_msg += Ir_file_path;
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)
	}

	CDSDBRFrontCurrents::rcode load_If_from_file_rval = CDSDBRFrontCurrents::rcode::ok;
	load_If_from_file_rval = _vector_If.readFromFile( If_file_path );

	if( load_If_from_file_rval != CDSDBRFrontCurrents::rcode::ok )
	{
		CString log_err_msg("CDSDBROverallModeMap::loadMapsFromFile() error reading front currents file: ");
		log_err_msg += If_file_path;
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)
	}

	CLaserModeMap::rcode load_from_file_rval = CLaserModeMap::rcode::ok;

	if( load_from_file_rval == CLaserModeMap::rcode::ok )
		load_from_file_rval = _map_power_ratio_dfs1.readFromFile( file_path_DFS1 );

	if( load_from_file_rval == CLaserModeMap::rcode::ok )
		load_from_file_rval = _map_power_ratio_dfs2.readFromFile( file_path_DFS2 );

	if( load_from_file_rval == CLaserModeMap::rcode::ok )
		load_from_file_rval = _map_power_ratio_dfs3.readFromFile( file_path_DFS3 );

	if( load_from_file_rval == CLaserModeMap::rcode::ok )
		load_from_file_rval = _map_power_ratio_dfs4.readFromFile( file_path_DFS4 );

	if( load_from_file_rval == CLaserModeMap::rcode::ok )
		load_from_file_rval = _map_power_ratio_dfs5.readFromFile( file_path_DFS5 );

	if( load_from_file_rval == CLaserModeMap::rcode::ok )
		load_from_file_rval = _map_power_ratio_dfs6.readFromFile( file_path_DFS6 );

	if( load_from_file_rval == CLaserModeMap::rcode::ok )
		load_from_file_rval = _map_power_ratio_dfs7.readFromFile( file_path_DFS7 );

	// map error codes

	if( load_from_file_rval == CLaserModeMap::rcode::file_does_not_exist
	 || load_Ir_from_file_rval == CCurrentsVector::rcode::file_does_not_exist
	 || load_If_from_file_rval == CDSDBRFrontCurrents::rcode::file_does_not_exist )
	{
		rval = rcode::file_does_not_exist;
		CGR_LOG("CDSDBROverallModeMap::loadMapsFromFile() error: file_does_not_exist",ERROR_CGR_LOG)
	}
	else
	if( load_from_file_rval == CLaserModeMap::rcode::could_not_open_file
	 || load_Ir_from_file_rval == CCurrentsVector::rcode::could_not_open_file
	 || load_If_from_file_rval == CDSDBRFrontCurrents::rcode::could_not_open_file )
	{
		rval = rcode::could_not_open_file;
		CGR_LOG("CDSDBROverallModeMap::loadMapsFromFile() error: could_not_open_file",ERROR_CGR_LOG)
	}
	else
	if( load_from_file_rval == CLaserModeMap::rcode::file_format_invalid
	 || load_from_file_rval == CLaserModeMap::rcode::vector_size_rows_cols_do_not_match
	 || load_Ir_from_file_rval == CCurrentsVector::rcode::file_format_invalid
	 || load_Ir_from_file_rval == CCurrentsVector::rcode::vector_size_rows_cols_do_not_match
	 || load_If_from_file_rval == CDSDBRFrontCurrents::rcode::file_format_invalid
	 || load_If_from_file_rval == CDSDBRFrontCurrents::rcode::vector_size_rows_cols_do_not_match )
	{
		rval = rcode::file_format_invalid;
		CGR_LOG("CDSDBROverallModeMap::loadMapsFromFile() error: file_format_invalid",ERROR_CGR_LOG)
	}


	if( rval == rcode::ok )
	{
		if( map_ramp_direction_CString == RAMP_DIRECTION_REAR )
			_Ir_ramp = true;
		else
			_Ir_ramp = false; // If ramp

		// check maps are of matching size
		if((_Ir_ramp && ( ( _vector_If._submap_length[0] != _map_power_ratio_dfs1._rows )
					   || ( _vector_If._submap_length[1] != _map_power_ratio_dfs2._rows )
					   || ( _vector_If._submap_length[2] != _map_power_ratio_dfs3._rows )
					   || ( _vector_If._submap_length[3] != _map_power_ratio_dfs4._rows )
					   || ( _vector_If._submap_length[4] != _map_power_ratio_dfs5._rows )
					   || ( _vector_If._submap_length[5] != _map_power_ratio_dfs6._rows )
					   || ( _vector_If._submap_length[6] != _map_power_ratio_dfs7._rows )))
		||(!_Ir_ramp && ( ( _vector_If._submap_length[0] != _map_power_ratio_dfs1._cols )
					   || ( _vector_If._submap_length[1] != _map_power_ratio_dfs2._cols )
					   || ( _vector_If._submap_length[2] != _map_power_ratio_dfs3._cols )
					   || ( _vector_If._submap_length[3] != _map_power_ratio_dfs4._cols )
					   || ( _vector_If._submap_length[4] != _map_power_ratio_dfs5._cols )
					   || ( _vector_If._submap_length[5] != _map_power_ratio_dfs6._cols )
					   || ( _vector_If._submap_length[6] != _map_power_ratio_dfs7._cols ))))
		{
			// maps are not of matching size
			clear();
			rval = rcode::files_not_matching_sizes;
			CGR_LOG("CDSDBROverallModeMap::loadMapsFromFile() error: files_not_matching_sizes",ERROR_CGR_LOG)
		}
		else
		{
			// all 7 maps are the same size
			// and match the the vector lengths
			_maps_loaded = true;
			_rows = _vector_If._total_length;
			_cols = _vector_Ir._length;
			_date_time_stamp_CString = date_time_stamp_CString;
			_laser_id_CString = laser_id_CString;
			_results_dir_CString = results_dir_CString;

			_xAxisLength = (int)_cols;
			_yAxisLength = (int)_rows;

		}
	}
	else
	{
		clear();
	}

	CGR_LOG("CDSDBROverallModeMap::loadMapsFromFile() exiting",DIAGNOSTIC_CGR_LOG)

	return rval;
}


std::vector<double>::iterator
CDSDBROverallModeMap::iPowerRatioPt( long row, long col )
{
	std::vector<double>::iterator i;

	// check point is within map
	if( row >=0 && row < _rows && col >=0 && col < _cols )
	{
		long row_in_submap = _vector_If.index_in_submap(row); //row % _vector_If._rows;
		long col_in_submap = col;

		long index = 0;
		if(_Ir_ramp) // Ir ramped
		{
			index = row_in_submap*_cols + col_in_submap;
		}
		else // If ramped
		{
			index = row_in_submap + col_in_submap*(_vector_If._submap_length[_vector_If.pair_number(row)-1]);
		}

		//switch( 1 + (long)(row / _vector_If._rows) )
		switch( _vector_If.pair_number( row ) )
		{
		case 1:
			i = _map_power_ratio_dfs1._map.begin();
			i += index;
			break;
		case 2:
			i = _map_power_ratio_dfs2._map.begin();
			i += index;
			break;
		case 3:
			i = _map_power_ratio_dfs3._map.begin();
			i += index;
			break;
		case 4:
			i = _map_power_ratio_dfs4._map.begin();
			i += index;
			break;
		case 5:
			i = _map_power_ratio_dfs5._map.begin();
			i += index;
			break;
		case 6:
			i = _map_power_ratio_dfs6._map.begin();
			i += index;
			break;
		case 7:
			i = _map_power_ratio_dfs7._map.begin();
			i += index;
			break;
		default:
			break;
		}
	}

	return i;
}


std::vector<double>::iterator
CDSDBROverallModeMap::iPowerRatioMedianFilteredPt( long row, long col )
{
	std::vector<double>::iterator i;

	// check point is within map
	if( row >=0 && row < _rows && col >=0 && col < _cols )
	{
		long row_in_submap = _vector_If.index_in_submap(row); //row % _vector_If._rows;
		long col_in_submap = col;

		long index = 0;
		if(_Ir_ramp) // Ir ramped
		{
			index = row_in_submap*_cols + col_in_submap;
		}
		else // If ramped
		{
			index = row_in_submap + col_in_submap*(_vector_If._submap_length[_vector_If.pair_number(row)-1]);
		}

		//switch( 1 + (long)(row / _vector_If._rows) )
		switch( _vector_If.pair_number( row ) )
		{
		case 1:
			i = _map_power_ratio_dfs1_median_filtered._map.begin();
			i += index;
			break;
		case 2:
			i = _map_power_ratio_dfs2_median_filtered._map.begin();
			i += index;
			break;
		case 3:
			i = _map_power_ratio_dfs3_median_filtered._map.begin();
			i += index;
			break;
		case 4:
			i = _map_power_ratio_dfs4_median_filtered._map.begin();
			i += index;
			break;
		case 5:
			i = _map_power_ratio_dfs5_median_filtered._map.begin();
			i += index;
			break;
		case 6:
			i = _map_power_ratio_dfs6_median_filtered._map.begin();
			i += index;
			break;
		case 7:
			i = _map_power_ratio_dfs7_median_filtered._map.begin();
			i += index;
			break;
		default:
			break;
		}
	}

	return i;
}


std::vector<double>::iterator
CDSDBROverallModeMap::iMaxDeltaPrPt( long row, long col )
{
	std::vector<double>::iterator i;

	// check point is within map
	if( row >=0 && row < _rows && col >=0 && col < _cols )
	{
		long row_in_submap = _vector_If.index_in_submap(row); //row % _vector_If._rows;
		long col_in_submap = col;

		long index = 0;
		if(_Ir_ramp) // Ir ramped
		{
			index = row_in_submap*_cols + col_in_submap;
		}
		else // If ramped
		{
			index = row_in_submap + col_in_submap*(_vector_If._submap_length[_vector_If.pair_number(row)-1]);
		}

		short map_num = _vector_If.pair_number( row );
		i = _map_max_deltaPr[map_num-1]._map.begin();
		i += index;

	}

	return i;
}

std::vector<double>::iterator
CDSDBROverallModeMap::iDirectPowerPt( long row, long col )
{
	std::vector<double>::iterator i;

	// check point is within map
	if( row >=0 && row < _rows && col >=0 && col < _cols )
	{
		long row_in_submap = _vector_If.index_in_submap(row); //row % _vector_If._rows;
		long col_in_submap = col;

		long index = 0;
		if(_Ir_ramp) // Ir ramped
		{
			index = row_in_submap*_cols + col_in_submap;
		}
		else // If ramped
		{
			index = row_in_submap + col_in_submap*(_vector_If._submap_length[_vector_If.pair_number(row)-1]);
		}

		short map_num = _vector_If.pair_number( row );
		i = _map_forward_direct_power[map_num-1]._map.begin();
		i += index;

	}

	return i;
}

bool
CDSDBROverallModeMap::isInMap( long row, long col )
{
	return (row >=0 && row < _rows && col >=0 && col < _cols);
}

CDSDBROverallModeMap::direction_type
CDSDBROverallModeMap::opposite(
	direction_type direction )
{
	direction_type opposite_direction;

	// find opposite direction 
	switch( direction )
	{
	case direction_type::N:
		opposite_direction = direction_type::S;
		break;
	case direction_type::NW:
		opposite_direction = direction_type::SE;
		break;
	case direction_type::W:
		opposite_direction = direction_type::E;
		break;
	case direction_type::SW:
		opposite_direction = direction_type::NE;
		break;
	case direction_type::S:
		opposite_direction = direction_type::N;
		break;
	case direction_type::SE:
		opposite_direction = direction_type::NW;
		break;
	case direction_type::E:
		opposite_direction = direction_type::W;
		break;
	case direction_type::NE:
		opposite_direction = direction_type::SW;
		break;
	default:
		break;
	}

	return opposite_direction;
};


CString
CDSDBROverallModeMap::getCStringForDirection(
	direction_type direction )
{
	CString direction_CString;

	// find opposite direction 
	switch( direction )
	{
	case direction_type::N:
		direction_CString = CString("N");
		break;
	case direction_type::NW:
		direction_CString = CString("NW");
		break;
	case direction_type::W:
		direction_CString = CString("W");
		break;
	case direction_type::SW:
		direction_CString = CString("SW");
		break;
	case direction_type::S:
		direction_CString = CString("S");
		break;
	case direction_type::SE:
		direction_CString = CString("SE");
		break;
	case direction_type::E:
		direction_CString = CString("E");
		break;
	case direction_type::NE:
		direction_CString = CString("NE");
		break;
	default:
		direction_CString = CString("-");
		break;
	}

	return direction_CString;
};

void
CDSDBROverallModeMap::getRelativeRowCol(
	long row_in,
	long col_in,
	direction_type direction,
	long distance,
	long &row_out,
	long &col_out )
{
	// find adjacent point 
	switch( direction )
	{
	case direction_type::N:
		row_out = row_in+distance;
		col_out = col_in;
		break;
	case direction_type::NW:
		row_out = row_in+distance;
		col_out = col_in-distance;
		break;
	case direction_type::W:
		row_out = row_in;
		col_out = col_in-distance;
		break;
	case direction_type::SW:
		row_out = row_in-distance;
		col_out = col_in-distance;
		break;
	case direction_type::S:
		row_out = row_in-distance;
		col_out = col_in;
		break;
	case direction_type::SE:
		row_out = row_in-distance;
		col_out = col_in+distance;
		break;
	case direction_type::E:
		row_out = row_in;
		col_out = col_in+distance;
		break;
	case direction_type::NE:
		row_out = row_in+distance;
		col_out = col_in+distance;
		break;
	default:
		break;
	}
}


CDSDBROverallModeMap::rcode
CDSDBROverallModeMap::getValueOfAdjacentPt(
	long row,
	long col, 
	direction_type direction,
	long &adj_pt_row,
	long &adj_pt_col,
	double &value )
{
	rcode rval = rcode::ok;

	// check point is within map
	if( isInMap( row, col ) )
	{
		adj_pt_row = -1;
		adj_pt_col = -1;

		// find adjacent point 
		getRelativeRowCol(
			row,
			col,
			direction,
			1,
			adj_pt_row,
			adj_pt_col );

		// check adjacent point is within map
		if( isInMap( adj_pt_row, adj_pt_col ) )
		{
			value = *iPowerRatioPt( adj_pt_row, adj_pt_col );
		}
		else
		{
			rval = rcode::outside_map_dimensions;
			CGR_LOG("CDSDBROverallModeMap::getValueOfAdjacentPt() error: outside_map_dimensions",ERROR_CGR_LOG)
		}
	}
	else
	{
		rval = rcode::outside_map_dimensions;
		CGR_LOG("CDSDBROverallModeMap::getValueOfAdjacentPt() error: outside_map_dimensions",ERROR_CGR_LOG)
	}

	return rval;
}


CDSDBROverallModeMap::rcode
CDSDBROverallModeMap::getMaxDeltaPrValueOfAdjacentPt(
	long row,
	long col, 
	direction_type direction,
	long &adj_pt_row,
	long &adj_pt_col,
	double &value )
{
	rcode rval = rcode::ok;

	// check point is within map
	if( isInMap( row, col ) )
	{
		adj_pt_row = -1;
		adj_pt_col = -1;

		// find adjacent point 
		getRelativeRowCol(
			row,
			col,
			direction,
			1,
			adj_pt_row,
			adj_pt_col );

		// check adjacent point is within map
		if( isInMap( adj_pt_row, adj_pt_col ) )
		{
			value = *iMaxDeltaPrPt( adj_pt_row, adj_pt_col );
		}
		else
		{
			rval = rcode::outside_map_dimensions;
		}
	}
	else
	{
		rval = rcode::outside_map_dimensions;
	}

	return rval;
}


CDSDBROverallModeMap::rcode
CDSDBROverallModeMap::calcFilteredValueOfAdjacentPt(
	long row,
	long col, 
	direction_type direction,
	long &adj_pt_row,
	long &adj_pt_col,
	double &value )
{
	rcode rval = rcode::ok;

	// check point is within map
	if( isInMap( row, col ) )
	{
		adj_pt_row = -1;
		adj_pt_col = -1;

		// find adjacent point 
		getRelativeRowCol(
			row,
			col,
			direction,
			1,
			adj_pt_row,
			adj_pt_col );

		// check adjacent point is within map
		if( isInMap( adj_pt_row, adj_pt_col ) )
		{
			std::vector<double> vals;

			long num_of_pts_each_side = (long)(CG_REG->get_DSDBR01_OMBD_median_filter_rank());
			direction_type opposite_direction = opposite( direction );

			// find points before input point and insert in vector
			for( long i = num_of_pts_each_side; i > 1; i-- )
			{
				long pt_row;
				long pt_col;
				getRelativeRowCol( adj_pt_row, adj_pt_col, opposite_direction, i, pt_row, pt_col );
				if( isInMap( pt_row, pt_col ) )
				{
					vals.push_back( *iPowerRatioPt( pt_row, pt_col ) );
				}
				else
				{
					vals.push_back( *iPowerRatioPt( row, col ) );
				}
			}

			// insert input point in vector
			vals.push_back( *iPowerRatioPt( row, col ) );

			// insert adjacent point in vector
			vals.push_back( *iPowerRatioPt( adj_pt_row, adj_pt_col ) );

			// find points after adjacent point and insert in vector
			for( long i = 0; i < num_of_pts_each_side; i++ )
			{
				long pt_row;
				long pt_col;
				getRelativeRowCol( adj_pt_row, adj_pt_col, direction, i, pt_row, pt_col );
				if( isInMap( pt_row, pt_col ) )
				{
					vals.push_back( *iPowerRatioPt( pt_row, pt_col ) );
				}
				else
				{
					vals.push_back( *iPowerRatioPt( adj_pt_row, adj_pt_col ) );
				}
			}

			// Calculate the median by sorting and taking middle value
			std::sort(vals.begin(),vals.end());
			value = vals[num_of_pts_each_side];

		}
		else
		{
			rval = rcode::outside_map_dimensions;
			CGR_LOG("CDSDBROverallModeMap::calcFilteredValueOfAdjacentPt() error: outside_map_dimensions",ERROR_CGR_LOG)
		}
	}
	else
	{
		rval = rcode::outside_map_dimensions;
		CGR_LOG("CDSDBROverallModeMap::calcFilteredValueOfAdjacentPt() error: outside_map_dimensions",ERROR_CGR_LOG)
	}

	return rval;
}


CDSDBROverallModeMap::rcode
CDSDBROverallModeMap::writeMapsToFile()
{
	// Write the overall map to file
	// The overall map is stored in 7 files named
	// MATRIX_[mapType]_DSDBR[##]_[laser_ID]_[YYYYMMDDHHMMSS]_DFS[i].csv
	// where i is from 1 to 7

	CGR_LOG("CDSDBROverallModeMap::writeMapsToFile() entered",DIAGNOSTIC_CGR_LOG)

	rcode rval = rcode::ok;

	CString results_dir_CString = _results_dir_CString;
	
	// Check for directory without trailing '\\' character
	if( results_dir_CString.GetAt( results_dir_CString.GetLength() - 1 ) != '\\' )
	{
		// Add trailing directory backslash '\\'
		results_dir_CString += CString( "\\" );
	}

	CString file_path = results_dir_CString;
	file_path += MATRIX_;
	file_path += MEAS_TYPE_ID_POWERRATIO_CSTRING;
	file_path += USCORE;
	file_path += LASER_TYPE_ID_DSDBR01_CSTRING;
	file_path += USCORE;
	file_path += _laser_id_CString;
	file_path += USCORE;
	file_path += _date_time_stamp_CString;
	file_path += CString("_DFS");

	// create absolute file path for each of the seven files

	CString map_ramp_direction_CString;
	if( _Ir_ramp )
		map_ramp_direction_CString = RAMP_DIRECTION_REAR;
	else
		map_ramp_direction_CString = RAMP_DIRECTION_FRONT;

	CString file_path_DFS1 = file_path + CString("1") + USCORE + map_ramp_direction_CString + CSV;
	CString file_path_DFS2 = file_path + CString("2") + USCORE + map_ramp_direction_CString + CSV;
	CString file_path_DFS3 = file_path + CString("3") + USCORE + map_ramp_direction_CString + CSV;
	CString file_path_DFS4 = file_path + CString("4") + USCORE + map_ramp_direction_CString + CSV;
	CString file_path_DFS5 = file_path + CString("5") + USCORE + map_ramp_direction_CString + CSV;
	CString file_path_DFS6 = file_path + CString("6") + USCORE + map_ramp_direction_CString + CSV;
	CString file_path_DFS7 = file_path + CString("7") + USCORE + map_ramp_direction_CString + CSV;



	CString If_file_path = results_dir_CString;
	If_file_path += FRONT_CURRENT_CSTRING;
	If_file_path += CString( "_OverallMap_");
	If_file_path += LASER_TYPE_ID_DSDBR01_CSTRING;
	If_file_path += USCORE;
	If_file_path += _laser_id_CString;
	If_file_path += USCORE;
	If_file_path += _date_time_stamp_CString;
	If_file_path += CSV;

	CString Ir_file_path = results_dir_CString;
	Ir_file_path += REAR_CURRENT_CSTRING;
	Ir_file_path += CString( "_OverallMap_");
	Ir_file_path += LASER_TYPE_ID_DSDBR01_CSTRING;
	Ir_file_path += USCORE;
	Ir_file_path += _laser_id_CString;
	Ir_file_path += USCORE;
	Ir_file_path += _date_time_stamp_CString;
	Ir_file_path += CSV;

	// write data to files

	CCurrentsVector::rcode write_Ir_to_file_rval = CCurrentsVector::rcode::ok;
	write_Ir_to_file_rval = _vector_Ir.writeToFile(
		Ir_file_path,
		CG_REG->get_DSDBR01_OMDC_overwrite_existing_files() );

	if( write_Ir_to_file_rval != CCurrentsVector::rcode::ok )
	{
		CString log_err_msg("CDSDBROverallModeMap::writeMapsToFile() error writing rear currents to file: ");
		log_err_msg += Ir_file_path;
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)
	}

	CDSDBRFrontCurrents::rcode write_If_to_file_rval = CDSDBRFrontCurrents::rcode::ok;
	write_If_to_file_rval = _vector_If.writeToFile(
		If_file_path,
		CG_REG->get_DSDBR01_OMDC_overwrite_existing_files() );

	if( write_If_to_file_rval != CDSDBRFrontCurrents::rcode::ok )
	{
		CString log_err_msg("CDSDBROverallModeMap::writeMapsToFile() error writing front currents to file: ");
		log_err_msg += If_file_path;
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)
	}

	CLaserModeMap::rcode write_to_file_rval = CLaserModeMap::rcode::ok;

	if( write_to_file_rval == CLaserModeMap::rcode::ok )
		write_to_file_rval = _map_power_ratio_dfs1.writeToFile(
			file_path_DFS1,
			CG_REG->get_DSDBR01_OMDC_overwrite_existing_files() );

	if( write_to_file_rval == CLaserModeMap::rcode::ok )
		write_to_file_rval = _map_power_ratio_dfs2.writeToFile(
			file_path_DFS2,
			CG_REG->get_DSDBR01_OMDC_overwrite_existing_files() );

	if( write_to_file_rval == CLaserModeMap::rcode::ok )
		write_to_file_rval = _map_power_ratio_dfs3.writeToFile(
			file_path_DFS3,
			CG_REG->get_DSDBR01_OMDC_overwrite_existing_files() );

	if( write_to_file_rval == CLaserModeMap::rcode::ok )
		write_to_file_rval = _map_power_ratio_dfs4.writeToFile(
			file_path_DFS4,
			CG_REG->get_DSDBR01_OMDC_overwrite_existing_files() );

	if( write_to_file_rval == CLaserModeMap::rcode::ok )
		write_to_file_rval = _map_power_ratio_dfs5.writeToFile(
			file_path_DFS5,
			CG_REG->get_DSDBR01_OMDC_overwrite_existing_files() );

	if( write_to_file_rval == CLaserModeMap::rcode::ok )
		write_to_file_rval = _map_power_ratio_dfs6.writeToFile(
			file_path_DFS6,
			CG_REG->get_DSDBR01_OMDC_overwrite_existing_files() );

	if( write_to_file_rval == CLaserModeMap::rcode::ok )
		write_to_file_rval = _map_power_ratio_dfs7.writeToFile( 
			file_path_DFS7,
			CG_REG->get_DSDBR01_OMDC_overwrite_existing_files() );

	if( write_to_file_rval == CLaserModeMap::rcode::ok )
	{

		for(int i = 0; i < (int)(_map_forward_direct_power.size()) ; i++ )
		{
			CString forward_direct_power_file_path = results_dir_CString;
			forward_direct_power_file_path += MATRIX_;
			forward_direct_power_file_path += MEAS_TYPE_ID_POWER_CSTRING;
			forward_direct_power_file_path += USCORE;
			forward_direct_power_file_path += LASER_TYPE_ID_DSDBR01_CSTRING;
			forward_direct_power_file_path += USCORE;
			forward_direct_power_file_path += _laser_id_CString;
			forward_direct_power_file_path += USCORE;
			forward_direct_power_file_path += _date_time_stamp_CString;
			char buf[10];
			sprintf( buf, "_DFS%d", i+1 );
			forward_direct_power_file_path += CString( buf );
			forward_direct_power_file_path += USCORE;
			forward_direct_power_file_path += map_ramp_direction_CString;
			forward_direct_power_file_path += CSV;

			_map_forward_direct_power[i].writeToFile(
				forward_direct_power_file_path,
				CG_REG->get_DSDBR01_OMDC_overwrite_existing_files() );
		}

		for(int i = 0; i < (int)(_map_reverse_direct_power.size()) ; i++ )
		{
			CString reverse_direct_power_file_path = results_dir_CString;
			reverse_direct_power_file_path += MATRIX_;
			reverse_direct_power_file_path += MEAS_TYPE_ID_REVERSEPOWER_CSTRING;
			reverse_direct_power_file_path += USCORE;
			reverse_direct_power_file_path += LASER_TYPE_ID_DSDBR01_CSTRING;
			reverse_direct_power_file_path += USCORE;
			reverse_direct_power_file_path += _laser_id_CString;
			reverse_direct_power_file_path += USCORE;
			reverse_direct_power_file_path += _date_time_stamp_CString;
			char buf[10];
			sprintf( buf, "_DFS%d", i+1 );
			reverse_direct_power_file_path += CString( buf );
			reverse_direct_power_file_path += USCORE;
			reverse_direct_power_file_path += map_ramp_direction_CString;
			reverse_direct_power_file_path += CSV;

			_map_reverse_direct_power[i].writeToFile(
				reverse_direct_power_file_path,
				CG_REG->get_DSDBR01_OMDC_overwrite_existing_files() );
		}

		if( CG_REG->get_DSDBR01_OMDC_write_filtered_power_maps() )
		{
			for(int i = 0; i < (int)(_map_forward_filtered_power.size()) ; i++ )
			{
				CString forward_filtered_power_file_path = results_dir_CString;
				forward_filtered_power_file_path += MATRIX_;
				forward_filtered_power_file_path += MEAS_TYPE_ID_FILTEREDPOWER_CSTRING;
				forward_filtered_power_file_path += USCORE;
				forward_filtered_power_file_path += LASER_TYPE_ID_DSDBR01_CSTRING;
				forward_filtered_power_file_path += USCORE;
				forward_filtered_power_file_path += _laser_id_CString;
				forward_filtered_power_file_path += USCORE;
				forward_filtered_power_file_path += _date_time_stamp_CString;
				char buf[10];
				sprintf( buf, "_DFS%d", i+1 );
				forward_filtered_power_file_path += CString( buf );
				forward_filtered_power_file_path += USCORE;
				forward_filtered_power_file_path += map_ramp_direction_CString;
				forward_filtered_power_file_path += CSV;

				_map_forward_filtered_power[i].writeToFile(
					forward_filtered_power_file_path,
					CG_REG->get_DSDBR01_OMDC_overwrite_existing_files() );
			}

			for(int i = 0; i < (int)(_map_reverse_filtered_power.size()) ; i++ )
			{
				CString reverse_filtered_power_file_path = results_dir_CString;
				reverse_filtered_power_file_path += MATRIX_;
				reverse_filtered_power_file_path += MEAS_TYPE_ID_REVERSEFILTEREDPOWER_CSTRING;
				reverse_filtered_power_file_path += USCORE;
				reverse_filtered_power_file_path += LASER_TYPE_ID_DSDBR01_CSTRING;
				reverse_filtered_power_file_path += USCORE;
				reverse_filtered_power_file_path += _laser_id_CString;
				reverse_filtered_power_file_path += USCORE;
				reverse_filtered_power_file_path += _date_time_stamp_CString;
				char buf[10];
				sprintf( buf, "_DFS%d", i+1 );
				reverse_filtered_power_file_path += CString( buf );
				reverse_filtered_power_file_path += USCORE;
				reverse_filtered_power_file_path += map_ramp_direction_CString;
				reverse_filtered_power_file_path += CSV;

				_map_reverse_filtered_power[i].writeToFile(
					reverse_filtered_power_file_path,
					CG_REG->get_DSDBR01_OMDC_overwrite_existing_files() );
			}
		}

		if( CG_REG->get_DSDBR01_OMDC_measure_photodiode_currents() )
		{
			for(int i = 0; i < (int)(_map_forward_photodiode1.size()) ; i++ )
			{
				CString forward_photodiode1_file_path = results_dir_CString;
				forward_photodiode1_file_path += MATRIX_;
				forward_photodiode1_file_path += MEAS_TYPE_ID_FORWARDPD1_CSTRING;
				forward_photodiode1_file_path += USCORE;
				forward_photodiode1_file_path += LASER_TYPE_ID_DSDBR01_CSTRING;
				forward_photodiode1_file_path += USCORE;
				forward_photodiode1_file_path += _laser_id_CString;
				forward_photodiode1_file_path += USCORE;
				forward_photodiode1_file_path += _date_time_stamp_CString;
				char buf[10];
				sprintf( buf, "_DFS%d", i+1 );
				forward_photodiode1_file_path += CString( buf );
				forward_photodiode1_file_path += USCORE;
				forward_photodiode1_file_path += map_ramp_direction_CString;
				forward_photodiode1_file_path += CSV;

				_map_forward_photodiode1[i].writeToFile(
					forward_photodiode1_file_path,
					CG_REG->get_DSDBR01_OMDC_overwrite_existing_files() );
			}

			for(int i = 0; i < (int)(_map_forward_photodiode2.size()) ; i++ )
			{
				CString forward_photodiode2_file_path = results_dir_CString;
				forward_photodiode2_file_path += MATRIX_;
				forward_photodiode2_file_path += MEAS_TYPE_ID_FORWARDPD2_CSTRING;
				forward_photodiode2_file_path += USCORE;
				forward_photodiode2_file_path += LASER_TYPE_ID_DSDBR01_CSTRING;
				forward_photodiode2_file_path += USCORE;
				forward_photodiode2_file_path += _laser_id_CString;
				forward_photodiode2_file_path += USCORE;
				forward_photodiode2_file_path += _date_time_stamp_CString;
				char buf[10];
				sprintf( buf, "_DFS%d", i+1 );
				forward_photodiode2_file_path += CString( buf );
				forward_photodiode2_file_path += USCORE;
				forward_photodiode2_file_path += map_ramp_direction_CString;
				forward_photodiode2_file_path += CSV;

				_map_forward_photodiode2[i].writeToFile(
					forward_photodiode2_file_path,
					CG_REG->get_DSDBR01_OMDC_overwrite_existing_files() );
			}


			for(int i = 0; i < (int)(_map_reverse_photodiode1.size()) ; i++ )
			{
				CString reverse_photodiode1_file_path = results_dir_CString;
				reverse_photodiode1_file_path += MATRIX_;
				reverse_photodiode1_file_path += MEAS_TYPE_ID_REVERSEPD1_CSTRING;
				reverse_photodiode1_file_path += USCORE;
				reverse_photodiode1_file_path += LASER_TYPE_ID_DSDBR01_CSTRING;
				reverse_photodiode1_file_path += USCORE;
				reverse_photodiode1_file_path += _laser_id_CString;
				reverse_photodiode1_file_path += USCORE;
				reverse_photodiode1_file_path += _date_time_stamp_CString;
				char buf[10];
				sprintf( buf, "_DFS%d", i+1 );
				reverse_photodiode1_file_path += CString( buf );
				reverse_photodiode1_file_path += USCORE;
				reverse_photodiode1_file_path += map_ramp_direction_CString;
				reverse_photodiode1_file_path += CSV;

				_map_reverse_photodiode1[i].writeToFile(
					reverse_photodiode1_file_path,
					CG_REG->get_DSDBR01_OMDC_overwrite_existing_files() );
			}

			for(int i = 0; i < (int)(_map_reverse_photodiode2.size()) ; i++ )
			{
				CString reverse_photodiode2_file_path = results_dir_CString;
				reverse_photodiode2_file_path += MATRIX_;
				reverse_photodiode2_file_path += MEAS_TYPE_ID_REVERSEPD2_CSTRING;
				reverse_photodiode2_file_path += USCORE;
				reverse_photodiode2_file_path += LASER_TYPE_ID_DSDBR01_CSTRING;
				reverse_photodiode2_file_path += USCORE;
				reverse_photodiode2_file_path += _laser_id_CString;
				reverse_photodiode2_file_path += USCORE;
				reverse_photodiode2_file_path += _date_time_stamp_CString;
				char buf[10];
				sprintf( buf, "_DFS%d", i+1 );
				reverse_photodiode2_file_path += CString( buf );
				reverse_photodiode2_file_path += USCORE;
				reverse_photodiode2_file_path += map_ramp_direction_CString;
				reverse_photodiode2_file_path += CSV;

				_map_reverse_photodiode2[i].writeToFile(
					reverse_photodiode2_file_path,
					CG_REG->get_DSDBR01_OMDC_overwrite_existing_files() );
			}
		}
	}
	else
	{
		CString log_err_msg("CDSDBROverallModeMap::writeMapsToFile() error writing power ratio map to file");
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)
	}



	// map error codes

	if( write_to_file_rval == CLaserModeMap::rcode::vector_size_rows_cols_do_not_match
	 || write_Ir_to_file_rval == CCurrentsVector::rcode::vector_size_rows_cols_do_not_match
	 || write_If_to_file_rval == CDSDBRFrontCurrents::rcode::vector_size_rows_cols_do_not_match )
	{
		rval = rcode::corrupt_map_not_written_to_file;
		CGR_LOG("CDSDBROverallModeMap::writeMapsToFile() error: corrupt_map_not_written_to_file",ERROR_CGR_LOG)
	}
	else
	if( write_to_file_rval == CLaserModeMap::rcode::could_not_open_file
	 || write_Ir_to_file_rval == CCurrentsVector::rcode::could_not_open_file
	 || write_If_to_file_rval == CDSDBRFrontCurrents::rcode::could_not_open_file )
	{
		rval = rcode::could_not_open_file;
		CGR_LOG("CDSDBROverallModeMap::writeMapsToFile() error: could_not_open_file",ERROR_CGR_LOG)
	}
	else
	if( write_to_file_rval == CLaserModeMap::rcode::file_already_exists
	 || write_Ir_to_file_rval == CCurrentsVector::rcode::file_already_exists
	 || write_If_to_file_rval == CDSDBRFrontCurrents::rcode::file_already_exists )
	{
		CGR_LOG("CDSDBROverallModeMap::writeMapsToFile() error: file_already_exists",ERROR_CGR_LOG)
		rval = rcode::existing_file_not_overwritten;
	}

	CGR_LOG("CDSDBROverallModeMap::writeMapsToFile() exiting",DIAGNOSTIC_CGR_LOG)

	return rval;
}


CDSDBROverallModeMap::rcode
CDSDBROverallModeMap::collectMapsFromLaser()
{
	CGR_LOG("CDSDBROverallModeMap::collectMapsFromLaser() entered",DIAGNOSTIC_CGR_LOG)

	rcode rval = rcode::ok;
	HRESULT err = S_OK;

	// first, clear anything previously collected/loaded

	CString IrFilePath = CG_REG->get_DSDBR01_OMDC_rear_currents_abspath();
	CString IfFilePath = CG_REG->get_DSDBR01_OMDC_front_currents_abspath();
	bool	hysteresis = CG_REG->get_DSDBR01_OMDC_forward_and_reverse();
	CString	rearModuleName	= CG_REG->get_DSDBR01_CMDC_rear_module_name();
	CString powerMeterModuleName = CG_REG->get_DSDBR01_CMDC_306II_module_name();

	if( CG_REG->get_DSDBR01_OMDC_ramp_direction() == CString(OMDC_RAMP_REAR) )
		_Ir_ramp = true;
	else
		_Ir_ramp = false;

	int	sourceDelay = CG_REG->get_DSDBR01_OMDC_source_delay();
	int	measureDelay = CG_REG->get_DSDBR01_OMDC_measure_delay();


	// read front currrents from file
	CDSDBRFrontCurrents::rcode read_If_rcode = CDSDBRFrontCurrents::rcode::ok;

	read_If_rcode = _vector_If.readFromFile( IfFilePath );

	if( read_If_rcode != CDSDBRFrontCurrents::rcode::ok )
	{
		rval = CDSDBROverallModeMap::rcode::could_not_open_file;

		CString log_err_msg("CDSDBROverallModeMap::collectMapsFromLaser() error reading front currents from file: ");
		log_err_msg += IfFilePath;
		CGR_LOG(log_err_msg,ERROR_CGR_LOG)
	}

	if( rval == rcode::ok )
	{

		// read rear currrents from file
		CCurrentsVector::rcode read_Ir_rcode = CCurrentsVector::rcode::ok;

		read_Ir_rcode = _vector_Ir.readFromFile( IrFilePath );

		if( read_Ir_rcode != CCurrentsVector::rcode::ok )
		{
			rval = CDSDBROverallModeMap::rcode::could_not_open_file;

			CString log_err_msg("CDSDBROverallModeMap::collectMapsFromLaser() error reading rear currents from file: ");
			log_err_msg += IrFilePath;
			CGR_LOG(log_err_msg,ERROR_CGR_LOG)
		}
	}


	if( rval == rcode::ok )
	{
		// loop through each front pair


		CString nonConstantFrontModuleName;
		CString constantFrontModuleName;
		double	constantFrontCurrent;

		for( short pair_number = 1 ; pair_number <=7 ; pair_number++ )
		{
			setPercentComplete( 13*pair_number );

			std::vector<double> rear_sequence;
			std::vector<double> front_sequence;

			// We have the currents, now define sequences

			rval = defineSequence(
				pair_number,
				rear_sequence,
				front_sequence);


			nonConstantFrontModuleName = _vector_If.getNonConstantModuleName(pair_number);
			constantFrontModuleName = _vector_If.getConstantModuleName(pair_number);
			constantFrontCurrent = _vector_If.getConstantCurrent(pair_number,0);

			///////////////////////////////////////////////////////
			///
			// We have the sequence for gathering one submap

			//

			/////////////////////////////////////////////////////
			///
			// set up modules
			//

			//
			/// connect outputs
			if(rval == rcode::ok)
				rval = connectFrontPairOutputs(rearModuleName,
											nonConstantFrontModuleName,
											constantFrontModuleName);

			//
			// set in sequence
			if(rval == rcode::ok)
				rval = 	setFrontPairModulesInSequence(rearModuleName,
													nonConstantFrontModuleName,
													constantFrontModuleName,
													powerMeterModuleName);



			std::vector<double>	powerCh1;
			std::vector<double>	powerCh2;
			std::vector<double>	photodiodeICh1;
			std::vector<double>	photodiodeICh2;

			powerCh1.clear();
			powerCh2.clear();
			photodiodeICh1.clear();
			photodiodeICh2.clear();

			// split sequences if too big
			vector<CCurrentsVector> rearSeqs;
			vector<CCurrentsVector> frontSeqs;

			rearSeqs.clear();
			frontSeqs.clear();

			splitSequence(MAX_SEQUENCE_SIZE,
					front_sequence,
					frontSeqs);
					

			splitSequence(MAX_SEQUENCE_SIZE,
					rear_sequence,
					rearSeqs);

			int numSplitSequences = (int)(rearSeqs.size());


			for(int sequenceNum=0; sequenceNum<numSplitSequences; sequenceNum++)
			{

				//////////////////////////////////////////////////////
				///
				//	Load Sequences
				//
				if(rval == rcode::ok)
					rval = loadFrontPairSequences(rearModuleName,
												rearSeqs[sequenceNum]._currents,
												nonConstantFrontModuleName,
												frontSeqs[sequenceNum]._currents,
												powerMeterModuleName,
												sourceDelay,
												measureDelay);

				//
				/////////////////////////////////////////////////////

				//
				/// set constant front current
				if(rval == rcode::ok)
					err = SERVERINTERFACE->setCurrent(constantFrontModuleName, constantFrontCurrent);

				//
				/// reset sequences
				if(err == S_OK)
					err = SERVERINTERFACE->resetSequences();

				//
				/// run sequences
				if(err == S_OK)
					err = SERVERINTERFACE->runSequences();
			

				//
				/// reset sequences
				SERVERINTERFACE->resetSequences();


				/////////////////////////////////////////////////////////////////////////////////
				/// gather power
				//
				std::vector<double>	seqPowerCh1;
				std::vector<double>	seqPowerCh2;
				if(err == S_OK)
					err = SERVERINTERFACE->get306SequenceMeasurements(powerMeterModuleName,
																		&seqPowerCh1,
																		&seqPowerCh2);

				for(int i=0; i<(int)seqPowerCh1.size(); i++)
				{
					powerCh1.push_back(seqPowerCh1[i]);
					powerCh2.push_back(seqPowerCh2[i]);
				}
				//
				////////////////////////////////////////////////////////////////////////////////////

				/// reset sequences
				if(err == S_OK)
					err = SERVERINTERFACE->resetSequences();

				if(err == S_OK
					&& CG_REG->get_DSDBR01_OMDC_measure_photodiode_currents() )
				{
					/////////////////////////////////////////////////////////////////////////////////
					/// gather photodiode currents
					//
					std::vector<double>	seqPhotodiodeICh1;
					std::vector<double>	seqPhotodiodeICh2;
					if(err == S_OK)
						err = SERVERINTERFACE->get306SequenceMeasurements(CG_REG->get_DSDBR01_CMDC_306EE_module_name(),
																			&seqPhotodiodeICh1,
																			&seqPhotodiodeICh2);

					for(int i=0; i<(int)seqPhotodiodeICh1.size(); i++)
					{
						photodiodeICh1.push_back(seqPhotodiodeICh1[i]);
						photodiodeICh2.push_back(seqPhotodiodeICh2[i]);
					}
					//
					////////////////////////////////////////////////////////////////////////////////////

					/// reset sequences
					if(err == S_OK)
						err = SERVERINTERFACE->resetSequences();
				}

			}
			/////////////////////////////////////////////////////////////////////////////////////


			/// set constant front current AND DISCONNECT
			SERVERINTERFACE->setCurrent(constantFrontModuleName, 0);
			SERVERINTERFACE->setOutputConnection(constantFrontModuleName, OUTPUT_OFF);

			//
			// set NOT in sequence
			if(rval == rcode::ok)
				rval = 	setFrontPairModulesNotInSequence(rearModuleName,
													nonConstantFrontModuleName,
													constantFrontModuleName,
													powerMeterModuleName);


			for(int i_seq = 0 ; i_seq < (int)rearSeqs.size() ; i_seq++ )
				rearSeqs[i_seq].clear();
			for(int i_seq = 0 ; i_seq < (int)frontSeqs.size() ; i_seq++ )
				frontSeqs[i_seq].clear();
			rearSeqs.clear();
			frontSeqs.clear();

			if(err == S_OK)
			{
				int	numPointsPerCycle = 0;
				int	numCycles = 0;
				long number_of_rows = 0;
				long number_of_cols = 0;
				if( CG_REG->get_DSDBR01_OMDC_ramp_direction() == CString(OMDC_RAMP_REAR) )
				{
					// ramp rear, step front
					numPointsPerCycle = (int)(_vector_Ir._length);
					number_of_cols = (long)numPointsPerCycle;					

					numCycles = (int)(_vector_If._submap_length[pair_number-1]);
					number_of_rows = (long)numCycles;
				}
				else
				{
					// ramp front, step rear
					numPointsPerCycle = (int)(_vector_If._submap_length[pair_number-1]);
					number_of_cols = (long)numPointsPerCycle;

					numCycles = (int)(_vector_Ir._length);
					number_of_rows = (long)numCycles;
				}

				_xAxisLength = number_of_cols;
				_yAxisLength = number_of_rows;

				CLaserModeMap map_forward_direct_power;
				_map_forward_direct_power.push_back(map_forward_direct_power);

				CLaserModeMap map_forward_filtered_power;
				_map_forward_filtered_power.push_back(map_forward_filtered_power);

				std::vector<double>	forwardPowerCh1;
				std::vector<double>	reversePowerCh1;
				std::vector<double>	forwardPowerCh2;
				std::vector<double>	reversePowerCh2;


				CLaserModeMap map_forward_photodiode1;
				_map_forward_photodiode1.push_back(map_forward_photodiode1);

				CLaserModeMap map_forward_photodiode2;
				_map_forward_photodiode2.push_back(map_forward_photodiode2);

				std::vector<double>	forwardPhotodiodeICh1;
				std::vector<double>	reversePhotodiodeICh1;
				std::vector<double>	forwardPhotodiodeICh2;
				std::vector<double>	reversePhotodiodeICh2;

				if(hysteresis)
				{

					CLaserModeMap map_reverse_direct_power;
					_map_reverse_direct_power.push_back(map_reverse_direct_power);

					CLaserModeMap map_reverse_filtered_power;
					_map_reverse_filtered_power.push_back(map_reverse_filtered_power);

					rval = splitHysteresisPowerArray(
						powerCh1,
						numPointsPerCycle,
						forwardPowerCh1,
						reversePowerCh1);

					rval = splitHysteresisPowerArray(
						powerCh2,
						numPointsPerCycle,
						forwardPowerCh2,
						reversePowerCh2);

					if( CG_REG->get_DSDBR01_OMDC_measure_photodiode_currents() )
					{
						CLaserModeMap map_reverse_photodiode1;
						_map_reverse_photodiode1.push_back(map_reverse_photodiode1);

						CLaserModeMap map_reverse_photodiode2;
						_map_reverse_photodiode2.push_back(map_reverse_photodiode2);

						rval = splitHysteresisPowerArray(
							photodiodeICh1,
							numPointsPerCycle,
							forwardPhotodiodeICh1,
							reversePhotodiodeICh1);

						rval = splitHysteresisPowerArray(
							photodiodeICh2,
							numPointsPerCycle,
							forwardPhotodiodeICh2,
							reversePhotodiodeICh2);
					}
				}
				else
				{
					forwardPowerCh1 = powerCh1;
					reversePowerCh1.clear();
					forwardPowerCh2 = powerCh2;
					reversePowerCh2.clear();

					if( CG_REG->get_DSDBR01_OMDC_measure_photodiode_currents() )
					{
						forwardPhotodiodeICh1 = photodiodeICh1;
						reversePhotodiodeICh1.clear();
						forwardPhotodiodeICh2 = photodiodeICh2;
						reversePhotodiodeICh2.clear();
					}
				}

				if( rval == rcode::ok )
				{
					if( CG_REG->get_DSDBR01_CMDC_306II_direct_channel() == CHANNEL_1 )
					{
						// ch1 is direct, ch2 is filtered

						_map_forward_direct_power[pair_number-1].setMap(
							forwardPowerCh1,
							number_of_rows,
							number_of_cols );

						_map_forward_filtered_power[pair_number-1].setMap(
							forwardPowerCh2,
							number_of_rows,
							number_of_cols );

						if(hysteresis)
						{
							_map_reverse_direct_power[pair_number-1].setMap(
								reversePowerCh1,
								number_of_rows,
								number_of_cols );

							_map_reverse_filtered_power[pair_number-1].setMap(
								reversePowerCh2,
								number_of_rows,
								number_of_cols );
						}
					}
					else
					{
						// ch1 is filtered, ch2 is direct

						_map_forward_filtered_power[pair_number-1].setMap(
							forwardPowerCh1,
							number_of_rows,
							number_of_cols );

						_map_forward_direct_power[pair_number-1].setMap(
							forwardPowerCh2,
							number_of_rows,
							number_of_cols );


						if(hysteresis)
						{
							_map_reverse_filtered_power[pair_number-1].setMap(
								reversePowerCh1,
								number_of_rows,
								number_of_cols );

							_map_reverse_direct_power[pair_number-1].setMap(
								reversePowerCh2,
								number_of_rows,
								number_of_cols );
						}
					}
				}


				if( rval == rcode::ok )
				{
					if( CG_REG->get_DSDBR01_OMDC_measure_photodiode_currents() )
					{

						_map_forward_photodiode1[pair_number-1].setMap(
							forwardPhotodiodeICh1,
							number_of_rows,
							number_of_cols );

						_map_forward_photodiode2[pair_number-1].setMap(
							forwardPhotodiodeICh2,
							number_of_rows,
							number_of_cols );

						if(hysteresis)
						{
							_map_reverse_photodiode1[pair_number-1].setMap(
								reversePhotodiodeICh1,
								number_of_rows,
								number_of_cols );

							_map_reverse_photodiode2[pair_number-1].setMap(
								reversePhotodiodeICh2,
								number_of_rows,
								number_of_cols );
						}
					}
				}

				if( rval == rcode::ok )
				{
					std::vector<double> forward_power_ratio;
					for( int i = 0; i < (int)(_map_forward_direct_power[pair_number-1]._map.size()) ; i++ )
					{
						double p_ratio = _map_forward_filtered_power[pair_number-1]._map[i]
									/ _map_forward_direct_power[pair_number-1]._map[i];
						
						if( p_ratio < 0 || p_ratio > 1 ) // invalid value for ratio
						{
							CString err_message("CDSDBROverallModeMap::collectMapsFromLaser : ");
							err_message.AppendFormat("calculated p_ratio = %g",p_ratio);
							CGR_LOG(err_message,ERROR_CGR_LOG);

							rval = rcode::laser_alignment_error;
							break;
						}

						forward_power_ratio.push_back(p_ratio);
					}

					
					if( rval == rcode::ok )
					{
						switch( pair_number )
						{
							case 1:
								_map_power_ratio_dfs1.setMap(
									forward_power_ratio,
									number_of_rows,
									number_of_cols );
								break;
							case 2:
								_map_power_ratio_dfs2.setMap(
									forward_power_ratio,
									number_of_rows,
									number_of_cols );
								break;
							case 3:
								_map_power_ratio_dfs3.setMap(
									forward_power_ratio,
									number_of_rows,
									number_of_cols );
								break;
							case 4:
								_map_power_ratio_dfs4.setMap(
									forward_power_ratio,
									number_of_rows,
									number_of_cols );
								break;
							case 5:
								_map_power_ratio_dfs5.setMap(
									forward_power_ratio,
									number_of_rows,
									number_of_cols );
								break;
							case 6:
								_map_power_ratio_dfs6.setMap(
									forward_power_ratio,
									number_of_rows,
									number_of_cols );
								break;
							case 7:
								_map_power_ratio_dfs7.setMap(
									forward_power_ratio,
									number_of_rows,
									number_of_cols );
								break;
						}
					}
					else
					{
						break;
					}
				}
			}

			if( err != S_OK )
			{
				rval = mapHResultToRCode(err);
			}

			if( rval != rcode::ok )
				break;
		}


		/// set final nonconstant front current AND DISCONNECT
		err = SERVERINTERFACE->setCurrent(nonConstantFrontModuleName, 0);
		if( err != S_OK ) rval = rcode::hardware_error;

		err = SERVERINTERFACE->setOutputConnection(nonConstantFrontModuleName, OUTPUT_OFF);
		if( err != S_OK ) rval = rcode::hardware_error;


		/// set rear current AND DISCONNECT
		err = SERVERINTERFACE->setCurrent(rearModuleName, 0);
		if( err != S_OK ) rval = rcode::hardware_error;

		err = SERVERINTERFACE->setOutputConnection(rearModuleName, OUTPUT_OFF);
		if( err != S_OK ) rval = rcode::hardware_error;


	}

	if( rval == rcode::ok )
	{
		_maps_loaded = true;
		_rows = _vector_If._total_length;
		_cols = _vector_Ir._length;
	}
	else
		_maps_loaded = false;

	CGR_LOG("CDSDBROverallModeMap::collectMapsFromLaser() exiting",DIAGNOSTIC_CGR_LOG)

	return rval;
}

CDSDBROverallModeMap::rcode
CDSDBROverallModeMap::splitHysteresisPowerArray(
	std::vector<double>	&powerVector,
	int				numPointsPerCycle,
	std::vector<double>	&forwardPower,
	std::vector<double>	&reversePower)
{
	rcode rval = rcode::ok;


	forwardPower.clear();
	reversePower.clear();

	try
	{
		///////////////////////////////////////////

		int				i			= 0;
		int				j			= 0;
		int				flipIndex	= 0;
		vector<double>	tempArray;

		int numElements = (int)(powerVector.size());
		while(i<numElements)
		{
			////////////////////////////////////////////////////

			j=0;
			while(j<numPointsPerCycle)
			{
				if(i<numElements)
					forwardPower.push_back(powerVector[i]);
				else
					break;

				j++;
				i++;
			}

			////////////////////////////////////////////////////

			j=0;
			tempArray.clear();
			while(j<numPointsPerCycle)
			{
				if(i<numElements)
					tempArray.push_back(powerVector[i]);
				else
					break;

				j++;
				i++;
			}

			if(tempArray.size() > 0)
			{
				// flip the temp array
				reverse(tempArray.begin(), tempArray.end());
				vector<double>::iterator iter;
				for(iter=tempArray.begin(); iter!=tempArray.end(); iter++)
					reversePower.push_back(*iter);
			}
			////////////////////////////////////////////////////

		}
	}
	catch(...)
	{
		rval = rcode::sequence_data_format_error;
	}

	return rval;
}


CDSDBROverallModeMap::rcode
CDSDBROverallModeMap::
loadFrontPairSequences(CString			rearModuleName,
					std::vector<double>&	rear_sequence,
					CString				nonConstantFrontModuleName,
					std::vector<double>&	front_sequence,
					CString				powerMeterModuleName,
					int					sourceDelay,
					int					measureDelay)
{
	rcode rval = rcode::ok;
	HRESULT err = S_OK;

	// reset sequences first
	err = SERVERINTERFACE->resetSequences();

	// rear sequence
	if(err == S_OK)
	{
		rval = load305Sequence(
					rearModuleName,
					rear_sequence,
					sourceDelay,
					measureDelay );
	}
	else
	{
		rval = mapHResultToRCode(err);
	}

	// front non constant sequence
	if(rval == rcode::ok)
	{
		rval = load305Sequence(
					nonConstantFrontModuleName,
					front_sequence,
					sourceDelay,
					measureDelay );
	}

	// 306 sequence
	if(rval == rcode::ok)
	{
		rval = load306Sequence(
					powerMeterModuleName,
					(int)(rear_sequence.size()),
					sourceDelay,
					measureDelay );
	}

	// 306EE sequence
	if(rval == rcode::ok
		&& CG_REG->get_DSDBR01_OMDC_measure_photodiode_currents() )
	{
		rval = load306Sequence(
					CG_REG->get_DSDBR01_CMDC_306EE_module_name(),
					(int)(rear_sequence.size()),
					sourceDelay,
					measureDelay );
	}

	return rval;
}

CDSDBROverallModeMap::rcode
CDSDBROverallModeMap::
connectFrontPairOutputs(CString	rearModuleName,
						CString	nonConstantFrontModuleName,
						CString	constantFrontModuleName)
{
	rcode rval = rcode::ok;
	HRESULT err = S_OK;

	if(err == S_OK)
		err = SERVERINTERFACE->setOutputConnection(rearModuleName, CONNECT_OUTPUT);
	if(err == S_OK)
		err = SERVERINTERFACE->setOutputConnection(nonConstantFrontModuleName, CONNECT_OUTPUT);
	if(err == S_OK)
		err = SERVERINTERFACE->setOutputConnection(constantFrontModuleName, CONNECT_OUTPUT);

	if(err != S_OK)
	{
		rval = mapHResultToRCode(err);
	}

	return rval;
}

CDSDBROverallModeMap::rcode
CDSDBROverallModeMap::
setFrontPairModulesInSequence(CString	rearModuleName,
							CString		nonConstantFrontModuleName,
							CString		constantFrontModuleName,
							CString		powerMeterModuleName)
{
	rcode rval = rcode::ok;
	HRESULT err = S_OK;

	if(err == S_OK)
		err = SERVERINTERFACE->setInSequence(rearModuleName, IN_SEQUENCE);			
	if(err == S_OK)
		err = SERVERINTERFACE->setInSequence(nonConstantFrontModuleName, IN_SEQUENCE);
	if(err == S_OK)
		err = SERVERINTERFACE->setInSequence(powerMeterModuleName, IN_SEQUENCE);
	if( err == S_OK
		&& CG_REG->get_DSDBR01_OMDC_measure_photodiode_currents() )
		err = SERVERINTERFACE->setInSequence(CG_REG->get_DSDBR01_CMDC_306EE_module_name(), IN_SEQUENCE);
	if(err == S_OK)
		err = SERVERINTERFACE->setInSequence(constantFrontModuleName, NOT_IN_SEQUENCE);

	if(err != S_OK)
	{
		rval = mapHResultToRCode(err);
	}

	return rval;
}

CDSDBROverallModeMap::rcode
CDSDBROverallModeMap::
setFrontPairModulesNotInSequence(CString	rearModuleName,
							CString		nonConstantFrontModuleName,
							CString		constantFrontModuleName,
							CString		powerMeterModuleName)
{
	rcode rval = rcode::ok;
	HRESULT err = S_OK;

	if(err == S_OK)
		err = SERVERINTERFACE->setInSequence(rearModuleName, NOT_IN_SEQUENCE);			
	if(err == S_OK)
		err = SERVERINTERFACE->setInSequence(nonConstantFrontModuleName, NOT_IN_SEQUENCE);
	if(err == S_OK)
		err = SERVERINTERFACE->setInSequence(powerMeterModuleName, NOT_IN_SEQUENCE);
	if( err == S_OK
		&& CG_REG->get_DSDBR01_OMDC_measure_photodiode_currents() )
		err = SERVERINTERFACE->setInSequence(CG_REG->get_DSDBR01_CMDC_306EE_module_name(), NOT_IN_SEQUENCE);
	if(err == S_OK)
		err = SERVERINTERFACE->setInSequence(constantFrontModuleName, NOT_IN_SEQUENCE);

	if(err != S_OK)
	{
		rval = mapHResultToRCode(err);
	}

	return rval;
}

CDSDBROverallModeMap::rcode
CDSDBROverallModeMap::
load305Sequence(CString				moduleName,
				std::vector<double>&	sequence,
				int					sourceDelay,
				int					measureDelay)
{
	rcode rval = rcode::ok;
	HRESULT err = S_OK;

	if(err == S_OK)
		err = SERVERINTERFACE->load305Sequence(moduleName,
										sequence,
										sourceDelay,
										measureDelay);

	if(err != S_OK)
	{
		rval = mapHResultToRCode(err);		
	}

	return rval;
}

CDSDBROverallModeMap::rcode
CDSDBROverallModeMap::
load306Sequence(CString				moduleName,
				int					numSequencePoints,
				int					sourceDelay,
				int					measureDelay)
{
	rcode rval = rcode::ok;
	HRESULT err = S_OK;

	if(err == S_OK)
	err = SERVERINTERFACE->load306Sequence(moduleName,
										numSequencePoints,
										ALL_CHANNELS,
										sourceDelay,
										measureDelay);
	if(err != S_OK)
	{
		rval = mapHResultToRCode(err);
	}


	return rval;
}

CDSDBROverallModeMap::rcode
CDSDBROverallModeMap::defineSequence(
	short pair_number,
	std::vector<double> &rear_sequence,
	std::vector<double> &front_sequence )
{
	rcode rval = rcode::ok;
	rear_sequence.clear();
	front_sequence.clear();

	if( _vector_Ir.isEmpty() || _vector_If.isEmpty() )
	{
		rval = CDSDBROverallModeMap::rcode::currents_not_loaded;
	}

	if( rval == rcode::ok )
	{
		// Calculate the rear and front sequences to play

		std::vector<double> non_constant_front_currents
			= _vector_If.getNonConstantCurrentVector( pair_number );

		if( CG_REG->get_DSDBR01_OMDC_ramp_direction() == CString(OMDC_RAMP_REAR) )
		{
			// ramp rear, step front

			createSequences(
				_vector_Ir._currents,
				non_constant_front_currents,
				CG_REG->get_DSDBR01_OMDC_forward_and_reverse(),
				rear_sequence,
				front_sequence );
		}
		else
		{
			// ramp front, step rear

			createSequences(
				non_constant_front_currents,
				_vector_Ir._currents,
				CG_REG->get_DSDBR01_OMDC_forward_and_reverse(),
				front_sequence,
				rear_sequence );
		}
		

	}

	return rval;
}


CDSDBROverallModeMap::rcode
CDSDBROverallModeMap::createSequences(
	std::vector<double>&	rampCurrents,
	std::vector<double>&	stepCurrents,
	bool hysteresis,
	std::vector<double>& rampSequence,
	std::vector<double>& stepSequence )
{
	rcode rval = rcode::ok;

	if((rampCurrents.size()>0)&&(stepCurrents.size()>0))
	{
		////////////////////////////////////////////////////////////////////////
		///
		//
		int numCycles			= (int)stepCurrents.size();
		int numPointsPerCycle	= (int)rampCurrents.size();
		//
		/////////////////////////////////////////////////////////////////////////
		///
		//	create sequences
		for(int i=0; i<numCycles; i++)
		{
			std::vector<double>::iterator iter;
			for(iter = rampCurrents.begin(); iter!= rampCurrents.end(); iter++)
			{
				rampSequence.push_back(*iter);
			}
			
			if(hysteresis == true)
			{
				iter = rampCurrents.end();
				iter--;
				// change to make this compliant to ISO C++
				for(iter=iter; iter!=rampCurrents.begin(); iter--)
				{
					rampSequence.push_back(*iter);
				}
				// Add last one
				rampSequence.push_back(*iter);
			}
			//
			///////////////////////////////////////////
			//

			for(int j=0; j<numPointsPerCycle; j++)
			{
				stepSequence.push_back(stepCurrents[i]);

				if(hysteresis==true)
				{
					stepSequence.push_back(stepCurrents[i]);
				}
			}
		}
		//
		////////////////////////////////////////////////////////////////////////
	}
	else
	{
		rval = CDSDBROverallModeMap::rcode::no_data;
	}

	return rval;
}


bool
CDSDBROverallModeMap::loaded()
{
	return _maps_loaded;
}



std::pair<short, std::string>
CDSDBROverallModeMap::get_error_pair( rcode error_in )
{
	std::pair<short, std::string> return_error_pair;

	switch( error_in )
	{
		case file_does_not_exist:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_FILE_DOES_NOT_EXIST_ERROR )
			break;
		case file_already_exists:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_FILEOPEN_ERROR )
			break;
		case could_not_open_file:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_FILEOPEN_ERROR )
			break;
		case file_format_invalid:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_INVALID_FILE_FORMAT_ERROR )
			break;
		case files_not_matching_sizes:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_FILES_NOT_MATCHING_SIZES_ERROR )
			break;
		case corrupt_map_not_written_to_file:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_ERROR_WRITING_RESULTS_TO_FILE )
			break;
		case existing_file_not_overwritten:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_ERROR_WRITING_RESULTS_TO_FILE )
			break;
		case maps_not_loaded:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_MAPS_NOT_LOADED_ERROR )
			break;
		case currents_not_loaded:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_CURRENTS_NOT_LOADED_ERROR )
			break;
		case outside_map_dimensions:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_CORRUPT_MAP_ERROR )
			break;
		case corrupt_map_dimensions:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_CORRUPT_MAP_ERROR )
			break;
		case next_pt_not_found:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_CORRUPT_MAP_ERROR )
			break;
		case rhs_top_border_not_reached:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_BOUNDARY_DETECTION_ERROR )
			break;
		case no_data:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_NO_DATA_ERROR )
			break;
		case no_start_point:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_NO_DATA_ERROR )
			break;
		case no_supermodes_found:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_NO_VALID_SUPERMODES_FOUND )
			break;
		case submap_indexing_error:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_CORRUPT_MAP_ERROR )
			break;
		case sequencing_error:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_SEQUENCING_ERROR )
			break;
		case sequence_data_format_error:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_SEQUENCING_ERROR )
			break;
		case sequencing_step_size_error:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_SEQUENCING_STEP_SIZE_ERROR )
			break;
		case hardware_error:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_HARDWARE_ERROR )
			break;
		case system_error:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_SYSTEM_ERROR )
			break;
		case set_current:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_SET_CURRENT_ERROR )
			break;
		case registry_error:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_REGISTRY_ERROR )
			break;
		case log_file_error:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_LOGFILE_ERROR )
			break;
		case results_disk_full:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_RESULTS_DISK_FULL_ERROR )
			break;
		case connectivity_error:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_CONNECTIVITY_ERROR )
			break;
		case power_meter_not_found:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_POWER_METER_NOT_FOUND_ERROR )
			break;
		case wavemeter_not_found:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_WAVEMETER_NOT_FOUND_ERROR )
			break;
		case tec_not_found:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_TEC_NOT_FOUND_ERROR )
			break;
		case power_read_error:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_POWER_READ_ERROR )
			break;
		case duplicate_device_name:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_DUPLICATE_DEVICE_NAME_ERROR )
			break;
		case pxit_module_error:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_PXIT_MODULE_ERROR )
			break;
		case device_name_not_on_card:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_DEVICE_NAME_NOT_ON_CARD_ERROR )
			break;
		case device_306_firmware_obsolete:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_306_FIRMWARE_OBSOLETE_ERROR )
			break;
		case laser_alignment_error:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_LASER_ALIGNMENT_ERROR )
			break;
		case empty_sequence_slots:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_EMPTY_SEQUENCE_SLOTS_ERROR )
			break;
		case module_name_not_found_error:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_MODULE_NAME_NOT_FOUND_ERROR )
			break;
		default:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_UNKNOWN_ERROR )
	}

	return return_error_pair;
}




void
CDSDBROverallModeMap::clear()
{
	_vector_Ir.clear();
	_vector_If.clear();

	for(int i = 0; i < (int)(_map_forward_direct_power.size()) ; i++ )
	{
		_map_forward_direct_power[i].clear();
	}
	_map_forward_direct_power.clear();

	for(int i = 0; i < (int)(_map_reverse_direct_power.size()) ; i++ )
	{
		_map_reverse_direct_power[i].clear();
	}
	_map_reverse_direct_power.clear();

	for(int i = 0; i < (int)(_map_forward_filtered_power.size()) ; i++ )
	{
		_map_forward_filtered_power[i].clear();
	}
	_map_forward_filtered_power.clear();

	for(int i = 0; i < (int)(_map_reverse_filtered_power.size()) ; i++ )
	{
		_map_reverse_filtered_power[i].clear();
	}
	_map_reverse_filtered_power.clear();

	for(int i = 0; i < (int)(_map_forward_photodiode1.size()) ; i++ )
	{
		_map_forward_photodiode1[i].clear();
	}
	_map_forward_photodiode1.clear();

	for(int i = 0; i < (int)(_map_forward_photodiode2.size()) ; i++ )
	{
		_map_forward_photodiode2[i].clear();
	}
	_map_forward_photodiode2.clear();

	for(int i = 0; i < (int)(_map_reverse_photodiode1.size()) ; i++ )
	{
		_map_reverse_photodiode1[i].clear();
	}
	_map_reverse_photodiode1.clear();

	for(int i = 0; i < (int)(_map_reverse_photodiode2.size()) ; i++ )
	{
		_map_reverse_photodiode2[i].clear();
	}
	_map_reverse_photodiode2.clear();


	for(int i = 0; i < (int)(_map_max_deltaPr.size()) ; i++ )
	{
		_map_max_deltaPr[i].clear();
	}
	_map_max_deltaPr.clear();


	_map_power_ratio_dfs1.clear();
	_map_power_ratio_dfs2.clear();
	_map_power_ratio_dfs3.clear();
	_map_power_ratio_dfs4.clear();
	_map_power_ratio_dfs5.clear();
	_map_power_ratio_dfs6.clear();
	_map_power_ratio_dfs7.clear();

	_map_power_ratio_dfs1_median_filtered.clear();
	_map_power_ratio_dfs2_median_filtered.clear();
	_map_power_ratio_dfs3_median_filtered.clear();
	_map_power_ratio_dfs4_median_filtered.clear();
	_map_power_ratio_dfs5_median_filtered.clear();
	_map_power_ratio_dfs6_median_filtered.clear();
	_map_power_ratio_dfs7_median_filtered.clear();

	for(int i = 0; i < (int)(_supermodes.size()) ; i++ )
	{
		_supermodes[i].clear();
	}
	_supermodes.clear();

	_supermodes_removed = 0;

	_maps_loaded = false;
	_Ir_ramp = false;
	_rows = 0;
	_cols = 0;
	_results_dir_CString = "";
	_date_time_stamp_CString = "";
	_laser_id_CString = "";

	_xAxisLength = 0;
	_yAxisLength = 0;

	//_threshold_maxDeltaPr = 0;

	return;
}





CDSDBROverallModeMap::rcode
CDSDBROverallModeMap::screen()
{
	CGR_LOG("CDSDBROverallModeMap::screen() entered",DIAGNOSTIC_CGR_LOG)

	rcode rval = rcode::ok;

	if( CG_REG->get_DSDBR01_OMBD_read_boundaries_from_file() )
	{
		// Step 1: Read in boundaries from file
		rval = readBoundariesFromFile();
	}
	else
	{
		//// Step 1: Find boundaries
		rval = findSMBoundaries();
	}

	// Step 2: Find middle lines in supermodes
	if( rval == CDSDBROverallModeMap::rcode::ok )
		rval = findSMMiddleLines();

	// Step 3: Filter middle lines in supermodes
	if( rval == CDSDBROverallModeMap::rcode::ok )
		rval = filterSMMiddleLines();

	// Step 4: Check each middle line
	// if not okay, remove the supermode

	_supermodes_removed = (short)(_supermodes.size());

	if( rval == CDSDBROverallModeMap::rcode::ok )
		rval = checkSMMiddleLines();

	_supermodes_removed -= (short)(_supermodes.size());

	// Step 5: For each supermode found
	// set its sm_number internally
	// set the row of the midpoint of its middle line
	// and set pointers for percent complete
	for( short i = 0; i < (short)(_supermodes.size()) ; i++ )
	{
		_supermodes[i]._p_data_protect = _p_data_protect;
		_supermodes[i]._p_percent_complete = _p_percent_complete;
		_supermodes[i]._sm_number = i;
		CDSDBROverallModeMapLine::rcode rv_ust = _supermodes[i]._filtered_middle_line.updateSubmapTracking();
		if(rv_ust == CDSDBROverallModeMapLine::rcode::ok)
		{
			long index_of_midpoint = _supermodes[i]._filtered_middle_line._total_length / 2;
			_supermodes[i]._row_of_midpoint_of_filtered_middle_line =
				_supermodes[i]._filtered_middle_line._points[index_of_midpoint].row;

			CString log_msg("CDSDBROverallModeMap::screen() ");
			log_msg.AppendFormat("row of midpoint of filtered middle line of supermode %d is %d",
				i, _supermodes[i]._row_of_midpoint_of_filtered_middle_line);
			CGR_LOG(log_msg,INFO_CGR_LOG)
		}
		else
		{
			CString log_msg("CDSDBROverallModeMap::screen() ");
			log_msg.AppendFormat("error updating indexing of filtered middle line of supermode %d",i);
			CGR_LOG(log_msg,ERROR_CGR_LOG)
		}
	}


	//////////////////////////////////////////////////////////////////////////////
	/////
	////
	setContinuityValues();
	

	_overallModeMapAnalysis.init();

	_overallModeMapAnalysis.runAnalysis(&_supermodes,
										_xAxisLength,
										_yAxisLength,
										_continuityValues,
										_supermodes_removed,
										_results_dir_CString,
										_laser_id_CString,
										_date_time_stamp_CString);
	
	///////////////////////////////////////////////////////////////////////////

	CGR_LOG("CDSDBROverallModeMap::screen() exiting",DIAGNOSTIC_CGR_LOG)

	return rval;
}

short
CDSDBROverallModeMap::supermode_count()
{
	short sm_count = 0;

	for( short sm_number = 0; sm_number < (short)(_supermodes.size()) ; sm_number++ )
	{
		if( !(_supermodes[sm_number]._filtered_middle_line._points.empty()) )
		{
			sm_count++;
		}
	}

	return sm_count;
}

CDSDBROverallModeMap::rcode
CDSDBROverallModeMap::getAveragePowersOnMiddleLines(
	std::vector<double> &average_powers_on_middle_lines )
{
	CGR_LOG("CDSDBROverallModeMap::getAveragePowersOnMiddleLines() entered",DIAGNOSTIC_CGR_LOG)

	rcode rval = rcode::ok;
	average_powers_on_middle_lines.clear();

	for( short sm_number = 0; sm_number < (short)(_supermodes.size()) ; sm_number++ )
	{
		if( !(_supermodes[sm_number]._filtered_middle_line._points.empty())
		 && _map_forward_direct_power.size() == 7 )
		{
			double average_power = 0;
			long line_length = (long)(_supermodes[sm_number]._filtered_middle_line._points.size());
			for( long i = 0; i < line_length ; i++ )
			{
				long row = _supermodes[sm_number]._filtered_middle_line._points[i].row;
				long col = _supermodes[sm_number]._filtered_middle_line._points[i].col;

				double direct_power_at_pt = *iDirectPowerPt( row, col );

				average_power += direct_power_at_pt;
			}
			average_power /= line_length;

			average_powers_on_middle_lines.push_back(average_power);

			CString log_msg("CDSDBROverallModeMap::getAveragePowersOnMiddleLines() ");
			log_msg.AppendFormat("average power on supermode %d is %g mW", sm_number, average_power );
			CGR_LOG(log_msg,INFO_CGR_LOG)
		}
		else
		{
			average_powers_on_middle_lines.push_back(0);

			CString log_msg("CDSDBROverallModeMap::getAveragePowersOnMiddleLines() ");
			log_msg.AppendFormat("no middle line found for supermode %d", sm_number );
			CGR_LOG(log_msg,ERROR_CGR_LOG)
		}
	}

	CGR_LOG("CDSDBROverallModeMap::getAveragePowersOnMiddleLines() exiting",DIAGNOSTIC_CGR_LOG)

	return rval;
}

CDSDBROverallModeMap::rcode
CDSDBROverallModeMap::writeScreeningResultsToFile()
{
	CGR_LOG("CDSDBROverallModeMap::writeScreeningResultsToFile() entered",DIAGNOSTIC_CGR_LOG)

	rcode rval = rcode::ok;

	CString lines_abs_filepath = _results_dir_CString;
	if(lines_abs_filepath.Right(1) != _T("\\") )
		lines_abs_filepath += _T("\\");
	lines_abs_filepath += ("lines_OverallMap_");
	lines_abs_filepath += LASER_TYPE_ID_DSDBR01_CSTRING;
	lines_abs_filepath += USCORE;
	lines_abs_filepath += _laser_id_CString;
	lines_abs_filepath += USCORE;
	lines_abs_filepath += _date_time_stamp_CString;
	lines_abs_filepath += CSV;

	// write 1 file containing row/col coordinates of
	// upper boundary line, lower boundary line and middle line
	// of all supermodes found
	rval = writeSMLinesToFile(
		lines_abs_filepath,
		CG_REG->get_DSDBR01_OMDC_overwrite_existing_files() );


	// For each supermode found
	// write a file containing currents of its middle line.
	// This describes the current to be used when gathering
	// a supermode's map
	CDSDBROverallModeMapLine::rcode lrval = CDSDBROverallModeMapLine::rcode::ok;

	for( short sm_number = 0; sm_number < (short)(_supermodes.size()) && rval == rcode::ok ; sm_number++ )
	{
		char sm_number_str[10];
		sprintf( sm_number_str, "%d", sm_number );

		CString middle_line_abs_filepath = _results_dir_CString;
		if(middle_line_abs_filepath.Right(1) != _T("\\") ) 
			middle_line_abs_filepath += _T("\\");
		middle_line_abs_filepath += MIDDLELINE_CURRENT_CSTRING;
		middle_line_abs_filepath += USCORE;
		middle_line_abs_filepath += LASER_TYPE_ID_DSDBR01_CSTRING;
		middle_line_abs_filepath += USCORE;
		middle_line_abs_filepath += _laser_id_CString;
		middle_line_abs_filepath += USCORE;
		middle_line_abs_filepath += _date_time_stamp_CString;
		middle_line_abs_filepath += USCORE;
		middle_line_abs_filepath += CString("SM");
		middle_line_abs_filepath += CString( sm_number_str );


		//CString ufmiddle_line_abs_filepath = middle_line_abs_filepath + _T("_uf");;
		middle_line_abs_filepath += CSV;
		//ufmiddle_line_abs_filepath += CSV;

		lrval = _supermodes[sm_number]._filtered_middle_line.writeCurrentsToFile(
			middle_line_abs_filepath,
			CG_REG->get_DSDBR01_OMDC_overwrite_existing_files() );
		if( lrval != CDSDBROverallModeMapLine::rcode::ok )
			break;


		//lrval = _supermodes[sm_number]._middle_line.writeCurrentsToFile(
		//	ufmiddle_line_abs_filepath,
		//	CG_REG->get_DSDBR01_OMDC_overwrite_existing_files() );
		//if( lrval != CDSDBROverallModeMapLine::rcode::ok )
		//	break;

		if( CG_REG->get_DSDBR01_OMBD_write_boundaries_to_file() )
		{
			CString lower_line_abs_filepath = _results_dir_CString;
			if(lower_line_abs_filepath.Right(1) != _T("\\") )
				lower_line_abs_filepath += _T("\\");
			lower_line_abs_filepath += CString("lower_boundary_line_");
			lower_line_abs_filepath += CString( sm_number_str );
			lower_line_abs_filepath += CString( ".txt" );

			lrval =_supermodes[sm_number]._lower_line.writeRowColToFile(
				lower_line_abs_filepath,
				CG_REG->get_DSDBR01_OMDC_overwrite_existing_files() );
			if( lrval != CDSDBROverallModeMapLine::rcode::ok )
				break;

			CString upper_line_abs_filepath = _results_dir_CString;
			if(upper_line_abs_filepath.Right(1) != _T("\\") )
				upper_line_abs_filepath += _T("\\");
			upper_line_abs_filepath += CString("upper_boundary_line_");
			upper_line_abs_filepath += CString( sm_number_str );
			upper_line_abs_filepath += CString( ".txt" );

			lrval =_supermodes[sm_number]._upper_line.writeRowColToFile(
				upper_line_abs_filepath,
				CG_REG->get_DSDBR01_OMDC_overwrite_existing_files() );
			if( lrval != CDSDBROverallModeMapLine::rcode::ok )
				break;

		}
	}

	if( lrval == CDSDBROverallModeMapLine::rcode::could_not_open_file )
		rval = rcode::could_not_open_file;
	if( lrval == CDSDBROverallModeMapLine::rcode::file_already_exists )
		rval = rcode::file_already_exists;
	if( lrval == CDSDBROverallModeMapLine::rcode::no_data )
		rval = rcode::no_data;

	//	CString middle_line_abs_filepath = _results_dir_CString;
	//  if(middle_line_abs_filepath.Right(1) != _T("\\") )
	//		middle_line_abs_filepath += _T("\\");
	//	middle_line_abs_filepath += ("filtered_middle_line_");
	//	_supermodes[sm_number]._filtered_middle_line.writeRowColToFile(
	//		middle_line_abs_filepath,
	//		CG_REG->get_DSDBR01_OMDC_overwrite_existing_files()  );


	CString continuity_abs_filepath = _results_dir_CString;
	if(continuity_abs_filepath.Right(1) != _T("\\") )
		continuity_abs_filepath += _T("\\");
	continuity_abs_filepath += ("PowerRatio_Continuity_");
	continuity_abs_filepath += LASER_TYPE_ID_DSDBR01_CSTRING;
	continuity_abs_filepath += USCORE;
	continuity_abs_filepath += _laser_id_CString;
	continuity_abs_filepath += USCORE;
	continuity_abs_filepath += _date_time_stamp_CString;
	continuity_abs_filepath += CSV;

	// Write 1 file containing the Power Ratio values
	// found on the middle lines of all supermodes found
	if( rval == rcode::ok )
	{
		rval = writePRatioContinuityToFile(
			continuity_abs_filepath,
			CG_REG->get_DSDBR01_OMDC_overwrite_existing_files() );
	}



	CGR_LOG("CDSDBROverallModeMap::writeScreeningResultsToFile() exiting",DIAGNOSTIC_CGR_LOG)

	return rval;
}


CDSDBROverallModeMap::rcode
CDSDBROverallModeMap::applyMedianFilterToMaps()
{
	rcode rval = rcode::ok;

	if( rval == rcode::ok  )
		rval = applyMedianFilter( _map_power_ratio_dfs1, _map_power_ratio_dfs1_median_filtered );

	if( rval == rcode::ok  )
		rval = applyMedianFilter( _map_power_ratio_dfs2, _map_power_ratio_dfs2_median_filtered );

	if( rval == rcode::ok  )
		rval = applyMedianFilter( _map_power_ratio_dfs3, _map_power_ratio_dfs3_median_filtered );

	if( rval == rcode::ok  )
		rval = applyMedianFilter( _map_power_ratio_dfs4, _map_power_ratio_dfs4_median_filtered );

	if( rval == rcode::ok  )
		rval = applyMedianFilter( _map_power_ratio_dfs5, _map_power_ratio_dfs5_median_filtered );

	if( rval == rcode::ok  )
		rval = applyMedianFilter( _map_power_ratio_dfs6, _map_power_ratio_dfs6_median_filtered );

	if( rval == rcode::ok  )
		rval = applyMedianFilter( _map_power_ratio_dfs7, _map_power_ratio_dfs7_median_filtered );

	if( rval != rcode::ok )
		CGR_LOG("CDSDBROverallModeMap::applyMedianFilterToMaps error applying median filter to power ratio maps",ERROR_CGR_LOG)

	return rval;
}

CDSDBROverallModeMap::rcode
CDSDBROverallModeMap::applyMedianFilter( CLaserModeMap &map_in, CLaserModeMap &filtered_map_out )
{
	rcode rval = rcode::ok;

	VectorAnalysis::medianFilter(
		map_in._map,
		filtered_map_out._map,
		(unsigned long)CG_REG->get_DSDBR01_OMBD_median_filter_rank(),
		(unsigned long)(map_in._cols) );

	// update the rows and cols

	filtered_map_out._rows = map_in._rows;
	filtered_map_out._cols = map_in._cols;

#ifdef _DEBUG

	//// Assume _abs_filepath is set from loading data from file
	//// or from writing to file after gathering data from laser
	//CString abs_filepath = map_in._abs_filepath;
	//abs_filepath.Replace( MATRIX_ , MATRIX_FILTERED_ );

	//// Write filtered map to file
	//filtered_map_out.writeToFile( abs_filepath,
	//	CG_REG->get_DSDBR01_OMDC_overwrite_existing_files() );

#endif

	return rval;
}



CDSDBROverallModeMap::rcode
CDSDBROverallModeMap::findSMBoundaries()
{
	CGR_LOG("CDSDBROverallModeMap::findSMBoundaries() entered",DIAGNOSTIC_CGR_LOG)

	rcode rval = rcode::ok;

	if( !_maps_loaded )
	{
		rval = rcode::maps_not_loaded;
		CGR_LOG("CDSDBROverallModeMap::findSMBoundaries error: maps_not_loaded",ERROR_CGR_LOG)
	}
	else
	{
		rval = applyMedianFilterToMaps();
	}

	if( rval == rcode::ok )
	{
		// calculate a map of max delta Pr
		// from each point to its immediate neighbours
		rval = calculateMaxDeltaPrMap();
	}



	double threshold_maxDeltaPr = CG_REG->get_DSDBR01_OMBD_max_deltaPr_in_sm();
	//double threshold_maxDeltaPr = _threshold_maxDeltaPr;

	if( rval == rcode::ok )
	{
		// Start from end of bottom_border_filtered, work back to origin
		// Then move up lhs_border_filtered
		// This should identify all supermodes

		std::vector<double> maxDeltaPr_on_bottom_lhs_border;

		for( long i = 0; i < _cols-1; i++ )
			maxDeltaPr_on_bottom_lhs_border.push_back( *iMaxDeltaPrPt( 0, _cols - 1 - i ) );
		for( long i = 0; i < _rows; i++ )
			maxDeltaPr_on_bottom_lhs_border.push_back( *iMaxDeltaPrPt( i, 0 ) );

		// Apply median filter to border data

		//std::vector<double> maxDeltaPr_on_bottom_lhs_border_filtered;

		//VectorAnalysis::medianFilter(
		//	maxDeltaPr_on_bottom_lhs_border,
		//	maxDeltaPr_on_bottom_lhs_border_filtered,
		//	(unsigned long)CG_REG->get_DSDBR01_OMBD_median_filter_rank(),
		//	(unsigned long)(maxDeltaPr_on_bottom_lhs_border.size()) );

		// find start points of upper and lower lines

		std::vector<long> bottom_lhs_lower_line_start_pts;
		std::vector<long> bottom_lhs_upper_line_start_pts;
		bottom_lhs_lower_line_start_pts.clear();
		bottom_lhs_upper_line_start_pts.clear();

		long width_of_sm = 0;
		long start_of_sm = 0;
		long end_of_sm = 0;

		for( long i = 0; i < (long)(maxDeltaPr_on_bottom_lhs_border.size()); i++ )
		{
			double maxDeltaPr = maxDeltaPr_on_bottom_lhs_border[i];

			if( maxDeltaPr > threshold_maxDeltaPr )
			{
				if( width_of_sm >= CG_REG->get_DSDBR01_OMBD_min_points_width_of_sm() )
				{
					end_of_sm = i;

					// record start of lower line and upper line
					bottom_lhs_lower_line_start_pts.push_back(start_of_sm);
					bottom_lhs_upper_line_start_pts.push_back(end_of_sm);

				}

				width_of_sm = 0;
				start_of_sm = i;
			}
			else
			{
				width_of_sm++;
			}
		}

		

		CString log_msg("CDSDBROverallModeMap::findSMBoundaries() ");
		log_msg.AppendFormat("found %d possible supermodes on bottom_lhs border", bottom_lhs_lower_line_start_pts.size() );
		CGR_LOG(log_msg,INFO_CGR_LOG)



		if( bottom_lhs_lower_line_start_pts.size() > 0 )
		{
			// Have found the start points of the
			// upper and lower lines on the lhs-bottom border

			_supermodes.clear();

			for( short sm_number = 0 ;
				 sm_number < (short)(bottom_lhs_lower_line_start_pts.size()) ;
				 sm_number++ )
			{
				boundary_point_type lower_line_start_point;
				boundary_point_type upper_line_start_point;

				lower_line_start_point.found_in_direction = START_PT;
				upper_line_start_point.found_in_direction = START_PT;

				if( bottom_lhs_lower_line_start_pts[sm_number] > _cols-1 )
				{
					// first_pt_on_lower_line is on lhs border
					lower_line_start_point.row = bottom_lhs_lower_line_start_pts[sm_number]-(_cols-1);
					lower_line_start_point.col = 0;
				}
				else
				{
					// first_pt_on_lower_line is on bottom border
					lower_line_start_point.row = 0;
					lower_line_start_point.col = (_cols-1)-bottom_lhs_lower_line_start_pts[sm_number];
				}

				if( bottom_lhs_upper_line_start_pts[sm_number] > _cols-1 )
				{
					// first_pt_on_lower_line is on lhs border
					upper_line_start_point.row = bottom_lhs_upper_line_start_pts[sm_number]-(_cols-1);
					upper_line_start_point.col = 0;
				}
				else
				{
					// first_pt_on_lower_line is on bottom border
					upper_line_start_point.row = 0;
					upper_line_start_point.col = (_cols-1)-bottom_lhs_upper_line_start_pts[sm_number];
				}

				std::vector<boundary_point_type> lower_boundary_line;
				std::vector<boundary_point_type> upper_boundary_line;

				lower_boundary_line.push_back(lower_line_start_point);
				upper_boundary_line.push_back(upper_line_start_point);



				// Next, attempt to find the remaining points on upper and lower lines

				rcode find_lbline = rcode::ok;
				rcode find_ubline = rcode::ok;

				find_lbline = findBoundaryLineOnMaxDeltaPrMap( 
					lower_boundary_line,
					threshold_maxDeltaPr,
					direction_type::SW,
					false ); // false => search clockwise 

				if( find_lbline == rcode::ok )
				{
					CString log_msg("CDSDBROverallModeMap::findSMBoundaries() ");
					log_msg.AppendFormat("found %d points on lower boundary of supermode %d",
						lower_boundary_line.size(), sm_number );
					CGR_LOG(log_msg,INFO_CGR_LOG)
				}
				else
				{
					CString log_msg("CDSDBROverallModeMap::findSMBoundaries() ");
					log_msg.AppendFormat("error occurred while searching for lower boundary of supermode %d", sm_number );
					CGR_LOG(log_msg,ERROR_CGR_LOG)
				}

				find_ubline = findBoundaryLineOnMaxDeltaPrMap( 
					upper_boundary_line,
					threshold_maxDeltaPr,
					direction_type::SW,
					true ); // true => search anticlockwise 

				if( find_ubline == rcode::ok )
				{
					CString log_msg("CDSDBROverallModeMap::findSMBoundaries() ");
					log_msg.AppendFormat("found %d points on upper boundary of supermode %d",
						upper_boundary_line.size(), sm_number );
					CGR_LOG(log_msg,INFO_CGR_LOG)
				}
				else
				{
					CString log_msg("CDSDBROverallModeMap::findSMBoundaries() ");
					log_msg.AppendFormat("error occurred while searching for upper boundary of supermode %d", sm_number );
					CGR_LOG(log_msg,ERROR_CGR_LOG)
				}



				// write upper and lower lines to file

				CString lower_line_abs_filepath = _results_dir_CString;
				if(lower_line_abs_filepath.Right(1) != _T("\\") )
					lower_line_abs_filepath += _T("\\");
				CString upper_line_abs_filepath = lower_line_abs_filepath;
				CString map_max_deltaPr_filepath = lower_line_abs_filepath;
				lower_line_abs_filepath += _T("lowerline");
				upper_line_abs_filepath += _T("upperline");
				char numbuf[10];
				sprintf( numbuf , "%d.txt", sm_number );
				lower_line_abs_filepath += _T(numbuf);
				upper_line_abs_filepath += _T(numbuf);

				if( find_lbline == rcode::ok
				 && find_ubline == rcode::ok )
				{
					CDSDBRSuperMode new_sm(sm_number, _xAxisLength, _yAxisLength);

					for( long i = 0 ; i < (long)(lower_boundary_line.size()) ; i++ )
					{
						new_sm._lower_line._points.push_back(
							convertRowCol2LinePoint(
								lower_boundary_line[i].row,
								lower_boundary_line[i].col ) );
					}

					for( long i = 0 ; i < (long)(upper_boundary_line.size()) ; i++ )
					{
						new_sm._upper_line._points.push_back(
							convertRowCol2LinePoint(
								upper_boundary_line[i].row,
								upper_boundary_line[i].col ) );
					}

					//writeBoundaryLineToFile(
					//	lower_boundary_line,
					//	lower_line_abs_filepath,
					//	true );

					//writeBoundaryLineToFile(
					//	upper_boundary_line,
					//	upper_line_abs_filepath,
					//	true );

					_supermodes.push_back(new_sm);
				}
			}
		}
		else
		{
			rval = rcode::no_supermodes_found;
			CGR_LOG("CDSDBROverallModeMap::findSMBoundaries error: no_supermodes_found",ERROR_CGR_LOG)
		}
	}

	if( rval == rcode::ok && _supermodes.size() == 0)
	{
		rval = rcode::no_supermodes_found;
		CGR_LOG("CDSDBROverallModeMap::findSMBoundaries error: no_supermodes_found",ERROR_CGR_LOG)
	}

	CGR_LOG("CDSDBROverallModeMap::findSMBoundaries() exiting",DIAGNOSTIC_CGR_LOG)

	return rval;
}


CDSDBROverallModeMap::rcode
CDSDBROverallModeMap::calculateMaxDeltaPrMap()
{
	rcode rval = rcode::ok;

	CGR_LOG("CDSDBROverallModeMap::calculateMaxDeltaPrMap() entered",DIAGNOSTIC_CGR_LOG)

	// clear previous maps
	for(int i = 0; i < (int)(_map_max_deltaPr.size()) ; i++ )
	{
		_map_max_deltaPr[i].clear();
	}
	_map_max_deltaPr.clear();

	// create new maps (copy Pr values to start with)
	for( short map_num = 1 ; map_num <= 7 ; map_num++ )
	{
		CLaserModeMap map_max_deltaPr;

		long rows = 0;
		long cols = 0;
		double max_deltaPr = 0;
		bool is_empty = false;
		std::vector<double> *p_Pr_values;

		switch( map_num )
		{
		case 1:
			is_empty = _map_power_ratio_dfs1_median_filtered.isEmpty();
			rows = _map_power_ratio_dfs1_median_filtered._rows;
			cols = _map_power_ratio_dfs1_median_filtered._cols;
			p_Pr_values = &(_map_power_ratio_dfs1_median_filtered._map);
			break;
		case 2:
			is_empty = _map_power_ratio_dfs2_median_filtered.isEmpty();
			rows = _map_power_ratio_dfs2_median_filtered._rows;
			cols = _map_power_ratio_dfs2_median_filtered._cols;
			p_Pr_values = &(_map_power_ratio_dfs2_median_filtered._map);
			break;
		case 3:
			is_empty = _map_power_ratio_dfs3_median_filtered.isEmpty();
			rows = _map_power_ratio_dfs3_median_filtered._rows;
			cols = _map_power_ratio_dfs3_median_filtered._cols;
			p_Pr_values = &(_map_power_ratio_dfs3_median_filtered._map);
			break;
		case 4:
			is_empty = _map_power_ratio_dfs4_median_filtered.isEmpty();
			rows = _map_power_ratio_dfs4_median_filtered._rows;
			cols = _map_power_ratio_dfs4_median_filtered._cols;
			p_Pr_values = &(_map_power_ratio_dfs4_median_filtered._map);
			break;
		case 5:
			is_empty = _map_power_ratio_dfs5_median_filtered.isEmpty();
			rows = _map_power_ratio_dfs5_median_filtered._rows;
			cols = _map_power_ratio_dfs5_median_filtered._cols;
			p_Pr_values = &(_map_power_ratio_dfs5_median_filtered._map);
			break;
		case 6:
			is_empty = _map_power_ratio_dfs6_median_filtered.isEmpty();
			rows = _map_power_ratio_dfs6_median_filtered._rows;
			cols = _map_power_ratio_dfs6_median_filtered._cols;
			p_Pr_values = &(_map_power_ratio_dfs6_median_filtered._map);
			break;
		case 7:
			is_empty = _map_power_ratio_dfs7_median_filtered.isEmpty();
			rows = _map_power_ratio_dfs7_median_filtered._rows;
			cols = _map_power_ratio_dfs7_median_filtered._cols;
			p_Pr_values = &(_map_power_ratio_dfs7_median_filtered._map);
			break;
		}

		if( !is_empty )
		{
			CLaserModeMap::rcode lmmrval = CLaserModeMap::rcode::ok;

			lmmrval = map_max_deltaPr.setMap( *p_Pr_values, rows, cols );
			if( lmmrval != CLaserModeMap::rcode::ok )
			{
				rval = rcode::corrupt_map_dimensions;
				CGR_LOG("CDSDBROverallModeMap::calculateMaxDeltaPrMap error: corrupt_map_dimensions",ERROR_CGR_LOG)
				break;
			}
		}

		_map_max_deltaPr.push_back(map_max_deltaPr);
	}

	// calculate max delta Pr at each point on map
	double lowest_max_deltaPr = 1.1;
	double highest_max_deltaPr = -0.1;

	for( long row = 0 ; row < _rows ; row++ )
	{
		for( long col = 0 ; col < _cols ; col++ )
		{
			double Pr_at_point = *iPowerRatioMedianFilteredPt( row, col );
			double delta_Pr_at_point = 0;
			double max_delta_Pr_at_point = 0;

			if( row > 0 && col < _cols-1 ) // row-1, col+1
			{
				delta_Pr_at_point = fabs( Pr_at_point - *iPowerRatioMedianFilteredPt( row-1, col+1 ));
				max_delta_Pr_at_point = max_delta_Pr_at_point > delta_Pr_at_point ? max_delta_Pr_at_point : delta_Pr_at_point;
			}

			if( row > 0 ) // row-1, col
			{
				delta_Pr_at_point = fabs( Pr_at_point - *iPowerRatioMedianFilteredPt( row-1, col ));
				max_delta_Pr_at_point = max_delta_Pr_at_point > delta_Pr_at_point ? max_delta_Pr_at_point : delta_Pr_at_point;
			}

			if( row > 0 && col > 0 ) // row-1, col-1
			{
				delta_Pr_at_point = fabs( Pr_at_point - *iPowerRatioMedianFilteredPt( row-1, col-1 ));
				max_delta_Pr_at_point = max_delta_Pr_at_point > delta_Pr_at_point ? max_delta_Pr_at_point : delta_Pr_at_point;
			}

			if( row < _rows-1 && col < _cols-1 ) // row+1, col+1
			{
				delta_Pr_at_point = fabs( Pr_at_point - *iPowerRatioMedianFilteredPt( row+1, col+1 ));
				max_delta_Pr_at_point = max_delta_Pr_at_point > delta_Pr_at_point ? max_delta_Pr_at_point : delta_Pr_at_point;
			}

			if( row < _rows-1  ) // row+1, col
			{
				delta_Pr_at_point = fabs( Pr_at_point - *iPowerRatioMedianFilteredPt( row+1, col ));
				max_delta_Pr_at_point = max_delta_Pr_at_point > delta_Pr_at_point ? max_delta_Pr_at_point : delta_Pr_at_point;
			}

			if( row < _rows-1 && col > 0 ) // row+1, col-1
			{
				delta_Pr_at_point = fabs( Pr_at_point - *iPowerRatioMedianFilteredPt( row+1, col-1 ));
				max_delta_Pr_at_point = max_delta_Pr_at_point > delta_Pr_at_point ? max_delta_Pr_at_point : delta_Pr_at_point;
			}

			if( col > 0 ) // row, col-1
			{
				delta_Pr_at_point = fabs( Pr_at_point - *iPowerRatioMedianFilteredPt( row, col-1 ));
				max_delta_Pr_at_point = max_delta_Pr_at_point > delta_Pr_at_point ? max_delta_Pr_at_point : delta_Pr_at_point;
			}

			if( col < _cols-1 ) // row, col+1
			{
				delta_Pr_at_point = fabs( Pr_at_point - *iPowerRatioMedianFilteredPt( row, col+1 ));
				max_delta_Pr_at_point = max_delta_Pr_at_point > delta_Pr_at_point ? max_delta_Pr_at_point : delta_Pr_at_point;
			}

			*iMaxDeltaPrPt( row, col ) = max_delta_Pr_at_point;

			lowest_max_deltaPr = lowest_max_deltaPr > max_delta_Pr_at_point ? max_delta_Pr_at_point : lowest_max_deltaPr ;
			highest_max_deltaPr = highest_max_deltaPr < max_delta_Pr_at_point ? max_delta_Pr_at_point : highest_max_deltaPr ;
		}
	}


	CGR_LOG("CDSDBROverallModeMap::calculateMaxDeltaPrMap() exiting",DIAGNOSTIC_CGR_LOG)

	return rval;
}


CDSDBROverallModeMap::rcode
CDSDBROverallModeMap::findSMMiddleLines()
{
	CGR_LOG("CDSDBROverallModeMap::findSMMiddleLines() entered",DIAGNOSTIC_CGR_LOG)

	rcode rval = rcode::ok;

	// NOTE: Whereas the supermode boundary lines can effectively be
	// a line zigzagging all over the place up/down, left/right,
	// more restrictions must exist on the middle line.
	// Each point must map directly onto the Ir ramp.
	// Therefore, the size of the middle line is the same as Ir ramp
	// unless the middle line hits the top|bottom instead of lhs|rhs

	int min_dist_of_middle_line_from_boundary = CG_REG->get_DSDBR01_OMBD_min_points_width_of_sm() / 2;

	for( short sm_number = 0 ; sm_number < (short)(_supermodes.size()) ; sm_number++ )
	{
		// clear any middle line data
		_supermodes[sm_number]._middle_line.clear();
		bool middle_point_found_at_col_zero = false;
		bool middle_point_found_at_last_col = false;

		// if right-most column covered is not last column in map
		//   find midpoint on rhs-top border
		//   plot line from right-most point found to midpoint
		//   add points along line to middle line
		// if left-most column covered is not first column in map
		//   find midpoint on lhs_bottom border
		//   plot line from left-most point found to lhs_bottom border
		//   add points along line to middle line

		for( long i = 0; i < _cols ; i++ )
		{
			// for each column

			long highest_row_at_i_on_lower_line = -1;
			long index_of_highest_row_at_i_on_lower_line = -1;

			// find highest point on lower line at column i

			for( long j = 0; j < (long)(_supermodes[sm_number]._lower_line._points.size()) ; j++ )
			{
				if( _supermodes[sm_number]._lower_line._points[j].col == i
				 && _supermodes[sm_number]._lower_line._points[j].row > highest_row_at_i_on_lower_line )
				{
					index_of_highest_row_at_i_on_lower_line = j;
					highest_row_at_i_on_lower_line = _supermodes[sm_number]._lower_line._points[j].row;
				}
			}

			long lowest_row_at_i_on_upper_line = _rows;
			long index_of_lowest_row_at_i_on_upper_line = -1;

			// find lowest point on upper line at column i

			for( long j = 0; j < (long)(_supermodes[sm_number]._upper_line._points.size()) ; j++ )
			{
				if( _supermodes[sm_number]._upper_line._points[j].col == i
				 && _supermodes[sm_number]._upper_line._points[j].row < lowest_row_at_i_on_upper_line )
				{
					index_of_lowest_row_at_i_on_upper_line = j;
					lowest_row_at_i_on_upper_line = _supermodes[sm_number]._upper_line._points[j].row;
				}
			}

			if( index_of_highest_row_at_i_on_lower_line != -1
			 && index_of_lowest_row_at_i_on_upper_line != -1 )
			{
				if( i == 0 ) middle_point_found_at_col_zero = true;
				if( i == _cols-1 ) middle_point_found_at_last_col = true;

				// if both found, check minimum separation distance

				if( lowest_row_at_i_on_upper_line
				  - highest_row_at_i_on_lower_line
				  > (long)(CG_REG->get_DSDBR01_OMBD_min_points_width_of_sm()) )
				{
					// find vertical midpoint

					long row_of_midpoint = highest_row_at_i_on_lower_line +
						(long)((lowest_row_at_i_on_upper_line - highest_row_at_i_on_lower_line)/2);

					//     add to middle line
					_supermodes[sm_number]._middle_line._points.push_back(
						convertRowCol2LinePoint(
							row_of_midpoint,
							i ) );
				}
				else
				{
					// supermode min width not met, remove all points
					_supermodes[sm_number]._middle_line.clear();

					CString log_msg("CDSDBROverallModeMap::findSMMiddleLines() ");
					log_msg.AppendFormat("supermode %d min width not met, removed all points", sm_number );
					CGR_LOG(log_msg,WARNING_CGR_LOG)

					break;
				}
			}
		}

		if( !middle_point_found_at_col_zero
		 && !(_supermodes[sm_number]._middle_line._points.empty()) )
		{
			// need to fill in the start of the middle line to the lhs-bottom border
			// find middle start point
			// calculate points on straight line fit first known middle point
			// and add to start of middle line

			std::vector<CDSDBROverallModeMapLine::point_type>::iterator lower_border_pt = _supermodes[sm_number]._lower_line._points.end();

			for( long j = 0; j < (long)(_supermodes[sm_number]._lower_line._points.size()) ; j++ )
			{
				if( (_supermodes[sm_number]._lower_line._points[j].col == 0
				  || _supermodes[sm_number]._lower_line._points[j].row == 0)
				 && lower_border_pt == _supermodes[sm_number]._lower_line._points.end() )
				{
					// found point on lhs-bottom border
					lower_border_pt = _supermodes[sm_number]._lower_line._points.begin();
					lower_border_pt += j;
					break;
				}
			}

			std::vector<CDSDBROverallModeMapLine::point_type>::iterator upper_border_pt = _supermodes[sm_number]._upper_line._points.end();

			for( long j = 0; j < (long)(_supermodes[sm_number]._upper_line._points.size()) ; j++ )
			{
				if( (_supermodes[sm_number]._upper_line._points[j].col == 0
				  || _supermodes[sm_number]._upper_line._points[j].row == 0)
				 && upper_border_pt == _supermodes[sm_number]._upper_line._points.end() )
				{
					// found point on lhs-bottom border
					upper_border_pt = _supermodes[sm_number]._upper_line._points.begin();
					upper_border_pt += j;
					break;
				}
			}

			if( lower_border_pt != _supermodes[sm_number]._lower_line._points.end()
			 && upper_border_pt != _supermodes[sm_number]._upper_line._points.end() )
			{
				// Have upper and lower border points, find midpoint

				long lower_dist_from_origin;
				if( (*lower_border_pt).col == 0 ) lower_dist_from_origin = -(*lower_border_pt).row;
				else lower_dist_from_origin = (*lower_border_pt).col;

				long upper_dist_from_origin;
				if( (*upper_border_pt).col == 0 ) upper_dist_from_origin = -(*upper_border_pt).row;
				else upper_dist_from_origin = (*upper_border_pt).col;

				long border_midpoint_row;
				long border_midpoint_col;
				long border_midpoint_dist_from_origin = lower_dist_from_origin - (lower_dist_from_origin - upper_dist_from_origin)/2;
				if( border_midpoint_dist_from_origin > 0 )
				{
					border_midpoint_row = 0;
					border_midpoint_col = border_midpoint_dist_from_origin;
				}
				else
				{
					border_midpoint_row = -border_midpoint_dist_from_origin;
					border_midpoint_col = 0;
				}

				std::vector<CDSDBROverallModeMapLine::point_type>::iterator first_middle_pt = _supermodes[sm_number]._middle_line._points.begin();
				long first_middle_pt_col = (*first_middle_pt).col;
				long first_middle_pt_row = (*first_middle_pt).row;

				double slope = ((double)(first_middle_pt_row - border_midpoint_row))/((double)(first_middle_pt_col - border_midpoint_col));

				// starting at the first existing point,
				// insert one new point in to middle line
				// for each column between the first existing point and the border point

				for( long i = first_middle_pt_col-1 ; i >= border_midpoint_col ; i-- )
				{
					double row_of_midpoint_double = (double)border_midpoint_row + slope*((double)(i-border_midpoint_col));
					long row_of_midpoint = (long)row_of_midpoint_double;
					if( row_of_midpoint_double - (double)row_of_midpoint > 0.5 ) row_of_midpoint++;
					if( row_of_midpoint < 0 ) row_of_midpoint = 0;

					// add to middle line
					_supermodes[sm_number]._middle_line._points.insert(
						_supermodes[sm_number]._middle_line._points.begin(),
						convertRowCol2LinePoint(
							row_of_midpoint,
							i ) );
				}

				// if the upper line's border point is on the lhs border,
				// and the midpoint is on the bottom border
				// then continue the middle line along the bottom border to the origin
				if( (*upper_border_pt).col == 0
				 && CG_REG->get_DSDBR01_OMBD_ml_extend_to_corner()
				 && border_midpoint_row == 0 )
				{
					for( long i = border_midpoint_col-1 ; i >= 0 ; i-- )
					{
						// add to middle line
						_supermodes[sm_number]._middle_line._points.insert(
							_supermodes[sm_number]._middle_line._points.begin(),
							convertRowCol2LinePoint(
								0,
								i ) );
					}
				}

			}

		}

		if( !middle_point_found_at_last_col
		 && !(_supermodes[sm_number]._middle_line._points.empty()) )
		{
			// need to fill in the end of the middle line to the rhs-top border
			// find middle end point
			// calculate points on straight line fit from last known middle point
			// and add to end of middle line

			std::vector<CDSDBROverallModeMapLine::point_type>::iterator lower_border_pt = _supermodes[sm_number]._lower_line._points.end();

			for( long j = 0; j < (long)(_supermodes[sm_number]._lower_line._points.size()) ; j++ )
			{
				if( (_supermodes[sm_number]._lower_line._points[j].col == _cols-1
				  || _supermodes[sm_number]._lower_line._points[j].row == _rows-1)
				 && lower_border_pt == _supermodes[sm_number]._lower_line._points.end() )
				{
					// found point on rhs-top border
					lower_border_pt = _supermodes[sm_number]._lower_line._points.begin();
					lower_border_pt += j;
					break;
				}
			}

			std::vector<CDSDBROverallModeMapLine::point_type>::iterator upper_border_pt = _supermodes[sm_number]._upper_line._points.end();

			for( long j = 0; j < (long)(_supermodes[sm_number]._upper_line._points.size()) ; j++ )
			{
				if( (_supermodes[sm_number]._upper_line._points[j].col == _cols-1
				  || _supermodes[sm_number]._upper_line._points[j].row == _rows-1)
				 && upper_border_pt == _supermodes[sm_number]._upper_line._points.end() )
				{
					// found point on rhs-top border
					upper_border_pt = _supermodes[sm_number]._upper_line._points.begin();
					upper_border_pt += j;
					break;
				}
			}

			if( lower_border_pt != _supermodes[sm_number]._lower_line._points.end()
			 && upper_border_pt != _supermodes[sm_number]._upper_line._points.end() )
			{
				// Have upper and lower border points, find midpoint

				long lower_dist_from_toprightcorner;
				if( (*lower_border_pt).col == _cols-1 ) lower_dist_from_toprightcorner = (_rows-1)-(*lower_border_pt).row;
				else lower_dist_from_toprightcorner = (*lower_border_pt).col - (_cols-1);

				long upper_dist_from_toprightcorner;
				if( (*upper_border_pt).col == _cols-1 ) upper_dist_from_toprightcorner = (_rows-1)-(*upper_border_pt).row;
				else upper_dist_from_toprightcorner = (*upper_border_pt).col - (_cols-1);

				long border_midpoint_row;
				long border_midpoint_col;
				long border_midpoint_dist_from_toprightcorner = lower_dist_from_toprightcorner - (lower_dist_from_toprightcorner - upper_dist_from_toprightcorner)/2;
				if( border_midpoint_dist_from_toprightcorner > 0 )
				{
					border_midpoint_row = (_rows-1)-border_midpoint_dist_from_toprightcorner;
					border_midpoint_col = _cols-1;
				}
				else
				{
					border_midpoint_row = _rows-1;
					border_midpoint_col = (_cols-1)+border_midpoint_dist_from_toprightcorner;
				}

				std::vector<CDSDBROverallModeMapLine::point_type>::iterator last_middle_pt = _supermodes[sm_number]._middle_line._points.end();
				last_middle_pt--;
				long last_middle_pt_col = (*last_middle_pt).col;
				long last_middle_pt_row = (*last_middle_pt).row;

				double slope = ((double)(border_midpoint_row - last_middle_pt_row))/((double)(border_midpoint_col - last_middle_pt_col));

				// starting at the end,
				// insert one new point in to middle line
				// for each column between the last existing point and the border point

				for( long i = last_middle_pt_col+1 ; i <= border_midpoint_col ; i++ )
				{
					double row_of_midpoint_double = (double)last_middle_pt_row + slope*((double)(i-last_middle_pt_col));
					long row_of_midpoint = (long)row_of_midpoint_double;
					if( row_of_midpoint_double - (double)row_of_midpoint > 0.5 ) row_of_midpoint++;
					if( row_of_midpoint > _rows-1 ) row_of_midpoint = _rows-1;

					// add to middle line
					_supermodes[sm_number]._middle_line._points.push_back(
						convertRowCol2LinePoint(
							row_of_midpoint,
							i ) );
				}

				// if the lower line's border point is on the rhs border,
				// and the midpoint is on the top border
				// then continue the middle line along the top border to the rhs border
				if( (*lower_border_pt).col == _cols-1
				 && CG_REG->get_DSDBR01_OMBD_ml_extend_to_corner()
				 && border_midpoint_row == _rows-1 )
				{
					for( long i = border_midpoint_col+1 ; i < _cols ; i++ )
					{
						// add to middle line
						_supermodes[sm_number]._middle_line._points.push_back(
							convertRowCol2LinePoint(
								_rows-1,
								i ) );
					}
				}

			}
		}


		CString log_msg("CDSDBROverallModeMap::findSMMiddleLines() ");
		log_msg.AppendFormat("found supermode %d middle line of length %d points",
			sm_number, _supermodes[sm_number]._middle_line._points.size() );
		CGR_LOG(log_msg,INFO_CGR_LOG)


		//CString middle_line_abs_filepath("C:\\CLoseGrid\\CLoseGrid4.0\\test\\testdata\\middle_line");
		//char buf[10];
		//sprintf( buf, "%d", sm_number );
		//middle_line_abs_filepath += CString( buf );
		//middle_line_abs_filepath += CString( ".txt" );

		//_supermodes[sm_number]._middle_line.writeRowColToFile(
		//	middle_line_abs_filepath,
		//	true );

		//middle_line_abs_filepath = CString("C:\\CLoseGrid\\CLoseGrid4.0\\test\\testdata\\middle_line");
		//sprintf( buf, "%d", sm_number );
		//middle_line_abs_filepath += CString( buf );
		//middle_line_abs_filepath += CString( "_currents.csv" );

		//_supermodes[sm_number]._middle_line.writeCurrentsToFile(
		//	middle_line_abs_filepath,
		//	true );
	}

	CGR_LOG("CDSDBROverallModeMap::findSMMiddleLines() exiting",DIAGNOSTIC_CGR_LOG)

	return rval;
}


CDSDBROverallModeMap::rcode
CDSDBROverallModeMap::checkSMMiddleLines()
{
	CGR_LOG("CDSDBROverallModeMap::checkSMMiddleLines() entered",DIAGNOSTIC_CGR_LOG)

	rcode rval = rcode::ok;

	// Check each middle line to ensure
	// it is in one supermode, that it's longer than a min length
	// and that it doesn't go back and forth across a change in front section pairs
	// if not, remove the supermode

	for( short sm = 0; sm < (short)(_supermodes.size()) ; sm++ )
	{

		if( (int)(_supermodes[sm]._filtered_middle_line._points.size()) 
			< CG_REG->get_DSDBR01_OMBD_ml_min_length() )
		{
			// middle line is too short
			// remove it
			_supermodes[sm].clear();

			CString log_msg("CDSDBROverallModeMap::checkSMMiddleLines() ");
			log_msg.AppendFormat("supermode %d filtered middle line of length %d points is too short, removing supermode",
				sm, _supermodes[sm]._filtered_middle_line._points.size() );
			CGR_LOG(log_msg,WARNING_CGR_LOG)

			std::vector<CDSDBRSuperMode>::iterator i_sm = _supermodes.begin();
			i_sm += sm;
			_supermodes.erase( i_sm );
			sm--;
		}
		else
		{
			// check front pair numbers don't cross back and forth
			bool crossing_back_and_forth = false;
			std::vector<int> found_front_pair_numbers;
			int last_pt_front_pair_number = 0;
			for( long i = 0 ; i < (long)(_supermodes[sm]._filtered_middle_line._points.size()) ; i++ )
			{
				int front_pair_number = _supermodes[sm]._filtered_middle_line._points[i].front_pair_number;

				if( front_pair_number != last_pt_front_pair_number )
				{
					for( int j = 0; j < (int)(found_front_pair_numbers.size()); j++ )
					{
						if( front_pair_number == found_front_pair_numbers[j] )
						{
							crossing_back_and_forth = true;
							break;
						}
					}
					if(crossing_back_and_forth)
					{
						// new front pair number has been seen earlier
						//gdm300306 'clear()' moved to after calls for row & col, was causing mem-viol exception
						long row = _supermodes[sm]._filtered_middle_line._points[i].row;
						long col = _supermodes[sm]._filtered_middle_line._points[i].col;

						CString log_msg("CDSDBROverallModeMap::checkSMMiddleLines() ");
						log_msg.AppendFormat("supermode %d front pair number %d at row=%d col=%d was visited earlier, removing supermode",
							sm, front_pair_number, row, col );
						CGR_LOG(log_msg,WARNING_CGR_LOG)
						
						//gdm300306 from above
						_supermodes[sm].clear();

						std::vector<CDSDBRSuperMode>::iterator i_sm = _supermodes.begin();
						i_sm += sm;
						_supermodes.erase( i_sm );
						sm--;

						break;
					}
					found_front_pair_numbers.push_back( front_pair_number );
				}

				last_pt_front_pair_number = front_pair_number;
			}

			if(!crossing_back_and_forth)
			{
				// get each maxDeltaPr values along middle line
				for( long i = 0 ; i < (long)(_supermodes[sm]._filtered_middle_line._points.size()) ; i++ )
				{

					long row = _supermodes[sm]._filtered_middle_line._points[i].row;
					long col = _supermodes[sm]._filtered_middle_line._points[i].col;

					double maxDeltaPr = *iMaxDeltaPrPt( row, col );

					if( maxDeltaPr > CG_REG->get_DSDBR01_OMBD_max_deltaPr_in_sm() )
					{
						// threshold exceeded
						// middle line is not in one supermode
						// remove it
						_supermodes[sm].clear();

						CString log_msg("CDSDBROverallModeMap::checkSMMiddleLines() ");
						log_msg.AppendFormat("supermode %d filtered middle line maxDeltaPr %g exceeds limit %g at row=%d col=%d, removing supermode",
							sm, maxDeltaPr, CG_REG->get_DSDBR01_OMBD_max_deltaPr_in_sm(), row, col );
						CGR_LOG(log_msg,WARNING_CGR_LOG)

						std::vector<CDSDBRSuperMode>::iterator i_sm = _supermodes.begin();
						i_sm += sm;
						_supermodes.erase( i_sm );
						sm--;

						break;
					}
				}
			}
		}
	}

	CGR_LOG("CDSDBROverallModeMap::checkSMMiddleLines() exiting",DIAGNOSTIC_CGR_LOG)

	return rval;
}

CDSDBROverallModeMap::rcode
CDSDBROverallModeMap::filterSMMiddleLines()
{
	CGR_LOG("CDSDBROverallModeMap::filterSMMiddleLines() entered",DIAGNOSTIC_CGR_LOG)

	rcode rval = rcode::ok;

	long filter_n = (long)(CG_REG->get_DSDBR01_OMBD_ml_moving_ave_filter_n());

	for( short sm_number = 0 ; sm_number < (short)(_supermodes.size()) ; sm_number++ )
	{

		// Perform averaging in the If direction only


		for( long i = 0; i < (long)(_supermodes[sm_number]._middle_line._points.size()) ; i++ )
		{
			long window_size = 0;
			long sum_of_rows_of_windowed_points = 0;
			for( long j = i - filter_n ; j <= i + filter_n ; j++ )
			{
				if( j >=0 && j < (long)(_supermodes[sm_number]._middle_line._points.size()) )
				{
					// sum rows in window, without exceeding start or end
					window_size++;
					sum_of_rows_of_windowed_points += _supermodes[sm_number]._middle_line._points[j].row;
				}
			}

			// the structure CDSDBROverallModeMapLine::point_type
			// contains both the row, col position on a map and
			// the currents at that point.
			// For the average, we need to interpolate the If current
			// while finding the nearest row (just so it can be plotted on a map)


			CDSDBROverallModeMapLine::point_type averaged_pt
				= interpolateLinePoint(
					(double)sum_of_rows_of_windowed_points / (double)window_size,
					(double)(_supermodes[sm_number]._middle_line._points[i].col) );

			// Add averaged point
			_supermodes[sm_number]._filtered_middle_line._points.push_back( averaged_pt );
		}


		CString log_msg("CDSDBROverallModeMap::findSMMiddleLines() ");
		log_msg.AppendFormat("supermode %d filtered middle line of length %d points",
			sm_number, _supermodes[sm_number]._filtered_middle_line._points.size() );
		CGR_LOG(log_msg,INFO_CGR_LOG)

	}

	CGR_LOG("CDSDBROverallModeMap::filterSMMiddleLines() exiting",DIAGNOSTIC_CGR_LOG)

	return rval;
}

CDSDBROverallModeMapLine::point_type
CDSDBROverallModeMap::interpolateLinePoint(
	double row,
	double col )
{
	CDSDBROverallModeMapLine::point_type point_on_map;

	// find nearest row
	point_on_map.row = (long)row;
	if( row - (double)((long)row) > 0.5 ) point_on_map.row++;

	// find nearest col
	point_on_map.col = (long)col;
	if( col - (double)((long)col) > 0.5 ) point_on_map.col++;

	if( col > (double)((long)col) )
	{
		// interpolate Ir linearly
		double Ir_a = _vector_Ir._currents[(long)col];
		double Ir_b = _vector_Ir._currents[((long)col)+1];

		double Ir_fraction = col - (double)((long)col);

		point_on_map.I_rear = Ir_a + Ir_fraction*( Ir_b - Ir_a );
	}
	else
	{
		// no need to interpolate
		point_on_map.I_rear = _vector_Ir._currents[(long)col];
	}

	point_on_map.I_front_1 = 0;
	point_on_map.I_front_2 = 0;
	point_on_map.I_front_3 = 0;
	point_on_map.I_front_4 = 0;
	point_on_map.I_front_5 = 0;
	point_on_map.I_front_6 = 0;
	point_on_map.I_front_7 = 0;
	point_on_map.I_front_8 = 0;

	// use the nearest point to determine which map to use
	point_on_map.front_pair_number = _vector_If.pair_number( point_on_map.row ); // 1 + (short)(point_on_map.row / _vector_If._map.size());

	short index_of_nearest = (short)(_vector_If.index_in_submap( point_on_map.row )); //(short)(point_on_map.row % _vector_If._map.size());

	// find out how far the interpolated point is from the nearest point
	double If_fraction = row - (double)(point_on_map.row);
	long top_row = _vector_If._submap_length[ point_on_map.front_pair_number-1 ];

	if( ( index_of_nearest == 0 && If_fraction < 0 )
	 || ( index_of_nearest == (short)top_row && If_fraction > 0 ) )
	{
		// cannot interpolate between maps!
		// use nearest point for currents
		If_fraction = 0;
	}

	double If_value;

	if( If_fraction != 0 )
	{
		// need to interpolate
		short index_of_nextpt;
		if( If_fraction > 0 ) index_of_nextpt = index_of_nearest+1;
		else
		{
			index_of_nextpt = index_of_nearest-1;
			If_fraction = -If_fraction; // ensure positive fraction
		}

		// interpolate If linearly
		double If_a = _vector_If.getNonConstantCurrent( point_on_map.front_pair_number, index_of_nearest );
		double If_b = _vector_If.getNonConstantCurrent( point_on_map.front_pair_number, index_of_nextpt );
		If_value = If_a + If_fraction*( If_b - If_a );
	}
	else
	{
		// no need to interpolate
		If_value = _vector_If.getNonConstantCurrent( point_on_map.front_pair_number, index_of_nearest );
	}

	switch( point_on_map.front_pair_number )
	{
	case 1:
		point_on_map.I_front_1 = _vector_If.getConstantCurrent( point_on_map.front_pair_number, index_of_nearest );
		point_on_map.I_front_2 = If_value;
		break;
	case 2:
		point_on_map.I_front_2 = _vector_If.getConstantCurrent( point_on_map.front_pair_number, index_of_nearest );
		point_on_map.I_front_3 = If_value;
		break;
	case 3:
		point_on_map.I_front_3 = _vector_If.getConstantCurrent( point_on_map.front_pair_number, index_of_nearest );
		point_on_map.I_front_4 = If_value;
		break;
	case 4:
		point_on_map.I_front_4 = _vector_If.getConstantCurrent( point_on_map.front_pair_number, index_of_nearest );
		point_on_map.I_front_5 = If_value;
		break;
	case 5:
		point_on_map.I_front_5 = _vector_If.getConstantCurrent( point_on_map.front_pair_number, index_of_nearest );
		point_on_map.I_front_6 = If_value;
		break;
	case 6:
		point_on_map.I_front_6 = _vector_If.getConstantCurrent( point_on_map.front_pair_number, index_of_nearest );
		point_on_map.I_front_7 = If_value;
		break;
	case 7:
		point_on_map.I_front_7 = _vector_If.getConstantCurrent( point_on_map.front_pair_number, index_of_nearest );
		point_on_map.I_front_8 = If_value;
		break;
	}

	return point_on_map;
}

CDSDBROverallModeMapLine::point_type
CDSDBROverallModeMap::convertRowCol2LinePoint(
	long row,
	long col )
{
	CDSDBROverallModeMapLine::point_type point_on_map;

	point_on_map.row = row;
	point_on_map.col = col;

	point_on_map.I_rear = _vector_Ir._currents[col];

	point_on_map.I_front_1 = 0;
	point_on_map.I_front_2 = 0;
	point_on_map.I_front_3 = 0;
	point_on_map.I_front_4 = 0;
	point_on_map.I_front_5 = 0;
	point_on_map.I_front_6 = 0;
	point_on_map.I_front_7 = 0;
	point_on_map.I_front_8 = 0;

	point_on_map.front_pair_number = _vector_If.pair_number(row); // 1 + (short)(row / _vector_If._map.size());
	short index = (short)(_vector_If.index_in_submap(row)); // (short)(row % _vector_If._map.size());

	switch( point_on_map.front_pair_number )
	{
	case 1:
		point_on_map.I_front_1 = _vector_If.getConstantCurrent( point_on_map.front_pair_number, index );
		point_on_map.I_front_2 = _vector_If.getNonConstantCurrent( point_on_map.front_pair_number, index );
		break;
	case 2:
		point_on_map.I_front_2 = _vector_If.getConstantCurrent( point_on_map.front_pair_number, index );
		point_on_map.I_front_3 = _vector_If.getNonConstantCurrent( point_on_map.front_pair_number, index );
		break;
	case 3:
		point_on_map.I_front_3 = _vector_If.getConstantCurrent( point_on_map.front_pair_number, index );
		point_on_map.I_front_4 = _vector_If.getNonConstantCurrent( point_on_map.front_pair_number, index );
		break;
	case 4:
		point_on_map.I_front_4 = _vector_If.getConstantCurrent( point_on_map.front_pair_number, index );
		point_on_map.I_front_5 = _vector_If.getNonConstantCurrent( point_on_map.front_pair_number, index );
		break;
	case 5:
		point_on_map.I_front_5 = _vector_If.getConstantCurrent( point_on_map.front_pair_number, index );
		point_on_map.I_front_6 = _vector_If.getNonConstantCurrent( point_on_map.front_pair_number, index );
		break;
	case 6:
		point_on_map.I_front_6 = _vector_If.getConstantCurrent( point_on_map.front_pair_number, index );
		point_on_map.I_front_7 = _vector_If.getNonConstantCurrent( point_on_map.front_pair_number, index );
		break;
	case 7:
		point_on_map.I_front_7 = _vector_If.getConstantCurrent( point_on_map.front_pair_number, index );
		point_on_map.I_front_8 = _vector_If.getNonConstantCurrent( point_on_map.front_pair_number, index );
		break;
	}

	return point_on_map;
}



std::vector<CDSDBROverallModeMap::boundary_point_type>::iterator
CDSDBROverallModeMap::findInBoundaryLine(
	std::vector<boundary_point_type> &boundary_line,
	boundary_point_type boundary_point )
{
	std::vector<boundary_point_type>::iterator i;

	for( i = boundary_line.begin() ; i != boundary_line.end() ; i++ )
	{
		if( boundary_point.row == (*i).row
		 && boundary_point.col == (*i).col )
		{
			break;
		}
	}

	return i;
}



CDSDBROverallModeMap::rcode
CDSDBROverallModeMap::findBoundaryLineOnMaxDeltaPrMap(
	std::vector<boundary_point_type> &boundary_line,
	double threshold_MaxDeltaPr,
	direction_type input_start_direction,
	bool search_anticlockwise )
{
	rcode rval = rcode::ok;
	bool found_rhs_top_border = false;
	std::vector<boundary_point_type> excluded_pts;


	// check start point has been inserted into boundary line
	if( boundary_line.size() == 1 )
	{
		direction_type start_direction = input_start_direction;

		boundary_point_type boundary_point;
		boundary_point.row = boundary_line[0].row;
		boundary_point.col = boundary_line[0].col;
		boundary_point.found_in_direction = START_PT; // start pt

		boundary_point_type next_pt;
		double next_pt_value;
		next_pt.row = boundary_line[0].row;
		next_pt.col = boundary_line[0].col;
		next_pt.found_in_direction = START_PT; // start pt

		for( long i = 0 ; i < 2000 ; i++ )
		{

			rcode rval_nextBdPt = findNextBoundaryPointOnMaxDeltaPrMap(
				start_direction,
				search_anticlockwise,
				boundary_point,
				next_pt,
				next_pt_value,
				excluded_pts,
				threshold_MaxDeltaPr );

			if( rval_nextBdPt != rcode::next_pt_not_found )
			{
				boundary_point.row = next_pt.row;
				boundary_point.col = next_pt.col;
				boundary_point.found_in_direction = next_pt.found_in_direction;
				std::vector<boundary_point_type>::iterator found_in_line_at_i
					= findInBoundaryLine( boundary_line, boundary_point );
				if( found_in_line_at_i != boundary_line.end() )
				{
					// point already exists in line,
					// what direction was next found in?
					if( found_in_line_at_i+1 != boundary_line.end() )
						start_direction = (*(found_in_line_at_i+1)).found_in_direction;

					// record all points after it as excluded
					excluded_pts.insert( excluded_pts.begin(), found_in_line_at_i+1, boundary_line.end() );
					//// also exclude the last point left as valid
					//excluded_pts.insert( excluded_pts.begin(), boundary_line.back() );

					// remove any points after it from the line 
					boundary_line.erase( found_in_line_at_i+1, boundary_line.end() );

					// start at next new direction
					if( search_anticlockwise ) start_direction = (direction_type)((short)(start_direction) + 1); // +1 for anticlockwise
					else start_direction = (direction_type)((short)(start_direction) - 1); // -1 for clockwise
					if( start_direction > direction_type::NE ) start_direction = direction_type::N;
					if( start_direction < direction_type::N ) start_direction = direction_type::NE;

					if( start_direction == input_start_direction )
					{
						// then we're gone full circle => stop looking for points
						break;
					}

					// find next point from last point in line with a new direction 
					boundary_point.row = boundary_line.back().row;
					boundary_point.col = boundary_line.back().col;
					boundary_point.found_in_direction = boundary_line.back().found_in_direction;
				}
				else
				{
					// insert new point into boundary line
					boundary_line.push_back( boundary_point );

					// set value in map to filtered value
					//*iPowerRatioPt( boundary_point.row, boundary_point.col ) = next_pt_filtered_value;

					// mark new point as excluded
					//excluded_pts.insert( excluded_pts.begin(), boundary_point );

					start_direction = opposite( boundary_point.found_in_direction );

					if( start_direction == opposite( boundary_point.found_in_direction ) )
					{ // don't start by going back to last point, around on one
						if( search_anticlockwise ) start_direction = (direction_type)((short)(start_direction) + 1); // +1 for anticlockwise
						else start_direction = (direction_type)((short)(start_direction) - 1); // -1 for clockwise
						if( start_direction > direction_type::NE ) start_direction = direction_type::N;
						if( start_direction < direction_type::N ) start_direction = direction_type::NE;
					}

					if( boundary_point.row == _rows-1 || boundary_point.col == _cols-1 )
					{ // have reached end of boundary!
						found_rhs_top_border = true;
						break;
					}

					if( boundary_point.row == 0 || boundary_point.col == 0 )
					{ // have reached wrong border
						found_rhs_top_border = false;
						break;
					}

				}

			}

		}
	}
	else
	{
		rval = rcode::no_start_point;
		CGR_LOG("CDSDBROverallModeMap::findBoundaryLineOnMaxDeltaPrMap error: no_start_point",ERROR_CGR_LOG)
	}

	if( !found_rhs_top_border )
	{
		rval = rcode::rhs_top_border_not_reached;
		CGR_LOG("CDSDBROverallModeMap::findBoundaryLineOnMaxDeltaPrMap error: rhs_top_border_not_reached",ERROR_CGR_LOG)
	}


	//printf("\ni row col found_in_direction");
	//for( long i = 0 ; i < (long)(boundary_line.size()) ; i++ )
	//{
	//	printf("\n%d %d %d %s", i, boundary_line[i].row, boundary_line[i].col, getCStringForDirection( boundary_line[i].found_in_direction ).GetBuffer() );
	//}

	return rval;
}



CDSDBROverallModeMap::rcode
CDSDBROverallModeMap::findNextBoundaryPointOnMaxDeltaPrMap(
		direction_type start_direction,
		bool find_new_point_anticlockwise,
		boundary_point_type &current_pt,
		boundary_point_type &next_pt,
		double &next_pt_MaxDeltaPr_value,
		std::vector<boundary_point_type> &excluded_pts,
		double threshold_MaxDeltaPr )
{
	rcode rval = rcode::next_pt_not_found;

	double pt_MaxDeltaPr_value = *iMaxDeltaPrPt( current_pt.row, current_pt.col );

	short increment;
	if( find_new_point_anticlockwise )
		increment = 1;
	else // find new point clockwise
		increment = -1;


	direction_type i_direction = start_direction;
	do
	{
		long adj_pt_row;
		long adj_pt_col; 
		double adj_pt_MaxDeltaPr_value;

		rcode rval_calAdjPt = getMaxDeltaPrValueOfAdjacentPt(
			current_pt.row,
			current_pt.col, 
			i_direction,
			adj_pt_row,
			adj_pt_col, 
			adj_pt_MaxDeltaPr_value );

		if( rval_calAdjPt == rcode::ok )
		{
			// check if adjacent point has been excluded
			boundary_point_type check_adj_pt;
			check_adj_pt.row = adj_pt_row;
			check_adj_pt.col = adj_pt_col;
			check_adj_pt.found_in_direction = i_direction;
			std::vector<boundary_point_type>::iterator found_in_excluded_pts_at_i
					= findInBoundaryLine( excluded_pts, check_adj_pt );

			if( found_in_excluded_pts_at_i == excluded_pts.end() ) // not excluded 
			{

				if( adj_pt_MaxDeltaPr_value > threshold_MaxDeltaPr )
				{
					// found next point
					next_pt.row = adj_pt_row;
					next_pt.col = adj_pt_col;
					next_pt.found_in_direction = i_direction;
					next_pt_MaxDeltaPr_value = adj_pt_MaxDeltaPr_value;
					rval = rcode::ok;
					break;
				}
			}
			else
			{ // adjacent point marked as excluded, ignore
			}
		}
		else
		{ // must be outside_map_dimensions, ignore
		}

		i_direction = (direction_type)((short)(i_direction) + increment);
		if( i_direction > direction_type::NE ) i_direction = direction_type::N;
		if( i_direction < direction_type::N ) i_direction = direction_type::NE;
	}
	while( i_direction != start_direction );

	return rval;
}


CDSDBROverallModeMap::rcode
CDSDBROverallModeMap::writePRatioContinuityToFile(
	CString abs_filepath,
	bool overwrite )
{
	rcode rval = rcode::ok;
    long row_count = 0;
    long col_count = 0;

	if( _supermodes.empty() )
	{
		rval = rcode::no_data;
		CGR_LOG("CDSDBROverallModeMap::writePRatioContinuityToFile error: no_data",ERROR_CGR_LOG)
	}
	else
	{
		CFileStatus status;

		// Check if file already exists and overwrite not specified

		if( CFile::GetStatus( abs_filepath, status ) && overwrite == false )
		{
			rval = rcode::file_already_exists;
			CGR_LOG("CDSDBROverallModeMap::writePRatioContinuityToFile error: file_already_exists",ERROR_CGR_LOG)
		}
		else
		{

			// Ensure the directory exists
			int index_of_last_dirslash = abs_filepath.ReverseFind('\\');
			if( index_of_last_dirslash == -1 )
			{
				// '\\' not found => not an absolute path
				rval = rcode::could_not_open_file;
				CGR_LOG("CDSDBROverallModeMap::writePRatioContinuityToFile error: could_not_open_file",ERROR_CGR_LOG)
			}
			else
			{
				CString dir_name = abs_filepath;
				dir_name.Truncate(index_of_last_dirslash);

				int i = 0;
				CString token = dir_name.Tokenize( "\\/", i );
				CString new_dir = "";
				while ( token != "" )
				{
					if( new_dir.GetLength() == 0 )
					{
						new_dir = token;
					}
					else
					{
						new_dir = new_dir + "\\" + token;
					}
					CFileStatus status;
					//printf("\n %s  = %d", new_dir, CFile::GetStatus( new_dir, status ) );
					if( new_dir.GetLength() > 2 // skip drive name
					&& !CFile::GetStatus( new_dir, status ) )
					{ // directory does not exists => make it
						_mkdir( new_dir );
					}
					else
					{ // directory exists
					}
					//printf("\n %s  = %d", new_dir, CFile::GetStatus( new_dir, status ) );
		            
					token = dir_name.Tokenize( "\\/", i );
				}

				try
				{
					std::ofstream file_stream( abs_filepath );

					if( file_stream )
					{
						// First write header row
						file_stream << "SM#";
						file_stream << COMMA_CHAR;
						file_stream << "PowerRatio";
						file_stream << std::endl;

						for( short sm = 0; sm < (short)(_supermodes.size()) ; sm++ )
						{
							std::vector<CDSDBROverallModeMapLine::point_type> *p_filtered_middle_points = &(_supermodes[sm]._filtered_middle_line._points);

							if( p_filtered_middle_points->empty() )
							{
								rval = rcode::no_data;
								CGR_LOG("CDSDBROverallModeMap::writePRatioContinuityToFile error: filtered middle line is empty",ERROR_CGR_LOG)
							}
							else
							{
								for( long i = 0 ; i < (long)(p_filtered_middle_points->size()) ; i++ )
								{
									long row_i = ((*p_filtered_middle_points)[i]).row;
									long col_i = ((*p_filtered_middle_points)[i]).col;
									double power_ratio = *iPowerRatioPt( row_i, col_i );

									file_stream << sm;
									file_stream << COMMA_CHAR;
									file_stream << power_ratio;
									file_stream << std::endl;
								}
							}
						}
					}
					else
					{
						rval = rcode::could_not_open_file;
						CGR_LOG("CDSDBROverallModeMap::writePRatioContinuityToFile error: could_not_open_file",ERROR_CGR_LOG)
					}
				}
				catch(...)
				{
					rval = rcode::could_not_open_file;
					CGR_LOG("CDSDBROverallModeMap::writePRatioContinuityToFile error: could_not_open_file",ERROR_CGR_LOG)
				}
			}
		}
	}

	return rval;
}


CDSDBROverallModeMap::rcode
CDSDBROverallModeMap::writeSMLinesToFile(
	CString abs_filepath,
	bool overwrite )
{
	rcode rval = rcode::ok;
    long row_count = 0;
    long col_count = 0;

	if( _supermodes.empty() )
	{
		CGR_LOG("CDSDBROverallModeMap::writeSMLinesToFile error: no_data",ERROR_CGR_LOG)
		rval = rcode::no_data;
	}
	else
	{

		CFileStatus status;

		// Check if file already exists and overwrite not specified

		if( CFile::GetStatus( abs_filepath, status ) && overwrite == false )
		{
			rval = rcode::file_already_exists;
			CGR_LOG("CDSDBROverallModeMap::writeSMLinesToFile error: file_already_exists",ERROR_CGR_LOG)
		}
		else
		{

			// Ensure the directory exists
			int index_of_last_dirslash = abs_filepath.ReverseFind('\\');
			if( index_of_last_dirslash == -1 )
			{
				// '\\' not found => not an absolute path
				rval = rcode::could_not_open_file;
				CGR_LOG("CDSDBROverallModeMap::writeSMLinesToFile error: could_not_open_file",ERROR_CGR_LOG)
			}
			else
			{
				CString dir_name = abs_filepath;
				dir_name.Truncate(index_of_last_dirslash);

				int i = 0;
				CString token = dir_name.Tokenize( "\\/", i );
				CString new_dir = "";
				while ( token != "" )
				{
					if( new_dir.GetLength() == 0 )
					{
						new_dir = token;
					}
					else
					{
						new_dir = new_dir + "\\" + token;
					}
					CFileStatus status;
					//printf("\n %s  = %d", new_dir, CFile::GetStatus( new_dir, status ) );
					if( new_dir.GetLength() > 2 // skip drive name
					&& !CFile::GetStatus( new_dir, status ) )
					{ // directory does not exists => make it
						_mkdir( new_dir );
					}
					else
					{ // directory exists
					}
					//printf("\n %s  = %d", new_dir, CFile::GetStatus( new_dir, status ) );
		            
					token = dir_name.Tokenize( "\\/", i );
				}

				try
				{
					std::ofstream file_stream( abs_filepath );

					if( file_stream )
					{
						// First write header row
						file_stream << "SM#";
						file_stream << COMMA_CHAR;
						file_stream << "LineType";
						file_stream << COMMA_CHAR;
						file_stream << "Row";
						file_stream << COMMA_CHAR;
						file_stream << "Col";
						file_stream << std::endl;

						for( short sm = 0; sm < (short)(_supermodes.size()) ; sm++ )
						{
							std::vector<CDSDBROverallModeMapLine::point_type> *p_lower_points = &(_supermodes[sm]._lower_line._points);
							std::vector<CDSDBROverallModeMapLine::point_type> *p_upper_points = &(_supermodes[sm]._upper_line._points);
							std::vector<CDSDBROverallModeMapLine::point_type> *p_filtered_middle_points = &(_supermodes[sm]._filtered_middle_line._points);

							if( p_lower_points->empty()
							|| p_upper_points->empty()
							|| p_filtered_middle_points->empty() )
							{
								rval = rcode::no_data;
								CGR_LOG("CDSDBROverallModeMap::writeSMLinesToFile error: filtered middle line empty",ERROR_CGR_LOG)
							}
							else
							{
								for( long i = 0 ; i < (long)(p_lower_points->size()) ; i++ )
								{
									file_stream << sm;
									file_stream << COMMA_CHAR;
									file_stream << "Lower";
									file_stream << COMMA_CHAR;
									file_stream << ((*p_lower_points)[i]).row;
									file_stream << COMMA_CHAR;
									file_stream << ((*p_lower_points)[i]).col;
									file_stream << std::endl;
								}

								for( long i = 0 ; i < (long)(p_upper_points->size()) ; i++ )
								{
									file_stream << sm;
									file_stream << COMMA_CHAR;
									file_stream << "Upper";
									file_stream << COMMA_CHAR;
									file_stream << ((*p_upper_points)[i]).row;
									file_stream << COMMA_CHAR;
									file_stream << ((*p_upper_points)[i]).col;
									file_stream << std::endl;
								}

								for( long i = 0 ; i < (long)(p_filtered_middle_points->size()) ; i++ )
								{
									file_stream << sm;
									file_stream << COMMA_CHAR;
									file_stream << "Middle";
									file_stream << COMMA_CHAR;
									file_stream << ((*p_filtered_middle_points)[i]).row;
									file_stream << COMMA_CHAR;
									file_stream << ((*p_filtered_middle_points)[i]).col;
									file_stream << std::endl;
								}
							}
						}
					}
					else
					{
						rval = rcode::could_not_open_file;
						CGR_LOG("CDSDBROverallModeMap::writeSMLinesToFile error: could_not_open_file",ERROR_CGR_LOG)
					}
				}
				catch(...)
				{
					rval = rcode::could_not_open_file;
					CGR_LOG("CDSDBROverallModeMap::writeSMLinesToFile error: could_not_open_file",ERROR_CGR_LOG)
				}
			}
		}
	}

	return rval;
}


CDSDBROverallModeMap::rcode
CDSDBROverallModeMap::writeLMMiddleFreqCurrentsToFile(
	CString abs_filepath,
	bool overwrite )
{
	rcode rval = rcode::ok;
    long row_count = 0;
    long col_count = 0;

	if( _supermodes.empty() )
	{
		CGR_LOG("CDSDBROverallModeMap::writeLMMiddleFreqCurrentsToFile error: no_data",ERROR_CGR_LOG)
		rval = rcode::no_data;
	}
	else
	{

		CFileStatus status;

		// Check if file already exists and overwrite not specified

		if( CFile::GetStatus( abs_filepath, status ) && overwrite == false )
		{
			rval = rcode::file_already_exists;
			CGR_LOG("CDSDBROverallModeMap::writeLMMiddleFreqCurrentsToFile error: file_already_exists",ERROR_CGR_LOG)
		}
		else
		{

			// Ensure the directory exists
			int index_of_last_dirslash = abs_filepath.ReverseFind('\\');
			if( index_of_last_dirslash == -1 )
			{
				// '\\' not found => not an absolute path
				rval = rcode::could_not_open_file;
				CGR_LOG("CDSDBROverallModeMap::writeLMMiddleFreqCurrentsToFile error: could_not_open_file",ERROR_CGR_LOG)
			}
			else
			{
				CString dir_name = abs_filepath;
				dir_name.Truncate(index_of_last_dirslash);

				int i = 0;
				CString token = dir_name.Tokenize( "\\/", i );
				CString new_dir = "";
				while ( token != "" )
				{
					if( new_dir.GetLength() == 0 )
					{
						new_dir = token;
					}
					else
					{
						new_dir = new_dir + "\\" + token;
					}
					CFileStatus status;
					//printf("\n %s  = %d", new_dir, CFile::GetStatus( new_dir, status ) );
					if( new_dir.GetLength() > 2 // skip drive name
					&& !CFile::GetStatus( new_dir, status ) )
					{ // directory does not exists => make it
						_mkdir( new_dir );
					}
					else
					{ // directory exists
					}
					//printf("\n %s  = %d", new_dir, CFile::GetStatus( new_dir, status ) );
		            
					token = dir_name.Tokenize( "\\/", i );
				}

				try
				{
					std::ofstream file_stream( abs_filepath );

					if( file_stream )
					{
						// First write header row
						file_stream << "SM#";
						file_stream << COMMA_CHAR;
						file_stream << "LM#";
						file_stream << COMMA_CHAR;
						file_stream << "Index";
						file_stream << COMMA_CHAR;
						file_stream << "Gain Current [mA]";
						file_stream << COMMA_CHAR;
						file_stream << "SOA Current [mA]";
						file_stream << COMMA_CHAR;
						file_stream << "Rear Current [mA]";
						file_stream << COMMA_CHAR;
						file_stream << "Front Pair Number";
						file_stream << COMMA_CHAR;
						file_stream << "Constant Front Current [mA]";
						file_stream << COMMA_CHAR;
						file_stream << "Non-constant Front Current [mA]";
						file_stream << COMMA_CHAR;
						file_stream << "Phase Current [mA]";
						file_stream << std::endl;

						for( short sm = 0; sm < (short)(_supermodes.size()) ; sm++ )
						{
							std::vector<CDSDBRSuperMode>::iterator i_sm = _supermodes.begin();
							i_sm += sm;

							for( short lm = 0; lm < (short)(i_sm->_lm_middle_lines_of_frequency.size()) ; lm++ )
							{
								std::vector<CDSDBRSupermodeMapLine>::iterator i_lm = i_sm->_lm_middle_lines_of_frequency.begin();
								i_lm += lm;

								for( short index = 0 ; index < (short)(i_lm->_points.size()) ; index++ )
								{
									file_stream << sm;
									file_stream << COMMA_CHAR;
									file_stream << lm;
									file_stream << COMMA_CHAR;
									file_stream << index;
									file_stream << COMMA_CHAR;

									file_stream << i_sm->_I_gain;
									file_stream << COMMA_CHAR;
									file_stream << i_sm->_I_soa;
									file_stream << COMMA_CHAR;
									file_stream << i_lm->_points[index].I_rear;
									file_stream << COMMA_CHAR;
									file_stream << i_lm->_points[index].front_pair_number;
									file_stream << COMMA_CHAR;

									switch( i_lm->_points[index].front_pair_number )
									{
									case 1:
										file_stream << i_lm->_points[index].I_front_1;
										file_stream << COMMA_CHAR;
										file_stream << i_lm->_points[index].I_front_2;
										break;
									case 2:
										file_stream << i_lm->_points[index].I_front_2;
										file_stream << COMMA_CHAR;
										file_stream << i_lm->_points[index].I_front_3;
										break;
									case 3:
										file_stream << i_lm->_points[index].I_front_3;
										file_stream << COMMA_CHAR;
										file_stream << i_lm->_points[index].I_front_4;
										break;
									case 4:
										file_stream << i_lm->_points[index].I_front_4;
										file_stream << COMMA_CHAR;
										file_stream << i_lm->_points[index].I_front_5;
										break;
									case 5:
										file_stream << i_lm->_points[index].I_front_5;
										file_stream << COMMA_CHAR;
										file_stream << i_lm->_points[index].I_front_6;
										break;
									case 6:
										file_stream << i_lm->_points[index].I_front_6;
										file_stream << COMMA_CHAR;
										file_stream << i_lm->_points[index].I_front_7;
										break;
									case 7:
										file_stream << i_lm->_points[index].I_front_7;
										file_stream << COMMA_CHAR;
										file_stream << i_lm->_points[index].I_front_8;
										break;
									}

									file_stream << COMMA_CHAR;
									file_stream << i_lm->_points[index].I_phase;
									file_stream << std::endl;
								}
							}
						}

					}
					else
					{
						rval = rcode::could_not_open_file;
						CGR_LOG("CDSDBROverallModeMap::writeLMMiddleFreqCurrentsToFile error: could_not_open_file",ERROR_CGR_LOG)
					}
				}
				catch(...)
				{
					rval = rcode::could_not_open_file;
					CGR_LOG("CDSDBROverallModeMap::writeLMMiddleFreqCurrentsToFile error: could_not_open_file",ERROR_CGR_LOG)
				}
			}
		}
	}

	return rval;
}



CDSDBROverallModeMap::rcode
CDSDBROverallModeMap::readBoundariesFromFile()
{
	CGR_LOG("CDSDBROverallModeMap::readBoundariesFromFile() entered",DIAGNOSTIC_CGR_LOG)

	rcode rval = rcode::ok;
	std::vector<CString> lower_filepaths;
	std::vector<CString> upper_filepaths;

	CString results_dir = _results_dir_CString;
	if( results_dir.Right(1) != _T("\\") )
		results_dir += _T("\\");

	// build a string with wildcards
	CString lower_strWildcard = results_dir + _T("lower_boundary_line_*.txt");

	// start working for files
	CFileFind lower_finder;
	BOOL bWorking = lower_finder.FindFile(lower_strWildcard);

	while (bWorking)
	{
		bWorking = lower_finder.FindNextFile();
		CString str = lower_finder.GetFilePath();
		lower_filepaths.push_back( str );
	}

	lower_finder.Close();

	// build a string with wildcards
	CString upper_strWildcard = results_dir + _T("upper_boundary_line_*.txt");

	// start working for files
	CFileFind upper_finder;
	bWorking = upper_finder.FindFile(upper_strWildcard);

	while (bWorking)
	{
		bWorking = upper_finder.FindNextFile();
		CString str = upper_finder.GetFilePath();
		upper_filepaths.push_back( str );
	}

	upper_finder.Close();

	short sm_count = (short)(lower_filepaths.size());

	if( sm_count > 0 )
	{
		if( sm_count == (short)(upper_filepaths.size()) )
		{
			for(short i = 0; i < sm_count ; i++ )
			{
				CDSDBRSuperMode new_sm(i, _xAxisLength, _yAxisLength);
				CDSDBROverallModeMapLine lower_line;
				CDSDBROverallModeMapLine upper_line;

				CDSDBROverallModeMapLine::rcode read_lowerline_rcode;
				read_lowerline_rcode = lower_line.readRowColFromFile(lower_filepaths[i]);

				CDSDBROverallModeMapLine::rcode read_upperline_rcode;
				read_upperline_rcode = upper_line.readRowColFromFile(upper_filepaths[i]);

				if( read_lowerline_rcode == CDSDBROverallModeMapLine::rcode::ok 
				&& read_upperline_rcode == CDSDBROverallModeMapLine::rcode::ok )
				{
					for(long j = 0; j < (long)(lower_line._points.size()); j++ )
					{
						new_sm._lower_line._points.push_back(
							convertRowCol2LinePoint(
								lower_line._points[j].row,
								lower_line._points[j].col ) );
					}

					for(long j = 0; j < (long)(upper_line._points.size()); j++ )
					{
						new_sm._upper_line._points.push_back(
							convertRowCol2LinePoint(
								upper_line._points[j].row,
								upper_line._points[j].col ) );
					}

					_supermodes.push_back(new_sm);
				}
			}

		}
		else
		{
			CGR_LOG("CDSDBROverallModeMap::readBoundariesFromFile() unequal number of upper and lower boundary line files found",ERROR_CGR_LOG)
		}
	}
	else
	{
		CGR_LOG("CDSDBROverallModeMap::readBoundariesFromFile() no boundary line files found",ERROR_CGR_LOG)
	}



	CString log_msg("CDSDBROverallModeMap::readBoundariesFromFile() ");
	log_msg.AppendFormat("read in upper and lower boundaries for %d supermodes", _supermodes.size() );
	CGR_LOG(log_msg,INFO_CGR_LOG)


	CGR_LOG("CDSDBROverallModeMap::readBoundariesFromFile() exiting",DIAGNOSTIC_CGR_LOG)

	return rval;
}



CDSDBROverallModeMap::rcode
CDSDBROverallModeMap::writeBoundaryLineToFile(
	std::vector<boundary_point_type> &boundary_line,
	CString abs_filepath,
	bool overwrite )
{

	rcode rval = rcode::ok;
    long row_count = 0;
    long col_count = 0;

	if( boundary_line.empty() )
	{
		rval = rcode::no_data;
		CGR_LOG("CDSDBROverallModeMap::writeBoundaryLineToFile error: no_data",ERROR_CGR_LOG)
	}
	else
	{

		CFileStatus status;

		// Check if file already exists and overwrite not specified

		if( CFile::GetStatus( abs_filepath, status ) && overwrite == false )
		{
			rval = rcode::file_already_exists;
			CGR_LOG("CDSDBROverallModeMap::writeBoundaryLineToFile error: file_already_exists",ERROR_CGR_LOG)
		}
		else
		{
			try
			{
				std::ofstream file_stream( abs_filepath );

				if( file_stream )
				{
					for( long i = 0 ; i < (long)(boundary_line.size()) ; i++ )
					{
						file_stream << boundary_line[i].row;
						file_stream << COMMA_CHAR;
						file_stream << boundary_line[i].col;
						file_stream << std::endl;
					}
				}
				else
				{
					rval = rcode::could_not_open_file;
					CGR_LOG("CDSDBROverallModeMap::writeBoundaryLineToFile error: could_not_open_file",ERROR_CGR_LOG)
				}
			}
			catch(...)
			{
				rval = rcode::could_not_open_file;
				CGR_LOG("CDSDBROverallModeMap::writeBoundaryLineToFile error: could_not_open_file",ERROR_CGR_LOG)
			}

		}

	}

	return rval;
}

CDSDBROverallModeMap::rcode
CDSDBROverallModeMap::
splitSequence(int				maxSequenceSize,
	vector<double>&				sequence,
	vector<CCurrentsVector>&	splitSequences)
{
	rcode rval = rcode::ok;

	int numSequences = 0;

	div_t result = div(((int)sequence.size()), maxSequenceSize);
	if(result.rem > 0)
		numSequences  = result.quot + 1;
	else
		numSequences  = result.quot;


	int sequenceIndex = 0;
	for(int sequenceNumber=0; sequenceNumber<numSequences; sequenceNumber++)
	{
		CCurrentsVector splitSequence;

		splitSequences.push_back(splitSequence);	

		for(int i=0; i<maxSequenceSize; i++)
		{
			if(sequenceIndex<(int)(sequence.size()))
				splitSequences[sequenceNumber]._currents.push_back(sequence[sequenceIndex]);
			else
				break;

			sequenceIndex++;
		}

	}


	return rval;
}

void
CDSDBROverallModeMap::
setContinuityValues()
{
	_continuityValues.clear();
	for( short sm = 0; sm < (short)(_supermodes.size()) ; sm++ )
	{
		std::vector<CDSDBROverallModeMapLine::point_type> *p_filtered_middle_points = &(_supermodes[sm]._filtered_middle_line._points);

		if(!(p_filtered_middle_points->empty()))
		{
			for( long i = 0 ; i < (long)(p_filtered_middle_points->size()) ; i++ )
			{
				long row_i = ((*p_filtered_middle_points)[i]).row;
				long col_i = ((*p_filtered_middle_points)[i]).col;
				double power_ratio = *iPowerRatioPt( row_i, col_i );
				
				_continuityValues.push_back(power_ratio);
			}
		}
	}

	
}

void
CDSDBROverallModeMap::
setPercentComplete( short percent_complete )
{
	EnterCriticalSection(_p_data_protect);

	*_p_percent_complete = percent_complete;

	LeaveCriticalSection(_p_data_protect);

	return;
}

CDSDBROverallModeMap::rcode
CDSDBROverallModeMap::
mapHResultToRCode(HRESULT err)
{
	rcode rval = rcode::ok;

	switch(err)
	{
		case S_OK:
			rval = rcode::ok;
			break;
		case CG_ERROR_SYSTEM_ERROR:
			rval = rcode::system_error;
			break;			
		case CG_ERROR_CONNECTIVITY_FAILED:
			rval = rcode::connectivity_error;
			break;
		case CG_ERROR_POWER_METER_MISSING:
			rval = rcode::power_meter_not_found;
			break;
		case CG_ERROR_WAVEMETER_OFFLINE:
			rval = rcode::wavemeter_not_found;
			break;
		case CG_ERROR_TEC_OFFLINE:
			rval = rcode::tec_not_found;
			break;
		case CG_ERROR_PXIT_READPOWER:
			rval = rcode::power_read_error;
			break;
		case CG_ERROR_DUPLICATE_DEVICE_NAMES:
			rval = rcode::duplicate_device_name;
			break;
		case CG_ERROR_EMPTY_SLOTS_IN_SEQUENCE:
			rval = rcode::empty_sequence_slots;
			break;
		case CG_ERROR_PXIT_MODULES_UNINITIALISED:
		case CG_ERROR_PXIT_MODULE_MISSING:
		case CG_ERROR_NO_PXIT_MODULES_FOUND:
			rval = rcode::pxit_module_error;
			break;
		case CG_ERROR_DEVICE_NAME_NOT_ON_CARD:
			rval = rcode::device_name_not_on_card;
			break;
		case CG_ERROR_306_FIRMWARE_OBSOLETE:
			rval = rcode::device_306_firmware_obsolete;
			break;
		case CG_ERROR_LASER_ALIGN_LOWPOWER:
		case CG_ERROR_LASER_ALIGN_MAXPOWER:
			rval = rcode::laser_alignment_error;
			break;
		case CG_ERROR_OUTPUT_NOT_CONNECTED:
		case CG_ERROR_SET_OUTPUT_RANGE:
		case CG_ERROR_SET_COMPLIANCE_VOLTAGE:
		case CG_ERROR_SET_OUTPUT_MODE:
		case CG_ERROR_SET_MAX_CURRENT:
			rval = rcode::hardware_error;
			break;
		case CG_ERROR_LOGFILE_OPEN:
		case CG_ERROR_LOGFILE_DISK_FULL:
		case CG_ERROR_LOGFILE_CLOSE:
		case CG_ERROR_LOGFILEPATHNAME_REGISTRY_KEY:
		case CG_ERROR_LOGFILEPATHNAME_REGISTRY_DATA:
		case CG_ERROR_LOGGINGLEVEL_REGISTRY_KEY:
		case CG_ERROR_LOGGINGLEVEL_REGISTRY_DATA:
		case CG_ERROR_LOGFILEMAXSIZE_REGISTRY_KEY:
		case CG_ERROR_LOGFILEMAXSIZE_REGISTRY_DATA:
			rval = rcode::log_file_error;
			break;
		case CG_ERROR_PXIT_SETCURRENT:
			rval = rcode::set_current;
			break;
		case CG_ERROR_REGISTRY_DATA:
		case CG_ERROR_REGISTRY_KEY:
			rval = rcode::registry_error;
			break;
		case CG_ERROR_RESULTS_DISK_FULL:
			rval = rcode::results_disk_full;
			break;
		case CG_ERROR_PXIT_STEP_SIZE:
			rval = rcode::sequencing_step_size_error;
		break;
		case CG_ERROR_PXIT_SEQUENCE:
			rval = rcode::sequencing_error;
		break;
		case CG_ERROR_MODULE_NAME_NOT_FOUND:
			rval = rcode::module_name_not_found_error;
		break;
		default:
			rval = rcode::unknown_error;
			break;
	}

	return rval;
}


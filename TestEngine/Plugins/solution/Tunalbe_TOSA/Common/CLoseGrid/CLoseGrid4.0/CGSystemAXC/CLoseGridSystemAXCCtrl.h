// Copyright (c) 2004 PXIT. All Rights Reserved
//
// FileName    : CLoseGridSystemAXCCtrl.h
// Description : Declaration of the CCLoseGridSystemAXCCtrl ActiveX Control class.
//
// Rev#  | Date          | Author               | Description of change
// --------------------------------------------------------------
// 0.1   | 12 July 2004   | Frank D'Arcy         | Initial Draft
//

#pragma once


#include "CGResearchidl.h"
#include <vector>
#include "defaults.h"
#include "DSDBROverallModeMap.h"
#include "DSDBRSuperMode.h"
#include "CGOperatingPoints.h"


class CCLoseGridSystemAXCCtrl : public COleControl
{
	DECLARE_DYNCREATE(CCLoseGridSystemAXCCtrl)

// Constructor
public:
	CCLoseGridSystemAXCCtrl();

// Overrides
public:
	virtual void OnDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid);
	virtual void DoPropExchange(CPropExchange* pPX);
	virtual void OnResetState();
	virtual DWORD GetControlFlags();

// Implementation
protected:
	~CCLoseGridSystemAXCCtrl();

	DECLARE_OLECREATE_EX(CCLoseGridSystemAXCCtrl)    // Class factory and guid
	DECLARE_OLETYPELIB(CCLoseGridSystemAXCCtrl)      // GetTypeInfo
	DECLARE_PROPPAGEIDS(CCLoseGridSystemAXCCtrl)     // Property page IDs
	DECLARE_OLECTLTYPE(CCLoseGridSystemAXCCtrl)		// Type name and misc status

// Message maps
	DECLARE_MESSAGE_MAP()

// Dispatch maps
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()

    afx_msg void getVersionInfo(
		BSTR* p_product_name,
		BSTR* p_release_no,
		BSTR* p_copyright_notice,
		BSTR* p_label,
		BSTR* p_label_info,
		BSTR* p_label_comment,
		BSTR* p_build_configuration,
		BSTR* p_build_datetime,
		BSTR* p_driver_version_info );

    afx_msg void startup();

	afx_msg void setup(
		LPCTSTR laser_type,
		LPCTSTR laser_id,
		BSTR* p_date_time_stamp );

    afx_msg void shutdown();

    afx_msg void rampdown();

	afx_msg void getCurrent(
		LPCTSTR module_name,
		double* p_current );

	afx_msg void setCurrent(
		LPCTSTR module_name,
		double current );

	afx_msg void getOutputConnection(
		LPCTSTR module_name,
		short* p_output_connection );

	afx_msg void setOutputConnection(
		LPCTSTR module_name,
		short output_connection );

	afx_msg void get2DData(
		LPCTSTR module_name,
		VARIANT* p_currents,
		short source_delay,
		short measure_delay,
		BSTR* p_results_dir,
		BSTR* p_date_time_stamp );

	afx_msg void get3DData(
		LPCTSTR module_name_x,
		VARIANT* p_currents_x,
		LPCTSTR module_name_y,
		VARIANT* p_currents_y,
		short map_type,
		short source_delay,
		short measure_delay,
		BSTR* p_results_dir,
		BSTR* p_date_time_stamp );

	afx_msg void startLaserScreeningOverallMap();

	afx_msg void startLaserScreeningSMMap(
		short supermode_number );

	afx_msg void estimateITUOPs(
		BSTR* p_results_dir,
		BSTR* p_date_time_stamp,
		short* p_ITUOP_estimates_count );

	afx_msg void startLaserCharacterisation();

	afx_msg void getPercentComplete(
		short* p_percent_complete );

	afx_msg void getState(
		BSTR* p_state,
		short* p_overall_map_loaded,
		short* p_supermodes_found_count,
		short* p_supermode_map_loaded_count,
		short* p_ITUOP_estimate_count,
		short* p_ITUOP_count );

	afx_msg void getScreeningOverallMapResults(
		BSTR* p_results_dir,
		BSTR* p_laser_type,
		BSTR* p_laser_id,
		BSTR* p_date_time_stamp,
		BSTR* p_map_ramp_direction,
		short* p_screening_overall_map_passed,
		short* p_supermode_count,
		VARIANT* p_average_powers_on_middle_lines );

	afx_msg void getScreeningSMMapResults(
		short supermode_number,
		BSTR* p_results_dir,
		BSTR* p_laser_type,
		BSTR* p_laser_id,
		BSTR* p_date_time_stamp,
		BSTR* p_map_ramp_direction,
		short* p_screening_sm_map_passed,
		short* p_longitudinal_mode_count );

	afx_msg void getCharacterisationResults(
		BSTR* p_results_dir,
		BSTR* p_date_time_stamp,
		short* p_ITUOP_count,
		short* p_duff_ITUOP_count );

	afx_msg void collatePassFailResults(
		BSTR* p_results_dir,
		BSTR* p_date_time_stamp,
		short* p_screening_overall_map_passed,
		VARIANT* p_screening_sm_maps_passed,
		short* p_total_passed );

	afx_msg void loadOverallMap(
		LPCTSTR results_dir,
		LPCTSTR laser_type,
		LPCTSTR laser_id,
		LPCTSTR date_time_stamp,
		LPCTSTR map_ramp_direction );

	afx_msg void loadSMMap(
		LPCTSTR results_dir,
		LPCTSTR laser_type,
		LPCTSTR laser_id,
		LPCTSTR date_time_stamp,
		short supermode_number,
		LPCTSTR map_ramp_direction );

	afx_msg void readVoltage(
		LPCTSTR module_name,
		double* p_voltage );

	afx_msg void readFrequency(
		double* p_frequency );

	afx_msg void readCoarseFrequency(
		double* p_coarse_frequency );

	afx_msg void readPhotodiodeCurrent(
		LPCTSTR module_name,
		short channel,
		double* p_current );

	afx_msg void readOpticalPower(
		LPCTSTR module_name,
		short channel,
		double* p_optical_power );

	afx_msg void readOpticalPowerAtFreq(
		LPCTSTR module_name,
		short channel,
		double frequency,
		double* p_optical_power );

	afx_msg void readTemperature(
		double* p_temperature );

	afx_msg void getTECTempSetpoint(
		double* p_temp_setpoint );

	afx_msg void setTECTempSetpoint(
		double temp_setpoint );

	afx_msg void calcCoarseFreqPolyCoeffs(
		short num_of_sample_pts, 
		short num_of_poly_coeffs,
		VARIANT* p_poly_coeffs,
		double* p_chi_squared );

	afx_msg void getCoarseFreqPolyCoeffs(
		VARIANT* p_poly_coeffs );

	afx_msg void setCoarseFreqPolyCoeffs(
		VARIANT* p_poly_coeffs );

	afx_msg void getCoarseFrequency(
		double power_ratio,
		double* p_coarse_frequency );

	afx_msg void getComplianceVoltage(
		LPCTSTR module_name,
		double* p_compliance_voltage );

	afx_msg void setComplianceVoltage(
		LPCTSTR module_name,
		double compliance_voltage );

	afx_msg void rampdownAndDisconnect();

	afx_msg void stopAndShutdown();

	afx_msg void applyStableOP(
		short sm,
		short lm,
		double index,
		short apply_Gain_SOA_of_SM_map );

	afx_msg void applyPointOnSMMap(
		short sm,
		double Im_index,
		double Ip_index,
		short apply_Gain_SOA_of_SM_map );

	afx_msg void getPointOnSMMap(
		short sm,
		double* p_Im_index,
		double* p_Ip_index );

	afx_msg void initWavemeter();

	afx_msg void loadImFromFile(
		short supermode_number,
		BSTR Im_filename );

	afx_msg void loadImiddleFreqFromFile(
		short supermode_number,
		short longitudinalmode_number,
		BSTR ImiddleFreq_filename );


	// DUAL_SUPPORT_START
	//    add declaration of IDualAClick implementation
	//    You need one entry here for each entry in the
	//    interface statement of the ODL, plus the entries for a
	//    dispatch interface. The BEGIN_DUAL_INTERFACE_PART
	//    macro in mfcdual.h automatically generates the IDispatch
	//    entries for you.
	//    Each entry with the "propput" attribute needs a function
	//    named "put_<property name>". Each entry with the "propget"
	//    attribute needs a function named "get_<property name>".
	//    You can pull these function prototypes from the header file
	//    generated by MKTYPLIB.
	BEGIN_DUAL_INTERFACE_PART(Dual_DCGSystemAXC, IDual_DCGSystemAXC)

		STDMETHOD(getVersionInfo)(THIS_ 
			BSTR* p_product_name,
			BSTR* p_release_no,
			BSTR* p_copyright_notice,
			BSTR* p_label,
			BSTR* p_label_info,
			BSTR* p_label_comment,
			BSTR* p_build_configuration,
			BSTR* p_build_datetime,
			BSTR* p_driver_version_info );

		STDMETHOD(startup)(THIS);

		STDMETHOD(setup)(THIS_ 
			BSTR laser_type,
			BSTR laser_id,
			BSTR* p_date_time_stamp );

		STDMETHOD(shutdown)(THIS);

		STDMETHOD(rampdown)(THIS);

		STDMETHOD(getCurrent)(THIS_ 
			BSTR module_name,
			double* p_current );

		STDMETHOD(setCurrent)(THIS_ 
			BSTR module_name,
			double current );

		STDMETHOD(getOutputConnection)(THIS_ 
			BSTR module_name,
			short* p_output_connection );

		STDMETHOD(setOutputConnection)(THIS_ 
			BSTR module_name,
			short output_connection );

		STDMETHOD(get2DData)(THIS_ 
			BSTR module_name,
			VARIANT* p_currents,
			short source_delay,
			short measure_delay,
			BSTR* p_results_dir,
			BSTR* p_date_time_stamp );

		STDMETHOD(get3DData)(THIS_ 
			BSTR module_name_x,
			VARIANT* p_currents_x,
			BSTR module_name_y,
			VARIANT* p_currents_y,
			short map_type,
			short source_delay,
			short measure_delay,
			BSTR* p_results_dir,
			BSTR* p_date_time_stamp );

		STDMETHOD(startLaserScreeningOverallMap)(THIS);

		STDMETHOD(startLaserScreeningSMMap)(THIS_ 
			short supermode_number );

		STDMETHOD(estimateITUOPs)(THIS_ 
			BSTR* p_results_dir,
			BSTR* p_date_time_stamp,
			short* p_ITUOP_estimates_count );

		STDMETHOD(startLaserCharacterisation)(THIS);

		STDMETHOD(getPercentComplete)(THIS_ 
			short* p_percent_complete );

		STDMETHOD(getState)(THIS_ 
			BSTR* p_state,
			short* p_overall_map_loaded,
			short* p_supermodes_found_count,
			short* p_supermode_map_loaded_count,
			short* p_ITUOP_estimate_count,
			short* p_ITUOP_count );

		STDMETHOD(getScreeningOverallMapResults)(THIS_ 
			BSTR* p_results_dir,
			BSTR* p_laser_type,
			BSTR* p_laser_id,
			BSTR* p_date_time_stamp,
			BSTR* p_map_ramp_direction,
			short* p_screening_overall_map_passed,
			short* p_supermode_count,
			VARIANT* p_average_powers_on_middle_lines );

		STDMETHOD(getScreeningSMMapResults)(THIS_ 
			short supermode_number,
			BSTR* p_results_dir,
			BSTR* p_laser_type,
			BSTR* p_laser_id,
			BSTR* p_date_time_stamp,
			BSTR* p_map_ramp_direction,
			short* p_screening_sm_map_passed,
			short* p_longitudinal_mode_count );

		STDMETHOD(getCharacterisationResults)(THIS_ 
			BSTR* p_results_dir,
			BSTR* p_date_time_stamp,
			short* p_ITUOP_count,
			short* p_duff_ITUOP_count );

		STDMETHOD(collatePassFailResults)(THIS_ 
			BSTR* p_results_dir,
			BSTR* p_date_time_stamp,
			short* p_screening_overall_map_passed,
			VARIANT* p_screening_sm_maps_passed,
			short* p_total_passed );

		STDMETHOD(loadOverallMap)(THIS_ 
			BSTR results_dir,
			BSTR laser_type,
			BSTR laser_id,
			BSTR date_time_stamp,
			BSTR map_ramp_direction );

		STDMETHOD(loadSMMap)(THIS_ 
			BSTR results_dir,
			BSTR laser_type,
			BSTR laser_id,
			BSTR date_time_stamp,
			short supermode_number,
			BSTR map_ramp_direction );

		STDMETHOD(readVoltage)(THIS_ 
			BSTR module_name,
			double* p_voltage );

		STDMETHOD(readFrequency)(THIS_ 
			double* p_frequency );

		STDMETHOD(readCoarseFrequency)(THIS_ 
			double* p_coarse_frequency );

		STDMETHOD(readPhotodiodeCurrent)(THIS_ 
			BSTR module_name,
			short channel,
			double* p_current );

		STDMETHOD(readOpticalPower)(THIS_ 
			BSTR module_name,
			short channel,
			double* p_optical_power );

		STDMETHOD(readOpticalPowerAtFreq)(THIS_ 
			BSTR module_name,
			short channel,
			double frequency,
			double* p_optical_power );

		STDMETHOD(readTemperature)(THIS_ 
			double* p_temperature );

		STDMETHOD(getTECTempSetpoint)(THIS_ 
			double* p_temp_setpoint );

		STDMETHOD(setTECTempSetpoint)(THIS_ 
			double temp_setpoint );

		STDMETHOD(calcCoarseFreqPolyCoeffs)(THIS_ 
			short num_of_sample_pts, 
			short num_of_poly_coeffs,
			VARIANT* p_poly_coeffs,
			double* p_chi_squared );

		STDMETHOD(getCoarseFreqPolyCoeffs)(THIS_ 
			VARIANT* p_poly_coeffs );

		STDMETHOD(setCoarseFreqPolyCoeffs)(THIS_ 
			VARIANT* p_poly_coeffs );

		STDMETHOD(getCoarseFrequency)(THIS_ 
			double power_ratio,
			double* p_coarse_frequency );

		STDMETHOD(getComplianceVoltage)(THIS_ 
			BSTR module_name,
			double* p_compliance_voltage );

		STDMETHOD(setComplianceVoltage)(THIS_ 
			BSTR module_name,
			double compliance_voltage );

		STDMETHOD(rampdownAndDisconnect)(THIS);

		STDMETHOD(stopAndShutdown)(THIS);

		STDMETHOD(applyStableOP)(THIS_ 
			short sm,
			short lm,
			double index,
			short apply_Gain_SOA_of_SM_map );

		STDMETHOD(applyPointOnSMMap)(THIS_
			short sm,
			double Im_index,
			double Ip_index,
			short apply_Gain_SOA_of_SM_map );

		STDMETHOD(getPointOnSMMap)(THIS_
			short sm,
			double* p_Im_index,
			double* p_Ip_index );

		STDMETHOD(initWavemeter)(THIS);


		STDMETHOD(loadImFromFile)(THIS_
			short supermode_number,
			BSTR Im_filename );

		STDMETHOD(loadImiddleFreqFromFile)(THIS_
			short supermode_number,
			short longitudinalmode_number,
			BSTR ImiddleFreq_filename );

	END_DUAL_INTERFACE_PART(Dual_DCGSystemAXC)

	//     add declaration of ISupportErrorInfo implementation
	//     to indicate we support the OLE Automation error object
	DECLARE_DUAL_ERRORINFO()
	// DUAL_SUPPORT_END

// Event maps
	DECLARE_EVENT_MAP()

// Dispatch and event IDs
public:
	enum {
	};

	typedef enum rcode_type
	{
		ok = 0,
		not_1D_array_of_doubles,
		empty_array,
		null_arg,
		variant_clear_error,
		variant_array_creation_error
	};

	typedef enum state_type
	{
		instantiated = 0,
		start_up,
		set_up,
		busy,
		complete
	};

	bool validModuleName( CString module_name_CString );

	bool validLaserType( CString laser_type_CString );

	bool validLaserID( CString laser_id_CString );

	void getNewDateTimeStamp( CString &date_time_stamp_CString );

	CString getSystemDateTimeStamp();
	void setSystemDateTimeStamp( CString date_time_stamp_CString );

	CString getSystemLaserID();
	void setSystemLaserID( CString laser_id_CString );

	bool validDateTimeStamp( CString date_time_stamp_CString );

	bool validDirectoryPath( CString directory_path_CString );

	rcode_type variantR8ArrayToVectorOfDoubles(
		VARIANT *p_array_VARIANT,
		std::vector<double> *p_vector_of_doubles );

	rcode_type vectorOfDoublesToVariantR8Array(
		std::vector<double> *p_vector_of_doubles,
		VARIANT *p_array_VARIANT );

	rcode_type vectorOfShortsToVariantI2Array(
		std::vector<short> *p_vector_of_shorts,
		VARIANT *p_array_VARIANT );

	state_type getSystemState();
	void setSystemState( state_type new_system_state );
	void setPercentComplete( short percent_complete );


	short getOverallMapLoaded();
	void setOverallMapLoaded( short overall_map_loaded );

	short getOMScreeningPassed();
	void setOMScreeningPassed( short overall_map_screening_passed );

	short getSMScreeningPassed( short sm_number );
	void setSMScreeningPassed( short sm_number, short supermode_map_screening_passed );

	short getSMFoundCount();
	void setSMFoundCount( short supermodes_found_count );

	short getSMMapLoadedCount();
	void setSMMapLoadedCount( short supermode_map_loaded_count );

	short getEstimateCount();
	void setEstimateCount( short estimate_count );

	short getITUOPCount();
	void setITUOPCount( short ITUOP_count );

	short getDuffITUOPCount();
	void setDuffITUOPCount( short ITUOP_count );

	DWORD getWorkerThreadID();
	void setWorkerThreadID( DWORD worker_thread_id );

	std::pair<short, std::string> getLastError();
	void setLastError( std::pair<short, std::string> last_error );

	void clear();

	CDSDBROverallModeMap _overall_mode_map;
	//std::vector<CDSDBRSuperMode> _supermodes;

	CCGOperatingPoints _ops;

	IDual_DCGResearch *_p_cgresearch;

private:


	CRITICAL_SECTION _data_protect; // protect system data
	state_type _system_state;
	CString _laser_id_CString;
	CString _date_time_stamp_CString;
	short _overall_map_loaded;
	short _overall_map_screening_passed;
	//std::vector<short> _screening_sm_maps_passed;
	std::vector<short> _supermode_map_screening_passed;
	short _supermodes_found_count;
	short _percent_complete;
	short _supermode_map_loaded_count;
	short _estimate_count;
	short _ITUOP_count;
	short _duff_ITUOP_count;
	std::pair<short, std::string> _last_error;
	DWORD _worker_thread_id;

	// maps CLose-Grid Research errors and throws exceptions
	void	handleCGResearchError(CString calling_function_name, HRESULT err);

};


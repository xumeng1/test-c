// Copyright (c) 2004 PXIT. All Rights Reserved
//
// FileName    : DSDBROverallModeMap.h
// Description : Declaration of CDSDBROverallModeMap class
//               Contains data for a DSDBR Overall mode map
//               including its seven parts as separate mode maps 
//               and utility routines
//
// Rev#  | Date          | Author               | Description of change
// --------------------------------------------------------------
// 0.1   | 23 July 2004   | Frank D'Arcy         | Initial Draft
//

#pragma once

#include "CurrentsVector.h"
#include "DSDBRFrontCurrents.h"
#include "LaserModeMap.h"
#include "DSDBRSuperMode.h"
#include "OverallModeMapAnalysis.h"

class CDSDBROverallModeMap
{
public:
	CDSDBROverallModeMap();
	~CDSDBROverallModeMap(void);

	typedef enum rcode
	{
		ok = 0,
		file_does_not_exist,
		file_already_exists,
		could_not_open_file,
		file_format_invalid,
		files_not_matching_sizes,
		corrupt_map_not_written_to_file,
		existing_file_not_overwritten,
		maps_not_loaded,
		currents_not_loaded,
		outside_map_dimensions,
		corrupt_map_dimensions,
		next_pt_not_found,
		rhs_top_border_not_reached,
		no_data,
		no_start_point,
		no_supermodes_found,
		submap_indexing_error,
		sequencing_error,
		sequence_data_format_error,
		sequencing_step_size_error,
		hardware_error,
		system_error,
		set_current,
		registry_error,
		log_file_error,
		results_disk_full,
		connectivity_error,
		power_meter_not_found,
		wavemeter_not_found,
		tec_not_found,
		power_read_error,
		duplicate_device_name,
		pxit_module_error,
		device_name_not_on_card,
		device_306_firmware_obsolete,
		laser_alignment_error,
		empty_sequence_slots,
		module_name_not_found_error,
		unknown_error
	};


	std::pair<short, std::string> get_error_pair( rcode error_in );

	typedef enum direction_type
	{
		N = 0,
		NW,
		W,
		SW,
		S,
		SE,
		E,
		NE
	};

#define START_PT (direction_type)-1

	typedef struct boundary_point_type
	{
		long row;
		long col;
		direction_type found_in_direction;
	};

	std::vector<boundary_point_type>::iterator findInBoundaryLine(
		std::vector<boundary_point_type> &boundary_line,
		boundary_point_type boundary_point );

	CDSDBROverallModeMapLine::point_type
	interpolateLinePoint(
		double row,
		double col );

	CDSDBROverallModeMapLine::point_type
	convertRowCol2LinePoint(
		long row,
		long col );

	rcode loadMapsFromFile(
		CString results_dir_CString,
		CString laser_id_CString,
		CString date_time_stamp_CString,
		CString map_ramp_direction_CString );

	rcode collectMapsFromLaser();

	rcode writeMapsToFile();

	bool loaded();

	void clear();

	rcode screen();

	rcode writeScreeningResultsToFile();

	short supermode_count();

	rcode getAveragePowersOnMiddleLines(
		std::vector<double> &average_powers_on_middle_lines );

	rcode applyMedianFilterToMaps();

	rcode applyMedianFilter(
		CLaserModeMap &map_in,
		CLaserModeMap &filtered_map_out );

	//rcode findSMsByBorders();

	rcode findSMBoundaries();

	rcode calculateMaxDeltaPrMap();

	rcode findSMMiddleLines();

	rcode filterSMMiddleLines();

	rcode checkSMMiddleLines();

	//rcode findSMsOnLine(
	//	std::vector<double> &line,
	//	std::vector<short> &sm_numbers);

	//rcode findNextBoundaryPoint(
	//	direction_type start_direction,
	//	bool find_new_point_anticlockwise,
	//	boundary_point_type &current_pt,
	//	boundary_point_type &next_pt,
	//	double &next_pt_value,
	//	std::vector<boundary_point_type> excluded_pts );

	rcode findNextBoundaryPointOnMaxDeltaPrMap(
		direction_type start_direction,
		bool find_new_point_anticlockwise,
		boundary_point_type &current_pt,
		boundary_point_type &next_pt,
		double &next_pt_MaxDeltaPr_value,
		std::vector<boundary_point_type> &excluded_pts,
		double threshold_MaxDeltaPr );

	//rcode findBoundaryLineFromRHSTopBorderPt(
	//	std::vector<boundary_point_type> &boundary_line,
	//	long row_of_start_pt,
	//	long col_of_start_pt,
	//	direction_type input_start_direction,
	//	bool search_anticlockwise );

	rcode findBoundaryLineOnMaxDeltaPrMap(
		std::vector<boundary_point_type> &boundary_line,
		double threshold_MaxDeltaPr,
		direction_type input_start_direction,
		bool search_anticlockwise );

	rcode writeSMLinesToFile(
		CString abs_filepath,
		bool overwrite = false );

	rcode writePRatioContinuityToFile(
		CString abs_filepath,
		bool overwrite = false );

	rcode writeBoundaryLineToFile(
		std::vector<boundary_point_type> &boundary_line,
		CString abs_filepath,
		bool overwrite = false );

	rcode writeLMMiddleFreqCurrentsToFile(
		CString abs_filepath,
		bool overwrite = false );

	rcode readBoundariesFromFile();

	//rcode findJumpsInMaps();

	//rcode findJumps(
	//	CLaserModeMap &map_in, 
	//	CLaserModeMap &vector_in );

	std::vector<double>::iterator iPowerRatioPt( long row, long col );
	std::vector<double>::iterator iDirectPowerPt( long row, long col );
	std::vector<double>::iterator iMaxDeltaPrPt( long row, long col );
	std::vector<double>::iterator iPowerRatioMedianFilteredPt( long row, long col );

	bool isInMap( long row, long col );


	rcode getValueOfAdjacentPt(
		long row,
		long col, 
		direction_type direction,
		long &adj_pt_row,
		long &adj_pt_col,
		double &value );

	rcode getMaxDeltaPrValueOfAdjacentPt(
		long row,
		long col, 
		direction_type direction,
		long &adj_pt_row,
		long &adj_pt_col,
		double &value );

	rcode calcFilteredValueOfAdjacentPt(
		long row,
		long col, 
		direction_type direction,
		long &adj_pt_row,
		long &adj_pt_col,
		double &value );

	direction_type opposite(
		direction_type direction );

	CString getCStringForDirection(
		direction_type direction );

	void getRelativeRowCol(
		long row_in,
		long col_in,
		direction_type direction,
		long distance,
		long &row_out,
		long &col_out );

	rcode defineSequence(
		short pair_number,
		std::vector<double> &rear_sequence,
		std::vector<double> &front_sequence );

	rcode createSequences(
		std::vector<double>&	rampCurrents,
		std::vector<double>&	stepCurrents,
		bool hysteresis,
		std::vector<double>& rampSequence,
		std::vector<double>& stepSequence );

	rcode loadFrontPairSequences(CString			rearModuleName,
								std::vector<double>&	rear_sequence,
								CString				nonConstantFrontModuleName,
								std::vector<double>&	front_sequence,
								CString				powerMeterModuleName,
								int					sourceDelay,
								int					measureDelay);

	rcode	load305Sequence(CString				moduleName,
							std::vector<double>&	sequence,
							int					sourceDelay,
							int					measureDelay);

	rcode	load306Sequence(CString				moduleName,
							int					numSequencePoints,
							int					sourceDelay,
							int					measureDelay);

	rcode	connectFrontPairOutputs(CString	rearModuleName,
									CString	nonConstantFrontModuleName,
									CString	constantModuleName);

	rcode	setFrontPairModulesInSequence(CString	rearModuleName,
										CString		nonConstantFrontModuleName,
										CString		constantModuleName,
										CString		powerMeterModuleName);

	rcode	setFrontPairModulesNotInSequence(CString	rearModuleName,
										CString		nonConstantFrontModuleName,
										CString		constantModuleName,
										CString		powerMeterModuleName);

	rcode splitHysteresisPowerArray(
		std::vector<double>	&powerVector,
		int				numPointsPerCycle,
		std::vector<double>	&forwardPower,
		std::vector<double>	&reversePower);

	rcode	splitSequence(int						maxSequenceSize,
						vector<double>&				sequence,
						vector<CCurrentsVector>&	splitSequences);

	//void modeBoundaries(
	//	std::vector<double>::iterator i_Po_start,	// starting point on Po vector
	//	std::vector<double>::iterator i_I_start,	// starting point on I vector
	//	unsigned long start_point_index,			// index of first point within a map
	//	unsigned long index_increment,				// increment on vectors to next point
	//	unsigned long line_length,					// number of points in the line
	//	boundary_multimap_typedef &boundary_map );	// map of boundary jumps found

	// Currents used to collect maps
	CCurrentsVector _vector_Ir;
	CDSDBRFrontCurrents _vector_If;

	// Direct Power Maps collected from laser or read from file
	std::vector<CLaserModeMap> _map_forward_direct_power;

	// Filtered Power Maps collected from laser or read from file
	std::vector<CLaserModeMap> _map_forward_filtered_power;

	// Direct Power Maps collected from laser or read from file
	std::vector<CLaserModeMap> _map_reverse_direct_power;

	// Filtered Power Maps collected from laser or read from file
	std::vector<CLaserModeMap> _map_reverse_filtered_power;

	// Photodiode Maps collected from laser
	std::vector<CLaserModeMap> _map_forward_photodiode1;
	std::vector<CLaserModeMap> _map_forward_photodiode2;
	std::vector<CLaserModeMap> _map_reverse_photodiode1;
	std::vector<CLaserModeMap> _map_reverse_photodiode2;

	// Power Ratio Maps collected from laser or read from file
	CLaserModeMap _map_power_ratio_dfs1;
	CLaserModeMap _map_power_ratio_dfs2;
	CLaserModeMap _map_power_ratio_dfs3;
	CLaserModeMap _map_power_ratio_dfs4;
	CLaserModeMap _map_power_ratio_dfs5;
	CLaserModeMap _map_power_ratio_dfs6;
	CLaserModeMap _map_power_ratio_dfs7;

	// Max deltaPr maps
	std::vector<CLaserModeMap> _map_max_deltaPr;

	// Power Ratio Maps after median filter has been applied
	CLaserModeMap _map_power_ratio_dfs1_median_filtered;
	CLaserModeMap _map_power_ratio_dfs2_median_filtered;
	CLaserModeMap _map_power_ratio_dfs3_median_filtered;
	CLaserModeMap _map_power_ratio_dfs4_median_filtered;
	CLaserModeMap _map_power_ratio_dfs5_median_filtered;
	CLaserModeMap _map_power_ratio_dfs6_median_filtered;
	CLaserModeMap _map_power_ratio_dfs7_median_filtered;

	// vector contains supermodes found in map
	std::vector<CDSDBRSuperMode> _supermodes;

	// count of the number of supermodes removed because of flaws encountered
	short _supermodes_removed;

	bool _maps_loaded;
	bool _Ir_ramp; // true for Ir ramp, false for If ramp

	// The above CLaserModeMap objects can contain
	// rear ramped or front ramped maps and the members
	// _rows and _cols in those objects change accordingly.
	// However, for the members _rows and _cols in this object
	// _rows always refers to the number of front current points and
	// _cols always refers to the number of rear current points.
	// As such, the overall mode map is always viewed the same way
	// independent of which ramp direction was used to gather the maps.
	long _rows;
	long _cols;

	int	_xAxisLength;
	int	_yAxisLength;

	//double _threshold_maxDeltaPr;

	CString _results_dir_CString;
	CString _laser_id_CString;
	CString _date_time_stamp_CString;

	void setPercentComplete( short percent_complete );
	CRITICAL_SECTION *_p_data_protect;
	short *_p_percent_complete;

	OverallModeMapAnalysis	_overallModeMapAnalysis;

	void	setContinuityValues();
	vector<double>	_continuityValues;

	rcode	mapHResultToRCode(HRESULT err);
};

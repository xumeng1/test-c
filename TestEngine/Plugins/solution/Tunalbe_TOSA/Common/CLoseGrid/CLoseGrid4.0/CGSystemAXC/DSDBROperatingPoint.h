// Copyright (c) 2004 PXIT. All Rights Reserved
//
// FileName    : DSDBROperatingPoint.h
// Description : Declaration of CDSDBROperatingPoint class
//
// Rev#  | Date          | Author               | Description of change
// --------------------------------------------------------------
// 0.1   | 01 Nov 2004  | Frank D'Arcy         | Initial Draft
//

#pragma once

#include "DSDBRSuperMode.h"

class CDSDBROperatingPoint 
{
public:

	typedef enum rtype
	{
		ok = 0,
		min_optical_power_not_reached,
		error_reading_optical_power,
		invalid_data,
		error_reading_output_connection,
		error_setting_output_connection,
		error_setting_current
	};

	// constructor
	CDSDBROperatingPoint();

	CDSDBROperatingPoint(
		std::vector<CDSDBRSuperMode>::iterator i_sm,
		std::vector<CDSDBRSupermodeMapLine>::iterator i_lm_line,
		short sm_number,
		short lm_number,
		short i_of_pt_on_line );

	CDSDBROperatingPoint(
		std::vector<CDSDBRSuperMode>::iterator i_sm,
		std::vector<CDSDBRSupermodeMapLine>::iterator i_lm_line,
		short sm_number,
		short lm_number,
		double i_of_pt_on_line );

	CDSDBROperatingPoint(
		std::vector<CDSDBRSuperMode>::iterator i_sm,
		short sm_number,
		double index_of_Im,
		double index_of_Ip );

	// destructor
    ~CDSDBROperatingPoint();


	void clear();

	rtype apply();

	short _ITU_channel_number;
	double _ITU_frequency;
	short _sm_number;
	short _lm_number;
	short _index_on_middle_line_of_frequency;
	double _real_index_on_middle_line_of_frequency;

	long _row_on_sm_map;
	long _col_on_sm_map;
	double _Pr_on_sm_map;

	short _front_pair_number;
	double _I_gain;
	double _I_soa;
	double _I_rear;
	double _I_front_1;
	double _I_front_2;
	double _I_front_3;
	double _I_front_4;
	double _I_front_5;
	double _I_front_6;
	double _I_front_7;
	double _I_front_8;
	double _I_phase;

	double _P_ratio_measured;
	double _F_measured;
	double _F_coarse;
	double _P_measured;
	bool _stable_F_measurement;
	bool _is_an_ITU_point;

	long _row_of_boundary_above;
	long _col_of_boundary_above;
	long _row_of_boundary_below;
	long _col_of_boundary_below;
	long _row_of_boundary_to_left;
	long _col_of_boundary_to_left;
	long _row_of_boundary_to_right;
	long _col_of_boundary_to_right;
};


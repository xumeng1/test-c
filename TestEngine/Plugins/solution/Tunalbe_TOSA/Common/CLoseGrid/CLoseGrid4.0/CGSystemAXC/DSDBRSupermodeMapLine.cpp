// Copyright (c) 2004 Tsunami Photonics Ltd. All Rights Reserved
//
// FileName    : DSDBRSupermodeMapLine.cpp
// Description : Definition of CDSDBRSupermodeMapLine class
//               Represents a set of point on DSDBRSupermodeMapLine
//               eg. Longitudinal mode boundaries
//               or Longitudinal mode middle lines of frequency
//
// Rev#  | Date          | Author               | Description of change
// --------------------------------------------------------------
// 0.1   | 12 Oct 2004   | Frank D'Arcy         | Initial Draft
//
//

#include "stdafx.h"
#include "DSDBRSupermodeMapLine.h"
#include <fstream>
#include <direct.h>


// constructor
CDSDBRSupermodeMapLine::CDSDBRSupermodeMapLine()
{
	clear();
}

// destructor
CDSDBRSupermodeMapLine::~CDSDBRSupermodeMapLine()
{
	clear();
}


// clear all contents
void
CDSDBRSupermodeMapLine::clear()
{
	_points.clear();
}

bool
CDSDBRSupermodeMapLine::hasChangeOfFrontPairs()
{
	for( long i = 0 ; i < (long)(_points.size()) ; i++ )
	{
		short first_pair_num = _points[0].front_pair_number;
		short pair_num = _points[i].front_pair_number;

		if( pair_num != first_pair_num )
			return true;
	}

	return false;
}

CDSDBRSupermodeMapLine::rcode
CDSDBRSupermodeMapLine::getFrontPairNumbers(
	std::vector<short> &front_pair_numbers )
{
	rcode rval = rcode::ok;
	front_pair_numbers.clear();

	if( !(_points.empty()) )
	{
		for( long i = 0 ; i < (long)(_points.size()) ; i++ )
		{
			short num_i = _points[i].front_pair_number;
			if( num_i >= 1 && num_i <= 7 )
			{
				bool in_vector = false;
				for( long ip = 0 ; ip < (long)(front_pair_numbers.size()) ; ip++ )
				{
					if( front_pair_numbers[ip] == num_i )
					{
						in_vector = true;
						break;
					}
				}
				if( in_vector == false )
				{
					front_pair_numbers.push_back( num_i );
				}
			}
			else
			{
				rval = rcode::invalid_front_pair_number;
				break;
			}
		}
	}
	else
	{
		rval = rcode::no_data;
	}

	return rval;
}



CDSDBRSupermodeMapLine::rcode
CDSDBRSupermodeMapLine::writeRowColToFile(
	CString abs_filepath,
	bool overwrite )
{
	rcode rval = rcode::ok;
    long row_count = 0;
    long col_count = 0;

	if( _points.empty() )
	{
		rval = rcode::no_data;
	}
	else
	{

		CFileStatus status;

		// Check if file already exists and overwrite not specified

		if( CFile::GetStatus( abs_filepath, status ) && overwrite == false )
		{
			rval = rcode::file_already_exists;
		}
		else
		{

			// Ensure the directory exists
			int index_of_last_dirslash = abs_filepath.ReverseFind('\\');
			if( index_of_last_dirslash == -1 )
			{
				// '\\' not found => not an absolute path
				rval = rcode::could_not_open_file;
			}
			else
			{
				CString dir_name = abs_filepath;
				dir_name.Truncate(index_of_last_dirslash);

				int i = 0;
				CString token = dir_name.Tokenize( "\\/", i );
				CString new_dir = "";
				while ( token != "" )
				{
					if( new_dir.GetLength() == 0 )
					{
						new_dir = token;
					}
					else
					{
						new_dir = new_dir + "\\" + token;
					}
					CFileStatus status;
					//printf("\n %s  = %d", new_dir, CFile::GetStatus( new_dir, status ) );
					if( new_dir.GetLength() > 2 // skip drive name
					&& !CFile::GetStatus( new_dir, status ) )
					{ // directory does not exists => make it
						_mkdir( new_dir );
					}
					else
					{ // directory exists
					}
					//printf("\n %s  = %d", new_dir, CFile::GetStatus( new_dir, status ) );
		            
					token = dir_name.Tokenize( "\\/", i );
				}
				try
				{
					std::ofstream file_stream( abs_filepath );

					if( file_stream )
					{
						for( long i = 0 ; i < (long)(_points.size()) ; i++ )
						{
							file_stream << _points[i].row;
							file_stream << COMMA_CHAR;
							file_stream << _points[i].col;
							file_stream << std::endl;
						}
					}
					else
					{
						rval = rcode::could_not_open_file;
					}
				}
				catch(...)
				{
					rval = rcode::could_not_open_file;
				}
			}
		}

	}

	return rval;
}


CDSDBRSupermodeMapLine::rcode
CDSDBRSupermodeMapLine::readRowColFromFile(
	CString abs_filepath )
{
	rcode rval = rcode::ok;

    long row_count = 0;
    long first_line_col_count = 0;
    long col_count = 0;

	CFileStatus status;

    // Check file

	if( CFile::GetStatus( abs_filepath, status ) )
	{
		// File exists => attempt to read it's contents
		try
		{
			std::ifstream file_stream( abs_filepath );

			if( file_stream )
			{
				row_count = 0;

				while( !file_stream.eof() )
				{
					// File stream is open, read each line
					char temp_buffer[MAX_LINE_LENGTH];
					*temp_buffer = '\0';
					file_stream.getline( temp_buffer, MAX_LINE_LENGTH );

					if( strlen(temp_buffer) > 0 ) // there is something on the line
					{
						point_type new_point;

        				col_count = 0;
						char *fp = strtok(temp_buffer,DELIMITERS);
						new_point.row = atol(fp);
						col_count++;

						fp = strtok(NULL,DELIMITERS);
						if(fp)
						{
							new_point.col = atol(fp);
							col_count++;
						}

						fp = strtok(NULL,DELIMITERS);
						if(fp)
						{ // should only be two values, row and col
							rval = rcode::file_format_invalid;
							break;
						}

						if( row_count == 0 )
						{
							first_line_col_count = col_count;

							if( col_count != 2  )
							{ // should only be two values, row and col
								rval = rcode::file_format_invalid;
								break;
							}
						}

						_points.push_back(new_point);
						row_count++;
					}

					if( row_count > 0 && col_count != first_line_col_count )
					{
						rval = rcode::file_format_invalid;
						break;
					}
				}
			}
			else
			{
        		rval = rcode::could_not_open_file;
			}
		}
		catch(...)
		{
			rval = rcode::could_not_open_file;
		}

	}
	else
	{
		rval = rcode::file_does_not_exist;
	}

    if( rval == rcode::ok && row_count == 0 )
	{
		rval = rcode::file_format_invalid;
	}

	return rval;
}



CDSDBRSupermodeMapLine::rcode
CDSDBRSupermodeMapLine::writeCurrentsToFile(
	CString abs_filepath,
	bool overwrite )
{
	rcode rval = rcode::ok;
    long row_count = 0;
    long col_count = 0;

	if( _points.empty() )
	{
		rval = rcode::no_data;
	}
	else
	{

		CFileStatus status;

		// Check if file already exists and overwrite not specified

		if( CFile::GetStatus( abs_filepath, status ) && overwrite == false )
		{
			rval = rcode::file_already_exists;
		}
		else
		{

        // Ensure the directory exists
		int index_of_last_dirslash = abs_filepath.ReverseFind('\\');
		if( index_of_last_dirslash == -1 )
		{
			// '\\' not found => not an absolute path
			rval = rcode::could_not_open_file;
		}
		else
		{
			CString dir_name = abs_filepath;
			dir_name.Truncate(index_of_last_dirslash);

			int i = 0;
			CString token = dir_name.Tokenize( "\\/", i );
			CString new_dir = "";
			while ( token != "" )
			{
				if( new_dir.GetLength() == 0 )
				{
					new_dir = token;
				}
				else
				{
					new_dir = new_dir + "\\" + token;
				}
				CFileStatus status;
				//printf("\n %s  = %d", new_dir, CFile::GetStatus( new_dir, status ) );
				if( new_dir.GetLength() > 2 // skip drive name
				&& !CFile::GetStatus( new_dir, status ) )
				{ // directory does not exists => make it
					_mkdir( new_dir );
				}
				else
				{ // directory exists
				}
				//printf("\n %s  = %d", new_dir, CFile::GetStatus( new_dir, status ) );
	            
				token = dir_name.Tokenize( "\\/", i );
			}

				try
				{
					std::ofstream file_stream( abs_filepath );

					if( file_stream )
					{
						// First write header row
						file_stream << "Rear Current [mA]";
						file_stream << COMMA_CHAR;
						file_stream << "Front Pair Number";
						file_stream << COMMA_CHAR;
						file_stream << "Constant Front Current [mA]";
						file_stream << COMMA_CHAR;
						file_stream << "Non-constant Front Current [mA]";
						file_stream << COMMA_CHAR;
						file_stream << "Phase Current [mA]";
						file_stream << COMMA_CHAR;
						file_stream << "Row Index";
						file_stream << COMMA_CHAR;
						file_stream << "Column Index";
						file_stream << std::endl;

						for( long i = 0 ; i < (long)(_points.size()) ; i++ )
						{
							// First write data rows
							file_stream << _points[i].I_rear;
							file_stream << COMMA_CHAR;
							file_stream << _points[i].front_pair_number;
							file_stream << COMMA_CHAR;


							switch( _points[i].front_pair_number )
							{
							case 1:
								file_stream << _points[i].I_front_1;
								file_stream << COMMA_CHAR;
								file_stream << _points[i].I_front_2;
								break;
							case 2:
								file_stream << _points[i].I_front_2;
								file_stream << COMMA_CHAR;
								file_stream << _points[i].I_front_3;
								break;
							case 3:
								file_stream << _points[i].I_front_3;
								file_stream << COMMA_CHAR;
								file_stream << _points[i].I_front_4;
								break;
							case 4:
								file_stream << _points[i].I_front_4;
								file_stream << COMMA_CHAR;
								file_stream << _points[i].I_front_5;
								break;
							case 5:
								file_stream << _points[i].I_front_5;
								file_stream << COMMA_CHAR;
								file_stream << _points[i].I_front_6;
								break;
							case 6:
								file_stream << _points[i].I_front_6;
								file_stream << COMMA_CHAR;
								file_stream << _points[i].I_front_7;
								break;
							case 7:
								file_stream << _points[i].I_front_7;
								file_stream << COMMA_CHAR;
								file_stream << _points[i].I_front_8;
								break;
							}

							file_stream << COMMA_CHAR;
							file_stream << _points[i].I_phase;
							file_stream << COMMA_CHAR;
							file_stream << _points[i].real_row;
							file_stream << COMMA_CHAR;
							file_stream << _points[i].real_col;

							file_stream << std::endl;
						}
					}
					else
					{
						rval = rcode::could_not_open_file;
					}
				}
				catch(...)
				{
					rval = rcode::could_not_open_file;
				}
			}
		}

	}

	return rval;
}




CDSDBRSupermodeMapLine::rcode
CDSDBRSupermodeMapLine::readCurrentsFromFile(
	CString abs_filepath )
{
	rcode rval = rcode::ok;

	clear();

    long row_count = 0;
    long first_line_col_count = 0;

	CFileStatus status;

    // Check file

	if( CFile::GetStatus( abs_filepath, status ) )
	{
		// File exists => attempt to read it's contents
		try
		{
			std::ifstream file_stream( abs_filepath );

			if( file_stream )
			{
				// File stream is open, read in header line, then ignore it!
				char map_buffer[MAX_LINE_LENGTH];
				*map_buffer = '\0';
				file_stream.getline( map_buffer, MAX_LINE_LENGTH );

				row_count = 0;
				while( !file_stream.eof() )
				{
					*map_buffer = '\0';
					file_stream.getline( map_buffer, MAX_LINE_LENGTH );

					int map_buffer_lenght = (int)strlen(map_buffer);
					if( map_buffer_lenght > 0 ) // there is something on the line
					{
						char *fp = strtok(map_buffer,DELIMITERS);

						double rear_current = -1;
						short front_pair_number = 0;
						double constant_If = -1;
						double nonconstant_If = -1;
						double phase_current = -1;
						double row_from_file = -1;
						double col_from_file = -1;

						if(fp) // read Irear
						{
							rear_current = atof(fp);
							fp = strtok(NULL,DELIMITERS);
						}

						if(fp) // read front_pair_number
						{
							front_pair_number = atoi(fp);
							fp = strtok(NULL,DELIMITERS);
						}

						if(fp) // read constant_If
						{
							constant_If = atof(fp);
							fp = strtok(NULL,DELIMITERS);
						}

						if(fp) // read nonconstant_If
						{
							nonconstant_If = atof(fp);
							fp = strtok(NULL,DELIMITERS);
						}

						if(fp) // read Iphase
						{
							phase_current = atof(fp);
							fp = strtok(NULL,DELIMITERS);
						}

						if(fp) // read row
						{
							row_from_file = atof(fp);
							fp = strtok(NULL,DELIMITERS);
						}

						if(fp) // read col
						{
							col_from_file = atof(fp);
							fp = strtok(NULL,DELIMITERS);
						}

						if( front_pair_number > 7 || front_pair_number < 1
						 || constant_If < 0 || nonconstant_If < 0 )
						{
							rval = rcode::file_format_invalid;
							break;
						}

						point_type new_Im_point;
						new_Im_point.I_rear = rear_current;
						new_Im_point.front_pair_number = front_pair_number;
						new_Im_point.I_front_1 = 0;
						new_Im_point.I_front_2 = 0;
						new_Im_point.I_front_3 = 0;
						new_Im_point.I_front_4 = 0;
						new_Im_point.I_front_5 = 0;
						new_Im_point.I_front_6 = 0;
						new_Im_point.I_front_7 = 0;
						new_Im_point.I_front_8 = 0;
						new_Im_point.I_phase = phase_current;
						new_Im_point.row = (long)row_from_file;
						new_Im_point.col = (long)row_from_file;
						new_Im_point.real_row = row_from_file;
						new_Im_point.real_col = row_from_file;


						switch( front_pair_number )
						{
						case 1:
							new_Im_point.I_front_1 = constant_If;
							new_Im_point.I_front_2 = nonconstant_If;
							break;
						case 2:
							new_Im_point.I_front_2 = constant_If;
							new_Im_point.I_front_3 = nonconstant_If;
							break;
						case 3:
							new_Im_point.I_front_3 = constant_If;
							new_Im_point.I_front_4 = nonconstant_If;
							break;
						case 4:
							new_Im_point.I_front_4 = constant_If;
							new_Im_point.I_front_5 = nonconstant_If;
							break;
						case 5:
							new_Im_point.I_front_5 = constant_If;
							new_Im_point.I_front_6 = nonconstant_If;
							break;
						case 6:
							new_Im_point.I_front_6 = constant_If;
							new_Im_point.I_front_7 = nonconstant_If;
							break;
						case 7:
							new_Im_point.I_front_7 = constant_If;
							new_Im_point.I_front_8 = nonconstant_If;
							break;
						}

						_points.push_back( new_Im_point );

						row_count++;
					}
				}

			}
			else
			{
        		rval = rcode::could_not_open_file;
			}
		}
		catch(...)
		{
			rval = rcode::could_not_open_file;
		}

	}
	else
	{
		rval = rcode::file_does_not_exist;
	}

    if( rval == rcode::ok
	 && ( row_count == 0
	   || _points.size() != row_count ) )
	{
		rval = rcode::file_format_invalid;
	}

	if( rval != rcode::ok )
	{
		// something is wrong, clear everything!
		clear();
	}

	return rval;
}

std::vector<double>
CDSDBRSupermodeMapLine::
getRows()
{
	std::vector<double> rows;

	for( long i = 0 ; i < (long)(_points.size()) ; i++ )
	{
		rows.push_back((double)_points[i].row);
	}

	return rows;
}

std::vector<double>
CDSDBRSupermodeMapLine::
getCols()
{
	std::vector<double> cols;

	for( long i = 0 ; i < (long)(_points.size()) ; i++ )
	{
		cols.push_back((double)_points[i].col);
	}

	return cols;
}
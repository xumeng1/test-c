// Copyright (c) 2004 PXIT. All Rights Reserved
//
// FileName    : CGSystemAXC.cpp
// Description : Implementation of CCGSystemAXCApp and DLL registration.
//
// Rev#  | Date          | Author               | Description of change
// --------------------------------------------------------------
// 0.1   | 12 July 2004   | Frank D'Arcy         | Initial Draft
//

#include "stdafx.h"
#include "CGSystemAXC.h"
#include "CGSystemAXC_i.c"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


CCGSystemAXCApp NEAR theApp;

const GUID CDECL BASED_CODE _tlid =
		{ 0x43320672, 0x7282, 0x4C55, { 0xB5, 0x86, 0xAB, 0x16, 0x3, 0xDD, 0x98, 0xCF } };
const WORD _wVerMajor = 1;
const WORD _wVerMinor = 0;



// CCGSystemAXCApp::InitInstance - DLL initialization

BOOL CCGSystemAXCApp::InitInstance()
{
	BOOL bInit = COleControlModule::InitInstance();

	if (bInit)
	{
		// TODO: Add your own module initialization code here.

	    // DUAL_SUPPORT_START
	    //    make sure the type library is registered. Otherwise dual interface won't work!
	    AfxOleRegisterTypeLib(AfxGetInstanceHandle(), LIBID_CGSystemAXCLib, _T("CGSystemAXC.tlb"));
	    // DUAL_SUPPORT_END
	}

	return bInit;
}



// CCGSystemAXCApp::ExitInstance - DLL termination

int CCGSystemAXCApp::ExitInstance()
{
	// TODO: Add your own module termination code here.

	return COleControlModule::ExitInstance();
}



// DllRegisterServer - Adds entries to the system registry

STDAPI DllRegisterServer(void)
{
	AFX_MANAGE_STATE(_afxModuleAddrThis);

	if (!AfxOleRegisterTypeLib(AfxGetInstanceHandle(), _tlid))
		return ResultFromScode(SELFREG_E_TYPELIB);

	if (!COleObjectFactoryEx::UpdateRegistryAll(TRUE))
		return ResultFromScode(SELFREG_E_CLASS);

	return NOERROR;
}



// DllUnregisterServer - Removes entries from the system registry

STDAPI DllUnregisterServer(void)
{
	AFX_MANAGE_STATE(_afxModuleAddrThis);

	if (!AfxOleUnregisterTypeLib(_tlid, _wVerMajor, _wVerMinor))
		return ResultFromScode(SELFREG_E_TYPELIB);

	if (!COleObjectFactoryEx::UpdateRegistryAll(FALSE))
		return ResultFromScode(SELFREG_E_CLASS);

	return NOERROR;
}

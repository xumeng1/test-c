// Copyright (c) 2002 Tsunami Photonics Ltd. All Rights Reserved

///////////////////////////////////////////////////////////////////////////////
///
//	Class	:
//
//	Contents:
//
//	Author	:
//
///
///////////////////////////////////////////////////////////////////////////////
///
//
#include "stdafx.h"

#include "PXIT305SequenceSettingsList.h"

#define new DEBUG_NEW

/////////////////////////////////////////////////////////////////////

PXIT305SequenceSettingsList::
PXIT305SequenceSettingsList()
{
}

PXIT305SequenceSettingsList::
~PXIT305SequenceSettingsList(void)
{
	emptyTheList();
}

/////////////////////////////////////////////////////////////////////

PXIT305SequenceSettings* 
PXIT305SequenceSettingsList::
findObjectByIndex(unsigned int index)
{
	PXIT305SequenceSettings* pPXIT305SequenceSettings = NULL;

	if (!IsEmpty())
	{
		POSITION pos;

		int i=0;
		for(pos = this->GetHeadPosition(); pos !=NULL;)
		{
			pPXIT305SequenceSettings = reinterpret_cast<PXIT305SequenceSettings*>(this->GetNext(pos));
			if(pPXIT305SequenceSettings!=NULL)
			{
				if (i == index)
				{
					return pPXIT305SequenceSettings;
				}
			}

			i++;
		}
	}

	return NULL;
}

/////////////////////////////////////////////////////////////////////

PXIT305SequenceSettings* 
PXIT305SequenceSettingsList::
findObject(CString moduleName)
{
	PXIT305SequenceSettings* pPXIT305SequenceSettings = NULL;

	if (!IsEmpty())
	{
		POSITION pos;

		for(pos = this->GetTailPosition(); pos !=NULL;)
		{
			pPXIT305SequenceSettings = reinterpret_cast<PXIT305SequenceSettings*>(this->GetPrev(pos));
			if(pPXIT305SequenceSettings!=NULL)
			{
				if (pPXIT305SequenceSettings->_moduleName == moduleName)
				{	
					return pPXIT305SequenceSettings;
				}
			}
		}
	}

	return NULL;
}

/////////////////////////////////////////////////////////////////////

bool 
PXIT305SequenceSettingsList::
addToList(PXIT305SequenceSettings* pPXIT305SequenceSettings)
{
	bool success = true;

	this->AddHead(pPXIT305SequenceSettings);

	return success;
}

/////////////////////////////////////////////////////////////////////

bool 
PXIT305SequenceSettingsList::
removeFromList(PXIT305SequenceSettings* pPXIT305SequenceSettingsIn)
{
	bool success = FALSE;

	if(inList(pPXIT305SequenceSettingsIn))
	{
		POSITION pos;

		for(pos = this->GetTailPosition(); pos !=NULL;)
		{
			PXIT305SequenceSettings* pPXIT305SequenceSettings = NULL;
			pPXIT305SequenceSettings = reinterpret_cast<PXIT305SequenceSettings*>(this->GetPrev(pos));
			if(pPXIT305SequenceSettings!=NULL)
			{
				if (pPXIT305SequenceSettings->_moduleName ==  pPXIT305SequenceSettingsIn->_moduleName)
				{
					this->RemoveAt(pos);
					success = TRUE;
				}
			}
		}
	}
	else
	{
		success = FALSE;
	}

	return success;
}

/////////////////////////////////////////////////////////////////////

bool 
PXIT305SequenceSettingsList::
inList(PXIT305SequenceSettings *pPXIT305SequenceSettingsIn)
{
	PXIT305SequenceSettings *pPXIT305SequenceSettings = NULL;

	if (!IsEmpty())
	{
		POSITION pos;

		for(pos = this->GetTailPosition(); pos !=NULL;)
		{
			pPXIT305SequenceSettings = reinterpret_cast<PXIT305SequenceSettings*>(this->GetPrev(pos));
			if(pPXIT305SequenceSettings!=NULL)
			{
				if (pPXIT305SequenceSettings->_moduleName ==  pPXIT305SequenceSettingsIn->_moduleName)
				{
					return TRUE;
				}
			}
		}
	}

	return FALSE;
}

void 
PXIT305SequenceSettingsList::
emptyTheList()
{
	PXIT305SequenceSettings* pPXIT305SequenceSettings;

	if (!IsEmpty())
	{
		POSITION pos;

		for (pos = this->GetTailPosition(); pos != NULL;)	//Start searching at the tail
		{
			pPXIT305SequenceSettings = reinterpret_cast <PXIT305SequenceSettings*> (this->GetPrev(pos)); //Select a pointer to this extension and move pos back by one position

			if(pPXIT305SequenceSettings!=NULL)
			{
				delete pPXIT305SequenceSettings;
				pPXIT305SequenceSettings = 0;
			}

		}
	}

	RemoveAll();
} 
//
///////////////////////////////////////////////////////////////////////////////

// Copyright (c) 2004 PXIT. All Rights Reserved
//
// FileName    : CGOperatingPoints.h
// Description : Declaration of CCGOperatingPoints class
//               Calculates and represents polynomial for coarse frequency
//               Calculates and represents all ITUOP Estimates
//               Finds and represents all final ITU OPs
//               Writes estimated and final ITU OPs to file
//
// Rev#  | Date         | Author               | Description of change
// ---------------------------------------------------------------------
// 0.1   | 01 Nov 2004  | Frank D'Arcy         | Initial Draft

#pragma once

#include "DSDBROperatingPoint.h"
#include "DSDBRSuperMode.h"
#include "ITUGrid.h"
#include <vector>

class CCGOperatingPoints
{
public:
	CCGOperatingPoints();
	~CCGOperatingPoints();

	typedef enum rtype
	{
		ok = 0,
		empty_poly,
		min_num_of_poly_coeffs_not_reached,
		max_num_of_poly_coeffs_exceeded,
		error_reading_power_meter,
		power_reading_too_low,
		power_ratio_out_of_range,
		could_not_open_file,
		file_does_not_exist,
		file_format_invalid,
		file_already_exists,
		not_enough_data,
		no_data,
		requested_num_of_sample_pts_not_found,
		error_reading_frequency_from_wavemeter,
		error_reading_temp_from_tec,
		temp_stability_not_reached,
		temp_stability_not_applied,
		invalid_frequency_from_wavemeter,
		error_applying_op,
		unstable_operating_points,
		too_many_samples,
		not_all_channels_found,
		sm_not_found,
		lm_not_found,
		index_not_found,
		error_getting_connection_state,
		error_getting_current_at_output,
		required_outputs_not_connected
	};

	typedef struct lm_freq_line_point_type
	{
		short sm;
		short lm;
		short i;
	};

	std::pair<short, std::string> get_error_pair( rtype error_in );

	void clear();

	rtype calcCoarseFreqPoly(
		short num_of_sample_pts, 
		short num_of_poly_coeffs,
		std::vector<double> &coarse_freq_poly,
		double* p_chi_squared );

	rtype getCoarseFreqPoly( std::vector<double> &coarse_freq_poly );
	rtype setCoarseFreqPoly( std::vector<double> &coarse_freq_poly );

	double getCoarseFreq( double power_ratio );
	rtype readCoarseFreq( double &coarse_freq );

	rtype loadDefaultCoarseFreqPoly();

	rtype findCoarseFreqSamplePoints( short num_of_sample_pts );

	rtype collectCoarseFreqSamplePoints();

	rtype estimateITUOperatingPoints();

	short countITUOPEstimatesFound();

	short countITUOPsFound();

	rtype measure_F_P_Pr(
		double& F,
		double& min_measured_freq,
		double& max_measured_freq,
		double& P,
		double& Pr,
		short num_F_samples_to_average,
		short num_P_and_Pr_samples_to_average);

	rtype applyStableOP(
		short sm,
		short lm,
		double index,
		short apply_Gain_SOA_of_SM_map );

	rtype applyIm(
		short sm,
		double index );

	rtype applyPointOnSMMap(
		short sm,
		double Im_index,
		double Ip_index,
		short apply_Gain_SOA_of_SM_map );

	rtype getPointOnSMMap(
		short sm,
		double* p_Im_index,
		double* p_Ip_index );


	rtype findITUOperatingPointsUsingWavemeter();

	rtype waitForTECSettling();

	rtype interpolationRoutine(
		double frequency_at_first_pt,
		double frequency_at_last_pt,
		std::vector<CDSDBRSupermodeMapLine>::iterator i_lm,
		std::vector<CDSDBRSuperMode>::iterator i_sm,
		short sm,
		short lm );

	typedef enum itusearch_scheme_type
	{
		original = 0, // this must be first
		quit_itupoint_after_problem,
		remove_upper_lower_after_problem,
		resample_upper_lower_after_problem,
		remove_upper_lower_find_N_samples_after_problem,
		bisection_search_after_problem,
		brute_force_search_after_problem,
		end_of_schemes // this must be last
	};

	typedef enum itusearch_nextguess_type
	{
		interpolation = 0,
		bisection
	};

	rtype newITUPointSearchRoutine(
		double frequency_at_first_pt,
		double min_measured_freq_at_first_pt,
		double max_measured_freq_at_first_pt,
		double P_at_first_pt,
		double Pr_at_first_pt,
		double frequency_at_last_pt,
		double min_measured_freq_at_last_pt,
		double max_measured_freq_at_last_pt,
		double P_at_last_pt,
		double Pr_at_last_pt,
		std::vector<CDSDBRSupermodeMapLine>::iterator i_lm,
		std::vector<CDSDBRSuperMode>::iterator i_sm,
		short sm,
		short lm );

	void findUpperAndLower(
	    double freq_of_channel_num,
		std::map<double,double> &freq_and_i,
		double &freq_upper,
		double &point_upper,
		double &freq_lower,
		double &point_lower);

	void removeUpperAndLower(
		double freq_of_channel_num,
		std::map<double,double> &freq_and_i,
		std::map<double,double> &i_and_min_measured_freq,
		std::map<double,double> &i_and_max_measured_freq,
		std::map<double,double> &i_and_P,
		std::map<double,double> &i_and_Pr);

	void removeUpperAndLowerButNotEndPoints(
		double freq_of_channel_num,
		double first_point_index,
		double last_point_index,
		std::map<double,double> &freq_and_i,
		std::map<double,double> &i_and_min_measured_freq,
		std::map<double,double> &i_and_max_measured_freq,
		std::map<double,double> &i_and_P,
		std::map<double,double> &i_and_Pr);

	rtype testNSamples(
		std::map<double,double> &freq_and_i,
		std::map<double,double> &i_and_min_measured_freq,
		std::map<double,double> &i_and_max_measured_freq,
		std::map<double,double> &i_and_P,
		std::map<double,double> &i_and_Pr,
		int number_of_samples,
		double first_point,
		double last_point,
		std::vector<CDSDBRSupermodeMapLine>::iterator i_lm,
		std::vector<CDSDBRSuperMode>::iterator i_sm,
		short sm,
		short lm );

	rtype findNearbyBoundaryCoords( std::vector<CDSDBROperatingPoint> &operating_points );

	rtype writeOPsToFile(
		CString abs_filepath,
		std::vector<CDSDBROperatingPoint> &operating_points,
		bool overwrite = false );

	rtype writeSamplePointsToFile(
		CString abs_filepath,
		bool overwrite = false );

	rtype writeFreqCoverageToFile(
		CString abs_filepath,
		bool overwrite );

	rtype writePolyCoeffsToFile(
		std::vector<double> &coarse_freq_poly,
		CString abs_filepath,
		bool overwrite );

	void setPercentComplete( short percent_complete );
	void setITUOPCount( short ITUOP_count );
	void incrementDuffITUOPCount();
	CRITICAL_SECTION *_p_data_protect;
	short *_p_percent_complete;
	short *_p_ITUOP_count;
	short *_p_duff_ITUOP_count;

	std::vector<CDSDBRSuperMode> *_p_supermodes;

	CString _results_dir_CString;
	CString _laser_id_CString;
	CString _date_time_stamp_CString;

	typedef struct lm_freq_line_point_type2
	{
		short sm;
		short lm;
		double i;
	};

	std::vector<double> _coarse_freq_poly;

	std::multimap<double,lm_freq_line_point_type> _Pr_pts_on_freq_lines;

	std::multimap<double,lm_freq_line_point_type2> _Freq_pts_on_freq_lines;

	std::vector<CDSDBROperatingPoint> _coarse_freq_sample_points;

	std::vector<CDSDBROperatingPoint> _estimation_itu_ops;

	std::vector<CDSDBROperatingPoint> _characterisation_itu_ops;

	CITUGrid _itugrid;
};


// Copyright (c) 2003 Tsunami Photonics Ltd. All Rights Reserved

///////////////////////////////////////////////////////////////////////////////
///
//	Class	:
//
//	Contents:
//
//	Author	:
//
///
///////////////////////////////////////////////////////////////////////////////
///
//
#ifndef __PXIT305SEQUENCESETTINGSLIST_H_
#define __PXIT305SEQUENCESETTINGSLIST_H_

#pragma once

#include "afxcoll.h"

#include "PXIT305SequenceSettings.h"

/////////////////////////////////////////////////////////////////////

class PXIT305SequenceSettingsList 
	: public CPtrList
{
public:
	PXIT305SequenceSettingsList(void);
	~PXIT305SequenceSettingsList(void);


									// Find a device given the handle
	PXIT305SequenceSettings*	findObjectByIndex(unsigned int index);

									// Find a device given the module name
	PXIT305SequenceSettings*	findObject(CString moduleName);

					// Insert at the head of the List
	bool	addToList(PXIT305SequenceSettings* pPXIT305SequenceSettings);

	bool	removeFromList(PXIT305SequenceSettings* pPXIT305SequenceSettings);

					// Is the device ptr in our list
	bool	inList(PXIT305SequenceSettings* pPXIT305SequenceSettings);


					// Destroy the List
					// by deleting all objects - 
					// Called on destruction
	void	emptyTheList();			

};
//
///////////////////////////////////////////////////////////////////////////////
#endif

// Copyright (c) 2004 PXIT. All Rights Reserved
//
// FileName    : CurrentsVector.h
// Description : Declaration of CCurrentsVector class
//               Contains laser currents vector
//               and utility routines
//
// Rev#  | Date          | Author               | Description of change
// --------------------------------------------------------------
// 0.1   | 09 Sept 2004  | Frank D'Arcy         | Initial Draft
//

#pragma once

#include <vector>

class CCurrentsVector
{
public:
	CCurrentsVector(void);

	~CCurrentsVector(void);

	typedef enum rcode
	{
		ok = 0,
		vector_size_rows_cols_do_not_match,
		could_not_open_file,
		file_does_not_exist,
		file_already_exists,
		file_format_invalid
	};

	rcode writeToFile( CString abs_filepath, bool overwrite = false );

	rcode readFromFile( CString abs_filepath );

	void clear();

	bool isEmpty();

	std::vector<double> _currents;
	long _length;

	CString _abs_filepath;
};

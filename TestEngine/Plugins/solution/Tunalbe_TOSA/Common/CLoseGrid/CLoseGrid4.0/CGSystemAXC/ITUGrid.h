// Copyright (c) 2006
//
// FileName    : ITUGrid.h
// Description : Declaration of CITUGrid class
//               Contains ITU Grid Definition
//
// Rev#  | Date          | Author               | Description of change
// --------------------------------------------------------------
// 0.1   | 04 Mar 2006   | Frank D'Arcy         | Initial Draft
//

#pragma once

#include <vector>

class CITUGrid
{
public:
	CITUGrid(void);

	~CITUGrid(void);

	typedef enum rcode
	{
		ok = 0,
		vector_size_rows_cols_do_not_match,
		could_not_open_file,
		file_does_not_exist,
		file_already_exists,
		file_format_invalid,
		no_itu_grid
	};

	rcode setup();
	rcode setupEx(CString abs_filepath);

	long channel_count();

	rcode findITUPtsInRange(
		std::vector<short> &channel_num_of_itu_pts_in_lm,
		std::vector<double> &freq_of_itu_pts_in_lm,
		double min_freq,
		double max_freq );

	rcode writeToFile( CString abs_filepath, bool overwrite = false );

	rcode readFromFile( CString abs_filepath );

	void clear();

	bool isEmpty();

	std::vector<double> _ITU_frequencies;
	long _length;

	CString _abs_filepath;
};

// Copyright (c) 2004 Tsunami Photonics Ltd. All Rights Reserved

///////////////////////////////////////////////////////////////////////////////
///
//	Class	:
//
//	Contents:
//
//	Author	:
//
///
///////////////////////////////////////////////////////////////////////////////
///
//
#ifndef __PXIT306SEQUENCESETTINGS_H_
#define __PXIT306SEQUENCESETTINGS_H_

#pragma once

using namespace std;

/////////////////////////////////////////////////////////////////////

class PXIT306SequenceSettings
{
public:
	PXIT306SequenceSettings(CString moduleName,
							short	inSequence,
							int		numPoints,
							short	sourceDelay,
							short	measureDelay);

	~PXIT306SequenceSettings(void);

	///////////////////////////////////////////////////////
	///
	//	Generic PXIT306SequenceSettings functions
	//

	///////////////////////////////////
	///
	//	Get and set functions
	//

	//
	///////////////////////////////////////////////////////

	CString		_moduleName;
	short		_inSequence;
	int			_numPoints;
	short		_sourceDelay;
	short		_measureDelay;
};
//
///////////////////////////////////////////////////////////////////////////////
#endif

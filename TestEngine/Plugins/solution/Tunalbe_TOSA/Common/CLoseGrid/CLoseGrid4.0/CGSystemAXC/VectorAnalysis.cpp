// Copyright (c) 2004 Tsunami Photonics Ltd. All Rights Reserved
//
// FileName    : VectorAnalysis.h
// Description : Definition of VectorAnalysis class
//
// Rev#  | Date          | Author               | Description of change
// --------------------------------------------------------------
// 0.1   | 11 Oct 2002   | Frank D'Arcy         | Initial Draft
// 0.2   | 26 Jul 2004   | Frank D'Arcy         | Ported to CG 4.0
//

#include "StdAfx.h"
#include "VectorAnalysis.h"

#include <math.h>
#include <set>
#include <map>
#include <algorithm>

#define new DEBUG_NEW

double
VectorAnalysis::derivative(
        double x1,
        double x2,
        double y1,
        double y2 )
{
    return (y2 - y1)/(x2 - x1);
}

double
VectorAnalysis::doubleDerivative(
        double x1,
        double x2,
        double x3,
        double y1,
        double y2,
        double y3 )
{
    return (derivative( x2, x3, y2, y3) - derivative( x1, x2, y1, y2))/(x2 - x1);
}



void
VectorAnalysis::medianFilter(
    std::vector<double> &x_in,
    std::vector<double> &filtered_x_out,
    unsigned int rank,
    unsigned long line_length )
{
    // The median filter is well documented in edge detection as
    // being suited to removing noise while preserving edges
    // whereas traditionally-used low pass filters effect the
    // edge so as to lower its slope.
    // This function is for a map where the filter is to be
    // applied to each ramp line in isolation from the next
    // Hence line_length tells us the length of each line

    // If the input is x[i] where i = 0, n-1
    // and the output is y[i] where i = 0, n-1
    // y[i] = median( x[i-r], ... x[i], ... x[i+r] )
    // where r is the rank of the filter
    // elements of x outside the range are treated as zero

    filtered_x_out.clear();

    if( line_length > 0
     && x_in.size() > 0
     && (x_in.size())%line_length == 0 )
    {
    // input data looks okay
    
    std::vector<double> subset_of_x;

    unsigned long point_count = 0;
    for( std::vector<double>::iterator i_x_in = x_in.begin();
        i_x_in != x_in.end();
        i_x_in++ )
    {
        // Iterate through each line in the map
        unsigned long point_in_line_count = point_count%line_length;

        // pick vector +/-rank about each sample 
        subset_of_x.clear();
        for( long j = (long)point_in_line_count-(long)rank;
            j<=(long)point_in_line_count+(long)rank;
            j++ )
        {
            if( j < 0 || j >= (long)line_length )
            { // outside bounds of current line, use current point as value
                subset_of_x.push_back(x_in[point_count]);
            }
            else
            { // within bounds of current line
                subset_of_x.push_back(x_in[(long)point_count-(long)point_in_line_count+j]);
            }
        }

        // The median of the sorted vector s[i] where i = 0, n-1 is defined as
        // s[(n-1)/2] where n is odd 
        // ( s[(n/2)-1] + s[n/2] ) / 2 where n is even
        // n is always odd in a median filter

        // Calculate the median for the subset of x and add it to the output
        std::sort(subset_of_x.begin(),subset_of_x.end());
        filtered_x_out.push_back( subset_of_x[rank] );

        point_count++;
    }

    }
    else
    { // input data is bad, see if() statement above
    }
}


void
VectorAnalysis::linearFit(
    std::vector<double> &x_in,
    std::vector<double> &y_in,
    double &slope,
    double &intercept
)
{
    // least squares linear regression
    // Given equal length vectors x and y
    // x is assumed noiseless and
    // y is to have normally distributed noise
    // A linear approximation is fitted to the data
    // y = slope*x + intercept
    slope = 0;
    intercept = 0;

    unsigned long N = (unsigned long)(x_in.size());
    if( N > 1 && N == y_in.size() )
    { // data looks okay

        double mean_x = meanOfValues( x_in );
        double mean_y = meanOfValues( y_in );

        double S_xx = 0;
        double S_xy = 0;
        for( unsigned long i = 0; i < N; i++ )
        {
            S_xx += ( x_in[i] - mean_x )*( x_in[i] - mean_x );
            S_xy += ( x_in[i] - mean_x )*( y_in[i] - mean_y );
        }

        slope = S_xy/S_xx;
        intercept = mean_y - slope*mean_x;
    }
    else
    { // data is no good
    }
}

void
VectorAnalysis::distancesFromPointsToLine(
    std::vector<double> &x_in,
    std::vector<double> &y_in,
    double &slope,
    double &intercept,
    std::vector<double> &distances
)
{
    // Calculate the distance from each point to a line

    distances.clear();
    unsigned long N = (unsigned long)(x_in.size());
    if( N > 1 && N == y_in.size() )
    { // data looks okay

        // find slope of perdendicular line through point
        double slope_p = tan( atan( slope ) - M_PI_2 );

        for( unsigned long i = 0; i < N; i++ )
        {
            if( slope == 0 )
            { // line is parallel to x-axis
                distances.push_back( y_in[i] - intercept );
            }
            else
            {
                // find intercept of perdendicular line through each point
                double intercept_p = y_in[i] - slope_p * x_in[i];

                // find intersection of perdendicular lines
                double x_on_line = (intercept - intercept_p)/(slope_p - slope);
                double y_on_line = slope*x_on_line + intercept;
                double distance = sqrt(
                    (x_in[i] - x_on_line)*(x_in[i] - x_on_line)
                  + (y_in[i] - y_on_line)*(y_in[i] - y_on_line) );

                distances.push_back( distance );
            }
        }

    }
}


double
VectorAnalysis::meanOfValues( std::vector<double> &values )
{
    double mean = 0;

	unsigned int count = 0;
    std::vector<double>::iterator iter = values.begin();
    while( iter!= values.end() )
    {
        mean += *iter;
        iter++;
        count++;
    }
    if( count > 1 )
    {
        mean /= count;
    }

	return mean;
}


// Copyright (c) 2003 Tsunami Photonics Ltd. All Rights Reserved

///////////////////////////////////////////////////////////////////////////////
///
//	Class	:
//
//	Contents:
//
//	Author	:
//
///
///////////////////////////////////////////////////////////////////////////////
///
//
#ifndef __PXIT306SEQUENCESETTINGSLIST_H_
#define __PXIT306SEQUENCESETTINGSLIST_H_

#pragma once

#include "afxcoll.h"

#include "PXIT306SequenceSettings.h"

/////////////////////////////////////////////////////////////////////

class PXIT306SequenceSettingsList 
	: public CPtrList
{
public:
	PXIT306SequenceSettingsList(void);
	~PXIT306SequenceSettingsList(void);


									// Find a device given the handle
	PXIT306SequenceSettings*	findObjectByIndex(unsigned int index);

									// Find a device given the module name
	PXIT306SequenceSettings*	findObject(CString moduleName);

					// Insert at the head of the List
	bool	addToList(PXIT306SequenceSettings* pPXIT306SequenceSettings);

	bool	removeFromList(PXIT306SequenceSettings* pPXIT306SequenceSettings);

					// Is the device ptr in our list
	bool	inList(PXIT306SequenceSettings* pPXIT306SequenceSettings);


					// Destroy the List
					// by deleting all objects - 
					// Called on destruction
	void	emptyTheList();			

};
//
///////////////////////////////////////////////////////////////////////////////
#endif

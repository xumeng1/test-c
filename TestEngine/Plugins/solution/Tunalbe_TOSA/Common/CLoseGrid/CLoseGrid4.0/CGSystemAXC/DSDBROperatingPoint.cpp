// Copyright (c) 2004 PXIT. All Rights Reserved
//
// FileName    : DSDBROperatingPoint.cpp
// Description : Definition of CDSDBROperatingPoint class
//
// Rev#  | Date          | Author               | Description of change
// --------------------------------------------------------------
// 0.1   | 01 Nov 2004  | Frank D'Arcy         | Initial Draft
//

#include "stdafx.h"
#include "DSDBROperatingPoint.h"
#include "CGResearchServerInterface.h"
#include "CGRegValues.h"
#include "defaults.h"

// constructor
CDSDBROperatingPoint::CDSDBROperatingPoint()
{
	clear();
}


CDSDBROperatingPoint::
CDSDBROperatingPoint(
	std::vector<CDSDBRSuperMode>::iterator i_sm,
	std::vector<CDSDBRSupermodeMapLine>::iterator i_lm_line,
	short sm_number,
	short lm_number,
	short i_of_pt_on_line )
{
	clear();

	long row_i_number = i_lm_line->_points[i_of_pt_on_line].row;
	long col_i_number = i_lm_line->_points[i_of_pt_on_line].col;

	short front_pair_i_number = i_lm_line->_points[i_of_pt_on_line].front_pair_number;
	double I_rear_i_number = i_lm_line->_points[i_of_pt_on_line].I_rear;
	double I_front_1_i_number = i_lm_line->_points[i_of_pt_on_line].I_front_1;
	double I_front_2_i_number = i_lm_line->_points[i_of_pt_on_line].I_front_2;
	double I_front_3_i_number = i_lm_line->_points[i_of_pt_on_line].I_front_3;
	double I_front_4_i_number = i_lm_line->_points[i_of_pt_on_line].I_front_4;
	double I_front_5_i_number = i_lm_line->_points[i_of_pt_on_line].I_front_5;
	double I_front_6_i_number = i_lm_line->_points[i_of_pt_on_line].I_front_6;
	double I_front_7_i_number = i_lm_line->_points[i_of_pt_on_line].I_front_7;
	double I_front_8_i_number = i_lm_line->_points[i_of_pt_on_line].I_front_8;
	double I_phase_i_number = i_lm_line->_points[i_of_pt_on_line].I_phase;

	//Add details of sample point to object in vector

	_row_on_sm_map = row_i_number;
	_col_on_sm_map = col_i_number;
	_Pr_on_sm_map = *(i_sm->_map_forward_power_ratio.iPt( row_i_number, col_i_number ));

	_sm_number = sm_number;
	_lm_number = lm_number;
	_index_on_middle_line_of_frequency = i_of_pt_on_line;
	_real_index_on_middle_line_of_frequency = (double)i_of_pt_on_line;

	_front_pair_number = front_pair_i_number;
	_I_gain = i_sm->_I_gain;
	_I_soa = i_sm->_I_soa;
	_I_rear = I_rear_i_number;
	_I_front_1 = I_front_1_i_number;
	_I_front_2 = I_front_2_i_number;
	_I_front_3 = I_front_3_i_number;
	_I_front_4 = I_front_4_i_number;
	_I_front_5 = I_front_5_i_number;
	_I_front_6 = I_front_6_i_number;
	_I_front_7 = I_front_7_i_number;
	_I_front_8 = I_front_8_i_number;
	_I_phase = I_phase_i_number;
}



CDSDBROperatingPoint::
CDSDBROperatingPoint(
	std::vector<CDSDBRSuperMode>::iterator i_sm,
	std::vector<CDSDBRSupermodeMapLine>::iterator i_lm_line,
	short sm_number,
	short lm_number,
	double interpolatable_i_of_pt_on_line )
{
	clear();

	short i_of_pt_on_line = (short)interpolatable_i_of_pt_on_line;

	long row_i_number = i_lm_line->_points[i_of_pt_on_line].row;
	long col_i_number = i_lm_line->_points[i_of_pt_on_line].col;

	double row_between_ab = (double)row_i_number;
	double col_between_ab = (double)col_i_number;

	if( i_of_pt_on_line+1 < (short)(i_lm_line->_points.size()) )
	{
		long row_a = i_lm_line->_points[i_of_pt_on_line].row;
		long col_a = i_lm_line->_points[i_of_pt_on_line].col;
		long row_b = i_lm_line->_points[i_of_pt_on_line+1].row;
		long col_b = i_lm_line->_points[i_of_pt_on_line+1].col;

		double fraction = interpolatable_i_of_pt_on_line-(double)i_of_pt_on_line;
		row_between_ab = (double)row_a + fraction*( (double)row_b - (double)row_a );
		col_between_ab = (double)col_a + fraction*( (double)col_b - (double)col_a );
	}

	CDSDBRSupermodeMapLine::point_type pt_between_ab = i_sm->interpolateLinePoint(
		row_between_ab,
		col_between_ab );

	_row_on_sm_map = pt_between_ab.row;
	_col_on_sm_map = pt_between_ab.col;
	_Pr_on_sm_map = *(i_sm->_map_forward_power_ratio.iPt( _row_on_sm_map, _col_on_sm_map ));

	_sm_number = sm_number;
	_lm_number = lm_number;
	_index_on_middle_line_of_frequency = i_of_pt_on_line;
	_real_index_on_middle_line_of_frequency = interpolatable_i_of_pt_on_line;

	_front_pair_number = pt_between_ab.front_pair_number;
	_I_gain = i_sm->_I_gain;
	_I_soa = i_sm->_I_soa;
	_I_rear = pt_between_ab.I_rear;
	_I_front_1 = pt_between_ab.I_front_1;
	_I_front_2 = pt_between_ab.I_front_2;
	_I_front_3 = pt_between_ab.I_front_3;
	_I_front_4 = pt_between_ab.I_front_4;
	_I_front_5 = pt_between_ab.I_front_5;
	_I_front_6 = pt_between_ab.I_front_6;
	_I_front_7 = pt_between_ab.I_front_7;
	_I_front_8 = pt_between_ab.I_front_8;
	_I_phase = pt_between_ab.I_phase;

}


CDSDBROperatingPoint::
CDSDBROperatingPoint(
	std::vector<CDSDBRSuperMode>::iterator i_sm,
	short sm_number,
	double index_of_Im,
	double index_of_Ip )
{
	clear();

	double row = 0;
	double col = 0;

	if( i_sm->_Im_ramp )
	{
		row = index_of_Ip;
		col = index_of_Im;
	}
	else
	{
		row = index_of_Im;
		col = index_of_Ip;
	}

	CDSDBRSupermodeMapLine::point_type pt_on_sm_map = i_sm->interpolateLinePoint(
		row,
		col );

	_row_on_sm_map = pt_on_sm_map.row;
	_col_on_sm_map = pt_on_sm_map.col;
	_Pr_on_sm_map = *(i_sm->_map_forward_power_ratio.iPt( _row_on_sm_map, _col_on_sm_map ));

	_sm_number = sm_number;
	_lm_number = -1;
	_index_on_middle_line_of_frequency = -1;
	_real_index_on_middle_line_of_frequency = -1;

	_front_pair_number = pt_on_sm_map.front_pair_number;
	_I_gain = i_sm->_I_gain;
	_I_soa = i_sm->_I_soa;
	_I_rear = pt_on_sm_map.I_rear;
	_I_front_1 = pt_on_sm_map.I_front_1;
	_I_front_2 = pt_on_sm_map.I_front_2;
	_I_front_3 = pt_on_sm_map.I_front_3;
	_I_front_4 = pt_on_sm_map.I_front_4;
	_I_front_5 = pt_on_sm_map.I_front_5;
	_I_front_6 = pt_on_sm_map.I_front_6;
	_I_front_7 = pt_on_sm_map.I_front_7;
	_I_front_8 = pt_on_sm_map.I_front_8;
	_I_phase = pt_on_sm_map.I_phase;
}

void
CDSDBROperatingPoint::clear()
{
	_ITU_channel_number = 0;
	_ITU_frequency = 0;

	_sm_number = 0;
	_lm_number = 0;
	_index_on_middle_line_of_frequency = 0;
	_real_index_on_middle_line_of_frequency = 0;

	_row_on_sm_map = 0;
	_col_on_sm_map = 0;
	_Pr_on_sm_map = 0;

	_front_pair_number = 0;
	_I_gain = 0;
	_I_soa = 0;
	_I_rear = 0;
	_I_front_1 = 0;
	_I_front_2 = 0;
	_I_front_3 = 0;
	_I_front_4 = 0;
	_I_front_5 = 0;
	_I_front_6 = 0;
	_I_front_7 = 0;
	_I_front_8 = 0;
	_I_phase = 0;

	_P_ratio_measured = 0;
	_F_measured = 0;
	_F_coarse = 0;
	_P_measured = 0;
	_stable_F_measurement = false;
	_is_an_ITU_point = false;

	_row_of_boundary_above = 0;
	_col_of_boundary_above = 0;
	_row_of_boundary_below = 0;
	_col_of_boundary_below = 0;
	_row_of_boundary_to_left = 0;
	_col_of_boundary_to_left = 0;
	_row_of_boundary_to_right = 0;
	_col_of_boundary_to_right = 0;
}

// destructor
CDSDBROperatingPoint::~CDSDBROperatingPoint()
{
}


CDSDBROperatingPoint::rtype
CDSDBROperatingPoint::apply()
{
	rtype rval = ok;

	CGR_LOG("CDSDBROperatingPoint::apply() entered",DIAGNOSTIC_CGR_LOG)

	// Step 1: Check for input power to prove laser is lasing
	double optical_power_test = 0;
	HRESULT hres = SERVERINTERFACE->readPhotodiodeCurrent(
		CG_REG->get_DSDBR01_CMDC_306II_module_name(),
		CG_REG->get_DSDBR01_CMDC_306II_direct_channel(),
		&optical_power_test );

	if( hres == S_OK )
	{
		if( optical_power_test >= MIN_DIRECT_OPTICAL_POWER )
		{
			// Step 2: Check if required front sections are connected
			// If not, ramp down and disconnect all front sections
			// and connect the required front sections

			CString constant_front_module_name;
			CString nonconstant_front_module_name;
			double constant_I_front;
			double nonconstant_I_front;
			switch( _front_pair_number )
			{
			case 1:
				constant_front_module_name = CG_REG->get_DSDBR01_CMDC_front1_module_name();
				constant_I_front = _I_front_1;
				nonconstant_front_module_name = CG_REG->get_DSDBR01_CMDC_front2_module_name();
				nonconstant_I_front = _I_front_2;
				break;
			case 2:
				constant_front_module_name = CG_REG->get_DSDBR01_CMDC_front2_module_name();
				constant_I_front = _I_front_2;
				nonconstant_front_module_name = CG_REG->get_DSDBR01_CMDC_front3_module_name();
				nonconstant_I_front = _I_front_3;
				break;
			case 3:
				constant_front_module_name = CG_REG->get_DSDBR01_CMDC_front3_module_name();
				constant_I_front = _I_front_3;
				nonconstant_front_module_name = CG_REG->get_DSDBR01_CMDC_front4_module_name();
				nonconstant_I_front = _I_front_4;
				break;
			case 4:
				constant_front_module_name = CG_REG->get_DSDBR01_CMDC_front4_module_name();
				constant_I_front = _I_front_4;
				nonconstant_front_module_name = CG_REG->get_DSDBR01_CMDC_front5_module_name();
				nonconstant_I_front = _I_front_5;
				break;
			case 5:
				constant_front_module_name = CG_REG->get_DSDBR01_CMDC_front5_module_name();
				constant_I_front = _I_front_5;
				nonconstant_front_module_name = CG_REG->get_DSDBR01_CMDC_front6_module_name();
				nonconstant_I_front = _I_front_6;
				break;
			case 6:
				constant_front_module_name = CG_REG->get_DSDBR01_CMDC_front6_module_name();
				constant_I_front = _I_front_6;
				nonconstant_front_module_name = CG_REG->get_DSDBR01_CMDC_front7_module_name();
				nonconstant_I_front = _I_front_7;
				break;
			case 7:
				constant_front_module_name = CG_REG->get_DSDBR01_CMDC_front7_module_name();
				constant_I_front = _I_front_7;
				nonconstant_front_module_name = CG_REG->get_DSDBR01_CMDC_front8_module_name();
				nonconstant_I_front = _I_front_8;
				break;
			default:
				rval = rtype::invalid_data;
			}

			if( rval == rtype::ok )
			{
				short constant_connection;
				short nonconstant_connection;
				hres = SERVERINTERFACE->getOutputConnection(
					constant_front_module_name,
					&constant_connection);
				if( hres == S_OK )
				hres = SERVERINTERFACE->getOutputConnection(
					nonconstant_front_module_name,
					&nonconstant_connection);

				if( hres == S_OK )
				{
					if( constant_connection != CONNECT_OUTPUT || nonconstant_connection != CONNECT_OUTPUT )
					{
						// need to change front section pair,
						// ramp down and disconnect existing front current sections

						short output_connection_state = DISCONNECT_OUTPUT;
						SERVERINTERFACE->getOutputConnection( CG_REG->get_DSDBR01_CMDC_front8_module_name(), &output_connection_state );
						if( output_connection_state != DISCONNECT_OUTPUT )
						{
							SERVERINTERFACE->setCurrent( CG_REG->get_DSDBR01_CMDC_front8_module_name() , 0 );
							SERVERINTERFACE->setOutputConnection( CG_REG->get_DSDBR01_CMDC_front8_module_name() , DISCONNECT_OUTPUT );
						}

						output_connection_state = DISCONNECT_OUTPUT;
						SERVERINTERFACE->getOutputConnection( CG_REG->get_DSDBR01_CMDC_front7_module_name(), &output_connection_state );
						if( output_connection_state != DISCONNECT_OUTPUT )
						{
							SERVERINTERFACE->setCurrent( CG_REG->get_DSDBR01_CMDC_front7_module_name() , 0 );
							SERVERINTERFACE->setOutputConnection( CG_REG->get_DSDBR01_CMDC_front7_module_name() , DISCONNECT_OUTPUT );
						}

						output_connection_state = DISCONNECT_OUTPUT;
						SERVERINTERFACE->getOutputConnection( CG_REG->get_DSDBR01_CMDC_front6_module_name(), &output_connection_state );
						if( output_connection_state != DISCONNECT_OUTPUT )
						{
							SERVERINTERFACE->setCurrent( CG_REG->get_DSDBR01_CMDC_front6_module_name() , 0 );
							SERVERINTERFACE->setOutputConnection( CG_REG->get_DSDBR01_CMDC_front6_module_name() , DISCONNECT_OUTPUT );
						}

						output_connection_state = DISCONNECT_OUTPUT;
						SERVERINTERFACE->getOutputConnection( CG_REG->get_DSDBR01_CMDC_front5_module_name(), &output_connection_state );
						if( output_connection_state != DISCONNECT_OUTPUT )
						{
							SERVERINTERFACE->setCurrent( CG_REG->get_DSDBR01_CMDC_front5_module_name() , 0 );
							SERVERINTERFACE->setOutputConnection( CG_REG->get_DSDBR01_CMDC_front5_module_name() , DISCONNECT_OUTPUT );
						}

						output_connection_state = DISCONNECT_OUTPUT;
						SERVERINTERFACE->getOutputConnection( CG_REG->get_DSDBR01_CMDC_front4_module_name(), &output_connection_state );
						if( output_connection_state != DISCONNECT_OUTPUT )
						{
							SERVERINTERFACE->setCurrent( CG_REG->get_DSDBR01_CMDC_front4_module_name() , 0 );
							SERVERINTERFACE->setOutputConnection( CG_REG->get_DSDBR01_CMDC_front4_module_name() , DISCONNECT_OUTPUT );
						}

						output_connection_state = DISCONNECT_OUTPUT;
						SERVERINTERFACE->getOutputConnection( CG_REG->get_DSDBR01_CMDC_front3_module_name(), &output_connection_state );
						if( output_connection_state != DISCONNECT_OUTPUT )
						{
							SERVERINTERFACE->setCurrent( CG_REG->get_DSDBR01_CMDC_front3_module_name() , 0 );
							SERVERINTERFACE->setOutputConnection( CG_REG->get_DSDBR01_CMDC_front3_module_name() , DISCONNECT_OUTPUT );
						}

						output_connection_state = DISCONNECT_OUTPUT;
						SERVERINTERFACE->getOutputConnection( CG_REG->get_DSDBR01_CMDC_front2_module_name(), &output_connection_state );
						if( output_connection_state != DISCONNECT_OUTPUT )
						{
							SERVERINTERFACE->setCurrent( CG_REG->get_DSDBR01_CMDC_front2_module_name() , 0 );
							SERVERINTERFACE->setOutputConnection( CG_REG->get_DSDBR01_CMDC_front2_module_name() , DISCONNECT_OUTPUT );
						}

						output_connection_state = DISCONNECT_OUTPUT;
						SERVERINTERFACE->getOutputConnection( CG_REG->get_DSDBR01_CMDC_front1_module_name(), &output_connection_state );
						if( output_connection_state != DISCONNECT_OUTPUT )
						{
							SERVERINTERFACE->setCurrent( CG_REG->get_DSDBR01_CMDC_front1_module_name() , 0 );
							SERVERINTERFACE->setOutputConnection( CG_REG->get_DSDBR01_CMDC_front1_module_name() , DISCONNECT_OUTPUT );
						}
					}

					if( rval == rtype::ok )
					{
						// Step 3: Apply operating point currents
						if( hres == S_OK && _I_gain > 0 )
						{
							hres = SERVERINTERFACE->setOutputConnection( CG_REG->get_DSDBR01_CMDC_gain_module_name() , CONNECT_OUTPUT );
							if( hres == S_OK ) hres = SERVERINTERFACE->setCurrent( CG_REG->get_DSDBR01_CMDC_gain_module_name() , _I_gain );
						}

						if( hres == S_OK && _I_soa > 0  )
						{
							hres = SERVERINTERFACE->setOutputConnection( CG_REG->get_DSDBR01_CMDC_soa_module_name() , CONNECT_OUTPUT );
							if( hres == S_OK ) hres = SERVERINTERFACE->setCurrent( CG_REG->get_DSDBR01_CMDC_soa_module_name() , _I_soa );
						}

						if( hres == S_OK ) hres = SERVERINTERFACE->setOutputConnection( CG_REG->get_DSDBR01_CMDC_rear_module_name() , CONNECT_OUTPUT );
						if( hres == S_OK ) hres = SERVERINTERFACE->setCurrent( CG_REG->get_DSDBR01_CMDC_rear_module_name() , _I_rear );

						if( hres == S_OK ) hres = SERVERINTERFACE->setOutputConnection( constant_front_module_name , CONNECT_OUTPUT );
						if( hres == S_OK ) hres = SERVERINTERFACE->setCurrent( constant_front_module_name , constant_I_front );

						if( hres == S_OK ) hres = SERVERINTERFACE->setOutputConnection( nonconstant_front_module_name , CONNECT_OUTPUT );
						if( hres == S_OK ) hres = SERVERINTERFACE->setCurrent( nonconstant_front_module_name , nonconstant_I_front );

						if( hres == S_OK ) hres = SERVERINTERFACE->setOutputConnection( CG_REG->get_DSDBR01_CMDC_phase_module_name() , CONNECT_OUTPUT );
						if( hres == S_OK ) hres = SERVERINTERFACE->setCurrent( CG_REG->get_DSDBR01_CMDC_phase_module_name() , _I_phase );

						if( hres != S_OK )
						{
							rval = rtype::error_setting_current;
							CGR_LOG("CDSDBROperatingPoint::apply() error_setting_current",ERROR_CGR_LOG)
							CString err_msg("CDSDBROperatingPoint::apply()");
							err_msg.AppendFormat(" hres = 0x%08x",hres);
							CGR_LOG(err_msg,ERROR_CGR_LOG)
						}
					}
				}
				else
				{
					rval = rtype::error_reading_output_connection;
					CGR_LOG("CDSDBROperatingPoint::apply() error_reading_output_connection",ERROR_CGR_LOG)
					CString err_msg("CDSDBROperatingPoint::apply()");
					err_msg.AppendFormat(" hres = 0x%08x",hres);
					CGR_LOG(err_msg,ERROR_CGR_LOG)
				}
			}
		}
		else
		{ // Laser might not be lasing
			rval = rtype::min_optical_power_not_reached;
			CGR_LOG("CDSDBROperatingPoint::apply() min_optical_power_not_reached",ERROR_CGR_LOG)
		}
	}
	else
	{
		rval = rtype::error_reading_optical_power;
		CGR_LOG("CDSDBROperatingPoint::apply() error_reading_optical_power",ERROR_CGR_LOG)
		CString err_msg("CDSDBROperatingPoint::apply()");
		err_msg.AppendFormat(" hres = 0x%08x",hres);
		CGR_LOG(err_msg,ERROR_CGR_LOG)
	}

	CGR_LOG("CDSDBROperatingPoint::apply() exiting",DIAGNOSTIC_CGR_LOG)
	CString error_msg("CDSDBROperatingPoint::apply()");
	error_msg.AppendFormat(" rval = %d",(int)rval);
	CGR_LOG(error_msg,DIAGNOSTIC_CGR_LOG)

	return rval;
}



// Copyright (c) 2004 PXIT. All Rights Reserved
//
// FileName    : CLoseGridSystemAXCPpg.cpp
// Description : Implementation of the CCLoseGridSystemAXCPropPage property page class.
//
// Rev#  | Date          | Author               | Description of change
// --------------------------------------------------------------
// 0.1   | 12 July 2004   | Frank D'Arcy         | Initial Draft
//

#include "stdafx.h"
#include "CGSystemAXC.h"
#include "CLoseGridSystemAXCPpg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


IMPLEMENT_DYNCREATE(CCLoseGridSystemAXCPropPage, COlePropertyPage)



// Message map

BEGIN_MESSAGE_MAP(CCLoseGridSystemAXCPropPage, COlePropertyPage)
END_MESSAGE_MAP()



// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CCLoseGridSystemAXCPropPage, "CGSYSTEMAXC.CLoseGridSystePropPage.1",
	0xd09f0f1a, 0x375, 0x41fc, 0xae, 0x1c, 0xa3, 0x84, 0x2f, 0xae, 0xaa, 0x55)



// CCLoseGridSystemAXCPropPage::CCLoseGridSystemAXCPropPageFactory::UpdateRegistry -
// Adds or removes system registry entries for CCLoseGridSystemAXCPropPage

BOOL CCLoseGridSystemAXCPropPage::CCLoseGridSystemAXCPropPageFactory::UpdateRegistry(BOOL bRegister)
{
	if (bRegister)
		return AfxOleRegisterPropertyPageClass(AfxGetInstanceHandle(),
			m_clsid, IDS_CGSYSTEMAXC_PPG);
	else
		return AfxOleUnregisterClass(m_clsid, NULL);
}



// CCLoseGridSystemAXCPropPage::CCLoseGridSystemAXCPropPage - Constructor

CCLoseGridSystemAXCPropPage::CCLoseGridSystemAXCPropPage() :
	COlePropertyPage(IDD, IDS_CGSYSTEMAXC_PPG_CAPTION)
{
}



// CCLoseGridSystemAXCPropPage::DoDataExchange - Moves data between page and properties

void CCLoseGridSystemAXCPropPage::DoDataExchange(CDataExchange* pDX)
{
	DDP_PostProcessing(pDX);
}



// CCLoseGridSystemAXCPropPage message handlers

// Copyright (c) 2002 Tsunami Photonics Ltd. All Rights Reserved

///////////////////////////////////////////////////////////////////////////////
///
//	Class	:
//
//	Contents:
//
//	Author	:
//
///
///////////////////////////////////////////////////////////////////////////////
///
//
#include "stdafx.h"

#include "PXIT306SequenceSettingsList.h"

#define new DEBUG_NEW

/////////////////////////////////////////////////////////////////////

PXIT306SequenceSettingsList::
PXIT306SequenceSettingsList()
{
}

PXIT306SequenceSettingsList::
~PXIT306SequenceSettingsList(void)
{
	emptyTheList();
}

/////////////////////////////////////////////////////////////////////

PXIT306SequenceSettings* 
PXIT306SequenceSettingsList::
findObjectByIndex(unsigned int index)
{
	PXIT306SequenceSettings* pPXIT306SequenceSettings = NULL;

	if (!IsEmpty())
	{
		POSITION pos;

		int i=0;
		for(pos = this->GetHeadPosition(); pos !=NULL;)
		{
			pPXIT306SequenceSettings = reinterpret_cast<PXIT306SequenceSettings*>(this->GetNext(pos));
			if(pPXIT306SequenceSettings!=NULL)
			{
				if (i == index)
				{
					return pPXIT306SequenceSettings;
				}
			}

			i++;
		}
	}

	return NULL;
}

/////////////////////////////////////////////////////////////////////

PXIT306SequenceSettings* 
PXIT306SequenceSettingsList::
findObject(CString	moduleName)
{
	PXIT306SequenceSettings* pPXIT306SequenceSettings = NULL;

	if (!IsEmpty())
	{
		POSITION pos;

		for(pos = this->GetTailPosition(); pos !=NULL;)
		{
			pPXIT306SequenceSettings = reinterpret_cast<PXIT306SequenceSettings*>(this->GetPrev(pos));
			if(pPXIT306SequenceSettings!=NULL)
			{
				if (pPXIT306SequenceSettings->_moduleName == moduleName)
				{	
					return pPXIT306SequenceSettings;
				}
			}
		}
	}

	return NULL;
}

/////////////////////////////////////////////////////////////////////

bool 
PXIT306SequenceSettingsList::
addToList(PXIT306SequenceSettings* pPXIT306SequenceSettings)
{
	bool success = true;

	this->AddHead(pPXIT306SequenceSettings);

	return success;
}

/////////////////////////////////////////////////////////////////////

bool 
PXIT306SequenceSettingsList::
removeFromList(PXIT306SequenceSettings* pPXIT306SequenceSettingsIn)
{
	bool success = FALSE;

	if(inList(pPXIT306SequenceSettingsIn))
	{
		POSITION pos;

		for(pos = this->GetTailPosition(); pos !=NULL;)
		{
			PXIT306SequenceSettings* pPXIT306SequenceSettings = NULL;
			pPXIT306SequenceSettings = reinterpret_cast<PXIT306SequenceSettings*>(this->GetPrev(pos));
			if(pPXIT306SequenceSettings!=NULL)
			{
				if (pPXIT306SequenceSettings->_moduleName ==  pPXIT306SequenceSettingsIn->_moduleName)
				{
					this->RemoveAt(pos);
					success = TRUE;
				}
			}
		}
	}
	else
	{
		success = FALSE;
	}

	return success;
}

/////////////////////////////////////////////////////////////////////

bool 
PXIT306SequenceSettingsList::
inList(PXIT306SequenceSettings *pPXIT306SequenceSettingsIn)
{
	PXIT306SequenceSettings *pPXIT306SequenceSettings = NULL;

	if (!IsEmpty())
	{
		POSITION pos;

		for(pos = this->GetTailPosition(); pos !=NULL;)
		{
			pPXIT306SequenceSettings = reinterpret_cast<PXIT306SequenceSettings*>(this->GetPrev(pos));
			if(pPXIT306SequenceSettings!=NULL)
			{
				if (pPXIT306SequenceSettings->_moduleName ==  pPXIT306SequenceSettingsIn->_moduleName)
				{
					return TRUE;
				}
			}
		}
	}

	return FALSE;
}

void 
PXIT306SequenceSettingsList::
emptyTheList()
{
	PXIT306SequenceSettings* pPXIT306SequenceSettings;

	if (!IsEmpty())
	{
		POSITION pos;

		for (pos = this->GetTailPosition(); pos != NULL;)	//Start searching at the tail
		{
			pPXIT306SequenceSettings = reinterpret_cast <PXIT306SequenceSettings*> (this->GetPrev(pos)); //Select a pointer to this extension and move pos back by one position

			if(pPXIT306SequenceSettings!=NULL)
			{
				delete pPXIT306SequenceSettings;
				pPXIT306SequenceSettings = 0;
			}

		}
	}

	RemoveAll();
} 
//
///////////////////////////////////////////////////////////////////////////////

// Copyright (c) 2003 Tsunami Photonics Ltd. All Rights Reserved

///////////////////////////////////////////////////////////////////////////////
///
//	Class	:
//
//	Contents:
//
//	Author	:
//
///
///////////////////////////////////////////////////////////////////////////////
///
//
#include "stdafx.h"

#include "SequenceRunSettings.h"

#define new DEBUG_NEW

/////////////////////////////////////////////////////////////////////

//
///	ctor and dtor
//
SequenceRunSettings::
SequenceRunSettings(int	runNumber)
{
	_runNumber			= runNumber;

	_PXIT305SequenceSettingsList = new PXIT305SequenceSettingsList();
	_PXIT306SequenceSettingsList = new PXIT306SequenceSettingsList();
}

SequenceRunSettings::
~SequenceRunSettings(void)
{
	if(_PXIT305SequenceSettingsList != NULL)
	{
		delete _PXIT305SequenceSettingsList;
		_PXIT305SequenceSettingsList = NULL;
	}

	if(_PXIT306SequenceSettingsList != NULL)
	{
		delete _PXIT306SequenceSettingsList;
		_PXIT306SequenceSettingsList = NULL;
	}

}

/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
///
//	Get and set functions
//

//
//
///	End of SequenceRunSettings.cpp
///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2004 PXIT. All Rights Reserved
//
// FileName    : LaserModeMap.h
// Description : Declaration of CLaserModeMap class
//               Contains data for a laser mode map
//               and utility routines
//
// Rev#  | Date          | Author               | Description of change
// --------------------------------------------------------------
// 0.1   | 22 July 2004   | Frank D'Arcy         | Initial Draft
//

#pragma once

#include <vector>
#include <map>

// structure to hold detected boundary jumps
// This stores unsigned long pair values
// The first value is the index of a point before a jump
// The second value is the index of a point after a jump
typedef std::multimap<unsigned long,unsigned long> boundary_multimap_typedef;


class CLaserModeMap
{
public:
	CLaserModeMap(void);

	CLaserModeMap( std::vector<double> map,
				   long rows,
				   long cols );

	~CLaserModeMap(void);

	typedef enum rcode
	{
		ok = 0,
		vector_size_rows_cols_do_not_match,
		could_not_open_file,
		file_does_not_exist,
		file_already_exists,
		file_format_invalid
	};

	rcode writeToFile( CString abs_filepath, bool overwrite = false );

	rcode readFromFile( CString abs_filepath );

	rcode setMap( std::vector<double>& map,
				  long rows,
				  long cols );

	void clear();

	bool isEmpty();


	std::vector<double>::iterator iPt( long row, long col );

	rcode writeJumpsToFile( CString abs_filepath );

	rcode writeLinkedJumpsToFile( CString abs_filepath );

	unsigned long boundaryPoint(boundary_multimap_typedef::iterator iter_b);

	std::vector<double> _map;
	long _rows;
	long _cols;
	CString _abs_filepath;

	//typedef std::multimap<unsigned long,unsigned long> boundary_multimap_typedef;
	// This stores unsigned long pair values
	// In this usage, the first value is point on the map (row*ramp_length+col)
	// The second value is the boundary line number, starting at line 0
	boundary_multimap_typedef _boundary_points;
	boundary_multimap_typedef _linked_boundary_points;
};

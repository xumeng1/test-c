// Copyright (c) 2004 PXIT. All Rights Reserved
//
// FileName    : CGOperatingPoints.cpp
// Description : Definition of CCGOperatingPoints class
//               Calculates and represents polynomial for coarse frequency
//               Calculates and represents all ITUOP Estimates
//               Finds and represents all final ITU OPs
//               Writes estimated and final ITU OPs to file
//
//
// Rev#  | Date         | Author               | Description of change
// ---------------------------------------------------------------------
// 0.1   | 01 Nov 2004  | Frank D'Arcy         | Initial Draft


#include "StdAfx.h"
#include "CGOperatingPoints.h"
#include "CGResearchServerInterface.h"
#include "CGRegValues.h"
#include <fstream>
#include <algorithm>
#include "CGSystemErrors.h"
#include <direct.h>
#include <math.h>

extern "C"
{
#include "nrsvdfit.h"
}

#define MAX_ITERATIONS_WITH_PROBLEM 5
#define MAX_ITERATIONS_WITHOUT_PROBLEM 20

extern bool g_prevent_front_section_switch;

CCGOperatingPoints::CCGOperatingPoints()
{
	_p_data_protect = NULL;
	_p_percent_complete = NULL;
	_p_ITUOP_count = NULL;
	_p_duff_ITUOP_count = NULL;

	clear();
	_coarse_freq_poly.clear();
	_coarse_freq_poly.push_back(DEFAULT_CFREQ_POLY_COEFF_0);
	_coarse_freq_poly.push_back(DEFAULT_CFREQ_POLY_COEFF_1);
}

CCGOperatingPoints::~CCGOperatingPoints()
{
	clear();
	_coarse_freq_poly.clear();
}

void
CCGOperatingPoints::clear()
{
	// Note: don't clear _coarse_freq_poly from one run to the next,
	// let user decide when to change it

	_coarse_freq_sample_points.clear();
	_estimation_itu_ops.clear();
	_characterisation_itu_ops.clear();
	_Freq_pts_on_freq_lines.clear();
	_itugrid.clear();

	return;
}


std::pair<short, std::string>
CCGOperatingPoints::get_error_pair( rtype error_in )
{
	std::pair<short, std::string> return_error_pair;

	switch( error_in )
	{
		case empty_poly:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_COARSEFREQPOLYINVALID_ERROR )
			break;
		case min_num_of_poly_coeffs_not_reached:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_COARSEFREQPOLYINVALID_ERROR )
			break;
		case max_num_of_poly_coeffs_exceeded:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_COARSEFREQPOLYINVALID_ERROR )
			break;
		case error_reading_power_meter:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_POWER_READ_ERROR )
			break;
		case power_reading_too_low:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_POWER_READING_TOO_LOW_ERROR )
			break;
		case power_ratio_out_of_range:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_POWER_RATIO_OUT_OF_RANGE_ERROR )
			break;
		case could_not_open_file:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_FILEOPEN_ERROR )
			break;
		case file_does_not_exist:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_FILE_DOES_NOT_EXIST_ERROR )
			break;
		case file_already_exists:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_FILEOPEN_ERROR )
			break;
		case file_format_invalid:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_INVALID_FILE_FORMAT_ERROR )
			break;
		case not_enough_data:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_COARSEFREQPOLYDATACOLLECTION_ERROR )
			break;
		case no_data:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_COARSEFREQPOLYDATACOLLECTION_ERROR )
			break;
		case requested_num_of_sample_pts_not_found:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_COARSEFREQPOLYDATACOLLECTION_ERROR )
			break;
		case error_reading_frequency_from_wavemeter:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_WAVEMETER_MEASUREMENT_ERROR )
			break;
		case invalid_frequency_from_wavemeter:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_WAVEMETER_MEASUREMENT_ERROR )
			break;
		case error_applying_op:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_OPERATION_POINT_APPLY_ERROR )
			break;
		case unstable_operating_points:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_UNSTABLE_OPERATION_POINTS_ERROR )
			break;
		case too_many_samples:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_COARSEFREQMAXSAMPLENUM_ERROR )
			break;
		case not_all_channels_found:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_ITUGRID_NOT_ALL_CHANNELS_FOUND_ERROR )
			break;
		case sm_not_found:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_SM_NOT_FOUND_ERROR )
			break;
		case lm_not_found:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_LM_NOT_FOUND_ERROR )
			break;
		case index_not_found:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_INDEX_NOT_FOUND_ERROR )
			break;
		case error_getting_connection_state:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_GET_OUTPUT_CONNECTION_ERROR )
			break;
		case required_outputs_not_connected:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_REQUIRED_OUTPUTS_NOT_CONNECTED_ERROR )
			break;
		case error_getting_current_at_output:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_GET_CURRENT_ERROR )
			break;
		default:
			CGSYSTEM_ERROR_SET_A_EQUAL_TO_B( return_error_pair, CGSYSTEM_UNKNOWN_ERROR )
	}

	return return_error_pair;
}




CCGOperatingPoints::rtype
CCGOperatingPoints::calcCoarseFreqPoly(
	short num_of_sample_pts, 
	short num_of_poly_coeffs,
	std::vector<double> &coarse_freq_poly,
	double* p_chi_squared )
{
	rtype rval = rtype::ok;

	CGR_LOG(CString("CCGOperatingPoints::calcCoarseFreqPoly() entered"),DIAGNOSTIC_CGR_LOG)

	std::vector<double> pr_sample_values;
	std::vector<double> freq_sample_values;
	pr_sample_values.clear();
	freq_sample_values.clear();

	if( num_of_sample_pts <= MAX_NUMBEROFCOARSEFREQPOLYSAMPLES )
	{
		if( !CG_REG->get_DSDBR01_BASE_stub_hardware_calls() )
		{
			// Step 1. Choose the sample points on LM middle lines of frequency
			rval = findCoarseFreqSamplePoints( num_of_sample_pts );

			// Step 2. Collect all the sample data
			if( rval == rtype::ok )
			rval = collectCoarseFreqSamplePoints();

			if( rval == rtype::ok )
			{ // Step 3. have collected data, extract it for fitting the polynomial

				for( short i = 0 ; i < (short)_coarse_freq_sample_points.size() ; i++ )
				{
					if( _coarse_freq_sample_points[i]._stable_F_measurement == true )
					{
						// only take stable measurements, ignore other
						freq_sample_values.push_back( _coarse_freq_sample_points[i]._F_measured );
						pr_sample_values.push_back( _coarse_freq_sample_points[i]._P_ratio_measured );
					}
				}
			}
		}
		else
		{
			// Have no hardware, just use test data
			double freq_a = 196336; // roughly, the start of the C-band
			double freq_b = 191326; // roughly, the end of the C-band
			double pr_a = 0.2;
			double pr_b = 0.8;
			srand( (unsigned)time( NULL ) );

			_coarse_freq_sample_points.clear();

			for(int i = 0; i < num_of_sample_pts; i++)
			{
				freq_sample_values.push_back( freq_a + ((double)i*(freq_b-freq_a)/(double)(num_of_sample_pts-1)));
				pr_sample_values.push_back( pr_a + ((double)i*(pr_b-pr_a)/(double)(num_of_sample_pts-1)));

				// add a random element to the data
				freq_sample_values[i] = freq_sample_values[i]*(1+((double)(rand()-RAND_MAX)/(double)RAND_MAX)/1000);
				pr_sample_values[i] = pr_sample_values[i]*(1+((double)(rand()-RAND_MAX)/(double)RAND_MAX)/1000);

				CDSDBROperatingPoint new_op;
				_coarse_freq_sample_points.push_back(new_op);

				_coarse_freq_sample_points[i]._F_measured = freq_sample_values[i];
				_coarse_freq_sample_points[i]._P_ratio_measured = pr_sample_values[i];
			}
		}

		if( rval == rtype::ok
		&& (short)freq_sample_values.size() <= num_of_poly_coeffs )
		{
			rval = rtype::not_enough_data;
			CGR_LOG("CCGOperatingPoints::calcCoarseFreqPoly() not_enough_data",DIAGNOSTIC_CGR_LOG)

		}

		// Step 4. Fit a polynomial to the sample data 
		if( rval == rtype::ok )
		{
			int ndata = (int)freq_sample_values.size();
			int ma = (int)num_of_poly_coeffs;
			double *pr = (double*)malloc((ndata+1)*sizeof(double));
			double *freq = (double*)malloc((ndata+1)*sizeof(double));
			double *coeffs = (double*)malloc((ma+1)*sizeof(double));

			for(int i = 0; i < ndata; i++)
			{
				// Note indexing in numerical recipes start at 1, not 0
				pr[i+1] = pr_sample_values[i];
				freq[i+1] = freq_sample_values[i];
			}

			CGR_LOG("CCGOperatingPoints::calcCoarseFreqPoly() calling svdpolyfit",DIAGNOSTIC_CGR_LOG)

			// fit a polynomial to the data
			// by linear least squares using singular value decomposition
			svdpolyfit(pr, freq, ndata, coeffs, ma, p_chi_squared );

			CGR_LOG("CCGOperatingPoints::calcCoarseFreqPoly() returned from svdpolyfit",DIAGNOSTIC_CGR_LOG)

			coarse_freq_poly.clear();
			for(int i = 1; i <= ma ; i++ )
			{
				coarse_freq_poly.push_back(coeffs[i]);

				CString log_msg = "";
				log_msg.Format("CCGOperatingPoints::calcCoarseFreqPoly() coeffs[%d] = %g",i,coeffs[i]);
				CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
			}

			free(pr);
			free(freq);
			free(coeffs);
		}

		//Step 5: Calculate coarse frequency for each sample point
		if( rval == rtype::ok )
		{
			for( short i = 0 ; i < (short)_coarse_freq_sample_points.size() ; i++ )
			{
				double power_ratio = _coarse_freq_sample_points[i]._P_ratio_measured;
				double freq_value = 0;
				double exp_value = 1;

				for(int coeff_index=0; coeff_index < (int)(coarse_freq_poly.size()); coeff_index++)
				{
					if( coeff_index > 0 ) exp_value *= power_ratio;
					freq_value = freq_value + coarse_freq_poly[coeff_index] * exp_value;
				}

				_coarse_freq_sample_points[i]._F_coarse = freq_value;
			}
		}


		//Step 5: If specified in registry, write sample points used to file
		if( CG_REG->get_DSDBR01_CFREQ_write_sample_points_to_file() )
		{

			CString lines_abs_filepath = _results_dir_CString;
			if(lines_abs_filepath.Right(1) != _T("\\") )
				lines_abs_filepath += _T("\\");
			lines_abs_filepath += ("coarseFreqSamplePoints_");
			lines_abs_filepath += LASER_TYPE_ID_DSDBR01_CSTRING;
			lines_abs_filepath += USCORE;
			lines_abs_filepath += _laser_id_CString;
			lines_abs_filepath += USCORE;
			lines_abs_filepath += _date_time_stamp_CString;
			lines_abs_filepath += CSV;

			rval = writeSamplePointsToFile(
				lines_abs_filepath,
				true );
		}

		//Step 6: Write coefficients to file
		if( rval == rtype::ok  )
		{
			CString coeffs_abs_filepath = _results_dir_CString;
			if(coeffs_abs_filepath.Right(1) != _T("\\") )
				coeffs_abs_filepath += _T("\\");
			coeffs_abs_filepath += ("CoarseFreqPolyCoeffs_");
			coeffs_abs_filepath += LASER_TYPE_ID_DSDBR01_CSTRING;
			coeffs_abs_filepath += USCORE;
			coeffs_abs_filepath += _laser_id_CString;
			coeffs_abs_filepath += USCORE;
			coeffs_abs_filepath += _date_time_stamp_CString;
			coeffs_abs_filepath += CSV;

			rval = writePolyCoeffsToFile(
				coarse_freq_poly,
				coeffs_abs_filepath,
				true );
		}

	}
	else
	{
		rval = rtype::too_many_samples;

		CGR_LOG("CCGOperatingPoints::calcCoarseFreqPoly() too_many_samples",DIAGNOSTIC_CGR_LOG)
	}

	CGR_LOG(CString("CCGOperatingPoints::calcCoarseFreqPoly() exiting"),DIAGNOSTIC_CGR_LOG)

	return rval;
}

CCGOperatingPoints::rtype
CCGOperatingPoints::getCoarseFreqPoly(
	std::vector<double> &coarse_freq_poly )
{
	rtype rval = rtype::ok;

	coarse_freq_poly.clear();

	if( _coarse_freq_poly.size() > 0 )
	{
		for( int i = 0 ; i < (int)(_coarse_freq_poly.size()) ; i++ )
		{
			coarse_freq_poly.push_back(_coarse_freq_poly[i]);
		}
	}
	else
	{
		rval = rtype::empty_poly;
	}

	return rval;
}

CCGOperatingPoints::rtype
CCGOperatingPoints::setCoarseFreqPoly(
	std::vector<double> &coarse_freq_poly )
{
	rtype rval = rtype::ok;

	if( coarse_freq_poly.size() > 0 )
	{
		if( coarse_freq_poly.size() >= MIN_COARSEFREQPOLYCOEFFS )
		{
			if( coarse_freq_poly.size() <= MAX_COARSEFREQPOLYCOEFFS )
			{
				_coarse_freq_poly.clear();

				for( int i = 0 ; i < (int)(coarse_freq_poly.size()) ; i++ )
				{
					_coarse_freq_poly.push_back(coarse_freq_poly[i]);
				}
			}
			else
			{
				rval = rtype::max_num_of_poly_coeffs_exceeded;
			}
		}
		else
		{
			rval = rtype::min_num_of_poly_coeffs_not_reached;
		}
	}
	else
	{
		rval = rtype::empty_poly;
	}

	return rval;
}



CCGOperatingPoints::rtype
CCGOperatingPoints::readCoarseFreq(
	double &coarse_freq )
{
	rtype rval = rtype::ok;

	if( !(_coarse_freq_poly.empty()) )
	{
		double power_ch1 = 0;
		double power_ch2 = 0;

		// loop to measure optical power N times (from reg)
		// and calculate average
		for( int i = 0 ; i < CG_REG->get_DSDBR01_CFREQ_num_power_ratio_meas_per_op() ; i++ )
		{
			double p_1 = 0;
			double p_2 = 0;

			HRESULT hr = SERVERINTERFACE->readOpticalPowers(
				CG_REG->get_DSDBR01_CMDC_306II_module_name(),
				&p_1,
				&p_2);

			if( hr == S_OK && p_1 >= 0 && p_2 >= 0 )
			{
				power_ch1 += p_1;
				power_ch2 += p_2;
			}
			else
			{
				rval = rtype::error_reading_power_meter;
				break;
			}

		}

		if( rval == rtype::ok )
		{
			// get average power readings
			power_ch1 /= CG_REG->get_DSDBR01_CFREQ_num_power_ratio_meas_per_op();
			power_ch2 /= CG_REG->get_DSDBR01_CFREQ_num_power_ratio_meas_per_op();

			double power_ratio = 0;

			if( CG_REG->get_DSDBR01_CMDC_306II_direct_channel() == CHANNEL_1 )
			{
				power_ratio = power_ch2/power_ch1;
			}
			else
			{
				power_ratio = power_ch1/power_ch2;
			}

			if( power_ratio > 0 && power_ratio < 1 )
			{
				coarse_freq = getCoarseFreq( power_ratio );
			}
			else
			{
				rval = rtype::power_ratio_out_of_range;
			}
		}
	}
	else
	{
		rval = rtype::empty_poly;
	}

	return rval;
}

double
CCGOperatingPoints::getCoarseFreq( double power_ratio )
{
	//
	// y = p[0] + p[1]*x + p[2]*x^2 + p[3]*x^3... 
	//
	double freq_value = 0;
	double exp_value = 1;

	for(int coeff_index=0; coeff_index < (int)(_coarse_freq_poly.size()); coeff_index++)
	{
		if( coeff_index > 0 ) exp_value *= power_ratio;
		freq_value = freq_value + _coarse_freq_poly[coeff_index] * exp_value;
	}

	return freq_value;
}

CCGOperatingPoints::rtype
CCGOperatingPoints::findCoarseFreqSamplePoints( short num_of_sample_pts )
{
	rtype rval = rtype::ok;

	CGR_LOG("CCGOperatingPoints::findCoarseFreqSamplePoints() entered",DIAGNOSTIC_CGR_LOG)

	_coarse_freq_sample_points.clear();

	// Find num_of_sample_pts evenly distributed
	// on middle lines of frequency in LMs.
	// Any point on these lines should be stable.

	_Pr_pts_on_freq_lines.clear();

	for( short sm = 0; sm <	(short)(_p_supermodes->size()) ; sm++ )
	{
		std::vector<CDSDBRSuperMode>::iterator i_sm = _p_supermodes->begin();
		i_sm += sm;

		// for each sm

		for( short lm = 0; lm <	(short)(i_sm->_lm_middle_lines_of_frequency.size()) ; lm++ )
		{
			std::vector<CDSDBRSupermodeMapLine>::iterator i_lm
				= i_sm->_lm_middle_lines_of_frequency.begin();
			i_lm += lm;

			// For each lm of each sm

			for( short i = 0; i < (short)(i_lm->_points.size()) ; i++ )
			{
				// For each point on each lm of each sm
				// get the power ratio

				long row = 0;
				long col = 0;
				if( i_sm->_Im_ramp )
				{
					col = i_lm->_points[i].row;
					row = i_lm->_points[i].col;
				}
				else // Ip ramped
				{
					row = i_lm->_points[i].row;
					col = i_lm->_points[i].col;
				}

				double Pr_value = *(i_sm->_map_forward_power_ratio.iPt( row, col ));

				lm_freq_line_point_type new_pt;
				new_pt.sm = sm;
				new_pt.lm = lm;
				new_pt.i = i;

				// For each point on each lm of each sm
				// insert the power ratio and the point to a map.
				// On insertion, the points in the multimap are
				// automatically sorted by Pr value.

				_Pr_pts_on_freq_lines.insert(
                    std::multimap<double,lm_freq_line_point_type>::value_type(
						Pr_value,
                        new_pt ) );
			}
		}
	}

	if( (long)_Pr_pts_on_freq_lines.size() > (long)(2*num_of_sample_pts) )
	{
		// We have ample points, choose num_of_sample_pts evenly distributed
		// First and last are always chosen

		std::multimap<double,lm_freq_line_point_type>::iterator i_pt;
		
		short point_count = 0;
		long index = 0;
		for( i_pt = _Pr_pts_on_freq_lines.begin() ;
			i_pt != _Pr_pts_on_freq_lines.end() ;
			i_pt++ )
		{
			// always use first and last points
			if( index+1 > (long)((double)point_count * (double)_Pr_pts_on_freq_lines.size() / (double)(num_of_sample_pts-1) )
			 || index+1 == (long)_Pr_pts_on_freq_lines.size() )
			{
				CDSDBROperatingPoint new_sample_pt;
				_coarse_freq_sample_points.push_back(new_sample_pt);

				double Pr_value_on_sm_map = (*i_pt).first;
				short sm_number = (*i_pt).second.sm;
				short lm_number = (*i_pt).second.lm;
				short i_number = (*i_pt).second.i;

				std::vector<CDSDBRSuperMode>::iterator i_sm = _p_supermodes->begin();
				i_sm += sm_number;
				std::vector<CDSDBRSupermodeMapLine>::iterator i_lm = i_sm->_lm_middle_lines_of_frequency.begin();
				i_lm += lm_number;

				long row_i_number = i_lm->_points[i_number].row;
				long col_i_number = i_lm->_points[i_number].col;
				short front_pair_i_number = i_lm->_points[i_number].front_pair_number;
				double I_rear_i_number = i_lm->_points[i_number].I_rear;
				double I_front_1_i_number = i_lm->_points[i_number].I_front_1;
				double I_front_2_i_number = i_lm->_points[i_number].I_front_2;
				double I_front_3_i_number = i_lm->_points[i_number].I_front_3;
				double I_front_4_i_number = i_lm->_points[i_number].I_front_4;
				double I_front_5_i_number = i_lm->_points[i_number].I_front_5;
				double I_front_6_i_number = i_lm->_points[i_number].I_front_6;
				double I_front_7_i_number = i_lm->_points[i_number].I_front_7;
				double I_front_8_i_number = i_lm->_points[i_number].I_front_8;
				double I_phase_i_number = i_lm->_points[i_number].I_phase;

				//Add details of sample point to object in vector

				_coarse_freq_sample_points.rbegin()->_row_on_sm_map = row_i_number;
				_coarse_freq_sample_points.rbegin()->_col_on_sm_map = col_i_number;
				_coarse_freq_sample_points.rbegin()->_Pr_on_sm_map = Pr_value_on_sm_map;

				_coarse_freq_sample_points.rbegin()->_sm_number = sm_number;
				_coarse_freq_sample_points.rbegin()->_lm_number = lm_number;
				_coarse_freq_sample_points.rbegin()->_index_on_middle_line_of_frequency = i_number;
				_coarse_freq_sample_points.rbegin()->_real_index_on_middle_line_of_frequency = (double)i_number;
				_coarse_freq_sample_points.rbegin()->_front_pair_number = front_pair_i_number;
				_coarse_freq_sample_points.rbegin()->_I_gain = i_sm->_I_gain;
				_coarse_freq_sample_points.rbegin()->_I_soa = i_sm->_I_soa;
				_coarse_freq_sample_points.rbegin()->_I_rear = I_rear_i_number;
				_coarse_freq_sample_points.rbegin()->_I_front_1 = I_front_1_i_number;
				_coarse_freq_sample_points.rbegin()->_I_front_2 = I_front_2_i_number;
				_coarse_freq_sample_points.rbegin()->_I_front_3 = I_front_3_i_number;
				_coarse_freq_sample_points.rbegin()->_I_front_4 = I_front_4_i_number;
				_coarse_freq_sample_points.rbegin()->_I_front_5 = I_front_5_i_number;
				_coarse_freq_sample_points.rbegin()->_I_front_6 = I_front_6_i_number;
				_coarse_freq_sample_points.rbegin()->_I_front_7 = I_front_7_i_number;
				_coarse_freq_sample_points.rbegin()->_I_front_8 = I_front_8_i_number;
				_coarse_freq_sample_points.rbegin()->_I_phase = I_phase_i_number;

				point_count++;
			}

			index++;
		}

	}
	else
	{
		rval = rtype::not_enough_data;

		CString log_msg = "CCGOperatingPoints::findCoarseFreqSamplePoints() not_enough_data, ";
		log_msg.Format("number of points on all middle lines of frequency = %d",(long)_Pr_pts_on_freq_lines.size());
		CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	}

	if( rval == rtype::ok
	 && (short)_coarse_freq_sample_points.size() != num_of_sample_pts )
	{
		rval = rtype::requested_num_of_sample_pts_not_found;

		CString log_msg = "CCGOperatingPoints::findCoarseFreqSamplePoints() requested_num_of_sample_pts_not_found, ";
		log_msg.Format("number of sampled points = %d",(long)_coarse_freq_sample_points.size());
		CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
	}

	CGR_LOG("CCGOperatingPoints::findCoarseFreqSamplePoints() exiting",DIAGNOSTIC_CGR_LOG)

	return rval;
}


CCGOperatingPoints::rtype
CCGOperatingPoints::collectCoarseFreqSamplePoints()
{
	CGR_LOG("CCGOperatingPoints::collectCoarseFreqSamplePoints() entered",DIAGNOSTIC_CGR_LOG)

	rtype rval = rtype::ok;

	// check for data
	if( _coarse_freq_sample_points.size() > 0 )
	{

		// Step 1: Check laser is producing light,
		// should be able to get a power reading

		double optical_power_test;
		HRESULT hres_power = SERVERINTERFACE->readPhotodiodeCurrent(
			CG_REG->get_DSDBR01_CMDC_306II_module_name(),
			CG_REG->get_DSDBR01_CMDC_306II_direct_channel(),
			&optical_power_test );

		//double freq_meas;
		//HRESULT freq_meas_rval = SERVERINTERFACE->readFrequency(&freq_meas);

		//if( freq_meas_rval == S_OK )
		//{
		//	if( freq_meas >= MIN_VALID_FREQ_MEAS
		//	 && freq_meas <= MAX_VALID_FREQ_MEAS )
		//	{

		if( hres_power == S_OK )
		{
			if( optical_power_test >= MIN_DIRECT_OPTICAL_POWER )
			{
				int count_additional_inserted_points = 0;

				for( short i = 0 ; i < (short)_coarse_freq_sample_points.size() ; i++ )
				{
					if( i > MAX_NUMBEROFCOARSEFREQPOLYSAMPLES )
					{
						rval = rtype::too_many_samples;
						break;
					}

					// Step 2: Play the currents of each sample point
					// ensure only two front sections are on at any time

					CGR_LOG("CCGOperatingPoints::collectCoarseFreqSamplePoints() applying sample point",DIAGNOSTIC_CGR_LOG)


					CDSDBROperatingPoint::rtype rval_apply = _coarse_freq_sample_points[i].apply();
					if( rval_apply != CDSDBROperatingPoint::rtype::ok )
					{
						rval = rtype::error_applying_op;
						break;
					}

					// Wait for settle time

					int settle_time_ms = CG_REG->get_DSDBR01_CFREQ_sample_point_settle_time();

					if( settle_time_ms > 0 )
						Sleep( (DWORD)settle_time_ms );

					CGR_LOG("CCGOperatingPoints::collectCoarseFreqSamplePoints() sample point applied and settled",DIAGNOSTIC_CGR_LOG)

					// Step 3: Take a number of frequency and power ratio measurements
					// as specified in the registry

					double average_frequency = 0;
					double average_power_ratio = 0;
					double average_power = 0;
					double max_measured_freq = 0;
					double min_measured_freq = 0;

					rval = measure_F_P_Pr(
							average_frequency,
							min_measured_freq,
							max_measured_freq,
							average_power,
							average_power_ratio,
							CG_REG->get_DSDBR01_CFREQ_num_freq_meas_per_op(),
							CG_REG->get_DSDBR01_CFREQ_num_power_ratio_meas_per_op() );


					CString log_msg("CCGOperatingPoints::collectCoarseFreqSamplePoints() ");
					log_msg.AppendFormat(" measured F = %.3f [GHz], P = %g [mW], Pr = %g [0:1]", average_frequency, average_power, average_power_ratio);
					CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

					if( rval == rtype::invalid_frequency_from_wavemeter
						|| max_measured_freq - min_measured_freq > CG_REG->get_DSDBR01_CFREQ_max_stable_freq_error() )
					{
						// operating point is producing an unstable frequency, ignore sample point and continue
						CString err_msg("CCGOperatingPoints::collectCoarseFreqSamplePoints() ");
						err_msg.AppendFormat(" unstable frequency: rval = %d, Fmin = %.3f [GHz], Fmax = %.3f [GHz], dFmax = %g [GHz]", (int)rval, min_measured_freq, max_measured_freq, CG_REG->get_DSDBR01_CFREQ_max_stable_freq_error());
						CGR_LOG(err_msg,WARNING_CGR_LOG)

						CGR_LOG("CCGOperatingPoints::collectCoarseFreqSamplePoints() ignore sample point and continue",WARNING_CGR_LOG)

						_coarse_freq_sample_points[i]._stable_F_measurement = false;


						//if( _coarse_freq_sample_points.size() < MAX_NUMBEROFCOARSEFREQPOLYSAMPLES
						//	|| count_additional_inserted_points < MAX_ADDITIONAL_SAMPLES )
						//{

						//	// insert a new operating point to try instead

						//	CGR_LOG("CCGOperatingPoints::collectCoarseFreqSamplePoints() find new sample point half way between i-1 and i (except where i==0)",WARNING_CGR_LOG)

						//	short below = 0;
						//	short above = 1;
						//	if( i > 0 ) // find point half way between i-1 and i (except where i==0)
						//	{
						//		below = i-1;
						//		above = i;
						//	}


						//	if( _coarse_freq_sample_points[below]._Pr_on_sm_map
						//	 != _coarse_freq_sample_points[above]._Pr_on_sm_map )
						//	{

						//		std::multimap<double,lm_freq_line_point_type>::iterator i_pt_below
						//			= _Pr_pts_on_freq_lines.find( _coarse_freq_sample_points[below]._Pr_on_sm_map );

						//		std::multimap<double,lm_freq_line_point_type>::iterator i_pt_above
						//			= _Pr_pts_on_freq_lines.find( _coarse_freq_sample_points[above]._Pr_on_sm_map );

						//		std::multimap<double,lm_freq_line_point_type>::iterator i_pt_between;
						//		std::multimap<double,lm_freq_line_point_type>::iterator i_pt_loop;
						//		
						//		bool even_count = true;
						//		i_pt_between = i_pt_below;
						//		for( i_pt_loop = i_pt_below ; i_pt_loop != i_pt_above ; i_pt_loop++ )
						//		{
						//			if( even_count ) even_count = false;
						//			else
						//			{
						//				// iterate this every 2nd time,
						//				// so as to find point half way between above and below
						//				i_pt_between++; 
						//				even_count = true;
						//			}
						//		}

						//		// find place to insert new point

						//		std::vector<CDSDBROperatingPoint>::iterator i_new_sample_point = _coarse_freq_sample_points.begin();
						//		i_new_sample_point += i+1;


						//		// create new point

						//		CDSDBROperatingPoint new_sample_pt;
						//		i_new_sample_point = _coarse_freq_sample_points.insert(i_new_sample_point,new_sample_pt);

						//		double Pr_value_on_sm_map = (*i_pt_between).first;
						//		short sm_number = (*i_pt_between).second.sm;
						//		short lm_number = (*i_pt_between).second.lm;
						//		short i_number = (*i_pt_between).second.i;

						//		std::vector<CDSDBRSuperMode>::iterator i_sm = _p_supermodes->begin();
						//		i_sm += sm_number;
						//		std::vector<CDSDBRSupermodeMapLine>::iterator i_lm = i_sm->_lm_middle_lines_of_frequency.begin();
						//		i_lm += lm_number;

						//		long row_i_number = i_lm->_points[i_number].row;
						//		long col_i_number = i_lm->_points[i_number].col;
						//		short front_pair_i_number = i_lm->_points[i_number].front_pair_number;
						//		double I_rear_i_number = i_lm->_points[i_number].I_rear;
						//		double I_front_1_i_number = i_lm->_points[i_number].I_front_1;
						//		double I_front_2_i_number = i_lm->_points[i_number].I_front_2;
						//		double I_front_3_i_number = i_lm->_points[i_number].I_front_3;
						//		double I_front_4_i_number = i_lm->_points[i_number].I_front_4;
						//		double I_front_5_i_number = i_lm->_points[i_number].I_front_5;
						//		double I_front_6_i_number = i_lm->_points[i_number].I_front_6;
						//		double I_front_7_i_number = i_lm->_points[i_number].I_front_7;
						//		double I_front_8_i_number = i_lm->_points[i_number].I_front_8;
						//		double I_phase_i_number = i_lm->_points[i_number].I_phase;

						//		//Add details of sample point to object in vector

						//		i_new_sample_point->_row_on_sm_map = row_i_number;
						//		i_new_sample_point->_col_on_sm_map = col_i_number;
						//		i_new_sample_point->_Pr_on_sm_map = Pr_value_on_sm_map;

						//		i_new_sample_point->_sm_number = sm_number;
						//		i_new_sample_point->_lm_number = lm_number;
						//		i_new_sample_point->_index_on_middle_line_of_frequency = i_number;
						//		i_new_sample_point->_front_pair_number = front_pair_i_number;
						//		i_new_sample_point->_I_gain = i_sm->_I_gain;
						//		i_new_sample_point->_I_soa = i_sm->_I_soa;
						//		i_new_sample_point->_I_rear = I_rear_i_number;
						//		i_new_sample_point->_I_front_1 = I_front_1_i_number;
						//		i_new_sample_point->_I_front_2 = I_front_2_i_number;
						//		i_new_sample_point->_I_front_3 = I_front_3_i_number;
						//		i_new_sample_point->_I_front_4 = I_front_4_i_number;
						//		i_new_sample_point->_I_front_5 = I_front_5_i_number;
						//		i_new_sample_point->_I_front_6 = I_front_6_i_number;
						//		i_new_sample_point->_I_front_7 = I_front_7_i_number;
						//		i_new_sample_point->_I_front_8 = I_front_8_i_number;
						//		i_new_sample_point->_I_phase = I_phase_i_number;

						//		count_additional_inserted_points++;

						//		CGR_LOG("CCGOperatingPoints::collectCoarseFreqSamplePoints() inserted new sample point",WARNING_CGR_LOG)
						//	}
						//	else
						//	{
						//		CString err_msg("CCGOperatingPoints::collectCoarseFreqSamplePoints() cannot find new sample point between two sample points, ignore and continue.");
						//		CGR_LOG(err_msg,WARNING_CGR_LOG)
						//	}
						//}
						//else
						//{
						//	CString err_msg("CCGOperatingPoints::collectCoarseFreqSamplePoints() cannot insert anymore sample points");
						//	err_msg.AppendFormat(" _coarse_freq_sample_points.size() = %d, count_additional_inserted_points = %d", (int)_coarse_freq_sample_points.size(), count_additional_inserted_points );
						//	CGR_LOG(err_msg,ERROR_CGR_LOG)

						//	rval = rtype::unstable_operating_points;
						//	break;
						//}
					}
					else if( rval == rtype::ok )
					{
						_coarse_freq_sample_points[i]._stable_F_measurement = true;

						// have successfully collected a sample at one operating point
						_coarse_freq_sample_points[i]._F_measured = average_frequency;
						_coarse_freq_sample_points[i]._P_ratio_measured = average_power_ratio;
						_coarse_freq_sample_points[i]._P_measured = average_power;
					}
					else // rval must have value other than ok or invalid_frequency_from_wavemeter
					{
						CString err_msg("CCGOperatingPoints::collectCoarseFreqSamplePoints() ");
						err_msg.AppendFormat(" measure_F_P_Pr returned rval = %d", (int)rval );
						CGR_LOG(err_msg,ERROR_CGR_LOG)

						break;
					}

				} // end of for loop
			}
			else
			{
				// could not measure valid power, laser may not be lasing
				rval = rtype::power_reading_too_low;
			}
		}
		else
		{
			// could not measure power meter, could be a hw problem
			rval = rtype::error_reading_power_meter;
		}
	}
	else
	{
		rval = rtype::no_data;
	}

	CGR_LOG("CCGOperatingPoints::collectCoarseFreqSamplePoints() exiting",DIAGNOSTIC_CGR_LOG)

	return rval;
}


CCGOperatingPoints::rtype
CCGOperatingPoints::waitForTECSettling()
{
	rtype rval = rtype::ok;

	CString log_msg_start("CCGOperatingPoints::waitForTECSettling(): ");
	CString log_msg = log_msg_start + CString("entered");
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	// get Registry values
	int time_window_size = CG_REG->get_DSDBR01_CHARTEC_time_window_size();
	double max_temp_deviation = CG_REG->get_DSDBR01_CHARTEC_max_temp_deviation();
	int num_temp_readings = CG_REG->get_DSDBR01_CHARTEC_num_temp_readings();
	int max_settle_time = CG_REG->get_DSDBR01_CHARTEC_max_settle_time();

	if( time_window_size <= 0
	 || max_temp_deviation <= (double)0
	 || num_temp_readings <= 0
	 || max_settle_time <= 0 )
	{
		log_msg = log_msg_start + CString("A TEC settling reg entry is zero or less => no TEC settling");
		CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
		log_msg = log_msg_start + CString("exiting");
		CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
		return rtype::temp_stability_not_applied;
	}

	int time_between_samples = time_window_size/(num_temp_readings - 1);
	log_msg = log_msg_start + CString("time_between_samples = ");
	log_msg.AppendFormat("%d [ms]",time_between_samples);
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	int sleep_time = max_settle_time < time_between_samples ? max_settle_time : time_between_samples;
	sleep_time = sleep_time/10;

	LARGE_INTEGER start_count;
	LARGE_INTEGER current_count;
	LARGE_INTEGER counts_per_second;
	LARGE_INTEGER max_counts;
	LARGE_INTEGER counts_since_start;
	LARGE_INTEGER count_at_last_sample;
	LARGE_INTEGER counts_since_last_sample;
	LARGE_INTEGER counts_between_samples;
	int num_samples = 0;
	int sample_index = 0;

	QueryPerformanceFrequency( &counts_per_second );
	QueryPerformanceCounter( &start_count );
	count_at_last_sample.QuadPart = start_count.QuadPart;

	max_counts.QuadPart = (counts_per_second.QuadPart/(LONGLONG)1000)*(LONGLONG)max_settle_time;
	counts_between_samples.QuadPart = (counts_per_second.QuadPart/(LONGLONG)1000)*(LONGLONG)time_between_samples;

	counts_since_start.QuadPart = 0;
	counts_since_last_sample.QuadPart = 0;

	std::vector<double> temp_readings;
	for(int i = 0 ; i < num_temp_readings ; i++ )
	{
		temp_readings.push_back( 0 );
	}

	while( counts_since_start.QuadPart < max_counts.QuadPart )
	{

		if( counts_since_last_sample.QuadPart >= counts_between_samples.QuadPart )
		{
			// Time to read temperature

			double temperature = 0;
			HRESULT hr = SERVERINTERFACE->readTemperature( &temperature );

			QueryPerformanceCounter( &count_at_last_sample );
			counts_since_last_sample.QuadPart = 0;

#ifndef HARDWARE_BUILD
			temperature = 25
						+ (1*( max_counts.QuadPart - counts_since_start.QuadPart) / max_counts.QuadPart)
						+ (0.1)*((2*(double)rand()/(double)RAND_MAX)-1);
#endif // HARDWARE_BUILD

			if( hr != S_OK )
			{
				log_msg = log_msg_start;
				log_msg.AppendFormat("Error 0x%08x from reading temperature", hr);
				CGR_LOG(log_msg,ERROR_CGR_LOG)

				log_msg = log_msg_start + CString("exiting");
				CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
				return rtype::error_reading_temp_from_tec;
			}

			log_msg = log_msg_start;
			log_msg.AppendFormat("Temperature %.3f", temperature);
			CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

			num_samples++;
			sample_index++;
			if(sample_index >= num_temp_readings) sample_index=0;

			temp_readings[sample_index] = temperature;

			if( num_samples >= num_temp_readings )
			{
				double min_temp =  10000; // temp initial value
				double max_temp = -10000; // temp initial value
				double temp_deviation = 0;

				for(int i = 0 ; i < num_temp_readings ; i++ )
				{
					if( temp_readings[i] < min_temp) min_temp = temp_readings[i];
					if( temp_readings[i] > max_temp) max_temp = temp_readings[i];
				}
				temp_deviation = max_temp - min_temp;

				log_msg = log_msg_start;
				log_msg.AppendFormat("Min temp = %.3f, Max temp = %.3f, deviation = %.3f", min_temp, max_temp, temp_deviation);
				CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

				if( temp_deviation < max_temp_deviation )
				{
					log_msg = log_msg_start + CString("Temperature stability achieved");
					CGR_LOG(log_msg,INFO_CGR_LOG)

					log_msg = log_msg_start + CString("exiting");
					CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
					return rtype::ok;
				}
			}
		}

		Sleep( (DWORD)(sleep_time) );
		QueryPerformanceCounter( &current_count );

		if( current_count.QuadPart >= start_count.QuadPart )
			counts_since_start.QuadPart = current_count.QuadPart - start_count.QuadPart;
		else
			counts_since_start.QuadPart = MAXLONGLONG - (start_count.QuadPart - current_count.QuadPart);

		if( current_count.QuadPart >= count_at_last_sample.QuadPart )
			counts_since_last_sample.QuadPart = current_count.QuadPart - count_at_last_sample.QuadPart;
		else
			counts_since_last_sample.QuadPart = MAXLONGLONG - (count_at_last_sample.QuadPart - current_count.QuadPart);
	}

	log_msg = log_msg_start + CString("Temperature stability not reached");
	CGR_LOG(log_msg,WARNING_CGR_LOG)

	log_msg = log_msg_start + CString("exiting");
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	return rtype::temp_stability_not_reached;
}

CCGOperatingPoints::rtype
CCGOperatingPoints::findITUOperatingPointsUsingWavemeter()
{
	rtype rval = rtype::ok;

	CGR_LOG(CString("CCGOperatingPoints::findITUOperatingPointsUsingWavemeter() entered"),DIAGNOSTIC_CGR_LOG)

	_characterisation_itu_ops.clear();
	_Freq_pts_on_freq_lines.clear();

	// set up ITU grip based on registry entries
	if( _itugrid.setupEx(CG_REG->get_DSDBR01_ITUGRID_abspath() ) != CITUGrid::rcode::ok )
	{
		CGR_LOG(CString("CCGOperatingPoints::findITUOperatingPointsUsingWavemeter() ITU Grid Setup Failed"),ERROR_CGR_LOG)
		return rtype::not_enough_data;
	}

	// count the total number of lms.
	// then use lms completed to set percent_complete
	short total_number_of_lms = 0;
	short completed_lms = 0;
	for( short sm = 0; sm <	(short)(_p_supermodes->size()) ; sm++ )
	{
		std::vector<CDSDBRSuperMode>::iterator i_sm = _p_supermodes->begin();
		i_sm += sm;

		for( short lm = 0; lm <	(short)(i_sm->_lm_middle_lines_of_frequency.size()) ; lm++ )
			total_number_of_lms++;
	}

	CString log_msg_start("CCGOperatingPoints::findITUOperatingPointsUsingWavemeter(): ");
	CString log_msg = log_msg_start + CString("total_number_of_lms = ");
	log_msg.AppendFormat("%d",total_number_of_lms);
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	int settle_time_ms = CG_REG->get_DSDBR01_CHAR_op_settle_time();

	log_msg = log_msg_start + CString("settle_time_ms = ");
	log_msg.AppendFormat("%d",settle_time_ms);
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	double wavemeter_error_ghz = CG_REG->get_DSDBR01_CHAR_freq_accuracy_of_wavemeter();

	log_msg = log_msg_start + CString("wavemeter_error_ghz = ");
	log_msg.AppendFormat("%g",wavemeter_error_ghz);
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	for( short sm = 0; sm <	(short)(_p_supermodes->size()) ; sm++ )
	{
		std::vector<CDSDBRSuperMode>::iterator i_sm = _p_supermodes->begin();
		i_sm += sm;

		// for each sm

		for( short lm = 0; lm <	(short)(i_sm->_lm_middle_lines_of_frequency.size()) ; lm++ )
		{
			std::vector<CDSDBRSupermodeMapLine>::iterator i_lm
				= i_sm->_lm_middle_lines_of_frequency.begin();
			i_lm += lm;

			log_msg = log_msg_start + CString("Starting search of Supermode Number ");
			log_msg.AppendFormat("%d",sm);
			log_msg += CString("Longitudinal Mode Number ");
			log_msg.AppendFormat("%d",lm);
			CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

			if( (int)i_lm->_points.size() > CG_REG->get_DSDBR01_SMBD_ml_min_length() )
			{
				// Only use middle lines with a decent number of points
				// For each lm of each sm

				// measure freq at start and end of lm middle line of frequency
				CDSDBROperatingPoint first_pt_on_middle_freq_line( i_sm, i_lm, sm, lm, (short)0 );
				CDSDBROperatingPoint last_pt_on_middle_freq_line( i_sm, i_lm, sm, lm, (short)(i_lm->_points.size()-1) );

				log_msg = log_msg_start + CString("Applying first point on line ");
				CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

				CDSDBROperatingPoint::rtype rval_op = CDSDBROperatingPoint::rtype::ok;

				rval_op = first_pt_on_middle_freq_line.apply();

				if( rval_op != CDSDBROperatingPoint::rtype::ok )
				{
					log_msg = log_msg_start + CString("Error applying first point on line ");
					CGR_LOG(log_msg,ERROR_CGR_LOG)

					rval = rtype::error_applying_op;
					break;
				}

				if( waitForTECSettling() == rtype::temp_stability_not_applied 
				 && settle_time_ms > 0 )
				{
					Sleep( (DWORD)(2*settle_time_ms) );
				}

				// Take a frequency sample
				double Pr_at_first_pt = 0;
				double P_at_first_pt = 0;
				double frequency_at_first_pt;
				double min_measured_freq_at_first_pt = 0;
				double max_measured_freq_at_first_pt = 0;

				rval = measure_F_P_Pr(
						frequency_at_first_pt,
						min_measured_freq_at_first_pt,
						max_measured_freq_at_first_pt,
						P_at_first_pt,
						Pr_at_first_pt,
						CG_REG->get_DSDBR01_CHAR_num_freq_meas_per_op(),
						CG_REG->get_DSDBR01_CHAR_num_power_ratio_meas_per_op() );

				if( rval != rtype::ok )
				{
					if( rval != rtype::invalid_frequency_from_wavemeter )
					{
						log_msg = log_msg_start + CString("Invalid Frequency from Wavemeter");
						CGR_LOG(log_msg,ERROR_CGR_LOG)
						break;
					}
					// otherwise,
					// invalid_frequency_from_wavemeter indicates instability,
					// simply move on to next lm

				}

				log_msg = log_msg_start + CString("Frequency measured at first point is ");
				log_msg.AppendFormat("%.3f",frequency_at_first_pt);
				CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

				log_msg = log_msg_start + CString("Power measured at first point is ");
				log_msg.AppendFormat("%g",P_at_first_pt);
				CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

				log_msg = log_msg_start + CString("Power Ratio measured at first point is ");
				log_msg.AppendFormat("%g",Pr_at_first_pt);
				CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

				if( rval == rtype::ok )
				{
					log_msg = log_msg_start + CString("Applying last point on line ");
					CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

					rval_op = last_pt_on_middle_freq_line.apply();
				}

				if( rval_op != CDSDBROperatingPoint::rtype::ok )
				{
					log_msg = log_msg_start + CString("Error applying last point on line ");
					CGR_LOG(log_msg,ERROR_CGR_LOG)

					rval = rtype::error_applying_op;
					break;
				}

				if( rval == rtype::ok
				 && waitForTECSettling() == rtype::temp_stability_not_applied 
				 && settle_time_ms > 0 )
				{
					Sleep( (DWORD)settle_time_ms );
				}

				// Take a frequency sample

				double Pr_at_last_pt = 0;
				double P_at_last_pt = 0;
				double frequency_at_last_pt;
				double min_measured_freq_at_last_pt = 0;
				double max_measured_freq_at_last_pt = 0;

				if( rval == rtype::ok )
				rval = measure_F_P_Pr(
						frequency_at_last_pt,
						min_measured_freq_at_last_pt,
						max_measured_freq_at_last_pt,
						P_at_last_pt,
						Pr_at_last_pt,
						CG_REG->get_DSDBR01_CHAR_num_freq_meas_per_op(),
						CG_REG->get_DSDBR01_CHAR_num_power_ratio_meas_per_op() );

				if( rval != rtype::ok )
				{
					if( rval != rtype::invalid_frequency_from_wavemeter )
					{
						log_msg = log_msg_start + CString("Invalid Frequency from Wavemeter");
						CGR_LOG(log_msg,ERROR_CGR_LOG)
						break;
					}
					// otherwise,
					// invalid_frequency_from_wavemeter indicates instability,
					// simply move on to next lm
				}

				log_msg = log_msg_start + CString("Frequency measured at last point is ");
				log_msg.AppendFormat("%.3f",frequency_at_last_pt);
				CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

				log_msg = log_msg_start + CString("Power measured at last point is ");
				log_msg.AppendFormat("%g",P_at_last_pt);
				CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

				log_msg = log_msg_start + CString("Power Ratio measured at last point is ");
				log_msg.AppendFormat("%g",Pr_at_last_pt);
				CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

				if( rval == rtype::ok )
				{
					int itusearch_scheme = CG_REG->get_DSDBR01_CHAR_itusearch_scheme();

					if( itusearch_scheme >= itusearch_scheme_type::end_of_schemes
					 || itusearch_scheme < 0 )
					{
						// invalid choice in registry
						log_msg = log_msg_start;
						log_msg.AppendFormat("invalid interpolation scheme %d set in registry",itusearch_scheme);
						CGR_LOG(log_msg,WARNING_CGR_LOG)

						// use default instead
						itusearch_scheme = itusearch_scheme_type::original;
						log_msg = log_msg_start;
						log_msg.AppendFormat("resetting interpolation scheme to default %d",itusearch_scheme);
						CGR_LOG(log_msg,WARNING_CGR_LOG)

						// save default to registry
						CG_REG->set_DSDBR01_CHAR_itusearch_scheme( itusearch_scheme );
						CG_REG->saveAllToRegistry();
					}

					log_msg = log_msg_start + CString("itusearch_scheme = ");
					switch(itusearch_scheme)
					{
					case itusearch_scheme_type::original:
						log_msg.Append("original");
						break;
					case itusearch_scheme_type::quit_itupoint_after_problem:
						log_msg.Append("quit_itupoint_after_problem");
						break;
					case itusearch_scheme_type::remove_upper_lower_after_problem:
						log_msg.Append("remove_upper_lower_after_problem");
						break;
					case itusearch_scheme_type::resample_upper_lower_after_problem:
						log_msg.Append("resample_upper_lower_after_problem");
						break;
					case itusearch_scheme_type::remove_upper_lower_find_N_samples_after_problem:
						log_msg.Append("remove_upper_lower_find_N_samples_after_problem");
						break;
					case itusearch_scheme_type::bisection_search_after_problem:
						log_msg.Append("bisection_search_after_problem");
						break;
					case itusearch_scheme_type::brute_force_search_after_problem:
						log_msg.Append("brute_force_search_after_problem");
						break;
					default:
						log_msg.Append("unknown!!!!");
						break;
					}
					CGR_LOG(log_msg,INFO_CGR_LOG)

					if( itusearch_scheme == itusearch_scheme_type::original )
					{
						rval = interpolationRoutine(
							frequency_at_first_pt,
							frequency_at_last_pt,
							i_lm,
							i_sm,
							sm,
							lm );
					}
					else
					{
						rval = newITUPointSearchRoutine(
							frequency_at_first_pt,
							min_measured_freq_at_first_pt,
							max_measured_freq_at_first_pt,
							P_at_first_pt,
							Pr_at_first_pt,
							frequency_at_last_pt,
							min_measured_freq_at_last_pt,
							max_measured_freq_at_last_pt,
							P_at_last_pt,
							Pr_at_last_pt,
							i_lm,
							i_sm,
							sm,
							lm );
					}

					if( rval != rtype::ok ) break;

					completed_lms++;

					setPercentComplete( (short)(95*(double)completed_lms/(double)total_number_of_lms) );

					setITUOPCount( countITUOPsFound() );

				}
			}
			else
			{
				log_msg = log_msg_start + CString("Supermode Number ");
				log_msg.AppendFormat("%d",sm);
				log_msg += CString(", Longitudinal Mode Number ");
				log_msg.AppendFormat("%d",lm);
				log_msg += CString(", Middle line of Frequency contains too few points ");
				log_msg.AppendFormat("%d",(int)i_lm->_points.size());
				log_msg += CString(", less than min_length ");
				log_msg.AppendFormat("%d",CG_REG->get_DSDBR01_SMBD_ml_min_length());
				CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
			}
		}
		if( rval != rtype::ok )
		{
			if( rval != rtype::invalid_frequency_from_wavemeter )
			{
				log_msg = log_msg_start + CString("invalid frequency returned from wavemeter");
				CGR_LOG(log_msg,WARNING_CGR_LOG)
				break;
			}
			// otherwise,
			// invalid_frequency_from_wavemeter indicates instability,
			// simply move on to next sm
		}
	}



	if( rval == rtype::ok  )
	{
		rval = findNearbyBoundaryCoords( _characterisation_itu_ops );
	}

	if( rval == rtype::ok  )
	{
		CString ops_abs_filepath = _results_dir_CString;
		if(ops_abs_filepath.Right(1) != _T("\\") )
			ops_abs_filepath += _T("\\");
		ops_abs_filepath += ("ITUOperatingPoints_");
		ops_abs_filepath += LASER_TYPE_ID_DSDBR01_CSTRING;
		ops_abs_filepath += USCORE;
		ops_abs_filepath += _laser_id_CString;
		ops_abs_filepath += USCORE;
		ops_abs_filepath += _date_time_stamp_CString;
		ops_abs_filepath += CSV;

		rval = writeOPsToFile(
			ops_abs_filepath,
			_characterisation_itu_ops,
			true );


		if( rval == rtype::ok  )
		{
			CString freq_coverage_abs_filepath = ops_abs_filepath;

			freq_coverage_abs_filepath.Replace("ITUOperatingPoints_","FrequencyCoverage_");

			rval = writeFreqCoverageToFile(
				freq_coverage_abs_filepath,
				true );
		}
	}

	if( rval == rtype::ok 
	 && (long)(countITUOPsFound()) < _itugrid.channel_count() )
	{
		// not all channels have been found
		// this is only a warning really

		CString log_err_msg("CCGOperatingPoints::findITUOperatingPointsUsingWavemeter warning: ");
		log_err_msg += CString("operating points not found for all ITU channels");
		CGR_LOG(log_err_msg,WARNING_CGR_LOG)
	}

	CGR_LOG(CString("CCGOperatingPoints::findITUOperatingPointsUsingWavemeter() exiting"),DIAGNOSTIC_CGR_LOG)

	return rval;
}


CCGOperatingPoints::rtype
CCGOperatingPoints::newITUPointSearchRoutine(
	double frequency_at_first_pt,
	double min_measured_freq_at_first_pt,
	double max_measured_freq_at_first_pt,
	double P_at_first_pt,
	double Pr_at_first_pt,
	double frequency_at_last_pt,
	double min_measured_freq_at_last_pt,
	double max_measured_freq_at_last_pt,
	double P_at_last_pt,
	double Pr_at_last_pt,
	std::vector<CDSDBRSupermodeMapLine>::iterator i_lm,
	std::vector<CDSDBRSuperMode>::iterator i_sm,
	short sm,
	short lm )
{
	rtype rval = rtype::ok;

	CString log_msg_start("CCGOperatingPoints::newITUPointSearchRoutine(): ");
	CString log_msg = log_msg_start + CString("entered");
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	int itusearch_scheme = CG_REG->get_DSDBR01_CHAR_itusearch_scheme();
	int settle_time_ms = CG_REG->get_DSDBR01_CHAR_op_settle_time();
	double wavemeter_error_ghz = CG_REG->get_DSDBR01_CHAR_freq_accuracy_of_wavemeter();

	// clear map of samples taken in an LM, add first and last to start with
	log_msg = log_msg_start + CString("Clearing map of samples in LM ");
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	std::map<double,double> freq_and_i;
	std::map<double,double> i_and_min_measured_freq;
	std::map<double,double> i_and_max_measured_freq;
	std::map<double,double> i_and_P;
	std::map<double,double> i_and_Pr;

	freq_and_i.clear();
	i_and_min_measured_freq.clear();
	i_and_max_measured_freq.clear();
	i_and_P.clear();
	i_and_Pr.clear();

	log_msg = log_msg_start + CString("Adding first and last points to map of samples in LM ");
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	freq_and_i.insert( std::map<double,double>::value_type(frequency_at_first_pt,(double)0) );
	i_and_min_measured_freq.insert( std::map<double,double>::value_type((double)0,min_measured_freq_at_first_pt) );
	i_and_max_measured_freq.insert( std::map<double,double>::value_type((double)0,max_measured_freq_at_first_pt) );
	i_and_P.insert( std::map<double,double>::value_type((double)0,P_at_first_pt) );
	i_and_Pr.insert( std::map<double,double>::value_type((double)0,Pr_at_first_pt) );

	freq_and_i.insert( std::map<double,double>::value_type(frequency_at_last_pt,(double)(i_lm->_points.size()-1)) );
	i_and_min_measured_freq.insert( std::map<double,double>::value_type((double)(i_lm->_points.size()-1),min_measured_freq_at_last_pt) );
	i_and_max_measured_freq.insert( std::map<double,double>::value_type((double)(i_lm->_points.size()-1),max_measured_freq_at_last_pt) );
	i_and_P.insert( std::map<double,double>::value_type((double)(i_lm->_points.size()-1),P_at_last_pt) );
	i_and_Pr.insert( std::map<double,double>::value_type((double)(i_lm->_points.size()-1),Pr_at_last_pt) );


	lm_freq_line_point_type2 new_pt;
	new_pt.sm = sm;
	new_pt.lm = lm;
	new_pt.i = (double)0;

	_Freq_pts_on_freq_lines.insert(
		std::multimap<double,lm_freq_line_point_type2>::value_type(
			frequency_at_first_pt,
			new_pt ) );

	new_pt.i = (double)(i_lm->_points.size()-1);

	_Freq_pts_on_freq_lines.insert(
		std::multimap<double,lm_freq_line_point_type2>::value_type(
			frequency_at_last_pt,
			new_pt ) );

	// identify ITU points to be found

	log_msg = log_msg_start + CString("Identifying ITU points to search for in LM");
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	std::vector<short> channel_num_of_itu_pts_in_lm;
	std::vector<double> freq_of_itu_pts_in_lm;

	double min_freq = 0;
	double max_freq = 0;
	if( frequency_at_last_pt > frequency_at_first_pt )
	{
		min_freq = frequency_at_first_pt - wavemeter_error_ghz;
		max_freq = frequency_at_last_pt + wavemeter_error_ghz;
	}
	else
	{
		min_freq = frequency_at_last_pt - wavemeter_error_ghz;
		max_freq = frequency_at_first_pt + wavemeter_error_ghz;
	}

	_itugrid.findITUPtsInRange(
		channel_num_of_itu_pts_in_lm,
		freq_of_itu_pts_in_lm,
		min_freq,
		max_freq );

	for( short i = 0; i < (short)channel_num_of_itu_pts_in_lm.size(); i++ )
	{
		// for each ITU point to be found
		//    start with best guess,
		//    then iterate to ITU point within specified accuracy of wavemeter
		//    record the ITU point's details

		short channel_num = channel_num_of_itu_pts_in_lm[i];
		double freq_of_channel_num = freq_of_itu_pts_in_lm[i];
		double freq_upper = 0;
		double point_upper = 0;
		double freq_lower = 0;
		double point_lower = 0;
		double guess_i = 0;
		double last_guess_i = -1;
		bool problem_with_freq_and_i_table = false;
		bool problem_with_hardware = false;
		double freq_i = 0;
		double Pr_i = 0;
		double P_i = 0;
		double max_measured_freq = 0;
		double min_measured_freq = 0;
		double prev_recorded_freq_i = 0;
		double resample_pointA = 0;
		double resample_freqA = 0;
		double resample_pointB = 0;
		double resample_freqB = 0;
		int resample_N = 0;
		int num_iterations_with_problem = 0;
		int num_iterations_without_problem = 0;

		int itusearch_nextguess = CG_REG->get_DSDBR01_CHAR_itusearch_nextguess();
		log_msg = log_msg_start;
		if( itusearch_nextguess == itusearch_nextguess_type::bisection )
			log_msg.AppendFormat("itusearch_nextguess_type = bisection");
		else
			log_msg.AppendFormat("itusearch_nextguess_type = interpolation");
		CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

		log_msg = log_msg_start + CString("Starting search for ");
		log_msg.AppendFormat("ITU Channel Number = %d, ",channel_num);
		log_msg.AppendFormat("ITU Frequency = %.3f [GHz], ",freq_of_channel_num);
		CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

		g_prevent_front_section_switch = false;
		short count_iterations;
		for( count_iterations = 0;
			count_iterations < MAX_ITERATIONS_TO_FIND_ITU_PT;
			count_iterations++ )
		{
			// This loop is trying to find a single ITU point

			bool end_this_itupoint_search = false;
			if(num_iterations_with_problem > 0)
			{
				if( CG_REG->get_DSDBR01_CHAR_prevent_front_section_switch() )
				{
					// After a problem, prevent any further front section switch
					g_prevent_front_section_switch = true;
				}

				// Here, Reg option chooses different ways to deal with problems
				switch(itusearch_scheme)
				{

				case itusearch_scheme_type::quit_itupoint_after_problem:
					// If any problem is encountered, simply move on to next ITU point

					log_msg = log_msg_start + CString("selected quit_itupoint_after_problem");
					CGR_LOG(log_msg,WARNING_CGR_LOG)

					end_this_itupoint_search = true;
					break;

				case itusearch_scheme_type::remove_upper_lower_after_problem:
					// If any problem is encountered,
					// remove the upper and lower points used
					// and try again

					log_msg = log_msg_start + CString("selected remove_upper_lower_after_problem");
					CGR_LOG(log_msg,WARNING_CGR_LOG)

					removeUpperAndLowerButNotEndPoints(
						freq_of_channel_num,
						(double)(0),
						(double)(i_lm->_points.size()-1),
						freq_and_i,
						i_and_min_measured_freq,
						i_and_max_measured_freq,
						i_and_P,
						i_and_Pr);
					break;

				case itusearch_scheme_type::resample_upper_lower_after_problem:
					// If any problem is encountered,
					// remove the upper and lower points used
					// resample them, and try again

					log_msg = log_msg_start + CString("selected resample_upper_lower_after_problem");
					CGR_LOG(log_msg,WARNING_CGR_LOG)

					findUpperAndLower(
						freq_of_channel_num,
						freq_and_i,
						resample_freqA, resample_pointA,
						resample_freqB, resample_pointB);

					removeUpperAndLower(
						freq_of_channel_num,
						freq_and_i,
						i_and_min_measured_freq,
						i_and_max_measured_freq,
						i_and_P,
						i_and_Pr);

					resample_N = 2;

					rval = testNSamples(
						freq_and_i,
						i_and_min_measured_freq,
						i_and_max_measured_freq,
						i_and_P,
						i_and_Pr,
						resample_N,
						resample_pointA,
						resample_pointB,
						i_lm,
						i_sm,
						sm,
						lm );

					if( rval != CDSDBROperatingPoint::rtype::ok )
						end_this_itupoint_search = true;

					break;

				case itusearch_scheme_type::remove_upper_lower_find_N_samples_after_problem:
					// If any problem is encountered,
					// remove the upper and lower points used
					// Take N (min 2) new samples between the upper and lower  
					// and try again

					log_msg = log_msg_start + CString("selected remove_upper_lower_find_N_samples_after_problem");
					CGR_LOG(log_msg,WARNING_CGR_LOG)

					findUpperAndLower(
						freq_of_channel_num,
						freq_and_i,
						resample_freqA, resample_pointA,
						resample_freqB, resample_pointB);

					removeUpperAndLower(
						freq_of_channel_num,
						freq_and_i,
						i_and_min_measured_freq,
						i_and_max_measured_freq,
						i_and_P,
						i_and_Pr);

					resample_N = CG_REG->get_DSDBR01_CHAR_itusearch_resamples();
					if(resample_N < 2) resample_N = 2;

					rval = testNSamples(
						freq_and_i,
						i_and_min_measured_freq,
						i_and_max_measured_freq,
						i_and_P,
						i_and_Pr,
						resample_N,
						resample_pointA,
						resample_pointB,
						i_lm,
						i_sm,
						sm,
						lm );

					if( rval != CDSDBROperatingPoint::rtype::ok )
						end_this_itupoint_search = true;

					break;

				case itusearch_scheme_type::bisection_search_after_problem:
					// If any problem is encountered,
					// remove all except start and end points
					// and try again using bisection instead of interpolation

					itusearch_nextguess = itusearch_nextguess_type::bisection;

					log_msg = log_msg_start + CString("selected bisection_search_after_problem");
					CGR_LOG(log_msg,WARNING_CGR_LOG)

					freq_and_i.clear();
					i_and_min_measured_freq.clear();
					i_and_max_measured_freq.clear();
					i_and_P.clear();
					i_and_Pr.clear();

					freq_and_i.insert( std::map<double,double>::value_type(frequency_at_first_pt,(double)0) );
					i_and_min_measured_freq.insert( std::map<double,double>::value_type((double)0,min_measured_freq_at_first_pt) );
					i_and_max_measured_freq.insert( std::map<double,double>::value_type((double)0,max_measured_freq_at_first_pt) );
					i_and_P.insert( std::map<double,double>::value_type((double)0,P_at_first_pt) );
					i_and_Pr.insert( std::map<double,double>::value_type((double)0,Pr_at_first_pt) );

					freq_and_i.insert( std::map<double,double>::value_type(frequency_at_last_pt,(double)(i_lm->_points.size()-1)) );
					i_and_min_measured_freq.insert( std::map<double,double>::value_type((double)(i_lm->_points.size()-1),min_measured_freq_at_last_pt) );
					i_and_max_measured_freq.insert( std::map<double,double>::value_type((double)(i_lm->_points.size()-1),max_measured_freq_at_last_pt) );
					i_and_P.insert( std::map<double,double>::value_type((double)(i_lm->_points.size()-1),P_at_last_pt) );
					i_and_Pr.insert( std::map<double,double>::value_type((double)(i_lm->_points.size()-1),Pr_at_last_pt) );

					break;

				case itusearch_scheme_type::brute_force_search_after_problem:
					// If any problem is encountered,
					// remove all except start and end points
					// Take N (min 2) new samples between start and end points  
					// and try again

					log_msg = log_msg_start + CString("selected brute_force_search_after_problem");
					CGR_LOG(log_msg,WARNING_CGR_LOG)

					freq_and_i.clear();
					i_and_min_measured_freq.clear();
					i_and_max_measured_freq.clear();
					i_and_P.clear();
					i_and_Pr.clear();

					freq_and_i.insert( std::map<double,double>::value_type(frequency_at_first_pt,(double)0) );
					i_and_min_measured_freq.insert( std::map<double,double>::value_type((double)0,min_measured_freq_at_first_pt) );
					i_and_max_measured_freq.insert( std::map<double,double>::value_type((double)0,max_measured_freq_at_first_pt) );
					i_and_P.insert( std::map<double,double>::value_type((double)0,P_at_first_pt) );
					i_and_Pr.insert( std::map<double,double>::value_type((double)0,Pr_at_first_pt) );

					freq_and_i.insert( std::map<double,double>::value_type(frequency_at_last_pt,(double)(i_lm->_points.size()-1)) );
					i_and_min_measured_freq.insert( std::map<double,double>::value_type((double)(i_lm->_points.size()-1),min_measured_freq_at_last_pt) );
					i_and_max_measured_freq.insert( std::map<double,double>::value_type((double)(i_lm->_points.size()-1),max_measured_freq_at_last_pt) );
					i_and_P.insert( std::map<double,double>::value_type((double)(i_lm->_points.size()-1),P_at_last_pt) );
					i_and_Pr.insert( std::map<double,double>::value_type((double)(i_lm->_points.size()-1),Pr_at_last_pt) );

					resample_N = CG_REG->get_DSDBR01_CHAR_itusearch_resamples();
					if(resample_N < 2) resample_N = 2;

					rval = testNSamples(
						freq_and_i,
						i_and_min_measured_freq,
						i_and_max_measured_freq,
						i_and_P,
						i_and_Pr,
						resample_N,
						(double)0, // first point
						(double)(i_lm->_points.size()-1), // last point
						i_lm,
						i_sm,
						sm,
						lm );

					if( rval != CDSDBROperatingPoint::rtype::ok )
						end_this_itupoint_search = true;

					break;

				default:
					log_msg = log_msg_start + CString("Stopping ITU point search, requested itusearch scheme unknown");
					CGR_LOG(log_msg,WARNING_CGR_LOG)
					end_this_itupoint_search = true;
				}

				// Assume problem is sorted out
				problem_with_hardware = false;
				problem_with_freq_and_i_table = false;

				if( num_iterations_with_problem > MAX_ITERATIONS_WITH_PROBLEM )
				{ // looks like the solution is not forthcoming, quit the ITU point search
					log_msg = log_msg_start;
					log_msg.AppendFormat("Stopping ITU point search after %d iterations with problems",num_iterations_with_problem);
					CGR_LOG(log_msg,WARNING_CGR_LOG)
					end_this_itupoint_search = true;
				}

			} // end of if( num_iterations_with_problem > 0)

			if(!end_this_itupoint_search)
			{
				bool found_ITUpoint_without_guess = false;

				findUpperAndLower(
					freq_of_channel_num,
					freq_and_i,
					freq_upper, point_upper,
					freq_lower, point_lower);

				log_msg = log_msg_start + CString("Found lower frequency point ");
				log_msg.AppendFormat("point_lower = %g, ",point_lower);
				log_msg.AppendFormat("freq_lower = %.3f [GHz], ",freq_lower);
				CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

				if( fabs(freq_lower - freq_of_channel_num) <= wavemeter_error_ghz )
				{
					log_msg = log_msg_start + CString("lower frequency is at ITU point ");
					log_msg.AppendFormat("%.3f [GHz], ",freq_of_channel_num);
					CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
					found_ITUpoint_without_guess = true;
				}

				log_msg = log_msg_start + CString("Found upper frequency point ");
				log_msg.AppendFormat("point_upper = %.3f, ",point_upper);
				log_msg.AppendFormat("freq_upper = %.3f [GHz], ",freq_upper);
				CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

				if( fabs(freq_upper - freq_of_channel_num) <= wavemeter_error_ghz )
				{
					log_msg = log_msg_start + CString("upper frequency is at ITU point ");
					log_msg.AppendFormat("%.3f [GHz], ",freq_of_channel_num);
					CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
					found_ITUpoint_without_guess = true;
				}

				if( !found_ITUpoint_without_guess && freq_upper <= freq_lower )
				{
					log_msg = log_msg_start + CString("freq_upper <= freq_lower");
					CGR_LOG(log_msg,ERROR_CGR_LOG)
					problem_with_freq_and_i_table = true; // deal with problem at start of next iteration
				}
				if( !found_ITUpoint_without_guess && fabs(point_upper - point_lower) < MIN_PT_GAP_TO_FIND_ITU_PT )
				{
					log_msg = log_msg_start;
					log_msg.AppendFormat("fabs(point_upper - point_lower) < MIN GAP (%g)",MIN_PT_GAP_TO_FIND_ITU_PT);
					CGR_LOG(log_msg,ERROR_CGR_LOG)
					problem_with_freq_and_i_table = true; // deal with problem at start of next iteration
				}
				if( !found_ITUpoint_without_guess && freq_of_channel_num > freq_upper )
				{
					log_msg = log_msg_start + CString("freq_of_channel_num > freq_upper");
					CGR_LOG(log_msg,ERROR_CGR_LOG)
					problem_with_freq_and_i_table = true; // deal with problem at start of next iteration
				}
				if( !found_ITUpoint_without_guess && freq_of_channel_num < freq_lower )
				{
					log_msg = log_msg_start + CString("freq_of_channel_num < freq_lower");
					CGR_LOG(log_msg,ERROR_CGR_LOG)
					problem_with_freq_and_i_table = true; // deal with problem at start of next iteration
				}

				if( !found_ITUpoint_without_guess && !problem_with_freq_and_i_table )
				{
					if( itusearch_nextguess == itusearch_nextguess_type::bisection )
					{
						// Calculate bisection point
						guess_i = 0.5*(point_upper-point_lower) + point_lower;
					}
					else
					{
						// Calculate interpolation point
						double fraction_from_lower = (freq_of_channel_num-freq_lower)/(freq_upper-freq_lower);
						guess_i = fraction_from_lower*(point_upper-point_lower) + point_lower;
					}

					log_msg = log_msg_start;
					log_msg.AppendFormat("new test guess point at %g",guess_i);
					CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

					if( fabs(guess_i-last_guess_i) < MIN_PT_GAP_TO_FIND_ITU_PT )
					{
						// too small a gap, can't find ITU point, move on to next.
						log_msg = log_msg_start;
						log_msg.AppendFormat("last interpolation point was at %g, ",last_guess_i);
						log_msg.AppendFormat("gap < MIN GAP (%g)",MIN_PT_GAP_TO_FIND_ITU_PT);
						CGR_LOG(log_msg,WARNING_CGR_LOG)
						problem_with_freq_and_i_table = true; // deal with problem at start of next iteration
					}

					if( guess_i < 0 || guess_i > (double)(i_lm->_points.size())-1 )
					{
						// obviously out of bounds, ignore this ITU OP
						log_msg = log_msg_start;
						log_msg.AppendFormat("new test interpolation point (%g) is outside the bounds of the line",guess_i);
						CGR_LOG(log_msg,WARNING_CGR_LOG)
						problem_with_freq_and_i_table = true; // deal with problem at start of next iteration
					}

					if(!problem_with_freq_and_i_table)
					{
						last_guess_i = guess_i;

						CDSDBROperatingPoint test_op( i_sm, i_lm, sm, lm, guess_i );


						log_msg = log_msg_start + CString("Applying new test interpolation point");
						CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

						CDSDBROperatingPoint::rtype rval_apply = test_op.apply();

						if( rval_apply != CDSDBROperatingPoint::rtype::ok )
						{
							log_msg = log_msg_start + CString("Error applying test interpolation point");
							CGR_LOG(log_msg,ERROR_CGR_LOG)
							rval = rtype::error_applying_op;
							problem_with_hardware = true;
						}
						else
						{

							if( settle_time_ms > 0 )
								Sleep( (DWORD)settle_time_ms );


							// take a frequency measurement

							rval = measure_F_P_Pr(
									freq_i,
									min_measured_freq,
									max_measured_freq,
									P_i,
									Pr_i,
									CG_REG->get_DSDBR01_CHAR_num_freq_meas_per_op(),
									CG_REG->get_DSDBR01_CHAR_num_power_ratio_meas_per_op() );

							if( rval != rtype::ok )
							{
								log_msg = log_msg_start + CString("Error measuring frequency at test interpolation point");
								CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
								problem_with_hardware = true;
							}
							else
							{
								new_pt.sm = sm;
								new_pt.lm = lm;
								new_pt.i = guess_i;

								// For each point on each lm of each sm
								// insert the power ratio and the point to a map.
								// On insertion, the points in the multimap are
								// automatically sorted by Pr value.

								log_msg = log_msg_start;
								log_msg.AppendFormat("At test point = %g, ",guess_i);
								log_msg.AppendFormat("Frequency = %.3f, ",freq_i);
								log_msg.AppendFormat("Power = %g, ",P_i);
								log_msg.AppendFormat("Power Ratio = %g",Pr_i);
								CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

								log_msg = log_msg_start + CString("Adding test sample to map of all samples in LM");
								CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

								_Freq_pts_on_freq_lines.insert(
									std::multimap<double,lm_freq_line_point_type2>::value_type(
										freq_i,
										new_pt ) );

								freq_and_i.insert( std::map<double,double>::value_type( freq_i, guess_i ) );
								i_and_min_measured_freq.insert( std::map<double,double>::value_type(guess_i,min_measured_freq) );
								i_and_max_measured_freq.insert( std::map<double,double>::value_type(guess_i,max_measured_freq) );
								i_and_P.insert( std::map<double,double>::value_type(guess_i,P_i) );
								i_and_Pr.insert( std::map<double,double>::value_type(guess_i,Pr_i) );

							} // end of if !error from measure_F_P_Pr

						} // end of if !error from test_op.apply()

					} // end of if(!problem_with_freq_and_i_table)

				} // end of if(!problem_with_freq_and_i_table)

				if( found_ITUpoint_without_guess || problem_with_freq_and_i_table || problem_with_hardware )
				{
					// use nearest freq to ITU point (upper or lower)
					// just in case it's good enough!

					if( fabs(freq_upper-freq_of_channel_num) < fabs(freq_lower-freq_of_channel_num) )
					{
						freq_i = freq_upper;
						guess_i = point_upper;
					}
					else
					{
						freq_i = freq_lower;
						guess_i = point_lower;
					}

					// find previously recorded values

					std::map<double,double>::iterator map_iter = i_and_min_measured_freq.find( guess_i );
					if( map_iter != i_and_min_measured_freq.end() )
						min_measured_freq = (*map_iter).second;

					map_iter = i_and_max_measured_freq.find( guess_i );
					if( map_iter != i_and_max_measured_freq.end() )
						max_measured_freq = (*map_iter).second;

					map_iter = i_and_P.find( guess_i );
					if( map_iter != i_and_P.end() )
						P_i = (*map_iter).second;

					map_iter = i_and_Pr.find( guess_i );
					if( map_iter != i_and_Pr.end() )
						Pr_i = (*map_iter).second;
				}

			} //end if end_this_itupoint_search

			if( count_iterations == MAX_ITERATIONS_TO_FIND_ITU_PT-1 )
			{ // This is the last time though count_iterations loop
			  // ensure last guess is written if duff points requested
				end_this_itupoint_search = true;
			}

			if( freq_i != prev_recorded_freq_i // prevent the same freq being recorded in a loop
			 && ((!end_this_itupoint_search && fabs(freq_i-freq_of_channel_num) <= wavemeter_error_ghz)
			 || (end_this_itupoint_search && CG_REG->get_DSDBR01_CHAR_write_duff_points_to_file())) )
			{
				prev_recorded_freq_i = freq_i;

				if(fabs(freq_i-freq_of_channel_num) > wavemeter_error_ghz)
				{
					incrementDuffITUOPCount();
					log_msg = log_msg_start + CString("Saving last guess sample point for ITU Frequency, ");
					log_msg.AppendFormat("fabs(%.3f-%.3f) > wavemeter_error_ghz (%g)",freq_i,freq_of_channel_num,wavemeter_error_ghz);
					CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
				}
				else
				{
					log_msg = log_msg_start + CString("Found ITU Frequency, ");
					log_msg.AppendFormat("fabs(%.3f-%.3f) <= wavemeter_error_ghz (%g)",freq_i,freq_of_channel_num,wavemeter_error_ghz);
					CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
				}

				// store the ITU operating point

				CDSDBROperatingPoint new_itu_operating_pt;
				_characterisation_itu_ops.push_back(new_itu_operating_pt);

				CDSDBROperatingPoint test_op( i_sm, i_lm, sm, lm, guess_i );
				double Pr_on_sm_map = *(i_sm->_map_forward_power_ratio.iPt( test_op._row_on_sm_map, test_op._col_on_sm_map ));

				//Add details of sample point to object in vector

				_characterisation_itu_ops.rbegin()->_ITU_channel_number = channel_num_of_itu_pts_in_lm[i];
				_characterisation_itu_ops.rbegin()->_ITU_frequency = freq_of_channel_num;

				_characterisation_itu_ops.rbegin()->_row_on_sm_map = test_op._row_on_sm_map;
				_characterisation_itu_ops.rbegin()->_col_on_sm_map = test_op._col_on_sm_map;
				_characterisation_itu_ops.rbegin()->_Pr_on_sm_map = Pr_on_sm_map;

				_characterisation_itu_ops.rbegin()->_sm_number = sm;
				_characterisation_itu_ops.rbegin()->_lm_number = lm;
				_characterisation_itu_ops.rbegin()->_index_on_middle_line_of_frequency = (short)guess_i;
				_characterisation_itu_ops.rbegin()->_real_index_on_middle_line_of_frequency = guess_i;
				_characterisation_itu_ops.rbegin()->_front_pair_number = test_op._front_pair_number;
				_characterisation_itu_ops.rbegin()->_I_gain = test_op._I_gain;
				_characterisation_itu_ops.rbegin()->_I_soa = test_op._I_soa;
				_characterisation_itu_ops.rbegin()->_I_rear = test_op._I_rear;
				_characterisation_itu_ops.rbegin()->_I_front_1 = test_op._I_front_1;
				_characterisation_itu_ops.rbegin()->_I_front_2 = test_op._I_front_2;
				_characterisation_itu_ops.rbegin()->_I_front_3 = test_op._I_front_3;
				_characterisation_itu_ops.rbegin()->_I_front_4 = test_op._I_front_4;
				_characterisation_itu_ops.rbegin()->_I_front_5 = test_op._I_front_5;
				_characterisation_itu_ops.rbegin()->_I_front_6 = test_op._I_front_6;
				_characterisation_itu_ops.rbegin()->_I_front_7 = test_op._I_front_7;
				_characterisation_itu_ops.rbegin()->_I_front_8 = test_op._I_front_8;
				_characterisation_itu_ops.rbegin()->_I_phase = test_op._I_phase;

				_characterisation_itu_ops.rbegin()->_F_measured = freq_i;
				_characterisation_itu_ops.rbegin()->_P_ratio_measured = Pr_i;
				_characterisation_itu_ops.rbegin()->_P_measured = P_i;

				if( max_measured_freq - min_measured_freq > CG_REG->get_DSDBR01_CHAR_max_stable_freq_error() )
				{
					_characterisation_itu_ops.rbegin()->_stable_F_measurement = false;

					log_msg = log_msg_start + CString("ITU Frequency recorded as having unstable frequency measurement");
					CGR_LOG(log_msg,WARNING_CGR_LOG)
				}
				else
				{
					_characterisation_itu_ops.rbegin()->_stable_F_measurement = true;
				}

				if(fabs(freq_i-freq_of_channel_num) <= wavemeter_error_ghz)
				{
					_characterisation_itu_ops.rbegin()->_is_an_ITU_point = true;
				}
				else
				{
					_characterisation_itu_ops.rbegin()->_is_an_ITU_point = false;
				}


				// found and stored ITU point, break and move onto next
				break;
			}

			if( end_this_itupoint_search ) break;

			if( problem_with_freq_and_i_table || problem_with_hardware )
			{
				num_iterations_with_problem++;
				num_iterations_without_problem = 0;
			}
			else
			{
				num_iterations_with_problem = 0;
				num_iterations_without_problem++;

				if( num_iterations_without_problem > MAX_ITERATIONS_WITHOUT_PROBLEM )
				{ // Could be stuck in an endless loop, treat as a problem!

					log_msg = log_msg_start;
					log_msg.AppendFormat("ITU point search performed %d iterations without success",num_iterations_without_problem);
					CGR_LOG(log_msg,WARNING_CGR_LOG)

					num_iterations_with_problem++;
					num_iterations_without_problem = 0;
					problem_with_freq_and_i_table = true;
				}
			}
		} // End of count_iterations Loop
		g_prevent_front_section_switch = false;

		if( count_iterations == MAX_ITERATIONS_TO_FIND_ITU_PT )
		{
			log_msg = log_msg_start;
			log_msg.AppendFormat("ITU point search exited after %d iterations without success",count_iterations);
			CGR_LOG(log_msg,WARNING_CGR_LOG)
		}

		//if( rval != rtype::ok ) break; // this indicates HW related problems
	}

	if( rval == rtype::invalid_frequency_from_wavemeter )
	{
		// indicates instability, simply move on to next lm
		log_msg = log_msg_start + CString("invalid frequency returned from wavemeter");
		CGR_LOG(log_msg,WARNING_CGR_LOG)
		rval = rtype::ok;
	}

	log_msg = log_msg_start + CString("exiting");
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	return rval;
}


void
CCGOperatingPoints::findUpperAndLower(
    double freq_of_channel_num,
	std::map<double,double> &freq_and_i,
	double &freq_upper,
	double &point_upper,
	double &freq_lower,
	double &point_lower)
{
	freq_upper = MAX_VALID_FREQ_MEAS;
	point_upper = 0;
	freq_lower = MIN_VALID_FREQ_MEAS;
	point_lower = 0;

	for( std::map<double,double>::iterator map_iter = freq_and_i.begin();
			map_iter != freq_and_i.end(); 
			map_iter++ )
	{
		if( (*map_iter).first > freq_of_channel_num
			&& (*map_iter).first < freq_upper )
		{
			freq_upper = (*map_iter).first;
			point_upper = (*map_iter).second;
		}

		if( (*map_iter).first < freq_of_channel_num
			&& (*map_iter).first > freq_lower )
		{
			freq_lower = (*map_iter).first;
			point_lower = (*map_iter).second;
		}
	}
}


void
CCGOperatingPoints::removeUpperAndLower(
	double freq_of_channel_num,
	std::map<double,double> &freq_and_i,
	std::map<double,double> &i_and_min_measured_freq,
	std::map<double,double> &i_and_max_measured_freq,
	std::map<double,double> &i_and_P,
	std::map<double,double> &i_and_Pr)
{
	removeUpperAndLowerButNotEndPoints(
		freq_of_channel_num,
		-1, // lower index that cannot exist
		9999999, // upper index that cannot exist
		freq_and_i,
		i_and_min_measured_freq,
		i_and_max_measured_freq,
		i_and_P,
		i_and_Pr);
}

void
CCGOperatingPoints::removeUpperAndLowerButNotEndPoints(
	double freq_of_channel_num,
	double first_point_index,
	double last_point_index,
	std::map<double,double> &freq_and_i,
	std::map<double,double> &i_and_min_measured_freq,
	std::map<double,double> &i_and_max_measured_freq,
	std::map<double,double> &i_and_P,
	std::map<double,double> &i_and_Pr)
{
	double freq_upper;
	double point_upper;
	double freq_lower;
	double point_lower;

	findUpperAndLower(
		freq_of_channel_num,
		freq_and_i,
		freq_upper,
		point_upper,
		freq_lower,
		point_lower);

	// Don't remove first or last point
	if( point_upper > first_point_index
	 && point_upper < last_point_index )
	{
		freq_and_i.erase( freq_upper );
		i_and_min_measured_freq.erase( point_upper );
		i_and_max_measured_freq.erase( point_upper );
		i_and_P.erase( point_upper );
		i_and_Pr.erase( point_upper );
	}

	// Don't remove first or last point
	if( point_lower > first_point_index
	 && point_lower < last_point_index )
	{
		freq_and_i.erase( freq_lower );
		i_and_min_measured_freq.erase( point_lower );
		i_and_max_measured_freq.erase( point_lower );
		i_and_P.erase( point_lower );
		i_and_Pr.erase( point_lower );
	}
}


CCGOperatingPoints::rtype
CCGOperatingPoints::testNSamples(
	std::map<double,double> &freq_and_i,
	std::map<double,double> &i_and_min_measured_freq,
	std::map<double,double> &i_and_max_measured_freq,
	std::map<double,double> &i_and_P,
	std::map<double,double> &i_and_Pr,
	int number_of_samples,
	double first_point,
	double last_point,
	std::vector<CDSDBRSupermodeMapLine>::iterator i_lm,
	std::vector<CDSDBRSuperMode>::iterator i_sm,
	short sm,
	short lm)
{
	rtype rval = rtype::ok;
	double index_of_point_to_sample;
	double freq_i;
	double min_measured_freq;
	double max_measured_freq;
	double P_i;
	double Pr_i;

	CString log_msg_start("CCGOperatingPoints::testNSamples(): ");
	CString log_msg = log_msg_start + CString("entered");
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	int settle_time_ms = CG_REG->get_DSDBR01_CHAR_op_settle_time();

	for( int i = 0; i < number_of_samples; i++  )
	{
		if( i == 0 )
		{	// first point
			index_of_point_to_sample = first_point;
		}
		else if ( i == number_of_samples-1 )
		{
			// last point
			index_of_point_to_sample = last_point;
		}
		else
		{
			index_of_point_to_sample = first_point
				+ (double)(i)*(last_point-first_point)/(double)(number_of_samples-1);
		}

		CDSDBROperatingPoint test_op( i_sm, i_lm, sm, lm, index_of_point_to_sample );

		log_msg = log_msg_start + CString("Applying new sample point");
		CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

		CDSDBROperatingPoint::rtype rval_apply = test_op.apply();

		if( rval_apply != CDSDBROperatingPoint::rtype::ok )
		{
			log_msg = log_msg_start + CString("Error applying sample point");
			CGR_LOG(log_msg,ERROR_CGR_LOG)
			rval = rtype::error_applying_op;
			break;
		}
		else
		{

			if( settle_time_ms > 0 )
				Sleep( (DWORD)settle_time_ms );

			// take a frequency measurement

			rval = measure_F_P_Pr(
					freq_i,
					min_measured_freq,
					max_measured_freq,
					P_i,
					Pr_i,
					CG_REG->get_DSDBR01_CHAR_num_freq_meas_per_op(),
					CG_REG->get_DSDBR01_CHAR_num_power_ratio_meas_per_op() );

			if( rval != rtype::ok )
			{
				log_msg = log_msg_start + CString("Error measuring frequency at sample point");
				CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
				break;
			}
			else
			{
				lm_freq_line_point_type2 new_pt;
				new_pt.sm = sm;
				new_pt.lm = lm;
				new_pt.i = index_of_point_to_sample;

				// For each point on each lm of each sm
				// insert the power ratio and the point to a map.
				// On insertion, the points in the multimap are
				// automatically sorted by Pr value.

				log_msg = log_msg_start;
				log_msg.AppendFormat("At sample point = %g, ",index_of_point_to_sample);
				log_msg.AppendFormat("Frequency = %.3f, ",freq_i);
				log_msg.AppendFormat("Power = %g, ",P_i);
				log_msg.AppendFormat("Power Ratio = %g",Pr_i);
				CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

				log_msg = log_msg_start + CString("Adding test sample to map of all samples in LM");
				CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

				_Freq_pts_on_freq_lines.insert(
					std::multimap<double,lm_freq_line_point_type2>::value_type(
						freq_i,
						new_pt ) );

				freq_and_i.insert( std::map<double,double>::value_type( freq_i, index_of_point_to_sample ) );
				i_and_min_measured_freq.insert( std::map<double,double>::value_type(index_of_point_to_sample,min_measured_freq) );
				i_and_max_measured_freq.insert( std::map<double,double>::value_type(index_of_point_to_sample,max_measured_freq) );
				i_and_P.insert( std::map<double,double>::value_type(index_of_point_to_sample,P_i) );
				i_and_Pr.insert( std::map<double,double>::value_type(index_of_point_to_sample,Pr_i) );

			} // end of if !error from measure_F_P_Pr

		} // end of if !error from test_op.apply()

	}

	log_msg = log_msg_start + CString("exiting");
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	return rval;
}



CCGOperatingPoints::rtype
CCGOperatingPoints::interpolationRoutine(
	double frequency_at_first_pt,
	double frequency_at_last_pt,
	std::vector<CDSDBRSupermodeMapLine>::iterator i_lm,
	std::vector<CDSDBRSuperMode>::iterator i_sm,
	short sm,
	short lm )
{
	rtype rval = rtype::ok;

	CGR_LOG(CString("CCGOperatingPoints::interpolationRoutine() entered"),DIAGNOSTIC_CGR_LOG)

	int settle_time_ms = CG_REG->get_DSDBR01_CHAR_op_settle_time();
	double wavemeter_error_ghz = CG_REG->get_DSDBR01_CHAR_freq_accuracy_of_wavemeter();

	// clear map of samples taken in an LM, add first and last to start with
	CString log_msg_start("CCGOperatingPoints::interpolationRoutine(): ");
	CString log_msg = log_msg_start + CString("Clearing map of samples in LM ");
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	std::map<double,double> freq_and_i;
	freq_and_i.clear();

	log_msg = log_msg_start + CString("Adding first and last points to map of samples in LM ");
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	freq_and_i.insert( std::map<double,double>::value_type(frequency_at_first_pt,(double)0) );
	freq_and_i.insert( std::map<double,double>::value_type(frequency_at_last_pt,(double)(i_lm->_points.size()-1)) );

	lm_freq_line_point_type2 new_pt;
	new_pt.sm = sm;
	new_pt.lm = lm;
	new_pt.i = (double)0;

	_Freq_pts_on_freq_lines.insert(
		std::multimap<double,lm_freq_line_point_type2>::value_type(
			frequency_at_first_pt,
			new_pt ) );

	new_pt.i = (double)(i_lm->_points.size()-1);

	_Freq_pts_on_freq_lines.insert(
		std::multimap<double,lm_freq_line_point_type2>::value_type(
			frequency_at_last_pt,
			new_pt ) );

	// identify ITU points to be found

	log_msg = log_msg_start + CString("Identifying ITU points to search for in LM");
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	std::vector<short> channel_num_of_itu_pts_in_lm;
	std::vector<double> freq_of_itu_pts_in_lm;

	double min_freq = 0;
	double max_freq = 0;
	if( frequency_at_last_pt > frequency_at_first_pt )
	{
		min_freq = frequency_at_first_pt + wavemeter_error_ghz;
		max_freq = frequency_at_last_pt - wavemeter_error_ghz;
	}
	else
	{
		min_freq = frequency_at_last_pt + wavemeter_error_ghz;
		max_freq = frequency_at_first_pt - wavemeter_error_ghz;
	}

	_itugrid.findITUPtsInRange(
		channel_num_of_itu_pts_in_lm,
		freq_of_itu_pts_in_lm,
		min_freq,
		max_freq );

	for( short i = 0; i < (short)channel_num_of_itu_pts_in_lm.size(); i++ )
	{
		// for each ITU point to be found
		//    start with best guess,
		//    then iterate to ITU point within specified accuracy of wavemeter
		//    record the ITU point's details

		short channel_num = channel_num_of_itu_pts_in_lm[i];
		double freq_of_channel_num = freq_of_itu_pts_in_lm[i];
		double freq_upper = MAX_VALID_FREQ_MEAS;
		double point_upper = 0;
		double freq_lower = MIN_VALID_FREQ_MEAS;
		double point_lower = 0;
		double interpolated_i = 0;
		double last_interpolated_i = -1;
		//int num_of_poly_coeffs = 3;

		log_msg = log_msg_start + CString("Starting search for ");
		log_msg.AppendFormat("ITU Channel Number = %d, ",channel_num);
		log_msg.AppendFormat("ITU Frequency = %.3f [GHz], ",freq_of_channel_num);
		CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

		for( short count_iterations = 0;
			count_iterations < MAX_ITERATIONS_TO_FIND_ITU_PT ;
			count_iterations++ )
		{
			// if this loop ends without finding the ITU point, it is simply ignored

			for( std::map<double,double>::iterator map_iter = freq_and_i.begin();
					map_iter != freq_and_i.end(); 
					map_iter++ )
			{
				if( (*map_iter).first > freq_of_channel_num
					&& (*map_iter).first < freq_upper )
				{
					freq_upper = (*map_iter).first;
					point_upper = (*map_iter).second;
				}

				if( (*map_iter).first < freq_of_channel_num
					&& (*map_iter).first > freq_lower )
				{
					freq_lower = (*map_iter).first;
					point_lower = (*map_iter).second;
				}
			}

			log_msg = log_msg_start + CString("Found lower frequency point ");
			log_msg.AppendFormat("point_lower = %g, ",point_lower);
			log_msg.AppendFormat("freq_lower = %.3f [GHz], ",freq_lower);
			CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

			log_msg = log_msg_start + CString("Found upper frequency point ");
			log_msg.AppendFormat("point_upper = %g, ",point_upper);
			log_msg.AppendFormat("freq_upper = %.3f [GHz], ",freq_upper);
			CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

			if( freq_upper <= freq_lower )
			{
				log_msg = log_msg_start + CString("freq_upper <= freq_lower");
				CGR_LOG(log_msg,ERROR_CGR_LOG)
				break; // ignore it, move on to next ITU pt
			}
			if( fabs(point_upper - point_lower) < MIN_PT_GAP_TO_FIND_ITU_PT )
			{
				log_msg = log_msg_start;
				log_msg.AppendFormat("fabs(point_upper - point_lower) < MIN GAP (%g)",MIN_PT_GAP_TO_FIND_ITU_PT);
				CGR_LOG(log_msg,ERROR_CGR_LOG)
				break; // ignore it, move on to next ITU pt
			}
			if( freq_of_channel_num > freq_upper )
			{
				log_msg = log_msg_start + CString("freq_of_channel_num > freq_upper");
				CGR_LOG(log_msg,ERROR_CGR_LOG)
				break; // ignore it, move on to next ITU pt
			}
			if( freq_of_channel_num < freq_lower )
			{
				log_msg = log_msg_start + CString("freq_of_channel_num < freq_lower");
				CGR_LOG(log_msg,ERROR_CGR_LOG)
				break; // ignore it, move on to next ITU pt
			}

			//interpolated_i = (point_upper-point_lower)/2 + point_lower;
			double fraction_from_lower = (freq_of_channel_num-freq_lower)/(freq_upper-freq_lower);
			interpolated_i = fraction_from_lower*(point_upper-point_lower) + point_lower;

			log_msg = log_msg_start;
			log_msg.AppendFormat("new test interpolation point at %g",interpolated_i);
			CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

			if( fabs(interpolated_i-last_interpolated_i) < MIN_PT_GAP_TO_FIND_ITU_PT )
			{
				// too small a gap, can't find ITU point, move on to next.
				log_msg = log_msg_start;
				log_msg.AppendFormat("last interpolation point was at %g, ",last_interpolated_i);
				log_msg.AppendFormat("gap < MIN GAP (%g), ignore this ITU point",MIN_PT_GAP_TO_FIND_ITU_PT);
				CGR_LOG(log_msg,ERROR_CGR_LOG)
				break;
			}

			if( interpolated_i < 0 || interpolated_i > (double)(i_lm->_points.size())-1 )
			{
				// obviously out of bounds, ignore this ITU OP
				log_msg = log_msg_start;
				log_msg.AppendFormat("new test interpolation point (%g) is outside the bounds of the line, ignore this ITU point",interpolated_i);
				CGR_LOG(log_msg,ERROR_CGR_LOG)
				break;
			}

			last_interpolated_i = interpolated_i;

			CDSDBROperatingPoint test_op( i_sm, i_lm, sm, lm, interpolated_i );


			log_msg = log_msg_start + CString("Applying new test interpolation point");
			CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

			CDSDBROperatingPoint::rtype rval_apply = test_op.apply();

			if( rval_apply != CDSDBROperatingPoint::rtype::ok )
			{
				log_msg = log_msg_start + CString("Error applying test interpolation point");
				CGR_LOG(log_msg,ERROR_CGR_LOG)
				rval = rtype::error_applying_op;
				break;
			}

			if( settle_time_ms > 0 )
				Sleep( (DWORD)settle_time_ms );


			// take a frequency measurement

			double Pr_i = 0;
			double P_i = 0;
			double freq_i = 0;
			double max_measured_freq = 0;
			double min_measured_freq = 0;

			rval = measure_F_P_Pr(
					freq_i,
					min_measured_freq,
					max_measured_freq,
					P_i,
					Pr_i,
					CG_REG->get_DSDBR01_CHAR_num_freq_meas_per_op(),
					CG_REG->get_DSDBR01_CHAR_num_power_ratio_meas_per_op() );

			if( rval != rtype::ok )
			{
				log_msg = log_msg_start + CString("Error measuring frequency at test interpolation point");
				CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)
				break;
			}

			new_pt.sm = sm;
			new_pt.lm = lm;
			new_pt.i = interpolated_i;

			// For each point on each lm of each sm
			// insert the power ratio and the point to a map.
			// On insertion, the points in the multimap are
			// automatically sorted by Pr value.

			log_msg = log_msg_start;
			log_msg.AppendFormat("At test point = %g, ",interpolated_i);
			log_msg.AppendFormat("Frequency = %.3f, ",freq_i);
			log_msg.AppendFormat("Power = %g, ",P_i);
			log_msg.AppendFormat("Power Ratio = %g",Pr_i);
			CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

			log_msg = log_msg_start + CString("Adding test sample to map of all samples in LM");
			CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

			_Freq_pts_on_freq_lines.insert(
				std::multimap<double,lm_freq_line_point_type2>::value_type(
					freq_i,
					new_pt ) );

			freq_and_i.insert( std::map<double,double>::value_type( freq_i, interpolated_i ) );

			if( fabs(freq_i-freq_of_channel_num) < wavemeter_error_ghz )
			{
				log_msg = log_msg_start + CString("Found ITU Frequency, ");
				log_msg.AppendFormat("fabs(%.3f-%.3f) < wavemeter_error_ghz (%g)",freq_i,freq_of_channel_num,wavemeter_error_ghz);
				CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

				// found ITU freq to within accuracy of wavemeter
				// store the ITU operating point

				CDSDBROperatingPoint new_itu_operating_pt;
				_characterisation_itu_ops.push_back(new_itu_operating_pt);

				double Pr_on_sm_map = *(i_sm->_map_forward_power_ratio.iPt( test_op._row_on_sm_map, test_op._col_on_sm_map ));

				//Add details of sample point to object in vector


				_characterisation_itu_ops.rbegin()->_ITU_channel_number = channel_num_of_itu_pts_in_lm[i];
				_characterisation_itu_ops.rbegin()->_ITU_frequency = freq_of_channel_num;

				_characterisation_itu_ops.rbegin()->_row_on_sm_map = test_op._row_on_sm_map;
				_characterisation_itu_ops.rbegin()->_col_on_sm_map = test_op._col_on_sm_map;
				_characterisation_itu_ops.rbegin()->_Pr_on_sm_map = Pr_on_sm_map;

				_characterisation_itu_ops.rbegin()->_sm_number = sm;
				_characterisation_itu_ops.rbegin()->_lm_number = lm;
				_characterisation_itu_ops.rbegin()->_index_on_middle_line_of_frequency = (short)interpolated_i;
				_characterisation_itu_ops.rbegin()->_real_index_on_middle_line_of_frequency = interpolated_i;
				_characterisation_itu_ops.rbegin()->_front_pair_number = test_op._front_pair_number;
				_characterisation_itu_ops.rbegin()->_I_gain = test_op._I_gain;
				_characterisation_itu_ops.rbegin()->_I_soa = test_op._I_soa;
				_characterisation_itu_ops.rbegin()->_I_rear = test_op._I_rear;
				_characterisation_itu_ops.rbegin()->_I_front_1 = test_op._I_front_1;
				_characterisation_itu_ops.rbegin()->_I_front_2 = test_op._I_front_2;
				_characterisation_itu_ops.rbegin()->_I_front_3 = test_op._I_front_3;
				_characterisation_itu_ops.rbegin()->_I_front_4 = test_op._I_front_4;
				_characterisation_itu_ops.rbegin()->_I_front_5 = test_op._I_front_5;
				_characterisation_itu_ops.rbegin()->_I_front_6 = test_op._I_front_6;
				_characterisation_itu_ops.rbegin()->_I_front_7 = test_op._I_front_7;
				_characterisation_itu_ops.rbegin()->_I_front_8 = test_op._I_front_8;
				_characterisation_itu_ops.rbegin()->_I_phase = test_op._I_phase;

				_characterisation_itu_ops.rbegin()->_F_measured = freq_i;
				_characterisation_itu_ops.rbegin()->_P_ratio_measured = Pr_i;
				_characterisation_itu_ops.rbegin()->_P_measured = P_i;

				if( max_measured_freq - min_measured_freq > CG_REG->get_DSDBR01_CHAR_max_stable_freq_error() )
				{
					_characterisation_itu_ops.rbegin()->_stable_F_measurement = false;

					log_msg = log_msg_start + CString("ITU Frequency recorded as having unstable frequency measurement");
					CGR_LOG(log_msg,WARNING_CGR_LOG)
				}
				else
					_characterisation_itu_ops.rbegin()->_stable_F_measurement = true;

				_characterisation_itu_ops.rbegin()->_is_an_ITU_point = true;

				// found and stored ITU point, break and move onto next
				break;
			}

		}

		if( rval != rtype::ok ) break;
	}

	if( rval == rtype::invalid_frequency_from_wavemeter )
	{
		// indicates instability, simply move on to next lm
		log_msg = log_msg_start + CString("invalid frequency returned from wavemeter");
		CGR_LOG(log_msg,WARNING_CGR_LOG)
		rval = rtype::ok;
	}

	CGR_LOG(CString("CCGOperatingPoints::interpolationRoutine() exiting"),DIAGNOSTIC_CGR_LOG)

	return rval;
}


CCGOperatingPoints::rtype
CCGOperatingPoints::measure_F_P_Pr(
		double& F,
		double& min_measured_freq,
		double& max_measured_freq,
		double& P,
		double& Pr,
		short num_F_samples_to_average,
		short num_P_and_Pr_samples_to_average)
{
	rtype rval = rtype::ok;

	F = 0;
	P = 0;
	Pr = 0;

	int freq_samples = 0;
	int power_ratio_samples = 0;
	short max_j = num_F_samples_to_average > num_P_and_Pr_samples_to_average ? num_F_samples_to_average : num_P_and_Pr_samples_to_average;

	for( short j = 0; j < max_j ; j++ )
	{
		if( freq_samples < num_F_samples_to_average )
		{
			// Take a frequency sample
			double frequency;
			HRESULT hres_freq = SERVERINTERFACE->readFrequency( &frequency );

			if( hres_freq != S_OK )
			{
				rval = rtype::invalid_frequency_from_wavemeter;
				break;
			}

			if( frequency < MIN_VALID_FREQ_MEAS
			 || frequency > MAX_VALID_FREQ_MEAS )
			{
				rval = rtype::invalid_frequency_from_wavemeter;

				CString log_err_msg("CCGOperatingPoints::measure_F_P_Pr() ");
				log_err_msg += CString("invalid_frequency_from_wavemeter");
				CGR_LOG(log_err_msg,ERROR_CGR_LOG)

				break;
			}

			if( j == 0 )
			{ // first time
				max_measured_freq = frequency;
				min_measured_freq = frequency;
			}
			else
			{ // remaining times
				max_measured_freq = max_measured_freq > frequency ? max_measured_freq : frequency;
				min_measured_freq = min_measured_freq < frequency ? min_measured_freq : frequency;
			}

			F += frequency;
			freq_samples++;
		}


		if( power_ratio_samples < num_P_and_Pr_samples_to_average )
		{
			// Take a power ratio sample
			double power_ch1;
			double power_ch2;
			HRESULT hres_power = SERVERINTERFACE->readOpticalPowers(
				CG_REG->get_DSDBR01_CMDC_306II_module_name(),
				&power_ch1,
				&power_ch2 );

			if( hres_power != S_OK )
			{
				rval = rtype::error_reading_power_meter;

				CString log_err_msg("CCGOperatingPoints::measure_F_P_Pr() ");
				log_err_msg += CString("error_reading_power_meter");
				CGR_LOG(log_err_msg,ERROR_CGR_LOG)

				break;
			}

			double power_ratio = 0;
			double direct_power = 0;
			if( CG_REG->get_DSDBR01_CMDC_306II_direct_channel() == CHANNEL_1 )
			{
				power_ratio = power_ch2/power_ch1;
				direct_power = power_ch1;
			}
			else
			{
				power_ratio = power_ch1/power_ch2;
				direct_power = power_ch2;
			}

			if( power_ratio >= 0 && power_ratio <= 1 )
			{
				Pr += power_ratio;
				P += direct_power;
				power_ratio_samples++;
			}
			else
			{
				rval = rtype::power_ratio_out_of_range;

				CString log_err_msg("CCGOperatingPoints::measure_F_P_Pr() ");
				log_err_msg.AppendFormat("power_ratio = %g => out_of_range", power_ratio );
				CGR_LOG(log_err_msg,ERROR_CGR_LOG)

				break;
			}
		}
	}

	F /= (double)freq_samples;
	Pr /= (double)power_ratio_samples;
	P /= (double)power_ratio_samples;

	return rval;
}

CCGOperatingPoints::rtype
CCGOperatingPoints::estimateITUOperatingPoints()
{
	rtype rval = rtype::ok;

	_estimation_itu_ops.clear();

	//gdm210306 mod 1 of 2 in estimateITUOperatingPoints for ITUgrid from csv file
	// set up ITU grip based on registry entries
    if( _itugrid.setupEx(CG_REG->get_DSDBR01_ESTIMATESITUGRID_abspath() ) != CITUGrid::rcode::ok )
    {
        CGR_LOG(CString("CCGOperatingPoints::estimateITUOperatingPoints() ITU Grid Setup Failed"),ERROR_CGR_LOG)
        return rtype::not_enough_data;
    }

	std::multimap<double,lm_freq_line_point_type> coarse_freq_pts_on_freq_lines;

	for( short sm = 0; sm <	(short)(_p_supermodes->size()) ; sm++ )
	{
		std::vector<CDSDBRSuperMode>::iterator i_sm = _p_supermodes->begin();
		i_sm += sm;

		// for each sm

		for( short lm = 0; lm <	(short)(i_sm->_lm_middle_lines_of_frequency.size()) ; lm++ )
		{
			std::vector<CDSDBRSupermodeMapLine>::iterator i_lm
				= i_sm->_lm_middle_lines_of_frequency.begin();
			i_lm += lm;

			// For each lm of each sm

			// First, arrange the points on middle line of frequency
			// by their coarse frequency estimate

			coarse_freq_pts_on_freq_lines.clear();

			for( short i = 0; i < (short)(i_lm->_points.size()) ; i++ )
			{
				// For each point on each lm of each sm
				// get the power ratio and then the coarse frequency

				long row = 0;
				long col = 0;
				if( i_sm->_Im_ramp )
				{
					col = i_lm->_points[i].row;
					row = i_lm->_points[i].col;
				}
				else // Ip ramped
				{
					row = i_lm->_points[i].row;
					col = i_lm->_points[i].col;
				}

				double Pr_value = *(i_sm->_map_forward_power_ratio.iPt( row, col ));

				double coarse_freq_value = getCoarseFreq(Pr_value);

				lm_freq_line_point_type new_pt;
				new_pt.sm = sm;
				new_pt.lm = lm;
				new_pt.i = i;

				// For each point on each lm of each sm
				// insert the coarse frequency and the point to a map.
				// On insertion, the points in the multimap are
				// automatically sorted by coarse frequency value.

				coarse_freq_pts_on_freq_lines.insert(
                    std::multimap<double,lm_freq_line_point_type>::value_type(
						coarse_freq_value,
                        new_pt ) );
			}

			// for each lm search for ITU operating point estimates
			
			for( short itu_channel_num = 0 ;
				itu_channel_num < (short)(_itugrid.channel_count()) ;
				itu_channel_num++ )
			{
				std::multimap<double,lm_freq_line_point_type>::iterator i_pt;
			
				double prev_coarse_freq_value_on_sm_map = 0;
				double coarse_freq_value_on_sm_map = 0;

				for( i_pt = coarse_freq_pts_on_freq_lines.begin() ;
					i_pt != coarse_freq_pts_on_freq_lines.end() ;
					i_pt++ )
				{
					coarse_freq_value_on_sm_map = (*i_pt).first;

					if( coarse_freq_value_on_sm_map >= _itugrid._ITU_frequencies[itu_channel_num]
					 && prev_coarse_freq_value_on_sm_map < _itugrid._ITU_frequencies[itu_channel_num]
					 && prev_coarse_freq_value_on_sm_map > 0 )
					{
						// have found two points either side of an ITU freqeuncy 
						if( coarse_freq_value_on_sm_map - _itugrid._ITU_frequencies[itu_channel_num]
						> fabs(prev_coarse_freq_value_on_sm_map - _itugrid._ITU_frequencies[itu_channel_num]) )
						{
							//previous point was nearer
							coarse_freq_value_on_sm_map = prev_coarse_freq_value_on_sm_map;
							i_pt--;
						}

						// at this point,
						// have found the nearest coarse frequency value to the ITU frequency
						// This assumes continuous tuning in a longitudunal mode
						// such that each ITU point can only occur once in a longitudinal mode
						// but could occur in many different longitudinal modes
						break;
					}

					prev_coarse_freq_value_on_sm_map = coarse_freq_value_on_sm_map;
				}


				if( i_pt != coarse_freq_pts_on_freq_lines.end() )
				{
					// have found an estimate ITU operating point, add to vector

					CDSDBROperatingPoint new_ITU_op_estimate;
					_estimation_itu_ops.push_back(new_ITU_op_estimate);

					double coarse_freq_value_on_sm_map = (*i_pt).first;
					short sm_number = (*i_pt).second.sm;
					short lm_number = (*i_pt).second.lm;
					short i_number = (*i_pt).second.i;

					std::vector<CDSDBRSuperMode>::iterator i_sm = _p_supermodes->begin();
					i_sm += sm_number;
					std::vector<CDSDBRSupermodeMapLine>::iterator i_lm = i_sm->_lm_middle_lines_of_frequency.begin();
					i_lm += lm_number;

					long row_i_number = i_lm->_points[i_number].row;
					long col_i_number = i_lm->_points[i_number].col;
					double Pr_value_on_sm_map = *(i_sm->_map_forward_power_ratio.iPt( row_i_number, col_i_number ));
					//gdm210306 mod 2 of 2 in estimateITUOperatingPoints for ITUgrid from csv file
					//double P_value_on_sm_map = *(i_sm->_map_forward_direct_power.iPt( row_i_number, col_i_number ));
					double P_value_on_sm_map = 0;
                    if( !(i_sm->_map_forward_direct_power.isEmpty()) )
                    {
                        P_value_on_sm_map = *(i_sm->_map_forward_direct_power.iPt( row_i_number, col_i_number ));
                    }
					
					short front_pair_i_number = i_lm->_points[i_number].front_pair_number;
					double I_rear_i_number = i_lm->_points[i_number].I_rear;
					double I_front_1_i_number = i_lm->_points[i_number].I_front_1;
					double I_front_2_i_number = i_lm->_points[i_number].I_front_2;
					double I_front_3_i_number = i_lm->_points[i_number].I_front_3;
					double I_front_4_i_number = i_lm->_points[i_number].I_front_4;
					double I_front_5_i_number = i_lm->_points[i_number].I_front_5;
					double I_front_6_i_number = i_lm->_points[i_number].I_front_6;
					double I_front_7_i_number = i_lm->_points[i_number].I_front_7;
					double I_front_8_i_number = i_lm->_points[i_number].I_front_8;
					double I_phase_i_number = i_lm->_points[i_number].I_phase;

					//Add details of sample point to object in vector

					_estimation_itu_ops.rbegin()->_ITU_channel_number = itu_channel_num;
					_estimation_itu_ops.rbegin()->_ITU_frequency = _itugrid._ITU_frequencies[itu_channel_num];

					_estimation_itu_ops.rbegin()->_sm_number = sm_number;
					_estimation_itu_ops.rbegin()->_lm_number = lm_number;
					_estimation_itu_ops.rbegin()->_index_on_middle_line_of_frequency = i_number;
					_estimation_itu_ops.rbegin()->_real_index_on_middle_line_of_frequency = (double)i_number;

					_estimation_itu_ops.rbegin()->_row_on_sm_map = row_i_number;
					_estimation_itu_ops.rbegin()->_col_on_sm_map = col_i_number;

					_estimation_itu_ops.rbegin()->_front_pair_number = front_pair_i_number;
					_estimation_itu_ops.rbegin()->_I_gain = i_sm->_I_gain;
					_estimation_itu_ops.rbegin()->_I_soa = i_sm->_I_soa;
					_estimation_itu_ops.rbegin()->_I_rear = I_rear_i_number;
					_estimation_itu_ops.rbegin()->_I_front_1 = I_front_1_i_number;
					_estimation_itu_ops.rbegin()->_I_front_2 = I_front_2_i_number;
					_estimation_itu_ops.rbegin()->_I_front_3 = I_front_3_i_number;
					_estimation_itu_ops.rbegin()->_I_front_4 = I_front_4_i_number;
					_estimation_itu_ops.rbegin()->_I_front_5 = I_front_5_i_number;
					_estimation_itu_ops.rbegin()->_I_front_6 = I_front_6_i_number;
					_estimation_itu_ops.rbegin()->_I_front_7 = I_front_7_i_number;
					_estimation_itu_ops.rbegin()->_I_front_8 = I_front_8_i_number;
					_estimation_itu_ops.rbegin()->_I_phase = I_phase_i_number;


					_estimation_itu_ops.rbegin()->_Pr_on_sm_map = Pr_value_on_sm_map;
					_estimation_itu_ops.rbegin()->_P_ratio_measured = Pr_value_on_sm_map;
					_estimation_itu_ops.rbegin()->_P_measured = P_value_on_sm_map;

					_estimation_itu_ops.rbegin()->_F_measured = 0;
					_estimation_itu_ops.rbegin()->_F_coarse = coarse_freq_value_on_sm_map;
					_estimation_itu_ops.rbegin()->_stable_F_measurement = false;

				}

			}

		}
	}


	if( rval == rtype::ok  )
	{
		rval = findNearbyBoundaryCoords( _estimation_itu_ops );
	}

	if( rval == rtype::ok  )
	{
		CString lines_abs_filepath = _results_dir_CString;
		if(lines_abs_filepath.Right(1) != _T("\\") )
			lines_abs_filepath += _T("\\");
		lines_abs_filepath += ("ITUOperatingPointEstimates_");
		lines_abs_filepath += LASER_TYPE_ID_DSDBR01_CSTRING;
		lines_abs_filepath += USCORE;
		lines_abs_filepath += _laser_id_CString;
		lines_abs_filepath += USCORE;
		lines_abs_filepath += _date_time_stamp_CString;
		lines_abs_filepath += CSV;

		rval = writeOPsToFile(
			lines_abs_filepath,
			_estimation_itu_ops,
			true );
	}

	if( rval == rtype::ok 
	 && (long)(countITUOPEstimatesFound()) < _itugrid.channel_count() )
	{
		// not all channels have estimates
		// this is only a warning really

		CString log_err_msg("CCGOperatingPoints::estimateITUOperatingPoints warning: ");
		log_err_msg += CString("estimates not found for all ITU channels");
		CGR_LOG(log_err_msg,WARNING_CGR_LOG)
	}

	return rval;
}


short
CCGOperatingPoints::countITUOPsFound()
{
	short itu_op_count = 0;

	for( int i = 0 ; i < (int)(_itugrid.channel_count()) ; i++ )
	{
		for( int j = 0 ; j < (int)(_characterisation_itu_ops.size()) ; j++ )
		{
			if( _characterisation_itu_ops[j]._ITU_channel_number == (short)i
			 && _characterisation_itu_ops[j]._is_an_ITU_point )
			{
				// Found estimate for ITU point i
				itu_op_count++;
				break;
			}
		}
	}

	return itu_op_count;
}

short
CCGOperatingPoints::countITUOPEstimatesFound()
{
	short itu_estimate_count = 0;

	for( int i = 0 ; i < (int)(_itugrid.channel_count()) ; i++ )
	{
		for( int j = 0 ; j < (int)(_estimation_itu_ops.size()) ; j++ )
		{
			if( _estimation_itu_ops[j]._ITU_channel_number == (short)i )
			{
				// Found estimate for ITU point i
				itu_estimate_count++;
				break;
			}
		}
	}

	return itu_estimate_count;
}

CCGOperatingPoints::rtype
CCGOperatingPoints::findNearbyBoundaryCoords(
	std::vector<CDSDBROperatingPoint> &operating_points )
{
	rtype rval = rtype::ok;

	for( long i = 0 ; i < (long)(operating_points.size()) ; i++ )
	{
		short sm_number = operating_points[i]._sm_number;
		short lm_number = operating_points[i]._lm_number;
		short i_number = operating_points[i]._index_on_middle_line_of_frequency;
		long row = operating_points[i]._row_on_sm_map;
		long col = operating_points[i]._col_on_sm_map;

		std::vector<CDSDBRSuperMode>::iterator i_sm = _p_supermodes->begin();
		i_sm += sm_number;

		// remember! _lm_upper_lines refers to upper side of hysteresis (lower line of a mode!)
		// and _lm_lower_lines refers to lower side of hysteresis (upper line of a mode!)
		std::vector<CDSDBRSupermodeMapLine::point_type> *p_lower_points = &(i_sm->_lm_upper_lines[lm_number]._points);
		std::vector<CDSDBRSupermodeMapLine::point_type> *p_upper_points = &(i_sm->_lm_lower_lines[lm_number+1]._points);

		operating_points[i]._row_of_boundary_above = i_sm->_rows-1;
		operating_points[i]._col_of_boundary_above = col;
		operating_points[i]._row_of_boundary_below = 0;
		operating_points[i]._col_of_boundary_below = col;
		operating_points[i]._row_of_boundary_to_left = row;
		operating_points[i]._col_of_boundary_to_left = 0;
		operating_points[i]._row_of_boundary_to_right = row;
		operating_points[i]._col_of_boundary_to_right = i_sm->_cols-1;;

		// first set boundary point on edge of map
		// then if points are found on boundaries, replace them
		
		for( long j = 0 ; j < (long)(p_lower_points->size()) ; j++ )
		{
			long lower_pt_row = ((*p_lower_points)[j]).row;
			long lower_pt_col = ((*p_lower_points)[j]).col;

			if( lower_pt_col == col )
			{
				// found point below
				if( lower_pt_row > operating_points[i]._row_of_boundary_below )
				{
					operating_points[i]._row_of_boundary_below = lower_pt_row;
					operating_points[i]._col_of_boundary_below = lower_pt_col;
				}
			}

			if( lower_pt_row == row )
			{
				if( lower_pt_col < operating_points[i]._col_of_boundary_to_right )
				{
					// found point to right
					operating_points[i]._row_of_boundary_to_right = lower_pt_row;
					operating_points[i]._col_of_boundary_to_right = lower_pt_col;
				}
			}
		}

		for( long j = 0 ; j < (long)(p_upper_points->size()) ; j++ )
		{
			long upper_pt_row = ((*p_upper_points)[j]).row;
			long upper_pt_col = ((*p_upper_points)[j]).col;

			if( upper_pt_col == col )
			{
				if( upper_pt_row < operating_points[i]._row_of_boundary_above )
				{
					// found point above
					operating_points[i]._row_of_boundary_above = upper_pt_row;
					operating_points[i]._col_of_boundary_above = upper_pt_col;
				}
			}

			if( upper_pt_row == row )
			{
				if( upper_pt_col > operating_points[i]._col_of_boundary_to_left )
				{
					// found point to left
					operating_points[i]._row_of_boundary_to_left = upper_pt_row;
					operating_points[i]._col_of_boundary_to_left = upper_pt_col;
				}
			}
		}


	}

	return rval;
}


CCGOperatingPoints::rtype
CCGOperatingPoints::applyStableOP(
	short sm,
	short lm,
	double index,
	short apply_Gain_SOA_of_SM_map )
{
	rtype rval = rtype::ok;


	CGR_LOG("CCGOperatingPoints::applyStableOP() entered",DIAGNOSTIC_CGR_LOG)


	// Check sm exists
	if( sm >= 0 && sm < (short)(_p_supermodes->size()) )
	{
		std::vector<CDSDBRSuperMode>::iterator i_sm = _p_supermodes->begin();
		i_sm += sm;

		// Check lm exists

		if( lm >= 0 && lm < (short)(i_sm->_lm_middle_lines_of_frequency.size()) )
		{
			std::vector<CDSDBRSupermodeMapLine>::iterator i_lm
				= i_sm->_lm_middle_lines_of_frequency.begin();
			i_lm += lm;

			// check index

			if( (short)index >= 0 && index <= (double)(i_lm->_points.size())-1 )
			{
				// Create stable operating point object

				CDSDBROperatingPoint stable_op( i_sm, i_lm, sm, lm, index );

				if( apply_Gain_SOA_of_SM_map != TRUE )
				{
					// setting _I_gain and _I_soa to zero means
					// that those currents won't be touched
					stable_op._I_gain = 0;
					stable_op._I_soa = 0;
				}

				CDSDBROperatingPoint::rtype rval_apply = stable_op.apply();

				if( rval_apply != CDSDBROperatingPoint::rtype::ok )
				{
					rval = rtype::error_applying_op;

					CGR_LOG("CCGOperatingPoints::applyStableOP() error applying op",ERROR_CGR_LOG)
				}

			}
			else
			{
				// index does not exist
				CGR_LOG("CCGOperatingPoints::applyStableOP() index does not exist",ERROR_CGR_LOG)
				rval = rtype::index_not_found;
			}
		}
		else
		{
			// lm does not exist
			CGR_LOG("CCGOperatingPoints::applyStableOP() lm does not exist",ERROR_CGR_LOG)
			rval = rtype::lm_not_found;
		}
	}
	else
	{
		// sm does not exist
		CGR_LOG("CCGOperatingPoints::applyStableOP() sm does not exist",ERROR_CGR_LOG)
		rval = rtype::sm_not_found;
	}

	return rval;
}




CCGOperatingPoints::rtype
CCGOperatingPoints::applyPointOnSMMap(
	short sm,
	double Im_index,
	double Ip_index,
	short apply_Gain_SOA_of_SM_map )
{
	rtype rval = rtype::ok;


	CGR_LOG("CCGOperatingPoints::applyPointOnSMMap() entered",DIAGNOSTIC_CGR_LOG)


	// Check sm exists
	if( sm >= 0 && sm < (short)(_p_supermodes->size()) )
	{
		std::vector<CDSDBRSuperMode>::iterator i_sm = _p_supermodes->begin();
		i_sm += sm;

		if( (short)Im_index >= 0 && Im_index <= (double)(i_sm->_filtered_middle_line._points.size())-1 )
		{

			if( (short)Ip_index >= 0 && Ip_index <= (double)(i_sm->_phase_current._currents.size())-1 )
			{
				// Create operating point object

				CDSDBROperatingPoint point_on_sm_map( i_sm, sm, Im_index, Ip_index );

				if( apply_Gain_SOA_of_SM_map != TRUE )
				{
					// setting _I_gain and _I_soa to zero means
					// that those currents won't be touched
					point_on_sm_map._I_gain = 0;
					point_on_sm_map._I_soa = 0;
				}

				CDSDBROperatingPoint::rtype rval_apply = point_on_sm_map.apply();

				if( rval_apply != CDSDBROperatingPoint::rtype::ok )
				{
					CGR_LOG("CCGOperatingPoints::applyPointOnSMMap() error applying op",ERROR_CGR_LOG)
					rval = rtype::error_applying_op;
				}

			}
			else
			{
				// point on phase vector does not exist
				CGR_LOG("CCGOperatingPoints::applyPointOnSMMap() point on phase vector does not exist",ERROR_CGR_LOG)
				rval = rtype::index_not_found;
			}

		}
		else
		{
			// point on middle line does not exist
			CGR_LOG("CCGOperatingPoints::applyPointOnSMMap() point on middle line does not exist",ERROR_CGR_LOG)
			rval = rtype::index_not_found;
		}
	}
	else
	{
		// sm does not exist
		CGR_LOG("CCGOperatingPoints::applyPointOnSMMap() sm does not exist",ERROR_CGR_LOG)
		rval = rtype::sm_not_found;
	}

	return rval;
}


CCGOperatingPoints::rtype
CCGOperatingPoints::getPointOnSMMap(
	short sm,
	double* p_Im_index,
	double* p_Ip_index )
{
	rtype rval = rtype::ok;


	CGR_LOG("CCGOperatingPoints::getPointOnSMMap() entered",DIAGNOSTIC_CGR_LOG)


	// Check sm exists
	if( sm >= 0 && sm < (short)(_p_supermodes->size()) )
	{
		std::vector<CDSDBRSuperMode>::iterator i_sm = _p_supermodes->begin();
		i_sm += sm;

		// check if two adjacent front sections and rear are connected

		short front1_output_connection_state;
		short front2_output_connection_state;
		short front3_output_connection_state;
		short front4_output_connection_state;
		short front5_output_connection_state;
		short front6_output_connection_state;
		short front7_output_connection_state;
		short front8_output_connection_state;
		short rear_output_connection_state;
		short phase_output_connection_state;

		HRESULT hr = S_OK;

		if( hr == S_OK )
			hr = SERVERINTERFACE->getOutputConnection( CG_REG->get_DSDBR01_CMDC_front1_module_name(), &front1_output_connection_state );
		if( hr == S_OK )
			hr = SERVERINTERFACE->getOutputConnection( CG_REG->get_DSDBR01_CMDC_front2_module_name(), &front2_output_connection_state );
		if( hr == S_OK )
			hr = SERVERINTERFACE->getOutputConnection( CG_REG->get_DSDBR01_CMDC_front3_module_name(), &front3_output_connection_state );
		if( hr == S_OK )
			hr = SERVERINTERFACE->getOutputConnection( CG_REG->get_DSDBR01_CMDC_front4_module_name(), &front4_output_connection_state );
		if( hr == S_OK )
			hr = SERVERINTERFACE->getOutputConnection( CG_REG->get_DSDBR01_CMDC_front5_module_name(), &front5_output_connection_state );
		if( hr == S_OK )
			hr = SERVERINTERFACE->getOutputConnection( CG_REG->get_DSDBR01_CMDC_front6_module_name(), &front6_output_connection_state );
		if( hr == S_OK )
			hr = SERVERINTERFACE->getOutputConnection( CG_REG->get_DSDBR01_CMDC_front7_module_name(), &front7_output_connection_state );
		if( hr == S_OK )
			hr = SERVERINTERFACE->getOutputConnection( CG_REG->get_DSDBR01_CMDC_front8_module_name(), &front8_output_connection_state );
		if( hr == S_OK )
			hr = SERVERINTERFACE->getOutputConnection( CG_REG->get_DSDBR01_CMDC_rear_module_name(), &rear_output_connection_state );
		if( hr == S_OK )
			hr = SERVERINTERFACE->getOutputConnection( CG_REG->get_DSDBR01_CMDC_phase_module_name(), &phase_output_connection_state );

		if( hr != S_OK )
		{ // error assessing connection state
			rval = rtype::error_getting_connection_state;
			CGR_LOG("CCGOperatingPoints::getPointOnSMMap() error assessing connection state",ERROR_CGR_LOG)
		}
		else
		{

			if( rear_output_connection_state == CONNECT_OUTPUT
			&& phase_output_connection_state == CONNECT_OUTPUT
			&&(( front1_output_connection_state == CONNECT_OUTPUT && front2_output_connection_state == CONNECT_OUTPUT )
			|| ( front2_output_connection_state == CONNECT_OUTPUT && front3_output_connection_state == CONNECT_OUTPUT )
			|| ( front3_output_connection_state == CONNECT_OUTPUT && front4_output_connection_state == CONNECT_OUTPUT )
			|| ( front4_output_connection_state == CONNECT_OUTPUT && front5_output_connection_state == CONNECT_OUTPUT )
			|| ( front5_output_connection_state == CONNECT_OUTPUT && front6_output_connection_state == CONNECT_OUTPUT )
			|| ( front6_output_connection_state == CONNECT_OUTPUT && front7_output_connection_state == CONNECT_OUTPUT )
			|| ( front7_output_connection_state == CONNECT_OUTPUT && front8_output_connection_state == CONNECT_OUTPUT ) ) )
			{

				double front1_current = 0;
				double front2_current = 0;
				double front3_current = 0;
				double front4_current = 0;
				double front5_current = 0;
				double front6_current = 0;
				double front7_current = 0;
				double front8_current = 0;
				double rear_current = 0;
				double phase_current = 0;

				if( hr == S_OK && front1_output_connection_state == CONNECT_OUTPUT )
					hr = SERVERINTERFACE->getCurrent( CG_REG->get_DSDBR01_CMDC_front1_module_name(), &front1_current );
				if( hr == S_OK && front2_output_connection_state == CONNECT_OUTPUT )
					hr = SERVERINTERFACE->getCurrent( CG_REG->get_DSDBR01_CMDC_front2_module_name(), &front2_current );
				if( hr == S_OK && front3_output_connection_state == CONNECT_OUTPUT )
					hr = SERVERINTERFACE->getCurrent( CG_REG->get_DSDBR01_CMDC_front3_module_name(), &front3_current );
				if( hr == S_OK && front4_output_connection_state == CONNECT_OUTPUT )
					hr = SERVERINTERFACE->getCurrent( CG_REG->get_DSDBR01_CMDC_front4_module_name(), &front4_current );
				if( hr == S_OK && front5_output_connection_state == CONNECT_OUTPUT )
					hr = SERVERINTERFACE->getCurrent( CG_REG->get_DSDBR01_CMDC_front5_module_name(), &front5_current );
				if( hr == S_OK && front6_output_connection_state == CONNECT_OUTPUT )
					hr = SERVERINTERFACE->getCurrent( CG_REG->get_DSDBR01_CMDC_front6_module_name(), &front6_current );
				if( hr == S_OK && front7_output_connection_state == CONNECT_OUTPUT )
					hr = SERVERINTERFACE->getCurrent( CG_REG->get_DSDBR01_CMDC_front7_module_name(), &front7_current );
				if( hr == S_OK && front8_output_connection_state == CONNECT_OUTPUT )
					hr = SERVERINTERFACE->getCurrent( CG_REG->get_DSDBR01_CMDC_front8_module_name(), &front8_current );
				if( hr == S_OK && rear_output_connection_state == CONNECT_OUTPUT )
					hr = SERVERINTERFACE->getCurrent( CG_REG->get_DSDBR01_CMDC_rear_module_name(), &rear_current );
				if( hr == S_OK && phase_output_connection_state == CONNECT_OUTPUT )
					hr = SERVERINTERFACE->getCurrent( CG_REG->get_DSDBR01_CMDC_phase_module_name(), &phase_current );


				if( hr != S_OK )
				{ // error assessing current at output
					CGR_LOG("CCGOperatingPoints::getPointOnSMMap() error assessing current at output",ERROR_CGR_LOG)
					rval = rtype::error_getting_current_at_output;
				}
				else
				{
					short pair_number = 0;
					double constant_front_current = 0;
					double nonconstant_front_current = 0;
					if( front1_current > 0 )
					{
						pair_number = 1;
						constant_front_current = front1_current;
						nonconstant_front_current = front2_current;
					}
					else
					if( front2_current > 0 )
					{
						pair_number = 2;
						constant_front_current = front2_current;
						nonconstant_front_current = front3_current;
					}
					else
					if( front3_current > 0 )
					{
						pair_number = 3;
						constant_front_current = front3_current;
						nonconstant_front_current = front4_current;
					}
					else
					if( front4_current > 0 )
					{
						pair_number = 4;
						constant_front_current = front4_current;
						nonconstant_front_current = front5_current;
					}
					else
					if( front5_current > 0 )
					{
						pair_number = 5;
						constant_front_current = front5_current;
						nonconstant_front_current = front6_current;
					}
					else
					if( front6_current > 0 )
					{
						pair_number = 6;
						constant_front_current = front6_current;
						nonconstant_front_current = front7_current;
					}
					else
					if( front7_current > 0 )
					{
						pair_number = 7;
						constant_front_current = front7_current;
						nonconstant_front_current = front8_current;
					}

					if( pair_number == 0 )
					{
						// no front pair is being used
						CGR_LOG("CCGOperatingPoints::getPointOnSMMap() no front pair is being used",ERROR_CGR_LOG)
						rval = rtype::required_outputs_not_connected;
					}
					else
					{
						// Find phase current in phase vector

						short phase_vector_size = (short)(i_sm->_phase_current._currents.size());

						if( phase_current >= i_sm->_phase_current._currents[0]
						 && phase_current <= i_sm->_phase_current._currents[phase_vector_size-1] )
						{
							for( short i = 0 ; i < phase_vector_size-1 ; i++ )
							{
								double Ip_at_i = i_sm->_phase_current._currents[i];
								double Ip_at_iplus1 = i_sm->_phase_current._currents[i+1];

								if( phase_current >= Ip_at_i && phase_current <= Ip_at_iplus1 )
								{
									*p_Ip_index = (double)i + (phase_current-Ip_at_i) / (Ip_at_iplus1-Ip_at_i);
									break;
								}
							}


							// Find rear current in middle line
							short middle_line_size = (short)(i_sm->_filtered_middle_line._points.size());

							if( rear_current >= i_sm->_filtered_middle_line._points[0].I_rear
							&& rear_current <= i_sm->_filtered_middle_line._points[middle_line_size-1].I_rear )
							{
								for( short j = 0 ; j < middle_line_size-1 ; j++ )
								{
									double Ir_at_j = i_sm->_filtered_middle_line._points[j].I_rear;
									double Ir_at_jplus1 = i_sm->_filtered_middle_line._points[j+1].I_rear;

									if( rear_current >= Ir_at_j && rear_current <= Ir_at_jplus1 )
									{
										*p_Im_index = (double)j + (rear_current-Ir_at_j) / (Ir_at_jplus1-Ir_at_j);

										short pair_num_at_j = i_sm->_filtered_middle_line.pair_number(j);
										short pair_num_at_jplus1 = i_sm->_filtered_middle_line.pair_number(j+1);
										long index_in_submap_at_j = i_sm->_filtered_middle_line.index_in_submap(j);
										long index_in_submap_at_jplus1 = i_sm->_filtered_middle_line.index_in_submap(j+1);

										double const_If_at_j = i_sm->_filtered_middle_line.getConstantIf( pair_num_at_j, index_in_submap_at_j );
										double const_If_at_jplus1 = i_sm->_filtered_middle_line.getConstantIf( pair_num_at_jplus1, index_in_submap_at_jplus1 );

										double nonconst_If_at_j = i_sm->_filtered_middle_line.getNonConstantIf( pair_num_at_j, index_in_submap_at_j );
										double nonconst_If_at_jplus1 = i_sm->_filtered_middle_line.getNonConstantIf( pair_num_at_jplus1, index_in_submap_at_jplus1 );

										if( (pair_number != pair_num_at_j && pair_number != pair_num_at_jplus1)
										 || (constant_front_current != const_If_at_j && constant_front_current != const_If_at_jplus1)
										 || (nonconstant_front_current < nonconst_If_at_j && nonconstant_front_current > nonconst_If_at_jplus1 )  )
										{ // front current being applied is outside middle line range
											CGR_LOG("CCGOperatingPoints::getPointOnSMMap() front current being applied is outside middle line range",ERROR_CGR_LOG)
											rval = rtype::index_not_found;
										}

										break;
									}
								}
							}
							else
							{ // rear current being applied is outside middle line range 
								CGR_LOG("CCGOperatingPoints::getPointOnSMMap() rear current being applied is outside middle line range",ERROR_CGR_LOG)
								rval = rtype::index_not_found;
							}

						}
						else
						{ // phase current being applied is outside phase vector range 
							CGR_LOG("CCGOperatingPoints::getPointOnSMMap() phase current being applied is outside phase vector range",ERROR_CGR_LOG)
							rval = rtype::index_not_found;
						}

					}
				}
			}
			else
			{ // required outputs not connected
				CGR_LOG("CCGOperatingPoints::getPointOnSMMap() required outputs not connected",ERROR_CGR_LOG)
				rval = rtype::required_outputs_not_connected;
			}
		}


	}
	else
	{
		// sm does not exist
		CGR_LOG("CCGOperatingPoints::getPointOnSMMap() sm does not exist",ERROR_CGR_LOG)
		rval = rtype::sm_not_found;
	}

	return rval;
}


CCGOperatingPoints::rtype
CCGOperatingPoints::loadDefaultCoarseFreqPoly()
{
	rtype rval = rtype::ok;

    long row_count = 0;
    long first_line_col_count = 0;
    long col_count = 0;

	std::vector<double> coeff_values;

	CFileStatus status;

    // Check file

	CString abs_filepath = CG_REG->get_DSDBR01_CFREQ_poly_coeffs_abspath();

	if( CFile::GetStatus( abs_filepath, status ) )
	{
		// File exists => attempt to read it's contents
		try
		{
			std::ifstream file_stream( abs_filepath );

			if( file_stream )
			{
				row_count = 0;

				while( !file_stream.eof() )
				{
					// File stream is open, read each line
					char temp_buffer[MAX_LINE_LENGTH];
					*temp_buffer = '\0';
					file_stream.getline( temp_buffer, MAX_LINE_LENGTH );

					int temp_buffer_lenght = (int)strlen(temp_buffer);
					if( temp_buffer_lenght > 0 ) // there is something on the line
					{
        				col_count = 0;
						char *fp = strtok(temp_buffer,DELIMITERS);
						while(fp)
						{
							double coeff_value = atof(fp);

							coeff_values.push_back(coeff_value);
							
							fp = strtok(NULL,DELIMITERS);
							col_count++;
						}

						if( col_count > 1 )
						{
							rval = rtype::file_format_invalid;
							break;
						}

						if(row_count == 0) first_line_col_count = col_count;

						row_count++;
					}

					if( row_count > 0 && col_count != first_line_col_count )
					{
						rval = rtype::file_format_invalid;
						break;
					}
				}
			}
			else
			{
        		rval = rtype::could_not_open_file;
			}
		}
		catch(...)
		{
			rval = rtype::could_not_open_file;
		}

	}
	else
	{
		rval = rtype::file_does_not_exist;
	}

    if( rval == rtype::ok
	 && ( row_count < MIN_COARSEFREQPOLYCOEFFS || row_count > MAX_COARSEFREQPOLYCOEFFS ) )
	{
		rval = rtype::file_format_invalid;
	}

	if( rval == rtype::ok )
	{
		_coarse_freq_poly.clear();

		for(int i = 0; i < (int)(coeff_values.size()) ; i++ )
			_coarse_freq_poly.push_back(coeff_values[i]);
	}

	return rval;
}

CCGOperatingPoints::rtype
CCGOperatingPoints::writeOPsToFile(
	CString abs_filepath,
	std::vector<CDSDBROperatingPoint> &operating_points,
	bool overwrite )
{
	rtype rval = rtype::ok;

    long row_count = 0;
    long col_count = 0;

	if( operating_points.empty() )
	{
		rval = rtype::no_data;
	}
	else
	{

		CFileStatus status;

		// Check if file already exists and overwrite not specified

		if( CFile::GetStatus( abs_filepath, status ) && overwrite == false )
		{
			rval = rtype::file_already_exists;
		}
		else
		{

        // Ensure the directory exists
		int index_of_last_dirslash = abs_filepath.ReverseFind('\\');
		if( index_of_last_dirslash == -1 )
		{
			// '\\' not found => not an absolute path
			rval = rtype::could_not_open_file;
		}
		else
		{
			CString dir_name = abs_filepath;
			dir_name.Truncate(index_of_last_dirslash);

			int i = 0;
			CString token = dir_name.Tokenize( "\\/", i );
			CString new_dir = "";
			while ( token != "" )
			{
				if( new_dir.GetLength() == 0 )
				{
					new_dir = token;
				}
				else
				{
					new_dir = new_dir + "\\" + token;
				}
				CFileStatus status;
				//printf("\n %s  = %d", new_dir, CFile::GetStatus( new_dir, status ) );
				if( new_dir.GetLength() > 2 // skip drive name
				&& !CFile::GetStatus( new_dir, status ) )
				{ // directory does not exists => make it
					_mkdir( new_dir );
				}
				else
				{ // directory exists
				}
				//printf("\n %s  = %d", new_dir, CFile::GetStatus( new_dir, status ) );
	            
				token = dir_name.Tokenize( "\\/", i );
			}

				try
				{
					std::ofstream file_stream( abs_filepath );

					if( file_stream )
					{
						// First write header row
						file_stream << "ITU Channel Number" << COMMA_CHAR;
						file_stream << "ITU Channel Frequency" << COMMA_CHAR;
						file_stream << "Supermode Number" << COMMA_CHAR;
						file_stream << "Longitudinal Mode Number" << COMMA_CHAR;
						file_stream << "Index on Middle Line Of Frequency" << COMMA_CHAR;
						file_stream << "Phase [mA]" << COMMA_CHAR;
						file_stream << "Rear [mA]" << COMMA_CHAR;
						file_stream << "Gain [mA]" << COMMA_CHAR;
						file_stream << "SOA [mA]" << COMMA_CHAR;
						file_stream << "Front Pair Number" << COMMA_CHAR;
						file_stream << "Constant Front [mA]" << COMMA_CHAR;
						file_stream << "Non-constant Front [mA]" << COMMA_CHAR;
						file_stream << "Frequency [GHz]" << COMMA_CHAR;
						file_stream << "Coarse Frequency [GHz]" << COMMA_CHAR;
						file_stream << "Optical Power Ratio [0:1]" << COMMA_CHAR;
						file_stream << "Optical Power [mW]" << COMMA_CHAR;
						file_stream << "X coordinate of boundary above" << COMMA_CHAR;
						file_stream << "Y coordinate of boundary above" << COMMA_CHAR;
						file_stream << "X coordinate of boundary below" << COMMA_CHAR;
						file_stream << "Y coordinate of boundary below" << COMMA_CHAR;
						file_stream << "X coordinate of boundary to left" << COMMA_CHAR;
						file_stream << "Y coordinate of boundary to left" << COMMA_CHAR;
						file_stream << "X coordinate of boundary to right" << COMMA_CHAR;
						file_stream << "Y coordinate of boundary to right" << COMMA_CHAR;
						file_stream << std::endl;

						for( long i = 0 ; i < (long)(operating_points.size()) ; i++ )
						{
							// First write data rows
							char val_buf[20];
							sprintf( val_buf, "%d", operating_points[i]._ITU_channel_number );
							file_stream << val_buf << COMMA_CHAR;
							sprintf( val_buf, "%.3f", operating_points[i]._ITU_frequency );
							file_stream << val_buf << COMMA_CHAR;
							sprintf( val_buf, "%d", operating_points[i]._sm_number );
							file_stream << val_buf << COMMA_CHAR;
							sprintf( val_buf, "%d", operating_points[i]._lm_number );
							file_stream << val_buf << COMMA_CHAR;
							sprintf( val_buf, "%.6g", operating_points[i]._real_index_on_middle_line_of_frequency );
							file_stream << val_buf << COMMA_CHAR;
							sprintf( val_buf, "%.6g", operating_points[i]._I_phase );
							file_stream << val_buf << COMMA_CHAR;
							sprintf( val_buf, "%.6g", operating_points[i]._I_rear );
							file_stream << val_buf << COMMA_CHAR;
							sprintf( val_buf, "%.6g", operating_points[i]._I_gain );
							file_stream << val_buf << COMMA_CHAR;
							sprintf( val_buf, "%.6g", operating_points[i]._I_soa );
							file_stream << val_buf << COMMA_CHAR;
							sprintf( val_buf, "%d", operating_points[i]._front_pair_number );
							file_stream << val_buf << COMMA_CHAR;

							double constant_If = 0;
							double nonconstant_If = 0;

							switch( operating_points[i]._front_pair_number )
							{
							case 1:
								constant_If = operating_points[i]._I_front_1;
								nonconstant_If = operating_points[i]._I_front_2;
								break;
							case 2:
								constant_If = operating_points[i]._I_front_2;
								nonconstant_If = operating_points[i]._I_front_3;
								break;
							case 3:
								constant_If = operating_points[i]._I_front_3;
								nonconstant_If = operating_points[i]._I_front_4;
								break;
							case 4:
								constant_If = operating_points[i]._I_front_4;
								nonconstant_If = operating_points[i]._I_front_5;
								break;
							case 5:
								constant_If = operating_points[i]._I_front_5;
								nonconstant_If = operating_points[i]._I_front_6;
								break;
							case 6:
								constant_If = operating_points[i]._I_front_6;
								nonconstant_If = operating_points[i]._I_front_7;
								break;
							case 7:
								constant_If = operating_points[i]._I_front_7;
								nonconstant_If = operating_points[i]._I_front_8;
								break;
							}

							sprintf( val_buf, "%.6g", constant_If );
							file_stream << val_buf << COMMA_CHAR;
							sprintf( val_buf, "%.6g", nonconstant_If );
							file_stream << val_buf << COMMA_CHAR;

							sprintf( val_buf, "%.3f", operating_points[i]._F_measured );
							file_stream << val_buf << COMMA_CHAR;
							sprintf( val_buf, "%.3f", operating_points[i]._F_coarse );
							file_stream << val_buf << COMMA_CHAR;
							sprintf( val_buf, "%.6g", operating_points[i]._P_ratio_measured );
							file_stream << val_buf << COMMA_CHAR;
							sprintf( val_buf, "%.6g", operating_points[i]._P_measured );
							file_stream << val_buf << COMMA_CHAR;

							sprintf( val_buf, "%d", operating_points[i]._col_of_boundary_above ); // X coordinate of boundary above
							file_stream << val_buf << COMMA_CHAR;
							sprintf( val_buf, "%d", operating_points[i]._row_of_boundary_above ); // Y coordinate of boundary above
							file_stream << val_buf << COMMA_CHAR;
							sprintf( val_buf, "%d", operating_points[i]._col_of_boundary_below ); // X coordinate of boundary below
							file_stream << val_buf << COMMA_CHAR;
							sprintf( val_buf, "%d", operating_points[i]._row_of_boundary_below ); // Y coordinate of boundary below
							file_stream << val_buf << COMMA_CHAR;
							sprintf( val_buf, "%d", operating_points[i]._col_of_boundary_to_left ); // X coordinate of boundary to left
							file_stream << val_buf << COMMA_CHAR;
							sprintf( val_buf, "%d", operating_points[i]._row_of_boundary_to_left ); // Y coordinate of boundary to left
							file_stream << val_buf << COMMA_CHAR;
							sprintf( val_buf, "%d", operating_points[i]._col_of_boundary_to_right ); // X coordinate of boundary to right
							file_stream << val_buf << COMMA_CHAR;
							sprintf( val_buf, "%d", operating_points[i]._row_of_boundary_to_right ); // Y coordinate of boundary to right
							file_stream << val_buf << COMMA_CHAR;

							file_stream << std::endl;
						}
					}
					else
					{
						rval = rtype::could_not_open_file;
					}
				}
				catch(...)
				{
					rval = rtype::could_not_open_file;
				}
			}
		}

	}

	return rval;
}

CCGOperatingPoints::rtype
CCGOperatingPoints::writePolyCoeffsToFile(
	std::vector<double> &coarse_freq_poly,
	CString abs_filepath,
	bool overwrite )
{
	rtype rval = rtype::ok;

    long row_count = 0;
    long col_count = 0;

	if( coarse_freq_poly.empty() )
	{
		rval = rtype::no_data;
	}
	else
	{

		CFileStatus status;

		// Check if file already exists and overwrite not specified

		if( CFile::GetStatus( abs_filepath, status ) && overwrite == false )
		{
			rval = rtype::file_already_exists;
		}
		else
		{

        // Ensure the directory exists
		int index_of_last_dirslash = abs_filepath.ReverseFind('\\');
		if( index_of_last_dirslash == -1 )
		{
			// '\\' not found => not an absolute path
			rval = rtype::could_not_open_file;
		}
		else
		{
			CString dir_name = abs_filepath;
			dir_name.Truncate(index_of_last_dirslash);

			int i = 0;
			CString token = dir_name.Tokenize( "\\/", i );
			CString new_dir = "";
			while ( token != "" )
			{
				if( new_dir.GetLength() == 0 )
				{
					new_dir = token;
				}
				else
				{
					new_dir = new_dir + "\\" + token;
				}
				CFileStatus status;
				//printf("\n %s  = %d", new_dir, CFile::GetStatus( new_dir, status ) );
				if( new_dir.GetLength() > 2 // skip drive name
				&& !CFile::GetStatus( new_dir, status ) )
				{ // directory does not exists => make it
					_mkdir( new_dir );
				}
				else
				{ // directory exists
				}
				//printf("\n %s  = %d", new_dir, CFile::GetStatus( new_dir, status ) );
	            
				token = dir_name.Tokenize( "\\/", i );
			}

				try
				{
					std::ofstream file_stream( abs_filepath );

					if( file_stream )
					{
						for( long i = 0 ; i < (long)(coarse_freq_poly.size()) ; i++ )
						{
							// First each coefficient on its own row
							char val_buf[20];
							sprintf( val_buf, "%f", coarse_freq_poly[i] );
							file_stream << val_buf << std::endl;
						}
					}
					else
					{
						rval = rtype::could_not_open_file;
					}
				}
				catch(...)
				{
					rval = rtype::could_not_open_file;
				}
			}
		}

	}

	return rval;
}



CCGOperatingPoints::rtype
CCGOperatingPoints::writeSamplePointsToFile(
	CString abs_filepath,
	bool overwrite )
{
	rtype rval = rtype::ok;

    long row_count = 0;
    long col_count = 0;

	if( _coarse_freq_sample_points.empty() )
	{
		rval = rtype::no_data;
	}
	else
	{

		CFileStatus status;

		// Check if file already exists and overwrite not specified

		if( CFile::GetStatus( abs_filepath, status ) && overwrite == false )
		{
			rval = rtype::file_already_exists;
		}
		else
		{

        // Ensure the directory exists
		int index_of_last_dirslash = abs_filepath.ReverseFind('\\');
		if( index_of_last_dirslash == -1 )
		{
			// '\\' not found => not an absolute path
			rval = rtype::could_not_open_file;
		}
		else
		{
			CString dir_name = abs_filepath;
			dir_name.Truncate(index_of_last_dirslash);

			int i = 0;
			CString token = dir_name.Tokenize( "\\/", i );
			CString new_dir = "";
			while ( token != "" )
			{
				if( new_dir.GetLength() == 0 )
				{
					new_dir = token;
				}
				else
				{
					new_dir = new_dir + "\\" + token;
				}
				CFileStatus status;
				//printf("\n %s  = %d", new_dir, CFile::GetStatus( new_dir, status ) );
				if( new_dir.GetLength() > 2 // skip drive name
				&& !CFile::GetStatus( new_dir, status ) )
				{ // directory does not exists => make it
					_mkdir( new_dir );
				}
				else
				{ // directory exists
				}
				//printf("\n %s  = %d", new_dir, CFile::GetStatus( new_dir, status ) );
	            
				token = dir_name.Tokenize( "\\/", i );
			}

				try
				{
					std::ofstream file_stream( abs_filepath );

					if( file_stream )
					{
						// First write header row
						file_stream << "Supermode Number" << COMMA_CHAR;
						file_stream << "Longitudinal Mode Number" << COMMA_CHAR;
						file_stream << "Phase [mA]" << COMMA_CHAR;
						file_stream << "Rear [mA]" << COMMA_CHAR;
						file_stream << "Gain [mA]" << COMMA_CHAR;
						file_stream << "SOA [mA]" << COMMA_CHAR;
						file_stream << "Front Pair Number" << COMMA_CHAR;
						file_stream << "Constant Front [mA]" << COMMA_CHAR;
						file_stream << "Non-constant Front [mA]" << COMMA_CHAR;
						file_stream << "Frequency [GHz]" << COMMA_CHAR;
						file_stream << "Coarse Frequency [GHz]" << COMMA_CHAR;
						file_stream << "Optical Power Ratio [0:1]" << COMMA_CHAR;
						file_stream << "Optical Power [mW]" << COMMA_CHAR;
						file_stream << "Stable" << COMMA_CHAR;
						file_stream << std::endl;

						for( long i = 0 ; i < (long)(_coarse_freq_sample_points.size()) ; i++ )
						{
							// First write data rows
							char val_buf[20];
							sprintf( val_buf, "%d", _coarse_freq_sample_points[i]._sm_number );
							file_stream << val_buf << COMMA_CHAR;
							sprintf( val_buf, "%d", _coarse_freq_sample_points[i]._lm_number );
							file_stream << val_buf << COMMA_CHAR;
							sprintf( val_buf, "%.6g", _coarse_freq_sample_points[i]._I_phase );
							file_stream << val_buf << COMMA_CHAR;
							sprintf( val_buf, "%.6g", _coarse_freq_sample_points[i]._I_rear );
							file_stream << val_buf << COMMA_CHAR;
							sprintf( val_buf, "%.6g", _coarse_freq_sample_points[i]._I_gain );
							file_stream << val_buf << COMMA_CHAR;
							sprintf( val_buf, "%.6g", _coarse_freq_sample_points[i]._I_soa );
							file_stream << val_buf << COMMA_CHAR;
							sprintf( val_buf, "%d", _coarse_freq_sample_points[i]._front_pair_number );
							file_stream << val_buf << COMMA_CHAR;

							double constant_If = 0;
							double nonconstant_If = 0;

							switch( _coarse_freq_sample_points[i]._front_pair_number )
							{
							case 1:
								constant_If = _coarse_freq_sample_points[i]._I_front_1;
								nonconstant_If = _coarse_freq_sample_points[i]._I_front_2;
								break;
							case 2:
								constant_If = _coarse_freq_sample_points[i]._I_front_2;
								nonconstant_If = _coarse_freq_sample_points[i]._I_front_3;
								break;
							case 3:
								constant_If = _coarse_freq_sample_points[i]._I_front_3;
								nonconstant_If = _coarse_freq_sample_points[i]._I_front_4;
								break;
							case 4:
								constant_If = _coarse_freq_sample_points[i]._I_front_4;
								nonconstant_If = _coarse_freq_sample_points[i]._I_front_5;
								break;
							case 5:
								constant_If = _coarse_freq_sample_points[i]._I_front_5;
								nonconstant_If = _coarse_freq_sample_points[i]._I_front_6;
								break;
							case 6:
								constant_If = _coarse_freq_sample_points[i]._I_front_6;
								nonconstant_If = _coarse_freq_sample_points[i]._I_front_7;
								break;
							case 7:
								constant_If = _coarse_freq_sample_points[i]._I_front_7;
								nonconstant_If = _coarse_freq_sample_points[i]._I_front_8;
								break;
							}

							sprintf( val_buf, "%.6g", constant_If );
							file_stream << val_buf << COMMA_CHAR;
							sprintf( val_buf, "%.6g", nonconstant_If );
							file_stream << val_buf << COMMA_CHAR;

							sprintf( val_buf, "%.3f", _coarse_freq_sample_points[i]._F_measured );
							file_stream << val_buf << COMMA_CHAR;
							sprintf( val_buf, "%.3f", _coarse_freq_sample_points[i]._F_coarse );
							file_stream << val_buf << COMMA_CHAR;
							sprintf( val_buf, "%.6g", _coarse_freq_sample_points[i]._P_ratio_measured );
							file_stream << val_buf << COMMA_CHAR;
							sprintf( val_buf, "%.6g", _coarse_freq_sample_points[i]._P_measured );
							file_stream << val_buf << COMMA_CHAR;

							if( _coarse_freq_sample_points[i]._stable_F_measurement )
							{
								file_stream << "Yes";
							}
							else
							{
								file_stream << "No";
							}

							file_stream << std::endl;
						}
					}
					else
					{
						rval = rtype::could_not_open_file;
					}
				}
				catch(...)
				{
					rval = rtype::could_not_open_file;
				}
			}
		}

	}

	return rval;
}


CCGOperatingPoints::rtype
CCGOperatingPoints::writeFreqCoverageToFile(
	CString abs_filepath,
	bool overwrite )
{
	rtype rval = rtype::ok;

    long row_count = 0;
    long col_count = 0;

	if( _Freq_pts_on_freq_lines.empty() )
	{
		rval = rtype::no_data;
	}
	else
	{

		CFileStatus status;

		// Check if file already exists and overwrite not specified

		if( CFile::GetStatus( abs_filepath, status ) && overwrite == false )
		{
			rval = rtype::file_already_exists;
		}
		else
		{

        // Ensure the directory exists
		int index_of_last_dirslash = abs_filepath.ReverseFind('\\');
		if( index_of_last_dirslash == -1 )
		{
			// '\\' not found => not an absolute path
			rval = rtype::could_not_open_file;
		}
		else
		{
			CString dir_name = abs_filepath;
			dir_name.Truncate(index_of_last_dirslash);

			int i = 0;
			CString token = dir_name.Tokenize( "\\/", i );
			CString new_dir = "";
			while ( token != "" )
			{
				if( new_dir.GetLength() == 0 )
				{
					new_dir = token;
				}
				else
				{
					new_dir = new_dir + "\\" + token;
				}
				CFileStatus status;
				//printf("\n %s  = %d", new_dir, CFile::GetStatus( new_dir, status ) );
				if( new_dir.GetLength() > 2 // skip drive name
				&& !CFile::GetStatus( new_dir, status ) )
				{ // directory does not exists => make it
					_mkdir( new_dir );
				}
				else
				{ // directory exists
				}
				//printf("\n %s  = %d", new_dir, CFile::GetStatus( new_dir, status ) );
	            
				token = dir_name.Tokenize( "\\/", i );
			}

				try
				{
					std::ofstream file_stream( abs_filepath );

					if( file_stream )
					{
						// First write header row
						file_stream << "Supermode Number" << COMMA_CHAR;
						file_stream << "Longitudinal Mode Number" << COMMA_CHAR;
						file_stream << "Index on middle line of frequency (interpolated)" << COMMA_CHAR;
						file_stream << "Frequency [GHz]" << COMMA_CHAR;
						file_stream << std::endl;


						std::multimap<double,lm_freq_line_point_type2>::iterator i_pt;
						
						long index = 0;
						for( i_pt = _Freq_pts_on_freq_lines.begin() ;
							i_pt != _Freq_pts_on_freq_lines.end() ;
							i_pt++ )
						{
							double Freq_value = (*i_pt).first;
							short sm_number = (*i_pt).second.sm;
							short lm_number = (*i_pt).second.lm;
							double i_number = (*i_pt).second.i;

							// First write data rows
							char val_buf[20];
							sprintf( val_buf, "%d", sm_number );
							file_stream << val_buf << COMMA_CHAR;
							sprintf( val_buf, "%d", lm_number );
							file_stream << val_buf << COMMA_CHAR;
							sprintf( val_buf, "%g", i_number );
							file_stream << val_buf << COMMA_CHAR;
							sprintf( val_buf, "%.3f", Freq_value );
							file_stream << val_buf;

							file_stream << std::endl;
						}
					}
					else
					{
						rval = rtype::could_not_open_file;
					}
				}
				catch(...)
				{
					rval = rtype::could_not_open_file;
				}
			}
		}

	}

	return rval;
}



void
CCGOperatingPoints::
setPercentComplete( short percent_complete )
{
	EnterCriticalSection(_p_data_protect);

	*_p_percent_complete = percent_complete;

	LeaveCriticalSection(_p_data_protect);

	return;
}


void
CCGOperatingPoints::
setITUOPCount( short ITUOP_count )
{
	EnterCriticalSection(_p_data_protect);

	*_p_ITUOP_count = ITUOP_count;

	LeaveCriticalSection(_p_data_protect);

	return;
}

void
CCGOperatingPoints::
incrementDuffITUOPCount()
{
	EnterCriticalSection(_p_data_protect);

	(*_p_duff_ITUOP_count)++;

	LeaveCriticalSection(_p_data_protect);

	return;
}



// Copyright (c) 2003 Tsunami Photonics Ltd. All Rights Reserved

///////////////////////////////////////////////////////////////////////////////
///
//	Class		:
//
//	Contents	:	
//
//	Containment	:	
//
//	Multiplicity:	
//
//	Created by	:	
//
//	Author		:	
//
//	Date		:	
///
///////////////////////////////////////////////////////////////////////////////
///
//
#include "Stdafx.h"
#include <direct.h>

#include "CLoseGridFile.h"

#define new DEBUG_NEW

#define STANDARD_DELIMITORS	"\\.,	:;!'#"
#define COMMA_DELIMITOR	","

///////////////////////////////////////////////////////////////////////////////

CLoseGridFile::
CLoseGridFile(CString filePath)
{
	_writeFileStream		= NULL;
	_readFileStream			= NULL;

	_filePath				= filePath;
	_delimitor				= COMMA_DELIMITOR;

	_numRows = 0;
}

CLoseGridFile::
CLoseGridFile(CString folderPath, 
			CString fileNameWithExtension)
{
	_writeFileStream		= NULL;
	_readFileStream			= NULL;

	_fileNameWithExtension	= fileNameWithExtension;
	_folderPath				= folderPath;

	_filePath				= _folderPath + FOLDER_SEPERATOR + _fileNameWithExtension;
	_delimitor				= TAB_DELIMITOR;

	_numRows = 0;

	setFileExtension();
	setFileName();
	setFolder();
	setDrive();
}

CLoseGridFile::
CLoseGridFile(CString folderPath, 
			CString fileNameWithExtension,
			CString	delimitor)	
{
	_writeFileStream		= NULL;
	_readFileStream			= NULL;

	_fileNameWithExtension	= fileNameWithExtension;
	_folderPath				= folderPath;

	_filePath				= _folderPath + FOLDER_SEPERATOR + _fileNameWithExtension;
	_delimitor				= delimitor;

	_numRows = 0;

	setFileExtension();
	setFileName();
	setFolder();
	setDrive();
}

CLoseGridFile::
CLoseGridFile(CString folderPath,	
			CString	fileName,		
			CString	extension,
			CString	delimitor)
{
	_writeFileStream		= NULL;
	_readFileStream			= NULL;

	_folderPath				= folderPath;
	_fileName				= fileName;
	_fileExtension			= extension;
	_delimitor				= delimitor;

	_fileNameWithExtension	= _fileName + EXT_SEPERATOR + _fileExtension;
	_filePath				= _folderPath + FOLDER_SEPERATOR + _fileNameWithExtension;

	_numRows = 0;

	setFolder();
	setDrive();
}

CLoseGridFile::
CLoseGridFile()
{
	_numRows = 0;
}

CLoseGridFile::
~CLoseGridFile()
{
	if(_writeFileStream != NULL)
	{
		delete _writeFileStream;
		_writeFileStream = NULL;
	}
	if(_readFileStream != NULL)
	{
		delete _readFileStream;
		_readFileStream = NULL;
	}
}

int
CLoseGridFile::
createFolderAndFile()
{
	int errorCode = CG_NO_ERROR;

	// extract the directory path
	CString newDir;
	int curPos= 0;
	newDir = _folderPath.Tokenize("\\",curPos);
	while (newDir != "")
	{
		if( !GetStatus( newDir, _fileStatus ) )
		{ 
			// directory does not exist
			// so make it
			_mkdir( newDir );
		}
		if((curPos<=_folderPath.GetLength())&&(curPos!=-1))
		{
			newDir= newDir + "\\" + _folderPath.Tokenize("\\",curPos);
		}
		else
		{
			newDir = "";
		}
	}
	//
	///////////////////////////////////////////////////////////////////////////

    if( GetStatus( _filePath, _fileStatus ) )
    {
		// file already exists
		errorCode = ERROR_FILEEXISTS;

        remove(_filePath.GetBuffer());
	}

    // File does not exist => okay to create it.
	_writeFileStream = new std::ofstream(_filePath);
    if( !*_writeFileStream )
    {
		// error
		errorCode = ERROR_FILEOPEN;
	}


	return errorCode;
}

int
CLoseGridFile::
openFileForWriting()
{
	int errorCode = CG_NO_ERROR;

	// extract the directory path
	CString newDir;
	int curPos= 0;
	newDir = _folderPath.Tokenize("\\",curPos);
	while (newDir != "")
	{
		if( !GetStatus( newDir, _fileStatus ) )
		{ 
			// directory does not exist
			// so make it
			_mkdir( newDir );
		}
		if((curPos<=_folderPath.GetLength())&&(curPos!=-1))
		{
			newDir= newDir + "\\" + _folderPath.Tokenize("\\",curPos);
		}
		else
		{
			newDir = "";
		}
	}
	//
	///////////////////////////////////////////////////////////////////////////

    // File does not exist => okay to create it.
	_writeFileStream = new std::ofstream(_filePath);
    if( !*_writeFileStream )
    {
		// error
		errorCode = ERROR_FILEOPEN;
	}


	return errorCode;
}


///////////////////////////////////////////////////////////////////////////////
///
//	Function	:
//
//	Returns		:		
//	Parameters	:	
//	Description	:
//	Updates		:	
//
int
CLoseGridFile::
openFileForReading()
{
	int errorCode = CG_NO_ERROR;

    if( GetStatus( _filePath, _fileStatus ) )
    {
		// file exists 
		//_readFileStream = new std::ifstream(_filePath, ios::in);
		_readFileStream = new std::ifstream(_filePath, ios::in);
		if( !*_readFileStream )
		{
			// error
			errorCode = ERROR_FILEOPEN;
		}
	}
	else
	{
		errorCode = ERROR_FILENOTFOUND;
	}
	
	return errorCode;
}

///////////////////////////////////////////////////////////////////////////////
///
//	Function	:
//
//	Returns		:		
//	Parameters	:	
//	Description	:
//	Updates		:	
//
int
CLoseGridFile::
addRow(CStringArray& rowArray)
{
	int errorCode = CG_NO_ERROR;

	if(_writeFileStream!=NULL)
	{
		for(int i=0; i<rowArray.GetSize(); i++)
		{
            *_writeFileStream << rowArray[i].GetBuffer();

            if(i<rowArray.GetSize()-1)
			{
				*_writeFileStream << _delimitor.GetBuffer();
			}
		}

		// go to next line
		*_writeFileStream << endl;
	}
	else
	{
		errorCode = ERROR_FILECLOSED;
	}

	return errorCode;
}



///////////////////////////////////////////////////////////////////////////////
///
//	Function	:
//
//	Returns		:		
//	Parameters	:	
//	Description	:
//	Updates		:	
//
int
CLoseGridFile::
addEmptyLine()
{
	int errorCode = CG_NO_ERROR;

	if(_writeFileStream!=NULL)
	{
		// go to next line
		*_writeFileStream << endl;
	}
	else
	{
		errorCode = ERROR_FILECLOSED;
	}

	return errorCode;
}

    
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
///
//	Function	:
//
//	Returns		:		
//	Parameters	:	
//	Description	:
//	Updates		:	
//
int
CLoseGridFile::
addPage(vector<double>&	array,
		int	rowSize)
{
	int errorCode = CG_NO_ERROR;

	if(array.size() < (unsigned int)rowSize)
	{
		rowSize = (int)(array.size());
	}

	vector<double>::iterator iter = array.begin();
	while(iter != array.end())
	{
		CStringArray row;

		for(int i=0; i<rowSize; i++)
		{
			if(iter != array.end())
			{
				CString	rowItem		= "";
				double arrayElement = 0;

				arrayElement = *iter;
				rowItem.Format("%g", arrayElement);
				row.Add(rowItem);

				iter++;
			}
		}
		
		errorCode = addRow(row);

		if(errorCode)
		{
			break;
		}
	}

	return errorCode;
}

//
///
///////////////////////////////////////////////////////////////////////////////

int		
CLoseGridFile::
readFile(vector<double>& fileContents)
{
	int errorCode = CG_NO_ERROR;

	if(_readFileStream!=NULL)
	{
		char buffer[MAX_ROW_LENGTH];

		// go to the start of the file
		_readFileStream->clear();
		_readFileStream->seekg(0, ios::beg);
	
		*buffer = '\0';
		
		// if this is the end of file, return
		while(!(_readFileStream->eof()))
		{
			_readFileStream->getline(buffer,
									MAX_ROW_LENGTH,
									ENDLINE_CHAR);

			if(strlen(buffer) > 0) // there is something on the line
			{
				char *fp = strtok(buffer, DELIMITORS);
				while(fp)
				{
					CString	rowEntry;
					rowEntry = CString(fp);

					if(rowEntry != "")
						fileContents.push_back(atof(rowEntry));
					else
						fileContents.push_back(0);

					// next row entry
					fp = strtok(NULL, DELIMITORS);
				}
			}
		}

		_readFileStream->clear();
	}
	else
	{
		errorCode = ERROR_FILECLOSED;
	}

	return errorCode;
}

int		
CLoseGridFile::
readFile(CStringArray& fileArray)
{
	int errorCode = CG_NO_ERROR;

	if(_readFileStream!=NULL)
	{
		char buffer[MAX_ROW_LENGTH];

		// go to the start of the file
		_readFileStream->clear();
		_readFileStream->seekg(0, ios::beg);
	
		*buffer = '\0';
		
		// if this is the end of file, return
		while(!(_readFileStream->eof()))
		{
			_readFileStream->getline(buffer,
									MAX_ROW_LENGTH,
									ENDLINE_CHAR);

			if(strlen(buffer) > 0) // there is something on the line
			{
				char *fp = strtok(buffer, DELIMITORS);
				while(fp)
				{
					CString	rowEntry;

					rowEntry = CString(fp);
					fileArray.Add(rowEntry);

					// next row entry
					fp = strtok(NULL, _delimitor);
				}
			}
		}

		_readFileStream->clear();
	}
	else
	{
		errorCode = ERROR_FILECLOSED;
	}

	return errorCode;
}

int
CLoseGridFile::
readRow(int				rowNumber,
		CStringArray&	rowArray)
{
	int errorCode = CG_NO_ERROR;

	if(_readFileStream!=NULL)
	{
		char buffer[MAX_ROW_LENGTH];

		// go to the start of the file
		_readFileStream->clear();
		_readFileStream->seekg(0, ios::beg);

		for(int i=0; i<rowNumber; i++)
		{
			*buffer = '\0';
			_readFileStream->getline(buffer,
									MAX_ROW_LENGTH,
									ENDLINE_CHAR);
		}

		// if this is the end of file, return
		if(_readFileStream->eof())
		{
			errorCode = ERROR_ENDOFFILE;
		}
		else
		{
			if(strlen(buffer) > 0) // there is something on the line
			{
				char *fp = strtok(buffer, DELIMITORS);
				while(fp)
				{
					CString	rowEntry;

					rowEntry = CString(fp);
					rowArray.Add(rowEntry);

					// next row entry
					fp = strtok(NULL, DELIMITORS);
				}
			}
		}

		_readFileStream->clear();
		_readFileStream->seekg(0, ios::beg);

	}
	else
	{
		errorCode = ERROR_FILECLOSED;
	}

	return errorCode;
}

//
///
////////////////////////////////////////////////////////////////////////////////////

int
CLoseGridFile::
readColumn(int				columnNumber,
		   bool				includeFirstCell,
		   vector<double>&	columnArray)
{
	int errorCode = CG_NO_ERROR;

	if(_readFileStream!=NULL)
	{
		char buffer[MAX_COL_LENGTH];

		// go to the start of the file
		_readFileStream->clear();
		_readFileStream->seekg(0, ios::beg);
	
		if(includeFirstCell == false)
		{
			// do initial read to move the pointer along
			*buffer = '\0';
			_readFileStream->getline(buffer,
									MAX_ROW_LENGTH,
									ENDLINE_CHAR);
		}

		while(!(_readFileStream->eof()))
		{
			*buffer = '\0';
			_readFileStream->getline(buffer,
									MAX_ROW_LENGTH,
									ENDLINE_CHAR);


			if(strlen(buffer) > 0) // there is something on the line
			{
				char *fp = strtok(buffer, DELIMITORS);
				for(int i=1; i!=columnNumber; i++)
				{
					fp = strtok(NULL, DELIMITORS);
				}

				double entry;
				if(fp)
				{
					CString entryString = CString(fp);

					if(entryString!="")
						entry = atof((LPCTSTR)entryString);
					else
						entry = 0;
				}
				else
				{
					entry = 0;
				}

				columnArray.push_back(entry);
			}
		}

		_readFileStream->clear();

	}
	else
	{
		errorCode = ERROR_FILECLOSED;
	}

	return errorCode;
}

int
CLoseGridFile::
readColumn(int			columnNumber,
		   bool			includeFirstCell,
		   vector<int>&	columnArray)
{
	int errorCode = CG_NO_ERROR;

	if(_readFileStream!=NULL)
	{
		char buffer[MAX_COL_LENGTH];

		// go to the start of the file
		_readFileStream->clear();
		_readFileStream->seekg(0, ios::beg);
	
		if(includeFirstCell == false)
		{
			// do initial read to move the pointer along
			*buffer = '\0';
			_readFileStream->getline(buffer,
									MAX_ROW_LENGTH,
									ENDLINE_CHAR);
		}

		while(!(_readFileStream->eof()))
		{
			*buffer = '\0';
			_readFileStream->getline(buffer,
									MAX_ROW_LENGTH,
									ENDLINE_CHAR);


			if(strlen(buffer) > 0) // there is something on the line
			{
				char *fp = strtok(buffer, DELIMITORS);
				for(int i=1; i!=columnNumber; i++)
				{
					fp = strtok(NULL, DELIMITORS);
				}

				int entry;
				if(fp)
				{
					CString entryString = CString(fp);

					if(entryString!="")
						entry = atoi((LPCTSTR)entryString);
					else
						entry = 0;
				}
				else
				{
					entry = 0;
				}

				columnArray.push_back(entry);
			}
		}

		_readFileStream->clear();

	}
	else
	{
		errorCode = ERROR_FILECLOSED;
	}

	return errorCode;
}

int
CLoseGridFile::
readColumn(int				columnNumber,
		   bool				includeFirstCell,
		   CStringArray&	columnArray)
{
	int errorCode = CG_NO_ERROR;

	if(_readFileStream!=NULL)
	{
		char buffer[MAX_COL_LENGTH];

		// go to the start of the file
		_readFileStream->clear();
		_readFileStream->seekg(0, ios::beg);
	
		if(includeFirstCell == false)
		{
			// do initial read to move the pointer along
			*buffer = '\0';
			_readFileStream->getline(buffer,
									MAX_ROW_LENGTH,
									ENDLINE_CHAR);
		}

		while(!(_readFileStream->eof()))
		{
			*buffer = '\0';
			_readFileStream->getline(buffer,
									MAX_ROW_LENGTH,
									ENDLINE_CHAR);


			if(strlen(buffer) > 0) // there is something on the line
			{
				char *fp = strtok(buffer, DELIMITORS);

				for(int i=1; i<columnNumber; i++)
				{
					fp = strtok(NULL, DELIMITORS);
				}

				CString entryString = CString(fp);
				if(fp)
				{
					columnArray.Add(entryString);
				}
			}
		}

		_readFileStream->clear();

	}
	else
	{
		errorCode = ERROR_FILECLOSED;
	}

	return errorCode;
}

int
CLoseGridFile::
numRows()
{
	int numRows = 0;

	if(_numRows == 0)
	{
		char buffer[MAX_COL_LENGTH];

		// go to the start of the file
		_readFileStream->clear();
		_readFileStream->seekg(0, ios::beg);

		while(!(_readFileStream->eof()))
		{
			*buffer = '\0';
			_readFileStream->getline(buffer,
									MAX_ROW_LENGTH,
									ENDLINE_CHAR);

			if(strlen(buffer) > 0) // there is something on the line
			{
				_numRows++;
			}
		}

		// go to the start of the file
		_readFileStream->clear();
		_readFileStream->seekg(0, ios::beg);
		
	}

	numRows = _numRows;

	return numRows;
}

//
///
////////////////////////////////////////////////////////////////////////////////////
///
//
void
CLoseGridFile::
splitIntoSeperateStrings(CString		theString,
						CStringArray&	stringArray,
						CString			delimitors)
{
	int curPos= 0;

	CString extractedString;
	extractedString = theString.Tokenize(delimitors,
										curPos);
	while (extractedString != "")
	{
		stringArray.Add(extractedString);
		extractedString = theString.Tokenize(delimitors,
											curPos);
	}
}

void
CLoseGridFile::
splitIntoSeperateStrings(CString		theString,
						CStringArray&	stringArray)
{
	int curPos= 0;

	CString extractedString;
	extractedString = theString.Tokenize(STANDARD_DELIMITORS,
										curPos);
	while (extractedString != "")
	{
		stringArray.Add(extractedString);
		extractedString = theString.Tokenize(STANDARD_DELIMITORS,
											curPos);
	}
}

void	
CLoseGridFile::
setFileExtension()
{
	CStringArray fileNameArray;
	splitIntoSeperateStrings(_fileNameWithExtension,
							fileNameArray,					
							_delimitor);

	if(fileNameArray.GetCount()==2)
		_fileExtension = fileNameArray.GetAt(1);
	else
		_fileExtension = "";
}

void	
CLoseGridFile::
setFileName()
{
	CStringArray fileNameArray;
	splitIntoSeperateStrings(_fileNameWithExtension,
							fileNameArray,					
							_delimitor);

	if(fileNameArray.GetCount()==2)
		_fileName = fileNameArray.GetAt(0);
	else
		_fileName = "";

}

void	
CLoseGridFile::
setFolder()
{
	CStringArray folderArray;
	splitIntoSeperateStrings(_folderPath,
							folderArray,					
							FOLDER_SEPERATOR);

	if(folderArray.GetCount()> 0)
	{
		INT_PTR numFolders  = folderArray.GetCount();
		_folder = folderArray.GetAt(numFolders-1);
	}
}

void	
CLoseGridFile::
setDrive()
{
	CStringArray folderArray;
	splitIntoSeperateStrings(_folderPath,
							folderArray,					
							FOLDER_SEPERATOR);

	if(folderArray.GetCount()> 0)
	{
		INT_PTR firstFolderIndex  = 0;
		_drive = folderArray.GetAt(firstFolderIndex);
	}
}
//
///
////////////////////////////////////////////////////////////////////////////////////
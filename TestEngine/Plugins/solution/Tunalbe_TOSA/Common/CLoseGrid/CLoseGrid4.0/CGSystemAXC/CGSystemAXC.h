// Copyright (c) 2004 PXIT. All Rights Reserved
//
// FileName    : CGSystemAXC.h
// Description : main header file for CGSystemAXC.DLL
//
// Rev#  | Date          | Author               | Description of change
// --------------------------------------------------------------
// 0.1   | 12 July 2004   | Frank D'Arcy         | Initial Draft
//

#pragma once


#if !defined( __AFXCTL_H__ )
#error include 'afxctl.h' before including this file
#endif

#include "resource.h"       // main symbols


// CCGSystemAXCApp : See CGSystemAXC.cpp for implementation.

class CCGSystemAXCApp : public COleControlModule
{
public:
	BOOL InitInstance();
	int ExitInstance();
};

extern const GUID CDECL _tlid;
extern const WORD _wVerMajor;
extern const WORD _wVerMinor;


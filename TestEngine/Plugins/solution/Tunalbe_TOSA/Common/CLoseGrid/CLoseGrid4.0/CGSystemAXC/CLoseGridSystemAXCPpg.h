// Copyright (c) 2004 PXIT. All Rights Reserved
//
// FileName    : CLoseGridSystemAXCPpg.h
// Description : Declaration of the CCLoseGridSystemAXCPropPage property page class.
//
// Rev#  | Date          | Author               | Description of change
// --------------------------------------------------------------
// 0.1   | 12 July 2004   | Frank D'Arcy         | Initial Draft
//

#pragma once


// CCLoseGridSystemAXCPropPage : See CLoseGridSystemAXCPpg.cpp.cpp for implementation.

class CCLoseGridSystemAXCPropPage : public COlePropertyPage
{
	DECLARE_DYNCREATE(CCLoseGridSystemAXCPropPage)
	DECLARE_OLECREATE_EX(CCLoseGridSystemAXCPropPage)

// Constructor
public:
	CCLoseGridSystemAXCPropPage();

// Dialog Data
	enum { IDD = IDD_PROPPAGE_CGSYSTEMAXC };

// Implementation
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Message maps
protected:
	DECLARE_MESSAGE_MAP()
};


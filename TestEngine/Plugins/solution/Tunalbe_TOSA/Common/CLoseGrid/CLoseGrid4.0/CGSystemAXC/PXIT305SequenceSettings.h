// Copyright (c) 2004 Tsunami Photonics Ltd. All Rights Reserved

///////////////////////////////////////////////////////////////////////////////
///
//	Class	:
//
//	Contents:
//
//	Author	:
//
///
///////////////////////////////////////////////////////////////////////////////
///
//
#ifndef __PXIT305SEQUENCESETTINGS_H_
#define __PXIT305SEQUENCESETTINGS_H_

#pragma once

#include <vector>

using namespace std;

/////////////////////////////////////////////////////////////////////

class PXIT305SequenceSettings
{
public:
	PXIT305SequenceSettings(CString			moduleName,
							vector<double>&	currents,
							short			outputConnection,
							double			constantCurrent,
							short			inSequence,
							short			sourceDelay,
							short			measureDelay);

	~PXIT305SequenceSettings(void);

	///////////////////////////////////////////////////////
	///
	//	Generic PXIT305SequenceSettings functions
	//

	///////////////////////////////////
	///
	//	Get and set functions
	//

	//
	///////////////////////////////////////////////////////

	CString			_moduleName;
	vector<double>	_currents;
	short			_outputConnection;
	double			_constantCurrent;
	short			_inSequence;
	short			_sourceDelay;
	short			_measureDelay;
};
//
///////////////////////////////////////////////////////////////////////////////
#endif

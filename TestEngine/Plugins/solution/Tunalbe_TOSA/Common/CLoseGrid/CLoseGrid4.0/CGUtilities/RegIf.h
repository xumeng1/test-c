// Copyright (c) 2004 PXIT. All Rights Reserved
//
// FileName    : RegIf.h
// Description : Declaration of RegInterface class
//               Handles saving and retrieving Registry data
//               Singleton class
//
// Rev#  | Date          | Author               | Description of change
// ---------------------------------------------------------------------
// 0.1   | 26 July 2004  | Frank D'Arcy         | Copied from CGResearch
//

#pragma once

#include <afx.h>
#include <atlbase.h>

#define REG_BOOL_YES_VALUE "Yes"
#define REG_BOOL_NO_VALUE "No"


#define P_REG RegInterface::instance()

class RegInterface : CRegKey
{
public:

	typedef enum return_type
	{
		ok = 0,
		registry_key_error,
		registry_data_error
	};

	// Singleton class
	static	RegInterface* instance();


	return_type	getSubKeyList(LPCTSTR keyName, 
						CStringArray& subKeyList); 

	return_type	deleteKey(LPCTSTR keyName);
	return_type	createKey(LPCTSTR keyName);

	bool		keyExists(LPCTSTR keyName);

	return_type	getBoolValue(LPCTSTR keyName, CString valueName, bool& boolValue);
	return_type	getCStringValue(LPCTSTR keyName, CString valueName, CString& stringValue);
	return_type	getLongValue(LPCTSTR keyName, CString valueName, long& longValue);
	return_type	getIntValue(LPCTSTR keyName, CString valueName, int& intValue);
	return_type	getFloatValue(LPCTSTR keyName, CString valueName, float& floatValue);
	return_type	getDoubleValue(LPCTSTR keyName, CString valueName, double& doubleValue);

	return_type	setBoolValue(LPCTSTR keyName, CString valueName, bool boolValue, bool yesNoUnits = false);
	return_type	setCStringValue(LPCTSTR keyName, CString valueName, CString stringValue);
	return_type	setLongValue(LPCTSTR keyName, CString valueName, long longValue);
	return_type	setIntValue(LPCTSTR keyName, CString valueName, int intValue);
	return_type	setFloatValue(LPCTSTR keyName, CString valueName, float floatValue);
	return_type	setDoubleValue(LPCTSTR keyName, CString valueName, double doubleValue);

protected:
	// Singleton class
	RegInterface();
	RegInterface(const RegInterface&);
	~RegInterface(void);

	// = operator for returning the instance
	RegInterface& operator= (const RegInterface&);

private:
	static RegInterface* theInstance;
	
			// initialise memebers to
			// default values
	void	initMembers();

			// registry errors
	char	errorString[1024];

	return_type	openForValueQuery(LPCTSTR keyName, HKEY& hKey);
	return_type	openForSetValue(LPCTSTR keyName, HKEY& hKey);
	return_type	openForSubKeyRead(LPCTSTR keyName, HKEY& hKey);

			// Function which converts any
			// reg errors into a error string
			// which can be logged
	void	registryError(long regError, char* errStr);

};

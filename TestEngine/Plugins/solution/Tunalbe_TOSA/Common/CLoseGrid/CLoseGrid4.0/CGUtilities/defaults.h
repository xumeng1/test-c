// Copyright (c) 2004 PXIT. All Rights Reserved
//
// FileName    : defaults.h
// Description : Defines defaults and common definitions
//
// Rev#  | Date          | Author               | Description of change
// --------------------------------------------------------------
// 0.1   | 20 July 2004   | Frank D'Arcy         | Initial Draft
//

#pragma once 

#define CGSYSTEM_DEFAULT_RESULTS_DIR "C:\\Program Files\\PXIT\\CLoseGrid4.0"

#define RAMP_DIRECTION_PHASE		CString("P")
#define RAMP_DIRECTION_MIDDLE_LINE	CString("M")
#define RAMP_DIRECTION_REAR			CString("R")
#define RAMP_DIRECTION_FRONT		CString("F")

#define CHANNEL_1_ON_306 1
#define CHANNEL_2_ON_306 2

#define	NO_CHANNEL		0
#define CHANNEL_1		1
#define CHANNEL_2		2
#define ALL_CHANNELS	3

#define DISCONNECT_OUTPUT		0
#define CONNECT_OUTPUT			1
#define CONNECT_REVERSED_OUTPUT	2

#define NOT_IN_SEQUENCE		0
#define IN_SEQUENCE			1

#define CSV CString(".csv")
#define USCORE CString("_")
#define MATRIX_ CString("MATRIX_")
#define MATRIX_FILTERED_ CString("MATRIX_F_")
#define JUMPS_ CString("JUMPS_")
#define LINKED_JUMPS_ CString("JUMPS_L_")

#define	QA_METRICS_FILE_NAME		CString("qaMetrics")
#define	PASSFAIL_FILE_NAME			CString("passFailMetrics")
#define	COLLATED_PASSFAIL_FILE_NAME	CString("collatedPassFailResults")

#define LASER_TYPE_ID_DSDBR01_CSTRING CString("DSDBR01")

#define MEAS_TYPE_ID_POWERRATIO_CSTRING CString("PowerRatio")
#define MEAS_TYPE_ID_POWER_CSTRING CString("Power")

#define MEAS_TYPE_ID_FORWARDPOWER_CSTRING  CString("forwardPower")
#define MEAS_TYPE_ID_REVERSEPOWER_CSTRING  CString("reversePower")
#define MEAS_TYPE_ID_HYSTERESISPOWER_CSTRING  CString("hysteresisPower")

#define MEAS_TYPE_ID_FILTEREDPOWER_CSTRING  CString("filteredPower")
#define MEAS_TYPE_ID_FORWARDFILTEREDPOWER_CSTRING  CString("forwardFilteredPower")
#define MEAS_TYPE_ID_REVERSEFILTEREDPOWER_CSTRING  CString("reverseFilteredPower")

#define MEAS_TYPE_ID_FORWARDPOWERRATIO_CSTRING  CString("forwardPowerRatio")
#define MEAS_TYPE_ID_REVERSEPOWERRATIO_CSTRING  CString("reversePowerRatio")
#define MEAS_TYPE_ID_HYSTERESISPOWERRATIO_CSTRING  CString("hysteresisPowerRatio")

#define MEAS_TYPE_ID_FILTEREDPOWERRATIO_CSTRING CString("FilteredPowerRatio")

#define MEAS_TYPE_ID_FORWARDPD1_CSTRING  CString("forwardPD1Current")
#define MEAS_TYPE_ID_REVERSEPD1_CSTRING  CString("reversePD1Current")
#define MEAS_TYPE_ID_FORWARDPD2_CSTRING  CString("forwardPD2Current")
#define MEAS_TYPE_ID_REVERSEPD2_CSTRING  CString("reversePD2Current")

#define FRONT_CURRENT_CSTRING CString("If")
#define REAR_CURRENT_CSTRING CString("Ir")
#define PHASE_CURRENT_CSTRING CString("Ip")
#define MIDDLELINE_CURRENT_CSTRING CString("Im")

#define LASER_ID_BAD_CHARS " \t\r\n"
#define MODULE_ID_BAD_CHARS "\t\r\n"
#define ID_MAX_LENGTH 32

#define NUMBER_STR_LENGTH 32
#define MAX_LINE_LENGTH 10000
#define DELIMITERS ",\t\r\b\f\n "
#define COMMA_CHAR ','

#define INSTANTIATED_STATE CString("Instantiated")
#define STARTUP_STATE CString("Startup")
#define SETUP_STATE CString("Setup")
#define BUSY_STATE CString("Busy")
#define COMPLETE_STATE CString("Complete")

#define MAX_SEQUENCE_SIZE 32000

#define BIG_NUM 1E99

#define MIN_COARSEFREQPOLYCOEFFS 2
#define MAX_COARSEFREQPOLYCOEFFS 100
#define MAX_NUMBEROFCOARSEFREQPOLYSAMPLES 1000
#define MAX_ADDITIONAL_SAMPLES 10
#define DEFAULT_CFREQ_POLY_COEFF_0 195333.369
#define DEFAULT_CFREQ_POLY_COEFF_1 -5008.548
#define MIN_VALID_FREQ_MEAS 100000 // GHz
#define MAX_VALID_FREQ_MEAS 300000 // GHz
#define MIN_DIRECT_OPTICAL_POWER 0.01 //mW

#define MAX_ITERATIONS_TO_FIND_ITU_PT 100
#define MIN_PT_GAP_TO_FIND_ITU_PT 0.01

#define SM_NOT_LOADED -1

#define MAP_TYPE_SAWTOOTH 0
#define MAP_TYPE_TRIANGLE 1

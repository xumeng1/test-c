// Copyright (c) 2004 PXIT. All Rights Reserved
//
// FileName    : CGRegValues.cpp
// Description : Definition of CCGRegValues class
//               Represents all Registry stored data
//               Singleton class
//
// Rev#  | Date          | Author               | Description of change
// ---------------------------------------------------------------------
// 0.1   | 26 July 2004  | Frank D'Arcy         | Initial Draft
// 0.2   | 12 Aug 2004   | Frank D'Arcy         | Cleaned up
//       |               |                      | Added Moving Average Num
// 0.3   | 10 Sept 2004  | Frank D'Arcy         | Added DSDBR01 for each entry
// 0.4   | 19 Sept 2004  | Frank D'Arcy         | Added _DSDBR01_OMBD_read_boundaries_from_file
//       |               |                      | and _DSDBR01_OMBD_write_boundaries_to_file
// 0.5   | 23 Sept 2004  | Frank D'Arcy         | Added Overall Map data collection parameters _DSDBR01_OMDC_
// 0.6   | 27 Sept 2004  | Frank D'Arcy         | Moving some common data collection params,
//       |               |                      | and adding option to overwrite files or not
// 0.7   | 28 Sept 2004  | Frank D'Arcy         | Adding some supermode data collection params
// 0.8   | 30 Sept 2004  | Frank D'Arcy         | Adding min length of supermode middle line
// 0.9   | 05 Oct  2004  | Frank D'Arcy         | Adding option to stub calls to CloseGrid Research
// 1.0   | 18 Oct  2004  | Frank D'Arcy         | Adding Boundary Detection parameters for supermode maps
// 1.1   | 29 Oct  2004  | Frank D'Arcy         | Adding more Boundary Detection parameters for supermode maps
// 1.2   | 02 Nov  2004  | Frank D'Arcy         | Adding coarse frequency parameters
// 1.3   | 15 Nov  2004  | Frank D'Arcy         | Adding ITU Grid parameters
// 1.4   | 13 Dec  2004  | Frank D'Arcy         | Adding option to ignore BDC pass/fail with front section change
// 1.5   | 11 Feb  2005  | Frank D'Arcy         | Adding options to output filtered maps and photodiode current maps
// 1.6   | 26 May  2005  | Frank D'Arcy         | Adding option to reinit wavemeter on bad read
// 1.7   | 04 Mar  2006  | Frank D'Arcy         | Moved functions into other files
//

#include "StdAfx.h"
#include "CGRegValues.h"
#include "RegIf.h"
#include "CGRegDefs.h"


// Only instance of this class
CCGRegValues* 
CCGRegValues::_p_instance = NULL;

CCGRegValues::CCGRegValues(void)
{
	_p_instance = this;

	setDefaults();
}

void
CCGRegValues::setDefaults()
{
	// set default values

	CCGRegEntry reg_entry;
	_reg_entries.clear();

	_DSDBR01_BASE_results_directory = DEFAULT_DSDBR01_BASE_RESULTS_DIR;
	reg_entry.reg_key_name = CG_DSDBR01_BASE_REGKEY;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_BASE_RESULTS_DIR;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_CString;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_BASE_stub_hardware_calls = DEFAULT_DSDBR01_BASE_STUB_HARDWARE_CALLS;
	reg_entry.reg_key_name = CG_DSDBR01_BASE_REGKEY;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_BASE_STUB_HARDWARE_CALLS;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_bool;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_CMDC_gain_module_name = DEFAULT_DSDBR01_CMDC_GAIN_MODULE_NAME;
	reg_entry.reg_key_name = CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CMDC);
	reg_entry.reg_entry_name = REGNAME_DSDBR01_CMDC_GAIN_MODULE_NAME;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_CString;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_CMDC_soa_module_name = DEFAULT_DSDBR01_CMDC_SOA_MODULE_NAME;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_CMDC_SOA_MODULE_NAME;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_CMDC_rear_module_name = DEFAULT_DSDBR01_CMDC_REAR_MODULE_NAME;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_CMDC_REAR_MODULE_NAME;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_CMDC_phase_module_name = DEFAULT_DSDBR01_CMDC_PHASE_MODULE_NAME;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_CMDC_PHASE_MODULE_NAME;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_CMDC_front1_module_name = DEFAULT_DSDBR01_CMDC_FRONT1_MODULE_NAME;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_CMDC_FRONT1_MODULE_NAME;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_CMDC_front2_module_name = DEFAULT_DSDBR01_CMDC_FRONT2_MODULE_NAME;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_CMDC_FRONT2_MODULE_NAME;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_CMDC_front3_module_name = DEFAULT_DSDBR01_CMDC_FRONT3_MODULE_NAME;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_CMDC_FRONT3_MODULE_NAME;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_CMDC_front4_module_name = DEFAULT_DSDBR01_CMDC_FRONT4_MODULE_NAME;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_CMDC_FRONT4_MODULE_NAME;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_CMDC_front5_module_name = DEFAULT_DSDBR01_CMDC_FRONT5_MODULE_NAME;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_CMDC_FRONT5_MODULE_NAME;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_CMDC_front6_module_name = DEFAULT_DSDBR01_CMDC_FRONT6_MODULE_NAME;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_CMDC_FRONT6_MODULE_NAME;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_CMDC_front7_module_name = DEFAULT_DSDBR01_CMDC_FRONT7_MODULE_NAME;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_CMDC_FRONT7_MODULE_NAME;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_CMDC_front8_module_name = DEFAULT_DSDBR01_CMDC_FRONT8_MODULE_NAME;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_CMDC_FRONT8_MODULE_NAME;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_CMDC_306II_module_name = DEFAULT_DSDBR01_CMDC_306II_MODULE_NAME;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_CMDC_306II_MODULE_NAME;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_CString;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_CMDC_306II_direct_channel = DEFAULT_DSDBR01_CMDC_306II_DIRECT_CHANNEL;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_CMDC_306II_DIRECT_CHANNEL;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_CMDC_306II_filtered_channel = DEFAULT_DSDBR01_CMDC_306II_FILTERED_CHANNEL;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_CMDC_306II_FILTERED_CHANNEL;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_CMDC_306EE_module_name = DEFAULT_DSDBR01_CMDC_306EE_MODULE_NAME;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_CMDC_306EE_MODULE_NAME;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_CString;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_CMDC_306EE_photodiode1_channel = DEFAULT_DSDBR01_CMDC_306EE_PHOTODIODE1_CHANNEL;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_CMDC_306EE_PHOTODIODE1_CHANNEL;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_CMDC_306EE_photodiode2_channel = DEFAULT_DSDBR01_CMDC_306EE_PHOTODIODE2_CHANNEL;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_CMDC_306EE_PHOTODIODE2_CHANNEL;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_CMDC_wavemeter_retries = DEFAULT_DSDBR01_CMDC_WAVEMETER_RETRIES;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_CMDC_WAVEMETER_RETRIES;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);



	_DSDBR01_OMDC_ramp_direction = DEFAULT_DSDBR01_OMDC_RAMP_DIRECTION;
	reg_entry.reg_key_name = CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMDC);
	reg_entry.reg_entry_name = REGNAME_DSDBR01_OMDC_RAMP_DIRECTION;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_CString;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_OMDC_forward_and_reverse = DEFAULT_DSDBR01_OMDC_FORWARD_AND_REVERSE;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_OMDC_FORWARD_AND_REVERSE;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_bool;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_OMDC_measure_photodiode_currents = DEFAULT_DSDBR01_OMDC_MEASURE_PHOTODIODE_CURRENTS;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_OMDC_MEASURE_PHOTODIODE_CURRENTS;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_bool;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_OMDC_write_filtered_power_maps = DEFAULT_DSDBR01_OMDC_WRITE_FILTERED_POWER_MAPS;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_OMDC_WRITE_FILTERED_POWER_MAPS;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_bool;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_OMDC_rear_currents_abspath = DEFAULT_DSDBR01_OMDC_REAR_CURRENTS_ABSPATH;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_OMDC_REAR_CURRENTS_ABSPATH;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_CString;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_OMDC_front_currents_abspath = DEFAULT_DSDBR01_OMDC_FRONT_CURRENTS_ABSPATH;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_OMDC_FRONT_CURRENTS_ABSPATH;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_CString;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_OMDC_source_delay = DEFAULT_DSDBR01_OMDC_SOURCE_DELAY;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_OMDC_SOURCE_DELAY;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_OMDC_measure_delay = DEFAULT_DSDBR01_OMDC_MEASURE_DELAY;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_OMDC_MEASURE_DELAY;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_OMDC_overwrite_existing_files = DEFAULT_DSDBR01_OMDC_OVERWRITE_EXISTING_FILES;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_OMDC_OVERWRITE_EXISTING_FILES;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_bool;
	_reg_entries.push_back(reg_entry);



	_DSDBR01_OMBD_read_boundaries_from_file = DEFAULT_DSDBR01_OMBD_READ_BOUNDARIES_FROM_FILE;
	reg_entry.reg_key_name = CG_DSDBR01_BASE_REGKEY + CString(RELATIVE_REGKEY_DSDBR01_OMBD);
	reg_entry.reg_entry_name = REGNAME_DSDBR01_OMBD_READ_BOUNDARIES_FROM_FILE;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_bool;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_OMBD_write_boundaries_to_file = DEFAULT_DSDBR01_OMBD_WRITE_BOUNDARIES_TO_FILE;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_OMBD_WRITE_BOUNDARIES_TO_FILE;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_bool;
	_reg_entries.push_back(reg_entry);

    _DSDBR01_OMBD_median_filter_rank = DEFAULT_DSDBR01_OMBD_MEDIAN_FILTER_RANK;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_OMBD_MEDIAN_FILTER_RANK;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_OMBD_max_deltaPr_in_sm = DEFAULT_DSDBR01_OMBD_MAX_DELTAPR_IN_SM;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_OMBD_MAX_DELTAPR_IN_SM;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_OMBD_min_points_width_of_sm = DEFAULT_DSDBR01_OMBD_MIN_POINTS_WIDTH_OF_SM;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_OMBD_MIN_POINTS_WIDTH_OF_SM;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_OMBD_ml_moving_ave_filter_n = DEFAULT_DSDBR01_OMBD_ML_MOVING_AVERAGE_FILTER_N;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_OMBD_ML_MOVING_AVERAGE_FILTER_N;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_OMBD_ml_min_length = DEFAULT_DSDBR01_OMBD_ML_MIN_LENGTH;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_OMBD_ML_MIN_LENGTH;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

    _DSDBR01_OMBD_ml_extend_to_corner = DEFAULT_DSDBR01_OMBD_ML_EXTEND_TO_CORNER;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_OMBD_ML_EXTEND_TO_CORNER;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_bool;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMDC_ramp_direction = DEFAULT_DSDBR01_SMDC_RAMP_DIRECTION;
	reg_entry.reg_key_name = CG_DSDBR01_BASE_REGKEY + CString(RELATIVE_REGKEY_DSDBR01_SMDC);
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMDC_RAMP_DIRECTION;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_CString;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMDC_phase_currents_abspath = DEFAULT_DSDBR01_SMDC_PHASE_CURRENTS_ABSPATH;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMDC_PHASE_CURRENTS_ABSPATH;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_CString;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMDC_measure_photodiode_currents = DEFAULT_DSDBR01_SMDC_MEASURE_PHOTODIODE_CURRENTS;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMDC_MEASURE_PHOTODIODE_CURRENTS;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_bool;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMDC_write_filtered_power_maps = DEFAULT_DSDBR01_SMDC_WRITE_FILTERED_POWER_MAPS;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMDC_WRITE_FILTERED_POWER_MAPS;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_bool;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMDC_source_delay = DEFAULT_DSDBR01_SMDC_SOURCE_DELAY;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMDC_SOURCE_DELAY;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMDC_measure_delay = DEFAULT_DSDBR01_SMDC_MEASURE_DELAY;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMDC_MEASURE_DELAY;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMDC_overwrite_existing_files = DEFAULT_DSDBR01_SMDC_OVERWRITE_EXISTING_FILES;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMDC_OVERWRITE_EXISTING_FILES;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_bool;
	_reg_entries.push_back(reg_entry);


	_DSDBR01_SMBD_read_boundaries_from_file = DEFAULT_DSDBR01_SMBD_READ_BOUNDARIES_FROM_FILE;
	reg_entry.reg_key_name = CG_DSDBR01_BASE_REGKEY + CString(RELATIVE_REGKEY_DSDBR01_SMBD);
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBD_READ_BOUNDARIES_FROM_FILE;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_bool;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMBD_write_boundaries_to_file = DEFAULT_DSDBR01_SMBD_WRITE_BOUNDARIES_TO_FILE;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBD_WRITE_BOUNDARIES_TO_FILE;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_bool;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMBD_write_middle_of_upper_lines_to_file = DEFAULT_DSDBR01_SMBD_write_middle_of_upper_lines_to_file;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBD_write_middle_of_upper_lines_to_file;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_bool;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMBD_write_middle_of_lower_lines_to_file = DEFAULT_DSDBR01_SMBD_write_middle_of_lower_lines_to_file;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBD_write_middle_of_lower_lines_to_file;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_bool;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMBD_write_debug_files = DEFAULT_DSDBR01_SMBD_WRITE_DEBUG_FILES;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBD_WRITE_DEBUG_FILES;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_bool;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMBD_extrapolate_to_map_edges = DEFAULT_DSDBR01_SMBD_extrapolate_to_map_edges;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBD_extrapolate_to_map_edges;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_bool;
	_reg_entries.push_back(reg_entry);

    _DSDBR01_SMBD_median_filter_rank = DEFAULT_DSDBR01_SMBD_MEDIAN_FILTER_RANK;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBD_MEDIAN_FILTER_RANK;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMBD_max_deltaPr_in_lm = DEFAULT_DSDBR01_SMBD_MAX_DELTAPR_IN_LM;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBD_MAX_DELTAPR_IN_LM;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMBD_min_points_width_of_lm = DEFAULT_DSDBR01_SMBD_MIN_POINTS_WIDTH_OF_LM;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBD_MIN_POINTS_WIDTH_OF_LM;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMBD_max_points_width_of_lm = DEFAULT_DSDBR01_SMBD_MAX_POINTS_WIDTH_OF_LM;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBD_MAX_POINTS_WIDTH_OF_LM;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMBD_ml_moving_ave_filter_n = DEFAULT_DSDBR01_SMBD_ML_MOVING_AVERAGE_FILTER_N;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBD_ML_MOVING_AVERAGE_FILTER_N;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMBD_ml_min_length = DEFAULT_DSDBR01_SMBD_ML_MIN_LENGTH;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBD_ML_MIN_LENGTH;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMBD_vertical_percent_shift = DEFAULT_DSDBR01_SMBD_VERTICAL_PERCENT_SHIFT;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBD_VERTICAL_PERCENT_SHIFT;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMBD_zprc_hysteresis_upper_threshold = DEFAULT_DSDBR01_SMBD_ZPRC_HYSTERESIS_UPPER_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBD_ZPRC_HYSTERESIS_UPPER_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMBD_zprc_hysteresis_lower_threshold = DEFAULT_DSDBR01_SMBD_ZPRC_HYSTERESIS_LOWER_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBD_ZPRC_HYSTERESIS_LOWER_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMBD_zprc_max_hysteresis_rise = DEFAULT_DSDBR01_SMBD_ZPRC_MAX_HYSTERESIS_RISE;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBD_ZPRC_MAX_HYSTERESIS_RISE;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMBD_zprc_min_points_separation = DEFAULT_DSDBR01_SMBD_ZPRC_MIN_POINTS_SEPARATION;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBD_ZPRC_MIN_POINTS_SEPARATION;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMBD_zprc_max_points_to_reverse_jump = DEFAULT_DSDBR01_SMBD_ZPRC_MAX_POINTS_TO_REVERSE_JUMP;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBD_ZPRC_MAX_POINTS_TO_REVERSE_JUMP;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMBD_zprc_max_hysteresis_points = DEFAULT_DSDBR01_SMBD_ZPRC_MAX_HYSTERESIS_POINTS;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBD_ZPRC_MAX_HYSTERESIS_POINTS;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMBD_ramp_abspath = DEFAULT_DSDBR01_SMBD_RAMP_ABSPATH;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBD_RAMP_ABSPATH;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_CString;
	_reg_entries.push_back(reg_entry);


    _DSDBR01_SMBDFWD_I_ramp_boundaries_max = DEFAULT_DSDBR01_SMBDFWD_I_RAMP_BOUNDARIES;
	reg_entry.reg_key_name = CG_DSDBR01_BASE_REGKEY + CString(RELATIVE_REGKEY_DSDBR01_SMBDFWD);
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBDFWD_I_RAMP_BOUNDARIES;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMBDFWD_boundaries_min_separation = DEFAULT_DSDBR01_SMBDFWD_BOUNDARIES_MIN_SEPARATION;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBDFWD_BOUNDARIES_MIN_SEPARATION;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

    _DSDBR01_SMBDFWD_min_d2Pr_dI2_zero_cross_jump_size = DEFAULT_DSDBR01_SMBDFWD_MIN_D2PR_DI2_ZERO_CROSS_JUMP_SIZE;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBDFWD_MIN_D2PR_DI2_ZERO_CROSS_JUMP_SIZE;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

    _DSDBR01_SMBDFWD_max_sum_of_squared_distances_from_Pr_I_line = DEFAULT_DSDBR01_SMBDFWD_MAX_SUM_SQUARED_DIST_FROM_PR_I_LINE;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBDFWD_MAX_SUM_SQUARED_DIST_FROM_PR_I_LINE;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

    _DSDBR01_SMBDFWD_min_slope_change_of_Pr_I_lines = DEFAULT_DSDBR01_SMBDFWD_MIN_SLOPE_CHANGE_OF_PR_I_LINE;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBDFWD_MIN_SLOPE_CHANGE_OF_PR_I_LINE;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMBDFWD_linking_max_distance_between_linked_jumps = DEFAULT_DSDBR01_SMBDFWD_LINKING_MAX_DISTANCE_BETWEEN_LINKED_JUMPS;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBDFWD_LINKING_MAX_DISTANCE_BETWEEN_LINKED_JUMPS;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMBDFWD_linking_min_sharpness =  DEFAULT_DSDBR01_SMBDFWD_LINKING_MAX_SHARPNESS;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBDFWD_LINKING_MAX_SHARPNESS;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMBDFWD_linking_max_sharpness = DEFAULT_DSDBR01_SMBDFWD_LINKING_MIN_SHARPNESS;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBDFWD_LINKING_MIN_SHARPNESS;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMBDFWD_linking_max_gap_between_double_lines = DEFAULT_DSDBR01_SMBDFWD_LINKING_MAX_GAP_BETWEEN_DOUBLE_LINES;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBDFWD_LINKING_MAX_GAP_BETWEEN_DOUBLE_LINES;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMBDFWD_linking_min_gap_between_double_lines = DEFAULT_DSDBR01_SMBDFWD_LINKING_MIN_GAP_BETWEEN_DOUBLE_LINES;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBDFWD_LINKING_MIN_GAP_BETWEEN_DOUBLE_LINES;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMBDFWD_linking_near_bottom_row = DEFAULT_DSDBR01_SMBDFWD_LINKING_NEAR_BOTTOM;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBDFWD_LINKING_NEAR_BOTTOM;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMBDFWD_linking_sharpness_near_bottom_row = DEFAULT_DSDBR01_SMBDFWD_LINKING_SHARPNESS_NEAR_BOTTOM;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBDFWD_LINKING_SHARPNESS_NEAR_BOTTOM;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMBDFWD_linking_max_distance_near_bottom_row = DEFAULT_DSDBR01_SMBDFWD_LINKING_MAX_DISTANCE_BETWEEN_LINKS_NEAR_BOTTOM;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBDFWD_LINKING_MAX_DISTANCE_BETWEEN_LINKS_NEAR_BOTTOM;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMBDFWD_linking_max_distance_to_single_unlinked_jump = DEFAULT_DSDBR01_SMBDFWD_LINKING_MAX_DISTANCE_TO_LINK_SINGLE_UNLINKED_JUMP;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBDFWD_LINKING_MAX_DISTANCE_TO_LINK_SINGLE_UNLINKED_JUMP;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMBDFWD_linking_max_y_distance_to_link = DEFAULT_DSDBR01_SMBDFWD_LINKING_MAX_Y_DISTANCE_TO_LINK;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBDFWD_LINKING_MAX_Y_DISTANCE_TO_LINK;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMBDFWD_linking_sharpness_ramp_split = DEFAULT_DSDBR01_SMBDFWD_LINKING_SHARPNESS_RAMP_SPLIT;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBDFWD_LINKING_SHARPNESS_RAMP_SPLIT;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

    _DSDBR01_SMBDREV_I_ramp_boundaries_max = DEFAULT_DSDBR01_SMBDREV_I_RAMP_BOUNDARIES;
	reg_entry.reg_key_name = CG_DSDBR01_BASE_REGKEY + CString(RELATIVE_REGKEY_DSDBR01_SMBDREV);
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBDREV_I_RAMP_BOUNDARIES;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMBDREV_boundaries_min_separation = DEFAULT_DSDBR01_SMBDREV_BOUNDARIES_MIN_SEPARATION;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBDREV_BOUNDARIES_MIN_SEPARATION;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

    _DSDBR01_SMBDREV_min_d2Pr_dI2_zero_cross_jump_size = DEFAULT_DSDBR01_SMBDREV_MIN_D2PR_DI2_ZERO_CROSS_JUMP_SIZE;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBDREV_MIN_D2PR_DI2_ZERO_CROSS_JUMP_SIZE;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

    _DSDBR01_SMBDREV_max_sum_of_squared_distances_from_Pr_I_line = DEFAULT_DSDBR01_SMBDREV_MAX_SUM_SQUARED_DIST_FROM_PR_I_LINE;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBDREV_MAX_SUM_SQUARED_DIST_FROM_PR_I_LINE;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

    _DSDBR01_SMBDREV_min_slope_change_of_Pr_I_lines = DEFAULT_DSDBR01_SMBDREV_MIN_SLOPE_CHANGE_OF_PR_I_LINE;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBDREV_MIN_SLOPE_CHANGE_OF_PR_I_LINE;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMBDREV_linking_max_distance_between_linked_jumps = DEFAULT_DSDBR01_SMBDREV_LINKING_MAX_DISTANCE_BETWEEN_LINKED_JUMPS;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBDREV_LINKING_MAX_DISTANCE_BETWEEN_LINKED_JUMPS;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMBDREV_linking_min_sharpness =  DEFAULT_DSDBR01_SMBDREV_LINKING_MAX_SHARPNESS;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBDREV_LINKING_MAX_SHARPNESS;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMBDREV_linking_max_sharpness = DEFAULT_DSDBR01_SMBDREV_LINKING_MIN_SHARPNESS;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBDREV_LINKING_MIN_SHARPNESS;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMBDREV_linking_max_gap_between_double_lines = DEFAULT_DSDBR01_SMBDREV_LINKING_MAX_GAP_BETWEEN_DOUBLE_LINES;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBDREV_LINKING_MAX_GAP_BETWEEN_DOUBLE_LINES;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMBDREV_linking_min_gap_between_double_lines = DEFAULT_DSDBR01_SMBDREV_LINKING_MIN_GAP_BETWEEN_DOUBLE_LINES;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBDREV_LINKING_MIN_GAP_BETWEEN_DOUBLE_LINES;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMBDREV_linking_near_bottom_row = DEFAULT_DSDBR01_SMBDREV_LINKING_NEAR_BOTTOM;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBDREV_LINKING_NEAR_BOTTOM;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMBDREV_linking_sharpness_near_bottom_row = DEFAULT_DSDBR01_SMBDREV_LINKING_SHARPNESS_NEAR_BOTTOM;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBDREV_LINKING_SHARPNESS_NEAR_BOTTOM;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMBDREV_linking_max_distance_near_bottom_row = DEFAULT_DSDBR01_SMBDREV_LINKING_MAX_DISTANCE_BETWEEN_LINKS_NEAR_BOTTOM;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBDREV_LINKING_MAX_DISTANCE_BETWEEN_LINKS_NEAR_BOTTOM;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMBDREV_linking_max_distance_to_single_unlinked_jump = DEFAULT_DSDBR01_SMBDREV_LINKING_MAX_DISTANCE_TO_LINK_SINGLE_UNLINKED_JUMP;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBDREV_LINKING_MAX_DISTANCE_TO_LINK_SINGLE_UNLINKED_JUMP;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMBDREV_linking_max_y_distance_to_link = DEFAULT_DSDBR01_SMBDREV_LINKING_MAX_Y_DISTANCE_TO_LINK;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBDREV_LINKING_MAX_Y_DISTANCE_TO_LINK;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMBDREV_linking_sharpness_ramp_split = DEFAULT_DSDBR01_SMBDREV_LINKING_SHARPNESS_RAMP_SPLIT;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMBDREV_LINKING_SHARPNESS_RAMP_SPLIT;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);


    _DSDBR01_OMQA_slope_window_size			= DEFAULT_DSDBR01_OMQA_SLOPE_WINDOW_SIZE;
	reg_entry.reg_key_name = CG_DSDBR01_BASE_REGKEY + CString(RELATIVE_REGKEY_DSDBR01_OMQA);
	reg_entry.reg_entry_name = REGNAME_DSDBR01_OMQA_SLOPE_WINDOW_SIZE;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

    _DSDBR01_OMQA_modal_distortion_min_x	= DEFAULT_DSDBR01_OMQA_MODAL_DISTORTION_MIN_X;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_OMQA_MODAL_DISTORTION_MIN_X;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

    _DSDBR01_OMQA_modal_distortion_max_x	= DEFAULT_DSDBR01_OMQA_MODAL_DISTORTION_MAX_X;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_OMQA_MODAL_DISTORTION_MAX_X;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

    _DSDBR01_OMQA_modal_distortion_min_y	= DEFAULT_DSDBR01_OMQA_MODAL_DISTORTION_MIN_Y;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_OMQA_MODAL_DISTORTION_MIN_Y;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);


	_DSDBR01_OMPF_max_mode_borders_removed_threshold		= DEFAULT_DSDBR01_OMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD;
	reg_entry.reg_key_name = CG_DSDBR01_BASE_REGKEY + CString(RELATIVE_REGKEY_DSDBR01_OMPF);
	reg_entry.reg_entry_name = REGNAME_DSDBR01_OMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_OMPF_max_middle_line_rms_value					= DEFAULT_DSDBR01_OMPF_MAX_MIDDLE_LINE_RMS_VALUE;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_OMPF_MAX_MIDDLE_LINE_RMS_VALUE;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_OMPF_min_middle_line_slope						= DEFAULT_DSDBR01_OMPF_MIN_MIDDLE_LINE_SLOPE;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_OMPF_MIN_MIDDLE_LINE_SLOPE;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_OMPF_max_middle_line_slope						= DEFAULT_DSDBR01_OMPF_MAX_MIDDLE_LINE_SLOPE;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_OMPF_MAX_MIDDLE_LINE_SLOPE;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_OMPF_max_pr_gap								= DEFAULT_DSDBR01_OMPF_MAX_PR_GAP;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_OMPF_MAX_PR_GAP;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_OMPF_max_lower_pr								= DEFAULT_DSDBR01_OMPF_MAX_LOWER_PR;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_OMPF_MAX_LOWER_PR;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_OMPF_min_upper_pr								= DEFAULT_DSDBR01_OMPF_MIN_UPPER_PR;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_OMPF_MIN_UPPER_PR;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_OMPF_min_mode_width_threshold					= DEFAULT_DSDBR01_OMPF_MIN_MODE_WIDTH_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_OMPF_MIN_MODE_WIDTH_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_OMPF_max_mode_width_threshold					= DEFAULT_DSDBR01_OMPF_MAX_MODE_WIDTH_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_OMPF_MAX_MODE_WIDTH_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_OMPF_max_mode_bdc_area_threshold				= DEFAULT_DSDBR01_OMPF_MAX_MODE_BDC_AREA_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_OMPF_MAX_MODE_BDC_AREA_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_OMPF_max_mode_bdc_area_x_length_threshold		= DEFAULT_DSDBR01_OMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_OMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_OMPF_sum_mode_bdc_areas_threshold				= DEFAULT_DSDBR01_OMPF_SUM_MODE_BDC_AREAS_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_OMPF_SUM_MODE_BDC_AREAS_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_OMPF_num_mode_bdc_areas_threshold				= DEFAULT_DSDBR01_OMPF_NUM_MODE_BDC_AREAS_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_OMPF_NUM_MODE_BDC_AREAS_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_OMPF_modal_distortion_angle_threshold			= DEFAULT_DSDBR01_OMPF_MODAL_DISTORTION_ANGLE_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_OMPF_MODAL_DISTORTION_ANGLE_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_OMPF_modal_distortion_angle_x_pos_threshold	= DEFAULT_DSDBR01_OMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_OMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_OMPF_max_mode_line_slope_threshold				= DEFAULT_DSDBR01_OMPF_MAX_MODE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_OMPF_MAX_MODE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_OMPF_min_mode_line_slope_threshold				= DEFAULT_DSDBR01_OMPF_MIN_MODE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_OMPF_MIN_MODE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);


    _DSDBR01_SMQA_slope_window_size			= DEFAULT_DSDBR01_SMQA_SLOPE_WINDOW_SIZE;
	reg_entry.reg_key_name = CG_DSDBR01_BASE_REGKEY + CString(RELATIVE_REGKEY_DSDBR01_SMQA);
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMQA_SLOPE_WINDOW_SIZE;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

    _DSDBR01_SMQA_modal_distortion_min_x	= DEFAULT_DSDBR01_SMQA_MODAL_DISTORTION_MIN_X;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMQA_MODAL_DISTORTION_MIN_X;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

    _DSDBR01_SMQA_modal_distortion_max_x	= DEFAULT_DSDBR01_SMQA_MODAL_DISTORTION_MAX_X;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMQA_MODAL_DISTORTION_MAX_X;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

    _DSDBR01_SMQA_modal_distortion_min_y	= DEFAULT_DSDBR01_SMQA_MODAL_DISTORTION_MIN_Y;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMQA_MODAL_DISTORTION_MIN_Y;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMQA_ignore_DBC_at_FSC			= DEFAULT_DSDBR01_SMQA_IGNORE_BDC_AT_FSC;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMQA_IGNORE_BDC_AT_FSC;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_bool;
	_reg_entries.push_back(reg_entry);

	//GDM Oct/Nov 06 added reg entries for Mode width and hysteresis analysis window
	//mode width 
	   _DSDBR01_SMQA_mode_width_analysis_min_x	= DEFAULT_DSDBR01_SMQA_MODE_WIDTH_ANALYSIS_MIN_X;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMQA_MODE_WIDTH_ANALYSIS_MIN_X;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

    _DSDBR01_SMQA_mode_width_analysis_max_x	= DEFAULT_DSDBR01_SMQA_MODE_WIDTH_ANALYSIS_MAX_X;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMQA_MODE_WIDTH_ANALYSIS_MAX_X;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);
	// hysteresis analysis
	   _DSDBR01_SMQA_hysteresis_analysis_min_x	= DEFAULT_DSDBR01_SMQA_HYSTERESIS_ANALYSIS_MIN_X;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMQA_HYSTERESIS_ANALYSIS_MIN_X;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

    _DSDBR01_SMQA_hysteresis_analysis_max_x	= DEFAULT_DSDBR01_SMQA_HYSTERESIS_ANALYSIS_MAX_X;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMQA_HYSTERESIS_ANALYSIS_MAX_X;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	//gdm




	_DSDBR01_SMPF_ALL_max_mode_width = DEFAULT_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD;
	reg_entry.reg_key_name = CG_DSDBR01_BASE_REGKEY + CString(RELATIVE_REGKEY_DSDBR01_SMPF_ALL);
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_ALL_min_mode_width = DEFAULT_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_ALL_max_mode_borders_removed = DEFAULT_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_ALL_max_middle_line_rms_value = DEFAULT_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_ALL_min_middle_line_slope = DEFAULT_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_ALL_max_middle_line_slope = DEFAULT_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_ALL_mean_perc_working_region = DEFAULT_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_ALL_min_perc_working_region = DEFAULT_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_ALL_modal_distortion_angle = DEFAULT_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_ALL_modal_distortion_angle_x_pos = DEFAULT_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_ALL_max_mode_bdc_area = DEFAULT_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_ALL_max_mode_bdc_area_x_length = DEFAULT_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_ALL_sum_mode_bdc_areas = DEFAULT_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_ALL_num_mode_bdc_areas = DEFAULT_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_ALL_max_mode_line_slope = DEFAULT_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);














	_DSDBR01_SMPF_SM0_max_mode_width = DEFAULT_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD;
	reg_entry.reg_key_name = CG_DSDBR01_BASE_REGKEY + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM0);
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM0_min_mode_width = DEFAULT_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM0_max_mode_borders_removed = DEFAULT_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM0_max_middle_line_rms_value = DEFAULT_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM0_min_middle_line_slope = DEFAULT_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM0_max_middle_line_slope = DEFAULT_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM0_mean_perc_working_region = DEFAULT_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM0_min_perc_working_region = DEFAULT_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM0_modal_distortion_angle = DEFAULT_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM0_modal_distortion_angle_x_pos = DEFAULT_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM0_max_mode_bdc_area = DEFAULT_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM0_max_mode_bdc_area_x_length = DEFAULT_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM0_sum_mode_bdc_areas = DEFAULT_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM0_num_mode_bdc_areas = DEFAULT_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM0_max_mode_line_slope = DEFAULT_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);













	_DSDBR01_SMPF_SM1_max_mode_width = DEFAULT_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD;
	reg_entry.reg_key_name = CG_DSDBR01_BASE_REGKEY + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM1);
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM1_min_mode_width = DEFAULT_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM1_max_mode_borders_removed = DEFAULT_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM1_max_middle_line_rms_value = DEFAULT_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM1_min_middle_line_slope = DEFAULT_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM1_max_middle_line_slope = DEFAULT_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM1_mean_perc_working_region = DEFAULT_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM1_min_perc_working_region = DEFAULT_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM1_modal_distortion_angle = DEFAULT_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM1_modal_distortion_angle_x_pos = DEFAULT_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM1_max_mode_bdc_area = DEFAULT_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM1_max_mode_bdc_area_x_length = DEFAULT_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM1_sum_mode_bdc_areas = DEFAULT_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM1_num_mode_bdc_areas = DEFAULT_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM1_max_mode_line_slope = DEFAULT_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);












	_DSDBR01_SMPF_SM2_max_mode_width = DEFAULT_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD;
	reg_entry.reg_key_name = CG_DSDBR01_BASE_REGKEY + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM2);
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM2_min_mode_width = DEFAULT_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM2_max_mode_borders_removed = DEFAULT_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM2_max_middle_line_rms_value = DEFAULT_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM2_min_middle_line_slope = DEFAULT_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM2_max_middle_line_slope = DEFAULT_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM2_mean_perc_working_region = DEFAULT_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM2_min_perc_working_region = DEFAULT_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM2_modal_distortion_angle = DEFAULT_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM2_modal_distortion_angle_x_pos = DEFAULT_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM2_max_mode_bdc_area = DEFAULT_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM2_max_mode_bdc_area_x_length = DEFAULT_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM2_sum_mode_bdc_areas = DEFAULT_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM2_num_mode_bdc_areas = DEFAULT_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM2_max_mode_line_slope = DEFAULT_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);












	_DSDBR01_SMPF_SM3_max_mode_width = DEFAULT_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD;
	reg_entry.reg_key_name = CG_DSDBR01_BASE_REGKEY + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM3);
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM3_min_mode_width = DEFAULT_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM3_max_mode_borders_removed = DEFAULT_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM3_max_middle_line_rms_value = DEFAULT_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM3_min_middle_line_slope = DEFAULT_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM3_max_middle_line_slope = DEFAULT_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM3_mean_perc_working_region = DEFAULT_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM3_min_perc_working_region = DEFAULT_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM3_modal_distortion_angle = DEFAULT_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM3_modal_distortion_angle_x_pos = DEFAULT_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM3_max_mode_bdc_area = DEFAULT_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM3_max_mode_bdc_area_x_length = DEFAULT_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM3_sum_mode_bdc_areas = DEFAULT_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM3_num_mode_bdc_areas = DEFAULT_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM3_max_mode_line_slope = DEFAULT_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);












	_DSDBR01_SMPF_SM4_max_mode_width = DEFAULT_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD;
	reg_entry.reg_key_name = CG_DSDBR01_BASE_REGKEY + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM4);
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM4_min_mode_width = DEFAULT_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM4_max_mode_borders_removed = DEFAULT_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM4_max_middle_line_rms_value = DEFAULT_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM4_min_middle_line_slope = DEFAULT_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM4_max_middle_line_slope = DEFAULT_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM4_mean_perc_working_region = DEFAULT_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM4_min_perc_working_region = DEFAULT_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM4_modal_distortion_angle = DEFAULT_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM4_modal_distortion_angle_x_pos = DEFAULT_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM4_max_mode_bdc_area = DEFAULT_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM4_max_mode_bdc_area_x_length = DEFAULT_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM4_sum_mode_bdc_areas = DEFAULT_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM4_num_mode_bdc_areas = DEFAULT_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM4_max_mode_line_slope = DEFAULT_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);












	_DSDBR01_SMPF_SM5_max_mode_width = DEFAULT_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD;
	reg_entry.reg_key_name = CG_DSDBR01_BASE_REGKEY + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM5);
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM5_min_mode_width = DEFAULT_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM5_max_mode_borders_removed = DEFAULT_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM5_max_middle_line_rms_value = DEFAULT_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM5_min_middle_line_slope = DEFAULT_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM5_max_middle_line_slope = DEFAULT_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM5_mean_perc_working_region = DEFAULT_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM5_min_perc_working_region = DEFAULT_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM5_modal_distortion_angle = DEFAULT_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM5_modal_distortion_angle_x_pos = DEFAULT_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM5_max_mode_bdc_area = DEFAULT_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM5_max_mode_bdc_area_x_length = DEFAULT_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM5_sum_mode_bdc_areas = DEFAULT_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM5_num_mode_bdc_areas = DEFAULT_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM5_max_mode_line_slope = DEFAULT_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);












	_DSDBR01_SMPF_SM6_max_mode_width = DEFAULT_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD;
	reg_entry.reg_key_name = CG_DSDBR01_BASE_REGKEY + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM6);
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM6_min_mode_width = DEFAULT_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM6_max_mode_borders_removed = DEFAULT_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM6_max_middle_line_rms_value = DEFAULT_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM6_min_middle_line_slope = DEFAULT_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM6_max_middle_line_slope = DEFAULT_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM6_mean_perc_working_region = DEFAULT_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM6_min_perc_working_region = DEFAULT_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM6_modal_distortion_angle = DEFAULT_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM6_modal_distortion_angle_x_pos = DEFAULT_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM6_max_mode_bdc_area = DEFAULT_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM6_max_mode_bdc_area_x_length = DEFAULT_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM6_sum_mode_bdc_areas = DEFAULT_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM6_num_mode_bdc_areas = DEFAULT_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM6_max_mode_line_slope = DEFAULT_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);












	_DSDBR01_SMPF_SM7_max_mode_width = DEFAULT_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD;
	reg_entry.reg_key_name = CG_DSDBR01_BASE_REGKEY + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM7);
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM7_min_mode_width = DEFAULT_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM7_max_mode_borders_removed = DEFAULT_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM7_max_middle_line_rms_value = DEFAULT_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM7_min_middle_line_slope = DEFAULT_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM7_max_middle_line_slope = DEFAULT_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM7_mean_perc_working_region = DEFAULT_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM7_min_perc_working_region = DEFAULT_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM7_modal_distortion_angle = DEFAULT_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM7_modal_distortion_angle_x_pos = DEFAULT_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM7_max_mode_bdc_area = DEFAULT_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM7_max_mode_bdc_area_x_length = DEFAULT_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM7_sum_mode_bdc_areas = DEFAULT_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM7_num_mode_bdc_areas = DEFAULT_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM7_max_mode_line_slope = DEFAULT_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);












	_DSDBR01_SMPF_SM8_max_mode_width = DEFAULT_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD;
	reg_entry.reg_key_name = CG_DSDBR01_BASE_REGKEY + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM8);
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM8_min_mode_width = DEFAULT_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM8_max_mode_borders_removed = DEFAULT_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM8_max_middle_line_rms_value = DEFAULT_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM8_min_middle_line_slope = DEFAULT_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM8_max_middle_line_slope = DEFAULT_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM8_mean_perc_working_region = DEFAULT_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM8_min_perc_working_region = DEFAULT_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM8_modal_distortion_angle = DEFAULT_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM8_modal_distortion_angle_x_pos = DEFAULT_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM8_max_mode_bdc_area = DEFAULT_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM8_max_mode_bdc_area_x_length = DEFAULT_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM8_sum_mode_bdc_areas = DEFAULT_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM8_num_mode_bdc_areas = DEFAULT_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM8_max_mode_line_slope = DEFAULT_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);












	_DSDBR01_SMPF_SM9_max_mode_width = DEFAULT_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD;
	reg_entry.reg_key_name = CG_DSDBR01_BASE_REGKEY + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM9);
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM9_min_mode_width = DEFAULT_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM9_max_mode_borders_removed = DEFAULT_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM9_max_middle_line_rms_value = DEFAULT_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM9_min_middle_line_slope = DEFAULT_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM9_max_middle_line_slope = DEFAULT_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM9_mean_perc_working_region = DEFAULT_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM9_min_perc_working_region = DEFAULT_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM9_modal_distortion_angle = DEFAULT_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM9_modal_distortion_angle_x_pos = DEFAULT_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM9_max_mode_bdc_area = DEFAULT_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM9_max_mode_bdc_area_x_length = DEFAULT_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM9_sum_mode_bdc_areas = DEFAULT_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM9_num_mode_bdc_areas = DEFAULT_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_SMPF_SM9_max_mode_line_slope = DEFAULT_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);




	_DSDBR01_CFREQ_poly_coeffs_abspath = DEFAULT_DSDBR01_CFREQ_POLY_COEFFS_ABSPATH;
	reg_entry.reg_key_name = CG_DSDBR01_BASE_REGKEY + CString(RELATIVE_REGKEY_DSDBR01_CFREQ);
	reg_entry.reg_entry_name = REGNAME_DSDBR01_CFREQ_POLY_COEFFS_ABSPATH;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_CString;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_CFREQ_num_freq_meas_per_op = DEFAULT_DSDBR01_CFREQ_NUM_FREQ_MEAS_PER_OP;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_CFREQ_NUM_FREQ_MEAS_PER_OP;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_CFREQ_num_power_ratio_meas_per_op = DEFAULT_DSDBR01_CFREQ_POWER_RATIO_MEAS_PER_OP;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_CFREQ_POWER_RATIO_MEAS_PER_OP;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_CFREQ_max_stable_freq_error = DEFAULT_DSDBR01_CFREQ_MAX_STABLE_FREQ_ERROR;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_CFREQ_MAX_STABLE_FREQ_ERROR;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_CFREQ_sample_point_settle_time = DEFAULT_DSDBR01_CFREQ_SAMPLE_POINT_SETTLE_TIME;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_CFREQ_SAMPLE_POINT_SETTLE_TIME;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_CFREQ_write_sample_points_to_file = DEFAULT_DSDBR01_CFREQ_WRITE_SAMPLE_POINTS_TO_FILE;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_CFREQ_WRITE_SAMPLE_POINTS_TO_FILE;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_bool;
	_reg_entries.push_back(reg_entry);



	_DSDBR01_ITUGRID_centre_freq = DEFAULT_DSDBR01_ITUGRID_CENTRE_FREQ;
	reg_entry.reg_key_name = CG_DSDBR01_BASE_REGKEY + CString(RELATIVE_REGKEY_DSDBR01_ITUGRID);
	reg_entry.reg_entry_name = REGNAME_DSDBR01_ITUGRID_CENTRE_FREQ;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_ITUGRID_step_freq = DEFAULT_DSDBR01_ITUGRID_STEP_FREQ;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_ITUGRID_STEP_FREQ;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_ITUGRID_channel_count = DEFAULT_DSDBR01_ITUGRID_CHANNEL_COUNT;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_ITUGRID_CHANNEL_COUNT;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_ITUGRID_abspath = DEFAULT_DSDBR01_ITUGRID_abspath;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_ITUGRID_abspath;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_CString;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_ESTIMATESITUGRID_abspath = DEFAULT_DSDBR01_ESTIMATESITUGRID_abspath;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_ESTIMATESITUGRID_abspath;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_CString;
	_reg_entries.push_back(reg_entry);


	_DSDBR01_CHAR_num_freq_meas_per_op = DEFAULT_DSDBR01_CHAR_NUM_FREQ_MEAS_PER_OP;
	reg_entry.reg_key_name = CG_DSDBR01_BASE_REGKEY + CString(RELATIVE_REGKEY_DSDBR01_CHAR);
	reg_entry.reg_entry_name = REGNAME_DSDBR01_CHAR_NUM_FREQ_MEAS_PER_OP;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_CHAR_freq_accuracy_of_wavemeter = DEFAULT_DSDBR01_CHAR_FREQ_ACCURACY_OF_WAVEMETER;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_CHAR_FREQ_ACCURACY_OF_WAVEMETER;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_CHAR_op_settle_time = DEFAULT_DSDBR01_CHAR_OP_SETTLE_TIME;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_CHAR_OP_SETTLE_TIME;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_CHAR_itusearch_scheme = DEFAULT_DSDBR01_CHAR_itusearch_scheme;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_CHAR_itusearch_scheme;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_CHAR_itusearch_nextguess = DEFAULT_DSDBR01_CHAR_itusearch_nextguess;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_CHAR_itusearch_nextguess;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_CHAR_itusearch_resamples = DEFAULT_DSDBR01_CHAR_itusearch_resamples;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_CHAR_itusearch_resamples;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_CHAR_num_power_ratio_meas_per_op = DEFAULT_DSDBR01_CHAR_POWER_RATIO_MEAS_PER_OP;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_CHAR_POWER_RATIO_MEAS_PER_OP;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_CHAR_max_stable_freq_error = DEFAULT_DSDBR01_CHAR_MAX_STABLE_FREQ_ERROR;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_CHAR_MAX_STABLE_FREQ_ERROR;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_CHAR_write_duff_points_to_file = DEFAULT_DSDBR01_CHAR_WRITE_DUFF_POINTS_TO_FILE;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_CHAR_WRITE_DUFF_POINTS_TO_FILE;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_bool;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_CHAR_write_duff_points_to_file = DEFAULT_DSDBR01_CHAR_prevent_front_section_switch;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_CHAR_prevent_front_section_switch;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_bool;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_CHAR_fakefreq_Iramp_module_name = DEFAULT_DSDBR01_CHAR_fakefreq_Iramp_module_name;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_CHAR_fakefreq_Iramp_module_name;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_CString;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_CHAR_Iramp_vs_fakefreq_abspath = DEFAULT_DSDBR01_CHAR_Iramp_vs_fakefreq_abspath;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_CHAR_Iramp_vs_fakefreq_abspath;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_CString;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_CHAR_fakefreq_max_error = DEFAULT_DSDBR01_CHAR_fakefreq_max_error;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_CHAR_fakefreq_max_error;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);



	// DSDBR01 TEC settling parameters used during characterisation

	reg_entry.reg_key_name = CG_DSDBR01_BASE_REGKEY + CString(RELATIVE_REGKEY_DSDBR01_CHARTEC);

	_DSDBR01_CHARTEC_time_window_size = DEFAULT_DSDBR01_CHARTEC_time_window_size;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_CHARTEC_time_window_size;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_CHARTEC_max_temp_deviation = DEFAULT_DSDBR01_CHARTEC_max_temp_deviation;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_CHARTEC_max_temp_deviation;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_double;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_CHARTEC_num_temp_readings = DEFAULT_DSDBR01_CHARTEC_num_temp_readings;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_CHARTEC_num_temp_readings;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);

	_DSDBR01_CHARTEC_max_settle_time = DEFAULT_DSDBR01_CHARTEC_max_settle_time;
	reg_entry.reg_entry_name = REGNAME_DSDBR01_CHARTEC_max_settle_time;
	reg_entry.reg_entry_datatype = CCGRegEntry::datatype::_int;
	_reg_entries.push_back(reg_entry);
}

CCGRegValues::~CCGRegValues(void)
{
}

// Accessor to the one and only
// instance of this class
CCGRegValues*
CCGRegValues::instance()
{
	// If we're calling this for
	// the first time, create our
	// instance, otherwise disallow
	// any more being created
	if( _p_instance == NULL )
		static CCGRegValues _p_instance;
	return _p_instance;
}


CString
CCGRegValues::getText(CCGRegEntry reg_entry)
{
	CString text = reg_entry.reg_entry_name + _T(" = ");
	bool b = false;
	int i = 0;
	double d = 0;

	CString reg = reg_entry.reg_key_name + _T("\\") + reg_entry.reg_entry_name;


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString("\\")
		+ CString(REGNAME_DSDBR01_BASE_RESULTS_DIR) )
	{
		text += get_DSDBR01_BASE_results_directory();
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString("\\")
		+ CString(REGNAME_DSDBR01_BASE_STUB_HARDWARE_CALLS) )
	{
		b = get_DSDBR01_BASE_stub_hardware_calls();
		if(b) text += _T(REG_BOOL_TRUE_VALUE);
		else text += _T(REG_BOOL_FALSE_VALUE);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_CMDC_GAIN_MODULE_NAME)  )
	{
		text += get_DSDBR01_CMDC_gain_module_name();
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_CMDC_SOA_MODULE_NAME)  )
	{
		text += get_DSDBR01_CMDC_soa_module_name();
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_CMDC_REAR_MODULE_NAME)  )
	{
		text += get_DSDBR01_CMDC_rear_module_name();
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_CMDC_PHASE_MODULE_NAME)  )
	{
		text += get_DSDBR01_CMDC_phase_module_name();
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_CMDC_FRONT1_MODULE_NAME)  )
	{
		text += get_DSDBR01_CMDC_front1_module_name();
	}
	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_CMDC_FRONT2_MODULE_NAME)  )
	{
		text += get_DSDBR01_CMDC_front2_module_name();
	}
	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_CMDC_FRONT3_MODULE_NAME)  )
	{
		text += get_DSDBR01_CMDC_front3_module_name();
	}
	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_CMDC_FRONT4_MODULE_NAME)  )
	{
		text += get_DSDBR01_CMDC_front4_module_name();
	}
	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_CMDC_FRONT5_MODULE_NAME)  )
	{
		text += get_DSDBR01_CMDC_front5_module_name();
	}
	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_CMDC_FRONT6_MODULE_NAME)  )
	{
		text += get_DSDBR01_CMDC_front6_module_name();
	}
	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_CMDC_FRONT7_MODULE_NAME)  )
	{
		text += get_DSDBR01_CMDC_front7_module_name();
	}
	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_CMDC_FRONT8_MODULE_NAME)  )
	{
		text += get_DSDBR01_CMDC_front8_module_name();
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_CMDC_306II_MODULE_NAME)  )
	{
		text += get_DSDBR01_CMDC_306II_module_name();
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_CMDC_306II_DIRECT_CHANNEL)  )
	{
		text.AppendFormat("%d",get_DSDBR01_CMDC_306II_direct_channel());
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_CMDC_306II_FILTERED_CHANNEL)  )
	{
		text.AppendFormat("%d",get_DSDBR01_CMDC_306II_filtered_channel());
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_CMDC_306EE_MODULE_NAME)  )
	{
		text += get_DSDBR01_CMDC_306EE_module_name();
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_CMDC_306EE_PHOTODIODE1_CHANNEL)  )
	{
		text.AppendFormat("%d",get_DSDBR01_CMDC_306EE_photodiode1_channel());
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_CMDC_306EE_PHOTODIODE2_CHANNEL)  )
	{
		text.AppendFormat("%d",get_DSDBR01_CMDC_306EE_photodiode2_channel());
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_CMDC_WAVEMETER_RETRIES)  )
	{
		text.AppendFormat("%d",get_DSDBR01_CMDC_wavemeter_retries());
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMDC_RAMP_DIRECTION)  )
	{
		text += get_DSDBR01_OMDC_ramp_direction();
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMDC_FORWARD_AND_REVERSE) )
	{
		b = get_DSDBR01_OMDC_forward_and_reverse();
		if(b) text += _T(REG_BOOL_TRUE_VALUE);
		else text += _T(REG_BOOL_FALSE_VALUE);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMDC_MEASURE_PHOTODIODE_CURRENTS) )
	{
		b = get_DSDBR01_OMDC_measure_photodiode_currents();
		if(b) text += _T(REG_BOOL_TRUE_VALUE);
		else text += _T(REG_BOOL_FALSE_VALUE);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMDC_WRITE_FILTERED_POWER_MAPS) )
	{
		b = get_DSDBR01_OMDC_write_filtered_power_maps();
		if(b) text += _T(REG_BOOL_TRUE_VALUE);
		else text += _T(REG_BOOL_FALSE_VALUE);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMDC_REAR_CURRENTS_ABSPATH)  )
	{
		text += get_DSDBR01_OMDC_rear_currents_abspath();
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMDC_FRONT_CURRENTS_ABSPATH)  )
	{
		text += get_DSDBR01_OMDC_front_currents_abspath();
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMDC_SOURCE_DELAY)  )
	{
		text.AppendFormat("%d",get_DSDBR01_OMDC_source_delay());
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMDC_MEASURE_DELAY)  )
	{
		text.AppendFormat("%d",get_DSDBR01_OMDC_measure_delay());
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMDC_OVERWRITE_EXISTING_FILES) )
	{
		b = get_DSDBR01_OMDC_overwrite_existing_files();
		if(b) text += _T(REG_BOOL_TRUE_VALUE);
		else text += _T(REG_BOOL_FALSE_VALUE);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMBD_READ_BOUNDARIES_FROM_FILE) )
	{
		b = get_DSDBR01_OMBD_read_boundaries_from_file();
		if(b) text += _T(REG_BOOL_TRUE_VALUE);
		else text += _T(REG_BOOL_FALSE_VALUE);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMBD_WRITE_BOUNDARIES_TO_FILE) )
	{
		b = get_DSDBR01_OMBD_write_boundaries_to_file();
		if(b) text += _T(REG_BOOL_TRUE_VALUE);
		else text += _T(REG_BOOL_FALSE_VALUE);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMBD_MEDIAN_FILTER_RANK) )
	{
		i = get_DSDBR01_OMBD_median_filter_rank();
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMBD_MAX_DELTAPR_IN_SM) )
	{
		d = get_DSDBR01_OMBD_max_deltaPr_in_sm();
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMBD_MIN_POINTS_WIDTH_OF_SM) )
	{
		i = get_DSDBR01_OMBD_min_points_width_of_sm();
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMBD_ML_MOVING_AVERAGE_FILTER_N) )
	{
		i = get_DSDBR01_OMBD_ml_moving_ave_filter_n();
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMBD_ML_MIN_LENGTH) )
	{
		i = get_DSDBR01_OMBD_ml_min_length();
		text.AppendFormat("%d",i);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMBD_ML_EXTEND_TO_CORNER) )
	{
		b = get_DSDBR01_OMBD_ml_extend_to_corner();
		if(b) text += _T(REG_BOOL_TRUE_VALUE);
		else text += _T(REG_BOOL_FALSE_VALUE);
	}



	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMQA) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMQA_SLOPE_WINDOW_SIZE) )
	{
		i = get_DSDBR01_OMQA_slope_window_size();
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMQA) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMQA_MODAL_DISTORTION_MIN_X) )
	{
		d = get_DSDBR01_OMQA_modal_distortion_min_x();
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMQA) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMQA_MODAL_DISTORTION_MAX_X) )
	{
		d = get_DSDBR01_OMQA_modal_distortion_max_x();
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMQA) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMQA_MODAL_DISTORTION_MIN_Y) )
	{
		d = get_DSDBR01_OMQA_modal_distortion_min_y();
		text.AppendFormat("%g",d);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMPF) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD) )
	{
		i = get_DSDBR01_OMPF_max_mode_borders_removed_threshold();
		text.AppendFormat("%d",i);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMPF) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMPF_MAX_MIDDLE_LINE_RMS_VALUE) )
	{
		d = get_DSDBR01_OMPF_max_middle_line_rms_value();
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMPF) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMPF_MIN_MIDDLE_LINE_SLOPE) )
	{
		d = get_DSDBR01_OMPF_min_middle_line_slope();
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMPF) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMPF_MAX_MIDDLE_LINE_SLOPE) )
	{
		d = get_DSDBR01_OMPF_max_middle_line_slope();
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMPF) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMPF_MAX_PR_GAP) )
	{
		d = get_DSDBR01_OMPF_max_pr_gap();
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMPF) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMPF_MAX_LOWER_PR) )
	{
		d = get_DSDBR01_OMPF_max_lower_pr();
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMPF) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMPF_MIN_UPPER_PR) )
	{
		d = get_DSDBR01_OMPF_min_upper_pr();
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMPF) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMPF_MIN_MODE_WIDTH_THRESHOLD) )
	{
		i = get_DSDBR01_OMPF_min_mode_width_threshold();
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMPF) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMPF_MAX_MODE_WIDTH_THRESHOLD) )
	{
		i = get_DSDBR01_OMPF_max_mode_width_threshold();
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMPF) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMPF_MAX_MODE_BDC_AREA_THRESHOLD) )
	{
		d = get_DSDBR01_OMPF_max_mode_bdc_area_threshold();
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMPF) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD) )
	{
		d = get_DSDBR01_OMPF_max_mode_bdc_area_x_length_threshold();
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMPF) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMPF_SUM_MODE_BDC_AREAS_THRESHOLD) )
	{
		d = get_DSDBR01_OMPF_sum_mode_bdc_areas_threshold();
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMPF) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMPF_NUM_MODE_BDC_AREAS_THRESHOLD) )
	{
		d = get_DSDBR01_OMPF_num_mode_bdc_areas_threshold();
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMPF) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMPF_MODAL_DISTORTION_ANGLE_THRESHOLD) )
	{
		d = get_DSDBR01_OMPF_modal_distortion_angle_threshold();
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMPF) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD) )
	{
		d = get_DSDBR01_OMPF_modal_distortion_angle_x_pos_threshold();
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMPF) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMPF_MAX_MODE_LINE_SLOPE_THRESHOLD) )
	{
		d = get_DSDBR01_OMPF_max_mode_line_slope_threshold();
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMPF) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMPF_MIN_MODE_LINE_SLOPE_THRESHOLD) )
	{
		d = get_DSDBR01_OMPF_min_mode_line_slope_threshold();
		text.AppendFormat("%g",d);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CFREQ) + CString("\\")
		+ CString(REGNAME_DSDBR01_CFREQ_POLY_COEFFS_ABSPATH) )
	{
		text += get_DSDBR01_CFREQ_poly_coeffs_abspath();
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CFREQ) + CString("\\")
		+ CString(REGNAME_DSDBR01_CFREQ_NUM_FREQ_MEAS_PER_OP) )
	{
		i = get_DSDBR01_CFREQ_num_freq_meas_per_op();
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CFREQ) + CString("\\")
		+ CString(REGNAME_DSDBR01_CFREQ_POWER_RATIO_MEAS_PER_OP) )
	{
		i = get_DSDBR01_CFREQ_num_power_ratio_meas_per_op();
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CFREQ) + CString("\\")
		+ CString(REGNAME_DSDBR01_CFREQ_MAX_STABLE_FREQ_ERROR) )
	{
		d = get_DSDBR01_CFREQ_max_stable_freq_error();
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CFREQ) + CString("\\")
		+ CString(REGNAME_DSDBR01_CFREQ_SAMPLE_POINT_SETTLE_TIME) )
	{
		i = get_DSDBR01_CFREQ_sample_point_settle_time();
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CFREQ) + CString("\\")
		+ CString(REGNAME_DSDBR01_CFREQ_WRITE_SAMPLE_POINTS_TO_FILE) )
	{
		b = get_DSDBR01_CFREQ_write_sample_points_to_file();
		if(b) text += _T(REG_BOOL_TRUE_VALUE);
		else text += _T(REG_BOOL_FALSE_VALUE);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_ITUGRID) + CString("\\")
		+ CString(REGNAME_DSDBR01_ITUGRID_CENTRE_FREQ) )
	{
		d = get_DSDBR01_ITUGRID_centre_freq();
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_ITUGRID) + CString("\\")
		+ CString(REGNAME_DSDBR01_ITUGRID_STEP_FREQ) )
	{
		d = get_DSDBR01_ITUGRID_step_freq();
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_ITUGRID) + CString("\\")
		+ CString(REGNAME_DSDBR01_ITUGRID_CHANNEL_COUNT) )
	{
		i = get_DSDBR01_ITUGRID_channel_count();
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_ITUGRID) + CString("\\")
		+ CString(REGNAME_DSDBR01_ITUGRID_abspath) )
	{
		text += get_DSDBR01_ITUGRID_abspath();
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_ITUGRID) + CString("\\")
		+ CString(REGNAME_DSDBR01_ESTIMATESITUGRID_abspath) )
	{
		text += get_DSDBR01_ESTIMATESITUGRID_abspath();
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CHAR) + CString("\\")
		+ CString(REGNAME_DSDBR01_CHAR_NUM_FREQ_MEAS_PER_OP) )
	{
		i = get_DSDBR01_CHAR_num_freq_meas_per_op();
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CHAR) + CString("\\")
		+ CString(REGNAME_DSDBR01_CHAR_FREQ_ACCURACY_OF_WAVEMETER) )
	{
		d = get_DSDBR01_CHAR_freq_accuracy_of_wavemeter();
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CHAR) + CString("\\")
		+ CString(REGNAME_DSDBR01_CHAR_OP_SETTLE_TIME) )
	{
		i = get_DSDBR01_CHAR_op_settle_time();
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CHAR) + CString("\\")
		+ CString(REGNAME_DSDBR01_CHAR_itusearch_scheme) )
	{
		i = get_DSDBR01_CHAR_itusearch_scheme();
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CHAR) + CString("\\")
		+ CString(REGNAME_DSDBR01_CHAR_itusearch_nextguess) )
	{
		i = get_DSDBR01_CHAR_itusearch_nextguess();
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CHAR) + CString("\\")
		+ CString(REGNAME_DSDBR01_CHAR_itusearch_resamples) )
	{
		i = get_DSDBR01_CHAR_itusearch_resamples();
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CHAR) + CString("\\")
		+ CString(REGNAME_DSDBR01_CHAR_POWER_RATIO_MEAS_PER_OP) )
	{
		i = get_DSDBR01_CHAR_num_power_ratio_meas_per_op();
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CHAR) + CString("\\")
		+ CString(REGNAME_DSDBR01_CHAR_MAX_STABLE_FREQ_ERROR) )
	{
		d = get_DSDBR01_CHAR_max_stable_freq_error();
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CHAR) + CString("\\")
		+ CString(REGNAME_DSDBR01_CHAR_WRITE_DUFF_POINTS_TO_FILE) )
	{
		b = get_DSDBR01_CHAR_write_duff_points_to_file();
		if(b) text += _T(REG_BOOL_TRUE_VALUE);
		else text += _T(REG_BOOL_FALSE_VALUE);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CHAR) + CString("\\")
		+ CString(REGNAME_DSDBR01_CHAR_prevent_front_section_switch) )
	{
		b = get_DSDBR01_CHAR_prevent_front_section_switch();
		if(b) text += _T(REG_BOOL_TRUE_VALUE);
		else text += _T(REG_BOOL_FALSE_VALUE);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CHAR) + CString("\\")
		+ CString(REGNAME_DSDBR01_CHAR_fakefreq_Iramp_module_name) )
	{
		text += get_DSDBR01_CHAR_fakefreq_Iramp_module_name();
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CHAR) + CString("\\")
		+ CString(REGNAME_DSDBR01_CHAR_Iramp_vs_fakefreq_abspath) )
	{
		text += get_DSDBR01_CHAR_Iramp_vs_fakefreq_abspath();
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CHAR) + CString("\\")
		+ CString(REGNAME_DSDBR01_CHAR_fakefreq_max_error) )
	{
		d = get_DSDBR01_CHAR_fakefreq_max_error();
		text.AppendFormat("%g",d);
	}


	// DSDBR01 TEC settling parameters used during characterisation

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CHARTEC) + CString("\\")
		+ CString(REGNAME_DSDBR01_CHARTEC_time_window_size) )
	{
		i = get_DSDBR01_CHARTEC_time_window_size();
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CHARTEC) + CString("\\")
		+ CString(REGNAME_DSDBR01_CHARTEC_max_temp_deviation) )
	{
		d = get_DSDBR01_CHARTEC_max_temp_deviation();
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CHARTEC) + CString("\\")
		+ CString(REGNAME_DSDBR01_CHARTEC_num_temp_readings) )
	{
		i = get_DSDBR01_CHARTEC_num_temp_readings();
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CHARTEC) + CString("\\")
		+ CString(REGNAME_DSDBR01_CHARTEC_max_settle_time) )
	{
		i = get_DSDBR01_CHARTEC_max_settle_time();
		text.AppendFormat("%d",i);
	}



	getSMText( reg_entry, text );

	return text;
}


void
CCGRegValues::getSMText( CCGRegEntry reg_entry, CString &text )
{
	CString reg = reg_entry.reg_key_name + _T("\\") + reg_entry.reg_entry_name;

	bool b = false;
	int i = 0;
	double d = 0;

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMDC_RAMP_DIRECTION) )
	{
		text += get_DSDBR01_SMDC_ramp_direction();
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMDC_PHASE_CURRENTS_ABSPATH) )
	{
		text += get_DSDBR01_SMDC_phase_currents_abspath();
	}



	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMDC_MEASURE_PHOTODIODE_CURRENTS) )
	{
		b = get_DSDBR01_SMDC_measure_photodiode_currents();
		if(b) text += _T(REG_BOOL_TRUE_VALUE);
		else text += _T(REG_BOOL_FALSE_VALUE);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMDC_WRITE_FILTERED_POWER_MAPS) )
	{
		b = get_DSDBR01_SMDC_write_filtered_power_maps();
		if(b) text += _T(REG_BOOL_TRUE_VALUE);
		else text += _T(REG_BOOL_FALSE_VALUE);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMDC_SOURCE_DELAY) )
	{
		i = get_DSDBR01_SMDC_source_delay();
		text.AppendFormat("%d",i);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMDC_MEASURE_DELAY) )
	{
		i = get_DSDBR01_SMDC_measure_delay();
		text.AppendFormat("%d",i);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMDC_OVERWRITE_EXISTING_FILES) )
	{
		b = get_DSDBR01_SMDC_overwrite_existing_files();
		if(b) text += _T(REG_BOOL_TRUE_VALUE);
		else text += _T(REG_BOOL_FALSE_VALUE);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBD_READ_BOUNDARIES_FROM_FILE) )
	{
		b = get_DSDBR01_SMBD_read_boundaries_from_file();
		if(b) text += _T(REG_BOOL_TRUE_VALUE);
		else text += _T(REG_BOOL_FALSE_VALUE);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBD_WRITE_BOUNDARIES_TO_FILE) )
	{
		b = get_DSDBR01_SMBD_write_boundaries_to_file();
		if(b) text += _T(REG_BOOL_TRUE_VALUE);
		else text += _T(REG_BOOL_FALSE_VALUE);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBD_write_middle_of_upper_lines_to_file) )
	{
		b = get_DSDBR01_SMBD_write_middle_of_upper_lines_to_file();
		if(b) text += _T(REG_BOOL_TRUE_VALUE);
		else text += _T(REG_BOOL_FALSE_VALUE);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBD_write_middle_of_lower_lines_to_file) )
	{
		b = get_DSDBR01_SMBD_write_middle_of_lower_lines_to_file();
		if(b) text += _T(REG_BOOL_TRUE_VALUE);
		else text += _T(REG_BOOL_FALSE_VALUE);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBD_WRITE_DEBUG_FILES) )
	{
		b = get_DSDBR01_SMBD_write_debug_files();
		if(b) text += _T(REG_BOOL_TRUE_VALUE);
		else text += _T(REG_BOOL_FALSE_VALUE);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBD_extrapolate_to_map_edges) )
	{
		b = get_DSDBR01_SMBD_extrapolate_to_map_edges();
		if(b) text += _T(REG_BOOL_TRUE_VALUE);
		else text += _T(REG_BOOL_FALSE_VALUE);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBD_MEDIAN_FILTER_RANK) )
	{
		i = get_DSDBR01_SMBD_median_filter_rank();
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBD_MAX_DELTAPR_IN_LM) )
	{
		d = get_DSDBR01_SMBD_max_deltaPr_in_lm();
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBD_MIN_POINTS_WIDTH_OF_LM) )
	{
		i = get_DSDBR01_SMBD_min_points_width_of_lm();
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBD_MAX_POINTS_WIDTH_OF_LM) )
	{
		i = get_DSDBR01_SMBD_max_points_width_of_lm();
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBD_ML_MOVING_AVERAGE_FILTER_N) )
	{
		i = get_DSDBR01_SMBD_ml_moving_ave_filter_n();
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBD_ML_MIN_LENGTH) )
	{
		i = get_DSDBR01_SMBD_ml_min_length();
		text.AppendFormat("%d",i);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBD_VERTICAL_PERCENT_SHIFT) )
	{
		d = get_DSDBR01_SMBD_vertical_percent_shift();
		text.AppendFormat("%g",d);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBD_ZPRC_HYSTERESIS_UPPER_THRESHOLD) )
	{
		d = get_DSDBR01_SMBD_zprc_hysteresis_upper_threshold();
		text.AppendFormat("%g",d);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBD_ZPRC_HYSTERESIS_LOWER_THRESHOLD) )
	{
		d = get_DSDBR01_SMBD_zprc_hysteresis_lower_threshold();
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBD_ZPRC_MAX_HYSTERESIS_RISE) )
	{
		d = get_DSDBR01_SMBD_zprc_max_hysteresis_rise();
		text.AppendFormat("%g",d);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBD_ZPRC_MIN_POINTS_SEPARATION) )
	{
		i = get_DSDBR01_SMBD_zprc_min_points_separation();
		text.AppendFormat("%d",i);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBD_ZPRC_MAX_POINTS_TO_REVERSE_JUMP) )
	{
		i = get_DSDBR01_SMBD_zprc_max_points_to_reverse_jump();
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBD_ZPRC_MAX_HYSTERESIS_POINTS) )
	{
		i = get_DSDBR01_SMBD_zprc_max_hysteresis_points();
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBD_RAMP_ABSPATH) )
	{
		text += get_DSDBR01_SMBD_ramp_abspath();
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDFWD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDFWD_I_RAMP_BOUNDARIES) )
	{
		i = get_DSDBR01_SMBDFWD_I_ramp_boundaries_max();
		text.AppendFormat("%d",i);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDFWD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDFWD_BOUNDARIES_MIN_SEPARATION) )
	{
		i = get_DSDBR01_SMBDFWD_boundaries_min_separation();
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDFWD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDFWD_MIN_D2PR_DI2_ZERO_CROSS_JUMP_SIZE) )
	{
		d = get_DSDBR01_SMBDFWD_min_d2Pr_dI2_zero_cross_jump_size();
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDFWD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDFWD_MAX_SUM_SQUARED_DIST_FROM_PR_I_LINE) )
	{
		d = get_DSDBR01_SMBDFWD_max_sum_of_squared_distances_from_Pr_I_line();
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDFWD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDFWD_MIN_SLOPE_CHANGE_OF_PR_I_LINE) )
	{
		d = get_DSDBR01_SMBDFWD_min_slope_change_of_Pr_I_lines();
		text.AppendFormat("%g",d);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDFWD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDFWD_LINKING_MAX_DISTANCE_BETWEEN_LINKED_JUMPS) )
	{
		i = get_DSDBR01_SMBDFWD_linking_max_distance_between_linked_jumps();
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDFWD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDFWD_LINKING_MAX_SHARPNESS) )
	{
		d = get_DSDBR01_SMBDFWD_linking_min_sharpness();
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDFWD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDFWD_LINKING_MIN_SHARPNESS) )
	{
		d = get_DSDBR01_SMBDFWD_linking_max_sharpness();
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDFWD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDFWD_LINKING_MAX_GAP_BETWEEN_DOUBLE_LINES) )
	{
		i = get_DSDBR01_SMBDFWD_linking_max_gap_between_double_lines();
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDFWD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDFWD_LINKING_MIN_GAP_BETWEEN_DOUBLE_LINES) )
	{
		i = get_DSDBR01_SMBDFWD_linking_min_gap_between_double_lines();
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDFWD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDFWD_LINKING_NEAR_BOTTOM) )
	{
		i = get_DSDBR01_SMBDFWD_linking_near_bottom_row();
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDFWD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDFWD_LINKING_SHARPNESS_NEAR_BOTTOM) )
	{
		d = get_DSDBR01_SMBDFWD_linking_sharpness_near_bottom_row();
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDFWD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDFWD_LINKING_MAX_DISTANCE_BETWEEN_LINKS_NEAR_BOTTOM) )
	{
		i = get_DSDBR01_SMBDFWD_linking_max_distance_near_bottom_row();
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDFWD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDFWD_LINKING_MAX_DISTANCE_TO_LINK_SINGLE_UNLINKED_JUMP) )
	{
		i = get_DSDBR01_SMBDFWD_linking_max_distance_to_single_unlinked_jump();
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDFWD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDFWD_LINKING_MAX_Y_DISTANCE_TO_LINK) )
	{
		i = get_DSDBR01_SMBDFWD_linking_max_y_distance_to_link();
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDFWD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDFWD_LINKING_SHARPNESS_RAMP_SPLIT) )
	{
		d = get_DSDBR01_SMBDFWD_linking_sharpness_ramp_split();
		text.AppendFormat("%g",d);
	}




	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDREV) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDREV_I_RAMP_BOUNDARIES) )
	{
		i = get_DSDBR01_SMBDREV_I_ramp_boundaries_max();
		text.AppendFormat("%d",i);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDREV) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDREV_BOUNDARIES_MIN_SEPARATION) )
	{
		i = get_DSDBR01_SMBDREV_boundaries_min_separation();
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDREV) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDREV_MIN_D2PR_DI2_ZERO_CROSS_JUMP_SIZE) )
	{
		d = get_DSDBR01_SMBDREV_min_d2Pr_dI2_zero_cross_jump_size();
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDREV) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDREV_MAX_SUM_SQUARED_DIST_FROM_PR_I_LINE) )
	{
		d = get_DSDBR01_SMBDREV_max_sum_of_squared_distances_from_Pr_I_line();
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDREV) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDREV_MIN_SLOPE_CHANGE_OF_PR_I_LINE) )
	{
		d = get_DSDBR01_SMBDREV_min_slope_change_of_Pr_I_lines();
		text.AppendFormat("%g",d);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDREV) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDREV_LINKING_MAX_DISTANCE_BETWEEN_LINKED_JUMPS) )
	{
		i = get_DSDBR01_SMBDREV_linking_max_distance_between_linked_jumps();
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDREV) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDREV_LINKING_MAX_SHARPNESS) )
	{
		d = get_DSDBR01_SMBDREV_linking_min_sharpness();
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDREV) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDREV_LINKING_MIN_SHARPNESS) )
	{
		d = get_DSDBR01_SMBDREV_linking_max_sharpness();
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDREV) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDREV_LINKING_MAX_GAP_BETWEEN_DOUBLE_LINES) )
	{
		i = get_DSDBR01_SMBDREV_linking_max_gap_between_double_lines();
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDREV) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDREV_LINKING_MIN_GAP_BETWEEN_DOUBLE_LINES) )
	{
		i = get_DSDBR01_SMBDREV_linking_min_gap_between_double_lines();
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDREV) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDREV_LINKING_NEAR_BOTTOM) )
	{
		i = get_DSDBR01_SMBDREV_linking_near_bottom_row();
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDREV) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDREV_LINKING_SHARPNESS_NEAR_BOTTOM) )
	{
		d = get_DSDBR01_SMBDREV_linking_sharpness_near_bottom_row();
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDREV) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDREV_LINKING_MAX_DISTANCE_BETWEEN_LINKS_NEAR_BOTTOM) )
	{
		i = get_DSDBR01_SMBDREV_linking_max_distance_near_bottom_row();
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDREV) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDREV_LINKING_MAX_DISTANCE_TO_LINK_SINGLE_UNLINKED_JUMP) )
	{
		i = get_DSDBR01_SMBDREV_linking_max_distance_to_single_unlinked_jump();
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDREV) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDREV_LINKING_MAX_Y_DISTANCE_TO_LINK) )
	{
		i = get_DSDBR01_SMBDREV_linking_max_y_distance_to_link();
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDREV) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDREV_LINKING_SHARPNESS_RAMP_SPLIT) )
	{
		d = get_DSDBR01_SMBDREV_linking_sharpness_ramp_split();
		text.AppendFormat("%g",d);
	}





	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMQA) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMQA_SLOPE_WINDOW_SIZE) )
	{
		i = get_DSDBR01_SMQA_slope_window_size();
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMQA) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMQA_MODAL_DISTORTION_MIN_X) )
	{
		d = get_DSDBR01_SMQA_modal_distortion_min_x();
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMQA) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMQA_MODAL_DISTORTION_MAX_X) )
	{
		d = get_DSDBR01_SMQA_modal_distortion_max_x();
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMQA) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMQA_MODAL_DISTORTION_MIN_Y) )
	{
		d = get_DSDBR01_SMQA_modal_distortion_min_y();
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMQA) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMQA_IGNORE_BDC_AT_FSC) )
	{
		b = get_DSDBR01_SMQA_ignore_DBC_at_FSC();
		if(b) text += _T(REG_BOOL_TRUE_VALUE);
		else text += _T(REG_BOOL_FALSE_VALUE);
	}

//gdm added reg entries for mode width and hysteresis analysis windows
	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMQA) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMQA_MODE_WIDTH_ANALYSIS_MIN_X) )
	{
		d = get_DSDBR01_SMQA_mode_width_analysis_min_x();
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMQA) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMQA_MODE_WIDTH_ANALYSIS_MAX_X) )
	{
		d = get_DSDBR01_SMQA_mode_width_analysis_max_x();
		text.AppendFormat("%g",d);
	}
//hysteresis
		if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMQA) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMQA_HYSTERESIS_ANALYSIS_MIN_X) )
	{
		d = get_DSDBR01_SMQA_hysteresis_analysis_min_x();
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMQA) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMQA_HYSTERESIS_ANALYSIS_MAX_X) )
	{
		d = get_DSDBR01_SMQA_hysteresis_analysis_max_x();
		text.AppendFormat("%g",d);
	}
//gdm





	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_ALL) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD) )
	{
		i = get_DSDBR01_SMPF_max_mode_width(0);
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_ALL) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD) )
	{
		i = get_DSDBR01_SMPF_min_mode_width(0);
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_ALL) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD) )
	{
		i = get_DSDBR01_SMPF_max_mode_borders_removed(0);
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_ALL) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_middle_line_rms_value(0);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_ALL) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_min_middle_line_slope(0);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_ALL) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_middle_line_slope(0);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_ALL) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_mean_perc_working_region(0);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_ALL) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_min_perc_working_region(0);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_ALL) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_modal_distortion_angle(0);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_ALL) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_modal_distortion_angle_x_pos(0);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_ALL) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_mode_bdc_area(0);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_ALL) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_mode_bdc_area_x_length(0);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_ALL) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_sum_mode_bdc_areas(0);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_ALL) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_num_mode_bdc_areas(0);
		text.AppendFormat("%g",d);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_ALL) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_mode_line_slope(0);
		text.AppendFormat("%g",d);
	}










	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM0) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD) )
	{
		i = get_DSDBR01_SMPF_max_mode_width(0);
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM0) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD) )
	{
		i = get_DSDBR01_SMPF_min_mode_width(0);
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM0) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD) )
	{
		i = get_DSDBR01_SMPF_max_mode_borders_removed(0);
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM0) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_middle_line_rms_value(0);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM0) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_min_middle_line_slope(0);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM0) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_middle_line_slope(0);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM0) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_mean_perc_working_region(0);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM0) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_min_perc_working_region(0);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM0) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_modal_distortion_angle(0);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM0) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_modal_distortion_angle_x_pos(0);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM0) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_mode_bdc_area(0);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM0) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_mode_bdc_area_x_length(0);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM0) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_sum_mode_bdc_areas(0);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM0) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_num_mode_bdc_areas(0);
		text.AppendFormat("%g",d);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM0) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_mode_line_slope(0);
		text.AppendFormat("%g",d);
	}








	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM1) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD) )
	{
		i = get_DSDBR01_SMPF_max_mode_width(1);
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM1) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD) )
	{
		i = get_DSDBR01_SMPF_min_mode_width(1);
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM1) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD) )
	{
		i = get_DSDBR01_SMPF_max_mode_borders_removed(1);
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM1) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_middle_line_rms_value(1);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM1) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_min_middle_line_slope(1);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM1) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_middle_line_slope(1);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM1) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_mean_perc_working_region(1);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM1) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_min_perc_working_region(1);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM1) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_modal_distortion_angle(1);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM1) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_modal_distortion_angle_x_pos(1);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM1) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_mode_bdc_area(1);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM1) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_mode_bdc_area_x_length(1);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM1) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_sum_mode_bdc_areas(1);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM1) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_num_mode_bdc_areas(1);
		text.AppendFormat("%g",d);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM1) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_mode_line_slope(1);
		text.AppendFormat("%g",d);
	}









	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM2) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD) )
	{
		i = get_DSDBR01_SMPF_max_mode_width(2);
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM2) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD) )
	{
		i = get_DSDBR01_SMPF_min_mode_width(2);
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM2) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD) )
	{
		i = get_DSDBR01_SMPF_max_mode_borders_removed(2);
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM2) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_middle_line_rms_value(2);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM2) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_min_middle_line_slope(2);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM2) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_middle_line_slope(2);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM2) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_mean_perc_working_region(2);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM2) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_min_perc_working_region(2);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM2) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_modal_distortion_angle(2);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM2) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_modal_distortion_angle_x_pos(2);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM2) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_mode_bdc_area(2);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM2) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_mode_bdc_area_x_length(2);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM2) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_sum_mode_bdc_areas(2);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM2) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_num_mode_bdc_areas(2);
		text.AppendFormat("%g",d);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM2) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_mode_line_slope(2);
		text.AppendFormat("%g",d);
	}









	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM3) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD) )
	{
		i = get_DSDBR01_SMPF_max_mode_width(3);
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM3) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD) )
	{
		i = get_DSDBR01_SMPF_min_mode_width(3);
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM3) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD) )
	{
		i = get_DSDBR01_SMPF_max_mode_borders_removed(3);
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM3) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_middle_line_rms_value(3);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM3) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_min_middle_line_slope(3);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM3) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_middle_line_slope(3);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM3) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_mean_perc_working_region(3);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM3) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_min_perc_working_region(3);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM3) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_modal_distortion_angle(3);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM3) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_modal_distortion_angle_x_pos(3);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM3) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_mode_bdc_area(3);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM3) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_mode_bdc_area_x_length(3);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM3) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_sum_mode_bdc_areas(3);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM3) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_num_mode_bdc_areas(3);
		text.AppendFormat("%g",d);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM3) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_mode_line_slope(3);
		text.AppendFormat("%g",d);
	}









	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM4) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD) )
	{
		i = get_DSDBR01_SMPF_max_mode_width(4);
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM4) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD) )
	{
		i = get_DSDBR01_SMPF_min_mode_width(4);
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM4) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD) )
	{
		i = get_DSDBR01_SMPF_max_mode_borders_removed(4);
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM4) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_middle_line_rms_value(4);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM4) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_min_middle_line_slope(4);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM4) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_middle_line_slope(4);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM4) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_mean_perc_working_region(4);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM4) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_min_perc_working_region(4);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM4) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_modal_distortion_angle(4);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM4) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_modal_distortion_angle_x_pos(4);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM4) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_mode_bdc_area(4);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM4) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_mode_bdc_area_x_length(4);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM4) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_sum_mode_bdc_areas(4);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM4) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_num_mode_bdc_areas(4);
		text.AppendFormat("%g",d);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM4) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_mode_line_slope(4);
		text.AppendFormat("%g",d);
	}









	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM5) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD) )
	{
		i = get_DSDBR01_SMPF_max_mode_width(5);
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM5) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD) )
	{
		i = get_DSDBR01_SMPF_min_mode_width(5);
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM5) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD) )
	{
		i = get_DSDBR01_SMPF_max_mode_borders_removed(5);
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM5) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_middle_line_rms_value(5);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM5) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_min_middle_line_slope(5);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM5) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_middle_line_slope(5);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM5) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_mean_perc_working_region(5);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM5) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_min_perc_working_region(5);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM5) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_modal_distortion_angle(5);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM5) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_modal_distortion_angle_x_pos(5);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM5) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_mode_bdc_area(5);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM5) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_mode_bdc_area_x_length(5);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM5) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_sum_mode_bdc_areas(5);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM5) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_num_mode_bdc_areas(5);
		text.AppendFormat("%g",d);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM5) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_mode_line_slope(5);
		text.AppendFormat("%g",d);
	}









	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM6) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD) )
	{
		i = get_DSDBR01_SMPF_max_mode_width(6);
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM6) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD) )
	{
		i = get_DSDBR01_SMPF_min_mode_width(6);
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM6) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD) )
	{
		i = get_DSDBR01_SMPF_max_mode_borders_removed(6);
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM6) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_middle_line_rms_value(6);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM6) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_min_middle_line_slope(6);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM6) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_middle_line_slope(6);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM6) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_mean_perc_working_region(6);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM6) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_min_perc_working_region(6);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM6) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_modal_distortion_angle(6);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM6) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_modal_distortion_angle_x_pos(6);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM6) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_mode_bdc_area(6);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM6) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_mode_bdc_area_x_length(6);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM6) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_sum_mode_bdc_areas(6);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM6) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_num_mode_bdc_areas(6);
		text.AppendFormat("%g",d);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM6) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_mode_line_slope(6);
		text.AppendFormat("%g",d);
	}









	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM7) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD) )
	{
		i = get_DSDBR01_SMPF_max_mode_width(7);
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM7) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD) )
	{
		i = get_DSDBR01_SMPF_min_mode_width(7);
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM7) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD) )
	{
		i = get_DSDBR01_SMPF_max_mode_borders_removed(7);
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM7) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_middle_line_rms_value(7);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM7) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_min_middle_line_slope(7);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM7) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_middle_line_slope(7);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM7) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_mean_perc_working_region(7);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM7) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_min_perc_working_region(7);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM7) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_modal_distortion_angle(7);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM7) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_modal_distortion_angle_x_pos(7);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM7) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_mode_bdc_area(7);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM7) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_mode_bdc_area_x_length(7);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM7) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_sum_mode_bdc_areas(7);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM7) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_num_mode_bdc_areas(7);
		text.AppendFormat("%g",d);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM7) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_mode_line_slope(7);
		text.AppendFormat("%g",d);
	}









	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM8) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD) )
	{
		i = get_DSDBR01_SMPF_max_mode_width(8);
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM8) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD) )
	{
		i = get_DSDBR01_SMPF_min_mode_width(8);
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM8) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD) )
	{
		i = get_DSDBR01_SMPF_max_mode_borders_removed(8);
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM8) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_middle_line_rms_value(8);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM8) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_min_middle_line_slope(8);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM8) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_middle_line_slope(8);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM8) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_mean_perc_working_region(8);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM8) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_min_perc_working_region(8);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM8) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_modal_distortion_angle(8);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM8) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_modal_distortion_angle_x_pos(8);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM8) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_mode_bdc_area(8);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM8) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_mode_bdc_area_x_length(8);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM8) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_sum_mode_bdc_areas(8);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM8) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_num_mode_bdc_areas(8);
		text.AppendFormat("%g",d);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM8) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_mode_line_slope(8);
		text.AppendFormat("%g",d);
	}









	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM9) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD) )
	{
		i = get_DSDBR01_SMPF_max_mode_width(9);
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM9) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD) )
	{
		i = get_DSDBR01_SMPF_min_mode_width(9);
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM9) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD) )
	{
		i = get_DSDBR01_SMPF_max_mode_borders_removed(9);
		text.AppendFormat("%d",i);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM9) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_middle_line_rms_value(9);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM9) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_min_middle_line_slope(9);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM9) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_middle_line_slope(9);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM9) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_mean_perc_working_region(9);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM9) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_min_perc_working_region(9);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM9) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_modal_distortion_angle(9);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM9) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_modal_distortion_angle_x_pos(9);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM9) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_mode_bdc_area(9);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM9) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_mode_bdc_area_x_length(9);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM9) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_sum_mode_bdc_areas(9);
		text.AppendFormat("%g",d);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM9) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_num_mode_bdc_areas(9);
		text.AppendFormat("%g",d);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM9) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD) )
	{
		d = get_DSDBR01_SMPF_max_mode_line_slope(9);
		text.AppendFormat("%g",d);
	}

}



CCGRegValues::rtype
CCGRegValues::setCString(CCGRegEntry reg_entry, CString value)
{
	CString reg = reg_entry.reg_key_name + _T("\\") + reg_entry.reg_entry_name;
	rtype rval = rtype::ok;


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString("\\")
		+ CString(REGNAME_DSDBR01_BASE_RESULTS_DIR) )
	{
		rval = set_DSDBR01_BASE_results_directory(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_CMDC_GAIN_MODULE_NAME)  )
	{
		rval = set_DSDBR01_CMDC_gain_module_name(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_CMDC_SOA_MODULE_NAME)  )
	{
		rval = set_DSDBR01_CMDC_soa_module_name(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_CMDC_REAR_MODULE_NAME)  )
	{
		rval = set_DSDBR01_CMDC_rear_module_name(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_CMDC_PHASE_MODULE_NAME)  )
	{
		rval = set_DSDBR01_CMDC_phase_module_name(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_CMDC_FRONT1_MODULE_NAME)  )
	{
		rval = set_DSDBR01_CMDC_front1_module_name(value);
	}
	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_CMDC_FRONT2_MODULE_NAME)  )
	{
		rval = set_DSDBR01_CMDC_front2_module_name(value);
	}
	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_CMDC_FRONT3_MODULE_NAME)  )
	{
		rval = set_DSDBR01_CMDC_front3_module_name(value);
	}
	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_CMDC_FRONT4_MODULE_NAME)  )
	{
		rval = set_DSDBR01_CMDC_front4_module_name(value);
	}
	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_CMDC_FRONT5_MODULE_NAME)  )
	{
		rval = set_DSDBR01_CMDC_front5_module_name(value);
	}
	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_CMDC_FRONT6_MODULE_NAME)  )
	{
		rval = set_DSDBR01_CMDC_front6_module_name(value);
	}
	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_CMDC_FRONT7_MODULE_NAME)  )
	{
		rval = set_DSDBR01_CMDC_front7_module_name(value);
	}
	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_CMDC_FRONT8_MODULE_NAME)  )
	{
		rval = set_DSDBR01_CMDC_front8_module_name(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_CMDC_306II_MODULE_NAME)  )
	{
		rval = set_DSDBR01_CMDC_306II_module_name(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_CMDC_306EE_MODULE_NAME)  )
	{
		rval = set_DSDBR01_CMDC_306EE_module_name(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMDC_RAMP_DIRECTION)  )
	{
		rval = set_DSDBR01_OMDC_ramp_direction(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMDC_REAR_CURRENTS_ABSPATH)  )
	{
		rval = set_DSDBR01_OMDC_rear_currents_abspath(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMDC_FRONT_CURRENTS_ABSPATH)  )
	{
		rval = set_DSDBR01_OMDC_front_currents_abspath(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CFREQ) + CString("\\")
		+ CString(REGNAME_DSDBR01_CFREQ_POLY_COEFFS_ABSPATH) )
	{
		rval = set_DSDBR01_CFREQ_poly_coeffs_abspath(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_ITUGRID) + CString("\\")
		+ CString(REGNAME_DSDBR01_ITUGRID_abspath) )
	{
		rval = set_DSDBR01_ITUGRID_abspath(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_ITUGRID) + CString("\\")
		+ CString(REGNAME_DSDBR01_ESTIMATESITUGRID_abspath) )
	{
		rval = set_DSDBR01_ESTIMATESITUGRID_abspath(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CHAR) + CString("\\")
		+ CString(REGNAME_DSDBR01_CHAR_fakefreq_Iramp_module_name) )
	{
		rval = set_DSDBR01_CHAR_fakefreq_Iramp_module_name(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CHAR) + CString("\\")
		+ CString(REGNAME_DSDBR01_CHAR_Iramp_vs_fakefreq_abspath) )
	{
		rval = set_DSDBR01_CHAR_Iramp_vs_fakefreq_abspath(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMDC_RAMP_DIRECTION) )
	{
		rval = set_DSDBR01_SMDC_ramp_direction(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMDC_PHASE_CURRENTS_ABSPATH) )
	{
		rval = set_DSDBR01_SMDC_phase_currents_abspath(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBD_RAMP_ABSPATH) )
	{
		rval = set_DSDBR01_SMBD_ramp_abspath(value);
	}


	return rval;
}


CCGRegValues::rtype
CCGRegValues::setDouble(CCGRegEntry reg_entry, double value)
{
	CString reg = reg_entry.reg_key_name + _T("\\") + reg_entry.reg_entry_name;
	rtype rval = rtype::ok;


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMBD_MAX_DELTAPR_IN_SM) )
	{
		rval = set_DSDBR01_OMBD_max_deltaPr_in_sm(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMQA) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMQA_MODAL_DISTORTION_MIN_X) )
	{
		rval = set_DSDBR01_OMQA_modal_distortion_min_x(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMQA) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMQA_MODAL_DISTORTION_MAX_X) )
	{
		rval = set_DSDBR01_OMQA_modal_distortion_max_x(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMQA) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMQA_MODAL_DISTORTION_MIN_Y) )
	{
		rval = set_DSDBR01_OMQA_modal_distortion_min_y(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMPF) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMPF_MAX_MIDDLE_LINE_RMS_VALUE) )
	{
		rval = set_DSDBR01_OMPF_max_middle_line_rms_value(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMPF) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMPF_MIN_MIDDLE_LINE_SLOPE) )
	{
		rval = set_DSDBR01_OMPF_min_middle_line_slope(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMPF) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMPF_MAX_MIDDLE_LINE_SLOPE) )
	{
		rval = set_DSDBR01_OMPF_max_middle_line_slope(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMPF) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMPF_MAX_PR_GAP) )
	{
		rval = set_DSDBR01_OMPF_max_pr_gap(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMPF) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMPF_MAX_LOWER_PR) )
	{
		rval = set_DSDBR01_OMPF_max_lower_pr(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMPF) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMPF_MIN_UPPER_PR) )
	{
		rval = set_DSDBR01_OMPF_min_upper_pr(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMPF) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMPF_MAX_MODE_BDC_AREA_THRESHOLD) )
	{
		rval = set_DSDBR01_OMPF_max_mode_bdc_area_threshold(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMPF) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD) )
	{
		rval = set_DSDBR01_OMPF_max_mode_bdc_area_x_length_threshold(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMPF) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMPF_SUM_MODE_BDC_AREAS_THRESHOLD) )
	{
		rval = set_DSDBR01_OMPF_sum_mode_bdc_areas_threshold(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMPF) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMPF_NUM_MODE_BDC_AREAS_THRESHOLD) )
	{
		rval = set_DSDBR01_OMPF_num_mode_bdc_areas_threshold(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMPF) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMPF_MODAL_DISTORTION_ANGLE_THRESHOLD) )
	{
		rval = set_DSDBR01_OMPF_modal_distortion_angle_threshold(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMPF) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD) )
	{
		rval = set_DSDBR01_OMPF_modal_distortion_angle_x_pos_threshold(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMPF) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMPF_MAX_MODE_LINE_SLOPE_THRESHOLD) )
	{
		rval = set_DSDBR01_OMPF_max_mode_line_slope_threshold(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMPF) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMPF_MIN_MODE_LINE_SLOPE_THRESHOLD) )
	{
		rval = set_DSDBR01_OMPF_min_mode_line_slope_threshold(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CFREQ) + CString("\\")
		+ CString(REGNAME_DSDBR01_CFREQ_MAX_STABLE_FREQ_ERROR) )
	{
		rval = set_DSDBR01_CFREQ_max_stable_freq_error(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_ITUGRID) + CString("\\")
		+ CString(REGNAME_DSDBR01_ITUGRID_CENTRE_FREQ) )
	{
		rval = set_DSDBR01_ITUGRID_centre_freq(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_ITUGRID) + CString("\\")
		+ CString(REGNAME_DSDBR01_ITUGRID_STEP_FREQ) )
	{
		rval = set_DSDBR01_ITUGRID_step_freq(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CHAR) + CString("\\")
		+ CString(REGNAME_DSDBR01_CHAR_FREQ_ACCURACY_OF_WAVEMETER) )
	{
		rval = set_DSDBR01_CHAR_freq_accuracy_of_wavemeter(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CHAR) + CString("\\")
		+ CString(REGNAME_DSDBR01_CHAR_MAX_STABLE_FREQ_ERROR) )
	{
		rval = set_DSDBR01_CHAR_max_stable_freq_error(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CHAR) + CString("\\")
		+ CString(REGNAME_DSDBR01_CHAR_fakefreq_max_error) )
	{
		rval = set_DSDBR01_CHAR_fakefreq_max_error(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CHARTEC) + CString("\\")
		+ CString(REGNAME_DSDBR01_CHARTEC_max_temp_deviation) )
	{
		rval = set_DSDBR01_CHARTEC_max_temp_deviation(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBD_MAX_DELTAPR_IN_LM) )
	{
		rval = set_DSDBR01_SMBD_max_deltaPr_in_lm(value);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBD_VERTICAL_PERCENT_SHIFT) )
	{
		rval = set_DSDBR01_SMBD_vertical_percent_shift(value);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBD_VERTICAL_PERCENT_SHIFT) )
	{
		rval = set_DSDBR01_SMBD_vertical_percent_shift(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBD_ZPRC_HYSTERESIS_UPPER_THRESHOLD) )
	{
		rval = set_DSDBR01_SMBD_zprc_hysteresis_upper_threshold(value);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBD_ZPRC_HYSTERESIS_LOWER_THRESHOLD) )
	{
		rval = set_DSDBR01_SMBD_zprc_hysteresis_lower_threshold(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBD_ZPRC_MAX_HYSTERESIS_RISE) )
	{
		rval = set_DSDBR01_SMBD_zprc_max_hysteresis_rise(value);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDFWD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDFWD_MIN_D2PR_DI2_ZERO_CROSS_JUMP_SIZE) )
	{
		rval = set_DSDBR01_SMBDFWD_min_d2Pr_dI2_zero_cross_jump_size(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDFWD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDFWD_MAX_SUM_SQUARED_DIST_FROM_PR_I_LINE) )
	{
		rval = set_DSDBR01_SMBDFWD_max_sum_of_squared_distances_from_Pr_I_line(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDFWD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDFWD_MIN_SLOPE_CHANGE_OF_PR_I_LINE) )
	{
		rval = set_DSDBR01_SMBDFWD_min_slope_change_of_Pr_I_lines(value);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDFWD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDFWD_LINKING_MAX_SHARPNESS) )
	{
		rval = set_DSDBR01_SMBDFWD_linking_min_sharpness(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDFWD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDFWD_LINKING_MIN_SHARPNESS) )
	{
		rval = set_DSDBR01_SMBDFWD_linking_max_sharpness(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDFWD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDFWD_LINKING_SHARPNESS_NEAR_BOTTOM) )
	{
		rval = set_DSDBR01_SMBDFWD_linking_sharpness_near_bottom_row(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDFWD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDFWD_LINKING_SHARPNESS_RAMP_SPLIT) )
	{
		rval = set_DSDBR01_SMBDFWD_linking_sharpness_ramp_split(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDREV) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDREV_MIN_D2PR_DI2_ZERO_CROSS_JUMP_SIZE) )
	{
		rval = set_DSDBR01_SMBDREV_min_d2Pr_dI2_zero_cross_jump_size(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDREV) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDREV_MAX_SUM_SQUARED_DIST_FROM_PR_I_LINE) )
	{
		rval = set_DSDBR01_SMBDREV_max_sum_of_squared_distances_from_Pr_I_line(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDREV) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDREV_MIN_SLOPE_CHANGE_OF_PR_I_LINE) )
	{
		rval = set_DSDBR01_SMBDREV_min_slope_change_of_Pr_I_lines(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDREV) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDREV_LINKING_MAX_SHARPNESS) )
	{
		rval = set_DSDBR01_SMBDREV_linking_min_sharpness(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDREV) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDREV_LINKING_MIN_SHARPNESS) )
	{
		rval = set_DSDBR01_SMBDREV_linking_max_sharpness(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDREV) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDREV_LINKING_SHARPNESS_NEAR_BOTTOM) )
	{
		rval = set_DSDBR01_SMBDREV_linking_sharpness_near_bottom_row(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDREV) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDREV_LINKING_SHARPNESS_RAMP_SPLIT) )
	{
		rval = set_DSDBR01_SMBDREV_linking_sharpness_ramp_split(value);
	}



	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMQA) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMQA_MODAL_DISTORTION_MIN_X) )
	{
		rval = set_DSDBR01_SMQA_modal_distortion_min_x(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMQA) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMQA_MODAL_DISTORTION_MAX_X) )
	{
		rval = set_DSDBR01_SMQA_modal_distortion_max_x(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMQA) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMQA_MODAL_DISTORTION_MIN_Y) )
	{
		rval = set_DSDBR01_SMQA_modal_distortion_min_y(value);
	}

	//gdm oct/nov 06 added mode width and hysteresis analysis reg entries
	//mode width
	
	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMQA) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMQA_MODE_WIDTH_ANALYSIS_MIN_X) )
	{
		rval = set_DSDBR01_SMQA_mode_width_analysis_min_x(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMQA) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMQA_MODE_WIDTH_ANALYSIS_MAX_X) )
	{
		rval = set_DSDBR01_SMQA_mode_width_analysis_max_x(value);
	}
	//hysteresis
	
	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMQA) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMQA_HYSTERESIS_ANALYSIS_MIN_X) )
	{
		rval = set_DSDBR01_SMQA_hysteresis_analysis_min_x(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMQA) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMQA_HYSTERESIS_ANALYSIS_MAX_X) )
	{
		rval = set_DSDBR01_SMQA_hysteresis_analysis_max_x(value);
	}
	//gdm


#define FIRST_SM_NUM 0
#define LAST_SM_NUM 9

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_ALL) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD) )
	{
		for(short sm = FIRST_SM_NUM; (sm <= LAST_SM_NUM) && (rval == rtype::ok); sm++)
			rval = set_DSDBR01_SMPF_max_middle_line_rms_value(sm,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_ALL) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD) )
	{
		for(short sm = FIRST_SM_NUM; (sm <= LAST_SM_NUM) && (rval == rtype::ok); sm++)
			rval = set_DSDBR01_SMPF_min_middle_line_slope(sm,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_ALL) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD) )
	{
		for(short sm = FIRST_SM_NUM; (sm <= LAST_SM_NUM) && (rval == rtype::ok); sm++)
			rval = set_DSDBR01_SMPF_max_middle_line_slope(sm,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_ALL) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD) )
	{
		for(short sm = FIRST_SM_NUM; (sm <= LAST_SM_NUM) && (rval == rtype::ok); sm++)
			rval = set_DSDBR01_SMPF_mean_perc_working_region(sm,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_ALL) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD) )
	{
		for(short sm = FIRST_SM_NUM; (sm <= LAST_SM_NUM) && (rval == rtype::ok); sm++)
			rval = set_DSDBR01_SMPF_min_perc_working_region(sm,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_ALL) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD) )
	{
		for(short sm = FIRST_SM_NUM; (sm <= LAST_SM_NUM) && (rval == rtype::ok); sm++)
			rval = set_DSDBR01_SMPF_modal_distortion_angle(sm,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_ALL) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD) )
	{
		for(short sm = FIRST_SM_NUM; (sm <= LAST_SM_NUM) && (rval == rtype::ok); sm++)
			rval = set_DSDBR01_SMPF_modal_distortion_angle_x_pos(sm,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_ALL) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD) )
	{
		for(short sm = FIRST_SM_NUM; (sm <= LAST_SM_NUM) && (rval == rtype::ok); sm++)
			rval = set_DSDBR01_SMPF_max_mode_bdc_area(sm,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_ALL) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD) )
	{
		for(short sm = FIRST_SM_NUM; (sm <= LAST_SM_NUM) && (rval == rtype::ok); sm++)
			rval = set_DSDBR01_SMPF_max_mode_bdc_area_x_length(sm,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_ALL) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD) )
	{
		for(short sm = FIRST_SM_NUM; (sm <= LAST_SM_NUM) && (rval == rtype::ok); sm++)
			rval = set_DSDBR01_SMPF_sum_mode_bdc_areas(sm,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_ALL) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD) )
	{
		for(short sm = FIRST_SM_NUM; (sm <= LAST_SM_NUM) && (rval == rtype::ok); sm++)
			rval = set_DSDBR01_SMPF_num_mode_bdc_areas(sm,value);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_ALL) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD) )
	{
		for(short sm = FIRST_SM_NUM; (sm <= LAST_SM_NUM) && (rval == rtype::ok); sm++)
			rval = set_DSDBR01_SMPF_max_mode_line_slope(sm,value);
	}










	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM0) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_middle_line_rms_value(0,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM0) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_min_middle_line_slope(0,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM0) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_middle_line_slope(0,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM0) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_mean_perc_working_region(0,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM0) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_min_perc_working_region(0,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM0) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_modal_distortion_angle(0,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM0) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_modal_distortion_angle_x_pos(0,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM0) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_bdc_area(0,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM0) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_bdc_area_x_length(0,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM0) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_sum_mode_bdc_areas(0,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM0) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_num_mode_bdc_areas(0,value);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM0) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_line_slope(0,value);
	}





	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM1) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_middle_line_rms_value(1,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM1) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_min_middle_line_slope(1,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM1) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_middle_line_slope(1,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM1) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_mean_perc_working_region(1,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM1) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_min_perc_working_region(1,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM1) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_modal_distortion_angle(1,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM1) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_modal_distortion_angle_x_pos(1,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM1) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_bdc_area(1,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM1) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_bdc_area_x_length(1,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM1) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_sum_mode_bdc_areas(1,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM1) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_num_mode_bdc_areas(1,value);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM1) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_line_slope(1,value);
	}








	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM2) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_middle_line_rms_value(2,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM2) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_min_middle_line_slope(2,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM2) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_middle_line_slope(2,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM2) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_mean_perc_working_region(2,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM2) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_min_perc_working_region(2,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM2) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_modal_distortion_angle(2,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM2) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_modal_distortion_angle_x_pos(2,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM2) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_bdc_area(2,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM2) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_bdc_area_x_length(2,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM2) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_sum_mode_bdc_areas(2,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM2) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_num_mode_bdc_areas(2,value);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM2) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_line_slope(2,value);
	}





	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM3) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_middle_line_rms_value(3,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM3) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_min_middle_line_slope(3,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM3) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_middle_line_slope(3,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM3) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_mean_perc_working_region(3,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM3) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_min_perc_working_region(3,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM3) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_modal_distortion_angle(3,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM3) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_modal_distortion_angle_x_pos(3,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM3) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_bdc_area(3,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM3) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_bdc_area_x_length(3,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM3) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_sum_mode_bdc_areas(3,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM3) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_num_mode_bdc_areas(3,value);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM3) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_line_slope(3,value);
	}







	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM4) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_middle_line_rms_value(4,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM4) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_min_middle_line_slope(4,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM4) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_middle_line_slope(4,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM4) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_mean_perc_working_region(4,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM4) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_min_perc_working_region(4,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM4) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_modal_distortion_angle(4,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM4) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_modal_distortion_angle_x_pos(4,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM4) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_bdc_area(4,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM4) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_bdc_area_x_length(4,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM4) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_sum_mode_bdc_areas(4,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM4) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_num_mode_bdc_areas(4,value);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM4) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_line_slope(4,value);
	}






	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM5) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_middle_line_rms_value(5,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM5) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_min_middle_line_slope(5,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM5) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_middle_line_slope(5,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM5) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_mean_perc_working_region(5,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM5) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_min_perc_working_region(5,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM5) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_modal_distortion_angle(5,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM5) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_modal_distortion_angle_x_pos(5,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM5) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_bdc_area(5,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM5) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_bdc_area_x_length(5,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM5) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_sum_mode_bdc_areas(5,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM5) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_num_mode_bdc_areas(5,value);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM5) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_line_slope(5,value);
	}



	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM6) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_middle_line_rms_value(6,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM6) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_min_middle_line_slope(6,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM6) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_middle_line_slope(6,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM6) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_mean_perc_working_region(6,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM6) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_min_perc_working_region(6,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM6) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_modal_distortion_angle(6,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM6) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_modal_distortion_angle_x_pos(6,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM6) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_bdc_area(6,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM6) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_bdc_area_x_length(6,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM6) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_sum_mode_bdc_areas(6,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM6) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_num_mode_bdc_areas(6,value);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM6) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_line_slope(6,value);
	}







	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM7) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_middle_line_rms_value(7,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM7) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_min_middle_line_slope(7,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM7) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_middle_line_slope(7,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM7) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_mean_perc_working_region(7,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM7) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_min_perc_working_region(7,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM7) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_modal_distortion_angle(7,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM7) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_modal_distortion_angle_x_pos(7,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM7) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_bdc_area(7,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM7) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_bdc_area_x_length(7,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM7) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_sum_mode_bdc_areas(7,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM7) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_num_mode_bdc_areas(7,value);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM7) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_line_slope(7,value);
	}







	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM8) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_middle_line_rms_value(8,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM8) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_min_middle_line_slope(8,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM8) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_middle_line_slope(8,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM8) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_mean_perc_working_region(8,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM8) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_min_perc_working_region(8,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM8) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_modal_distortion_angle(8,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM8) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_modal_distortion_angle_x_pos(8,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM8) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_bdc_area(8,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM8) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_bdc_area_x_length(8,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM8) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_sum_mode_bdc_areas(8,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM8) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_num_mode_bdc_areas(8,value);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM8) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_line_slope(8,value);
	}







	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM9) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_middle_line_rms_value(9,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM9) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_min_middle_line_slope(9,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM9) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_middle_line_slope(9,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM9) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_mean_perc_working_region(9,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM9) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_min_perc_working_region(9,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM9) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_modal_distortion_angle(9,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM9) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_modal_distortion_angle_x_pos(9,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM9) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_bdc_area(9,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM9) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_bdc_area_x_length(9,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM9) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_sum_mode_bdc_areas(9,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM9) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_num_mode_bdc_areas(9,value);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM9) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_line_slope(9,value);
	}

	return rval;
}


CCGRegValues::rtype
CCGRegValues::setInt(CCGRegEntry reg_entry, int value)
{
	CString reg = reg_entry.reg_key_name + _T("\\") + reg_entry.reg_entry_name;
	rtype rval = rtype::ok;


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_CMDC_306II_DIRECT_CHANNEL)  )
	{
		rval = set_DSDBR01_CMDC_306II_direct_channel(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_CMDC_306II_FILTERED_CHANNEL)  )
	{
		rval = set_DSDBR01_CMDC_306II_filtered_channel(value);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_CMDC_306EE_PHOTODIODE1_CHANNEL)  )
	{
		rval = set_DSDBR01_CMDC_306EE_photodiode1_channel(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_CMDC_306EE_PHOTODIODE2_CHANNEL)  )
	{
		rval = set_DSDBR01_CMDC_306EE_photodiode2_channel(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_CMDC_WAVEMETER_RETRIES)  )
	{
		rval = set_DSDBR01_CMDC_wavemeter_retries(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMDC_SOURCE_DELAY)  )
	{
		rval = set_DSDBR01_OMDC_source_delay(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMDC_MEASURE_DELAY)  )
	{
		rval = set_DSDBR01_OMDC_measure_delay(value);
	}



	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMBD_MEDIAN_FILTER_RANK) )
	{
		rval = set_DSDBR01_OMBD_median_filter_rank(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMBD_MIN_POINTS_WIDTH_OF_SM) )
	{
		rval = set_DSDBR01_OMBD_min_points_width_of_sm(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMBD_ML_MOVING_AVERAGE_FILTER_N) )
	{
		rval = set_DSDBR01_OMBD_ml_moving_ave_filter_n(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMBD_ML_MIN_LENGTH) )
	{
		rval = set_DSDBR01_OMBD_ml_min_length(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMQA) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMQA_SLOPE_WINDOW_SIZE) )
	{
		rval = set_DSDBR01_OMQA_slope_window_size(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMPF) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD) )
	{
		rval = set_DSDBR01_OMPF_max_mode_borders_removed_threshold(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMPF) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMPF_MIN_MODE_WIDTH_THRESHOLD) )
	{
		rval = set_DSDBR01_OMPF_min_mode_width_threshold(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMPF) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMPF_MAX_MODE_WIDTH_THRESHOLD) )
	{
		rval = set_DSDBR01_OMPF_max_mode_width_threshold(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CFREQ) + CString("\\")
		+ CString(REGNAME_DSDBR01_CFREQ_NUM_FREQ_MEAS_PER_OP) )
	{
		rval = set_DSDBR01_CFREQ_num_freq_meas_per_op(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CFREQ) + CString("\\")
		+ CString(REGNAME_DSDBR01_CFREQ_POWER_RATIO_MEAS_PER_OP) )
	{
		rval = set_DSDBR01_CFREQ_num_power_ratio_meas_per_op(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CFREQ) + CString("\\")
		+ CString(REGNAME_DSDBR01_CFREQ_SAMPLE_POINT_SETTLE_TIME) )
	{
		rval = set_DSDBR01_CFREQ_sample_point_settle_time(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_ITUGRID) + CString("\\")
		+ CString(REGNAME_DSDBR01_ITUGRID_CHANNEL_COUNT) )
	{
		rval = set_DSDBR01_ITUGRID_channel_count(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CHAR) + CString("\\")
		+ CString(REGNAME_DSDBR01_CHAR_NUM_FREQ_MEAS_PER_OP) )
	{
		rval = set_DSDBR01_CHAR_num_freq_meas_per_op(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CHAR) + CString("\\")
		+ CString(REGNAME_DSDBR01_CHAR_OP_SETTLE_TIME) )
	{
		rval = set_DSDBR01_CHAR_op_settle_time(value);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CHAR) + CString("\\")
		+ CString(REGNAME_DSDBR01_CHAR_itusearch_scheme) )
	{
		rval = set_DSDBR01_CHAR_itusearch_scheme(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CHAR) + CString("\\")
		+ CString(REGNAME_DSDBR01_CHAR_itusearch_nextguess) )
	{
		rval = set_DSDBR01_CHAR_itusearch_nextguess(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CHAR) + CString("\\")
		+ CString(REGNAME_DSDBR01_CHAR_itusearch_resamples) )
	{
		rval = set_DSDBR01_CHAR_itusearch_resamples(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CHAR) + CString("\\")
		+ CString(REGNAME_DSDBR01_CHAR_POWER_RATIO_MEAS_PER_OP) )
	{
		rval = set_DSDBR01_CHAR_num_power_ratio_meas_per_op(value);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CHARTEC) + CString("\\")
		+ CString(REGNAME_DSDBR01_CHARTEC_time_window_size) )
	{
		rval = set_DSDBR01_CHARTEC_time_window_size(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CHARTEC) + CString("\\")
		+ CString(REGNAME_DSDBR01_CHARTEC_num_temp_readings) )
	{
		rval = set_DSDBR01_CHARTEC_num_temp_readings(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CHARTEC) + CString("\\")
		+ CString(REGNAME_DSDBR01_CHARTEC_max_settle_time) )
	{
		rval = set_DSDBR01_CHARTEC_max_settle_time(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMDC_SOURCE_DELAY) )
	{
		rval = set_DSDBR01_SMDC_source_delay(value);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMDC_MEASURE_DELAY) )
	{
		rval = set_DSDBR01_SMDC_measure_delay(value);
	}



	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBD_MEDIAN_FILTER_RANK) )
	{
		rval = set_DSDBR01_SMBD_median_filter_rank(value);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBD_MIN_POINTS_WIDTH_OF_LM) )
	{
		rval = set_DSDBR01_SMBD_min_points_width_of_lm(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBD_MAX_POINTS_WIDTH_OF_LM) )
	{
		rval = set_DSDBR01_SMBD_max_points_width_of_lm(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBD_ML_MOVING_AVERAGE_FILTER_N) )
	{
		rval = set_DSDBR01_SMBD_ml_moving_ave_filter_n(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBD_ML_MIN_LENGTH) )
	{
		rval = set_DSDBR01_SMBD_ml_min_length(value);
	}



	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBD_ZPRC_MIN_POINTS_SEPARATION) )
	{
		rval = set_DSDBR01_SMBD_zprc_min_points_separation(value);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBD_ZPRC_MAX_POINTS_TO_REVERSE_JUMP) )
	{
		rval = set_DSDBR01_SMBD_zprc_max_points_to_reverse_jump(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBD_ZPRC_MAX_HYSTERESIS_POINTS) )
	{
		rval = set_DSDBR01_SMBD_zprc_max_hysteresis_points(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDFWD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDFWD_I_RAMP_BOUNDARIES) )
	{
		rval = set_DSDBR01_SMBDFWD_I_ramp_boundaries_max(value);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDFWD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDFWD_BOUNDARIES_MIN_SEPARATION) )
	{
		rval = set_DSDBR01_SMBDFWD_boundaries_min_separation(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDFWD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDFWD_LINKING_MAX_DISTANCE_BETWEEN_LINKED_JUMPS) )
	{
		rval = set_DSDBR01_SMBDFWD_linking_max_distance_between_linked_jumps(value);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDFWD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDFWD_LINKING_MAX_GAP_BETWEEN_DOUBLE_LINES) )
	{
		rval = set_DSDBR01_SMBDFWD_linking_max_gap_between_double_lines(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDFWD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDFWD_LINKING_MIN_GAP_BETWEEN_DOUBLE_LINES) )
	{
		rval = set_DSDBR01_SMBDFWD_linking_min_gap_between_double_lines(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDFWD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDFWD_LINKING_NEAR_BOTTOM) )
	{
		rval = set_DSDBR01_SMBDFWD_linking_near_bottom_row(value);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDFWD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDFWD_LINKING_MAX_DISTANCE_BETWEEN_LINKS_NEAR_BOTTOM) )
	{
		rval = set_DSDBR01_SMBDFWD_linking_max_distance_near_bottom_row(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDFWD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDFWD_LINKING_MAX_DISTANCE_TO_LINK_SINGLE_UNLINKED_JUMP) )
	{
		rval = set_DSDBR01_SMBDFWD_linking_max_distance_to_single_unlinked_jump(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDFWD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDFWD_LINKING_MAX_Y_DISTANCE_TO_LINK) )
	{
		rval = set_DSDBR01_SMBDFWD_linking_max_y_distance_to_link(value);
	}





	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDREV) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDREV_I_RAMP_BOUNDARIES) )
	{
		rval = set_DSDBR01_SMBDREV_I_ramp_boundaries_max(value);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDREV) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDREV_BOUNDARIES_MIN_SEPARATION) )
	{
		rval = set_DSDBR01_SMBDREV_boundaries_min_separation(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDREV) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDREV_LINKING_MAX_DISTANCE_BETWEEN_LINKED_JUMPS) )
	{
		rval = set_DSDBR01_SMBDREV_linking_max_distance_between_linked_jumps(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDREV) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDREV_LINKING_MAX_GAP_BETWEEN_DOUBLE_LINES) )
	{
		rval = set_DSDBR01_SMBDREV_linking_max_gap_between_double_lines(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDREV) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDREV_LINKING_MIN_GAP_BETWEEN_DOUBLE_LINES) )
	{
		rval = set_DSDBR01_SMBDREV_linking_min_gap_between_double_lines(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDREV) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDREV_LINKING_NEAR_BOTTOM) )
	{
		rval = set_DSDBR01_SMBDREV_linking_near_bottom_row(value);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDREV) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDREV_LINKING_MAX_DISTANCE_BETWEEN_LINKS_NEAR_BOTTOM) )
	{
		rval = set_DSDBR01_SMBDREV_linking_max_distance_near_bottom_row(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDREV) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDREV_LINKING_MAX_DISTANCE_TO_LINK_SINGLE_UNLINKED_JUMP) )
	{
		rval = set_DSDBR01_SMBDREV_linking_max_distance_to_single_unlinked_jump(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBDREV) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBDREV_LINKING_MAX_Y_DISTANCE_TO_LINK) )
	{
		rval = set_DSDBR01_SMBDREV_linking_max_y_distance_to_link(value);
	}



	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMQA) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMQA_SLOPE_WINDOW_SIZE) )
	{
		rval = set_DSDBR01_SMQA_slope_window_size(value);
	}



			


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_ALL) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD) )
	{
		for(short sm = FIRST_SM_NUM; (sm <= LAST_SM_NUM) && (rval == rtype::ok); sm++)
			rval = set_DSDBR01_SMPF_max_mode_width(sm,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_ALL) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD) )
	{
		for(short sm = FIRST_SM_NUM; (sm <= LAST_SM_NUM) && (rval == rtype::ok); sm++)
			rval = set_DSDBR01_SMPF_min_mode_width(sm,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_ALL) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD) )
	{
		for(short sm = FIRST_SM_NUM; (sm <= LAST_SM_NUM) && (rval == rtype::ok); sm++)
			rval = set_DSDBR01_SMPF_max_mode_borders_removed(sm,value);
	}




	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM0) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_width(0,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM0) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_min_mode_width(0,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM0) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_borders_removed(0,value);
	}





	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM1) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_width(1,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM1) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_min_mode_width(1,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM1) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_borders_removed(1,value);
	}




	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM2) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_width(2,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM2) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_min_mode_width(2,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM2) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_borders_removed(2,value);
	}



	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM3) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_width(3,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM3) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_min_mode_width(3,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM3) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_borders_removed(3,value);
	}



	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM4) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_width(4,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM4) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_min_mode_width(4,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM4) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_borders_removed(4,value);
	}



	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM5) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_width(5,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM5) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_min_mode_width(5,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM5) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_borders_removed(5,value);
	}



	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM6) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_width(6,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM6) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_min_mode_width(6,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM6) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_borders_removed(6,value);
	}




	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM7) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_width(7,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM7) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_min_mode_width(7,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM7) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_borders_removed(7,value);
	}




	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM8) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_width(8,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM8) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_min_mode_width(8,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM8) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_borders_removed(8,value);
	}





	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM9) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_width(9,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM9) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_min_mode_width(9,value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMPF_SM9) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD) )
	{
		rval = set_DSDBR01_SMPF_max_mode_borders_removed(9,value);
	}


	return rval;
}


CCGRegValues::rtype
CCGRegValues::setBool(CCGRegEntry reg_entry, bool value)
{
	CString reg = reg_entry.reg_key_name + _T("\\") + reg_entry.reg_entry_name;
	rtype rval = rtype::ok;


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString("\\")
		+ CString(REGNAME_DSDBR01_BASE_STUB_HARDWARE_CALLS) )
	{
		rval = set_DSDBR01_BASE_stub_hardware_calls(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMDC_FORWARD_AND_REVERSE) )
	{
		rval = set_DSDBR01_OMDC_forward_and_reverse(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMDC_MEASURE_PHOTODIODE_CURRENTS) )
	{
		rval = set_DSDBR01_OMDC_measure_photodiode_currents(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMDC_WRITE_FILTERED_POWER_MAPS) )
	{
		rval = set_DSDBR01_OMDC_write_filtered_power_maps(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMDC_OVERWRITE_EXISTING_FILES) )
	{
		rval = set_DSDBR01_OMDC_overwrite_existing_files(value);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMBD_READ_BOUNDARIES_FROM_FILE) )
	{
		rval = set_DSDBR01_OMBD_read_boundaries_from_file(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMBD_WRITE_BOUNDARIES_TO_FILE) )
	{
		rval = set_DSDBR01_OMBD_write_boundaries_to_file(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_OMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_OMBD_ML_EXTEND_TO_CORNER) )
	{
		rval = set_DSDBR01_OMBD_ml_extend_to_corner(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CFREQ) + CString("\\")
		+ CString(REGNAME_DSDBR01_CFREQ_WRITE_SAMPLE_POINTS_TO_FILE) )
	{
		rval = set_DSDBR01_CFREQ_write_sample_points_to_file(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CHAR) + CString("\\")
		+ CString(REGNAME_DSDBR01_CHAR_WRITE_DUFF_POINTS_TO_FILE) )
	{
		rval = set_DSDBR01_CHAR_write_duff_points_to_file(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_CHAR) + CString("\\")
		+ CString(REGNAME_DSDBR01_CHAR_prevent_front_section_switch) )
	{
		rval = set_DSDBR01_CHAR_prevent_front_section_switch(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMDC_MEASURE_PHOTODIODE_CURRENTS) )
	{
		rval = set_DSDBR01_SMDC_measure_photodiode_currents(value);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMDC_WRITE_FILTERED_POWER_MAPS) )
	{
		rval = set_DSDBR01_SMDC_write_filtered_power_maps(value);
	}


	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMDC) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMDC_OVERWRITE_EXISTING_FILES) )
	{
		rval = set_DSDBR01_SMDC_overwrite_existing_files(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBD_READ_BOUNDARIES_FROM_FILE) )
	{
		rval = set_DSDBR01_SMBD_read_boundaries_from_file(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBD_WRITE_BOUNDARIES_TO_FILE) )
	{
		rval = set_DSDBR01_SMBD_write_boundaries_to_file(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBD_write_middle_of_upper_lines_to_file) )
	{
		rval = set_DSDBR01_SMBD_write_middle_of_upper_lines_to_file(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBD_write_middle_of_lower_lines_to_file) )
	{
		rval = set_DSDBR01_SMBD_write_middle_of_lower_lines_to_file(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBD_WRITE_DEBUG_FILES) )
	{
		rval = set_DSDBR01_SMBD_write_debug_files(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMBD) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMBD_extrapolate_to_map_edges) )
	{
		rval = set_DSDBR01_SMBD_extrapolate_to_map_edges(value);
	}

	if( reg == CString(CG_DSDBR01_BASE_REGKEY) + CString(RELATIVE_REGKEY_DSDBR01_SMQA) + CString("\\")
		+ CString(REGNAME_DSDBR01_SMQA_IGNORE_BDC_AT_FSC) )
	{
		rval = set_DSDBR01_SMQA_ignore_DBC_at_FSC(value);
	}



	return rval;
}




//
/////////////////////////////////////////////////////////////////
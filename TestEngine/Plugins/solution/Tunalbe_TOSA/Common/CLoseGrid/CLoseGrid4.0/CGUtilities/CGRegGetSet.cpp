// Copyright (c) 2006 
//
// FileName    : CGRegGetSet.cpp
// Description : Definition of get and set functions of CCGRegValues class
//
// Rev#  | Date          | Author               | Description of change
// ---------------------------------------------------------------------
// 0.1   | 04 Mar  2006  | Frank D'Arcy         | Initial Draft


#include "StdAfx.h"
#include "CGRegValues.h"
#include "CGRegDefs.h"


// Base parameters

CString
CCGRegValues::get_DSDBR01_BASE_results_directory()
{
	return _DSDBR01_BASE_results_directory;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_BASE_results_directory( CString value )
{
	rtype rval = rtype::ok;

	_DSDBR01_BASE_results_directory = value;

	return rval;
}


bool
CCGRegValues::get_DSDBR01_BASE_stub_hardware_calls()
{
	return _DSDBR01_BASE_stub_hardware_calls;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_BASE_stub_hardware_calls( bool value )
{
	rtype rval = rtype::ok;

	_DSDBR01_BASE_stub_hardware_calls = value;

	return rval;
}



// DSDBR01 Common Data Collection parameters


CString
CCGRegValues::get_DSDBR01_CMDC_gain_module_name()
{
	return _DSDBR01_CMDC_gain_module_name;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_CMDC_gain_module_name( CString value )
{
	rtype rval = rtype::ok;

	_DSDBR01_CMDC_gain_module_name = value;

	return rval;
}


CString
CCGRegValues::get_DSDBR01_CMDC_soa_module_name()
{
	return _DSDBR01_CMDC_soa_module_name;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_CMDC_soa_module_name( CString value )
{
	rtype rval = rtype::ok;

	_DSDBR01_CMDC_soa_module_name = value;

	return rval;
}



CString
CCGRegValues::get_DSDBR01_CMDC_rear_module_name()
{
	return _DSDBR01_CMDC_rear_module_name;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_CMDC_rear_module_name( CString value )
{
	rtype rval = rtype::ok;

	_DSDBR01_CMDC_rear_module_name = value;

	return rval;
}



CString
CCGRegValues::get_DSDBR01_CMDC_phase_module_name()
{
	return _DSDBR01_CMDC_phase_module_name;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_CMDC_phase_module_name( CString value )
{
	rtype rval = rtype::ok;

	_DSDBR01_CMDC_phase_module_name = value;

	return rval;
}




CString
CCGRegValues::get_DSDBR01_CMDC_front1_module_name()
{
	return _DSDBR01_CMDC_front1_module_name;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_CMDC_front1_module_name( CString value )
{
	rtype rval = rtype::ok;

	_DSDBR01_CMDC_front1_module_name = value;

	return rval;
}



CString
CCGRegValues::get_DSDBR01_CMDC_front2_module_name()
{
	return _DSDBR01_CMDC_front2_module_name;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_CMDC_front2_module_name( CString value )
{
	rtype rval = rtype::ok;

	_DSDBR01_CMDC_front2_module_name = value;

	return rval;
}



CString
CCGRegValues::get_DSDBR01_CMDC_front3_module_name()
{
	return _DSDBR01_CMDC_front3_module_name;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_CMDC_front3_module_name( CString value )
{
	rtype rval = rtype::ok;

	_DSDBR01_CMDC_front3_module_name = value;

	return rval;
}



CString
CCGRegValues::get_DSDBR01_CMDC_front4_module_name()
{
	return _DSDBR01_CMDC_front4_module_name;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_CMDC_front4_module_name( CString value )
{
	rtype rval = rtype::ok;

	_DSDBR01_CMDC_front4_module_name = value;

	return rval;
}



CString
CCGRegValues::get_DSDBR01_CMDC_front5_module_name()
{
	return _DSDBR01_CMDC_front5_module_name;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_CMDC_front5_module_name( CString value )
{
	rtype rval = rtype::ok;

	_DSDBR01_CMDC_front5_module_name = value;

	return rval;
}



CString
CCGRegValues::get_DSDBR01_CMDC_front6_module_name()
{
	return _DSDBR01_CMDC_front6_module_name;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_CMDC_front6_module_name( CString value )
{
	rtype rval = rtype::ok;

	_DSDBR01_CMDC_front6_module_name = value;

	return rval;
}



CString
CCGRegValues::get_DSDBR01_CMDC_front7_module_name()
{
	return _DSDBR01_CMDC_front7_module_name;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_CMDC_front7_module_name( CString value )
{
	rtype rval = rtype::ok;

	_DSDBR01_CMDC_front7_module_name = value;

	return rval;
}



CString
CCGRegValues::get_DSDBR01_CMDC_front8_module_name()
{
	return _DSDBR01_CMDC_front8_module_name;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_CMDC_front8_module_name( CString value )
{
	rtype rval = rtype::ok;

	_DSDBR01_CMDC_front8_module_name = value;

	return rval;
}


CString
CCGRegValues::get_DSDBR01_CMDC_306II_module_name()
{
	return _DSDBR01_CMDC_306II_module_name;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_CMDC_306II_module_name( CString value )
{
	rtype rval = rtype::ok;

	_DSDBR01_CMDC_306II_module_name = value;

	return rval;
}


int
CCGRegValues::get_DSDBR01_CMDC_306II_direct_channel()
{
	return _DSDBR01_CMDC_306II_direct_channel;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_CMDC_306II_direct_channel( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_CMDC_306II_direct_channel = value;

	return rval;
}


int
CCGRegValues::get_DSDBR01_CMDC_306II_filtered_channel()
{
	return _DSDBR01_CMDC_306II_filtered_channel;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_CMDC_306II_filtered_channel( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_CMDC_306II_filtered_channel = value;

	return rval;
}


CString
CCGRegValues::get_DSDBR01_CMDC_306EE_module_name()
{
	return _DSDBR01_CMDC_306EE_module_name;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_CMDC_306EE_module_name( CString value )
{
	rtype rval = rtype::ok;

	_DSDBR01_CMDC_306EE_module_name = value;

	return rval;
}


int
CCGRegValues::get_DSDBR01_CMDC_306EE_photodiode1_channel()
{
	return _DSDBR01_CMDC_306EE_photodiode1_channel;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_CMDC_306EE_photodiode1_channel( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_CMDC_306EE_photodiode1_channel = value;

	return rval;
}


int
CCGRegValues::get_DSDBR01_CMDC_306EE_photodiode2_channel()
{
	return _DSDBR01_CMDC_306EE_photodiode2_channel;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_CMDC_306EE_photodiode2_channel( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_CMDC_306EE_photodiode2_channel = value;

	return rval;
}

int
CCGRegValues::get_DSDBR01_CMDC_wavemeter_retries()
{
	return _DSDBR01_CMDC_wavemeter_retries;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_CMDC_wavemeter_retries( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_CMDC_wavemeter_retries = value;

	return rval;
}


// Overall Map Data Collection parameters


CString
CCGRegValues::get_DSDBR01_OMDC_ramp_direction()
{
	return _DSDBR01_OMDC_ramp_direction;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_OMDC_ramp_direction( CString value )
{
	rtype rval = rtype::ok;

	if( value != OMDC_RAMP_REAR
	 && value != OMDC_RAMP_FRONT )
	{
		rval = rtype::invalid_value;
	}
	else
	{
		_DSDBR01_OMDC_ramp_direction = value;
	}

	return rval;
}


bool
CCGRegValues::get_DSDBR01_OMDC_forward_and_reverse()
{
	return _DSDBR01_OMDC_forward_and_reverse;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_OMDC_forward_and_reverse( bool value )
{
	rtype rval = rtype::ok;

	_DSDBR01_OMDC_forward_and_reverse = value;

	return rval;
}


bool
CCGRegValues::get_DSDBR01_OMDC_measure_photodiode_currents()
{
	return _DSDBR01_OMDC_measure_photodiode_currents;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_OMDC_measure_photodiode_currents( bool value )
{
	rtype rval = rtype::ok;

	_DSDBR01_OMDC_measure_photodiode_currents = value;

	return rval;
}


bool
CCGRegValues::get_DSDBR01_OMDC_write_filtered_power_maps()
{
	return _DSDBR01_OMDC_write_filtered_power_maps;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_OMDC_write_filtered_power_maps( bool value )
{
	rtype rval = rtype::ok;

	_DSDBR01_OMDC_write_filtered_power_maps = value;

	return rval;
}


CString
CCGRegValues::get_DSDBR01_OMDC_rear_currents_abspath()
{
	return _DSDBR01_OMDC_rear_currents_abspath;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_OMDC_rear_currents_abspath( CString value )
{
	rtype rval = rtype::ok;

	_DSDBR01_OMDC_rear_currents_abspath = value;

	return rval;
}


CString
CCGRegValues::get_DSDBR01_OMDC_front_currents_abspath()
{
	return _DSDBR01_OMDC_front_currents_abspath;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_OMDC_front_currents_abspath( CString value )
{
	rtype rval = rtype::ok;

	_DSDBR01_OMDC_front_currents_abspath = value;

	return rval;
}




int
CCGRegValues::get_DSDBR01_OMDC_source_delay()
{
	return _DSDBR01_OMDC_source_delay;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_OMDC_source_delay( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_OMDC_source_delay = value;

	return rval;
}


int
CCGRegValues::get_DSDBR01_OMDC_measure_delay()
{
	return _DSDBR01_OMDC_measure_delay;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_OMDC_measure_delay( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_OMDC_measure_delay = value;

	return rval;
}



bool
CCGRegValues::get_DSDBR01_OMDC_overwrite_existing_files()
{
	return _DSDBR01_OMDC_overwrite_existing_files;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_OMDC_overwrite_existing_files( bool value )
{
	rtype rval = rtype::ok;

	_DSDBR01_OMDC_overwrite_existing_files = value;

	return rval;
}


// Overall Map Boundary Detection parameters


bool
CCGRegValues::get_DSDBR01_OMBD_read_boundaries_from_file()
{
	return _DSDBR01_OMBD_read_boundaries_from_file;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_OMBD_read_boundaries_from_file( bool value )
{
	rtype rval = rtype::ok;

	_DSDBR01_OMBD_read_boundaries_from_file = value;

	return rval;
}

bool
CCGRegValues::get_DSDBR01_OMBD_write_boundaries_to_file()
{
	return _DSDBR01_OMBD_write_boundaries_to_file;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_OMBD_write_boundaries_to_file( bool value )
{
	rtype rval = rtype::ok;

	_DSDBR01_OMBD_write_boundaries_to_file = value;

	return rval;
}

int
CCGRegValues::get_DSDBR01_OMBD_median_filter_rank()
{
	return _DSDBR01_OMBD_median_filter_rank;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_OMBD_median_filter_rank( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_OMBD_median_filter_rank = value;

	return rval;
}

double
CCGRegValues::get_DSDBR01_OMBD_max_deltaPr_in_sm()
{
	return _DSDBR01_OMBD_max_deltaPr_in_sm;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_OMBD_max_deltaPr_in_sm( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_OMBD_max_deltaPr_in_sm = value;

	return rval;
}



int
CCGRegValues::get_DSDBR01_OMBD_min_points_width_of_sm()
{
	return _DSDBR01_OMBD_min_points_width_of_sm;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_OMBD_min_points_width_of_sm( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_OMBD_min_points_width_of_sm = value;

	return rval;
}

int
CCGRegValues::get_DSDBR01_OMBD_ml_moving_ave_filter_n()
{
	return _DSDBR01_OMBD_ml_moving_ave_filter_n;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_OMBD_ml_moving_ave_filter_n( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_OMBD_ml_moving_ave_filter_n = value;

	return rval;
}


int
CCGRegValues::get_DSDBR01_OMBD_ml_min_length()
{
	return _DSDBR01_OMBD_ml_min_length;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_OMBD_ml_min_length( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_OMBD_ml_min_length = value;

	return rval;
}



bool
CCGRegValues::get_DSDBR01_OMBD_ml_extend_to_corner()
{
	return _DSDBR01_OMBD_ml_extend_to_corner;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_OMBD_ml_extend_to_corner( bool value )
{
	rtype rval = rtype::ok;

	_DSDBR01_OMBD_ml_extend_to_corner = value;

	return rval;
}

// Supermode Map collection parameters


CString
CCGRegValues::get_DSDBR01_SMDC_ramp_direction()
{
	return _DSDBR01_SMDC_ramp_direction;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMDC_ramp_direction( CString value )
{
	rtype rval = rtype::ok;

	if( value != SMDC_RAMP_MIDDLE_LINE
	 && value != SMDC_RAMP_PHASE )
	{
		rval = rtype::invalid_value;
	}
	else
	{
		_DSDBR01_SMDC_ramp_direction = value;
	}

	return rval;
}


CString
CCGRegValues::get_DSDBR01_SMDC_phase_currents_abspath()
{
	return _DSDBR01_SMDC_phase_currents_abspath;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMDC_phase_currents_abspath( CString value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMDC_phase_currents_abspath = value;

	return rval;
}


bool
CCGRegValues::get_DSDBR01_SMDC_measure_photodiode_currents()
{
	return _DSDBR01_SMDC_measure_photodiode_currents;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMDC_measure_photodiode_currents( bool value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMDC_measure_photodiode_currents = value;

	return rval;
}


bool
CCGRegValues::get_DSDBR01_SMDC_write_filtered_power_maps()
{
	return _DSDBR01_SMDC_write_filtered_power_maps;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMDC_write_filtered_power_maps( bool value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMDC_write_filtered_power_maps = value;

	return rval;
}


int
CCGRegValues::get_DSDBR01_SMDC_source_delay()
{
	return _DSDBR01_SMDC_source_delay;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMDC_source_delay( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMDC_source_delay = value;

	return rval;
}


int
CCGRegValues::get_DSDBR01_SMDC_measure_delay()
{
	return _DSDBR01_SMDC_measure_delay;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMDC_measure_delay( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMDC_measure_delay = value;

	return rval;
}



bool
CCGRegValues::get_DSDBR01_SMDC_overwrite_existing_files()
{
	return _DSDBR01_SMDC_overwrite_existing_files;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMDC_overwrite_existing_files( bool value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMDC_overwrite_existing_files = value;

	return rval;
}




// Supermode Map Boundary Detection parameters


bool
CCGRegValues::get_DSDBR01_SMBD_read_boundaries_from_file()
{
	return _DSDBR01_SMBD_read_boundaries_from_file;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBD_read_boundaries_from_file( bool value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBD_read_boundaries_from_file = value;

	return rval;
}

bool
CCGRegValues::get_DSDBR01_SMBD_write_boundaries_to_file()
{
	return _DSDBR01_SMBD_write_boundaries_to_file;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBD_write_boundaries_to_file( bool value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBD_write_boundaries_to_file = value;

	return rval;
}


bool
CCGRegValues::get_DSDBR01_SMBD_write_middle_of_lower_lines_to_file()
{
	return _DSDBR01_SMBD_write_middle_of_lower_lines_to_file;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBD_write_middle_of_lower_lines_to_file( bool value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBD_write_middle_of_lower_lines_to_file = value;

	return rval;
}

bool
CCGRegValues::get_DSDBR01_SMBD_write_middle_of_upper_lines_to_file()
{
	return _DSDBR01_SMBD_write_middle_of_upper_lines_to_file;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBD_write_middle_of_upper_lines_to_file( bool value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBD_write_middle_of_upper_lines_to_file = value;

	return rval;
}

bool
CCGRegValues::get_DSDBR01_SMBD_write_debug_files()
{
	return _DSDBR01_SMBD_write_debug_files;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBD_write_debug_files( bool value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBD_write_debug_files = value;

	return rval;
}

bool
CCGRegValues::get_DSDBR01_SMBD_extrapolate_to_map_edges()
{
	return _DSDBR01_SMBD_extrapolate_to_map_edges;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBD_extrapolate_to_map_edges( bool value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBD_extrapolate_to_map_edges = value;

	return rval;
}


int
CCGRegValues::get_DSDBR01_SMBD_median_filter_rank()
{
	return _DSDBR01_SMBD_median_filter_rank;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBD_median_filter_rank( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBD_median_filter_rank = value;

	return rval;
}

double
CCGRegValues::get_DSDBR01_SMBD_max_deltaPr_in_lm()
{
	return _DSDBR01_SMBD_max_deltaPr_in_lm;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBD_max_deltaPr_in_lm( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBD_max_deltaPr_in_lm = value;

	return rval;
}



int
CCGRegValues::get_DSDBR01_SMBD_min_points_width_of_lm()
{
	return _DSDBR01_SMBD_min_points_width_of_lm;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBD_min_points_width_of_lm( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBD_min_points_width_of_lm = value;

	return rval;
}


int
CCGRegValues::get_DSDBR01_SMBD_max_points_width_of_lm()
{
	return _DSDBR01_SMBD_max_points_width_of_lm;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBD_max_points_width_of_lm( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBD_max_points_width_of_lm = value;

	return rval;
}


int
CCGRegValues::get_DSDBR01_SMBD_ml_moving_ave_filter_n()
{
	return _DSDBR01_SMBD_ml_moving_ave_filter_n;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBD_ml_moving_ave_filter_n( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBD_ml_moving_ave_filter_n = value;

	return rval;
}


int
CCGRegValues::get_DSDBR01_SMBD_ml_min_length()
{
	return _DSDBR01_SMBD_ml_min_length;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBD_ml_min_length( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBD_ml_min_length = value;

	return rval;
}


double
CCGRegValues::get_DSDBR01_SMBD_vertical_percent_shift()
{
	return _DSDBR01_SMBD_vertical_percent_shift;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBD_vertical_percent_shift( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBD_vertical_percent_shift = value;

	return rval;
}


double
CCGRegValues::get_DSDBR01_SMBD_zprc_hysteresis_upper_threshold()
{
	return _DSDBR01_SMBD_zprc_hysteresis_upper_threshold;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBD_zprc_hysteresis_upper_threshold( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBD_zprc_hysteresis_upper_threshold = value;

	return rval;
}


double
CCGRegValues::get_DSDBR01_SMBD_zprc_hysteresis_lower_threshold()
{
	return _DSDBR01_SMBD_zprc_hysteresis_lower_threshold;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBD_zprc_hysteresis_lower_threshold( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBD_zprc_hysteresis_lower_threshold = value;

	return rval;
}


double
CCGRegValues::get_DSDBR01_SMBD_zprc_max_hysteresis_rise()
{
	return _DSDBR01_SMBD_zprc_max_hysteresis_rise;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBD_zprc_max_hysteresis_rise( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBD_zprc_max_hysteresis_rise = value;

	return rval;
}


int
CCGRegValues::get_DSDBR01_SMBD_zprc_min_points_separation()
{
	return _DSDBR01_SMBD_zprc_min_points_separation;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBD_zprc_min_points_separation( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBD_zprc_min_points_separation = value;

	return rval;
}


int
CCGRegValues::get_DSDBR01_SMBD_zprc_max_points_to_reverse_jump()
{
	return _DSDBR01_SMBD_zprc_max_points_to_reverse_jump;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBD_zprc_max_points_to_reverse_jump( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBD_zprc_max_points_to_reverse_jump = value;

	return rval;
}


int
CCGRegValues::get_DSDBR01_SMBD_zprc_max_hysteresis_points()
{
	return _DSDBR01_SMBD_zprc_max_hysteresis_points;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBD_zprc_max_hysteresis_points( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBD_zprc_max_hysteresis_points = value;

	return rval;
}


CString
CCGRegValues::get_DSDBR01_SMBD_ramp_abspath()
{
	return _DSDBR01_SMBD_ramp_abspath;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBD_ramp_abspath( CString value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBD_ramp_abspath = value;

	return rval;
}


// DSDBR01 Forward Supermode Map Boundary Detection parameters


int
CCGRegValues::get_DSDBR01_SMBDFWD_I_ramp_boundaries_max()
{
	return _DSDBR01_SMBDFWD_I_ramp_boundaries_max;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBDFWD_I_ramp_boundaries_max( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBDFWD_I_ramp_boundaries_max = value;

	return rval;
}


int
CCGRegValues::get_DSDBR01_SMBDFWD_boundaries_min_separation()
{
	return _DSDBR01_SMBDFWD_boundaries_min_separation;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBDFWD_boundaries_min_separation( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBDFWD_boundaries_min_separation = value;

	return rval;
}


double
CCGRegValues::get_DSDBR01_SMBDFWD_min_d2Pr_dI2_zero_cross_jump_size()
{
	return _DSDBR01_SMBDFWD_min_d2Pr_dI2_zero_cross_jump_size;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBDFWD_min_d2Pr_dI2_zero_cross_jump_size( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBDFWD_min_d2Pr_dI2_zero_cross_jump_size = value;

	return rval;
}


double
CCGRegValues::get_DSDBR01_SMBDFWD_max_sum_of_squared_distances_from_Pr_I_line()
{
	return _DSDBR01_SMBDFWD_max_sum_of_squared_distances_from_Pr_I_line;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBDFWD_max_sum_of_squared_distances_from_Pr_I_line( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBDFWD_max_sum_of_squared_distances_from_Pr_I_line = value;

	return rval;
}


double
CCGRegValues::get_DSDBR01_SMBDFWD_min_slope_change_of_Pr_I_lines()
{
	return _DSDBR01_SMBDFWD_min_slope_change_of_Pr_I_lines;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBDFWD_min_slope_change_of_Pr_I_lines( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBDFWD_min_slope_change_of_Pr_I_lines = value;

	return rval;
}


int
CCGRegValues::get_DSDBR01_SMBDFWD_linking_max_distance_between_linked_jumps()
{
	return _DSDBR01_SMBDFWD_linking_max_distance_between_linked_jumps;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBDFWD_linking_max_distance_between_linked_jumps( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBDFWD_linking_max_distance_between_linked_jumps = value;

	return rval;
}


double
CCGRegValues::get_DSDBR01_SMBDFWD_linking_min_sharpness()
{
	return _DSDBR01_SMBDFWD_linking_min_sharpness;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBDFWD_linking_min_sharpness( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBDFWD_linking_min_sharpness = value;

	return rval;
}

double
CCGRegValues::get_DSDBR01_SMBDFWD_linking_max_sharpness()
{
	return _DSDBR01_SMBDFWD_linking_max_sharpness;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBDFWD_linking_max_sharpness( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBDFWD_linking_max_sharpness = value;

	return rval;
}


int
CCGRegValues::get_DSDBR01_SMBDFWD_linking_max_gap_between_double_lines()
{
	return _DSDBR01_SMBDFWD_linking_max_gap_between_double_lines;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBDFWD_linking_max_gap_between_double_lines( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBDFWD_linking_max_gap_between_double_lines = value;

	return rval;
}

int
CCGRegValues::get_DSDBR01_SMBDFWD_linking_min_gap_between_double_lines()
{
	return _DSDBR01_SMBDFWD_linking_min_gap_between_double_lines;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBDFWD_linking_min_gap_between_double_lines( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBDFWD_linking_min_gap_between_double_lines = value;

	return rval;
}

int
CCGRegValues::get_DSDBR01_SMBDFWD_linking_near_bottom_row()
{
	return _DSDBR01_SMBDFWD_linking_near_bottom_row;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBDFWD_linking_near_bottom_row( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBDFWD_linking_near_bottom_row = value;

	return rval;
}


double
CCGRegValues::get_DSDBR01_SMBDFWD_linking_sharpness_near_bottom_row()
{
	return _DSDBR01_SMBDFWD_linking_sharpness_near_bottom_row;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBDFWD_linking_sharpness_near_bottom_row( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBDFWD_linking_sharpness_near_bottom_row = value;

	return rval;
}	
	

int
CCGRegValues::get_DSDBR01_SMBDFWD_linking_max_distance_near_bottom_row()
{
	return _DSDBR01_SMBDFWD_linking_max_distance_near_bottom_row;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBDFWD_linking_max_distance_near_bottom_row( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBDFWD_linking_max_distance_near_bottom_row = value;

	return rval;
}


int
CCGRegValues::get_DSDBR01_SMBDFWD_linking_max_distance_to_single_unlinked_jump()
{
	return _DSDBR01_SMBDFWD_linking_max_distance_to_single_unlinked_jump;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBDFWD_linking_max_distance_to_single_unlinked_jump( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBDFWD_linking_max_distance_to_single_unlinked_jump = value;

	return rval;
}

int
CCGRegValues::get_DSDBR01_SMBDFWD_linking_max_y_distance_to_link()
{
	return _DSDBR01_SMBDFWD_linking_max_y_distance_to_link;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBDFWD_linking_max_y_distance_to_link( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBDFWD_linking_max_y_distance_to_link = value;

	return rval;
}


double
CCGRegValues::get_DSDBR01_SMBDFWD_linking_sharpness_ramp_split()
{
	return _DSDBR01_SMBDFWD_linking_sharpness_ramp_split;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBDFWD_linking_sharpness_ramp_split( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBDFWD_linking_sharpness_ramp_split = value;

	return rval;
}	



// DSDBR01 Reverse Supermode Map Boundary Detection parameters


int
CCGRegValues::get_DSDBR01_SMBDREV_I_ramp_boundaries_max()
{
	return _DSDBR01_SMBDREV_I_ramp_boundaries_max;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBDREV_I_ramp_boundaries_max( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBDREV_I_ramp_boundaries_max = value;

	return rval;
}


int
CCGRegValues::get_DSDBR01_SMBDREV_boundaries_min_separation()
{
	return _DSDBR01_SMBDREV_boundaries_min_separation;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBDREV_boundaries_min_separation( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBDREV_boundaries_min_separation = value;

	return rval;
}


double
CCGRegValues::get_DSDBR01_SMBDREV_min_d2Pr_dI2_zero_cross_jump_size()
{
	return _DSDBR01_SMBDREV_min_d2Pr_dI2_zero_cross_jump_size;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBDREV_min_d2Pr_dI2_zero_cross_jump_size( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBDREV_min_d2Pr_dI2_zero_cross_jump_size = value;

	return rval;
}


double
CCGRegValues::get_DSDBR01_SMBDREV_max_sum_of_squared_distances_from_Pr_I_line()
{
	return _DSDBR01_SMBDREV_max_sum_of_squared_distances_from_Pr_I_line;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBDREV_max_sum_of_squared_distances_from_Pr_I_line( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBDREV_max_sum_of_squared_distances_from_Pr_I_line = value;

	return rval;
}


double
CCGRegValues::get_DSDBR01_SMBDREV_min_slope_change_of_Pr_I_lines()
{
	return _DSDBR01_SMBDREV_min_slope_change_of_Pr_I_lines;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBDREV_min_slope_change_of_Pr_I_lines( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBDREV_min_slope_change_of_Pr_I_lines = value;

	return rval;
}



int
CCGRegValues::get_DSDBR01_SMBDREV_linking_max_distance_between_linked_jumps()
{
	return _DSDBR01_SMBDREV_linking_max_distance_between_linked_jumps;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBDREV_linking_max_distance_between_linked_jumps( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBDREV_linking_max_distance_between_linked_jumps = value;

	return rval;
}


double
CCGRegValues::get_DSDBR01_SMBDREV_linking_min_sharpness()
{
	return _DSDBR01_SMBDREV_linking_min_sharpness;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBDREV_linking_min_sharpness( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBDREV_linking_min_sharpness = value;

	return rval;
}

double
CCGRegValues::get_DSDBR01_SMBDREV_linking_max_sharpness()
{
	return _DSDBR01_SMBDREV_linking_max_sharpness;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBDREV_linking_max_sharpness( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBDREV_linking_max_sharpness = value;

	return rval;
}


int
CCGRegValues::get_DSDBR01_SMBDREV_linking_max_gap_between_double_lines()
{
	return _DSDBR01_SMBDREV_linking_max_gap_between_double_lines;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBDREV_linking_max_gap_between_double_lines( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBDREV_linking_max_gap_between_double_lines = value;

	return rval;
}

int
CCGRegValues::get_DSDBR01_SMBDREV_linking_min_gap_between_double_lines()
{
	return _DSDBR01_SMBDREV_linking_min_gap_between_double_lines;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBDREV_linking_min_gap_between_double_lines( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBDREV_linking_min_gap_between_double_lines = value;

	return rval;
}

int
CCGRegValues::get_DSDBR01_SMBDREV_linking_near_bottom_row()
{
	return _DSDBR01_SMBDREV_linking_near_bottom_row;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBDREV_linking_near_bottom_row( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBDREV_linking_near_bottom_row = value;

	return rval;
}


double
CCGRegValues::get_DSDBR01_SMBDREV_linking_sharpness_near_bottom_row()
{
	return _DSDBR01_SMBDREV_linking_sharpness_near_bottom_row;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBDREV_linking_sharpness_near_bottom_row( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBDREV_linking_sharpness_near_bottom_row = value;

	return rval;
}	
	

int
CCGRegValues::get_DSDBR01_SMBDREV_linking_max_distance_near_bottom_row()
{
	return _DSDBR01_SMBDREV_linking_max_distance_near_bottom_row;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBDREV_linking_max_distance_near_bottom_row( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBDREV_linking_max_distance_near_bottom_row = value;

	return rval;
}


int
CCGRegValues::get_DSDBR01_SMBDREV_linking_max_distance_to_single_unlinked_jump()
{
	return _DSDBR01_SMBDREV_linking_max_distance_to_single_unlinked_jump;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBDREV_linking_max_distance_to_single_unlinked_jump( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBDREV_linking_max_distance_to_single_unlinked_jump = value;

	return rval;
}

int
CCGRegValues::get_DSDBR01_SMBDREV_linking_max_y_distance_to_link()
{
	return _DSDBR01_SMBDREV_linking_max_y_distance_to_link;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBDREV_linking_max_y_distance_to_link( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBDREV_linking_max_y_distance_to_link = value;

	return rval;
}

double
CCGRegValues::get_DSDBR01_SMBDREV_linking_sharpness_ramp_split()
{
	return _DSDBR01_SMBDREV_linking_sharpness_ramp_split;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMBDREV_linking_sharpness_ramp_split( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMBDREV_linking_sharpness_ramp_split = value;

	return rval;
}



// DSDBR01 Overall Map Quantitative Analysis parameters
int
CCGRegValues::get_DSDBR01_OMQA_slope_window_size()
{
	return _DSDBR01_OMQA_slope_window_size;
}
CCGRegValues::rtype
CCGRegValues::set_DSDBR01_OMQA_slope_window_size( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_OMQA_slope_window_size = value;

	return rval;
}

double
CCGRegValues::get_DSDBR01_OMQA_modal_distortion_min_x()
{
	return _DSDBR01_OMQA_modal_distortion_min_x;
}
CCGRegValues::rtype
CCGRegValues::set_DSDBR01_OMQA_modal_distortion_min_x( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_OMQA_modal_distortion_min_x = value;

	return rval;
}

double
CCGRegValues::get_DSDBR01_OMQA_modal_distortion_max_x()
{
	return _DSDBR01_OMQA_modal_distortion_max_x;
}
CCGRegValues::rtype
CCGRegValues::set_DSDBR01_OMQA_modal_distortion_max_x( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_OMQA_modal_distortion_max_x = value;

	return rval;
}

double
CCGRegValues::get_DSDBR01_OMQA_modal_distortion_min_y()
{
	return _DSDBR01_OMQA_modal_distortion_min_y;
}
CCGRegValues::rtype
CCGRegValues::set_DSDBR01_OMQA_modal_distortion_min_y( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_OMQA_modal_distortion_min_y = value;

	return rval;
}

////////////////////////////////////////////////////////////////////////////////
// DSDBR01 Overall Map Quantitative Analysis parameters
int
CCGRegValues::get_DSDBR01_SMQA_slope_window_size()
{
	return _DSDBR01_SMQA_slope_window_size;
}
CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMQA_slope_window_size( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMQA_slope_window_size = value;

	return rval;
}

double
CCGRegValues::get_DSDBR01_SMQA_modal_distortion_min_x()
{
	return _DSDBR01_SMQA_modal_distortion_min_x;
}
CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMQA_modal_distortion_min_x( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMQA_modal_distortion_min_x = value;

	return rval;
}

double
CCGRegValues::get_DSDBR01_SMQA_modal_distortion_max_x()
{
	return _DSDBR01_SMQA_modal_distortion_max_x;
}
CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMQA_modal_distortion_max_x( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMQA_modal_distortion_max_x = value;

	return rval;
}

double
CCGRegValues::get_DSDBR01_SMQA_modal_distortion_min_y()
{
	return _DSDBR01_SMQA_modal_distortion_min_y;
}
CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMQA_modal_distortion_min_y( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMQA_modal_distortion_min_y = value;

	return rval;
}
//GDM 31/10/06 additonal settings for modewidth and hysteresis analysis windows
//Mode Width Analysis window
double
CCGRegValues::get_DSDBR01_SMQA_mode_width_analysis_min_x()
{
	return _DSDBR01_SMQA_mode_width_analysis_min_x;
}
CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMQA_mode_width_analysis_min_x( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMQA_mode_width_analysis_min_x = value;

	return rval;
}

double
CCGRegValues::get_DSDBR01_SMQA_mode_width_analysis_max_x()
{
	return _DSDBR01_SMQA_mode_width_analysis_max_x;
}
CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMQA_mode_width_analysis_max_x( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMQA_mode_width_analysis_max_x = value;

	return rval;
}

//Hysteresis Analysis Window
double
CCGRegValues::get_DSDBR01_SMQA_hysteresis_analysis_min_x()
{
	return _DSDBR01_SMQA_hysteresis_analysis_min_x;
}
CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMQA_hysteresis_analysis_min_x( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMQA_hysteresis_analysis_min_x = value;

	return rval;
}

double
CCGRegValues::get_DSDBR01_SMQA_hysteresis_analysis_max_x()
{
	return _DSDBR01_SMQA_hysteresis_analysis_max_x;
}
CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMQA_hysteresis_analysis_max_x( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMQA_hysteresis_analysis_max_x = value;

	return rval;
}

//GDM

bool
CCGRegValues::get_DSDBR01_SMQA_ignore_DBC_at_FSC()
{
	return _DSDBR01_SMQA_ignore_DBC_at_FSC;
}
CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMQA_ignore_DBC_at_FSC( bool value )
{
	rtype rval = rtype::ok;

	_DSDBR01_SMQA_ignore_DBC_at_FSC = value;

	return rval;
}



////////////////////////////////////////////////////////////////////////////////
///
//
double
CCGRegValues::get_DSDBR01_OMPF_max_middle_line_rms_value()
{
	return _DSDBR01_OMPF_max_middle_line_rms_value;
}
CCGRegValues::rtype
CCGRegValues::set_DSDBR01_OMPF_max_middle_line_rms_value( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_OMPF_max_middle_line_rms_value= value;

	return rval;
}

double
CCGRegValues::get_DSDBR01_OMPF_min_middle_line_slope()
{
	return _DSDBR01_OMPF_min_middle_line_slope;
}
CCGRegValues::rtype
CCGRegValues::set_DSDBR01_OMPF_min_middle_line_slope( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_OMPF_min_middle_line_slope= value;

	return rval;
}

double
CCGRegValues::get_DSDBR01_OMPF_max_middle_line_slope()
{
	return _DSDBR01_OMPF_max_middle_line_slope;
}
CCGRegValues::rtype
CCGRegValues::set_DSDBR01_OMPF_max_middle_line_slope( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_OMPF_max_middle_line_slope= value;

	return rval;
}

double
CCGRegValues::get_DSDBR01_OMPF_max_pr_gap()
{
	return _DSDBR01_OMPF_max_pr_gap;
}
CCGRegValues::rtype
CCGRegValues::set_DSDBR01_OMPF_max_pr_gap( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_OMPF_max_pr_gap = value;

	return rval;
}

double
CCGRegValues::get_DSDBR01_OMPF_max_lower_pr()
{
	return _DSDBR01_OMPF_max_lower_pr;
}
CCGRegValues::rtype
CCGRegValues::set_DSDBR01_OMPF_max_lower_pr( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_OMPF_max_lower_pr = value;

	return rval;
}

double
CCGRegValues::get_DSDBR01_OMPF_min_upper_pr()
{
	return _DSDBR01_OMPF_min_upper_pr;
}
CCGRegValues::rtype
CCGRegValues::set_DSDBR01_OMPF_min_upper_pr( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_OMPF_min_upper_pr = value;

	return rval;
}

int
CCGRegValues::get_DSDBR01_OMPF_min_mode_width_threshold()
{
	return _DSDBR01_OMPF_min_mode_width_threshold;
}
CCGRegValues::rtype
CCGRegValues::set_DSDBR01_OMPF_min_mode_width_threshold( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_OMPF_min_mode_width_threshold = value;

	return rval;
}

int
CCGRegValues::
get_DSDBR01_OMPF_max_mode_borders_removed_threshold()
{
	return _DSDBR01_OMPF_max_mode_borders_removed_threshold;
}
CCGRegValues::rtype
CCGRegValues::set_DSDBR01_OMPF_max_mode_borders_removed_threshold( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_OMPF_max_mode_borders_removed_threshold = value;

	return rval;
}

int
CCGRegValues::get_DSDBR01_OMPF_max_mode_width_threshold()
{
	return _DSDBR01_OMPF_max_mode_width_threshold;
}
CCGRegValues::rtype
CCGRegValues::set_DSDBR01_OMPF_max_mode_width_threshold( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_OMPF_max_mode_width_threshold = value;

	return rval;
}

double
CCGRegValues::get_DSDBR01_OMPF_max_mode_bdc_area_threshold()
{
	return _DSDBR01_OMPF_max_mode_bdc_area_threshold;
}
CCGRegValues::rtype
CCGRegValues::set_DSDBR01_OMPF_max_mode_bdc_area_threshold( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_OMPF_max_mode_bdc_area_threshold = value;

	return rval;
}

double
CCGRegValues::get_DSDBR01_OMPF_max_mode_bdc_area_x_length_threshold()
{
	return _DSDBR01_OMPF_max_mode_bdc_area_x_length_threshold;
}
CCGRegValues::rtype
CCGRegValues::set_DSDBR01_OMPF_max_mode_bdc_area_x_length_threshold( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_OMPF_max_mode_bdc_area_x_length_threshold = value;

	return rval;
}

double
CCGRegValues::get_DSDBR01_OMPF_sum_mode_bdc_areas_threshold()
{
	return _DSDBR01_OMPF_sum_mode_bdc_areas_threshold;
}
CCGRegValues::rtype
CCGRegValues::set_DSDBR01_OMPF_sum_mode_bdc_areas_threshold( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_OMPF_sum_mode_bdc_areas_threshold = value;

	return rval;
}

double
CCGRegValues::get_DSDBR01_OMPF_num_mode_bdc_areas_threshold()
{
	return _DSDBR01_OMPF_num_mode_bdc_areas_threshold;
}
CCGRegValues::rtype
CCGRegValues::set_DSDBR01_OMPF_num_mode_bdc_areas_threshold( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_OMPF_num_mode_bdc_areas_threshold = value;

	return rval;
}

double
CCGRegValues::get_DSDBR01_OMPF_modal_distortion_angle_threshold()
{
	return _DSDBR01_OMPF_modal_distortion_angle_threshold;
}
CCGRegValues::rtype
CCGRegValues::set_DSDBR01_OMPF_modal_distortion_angle_threshold( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_OMPF_modal_distortion_angle_threshold = value;

	return rval;
}

double
CCGRegValues::get_DSDBR01_OMPF_modal_distortion_angle_x_pos_threshold()
{
	return _DSDBR01_OMPF_modal_distortion_angle_x_pos_threshold;
}
CCGRegValues::rtype
CCGRegValues::set_DSDBR01_OMPF_modal_distortion_angle_x_pos_threshold( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_OMPF_modal_distortion_angle_x_pos_threshold = value;

	return rval;
}

double
CCGRegValues::get_DSDBR01_OMPF_max_mode_line_slope_threshold()
{
	return _DSDBR01_OMPF_max_mode_line_slope_threshold;
}
CCGRegValues::rtype
CCGRegValues::set_DSDBR01_OMPF_max_mode_line_slope_threshold( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_OMPF_max_mode_line_slope_threshold = value;

	return rval;
}


double
CCGRegValues::get_DSDBR01_OMPF_min_mode_line_slope_threshold()
{
	return _DSDBR01_OMPF_min_mode_line_slope_threshold;
}
CCGRegValues::rtype
CCGRegValues::set_DSDBR01_OMPF_min_mode_line_slope_threshold( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_OMPF_min_mode_line_slope_threshold = value;

	return rval;
}
//
////////////////////////////////////////////////////////////////////////////////

// DSDBR01 Coarse Frequency parameters

CString
CCGRegValues::get_DSDBR01_CFREQ_poly_coeffs_abspath()
{
	return _DSDBR01_CFREQ_poly_coeffs_abspath;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_CFREQ_poly_coeffs_abspath( CString value )
{
	rtype rval = rtype::ok;

	_DSDBR01_CFREQ_poly_coeffs_abspath = value;

	return rval;
}


int
CCGRegValues::get_DSDBR01_CFREQ_num_freq_meas_per_op()
{
	return _DSDBR01_CFREQ_num_freq_meas_per_op;
}
CCGRegValues::rtype
CCGRegValues::set_DSDBR01_CFREQ_num_freq_meas_per_op( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_CFREQ_num_freq_meas_per_op= value;

	return rval;
}



int
CCGRegValues::get_DSDBR01_CFREQ_num_power_ratio_meas_per_op()
{
	return _DSDBR01_CFREQ_num_power_ratio_meas_per_op;
}
CCGRegValues::rtype
CCGRegValues::set_DSDBR01_CFREQ_num_power_ratio_meas_per_op( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_CFREQ_num_power_ratio_meas_per_op = value;

	return rval;
}


double
CCGRegValues::get_DSDBR01_CFREQ_max_stable_freq_error()
{
	return _DSDBR01_CFREQ_max_stable_freq_error;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_CFREQ_max_stable_freq_error( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_CFREQ_max_stable_freq_error = value;

	return rval;
}

int
CCGRegValues::get_DSDBR01_CFREQ_sample_point_settle_time()
{
	return _DSDBR01_CFREQ_sample_point_settle_time;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_CFREQ_sample_point_settle_time( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_CFREQ_sample_point_settle_time = value;

	return rval;
}

bool
CCGRegValues::get_DSDBR01_CFREQ_write_sample_points_to_file()
{
	return _DSDBR01_CFREQ_write_sample_points_to_file;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_CFREQ_write_sample_points_to_file( bool value )
{
	rtype rval = rtype::ok;

	_DSDBR01_CFREQ_write_sample_points_to_file = value;

	return rval;
}



double
CCGRegValues::get_DSDBR01_ITUGRID_centre_freq()
{
	return _DSDBR01_ITUGRID_centre_freq;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_ITUGRID_centre_freq( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_ITUGRID_centre_freq = value;

	return rval;
}

double
CCGRegValues::get_DSDBR01_ITUGRID_step_freq()
{
	return _DSDBR01_ITUGRID_step_freq;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_ITUGRID_step_freq( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_ITUGRID_step_freq = value;

	return rval;
}

int
CCGRegValues::get_DSDBR01_ITUGRID_channel_count()
{
	return _DSDBR01_ITUGRID_channel_count;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_ITUGRID_channel_count( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_ITUGRID_channel_count = value;

	return rval;
}

CString
CCGRegValues::get_DSDBR01_ITUGRID_abspath()
{
	return _DSDBR01_ITUGRID_abspath;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_ITUGRID_abspath( CString value )
{
	rtype rval = rtype::ok;

	_DSDBR01_ITUGRID_abspath = value;

	return rval;
}

CString
CCGRegValues::get_DSDBR01_ESTIMATESITUGRID_abspath()
{
	return _DSDBR01_ESTIMATESITUGRID_abspath;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_ESTIMATESITUGRID_abspath( CString value )
{
	rtype rval = rtype::ok;

	_DSDBR01_ESTIMATESITUGRID_abspath = value;

	return rval;
}

int
CCGRegValues::get_DSDBR01_CHAR_num_freq_meas_per_op()
{
	return _DSDBR01_CHAR_num_freq_meas_per_op;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_CHAR_num_freq_meas_per_op( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_CHAR_num_freq_meas_per_op = value;

	return rval;
}


double
CCGRegValues::get_DSDBR01_CHAR_freq_accuracy_of_wavemeter()
{
	return _DSDBR01_CHAR_freq_accuracy_of_wavemeter;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_CHAR_freq_accuracy_of_wavemeter( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_CHAR_freq_accuracy_of_wavemeter = value;

	return rval;
}

int
CCGRegValues::get_DSDBR01_CHAR_op_settle_time()
{
	return _DSDBR01_CHAR_op_settle_time;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_CHAR_op_settle_time( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_CHAR_op_settle_time = value;

	return rval;
}

int
CCGRegValues::get_DSDBR01_CHAR_itusearch_scheme()
{
	return _DSDBR01_CHAR_itusearch_scheme;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_CHAR_itusearch_scheme( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_CHAR_itusearch_scheme = value;

	return rval;
}

int
CCGRegValues::get_DSDBR01_CHAR_itusearch_nextguess()
{
	return _DSDBR01_CHAR_itusearch_nextguess;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_CHAR_itusearch_nextguess( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_CHAR_itusearch_nextguess = value;

	return rval;
}

int
CCGRegValues::get_DSDBR01_CHAR_itusearch_resamples()
{
	return _DSDBR01_CHAR_itusearch_resamples;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_CHAR_itusearch_resamples( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_CHAR_itusearch_resamples = value;

	return rval;
}

int
CCGRegValues::get_DSDBR01_CHAR_num_power_ratio_meas_per_op()
{
	return _DSDBR01_CHAR_num_power_ratio_meas_per_op;
}
CCGRegValues::rtype
CCGRegValues::set_DSDBR01_CHAR_num_power_ratio_meas_per_op( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_CHAR_num_power_ratio_meas_per_op = value;

	return rval;
}


double
CCGRegValues::get_DSDBR01_CHAR_max_stable_freq_error()
{
	return _DSDBR01_CHAR_max_stable_freq_error;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_CHAR_max_stable_freq_error( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_CHAR_max_stable_freq_error = value;

	return rval;
}

bool
CCGRegValues::get_DSDBR01_CHAR_write_duff_points_to_file()
{
	return _DSDBR01_CHAR_write_duff_points_to_file;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_CHAR_write_duff_points_to_file( bool value )
{
	rtype rval = rtype::ok;

	_DSDBR01_CHAR_write_duff_points_to_file = value;

	return rval;
}

bool
CCGRegValues::get_DSDBR01_CHAR_prevent_front_section_switch()
{
	return _DSDBR01_CHAR_prevent_front_section_switch;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_CHAR_prevent_front_section_switch( bool value )
{
	rtype rval = rtype::ok;

	_DSDBR01_CHAR_prevent_front_section_switch = value;

	return rval;
}

CString
CCGRegValues::get_DSDBR01_CHAR_fakefreq_Iramp_module_name()
{
	return _DSDBR01_CHAR_fakefreq_Iramp_module_name;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_CHAR_fakefreq_Iramp_module_name( CString value )
{
	rtype rval = rtype::ok;

	_DSDBR01_CHAR_fakefreq_Iramp_module_name = value;

	return rval;
}

CString
CCGRegValues::get_DSDBR01_CHAR_Iramp_vs_fakefreq_abspath()
{
	return _DSDBR01_CHAR_Iramp_vs_fakefreq_abspath;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_CHAR_Iramp_vs_fakefreq_abspath( CString value )
{
	rtype rval = rtype::ok;

	_DSDBR01_CHAR_Iramp_vs_fakefreq_abspath = value;

	return rval;
}

double
CCGRegValues::get_DSDBR01_CHAR_fakefreq_max_error()
{
	return _DSDBR01_CHAR_fakefreq_max_error;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_CHAR_fakefreq_max_error( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_CHAR_fakefreq_max_error = value;

	return rval;
}




int
CCGRegValues::get_DSDBR01_CHARTEC_time_window_size()
{
	return _DSDBR01_CHARTEC_time_window_size;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_CHARTEC_time_window_size( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_CHARTEC_time_window_size = value;

	return rval;
}

double
CCGRegValues::get_DSDBR01_CHARTEC_max_temp_deviation()
{
	return _DSDBR01_CHARTEC_max_temp_deviation;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_CHARTEC_max_temp_deviation( double value )
{
	rtype rval = rtype::ok;

	_DSDBR01_CHARTEC_max_temp_deviation = value;

	return rval;
}

int
CCGRegValues::get_DSDBR01_CHARTEC_num_temp_readings()
{
	return _DSDBR01_CHARTEC_num_temp_readings;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_CHARTEC_num_temp_readings( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_CHARTEC_num_temp_readings = value;

	return rval;
}

int
CCGRegValues::get_DSDBR01_CHARTEC_max_settle_time()
{
	return _DSDBR01_CHARTEC_max_settle_time;
}

CCGRegValues::rtype
CCGRegValues::set_DSDBR01_CHARTEC_max_settle_time( int value )
{
	rtype rval = rtype::ok;

	_DSDBR01_CHARTEC_max_settle_time = value;

	return rval;
}




//////////////////////////////////////////////////////////////////
///
//	Supermode Map Pass/Fail Thresholds
//
int
CCGRegValues::get_DSDBR01_SMPF_max_mode_width(short smNumber)
{
	int maxModeWidth = 0;

	if(smNumber == 0)
		maxModeWidth = _DSDBR01_SMPF_SM0_max_mode_width;
	else if(smNumber == 1)
		maxModeWidth = _DSDBR01_SMPF_SM1_max_mode_width;
	else if(smNumber == 2)
		maxModeWidth = _DSDBR01_SMPF_SM2_max_mode_width;
	else if(smNumber == 3)
		maxModeWidth = _DSDBR01_SMPF_SM3_max_mode_width;
	else if(smNumber == 4)
		maxModeWidth = _DSDBR01_SMPF_SM4_max_mode_width;
	else if(smNumber == 5)
		maxModeWidth = _DSDBR01_SMPF_SM5_max_mode_width;
	else if(smNumber == 6)
		maxModeWidth = _DSDBR01_SMPF_SM6_max_mode_width;
	else if(smNumber == 7)
		maxModeWidth = _DSDBR01_SMPF_SM7_max_mode_width;
	else if(smNumber == 8)
		maxModeWidth = _DSDBR01_SMPF_SM8_max_mode_width;
	else if(smNumber == 9)
		maxModeWidth = _DSDBR01_SMPF_SM9_max_mode_width;

	return maxModeWidth;
}
CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMPF_max_mode_width(short smNumber, int value)
{
	rtype rval = rtype::ok;

	if(smNumber == 0)
		_DSDBR01_SMPF_SM0_max_mode_width = value;
	else if(smNumber == 1)
		_DSDBR01_SMPF_SM1_max_mode_width = value;
	else if(smNumber == 2)
		_DSDBR01_SMPF_SM2_max_mode_width = value;
	else if(smNumber == 3)
		_DSDBR01_SMPF_SM3_max_mode_width = value;
	else if(smNumber == 4)
		_DSDBR01_SMPF_SM4_max_mode_width = value;
	else if(smNumber == 5)
		_DSDBR01_SMPF_SM5_max_mode_width = value;
	else if(smNumber == 6)
		_DSDBR01_SMPF_SM6_max_mode_width = value;
	else if(smNumber == 7)
		_DSDBR01_SMPF_SM7_max_mode_width = value;
	else if(smNumber == 8)
		_DSDBR01_SMPF_SM8_max_mode_width = value;
	else if(smNumber == 9)
		_DSDBR01_SMPF_SM9_max_mode_width = value;

	return rval;
}

int
CCGRegValues::get_DSDBR01_SMPF_min_mode_width(short smNumber)
{
	int minModeWidth = 0;

	if(smNumber == 0)
		minModeWidth = _DSDBR01_SMPF_SM0_min_mode_width;
	else if(smNumber == 1)
		minModeWidth = _DSDBR01_SMPF_SM1_min_mode_width;
	else if(smNumber == 2)
		minModeWidth = _DSDBR01_SMPF_SM2_min_mode_width;
	else if(smNumber == 3)
		minModeWidth = _DSDBR01_SMPF_SM3_min_mode_width;
	else if(smNumber == 4)
		minModeWidth = _DSDBR01_SMPF_SM4_min_mode_width;
	else if(smNumber == 5)
		minModeWidth = _DSDBR01_SMPF_SM5_min_mode_width;
	else if(smNumber == 6)
		minModeWidth = _DSDBR01_SMPF_SM6_min_mode_width;
	else if(smNumber == 7)
		minModeWidth = _DSDBR01_SMPF_SM7_min_mode_width;
	else if(smNumber == 8)
		minModeWidth = _DSDBR01_SMPF_SM8_min_mode_width;
	else if(smNumber == 9)
		minModeWidth = _DSDBR01_SMPF_SM9_min_mode_width;

	return minModeWidth;
}
CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMPF_min_mode_width(short smNumber, int value)
{
	rtype rval = rtype::ok;

	if(smNumber == 0)
		_DSDBR01_SMPF_SM0_min_mode_width = value;
	else if(smNumber == 1)
		_DSDBR01_SMPF_SM1_min_mode_width = value;
	else if(smNumber == 2)
		_DSDBR01_SMPF_SM2_min_mode_width = value;
	else if(smNumber == 3)
		_DSDBR01_SMPF_SM3_min_mode_width = value;
	else if(smNumber == 4)
		_DSDBR01_SMPF_SM4_min_mode_width = value;
	else if(smNumber == 5)
		_DSDBR01_SMPF_SM5_min_mode_width = value;
	else if(smNumber == 6)
		_DSDBR01_SMPF_SM6_min_mode_width = value;
	else if(smNumber == 7)
		_DSDBR01_SMPF_SM7_min_mode_width = value;
	else if(smNumber == 8)
		_DSDBR01_SMPF_SM8_min_mode_width = value;
	else if(smNumber == 9)
		_DSDBR01_SMPF_SM9_min_mode_width = value;

	return rval;
}

int
CCGRegValues::get_DSDBR01_SMPF_max_mode_borders_removed(short smNumber)
{
	int modeBordersRemoved = 0;

	if(smNumber == 0)
		modeBordersRemoved = _DSDBR01_SMPF_SM0_max_mode_borders_removed;
	else if(smNumber == 1)
		modeBordersRemoved = _DSDBR01_SMPF_SM1_max_mode_borders_removed;
	else if(smNumber == 2)
		modeBordersRemoved = _DSDBR01_SMPF_SM2_max_mode_borders_removed;
	else if(smNumber == 3)
		modeBordersRemoved = _DSDBR01_SMPF_SM3_max_mode_borders_removed;
	else if(smNumber == 4)
		modeBordersRemoved = _DSDBR01_SMPF_SM4_max_mode_borders_removed;
	else if(smNumber == 5)
		modeBordersRemoved = _DSDBR01_SMPF_SM5_max_mode_borders_removed;
	else if(smNumber == 6)
		modeBordersRemoved = _DSDBR01_SMPF_SM6_max_mode_borders_removed;
	else if(smNumber == 7)
		modeBordersRemoved = _DSDBR01_SMPF_SM7_max_mode_borders_removed;
	else if(smNumber == 8)
		modeBordersRemoved = _DSDBR01_SMPF_SM8_max_mode_borders_removed;
	else if(smNumber == 9)
		modeBordersRemoved = _DSDBR01_SMPF_SM9_max_mode_borders_removed;

	return modeBordersRemoved;
}
CCGRegValues::rtype
CCGRegValues::set_DSDBR01_SMPF_max_mode_borders_removed(short smNumber, int value)
{
	rtype rval = rtype::ok;

	if(smNumber == 0)
		_DSDBR01_SMPF_SM0_max_mode_borders_removed = value;
	else if(smNumber == 1)
		_DSDBR01_SMPF_SM1_max_mode_borders_removed = value;
	else if(smNumber == 2)
		_DSDBR01_SMPF_SM2_max_mode_borders_removed = value;
	else if(smNumber == 3)
		_DSDBR01_SMPF_SM3_max_mode_borders_removed = value;
	else if(smNumber == 4)
		_DSDBR01_SMPF_SM4_max_mode_borders_removed = value;
	else if(smNumber == 5)
		_DSDBR01_SMPF_SM5_max_mode_borders_removed = value;
	else if(smNumber == 6)
		_DSDBR01_SMPF_SM6_max_mode_borders_removed = value;
	else if(smNumber == 7)
		_DSDBR01_SMPF_SM7_max_mode_borders_removed = value;
	else if(smNumber == 8)
		_DSDBR01_SMPF_SM8_max_mode_borders_removed = value;
	else if(smNumber == 9)
		_DSDBR01_SMPF_SM9_max_mode_borders_removed = value;

	return rval;
}

double
CCGRegValues::
get_DSDBR01_SMPF_mean_perc_working_region(short smNumber)
{
	double meanPercWorkingRegion = 0;

	if(smNumber == 0)
		meanPercWorkingRegion = _DSDBR01_SMPF_SM0_mean_perc_working_region;
	else if(smNumber == 1)
		meanPercWorkingRegion = _DSDBR01_SMPF_SM1_mean_perc_working_region;
	else if(smNumber == 2)
		meanPercWorkingRegion = _DSDBR01_SMPF_SM2_mean_perc_working_region;
	else if(smNumber == 3)
		meanPercWorkingRegion = _DSDBR01_SMPF_SM3_mean_perc_working_region;
	else if(smNumber == 4)
		meanPercWorkingRegion = _DSDBR01_SMPF_SM4_mean_perc_working_region;
	else if(smNumber == 5)
		meanPercWorkingRegion = _DSDBR01_SMPF_SM5_mean_perc_working_region;
	else if(smNumber == 6)
		meanPercWorkingRegion = _DSDBR01_SMPF_SM6_mean_perc_working_region;
	else if(smNumber == 7)
		meanPercWorkingRegion = _DSDBR01_SMPF_SM7_mean_perc_working_region;
	else if(smNumber == 8)
		meanPercWorkingRegion = _DSDBR01_SMPF_SM8_mean_perc_working_region;
	else if(smNumber == 9)
		meanPercWorkingRegion = _DSDBR01_SMPF_SM9_mean_perc_working_region;

	return meanPercWorkingRegion;
}
CCGRegValues::rtype
CCGRegValues::
set_DSDBR01_SMPF_mean_perc_working_region(short		smNumber,
										  double	value)
{
	rtype rval = rtype::ok;

	if(smNumber == 0)
		_DSDBR01_SMPF_SM0_mean_perc_working_region = value;
	else if(smNumber == 1)
		_DSDBR01_SMPF_SM1_mean_perc_working_region = value;
	else if(smNumber == 2)
		_DSDBR01_SMPF_SM2_mean_perc_working_region = value;
	else if(smNumber == 3)
		_DSDBR01_SMPF_SM3_mean_perc_working_region = value;
	else if(smNumber == 4)
		_DSDBR01_SMPF_SM4_mean_perc_working_region = value;
	else if(smNumber == 5)
		_DSDBR01_SMPF_SM5_mean_perc_working_region = value;
	else if(smNumber == 6)
		_DSDBR01_SMPF_SM6_mean_perc_working_region = value;
	else if(smNumber == 7)
		_DSDBR01_SMPF_SM7_mean_perc_working_region = value;
	else if(smNumber == 8)
		_DSDBR01_SMPF_SM8_mean_perc_working_region = value;
	else if(smNumber == 9)
		_DSDBR01_SMPF_SM9_mean_perc_working_region = value;

	return rval;
}

double
CCGRegValues::
get_DSDBR01_SMPF_min_perc_working_region(short smNumber)
{
	double minPercWorkingRegion = 0;

	if(smNumber == 0)
		minPercWorkingRegion = _DSDBR01_SMPF_SM0_min_perc_working_region;
	else if(smNumber == 1)
		minPercWorkingRegion = _DSDBR01_SMPF_SM1_min_perc_working_region;
	else if(smNumber == 2)
		minPercWorkingRegion = _DSDBR01_SMPF_SM2_min_perc_working_region;
	else if(smNumber == 3)
		minPercWorkingRegion = _DSDBR01_SMPF_SM3_min_perc_working_region;
	else if(smNumber == 4)
		minPercWorkingRegion = _DSDBR01_SMPF_SM4_min_perc_working_region;
	else if(smNumber == 5)
		minPercWorkingRegion = _DSDBR01_SMPF_SM5_min_perc_working_region;
	else if(smNumber == 6)
		minPercWorkingRegion = _DSDBR01_SMPF_SM6_min_perc_working_region;
	else if(smNumber == 7)
		minPercWorkingRegion = _DSDBR01_SMPF_SM7_min_perc_working_region;
	else if(smNumber == 8)
		minPercWorkingRegion = _DSDBR01_SMPF_SM8_min_perc_working_region;
	else if(smNumber == 9)
		minPercWorkingRegion = _DSDBR01_SMPF_SM9_min_perc_working_region;

	return minPercWorkingRegion;
}
CCGRegValues::rtype
CCGRegValues::
set_DSDBR01_SMPF_min_perc_working_region(short		smNumber,
										  double	value)
{
	rtype rval = rtype::ok;

	if(smNumber == 0)
		_DSDBR01_SMPF_SM0_min_perc_working_region = value;
	else if(smNumber == 1)
		_DSDBR01_SMPF_SM1_min_perc_working_region = value;
	else if(smNumber == 2)
		_DSDBR01_SMPF_SM2_min_perc_working_region = value;
	else if(smNumber == 3)
		_DSDBR01_SMPF_SM3_min_perc_working_region = value;
	else if(smNumber == 4)
		_DSDBR01_SMPF_SM4_min_perc_working_region = value;
	else if(smNumber == 5)
		_DSDBR01_SMPF_SM5_min_perc_working_region = value;
	else if(smNumber == 6)
		_DSDBR01_SMPF_SM6_min_perc_working_region = value;
	else if(smNumber == 7)
		_DSDBR01_SMPF_SM7_min_perc_working_region = value;
	else if(smNumber == 8)
		_DSDBR01_SMPF_SM8_min_perc_working_region = value;
	else if(smNumber == 9)
		_DSDBR01_SMPF_SM9_min_perc_working_region = value;

	return rval;
}

double
CCGRegValues::
get_DSDBR01_SMPF_max_mode_bdc_area(short smNumber)
{
	double maxBdcArea = 0;

	if(smNumber == 0)
		maxBdcArea = _DSDBR01_SMPF_SM0_max_mode_bdc_area;
	else if(smNumber == 1)
		maxBdcArea = _DSDBR01_SMPF_SM1_max_mode_bdc_area;
	else if(smNumber == 2)
		maxBdcArea = _DSDBR01_SMPF_SM2_max_mode_bdc_area;
	else if(smNumber == 3)
		maxBdcArea = _DSDBR01_SMPF_SM3_max_mode_bdc_area;
	else if(smNumber == 4)
		maxBdcArea = _DSDBR01_SMPF_SM4_max_mode_bdc_area;
	else if(smNumber == 5)
		maxBdcArea = _DSDBR01_SMPF_SM5_max_mode_bdc_area;
	else if(smNumber == 6)
		maxBdcArea = _DSDBR01_SMPF_SM6_max_mode_bdc_area;
	else if(smNumber == 7)
		maxBdcArea = _DSDBR01_SMPF_SM7_max_mode_bdc_area;
	else if(smNumber == 8)
		maxBdcArea = _DSDBR01_SMPF_SM8_max_mode_bdc_area;
	else if(smNumber == 9)
		maxBdcArea = _DSDBR01_SMPF_SM9_max_mode_bdc_area;

	return maxBdcArea;
}
CCGRegValues::rtype
CCGRegValues::
set_DSDBR01_SMPF_max_mode_bdc_area(short		smNumber,
							  double	value)
{
	rtype rval = rtype::ok;

	if(smNumber == 0)
		_DSDBR01_SMPF_SM0_max_mode_bdc_area = value;
	else if(smNumber == 1)
		_DSDBR01_SMPF_SM1_max_mode_bdc_area = value;
	else if(smNumber == 2)
		_DSDBR01_SMPF_SM2_max_mode_bdc_area = value;
	else if(smNumber == 3)
		_DSDBR01_SMPF_SM3_max_mode_bdc_area = value;
	else if(smNumber == 4)
		_DSDBR01_SMPF_SM4_max_mode_bdc_area = value;
	else if(smNumber == 5)
		_DSDBR01_SMPF_SM5_max_mode_bdc_area = value;
	else if(smNumber == 6)
		_DSDBR01_SMPF_SM6_max_mode_bdc_area = value;
	else if(smNumber == 7)
		_DSDBR01_SMPF_SM7_max_mode_bdc_area = value;
	else if(smNumber == 8)
		_DSDBR01_SMPF_SM8_max_mode_bdc_area = value;
	else if(smNumber == 9)
		_DSDBR01_SMPF_SM9_max_mode_bdc_area = value;

	return rval;
}

double
CCGRegValues::
get_DSDBR01_SMPF_max_mode_bdc_area_x_length(short smNumber)
{
	double maxBdcAreaXLength = 0;

	if(smNumber == 0)
		maxBdcAreaXLength = _DSDBR01_SMPF_SM0_max_mode_bdc_area_x_length;
	else if(smNumber == 1)
		maxBdcAreaXLength = _DSDBR01_SMPF_SM1_max_mode_bdc_area_x_length;
	else if(smNumber == 2)
		maxBdcAreaXLength = _DSDBR01_SMPF_SM2_max_mode_bdc_area_x_length;
	else if(smNumber == 3)
		maxBdcAreaXLength = _DSDBR01_SMPF_SM3_max_mode_bdc_area_x_length;
	else if(smNumber == 4)
		maxBdcAreaXLength = _DSDBR01_SMPF_SM4_max_mode_bdc_area_x_length;
	else if(smNumber == 5)
		maxBdcAreaXLength = _DSDBR01_SMPF_SM5_max_mode_bdc_area_x_length;
	else if(smNumber == 6)
		maxBdcAreaXLength = _DSDBR01_SMPF_SM6_max_mode_bdc_area_x_length;
	else if(smNumber == 7)
		maxBdcAreaXLength = _DSDBR01_SMPF_SM7_max_mode_bdc_area_x_length;
	else if(smNumber == 8)
		maxBdcAreaXLength = _DSDBR01_SMPF_SM8_max_mode_bdc_area_x_length;
	else if(smNumber == 9)
		maxBdcAreaXLength = _DSDBR01_SMPF_SM9_max_mode_bdc_area_x_length;

	return maxBdcAreaXLength;
}
CCGRegValues::rtype
CCGRegValues::
set_DSDBR01_SMPF_max_mode_bdc_area_x_length(short	smNumber,
									   double	value)
{
	rtype rval = rtype::ok;

	if(smNumber == 0)
		_DSDBR01_SMPF_SM0_max_mode_bdc_area_x_length = value;
	else if(smNumber == 1)
		_DSDBR01_SMPF_SM1_max_mode_bdc_area_x_length = value;
	else if(smNumber == 2)
		_DSDBR01_SMPF_SM2_max_mode_bdc_area_x_length = value;
	else if(smNumber == 3)
		_DSDBR01_SMPF_SM3_max_mode_bdc_area_x_length = value;
	else if(smNumber == 4)
		_DSDBR01_SMPF_SM4_max_mode_bdc_area_x_length = value;
	else if(smNumber == 5)
		_DSDBR01_SMPF_SM5_max_mode_bdc_area_x_length = value;
	else if(smNumber == 6)
		_DSDBR01_SMPF_SM6_max_mode_bdc_area_x_length = value;
	else if(smNumber == 7)
		_DSDBR01_SMPF_SM7_max_mode_bdc_area_x_length = value;
	else if(smNumber == 8)
		_DSDBR01_SMPF_SM8_max_mode_bdc_area_x_length = value;
	else if(smNumber == 9)
		_DSDBR01_SMPF_SM9_max_mode_bdc_area_x_length = value;

	return rval;
}

double
CCGRegValues::
get_DSDBR01_SMPF_sum_mode_bdc_areas(short smNumber)
{
	double sumBdcArea = 0;

	if(smNumber == 0)
		sumBdcArea = _DSDBR01_SMPF_SM0_sum_mode_bdc_areas;
	else if(smNumber == 1)
		sumBdcArea = _DSDBR01_SMPF_SM1_sum_mode_bdc_areas;
	else if(smNumber == 2)
		sumBdcArea = _DSDBR01_SMPF_SM2_sum_mode_bdc_areas;
	else if(smNumber == 3)
		sumBdcArea = _DSDBR01_SMPF_SM3_sum_mode_bdc_areas;
	else if(smNumber == 4)
		sumBdcArea = _DSDBR01_SMPF_SM4_sum_mode_bdc_areas;
	else if(smNumber == 5)
		sumBdcArea = _DSDBR01_SMPF_SM5_sum_mode_bdc_areas;
	else if(smNumber == 6)
		sumBdcArea = _DSDBR01_SMPF_SM6_sum_mode_bdc_areas;
	else if(smNumber == 7)
		sumBdcArea = _DSDBR01_SMPF_SM7_sum_mode_bdc_areas;
	else if(smNumber == 8)
		sumBdcArea = _DSDBR01_SMPF_SM8_sum_mode_bdc_areas;
	else if(smNumber == 9)
		sumBdcArea = _DSDBR01_SMPF_SM9_sum_mode_bdc_areas;

	return sumBdcArea;
}
CCGRegValues::rtype
CCGRegValues::
set_DSDBR01_SMPF_sum_mode_bdc_areas(short	smNumber,
							   double	value)
{
	rtype rval = rtype::ok;

	if(smNumber == 0)
		_DSDBR01_SMPF_SM0_sum_mode_bdc_areas = value;
	else if(smNumber == 1)
		_DSDBR01_SMPF_SM1_sum_mode_bdc_areas = value;
	else if(smNumber == 2)
		_DSDBR01_SMPF_SM2_sum_mode_bdc_areas = value;
	else if(smNumber == 3)
		_DSDBR01_SMPF_SM3_sum_mode_bdc_areas = value;
	else if(smNumber == 4)
		_DSDBR01_SMPF_SM4_sum_mode_bdc_areas = value;
	else if(smNumber == 5)
		_DSDBR01_SMPF_SM5_sum_mode_bdc_areas = value;
	else if(smNumber == 6)
		_DSDBR01_SMPF_SM6_sum_mode_bdc_areas = value;
	else if(smNumber == 7)
		_DSDBR01_SMPF_SM7_sum_mode_bdc_areas = value;
	else if(smNumber == 8)
		_DSDBR01_SMPF_SM8_sum_mode_bdc_areas = value;
	else if(smNumber == 9)
		_DSDBR01_SMPF_SM9_sum_mode_bdc_areas = value;

	return rval;
}

double
CCGRegValues::
get_DSDBR01_SMPF_num_mode_bdc_areas(short smNumber)
{
	double numBdcArea = 0;

	if(smNumber == 0)
		numBdcArea = _DSDBR01_SMPF_SM0_num_mode_bdc_areas;
	else if(smNumber == 1)
		numBdcArea = _DSDBR01_SMPF_SM1_num_mode_bdc_areas;
	else if(smNumber == 2)
		numBdcArea = _DSDBR01_SMPF_SM2_num_mode_bdc_areas;
	else if(smNumber == 3)
		numBdcArea = _DSDBR01_SMPF_SM3_num_mode_bdc_areas;
	else if(smNumber == 4)
		numBdcArea = _DSDBR01_SMPF_SM4_num_mode_bdc_areas;
	else if(smNumber == 5)
		numBdcArea = _DSDBR01_SMPF_SM5_num_mode_bdc_areas;
	else if(smNumber == 6)
		numBdcArea = _DSDBR01_SMPF_SM6_num_mode_bdc_areas;
	else if(smNumber == 7)
		numBdcArea = _DSDBR01_SMPF_SM7_num_mode_bdc_areas;
	else if(smNumber == 8)
		numBdcArea = _DSDBR01_SMPF_SM8_num_mode_bdc_areas;
	else if(smNumber == 9)
		numBdcArea = _DSDBR01_SMPF_SM9_num_mode_bdc_areas;

	return numBdcArea;
}
CCGRegValues::rtype
CCGRegValues::
set_DSDBR01_SMPF_num_mode_bdc_areas(short	smNumber,
							   double	value)
{
	rtype rval = rtype::ok;

	if(smNumber == 0)
		_DSDBR01_SMPF_SM0_num_mode_bdc_areas = value;
	else if(smNumber == 1)
		_DSDBR01_SMPF_SM1_num_mode_bdc_areas = value;
	else if(smNumber == 2)
		_DSDBR01_SMPF_SM2_num_mode_bdc_areas = value;
	else if(smNumber == 3)
		_DSDBR01_SMPF_SM3_num_mode_bdc_areas = value;
	else if(smNumber == 4)
		_DSDBR01_SMPF_SM4_num_mode_bdc_areas = value;
	else if(smNumber == 5)
		_DSDBR01_SMPF_SM5_num_mode_bdc_areas = value;
	else if(smNumber == 6)
		_DSDBR01_SMPF_SM6_num_mode_bdc_areas = value;
	else if(smNumber == 7)
		_DSDBR01_SMPF_SM7_num_mode_bdc_areas = value;
	else if(smNumber == 8)
		_DSDBR01_SMPF_SM8_num_mode_bdc_areas = value;
	else if(smNumber == 9)
		_DSDBR01_SMPF_SM9_num_mode_bdc_areas = value;

	return rval;
}

double
CCGRegValues::
get_DSDBR01_SMPF_modal_distortion_angle(short smNumber)
{
	double modalDistortionAngle = 0;

	if(smNumber == 0)
		modalDistortionAngle = _DSDBR01_SMPF_SM0_modal_distortion_angle;
	else if(smNumber == 1)
		modalDistortionAngle = _DSDBR01_SMPF_SM1_modal_distortion_angle;
	else if(smNumber == 2)
		modalDistortionAngle = _DSDBR01_SMPF_SM2_modal_distortion_angle;
	else if(smNumber == 3)
		modalDistortionAngle = _DSDBR01_SMPF_SM3_modal_distortion_angle;
	else if(smNumber == 4)
		modalDistortionAngle = _DSDBR01_SMPF_SM4_modal_distortion_angle;
	else if(smNumber == 5)
		modalDistortionAngle = _DSDBR01_SMPF_SM5_modal_distortion_angle;
	else if(smNumber == 6)
		modalDistortionAngle = _DSDBR01_SMPF_SM6_modal_distortion_angle;
	else if(smNumber == 7)
		modalDistortionAngle = _DSDBR01_SMPF_SM7_modal_distortion_angle;
	else if(smNumber == 8)
		modalDistortionAngle = _DSDBR01_SMPF_SM8_modal_distortion_angle;
	else if(smNumber == 9)
		modalDistortionAngle = _DSDBR01_SMPF_SM9_modal_distortion_angle;

	return modalDistortionAngle;
}
CCGRegValues::rtype
CCGRegValues::
set_DSDBR01_SMPF_modal_distortion_angle(short	smNumber,
										double	value)
{
	rtype rval = rtype::ok;

	if(smNumber == 0)
		_DSDBR01_SMPF_SM0_modal_distortion_angle = value;
	else if(smNumber == 1)
		_DSDBR01_SMPF_SM1_modal_distortion_angle = value;
	else if(smNumber == 2)
		_DSDBR01_SMPF_SM2_modal_distortion_angle = value;
	else if(smNumber == 3)
		_DSDBR01_SMPF_SM3_modal_distortion_angle = value;
	else if(smNumber == 4)
		_DSDBR01_SMPF_SM4_modal_distortion_angle = value;
	else if(smNumber == 5)
		_DSDBR01_SMPF_SM5_modal_distortion_angle = value;
	else if(smNumber == 6)
		_DSDBR01_SMPF_SM6_modal_distortion_angle = value;
	else if(smNumber == 7)
		_DSDBR01_SMPF_SM7_modal_distortion_angle = value;
	else if(smNumber == 8)
		_DSDBR01_SMPF_SM8_modal_distortion_angle = value;
	else if(smNumber == 9)
		_DSDBR01_SMPF_SM9_modal_distortion_angle = value;

	return rval;
}

double
CCGRegValues::
get_DSDBR01_SMPF_modal_distortion_angle_x_pos(short smNumber)
{
	double modalDistortionAngleXPos = 0;

	if(smNumber == 0)
		modalDistortionAngleXPos = _DSDBR01_SMPF_SM0_modal_distortion_angle_x_pos;
	else if(smNumber == 1)
		modalDistortionAngleXPos = _DSDBR01_SMPF_SM1_modal_distortion_angle_x_pos;
	else if(smNumber == 2)
		modalDistortionAngleXPos = _DSDBR01_SMPF_SM2_modal_distortion_angle_x_pos;
	else if(smNumber == 3)
		modalDistortionAngleXPos = _DSDBR01_SMPF_SM3_modal_distortion_angle_x_pos;
	else if(smNumber == 4)
		modalDistortionAngleXPos = _DSDBR01_SMPF_SM4_modal_distortion_angle_x_pos;
	else if(smNumber == 5)
		modalDistortionAngleXPos = _DSDBR01_SMPF_SM5_modal_distortion_angle_x_pos;
	else if(smNumber == 6)
		modalDistortionAngleXPos = _DSDBR01_SMPF_SM6_modal_distortion_angle_x_pos;
	else if(smNumber == 7)
		modalDistortionAngleXPos = _DSDBR01_SMPF_SM7_modal_distortion_angle_x_pos;
	else if(smNumber == 8)
		modalDistortionAngleXPos = _DSDBR01_SMPF_SM8_modal_distortion_angle_x_pos;
	else if(smNumber == 9)
		modalDistortionAngleXPos = _DSDBR01_SMPF_SM9_modal_distortion_angle_x_pos;

	return modalDistortionAngleXPos;
}
CCGRegValues::rtype
CCGRegValues::
set_DSDBR01_SMPF_modal_distortion_angle_x_pos(short		smNumber,
											  double	value)
{
	rtype rval = rtype::ok;

	if(smNumber == 0)
		_DSDBR01_SMPF_SM0_modal_distortion_angle_x_pos = value;
	else if(smNumber == 1)
		_DSDBR01_SMPF_SM1_modal_distortion_angle_x_pos = value;
	else if(smNumber == 2)
		_DSDBR01_SMPF_SM2_modal_distortion_angle_x_pos = value;
	else if(smNumber == 3)
		_DSDBR01_SMPF_SM3_modal_distortion_angle_x_pos = value;
	else if(smNumber == 4)
		_DSDBR01_SMPF_SM4_modal_distortion_angle_x_pos = value;
	else if(smNumber == 5)
		_DSDBR01_SMPF_SM5_modal_distortion_angle_x_pos = value;
	else if(smNumber == 6)
		_DSDBR01_SMPF_SM6_modal_distortion_angle_x_pos = value;
	else if(smNumber == 7)
		_DSDBR01_SMPF_SM7_modal_distortion_angle_x_pos = value;
	else if(smNumber == 8)
		_DSDBR01_SMPF_SM8_modal_distortion_angle_x_pos = value;
	else if(smNumber == 9)
		_DSDBR01_SMPF_SM9_modal_distortion_angle_x_pos = value;

	return rval;
}

double
CCGRegValues::
get_DSDBR01_SMPF_max_middle_line_rms_value(short smNumber)
{
	double middleLineRMSValue = 0;

	if(smNumber == 0)
		middleLineRMSValue = _DSDBR01_SMPF_SM0_max_middle_line_rms_value;
	else if(smNumber == 1)
		middleLineRMSValue = _DSDBR01_SMPF_SM1_max_middle_line_rms_value;
	else if(smNumber == 2)
		middleLineRMSValue = _DSDBR01_SMPF_SM2_max_middle_line_rms_value;
	else if(smNumber == 3)
		middleLineRMSValue = _DSDBR01_SMPF_SM3_max_middle_line_rms_value;
	else if(smNumber == 4)
		middleLineRMSValue = _DSDBR01_SMPF_SM4_max_middle_line_rms_value;
	else if(smNumber == 5)
		middleLineRMSValue = _DSDBR01_SMPF_SM5_max_middle_line_rms_value;
	else if(smNumber == 6)
		middleLineRMSValue = _DSDBR01_SMPF_SM6_max_middle_line_rms_value;
	else if(smNumber == 7)
		middleLineRMSValue = _DSDBR01_SMPF_SM7_max_middle_line_rms_value;
	else if(smNumber == 8)
		middleLineRMSValue = _DSDBR01_SMPF_SM8_max_middle_line_rms_value;
	else if(smNumber == 9)
		middleLineRMSValue = _DSDBR01_SMPF_SM9_max_middle_line_rms_value;

	return middleLineRMSValue;
}
CCGRegValues::rtype
CCGRegValues::
set_DSDBR01_SMPF_max_middle_line_rms_value(short	smNumber,
									   double	value)
{
	rtype rval = rtype::ok;

	if(smNumber == 0)
		_DSDBR01_SMPF_SM0_max_middle_line_rms_value = value;
	else if(smNumber == 1)
		_DSDBR01_SMPF_SM1_max_middle_line_rms_value = value;
	else if(smNumber == 2)
		_DSDBR01_SMPF_SM2_max_middle_line_rms_value = value;
	else if(smNumber == 3)
		_DSDBR01_SMPF_SM3_max_middle_line_rms_value = value;
	else if(smNumber == 4)
		_DSDBR01_SMPF_SM4_max_middle_line_rms_value = value;
	else if(smNumber == 5)
		_DSDBR01_SMPF_SM5_max_middle_line_rms_value = value;
	else if(smNumber == 6)
		_DSDBR01_SMPF_SM6_max_middle_line_rms_value = value;
	else if(smNumber == 7)
		_DSDBR01_SMPF_SM7_max_middle_line_rms_value = value;
	else if(smNumber == 8)
		_DSDBR01_SMPF_SM8_max_middle_line_rms_value = value;
	else if(smNumber == 9)
		_DSDBR01_SMPF_SM9_max_middle_line_rms_value = value;

	return rval;
}

double
CCGRegValues::
get_DSDBR01_SMPF_min_middle_line_slope(short smNumber)
{
	double minMiddleLineSlope = 0;

	if(smNumber == 0)
		minMiddleLineSlope = _DSDBR01_SMPF_SM0_min_middle_line_slope;
	else if(smNumber == 1)
		minMiddleLineSlope = _DSDBR01_SMPF_SM1_min_middle_line_slope;
	else if(smNumber == 2)
		minMiddleLineSlope = _DSDBR01_SMPF_SM2_min_middle_line_slope;
	else if(smNumber == 3)
		minMiddleLineSlope = _DSDBR01_SMPF_SM3_min_middle_line_slope;
	else if(smNumber == 4)
		minMiddleLineSlope = _DSDBR01_SMPF_SM4_min_middle_line_slope;
	else if(smNumber == 5)
		minMiddleLineSlope = _DSDBR01_SMPF_SM5_min_middle_line_slope;
	else if(smNumber == 6)
		minMiddleLineSlope = _DSDBR01_SMPF_SM6_min_middle_line_slope;
	else if(smNumber == 7)
		minMiddleLineSlope = _DSDBR01_SMPF_SM7_min_middle_line_slope;
	else if(smNumber == 8)
		minMiddleLineSlope = _DSDBR01_SMPF_SM8_min_middle_line_slope;
	else if(smNumber == 9)
		minMiddleLineSlope = _DSDBR01_SMPF_SM9_min_middle_line_slope;

	return minMiddleLineSlope;
}
CCGRegValues::rtype
CCGRegValues::
set_DSDBR01_SMPF_min_middle_line_slope(short	smNumber,
									   double	value)
{
	rtype rval = rtype::ok;

	if(smNumber == 0)
		_DSDBR01_SMPF_SM0_min_middle_line_slope = value;
	else if(smNumber == 1)
		_DSDBR01_SMPF_SM1_min_middle_line_slope = value;
	else if(smNumber == 2)
		_DSDBR01_SMPF_SM2_min_middle_line_slope = value;
	else if(smNumber == 3)
		_DSDBR01_SMPF_SM3_min_middle_line_slope = value;
	else if(smNumber == 4)
		_DSDBR01_SMPF_SM4_min_middle_line_slope = value;
	else if(smNumber == 5)
		_DSDBR01_SMPF_SM5_min_middle_line_slope = value;
	else if(smNumber == 6)
		_DSDBR01_SMPF_SM6_min_middle_line_slope = value;
	else if(smNumber == 7)
		_DSDBR01_SMPF_SM7_min_middle_line_slope = value;
	else if(smNumber == 8)
		_DSDBR01_SMPF_SM8_min_middle_line_slope = value;
	else if(smNumber == 9)
		_DSDBR01_SMPF_SM9_min_middle_line_slope = value;

	return rval;
}

double
CCGRegValues::
get_DSDBR01_SMPF_max_middle_line_slope(short smNumber)
{
	double maxMiddleLineSlope = 0;

	if(smNumber == 0)
		maxMiddleLineSlope = _DSDBR01_SMPF_SM0_max_middle_line_slope;
	else if(smNumber == 1)
		maxMiddleLineSlope = _DSDBR01_SMPF_SM1_max_middle_line_slope;
	else if(smNumber == 2)
		maxMiddleLineSlope = _DSDBR01_SMPF_SM2_max_middle_line_slope;
	else if(smNumber == 3)
		maxMiddleLineSlope = _DSDBR01_SMPF_SM3_max_middle_line_slope;
	else if(smNumber == 4)
		maxMiddleLineSlope = _DSDBR01_SMPF_SM4_max_middle_line_slope;
	else if(smNumber == 5)
		maxMiddleLineSlope = _DSDBR01_SMPF_SM5_max_middle_line_slope;
	else if(smNumber == 6)
		maxMiddleLineSlope = _DSDBR01_SMPF_SM6_max_middle_line_slope;
	else if(smNumber == 7)
		maxMiddleLineSlope = _DSDBR01_SMPF_SM7_max_middle_line_slope;
	else if(smNumber == 8)
		maxMiddleLineSlope = _DSDBR01_SMPF_SM8_max_middle_line_slope;
	else if(smNumber == 9)
		maxMiddleLineSlope = _DSDBR01_SMPF_SM9_max_middle_line_slope;

	return maxMiddleLineSlope;
}
CCGRegValues::rtype
CCGRegValues::
set_DSDBR01_SMPF_max_middle_line_slope(short	smNumber,
									   double	value)
{
	rtype rval = rtype::ok;

	if(smNumber == 0)
		_DSDBR01_SMPF_SM0_max_middle_line_slope = value;
	else if(smNumber == 1)
		_DSDBR01_SMPF_SM1_max_middle_line_slope = value;
	else if(smNumber == 2)
		_DSDBR01_SMPF_SM2_max_middle_line_slope = value;
	else if(smNumber == 3)
		_DSDBR01_SMPF_SM3_max_middle_line_slope = value;
	else if(smNumber == 4)
		_DSDBR01_SMPF_SM4_max_middle_line_slope = value;
	else if(smNumber == 5)
		_DSDBR01_SMPF_SM5_max_middle_line_slope = value;
	else if(smNumber == 6)
		_DSDBR01_SMPF_SM6_max_middle_line_slope = value;
	else if(smNumber == 7)
		_DSDBR01_SMPF_SM7_max_middle_line_slope = value;
	else if(smNumber == 8)
		_DSDBR01_SMPF_SM8_max_middle_line_slope = value;
	else if(smNumber == 9)
		_DSDBR01_SMPF_SM9_max_middle_line_slope = value;

	return rval;
}

//double
//CCGRegValues::
//get_DSDBR01_SMPF_continuity_spacing(short smNumber)
//{
//	double continuitySpacing = DEFAULT_DSDBR01_SMPF_MAX_CONTINUITY_SPACING_THRESHOLD;
//
//	if(smNumber == 0)
//		continuitySpacing = _DSDBR01_SMPF_SM0_max_continuity_spacing;
//	else if(smNumber == 1)
//		continuitySpacing = _DSDBR01_SMPF_SM1_max_continuity_spacing;
//	else if(smNumber == 2)
//		continuitySpacing = _DSDBR01_SMPF_SM2_max_continuity_spacing;
//	else if(smNumber == 3)
//		continuitySpacing = _DSDBR01_SMPF_SM3_max_continuity_spacing;
//	else if(smNumber == 4)
//		continuitySpacing = _DSDBR01_SMPF_SM4_max_continuity_spacing;
//	else if(smNumber == 5)
//		continuitySpacing = _DSDBR01_SMPF_SM5_max_continuity_spacing;
//	else if(smNumber == 6)
//		continuitySpacing = _DSDBR01_SMPF_SM6_max_continuity_spacing;
//	else if(smNumber == 7)
//		continuitySpacing = _DSDBR01_SMPF_SM7_max_continuity_spacing;
//	else if(smNumber == 8)
//		continuitySpacing = _DSDBR01_SMPF_SM8_max_continuity_spacing;
//	else if(smNumber == 9)
//		continuitySpacing = _DSDBR01_SMPF_SM9_max_continuity_spacing;
//
//	return continuitySpacing;
//}
//CCGRegValues::rtype
//CCGRegValues::
//set_DSDBR01_SMPF_continuity_spacing(short	smNumber,
//									double	value)
//{
//	rtype rval = rtype::ok;
//
//	if(smNumber == 0)
//		_DSDBR01_SMPF_SM0_max_continuity_spacing = value;
//	else if(smNumber == 1)
//		_DSDBR01_SMPF_SM1_max_continuity_spacing = value;
//	else if(smNumber == 2)
//		_DSDBR01_SMPF_SM2_max_continuity_spacing = value;
//	else if(smNumber == 3)
//		_DSDBR01_SMPF_SM3_max_continuity_spacing = value;
//	else if(smNumber == 4)
//		_DSDBR01_SMPF_SM4_max_continuity_spacing = value;
//	else if(smNumber == 5)
//		_DSDBR01_SMPF_SM5_max_continuity_spacing = value;
//	else if(smNumber == 6)
//		_DSDBR01_SMPF_SM6_max_continuity_spacing = value;
//	else if(smNumber == 7)
//		_DSDBR01_SMPF_SM7_max_continuity_spacing = value;
//	else if(smNumber == 8)
//		_DSDBR01_SMPF_SM8_max_continuity_spacing = value;
//	else if(smNumber == 9)
//		_DSDBR01_SMPF_SM9_max_continuity_spacing = value;
//
//	return rval;
//}

double
CCGRegValues::
get_DSDBR01_SMPF_max_mode_line_slope(short smNumber)
{
	double maxModeLineSlope = 0;

	if(smNumber == 0)
		maxModeLineSlope = _DSDBR01_SMPF_SM0_max_mode_line_slope;
	else if(smNumber == 1)
		maxModeLineSlope = _DSDBR01_SMPF_SM1_max_mode_line_slope;
	else if(smNumber == 2)
		maxModeLineSlope = _DSDBR01_SMPF_SM2_max_mode_line_slope;
	else if(smNumber == 3)
		maxModeLineSlope = _DSDBR01_SMPF_SM3_max_mode_line_slope;
	else if(smNumber == 4)
		maxModeLineSlope = _DSDBR01_SMPF_SM4_max_mode_line_slope;
	else if(smNumber == 5)
		maxModeLineSlope = _DSDBR01_SMPF_SM5_max_mode_line_slope;
	else if(smNumber == 6)
		maxModeLineSlope = _DSDBR01_SMPF_SM6_max_mode_line_slope;
	else if(smNumber == 7)
		maxModeLineSlope = _DSDBR01_SMPF_SM7_max_mode_line_slope;
	else if(smNumber == 8)
		maxModeLineSlope = _DSDBR01_SMPF_SM8_max_mode_line_slope;
	else if(smNumber == 9)
		maxModeLineSlope = _DSDBR01_SMPF_SM9_max_mode_line_slope;

	return maxModeLineSlope;
}
CCGRegValues::rtype
CCGRegValues::
set_DSDBR01_SMPF_max_mode_line_slope(short	smNumber,
									   double	value)
{
	rtype rval = rtype::ok;

	if(smNumber == 0)
		_DSDBR01_SMPF_SM0_max_mode_line_slope = value;
	else if(smNumber == 1)
		_DSDBR01_SMPF_SM1_max_mode_line_slope = value;
	else if(smNumber == 2)
		_DSDBR01_SMPF_SM2_max_mode_line_slope = value;
	else if(smNumber == 3)
		_DSDBR01_SMPF_SM3_max_mode_line_slope = value;
	else if(smNumber == 4)
		_DSDBR01_SMPF_SM4_max_mode_line_slope = value;
	else if(smNumber == 5)
		_DSDBR01_SMPF_SM5_max_mode_line_slope = value;
	else if(smNumber == 6)
		_DSDBR01_SMPF_SM6_max_mode_line_slope = value;
	else if(smNumber == 7)
		_DSDBR01_SMPF_SM7_max_mode_line_slope = value;
	else if(smNumber == 8)
		_DSDBR01_SMPF_SM8_max_mode_line_slope = value;
	else if(smNumber == 9)
		_DSDBR01_SMPF_SM9_max_mode_line_slope = value;

	return rval;
}
//
/////////////////////////////////////////////////////////////////
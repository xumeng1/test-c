// Copyright (c) 2004 PXIT. All Rights Reserved
//
// FileName    : CGRegValues.cpp
// Description : Definition of CCGRegValues class
//               Represents all Registry stored data
//               Singleton class
//
// Rev#  | Date          | Author               | Description of change
// ---------------------------------------------------------------------
// 0.1   | 04 Mar  2006  | Frank D'Arcy         | Initial Draft
//

#include "StdAfx.h"
#include "CGRegValues.h"
#include "RegIf.h"
#include "CGRegDefs.h"



CCGRegValues::rtype
CCGRegValues::retrieveAllFromRegistry()
{
	rtype rval2return = rtype::ok;
	rtype rval = rtype::ok;
	RegInterface::return_type reg_rval = RegInterface::return_type::ok;

	setDefaults();

	// Base parameters

	CString reg_key = CString( CG_DSDBR01_BASE_REGKEY );

	if( P_REG->keyExists( reg_key.GetBuffer() ) )
	{
		// Key exists, read in stored values, otherwise use defaults


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getCStringValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_BASE_RESULTS_DIR ),
			_DSDBR01_BASE_results_directory );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_BASE_results_directory = CString(DEFAULT_DSDBR01_BASE_RESULTS_DIR);
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getBoolValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_BASE_STUB_HARDWARE_CALLS ),
			_DSDBR01_BASE_stub_hardware_calls );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_BASE_stub_hardware_calls = DEFAULT_DSDBR01_BASE_STUB_HARDWARE_CALLS;
		}

	}
	else
	{
		rval2return = rtype::registry_key_does_not_exist;
	}



	// DSDBR01 Common Data Collection parameters

	reg_key = CString( CG_DSDBR01_BASE_REGKEY ) + CString( RELATIVE_REGKEY_DSDBR01_CMDC );

	if( P_REG->keyExists( reg_key.GetBuffer() ) )
	{
		// Key exists, read in stored values, otherwise use defaults


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getCStringValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_CMDC_GAIN_MODULE_NAME ),
			_DSDBR01_CMDC_gain_module_name );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_CMDC_gain_module_name = CString(DEFAULT_DSDBR01_CMDC_GAIN_MODULE_NAME);
		}


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getCStringValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_CMDC_SOA_MODULE_NAME ),
			_DSDBR01_CMDC_soa_module_name );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_CMDC_soa_module_name = CString(DEFAULT_DSDBR01_CMDC_SOA_MODULE_NAME);
		}


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getCStringValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_CMDC_PHASE_MODULE_NAME ),
			_DSDBR01_CMDC_phase_module_name );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_CMDC_phase_module_name = CString(DEFAULT_DSDBR01_CMDC_PHASE_MODULE_NAME);
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getCStringValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_CMDC_REAR_MODULE_NAME ),
			_DSDBR01_CMDC_rear_module_name );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_CMDC_rear_module_name = CString(DEFAULT_DSDBR01_CMDC_REAR_MODULE_NAME);
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getCStringValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_CMDC_FRONT1_MODULE_NAME ),
			_DSDBR01_CMDC_front1_module_name );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_CMDC_front1_module_name = CString(DEFAULT_DSDBR01_CMDC_FRONT1_MODULE_NAME);
		}



		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getCStringValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_CMDC_FRONT2_MODULE_NAME ),
			_DSDBR01_CMDC_front2_module_name );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_CMDC_front2_module_name = CString(DEFAULT_DSDBR01_CMDC_FRONT2_MODULE_NAME);
		}


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getCStringValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_CMDC_FRONT3_MODULE_NAME ),
			_DSDBR01_CMDC_front3_module_name );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_CMDC_front3_module_name = CString(DEFAULT_DSDBR01_CMDC_FRONT3_MODULE_NAME);
		}


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getCStringValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_CMDC_FRONT4_MODULE_NAME ),
			_DSDBR01_CMDC_front4_module_name );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_CMDC_front4_module_name = CString(DEFAULT_DSDBR01_CMDC_FRONT4_MODULE_NAME);
		}


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getCStringValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_CMDC_FRONT5_MODULE_NAME ),
			_DSDBR01_CMDC_front5_module_name );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_CMDC_front5_module_name = CString(DEFAULT_DSDBR01_CMDC_FRONT5_MODULE_NAME);
		}


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getCStringValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_CMDC_FRONT6_MODULE_NAME ),
			_DSDBR01_CMDC_front6_module_name );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_CMDC_front6_module_name = CString(DEFAULT_DSDBR01_CMDC_FRONT6_MODULE_NAME);
		}


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getCStringValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_CMDC_FRONT7_MODULE_NAME ),
			_DSDBR01_CMDC_front7_module_name );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_CMDC_front7_module_name = CString(DEFAULT_DSDBR01_CMDC_FRONT7_MODULE_NAME);
		}


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getCStringValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_CMDC_FRONT8_MODULE_NAME ),
			_DSDBR01_CMDC_front8_module_name );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_CMDC_front8_module_name = CString(DEFAULT_DSDBR01_CMDC_FRONT8_MODULE_NAME);
		}



		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getCStringValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_CMDC_306II_MODULE_NAME ),
			_DSDBR01_CMDC_306II_module_name );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_CMDC_306II_module_name = CString(DEFAULT_DSDBR01_CMDC_306II_MODULE_NAME);
		}


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_CMDC_306II_DIRECT_CHANNEL ),
			_DSDBR01_CMDC_306II_direct_channel );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_CMDC_306II_direct_channel = DEFAULT_DSDBR01_CMDC_306II_DIRECT_CHANNEL;
		}


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_CMDC_306II_FILTERED_CHANNEL ),
			_DSDBR01_CMDC_306II_filtered_channel );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_CMDC_306II_filtered_channel = DEFAULT_DSDBR01_CMDC_306II_FILTERED_CHANNEL;
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getCStringValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_CMDC_306EE_MODULE_NAME ),
			_DSDBR01_CMDC_306EE_module_name );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_CMDC_306EE_module_name = CString(DEFAULT_DSDBR01_CMDC_306EE_MODULE_NAME);
		}



		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_CMDC_306EE_PHOTODIODE1_CHANNEL ),
			_DSDBR01_CMDC_306EE_photodiode1_channel );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_CMDC_306EE_photodiode1_channel = DEFAULT_DSDBR01_CMDC_306EE_PHOTODIODE1_CHANNEL;
		}


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_CMDC_306EE_PHOTODIODE2_CHANNEL ),
			_DSDBR01_CMDC_306EE_photodiode2_channel );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_CMDC_306EE_photodiode2_channel = DEFAULT_DSDBR01_CMDC_306EE_PHOTODIODE2_CHANNEL;
		}


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_CMDC_WAVEMETER_RETRIES ),
			_DSDBR01_CMDC_wavemeter_retries );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_CMDC_wavemeter_retries = DEFAULT_DSDBR01_CMDC_WAVEMETER_RETRIES;
		}

	}
	else
	{
		rval2return = rtype::registry_key_does_not_exist;
	}

	// Overall Map Data Collection parameters

	reg_key = CString( CG_DSDBR01_BASE_REGKEY ) + CString( RELATIVE_REGKEY_DSDBR01_OMDC );

	if( P_REG->keyExists( reg_key.GetBuffer() ) )
	{
		// Key exists, read in stored values, otherwise use defaults


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getCStringValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_OMDC_RAMP_DIRECTION ),
			_DSDBR01_OMDC_ramp_direction );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		if( reg_rval == RegInterface::return_type::ok 
		 && _DSDBR01_OMDC_ramp_direction != OMDC_RAMP_REAR
		 && _DSDBR01_OMDC_ramp_direction != OMDC_RAMP_FRONT )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_OMDC_ramp_direction = CString(DEFAULT_DSDBR01_OMDC_RAMP_DIRECTION);
		}


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getBoolValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_OMDC_FORWARD_AND_REVERSE ),
			_DSDBR01_OMDC_forward_and_reverse );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_OMDC_forward_and_reverse = DEFAULT_DSDBR01_OMDC_FORWARD_AND_REVERSE;
		}



		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getBoolValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_OMDC_MEASURE_PHOTODIODE_CURRENTS ),
			_DSDBR01_OMDC_measure_photodiode_currents );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_OMDC_measure_photodiode_currents = DEFAULT_DSDBR01_OMDC_MEASURE_PHOTODIODE_CURRENTS;
		}


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getBoolValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_OMDC_WRITE_FILTERED_POWER_MAPS ),
			_DSDBR01_OMDC_write_filtered_power_maps );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_OMDC_write_filtered_power_maps = DEFAULT_DSDBR01_OMDC_WRITE_FILTERED_POWER_MAPS;
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getCStringValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_OMDC_REAR_CURRENTS_ABSPATH ),
			_DSDBR01_OMDC_rear_currents_abspath );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_OMDC_rear_currents_abspath = CString(DEFAULT_DSDBR01_OMDC_REAR_CURRENTS_ABSPATH);
		}


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getCStringValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_OMDC_FRONT_CURRENTS_ABSPATH ),
			_DSDBR01_OMDC_front_currents_abspath );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_OMDC_front_currents_abspath = CString(DEFAULT_DSDBR01_OMDC_FRONT_CURRENTS_ABSPATH);
		}


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_OMDC_SOURCE_DELAY ),
			_DSDBR01_OMDC_source_delay );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_OMDC_source_delay = DEFAULT_DSDBR01_OMDC_SOURCE_DELAY;
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_OMDC_MEASURE_DELAY ),
			_DSDBR01_OMDC_measure_delay );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_OMDC_measure_delay = DEFAULT_DSDBR01_OMDC_MEASURE_DELAY;
		}


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getBoolValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_OMDC_OVERWRITE_EXISTING_FILES ),
			_DSDBR01_OMDC_overwrite_existing_files );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_OMDC_overwrite_existing_files = DEFAULT_DSDBR01_OMDC_OVERWRITE_EXISTING_FILES;
		}

	}
	else
	{
		rval2return = rtype::registry_key_does_not_exist;
	}

	// Overall Map Boundary Detection parameters

	reg_key = CString( CG_DSDBR01_BASE_REGKEY ) + CString( RELATIVE_REGKEY_DSDBR01_OMBD );

	if( P_REG->keyExists( reg_key.GetBuffer() ) )
	{
		// Key exists, read in stored values, otherwise use defaults


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getBoolValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_OMBD_READ_BOUNDARIES_FROM_FILE ),
			_DSDBR01_OMBD_read_boundaries_from_file );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_OMBD_read_boundaries_from_file = DEFAULT_DSDBR01_OMBD_READ_BOUNDARIES_FROM_FILE;
		}



		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getBoolValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_OMBD_WRITE_BOUNDARIES_TO_FILE ),
			_DSDBR01_OMBD_write_boundaries_to_file );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_OMBD_write_boundaries_to_file = DEFAULT_DSDBR01_OMBD_WRITE_BOUNDARIES_TO_FILE;
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_OMBD_MEDIAN_FILTER_RANK ),
			_DSDBR01_OMBD_median_filter_rank );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_OMBD_median_filter_rank = DEFAULT_DSDBR01_OMBD_MEDIAN_FILTER_RANK;
		}



		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getDoubleValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_OMBD_MAX_DELTAPR_IN_SM ),
			_DSDBR01_OMBD_max_deltaPr_in_sm );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_OMBD_max_deltaPr_in_sm = DEFAULT_DSDBR01_OMBD_MAX_DELTAPR_IN_SM;
		}




		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_OMBD_MIN_POINTS_WIDTH_OF_SM ),
			_DSDBR01_OMBD_min_points_width_of_sm );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_OMBD_min_points_width_of_sm = DEFAULT_DSDBR01_OMBD_MIN_POINTS_WIDTH_OF_SM;
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_OMBD_ML_MOVING_AVERAGE_FILTER_N ),
			_DSDBR01_OMBD_ml_moving_ave_filter_n );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_OMBD_ml_moving_ave_filter_n = DEFAULT_DSDBR01_OMBD_ML_MOVING_AVERAGE_FILTER_N;
		}


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_OMBD_ML_MIN_LENGTH ),
			_DSDBR01_OMBD_ml_min_length );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_OMBD_ml_min_length = DEFAULT_DSDBR01_OMBD_ML_MIN_LENGTH;
		}


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getBoolValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_OMBD_ML_EXTEND_TO_CORNER ),
			_DSDBR01_OMBD_ml_extend_to_corner );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_OMBD_ml_extend_to_corner = DEFAULT_DSDBR01_OMBD_ML_EXTEND_TO_CORNER;
		}

	}
	else
	{
		rval2return = rtype::registry_key_does_not_exist;
	}

	// Supermode Map collection parameters

	reg_key = CString( CG_DSDBR01_BASE_REGKEY ) + CString( RELATIVE_REGKEY_DSDBR01_SMDC );

	if( P_REG->keyExists( reg_key.GetBuffer() ) )
	{
		// Key exists, read in stored values, otherwise use defaults


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getCStringValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMDC_RAMP_DIRECTION ),
			_DSDBR01_SMDC_ramp_direction );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		if( reg_rval == RegInterface::return_type::ok 
		 && _DSDBR01_SMDC_ramp_direction != SMDC_RAMP_MIDDLE_LINE
		 && _DSDBR01_SMDC_ramp_direction != SMDC_RAMP_PHASE )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMDC_ramp_direction = CString(DEFAULT_DSDBR01_SMDC_RAMP_DIRECTION);
		}


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getCStringValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMDC_PHASE_CURRENTS_ABSPATH ),
			_DSDBR01_SMDC_phase_currents_abspath );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMDC_phase_currents_abspath = CString(DEFAULT_DSDBR01_SMDC_PHASE_CURRENTS_ABSPATH);
		}


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getBoolValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMDC_MEASURE_PHOTODIODE_CURRENTS ),
			_DSDBR01_SMDC_measure_photodiode_currents );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMDC_measure_photodiode_currents = DEFAULT_DSDBR01_SMDC_MEASURE_PHOTODIODE_CURRENTS;
		}


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getBoolValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMDC_WRITE_FILTERED_POWER_MAPS ),
			_DSDBR01_SMDC_write_filtered_power_maps );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMDC_write_filtered_power_maps = DEFAULT_DSDBR01_SMDC_WRITE_FILTERED_POWER_MAPS;
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMDC_SOURCE_DELAY ),
			_DSDBR01_SMDC_source_delay );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMDC_source_delay = DEFAULT_DSDBR01_SMDC_SOURCE_DELAY;
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMDC_MEASURE_DELAY ),
			_DSDBR01_SMDC_measure_delay );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMDC_measure_delay = DEFAULT_DSDBR01_SMDC_MEASURE_DELAY;
		}


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getBoolValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMDC_OVERWRITE_EXISTING_FILES ),
			_DSDBR01_SMDC_overwrite_existing_files );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMDC_overwrite_existing_files = DEFAULT_DSDBR01_SMDC_OVERWRITE_EXISTING_FILES;
		}

	}
	else
	{
		rval2return = rtype::registry_key_does_not_exist;
	}


	// Supermode Map Boundary Detection parameters

	reg_key = CString( CG_DSDBR01_BASE_REGKEY ) + CString( RELATIVE_REGKEY_DSDBR01_SMBD );

	if( P_REG->keyExists( reg_key.GetBuffer() ) )
	{
		// Key exists, read in stored values, otherwise use defaults


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getBoolValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBD_READ_BOUNDARIES_FROM_FILE ),
			_DSDBR01_SMBD_read_boundaries_from_file );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBD_read_boundaries_from_file = DEFAULT_DSDBR01_SMBD_READ_BOUNDARIES_FROM_FILE;
		}



		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getBoolValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBD_WRITE_BOUNDARIES_TO_FILE ),
			_DSDBR01_SMBD_write_boundaries_to_file );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBD_write_boundaries_to_file = DEFAULT_DSDBR01_SMBD_WRITE_BOUNDARIES_TO_FILE;
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getBoolValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBD_write_middle_of_upper_lines_to_file ),
			_DSDBR01_SMBD_write_middle_of_upper_lines_to_file );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBD_write_middle_of_upper_lines_to_file = DEFAULT_DSDBR01_SMBD_write_middle_of_upper_lines_to_file;
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getBoolValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBD_write_middle_of_lower_lines_to_file ),
			_DSDBR01_SMBD_write_middle_of_lower_lines_to_file );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBD_write_middle_of_lower_lines_to_file = DEFAULT_DSDBR01_SMBD_write_middle_of_lower_lines_to_file;
		}


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getBoolValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBD_WRITE_DEBUG_FILES ),
			_DSDBR01_SMBD_write_debug_files );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBD_write_debug_files = DEFAULT_DSDBR01_SMBD_WRITE_DEBUG_FILES;
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getBoolValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBD_extrapolate_to_map_edges ),
			_DSDBR01_SMBD_extrapolate_to_map_edges );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBD_extrapolate_to_map_edges = DEFAULT_DSDBR01_SMBD_extrapolate_to_map_edges;
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBD_MEDIAN_FILTER_RANK ),
			_DSDBR01_SMBD_median_filter_rank );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBD_median_filter_rank = DEFAULT_DSDBR01_SMBD_MEDIAN_FILTER_RANK;
		}



		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getDoubleValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBD_MAX_DELTAPR_IN_LM ),
			_DSDBR01_SMBD_max_deltaPr_in_lm );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBD_max_deltaPr_in_lm = DEFAULT_DSDBR01_SMBD_MAX_DELTAPR_IN_LM;
		}




		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBD_MIN_POINTS_WIDTH_OF_LM ),
			_DSDBR01_SMBD_min_points_width_of_lm );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBD_min_points_width_of_lm = DEFAULT_DSDBR01_SMBD_MIN_POINTS_WIDTH_OF_LM;
		}



		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBD_MAX_POINTS_WIDTH_OF_LM ),
			_DSDBR01_SMBD_max_points_width_of_lm );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBD_max_points_width_of_lm = DEFAULT_DSDBR01_SMBD_MAX_POINTS_WIDTH_OF_LM;
		}


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBD_ML_MOVING_AVERAGE_FILTER_N ),
			_DSDBR01_SMBD_ml_moving_ave_filter_n );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBD_ml_moving_ave_filter_n = DEFAULT_DSDBR01_SMBD_ML_MOVING_AVERAGE_FILTER_N;
		}


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBD_ML_MIN_LENGTH ),
			_DSDBR01_SMBD_ml_min_length );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBD_ml_min_length = DEFAULT_DSDBR01_SMBD_ML_MIN_LENGTH;
		}




		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getDoubleValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBD_VERTICAL_PERCENT_SHIFT ),
			_DSDBR01_SMBD_vertical_percent_shift );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBD_vertical_percent_shift = DEFAULT_DSDBR01_SMBD_VERTICAL_PERCENT_SHIFT;
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getDoubleValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBD_ZPRC_HYSTERESIS_UPPER_THRESHOLD ),
			_DSDBR01_SMBD_zprc_hysteresis_upper_threshold );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBD_zprc_hysteresis_upper_threshold = DEFAULT_DSDBR01_SMBD_ZPRC_HYSTERESIS_UPPER_THRESHOLD;
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getDoubleValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBD_ZPRC_HYSTERESIS_LOWER_THRESHOLD ),
			_DSDBR01_SMBD_zprc_hysteresis_lower_threshold );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBD_zprc_hysteresis_lower_threshold = DEFAULT_DSDBR01_SMBD_ZPRC_HYSTERESIS_LOWER_THRESHOLD;
		}


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getDoubleValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBD_ZPRC_MAX_HYSTERESIS_RISE ),
			_DSDBR01_SMBD_zprc_max_hysteresis_rise );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBD_zprc_max_hysteresis_rise = DEFAULT_DSDBR01_SMBD_ZPRC_MAX_HYSTERESIS_RISE;
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBD_ZPRC_MIN_POINTS_SEPARATION ),
			_DSDBR01_SMBD_zprc_min_points_separation );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBD_zprc_min_points_separation = DEFAULT_DSDBR01_SMBD_ZPRC_MIN_POINTS_SEPARATION;
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBD_ZPRC_MAX_POINTS_TO_REVERSE_JUMP ),
			_DSDBR01_SMBD_zprc_max_points_to_reverse_jump );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBD_zprc_max_points_to_reverse_jump = DEFAULT_DSDBR01_SMBD_ZPRC_MAX_POINTS_TO_REVERSE_JUMP;
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBD_ZPRC_MAX_HYSTERESIS_POINTS ),
			_DSDBR01_SMBD_zprc_max_hysteresis_points );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBD_zprc_max_hysteresis_points = DEFAULT_DSDBR01_SMBD_ZPRC_MAX_HYSTERESIS_POINTS;
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getCStringValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBD_RAMP_ABSPATH ),
			_DSDBR01_SMBD_ramp_abspath );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBD_ramp_abspath = CString(DEFAULT_DSDBR01_SMBD_RAMP_ABSPATH);
		}


	}
	else
	{
		rval2return = rtype::registry_key_does_not_exist;
	}


	// Forward Supermode Map Boundary Detection parameters

	reg_key = CString( CG_DSDBR01_BASE_REGKEY ) + CString( RELATIVE_REGKEY_DSDBR01_SMBDFWD );


	if( P_REG->keyExists( reg_key.GetBuffer() ) )
	{
		// Key exists, read in stored values, otherwise use defaults


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBDFWD_I_RAMP_BOUNDARIES ),
			_DSDBR01_SMBDFWD_I_ramp_boundaries_max );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBDFWD_I_ramp_boundaries_max = DEFAULT_DSDBR01_SMBDFWD_I_RAMP_BOUNDARIES;
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBDFWD_BOUNDARIES_MIN_SEPARATION ),
			_DSDBR01_SMBDFWD_boundaries_min_separation );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBDFWD_boundaries_min_separation = DEFAULT_DSDBR01_SMBDFWD_BOUNDARIES_MIN_SEPARATION;
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getDoubleValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBDFWD_MIN_D2PR_DI2_ZERO_CROSS_JUMP_SIZE ),
			_DSDBR01_SMBDFWD_min_d2Pr_dI2_zero_cross_jump_size );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBDFWD_min_d2Pr_dI2_zero_cross_jump_size = DEFAULT_DSDBR01_SMBDFWD_MIN_D2PR_DI2_ZERO_CROSS_JUMP_SIZE;
		}



		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getDoubleValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBDFWD_MAX_SUM_SQUARED_DIST_FROM_PR_I_LINE ),
			_DSDBR01_SMBDFWD_max_sum_of_squared_distances_from_Pr_I_line );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBDFWD_max_sum_of_squared_distances_from_Pr_I_line = DEFAULT_DSDBR01_SMBDFWD_MAX_SUM_SQUARED_DIST_FROM_PR_I_LINE;
		}


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getDoubleValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBDFWD_MIN_SLOPE_CHANGE_OF_PR_I_LINE ),
			_DSDBR01_SMBDFWD_min_slope_change_of_Pr_I_lines );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBDFWD_min_slope_change_of_Pr_I_lines = DEFAULT_DSDBR01_SMBDFWD_MIN_SLOPE_CHANGE_OF_PR_I_LINE;
		}




		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBDFWD_LINKING_MAX_DISTANCE_BETWEEN_LINKED_JUMPS ),
			_DSDBR01_SMBDFWD_linking_max_distance_between_linked_jumps );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBDFWD_linking_max_distance_between_linked_jumps = DEFAULT_DSDBR01_SMBDFWD_LINKING_MAX_DISTANCE_BETWEEN_LINKED_JUMPS;
		}


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getDoubleValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBDFWD_LINKING_MIN_SHARPNESS ),
			_DSDBR01_SMBDFWD_linking_min_sharpness );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBDFWD_linking_min_sharpness = DEFAULT_DSDBR01_SMBDFWD_LINKING_MIN_SHARPNESS;
		}


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getDoubleValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBDFWD_LINKING_MAX_SHARPNESS ),
			_DSDBR01_SMBDFWD_linking_max_sharpness );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBDFWD_linking_max_sharpness = DEFAULT_DSDBR01_SMBDFWD_LINKING_MAX_SHARPNESS;
		}


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBDFWD_LINKING_MAX_GAP_BETWEEN_DOUBLE_LINES ),
			_DSDBR01_SMBDFWD_linking_max_gap_between_double_lines );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBDFWD_linking_max_gap_between_double_lines = DEFAULT_DSDBR01_SMBDFWD_LINKING_MAX_GAP_BETWEEN_DOUBLE_LINES;
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBDFWD_LINKING_MIN_GAP_BETWEEN_DOUBLE_LINES ),
			_DSDBR01_SMBDFWD_linking_min_gap_between_double_lines );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBDFWD_linking_min_gap_between_double_lines = DEFAULT_DSDBR01_SMBDFWD_LINKING_MIN_GAP_BETWEEN_DOUBLE_LINES;
		}



		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBDFWD_LINKING_NEAR_BOTTOM ),
			_DSDBR01_SMBDFWD_linking_near_bottom_row );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBDFWD_linking_near_bottom_row = DEFAULT_DSDBR01_SMBDFWD_LINKING_NEAR_BOTTOM;
		}


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getDoubleValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBDFWD_LINKING_SHARPNESS_NEAR_BOTTOM ),
			_DSDBR01_SMBDFWD_linking_sharpness_near_bottom_row );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBDFWD_linking_sharpness_near_bottom_row = DEFAULT_DSDBR01_SMBDFWD_LINKING_SHARPNESS_NEAR_BOTTOM;
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBDFWD_LINKING_MAX_DISTANCE_BETWEEN_LINKS_NEAR_BOTTOM ),
			_DSDBR01_SMBDFWD_linking_max_distance_near_bottom_row );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBDFWD_linking_max_distance_near_bottom_row = DEFAULT_DSDBR01_SMBDFWD_LINKING_MAX_DISTANCE_BETWEEN_LINKS_NEAR_BOTTOM;
		}



		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBDFWD_LINKING_MAX_DISTANCE_TO_LINK_SINGLE_UNLINKED_JUMP ),
			_DSDBR01_SMBDFWD_linking_max_distance_to_single_unlinked_jump );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBDFWD_linking_max_distance_to_single_unlinked_jump = DEFAULT_DSDBR01_SMBDFWD_LINKING_MAX_DISTANCE_TO_LINK_SINGLE_UNLINKED_JUMP;
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBDFWD_LINKING_MAX_Y_DISTANCE_TO_LINK ),
			_DSDBR01_SMBDFWD_linking_max_y_distance_to_link );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBDFWD_linking_max_y_distance_to_link = DEFAULT_DSDBR01_SMBDFWD_LINKING_MAX_Y_DISTANCE_TO_LINK;
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getDoubleValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBDFWD_LINKING_SHARPNESS_RAMP_SPLIT ),
			_DSDBR01_SMBDFWD_linking_sharpness_ramp_split );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBDFWD_linking_sharpness_ramp_split = DEFAULT_DSDBR01_SMBDFWD_LINKING_SHARPNESS_RAMP_SPLIT;
		}

	}
	else
	{
		rval2return = rtype::registry_key_does_not_exist;
	}



	// Reverse Supermode Map Boundary Detection parameters

	reg_key = CString( CG_DSDBR01_BASE_REGKEY ) + CString( RELATIVE_REGKEY_DSDBR01_SMBDREV );


	if( P_REG->keyExists( reg_key.GetBuffer() ) )
	{
		// Key exists, read in stored values, otherwise use defaults


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBDREV_I_RAMP_BOUNDARIES ),
			_DSDBR01_SMBDREV_I_ramp_boundaries_max );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBDREV_I_ramp_boundaries_max = DEFAULT_DSDBR01_SMBDREV_I_RAMP_BOUNDARIES;
		}



		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBDREV_BOUNDARIES_MIN_SEPARATION ),
			_DSDBR01_SMBDREV_boundaries_min_separation );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBDREV_boundaries_min_separation = DEFAULT_DSDBR01_SMBDREV_BOUNDARIES_MIN_SEPARATION;
		}


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getDoubleValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBDREV_MIN_D2PR_DI2_ZERO_CROSS_JUMP_SIZE ),
			_DSDBR01_SMBDREV_min_d2Pr_dI2_zero_cross_jump_size );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBDREV_min_d2Pr_dI2_zero_cross_jump_size = DEFAULT_DSDBR01_SMBDREV_MIN_D2PR_DI2_ZERO_CROSS_JUMP_SIZE;
		}



		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getDoubleValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBDREV_MAX_SUM_SQUARED_DIST_FROM_PR_I_LINE ),
			_DSDBR01_SMBDREV_max_sum_of_squared_distances_from_Pr_I_line );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBDREV_max_sum_of_squared_distances_from_Pr_I_line = DEFAULT_DSDBR01_SMBDREV_MAX_SUM_SQUARED_DIST_FROM_PR_I_LINE;
		}


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getDoubleValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBDREV_MIN_SLOPE_CHANGE_OF_PR_I_LINE ),
			_DSDBR01_SMBDREV_min_slope_change_of_Pr_I_lines );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBDREV_min_slope_change_of_Pr_I_lines = DEFAULT_DSDBR01_SMBDREV_MIN_SLOPE_CHANGE_OF_PR_I_LINE;
		}



		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBDREV_LINKING_MAX_DISTANCE_BETWEEN_LINKED_JUMPS ),
			_DSDBR01_SMBDREV_linking_max_distance_between_linked_jumps );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBDREV_linking_max_distance_between_linked_jumps = DEFAULT_DSDBR01_SMBDREV_LINKING_MAX_DISTANCE_BETWEEN_LINKED_JUMPS;
		}


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getDoubleValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBDREV_LINKING_MIN_SHARPNESS ),
			_DSDBR01_SMBDREV_linking_min_sharpness );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBDREV_linking_min_sharpness = DEFAULT_DSDBR01_SMBDREV_LINKING_MIN_SHARPNESS;
		}


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getDoubleValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBDREV_LINKING_MAX_SHARPNESS ),
			_DSDBR01_SMBDREV_linking_max_sharpness );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBDREV_linking_max_sharpness = DEFAULT_DSDBR01_SMBDREV_LINKING_MAX_SHARPNESS;
		}


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBDREV_LINKING_MAX_GAP_BETWEEN_DOUBLE_LINES ),
			_DSDBR01_SMBDREV_linking_max_gap_between_double_lines );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBDREV_linking_max_gap_between_double_lines = DEFAULT_DSDBR01_SMBDREV_LINKING_MAX_GAP_BETWEEN_DOUBLE_LINES;
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBDREV_LINKING_MIN_GAP_BETWEEN_DOUBLE_LINES ),
			_DSDBR01_SMBDREV_linking_min_gap_between_double_lines );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBDREV_linking_min_gap_between_double_lines = DEFAULT_DSDBR01_SMBDREV_LINKING_MIN_GAP_BETWEEN_DOUBLE_LINES;
		}



		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBDREV_LINKING_NEAR_BOTTOM ),
			_DSDBR01_SMBDREV_linking_near_bottom_row );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBDREV_linking_near_bottom_row = DEFAULT_DSDBR01_SMBDREV_LINKING_NEAR_BOTTOM;
		}


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getDoubleValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBDREV_LINKING_SHARPNESS_NEAR_BOTTOM ),
			_DSDBR01_SMBDREV_linking_sharpness_near_bottom_row );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBDREV_linking_sharpness_near_bottom_row = DEFAULT_DSDBR01_SMBDREV_LINKING_SHARPNESS_NEAR_BOTTOM;
		}


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBDREV_LINKING_MAX_DISTANCE_BETWEEN_LINKS_NEAR_BOTTOM ),
			_DSDBR01_SMBDREV_linking_max_distance_near_bottom_row );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBDREV_linking_max_distance_near_bottom_row = DEFAULT_DSDBR01_SMBDREV_LINKING_MAX_DISTANCE_BETWEEN_LINKS_NEAR_BOTTOM;
		}



		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBDREV_LINKING_MAX_DISTANCE_TO_LINK_SINGLE_UNLINKED_JUMP ),
			_DSDBR01_SMBDREV_linking_max_distance_to_single_unlinked_jump );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBDREV_linking_max_distance_to_single_unlinked_jump = DEFAULT_DSDBR01_SMBDREV_LINKING_MAX_DISTANCE_TO_LINK_SINGLE_UNLINKED_JUMP;
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBDREV_LINKING_MAX_Y_DISTANCE_TO_LINK ),
			_DSDBR01_SMBDREV_linking_max_y_distance_to_link );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBDREV_linking_max_y_distance_to_link = DEFAULT_DSDBR01_SMBDREV_LINKING_MAX_Y_DISTANCE_TO_LINK;
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getDoubleValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMBDREV_LINKING_SHARPNESS_RAMP_SPLIT ),
			_DSDBR01_SMBDREV_linking_sharpness_ramp_split );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMBDREV_linking_sharpness_ramp_split = DEFAULT_DSDBR01_SMBDREV_LINKING_SHARPNESS_RAMP_SPLIT;
		}

	}
	else
	{
		rval2return = rtype::registry_key_does_not_exist;
	}

	// Overall Map Quantitative Analysis parameters

	reg_key = CString( CG_DSDBR01_BASE_REGKEY ) + CString( RELATIVE_REGKEY_DSDBR01_OMQA );


	if( P_REG->keyExists( reg_key.GetBuffer() ) )
	{
		// Key exists, read in stored values, otherwise use defaults

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_OMQA_SLOPE_WINDOW_SIZE ),
			_DSDBR01_OMQA_slope_window_size );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_OMQA_slope_window_size = DEFAULT_DSDBR01_OMQA_SLOPE_WINDOW_SIZE;
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getDoubleValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_OMQA_MODAL_DISTORTION_MIN_X ),
			_DSDBR01_OMQA_modal_distortion_min_x );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_OMQA_modal_distortion_min_x = DEFAULT_DSDBR01_OMQA_MODAL_DISTORTION_MIN_X;
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getDoubleValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_OMQA_MODAL_DISTORTION_MAX_X ),
			_DSDBR01_OMQA_modal_distortion_max_x );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_OMQA_modal_distortion_max_x = DEFAULT_DSDBR01_OMQA_MODAL_DISTORTION_MAX_X;
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getDoubleValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_OMQA_MODAL_DISTORTION_MIN_Y ),
			_DSDBR01_OMQA_modal_distortion_min_y );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_OMQA_modal_distortion_min_y = DEFAULT_DSDBR01_OMQA_MODAL_DISTORTION_MIN_Y;
		}

	}
	else
	{
		rval2return = rtype::registry_key_does_not_exist;
	}


	////////////////////////////////////////////////////////////////////////////////////////////

	reg_key = CString( CG_DSDBR01_BASE_REGKEY ) + CString( RELATIVE_REGKEY_DSDBR01_SMQA );


	if( P_REG->keyExists( reg_key.GetBuffer() ) )
	{
		// Key exists, read in stored values, otherwise use defaults

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMQA_SLOPE_WINDOW_SIZE ),
			_DSDBR01_SMQA_slope_window_size );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMQA_slope_window_size = DEFAULT_DSDBR01_SMQA_SLOPE_WINDOW_SIZE;
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getDoubleValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMQA_MODAL_DISTORTION_MIN_X ),
			_DSDBR01_SMQA_modal_distortion_min_x );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMQA_modal_distortion_min_x = DEFAULT_DSDBR01_SMQA_MODAL_DISTORTION_MIN_X;
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getDoubleValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMQA_MODAL_DISTORTION_MAX_X ),
			_DSDBR01_SMQA_modal_distortion_max_x );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMQA_modal_distortion_max_x = DEFAULT_DSDBR01_SMQA_MODAL_DISTORTION_MAX_X;
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getDoubleValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMQA_MODAL_DISTORTION_MIN_Y ),
			_DSDBR01_SMQA_modal_distortion_min_y );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMQA_modal_distortion_min_y = DEFAULT_DSDBR01_SMQA_MODAL_DISTORTION_MIN_Y;
		}
	
		//GDM 31/10/06 Additional Params for Mode Width and Hysteresis analysis
		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getDoubleValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMQA_MODE_WIDTH_ANALYSIS_MIN_X ),
			_DSDBR01_SMQA_mode_width_analysis_min_x );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMQA_mode_width_analysis_min_x = DEFAULT_DSDBR01_SMQA_MODE_WIDTH_ANALYSIS_MIN_X;
		}

		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getDoubleValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMQA_MODE_WIDTH_ANALYSIS_MAX_X ),
			_DSDBR01_SMQA_mode_width_analysis_max_x );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMQA_mode_width_analysis_max_x = DEFAULT_DSDBR01_SMQA_MODE_WIDTH_ANALYSIS_MAX_X;
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getDoubleValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMQA_HYSTERESIS_ANALYSIS_MIN_X ),
			_DSDBR01_SMQA_hysteresis_analysis_min_x );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMQA_hysteresis_analysis_min_x = DEFAULT_DSDBR01_SMQA_HYSTERESIS_ANALYSIS_MIN_X;
		}

		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getDoubleValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMQA_HYSTERESIS_ANALYSIS_MAX_X ),
			_DSDBR01_SMQA_hysteresis_analysis_max_x );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMQA_hysteresis_analysis_max_x = DEFAULT_DSDBR01_SMQA_HYSTERESIS_ANALYSIS_MAX_X;
		}
		//GDM

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getBoolValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_SMQA_IGNORE_BDC_AT_FSC ),
			_DSDBR01_SMQA_ignore_DBC_at_FSC );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_SMQA_ignore_DBC_at_FSC = DEFAULT_DSDBR01_SMQA_IGNORE_BDC_AT_FSC;
		}

	}
	else
	{
		rval2return = rtype::registry_key_does_not_exist;
	}



	// Overall Map Pass Fail parameters

	reg_key = CString( CG_DSDBR01_BASE_REGKEY ) + CString( RELATIVE_REGKEY_DSDBR01_OMPF );


	if( P_REG->keyExists( reg_key.GetBuffer() ) )
	{
		// Key exists, read in stored values, otherwise use defaults

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getDoubleValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_OMPF_MAX_MIDDLE_LINE_RMS_VALUE ),
			_DSDBR01_OMPF_max_middle_line_rms_value );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_OMPF_max_middle_line_rms_value = DEFAULT_DSDBR01_OMPF_MAX_MIDDLE_LINE_RMS_VALUE;
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getDoubleValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_OMPF_MIN_MIDDLE_LINE_SLOPE ),
			_DSDBR01_OMPF_min_middle_line_slope );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_OMPF_min_middle_line_slope = DEFAULT_DSDBR01_OMPF_MIN_MIDDLE_LINE_SLOPE;
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getDoubleValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_OMPF_MAX_MIDDLE_LINE_SLOPE ),
			_DSDBR01_OMPF_max_middle_line_slope );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_OMPF_max_middle_line_slope = DEFAULT_DSDBR01_OMPF_MAX_MIDDLE_LINE_SLOPE;
		}

	}
	else
	{
		rval2return = rtype::registry_key_does_not_exist;
	}


	///////////////////////////////////////////////////////////////////////////////////////////
	///
	// Overall Map Pass Fail Thresholds
	//

	reg_key = CString( CG_DSDBR01_BASE_REGKEY ) + CString( RELATIVE_REGKEY_DSDBR01_OMPF );

	if( P_REG->keyExists( reg_key.GetBuffer() ) )
	{
		// Key exists, read in stored values, otherwise use defaults

		// get value from registry
		reg_rval = RegInterface::return_type::ok;

		///////////////////////////////////////////////////////////////////////////////////////////
		// get values

		// max continuity spacing
		reg_rval = P_REG->getDoubleValue(reg_key.GetBuffer(),
										CString( REGNAME_DSDBR01_OMPF_MAX_PR_GAP ),
										_DSDBR01_OMPF_max_pr_gap );
		if( reg_rval != RegInterface::return_type::ok )
		{
			_DSDBR01_OMPF_max_pr_gap = DEFAULT_DSDBR01_OMPF_MAX_PR_GAP;
			rval2return = rval;
		}

		// max Pr
		reg_rval = P_REG->getDoubleValue(reg_key.GetBuffer(),
										CString( REGNAME_DSDBR01_OMPF_MAX_LOWER_PR ),
										_DSDBR01_OMPF_max_lower_pr );
		if( reg_rval != RegInterface::return_type::ok )
		{
			_DSDBR01_OMPF_max_lower_pr = DEFAULT_DSDBR01_OMPF_MAX_LOWER_PR;
			rval2return = rval;
		}

		// min Pr
		reg_rval = P_REG->getDoubleValue(reg_key.GetBuffer(),
										CString( REGNAME_DSDBR01_OMPF_MIN_UPPER_PR ),
										_DSDBR01_OMPF_min_upper_pr );
		if( reg_rval != RegInterface::return_type::ok )
		{
			_DSDBR01_OMPF_min_upper_pr = DEFAULT_DSDBR01_OMPF_MIN_UPPER_PR;
			rval2return = rval;
		}

		// max mode borders removed
		reg_rval = P_REG->getIntValue(reg_key.GetBuffer(),
									CString( REGNAME_DSDBR01_OMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD ),
									_DSDBR01_OMPF_max_mode_borders_removed_threshold );
		if( reg_rval != RegInterface::return_type::ok )
		{
			_DSDBR01_OMPF_max_mode_borders_removed_threshold = DEFAULT_DSDBR01_OMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD;
			rval2return = rval;
		}

		// min mode width
		reg_rval = P_REG->getIntValue(reg_key.GetBuffer(),
									CString( REGNAME_DSDBR01_OMPF_MIN_MODE_WIDTH_THRESHOLD ),
									_DSDBR01_OMPF_min_mode_width_threshold );
		if( reg_rval != RegInterface::return_type::ok )
		{
			_DSDBR01_OMPF_min_mode_width_threshold = DEFAULT_DSDBR01_OMPF_MIN_MODE_WIDTH_THRESHOLD;
			rval2return = rval;
		}

		// max mode width
		reg_rval = P_REG->getIntValue(reg_key.GetBuffer(),
									CString( REGNAME_DSDBR01_OMPF_MAX_MODE_WIDTH_THRESHOLD ),
									_DSDBR01_OMPF_max_mode_width_threshold );
		if( reg_rval != RegInterface::return_type::ok )
		{
			_DSDBR01_OMPF_max_mode_width_threshold = DEFAULT_DSDBR01_OMPF_MAX_MODE_WIDTH_THRESHOLD;
			rval2return = rval;
		}

		// max mode bdc area
		reg_rval = P_REG->getDoubleValue(reg_key.GetBuffer(),
										CString( REGNAME_DSDBR01_OMPF_MAX_MODE_BDC_AREA_THRESHOLD ),
										_DSDBR01_OMPF_max_mode_bdc_area_threshold );
		if( reg_rval != RegInterface::return_type::ok )
		{
			_DSDBR01_OMPF_max_mode_bdc_area_threshold = DEFAULT_DSDBR01_OMPF_MAX_MODE_BDC_AREA_THRESHOLD;
			rval2return = rval;
		}

		// max mode bdc area x length
		reg_rval = P_REG->getDoubleValue(reg_key.GetBuffer(),
										CString( REGNAME_DSDBR01_OMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD ),
										_DSDBR01_OMPF_max_mode_bdc_area_x_length_threshold );
		if( reg_rval != RegInterface::return_type::ok )
		{
			_DSDBR01_OMPF_max_mode_bdc_area_x_length_threshold = DEFAULT_DSDBR01_OMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD;
			rval2return = rval;
		}

		// sum mode bdc areas
		reg_rval = P_REG->getDoubleValue(reg_key.GetBuffer(),
										CString( REGNAME_DSDBR01_OMPF_SUM_MODE_BDC_AREAS_THRESHOLD ),
										_DSDBR01_OMPF_sum_mode_bdc_areas_threshold );
		if( reg_rval != RegInterface::return_type::ok )
		{
			_DSDBR01_OMPF_sum_mode_bdc_areas_threshold = DEFAULT_DSDBR01_OMPF_SUM_MODE_BDC_AREAS_THRESHOLD;
			rval2return = rval;
		}

		// num mode bdc areas
		reg_rval = P_REG->getDoubleValue(reg_key.GetBuffer(),
										CString( REGNAME_DSDBR01_OMPF_NUM_MODE_BDC_AREAS_THRESHOLD ),
										_DSDBR01_OMPF_num_mode_bdc_areas_threshold );
		if( reg_rval != RegInterface::return_type::ok )
		{
			_DSDBR01_OMPF_num_mode_bdc_areas_threshold = DEFAULT_DSDBR01_OMPF_NUM_MODE_BDC_AREAS_THRESHOLD;
			rval2return = rval;
		}

		// modal distortion angle
		reg_rval = P_REG->getDoubleValue(reg_key.GetBuffer(),
										CString( REGNAME_DSDBR01_OMPF_MODAL_DISTORTION_ANGLE_THRESHOLD ),
										_DSDBR01_OMPF_modal_distortion_angle_threshold );
		if( reg_rval != RegInterface::return_type::ok )
		{
			_DSDBR01_OMPF_modal_distortion_angle_threshold = DEFAULT_DSDBR01_OMPF_MODAL_DISTORTION_ANGLE_THRESHOLD;
			rval2return = rval;
		}

		// modal distortion angle x position
		reg_rval = P_REG->getDoubleValue(reg_key.GetBuffer(),
										CString( REGNAME_DSDBR01_OMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD ),
										_DSDBR01_OMPF_modal_distortion_angle_x_pos_threshold );
		if( reg_rval != RegInterface::return_type::ok )
		{
			_DSDBR01_OMPF_modal_distortion_angle_x_pos_threshold = DEFAULT_DSDBR01_OMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD;
			rval2return = rval;
		}

		// mode line slope 
		reg_rval = P_REG->getDoubleValue(reg_key.GetBuffer(),
										CString( REGNAME_DSDBR01_OMPF_MAX_MODE_LINE_SLOPE_THRESHOLD ),
										_DSDBR01_OMPF_max_mode_line_slope_threshold );
		if( reg_rval != RegInterface::return_type::ok )
		{
			_DSDBR01_OMPF_max_mode_line_slope_threshold = DEFAULT_DSDBR01_OMPF_MAX_MODE_LINE_SLOPE_THRESHOLD;
			rval2return = rval;
		}


		// mode line slope 
		reg_rval = P_REG->getDoubleValue(reg_key.GetBuffer(),
										CString( REGNAME_DSDBR01_OMPF_MIN_MODE_LINE_SLOPE_THRESHOLD ),
										_DSDBR01_OMPF_min_mode_line_slope_threshold );
		if( reg_rval != RegInterface::return_type::ok )
		{
			_DSDBR01_OMPF_min_mode_line_slope_threshold = DEFAULT_DSDBR01_OMPF_MIN_MODE_LINE_SLOPE_THRESHOLD;
			rval2return = rval;
		}

		///////////////////////////////////////////////////////////////////////////////////////////

		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;

	}
	else
	{
		_DSDBR01_OMPF_max_mode_borders_removed_threshold		= DEFAULT_DSDBR01_OMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD;
		_DSDBR01_OMPF_max_pr_gap								= DEFAULT_DSDBR01_OMPF_MAX_PR_GAP;
		_DSDBR01_OMPF_max_lower_pr								= DEFAULT_DSDBR01_OMPF_MAX_LOWER_PR;
		_DSDBR01_OMPF_min_upper_pr								= DEFAULT_DSDBR01_OMPF_MIN_UPPER_PR;
		_DSDBR01_OMPF_min_mode_width_threshold					= DEFAULT_DSDBR01_OMPF_MIN_MODE_WIDTH_THRESHOLD;
		_DSDBR01_OMPF_max_mode_width_threshold					= DEFAULT_DSDBR01_OMPF_MAX_MODE_WIDTH_THRESHOLD;
		_DSDBR01_OMPF_max_mode_bdc_area_threshold				= DEFAULT_DSDBR01_OMPF_MAX_MODE_BDC_AREA_THRESHOLD;
		_DSDBR01_OMPF_max_mode_bdc_area_x_length_threshold		= DEFAULT_DSDBR01_OMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD;
		_DSDBR01_OMPF_sum_mode_bdc_areas_threshold				= DEFAULT_DSDBR01_OMPF_SUM_MODE_BDC_AREAS_THRESHOLD;
		_DSDBR01_OMPF_num_mode_bdc_areas_threshold				= DEFAULT_DSDBR01_OMPF_NUM_MODE_BDC_AREAS_THRESHOLD;
		_DSDBR01_OMPF_modal_distortion_angle_threshold			= DEFAULT_DSDBR01_OMPF_MODAL_DISTORTION_ANGLE_THRESHOLD;
		_DSDBR01_OMPF_modal_distortion_angle_x_pos_threshold	= DEFAULT_DSDBR01_OMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD;
		_DSDBR01_OMPF_max_mode_line_slope_threshold				= DEFAULT_DSDBR01_OMPF_MAX_MODE_LINE_SLOPE_THRESHOLD;
		_DSDBR01_OMPF_min_mode_line_slope_threshold				= DEFAULT_DSDBR01_OMPF_MIN_MODE_LINE_SLOPE_THRESHOLD;
		_DSDBR01_OMPF_max_middle_line_rms_value					= DEFAULT_DSDBR01_OMPF_MAX_MIDDLE_LINE_RMS_VALUE;
		_DSDBR01_OMPF_min_middle_line_slope						= DEFAULT_DSDBR01_OMPF_MIN_MIDDLE_LINE_SLOPE;
		_DSDBR01_OMPF_max_middle_line_slope						= DEFAULT_DSDBR01_OMPF_MAX_MIDDLE_LINE_SLOPE;

		rval2return = rtype::registry_key_does_not_exist;
	}



	// DSDBR01 Coarse Frequency parameters
	reg_key = CString( CG_DSDBR01_BASE_REGKEY ) + CString( RELATIVE_REGKEY_DSDBR01_CFREQ );

	if( P_REG->keyExists( reg_key.GetBuffer() ) )
	{
		// Key exists, read in stored values, otherwise use defaults


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getCStringValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_CFREQ_POLY_COEFFS_ABSPATH ),
			_DSDBR01_CFREQ_poly_coeffs_abspath );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_CFREQ_poly_coeffs_abspath = CString(DEFAULT_DSDBR01_CFREQ_POLY_COEFFS_ABSPATH);
		}


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_CFREQ_NUM_FREQ_MEAS_PER_OP ),
			_DSDBR01_CFREQ_num_freq_meas_per_op );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_CFREQ_num_freq_meas_per_op = DEFAULT_DSDBR01_CFREQ_NUM_FREQ_MEAS_PER_OP;
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_CFREQ_POWER_RATIO_MEAS_PER_OP ),
			_DSDBR01_CFREQ_num_power_ratio_meas_per_op );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_CFREQ_num_power_ratio_meas_per_op = DEFAULT_DSDBR01_CFREQ_POWER_RATIO_MEAS_PER_OP;
		}


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getDoubleValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_CFREQ_MAX_STABLE_FREQ_ERROR ),
			_DSDBR01_CFREQ_max_stable_freq_error );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_CFREQ_max_stable_freq_error = DEFAULT_DSDBR01_CFREQ_MAX_STABLE_FREQ_ERROR;
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_CFREQ_SAMPLE_POINT_SETTLE_TIME ),
			_DSDBR01_CFREQ_sample_point_settle_time );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_CFREQ_sample_point_settle_time = DEFAULT_DSDBR01_CFREQ_SAMPLE_POINT_SETTLE_TIME;
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getBoolValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_CFREQ_WRITE_SAMPLE_POINTS_TO_FILE ),
			_DSDBR01_CFREQ_write_sample_points_to_file );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_CFREQ_write_sample_points_to_file = DEFAULT_DSDBR01_CFREQ_WRITE_SAMPLE_POINTS_TO_FILE;
		}

	}
	else
	{
		rval2return = rtype::registry_key_does_not_exist;
	}


	// DSDBR01 ITU Grid

	reg_key = CString( CG_DSDBR01_BASE_REGKEY ) + CString( RELATIVE_REGKEY_DSDBR01_ITUGRID );

	if( P_REG->keyExists( reg_key.GetBuffer() ) )
	{
		// Key exists, read in stored values, otherwise use defaults


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getDoubleValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_ITUGRID_CENTRE_FREQ ),
			_DSDBR01_ITUGRID_centre_freq );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_ITUGRID_centre_freq = DEFAULT_DSDBR01_ITUGRID_CENTRE_FREQ;
		}


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getDoubleValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_ITUGRID_STEP_FREQ ),
			_DSDBR01_ITUGRID_step_freq );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_ITUGRID_step_freq = DEFAULT_DSDBR01_ITUGRID_STEP_FREQ;
		}



		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_ITUGRID_CHANNEL_COUNT ),
			_DSDBR01_ITUGRID_channel_count );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		 // if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_ITUGRID_channel_count = DEFAULT_DSDBR01_ITUGRID_CHANNEL_COUNT;
		}


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getCStringValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_ITUGRID_abspath ),
			_DSDBR01_ITUGRID_abspath );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_ITUGRID_abspath = CString(DEFAULT_DSDBR01_ITUGRID_abspath);
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getCStringValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_ESTIMATESITUGRID_abspath ),
			_DSDBR01_ESTIMATESITUGRID_abspath );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_ESTIMATESITUGRID_abspath = CString(DEFAULT_DSDBR01_ESTIMATESITUGRID_abspath);
		}
	}
	else
	{
		rval2return = rtype::registry_key_does_not_exist;
	}


	// DSDBR01 ITU Grid characterisation parameters

	reg_key = CString( CG_DSDBR01_BASE_REGKEY ) + CString( RELATIVE_REGKEY_DSDBR01_CHAR );

	if( P_REG->keyExists( reg_key.GetBuffer() ) )
	{
		// Key exists, read in stored values, otherwise use defaults

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_CHAR_NUM_FREQ_MEAS_PER_OP ),
			_DSDBR01_CHAR_num_freq_meas_per_op );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_CHAR_num_freq_meas_per_op = DEFAULT_DSDBR01_CHAR_NUM_FREQ_MEAS_PER_OP;
		}


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getDoubleValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_CHAR_FREQ_ACCURACY_OF_WAVEMETER ),
			_DSDBR01_CHAR_freq_accuracy_of_wavemeter );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_CHAR_freq_accuracy_of_wavemeter = DEFAULT_DSDBR01_CHAR_FREQ_ACCURACY_OF_WAVEMETER;
		}



		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_CHAR_OP_SETTLE_TIME ),
			_DSDBR01_CHAR_op_settle_time );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_CHAR_op_settle_time = DEFAULT_DSDBR01_CHAR_OP_SETTLE_TIME;
		}


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_CHAR_itusearch_scheme ),
			_DSDBR01_CHAR_itusearch_scheme );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_CHAR_itusearch_scheme = DEFAULT_DSDBR01_CHAR_itusearch_scheme;
		}


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_CHAR_itusearch_nextguess ),
			_DSDBR01_CHAR_itusearch_nextguess );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_CHAR_itusearch_nextguess = DEFAULT_DSDBR01_CHAR_itusearch_nextguess;
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_CHAR_itusearch_resamples ),
			_DSDBR01_CHAR_itusearch_resamples );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_CHAR_itusearch_resamples = DEFAULT_DSDBR01_CHAR_itusearch_resamples;
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_CHAR_POWER_RATIO_MEAS_PER_OP ),
			_DSDBR01_CHAR_num_power_ratio_meas_per_op );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_CHAR_num_power_ratio_meas_per_op = DEFAULT_DSDBR01_CHAR_POWER_RATIO_MEAS_PER_OP;
		}


		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getDoubleValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_CHAR_MAX_STABLE_FREQ_ERROR ),
			_DSDBR01_CHAR_max_stable_freq_error );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_CHAR_max_stable_freq_error = DEFAULT_DSDBR01_CHAR_MAX_STABLE_FREQ_ERROR;
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getBoolValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_CHAR_WRITE_DUFF_POINTS_TO_FILE ),
			_DSDBR01_CHAR_write_duff_points_to_file );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_CHAR_write_duff_points_to_file = DEFAULT_DSDBR01_CHAR_WRITE_DUFF_POINTS_TO_FILE;
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getBoolValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_CHAR_prevent_front_section_switch ),
			_DSDBR01_CHAR_prevent_front_section_switch );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_CHAR_prevent_front_section_switch = DEFAULT_DSDBR01_CHAR_prevent_front_section_switch;
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getCStringValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_CHAR_fakefreq_Iramp_module_name ),
			_DSDBR01_CHAR_fakefreq_Iramp_module_name );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_CHAR_fakefreq_Iramp_module_name = CString(DEFAULT_DSDBR01_CHAR_fakefreq_Iramp_module_name);
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getCStringValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_CHAR_Iramp_vs_fakefreq_abspath ),
			_DSDBR01_CHAR_Iramp_vs_fakefreq_abspath );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_CHAR_Iramp_vs_fakefreq_abspath = CString(DEFAULT_DSDBR01_CHAR_Iramp_vs_fakefreq_abspath);
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getDoubleValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_CHAR_fakefreq_max_error ),
			_DSDBR01_CHAR_fakefreq_max_error );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_CHAR_fakefreq_max_error = DEFAULT_DSDBR01_CHAR_fakefreq_max_error;
		}

	}
	else
	{
		rval2return = rtype::registry_key_does_not_exist;
	}


	// DSDBR01 TEC settling parameters

	reg_key = CString( CG_DSDBR01_BASE_REGKEY ) + CString( RELATIVE_REGKEY_DSDBR01_CHARTEC );

	if( P_REG->keyExists( reg_key.GetBuffer() ) )
	{
		// Key exists, read in stored values, otherwise use defaults

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_CHARTEC_time_window_size ),
			_DSDBR01_CHARTEC_time_window_size );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_CHARTEC_time_window_size = DEFAULT_DSDBR01_CHARTEC_time_window_size;
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getDoubleValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_CHARTEC_max_temp_deviation ),
			_DSDBR01_CHARTEC_max_temp_deviation );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_CHARTEC_max_temp_deviation = DEFAULT_DSDBR01_CHARTEC_max_temp_deviation;
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_CHARTEC_num_temp_readings ),
			_DSDBR01_CHARTEC_num_temp_readings );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_CHARTEC_num_temp_readings = DEFAULT_DSDBR01_CHARTEC_num_temp_readings;
		}

		// get value from registry
		reg_rval = RegInterface::return_type::ok;
		reg_rval = P_REG->getIntValue(
			reg_key.GetBuffer(),
			CString( REGNAME_DSDBR01_CHARTEC_max_settle_time ),
			_DSDBR01_CHARTEC_max_settle_time );
		// map error
		if( reg_rval == RegInterface::return_type::registry_key_error )
			rval = rtype::registry_key_error;
		if( reg_rval == RegInterface::return_type::registry_data_error )
			rval = rtype::registry_data_error;
		// if error, set default
		if( reg_rval != RegInterface::return_type::ok )
		{
			rval2return = rval;
			_DSDBR01_CHARTEC_max_settle_time = DEFAULT_DSDBR01_CHARTEC_max_settle_time;
		}

	}
	else
	{
		rval2return = rtype::registry_key_does_not_exist;
	}


	/////////////////////////////////////////////////////////////////////////////////////////////
	///
	// Supermode Map Pass Fail Thresholds
	//
	reg_key = CString( CG_DSDBR01_BASE_REGKEY );

	reg_rval = RegInterface::return_type::ok;

	for(int i=FIRST_SM; i<=LAST_SM; i++)
	{
		reg_key = CString( CG_DSDBR01_BASE_REGKEY );

		////////////////////////////////////////////
		switch(i)
		{
			case 0:
				reg_key = reg_key + CString( RELATIVE_REGKEY_DSDBR01_SMPF_SM0 );
			break;
			case 1:
				reg_key = reg_key + CString( RELATIVE_REGKEY_DSDBR01_SMPF_SM1 );
			break;
			case 2:
				reg_key = reg_key + CString( RELATIVE_REGKEY_DSDBR01_SMPF_SM2 );
			break;
			case 3:
				reg_key = reg_key + CString( RELATIVE_REGKEY_DSDBR01_SMPF_SM3 );
			break;
			case 4:
				reg_key = reg_key + CString( RELATIVE_REGKEY_DSDBR01_SMPF_SM4 );
			break;
			case 5:
				reg_key = reg_key + CString( RELATIVE_REGKEY_DSDBR01_SMPF_SM5 );
			break;
			case 6:
				reg_key = reg_key + CString( RELATIVE_REGKEY_DSDBR01_SMPF_SM6 );
			break;
			case 7:
				reg_key = reg_key + CString( RELATIVE_REGKEY_DSDBR01_SMPF_SM7 );
			break;
			case 8:
				reg_key = reg_key + CString( RELATIVE_REGKEY_DSDBR01_SMPF_SM8 );
			break;
			case 9:
				reg_key = reg_key + CString( RELATIVE_REGKEY_DSDBR01_SMPF_SM9 );
			break;
		}

		////////////////////////////////////////////

		int		maxModeWidth			= DEFAULT_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD;
		int		minModeWidth			= DEFAULT_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD;
		int		modeBordersRemoved		= DEFAULT_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD;
		double	maxMiddleLineRMSValue	= DEFAULT_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD;
		double	minMiddleLineSlope		= DEFAULT_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD;
		double	maxMiddleLineSlope		= DEFAULT_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD;
		//double	maxContinuitySpacing	= DEFAULT_DSDBR01_SMPF_MAX_CONTINUITY_SPACING_THRESHOLD;
		double	meanPercWorkingRegion	= DEFAULT_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD;
		double	minPercWorkingRegion	= DEFAULT_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD;
		double	modalDistortionAngle	= DEFAULT_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD;
		double	modalDistortionAngleXPos= DEFAULT_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD;
		double	maxModeBDCArea			= DEFAULT_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD;
		double	maxModeBDCAreaXLength	= DEFAULT_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD;
		double	sumModeBDCAreas			= DEFAULT_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD;
		double	numModeBDCAreas			= DEFAULT_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD;
		double	maxModeLineSlope		= DEFAULT_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD;

		if( P_REG->keyExists( reg_key.GetBuffer() ) )
		{

			if(reg_rval == RegInterface::return_type::ok)
			{
				reg_rval = P_REG->getIntValue(reg_key.GetBuffer(),
											CString(REGNAME_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD),
											maxModeWidth );
				if(reg_rval != RegInterface::return_type::ok)
					maxModeWidth = DEFAULT_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD;

				reg_rval = P_REG->getIntValue(reg_key.GetBuffer(),
											CString(REGNAME_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD),
											minModeWidth );
				if(reg_rval != RegInterface::return_type::ok)
					minModeWidth = DEFAULT_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD;

				reg_rval = P_REG->getIntValue(reg_key.GetBuffer(),
											CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD),
											modeBordersRemoved );
				if(reg_rval != RegInterface::return_type::ok)
					modeBordersRemoved = DEFAULT_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD;

				reg_rval = P_REG->getDoubleValue(reg_key.GetBuffer(),
											CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD),
											maxMiddleLineRMSValue );
				if(reg_rval != RegInterface::return_type::ok)
					maxMiddleLineRMSValue = DEFAULT_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD;

				reg_rval = P_REG->getDoubleValue(reg_key.GetBuffer(),
											CString(REGNAME_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD),
											minMiddleLineSlope );
				if(reg_rval != RegInterface::return_type::ok)
					minMiddleLineSlope = DEFAULT_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD;

				reg_rval = P_REG->getDoubleValue(reg_key.GetBuffer(),
											CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD),
											maxMiddleLineSlope );
				if(reg_rval != RegInterface::return_type::ok)
					maxMiddleLineSlope = DEFAULT_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD;

				//reg_rval = P_REG->getDoubleValue(reg_key.GetBuffer(),
				//							CString(REGNAME_DSDBR01_SMPF_MAX_CONTINUITY_SPACING_THRESHOLD),
				//							maxContinuitySpacing );
				//if(reg_rval != RegInterface::return_type::ok)
				//	maxContinuitySpacing = DEFAULT_DSDBR01_SMPF_MAX_CONTINUITY_SPACING_THRESHOLD;

				reg_rval = P_REG->getDoubleValue(reg_key.GetBuffer(),
											CString(REGNAME_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD),
											meanPercWorkingRegion );
				if(reg_rval != RegInterface::return_type::ok)
					meanPercWorkingRegion = DEFAULT_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD;

				reg_rval = P_REG->getDoubleValue(reg_key.GetBuffer(),
											CString(REGNAME_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD),
											minPercWorkingRegion );
				if(reg_rval != RegInterface::return_type::ok)
					minPercWorkingRegion = DEFAULT_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD;

				reg_rval = P_REG->getDoubleValue(reg_key.GetBuffer(),
											CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD),
											modalDistortionAngle );
				if(reg_rval != RegInterface::return_type::ok)
					modalDistortionAngle = DEFAULT_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD;

				reg_rval = P_REG->getDoubleValue(reg_key.GetBuffer(),
											CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD),
											modalDistortionAngleXPos );
				if(reg_rval != RegInterface::return_type::ok)
					modalDistortionAngleXPos = DEFAULT_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD;

				reg_rval = P_REG->getDoubleValue(reg_key.GetBuffer(),
											CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD),
											maxModeBDCArea );
				if(reg_rval != RegInterface::return_type::ok)
					maxModeBDCArea = DEFAULT_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD;

				reg_rval = P_REG->getDoubleValue(reg_key.GetBuffer(),
											CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD),
											maxModeBDCAreaXLength );
				if(reg_rval != RegInterface::return_type::ok)
					maxModeBDCAreaXLength = DEFAULT_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD;

				reg_rval = P_REG->getDoubleValue(reg_key.GetBuffer(),
											CString(REGNAME_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD),
											sumModeBDCAreas );
				if(reg_rval != RegInterface::return_type::ok)
					sumModeBDCAreas = DEFAULT_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD;

				reg_rval = P_REG->getDoubleValue(reg_key.GetBuffer(),
											CString(REGNAME_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD),
											numModeBDCAreas );
				if(reg_rval != RegInterface::return_type::ok)
					numModeBDCAreas = DEFAULT_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD;

				reg_rval = P_REG->getDoubleValue(reg_key.GetBuffer(),
											CString(REGNAME_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD),
											maxModeLineSlope );
				if(reg_rval != RegInterface::return_type::ok)
					maxModeLineSlope = DEFAULT_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD;

			}

			switch(i)
			{
				case 0:
				{
					_DSDBR01_SMPF_SM0_max_mode_width			= maxModeWidth;
					_DSDBR01_SMPF_SM0_min_mode_width			= minModeWidth;
					_DSDBR01_SMPF_SM0_max_mode_borders_removed	= modeBordersRemoved;
					_DSDBR01_SMPF_SM0_max_middle_line_rms_value	= maxMiddleLineRMSValue;
					_DSDBR01_SMPF_SM0_min_middle_line_slope		= minMiddleLineSlope;
					_DSDBR01_SMPF_SM0_max_middle_line_slope		= maxMiddleLineSlope;
					//_DSDBR01_SMPF_SM0_max_continuity_spacing	= maxContinuitySpacing;
					_DSDBR01_SMPF_SM0_mean_perc_working_region	= meanPercWorkingRegion;
					_DSDBR01_SMPF_SM0_min_perc_working_region	= minPercWorkingRegion;
					_DSDBR01_SMPF_SM0_modal_distortion_angle	= modalDistortionAngle;
					_DSDBR01_SMPF_SM0_modal_distortion_angle_x_pos	= modalDistortionAngleXPos;
					_DSDBR01_SMPF_SM0_max_mode_bdc_area			= maxModeBDCArea;
					_DSDBR01_SMPF_SM0_max_mode_bdc_area_x_length= maxModeBDCAreaXLength;
					_DSDBR01_SMPF_SM0_sum_mode_bdc_areas		= sumModeBDCAreas;
					_DSDBR01_SMPF_SM0_num_mode_bdc_areas		= numModeBDCAreas;
					_DSDBR01_SMPF_SM0_max_mode_line_slope		= maxModeLineSlope;
				}
				break;
				case 1:
				{
					_DSDBR01_SMPF_SM1_max_mode_width			= maxModeWidth;
					_DSDBR01_SMPF_SM1_min_mode_width			= minModeWidth;
					_DSDBR01_SMPF_SM1_max_mode_borders_removed	= modeBordersRemoved;
					_DSDBR01_SMPF_SM1_max_middle_line_rms_value	= maxMiddleLineRMSValue;
					_DSDBR01_SMPF_SM1_min_middle_line_slope		= minMiddleLineSlope;
					_DSDBR01_SMPF_SM1_max_middle_line_slope		= maxMiddleLineSlope;
					//_DSDBR01_SMPF_SM1_max_continuity_spacing	= maxContinuitySpacing;
					_DSDBR01_SMPF_SM1_mean_perc_working_region	= meanPercWorkingRegion;
					_DSDBR01_SMPF_SM1_min_perc_working_region	= minPercWorkingRegion;
					_DSDBR01_SMPF_SM1_modal_distortion_angle	= modalDistortionAngle;
					_DSDBR01_SMPF_SM1_modal_distortion_angle_x_pos	= modalDistortionAngleXPos;
					_DSDBR01_SMPF_SM1_max_mode_bdc_area			= maxModeBDCArea;
					_DSDBR01_SMPF_SM1_max_mode_bdc_area_x_length= maxModeBDCAreaXLength;
					_DSDBR01_SMPF_SM1_sum_mode_bdc_areas		= sumModeBDCAreas;
					_DSDBR01_SMPF_SM1_num_mode_bdc_areas		= numModeBDCAreas;
					_DSDBR01_SMPF_SM1_max_mode_line_slope		= maxModeLineSlope;
				}
				break;
				case 2:
				{
					_DSDBR01_SMPF_SM2_max_mode_width			= maxModeWidth;
					_DSDBR01_SMPF_SM2_min_mode_width			= minModeWidth;
					_DSDBR01_SMPF_SM2_max_mode_borders_removed	= modeBordersRemoved;
					_DSDBR01_SMPF_SM2_max_middle_line_rms_value	= maxMiddleLineRMSValue;
					_DSDBR01_SMPF_SM2_min_middle_line_slope		= minMiddleLineSlope;
					_DSDBR01_SMPF_SM2_max_middle_line_slope		= maxMiddleLineSlope;
					//_DSDBR01_SMPF_SM2_max_continuity_spacing	= maxContinuitySpacing;
					_DSDBR01_SMPF_SM2_mean_perc_working_region	= meanPercWorkingRegion;
					_DSDBR01_SMPF_SM2_min_perc_working_region	= minPercWorkingRegion;
					_DSDBR01_SMPF_SM2_modal_distortion_angle	= modalDistortionAngle;
					_DSDBR01_SMPF_SM2_modal_distortion_angle_x_pos	= modalDistortionAngleXPos;
					_DSDBR01_SMPF_SM2_max_mode_bdc_area			= maxModeBDCArea;
					_DSDBR01_SMPF_SM2_max_mode_bdc_area_x_length= maxModeBDCAreaXLength;
					_DSDBR01_SMPF_SM2_sum_mode_bdc_areas		= sumModeBDCAreas;
					_DSDBR01_SMPF_SM2_num_mode_bdc_areas		= numModeBDCAreas;
					_DSDBR01_SMPF_SM2_max_mode_line_slope		= maxModeLineSlope;
				}
				break;
				case 3:
				{
					_DSDBR01_SMPF_SM3_max_mode_width			= maxModeWidth;
					_DSDBR01_SMPF_SM3_min_mode_width			= minModeWidth;
					_DSDBR01_SMPF_SM3_max_mode_borders_removed	= modeBordersRemoved;
					_DSDBR01_SMPF_SM3_max_middle_line_rms_value	= maxMiddleLineRMSValue;
					_DSDBR01_SMPF_SM3_min_middle_line_slope		= minMiddleLineSlope;
					_DSDBR01_SMPF_SM3_max_middle_line_slope		= maxMiddleLineSlope;
					//_DSDBR01_SMPF_SM3_max_continuity_spacing	= maxContinuitySpacing;
					_DSDBR01_SMPF_SM3_mean_perc_working_region	= meanPercWorkingRegion;
					_DSDBR01_SMPF_SM3_min_perc_working_region	= minPercWorkingRegion;
					_DSDBR01_SMPF_SM3_modal_distortion_angle	= modalDistortionAngle;
					_DSDBR01_SMPF_SM3_modal_distortion_angle_x_pos	= modalDistortionAngleXPos;
					_DSDBR01_SMPF_SM3_max_mode_bdc_area			= maxModeBDCArea;
					_DSDBR01_SMPF_SM3_max_mode_bdc_area_x_length= maxModeBDCAreaXLength;
					_DSDBR01_SMPF_SM3_sum_mode_bdc_areas		= sumModeBDCAreas;
					_DSDBR01_SMPF_SM3_num_mode_bdc_areas		= numModeBDCAreas;
					_DSDBR01_SMPF_SM3_max_mode_line_slope		= maxModeLineSlope;
				}
				break;
				case 4:
				{
					_DSDBR01_SMPF_SM4_max_mode_width			= maxModeWidth;
					_DSDBR01_SMPF_SM4_min_mode_width			= minModeWidth;
					_DSDBR01_SMPF_SM4_max_mode_borders_removed	= modeBordersRemoved;
					_DSDBR01_SMPF_SM4_max_middle_line_rms_value	= maxMiddleLineRMSValue;
					_DSDBR01_SMPF_SM4_min_middle_line_slope		= minMiddleLineSlope;
					_DSDBR01_SMPF_SM4_max_middle_line_slope		= maxMiddleLineSlope;
					//_DSDBR01_SMPF_SM4_max_continuity_spacing	= maxContinuitySpacing;
					_DSDBR01_SMPF_SM4_mean_perc_working_region	= meanPercWorkingRegion;
					_DSDBR01_SMPF_SM4_min_perc_working_region	= minPercWorkingRegion;
					_DSDBR01_SMPF_SM4_modal_distortion_angle	= modalDistortionAngle;
					_DSDBR01_SMPF_SM4_modal_distortion_angle_x_pos	= modalDistortionAngleXPos;
					_DSDBR01_SMPF_SM4_max_mode_bdc_area			= maxModeBDCArea;
					_DSDBR01_SMPF_SM4_max_mode_bdc_area_x_length= maxModeBDCAreaXLength;
					_DSDBR01_SMPF_SM4_sum_mode_bdc_areas		= sumModeBDCAreas;
					_DSDBR01_SMPF_SM4_num_mode_bdc_areas		= numModeBDCAreas;
					_DSDBR01_SMPF_SM4_max_mode_line_slope		= maxModeLineSlope;
				}
				break;
				case 5:
				{
					_DSDBR01_SMPF_SM5_max_mode_width			= maxModeWidth;
					_DSDBR01_SMPF_SM5_min_mode_width			= minModeWidth;
					_DSDBR01_SMPF_SM5_max_mode_borders_removed	= modeBordersRemoved;
					_DSDBR01_SMPF_SM5_max_middle_line_rms_value	= maxMiddleLineRMSValue;
					_DSDBR01_SMPF_SM5_min_middle_line_slope		= minMiddleLineSlope;
					_DSDBR01_SMPF_SM5_max_middle_line_slope		= maxMiddleLineSlope;
					//_DSDBR01_SMPF_SM5_max_continuity_spacing	= maxContinuitySpacing;
					_DSDBR01_SMPF_SM5_mean_perc_working_region	= meanPercWorkingRegion;
					_DSDBR01_SMPF_SM5_min_perc_working_region	= minPercWorkingRegion;
					_DSDBR01_SMPF_SM5_modal_distortion_angle	= modalDistortionAngle;
					_DSDBR01_SMPF_SM5_modal_distortion_angle_x_pos	= modalDistortionAngleXPos;
					_DSDBR01_SMPF_SM5_max_mode_bdc_area			= maxModeBDCArea;
					_DSDBR01_SMPF_SM5_max_mode_bdc_area_x_length= maxModeBDCAreaXLength;
					_DSDBR01_SMPF_SM5_sum_mode_bdc_areas		= sumModeBDCAreas;
					_DSDBR01_SMPF_SM5_num_mode_bdc_areas		= numModeBDCAreas;
					_DSDBR01_SMPF_SM5_max_mode_line_slope		= maxModeLineSlope;
				}
				break;
				case 6:
				{
					_DSDBR01_SMPF_SM6_max_mode_width			= maxModeWidth;
					_DSDBR01_SMPF_SM6_min_mode_width			= minModeWidth;
					_DSDBR01_SMPF_SM6_max_mode_borders_removed	= modeBordersRemoved;
					_DSDBR01_SMPF_SM6_max_middle_line_rms_value	= maxMiddleLineRMSValue;
					_DSDBR01_SMPF_SM6_min_middle_line_slope		= minMiddleLineSlope;
					_DSDBR01_SMPF_SM6_max_middle_line_slope		= maxMiddleLineSlope;
					//_DSDBR01_SMPF_SM6_max_continuity_spacing	= maxContinuitySpacing;
					_DSDBR01_SMPF_SM6_mean_perc_working_region	= meanPercWorkingRegion;
					_DSDBR01_SMPF_SM6_min_perc_working_region	= minPercWorkingRegion;
					_DSDBR01_SMPF_SM6_modal_distortion_angle	= modalDistortionAngle;
					_DSDBR01_SMPF_SM6_modal_distortion_angle_x_pos	= modalDistortionAngleXPos;
					_DSDBR01_SMPF_SM6_max_mode_bdc_area			= maxModeBDCArea;
					_DSDBR01_SMPF_SM6_max_mode_bdc_area_x_length= maxModeBDCAreaXLength;
					_DSDBR01_SMPF_SM6_sum_mode_bdc_areas		= sumModeBDCAreas;
					_DSDBR01_SMPF_SM6_num_mode_bdc_areas		= numModeBDCAreas;
					_DSDBR01_SMPF_SM6_max_mode_line_slope		= maxModeLineSlope;
				}
				break;
				case 7:
				{
					_DSDBR01_SMPF_SM7_max_mode_width			= maxModeWidth;
					_DSDBR01_SMPF_SM7_min_mode_width			= minModeWidth;
					_DSDBR01_SMPF_SM7_max_mode_borders_removed	= modeBordersRemoved;
					_DSDBR01_SMPF_SM7_max_middle_line_rms_value	= maxMiddleLineRMSValue;
					_DSDBR01_SMPF_SM7_min_middle_line_slope		= minMiddleLineSlope;
					_DSDBR01_SMPF_SM7_max_middle_line_slope		= maxMiddleLineSlope;
					//_DSDBR01_SMPF_SM7_max_continuity_spacing	= maxContinuitySpacing;
					_DSDBR01_SMPF_SM7_mean_perc_working_region	= meanPercWorkingRegion;
					_DSDBR01_SMPF_SM7_min_perc_working_region	= minPercWorkingRegion;
					_DSDBR01_SMPF_SM7_modal_distortion_angle	= modalDistortionAngle;
					_DSDBR01_SMPF_SM7_modal_distortion_angle_x_pos	= modalDistortionAngleXPos;
					_DSDBR01_SMPF_SM7_max_mode_bdc_area			= maxModeBDCArea;
					_DSDBR01_SMPF_SM7_max_mode_bdc_area_x_length= maxModeBDCAreaXLength;
					_DSDBR01_SMPF_SM7_sum_mode_bdc_areas		= sumModeBDCAreas;
					_DSDBR01_SMPF_SM7_num_mode_bdc_areas		= numModeBDCAreas;
					_DSDBR01_SMPF_SM7_max_mode_line_slope		= maxModeLineSlope;
				}
				break;
				case 8:
				{
					_DSDBR01_SMPF_SM8_max_mode_width			= maxModeWidth;
					_DSDBR01_SMPF_SM8_min_mode_width			= minModeWidth;
					_DSDBR01_SMPF_SM8_max_mode_borders_removed	= modeBordersRemoved;
					_DSDBR01_SMPF_SM8_max_middle_line_rms_value	= maxMiddleLineRMSValue;
					_DSDBR01_SMPF_SM8_min_middle_line_slope		= minMiddleLineSlope;
					_DSDBR01_SMPF_SM8_max_middle_line_slope		= maxMiddleLineSlope;
					//_DSDBR01_SMPF_SM8_max_continuity_spacing	= maxContinuitySpacing;
					_DSDBR01_SMPF_SM8_mean_perc_working_region	= meanPercWorkingRegion;
					_DSDBR01_SMPF_SM8_min_perc_working_region	= minPercWorkingRegion;
					_DSDBR01_SMPF_SM8_modal_distortion_angle	= modalDistortionAngle;
					_DSDBR01_SMPF_SM8_modal_distortion_angle_x_pos	= modalDistortionAngleXPos;
					_DSDBR01_SMPF_SM8_max_mode_bdc_area			= maxModeBDCArea;
					_DSDBR01_SMPF_SM8_max_mode_bdc_area_x_length= maxModeBDCAreaXLength;
					_DSDBR01_SMPF_SM8_sum_mode_bdc_areas		= sumModeBDCAreas;
					_DSDBR01_SMPF_SM8_num_mode_bdc_areas		= numModeBDCAreas;
					_DSDBR01_SMPF_SM8_max_mode_line_slope		= maxModeLineSlope;
				}
				break;
				case 9:
				{
					_DSDBR01_SMPF_SM9_max_mode_width			= maxModeWidth;
					_DSDBR01_SMPF_SM9_min_mode_width			= minModeWidth;
					_DSDBR01_SMPF_SM9_max_mode_borders_removed	= modeBordersRemoved;
					_DSDBR01_SMPF_SM9_max_middle_line_rms_value	= maxMiddleLineRMSValue;
					_DSDBR01_SMPF_SM9_min_middle_line_slope		= minMiddleLineSlope;
					_DSDBR01_SMPF_SM9_max_middle_line_slope		= maxMiddleLineSlope;
					//_DSDBR01_SMPF_SM9_max_continuity_spacing	= maxContinuitySpacing;
					_DSDBR01_SMPF_SM9_mean_perc_working_region	= meanPercWorkingRegion;
					_DSDBR01_SMPF_SM9_min_perc_working_region	= minPercWorkingRegion;
					_DSDBR01_SMPF_SM9_modal_distortion_angle	= modalDistortionAngle;
					_DSDBR01_SMPF_SM9_modal_distortion_angle_x_pos	= modalDistortionAngleXPos;
					_DSDBR01_SMPF_SM9_max_mode_bdc_area			= maxModeBDCArea;
					_DSDBR01_SMPF_SM9_max_mode_bdc_area_x_length= maxModeBDCAreaXLength;
					_DSDBR01_SMPF_SM9_sum_mode_bdc_areas		= sumModeBDCAreas;
					_DSDBR01_SMPF_SM9_num_mode_bdc_areas		= numModeBDCAreas;
					_DSDBR01_SMPF_SM9_max_mode_line_slope		= maxModeLineSlope;
				}
				break;
			}

			if( reg_rval == RegInterface::return_type::registry_key_error )
				rval = rtype::registry_key_error;
			if( reg_rval == RegInterface::return_type::registry_data_error )
				rval = rtype::registry_data_error;

			// if error, set default
			if( reg_rval != RegInterface::return_type::ok )
			{
				rval2return = rval;
				break;
			}

		}
		else
		{
			////////////////////////////////////////////////////////////////////////////////////
			///
			//	Couldn't find the registry key - use defaults instead

			_DSDBR01_SMPF_SM0_max_mode_width			= maxModeWidth;
			_DSDBR01_SMPF_SM1_max_mode_width			= maxModeWidth;
			_DSDBR01_SMPF_SM2_max_mode_width			= maxModeWidth;
			_DSDBR01_SMPF_SM3_max_mode_width			= maxModeWidth;
			_DSDBR01_SMPF_SM4_max_mode_width			= maxModeWidth;
			_DSDBR01_SMPF_SM5_max_mode_width			= maxModeWidth;
			_DSDBR01_SMPF_SM6_max_mode_width			= maxModeWidth;
			_DSDBR01_SMPF_SM7_max_mode_width			= maxModeWidth;
			_DSDBR01_SMPF_SM8_max_mode_width			= maxModeWidth;
			_DSDBR01_SMPF_SM9_max_mode_width			= maxModeWidth;

			_DSDBR01_SMPF_SM0_min_mode_width			= minModeWidth;
			_DSDBR01_SMPF_SM1_min_mode_width			= minModeWidth;
			_DSDBR01_SMPF_SM2_min_mode_width			= minModeWidth;
			_DSDBR01_SMPF_SM3_min_mode_width			= minModeWidth;
			_DSDBR01_SMPF_SM4_min_mode_width			= minModeWidth;
			_DSDBR01_SMPF_SM5_min_mode_width			= minModeWidth;
			_DSDBR01_SMPF_SM6_min_mode_width			= minModeWidth;
			_DSDBR01_SMPF_SM7_min_mode_width			= minModeWidth;
			_DSDBR01_SMPF_SM8_min_mode_width			= minModeWidth;
			_DSDBR01_SMPF_SM9_min_mode_width			= minModeWidth;

			_DSDBR01_SMPF_SM0_max_mode_borders_removed	= modeBordersRemoved;
			_DSDBR01_SMPF_SM1_max_mode_borders_removed	= modeBordersRemoved;
			_DSDBR01_SMPF_SM2_max_mode_borders_removed	= modeBordersRemoved;
			_DSDBR01_SMPF_SM3_max_mode_borders_removed	= modeBordersRemoved;
			_DSDBR01_SMPF_SM4_max_mode_borders_removed	= modeBordersRemoved;
			_DSDBR01_SMPF_SM5_max_mode_borders_removed	= modeBordersRemoved;
			_DSDBR01_SMPF_SM6_max_mode_borders_removed	= modeBordersRemoved;
			_DSDBR01_SMPF_SM7_max_mode_borders_removed	= modeBordersRemoved;
			_DSDBR01_SMPF_SM8_max_mode_borders_removed	= modeBordersRemoved;
			_DSDBR01_SMPF_SM9_max_mode_borders_removed	= modeBordersRemoved;

			_DSDBR01_SMPF_SM0_max_middle_line_rms_value	= maxMiddleLineRMSValue;
			_DSDBR01_SMPF_SM1_max_middle_line_rms_value	= maxMiddleLineRMSValue;
			_DSDBR01_SMPF_SM2_max_middle_line_rms_value	= maxMiddleLineRMSValue;
			_DSDBR01_SMPF_SM3_max_middle_line_rms_value	= maxMiddleLineRMSValue;
			_DSDBR01_SMPF_SM4_max_middle_line_rms_value	= maxMiddleLineRMSValue;
			_DSDBR01_SMPF_SM5_max_middle_line_rms_value	= maxMiddleLineRMSValue;
			_DSDBR01_SMPF_SM6_max_middle_line_rms_value	= maxMiddleLineRMSValue;
			_DSDBR01_SMPF_SM7_max_middle_line_rms_value	= maxMiddleLineRMSValue;
			_DSDBR01_SMPF_SM8_max_middle_line_rms_value	= maxMiddleLineRMSValue;
			_DSDBR01_SMPF_SM9_max_middle_line_rms_value	= maxMiddleLineRMSValue;

			_DSDBR01_SMPF_SM0_min_middle_line_slope		= minMiddleLineSlope;
			_DSDBR01_SMPF_SM1_min_middle_line_slope		= minMiddleLineSlope;
			_DSDBR01_SMPF_SM2_min_middle_line_slope		= minMiddleLineSlope;
			_DSDBR01_SMPF_SM3_min_middle_line_slope		= minMiddleLineSlope;
			_DSDBR01_SMPF_SM4_min_middle_line_slope		= minMiddleLineSlope;
			_DSDBR01_SMPF_SM5_min_middle_line_slope		= minMiddleLineSlope;
			_DSDBR01_SMPF_SM6_min_middle_line_slope		= minMiddleLineSlope;
			_DSDBR01_SMPF_SM7_min_middle_line_slope		= minMiddleLineSlope;
			_DSDBR01_SMPF_SM8_min_middle_line_slope		= minMiddleLineSlope;
			_DSDBR01_SMPF_SM9_min_middle_line_slope		= minMiddleLineSlope;

			_DSDBR01_SMPF_SM0_max_middle_line_slope		= maxMiddleLineSlope;
			_DSDBR01_SMPF_SM1_max_middle_line_slope		= maxMiddleLineSlope;
			_DSDBR01_SMPF_SM2_max_middle_line_slope		= maxMiddleLineSlope;
			_DSDBR01_SMPF_SM3_max_middle_line_slope		= maxMiddleLineSlope;
			_DSDBR01_SMPF_SM4_max_middle_line_slope		= maxMiddleLineSlope;
			_DSDBR01_SMPF_SM5_max_middle_line_slope		= maxMiddleLineSlope;
			_DSDBR01_SMPF_SM6_max_middle_line_slope		= maxMiddleLineSlope;
			_DSDBR01_SMPF_SM7_max_middle_line_slope		= maxMiddleLineSlope;
			_DSDBR01_SMPF_SM8_max_middle_line_slope		= maxMiddleLineSlope;
			_DSDBR01_SMPF_SM9_max_middle_line_slope		= maxMiddleLineSlope;

			//_DSDBR01_SMPF_SM0_max_continuity_spacing	= maxContinuitySpacing;
			//_DSDBR01_SMPF_SM1_max_continuity_spacing	= maxContinuitySpacing;
			//_DSDBR01_SMPF_SM2_max_continuity_spacing	= maxContinuitySpacing;
			//_DSDBR01_SMPF_SM3_max_continuity_spacing	= maxContinuitySpacing;
			//_DSDBR01_SMPF_SM4_max_continuity_spacing	= maxContinuitySpacing;
			//_DSDBR01_SMPF_SM5_max_continuity_spacing	= maxContinuitySpacing;
			//_DSDBR01_SMPF_SM6_max_continuity_spacing	= maxContinuitySpacing;
			//_DSDBR01_SMPF_SM7_max_continuity_spacing	= maxContinuitySpacing;
			//_DSDBR01_SMPF_SM8_max_continuity_spacing	= maxContinuitySpacing;
			//_DSDBR01_SMPF_SM9_max_continuity_spacing	= maxContinuitySpacing;

			_DSDBR01_SMPF_SM0_mean_perc_working_region	= meanPercWorkingRegion;
			_DSDBR01_SMPF_SM1_mean_perc_working_region	= meanPercWorkingRegion;
			_DSDBR01_SMPF_SM2_mean_perc_working_region	= meanPercWorkingRegion;
			_DSDBR01_SMPF_SM3_mean_perc_working_region	= meanPercWorkingRegion;
			_DSDBR01_SMPF_SM4_mean_perc_working_region	= meanPercWorkingRegion;
			_DSDBR01_SMPF_SM5_mean_perc_working_region	= meanPercWorkingRegion;
			_DSDBR01_SMPF_SM6_mean_perc_working_region	= meanPercWorkingRegion;
			_DSDBR01_SMPF_SM7_mean_perc_working_region	= meanPercWorkingRegion;
			_DSDBR01_SMPF_SM8_mean_perc_working_region	= meanPercWorkingRegion;
			_DSDBR01_SMPF_SM9_mean_perc_working_region	= meanPercWorkingRegion;

			_DSDBR01_SMPF_SM0_min_perc_working_region	= minPercWorkingRegion;
			_DSDBR01_SMPF_SM1_min_perc_working_region	= minPercWorkingRegion;
			_DSDBR01_SMPF_SM2_min_perc_working_region	= minPercWorkingRegion;
			_DSDBR01_SMPF_SM3_min_perc_working_region	= minPercWorkingRegion;
			_DSDBR01_SMPF_SM4_min_perc_working_region	= minPercWorkingRegion;
			_DSDBR01_SMPF_SM5_min_perc_working_region	= minPercWorkingRegion;
			_DSDBR01_SMPF_SM6_min_perc_working_region	= minPercWorkingRegion;
			_DSDBR01_SMPF_SM7_min_perc_working_region	= minPercWorkingRegion;
			_DSDBR01_SMPF_SM8_min_perc_working_region	= minPercWorkingRegion;
			_DSDBR01_SMPF_SM9_min_perc_working_region	= minPercWorkingRegion;

			_DSDBR01_SMPF_SM0_modal_distortion_angle	= modalDistortionAngle;
			_DSDBR01_SMPF_SM1_modal_distortion_angle	= modalDistortionAngle;
			_DSDBR01_SMPF_SM2_modal_distortion_angle	= modalDistortionAngle;
			_DSDBR01_SMPF_SM3_modal_distortion_angle	= modalDistortionAngle;
			_DSDBR01_SMPF_SM4_modal_distortion_angle	= modalDistortionAngle;
			_DSDBR01_SMPF_SM5_modal_distortion_angle	= modalDistortionAngle;
			_DSDBR01_SMPF_SM6_modal_distortion_angle	= modalDistortionAngle;
			_DSDBR01_SMPF_SM7_modal_distortion_angle	= modalDistortionAngle;
			_DSDBR01_SMPF_SM8_modal_distortion_angle	= modalDistortionAngle;
			_DSDBR01_SMPF_SM9_modal_distortion_angle	= modalDistortionAngle;

			_DSDBR01_SMPF_SM0_modal_distortion_angle_x_pos	= modalDistortionAngleXPos;
			_DSDBR01_SMPF_SM1_modal_distortion_angle_x_pos	= modalDistortionAngleXPos;
			_DSDBR01_SMPF_SM2_modal_distortion_angle_x_pos	= modalDistortionAngleXPos;
			_DSDBR01_SMPF_SM3_modal_distortion_angle_x_pos	= modalDistortionAngleXPos;
			_DSDBR01_SMPF_SM4_modal_distortion_angle_x_pos	= modalDistortionAngleXPos;
			_DSDBR01_SMPF_SM5_modal_distortion_angle_x_pos	= modalDistortionAngleXPos;
			_DSDBR01_SMPF_SM6_modal_distortion_angle_x_pos	= modalDistortionAngleXPos;
			_DSDBR01_SMPF_SM7_modal_distortion_angle_x_pos	= modalDistortionAngleXPos;
			_DSDBR01_SMPF_SM8_modal_distortion_angle_x_pos	= modalDistortionAngleXPos;
			_DSDBR01_SMPF_SM9_modal_distortion_angle_x_pos	= modalDistortionAngleXPos;


			_DSDBR01_SMPF_SM0_max_mode_bdc_area			= maxModeBDCArea;
			_DSDBR01_SMPF_SM1_max_mode_bdc_area			= maxModeBDCArea;
			_DSDBR01_SMPF_SM2_max_mode_bdc_area			= maxModeBDCArea;
			_DSDBR01_SMPF_SM3_max_mode_bdc_area			= maxModeBDCArea;
			_DSDBR01_SMPF_SM4_max_mode_bdc_area			= maxModeBDCArea;
			_DSDBR01_SMPF_SM5_max_mode_bdc_area			= maxModeBDCArea;
			_DSDBR01_SMPF_SM6_max_mode_bdc_area			= maxModeBDCArea;
			_DSDBR01_SMPF_SM7_max_mode_bdc_area			= maxModeBDCArea;
			_DSDBR01_SMPF_SM8_max_mode_bdc_area			= maxModeBDCArea;
			_DSDBR01_SMPF_SM9_max_mode_bdc_area			= maxModeBDCArea;

			_DSDBR01_SMPF_SM0_max_mode_bdc_area_x_length= maxModeBDCAreaXLength;
			_DSDBR01_SMPF_SM1_max_mode_bdc_area_x_length= maxModeBDCAreaXLength;
			_DSDBR01_SMPF_SM2_max_mode_bdc_area_x_length= maxModeBDCAreaXLength;
			_DSDBR01_SMPF_SM3_max_mode_bdc_area_x_length= maxModeBDCAreaXLength;
			_DSDBR01_SMPF_SM4_max_mode_bdc_area_x_length= maxModeBDCAreaXLength;
			_DSDBR01_SMPF_SM5_max_mode_bdc_area_x_length= maxModeBDCAreaXLength;
			_DSDBR01_SMPF_SM6_max_mode_bdc_area_x_length= maxModeBDCAreaXLength;
			_DSDBR01_SMPF_SM7_max_mode_bdc_area_x_length= maxModeBDCAreaXLength;
			_DSDBR01_SMPF_SM8_max_mode_bdc_area_x_length= maxModeBDCAreaXLength;
			_DSDBR01_SMPF_SM9_max_mode_bdc_area_x_length= maxModeBDCAreaXLength;

			_DSDBR01_SMPF_SM0_sum_mode_bdc_areas		= sumModeBDCAreas;
			_DSDBR01_SMPF_SM1_sum_mode_bdc_areas		= sumModeBDCAreas;
			_DSDBR01_SMPF_SM2_sum_mode_bdc_areas		= sumModeBDCAreas;
			_DSDBR01_SMPF_SM3_sum_mode_bdc_areas		= sumModeBDCAreas;
			_DSDBR01_SMPF_SM4_sum_mode_bdc_areas		= sumModeBDCAreas;
			_DSDBR01_SMPF_SM5_sum_mode_bdc_areas		= sumModeBDCAreas;
			_DSDBR01_SMPF_SM6_sum_mode_bdc_areas		= sumModeBDCAreas;
			_DSDBR01_SMPF_SM7_sum_mode_bdc_areas		= sumModeBDCAreas;
			_DSDBR01_SMPF_SM8_sum_mode_bdc_areas		= sumModeBDCAreas;
			_DSDBR01_SMPF_SM9_sum_mode_bdc_areas		= sumModeBDCAreas;

			_DSDBR01_SMPF_SM0_num_mode_bdc_areas		= numModeBDCAreas;
			_DSDBR01_SMPF_SM1_num_mode_bdc_areas		= numModeBDCAreas;
			_DSDBR01_SMPF_SM2_num_mode_bdc_areas		= numModeBDCAreas;
			_DSDBR01_SMPF_SM3_num_mode_bdc_areas		= numModeBDCAreas;
			_DSDBR01_SMPF_SM4_num_mode_bdc_areas		= numModeBDCAreas;
			_DSDBR01_SMPF_SM5_num_mode_bdc_areas		= numModeBDCAreas;
			_DSDBR01_SMPF_SM6_num_mode_bdc_areas		= numModeBDCAreas;
			_DSDBR01_SMPF_SM7_num_mode_bdc_areas		= numModeBDCAreas;
			_DSDBR01_SMPF_SM8_num_mode_bdc_areas		= numModeBDCAreas;
			_DSDBR01_SMPF_SM9_num_mode_bdc_areas		= numModeBDCAreas;

			_DSDBR01_SMPF_SM0_max_mode_line_slope		= maxModeLineSlope;
			_DSDBR01_SMPF_SM1_max_mode_line_slope		= maxModeLineSlope;
			_DSDBR01_SMPF_SM2_max_mode_line_slope		= maxModeLineSlope;
			_DSDBR01_SMPF_SM3_max_mode_line_slope		= maxModeLineSlope;
			_DSDBR01_SMPF_SM4_max_mode_line_slope		= maxModeLineSlope;
			_DSDBR01_SMPF_SM5_max_mode_line_slope		= maxModeLineSlope;
			_DSDBR01_SMPF_SM6_max_mode_line_slope		= maxModeLineSlope;
			_DSDBR01_SMPF_SM7_max_mode_line_slope		= maxModeLineSlope;
			_DSDBR01_SMPF_SM8_max_mode_line_slope		= maxModeLineSlope;
			_DSDBR01_SMPF_SM9_max_mode_line_slope		= maxModeLineSlope;

			////////////////////////////////////////////////////////////////////////////////////

			rval2return = rtype::registry_key_does_not_exist;

			break;
		}

		////////////////////////////////////////////

	} //for
	//
	///
	////////////////////////////////////////////////////////////////////////////////////////////////


	return rval2return;
}


//
/////////////////////////////////////////////////////////////////
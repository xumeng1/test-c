// Copyright (c) 2004 PXIT. All Rights Reserved
// 
// FileName    : CGRegValues.h
// Description : Declaration of CCGRegValues class
//               Represents all Registry stored data
//               Singleton class 
//
// Rev#  | Date          | Author               | Description of change
// ---------------------------------------------------------------------
// 0.1   | 26 July 2004  | Frank D'Arcy         | Initial Draft
// 0.2   | 12 Aug  2004  | Frank D'Arcy         | Cleaned up
//       |               |                      | Added Moving Average Num
// 0.3   | 10 Sept 2004  | Frank D'Arcy         | Added DSDBR01 for each entry
// 0.4   | 19 Sept 2004  | Frank D'Arcy         | Added _DSDBR01_OMBD_read_boundaries_from_file
//       |               |                      | and _DSDBR01_OMBD_write_boundaries_to_file
// 0.5   | 23 Sept 2004  | Frank D'Arcy         | Added Overall Map data collection parameters _DSDBR01_OMDC_
// 0.6   | 27 Sept 2004  | Frank D'Arcy         | Moving some common data collection params,
//       |               |                      | and adding option to overwrite files or not
// 0.7   | 28 Sept 2004  | Frank D'Arcy         | Adding some supermode data collection params
// 0.8   | 30 Sept 2004  | Frank D'Arcy         | Adding min length of supermode middle line
// 0.9   | 05 Oct  2004  | Frank D'Arcy         | Adding option to stub calls to CloseGrid Research
// 1.0   | 18 Oct  2004  | Frank D'Arcy         | Adding Boundary Detection parameters for supermode maps
// 1.1   | 29 Oct  2004  | Frank D'Arcy         | Adding more Boundary Detection parameters for supermode maps
// 1.2   | 02 Nov  2004  | Frank D'Arcy         | Adding coarse frequency parameters
// 1.3   | 15 Nov  2004  | Frank D'Arcy         | Adding ITU Grid parameters
// 1.4   | 13 Dec  2004  | Frank D'Arcy         | Adding option to ignore BDC pass/fail with front section change
// 1.5   | 11 Feb  2005  | Frank D'Arcy         | Adding options to output filtered maps and photodiode current maps
// 1.6   | 26 May  2005  | Frank D'Arcy         | Adding option to reinit wavemeter on bad read
// 1.7   | 31 Oct  2006  | gareth Marsden		| Adding window min and max for Mode Width and Hysteresis analysis

#pragma once

#include <vector>

#define CG_REG CCGRegValues::instance()
#define CG_REG_RTYPE CCGRegValues::rtype


#define REG_BOOL_TRUE_VALUE "true"
#define REG_BOOL_FALSE_VALUE "false"

class CCGRegEntry
{
public:
	typedef enum datatype
	{
		_bool = 0,
		_int,
		_CString,
		_double
	};

	CString reg_key_name;
	CString reg_entry_name;
	datatype reg_entry_datatype;

};


class CCGRegValues
{
public:

	typedef enum rtype
	{
		ok = 0,
		registry_key_error,
		registry_data_error,
		registry_key_does_not_exist,
		invalid_value
	};

	static CCGRegValues* CCGRegValues::instance();

	~CCGRegValues(void);

	void setDefaults();

	std::vector<CCGRegEntry> _reg_entries;

	CString getText(CCGRegEntry reg_entry);
	void getSMText(CCGRegEntry reg_entry, CString &text );

	rtype setCString(CCGRegEntry reg_entry, CString value);
	rtype setDouble(CCGRegEntry reg_entry, double value);
	rtype setInt(CCGRegEntry reg_entry, int value);
	rtype setBool(CCGRegEntry reg_entry, bool value);


	rtype retrieveAllFromRegistry();
	rtype saveAllToRegistry();


	// DSDBR01 Base parameters

    CString get_DSDBR01_BASE_results_directory();
    rtype set_DSDBR01_BASE_results_directory( CString value );

    bool get_DSDBR01_BASE_stub_hardware_calls();
    rtype set_DSDBR01_BASE_stub_hardware_calls( bool value );

	// DSDBR01 Common Data Collection parameters

    CString get_DSDBR01_CMDC_gain_module_name();
    rtype set_DSDBR01_CMDC_gain_module_name( CString value );

    CString get_DSDBR01_CMDC_soa_module_name();
    rtype set_DSDBR01_CMDC_soa_module_name( CString value );

    CString get_DSDBR01_CMDC_rear_module_name();
    rtype set_DSDBR01_CMDC_rear_module_name( CString value );

    CString get_DSDBR01_CMDC_phase_module_name();
    rtype set_DSDBR01_CMDC_phase_module_name( CString value );

    CString get_DSDBR01_CMDC_front1_module_name();
    rtype set_DSDBR01_CMDC_front1_module_name( CString value );

    CString get_DSDBR01_CMDC_front2_module_name();
    rtype set_DSDBR01_CMDC_front2_module_name( CString value );

    CString get_DSDBR01_CMDC_front3_module_name();
    rtype set_DSDBR01_CMDC_front3_module_name( CString value );

    CString get_DSDBR01_CMDC_front4_module_name();
    rtype set_DSDBR01_CMDC_front4_module_name( CString value );

    CString get_DSDBR01_CMDC_front5_module_name();
    rtype set_DSDBR01_CMDC_front5_module_name( CString value );

    CString get_DSDBR01_CMDC_front6_module_name();
    rtype set_DSDBR01_CMDC_front6_module_name( CString value );

    CString get_DSDBR01_CMDC_front7_module_name();
    rtype set_DSDBR01_CMDC_front7_module_name( CString value );

    CString get_DSDBR01_CMDC_front8_module_name();
    rtype set_DSDBR01_CMDC_front8_module_name( CString value );

    CString get_DSDBR01_CMDC_306II_module_name();
    rtype set_DSDBR01_CMDC_306II_module_name( CString value );

    int get_DSDBR01_CMDC_306II_direct_channel();
    rtype set_DSDBR01_CMDC_306II_direct_channel( int value );

    int get_DSDBR01_CMDC_306II_filtered_channel();
    rtype set_DSDBR01_CMDC_306II_filtered_channel( int value );

    CString get_DSDBR01_CMDC_306EE_module_name();
    rtype set_DSDBR01_CMDC_306EE_module_name( CString value );

    int get_DSDBR01_CMDC_306EE_photodiode1_channel();
    rtype set_DSDBR01_CMDC_306EE_photodiode1_channel( int value );

    int get_DSDBR01_CMDC_306EE_photodiode2_channel();
    rtype set_DSDBR01_CMDC_306EE_photodiode2_channel( int value );

    int get_DSDBR01_CMDC_wavemeter_retries();
    rtype set_DSDBR01_CMDC_wavemeter_retries( int value );

	// DSDBR01 Overall Map Data Collection parameters

	#define OMDC_RAMP_REAR "R"
	#define OMDC_RAMP_FRONT "F"
    CString get_DSDBR01_OMDC_ramp_direction();
    rtype set_DSDBR01_OMDC_ramp_direction( CString value );

	bool get_DSDBR01_OMDC_forward_and_reverse();
	rtype set_DSDBR01_OMDC_forward_and_reverse( bool value );

	bool get_DSDBR01_OMDC_measure_photodiode_currents();
	rtype set_DSDBR01_OMDC_measure_photodiode_currents( bool value );

	bool get_DSDBR01_OMDC_write_filtered_power_maps();
	rtype set_DSDBR01_OMDC_write_filtered_power_maps( bool value );

    CString get_DSDBR01_OMDC_rear_currents_abspath();
    rtype set_DSDBR01_OMDC_rear_currents_abspath( CString value );

    CString get_DSDBR01_OMDC_front_currents_abspath();
    rtype set_DSDBR01_OMDC_front_currents_abspath( CString value );

    int get_DSDBR01_OMDC_source_delay();
    rtype set_DSDBR01_OMDC_source_delay( int value );

    int get_DSDBR01_OMDC_measure_delay();
    rtype set_DSDBR01_OMDC_measure_delay( int value );

	bool get_DSDBR01_OMDC_overwrite_existing_files();
	rtype set_DSDBR01_OMDC_overwrite_existing_files( bool value );

	// DSDBR01 Overall Map Boundary Detection parameters

	bool get_DSDBR01_OMBD_read_boundaries_from_file();
	rtype set_DSDBR01_OMBD_read_boundaries_from_file( bool value );

	bool get_DSDBR01_OMBD_write_boundaries_to_file();
	rtype set_DSDBR01_OMBD_write_boundaries_to_file( bool value );

	int get_DSDBR01_OMBD_median_filter_rank();
	rtype set_DSDBR01_OMBD_median_filter_rank( int value );

    double get_DSDBR01_OMBD_max_deltaPr_in_sm();
    rtype set_DSDBR01_OMBD_max_deltaPr_in_sm( double value );

    int get_DSDBR01_OMBD_min_points_width_of_sm();
    rtype set_DSDBR01_OMBD_min_points_width_of_sm( int value );

    int get_DSDBR01_OMBD_ml_moving_ave_filter_n();
    rtype set_DSDBR01_OMBD_ml_moving_ave_filter_n( int value );

    int get_DSDBR01_OMBD_ml_min_length();
    rtype set_DSDBR01_OMBD_ml_min_length( int value );

    bool get_DSDBR01_OMBD_ml_extend_to_corner();
    rtype set_DSDBR01_OMBD_ml_extend_to_corner( bool value );


	// DSDBR01 Supermode Map Data Collection parameters

	#define SMDC_RAMP_MIDDLE_LINE "M"
	#define SMDC_RAMP_PHASE "P"
    CString get_DSDBR01_SMDC_ramp_direction();
    rtype set_DSDBR01_SMDC_ramp_direction( CString value );

    CString get_DSDBR01_SMDC_phase_currents_abspath();
    rtype set_DSDBR01_SMDC_phase_currents_abspath( CString value );

	bool get_DSDBR01_SMDC_measure_photodiode_currents();
	rtype set_DSDBR01_SMDC_measure_photodiode_currents( bool value );

	bool get_DSDBR01_SMDC_write_filtered_power_maps();
	rtype set_DSDBR01_SMDC_write_filtered_power_maps( bool value );

    int get_DSDBR01_SMDC_source_delay();
    rtype set_DSDBR01_SMDC_source_delay( int value );

    int get_DSDBR01_SMDC_measure_delay();
    rtype set_DSDBR01_SMDC_measure_delay( int value );

	bool get_DSDBR01_SMDC_overwrite_existing_files();
	rtype set_DSDBR01_SMDC_overwrite_existing_files( bool value );

	// DSDBR01 Supermode Map Boundary Detection parameters

	bool get_DSDBR01_SMBD_read_boundaries_from_file();
	rtype set_DSDBR01_SMBD_read_boundaries_from_file( bool value );

	bool get_DSDBR01_SMBD_write_boundaries_to_file();
	rtype set_DSDBR01_SMBD_write_boundaries_to_file( bool value );

	bool get_DSDBR01_SMBD_write_middle_of_upper_lines_to_file();
	rtype set_DSDBR01_SMBD_write_middle_of_upper_lines_to_file( bool value );

	bool get_DSDBR01_SMBD_write_middle_of_lower_lines_to_file();
	rtype set_DSDBR01_SMBD_write_middle_of_lower_lines_to_file( bool value );

	bool get_DSDBR01_SMBD_write_debug_files();
	rtype set_DSDBR01_SMBD_write_debug_files( bool value );

	bool get_DSDBR01_SMBD_extrapolate_to_map_edges();
	rtype set_DSDBR01_SMBD_extrapolate_to_map_edges( bool value );

	int get_DSDBR01_SMBD_median_filter_rank();
	rtype set_DSDBR01_SMBD_median_filter_rank( int value );

    double get_DSDBR01_SMBD_max_deltaPr_in_lm();
    rtype set_DSDBR01_SMBD_max_deltaPr_in_lm( double value );

    int get_DSDBR01_SMBD_min_points_width_of_lm();
    rtype set_DSDBR01_SMBD_min_points_width_of_lm( int value );

    int get_DSDBR01_SMBD_max_points_width_of_lm();
    rtype set_DSDBR01_SMBD_max_points_width_of_lm( int value );

    int get_DSDBR01_SMBD_ml_moving_ave_filter_n();
    rtype set_DSDBR01_SMBD_ml_moving_ave_filter_n( int value );

    int get_DSDBR01_SMBD_ml_min_length();
    rtype set_DSDBR01_SMBD_ml_min_length( int value );

    double get_DSDBR01_SMBD_vertical_percent_shift();
    rtype set_DSDBR01_SMBD_vertical_percent_shift( double value );

    double get_DSDBR01_SMBD_zprc_hysteresis_upper_threshold();
    rtype set_DSDBR01_SMBD_zprc_hysteresis_upper_threshold( double value );

	double get_DSDBR01_SMBD_zprc_hysteresis_lower_threshold();
    rtype set_DSDBR01_SMBD_zprc_hysteresis_lower_threshold( double value );

	double get_DSDBR01_SMBD_zprc_max_hysteresis_rise();
    rtype set_DSDBR01_SMBD_zprc_max_hysteresis_rise( double value );

    int get_DSDBR01_SMBD_zprc_min_points_separation();
    rtype set_DSDBR01_SMBD_zprc_min_points_separation( int value );

    int get_DSDBR01_SMBD_zprc_max_points_to_reverse_jump();
    rtype set_DSDBR01_SMBD_zprc_max_points_to_reverse_jump( int value );

    int get_DSDBR01_SMBD_zprc_max_hysteresis_points();
    rtype set_DSDBR01_SMBD_zprc_max_hysteresis_points( int value );

    CString get_DSDBR01_SMBD_ramp_abspath();
    rtype set_DSDBR01_SMBD_ramp_abspath( CString value );

	// DSDBR01 Forward Supermode Map Boundary Detection parameters

    int get_DSDBR01_SMBDFWD_I_ramp_boundaries_max();
    rtype set_DSDBR01_SMBDFWD_I_ramp_boundaries_max( int value );

    int get_DSDBR01_SMBDFWD_boundaries_min_separation();
    rtype set_DSDBR01_SMBDFWD_boundaries_min_separation( int value );

    double get_DSDBR01_SMBDFWD_min_d2Pr_dI2_zero_cross_jump_size();
    rtype set_DSDBR01_SMBDFWD_min_d2Pr_dI2_zero_cross_jump_size( double value );

    double get_DSDBR01_SMBDFWD_max_sum_of_squared_distances_from_Pr_I_line();
    rtype set_DSDBR01_SMBDFWD_max_sum_of_squared_distances_from_Pr_I_line( double value );

    double get_DSDBR01_SMBDFWD_min_slope_change_of_Pr_I_lines();
    rtype set_DSDBR01_SMBDFWD_min_slope_change_of_Pr_I_lines( double value );

    int get_DSDBR01_SMBDFWD_linking_max_distance_between_linked_jumps();
    rtype set_DSDBR01_SMBDFWD_linking_max_distance_between_linked_jumps( int value );

    double get_DSDBR01_SMBDFWD_linking_min_sharpness();
    rtype set_DSDBR01_SMBDFWD_linking_min_sharpness( double value );

    double get_DSDBR01_SMBDFWD_linking_max_sharpness();
    rtype set_DSDBR01_SMBDFWD_linking_max_sharpness( double value );

    int get_DSDBR01_SMBDFWD_linking_max_gap_between_double_lines();
    rtype set_DSDBR01_SMBDFWD_linking_max_gap_between_double_lines( int value );

    int get_DSDBR01_SMBDFWD_linking_min_gap_between_double_lines();
    rtype set_DSDBR01_SMBDFWD_linking_min_gap_between_double_lines( int value );

    int get_DSDBR01_SMBDFWD_linking_near_bottom_row();
    rtype set_DSDBR01_SMBDFWD_linking_near_bottom_row( int value );

    double get_DSDBR01_SMBDFWD_linking_sharpness_near_bottom_row();
    rtype set_DSDBR01_SMBDFWD_linking_sharpness_near_bottom_row( double value );

    int get_DSDBR01_SMBDFWD_linking_max_distance_near_bottom_row();
    rtype set_DSDBR01_SMBDFWD_linking_max_distance_near_bottom_row( int value );

    int get_DSDBR01_SMBDFWD_linking_max_distance_to_single_unlinked_jump();
    rtype set_DSDBR01_SMBDFWD_linking_max_distance_to_single_unlinked_jump( int value );

    int get_DSDBR01_SMBDFWD_linking_max_y_distance_to_link();
    rtype set_DSDBR01_SMBDFWD_linking_max_y_distance_to_link( int value );

    double get_DSDBR01_SMBDFWD_linking_sharpness_ramp_split();
    rtype set_DSDBR01_SMBDFWD_linking_sharpness_ramp_split( double value );

	// DSDBR01 Reverse Supermode Map Boundary Detection parameters

    int get_DSDBR01_SMBDREV_I_ramp_boundaries_max();
    rtype set_DSDBR01_SMBDREV_I_ramp_boundaries_max( int value );

    int get_DSDBR01_SMBDREV_boundaries_min_separation();
    rtype set_DSDBR01_SMBDREV_boundaries_min_separation( int value );

    double get_DSDBR01_SMBDREV_min_d2Pr_dI2_zero_cross_jump_size();
    rtype set_DSDBR01_SMBDREV_min_d2Pr_dI2_zero_cross_jump_size( double value );

    double get_DSDBR01_SMBDREV_max_sum_of_squared_distances_from_Pr_I_line();
    rtype set_DSDBR01_SMBDREV_max_sum_of_squared_distances_from_Pr_I_line( double value );

    double get_DSDBR01_SMBDREV_min_slope_change_of_Pr_I_lines();
    rtype set_DSDBR01_SMBDREV_min_slope_change_of_Pr_I_lines( double value );

    int get_DSDBR01_SMBDREV_linking_max_distance_between_linked_jumps();
    rtype set_DSDBR01_SMBDREV_linking_max_distance_between_linked_jumps( int value );

    double get_DSDBR01_SMBDREV_linking_min_sharpness();
    rtype set_DSDBR01_SMBDREV_linking_min_sharpness( double value );

    double get_DSDBR01_SMBDREV_linking_max_sharpness();
    rtype set_DSDBR01_SMBDREV_linking_max_sharpness( double value );

    int get_DSDBR01_SMBDREV_linking_max_gap_between_double_lines();
    rtype set_DSDBR01_SMBDREV_linking_max_gap_between_double_lines( int value );

    int get_DSDBR01_SMBDREV_linking_min_gap_between_double_lines();
    rtype set_DSDBR01_SMBDREV_linking_min_gap_between_double_lines( int value );

    int get_DSDBR01_SMBDREV_linking_near_bottom_row();
    rtype set_DSDBR01_SMBDREV_linking_near_bottom_row( int value );

    double get_DSDBR01_SMBDREV_linking_sharpness_near_bottom_row();
    rtype set_DSDBR01_SMBDREV_linking_sharpness_near_bottom_row( double value );

    int get_DSDBR01_SMBDREV_linking_max_distance_near_bottom_row();
    rtype set_DSDBR01_SMBDREV_linking_max_distance_near_bottom_row( int value );

    int get_DSDBR01_SMBDREV_linking_max_distance_to_single_unlinked_jump();
    rtype set_DSDBR01_SMBDREV_linking_max_distance_to_single_unlinked_jump( int value );

    int get_DSDBR01_SMBDREV_linking_max_y_distance_to_link();
    rtype set_DSDBR01_SMBDREV_linking_max_y_distance_to_link( int value );

    double get_DSDBR01_SMBDREV_linking_sharpness_ramp_split();
    rtype set_DSDBR01_SMBDREV_linking_sharpness_ramp_split( double value );


	// Overall Map Quantitative Analysis Thresholds

    int get_DSDBR01_OMQA_slope_window_size();
    rtype set_DSDBR01_OMQA_slope_window_size( int value );

    double get_DSDBR01_OMQA_modal_distortion_min_x();
    rtype set_DSDBR01_OMQA_modal_distortion_min_x( double value );
	
    double get_DSDBR01_OMQA_modal_distortion_max_x();
    rtype set_DSDBR01_OMQA_modal_distortion_max_x( double value );
	
    double get_DSDBR01_OMQA_modal_distortion_min_y();
    rtype set_DSDBR01_OMQA_modal_distortion_min_y( double value );

	///////////////////////////////////////////////////////////////////////

    int get_DSDBR01_SMQA_slope_window_size();
    rtype set_DSDBR01_SMQA_slope_window_size( int value );

    double get_DSDBR01_SMQA_modal_distortion_min_x();
    rtype set_DSDBR01_SMQA_modal_distortion_min_x( double value );
	
    double get_DSDBR01_SMQA_modal_distortion_max_x();
    rtype set_DSDBR01_SMQA_modal_distortion_max_x( double value );
	
    double get_DSDBR01_SMQA_modal_distortion_min_y();
    rtype set_DSDBR01_SMQA_modal_distortion_min_y( double value );

    bool get_DSDBR01_SMQA_ignore_DBC_at_FSC();
    rtype set_DSDBR01_SMQA_ignore_DBC_at_FSC( bool value );

	//GDM 31/10/06 New get sets for the Mode Width and Hysteresis Analysis Window Min Max's
	double get_DSDBR01_SMQA_mode_width_analysis_min_x();
    rtype set_DSDBR01_SMQA_mode_width_analysis_min_x( double value );
	
    double get_DSDBR01_SMQA_mode_width_analysis_max_x();
    rtype set_DSDBR01_SMQA_mode_width_analysis_max_x( double value );

	double get_DSDBR01_SMQA_hysteresis_analysis_min_x();
    rtype set_DSDBR01_SMQA_hysteresis_analysis_min_x( double value );
	
    double get_DSDBR01_SMQA_hysteresis_analysis_max_x();
    rtype set_DSDBR01_SMQA_hysteresis_analysis_max_x( double value );
	
	//GDM

	///////////////////////////////////////////////////////////////////////
	///
	// Overall Map Pass Fail Thresholds
    double get_DSDBR01_OMPF_max_middle_line_rms_value();
    rtype set_DSDBR01_OMPF_max_middle_line_rms_value( double value );

    double get_DSDBR01_OMPF_min_middle_line_slope();
    rtype set_DSDBR01_OMPF_min_middle_line_slope( double value );

    double get_DSDBR01_OMPF_max_middle_line_slope();
    rtype set_DSDBR01_OMPF_max_middle_line_slope( double value );

	double	get_DSDBR01_OMPF_max_pr_gap();
    rtype	set_DSDBR01_OMPF_max_pr_gap( double value );

	double	get_DSDBR01_OMPF_max_lower_pr();
    rtype	set_DSDBR01_OMPF_max_lower_pr( double value );

	double	get_DSDBR01_OMPF_min_upper_pr();
    rtype	set_DSDBR01_OMPF_min_upper_pr( double value );

	int		get_DSDBR01_OMPF_max_mode_borders_removed_threshold();
    rtype	set_DSDBR01_OMPF_max_mode_borders_removed_threshold( int value );

	int		get_DSDBR01_OMPF_min_mode_width_threshold();
    rtype	set_DSDBR01_OMPF_min_mode_width_threshold( int value );

	int		get_DSDBR01_OMPF_max_mode_width_threshold();
    rtype	set_DSDBR01_OMPF_max_mode_width_threshold( int value );

	double	get_DSDBR01_OMPF_max_mode_bdc_area_threshold();
    rtype	set_DSDBR01_OMPF_max_mode_bdc_area_threshold( double value );

	double	get_DSDBR01_OMPF_max_mode_bdc_area_x_length_threshold();
    rtype	set_DSDBR01_OMPF_max_mode_bdc_area_x_length_threshold( double value );

	double	get_DSDBR01_OMPF_sum_mode_bdc_areas_threshold();
    rtype	set_DSDBR01_OMPF_sum_mode_bdc_areas_threshold( double value );
	
	double	get_DSDBR01_OMPF_num_mode_bdc_areas_threshold();
    rtype	set_DSDBR01_OMPF_num_mode_bdc_areas_threshold( double value );

	double	get_DSDBR01_OMPF_modal_distortion_angle_threshold();
    rtype	set_DSDBR01_OMPF_modal_distortion_angle_threshold( double value );

	double	get_DSDBR01_OMPF_modal_distortion_angle_x_pos_threshold();
    rtype	set_DSDBR01_OMPF_modal_distortion_angle_x_pos_threshold( double value );

	double	get_DSDBR01_OMPF_max_mode_line_slope_threshold();
    rtype	set_DSDBR01_OMPF_max_mode_line_slope_threshold( double value );

	double	get_DSDBR01_OMPF_min_mode_line_slope_threshold();
    rtype	set_DSDBR01_OMPF_min_mode_line_slope_threshold( double value );

	//
	/////////////////////////////////////////////////////////////////////////

	// DSDBR01 Coarse Frequency parameters

    CString get_DSDBR01_CFREQ_poly_coeffs_abspath();
    rtype set_DSDBR01_CFREQ_poly_coeffs_abspath( CString value );

	int		get_DSDBR01_CFREQ_num_freq_meas_per_op();
	rtype	set_DSDBR01_CFREQ_num_freq_meas_per_op(int value);

	int		get_DSDBR01_CFREQ_num_power_ratio_meas_per_op();
	rtype	set_DSDBR01_CFREQ_num_power_ratio_meas_per_op(int value);

	double	get_DSDBR01_CFREQ_max_stable_freq_error();
    rtype	set_DSDBR01_CFREQ_max_stable_freq_error( double value );

	int		get_DSDBR01_CFREQ_sample_point_settle_time();
	rtype	set_DSDBR01_CFREQ_sample_point_settle_time( int value );

	bool	get_DSDBR01_CFREQ_write_sample_points_to_file();
    rtype	set_DSDBR01_CFREQ_write_sample_points_to_file( bool value );



	// DSDBR01 ITU Grid

	double	get_DSDBR01_ITUGRID_centre_freq();
    rtype	set_DSDBR01_ITUGRID_centre_freq( double value );

	double	get_DSDBR01_ITUGRID_step_freq();
    rtype	set_DSDBR01_ITUGRID_step_freq( double value );

	int		get_DSDBR01_ITUGRID_channel_count();
    rtype	set_DSDBR01_ITUGRID_channel_count( int value );

    CString get_DSDBR01_ITUGRID_abspath();
    rtype set_DSDBR01_ITUGRID_abspath( CString value );

    CString get_DSDBR01_ESTIMATESITUGRID_abspath();
    rtype set_DSDBR01_ESTIMATESITUGRID_abspath( CString value );    
	// DSDBR01 ITU Grid characterisation parameters

	int		get_DSDBR01_CHAR_num_freq_meas_per_op();
    rtype	set_DSDBR01_CHAR_num_freq_meas_per_op( int value );

	double	get_DSDBR01_CHAR_freq_accuracy_of_wavemeter();
    rtype	set_DSDBR01_CHAR_freq_accuracy_of_wavemeter( double value );

	int		get_DSDBR01_CHAR_op_settle_time();
    rtype	set_DSDBR01_CHAR_op_settle_time( int value );

	int		get_DSDBR01_CHAR_itusearch_scheme();
    rtype	set_DSDBR01_CHAR_itusearch_scheme( int value );

	int		get_DSDBR01_CHAR_itusearch_nextguess();
    rtype	set_DSDBR01_CHAR_itusearch_nextguess( int value );

	int		get_DSDBR01_CHAR_itusearch_resamples();
    rtype	set_DSDBR01_CHAR_itusearch_resamples( int value );

	int		get_DSDBR01_CHAR_num_power_ratio_meas_per_op();
	rtype	set_DSDBR01_CHAR_num_power_ratio_meas_per_op(int value);

	double	get_DSDBR01_CHAR_max_stable_freq_error();
    rtype	set_DSDBR01_CHAR_max_stable_freq_error( double value );

	bool	get_DSDBR01_CHAR_write_duff_points_to_file();
    rtype	set_DSDBR01_CHAR_write_duff_points_to_file( bool value );

	bool	get_DSDBR01_CHAR_prevent_front_section_switch();
    rtype	set_DSDBR01_CHAR_prevent_front_section_switch( bool value );

    CString	get_DSDBR01_CHAR_fakefreq_Iramp_module_name();
    rtype	set_DSDBR01_CHAR_fakefreq_Iramp_module_name( CString value );

    CString	get_DSDBR01_CHAR_Iramp_vs_fakefreq_abspath();
    rtype	set_DSDBR01_CHAR_Iramp_vs_fakefreq_abspath( CString value );

	double	get_DSDBR01_CHAR_fakefreq_max_error();
    rtype	set_DSDBR01_CHAR_fakefreq_max_error( double value );




	// DSDBR01 TEC settling parameters used during characterisation

	int	get_DSDBR01_CHARTEC_time_window_size();
    rtype	set_DSDBR01_CHARTEC_time_window_size( int value );

	double	get_DSDBR01_CHARTEC_max_temp_deviation();
    rtype	set_DSDBR01_CHARTEC_max_temp_deviation( double value );

	int		get_DSDBR01_CHARTEC_num_temp_readings();
	rtype	set_DSDBR01_CHARTEC_num_temp_readings(int value);

	int		get_DSDBR01_CHARTEC_max_settle_time();
    rtype	set_DSDBR01_CHARTEC_max_settle_time( int value );


	//////////////////////////////////////////////////////////
	///
	// Super Mode Map Pass Fail Thresholds
	int		get_DSDBR01_SMPF_max_mode_width(short	smNumber);
	rtype	set_DSDBR01_SMPF_max_mode_width(short	smNumber, 
											int		value);
	int		get_DSDBR01_SMPF_min_mode_width(short	smNumber);
	rtype	set_DSDBR01_SMPF_min_mode_width(short	smNumber, 
											int		value);
	int		get_DSDBR01_SMPF_max_mode_borders_removed(short	smNumber);
	rtype	set_DSDBR01_SMPF_max_mode_borders_removed(short	smNumber, 
													int		value);
	double	get_DSDBR01_SMPF_mean_perc_working_region(short	smNumber);
	rtype	set_DSDBR01_SMPF_mean_perc_working_region(short	smNumber, 
													double	value);
	double	get_DSDBR01_SMPF_min_perc_working_region(short	smNumber);
	rtype	set_DSDBR01_SMPF_min_perc_working_region(short	smNumber, 
													double	value);
	double	get_DSDBR01_SMPF_max_mode_bdc_area(short	smNumber);
	rtype	set_DSDBR01_SMPF_max_mode_bdc_area(short	smNumber, 
										double	value);
	double	get_DSDBR01_SMPF_max_mode_bdc_area_x_length(short	smNumber);
	rtype	set_DSDBR01_SMPF_max_mode_bdc_area_x_length(short	smNumber, 
												double		value);
	double	get_DSDBR01_SMPF_sum_mode_bdc_areas(short	smNumber);
	rtype	set_DSDBR01_SMPF_sum_mode_bdc_areas(short	smNumber, 
										double	value);
	double	get_DSDBR01_SMPF_num_mode_bdc_areas(short	smNumber);
	rtype	set_DSDBR01_SMPF_num_mode_bdc_areas(short	smNumber, 
										double	value);
	double	get_DSDBR01_SMPF_modal_distortion_angle(short	smNumber);
	rtype	set_DSDBR01_SMPF_modal_distortion_angle(short	smNumber, 
													double	value);
	double	get_DSDBR01_SMPF_modal_distortion_angle_x_pos(short	smNumber);
	rtype	set_DSDBR01_SMPF_modal_distortion_angle_x_pos(short	smNumber, 
														double	value);
	double	get_DSDBR01_SMPF_max_middle_line_rms_value(short	smNumber);
	rtype	set_DSDBR01_SMPF_max_middle_line_rms_value(short	smNumber, 
												double	value);
	double	get_DSDBR01_SMPF_max_middle_line_slope(short	smNumber);
	rtype	set_DSDBR01_SMPF_max_middle_line_slope(short	smNumber, 
												double	value);
	double	get_DSDBR01_SMPF_min_middle_line_slope(short	smNumber);
	rtype	set_DSDBR01_SMPF_min_middle_line_slope(short	smNumber, 
												double	value);
	//double	get_DSDBR01_SMPF_continuity_spacing(short	smNumber);
	//rtype	set_DSDBR01_SMPF_continuity_spacing(short	smNumber, 
	//											double	value);
	double	get_DSDBR01_SMPF_max_mode_line_slope(short	smNumber);
	rtype	set_DSDBR01_SMPF_max_mode_line_slope(short	smNumber, 
												double	value);

	//
	//////////////////////////////////////////////////////////

protected:
	CCGRegValues(void);
private:
	static CCGRegValues* _p_instance;


	// DSDBR01 Base parameters
	CString _DSDBR01_BASE_results_directory;
    bool _DSDBR01_BASE_stub_hardware_calls;

	// DSDBR01 Common Data Collection parameters
	CString _DSDBR01_CMDC_gain_module_name;
	CString _DSDBR01_CMDC_soa_module_name;
	CString _DSDBR01_CMDC_rear_module_name;
	CString _DSDBR01_CMDC_phase_module_name;
	CString _DSDBR01_CMDC_front1_module_name;
	CString _DSDBR01_CMDC_front2_module_name;
	CString _DSDBR01_CMDC_front3_module_name;
	CString _DSDBR01_CMDC_front4_module_name;
	CString _DSDBR01_CMDC_front5_module_name;
	CString _DSDBR01_CMDC_front6_module_name;
	CString _DSDBR01_CMDC_front7_module_name;
	CString _DSDBR01_CMDC_front8_module_name;
	CString _DSDBR01_CMDC_306II_module_name;
	int _DSDBR01_CMDC_306II_direct_channel;
	int _DSDBR01_CMDC_306II_filtered_channel;
	CString _DSDBR01_CMDC_306EE_module_name;
	int _DSDBR01_CMDC_306EE_photodiode1_channel;
	int _DSDBR01_CMDC_306EE_photodiode2_channel;
	int _DSDBR01_CMDC_wavemeter_retries;

	// DSDBR01 Overall Map Data Collection parameters
	CString _DSDBR01_OMDC_ramp_direction;
	bool _DSDBR01_OMDC_forward_and_reverse;
	bool _DSDBR01_OMDC_measure_photodiode_currents;
	bool _DSDBR01_OMDC_write_filtered_power_maps;
	CString _DSDBR01_OMDC_rear_currents_abspath;
	CString _DSDBR01_OMDC_front_currents_abspath;
	int _DSDBR01_OMDC_source_delay;
	int _DSDBR01_OMDC_measure_delay;
	bool _DSDBR01_OMDC_overwrite_existing_files;

	// DSDBR01 Overall Map Boundary Detection parameters
	bool _DSDBR01_OMBD_read_boundaries_from_file;
	bool _DSDBR01_OMBD_write_boundaries_to_file;
    int _DSDBR01_OMBD_median_filter_rank;
	double _DSDBR01_OMBD_max_deltaPr_in_sm;
	int _DSDBR01_OMBD_min_points_width_of_sm;
	int _DSDBR01_OMBD_ml_moving_ave_filter_n;
	int _DSDBR01_OMBD_ml_min_length;
    bool _DSDBR01_OMBD_ml_extend_to_corner;


	// DSDBR01 Supermode Map Data Collection parameters
	CString _DSDBR01_SMDC_ramp_direction;
	CString _DSDBR01_SMDC_phase_currents_abspath;
	bool _DSDBR01_SMDC_measure_photodiode_currents;
	bool _DSDBR01_SMDC_write_filtered_power_maps;
	int _DSDBR01_SMDC_source_delay;
	int _DSDBR01_SMDC_measure_delay;
	bool _DSDBR01_SMDC_overwrite_existing_files;

	// DSDBR01 Supermode Map Boundary Detection parameters
	bool _DSDBR01_SMBD_read_boundaries_from_file;
	bool _DSDBR01_SMBD_write_boundaries_to_file;
	bool _DSDBR01_SMBD_write_middle_of_upper_lines_to_file;
	bool _DSDBR01_SMBD_write_middle_of_lower_lines_to_file;
	bool _DSDBR01_SMBD_write_debug_files;
	bool _DSDBR01_SMBD_extrapolate_to_map_edges;
    int _DSDBR01_SMBD_median_filter_rank;
	double _DSDBR01_SMBD_max_deltaPr_in_lm;
	int _DSDBR01_SMBD_min_points_width_of_lm;
	int _DSDBR01_SMBD_max_points_width_of_lm;
	int _DSDBR01_SMBD_ml_moving_ave_filter_n;
	int _DSDBR01_SMBD_ml_min_length;
	double _DSDBR01_SMBD_vertical_percent_shift;

	double _DSDBR01_SMBD_zprc_hysteresis_upper_threshold;
	double _DSDBR01_SMBD_zprc_hysteresis_lower_threshold;
	double _DSDBR01_SMBD_zprc_max_hysteresis_rise;
	int _DSDBR01_SMBD_zprc_min_points_separation;
	int _DSDBR01_SMBD_zprc_max_points_to_reverse_jump;
	int _DSDBR01_SMBD_zprc_max_hysteresis_points;
	CString _DSDBR01_SMBD_ramp_abspath;

	// DSDBR01 Forward Supermode Map Boundary Detection parameters
    int _DSDBR01_SMBDFWD_I_ramp_boundaries_max;
	int _DSDBR01_SMBDFWD_boundaries_min_separation;
    double _DSDBR01_SMBDFWD_min_d2Pr_dI2_zero_cross_jump_size;
    double _DSDBR01_SMBDFWD_max_sum_of_squared_distances_from_Pr_I_line;
    double _DSDBR01_SMBDFWD_min_slope_change_of_Pr_I_lines;
    int _DSDBR01_SMBDFWD_linking_max_distance_between_linked_jumps;
    double _DSDBR01_SMBDFWD_linking_min_sharpness;
    double _DSDBR01_SMBDFWD_linking_max_sharpness;
    int _DSDBR01_SMBDFWD_linking_max_gap_between_double_lines;
    int _DSDBR01_SMBDFWD_linking_min_gap_between_double_lines;
    int _DSDBR01_SMBDFWD_linking_near_bottom_row;
    double _DSDBR01_SMBDFWD_linking_sharpness_near_bottom_row;
    int _DSDBR01_SMBDFWD_linking_max_distance_near_bottom_row;
    int _DSDBR01_SMBDFWD_linking_max_distance_to_single_unlinked_jump;
    int _DSDBR01_SMBDFWD_linking_max_y_distance_to_link;
    double _DSDBR01_SMBDFWD_linking_sharpness_ramp_split;

	// DSDBR01 Reverse Supermode Map Boundary Detection parameters
    int _DSDBR01_SMBDREV_I_ramp_boundaries_max;
	int _DSDBR01_SMBDREV_boundaries_min_separation;
    double _DSDBR01_SMBDREV_min_d2Pr_dI2_zero_cross_jump_size;
    double _DSDBR01_SMBDREV_max_sum_of_squared_distances_from_Pr_I_line;
    double _DSDBR01_SMBDREV_min_slope_change_of_Pr_I_lines;
    int _DSDBR01_SMBDREV_linking_max_distance_between_linked_jumps;
    double _DSDBR01_SMBDREV_linking_min_sharpness;
    double _DSDBR01_SMBDREV_linking_max_sharpness;
    int _DSDBR01_SMBDREV_linking_max_gap_between_double_lines;
    int _DSDBR01_SMBDREV_linking_min_gap_between_double_lines;
    int _DSDBR01_SMBDREV_linking_near_bottom_row;
    double _DSDBR01_SMBDREV_linking_sharpness_near_bottom_row;
    int _DSDBR01_SMBDREV_linking_max_distance_near_bottom_row;
    int _DSDBR01_SMBDREV_linking_max_distance_to_single_unlinked_jump;
    int _DSDBR01_SMBDREV_linking_max_y_distance_to_link;
    double _DSDBR01_SMBDREV_linking_sharpness_ramp_split;

	// DSDBR01 Overall Map Quantitative Analysis parameters
    int		_DSDBR01_OMQA_slope_window_size;
    double	_DSDBR01_OMQA_modal_distortion_min_x;
    double	_DSDBR01_OMQA_modal_distortion_max_x;
    double	_DSDBR01_OMQA_modal_distortion_min_y;

	// DSDBR01 Overall Map Pass Fail Thresholds
	int		_DSDBR01_OMPF_max_mode_borders_removed_threshold;
	double	_DSDBR01_OMPF_max_middle_line_rms_value;
	double	_DSDBR01_OMPF_min_middle_line_slope;
	double	_DSDBR01_OMPF_max_middle_line_slope;
	double	_DSDBR01_OMPF_max_pr_gap;
	double	_DSDBR01_OMPF_max_lower_pr;
	double	_DSDBR01_OMPF_min_upper_pr;
	int		_DSDBR01_OMPF_min_mode_width_threshold;
	int		_DSDBR01_OMPF_max_mode_width_threshold;
	double	_DSDBR01_OMPF_max_mode_bdc_area_threshold;
	double	_DSDBR01_OMPF_max_mode_bdc_area_x_length_threshold;
	double	_DSDBR01_OMPF_sum_mode_bdc_areas_threshold;
	double	_DSDBR01_OMPF_num_mode_bdc_areas_threshold;
	double	_DSDBR01_OMPF_modal_distortion_angle_threshold;
	double	_DSDBR01_OMPF_modal_distortion_angle_x_pos_threshold;
	double	_DSDBR01_OMPF_max_mode_line_slope_threshold;
	double	_DSDBR01_OMPF_min_mode_line_slope_threshold;


	// DSDBR01 Supermode Map QA parameters

    int		_DSDBR01_SMQA_slope_window_size;
    double	_DSDBR01_SMQA_modal_distortion_min_x;
    double	_DSDBR01_SMQA_modal_distortion_max_x;
    double	_DSDBR01_SMQA_modal_distortion_min_y;
	bool	_DSDBR01_SMQA_ignore_DBC_at_FSC;

	//GDM additional Min Max window QA params for Mode Width and Hysteresis analysis
	double	_DSDBR01_SMQA_mode_width_analysis_min_x;
    double	_DSDBR01_SMQA_mode_width_analysis_max_x;
	double	_DSDBR01_SMQA_hysteresis_analysis_min_x;
	double	_DSDBR01_SMQA_hysteresis_analysis_max_x;
	//GDM
	///////////////////////////////////////////////////////
	///

	// Super mode map Pass Fail Thresholds
	int		_DSDBR01_SMPF_ALL_max_mode_width;
	int		_DSDBR01_SMPF_ALL_min_mode_width;
	int		_DSDBR01_SMPF_ALL_max_mode_borders_removed;
	double	_DSDBR01_SMPF_ALL_max_middle_line_rms_value;
	double	_DSDBR01_SMPF_ALL_min_middle_line_slope;
	double	_DSDBR01_SMPF_ALL_max_middle_line_slope;
	//double	_DSDBR01_SMPF_ALL_max_continuity_spacing;
	double	_DSDBR01_SMPF_ALL_mean_perc_working_region;
	double	_DSDBR01_SMPF_ALL_min_perc_working_region;
	double	_DSDBR01_SMPF_ALL_modal_distortion_angle;
	double	_DSDBR01_SMPF_ALL_modal_distortion_angle_x_pos;
	double	_DSDBR01_SMPF_ALL_max_mode_bdc_area;
	double	_DSDBR01_SMPF_ALL_max_mode_bdc_area_x_length;
	double	_DSDBR01_SMPF_ALL_sum_mode_bdc_areas;
	double	_DSDBR01_SMPF_ALL_num_mode_bdc_areas;
	double	_DSDBR01_SMPF_ALL_max_mode_line_slope;

	// Super mode map Pass Fail Thresholds
	int		_DSDBR01_SMPF_SM0_max_mode_width;
	int		_DSDBR01_SMPF_SM0_min_mode_width;
	int		_DSDBR01_SMPF_SM0_max_mode_borders_removed;
	double	_DSDBR01_SMPF_SM0_max_middle_line_rms_value;
	double	_DSDBR01_SMPF_SM0_min_middle_line_slope;
	double	_DSDBR01_SMPF_SM0_max_middle_line_slope;
	//double	_DSDBR01_SMPF_SM0_max_continuity_spacing;
	double	_DSDBR01_SMPF_SM0_mean_perc_working_region;
	double	_DSDBR01_SMPF_SM0_min_perc_working_region;
	double	_DSDBR01_SMPF_SM0_modal_distortion_angle;
	double	_DSDBR01_SMPF_SM0_modal_distortion_angle_x_pos;
	double	_DSDBR01_SMPF_SM0_max_mode_bdc_area;
	double	_DSDBR01_SMPF_SM0_max_mode_bdc_area_x_length;
	double	_DSDBR01_SMPF_SM0_sum_mode_bdc_areas;
	double	_DSDBR01_SMPF_SM0_num_mode_bdc_areas;
	double	_DSDBR01_SMPF_SM0_max_mode_line_slope;

	/////////////////////////////////////////////////////////

	int		_DSDBR01_SMPF_SM1_max_mode_width;
	int		_DSDBR01_SMPF_SM1_min_mode_width;
	int		_DSDBR01_SMPF_SM1_max_mode_borders_removed;
	double	_DSDBR01_SMPF_SM1_max_middle_line_rms_value;
	double	_DSDBR01_SMPF_SM1_min_middle_line_slope;
	double	_DSDBR01_SMPF_SM1_max_middle_line_slope;
	//double	_DSDBR01_SMPF_SM1_max_continuity_spacing;
	double	_DSDBR01_SMPF_SM1_mean_perc_working_region;
	double	_DSDBR01_SMPF_SM1_min_perc_working_region;
	double	_DSDBR01_SMPF_SM1_modal_distortion_angle;
	double	_DSDBR01_SMPF_SM1_modal_distortion_angle_x_pos;
	double	_DSDBR01_SMPF_SM1_max_mode_bdc_area;
	double	_DSDBR01_SMPF_SM1_max_mode_bdc_area_x_length;
	double	_DSDBR01_SMPF_SM1_sum_mode_bdc_areas;
	double	_DSDBR01_SMPF_SM1_num_mode_bdc_areas;
	double	_DSDBR01_SMPF_SM1_max_mode_line_slope;

	/////////////////////////////////////////////////////////

	int		_DSDBR01_SMPF_SM2_max_mode_width;
	int		_DSDBR01_SMPF_SM2_min_mode_width;
	int		_DSDBR01_SMPF_SM2_max_mode_borders_removed;
	double	_DSDBR01_SMPF_SM2_max_middle_line_rms_value;
	double	_DSDBR01_SMPF_SM2_min_middle_line_slope;
	double	_DSDBR01_SMPF_SM2_max_middle_line_slope;
	//double	_DSDBR01_SMPF_SM2_max_continuity_spacing;
	double	_DSDBR01_SMPF_SM2_mean_perc_working_region;
	double	_DSDBR01_SMPF_SM2_min_perc_working_region;
	double	_DSDBR01_SMPF_SM2_modal_distortion_angle;
	double	_DSDBR01_SMPF_SM2_modal_distortion_angle_x_pos;
	double	_DSDBR01_SMPF_SM2_max_mode_bdc_area;
	double	_DSDBR01_SMPF_SM2_max_mode_bdc_area_x_length;
	double	_DSDBR01_SMPF_SM2_sum_mode_bdc_areas;
	double	_DSDBR01_SMPF_SM2_num_mode_bdc_areas;
	double	_DSDBR01_SMPF_SM2_max_mode_line_slope;

	/////////////////////////////////////////////////////////

	int		_DSDBR01_SMPF_SM3_max_mode_width;
	int		_DSDBR01_SMPF_SM3_min_mode_width;
	int		_DSDBR01_SMPF_SM3_max_mode_borders_removed;
	double	_DSDBR01_SMPF_SM3_max_middle_line_rms_value;
	double	_DSDBR01_SMPF_SM3_min_middle_line_slope;
	double	_DSDBR01_SMPF_SM3_max_middle_line_slope;
	//double	_DSDBR01_SMPF_SM3_max_continuity_spacing;
	double	_DSDBR01_SMPF_SM3_mean_perc_working_region;
	double	_DSDBR01_SMPF_SM3_min_perc_working_region;
	double	_DSDBR01_SMPF_SM3_modal_distortion_angle;
	double	_DSDBR01_SMPF_SM3_modal_distortion_angle_x_pos;
	double	_DSDBR01_SMPF_SM3_max_mode_bdc_area;
	double	_DSDBR01_SMPF_SM3_max_mode_bdc_area_x_length;
	double	_DSDBR01_SMPF_SM3_sum_mode_bdc_areas;
	double	_DSDBR01_SMPF_SM3_num_mode_bdc_areas;
	double	_DSDBR01_SMPF_SM3_max_mode_line_slope;

	/////////////////////////////////////////////////////////

	int		_DSDBR01_SMPF_SM4_max_mode_width;
	int		_DSDBR01_SMPF_SM4_min_mode_width;
	int		_DSDBR01_SMPF_SM4_max_mode_borders_removed;
	double	_DSDBR01_SMPF_SM4_max_middle_line_rms_value;
	double	_DSDBR01_SMPF_SM4_min_middle_line_slope;
	double	_DSDBR01_SMPF_SM4_max_middle_line_slope;
	//double	_DSDBR01_SMPF_SM4_max_continuity_spacing;
	double	_DSDBR01_SMPF_SM4_mean_perc_working_region;
	double	_DSDBR01_SMPF_SM4_min_perc_working_region;
	double	_DSDBR01_SMPF_SM4_modal_distortion_angle;
	double	_DSDBR01_SMPF_SM4_modal_distortion_angle_x_pos;
	double	_DSDBR01_SMPF_SM4_max_mode_bdc_area;
	double	_DSDBR01_SMPF_SM4_max_mode_bdc_area_x_length;
	double	_DSDBR01_SMPF_SM4_sum_mode_bdc_areas;
	double	_DSDBR01_SMPF_SM4_num_mode_bdc_areas;
	double	_DSDBR01_SMPF_SM4_max_mode_line_slope;

	/////////////////////////////////////////////////////////

	int		_DSDBR01_SMPF_SM5_max_mode_width;
	int		_DSDBR01_SMPF_SM5_min_mode_width;
	int		_DSDBR01_SMPF_SM5_max_mode_borders_removed;
	double	_DSDBR01_SMPF_SM5_max_middle_line_rms_value;
	double	_DSDBR01_SMPF_SM5_min_middle_line_slope;
	double	_DSDBR01_SMPF_SM5_max_middle_line_slope;
	//double	_DSDBR01_SMPF_SM5_max_continuity_spacing;
	double	_DSDBR01_SMPF_SM5_mean_perc_working_region;
	double	_DSDBR01_SMPF_SM5_min_perc_working_region;
	double	_DSDBR01_SMPF_SM5_modal_distortion_angle;
	double	_DSDBR01_SMPF_SM5_modal_distortion_angle_x_pos;
	double	_DSDBR01_SMPF_SM5_max_mode_bdc_area;
	double	_DSDBR01_SMPF_SM5_max_mode_bdc_area_x_length;
	double	_DSDBR01_SMPF_SM5_sum_mode_bdc_areas;
	double	_DSDBR01_SMPF_SM5_num_mode_bdc_areas;
	double	_DSDBR01_SMPF_SM5_max_mode_line_slope;

	/////////////////////////////////////////////////////////

	int		_DSDBR01_SMPF_SM6_max_mode_width;
	int		_DSDBR01_SMPF_SM6_min_mode_width;
	int		_DSDBR01_SMPF_SM6_max_mode_borders_removed;
	double	_DSDBR01_SMPF_SM6_max_middle_line_rms_value;
	double	_DSDBR01_SMPF_SM6_min_middle_line_slope;
	double	_DSDBR01_SMPF_SM6_max_middle_line_slope;
	//double	_DSDBR01_SMPF_SM6_max_continuity_spacing;
	double	_DSDBR01_SMPF_SM6_mean_perc_working_region;
	double	_DSDBR01_SMPF_SM6_min_perc_working_region;
	double	_DSDBR01_SMPF_SM6_modal_distortion_angle;
	double	_DSDBR01_SMPF_SM6_modal_distortion_angle_x_pos;
	double	_DSDBR01_SMPF_SM6_max_mode_bdc_area;
	double	_DSDBR01_SMPF_SM6_max_mode_bdc_area_x_length;
	double	_DSDBR01_SMPF_SM6_sum_mode_bdc_areas;
	double	_DSDBR01_SMPF_SM6_num_mode_bdc_areas;
	double	_DSDBR01_SMPF_SM6_max_mode_line_slope;

	/////////////////////////////////////////////////////////

	int		_DSDBR01_SMPF_SM7_max_mode_width;
	int		_DSDBR01_SMPF_SM7_min_mode_width;
	int		_DSDBR01_SMPF_SM7_max_mode_borders_removed;
	double	_DSDBR01_SMPF_SM7_max_middle_line_rms_value;
	double	_DSDBR01_SMPF_SM7_min_middle_line_slope;
	double	_DSDBR01_SMPF_SM7_max_middle_line_slope;
	//double	_DSDBR01_SMPF_SM7_max_continuity_spacing;
	double	_DSDBR01_SMPF_SM7_mean_perc_working_region;
	double	_DSDBR01_SMPF_SM7_min_perc_working_region;
	double	_DSDBR01_SMPF_SM7_modal_distortion_angle;
	double	_DSDBR01_SMPF_SM7_modal_distortion_angle_x_pos;
	double	_DSDBR01_SMPF_SM7_max_mode_bdc_area;
	double	_DSDBR01_SMPF_SM7_max_mode_bdc_area_x_length;
	double	_DSDBR01_SMPF_SM7_sum_mode_bdc_areas;
	double	_DSDBR01_SMPF_SM7_num_mode_bdc_areas;
	double	_DSDBR01_SMPF_SM7_max_mode_line_slope;

	/////////////////////////////////////////////////////////

	int		_DSDBR01_SMPF_SM8_max_mode_width;
	int		_DSDBR01_SMPF_SM8_min_mode_width;
	int		_DSDBR01_SMPF_SM8_max_mode_borders_removed;
	double	_DSDBR01_SMPF_SM8_max_middle_line_rms_value;
	double	_DSDBR01_SMPF_SM8_min_middle_line_slope;
	double	_DSDBR01_SMPF_SM8_max_middle_line_slope;
	//double	_DSDBR01_SMPF_SM8_max_continuity_spacing;
	double	_DSDBR01_SMPF_SM8_mean_perc_working_region;
	double	_DSDBR01_SMPF_SM8_min_perc_working_region;
	double	_DSDBR01_SMPF_SM8_modal_distortion_angle;
	double	_DSDBR01_SMPF_SM8_modal_distortion_angle_x_pos;
	double	_DSDBR01_SMPF_SM8_max_mode_bdc_area;
	double	_DSDBR01_SMPF_SM8_max_mode_bdc_area_x_length;
	double	_DSDBR01_SMPF_SM8_sum_mode_bdc_areas;
	double	_DSDBR01_SMPF_SM8_num_mode_bdc_areas;
	double	_DSDBR01_SMPF_SM8_max_mode_line_slope;

	/////////////////////////////////////////////////////////

	int		_DSDBR01_SMPF_SM9_max_mode_width;
	int		_DSDBR01_SMPF_SM9_min_mode_width;
	int		_DSDBR01_SMPF_SM9_max_mode_borders_removed;
	double	_DSDBR01_SMPF_SM9_max_middle_line_rms_value;
	double	_DSDBR01_SMPF_SM9_min_middle_line_slope;
	double	_DSDBR01_SMPF_SM9_max_middle_line_slope;
	//double	_DSDBR01_SMPF_SM9_max_continuity_spacing;
	double	_DSDBR01_SMPF_SM9_mean_perc_working_region;
	double	_DSDBR01_SMPF_SM9_min_perc_working_region;
	double	_DSDBR01_SMPF_SM9_modal_distortion_angle;
	double	_DSDBR01_SMPF_SM9_modal_distortion_angle_x_pos;
	double	_DSDBR01_SMPF_SM9_max_mode_bdc_area;
	double	_DSDBR01_SMPF_SM9_max_mode_bdc_area_x_length;
	double	_DSDBR01_SMPF_SM9_sum_mode_bdc_areas;
	double	_DSDBR01_SMPF_SM9_num_mode_bdc_areas;
	double	_DSDBR01_SMPF_SM9_max_mode_line_slope;

	/////////////////////////////////////////////////////////

	//
	//////////////////////////////////////////////////////

	// DSDBR01 Coarse Frequency parameters
	CString _DSDBR01_CFREQ_poly_coeffs_abspath;
	int		_DSDBR01_CFREQ_num_freq_meas_per_op;
	int		_DSDBR01_CFREQ_num_power_ratio_meas_per_op;
	double	_DSDBR01_CFREQ_max_stable_freq_error;
	int		_DSDBR01_CFREQ_sample_point_settle_time;
	bool	_DSDBR01_CFREQ_write_sample_points_to_file;

	// DSDBR01 ITU Grid
	double	_DSDBR01_ITUGRID_centre_freq;
	double	_DSDBR01_ITUGRID_step_freq;
	int		_DSDBR01_ITUGRID_channel_count;
    CString _DSDBR01_ITUGRID_abspath;
	CString _DSDBR01_ESTIMATESITUGRID_abspath;

	// DSDBR01 ITU Grid characterisation parameters
	int		_DSDBR01_CHAR_num_freq_meas_per_op;
	double	_DSDBR01_CHAR_freq_accuracy_of_wavemeter;
	int		_DSDBR01_CHAR_op_settle_time;
	int		_DSDBR01_CHAR_itusearch_scheme;
	int		_DSDBR01_CHAR_itusearch_nextguess;
	int		_DSDBR01_CHAR_itusearch_resamples;
	int		_DSDBR01_CHAR_num_power_ratio_meas_per_op;
	double	_DSDBR01_CHAR_max_stable_freq_error;
	bool	_DSDBR01_CHAR_write_duff_points_to_file;
	bool	_DSDBR01_CHAR_prevent_front_section_switch;
	CString _DSDBR01_CHAR_fakefreq_Iramp_module_name;
	CString _DSDBR01_CHAR_Iramp_vs_fakefreq_abspath;
	double	_DSDBR01_CHAR_fakefreq_max_error;

	// DSDBR01 TEC settling parameters used during characterisation
	int		_DSDBR01_CHARTEC_time_window_size;
	double	_DSDBR01_CHARTEC_max_temp_deviation;
	int		_DSDBR01_CHARTEC_num_temp_readings;
	int		_DSDBR01_CHARTEC_max_settle_time;
};




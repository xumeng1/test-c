// Copyright (c) 2004 PXIT. All Rights Reserved
//
// FileName    : RegIf.cpp
// Description : Definition of RegInterface class
//               Handles saving and retrieving Registry data
//               Singleton class
//
// Rev#  | Date          | Author               | Description of change
// ---------------------------------------------------------------------
// 0.1   | 26 July 2004  | Frank D'Arcy         | Copied from CGResearch
//

#include "StdAfx.h"
#include "RegIf.h"


#define new DEBUG_NEW

const int BUF_SIZE = 1024;

/////////////////////////////////////////////////////////////////////
// Only instance of this class
RegInterface* 
RegInterface::theInstance = NULL;

/////////////////////////////////////////////////////////////////////

// Accessor to the one and only
// instance of this class
RegInterface*
RegInterface::instance()
{
	// If we're calling this for
	// the first time, create our
	// instance, otherwise disallow
	// any more being created
	if(theInstance == NULL)
		static RegInterface theInstance;
	return theInstance;
}

RegInterface::
RegInterface()
{
	theInstance = this;

	initMembers();
}

RegInterface::
~RegInterface(void)
{
	// close the key
	Close();
}

void
RegInterface::
initMembers()
{
}

RegInterface::return_type
RegInterface::
openForSubKeyRead(LPCTSTR keyName, HKEY& hKey)
{

	//////////////////////////////////////////////////////////////////////

	return_type res = ok;

	long regError = -1;
	regError = RegOpenKeyEx(HKEY_LOCAL_MACHINE,
						keyName,
						0,
						KEY_READ,
						&hKey);
	if(regError!=ERROR_SUCCESS)
	{
		// can't open registry because of error 161/whatever
		// 161 is a bad key
		// Will use defaults instead
		memset(errorString, '\0', sizeof(errorString));
		sprintf(errorString,
			"RegInterface::openForSubKeyRead(keyName=%s)"
			,keyName);

		registryError(regError, errorString);

		res = return_type::registry_key_error;
	}

	//////////////////////////////////////////////////////////////////////


	return res;
}

RegInterface::return_type
RegInterface::
openForValueQuery(LPCTSTR keyName, HKEY& hKey)
{
	//////////////////////////////////////////////////////////////////////

	return_type res = return_type::ok;
	long regError = -1;
	regError = RegOpenKeyEx(HKEY_LOCAL_MACHINE,
						keyName,
						0,
						KEY_QUERY_VALUE,
						&hKey);

	if(regError!=ERROR_SUCCESS)
	{
		// can't open registry because of error 161/whatever
		// 161 is a bad key
		// Will use defaults instead
		memset(errorString, '\0', sizeof(errorString));
		sprintf(errorString,
			"RegInterface::openForValueQuery(keyName=%s, HKEY& hKey)"
			,keyName
		);

		registryError(regError, errorString);

		res = return_type::registry_key_error;
	}

	//////////////////////////////////////////////////////////////////////

	return res;
}

RegInterface::return_type
RegInterface::
openForSetValue(LPCTSTR keyName, HKEY& hKey)
{
	//////////////////////////////////////////////////////////////////////

	return_type res = return_type::ok;
	long regError = -1;
	regError = RegOpenKeyEx(HKEY_LOCAL_MACHINE,
						keyName,
						0,
						KEY_SET_VALUE,
						&hKey);

	if(regError!=ERROR_SUCCESS)
	{
		// can't open registry because of error 161/whatever
		// 161 is a bad key
		// Will use defaults instead
		memset(errorString, '\0', sizeof(errorString));
		sprintf(errorString,
			"RegInterface::openForSetValue(keyName=%s, HKEY& hKey)"
			,keyName
		);

		registryError(regError, errorString);

		res = return_type::registry_key_error;
	}

	//////////////////////////////////////////////////////////////////////

	return res;
}

RegInterface::return_type
RegInterface::
getSubKeyList(LPCTSTR keyName, 
			  CStringArray& subKeyList)
{
	//////////////////////////////////////////////////////////////////////

	return_type			res = return_type::ok;

	HKEY			parentKey;
	long			regError = -1;

	// clear the list
	subKeyList.RemoveAll();

	///////////////////////////////////
	///	Open the reg key for
	//	reading subkey names
	regError = openForSubKeyRead(keyName, parentKey);
	if(regError)
	{
		registryError(regError, errorString);

		// error
		return return_type::registry_key_error;
	}
	//
	///////////////////////////////////////

	DWORD subKeyCnt ;
	DWORD maxSubKey ;
	DWORD valueCnt ;
	DWORD maxValueName ;
	DWORD maxValueData ;
	regError =	RegQueryInfoKey(
					parentKey,
					0,				// buffer for class name
					0,				// length of class name string
					0,				// reserved
					&subKeyCnt,		// # of subkeys
					&maxSubKey,		// length of longest subkey
					0,				// length of longest class name string
					&valueCnt,		// # of values
					&maxValueName,	// length of longest value name
					&maxValueData,	// length of longest value data
					0,				// security descriptor
					0				// last write time
				) ;
	if(regError)
	{
		registryError(regError, errorString);
		return return_type::registry_key_error;
	}

	LPTSTR subKeyName = new TCHAR [ maxSubKey + 1 ] ;
	DWORD keyNameLen = maxSubKey ;

	regError = ERROR_SUCCESS;
	for (DWORD i = 0; regError == ERROR_SUCCESS; i++)
	{
		regError = RegEnumKey(parentKey,
							i,		// index
							subKeyName,	// address of buffer for key name string
							keyNameLen+1	// max. length of key name string
							) ;

		if (regError == (DWORD)ERROR_SUCCESS) 
        {
			subKeyList.Add((LPCTSTR)subKeyName);
		}
	}

	//ml
	delete subKeyName;

	//
	/// Close the key
	//
	regError = RegCloseKey(parentKey);
	if(regError)
	{
		memset(errorString, '\0', sizeof(errorString));
		sprintf(errorString,
			"RegInterface::getSubKeyList(keyName=%s, HKEY& hKey) : Couldn't Close Registry Key!\n"
			,keyName
			);
		registryError(regError, errorString);

		return return_type::registry_key_error;
	}

	//////////////////////////////////////////////////////////////////////

	return return_type::ok;
}


///////////////////////////////////////////////////////////////////////////////

RegInterface::return_type	
RegInterface::
deleteKey(LPCTSTR	keyName)
{
	return_type res = return_type::ok;

	long regError = -1;
	regError = SHDeleteKey(HKEY_LOCAL_MACHINE,
						keyName);
	if(regError!=ERROR_SUCCESS)
	{
		// can't open registry because of error 161/whatever
		// 161 is a bad key
		// Will use defaults instead
		memset(errorString, '\0', sizeof(errorString));
		sprintf(errorString,
			"RegInterface::SHDeleteKey(keyName=%s)"
			,keyName);

		registryError(regError, errorString);

		res = return_type::registry_key_error;
	}

	return res;
}

///////////////////////////////////////////////////////////////////////////////

RegInterface::return_type	
RegInterface::
createKey(LPCTSTR	keyName)
{
	return_type res = return_type::ok;


	HKEY hKey = 0;
	long regError = -1;
	regError = RegCreateKeyEx(HKEY_LOCAL_MACHINE,  // handle to open key
							keyName,			   // subkey name
							0,				       // reserved
							NULL,			       // class string
							0,					   // special options
							KEY_READ|KEY_WRITE,    // desired security access
							NULL,				   // inheritance
							&hKey,				   // key handle 
							NULL				   // disposition value buffer
							);
	if(regError!=ERROR_SUCCESS)
	{
		// can't open registry because of error 161/whatever
		// 161 is a bad key
		// Will use defaults instead
		memset(errorString, '\0', sizeof(errorString));
		sprintf(errorString,
			"RegInterface::RegCreateKeyEx(keyName=%s)"
			,keyName);

		registryError(regError, errorString);

		res = return_type::registry_key_error;
	}

	return res;
}

///////////////////////////////////////////////////////////////////////////////

bool	
RegInterface::
keyExists(LPCTSTR	keyName)
{
	bool exists = true;

	// attempt to open the key
	// this will fail if it does not exist
	long regError = -1;
	HKEY hKey;
	regError = RegOpenKeyEx(HKEY_LOCAL_MACHINE,
						keyName,
						0,
						KEY_QUERY_VALUE,
						&hKey);

	if(regError!=ERROR_SUCCESS)
	{
		exists = false;
	}
	else
	{
		exists = true;
	}

	return exists;
}

///////////////////////////////////////////////////////////////////////////////


//
///
//
RegInterface::return_type
RegInterface::
getBoolValue(LPCTSTR keyName, CString valueName, bool& boolValue)
{

	//////////////////////////////////////////////////////////////////////

	return_type		returnCode = return_type::ok;
	HKEY		hKey;

	boolValue = false;
	long	regError = -1;
	TCHAR	value[BUF_SIZE];
	DWORD	valueLen = BUF_SIZE;

	memset(value, '\0', sizeof(value));

	strcpy(value, valueName.GetBuffer());


	regError = openForValueQuery(keyName, hKey);
	if(!regError)
	{


		regError = RegQueryValueEx(hKey,
								TEXT(value),
								NULL,
								NULL,
								(LPBYTE)value,
								&valueLen);
		
		if(!regError)
		{
			// convert from string 
			// to boolean
			CString result = "\0";
			result = value;

			if((result == "1")||(result == "true"))
			{
				boolValue = true;
			}
			else if((result == "0")||(result == "false"))
			{
				boolValue = false;
			}
			else if(result == REG_BOOL_YES_VALUE)
			{
				boolValue = true;
			}
			else if(result == REG_BOOL_NO_VALUE)
			{
				boolValue = false;
			}
			else
			{
				boolValue = true;
				returnCode = return_type::registry_data_error;
			}
		}
		else
		{
			memset(errorString, '\0', sizeof(errorString));
			sprintf(errorString,
				"RegInterface::getBoolValue(keyName=%s, hKey=%s)"
				,keyName
				,valueName
				);
			registryError(regError, errorString);

			boolValue = true;

			returnCode = return_type::registry_key_error;
		} 

		//
		/// Close the key
		//
		regError = RegCloseKey(hKey);
		if(regError)
		{
			memset(errorString, '\0', sizeof(errorString));
			sprintf(errorString,
				"RegInterface::getBoolValue(keyName=%s, hKey=%s) : Couldn't Close Registry Key!"
				,keyName
				,valueName
				);
			registryError(regError, errorString);
			
			boolValue = false;
			returnCode = return_type::registry_key_error;
		}
	}
	else
	{
		memset(errorString, '\0', sizeof(errorString));
		sprintf(errorString,
			"RegInterface::getBoolValue(keyName=%s, valueName=%s) : Couldn't open registry Key."
			,keyName
			,valueName
			);
		registryError(regError, errorString);

		returnCode = return_type::registry_key_error;
	}

	//////////////////////////////////////////////////////////////////////

	return returnCode;
}

//
/// looking for a CString value of a registry key
//
RegInterface::return_type
RegInterface::
getCStringValue(LPCTSTR keyName, CString valueName, CString& stringValue)
{
	//////////////////////////////////////////////////////////////////////

	HKEY	hKey; // handle to the registry key
	stringValue = "RegError";
	long	regError = -1;
	TCHAR	value[BUF_SIZE];
	DWORD	valueLen = BUF_SIZE;

	memset(value, '\0', sizeof(value));

	strcpy(value, valueName.GetBuffer());

	regError = openForValueQuery(keyName, hKey);
	if(regError)
	{
		memset(errorString, '\0', sizeof(errorString));
		sprintf(errorString,
			"RegInterface::getStringValue(keyName=%s, hKey=%s) : Couldn't open key"
			,keyName
			,valueName
			);
		registryError(regError, errorString);

		return return_type::registry_key_error;
	}

	regError = RegQueryValueEx(hKey,
							TEXT(value),
							NULL,
							NULL,
							(LPBYTE)value,
							&valueLen);
	
	if(!regError)
	{
		// convert from TCHAR
		// to CString
		stringValue = value;

	}
	else
	{
		memset(errorString, '\0', sizeof(errorString));
		sprintf(errorString,
			"RegInterface::getStringValue(keyName=%s, hKey=%s) : Couldn't query value."
			,keyName
			,valueName
			);
		registryError(regError, errorString);

		stringValue = "RegError";
		return return_type::registry_key_error;
	} 

	//
	/// Close the key
	//
	regError = RegCloseKey(hKey);
	if(regError)
	{
		memset(errorString, '\0', sizeof(errorString));
		sprintf(errorString,
			"RegInterface::getStringValue(keyName=%s, hKey=%s) : Couldn't Close Registry Key!"
			,keyName
			,valueName
			);
		registryError(regError, errorString);

		stringValue = "RegError";
		return return_type::registry_key_error;
	}

	//////////////////////////////////////////////////////////////////////

	return return_type::ok;
}

RegInterface::return_type
RegInterface::
getIntValue(LPCTSTR keyName, CString valueName, int& intValue)
{
	//////////////////////////////////////////////////////////////////////

	HKEY	hKey;
	intValue = 0;
	long	regError = 0;
	TCHAR	value[BUF_SIZE];
	DWORD	valueLen = BUF_SIZE;

	memset(value, '\0', sizeof(value));

	strcpy(value, valueName.GetBuffer());

	regError = openForValueQuery(keyName, hKey);
	if(regError)
	{
		memset(errorString, '\0', sizeof(errorString));
		sprintf(errorString,
			"RegInterface::getIntValue(keyName=%s, hKey=%s) : Couldn't open key"
			,keyName
			,valueName
			);
		registryError(regError, errorString);

		return return_type::registry_key_error;
	}

	regError = RegQueryValueEx(hKey,
							TEXT(value),
							NULL,
							NULL,
							(LPBYTE)value,
							&valueLen);
	
	if(!regError)
	{
		// convert from string 
		// to int
		CString result = "\0";
		result = value;

		intValue = atoi(result);
	}
	else
	{
		memset(errorString, '\0', sizeof(errorString));
		sprintf(errorString,
			"RegInterface::getIntValue(keyName=%s, hKey=%s)"
			,keyName
			,valueName
			);
		registryError(regError, errorString);

		return return_type::registry_key_error;
	} 

	//
	/// Close the key
	//
	regError = RegCloseKey(hKey);
	if(regError)
	{
		memset(errorString, '\0', sizeof(errorString));
		sprintf(errorString,
			"RegInterface::getIntValue(keyName=%s, hKey=%s) : Couldn't Close Registry Key!"
			,keyName
			,valueName
			);
		registryError(regError, errorString);

		return return_type::registry_key_error;
	}

	//////////////////////////////////////////////////////////////////////

	return return_type::ok;
}

RegInterface::return_type
RegInterface::
getFloatValue(LPCTSTR keyName, CString valueName, float& floatValue)
{
	//////////////////////////////////////////////////////////////////////

	HKEY	hKey;
	floatValue = 0;
	long	regError = 0;
	TCHAR	value[BUF_SIZE];
	DWORD	valueLen = BUF_SIZE;

	memset(value, '\0', sizeof(value));

	strcpy(value, valueName.GetBuffer());

	regError = openForValueQuery(keyName, hKey);
	if(regError)
	{
		memset(errorString, '\0', sizeof(errorString));
		sprintf(errorString,
			"RegInterface::getFloatValue(keyName=%s, hKey=%s) : Couldn't open key"
			,keyName
			,valueName
			);
		registryError(regError, errorString);

		return return_type::registry_key_error;
	}

	regError = RegQueryValueEx(hKey,
							TEXT(value),
							NULL,
							NULL,
							(LPBYTE)value,
							&valueLen);
	
	if(!regError)
	{
		// convert from string 
		// to int
		CString result = "\0";
		result = value;

		if(result!="")
			floatValue = (float)(atof(result));
		else
			floatValue = 0;
	}
	else
	{
		memset(errorString, '\0', sizeof(errorString));
		sprintf(errorString,
			"RegInterface::getFloatValue(keyName=%s, hKey=%s)"
			,keyName
			,valueName
			);
		registryError(regError, errorString);

		return return_type::registry_key_error;
	} 

	//
	/// Close the key
	//
	regError = RegCloseKey(hKey);
	if(regError)
	{
		memset(errorString, '\0', sizeof(errorString));
		sprintf(errorString,
			"RegInterface::getFloatValue(keyName=%s, hKey=%s) : Couldn't Close Registry Key!"
			,keyName
			,valueName
			);
		registryError(regError, errorString);

		return return_type::registry_key_error;
	}

	//////////////////////////////////////////////////////////////////////

	return return_type::ok;
}

RegInterface::return_type
RegInterface::
getDoubleValue(LPCTSTR keyName, CString valueName, double& doubleValue)
{
	//////////////////////////////////////////////////////////////////////

	HKEY	hKey;
	doubleValue = 0;
	long	regError = 0;
	TCHAR	value[BUF_SIZE];
	DWORD	valueLen = BUF_SIZE;

	memset(value, '\0', sizeof(value));

	strcpy(value, valueName.GetBuffer());

	regError = openForValueQuery(keyName, hKey);
	if(regError)
	{
		memset(errorString, '\0', sizeof(errorString));
		sprintf(errorString,
			"RegInterface::getDoubleValue(keyName=%s, hKey=%s) : Couldn't open key"
			,keyName
			,valueName
			);
		registryError(regError, errorString);

		return return_type::registry_key_error;
	}

	regError = RegQueryValueEx(hKey,
							TEXT(value),
							NULL,
							NULL,
							(LPBYTE)value,
							&valueLen);
	
	if(!regError)
	{
		// convert from string 
		// to int
		CString result = "\0";
		result = value;

		if(result!="")
			doubleValue = atof(result);
		else
			doubleValue = 0;
	}
	else
	{
		memset(errorString, '\0', sizeof(errorString));
		sprintf(errorString,
			"RegInterface::getDoubleValue(keyName=%s, hKey=%s)"
			,keyName
			,valueName
			);
		registryError(regError, errorString);

		return return_type::registry_key_error;
	} 

	//
	/// Close the key
	//
	regError = RegCloseKey(hKey);
	if(regError)
	{
		memset(errorString, '\0', sizeof(errorString));
		sprintf(errorString,
			"RegInterface::getDoubleValue(keyName=%s, hKey=%s) : Couldn't Close Registry Key!"
			,keyName
			,valueName
			);
		registryError(regError, errorString);

		return return_type::registry_key_error;
	}

	//////////////////////////////////////////////////////////////////////

	return return_type::ok;
}

RegInterface::return_type
RegInterface::
getLongValue(LPCTSTR keyName, CString valueName, long& longValue)
{
	//////////////////////////////////////////////////////////////////////

	HKEY	hKey;
	longValue = 0;
	long	regError = 0;
	TCHAR	value[BUF_SIZE];
	DWORD	valueLen = BUF_SIZE;

	memset(value, '\0', sizeof(value));

	strcpy(value, valueName.GetBuffer());

	///////////////////////////////////////////////
	///
	//	Open the key
	regError = openForValueQuery(keyName, hKey);
	if(regError)
	{
		memset(errorString, '\0', sizeof(errorString));
		sprintf(errorString,
			"RegInterface::getLongValue(keyName=%s, hKey=%s) : Couldn't open key"
			,keyName
			,valueName
			);
		registryError(regError, errorString);

		return return_type::registry_key_error;
	}

	//
	/// Get the value
	//
	regError = RegQueryValueEx(hKey,
							TEXT(value),
							NULL,
							NULL,
							(LPBYTE)value,
							&valueLen);
	
	//
	/// Convert to an int
	//
	if(!regError)
	{
		// convert from string 
		// to int
		CString result = "\0";
		result = value;

		longValue = atol(result);
	}
	else
	{
		memset(errorString, '\0', sizeof(errorString));
		sprintf(errorString,
			"RegInterface::getLongValue(keyName=%s, hKey=%s)"
			,keyName
			,valueName
			);
		registryError(regError, errorString);

		return return_type::registry_key_error;
	} 
	
	//
	/// Close the key
	//
	regError = RegCloseKey(hKey);
	if(regError)
	{
		memset(errorString, '\0', sizeof(errorString));
		sprintf(errorString,
			"RegInterface::getLongValue(keyName=%s, hKey=%s) : Couldn't Close Registry Key!"
			,keyName
			,valueName
			);
		registryError(regError, errorString);

		return return_type::registry_key_error;
	}

	//////////////////////////////////////////////////////////////////////

	// return the value
	return return_type::ok;
}

///////////////////////////////////////////////////////////////////////////////

//
///
//
RegInterface::return_type
RegInterface::
setBoolValue(LPCTSTR keyName, CString valueName, bool boolValue, bool yesNoUnits)
{
	return_type		returnCode = return_type::ok;
	HKEY		hKey;

	long	regError = -1;
	UINT	size;

	// convert from bool to string
	CString newValueStr = "";
	if(boolValue)
	{
		if(yesNoUnits) newValueStr = REG_BOOL_YES_VALUE;
		else newValueStr = "1";
	}
	else
	{
		if(yesNoUnits) newValueStr = REG_BOOL_NO_VALUE;
		else newValueStr = "0";
	}

	// get size of new data
	size = newValueStr.GetLength();

	regError = openForSetValue(keyName, hKey);
	if(!regError)
	{
		regError = RegSetValueEx(hKey,
								TEXT(valueName.GetBuffer()),
								0,
								REG_SZ,
								(LPBYTE)LPCSTR(newValueStr),
								size);
		if(regError)
		{
			memset(errorString, '\0', sizeof(errorString));
			sprintf(errorString,
				"RegInterface::setBoolValue(keyName=%s, hKey=%s)"
				,keyName
				,valueName
				);
			registryError(regError, errorString);

			returnCode = return_type::registry_key_error;
		} 

		//
		/// Close the key
		//
		regError = RegCloseKey(hKey);
		if(regError)
		{
			memset(errorString, '\0', sizeof(errorString));
			sprintf(errorString,
				"RegInterface::getBoolValue(keyName=%s, hKey=%s) : Couldn't Close Registry Key!"
				,keyName
				,valueName
				);
			registryError(regError, errorString);
			
			boolValue = false;
			returnCode = return_type::registry_key_error;
		}
	}
	else
	{
		memset(errorString, '\0', sizeof(errorString));
		sprintf(errorString,
			"RegInterface::getBoolValue(keyName=%s, valueName=%s) : Couldn't open registry Key."
			,keyName
			,valueName
			);
		registryError(regError, errorString);

		returnCode = return_type::registry_key_error;
	}

	//////////////////////////////////////////////////////////////////////

	return returnCode;
}

//
/// looking for a CString value of a registry key
//
RegInterface::return_type
RegInterface::
setCStringValue(LPCTSTR keyName, CString valueName, CString stringValue)
{
	//////////////////////////////////////////////////////////////////////

	return_type		returnCode = return_type::ok;
	HKEY		hKey;

	long	regError = -1;
	UINT	size;

	// convert from bool to string
	CString newValueStr = stringValue;

	// get size of new data
	size = newValueStr.GetLength();

	regError = openForSetValue(keyName, hKey);
	if(!regError)
	{
		regError = RegSetValueEx(hKey,
								TEXT(valueName.GetBuffer()),
								0,
								REG_SZ,
								(LPBYTE)LPCSTR(newValueStr),
								size);
		if(regError)
		{
			memset(errorString, '\0', sizeof(errorString));
			sprintf(errorString,
				"RegInterface::setBoolValue(keyName=%s, hKey=%s)"
				,keyName
				,valueName
				);
			registryError(regError, errorString);

			returnCode = return_type::registry_key_error;
		} 

		//
		/// Close the key
		//
		regError = RegCloseKey(hKey);
		if(regError)
		{
			memset(errorString, '\0', sizeof(errorString));
			sprintf(errorString,
				"RegInterface::getBoolValue(keyName=%s, hKey=%s) : Couldn't Close Registry Key!"
				,keyName
				,valueName
				);
			registryError(regError, errorString);
			
			returnCode = return_type::registry_key_error;
		}
	}
	else
	{
		memset(errorString, '\0', sizeof(errorString));
		sprintf(errorString,
			"RegInterface::getBoolValue(keyName=%s, valueName=%s) : Couldn't open registry Key."
			,keyName
			,valueName
			);
		registryError(regError, errorString);

		returnCode = return_type::registry_key_error;
	}

	//////////////////////////////////////////////////////////////////////

	return returnCode;
}

RegInterface::return_type
RegInterface::
setIntValue(LPCTSTR keyName, CString valueName, int intValue)
{
	return_type		returnCode = return_type::ok;
	HKEY		hKey;

	long	regError = -1;
	UINT	size;

	// convert from bool to string
	CString newValueStr = "";
	newValueStr.Format("%d", intValue);

	// get size of new data
	size = newValueStr.GetLength();

	regError = openForSetValue(keyName, hKey);
	if(!regError)
	{
		regError = RegSetValueEx(hKey,
								TEXT(valueName.GetBuffer()),
								0,
								REG_SZ,
								(LPBYTE)LPCSTR(newValueStr),
								size);
		if(regError)
		{
			memset(errorString, '\0', sizeof(errorString));
			sprintf(errorString,
				"RegInterface::setBoolValue(keyName=%s, hKey=%s)"
				,keyName
				,valueName
				);
			registryError(regError, errorString);

			returnCode = return_type::registry_key_error;
		} 

		//
		/// Close the key
		//
		regError = RegCloseKey(hKey);
		if(regError)
		{
			memset(errorString, '\0', sizeof(errorString));
			sprintf(errorString,
				"RegInterface::getBoolValue(keyName=%s, hKey=%s) : Couldn't Close Registry Key!"
				,keyName
				,valueName
				);
			registryError(regError, errorString);
			
			returnCode = return_type::registry_key_error;
		}
	}
	else
	{
		memset(errorString, '\0', sizeof(errorString));
		sprintf(errorString,
			"RegInterface::getBoolValue(keyName=%s, valueName=%s) : Couldn't open registry Key."
			,keyName
			,valueName
			);
		registryError(regError, errorString);

		returnCode = return_type::registry_key_error;
	}

	//////////////////////////////////////////////////////////////////////

	return returnCode;
}

RegInterface::return_type
RegInterface::
setFloatValue(LPCTSTR keyName, CString valueName, float floatValue)
{
	return_type		returnCode = return_type::ok;
	HKEY		hKey;

	long	regError = -1;
	UINT	size;

	// convert from bool to string
	CString newValueStr = "";
	newValueStr.Format("%.2f", floatValue);

	// get size of new data
	size = newValueStr.GetLength();

	regError = openForSetValue(keyName, hKey);
	if(!regError)
	{
		regError = RegSetValueEx(hKey,
								TEXT(valueName.GetBuffer()),
								0,
								REG_SZ,
								(LPBYTE)LPCSTR(newValueStr),
								size);
		if(regError)
		{
			memset(errorString, '\0', sizeof(errorString));
			sprintf(errorString,
				"RegInterface::setBoolValue(keyName=%s, hKey=%s)"
				,keyName
				,valueName
				);
			registryError(regError, errorString);

			returnCode = return_type::registry_key_error;
		} 

		//
		/// Close the key
		//
		regError = RegCloseKey(hKey);
		if(regError)
		{
			memset(errorString, '\0', sizeof(errorString));
			sprintf(errorString,
				"RegInterface::getBoolValue(keyName=%s, hKey=%s) : Couldn't Close Registry Key!"
				,keyName
				,valueName
				);
			registryError(regError, errorString);
			
			returnCode = return_type::registry_key_error;
		}
	}
	else
	{
		memset(errorString, '\0', sizeof(errorString));
		sprintf(errorString,
			"RegInterface::getBoolValue(keyName=%s, valueName=%s) : Couldn't open registry Key."
			,keyName
			,valueName
			);
		registryError(regError, errorString);

		returnCode = return_type::registry_key_error;
	}

	//////////////////////////////////////////////////////////////////////

	return returnCode;
}

RegInterface::return_type
RegInterface::
setDoubleValue(LPCTSTR keyName, CString valueName, double doubleValue)
{
	return_type		returnCode = return_type::ok;
	HKEY		hKey;

	long	regError = -1;
	UINT	size;

	// convert from bool to string
	CString newValueStr = "";
	newValueStr.Format("%g", doubleValue);

	// get size of new data
	size = newValueStr.GetLength();

	regError = openForSetValue(keyName, hKey);
	if(!regError)
	{
		regError = RegSetValueEx(hKey,
								TEXT(valueName.GetBuffer()),
								0,
								REG_SZ,
								(LPBYTE)LPCSTR(newValueStr),
								size);
		if(regError)
		{
			memset(errorString, '\0', sizeof(errorString));
			sprintf(errorString,
				"RegInterface::setBoolValue(keyName=%s, hKey=%s)"
				,keyName
				,valueName
				);
			registryError(regError, errorString);

			returnCode = return_type::registry_key_error;
		} 

		//
		/// Close the key
		//
		regError = RegCloseKey(hKey);
		if(regError)
		{
			memset(errorString, '\0', sizeof(errorString));
			sprintf(errorString,
				"RegInterface::getBoolValue(keyName=%s, hKey=%s) : Couldn't Close Registry Key!"
				,keyName
				,valueName
				);
			registryError(regError, errorString);
			
			returnCode = return_type::registry_key_error;
		}
	}
	else
	{
		memset(errorString, '\0', sizeof(errorString));
		sprintf(errorString,
			"RegInterface::getBoolValue(keyName=%s, valueName=%s) : Couldn't open registry Key."
			,keyName
			,valueName
			);
		registryError(regError, errorString);

		returnCode = return_type::registry_key_error;
	}

	//////////////////////////////////////////////////////////////////////

	return returnCode;
}

RegInterface::return_type
RegInterface::
setLongValue(LPCTSTR keyName, CString valueName, long longValue)
{
	return_type		returnCode = return_type::ok;
	HKEY		hKey;

	long	regError = -1;
	UINT	size;

	// convert from bool to string
	CString newValueStr = "";
	newValueStr.Format("%ld", longValue);

	// get size of new data
	size = newValueStr.GetLength();

	regError = openForSetValue(keyName, hKey);
	if(!regError)
	{
		regError = RegSetValueEx(hKey,
								TEXT(valueName.GetBuffer()),
								0,
								REG_SZ,
								(LPBYTE)LPCSTR(newValueStr),
								size);
		if(regError)
		{
			memset(errorString, '\0', sizeof(errorString));
			sprintf(errorString,
				"RegInterface::setBoolValue(keyName=%s, hKey=%s)"
				,keyName
				,valueName
				);
			registryError(regError, errorString);

			returnCode = return_type::registry_key_error;
		} 

		//
		/// Close the key
		//
		regError = RegCloseKey(hKey);
		if(regError)
		{
			memset(errorString, '\0', sizeof(errorString));
			sprintf(errorString,
				"RegInterface::getBoolValue(keyName=%s, hKey=%s) : Couldn't Close Registry Key!"
				,keyName
				,valueName
				);
			registryError(regError, errorString);
			
			returnCode = return_type::registry_key_error;
		}
	}
	else
	{
		memset(errorString, '\0', sizeof(errorString));
		sprintf(errorString,
			"RegInterface::getBoolValue(keyName=%s, valueName=%s) : Couldn't open registry Key."
			,keyName
			,valueName
			);
		registryError(regError, errorString);

		returnCode = return_type::registry_key_error;
	}

	//////////////////////////////////////////////////////////////////////

	return returnCode;
}

void
RegInterface::
registryError(long regError, char* errStr)
{
	switch(regError)
	{
		case ERROR_FILE_NOT_FOUND:
			{
				strcat(errStr, "  : Key does not exist");
			}
			break;

		case ERROR_INVALID_HANDLE:
			{
				strcat(errStr, "  : Handle is invalid");
			}
			break;

		case ERROR_BAD_PATHNAME:
			{
				strcat(errStr, "  : Bad Pathname");
			}
			break;

		case ERROR_ACCESS_DENIED:
			{
				strcat(errStr, "  : Error Access Denied");
			}
			break;

		default:
			{
				strcat(errStr, "  : Unknown return error");
			}
			break;
	}
}
// Copyright (c) 2004 PXIT. All Rights Reserved
//
// FileName    : CGRegValues.cpp
// Description : Definition of CCGRegValues class
//               Represents all Registry stored data
//               Singleton class
//
// Rev#  | Date          | Author               | Description of change
// ---------------------------------------------------------------------
// 0.1   | 04 Mar  2006  | Frank D'Arcy         | Initial Draft
//

#include "StdAfx.h"
#include "CGRegValues.h"
#include "RegIf.h"
#include "CGRegDefs.h"


CCGRegValues::rtype
CCGRegValues::saveAllToRegistry()
{
	rtype rval = rtype::ok;
	CString reg_key;
	RegInterface::return_type reg_rval = RegInterface::return_type::ok;


	// delete existing registry info
	if( P_REG->keyExists( CG_DSDBR01_BASE_REGKEY ) )
	{
		reg_rval = P_REG->deleteKey( CG_DSDBR01_BASE_REGKEY );
	}


	if( reg_rval == RegInterface::return_type::ok )
	{

		// Base parameters

		reg_key = CString( CG_DSDBR01_BASE_REGKEY );

		// recreate empty registry key
		reg_rval = P_REG->createKey( reg_key );

		if( reg_rval == RegInterface::return_type::ok )
		{

			P_REG->setCStringValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_BASE_RESULTS_DIR ),
				_DSDBR01_BASE_results_directory );

			P_REG->setBoolValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_BASE_STUB_HARDWARE_CALLS ),
				_DSDBR01_BASE_stub_hardware_calls );
		}


		// DSDBR01 Common Data Collection parameters

		reg_key = CString( CG_DSDBR01_BASE_REGKEY ) + CString( RELATIVE_REGKEY_DSDBR01_CMDC );

		// recreate empty registry key
		reg_rval = P_REG->createKey( reg_key.GetBuffer() );

		if( reg_rval == RegInterface::return_type::ok )
		{
			P_REG->setCStringValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_CMDC_GAIN_MODULE_NAME ),
				_DSDBR01_CMDC_gain_module_name );

			P_REG->setCStringValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_CMDC_SOA_MODULE_NAME ),
				_DSDBR01_CMDC_soa_module_name );

			P_REG->setCStringValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_CMDC_REAR_MODULE_NAME ),
				_DSDBR01_CMDC_rear_module_name );

			P_REG->setCStringValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_CMDC_PHASE_MODULE_NAME ),
				_DSDBR01_CMDC_phase_module_name );

			P_REG->setCStringValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_CMDC_FRONT1_MODULE_NAME ),
				_DSDBR01_CMDC_front1_module_name );

			P_REG->setCStringValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_CMDC_FRONT2_MODULE_NAME ),
				_DSDBR01_CMDC_front2_module_name );

			P_REG->setCStringValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_CMDC_FRONT3_MODULE_NAME ),
				_DSDBR01_CMDC_front3_module_name );

			P_REG->setCStringValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_CMDC_FRONT4_MODULE_NAME ),
				_DSDBR01_CMDC_front4_module_name );

			P_REG->setCStringValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_CMDC_FRONT5_MODULE_NAME ),
				_DSDBR01_CMDC_front5_module_name );

			P_REG->setCStringValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_CMDC_FRONT6_MODULE_NAME ),
				_DSDBR01_CMDC_front6_module_name );

			P_REG->setCStringValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_CMDC_FRONT7_MODULE_NAME ),
				_DSDBR01_CMDC_front7_module_name );

			P_REG->setCStringValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_CMDC_FRONT8_MODULE_NAME ),
				_DSDBR01_CMDC_front8_module_name );

			P_REG->setCStringValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_CMDC_306II_MODULE_NAME ),
				_DSDBR01_CMDC_306II_module_name );

			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_CMDC_306II_DIRECT_CHANNEL ),
				_DSDBR01_CMDC_306II_direct_channel );

			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_CMDC_306II_FILTERED_CHANNEL ),
				_DSDBR01_CMDC_306II_filtered_channel );

			P_REG->setCStringValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_CMDC_306EE_MODULE_NAME ),
				_DSDBR01_CMDC_306EE_module_name );

			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_CMDC_306EE_PHOTODIODE1_CHANNEL ),
				_DSDBR01_CMDC_306EE_photodiode1_channel );

			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_CMDC_306EE_PHOTODIODE2_CHANNEL ),
				_DSDBR01_CMDC_306EE_photodiode2_channel );

			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_CMDC_WAVEMETER_RETRIES ),
				_DSDBR01_CMDC_wavemeter_retries );


		}

		// Overall Map Data Collection parameters

		reg_key = CString( CG_DSDBR01_BASE_REGKEY ) + CString( RELATIVE_REGKEY_DSDBR01_OMDC );

		// recreate empty registry key
		reg_rval = P_REG->createKey( reg_key.GetBuffer() );

		if( reg_rval == RegInterface::return_type::ok )
		{

			P_REG->setCStringValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_OMDC_RAMP_DIRECTION ),
				_DSDBR01_OMDC_ramp_direction );

			P_REG->setBoolValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_OMDC_FORWARD_AND_REVERSE ),
				_DSDBR01_OMDC_forward_and_reverse );

			P_REG->setBoolValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_OMDC_MEASURE_PHOTODIODE_CURRENTS ),
				_DSDBR01_OMDC_measure_photodiode_currents );

			P_REG->setBoolValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_OMDC_WRITE_FILTERED_POWER_MAPS ),
				_DSDBR01_OMDC_write_filtered_power_maps );

			P_REG->setCStringValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_OMDC_REAR_CURRENTS_ABSPATH ),
				_DSDBR01_OMDC_rear_currents_abspath );

			P_REG->setCStringValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_OMDC_FRONT_CURRENTS_ABSPATH ),
				_DSDBR01_OMDC_front_currents_abspath );

			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_OMDC_SOURCE_DELAY ),
				_DSDBR01_OMDC_source_delay );

			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_OMDC_MEASURE_DELAY ),
				_DSDBR01_OMDC_measure_delay );

			P_REG->setBoolValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_OMDC_OVERWRITE_EXISTING_FILES ),
				_DSDBR01_OMDC_overwrite_existing_files );

		}

		// Overall Map Boundary Detection parameters

		reg_key = CString( CG_DSDBR01_BASE_REGKEY ) + CString( RELATIVE_REGKEY_DSDBR01_OMBD );

		// recreate empty registry key
		reg_rval = P_REG->createKey( reg_key.GetBuffer() );

		if( reg_rval == RegInterface::return_type::ok )
		{
			P_REG->setBoolValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_OMBD_READ_BOUNDARIES_FROM_FILE ),
				_DSDBR01_OMBD_read_boundaries_from_file );

			P_REG->setBoolValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_OMBD_WRITE_BOUNDARIES_TO_FILE ),
				_DSDBR01_OMBD_write_boundaries_to_file );

			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_OMBD_MEDIAN_FILTER_RANK ),
				_DSDBR01_OMBD_median_filter_rank );

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_OMBD_MAX_DELTAPR_IN_SM ),
				_DSDBR01_OMBD_max_deltaPr_in_sm );

			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_OMBD_MIN_POINTS_WIDTH_OF_SM ),
				_DSDBR01_OMBD_min_points_width_of_sm );

			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_OMBD_ML_MOVING_AVERAGE_FILTER_N ),
				_DSDBR01_OMBD_ml_moving_ave_filter_n );

			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_OMBD_ML_MIN_LENGTH ),
				_DSDBR01_OMBD_ml_min_length );

			P_REG->setBoolValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_OMBD_ML_EXTEND_TO_CORNER ),
				_DSDBR01_OMBD_ml_extend_to_corner );

		}


		// Supermode Map Data Collection parameters

		reg_key = CString( CG_DSDBR01_BASE_REGKEY ) + CString( RELATIVE_REGKEY_DSDBR01_SMDC );

		// recreate empty registry key
		reg_rval = P_REG->createKey( reg_key.GetBuffer() );

		if( reg_rval == RegInterface::return_type::ok )
		{

			P_REG->setCStringValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMDC_RAMP_DIRECTION ),
				_DSDBR01_SMDC_ramp_direction );

			P_REG->setCStringValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMDC_PHASE_CURRENTS_ABSPATH ),
				_DSDBR01_SMDC_phase_currents_abspath );

			P_REG->setBoolValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMDC_MEASURE_PHOTODIODE_CURRENTS ),
				_DSDBR01_SMDC_measure_photodiode_currents );

			P_REG->setBoolValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMDC_WRITE_FILTERED_POWER_MAPS ),
				_DSDBR01_SMDC_write_filtered_power_maps );

			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMDC_SOURCE_DELAY ),
				_DSDBR01_SMDC_source_delay );

			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMDC_MEASURE_DELAY ),
				_DSDBR01_SMDC_measure_delay );

			P_REG->setBoolValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMDC_OVERWRITE_EXISTING_FILES ),
				_DSDBR01_SMDC_overwrite_existing_files );

		}


		// Supermode Map Boundary Detection parameters

		reg_key = CString( CG_DSDBR01_BASE_REGKEY ) + CString( RELATIVE_REGKEY_DSDBR01_SMBD );

		// recreate empty registry key
		reg_rval = P_REG->createKey( reg_key.GetBuffer() );

		if( reg_rval == RegInterface::return_type::ok )
		{
			P_REG->setBoolValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBD_READ_BOUNDARIES_FROM_FILE ),
				_DSDBR01_SMBD_read_boundaries_from_file );

			P_REG->setBoolValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBD_WRITE_BOUNDARIES_TO_FILE ),
				_DSDBR01_SMBD_write_boundaries_to_file );

			P_REG->setBoolValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBD_write_middle_of_upper_lines_to_file ),
				_DSDBR01_SMBD_write_middle_of_upper_lines_to_file );

			P_REG->setBoolValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBD_write_middle_of_lower_lines_to_file ),
				_DSDBR01_SMBD_write_middle_of_lower_lines_to_file );

			P_REG->setBoolValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBD_WRITE_DEBUG_FILES ),
				_DSDBR01_SMBD_write_debug_files );

			P_REG->setBoolValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBD_extrapolate_to_map_edges ),
				_DSDBR01_SMBD_extrapolate_to_map_edges );

			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBD_MEDIAN_FILTER_RANK ),
				_DSDBR01_SMBD_median_filter_rank );

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBD_MAX_DELTAPR_IN_LM ),
				_DSDBR01_SMBD_max_deltaPr_in_lm );

			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBD_MIN_POINTS_WIDTH_OF_LM ),
				_DSDBR01_SMBD_min_points_width_of_lm );

			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBD_MAX_POINTS_WIDTH_OF_LM ),
				_DSDBR01_SMBD_max_points_width_of_lm );

			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBD_ML_MOVING_AVERAGE_FILTER_N ),
				_DSDBR01_SMBD_ml_moving_ave_filter_n );

			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBD_ML_MIN_LENGTH ),
				_DSDBR01_SMBD_ml_min_length );

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBD_VERTICAL_PERCENT_SHIFT ),
				_DSDBR01_SMBD_vertical_percent_shift );

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBD_ZPRC_HYSTERESIS_UPPER_THRESHOLD ),
				_DSDBR01_SMBD_zprc_hysteresis_upper_threshold );

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBD_ZPRC_HYSTERESIS_LOWER_THRESHOLD ),
				_DSDBR01_SMBD_zprc_hysteresis_lower_threshold );

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBD_ZPRC_MAX_HYSTERESIS_RISE ),
				_DSDBR01_SMBD_zprc_max_hysteresis_rise );

			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBD_ZPRC_MIN_POINTS_SEPARATION ),
				_DSDBR01_SMBD_zprc_min_points_separation );

			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBD_ZPRC_MAX_POINTS_TO_REVERSE_JUMP ),
				_DSDBR01_SMBD_zprc_max_points_to_reverse_jump );

			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBD_ZPRC_MAX_HYSTERESIS_POINTS ),
				_DSDBR01_SMBD_zprc_max_hysteresis_points );

			P_REG->setCStringValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBD_RAMP_ABSPATH ),
				_DSDBR01_SMBD_ramp_abspath );
		}


		// Forward Supermode Map Boundary Detection parameters

		reg_key = CString( CG_DSDBR01_BASE_REGKEY ) + CString( RELATIVE_REGKEY_DSDBR01_SMBDFWD );

		// recreate empty registry key
		reg_rval = P_REG->createKey( reg_key.GetBuffer() );

		if( reg_rval == RegInterface::return_type::ok )
		{
			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBDFWD_I_RAMP_BOUNDARIES ),
				_DSDBR01_SMBDFWD_I_ramp_boundaries_max );

			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBDFWD_BOUNDARIES_MIN_SEPARATION ),
				_DSDBR01_SMBDFWD_boundaries_min_separation );

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBDFWD_MIN_D2PR_DI2_ZERO_CROSS_JUMP_SIZE ),
				_DSDBR01_SMBDFWD_min_d2Pr_dI2_zero_cross_jump_size );

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBDFWD_MAX_SUM_SQUARED_DIST_FROM_PR_I_LINE ),
				_DSDBR01_SMBDFWD_max_sum_of_squared_distances_from_Pr_I_line );

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBDFWD_MIN_SLOPE_CHANGE_OF_PR_I_LINE ),
				_DSDBR01_SMBDFWD_min_slope_change_of_Pr_I_lines );

			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBDFWD_LINKING_MAX_DISTANCE_BETWEEN_LINKED_JUMPS ),
				_DSDBR01_SMBDFWD_linking_max_distance_between_linked_jumps );

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBDFWD_LINKING_MIN_SHARPNESS ),
				_DSDBR01_SMBDFWD_linking_min_sharpness );

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBDFWD_LINKING_MAX_SHARPNESS ),
				_DSDBR01_SMBDFWD_linking_max_sharpness );

			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBDFWD_LINKING_MAX_GAP_BETWEEN_DOUBLE_LINES ),
				_DSDBR01_SMBDFWD_linking_max_gap_between_double_lines );

			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBDFWD_LINKING_MIN_GAP_BETWEEN_DOUBLE_LINES ),
				_DSDBR01_SMBDFWD_linking_min_gap_between_double_lines );

			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBDFWD_LINKING_NEAR_BOTTOM ),
				_DSDBR01_SMBDFWD_linking_near_bottom_row );

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBDFWD_LINKING_SHARPNESS_NEAR_BOTTOM ),
				_DSDBR01_SMBDFWD_linking_sharpness_near_bottom_row );

			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBDFWD_LINKING_MAX_DISTANCE_BETWEEN_LINKS_NEAR_BOTTOM ),
				_DSDBR01_SMBDFWD_linking_max_distance_near_bottom_row );

			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBDFWD_LINKING_MAX_DISTANCE_TO_LINK_SINGLE_UNLINKED_JUMP ),
				_DSDBR01_SMBDFWD_linking_max_distance_to_single_unlinked_jump );

			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBDFWD_LINKING_MAX_Y_DISTANCE_TO_LINK ),
				_DSDBR01_SMBDFWD_linking_max_y_distance_to_link );

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBDFWD_LINKING_SHARPNESS_RAMP_SPLIT ),
				_DSDBR01_SMBDFWD_linking_sharpness_ramp_split );
		}


		// Reverse Supermode Map Boundary Detection parameters

		reg_key = CString( CG_DSDBR01_BASE_REGKEY ) + CString( RELATIVE_REGKEY_DSDBR01_SMBDREV );

		// recreate empty registry key
		reg_rval = P_REG->createKey( reg_key.GetBuffer() );

		if( reg_rval == RegInterface::return_type::ok )
		{
			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBDREV_I_RAMP_BOUNDARIES ),
				_DSDBR01_SMBDREV_I_ramp_boundaries_max );

			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBDREV_BOUNDARIES_MIN_SEPARATION ),
				_DSDBR01_SMBDREV_boundaries_min_separation );

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBDREV_MIN_D2PR_DI2_ZERO_CROSS_JUMP_SIZE ),
				_DSDBR01_SMBDREV_min_d2Pr_dI2_zero_cross_jump_size );

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBDREV_MAX_SUM_SQUARED_DIST_FROM_PR_I_LINE ),
				_DSDBR01_SMBDREV_max_sum_of_squared_distances_from_Pr_I_line );

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBDREV_MIN_SLOPE_CHANGE_OF_PR_I_LINE ),
				_DSDBR01_SMBDREV_min_slope_change_of_Pr_I_lines );


			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBDREV_LINKING_MAX_DISTANCE_BETWEEN_LINKED_JUMPS ),
				_DSDBR01_SMBDREV_linking_max_distance_between_linked_jumps );

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBDREV_LINKING_MIN_SHARPNESS ),
				_DSDBR01_SMBDREV_linking_min_sharpness );

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBDREV_LINKING_MAX_SHARPNESS ),
				_DSDBR01_SMBDREV_linking_max_sharpness );

			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBDREV_LINKING_MAX_GAP_BETWEEN_DOUBLE_LINES ),
				_DSDBR01_SMBDREV_linking_max_gap_between_double_lines );

			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBDREV_LINKING_MIN_GAP_BETWEEN_DOUBLE_LINES ),
				_DSDBR01_SMBDREV_linking_min_gap_between_double_lines );

			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBDREV_LINKING_NEAR_BOTTOM ),
				_DSDBR01_SMBDREV_linking_near_bottom_row );

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBDREV_LINKING_SHARPNESS_NEAR_BOTTOM ),
				_DSDBR01_SMBDREV_linking_sharpness_near_bottom_row );

			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBDREV_LINKING_MAX_DISTANCE_BETWEEN_LINKS_NEAR_BOTTOM ),
				_DSDBR01_SMBDREV_linking_max_distance_near_bottom_row );

			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBDREV_LINKING_MAX_DISTANCE_TO_LINK_SINGLE_UNLINKED_JUMP ),
				_DSDBR01_SMBDREV_linking_max_distance_to_single_unlinked_jump );

			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBDREV_LINKING_MAX_Y_DISTANCE_TO_LINK ),
				_DSDBR01_SMBDREV_linking_max_y_distance_to_link );

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMBDREV_LINKING_SHARPNESS_RAMP_SPLIT ),
				_DSDBR01_SMBDREV_linking_sharpness_ramp_split );
		}


		//////////////////////////////////////////////////////////////////////////////////////////
		///
		// SuperMode Map Quantitative Analysis parameters

		reg_key = CString( CG_DSDBR01_BASE_REGKEY ) + CString( RELATIVE_REGKEY_DSDBR01_SMQA );

		// recreate empty registry key
		reg_rval = P_REG->createKey( reg_key.GetBuffer() );

		if( reg_rval == RegInterface::return_type::ok )
		{
			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMQA_SLOPE_WINDOW_SIZE ),
				_DSDBR01_SMQA_slope_window_size );

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMQA_MODAL_DISTORTION_MIN_X ),
				_DSDBR01_SMQA_modal_distortion_min_x );

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMQA_MODAL_DISTORTION_MAX_X ),
				_DSDBR01_SMQA_modal_distortion_max_x );

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMQA_MODAL_DISTORTION_MIN_Y ),
				_DSDBR01_SMQA_modal_distortion_min_y );

			//GDM 31/10/06 additional params for Min Max window of Mode Width and Hysteresis analysis
			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMQA_MODE_WIDTH_ANALYSIS_MIN_X ),
				_DSDBR01_SMQA_mode_width_analysis_min_x );

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMQA_MODE_WIDTH_ANALYSIS_MAX_X ),
				_DSDBR01_SMQA_mode_width_analysis_max_x );

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMQA_HYSTERESIS_ANALYSIS_MIN_X ),
				_DSDBR01_SMQA_hysteresis_analysis_min_x );

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMQA_HYSTERESIS_ANALYSIS_MAX_X ),
				_DSDBR01_SMQA_hysteresis_analysis_max_x );

			//GDM

			P_REG->setBoolValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_SMQA_IGNORE_BDC_AT_FSC ),
				_DSDBR01_SMQA_ignore_DBC_at_FSC );


		}
		//////////////////////////////////////////////////////////////////////////////////////////

		//////////////////////////////////////////////////////////////////////////////////////////
		///
		// Overall Map Quantitative Analysis parameters

		reg_key = CString( CG_DSDBR01_BASE_REGKEY ) + CString( RELATIVE_REGKEY_DSDBR01_OMQA );

		// recreate empty registry key
		reg_rval = P_REG->createKey( reg_key.GetBuffer() );

		if( reg_rval == RegInterface::return_type::ok )
		{
			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_OMQA_SLOPE_WINDOW_SIZE ),
				_DSDBR01_OMQA_slope_window_size );

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_OMQA_MODAL_DISTORTION_MIN_X ),
				_DSDBR01_OMQA_modal_distortion_min_x );

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_OMQA_MODAL_DISTORTION_MAX_X ),
				_DSDBR01_OMQA_modal_distortion_max_x );

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_OMQA_MODAL_DISTORTION_MIN_Y ),
				_DSDBR01_OMQA_modal_distortion_min_y );
		}

		//
		///////////////////////////////////////////////////////////////////////////////////////////
		///
		//	Overall Map Pass Fail thresholds

		reg_key = CString( CG_DSDBR01_BASE_REGKEY ) + CString( RELATIVE_REGKEY_DSDBR01_OMPF );

		// recreate empty registry key
		reg_rval = P_REG->createKey( reg_key.GetBuffer() );

		if( reg_rval == RegInterface::return_type::ok )
		{
			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_OMPF_MAX_MIDDLE_LINE_RMS_VALUE ),
				_DSDBR01_OMPF_max_middle_line_rms_value);

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_OMPF_MIN_MIDDLE_LINE_SLOPE ),
				_DSDBR01_OMPF_min_middle_line_slope);

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_OMPF_MAX_MIDDLE_LINE_SLOPE ),
				_DSDBR01_OMPF_max_middle_line_slope);

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_OMPF_MAX_PR_GAP ),
				_DSDBR01_OMPF_max_pr_gap);

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_OMPF_MAX_LOWER_PR ),
				_DSDBR01_OMPF_max_lower_pr);

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_OMPF_MIN_UPPER_PR ),
				_DSDBR01_OMPF_min_upper_pr);

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_OMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD ),
				_DSDBR01_OMPF_max_mode_borders_removed_threshold);

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_OMPF_MIN_MODE_WIDTH_THRESHOLD ),
				_DSDBR01_OMPF_min_mode_width_threshold);

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_OMPF_MAX_MODE_WIDTH_THRESHOLD ),
				_DSDBR01_OMPF_max_mode_width_threshold);

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_OMPF_MAX_MODE_BDC_AREA_THRESHOLD ),
				_DSDBR01_OMPF_max_mode_bdc_area_threshold);

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_OMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD ),
				_DSDBR01_OMPF_max_mode_bdc_area_x_length_threshold);

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_OMPF_SUM_MODE_BDC_AREAS_THRESHOLD ),
				_DSDBR01_OMPF_sum_mode_bdc_areas_threshold);

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_OMPF_NUM_MODE_BDC_AREAS_THRESHOLD ),
				_DSDBR01_OMPF_num_mode_bdc_areas_threshold);

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_OMPF_MODAL_DISTORTION_ANGLE_THRESHOLD ),
				_DSDBR01_OMPF_modal_distortion_angle_threshold);

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_OMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD ),
				_DSDBR01_OMPF_modal_distortion_angle_x_pos_threshold);

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_OMPF_MAX_MODE_LINE_SLOPE_THRESHOLD ),
				_DSDBR01_OMPF_max_mode_line_slope_threshold);

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_OMPF_MIN_MODE_LINE_SLOPE_THRESHOLD ),
				_DSDBR01_OMPF_min_mode_line_slope_threshold);

		}

		//
		///////////////////////////////////////////////////////////////////////////////////////////

		///////////////////////////////////////////////////////////////////////////////////////////
		///
		//
		// DSDBR01 Super Mode Map Pass Fail Parameters

		// SM0
		reg_key = CString( CG_DSDBR01_BASE_REGKEY ) + CString( RELATIVE_REGKEY_DSDBR01_SMPF_SM0 );

		// recreate empty registry key
		reg_rval = P_REG->createKey( reg_key.GetBuffer() );

		if( reg_rval == RegInterface::return_type::ok )
		{

			P_REG->setIntValue(reg_key.GetBuffer(),
							CString(REGNAME_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD),
							_DSDBR01_SMPF_SM0_max_mode_width );

			P_REG->setIntValue(reg_key.GetBuffer(),
							CString(REGNAME_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD),
							_DSDBR01_SMPF_SM0_min_mode_width );

			P_REG->setIntValue(reg_key.GetBuffer(),
							CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD),
							_DSDBR01_SMPF_SM0_max_mode_borders_removed );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD),
								_DSDBR01_SMPF_SM0_max_middle_line_rms_value );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD),
								_DSDBR01_SMPF_SM0_min_middle_line_slope );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD),
								_DSDBR01_SMPF_SM0_max_middle_line_slope );

			//P_REG->setDoubleValue(reg_key.GetBuffer(),
			//					CString(REGNAME_DSDBR01_SMPF_MAX_CONTINUITY_SPACING_THRESHOLD),
			//					_DSDBR01_SMPF_SM0_max_continuity_spacing );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD),
								_DSDBR01_SMPF_SM0_mean_perc_working_region );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD),
								_DSDBR01_SMPF_SM0_min_perc_working_region );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD),
								_DSDBR01_SMPF_SM0_modal_distortion_angle );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD),
								_DSDBR01_SMPF_SM0_modal_distortion_angle_x_pos );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD),
								_DSDBR01_SMPF_SM0_max_mode_bdc_area );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD),
								_DSDBR01_SMPF_SM0_max_mode_bdc_area_x_length );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD),
								_DSDBR01_SMPF_SM0_sum_mode_bdc_areas );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD),
								_DSDBR01_SMPF_SM0_num_mode_bdc_areas );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD),
								_DSDBR01_SMPF_SM0_max_mode_line_slope );
		}
		//
		///////////////////////////////////////////////////////////////////////////////////////////
		//
		// SM1
		reg_key = CString( CG_DSDBR01_BASE_REGKEY ) + CString( RELATIVE_REGKEY_DSDBR01_SMPF_SM1 );

		// recreate empty registry key
		reg_rval = P_REG->createKey( reg_key.GetBuffer() );

		if( reg_rval == RegInterface::return_type::ok )
		{

			P_REG->setIntValue(reg_key.GetBuffer(),
							CString(REGNAME_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD),
							_DSDBR01_SMPF_SM1_max_mode_width );

			P_REG->setIntValue(reg_key.GetBuffer(),
							CString(REGNAME_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD),
							_DSDBR01_SMPF_SM1_min_mode_width );

			P_REG->setIntValue(reg_key.GetBuffer(),
							CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD),
							_DSDBR01_SMPF_SM1_max_mode_borders_removed );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD),
								_DSDBR01_SMPF_SM1_max_middle_line_rms_value );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD),
								_DSDBR01_SMPF_SM1_min_middle_line_slope );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD),
								_DSDBR01_SMPF_SM1_max_middle_line_slope );

			//P_REG->setDoubleValue(reg_key.GetBuffer(),
			//					CString(REGNAME_DSDBR01_SMPF_MAX_CONTINUITY_SPACING_THRESHOLD),
			//					_DSDBR01_SMPF_SM1_max_continuity_spacing );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD),
								_DSDBR01_SMPF_SM1_mean_perc_working_region );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD),
								_DSDBR01_SMPF_SM1_min_perc_working_region );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD),
								_DSDBR01_SMPF_SM1_modal_distortion_angle );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD),
								_DSDBR01_SMPF_SM1_modal_distortion_angle_x_pos );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD),
								_DSDBR01_SMPF_SM1_max_mode_bdc_area );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD),
								_DSDBR01_SMPF_SM1_max_mode_bdc_area_x_length );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD),
								_DSDBR01_SMPF_SM1_sum_mode_bdc_areas );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD),
								_DSDBR01_SMPF_SM1_num_mode_bdc_areas );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD),
								_DSDBR01_SMPF_SM1_max_mode_line_slope );
		}
		//
		///////////////////////////////////////////////////////////////////////////////////////////
		//
		// SM2
		reg_key = CString( CG_DSDBR01_BASE_REGKEY ) + CString( RELATIVE_REGKEY_DSDBR01_SMPF_SM2 );

		// recreate empty registry key
		reg_rval = P_REG->createKey( reg_key.GetBuffer() );

		if( reg_rval == RegInterface::return_type::ok )
		{

			P_REG->setIntValue(reg_key.GetBuffer(),
							CString(REGNAME_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD),
							_DSDBR01_SMPF_SM2_max_mode_width );

			P_REG->setIntValue(reg_key.GetBuffer(),
							CString(REGNAME_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD),
							_DSDBR01_SMPF_SM2_min_mode_width );

			P_REG->setIntValue(reg_key.GetBuffer(),
							CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD),
							_DSDBR01_SMPF_SM2_max_mode_borders_removed );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD),
								_DSDBR01_SMPF_SM2_max_middle_line_rms_value );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD),
								_DSDBR01_SMPF_SM2_min_middle_line_slope );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD),
								_DSDBR01_SMPF_SM2_max_middle_line_slope );

			//P_REG->setDoubleValue(reg_key.GetBuffer(),
			//					CString(REGNAME_DSDBR01_SMPF_MAX_CONTINUITY_SPACING_THRESHOLD),
			//					_DSDBR01_SMPF_SM2_max_continuity_spacing );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD),
								_DSDBR01_SMPF_SM2_mean_perc_working_region );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD),
								_DSDBR01_SMPF_SM2_min_perc_working_region );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD),
								_DSDBR01_SMPF_SM2_modal_distortion_angle );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD),
								_DSDBR01_SMPF_SM2_modal_distortion_angle_x_pos );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD),
								_DSDBR01_SMPF_SM2_max_mode_bdc_area );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD),
								_DSDBR01_SMPF_SM2_max_mode_bdc_area_x_length );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD),
								_DSDBR01_SMPF_SM2_sum_mode_bdc_areas );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD),
								_DSDBR01_SMPF_SM2_num_mode_bdc_areas );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD),
								_DSDBR01_SMPF_SM2_max_mode_line_slope );
		}
		//
		///////////////////////////////////////////////////////////////////////////////////////////
		//
		// SM3
		reg_key = CString( CG_DSDBR01_BASE_REGKEY ) + CString( RELATIVE_REGKEY_DSDBR01_SMPF_SM3 );

		// recreate empty registry key
		reg_rval = P_REG->createKey( reg_key.GetBuffer() );

		if( reg_rval == RegInterface::return_type::ok )
		{

			P_REG->setIntValue(reg_key.GetBuffer(),
							CString(REGNAME_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD),
							_DSDBR01_SMPF_SM3_max_mode_width );

			P_REG->setIntValue(reg_key.GetBuffer(),
							CString(REGNAME_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD),
							_DSDBR01_SMPF_SM3_min_mode_width );

			P_REG->setIntValue(reg_key.GetBuffer(),
							CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD),
							_DSDBR01_SMPF_SM3_max_mode_borders_removed );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD),
								_DSDBR01_SMPF_SM3_max_middle_line_rms_value );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD),
								_DSDBR01_SMPF_SM3_min_middle_line_slope );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD),
								_DSDBR01_SMPF_SM3_max_middle_line_slope );

			//P_REG->setDoubleValue(reg_key.GetBuffer(),
			//					CString(REGNAME_DSDBR01_SMPF_MAX_CONTINUITY_SPACING_THRESHOLD),
			//					_DSDBR01_SMPF_SM3_max_continuity_spacing );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD),
								_DSDBR01_SMPF_SM3_mean_perc_working_region );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD),
								_DSDBR01_SMPF_SM3_min_perc_working_region );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD),
								_DSDBR01_SMPF_SM3_modal_distortion_angle );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD),
								_DSDBR01_SMPF_SM3_modal_distortion_angle_x_pos );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD),
								_DSDBR01_SMPF_SM3_max_mode_bdc_area );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD),
								_DSDBR01_SMPF_SM3_max_mode_bdc_area_x_length );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD),
								_DSDBR01_SMPF_SM3_sum_mode_bdc_areas );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD),
								_DSDBR01_SMPF_SM3_num_mode_bdc_areas );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD),
								_DSDBR01_SMPF_SM3_max_mode_line_slope);
		}
		//
		///////////////////////////////////////////////////////////////////////////////////////////
		//
		// SM4
		reg_key = CString( CG_DSDBR01_BASE_REGKEY ) + CString( RELATIVE_REGKEY_DSDBR01_SMPF_SM4 );

		// recreate empty registry key
		reg_rval = P_REG->createKey( reg_key.GetBuffer() );

		if( reg_rval == RegInterface::return_type::ok )
		{

			P_REG->setIntValue(reg_key.GetBuffer(),
							CString(REGNAME_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD),
							_DSDBR01_SMPF_SM4_max_mode_width );

			P_REG->setIntValue(reg_key.GetBuffer(),
							CString(REGNAME_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD),
							_DSDBR01_SMPF_SM4_min_mode_width );

			P_REG->setIntValue(reg_key.GetBuffer(),
							CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD),
							_DSDBR01_SMPF_SM4_max_mode_borders_removed );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD),
								_DSDBR01_SMPF_SM4_max_middle_line_rms_value );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD),
								_DSDBR01_SMPF_SM4_min_middle_line_slope );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD),
								_DSDBR01_SMPF_SM4_max_middle_line_slope );

			//P_REG->setDoubleValue(reg_key.GetBuffer(),
			//					CString(REGNAME_DSDBR01_SMPF_MAX_CONTINUITY_SPACING_THRESHOLD),
			//					_DSDBR01_SMPF_SM4_max_continuity_spacing );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD),
								_DSDBR01_SMPF_SM4_mean_perc_working_region );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD),
								_DSDBR01_SMPF_SM4_min_perc_working_region );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD),
								_DSDBR01_SMPF_SM4_modal_distortion_angle );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD),
								_DSDBR01_SMPF_SM4_modal_distortion_angle_x_pos );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD),
								_DSDBR01_SMPF_SM4_max_mode_bdc_area );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD),
								_DSDBR01_SMPF_SM4_max_mode_bdc_area_x_length );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD),
								_DSDBR01_SMPF_SM4_sum_mode_bdc_areas );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD),
								_DSDBR01_SMPF_SM4_num_mode_bdc_areas );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD),
								_DSDBR01_SMPF_SM4_max_mode_line_slope );
		}
		//
		///////////////////////////////////////////////////////////////////////////////////////////
		//
		// SM5
		reg_key = CString( CG_DSDBR01_BASE_REGKEY ) + CString( RELATIVE_REGKEY_DSDBR01_SMPF_SM5 );

		// recreate empty registry key
		reg_rval = P_REG->createKey( reg_key.GetBuffer() );

		if( reg_rval == RegInterface::return_type::ok )
		{

			P_REG->setIntValue(reg_key.GetBuffer(),
							CString(REGNAME_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD),
							_DSDBR01_SMPF_SM5_max_mode_width );

			P_REG->setIntValue(reg_key.GetBuffer(),
							CString(REGNAME_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD),
							_DSDBR01_SMPF_SM5_min_mode_width );

			P_REG->setIntValue(reg_key.GetBuffer(),
							CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD),
							_DSDBR01_SMPF_SM5_max_mode_borders_removed );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD),
								_DSDBR01_SMPF_SM5_max_middle_line_rms_value );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD),
								_DSDBR01_SMPF_SM5_min_middle_line_slope );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD),
								_DSDBR01_SMPF_SM5_max_middle_line_slope );

			//P_REG->setDoubleValue(reg_key.GetBuffer(),
			//					CString(REGNAME_DSDBR01_SMPF_MAX_CONTINUITY_SPACING_THRESHOLD),
			//					_DSDBR01_SMPF_SM5_max_continuity_spacing );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD),
								_DSDBR01_SMPF_SM5_mean_perc_working_region );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD),
								_DSDBR01_SMPF_SM5_min_perc_working_region );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD),
								_DSDBR01_SMPF_SM5_modal_distortion_angle );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD),
								_DSDBR01_SMPF_SM5_modal_distortion_angle_x_pos );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD),
								_DSDBR01_SMPF_SM5_max_mode_bdc_area );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD),
								_DSDBR01_SMPF_SM5_max_mode_bdc_area_x_length );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD),
								_DSDBR01_SMPF_SM5_sum_mode_bdc_areas );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD),
								_DSDBR01_SMPF_SM5_num_mode_bdc_areas );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD),
								_DSDBR01_SMPF_SM5_max_mode_line_slope );
		}
		//
		///////////////////////////////////////////////////////////////////////////////////////////
		//
		// SM6
		reg_key = CString( CG_DSDBR01_BASE_REGKEY ) + CString( RELATIVE_REGKEY_DSDBR01_SMPF_SM6 );

		// recreate empty registry key
		reg_rval = P_REG->createKey( reg_key.GetBuffer() );

		if( reg_rval == RegInterface::return_type::ok )
		{

			P_REG->setIntValue(reg_key.GetBuffer(),
							CString(REGNAME_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD),
							_DSDBR01_SMPF_SM6_max_mode_width );

			P_REG->setIntValue(reg_key.GetBuffer(),
							CString(REGNAME_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD),
							_DSDBR01_SMPF_SM6_min_mode_width );

			P_REG->setIntValue(reg_key.GetBuffer(),
							CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD),
							_DSDBR01_SMPF_SM6_max_mode_borders_removed );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD),
								_DSDBR01_SMPF_SM6_max_middle_line_rms_value );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD),
								_DSDBR01_SMPF_SM6_min_middle_line_slope );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD),
								_DSDBR01_SMPF_SM6_max_middle_line_slope );

			//P_REG->setDoubleValue(reg_key.GetBuffer(),
			//					CString(REGNAME_DSDBR01_SMPF_MAX_CONTINUITY_SPACING_THRESHOLD),
			//					_DSDBR01_SMPF_SM6_max_continuity_spacing );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD),
								_DSDBR01_SMPF_SM6_mean_perc_working_region );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD),
								_DSDBR01_SMPF_SM6_min_perc_working_region );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD),
								_DSDBR01_SMPF_SM6_modal_distortion_angle );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD),
								_DSDBR01_SMPF_SM6_modal_distortion_angle_x_pos );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD),
								_DSDBR01_SMPF_SM6_max_mode_bdc_area );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD),
								_DSDBR01_SMPF_SM6_max_mode_bdc_area_x_length );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD),
								_DSDBR01_SMPF_SM6_sum_mode_bdc_areas );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD),
								_DSDBR01_SMPF_SM6_num_mode_bdc_areas );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD),
								_DSDBR01_SMPF_SM6_max_mode_line_slope);
		}
		//
		///////////////////////////////////////////////////////////////////////////////////////////
		//
		// SM7
		reg_key = CString( CG_DSDBR01_BASE_REGKEY ) + CString( RELATIVE_REGKEY_DSDBR01_SMPF_SM7 );

		// recreate empty registry key
		reg_rval = P_REG->createKey( reg_key.GetBuffer() );

		if( reg_rval == RegInterface::return_type::ok )
		{

			P_REG->setIntValue(reg_key.GetBuffer(),
							CString(REGNAME_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD),
							_DSDBR01_SMPF_SM7_max_mode_width );

			P_REG->setIntValue(reg_key.GetBuffer(),
							CString(REGNAME_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD),
							_DSDBR01_SMPF_SM7_min_mode_width );

			P_REG->setIntValue(reg_key.GetBuffer(),
							CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD),
							_DSDBR01_SMPF_SM7_max_mode_borders_removed );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD),
								_DSDBR01_SMPF_SM7_max_middle_line_rms_value );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD),
								_DSDBR01_SMPF_SM7_min_middle_line_slope );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD),
								_DSDBR01_SMPF_SM7_max_middle_line_slope );

			//P_REG->setDoubleValue(reg_key.GetBuffer(),
			//					CString(REGNAME_DSDBR01_SMPF_MAX_CONTINUITY_SPACING_THRESHOLD),
			//					_DSDBR01_SMPF_SM7_max_continuity_spacing );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD),
								_DSDBR01_SMPF_SM7_mean_perc_working_region );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD),
								_DSDBR01_SMPF_SM7_min_perc_working_region );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD),
								_DSDBR01_SMPF_SM7_modal_distortion_angle );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD),
								_DSDBR01_SMPF_SM7_modal_distortion_angle_x_pos );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD),
								_DSDBR01_SMPF_SM7_max_mode_bdc_area );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD),
								_DSDBR01_SMPF_SM7_max_mode_bdc_area_x_length );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD),
								_DSDBR01_SMPF_SM7_sum_mode_bdc_areas );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD),
								_DSDBR01_SMPF_SM7_num_mode_bdc_areas );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD),
								_DSDBR01_SMPF_SM7_max_mode_line_slope );
		}
		//
		///////////////////////////////////////////////////////////////////////////////////////////
		//
		// SM8
		reg_key = CString( CG_DSDBR01_BASE_REGKEY ) + CString( RELATIVE_REGKEY_DSDBR01_SMPF_SM8 );

		// recreate empty registry key
		reg_rval = P_REG->createKey( reg_key.GetBuffer() );

		if( reg_rval == RegInterface::return_type::ok )
		{

			P_REG->setIntValue(reg_key.GetBuffer(),
							CString(REGNAME_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD),
							_DSDBR01_SMPF_SM8_max_mode_width );

			P_REG->setIntValue(reg_key.GetBuffer(),
							CString(REGNAME_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD),
							_DSDBR01_SMPF_SM8_min_mode_width );

			P_REG->setIntValue(reg_key.GetBuffer(),
							CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD),
							_DSDBR01_SMPF_SM8_max_mode_borders_removed );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD),
								_DSDBR01_SMPF_SM8_max_middle_line_rms_value );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD),
								_DSDBR01_SMPF_SM8_min_middle_line_slope );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD),
								_DSDBR01_SMPF_SM8_max_middle_line_slope );

			//P_REG->setDoubleValue(reg_key.GetBuffer(),
			//					CString(REGNAME_DSDBR01_SMPF_MAX_CONTINUITY_SPACING_THRESHOLD),
			//					_DSDBR01_SMPF_SM8_max_continuity_spacing );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD),
								_DSDBR01_SMPF_SM8_mean_perc_working_region );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD),
								_DSDBR01_SMPF_SM8_min_perc_working_region );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD),
								_DSDBR01_SMPF_SM8_modal_distortion_angle );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD),
								_DSDBR01_SMPF_SM8_modal_distortion_angle_x_pos );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD),
								_DSDBR01_SMPF_SM8_max_mode_bdc_area );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD),
								_DSDBR01_SMPF_SM8_max_mode_bdc_area_x_length );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD),
								_DSDBR01_SMPF_SM8_sum_mode_bdc_areas );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD),
								_DSDBR01_SMPF_SM8_num_mode_bdc_areas );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD),
								_DSDBR01_SMPF_SM8_max_mode_line_slope );
		}
		//
		///////////////////////////////////////////////////////////////////////////////////////////
		//
		// SM9
		reg_key = CString( CG_DSDBR01_BASE_REGKEY ) + CString( RELATIVE_REGKEY_DSDBR01_SMPF_SM9 );

		// recreate empty registry key
		reg_rval = P_REG->createKey( reg_key.GetBuffer() );

		if( reg_rval == RegInterface::return_type::ok )
		{

			P_REG->setIntValue(reg_key.GetBuffer(),
							CString(REGNAME_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD),
							_DSDBR01_SMPF_SM9_max_mode_width );

			P_REG->setIntValue(reg_key.GetBuffer(),
							CString(REGNAME_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD),
							_DSDBR01_SMPF_SM9_min_mode_width );

			P_REG->setIntValue(reg_key.GetBuffer(),
							CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD),
							_DSDBR01_SMPF_SM9_max_mode_borders_removed );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD),
								_DSDBR01_SMPF_SM9_max_middle_line_rms_value );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD),
								_DSDBR01_SMPF_SM9_min_middle_line_slope );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD),
								_DSDBR01_SMPF_SM9_max_middle_line_slope );

			//P_REG->setDoubleValue(reg_key.GetBuffer(),
			//					CString(REGNAME_DSDBR01_SMPF_MAX_CONTINUITY_SPACING_THRESHOLD),
			//					_DSDBR01_SMPF_SM9_max_continuity_spacing );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD),
								_DSDBR01_SMPF_SM9_mean_perc_working_region );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD),
								_DSDBR01_SMPF_SM9_min_perc_working_region );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD),
								_DSDBR01_SMPF_SM9_modal_distortion_angle );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD),
								_DSDBR01_SMPF_SM9_modal_distortion_angle_x_pos );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD),
								_DSDBR01_SMPF_SM9_max_mode_bdc_area );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD),
								_DSDBR01_SMPF_SM9_max_mode_bdc_area_x_length );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD),
								_DSDBR01_SMPF_SM9_sum_mode_bdc_areas );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD),
								_DSDBR01_SMPF_SM9_num_mode_bdc_areas );

			P_REG->setDoubleValue(reg_key.GetBuffer(),
								CString(REGNAME_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD),
								_DSDBR01_SMPF_SM9_max_mode_line_slope );
		}
		//
		///////////////////////////////////////////////////////////////////////////////////////////


		// DSDBR01 Coarse Frequency parameters

		reg_key = CString( CG_DSDBR01_BASE_REGKEY ) + CString( RELATIVE_REGKEY_DSDBR01_CFREQ );

		// recreate empty registry key
		reg_rval = P_REG->createKey( reg_key.GetBuffer() );

		if( reg_rval == RegInterface::return_type::ok )
		{
			P_REG->setCStringValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_CFREQ_POLY_COEFFS_ABSPATH ),
				_DSDBR01_CFREQ_poly_coeffs_abspath );

			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_CFREQ_NUM_FREQ_MEAS_PER_OP ),
				_DSDBR01_CFREQ_num_freq_meas_per_op );

			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_CFREQ_POWER_RATIO_MEAS_PER_OP ),
				_DSDBR01_CFREQ_num_power_ratio_meas_per_op );

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_CFREQ_MAX_STABLE_FREQ_ERROR ),
				_DSDBR01_CFREQ_max_stable_freq_error );

			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_CFREQ_SAMPLE_POINT_SETTLE_TIME ),
				_DSDBR01_CFREQ_sample_point_settle_time );

			P_REG->setBoolValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_CFREQ_WRITE_SAMPLE_POINTS_TO_FILE ),
				_DSDBR01_CFREQ_write_sample_points_to_file );

		}


		// DSDBR01 ITU Grid

		reg_key = CString( CG_DSDBR01_BASE_REGKEY ) + CString( RELATIVE_REGKEY_DSDBR01_ITUGRID );

		// recreate empty registry key
		reg_rval = P_REG->createKey( reg_key.GetBuffer() );

		if( reg_rval == RegInterface::return_type::ok )
		{
			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_ITUGRID_CENTRE_FREQ ),
				_DSDBR01_ITUGRID_centre_freq );

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_ITUGRID_STEP_FREQ ),
				_DSDBR01_ITUGRID_step_freq );

			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_ITUGRID_CHANNEL_COUNT ),
				_DSDBR01_ITUGRID_channel_count );

			P_REG->setCStringValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_ITUGRID_abspath ),
				_DSDBR01_ITUGRID_abspath );

			P_REG->setCStringValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_ESTIMATESITUGRID_abspath ),
				_DSDBR01_ESTIMATESITUGRID_abspath );

		}

		// DSDBR01 ITU Grid characterisation parameters

		reg_key = CString( CG_DSDBR01_BASE_REGKEY ) + CString( RELATIVE_REGKEY_DSDBR01_CHAR );

		// recreate empty registry key
		reg_rval = P_REG->createKey( reg_key.GetBuffer() );

		if( reg_rval == RegInterface::return_type::ok )
		{
			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_CHAR_NUM_FREQ_MEAS_PER_OP ),
				_DSDBR01_CHAR_num_freq_meas_per_op );

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_CHAR_FREQ_ACCURACY_OF_WAVEMETER ),
				_DSDBR01_CHAR_freq_accuracy_of_wavemeter );

			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_CHAR_OP_SETTLE_TIME ),
				_DSDBR01_CHAR_op_settle_time );

			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_CHAR_itusearch_scheme ),
				_DSDBR01_CHAR_itusearch_scheme );

			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_CHAR_itusearch_nextguess ),
				_DSDBR01_CHAR_itusearch_nextguess );

			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_CHAR_itusearch_resamples ),
				_DSDBR01_CHAR_itusearch_resamples );

			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_CHAR_POWER_RATIO_MEAS_PER_OP ),
				_DSDBR01_CHAR_num_power_ratio_meas_per_op );

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_CHAR_MAX_STABLE_FREQ_ERROR ),
				_DSDBR01_CHAR_max_stable_freq_error );

			P_REG->setBoolValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_CHAR_WRITE_DUFF_POINTS_TO_FILE ),
				_DSDBR01_CHAR_write_duff_points_to_file );

			P_REG->setBoolValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_CHAR_prevent_front_section_switch ),
				_DSDBR01_CHAR_prevent_front_section_switch );

			P_REG->setCStringValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_CHAR_fakefreq_Iramp_module_name ),
				_DSDBR01_CHAR_fakefreq_Iramp_module_name );

			P_REG->setCStringValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_CHAR_Iramp_vs_fakefreq_abspath ),
				_DSDBR01_CHAR_Iramp_vs_fakefreq_abspath );

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_CHAR_fakefreq_max_error ),
				_DSDBR01_CHAR_fakefreq_max_error );
		}

		// DSDBR01 TEC Settling parameters

		reg_key = CString( CG_DSDBR01_BASE_REGKEY ) + CString( RELATIVE_REGKEY_DSDBR01_CHARTEC );

		// recreate empty registry key
		reg_rval = P_REG->createKey( reg_key.GetBuffer() );

		if( reg_rval == RegInterface::return_type::ok )
		{
			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_CHARTEC_time_window_size ),
				_DSDBR01_CHARTEC_time_window_size );

			P_REG->setDoubleValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_CHARTEC_max_temp_deviation ),
				_DSDBR01_CHARTEC_max_temp_deviation );

			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_CHARTEC_num_temp_readings ),
				_DSDBR01_CHARTEC_num_temp_readings );

			P_REG->setIntValue(
				reg_key.GetBuffer(),
				CString( REGNAME_DSDBR01_CHARTEC_max_settle_time ),
				_DSDBR01_CHARTEC_max_settle_time );
		}
	}


	return rval;
}


//
/////////////////////////////////////////////////////////////////
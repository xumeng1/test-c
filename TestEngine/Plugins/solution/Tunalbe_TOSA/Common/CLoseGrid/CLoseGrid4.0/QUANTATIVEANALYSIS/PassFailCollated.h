
#ifndef __PASSFAILCOLLATED__H__
#define __PASSFAILCOLLATED__H__

#include <afx.h>

#include "OverallModeMapAnalysis.h"
#include "SuperModeMapAnalysis.h"


class PassFailCollated
{
public:
	// Singleton class
	static PassFailCollated* instance();

	//////////////////////////////////////////////////

					// set parameters needed to create results file
	void setFileParameters(	CString	resultsDir,
							CString	laserId,
							CString	dateTimeStamp);

	void setOverallModeMapAnalysis(OverallModeMapAnalysis* overallModeMapAnalysis);
	void addAnalysedSuperMode(CDSDBRSuperMode* superMode);

						// collate results and write to file
	void writeResultsToFile();

	void clearSuperModes();

	////////////////////////////////////////

	bool			_totalPassed;
	bool			_overallModeMapPassed;
	bool			_superModeMapPassed;
	vector<short>	_superModeMapsPassed;

	//////////////////////////////////////////////////

protected:
	PassFailCollated();
	~PassFailCollated();

	PassFailCollated(const PassFailCollated&);

	// = operator for returning the instance
	PassFailCollated& operator= (const PassFailCollated&);

private:
			// the instance of this class
	static	PassFailCollated*	theInstance;
	

	////////////////////////////////////////////////////
	///
	//
				// set all attributes to 0
	void	clear();

	CString	getResultsAbsFilePath(CString	resultsDir,
								CString		laserId,
								CString		dateTimeStamp);

	////////////////////////////////////////////////////

	CString	_resultsDir;
	CString	_laserId;
	CString	_dateTimeStamp;

	CString	_collatedPassFailResultsAbsFilePath;

	///////////////////////////////////////////////////

	OverallModeMapAnalysis*			_p_overallModeMapAnalysis;
	vector<CDSDBRSuperMode*>		_v_p_superModes;

	int		_numSuperModesAnalysed;

	///////////////////////////////////////////////////

	//
	///
	///////////////////////////////////////////////////
};
#endif	// __PASSFAILCOLLATED__H__
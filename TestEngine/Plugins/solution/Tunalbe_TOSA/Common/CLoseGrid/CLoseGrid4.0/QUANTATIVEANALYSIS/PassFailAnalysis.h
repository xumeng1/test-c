// Copyright (c) 2003 Tsunami Photonics Ltd. All Rights Reserved

///////////////////////////////////////////////////////////////////////////////
///
//	Class		:	PassFailAnalysis
//
//	Contents	:	Compares QA results with set thresholds to determine
//					whether the results have passed or failed.
//
//	Containment	:	
//
//	Multiplicity:	Many
//
//	Created by	:	QuantativeAnalysis
//
//	Author		:	Richard Ashe
//
//	Date		:	14/01/2003
///
///////////////////////////////////////////////////////////////////////////////
///
//
#ifndef __PASSFAILANALYSIS_H_
#define __PASSFAILANALYSIS_H_

#pragma once

#include <vector>

/////////////////////////////////////////////////////////////////////

class PassFailAnalysis
{
public:

	PassFailAnalysis();

	~PassFailAnalysis(void);


	// initialise object
	// and get settings from 
	// the registry using
	// getConfigInfo
	void	init();

	/////////////////////////////////////////////
	///	Functionality
	//
	void	runPassFailWithUpperLimit(std::vector<double> values);

	void	runPassFailWithLowerLimit(std::vector<double> values);

	void	runPassFailWithUpperLimit(std::vector<double> values,std::vector<bool> ignore);

	void	runPassFailWithLowerLimit(std::vector<double> values,std::vector<bool> ignore);
	
	void	runPassFailWithUpperLimit( double value );

	void	runPassFailWithLowerLimit( double value );

	//
	/////////////////////////////////////////////

	//
	///	the results of pass fail analysis
	//
					// threshold to compare
					// results with
	double			threshold;

					// Pass or Fail
	bool			pass;
					
					// array of Modes which failed
	std::vector<double>	failedModes;

					// no. of LMs which failed
	int				numFailedModes;

					// values which failed
	std::vector<double>	valuesWhichFailed;

					// smallest value measured
	double			minValue;

					// largest value measured
	double			maxValue;

	//
	///////////////////////////////////////////////////////

private:
	/////////////////////////////////////////////
	///	Private member functions
	//

	//
	///	Standard Functions
	//

	//
	/////////////////////////////////////////////
};
//
///
///////////////////////////////////////////////////////////////////////////////
#endif

// Copyright (c) 2004 PXIT Ltd. All Rights Reserved

///////////////////////////////////////////////////////////////////////////////
///
//	Class	:
//
//	Contents:
//
//	Author	:
//
///
///////////////////////////////////////////////////////////////////////////////
///
//

#include "stdafx.h"

#include <algorithm>

#include "HysteresisAnalysis.h"
#include "ModeBoundaryLine.h"
#include "CGResearchServerInterface.h"

//
/////////////////////////////////////////////////////////////////////
//

#define new DEBUG_NEW

/////////////////////////////////////////////////////////////////////

//
///	ctor and dtor
//
HysteresisAnalysis::
HysteresisAnalysis(ModeAnalysis&	forwardMode,
				   ModeAnalysis&	reverseMode,
				   int				xAxisLength,
				   int				yAxisLength)
{
	CGR_LOG("HysteresisAnalysis::HysteresisAnalysis() entered",DIAGNOSTIC_CGR_LOG)

	_forwardMode = forwardMode;
	_reverseMode = reverseMode;

	_xAxisLength = xAxisLength;
	_yAxisLength = yAxisLength;

	init();

	CString log_msg("HysteresisAnalysis::HysteresisAnalysis() : ");
	log_msg.AppendFormat("created _forwardMode._lowerBoundaryLine._x.size()=%d, _forwardMode._lowerBoundaryLine._y.size()=%d",
		_forwardMode._lowerBoundaryLine._x.size(), _forwardMode._lowerBoundaryLine._y.size() );
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	log_msg = CString("HysteresisAnalysis::HysteresisAnalysis() : ");
	log_msg.AppendFormat("created _forwardMode._upperBoundaryLine._x.size()=%d, _forwardMode._upperBoundaryLine._y.size()=%d",
		_forwardMode._upperBoundaryLine._x.size(), _forwardMode._upperBoundaryLine._y.size() );
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	log_msg = CString("HysteresisAnalysis::HysteresisAnalysis() : ");
	log_msg.AppendFormat("created _reverseMode._lowerBoundaryLine._x.size()=%d, _reverseMode._lowerBoundaryLine._y.size()=%d",
		_reverseMode._lowerBoundaryLine._x.size(), _reverseMode._lowerBoundaryLine._y.size() );
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	log_msg = CString("HysteresisAnalysis::HysteresisAnalysis() : ");
	log_msg.AppendFormat("created _reverseMode._upperBoundaryLine._x.size()=%d, _reverseMode._upperBoundaryLine._y.size()=%d",
		_reverseMode._upperBoundaryLine._x.size(), _reverseMode._upperBoundaryLine._y.size() );
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	log_msg = CString("HysteresisAnalysis::HysteresisAnalysis() : ");
	log_msg.AppendFormat("created  _xAxisLength = %d, _yAxisLength = %d",
		_xAxisLength, _yAxisLength );
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	CGR_LOG("HysteresisAnalysis::HysteresisAnalysis() exiting",DIAGNOSTIC_CGR_LOG)
}

HysteresisAnalysis::
HysteresisAnalysis()
{
	init();
}

HysteresisAnalysis::
~HysteresisAnalysis(void)
{
}

void
HysteresisAnalysis::
clear()
{
	_forwardMode.init();
	_reverseMode.init();
	_innerHysteresisMode.init();
	_outerHysteresisMode.init();

	_xAxisLength = 0;
	_yAxisLength = 0;

	_stableAreaWidths.clear();
	_totalWidths.clear();
	_percWorkingRegions.clear();

	_maxStableAreaWidth			= 0;
	_minStableAreaWidth			= 0;
	_meanStableAreaWidths		= 0;
	_sumStableAreaWidths		= 0;
	_numStableAreaWidthSamples	= 0;
	_stdDevStableAreaWidths		= 0;
	_varStableAreaWidths		= 0;


	_maxPercWorkingRegion		= 0;
	_minPercWorkingRegion		= 0;
	_meanPercWorkingRegions		= 0;
	_sumPercWorkingRegions		= 0;
	_numPercWorkingRegionSamples= 0;
	_stdDevPercWorkingRegions	= 0;
	_varPercWorkingRegions		= 0;

}

void
HysteresisAnalysis::
init()
{
	/////////////////////////////
	// initialise attributes
	// before use

	//////////////////////////////////////////////////

	_stableAreaWidths.clear();

	_maxStableAreaWidth			= 0;
	_minStableAreaWidth			= 0;
	_meanStableAreaWidths		= 0;
	_sumStableAreaWidths		= 0;
	_numStableAreaWidthSamples	= 0;
	_stdDevStableAreaWidths		= 0;
	_varStableAreaWidths		= 0;

	//////////////////////////////////////////////////

	// create inner hysteresis mode

	_innerHysteresisMode.loadBoundaryLines(0, _xAxisLength, _yAxisLength,_forwardMode._upperBoundaryLine, _reverseMode._lowerBoundaryLine);

	//////////////////////////////////////////////////

	// create outer hysteresis mode
	CGR_LOG("HysteresisAnalysis::init() calling _outerHysteresisMode.loadBoundaryLines ...",DIAGNOSTIC_CGR_LOG)

	_outerHysteresisMode.loadBoundaryLines(0, _xAxisLength, _yAxisLength,_reverseMode._upperBoundaryLine, _forwardMode._lowerBoundaryLine);

	//////////////////////////////////////////////////
}

//GDM 31/10/06 need to access some QaThresholds so init vars
void
HysteresisAnalysis::
setQAThresholds(QAThresholds* qaThresholds)
{
	_qaThresholds = qaThresholds;
}

HysteresisAnalysis::rcode
HysteresisAnalysis::
runAnalysis()
{
	rcode err = rcode::ok;

	if(err == rcode::ok)
		err = runStableAreaWidthsAnalysis();		
	if(err == rcode::ok)
		runPercWorkingRegionAnalysis();

	return err;
}

/////////////////////////////////////////////////////////////////////
///
//
HysteresisAnalysis::rcode
HysteresisAnalysis::
runStableAreaWidthsAnalysis()
{
	rcode err = rcode::ok;

	Vector stableAreaWidths;
	getStableAreaWidths(stableAreaWidths);

	calcMaxStableAreaWidths();
	calcMinStableAreaWidths();
	calcMeanStableAreaWidths();
	calcSumStableAreaWidths();
	calcNumStableAreaWidthSamples();
	calcStdDevStableAreaWidths();
	calcVarStableAreaWidths();

	return err;
}

///////////////////////////////////////////////////////////////////////

HysteresisAnalysis::rcode
HysteresisAnalysis::
runPercWorkingRegionAnalysis()
{
	rcode err = rcode::ok;

	Vector percWorkingRegions;
	getPercWorkingRegions(percWorkingRegions);

	calcMaxPercWorkingRegions();
	calcMinPercWorkingRegions();
	calcMeanPercWorkingRegions();
	calcSumPercWorkingRegions();
	calcNumPercWorkingRegionSamples();
	calcStdDevPercWorkingRegions();
	calcVarPercWorkingRegions();

	return err;
}

///////////////////////////////////////////////////////////////////////

//
///
/////////////////////////////////////////////////////////////////////
///
//
void
HysteresisAnalysis::
getStableAreaWidths(Vector& stableAreaWidths)
{

	short startIndex = 0;
	short stopIndex;
	


	if(_stableAreaWidths.size() == 0)
		//GDM 31/10/06 change of call to getModeWidths to include start and stop index's
		if (_qaThresholds->_SMMapQA == 1)
		{
			startIndex = (short)_qaThresholds->_hysteresisAnalysisMinX;
			stopIndex = (short)_qaThresholds->_hysteresisAnalysisMaxX;
			_innerHysteresisMode.getModeWidths(_stableAreaWidths, startIndex, stopIndex );
		}
		if (_qaThresholds->_SMMapQA == 0)
		{
		//old call
			if (_innerHysteresisMode._lowerBoundaryLine.numXPoints() > _outerHysteresisMode._lowerBoundaryLine.numXPoints())
				startIndex = ((short)(_outerHysteresisMode._lowerBoundaryLine.x(0)) - (short)(_innerHysteresisMode._lowerBoundaryLine.x(0)));

			_innerHysteresisMode.getModeWidths(_stableAreaWidths, startIndex);
		}
	stableAreaWidths = _stableAreaWidths;
}
double
HysteresisAnalysis::
getStableAreaWidthsStatistic(Vector::VectorAttribute attrib)
{
	double returnValue = 0;

	Vector stableAreaWidths;
	getStableAreaWidths(stableAreaWidths);

	returnValue = stableAreaWidths.getVectorAttribute(attrib);

	return returnValue;
}
//
////////////////////////////////////////////////////////////////////////////////////////////
//
void
HysteresisAnalysis::
calcMaxStableAreaWidths()
{
	Vector stableAreaWidths;
	getStableAreaWidths(stableAreaWidths);

	_maxStableAreaWidth = stableAreaWidths.getVectorAttribute(Vector::max);
}
void
HysteresisAnalysis::
calcMinStableAreaWidths()
{
	Vector stableAreaWidths;
	getStableAreaWidths(stableAreaWidths);

	_minStableAreaWidth = stableAreaWidths.getVectorAttribute(Vector::min);
}
void
HysteresisAnalysis::
calcMeanStableAreaWidths()
{
	Vector stableAreaWidths;
	getStableAreaWidths(stableAreaWidths);

	_meanStableAreaWidths = stableAreaWidths.getVectorAttribute(Vector::mean);
}
void
HysteresisAnalysis::
calcSumStableAreaWidths()
{
	Vector stableAreaWidths;
	getStableAreaWidths(stableAreaWidths);

	_sumStableAreaWidths = stableAreaWidths.getVectorAttribute(Vector::sum);
}
void
HysteresisAnalysis::
calcNumStableAreaWidthSamples()
{
	Vector stableAreaWidths;
	getStableAreaWidths(stableAreaWidths);

	_numStableAreaWidthSamples = stableAreaWidths.getVectorAttribute(Vector::count);
}
void
HysteresisAnalysis::
calcVarStableAreaWidths()
{
	Vector stableAreaWidths;
	getStableAreaWidths(stableAreaWidths);

	_varStableAreaWidths = stableAreaWidths.getVectorAttribute(Vector::variance);
}
void
HysteresisAnalysis::
calcStdDevStableAreaWidths()
{
	Vector stableAreaWidths;
	getStableAreaWidths(stableAreaWidths);

	_stdDevStableAreaWidths = stableAreaWidths.getVectorAttribute(Vector::stdDev);
}

//
///
/////////////////////////////////////////////////////////////////////
///
//
void
HysteresisAnalysis::
getTotalWidths(Vector& totalWidths)
{
	short startIndex = 0;
	short stopIndex;
	

	if(_totalWidths.size() == 0)
		if (_qaThresholds->_SMMapQA == 1)
		{
		//GDM 31/10/06 change in call to getmodewidths to permit start and stop index
		startIndex = _qaThresholds->_hysteresisAnalysisMinX;
		stopIndex = _qaThresholds->_hysteresisAnalysisMaxX;

		_outerHysteresisMode.getModeWidths(_totalWidths, startIndex, stopIndex);
		}
		if (_qaThresholds->_SMMapQA == 0)
		{
			//old call  
			_outerHysteresisMode.getModeWidths(_totalWidths);
		}
	totalWidths = _totalWidths;
}

//
///
/////////////////////////////////////////////////////////////////////
///
//
void
HysteresisAnalysis::
getPercWorkingRegions(Vector& percWorkingRegions)
{
	if(_percWorkingRegions.size() == 0)
	{
		Vector totalWidths;
		getTotalWidths(totalWidths);

		Vector stableAreaWidths;
		getStableAreaWidths(stableAreaWidths);

		for(int i=0; i<(int)_stableAreaWidths.size(); i++)
		{
			double percWorkingRegion = 0;

			double stableAreaWidth = stableAreaWidths[i];
			double totalWidth = totalWidths[i];

			if(stableAreaWidth < totalWidth)
				percWorkingRegion = 100 * ( stableAreaWidth/totalWidth );
			else
				percWorkingRegion = 100;

			_percWorkingRegions.push_back(percWorkingRegion);
		}
	}

	percWorkingRegions = _percWorkingRegions;
}
double
HysteresisAnalysis::
getPercWorkingRegionsStatistic(Vector::VectorAttribute attrib)
{
	double returnValue = 0;

	Vector percWorkingRegions;
	getPercWorkingRegions(percWorkingRegions);

	returnValue = percWorkingRegions.getVectorAttribute(attrib);

	return returnValue;
}
//
////////////////////////////////////////////////////////////////////////////////////////////
//
void
HysteresisAnalysis::
calcMaxPercWorkingRegions()
{
	Vector percWorkingRegions;
	getPercWorkingRegions(percWorkingRegions);

	_maxPercWorkingRegion = percWorkingRegions.getVectorAttribute(Vector::max);
}
void
HysteresisAnalysis::
calcMinPercWorkingRegions()
{
	Vector percWorkingRegions;
	getPercWorkingRegions(percWorkingRegions);

	_minPercWorkingRegion = percWorkingRegions.getVectorAttribute(Vector::min);
}
void
HysteresisAnalysis::
calcMeanPercWorkingRegions()
{
	Vector percWorkingRegions;
	getPercWorkingRegions(percWorkingRegions);

	_meanPercWorkingRegions = percWorkingRegions.getVectorAttribute(Vector::mean);
}
void
HysteresisAnalysis::
calcSumPercWorkingRegions()
{
	Vector percWorkingRegions;
	getPercWorkingRegions(percWorkingRegions);

	_sumPercWorkingRegions = percWorkingRegions.getVectorAttribute(Vector::sum);
}
void
HysteresisAnalysis::
calcNumPercWorkingRegionSamples()
{
	Vector percWorkingRegions;
	getPercWorkingRegions(percWorkingRegions);

	_numPercWorkingRegionSamples = percWorkingRegions.getVectorAttribute(Vector::count);
}
void
HysteresisAnalysis::
calcVarPercWorkingRegions()
{
	Vector percWorkingRegions;
	getPercWorkingRegions(percWorkingRegions);

	_varPercWorkingRegions = percWorkingRegions.getVectorAttribute(Vector::variance);
}
void
HysteresisAnalysis::
calcStdDevPercWorkingRegions()
{
	Vector percWorkingRegions;
	getPercWorkingRegions(percWorkingRegions);

	_stdDevPercWorkingRegions = percWorkingRegions.getVectorAttribute(Vector::stdDev);
}
//
///
//
//
///	End of HysteresisAnalysis.cpp
///////////////////////////////////////////////////////////////////////////////
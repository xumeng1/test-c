// Copyright (c) 2004 PXIT Ltd. All Rights Reserved

///////////////////////////////////////////////////////////////////////////////
///
//	Class	:
//
//	Contents:
//
//	Author	:
//
///
///////////////////////////////////////////////////////////////////////////////
///
//
#ifndef __MODEBOUNDARYLINE_H_
#define __MODEBOUNDARYLINE_H_

#pragma once

//
///////////////////////////////////////////////////////////////////////////////
//
#include <afx.h>

#include "Vector.h"

using namespace std;

/////////////////////////////////////////////////////////////////////

class ModeBoundaryLine
{
public:
	ModeBoundaryLine(const vector<double>& xPoints,
					const vector<double>& yPoints,
					bool crosses_front_sections = false );
	ModeBoundaryLine();
	~ModeBoundaryLine(void);

	int		numXPoints();
	int		numYPoints();

	double	x(int index);
	double	y(int index);

	std::vector<double>		getSlopes(int slopeWindow);

	void	runModalDistortionAnalysis(double modalDistortionMinX,
									double modalDistortionMaxX);
	double	getMaxModalDistortionAngle();
	double	getMaxModalDistortionAngleXPosition();


	std::vector<double>	xPoints();
	std::vector<double>	yPoints();
	bool _crosses_front_sections;

	///////////////////////////////////////////////////
	///
	//
	void	runModeBDCAreaAnalysis();
	
	Vector	getModeBDCAreas();
	double	getMinModeBDCArea();
	double	getMaxModeBDCArea();
	double	getMeanModeBDCAreas();
	double	getSumModeBDCAreas();
	double	getNumModeBDCAreas();
	double	getStdDevModeBDCAreas();
	double	getVarModeBDCAreas();


	Vector	getModeBDCAreaXLengths();
	double	getMaxModeBDCAreaXLength();
	double	getMinModeBDCAreaXLength();
	double	getMeanModeBDCAreaXLengths();
	double	getSumModeBDCAreaXLengths();
	double	getNumModeBDCAreaXLengths();
	double	getStdDevModeBDCAreaXLengths();
	double	getVarModeBDCAreaXLengths();
	//
	////////////////////////////////////////////////////

	///////////////////////////////////////////////////////
	//
	std::vector<double>	_x;
	std::vector<double>	_y;

	std::vector<double>	_slopes;

	Vector	_modalDistortionAngles;
	double	_maxModalDistortionAngle;
	double	_maxModalDistortionAngleXPosition;


	Vector	_modeBDCAreas;
	double	_maxModeBDCArea;
	double	_minModeBDCArea;
	double	_meanModeBDCArea;
	double	_sumModeBDCAreas;
	double	_numModeBDCAreas;
	double	_stdDevModeBDCArea;
	double	_varModeBDCArea;

	Vector	_modeBDCAreaXLengths;
	double	_maxModeBDCAreaXLength;
	double	_minModeBDCAreaXLength;
	double	_meanModeBDCAreaXLength;
	double	_sumModeBDCAreaXLengths;
	double	_numModeBDCAreaXLengths;
	double	_stdDevModeBDCAreaXLength;
	double	_varModeBDCAreaXLength;

	bool	_modeBDCAreaAnalysisRun;

	//////////////////////////////////////////////////////////

	double getSlopeInDegrees(double x1,
							double	y1,
							double	x2,
							double	y2);

};
//
///////////////////////////////////////////////////////////////////////////////
#endif

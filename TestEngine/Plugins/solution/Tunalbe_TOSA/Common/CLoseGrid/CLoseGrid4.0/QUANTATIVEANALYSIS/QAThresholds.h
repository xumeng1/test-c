// Copyright (c) 2002 Tsunami Photonics Ltd. All Rights Reserved
//
// FileName    : QAThresholds.h
// Description : Declaration of Vector class
//
// Rev#  | Date          | Author               | Description of change
// --------------------------------------------------------------
// 0.1   | 02 Oct 2002   | Frank D'Arcy         | Initial Draft
//

#ifndef __QATHRESHOLDS__H__
#define __QATHRESHOLDS__H__

class QAThresholds 
{
public:
	QAThresholds();
    ~QAThresholds( );
	void	setThresholdsToDefault();

	////////////////////////////////////
	int		_SMMapQA;		//set to 1 if SuperModeQA
	int		_slopeWindow;
	double	_modalDistortionMinX;
	double	_modalDistortionMaxX;
	double	_modalDistortionMinY;
	double	_modeWidthAnalysisMinX;
	double	_modeWidthAnalysisMaxX;
	double	_hysteresisAnalysisMinX;
	double	_hysteresisAnalysisMaxX;


	////////////////////////////////////

};

#endif	// QATHRESHOLDS__H__

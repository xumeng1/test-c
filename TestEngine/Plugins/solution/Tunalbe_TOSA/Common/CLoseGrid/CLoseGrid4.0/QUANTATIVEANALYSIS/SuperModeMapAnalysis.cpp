// Copyright (c) 2004 PXIT Ltd. All Rights Reserved

///////////////////////////////////////////////////////////////////////////////
///
//	Class	:
//
//	Contents:
//
//	Author	:
//
///
///////////////////////////////////////////////////////////////////////////////
///
//
#include "stdafx.h"

#include <algorithm>
#include <math.h>

#include "SuperModeMapAnalysis.h"
#include "CLoseGridFile.h"
#include "CGRegValues.h"
#include "VectorAnalysis.h"
#include "defaults.h"
//
/////////////////////////////////////////////////////////////////////
//

#define new DEBUG_NEW


#define MIN_NUMBER_OF_MODES 5

#define RESULTS_NO_ERROR				0
#define RESULTS_GENERAL_ERROR			1
#define RESULTS_NO_MODEMAP_DATA			2
#define RESULTS_PO_TOO_LOW				4
#define RESULTS_PO_TOO_HIGH				8
#define RESULTS_TOO_MANY_JUMPS			16
#define RESULTS_TOO_FEW_JUMPS			32
#define RESULTS_INVALID_NUMBER_OF_LINES 64
#define RESULTS_INVALID_LINES_REMOVED	128
#define RESULTS_FORWARD_LINES_MISSING	256
#define RESULTS_REVERSE_LINES_MISSING	512


/////////////////////////////////////////////////////////////////////

//
///	ctor and dtor
//

SuperModeMapAnalysis::
SuperModeMapAnalysis()
{
}

SuperModeMapAnalysis::
~SuperModeMapAnalysis(void)
{
}

void
SuperModeMapAnalysis::
createModeAnalysisVectors(const vector<ModeBoundaryLine>&	upperLines, 
						const vector<ModeBoundaryLine>&		lowerLines)
{
	// create a vector of forward modes and a vector of reverse modes

	for( int i = 0 ; i < (int)_forwardModes.size() ; i++ )
	{
		_forwardModes[i].init();
	}
	_forwardModes.clear();

	for( int i = 0 ; i < (int)_reverseModes.size() ; i++ )
	{
		_reverseModes[i].init();
	}
	_reverseModes.clear();

	if( lowerLines.size() > 0
	 && lowerLines.size() == upperLines.size() )
	{
		// forward modes - the lower lines
		ModeAnalysis forwardMode;
		ModeAnalysis reverseMode;

		getQAThresholds();

		for(int i=1; i<(int)lowerLines.size(); i++)
		{
			forwardMode.loadBoundaryLines(i, _xAxisLength, _yAxisLength,lowerLines[i], lowerLines[i-1]);
			forwardMode.setQAThresholds(&_qaThresholds);

			_forwardModes.push_back(forwardMode);

			reverseMode.loadBoundaryLines(i, _xAxisLength, _yAxisLength, upperLines[i], upperLines[i-1]);
			reverseMode.setQAThresholds(&_qaThresholds);

			_reverseModes.push_back(reverseMode);
		}
	}
}

void
SuperModeMapAnalysis::
createHysteresisAnalysisVector(const vector<ModeAnalysis>&	forwardModes, 
							const vector<ModeAnalysis>&		reverseModes)
{
	int numModes =0;

	for( int i = 0 ; i < (int)_hysteresisAnalysis.size() ; i++ )
	{
		_hysteresisAnalysis[i].clear();
	}
	_hysteresisAnalysis.clear();

	//GDM 31/10/06 need to get the QAAnalysis window for hysteresis
	getQAThresholds();
	
	if( forwardModes.size() > 0
	 && reverseModes.size() == forwardModes.size() )
	{
		numModes = (int)forwardModes.size();

		HysteresisAnalysis hysteresisAnalysis;
		for(int i=0; i<numModes; i++)
		{
			ModeAnalysis forwardMode = forwardModes[i];
			ModeAnalysis reverseMode = reverseModes[i];
			
			//GDM 31/10/06 added  req for _qaThreshold for hysteresis analysis in  fwd and rev modes
			forwardMode.setQAThresholds(&_qaThresholds);
			reverseMode.setQAThresholds(&_qaThresholds);
			hysteresisAnalysis.setQAThresholds(&_qaThresholds);

			if( forwardMode._lowerBoundaryLine._x.size() > 0
			 && forwardMode._lowerBoundaryLine._y.size() == forwardMode._lowerBoundaryLine._x.size()
			 && reverseMode._lowerBoundaryLine._x.size() > 0
			 && reverseMode._lowerBoundaryLine._y.size() == reverseMode._lowerBoundaryLine._x.size() )
			{
				hysteresisAnalysis = HysteresisAnalysis(forwardMode, reverseMode, _xAxisLength, _yAxisLength);
				_hysteresisAnalysis.push_back(hysteresisAnalysis);
			}
		}
	}

	_numHysteresisModes = (int)_hysteresisAnalysis.size();
}

void
SuperModeMapAnalysis::
getQAThresholds()
{
	_qaThresholds._slopeWindow			= CG_REG->get_DSDBR01_SMQA_slope_window_size();
	_qaThresholds._modalDistortionMinX	= CG_REG->get_DSDBR01_SMQA_modal_distortion_min_x();
	_qaThresholds._modalDistortionMaxX	= CG_REG->get_DSDBR01_SMQA_modal_distortion_max_x();
	_qaThresholds._modalDistortionMinY	= CG_REG->get_DSDBR01_SMQA_modal_distortion_min_y();
	//GDM 31/10/06 new members to hold hysteresis and mode width analysis windows
	_qaThresholds._hysteresisAnalysisMinX	= CG_REG->get_DSDBR01_SMQA_hysteresis_analysis_min_x();
	_qaThresholds._hysteresisAnalysisMaxX	= CG_REG->get_DSDBR01_SMQA_hysteresis_analysis_max_x();
	_qaThresholds._modeWidthAnalysisMinX	= CG_REG->get_DSDBR01_SMQA_mode_width_analysis_min_x();
	_qaThresholds._modeWidthAnalysisMaxX	= CG_REG->get_DSDBR01_SMQA_mode_width_analysis_max_x();
	//GDM 31/10/06 flag that this is the OM qa to the analysis sub
	_qaThresholds._SMMapQA = 1;
	//GDM
}

void
SuperModeMapAnalysis::
init()
{
	for( int i = 0 ; i < (int)_forwardModes.size() ; i++ )
		_forwardModes[i].init();
	_forwardModes.clear();

	for( int i = 0 ; i < (int)_reverseModes.size() ; i++ )
		_reverseModes[i].init();
	_reverseModes.clear();

	for( int i = 0 ; i < (int)_hysteresisAnalysis.size() ; i++ )
		_hysteresisAnalysis[i].clear();
	_hysteresisAnalysis.clear();
	

	////////////////////////////////////////////////
	///
	//	QA Data
	//
	_forwardModeWidths.clear();
	_maxForwardModeWidths.clear();
	_minForwardModeWidths.clear();
	_meanForwardModeWidths.clear();
	_sumForwardModeWidths.clear();
	_varForwardModeWidths.clear();
	_stdDevForwardModeWidths.clear();
	_medianForwardModeWidths.clear();
	_numForwardModeWidthSamples.clear();

	///////////////////////////////////////

	_forwardLowerModeSlopes.clear();
	_maxForwardLowerModeSlopes.clear();
	_minForwardLowerModeSlopes.clear();
	_meanForwardLowerModeSlopes.clear();
	_sumForwardLowerModeSlopes.clear();
	_varForwardLowerModeSlopes.clear();
	_stdDevForwardLowerModeSlopes.clear();
	_medianForwardLowerModeSlopes.clear();
	_numForwardLowerModeSlopeSamples.clear();

	_forwardUpperModeSlopes.clear();
	_maxForwardUpperModeSlopes.clear();
	_minForwardUpperModeSlopes.clear();
	_meanForwardUpperModeSlopes.clear();
	_sumForwardUpperModeSlopes.clear();
	_varForwardUpperModeSlopes.clear();
	_stdDevForwardUpperModeSlopes.clear();
	_medianForwardUpperModeSlopes.clear();
	_numForwardUpperModeSlopeSamples.clear();

	///////////////////////////////////////

	_forwardLowerModalDistortionAngles.clear();
	_forwardUpperModalDistortionAngles.clear();

	///////////////////////////////////////

	_forwardLowerModeBDCAreas.clear();
	_maxForwardLowerModeBDCAreas.clear();
	_minForwardLowerModeBDCAreas.clear();
	_meanForwardLowerModeBDCAreas.clear();
	_sumForwardLowerModeBDCAreas.clear();
	_varForwardLowerModeBDCAreas.clear();
	_stdDevForwardLowerModeBDCAreas.clear();
	_medianForwardLowerModeBDCAreas.clear();
	_numForwardLowerModeBDCAreaSamples.clear();
	//
	_forwardUpperModeBDCAreas.clear();
	_maxForwardUpperModeBDCAreas.clear();
	_minForwardUpperModeBDCAreas.clear();
	_meanForwardUpperModeBDCAreas.clear();
	_sumForwardUpperModeBDCAreas.clear();
	_varForwardUpperModeBDCAreas.clear();
	_stdDevForwardUpperModeBDCAreas.clear();
	_medianForwardUpperModeBDCAreas.clear();
	_numForwardUpperModeBDCAreaSamples.clear();

	///////////////////////////////////////

	_forwardLowerModeBDCAreaXLengths.clear();
	_maxForwardLowerModeBDCAreaXLengths.clear();
	_minForwardLowerModeBDCAreaXLengths.clear();
	_meanForwardLowerModeBDCAreaXLengths.clear();
	_sumForwardLowerModeBDCAreaXLengths.clear();
	_varForwardLowerModeBDCAreaXLengths.clear();
	_stdDevForwardLowerModeBDCAreaXLengths.clear();
	_medianForwardLowerModeBDCAreaXLengths.clear();
	_numForwardLowerModeBDCAreaXLengthSamples.clear();

	_forwardUpperModeBDCAreaXLengths.clear();
	_maxForwardUpperModeBDCAreaXLengths.clear();
	_minForwardUpperModeBDCAreaXLengths.clear();
	_meanForwardUpperModeBDCAreaXLengths.clear();
	_sumForwardUpperModeBDCAreaXLengths.clear();
	_varForwardUpperModeBDCAreaXLengths.clear();
	_stdDevForwardUpperModeBDCAreaXLengths.clear();
	_medianForwardUpperModeBDCAreaXLengths.clear();
	_numForwardUpperModeBDCAreaXLengthSamples.clear();

	//
	///////////////////////////////////////
	//

	_reverseModeWidths.clear();
	_maxReverseModeWidths.clear();
	_minReverseModeWidths.clear();
	_meanReverseModeWidths.clear();
	_sumReverseModeWidths.clear();
	_varReverseModeWidths.clear();
	_stdDevReverseModeWidths.clear();
	_medianReverseModeWidths.clear();
	_numReverseModeWidthSamples.clear();

	///////////////////////////////////////

	_reverseLowerModeSlopes.clear();
	_maxReverseLowerModeSlopes.clear();
	_minReverseLowerModeSlopes.clear();
	_meanReverseLowerModeSlopes.clear();
	_sumReverseLowerModeSlopes.clear();
	_varReverseLowerModeSlopes.clear();
	_stdDevReverseLowerModeSlopes.clear();
	_medianReverseLowerModeSlopes.clear();
	_numReverseLowerModeSlopeSamples.clear();

	_reverseUpperModeSlopes.clear();
	_maxReverseUpperModeSlopes.clear();
	_minReverseUpperModeSlopes.clear();
	_meanReverseUpperModeSlopes.clear();
	_sumReverseUpperModeSlopes.clear();
	_varReverseUpperModeSlopes.clear();
	_stdDevReverseUpperModeSlopes.clear();
	_medianReverseUpperModeSlopes.clear();
	_numReverseUpperModeSlopeSamples.clear();

	/////////////////////////////////////////////

	_reverseLowerModalDistortionAngles.clear();
	_reverseUpperModalDistortionAngles.clear();

	/////////////////////////////////////////////

	_reverseLowerModeBDCAreas.clear();
	_maxReverseLowerModeBDCAreas.clear();
	_minReverseLowerModeBDCAreas.clear();
	_meanReverseLowerModeBDCAreas.clear();
	_sumReverseLowerModeBDCAreas.clear();
	_varReverseLowerModeBDCAreas.clear();
	_stdDevReverseLowerModeBDCAreas.clear();
	_medianReverseLowerModeBDCAreas.clear();
	_numReverseLowerModeBDCAreaSamples.clear();

	_reverseUpperModeBDCAreas.clear();
	_maxReverseUpperModeBDCAreas.clear();
	_minReverseUpperModeBDCAreas.clear();
	_meanReverseUpperModeBDCAreas.clear();
	_sumReverseUpperModeBDCAreas.clear();
	_varReverseUpperModeBDCAreas.clear();
	_stdDevReverseUpperModeBDCAreas.clear();
	_medianReverseUpperModeBDCAreas.clear();
	_numReverseUpperModeBDCAreaSamples.clear();

	///////////////////////////////////////

	_reverseLowerModeBDCAreaXLengths.clear();
	_maxReverseLowerModeBDCAreaXLengths.clear();
	_minReverseLowerModeBDCAreaXLengths.clear();
	_meanReverseLowerModeBDCAreaXLengths.clear();
	_sumReverseLowerModeBDCAreaXLengths.clear();
	_varReverseLowerModeBDCAreaXLengths.clear();
	_stdDevReverseLowerModeBDCAreaXLengths.clear();
	_medianReverseLowerModeBDCAreaXLengths.clear();
	_numReverseLowerModeBDCAreaXLengthSamples.clear();

	_reverseUpperModeBDCAreaXLengths.clear();
	_maxReverseUpperModeBDCAreaXLengths.clear();
	_minReverseUpperModeBDCAreaXLengths.clear();
	_meanReverseUpperModeBDCAreaXLengths.clear();
	_sumReverseUpperModeBDCAreaXLengths.clear();
	_varReverseUpperModeBDCAreaXLengths.clear();
	_stdDevReverseUpperModeBDCAreaXLengths.clear();
	_medianReverseUpperModeBDCAreaXLengths.clear();
	_numReverseUpperModeBDCAreaXLengthSamples.clear();

	///////////////////////////////////////

	_stableAreaWidths.clear();
	_maxStableAreaWidths.clear();
	_minStableAreaWidths.clear();
	_meanStableAreaWidths.clear();
	_sumStableAreaWidths.clear();
	_varStableAreaWidths.clear();
	_stdDevStableAreaWidths.clear();
	_medianStableAreaWidths.clear();
	_numStableAreaWidthSamples.clear();

	///////////////////////////////////////

	_percWorkingRegions.clear();
	_maxPercWorkingRegions.clear();
	_minPercWorkingRegions.clear();
	_meanPercWorkingRegions.clear();
	_sumPercWorkingRegions.clear();
	_varPercWorkingRegions.clear();
	_stdDevPercWorkingRegions.clear();
	_medianPercWorkingRegions.clear();
	_numPercWorkingRegionSamples.clear();

	///////////////////////////////////////

	_middleLineRMSValues.clear();
	_middleLineSlopes.clear();

	///////////////////////////////////////

	_continuityValues.clear();
	_maxContinuityValue		= 0;
	_minContinuityValue		= 0;
	_meanContinuityValues	= 0;
	_sumContinuityValues	= 0;
	_varContinuityValues	= 0;
	_stdDevContinuityValues	= 0;
	_medianContinuityValues	= 0;
	_numContinuityValues	= 0;
	_maxPrGap	= 0;

	///////////////////////////////////////

	_overallPercStableArea = 0;

	///////////////////////////////////////

	_qaAnalysisComplete = false;

	//
	//////////////////////////////////////////////////////

	//////////////////////////////////////////////////////
	///
	//	Pass Fail Analysis
	//
	_passFailAnalysisComplete = false;

	_maxUpperModeBordersRemovedPFAnalysis.init();
	_maxLowerModeBordersRemovedPFAnalysis.init();
	_maxMiddleLineRemovedPFAnalysis.init();

	_maxForwardModeWidthPFAnalysis.init();
	_minForwardModeWidthPFAnalysis.init();

	_maxReverseModeWidthPFAnalysis.init();
	_minReverseModeWidthPFAnalysis.init();

	_meanPercWorkingRegionPFAnalysis.init();
	_minPercWorkingRegionPFAnalysis.init();

	_maxForwardLowerModeBDCAreaPFAnalysis.init();
	_maxForwardLowerModeBDCAreaXLengthPFAnalysis.init();
	_sumForwardLowerModeBDCAreasPFAnalysis.init();
	_numForwardLowerModeBDCAreasPFAnalysis.init();
	_maxForwardUpperModeBDCAreaPFAnalysis.init();
	_maxForwardUpperModeBDCAreaXLengthPFAnalysis.init();
	_sumForwardUpperModeBDCAreasPFAnalysis.init();
	_numForwardUpperModeBDCAreasPFAnalysis.init();

	_maxReverseLowerModeBDCAreaPFAnalysis.init();
	_maxReverseLowerModeBDCAreaXLengthPFAnalysis.init();
	_sumReverseLowerModeBDCAreasPFAnalysis.init();
	_numReverseLowerModeBDCAreasPFAnalysis.init();
	_maxReverseUpperModeBDCAreaPFAnalysis.init();
	_maxReverseUpperModeBDCAreaXLengthPFAnalysis.init();
	_sumReverseUpperModeBDCAreasPFAnalysis.init();
	_numReverseUpperModeBDCAreasPFAnalysis.init();

	_forwardLowerModalDistortionAnglePFAnalysis.init();
	_forwardLowerModalDistortionAngleXPositionPFAnalysis.init();
	_forwardUpperModalDistortionAnglePFAnalysis.init();
	_forwardUpperModalDistortionAngleXPositionPFAnalysis.init();

	_reverseLowerModalDistortionAnglePFAnalysis.init();
	_reverseLowerModalDistortionAngleXPositionPFAnalysis.init();
	_reverseUpperModalDistortionAnglePFAnalysis.init();
	_reverseUpperModalDistortionAngleXPositionPFAnalysis.init();

	_middleLineRMSValuesPFAnalysis.init();
	_maxMiddleLineSlopePFAnalysis.init();
	_minMiddleLineSlopePFAnalysis.init();
	_maxPrGapPFAnalysis.init();

	_maxForwardLowerModeSlopePFAnalysis.init();
	_maxForwardUpperModeSlopePFAnalysis.init();
	_maxReverseLowerModeSlopePFAnalysis.init();
	_maxReverseUpperModeSlopePFAnalysis.init();

	//
	//////////////////////////////////////////////////////

	_screeningPassed = false;

	_qaDetectedError = RESULTS_NO_ERROR;


}

CString
SuperModeMapAnalysis::
createQaResultsAbsFilePath(CString	resultsDir,
						CString		laserId,
						CString		dateTimeStamp)
{
	CString absFilePath = "";
	
	CString superModeNumString = "";
	superModeNumString.Format("%d", _superModeNumber);

	absFilePath += resultsDir;
	absFilePath += "\\";
	absFilePath += QA_METRICS_FILE_NAME;
	absFilePath += USCORE;
	absFilePath += LASER_TYPE_ID_DSDBR01_CSTRING;
	absFilePath += USCORE;
	absFilePath += laserId;
	absFilePath += USCORE;
	absFilePath += dateTimeStamp;
	absFilePath += USCORE;
	absFilePath += "SM";
	absFilePath += superModeNumString;
	absFilePath += CSV;

	return absFilePath;
}
CString
SuperModeMapAnalysis::
createPassFailResultsAbsFilePath(CString	resultsDir,
								CString		laserId,
								CString		dateTimeStamp)
{
	CString absFilePath = "";
	CString superModeNumString = "";
	superModeNumString.Format("%d", _superModeNumber);

	absFilePath += resultsDir;
	absFilePath += "\\";
	absFilePath += PASSFAIL_FILE_NAME;
	absFilePath += USCORE;
	absFilePath += LASER_TYPE_ID_DSDBR01_CSTRING;
	absFilePath += USCORE;
	absFilePath += laserId;
	absFilePath += USCORE;
	absFilePath += dateTimeStamp;
	absFilePath += USCORE;
	absFilePath += "SM";
	absFilePath += superModeNumString;
	absFilePath += CSV;

	return absFilePath;
}
/////////////////////////////////////////////////////////////////////

SuperModeMapAnalysis::rcode
SuperModeMapAnalysis::
runAnalysis(short			superModeNumber,
			const			vector<ModeBoundaryLine>& upperLines,
			const			vector<ModeBoundaryLine>& lowerLines,
			const			vector<ModeBoundaryLine>& middleLines,
			short			numUpperLinesRemoved,
			short			numLowerLinesRemoved,
			short			numMiddleLinesRemoved,
			int				xAxisLength,
			int				yAxisLength,
			vector<double>	continuityValues,
			CString			resultsDir,
			CString			laserId,
			CString			dateTimeStamp)
{
	_superModeNumber = superModeNumber;
	//mod GDM 23/10/06 _xAxisLength needs initiating before createModeAnalysisVectors else
	//they get created with random(?) huge 4-6million plus value for _xAxisLength, which
	//in turn screws up ModeAnalysis
	_xAxisLength = xAxisLength;
	_yAxisLength = yAxisLength;

	if( upperLines.size() != lowerLines.size()
	 || upperLines.size() == 0 )
	{
		return rcode::qaerror;
	}

	createModeAnalysisVectors(upperLines, lowerLines);
	createHysteresisAnalysisVector(_forwardModes, _reverseModes);


	_numForwardModes = (int)_forwardModes.size();
	_numReverseModes = (int)_reverseModes.size();

	_middleLines = middleLines;
	_numMiddleLines = (int)_middleLines.size();

	_qaResultsAbsFilePath =	createQaResultsAbsFilePath(resultsDir,
													laserId,
													dateTimeStamp);

	_passFailResultsAbsFilePath = createPassFailResultsAbsFilePath(resultsDir,
															laserId,
															dateTimeStamp);
	
	_resultsDir		= resultsDir;
	_laserId		= laserId;
	_dateTimeStamp	= dateTimeStamp;

	_xAxisLength = xAxisLength;
	_yAxisLength = yAxisLength;

	_continuityValues = continuityValues;

	_numUpperLinesRemoved	= numUpperLinesRemoved;
	_numLowerLinesRemoved	= numLowerLinesRemoved;
	_numMiddleLinesRemoved	= numMiddleLinesRemoved;

	rcode err = rcode::ok;

	err = runQaAnalysis();
	if(err == rcode::ok)
		err = runPassFailAnalysis();

	err = writeQaResultsToFile();

	CLoseGridFile passFailResults(_passFailResultsAbsFilePath);
	passFailResults.createFolderAndFile();

	err = writePassFailResultsToFile( passFailResults );

	return err;
}

SuperModeMapAnalysis::rcode
SuperModeMapAnalysis::
runQaAnalysis()
{
	rcode err = rcode::ok;

	if(err == rcode::ok)
		err = runForwardModesQaAnalysis();
	if(err == rcode::ok)
		err = runReverseModesQaAnalysis();
	if(err == rcode::ok)
		err = runHysteresisQaAnalysis();
	if(err == rcode::ok)
		err = runMiddleLinesQaAnalysis();

	////////////////////////////////////////////////////////////////////////////////////

	CString error_message = "";
    int error_number = RESULTS_NO_ERROR;
    if(( _numReverseModes < MIN_NUMBER_OF_MODES )||( _numForwardModes < MIN_NUMBER_OF_MODES ))
        _qaDetectedError |= RESULTS_GENERAL_ERROR | RESULTS_INVALID_NUMBER_OF_LINES;
    if( !(_numUpperLinesRemoved == 0) )
        _qaDetectedError |= RESULTS_GENERAL_ERROR | RESULTS_FORWARD_LINES_MISSING;
    if( !(_numLowerLinesRemoved == 0) )
        _qaDetectedError |= RESULTS_GENERAL_ERROR | RESULTS_REVERSE_LINES_MISSING;


	getQAError(_qaDetectedError, error_number, error_message);

    if( _qaDetectedError > RESULTS_NO_ERROR ) 
		_screeningPassed = false;

	_qaAnalysisComplete = true;

	return err;
}

SuperModeMapAnalysis::rcode
SuperModeMapAnalysis::
runForwardModesQaAnalysis()
{
	rcode err = rcode::ok;

	for(int i=0; i<_numForwardModes; i++)
		_forwardModes[i].runAnalysis();

	getForwardModeWidths();
	getMaxForwardModeWidths();
	getMinForwardModeWidths();
	getMeanForwardModeWidths();
	getSumForwardModeWidths();
	getVarForwardModeWidths();
	getStdDevForwardModeWidths();
	getMedianForwardModeWidths();
	getNumForwardModeWidthSamples();

	getForwardModeSlopes();
	getMaxForwardModeSlopes();
	getMinForwardModeSlopes();
	getMeanForwardModeSlopes();
	getSumForwardModeSlopes();
	getVarForwardModeSlopes();
	getStdDevForwardModeSlopes();
	getMedianForwardModeSlopes();
	getNumForwardModeSlopeSamples();

	getForwardModalDistortionAngles();
	getForwardModalDistortionAngleXPositions();

	getForwardModeBDCAreas();
	getMaxForwardModeBDCAreas();
	getMinForwardModeBDCAreas();
	getMeanForwardModeBDCAreas();
	getSumForwardModeBDCAreas();
	getVarForwardModeBDCAreas();
	getStdDevForwardModeBDCAreas();
	getMedianForwardModeBDCAreas();
	getNumForwardModeBDCAreaSamples();


	getMaxForwardModeBDCAreaXLengths();

	//////////////////////////////////

	return err;
}

SuperModeMapAnalysis::rcode
SuperModeMapAnalysis::
runReverseModesQaAnalysis()
{
	rcode err = rcode::ok;

	for(int i=0; i<_numReverseModes; i++)
		_reverseModes[i].runAnalysis();

	getReverseModeWidths();
	getMaxReverseModeWidths();
	getMinReverseModeWidths();
	getMeanReverseModeWidths();
	getSumReverseModeWidths();
	getVarReverseModeWidths();
	getStdDevReverseModeWidths();
	getMedianReverseModeWidths();
	getNumReverseModeWidthSamples();

	getReverseModeSlopes();
	getMaxReverseModeSlopes();
	getMinReverseModeSlopes();
	getMeanReverseModeSlopes();
	getSumReverseModeSlopes();
	getVarReverseModeSlopes();
	getStdDevReverseModeSlopes();
	getMedianReverseModeSlopes();
	getNumReverseModeSlopeSamples();

	getReverseModeSlopes();
	getMaxReverseModeSlopes();
	getMinReverseModeSlopes();
	getMeanReverseModeSlopes();
	getSumReverseModeSlopes();
	getVarReverseModeSlopes();
	getStdDevReverseModeSlopes();
	getMedianReverseModeSlopes();
	getNumReverseModeSlopeSamples();

	getReverseModalDistortionAngles();
	getReverseModalDistortionAngleXPositions();

	getReverseModeBDCAreas();
	getMaxReverseModeBDCAreas();
	getMinReverseModeBDCAreas();
	getMeanReverseModeBDCAreas();
	getSumReverseModeBDCAreas();
	getVarReverseModeBDCAreas();
	getStdDevReverseModeBDCAreas();
	getMedianReverseModeBDCAreas();
	getNumReverseModeBDCAreaSamples();


	getMaxReverseModeBDCAreaXLengths();

	//////////////////////////////////

	return err;
}

SuperModeMapAnalysis::rcode
SuperModeMapAnalysis::
runHysteresisQaAnalysis()
{
	rcode err = rcode::ok;

	for(int i=0; i<(int)_hysteresisAnalysis.size(); i++)
	{
		_hysteresisAnalysis[i].setQAThresholds(&_qaThresholds);	
		_hysteresisAnalysis[i].runAnalysis();
	}

	getStableAreaWidths();
	getMaxStableAreaWidths();
	getMinStableAreaWidths();
	getMeanStableAreaWidths();
	getSumStableAreaWidths();
	getVarStableAreaWidths();
	getStdDevStableAreaWidths();
	getMedianStableAreaWidths();
	getNumStableAreaWidthSamples();

	getPercWorkingRegions();
	getMaxPercWorkingRegions();
	getMinPercWorkingRegions();
	getMeanPercWorkingRegions();
	getSumPercWorkingRegions();
	getVarPercWorkingRegions();
	getStdDevPercWorkingRegions();
	getMedianPercWorkingRegions();
	getNumPercWorkingRegionSamples();


	/////////////////////////////////////////////////////////////////////
	///
	// get overallPercStableArea
	//
	double totalSumStableAreaWidths = 0;
	for(int i=0; i<(int)_sumStableAreaWidths.size(); i++)
	{
		totalSumStableAreaWidths = totalSumStableAreaWidths + _sumStableAreaWidths[i];
	}

	_overallPercStableArea = (totalSumStableAreaWidths / (_xAxisLength*_yAxisLength)) * 100;

	//
	/////////////////////////////////////////////////////////////////////


	return err;
}

SuperModeMapAnalysis::rcode
SuperModeMapAnalysis::
runMiddleLinesQaAnalysis()
{
	rcode err = rcode::ok;

	getMiddleLineRMSValues();

	/////////////////////////////////

	getMaxContinuityValue();
	getMinContinuityValue();
	getMeanContinuityValues();
	getSumContinuityValues();
	getVarContinuityValues();
	getStdDevContinuityValues();
	getMedianContinuityValues();
	getNumContinuityValues();
	getMaxContinuityValueSpacing();

	/////////////////////////////////////

	return err;
}

SuperModeMapAnalysis::rcode
SuperModeMapAnalysis::
runPassFailAnalysis()
{
	rcode err = rcode::ok;

	if(_qaAnalysisComplete)
	{
		_screeningPassed = true;

		//////////////////////////////////////////////////////
		///
		// get the registry values based on the supermode number
		//
		_lmPassFailThresholds = getPassFailThresholds();

		/////////////////////////////////////////////////////////////////////////////
		///
		// Set thresholds for all modes
		_maxUpperModeBordersRemovedPFAnalysis.threshold			= _lmPassFailThresholds._maxModeBordersRemoved;
		_maxLowerModeBordersRemovedPFAnalysis.threshold			= _lmPassFailThresholds._maxModeBordersRemoved;
		_maxMiddleLineRemovedPFAnalysis.threshold				= _lmPassFailThresholds._maxModeBordersRemoved;

		_maxForwardModeWidthPFAnalysis.threshold				= _lmPassFailThresholds._maxModeWidth;
		_maxReverseModeWidthPFAnalysis.threshold				= _lmPassFailThresholds._maxModeWidth;

		_minForwardModeWidthPFAnalysis.threshold				= _lmPassFailThresholds._minModeWidth;
		_minReverseModeWidthPFAnalysis.threshold				= _lmPassFailThresholds._minModeWidth;

		_meanPercWorkingRegionPFAnalysis.threshold				= _lmPassFailThresholds._meanPercWorkingRegion;
		_minPercWorkingRegionPFAnalysis.threshold				= _lmPassFailThresholds._minPercWorkingRegion;

		_maxForwardLowerModeBDCAreaPFAnalysis.threshold			= _lmPassFailThresholds._maxModeBDCArea;
		_maxForwardLowerModeBDCAreaXLengthPFAnalysis.threshold	= _lmPassFailThresholds._maxModeBDCAreaXLength;
		_sumForwardLowerModeBDCAreasPFAnalysis.threshold		= _lmPassFailThresholds._sumModeBDCAreas;
		_numForwardLowerModeBDCAreasPFAnalysis.threshold		= _lmPassFailThresholds._numModeBDCAreas;
		_maxForwardUpperModeBDCAreaPFAnalysis.threshold			= _lmPassFailThresholds._maxModeBDCArea;
		_maxForwardUpperModeBDCAreaXLengthPFAnalysis.threshold	= _lmPassFailThresholds._maxModeBDCAreaXLength;
		_sumForwardUpperModeBDCAreasPFAnalysis.threshold		= _lmPassFailThresholds._sumModeBDCAreas;
		_numForwardUpperModeBDCAreasPFAnalysis.threshold		= _lmPassFailThresholds._numModeBDCAreas;

		_maxReverseLowerModeBDCAreaPFAnalysis.threshold			= _lmPassFailThresholds._maxModeBDCArea;
		_maxReverseLowerModeBDCAreaXLengthPFAnalysis.threshold	= _lmPassFailThresholds._maxModeBDCAreaXLength;
		_sumReverseLowerModeBDCAreasPFAnalysis.threshold		= _lmPassFailThresholds._sumModeBDCAreas;
		_numReverseLowerModeBDCAreasPFAnalysis.threshold		= _lmPassFailThresholds._numModeBDCAreas;
		_maxReverseUpperModeBDCAreaPFAnalysis.threshold			= _lmPassFailThresholds._maxModeBDCArea;
		_maxReverseUpperModeBDCAreaXLengthPFAnalysis.threshold	= _lmPassFailThresholds._maxModeBDCAreaXLength;
		_sumReverseUpperModeBDCAreasPFAnalysis.threshold		= _lmPassFailThresholds._sumModeBDCAreas;
		_numReverseUpperModeBDCAreasPFAnalysis.threshold		= _lmPassFailThresholds._numModeBDCAreas;

		_forwardLowerModalDistortionAnglePFAnalysis.threshold			= _lmPassFailThresholds._modalDistortionAngle;
		_forwardLowerModalDistortionAngleXPositionPFAnalysis.threshold	= _lmPassFailThresholds._modalDistortionAngleXPosition;
		_forwardUpperModalDistortionAnglePFAnalysis.threshold			= _lmPassFailThresholds._modalDistortionAngle;
		_forwardUpperModalDistortionAngleXPositionPFAnalysis.threshold	= _lmPassFailThresholds._modalDistortionAngleXPosition;

		_reverseLowerModalDistortionAnglePFAnalysis.threshold			= _lmPassFailThresholds._modalDistortionAngle;
		_reverseLowerModalDistortionAngleXPositionPFAnalysis.threshold	= _lmPassFailThresholds._modalDistortionAngleXPosition;
		_reverseUpperModalDistortionAnglePFAnalysis.threshold			= _lmPassFailThresholds._modalDistortionAngle;
		_reverseUpperModalDistortionAngleXPositionPFAnalysis.threshold	= _lmPassFailThresholds._modalDistortionAngleXPosition;

		_middleLineRMSValuesPFAnalysis.threshold				= _lmPassFailThresholds._middleLineRMSValue;
		_maxMiddleLineSlopePFAnalysis.threshold					= _lmPassFailThresholds._maxMiddleLineSlope;
		_minMiddleLineSlopePFAnalysis.threshold					= _lmPassFailThresholds._minMiddleLineSlope;

		_maxPrGapPFAnalysis.threshold							= _lmPassFailThresholds._maxPrGap;

		_maxForwardLowerModeSlopePFAnalysis.threshold			= _lmPassFailThresholds._maxModeLineSlope;
		_maxForwardUpperModeSlopePFAnalysis.threshold			= _lmPassFailThresholds._maxModeLineSlope;
		_maxReverseLowerModeSlopePFAnalysis.threshold			= _lmPassFailThresholds._maxModeLineSlope;
		_maxReverseUpperModeSlopePFAnalysis.threshold			= _lmPassFailThresholds._maxModeLineSlope;



		// find out which lines crossed front section pairs
		// and ignore failures at these locations

		std::vector<bool> forward_mode_has_front_section_change;
		std::vector<bool> upper_line_of_forward_mode_has_front_section_change;
		std::vector<bool> lower_line_of_forward_mode_has_front_section_change;

		for( int i = 0 ; i < (int)_forwardModes.size() ; i++ )
		{
			// find out which modes have lines crossing front sections
			bool upper_has_change = _forwardModes[i]._upperBoundaryLine._crosses_front_sections;
			upper_line_of_forward_mode_has_front_section_change.push_back( upper_has_change );

			bool lower_has_change = _forwardModes[i]._lowerBoundaryLine._crosses_front_sections;
			lower_line_of_forward_mode_has_front_section_change.push_back( lower_has_change );

			if( upper_has_change || lower_has_change )
				forward_mode_has_front_section_change.push_back( true );
			else
				forward_mode_has_front_section_change.push_back( false );
		}

		std::vector<bool> reverse_mode_has_front_section_change;
		std::vector<bool> upper_line_of_reverse_mode_has_front_section_change;
		std::vector<bool> lower_line_of_reverse_mode_has_front_section_change;

		for( int i = 0 ; i < (int)_reverseModes.size() ; i++ )
		{
			// find out which modes have lines crossing front sections
			bool upper_has_change = _reverseModes[i]._upperBoundaryLine._crosses_front_sections;
			upper_line_of_reverse_mode_has_front_section_change.push_back( upper_has_change );

			bool lower_has_change = _reverseModes[i]._lowerBoundaryLine._crosses_front_sections;
			lower_line_of_reverse_mode_has_front_section_change.push_back( lower_has_change );

			if( upper_has_change || lower_has_change )
				reverse_mode_has_front_section_change.push_back( true );
			else
				reverse_mode_has_front_section_change.push_back( false );
		}




		// run pass fail
		if(_numLowerLinesRemoved > _maxLowerModeBordersRemovedPFAnalysis.threshold)
			_maxLowerModeBordersRemovedPFAnalysis.pass = false;
		else
			_maxLowerModeBordersRemovedPFAnalysis.pass = true;
		if(_numUpperLinesRemoved > _maxUpperModeBordersRemovedPFAnalysis.threshold)
			_maxUpperModeBordersRemovedPFAnalysis.pass = false;
		else
			_maxUpperModeBordersRemovedPFAnalysis.pass = true;
		if(_numMiddleLinesRemoved > _maxMiddleLineRemovedPFAnalysis.threshold)
			_maxMiddleLineRemovedPFAnalysis.pass = false;
		else
			_maxMiddleLineRemovedPFAnalysis.pass = true;

		
		_maxForwardModeWidthPFAnalysis.runPassFailWithUpperLimit(_maxForwardModeWidths);
		_maxReverseModeWidthPFAnalysis.runPassFailWithUpperLimit(_maxReverseModeWidths);
		_minForwardModeWidthPFAnalysis.runPassFailWithLowerLimit(_minForwardModeWidths);
		_minReverseModeWidthPFAnalysis.runPassFailWithLowerLimit(_minReverseModeWidths);

		if( CG_REG->get_DSDBR01_SMQA_ignore_DBC_at_FSC() )
		{
			_maxForwardLowerModeBDCAreaPFAnalysis.runPassFailWithUpperLimit(_maxForwardLowerModeBDCAreas,lower_line_of_forward_mode_has_front_section_change);
			_maxForwardLowerModeBDCAreaXLengthPFAnalysis.runPassFailWithUpperLimit(_maxForwardLowerModeBDCAreaXLengths,lower_line_of_forward_mode_has_front_section_change);
			_maxReverseLowerModeBDCAreaPFAnalysis.runPassFailWithUpperLimit(_maxReverseLowerModeBDCAreas,lower_line_of_reverse_mode_has_front_section_change);
			_maxReverseLowerModeBDCAreaXLengthPFAnalysis.runPassFailWithUpperLimit(_maxReverseLowerModeBDCAreaXLengths,lower_line_of_reverse_mode_has_front_section_change);
			_maxForwardUpperModeBDCAreaPFAnalysis.runPassFailWithUpperLimit(_maxForwardUpperModeBDCAreas,upper_line_of_forward_mode_has_front_section_change);
			_maxForwardUpperModeBDCAreaXLengthPFAnalysis.runPassFailWithUpperLimit(_maxForwardUpperModeBDCAreaXLengths,upper_line_of_forward_mode_has_front_section_change);
			_maxReverseUpperModeBDCAreaPFAnalysis.runPassFailWithUpperLimit(_maxReverseUpperModeBDCAreas,upper_line_of_reverse_mode_has_front_section_change);
			_maxReverseUpperModeBDCAreaXLengthPFAnalysis.runPassFailWithUpperLimit(_maxReverseUpperModeBDCAreaXLengths,upper_line_of_reverse_mode_has_front_section_change);

			_sumForwardLowerModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_sumForwardLowerModeBDCAreas,lower_line_of_forward_mode_has_front_section_change);
			_numForwardLowerModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_numForwardLowerModeBDCAreaSamples,lower_line_of_forward_mode_has_front_section_change);
			_sumReverseLowerModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_sumReverseLowerModeBDCAreas,lower_line_of_reverse_mode_has_front_section_change);
			_numReverseLowerModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_numReverseLowerModeBDCAreaSamples,lower_line_of_reverse_mode_has_front_section_change);
			_sumForwardUpperModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_sumForwardUpperModeBDCAreas,upper_line_of_forward_mode_has_front_section_change);
			_numForwardUpperModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_numForwardUpperModeBDCAreaSamples,upper_line_of_forward_mode_has_front_section_change);
			_sumReverseUpperModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_sumReverseUpperModeBDCAreas,upper_line_of_reverse_mode_has_front_section_change);
			_numReverseUpperModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_numReverseUpperModeBDCAreaSamples,upper_line_of_reverse_mode_has_front_section_change);
		}
		else
		{
			_maxForwardLowerModeBDCAreaPFAnalysis.runPassFailWithUpperLimit(_maxForwardLowerModeBDCAreas);
			_maxForwardLowerModeBDCAreaXLengthPFAnalysis.runPassFailWithUpperLimit(_maxForwardLowerModeBDCAreaXLengths);
			_maxReverseLowerModeBDCAreaPFAnalysis.runPassFailWithUpperLimit(_maxReverseLowerModeBDCAreas);
			_maxReverseLowerModeBDCAreaXLengthPFAnalysis.runPassFailWithUpperLimit(_maxReverseLowerModeBDCAreaXLengths);
			_maxForwardUpperModeBDCAreaPFAnalysis.runPassFailWithUpperLimit(_maxForwardUpperModeBDCAreas);
			_maxForwardUpperModeBDCAreaXLengthPFAnalysis.runPassFailWithUpperLimit(_maxForwardUpperModeBDCAreaXLengths);
			_maxReverseUpperModeBDCAreaPFAnalysis.runPassFailWithUpperLimit(_maxReverseUpperModeBDCAreas);
			_maxReverseUpperModeBDCAreaXLengthPFAnalysis.runPassFailWithUpperLimit(_maxReverseUpperModeBDCAreaXLengths);

			_sumForwardLowerModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_sumForwardLowerModeBDCAreas);
			_numForwardLowerModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_numForwardLowerModeBDCAreaSamples);
			_sumReverseLowerModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_sumReverseLowerModeBDCAreas);
			_numReverseLowerModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_numReverseLowerModeBDCAreaSamples);
			_sumForwardUpperModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_sumForwardUpperModeBDCAreas);
			_numForwardUpperModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_numForwardUpperModeBDCAreaSamples);
			_sumReverseUpperModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_sumReverseUpperModeBDCAreas);
			_numReverseUpperModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_numReverseUpperModeBDCAreaSamples);
		}

		_forwardLowerModalDistortionAnglePFAnalysis.runPassFailWithUpperLimit(_forwardLowerModalDistortionAngles);
		_forwardLowerModalDistortionAngleXPositionPFAnalysis.runPassFailWithUpperLimit(_forwardLowerModalDistortionAngleXPositions);
		_forwardUpperModalDistortionAnglePFAnalysis.runPassFailWithUpperLimit(_forwardUpperModalDistortionAngles);
		_forwardUpperModalDistortionAngleXPositionPFAnalysis.runPassFailWithUpperLimit(_forwardUpperModalDistortionAngleXPositions);

		_reverseLowerModalDistortionAnglePFAnalysis.runPassFailWithUpperLimit(_reverseLowerModalDistortionAngles);
		_reverseLowerModalDistortionAngleXPositionPFAnalysis.runPassFailWithUpperLimit(_reverseLowerModalDistortionAngleXPositions);
		_reverseUpperModalDistortionAnglePFAnalysis.runPassFailWithUpperLimit(_reverseUpperModalDistortionAngles);
		_reverseUpperModalDistortionAngleXPositionPFAnalysis.runPassFailWithUpperLimit(_reverseUpperModalDistortionAngleXPositions);


		_meanPercWorkingRegionPFAnalysis.runPassFailWithLowerLimit(_meanPercWorkingRegions);
		_minPercWorkingRegionPFAnalysis.runPassFailWithLowerLimit(_minPercWorkingRegions);

		_middleLineRMSValuesPFAnalysis.runPassFailWithUpperLimit(_middleLineRMSValues);
		_maxMiddleLineSlopePFAnalysis.runPassFailWithUpperLimit(_middleLineSlopes);
		_minMiddleLineSlopePFAnalysis.runPassFailWithLowerLimit(_middleLineSlopes);

		_maxPrGapPFAnalysis.runPassFailWithUpperLimit(*_continuityValues.getValues());

		_maxForwardLowerModeSlopePFAnalysis.runPassFailWithUpperLimit(_maxForwardLowerModeSlopes);
		_maxForwardUpperModeSlopePFAnalysis.runPassFailWithUpperLimit(_maxForwardUpperModeSlopes);
		_maxReverseLowerModeSlopePFAnalysis.runPassFailWithUpperLimit(_maxReverseLowerModeSlopes);
		_maxReverseUpperModeSlopePFAnalysis.runPassFailWithUpperLimit(_maxReverseUpperModeSlopes);

		if(	!(_maxLowerModeBordersRemovedPFAnalysis.pass)||
			!(_maxUpperModeBordersRemovedPFAnalysis.pass)||
			!(_maxMiddleLineRemovedPFAnalysis.pass)||
			!(_maxForwardModeWidthPFAnalysis.pass)||
			!(_maxReverseModeWidthPFAnalysis.pass)||
			!(_minForwardModeWidthPFAnalysis.pass)||
			!(_minReverseModeWidthPFAnalysis.pass)||
			!(_maxForwardLowerModeBDCAreaPFAnalysis.pass)||
			!(_maxForwardLowerModeBDCAreaXLengthPFAnalysis.pass)||
			!(_maxForwardUpperModeBDCAreaPFAnalysis.pass)||
			!(_maxForwardUpperModeBDCAreaXLengthPFAnalysis.pass)||
			!(_maxReverseLowerModeBDCAreaPFAnalysis.pass)||
			!(_maxReverseLowerModeBDCAreaXLengthPFAnalysis.pass)||
			!(_maxReverseUpperModeBDCAreaPFAnalysis.pass)||
			!(_maxReverseUpperModeBDCAreaXLengthPFAnalysis.pass)||
			!(_sumForwardLowerModeBDCAreasPFAnalysis.pass)||
			!(_numForwardLowerModeBDCAreasPFAnalysis.pass)||
			!(_sumForwardUpperModeBDCAreasPFAnalysis.pass)||
			!(_numForwardUpperModeBDCAreasPFAnalysis.pass)||
			!(_sumReverseLowerModeBDCAreasPFAnalysis.pass)||
			!(_numReverseLowerModeBDCAreasPFAnalysis.pass)||
			!(_sumReverseUpperModeBDCAreasPFAnalysis.pass)||
			!(_numReverseUpperModeBDCAreasPFAnalysis.pass)||
			!(_forwardLowerModalDistortionAnglePFAnalysis.pass)||
			!(_forwardLowerModalDistortionAngleXPositionPFAnalysis.pass)||
			!(_forwardUpperModalDistortionAnglePFAnalysis.pass)||
			!(_forwardUpperModalDistortionAngleXPositionPFAnalysis.pass)||
			!(_reverseLowerModalDistortionAnglePFAnalysis.pass)||
			!(_reverseLowerModalDistortionAngleXPositionPFAnalysis.pass)||
			!(_reverseUpperModalDistortionAnglePFAnalysis.pass)||
			!(_reverseUpperModalDistortionAngleXPositionPFAnalysis.pass)||
			!(_middleLineRMSValuesPFAnalysis.pass)||
			!(_meanPercWorkingRegionPFAnalysis.pass)||
			!(_minPercWorkingRegionPFAnalysis.pass)||
			!(_maxMiddleLineSlopePFAnalysis.pass)||
			!(_minMiddleLineSlopePFAnalysis.pass)||
			!(_maxForwardLowerModeSlopePFAnalysis.pass)||
			!(_maxForwardUpperModeSlopePFAnalysis.pass)||
			!(_maxReverseLowerModeSlopePFAnalysis.pass)||
			!(_maxReverseUpperModeSlopePFAnalysis.pass)||
			!(_maxPrGapPFAnalysis.pass))
		{
			_screeningPassed = false;
		}

	}

	//////////////////////////////////////////////////////////////////////////////////
	///
	// analyse results
	//
    if( !(_numUpperLinesRemoved == 0) )
        _qaDetectedError |= RESULTS_GENERAL_ERROR | RESULTS_FORWARD_LINES_MISSING;
    if( !(_numLowerLinesRemoved == 0) )
        _qaDetectedError |= RESULTS_GENERAL_ERROR | RESULTS_REVERSE_LINES_MISSING;
    if( _numReverseModes < MIN_NUMBER_OF_MODES )
        _qaDetectedError |= RESULTS_GENERAL_ERROR | RESULTS_INVALID_NUMBER_OF_LINES;
    if( _numForwardModes < MIN_NUMBER_OF_MODES )
        _qaDetectedError |= RESULTS_GENERAL_ERROR | RESULTS_INVALID_NUMBER_OF_LINES;

    if( _qaDetectedError > RESULTS_NO_ERROR ) 
		_screeningPassed = false;
	//
	////////////////////////////////////////////////////////////////////////////////////

	_passFailAnalysisComplete = true;

	return err;
}

void
SuperModeMapAnalysis::
getQAError(int		qaError,
		  int&		errorNumber,
		  CString&	errorMsg)
{
	errorMsg	= "";
	errorNumber = 0;

    if( qaError & RESULTS_GENERAL_ERROR )
    {
        errorMsg.Append("Error(s) detected. ");
        errorNumber |= RESULTS_GENERAL_ERROR;
    }
    if( qaError & RESULTS_NO_MODEMAP_DATA )
    {
        errorMsg.Append("Mode map data not available. ");
        errorNumber |= RESULTS_NO_MODEMAP_DATA;
    }
    if( qaError & RESULTS_PO_TOO_LOW )
    {
        errorMsg.Append("Invalid laser screening run - optical power is too low. ");
        errorNumber |= RESULTS_PO_TOO_LOW;
    }
    if( qaError & RESULTS_PO_TOO_HIGH )
    {
        errorMsg.Append("Invalid laser screening run - optical power is too high. ");
        errorNumber |= RESULTS_PO_TOO_HIGH;
    }
    if( qaError & RESULTS_TOO_MANY_JUMPS )
    {
        errorMsg.Append("Invalid laser screening run - too many laser mode jumps found. ");
        errorNumber |= RESULTS_TOO_MANY_JUMPS;
    }
    if( qaError & RESULTS_TOO_FEW_JUMPS )
    {
        errorMsg.Append("Invalid laser screening run - too few laser mode jumps found. ");
        errorNumber |= RESULTS_TOO_FEW_JUMPS;
    }
    if( qaError & RESULTS_INVALID_NUMBER_OF_LINES )
    {
        errorMsg.Append("Invalid laser screening run - invalid number of mode boundaries detected. ");
        errorNumber |= RESULTS_INVALID_NUMBER_OF_LINES;
    }
    if( qaError & RESULTS_INVALID_LINES_REMOVED )
    {
        errorMsg.Append("Invalid mode boundaries detected. ");
        errorNumber |= RESULTS_INVALID_LINES_REMOVED;
    }
    if( qaError & RESULTS_FORWARD_LINES_MISSING )
    {
        errorMsg.Append("Undetected forward mode boundaries. ");
        errorNumber |= RESULTS_FORWARD_LINES_MISSING;
    }
    if( qaError & RESULTS_REVERSE_LINES_MISSING )
    {
        errorMsg.Append("Undetected reverse mode boundaries. ");
        errorNumber |= RESULTS_REVERSE_LINES_MISSING;
    }

	CString errNumMsg = "";
	if( errorNumber > RESULTS_NO_ERROR )
		errNumMsg.AppendFormat("%d,", errorNumber);
	else
		errNumMsg.Append(",");
	errNumMsg.Append(errorMsg);


	errorMsg = errNumMsg;
}
//
////////////////////////////////////////////////////////////////////////////////////////
///
//	Data Gathering Section - gather analysis data of the supermodes
//

//
/////////////////////////////////////////////////////////////////////////////
///
//	Forward Mode Widths
void
SuperModeMapAnalysis::
getForwardModeWidths()
{
	if(_forwardModeWidths.size() == 0)
	{
		for(int i=0; i<_numForwardModes; i++)
		{
			Vector forwardModeWidths;
			_forwardModes[i].getModeWidths(forwardModeWidths);

			_forwardModeWidths.push_back(forwardModeWidths);
		}
	}
}

void
SuperModeMapAnalysis::
getMaxForwardModeWidths()
{
	if(_maxForwardModeWidths.size() == 0)
	{
		for(int i=0; i<_numForwardModes; i++)
			_maxForwardModeWidths.push_back(_forwardModes[i].getModeWidthsStatistic(Vector::max));
	}
}
void
SuperModeMapAnalysis::
getMinForwardModeWidths()
{
	if(_minForwardModeWidths.size() == 0)
	{
		for(int i=0; i<_numForwardModes; i++)
			_minForwardModeWidths.push_back(_forwardModes[i].getModeWidthsStatistic(Vector::min));
	}
}
void
SuperModeMapAnalysis::
getMeanForwardModeWidths()
{
	if(_meanForwardModeWidths.size() == 0)
	{
		for(int i=0; i<_numForwardModes; i++)
			_meanForwardModeWidths.push_back(_forwardModes[i].getModeWidthsStatistic(Vector::mean));
	}
}
void
SuperModeMapAnalysis::
getSumForwardModeWidths()
{
	if(_sumForwardModeWidths.size() == 0)
	{
		for(int i=0; i<_numForwardModes; i++)
			_sumForwardModeWidths.push_back(_forwardModes[i].getModeWidthsStatistic(Vector::sum));
	}
}
void
SuperModeMapAnalysis::
getVarForwardModeWidths()
{
	if(_varForwardModeWidths.size() == 0)
	{
		for(int i=0; i<_numForwardModes; i++)
			_varForwardModeWidths.push_back(_forwardModes[i].getModeWidthsStatistic(Vector::variance));
	}
}
void
SuperModeMapAnalysis::
getStdDevForwardModeWidths()
{
	if(_stdDevForwardModeWidths.size() == 0)
	{
		for(int i=0; i<_numForwardModes; i++)
			_stdDevForwardModeWidths.push_back(_forwardModes[i].getModeWidthsStatistic(Vector::stdDev));
	}
}
void
SuperModeMapAnalysis::
getMedianForwardModeWidths()
{
	if(_medianForwardModeWidths.size() == 0)
	{
		for(int i=0; i<_numForwardModes; i++)
			_medianForwardModeWidths.push_back(_forwardModes[i].getModeWidthsStatistic(Vector::median));
	}
}
void
SuperModeMapAnalysis::
getNumForwardModeWidthSamples()
{
	if(_numForwardModeWidthSamples.size() == 0)
	{
		for(int i=0; i<_numForwardModes; i++)
			_numForwardModeWidthSamples.push_back(_forwardModes[i].getModeWidthsStatistic(Vector::count));
	}
}

//
/////////////////////////////////////////////////////////////////////////////
///
//	Forward Mode Slopes
//
void
SuperModeMapAnalysis::
getForwardModeSlopes()
{
	if(_forwardLowerModeSlopes.size() == 0)
	{
		for(int i=0; i<_numForwardModes; i++)
		{
			Vector forwardLowerModeSlopes;
			Vector forwardUpperModeSlopes;
			_forwardModes[i].getModeSlopes(forwardLowerModeSlopes, forwardUpperModeSlopes);

			_forwardLowerModeSlopes.push_back(forwardLowerModeSlopes);
			_forwardUpperModeSlopes.push_back(forwardUpperModeSlopes);
		}
	}
}

void
SuperModeMapAnalysis::
getMaxForwardModeSlopes()
{
	if(_maxForwardLowerModeSlopes.size() == 0)
	{
		for(int i=0; i<_numForwardModes; i++)
		{
			_maxForwardLowerModeSlopes.push_back(_forwardModes[i].getModeLowerSlopesStatistic(Vector::max));
			_maxForwardUpperModeSlopes.push_back(_forwardModes[i].getModeUpperSlopesStatistic(Vector::max));
		}
	}
}
void
SuperModeMapAnalysis::
getMinForwardModeSlopes()
{
	if(_minForwardLowerModeSlopes.size() == 0)
	{
		for(int i=0; i<_numForwardModes; i++)
		{
			_minForwardLowerModeSlopes.push_back(_forwardModes[i].getModeLowerSlopesStatistic(Vector::min));
			_minForwardUpperModeSlopes.push_back(_forwardModes[i].getModeUpperSlopesStatistic(Vector::min));
		}
	}
}
void
SuperModeMapAnalysis::
getMeanForwardModeSlopes()
{
	if(_meanForwardLowerModeSlopes.size() == 0)
	{
		for(int i=0; i<_numForwardModes; i++)
		{
			_meanForwardLowerModeSlopes.push_back(_forwardModes[i].getModeLowerSlopesStatistic(Vector::mean));
			_meanForwardUpperModeSlopes.push_back(_forwardModes[i].getModeUpperSlopesStatistic(Vector::mean));
		}
	}
}
void
SuperModeMapAnalysis::
getSumForwardModeSlopes()
{
	if(_sumForwardLowerModeSlopes.size() == 0)
	{
		for(int i=0; i<_numForwardModes; i++)
		{
			_sumForwardLowerModeSlopes.push_back(_forwardModes[i].getModeLowerSlopesStatistic(Vector::sum));
			_sumForwardUpperModeSlopes.push_back(_forwardModes[i].getModeUpperSlopesStatistic(Vector::sum));
		}
	}
}
void
SuperModeMapAnalysis::
getVarForwardModeSlopes()
{
	if(_varForwardLowerModeSlopes.size() == 0)
	{
		for(int i=0; i<_numForwardModes; i++)
		{
			_varForwardLowerModeSlopes.push_back(_forwardModes[i].getModeLowerSlopesStatistic(Vector::variance));
			_varForwardUpperModeSlopes.push_back(_forwardModes[i].getModeUpperSlopesStatistic(Vector::variance));
		}
	}
}
void
SuperModeMapAnalysis::
getStdDevForwardModeSlopes()
{
	if(_stdDevForwardLowerModeSlopes.size() == 0)
	{
		for(int i=0; i<_numForwardModes; i++)
		{
			_stdDevForwardLowerModeSlopes.push_back(_forwardModes[i].getModeLowerSlopesStatistic(Vector::stdDev));
			_stdDevForwardUpperModeSlopes.push_back(_forwardModes[i].getModeUpperSlopesStatistic(Vector::stdDev));
		}
	}
}
void
SuperModeMapAnalysis::
getMedianForwardModeSlopes()
{
	if(_medianForwardLowerModeSlopes.size() == 0)
	{
		for(int i=0; i<_numForwardModes; i++)
		{
			_medianForwardLowerModeSlopes.push_back(_forwardModes[i].getModeLowerSlopesStatistic(Vector::median));
			_medianForwardUpperModeSlopes.push_back(_forwardModes[i].getModeUpperSlopesStatistic(Vector::median));
		}
	}
}
void
SuperModeMapAnalysis::
getNumForwardModeSlopeSamples()
{
	if(_numForwardLowerModeSlopeSamples.size() == 0)
	{
		for(int i=0; i<_numForwardModes; i++)
		{
			_numForwardLowerModeSlopeSamples.push_back(_forwardModes[i].getModeLowerSlopesStatistic(Vector::count));
			_numForwardUpperModeSlopeSamples.push_back(_forwardModes[i].getModeUpperSlopesStatistic(Vector::count));
		}
	}
}

//
/////////////////////////////////////////////////////////////////////////////
///
//	Stable Area Widths
//
void
SuperModeMapAnalysis::
getStableAreaWidths()
{
	if(_stableAreaWidths.size() == 0)
	{
		for(int i=0; i<(int)_hysteresisAnalysis.size(); i++)
		{
			Vector stableAreaWidths;
			_hysteresisAnalysis[i].getStableAreaWidths(stableAreaWidths);

			_stableAreaWidths.push_back(stableAreaWidths);
		}
	}
}

void
SuperModeMapAnalysis::
getMaxStableAreaWidths()
{
	if(_maxStableAreaWidths.size() == 0)
	{
		for(int i=0; i<(int)_hysteresisAnalysis.size(); i++)
			_maxStableAreaWidths.push_back(_hysteresisAnalysis[i].getStableAreaWidthsStatistic(Vector::max));
	}
}

void
SuperModeMapAnalysis::
getMinStableAreaWidths()
{
	if(_minStableAreaWidths.size() == 0)
	{
		for(int i=0; i<(int)_hysteresisAnalysis.size(); i++)
			_minStableAreaWidths.push_back(_hysteresisAnalysis[i].getStableAreaWidthsStatistic(Vector::min));
	}
}

void
SuperModeMapAnalysis::
getMeanStableAreaWidths()
{
	if(_meanStableAreaWidths.size() == 0)
	{
		for(int i=0; i<(int)_hysteresisAnalysis.size(); i++)
			_meanStableAreaWidths.push_back(_hysteresisAnalysis[i].getStableAreaWidthsStatistic(Vector::mean));
	}
}

void
SuperModeMapAnalysis::
getSumStableAreaWidths()
{
	if(_sumStableAreaWidths.size() == 0)
	{
		for(int i=0; i<(int)_hysteresisAnalysis.size(); i++)
			_sumStableAreaWidths.push_back(_hysteresisAnalysis[i].getStableAreaWidthsStatistic(Vector::sum));
	}
}

void
SuperModeMapAnalysis::
getVarStableAreaWidths()
{
	if(_varStableAreaWidths.size() == 0)
	{
		for(int i=0; i<(int)_hysteresisAnalysis.size(); i++)
			_varStableAreaWidths.push_back(_hysteresisAnalysis[i].getStableAreaWidthsStatistic(Vector::variance));
	}
}

void
SuperModeMapAnalysis::
getStdDevStableAreaWidths()
{
	if(_stdDevStableAreaWidths.size() == 0)
	{
		for(int i=0; i<(int)_hysteresisAnalysis.size(); i++)
			_stdDevStableAreaWidths.push_back(_hysteresisAnalysis[i].getStableAreaWidthsStatistic(Vector::stdDev));
	}
}

void
SuperModeMapAnalysis::
getMedianStableAreaWidths()
{
	if(_medianStableAreaWidths.size() == 0)
	{
		for(int i=0; i<(int)_hysteresisAnalysis.size(); i++)
			_medianStableAreaWidths.push_back(_hysteresisAnalysis[i].getStableAreaWidthsStatistic(Vector::median));
	}
}

void
SuperModeMapAnalysis::
getNumStableAreaWidthSamples()
{
	if(_numStableAreaWidthSamples.size() == 0)
	{
		for(int i=0; i<(int)_hysteresisAnalysis.size(); i++)
			_numStableAreaWidthSamples.push_back(_hysteresisAnalysis[i].getStableAreaWidthsStatistic(Vector::count));
	}
}

//
/////////////////////////////////////////////////////////////////////////////
///
//	Percentage working Region
//
void
SuperModeMapAnalysis::
getPercWorkingRegions()
{
	if(_percWorkingRegions.size() == 0)
	{
		for(int i=0; i<(int)_hysteresisAnalysis.size(); i++)
		{
			Vector percWorkingRegions;
			_hysteresisAnalysis[i].getPercWorkingRegions(percWorkingRegions);

			_percWorkingRegions.push_back(percWorkingRegions);
		}
	}
}

void
SuperModeMapAnalysis::
getMaxPercWorkingRegions()
{
	if(_maxPercWorkingRegions.size() == 0)
	{
		for(int i=0; i<(int)_hysteresisAnalysis.size(); i++)
			_maxPercWorkingRegions.push_back(_hysteresisAnalysis[i].getPercWorkingRegionsStatistic(Vector::max));
	}
}

void
SuperModeMapAnalysis::
getMinPercWorkingRegions()
{
	if(_minPercWorkingRegions.size() == 0)
	{
		for(int i=0; i<(int)_hysteresisAnalysis.size(); i++)
			_minPercWorkingRegions.push_back(_hysteresisAnalysis[i].getPercWorkingRegionsStatistic(Vector::min));
	}
}

void
SuperModeMapAnalysis::
getMeanPercWorkingRegions()
{
	if(_meanPercWorkingRegions.size() == 0)
	{
		for(int i=0; i<(int)_hysteresisAnalysis.size(); i++)
			_meanPercWorkingRegions.push_back(_hysteresisAnalysis[i].getPercWorkingRegionsStatistic(Vector::mean));
	}
}

void
SuperModeMapAnalysis::
getSumPercWorkingRegions()
{
	if(_sumPercWorkingRegions.size() == 0)
	{
		for(int i=0; i<(int)_hysteresisAnalysis.size(); i++)
			_sumPercWorkingRegions.push_back(_hysteresisAnalysis[i].getPercWorkingRegionsStatistic(Vector::sum));
	}
}

void
SuperModeMapAnalysis::
getVarPercWorkingRegions()
{
	if(_varPercWorkingRegions.size() == 0)
	{
		for(int i=0; i<(int)_hysteresisAnalysis.size(); i++)
			_varPercWorkingRegions.push_back(_hysteresisAnalysis[i].getPercWorkingRegionsStatistic(Vector::variance));
	}
}

void
SuperModeMapAnalysis::
getStdDevPercWorkingRegions()
{
	if(_stdDevPercWorkingRegions.size() == 0)
	{
		for(int i=0; i<(int)_hysteresisAnalysis.size(); i++)
			_stdDevPercWorkingRegions.push_back(_hysteresisAnalysis[i].getPercWorkingRegionsStatistic(Vector::stdDev));
	}
}

void
SuperModeMapAnalysis::
getMedianPercWorkingRegions()
{
	if(_medianPercWorkingRegions.size() == 0)
	{
		for(int i=0; i<(int)_hysteresisAnalysis.size(); i++)
			_medianPercWorkingRegions.push_back(_hysteresisAnalysis[i].getPercWorkingRegionsStatistic(Vector::median));
	}
}

void
SuperModeMapAnalysis::
getNumPercWorkingRegionSamples()
{
	if(_numPercWorkingRegionSamples.size() == 0)
	{
		for(int i=0; i<(int)_hysteresisAnalysis.size(); i++)
			_numPercWorkingRegionSamples.push_back(_hysteresisAnalysis[i].getPercWorkingRegionsStatistic(Vector::count));
	}
}

////////////////////////////////////////////////////////////////////////////////
///
//	Modal Distortion
//
void
SuperModeMapAnalysis::
getForwardModalDistortionAngles()
{
	if(_forwardLowerModalDistortionAngles.size() == 0)
	{
		for(int i=0; i<_numForwardModes; i++)
		{
			_forwardLowerModalDistortionAngles.push_back(_forwardModes[i].getLowerModeModalDistortionAngle());
			_forwardUpperModalDistortionAngles.push_back(_forwardModes[i].getUpperModeModalDistortionAngle());
		}
	}
}
void
SuperModeMapAnalysis::
getForwardModalDistortionAngleXPositions()
{
	if(_forwardLowerModalDistortionAngleXPositions.size() == 0)
	{
		for(int i=0; i<_numForwardModes; i++)
		{
			_forwardLowerModalDistortionAngleXPositions.push_back(_forwardModes[i].getLowerModeMaxModalDistortionAngleXPosition());
			_forwardUpperModalDistortionAngleXPositions.push_back(_forwardModes[i].getUpperModeMaxModalDistortionAngleXPosition());
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
///
//	Mode Boundary Direction Change Area measurements
//
void
SuperModeMapAnalysis::
getForwardModeBDCAreas()
{
	if(_forwardLowerModeBDCAreas.size() == 0)
	{
		for(int i=0; i<_numForwardModes; i++)
		{
			Vector forwardLowerModeBDCAreas;
			Vector forwardUpperModeBDCAreas;
			_forwardModes[i].getModeBDCAreas(forwardLowerModeBDCAreas, forwardUpperModeBDCAreas);

			_forwardLowerModeBDCAreas.push_back(forwardLowerModeBDCAreas);
			_forwardUpperModeBDCAreas.push_back(forwardUpperModeBDCAreas);
		}
	}
}

void
SuperModeMapAnalysis::
getMaxForwardModeBDCAreas()
{
	if(_maxForwardLowerModeBDCAreas.size() == 0)
	{
		for(int i=0; i<_numForwardModes; i++)
		{
			_maxForwardLowerModeBDCAreas.push_back(_forwardModes[i].getLowerModeBDCAreasStatistic(Vector::max));
			_maxForwardUpperModeBDCAreas.push_back(_forwardModes[i].getUpperModeBDCAreasStatistic(Vector::max));
		}
	}
}
void
SuperModeMapAnalysis::
getMinForwardModeBDCAreas()
{
	if(_minForwardLowerModeBDCAreas.size() == 0)
	{
		for(int i=0; i<_numForwardModes; i++)
		{
			_minForwardLowerModeBDCAreas.push_back(_forwardModes[i].getLowerModeBDCAreasStatistic(Vector::min));
			_minForwardUpperModeBDCAreas.push_back(_forwardModes[i].getUpperModeBDCAreasStatistic(Vector::min));
		}
	}
}
void
SuperModeMapAnalysis::
getMeanForwardModeBDCAreas()
{
	if(_meanForwardLowerModeBDCAreas.size() == 0)
	{
		for(int i=0; i<_numForwardModes; i++)
		{
			_meanForwardLowerModeBDCAreas.push_back(_forwardModes[i].getLowerModeBDCAreasStatistic(Vector::mean));
			_meanForwardUpperModeBDCAreas.push_back(_forwardModes[i].getUpperModeBDCAreasStatistic(Vector::mean));
		}
	}
}
void
SuperModeMapAnalysis::
getSumForwardModeBDCAreas()
{
	if(_sumForwardLowerModeBDCAreas.size() == 0)
	{
		for(int i=0; i<_numForwardModes; i++)
		{
			_sumForwardLowerModeBDCAreas.push_back(_forwardModes[i].getLowerModeBDCAreasStatistic(Vector::sum));
			_sumForwardUpperModeBDCAreas.push_back(_forwardModes[i].getUpperModeBDCAreasStatistic(Vector::sum));
		}
	}
}
void
SuperModeMapAnalysis::
getVarForwardModeBDCAreas()
{
	if(_varForwardLowerModeBDCAreas.size() == 0)
	{
		for(int i=0; i<_numForwardModes; i++)
		{
			_varForwardLowerModeBDCAreas.push_back(_forwardModes[i].getLowerModeBDCAreasStatistic(Vector::variance));
			_varForwardUpperModeBDCAreas.push_back(_forwardModes[i].getUpperModeBDCAreasStatistic(Vector::variance));
		}
	}
}
void
SuperModeMapAnalysis::
getStdDevForwardModeBDCAreas()
{
	if(_stdDevForwardLowerModeBDCAreas.size() == 0)
	{
		for(int i=0; i<_numForwardModes; i++)
		{
			_stdDevForwardLowerModeBDCAreas.push_back(_forwardModes[i].getLowerModeBDCAreasStatistic(Vector::stdDev));
			_stdDevForwardUpperModeBDCAreas.push_back(_forwardModes[i].getUpperModeBDCAreasStatistic(Vector::stdDev));
		}
	}
}
void
SuperModeMapAnalysis::
getMedianForwardModeBDCAreas()
{
	if(_medianForwardLowerModeBDCAreas.size() == 0)
	{
		for(int i=0; i<_numForwardModes; i++)
		{
			_medianForwardLowerModeBDCAreas.push_back(_forwardModes[i].getLowerModeBDCAreasStatistic(Vector::median));
			_medianForwardUpperModeBDCAreas.push_back(_forwardModes[i].getUpperModeBDCAreasStatistic(Vector::median));
		}
	}
}
void
SuperModeMapAnalysis::
getNumForwardModeBDCAreaSamples()
{
	if(_numForwardLowerModeBDCAreaSamples.size() == 0)
	{
		for(int i=0; i<_numForwardModes; i++)
		{
			_numForwardLowerModeBDCAreaSamples.push_back(_forwardModes[i].getLowerModeBDCAreasStatistic(Vector::count));
			_numForwardUpperModeBDCAreaSamples.push_back(_forwardModes[i].getUpperModeBDCAreasStatistic(Vector::count));
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
///
//	Mode Boundary Direction Change Area X Length measurements
//
void
SuperModeMapAnalysis::
getForwardModeBDCAreaXLengths()
{
	if(_forwardLowerModeBDCAreaXLengths.size() == 0)
	{
		for(int i=0; i<_numForwardModes; i++)
		{
			Vector forwardLowerModeBDCAreaXLengths;
			Vector forwardUpperModeBDCAreaXLengths;
			_forwardModes[i].getModeBDCAreaXLengths(forwardLowerModeBDCAreaXLengths, forwardUpperModeBDCAreaXLengths);

			_forwardLowerModeBDCAreaXLengths.push_back(forwardLowerModeBDCAreaXLengths);
			_forwardUpperModeBDCAreaXLengths.push_back(forwardUpperModeBDCAreaXLengths);
		}
	}
}

void
SuperModeMapAnalysis::
getMaxForwardModeBDCAreaXLengths()
{
	if(_maxForwardLowerModeBDCAreaXLengths.size() == 0)
	{
		for(int i=0; i<_numForwardModes; i++)
		{
			_maxForwardLowerModeBDCAreaXLengths.push_back(_forwardModes[i].getLowerModeBDCAreaXLengthsStatistic(Vector::max));
			_maxForwardUpperModeBDCAreaXLengths.push_back(_forwardModes[i].getUpperModeBDCAreaXLengthsStatistic(Vector::max));
		}
	}
}
void
SuperModeMapAnalysis::
getMinForwardModeBDCAreaXLengths()
{
	if(_minForwardLowerModeBDCAreaXLengths.size() == 0)
	{
		for(int i=0; i<_numForwardModes; i++)
		{
			_minForwardLowerModeBDCAreaXLengths.push_back(_forwardModes[i].getLowerModeBDCAreaXLengthsStatistic(Vector::min));
			_minForwardUpperModeBDCAreaXLengths.push_back(_forwardModes[i].getUpperModeBDCAreaXLengthsStatistic(Vector::min));
		}
	}
}
void
SuperModeMapAnalysis::
getMeanForwardModeBDCAreaXLengths()
{
	if(_meanForwardLowerModeBDCAreaXLengths.size() == 0)
	{
		for(int i=0; i<_numForwardModes; i++)
		{
			_meanForwardLowerModeBDCAreaXLengths.push_back(_forwardModes[i].getLowerModeBDCAreaXLengthsStatistic(Vector::mean));
			_meanForwardUpperModeBDCAreaXLengths.push_back(_forwardModes[i].getUpperModeBDCAreaXLengthsStatistic(Vector::mean));
		}
	}
}
void
SuperModeMapAnalysis::
getSumForwardModeBDCAreaXLengths()
{
	if(_sumForwardLowerModeBDCAreaXLengths.size() == 0)
	{
		for(int i=0; i<_numForwardModes; i++)
		{
			_sumForwardLowerModeBDCAreaXLengths.push_back(_forwardModes[i].getLowerModeBDCAreaXLengthsStatistic(Vector::sum));
			_sumForwardUpperModeBDCAreaXLengths.push_back(_forwardModes[i].getUpperModeBDCAreaXLengthsStatistic(Vector::sum));
		}
	}
}
void
SuperModeMapAnalysis::
getVarForwardModeBDCAreaXLengths()
{
	if(_varForwardLowerModeBDCAreaXLengths.size() == 0)
	{
		for(int i=0; i<_numForwardModes; i++)
		{
			_varForwardLowerModeBDCAreaXLengths.push_back(_forwardModes[i].getLowerModeBDCAreaXLengthsStatistic(Vector::variance));
			_varForwardUpperModeBDCAreaXLengths.push_back(_forwardModes[i].getUpperModeBDCAreaXLengthsStatistic(Vector::variance));
		}
	}
}
void
SuperModeMapAnalysis::
getStdDevForwardModeBDCAreaXLengths()
{
	if(_stdDevForwardLowerModeBDCAreaXLengths.size() == 0)
	{
		for(int i=0; i<_numForwardModes; i++)
		{
			_stdDevForwardLowerModeBDCAreaXLengths.push_back(_forwardModes[i].getLowerModeBDCAreaXLengthsStatistic(Vector::stdDev));
			_stdDevForwardUpperModeBDCAreaXLengths.push_back(_forwardModes[i].getUpperModeBDCAreaXLengthsStatistic(Vector::stdDev));
		}
	}
}
void
SuperModeMapAnalysis::
getMedianForwardModeBDCAreaXLengths()
{
	if(_medianForwardLowerModeBDCAreaXLengths.size() == 0)
	{
		for(int i=0; i<_numForwardModes; i++)
		{
			_medianForwardLowerModeBDCAreaXLengths.push_back(_forwardModes[i].getLowerModeBDCAreaXLengthsStatistic(Vector::median));
			_medianForwardUpperModeBDCAreaXLengths.push_back(_forwardModes[i].getUpperModeBDCAreaXLengthsStatistic(Vector::median));
		}
	}
}
void
SuperModeMapAnalysis::
getNumForwardModeBDCAreaXLengthSamples()
{
	if(_numForwardLowerModeBDCAreaXLengthSamples.size() == 0)
	{
		for(int i=0; i<_numForwardModes; i++)
		{
			_numForwardLowerModeBDCAreaXLengthSamples.push_back(_forwardModes[i].getLowerModeBDCAreaXLengthsStatistic(Vector::count));
			_numForwardUpperModeBDCAreaXLengthSamples.push_back(_forwardModes[i].getUpperModeBDCAreaXLengthsStatistic(Vector::count));
		}
	}
}

//
/////////////////////////////////////////////////////////////////////////////
///
//	Reverse Mode Widths
void
SuperModeMapAnalysis::
getReverseModeWidths()
{
	if(_reverseModeWidths.size() == 0)
	{
		for(int i=0; i<_numReverseModes; i++)
		{
			Vector reverseModeWidths;
			_reverseModes[i].getModeWidths(reverseModeWidths);

			_reverseModeWidths.push_back(reverseModeWidths);
		}
	}
}

void
SuperModeMapAnalysis::
getMaxReverseModeWidths()
{
	if(_maxReverseModeWidths.size() == 0)
	{
		for(int i=0; i<_numReverseModes; i++)
			_maxReverseModeWidths.push_back(_reverseModes[i].getModeWidthsStatistic(Vector::max));
	}
}
void
SuperModeMapAnalysis::
getMinReverseModeWidths()
{
	if(_minReverseModeWidths.size() == 0)
	{
		for(int i=0; i<_numReverseModes; i++)
			_minReverseModeWidths.push_back(_reverseModes[i].getModeWidthsStatistic(Vector::min));
	}
}
void
SuperModeMapAnalysis::
getMeanReverseModeWidths()
{
	if(_meanReverseModeWidths.size() == 0)
	{
		for(int i=0; i<_numReverseModes; i++)
			_meanReverseModeWidths.push_back(_reverseModes[i].getModeWidthsStatistic(Vector::mean));
	}
}
void
SuperModeMapAnalysis::
getSumReverseModeWidths()
{
	if(_sumReverseModeWidths.size() == 0)
	{
		for(int i=0; i<_numReverseModes; i++)
			_sumReverseModeWidths.push_back(_reverseModes[i].getModeWidthsStatistic(Vector::sum));
	}
}
void
SuperModeMapAnalysis::
getVarReverseModeWidths()
{
	if(_varReverseModeWidths.size() == 0)
	{
		for(int i=0; i<_numReverseModes; i++)
			_varReverseModeWidths.push_back(_reverseModes[i].getModeWidthsStatistic(Vector::variance));
	}
}
void
SuperModeMapAnalysis::
getStdDevReverseModeWidths()
{
	if(_stdDevReverseModeWidths.size() == 0)
	{
		for(int i=0; i<_numReverseModes; i++)
			_stdDevReverseModeWidths.push_back(_reverseModes[i].getModeWidthsStatistic(Vector::stdDev));
	}
}
void
SuperModeMapAnalysis::
getMedianReverseModeWidths()
{
	if(_medianReverseModeWidths.size() == 0)
	{
		for(int i=0; i<_numReverseModes; i++)
			_medianReverseModeWidths.push_back(_reverseModes[i].getModeWidthsStatistic(Vector::median));
	}
}
void
SuperModeMapAnalysis::
getNumReverseModeWidthSamples()
{
	if(_numReverseModeWidthSamples.size() == 0)
	{
		for(int i=0; i<_numReverseModes; i++)
			_numReverseModeWidthSamples.push_back(_reverseModes[i].getModeWidthsStatistic(Vector::count));
	}
}

//
/////////////////////////////////////////////////////////////////////////////
///
//	Reverse Mode Slopes
//
void
SuperModeMapAnalysis::
getReverseModeSlopes()
{
	if(_reverseLowerModeSlopes.size() == 0)
	{
		for(int i=0; i<_numReverseModes; i++)
		{
			Vector reverseLowerModeSlopes;
			Vector reverseUpperModeSlopes;
			_reverseModes[i].getModeSlopes(reverseLowerModeSlopes, reverseUpperModeSlopes);

			_reverseLowerModeSlopes.push_back(reverseLowerModeSlopes);
			_reverseUpperModeSlopes.push_back(reverseUpperModeSlopes);
		}
	}
}

void
SuperModeMapAnalysis::
getMaxReverseModeSlopes()
{
	if(_maxReverseLowerModeSlopes.size() == 0)
	{
		for(int i=0; i<_numReverseModes; i++)
		{
			_maxReverseLowerModeSlopes.push_back(_reverseModes[i].getModeLowerSlopesStatistic(Vector::max));
			_maxReverseUpperModeSlopes.push_back(_reverseModes[i].getModeUpperSlopesStatistic(Vector::max));
		}
	}
}
void
SuperModeMapAnalysis::
getMinReverseModeSlopes()
{
	if(_minReverseLowerModeSlopes.size() == 0)
	{
		for(int i=0; i<_numReverseModes; i++)
		{
			_minReverseLowerModeSlopes.push_back(_reverseModes[i].getModeLowerSlopesStatistic(Vector::min));
			_minReverseUpperModeSlopes.push_back(_reverseModes[i].getModeUpperSlopesStatistic(Vector::min));
		}
	}
}
void
SuperModeMapAnalysis::
getMeanReverseModeSlopes()
{
	if(_meanReverseLowerModeSlopes.size() == 0)
	{
		for(int i=0; i<_numReverseModes; i++)
		{
			_meanReverseLowerModeSlopes.push_back(_reverseModes[i].getModeLowerSlopesStatistic(Vector::mean));
			_meanReverseUpperModeSlopes.push_back(_reverseModes[i].getModeUpperSlopesStatistic(Vector::mean));
		}
	}
}
void
SuperModeMapAnalysis::
getSumReverseModeSlopes()
{
	if(_sumReverseLowerModeSlopes.size() == 0)
	{
		for(int i=0; i<_numReverseModes; i++)
		{
			_sumReverseLowerModeSlopes.push_back(_reverseModes[i].getModeLowerSlopesStatistic(Vector::sum));
			_sumReverseUpperModeSlopes.push_back(_reverseModes[i].getModeUpperSlopesStatistic(Vector::sum));
		}
	}
}
void
SuperModeMapAnalysis::
getVarReverseModeSlopes()
{
	if(_varReverseLowerModeSlopes.size() == 0)
	{
		for(int i=0; i<_numReverseModes; i++)
		{
			_varReverseLowerModeSlopes.push_back(_reverseModes[i].getModeLowerSlopesStatistic(Vector::variance));
			_varReverseUpperModeSlopes.push_back(_reverseModes[i].getModeUpperSlopesStatistic(Vector::variance));
		}
	}
}
void
SuperModeMapAnalysis::
getStdDevReverseModeSlopes()
{
	if(_stdDevReverseLowerModeSlopes.size() == 0)
	{
		for(int i=0; i<_numReverseModes; i++)
		{
			_stdDevReverseLowerModeSlopes.push_back(_reverseModes[i].getModeLowerSlopesStatistic(Vector::stdDev));
			_stdDevReverseUpperModeSlopes.push_back(_reverseModes[i].getModeUpperSlopesStatistic(Vector::stdDev));
		}
	}
}
void
SuperModeMapAnalysis::
getMedianReverseModeSlopes()
{
	if(_medianReverseLowerModeSlopes.size() == 0)
	{
		for(int i=0; i<_numReverseModes; i++)
		{
			_medianReverseLowerModeSlopes.push_back(_reverseModes[i].getModeLowerSlopesStatistic(Vector::median));
			_medianReverseUpperModeSlopes.push_back(_reverseModes[i].getModeUpperSlopesStatistic(Vector::median));
		}
	}
}
void
SuperModeMapAnalysis::
getNumReverseModeSlopeSamples()
{
	if(_numReverseLowerModeSlopeSamples.size() == 0)
	{
		for(int i=0; i<_numReverseModes; i++)
		{
			_numReverseLowerModeSlopeSamples.push_back(_reverseModes[i].getModeLowerSlopesStatistic(Vector::count));
			_numReverseUpperModeSlopeSamples.push_back(_reverseModes[i].getModeUpperSlopesStatistic(Vector::count));
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
///
//	Modal Distortion
//
void
SuperModeMapAnalysis::
getReverseModalDistortionAngles()
{
	if(_reverseLowerModalDistortionAngles.size() == 0)
	{
		for(int i=0; i<_numReverseModes; i++)
		{
			_reverseLowerModalDistortionAngles.push_back(_reverseModes[i].getLowerModeModalDistortionAngle());
			_reverseUpperModalDistortionAngles.push_back(_reverseModes[i].getUpperModeModalDistortionAngle());
		}
	}
}
void
SuperModeMapAnalysis::
getReverseModalDistortionAngleXPositions()
{
	if(_reverseLowerModalDistortionAngleXPositions.size() == 0)
	{
		for(int i=0; i<_numReverseModes; i++)
		{
			_reverseLowerModalDistortionAngleXPositions.push_back(_reverseModes[i].getLowerModeMaxModalDistortionAngleXPosition());
			_reverseUpperModalDistortionAngleXPositions.push_back(_reverseModes[i].getUpperModeMaxModalDistortionAngleXPosition());
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
///
//	Mode Boundary Direction Change Area measurements
//
void
SuperModeMapAnalysis::
getReverseModeBDCAreas()
{
	if(_reverseLowerModeBDCAreas.size() == 0)
	{
		for(int i=0; i<_numReverseModes; i++)
		{
			Vector reverseLowerModeBDCAreas;
			Vector reverseUpperModeBDCAreas;

			_reverseModes[i].getModeBDCAreas(reverseLowerModeBDCAreas, reverseUpperModeBDCAreas);

			_reverseLowerModeBDCAreas.push_back(reverseLowerModeBDCAreas);
			_reverseUpperModeBDCAreas.push_back(reverseUpperModeBDCAreas);
		}
	}
}

void
SuperModeMapAnalysis::
getMaxReverseModeBDCAreas()
{
	if(_maxReverseLowerModeBDCAreas.size() == 0)
	{
		for(int i=0; i<_numReverseModes; i++)
		{
			_maxReverseLowerModeBDCAreas.push_back(_reverseModes[i].getLowerModeBDCAreasStatistic(Vector::max));
			_maxReverseUpperModeBDCAreas.push_back(_reverseModes[i].getUpperModeBDCAreasStatistic(Vector::max));
		}
	}
}
void
SuperModeMapAnalysis::
getMinReverseModeBDCAreas()
{
	if(_minReverseLowerModeBDCAreas.size() == 0)
	{
		for(int i=0; i<_numReverseModes; i++)
		{
			_minReverseLowerModeBDCAreas.push_back(_reverseModes[i].getLowerModeBDCAreasStatistic(Vector::min));
			_minReverseUpperModeBDCAreas.push_back(_reverseModes[i].getUpperModeBDCAreasStatistic(Vector::min));
		}
	}
}
void
SuperModeMapAnalysis::
getMeanReverseModeBDCAreas()
{
	if(_meanReverseLowerModeBDCAreas.size() == 0)
	{
		for(int i=0; i<_numReverseModes; i++)
		{
			_meanReverseLowerModeBDCAreas.push_back(_reverseModes[i].getLowerModeBDCAreasStatistic(Vector::mean));
			_meanReverseUpperModeBDCAreas.push_back(_reverseModes[i].getUpperModeBDCAreasStatistic(Vector::mean));
		}
	}
}
void
SuperModeMapAnalysis::
getSumReverseModeBDCAreas()
{
	if(_sumReverseLowerModeBDCAreas.size() == 0)
	{
		for(int i=0; i<_numReverseModes; i++)
		{
			_sumReverseLowerModeBDCAreas.push_back(_reverseModes[i].getLowerModeBDCAreasStatistic(Vector::sum));
			_sumReverseUpperModeBDCAreas.push_back(_reverseModes[i].getUpperModeBDCAreasStatistic(Vector::sum));
		}
	}
}
void
SuperModeMapAnalysis::
getVarReverseModeBDCAreas()
{
	if(_varReverseLowerModeBDCAreas.size() == 0)
	{
		for(int i=0; i<_numReverseModes; i++)
		{
			_varReverseLowerModeBDCAreas.push_back(_reverseModes[i].getLowerModeBDCAreasStatistic(Vector::variance));
			_varReverseUpperModeBDCAreas.push_back(_reverseModes[i].getUpperModeBDCAreasStatistic(Vector::variance));
		}
	}
}
void
SuperModeMapAnalysis::
getStdDevReverseModeBDCAreas()
{
	if(_stdDevReverseLowerModeBDCAreas.size() == 0)
	{
		for(int i=0; i<_numReverseModes; i++)
		{
			_stdDevReverseLowerModeBDCAreas.push_back(_reverseModes[i].getLowerModeBDCAreasStatistic(Vector::stdDev));
			_stdDevReverseUpperModeBDCAreas.push_back(_reverseModes[i].getUpperModeBDCAreasStatistic(Vector::stdDev));
		}
	}
}
void
SuperModeMapAnalysis::
getMedianReverseModeBDCAreas()
{
	if(_medianReverseLowerModeBDCAreas.size() == 0)
	{
		for(int i=0; i<_numReverseModes; i++)
		{
			_medianReverseLowerModeBDCAreas.push_back(_reverseModes[i].getLowerModeBDCAreasStatistic(Vector::median));
			_medianReverseUpperModeBDCAreas.push_back(_reverseModes[i].getUpperModeBDCAreasStatistic(Vector::median));
		}
	}
}
void
SuperModeMapAnalysis::
getNumReverseModeBDCAreaSamples()
{
	if(_numReverseLowerModeBDCAreaSamples.size() == 0)
	{
		for(int i=0; i<_numReverseModes; i++)
		{
			_numReverseLowerModeBDCAreaSamples.push_back(_reverseModes[i].getLowerModeBDCAreasStatistic(Vector::count));
			_numReverseUpperModeBDCAreaSamples.push_back(_reverseModes[i].getUpperModeBDCAreasStatistic(Vector::count));
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
///
//	Mode Boundary Direction Change Area X Length measurements
//
void
SuperModeMapAnalysis::
getReverseModeBDCAreaXLengths()
{
	if(_reverseLowerModeBDCAreaXLengths.size() == 0)
	{
		for(int i=0; i<_numReverseModes; i++)
		{
			Vector reverseLowerModeBDCAreaXLengths;
			Vector reverseUpperModeBDCAreaXLengths;
			_reverseModes[i].getModeBDCAreaXLengths(reverseLowerModeBDCAreaXLengths, reverseUpperModeBDCAreaXLengths);

			_reverseLowerModeBDCAreaXLengths.push_back(reverseLowerModeBDCAreaXLengths);
			_reverseUpperModeBDCAreaXLengths.push_back(reverseUpperModeBDCAreaXLengths);
		}
	}
}

void
SuperModeMapAnalysis::
getMaxReverseModeBDCAreaXLengths()
{
	if(_maxReverseLowerModeBDCAreaXLengths.size() == 0)
	{
		for(int i=0; i<_numReverseModes; i++)
		{
			_maxReverseLowerModeBDCAreaXLengths.push_back(_reverseModes[i].getLowerModeBDCAreaXLengthsStatistic(Vector::max));
			_maxReverseUpperModeBDCAreaXLengths.push_back(_reverseModes[i].getUpperModeBDCAreaXLengthsStatistic(Vector::max));
		}
	}
}
void
SuperModeMapAnalysis::
getMinReverseModeBDCAreaXLengths()
{
	if(_minReverseLowerModeBDCAreaXLengths.size() == 0)
	{
		for(int i=0; i<_numReverseModes; i++)
		{
			_minReverseLowerModeBDCAreaXLengths.push_back(_reverseModes[i].getLowerModeBDCAreaXLengthsStatistic(Vector::min));
			_minReverseUpperModeBDCAreaXLengths.push_back(_reverseModes[i].getUpperModeBDCAreaXLengthsStatistic(Vector::min));
		}
	}
}
void
SuperModeMapAnalysis::
getMeanReverseModeBDCAreaXLengths()
{
	if(_meanReverseLowerModeBDCAreaXLengths.size() == 0)
	{
		for(int i=0; i<_numReverseModes; i++)
		{
			_meanReverseLowerModeBDCAreaXLengths.push_back(_reverseModes[i].getLowerModeBDCAreaXLengthsStatistic(Vector::mean));
			_meanReverseUpperModeBDCAreaXLengths.push_back(_reverseModes[i].getUpperModeBDCAreaXLengthsStatistic(Vector::mean));
		}
	}
}
void
SuperModeMapAnalysis::
getSumReverseModeBDCAreaXLengths()
{
	if(_sumReverseLowerModeBDCAreaXLengths.size() == 0)
	{
		for(int i=0; i<_numReverseModes; i++)
		{
			_sumReverseLowerModeBDCAreaXLengths.push_back(_reverseModes[i].getLowerModeBDCAreaXLengthsStatistic(Vector::sum));
			_sumReverseUpperModeBDCAreaXLengths.push_back(_reverseModes[i].getUpperModeBDCAreaXLengthsStatistic(Vector::sum));
		}
	}
}
void
SuperModeMapAnalysis::
getVarReverseModeBDCAreaXLengths()
{
	if(_varReverseLowerModeBDCAreaXLengths.size() == 0)
	{
		for(int i=0; i<_numReverseModes; i++)
		{
			_varReverseLowerModeBDCAreaXLengths.push_back(_reverseModes[i].getLowerModeBDCAreaXLengthsStatistic(Vector::variance));
			_varReverseUpperModeBDCAreaXLengths.push_back(_reverseModes[i].getUpperModeBDCAreaXLengthsStatistic(Vector::variance));
		}
	}
}
void
SuperModeMapAnalysis::
getStdDevReverseModeBDCAreaXLengths()
{
	if(_stdDevReverseLowerModeBDCAreaXLengths.size() == 0)
	{
		for(int i=0; i<_numReverseModes; i++)
		{
			_stdDevReverseLowerModeBDCAreaXLengths.push_back(_reverseModes[i].getLowerModeBDCAreaXLengthsStatistic(Vector::stdDev));
			_stdDevReverseUpperModeBDCAreaXLengths.push_back(_reverseModes[i].getUpperModeBDCAreaXLengthsStatistic(Vector::stdDev));
		}
	}
}
void
SuperModeMapAnalysis::
getMedianReverseModeBDCAreaXLengths()
{
	if(_medianReverseLowerModeBDCAreaXLengths.size() == 0)
	{
		for(int i=0; i<_numReverseModes; i++)
		{
			_medianReverseLowerModeBDCAreaXLengths.push_back(_reverseModes[i].getLowerModeBDCAreaXLengthsStatistic(Vector::median));
			_medianReverseUpperModeBDCAreaXLengths.push_back(_reverseModes[i].getUpperModeBDCAreaXLengthsStatistic(Vector::median));
		}
	}
}
void
SuperModeMapAnalysis::
getNumReverseModeBDCAreaXLengthSamples()
{
	if(_numReverseLowerModeBDCAreaXLengthSamples.size() == 0)
	{
		for(int i=0; i<_numReverseModes; i++)
		{
			_numReverseLowerModeBDCAreaXLengthSamples.push_back(_reverseModes[i].getLowerModeBDCAreaXLengthsStatistic(Vector::count));
			_numReverseUpperModeBDCAreaXLengthSamples.push_back(_reverseModes[i].getUpperModeBDCAreaXLengthsStatistic(Vector::count));
		}
	}
}


//
/////////////////////////////////////////////////////////////////////////////
///
//	MiddleLine RootMeanSquare analysis
//
void
SuperModeMapAnalysis::
getMiddleLineRMSValues()
{
	/////////////////////////////////////////////////////////////////////
	/// Use VectorAnalysis to get RMS value of this supermodes middle line
	//
	if(_middleLineRMSValues.size() == 0)
	{
		for(int i=0; i<(int)_middleLines.size(); i++)
		{
			double	linearFitLineSlope = 0;
			double	linearFitLineIntercept = 0;

			// find linear fit line
			VectorAnalysis::linearFit(_middleLines[i].xPoints(), 
									_middleLines[i].yPoints(),
									linearFitLineSlope,
									linearFitLineIntercept);

			// find distances from points to linear fit line
			vector<double> distances;
			VectorAnalysis::distancesFromPointsToLine(_middleLines[i].xPoints(),
													_middleLines[i].yPoints(),
													linearFitLineSlope,
													linearFitLineIntercept,
													distances);

			// get mean of distances
			double meanDistance = 0;
			meanDistance = VectorAnalysis::meanOfValues(distances);

			_middleLineSlopes.push_back(linearFitLineSlope);

			_middleLineRMSValues.push_back(sqrt(pow(meanDistance, 2)));			
		}
	}

}

void
SuperModeMapAnalysis::
getMiddleLineSlopes()
{
	if(_middleLineSlopes.size() == 0)
	{
		for(int i=0; i<(int)_middleLines.size(); i++)
		{			
			double	linearFitLineSlope = 0;
			double	linearFitLineIntercept = 0;

			// find linear fit line
			VectorAnalysis::linearFit(_middleLines[i].xPoints(), 
									_middleLines[i].yPoints(),
									linearFitLineSlope,
									linearFitLineIntercept);

			_middleLineSlopes.push_back(linearFitLineSlope);
		}
	}
}
//
/////////////////////////////////////////////////////////////////////////////
///
//	Continuity analysis
//
void
SuperModeMapAnalysis::
getMaxContinuityValue()
{
	if(_maxContinuityValue == 0)
		_maxContinuityValue = _continuityValues.getVectorAttribute(Vector::max);
}

void
SuperModeMapAnalysis::
getMinContinuityValue()
{
	if(_minContinuityValue == 0)
		_minContinuityValue = _continuityValues.getVectorAttribute(Vector::min);
}

void
SuperModeMapAnalysis::
getMeanContinuityValues()
{
	if(_meanContinuityValues == 0)
		_meanContinuityValues = _continuityValues.getVectorAttribute(Vector::mean);
}

void
SuperModeMapAnalysis::
getSumContinuityValues()
{
	if(_sumContinuityValues == 0)
		_sumContinuityValues = _continuityValues.getVectorAttribute(Vector::sum);
}

void
SuperModeMapAnalysis::
getVarContinuityValues()
{
	if(_varContinuityValues == 0)
		_varContinuityValues = _continuityValues.getVectorAttribute(Vector::variance);
}

void
SuperModeMapAnalysis::
getStdDevContinuityValues()
{
	if(_stdDevContinuityValues == 0)
		_stdDevContinuityValues = _continuityValues.getVectorAttribute(Vector::stdDev);
}

void
SuperModeMapAnalysis::
getMedianContinuityValues()
{
	if(_medianContinuityValues == 0)
		_medianContinuityValues = _continuityValues.getVectorAttribute(Vector::median);
}

void
SuperModeMapAnalysis::
getNumContinuityValues()
{
	if(_numContinuityValues == 0)
		_numContinuityValues = _continuityValues.getVectorAttribute(Vector::count);
}

void
SuperModeMapAnalysis::
getMaxContinuityValueSpacing()
{
	if(_maxPrGap == 0)
	{
		_maxPrGap = _continuityValues.getVectorAttribute(Vector::maxGap);
	}
}
//
///
////////////////////////////////////////////////////////////////////////////////
///
//	File Handling section - writing results to file
//
SuperModeMapAnalysis::rcode
SuperModeMapAnalysis::
writeQaResultsToFile()
{
	rcode err = rcode::ok;

	CLoseGridFile qaResults(_qaResultsAbsFilePath);
	qaResults.createFolderAndFile();

	//////////////////////////////////////////////////////////////////////////

	CString errorMessage = "";
	int errorNum = 0;
	getQAError(_qaDetectedError, errorNum, errorMessage);

    CStringArray error_msg_row;
    error_msg_row.Add(errorMessage);
    qaResults.addRow(error_msg_row);
    qaResults.addEmptyLine();

	//////////////////////////////////////////
	
	CStringArray row;
	CString cellString = "";

	row.RemoveAll();
	row.Add("Slope Window Size");
	cellString.Format("%d", CG_REG->get_DSDBR01_SMQA_slope_window_size());
	row.Add(cellString);
	qaResults.addRow(row);

	row.RemoveAll();
	row.Add("Modal Distortion Min X");
	cellString.Format("%g", CG_REG->get_DSDBR01_SMQA_modal_distortion_min_x());
	row.Add(cellString);
	//GDM 31/10/06 added Mode width and Hysteresis window mins
	row.Add("Mode Width Analysis Min X");
	cellString.Format("%g", CG_REG->get_DSDBR01_SMQA_mode_width_analysis_min_x());
	row.Add(cellString);
	row.Add("Hysteresis Analysis Min X");
	cellString.Format("%g", CG_REG->get_DSDBR01_SMQA_hysteresis_analysis_min_x());
	row.Add(cellString);
	//GDM
	qaResults.addRow(row);

	row.RemoveAll();
	row.Add("Modal Distortion Max X");
	cellString.Format("%g", CG_REG->get_DSDBR01_SMQA_modal_distortion_max_x());
	row.Add(cellString);
	//GDM 31/10/06 added Mode width and Hysteresis window mins
	row.Add("Mode Width Analysis Max X");
	cellString.Format("%g", CG_REG->get_DSDBR01_SMQA_mode_width_analysis_max_x());
	row.Add(cellString);
	row.Add("Hysteresis Analysis Max X");
	cellString.Format("%g", CG_REG->get_DSDBR01_SMQA_hysteresis_analysis_max_x());
	row.Add(cellString);
	//GDM
	qaResults.addRow(row);

	row.RemoveAll();
	row.Add("Modal Distortion Min Y");
	cellString.Format("%g", CG_REG->get_DSDBR01_SMQA_modal_distortion_min_y());
	row.Add(cellString);
	qaResults.addRow(row);

	qaResults.addEmptyLine();

	//////////////////////////////////////////

	const CString	QA_XLS_HEADER1 = "Overall % Stable Area";
	const CString	QA_XLS_HEADER2 = "Index";
	const CString	QA_XLS_HEADER3 = "Function";
	const CString	QA_XLS_HEADER4 = "Slope of forward lower mode boundary line";
	const CString	QA_XLS_HEADER5 = "Slope of forward upper mode boundary line";
	const CString	QA_XLS_HEADER6 = "Slope of reverse lower mode boundary line";
	const CString	QA_XLS_HEADER7 = "Slope of reverse upper mode boundary line";
	const CString	QA_XLS_HEADER8 = "Width for Stable Area";
	const CString	QA_XLS_HEADER9 = "Reverse Mode Width";
	const CString	QA_XLS_HEADER10 = "Forward Mode Width";
	const CString	QA_XLS_HEADER11 = "% Working Region";
	const CString	QA_XLS_HEADER12 = "Forward Lower Max Modal Distortion Angle";
	const CString	QA_XLS_HEADER13 = "Forward Lower Max Modal Distortion X-Position";
	const CString	QA_XLS_HEADER14 = "Forward Upper Max Modal Distortion Angle";
	const CString	QA_XLS_HEADER15 = "Forward Upper Max Modal Distortion X-Position";
	const CString	QA_XLS_HEADER16 = "Reverse Lower Max Modal Distortion Angle";
	const CString	QA_XLS_HEADER17 = "Reverse Lower Max Modal Distortion X-Position";
	const CString	QA_XLS_HEADER18 = "Reverse Upper Max Modal Distortion Angle";
	const CString	QA_XLS_HEADER19 = "Reverse Upper Max Modal Distortion X-Position";
	const CString	QA_XLS_HEADER20 = "Forward Lower Max Boundary Direction Change X-Length";
	const CString	QA_XLS_HEADER21 = "Forward Upper Max Boundary Direction Change X-Length";
	const CString	QA_XLS_HEADER22 = "Forward Lower Max Boundary Direction Change Area";
	const CString	QA_XLS_HEADER23 = "Forward Upper Max Boundary Direction Change Area";
	const CString	QA_XLS_HEADER24 = "Forward Lower Sum of Boundary Direction Change Areas";
	const CString	QA_XLS_HEADER25 = "Forward Upper Sum of Boundary Direction Change Areas";
	const CString	QA_XLS_HEADER26 = "Forward Lower Number of Boundary Direction Changes";
	const CString	QA_XLS_HEADER27 = "Forward Upper Number of Boundary Direction Changes";
	const CString	QA_XLS_HEADER28 = "Reverse Lower Max Boundary Direction Change X-Length";
	const CString	QA_XLS_HEADER29 = "Reverse Upper Max Boundary Direction Change X-Length";
	const CString	QA_XLS_HEADER30 = "Reverse Lower Max Boundary Direction Change Area";
	const CString	QA_XLS_HEADER31 = "Reverse Upper Max Boundary Direction Change Area";
	const CString	QA_XLS_HEADER32 = "Reverse Lower Sum of Boundary Direction Change Areas";
	const CString	QA_XLS_HEADER33 = "Reverse Upper Sum of Boundary Direction Change Areas";
	const CString	QA_XLS_HEADER34 = "Reverse Lower Number of Boundary Direction Changes";
	const CString	QA_XLS_HEADER35 = "Reverse Upper Number of Boundary Direction Changes";
	const CString	QA_XLS_HEADER36 = "Middle Line RMS Value";

	const CString	QA_XLS_MAX		= "Maximum";
	const CString	QA_XLS_MIN		= "Minimum";
	const CString	QA_XLS_MEAN		= "Mean";
	const CString	QA_XLS_SUM		= "Sum";
	const CString	QA_XLS_COUNT	= "Number Of Samples";
	const CString	QA_XLS_STDDEV	= "Standard Deviation";
	const CString	QA_XLS_VAR		= "Variance";
	const CString	QA_XLS_OTHERS	= "";

	CStringArray attribArray;
	attribArray.Add(QA_XLS_MAX);
	attribArray.Add(QA_XLS_MIN);
	attribArray.Add(QA_XLS_MEAN);
	attribArray.Add(QA_XLS_SUM);
	attribArray.Add(QA_XLS_COUNT);
	attribArray.Add(QA_XLS_STDDEV);
	attribArray.Add(QA_XLS_VAR);
	attribArray.Add(QA_XLS_OTHERS);

	CString	QA_XLS_COL1_VALUE = "";
	CString	QA_XLS_COL2_VALUE = "";
	CString	QA_XLS_COL3_VALUE = "";
	CString	QA_XLS_COL4_VALUE = "";
	CString	QA_XLS_COL5_VALUE = "";
	CString	QA_XLS_COL6_VALUE = "";
	CString	QA_XLS_COL7_VALUE = "";
	CString	QA_XLS_COL8_VALUE = "";
	CString	QA_XLS_COL9_VALUE = "";
	CString	QA_XLS_COL10_VALUE = "";
	CString	QA_XLS_COL11_VALUE = "";
	CString	QA_XLS_COL12_VALUE = "";
	CString	QA_XLS_COL13_VALUE = "";
	CString	QA_XLS_COL14_VALUE = "";
	CString	QA_XLS_COL15_VALUE = "";
	CString	QA_XLS_COL16_VALUE = "";
	CString	QA_XLS_COL17_VALUE = "";
	CString	QA_XLS_COL18_VALUE = "";
	CString	QA_XLS_COL19_VALUE = "";
	CString	QA_XLS_COL20_VALUE = "";
	CString	QA_XLS_COL21_VALUE = "";
	CString	QA_XLS_COL22_VALUE = "";
	CString	QA_XLS_COL23_VALUE = "";
	CString	QA_XLS_COL24_VALUE = "";
	CString	QA_XLS_COL25_VALUE = "";
	CString	QA_XLS_COL26_VALUE = "";
	CString	QA_XLS_COL27_VALUE = "";
	CString	QA_XLS_COL28_VALUE = "";
	CString	QA_XLS_COL29_VALUE = "";
	CString	QA_XLS_COL30_VALUE = "";
	CString	QA_XLS_COL31_VALUE = "";
	CString	QA_XLS_COL32_VALUE = "";
	CString	QA_XLS_COL33_VALUE = "";
	CString	QA_XLS_COL34_VALUE = "";
	CString	QA_XLS_COL35_VALUE = "";
	CString	QA_XLS_COL36_VALUE = "";

	CStringArray headerArray;
	headerArray.Add(QA_XLS_HEADER1);
	headerArray.Add(QA_XLS_HEADER2);
	headerArray.Add(QA_XLS_HEADER3);
	headerArray.Add(QA_XLS_HEADER4);
	headerArray.Add(QA_XLS_HEADER5);
	headerArray.Add(QA_XLS_HEADER6);
	headerArray.Add(QA_XLS_HEADER7);
	headerArray.Add(QA_XLS_HEADER8);
	headerArray.Add(QA_XLS_HEADER9);
	headerArray.Add(QA_XLS_HEADER10);
	headerArray.Add(QA_XLS_HEADER11);
	headerArray.Add(QA_XLS_HEADER12);
	headerArray.Add(QA_XLS_HEADER13);
	headerArray.Add(QA_XLS_HEADER14);
	headerArray.Add(QA_XLS_HEADER15);
	headerArray.Add(QA_XLS_HEADER16);
	headerArray.Add(QA_XLS_HEADER17);
	headerArray.Add(QA_XLS_HEADER18);
	headerArray.Add(QA_XLS_HEADER19);
	headerArray.Add(QA_XLS_HEADER20);
	headerArray.Add(QA_XLS_HEADER21);
	headerArray.Add(QA_XLS_HEADER22);
	headerArray.Add(QA_XLS_HEADER23);
	headerArray.Add(QA_XLS_HEADER24);
	headerArray.Add(QA_XLS_HEADER25);
	headerArray.Add(QA_XLS_HEADER26);
	headerArray.Add(QA_XLS_HEADER27);
	headerArray.Add(QA_XLS_HEADER28);
	headerArray.Add(QA_XLS_HEADER29);
	headerArray.Add(QA_XLS_HEADER30);
	headerArray.Add(QA_XLS_HEADER31);
	headerArray.Add(QA_XLS_HEADER32);
	headerArray.Add(QA_XLS_HEADER33);
	headerArray.Add(QA_XLS_HEADER34);
	headerArray.Add(QA_XLS_HEADER35);
	headerArray.Add(QA_XLS_HEADER36);

	qaResults.addRow(headerArray);

	//////////////////////////////////////////////////////////////////////////

	QA_XLS_COL1_VALUE.Format("%g", _overallPercStableArea);

	CStringArray rowArray;
	rowArray.Add(QA_XLS_COL1_VALUE);
	rowArray.Add(QA_XLS_COL2_VALUE);
	rowArray.Add(QA_XLS_COL3_VALUE);
	rowArray.Add(QA_XLS_COL4_VALUE);
	rowArray.Add(QA_XLS_COL5_VALUE);
	rowArray.Add(QA_XLS_COL6_VALUE);
	rowArray.Add(QA_XLS_COL7_VALUE);
	rowArray.Add(QA_XLS_COL8_VALUE);
	rowArray.Add(QA_XLS_COL9_VALUE);
	rowArray.Add(QA_XLS_COL10_VALUE);
	rowArray.Add(QA_XLS_COL11_VALUE);
	rowArray.Add(QA_XLS_COL12_VALUE);
	rowArray.Add(QA_XLS_COL13_VALUE);
	rowArray.Add(QA_XLS_COL14_VALUE);
	rowArray.Add(QA_XLS_COL15_VALUE);
	rowArray.Add(QA_XLS_COL16_VALUE);
	rowArray.Add(QA_XLS_COL17_VALUE);
	rowArray.Add(QA_XLS_COL18_VALUE);
	rowArray.Add(QA_XLS_COL19_VALUE);
	rowArray.Add(QA_XLS_COL20_VALUE);
	rowArray.Add(QA_XLS_COL21_VALUE);
	rowArray.Add(QA_XLS_COL22_VALUE);
	rowArray.Add(QA_XLS_COL23_VALUE);
	rowArray.Add(QA_XLS_COL24_VALUE);
	rowArray.Add(QA_XLS_COL25_VALUE);
	rowArray.Add(QA_XLS_COL26_VALUE);
	rowArray.Add(QA_XLS_COL27_VALUE);
	rowArray.Add(QA_XLS_COL28_VALUE);
	rowArray.Add(QA_XLS_COL29_VALUE);
	rowArray.Add(QA_XLS_COL30_VALUE);
	rowArray.Add(QA_XLS_COL31_VALUE);
	rowArray.Add(QA_XLS_COL32_VALUE);
	rowArray.Add(QA_XLS_COL33_VALUE);
	rowArray.Add(QA_XLS_COL34_VALUE);
	rowArray.Add(QA_XLS_COL35_VALUE);
	rowArray.Add(QA_XLS_COL36_VALUE);
    qaResults.addRow(rowArray);

    QA_XLS_COL1_VALUE = "";
    QA_XLS_COL10_VALUE = "";

	//////////////////////////////////////////////////////////////////////////

    int QA_XLS_NUM_ROWS_PER_SM = (int)attribArray.GetCount();

	int numModes = _numForwardModes;
	if(numModes > _numReverseModes)
		numModes = _numReverseModes;

    for(int i=0;  i<numModes; i++)
    {
	    for(int j = 0; j<QA_XLS_NUM_ROWS_PER_SM; j++)
	    {
			// clear vars
			rowArray.RemoveAll();
			QA_XLS_COL1_VALUE = "";
			QA_XLS_COL2_VALUE = "";
			QA_XLS_COL3_VALUE = "";
			QA_XLS_COL4_VALUE = "";
			QA_XLS_COL5_VALUE = "";
			QA_XLS_COL6_VALUE = "";
			QA_XLS_COL7_VALUE = "";
			QA_XLS_COL8_VALUE = "";
			QA_XLS_COL9_VALUE = "";
			QA_XLS_COL10_VALUE = "";
			QA_XLS_COL11_VALUE = "";
			QA_XLS_COL12_VALUE = "";
			QA_XLS_COL13_VALUE = "";
			QA_XLS_COL14_VALUE = "";
			QA_XLS_COL15_VALUE = "";
			QA_XLS_COL16_VALUE = "";
			QA_XLS_COL17_VALUE = "";
			QA_XLS_COL18_VALUE = "";
			QA_XLS_COL19_VALUE = "";
			QA_XLS_COL20_VALUE = "";
			QA_XLS_COL21_VALUE = "";
			QA_XLS_COL22_VALUE = "";
			QA_XLS_COL23_VALUE = "";
			QA_XLS_COL24_VALUE = "";
			QA_XLS_COL25_VALUE = "";
			QA_XLS_COL26_VALUE = "";
			QA_XLS_COL27_VALUE = "";
			QA_XLS_COL28_VALUE = "";
			QA_XLS_COL29_VALUE = "";
			QA_XLS_COL30_VALUE = "";
			QA_XLS_COL31_VALUE = "";
			QA_XLS_COL32_VALUE = "";
			QA_XLS_COL33_VALUE = "";
			QA_XLS_COL34_VALUE = "";
			QA_XLS_COL35_VALUE = "";
			QA_XLS_COL36_VALUE = "";

			QA_XLS_COL1_VALUE = "";
			QA_XLS_COL2_VALUE.AppendFormat("%d", i);
			QA_XLS_COL3_VALUE = attribArray[j];

			if(QA_XLS_COL3_VALUE == QA_XLS_MAX)
			{
				if(i<(int)_maxForwardLowerModeSlopes.size())
					QA_XLS_COL4_VALUE.AppendFormat("%g", _maxForwardLowerModeSlopes[i]);
				else
					QA_XLS_COL4_VALUE = "";
				if(i<(int)_maxForwardUpperModeSlopes.size())
					QA_XLS_COL5_VALUE.AppendFormat("%g", _maxForwardUpperModeSlopes[i]);
				else
					QA_XLS_COL5_VALUE = "";
				if(i<(int)_maxReverseLowerModeSlopes.size())
					QA_XLS_COL6_VALUE.AppendFormat("%g", _maxReverseLowerModeSlopes[i]);
				else
					QA_XLS_COL6_VALUE = "";
				if(i<(int)_maxReverseUpperModeSlopes.size())
					QA_XLS_COL7_VALUE.AppendFormat("%g", _maxReverseUpperModeSlopes[i]);
				else
					QA_XLS_COL7_VALUE = "";
				if(i<(int)_maxStableAreaWidths.size())
					QA_XLS_COL8_VALUE.AppendFormat("%g", _maxStableAreaWidths[i]);
				else
					QA_XLS_COL8_VALUE = "";
				if(i<(int)_maxReverseModeWidths.size())
					QA_XLS_COL9_VALUE.AppendFormat("%g", _maxReverseModeWidths[i]);
				else
					QA_XLS_COL9_VALUE = "";
				if(i<(int)_maxForwardModeWidths.size())
					QA_XLS_COL10_VALUE.AppendFormat("%g", _maxForwardModeWidths[i]);
				else
					QA_XLS_COL10_VALUE = "";
				if(i<(int)_maxPercWorkingRegions.size())
					QA_XLS_COL11_VALUE.AppendFormat("%g", _maxPercWorkingRegions[i]);
				else
					QA_XLS_COL11_VALUE = "";
			}
			else if(QA_XLS_COL3_VALUE == QA_XLS_MIN)
			{
				if(i<(int)_minForwardLowerModeSlopes.size())
					QA_XLS_COL4_VALUE.AppendFormat("%g", _minForwardLowerModeSlopes[i]);
				else
					QA_XLS_COL4_VALUE = "";
				if(i<(int)_minForwardUpperModeSlopes.size())
					QA_XLS_COL5_VALUE.AppendFormat("%g", _minForwardUpperModeSlopes[i]);
				else
					QA_XLS_COL5_VALUE = "";
				if(i<(int)_minReverseLowerModeSlopes.size())
					QA_XLS_COL6_VALUE.AppendFormat("%g", _minReverseLowerModeSlopes[i]);
				else
					QA_XLS_COL6_VALUE = "";
				if(i<(int)_minReverseUpperModeSlopes.size())
					QA_XLS_COL7_VALUE.AppendFormat("%g", _minReverseUpperModeSlopes[i]);
				else
					QA_XLS_COL7_VALUE = "";
				if(i<(int)_minStableAreaWidths.size())
					QA_XLS_COL8_VALUE.AppendFormat("%g", _minStableAreaWidths[i]);
				else
					QA_XLS_COL8_VALUE = "";
				if(i<(int)_minReverseModeWidths.size())
					QA_XLS_COL9_VALUE.AppendFormat("%g", _minReverseModeWidths[i]);
				else
					QA_XLS_COL9_VALUE = "";
				if(i<(int)_minForwardModeWidths.size())
					QA_XLS_COL10_VALUE.AppendFormat("%g", _minForwardModeWidths[i]);
				else
					QA_XLS_COL10_VALUE = "";
				if(i<(int)_minPercWorkingRegions.size())
					QA_XLS_COL11_VALUE.AppendFormat("%g", _minPercWorkingRegions[i]);
				else
					QA_XLS_COL11_VALUE = "";
			}
			else if(QA_XLS_COL3_VALUE == QA_XLS_MEAN)
			{
				if(i<(int)_meanForwardLowerModeSlopes.size())
					QA_XLS_COL4_VALUE.AppendFormat("%g", _meanForwardLowerModeSlopes[i]);
				else
					QA_XLS_COL4_VALUE = "";
				if(i<(int)_meanForwardUpperModeSlopes.size())
					QA_XLS_COL5_VALUE.AppendFormat("%g", _meanForwardUpperModeSlopes[i]);
				else
					QA_XLS_COL5_VALUE = "";
				if(i<(int)_meanReverseLowerModeSlopes.size())
					QA_XLS_COL6_VALUE.AppendFormat("%g", _meanReverseLowerModeSlopes[i]);
				else
					QA_XLS_COL6_VALUE = "";
				if(i<(int)_meanReverseUpperModeSlopes.size())
					QA_XLS_COL7_VALUE.AppendFormat("%g", _meanReverseUpperModeSlopes[i]);
				else
					QA_XLS_COL7_VALUE = "";
				if(i<(int)_meanStableAreaWidths.size())
					QA_XLS_COL8_VALUE.AppendFormat("%g", _meanStableAreaWidths[i]);
				else
					QA_XLS_COL8_VALUE = "";
				if(i<(int)_meanReverseModeWidths.size())
					QA_XLS_COL9_VALUE.AppendFormat("%g", _meanReverseModeWidths[i]);
				else
					QA_XLS_COL9_VALUE = "";
				if(i<(int)_meanForwardModeWidths.size())
					QA_XLS_COL10_VALUE.AppendFormat("%g", _meanForwardModeWidths[i]);
				else
					QA_XLS_COL10_VALUE = "";
				if(i<(int)_meanPercWorkingRegions.size())
					QA_XLS_COL11_VALUE.AppendFormat("%g", _meanPercWorkingRegions[i]);
				else
					QA_XLS_COL11_VALUE = "";
			}
			else if(QA_XLS_COL3_VALUE == QA_XLS_SUM)
			{
				if(i<(int)_sumForwardLowerModeSlopes.size())
					QA_XLS_COL4_VALUE.AppendFormat("%g", _sumForwardLowerModeSlopes[i]);
				else
					QA_XLS_COL4_VALUE = "";
				if(i<(int)_sumForwardUpperModeSlopes.size())
					QA_XLS_COL5_VALUE.AppendFormat("%g", _sumForwardUpperModeSlopes[i]);
				else
					QA_XLS_COL5_VALUE = "";
				if(i<(int)_sumReverseLowerModeSlopes.size())
					QA_XLS_COL6_VALUE.AppendFormat("%g", _sumReverseLowerModeSlopes[i]);
				else
					QA_XLS_COL6_VALUE = "";
				if(i<(int)_sumReverseUpperModeSlopes.size())
					QA_XLS_COL7_VALUE.AppendFormat("%g", _sumReverseUpperModeSlopes[i]);
				else
					QA_XLS_COL7_VALUE = "";
				if(i<(int)_sumStableAreaWidths.size())
					QA_XLS_COL8_VALUE.AppendFormat("%g", _sumStableAreaWidths[i]);
				else
					QA_XLS_COL8_VALUE = "";
				if(i<(int)_sumReverseModeWidths.size())
					QA_XLS_COL9_VALUE.AppendFormat("%g", _sumReverseModeWidths[i]);
				else
					QA_XLS_COL9_VALUE = "";
				if(i<(int)_sumForwardModeWidths.size())
					QA_XLS_COL10_VALUE.AppendFormat("%g", _sumForwardModeWidths[i]);
				else
					QA_XLS_COL10_VALUE = "";
				if(i<(int)_sumPercWorkingRegions.size())
					QA_XLS_COL11_VALUE.AppendFormat("%g", _sumPercWorkingRegions[i]);
				else
					QA_XLS_COL11_VALUE = "";
			}
			else if(QA_XLS_COL3_VALUE == QA_XLS_COUNT)
			{
				if(i<(int)_numForwardLowerModeSlopeSamples.size())
					QA_XLS_COL4_VALUE.AppendFormat("%g", _numForwardLowerModeSlopeSamples[i]);
				else
					QA_XLS_COL4_VALUE = "";
				if(i<(int)_numForwardUpperModeSlopeSamples.size())
					QA_XLS_COL5_VALUE.AppendFormat("%g", _numForwardUpperModeSlopeSamples[i]);
				else
					QA_XLS_COL5_VALUE = "";
				if(i<(int)_numReverseLowerModeSlopeSamples.size())
					QA_XLS_COL6_VALUE.AppendFormat("%g", _numReverseLowerModeSlopeSamples[i]);
				else
					QA_XLS_COL6_VALUE = "";
				if(i<(int)_numReverseUpperModeSlopeSamples.size())
					QA_XLS_COL7_VALUE.AppendFormat("%g", _numReverseUpperModeSlopeSamples[i]);
				else
					QA_XLS_COL7_VALUE = "";
				if(i<(int)_numStableAreaWidthSamples.size())
					QA_XLS_COL8_VALUE.AppendFormat("%g", _numStableAreaWidthSamples[i]);
				else
					QA_XLS_COL8_VALUE = "";
				if(i<(int)_numReverseModeWidthSamples.size())
					QA_XLS_COL9_VALUE.AppendFormat("%g", _numReverseModeWidthSamples[i]);
				else
					QA_XLS_COL9_VALUE = "";
				if(i<(int)_numForwardModeWidthSamples.size())
					QA_XLS_COL10_VALUE.AppendFormat("%g", _numForwardModeWidthSamples[i]);
				else
					QA_XLS_COL10_VALUE = "";
				if(i<(int)_numPercWorkingRegionSamples.size())
					QA_XLS_COL11_VALUE.AppendFormat("%g", _numPercWorkingRegionSamples[i]);
				else
					QA_XLS_COL11_VALUE = "";
			}
			else if(QA_XLS_COL3_VALUE == QA_XLS_STDDEV)
			{
				if(i<(int)_stdDevForwardLowerModeSlopes.size())
					QA_XLS_COL4_VALUE.AppendFormat("%g", _stdDevForwardLowerModeSlopes[i]);
				else
					QA_XLS_COL4_VALUE = "";
				if(i<(int)_stdDevForwardUpperModeSlopes.size())
					QA_XLS_COL5_VALUE.AppendFormat("%g", _stdDevForwardUpperModeSlopes[i]);
				else
					QA_XLS_COL5_VALUE = "";
				if(i<(int)_stdDevReverseLowerModeSlopes.size())
					QA_XLS_COL6_VALUE.AppendFormat("%g", _stdDevReverseLowerModeSlopes[i]);
				else
					QA_XLS_COL6_VALUE = "";
				if(i<(int)_stdDevReverseUpperModeSlopes.size())
					QA_XLS_COL7_VALUE.AppendFormat("%g", _stdDevReverseUpperModeSlopes[i]);
				else
					QA_XLS_COL7_VALUE = "";
				if(i<(int)_stdDevStableAreaWidths.size())
					QA_XLS_COL8_VALUE.AppendFormat("%g", _stdDevStableAreaWidths[i]);
				else
					QA_XLS_COL8_VALUE = "";
				if(i<(int)_stdDevReverseModeWidths.size())
					QA_XLS_COL9_VALUE.AppendFormat("%g", _stdDevReverseModeWidths[i]);
				else
					QA_XLS_COL9_VALUE = "";
				if(i<(int)_stdDevForwardModeWidths.size())
					QA_XLS_COL10_VALUE.AppendFormat("%g", _stdDevForwardModeWidths[i]);
				else
					QA_XLS_COL10_VALUE = "";
				if(i<(int)_stdDevPercWorkingRegions.size())
					QA_XLS_COL11_VALUE.AppendFormat("%g", _stdDevPercWorkingRegions[i]);
				else
					QA_XLS_COL11_VALUE = "";
			}
			else if(QA_XLS_COL3_VALUE == QA_XLS_VAR)
			{
				if(i<(int)_varForwardLowerModeSlopes.size())
					QA_XLS_COL4_VALUE.AppendFormat("%g", _varForwardLowerModeSlopes[i]);
				else
					QA_XLS_COL4_VALUE = "";
				if(i<(int)_varForwardUpperModeSlopes.size())
					QA_XLS_COL5_VALUE.AppendFormat("%g", _varForwardUpperModeSlopes[i]);
				else
					QA_XLS_COL5_VALUE = "";
				if(i<(int)_varReverseLowerModeSlopes.size())
					QA_XLS_COL6_VALUE.AppendFormat("%g", _varReverseLowerModeSlopes[i]);
				else
					QA_XLS_COL6_VALUE = "";
				if(i<(int)_varReverseUpperModeSlopes.size())
					QA_XLS_COL7_VALUE.AppendFormat("%g", _varReverseUpperModeSlopes[i]);
				else
					QA_XLS_COL7_VALUE = "";
				if(i<(int)_varStableAreaWidths.size())
					QA_XLS_COL8_VALUE.AppendFormat("%g", _varStableAreaWidths[i]);
				else
					QA_XLS_COL8_VALUE = "";
				if(i<(int)_varReverseModeWidths.size())
					QA_XLS_COL9_VALUE.AppendFormat("%g", _varReverseModeWidths[i]);
				else
					QA_XLS_COL9_VALUE = "";
				if(i<(int)_varForwardModeWidths.size())
					QA_XLS_COL10_VALUE.AppendFormat("%g", _varForwardModeWidths[i]);
				else
					QA_XLS_COL10_VALUE = "";
				if(i<(int)_varPercWorkingRegions.size())
					QA_XLS_COL11_VALUE.AppendFormat("%g", _varPercWorkingRegions[i]);
				else
					QA_XLS_COL11_VALUE = "";
			}
			else if(QA_XLS_COL3_VALUE == QA_XLS_OTHERS)
			{
				if(i<(int)_forwardLowerModalDistortionAngles.size())
					QA_XLS_COL12_VALUE.AppendFormat("%g", _forwardLowerModalDistortionAngles[i]);
				else
					QA_XLS_COL12_VALUE = "";
				if(i<(int)_forwardLowerModalDistortionAngleXPositions.size())
					QA_XLS_COL13_VALUE.AppendFormat("%g", _forwardLowerModalDistortionAngleXPositions[i]);
				else
					QA_XLS_COL13_VALUE = "";
				if(i<(int)_forwardUpperModalDistortionAngles.size())
					QA_XLS_COL14_VALUE.AppendFormat("%g", _forwardUpperModalDistortionAngles[i]);
				else
					QA_XLS_COL14_VALUE = "";
				if(i<(int)_forwardUpperModalDistortionAngleXPositions.size())
					QA_XLS_COL15_VALUE.AppendFormat("%g", _forwardUpperModalDistortionAngleXPositions[i]);
				else
					QA_XLS_COL15_VALUE = "";
				if(i<(int)_reverseLowerModalDistortionAngles.size())
					QA_XLS_COL16_VALUE.AppendFormat("%g", _reverseLowerModalDistortionAngles[i]);
				else
					QA_XLS_COL16_VALUE = "";
				if(i<(int)_reverseLowerModalDistortionAngleXPositions.size())
					QA_XLS_COL17_VALUE.AppendFormat("%g", _reverseLowerModalDistortionAngleXPositions[i]);
				else
					QA_XLS_COL17_VALUE = "";
				if(i<(int)_reverseUpperModalDistortionAngles.size())
					QA_XLS_COL18_VALUE.AppendFormat("%g", _reverseUpperModalDistortionAngles[i]);
				else
					QA_XLS_COL18_VALUE = "";
				if(i<(int)_reverseUpperModalDistortionAngleXPositions.size())
					QA_XLS_COL19_VALUE.AppendFormat("%g", _reverseUpperModalDistortionAngleXPositions[i]);
				else
					QA_XLS_COL19_VALUE = "";
				if(i<(int)_maxForwardLowerModeBDCAreaXLengths.size())
					QA_XLS_COL20_VALUE.AppendFormat("%g", _maxForwardLowerModeBDCAreaXLengths[i]);
				else
					QA_XLS_COL20_VALUE = "";
				if(i<(int)_maxForwardUpperModeBDCAreaXLengths.size())
					QA_XLS_COL21_VALUE.AppendFormat("%g", _maxForwardUpperModeBDCAreaXLengths[i]);
				else
					QA_XLS_COL21_VALUE = "";
				if(i<(int)_maxForwardLowerModeBDCAreas.size())
					QA_XLS_COL22_VALUE.AppendFormat("%g", _maxForwardLowerModeBDCAreas[i]);
				else
					QA_XLS_COL22_VALUE = "";
				if(i<(int)_maxForwardUpperModeBDCAreas.size())
					QA_XLS_COL23_VALUE.AppendFormat("%g", _maxForwardUpperModeBDCAreas[i]);
				else
					QA_XLS_COL23_VALUE = "";
				if(i<(int)_sumForwardLowerModeBDCAreas.size())
					QA_XLS_COL24_VALUE.AppendFormat("%g", _sumForwardLowerModeBDCAreas[i]);
				else
					QA_XLS_COL24_VALUE = "";
				if(i<(int)_sumForwardUpperModeBDCAreas.size())
					QA_XLS_COL25_VALUE.AppendFormat("%g", _sumForwardUpperModeBDCAreas[i]);
				else
					QA_XLS_COL25_VALUE = "";
				if(i<(int)_numForwardLowerModeBDCAreaSamples.size())
					QA_XLS_COL26_VALUE.AppendFormat("%g", _numForwardLowerModeBDCAreaSamples[i]);
				else
					QA_XLS_COL26_VALUE = "";
				if(i<(int)_numForwardUpperModeBDCAreaSamples.size())
					QA_XLS_COL27_VALUE.AppendFormat("%g", _numForwardUpperModeBDCAreaSamples[i]);
				else
					QA_XLS_COL27_VALUE = "";
				if(i<(int)_maxReverseLowerModeBDCAreaXLengths.size())
					QA_XLS_COL28_VALUE.AppendFormat("%g", _maxReverseLowerModeBDCAreaXLengths[i]);
				else
					QA_XLS_COL28_VALUE = "";
				if(i<(int)_maxReverseUpperModeBDCAreaXLengths.size())
					QA_XLS_COL29_VALUE.AppendFormat("%g", _maxReverseUpperModeBDCAreaXLengths[i]);
				else
					QA_XLS_COL29_VALUE = "";
				if(i<(int)_maxReverseLowerModeBDCAreas.size())
					QA_XLS_COL30_VALUE.AppendFormat("%g", _maxReverseLowerModeBDCAreas[i]);
				else
					QA_XLS_COL30_VALUE = "";
				if(i<(int)_maxReverseUpperModeBDCAreas.size())
					QA_XLS_COL31_VALUE.AppendFormat("%g", _maxReverseUpperModeBDCAreas[i]);
				else
					QA_XLS_COL31_VALUE = "";
				if(i<(int)_sumReverseLowerModeBDCAreas.size())
					QA_XLS_COL32_VALUE.AppendFormat("%g", _sumReverseLowerModeBDCAreas[i]);
				else
					QA_XLS_COL32_VALUE = "";
				if(i<(int)_sumReverseUpperModeBDCAreas.size())
					QA_XLS_COL33_VALUE.AppendFormat("%g", _sumReverseUpperModeBDCAreas[i]);
				else
					QA_XLS_COL33_VALUE = "";
				if(i<(int)_numReverseLowerModeBDCAreaSamples.size())
					QA_XLS_COL34_VALUE.AppendFormat("%g", _numReverseLowerModeBDCAreaSamples[i]);
				else
					QA_XLS_COL34_VALUE = "";
				if(i<(int)_numReverseUpperModeBDCAreaSamples.size())
					QA_XLS_COL35_VALUE.AppendFormat("%g", _numReverseUpperModeBDCAreaSamples[i]);
				else
					QA_XLS_COL35_VALUE = "";
				if(i<(int)_middleLineRMSValues.size())
					QA_XLS_COL36_VALUE.AppendFormat("%g", _middleLineRMSValues[i]);
				else
					QA_XLS_COL36_VALUE = "";
			}

			rowArray.RemoveAll();
			rowArray.Add(QA_XLS_COL1_VALUE);
			rowArray.Add(QA_XLS_COL2_VALUE);
			rowArray.Add(QA_XLS_COL3_VALUE);
			rowArray.Add(QA_XLS_COL4_VALUE);
			rowArray.Add(QA_XLS_COL5_VALUE);
			rowArray.Add(QA_XLS_COL6_VALUE);
			rowArray.Add(QA_XLS_COL7_VALUE);
			rowArray.Add(QA_XLS_COL8_VALUE);
			rowArray.Add(QA_XLS_COL9_VALUE);
			rowArray.Add(QA_XLS_COL10_VALUE);
			rowArray.Add(QA_XLS_COL11_VALUE);
			rowArray.Add(QA_XLS_COL12_VALUE);
			rowArray.Add(QA_XLS_COL13_VALUE);
			rowArray.Add(QA_XLS_COL14_VALUE);
			rowArray.Add(QA_XLS_COL15_VALUE);
			rowArray.Add(QA_XLS_COL16_VALUE);
			rowArray.Add(QA_XLS_COL17_VALUE);
			rowArray.Add(QA_XLS_COL18_VALUE);
			rowArray.Add(QA_XLS_COL19_VALUE);
			rowArray.Add(QA_XLS_COL20_VALUE);
			rowArray.Add(QA_XLS_COL21_VALUE);
			rowArray.Add(QA_XLS_COL22_VALUE);
			rowArray.Add(QA_XLS_COL23_VALUE);
			rowArray.Add(QA_XLS_COL24_VALUE);
			rowArray.Add(QA_XLS_COL25_VALUE);
			rowArray.Add(QA_XLS_COL26_VALUE);
			rowArray.Add(QA_XLS_COL27_VALUE);
			rowArray.Add(QA_XLS_COL28_VALUE);
			rowArray.Add(QA_XLS_COL29_VALUE);
			rowArray.Add(QA_XLS_COL30_VALUE);
			rowArray.Add(QA_XLS_COL31_VALUE);
			rowArray.Add(QA_XLS_COL32_VALUE);
			rowArray.Add(QA_XLS_COL33_VALUE);
			rowArray.Add(QA_XLS_COL34_VALUE);
			rowArray.Add(QA_XLS_COL35_VALUE);
			rowArray.Add(QA_XLS_COL36_VALUE);

			qaResults.addRow(rowArray);
		}

		qaResults.addEmptyLine();

	}

	return err;
}


SuperModeMapAnalysis::rcode
SuperModeMapAnalysis::
writePassFailResultsToFile( CLoseGridFile &passFailResults )
{
	rcode err = rcode::ok;

    if(_passFailAnalysisComplete)
    {
		CString errorMessage = "";
		int errorNum = 0;
		getQAError(_qaDetectedError, errorNum, errorMessage);

        CStringArray error_msg_row;
        error_msg_row.Add(errorMessage);
        passFailResults.addRow(error_msg_row);
        passFailResults.addEmptyLine();

		//////////////////////////////////////////
		
		CStringArray row;
		CString cellString = "";

		row.RemoveAll();
		row.Add("Slope Window Size");
		cellString.Format("%d", CG_REG->get_DSDBR01_SMQA_slope_window_size());
		row.Add(cellString);
		passFailResults.addRow(row);

		row.RemoveAll();
		row.Add("Modal Distortion Min X");
		cellString.Format("%g", CG_REG->get_DSDBR01_SMQA_modal_distortion_min_x());
		row.Add(cellString);
		passFailResults.addRow(row);

		row.RemoveAll();
		row.Add("Modal Distortion Max X");
		cellString.Format("%g", CG_REG->get_DSDBR01_SMQA_modal_distortion_max_x());
		row.Add(cellString);
		passFailResults.addRow(row);

		row.RemoveAll();
		row.Add("Modal Distortion Min Y");
		cellString.Format("%g", CG_REG->get_DSDBR01_SMQA_modal_distortion_min_y());
		row.Add(cellString);
		passFailResults.addRow(row);

		passFailResults.addEmptyLine();

		//////////////////////////////////////////

	    const CString	QA_XLS_HEADER1	= "";
	    const CString	QA_XLS_HEADER2	= "Upper line mode borders removed due to error";
	    const CString	QA_XLS_HEADER3	= "Lower line mode borders removed due to error";
	    const CString	QA_XLS_HEADER4	= "Middle lines removed due to error";
	    const CString	QA_XLS_HEADER5	= "Forward Max Mode Width";
	    const CString	QA_XLS_HEADER6	= "Reverse Max Mode Width";
		const CString	QA_XLS_HEADER7	= "Mean % Working Region";
		const CString	QA_XLS_HEADER8	= "Min % Working Region";
		const CString	QA_XLS_HEADER9	= "Forward Min Mode Width";
		const CString	QA_XLS_HEADER10	= "Reverse Min Mode Width";
	    const CString	QA_XLS_HEADER11	= "Forward Max Lower Modal Distortion Angle";
	    const CString	QA_XLS_HEADER12	= "Forward Max Lower Modal Distortion X-Position";
	    const CString	QA_XLS_HEADER13	= "Forward Max Upper Modal Distortion Angle";
	    const CString	QA_XLS_HEADER14	= "Forward Max Upper Modal Distortion X-Position";
	    const CString	QA_XLS_HEADER15	= "Reverse Max Lower Modal Distortion Angle";
	    const CString	QA_XLS_HEADER16	= "Reverse Max Lower Modal Distortion X-Position";
	    const CString	QA_XLS_HEADER17	= "Reverse Max Upper Modal Distortion Angle";
	    const CString	QA_XLS_HEADER18	= "Reverse Max Upper Modal Distortion X-Position";
	    const CString	QA_XLS_HEADER19 = "Forward Lower Max Boundary Direction Change X-Length";
	    const CString	QA_XLS_HEADER20 = "Forward Upper Max Boundary Direction Change X-Length";
		const CString	QA_XLS_HEADER21 = "Forward Lower Max Boundary Direction Change Area";
		const CString	QA_XLS_HEADER22 = "Forward Upper Max Boundary Direction Change Area";
	    const CString	QA_XLS_HEADER23 = "Forward Lower Sum of Boundary Direction Change Areas";
	    const CString	QA_XLS_HEADER24 = "Forward Upper Sum of Boundary Direction Change Areas";
	    const CString	QA_XLS_HEADER25 = "Forward Lower Number of Boundary Direction Changes";
	    const CString	QA_XLS_HEADER26 = "Forward Upper Number of Boundary Direction Changes";
	    const CString	QA_XLS_HEADER27 = "Reverse Lower Max Boundary Direction Change X-Length";
	    const CString	QA_XLS_HEADER28 = "Reverse Upper Max Boundary Direction Change X-Length";
		const CString	QA_XLS_HEADER29 = "Reverse Lower Max Boundary Direction Change Area";
		const CString	QA_XLS_HEADER30 = "Reverse Upper Max Boundary Direction Change Area";
	    const CString	QA_XLS_HEADER31 = "Reverse Lower Sum of Boundary Direction Change Areas";
	    const CString	QA_XLS_HEADER32 = "Reverse Upper Sum of Boundary Direction Change Areas";
	    const CString	QA_XLS_HEADER33 = "Reverse Lower Number of Boundary Direction Changes";
	    const CString	QA_XLS_HEADER34 = "Reverse Upper Number of Boundary Direction Changes";
	    const CString	QA_XLS_HEADER35 = "Forward Lower Max Mode Slope";
	    const CString	QA_XLS_HEADER36 = "Forward Upper Max Mode Slope";
	    const CString	QA_XLS_HEADER37 = "Reverse Lower Max Mode Slope";
	    const CString	QA_XLS_HEADER38 = "Reverse Upper Max Mode Slope";
	    const CString	QA_XLS_HEADER39 = "Max Middle Line RMS Value";
	    const CString	QA_XLS_HEADER40 = "Min Middle Line Slope";
	    const CString	QA_XLS_HEADER41 = "Max Middle Line Slope";


	    const CString	QA_XLS_ROWTITLE1	= "Pass/Fail Result";
	    const CString	QA_XLS_ROWTITLE2	= "Min Value Measured";
	    const CString	QA_XLS_ROWTITLE3	= "Max Value Measured";
	    const CString	QA_XLS_ROWTITLE4	= "Number of Failed LMs";
	    const CString	QA_XLS_ROWTITLE5	= "Threshold Applied";

	    CString	QA_XLS_COL1_VALUE = "";
	    CString	QA_XLS_COL2_VALUE = "";
	    CString	QA_XLS_COL3_VALUE = "";
	    CString	QA_XLS_COL4_VALUE = "";
	    CString	QA_XLS_COL5_VALUE = "";
	    CString	QA_XLS_COL6_VALUE = "";
	    CString	QA_XLS_COL7_VALUE = "";
	    CString	QA_XLS_COL8_VALUE = "";
	    CString	QA_XLS_COL9_VALUE = "";
	    CString	QA_XLS_COL10_VALUE = "";
	    CString	QA_XLS_COL11_VALUE = "";
	    CString	QA_XLS_COL12_VALUE = "";
	    CString	QA_XLS_COL13_VALUE = "";
	    CString	QA_XLS_COL14_VALUE = "";
	    CString	QA_XLS_COL15_VALUE = "";
	    CString	QA_XLS_COL16_VALUE = "";
	    CString	QA_XLS_COL17_VALUE = "";
	    CString	QA_XLS_COL18_VALUE = "";
	    CString	QA_XLS_COL19_VALUE = "";
	    CString	QA_XLS_COL20_VALUE = "";
	    CString	QA_XLS_COL21_VALUE = "";
	    CString	QA_XLS_COL22_VALUE = "";
	    CString	QA_XLS_COL23_VALUE = "";
	    CString	QA_XLS_COL24_VALUE = "";
	    CString	QA_XLS_COL25_VALUE = "";
	    CString	QA_XLS_COL26_VALUE = "";
	    CString	QA_XLS_COL27_VALUE = "";
	    CString	QA_XLS_COL28_VALUE = "";
	    CString	QA_XLS_COL29_VALUE = "";
	    CString	QA_XLS_COL30_VALUE = "";
	    CString	QA_XLS_COL31_VALUE = "";
	    CString	QA_XLS_COL32_VALUE = "";
	    CString	QA_XLS_COL33_VALUE = "";
	    CString	QA_XLS_COL34_VALUE = "";
	    CString	QA_XLS_COL35_VALUE = "";
	    CString	QA_XLS_COL36_VALUE = "";
	    CString	QA_XLS_COL37_VALUE = "";
	    CString	QA_XLS_COL38_VALUE = "";
	    CString	QA_XLS_COL39_VALUE = "";
	    CString	QA_XLS_COL40_VALUE = "";
	    CString	QA_XLS_COL41_VALUE = "";

	    CStringArray headerArray;
	    headerArray.Add(QA_XLS_HEADER1);
	    headerArray.Add(QA_XLS_HEADER2);
	    headerArray.Add(QA_XLS_HEADER3);
	    headerArray.Add(QA_XLS_HEADER4);
	    headerArray.Add(QA_XLS_HEADER5);
	    headerArray.Add(QA_XLS_HEADER6);
	    headerArray.Add(QA_XLS_HEADER7);
	    headerArray.Add(QA_XLS_HEADER8);
	    headerArray.Add(QA_XLS_HEADER9);
	    headerArray.Add(QA_XLS_HEADER10);
	    headerArray.Add(QA_XLS_HEADER11);
	    headerArray.Add(QA_XLS_HEADER12);
	    headerArray.Add(QA_XLS_HEADER13);
	    headerArray.Add(QA_XLS_HEADER14);
	    headerArray.Add(QA_XLS_HEADER15);
	    headerArray.Add(QA_XLS_HEADER16);
	    headerArray.Add(QA_XLS_HEADER17);
	    headerArray.Add(QA_XLS_HEADER18);
	    headerArray.Add(QA_XLS_HEADER19);
	    headerArray.Add(QA_XLS_HEADER20);
	    headerArray.Add(QA_XLS_HEADER21);
	    headerArray.Add(QA_XLS_HEADER22);
	    headerArray.Add(QA_XLS_HEADER23);
	    headerArray.Add(QA_XLS_HEADER24);
	    headerArray.Add(QA_XLS_HEADER25);
	    headerArray.Add(QA_XLS_HEADER26);
	    headerArray.Add(QA_XLS_HEADER27);
	    headerArray.Add(QA_XLS_HEADER28);
	    headerArray.Add(QA_XLS_HEADER29);
	    headerArray.Add(QA_XLS_HEADER30);
	    headerArray.Add(QA_XLS_HEADER31);
	    headerArray.Add(QA_XLS_HEADER32);
	    headerArray.Add(QA_XLS_HEADER33);
	    headerArray.Add(QA_XLS_HEADER34);
	    headerArray.Add(QA_XLS_HEADER35);
	    headerArray.Add(QA_XLS_HEADER36);
	    headerArray.Add(QA_XLS_HEADER37);
	    headerArray.Add(QA_XLS_HEADER38);
	    headerArray.Add(QA_XLS_HEADER39);
	    headerArray.Add(QA_XLS_HEADER40);
	    headerArray.Add(QA_XLS_HEADER41);

	    CStringArray rowTitleArray;
	    rowTitleArray.Add(QA_XLS_ROWTITLE1);
	    rowTitleArray.Add(QA_XLS_ROWTITLE2);
	    rowTitleArray.Add(QA_XLS_ROWTITLE3);
	    rowTitleArray.Add(QA_XLS_ROWTITLE4);
	    rowTitleArray.Add(QA_XLS_ROWTITLE5);


	    // set headers
	    passFailResults.addRow(headerArray);

        // add space
	    passFailResults.addEmptyLine();


	    ///////////////////////////////
	    /// first row - pass/fails
	    //
		QA_XLS_COL2_VALUE	= _maxUpperModeBordersRemovedPFAnalysis.pass ? "PASS" : "FAIL";
		QA_XLS_COL3_VALUE	= _maxLowerModeBordersRemovedPFAnalysis.pass ? "PASS" : "FAIL";
		QA_XLS_COL4_VALUE	= _maxMiddleLineRemovedPFAnalysis.pass ? "PASS" : "FAIL";
        QA_XLS_COL5_VALUE	= _maxForwardModeWidthPFAnalysis.pass ? "PASS" : "FAIL";
		QA_XLS_COL6_VALUE	= _maxReverseModeWidthPFAnalysis.pass ? "PASS" : "FAIL";
		QA_XLS_COL7_VALUE	= _meanPercWorkingRegionPFAnalysis.pass ? "PASS" : "FAIL";
		QA_XLS_COL8_VALUE	= _minPercWorkingRegionPFAnalysis.pass ? "PASS" : "FAIL";
	    QA_XLS_COL9_VALUE	= _minForwardModeWidthPFAnalysis.pass ? "PASS" : "FAIL";
	    QA_XLS_COL10_VALUE	= _minReverseModeWidthPFAnalysis.pass ? "PASS" : "FAIL";
        QA_XLS_COL11_VALUE	= _forwardLowerModalDistortionAnglePFAnalysis.pass ? "PASS" : "FAIL";
		QA_XLS_COL12_VALUE	= _forwardLowerModalDistortionAngleXPositionPFAnalysis.pass ? "PASS" : "FAIL";
        QA_XLS_COL13_VALUE	= _forwardUpperModalDistortionAnglePFAnalysis.pass ? "PASS" : "FAIL";
		QA_XLS_COL14_VALUE	= _forwardUpperModalDistortionAngleXPositionPFAnalysis.pass ? "PASS" : "FAIL";
        QA_XLS_COL15_VALUE	= _reverseLowerModalDistortionAnglePFAnalysis.pass ? "PASS" : "FAIL";
		QA_XLS_COL16_VALUE	= _reverseLowerModalDistortionAngleXPositionPFAnalysis.pass ? "PASS" : "FAIL";
        QA_XLS_COL17_VALUE	= _reverseUpperModalDistortionAnglePFAnalysis.pass ? "PASS" : "FAIL";
		QA_XLS_COL18_VALUE	= _reverseUpperModalDistortionAngleXPositionPFAnalysis.pass ? "PASS" : "FAIL";
        QA_XLS_COL19_VALUE	= _maxForwardLowerModeBDCAreaXLengthPFAnalysis.pass ? "PASS" : "FAIL";
        QA_XLS_COL20_VALUE	= _maxForwardUpperModeBDCAreaXLengthPFAnalysis.pass ? "PASS" : "FAIL";
        QA_XLS_COL21_VALUE	= _maxForwardLowerModeBDCAreaPFAnalysis.pass ? "PASS" : "FAIL";
        QA_XLS_COL22_VALUE	= _maxForwardUpperModeBDCAreaPFAnalysis.pass ? "PASS" : "FAIL";
        QA_XLS_COL23_VALUE	= _sumForwardLowerModeBDCAreasPFAnalysis.pass ? "PASS" : "FAIL";
        QA_XLS_COL24_VALUE	= _sumForwardUpperModeBDCAreasPFAnalysis.pass ? "PASS" : "FAIL";
        QA_XLS_COL25_VALUE	= _numForwardLowerModeBDCAreasPFAnalysis.pass ? "PASS" : "FAIL";
        QA_XLS_COL26_VALUE	= _numForwardUpperModeBDCAreasPFAnalysis.pass ? "PASS" : "FAIL";
        QA_XLS_COL27_VALUE	= _maxReverseLowerModeBDCAreaXLengthPFAnalysis.pass ? "PASS" : "FAIL";
        QA_XLS_COL28_VALUE	= _maxReverseUpperModeBDCAreaXLengthPFAnalysis.pass ? "PASS" : "FAIL";
        QA_XLS_COL29_VALUE	= _maxReverseLowerModeBDCAreaPFAnalysis.pass ? "PASS" : "FAIL";
        QA_XLS_COL30_VALUE	= _maxReverseUpperModeBDCAreaPFAnalysis.pass ? "PASS" : "FAIL";
        QA_XLS_COL31_VALUE	= _sumReverseLowerModeBDCAreasPFAnalysis.pass ? "PASS" : "FAIL";
        QA_XLS_COL32_VALUE	= _sumReverseUpperModeBDCAreasPFAnalysis.pass ? "PASS" : "FAIL";
        QA_XLS_COL33_VALUE	= _numReverseLowerModeBDCAreasPFAnalysis.pass ? "PASS" : "FAIL";
        QA_XLS_COL34_VALUE	= _numReverseUpperModeBDCAreasPFAnalysis.pass ? "PASS" : "FAIL";
        QA_XLS_COL35_VALUE	= _maxForwardLowerModeSlopePFAnalysis.pass ? "PASS" : "FAIL";
        QA_XLS_COL36_VALUE	= _maxForwardUpperModeSlopePFAnalysis.pass ? "PASS" : "FAIL";
        QA_XLS_COL37_VALUE	= _maxReverseLowerModeSlopePFAnalysis.pass ? "PASS" : "FAIL";
        QA_XLS_COL38_VALUE	= _maxReverseUpperModeSlopePFAnalysis.pass ? "PASS" : "FAIL";
        QA_XLS_COL39_VALUE	= _middleLineRMSValuesPFAnalysis.pass ? "PASS" : "FAIL";
        QA_XLS_COL40_VALUE	= _minMiddleLineSlopePFAnalysis.pass ? "PASS" : "FAIL";
        QA_XLS_COL41_VALUE	= _maxMiddleLineSlopePFAnalysis.pass ? "PASS" : "FAIL";

	    CStringArray rowArray;

	    rowArray.RemoveAll();
        rowArray.Add(QA_XLS_ROWTITLE1);
	    rowArray.Add(QA_XLS_COL2_VALUE);
	    rowArray.Add(QA_XLS_COL3_VALUE);
	    rowArray.Add(QA_XLS_COL4_VALUE);
	    rowArray.Add(QA_XLS_COL5_VALUE);
	    rowArray.Add(QA_XLS_COL6_VALUE);
	    rowArray.Add(QA_XLS_COL7_VALUE);
	    rowArray.Add(QA_XLS_COL8_VALUE);
	    rowArray.Add(QA_XLS_COL9_VALUE);
	    rowArray.Add(QA_XLS_COL10_VALUE);
	    rowArray.Add(QA_XLS_COL11_VALUE);
	    rowArray.Add(QA_XLS_COL12_VALUE);
	    rowArray.Add(QA_XLS_COL13_VALUE);
	    rowArray.Add(QA_XLS_COL14_VALUE);
	    rowArray.Add(QA_XLS_COL15_VALUE);
	    rowArray.Add(QA_XLS_COL16_VALUE);
	    rowArray.Add(QA_XLS_COL17_VALUE);
	    rowArray.Add(QA_XLS_COL18_VALUE);
	    rowArray.Add(QA_XLS_COL19_VALUE);
	    rowArray.Add(QA_XLS_COL20_VALUE);
	    rowArray.Add(QA_XLS_COL21_VALUE);
	    rowArray.Add(QA_XLS_COL22_VALUE);
	    rowArray.Add(QA_XLS_COL23_VALUE);
	    rowArray.Add(QA_XLS_COL24_VALUE);
	    rowArray.Add(QA_XLS_COL25_VALUE);
	    rowArray.Add(QA_XLS_COL26_VALUE);
	    rowArray.Add(QA_XLS_COL27_VALUE);
	    rowArray.Add(QA_XLS_COL28_VALUE);
	    rowArray.Add(QA_XLS_COL29_VALUE);
	    rowArray.Add(QA_XLS_COL30_VALUE);
	    rowArray.Add(QA_XLS_COL31_VALUE);
	    rowArray.Add(QA_XLS_COL32_VALUE);
	    rowArray.Add(QA_XLS_COL33_VALUE);
	    rowArray.Add(QA_XLS_COL34_VALUE);
	    rowArray.Add(QA_XLS_COL35_VALUE);
	    rowArray.Add(QA_XLS_COL36_VALUE);
	    rowArray.Add(QA_XLS_COL37_VALUE);
	    rowArray.Add(QA_XLS_COL38_VALUE);
	    rowArray.Add(QA_XLS_COL39_VALUE);
	    rowArray.Add(QA_XLS_COL40_VALUE);
	    rowArray.Add(QA_XLS_COL41_VALUE);
	    passFailResults.addRow(rowArray);


        ////////////////////////////////////////
	    ///
	    //	num failed Modes
	    //
        QA_XLS_COL2_VALUE = "";
        QA_XLS_COL3_VALUE = "";
        QA_XLS_COL4_VALUE = "";
        QA_XLS_COL5_VALUE = "";
        QA_XLS_COL6_VALUE = "";
        QA_XLS_COL7_VALUE = "";
        QA_XLS_COL8_VALUE = "";
        QA_XLS_COL9_VALUE = "";
        QA_XLS_COL10_VALUE = "";
        QA_XLS_COL11_VALUE = "";
        QA_XLS_COL12_VALUE = "";
        QA_XLS_COL13_VALUE = "";
        QA_XLS_COL14_VALUE = "";
        QA_XLS_COL15_VALUE = "";
        QA_XLS_COL16_VALUE = "";
        QA_XLS_COL17_VALUE = "";
        QA_XLS_COL18_VALUE = "";
        QA_XLS_COL19_VALUE = "";
        QA_XLS_COL20_VALUE = "";
        QA_XLS_COL21_VALUE = "";
        QA_XLS_COL22_VALUE = "";
        QA_XLS_COL23_VALUE = "";
        QA_XLS_COL24_VALUE = "";
        QA_XLS_COL25_VALUE = "";
        QA_XLS_COL26_VALUE = "";
        QA_XLS_COL27_VALUE = "";
        QA_XLS_COL28_VALUE = "";
        QA_XLS_COL29_VALUE = "";
        QA_XLS_COL30_VALUE = "";
        QA_XLS_COL31_VALUE = "";
        QA_XLS_COL32_VALUE = "";
        QA_XLS_COL33_VALUE = "";
        QA_XLS_COL34_VALUE = "";
        QA_XLS_COL35_VALUE = "";
        QA_XLS_COL36_VALUE = "";
        QA_XLS_COL37_VALUE = "";
        QA_XLS_COL38_VALUE = "";
        QA_XLS_COL39_VALUE = "";
        QA_XLS_COL40_VALUE = "";
        QA_XLS_COL41_VALUE = "";

		QA_XLS_COL2_VALUE.Format("%d", _numUpperLinesRemoved);
		QA_XLS_COL3_VALUE.Format("%d", _numLowerLinesRemoved);
		QA_XLS_COL4_VALUE.Format("%d", _numMiddleLinesRemoved);
	    QA_XLS_COL5_VALUE.AppendFormat("%d", _maxForwardModeWidthPFAnalysis.numFailedModes);
	    QA_XLS_COL6_VALUE.AppendFormat("%d", _maxReverseModeWidthPFAnalysis.numFailedModes);
	    QA_XLS_COL7_VALUE.AppendFormat("%d", _meanPercWorkingRegionPFAnalysis.numFailedModes);
	    QA_XLS_COL8_VALUE.AppendFormat("%d", _minPercWorkingRegionPFAnalysis.numFailedModes);
	    QA_XLS_COL9_VALUE.AppendFormat("%d", _minForwardModeWidthPFAnalysis.numFailedModes);
	    QA_XLS_COL10_VALUE.AppendFormat("%d", _minReverseModeWidthPFAnalysis.numFailedModes);
	    QA_XLS_COL11_VALUE.AppendFormat("%d", _forwardLowerModalDistortionAnglePFAnalysis.numFailedModes);
	    QA_XLS_COL12_VALUE.AppendFormat("%d", _forwardLowerModalDistortionAngleXPositionPFAnalysis.numFailedModes);
	    QA_XLS_COL13_VALUE.AppendFormat("%d", _forwardUpperModalDistortionAnglePFAnalysis.numFailedModes);
	    QA_XLS_COL14_VALUE.AppendFormat("%d", _forwardUpperModalDistortionAngleXPositionPFAnalysis.numFailedModes);
	    QA_XLS_COL15_VALUE.AppendFormat("%d", _reverseLowerModalDistortionAnglePFAnalysis.numFailedModes);
	    QA_XLS_COL16_VALUE.AppendFormat("%d", _reverseLowerModalDistortionAngleXPositionPFAnalysis.numFailedModes);
	    QA_XLS_COL17_VALUE.AppendFormat("%d", _reverseUpperModalDistortionAnglePFAnalysis.numFailedModes);
	    QA_XLS_COL18_VALUE.AppendFormat("%d", _reverseUpperModalDistortionAngleXPositionPFAnalysis.numFailedModes);
		QA_XLS_COL19_VALUE.AppendFormat("%d", _maxForwardLowerModeBDCAreaXLengthPFAnalysis.numFailedModes);
		QA_XLS_COL20_VALUE.AppendFormat("%d", _maxForwardUpperModeBDCAreaXLengthPFAnalysis.numFailedModes);
	    QA_XLS_COL21_VALUE.AppendFormat("%d", _maxForwardLowerModeBDCAreaPFAnalysis.numFailedModes);
	    QA_XLS_COL22_VALUE.AppendFormat("%d", _maxForwardUpperModeBDCAreaPFAnalysis.numFailedModes);
	    QA_XLS_COL23_VALUE.AppendFormat("%d", _sumForwardLowerModeBDCAreasPFAnalysis.numFailedModes);
	    QA_XLS_COL24_VALUE.AppendFormat("%d", _sumForwardUpperModeBDCAreasPFAnalysis.numFailedModes);
	    QA_XLS_COL25_VALUE.AppendFormat("%d", _numForwardLowerModeBDCAreasPFAnalysis.numFailedModes);
	    QA_XLS_COL26_VALUE.AppendFormat("%d", _numForwardUpperModeBDCAreasPFAnalysis.numFailedModes);
		QA_XLS_COL27_VALUE.AppendFormat("%d", _maxReverseLowerModeBDCAreaXLengthPFAnalysis.numFailedModes);
		QA_XLS_COL28_VALUE.AppendFormat("%d", _maxReverseUpperModeBDCAreaXLengthPFAnalysis.numFailedModes);
	    QA_XLS_COL29_VALUE.AppendFormat("%d", _maxReverseLowerModeBDCAreaPFAnalysis.numFailedModes);
	    QA_XLS_COL30_VALUE.AppendFormat("%d", _maxReverseUpperModeBDCAreaPFAnalysis.numFailedModes);
	    QA_XLS_COL31_VALUE.AppendFormat("%d", _sumReverseLowerModeBDCAreasPFAnalysis.numFailedModes);
	    QA_XLS_COL32_VALUE.AppendFormat("%d", _sumReverseUpperModeBDCAreasPFAnalysis.numFailedModes);
	    QA_XLS_COL33_VALUE.AppendFormat("%d", _numReverseLowerModeBDCAreasPFAnalysis.numFailedModes);
	    QA_XLS_COL34_VALUE.AppendFormat("%d", _numReverseUpperModeBDCAreasPFAnalysis.numFailedModes);
	    QA_XLS_COL35_VALUE.AppendFormat("%d", _maxForwardLowerModeSlopePFAnalysis.numFailedModes);
	    QA_XLS_COL36_VALUE.AppendFormat("%d", _maxForwardUpperModeSlopePFAnalysis.numFailedModes);
	    QA_XLS_COL37_VALUE.AppendFormat("%d", _maxReverseLowerModeSlopePFAnalysis.numFailedModes);
	    QA_XLS_COL38_VALUE.AppendFormat("%d", _maxReverseUpperModeSlopePFAnalysis.numFailedModes);
	    QA_XLS_COL39_VALUE.AppendFormat("%d", _middleLineRMSValuesPFAnalysis.numFailedModes);
	    QA_XLS_COL40_VALUE.AppendFormat("%d", _minMiddleLineSlopePFAnalysis.numFailedModes);
	    QA_XLS_COL41_VALUE.AppendFormat("%d", _maxMiddleLineSlopePFAnalysis.numFailedModes);


	    rowArray.RemoveAll();
	    rowArray.Add(QA_XLS_ROWTITLE4);
	    rowArray.Add(QA_XLS_COL2_VALUE);
	    rowArray.Add(QA_XLS_COL3_VALUE);
	    rowArray.Add(QA_XLS_COL4_VALUE);
	    rowArray.Add(QA_XLS_COL5_VALUE);
	    rowArray.Add(QA_XLS_COL6_VALUE);
	    rowArray.Add(QA_XLS_COL7_VALUE);
	    rowArray.Add(QA_XLS_COL8_VALUE);
	    rowArray.Add(QA_XLS_COL9_VALUE);
	    rowArray.Add(QA_XLS_COL10_VALUE);
 	    rowArray.Add(QA_XLS_COL11_VALUE);
 	    rowArray.Add(QA_XLS_COL12_VALUE);
 	    rowArray.Add(QA_XLS_COL13_VALUE);
 	    rowArray.Add(QA_XLS_COL14_VALUE);
 	    rowArray.Add(QA_XLS_COL15_VALUE);
 	    rowArray.Add(QA_XLS_COL16_VALUE);
 	    rowArray.Add(QA_XLS_COL17_VALUE);
 	    rowArray.Add(QA_XLS_COL18_VALUE);
 	    rowArray.Add(QA_XLS_COL19_VALUE);
 	    rowArray.Add(QA_XLS_COL20_VALUE);
 	    rowArray.Add(QA_XLS_COL21_VALUE);
 	    rowArray.Add(QA_XLS_COL22_VALUE);
 	    rowArray.Add(QA_XLS_COL23_VALUE);
 	    rowArray.Add(QA_XLS_COL24_VALUE);
 	    rowArray.Add(QA_XLS_COL25_VALUE);
 	    rowArray.Add(QA_XLS_COL26_VALUE);
 	    rowArray.Add(QA_XLS_COL27_VALUE);
 	    rowArray.Add(QA_XLS_COL28_VALUE);
 	    rowArray.Add(QA_XLS_COL29_VALUE);
 	    rowArray.Add(QA_XLS_COL30_VALUE);
 	    rowArray.Add(QA_XLS_COL31_VALUE);
 	    rowArray.Add(QA_XLS_COL32_VALUE);
 	    rowArray.Add(QA_XLS_COL33_VALUE);
 	    rowArray.Add(QA_XLS_COL34_VALUE);
 	    rowArray.Add(QA_XLS_COL35_VALUE);
 	    rowArray.Add(QA_XLS_COL36_VALUE);
 	    rowArray.Add(QA_XLS_COL37_VALUE);
 	    rowArray.Add(QA_XLS_COL38_VALUE);
 	    rowArray.Add(QA_XLS_COL39_VALUE);
 	    rowArray.Add(QA_XLS_COL40_VALUE);
 	    rowArray.Add(QA_XLS_COL41_VALUE);
		passFailResults.addRow(rowArray);


	    ////////////////////////////////////////
	    ///
	    //	thresholds
	    //
        QA_XLS_COL2_VALUE = "";
        QA_XLS_COL3_VALUE = "";
        QA_XLS_COL4_VALUE = "";
        QA_XLS_COL5_VALUE = "";
        QA_XLS_COL6_VALUE = "";
        QA_XLS_COL7_VALUE = "";
        QA_XLS_COL8_VALUE = "";
        QA_XLS_COL9_VALUE = "";
        QA_XLS_COL10_VALUE = "";
        QA_XLS_COL11_VALUE = "";
        QA_XLS_COL12_VALUE = "";
        QA_XLS_COL13_VALUE = "";
        QA_XLS_COL14_VALUE = "";
        QA_XLS_COL15_VALUE = "";
        QA_XLS_COL16_VALUE = "";
        QA_XLS_COL17_VALUE = "";
        QA_XLS_COL18_VALUE = "";
        QA_XLS_COL19_VALUE = "";
        QA_XLS_COL20_VALUE = "";
        QA_XLS_COL21_VALUE = "";
        QA_XLS_COL22_VALUE = "";
        QA_XLS_COL23_VALUE = "";
        QA_XLS_COL24_VALUE = "";
        QA_XLS_COL25_VALUE = "";
        QA_XLS_COL26_VALUE = "";
        QA_XLS_COL27_VALUE = "";
        QA_XLS_COL28_VALUE = "";
        QA_XLS_COL29_VALUE = "";
        QA_XLS_COL30_VALUE = "";
        QA_XLS_COL31_VALUE = "";
        QA_XLS_COL32_VALUE = "";
        QA_XLS_COL33_VALUE = "";
        QA_XLS_COL34_VALUE = "";
        QA_XLS_COL35_VALUE = "";
        QA_XLS_COL36_VALUE = "";
        QA_XLS_COL37_VALUE = "";
        QA_XLS_COL38_VALUE = "";
        QA_XLS_COL39_VALUE = "";
        QA_XLS_COL40_VALUE = "";
        QA_XLS_COL41_VALUE = "";


		QA_XLS_COL2_VALUE.AppendFormat(" > %g", _maxUpperModeBordersRemovedPFAnalysis.threshold);
		QA_XLS_COL3_VALUE.AppendFormat(" > %g", _maxLowerModeBordersRemovedPFAnalysis.threshold);
		QA_XLS_COL4_VALUE.AppendFormat(" > %g", _maxMiddleLineRemovedPFAnalysis.threshold);
	    QA_XLS_COL5_VALUE.AppendFormat(" > %g", _maxForwardModeWidthPFAnalysis.threshold);
	    QA_XLS_COL6_VALUE.AppendFormat(" > %g", _maxReverseModeWidthPFAnalysis.threshold);
	    QA_XLS_COL7_VALUE.AppendFormat(" > %g", _meanPercWorkingRegionPFAnalysis.threshold);
	    QA_XLS_COL8_VALUE.AppendFormat(" > %g", _minPercWorkingRegionPFAnalysis.threshold);
	    QA_XLS_COL9_VALUE.AppendFormat(" < %g", _minForwardModeWidthPFAnalysis.threshold);
	    QA_XLS_COL10_VALUE.AppendFormat(" < %g", _minReverseModeWidthPFAnalysis.threshold);
	    QA_XLS_COL11_VALUE.AppendFormat(" > %g", _forwardLowerModalDistortionAnglePFAnalysis.threshold);
		QA_XLS_COL12_VALUE.AppendFormat(" > %g", _forwardLowerModalDistortionAngleXPositionPFAnalysis.threshold);
	    QA_XLS_COL13_VALUE.AppendFormat(" > %g", _forwardUpperModalDistortionAnglePFAnalysis.threshold);
		QA_XLS_COL14_VALUE.AppendFormat(" > %g", _forwardUpperModalDistortionAngleXPositionPFAnalysis.threshold);
	    QA_XLS_COL15_VALUE.AppendFormat(" > %g", _reverseLowerModalDistortionAnglePFAnalysis.threshold);
		QA_XLS_COL16_VALUE.AppendFormat(" > %g", _reverseLowerModalDistortionAngleXPositionPFAnalysis.threshold);
	    QA_XLS_COL17_VALUE.AppendFormat(" > %g", _reverseUpperModalDistortionAnglePFAnalysis.threshold);
		QA_XLS_COL18_VALUE.AppendFormat(" > %g", _reverseUpperModalDistortionAngleXPositionPFAnalysis.threshold);
	    QA_XLS_COL19_VALUE.AppendFormat(" > %g", _maxForwardLowerModeBDCAreaXLengthPFAnalysis.threshold);
	    QA_XLS_COL20_VALUE.AppendFormat(" > %g", _maxForwardUpperModeBDCAreaXLengthPFAnalysis.threshold);
	    QA_XLS_COL21_VALUE.AppendFormat(" > %g", _maxForwardLowerModeBDCAreaPFAnalysis.threshold);
	    QA_XLS_COL22_VALUE.AppendFormat(" > %g", _maxForwardUpperModeBDCAreaPFAnalysis.threshold);
	    QA_XLS_COL23_VALUE.AppendFormat(" > %g", _sumForwardLowerModeBDCAreasPFAnalysis.threshold);
	    QA_XLS_COL24_VALUE.AppendFormat(" > %g", _sumForwardUpperModeBDCAreasPFAnalysis.threshold);
	    QA_XLS_COL25_VALUE.AppendFormat(" > %g", _numForwardLowerModeBDCAreasPFAnalysis.threshold);
	    QA_XLS_COL26_VALUE.AppendFormat(" > %g", _numForwardUpperModeBDCAreasPFAnalysis.threshold);
	    QA_XLS_COL27_VALUE.AppendFormat(" > %g", _maxReverseLowerModeBDCAreaXLengthPFAnalysis.threshold);
	    QA_XLS_COL28_VALUE.AppendFormat(" > %g", _maxReverseUpperModeBDCAreaXLengthPFAnalysis.threshold);
	    QA_XLS_COL29_VALUE.AppendFormat(" > %g", _maxReverseLowerModeBDCAreaPFAnalysis.threshold);
	    QA_XLS_COL30_VALUE.AppendFormat(" > %g", _maxReverseUpperModeBDCAreaPFAnalysis.threshold);
	    QA_XLS_COL31_VALUE.AppendFormat(" > %g", _sumReverseLowerModeBDCAreasPFAnalysis.threshold);
	    QA_XLS_COL32_VALUE.AppendFormat(" > %g", _sumReverseUpperModeBDCAreasPFAnalysis.threshold);
	    QA_XLS_COL33_VALUE.AppendFormat(" > %g", _numReverseLowerModeBDCAreasPFAnalysis.threshold);
	    QA_XLS_COL34_VALUE.AppendFormat(" > %g", _numReverseUpperModeBDCAreasPFAnalysis.threshold);
	    QA_XLS_COL35_VALUE.AppendFormat(" > %g", _maxForwardLowerModeSlopePFAnalysis.threshold);
	    QA_XLS_COL36_VALUE.AppendFormat(" > %g", _maxForwardUpperModeSlopePFAnalysis.threshold);
	    QA_XLS_COL37_VALUE.AppendFormat(" > %g", _maxReverseLowerModeSlopePFAnalysis.threshold);
	    QA_XLS_COL38_VALUE.AppendFormat(" > %g", _maxReverseUpperModeSlopePFAnalysis.threshold);
	    QA_XLS_COL39_VALUE.AppendFormat(" > %g", _middleLineRMSValuesPFAnalysis.threshold);
	    QA_XLS_COL40_VALUE.AppendFormat(" < %g", _minMiddleLineSlopePFAnalysis.threshold);
	    QA_XLS_COL41_VALUE.AppendFormat(" > %g", _maxMiddleLineSlopePFAnalysis.threshold);

	    rowArray.RemoveAll();
	    rowArray.Add(QA_XLS_ROWTITLE5);
	    rowArray.Add(QA_XLS_COL2_VALUE);
	    rowArray.Add(QA_XLS_COL3_VALUE);
	    rowArray.Add(QA_XLS_COL4_VALUE);
	    rowArray.Add(QA_XLS_COL5_VALUE);
	    rowArray.Add(QA_XLS_COL6_VALUE);
	    rowArray.Add(QA_XLS_COL7_VALUE);
	    rowArray.Add(QA_XLS_COL8_VALUE);
	    rowArray.Add(QA_XLS_COL9_VALUE);
	    rowArray.Add(QA_XLS_COL10_VALUE);
		rowArray.Add(QA_XLS_COL11_VALUE);
		rowArray.Add(QA_XLS_COL12_VALUE);
		rowArray.Add(QA_XLS_COL13_VALUE);
  		rowArray.Add(QA_XLS_COL14_VALUE);
  		rowArray.Add(QA_XLS_COL15_VALUE);
  		rowArray.Add(QA_XLS_COL16_VALUE);
  		rowArray.Add(QA_XLS_COL17_VALUE);
  		rowArray.Add(QA_XLS_COL18_VALUE);
  		rowArray.Add(QA_XLS_COL19_VALUE);
  		rowArray.Add(QA_XLS_COL20_VALUE);
		rowArray.Add(QA_XLS_COL21_VALUE);
		rowArray.Add(QA_XLS_COL22_VALUE);
		rowArray.Add(QA_XLS_COL23_VALUE);
  		rowArray.Add(QA_XLS_COL24_VALUE);
  		rowArray.Add(QA_XLS_COL25_VALUE);
  		rowArray.Add(QA_XLS_COL26_VALUE);
  		rowArray.Add(QA_XLS_COL27_VALUE);
  		rowArray.Add(QA_XLS_COL28_VALUE);
  		rowArray.Add(QA_XLS_COL29_VALUE);
  		rowArray.Add(QA_XLS_COL30_VALUE);
  		rowArray.Add(QA_XLS_COL31_VALUE);
  		rowArray.Add(QA_XLS_COL32_VALUE);
  		rowArray.Add(QA_XLS_COL33_VALUE);
  		rowArray.Add(QA_XLS_COL34_VALUE);
  		rowArray.Add(QA_XLS_COL35_VALUE);
  		rowArray.Add(QA_XLS_COL36_VALUE);
  		rowArray.Add(QA_XLS_COL37_VALUE);
  		rowArray.Add(QA_XLS_COL38_VALUE);
  		rowArray.Add(QA_XLS_COL39_VALUE);
  		rowArray.Add(QA_XLS_COL40_VALUE);
  		rowArray.Add(QA_XLS_COL41_VALUE);
	    passFailResults.addRow(rowArray);


        // Calculation parameters
        QA_XLS_COL1_VALUE = "";
        QA_XLS_COL2_VALUE = "";
        QA_XLS_COL3_VALUE = "";
        QA_XLS_COL4_VALUE = "";
        QA_XLS_COL5_VALUE = "";
        QA_XLS_COL6_VALUE = "";
        QA_XLS_COL7_VALUE = "";
        QA_XLS_COL8_VALUE = "";
        QA_XLS_COL9_VALUE = "";
        QA_XLS_COL10_VALUE = "";
        QA_XLS_COL11_VALUE = "";
        QA_XLS_COL12_VALUE = "";
        QA_XLS_COL13_VALUE = "";
        QA_XLS_COL14_VALUE = "";
        QA_XLS_COL15_VALUE = "";
        QA_XLS_COL16_VALUE = "";
        QA_XLS_COL17_VALUE = "";
        QA_XLS_COL18_VALUE = "";
        QA_XLS_COL19_VALUE = "";
        QA_XLS_COL20_VALUE = "";
        QA_XLS_COL21_VALUE = "";
        QA_XLS_COL22_VALUE = "";
        QA_XLS_COL23_VALUE = "";
        QA_XLS_COL24_VALUE = "";
        QA_XLS_COL25_VALUE = "";
        QA_XLS_COL26_VALUE = "";
        QA_XLS_COL27_VALUE = "";
        QA_XLS_COL28_VALUE = "";
        QA_XLS_COL29_VALUE = "";
        QA_XLS_COL30_VALUE = "";
        QA_XLS_COL31_VALUE = "";
        QA_XLS_COL32_VALUE = "";
        QA_XLS_COL33_VALUE = "";
        QA_XLS_COL34_VALUE = "";
        QA_XLS_COL35_VALUE = "";
        QA_XLS_COL36_VALUE = "";
        QA_XLS_COL37_VALUE = "";
        QA_XLS_COL38_VALUE = "";
        QA_XLS_COL39_VALUE = "";
        QA_XLS_COL40_VALUE = "";
        QA_XLS_COL41_VALUE = "";


	    rowArray.RemoveAll();
	    rowArray.Add(QA_XLS_COL1_VALUE);
	    rowArray.Add(QA_XLS_COL2_VALUE);
	    rowArray.Add(QA_XLS_COL3_VALUE);
	    rowArray.Add(QA_XLS_COL4_VALUE);
	    rowArray.Add(QA_XLS_COL5_VALUE);
	    rowArray.Add(QA_XLS_COL6_VALUE);
	    rowArray.Add(QA_XLS_COL7_VALUE);
	    rowArray.Add(QA_XLS_COL8_VALUE);
	    rowArray.Add(QA_XLS_COL9_VALUE);
	    rowArray.Add(QA_XLS_COL10_VALUE);
	    rowArray.Add(QA_XLS_COL11_VALUE);
	    rowArray.Add(QA_XLS_COL12_VALUE);
	    rowArray.Add(QA_XLS_COL13_VALUE);
	    rowArray.Add(QA_XLS_COL14_VALUE);
	    rowArray.Add(QA_XLS_COL15_VALUE);
	    rowArray.Add(QA_XLS_COL16_VALUE);
	    rowArray.Add(QA_XLS_COL17_VALUE);
	    rowArray.Add(QA_XLS_COL18_VALUE);
	    rowArray.Add(QA_XLS_COL19_VALUE);
	    rowArray.Add(QA_XLS_COL20_VALUE);
	    rowArray.Add(QA_XLS_COL21_VALUE);
	    rowArray.Add(QA_XLS_COL22_VALUE);
	    rowArray.Add(QA_XLS_COL23_VALUE);
	    rowArray.Add(QA_XLS_COL24_VALUE);
	    rowArray.Add(QA_XLS_COL25_VALUE);
	    rowArray.Add(QA_XLS_COL26_VALUE);
	    rowArray.Add(QA_XLS_COL27_VALUE);
	    rowArray.Add(QA_XLS_COL28_VALUE);
	    rowArray.Add(QA_XLS_COL29_VALUE);
	    rowArray.Add(QA_XLS_COL30_VALUE);
	    rowArray.Add(QA_XLS_COL31_VALUE);
	    rowArray.Add(QA_XLS_COL32_VALUE);
	    rowArray.Add(QA_XLS_COL33_VALUE);
	    rowArray.Add(QA_XLS_COL34_VALUE);
	    rowArray.Add(QA_XLS_COL35_VALUE);
	    rowArray.Add(QA_XLS_COL36_VALUE);
	    rowArray.Add(QA_XLS_COL37_VALUE);
	    rowArray.Add(QA_XLS_COL38_VALUE);
	    rowArray.Add(QA_XLS_COL39_VALUE);
	    rowArray.Add(QA_XLS_COL40_VALUE);
	    rowArray.Add(QA_XLS_COL41_VALUE);
        passFailResults.addRow(rowArray);

        // add space
	    passFailResults.addEmptyLine();


	    //
	    // Find location of failing statistic(s)
	    //

	    // row title
	    rowArray.RemoveAll();
	    rowArray.Add("Index");
	    passFailResults.addRow(rowArray);


	    // final rows - Modes
	    for(unsigned int i = 0;  i<(unsigned int)_numForwardModes; i++)
	    {
            QA_XLS_COL1_VALUE = "";
            QA_XLS_COL2_VALUE = "";
            QA_XLS_COL3_VALUE = "";
            QA_XLS_COL4_VALUE = "";
            QA_XLS_COL5_VALUE = "";
            QA_XLS_COL6_VALUE = "";
            QA_XLS_COL7_VALUE = "";
            QA_XLS_COL8_VALUE = "";
            QA_XLS_COL9_VALUE = "";
            QA_XLS_COL10_VALUE = "";
            QA_XLS_COL11_VALUE = "";
            QA_XLS_COL12_VALUE = "";
            QA_XLS_COL13_VALUE = "";
            QA_XLS_COL14_VALUE = "";
            QA_XLS_COL15_VALUE = "";
            QA_XLS_COL16_VALUE = "";
            QA_XLS_COL17_VALUE = "";
            QA_XLS_COL18_VALUE = "";
            QA_XLS_COL19_VALUE = "";
            QA_XLS_COL20_VALUE = "";
            QA_XLS_COL21_VALUE = "";
            QA_XLS_COL22_VALUE = "";
            QA_XLS_COL23_VALUE = "";
            QA_XLS_COL24_VALUE = "";
            QA_XLS_COL25_VALUE = "";
            QA_XLS_COL26_VALUE = "";
            QA_XLS_COL27_VALUE = "";
            QA_XLS_COL28_VALUE = "";
            QA_XLS_COL29_VALUE = "";
            QA_XLS_COL30_VALUE = "";
            QA_XLS_COL31_VALUE = "";
            QA_XLS_COL32_VALUE = "";
            QA_XLS_COL33_VALUE = "";
            QA_XLS_COL34_VALUE = "";
            QA_XLS_COL35_VALUE = "";
            QA_XLS_COL36_VALUE = "";
            QA_XLS_COL37_VALUE = "";
            QA_XLS_COL38_VALUE = "";
            QA_XLS_COL39_VALUE = "";
            QA_XLS_COL40_VALUE = "";
            QA_XLS_COL41_VALUE = "";

            // Index location
		    QA_XLS_COL1_VALUE.AppendFormat(" %d", i);


		    // Max Forward Mode Width
            std::vector<double>::iterator i_f = std::find(
                _maxForwardModeWidthPFAnalysis.failedModes.begin(),
                _maxForwardModeWidthPFAnalysis.failedModes.end(),
                i );
		    if(i < _maxForwardModeWidths.size() && i_f != _maxForwardModeWidthPFAnalysis.failedModes.end() )
			    QA_XLS_COL5_VALUE.AppendFormat("%f", _maxForwardModeWidths[i]);

		    // Max Reverse Mode Width
            i_f = std::find(
                _maxReverseModeWidthPFAnalysis.failedModes.begin(),
                _maxReverseModeWidthPFAnalysis.failedModes.end(),
                i );
		    if(i < _maxReverseModeWidths.size() && i_f != _maxReverseModeWidthPFAnalysis.failedModes.end() )
			    QA_XLS_COL6_VALUE.AppendFormat("%f", _maxReverseModeWidths[i]);

		    // Mean Percentage Working Region
            i_f = std::find(
                _meanPercWorkingRegionPFAnalysis.failedModes.begin(),
                _meanPercWorkingRegionPFAnalysis.failedModes.end(),
                i );
		    if(i < _meanPercWorkingRegions.size() && i_f != _meanPercWorkingRegionPFAnalysis.failedModes.end() )
			    QA_XLS_COL7_VALUE.AppendFormat("%f", _meanPercWorkingRegions[i]);

		    // Min Percentage Working Region
            i_f = std::find(
                _minPercWorkingRegionPFAnalysis.failedModes.begin(),
                _minPercWorkingRegionPFAnalysis.failedModes.end(),
                i );
		    if(i < _minPercWorkingRegions.size() && i_f != _minPercWorkingRegionPFAnalysis.failedModes.end() )
			    QA_XLS_COL8_VALUE.AppendFormat("%f", _minPercWorkingRegions[i]);

			// Min Forward Mode Width
            i_f = std::find(
				_minForwardModeWidthPFAnalysis.failedModes.begin(),
                _minForwardModeWidthPFAnalysis.failedModes.end(),
                i );
		    if(i < _minForwardModeWidths.size() && i_f != _minForwardModeWidthPFAnalysis.failedModes.end() )
			    QA_XLS_COL9_VALUE.AppendFormat("%f", _minForwardModeWidths[i]);

			// Min Reverse Mode Width
            i_f = std::find(
				_minReverseModeWidthPFAnalysis.failedModes.begin(),
                _minReverseModeWidthPFAnalysis.failedModes.end(),
                i );
		    if(i < _minReverseModeWidths.size() && i_f != _minReverseModeWidthPFAnalysis.failedModes.end() )
			    QA_XLS_COL10_VALUE.AppendFormat("%f", _minReverseModeWidths[i]);


		    // forward lower modal_distortion
            i_f = std::find(
                _forwardLowerModalDistortionAnglePFAnalysis.failedModes.begin(),
                _forwardLowerModalDistortionAnglePFAnalysis.failedModes.end(),
                i );
		    if( i < _forwardLowerModalDistortionAngles.size()
            && i < _forwardLowerModalDistortionAngleXPositions.size()
            && i_f != _forwardLowerModalDistortionAnglePFAnalysis.failedModes.end())
            {
			    QA_XLS_COL11_VALUE.AppendFormat("%f", _forwardLowerModalDistortionAngles[i]);
			    QA_XLS_COL12_VALUE.AppendFormat("%d", _forwardLowerModalDistortionAngleXPositions[i]);
            }

		    // forward upper modal_distortion
            i_f = std::find(
                _forwardUpperModalDistortionAnglePFAnalysis.failedModes.begin(),
                _forwardUpperModalDistortionAnglePFAnalysis.failedModes.end(),
                i );
		    if( i < _forwardUpperModalDistortionAngles.size()
            && i < _forwardUpperModalDistortionAngleXPositions.size()
            && i_f != _forwardUpperModalDistortionAnglePFAnalysis.failedModes.end())
            {
			    QA_XLS_COL13_VALUE.AppendFormat("%f", _forwardUpperModalDistortionAngles[i]);
			    QA_XLS_COL14_VALUE.AppendFormat("%d", _forwardUpperModalDistortionAngleXPositions[i]);
            }

		    // reverse lower modal_distortion
            i_f = std::find(
                _reverseLowerModalDistortionAnglePFAnalysis.failedModes.begin(),
                _reverseLowerModalDistortionAnglePFAnalysis.failedModes.end(),
                i );
		    if( i < _reverseLowerModalDistortionAngles.size()
            && i < _reverseLowerModalDistortionAngleXPositions.size()
            && i_f != _reverseLowerModalDistortionAnglePFAnalysis.failedModes.end())
            {
			    QA_XLS_COL15_VALUE.AppendFormat("%f", _reverseLowerModalDistortionAngles[i]);
			    QA_XLS_COL16_VALUE.AppendFormat("%d", _reverseLowerModalDistortionAngleXPositions[i]);
            }

		    // reverse upper modal_distortion
            i_f = std::find(
                _reverseUpperModalDistortionAnglePFAnalysis.failedModes.begin(),
                _reverseUpperModalDistortionAnglePFAnalysis.failedModes.end(),
                i );
		    if( i < _reverseUpperModalDistortionAngles.size()
            && i < _reverseUpperModalDistortionAngleXPositions.size()
            && i_f != _reverseUpperModalDistortionAnglePFAnalysis.failedModes.end())
            {
			    QA_XLS_COL17_VALUE.AppendFormat("%f", _reverseUpperModalDistortionAngles[i]);
			    QA_XLS_COL18_VALUE.AppendFormat("%d", _reverseUpperModalDistortionAngleXPositions[i]);
            }

			// Lower Max Forward Bdc X-Length
            i_f = std::find(
                _maxForwardLowerModeBDCAreaXLengthPFAnalysis.failedModes.begin(),
                _maxForwardLowerModeBDCAreaXLengthPFAnalysis.failedModes.end(),
                i );
		    if(i < _maxForwardLowerModeBDCAreaXLengths.size() && i_f != _maxForwardLowerModeBDCAreaXLengthPFAnalysis.failedModes.end() )
			    QA_XLS_COL19_VALUE.AppendFormat("%f", _maxForwardLowerModeBDCAreaXLengths[i]);

			// Upper Max Forward Bdc X-Length
            i_f = std::find(
                _maxForwardUpperModeBDCAreaXLengthPFAnalysis.failedModes.begin(),
                _maxForwardUpperModeBDCAreaXLengthPFAnalysis.failedModes.end(),
                i );
		    if(i < _maxForwardUpperModeBDCAreaXLengths.size() && i_f != _maxForwardUpperModeBDCAreaXLengthPFAnalysis.failedModes.end() )
			    QA_XLS_COL20_VALUE.AppendFormat("%f", _maxForwardUpperModeBDCAreaXLengths[i]);

		    // Lower Max Forward Bdc Area
            i_f = std::find(
                _maxForwardLowerModeBDCAreaPFAnalysis.failedModes.begin(),
                _maxForwardLowerModeBDCAreaPFAnalysis.failedModes.end(),
                i );
		    if(i < _maxForwardLowerModeBDCAreas.size() && i_f != _maxForwardLowerModeBDCAreaPFAnalysis.failedModes.end() )
			    QA_XLS_COL21_VALUE.AppendFormat("%f", _maxForwardLowerModeBDCAreas[i]);

		    // Upper Max Forward Bdc Area
            i_f = std::find(
                _maxForwardUpperModeBDCAreaPFAnalysis.failedModes.begin(),
                _maxForwardUpperModeBDCAreaPFAnalysis.failedModes.end(),
                i );
		    if(i < _maxForwardUpperModeBDCAreas.size() && i_f != _maxForwardUpperModeBDCAreaPFAnalysis.failedModes.end() )
			    QA_XLS_COL22_VALUE.AppendFormat("%f", _maxForwardUpperModeBDCAreas[i]);

		    // Lower Sum of Forward Bdc Areas
            i_f = std::find(
                _sumForwardLowerModeBDCAreasPFAnalysis.failedModes.begin(),
                _sumForwardLowerModeBDCAreasPFAnalysis.failedModes.end(),
                i );
		    if(i < _sumForwardLowerModeBDCAreas.size() && i_f != _sumForwardLowerModeBDCAreasPFAnalysis.failedModes.end() )
			    QA_XLS_COL23_VALUE.AppendFormat("%f", _sumForwardLowerModeBDCAreas[i]);

		    // Upper Sum of Forward Bdc Areas
            i_f = std::find(
                _sumForwardUpperModeBDCAreasPFAnalysis.failedModes.begin(),
                _sumForwardUpperModeBDCAreasPFAnalysis.failedModes.end(),
                i );
		    if(i < _sumForwardUpperModeBDCAreas.size() && i_f != _sumForwardUpperModeBDCAreasPFAnalysis.failedModes.end() )
			    QA_XLS_COL24_VALUE.AppendFormat("%f", _sumForwardUpperModeBDCAreas[i]);

		    // Lower Number of Forward Bdcs
            i_f = std::find(
                _numForwardLowerModeBDCAreasPFAnalysis.failedModes.begin(),
                _numForwardLowerModeBDCAreasPFAnalysis.failedModes.end(),
                i );
		    if(i < _numForwardLowerModeBDCAreaSamples.size() && i_f != _numForwardLowerModeBDCAreasPFAnalysis.failedModes.end() )
			    QA_XLS_COL25_VALUE.AppendFormat("%f", _numForwardLowerModeBDCAreaSamples[i]);

		    // Upper Number of Forward Bdcs
            i_f = std::find(
                _numForwardUpperModeBDCAreasPFAnalysis.failedModes.begin(),
                _numForwardUpperModeBDCAreasPFAnalysis.failedModes.end(),
                i );
		    if(i < _numForwardUpperModeBDCAreaSamples.size() && i_f != _numForwardUpperModeBDCAreasPFAnalysis.failedModes.end() )
			    QA_XLS_COL26_VALUE.AppendFormat("%f", _numForwardUpperModeBDCAreaSamples[i]);

			// Lower Max Reverse Bdc X-Length
            i_f = std::find(
                _maxReverseLowerModeBDCAreaXLengthPFAnalysis.failedModes.begin(),
                _maxReverseLowerModeBDCAreaXLengthPFAnalysis.failedModes.end(),
                i );
		    if(i < _maxReverseLowerModeBDCAreaXLengths.size() && i_f != _maxReverseLowerModeBDCAreaXLengthPFAnalysis.failedModes.end() )
			    QA_XLS_COL27_VALUE.AppendFormat("%f", _maxReverseLowerModeBDCAreaXLengths[i]);

			// Upper Max Reverse Bdc X-Length
            i_f = std::find(
                _maxReverseUpperModeBDCAreaXLengthPFAnalysis.failedModes.begin(),
                _maxReverseUpperModeBDCAreaXLengthPFAnalysis.failedModes.end(),
                i );
		    if(i < _maxReverseUpperModeBDCAreaXLengths.size() && i_f != _maxReverseUpperModeBDCAreaXLengthPFAnalysis.failedModes.end() )
			    QA_XLS_COL28_VALUE.AppendFormat("%f", _maxReverseUpperModeBDCAreaXLengths[i]);

		    // Lower Max Reverse Bdc Area
            i_f = std::find(
                _maxReverseLowerModeBDCAreaPFAnalysis.failedModes.begin(),
                _maxReverseLowerModeBDCAreaPFAnalysis.failedModes.end(),
                i );
		    if(i < _maxReverseLowerModeBDCAreas.size() && i_f != _maxReverseLowerModeBDCAreaPFAnalysis.failedModes.end() )
			    QA_XLS_COL29_VALUE.AppendFormat("%f", _maxReverseLowerModeBDCAreas[i]);

		    // Upper Max Reverse Bdc Area
            i_f = std::find(
                _maxReverseUpperModeBDCAreaPFAnalysis.failedModes.begin(),
                _maxReverseUpperModeBDCAreaPFAnalysis.failedModes.end(),
                i );
		    if(i < _maxReverseUpperModeBDCAreas.size() && i_f != _maxReverseUpperModeBDCAreaPFAnalysis.failedModes.end() )
			    QA_XLS_COL30_VALUE.AppendFormat("%f", _maxReverseUpperModeBDCAreas[i]);

		    // Lower Sum of Reverse Bdc Areas
            i_f = std::find(
                _sumReverseLowerModeBDCAreasPFAnalysis.failedModes.begin(),
                _sumReverseLowerModeBDCAreasPFAnalysis.failedModes.end(),
                i );
		    if(i < _sumReverseLowerModeBDCAreas.size() && i_f != _sumReverseLowerModeBDCAreasPFAnalysis.failedModes.end() )
			    QA_XLS_COL31_VALUE.AppendFormat("%f", _sumReverseLowerModeBDCAreas[i]);

		    // Upper Sum of Reverse Bdc Areas
            i_f = std::find(
                _sumReverseUpperModeBDCAreasPFAnalysis.failedModes.begin(),
                _sumReverseUpperModeBDCAreasPFAnalysis.failedModes.end(),
                i );
		    if(i < _sumReverseUpperModeBDCAreas.size() && i_f != _sumReverseUpperModeBDCAreasPFAnalysis.failedModes.end() )
			    QA_XLS_COL32_VALUE.AppendFormat("%f", _sumReverseUpperModeBDCAreas[i]);

		    // Lower Number of Reverse Bdcs
            i_f = std::find(
                _numReverseLowerModeBDCAreasPFAnalysis.failedModes.begin(),
                _numReverseLowerModeBDCAreasPFAnalysis.failedModes.end(),
                i );
		    if(i < _numReverseLowerModeBDCAreaSamples.size() && i_f != _numReverseLowerModeBDCAreasPFAnalysis.failedModes.end() )
			    QA_XLS_COL33_VALUE.AppendFormat("%f", _numReverseLowerModeBDCAreaSamples[i]);
			
		    // Upper Number of Reverse Bdcs
            i_f = std::find(
                _numReverseUpperModeBDCAreasPFAnalysis.failedModes.begin(),
                _numReverseUpperModeBDCAreasPFAnalysis.failedModes.end(),
                i );
		    if(i < _numReverseUpperModeBDCAreaSamples.size() && i_f != _numReverseUpperModeBDCAreasPFAnalysis.failedModes.end() )
			    QA_XLS_COL34_VALUE.AppendFormat("%f", _numReverseUpperModeBDCAreaSamples[i]);

		    // Max Forward Lower Slope
            i_f = std::find(
                _maxForwardLowerModeSlopePFAnalysis.failedModes.begin(),
                _maxForwardLowerModeSlopePFAnalysis.failedModes.end(),
                i );
		    if(i < _maxForwardLowerModeSlopes.size() && i_f != _maxForwardLowerModeSlopePFAnalysis.failedModes.end() )
			    QA_XLS_COL35_VALUE.AppendFormat("%f", _maxForwardLowerModeSlopes[i]);

		    // Max Forward Upper Slope
            i_f = std::find(
                _maxForwardUpperModeSlopePFAnalysis.failedModes.begin(),
                _maxForwardUpperModeSlopePFAnalysis.failedModes.end(),
                i );
		    if(i < _maxForwardUpperModeSlopes.size() && i_f != _maxForwardUpperModeSlopePFAnalysis.failedModes.end() )
			    QA_XLS_COL36_VALUE.AppendFormat("%f", _maxForwardUpperModeSlopes[i]);

		    // Max Reverse Lower Slope
            i_f = std::find(
                _maxReverseLowerModeSlopePFAnalysis.failedModes.begin(),
                _maxReverseLowerModeSlopePFAnalysis.failedModes.end(),
                i );
		    if(i < _maxReverseLowerModeSlopes.size() && i_f != _maxReverseLowerModeSlopePFAnalysis.failedModes.end() )
			    QA_XLS_COL37_VALUE.AppendFormat("%f", _maxReverseLowerModeSlopes[i]);

		    // Max Reverse Upper Slope
            i_f = std::find(
                _maxReverseUpperModeSlopePFAnalysis.failedModes.begin(),
                _maxReverseUpperModeSlopePFAnalysis.failedModes.end(),
                i );
		    if(i < _maxReverseUpperModeSlopes.size() && i_f != _maxReverseUpperModeSlopePFAnalysis.failedModes.end() )
			    QA_XLS_COL38_VALUE.AppendFormat("%f", _maxReverseUpperModeSlopes[i]);

		    // Max Middle Line RMS Value
            i_f = std::find(
                _middleLineRMSValuesPFAnalysis.failedModes.begin(),
                _middleLineRMSValuesPFAnalysis.failedModes.end(),
                i );
		    if(i < _middleLineRMSValues.size() && i_f != _middleLineRMSValuesPFAnalysis.failedModes.end() )
			    QA_XLS_COL39_VALUE.AppendFormat("%f", _middleLineRMSValues[i]);

		    // Min Middle Line Slope
            i_f = std::find(
                _minMiddleLineSlopePFAnalysis.failedModes.begin(),
                _minMiddleLineSlopePFAnalysis.failedModes.end(),
                i );
		    if(i < _middleLineSlopes.size() && i_f != _minMiddleLineSlopePFAnalysis.failedModes.end() )
			    QA_XLS_COL40_VALUE.AppendFormat("%f", _middleLineSlopes[i]);

		    // Max Middle Line Slope
            i_f = std::find(
                _maxMiddleLineSlopePFAnalysis.failedModes.begin(),
                _maxMiddleLineSlopePFAnalysis.failedModes.end(),
                i );
		    if(i < _middleLineSlopes.size() && i_f != _maxMiddleLineSlopePFAnalysis.failedModes.end() )
			    QA_XLS_COL41_VALUE.AppendFormat("%f", _middleLineSlopes[i]);

			rowArray.RemoveAll();
	        rowArray.Add(QA_XLS_COL1_VALUE);
	        rowArray.Add(QA_XLS_COL2_VALUE);
	        rowArray.Add(QA_XLS_COL3_VALUE);
	        rowArray.Add(QA_XLS_COL4_VALUE);
	        rowArray.Add(QA_XLS_COL5_VALUE);
	        rowArray.Add(QA_XLS_COL6_VALUE);
	        rowArray.Add(QA_XLS_COL7_VALUE);
	        rowArray.Add(QA_XLS_COL8_VALUE);
	        rowArray.Add(QA_XLS_COL9_VALUE);
	        rowArray.Add(QA_XLS_COL10_VALUE);
	        rowArray.Add(QA_XLS_COL11_VALUE);
 	        rowArray.Add(QA_XLS_COL12_VALUE);
 	        rowArray.Add(QA_XLS_COL13_VALUE);
 	        rowArray.Add(QA_XLS_COL14_VALUE);
 	        rowArray.Add(QA_XLS_COL15_VALUE);
 	        rowArray.Add(QA_XLS_COL16_VALUE);
 	        rowArray.Add(QA_XLS_COL17_VALUE);
 	        rowArray.Add(QA_XLS_COL18_VALUE);
 	        rowArray.Add(QA_XLS_COL19_VALUE);
 	        rowArray.Add(QA_XLS_COL20_VALUE);
	        rowArray.Add(QA_XLS_COL21_VALUE);
 	        rowArray.Add(QA_XLS_COL22_VALUE);
 	        rowArray.Add(QA_XLS_COL23_VALUE);
 	        rowArray.Add(QA_XLS_COL24_VALUE);
 	        rowArray.Add(QA_XLS_COL25_VALUE);
 	        rowArray.Add(QA_XLS_COL26_VALUE);
 	        rowArray.Add(QA_XLS_COL27_VALUE);
 	        rowArray.Add(QA_XLS_COL28_VALUE);
 	        rowArray.Add(QA_XLS_COL29_VALUE);
 	        rowArray.Add(QA_XLS_COL30_VALUE);
 	        rowArray.Add(QA_XLS_COL31_VALUE);
 	        rowArray.Add(QA_XLS_COL32_VALUE);
 	        rowArray.Add(QA_XLS_COL33_VALUE);
 	        rowArray.Add(QA_XLS_COL34_VALUE);
 	        rowArray.Add(QA_XLS_COL35_VALUE);
 	        rowArray.Add(QA_XLS_COL36_VALUE);
 	        rowArray.Add(QA_XLS_COL37_VALUE);
 	        rowArray.Add(QA_XLS_COL38_VALUE);
 	        rowArray.Add(QA_XLS_COL39_VALUE);
 	        rowArray.Add(QA_XLS_COL40_VALUE);
 	        rowArray.Add(QA_XLS_COL41_VALUE);
	        passFailResults.addRow(rowArray);

	    }
    }
    else // qaWasRun == false
    {
        CStringArray error_msg_row;
        error_msg_row.Add("Error : Function called before Pass Fail Analysis completed.");
        passFailResults.addRow(error_msg_row);
    }

	return err;
}

PassFailThresholds
SuperModeMapAnalysis::
getPassFailThresholds()
{
	PassFailThresholds thresholds;

	thresholds._maxModeWidth					= CG_REG->get_DSDBR01_SMPF_max_mode_width(_superModeNumber);
	thresholds._minModeWidth					= CG_REG->get_DSDBR01_SMPF_min_mode_width(_superModeNumber);
	thresholds._maxModeBordersRemoved			= CG_REG->get_DSDBR01_SMPF_max_mode_borders_removed(_superModeNumber);
	thresholds._maxModeBDCArea					= CG_REG->get_DSDBR01_SMPF_max_mode_bdc_area(_superModeNumber);
	thresholds._maxModeBDCAreaXLength			= CG_REG->get_DSDBR01_SMPF_max_mode_bdc_area_x_length(_superModeNumber);
	thresholds._meanPercWorkingRegion			= CG_REG->get_DSDBR01_SMPF_mean_perc_working_region(_superModeNumber);
	thresholds._minPercWorkingRegion			= CG_REG->get_DSDBR01_SMPF_min_perc_working_region(_superModeNumber);
	thresholds._modalDistortionAngle			= CG_REG->get_DSDBR01_SMPF_modal_distortion_angle(_superModeNumber);
	thresholds._modalDistortionAngleXPosition	= CG_REG->get_DSDBR01_SMPF_modal_distortion_angle_x_pos(_superModeNumber);
	thresholds._numModeBDCAreas					= CG_REG->get_DSDBR01_SMPF_num_mode_bdc_areas(_superModeNumber);
	thresholds._sumModeBDCAreas					= CG_REG->get_DSDBR01_SMPF_sum_mode_bdc_areas(_superModeNumber);
	//thresholds._maxPrGap			= CG_REG->get_DSDBR01_SMPF_continuity_spacing(_superModeNumber);
	thresholds._maxMiddleLineSlope				= CG_REG->get_DSDBR01_SMPF_max_middle_line_slope(_superModeNumber);
	thresholds._minMiddleLineSlope				= CG_REG->get_DSDBR01_SMPF_min_middle_line_slope(_superModeNumber);
	thresholds._middleLineRMSValue				= CG_REG->get_DSDBR01_SMPF_max_middle_line_rms_value(_superModeNumber);
	thresholds._maxModeLineSlope				= CG_REG->get_DSDBR01_SMPF_max_mode_line_slope(_superModeNumber);

	return thresholds;
}

//
//
///	End of SuperModeMapAnalysis.cpp
///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2004 PXIT Ltd. All Rights Reserved

///////////////////////////////////////////////////////////////////////////////
///
//	Class	:
//
//	Contents:
//
//	Author	:
//
///
///////////////////////////////////////////////////////////////////////////////
///
//

#include "stdafx.h"


#include <algorithm>

#include "OverallModeMapAnalysis.h"
#include "CGRegValues.h"
#include "PassFailCollated.h"

//
/////////////////////////////////////////////////////////////////////
//

#define new DEBUG_NEW


#define MIN_NUMBER_OF_MODES 5

#define RESULTS_NO_ERROR				0
#define RESULTS_GENERAL_ERROR			1
#define RESULTS_NO_MODEMAP_DATA			2
#define RESULTS_PO_TOO_LOW				4
#define RESULTS_PO_TOO_HIGH				8
#define RESULTS_TOO_MANY_JUMPS			16
#define RESULTS_TOO_FEW_JUMPS			32
#define RESULTS_INVALID_NUMBER_OF_LINES 64
#define RESULTS_INVALID_LINES_REMOVED	128
#define RESULTS_LINES_MISSING			256


/////////////////////////////////////////////////////////////////////

//
///	ctor and dtor
//

//OverallModeMapAnalysis::
//OverallModeMapAnalysis(const vector<CDSDBRSuperMode>& superModes,
//					int				xAxisLength,
//					int				yAxisLength,
//					vector<double>&	continuityValues,
//					short			superModesRemoved,
//					CString			resultsDir,
//					CString			laserId,
//					CString			dateTimeStamp)
//{
//	_superModes = superModes;
//	_numSuperModes = (int)_superModes.size();
//
//	_numSuperModesRemoved = superModesRemoved;
//
//	_qaResultsAbsFilePath =	createQaResultsAbsFilePath(resultsDir,
//													laserId,
//													dateTimeStamp);
//
//	_passFailResultsAbsFilePath = createPassFailResultsAbsFilePath(resultsDir,
//															laserId,
//															dateTimeStamp);
//
//	_collatedPassFailResultsAbsFilePath = createPassFailCollateResultsAbsFilePath(resultsDir,
//																				laserId,
//																				dateTimeStamp);
//	_resultsDir		= resultsDir;
//	_laserId		= laserId;
//	_dateTimeStamp	= dateTimeStamp;
//
//	_xAxisLength = xAxisLength;
//	_yAxisLength = yAxisLength;
//
//	_continuityValues = continuityValues;
//
//	init();
//}

OverallModeMapAnalysis::
OverallModeMapAnalysis()
{
	init();
}

OverallModeMapAnalysis::
~OverallModeMapAnalysis(void)
{
}

void
OverallModeMapAnalysis::
init()
{
	////////////////////////////////////////////////
	///
	//	QA Data
	//
	_modeWidths.clear();
	_maxModeWidths.clear();
	_minModeWidths.clear();
	_meanModeWidths.clear();
	_sumModeWidths.clear();
	_varModeWidths.clear();
	_stdDevModeWidths.clear();
	_medianModeWidths.clear();
	_numModeWidthSamples.clear();

	///////////////////////////////////////

	_modeUpperSlopes.clear();
	_maxModeUpperSlopes.clear();
	_minModeUpperSlopes.clear();
	_meanModeUpperSlopes.clear();
	_sumModeUpperSlopes.clear();
	_varModeUpperSlopes.clear();
	_stdDevModeUpperSlopes.clear();
	_medianModeUpperSlopes.clear();
	_numModeUpperSlopeSamples.clear();

	///////////////////////////////////////

	_modeLowerSlopes.clear();
	_maxModeLowerSlopes.clear();
	_minModeLowerSlopes.clear();
	_meanModeLowerSlopes.clear();
	_sumModeLowerSlopes.clear();
	_varModeLowerSlopes.clear();
	_stdDevModeLowerSlopes.clear();
	_medianModeLowerSlopes.clear();
	_numModeLowerSlopeSamples.clear();

	////////////////////////////////////////

	_lowerModalDistortionAngles.clear();
	_upperModalDistortionAngles.clear();

	///////////////////////////////////////

	_lowerModeBDCAreas.clear();
	_lowerMaxModeBDCAreas.clear();
	_lowerMinModeBDCAreas.clear();
	_lowerMeanModeBDCAreas.clear();
	_lowerSumModeBDCAreas.clear();
	_lowerVarModeBDCAreas.clear();
	_lowerStdDevModeBDCAreas.clear();
	_lowerMedianModeBDCAreas.clear();
	_lowerNumModeBDCAreaSamples.clear();

	_upperModeBDCAreas.clear();
	_upperMaxModeBDCAreas.clear();
	_upperMinModeBDCAreas.clear();
	_upperMeanModeBDCAreas.clear();
	_upperSumModeBDCAreas.clear();
	_upperVarModeBDCAreas.clear();
	_upperStdDevModeBDCAreas.clear();
	_upperMedianModeBDCAreas.clear();
	_upperNumModeBDCAreaSamples.clear();

	///////////////////////////////////////

	_lowerModeBDCAreaXLengths.clear();
	_lowerMaxModeBDCAreaXLengths.clear();
	_lowerMinModeBDCAreaXLengths.clear();
	_lowerMeanModeBDCAreaXLengths.clear();
	_lowerSumModeBDCAreaXLengths.clear();
	_lowerVarModeBDCAreaXLengths.clear();
	_lowerStdDevModeBDCAreaXLengths.clear();
	_lowerMedianModeBDCAreaXLengths.clear();
	_lowerNumModeBDCAreaXLengthSamples.clear();

	_upperModeBDCAreaXLengths.clear();
	_upperMaxModeBDCAreaXLengths.clear();
	_upperMinModeBDCAreaXLengths.clear();
	_upperMeanModeBDCAreaXLengths.clear();
	_upperSumModeBDCAreaXLengths.clear();
	_upperVarModeBDCAreaXLengths.clear();
	_upperStdDevModeBDCAreaXLengths.clear();
	_upperMedianModeBDCAreaXLengths.clear();
	_upperNumModeBDCAreaXLengthSamples.clear();

	///////////////////////////////////////

	_middleLineRMSValues.clear();
	_middleLineSlopes.clear();

	///////////////////////////////////////

	_maxContinuityValue		= 0;
	_minContinuityValue		= 0;
	_meanContinuityValues	= 0;
	_sumContinuityValues	= 0;
	_varContinuityValues	= 0;
	_stdDevContinuityValues	= 0;
	_medianContinuityValues	= 0;
	_numContinuityValues	= 0;
	_maxPrGap				= 0;
	_maxLowerPr				= 0;
	_minUpperPr				= 0;

	///////////////////////////////////////

	_qaAnalysisComplete = false;

	//
	//////////////////////////////////////////////////////

	//////////////////////////////////////////////////////
	///
	//	Pass Fail Analysis
	//
	_passFailAnalysisComplete = false;

	_maxModeBordersRemovedAnalysis.init();
	_maxModeWidthPFAnalysis.init();
	_minModeWidthPFAnalysis.init();

	_lowerMaxModeBDCAreaPFAnalysis.init();
	_lowerMaxModeBDCAreaXLengthPFAnalysis.init();
	_lowerSumModeBDCAreasPFAnalysis.init();
	_lowerNumModeBDCAreasPFAnalysis.init();

	_upperMaxModeBDCAreaPFAnalysis.init();
	_upperMaxModeBDCAreaXLengthPFAnalysis.init();
	_upperSumModeBDCAreasPFAnalysis.init();
	_upperNumModeBDCAreasPFAnalysis.init();

	_middleLineRMSValuesPFAnalysis.init();
	_maxMiddleLineSlopePFAnalysis.init();
	_minMiddleLineSlopePFAnalysis.init();
	_maxPrGapPFAnalysis.init();
	_maxLowerPrPFAnalysis.init();
	_minUpperPrPFAnalysis.init();


	_lowerMaxModeLineSlopePFAnalysis.init();
	_upperMaxModeLineSlopePFAnalysis.init();
	_lowerMinModeLineSlopePFAnalysis.init();
	_upperMinModeLineSlopePFAnalysis.init();

	//
	//////////////////////////////////////////////////////

	_screeningPassed = false;

	_qaDetectedError = RESULTS_NO_ERROR;

}

CString
OverallModeMapAnalysis::
createQaResultsAbsFilePath(CString	resultsDir,
						CString		laserId,
						CString		dateTimeStamp)
{
	CString absFilePath = "";

	absFilePath += resultsDir;
	absFilePath += "\\";
	absFilePath += QA_METRICS_FILE_NAME;
	absFilePath += USCORE;
	absFilePath += "OverallMap";
	absFilePath += USCORE;
	absFilePath += LASER_TYPE_ID_DSDBR01_CSTRING;
	absFilePath += USCORE;
	absFilePath += laserId;
	absFilePath += USCORE;
	absFilePath += dateTimeStamp;
	absFilePath += CSV;

	return absFilePath;
}
CString
OverallModeMapAnalysis::
createPassFailResultsAbsFilePath(CString	resultsDir,
								CString		laserId,
								CString		dateTimeStamp)
{
	CString absFilePath = "";

	absFilePath += resultsDir;
	absFilePath += "\\";
	absFilePath += PASSFAIL_FILE_NAME;
	absFilePath += USCORE;
	absFilePath += "OverallMap";
	absFilePath += USCORE;
	absFilePath += LASER_TYPE_ID_DSDBR01_CSTRING;
	absFilePath += USCORE;
	absFilePath += laserId;
	absFilePath += USCORE;
	absFilePath += dateTimeStamp;
	absFilePath += CSV;

	return absFilePath;
}

CString
OverallModeMapAnalysis::
createPassFailCollateResultsAbsFilePath(CString	resultsDir,
								CString		laserId,
								CString		dateTimeStamp)
{
	CString absFilePath = "";

	absFilePath += resultsDir;
	absFilePath += "\\";
	absFilePath += COLLATED_PASSFAIL_FILE_NAME;
	absFilePath += USCORE;
	absFilePath += LASER_TYPE_ID_DSDBR01_CSTRING;
	absFilePath += USCORE;
	absFilePath += laserId;
	absFilePath += USCORE;
	absFilePath += dateTimeStamp;
	absFilePath += CSV;

	return absFilePath;
}
/////////////////////////////////////////////////////////////////////

OverallModeMapAnalysis::rcode
OverallModeMapAnalysis::
runAnalysis(vector<CDSDBRSuperMode>* p_superModes,
			int				xAxisLength,
			int				yAxisLength,
			vector<double>& continuityValues,
			short			superModesRemoved,
			CString			resultsDir,
			CString			laserId,
			CString			dateTimeStamp)
{
	_p_superModes = p_superModes;
	_numSuperModes = (int)p_superModes->size();

	_numSuperModesRemoved = superModesRemoved;

	_qaResultsAbsFilePath =	createQaResultsAbsFilePath(resultsDir,
													laserId,
													dateTimeStamp);

	_passFailResultsAbsFilePath = createPassFailResultsAbsFilePath(resultsDir,
															laserId,
															dateTimeStamp);

	_collatedPassFailResultsAbsFilePath = createPassFailCollateResultsAbsFilePath(resultsDir,
																				laserId,
																				dateTimeStamp);
	_resultsDir		= resultsDir;
	_laserId		= laserId;
	_dateTimeStamp	= dateTimeStamp;

	_xAxisLength = xAxisLength;
	_yAxisLength = yAxisLength;

	_continuityValues = continuityValues;


	rcode err = rcode::ok;

	err = runQaAnalysis();
	if(err == rcode::ok)
		err = runPassFailAnalysis();

	err = writeQaResultsToFile();

	CLoseGridFile passFailResults(_passFailResultsAbsFilePath);
	passFailResults.createFolderAndFile();

	err = writePassFailResultsToFile( passFailResults );

	if(err == rcode::ok)
	{
		PassFailCollated::instance()->setFileParameters(_resultsDir, _laserId, _dateTimeStamp);
		PassFailCollated::instance()->clearSuperModes();
		PassFailCollated::instance()->setOverallModeMapAnalysis(this);
	}

	return err;
}

OverallModeMapAnalysis::rcode
OverallModeMapAnalysis::
runQaAnalysis()
{
	rcode err = rcode::ok;

	for(int i=0; i<_numSuperModes; i++)
	{	
		(*_p_superModes)[i].runModeAnalysis();
	}

	getModeWidths();
	getMaxModeWidths();
	getMinModeWidths();
	getMeanModeWidths();
	getSumModeWidths();
	getVarModeWidths();
	getStdDevModeWidths();
	getMedianModeWidths();
	getNumModeWidthSamples();

	getModeUpperSlopes();
	getMaxModeUpperSlopes();
	getMinModeUpperSlopes();
	getMeanModeUpperSlopes();
	getSumModeUpperSlopes();
	getVarModeUpperSlopes();
	getStdDevModeUpperSlopes();
	getMedianModeUpperSlopes();
	getNumModeUpperSlopeSamples();

	getModeLowerSlopes();
	getMaxModeLowerSlopes();
	getMinModeLowerSlopes();
	getMeanModeLowerSlopes();
	getSumModeLowerSlopes();
	getVarModeLowerSlopes();
	getStdDevModeLowerSlopes();
	getMedianModeLowerSlopes();
	getNumModeLowerSlopeSamples();

	getModalDistortionAngles();
	getModalDistortionAngleXPositions();

	getModeBDCAreas();
	getMaxModeBDCAreas();
	getMinModeBDCAreas();
	getMeanModeBDCAreas();
	getSumModeBDCAreas();
	getVarModeBDCAreas();
	getStdDevModeBDCAreas();
	getMedianModeBDCAreas();
	getNumModeBDCAreaSamples();

	getMaxModeBDCAreaXLengths();

	//////////////////////////////////

	getMiddleLineRMSValues();

	/////////////////////////////////

	getMiddleLineSlopes();

	/////////////////////////////////

	getMaxContinuityValue();
	getMinContinuityValue();
	getMeanContinuityValues();
	getSumContinuityValues();
	getVarContinuityValues();
	getStdDevContinuityValues();
	getMedianContinuityValues();
	getNumContinuityValues();
	getMaxPrGap();
	getMaxLowerPr();
	getMinUpperPr();
	//
	///
	////////////////////////////////////////////////////////////////////////////////////
	///
	//


	CString error_message = "";
    int error_number = RESULTS_NO_ERROR;
    if( _numSuperModes < MIN_NUMBER_OF_MODES )
	{
        _qaDetectedError |= RESULTS_GENERAL_ERROR | RESULTS_INVALID_NUMBER_OF_LINES;
	}

	getQAError(_qaDetectedError, error_number, error_message);

    if( _qaDetectedError > RESULTS_NO_ERROR ) 
		_screeningPassed = false;


	_qaAnalysisComplete = true;

	return err;
}

OverallModeMapAnalysis::rcode
OverallModeMapAnalysis::
runPassFailAnalysis()
{
	rcode err = rcode::ok;

	if(_qaAnalysisComplete)
	{
		_screeningPassed = true;

		_smPassFailThresholds = getPassFailThresholds();

		// Set thresholds for this mode
		_maxModeBordersRemovedAnalysis.threshold			= _smPassFailThresholds._maxModeBordersRemoved;
		_maxModeWidthPFAnalysis.threshold					= _smPassFailThresholds._maxModeWidth;
		_minModeWidthPFAnalysis.threshold					= _smPassFailThresholds._minModeWidth;

		_lowerMaxModeBDCAreaPFAnalysis.threshold			= _smPassFailThresholds._maxModeBDCArea;
		_lowerMaxModeBDCAreaXLengthPFAnalysis.threshold		= _smPassFailThresholds._maxModeBDCAreaXLength;
		_lowerSumModeBDCAreasPFAnalysis.threshold			= _smPassFailThresholds._sumModeBDCAreas;
		_lowerNumModeBDCAreasPFAnalysis.threshold			= _smPassFailThresholds._numModeBDCAreas;

		_upperMaxModeBDCAreaPFAnalysis.threshold			= _smPassFailThresholds._maxModeBDCArea;
		_upperMaxModeBDCAreaXLengthPFAnalysis.threshold		= _smPassFailThresholds._maxModeBDCAreaXLength;
		_upperSumModeBDCAreasPFAnalysis.threshold			= _smPassFailThresholds._sumModeBDCAreas;
		_upperNumModeBDCAreasPFAnalysis.threshold			= _smPassFailThresholds._numModeBDCAreas;

		_lowerModalDistortionAnglePFAnalysis.threshold			= _smPassFailThresholds._modalDistortionAngle;
		_lowerModalDistortionAngleXPositionPFAnalysis.threshold	= _smPassFailThresholds._modalDistortionAngleXPosition;

		_upperModalDistortionAnglePFAnalysis.threshold			= _smPassFailThresholds._modalDistortionAngle;
		_upperModalDistortionAngleXPositionPFAnalysis.threshold	= _smPassFailThresholds._modalDistortionAngleXPosition;

		_middleLineRMSValuesPFAnalysis.threshold			= _smPassFailThresholds._middleLineRMSValue;
		_maxMiddleLineSlopePFAnalysis.threshold				= _smPassFailThresholds._maxMiddleLineSlope;
		_minMiddleLineSlopePFAnalysis.threshold				= _smPassFailThresholds._minMiddleLineSlope;
		_maxPrGapPFAnalysis.threshold						= _smPassFailThresholds._maxPrGap;
		_maxLowerPrPFAnalysis.threshold						= _smPassFailThresholds._maxLowerPr;
		_minUpperPrPFAnalysis.threshold						= _smPassFailThresholds._minUpperPr;

		_lowerMaxModeLineSlopePFAnalysis.threshold			= _smPassFailThresholds._maxModeLineSlope;
		_upperMaxModeLineSlopePFAnalysis.threshold			= _smPassFailThresholds._maxModeLineSlope;

		_lowerMinModeLineSlopePFAnalysis.threshold			= _smPassFailThresholds._minModeLineSlope;
		_upperMinModeLineSlopePFAnalysis.threshold			= _smPassFailThresholds._minModeLineSlope;

		// run pass fail
		_maxModeWidthPFAnalysis.runPassFailWithUpperLimit(_maxModeWidths);
		_minModeWidthPFAnalysis.runPassFailWithLowerLimit(_minModeWidths);

		_lowerMaxModeBDCAreaPFAnalysis.runPassFailWithUpperLimit(_lowerMaxModeBDCAreas);
		_lowerMaxModeBDCAreaXLengthPFAnalysis.runPassFailWithUpperLimit(_lowerMaxModeBDCAreaXLengths);
		_lowerSumModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_lowerSumModeBDCAreas);
		_lowerNumModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_lowerNumModeBDCAreaSamples);

		_upperMaxModeBDCAreaPFAnalysis.runPassFailWithUpperLimit(_upperMaxModeBDCAreas);
		_upperMaxModeBDCAreaXLengthPFAnalysis.runPassFailWithUpperLimit(_upperMaxModeBDCAreaXLengths);
		_upperSumModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_upperSumModeBDCAreas);
		_upperNumModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_upperNumModeBDCAreaSamples);

		_lowerModalDistortionAnglePFAnalysis.runPassFailWithUpperLimit(_lowerModalDistortionAngles);
		_lowerModalDistortionAngleXPositionPFAnalysis.runPassFailWithUpperLimit(_lowerModalDistortionAngleXPositions);

		_upperModalDistortionAnglePFAnalysis.runPassFailWithUpperLimit(_upperModalDistortionAngles);
		_upperModalDistortionAngleXPositionPFAnalysis.runPassFailWithUpperLimit(_upperModalDistortionAngleXPositions);

		_middleLineRMSValuesPFAnalysis.runPassFailWithUpperLimit(_middleLineRMSValues);
		_maxMiddleLineSlopePFAnalysis.runPassFailWithUpperLimit(_middleLineSlopes);
		_minMiddleLineSlopePFAnalysis.runPassFailWithLowerLimit(_middleLineSlopes);

		_maxPrGapPFAnalysis.runPassFailWithUpperLimit(_maxPrGap);
		_maxLowerPrPFAnalysis.runPassFailWithUpperLimit(_maxLowerPr);
		_minUpperPrPFAnalysis.runPassFailWithLowerLimit(_minUpperPr);

		_lowerMaxModeLineSlopePFAnalysis.runPassFailWithUpperLimit(_maxModeLowerSlopes);
		_upperMaxModeLineSlopePFAnalysis.runPassFailWithUpperLimit(_maxModeUpperSlopes);
		_lowerMinModeLineSlopePFAnalysis.runPassFailWithLowerLimit(_minModeLowerSlopes);
		_upperMinModeLineSlopePFAnalysis.runPassFailWithLowerLimit(_minModeUpperSlopes);


		if(	!(_numSuperModesRemoved <= _maxModeBordersRemovedAnalysis.threshold)||
			!(_maxModeWidthPFAnalysis.pass)||
			!(_minModeWidthPFAnalysis.pass)||
			!(_lowerMaxModeBDCAreaPFAnalysis.pass)||
			!(_lowerMaxModeBDCAreaXLengthPFAnalysis.pass)||
			!(_lowerSumModeBDCAreasPFAnalysis.pass)||
			!(_lowerNumModeBDCAreasPFAnalysis.pass)||
			!(_upperMaxModeBDCAreaPFAnalysis.pass)||
			!(_upperMaxModeBDCAreaXLengthPFAnalysis.pass)||
			!(_upperSumModeBDCAreasPFAnalysis.pass)||
			!(_upperNumModeBDCAreasPFAnalysis.pass)||
			!(_lowerModalDistortionAnglePFAnalysis.pass)||
			!(_lowerModalDistortionAngleXPositionPFAnalysis.pass)||
			!(_upperModalDistortionAnglePFAnalysis.pass)||
			!(_upperModalDistortionAngleXPositionPFAnalysis.pass)||
			!(_middleLineRMSValuesPFAnalysis.pass)||
			!(_maxMiddleLineSlopePFAnalysis.pass)||
			!(_minMiddleLineSlopePFAnalysis.pass)||
			!(_lowerMaxModeLineSlopePFAnalysis.pass)||
			!(_upperMaxModeLineSlopePFAnalysis.pass)||
			!(_lowerMinModeLineSlopePFAnalysis.pass)||
			!(_upperMinModeLineSlopePFAnalysis.pass)||
			!(_maxPrGapPFAnalysis.pass)||
			!(_maxLowerPrPFAnalysis.pass)||
			!(_minUpperPrPFAnalysis.pass))
		{
			_screeningPassed = false;
		}

	}

	//////////////////////////////////////////////////////////////////////////////////
	///
	// analyse results
	//
    if( !(_maxModeWidthPFAnalysis.pass) )
        _qaDetectedError |= RESULTS_GENERAL_ERROR | RESULTS_LINES_MISSING;
    if( _numSuperModes < MIN_NUMBER_OF_MODES )
        _qaDetectedError |= RESULTS_GENERAL_ERROR | RESULTS_INVALID_NUMBER_OF_LINES;

    if( _qaDetectedError > RESULTS_NO_ERROR ) 
		_screeningPassed = false;
	//
	////////////////////////////////////////////////////////////////////////////////////


	_passFailAnalysisComplete = true;

	return err;
}

void
OverallModeMapAnalysis::
getQAError(int		qaError,
		  int&		errorNumber,
		  CString&	errorMsg)
{
	errorMsg	= "";
	errorNumber = 0;

    if( qaError & RESULTS_GENERAL_ERROR )
    {
        errorMsg.Append("Error(s) detected. ");
        errorNumber |= RESULTS_GENERAL_ERROR;
    }
    if( qaError & RESULTS_NO_MODEMAP_DATA )
    {
        errorMsg.Append("Mode map data not available. ");
        errorNumber |= RESULTS_NO_MODEMAP_DATA;
    }
    if( qaError & RESULTS_PO_TOO_LOW )
    {
        errorMsg.Append("Invalid laser screening run - optical power is too low. ");
        errorNumber |= RESULTS_PO_TOO_LOW;
    }
    if( qaError & RESULTS_PO_TOO_HIGH )
    {
        errorMsg.Append("Invalid laser screening run - optical power is too high. ");
        errorNumber |= RESULTS_PO_TOO_HIGH;
    }
    if( qaError & RESULTS_TOO_MANY_JUMPS )
    {
        errorMsg.Append("Invalid laser screening run - too many laser mode jumps found. ");
        errorNumber |= RESULTS_TOO_MANY_JUMPS;
    }
    if( qaError & RESULTS_TOO_FEW_JUMPS )
    {
        errorMsg.Append("Invalid laser screening run - too few laser mode jumps found. ");
        errorNumber |= RESULTS_TOO_FEW_JUMPS;
    }
    if( qaError & RESULTS_INVALID_NUMBER_OF_LINES )
    {
        errorMsg.Append("Invalid laser screening run - invalid number of mode boundaries detected. ");
        errorNumber |= RESULTS_INVALID_NUMBER_OF_LINES;
    }
    if( qaError & RESULTS_INVALID_LINES_REMOVED )
    {
        errorMsg.Append("Invalid mode boundaries detected. ");
        errorNumber |= RESULTS_INVALID_LINES_REMOVED;
    }
    if( qaError & RESULTS_LINES_MISSING )
    {
        errorMsg.Append("Undetected mode boundaries. ");
        errorNumber |= RESULTS_LINES_MISSING;
    }

	CString errNumMsg = "";
	if( errorNumber > RESULTS_NO_ERROR )
		errNumMsg.AppendFormat("%d,", errorNumber);
	else
		errNumMsg.Append(",");
	errNumMsg.Append(errorMsg);


	errorMsg = errNumMsg;
}
//
////////////////////////////////////////////////////////////////////////////////////////
///
//	Data Gathering Section - gather analysis data of the supermodes
//

//
/////////////////////////////////////////////////////////////////////////////
///
//	Mode Widths
void
OverallModeMapAnalysis::
getModeWidths()
{
	if(_modeWidths.size() == 0)
	{
		for(int i=0; i<_numSuperModes; i++)
		{
			Vector modeWidths;
			(*_p_superModes)[i]._modeAnalysis.getModeWidths(modeWidths);

			_modeWidths.push_back(modeWidths);
		}
	}
}

void
OverallModeMapAnalysis::
getMaxModeWidths()
{
	if(_maxModeWidths.size() == 0)
	{
		for(int i=0; i<_numSuperModes; i++)
			_maxModeWidths.push_back((*_p_superModes)[i]._modeAnalysis.getModeWidthsStatistic(Vector::max));
	}
}
void
OverallModeMapAnalysis::
getMinModeWidths()
{
	if(_minModeWidths.size() == 0)
	{
		for(int i=0; i<_numSuperModes; i++)
			_minModeWidths.push_back((*_p_superModes)[i]._modeAnalysis.getModeWidthsStatistic(Vector::min));
	}
}
void
OverallModeMapAnalysis::
getMeanModeWidths()
{
	if(_meanModeWidths.size() == 0)
	{
		for(int i=0; i<_numSuperModes; i++)
			_meanModeWidths.push_back((*_p_superModes)[i]._modeAnalysis.getModeWidthsStatistic(Vector::mean));
	}
}
void
OverallModeMapAnalysis::
getSumModeWidths()
{
	if(_sumModeWidths.size() == 0)
	{
		for(int i=0; i<_numSuperModes; i++)
			_sumModeWidths.push_back((*_p_superModes)[i]._modeAnalysis.getModeWidthsStatistic(Vector::sum));
	}
}
void
OverallModeMapAnalysis::
getVarModeWidths()
{
	if(_varModeWidths.size() == 0)
	{
		for(int i=0; i<_numSuperModes; i++)
			_varModeWidths.push_back((*_p_superModes)[i]._modeAnalysis.getModeWidthsStatistic(Vector::variance));
	}
}
void
OverallModeMapAnalysis::
getStdDevModeWidths()
{
	if(_stdDevModeWidths.size() == 0)
	{
		for(int i=0; i<_numSuperModes; i++)
			_stdDevModeWidths.push_back((*_p_superModes)[i]._modeAnalysis.getModeWidthsStatistic(Vector::stdDev));
	}
}
void
OverallModeMapAnalysis::
getMedianModeWidths()
{
	if(_medianModeWidths.size() == 0)
	{
		for(int i=0; i<_numSuperModes; i++)
			_medianModeWidths.push_back((*_p_superModes)[i]._modeAnalysis.getModeWidthsStatistic(Vector::median));
	}
}
void
OverallModeMapAnalysis::
getNumModeWidthSamples()
{
	if(_numModeWidthSamples.size() == 0)
	{
		for(int i=0; i<_numSuperModes; i++)
			_numModeWidthSamples.push_back((*_p_superModes)[i]._modeAnalysis.getModeWidthsStatistic(Vector::count));
	}
}

//
/////////////////////////////////////////////////////////////////////////////
///
//	Mode Upper Slopes
//
void
OverallModeMapAnalysis::
getModeUpperSlopes()
{
	if(_modeUpperSlopes.size() == 0)
	{
		for(int i=0; i<_numSuperModes; i++)
		{
			Vector modeUpperSlopes;
			(*_p_superModes)[i]._modeAnalysis.getModeUpperSlopes(modeUpperSlopes);

			_modeUpperSlopes.push_back(modeUpperSlopes);
		}
	}
}

void
OverallModeMapAnalysis::
getMaxModeUpperSlopes()
{
	if(_maxModeUpperSlopes.size() == 0)
	{
		for(int i=0; i<_numSuperModes; i++)
			_maxModeUpperSlopes.push_back((*_p_superModes)[i]._modeAnalysis.getModeUpperSlopesStatistic(Vector::max));
	}
}
void
OverallModeMapAnalysis::
getMinModeUpperSlopes()
{
	if(_minModeUpperSlopes.size() == 0)
	{
		for(int i=0; i<_numSuperModes; i++)
			_minModeUpperSlopes.push_back((*_p_superModes)[i]._modeAnalysis.getModeUpperSlopesStatistic(Vector::min));
	}
}
void
OverallModeMapAnalysis::
getMeanModeUpperSlopes()
{
	if(_meanModeUpperSlopes.size() == 0)
	{
		for(int i=0; i<_numSuperModes; i++)
			_meanModeUpperSlopes.push_back((*_p_superModes)[i]._modeAnalysis.getModeUpperSlopesStatistic(Vector::mean));
	}
}
void
OverallModeMapAnalysis::
getSumModeUpperSlopes()
{
	if(_sumModeUpperSlopes.size() == 0)
	{
		for(int i=0; i<_numSuperModes; i++)
			_sumModeUpperSlopes.push_back((*_p_superModes)[i]._modeAnalysis.getModeUpperSlopesStatistic(Vector::sum));
	}
}
void
OverallModeMapAnalysis::
getVarModeUpperSlopes()
{
	if(_varModeUpperSlopes.size() == 0)
	{
		for(int i=0; i<_numSuperModes; i++)
			_varModeUpperSlopes.push_back((*_p_superModes)[i]._modeAnalysis.getModeUpperSlopesStatistic(Vector::variance));
	}
}
void
OverallModeMapAnalysis::
getStdDevModeUpperSlopes()
{
	if(_stdDevModeUpperSlopes.size() == 0)
	{
		for(int i=0; i<_numSuperModes; i++)
			_stdDevModeUpperSlopes.push_back((*_p_superModes)[i]._modeAnalysis.getModeUpperSlopesStatistic(Vector::stdDev));
	}
}
void
OverallModeMapAnalysis::
getMedianModeUpperSlopes()
{
	if(_medianModeUpperSlopes.size() == 0)
	{
		for(int i=0; i<_numSuperModes; i++)
			_medianModeUpperSlopes.push_back((*_p_superModes)[i]._modeAnalysis.getModeUpperSlopesStatistic(Vector::median));
	}
}
void
OverallModeMapAnalysis::
getNumModeUpperSlopeSamples()
{
	if(_numModeUpperSlopeSamples.size() == 0)
	{
		for(int i=0; i<_numSuperModes; i++)
			_numModeUpperSlopeSamples.push_back((*_p_superModes)[i]._modeAnalysis.getModeUpperSlopesStatistic(Vector::count));
	}
}

//
/////////////////////////////////////////////////////////////////////////////
///
//	Mode Lower Slopes
//
void
OverallModeMapAnalysis::
getModeLowerSlopes()
{
	if(_modeLowerSlopes.size() == 0)
	{
		for(int i=0; i<_numSuperModes; i++)
		{
			Vector modeLowerSlopes;
			(*_p_superModes)[i]._modeAnalysis.getModeLowerSlopes(modeLowerSlopes);

			_modeLowerSlopes.push_back(modeLowerSlopes);
		}
	}
}

void
OverallModeMapAnalysis::
getMaxModeLowerSlopes()
{
	if(_maxModeLowerSlopes.size() == 0)
	{
		for(int i=0; i<_numSuperModes; i++)
			_maxModeLowerSlopes.push_back((*_p_superModes)[i]._modeAnalysis.getModeLowerSlopesStatistic(Vector::max));
	}
}
void
OverallModeMapAnalysis::
getMinModeLowerSlopes()
{
	if(_minModeLowerSlopes.size() == 0)
	{
		for(int i=0; i<_numSuperModes; i++)
			_minModeLowerSlopes.push_back((*_p_superModes)[i]._modeAnalysis.getModeLowerSlopesStatistic(Vector::min));
	}
}
void
OverallModeMapAnalysis::
getMeanModeLowerSlopes()
{
	if(_meanModeLowerSlopes.size() == 0)
	{
		for(int i=0; i<_numSuperModes; i++)
			_meanModeLowerSlopes.push_back((*_p_superModes)[i]._modeAnalysis.getModeLowerSlopesStatistic(Vector::mean));
	}
}
void
OverallModeMapAnalysis::
getSumModeLowerSlopes()
{
	if(_sumModeLowerSlopes.size() == 0)
	{
		for(int i=0; i<_numSuperModes; i++)
			_sumModeLowerSlopes.push_back((*_p_superModes)[i]._modeAnalysis.getModeLowerSlopesStatistic(Vector::sum));
	}
}
void
OverallModeMapAnalysis::
getVarModeLowerSlopes()
{
	if(_varModeLowerSlopes.size() == 0)
	{
		for(int i=0; i<_numSuperModes; i++)
			_varModeLowerSlopes.push_back((*_p_superModes)[i]._modeAnalysis.getModeLowerSlopesStatistic(Vector::variance));
	}
}
void
OverallModeMapAnalysis::
getStdDevModeLowerSlopes()
{
	if(_stdDevModeLowerSlopes.size() == 0)
	{
		for(int i=0; i<_numSuperModes; i++)
			_stdDevModeLowerSlopes.push_back((*_p_superModes)[i]._modeAnalysis.getModeLowerSlopesStatistic(Vector::stdDev));
	}
}
void
OverallModeMapAnalysis::
getMedianModeLowerSlopes()
{
	if(_medianModeLowerSlopes.size() == 0)
	{
		for(int i=0; i<_numSuperModes; i++)
			_medianModeLowerSlopes.push_back((*_p_superModes)[i]._modeAnalysis.getModeLowerSlopesStatistic(Vector::median));
	}
}
void
OverallModeMapAnalysis::
getNumModeLowerSlopeSamples()
{
	if(_numModeLowerSlopeSamples.size() == 0)
	{
		for(int i=0; i<_numSuperModes; i++)
			_numModeLowerSlopeSamples.push_back((*_p_superModes)[i]._modeAnalysis.getModeLowerSlopesStatistic(Vector::count));
	}
}

////////////////////////////////////////////////////////////////////////////////
///
//	Modal Distortion
//
void
OverallModeMapAnalysis::
getModalDistortionAngles()
{
	if(_lowerModalDistortionAngles.size() == 0)
	{
		for(int i=0; i<_numSuperModes; i++)
		{
			_lowerModalDistortionAngles.push_back((*_p_superModes)[i]._modeAnalysis.getLowerModeModalDistortionAngle());
			_upperModalDistortionAngles.push_back((*_p_superModes)[i]._modeAnalysis.getUpperModeModalDistortionAngle());
		}
	}
}
void
OverallModeMapAnalysis::
getModalDistortionAngleXPositions()
{
	if(_lowerModalDistortionAngleXPositions.size() == 0)
	{
		for(int i=0; i<_numSuperModes; i++)
		{
			_lowerModalDistortionAngleXPositions.push_back((*_p_superModes)[i]._modeAnalysis.getLowerModeMaxModalDistortionAngleXPosition());
			_upperModalDistortionAngleXPositions.push_back((*_p_superModes)[i]._modeAnalysis.getUpperModeMaxModalDistortionAngleXPosition());
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
///
//	Mode Boundary Direction Change Area measurements
//
void
OverallModeMapAnalysis::
getModeBDCAreas()
{
	if(_lowerModeBDCAreas.size() == 0)
	{
		for(int i=0; i<_numSuperModes; i++)
		{
			Vector lowerModeBDCAreas;
			Vector upperModeBDCAreas;
			(*_p_superModes)[i]._modeAnalysis.getModeBDCAreas(lowerModeBDCAreas, upperModeBDCAreas);

			_lowerModeBDCAreas.push_back(lowerModeBDCAreas);
			_upperModeBDCAreas.push_back(upperModeBDCAreas);
		}
	}
}

void
OverallModeMapAnalysis::
getMaxModeBDCAreas()
{
	if(_lowerMaxModeBDCAreas.size() == 0)
	{
		for(int i=0; i<_numSuperModes; i++)
		{
			_lowerMaxModeBDCAreas.push_back((*_p_superModes)[i]._modeAnalysis.getLowerModeBDCAreasStatistic(Vector::max));
			_upperMaxModeBDCAreas.push_back((*_p_superModes)[i]._modeAnalysis.getUpperModeBDCAreasStatistic(Vector::max));
		}
	}
}
void
OverallModeMapAnalysis::
getMinModeBDCAreas()
{
	if(_lowerMinModeBDCAreas.size() == 0)
	{
		for(int i=0; i<_numSuperModes; i++)
		{
			_lowerMinModeBDCAreas.push_back((*_p_superModes)[i]._modeAnalysis.getLowerModeBDCAreasStatistic(Vector::min));
			_upperMinModeBDCAreas.push_back((*_p_superModes)[i]._modeAnalysis.getUpperModeBDCAreasStatistic(Vector::min));
		}
	}
}
void
OverallModeMapAnalysis::
getMeanModeBDCAreas()
{
	if(_lowerMeanModeBDCAreas.size() == 0)
	{
		for(int i=0; i<_numSuperModes; i++)
		{
			_lowerMeanModeBDCAreas.push_back((*_p_superModes)[i]._modeAnalysis.getLowerModeBDCAreasStatistic(Vector::mean));
			_upperMeanModeBDCAreas.push_back((*_p_superModes)[i]._modeAnalysis.getUpperModeBDCAreasStatistic(Vector::mean));
		}
	}
}
void
OverallModeMapAnalysis::
getSumModeBDCAreas()
{
	if(_lowerSumModeBDCAreas.size() == 0)
	{
		for(int i=0; i<_numSuperModes; i++)
		{
			_lowerSumModeBDCAreas.push_back((*_p_superModes)[i]._modeAnalysis.getLowerModeBDCAreasStatistic(Vector::sum));
			_upperSumModeBDCAreas.push_back((*_p_superModes)[i]._modeAnalysis.getUpperModeBDCAreasStatistic(Vector::sum));
		}
	}
}
void
OverallModeMapAnalysis::
getVarModeBDCAreas()
{
	if(_lowerVarModeBDCAreas.size() == 0)
	{
		for(int i=0; i<_numSuperModes; i++)
		{
			_lowerVarModeBDCAreas.push_back((*_p_superModes)[i]._modeAnalysis.getLowerModeBDCAreasStatistic(Vector::variance));
			_upperVarModeBDCAreas.push_back((*_p_superModes)[i]._modeAnalysis.getUpperModeBDCAreasStatistic(Vector::variance));
		}
	}
}
void
OverallModeMapAnalysis::
getStdDevModeBDCAreas()
{
	if(_lowerStdDevModeBDCAreas.size() == 0)
	{
		for(int i=0; i<_numSuperModes; i++)
		{
			_lowerStdDevModeBDCAreas.push_back((*_p_superModes)[i]._modeAnalysis.getLowerModeBDCAreasStatistic(Vector::stdDev));
			_upperStdDevModeBDCAreas.push_back((*_p_superModes)[i]._modeAnalysis.getUpperModeBDCAreasStatistic(Vector::stdDev));
		}
	}
}
void
OverallModeMapAnalysis::
getMedianModeBDCAreas()
{
	if(_lowerMedianModeBDCAreas.size() == 0)
	{
		for(int i=0; i<_numSuperModes; i++)
		{
			_lowerMedianModeBDCAreas.push_back((*_p_superModes)[i]._modeAnalysis.getLowerModeBDCAreasStatistic(Vector::median));
			_upperMedianModeBDCAreas.push_back((*_p_superModes)[i]._modeAnalysis.getUpperModeBDCAreasStatistic(Vector::median));
		}
	}
}
void
OverallModeMapAnalysis::
getNumModeBDCAreaSamples()
{
	if(_lowerNumModeBDCAreaSamples.size() == 0)
	{
		for(int i=0; i<_numSuperModes; i++)
		{
			_lowerNumModeBDCAreaSamples.push_back((*_p_superModes)[i]._modeAnalysis.getLowerModeBDCAreasStatistic(Vector::count));
			_upperNumModeBDCAreaSamples.push_back((*_p_superModes)[i]._modeAnalysis.getUpperModeBDCAreasStatistic(Vector::count));
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
///
//	Mode Boundary Direction Change Area X Length measurements
//
void
OverallModeMapAnalysis::
getModeBDCAreaXLengths()
{
	if(_lowerModeBDCAreaXLengths.size() == 0)
	{
		for(int i=0; i<_numSuperModes; i++)
		{
			Vector lowerModeBDCAreaXLengths;
			Vector upperModeBDCAreaXLengths;
			(*_p_superModes)[i]._modeAnalysis.getModeBDCAreaXLengths(lowerModeBDCAreaXLengths, upperModeBDCAreaXLengths);

			_lowerModeBDCAreaXLengths.push_back(lowerModeBDCAreaXLengths);
			_upperModeBDCAreaXLengths.push_back(upperModeBDCAreaXLengths);
		}
	}
}

void
OverallModeMapAnalysis::
getMaxModeBDCAreaXLengths()
{
	if(_lowerMaxModeBDCAreaXLengths.size() == 0)
	{
		for(int i=0; i<_numSuperModes; i++)
		{
			_lowerMaxModeBDCAreaXLengths.push_back((*_p_superModes)[i]._modeAnalysis.getLowerModeBDCAreaXLengthsStatistic(Vector::max));
			_upperMaxModeBDCAreaXLengths.push_back((*_p_superModes)[i]._modeAnalysis.getUpperModeBDCAreaXLengthsStatistic(Vector::max));
		}
	}
}
void
OverallModeMapAnalysis::
getMinModeBDCAreaXLengths()
{
	if(_lowerMinModeBDCAreaXLengths.size() == 0)
	{
		for(int i=0; i<_numSuperModes; i++)
		{
			_lowerMinModeBDCAreaXLengths.push_back((*_p_superModes)[i]._modeAnalysis.getLowerModeBDCAreaXLengthsStatistic(Vector::min));
			_upperMinModeBDCAreaXLengths.push_back((*_p_superModes)[i]._modeAnalysis.getUpperModeBDCAreaXLengthsStatistic(Vector::min));
		}
	}
}
void
OverallModeMapAnalysis::
getMeanModeBDCAreaXLengths()
{
	if(_lowerMeanModeBDCAreaXLengths.size() == 0)
	{
		for(int i=0; i<_numSuperModes; i++)
		{
			_lowerMeanModeBDCAreaXLengths.push_back((*_p_superModes)[i]._modeAnalysis.getLowerModeBDCAreaXLengthsStatistic(Vector::mean));
			_upperMeanModeBDCAreaXLengths.push_back((*_p_superModes)[i]._modeAnalysis.getUpperModeBDCAreaXLengthsStatistic(Vector::mean));
		}
	}
}
void
OverallModeMapAnalysis::
getSumModeBDCAreaXLengths()
{
	if(_lowerSumModeBDCAreaXLengths.size() == 0)
	{
		for(int i=0; i<_numSuperModes; i++)
		{
			_lowerSumModeBDCAreaXLengths.push_back((*_p_superModes)[i]._modeAnalysis.getLowerModeBDCAreaXLengthsStatistic(Vector::sum));
			_upperSumModeBDCAreaXLengths.push_back((*_p_superModes)[i]._modeAnalysis.getUpperModeBDCAreaXLengthsStatistic(Vector::sum));
		}
	}
}
void
OverallModeMapAnalysis::
getVarModeBDCAreaXLengths()
{
	if(_lowerVarModeBDCAreaXLengths.size() == 0)
	{
		for(int i=0; i<_numSuperModes; i++)
		{
			_lowerVarModeBDCAreaXLengths.push_back((*_p_superModes)[i]._modeAnalysis.getLowerModeBDCAreaXLengthsStatistic(Vector::variance));
			_upperVarModeBDCAreaXLengths.push_back((*_p_superModes)[i]._modeAnalysis.getUpperModeBDCAreaXLengthsStatistic(Vector::variance));
		}
	}
}
void
OverallModeMapAnalysis::
getStdDevModeBDCAreaXLengths()
{
	if(_lowerStdDevModeBDCAreaXLengths.size() == 0)
	{
		for(int i=0; i<_numSuperModes; i++)
		{
			_lowerStdDevModeBDCAreaXLengths.push_back((*_p_superModes)[i]._modeAnalysis.getLowerModeBDCAreaXLengthsStatistic(Vector::stdDev));
			_upperStdDevModeBDCAreaXLengths.push_back((*_p_superModes)[i]._modeAnalysis.getUpperModeBDCAreaXLengthsStatistic(Vector::stdDev));
		}
	}
}
void
OverallModeMapAnalysis::
getMedianModeBDCAreaXLengths()
{
	if(_lowerMedianModeBDCAreaXLengths.size() == 0)
	{
		for(int i=0; i<_numSuperModes; i++)
		{
			_lowerMedianModeBDCAreaXLengths.push_back((*_p_superModes)[i]._modeAnalysis.getLowerModeBDCAreaXLengthsStatistic(Vector::median));
			_upperMedianModeBDCAreaXLengths.push_back((*_p_superModes)[i]._modeAnalysis.getUpperModeBDCAreaXLengthsStatistic(Vector::median));
		}
	}
}
void
OverallModeMapAnalysis::
getNumModeBDCAreaXLengthSamples()
{
	if(_lowerNumModeBDCAreaXLengthSamples.size() == 0)
	{
		for(int i=0; i<_numSuperModes; i++)
		{
			_lowerNumModeBDCAreaXLengthSamples.push_back((*_p_superModes)[i]._modeAnalysis.getLowerModeBDCAreaXLengthsStatistic(Vector::count));
			_upperNumModeBDCAreaXLengthSamples.push_back((*_p_superModes)[i]._modeAnalysis.getUpperModeBDCAreaXLengthsStatistic(Vector::count));
		}
	}
}

//
/////////////////////////////////////////////////////////////////////////////
///
//	MiddleLine RootMeanSquare analysis
//
void
OverallModeMapAnalysis::
getMiddleLineRMSValues()
{
	if(_middleLineRMSValues.size() == 0)
	{
		for(int i=0; i<_numSuperModes; i++)
		{			
			double rmsValue = 0;
			(*_p_superModes)[i].getMiddleLineRMSValue(rmsValue);

			_middleLineRMSValues.push_back(rmsValue);
		}
	}
}

void
OverallModeMapAnalysis::
getMiddleLineSlopes()
{
	if(_middleLineSlopes.size() == 0)
	{
		for(int i=0; i<_numSuperModes; i++)
		{			
			double slope = 0;
			(*_p_superModes)[i].getMiddleLineSlope(slope);

			_middleLineSlopes.push_back(slope);
		}
	}
}
//
/////////////////////////////////////////////////////////////////////////////
///
//	Continuity analysis
//
void
OverallModeMapAnalysis::
getMaxContinuityValue()
{
	if(_maxContinuityValue == 0)
		_maxContinuityValue = _continuityValues.getVectorAttribute(Vector::max);
}

void
OverallModeMapAnalysis::
getMinContinuityValue()
{
	if(_minContinuityValue == 0)
		_minContinuityValue = _continuityValues.getVectorAttribute(Vector::min);
}

void
OverallModeMapAnalysis::
getMeanContinuityValues()
{
	if(_meanContinuityValues == 0)
		_meanContinuityValues = _continuityValues.getVectorAttribute(Vector::mean);
}

void
OverallModeMapAnalysis::
getSumContinuityValues()
{
	if(_sumContinuityValues == 0)
		_sumContinuityValues = _continuityValues.getVectorAttribute(Vector::sum);
}

void
OverallModeMapAnalysis::
getVarContinuityValues()
{
	if(_varContinuityValues == 0)
		_varContinuityValues = _continuityValues.getVectorAttribute(Vector::variance);
}

void
OverallModeMapAnalysis::
getStdDevContinuityValues()
{
	if(_stdDevContinuityValues == 0)
		_stdDevContinuityValues = _continuityValues.getVectorAttribute(Vector::stdDev);
}

void
OverallModeMapAnalysis::
getMedianContinuityValues()
{
	if(_medianContinuityValues == 0)
		_medianContinuityValues = _continuityValues.getVectorAttribute(Vector::median);
}

void
OverallModeMapAnalysis::
getNumContinuityValues()
{
	if(_numContinuityValues == 0)
		_numContinuityValues = _continuityValues.getVectorAttribute(Vector::count);
}

void
OverallModeMapAnalysis::
getMaxPrGap()
{
	if(_maxPrGap == 0)
	{
		_maxPrGap = _continuityValues.getVectorAttribute(Vector::maxGap);
	}
}

void
OverallModeMapAnalysis::
getMaxLowerPr()
{
	if(_maxLowerPr == 0)
	{
		_maxLowerPr = _continuityValues.getVectorAttribute(Vector::min);
	}
}
void
OverallModeMapAnalysis::
getMinUpperPr()
{
	if(_minUpperPr == 0)
	{
		_minUpperPr = _continuityValues.getVectorAttribute(Vector::max);
	}
}
//
///
////////////////////////////////////////////////////////////////////////////////
///
//	File Handling section - writing results to file
//
OverallModeMapAnalysis::rcode
OverallModeMapAnalysis::
writeQaResultsToFile()
{
	rcode err = rcode::ok;

	CLoseGridFile qaResults(_qaResultsAbsFilePath);
	qaResults.createFolderAndFile();

	//////////////////////////////////////////////////////////////////////////

	CString errorMessage = "";
	int errorNum = 0;
	getQAError(_qaDetectedError, errorNum, errorMessage);

    CStringArray error_msg_row;
    error_msg_row.Add(errorMessage);
    qaResults.addRow(error_msg_row);
    qaResults.addEmptyLine();

	//////////////////////////////////////////
	
	CStringArray row;
	CString cellString = "";

	row.RemoveAll();
	row.Add("Slope Window Size");
	cellString.Format("%d", CG_REG->get_DSDBR01_OMQA_slope_window_size());
	row.Add(cellString);
	qaResults.addRow(row);

	row.RemoveAll();
	row.Add("Modal Distortion Min X");
	cellString.Format("%g", CG_REG->get_DSDBR01_OMQA_modal_distortion_min_x());
	row.Add(cellString);
	qaResults.addRow(row);

	row.RemoveAll();
	row.Add("Modal Distortion Max X");
	cellString.Format("%g", CG_REG->get_DSDBR01_OMQA_modal_distortion_max_x());
	row.Add(cellString);
	qaResults.addRow(row);

	row.RemoveAll();
	row.Add("Modal Distortion Min Y");
	cellString.Format("%g", CG_REG->get_DSDBR01_OMQA_modal_distortion_min_y());
	row.Add(cellString);
	qaResults.addRow(row);

    qaResults.addEmptyLine();

	row.RemoveAll();
	row.Add("Max Pr Gap in Continuity Data");
	cellString.Format("%g", _maxPrGap);
	row.Add(cellString);
	qaResults.addRow(row);

	row.RemoveAll();
	row.Add("Lower Pr in Continuity Data");
	cellString.Format("%g", _maxLowerPr);
	row.Add(cellString);
	qaResults.addRow(row);

	row.RemoveAll();
	row.Add("Upper Pr in Continuity Data");
	cellString.Format("%g", _minUpperPr);
	row.Add(cellString);
	qaResults.addRow(row);

    qaResults.addEmptyLine();

	//////////////////////////////////////////

	const CString	QA_XLS_HEADER1 = "";
	const CString	QA_XLS_HEADER2 = "Index";
	const CString	QA_XLS_HEADER3 = "Function";
	const CString	QA_XLS_HEADER4 = "Slope of upper mode boundary line";
	const CString	QA_XLS_HEADER5 = "Slope of lower mode boundary line";
	const CString	QA_XLS_HEADER6 = "Mode Width";
	const CString	QA_XLS_HEADER7 = "Max Lower Modal Distortion Angle";
	const CString	QA_XLS_HEADER8 = "Max Upper Modal Distortion Angle";
	const CString	QA_XLS_HEADER9 = "Max Lower Modal Distortion X-Position";
	const CString	QA_XLS_HEADER10 = "Max Upper Modal Distortion X-Position";
	const CString	QA_XLS_HEADER11 = "Max Lower Boundary Direction Change X-Length";
	const CString	QA_XLS_HEADER12 = "Max Upper Boundary Direction Change X-Length";
	const CString	QA_XLS_HEADER13 = "Max Lower Boundary Direction Change Area";
	const CString	QA_XLS_HEADER14 = "Max Upper Boundary Direction Change Area";
	const CString	QA_XLS_HEADER15 = "Sum of Lower Boundary Direction Change Areas";
	const CString	QA_XLS_HEADER16 = "Sum of Upper Boundary Direction Change Areas";
	const CString	QA_XLS_HEADER17 = "Number of Lower Boundary Direction Changes";
	const CString	QA_XLS_HEADER18 = "Number of Upper Boundary Direction Changes";
	const CString	QA_XLS_HEADER19 = "Middle Line RMS Value";

	const CString	QA_XLS_MAX		= "Maximum";
	const CString	QA_XLS_MIN		= "Minimum";
	const CString	QA_XLS_MEAN		= "Mean";
	const CString	QA_XLS_SUM		= "Sum";
	const CString	QA_XLS_COUNT	= "Number Of Samples";
	const CString	QA_XLS_STDDEV	= "Standard Deviation";
	const CString	QA_XLS_VAR		= "Variance";
	const CString	QA_XLS_OTHERS	= "";

	CStringArray attribArray;
	attribArray.Add(QA_XLS_MAX);
	attribArray.Add(QA_XLS_MIN);
	attribArray.Add(QA_XLS_MEAN);
	attribArray.Add(QA_XLS_SUM);
	attribArray.Add(QA_XLS_COUNT);
	attribArray.Add(QA_XLS_STDDEV);
	attribArray.Add(QA_XLS_VAR);
	attribArray.Add(QA_XLS_OTHERS);

	CString	QA_XLS_COL1_VALUE = "";
	CString	QA_XLS_COL2_VALUE = "";
	CString	QA_XLS_COL3_VALUE = "";
	CString	QA_XLS_COL4_VALUE = "";
	CString	QA_XLS_COL5_VALUE = "";
	CString	QA_XLS_COL6_VALUE = "";
	CString	QA_XLS_COL7_VALUE = "";
	CString	QA_XLS_COL8_VALUE = "";
	CString	QA_XLS_COL9_VALUE = "";
	CString	QA_XLS_COL10_VALUE = "";
	CString	QA_XLS_COL11_VALUE = "";
	CString	QA_XLS_COL12_VALUE = "";
	CString	QA_XLS_COL13_VALUE = "";
	CString	QA_XLS_COL14_VALUE = "";
	CString	QA_XLS_COL15_VALUE = "";
	CString	QA_XLS_COL16_VALUE = "";
	CString	QA_XLS_COL17_VALUE = "";
	CString	QA_XLS_COL18_VALUE = "";
	CString	QA_XLS_COL19_VALUE = "";

	CStringArray headerArray;
	headerArray.Add(QA_XLS_HEADER1);
	headerArray.Add(QA_XLS_HEADER2);
	headerArray.Add(QA_XLS_HEADER3);
	headerArray.Add(QA_XLS_HEADER4);
	headerArray.Add(QA_XLS_HEADER5);
	headerArray.Add(QA_XLS_HEADER6);
	headerArray.Add(QA_XLS_HEADER7);
	headerArray.Add(QA_XLS_HEADER8);
	headerArray.Add(QA_XLS_HEADER9);
	headerArray.Add(QA_XLS_HEADER10);
	headerArray.Add(QA_XLS_HEADER11);
	headerArray.Add(QA_XLS_HEADER12);
	headerArray.Add(QA_XLS_HEADER13);
	headerArray.Add(QA_XLS_HEADER14);
	headerArray.Add(QA_XLS_HEADER15);
	headerArray.Add(QA_XLS_HEADER16);
	headerArray.Add(QA_XLS_HEADER17);
	headerArray.Add(QA_XLS_HEADER18);
	headerArray.Add(QA_XLS_HEADER19);

	qaResults.addRow(headerArray);

	//////////////////////////////////////////////////////////////////////////

	CStringArray rowArray;
	rowArray.Add(QA_XLS_COL1_VALUE);
	rowArray.Add(QA_XLS_COL2_VALUE);
	rowArray.Add(QA_XLS_COL3_VALUE);
	rowArray.Add(QA_XLS_COL4_VALUE);
	rowArray.Add(QA_XLS_COL5_VALUE);
	rowArray.Add(QA_XLS_COL6_VALUE);
	rowArray.Add(QA_XLS_COL7_VALUE);
	rowArray.Add(QA_XLS_COL8_VALUE);
	rowArray.Add(QA_XLS_COL9_VALUE);
	rowArray.Add(QA_XLS_COL10_VALUE);
	rowArray.Add(QA_XLS_COL11_VALUE);
	rowArray.Add(QA_XLS_COL12_VALUE);
 	rowArray.Add(QA_XLS_COL13_VALUE);
	rowArray.Add(QA_XLS_COL14_VALUE);
	rowArray.Add(QA_XLS_COL15_VALUE);
	rowArray.Add(QA_XLS_COL16_VALUE);
	rowArray.Add(QA_XLS_COL17_VALUE);
	rowArray.Add(QA_XLS_COL18_VALUE);
	rowArray.Add(QA_XLS_COL19_VALUE);
	qaResults.addRow(rowArray);

    QA_XLS_COL1_VALUE = "";
    QA_XLS_COL10_VALUE = "";

	//////////////////////////////////////////////////////////////////////////

    int QA_XLS_NUM_ROWS_PER_SM = (int)attribArray.GetCount();

    for(int i=0;  i<_numSuperModes; i++)
    {
	    for(int j = 0; j<QA_XLS_NUM_ROWS_PER_SM; j++)
	    {
			// clear vars
			rowArray.RemoveAll();
			QA_XLS_COL1_VALUE = "";
			QA_XLS_COL2_VALUE = "";
			QA_XLS_COL3_VALUE = "";
			QA_XLS_COL4_VALUE = "";
			QA_XLS_COL5_VALUE = "";
			QA_XLS_COL6_VALUE = "";
			QA_XLS_COL7_VALUE = "";
			QA_XLS_COL8_VALUE = "";
			QA_XLS_COL9_VALUE = "";
			QA_XLS_COL10_VALUE = "";
			QA_XLS_COL11_VALUE = "";
			QA_XLS_COL12_VALUE = "";
			QA_XLS_COL13_VALUE = "";
			QA_XLS_COL14_VALUE = "";
			QA_XLS_COL15_VALUE = "";
			QA_XLS_COL16_VALUE = "";
			QA_XLS_COL17_VALUE = "";
			QA_XLS_COL18_VALUE = "";
			QA_XLS_COL19_VALUE = "";

			QA_XLS_COL1_VALUE = "";
			QA_XLS_COL2_VALUE.AppendFormat("%d", i);
			QA_XLS_COL3_VALUE = attribArray[j];

			if(QA_XLS_COL3_VALUE == QA_XLS_MAX)
			{
				if(i<(int)_maxModeUpperSlopes.size())
					QA_XLS_COL4_VALUE.AppendFormat("%g", _maxModeUpperSlopes[i]);
				else
					QA_XLS_COL4_VALUE = "";
				if(i<(int)_maxModeLowerSlopes.size())
					QA_XLS_COL5_VALUE.AppendFormat("%g", _maxModeLowerSlopes[i]);
				else
					QA_XLS_COL5_VALUE = "";
				if(i<(int)_maxModeWidths.size())
					QA_XLS_COL6_VALUE.AppendFormat("%g", _maxModeWidths[i]);
				else
					QA_XLS_COL6_VALUE = "";
			}
			else if(QA_XLS_COL3_VALUE == QA_XLS_MIN)
			{
				if(i<(int)_minModeUpperSlopes.size())
					QA_XLS_COL4_VALUE.AppendFormat("%g", _minModeUpperSlopes[i]);
				else
					QA_XLS_COL4_VALUE = "";
				if(i<(int)_minModeLowerSlopes.size())
					QA_XLS_COL5_VALUE.AppendFormat("%g", _minModeLowerSlopes[i]);
				else
					QA_XLS_COL5_VALUE = "";
				if(i<(int)_minModeWidths.size())
					QA_XLS_COL6_VALUE.AppendFormat("%g", _minModeWidths[i]);
				else
					QA_XLS_COL6_VALUE = "";
			}
			else if(QA_XLS_COL3_VALUE == QA_XLS_MEAN)
			{
				if(i<(int)_meanModeUpperSlopes.size())
					QA_XLS_COL4_VALUE.AppendFormat("%g", _meanModeUpperSlopes[i]);
				else
					QA_XLS_COL4_VALUE = "";
				if(i<(int)_meanModeLowerSlopes.size())
					QA_XLS_COL5_VALUE.AppendFormat("%g", _meanModeLowerSlopes[i]);
				else
					QA_XLS_COL5_VALUE = "";
				if(i<(int)_meanModeWidths.size())
					QA_XLS_COL6_VALUE.AppendFormat("%g", _meanModeWidths[i]);
				else
					QA_XLS_COL6_VALUE = "";
			}
			else if(QA_XLS_COL3_VALUE == QA_XLS_SUM)
			{
				if(i<(int)_sumModeUpperSlopes.size())
					QA_XLS_COL4_VALUE.AppendFormat("%g", _sumModeUpperSlopes[i]);
				else
					QA_XLS_COL4_VALUE = "";
				if(i<(int)_sumModeLowerSlopes.size())
					QA_XLS_COL5_VALUE.AppendFormat("%g", _sumModeLowerSlopes[i]);
				else
					QA_XLS_COL5_VALUE = "";
				if(i<(int)_sumModeWidths.size())
					QA_XLS_COL6_VALUE.AppendFormat("%g", _sumModeWidths[i]);
				else
					QA_XLS_COL6_VALUE = "";
			}
			else if(QA_XLS_COL3_VALUE == QA_XLS_COUNT)
			{
				if(i<(int)_numModeUpperSlopeSamples.size())
					QA_XLS_COL4_VALUE.AppendFormat("%g", _numModeUpperSlopeSamples[i]);
				else
					QA_XLS_COL4_VALUE = "";
				if(i<(int)_numModeLowerSlopeSamples.size())
					QA_XLS_COL5_VALUE.AppendFormat("%g", _numModeLowerSlopeSamples[i]);
				else
					QA_XLS_COL5_VALUE = "";
				if(i<(int)_numModeWidthSamples.size())
					QA_XLS_COL6_VALUE.AppendFormat("%g", _numModeWidthSamples[i]);
				else
					QA_XLS_COL6_VALUE = "";
			}
			else if(QA_XLS_COL3_VALUE == QA_XLS_STDDEV)
			{
				if(i<(int)_stdDevModeUpperSlopes.size())
					QA_XLS_COL4_VALUE.AppendFormat("%g", _stdDevModeUpperSlopes[i]);
				else
					QA_XLS_COL4_VALUE = "";
				if(i<(int)_stdDevModeLowerSlopes.size())
					QA_XLS_COL5_VALUE.AppendFormat("%g", _stdDevModeLowerSlopes[i]);
				else
					QA_XLS_COL5_VALUE = "";
				if(i<(int)_stdDevModeWidths.size())
					QA_XLS_COL6_VALUE.AppendFormat("%g", _stdDevModeWidths[i]);
				else
					QA_XLS_COL6_VALUE = "";
			}
			else if(QA_XLS_COL3_VALUE == QA_XLS_VAR)
			{
				if(i<(int)_varModeUpperSlopes.size())
					QA_XLS_COL4_VALUE.AppendFormat("%g", _varModeUpperSlopes[i]);
				else
					QA_XLS_COL4_VALUE = "";
				if(i<(int)_varModeLowerSlopes.size())
					QA_XLS_COL5_VALUE.AppendFormat("%g", _varModeLowerSlopes[i]);
				else
					QA_XLS_COL5_VALUE = "";
				if(i<(int)_varModeWidths.size())
					QA_XLS_COL6_VALUE.AppendFormat("%g", _varModeWidths[i]);
				else
					QA_XLS_COL6_VALUE = "";
			}
			else if(QA_XLS_COL3_VALUE == QA_XLS_OTHERS)
			{
				if(i<(int)_lowerModalDistortionAngles.size())
					QA_XLS_COL7_VALUE.AppendFormat("%g", _lowerModalDistortionAngles[i]);
				else
					QA_XLS_COL7_VALUE = "";
				if(i<(int)_upperModalDistortionAngles.size())
					QA_XLS_COL8_VALUE.AppendFormat("%g", _upperModalDistortionAngles[i]);
				else
					QA_XLS_COL8_VALUE = "";
				if(i<(int)_lowerModalDistortionAngleXPositions.size())
					QA_XLS_COL9_VALUE.AppendFormat("%g", _lowerModalDistortionAngleXPositions[i]);
				else
					QA_XLS_COL9_VALUE = "";
				if(i<(int)_upperModalDistortionAngleXPositions.size())
					QA_XLS_COL10_VALUE.AppendFormat("%g", _upperModalDistortionAngleXPositions[i]);
				else
					QA_XLS_COL10_VALUE = "";
				if(i<(int)_lowerMaxModeBDCAreaXLengths.size())
					QA_XLS_COL11_VALUE.AppendFormat("%g", _lowerMaxModeBDCAreaXLengths[i]);
				else
					QA_XLS_COL11_VALUE = "";
				if(i<(int)_upperMaxModeBDCAreaXLengths.size())
					QA_XLS_COL12_VALUE.AppendFormat("%g", _upperMaxModeBDCAreaXLengths[i]);
				else
					QA_XLS_COL12_VALUE = "";
				if(i<(int)_lowerMaxModeBDCAreas.size())
					QA_XLS_COL13_VALUE.AppendFormat("%g", _lowerMaxModeBDCAreas[i]);
				else
					QA_XLS_COL13_VALUE = "";
				if(i<(int)_upperMaxModeBDCAreas.size())
					QA_XLS_COL14_VALUE.AppendFormat("%g", _upperMaxModeBDCAreas[i]);
				else
					QA_XLS_COL14_VALUE = "";
				if(i<(int)_lowerSumModeBDCAreas.size())
					QA_XLS_COL15_VALUE.AppendFormat("%g", _lowerSumModeBDCAreas[i]);
				else
					QA_XLS_COL15_VALUE = "";
				if(i<(int)_upperSumModeBDCAreas.size())
					QA_XLS_COL16_VALUE.AppendFormat("%g", _upperSumModeBDCAreas[i]);
				else
					QA_XLS_COL16_VALUE = "";
				if(i<(int)_lowerNumModeBDCAreaSamples.size())
					QA_XLS_COL17_VALUE.AppendFormat("%g", _lowerNumModeBDCAreaSamples[i]);
				else
					QA_XLS_COL17_VALUE = "";
				if(i<(int)_upperNumModeBDCAreaSamples.size())
					QA_XLS_COL18_VALUE.AppendFormat("%g", _upperNumModeBDCAreaSamples[i]);
				else
					QA_XLS_COL18_VALUE = "";
				if(i<(int)_middleLineRMSValues.size())
					QA_XLS_COL19_VALUE.AppendFormat("%g", _middleLineRMSValues[i]);
				else
					QA_XLS_COL19_VALUE = "";
			}

			rowArray.RemoveAll();
			rowArray.Add(QA_XLS_COL1_VALUE);
			rowArray.Add(QA_XLS_COL2_VALUE);
			rowArray.Add(QA_XLS_COL3_VALUE);
			rowArray.Add(QA_XLS_COL4_VALUE);
			rowArray.Add(QA_XLS_COL5_VALUE);
			rowArray.Add(QA_XLS_COL6_VALUE);
			rowArray.Add(QA_XLS_COL7_VALUE);
			rowArray.Add(QA_XLS_COL8_VALUE);
			rowArray.Add(QA_XLS_COL9_VALUE);
			rowArray.Add(QA_XLS_COL10_VALUE);
			rowArray.Add(QA_XLS_COL11_VALUE);
			rowArray.Add(QA_XLS_COL12_VALUE);
			rowArray.Add(QA_XLS_COL13_VALUE);
			rowArray.Add(QA_XLS_COL14_VALUE);
			rowArray.Add(QA_XLS_COL15_VALUE);
			rowArray.Add(QA_XLS_COL16_VALUE);
			rowArray.Add(QA_XLS_COL17_VALUE);
			rowArray.Add(QA_XLS_COL18_VALUE);
			rowArray.Add(QA_XLS_COL19_VALUE);

			qaResults.addRow(rowArray);
		}

		qaResults.addEmptyLine();

	}

	return err;
}


OverallModeMapAnalysis::rcode
OverallModeMapAnalysis::
writePassFailResultsToFile( CLoseGridFile &passFailResults )
{
	rcode err = rcode::ok;

    if(_passFailAnalysisComplete)
    {
		CString errorMessage = "";
		int errorNum = 0;
		getQAError(_qaDetectedError, errorNum, errorMessage);

        CStringArray error_msg_row;
        error_msg_row.Add(errorMessage);
        passFailResults.addRow(error_msg_row);
        passFailResults.addEmptyLine();

		//////////////////////////////////////////
		
		CStringArray row;
		CString cellString = "";

		row.RemoveAll();
		row.Add("Slope Window Size");
		cellString.Format("%d", CG_REG->get_DSDBR01_OMQA_slope_window_size());
		row.Add(cellString);
		passFailResults.addRow(row);

		row.RemoveAll();
		row.Add("Modal Distortion Min X");
		cellString.Format("%g", CG_REG->get_DSDBR01_OMQA_modal_distortion_min_x());
		row.Add(cellString);
		passFailResults.addRow(row);

		row.RemoveAll();
		row.Add("Modal Distortion Max X");
		cellString.Format("%g", CG_REG->get_DSDBR01_OMQA_modal_distortion_max_x());
		row.Add(cellString);
		passFailResults.addRow(row);

		row.RemoveAll();
		row.Add("Modal Distortion Min Y");
		cellString.Format("%g", CG_REG->get_DSDBR01_OMQA_modal_distortion_min_y());
		row.Add(cellString);
		passFailResults.addRow(row);

		passFailResults.addEmptyLine();

		//////////////////////////////////////////

	    const CString	QA_XLS_HEADER1 = "";
	    const CString	QA_XLS_HEADER2 = "Mode borders removed due to error";
	    const CString	QA_XLS_HEADER3 = "Max Mode Width";
	    const CString	QA_XLS_HEADER4 = "Min Mode Width";
	    const CString	QA_XLS_HEADER5 = "Max Lower Modal Distortion Angle";
	    const CString	QA_XLS_HEADER6 = "Max Upper Modal Distortion Angle";
	    const CString	QA_XLS_HEADER7 = "Max Lower Modal Distortion X-Position";
	    const CString	QA_XLS_HEADER8 = "Max Upper Modal Distortion X-Position";
	    const CString	QA_XLS_HEADER9 = "Max Lower Boundary Direction Change X-Length";
	    const CString	QA_XLS_HEADER10 = "Max Upper Boundary Direction Change X-Length";
	    const CString	QA_XLS_HEADER11 = "Max Lower Boundary Direction Change Area";
	    const CString	QA_XLS_HEADER12 = "Max Upper Boundary Direction Change Area";
	    const CString	QA_XLS_HEADER13 = "Sum of Lower Boundary Direction Change Areas";
	    const CString	QA_XLS_HEADER14 = "Sum of Upper Boundary Direction Change Areas";
	    const CString	QA_XLS_HEADER15 = "Number of Lower Boundary Direction Changes";
	    const CString	QA_XLS_HEADER16 = "Number of Upper Boundary Direction Changes";
	    const CString	QA_XLS_HEADER17 = "Max Middle Line RMS Value";
	    const CString	QA_XLS_HEADER18 = "Min Middle Line Slope";
	    const CString	QA_XLS_HEADER19 = "Max Middle Line Slope";
	    const CString	QA_XLS_HEADER20 = "Max Pr Gap";
		const CString	QA_XLS_HEADER21 = "Max Lower Pr";
		const CString	QA_XLS_HEADER22 = "Min Upper Pr";
	    const CString	QA_XLS_HEADER23 = "Max Mode Lower Line Slope";
	    const CString	QA_XLS_HEADER24 = "Max Mode Upper Line Slope";
	    const CString	QA_XLS_HEADER25 = "Min Mode Lower Line Slope";
	    const CString	QA_XLS_HEADER26 = "Min Mode Upper Line Slope";

	    const CString	QA_XLS_ROWTITLE1	= "Pass/Fail Result";
	    const CString	QA_XLS_ROWTITLE2	= "Min Value Measured";
	    const CString	QA_XLS_ROWTITLE3	= "Max Value Measured";
	    const CString	QA_XLS_ROWTITLE4	= "Number of Failed SMs";
	    const CString	QA_XLS_ROWTITLE5	= "Threshold Applied";

	    CString	QA_XLS_COL1_VALUE = "";
	    CString	QA_XLS_COL2_VALUE = "";
	    CString	QA_XLS_COL3_VALUE = "";
	    CString	QA_XLS_COL4_VALUE = "";
	    CString	QA_XLS_COL5_VALUE = "";
	    CString	QA_XLS_COL6_VALUE = "";
	    CString	QA_XLS_COL7_VALUE = "";
	    CString	QA_XLS_COL8_VALUE = "";
	    CString	QA_XLS_COL9_VALUE = "";
	    CString	QA_XLS_COL10_VALUE = "";
	    CString	QA_XLS_COL11_VALUE = "";
	    CString	QA_XLS_COL12_VALUE = "";
	    CString	QA_XLS_COL13_VALUE = "";
	    CString	QA_XLS_COL14_VALUE = "";
	    CString	QA_XLS_COL15_VALUE = "";
	    CString	QA_XLS_COL16_VALUE = "";
	    CString	QA_XLS_COL17_VALUE = "";
	    CString	QA_XLS_COL18_VALUE = "";
	    CString	QA_XLS_COL19_VALUE = "";
	    CString	QA_XLS_COL20_VALUE = "";
	    CString	QA_XLS_COL21_VALUE = "";
	    CString	QA_XLS_COL22_VALUE = "";
	    CString	QA_XLS_COL23_VALUE = "";
	    CString	QA_XLS_COL24_VALUE = "";
	    CString	QA_XLS_COL25_VALUE = "";
	    CString	QA_XLS_COL26_VALUE = "";

	    CStringArray headerArray;
	    headerArray.Add(QA_XLS_HEADER1);
	    headerArray.Add(QA_XLS_HEADER2);
	    headerArray.Add(QA_XLS_HEADER3);
	    headerArray.Add(QA_XLS_HEADER4);
	    headerArray.Add(QA_XLS_HEADER5);
	    headerArray.Add(QA_XLS_HEADER6);
	    headerArray.Add(QA_XLS_HEADER7);
	    headerArray.Add(QA_XLS_HEADER8);
	    headerArray.Add(QA_XLS_HEADER9);
	    headerArray.Add(QA_XLS_HEADER10);
	    headerArray.Add(QA_XLS_HEADER11);
	    headerArray.Add(QA_XLS_HEADER12);
	    headerArray.Add(QA_XLS_HEADER13);
	    headerArray.Add(QA_XLS_HEADER14);
	    headerArray.Add(QA_XLS_HEADER15);
	    headerArray.Add(QA_XLS_HEADER16);
	    headerArray.Add(QA_XLS_HEADER17);
	    headerArray.Add(QA_XLS_HEADER18);
	    headerArray.Add(QA_XLS_HEADER19);
	    headerArray.Add(QA_XLS_HEADER20);
	    headerArray.Add(QA_XLS_HEADER21);
	    headerArray.Add(QA_XLS_HEADER22);
	    headerArray.Add(QA_XLS_HEADER23);
	    headerArray.Add(QA_XLS_HEADER24);
	    headerArray.Add(QA_XLS_HEADER25);
	    headerArray.Add(QA_XLS_HEADER26);

	    CStringArray rowTitleArray;
	    rowTitleArray.Add(QA_XLS_ROWTITLE1);
	    rowTitleArray.Add(QA_XLS_ROWTITLE2);
	    rowTitleArray.Add(QA_XLS_ROWTITLE3);
	    rowTitleArray.Add(QA_XLS_ROWTITLE4);
	    rowTitleArray.Add(QA_XLS_ROWTITLE5);


	    // set headers
	    passFailResults.addRow(headerArray);

        // add space
	    passFailResults.addEmptyLine();


	    ///////////////////////////////
	    /// first row - pass/fails
	    //
		QA_XLS_COL2_VALUE	= (_numSuperModesRemoved <= _maxModeBordersRemovedAnalysis.threshold) ? "PASS" : "FAIL";
        QA_XLS_COL3_VALUE	= _maxModeWidthPFAnalysis.pass ? "PASS" : "FAIL";
        QA_XLS_COL4_VALUE	= _minModeWidthPFAnalysis.pass ? "PASS" : "FAIL";
        QA_XLS_COL5_VALUE	= _lowerModalDistortionAnglePFAnalysis.pass ? "PASS" : "FAIL";
        QA_XLS_COL6_VALUE	= _upperModalDistortionAnglePFAnalysis.pass ? "PASS" : "FAIL";
        QA_XLS_COL7_VALUE	= _lowerModalDistortionAngleXPositionPFAnalysis.pass ? "PASS" : "FAIL";
        QA_XLS_COL8_VALUE	= _upperModalDistortionAngleXPositionPFAnalysis.pass ? "PASS" : "FAIL";
        QA_XLS_COL9_VALUE	= _lowerMaxModeBDCAreaXLengthPFAnalysis.pass ? "PASS" : "FAIL";
        QA_XLS_COL10_VALUE	= _upperMaxModeBDCAreaXLengthPFAnalysis.pass ? "PASS" : "FAIL";
        QA_XLS_COL11_VALUE	= _lowerMaxModeBDCAreaPFAnalysis.pass ? "PASS" : "FAIL";
        QA_XLS_COL12_VALUE	= _upperMaxModeBDCAreaPFAnalysis.pass ? "PASS" : "FAIL";
        QA_XLS_COL13_VALUE	= _lowerSumModeBDCAreasPFAnalysis.pass ? "PASS" : "FAIL";
        QA_XLS_COL14_VALUE	= _upperSumModeBDCAreasPFAnalysis.pass ? "PASS" : "FAIL";
        QA_XLS_COL15_VALUE	= _lowerNumModeBDCAreasPFAnalysis.pass ? "PASS" : "FAIL";
        QA_XLS_COL16_VALUE	= _upperNumModeBDCAreasPFAnalysis.pass ? "PASS" : "FAIL";
		QA_XLS_COL17_VALUE	= _middleLineRMSValuesPFAnalysis.pass ? "PASS" : "FAIL";
		QA_XLS_COL18_VALUE	= _minMiddleLineSlopePFAnalysis.pass ? "PASS" : "FAIL";
		QA_XLS_COL19_VALUE	= _maxMiddleLineSlopePFAnalysis.pass ? "PASS" : "FAIL";
		QA_XLS_COL20_VALUE	= _maxPrGapPFAnalysis.pass ? "PASS" : "FAIL";
		QA_XLS_COL21_VALUE	= _maxLowerPrPFAnalysis.pass ? "PASS" : "FAIL";
		QA_XLS_COL22_VALUE	= _minUpperPrPFAnalysis.pass ? "PASS" : "FAIL";
		QA_XLS_COL23_VALUE	= _lowerMaxModeLineSlopePFAnalysis.pass ? "PASS" : "FAIL";
		QA_XLS_COL24_VALUE	= _upperMaxModeLineSlopePFAnalysis.pass ? "PASS" : "FAIL";
		QA_XLS_COL25_VALUE	= _lowerMinModeLineSlopePFAnalysis.pass ? "PASS" : "FAIL";
		QA_XLS_COL26_VALUE	= _upperMinModeLineSlopePFAnalysis.pass ? "PASS" : "FAIL";

	    CStringArray rowArray;

	    rowArray.RemoveAll();
        rowArray.Add(QA_XLS_ROWTITLE1);
	    rowArray.Add(QA_XLS_COL2_VALUE);
	    rowArray.Add(QA_XLS_COL3_VALUE);
	    rowArray.Add(QA_XLS_COL4_VALUE);
	    rowArray.Add(QA_XLS_COL5_VALUE);
	    rowArray.Add(QA_XLS_COL6_VALUE);
	    rowArray.Add(QA_XLS_COL7_VALUE);
	    rowArray.Add(QA_XLS_COL8_VALUE);
	    rowArray.Add(QA_XLS_COL9_VALUE);
	    rowArray.Add(QA_XLS_COL10_VALUE);
	    rowArray.Add(QA_XLS_COL11_VALUE);
	    rowArray.Add(QA_XLS_COL12_VALUE);
	    rowArray.Add(QA_XLS_COL13_VALUE);
	    rowArray.Add(QA_XLS_COL14_VALUE);
	    rowArray.Add(QA_XLS_COL15_VALUE);
	    rowArray.Add(QA_XLS_COL16_VALUE);
	    rowArray.Add(QA_XLS_COL17_VALUE);
	    rowArray.Add(QA_XLS_COL18_VALUE);
	    rowArray.Add(QA_XLS_COL19_VALUE);
	    rowArray.Add(QA_XLS_COL20_VALUE);
	    rowArray.Add(QA_XLS_COL21_VALUE);
	    rowArray.Add(QA_XLS_COL22_VALUE);
	    rowArray.Add(QA_XLS_COL23_VALUE);
	    rowArray.Add(QA_XLS_COL24_VALUE);
	    rowArray.Add(QA_XLS_COL25_VALUE);
	    rowArray.Add(QA_XLS_COL26_VALUE);
	    passFailResults.addRow(rowArray);


        ////////////////////////////////////////
	    ///
	    //	num failed Modes
	    //
        QA_XLS_COL2_VALUE = "";
        QA_XLS_COL3_VALUE = "";
        QA_XLS_COL4_VALUE = "";
        QA_XLS_COL5_VALUE = "";
        QA_XLS_COL6_VALUE = "";
        QA_XLS_COL7_VALUE = "";
        QA_XLS_COL8_VALUE = "";
        QA_XLS_COL9_VALUE = "";
        QA_XLS_COL10_VALUE = "";
        QA_XLS_COL11_VALUE = "";
        QA_XLS_COL12_VALUE = "";
        QA_XLS_COL13_VALUE = "";
        QA_XLS_COL14_VALUE = "";
        QA_XLS_COL15_VALUE = "";
        QA_XLS_COL16_VALUE = "";
        QA_XLS_COL17_VALUE = "";
        QA_XLS_COL18_VALUE = "";
        QA_XLS_COL19_VALUE = "";
        QA_XLS_COL20_VALUE = "";
        QA_XLS_COL21_VALUE = "";
        QA_XLS_COL22_VALUE = "";
        QA_XLS_COL23_VALUE = "";
        QA_XLS_COL24_VALUE = "";
        QA_XLS_COL25_VALUE = "";
        QA_XLS_COL26_VALUE = "";

		QA_XLS_COL2_VALUE.Format("%d", _numSuperModesRemoved);
	    QA_XLS_COL3_VALUE.AppendFormat("%d", _maxModeWidthPFAnalysis.numFailedModes);
	    QA_XLS_COL4_VALUE.AppendFormat("%d", _minModeWidthPFAnalysis.numFailedModes);
	    QA_XLS_COL5_VALUE.AppendFormat("%d", _lowerModalDistortionAnglePFAnalysis.numFailedModes);
	    QA_XLS_COL6_VALUE.AppendFormat("%d", _upperModalDistortionAnglePFAnalysis.numFailedModes);
	    QA_XLS_COL7_VALUE.AppendFormat("%d", _lowerModalDistortionAngleXPositionPFAnalysis.numFailedModes);
	    QA_XLS_COL8_VALUE.AppendFormat("%d", _upperModalDistortionAngleXPositionPFAnalysis.numFailedModes);
	    QA_XLS_COL9_VALUE.AppendFormat("%d", _lowerMaxModeBDCAreaXLengthPFAnalysis.numFailedModes);
	    QA_XLS_COL10_VALUE.AppendFormat("%d", _upperMaxModeBDCAreaXLengthPFAnalysis.numFailedModes);
	    QA_XLS_COL11_VALUE.AppendFormat("%d", _lowerMaxModeBDCAreaPFAnalysis.numFailedModes);
	    QA_XLS_COL12_VALUE.AppendFormat("%d", _upperMaxModeBDCAreaPFAnalysis.numFailedModes);
	    QA_XLS_COL13_VALUE.AppendFormat("%d", _lowerSumModeBDCAreasPFAnalysis.numFailedModes);
	    QA_XLS_COL14_VALUE.AppendFormat("%d", _upperSumModeBDCAreasPFAnalysis.numFailedModes);
	    QA_XLS_COL15_VALUE.AppendFormat("%d", _lowerSumModeBDCAreasPFAnalysis.numFailedModes);
	    QA_XLS_COL16_VALUE.AppendFormat("%d", _upperSumModeBDCAreasPFAnalysis.numFailedModes);
		QA_XLS_COL17_VALUE.AppendFormat("%d", _middleLineRMSValuesPFAnalysis.numFailedModes);
		QA_XLS_COL18_VALUE.AppendFormat("%d", _minMiddleLineSlopePFAnalysis.numFailedModes);
		QA_XLS_COL19_VALUE.AppendFormat("%d", _maxMiddleLineSlopePFAnalysis.numFailedModes);
		QA_XLS_COL20_VALUE.AppendFormat("%d", _maxPrGapPFAnalysis.numFailedModes);
		QA_XLS_COL21_VALUE.AppendFormat("%d", _maxLowerPrPFAnalysis.numFailedModes);
		QA_XLS_COL22_VALUE.AppendFormat("%d", _minUpperPrPFAnalysis.numFailedModes);
		QA_XLS_COL23_VALUE.AppendFormat("%d", _lowerMaxModeLineSlopePFAnalysis.numFailedModes);
		QA_XLS_COL24_VALUE.AppendFormat("%d", _upperMaxModeLineSlopePFAnalysis.numFailedModes);
		QA_XLS_COL25_VALUE.AppendFormat("%d", _lowerMinModeLineSlopePFAnalysis.numFailedModes);
		QA_XLS_COL26_VALUE.AppendFormat("%d", _upperMinModeLineSlopePFAnalysis.numFailedModes);

	    rowArray.RemoveAll();
	    rowArray.Add(QA_XLS_ROWTITLE4);
	    rowArray.Add(QA_XLS_COL2_VALUE);
	    rowArray.Add(QA_XLS_COL3_VALUE);
	    rowArray.Add(QA_XLS_COL4_VALUE);
	    rowArray.Add(QA_XLS_COL5_VALUE);
	    rowArray.Add(QA_XLS_COL6_VALUE);
	    rowArray.Add(QA_XLS_COL7_VALUE);
	    rowArray.Add(QA_XLS_COL8_VALUE);
	    rowArray.Add(QA_XLS_COL9_VALUE);
	    rowArray.Add(QA_XLS_COL10_VALUE);
 	    rowArray.Add(QA_XLS_COL11_VALUE);
 	    rowArray.Add(QA_XLS_COL12_VALUE);
 	    rowArray.Add(QA_XLS_COL13_VALUE);
 	    rowArray.Add(QA_XLS_COL14_VALUE);
 	    rowArray.Add(QA_XLS_COL15_VALUE);
 	    rowArray.Add(QA_XLS_COL16_VALUE);
 	    rowArray.Add(QA_XLS_COL17_VALUE);
 	    rowArray.Add(QA_XLS_COL18_VALUE);
 	    rowArray.Add(QA_XLS_COL19_VALUE);
 	    rowArray.Add(QA_XLS_COL20_VALUE);
 	    rowArray.Add(QA_XLS_COL21_VALUE);
 	    rowArray.Add(QA_XLS_COL22_VALUE);
 	    rowArray.Add(QA_XLS_COL23_VALUE);
 	    rowArray.Add(QA_XLS_COL24_VALUE);
 	    rowArray.Add(QA_XLS_COL25_VALUE);
 	    rowArray.Add(QA_XLS_COL26_VALUE);
		passFailResults.addRow(rowArray);


	    ////////////////////////////////////////
	    ///
	    //	thresholds
	    //
        QA_XLS_COL2_VALUE = "";
        QA_XLS_COL3_VALUE = "";
        QA_XLS_COL4_VALUE = "";
        QA_XLS_COL5_VALUE = "";
        QA_XLS_COL6_VALUE = "";
        QA_XLS_COL7_VALUE = "";
        QA_XLS_COL8_VALUE = "";
        QA_XLS_COL9_VALUE = "";
        QA_XLS_COL10_VALUE = "";
        QA_XLS_COL11_VALUE = "";
        QA_XLS_COL12_VALUE = "";
        QA_XLS_COL13_VALUE = "";
        QA_XLS_COL14_VALUE = "";
        QA_XLS_COL15_VALUE = "";
        QA_XLS_COL16_VALUE = "";
        QA_XLS_COL17_VALUE = "";
        QA_XLS_COL18_VALUE = "";
        QA_XLS_COL19_VALUE = "";
        QA_XLS_COL20_VALUE = "";
        QA_XLS_COL21_VALUE = "";
        QA_XLS_COL22_VALUE = "";
        QA_XLS_COL23_VALUE = "";
        QA_XLS_COL24_VALUE = "";
        QA_XLS_COL25_VALUE = "";
        QA_XLS_COL26_VALUE = "";

		QA_XLS_COL2_VALUE.AppendFormat(" > %g", _maxModeBordersRemovedAnalysis.threshold);
	    QA_XLS_COL3_VALUE.AppendFormat(" > %g", _maxModeWidthPFAnalysis.threshold);
	    QA_XLS_COL4_VALUE.AppendFormat(" < %g", _minModeWidthPFAnalysis.threshold);
	    QA_XLS_COL5_VALUE.AppendFormat(" > %g", _lowerModalDistortionAnglePFAnalysis.threshold);
	    QA_XLS_COL6_VALUE.AppendFormat(" > %g", _upperModalDistortionAnglePFAnalysis.threshold);
	    QA_XLS_COL7_VALUE.AppendFormat(" > %g", _lowerModalDistortionAngleXPositionPFAnalysis.threshold);
	    QA_XLS_COL8_VALUE.AppendFormat(" > %g", _upperModalDistortionAngleXPositionPFAnalysis.threshold);
	    QA_XLS_COL9_VALUE.AppendFormat(" > %g", _lowerMaxModeBDCAreaXLengthPFAnalysis.threshold);
	    QA_XLS_COL10_VALUE.AppendFormat(" > %g", _upperMaxModeBDCAreaXLengthPFAnalysis.threshold);
	    QA_XLS_COL11_VALUE.AppendFormat(" > %g", _lowerMaxModeBDCAreaPFAnalysis.threshold);
	    QA_XLS_COL12_VALUE.AppendFormat(" > %g", _upperMaxModeBDCAreaPFAnalysis.threshold);
	    QA_XLS_COL13_VALUE.AppendFormat(" > %g", _lowerSumModeBDCAreasPFAnalysis.threshold);
	    QA_XLS_COL14_VALUE.AppendFormat(" > %g", _upperSumModeBDCAreasPFAnalysis.threshold);
	    QA_XLS_COL15_VALUE.AppendFormat(" > %g", _lowerNumModeBDCAreasPFAnalysis.threshold);
	    QA_XLS_COL16_VALUE.AppendFormat(" > %g", _upperNumModeBDCAreasPFAnalysis.threshold);
	    QA_XLS_COL17_VALUE.AppendFormat(" > %g", _middleLineRMSValuesPFAnalysis.threshold);
	    QA_XLS_COL18_VALUE.AppendFormat(" < %g", _minMiddleLineSlopePFAnalysis.threshold);
	    QA_XLS_COL19_VALUE.AppendFormat(" > %g", _maxMiddleLineSlopePFAnalysis.threshold);
	    QA_XLS_COL20_VALUE.AppendFormat(" > %g", _maxPrGapPFAnalysis.threshold);
	    QA_XLS_COL21_VALUE.AppendFormat(" > %g", _maxLowerPrPFAnalysis.threshold);
	    QA_XLS_COL22_VALUE.AppendFormat(" < %g", _minUpperPrPFAnalysis.threshold);
	    QA_XLS_COL23_VALUE.AppendFormat(" > %g", _lowerMaxModeLineSlopePFAnalysis.threshold);
	    QA_XLS_COL24_VALUE.AppendFormat(" > %g", _upperMaxModeLineSlopePFAnalysis.threshold);
	    QA_XLS_COL25_VALUE.AppendFormat(" < %g", _lowerMinModeLineSlopePFAnalysis.threshold);
	    QA_XLS_COL26_VALUE.AppendFormat(" < %g", _upperMinModeLineSlopePFAnalysis.threshold);

	    rowArray.RemoveAll();
	    rowArray.Add(QA_XLS_ROWTITLE5);
	    rowArray.Add(QA_XLS_COL2_VALUE);
	    rowArray.Add(QA_XLS_COL3_VALUE);
	    rowArray.Add(QA_XLS_COL4_VALUE);
	    rowArray.Add(QA_XLS_COL5_VALUE);
	    rowArray.Add(QA_XLS_COL6_VALUE);
	    rowArray.Add(QA_XLS_COL7_VALUE);
	    rowArray.Add(QA_XLS_COL8_VALUE);
	    rowArray.Add(QA_XLS_COL9_VALUE);
	    rowArray.Add(QA_XLS_COL10_VALUE);
		rowArray.Add(QA_XLS_COL11_VALUE);
		rowArray.Add(QA_XLS_COL12_VALUE);
		rowArray.Add(QA_XLS_COL13_VALUE);
  		rowArray.Add(QA_XLS_COL14_VALUE);
  		rowArray.Add(QA_XLS_COL15_VALUE);
  		rowArray.Add(QA_XLS_COL16_VALUE);
  		rowArray.Add(QA_XLS_COL17_VALUE);
  		rowArray.Add(QA_XLS_COL18_VALUE);
  		rowArray.Add(QA_XLS_COL19_VALUE);
  		rowArray.Add(QA_XLS_COL20_VALUE);
  		rowArray.Add(QA_XLS_COL21_VALUE);
  		rowArray.Add(QA_XLS_COL22_VALUE);
  		rowArray.Add(QA_XLS_COL23_VALUE);
  		rowArray.Add(QA_XLS_COL24_VALUE);
  		rowArray.Add(QA_XLS_COL25_VALUE);
  		rowArray.Add(QA_XLS_COL26_VALUE);
	    passFailResults.addRow(rowArray);


        // Calculation parameters
        QA_XLS_COL1_VALUE = "";
        QA_XLS_COL2_VALUE = "";
        QA_XLS_COL3_VALUE = "";
        QA_XLS_COL4_VALUE = "";
        QA_XLS_COL5_VALUE = "";
        QA_XLS_COL6_VALUE = "";
        QA_XLS_COL7_VALUE = "";
        QA_XLS_COL8_VALUE = "";
        QA_XLS_COL9_VALUE = "";
        QA_XLS_COL10_VALUE = "";
        QA_XLS_COL11_VALUE = "";
        QA_XLS_COL12_VALUE = "";
        QA_XLS_COL13_VALUE = "";
        QA_XLS_COL14_VALUE = "";
        QA_XLS_COL15_VALUE = "";
        QA_XLS_COL16_VALUE = "";
        QA_XLS_COL17_VALUE = "";
        QA_XLS_COL18_VALUE = "";
        QA_XLS_COL19_VALUE = "";
        QA_XLS_COL20_VALUE = "";
        QA_XLS_COL21_VALUE = "";
        QA_XLS_COL22_VALUE = "";
        QA_XLS_COL23_VALUE = "";
        QA_XLS_COL24_VALUE = "";
        QA_XLS_COL25_VALUE = "";
        QA_XLS_COL26_VALUE = "";


	    rowArray.RemoveAll();
	    rowArray.Add(QA_XLS_COL1_VALUE);
	    rowArray.Add(QA_XLS_COL2_VALUE);
	    rowArray.Add(QA_XLS_COL3_VALUE);
	    rowArray.Add(QA_XLS_COL4_VALUE);
	    rowArray.Add(QA_XLS_COL5_VALUE);
	    rowArray.Add(QA_XLS_COL6_VALUE);
	    rowArray.Add(QA_XLS_COL7_VALUE);
	    rowArray.Add(QA_XLS_COL8_VALUE);
	    rowArray.Add(QA_XLS_COL9_VALUE);
	    rowArray.Add(QA_XLS_COL10_VALUE);
	    rowArray.Add(QA_XLS_COL11_VALUE);
	    rowArray.Add(QA_XLS_COL12_VALUE);
	    rowArray.Add(QA_XLS_COL13_VALUE);
	    rowArray.Add(QA_XLS_COL14_VALUE);
	    rowArray.Add(QA_XLS_COL15_VALUE);
	    rowArray.Add(QA_XLS_COL16_VALUE);
	    rowArray.Add(QA_XLS_COL17_VALUE);
	    rowArray.Add(QA_XLS_COL18_VALUE);
	    rowArray.Add(QA_XLS_COL19_VALUE);
	    rowArray.Add(QA_XLS_COL20_VALUE);
	    rowArray.Add(QA_XLS_COL21_VALUE);
	    rowArray.Add(QA_XLS_COL22_VALUE);
	    rowArray.Add(QA_XLS_COL23_VALUE);
	    rowArray.Add(QA_XLS_COL24_VALUE);
	    rowArray.Add(QA_XLS_COL25_VALUE);
	    rowArray.Add(QA_XLS_COL26_VALUE);
        passFailResults.addRow(rowArray);

        // add space
	    passFailResults.addEmptyLine();



	    //
	    // Find location of failing statistic(s)
	    //

	    // row title
	    rowArray.RemoveAll();
	    rowArray.Add("Index");
	    passFailResults.addRow(rowArray);


	    // final rows - Modes
	    for(unsigned int i = 0;  i<(unsigned int)_numSuperModes; i++)
	    {
            QA_XLS_COL1_VALUE = "";
            QA_XLS_COL2_VALUE = "";
            QA_XLS_COL3_VALUE = "";
            QA_XLS_COL4_VALUE = "";
            QA_XLS_COL5_VALUE = "";
            QA_XLS_COL6_VALUE = "";
            QA_XLS_COL7_VALUE = "";
            QA_XLS_COL8_VALUE = "";
            QA_XLS_COL9_VALUE = "";
            QA_XLS_COL10_VALUE = "";
            QA_XLS_COL11_VALUE = "";
            QA_XLS_COL12_VALUE = "";
            QA_XLS_COL13_VALUE = "";
            QA_XLS_COL14_VALUE = "";
            QA_XLS_COL15_VALUE = "";
            QA_XLS_COL16_VALUE = "";
            QA_XLS_COL17_VALUE = "";
            QA_XLS_COL18_VALUE = "";
            QA_XLS_COL19_VALUE = "";
            QA_XLS_COL20_VALUE = "";
            QA_XLS_COL21_VALUE = "";
            QA_XLS_COL22_VALUE = "";
            QA_XLS_COL23_VALUE = "";
            QA_XLS_COL24_VALUE = "";
			QA_XLS_COL25_VALUE = "";
			QA_XLS_COL26_VALUE = "";

            // Index location
		    QA_XLS_COL1_VALUE.AppendFormat(" %d", i);

		    // Max Mode Width
            std::vector<double>::iterator i_f = std::find(
                _maxModeWidthPFAnalysis.failedModes.begin(),
                _maxModeWidthPFAnalysis.failedModes.end(),
                i );
		    if(i < _maxModeWidths.size() && i_f != _maxModeWidthPFAnalysis.failedModes.end() )
			    QA_XLS_COL3_VALUE.AppendFormat("%f", _maxModeWidths[i]);


		    // Min Mode Width
            i_f = std::find(
				_minModeWidthPFAnalysis.failedModes.begin(),
                _minModeWidthPFAnalysis.failedModes.end(),
                i );
		    if(i < _minModeWidths.size() && i_f != _minModeWidthPFAnalysis.failedModes.end() )
			    QA_XLS_COL4_VALUE.AppendFormat("%f", _minModeWidths[i]);


		    // lower modal_distortion
            i_f = std::find(
                _lowerModalDistortionAnglePFAnalysis.failedModes.begin(),
                _lowerModalDistortionAnglePFAnalysis.failedModes.end(),
                i );
		    if( i < _lowerModalDistortionAngles.size()
            && i < _lowerModalDistortionAngleXPositions.size()
            && i_f != _lowerModalDistortionAnglePFAnalysis.failedModes.end())
            {
			    QA_XLS_COL5_VALUE.AppendFormat("%f", _lowerModalDistortionAngles[i]);
			    QA_XLS_COL7_VALUE.AppendFormat("%d", _lowerModalDistortionAngleXPositions[i]);
            }

		    // upper modal_distortion
            i_f = std::find(
                _upperModalDistortionAnglePFAnalysis.failedModes.begin(),
                _upperModalDistortionAnglePFAnalysis.failedModes.end(),
                i );
		    if( i < _upperModalDistortionAngles.size()
            && i < _upperModalDistortionAngleXPositions.size()
            && i_f != _upperModalDistortionAnglePFAnalysis.failedModes.end())
            {
			    QA_XLS_COL6_VALUE.AppendFormat("%f", _upperModalDistortionAngles[i]);
			    QA_XLS_COL8_VALUE.AppendFormat("%d", _upperModalDistortionAngleXPositions[i]);
            }

		    // Lower Max Bdc X-Length
            i_f = std::find(
                _lowerMaxModeBDCAreaXLengthPFAnalysis.failedModes.begin(),
                _lowerMaxModeBDCAreaXLengthPFAnalysis.failedModes.end(),
                i );
		    if(i < _lowerMaxModeBDCAreaXLengths.size() && i_f != _lowerMaxModeBDCAreaXLengthPFAnalysis.failedModes.end() )
			    QA_XLS_COL9_VALUE.AppendFormat("%f", _lowerMaxModeBDCAreaXLengths[i]);

		    // Upper Max Bdc X-Length
            i_f = std::find(
                _upperMaxModeBDCAreaXLengthPFAnalysis.failedModes.begin(),
                _upperMaxModeBDCAreaXLengthPFAnalysis.failedModes.end(),
                i );
		    if(i < _upperMaxModeBDCAreaXLengths.size() && i_f != _upperMaxModeBDCAreaXLengthPFAnalysis.failedModes.end() )
			    QA_XLS_COL10_VALUE.AppendFormat("%f", _upperMaxModeBDCAreaXLengths[i]);

		    // Lower Max Bdc Area
            i_f = std::find(
                _lowerMaxModeBDCAreaPFAnalysis.failedModes.begin(),
                _lowerMaxModeBDCAreaPFAnalysis.failedModes.end(),
                i );
		    if(i < _lowerMaxModeBDCAreas.size() && i_f != _lowerMaxModeBDCAreaPFAnalysis.failedModes.end() )
			    QA_XLS_COL11_VALUE.AppendFormat("%f", _lowerMaxModeBDCAreas[i]);

		    // Upper Max Bdc Area
            i_f = std::find(
                _upperMaxModeBDCAreaPFAnalysis.failedModes.begin(),
                _upperMaxModeBDCAreaPFAnalysis.failedModes.end(),
                i );
		    if(i < _upperMaxModeBDCAreas.size() && i_f != _upperMaxModeBDCAreaPFAnalysis.failedModes.end() )
			    QA_XLS_COL12_VALUE.AppendFormat("%f", _upperMaxModeBDCAreas[i]);

		    // Lower Sum of Bdc Areas
            i_f = std::find(
                _lowerSumModeBDCAreasPFAnalysis.failedModes.begin(),
                _lowerSumModeBDCAreasPFAnalysis.failedModes.end(),
                i );
		    if(i < _lowerSumModeBDCAreas.size() && i_f != _lowerSumModeBDCAreasPFAnalysis.failedModes.end() )
			    QA_XLS_COL13_VALUE.AppendFormat("%f", _lowerSumModeBDCAreas[i]);

		    // Upper Sum of Bdc Areas
            i_f = std::find(
                _upperSumModeBDCAreasPFAnalysis.failedModes.begin(),
                _upperSumModeBDCAreasPFAnalysis.failedModes.end(),
                i );
		    if(i < _upperSumModeBDCAreas.size() && i_f != _upperSumModeBDCAreasPFAnalysis.failedModes.end() )
			    QA_XLS_COL14_VALUE.AppendFormat("%f", _upperSumModeBDCAreas[i]);

		    // Lower Number of Bdcs
            i_f = std::find(
                _lowerNumModeBDCAreasPFAnalysis.failedModes.begin(),
                _lowerNumModeBDCAreasPFAnalysis.failedModes.end(),
                i );
		    if(i < _lowerNumModeBDCAreaSamples.size() && i_f != _lowerNumModeBDCAreasPFAnalysis.failedModes.end() )
			    QA_XLS_COL15_VALUE.AppendFormat("%f", _lowerNumModeBDCAreaSamples[i]);

		    // Upper Number of Bdcs
            i_f = std::find(
                _upperNumModeBDCAreasPFAnalysis.failedModes.begin(),
                _upperNumModeBDCAreasPFAnalysis.failedModes.end(),
                i );
		    if(i < _upperNumModeBDCAreaSamples.size() && i_f != _upperNumModeBDCAreasPFAnalysis.failedModes.end() )
			    QA_XLS_COL16_VALUE.AppendFormat("%f", _upperNumModeBDCAreaSamples[i]);

		    // Max Middle Line RMS Value
            i_f = std::find(
                _middleLineRMSValuesPFAnalysis.failedModes.begin(),
                _middleLineRMSValuesPFAnalysis.failedModes.end(),
                i );
		    if(i < _middleLineRMSValues.size() && i_f != _middleLineRMSValuesPFAnalysis.failedModes.end() )
			    QA_XLS_COL17_VALUE.AppendFormat("%f", _middleLineRMSValues[i]);

			// Min Middle Line Slope
            i_f = std::find(
                _minMiddleLineSlopePFAnalysis.failedModes.begin(),
                _minMiddleLineSlopePFAnalysis.failedModes.end(),
                i );
		    if(i < _middleLineSlopes.size() && i_f != _minMiddleLineSlopePFAnalysis.failedModes.end() )
			    QA_XLS_COL18_VALUE.AppendFormat("%f", _middleLineSlopes[i]);

		    // Max Middle Line Slope
            i_f = std::find(
                _maxMiddleLineSlopePFAnalysis.failedModes.begin(),
                _maxMiddleLineSlopePFAnalysis.failedModes.end(),
                i );
		    if(i < _middleLineSlopes.size() && i_f != _maxMiddleLineSlopePFAnalysis.failedModes.end() )
			    QA_XLS_COL19_VALUE.AppendFormat("%f", _middleLineSlopes[i]);

		    // Max Pr Gap
            i_f = std::find(
                _maxPrGapPFAnalysis.failedModes.begin(),
                _maxPrGapPFAnalysis.failedModes.end(),
                i );
		    if(i < (unsigned int)(_continuityValues.size()) && i_f != _maxPrGapPFAnalysis.failedModes.end() )
			    QA_XLS_COL20_VALUE.AppendFormat("%f", _maxPrGap);

		    // Max Lower Pr
            i_f = std::find(
                _maxLowerPrPFAnalysis.failedModes.begin(),
                _maxLowerPrPFAnalysis.failedModes.end(),
                i );
		    if(i < (unsigned int)(_continuityValues.size()) && i_f != _maxLowerPrPFAnalysis.failedModes.end() )
			    QA_XLS_COL21_VALUE.AppendFormat("%f", _maxLowerPr);

		    // Min Upper Pr
            i_f = std::find(
                _minUpperPrPFAnalysis.failedModes.begin(),
                _minUpperPrPFAnalysis.failedModes.end(),
                i );
		    if(i < (unsigned int)(_continuityValues.size()) && i_f != _minUpperPrPFAnalysis.failedModes.end() )
			    QA_XLS_COL22_VALUE.AppendFormat("%f", _minUpperPr);

		    // Lower max mode line slope
            i_f = std::find(
                _lowerMaxModeLineSlopePFAnalysis.failedModes.begin(),
                _lowerMaxModeLineSlopePFAnalysis.failedModes.end(),
                i );
		    if(i < (unsigned int)(_maxModeLowerSlopes.size()) && i_f != _lowerMaxModeLineSlopePFAnalysis.failedModes.end() )
			    QA_XLS_COL23_VALUE.AppendFormat("%f", _maxModeLowerSlopes[i]);

		    // Upper max mode line slope
            i_f = std::find(
                _upperMaxModeLineSlopePFAnalysis.failedModes.begin(),
                _upperMaxModeLineSlopePFAnalysis.failedModes.end(),
                i );
		    if(i < (unsigned int)(_maxModeUpperSlopes.size()) && i_f != _upperMaxModeLineSlopePFAnalysis.failedModes.end() )
			    QA_XLS_COL24_VALUE.AppendFormat("%f", _maxModeUpperSlopes[i]);


		    // Lower min mode line slope
            i_f = std::find(
                _lowerMinModeLineSlopePFAnalysis.failedModes.begin(),
                _lowerMinModeLineSlopePFAnalysis.failedModes.end(),
                i );
		    if(i < (unsigned int)(_minModeLowerSlopes.size()) && i_f != _lowerMinModeLineSlopePFAnalysis.failedModes.end() )
			    QA_XLS_COL25_VALUE.AppendFormat("%f", _minModeLowerSlopes[i]);

		    // Upper min mode line slope
            i_f = std::find(
                _upperMinModeLineSlopePFAnalysis.failedModes.begin(),
                _upperMinModeLineSlopePFAnalysis.failedModes.end(),
                i );
		    if(i < (unsigned int)(_minModeUpperSlopes.size()) && i_f != _upperMinModeLineSlopePFAnalysis.failedModes.end() )
			    QA_XLS_COL26_VALUE.AppendFormat("%f", _minModeUpperSlopes[i]);



			rowArray.RemoveAll();
	        rowArray.Add(QA_XLS_COL1_VALUE);
	        rowArray.Add(QA_XLS_COL2_VALUE);
	        rowArray.Add(QA_XLS_COL3_VALUE);
	        rowArray.Add(QA_XLS_COL4_VALUE);
	        rowArray.Add(QA_XLS_COL5_VALUE);
	        rowArray.Add(QA_XLS_COL6_VALUE);
	        rowArray.Add(QA_XLS_COL7_VALUE);
	        rowArray.Add(QA_XLS_COL8_VALUE);
	        rowArray.Add(QA_XLS_COL9_VALUE);
	        rowArray.Add(QA_XLS_COL10_VALUE);
	        rowArray.Add(QA_XLS_COL11_VALUE);
 	        rowArray.Add(QA_XLS_COL12_VALUE);
 	        rowArray.Add(QA_XLS_COL13_VALUE);
 	        rowArray.Add(QA_XLS_COL14_VALUE);
 	        rowArray.Add(QA_XLS_COL15_VALUE);
 	        rowArray.Add(QA_XLS_COL16_VALUE);
 	        rowArray.Add(QA_XLS_COL17_VALUE);
 	        rowArray.Add(QA_XLS_COL18_VALUE);
 	        rowArray.Add(QA_XLS_COL19_VALUE);
 	        rowArray.Add(QA_XLS_COL20_VALUE);
 	        rowArray.Add(QA_XLS_COL21_VALUE);
 	        rowArray.Add(QA_XLS_COL22_VALUE);
 	        rowArray.Add(QA_XLS_COL23_VALUE);
 	        rowArray.Add(QA_XLS_COL24_VALUE);
 	        rowArray.Add(QA_XLS_COL25_VALUE);
 	        rowArray.Add(QA_XLS_COL26_VALUE);
	        passFailResults.addRow(rowArray);

	    }
    }
    else // qaWasRun == false
    {
        CStringArray error_msg_row;
		error_msg_row.Add("Error : Function called before Pass Fail Analysis completed.");
        passFailResults.addRow(error_msg_row);
    }

	return err;
}

int	
OverallModeMapAnalysis::
numSuperModes()
{
	return _numSuperModes;
}

PassFailThresholds
OverallModeMapAnalysis::
getPassFailThresholds()
{
	PassFailThresholds thresholds;

	thresholds._maxModeBordersRemoved			= CG_REG->get_DSDBR01_OMPF_max_mode_borders_removed_threshold();
	thresholds._maxModeWidth					= CG_REG->get_DSDBR01_OMPF_max_mode_width_threshold();
	thresholds._minModeWidth					= CG_REG->get_DSDBR01_OMPF_min_mode_width_threshold();
	thresholds._maxModeBDCArea					= CG_REG->get_DSDBR01_OMPF_max_mode_bdc_area_threshold();
	thresholds._maxModeBDCAreaXLength			= CG_REG->get_DSDBR01_OMPF_max_mode_bdc_area_x_length_threshold();
	thresholds._sumModeBDCAreas					= CG_REG->get_DSDBR01_OMPF_sum_mode_bdc_areas_threshold();
	thresholds._numModeBDCAreas					= CG_REG->get_DSDBR01_OMPF_num_mode_bdc_areas_threshold();
	thresholds._modalDistortionAngle			= CG_REG->get_DSDBR01_OMPF_modal_distortion_angle_threshold();
	thresholds._modalDistortionAngleXPosition	= CG_REG->get_DSDBR01_OMPF_modal_distortion_angle_x_pos_threshold();
	thresholds._middleLineRMSValue				= CG_REG->get_DSDBR01_OMPF_max_middle_line_rms_value();
	thresholds._minMiddleLineSlope				= CG_REG->get_DSDBR01_OMPF_min_middle_line_slope();
	thresholds._maxMiddleLineSlope				= CG_REG->get_DSDBR01_OMPF_max_middle_line_slope();
	thresholds._maxPrGap						= CG_REG->get_DSDBR01_OMPF_max_pr_gap();
	thresholds._maxLowerPr						= CG_REG->get_DSDBR01_OMPF_max_lower_pr();
	thresholds._minUpperPr						= CG_REG->get_DSDBR01_OMPF_min_upper_pr();
	thresholds._maxModeLineSlope				= CG_REG->get_DSDBR01_OMPF_max_mode_line_slope_threshold();
	thresholds._minModeLineSlope				= CG_REG->get_DSDBR01_OMPF_min_mode_line_slope_threshold();

	return thresholds;
}
//
//
///	End of OverallModeMapAnalysis.cpp
///////////////////////////////////////////////////////////////////////////////
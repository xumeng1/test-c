// Copyright (c) 2002 Tsunami Photonics Ltd. All Rights Reserved
//
// FileName    : Vector.cpp
// Description : Implementation of of Vector class functions
//
// Rev#  | Date          | Author               | Description of change
// --------------------------------------------------------------
// 0.1   | 02 Oct 2002   | Frank D'Arcy         | Initial Draft
//

#include "stdafx.h"

#include "Vector.h"
#include <algorithm>
#include <math.h>
#include <atlstr.h>

#define new DEBUG_NEW

Vector::
Vector()
{
	_values.clear();
}

Vector::Vector( std::vector<double> &values)
{
	_values.clear();
    _values = values;
}

Vector::~Vector( )
{
}

std::vector<double>*
Vector::getValues()
{
    return &_values;
}

void
Vector::
push_back(double value)
{
	_values.push_back(value);
}


double
Vector::maxOfValues()
{
    double maximum = 0;
    if( size() > 0 )
    {
		std::vector<double>::iterator iter = std::max_element( _values.begin(), _values.end() );
        maximum = *iter;
    }
    return maximum;
}

double
Vector::minOfValues()
{
    double minimum = 0;
    if( size() > 0 )
    {
        std::vector<double>::iterator iter = std::min_element( _values.begin(), _values.end() );
        minimum = *iter;
    }
    return minimum;
}

double
Vector::meanOfValues()
{
    double mean = 0;
    if( _values.size() > 0 )
    {
        unsigned int count = 0;
		std::vector<double>::iterator iter;
        iter = _values.begin();
        while( iter!= _values.end() )
        {
            mean += *iter;
            iter++;
            count++;
        }
        if( count > 1 )
        {
            mean /= count;
        }
    }
    return mean;
}

double
Vector::
sumOfValues()
{
    double sum = 0;
    if( _values.size() > 0 )
    {
		std::vector<double>::iterator iter;
        iter = _values.begin();
        while( iter!= _values.end() )
        {
            sum += *iter;
            iter++;
        }
    }

    return sum;
}

double
Vector::stdDevOfValues()
{
    double stdev = sqrt( varianceOfValues() );
    return stdev;
}

double
Vector::varianceOfValues()
{
    double variance = 0;
    if( _values.size() > 0 )
    {
        double sum = 0;
        unsigned long count = 0;
        double mean = meanOfValues();

		std::vector<double>::iterator iter;
        iter = _values.begin();
        while( iter!= _values.end() )
        {
            sum += pow( *iter - mean , 2 );
            iter++;
            count++;
        }
        if( count > 1 )
        {
            variance = sum / (count - 1); 
        }
    }
    return variance;
}

double
Vector::medianOfValues()
{
    // The median of the sorted vector s[i] where i = 0, n-1 is defined as
    // s[(n-1)/2] where n is odd
    // ( s[(n/2)-1] + s[n/2] ) / 2 where n is even

    double median = 0;
    unsigned long n = size();

    if(n>0)
    {
        // make a copy of the vector
        std::vector<double> sorted_values(_values);
        std::sort(sorted_values.begin(),sorted_values.end());

        if( n%2 == 0 )
        { // even
            median = ( sorted_values[(n/2)-1] + sorted_values[n/2] )/ 2;
        }
        else
        { // odd
            median = sorted_values[(n-1)/2];
        }
    }
    return median;
}

double
Vector::maxGapInValues()
{
    double maxGap = 0;
    unsigned long n = size();

    if(n>0)
    {
        // make a copy of the vector
        std::vector<double> sorted_values(_values);
        std::sort(sorted_values.begin(),sorted_values.end());
		
		for(unsigned long i=0; i<n-1; i++)
		{
			double gap = sorted_values[i+1] - sorted_values[i];

			if(gap > maxGap)
				maxGap = gap;
		}
    }

    return maxGap;
}
///////////////////////////////////////////////////////////////////////////////
///
//	Function	:
//
//	Returns		:
//
//	Parameters	:	QA_Attrib - what QA stat are we interested in 
//
//	Description	:	private method which decides what statistic we are looking for
//
double 
Vector::
getVectorAttribute(VectorAttribute attrib)
{
	double result = -1;
	switch(attrib)
	{	// Max,	Min, Mean, Sum, count, StandardDev, Variance
		case max:
			result = maxOfValues();
			break;
		case min:
			result = minOfValues();
			break;
		case mean:
			result = meanOfValues();
			break;
		case sum:
			result = sumOfValues();
			break;
		case count:
			result = size();
			break;
		case stdDev:
			result = stdDevOfValues();
			break;
		case variance:
			result = varianceOfValues();
			break;
		case median:
			result = medianOfValues();
			break;
		case maxGap:
			result = maxGapInValues();

		default:
			CString error;
			error.Format("Vector::getQaAttrib(%d). Invalid parameter passed",attrib);
	}
	return result;
}

int
Vector::
size()
{
    int size = 0;
    if( _values.size() > 0 )
    {
        size = (int)_values.size();
    }
    return size;
}

void
Vector::
sort()
{
	std::sort(_values.begin(), _values.end());
}

double
Vector::
operator[](int index)
{
    double valueAtIndex = 0;

    if( index >= 0 && index < size() )
		valueAtIndex = _values[index];

    return valueAtIndex;
}

void
Vector::
operator=(std::vector<double> vectorOfDoubles)
{
	_values.clear();

    _values = vectorOfDoubles;
}

void
Vector::
operator+(std::vector<double> vectorOfDoubles)
{
	std::vector<double>::iterator iter;
	for(iter=vectorOfDoubles.begin(); iter!=vectorOfDoubles.end(); iter++)
	{
		_values.push_back(*iter);
	}
}

void
Vector::
clear()
{
	_values.clear();
}
// Copyright (c) 2002 Tsunami Photonics Ltd. All Rights Reserved
//
// FileName    : Vector.cpp
// Description : Implementation of of Vector class functions
//
// Rev#  | Date          | Author               | Description of change
// --------------------------------------------------------------
// 0.1   | 02 Oct 2002   | Frank D'Arcy         | Initial Draft
//
#include "stdafx.h"

#include "PassFailThresholds.h"

#define new DEBUG_NEW

#define DEFAULT_MAX_MODE_BORDERS_REMOVED	0
#define DEFAULT_MAX_MODE_WIDTH				40
#define DEFAULT_MIN_MODE_WIDTH				10
#define DEFAULT_MAX_MODE_BDC_AREA			30
#define	DEFAULT_MAX_MODE_BDC_AREA_X_LENGTH	20
#define	DEFAULT_SUM_MODE_BDC_AREAS			60
#define	DEFAULT_NUM_MODE_BDC_AREAS			8
#define DEFAULT_MAX_MODAL_DISTORTION_ANGLE	19
#define DEFAULT_MAX_MODAL_DISTORTION_ANGLE_X_POS 80
#define DEFAULT_MEAN_PERC_WORKING_REGION	50
#define DEFAULT_MIN_PERC_WORKING_REGION		39
#define DEFAULT_MIDDLE_LINE_RMS_VALUE		5
#define DEFAULT_MIN_MIDDLE_LINE_SLOPE		0
#define DEFAULT_MAX_MIDDLE_LINE_SLOPE		90
#define DEFAULT_MAX_CONTINUITY_SPACING		0.5
#define DEFAULT_MAX_PR						0.9
#define DEFAULT_MIN_PR						0.1
#define DEFAULT_MAX_MODE_LINE_SLOPE			5
#define DEFAULT_MIN_MODE_LINE_SLOPE			-5

PassFailThresholds::
PassFailThresholds()
{
	setThresholdsToDefault();
}

PassFailThresholds::~PassFailThresholds( )
{
}

void
PassFailThresholds::
setThresholdsToDefault()
{
	_maxModeBordersRemoved	= DEFAULT_MAX_MODE_BORDERS_REMOVED;
	_maxModeWidth			= DEFAULT_MAX_MODE_WIDTH;
	_minModeWidth			= DEFAULT_MIN_MODE_WIDTH;
	_maxModeBDCArea			= DEFAULT_MAX_MODE_BDC_AREA;
	_maxModeBDCAreaXLength	= DEFAULT_MAX_MODE_BDC_AREA_X_LENGTH;
	_sumModeBDCAreas		= DEFAULT_SUM_MODE_BDC_AREAS;
	_numModeBDCAreas		= DEFAULT_NUM_MODE_BDC_AREAS;
	_modalDistortionAngle	= DEFAULT_MAX_MODAL_DISTORTION_ANGLE;
	_modalDistortionAngleXPosition = DEFAULT_MAX_MODAL_DISTORTION_ANGLE_X_POS;
	_meanPercWorkingRegion	= DEFAULT_MEAN_PERC_WORKING_REGION;
	_minPercWorkingRegion	= DEFAULT_MIN_PERC_WORKING_REGION;
	_middleLineRMSValue		= DEFAULT_MIDDLE_LINE_RMS_VALUE;
	_maxMiddleLineSlope		= DEFAULT_MAX_MIDDLE_LINE_SLOPE;
	_minMiddleLineSlope		= DEFAULT_MIN_MIDDLE_LINE_SLOPE;
	_maxPrGap				= DEFAULT_MAX_CONTINUITY_SPACING;
	_maxLowerPr					= DEFAULT_MAX_PR;
	_minUpperPr					= DEFAULT_MIN_PR;
	_maxModeLineSlope		= DEFAULT_MAX_MODE_LINE_SLOPE;
	_minModeLineSlope		= DEFAULT_MIN_MODE_LINE_SLOPE;
}

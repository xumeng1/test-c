// Copyright (c) 2004 PXIT Ltd. All Rights Reserved

///////////////////////////////////////////////////////////////////////////////
///
//	Class	:
//
//	Contents:
//
//	Author	:
//
///
///////////////////////////////////////////////////////////////////////////////
///
//
#ifndef __SUPERMODEMAPANALYSIS_H_
#define __SUPERMODEMAPANALYSIS_H_

#pragma once

//
///////////////////////////////////////////////////////////////////////////////
//
#include <afx.h>
#include <vector>

#include "PassFailAnalysis.h"
#include "HysteresisAnalysis.h"
#include "PassFailThresholds.h"
#include "QAThresholds.h"
#include "CLoseGridFile.h"

using namespace std;

/////////////////////////////////////////////////////////////////////

class SuperModeMapAnalysis
{
public:
	SuperModeMapAnalysis();
	~SuperModeMapAnalysis(void);

	////////////////////////////////////////
	///
	//	Error codes
	//
	typedef enum rcode
	{
		ok = 0,
		qaerror
	};
	//
	///////////////////////////////////////

	///////////////////////////////////////////////////////
	///
	//	 Public functions
	//
	rcode	runAnalysis(short			superModeNumber,
						const		 	vector<ModeBoundaryLine>& _upperLines,
						const			vector<ModeBoundaryLine>& _lowerLines,
						const			vector<ModeBoundaryLine>& _middleLines,
						short			numUpperLinesRemoved,
						short			numLowerLinesRemoved,
						short			numMiddleLinesRemoved,
						int				xAxisLength,
						int				yAxisLength,
						vector<double>	continuityValues,
						CString			resultsDir,
						CString			laserId,
						CString			dateTimeStamp);

	///////////////////////////////////////////////////////
	///
	//	public attributes
	//
	PassFailThresholds	getPassFailThresholds();

	void	getQAThresholds();

	///////////////////////////////////
	///
	//
	PassFailThresholds	_lmPassFailThresholds; // Pass fail thresholds for the longitudinal mode

	//Data
	PassFailAnalysis	_maxUpperModeBordersRemovedPFAnalysis;
	PassFailAnalysis	_maxLowerModeBordersRemovedPFAnalysis;
	PassFailAnalysis	_maxMiddleLineRemovedPFAnalysis;

	PassFailAnalysis	_maxForwardModeWidthPFAnalysis;
	PassFailAnalysis	_minForwardModeWidthPFAnalysis;

	PassFailAnalysis	_maxReverseModeWidthPFAnalysis;
	PassFailAnalysis	_minReverseModeWidthPFAnalysis;

	PassFailAnalysis	_meanPercWorkingRegionPFAnalysis;
	PassFailAnalysis	_minPercWorkingRegionPFAnalysis;

	PassFailAnalysis	_maxForwardLowerModeBDCAreaPFAnalysis;
	PassFailAnalysis	_maxForwardLowerModeBDCAreaXLengthPFAnalysis;
	PassFailAnalysis	_sumForwardLowerModeBDCAreasPFAnalysis;
	PassFailAnalysis	_numForwardLowerModeBDCAreasPFAnalysis;
	PassFailAnalysis	_maxForwardUpperModeBDCAreaPFAnalysis;
	PassFailAnalysis	_maxForwardUpperModeBDCAreaXLengthPFAnalysis;
	PassFailAnalysis	_sumForwardUpperModeBDCAreasPFAnalysis;
	PassFailAnalysis	_numForwardUpperModeBDCAreasPFAnalysis;

	PassFailAnalysis	_maxReverseLowerModeBDCAreaPFAnalysis;
	PassFailAnalysis	_maxReverseLowerModeBDCAreaXLengthPFAnalysis;
	PassFailAnalysis	_sumReverseLowerModeBDCAreasPFAnalysis;
	PassFailAnalysis	_numReverseLowerModeBDCAreasPFAnalysis;
	PassFailAnalysis	_maxReverseUpperModeBDCAreaPFAnalysis;
	PassFailAnalysis	_maxReverseUpperModeBDCAreaXLengthPFAnalysis;
	PassFailAnalysis	_sumReverseUpperModeBDCAreasPFAnalysis;
	PassFailAnalysis	_numReverseUpperModeBDCAreasPFAnalysis;

	PassFailAnalysis	_forwardLowerModalDistortionAnglePFAnalysis;
	PassFailAnalysis	_forwardUpperModalDistortionAnglePFAnalysis;
	PassFailAnalysis	_reverseLowerModalDistortionAnglePFAnalysis;
	PassFailAnalysis	_reverseUpperModalDistortionAnglePFAnalysis;

	PassFailAnalysis	_forwardLowerModalDistortionAngleXPositionPFAnalysis;
	PassFailAnalysis	_forwardUpperModalDistortionAngleXPositionPFAnalysis;
	PassFailAnalysis	_reverseLowerModalDistortionAngleXPositionPFAnalysis;
	PassFailAnalysis	_reverseUpperModalDistortionAngleXPositionPFAnalysis;


	PassFailAnalysis	_middleLineRMSValuesPFAnalysis;
	PassFailAnalysis	_maxMiddleLineSlopePFAnalysis;
	PassFailAnalysis	_minMiddleLineSlopePFAnalysis;
	PassFailAnalysis	_maxPrGapPFAnalysis;

	PassFailAnalysis	_maxForwardLowerModeSlopePFAnalysis;
	PassFailAnalysis	_maxForwardUpperModeSlopePFAnalysis;
	PassFailAnalysis	_maxReverseLowerModeSlopePFAnalysis;
	PassFailAnalysis	_maxReverseUpperModeSlopePFAnalysis;

	//
	/////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////
	///
	//	 private functions
	//
	void	init();

	void	createModeAnalysisVectors(const vector<ModeBoundaryLine>&	_upperLines, 
									const vector<ModeBoundaryLine>&		_lowerLines);

	void	createHysteresisAnalysisVector(const vector<ModeAnalysis>&	forwardModes, 
										const vector<ModeAnalysis>&		reverseModes);

	rcode	runQaAnalysis();
	rcode	runForwardModesQaAnalysis();
	rcode	runReverseModesQaAnalysis();
	rcode	runHysteresisQaAnalysis();
	rcode	runMiddleLinesQaAnalysis();

	rcode	runPassFailAnalysis();

	CString	createQaResultsAbsFilePath(CString	resultsDir,
									CString		laserId,
									CString		dateTimeStamp);
	CString	createPassFailResultsAbsFilePath(CString	resultsDir,
											CString		laserId,
											CString		dateTimeStamp);

	rcode	writeQaResultsToFile();
	rcode	writePassFailResultsToFile( CLoseGridFile &passFailResults );

	void	getQAError(int		qaError,
					int&		errorNumber,
					CString&	errorMsg);

	bool	_screeningPassed;

	int		_qaDetectedError;

	///////////////////////////////////////////////////////
	///
	//	Forward stats
	//
	void	getForwardModeWidths();
	void	getMaxForwardModeWidths();
	void	getMinForwardModeWidths();
	void	getMeanForwardModeWidths();
	void	getSumForwardModeWidths();
	void	getVarForwardModeWidths();
	void	getStdDevForwardModeWidths();
	void	getMedianForwardModeWidths();
	void	getNumForwardModeWidthSamples();

	///////////////////////////////////////////////////////

	void	getForwardModeSlopes();
	void	getMaxForwardModeSlopes();
	void	getMinForwardModeSlopes();
	void	getMeanForwardModeSlopes();
	void	getSumForwardModeSlopes();
	void	getVarForwardModeSlopes();
	void	getStdDevForwardModeSlopes();
	void	getMedianForwardModeSlopes();
	void	getNumForwardModeSlopeSamples();

	///////////////////////////////////////////////////////

	void	getForwardModalDistortionAngles();
	void	getForwardModalDistortionAngleXPositions();

	///////////////////////////////////////////////////////

	void	getForwardModeBDCAreas();
	void	getMaxForwardModeBDCAreas();
	void	getMinForwardModeBDCAreas();
	void	getMeanForwardModeBDCAreas();
	void	getSumForwardModeBDCAreas();
	void	getVarForwardModeBDCAreas();
	void	getStdDevForwardModeBDCAreas();
	void	getMedianForwardModeBDCAreas();
	void	getNumForwardModeBDCAreaSamples();

	///////////////////////////////////////////////////////

	void	getForwardModeBDCAreaXLengths();
	void	getMaxForwardModeBDCAreaXLengths();
	void	getMinForwardModeBDCAreaXLengths();
	void	getMeanForwardModeBDCAreaXLengths();
	void	getSumForwardModeBDCAreaXLengths();
	void	getVarForwardModeBDCAreaXLengths();
	void	getStdDevForwardModeBDCAreaXLengths();
	void	getMedianForwardModeBDCAreaXLengths();
	void	getNumForwardModeBDCAreaXLengthSamples();


	//
	///////////////////////////////////////////////////////////////
	//

						// vector of mode width vectors
	vector<Vector>	_forwardModeWidths;
	vector<double>	_maxForwardModeWidths;
	vector<double>	_minForwardModeWidths;
	vector<double>	_meanForwardModeWidths;
	vector<double>	_sumForwardModeWidths;
	vector<double>	_varForwardModeWidths;
	vector<double>	_stdDevForwardModeWidths;
	vector<double>	_medianForwardModeWidths;
	vector<double>	_numForwardModeWidthSamples;

	///////////////////////////////////////

	vector<Vector>	_forwardLowerModeSlopes;
	vector<double>	_maxForwardLowerModeSlopes;
	vector<double>	_minForwardLowerModeSlopes;
	vector<double>	_meanForwardLowerModeSlopes;
	vector<double>	_sumForwardLowerModeSlopes;
	vector<double>	_varForwardLowerModeSlopes;
	vector<double>	_stdDevForwardLowerModeSlopes;
	vector<double>	_medianForwardLowerModeSlopes;
	vector<double>	_numForwardLowerModeSlopeSamples;

	vector<Vector>	_forwardUpperModeSlopes;
	vector<double>	_maxForwardUpperModeSlopes;
	vector<double>	_minForwardUpperModeSlopes;
	vector<double>	_meanForwardUpperModeSlopes;
	vector<double>	_sumForwardUpperModeSlopes;
	vector<double>	_varForwardUpperModeSlopes;
	vector<double>	_stdDevForwardUpperModeSlopes;
	vector<double>	_medianForwardUpperModeSlopes;
	vector<double>	_numForwardUpperModeSlopeSamples;

	///////////////////////////////////////

	vector<double>	_forwardLowerModalDistortionAngles;
	vector<double>	_forwardUpperModalDistortionAngles;

	vector<double>	_forwardLowerModalDistortionAngleXPositions;
	vector<double>	_forwardUpperModalDistortionAngleXPositions;

	///////////////////////////////////////

	vector<Vector>	_forwardLowerModeBDCAreas;
	vector<double>	_maxForwardLowerModeBDCAreas;
	vector<double>	_minForwardLowerModeBDCAreas;
	vector<double>	_meanForwardLowerModeBDCAreas;
	vector<double>	_sumForwardLowerModeBDCAreas;
	vector<double>	_varForwardLowerModeBDCAreas;
	vector<double>	_stdDevForwardLowerModeBDCAreas;
	vector<double>	_medianForwardLowerModeBDCAreas;
	vector<double>	_numForwardLowerModeBDCAreaSamples;
	//
	vector<Vector>	_forwardUpperModeBDCAreas;
	vector<double>	_maxForwardUpperModeBDCAreas;
	vector<double>	_minForwardUpperModeBDCAreas;
	vector<double>	_meanForwardUpperModeBDCAreas;
	vector<double>	_sumForwardUpperModeBDCAreas;
	vector<double>	_varForwardUpperModeBDCAreas;
	vector<double>	_stdDevForwardUpperModeBDCAreas;
	vector<double>	_medianForwardUpperModeBDCAreas;
	vector<double>	_numForwardUpperModeBDCAreaSamples;

	///////////////////////////////////////

	vector<Vector>	_forwardLowerModeBDCAreaXLengths;
	vector<double>	_maxForwardLowerModeBDCAreaXLengths;
	vector<double>	_minForwardLowerModeBDCAreaXLengths;
	vector<double>	_meanForwardLowerModeBDCAreaXLengths;
	vector<double>	_sumForwardLowerModeBDCAreaXLengths;
	vector<double>	_varForwardLowerModeBDCAreaXLengths;
	vector<double>	_stdDevForwardLowerModeBDCAreaXLengths;
	vector<double>	_medianForwardLowerModeBDCAreaXLengths;
	vector<double>	_numForwardLowerModeBDCAreaXLengthSamples;

	vector<Vector>	_forwardUpperModeBDCAreaXLengths;
	vector<double>	_maxForwardUpperModeBDCAreaXLengths;
	vector<double>	_minForwardUpperModeBDCAreaXLengths;
	vector<double>	_meanForwardUpperModeBDCAreaXLengths;
	vector<double>	_sumForwardUpperModeBDCAreaXLengths;
	vector<double>	_varForwardUpperModeBDCAreaXLengths;
	vector<double>	_stdDevForwardUpperModeBDCAreaXLengths;
	vector<double>	_medianForwardUpperModeBDCAreaXLengths;
	vector<double>	_numForwardUpperModeBDCAreaXLengthSamples;

	///////////////////////////////////////

	//
	///////////////////////////////////////////////////////

	///////////////////////////////////////////////////////
	///
	//	Reverse stats
	//
	void	getReverseModeWidths();
	void	getMaxReverseModeWidths();
	void	getMinReverseModeWidths();
	void	getMeanReverseModeWidths();
	void	getSumReverseModeWidths();
	void	getVarReverseModeWidths();
	void	getStdDevReverseModeWidths();
	void	getMedianReverseModeWidths();
	void	getNumReverseModeWidthSamples();

	///////////////////////////////////////////////////////

	void	getReverseModeSlopes();
	void	getMaxReverseModeSlopes();
	void	getMinReverseModeSlopes();
	void	getMeanReverseModeSlopes();
	void	getSumReverseModeSlopes();
	void	getVarReverseModeSlopes();
	void	getStdDevReverseModeSlopes();
	void	getMedianReverseModeSlopes();
	void	getNumReverseModeSlopeSamples();


	///////////////////////////////////////////////////////

	void	getReverseModalDistortionAngles();
	void	getReverseModalDistortionAngleXPositions();

	///////////////////////////////////////////////////////

	void	getReverseModeBDCAreas();
	void	getMaxReverseModeBDCAreas();
	void	getMinReverseModeBDCAreas();
	void	getMeanReverseModeBDCAreas();
	void	getSumReverseModeBDCAreas();
	void	getVarReverseModeBDCAreas();
	void	getStdDevReverseModeBDCAreas();
	void	getMedianReverseModeBDCAreas();
	void	getNumReverseModeBDCAreaSamples();

	///////////////////////////////////////////////////////

	void	getReverseModeBDCAreaXLengths();
	void	getMaxReverseModeBDCAreaXLengths();
	void	getMinReverseModeBDCAreaXLengths();
	void	getMeanReverseModeBDCAreaXLengths();
	void	getSumReverseModeBDCAreaXLengths();
	void	getVarReverseModeBDCAreaXLengths();
	void	getStdDevReverseModeBDCAreaXLengths();
	void	getMedianReverseModeBDCAreaXLengths();
	void	getNumReverseModeBDCAreaXLengthSamples();

	//
	///////////////////////////////////////////////////////////////
	//

						// vector of mode width vectors
	vector<Vector>	_reverseModeWidths;
	vector<double>	_maxReverseModeWidths;
	vector<double>	_minReverseModeWidths;
	vector<double>	_meanReverseModeWidths;
	vector<double>	_sumReverseModeWidths;
	vector<double>	_varReverseModeWidths;
	vector<double>	_stdDevReverseModeWidths;
	vector<double>	_medianReverseModeWidths;
	vector<double>	_numReverseModeWidthSamples;

	///////////////////////////////////////

	vector<Vector>	_reverseLowerModeSlopes;
	vector<double>	_maxReverseLowerModeSlopes;
	vector<double>	_minReverseLowerModeSlopes;
	vector<double>	_meanReverseLowerModeSlopes;
	vector<double>	_sumReverseLowerModeSlopes;
	vector<double>	_varReverseLowerModeSlopes;
	vector<double>	_stdDevReverseLowerModeSlopes;
	vector<double>	_medianReverseLowerModeSlopes;
	vector<double>	_numReverseLowerModeSlopeSamples;

	vector<Vector>	_reverseUpperModeSlopes;
	vector<double>	_maxReverseUpperModeSlopes;
	vector<double>	_minReverseUpperModeSlopes;
	vector<double>	_meanReverseUpperModeSlopes;
	vector<double>	_sumReverseUpperModeSlopes;
	vector<double>	_varReverseUpperModeSlopes;
	vector<double>	_stdDevReverseUpperModeSlopes;
	vector<double>	_medianReverseUpperModeSlopes;
	vector<double>	_numReverseUpperModeSlopeSamples;

	///////////////////////////////////////

	vector<double>	_reverseLowerModalDistortionAngles;
	vector<double>	_reverseLowerModalDistortionAngleXPositions;

	vector<double>	_reverseUpperModalDistortionAngles;
	vector<double>	_reverseUpperModalDistortionAngleXPositions;

	///////////////////////////////////////

	vector<Vector>	_reverseLowerModeBDCAreas;
	vector<double>	_maxReverseLowerModeBDCAreas;
	vector<double>	_minReverseLowerModeBDCAreas;
	vector<double>	_meanReverseLowerModeBDCAreas;
	vector<double>	_sumReverseLowerModeBDCAreas;
	vector<double>	_varReverseLowerModeBDCAreas;
	vector<double>	_stdDevReverseLowerModeBDCAreas;
	vector<double>	_medianReverseLowerModeBDCAreas;
	vector<double>	_numReverseLowerModeBDCAreaSamples;

	vector<Vector>	_reverseUpperModeBDCAreas;
	vector<double>	_maxReverseUpperModeBDCAreas;
	vector<double>	_minReverseUpperModeBDCAreas;
	vector<double>	_meanReverseUpperModeBDCAreas;
	vector<double>	_sumReverseUpperModeBDCAreas;
	vector<double>	_varReverseUpperModeBDCAreas;
	vector<double>	_stdDevReverseUpperModeBDCAreas;
	vector<double>	_medianReverseUpperModeBDCAreas;
	vector<double>	_numReverseUpperModeBDCAreaSamples;

	///////////////////////////////////////

	vector<Vector>	_reverseLowerModeBDCAreaXLengths;
	vector<double>	_maxReverseLowerModeBDCAreaXLengths;
	vector<double>	_minReverseLowerModeBDCAreaXLengths;
	vector<double>	_meanReverseLowerModeBDCAreaXLengths;
	vector<double>	_sumReverseLowerModeBDCAreaXLengths;
	vector<double>	_varReverseLowerModeBDCAreaXLengths;
	vector<double>	_stdDevReverseLowerModeBDCAreaXLengths;
	vector<double>	_medianReverseLowerModeBDCAreaXLengths;
	vector<double>	_numReverseLowerModeBDCAreaXLengthSamples;

	vector<Vector>	_reverseUpperModeBDCAreaXLengths;
	vector<double>	_maxReverseUpperModeBDCAreaXLengths;
	vector<double>	_minReverseUpperModeBDCAreaXLengths;
	vector<double>	_meanReverseUpperModeBDCAreaXLengths;
	vector<double>	_sumReverseUpperModeBDCAreaXLengths;
	vector<double>	_varReverseUpperModeBDCAreaXLengths;
	vector<double>	_stdDevReverseUpperModeBDCAreaXLengths;
	vector<double>	_medianReverseUpperModeBDCAreaXLengths;
	vector<double>	_numReverseUpperModeBDCAreaXLengthSamples;

	///////////////////////////////////////

	double	_overallPercStableArea;

	//
	///
	///////////////////////////////////////////////////////

	///////////////////////////////////////////////////////
	///
	//
	void	getStableAreaWidths();
	void	getMaxStableAreaWidths();
	void	getMinStableAreaWidths();
	void	getMeanStableAreaWidths();
	void	getSumStableAreaWidths();
	void	getVarStableAreaWidths();
	void	getStdDevStableAreaWidths();
	void	getMedianStableAreaWidths();
	void	getNumStableAreaWidthSamples();

	vector<Vector>	_stableAreaWidths;
	vector<double>	_maxStableAreaWidths;
	vector<double>	_minStableAreaWidths;
	vector<double>	_meanStableAreaWidths;
	vector<double>	_sumStableAreaWidths;
	vector<double>	_varStableAreaWidths;
	vector<double>	_stdDevStableAreaWidths;
	vector<double>	_medianStableAreaWidths;
	vector<double>	_numStableAreaWidthSamples;

	//
	///////////////////////////////////////////////////////

	///////////////////////////////////////////////////////
	///
	//
	void	getPercWorkingRegions();
	void	getMaxPercWorkingRegions();
	void	getMinPercWorkingRegions();
	void	getMeanPercWorkingRegions();
	void	getSumPercWorkingRegions();
	void	getVarPercWorkingRegions();
	void	getStdDevPercWorkingRegions();
	void	getMedianPercWorkingRegions();
	void	getNumPercWorkingRegionSamples();

	vector<Vector>	_percWorkingRegions;
	vector<double>	_maxPercWorkingRegions;
	vector<double>	_minPercWorkingRegions;
	vector<double>	_meanPercWorkingRegions;
	vector<double>	_sumPercWorkingRegions;
	vector<double>	_varPercWorkingRegions;
	vector<double>	_stdDevPercWorkingRegions;
	vector<double>	_medianPercWorkingRegions;
	vector<double>	_numPercWorkingRegionSamples;

	//
	///////////////////////////////////////////////////////

	///////////////////////////////////////////////////////
	///
	//	Middle Lines

	void	getMiddleLineRMSValues();
	void	getMiddleLineSlopes();

	///////////////////////////////////////////////////////

	void	getMaxContinuityValue();
	void	getMinContinuityValue();
	void	getMeanContinuityValues();
	void	getSumContinuityValues();
	void	getVarContinuityValues();
	void	getStdDevContinuityValues();
	void	getMedianContinuityValues();
	void	getNumContinuityValues();
	void	getMaxContinuityValueSpacing();


	///////////////////////////////////////

	vector<double>	_middleLineRMSValues;
	vector<double>	_middleLineSlopes;

	///////////////////////////////////////

	Vector			_continuityValues;
	double			_maxContinuityValue;
	double			_minContinuityValue;
	double			_meanContinuityValues;
	double			_sumContinuityValues;
	double			_varContinuityValues;
	double			_stdDevContinuityValues;
	double			_medianContinuityValues;
	double			_numContinuityValues;

	double			_maxPrGap;

	//
	///////////////////////////////////////

	//
	///////////////////////////////////////////////////////

	///////////////////////////////////////////////////////
	///
	//	private attributes
	//

	short	_superModeNumber;

	int	_numForwardModes;
	int	_numReverseModes;
	vector<ModeAnalysis>		_forwardModes;
	vector<ModeAnalysis>		_reverseModes;

	int _numHysteresisModes;
	vector<HysteresisAnalysis>	_hysteresisAnalysis;

	int	_numMiddleLines;
	vector<ModeBoundaryLine>	_middleLines;

	CString	_resultsDir;
	CString	_laserId;
	CString	_dateTimeStamp;

	int	_xAxisLength;
	int	_yAxisLength;

	short		_numUpperLinesRemoved;
	short		_numLowerLinesRemoved;
	short		_numMiddleLinesRemoved;
	
	QAThresholds	_qaThresholds;
	///////////////////////////////////

	CString	_qaResultsAbsFilePath;
	CString	_passFailResultsAbsFilePath;

	///////////////////////////////////

	bool	_qaAnalysisComplete;
	bool	_passFailAnalysisComplete;

};
//
///////////////////////////////////////////////////////////////////////////////
#endif

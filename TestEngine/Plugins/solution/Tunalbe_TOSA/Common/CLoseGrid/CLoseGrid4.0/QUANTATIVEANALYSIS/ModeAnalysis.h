// Copyright (c) 2004 PXIT Ltd. All Rights Reserved

///////////////////////////////////////////////////////////////////////////////
///
//	Class	:
//
//	Contents:
//
//	Author	:
//
///
///////////////////////////////////////////////////////////////////////////////
///
//
#ifndef __MODEANALYSIS_H_
#define __MODEANALYSIS_H_

#pragma once

//
///////////////////////////////////////////////////////////////////////////////
//
#include <afx.h>
#include "Vector.h"

#include "ModeBoundaryLine.h"
#include "QAThresholds.h"

using namespace std;

/////////////////////////////////////////////////////////////////////

class ModeAnalysis
{
public:
	ModeAnalysis();
	~ModeAnalysis(void);

	////////////////////////////////////////
	///
	//	Error codes
	//
	typedef enum rcode
	{
		ok = 0
	};
	//
	////////////////////////////////////////

	void	loadBoundaryLines(int		modeNumber,
							int			xAxisLength,
							int			yAxisLength,
							const ModeBoundaryLine& upperLine,
							const ModeBoundaryLine& lowerLine);

	void setQAThresholds(QAThresholds* qaThresholds);

	rcode	runAnalysis();

	//////////////////////////////////////////////////////

	void	getModeWidths(Vector& modeWidths);
	void	getModeWidths(Vector& modeWidths, short startIndex);
	void	getModeWidths(Vector&	modeWidths, short startIndex, short stopIndex);
	double	getModeWidthsStatistic(Vector::VectorAttribute attrib);

	//////////////////////////////////////////////////////

	void	getModeSlopes(Vector& lowerModeSlopes, Vector& upperModeSlopes);

	void	getModeUpperSlopes(Vector& modeSlopes);
	double	getModeUpperSlopesStatistic(Vector::VectorAttribute attrib);

	void	getModeLowerSlopes(Vector& modeSlopes);
	double	getModeLowerSlopesStatistic(Vector::VectorAttribute attrib);

	///////////////////////////////////////////////////////

	double	getLowerModeModalDistortionAngle();
	double	getLowerModeMaxModalDistortionAngleXPosition();
	//
	double	getUpperModeModalDistortionAngle();
	double	getUpperModeMaxModalDistortionAngleXPosition();

	///////////////////////////////////////////////////////

	void	getModeBDCAreas(Vector& lowerModeBDCAreas, Vector& upperModeBDCAreas);
	double	getLowerModeBDCAreasStatistic(Vector::VectorAttribute attrib);
	double	getUpperModeBDCAreasStatistic(Vector::VectorAttribute attrib);

	///////////////////////////////////////////////////////

	void	getModeBDCAreaXLengths(Vector& lowerModeBDCAreaXLengths, Vector& upperModeBDCAreaXLengths);

	double	getLowerModeBDCAreaXLengthsStatistic(Vector::VectorAttribute attrib);
	double	getUpperModeBDCAreaXLengthsStatistic(Vector::VectorAttribute attrib);

	///////////////////////////////////////////////////////


	ModeBoundaryLine	_upperBoundaryLine;
	ModeBoundaryLine	_lowerBoundaryLine;

	void	init();

private:


	rcode	runModeWidthsAnalysis();
	rcode	runModeSlopesAnalysis();
	rcode	runModeUpperSlopesAnalysis();
	rcode	runModeLowerSlopesAnalysis();
	rcode	runModeModalDistortionAnalysis();
	rcode	runModeBDCAnalysis();

	///////////////////////////////////////////////////////
	///
	//	private attributes
	//
	int	_modeNumber;

	int	_xAxisLength;
	int	_yAxisLength;

			// length of SMALLEST boundary line
	int		_modeLength;

	bool	_modeBoundaryLinesLoaded;


	///////////////////////////////////////////////////////
	///
	//	Thresholds
	//
	QAThresholds*		_qaThresholds;

	bool	_modalDistortionAnalysisRun;
	//
	////////////////////////////////////

	//
	///////////////////////////////////////////////////////

	////////////////////////////////////////////////////////
	///
	//	Functions for gathering statistics for this Mode
	//

	////////////////////////////////////////////

	void	calcMaxModeWidth();
	void	calcMinModeWidth();
	void	calcMeanModeWidths();
	void	calcSumModeWidths();
	void	calcNumModeWidthSamples();
	void	calcStdDevModeWidths();
	void	calcVarModeWidths();

	////////////////////////////////////////////

	void	calcMaxModeSlope();
	void	calcMinModeSlope();
	void	calcMeanModeSlopes();
	void	calcSumModeSlopes();
	void	calcNumModeSlopeSamples();
	void	calcStdDevModeSlopes();
	void	calcVarModeSlopes();

	////////////////////////////////////////////

	void	calcMaxModeUpperSlope();
	void	calcMinModeUpperSlope();
	void	calcMeanModeUpperSlopes();
	void	calcSumModeUpperSlopes();
	void	calcNumModeUpperSlopeSamples();
	void	calcStdDevModeUpperSlopes();
	void	calcVarModeUpperSlopes();

	////////////////////////////////////////////

	void	calcMaxModeLowerSlope();
	void	calcMinModeLowerSlope();
	void	calcMeanModeLowerSlopes();
	void	calcSumModeLowerSlopes();
	void	calcNumModeLowerSlopeSamples();
	void	calcStdDevModeLowerSlopes();
	void	calcVarModeLowerSlopes();

	////////////////////////////////////////////

	void	calcMaxModeBDCArea();
	void	calcMinModeBDCArea();
	void	calcMeanModeBDCAreas();
	void	calcSumModeBDCAreas();
	void	calcNumModeBDCAreas();
	void	calcStdDevModeBDCAreas();
	void	calcVarModeBDCAreas();

	////////////////////////////////////////////

	void	calcMaxModeBDCAreaXLength();
	void	calcMinModeBDCAreaXLength();
	void	calcMeanModeBDCAreaXLengths();
	void	calcSumModeBDCAreaXLengths();
	void	calcNumModeBDCAreaXLengths();
	void	calcStdDevModeBDCAreaXLengths();
	void	calcVarModeBDCAreaXLengths();

	//
	///
	////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////
	///
	//	Data

	//
	////////////////////////////////////////////
	//
	Vector		_modeWidths;
	double		_maxModeWidth;
	double		_minModeWidth;
	double		_meanModeWidths;
	double		_sumModeWidths;
	double		_numModeWidthSamples;
	double		_stdDevModeWidths;
	double		_varModeWidths;

	//
	////////////////////////////////////////////
	//
	Vector		_modeUpperSlopes;
	double		_maxModeUpperSlope;
	double		_minModeUpperSlope;
	double		_meanModeUpperSlopes;
	double		_sumModeUpperSlopes;
	double		_numModeUpperSlopeSamples;
	double		_stdDevModeUpperSlopes;
	double		_varModeUpperSlopes;

	//
	////////////////////////////////////////////
	//
	Vector		_modeLowerSlopes;
	double		_maxModeLowerSlope;
	double		_minModeLowerSlope;
	double		_meanModeLowerSlopes;
	double		_sumModeLowerSlopes;
	double		_numModeLowerSlopeSamples;
	double		_stdDevModeLowerSlopes;
	double		_varModeLowerSlopes;

	//
	////////////////////////////////////////////
	//
	double		_lowerModalDistortionAngle;
	double		_maxLowerModalDistortionAngleXPosition;
	double		_upperModalDistortionAngle;
	double		_maxUpperModalDistortionAngleXPosition;

	//
	////////////////////////////////////////////
	//
	Vector		_lowerModeBDCAreas;
	double		_lowerMaxModeBDCArea;
	double		_lowerMinModeBDCArea;
	double		_lowerMeanModeBDCAreas;
	double		_lowerSumModeBDCAreas;
	double		_lowerNumModeBDCAreas;
	double		_lowerStdDevModeBDCAreas;
	double		_lowerVarModeBDCAreas;
	//
	Vector		_upperModeBDCAreas;
	double		_upperMaxModeBDCArea;
	double		_upperMinModeBDCArea;
	double		_upperMeanModeBDCAreas;
	double		_upperSumModeBDCAreas;
	double		_upperNumModeBDCAreas;
	double		_upperStdDevModeBDCAreas;
	double		_upperVarModeBDCAreas;

	//
	////////////////////////////////////////////
	//
	Vector		_lowerModeBDCAreaXLengths;
	double		_lowerMaxModeBDCAreaXLength;
	double		_lowerMinModeBDCAreaXLength;
	double		_lowerMeanModeBDCAreaXLengths;
	double		_lowerSumModeBDCAreaXLengths;
	double		_lowerNumModeBDCAreaXLengths;
	double		_lowerStdDevModeBDCAreaXLengths;
	double		_lowerVarModeBDCAreaXLengths;
	//
	Vector		_upperModeBDCAreaXLengths;
	double		_upperMaxModeBDCAreaXLength;
	double		_upperMinModeBDCAreaXLength;
	double		_upperMeanModeBDCAreaXLengths;
	double		_upperSumModeBDCAreaXLengths;
	double		_upperNumModeBDCAreaXLengths;
	double		_upperStdDevModeBDCAreaXLengths;
	double		_upperVarModeBDCAreaXLengths;
	//
	//////////////////////////////////////////////////////////

};
//
///////////////////////////////////////////////////////////////////////////////
#endif

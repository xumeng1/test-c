#include "stdafx.h"


#include "PassFailCollated.h"
#include "defaults.h"
#include "CLoseGridFile.h"

#include <algorithm>

#define new DEBUG_NEW

/////////////////////////////////////////////////////////////////////
// Only instance of this class
PassFailCollated* 
PassFailCollated::theInstance = NULL;

///////////////////////////////////////////////////////////

PassFailCollated::
PassFailCollated()
{
	// PassFailCollated is a singleton class
	theInstance = this;

	clear();
}

PassFailCollated::
~PassFailCollated()
{
}

/////////////////////////////////////////////////////////////////////
///
//	Function	:
//
//	Returns		:		
//	Parameters	:	
//	Description	:
//	Updates		:	
//
PassFailCollated* 
PassFailCollated::instance()
{
	// If we're calling this for
	// the first time, create our
	// instance, otherwise disallow
	// any more being created
	if(theInstance == NULL)
	{
		static PassFailCollated theInstance;
	}

	return theInstance;
}

//
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
///
//
void
PassFailCollated::
clear()
{
	_resultsDir		= "";
	_laserId		= "";
	_dateTimeStamp	= "";

	_collatedPassFailResultsAbsFilePath = "";

	_p_overallModeMapAnalysis = NULL;
	_v_p_superModes.clear();

	_numSuperModesAnalysed = 0;

	_totalPassed = true;
	_overallModeMapPassed = true;
	_superModeMapPassed = true;
	_superModeMapsPassed.clear();

}
//
////////////////////////////////////////////////////////////////////
//
CString
PassFailCollated::
getResultsAbsFilePath(CString	resultsDir,
					CString		laserId,
					CString		dateTimeStamp)
{
	CString absFilePath = "";

	absFilePath += resultsDir;
	absFilePath += "\\";
	absFilePath += COLLATED_PASSFAIL_FILE_NAME;
	absFilePath += USCORE;
	absFilePath += LASER_TYPE_ID_DSDBR01_CSTRING;
	absFilePath += USCORE;
	absFilePath += laserId;
	absFilePath += USCORE;
	absFilePath += dateTimeStamp;
	absFilePath += CSV;

	return absFilePath;
}
//
////////////////////////////////////////////////////////////////////
//
void 
PassFailCollated::
setFileParameters(CString	resultsDir,
				CString		laserId,
				CString		dateTimeStamp)
{
	_resultsDir		= resultsDir;
	_laserId		= laserId;
	_dateTimeStamp	= dateTimeStamp;
}
//
////////////////////////////////////////////////////////////////////
//
void 
PassFailCollated::
setOverallModeMapAnalysis(OverallModeMapAnalysis* overallModeMapAnalysis)
{
	_p_overallModeMapAnalysis = overallModeMapAnalysis;
}

//
////////////////////////////////////////////////////////////////////
//
void
PassFailCollated::
clearSuperModes()
{
	_v_p_superModes.clear();
}
//
////////////////////////////////////////////////////////////////////
//

void
PassFailCollated::
addAnalysedSuperMode(CDSDBRSuperMode* p_superMode)
{
	_v_p_superModes.push_back(p_superMode);
	_numSuperModesAnalysed++;
}



void 
PassFailCollated::
writeResultsToFile()
{
    if(_p_overallModeMapAnalysis != NULL)
    {
		_collatedPassFailResultsAbsFilePath = getResultsAbsFilePath(_resultsDir, _laserId, _dateTimeStamp);

		CLoseGridFile collatedPassFailResults(_collatedPassFailResultsAbsFilePath);
		collatedPassFailResults.createFolderAndFile();


		// calculate _totalPassed value
		if( _p_overallModeMapAnalysis->_screeningPassed )
		{
			if( _p_overallModeMapAnalysis->_numSuperModes > 0 )
			{
				_totalPassed = true;

				for( int i = 0 ; i < (int)_v_p_superModes.size() ; i++ )
				{
					CDSDBRSuperMode* p_sm = _v_p_superModes[i];

					if( !(p_sm->_superModeMapAnalysis._screeningPassed) )
					{
						_totalPassed = false;
						break;
					}
				}
			}
			else
			{
				_totalPassed = false;
			}
		}
		else
		{
			_totalPassed = false;
		}


		//	Column headers

		const CString	QA_XLS_HEADER1 = "Total Pass/Fail";
		const CString	QA_XLS_HEADER2 = "Overall Mode Map Pass/Fail";
		const CString	QA_XLS_HEADER3 = "SM Index";
		const CString	QA_XLS_HEADER4 = "Supermode Map Pass/Fail/NotLoaded";

		CStringArray headerArray;
	    headerArray.Add(QA_XLS_HEADER1);
	    headerArray.Add(QA_XLS_HEADER2);
	    headerArray.Add(QA_XLS_HEADER3);
	    headerArray.Add(QA_XLS_HEADER4);

	    // write headers
	    collatedPassFailResults.addRow(headerArray);

        // write empty line
	    collatedPassFailResults.addEmptyLine();




	    CString	QA_XLS_COL1_VALUE = "";
	    CString	QA_XLS_COL2_VALUE = "";
	    CString	QA_XLS_COL3_VALUE = "";
	    CString	QA_XLS_COL4_VALUE = "";


		if( _totalPassed ) 
			QA_XLS_COL1_VALUE = "PASS";
		else
			QA_XLS_COL1_VALUE = "FAIL";

		if( _p_overallModeMapAnalysis->_screeningPassed ) 
			QA_XLS_COL2_VALUE = "PASS";
		else
			QA_XLS_COL2_VALUE = "FAIL";


		CStringArray rowArray;

		if( _p_overallModeMapAnalysis->_numSuperModes <= 0 )
		{
			// write just these for case when no SMs were found
			rowArray.Add(QA_XLS_COL1_VALUE);
			rowArray.Add(QA_XLS_COL2_VALUE);
			collatedPassFailResults.addRow(rowArray);
			rowArray.RemoveAll();
		}

		for( int sm = 0 ; sm < _p_overallModeMapAnalysis->_numSuperModes ; sm++ )
		{
			QA_XLS_COL3_VALUE = "";
			QA_XLS_COL3_VALUE.AppendFormat("%d",sm);
			QA_XLS_COL4_VALUE = "Not Loaded";

			for( int i = 0 ; i < (int)_v_p_superModes.size() ; i++ )
			{
				CDSDBRSuperMode* p_sm = _v_p_superModes[i];
				if( (int)(p_sm->_sm_number) == sm )
				{ // supermap has been loaded

					if( p_sm->_superModeMapAnalysis._screeningPassed )
						QA_XLS_COL4_VALUE = "PASS";
					else
						QA_XLS_COL4_VALUE = "FAIL";

					break;
				}
			}

			rowArray.Add(QA_XLS_COL1_VALUE);
			rowArray.Add(QA_XLS_COL2_VALUE);
			rowArray.Add(QA_XLS_COL3_VALUE);
			rowArray.Add(QA_XLS_COL4_VALUE);
			collatedPassFailResults.addRow(rowArray);
			rowArray.RemoveAll();

			QA_XLS_COL1_VALUE = "";
			QA_XLS_COL2_VALUE = "";

		}




		// write 3 empty lines
		collatedPassFailResults.addEmptyLine();
		collatedPassFailResults.addEmptyLine();
		collatedPassFailResults.addEmptyLine();


		QA_XLS_COL1_VALUE = "Overall Map Pass/Fail Analysis";
		rowArray.Add(QA_XLS_COL1_VALUE);
		collatedPassFailResults.addRow(rowArray);
		rowArray.RemoveAll();


		// write overall map pass/fail
		_p_overallModeMapAnalysis->writePassFailResultsToFile( collatedPassFailResults );



		for( int i = 0 ; i < (int)_v_p_superModes.size() ; i++ )
		{
			CDSDBRSuperMode* p_sm = _v_p_superModes[i];

			// write 3 empty lines
			collatedPassFailResults.addEmptyLine();
			collatedPassFailResults.addEmptyLine();
			collatedPassFailResults.addEmptyLine();


			QA_XLS_COL1_VALUE = "";
			QA_XLS_COL1_VALUE.AppendFormat("Supermode %d Map Pass/Fail Analysis",i);
			rowArray.Add(QA_XLS_COL1_VALUE);
			collatedPassFailResults.addRow(rowArray);
			rowArray.RemoveAll();


			// write supermode map pass/fail
			p_sm->_superModeMapAnalysis.writePassFailResultsToFile( collatedPassFailResults );

		}

	}	
}


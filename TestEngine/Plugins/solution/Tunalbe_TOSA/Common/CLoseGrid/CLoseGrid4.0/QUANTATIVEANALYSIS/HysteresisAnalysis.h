// Copyright (c) 2004 PXIT Ltd. All Rights Reserved

///////////////////////////////////////////////////////////////////////////////
///
//	Class	:
//
//	Contents:
//
//	Author	:
//
///
///////////////////////////////////////////////////////////////////////////////
///
//
#ifndef __HYSTERESISANALYSIS_H_
#define __HYSTERESISANALYSIS_H_

#pragma once

//
///////////////////////////////////////////////////////////////////////////////
//
#include <afx.h>
#include "Vector.h"

#include "ModeAnalysis.h"
#include "QAThresholds.h"

using namespace std;

/////////////////////////////////////////////////////////////////////

class HysteresisAnalysis
{
public:
	HysteresisAnalysis();
	HysteresisAnalysis(ModeAnalysis&	forwardMode,
					ModeAnalysis&		reverseMode,
					int					xAxisLength,
					int					yAxisLength);
	~HysteresisAnalysis(void);

	////////////////////////////////////////
	///
	//	Error codes
	//
	typedef enum rcode
	{
		ok = 0
	};
	//
	////////////////////////////////////////

	void clear();
	
	//GDM 31/10/67 added use of qaThreshold values for hysteresis window 
	void setQAThresholds(QAThresholds* qaThresholds);

	rcode	runAnalysis();

	//////////////////////////////////////////////////////

	void	getStableAreaWidths(Vector& stableAreaWidths);
	double	getStableAreaWidthsStatistic(Vector::VectorAttribute attrib);

	///////////////////////////////////////////////////////

	void	getTotalWidths(Vector& totalWidths);

	///////////////////////////////////////////////////////

	void	getPercWorkingRegions(Vector& percWorkingRegions);
	double	getPercWorkingRegionsStatistic(Vector::VectorAttribute attrib);

	///////////////////////////////////////////////////////

private:

	void init();

	rcode	runStableAreaWidthsAnalysis();
	rcode	runPercWorkingRegionAnalysis();

	///////////////////////////////////////////////////////
	///
	//	private attributes
	//
	ModeAnalysis	_forwardMode;
	ModeAnalysis	_reverseMode;

							// formed by the forward modes' upper line
							// and the reverse modes' lower line
	ModeAnalysis	_innerHysteresisMode;
							// formed by the reverse modes' upper line
							// and the forward modes' lower line
	ModeAnalysis	_outerHysteresisMode;


	int	_xAxisLength;
	int	_yAxisLength;

	//GDM 31/10/06 add dec to retrieve Qa thresholds
	///////////////////////////////////////////////////////
	///
	//	Thresholds
	//
	QAThresholds*		_qaThresholds;
	//
	///////////////////////////////////////////////////////

	////////////////////////////////////////////////////////
	///
	//	Functions for gathering statistics
	//

	////////////////////////////////////////////

	void	calcMaxStableAreaWidths();
	void	calcMinStableAreaWidths();
	void	calcMeanStableAreaWidths();
	void	calcSumStableAreaWidths();
	void	calcNumStableAreaWidthSamples();
	void	calcStdDevStableAreaWidths();
	void	calcVarStableAreaWidths();

	////////////////////////////////////////////

	void	calcMaxPercWorkingRegions();
	void	calcMinPercWorkingRegions();
	void	calcMeanPercWorkingRegions();
	void	calcSumPercWorkingRegions();
	void	calcNumPercWorkingRegionSamples();
	void	calcStdDevPercWorkingRegions();
	void	calcVarPercWorkingRegions();

	////////////////////////////////////////////

	//
	///
	////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////
	///
	//	Data

	//
	////////////////////////////////////////////
	//
	Vector		_stableAreaWidths;
	double		_maxStableAreaWidth;
	double		_minStableAreaWidth;
	double		_meanStableAreaWidths;
	double		_sumStableAreaWidths;
	double		_numStableAreaWidthSamples;
	double		_stdDevStableAreaWidths;
	double		_varStableAreaWidths;

	////////////////////////////////////////////

	Vector		_totalWidths;

	////////////////////////////////////////////

	Vector		_percWorkingRegions;
	double		_maxPercWorkingRegion;
	double		_minPercWorkingRegion;
	double		_meanPercWorkingRegions;
	double		_sumPercWorkingRegions;
	double		_numPercWorkingRegionSamples;
	double		_stdDevPercWorkingRegions;
	double		_varPercWorkingRegions;

	////////////////////////////////////////////

	//
	//////////////////////////////////////////////////////////

};
//
///////////////////////////////////////////////////////////////////////////////
#endif

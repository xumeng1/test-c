// Copyright (c) 2002 Tsunami Photonics Ltd. All Rights Reserved
//
// FileName    : Vector.cpp
// Description : Implementation of of Vector class functions
//
// Rev#  | Date          | Author               | Description of change
// --------------------------------------------------------------
// 0.1   | 02 Oct 2002   | Frank D'Arcy         | Initial Draft
//
#include "stdafx.h"

#include "QAThresholds.h"

#define new DEBUG_NEW

#define DEFAULT_SLOPE_WINDOW			5
#define DEFAULT_MODAL_DISTORTION_MIN_X	20
#define DEFAULT_MODAL_DISTORTION_MAX_X	80
#define DEFAULT_MODAL_DISTORTION_MIN_Y	20
#define DEFAULT_MODE_WIDTH_ANALYSIS_MIN_X 20
#define DEFAULT_MODE_WIDTH_ANALYSIS_MAX_X 80
#define DEFAULT_HYSTERESIS_ANALYSIS_MIN_X 20
#define DEFAULT_HYSTERESIS_ANALYSIS_MAX_X 80

QAThresholds::
QAThresholds()
{
	setThresholdsToDefault();
}

QAThresholds::~QAThresholds( )
{
}

void
QAThresholds::
setThresholdsToDefault()
{
	_slopeWindow			= DEFAULT_SLOPE_WINDOW;
	_modalDistortionMinX	= DEFAULT_MODAL_DISTORTION_MIN_X;
	_modalDistortionMaxX	= DEFAULT_MODAL_DISTORTION_MAX_X;
	_modalDistortionMinY	= DEFAULT_MODAL_DISTORTION_MIN_Y;
	_modeWidthAnalysisMinX	= DEFAULT_MODE_WIDTH_ANALYSIS_MIN_X;
	_modeWidthAnalysisMaxX	= DEFAULT_MODE_WIDTH_ANALYSIS_MAX_X;
	_hysteresisAnalysisMinX	= DEFAULT_HYSTERESIS_ANALYSIS_MIN_X;
	_hysteresisAnalysisMaxX	= DEFAULT_HYSTERESIS_ANALYSIS_MAX_X;
}

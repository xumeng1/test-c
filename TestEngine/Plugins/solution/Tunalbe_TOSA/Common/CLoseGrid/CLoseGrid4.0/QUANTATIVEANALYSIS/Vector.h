// Copyright (c) 2002 Tsunami Photonics Ltd. All Rights Reserved
//
// FileName    : Vector.h
// Description : Declaration of Vector class
//
// Rev#  | Date          | Author               | Description of change
// --------------------------------------------------------------
// 0.1   | 02 Oct 2002   | Frank D'Arcy         | Initial Draft
//

#ifndef __VECTOR__H__
#define __VECTOR__H__

#include <vector>

class Vector 
{
public:
	Vector();
    Vector(std::vector<double> &values);
    ~Vector( );

    std::vector<double>* getValues();

	////////////////////////////////////

	typedef enum VectorAttribute
	{
		max = 0,
		min,
		mean,
		sum,
		count,
		variance,
		stdDev,
		median,
		maxGap // max gap between sorted elements
	};	
	double	getVectorAttribute(VectorAttribute attrib);

	////////////////////////////////////

	void			sort();
	void			push_back(double value);
	int				size();
	double			operator[](int index);
	void			operator=(std::vector<double> vectorOfDoubles);
	void			operator+(std::vector<double> vectorOfDoubles);
	void			clear();

	//////////////////////////////////////

protected:
    std::vector<double> _values;

private:

    double		maxOfValues();
    double		minOfValues();
    double		meanOfValues();
    double		sumOfValues();
    double		varianceOfValues();
    double		stdDevOfValues();
    double		medianOfValues();
	double		maxGapInValues();
};

#endif	// __DATAPROCESSING_VECTOR__H__

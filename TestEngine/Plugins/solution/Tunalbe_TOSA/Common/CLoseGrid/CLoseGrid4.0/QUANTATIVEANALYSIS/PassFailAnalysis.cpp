// Copyright (c) 2003 Tsunami Photonics Ltd. All Rights Reserved

///////////////////////////////////////////////////////////////////////////////
///
//	Class		:	PassFailAnalysis
//
//	Contents	:	Compares QA results with set thresholds to determine
//					whether the results have passed or failed.
//
//	Containment	:	
//
//	Multiplicity:	Many
//
//	Created by	:	QuantativeAnalysis
//
//	Author		:	Richard Ashe
//
//	Date		:	14/01/2003
///
///////////////////////////////////////////////////////////////////////////////
///
//

#include "stdafx.h"

#include "PassFailAnalysis.h"

#define new DEBUG_NEW

/////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
///
//	Function	:	constructor
//
//	Returns		:	none
//
//	Parameters	:	none
//
//	Description	:	sets starting values for new object
//
PassFailAnalysis::
PassFailAnalysis()
{

	init();

}

///////////////////////////////////////////////////////////////////////////////
///
//	Function	:	destructor
//
//	Returns		:	none
//
//	Parameters	:	none
//
//	Description	:	destroys the object
//
PassFailAnalysis::
~PassFailAnalysis(void)
{
}

///////////////////////////////////////////////////////////////////////////////
///
//	Function	:	init
//
//	Returns		:	void
//
//	Parameters	:	none
//
//	Description	:	sets starting values, called by constructor
//
void
PassFailAnalysis::
init()
{
	threshold		= 0;
    failedModes.clear();
    valuesWhichFailed.clear();
	pass			= true;
	numFailedModes	= 0;
	minValue		= 0;
	maxValue		= 0;
}

///////////////////////////////////////////////////////////////////////////////
///
//	Function	:	runPassFailWithUpperLimit
//
//	Returns		:	void
//
//	Parameters	:	vector of doubles
//
//	Description	:	checks each double to see if any are greater than threshold
//
void
PassFailAnalysis::
runPassFailWithUpperLimit(std::vector<double> values)
{

	std::vector<bool> ignore_none;

	for( int i = 0 ; i < (int)values.size() ; i++ )
	{
		ignore_none.push_back(false);
	}

	runPassFailWithUpperLimit(values, ignore_none);

}


///////////////////////////////////////////////////////////////////////////////
///
//	Function	:	runPassFailWithLowerLimit
//
//	Returns		:	void
//
//	Parameters	:	vector of doubles
//
//	Description	:	checks each double to see if any are lower than threshold
//
void
PassFailAnalysis::
runPassFailWithLowerLimit(std::vector<double> values)
{
	std::vector<bool> ignore_none;

	for( int i = 0 ; i < (int)values.size() ; i++ )
	{
		ignore_none.push_back(false);
	}

	runPassFailWithLowerLimit(values, ignore_none);
}
//
///
///////////////////////////////////////////////////////////////////////////////


void
PassFailAnalysis::
runPassFailWithUpperLimit( double value )
{
	std::vector<double> values;
	std::vector<bool> ignore_none;

	values.push_back(value);
	ignore_none.push_back(false);

	runPassFailWithUpperLimit(values, ignore_none);
}


void
PassFailAnalysis::
runPassFailWithLowerLimit( double value )
{
	std::vector<double> values;
	std::vector<bool> ignore_none;

	values.push_back(value);
	ignore_none.push_back(false);

	runPassFailWithLowerLimit(values, ignore_none);
}



void
PassFailAnalysis::
runPassFailWithUpperLimit(std::vector<double> values,std::vector<bool> ignore)
{
    failedModes.clear();
    valuesWhichFailed.clear();
	pass			= true;
	numFailedModes	= 0;
	minValue		= 0;
	maxValue		= 0;

    double value;

	if( values.size()>0 && values.size()==ignore.size() )
	{
		// set the initial min and max values
		// and the pass boolean
		minValue = values[0];
		maxValue = values[0];
		pass = true;

		for(unsigned int i=0; i<values.size(); i++)
		{
			value = values[i];

			// work out min and max
			if(value>maxValue)
			{
				maxValue = value;
			}
			else if(value<minValue)
			{
				minValue = value;
			}

			// if value is less than the
			// threshold/upper limit then it passes
			//
			if(value<=threshold || ignore[i])
			{
				// value passed
			}
			else
			{
				pass = false;
				failedModes.push_back(i);
				numFailedModes++;
				valuesWhichFailed.push_back(value);
			}
		}
	}
	else
	{
		// error - no values to work with
	}
}


void
PassFailAnalysis::
runPassFailWithLowerLimit(std::vector<double> values,std::vector<bool> ignore)
{
    failedModes.clear();
    valuesWhichFailed.clear();
	pass			= true;
	numFailedModes	= 0;
	minValue		= 0;
	maxValue		= 0;

    double value;

	if( values.size()>0 && values.size()==ignore.size() )
	{

		// set the initial min and max values
		// and the pass boolean
		minValue = values[0];
		maxValue = values[0];
		pass = true;

		for(unsigned int i=0; i<values.size(); i++)
		{
			value = values[i];
			// work out min and max
			if(value>maxValue)
			{
				maxValue = value;
			}
			else if(value<minValue)
			{
				minValue = value;
			}


			// if value is greater than the
			// threshold/lower limit then it passes
			//
			if(value>=threshold || ignore[i])
			{
				// value passed
			}
			else
			{
				pass = false;
				failedModes.push_back(i);
				numFailedModes++;
				valuesWhichFailed.push_back(value);
			}
		}
	}
	else
	{
		// error - no values to work with
	}
}

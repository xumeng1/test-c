// Copyright (c) 2004 PXIT Ltd. All Rights Reserved

///////////////////////////////////////////////////////////////////////////////
///
//	Class	:
//
//	Contents:
//
//	Author	:
//
///
///////////////////////////////////////////////////////////////////////////////
///
//

#include "stdafx.h"

#include <algorithm>

#include "ModeAnalysis.h"
#include "CGResearchServerInterface.h"

//
/////////////////////////////////////////////////////////////////////
//

#define new DEBUG_NEW

/////////////////////////////////////////////////////////////////////

//
///	ctor and dtor
//

ModeAnalysis::
ModeAnalysis()
{
	init();
}

ModeAnalysis::
~ModeAnalysis(void)
{
}

void
ModeAnalysis::
init()
{
	/////////////////////////////
	// initialise attributes
	// before use
	_modeBoundaryLinesLoaded = false;

	_modalDistortionAnalysisRun = false;

	_modeLength = 0;

	//////////////////////////////////////////////////

	_modeWidths.clear();
	_modeUpperSlopes.clear();
	_modeLowerSlopes.clear();
	_lowerModeBDCAreas.clear();
	_upperModeBDCAreas.clear();
	_lowerModeBDCAreaXLengths.clear();
	_upperModeBDCAreaXLengths.clear();

	_maxModeWidth			= 0;
	_minModeWidth			= 0;
	_meanModeWidths			= 0;
	_sumModeWidths			= 0;
	_numModeWidthSamples	= 0;
	_stdDevModeWidths		= 0;
	_varModeWidths			= 0;

	//////////////////////////////////////////////////


	_maxModeUpperSlope			= 0;
	_minModeUpperSlope			= 0;
	_meanModeUpperSlopes		= 0;
	_sumModeUpperSlopes			= 0;
	_numModeUpperSlopeSamples	= 0;
	_stdDevModeUpperSlopes		= 0;
	_varModeUpperSlopes			= 0;

	_maxModeLowerSlope			= 0;
	_minModeLowerSlope			= 0;
	_meanModeLowerSlopes		= 0;
	_sumModeLowerSlopes			= 0;
	_numModeLowerSlopeSamples	= 0;
	_stdDevModeLowerSlopes		= 0;
	_varModeLowerSlopes			= 0;

	//////////////////////////////////////////////////

	_lowerMaxModeBDCArea		= 0;
	_lowerMinModeBDCArea		= 0;
	_lowerMeanModeBDCAreas		= 0;
	_lowerSumModeBDCAreas		= 0;
	_lowerNumModeBDCAreas		= 0;
	_lowerStdDevModeBDCAreas	= 0;
	_lowerVarModeBDCAreas		= 0;

	_upperMaxModeBDCArea		= 0;
	_upperMinModeBDCArea		= 0;
	_upperMeanModeBDCAreas		= 0;
	_upperSumModeBDCAreas		= 0;
	_upperNumModeBDCAreas		= 0;
	_upperStdDevModeBDCAreas	= 0;
	_upperVarModeBDCAreas		= 0;

	//////////////////////////////////////////////////

	_lowerMaxModeBDCAreaXLength		= 0;
	_lowerMinModeBDCAreaXLength		= 0;
	_lowerMeanModeBDCAreaXLengths	= 0;
	_lowerSumModeBDCAreaXLengths	= 0;
	_lowerNumModeBDCAreaXLengths	= 0;
	_lowerStdDevModeBDCAreaXLengths	= 0;
	_lowerVarModeBDCAreaXLengths	= 0;

	_upperMaxModeBDCAreaXLength		= 0;
	_upperMinModeBDCAreaXLength		= 0;
	_upperMeanModeBDCAreaXLengths	= 0;
	_upperSumModeBDCAreaXLengths	= 0;
	_upperNumModeBDCAreaXLengths	= 0;
	_upperStdDevModeBDCAreaXLengths	= 0;
	_upperVarModeBDCAreaXLengths	= 0;

	//////////////////////////////////////////////////

	_lowerModalDistortionAngle = 0;
	_maxLowerModalDistortionAngleXPosition = 0;

	_upperModalDistortionAngle = 0;
	_maxUpperModalDistortionAngleXPosition = 0;

	/////////////////////////////////////////////////

}

/////////////////////////////////////////////////////////////////////

void
ModeAnalysis::
setQAThresholds(QAThresholds* qaThresholds)
{
	_qaThresholds = qaThresholds;
}

/////////////////////////////////////////////////////////////////////

void
ModeAnalysis::
loadBoundaryLines(int		modeNumber,
				int			xAxisLength,
				int			yAxisLength,
				const ModeBoundaryLine& upperLine,
				const ModeBoundaryLine& lowerLine)
{
	init();
	_modeNumber = modeNumber;
	_xAxisLength = xAxisLength;
	_yAxisLength = yAxisLength;
	_upperBoundaryLine = upperLine;
	_lowerBoundaryLine = lowerLine;

	if(_upperBoundaryLine.numXPoints() < _lowerBoundaryLine.numXPoints())
		_modeLength = _upperBoundaryLine.numXPoints();
	else
		_modeLength = _lowerBoundaryLine.numXPoints();

	if(_modeLength > 0)
		_modeBoundaryLinesLoaded = true;
	else
		_modeBoundaryLinesLoaded = false;

	CString log_msg("ModeAnalysis::loadBoundaryLines : ");
	log_msg.AppendFormat("created _upperBoundaryLine._x.size()=%d, _upperBoundaryLine._y.size()=%d",
		_upperBoundaryLine._x.size(), _upperBoundaryLine._y.size() );
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	log_msg = CString("ModeAnalysis::loadBoundaryLines : ");
	log_msg.AppendFormat("created _lowerBoundaryLine._x.size()=%d, _lowerBoundaryLine._y.size()=%d",
		_lowerBoundaryLine._x.size(), _lowerBoundaryLine._y.size() );
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

	log_msg = CString("ModeAnalysis::loadBoundaryLines : ");
	log_msg.AppendFormat("created _modeNumber=%d, _xAxisLength=%d, _yAxisLength=%d, _modeLength=%d",
		_modeNumber, _xAxisLength, _yAxisLength, _modeLength );
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

}

ModeAnalysis::rcode
ModeAnalysis::
runAnalysis()
{
	rcode err = rcode::ok;

	if(_modeBoundaryLinesLoaded)
	{
		if(err == rcode::ok)
			err = runModeWidthsAnalysis();		
		if(err == rcode::ok)
			err = runModeSlopesAnalysis();		
		if(err == rcode::ok)
			err = runModeUpperSlopesAnalysis();		
		if(err == rcode::ok)
			err = runModeLowerSlopesAnalysis();		
		if(err == rcode::ok)
			err = runModeModalDistortionAnalysis();
		if(err == rcode::ok)
			err = runModeBDCAnalysis();
	}

	return err;
}

/////////////////////////////////////////////////////////////////////
///
//
ModeAnalysis::rcode
ModeAnalysis::
runModeWidthsAnalysis()
{
	rcode err = rcode::ok;

	if(_modeBoundaryLinesLoaded)
	{
		Vector modeWidths;
		//GDM additional call if in SM analysis, 
		if (_qaThresholds->_SMMapQA == 1)
			getModeWidths(modeWidths,_qaThresholds->_modeWidthAnalysisMinX,
														_qaThresholds->_modeWidthAnalysisMaxX);
		if (_qaThresholds->_SMMapQA == 0)
		//original call, now just used for OM
			getModeWidths(modeWidths);

		calcMaxModeWidth();
		calcMinModeWidth();
		calcMeanModeWidths();
		calcSumModeWidths();
		calcNumModeWidthSamples();
		calcStdDevModeWidths();
		calcVarModeWidths();
	}

	return err;
}

///////////////////////////////////////////////////////////////////////

ModeAnalysis::rcode
ModeAnalysis::
runModeSlopesAnalysis()
{
	rcode err = rcode::ok;

	if(_modeBoundaryLinesLoaded)
	{
		Vector lowerModeSlopes;
		Vector upperModeSlopes;
		getModeSlopes(lowerModeSlopes, upperModeSlopes);

		calcMaxModeSlope();
		calcMinModeSlope();
		calcMeanModeSlopes();
		calcSumModeSlopes();
		calcNumModeSlopeSamples();
		calcStdDevModeSlopes();
		calcVarModeSlopes();
	}

	return err;
}

///////////////////////////////////////////////////////////////////////

ModeAnalysis::rcode
ModeAnalysis::
runModeUpperSlopesAnalysis()
{
	rcode err = rcode::ok;

	if(_modeBoundaryLinesLoaded)
	{
		Vector modeUpperSlopes;
		getModeUpperSlopes(modeUpperSlopes);

		calcMaxModeUpperSlope();
		calcMinModeUpperSlope();
		calcMeanModeUpperSlopes();
		calcSumModeUpperSlopes();
		calcNumModeUpperSlopeSamples();
		calcStdDevModeUpperSlopes();
		calcVarModeUpperSlopes();
	}

	return err;
}

///////////////////////////////////////////////////////////////////////

ModeAnalysis::rcode
ModeAnalysis::
runModeLowerSlopesAnalysis()
{
	rcode err = rcode::ok;

	if(_modeBoundaryLinesLoaded)
	{
		Vector modeLowerSlopes;
		getModeLowerSlopes(modeLowerSlopes);

		calcMaxModeLowerSlope();
		calcMinModeLowerSlope();
		calcMeanModeLowerSlopes();
		calcSumModeLowerSlopes();
		calcNumModeLowerSlopeSamples();
		calcStdDevModeLowerSlopes();
		calcVarModeLowerSlopes();
	}

	return err;
}

///////////////////////////////////////////////////////////////////////

ModeAnalysis::rcode
ModeAnalysis::
runModeModalDistortionAnalysis()
{
	rcode err = rcode::ok;

	if(_modeBoundaryLinesLoaded)
	{
		if(_lowerBoundaryLine.numXPoints() == _xAxisLength)
		{
			_lowerBoundaryLine.runModalDistortionAnalysis(_qaThresholds->_modalDistortionMinX,
														_qaThresholds->_modalDistortionMaxX);

			_lowerModalDistortionAngle = _lowerBoundaryLine.getMaxModalDistortionAngle();
			_maxLowerModalDistortionAngleXPosition = _lowerBoundaryLine.getMaxModalDistortionAngleXPosition();
			
		}

		if(_upperBoundaryLine.numXPoints() == _xAxisLength)
		{
			_upperBoundaryLine.runModalDistortionAnalysis(_qaThresholds->_modalDistortionMinX,
														_qaThresholds->_modalDistortionMaxX);

			_upperModalDistortionAngle = _upperBoundaryLine.getMaxModalDistortionAngle();
			_maxUpperModalDistortionAngleXPosition = _upperBoundaryLine.getMaxModalDistortionAngleXPosition();
			
		}

		_modalDistortionAnalysisRun = true;
	}

	return err;
}

///////////////////////////////////////////////////////////////////////

ModeAnalysis::rcode
ModeAnalysis::
runModeBDCAnalysis()
{
	rcode err = rcode::ok;

	if(_modeBoundaryLinesLoaded)
	{
		Vector lowerModeBDCAreas;
		Vector upperModeBDCAreas;
		getModeBDCAreas(lowerModeBDCAreas, upperModeBDCAreas);

		calcMaxModeBDCArea();
		calcMinModeBDCArea();
		calcMeanModeBDCAreas();
		calcSumModeBDCAreas();
		calcNumModeBDCAreas();
		calcStdDevModeBDCAreas();
		calcVarModeBDCAreas();


		Vector lowerModeBDCAreaXLengths;
		Vector upperModeBDCAreaXLengths;
		getModeBDCAreaXLengths(lowerModeBDCAreaXLengths, upperModeBDCAreaXLengths);

		calcMaxModeBDCAreaXLength();
		calcMinModeBDCAreaXLength();
		calcMeanModeBDCAreaXLengths();
		calcSumModeBDCAreaXLengths();
		calcNumModeBDCAreaXLengths();
		calcStdDevModeBDCAreaXLengths();
		calcVarModeBDCAreaXLengths();

	}

	return err;
}

//
///
/////////////////////////////////////////////////////////////////////
///
//
void
ModeAnalysis::
getModeWidths(Vector&	modeWidths)
{
	//GDM additional call if in SM analysis, 
	if (_qaThresholds->_SMMapQA == 1)
			getModeWidths(modeWidths,_qaThresholds->_modeWidthAnalysisMinX,
														_qaThresholds->_modeWidthAnalysisMaxX);
	if (_qaThresholds->_SMMapQA == 0)
	{	//original call, now just used for OM
		short startIndex = 0;
		getModeWidths(modeWidths, startIndex);
	}
}

void
ModeAnalysis::
getModeWidths(Vector&	modeWidths, 
			  short		startIndex)
{
	if(_modeBoundaryLinesLoaded)
	{
		// only do this once
		if(_modeWidths.size() == 0)
		{

			//////////////////////////////////////////////////////////////////////////////

			// create vector of highest points on bottom line at each x_coord
			std::vector<double> bottom_line_highest_points;

			int lowest_x = 9999999; // Huge number to begin with
			int highest_x = -1;


			CString log_msg;
			//log_msg.AppendFormat("ModeAnalysis::getModeWidths() _lowerBoundaryLine._x.size() = %d",_lowerBoundaryLine._x.size() );
			//CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

			//log_msg.Format("ModeAnalysis::getModeWidths() _lowerBoundaryLine._y.size() = %d",_lowerBoundaryLine._y.size() );
			//CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

			for(int i=startIndex; i<_lowerBoundaryLine.numXPoints(); i++)
			{
				// find x and y position of each point on the line

				int x_point = (int)_lowerBoundaryLine.x(i);
				int y_point = (int)_lowerBoundaryLine.y(i);

				if( (int)x_point < lowest_x ) 
						lowest_x = (int)x_point;

				if( (int)x_point > highest_x )
				{
					highest_x = (int)x_point;
					// add point to vector
					bottom_line_highest_points.push_back(i);

				}
				else
				{ 
					// check for previous occurances of x_point
					int num = (int)(bottom_line_highest_points.size());

					for( int f = num-1; f >= 0; f-- )
					{
						double index_find = bottom_line_highest_points[f];

						if( (int)index_find < 0
						 || (int)index_find >= (int)_lowerBoundaryLine._y.size()
						 || (int)index_find >= (int)_lowerBoundaryLine._x.size() )
						{
							CGR_LOG("ModeAnalysis::getModeWidths() : Bounds exceeded",ERROR_CGR_LOG)
							log_msg.Format("ModeAnalysis::getModeWidths() index_find = %g, (int)index_find = %d",index_find,(int)index_find );
							CGR_LOG(log_msg,ERROR_CGR_LOG)
							log_msg.Format("ModeAnalysis::getModeWidths() _lowerBoundaryLine._x.size() = %d",_lowerBoundaryLine._x.size() );
							CGR_LOG(log_msg,ERROR_CGR_LOG)
							log_msg.Format("ModeAnalysis::getModeWidths() _lowerBoundaryLine._y.size() = %d",_lowerBoundaryLine._y.size() );
							CGR_LOG(log_msg,ERROR_CGR_LOG)
						}
						else
						{		
							double y_find = _lowerBoundaryLine.y((int)index_find);
							double x_find = _lowerBoundaryLine.x((int)index_find);
							if( x_find == x_point )
							{ 
								// found previous point at the same x location, is it at a higher y location?
								if( y_point > y_find )
									bottom_line_highest_points[f] = i;

								break;
							}
						}
					}
				}
			}

			
			/////////////////////////////////////////////////////////////////////

			//log_msg.Format("ModeAnalysis::getModeWidths() _upperBoundaryLine._x.size() = %d",_upperBoundaryLine._x.size() );
			//CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

			//log_msg.Format("ModeAnalysis::getModeWidths() _upperBoundaryLine._y.size() = %d",_upperBoundaryLine._y.size() );
			//CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

			// create vector of lowest points on top line at each x_coord
			std::vector<double> top_line_lowest_points;

			lowest_x = 9999999; // Huge number to begin with
			highest_x = -1;
			for(int i=startIndex; i<_upperBoundaryLine.numXPoints(); i++)
			{
				// find x and y position of each point on the line
				double y_point = _upperBoundaryLine.y(i);
				double x_point = _upperBoundaryLine.x(i);

				if( (int)x_point < lowest_x ) lowest_x = (int)x_point;

				if( (int)x_point > highest_x )
				{
					highest_x = (int)x_point;
					// add point to vector
					top_line_lowest_points.push_back(i);
				}
				else
				{ 
					// check for previous occurances of x_point
					int num = (int)(top_line_lowest_points.size());

					for( int f = num-1; f >= 0; f-- )
					{
						double index_find = top_line_lowest_points[f];

						if( (int)index_find < 0
						 || (int)index_find >= (int)_upperBoundaryLine._y.size()
						 || (int)index_find >= (int)_upperBoundaryLine._x.size() )
						{
							CGR_LOG("ModeAnalysis::getModeWidths() : Bounds exceeded",ERROR_CGR_LOG)
							log_msg.Format("ModeAnalysis::getModeWidths() index_find = %g, (int)index_find = %d",index_find,(int)index_find );
							CGR_LOG(log_msg,ERROR_CGR_LOG)
							log_msg.Format("ModeAnalysis::getModeWidths() _upperBoundaryLine._x.size() = %d",_upperBoundaryLine._x.size() );
							CGR_LOG(log_msg,ERROR_CGR_LOG)
							log_msg.Format("ModeAnalysis::getModeWidths() _upperBoundaryLine._y.size() = %d",_upperBoundaryLine._y.size() );
							CGR_LOG(log_msg,ERROR_CGR_LOG)
						}
						else
						{
							double y_find = _upperBoundaryLine.y((int)index_find);
							double x_find = _upperBoundaryLine.x((int)index_find);

							if( x_find == x_point )
							{ // found point at the same x location, is it at a lowest y location?
								if( y_point < y_find )
								{
									top_line_lowest_points[f] = i;
								}
								break;
							}
						}
					}
				}
			}


			double lowest_val = 99999;
			double highest_val = -99999;
			for( int f=0; f < (int)bottom_line_highest_points.size(); f++ )
			{
				lowest_val = bottom_line_highest_points[f] < lowest_val ? bottom_line_highest_points[f] : lowest_val;
				highest_val = bottom_line_highest_points[f] > highest_val ? bottom_line_highest_points[f] : highest_val;
			}

			//log_msg.Format("ModeAnalysis::getModeWidths()   bottom_line_highest_points  lowest_val = %g", lowest_val );
			//CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

			//log_msg.Format("ModeAnalysis::getModeWidths()   bottom_line_highest_points  highest_val = %g", highest_val );
			//CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

			//log_msg.Format("ModeAnalysis::getModeWidths()   _lowerBoundaryLine._x.size() = %d",_lowerBoundaryLine._x.size() );
			//CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

			//log_msg.Format("ModeAnalysis::getModeWidths()   _lowerBoundaryLine._y.size() = %d",_lowerBoundaryLine._y.size() );
			//CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)


			lowest_val = 99999;
			highest_val = -99999;
			for( int f=0; f < (int)top_line_lowest_points.size(); f++ )
			{
				lowest_val = top_line_lowest_points[f] < lowest_val ? top_line_lowest_points[f] : lowest_val;
				highest_val = top_line_lowest_points[f] > highest_val ? top_line_lowest_points[f] : highest_val;
			}

			//log_msg.Format("ModeAnalysis::getModeWidths()   top_line_lowest_points  lowest_val = %g", lowest_val );
			//CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

			//log_msg.Format("ModeAnalysis::getModeWidths()   top_line_lowest_points  highest_val = %g", highest_val );
			//CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

			//log_msg.Format("ModeAnalysis::getModeWidths()   _upperBoundaryLine._x.size() = %d",_upperBoundaryLine._x.size() );
			//CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

			//log_msg.Format("ModeAnalysis::getModeWidths()   _upperBoundaryLine._y.size() = %d",_upperBoundaryLine._y.size() );
			//CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)


			// find match x coordinates on both lines
			int ti = 0;
			for( int bi = 0; bi < (int)(bottom_line_highest_points.size()) ; bi++ )
			{
				double index_b = bottom_line_highest_points[bi];
				double y_b = _lowerBoundaryLine.y((int)index_b);
				double x_b = _lowerBoundaryLine.x((int)index_b);
	            
				if( ti >= (int)(top_line_lowest_points.size()) ) break;
				double index_t = top_line_lowest_points[ti];
				double y_t = _upperBoundaryLine.y((int)index_t);
				double x_t = _upperBoundaryLine.x((int)index_t);
				while(ti < (int)(top_line_lowest_points.size()) )
				{ // find
					index_t = top_line_lowest_points[ti];
					y_t = _upperBoundaryLine.y((int)index_t);
					x_t = _upperBoundaryLine.x((int)index_t);
					if( x_t < x_b ) ti++;
					else break;
				}

				if( ti >= (int)(top_line_lowest_points.size()) ) break;
				if( x_t == x_b )
				{ // found matching x coordinates
				// find width between two lines at this point
					double width = y_t - y_b;
					if(width < 0)
						width = width * -1;

					_modeWidths.push_back(width);
				}
			}

			//////////////////////////////////////////////////////////////////////////////
		}
	}

	modeWidths = _modeWidths;

}

//GDM 30/10/06 overloaded version of getModeWidths which allows the boundary 'map'
//to be windowed using a start and stop index so that the width analysis can be 
//targeted to meaningful sections of the maps, eg avoiding edge hysteresis
void
ModeAnalysis::
getModeWidths(Vector&	modeWidths, 
			  short		startIndex,
			  short		stopIndex)
{
	if(_modeBoundaryLinesLoaded)
	{
		// only do this once
		if(_modeWidths.size() == 0)
		{
			//before we start we need to check that the start and stop index's specced are 
			//useful for this LM. ie is ther a lower and upper line that both traverse
			// the specced window. If not then just examine the whole mode from start to end
			// as would be done if no window specced. This will give a useful set of metrics
			// to gauge the LM by, rather than returning zero's, and not skew any overall SM appraisals. 
			int lowerBoundaryLineStart = (int)_lowerBoundaryLine.x(0);
			int upperBoundaryLineEnd = (int)_upperBoundaryLine.x(_upperBoundaryLine.numXPoints()-1);
			if ((lowerBoundaryLineStart > startIndex) || upperBoundaryLineEnd < stopIndex)
			{
				getModeWidths(modeWidths, 0);	//call original func over entire length
			}
			else //analyse between window, we know line has an index at startIndex thru to  stopIndex
			{
				//////////////////////////////////////////////////////////////////////////////

				// create vector of highest points on bottom line at each x_coord
				std::vector<double> bottom_line_highest_points;

				int lowest_x = 9999999; // Huge number to begin with
				int highest_x = -1;


				CString log_msg;
				//log_msg.AppendFormat("ModeAnalysis::getModeWidths() _lowerBoundaryLine._x.size() = %d",_lowerBoundaryLine._x.size() );
				//CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

				//log_msg.Format("ModeAnalysis::getModeWidths() _lowerBoundaryLine._y.size() = %d",_lowerBoundaryLine._y.size() );
				//CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

				//GDM lower boundsary analysis, ensure we don't exceed boundary or vector length, also we can set start stop = 0 to do whole vector as before
				if (startIndex < 0)
					startIndex = 0;
				if (stopIndex <= startIndex)
					stopIndex = _lowerBoundaryLine.numXPoints();
				if (stopIndex > _lowerBoundaryLine.numXPoints())
					stopIndex = _lowerBoundaryLine.numXPoints();

				for(int i=startIndex; i<stopIndex; i++)
				{
					// find x and y position of each point on the line

					int x_point = (int)_lowerBoundaryLine.x(i);
					int y_point = (int)_lowerBoundaryLine.y(i);

					if( (int)x_point < lowest_x ) 
							lowest_x = (int)x_point;

					if( (int)x_point > highest_x )
					{
						highest_x = (int)x_point;
						// add point to vector
						bottom_line_highest_points.push_back(i);

					}
					else
					{ 
						// check for previous occurances of x_point
						int num = (int)(bottom_line_highest_points.size());

						for( int f = num-1; f >= 0; f-- )
						{
							double index_find = bottom_line_highest_points[f];

							if( (int)index_find < 0
							|| (int)index_find >= (int)_lowerBoundaryLine._y.size()
							|| (int)index_find >= (int)_lowerBoundaryLine._x.size() )
							{
								CGR_LOG("ModeAnalysis::getModeWidths() : Bounds exceeded",ERROR_CGR_LOG)
								log_msg.Format("ModeAnalysis::getModeWidths() index_find = %g, (int)index_find = %d",index_find,(int)index_find );
								CGR_LOG(log_msg,ERROR_CGR_LOG)
								log_msg.Format("ModeAnalysis::getModeWidths() _lowerBoundaryLine._x.size() = %d",_lowerBoundaryLine._x.size() );
								CGR_LOG(log_msg,ERROR_CGR_LOG)
								log_msg.Format("ModeAnalysis::getModeWidths() _lowerBoundaryLine._y.size() = %d",_lowerBoundaryLine._y.size() );
								CGR_LOG(log_msg,ERROR_CGR_LOG)
							}
							else
							{		
								double y_find = _lowerBoundaryLine.y((int)index_find);
								double x_find = _lowerBoundaryLine.x((int)index_find);
								if( x_find == x_point )
								{ 
									// found previous point at the same x location, is it at a higher y location?
									if( y_point > y_find )
										bottom_line_highest_points[f] = i;

									break;
								}
							}
						}
					}
				}

				
				/////////////////////////////////////////////////////////////////////

				//log_msg.Format("ModeAnalysis::getModeWidths() _upperBoundaryLine._x.size() = %d",_upperBoundaryLine._x.size() );
				//CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

				//log_msg.Format("ModeAnalysis::getModeWidths() _upperBoundaryLine._y.size() = %d",_upperBoundaryLine._y.size() );
				//CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

				// create vector of lowest points on top line at each x_coord
				std::vector<double> top_line_lowest_points;

				//GDM Upper boundary analysis, ensure we don't exceed boundary or vector length, also we can set start stop = 0 to do whole vector as before
				if (startIndex < 0)
					startIndex = 0;
				if (stopIndex <= startIndex)
					stopIndex = _upperBoundaryLine.numXPoints();
				if (stopIndex > _upperBoundaryLine.numXPoints())
					stopIndex = _upperBoundaryLine.numXPoints();


				lowest_x = 9999999; // Huge number to begin with
				highest_x = -1;
				for(int i=startIndex; i<stopIndex; i++)
				{
					// find x and y position of each point on the line
					double y_point = _upperBoundaryLine.y(i);
					double x_point = _upperBoundaryLine.x(i);

					if( (int)x_point < lowest_x ) lowest_x = (int)x_point;

					if( (int)x_point > highest_x )
					{
						highest_x = (int)x_point;
						// add point to vector
						top_line_lowest_points.push_back(i);
					}
					else
					{ 
						// check for previous occurances of x_point
						int num = (int)(top_line_lowest_points.size());

						for( int f = num-1; f >= 0; f-- )
						{
							double index_find = top_line_lowest_points[f];

							if( (int)index_find < 0
							|| (int)index_find >= (int)_upperBoundaryLine._y.size()
							|| (int)index_find >= (int)_upperBoundaryLine._x.size() )
							{
								CGR_LOG("ModeAnalysis::getModeWidths() : Bounds exceeded",ERROR_CGR_LOG)
								log_msg.Format("ModeAnalysis::getModeWidths() index_find = %g, (int)index_find = %d",index_find,(int)index_find );
								CGR_LOG(log_msg,ERROR_CGR_LOG)
								log_msg.Format("ModeAnalysis::getModeWidths() _upperBoundaryLine._x.size() = %d",_upperBoundaryLine._x.size() );
								CGR_LOG(log_msg,ERROR_CGR_LOG)
								log_msg.Format("ModeAnalysis::getModeWidths() _upperBoundaryLine._y.size() = %d",_upperBoundaryLine._y.size() );
								CGR_LOG(log_msg,ERROR_CGR_LOG)
							}
							else
							{
								double y_find = _upperBoundaryLine.y((int)index_find);
								double x_find = _upperBoundaryLine.x((int)index_find);

								if( x_find == x_point )
								{ // found point at the same x location, is it at a lowest y location?
									if( y_point < y_find )
									{
										top_line_lowest_points[f] = i;
									}
									break;
								}
							}
						}
					}
				}


				double lowest_val = 99999;
				double highest_val = -99999;
				for( int f=0; f < (int)bottom_line_highest_points.size(); f++ )
				{
					lowest_val = bottom_line_highest_points[f] < lowest_val ? bottom_line_highest_points[f] : lowest_val;
					highest_val = bottom_line_highest_points[f] > highest_val ? bottom_line_highest_points[f] : highest_val;
				}

				//log_msg.Format("ModeAnalysis::getModeWidths()   bottom_line_highest_points  lowest_val = %g", lowest_val );
				//CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

				//log_msg.Format("ModeAnalysis::getModeWidths()   bottom_line_highest_points  highest_val = %g", highest_val );
				//CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

				//log_msg.Format("ModeAnalysis::getModeWidths()   _lowerBoundaryLine._x.size() = %d",_lowerBoundaryLine._x.size() );
				//CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

				//log_msg.Format("ModeAnalysis::getModeWidths()   _lowerBoundaryLine._y.size() = %d",_lowerBoundaryLine._y.size() );
				//CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)


				lowest_val = 99999;
				highest_val = -99999;
				for( int f=0; f < (int)top_line_lowest_points.size(); f++ )
				{
					lowest_val = top_line_lowest_points[f] < lowest_val ? top_line_lowest_points[f] : lowest_val;
					highest_val = top_line_lowest_points[f] > highest_val ? top_line_lowest_points[f] : highest_val;
				}

				//log_msg.Format("ModeAnalysis::getModeWidths()   top_line_lowest_points  lowest_val = %g", lowest_val );
				//CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

				//log_msg.Format("ModeAnalysis::getModeWidths()   top_line_lowest_points  highest_val = %g", highest_val );
				//CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

				//log_msg.Format("ModeAnalysis::getModeWidths()   _upperBoundaryLine._x.size() = %d",_upperBoundaryLine._x.size() );
				//CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

				//log_msg.Format("ModeAnalysis::getModeWidths()   _upperBoundaryLine._y.size() = %d",_upperBoundaryLine._y.size() );
				//CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)


				// find match x coordinates on both lines
				int ti = 0;
				for( int bi = 0; bi < (int)(bottom_line_highest_points.size()) ; bi++ )
				{
					double index_b = bottom_line_highest_points[bi];
					double y_b = _lowerBoundaryLine.y((int)index_b);
					double x_b = _lowerBoundaryLine.x((int)index_b);
		            
					if( ti >= (int)(top_line_lowest_points.size()) ) break;
					double index_t = top_line_lowest_points[ti];
					double y_t = _upperBoundaryLine.y((int)index_t);
					double x_t = _upperBoundaryLine.x((int)index_t);
					while(ti < (int)(top_line_lowest_points.size()) )
					{ // find
						index_t = top_line_lowest_points[ti];
						y_t = _upperBoundaryLine.y((int)index_t);
						x_t = _upperBoundaryLine.x((int)index_t);
						if( x_t < x_b ) ti++;
						else break;
					}

					if( ti >= (int)(top_line_lowest_points.size()) ) break;
					if( x_t == x_b )
					{ // found matching x coordinates
					// find width between two lines at this point
						double width = y_t - y_b;
						if(width < 0)
							width = width * -1;

						_modeWidths.push_back(width);
					}
				}

				//////////////////////////////////////////////////////////////////////////////
			}
		}
	}
	modeWidths = _modeWidths;

}

double
ModeAnalysis::
getModeWidthsStatistic(Vector::VectorAttribute attrib)
{
	double returnValue = 0;

	if(_modeBoundaryLinesLoaded)
	{
		Vector modeWidths;
		getModeWidths(modeWidths);

		returnValue = modeWidths.getVectorAttribute(attrib);
	}

	return returnValue;
}
//
////////////////////////////////////////////////////////////////////////////////////////////
//
void
ModeAnalysis::
calcMaxModeWidth()
{
	if(_modeBoundaryLinesLoaded)
	{
		if(_maxModeWidth == 0)
		{
			Vector modeWidths;
			getModeWidths(modeWidths);

			_maxModeWidth = modeWidths.getVectorAttribute(Vector::VectorAttribute::max);
		}
	}
}
void
ModeAnalysis::
calcMinModeWidth()
{
	if(_modeBoundaryLinesLoaded)
	{
		if(_minModeWidth == 0)
		{
			Vector modeWidths;
			getModeWidths(modeWidths);

			_minModeWidth = modeWidths.getVectorAttribute(Vector::VectorAttribute::min);
		}
	}
}
void
ModeAnalysis::
calcMeanModeWidths()
{
	if(_modeBoundaryLinesLoaded)
	{
		if(_meanModeWidths == 0)
		{
			Vector modeWidths;
			getModeWidths(modeWidths);

			_meanModeWidths = modeWidths.getVectorAttribute(Vector::VectorAttribute::mean);
		}
	}
}
void
ModeAnalysis::
calcSumModeWidths()
{
	if(_modeBoundaryLinesLoaded)
	{
		if(_sumModeWidths == 0)
		{
			Vector modeWidths;
			getModeWidths(modeWidths);

			_sumModeWidths = modeWidths.getVectorAttribute(Vector::VectorAttribute::sum);
		}
	}
}
void
ModeAnalysis::
calcNumModeWidthSamples()
{
	if(_modeBoundaryLinesLoaded)
	{
		if(_modeLength == 0)
		{
			Vector modeWidths;
			getModeWidths(modeWidths);

			_modeLength = (int)modeWidths.getVectorAttribute(Vector::VectorAttribute::count);
		}
	}
}
void
ModeAnalysis::
calcVarModeWidths()
{
	if(_modeBoundaryLinesLoaded)
	{
		if(_varModeWidths == 0)
		{
			Vector modeWidths;
			getModeWidths(modeWidths);

			_varModeWidths = modeWidths.getVectorAttribute(Vector::VectorAttribute::variance);
		}
	}
}
void
ModeAnalysis::
calcStdDevModeWidths()
{
	if(_modeBoundaryLinesLoaded)
	{
		if(_stdDevModeWidths == 0)
		{
			Vector modeWidths;
			getModeWidths(modeWidths);

			_stdDevModeWidths = modeWidths.getVectorAttribute(Vector::VectorAttribute::stdDev);
		}
	}
}
//
/////////////////////////////////////////////////////////////////////
///
//
void
ModeAnalysis::
getModeUpperSlopes(Vector& modeUpperSlopes)
{
	if(_modeBoundaryLinesLoaded)
	{
		// only do this once
		if(_modeUpperSlopes.size() == 0)
		{
			_modeUpperSlopes = _upperBoundaryLine.getSlopes(_qaThresholds->_slopeWindow);
		}
	}

	modeUpperSlopes = _modeUpperSlopes;
}
double
ModeAnalysis::
getModeUpperSlopesStatistic(Vector::VectorAttribute attrib)
{
	double returnValue = 0;

	if(_modeBoundaryLinesLoaded)
	{
		Vector modeUpperSlopes;
		getModeUpperSlopes(modeUpperSlopes);

		returnValue = modeUpperSlopes.getVectorAttribute(attrib);
	}

	return returnValue;
}
//
/////////////////////////////////////////////////////////////////////
///
//
void
ModeAnalysis::
getModeSlopes(Vector& lowerModeSlopes,
			  Vector& upperModeSlopes)
{
	if(_modeBoundaryLinesLoaded)
	{
		// only do this once
		if(_modeLowerSlopes.size() == 0)
		{
			_modeLowerSlopes = _lowerBoundaryLine.getSlopes(_qaThresholds->_slopeWindow);
			_modeUpperSlopes = _upperBoundaryLine.getSlopes(_qaThresholds->_slopeWindow);

		}
	}

	lowerModeSlopes = _modeLowerSlopes;
	upperModeSlopes = _modeUpperSlopes;
}

//
////////////////////////////////////////////////////////////////////////////////////////////
//
void
ModeAnalysis::
calcMaxModeSlope()
{
	if(_modeBoundaryLinesLoaded)
	{
		calcMaxModeLowerSlope();
		calcMaxModeUpperSlope();
	}
}
void
ModeAnalysis::
calcMinModeSlope()
{
	if(_modeBoundaryLinesLoaded)
	{
		calcMinModeLowerSlope();
		calcMinModeUpperSlope();
	}
}
void
ModeAnalysis::
calcMeanModeSlopes()
{
	if(_modeBoundaryLinesLoaded)
	{
		calcMeanModeLowerSlopes();
		calcMeanModeUpperSlopes();
	}
}
void
ModeAnalysis::
calcSumModeSlopes()
{
	if(_modeBoundaryLinesLoaded)
	{
		calcSumModeLowerSlopes();
		calcSumModeUpperSlopes();
	}
}
void
ModeAnalysis::
calcNumModeSlopeSamples()
{
	if(_modeBoundaryLinesLoaded)
	{
		calcNumModeLowerSlopeSamples();
		calcNumModeUpperSlopeSamples();
	}
}
void
ModeAnalysis::
calcVarModeSlopes()
{
	if(_modeBoundaryLinesLoaded)
	{
		calcVarModeLowerSlopes();
		calcVarModeUpperSlopes();
	}
}
void
ModeAnalysis::
calcStdDevModeSlopes()
{
	if(_modeBoundaryLinesLoaded)
	{
		calcStdDevModeLowerSlopes();
		calcStdDevModeUpperSlopes();
	}
}
//
////////////////////////////////////////////////////////////////////////////////////////////
//
void
ModeAnalysis::
calcMaxModeUpperSlope()
{
	if(_modeBoundaryLinesLoaded)
	{
		if(_maxModeUpperSlope == 0)
		{
			Vector modeUpperSlopes;
			getModeUpperSlopes(modeUpperSlopes);

			_maxModeUpperSlope = modeUpperSlopes.getVectorAttribute(Vector::VectorAttribute::max);
		}
	}
}
void
ModeAnalysis::
calcMinModeUpperSlope()
{
	if(_modeBoundaryLinesLoaded)
	{
		if(_minModeUpperSlope == 0)
		{
			Vector modeUpperSlopes;
			getModeUpperSlopes(modeUpperSlopes);

			_minModeUpperSlope = modeUpperSlopes.getVectorAttribute(Vector::VectorAttribute::min);
		}
	}
}
void
ModeAnalysis::
calcMeanModeUpperSlopes()
{
	if(_modeBoundaryLinesLoaded)
	{
		if(_meanModeUpperSlopes == 0)
		{
			Vector modeUpperSlopes;
			getModeUpperSlopes(modeUpperSlopes);

			_meanModeUpperSlopes = modeUpperSlopes.getVectorAttribute(Vector::VectorAttribute::mean);
		}
	}
}
void
ModeAnalysis::
calcSumModeUpperSlopes()
{
	if(_modeBoundaryLinesLoaded)
	{
		if(_sumModeUpperSlopes == 0)
		{
			Vector modeUpperSlopes;
			getModeUpperSlopes(modeUpperSlopes);

			_sumModeUpperSlopes = modeUpperSlopes.getVectorAttribute(Vector::VectorAttribute::sum);
		}
	}
}
void
ModeAnalysis::
calcNumModeUpperSlopeSamples()
{
	if(_modeBoundaryLinesLoaded)
	{
		if(_modeLength == 0)
		{
			Vector modeUpperSlopes;
			getModeUpperSlopes(modeUpperSlopes);

			_modeLength = (int)modeUpperSlopes.getVectorAttribute(Vector::VectorAttribute::count);
		}
	}
}
void
ModeAnalysis::
calcVarModeUpperSlopes()
{
	if(_modeBoundaryLinesLoaded)
	{
		if(_varModeUpperSlopes == 0)
		{
			Vector modeUpperSlopes;
			getModeUpperSlopes(modeUpperSlopes);

			_varModeUpperSlopes = modeUpperSlopes.getVectorAttribute(Vector::VectorAttribute::variance);
		}
	}
}
void
ModeAnalysis::
calcStdDevModeUpperSlopes()
{
	if(_modeBoundaryLinesLoaded)
	{
		if(_stdDevModeUpperSlopes == 0)
		{
			Vector modeUpperSlopes;
			getModeUpperSlopes(modeUpperSlopes);

			_stdDevModeUpperSlopes = modeUpperSlopes.getVectorAttribute(Vector::VectorAttribute::stdDev);
		}
	}
}
//
/////////////////////////////////////////////////////////////////////
///
//
void
ModeAnalysis::
getModeLowerSlopes(Vector& modeLowerSlopes)
{
	if(_modeBoundaryLinesLoaded)
	{
		// only do this once
		if(_modeLowerSlopes.size() == 0)
		{
			_modeLowerSlopes = _lowerBoundaryLine.getSlopes(_qaThresholds->_slopeWindow);
		}
	}

	modeLowerSlopes = _modeLowerSlopes;
}
double
ModeAnalysis::
getModeLowerSlopesStatistic(Vector::VectorAttribute attrib)
{
	double returnValue = 0;

	if(_modeBoundaryLinesLoaded)
	{
		Vector modeLowerSlopes;
		getModeLowerSlopes(modeLowerSlopes);

		returnValue = modeLowerSlopes.getVectorAttribute(attrib);
	}

	return returnValue;
}
//
////////////////////////////////////////////////////////////////////////////////////////////
//
void
ModeAnalysis::
calcMaxModeLowerSlope()
{
	if(_modeBoundaryLinesLoaded)
	{
		if(_maxModeLowerSlope == 0)
		{
			Vector modeLowerSlopes;
			getModeLowerSlopes(modeLowerSlopes);

			_maxModeLowerSlope = modeLowerSlopes.getVectorAttribute(Vector::VectorAttribute::max);
		}
	}
}
void
ModeAnalysis::
calcMinModeLowerSlope()
{
	if(_modeBoundaryLinesLoaded)
	{
		if(_minModeLowerSlope == 0)
		{
			Vector modeLowerSlopes;
			getModeLowerSlopes(modeLowerSlopes);

			_minModeLowerSlope = modeLowerSlopes.getVectorAttribute(Vector::VectorAttribute::min);
		}
	}
}
void
ModeAnalysis::
calcMeanModeLowerSlopes()
{
	if(_modeBoundaryLinesLoaded)
	{
		if(_meanModeLowerSlopes == 0)
		{
			Vector modeLowerSlopes;
			getModeLowerSlopes(modeLowerSlopes);

			_meanModeLowerSlopes = modeLowerSlopes.getVectorAttribute(Vector::VectorAttribute::mean);
		}
	}
}
void
ModeAnalysis::
calcSumModeLowerSlopes()
{
	if(_modeBoundaryLinesLoaded)
	{
		if(_sumModeLowerSlopes == 0)
		{
			Vector modeLowerSlopes;
			getModeLowerSlopes(modeLowerSlopes);

			_sumModeLowerSlopes = modeLowerSlopes.getVectorAttribute(Vector::VectorAttribute::sum);
		}
	}
}
void
ModeAnalysis::
calcNumModeLowerSlopeSamples()
{
	if(_modeBoundaryLinesLoaded)
	{
		if(_modeLength == 0)
		{
			Vector modeLowerSlopes;
			getModeLowerSlopes(modeLowerSlopes);

			_modeLength = (int)modeLowerSlopes.getVectorAttribute(Vector::VectorAttribute::count);
		}
	}
}
void
ModeAnalysis::
calcVarModeLowerSlopes()
{
	if(_modeBoundaryLinesLoaded)
	{
		if(_varModeLowerSlopes == 0)
		{
			Vector modeLowerSlopes;
			getModeLowerSlopes(modeLowerSlopes);

			_varModeLowerSlopes = modeLowerSlopes.getVectorAttribute(Vector::VectorAttribute::variance);
		}
	}
}
void
ModeAnalysis::
calcStdDevModeLowerSlopes()
{
	if(_modeBoundaryLinesLoaded)
	{
		if(_stdDevModeLowerSlopes == 0)
		{
			Vector modeLowerSlopes;
			getModeLowerSlopes(modeLowerSlopes);

			_stdDevModeLowerSlopes = modeLowerSlopes.getVectorAttribute(Vector::VectorAttribute::stdDev);
		}
	}
}

///////////////////////////////////////////////////////////////////////////////

double
ModeAnalysis::
getLowerModeModalDistortionAngle()
{
	return _lowerModalDistortionAngle;
}

double
ModeAnalysis::
getLowerModeMaxModalDistortionAngleXPosition()
{
	return _maxLowerModalDistortionAngleXPosition;
}

double
ModeAnalysis::
getUpperModeModalDistortionAngle()
{
	return _upperModalDistortionAngle;
}

double
ModeAnalysis::
getUpperModeMaxModalDistortionAngleXPosition()
{
	return _maxUpperModalDistortionAngleXPosition;
}

///////////////////////////////////////////////////////////////////////////////

void
ModeAnalysis::
getModeBDCAreas(Vector& lowerModeBDCAreas,
				Vector& upperModeBDCAreas)
{
	if(_modeBoundaryLinesLoaded)
	{
		// only do this once
		if(_lowerModeBDCAreas.size() == 0)
			_lowerModeBDCAreas = _lowerBoundaryLine.getModeBDCAreas();

		if(_upperModeBDCAreas.size() == 0)
			_upperModeBDCAreas = _upperBoundaryLine.getModeBDCAreas();
	}

	lowerModeBDCAreas = _lowerModeBDCAreas;
	upperModeBDCAreas = _upperModeBDCAreas;
}

double
ModeAnalysis::
getLowerModeBDCAreasStatistic(Vector::VectorAttribute attrib)
{
	double returnValue = 0;

	if(_modeBoundaryLinesLoaded)
	{
		Vector lowerModeBDCAreas;
		Vector upperModeBDCAreas;
		getModeBDCAreas(lowerModeBDCAreas, upperModeBDCAreas);

		returnValue = lowerModeBDCAreas.getVectorAttribute(attrib);
	}

	return returnValue;
}
double
ModeAnalysis::
getUpperModeBDCAreasStatistic(Vector::VectorAttribute attrib)
{
	double returnValue = 0;

	if(_modeBoundaryLinesLoaded)
	{
		Vector lowerModeBDCAreas;
		Vector upperModeBDCAreas;
		getModeBDCAreas(lowerModeBDCAreas, upperModeBDCAreas);

		returnValue = upperModeBDCAreas.getVectorAttribute(attrib);
	}

	return returnValue;
}
void
ModeAnalysis::
calcMaxModeBDCArea()
{
	if(_modeBoundaryLinesLoaded)
	{
		if(_lowerMaxModeBDCArea == 0)
			_lowerMaxModeBDCArea = _lowerBoundaryLine.getMaxModeBDCArea();

		if(_upperMaxModeBDCArea == 0)
			_upperMaxModeBDCArea = _upperBoundaryLine.getMaxModeBDCArea();
	}
}
void
ModeAnalysis::
calcMinModeBDCArea()
{
	if(_modeBoundaryLinesLoaded)
	{
		if(_lowerMinModeBDCArea == 0)
			_lowerMinModeBDCArea = _lowerBoundaryLine.getMinModeBDCArea();

		if(_upperMinModeBDCArea == 0)
			_upperMinModeBDCArea = _upperBoundaryLine.getMinModeBDCArea();

	}
}
void
ModeAnalysis::
calcMeanModeBDCAreas()
{
	if(_modeBoundaryLinesLoaded)
	{
		if(_lowerMeanModeBDCAreas == 0)
			_lowerMeanModeBDCAreas = _lowerBoundaryLine.getMeanModeBDCAreas();
		if(_upperMeanModeBDCAreas == 0)
			_upperMeanModeBDCAreas = _upperBoundaryLine.getMeanModeBDCAreas();
	}
}
void
ModeAnalysis::
calcSumModeBDCAreas()
{
	if(_modeBoundaryLinesLoaded)
	{
		if(_lowerSumModeBDCAreas == 0)
			_lowerSumModeBDCAreas = _lowerBoundaryLine.getSumModeBDCAreas();
		if(_upperMeanModeBDCAreas == 0)
			_upperMeanModeBDCAreas = _upperBoundaryLine.getMeanModeBDCAreas();
	}
}
void
ModeAnalysis::
calcNumModeBDCAreas()
{
	if(_modeBoundaryLinesLoaded)
	{
		if(_lowerNumModeBDCAreas == 0)
			_lowerNumModeBDCAreas = _lowerBoundaryLine.getNumModeBDCAreas();
		if(_upperMeanModeBDCAreas == 0)
			_upperMeanModeBDCAreas = _upperBoundaryLine.getMeanModeBDCAreas();
	}
}
void
ModeAnalysis::
calcStdDevModeBDCAreas()
{
	if(_modeBoundaryLinesLoaded)
	{
		if(_lowerStdDevModeBDCAreas == 0)
			_lowerStdDevModeBDCAreas = _lowerBoundaryLine.getStdDevModeBDCAreas();
		if(_upperMeanModeBDCAreas == 0)
			_upperMeanModeBDCAreas = _upperBoundaryLine.getMeanModeBDCAreas();
	}
}
void
ModeAnalysis::
calcVarModeBDCAreas()
{
	if(_modeBoundaryLinesLoaded)
	{
		if(_lowerVarModeBDCAreas == 0)
			_lowerVarModeBDCAreas = _lowerBoundaryLine.getVarModeBDCAreas();
		if(_upperMeanModeBDCAreas == 0)
			_upperMeanModeBDCAreas = _upperBoundaryLine.getMeanModeBDCAreas();
	}
}
///////////////////////////////////////////////////////////////////////////////

void
ModeAnalysis::
getModeBDCAreaXLengths(Vector& lowerModeBDCAreaXLengths, Vector& upperModeBDCAreaXLengths)
{
	if(_modeBoundaryLinesLoaded)
	{
		// only do this once
		if(_lowerModeBDCAreaXLengths.size() == 0)
			_lowerModeBDCAreaXLengths = _lowerBoundaryLine.getModeBDCAreaXLengths();

		if(_upperModeBDCAreaXLengths.size() == 0)
			_upperModeBDCAreaXLengths = _lowerBoundaryLine.getModeBDCAreaXLengths();
	}

	lowerModeBDCAreaXLengths = _lowerModeBDCAreaXLengths;
	upperModeBDCAreaXLengths = _upperModeBDCAreaXLengths;

}
double
ModeAnalysis::
getLowerModeBDCAreaXLengthsStatistic(Vector::VectorAttribute attrib)
{
	double returnValue = 0;

	if(_modeBoundaryLinesLoaded)
	{
		Vector lowerModeBDCAreaXLengths;
		Vector upperModeBDCAreaXLengths;
		getModeBDCAreaXLengths(lowerModeBDCAreaXLengths, upperModeBDCAreaXLengths);

		returnValue = lowerModeBDCAreaXLengths.getVectorAttribute(attrib);
	}

	return returnValue;
}
double
ModeAnalysis::
getUpperModeBDCAreaXLengthsStatistic(Vector::VectorAttribute attrib)
{
	double returnValue = 0;

	if(_modeBoundaryLinesLoaded)
	{
		Vector lowerModeBDCAreaXLengths;
		Vector upperModeBDCAreaXLengths;
		getModeBDCAreaXLengths(lowerModeBDCAreaXLengths, upperModeBDCAreaXLengths);

		returnValue = upperModeBDCAreaXLengths.getVectorAttribute(attrib);
	}

	return returnValue;
}

void
ModeAnalysis::
calcMaxModeBDCAreaXLength()
{
	if(_modeBoundaryLinesLoaded)
	{
		if(_lowerMaxModeBDCAreaXLength == 0)
			_lowerMaxModeBDCAreaXLength = _lowerBoundaryLine.getMaxModeBDCAreaXLength();
		if(_upperMaxModeBDCAreaXLength == 0)
			_upperMaxModeBDCAreaXLength = _upperBoundaryLine.getMaxModeBDCAreaXLength();
	}
}
void
ModeAnalysis::
calcMinModeBDCAreaXLength()
{
	if(_modeBoundaryLinesLoaded)
	{
		if(_lowerMinModeBDCAreaXLength == 0)
			_lowerMinModeBDCAreaXLength = _lowerBoundaryLine.getMinModeBDCAreaXLength();
		if(_upperMinModeBDCAreaXLength == 0)
			_upperMinModeBDCAreaXLength = _upperBoundaryLine.getMinModeBDCAreaXLength();
		
	}
}
void
ModeAnalysis::
calcMeanModeBDCAreaXLengths()
{
	if(_modeBoundaryLinesLoaded)
	{
		if(_lowerMeanModeBDCAreaXLengths == 0)
			_lowerMeanModeBDCAreaXLengths = _lowerBoundaryLine.getMeanModeBDCAreaXLengths();
		if(_upperMeanModeBDCAreaXLengths == 0)
			_upperMeanModeBDCAreaXLengths = _upperBoundaryLine.getMeanModeBDCAreaXLengths();
		
	}
}
void
ModeAnalysis::
calcSumModeBDCAreaXLengths()
{
	if(_modeBoundaryLinesLoaded)
	{
		if(_lowerSumModeBDCAreaXLengths == 0)
			_lowerSumModeBDCAreaXLengths = _lowerBoundaryLine.getSumModeBDCAreaXLengths();
		if(_upperSumModeBDCAreaXLengths == 0)
			_upperSumModeBDCAreaXLengths = _upperBoundaryLine.getSumModeBDCAreaXLengths();		
	}
}
void
ModeAnalysis::
calcNumModeBDCAreaXLengths()
{
	if(_modeBoundaryLinesLoaded)
	{
		if(_lowerNumModeBDCAreaXLengths == 0)
			_lowerNumModeBDCAreaXLengths = _lowerBoundaryLine.getNumModeBDCAreaXLengths();
		if(_upperNumModeBDCAreaXLengths == 0)
			_upperNumModeBDCAreaXLengths = _upperBoundaryLine.getNumModeBDCAreaXLengths();		
	}
}
void
ModeAnalysis::
calcStdDevModeBDCAreaXLengths()
{
	if(_modeBoundaryLinesLoaded)
	{
		if(_lowerStdDevModeBDCAreaXLengths == 0)
			_lowerStdDevModeBDCAreaXLengths = _lowerBoundaryLine.getStdDevModeBDCAreaXLengths();
		if(_upperStdDevModeBDCAreaXLengths == 0)
			_upperStdDevModeBDCAreaXLengths = _upperBoundaryLine.getStdDevModeBDCAreaXLengths();		
	}
}
void
ModeAnalysis::
calcVarModeBDCAreaXLengths()
{
	if(_modeBoundaryLinesLoaded)
	{
		if(_lowerVarModeBDCAreaXLengths == 0)
			_lowerVarModeBDCAreaXLengths = _lowerBoundaryLine.getVarModeBDCAreaXLengths();
		if(_upperVarModeBDCAreaXLengths == 0)
			_upperVarModeBDCAreaXLengths = _upperBoundaryLine.getVarModeBDCAreaXLengths();		
	}
}
//
//
///	End of ModeAnalysis.cpp
///////////////////////////////////////////////////////////////////////////////
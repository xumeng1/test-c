// Copyright (c) 2004 PXIT Ltd. All Rights Reserved

///////////////////////////////////////////////////////////////////////////////
///
//	Class	:
//
//	Contents:
//
//	Author	:
//
///
///////////////////////////////////////////////////////////////////////////////
///
//
#ifndef __OVERALLMODEMAPANALYSIS_H_
#define __OVERALLMODEMAPANALYSIS_H_

#pragma once

//
///////////////////////////////////////////////////////////////////////////////
//
#include <afx.h>
#include <vector>

#include "DSDBRSuperMode.h"
#include "PassFailAnalysis.h"
#include "CLoseGridFile.h"

using namespace std;

/////////////////////////////////////////////////////////////////////

class OverallModeMapAnalysis
{
public:
	//OverallModeMapAnalysis();
	OverallModeMapAnalysis();
	~OverallModeMapAnalysis(void);

	////////////////////////////////////////
	///
	//	Error codes
	//
	typedef enum rcode
	{
		ok = 0
	};
	//
	///////////////////////////////////////

	///////////////////////////////////////////////////////
	///
	//	 Public functions
	//
	rcode	runAnalysis(vector<CDSDBRSuperMode>* p_superModes,
						int				xAxisLength,
						int				yAxisLength,
						vector<double>& continuityValues,
						short			superModesRemoved,
						CString			resultsDir,
						CString			laserId,
						CString			dateTimeStamp);

	///////////////////////////////////////////////////////
	///
	//	public attributes
	//
	int	numSuperModes();

	///////////////////////////////////////////////////////
	///
	//	 private functions
	//
	void	init();
	
	rcode	runQaAnalysis();
	rcode	runPassFailAnalysis();

	CString	createQaResultsAbsFilePath(CString	resultsDir,
									CString		laserId,
									CString		dateTimeStamp);
	CString	createPassFailResultsAbsFilePath(CString	resultsDir,
											CString		laserId,
											CString		dateTimeStamp);

	CString	createPassFailCollateResultsAbsFilePath(CString	resultsDir,
													CString		laserId,
													CString		dateTimeStamp);

	rcode	writeQaResultsToFile();
	rcode	writePassFailResultsToFile( CLoseGridFile &passFailResults );

	void	getQAError(int		qaError,
					int&		errorNumber,
					CString&	errorMsg);

	bool	_screeningPassed;

	int		_qaDetectedError;

	///////////////////////////////////////////////////////
	//
	void	getModeWidths();
	void	getMaxModeWidths();
	void	getMinModeWidths();
	void	getMeanModeWidths();
	void	getSumModeWidths();
	void	getVarModeWidths();
	void	getStdDevModeWidths();
	void	getMedianModeWidths();
	void	getNumModeWidthSamples();

	///////////////////////////////////////////////////////

	void	getModeUpperSlopes();
	void	getMaxModeUpperSlopes();
	void	getMinModeUpperSlopes();
	void	getMeanModeUpperSlopes();
	void	getSumModeUpperSlopes();
	void	getVarModeUpperSlopes();
	void	getStdDevModeUpperSlopes();
	void	getMedianModeUpperSlopes();
	void	getNumModeUpperSlopeSamples();

	///////////////////////////////////////////////////////

	void	getModeLowerSlopes();
	void	getMaxModeLowerSlopes();
	void	getMinModeLowerSlopes();
	void	getMeanModeLowerSlopes();
	void	getSumModeLowerSlopes();
	void	getVarModeLowerSlopes();
	void	getStdDevModeLowerSlopes();
	void	getMedianModeLowerSlopes();
	void	getNumModeLowerSlopeSamples();

	///////////////////////////////////////////////////////

	void	getModalDistortionAngles();
	void	getModalDistortionAngleXPositions();

	///////////////////////////////////////////////////////

	void	getModeBDCAreas();
	void	getMaxModeBDCAreas();
	void	getMinModeBDCAreas();
	void	getMeanModeBDCAreas();
	void	getSumModeBDCAreas();
	void	getVarModeBDCAreas();
	void	getStdDevModeBDCAreas();
	void	getMedianModeBDCAreas();
	void	getNumModeBDCAreaSamples();

	///////////////////////////////////////////////////////

	void	getModeBDCAreaXLengths();
	void	getMaxModeBDCAreaXLengths();
	void	getMinModeBDCAreaXLengths();
	void	getMeanModeBDCAreaXLengths();
	void	getSumModeBDCAreaXLengths();
	void	getVarModeBDCAreaXLengths();
	void	getStdDevModeBDCAreaXLengths();
	void	getMedianModeBDCAreaXLengths();
	void	getNumModeBDCAreaXLengthSamples();

	///////////////////////////////////////////////////////

	void	getMiddleLineRMSValues();
	void	getMiddleLineSlopes();

	///////////////////////////////////////////////////////

	void	getMaxContinuityValue();
	void	getMinContinuityValue();
	void	getMeanContinuityValues();
	void	getSumContinuityValues();
	void	getVarContinuityValues();
	void	getStdDevContinuityValues();
	void	getMedianContinuityValues();
	void	getNumContinuityValues();

	///////////////////////////////////////////////////////

	void	getMaxPrGap();
	void	getMaxLowerPr();
	void	getMinUpperPr();

	//
	///////////////////////////////////////////////////////

	///////////////////////////////////////////////////////
	///
	//	private attributes
	//
						// vector of mode width vectors
	vector<Vector>	_modeWidths;
	vector<double>	_maxModeWidths;
	vector<double>	_minModeWidths;
	vector<double>	_meanModeWidths;
	vector<double>	_sumModeWidths;
	vector<double>	_varModeWidths;
	vector<double>	_stdDevModeWidths;
	vector<double>	_medianModeWidths;
	vector<double>	_numModeWidthSamples;

	///////////////////////////////////////

	vector<Vector>	_modeUpperSlopes;
	vector<double>	_maxModeUpperSlopes;
	vector<double>	_minModeUpperSlopes;
	vector<double>	_meanModeUpperSlopes;
	vector<double>	_sumModeUpperSlopes;
	vector<double>	_varModeUpperSlopes;
	vector<double>	_stdDevModeUpperSlopes;
	vector<double>	_medianModeUpperSlopes;
	vector<double>	_numModeUpperSlopeSamples;

	///////////////////////////////////////

	vector<Vector>	_modeLowerSlopes;
	vector<double>	_maxModeLowerSlopes;
	vector<double>	_minModeLowerSlopes;
	vector<double>	_meanModeLowerSlopes;
	vector<double>	_sumModeLowerSlopes;
	vector<double>	_varModeLowerSlopes;
	vector<double>	_stdDevModeLowerSlopes;
	vector<double>	_medianModeLowerSlopes;
	vector<double>	_numModeLowerSlopeSamples;

	///////////////////////////////////////

	vector<double>	_lowerModalDistortionAngles;
	vector<double>	_lowerModalDistortionAngleXPositions;

	vector<double>	_upperModalDistortionAngles;
	vector<double>	_upperModalDistortionAngleXPositions;

	///////////////////////////////////////

	vector<Vector>	_lowerModeBDCAreas;
	vector<double>	_lowerMaxModeBDCAreas;
	vector<double>	_lowerMinModeBDCAreas;
	vector<double>	_lowerMeanModeBDCAreas;
	vector<double>	_lowerSumModeBDCAreas;
	vector<double>	_lowerVarModeBDCAreas;
	vector<double>	_lowerStdDevModeBDCAreas;
	vector<double>	_lowerMedianModeBDCAreas;
	vector<double>	_lowerNumModeBDCAreaSamples;

	vector<Vector>	_upperModeBDCAreas;
	vector<double>	_upperMaxModeBDCAreas;
	vector<double>	_upperMinModeBDCAreas;
	vector<double>	_upperMeanModeBDCAreas;
	vector<double>	_upperSumModeBDCAreas;
	vector<double>	_upperVarModeBDCAreas;
	vector<double>	_upperStdDevModeBDCAreas;
	vector<double>	_upperMedianModeBDCAreas;
	vector<double>	_upperNumModeBDCAreaSamples;

	///////////////////////////////////////

	vector<Vector>	_lowerModeBDCAreaXLengths;
	vector<double>	_lowerMaxModeBDCAreaXLengths;
	vector<double>	_lowerMinModeBDCAreaXLengths;
	vector<double>	_lowerMeanModeBDCAreaXLengths;
	vector<double>	_lowerSumModeBDCAreaXLengths;
	vector<double>	_lowerVarModeBDCAreaXLengths;
	vector<double>	_lowerStdDevModeBDCAreaXLengths;
	vector<double>	_lowerMedianModeBDCAreaXLengths;
	vector<double>	_lowerNumModeBDCAreaXLengthSamples;

	vector<Vector>	_upperModeBDCAreaXLengths;
	vector<double>	_upperMaxModeBDCAreaXLengths;
	vector<double>	_upperMinModeBDCAreaXLengths;
	vector<double>	_upperMeanModeBDCAreaXLengths;
	vector<double>	_upperSumModeBDCAreaXLengths;
	vector<double>	_upperVarModeBDCAreaXLengths;
	vector<double>	_upperStdDevModeBDCAreaXLengths;
	vector<double>	_upperMedianModeBDCAreaXLengths;
	vector<double>	_upperNumModeBDCAreaXLengthSamples;

	///////////////////////////////////////

	vector<double>	_middleLineRMSValues;
	vector<double>	_middleLineSlopes;

	///////////////////////////////////////

	Vector			_continuityValues;
	double			_maxContinuityValue;
	double			_minContinuityValue;
	double			_meanContinuityValues;
	double			_sumContinuityValues;
	double			_varContinuityValues;
	double			_stdDevContinuityValues;
	double			_medianContinuityValues;
	double			_numContinuityValues;

	double			_maxPrGap;
	double			_maxLowerPr;
	double			_minUpperPr;

	//
	///////////////////////////////////////

	int	_numSuperModes;
	vector<CDSDBRSuperMode> *_p_superModes;
	
	short	_numSuperModesRemoved;

	CString	_resultsDir;
	CString	_laserId;
	CString	_dateTimeStamp;

	int	_xAxisLength;
	int	_yAxisLength;

	///////////////////////////////////

	CString	_qaResultsAbsFilePath;
	CString	_passFailResultsAbsFilePath;
	CString	_collatedPassFailResultsAbsFilePath;

	///////////////////////////////////

	bool	_qaAnalysisComplete;
	bool	_passFailAnalysisComplete;

	///////////////////////////////////
	///
	//
	PassFailThresholds	getPassFailThresholds();
	PassFailThresholds	_smPassFailThresholds;

	//Data
	PassFailAnalysis	_maxModeBordersRemovedAnalysis;
	PassFailAnalysis	_maxModeWidthPFAnalysis;
	PassFailAnalysis	_minModeWidthPFAnalysis;
	
	PassFailAnalysis	_lowerMaxModeBDCAreaPFAnalysis;
	PassFailAnalysis	_lowerMaxModeBDCAreaXLengthPFAnalysis;
	PassFailAnalysis	_lowerSumModeBDCAreasPFAnalysis;
	PassFailAnalysis	_lowerNumModeBDCAreasPFAnalysis;
	PassFailAnalysis	_lowerModalDistortionAnglePFAnalysis;
	PassFailAnalysis	_lowerModalDistortionAngleXPositionPFAnalysis;

	PassFailAnalysis	_upperMaxModeBDCAreaPFAnalysis;
	PassFailAnalysis	_upperMaxModeBDCAreaXLengthPFAnalysis;
	PassFailAnalysis	_upperSumModeBDCAreasPFAnalysis;
	PassFailAnalysis	_upperNumModeBDCAreasPFAnalysis;
	PassFailAnalysis	_upperModalDistortionAnglePFAnalysis;
	PassFailAnalysis	_upperModalDistortionAngleXPositionPFAnalysis;

	PassFailAnalysis	_middleLineRMSValuesPFAnalysis;
	PassFailAnalysis	_maxMiddleLineSlopePFAnalysis;
	PassFailAnalysis	_minMiddleLineSlopePFAnalysis;
	PassFailAnalysis	_maxPrGapPFAnalysis;
	PassFailAnalysis	_maxLowerPrPFAnalysis;
	PassFailAnalysis	_minUpperPrPFAnalysis;

	PassFailAnalysis	_lowerMaxModeLineSlopePFAnalysis;
	PassFailAnalysis	_upperMaxModeLineSlopePFAnalysis;
	PassFailAnalysis	_lowerMinModeLineSlopePFAnalysis;
	PassFailAnalysis	_upperMinModeLineSlopePFAnalysis;

	//
	/////////////////////////////////////////////////////////////////
};
//
///////////////////////////////////////////////////////////////////////////////
#endif

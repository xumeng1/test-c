// Copyright (c) 2004 PXIT Ltd. All Rights Reserved

///////////////////////////////////////////////////////////////////////////////
///
//	Class	:
//
//	Contents:
//
//	Author	:
//
///
///////////////////////////////////////////////////////////////////////////////
///
//

#include "stdafx.h"


//#define _USE_MATH_DEFINES
#include <math.h>

//#include "VectorAnalysis.h"

#include "ModeBoundaryLine.h"
#include "CGResearchServerInterface.h"

//
/////////////////////////////////////////////////////////////////////
//

#define new DEBUG_NEW

/////////////////////////////////////////////////////////////////////

//
///	ctor and dtor
//
ModeBoundaryLine::
ModeBoundaryLine(const vector<double>& xPoints,
				 const vector<double>& yPoints,
				 bool crosses_front_sections )
{
	CGR_LOG("ModeBoundaryLine::ModeBoundaryLine() entered",DIAGNOSTIC_CGR_LOG)

	_x = xPoints;
	_y = yPoints;

	_slopes.clear();

	_modalDistortionAngles.clear();

	_maxModalDistortionAngle			= 0;
	_maxModalDistortionAngleXPosition	= 0;

	_modeBDCAreaAnalysisRun = false;
	_modeBDCAreas.clear();

	_crosses_front_sections = crosses_front_sections;


	CString log_msg("ModeBoundaryLine::ModeBoundaryLine() : ");
	log_msg.AppendFormat("created _x.size()=%d, _y.size()=%d, _crosses_front_sections = %d",
		_x.size(), _y.size(), (int)_crosses_front_sections);
	CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)


	CGR_LOG("ModeBoundaryLine::ModeBoundaryLine() exiting",DIAGNOSTIC_CGR_LOG)
}

ModeBoundaryLine::
ModeBoundaryLine()
{
}

ModeBoundaryLine::
~ModeBoundaryLine(void)
{
}

/////////////////////////////////////////////////////////////////////

int
ModeBoundaryLine::
numXPoints()
{
	return (int)(_x.size());
}

int
ModeBoundaryLine::
numYPoints()
{
	return (int)(_y.size());
}

double	
ModeBoundaryLine::
x(int index)
{
	if( index >= 0 
	 && index < (int)_x.size())
		return _x[index];
	else
		return 0;
}

double
ModeBoundaryLine::
y(int index)
{
	if( index >= 0 
	 && index < (int)_y.size())
		return _y[index];
	else
		return 0;
}

std::vector<double>
ModeBoundaryLine::
xPoints()
{
	return _x;
}

std::vector<double>
ModeBoundaryLine::
yPoints()
{
	return _y;
}

std::vector<double>
ModeBoundaryLine::
getSlopes(int slopeWindow)
{
	if(slopeWindow<=0)
		slopeWindow = 1;

	if(_slopes.size()==0)
	{
		// Iterate through each pair of points on this line
		// starting with the first two points and calculate
		// slope IN DEGREES.
		//
		// Originally the actual slopes where calculated but we
		// run into difficulties where the angle becomes greater
		// than 90 degrees in relation to the horizon. This can
		// occur where a line folds back on itself. Slopes are 
		// calculated by dividing deltaX into deltaY but these
		// will tend towards infinity when the angle becomes greater
		// than 90 degrees. Therefore angles are used instead.
		
		// check we actually have x and y
		// values in this line
		if((numXPoints()>0)&&
			(numYPoints()>0))
		{
			double x1		= 0;		
			double x2		= 0;
			double y1		= 0;
			double y2		= 0;

			for(int i=0; i<numXPoints();i++)
			{
				// don't want to access
				// the vector with an invalid
				// index so check that this 
				// point pair is valid i.e.
				// we have a y for our x, and
				// that the next point is valid
				// as well
				double slopeInDegrees = 0;

                if(((i+slopeWindow) < (int)_x.size()))
				{
					// slope = y2-y1/x2-x1
                    //int y_coord
                    //    = (int)((_x[i])/_xAxisLength);
                    //int x_coord
                    //    = (_x[i]) - y_coord*_xAxisLength;

                    x1 = (double)_x[i];
					y1 = (double)_y[i];

                    //y_coord
                    //    = (int)((_x[i+slopeWindow])/_xAxisLength);
                    //x_coord
                    //    = (_x[i+slopeWindow]) - y_coord*_xAxisLength;

                    x2 = (double)_x[i+slopeWindow];
					y2 = (double)_y[i+slopeWindow];

					slopeInDegrees = getSlopeInDegrees(x1, y1, x2, y2);

					_slopes.push_back( slopeInDegrees );

				}
			}
		}
	}

	return _slopes;
}

double
ModeBoundaryLine::
getSlopeInDegrees(double x1,
				double y1,
				double x2,
				double y2)
{
	double slopeInDegrees = 0;

	double slope		= 0;
	double atanOfSlope	= 0;
	double piOver180	= 180/M_PI;

    if( x2 == x1 )
    { // avoid division by zero
        if( y2 == y1 ) slopeInDegrees = 0; // points are the same
        else if( y2 < y1 ) slopeInDegrees = -90;
        else slopeInDegrees = 90;
    }
    else if( x2 > x1 )
    {
        if( y2 == y1 ) slopeInDegrees = 0;
        else if( y2 > y1 )
        {
			slope = (y2-y1)/(x2-x1);
			atanOfSlope = atan(slope);
			slopeInDegrees = atanOfSlope * piOver180;
        }
        else // y1 > y2
        {
			slope = (y1-y2)/(x2-x1);
			atanOfSlope = atan(slope);
			slopeInDegrees = - atanOfSlope * piOver180;
        }
    }
    else // x2 < x1 
    {
        if( y2 == y1 ) slopeInDegrees = 180;
        else if( y2 > y1 )
        {
			slope = (y2-y1)/(x1-x2);
			atanOfSlope = atan(slope);
			slopeInDegrees = 180 - atanOfSlope * piOver180;
        }
        else // y1 > y2
        {
			slope = (y1-y2)/(x1-x2);
			atanOfSlope = atan(slope);
			slopeInDegrees = - (180 - atanOfSlope * piOver180);
        }
    }

	return slopeInDegrees;
}

void
ModeBoundaryLine::
runModalDistortionAnalysis(double modalDistortionMinX,
							double modalDistortionMaxX)
{
	// error check
	if(modalDistortionMinX < modalDistortionMaxX)
	{
		double firstX = x(0);
		double firstY = y(0);
		double lastX = x(numXPoints()-1);
		double lastY = y(numYPoints()-1);

		double slopeA = 0;
		double slopeB = 0;

		double modalDistortionAngle = 0;

		_maxModalDistortionAngle = 0;
		_maxModalDistortionAngleXPosition = 0;

		double maxXValue = firstX;

		for(int i=1; i<numXPoints(); i++)
		{
			if(x(i) > maxXValue) // x must be increasing
			{
				maxXValue = x(i);

				if((x(i) >= modalDistortionMinX) && (x(i) <= modalDistortionMaxX))
				{
					slopeA = getSlopeInDegrees(firstX, firstY, x(i), y(i));
					slopeB = getSlopeInDegrees(x(i), y(i), lastX, lastY);

					modalDistortionAngle = slopeA - slopeB;

					if((modalDistortionAngle >= 0)&&(modalDistortionAngle <= 90))
					{
						_modalDistortionAngles.push_back(modalDistortionAngle);

						if(modalDistortionAngle > _maxModalDistortionAngle)
						{
							_maxModalDistortionAngle = modalDistortionAngle;
							_maxModalDistortionAngleXPosition = x(i);
						}
					}
				}
			}
		}
	}
}


double
ModeBoundaryLine::
getMaxModalDistortionAngle()
{
	return _maxModalDistortionAngle;
}

double
ModeBoundaryLine::
getMaxModalDistortionAngleXPosition()
{
	return _maxModalDistortionAngleXPosition;
}

//
///
///////////////////////////////////////////////////////////////////////////////////
///
//

void
ModeBoundaryLine::
runModeBDCAreaAnalysis()
{
	if(_modeBDCAreas.size()==0)
	{
		_maxModeBDCAreaXLength = 0;
		_maxModeBDCArea = 0;
		_sumModeBDCAreas = 0;
		_numModeBDCAreas = 0;

		int maxX = 0;
		int yAtMaxX = 0;
		bool inBDCArea = false;
		std::vector<int> bdcXPoints;
		std::vector<int> bdcYPoints;

		for( int i=0; i<numXPoints(); i++ )
		{
			if(x(i)>maxX)
			{
				maxX = (int)x(i);
				yAtMaxX = (int)y(i);

				if(inBDCArea)
				{ // found end of bdc area
					inBDCArea = false;

					if(bdcYPoints.size() > 1)
					{
						// calculate area of bdc
						std::vector<int>::iterator i_zx = bdcXPoints.begin();
						std::vector<int>::iterator i_zy = bdcYPoints.begin();
						std::vector<int> x_bdc_lengths;
						x_bdc_lengths.clear();
						for(int j = 0; j <= yAtMaxX-(*(bdcYPoints.begin())); j++)
							x_bdc_lengths.push_back(0);
						while( i_zx != bdcXPoints.end()
							&& i_zy != bdcYPoints.end())
						{ // find longest x-length for each y in bdc
							int y_pos = (*i_zy)-(*(bdcYPoints.begin()));
							int x_bdc_length = (*(bdcXPoints.begin()))-(*i_zx)+1;
							if( y_pos>=0 && y_pos<(int)(x_bdc_lengths.size()) )
							{
								if( x_bdc_length > x_bdc_lengths[y_pos] )
								{
									x_bdc_lengths[y_pos] = x_bdc_length;
								}
							}
							i_zx++;
							i_zy++;
						}

						// calculate area
						double area = 0;
						for(int j = 0; j < (int)(x_bdc_lengths.size()); j++)
							area += (double)(x_bdc_lengths[j]);

						if(area>_maxModeBDCArea) 
							_maxModeBDCArea = area;

						_sumModeBDCAreas += area;

						_modeBDCAreas.push_back(area);
						_modeBDCAreaXLengths.push_back(_maxModeBDCAreaXLength);
					}
				}
			}

			if(x(i)<maxX && !inBDCArea)
			{ // found the start of a new bdc
				inBDCArea = true;
				_numModeBDCAreas += 1;
				bdcXPoints.clear();
				bdcYPoints.clear();
				bdcXPoints.push_back(maxX);
				bdcYPoints.push_back(yAtMaxX);
			}

			if(inBDCArea)
			{ // add point to collection of bdc points
				bdcXPoints.push_back((int)x(i));
				bdcYPoints.push_back((int)y(i));
			}

			if((maxX-x(i))>_maxModeBDCAreaXLength)
				_maxModeBDCAreaXLength = maxX - x(i);
		}

	}

	_modeBDCAreaAnalysisRun = true;
}

/////////////////////////////////////////////////////////////////////////////////

Vector
ModeBoundaryLine::
getModeBDCAreas()
{
	if(!_modeBDCAreaAnalysisRun)
		runModeBDCAreaAnalysis();

	return _modeBDCAreas;
}
double
ModeBoundaryLine::
getMaxModeBDCArea()
{
	if(!_modeBDCAreaAnalysisRun)
		runModeBDCAreaAnalysis();

	return _maxModeBDCArea;
}
double
ModeBoundaryLine::
getMinModeBDCArea()
{
	if(!_modeBDCAreaAnalysisRun)
		runModeBDCAreaAnalysis();

	return _modeBDCAreas.getVectorAttribute(Vector::min);
}
double
ModeBoundaryLine::
getMeanModeBDCAreas()
{
	if(!_modeBDCAreaAnalysisRun)
		runModeBDCAreaAnalysis();

	return _modeBDCAreas.getVectorAttribute(Vector::mean);
}
double
ModeBoundaryLine::
getSumModeBDCAreas()
{
	if(!_modeBDCAreaAnalysisRun)
		runModeBDCAreaAnalysis();

	return _sumModeBDCAreas;
}
double
ModeBoundaryLine::
getNumModeBDCAreas()
{
	if(!_modeBDCAreaAnalysisRun)
		runModeBDCAreaAnalysis();

	return _numModeBDCAreas;
}
double
ModeBoundaryLine::
getStdDevModeBDCAreas()
{
	if(!_modeBDCAreaAnalysisRun)
		runModeBDCAreaAnalysis();

	return _modeBDCAreas.getVectorAttribute(Vector::stdDev);
}
double
ModeBoundaryLine::
getVarModeBDCAreas()
{
	if(!_modeBDCAreaAnalysisRun)
		runModeBDCAreaAnalysis();

	return _modeBDCAreas.getVectorAttribute(Vector::variance);
}

//
///////////////////////////////////////////////////////////////////////////
//

Vector
ModeBoundaryLine::
getModeBDCAreaXLengths()
{
	if(!_modeBDCAreaAnalysisRun)
		runModeBDCAreaAnalysis();

	return _modeBDCAreaXLengths;
}
double
ModeBoundaryLine::
getMaxModeBDCAreaXLength()
{
	if(!_modeBDCAreaAnalysisRun)
		runModeBDCAreaAnalysis();

	return _maxModeBDCAreaXLength;
}
double
ModeBoundaryLine::
getMinModeBDCAreaXLength()
{
	if(!_modeBDCAreaAnalysisRun)
		runModeBDCAreaAnalysis();

	return _modeBDCAreaXLengths.getVectorAttribute(Vector::min);
}
double
ModeBoundaryLine::
getMeanModeBDCAreaXLengths()
{
	if(!_modeBDCAreaAnalysisRun)
		runModeBDCAreaAnalysis();

	return _modeBDCAreaXLengths.getVectorAttribute(Vector::mean);
}
double
ModeBoundaryLine::
getSumModeBDCAreaXLengths()
{
	if(!_modeBDCAreaAnalysisRun)
		runModeBDCAreaAnalysis();

	return _sumModeBDCAreaXLengths;
}
double
ModeBoundaryLine::
getNumModeBDCAreaXLengths()
{
	if(!_modeBDCAreaAnalysisRun)
		runModeBDCAreaAnalysis();

	return _numModeBDCAreaXLengths;
}
double
ModeBoundaryLine::
getStdDevModeBDCAreaXLengths()
{
	if(!_modeBDCAreaAnalysisRun)
		runModeBDCAreaAnalysis();

	return _modeBDCAreaXLengths.getVectorAttribute(Vector::stdDev);
}
double
ModeBoundaryLine::
getVarModeBDCAreaXLengths()
{
	if(!_modeBDCAreaAnalysisRun)
		runModeBDCAreaAnalysis();

	return _modeBDCAreaXLengths.getVectorAttribute(Vector::variance);
}
/////////////////////////////////////////////////////////////////////
///
//	Get and set functions
//

//
//
///	End of ModeBoundaryLine.cpp
///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2002 Tsunami Photonics Ltd. All Rights Reserved
//
// FileName    : QAThresholds.h
// Description : Declaration of Vector class
//
// Rev#  | Date          | Author               | Description of change
// --------------------------------------------------------------
// 0.1   | 02 Oct 2002   | Frank D'Arcy         | Initial Draft
//

#ifndef __PASSFAILTHRESHOLDS__H__
#define __PASSFAILTHRESHOLDS__H__

class PassFailThresholds 
{
public:
	PassFailThresholds();
    ~PassFailThresholds( );

	////////////////////////////////////

	int		_maxModeBordersRemoved;
	int		_maxModeWidth;
	int		_minModeWidth;
	double	_maxModeBDCArea;
	double	_maxModeBDCAreaXLength;
	double	_sumModeBDCAreas;
	double	_numModeBDCAreas;
	double	_modalDistortionAngle;
	double	_modalDistortionAngleXPosition;
	double	_meanPercWorkingRegion;
	double	_minPercWorkingRegion;
	double	_middleLineRMSValue;
	double	_maxMiddleLineSlope;
	double	_minMiddleLineSlope;
	double	_maxPrGap;
	double	_maxLowerPr;
	double	_minUpperPr;
	double	_maxModeLineSlope;
	double	_minModeLineSlope;


	////////////////////////////////////

private:
	void	setThresholdsToDefault();
};

#endif	// PASSFAILTHRESHOLDS__H__

#ifndef _NR_SVDFIT_H_
#define _NR_SVDFIT_H_

void svdpolyfit(double x[], double y[], int ndata, double a[], int ma, double *chisq );

void svdfit(double x[], double y[], double sig[], int ndata, double a[], int ma,
	double **u, double **v, double w[], double *chisq,
	void (*funcs)(double, double [], int));

void fpoly(double x, double p[], int np);

void svbksb(double **u, double w[], double **v, int m, int n, double b[],double x[]);

void svdcmp(double **a, int m, int n, double w[], double **v);

double pythag(double a, double b);

void svdvar(double **v, int ma, double w[], double **cvm);

void testlib(double *test_value);

#endif /* _NR_SVDFIT_H_ */


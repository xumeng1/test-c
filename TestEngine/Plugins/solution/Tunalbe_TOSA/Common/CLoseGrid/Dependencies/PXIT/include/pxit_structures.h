#if defined(__cplusplus) || defined(__cplusplus__)
   extern "C" {
#endif

// version 1.0 release
// version 1.1 change for 108 cards, bar + offset introduced
// version 3.0 change in pxitcard.ini to allow for buffer, relay and matrix
// information such as BAR number, address offset, inversion
// pxit.dll, pxitrelay.dll, mxitmatrix.dll all upgraded to 3.0 for
// this modification
// also comments are now allowed, and information is read as a line allowing
// modifications to pxitcard.ini to be simpler
// version 3.11 matrix structure was defined as an array of 15 not 16 numbers, dr kuhn

typedef struct
{
	void		*next;
	ViUInt8		PXISlotNo;
	ViUInt8		PCIDevNo;
} Device_Map;

typedef struct
{
	void		*next;
	ViUInt8		PXITInstNo;	// PXIT assigned number
	ViUInt8		PCIDevNo;
} Instrument_Map;

// version 3.0
typedef struct
{
	void		*next;
	ViUInt16	cardtype;

	// relay fuctions
	// relays are always assumed to have a relaymaxoffset block starting
	// at location relayoffset on BAR relayBAR
	// relaymaxrelay is used in the selftest as each driver latch is not
	// guarenteed to contain the full 8 relays
	ViUInt8		relaycard;		// is card a relay card?
	ViUInt8		relaymaxoffset;	// how many CS lines?
	ViUInt8		relaymaxrelay;	// how many relays?
	ViUInt8		relayinverted;	// output inverted?
	ViUInt8		relaybar;		// which BAR are addresses mapped to?
	ViUInt8		relayoffset;	// where is the initial address offset?

	// matrix functions
	// matrix is always assumed to have an 0x80 byte block starting
	// at location matrixoffset on BAR matrixBAR
	ViUInt8		matrixcard;		// does card contain a 16-8 matrix?
	ViUInt8		matrixbar;		// which BAR are addresses mapped to?
	ViUInt8		matrixoffset;	// where is the initial address offset?

	// buffer IO functions
	// buffer is always assumed to have two latches, one controlling
	// the input, one controlling the output. if there is only one
	// latch then either the input or output will be inverted
	ViUInt8		buffercard;		// is card a trigger buffer card?
	ViUInt8		bufferbar;		// which BAR are addresses mapped to?
	ViUInt8		bufferIoffset;	// where is the input address offset?
	ViUInt8		bufferOoffset;	// where is the output address offset?
	ViUInt8		bufferIinverted;// output inverted?
	ViUInt8		bufferOinverted;// output inverted?


} Pxit_Map;

// version 3.1
typedef struct
{
	void		*next;
	ViUInt16	cardtype;

	ViUInt8		PXIBus[16];	// 0 - 15 modified 04 sep 2001

} Matrix_Map;

#define DllExport    __declspec( dllexport )

#if defined(__cplusplus) || defined(__cplusplus__)
   }
#endif
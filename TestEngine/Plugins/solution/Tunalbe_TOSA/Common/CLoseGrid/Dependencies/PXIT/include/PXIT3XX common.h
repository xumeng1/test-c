typedef union
{
	ViReal64	real64[2];
	ViReal32	real32[4];
	ViUInt32	int32[4];
	ViUInt16	int16[8];
	ViUInt8		int8[16];
	ViChar		chr[16];
} PXIT128;

typedef struct sharedMemMessagetag
{
	char	message[64];
} sharedMemMessage;

typedef struct PXITDebugMsg64tag
{
	ViUInt32		size;
	ViUInt32		inptr;	// position of next entry to write
    ViUInt32		clientsConnected;
    ViUInt32		serversConnected;
    sharedMemMessage	message[0x100];	// allocate 16K for 256 messages
    // Message for Client
} PXITDebugMsg64;

typedef struct PXITDebugReal64tag
{
	ViUInt32		size;
	ViUInt32		inptr;	// position of next entry to write
    ViUInt32		clientsConnected;
    ViUInt32		serversConnected;
    ViReal64	realval[0x2000];	// allocate 64K for 8K real64 values
    // Message for Client
} PXITDebugReal64;

typedef struct PXITDebugReal32tag
{
	ViUInt32		size;
	ViUInt32		inptr;	// position of next entry to write
    ViUInt32		clientsConnected;
    ViUInt32		serversConnected;
    ViReal32	realval[0x2000];	// allocate 32K for 8K real64 values
    // Message for Client
} PXITDebugReal32;

typedef struct PXITDebugInt32tag
{
	ViUInt32		size;
	ViUInt32		inptr;	// position of next entry to write
    ViUInt32		clientsConnected;
    ViUInt32		serversConnected;
    ViUInt32		intval[0x2000];	// allocate 32K for 8K int32 values
    // Message for Client
} PXITDebugInt32;

typedef struct PXITDebugInt16tag
{
	ViUInt32		size;
	ViUInt32		inptr;	// position of next entry to write
    ViUInt32		clientsConnected;
    ViUInt32		serversConnected;
    ViUInt16		intval[0x2000];	// allocate 32K for 8K int32 values
    // Message for Client
} PXITDebugInt16;

typedef struct PXITDebugInt8tag
{
	ViUInt32		size;
	ViUInt32		inptr;	// position of next entry to write
    ViUInt32		clientsConnected;
    ViUInt32		serversConnected;
    ViUInt8		intval[0x2000];	// allocate 32K for 8K int32 values
    // Message for Client
} PXITDebugInt8;

#if defined(__cplusplus) || defined(__cplusplus__)
extern "C" {
#endif
	   
ViReal64 _VI_FUNC PXITXEepromVersion(void);

// eeprom functions
ViInt16 _VI_FUNC PXITX9050eepromEWDS(ViSession instr);
ViInt16 _VI_FUNC PXITX9050eepromEWEN(ViSession instr);
ViInt16 _VI_FUNC PXITX9050eepromERAL(ViSession instr);
ViInt16 _VI_FUNC PXITX9050eepromERASE(ViSession instr, ViInt16 address);
ViInt16 _VI_FUNC PXITX9050eepromREAD(ViSession instr, ViInt16 address, ViUInt16 *data);
ViInt16 _VI_FUNC PXITX9050eepromWRITE(ViSession instr, ViInt16 address, ViUInt16 data);
ViInt16 _VI_FUNC PXITX9050eepromWRAL(ViSession instr, ViInt32 data);

// eeprom update functions
ViInt16 _VI_FUNC PXITXeepromsetvendorID(ViSession instr, ViInt16 ID);
ViInt16 _VI_FUNC PXITXeepromsetvendorsubID(ViSession instr, ViInt16 ID);
ViInt16 _VI_FUNC PXITXeepromsetdeviceID(ViSession instr, ViInt16 ID);
ViInt16 _VI_FUNC PXITXeepromsetdevicesubID(ViSession instr, ViInt16 ID);

ViInt16 _VI_FUNC PXITXeepromsetmemory(ViSession instr, ViInt16 LocalBusSpace, ViUInt32 range,
												ViUInt32 remap, ViUInt32 descriptor, ViUInt32 csbase);
ViInt16 _VI_FUNC PXITXeepromsetcontrolmisc(ViSession instr, ViUInt32 controlmisc);

// put/get 16 bit eeprom word functions, primarily used to access the 14 unused eeprom locations in the 9050
ViInt16 _VI_FUNC PXITXeepromputword(ViSession instr, ViInt16 offset, ViUInt16 value);
ViInt16 _VI_FUNC PXITXeepromgetword(ViSession instr, ViInt16 offset, ViUInt16 *value);

 ViInt16 _VI_FUNC PXITXeepromverify(ViSession instr, char filename[]);
 ViInt16 _VI_FUNC PXITXeepromprogram(ViSession instr, char filename[]);
 ViInt16 _VI_FUNC PXITXeepromretrieve(ViSession instr, char filename[]);

#if defined(__cplusplus) || defined(__cplusplus__)
}
#endif
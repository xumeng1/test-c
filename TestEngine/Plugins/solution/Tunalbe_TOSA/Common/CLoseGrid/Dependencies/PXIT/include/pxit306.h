#if defined(__cplusplus) || defined(__cplusplus__)
extern "C" {
#endif
	   
#if defined (EXPORT306DLL) 
	#define DLL306  __declspec( dllexport  )
#else
	#define DLL306
#endif


// external dll functions
DLL306	ViReal64 _VI_FUNC PXIT306Version(void);

DLL306	ViInt16	_VI_FUNC PXIT306SetTriggerSetup(ViSession instr, ViUInt8	config);
DLL306	ViInt16	_VI_FUNC PXIT306SetMasterMode(ViSession instr, ViUInt8	config);
DLL306	ViInt16	_VI_FUNC PXIT306SetHWTriggerMode(ViSession instr, ViUInt8	config);

DLL306	ViInt16	_VI_FUNC PXIT306ReadElectricalCurrent(ViSession instr, ViUInt8* rangeA, ViUInt8* rangeB, ViReal64* iA, ViReal64* iB);
DLL306	ViInt16	_VI_FUNC PXIT306ReadOpticalPower(ViSession instr, ViUInt8* rangeA, ViUInt8* rangeB, ViReal32 wavelengthA, ViReal32 wavelengthB, ViReal64* pA, ViReal64* pB);

DLL306	ViInt16	_VI_FUNC PXIT306ReadTemperature(ViSession instr, ViReal32	*temp);

// initialise card to default state and use calibration parameters held in eeprom where available
DLL306	ViInt16	_VI_FUNC PXIT306InitialiseModule(ViSession instr);
DLL306	ViInt16	_VI_FUNC PXIT306UnInitialiseModule(ViSession instr);

DLL306	ViInt16	_VI_FUNC PXIT306SequenceWriteBlockHeader(ViSession instr, ViUInt16 count, ViUInt8 sourcedelay, ViUInt8 measuredelay, ViUInt8 range);
DLL306	ViInt16	_VI_FUNC PXIT306SequenceReadBlockHeader(ViSession instr, ViUInt16 *count, ViUInt8 *sourcedelay, ViUInt8 *measuredelay, ViUInt16 *controlword);
DLL306	ViInt16	_VI_FUNC PXIT306SequenceReadBlockData(ViSession instr, ViUInt16 count, PXIT3XXSequenceData *data, ViUInt8 format, ViUInt8 range);
DLL306	ViInt16	_VI_FUNC PXIT306SequenceWriteBlockData(ViSession instr, ViUInt16 count, PXIT3XXSequenceData *data, ViUInt8 format, ViUInt8 range);
DLL306	ViInt16	_VI_FUNC PXIT306SequenceReadBlockDataP(ViSession instr, ViUInt16 count, PXIT3XXSequenceData *data, ViUInt8 format, ViUInt8 range, ViReal32 wavelengthA, ViReal32 wavelengthB);
DLL306	ViInt16	_VI_FUNC PXIT306SetLocalRelay(ViSession instr, ViUInt8 value);
DLL306	ViInt16	_VI_FUNC PXIT306SequenceStatus(ViSession instr, ViUInt8 *value);	// return sequence status for board
DLL306	ViInt16	_VI_FUNC PXIT306SetMeasureAverage(ViUInt8 value);
DLL306	ViInt16	_VI_FUNC PXIT306SetBiasVoltages(ViSession instr, ViReal64	valueA, ViReal64 valueB);
DLL306	ViInt16	_VI_FUNC PXIT306SetAutoRangeLimit(ViSession instr, ViUInt8 ch1, ViUInt8 ch2);

DLL306	ViInt16	_VI_FUNC PXIT306SetUserSensorTemperature(ViSession instr, ViReal32 channel1, ViReal32 channel2);
DLL306	ViInt16	_VI_FUNC PXIT306GetUserSensorTemperature(ViSession instr, ViReal32* channel1, ViReal32* channel2);






DLL306	ViInt16	_VI_FUNC PXIT306MeasurePowerdBmWithFeedback(ViSession instr, ViReal32 wavelength, ViReal32 wavelength2, ViReal64 pA, ViReal64 pB, ViUInt8	delay, ViUInt8 channelflag);
DLL306	ViInt16	_VI_FUNC PXIT306MeasurePoweruWWithFeedback(ViSession instr, ViReal32 wavelength, ViReal32 wavelength2, ViReal64 pA, ViReal64 pB, ViUInt8	delay, ViUInt8 channelflag);

DLL306	ViInt16	_VI_FUNC PXIT306SequenceReadBlockDataVB(ViSession instr, ViUInt16 count, ViReal64* measure1, ViReal64* measure2, ViUInt8 format, ViUInt8 range);
DLL306	ViInt16	_VI_FUNC PXIT306SequenceReadBlockDataPVB(ViSession instr, ViUInt16 count, ViReal64* measure1, ViReal64* measure2, ViUInt8 format, ViUInt8 range, ViReal32 wavelengthA, ViReal32 wavelengthB);
DLL306	ViInt16	_VI_FUNC PXIT306SequenceWriteBlockDataVB(ViSession instr, ViUInt16 count, ViReal64* measure1, ViReal64* measure2, ViUInt8 format, ViUInt8 range);

DLL306	ViStatus	_VI_FUNC PXIT306SequenceReadBlockVB(ViSession instr, ViUInt16 *count, ViReal64* measure1, ViReal64* measure2, ViUInt8 format, ViUInt8 *range, ViUInt8 *sourcedelay, ViUInt8 *measuredelay);
DLL306	ViStatus	_VI_FUNC PXIT306SequenceWriteBlockVB(ViSession instr, ViUInt16 count, ViReal64* measure1, ViReal64* measure2, ViUInt8 format, ViUInt8 range, ViUInt8 sourcedelay, ViUInt8 measuredelay);

DLL306 ViInt16	_VI_FUNC PXIT306SequenceIsolate(ViSession instr);

#if defined(__cplusplus) || defined(__cplusplus__)
}
#endif

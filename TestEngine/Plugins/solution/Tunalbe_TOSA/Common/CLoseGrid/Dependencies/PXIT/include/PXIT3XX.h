#if defined(__cplusplus) || defined(__cplusplus__)
extern "C" {
#endif

#if defined (EXPORT3XXDLL) 
	#define DLL3XX  __declspec( dllexport )
#else
	#define DLL3XX
#endif

#include "pxit3XX curve.h"
#include "pxit3xx types.h"
#include "pxit3xx common.h"

DLL3XX ViReal64 _VI_FUNC PXIT3XXSequenceVersion(void);
DLL3XX ViReal64 _VI_FUNC PXIT3XXVersion(void);

// PIC functions
DLL3XX ViInt16 _VI_FUNC PXIT3XXRDCodeVersion(ViSession instr);
DLL3XX ViInt16 _VI_FUNC PXIT3XXResetPic(ViSession instr);

// Sequence functions 
DLL3XX ViInt16	_VI_FUNC PXIT3XXSequenceRunSlave(ViSession instr);
DLL3XX ViInt16	_VI_FUNC PXIT3XXSequenceRunMaster(ViSession instr);
DLL3XX void	_VI_FUNC PXIT3XXSetSequenceRunLoopMultiplier(ViUInt8 count);
DLL3XX	ViInt16	_VI_FUNC PXIT3XXSequenceReset(ViSession instr);

// obsolete sequence functions - use module functions where availble
DLL3XX	ViInt16	_VI_FUNC PXIT3XXSequenceWriteHeaderBlock(ViSession instr, ViUInt32 count, ViUInt8 format);
DLL3XX	ViInt16	_VI_FUNC PXIT3XXSequenceReadHeaderBlock(ViSession instr, ViUInt32 *count, ViUInt8 *format);
DLL3XX	ViInt16	_VI_FUNC PXIT3XXSequenceWriteBlock(ViSession instr, ViUInt16 count, ViUInt8 sourcedelay, ViUInt8 measuredelay, ViUInt16 aquisition, PXIT3XXSequenceData *data, ViUInt8 format, ViUInt8 datatype);
DLL3XX	ViInt16	_VI_FUNC PXIT3XXSequenceReadBlock(ViSession instr, ViUInt16 *count, ViUInt8 *sourcedelay, ViUInt8 *measuredelay, ViUInt16 *aquisition, PXIT3XXSequenceData *data, ViUInt8 format, ViUInt8 datatype);
DLL3XX	ViInt16	_VI_FUNC PXIT3XXSequenceWriteBlockHeader(ViSession instr, ViUInt16 count, ViUInt8 sourcedelay, ViUInt8 measuredelay, ViUInt16 controlword);
DLL3XX	ViInt16	_VI_FUNC PXIT3XXSequenceReadBlockHeader(ViSession instr, ViUInt16 *count, ViUInt8 *sourcedelay, ViUInt8 *measuredelay, ViUInt16 *controlword);
DLL3XX	ViInt16	_VI_FUNC PXIT3XXSequenceReadBlockData(ViSession instr, ViUInt16 count, PXIT3XXSequenceData *data, ViUInt8 format, ViUInt8 datatype);
DLL3XX	ViInt16	_VI_FUNC PXIT3XXSequenceWriteBlockData(ViSession instr, ViUInt16 count, PXIT3XXSequenceData *data, ViUInt8 format, ViUInt8 datatype);

// EEPROM access functions
DLL3XX ViInt16 _VI_FUNC PXIT3XXeepromWRITE(ViSession instr, ViInt16	number, ViUInt16 address, ViUInt8 dataout);
DLL3XX ViInt16 _VI_FUNC PXIT3XXeepromREAD(ViSession instr, ViInt16	number, ViUInt16 address, ViUInt8* datain);

DLL3XX ViInt16 _VI_FUNC PXIT3XXeepromGetReal64(ViSession instr, ViInt16	number, ViUInt16 address, ViReal64* dataout);
DLL3XX ViInt16 _VI_FUNC PXIT3XXeepromGetReal32(ViSession instr, ViInt16	number, ViUInt16 address, ViReal32* dataout);
DLL3XX ViInt16 _VI_FUNC PXIT3XXeepromGetUInt32(ViSession instr, ViInt16	number, ViUInt16 address, ViUInt32* dataout);
DLL3XX ViInt16 _VI_FUNC PXIT3XXeepromGetUInt16(ViSession instr, ViInt16	number, ViUInt16 address, ViUInt16* dataout);
DLL3XX ViInt16 _VI_FUNC PXIT3XXeepromGetUInt8(ViSession instr, ViInt16	number, ViUInt16 address, ViUInt8* dataout);
DLL3XX ViInt16 _VI_FUNC PXIT3XXeepromGetChars(ViSession instr, ViInt16	number, ViUInt16 address, ViChar* chars, ViUInt16 length);

DLL3XX ViInt16 _VI_FUNC PXIT3XXeepromPutReal64(ViSession instr, ViInt16	number, ViUInt16 address, ViReal64 dataout);
DLL3XX ViInt16 _VI_FUNC PXIT3XXeepromPutReal32(ViSession instr, ViInt16	number, ViUInt16 address, ViReal32 dataout);
DLL3XX ViInt16 _VI_FUNC PXIT3XXeepromPutUInt32(ViSession instr, ViInt16	number, ViUInt16 address, ViUInt32 dataout);
DLL3XX ViInt16 _VI_FUNC PXIT3XXeepromPutUInt16(ViSession instr, ViInt16	number, ViUInt16 address, ViUInt16 dataout);
DLL3XX ViInt16 _VI_FUNC PXIT3XXeepromPutUInt8(ViSession instr, ViInt16	number, ViUInt16 address, ViUInt8 dataout);
DLL3XX ViInt16 _VI_FUNC PXIT3XXeepromPutChars(ViSession instr, ViInt16	number, ViUInt16 address, ViChar* chars, ViUInt16 length);

DLL3XX ViInt16 _VI_FUNC PXIT3XXPutModuleName(ViSession instr, ViChar* chars);
DLL3XX ViInt16 _VI_FUNC PXIT3XXGetModuleName(ViSession instr, ViChar* chars);

DLL3XX ViInt16 _VI_FUNC PXIT3XXGetModuleSerialNumber(ViSession instr, ViChar* chars);

DLL3XX ViInt16 _VI_FUNC PXIT3XXGetPXITVariable(ViUInt8 number, PXIT128* value);
DLL3XX ViInt16 _VI_FUNC PXIT3XXPutPXITVariable(ViUInt8 number, PXIT128* value);

DLL3XX ViInt16 _VI_FUNC PXIT3XXGetPXITSharedVariable(ViUInt8 number, PXIT128* value);
DLL3XX ViInt16 _VI_FUNC PXIT3XXPutPXITSharedVariable(ViUInt8 number, PXIT128* value);

DLL3XX ViInt16 _VI_FUNC PXIT3XXConfigurationGetReal32(ViSession instr, ViUInt16	number, ViReal32* dataout);
DLL3XX ViInt16 _VI_FUNC PXIT3XXConfigurationGetUInt32(ViSession instr, ViUInt16	number, ViUInt32* dataout);
DLL3XX ViInt16 _VI_FUNC PXIT3XXConfigurationPutReal32(ViSession instr, ViUInt16	number, ViReal32 dataout);
DLL3XX ViInt16 _VI_FUNC PXIT3XXConfigurationPutUInt32(ViSession instr, ViUInt16	number, ViUInt32 dataout);

// labview get around to re-refernece an external variable
DLL3XX void _VI_FUNC PXIT3XXNOP(ViReal64* dummy);

DLL3XX	ViStatus	_stdcall	addressofViReal64(ViReal64 *variable);
DLL3XX	ViStatus	_stdcall	addressofViReal32(ViReal32 *variable);
DLL3XX	ViStatus	_stdcall	addressofViInt32(ViInt32 *variable);
DLL3XX	ViStatus	_stdcall	addressofViInt16(ViInt16 *variable);
DLL3XX	ViStatus	_stdcall	addressofViInt8(ViInt8 *variable);

DLL3XX ViStatus _VI_FUNC PXIT3XXWriteDebugMessage(ViChar* msg);
DLL3XX ViStatus _VI_FUNC PXIT3XXWriteDebugReal64(ViReal64 value);
DLL3XX ViStatus _VI_FUNC PXIT3XXWriteDebugInt32(ViInt32 value);

DLL3XX ViStatus _VI_FUNC PXIT3XXWriteLogMessage(ViChar* msg);

DLL3XX void _VI_FUNC PXIT3XXEnableMsgLogging(void);
DLL3XX void _VI_FUNC PXIT3XXDisableMsgLogging(void);

DLL3XX void _VI_FUNC PXIT3XXSetDbgMsgLoggingLevel(ViInt16	level);

#if defined(__cplusplus) || defined(__cplusplus__)
}
#endif


#if defined(__cplusplus) || defined(__cplusplus__)
extern "C" {
#endif
	   
ViReal64 _VI_FUNC PXITXVersion(void);

void _VI_FUNC PXITX9050SetPLXConfigBar(ViInt16 Bar);

// bus descriptor modification
ViInt16 _VI_FUNC PXITXSet9050BusTiming(ViSession instr, ViInt16 LocalBusSpace, ViInt16 NRAD, ViInt16 NRDD, ViInt16 NXDA,
												ViInt16 NWAD, ViInt16 NWDD, ViInt16 RdStbDelay, ViInt16 WrStbDelay,
												ViInt16 WrCycleHold);
ViInt16 _VI_FUNC PXITXSet9050BusWidth(ViSession instr, ViInt16 LocalBusSpace, ViInt16 width);


// reset function
// pulse the reset line
ViInt16 _VI_FUNC PXITXReset9050(ViSession instr);

ViInt16 _VI_FUNC PXITX9050getRESET(ViSession instr);
ViInt16 _VI_FUNC PXITX9050putRESET(ViSession instr, ViInt16 value);

// reload PCI config reg
ViInt16 _VI_FUNC PXITX9050eepromReload(ViSession instr);

// user port control
ViInt16 _VI_FUNC PXITX9050configuserport(ViSession instr, ViUInt32 user);	// config for function or user I/O
ViInt16 _VI_FUNC PXITX9050writeuserport(ViSession instr, ViUInt32 user);	// write to port
ViInt16 _VI_FUNC PXITX9050controluserport(ViSession instr, ViUInt32 user);	// config for direction
ViInt16 _VI_FUNC PXITX9050readuserport(ViSession instr);					// read from port

// control register

DllExport ViInt32 _VI_FUNC PXITX9050getCONTROLREG(ViSession instr);
DllExport ViInt16 _VI_FUNC PXITX9050putCONTROLREG(ViSession instr, ViInt32 control);

// interupt register

ViInt32 _VI_FUNC PXITX9050getINTERUPT(ViSession instr);
ViInt16 _VI_FUNC PXITX9050putINTERUPT(ViSession instr, ViInt32 interupt);
ViInt16 _VI_FUNC PXITX9050getINTERUPT1STATUS(ViSession instr);
ViInt16 _VI_FUNC PXITX9050getINTERUPT2STATUS(ViSession instr);
ViInt16 _VI_FUNC PXITX9050getINTERUPT1ENABLE(ViSession instr);
ViInt16 _VI_FUNC PXITX9050putINTERUPT1ENABLE(ViSession instr, ViInt16 value);
ViInt16 _VI_FUNC PXITX9050getINTERUPT2ENABLE(ViSession instr);
ViInt16 _VI_FUNC PXITX9050putINTERUPT2ENABLE(ViSession instr, ViInt16 value);
ViInt16 _VI_FUNC PXITX9050getINTERUPT1POLARITY(ViSession instr);
ViInt16 _VI_FUNC PXITX9050putINTERUPT1POLARITY(ViSession instr, ViInt16 value);
ViInt16 _VI_FUNC PXITX9050getINTERUPT2POLARITY(ViSession instr);
ViInt16 _VI_FUNC PXITX9050putINTERUPT2POLARITY(ViSession instr, ViInt16 value);

// eeprom I/O
ViInt16 _VI_FUNC PXITX9050getEepromValid(ViSession instr);
ViInt16 _VI_FUNC PXITX9050getEEDO(ViSession instr);
ViInt16 _VI_FUNC PXITX9050putEEDI(ViSession instr, ViInt16 value);
ViInt16 _VI_FUNC PXITX9050getEEDI(ViSession instr);
ViInt16 _VI_FUNC PXITX9050putEECK(ViSession instr, ViInt16 value);
ViInt16 _VI_FUNC PXITX9050getEECK(ViSession instr);
ViInt16 _VI_FUNC PXITX9050putEECS(ViSession instr, ViInt16 value);
ViInt16 _VI_FUNC PXITX9050getEECS(ViSession instr);

// user IO
ViInt16 _VI_FUNC PXITX9050putUSER0(ViSession instr, ViInt16 value);
ViInt16 _VI_FUNC PXITX9050putUSER1(ViSession instr, ViInt16 value);
ViInt16 _VI_FUNC PXITX9050putUSER2(ViSession instr, ViInt16 value);
ViInt16 _VI_FUNC PXITX9050putUSER3(ViSession instr, ViInt16 value);

ViInt16 _VI_FUNC PXITX9050getUSER0(ViSession instr);
ViInt16 _VI_FUNC PXITX9050getUSER1(ViSession instr);
ViInt16 _VI_FUNC PXITX9050getUSER2(ViSession instr);
ViInt16 _VI_FUNC PXITX9050getUSER3(ViSession instr);


#if defined(__cplusplus) || defined(__cplusplus__)
}
#endif
#if defined(__cplusplus) || defined(__cplusplus__)
extern "C" {
#endif
	
#if defined (EXPORT311DLL) 
	#define DLL311  __declspec( dllexport  )
#else
	#define DLL311
#endif

DLL311 ViReal64 _VI_FUNC PXIT311Version(void);

DLL311 ViInt16	_VI_FUNC PXIT311ReadTemperature(ViSession instr, ViReal32	*temp);
DLL311 ViInt16	_VI_FUNC PXIT311ReadTemperatureCh(ViSession instr, ViReal32	*temp, ViUInt8 channel);

DLL311 ViInt16	_VI_FUNC PXIT311InitialiseModule(ViSession instr);
DLL311 ViInt16	_VI_FUNC PXIT311UnInitialiseModule(ViSession instr);

DLL311 ViInt16	_VI_FUNC PXIT311SetAttenuationA(ViSession instr, ViReal32 wavelength, ViReal32 attenuation);
DLL311 ViInt16	_VI_FUNC PXIT311SetAttenuationB(ViSession instr, ViReal32 wavelength, ViReal32 attenuation);

// controlled feedback
DLL311 ViInt16	_VI_FUNC PXIT311UnlockRefAttenuation(ViSession instr311, ViSession instr306, ViUInt8	channel);
DLL311 ViInt16	_VI_FUNC PXIT311SetAttenuationWithFeedback(ViSession instr, ViReal32 wavelength, ViReal32 wavelength2, ViReal32 attenuation, ViReal32 attenuation2, ViUInt8 channelflag);
DLL311 ViInt16	_VI_FUNC PXIT311InitRefAttenuation(ViSession instr311, ViSession instr306, ViUInt8 channel, ViReal32 wavelength, ViReal32* power, ViUInt8 trigger);
DLL311 ViInt16	_VI_FUNC PXIT311SetRefAttenuation(ViSession instr311, ViSession instr306, ViUInt8 channel, ViReal32 wavelength, ViReal32 attenuation);
DLL311 ViInt16	_VI_FUNC PXIT311SetRefPower(ViSession instr311, ViSession instr306, ViUInt8 channel, ViReal32 wavelength, ViReal32* power);
DLL311 ViInt16	_VI_FUNC PXIT311RefStatus(ViSession instr311, ViSession instr306, ViUInt8 *lock, ViUInt8 *limit);	// return feedback status for board

// sequence
DLL311 ViInt16	_VI_FUNC PXIT311SetTriggerSetup(ViSession instr, ViUInt8	config);
DLL311 ViInt16	_VI_FUNC PXIT311SetMasterMode(ViSession instr, ViUInt8	config);
DLL311 ViInt16	_VI_FUNC PXIT311SetHWTriggerMode(ViSession instr, ViUInt8	config);
DLL311 ViInt16	_VI_FUNC PXIT311Status(ViSession instr, ViUInt8 *value);	// return status reg
DLL311 ViInt16	_VI_FUNC PXIT311SequenceStatus(ViSession instr, ViUInt8 *value);	// return sequence status for board

DLL311	ViInt16	_VI_FUNC PXIT311SequenceWriteBlockData(ViSession instr, ViUInt16 count, PXIT3XXSequenceData *data, ViUInt8 format, ViReal32 wavelengthA, ViReal32 wavelengthB);
DLL311	ViInt16	_VI_FUNC PXIT311SequenceWriteBlockHeader(ViSession instr, ViUInt16 count, ViUInt8 sourcedelay, ViUInt8 measuredelay);
DLL311	ViInt16	_VI_FUNC PXIT311SequenceReadBlockData(ViSession instr, ViUInt16 count, PXIT3XXSequenceData *data, ViUInt8 format);
DLL311	ViInt16	_VI_FUNC PXIT311SequenceReadBlockHeader(ViSession instr, ViUInt16 *count, ViUInt8 *sourcedelay, ViUInt8 *measuredelay, ViUInt16 *controlword);
DLL311	ViInt16	_VI_FUNC PXIT311SequenceWriteBlockDataVB(ViSession instr, ViUInt16 count, ViReal64* source1, ViReal64* source2, ViUInt8 format, ViReal32 wavelengthA, ViReal32 wavelengthB);
DLL311	ViInt16	_VI_FUNC PXIT311SequenceReadBlockDataVB(ViSession instr, ViUInt16 count, ViReal64* source1, ViReal64* source2, ViUInt8 format);

DLL311 ViInt16	_VI_FUNC PXIT311SetLocalRelay(ViSession instr, ViUInt8 value);

DLL311 ViInt16 _VI_FUNC PXIT311SetReferenceAttenuation(ViReal32	AttenuationCh1, ViReal32	AttenuationCh2);

DLL311 ViInt16	_VI_FUNC PXIT311SequenceIsolate(ViSession instr);

DLL311 ViStatus _VI_FUNC PXIT311UpdateRelayInfoToEEPROM(instr);
DLL311 ViStatus _VI_FUNC PXIT311SetRelayThresholds(ViSession instr, ViUInt32* threshold);
DLL311 ViStatus _VI_FUNC PXIT311GetRelayThresholds(ViSession instr, ViUInt32* threshold);

#if defined(__cplusplus) || defined(__cplusplus__)
}
#endif
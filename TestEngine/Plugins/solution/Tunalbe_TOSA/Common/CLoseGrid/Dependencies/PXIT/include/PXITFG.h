#if defined(__cplusplus) || defined(__cplusplus__)
   extern "C" {
#endif

ViInt16	_VI_FUNC PXITFGSetFrequency(ViSession instr, ViReal64 value, ViReal64 callo, ViReal64 calhi);

// routine to select amplitude from a calibration table
ViInt16	_VI_FUNC PXITFGSetAmplitude(ViSession instr, ViReal64 value, ViReal64 callo, ViReal64 calhi);

// this user command specifies the duty cycle between 15% and 85%
ViInt16	_VI_FUNC PXITFGSetDutyCycle(ViSession instr, ViReal64 value, ViReal64 callo, ViReal64 calhi);

// this user command specifies the waveform type
ViInt16	_VI_FUNC PXITFGSetWaveform(ViSession instr, ViInt16 value);

ViInt16	_VI_FUNC PXITFGWaveformOn(ViSession instr);
ViInt16	_VI_FUNC PXITFGWaveformOff(ViSession instr);


ViInt16	_VI_FUNC PXITFGInitWaveform(ViSession instr);

ViInt16	_VI_FUNC PXITFGSetVoltage(ViSession instr, ViInt16 channel, ViReal64 value, ViReal64 callo, ViReal64 calhi);
ViReal64	_VI_FUNC PXITFGReadVoltage(ViSession instr, ViInt16 channel, ViReal64 callo, ViReal64 calhi);


#if defined(__cplusplus) || defined(__cplusplus__)
   }
#endif
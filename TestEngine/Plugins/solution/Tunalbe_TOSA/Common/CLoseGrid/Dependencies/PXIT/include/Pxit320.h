#if defined(__cplusplus) || defined(__cplusplus__)
extern "C" {
#endif
	   

#if defined (EXPORT320DLL) 
	#define DLL320  __declspec( dllexport )
#else
	#define DLL320
#endif

// external dll functions
DLL320	ViReal64 _VI_FUNC PXIT320Version(void);

DLL320 ViInt16	_VI_FUNC PXIT320InitialiseModule(ViSession instr);
DLL320 ViInt16	_VI_FUNC PXIT320UnInitialiseModule(ViSession instr);
DLL320  ViInt16	_VI_FUNC PXIT320PulseESD(ViSession instr, ViUInt8 polarity, ViUInt16 repeatcount, ViReal32	repeatwidth, ViReal32 voltage);
DLL320 ViInt16	_VI_FUNC PXIT320PulseComplexESD(ViSession instr, ViUInt8* polarityArray, ViUInt16 ArrayCount, ViReal32	repeatwidth, ViReal32	Voltage);

DLL320 ViStatus _VI_FUNC PXIT320UpdateRelayInfoToEEPROM(instr);
DLL320 ViStatus _VI_FUNC PXIT320GetRelayThresholds(ViSession instr, ViUInt32* threshold);
DLL320 ViStatus _VI_FUNC PXIT320SetRelayThresholds(ViSession instr, ViUInt32* threshold);
DLL320 ViStatus _VI_FUNC PXIT320GetModuleSerialNumber(ViSession instr, ViChar* name);


#if defined(__cplusplus) || defined(__cplusplus__)
}
#endif
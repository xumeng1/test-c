ViInt16 _VI_FUNC PXIT108LDSeqA(ViSession instr,ViInt16 startptr, ViInt16 ptr);

// 1.2 - dual port ram commands
ViInt16 _VI_FUNC PXIT108FillSeqA(ViSession instr,ViInt16 *block);

// run specific sequence. at 10th september only sequence A is set to run. later version will allow all to run
ViInt16 _VI_FUNC PXIT108RunSeq(ViSession instr);

// 1.2 - sequence control
ViInt16 _VI_FUNC PXIT108SetupSequence(ViSession instr, ViInt16 looptype, ViInt16 sequenceflag);

// 1.2 - timing commands
ViInt16 _VI_FUNC PXIT108SetTriggerPeriod(ViSession instr, ViInt32 period, ViInt32 count);
ViInt16 _VI_FUNC PXIT108SetTriggerMode(ViSession instr,ViInt16 mode);

// 1.2 - handshake control byte
ViInt16 _VI_FUNC PXIT108WriteControlByte(ViSession instr,ViInt16 value);
ViInt16 _VI_FUNC PXIT108ReadControlByte(ViSession instr,ViInt16 value);

// set up tmrs and ccp modules which control internal timing
ViInt16 _VI_FUNC PXIT108LDTMR1(ViSession instr,ViInt16 T1CON,ViInt16 TMR1H,ViInt16 TMR1L);
ViInt16 _VI_FUNC PXIT108LDTMR2(ViSession instr,ViInt16 T2CON,ViInt16 TMR2,ViInt16 PR2);
ViInt16 _VI_FUNC PXIT108LDCCP2(ViSession instr,ViInt16 CCP2CON,ViInt16 CCPR2H,ViInt16 CCPR2L);
ViInt16 _VI_FUNC PXIT108LDCCP1(ViSession instr,ViInt16 CCP1CON,ViInt16 CCPR1H,ViInt16 CCPR1L);

// enable and diable the pic to allow master control over pic bus by 9050
ViInt16 _VI_FUNC PXIT108EnablePic(ViSession instr);
ViInt16 _VI_FUNC PXIT108DisablePic(ViSession instr);

ViInt16 _VI_FUNC PXIT108RDCodeVersion(ViSession instr);

ViInt16 _VI_FUNC PXIT108ConnectLocalIn(ViSession instr, ViInt16 triggernumber);
ViInt16 _VI_FUNC PXIT108ConnectLocalOut(ViSession instr, ViInt16 triggernumber);
ViInt16 _VI_FUNC PXIT108DisonnectLocalIn(ViSession instr, ViInt16 triggernumber);
ViInt16 _VI_FUNC PXIT108DisonnectLocalOut(ViSession instr, ViInt16 triggernumber);

ViInt16 _VI_FUNC PXIT108SyncComms(ViSession instr);

ViReal64 _VI_FUNC PXIT108Version(void);

ViInt16 _VI_FUNC PXIT108SelfTest1(ViSession instr);
ViInt16 _VI_FUNC PXIT108SelfTest2(ViSession instr);

ViInt16 _VI_FUNC PXIT108RdInternalRAM(ViSession instr, ViInt16 address);
ViInt16 _VI_FUNC PXIT108WrInternalRAM(ViSession instr, ViInt16 address, ViInt16 value);

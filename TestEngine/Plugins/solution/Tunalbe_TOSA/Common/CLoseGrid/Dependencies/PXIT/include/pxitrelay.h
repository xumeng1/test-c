#if defined(__cplusplus) || defined(__cplusplus__)
extern "C" {
#endif
	   
ViInt16 _VI_FUNC PXITOpenRelay(ViSession instr, ViUInt8 relayno);
ViInt16 _VI_FUNC PXITCloseRelay(ViSession instr, ViUInt8 relayno);
ViInt16 _VI_FUNC PXITResetRelayCard(ViSession instr);
ViInt16 _VI_FUNC PXITRelayStatus(ViSession instr, ViUInt8 relayno);

ViInt16 _VI_FUNC PXITRelaySelfTest(ViSession instr);
ViReal64 _VI_FUNC PXITRelayVersion(void);
ViInt16 _VI_FUNC PXITRelaySetRelays(ViSession instr, ViInt16 *relaylist, ViInt16 count);
ViInt16 _VI_FUNC PXITRelayAddRelays(ViSession instr, ViInt16 *relaylist, ViInt16 count);
ViInt16 _VI_FUNC PXITRelaySubRelays(ViSession instr, ViInt16 *relaylist, ViInt16 count);

#if defined(__cplusplus) || defined(__cplusplus__)
}
#endif
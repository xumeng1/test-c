// 14 may 2003 - 4th dimension in the form of W added to facilitate attenuation

typedef struct
{
	void		*next;
	ViReal32	x0;	// lower wavelength
//	ViReal32	x1;	// left control point
//	ViReal32	x2;	// right control point
	ViReal32	x3;	// upper wavelength
	ViReal32	y0;	// lower responsivity
//	ViReal32	y1;	// left control point
//	ViReal32	y2;	// right control point
	ViReal32	y3;	// upper responsivity
	ViReal32	Ax;	// a factor
	ViReal32	Bx;	// b factor
	ViReal32	Cx;	// c factor
	ViReal32	Ay;	// a factor
	ViReal32	By;	// b factor
	ViReal32	Cy;	// c factor
	ViReal32	Z;	// third dimension
	ViReal32	W;	// fourth dimension
} curve;

typedef struct
{
	ViReal32	w;
	ViReal32	x;
	ViReal32	y;
	ViReal32	z;
	void*		next;	// added for file loading of caldata
} PXITCal;

typedef struct
{
	void*	next;
	ViReal32 Z;
} Zstore;

typedef struct
{
	void*	next;
	curve	curve;
} curve4D;


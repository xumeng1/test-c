#if defined(__cplusplus) || defined(__cplusplus__)
extern "C" {
#endif
	   
#if defined (EXPORT305DLL) 
#define DLL305  __declspec( dllexport )
#else
#define DLL305
#endif

// external dll functions
DLL305 ViReal64 _VI_FUNC PXIT305Version(void);

DLL305 ViInt16	_VI_FUNC PXIT305SetTriggerSetup(ViSession instr, ViUInt8	config);
DLL305 ViInt16	_VI_FUNC PXIT305SetMasterMode(ViSession instr, ViUInt8	config);
DLL305 ViInt16	_VI_FUNC PXIT305SetHWTriggerMode(ViSession instr, ViUInt8	config);

DLL305 ViInt16	_VI_FUNC PXIT305SetComplianceVoltage(ViSession instr, ViReal64	value);
DLL305 ViInt16	_VI_FUNC PXIT305SetCurrent(ViSession instr, ViReal64	value);
DLL305 ViInt16	_VI_FUNC PXIT305ReadVoltage(ViSession instr, ViReal64 *value);
DLL305 ViInt16	_VI_FUNC PXIT305SetMeasureAverage(ViUInt8 value);
DLL305 ViInt16	_VI_FUNC PXIT305SetHistory(ViUInt8 value);

DLL305 ViInt16	_VI_FUNC PXIT305ConnectOutput(ViSession instr);	// set output relay
DLL305 ViInt16	_VI_FUNC PXIT305DisconnectOutput(ViSession instr);	// clr output relay
DLL305 ViInt16	_VI_FUNC PXIT305ShortOutput(ViSession instr);	// set short relay
DLL305 ViInt16	_VI_FUNC PXIT305UnShortOutput(ViSession instr);	// clr short relay
DLL305 ViInt16	_VI_FUNC PXIT305SetOutputRange(ViSession instr, ViInt16 Range);	// 500mA or 250mA
DLL305 ViInt16	_VI_FUNC PXIT305SetLocalRelay(ViSession instr, ViUInt8 value);
DLL305 ViInt16	_VI_FUNC PXIT305InitialiseModule(ViSession instr);
DLL305 ViInt16	_VI_FUNC PXIT305UnInitialiseModule(ViSession instr);
DLL305 ViInt16	_VI_FUNC PXIT305SetOutputMode(ViSession instr, ViInt16 value);	// 2 wire/4 wire
DLL305 ViInt16	_VI_FUNC PXIT305SetMaximumCurrent(ViSession instr, ViReal32 value);
DLL305 ViInt16	_VI_FUNC PXIT305ReadMaximumCurrent(ViSession instr, ViReal32* value);


DLL305	ViInt16	_VI_FUNC PXIT305SequenceReadBlockData(ViSession instr, ViUInt16 count, PXIT3XXSequenceData *data, ViUInt8 format);
DLL305	ViInt16	_VI_FUNC PXIT305SequenceWriteBlockData(ViSession instr, ViUInt16 count, PXIT3XXSequenceData *data, ViUInt8 format);
DLL305	ViInt16	_VI_FUNC PXIT305SequenceWriteBlockDataVB(ViSession instr, ViUInt16 count, ViReal64* source, ViReal64* measure, ViUInt8 format);
DLL305	ViInt16	_VI_FUNC PXIT305SequenceReadBlockDataVB(ViSession instr, ViUInt16 count, ViReal64* source, ViReal64* measure, ViUInt8 format);
DLL305	ViInt16	_VI_FUNC PXIT305SequenceWriteBlockHeader(ViSession instr, ViUInt16 count, ViUInt8 sourcedelay, ViUInt8 measuredelay);
DLL305	ViInt16	_VI_FUNC PXIT305SequenceReadBlockHeader(ViSession instr, ViUInt16 *count, ViUInt8 *sourcedelay, ViUInt8 *measuredelay, ViUInt16 *controlword);
DLL305 ViInt16	_VI_FUNC PXIT305SequenceStatus(ViSession instr, ViUInt8 *value);	// return sequence status for board

DLL305	ViStatus	_VI_FUNC PXIT305SequenceWriteBlockVB(ViSession instr, ViUInt16 count, ViReal64* source, ViReal64* measure, ViUInt8 format, ViUInt8 sourcedelay, ViUInt8 measuredelay);
DLL305	ViStatus	_VI_FUNC PXIT305SequenceReadBlockVB(ViSession instr, ViUInt16 *count, ViReal64* source, ViReal64* measure, ViUInt8 format, ViUInt8 *sourcedelay, ViUInt8 *measuredelay);

// improved version of call that allow block to have control of the format, the average, and the range
// requierd specific PIC version
DLL305	ViInt16	_VI_FUNC PXIT305SequenceWriteBlockHeader2(ViSession instr, ViUInt16 count, ViUInt8 sourcedelay, ViUInt8 measuredelay, ViUInt8 average, ViUInt8 format, ViUInt8 range);
DLL305	ViInt16	_VI_FUNC PXIT305SequenceReadBlockHeader2(ViSession instr, ViUInt16 *count, ViUInt8 *sourcedelay, ViUInt8 *measuredelay, ViUInt8 *average, ViUInt8 *format, ViUInt8 *range);

// ensure a sequence module does not inhibit sequencing
DLL305	ViInt16	_VI_FUNC PXIT305SequenceIsolate(ViSession instr);

DLL305 ViStatus _VI_FUNC PXIT305UpdateRelayInfoToEEPROM(ViSession instr);
DLL305 ViStatus _VI_FUNC PXIT305SetRelayThresholds(ViSession instr, ViUInt32* threshold);
DLL305 ViStatus _VI_FUNC PXIT305GetRelayThresholds(ViSession instr, ViUInt32* threshold);

#if defined(__cplusplus) || defined(__cplusplus__)
}
#endif
#if defined(__cplusplus) || defined(__cplusplus__)
extern "C" {
#endif
	   

#if defined (EXPORT331DLL) 
	#define DLL331  __declspec( dllexport )
#else
	#define DLL331
#endif

// external dll functions
DLL331	ViReal64 _VI_FUNC PXIT331Version(void);

DLL331 ViInt16	_VI_FUNC PXIT331ReadVoltageAndCurrent(ViSession instr, ViReal64 *voltage,  ViReal64 *current);

DLL331	ViInt16	_VI_FUNC PXIT331SetVoltage(ViSession instr, ViReal64 value);
DLL331	ViInt16	_VI_FUNC PXIT331ReadVoltage(ViSession instr, ViReal64 *value);
DLL331	ViInt16	_VI_FUNC PXIT331SetComplianceCurrent(ViSession instr, ViReal64 value);
DLL331	ViInt16	_VI_FUNC PXIT331ReadCurrent(ViSession instr, ViReal64 *value);

DLL331	ViInt16	_VI_FUNC PXIT331ReadMaximumVoltage(ViSession instr, ViReal32* value);
DLL331	ViInt16	_VI_FUNC PXIT331SetMaximumVoltage(ViSession instr, ViReal32 value);

DLL331	ViInt16	_VI_FUNC PXIT331SetRange(ViSession instr, ViUInt8 currentmeasurerange, ViUInt8 voltagerange, ViUInt8 currentcompliancerange);
DLL331	ViInt16	_VI_FUNC PXIT331ConnectOutput(ViSession instr, ViUInt8 polarity, ViUInt8 bypass);
DLL331	ViInt16	_VI_FUNC PXIT331DisconnectOutput(ViSession instr);

DLL331	ViInt16	_VI_FUNC PXIT331InitialiseModule(ViSession instr);
DLL331	ViInt16	_VI_FUNC PXIT331UnInitialiseModule(ViSession instr);

DLL331 ViInt16	_VI_FUNC PXIT331SetMeasureAverage(ViUInt8 value);

DLL331 ViInt16	_VI_FUNC PXIT331GetComplianceCurrentSetting(ViSession instr, ViReal64 *value);
DLL331 ViInt16	_VI_FUNC PXIT331GetVoltageSetting(ViSession instr, ViReal64 *value);

DLL331 ViInt16	_VI_FUNC PXIT331SetHistory(ViUInt8 value);

DLL331 ViInt16	_VI_FUNC PXIT331ReadRangeLimit(ViSession instr, ViReal32* MaxSCurrenthi, ViReal32* MaxSCurrentlo, ViReal32* MaxSVoltagehi, ViReal32* MaxSVoltagelo, ViReal32* MaxMCurrenthi, ViReal32* MaxMCurrentme, ViReal32* MaxMCurrentlo, ViReal32* MaxMVoltagehi, ViReal32* MaxMVoltagelo, ViReal32* VLimit);


DLL331 ViStatus _VI_FUNC PXIT331UpdateRelayInfoToEEPROM(instr);
DLL331 ViStatus _VI_FUNC PXIT331GetRelayThresholds(ViSession instr, ViUInt32* threshold);
DLL331 ViStatus _VI_FUNC PXIT331SetRelayThresholds(ViSession instr, ViUInt32* threshold);

#if defined(__cplusplus) || defined(__cplusplus__)
}
#endif
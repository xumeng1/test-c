#if defined(__cplusplus) || defined(__cplusplus__)
   extern "C" {
#endif
	   
// conversion routines
ViInt16 _VI_FUNC PXITInstNotoPciNo(ViInt16 InstNo);
ViInt16 _VI_FUNC PXITPciNotoInstNo(ViInt16 PciNo);
ViInt16 _VI_FUNC PXITPciNotoPxiNo(ViInt16 PciNo);
ViInt16 _VI_FUNC PXITPxiNotoPciNo(ViInt16 PxiNo);

// ensure that the ViSession is for a pxit card
ViInt16	_VI_FUNC PXITVerifyPxitCard(ViSession instr);

// return dll version
ViReal64 _VI_FUNC PXITVersion(void);

// return pxit ini versions
ViReal64 _VI_FUNC PXITcardiniVersion(void);
ViReal64 _VI_FUNC PXITmatrixiniVersion(void);

// return pxit card ID using eeprom
ViInt16	_VI_FUNC PXITGetCardType(ViSession instr);

// return pxit card version using eeprom
ViInt16	_VI_FUNC PXITGetCardVersion(ViSession instr);

Pxit_Map* _VI_FUNC PXITFindCardInfo(ViSession instr);
Matrix_Map* _VI_FUNC PXITFindMatrixInfo(ViSession instr);

#if defined(__cplusplus) || defined(__cplusplus__)
   }
#endif
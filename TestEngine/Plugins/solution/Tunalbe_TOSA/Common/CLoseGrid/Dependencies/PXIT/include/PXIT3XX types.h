#define Master			1
#define Slave			0
#define	Unknown			0

#define PXIT_integer	0x01
#define PXIT_float		0x00	// default

// when data is passed to and from the Read/Write Block Data functions, it will be passed
// as a set of pointers that can either be NULL or defined

// the pointer can either be to a ViUInt16 or a ViReal64 so is left void

typedef struct	
{
	ViUInt8 command;
	ViUInt8 dataitems;
	ViUInt8 data[33];							// array for packet transfer of data
} Packet;

union pxitdata
{
	ViReal64	*f;		// real values
	ViInt16		*i;		// DAC ADC data
};

typedef struct
{
	union pxitdata source1;
	union pxitdata source2;
	union pxitdata measure1;
	union pxitdata measure2;

	union pxitdata source3;		// expansion
	union pxitdata source4;		// expansion
	union pxitdata measure3;	// expansion
	union pxitdata measure4;	// expansion
} PXIT3XXSequenceData;

ViInt16	_VI_FUNC	PXITPXIOpticalSwitchClose(ViSession instr, ViInt16 switch_num, ViInt16 position_num);
ViReal64 _VI_FUNC	PXITPXIOpticalSwitchVersion(void);
ViInt16	_VI_FUNC	PXITPXIOpticalSwitchResetSwitch(ViSession instr, ViInt16 switch_num);
ViInt16	_VI_FUNC	PXITPXIOpticalSwitchReset(ViSession instr);

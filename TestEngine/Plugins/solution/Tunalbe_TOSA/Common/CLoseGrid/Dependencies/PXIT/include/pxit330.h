#if defined(__cplusplus) || defined(__cplusplus__)
extern "C" {
#endif
	   

#if defined (EXPORT330DLL) 
	#define DLL330  __declspec( dllexport )
#else
	#define DLL330
#endif

// external dll functions
DLL330	ViReal64 _VI_FUNC PXIT330Version(void);

DLL330	ViInt16	_VI_FUNC PXIT330ReadSpectrum(ViSession instr, ViUInt16 *intensity, ViUInt16 *exposure);
DLL330	ViInt16	_VI_FUNC PXIT330GetWavelength(ViSession instr, ViReal32 *wavelength);

DLL330	ViInt16	_VI_FUNC PXIT330SequenceReadBlockData(ViSession instr, ViUInt16 count, PXIT3XXSequenceData *data, ViUInt8 format);
DLL330	ViInt16	_VI_FUNC PXIT330SequenceWriteBlockData(ViSession instr, ViUInt16 count, PXIT3XXSequenceData *data, ViUInt8 format);
DLL330	ViInt16	_VI_FUNC PXIT330SequenceReadBlockDataVB(ViSession instr, ViUInt16 count, ViUInt16* measure, ViUInt8 format);
DLL330	ViInt16	_VI_FUNC PXIT330SequenceWriteBlockDataVB(ViSession instr, ViUInt16 count, ViUInt16* measure, ViUInt8 format);

DLL330	ViInt16	_VI_FUNC PXIT330SequenceWriteBlockHeader(ViSession instr, ViUInt16 count, ViUInt8 sourcedelay, ViUInt8 measuredelay, ViUInt8 format, ViUInt16 exposure);
DLL330	ViInt16	_VI_FUNC PXIT330SequenceReadBlockHeader(ViSession instr, ViUInt16* count, ViUInt8* sourcedelay, ViUInt8* measuredelay, ViUInt8* format,  ViUInt16* exposure);

DLL330 ViInt16	_VI_FUNC PXIT330InitialiseModule(ViSession instr);
DLL330 ViInt16	_VI_FUNC PXIT330UnInitialiseModule(ViSession instr);

DLL330 ViInt16	_VI_FUNC PXIT330SetTriggerSetup(ViSession instr, ViUInt8	config);
DLL330 ViInt16	_VI_FUNC PXIT330SetMasterMode(ViSession instr, ViUInt8	config);
DLL330 ViInt16	_VI_FUNC PXIT330SetHWTriggerMode(ViSession instr, ViUInt8	config);

DLL330 ViInt16	_VI_FUNC PXIT330SequenceStatus(ViSession instr, ViUInt8 *value);
DLL330 ViInt16	_VI_FUNC PXIT330SetLocalRelay(ViSession instr, ViUInt8 value);

DLL330 ViInt16	_VI_FUNC PXIT330SetMeasureAverage(ViUInt8 value);

DLL330 ViInt16	_VI_FUNC PXIT330SetAutoExposure(ViReal32 MaximumIntensity,
													ViReal32 MinimumIntensity, 
													ViReal32 TargetIntensity, 
													ViUInt8 Enable, 
													ViUInt8 MaximumRetries, 
													ViUInt16 MaximumExposure, 
													ViUInt16 MinimumExposure,
													ViReal64	HighMultiplier,
													ViReal64	LowMultiplier,
													ViReal32	ThresholdIntensity,
													ViReal32	UpperThresholdIntensity);

DLL330 ViInt16	_VI_FUNC PXIT330GetAutoExposure(ViReal32* iMaximumIntensity,
													ViReal32* iMinimumIntensity, 
													ViReal32* iTargetIntensity, 
													ViUInt8* iEnable, 
													ViUInt8* iMaximumRetries, 
													ViUInt16* iMaximumExposure, 
													ViUInt16* iMinimumExposure,
													ViUInt16* iAutoExposureRetryCount,
													ViUInt32* iAutoExposureMeasurements,
													ViReal64*	HighMultiplier,
													ViReal64*	LowMultiplier,
													ViReal32*	ThresholdIntensity,
													ViReal32*	UpperThresholdIntensity);

DLL330 ViInt16	_VI_FUNC PXIT330ReadRawSpectrum(ViSession instr, ViUInt16 *intensity, ViUInt16* exposure);

DLL330 ViInt16	_VI_FUNC PXIT330DisableAutoExposure(void);
DLL330 ViInt16	_VI_FUNC PXIT330EnableAutoExposure(void);


#if defined(__cplusplus) || defined(__cplusplus__)
}
#endif
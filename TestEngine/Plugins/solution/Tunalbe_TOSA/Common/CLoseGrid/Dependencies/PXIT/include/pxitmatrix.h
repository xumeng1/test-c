#if defined(__cplusplus) || defined(__cplusplus__)
   extern "C" {
#endif
	   
ViInt16 _VI_FUNC 	PXITMatrixOpen(ViSession instr, ViUInt8 ExternalID, ViUInt8 InternalID);
ViInt16 _VI_FUNC 	PXITMatrixClose(ViSession instr, ViUInt8 ExternalID, ViUInt8 InternalID);
ViInt16 _VI_FUNC 	PXITBufferIO(ViSession instr, ViUInt8 byte);
ViReal64 _VI_FUNC PXITMatrixVersion(void);
ViInt16 _VI_FUNC 	PXITMatrixReset(ViSession instr);

#if defined(__cplusplus) || defined(__cplusplus__)
   }
#endif
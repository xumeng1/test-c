using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.Algorithms
{
    class SuperModeMapAnalysis
    {
        const int MIN_NUMBER_OF_MODES = 5;

        const int RESULTS_NO_ERROR = 0;
        const int RESULTS_GENERAL_ERROR = 1;
        const int RESULTS_NO_MODEMAP_DATA = 2;
        const int RESULTS_PO_TOO_LOW = 4;
        const int RESULTS_PO_TOO_HIGH = 8;
        const int RESULTS_TOO_MANY_JUMPS = 16;
        const int RESULTS_TOO_FEW_JUMPS = 32;
        const int RESULTS_INVALID_NUMBER_OF_LINES = 64;
        const int RESULTS_INVALID_LINES_REMOVED = 128;
        const int RESULTS_FORWARD_LINES_MISSING = 256;
        const int RESULTS_REVERSE_LINES_MISSING = 512;

        internal bool _screeningPassed;

        int _qaDetectedError;

        public enum rcode
        {
            ok = 0,
            qaerror
        }

        #region Varients
        // Pass fail thresholds for the longitudinal mode
        PassFailThresholds _lmPassFailThresholds = new PassFailThresholds();

        //Data
        PassFailAnalysis _maxUpperModeBordersRemovedPFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _maxLowerModeBordersRemovedPFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _maxMiddleLineRemovedPFAnalysis = new PassFailAnalysis();

        PassFailAnalysis _maxForwardModeWidthPFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _minForwardModeWidthPFAnalysis = new PassFailAnalysis();

        PassFailAnalysis _maxReverseModeWidthPFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _minReverseModeWidthPFAnalysis = new PassFailAnalysis();

        PassFailAnalysis _meanPercWorkingRegionPFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _minPercWorkingRegionPFAnalysis = new PassFailAnalysis();

        PassFailAnalysis _maxForwardLowerModeBDCAreaPFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _maxForwardLowerModeBDCAreaXLengthPFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _sumForwardLowerModeBDCAreasPFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _numForwardLowerModeBDCAreasPFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _maxForwardUpperModeBDCAreaPFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _maxForwardUpperModeBDCAreaXLengthPFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _sumForwardUpperModeBDCAreasPFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _numForwardUpperModeBDCAreasPFAnalysis = new PassFailAnalysis();

        PassFailAnalysis _maxReverseLowerModeBDCAreaPFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _maxReverseLowerModeBDCAreaXLengthPFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _sumReverseLowerModeBDCAreasPFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _numReverseLowerModeBDCAreasPFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _maxReverseUpperModeBDCAreaPFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _maxReverseUpperModeBDCAreaXLengthPFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _sumReverseUpperModeBDCAreasPFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _numReverseUpperModeBDCAreasPFAnalysis = new PassFailAnalysis();

        PassFailAnalysis _forwardLowerModalDistortionAnglePFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _forwardUpperModalDistortionAnglePFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _reverseLowerModalDistortionAnglePFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _reverseUpperModalDistortionAnglePFAnalysis = new PassFailAnalysis();

        PassFailAnalysis _forwardLowerModalDistortionAngleXPositionPFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _forwardUpperModalDistortionAngleXPositionPFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _reverseLowerModalDistortionAngleXPositionPFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _reverseUpperModalDistortionAngleXPositionPFAnalysis = new PassFailAnalysis();

        PassFailAnalysis _middleLineRMSValuesPFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _maxMiddleLineSlopePFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _minMiddleLineSlopePFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _maxPrGapPFAnalysis = new PassFailAnalysis();

        PassFailAnalysis _maxForwardLowerModeSlopePFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _maxForwardUpperModeSlopePFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _maxReverseLowerModeSlopePFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _maxReverseUpperModeSlopePFAnalysis = new PassFailAnalysis();

        //
        /////////////////////////////////////////////////////////////////
        // List of mode width vectors
        List<Vector> _forwardModeWidths = new List<Vector>();
        List<double> _maxForwardModeWidths = new List<double>();
        List<double> _minForwardModeWidths = new List<double>();
        List<double> _meanForwardModeWidths = new List<double>();
        List<double> _sumForwardModeWidths = new List<double>();
        List<double> _varForwardModeWidths = new List<double>();
        List<double> _stdDevForwardModeWidths = new List<double>();
        List<double> _medianForwardModeWidths = new List<double>();
        List<double> _numForwardModeWidthSamples = new List<double>();

        ///////////////////////////////////////

        List<Vector> _forwardLowerModeSlopes = new List<Vector>();
        List<double> _maxForwardLowerModeSlopes=new List<double>();
        List<double> _minForwardLowerModeSlopes = new List<double>();
        List<double> _meanForwardLowerModeSlopes = new List<double>();
        List<double> _sumForwardLowerModeSlopes = new List<double>();
        List<double> _varForwardLowerModeSlopes = new List<double>();
        List<double> _stdDevForwardLowerModeSlopes = new List<double>();
        List<double> _medianForwardLowerModeSlopes = new List<double>();
        List<double> _numForwardLowerModeSlopeSamples = new List<double>();

        List<Vector> _forwardUpperModeSlopes = new List<Vector>();
        List<double> _maxForwardUpperModeSlopes = new List<double>();
        List<double> _minForwardUpperModeSlopes = new List<double>();
        List<double> _meanForwardUpperModeSlopes = new List<double>();
        List<double> _sumForwardUpperModeSlopes = new List<double>();
        List<double> _varForwardUpperModeSlopes = new List<double>();
        List<double> _stdDevForwardUpperModeSlopes = new List<double>();
        List<double> _medianForwardUpperModeSlopes = new List<double>();
        List<double> _numForwardUpperModeSlopeSamples = new List<double>();

        ///////////////////////////////////////

        List<double> _forwardLowerModalDistortionAngles = new List<double>();
        List<double> _forwardUpperModalDistortionAngles = new List<double>();

        List<double> _forwardLowerModalDistortionAngleXPositions = new List<double>();
        List<double> _forwardUpperModalDistortionAngleXPositions = new List<double>();

        ///////////////////////////////////////

        List<Vector> _forwardLowerModeBDCAreas = new List<Vector>();
        List<double> _maxForwardLowerModeBDCAreas = new List<double>();
        List<double> _minForwardLowerModeBDCAreas = new List<double>();
        List<double> _meanForwardLowerModeBDCAreas = new List<double>();
        List<double> _sumForwardLowerModeBDCAreas = new List<double>();
        List<double> _varForwardLowerModeBDCAreas  = new List<double>();
        List<double> _stdDevForwardLowerModeBDCAreas  = new List<double>();
        List<double> _medianForwardLowerModeBDCAreas  = new List<double>();
        List<double> _numForwardLowerModeBDCAreaSamples  = new List<double>();
        //
        List<Vector> _forwardUpperModeBDCAreas = new List<Vector>();
        List<double> _maxForwardUpperModeBDCAreas  = new List<double>();
        List<double> _minForwardUpperModeBDCAreas  = new List<double>();
        List<double> _meanForwardUpperModeBDCAreas  = new List<double>();
        List<double> _sumForwardUpperModeBDCAreas  = new List<double>();
        List<double> _varForwardUpperModeBDCAreas  = new List<double>();
        List<double> _stdDevForwardUpperModeBDCAreas  = new List<double>();
        List<double> _medianForwardUpperModeBDCAreas  = new List<double>();
        List<double> _numForwardUpperModeBDCAreaSamples  = new List<double>();

        ///////////////////////////////////////

        List<Vector> _forwardLowerModeBDCAreaXLengths = new List<Vector>();
        List<double> _maxForwardLowerModeBDCAreaXLengths  = new List<double>();
        List<double> _minForwardLowerModeBDCAreaXLengths  = new List<double>();
        List<double> _meanForwardLowerModeBDCAreaXLengths  = new List<double>();
        List<double> _sumForwardLowerModeBDCAreaXLengths  = new List<double>();
        List<double> _varForwardLowerModeBDCAreaXLengths  = new List<double>();
        List<double> _stdDevForwardLowerModeBDCAreaXLengths  = new List<double>();
        List<double> _medianForwardLowerModeBDCAreaXLengths  = new List<double>();
        List<double> _numForwardLowerModeBDCAreaXLengthSamples  = new List<double>();

        List<Vector> _forwardUpperModeBDCAreaXLengths = new List<Vector>();
        List<double> _maxForwardUpperModeBDCAreaXLengths  = new List<double>();
        List<double> _minForwardUpperModeBDCAreaXLengths  = new List<double>();
        List<double> _meanForwardUpperModeBDCAreaXLengths  = new List<double>();
        List<double> _sumForwardUpperModeBDCAreaXLengths  = new List<double>();
        List<double> _varForwardUpperModeBDCAreaXLengths  = new List<double>();
        List<double> _stdDevForwardUpperModeBDCAreaXLengths  = new List<double>();
        List<double> _medianForwardUpperModeBDCAreaXLengths  = new List<double>();
        List<double> _numForwardUpperModeBDCAreaXLengthSamples  = new List<double>();

        ///////////////////////////////////////

        // List of mode width vectors
        List<Vector> _reverseModeWidths = new List<Vector>();
        List<double> _maxReverseModeWidths  = new List<double>();
        List<double> _minReverseModeWidths  = new List<double>();
        List<double> _meanReverseModeWidths  = new List<double>();
        List<double> _sumReverseModeWidths  = new List<double>();
        List<double> _varReverseModeWidths  = new List<double>();
        List<double> _stdDevReverseModeWidths  = new List<double>();
        List<double> _medianReverseModeWidths  = new List<double>();
        List<double> _numReverseModeWidthSamples  = new List<double>();

        ///////////////////////////////////////

        List<Vector> _reverseLowerModeSlopes = new List<Vector>();
        List<double> _maxReverseLowerModeSlopes  = new List<double>();
        List<double> _minReverseLowerModeSlopes  = new List<double>();
        List<double> _meanReverseLowerModeSlopes  = new List<double>();
        List<double> _sumReverseLowerModeSlopes  = new List<double>();
        List<double> _varReverseLowerModeSlopes  = new List<double>();
        List<double> _stdDevReverseLowerModeSlopes  = new List<double>();
        List<double> _medianReverseLowerModeSlopes  = new List<double>();
        List<double> _numReverseLowerModeSlopeSamples  = new List<double>();

        List<Vector> _reverseUpperModeSlopes = new List<Vector>();
        List<double> _maxReverseUpperModeSlopes  = new List<double>();
        List<double> _minReverseUpperModeSlopes  = new List<double>();
        List<double> _meanReverseUpperModeSlopes  = new List<double>();
        List<double> _sumReverseUpperModeSlopes  = new List<double>();
        List<double> _varReverseUpperModeSlopes  = new List<double>();
        List<double> _stdDevReverseUpperModeSlopes  = new List<double>();
        List<double> _medianReverseUpperModeSlopes  = new List<double>();
        List<double> _numReverseUpperModeSlopeSamples  = new List<double>();

        ///////////////////////////////////////

        List<double> _reverseLowerModalDistortionAngles  = new List<double>();
        List<double> _reverseLowerModalDistortionAngleXPositions  = new List<double>();

        List<double> _reverseUpperModalDistortionAngles  = new List<double>();
        List<double> _reverseUpperModalDistortionAngleXPositions  = new List<double>();

        ///////////////////////////////////////

        List<Vector> _reverseLowerModeBDCAreas = new List<Vector>();
        List<double> _maxReverseLowerModeBDCAreas  = new List<double>();
        List<double> _minReverseLowerModeBDCAreas  = new List<double>();
        List<double> _meanReverseLowerModeBDCAreas  = new List<double>();
        List<double> _sumReverseLowerModeBDCAreas  = new List<double>();
        List<double> _varReverseLowerModeBDCAreas  = new List<double>();
        List<double> _stdDevReverseLowerModeBDCAreas  = new List<double>();
        List<double> _medianReverseLowerModeBDCAreas  = new List<double>();
        List<double> _numReverseLowerModeBDCAreaSamples  = new List<double>();

        List<Vector> _reverseUpperModeBDCAreas = new List<Vector>();
        List<double> _maxReverseUpperModeBDCAreas  = new List<double>();
        List<double> _minReverseUpperModeBDCAreas  = new List<double>();
        List<double> _meanReverseUpperModeBDCAreas  = new List<double>();
        List<double> _sumReverseUpperModeBDCAreas  = new List<double>();
        List<double> _varReverseUpperModeBDCAreas  = new List<double>();
        List<double> _stdDevReverseUpperModeBDCAreas  = new List<double>();
        List<double> _medianReverseUpperModeBDCAreas  = new List<double>();
        List<double> _numReverseUpperModeBDCAreaSamples  = new List<double>();

        ///////////////////////////////////////

        List<Vector> _reverseLowerModeBDCAreaXLengths = new List<Vector>();
        List<double> _maxReverseLowerModeBDCAreaXLengths  = new List<double>();
        List<double> _minReverseLowerModeBDCAreaXLengths  = new List<double>();
        List<double> _meanReverseLowerModeBDCAreaXLengths  = new List<double>();
        List<double> _sumReverseLowerModeBDCAreaXLengths  = new List<double>();
        List<double> _varReverseLowerModeBDCAreaXLengths  = new List<double>();
        List<double> _stdDevReverseLowerModeBDCAreaXLengths  = new List<double>();
        List<double> _medianReverseLowerModeBDCAreaXLengths  = new List<double>();
        List<double> _numReverseLowerModeBDCAreaXLengthSamples  = new List<double>();

        List<Vector> _reverseUpperModeBDCAreaXLengths = new List<Vector>();
        List<double> _maxReverseUpperModeBDCAreaXLengths  = new List<double>();
        List<double> _minReverseUpperModeBDCAreaXLengths  = new List<double>();
        List<double> _meanReverseUpperModeBDCAreaXLengths  = new List<double>();
        List<double> _sumReverseUpperModeBDCAreaXLengths  = new List<double>();
        List<double> _varReverseUpperModeBDCAreaXLengths  = new List<double>();
        List<double> _stdDevReverseUpperModeBDCAreaXLengths  = new List<double>();
        List<double> _medianReverseUpperModeBDCAreaXLengths  = new List<double>();
        List<double> _numReverseUpperModeBDCAreaXLengthSamples  = new List<double>();

        ///////////////////////////////////////

        double _overallPercStableArea;

        //
        ///
        ///////////////////////////////////////////////////////

        List<Vector> _stableAreaWidths = new List<Vector>();
        List<double> _maxStableAreaWidths  = new List<double>();
        List<double> _minStableAreaWidths  = new List<double>();
        List<double> _meanStableAreaWidths  = new List<double>();
        List<double> _sumStableAreaWidths  = new List<double>();
        List<double> _varStableAreaWidths  = new List<double>();
        List<double> _stdDevStableAreaWidths  = new List<double>();
        List<double> _medianStableAreaWidths  = new List<double>();
        List<double> _numStableAreaWidthSamples  = new List<double>();

        //
        ///////////////////////////////////////////////////////



        List<Vector> _percWorkingRegions = new List<Vector>();
        List<double> _maxPercWorkingRegions  = new List<double>();
        List<double> _minPercWorkingRegions  = new List<double>();
        List<double> _meanPercWorkingRegions  = new List<double>();
        List<double> _sumPercWorkingRegions  = new List<double>();
        List<double> _varPercWorkingRegions  = new List<double>();
        List<double> _stdDevPercWorkingRegions  = new List<double>();
        List<double> _medianPercWorkingRegions  = new List<double>();
        List<double> _numPercWorkingRegionSamples  = new List<double>();

        //
        ///////////////////////////////////////////////////////

        ///////////////////////////////////////

        List<double> _middleLineRMSValues  = new List<double>();
        List<double> _middleLineSlopes  = new List<double>();

        ///////////////////////////////////////

        Vector _continuityValues = new Vector();
        double _maxContinuityValue;
        double _minContinuityValue;
        double _meanContinuityValues;
        double _sumContinuityValues;
        double _varContinuityValues;
        double _stdDevContinuityValues;
        double _medianContinuityValues;
        double _numContinuityValues;

        double _maxPrGap;

        //
        ///////////////////////////////////////

        //
        ///////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////
        ///
        //	private attributes
        //

        short _superModeNumber;

        int _numForwardModes;
        int _numReverseModes;
        List<ModeAnalysis> _forwardModes = new List<ModeAnalysis>();
        List<ModeAnalysis> _reverseModes = new List<ModeAnalysis>();

        int _numHysteresisModes;
        List<HysteresisAnalysis> _hysteresisAnalysis = new List<HysteresisAnalysis>();

        int _numMiddleLines;
        List<ModeBoundaryLine> _middleLines = new List<ModeBoundaryLine>();

        string _resultsDir;
        string _laserId;
        string _dateTimeStamp;

        int _xAxisLength;
        int _yAxisLength;

        short _numUpperLinesRemoved;
        short _numLowerLinesRemoved;
        short _numMiddleLinesRemoved;

        QAThresholds _qaThresholds = new QAThresholds();
        ///////////////////////////////////

        string _qaResultsAbsFilePath;
        string _passFailResultsAbsFilePath;

        ///////////////////////////////////

        bool _qaAnalysisComplete;
        bool _passFailAnalysisComplete;


        #endregion

        internal void createModeAnalysisVectors(List<ModeBoundaryLine> upperLines, List<ModeBoundaryLine> lowerLines)
        {
            // create a vector of forward modes and a vector of reverse modes

            for (int i = 0; i < _forwardModes.Count; i++)
            {
                _forwardModes[i].init();
            }
            _forwardModes.Clear();

            for (int i = 0; i < _reverseModes.Count; i++)
            {
                _reverseModes[i].init();
            }
            _reverseModes.Clear();

            if (lowerLines.Count > 0
             && lowerLines.Count == upperLines.Count)
            {
                // forward modes - the lower lines
                ModeAnalysis forwardMode = new ModeAnalysis();
                ModeAnalysis reverseMode = new ModeAnalysis();

                getQAThresholds();

                for (int i = 1; i < lowerLines.Count; i++)
                {
                    forwardMode.loadBoundaryLines(i, _xAxisLength, _yAxisLength, lowerLines[i], lowerLines[i - 1]);
                    //forwardMode.setQAThresholds(&_qaThresholds);
                    forwardMode.setQAThresholds(_qaThresholds);

                    _forwardModes.Add(forwardMode);

                    reverseMode.loadBoundaryLines(i, _xAxisLength, _yAxisLength, upperLines[i], upperLines[i - 1]);
                    reverseMode.setQAThresholds(_qaThresholds);

                    _reverseModes.Add(reverseMode);
                }
            }
        }

        internal void createHysteresisAnalysisVector(List<ModeAnalysis> forwardModes, List<ModeAnalysis> reverseModes)
        {
            int numModes = 0;

            for (int i = 0; i < _hysteresisAnalysis.Count; i++)
            {
                _hysteresisAnalysis[i].clear();
            }
            _hysteresisAnalysis.Clear();

            //GDM 31/10/06 need to get the QAAnalysis window for hysteresis
            getQAThresholds();

            if (forwardModes.Count > 0
             && reverseModes.Count == forwardModes.Count)
            {
                numModes = forwardModes.Count;

                HysteresisAnalysis hysteresisAnalysis = new HysteresisAnalysis();
                for (int i = 0; i < numModes; i++)
                {
                    ModeAnalysis forwardMode = forwardModes[i];
                    ModeAnalysis reverseMode = reverseModes[i];

                    //GDM 31/10/06 added  req for _qaThreshold for hysteresis analysis in  fwd and rev modes
                    //forwardMode.setQAThresholds(&_qaThresholds);
                    forwardMode.setQAThresholds(_qaThresholds);
                    reverseMode.setQAThresholds(_qaThresholds);
                    hysteresisAnalysis.setQAThresholds(_qaThresholds);

                    if (forwardMode._lowerBoundaryLine._x.Count > 0
                     && forwardMode._lowerBoundaryLine._y.Count == forwardMode._lowerBoundaryLine._x.Count
                     && reverseMode._lowerBoundaryLine._x.Count > 0
                     && reverseMode._lowerBoundaryLine._y.Count == reverseMode._lowerBoundaryLine._x.Count)
                    {
                        hysteresisAnalysis = new HysteresisAnalysis(forwardMode, reverseMode, _xAxisLength, _yAxisLength);
                        _hysteresisAnalysis.Add(hysteresisAnalysis);
                    }
                }
            }

            _numHysteresisModes = _hysteresisAnalysis.Count;
        }

        public void getQAThresholds()
        {
            _qaThresholds._slopeWindow = DSDBR01.Instance._DSDBR01_SMQA_slope_window_size;
            _qaThresholds._modalDistortionMinX = DSDBR01.Instance._DSDBR01_SMQA_modal_distortion_min_x;
            _qaThresholds._modalDistortionMaxX = DSDBR01.Instance._DSDBR01_SMQA_modal_distortion_max_x;
            _qaThresholds._modalDistortionMinY = DSDBR01.Instance._DSDBR01_SMQA_modal_distortion_min_y;
            //GDM 31/10/06 new members to hold hysteresis and mode width analysis windows
            _qaThresholds._hysteresisAnalysisMinX = DSDBR01.Instance._DSDBR01_SMQA_hysteresis_analysis_min_x;
            _qaThresholds._hysteresisAnalysisMaxX = DSDBR01.Instance._DSDBR01_SMQA_hysteresis_analysis_max_x;
            _qaThresholds._modeWidthAnalysisMinX = DSDBR01.Instance._DSDBR01_SMQA_mode_width_analysis_min_x;
            _qaThresholds._modeWidthAnalysisMaxX = DSDBR01.Instance._DSDBR01_SMQA_mode_width_analysis_max_x;
            //GDM 31/10/06 flag that this is the OM qa to the analysis sub
            _qaThresholds._SMMapQA = 1;
            //GDM
        }

        internal void init()
        {
            for (int i = 0; i < (int)_forwardModes.Count; i++)
                _forwardModes[i].init();
            _forwardModes.Clear();

            for (int i = 0; i < (int)_reverseModes.Count; i++)
                _reverseModes[i].init();
            _reverseModes.Clear();

            for (int i = 0; i < (int)_hysteresisAnalysis.Count; i++)
                _hysteresisAnalysis[i].clear();
            _hysteresisAnalysis.Clear();


            ////////////////////////////////////////////////
            //
            //	QA Data
            //
            _forwardModeWidths.Clear();
            _maxForwardModeWidths.Clear();
            _minForwardModeWidths.Clear();
            _meanForwardModeWidths.Clear();
            _sumForwardModeWidths.Clear();
            _varForwardModeWidths.Clear();
            _stdDevForwardModeWidths.Clear();
            _medianForwardModeWidths.Clear();
            _numForwardModeWidthSamples.Clear();

            ///////////////////////////////////////

            _forwardLowerModeSlopes.Clear();
            _maxForwardLowerModeSlopes.Clear();
            _minForwardLowerModeSlopes.Clear();
            _meanForwardLowerModeSlopes.Clear();
            _sumForwardLowerModeSlopes.Clear();
            _varForwardLowerModeSlopes.Clear();
            _stdDevForwardLowerModeSlopes.Clear();
            _medianForwardLowerModeSlopes.Clear();
            _numForwardLowerModeSlopeSamples.Clear();

            _forwardUpperModeSlopes.Clear();
            _maxForwardUpperModeSlopes.Clear();
            _minForwardUpperModeSlopes.Clear();
            _meanForwardUpperModeSlopes.Clear();
            _sumForwardUpperModeSlopes.Clear();
            _varForwardUpperModeSlopes.Clear();
            _stdDevForwardUpperModeSlopes.Clear();
            _medianForwardUpperModeSlopes.Clear();
            _numForwardUpperModeSlopeSamples.Clear();

            ///////////////////////////////////////

            _forwardLowerModalDistortionAngles.Clear();
            _forwardUpperModalDistortionAngles.Clear();

            ///////////////////////////////////////

            _forwardLowerModeBDCAreas.Clear();
            _maxForwardLowerModeBDCAreas.Clear();
            _minForwardLowerModeBDCAreas.Clear();
            _meanForwardLowerModeBDCAreas.Clear();
            _sumForwardLowerModeBDCAreas.Clear();
            _varForwardLowerModeBDCAreas.Clear();
            _stdDevForwardLowerModeBDCAreas.Clear();
            _medianForwardLowerModeBDCAreas.Clear();
            _numForwardLowerModeBDCAreaSamples.Clear();
            //
            _forwardUpperModeBDCAreas.Clear();
            _maxForwardUpperModeBDCAreas.Clear();
            _minForwardUpperModeBDCAreas.Clear();
            _meanForwardUpperModeBDCAreas.Clear();
            _sumForwardUpperModeBDCAreas.Clear();
            _varForwardUpperModeBDCAreas.Clear();
            _stdDevForwardUpperModeBDCAreas.Clear();
            _medianForwardUpperModeBDCAreas.Clear();
            _numForwardUpperModeBDCAreaSamples.Clear();

            ///////////////////////////////////////

            _forwardLowerModeBDCAreaXLengths.Clear();
            _maxForwardLowerModeBDCAreaXLengths.Clear();
            _minForwardLowerModeBDCAreaXLengths.Clear();
            _meanForwardLowerModeBDCAreaXLengths.Clear();
            _sumForwardLowerModeBDCAreaXLengths.Clear();
            _varForwardLowerModeBDCAreaXLengths.Clear();
            _stdDevForwardLowerModeBDCAreaXLengths.Clear();
            _medianForwardLowerModeBDCAreaXLengths.Clear();
            _numForwardLowerModeBDCAreaXLengthSamples.Clear();

            _forwardUpperModeBDCAreaXLengths.Clear();
            _maxForwardUpperModeBDCAreaXLengths.Clear();
            _minForwardUpperModeBDCAreaXLengths.Clear();
            _meanForwardUpperModeBDCAreaXLengths.Clear();
            _sumForwardUpperModeBDCAreaXLengths.Clear();
            _varForwardUpperModeBDCAreaXLengths.Clear();
            _stdDevForwardUpperModeBDCAreaXLengths.Clear();
            _medianForwardUpperModeBDCAreaXLengths.Clear();
            _numForwardUpperModeBDCAreaXLengthSamples.Clear();

            //
            ///////////////////////////////////////
            //

            _reverseModeWidths.Clear();
            _maxReverseModeWidths.Clear();
            _minReverseModeWidths.Clear();
            _meanReverseModeWidths.Clear();
            _sumReverseModeWidths.Clear();
            _varReverseModeWidths.Clear();
            _stdDevReverseModeWidths.Clear();
            _medianReverseModeWidths.Clear();
            _numReverseModeWidthSamples.Clear();

            ///////////////////////////////////////

            _reverseLowerModeSlopes.Clear();
            _maxReverseLowerModeSlopes.Clear();
            _minReverseLowerModeSlopes.Clear();
            _meanReverseLowerModeSlopes.Clear();
            _sumReverseLowerModeSlopes.Clear();
            _varReverseLowerModeSlopes.Clear();
            _stdDevReverseLowerModeSlopes.Clear();
            _medianReverseLowerModeSlopes.Clear();
            _numReverseLowerModeSlopeSamples.Clear();

            _reverseUpperModeSlopes.Clear();
            _maxReverseUpperModeSlopes.Clear();
            _minReverseUpperModeSlopes.Clear();
            _meanReverseUpperModeSlopes.Clear();
            _sumReverseUpperModeSlopes.Clear();
            _varReverseUpperModeSlopes.Clear();
            _stdDevReverseUpperModeSlopes.Clear();
            _medianReverseUpperModeSlopes.Clear();
            _numReverseUpperModeSlopeSamples.Clear();

            /////////////////////////////////////////////

            _reverseLowerModalDistortionAngles.Clear();
            _reverseUpperModalDistortionAngles.Clear();

            /////////////////////////////////////////////

            _reverseLowerModeBDCAreas.Clear();
            _maxReverseLowerModeBDCAreas.Clear();
            _minReverseLowerModeBDCAreas.Clear();
            _meanReverseLowerModeBDCAreas.Clear();
            _sumReverseLowerModeBDCAreas.Clear();
            _varReverseLowerModeBDCAreas.Clear();
            _stdDevReverseLowerModeBDCAreas.Clear();
            _medianReverseLowerModeBDCAreas.Clear();
            _numReverseLowerModeBDCAreaSamples.Clear();

            _reverseUpperModeBDCAreas.Clear();
            _maxReverseUpperModeBDCAreas.Clear();
            _minReverseUpperModeBDCAreas.Clear();
            _meanReverseUpperModeBDCAreas.Clear();
            _sumReverseUpperModeBDCAreas.Clear();
            _varReverseUpperModeBDCAreas.Clear();
            _stdDevReverseUpperModeBDCAreas.Clear();
            _medianReverseUpperModeBDCAreas.Clear();
            _numReverseUpperModeBDCAreaSamples.Clear();

            ///////////////////////////////////////

            _reverseLowerModeBDCAreaXLengths.Clear();
            _maxReverseLowerModeBDCAreaXLengths.Clear();
            _minReverseLowerModeBDCAreaXLengths.Clear();
            _meanReverseLowerModeBDCAreaXLengths.Clear();
            _sumReverseLowerModeBDCAreaXLengths.Clear();
            _varReverseLowerModeBDCAreaXLengths.Clear();
            _stdDevReverseLowerModeBDCAreaXLengths.Clear();
            _medianReverseLowerModeBDCAreaXLengths.Clear();
            _numReverseLowerModeBDCAreaXLengthSamples.Clear();

            _reverseUpperModeBDCAreaXLengths.Clear();
            _maxReverseUpperModeBDCAreaXLengths.Clear();
            _minReverseUpperModeBDCAreaXLengths.Clear();
            _meanReverseUpperModeBDCAreaXLengths.Clear();
            _sumReverseUpperModeBDCAreaXLengths.Clear();
            _varReverseUpperModeBDCAreaXLengths.Clear();
            _stdDevReverseUpperModeBDCAreaXLengths.Clear();
            _medianReverseUpperModeBDCAreaXLengths.Clear();
            _numReverseUpperModeBDCAreaXLengthSamples.Clear();

            ///////////////////////////////////////

            _stableAreaWidths.Clear();
            _maxStableAreaWidths.Clear();
            _minStableAreaWidths.Clear();
            _meanStableAreaWidths.Clear();
            _sumStableAreaWidths.Clear();
            _varStableAreaWidths.Clear();
            _stdDevStableAreaWidths.Clear();
            _medianStableAreaWidths.Clear();
            _numStableAreaWidthSamples.Clear();

            ///////////////////////////////////////

            _percWorkingRegions.Clear();
            _maxPercWorkingRegions.Clear();
            _minPercWorkingRegions.Clear();
            _meanPercWorkingRegions.Clear();
            _sumPercWorkingRegions.Clear();
            _varPercWorkingRegions.Clear();
            _stdDevPercWorkingRegions.Clear();
            _medianPercWorkingRegions.Clear();
            _numPercWorkingRegionSamples.Clear();

            ///////////////////////////////////////

            _middleLineRMSValues.Clear();
            _middleLineSlopes.Clear();

            ///////////////////////////////////////

            _continuityValues.Clear();
            _maxContinuityValue = 0;
            _minContinuityValue = 0;
            _meanContinuityValues = 0;
            _sumContinuityValues = 0;
            _varContinuityValues = 0;
            _stdDevContinuityValues = 0;
            _medianContinuityValues = 0;
            _numContinuityValues = 0;
            _maxPrGap = 0;

            ///////////////////////////////////////

            _overallPercStableArea = 0;

            ///////////////////////////////////////

            _qaAnalysisComplete = false;

            //
            //////////////////////////////////////////////////////

            //////////////////////////////////////////////////////
            ///
            //	Pass Fail Analysis
            //
            _passFailAnalysisComplete = false;

            _maxUpperModeBordersRemovedPFAnalysis.init();
            _maxLowerModeBordersRemovedPFAnalysis.init();
            _maxMiddleLineRemovedPFAnalysis.init();

            _maxForwardModeWidthPFAnalysis.init();
            _minForwardModeWidthPFAnalysis.init();

            _maxReverseModeWidthPFAnalysis.init();
            _minReverseModeWidthPFAnalysis.init();

            _meanPercWorkingRegionPFAnalysis.init();
            _minPercWorkingRegionPFAnalysis.init();

            _maxForwardLowerModeBDCAreaPFAnalysis.init();
            _maxForwardLowerModeBDCAreaXLengthPFAnalysis.init();
            _sumForwardLowerModeBDCAreasPFAnalysis.init();
            _numForwardLowerModeBDCAreasPFAnalysis.init();
            _maxForwardUpperModeBDCAreaPFAnalysis.init();
            _maxForwardUpperModeBDCAreaXLengthPFAnalysis.init();
            _sumForwardUpperModeBDCAreasPFAnalysis.init();
            _numForwardUpperModeBDCAreasPFAnalysis.init();

            _maxReverseLowerModeBDCAreaPFAnalysis.init();
            _maxReverseLowerModeBDCAreaXLengthPFAnalysis.init();
            _sumReverseLowerModeBDCAreasPFAnalysis.init();
            _numReverseLowerModeBDCAreasPFAnalysis.init();
            _maxReverseUpperModeBDCAreaPFAnalysis.init();
            _maxReverseUpperModeBDCAreaXLengthPFAnalysis.init();
            _sumReverseUpperModeBDCAreasPFAnalysis.init();
            _numReverseUpperModeBDCAreasPFAnalysis.init();

            _forwardLowerModalDistortionAnglePFAnalysis.init();
            _forwardLowerModalDistortionAngleXPositionPFAnalysis.init();
            _forwardUpperModalDistortionAnglePFAnalysis.init();
            _forwardUpperModalDistortionAngleXPositionPFAnalysis.init();

            _reverseLowerModalDistortionAnglePFAnalysis.init();
            _reverseLowerModalDistortionAngleXPositionPFAnalysis.init();
            _reverseUpperModalDistortionAnglePFAnalysis.init();
            _reverseUpperModalDistortionAngleXPositionPFAnalysis.init();

            _middleLineRMSValuesPFAnalysis.init();
            _maxMiddleLineSlopePFAnalysis.init();
            _minMiddleLineSlopePFAnalysis.init();
            _maxPrGapPFAnalysis.init();

            _maxForwardLowerModeSlopePFAnalysis.init();
            _maxForwardUpperModeSlopePFAnalysis.init();
            _maxReverseLowerModeSlopePFAnalysis.init();
            _maxReverseUpperModeSlopePFAnalysis.init();

            //
            //////////////////////////////////////////////////////

            _screeningPassed = false;

            _qaDetectedError = RESULTS_NO_ERROR;

        }

        string createQaResultsAbsFilePath(String resultsDir, String laserId, String dateTimeStamp)
        {
            String absFilePath = "";

            String superModeNumString = _superModeNumber.ToString();
            //superModeNumString.Format("%d", _superModeNumber);

            absFilePath += resultsDir;
            absFilePath += "\\";
            absFilePath += Defaults.QA_METRICS_FILE_NAME;
            absFilePath += Defaults.USCORE;
            absFilePath += Defaults.LASER_TYPE_ID_DSDBR01_CSTRING;
            absFilePath += Defaults.USCORE;
            absFilePath += laserId;
            absFilePath += Defaults.USCORE;
            absFilePath += dateTimeStamp;
            absFilePath += Defaults.USCORE;
            absFilePath += "SM";
            absFilePath += superModeNumString;
            absFilePath += Defaults.CSV;

            return absFilePath;
        }

        string createPassFailResultsAbsFilePath(String resultsDir, String laserId, String dateTimeStamp)
        {
            String absFilePath = "";
            String superModeNumString = _superModeNumber.ToString();
            //superModeNumString.Format("%d", _superModeNumber);

            absFilePath += resultsDir;
            absFilePath += "\\";
            absFilePath += Defaults.PASSFAIL_FILE_NAME;
            absFilePath += Defaults.USCORE;
            absFilePath += Defaults.LASER_TYPE_ID_DSDBR01_CSTRING;
            absFilePath += Defaults.USCORE;
            absFilePath += laserId;
            absFilePath += Defaults.USCORE;
            absFilePath += dateTimeStamp;
            absFilePath += Defaults.USCORE;
            absFilePath += "SM";
            absFilePath += superModeNumString;
            absFilePath += Defaults.CSV;

            return absFilePath;
        }

        //	 Public functions
        public rcode runAnalysis(short superModeNumber,
                                List<ModeBoundaryLine> upperLines,
                                List<ModeBoundaryLine> lowerLines,
                                List<ModeBoundaryLine> middleLines,
                                short numUpperLinesRemoved,
                                short numLowerLinesRemoved,
                                short numMiddleLinesRemoved,
                                int xAxisLength,
                                int yAxisLength,
                                List<double> continuityValues,
                                String resultsDir,
                                String laserId,
                                String dateTimeStamp)
        {
            _superModeNumber = superModeNumber;
            //mod GDM 23/10/06 _xAxisLength needs initiating before createModeAnalysisVectors else
            //they get created with random(?) huge 4-6million plus value for _xAxisLength, which
            //in turn screws up ModeAnalysis
            _xAxisLength = xAxisLength;
            _yAxisLength = yAxisLength;

            if (upperLines.Count != lowerLines.Count
             || upperLines.Count == 0)
            {
                return rcode.qaerror;
            }

            createModeAnalysisVectors(upperLines, lowerLines);
            createHysteresisAnalysisVector(_forwardModes, _reverseModes);


            _numForwardModes = _forwardModes.Count;
            _numReverseModes = _reverseModes.Count;

            _middleLines = middleLines;
            _numMiddleLines = _middleLines.Count;

            _qaResultsAbsFilePath = createQaResultsAbsFilePath(resultsDir,
                                                            laserId,
                                                            dateTimeStamp);

            _passFailResultsAbsFilePath = createPassFailResultsAbsFilePath(resultsDir,
                                                                    laserId,
                                                                    dateTimeStamp);

            _resultsDir = resultsDir;
            _laserId = laserId;
            _dateTimeStamp = dateTimeStamp;

            _xAxisLength = xAxisLength;
            _yAxisLength = yAxisLength;

            _continuityValues.op_Equal(continuityValues);

            _numUpperLinesRemoved = numUpperLinesRemoved;
            _numLowerLinesRemoved = numLowerLinesRemoved;
            _numMiddleLinesRemoved = numMiddleLinesRemoved;

            rcode err = rcode.ok;

            err = runQaAnalysis();
            if (err == rcode.ok)
                err = runPassFailAnalysis();

            err = writeQaResultsToFile();

            CLoseGridFile passFailResults = new CLoseGridFile(_passFailResultsAbsFilePath);
            passFailResults.createFolderAndFile();

            err = writePassFailResultsToFile(passFailResults);

            return err;
        }

        rcode runQaAnalysis()
        {
            rcode err = rcode.ok;

            if (err == rcode.ok)
                err = runForwardModesQaAnalysis();
            if (err == rcode.ok)
                err = runReverseModesQaAnalysis();
            if (err == rcode.ok)
                err = runHysteresisQaAnalysis();
            if (err == rcode.ok)
                err = runMiddleLinesQaAnalysis();

            ////////////////////////////////////////////////////////////////////////////////////

            String error_message = "";
            int error_number = RESULTS_NO_ERROR;
            if ((_numReverseModes < MIN_NUMBER_OF_MODES) || (_numForwardModes < MIN_NUMBER_OF_MODES))
                _qaDetectedError |= RESULTS_GENERAL_ERROR | RESULTS_INVALID_NUMBER_OF_LINES;
            if (!(_numUpperLinesRemoved == 0))
                _qaDetectedError |= RESULTS_GENERAL_ERROR | RESULTS_FORWARD_LINES_MISSING;
            if (!(_numLowerLinesRemoved == 0))
                _qaDetectedError |= RESULTS_GENERAL_ERROR | RESULTS_REVERSE_LINES_MISSING;


            getQAError(_qaDetectedError, out error_number,out error_message);

            if (_qaDetectedError > RESULTS_NO_ERROR)
                _screeningPassed = false;

            _qaAnalysisComplete = true;

            return err;
        }

        rcode runForwardModesQaAnalysis()
        {
            rcode err = rcode.ok;

            for (int i = 0; i < _numForwardModes; i++)
                _forwardModes[i].runAnalysis();

            getForwardModeWidths();
            getMaxForwardModeWidths();
            getMinForwardModeWidths();
            getMeanForwardModeWidths();
            getSumForwardModeWidths();
            getVarForwardModeWidths();
            getStdDevForwardModeWidths();
            getMedianForwardModeWidths();
            getNumForwardModeWidthSamples();

            getForwardModeSlopes();
            getMaxForwardModeSlopes();
            getMinForwardModeSlopes();
            getMeanForwardModeSlopes();
            getSumForwardModeSlopes();
            getVarForwardModeSlopes();
            getStdDevForwardModeSlopes();
            getMedianForwardModeSlopes();
            getNumForwardModeSlopeSamples();

            getForwardModalDistortionAngles();
            getForwardModalDistortionAngleXPositions();

            getForwardModeBDCAreas();
            getMaxForwardModeBDCAreas();
            getMinForwardModeBDCAreas();
            getMeanForwardModeBDCAreas();
            getSumForwardModeBDCAreas();
            getVarForwardModeBDCAreas();
            getStdDevForwardModeBDCAreas();
            getMedianForwardModeBDCAreas();
            getNumForwardModeBDCAreaSamples();


            getMaxForwardModeBDCAreaXLengths();

            //////////////////////////////////

            return err;
        }

        

        rcode runReverseModesQaAnalysis()
        {
            rcode err = rcode.ok;

            for (int i = 0; i < _numReverseModes; i++)
                _reverseModes[i].runAnalysis();

            getReverseModeWidths();
            getMaxReverseModeWidths();
            getMinReverseModeWidths();
            getMeanReverseModeWidths();
            getSumReverseModeWidths();
            getVarReverseModeWidths();
            getStdDevReverseModeWidths();
            getMedianReverseModeWidths();
            getNumReverseModeWidthSamples();

            getReverseModeSlopes();
            getMaxReverseModeSlopes();
            getMinReverseModeSlopes();
            getMeanReverseModeSlopes();
            getSumReverseModeSlopes();
            getVarReverseModeSlopes();
            getStdDevReverseModeSlopes();
            getMedianReverseModeSlopes();
            getNumReverseModeSlopeSamples();

            getReverseModeSlopes();
            getMaxReverseModeSlopes();
            getMinReverseModeSlopes();
            getMeanReverseModeSlopes();
            getSumReverseModeSlopes();
            getVarReverseModeSlopes();
            getStdDevReverseModeSlopes();
            getMedianReverseModeSlopes();
            getNumReverseModeSlopeSamples();

            getReverseModalDistortionAngles();
            getReverseModalDistortionAngleXPositions();

            getReverseModeBDCAreas();
            getMaxReverseModeBDCAreas();
            getMinReverseModeBDCAreas();
            getMeanReverseModeBDCAreas();
            getSumReverseModeBDCAreas();
            getVarReverseModeBDCAreas();
            getStdDevReverseModeBDCAreas();
            getMedianReverseModeBDCAreas();
            getNumReverseModeBDCAreaSamples();


            getMaxReverseModeBDCAreaXLengths();

            //////////////////////////////////

            return err;
        }

        rcode runHysteresisQaAnalysis()
        {
            rcode err = rcode.ok;

            for (int i = 0; i < (int)_hysteresisAnalysis.Count; i++)
            {
                _hysteresisAnalysis[i].setQAThresholds(_qaThresholds);
                _hysteresisAnalysis[i].runAnalysis();
            }

            getStableAreaWidths();
            getMaxStableAreaWidths();
            getMinStableAreaWidths();
            getMeanStableAreaWidths();
            getSumStableAreaWidths();
            getVarStableAreaWidths();
            getStdDevStableAreaWidths();
            getMedianStableAreaWidths();
            getNumStableAreaWidthSamples();

            getPercWorkingRegions();
            getMaxPercWorkingRegions();
            getMinPercWorkingRegions();
            getMeanPercWorkingRegions();
            getSumPercWorkingRegions();
            getVarPercWorkingRegions();
            getStdDevPercWorkingRegions();
            getMedianPercWorkingRegions();
            getNumPercWorkingRegionSamples();


            /////////////////////////////////////////////////////////////////////
            ///
            // get overallPercStableArea
            //
            double totalSumStableAreaWidths = 0;
            for (int i = 0; i < (int)_sumStableAreaWidths.Count; i++)
            {
                totalSumStableAreaWidths = totalSumStableAreaWidths + _sumStableAreaWidths[i];
            }

            _overallPercStableArea = (totalSumStableAreaWidths / (_xAxisLength * _yAxisLength)) * 100;

            //
            /////////////////////////////////////////////////////////////////////
            return err;
        }

        rcode runMiddleLinesQaAnalysis()
        {
            rcode err = rcode.ok;

            getMiddleLineRMSValues();

            /////////////////////////////////

            getMaxContinuityValue();
            getMinContinuityValue();
            getMeanContinuityValues();
            getSumContinuityValues();
            getVarContinuityValues();
            getStdDevContinuityValues();
            getMedianContinuityValues();
            getNumContinuityValues();
            getMaxContinuityValueSpacing();

            /////////////////////////////////////

            return err;
        }

        rcode runPassFailAnalysis()
        {
            rcode err = rcode.ok;

            if (_qaAnalysisComplete)
            {
                _screeningPassed = true;

                //////////////////////////////////////////////////////
                ///
                // get the registry values based on the supermode number
                //
                _lmPassFailThresholds = getPassFailThresholds();

                /////////////////////////////////////////////////////////////////////////////
                ///
                // Set thresholds for all modes
                _maxUpperModeBordersRemovedPFAnalysis.threshold = _lmPassFailThresholds._maxModeBordersRemoved;
                _maxLowerModeBordersRemovedPFAnalysis.threshold = _lmPassFailThresholds._maxModeBordersRemoved;
                _maxMiddleLineRemovedPFAnalysis.threshold = _lmPassFailThresholds._maxModeBordersRemoved;

                _maxForwardModeWidthPFAnalysis.threshold = _lmPassFailThresholds._maxModeWidth;
                _maxReverseModeWidthPFAnalysis.threshold = _lmPassFailThresholds._maxModeWidth;

                _minForwardModeWidthPFAnalysis.threshold = _lmPassFailThresholds._minModeWidth;
                _minReverseModeWidthPFAnalysis.threshold = _lmPassFailThresholds._minModeWidth;

                _meanPercWorkingRegionPFAnalysis.threshold = _lmPassFailThresholds._meanPercWorkingRegion;
                _minPercWorkingRegionPFAnalysis.threshold = _lmPassFailThresholds._minPercWorkingRegion;

                _maxForwardLowerModeBDCAreaPFAnalysis.threshold = _lmPassFailThresholds._maxModeBDCArea;
                _maxForwardLowerModeBDCAreaXLengthPFAnalysis.threshold = _lmPassFailThresholds._maxModeBDCAreaXLength;
                _sumForwardLowerModeBDCAreasPFAnalysis.threshold = _lmPassFailThresholds._sumModeBDCAreas;
                _numForwardLowerModeBDCAreasPFAnalysis.threshold = _lmPassFailThresholds._numModeBDCAreas;
                _maxForwardUpperModeBDCAreaPFAnalysis.threshold = _lmPassFailThresholds._maxModeBDCArea;
                _maxForwardUpperModeBDCAreaXLengthPFAnalysis.threshold = _lmPassFailThresholds._maxModeBDCAreaXLength;
                _sumForwardUpperModeBDCAreasPFAnalysis.threshold = _lmPassFailThresholds._sumModeBDCAreas;
                _numForwardUpperModeBDCAreasPFAnalysis.threshold = _lmPassFailThresholds._numModeBDCAreas;

                _maxReverseLowerModeBDCAreaPFAnalysis.threshold = _lmPassFailThresholds._maxModeBDCArea;
                _maxReverseLowerModeBDCAreaXLengthPFAnalysis.threshold = _lmPassFailThresholds._maxModeBDCAreaXLength;
                _sumReverseLowerModeBDCAreasPFAnalysis.threshold = _lmPassFailThresholds._sumModeBDCAreas;
                _numReverseLowerModeBDCAreasPFAnalysis.threshold = _lmPassFailThresholds._numModeBDCAreas;
                _maxReverseUpperModeBDCAreaPFAnalysis.threshold = _lmPassFailThresholds._maxModeBDCArea;
                _maxReverseUpperModeBDCAreaXLengthPFAnalysis.threshold = _lmPassFailThresholds._maxModeBDCAreaXLength;
                _sumReverseUpperModeBDCAreasPFAnalysis.threshold = _lmPassFailThresholds._sumModeBDCAreas;
                _numReverseUpperModeBDCAreasPFAnalysis.threshold = _lmPassFailThresholds._numModeBDCAreas;

                _forwardLowerModalDistortionAnglePFAnalysis.threshold = _lmPassFailThresholds._modalDistortionAngle;
                _forwardLowerModalDistortionAngleXPositionPFAnalysis.threshold = _lmPassFailThresholds._modalDistortionAngleXPosition;
                _forwardUpperModalDistortionAnglePFAnalysis.threshold = _lmPassFailThresholds._modalDistortionAngle;
                _forwardUpperModalDistortionAngleXPositionPFAnalysis.threshold = _lmPassFailThresholds._modalDistortionAngleXPosition;

                _reverseLowerModalDistortionAnglePFAnalysis.threshold = _lmPassFailThresholds._modalDistortionAngle;
                _reverseLowerModalDistortionAngleXPositionPFAnalysis.threshold = _lmPassFailThresholds._modalDistortionAngleXPosition;
                _reverseUpperModalDistortionAnglePFAnalysis.threshold = _lmPassFailThresholds._modalDistortionAngle;
                _reverseUpperModalDistortionAngleXPositionPFAnalysis.threshold = _lmPassFailThresholds._modalDistortionAngleXPosition;

                _middleLineRMSValuesPFAnalysis.threshold = _lmPassFailThresholds._middleLineRMSValue;
                _maxMiddleLineSlopePFAnalysis.threshold = _lmPassFailThresholds._maxMiddleLineSlope;
                _minMiddleLineSlopePFAnalysis.threshold = _lmPassFailThresholds._minMiddleLineSlope;

                _maxPrGapPFAnalysis.threshold = _lmPassFailThresholds._maxPrGap;

                _maxForwardLowerModeSlopePFAnalysis.threshold = _lmPassFailThresholds._maxModeLineSlope;
                _maxForwardUpperModeSlopePFAnalysis.threshold = _lmPassFailThresholds._maxModeLineSlope;
                _maxReverseLowerModeSlopePFAnalysis.threshold = _lmPassFailThresholds._maxModeLineSlope;
                _maxReverseUpperModeSlopePFAnalysis.threshold = _lmPassFailThresholds._maxModeLineSlope;



                // find out which lines crossed front section pairs
                // and ignore failures at these locations

                List<bool> forward_mode_has_front_section_change = new List<bool>();
                List<bool> upper_line_of_forward_mode_has_front_section_change = new List<bool>();
                List<bool> lower_line_of_forward_mode_has_front_section_change = new List<bool>();

                for (int i = 0; i < (int)_forwardModes.Count; i++)
                {
                    // find out which modes have lines crossing front sections
                    bool upper_has_change = _forwardModes[i]._upperBoundaryLine._crosses_front_sections;
                    upper_line_of_forward_mode_has_front_section_change.Add(upper_has_change);

                    bool lower_has_change = _forwardModes[i]._lowerBoundaryLine._crosses_front_sections;
                    lower_line_of_forward_mode_has_front_section_change.Add(lower_has_change);

                    if (upper_has_change || lower_has_change)
                        forward_mode_has_front_section_change.Add(true);
                    else
                        forward_mode_has_front_section_change.Add(false);
                }

                List<bool> reverse_mode_has_front_section_change = new List<bool>();
                List<bool> upper_line_of_reverse_mode_has_front_section_change = new List<bool>();
                List<bool> lower_line_of_reverse_mode_has_front_section_change = new List<bool>();

                for (int i = 0; i < (int)_reverseModes.Count; i++)
                {
                    // find out which modes have lines crossing front sections
                    bool upper_has_change = _reverseModes[i]._upperBoundaryLine._crosses_front_sections;
                    upper_line_of_reverse_mode_has_front_section_change.Add(upper_has_change);

                    bool lower_has_change = _reverseModes[i]._lowerBoundaryLine._crosses_front_sections;
                    lower_line_of_reverse_mode_has_front_section_change.Add(lower_has_change);

                    if (upper_has_change || lower_has_change)
                        reverse_mode_has_front_section_change.Add(true);
                    else
                        reverse_mode_has_front_section_change.Add(false);
                }




                // run pass fail
                if (_numLowerLinesRemoved > _maxLowerModeBordersRemovedPFAnalysis.threshold)
                    _maxLowerModeBordersRemovedPFAnalysis.pass = false;
                else
                    _maxLowerModeBordersRemovedPFAnalysis.pass = true;
                if (_numUpperLinesRemoved > _maxUpperModeBordersRemovedPFAnalysis.threshold)
                    _maxUpperModeBordersRemovedPFAnalysis.pass = false;
                else
                    _maxUpperModeBordersRemovedPFAnalysis.pass = true;
                if (_numMiddleLinesRemoved > _maxMiddleLineRemovedPFAnalysis.threshold)
                    _maxMiddleLineRemovedPFAnalysis.pass = false;
                else
                    _maxMiddleLineRemovedPFAnalysis.pass = true;


                _maxForwardModeWidthPFAnalysis.runPassFailWithUpperLimit(_maxForwardModeWidths);
                _maxReverseModeWidthPFAnalysis.runPassFailWithUpperLimit(_maxReverseModeWidths);
                _minForwardModeWidthPFAnalysis.runPassFailWithLowerLimit(_minForwardModeWidths);
                _minReverseModeWidthPFAnalysis.runPassFailWithLowerLimit(_minReverseModeWidths);

                if (DSDBR01.Instance._DSDBR01_SMQA_ignore_DBC_at_FSC)
                {
                    _maxForwardLowerModeBDCAreaPFAnalysis.runPassFailWithUpperLimit(_maxForwardLowerModeBDCAreas, lower_line_of_forward_mode_has_front_section_change);
                    _maxForwardLowerModeBDCAreaXLengthPFAnalysis.runPassFailWithUpperLimit(_maxForwardLowerModeBDCAreaXLengths, lower_line_of_forward_mode_has_front_section_change);
                    _maxReverseLowerModeBDCAreaPFAnalysis.runPassFailWithUpperLimit(_maxReverseLowerModeBDCAreas, lower_line_of_reverse_mode_has_front_section_change);
                    _maxReverseLowerModeBDCAreaXLengthPFAnalysis.runPassFailWithUpperLimit(_maxReverseLowerModeBDCAreaXLengths, lower_line_of_reverse_mode_has_front_section_change);
                    _maxForwardUpperModeBDCAreaPFAnalysis.runPassFailWithUpperLimit(_maxForwardUpperModeBDCAreas, upper_line_of_forward_mode_has_front_section_change);
                    _maxForwardUpperModeBDCAreaXLengthPFAnalysis.runPassFailWithUpperLimit(_maxForwardUpperModeBDCAreaXLengths, upper_line_of_forward_mode_has_front_section_change);
                    _maxReverseUpperModeBDCAreaPFAnalysis.runPassFailWithUpperLimit(_maxReverseUpperModeBDCAreas, upper_line_of_reverse_mode_has_front_section_change);
                    _maxReverseUpperModeBDCAreaXLengthPFAnalysis.runPassFailWithUpperLimit(_maxReverseUpperModeBDCAreaXLengths, upper_line_of_reverse_mode_has_front_section_change);

                    _sumForwardLowerModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_sumForwardLowerModeBDCAreas, lower_line_of_forward_mode_has_front_section_change);
                    _numForwardLowerModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_numForwardLowerModeBDCAreaSamples, lower_line_of_forward_mode_has_front_section_change);
                    _sumReverseLowerModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_sumReverseLowerModeBDCAreas, lower_line_of_reverse_mode_has_front_section_change);
                    _numReverseLowerModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_numReverseLowerModeBDCAreaSamples, lower_line_of_reverse_mode_has_front_section_change);
                    _sumForwardUpperModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_sumForwardUpperModeBDCAreas, upper_line_of_forward_mode_has_front_section_change);
                    _numForwardUpperModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_numForwardUpperModeBDCAreaSamples, upper_line_of_forward_mode_has_front_section_change);
                    _sumReverseUpperModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_sumReverseUpperModeBDCAreas, upper_line_of_reverse_mode_has_front_section_change);
                    _numReverseUpperModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_numReverseUpperModeBDCAreaSamples, upper_line_of_reverse_mode_has_front_section_change);
                }
                else
                {
                    _maxForwardLowerModeBDCAreaPFAnalysis.runPassFailWithUpperLimit(_maxForwardLowerModeBDCAreas);
                    _maxForwardLowerModeBDCAreaXLengthPFAnalysis.runPassFailWithUpperLimit(_maxForwardLowerModeBDCAreaXLengths);
                    _maxReverseLowerModeBDCAreaPFAnalysis.runPassFailWithUpperLimit(_maxReverseLowerModeBDCAreas);
                    _maxReverseLowerModeBDCAreaXLengthPFAnalysis.runPassFailWithUpperLimit(_maxReverseLowerModeBDCAreaXLengths);
                    _maxForwardUpperModeBDCAreaPFAnalysis.runPassFailWithUpperLimit(_maxForwardUpperModeBDCAreas);
                    _maxForwardUpperModeBDCAreaXLengthPFAnalysis.runPassFailWithUpperLimit(_maxForwardUpperModeBDCAreaXLengths);
                    _maxReverseUpperModeBDCAreaPFAnalysis.runPassFailWithUpperLimit(_maxReverseUpperModeBDCAreas);
                    _maxReverseUpperModeBDCAreaXLengthPFAnalysis.runPassFailWithUpperLimit(_maxReverseUpperModeBDCAreaXLengths);

                    _sumForwardLowerModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_sumForwardLowerModeBDCAreas);
                    _numForwardLowerModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_numForwardLowerModeBDCAreaSamples);
                    _sumReverseLowerModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_sumReverseLowerModeBDCAreas);
                    _numReverseLowerModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_numReverseLowerModeBDCAreaSamples);
                    _sumForwardUpperModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_sumForwardUpperModeBDCAreas);
                    _numForwardUpperModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_numForwardUpperModeBDCAreaSamples);
                    _sumReverseUpperModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_sumReverseUpperModeBDCAreas);
                    _numReverseUpperModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_numReverseUpperModeBDCAreaSamples);
                }

                _forwardLowerModalDistortionAnglePFAnalysis.runPassFailWithUpperLimit(_forwardLowerModalDistortionAngles);
                _forwardLowerModalDistortionAngleXPositionPFAnalysis.runPassFailWithUpperLimit(_forwardLowerModalDistortionAngleXPositions);
                _forwardUpperModalDistortionAnglePFAnalysis.runPassFailWithUpperLimit(_forwardUpperModalDistortionAngles);
                _forwardUpperModalDistortionAngleXPositionPFAnalysis.runPassFailWithUpperLimit(_forwardUpperModalDistortionAngleXPositions);

                _reverseLowerModalDistortionAnglePFAnalysis.runPassFailWithUpperLimit(_reverseLowerModalDistortionAngles);
                _reverseLowerModalDistortionAngleXPositionPFAnalysis.runPassFailWithUpperLimit(_reverseLowerModalDistortionAngleXPositions);
                _reverseUpperModalDistortionAnglePFAnalysis.runPassFailWithUpperLimit(_reverseUpperModalDistortionAngles);
                _reverseUpperModalDistortionAngleXPositionPFAnalysis.runPassFailWithUpperLimit(_reverseUpperModalDistortionAngleXPositions);


                _meanPercWorkingRegionPFAnalysis.runPassFailWithLowerLimit(_meanPercWorkingRegions);
                _minPercWorkingRegionPFAnalysis.runPassFailWithLowerLimit(_minPercWorkingRegions);

                _middleLineRMSValuesPFAnalysis.runPassFailWithUpperLimit(_middleLineRMSValues);
                _maxMiddleLineSlopePFAnalysis.runPassFailWithUpperLimit(_middleLineSlopes);
                _minMiddleLineSlopePFAnalysis.runPassFailWithLowerLimit(_middleLineSlopes);

                _maxPrGapPFAnalysis.runPassFailWithUpperLimit(_continuityValues.Values);

                _maxForwardLowerModeSlopePFAnalysis.runPassFailWithUpperLimit(_maxForwardLowerModeSlopes);
                _maxForwardUpperModeSlopePFAnalysis.runPassFailWithUpperLimit(_maxForwardUpperModeSlopes);
                _maxReverseLowerModeSlopePFAnalysis.runPassFailWithUpperLimit(_maxReverseLowerModeSlopes);
                _maxReverseUpperModeSlopePFAnalysis.runPassFailWithUpperLimit(_maxReverseUpperModeSlopes);

                if (!(_maxLowerModeBordersRemovedPFAnalysis.pass) ||
                    !(_maxUpperModeBordersRemovedPFAnalysis.pass) ||
                    !(_maxMiddleLineRemovedPFAnalysis.pass) ||
                    !(_maxForwardModeWidthPFAnalysis.pass) ||
                    !(_maxReverseModeWidthPFAnalysis.pass) ||
                    !(_minForwardModeWidthPFAnalysis.pass) ||
                    !(_minReverseModeWidthPFAnalysis.pass) ||
                    !(_maxForwardLowerModeBDCAreaPFAnalysis.pass) ||
                    !(_maxForwardLowerModeBDCAreaXLengthPFAnalysis.pass) ||
                    !(_maxForwardUpperModeBDCAreaPFAnalysis.pass) ||
                    !(_maxForwardUpperModeBDCAreaXLengthPFAnalysis.pass) ||
                    !(_maxReverseLowerModeBDCAreaPFAnalysis.pass) ||
                    !(_maxReverseLowerModeBDCAreaXLengthPFAnalysis.pass) ||
                    !(_maxReverseUpperModeBDCAreaPFAnalysis.pass) ||
                    !(_maxReverseUpperModeBDCAreaXLengthPFAnalysis.pass) ||
                    !(_sumForwardLowerModeBDCAreasPFAnalysis.pass) ||
                    !(_numForwardLowerModeBDCAreasPFAnalysis.pass) ||
                    !(_sumForwardUpperModeBDCAreasPFAnalysis.pass) ||
                    !(_numForwardUpperModeBDCAreasPFAnalysis.pass) ||
                    !(_sumReverseLowerModeBDCAreasPFAnalysis.pass) ||
                    !(_numReverseLowerModeBDCAreasPFAnalysis.pass) ||
                    !(_sumReverseUpperModeBDCAreasPFAnalysis.pass) ||
                    !(_numReverseUpperModeBDCAreasPFAnalysis.pass) ||
                    !(_forwardLowerModalDistortionAnglePFAnalysis.pass) ||
                    !(_forwardLowerModalDistortionAngleXPositionPFAnalysis.pass) ||
                    !(_forwardUpperModalDistortionAnglePFAnalysis.pass) ||
                    !(_forwardUpperModalDistortionAngleXPositionPFAnalysis.pass) ||
                    !(_reverseLowerModalDistortionAnglePFAnalysis.pass) ||
                    !(_reverseLowerModalDistortionAngleXPositionPFAnalysis.pass) ||
                    !(_reverseUpperModalDistortionAnglePFAnalysis.pass) ||
                    !(_reverseUpperModalDistortionAngleXPositionPFAnalysis.pass) ||
                    !(_middleLineRMSValuesPFAnalysis.pass) ||
                    !(_meanPercWorkingRegionPFAnalysis.pass) ||
                    !(_minPercWorkingRegionPFAnalysis.pass) ||
                    !(_maxMiddleLineSlopePFAnalysis.pass) ||
                    !(_minMiddleLineSlopePFAnalysis.pass) ||
                    !(_maxForwardLowerModeSlopePFAnalysis.pass) ||
                    !(_maxForwardUpperModeSlopePFAnalysis.pass) ||
                    !(_maxReverseLowerModeSlopePFAnalysis.pass) ||
                    !(_maxReverseUpperModeSlopePFAnalysis.pass) ||
                    !(_maxPrGapPFAnalysis.pass))
                {
                    _screeningPassed = false;
                }

            }

            //////////////////////////////////////////////////////////////////////////////////
            //
            // analyse results
            //
            if (!(_numUpperLinesRemoved == 0))
                _qaDetectedError |= RESULTS_GENERAL_ERROR | RESULTS_FORWARD_LINES_MISSING;
            if (!(_numLowerLinesRemoved == 0))
                _qaDetectedError |= RESULTS_GENERAL_ERROR | RESULTS_REVERSE_LINES_MISSING;
            if (_numReverseModes < MIN_NUMBER_OF_MODES)
                _qaDetectedError |= RESULTS_GENERAL_ERROR | RESULTS_INVALID_NUMBER_OF_LINES;
            if (_numForwardModes < MIN_NUMBER_OF_MODES)
                _qaDetectedError |= RESULTS_GENERAL_ERROR | RESULTS_INVALID_NUMBER_OF_LINES;

            if (_qaDetectedError > RESULTS_NO_ERROR)
                _screeningPassed = false;
            //
            ////////////////////////////////////////////////////////////////////////////////////

            _passFailAnalysisComplete = true;

            return err;
        }

        void getQAError(int qaError, out int errorNumber, out String errorMsg)
        {
            StringBuilder errorMsgBuilder = new StringBuilder();
            errorNumber = 0;

            if ((qaError & RESULTS_GENERAL_ERROR) == RESULTS_GENERAL_ERROR)
            {
                errorMsgBuilder.Append("Error(s) detected. ");
                errorNumber |= RESULTS_GENERAL_ERROR;
            }
            if ((qaError & RESULTS_NO_MODEMAP_DATA) == RESULTS_NO_MODEMAP_DATA)
            {
                errorMsgBuilder.Append("Mode map data not available. ");
                errorNumber |= RESULTS_NO_MODEMAP_DATA;
            }
            if ((qaError & RESULTS_PO_TOO_LOW)==RESULTS_PO_TOO_LOW)
            {
                errorMsgBuilder.Append("Invalid laser screening run - optical power is too low. ");
                errorNumber |= RESULTS_PO_TOO_LOW;
            }
            if ((qaError & RESULTS_PO_TOO_HIGH)==RESULTS_PO_TOO_HIGH)
            {
                errorMsgBuilder.Append("Invalid laser screening run - optical power is too high. ");
                errorNumber |= RESULTS_PO_TOO_HIGH;
            }
            if ((qaError & RESULTS_TOO_MANY_JUMPS)==RESULTS_TOO_MANY_JUMPS)
            {
                errorMsgBuilder.Append("Invalid laser screening run - too many laser mode jumps found. ");
                errorNumber |= RESULTS_TOO_MANY_JUMPS;
            }
            if ((qaError & RESULTS_TOO_FEW_JUMPS)==RESULTS_TOO_FEW_JUMPS)
            {
                errorMsgBuilder.Append("Invalid laser screening run - too few laser mode jumps found. ");
                errorNumber |= RESULTS_TOO_FEW_JUMPS;
            }
            if ((qaError & RESULTS_INVALID_NUMBER_OF_LINES)==RESULTS_INVALID_NUMBER_OF_LINES)
            {
                errorMsgBuilder.Append("Invalid laser screening run - invalid number of mode boundaries detected. ");
                errorNumber |= RESULTS_INVALID_NUMBER_OF_LINES;
            }
            if ((qaError & RESULTS_INVALID_LINES_REMOVED)==RESULTS_INVALID_LINES_REMOVED)
            {
                errorMsgBuilder.Append("Invalid mode boundaries detected. ");
                errorNumber |= RESULTS_INVALID_LINES_REMOVED;
            }
            if ((qaError & RESULTS_FORWARD_LINES_MISSING)==RESULTS_FORWARD_LINES_MISSING)
            {
                errorMsgBuilder.Append("Undetected forward mode boundaries. ");
                errorNumber |= RESULTS_FORWARD_LINES_MISSING;
            }
            if ((qaError & RESULTS_REVERSE_LINES_MISSING)==RESULTS_REVERSE_LINES_MISSING)
            {
                errorMsgBuilder.Append("Undetected reverse mode boundaries. ");
                errorNumber |= RESULTS_REVERSE_LINES_MISSING;
            }

            String errNumMsg;
            if (errorNumber > RESULTS_NO_ERROR)
                errNumMsg = string.Format("{0:d},", errorNumber);
            else
                errNumMsg = ",";
            errorMsgBuilder.Insert(0, errNumMsg);

            errorMsg = errorMsgBuilder.ToString();

        }

        /////////////////////////////////////////////////////////////////////////////
        //
        //	Data Gathering Section - gather analysis data of the supermodes
        //
        //
        /////////////////////////////////////////////////////////////////////////////
        //
        //	Forward Mode Widths
        void getForwardModeWidths()
        {
            if (_forwardModeWidths.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    Vector forwardModeWidths;
                    _forwardModes[i].getModeWidths(out forwardModeWidths);

                    _forwardModeWidths.Add(forwardModeWidths);
                }
            }
        }

        void getMaxForwardModeWidths()
        {
            if (_maxForwardModeWidths.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                    _maxForwardModeWidths.Add(_forwardModes[i].getModeWidthsStatistic(Vector.VectorAttribute.max));
            }
        }


        void getMinForwardModeWidths()
        {
            if (_minForwardModeWidths.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                    _minForwardModeWidths.Add(_forwardModes[i].getModeWidthsStatistic(Vector.VectorAttribute.min));
            }
        }


        void getMeanForwardModeWidths()
        {
            if (_meanForwardModeWidths.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                    _meanForwardModeWidths.Add(_forwardModes[i].getModeWidthsStatistic(Vector.VectorAttribute.mean));
            }
        }


        void getSumForwardModeWidths()
        {
            if (_sumForwardModeWidths.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                    _sumForwardModeWidths.Add(_forwardModes[i].getModeWidthsStatistic(Vector.VectorAttribute.sum));
            }
        }


        void getVarForwardModeWidths()
        {
            if (_varForwardModeWidths.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                    _varForwardModeWidths.Add(_forwardModes[i].getModeWidthsStatistic(Vector.VectorAttribute.variance));
            }
        }


        void getStdDevForwardModeWidths()
        {
            if (_stdDevForwardModeWidths.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                    _stdDevForwardModeWidths.Add(_forwardModes[i].getModeWidthsStatistic(Vector.VectorAttribute.stdDev));
            }
        }


        void getMedianForwardModeWidths()
        {
            if (_medianForwardModeWidths.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                    _medianForwardModeWidths.Add(_forwardModes[i].getModeWidthsStatistic(Vector.VectorAttribute.median));
            }
        }


        void getNumForwardModeWidthSamples()
        {
            if (_numForwardModeWidthSamples.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                    _numForwardModeWidthSamples.Add(_forwardModes[i].getModeWidthsStatistic(Vector.VectorAttribute.count));
            }
        }


        //
        /////////////////////////////////////////////////////////////////////////////
        //
        //	Forward Mode Slopes
        //
        void getForwardModeSlopes()
        {
            if (_forwardLowerModeSlopes.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    Vector forwardLowerModeSlopes;
                    Vector forwardUpperModeSlopes;
                    _forwardModes[i].getModeSlopes(out forwardLowerModeSlopes, out forwardUpperModeSlopes);

                    _forwardLowerModeSlopes.Add(forwardLowerModeSlopes);
                    _forwardUpperModeSlopes.Add(forwardUpperModeSlopes);
                }
            }
        }


        void getMaxForwardModeSlopes()
        {
            if (_maxForwardLowerModeSlopes.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _maxForwardLowerModeSlopes.Add(_forwardModes[i].getModeLowerSlopesStatistic(Vector.VectorAttribute.max));
                    _maxForwardUpperModeSlopes.Add(_forwardModes[i].getModeUpperSlopesStatistic(Vector.VectorAttribute.max));
                }
            }
        }


        void getMinForwardModeSlopes()
        {
            if (_minForwardLowerModeSlopes.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _minForwardLowerModeSlopes.Add(_forwardModes[i].getModeLowerSlopesStatistic(Vector.VectorAttribute.min));
                    _minForwardUpperModeSlopes.Add(_forwardModes[i].getModeUpperSlopesStatistic(Vector.VectorAttribute.min));
                }
            }
        }


        void getMeanForwardModeSlopes()
        {
            if (_meanForwardLowerModeSlopes.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _meanForwardLowerModeSlopes.Add(_forwardModes[i].getModeLowerSlopesStatistic(Vector.VectorAttribute.mean));
                    _meanForwardUpperModeSlopes.Add(_forwardModes[i].getModeUpperSlopesStatistic(Vector.VectorAttribute.mean));
                }
            }
        }


        void getSumForwardModeSlopes()
        {
            if (_sumForwardLowerModeSlopes.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _sumForwardLowerModeSlopes.Add(_forwardModes[i].getModeLowerSlopesStatistic(Vector.VectorAttribute.sum));
                    _sumForwardUpperModeSlopes.Add(_forwardModes[i].getModeUpperSlopesStatistic(Vector.VectorAttribute.sum));
                }
            }
        }


        void getVarForwardModeSlopes()
        {
            if (_varForwardLowerModeSlopes.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _varForwardLowerModeSlopes.Add(_forwardModes[i].getModeLowerSlopesStatistic(Vector.VectorAttribute.variance));
                    _varForwardUpperModeSlopes.Add(_forwardModes[i].getModeUpperSlopesStatistic(Vector.VectorAttribute.variance));
                }
            }
        }


        void getStdDevForwardModeSlopes()
        {
            if (_stdDevForwardLowerModeSlopes.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _stdDevForwardLowerModeSlopes.Add(_forwardModes[i].getModeLowerSlopesStatistic(Vector.VectorAttribute.stdDev));
                    _stdDevForwardUpperModeSlopes.Add(_forwardModes[i].getModeUpperSlopesStatistic(Vector.VectorAttribute.stdDev));
                }
            }
        }


        void getMedianForwardModeSlopes()
        {
            if (_medianForwardLowerModeSlopes.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _medianForwardLowerModeSlopes.Add(_forwardModes[i].getModeLowerSlopesStatistic(Vector.VectorAttribute.median));
                    _medianForwardUpperModeSlopes.Add(_forwardModes[i].getModeUpperSlopesStatistic(Vector.VectorAttribute.median));
                }
            }
        }


        void getNumForwardModeSlopeSamples()
        {
            if (_numForwardLowerModeSlopeSamples.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _numForwardLowerModeSlopeSamples.Add(_forwardModes[i].getModeLowerSlopesStatistic(Vector.VectorAttribute.count));
                    _numForwardUpperModeSlopeSamples.Add(_forwardModes[i].getModeUpperSlopesStatistic(Vector.VectorAttribute.count));
                }
            }
        }


        //
        /////////////////////////////////////////////////////////////////////////////
        ///
        //	Stable Area Widths
        //
        void getStableAreaWidths()
        {
            if (_stableAreaWidths.Count == 0)
            {
                for (int i = 0; i < _hysteresisAnalysis.Count; i++)
                {
                    Vector stableAreaWidths;
                    _hysteresisAnalysis[i].getStableAreaWidths(out stableAreaWidths);

                    _stableAreaWidths.Add(stableAreaWidths);
                }
            }
        }


        void getMaxStableAreaWidths()
        {
            if (_maxStableAreaWidths.Count == 0)
            {
                for (int i = 0; i < _hysteresisAnalysis.Count; i++)
                    _maxStableAreaWidths.Add(_hysteresisAnalysis[i].getStableAreaWidthsStatistic(Vector.VectorAttribute.max));
            }
        }


        void getMinStableAreaWidths()
        {
            if (_minStableAreaWidths.Count == 0)
            {
                for (int i = 0; i < _hysteresisAnalysis.Count; i++)
                    _minStableAreaWidths.Add(_hysteresisAnalysis[i].getStableAreaWidthsStatistic(Vector.VectorAttribute.min));
            }
        }


        void getMeanStableAreaWidths()
        {
            if (_meanStableAreaWidths.Count == 0)
            {
                for (int i = 0; i < _hysteresisAnalysis.Count; i++)
                    _meanStableAreaWidths.Add(_hysteresisAnalysis[i].getStableAreaWidthsStatistic(Vector.VectorAttribute.mean));
            }
        }



        void getSumStableAreaWidths()
        {
            if (_sumStableAreaWidths.Count == 0)
            {
                for (int i = 0; i < _hysteresisAnalysis.Count; i++)
                    _sumStableAreaWidths.Add(_hysteresisAnalysis[i].getStableAreaWidthsStatistic(Vector.VectorAttribute.sum));
            }
        }



        void getVarStableAreaWidths()
        {
            if (_varStableAreaWidths.Count == 0)
            {
                for (int i = 0; i < _hysteresisAnalysis.Count; i++)
                    _varStableAreaWidths.Add(_hysteresisAnalysis[i].getStableAreaWidthsStatistic(Vector.VectorAttribute.variance));
            }
        }



        void getStdDevStableAreaWidths()
        {
            if (_stdDevStableAreaWidths.Count == 0)
            {
                for (int i = 0; i < _hysteresisAnalysis.Count; i++)
                    _stdDevStableAreaWidths.Add(_hysteresisAnalysis[i].getStableAreaWidthsStatistic(Vector.VectorAttribute.stdDev));
            }
        }



        void getMedianStableAreaWidths()
        {
            if (_medianStableAreaWidths.Count == 0)
            {
                for (int i = 0; i < _hysteresisAnalysis.Count; i++)
                    _medianStableAreaWidths.Add(_hysteresisAnalysis[i].getStableAreaWidthsStatistic(Vector.VectorAttribute.median));
            }
        }



        void getNumStableAreaWidthSamples()
        {
            if (_numStableAreaWidthSamples.Count == 0)
            {
                for (int i = 0; i < _hysteresisAnalysis.Count; i++)
                    _numStableAreaWidthSamples.Add(_hysteresisAnalysis[i].getStableAreaWidthsStatistic(Vector.VectorAttribute.count));
            }
        }



        //
        /////////////////////////////////////////////////////////////////////////////
        ///
        //	Percentage working Region
        //
        void getPercWorkingRegions()
        {
            if (_percWorkingRegions.Count == 0)
            {
                for (int i = 0; i < _hysteresisAnalysis.Count; i++)
                {
                    Vector percWorkingRegions;
                    _hysteresisAnalysis[i].getPercWorkingRegions(out percWorkingRegions);

                    _percWorkingRegions.Add(percWorkingRegions);
                }
            }
        }

        void getMaxPercWorkingRegions()
        {
            if (_maxPercWorkingRegions.Count == 0)
            {
                for (int i = 0; i < _hysteresisAnalysis.Count; i++)
                    _maxPercWorkingRegions.Add(_hysteresisAnalysis[i].getPercWorkingRegionsStatistic(Vector.VectorAttribute.max));
            }
        }



        void getMinPercWorkingRegions()
        {
            if (_minPercWorkingRegions.Count == 0)
            {
                for (int i = 0; i < _hysteresisAnalysis.Count; i++)
                    _minPercWorkingRegions.Add(_hysteresisAnalysis[i].getPercWorkingRegionsStatistic(Vector.VectorAttribute.min));
            }
        }



        void getMeanPercWorkingRegions()
        {
            if (_meanPercWorkingRegions.Count == 0)
            {
                for (int i = 0; i < _hysteresisAnalysis.Count; i++)
                    _meanPercWorkingRegions.Add(_hysteresisAnalysis[i].getPercWorkingRegionsStatistic(Vector.VectorAttribute.mean));
            }
        }


        void getSumPercWorkingRegions()
        {
            if (_sumPercWorkingRegions.Count == 0)
            {
                for (int i = 0; i < _hysteresisAnalysis.Count; i++)
                    _sumPercWorkingRegions.Add(_hysteresisAnalysis[i].getPercWorkingRegionsStatistic(Vector.VectorAttribute.sum));
            }
        }



        void getVarPercWorkingRegions()
        {
            if (_varPercWorkingRegions.Count == 0)
            {
                for (int i = 0; i < _hysteresisAnalysis.Count; i++)
                    _varPercWorkingRegions.Add(_hysteresisAnalysis[i].getPercWorkingRegionsStatistic(Vector.VectorAttribute.variance));
            }
        }


        void getStdDevPercWorkingRegions()
        {
            if (_stdDevPercWorkingRegions.Count == 0)
            {
                for (int i = 0; i < _hysteresisAnalysis.Count; i++)
                    _stdDevPercWorkingRegions.Add(_hysteresisAnalysis[i].getPercWorkingRegionsStatistic(Vector.VectorAttribute.stdDev));
            }
        }



        void getMedianPercWorkingRegions()
        {
            if (_medianPercWorkingRegions.Count == 0)
            {
                for (int i = 0; i < _hysteresisAnalysis.Count; i++)
                    _medianPercWorkingRegions.Add(_hysteresisAnalysis[i].getPercWorkingRegionsStatistic(Vector.VectorAttribute.median));
            }
        }



        void getNumPercWorkingRegionSamples()
        {
            if (_numPercWorkingRegionSamples.Count == 0)
            {
                for (int i = 0; i < _hysteresisAnalysis.Count; i++)
                    _numPercWorkingRegionSamples.Add(_hysteresisAnalysis[i].getPercWorkingRegionsStatistic(Vector.VectorAttribute.count));
            }
        }



        ////////////////////////////////////////////////////////////////////////////////
        //	Modal Distortion
        //
        void getForwardModalDistortionAngles()
        {
            if (_forwardLowerModalDistortionAngles.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _forwardLowerModalDistortionAngles.Add(_forwardModes[i].getLowerModeModalDistortionAngle());
                    _forwardUpperModalDistortionAngles.Add(_forwardModes[i].getUpperModeModalDistortionAngle());
                }
            }
        }


        void getForwardModalDistortionAngleXPositions()
        {
            if (_forwardLowerModalDistortionAngleXPositions.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _forwardLowerModalDistortionAngleXPositions.Add(_forwardModes[i].getLowerModeMaxModalDistortionAngleXPosition());
                    _forwardUpperModalDistortionAngleXPositions.Add(_forwardModes[i].getUpperModeMaxModalDistortionAngleXPosition());
                }
            }
        }



        /////////////////////////////////////////////////////////////////////////////
        ///
        //	Mode Boundary Direction Change Area measurements
        //
        void getForwardModeBDCAreas()
        {
            if (_forwardLowerModeBDCAreas.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    Vector forwardLowerModeBDCAreas;
                    Vector forwardUpperModeBDCAreas;
                    _forwardModes[i].getModeBDCAreas(out forwardLowerModeBDCAreas, out forwardUpperModeBDCAreas);

                    _forwardLowerModeBDCAreas.Add(forwardLowerModeBDCAreas);
                    _forwardUpperModeBDCAreas.Add(forwardUpperModeBDCAreas);
                }
            }
        }



        void getMaxForwardModeBDCAreas()
        {
            if (_maxForwardLowerModeBDCAreas.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _maxForwardLowerModeBDCAreas.Add(_forwardModes[i].getLowerModeBDCAreasStatistic(Vector.VectorAttribute.max));
                    _maxForwardUpperModeBDCAreas.Add(_forwardModes[i].getUpperModeBDCAreasStatistic(Vector.VectorAttribute.max));
                }
            }
        }


        void getMinForwardModeBDCAreas()
        {
            if (_minForwardLowerModeBDCAreas.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _minForwardLowerModeBDCAreas.Add(_forwardModes[i].getLowerModeBDCAreasStatistic(Vector.VectorAttribute.min));
                    _minForwardUpperModeBDCAreas.Add(_forwardModes[i].getUpperModeBDCAreasStatistic(Vector.VectorAttribute.min));
                }
            }
        }


        void getMeanForwardModeBDCAreas()
        {
            if (_meanForwardLowerModeBDCAreas.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _meanForwardLowerModeBDCAreas.Add(_forwardModes[i].getLowerModeBDCAreasStatistic(Vector.VectorAttribute.mean));
                    _meanForwardUpperModeBDCAreas.Add(_forwardModes[i].getUpperModeBDCAreasStatistic(Vector.VectorAttribute.mean));
                }
            }
        }


        void getSumForwardModeBDCAreas()
        {
            if (_sumForwardLowerModeBDCAreas.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _sumForwardLowerModeBDCAreas.Add(_forwardModes[i].getLowerModeBDCAreasStatistic(Vector.VectorAttribute.sum));
                    _sumForwardUpperModeBDCAreas.Add(_forwardModes[i].getUpperModeBDCAreasStatistic(Vector.VectorAttribute.sum));
                }
            }
        }


        void getVarForwardModeBDCAreas()
        {
            if (_varForwardLowerModeBDCAreas.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _varForwardLowerModeBDCAreas.Add(_forwardModes[i].getLowerModeBDCAreasStatistic(Vector.VectorAttribute.variance));
                    _varForwardUpperModeBDCAreas.Add(_forwardModes[i].getUpperModeBDCAreasStatistic(Vector.VectorAttribute.variance));
                }
            }
        }


        void getStdDevForwardModeBDCAreas()
        {
            if (_stdDevForwardLowerModeBDCAreas.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _stdDevForwardLowerModeBDCAreas.Add(_forwardModes[i].getLowerModeBDCAreasStatistic(Vector.VectorAttribute.stdDev));
                    _stdDevForwardUpperModeBDCAreas.Add(_forwardModes[i].getUpperModeBDCAreasStatistic(Vector.VectorAttribute.stdDev));
                }
            }
        }


        void getMedianForwardModeBDCAreas()
        {
            if (_medianForwardLowerModeBDCAreas.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _medianForwardLowerModeBDCAreas.Add(_forwardModes[i].getLowerModeBDCAreasStatistic(Vector.VectorAttribute.median));
                    _medianForwardUpperModeBDCAreas.Add(_forwardModes[i].getUpperModeBDCAreasStatistic(Vector.VectorAttribute.median));
                }
            }
        }


        void getNumForwardModeBDCAreaSamples()
        {
            if (_numForwardLowerModeBDCAreaSamples.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _numForwardLowerModeBDCAreaSamples.Add(_forwardModes[i].getLowerModeBDCAreasStatistic(Vector.VectorAttribute.count));
                    _numForwardUpperModeBDCAreaSamples.Add(_forwardModes[i].getUpperModeBDCAreasStatistic(Vector.VectorAttribute.count));
                }
            }
        }



        /////////////////////////////////////////////////////////////////////////////
        //	Mode Boundary Direction Change Area X Length measurements
        //
        void getForwardModeBDCAreaXLengths()
        {
            if (_forwardLowerModeBDCAreaXLengths.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    Vector forwardLowerModeBDCAreaXLengths;
                    Vector forwardUpperModeBDCAreaXLengths;
                    _forwardModes[i].getModeBDCAreaXLengths(out forwardLowerModeBDCAreaXLengths, out forwardUpperModeBDCAreaXLengths);

                    _forwardLowerModeBDCAreaXLengths.Add(forwardLowerModeBDCAreaXLengths);
                    _forwardUpperModeBDCAreaXLengths.Add(forwardUpperModeBDCAreaXLengths);
                }
            }
        }


        void getMaxForwardModeBDCAreaXLengths()
        {
            if (_maxForwardLowerModeBDCAreaXLengths.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _maxForwardLowerModeBDCAreaXLengths.Add(_forwardModes[i].getLowerModeBDCAreaXLengthsStatistic(Vector.VectorAttribute.max));
                    _maxForwardUpperModeBDCAreaXLengths.Add(_forwardModes[i].getUpperModeBDCAreaXLengthsStatistic(Vector.VectorAttribute.max));
                }
            }
        }


        void getMinForwardModeBDCAreaXLengths()
        {
            if (_minForwardLowerModeBDCAreaXLengths.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _minForwardLowerModeBDCAreaXLengths.Add(_forwardModes[i].getLowerModeBDCAreaXLengthsStatistic(Vector.VectorAttribute.min));
                    _minForwardUpperModeBDCAreaXLengths.Add(_forwardModes[i].getUpperModeBDCAreaXLengthsStatistic(Vector.VectorAttribute.min));
                }
            }
        }


        void getMeanForwardModeBDCAreaXLengths()
        {
            if (_meanForwardLowerModeBDCAreaXLengths.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _meanForwardLowerModeBDCAreaXLengths.Add(_forwardModes[i].getLowerModeBDCAreaXLengthsStatistic(Vector.VectorAttribute.mean));
                    _meanForwardUpperModeBDCAreaXLengths.Add(_forwardModes[i].getUpperModeBDCAreaXLengthsStatistic(Vector.VectorAttribute.mean));
                }
            }
        }


        void getSumForwardModeBDCAreaXLengths()
        {
            if (_sumForwardLowerModeBDCAreaXLengths.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _sumForwardLowerModeBDCAreaXLengths.Add(_forwardModes[i].getLowerModeBDCAreaXLengthsStatistic(Vector.VectorAttribute.sum));
                    _sumForwardUpperModeBDCAreaXLengths.Add(_forwardModes[i].getUpperModeBDCAreaXLengthsStatistic(Vector.VectorAttribute.sum));
                }
            }
        }


        void getVarForwardModeBDCAreaXLengths()
        {
            if (_varForwardLowerModeBDCAreaXLengths.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _varForwardLowerModeBDCAreaXLengths.Add(_forwardModes[i].getLowerModeBDCAreaXLengthsStatistic(Vector.VectorAttribute.variance));
                    _varForwardUpperModeBDCAreaXLengths.Add(_forwardModes[i].getUpperModeBDCAreaXLengthsStatistic(Vector.VectorAttribute.variance));
                }
            }
        }


        void getStdDevForwardModeBDCAreaXLengths()
        {
            if (_stdDevForwardLowerModeBDCAreaXLengths.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _stdDevForwardLowerModeBDCAreaXLengths.Add(_forwardModes[i].getLowerModeBDCAreaXLengthsStatistic(Vector.VectorAttribute.stdDev));
                    _stdDevForwardUpperModeBDCAreaXLengths.Add(_forwardModes[i].getUpperModeBDCAreaXLengthsStatistic(Vector.VectorAttribute.stdDev));
                }
            }
        }


        void getMedianForwardModeBDCAreaXLengths()
        {
            if (_medianForwardLowerModeBDCAreaXLengths.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _medianForwardLowerModeBDCAreaXLengths.Add(_forwardModes[i].getLowerModeBDCAreaXLengthsStatistic(Vector.VectorAttribute.median));
                    _medianForwardUpperModeBDCAreaXLengths.Add(_forwardModes[i].getUpperModeBDCAreaXLengthsStatistic(Vector.VectorAttribute.median));
                }
            }
        }


        void getNumForwardModeBDCAreaXLengthSamples()
        {
            if (_numForwardLowerModeBDCAreaXLengthSamples.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _numForwardLowerModeBDCAreaXLengthSamples.Add(_forwardModes[i].getLowerModeBDCAreaXLengthsStatistic(Vector.VectorAttribute.count));
                    _numForwardUpperModeBDCAreaXLengthSamples.Add(_forwardModes[i].getUpperModeBDCAreaXLengthsStatistic(Vector.VectorAttribute.count));
                }
            }
        }



        //
        /////////////////////////////////////////////////////////////////////////////
        //	Reverse Mode Widths

        void getReverseModeWidths()
        {
            if (_reverseModeWidths.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    Vector reverseModeWidths;
                    _reverseModes[i].getModeWidths(out reverseModeWidths);

                    _reverseModeWidths.Add(reverseModeWidths);
                }
            }
        }



        void getMaxReverseModeWidths()
        {
            if (_maxReverseModeWidths.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                    _maxReverseModeWidths.Add(_reverseModes[i].getModeWidthsStatistic(Vector.VectorAttribute.max));
            }
        }


        void getMinReverseModeWidths()
        {
            if (_minReverseModeWidths.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                    _minReverseModeWidths.Add(_reverseModes[i].getModeWidthsStatistic(Vector.VectorAttribute.min));
            }
        }


        void getMeanReverseModeWidths()
        {
            if (_meanReverseModeWidths.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                    _meanReverseModeWidths.Add(_reverseModes[i].getModeWidthsStatistic(Vector.VectorAttribute.mean));
            }
        }


        void getSumReverseModeWidths()
        {
            if (_sumReverseModeWidths.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                    _sumReverseModeWidths.Add(_reverseModes[i].getModeWidthsStatistic(Vector.VectorAttribute.sum));
            }
        }


        void getVarReverseModeWidths()
        {
            if (_varReverseModeWidths.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                    _varReverseModeWidths.Add(_reverseModes[i].getModeWidthsStatistic(Vector.VectorAttribute.variance));
            }
        }


        void getStdDevReverseModeWidths()
        {
            if (_stdDevReverseModeWidths.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                    _stdDevReverseModeWidths.Add(_reverseModes[i].getModeWidthsStatistic(Vector.VectorAttribute.stdDev));
            }
        }


        void getMedianReverseModeWidths()
        {
            if (_medianReverseModeWidths.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                    _medianReverseModeWidths.Add(_reverseModes[i].getModeWidthsStatistic(Vector.VectorAttribute.median));
            }
        }


        void getNumReverseModeWidthSamples()
        {
            if (_numReverseModeWidthSamples.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                    _numReverseModeWidthSamples.Add(_reverseModes[i].getModeWidthsStatistic(Vector.VectorAttribute.count));
            }
        }

        //
        /////////////////////////////////////////////////////////////////////////////
        ///
        //	Reverse Mode Slopes
        //


        void getReverseModeSlopes()
        {
            if (_reverseLowerModeSlopes.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    Vector reverseLowerModeSlopes;
                    Vector reverseUpperModeSlopes;
                    _reverseModes[i].getModeSlopes(out reverseLowerModeSlopes, out reverseUpperModeSlopes);

                    _reverseLowerModeSlopes.Add(reverseLowerModeSlopes);
                    _reverseUpperModeSlopes.Add(reverseUpperModeSlopes);
                }
            }
        }



        void getMaxReverseModeSlopes()
        {
            if (_maxReverseLowerModeSlopes.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _maxReverseLowerModeSlopes.Add(_reverseModes[i].getModeLowerSlopesStatistic(Vector.VectorAttribute.max));
                    _maxReverseUpperModeSlopes.Add(_reverseModes[i].getModeUpperSlopesStatistic(Vector.VectorAttribute.max));
                }
            }
        }


        void getMinReverseModeSlopes()
        {
            if (_minReverseLowerModeSlopes.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _minReverseLowerModeSlopes.Add(_reverseModes[i].getModeLowerSlopesStatistic(Vector.VectorAttribute.min));
                    _minReverseUpperModeSlopes.Add(_reverseModes[i].getModeUpperSlopesStatistic(Vector.VectorAttribute.min));
                }
            }
        }


        void getMeanReverseModeSlopes()
        {
            if (_meanReverseLowerModeSlopes.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _meanReverseLowerModeSlopes.Add(_reverseModes[i].getModeLowerSlopesStatistic(Vector.VectorAttribute.mean));
                    _meanReverseUpperModeSlopes.Add(_reverseModes[i].getModeUpperSlopesStatistic(Vector.VectorAttribute.mean));
                }
            }
        }


        void getSumReverseModeSlopes()
        {
            if (_sumReverseLowerModeSlopes.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _sumReverseLowerModeSlopes.Add(_reverseModes[i].getModeLowerSlopesStatistic(Vector.VectorAttribute.sum));
                    _sumReverseUpperModeSlopes.Add(_reverseModes[i].getModeUpperSlopesStatistic(Vector.VectorAttribute.sum));
                }
            }
        }


        void getVarReverseModeSlopes()
        {
            if (_varReverseLowerModeSlopes.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _varReverseLowerModeSlopes.Add(_reverseModes[i].getModeLowerSlopesStatistic(Vector.VectorAttribute.variance));
                    _varReverseUpperModeSlopes.Add(_reverseModes[i].getModeUpperSlopesStatistic(Vector.VectorAttribute.variance));
                }
            }
        }


        void getStdDevReverseModeSlopes()
        {
            if (_stdDevReverseLowerModeSlopes.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _stdDevReverseLowerModeSlopes.Add(_reverseModes[i].getModeLowerSlopesStatistic(Vector.VectorAttribute.stdDev));
                    _stdDevReverseUpperModeSlopes.Add(_reverseModes[i].getModeUpperSlopesStatistic(Vector.VectorAttribute.stdDev));
                }
            }
        }


        void getMedianReverseModeSlopes()
        {
            if (_medianReverseLowerModeSlopes.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _medianReverseLowerModeSlopes.Add(_reverseModes[i].getModeLowerSlopesStatistic(Vector.VectorAttribute.median));
                    _medianReverseUpperModeSlopes.Add(_reverseModes[i].getModeUpperSlopesStatistic(Vector.VectorAttribute.median));
                }
            }
        }


        void getNumReverseModeSlopeSamples()
        {
            if (_numReverseLowerModeSlopeSamples.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _numReverseLowerModeSlopeSamples.Add(_reverseModes[i].getModeLowerSlopesStatistic(Vector.VectorAttribute.count));
                    _numReverseUpperModeSlopeSamples.Add(_reverseModes[i].getModeUpperSlopesStatistic(Vector.VectorAttribute.count));
                }
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        //	Modal Distortion
        //


        void getReverseModalDistortionAngles()
        {
            if (_reverseLowerModalDistortionAngles.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _reverseLowerModalDistortionAngles.Add(_reverseModes[i].getLowerModeModalDistortionAngle());
                    _reverseUpperModalDistortionAngles.Add(_reverseModes[i].getUpperModeModalDistortionAngle());
                }
            }
        }


        void getReverseModalDistortionAngleXPositions()
        {
            if (_reverseLowerModalDistortionAngleXPositions.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _reverseLowerModalDistortionAngleXPositions.Add(_reverseModes[i].getLowerModeMaxModalDistortionAngleXPosition());
                    _reverseUpperModalDistortionAngleXPositions.Add(_reverseModes[i].getUpperModeMaxModalDistortionAngleXPosition());
                }
            }
        }

        /////////////////////////////////////////////////////////////////////////////
        ///
        //	Mode Boundary Direction Change Area measurements
        //


        void getReverseModeBDCAreas()
        {
            if (_reverseLowerModeBDCAreas.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    Vector reverseLowerModeBDCAreas;
                    Vector reverseUpperModeBDCAreas;

                    _reverseModes[i].getModeBDCAreas(out reverseLowerModeBDCAreas, out reverseUpperModeBDCAreas);

                    _reverseLowerModeBDCAreas.Add(reverseLowerModeBDCAreas);
                    _reverseUpperModeBDCAreas.Add(reverseUpperModeBDCAreas);
                }
            }
        }



        void getMaxReverseModeBDCAreas()
        {
            if (_maxReverseLowerModeBDCAreas.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _maxReverseLowerModeBDCAreas.Add(_reverseModes[i].getLowerModeBDCAreasStatistic(Vector.VectorAttribute.max));
                    _maxReverseUpperModeBDCAreas.Add(_reverseModes[i].getUpperModeBDCAreasStatistic(Vector.VectorAttribute.max));
                }
            }
        }


        void getMinReverseModeBDCAreas()
        {
            if (_minReverseLowerModeBDCAreas.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _minReverseLowerModeBDCAreas.Add(_reverseModes[i].getLowerModeBDCAreasStatistic(Vector.VectorAttribute.min));
                    _minReverseUpperModeBDCAreas.Add(_reverseModes[i].getUpperModeBDCAreasStatistic(Vector.VectorAttribute.min));
                }
            }
        }


        void getMeanReverseModeBDCAreas()
        {
            if (_meanReverseLowerModeBDCAreas.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _meanReverseLowerModeBDCAreas.Add(_reverseModes[i].getLowerModeBDCAreasStatistic(Vector.VectorAttribute.mean));
                    _meanReverseUpperModeBDCAreas.Add(_reverseModes[i].getUpperModeBDCAreasStatistic(Vector.VectorAttribute.mean));
                }
            }
        }


        void getSumReverseModeBDCAreas()
        {
            if (_sumReverseLowerModeBDCAreas.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _sumReverseLowerModeBDCAreas.Add(_reverseModes[i].getLowerModeBDCAreasStatistic(Vector.VectorAttribute.sum));
                    _sumReverseUpperModeBDCAreas.Add(_reverseModes[i].getUpperModeBDCAreasStatistic(Vector.VectorAttribute.sum));
                }
            }
        }


        void getVarReverseModeBDCAreas()
        {
            if (_varReverseLowerModeBDCAreas.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _varReverseLowerModeBDCAreas.Add(_reverseModes[i].getLowerModeBDCAreasStatistic(Vector.VectorAttribute.variance));
                    _varReverseUpperModeBDCAreas.Add(_reverseModes[i].getUpperModeBDCAreasStatistic(Vector.VectorAttribute.variance));
                }
            }
        }


        void getStdDevReverseModeBDCAreas()
        {
            if (_stdDevReverseLowerModeBDCAreas.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _stdDevReverseLowerModeBDCAreas.Add(_reverseModes[i].getLowerModeBDCAreasStatistic(Vector.VectorAttribute.stdDev));
                    _stdDevReverseUpperModeBDCAreas.Add(_reverseModes[i].getUpperModeBDCAreasStatistic(Vector.VectorAttribute.stdDev));
                }
            }
        }


        void getMedianReverseModeBDCAreas()
        {
            if (_medianReverseLowerModeBDCAreas.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _medianReverseLowerModeBDCAreas.Add(_reverseModes[i].getLowerModeBDCAreasStatistic(Vector.VectorAttribute.median));
                    _medianReverseUpperModeBDCAreas.Add(_reverseModes[i].getUpperModeBDCAreasStatistic(Vector.VectorAttribute.median));
                }
            }
        }


        void getNumReverseModeBDCAreaSamples()
        {
            if (_numReverseLowerModeBDCAreaSamples.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _numReverseLowerModeBDCAreaSamples.Add(_reverseModes[i].getLowerModeBDCAreasStatistic(Vector.VectorAttribute.count));
                    _numReverseUpperModeBDCAreaSamples.Add(_reverseModes[i].getUpperModeBDCAreasStatistic(Vector.VectorAttribute.count));
                }
            }
        }

        /////////////////////////////////////////////////////////////////////////////
        ///
        //	Mode Boundary Direction Change Area X Length measurements
        //


        void getReverseModeBDCAreaXLengths()
        {
            if (_reverseLowerModeBDCAreaXLengths.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    Vector reverseLowerModeBDCAreaXLengths;
                    Vector reverseUpperModeBDCAreaXLengths;
                    _reverseModes[i].getModeBDCAreaXLengths(out reverseLowerModeBDCAreaXLengths, out reverseUpperModeBDCAreaXLengths);

                    _reverseLowerModeBDCAreaXLengths.Add(reverseLowerModeBDCAreaXLengths);
                    _reverseUpperModeBDCAreaXLengths.Add(reverseUpperModeBDCAreaXLengths);
                }
            }
        }



        void getMaxReverseModeBDCAreaXLengths()
        {
            if (_maxReverseLowerModeBDCAreaXLengths.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _maxReverseLowerModeBDCAreaXLengths.Add(_reverseModes[i].getLowerModeBDCAreaXLengthsStatistic(Vector.VectorAttribute.max));
                    _maxReverseUpperModeBDCAreaXLengths.Add(_reverseModes[i].getUpperModeBDCAreaXLengthsStatistic(Vector.VectorAttribute.max));
                }
            }
        }


        void getMinReverseModeBDCAreaXLengths()
        {
            if (_minReverseLowerModeBDCAreaXLengths.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _minReverseLowerModeBDCAreaXLengths.Add(_reverseModes[i].getLowerModeBDCAreaXLengthsStatistic(Vector.VectorAttribute.min));
                    _minReverseUpperModeBDCAreaXLengths.Add(_reverseModes[i].getUpperModeBDCAreaXLengthsStatistic(Vector.VectorAttribute.min));
                }
            }
        }


        void getMeanReverseModeBDCAreaXLengths()
        {
            if (_meanReverseLowerModeBDCAreaXLengths.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _meanReverseLowerModeBDCAreaXLengths.Add(_reverseModes[i].getLowerModeBDCAreaXLengthsStatistic(Vector.VectorAttribute.mean));
                    _meanReverseUpperModeBDCAreaXLengths.Add(_reverseModes[i].getUpperModeBDCAreaXLengthsStatistic(Vector.VectorAttribute.mean));
                }
            }
        }


        void getSumReverseModeBDCAreaXLengths()
        {
            if (_sumReverseLowerModeBDCAreaXLengths.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _sumReverseLowerModeBDCAreaXLengths.Add(_reverseModes[i].getLowerModeBDCAreaXLengthsStatistic(Vector.VectorAttribute.sum));
                    _sumReverseUpperModeBDCAreaXLengths.Add(_reverseModes[i].getUpperModeBDCAreaXLengthsStatistic(Vector.VectorAttribute.sum));
                }
            }
        }


        void getVarReverseModeBDCAreaXLengths()
        {
            if (_varReverseLowerModeBDCAreaXLengths.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _varReverseLowerModeBDCAreaXLengths.Add(_reverseModes[i].getLowerModeBDCAreaXLengthsStatistic(Vector.VectorAttribute.variance));
                    _varReverseUpperModeBDCAreaXLengths.Add(_reverseModes[i].getUpperModeBDCAreaXLengthsStatistic(Vector.VectorAttribute.variance));
                }
            }
        }


        void getStdDevReverseModeBDCAreaXLengths()
        {
            if (_stdDevReverseLowerModeBDCAreaXLengths.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _stdDevReverseLowerModeBDCAreaXLengths.Add(_reverseModes[i].getLowerModeBDCAreaXLengthsStatistic(Vector.VectorAttribute.stdDev));
                    _stdDevReverseUpperModeBDCAreaXLengths.Add(_reverseModes[i].getUpperModeBDCAreaXLengthsStatistic(Vector.VectorAttribute.stdDev));
                }
            }
        }


        void getMedianReverseModeBDCAreaXLengths()
        {
            if (_medianReverseLowerModeBDCAreaXLengths.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _medianReverseLowerModeBDCAreaXLengths.Add(_reverseModes[i].getLowerModeBDCAreaXLengthsStatistic(Vector.VectorAttribute.median));
                    _medianReverseUpperModeBDCAreaXLengths.Add(_reverseModes[i].getUpperModeBDCAreaXLengthsStatistic(Vector.VectorAttribute.median));
                }
            }
        }


        void getNumReverseModeBDCAreaXLengthSamples()
        {
            if (_numReverseLowerModeBDCAreaXLengthSamples.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _numReverseLowerModeBDCAreaXLengthSamples.Add(_reverseModes[i].getLowerModeBDCAreaXLengthsStatistic(Vector.VectorAttribute.count));
                    _numReverseUpperModeBDCAreaXLengthSamples.Add(_reverseModes[i].getUpperModeBDCAreaXLengthsStatistic(Vector.VectorAttribute.count));
                }
            }
        }


        //
        /////////////////////////////////////////////////////////////////////////////
        //	MiddleLine RootMeanSquare analysis
        //

        void getMiddleLineRMSValues()
        {
            /////////////////////////////////////////////////////////////////////
            /// Use VectorAnalysis to void get RMS value of this supermodes middle line
            //
            if (_middleLineRMSValues.Count == 0)
            {
                for (int i = 0; i < _middleLines.Count; i++)
                {
                    double linearFitLineSlope = 0;
                    double linearFitLineIntercept = 0;

                    // find linear fit line
                    VectorAnalysis.linearFit(_middleLines[i].xPoints(),
                                            _middleLines[i].yPoints(),
                                            out linearFitLineSlope,
                                            out linearFitLineIntercept);

                    // find distances from points to linear fit line
                    List<double> distances = new List<double>();
                    VectorAnalysis.distancesFromPointsToLine(_middleLines[i].xPoints(),
                                                            _middleLines[i].yPoints(),
                                                            linearFitLineSlope,
                                                            linearFitLineIntercept,
                                                            distances);

                    // void get mean of distances
                    double meanDistance = 0;
                    meanDistance = VectorAnalysis.meanOfValues(distances);

                    _middleLineSlopes.Add(linearFitLineSlope);

                    _middleLineRMSValues.Add(Math.Sqrt(Math.Pow(meanDistance, 2)));
                }
            }

        }


        void getMiddleLineSlopes()
        {
            if (_middleLineSlopes.Count == 0)
            {
                for (int i = 0; i < _middleLines.Count; i++)
                {
                    double linearFitLineSlope = 0;
                    double linearFitLineIntercept = 0;

                    // find linear fit line
                    VectorAnalysis.linearFit(_middleLines[i].xPoints(),
                                            _middleLines[i].yPoints(),
                                            out linearFitLineSlope,
                                            out linearFitLineIntercept);

                    _middleLineSlopes.Add(linearFitLineSlope);
                }
            }
        }
        //
        /////////////////////////////////////////////////////////////////////////////
        ///
        //	Continuity analysis
        //


        void getMaxContinuityValue()
        {
            if (_maxContinuityValue == 0)
                _maxContinuityValue = _continuityValues.GetVectorAttribute(Vector.VectorAttribute.max);
        }



        void getMinContinuityValue()
        {
            if (_minContinuityValue == 0)
                _minContinuityValue = _continuityValues.GetVectorAttribute(Vector.VectorAttribute.min);
        }



        void getMeanContinuityValues()
        {
            if (_meanContinuityValues == 0)
                _meanContinuityValues = _continuityValues.GetVectorAttribute(Vector.VectorAttribute.mean);
        }



        void getSumContinuityValues()
        {
            if (_sumContinuityValues == 0)
                _sumContinuityValues = _continuityValues.GetVectorAttribute(Vector.VectorAttribute.sum);
        }



        void getVarContinuityValues()
        {
            if (_varContinuityValues == 0)
                _varContinuityValues = _continuityValues.GetVectorAttribute(Vector.VectorAttribute.variance);
        }



        void getStdDevContinuityValues()
        {
            if (_stdDevContinuityValues == 0)
                _stdDevContinuityValues = _continuityValues.GetVectorAttribute(Vector.VectorAttribute.stdDev);
        }



        void getMedianContinuityValues()
        {
            if (_medianContinuityValues == 0)
                _medianContinuityValues = _continuityValues.GetVectorAttribute(Vector.VectorAttribute.median);
        }



        void getNumContinuityValues()
        {
            if (_numContinuityValues == 0)
                _numContinuityValues = _continuityValues.GetVectorAttribute(Vector.VectorAttribute.count);
        }



        void getMaxContinuityValueSpacing()
        {
            if (_maxPrGap == 0)
            {
                _maxPrGap = _continuityValues.GetVectorAttribute(Vector.VectorAttribute.maxGap);
            }
        }

        //
        ///
        ////////////////////////////////////////////////////////////////////////////////
        ///
        //	File Handling section - writing results to file
        //
        rcode writeQaResultsToFile()
        {
            rcode err = rcode.ok;

            CLoseGridFile qaResults = new CLoseGridFile(_qaResultsAbsFilePath);
            qaResults.createFolderAndFile();

            //////////////////////////////////////////////////////////////////////////

            string errorMessage = "";
            int errorNum = 0;
            getQAError(_qaDetectedError,out errorNum,out errorMessage);

            //CStringArray error_msg_row;
            List<string> error_msg_row = new List<string>();
            error_msg_row.Add(errorMessage);
            qaResults.addRow(error_msg_row);
            qaResults.addEmptyLine();

            //////////////////////////////////////////

            //CStringArray row;
            List<string> row = new List<string>();
            string cellString = "";

            row.Add("Slope Window Size");
            cellString = DSDBR01.Instance._DSDBR01_SMQA_slope_window_size.ToString();
            row.Add(cellString);
            qaResults.addRow(row);

            row.Clear();
            row.Add("Modal Distortion Min X");
            cellString = DSDBR01.Instance._DSDBR01_SMQA_modal_distortion_min_x.ToString("g");
            row.Add(cellString);
            //GDM 31/10/06 added Mode width and Hysteresis window mins
            row.Add("Mode Width Analysis Min X");
            cellString = DSDBR01.Instance._DSDBR01_SMQA_mode_width_analysis_min_x.ToString("g");
            row.Add(cellString);
            row.Add("Hysteresis Analysis Min X");
            cellString = DSDBR01.Instance._DSDBR01_SMQA_hysteresis_analysis_min_x.ToString("g");
            row.Add(cellString);
            //GDM
            qaResults.addRow(row);

            row.Clear();
            row.Add("Modal Distortion Max X");
            cellString = DSDBR01.Instance._DSDBR01_SMQA_modal_distortion_max_x.ToString("g");
            row.Add(cellString);
            //GDM 31/10/06 added Mode width and Hysteresis window mins
            row.Add("Mode Width Analysis Max X");
            cellString = DSDBR01.Instance._DSDBR01_SMQA_mode_width_analysis_max_x.ToString("g");
            row.Add(cellString);
            row.Add("Hysteresis Analysis Max X");
            cellString = DSDBR01.Instance._DSDBR01_SMQA_hysteresis_analysis_max_x.ToString("g");
            row.Add(cellString);
            //GDM
            qaResults.addRow(row);

            row.Clear();
            row.Add("Modal Distortion Min Y");
            cellString = DSDBR01.Instance._DSDBR01_SMQA_modal_distortion_min_y.ToString("g");
            row.Add(cellString);
            qaResults.addRow(row);

            qaResults.addEmptyLine();

            //////////////////////////////////////////

            const string QA_XLS_HEADER1 = "Overall % Stable Area";
            const string QA_XLS_HEADER2 = "Index";
            const string QA_XLS_HEADER3 = "Function";
            const string QA_XLS_HEADER4 = "Slope of forward lower mode boundary line";
            const string QA_XLS_HEADER5 = "Slope of forward upper mode boundary line";
            const string QA_XLS_HEADER6 = "Slope of reverse lower mode boundary line";
            const string QA_XLS_HEADER7 = "Slope of reverse upper mode boundary line";
            const string QA_XLS_HEADER8 = "Width for Stable Area";
            const string QA_XLS_HEADER9 = "Reverse Mode Width";
            const string QA_XLS_HEADER10 = "Forward Mode Width";
            const string QA_XLS_HEADER11 = "% Working Region";
            const string QA_XLS_HEADER12 = "Forward Lower Max Modal Distortion Angle";
            const string QA_XLS_HEADER13 = "Forward Lower Max Modal Distortion X-Position";
            const string QA_XLS_HEADER14 = "Forward Upper Max Modal Distortion Angle";
            const string QA_XLS_HEADER15 = "Forward Upper Max Modal Distortion X-Position";
            const string QA_XLS_HEADER16 = "Reverse Lower Max Modal Distortion Angle";
            const string QA_XLS_HEADER17 = "Reverse Lower Max Modal Distortion X-Position";
            const string QA_XLS_HEADER18 = "Reverse Upper Max Modal Distortion Angle";
            const string QA_XLS_HEADER19 = "Reverse Upper Max Modal Distortion X-Position";
            const string QA_XLS_HEADER20 = "Forward Lower Max Boundary Direction Change X-Length";
            const string QA_XLS_HEADER21 = "Forward Upper Max Boundary Direction Change X-Length";
            const string QA_XLS_HEADER22 = "Forward Lower Max Boundary Direction Change Area";
            const string QA_XLS_HEADER23 = "Forward Upper Max Boundary Direction Change Area";
            const string QA_XLS_HEADER24 = "Forward Lower Sum of Boundary Direction Change Areas";
            const string QA_XLS_HEADER25 = "Forward Upper Sum of Boundary Direction Change Areas";
            const string QA_XLS_HEADER26 = "Forward Lower Number of Boundary Direction Changes";
            const string QA_XLS_HEADER27 = "Forward Upper Number of Boundary Direction Changes";
            const string QA_XLS_HEADER28 = "Reverse Lower Max Boundary Direction Change X-Length";
            const string QA_XLS_HEADER29 = "Reverse Upper Max Boundary Direction Change X-Length";
            const string QA_XLS_HEADER30 = "Reverse Lower Max Boundary Direction Change Area";
            const string QA_XLS_HEADER31 = "Reverse Upper Max Boundary Direction Change Area";
            const string QA_XLS_HEADER32 = "Reverse Lower Sum of Boundary Direction Change Areas";
            const string QA_XLS_HEADER33 = "Reverse Upper Sum of Boundary Direction Change Areas";
            const string QA_XLS_HEADER34 = "Reverse Lower Number of Boundary Direction Changes";
            const string QA_XLS_HEADER35 = "Reverse Upper Number of Boundary Direction Changes";
            const string QA_XLS_HEADER36 = "Middle Line RMS Value";

            const string QA_XLS_MAX = "Maximum";
            const string QA_XLS_MIN = "Minimum";
            const string QA_XLS_MEAN = "Mean";
            const string QA_XLS_SUM = "Sum";
            const string QA_XLS_COUNT = "Number Of Samples";
            const string QA_XLS_STDDEV = "Standard Deviation";
            const string QA_XLS_VAR = "Variance";
            const string QA_XLS_OTHERS = "";

            //CStringArray attribArray;
            List<string> attribArray = new List<string>();
            attribArray.Add(QA_XLS_MAX);
            attribArray.Add(QA_XLS_MIN);
            attribArray.Add(QA_XLS_MEAN);
            attribArray.Add(QA_XLS_SUM);
            attribArray.Add(QA_XLS_COUNT);
            attribArray.Add(QA_XLS_STDDEV);
            attribArray.Add(QA_XLS_VAR);
            attribArray.Add(QA_XLS_OTHERS);

            string QA_XLS_COL1_VALUE = "";
            string QA_XLS_COL2_VALUE = "";
            string QA_XLS_COL3_VALUE = "";
            string QA_XLS_COL4_VALUE = "";
            string QA_XLS_COL5_VALUE = "";
            string QA_XLS_COL6_VALUE = "";
            string QA_XLS_COL7_VALUE = "";
            string QA_XLS_COL8_VALUE = "";
            string QA_XLS_COL9_VALUE = "";
            string QA_XLS_COL10_VALUE = "";
            string QA_XLS_COL11_VALUE = "";
            string QA_XLS_COL12_VALUE = "";
            string QA_XLS_COL13_VALUE = "";
            string QA_XLS_COL14_VALUE = "";
            string QA_XLS_COL15_VALUE = "";
            string QA_XLS_COL16_VALUE = "";
            string QA_XLS_COL17_VALUE = "";
            string QA_XLS_COL18_VALUE = "";
            string QA_XLS_COL19_VALUE = "";
            string QA_XLS_COL20_VALUE = "";
            string QA_XLS_COL21_VALUE = "";
            string QA_XLS_COL22_VALUE = "";
            string QA_XLS_COL23_VALUE = "";
            string QA_XLS_COL24_VALUE = "";
            string QA_XLS_COL25_VALUE = "";
            string QA_XLS_COL26_VALUE = "";
            string QA_XLS_COL27_VALUE = "";
            string QA_XLS_COL28_VALUE = "";
            string QA_XLS_COL29_VALUE = "";
            string QA_XLS_COL30_VALUE = "";
            string QA_XLS_COL31_VALUE = "";
            string QA_XLS_COL32_VALUE = "";
            string QA_XLS_COL33_VALUE = "";
            string QA_XLS_COL34_VALUE = "";
            string QA_XLS_COL35_VALUE = "";
            string QA_XLS_COL36_VALUE = "";

            //CStringArray headerArray;
            List<string> headerArray = new List<string>();
            headerArray.Add(QA_XLS_HEADER1);
            headerArray.Add(QA_XLS_HEADER2);
            headerArray.Add(QA_XLS_HEADER3);
            headerArray.Add(QA_XLS_HEADER4);
            headerArray.Add(QA_XLS_HEADER5);
            headerArray.Add(QA_XLS_HEADER6);
            headerArray.Add(QA_XLS_HEADER7);
            headerArray.Add(QA_XLS_HEADER8);
            headerArray.Add(QA_XLS_HEADER9);
            headerArray.Add(QA_XLS_HEADER10);
            headerArray.Add(QA_XLS_HEADER11);
            headerArray.Add(QA_XLS_HEADER12);
            headerArray.Add(QA_XLS_HEADER13);
            headerArray.Add(QA_XLS_HEADER14);
            headerArray.Add(QA_XLS_HEADER15);
            headerArray.Add(QA_XLS_HEADER16);
            headerArray.Add(QA_XLS_HEADER17);
            headerArray.Add(QA_XLS_HEADER18);
            headerArray.Add(QA_XLS_HEADER19);
            headerArray.Add(QA_XLS_HEADER20);
            headerArray.Add(QA_XLS_HEADER21);
            headerArray.Add(QA_XLS_HEADER22);
            headerArray.Add(QA_XLS_HEADER23);
            headerArray.Add(QA_XLS_HEADER24);
            headerArray.Add(QA_XLS_HEADER25);
            headerArray.Add(QA_XLS_HEADER26);
            headerArray.Add(QA_XLS_HEADER27);
            headerArray.Add(QA_XLS_HEADER28);
            headerArray.Add(QA_XLS_HEADER29);
            headerArray.Add(QA_XLS_HEADER30);
            headerArray.Add(QA_XLS_HEADER31);
            headerArray.Add(QA_XLS_HEADER32);
            headerArray.Add(QA_XLS_HEADER33);
            headerArray.Add(QA_XLS_HEADER34);
            headerArray.Add(QA_XLS_HEADER35);
            headerArray.Add(QA_XLS_HEADER36);

            qaResults.addRow(headerArray);

            //////////////////////////////////////////////////////////////////////////

            QA_XLS_COL1_VALUE = _overallPercStableArea.ToString("g");

            //CStringArray rowArray;
            List<string> rowArray = new List<string>();
            rowArray.Add(QA_XLS_COL1_VALUE);
            rowArray.Add(QA_XLS_COL2_VALUE);
            rowArray.Add(QA_XLS_COL3_VALUE);
            rowArray.Add(QA_XLS_COL4_VALUE);
            rowArray.Add(QA_XLS_COL5_VALUE);
            rowArray.Add(QA_XLS_COL6_VALUE);
            rowArray.Add(QA_XLS_COL7_VALUE);
            rowArray.Add(QA_XLS_COL8_VALUE);
            rowArray.Add(QA_XLS_COL9_VALUE);
            rowArray.Add(QA_XLS_COL10_VALUE);
            rowArray.Add(QA_XLS_COL11_VALUE);
            rowArray.Add(QA_XLS_COL12_VALUE);
            rowArray.Add(QA_XLS_COL13_VALUE);
            rowArray.Add(QA_XLS_COL14_VALUE);
            rowArray.Add(QA_XLS_COL15_VALUE);
            rowArray.Add(QA_XLS_COL16_VALUE);
            rowArray.Add(QA_XLS_COL17_VALUE);
            rowArray.Add(QA_XLS_COL18_VALUE);
            rowArray.Add(QA_XLS_COL19_VALUE);
            rowArray.Add(QA_XLS_COL20_VALUE);
            rowArray.Add(QA_XLS_COL21_VALUE);
            rowArray.Add(QA_XLS_COL22_VALUE);
            rowArray.Add(QA_XLS_COL23_VALUE);
            rowArray.Add(QA_XLS_COL24_VALUE);
            rowArray.Add(QA_XLS_COL25_VALUE);
            rowArray.Add(QA_XLS_COL26_VALUE);
            rowArray.Add(QA_XLS_COL27_VALUE);
            rowArray.Add(QA_XLS_COL28_VALUE);
            rowArray.Add(QA_XLS_COL29_VALUE);
            rowArray.Add(QA_XLS_COL30_VALUE);
            rowArray.Add(QA_XLS_COL31_VALUE);
            rowArray.Add(QA_XLS_COL32_VALUE);
            rowArray.Add(QA_XLS_COL33_VALUE);
            rowArray.Add(QA_XLS_COL34_VALUE);
            rowArray.Add(QA_XLS_COL35_VALUE);
            rowArray.Add(QA_XLS_COL36_VALUE);
            qaResults.addRow(rowArray);

            QA_XLS_COL1_VALUE = "";
            QA_XLS_COL10_VALUE = "";

            //////////////////////////////////////////////////////////////////////////

            int QA_XLS_NUM_ROWS_PER_SM = attribArray.Count;

            int numModes = _numForwardModes;
            if (numModes > _numReverseModes)
                numModes = _numReverseModes;

            for (int i = 0; i < numModes; i++)
            {
                for (int j = 0; j < QA_XLS_NUM_ROWS_PER_SM; j++)
                {
                    // clear vars
                    rowArray.Clear();
                    QA_XLS_COL1_VALUE = "";
                    QA_XLS_COL2_VALUE = "";
                    QA_XLS_COL3_VALUE = "";
                    QA_XLS_COL4_VALUE = "";
                    QA_XLS_COL5_VALUE = "";
                    QA_XLS_COL6_VALUE = "";
                    QA_XLS_COL7_VALUE = "";
                    QA_XLS_COL8_VALUE = "";
                    QA_XLS_COL9_VALUE = "";
                    QA_XLS_COL10_VALUE = "";
                    QA_XLS_COL11_VALUE = "";
                    QA_XLS_COL12_VALUE = "";
                    QA_XLS_COL13_VALUE = "";
                    QA_XLS_COL14_VALUE = "";
                    QA_XLS_COL15_VALUE = "";
                    QA_XLS_COL16_VALUE = "";
                    QA_XLS_COL17_VALUE = "";
                    QA_XLS_COL18_VALUE = "";
                    QA_XLS_COL19_VALUE = "";
                    QA_XLS_COL20_VALUE = "";
                    QA_XLS_COL21_VALUE = "";
                    QA_XLS_COL22_VALUE = "";
                    QA_XLS_COL23_VALUE = "";
                    QA_XLS_COL24_VALUE = "";
                    QA_XLS_COL25_VALUE = "";
                    QA_XLS_COL26_VALUE = "";
                    QA_XLS_COL27_VALUE = "";
                    QA_XLS_COL28_VALUE = "";
                    QA_XLS_COL29_VALUE = "";
                    QA_XLS_COL30_VALUE = "";
                    QA_XLS_COL31_VALUE = "";
                    QA_XLS_COL32_VALUE = "";
                    QA_XLS_COL33_VALUE = "";
                    QA_XLS_COL34_VALUE = "";
                    QA_XLS_COL35_VALUE = "";
                    QA_XLS_COL36_VALUE = "";

                    QA_XLS_COL1_VALUE = "";
                    QA_XLS_COL2_VALUE += i.ToString("d");//.AppendFormat("%d", i);
                    QA_XLS_COL3_VALUE = attribArray[j];

                    if (QA_XLS_COL3_VALUE == QA_XLS_MAX)
                    {
                        if (i < _maxForwardLowerModeSlopes.Count)
                            QA_XLS_COL4_VALUE += _maxForwardLowerModeSlopes[i].ToString("g");
                        else
                            QA_XLS_COL4_VALUE = "";
                        if (i < _maxForwardUpperModeSlopes.Count)
                            QA_XLS_COL5_VALUE += _maxForwardUpperModeSlopes[i].ToString("g");
                        else
                            QA_XLS_COL5_VALUE = "";
                        if (i < _maxReverseLowerModeSlopes.Count)
                            QA_XLS_COL6_VALUE += _maxReverseLowerModeSlopes[i].ToString("g");
                        else
                            QA_XLS_COL6_VALUE = "";
                        if (i < _maxReverseUpperModeSlopes.Count)
                            QA_XLS_COL7_VALUE += _maxReverseUpperModeSlopes[i].ToString("g");
                        else
                            QA_XLS_COL7_VALUE = "";
                        if (i < _maxStableAreaWidths.Count)
                            QA_XLS_COL8_VALUE += _maxStableAreaWidths[i].ToString("g");
                        else
                            QA_XLS_COL8_VALUE = "";
                        if (i < _maxReverseModeWidths.Count)
                            QA_XLS_COL9_VALUE += _maxReverseModeWidths[i].ToString("g");
                        else
                            QA_XLS_COL9_VALUE = "";
                        if (i < _maxForwardModeWidths.Count)
                            QA_XLS_COL10_VALUE += _maxForwardModeWidths[i].ToString("g");
                        else
                            QA_XLS_COL10_VALUE = "";
                        if (i < _maxPercWorkingRegions.Count)
                            QA_XLS_COL11_VALUE += _maxPercWorkingRegions[i].ToString("g");
                        else
                            QA_XLS_COL11_VALUE = "";
                    }
                    else if (QA_XLS_COL3_VALUE == QA_XLS_MIN)
                    {
                        if (i < _minForwardLowerModeSlopes.Count)
                            QA_XLS_COL4_VALUE += _minForwardLowerModeSlopes[i].ToString("g");
                        else
                            QA_XLS_COL4_VALUE = "";
                        if (i < _minForwardUpperModeSlopes.Count)
                            QA_XLS_COL5_VALUE += _minForwardUpperModeSlopes[i].ToString("g");
                        else
                            QA_XLS_COL5_VALUE = "";
                        if (i < _minReverseLowerModeSlopes.Count)
                            QA_XLS_COL6_VALUE += _minReverseLowerModeSlopes[i].ToString("g");
                        else
                            QA_XLS_COL6_VALUE = "";
                        if (i < _minReverseUpperModeSlopes.Count)
                            QA_XLS_COL7_VALUE += _minReverseUpperModeSlopes[i].ToString("g");
                        else
                            QA_XLS_COL7_VALUE = "";
                        if (i < _minStableAreaWidths.Count)
                            QA_XLS_COL8_VALUE += _minStableAreaWidths[i].ToString("g");
                        else
                            QA_XLS_COL8_VALUE = "";
                        if (i < _minReverseModeWidths.Count)
                            QA_XLS_COL9_VALUE += _minReverseModeWidths[i].ToString("g");
                        else
                            QA_XLS_COL9_VALUE = "";
                        if (i < _minForwardModeWidths.Count)
                            QA_XLS_COL10_VALUE += _minForwardModeWidths[i].ToString("g");
                        else
                            QA_XLS_COL10_VALUE = "";
                        if (i < _minPercWorkingRegions.Count)
                            QA_XLS_COL11_VALUE += _minPercWorkingRegions[i].ToString("g");
                        else
                            QA_XLS_COL11_VALUE = "";
                    }
                    else if (QA_XLS_COL3_VALUE == QA_XLS_MEAN)
                    {
                        if (i < _meanForwardLowerModeSlopes.Count)
                            QA_XLS_COL4_VALUE += _meanForwardLowerModeSlopes[i].ToString("g");
                        else
                            QA_XLS_COL4_VALUE = "";
                        if (i < _meanForwardUpperModeSlopes.Count)
                            QA_XLS_COL5_VALUE += _meanForwardUpperModeSlopes[i].ToString("g");
                        else
                            QA_XLS_COL5_VALUE = "";
                        if (i < _meanReverseLowerModeSlopes.Count)
                            QA_XLS_COL6_VALUE += _meanReverseLowerModeSlopes[i].ToString("g");
                        else
                            QA_XLS_COL6_VALUE = "";
                        if (i < _meanReverseUpperModeSlopes.Count)
                            QA_XLS_COL7_VALUE += _meanReverseUpperModeSlopes[i].ToString("g");
                        else
                            QA_XLS_COL7_VALUE = "";
                        if (i < _meanStableAreaWidths.Count)
                            QA_XLS_COL8_VALUE += _meanStableAreaWidths[i].ToString("g");
                        else
                            QA_XLS_COL8_VALUE = "";
                        if (i < _meanReverseModeWidths.Count)
                            QA_XLS_COL9_VALUE += _meanReverseModeWidths[i].ToString("g");
                        else
                            QA_XLS_COL9_VALUE = "";
                        if (i < _meanForwardModeWidths.Count)
                            QA_XLS_COL10_VALUE += _meanForwardModeWidths[i].ToString("g");
                        else
                            QA_XLS_COL10_VALUE = "";
                        if (i < _meanPercWorkingRegions.Count)
                            QA_XLS_COL11_VALUE += _meanPercWorkingRegions[i].ToString("g");
                        else
                            QA_XLS_COL11_VALUE = "";
                    }
                    else if (QA_XLS_COL3_VALUE == QA_XLS_SUM)
                    {
                        if (i < _sumForwardLowerModeSlopes.Count)
                            QA_XLS_COL4_VALUE += _sumForwardLowerModeSlopes[i].ToString("g");
                        else
                            QA_XLS_COL4_VALUE = "";
                        if (i < _sumForwardUpperModeSlopes.Count)
                            QA_XLS_COL5_VALUE += _sumForwardUpperModeSlopes[i].ToString("g");
                        else
                            QA_XLS_COL5_VALUE = "";
                        if (i < _sumReverseLowerModeSlopes.Count)
                            QA_XLS_COL6_VALUE += _sumReverseLowerModeSlopes[i].ToString("g");
                        else
                            QA_XLS_COL6_VALUE = "";
                        if (i < _sumReverseUpperModeSlopes.Count)
                            QA_XLS_COL7_VALUE += _sumReverseUpperModeSlopes[i].ToString("g");
                        else
                            QA_XLS_COL7_VALUE = "";
                        if (i < _sumStableAreaWidths.Count)
                            QA_XLS_COL8_VALUE += _sumStableAreaWidths[i].ToString("g");
                        else
                            QA_XLS_COL8_VALUE = "";
                        if (i < _sumReverseModeWidths.Count)
                            QA_XLS_COL9_VALUE += _sumReverseModeWidths[i].ToString("g");
                        else
                            QA_XLS_COL9_VALUE = "";
                        if (i < _sumForwardModeWidths.Count)
                            QA_XLS_COL10_VALUE += _sumForwardModeWidths[i].ToString("g");
                        else
                            QA_XLS_COL10_VALUE = "";
                        if (i < _sumPercWorkingRegions.Count)
                            QA_XLS_COL11_VALUE += _sumPercWorkingRegions[i].ToString("g");
                        else
                            QA_XLS_COL11_VALUE = "";
                    }
                    else if (QA_XLS_COL3_VALUE == QA_XLS_COUNT)
                    {
                        if (i < _numForwardLowerModeSlopeSamples.Count)
                            QA_XLS_COL4_VALUE += _numForwardLowerModeSlopeSamples[i].ToString("g");
                        else
                            QA_XLS_COL4_VALUE = "";
                        if (i < _numForwardUpperModeSlopeSamples.Count)
                            QA_XLS_COL5_VALUE += _numForwardUpperModeSlopeSamples[i].ToString("g");
                        else
                            QA_XLS_COL5_VALUE = "";
                        if (i < _numReverseLowerModeSlopeSamples.Count)
                            QA_XLS_COL6_VALUE += _numReverseLowerModeSlopeSamples[i].ToString("g");
                        else
                            QA_XLS_COL6_VALUE = "";
                        if (i < _numReverseUpperModeSlopeSamples.Count)
                            QA_XLS_COL7_VALUE += _numReverseUpperModeSlopeSamples[i].ToString("g");
                        else
                            QA_XLS_COL7_VALUE = "";
                        if (i < _numStableAreaWidthSamples.Count)
                            QA_XLS_COL8_VALUE += _numStableAreaWidthSamples[i].ToString("g");
                        else
                            QA_XLS_COL8_VALUE = "";
                        if (i < _numReverseModeWidthSamples.Count)
                            QA_XLS_COL9_VALUE += _numReverseModeWidthSamples[i].ToString("g");
                        else
                            QA_XLS_COL9_VALUE = "";
                        if (i < _numForwardModeWidthSamples.Count)
                            QA_XLS_COL10_VALUE += _numForwardModeWidthSamples[i].ToString("g");
                        else
                            QA_XLS_COL10_VALUE = "";
                        if (i < _numPercWorkingRegionSamples.Count)
                            QA_XLS_COL11_VALUE += _numPercWorkingRegionSamples[i].ToString("g");
                        else
                            QA_XLS_COL11_VALUE = "";
                    }
                    else if (QA_XLS_COL3_VALUE == QA_XLS_STDDEV)
                    {
                        if (i < _stdDevForwardLowerModeSlopes.Count)
                            QA_XLS_COL4_VALUE += _stdDevForwardLowerModeSlopes[i].ToString("g");
                        else
                            QA_XLS_COL4_VALUE = "";
                        if (i < _stdDevForwardUpperModeSlopes.Count)
                            QA_XLS_COL5_VALUE += _stdDevForwardUpperModeSlopes[i].ToString("g");
                        else
                            QA_XLS_COL5_VALUE = "";
                        if (i < _stdDevReverseLowerModeSlopes.Count)
                            QA_XLS_COL6_VALUE += _stdDevReverseLowerModeSlopes[i].ToString("g");
                        else
                            QA_XLS_COL6_VALUE = "";
                        if (i < _stdDevReverseUpperModeSlopes.Count)
                            QA_XLS_COL7_VALUE += _stdDevReverseUpperModeSlopes[i].ToString("g");
                        else
                            QA_XLS_COL7_VALUE = "";
                        if (i < _stdDevStableAreaWidths.Count)
                            QA_XLS_COL8_VALUE += _stdDevStableAreaWidths[i].ToString("g");
                        else
                            QA_XLS_COL8_VALUE = "";
                        if (i < _stdDevReverseModeWidths.Count)
                            QA_XLS_COL9_VALUE += _stdDevReverseModeWidths[i].ToString("g");
                        else
                            QA_XLS_COL9_VALUE = "";
                        if (i < _stdDevForwardModeWidths.Count)
                            QA_XLS_COL10_VALUE += _stdDevForwardModeWidths[i].ToString("g");
                        else
                            QA_XLS_COL10_VALUE = "";
                        if (i < _stdDevPercWorkingRegions.Count)
                            QA_XLS_COL11_VALUE += _stdDevPercWorkingRegions[i].ToString("g");
                        else
                            QA_XLS_COL11_VALUE = "";
                    }
                    else if (QA_XLS_COL3_VALUE == QA_XLS_VAR)
                    {
                        if (i < _varForwardLowerModeSlopes.Count)
                            QA_XLS_COL4_VALUE += _varForwardLowerModeSlopes[i].ToString("g");
                        else
                            QA_XLS_COL4_VALUE = "";
                        if (i < _varForwardUpperModeSlopes.Count)
                            QA_XLS_COL5_VALUE += _varForwardUpperModeSlopes[i].ToString("g");
                        else
                            QA_XLS_COL5_VALUE = "";
                        if (i < _varReverseLowerModeSlopes.Count)
                            QA_XLS_COL6_VALUE += _varReverseLowerModeSlopes[i].ToString("g");
                        else
                            QA_XLS_COL6_VALUE = "";
                        if (i < _varReverseUpperModeSlopes.Count)
                            QA_XLS_COL7_VALUE += _varReverseUpperModeSlopes[i].ToString("g");
                        else
                            QA_XLS_COL7_VALUE = "";
                        if (i < _varStableAreaWidths.Count)
                            QA_XLS_COL8_VALUE += _varStableAreaWidths[i].ToString("g");
                        else
                            QA_XLS_COL8_VALUE = "";
                        if (i < _varReverseModeWidths.Count)
                            QA_XLS_COL9_VALUE += _varReverseModeWidths[i].ToString("g");
                        else
                            QA_XLS_COL9_VALUE = "";
                        if (i < _varForwardModeWidths.Count)
                            QA_XLS_COL10_VALUE += _varForwardModeWidths[i].ToString("g");
                        else
                            QA_XLS_COL10_VALUE = "";
                        if (i < _varPercWorkingRegions.Count)
                            QA_XLS_COL11_VALUE += _varPercWorkingRegions[i].ToString("g");
                        else
                            QA_XLS_COL11_VALUE = "";
                    }
                    else if (QA_XLS_COL3_VALUE == QA_XLS_OTHERS)
                    {
                        if (i < _forwardLowerModalDistortionAngles.Count)
                            QA_XLS_COL12_VALUE += _forwardLowerModalDistortionAngles[i].ToString("g");
                        else
                            QA_XLS_COL12_VALUE = "";
                        if (i < _forwardLowerModalDistortionAngleXPositions.Count)
                            QA_XLS_COL13_VALUE += _forwardLowerModalDistortionAngleXPositions[i].ToString("g");
                        else
                            QA_XLS_COL13_VALUE = "";
                        if (i < _forwardUpperModalDistortionAngles.Count)
                            QA_XLS_COL14_VALUE += _forwardUpperModalDistortionAngles[i].ToString("g");
                        else
                            QA_XLS_COL14_VALUE = "";
                        if (i < _forwardUpperModalDistortionAngleXPositions.Count)
                            QA_XLS_COL15_VALUE += _forwardUpperModalDistortionAngleXPositions[i].ToString("g");
                        else
                            QA_XLS_COL15_VALUE = "";
                        if (i < _reverseLowerModalDistortionAngles.Count)
                            QA_XLS_COL16_VALUE += _reverseLowerModalDistortionAngles[i].ToString("g");
                        else
                            QA_XLS_COL16_VALUE = "";
                        if (i < _reverseLowerModalDistortionAngleXPositions.Count)
                            QA_XLS_COL17_VALUE += _reverseLowerModalDistortionAngleXPositions[i].ToString("g");
                        else
                            QA_XLS_COL17_VALUE = "";
                        if (i < _reverseUpperModalDistortionAngles.Count)
                            QA_XLS_COL18_VALUE += _reverseUpperModalDistortionAngles[i].ToString("g");
                        else
                            QA_XLS_COL18_VALUE = "";
                        if (i < _reverseUpperModalDistortionAngleXPositions.Count)
                            QA_XLS_COL19_VALUE += _reverseUpperModalDistortionAngleXPositions[i].ToString("g");
                        else
                            QA_XLS_COL19_VALUE = "";
                        if (i < _maxForwardLowerModeBDCAreaXLengths.Count)
                            QA_XLS_COL20_VALUE += _maxForwardLowerModeBDCAreaXLengths[i].ToString("g");
                        else
                            QA_XLS_COL20_VALUE = "";
                        if (i < _maxForwardUpperModeBDCAreaXLengths.Count)
                            QA_XLS_COL21_VALUE += _maxForwardUpperModeBDCAreaXLengths[i].ToString("g");
                        else
                            QA_XLS_COL21_VALUE = "";
                        if (i < _maxForwardLowerModeBDCAreas.Count)
                            QA_XLS_COL22_VALUE += _maxForwardLowerModeBDCAreas[i].ToString("g");
                        else
                            QA_XLS_COL22_VALUE = "";
                        if (i < _maxForwardUpperModeBDCAreas.Count)
                            QA_XLS_COL23_VALUE += _maxForwardUpperModeBDCAreas[i].ToString("g");
                        else
                            QA_XLS_COL23_VALUE = "";
                        if (i < _sumForwardLowerModeBDCAreas.Count)
                            QA_XLS_COL24_VALUE += _sumForwardLowerModeBDCAreas[i].ToString("g");
                        else
                            QA_XLS_COL24_VALUE = "";
                        if (i < _sumForwardUpperModeBDCAreas.Count)
                            QA_XLS_COL25_VALUE += _sumForwardUpperModeBDCAreas[i].ToString("g");
                        else
                            QA_XLS_COL25_VALUE = "";
                        if (i < _numForwardLowerModeBDCAreaSamples.Count)
                            QA_XLS_COL26_VALUE += _numForwardLowerModeBDCAreaSamples[i].ToString("g");
                        else
                            QA_XLS_COL26_VALUE = "";
                        if (i < _numForwardUpperModeBDCAreaSamples.Count)
                            QA_XLS_COL27_VALUE += _numForwardUpperModeBDCAreaSamples[i].ToString("g");
                        else
                            QA_XLS_COL27_VALUE = "";
                        if (i < _maxReverseLowerModeBDCAreaXLengths.Count)
                            QA_XLS_COL28_VALUE += _maxReverseLowerModeBDCAreaXLengths[i].ToString("g");
                        else
                            QA_XLS_COL28_VALUE = "";
                        if (i < _maxReverseUpperModeBDCAreaXLengths.Count)
                            QA_XLS_COL29_VALUE += _maxReverseUpperModeBDCAreaXLengths[i].ToString("g");
                        else
                            QA_XLS_COL29_VALUE = "";
                        if (i < _maxReverseLowerModeBDCAreas.Count)
                            QA_XLS_COL30_VALUE += _maxReverseLowerModeBDCAreas[i].ToString("g");
                        else
                            QA_XLS_COL30_VALUE = "";
                        if (i < _maxReverseUpperModeBDCAreas.Count)
                            QA_XLS_COL31_VALUE += _maxReverseUpperModeBDCAreas[i].ToString("g");
                        else
                            QA_XLS_COL31_VALUE = "";
                        if (i < _sumReverseLowerModeBDCAreas.Count)
                            QA_XLS_COL32_VALUE += _sumReverseLowerModeBDCAreas[i].ToString("g");
                        else
                            QA_XLS_COL32_VALUE = "";
                        if (i < _sumReverseUpperModeBDCAreas.Count)
                            QA_XLS_COL33_VALUE += _sumReverseUpperModeBDCAreas[i].ToString("g");
                        else
                            QA_XLS_COL33_VALUE = "";
                        if (i < _numReverseLowerModeBDCAreaSamples.Count)
                            QA_XLS_COL34_VALUE += _numReverseLowerModeBDCAreaSamples[i].ToString("g");
                        else
                            QA_XLS_COL34_VALUE = "";
                        if (i < _numReverseUpperModeBDCAreaSamples.Count)
                            QA_XLS_COL35_VALUE += _numReverseUpperModeBDCAreaSamples[i].ToString("g");
                        else
                            QA_XLS_COL35_VALUE = "";
                        if (i < _middleLineRMSValues.Count)
                            QA_XLS_COL36_VALUE += _middleLineRMSValues[i].ToString("g");
                        else
                            QA_XLS_COL36_VALUE = "";
                    }

                    rowArray.Clear();
                    rowArray.Add(QA_XLS_COL1_VALUE);
                    rowArray.Add(QA_XLS_COL2_VALUE);
                    rowArray.Add(QA_XLS_COL3_VALUE);
                    rowArray.Add(QA_XLS_COL4_VALUE);
                    rowArray.Add(QA_XLS_COL5_VALUE);
                    rowArray.Add(QA_XLS_COL6_VALUE);
                    rowArray.Add(QA_XLS_COL7_VALUE);
                    rowArray.Add(QA_XLS_COL8_VALUE);
                    rowArray.Add(QA_XLS_COL9_VALUE);
                    rowArray.Add(QA_XLS_COL10_VALUE);
                    rowArray.Add(QA_XLS_COL11_VALUE);
                    rowArray.Add(QA_XLS_COL12_VALUE);
                    rowArray.Add(QA_XLS_COL13_VALUE);
                    rowArray.Add(QA_XLS_COL14_VALUE);
                    rowArray.Add(QA_XLS_COL15_VALUE);
                    rowArray.Add(QA_XLS_COL16_VALUE);
                    rowArray.Add(QA_XLS_COL17_VALUE);
                    rowArray.Add(QA_XLS_COL18_VALUE);
                    rowArray.Add(QA_XLS_COL19_VALUE);
                    rowArray.Add(QA_XLS_COL20_VALUE);
                    rowArray.Add(QA_XLS_COL21_VALUE);
                    rowArray.Add(QA_XLS_COL22_VALUE);
                    rowArray.Add(QA_XLS_COL23_VALUE);
                    rowArray.Add(QA_XLS_COL24_VALUE);
                    rowArray.Add(QA_XLS_COL25_VALUE);
                    rowArray.Add(QA_XLS_COL26_VALUE);
                    rowArray.Add(QA_XLS_COL27_VALUE);
                    rowArray.Add(QA_XLS_COL28_VALUE);
                    rowArray.Add(QA_XLS_COL29_VALUE);
                    rowArray.Add(QA_XLS_COL30_VALUE);
                    rowArray.Add(QA_XLS_COL31_VALUE);
                    rowArray.Add(QA_XLS_COL32_VALUE);
                    rowArray.Add(QA_XLS_COL33_VALUE);
                    rowArray.Add(QA_XLS_COL34_VALUE);
                    rowArray.Add(QA_XLS_COL35_VALUE);
                    rowArray.Add(QA_XLS_COL36_VALUE);

                    qaResults.addRow(rowArray);
                }

                qaResults.addEmptyLine();

            }

            return err;
        }


        /// <summary>
        /// (CLoseGridFile& passFailResults )
        /// </summary>
        /// <param name="passFailResults"></param>
        /// <returns></returns>
        internal rcode writePassFailResultsToFile(CLoseGridFile passFailResults)//(CLoseGridFile& passFailResults )
        {
            rcode err = rcode.ok;

            if (_passFailAnalysisComplete)
            {
                String errorMessage = "";
                int errorNum = 0;
                getQAError(_qaDetectedError, out errorNum, out errorMessage);

                //CStringArray error_msg_row;
                List<string> error_msg_row = new List<string>();
                error_msg_row.Add(errorMessage);
                passFailResults.addRow(error_msg_row);
                passFailResults.addEmptyLine();

                //////////////////////////////////////////

                //CStringArray row;
                List<string> row = new List<string>();
                String cellString = "";

                row.Clear();
                row.Add("Slope Window Size");
                cellString = DSDBR01.Instance._DSDBR01_SMQA_slope_window_size.ToString("d");
                row.Add(cellString);
                passFailResults.addRow(row);

                row.Clear();
                row.Add("Modal Distortion Min X");
                cellString= DSDBR01.Instance._DSDBR01_SMQA_modal_distortion_min_x.ToString("g");
                row.Add(cellString);
                passFailResults.addRow(row);

                row.Clear();
                row.Add("Modal Distortion Max X");
                cellString = DSDBR01.Instance._DSDBR01_SMQA_modal_distortion_max_x.ToString("g");
                row.Add(cellString);
                passFailResults.addRow(row);

                row.Clear();
                row.Add("Modal Distortion Min Y");
                cellString= DSDBR01.Instance._DSDBR01_SMQA_modal_distortion_min_y.ToString("g");
                row.Add(cellString);
                passFailResults.addRow(row);

                passFailResults.addEmptyLine();

                //////////////////////////////////////////

                const String QA_XLS_HEADER1 = "";
                const String QA_XLS_HEADER2 = "Upper line mode borders removed due to error";
                const String QA_XLS_HEADER3 = "Lower line mode borders removed due to error";
                const String QA_XLS_HEADER4 = "Middle lines removed due to error";
                const String QA_XLS_HEADER5 = "Forward Max Mode Width";
                const String QA_XLS_HEADER6 = "Reverse Max Mode Width";
                const String QA_XLS_HEADER7 = "Mean % Working Region";
                const String QA_XLS_HEADER8 = "Min % Working Region";
                const String QA_XLS_HEADER9 = "Forward Min Mode Width";
                const String QA_XLS_HEADER10 = "Reverse Min Mode Width";
                const String QA_XLS_HEADER11 = "Forward Max Lower Modal Distortion Angle";
                const String QA_XLS_HEADER12 = "Forward Max Lower Modal Distortion X-Position";
                const String QA_XLS_HEADER13 = "Forward Max Upper Modal Distortion Angle";
                const String QA_XLS_HEADER14 = "Forward Max Upper Modal Distortion X-Position";
                const String QA_XLS_HEADER15 = "Reverse Max Lower Modal Distortion Angle";
                const String QA_XLS_HEADER16 = "Reverse Max Lower Modal Distortion X-Position";
                const String QA_XLS_HEADER17 = "Reverse Max Upper Modal Distortion Angle";
                const String QA_XLS_HEADER18 = "Reverse Max Upper Modal Distortion X-Position";
                const String QA_XLS_HEADER19 = "Forward Lower Max Boundary Direction Change X-Length";
                const String QA_XLS_HEADER20 = "Forward Upper Max Boundary Direction Change X-Length";
                const String QA_XLS_HEADER21 = "Forward Lower Max Boundary Direction Change Area";
                const String QA_XLS_HEADER22 = "Forward Upper Max Boundary Direction Change Area";
                const String QA_XLS_HEADER23 = "Forward Lower Sum of Boundary Direction Change Areas";
                const String QA_XLS_HEADER24 = "Forward Upper Sum of Boundary Direction Change Areas";
                const String QA_XLS_HEADER25 = "Forward Lower Number of Boundary Direction Changes";
                const String QA_XLS_HEADER26 = "Forward Upper Number of Boundary Direction Changes";
                const String QA_XLS_HEADER27 = "Reverse Lower Max Boundary Direction Change X-Length";
                const String QA_XLS_HEADER28 = "Reverse Upper Max Boundary Direction Change X-Length";
                const String QA_XLS_HEADER29 = "Reverse Lower Max Boundary Direction Change Area";
                const String QA_XLS_HEADER30 = "Reverse Upper Max Boundary Direction Change Area";
                const String QA_XLS_HEADER31 = "Reverse Lower Sum of Boundary Direction Change Areas";
                const String QA_XLS_HEADER32 = "Reverse Upper Sum of Boundary Direction Change Areas";
                const String QA_XLS_HEADER33 = "Reverse Lower Number of Boundary Direction Changes";
                const String QA_XLS_HEADER34 = "Reverse Upper Number of Boundary Direction Changes";
                const String QA_XLS_HEADER35 = "Forward Lower Max Mode Slope";
                const String QA_XLS_HEADER36 = "Forward Upper Max Mode Slope";
                const String QA_XLS_HEADER37 = "Reverse Lower Max Mode Slope";
                const String QA_XLS_HEADER38 = "Reverse Upper Max Mode Slope";
                const String QA_XLS_HEADER39 = "Max Middle Line RMS Value";
                const String QA_XLS_HEADER40 = "Min Middle Line Slope";
                const String QA_XLS_HEADER41 = "Max Middle Line Slope";


                const String QA_XLS_ROWTITLE1 = "Pass/Fail Result";
                const String QA_XLS_ROWTITLE2 = "Min Value Measured";
                const String QA_XLS_ROWTITLE3 = "Max Value Measured";
                const String QA_XLS_ROWTITLE4 = "Number of Failed LMs";
                const String QA_XLS_ROWTITLE5 = "Threshold Applied";

                String QA_XLS_COL1_VALUE = "";
                String QA_XLS_COL2_VALUE = "";
                String QA_XLS_COL3_VALUE = "";
                String QA_XLS_COL4_VALUE = "";
                String QA_XLS_COL5_VALUE = "";
                String QA_XLS_COL6_VALUE = "";
                String QA_XLS_COL7_VALUE = "";
                String QA_XLS_COL8_VALUE = "";
                String QA_XLS_COL9_VALUE = "";
                String QA_XLS_COL10_VALUE = "";
                String QA_XLS_COL11_VALUE = "";
                String QA_XLS_COL12_VALUE = "";
                String QA_XLS_COL13_VALUE = "";
                String QA_XLS_COL14_VALUE = "";
                String QA_XLS_COL15_VALUE = "";
                String QA_XLS_COL16_VALUE = "";
                String QA_XLS_COL17_VALUE = "";
                String QA_XLS_COL18_VALUE = "";
                String QA_XLS_COL19_VALUE = "";
                String QA_XLS_COL20_VALUE = "";
                String QA_XLS_COL21_VALUE = "";
                String QA_XLS_COL22_VALUE = "";
                String QA_XLS_COL23_VALUE = "";
                String QA_XLS_COL24_VALUE = "";
                String QA_XLS_COL25_VALUE = "";
                String QA_XLS_COL26_VALUE = "";
                String QA_XLS_COL27_VALUE = "";
                String QA_XLS_COL28_VALUE = "";
                String QA_XLS_COL29_VALUE = "";
                String QA_XLS_COL30_VALUE = "";
                String QA_XLS_COL31_VALUE = "";
                String QA_XLS_COL32_VALUE = "";
                String QA_XLS_COL33_VALUE = "";
                String QA_XLS_COL34_VALUE = "";
                String QA_XLS_COL35_VALUE = "";
                String QA_XLS_COL36_VALUE = "";
                String QA_XLS_COL37_VALUE = "";
                String QA_XLS_COL38_VALUE = "";
                String QA_XLS_COL39_VALUE = "";
                String QA_XLS_COL40_VALUE = "";
                String QA_XLS_COL41_VALUE = "";

                //CStringArray headerArray;
                List<string> headerArray=new List<string>();
                headerArray.Add(QA_XLS_HEADER1);
                headerArray.Add(QA_XLS_HEADER2);
                headerArray.Add(QA_XLS_HEADER3);
                headerArray.Add(QA_XLS_HEADER4);
                headerArray.Add(QA_XLS_HEADER5);
                headerArray.Add(QA_XLS_HEADER6);
                headerArray.Add(QA_XLS_HEADER7);
                headerArray.Add(QA_XLS_HEADER8);
                headerArray.Add(QA_XLS_HEADER9);
                headerArray.Add(QA_XLS_HEADER10);
                headerArray.Add(QA_XLS_HEADER11);
                headerArray.Add(QA_XLS_HEADER12);
                headerArray.Add(QA_XLS_HEADER13);
                headerArray.Add(QA_XLS_HEADER14);
                headerArray.Add(QA_XLS_HEADER15);
                headerArray.Add(QA_XLS_HEADER16);
                headerArray.Add(QA_XLS_HEADER17);
                headerArray.Add(QA_XLS_HEADER18);
                headerArray.Add(QA_XLS_HEADER19);
                headerArray.Add(QA_XLS_HEADER20);
                headerArray.Add(QA_XLS_HEADER21);
                headerArray.Add(QA_XLS_HEADER22);
                headerArray.Add(QA_XLS_HEADER23);
                headerArray.Add(QA_XLS_HEADER24);
                headerArray.Add(QA_XLS_HEADER25);
                headerArray.Add(QA_XLS_HEADER26);
                headerArray.Add(QA_XLS_HEADER27);
                headerArray.Add(QA_XLS_HEADER28);
                headerArray.Add(QA_XLS_HEADER29);
                headerArray.Add(QA_XLS_HEADER30);
                headerArray.Add(QA_XLS_HEADER31);
                headerArray.Add(QA_XLS_HEADER32);
                headerArray.Add(QA_XLS_HEADER33);
                headerArray.Add(QA_XLS_HEADER34);
                headerArray.Add(QA_XLS_HEADER35);
                headerArray.Add(QA_XLS_HEADER36);
                headerArray.Add(QA_XLS_HEADER37);
                headerArray.Add(QA_XLS_HEADER38);
                headerArray.Add(QA_XLS_HEADER39);
                headerArray.Add(QA_XLS_HEADER40);
                headerArray.Add(QA_XLS_HEADER41);

                //CStringArray rowTitleArray;
                List<string> rowTitleArray=new List<string>();
                rowTitleArray.Add(QA_XLS_ROWTITLE1);
                rowTitleArray.Add(QA_XLS_ROWTITLE2);
                rowTitleArray.Add(QA_XLS_ROWTITLE3);
                rowTitleArray.Add(QA_XLS_ROWTITLE4);
                rowTitleArray.Add(QA_XLS_ROWTITLE5);


                // set headers
                passFailResults.addRow(headerArray);

                // add space
                passFailResults.addEmptyLine();


                ///////////////////////////////
                /// first row - pass/fails
                //
                QA_XLS_COL2_VALUE = _maxUpperModeBordersRemovedPFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL3_VALUE = _maxLowerModeBordersRemovedPFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL4_VALUE = _maxMiddleLineRemovedPFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL5_VALUE = _maxForwardModeWidthPFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL6_VALUE = _maxReverseModeWidthPFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL7_VALUE = _meanPercWorkingRegionPFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL8_VALUE = _minPercWorkingRegionPFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL9_VALUE = _minForwardModeWidthPFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL10_VALUE = _minReverseModeWidthPFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL11_VALUE = _forwardLowerModalDistortionAnglePFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL12_VALUE = _forwardLowerModalDistortionAngleXPositionPFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL13_VALUE = _forwardUpperModalDistortionAnglePFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL14_VALUE = _forwardUpperModalDistortionAngleXPositionPFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL15_VALUE = _reverseLowerModalDistortionAnglePFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL16_VALUE = _reverseLowerModalDistortionAngleXPositionPFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL17_VALUE = _reverseUpperModalDistortionAnglePFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL18_VALUE = _reverseUpperModalDistortionAngleXPositionPFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL19_VALUE = _maxForwardLowerModeBDCAreaXLengthPFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL20_VALUE = _maxForwardUpperModeBDCAreaXLengthPFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL21_VALUE = _maxForwardLowerModeBDCAreaPFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL22_VALUE = _maxForwardUpperModeBDCAreaPFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL23_VALUE = _sumForwardLowerModeBDCAreasPFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL24_VALUE = _sumForwardUpperModeBDCAreasPFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL25_VALUE = _numForwardLowerModeBDCAreasPFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL26_VALUE = _numForwardUpperModeBDCAreasPFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL27_VALUE = _maxReverseLowerModeBDCAreaXLengthPFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL28_VALUE = _maxReverseUpperModeBDCAreaXLengthPFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL29_VALUE = _maxReverseLowerModeBDCAreaPFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL30_VALUE = _maxReverseUpperModeBDCAreaPFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL31_VALUE = _sumReverseLowerModeBDCAreasPFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL32_VALUE = _sumReverseUpperModeBDCAreasPFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL33_VALUE = _numReverseLowerModeBDCAreasPFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL34_VALUE = _numReverseUpperModeBDCAreasPFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL35_VALUE = _maxForwardLowerModeSlopePFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL36_VALUE = _maxForwardUpperModeSlopePFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL37_VALUE = _maxReverseLowerModeSlopePFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL38_VALUE = _maxReverseUpperModeSlopePFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL39_VALUE = _middleLineRMSValuesPFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL40_VALUE = _minMiddleLineSlopePFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL41_VALUE = _maxMiddleLineSlopePFAnalysis.pass ? "PASS" : "FAIL";

                //CStringArray rowArray;
                List<string> rowArray = new List<string>();
                rowArray.Clear();
                rowArray.Add(QA_XLS_ROWTITLE1);
                rowArray.Add(QA_XLS_COL2_VALUE);
                rowArray.Add(QA_XLS_COL3_VALUE);
                rowArray.Add(QA_XLS_COL4_VALUE);
                rowArray.Add(QA_XLS_COL5_VALUE);
                rowArray.Add(QA_XLS_COL6_VALUE);
                rowArray.Add(QA_XLS_COL7_VALUE);
                rowArray.Add(QA_XLS_COL8_VALUE);
                rowArray.Add(QA_XLS_COL9_VALUE);
                rowArray.Add(QA_XLS_COL10_VALUE);
                rowArray.Add(QA_XLS_COL11_VALUE);
                rowArray.Add(QA_XLS_COL12_VALUE);
                rowArray.Add(QA_XLS_COL13_VALUE);
                rowArray.Add(QA_XLS_COL14_VALUE);
                rowArray.Add(QA_XLS_COL15_VALUE);
                rowArray.Add(QA_XLS_COL16_VALUE);
                rowArray.Add(QA_XLS_COL17_VALUE);
                rowArray.Add(QA_XLS_COL18_VALUE);
                rowArray.Add(QA_XLS_COL19_VALUE);
                rowArray.Add(QA_XLS_COL20_VALUE);
                rowArray.Add(QA_XLS_COL21_VALUE);
                rowArray.Add(QA_XLS_COL22_VALUE);
                rowArray.Add(QA_XLS_COL23_VALUE);
                rowArray.Add(QA_XLS_COL24_VALUE);
                rowArray.Add(QA_XLS_COL25_VALUE);
                rowArray.Add(QA_XLS_COL26_VALUE);
                rowArray.Add(QA_XLS_COL27_VALUE);
                rowArray.Add(QA_XLS_COL28_VALUE);
                rowArray.Add(QA_XLS_COL29_VALUE);
                rowArray.Add(QA_XLS_COL30_VALUE);
                rowArray.Add(QA_XLS_COL31_VALUE);
                rowArray.Add(QA_XLS_COL32_VALUE);
                rowArray.Add(QA_XLS_COL33_VALUE);
                rowArray.Add(QA_XLS_COL34_VALUE);
                rowArray.Add(QA_XLS_COL35_VALUE);
                rowArray.Add(QA_XLS_COL36_VALUE);
                rowArray.Add(QA_XLS_COL37_VALUE);
                rowArray.Add(QA_XLS_COL38_VALUE);
                rowArray.Add(QA_XLS_COL39_VALUE);
                rowArray.Add(QA_XLS_COL40_VALUE);
                rowArray.Add(QA_XLS_COL41_VALUE);
                passFailResults.addRow(rowArray);


                ////////////////////////////////////////
                //	num failed Modes
                //
                QA_XLS_COL2_VALUE = "";
                QA_XLS_COL3_VALUE = "";
                QA_XLS_COL4_VALUE = "";
                QA_XLS_COL5_VALUE = "";
                QA_XLS_COL6_VALUE = "";
                QA_XLS_COL7_VALUE = "";
                QA_XLS_COL8_VALUE = "";
                QA_XLS_COL9_VALUE = "";
                QA_XLS_COL10_VALUE = "";
                QA_XLS_COL11_VALUE = "";
                QA_XLS_COL12_VALUE = "";
                QA_XLS_COL13_VALUE = "";
                QA_XLS_COL14_VALUE = "";
                QA_XLS_COL15_VALUE = "";
                QA_XLS_COL16_VALUE = "";
                QA_XLS_COL17_VALUE = "";
                QA_XLS_COL18_VALUE = "";
                QA_XLS_COL19_VALUE = "";
                QA_XLS_COL20_VALUE = "";
                QA_XLS_COL21_VALUE = "";
                QA_XLS_COL22_VALUE = "";
                QA_XLS_COL23_VALUE = "";
                QA_XLS_COL24_VALUE = "";
                QA_XLS_COL25_VALUE = "";
                QA_XLS_COL26_VALUE = "";
                QA_XLS_COL27_VALUE = "";
                QA_XLS_COL28_VALUE = "";
                QA_XLS_COL29_VALUE = "";
                QA_XLS_COL30_VALUE = "";
                QA_XLS_COL31_VALUE = "";
                QA_XLS_COL32_VALUE = "";
                QA_XLS_COL33_VALUE = "";
                QA_XLS_COL34_VALUE = "";
                QA_XLS_COL35_VALUE = "";
                QA_XLS_COL36_VALUE = "";
                QA_XLS_COL37_VALUE = "";
                QA_XLS_COL38_VALUE = "";
                QA_XLS_COL39_VALUE = "";
                QA_XLS_COL40_VALUE = "";
                QA_XLS_COL41_VALUE = "";

                QA_XLS_COL2_VALUE= _numUpperLinesRemoved.ToString("d");
                QA_XLS_COL3_VALUE= _numLowerLinesRemoved.ToString("d");
                QA_XLS_COL4_VALUE=_numMiddleLinesRemoved.ToString("d");
                QA_XLS_COL5_VALUE += _maxForwardModeWidthPFAnalysis.numFailedModes.ToString();
                QA_XLS_COL6_VALUE+= _maxReverseModeWidthPFAnalysis.numFailedModes.ToString();
                QA_XLS_COL7_VALUE+= _meanPercWorkingRegionPFAnalysis.numFailedModes.ToString();
                QA_XLS_COL8_VALUE+= _minPercWorkingRegionPFAnalysis.numFailedModes.ToString();
                QA_XLS_COL9_VALUE+= _minForwardModeWidthPFAnalysis.numFailedModes.ToString();
                QA_XLS_COL10_VALUE+= _minReverseModeWidthPFAnalysis.numFailedModes.ToString();
                QA_XLS_COL11_VALUE+= _forwardLowerModalDistortionAnglePFAnalysis.numFailedModes.ToString();
                QA_XLS_COL12_VALUE+= _forwardLowerModalDistortionAngleXPositionPFAnalysis.numFailedModes.ToString();
                QA_XLS_COL13_VALUE+= _forwardUpperModalDistortionAnglePFAnalysis.numFailedModes.ToString();
                QA_XLS_COL14_VALUE+= _forwardUpperModalDistortionAngleXPositionPFAnalysis.numFailedModes.ToString();
                QA_XLS_COL15_VALUE+= _reverseLowerModalDistortionAnglePFAnalysis.numFailedModes.ToString();
                QA_XLS_COL16_VALUE+= _reverseLowerModalDistortionAngleXPositionPFAnalysis.numFailedModes.ToString();
                QA_XLS_COL17_VALUE+= _reverseUpperModalDistortionAnglePFAnalysis.numFailedModes.ToString();
                QA_XLS_COL18_VALUE+= _reverseUpperModalDistortionAngleXPositionPFAnalysis.numFailedModes.ToString();
                QA_XLS_COL19_VALUE+= _maxForwardLowerModeBDCAreaXLengthPFAnalysis.numFailedModes.ToString();
                QA_XLS_COL20_VALUE+= _maxForwardUpperModeBDCAreaXLengthPFAnalysis.numFailedModes.ToString();
                QA_XLS_COL21_VALUE+= _maxForwardLowerModeBDCAreaPFAnalysis.numFailedModes.ToString();
                QA_XLS_COL22_VALUE+= _maxForwardUpperModeBDCAreaPFAnalysis.numFailedModes.ToString();
                QA_XLS_COL23_VALUE+= _sumForwardLowerModeBDCAreasPFAnalysis.numFailedModes.ToString();
                QA_XLS_COL24_VALUE+= _sumForwardUpperModeBDCAreasPFAnalysis.numFailedModes.ToString();
                QA_XLS_COL25_VALUE+= _numForwardLowerModeBDCAreasPFAnalysis.numFailedModes.ToString();
                QA_XLS_COL26_VALUE+= _numForwardUpperModeBDCAreasPFAnalysis.numFailedModes;
                QA_XLS_COL27_VALUE+= _maxReverseLowerModeBDCAreaXLengthPFAnalysis.numFailedModes;
                QA_XLS_COL28_VALUE+= _maxReverseUpperModeBDCAreaXLengthPFAnalysis.numFailedModes;
                QA_XLS_COL29_VALUE+= _maxReverseLowerModeBDCAreaPFAnalysis.numFailedModes.ToString();
                QA_XLS_COL30_VALUE+= _maxReverseUpperModeBDCAreaPFAnalysis.numFailedModes.ToString();
                QA_XLS_COL31_VALUE+= _sumReverseLowerModeBDCAreasPFAnalysis.numFailedModes.ToString();
                QA_XLS_COL32_VALUE+= _sumReverseUpperModeBDCAreasPFAnalysis.numFailedModes.ToString();
                QA_XLS_COL33_VALUE+= _numReverseLowerModeBDCAreasPFAnalysis.numFailedModes.ToString();
                QA_XLS_COL34_VALUE+= _numReverseUpperModeBDCAreasPFAnalysis.numFailedModes.ToString();
                QA_XLS_COL35_VALUE+= _maxForwardLowerModeSlopePFAnalysis.numFailedModes.ToString();
                QA_XLS_COL36_VALUE+= _maxForwardUpperModeSlopePFAnalysis.numFailedModes.ToString();
                QA_XLS_COL37_VALUE+= _maxReverseLowerModeSlopePFAnalysis.numFailedModes.ToString();
                QA_XLS_COL38_VALUE+= _maxReverseUpperModeSlopePFAnalysis.numFailedModes.ToString();
                QA_XLS_COL39_VALUE+= _middleLineRMSValuesPFAnalysis.numFailedModes.ToString();
                QA_XLS_COL40_VALUE+= _minMiddleLineSlopePFAnalysis.numFailedModes.ToString();
                QA_XLS_COL41_VALUE+= _maxMiddleLineSlopePFAnalysis.numFailedModes.ToString();


                rowArray.Clear();
                rowArray.Add(QA_XLS_ROWTITLE4);
                rowArray.Add(QA_XLS_COL2_VALUE);
                rowArray.Add(QA_XLS_COL3_VALUE);
                rowArray.Add(QA_XLS_COL4_VALUE);
                rowArray.Add(QA_XLS_COL5_VALUE);
                rowArray.Add(QA_XLS_COL6_VALUE);
                rowArray.Add(QA_XLS_COL7_VALUE);
                rowArray.Add(QA_XLS_COL8_VALUE);
                rowArray.Add(QA_XLS_COL9_VALUE);
                rowArray.Add(QA_XLS_COL10_VALUE);
                rowArray.Add(QA_XLS_COL11_VALUE);
                rowArray.Add(QA_XLS_COL12_VALUE);
                rowArray.Add(QA_XLS_COL13_VALUE);
                rowArray.Add(QA_XLS_COL14_VALUE);
                rowArray.Add(QA_XLS_COL15_VALUE);
                rowArray.Add(QA_XLS_COL16_VALUE);
                rowArray.Add(QA_XLS_COL17_VALUE);
                rowArray.Add(QA_XLS_COL18_VALUE);
                rowArray.Add(QA_XLS_COL19_VALUE);
                rowArray.Add(QA_XLS_COL20_VALUE);
                rowArray.Add(QA_XLS_COL21_VALUE);
                rowArray.Add(QA_XLS_COL22_VALUE);
                rowArray.Add(QA_XLS_COL23_VALUE);
                rowArray.Add(QA_XLS_COL24_VALUE);
                rowArray.Add(QA_XLS_COL25_VALUE);
                rowArray.Add(QA_XLS_COL26_VALUE);
                rowArray.Add(QA_XLS_COL27_VALUE);
                rowArray.Add(QA_XLS_COL28_VALUE);
                rowArray.Add(QA_XLS_COL29_VALUE);
                rowArray.Add(QA_XLS_COL30_VALUE);
                rowArray.Add(QA_XLS_COL31_VALUE);
                rowArray.Add(QA_XLS_COL32_VALUE);
                rowArray.Add(QA_XLS_COL33_VALUE);
                rowArray.Add(QA_XLS_COL34_VALUE);
                rowArray.Add(QA_XLS_COL35_VALUE);
                rowArray.Add(QA_XLS_COL36_VALUE);
                rowArray.Add(QA_XLS_COL37_VALUE);
                rowArray.Add(QA_XLS_COL38_VALUE);
                rowArray.Add(QA_XLS_COL39_VALUE);
                rowArray.Add(QA_XLS_COL40_VALUE);
                rowArray.Add(QA_XLS_COL41_VALUE);
                passFailResults.addRow(rowArray);


                ////////////////////////////////////////
                ///
                //	thresholds
                //
                QA_XLS_COL2_VALUE = "";
                QA_XLS_COL3_VALUE = "";
                QA_XLS_COL4_VALUE = "";
                QA_XLS_COL5_VALUE = "";
                QA_XLS_COL6_VALUE = "";
                QA_XLS_COL7_VALUE = "";
                QA_XLS_COL8_VALUE = "";
                QA_XLS_COL9_VALUE = "";
                QA_XLS_COL10_VALUE = "";
                QA_XLS_COL11_VALUE = "";
                QA_XLS_COL12_VALUE = "";
                QA_XLS_COL13_VALUE = "";
                QA_XLS_COL14_VALUE = "";
                QA_XLS_COL15_VALUE = "";
                QA_XLS_COL16_VALUE = "";
                QA_XLS_COL17_VALUE = "";
                QA_XLS_COL18_VALUE = "";
                QA_XLS_COL19_VALUE = "";
                QA_XLS_COL20_VALUE = "";
                QA_XLS_COL21_VALUE = "";
                QA_XLS_COL22_VALUE = "";
                QA_XLS_COL23_VALUE = "";
                QA_XLS_COL24_VALUE = "";
                QA_XLS_COL25_VALUE = "";
                QA_XLS_COL26_VALUE = "";
                QA_XLS_COL27_VALUE = "";
                QA_XLS_COL28_VALUE = "";
                QA_XLS_COL29_VALUE = "";
                QA_XLS_COL30_VALUE = "";
                QA_XLS_COL31_VALUE = "";
                QA_XLS_COL32_VALUE = "";
                QA_XLS_COL33_VALUE = "";
                QA_XLS_COL34_VALUE = "";
                QA_XLS_COL35_VALUE = "";
                QA_XLS_COL36_VALUE = "";
                QA_XLS_COL37_VALUE = "";
                QA_XLS_COL38_VALUE = "";
                QA_XLS_COL39_VALUE = "";
                QA_XLS_COL40_VALUE = "";
                QA_XLS_COL41_VALUE = "";


                QA_XLS_COL2_VALUE+=string.Format(" > {0:g}", _maxUpperModeBordersRemovedPFAnalysis.threshold);
                QA_XLS_COL3_VALUE+=string.Format(" > {0:g}", _maxLowerModeBordersRemovedPFAnalysis.threshold);
                QA_XLS_COL4_VALUE+=string.Format(" > {0:g}", _maxMiddleLineRemovedPFAnalysis.threshold);
                QA_XLS_COL5_VALUE+=string.Format(" > {0:g}", _maxForwardModeWidthPFAnalysis.threshold);
                QA_XLS_COL6_VALUE+=string.Format(" > {0:g}", _maxReverseModeWidthPFAnalysis.threshold);
                QA_XLS_COL7_VALUE+=string.Format(" > {0:g}", _meanPercWorkingRegionPFAnalysis.threshold);
                QA_XLS_COL8_VALUE+=string.Format(" > {0:g}", _minPercWorkingRegionPFAnalysis.threshold);
                QA_XLS_COL9_VALUE+=string.Format(" < {0:g}", _minForwardModeWidthPFAnalysis.threshold);
                QA_XLS_COL10_VALUE+=string.Format(" < {0:g}", _minReverseModeWidthPFAnalysis.threshold);
                QA_XLS_COL11_VALUE+=string.Format(" > {0:g}", _forwardLowerModalDistortionAnglePFAnalysis.threshold);
                QA_XLS_COL12_VALUE+=string.Format(" > {0:g}", _forwardLowerModalDistortionAngleXPositionPFAnalysis.threshold);
                QA_XLS_COL13_VALUE+=string.Format(" > {0:g}", _forwardUpperModalDistortionAnglePFAnalysis.threshold);
                QA_XLS_COL14_VALUE+=string.Format(" > {0:g}", _forwardUpperModalDistortionAngleXPositionPFAnalysis.threshold);
                QA_XLS_COL15_VALUE+=string.Format(" > {0:g}", _reverseLowerModalDistortionAnglePFAnalysis.threshold);
                QA_XLS_COL16_VALUE+=string.Format(" > {0:g}", _reverseLowerModalDistortionAngleXPositionPFAnalysis.threshold);
                QA_XLS_COL17_VALUE+=string.Format(" > {0:g}", _reverseUpperModalDistortionAnglePFAnalysis.threshold);
                QA_XLS_COL18_VALUE+=string.Format(" > {0:g}", _reverseUpperModalDistortionAngleXPositionPFAnalysis.threshold);
                QA_XLS_COL19_VALUE+=string.Format(" > {0:g}", _maxForwardLowerModeBDCAreaXLengthPFAnalysis.threshold);
                QA_XLS_COL20_VALUE+=string.Format(" > {0:g}", _maxForwardUpperModeBDCAreaXLengthPFAnalysis.threshold);
                QA_XLS_COL21_VALUE+=string.Format(" > {0:g}", _maxForwardLowerModeBDCAreaPFAnalysis.threshold);
                QA_XLS_COL22_VALUE+=string.Format(" > {0:g}", _maxForwardUpperModeBDCAreaPFAnalysis.threshold);
                QA_XLS_COL23_VALUE+=string.Format(" > {0:g}", _sumForwardLowerModeBDCAreasPFAnalysis.threshold);
                QA_XLS_COL24_VALUE+=string.Format(" > {0:g}", _sumForwardUpperModeBDCAreasPFAnalysis.threshold);
                QA_XLS_COL25_VALUE+=string.Format(" > {0:g}", _numForwardLowerModeBDCAreasPFAnalysis.threshold);
                QA_XLS_COL26_VALUE+=string.Format(" > {0:g}", _numForwardUpperModeBDCAreasPFAnalysis.threshold);
                QA_XLS_COL27_VALUE+=string.Format(" > {0:g}", _maxReverseLowerModeBDCAreaXLengthPFAnalysis.threshold);
                QA_XLS_COL28_VALUE+=string.Format(" > {0:g}", _maxReverseUpperModeBDCAreaXLengthPFAnalysis.threshold);
                QA_XLS_COL29_VALUE+=string.Format(" > {0:g}", _maxReverseLowerModeBDCAreaPFAnalysis.threshold);
                QA_XLS_COL30_VALUE+=string.Format(" > {0:g}", _maxReverseUpperModeBDCAreaPFAnalysis.threshold);
                QA_XLS_COL31_VALUE+=string.Format(" > {0:g}", _sumReverseLowerModeBDCAreasPFAnalysis.threshold);
                QA_XLS_COL32_VALUE+=string.Format(" > {0:g}", _sumReverseUpperModeBDCAreasPFAnalysis.threshold);
                QA_XLS_COL33_VALUE+=string.Format(" > {0:g}", _numReverseLowerModeBDCAreasPFAnalysis.threshold);
                QA_XLS_COL34_VALUE+=string.Format(" > {0:g}", _numReverseUpperModeBDCAreasPFAnalysis.threshold);
                QA_XLS_COL35_VALUE+=string.Format(" > {0:g}", _maxForwardLowerModeSlopePFAnalysis.threshold);
                QA_XLS_COL36_VALUE+=string.Format(" > {0:g}", _maxForwardUpperModeSlopePFAnalysis.threshold);
                QA_XLS_COL37_VALUE+=string.Format(" > {0:g}", _maxReverseLowerModeSlopePFAnalysis.threshold);
                QA_XLS_COL38_VALUE+=string.Format(" > {0:g}", _maxReverseUpperModeSlopePFAnalysis.threshold);
                QA_XLS_COL39_VALUE+=string.Format(" > {0:g}", _middleLineRMSValuesPFAnalysis.threshold);
                QA_XLS_COL40_VALUE+=string.Format(" < {0:g}", _minMiddleLineSlopePFAnalysis.threshold);
                QA_XLS_COL41_VALUE+=string.Format(" > {0:g}", _maxMiddleLineSlopePFAnalysis.threshold);

                rowArray.Clear();
                rowArray.Add(QA_XLS_ROWTITLE5);
                rowArray.Add(QA_XLS_COL2_VALUE);
                rowArray.Add(QA_XLS_COL3_VALUE);
                rowArray.Add(QA_XLS_COL4_VALUE);
                rowArray.Add(QA_XLS_COL5_VALUE);
                rowArray.Add(QA_XLS_COL6_VALUE);
                rowArray.Add(QA_XLS_COL7_VALUE);
                rowArray.Add(QA_XLS_COL8_VALUE);
                rowArray.Add(QA_XLS_COL9_VALUE);
                rowArray.Add(QA_XLS_COL10_VALUE);
                rowArray.Add(QA_XLS_COL11_VALUE);
                rowArray.Add(QA_XLS_COL12_VALUE);
                rowArray.Add(QA_XLS_COL13_VALUE);
                rowArray.Add(QA_XLS_COL14_VALUE);
                rowArray.Add(QA_XLS_COL15_VALUE);
                rowArray.Add(QA_XLS_COL16_VALUE);
                rowArray.Add(QA_XLS_COL17_VALUE);
                rowArray.Add(QA_XLS_COL18_VALUE);
                rowArray.Add(QA_XLS_COL19_VALUE);
                rowArray.Add(QA_XLS_COL20_VALUE);
                rowArray.Add(QA_XLS_COL21_VALUE);
                rowArray.Add(QA_XLS_COL22_VALUE);
                rowArray.Add(QA_XLS_COL23_VALUE);
                rowArray.Add(QA_XLS_COL24_VALUE);
                rowArray.Add(QA_XLS_COL25_VALUE);
                rowArray.Add(QA_XLS_COL26_VALUE);
                rowArray.Add(QA_XLS_COL27_VALUE);
                rowArray.Add(QA_XLS_COL28_VALUE);
                rowArray.Add(QA_XLS_COL29_VALUE);
                rowArray.Add(QA_XLS_COL30_VALUE);
                rowArray.Add(QA_XLS_COL31_VALUE);
                rowArray.Add(QA_XLS_COL32_VALUE);
                rowArray.Add(QA_XLS_COL33_VALUE);
                rowArray.Add(QA_XLS_COL34_VALUE);
                rowArray.Add(QA_XLS_COL35_VALUE);
                rowArray.Add(QA_XLS_COL36_VALUE);
                rowArray.Add(QA_XLS_COL37_VALUE);
                rowArray.Add(QA_XLS_COL38_VALUE);
                rowArray.Add(QA_XLS_COL39_VALUE);
                rowArray.Add(QA_XLS_COL40_VALUE);
                rowArray.Add(QA_XLS_COL41_VALUE);
                passFailResults.addRow(rowArray);


                // Calculation parameters
                QA_XLS_COL1_VALUE = "";
                QA_XLS_COL2_VALUE = "";
                QA_XLS_COL3_VALUE = "";
                QA_XLS_COL4_VALUE = "";
                QA_XLS_COL5_VALUE = "";
                QA_XLS_COL6_VALUE = "";
                QA_XLS_COL7_VALUE = "";
                QA_XLS_COL8_VALUE = "";
                QA_XLS_COL9_VALUE = "";
                QA_XLS_COL10_VALUE = "";
                QA_XLS_COL11_VALUE = "";
                QA_XLS_COL12_VALUE = "";
                QA_XLS_COL13_VALUE = "";
                QA_XLS_COL14_VALUE = "";
                QA_XLS_COL15_VALUE = "";
                QA_XLS_COL16_VALUE = "";
                QA_XLS_COL17_VALUE = "";
                QA_XLS_COL18_VALUE = "";
                QA_XLS_COL19_VALUE = "";
                QA_XLS_COL20_VALUE = "";
                QA_XLS_COL21_VALUE = "";
                QA_XLS_COL22_VALUE = "";
                QA_XLS_COL23_VALUE = "";
                QA_XLS_COL24_VALUE = "";
                QA_XLS_COL25_VALUE = "";
                QA_XLS_COL26_VALUE = "";
                QA_XLS_COL27_VALUE = "";
                QA_XLS_COL28_VALUE = "";
                QA_XLS_COL29_VALUE = "";
                QA_XLS_COL30_VALUE = "";
                QA_XLS_COL31_VALUE = "";
                QA_XLS_COL32_VALUE = "";
                QA_XLS_COL33_VALUE = "";
                QA_XLS_COL34_VALUE = "";
                QA_XLS_COL35_VALUE = "";
                QA_XLS_COL36_VALUE = "";
                QA_XLS_COL37_VALUE = "";
                QA_XLS_COL38_VALUE = "";
                QA_XLS_COL39_VALUE = "";
                QA_XLS_COL40_VALUE = "";
                QA_XLS_COL41_VALUE = "";


                rowArray.Clear();
                rowArray.Add(QA_XLS_COL1_VALUE);
                rowArray.Add(QA_XLS_COL2_VALUE);
                rowArray.Add(QA_XLS_COL3_VALUE);
                rowArray.Add(QA_XLS_COL4_VALUE);
                rowArray.Add(QA_XLS_COL5_VALUE);
                rowArray.Add(QA_XLS_COL6_VALUE);
                rowArray.Add(QA_XLS_COL7_VALUE);
                rowArray.Add(QA_XLS_COL8_VALUE);
                rowArray.Add(QA_XLS_COL9_VALUE);
                rowArray.Add(QA_XLS_COL10_VALUE);
                rowArray.Add(QA_XLS_COL11_VALUE);
                rowArray.Add(QA_XLS_COL12_VALUE);
                rowArray.Add(QA_XLS_COL13_VALUE);
                rowArray.Add(QA_XLS_COL14_VALUE);
                rowArray.Add(QA_XLS_COL15_VALUE);
                rowArray.Add(QA_XLS_COL16_VALUE);
                rowArray.Add(QA_XLS_COL17_VALUE);
                rowArray.Add(QA_XLS_COL18_VALUE);
                rowArray.Add(QA_XLS_COL19_VALUE);
                rowArray.Add(QA_XLS_COL20_VALUE);
                rowArray.Add(QA_XLS_COL21_VALUE);
                rowArray.Add(QA_XLS_COL22_VALUE);
                rowArray.Add(QA_XLS_COL23_VALUE);
                rowArray.Add(QA_XLS_COL24_VALUE);
                rowArray.Add(QA_XLS_COL25_VALUE);
                rowArray.Add(QA_XLS_COL26_VALUE);
                rowArray.Add(QA_XLS_COL27_VALUE);
                rowArray.Add(QA_XLS_COL28_VALUE);
                rowArray.Add(QA_XLS_COL29_VALUE);
                rowArray.Add(QA_XLS_COL30_VALUE);
                rowArray.Add(QA_XLS_COL31_VALUE);
                rowArray.Add(QA_XLS_COL32_VALUE);
                rowArray.Add(QA_XLS_COL33_VALUE);
                rowArray.Add(QA_XLS_COL34_VALUE);
                rowArray.Add(QA_XLS_COL35_VALUE);
                rowArray.Add(QA_XLS_COL36_VALUE);
                rowArray.Add(QA_XLS_COL37_VALUE);
                rowArray.Add(QA_XLS_COL38_VALUE);
                rowArray.Add(QA_XLS_COL39_VALUE);
                rowArray.Add(QA_XLS_COL40_VALUE);
                rowArray.Add(QA_XLS_COL41_VALUE);
                passFailResults.addRow(rowArray);

                // add space
                passFailResults.addEmptyLine();


                //
                // Find location of failing statistic(s)
                //

                // row title
                rowArray.Clear();
                rowArray.Add("Index");
                passFailResults.addRow(rowArray);


                // final rows - Modes
                for (int i = 0; i < _numForwardModes; i++)
                {
                    QA_XLS_COL1_VALUE = "";
                    QA_XLS_COL2_VALUE = "";
                    QA_XLS_COL3_VALUE = "";
                    QA_XLS_COL4_VALUE = "";
                    QA_XLS_COL5_VALUE = "";
                    QA_XLS_COL6_VALUE = "";
                    QA_XLS_COL7_VALUE = "";
                    QA_XLS_COL8_VALUE = "";
                    QA_XLS_COL9_VALUE = "";
                    QA_XLS_COL10_VALUE = "";
                    QA_XLS_COL11_VALUE = "";
                    QA_XLS_COL12_VALUE = "";
                    QA_XLS_COL13_VALUE = "";
                    QA_XLS_COL14_VALUE = "";
                    QA_XLS_COL15_VALUE = "";
                    QA_XLS_COL16_VALUE = "";
                    QA_XLS_COL17_VALUE = "";
                    QA_XLS_COL18_VALUE = "";
                    QA_XLS_COL19_VALUE = "";
                    QA_XLS_COL20_VALUE = "";
                    QA_XLS_COL21_VALUE = "";
                    QA_XLS_COL22_VALUE = "";
                    QA_XLS_COL23_VALUE = "";
                    QA_XLS_COL24_VALUE = "";
                    QA_XLS_COL25_VALUE = "";
                    QA_XLS_COL26_VALUE = "";
                    QA_XLS_COL27_VALUE = "";
                    QA_XLS_COL28_VALUE = "";
                    QA_XLS_COL29_VALUE = "";
                    QA_XLS_COL30_VALUE = "";
                    QA_XLS_COL31_VALUE = "";
                    QA_XLS_COL32_VALUE = "";
                    QA_XLS_COL33_VALUE = "";
                    QA_XLS_COL34_VALUE = "";
                    QA_XLS_COL35_VALUE = "";
                    QA_XLS_COL36_VALUE = "";
                    QA_XLS_COL37_VALUE = "";
                    QA_XLS_COL38_VALUE = "";
                    QA_XLS_COL39_VALUE = "";
                    QA_XLS_COL40_VALUE = "";
                    QA_XLS_COL41_VALUE = "";

                    // Index location
                    //QA_XLS_COL1_VALUE.AppendFormat(" %d", i);
                    QA_XLS_COL1_VALUE+=string.Format(" {0:d}",i);

                    // Max Forward Mode Width
                    //std.vector<double>.iterator i_f = std.find(
                    //    _maxForwardModeWidthPFAnalysis.failedModes.begin(),
                    //    _maxForwardModeWidthPFAnalysis.failedModes.end(),
                    //    i);
                    int i_f=_maxForwardModeWidthPFAnalysis.failedModes.IndexOf(i);
                    if (i_f==-1)i_f=_maxForwardModeWidthPFAnalysis.failedModes.Count;

                    if (i < _maxForwardModeWidths.Count && i_f != _maxForwardModeWidthPFAnalysis.failedModes.Count)
                        QA_XLS_COL5_VALUE+=string.Format("{0:f}", _maxForwardModeWidths[i]);

                    // Max Reverse Mode Width
                    //i_f = std.find(
                    //    _maxReverseModeWidthPFAnalysis.failedModes.begin(),
                    //    _maxReverseModeWidthPFAnalysis.failedModes.end(),
                    //    i);
                    i_f=_maxReverseModeWidthPFAnalysis.failedModes.IndexOf(i);
                    if (i_f==-1)i_f=_maxReverseModeWidthPFAnalysis.failedModes.Count;

                    if (i < _maxReverseModeWidths.Count && i_f != _maxReverseModeWidthPFAnalysis.failedModes.Count)
                        QA_XLS_COL6_VALUE+=string.Format("{0:f}", _maxReverseModeWidths[i]);

                    // Mean Percentage Working Region
                    //i_f = std.find(
                    //    _meanPercWorkingRegionPFAnalysis.failedModes.begin(),
                    //    _meanPercWorkingRegionPFAnalysis.failedModes.end(),
                    //    i);
                    i_f=_meanPercWorkingRegionPFAnalysis.failedModes.IndexOf(i);
                    if (i_f==-1)i_f=_meanPercWorkingRegionPFAnalysis.failedModes.Count;

                    if (i < _meanPercWorkingRegions.Count && i_f != _meanPercWorkingRegionPFAnalysis.failedModes.Count)
                        QA_XLS_COL7_VALUE+=string.Format("{0:f}", _meanPercWorkingRegions[i]);

                    // Min Percentage Working Region
                    //i_f = std.find(
                    //    _minPercWorkingRegionPFAnalysis.failedModes.begin(),
                    //    _minPercWorkingRegionPFAnalysis.failedModes.end(),
                    //    i);
                    i_f=_minPercWorkingRegionPFAnalysis.failedModes.IndexOf(i);
                    if (i_f==-1)i_f=_minPercWorkingRegionPFAnalysis.failedModes.Count;

                    if (i < _minPercWorkingRegions.Count && i_f != _minPercWorkingRegionPFAnalysis.failedModes.Count)
                        QA_XLS_COL8_VALUE+=string.Format("{0:f}", _minPercWorkingRegions[i]);

                    // Min Forward Mode Width
                    //i_f = std.find(
                    //    _minForwardModeWidthPFAnalysis.failedModes.begin(),
                    //    _minForwardModeWidthPFAnalysis.failedModes.end(),
                    //    i);
                    i_f=_minForwardModeWidthPFAnalysis.failedModes.IndexOf(i);
                    if (i_f==-1)i_f=_minForwardModeWidthPFAnalysis.failedModes.Count;

                    if (i < _minForwardModeWidths.Count && i_f != _minForwardModeWidthPFAnalysis.failedModes.Count)
                        QA_XLS_COL9_VALUE+=string.Format("{0:f}", _minForwardModeWidths[i]);

                    // Min Reverse Mode Width
                    //i_f = std.find(
                    //    _minReverseModeWidthPFAnalysis.failedModes.begin(),
                    //    _minReverseModeWidthPFAnalysis.failedModes.end(),
                    //    i);
                    i_f=_minReverseModeWidthPFAnalysis.failedModes.IndexOf(i);
                    if (i_f==-1)i_f=_minReverseModeWidthPFAnalysis.failedModes.Count;

                    if (i < _minReverseModeWidths.Count && i_f != _minReverseModeWidthPFAnalysis.failedModes.Count)
                        QA_XLS_COL10_VALUE+=string.Format("{0:f}", _minReverseModeWidths[i]);


                    // forward lower modal_distortion
                    //i_f = std.find(
                    //    _forwardLowerModalDistortionAnglePFAnalysis.failedModes.begin(),
                    //    _forwardLowerModalDistortionAnglePFAnalysis.failedModes.end(),
                    //    i);
                    i_f=_forwardLowerModalDistortionAnglePFAnalysis.failedModes.IndexOf(i);
                    if (i_f==-1)i_f=_forwardLowerModalDistortionAnglePFAnalysis.failedModes.Count;

                    if (i < _forwardLowerModalDistortionAngles.Count
                    && i < _forwardLowerModalDistortionAngleXPositions.Count
                    && i_f != _forwardLowerModalDistortionAnglePFAnalysis.failedModes.Count)
                    {
                        QA_XLS_COL11_VALUE+=string.Format("{0:f}", _forwardLowerModalDistortionAngles[i]);
                        QA_XLS_COL12_VALUE+=string.Format("{0:f}", _forwardLowerModalDistortionAngleXPositions[i]);
                    }

                    // forward upper modal_distortion
                    //i_f = std.find(
                    //    _forwardUpperModalDistortionAnglePFAnalysis.failedModes.begin(),
                    //    _forwardUpperModalDistortionAnglePFAnalysis.failedModes.end(),
                    //    i);
                    i_f=_forwardUpperModalDistortionAnglePFAnalysis.failedModes.IndexOf(i);
                    if (i_f==-1)i_f=_forwardUpperModalDistortionAnglePFAnalysis.failedModes.Count;

                    if (i < _forwardUpperModalDistortionAngles.Count
                    && i < _forwardUpperModalDistortionAngleXPositions.Count
                    && i_f != _forwardUpperModalDistortionAnglePFAnalysis.failedModes.Count)
                    {
                        QA_XLS_COL13_VALUE+=string.Format("{0:f}", _forwardUpperModalDistortionAngles[i]);
                        QA_XLS_COL14_VALUE+=string.Format("{0:f}", _forwardUpperModalDistortionAngleXPositions[i]);
                    }

                    // reverse lower modal_distortion
                    //i_f = std.find(
                    //    _reverseLowerModalDistortionAnglePFAnalysis.failedModes.begin(),
                    //    _reverseLowerModalDistortionAnglePFAnalysis.failedModes.end(),
                    //    i);
                    i_f=_reverseLowerModalDistortionAnglePFAnalysis.failedModes.IndexOf(i);
                    if (i_f==-1)i_f=_reverseLowerModalDistortionAnglePFAnalysis.failedModes.Count;

                    if (i < _reverseLowerModalDistortionAngles.Count
                    && i < _reverseLowerModalDistortionAngleXPositions.Count
                    && i_f != _reverseLowerModalDistortionAnglePFAnalysis.failedModes.Count)
                    {
                        QA_XLS_COL15_VALUE+=string.Format("{0:f}", _reverseLowerModalDistortionAngles[i]);
                        QA_XLS_COL16_VALUE+=string.Format("{0:f}", _reverseLowerModalDistortionAngleXPositions[i]);
                    }

                    // reverse upper modal_distortion
                    //i_f = std.find(
                    //    _reverseUpperModalDistortionAnglePFAnalysis.failedModes.begin(),
                    //    _reverseUpperModalDistortionAnglePFAnalysis.failedModes.end(),
                    //    i);
                    i_f=_reverseUpperModalDistortionAnglePFAnalysis.failedModes.IndexOf(i);
                    if (i_f==-1)i_f=_reverseUpperModalDistortionAnglePFAnalysis.failedModes.Count;

                    if (i < _reverseUpperModalDistortionAngles.Count
                    && i < _reverseUpperModalDistortionAngleXPositions.Count
                    && i_f != _reverseUpperModalDistortionAnglePFAnalysis.failedModes.Count)
                    {
                        QA_XLS_COL17_VALUE+=string.Format("{0:f}", _reverseUpperModalDistortionAngles[i]);
                        QA_XLS_COL18_VALUE+=string.Format("{0:f}", _reverseUpperModalDistortionAngleXPositions[i]);
                    }

                    // Lower Max Forward Bdc X-Length
                    //i_f = std.find(
                    //    _maxForwardLowerModeBDCAreaXLengthPFAnalysis.failedModes.begin(),
                    //    _maxForwardLowerModeBDCAreaXLengthPFAnalysis.failedModes.end(),
                    //    i);
                    i_f= _maxForwardLowerModeBDCAreaXLengthPFAnalysis.failedModes.IndexOf(i);
                    if (i_f==-1)i_f= _maxForwardLowerModeBDCAreaXLengthPFAnalysis.failedModes.Count;

                    if (i < _maxForwardLowerModeBDCAreaXLengths.Count && i_f != _maxForwardLowerModeBDCAreaXLengthPFAnalysis.failedModes.Count)
                        QA_XLS_COL19_VALUE+=string.Format("{0:f}", _maxForwardLowerModeBDCAreaXLengths[i]);

                    // Upper Max Forward Bdc X-Length
                    //i_f = std.find(
                    //    _maxForwardUpperModeBDCAreaXLengthPFAnalysis.failedModes.begin(),
                    //    _maxForwardUpperModeBDCAreaXLengthPFAnalysis.failedModes.end(),
                    //    i);
                    i_f=_maxForwardUpperModeBDCAreaXLengthPFAnalysis.failedModes.IndexOf(i);
                    if (i_f==-1)i_f=_maxForwardUpperModeBDCAreaXLengthPFAnalysis.failedModes.Count;

                    if (i < _maxForwardUpperModeBDCAreaXLengths.Count && i_f != _maxForwardUpperModeBDCAreaXLengthPFAnalysis.failedModes.Count)
                        QA_XLS_COL20_VALUE+=string.Format("{0:f}", _maxForwardUpperModeBDCAreaXLengths[i]);

                    // Lower Max Forward Bdc Area
                    //i_f = std.find(
                    //    _maxForwardLowerModeBDCAreaPFAnalysis.failedModes.begin(),
                    //    _maxForwardLowerModeBDCAreaPFAnalysis.failedModes.end(),
                    //    i);
                    i_f=_maxForwardLowerModeBDCAreaPFAnalysis.failedModes.IndexOf(i);
                    if (i_f==-1)i_f=_maxForwardLowerModeBDCAreaPFAnalysis.failedModes.Count;

                    if (i < _maxForwardLowerModeBDCAreas.Count && i_f != _maxForwardLowerModeBDCAreaPFAnalysis.failedModes.Count)
                        QA_XLS_COL21_VALUE+=string.Format("{0:f}", _maxForwardLowerModeBDCAreas[i]);

                    // Upper Max Forward Bdc Area
                    //i_f = std.find(
                    //    _maxForwardUpperModeBDCAreaPFAnalysis.failedModes.begin(),
                    //    _maxForwardUpperModeBDCAreaPFAnalysis.failedModes.end(),
                    //    i);
                    i_f=_maxForwardUpperModeBDCAreaPFAnalysis.failedModes.IndexOf(i);
                    if (i_f==-1)i_f=_maxForwardUpperModeBDCAreaPFAnalysis.failedModes.Count;

                    if (i < _maxForwardUpperModeBDCAreas.Count && i_f != _maxForwardUpperModeBDCAreaPFAnalysis.failedModes.Count)
                        QA_XLS_COL22_VALUE+=string.Format("{0:f}", _maxForwardUpperModeBDCAreas[i]);

                    // Lower Sum of Forward Bdc Areas
                    //i_f = std.find(
                    //    _sumForwardLowerModeBDCAreasPFAnalysis.failedModes.begin(),
                    //    _sumForwardLowerModeBDCAreasPFAnalysis.failedModes.end(),
                    //    i);
                    i_f=_sumForwardLowerModeBDCAreasPFAnalysis.failedModes.IndexOf(i);
                    if (i_f==-1)i_f=_sumForwardLowerModeBDCAreasPFAnalysis.failedModes.Count;

                    if (i < _sumForwardLowerModeBDCAreas.Count && i_f != _sumForwardLowerModeBDCAreasPFAnalysis.failedModes.Count)
                        QA_XLS_COL23_VALUE+=string.Format("{0:f}", _sumForwardLowerModeBDCAreas[i]);

                    // Upper Sum of Forward Bdc Areas
                    //i_f = std.find(
                    //    _sumForwardUpperModeBDCAreasPFAnalysis.failedModes.begin(),
                    //    _sumForwardUpperModeBDCAreasPFAnalysis.failedModes.end(),
                    //    i);
                    i_f= _sumForwardUpperModeBDCAreasPFAnalysis.failedModes.IndexOf(i);
                    if (i_f==-1)i_f= _sumForwardUpperModeBDCAreasPFAnalysis.failedModes.Count;

                    if (i < _sumForwardUpperModeBDCAreas.Count && i_f != _sumForwardUpperModeBDCAreasPFAnalysis.failedModes.Count)
                        QA_XLS_COL24_VALUE+=string.Format("{0:f}", _sumForwardUpperModeBDCAreas[i]);

                    // Lower Number of Forward Bdcs
                    //i_f = std.find(
                    //    _numForwardLowerModeBDCAreasPFAnalysis.failedModes.begin(),
                    //    _numForwardLowerModeBDCAreasPFAnalysis.failedModes.end(),
                    //    i);
                    i_f=_numForwardLowerModeBDCAreasPFAnalysis.failedModes.IndexOf(i);
                    if (i_f==-1)i_f=_numForwardLowerModeBDCAreasPFAnalysis.failedModes.Count;

                    if (i < _numForwardLowerModeBDCAreaSamples.Count && i_f != _numForwardLowerModeBDCAreasPFAnalysis.failedModes.Count)
                        QA_XLS_COL25_VALUE+=string.Format("{0:f}", _numForwardLowerModeBDCAreaSamples[i]);

                    // Upper Number of Forward Bdcs
                    //i_f = std.find(
                    //    _numForwardUpperModeBDCAreasPFAnalysis.failedModes.begin(),
                    //    _numForwardUpperModeBDCAreasPFAnalysis.failedModes.end(),
                    //    i);
                    i_f=_numForwardUpperModeBDCAreasPFAnalysis.failedModes.IndexOf(i);
                    if (i_f==-1)i_f=_numForwardUpperModeBDCAreasPFAnalysis.failedModes.Count;

                    if (i < _numForwardUpperModeBDCAreaSamples.Count && i_f != _numForwardUpperModeBDCAreasPFAnalysis.failedModes.Count)
                        QA_XLS_COL26_VALUE+=string.Format("{0:f}", _numForwardUpperModeBDCAreaSamples[i]);

                    // Lower Max Reverse Bdc X-Length
                    //i_f = std.find(
                    //    _maxReverseLowerModeBDCAreaXLengthPFAnalysis.failedModes.begin(),
                    //    _maxReverseLowerModeBDCAreaXLengthPFAnalysis.failedModes.end(),
                    //    i);
                    i_f=_maxReverseLowerModeBDCAreaXLengthPFAnalysis.failedModes.IndexOf(i);
                    if (i_f==-1)i_f=_maxReverseLowerModeBDCAreaXLengthPFAnalysis.failedModes.Count;

                    if (i < _maxReverseLowerModeBDCAreaXLengths.Count && i_f != _maxReverseLowerModeBDCAreaXLengthPFAnalysis.failedModes.Count)
                        QA_XLS_COL27_VALUE+=string.Format("{0:f}", _maxReverseLowerModeBDCAreaXLengths[i]);

                    // Upper Max Reverse Bdc X-Length
                    //i_f = std.find(
                    //    _maxReverseUpperModeBDCAreaXLengthPFAnalysis.failedModes.begin(),
                    //    _maxReverseUpperModeBDCAreaXLengthPFAnalysis.failedModes.end(),
                    //    i);
                    i_f=_maxReverseUpperModeBDCAreaXLengthPFAnalysis.failedModes.IndexOf(i);
                    if (i_f==-1)i_f=_maxReverseUpperModeBDCAreaXLengthPFAnalysis.failedModes.Count;

                    if (i < _maxReverseUpperModeBDCAreaXLengths.Count && i_f != _maxReverseUpperModeBDCAreaXLengthPFAnalysis.failedModes.Count)
                        QA_XLS_COL28_VALUE+=string.Format("{0:f}", _maxReverseUpperModeBDCAreaXLengths[i]);

                    // Lower Max Reverse Bdc Area
                    //i_f = std.find(
                    //    _maxReverseLowerModeBDCAreaPFAnalysis.failedModes.begin(),
                    //    _maxReverseLowerModeBDCAreaPFAnalysis.failedModes.end(),
                    //    i);
                    i_f=_maxReverseLowerModeBDCAreaPFAnalysis.failedModes.IndexOf(i);
                    if (i_f==-1)i_f=_maxReverseLowerModeBDCAreaPFAnalysis.failedModes.Count;

                    if (i < _maxReverseLowerModeBDCAreas.Count && i_f != _maxReverseLowerModeBDCAreaPFAnalysis.failedModes.Count)
                        QA_XLS_COL29_VALUE+=string.Format("{0:f}", _maxReverseLowerModeBDCAreas[i]);

                    // Upper Max Reverse Bdc Area
                    //i_f = std.find(
                    //    _maxReverseUpperModeBDCAreaPFAnalysis.failedModes.begin(),
                    //    _maxReverseUpperModeBDCAreaPFAnalysis.failedModes.end(),
                    //    i);
                    i_f=_maxReverseUpperModeBDCAreaPFAnalysis.failedModes.IndexOf(i);
                    if (i_f==-1)i_f=_maxReverseUpperModeBDCAreaPFAnalysis.failedModes.Count;

                    if (i < _maxReverseUpperModeBDCAreas.Count && i_f != _maxReverseUpperModeBDCAreaPFAnalysis.failedModes.Count)
                        QA_XLS_COL30_VALUE+=string.Format("{0:f}", _maxReverseUpperModeBDCAreas[i]);

                    // Lower Sum of Reverse Bdc Areas
                    //i_f = std.find(
                    //    _sumReverseLowerModeBDCAreasPFAnalysis.failedModes.begin(),
                    //    _sumReverseLowerModeBDCAreasPFAnalysis.failedModes.end(),
                    //    i);
                    i_f=_sumReverseLowerModeBDCAreasPFAnalysis.failedModes.IndexOf(i);
                    if (i_f==-1)i_f=_sumReverseLowerModeBDCAreasPFAnalysis.failedModes.Count;

                    if (i < _sumReverseLowerModeBDCAreas.Count && i_f != _sumReverseLowerModeBDCAreasPFAnalysis.failedModes.Count)
                        QA_XLS_COL31_VALUE+=string.Format("{0:f}", _sumReverseLowerModeBDCAreas[i]);

                    // Upper Sum of Reverse Bdc Areas
                    //i_f = std.find(
                    //    _sumReverseUpperModeBDCAreasPFAnalysis.failedModes.begin(),
                    //    _sumReverseUpperModeBDCAreasPFAnalysis.failedModes.end(),
                    //    i);
                    i_f=_sumReverseUpperModeBDCAreasPFAnalysis.failedModes.IndexOf(i);
                    if (i_f==-1)i_f=_sumReverseUpperModeBDCAreasPFAnalysis.failedModes.Count;

                    if (i < _sumReverseUpperModeBDCAreas.Count && i_f != _sumReverseUpperModeBDCAreasPFAnalysis.failedModes.Count)
                        QA_XLS_COL32_VALUE+=string.Format("{0:f}", _sumReverseUpperModeBDCAreas[i]);

                    // Lower Number of Reverse Bdcs
                    //i_f = std.find(
                    //    _numReverseLowerModeBDCAreasPFAnalysis.failedModes.begin(),
                    //    _numReverseLowerModeBDCAreasPFAnalysis.failedModes.end(),
                    //    i);
                    i_f=_numReverseLowerModeBDCAreasPFAnalysis.failedModes.IndexOf(i);
                    if (i_f==-1)i_f=_numReverseLowerModeBDCAreasPFAnalysis.failedModes.Count;

                    if (i < _numReverseLowerModeBDCAreaSamples.Count && i_f != _numReverseLowerModeBDCAreasPFAnalysis.failedModes.Count)
                        QA_XLS_COL33_VALUE+=string.Format("{0:f}", _numReverseLowerModeBDCAreaSamples[i]);

                    // Upper Number of Reverse Bdcs
                    //i_f = std.find(
                    //    _numReverseUpperModeBDCAreasPFAnalysis.failedModes.begin(),
                    //    _numReverseUpperModeBDCAreasPFAnalysis.failedModes.end(),
                    //    i);
                    i_f=_numReverseUpperModeBDCAreasPFAnalysis.failedModes.IndexOf(i);
                    if (i_f==-1)i_f=_numReverseUpperModeBDCAreasPFAnalysis.failedModes.Count;

                    if (i < _numReverseUpperModeBDCAreaSamples.Count && i_f != _numReverseUpperModeBDCAreasPFAnalysis.failedModes.Count)
                        QA_XLS_COL34_VALUE+=string.Format("{0:f}", _numReverseUpperModeBDCAreaSamples[i]);

                    // Max Forward Lower Slope
                    //i_f = std.find(
                    //    _maxForwardLowerModeSlopePFAnalysis.failedModes.begin(),
                    //    _maxForwardLowerModeSlopePFAnalysis.failedModes.end(),
                    //    i);
                    i_f=_maxForwardLowerModeSlopePFAnalysis.failedModes.IndexOf(i);
                    if (i_f==-1)i_f=_maxForwardLowerModeSlopePFAnalysis.failedModes.Count;

                    if (i < _maxForwardLowerModeSlopes.Count && i_f != _maxForwardLowerModeSlopePFAnalysis.failedModes.Count)
                        QA_XLS_COL35_VALUE+=string.Format("{0:f}", _maxForwardLowerModeSlopes[i]);

                    // Max Forward Upper Slope
                    //i_f = std.find(
                    //    _maxForwardUpperModeSlopePFAnalysis.failedModes.begin(),
                    //    _maxForwardUpperModeSlopePFAnalysis.failedModes.end(),
                    //    i);
                    i_f=_maxForwardUpperModeSlopePFAnalysis.failedModes.IndexOf(i);
                    if (i_f==-1)i_f=_maxForwardUpperModeSlopePFAnalysis.failedModes.Count;

                    if (i < _maxForwardUpperModeSlopes.Count && i_f != _maxForwardUpperModeSlopePFAnalysis.failedModes.Count)
                        QA_XLS_COL36_VALUE+=string.Format("{0:f}", _maxForwardUpperModeSlopes[i]);

                    // Max Reverse Lower Slope
                    //i_f = std.find(
                    //    _maxReverseLowerModeSlopePFAnalysis.failedModes.begin(),
                    //    _maxReverseLowerModeSlopePFAnalysis.failedModes.end(),
                    //    i);
                    i_f=_maxReverseLowerModeSlopePFAnalysis.failedModes.IndexOf(i);
                    if (i_f==-1)i_f=_maxReverseLowerModeSlopePFAnalysis.failedModes.Count;

                    if (i < _maxReverseLowerModeSlopes.Count && i_f != _maxReverseLowerModeSlopePFAnalysis.failedModes.Count)
                        QA_XLS_COL37_VALUE+=string.Format("{0:f}", _maxReverseLowerModeSlopes[i]);

                    // Max Reverse Upper Slope
                    //i_f = std.find(
                    //    _maxReverseUpperModeSlopePFAnalysis.failedModes.begin(),
                    //    _maxReverseUpperModeSlopePFAnalysis.failedModes.end(),
                    //    i);
                    i_f= _maxReverseUpperModeSlopePFAnalysis.failedModes.IndexOf(i);
                    if (i_f==-1)i_f= _maxReverseUpperModeSlopePFAnalysis.failedModes.Count;

                    if (i < _maxReverseUpperModeSlopes.Count && i_f != _maxReverseUpperModeSlopePFAnalysis.failedModes.Count)
                        QA_XLS_COL38_VALUE+=string.Format("{0:f}", _maxReverseUpperModeSlopes[i]);

                    // Max Middle Line RMS Value
                    //i_f = std.find(
                    //    _middleLineRMSValuesPFAnalysis.failedModes.begin(),
                    //    _middleLineRMSValuesPFAnalysis.failedModes.end(),
                    //    i);
                    i_f=_middleLineRMSValuesPFAnalysis.failedModes.IndexOf(i);
                    if (i_f==-1)i_f=_middleLineRMSValuesPFAnalysis.failedModes.Count;

                    if (i < _middleLineRMSValues.Count && i_f != _middleLineRMSValuesPFAnalysis.failedModes.Count)
                        QA_XLS_COL39_VALUE+=string.Format("{0:f}", _middleLineRMSValues[i]);

                    // Min Middle Line Slope
                    //i_f = std.find(
                    //    _minMiddleLineSlopePFAnalysis.failedModes.begin(),
                    //    _minMiddleLineSlopePFAnalysis.failedModes.end(),
                    //    i);
                    i_f=_minMiddleLineSlopePFAnalysis.failedModes.IndexOf(i);
                    if (i_f==-1)i_f=_minMiddleLineSlopePFAnalysis.failedModes.Count;

                    if (i < _middleLineSlopes.Count && i_f != _minMiddleLineSlopePFAnalysis.failedModes.Count)
                        QA_XLS_COL40_VALUE+=string.Format("{0:f}", _middleLineSlopes[i]);

                    // Max Middle Line Slope
                    //i_f = std.find(
                    //    _maxMiddleLineSlopePFAnalysis.failedModes.begin(),
                    //    _maxMiddleLineSlopePFAnalysis.failedModes.end(),
                    //    i);
                    i_f=_maxMiddleLineSlopePFAnalysis.failedModes.IndexOf(i);
                    if (i_f==-1)i_f=_maxMiddleLineSlopePFAnalysis.failedModes.Count;

                    if (i < _middleLineSlopes.Count && i_f != _maxMiddleLineSlopePFAnalysis.failedModes.Count)
                        QA_XLS_COL41_VALUE+=string.Format("{0:f}", _middleLineSlopes[i]);

                    rowArray.Clear();
                    rowArray.Add(QA_XLS_COL1_VALUE);
                    rowArray.Add(QA_XLS_COL2_VALUE);
                    rowArray.Add(QA_XLS_COL3_VALUE);
                    rowArray.Add(QA_XLS_COL4_VALUE);
                    rowArray.Add(QA_XLS_COL5_VALUE);
                    rowArray.Add(QA_XLS_COL6_VALUE);
                    rowArray.Add(QA_XLS_COL7_VALUE);
                    rowArray.Add(QA_XLS_COL8_VALUE);
                    rowArray.Add(QA_XLS_COL9_VALUE);
                    rowArray.Add(QA_XLS_COL10_VALUE);
                    rowArray.Add(QA_XLS_COL11_VALUE);
                    rowArray.Add(QA_XLS_COL12_VALUE);
                    rowArray.Add(QA_XLS_COL13_VALUE);
                    rowArray.Add(QA_XLS_COL14_VALUE);
                    rowArray.Add(QA_XLS_COL15_VALUE);
                    rowArray.Add(QA_XLS_COL16_VALUE);
                    rowArray.Add(QA_XLS_COL17_VALUE);
                    rowArray.Add(QA_XLS_COL18_VALUE);
                    rowArray.Add(QA_XLS_COL19_VALUE);
                    rowArray.Add(QA_XLS_COL20_VALUE);
                    rowArray.Add(QA_XLS_COL21_VALUE);
                    rowArray.Add(QA_XLS_COL22_VALUE);
                    rowArray.Add(QA_XLS_COL23_VALUE);
                    rowArray.Add(QA_XLS_COL24_VALUE);
                    rowArray.Add(QA_XLS_COL25_VALUE);
                    rowArray.Add(QA_XLS_COL26_VALUE);
                    rowArray.Add(QA_XLS_COL27_VALUE);
                    rowArray.Add(QA_XLS_COL28_VALUE);
                    rowArray.Add(QA_XLS_COL29_VALUE);
                    rowArray.Add(QA_XLS_COL30_VALUE);
                    rowArray.Add(QA_XLS_COL31_VALUE);
                    rowArray.Add(QA_XLS_COL32_VALUE);
                    rowArray.Add(QA_XLS_COL33_VALUE);
                    rowArray.Add(QA_XLS_COL34_VALUE);
                    rowArray.Add(QA_XLS_COL35_VALUE);
                    rowArray.Add(QA_XLS_COL36_VALUE);
                    rowArray.Add(QA_XLS_COL37_VALUE);
                    rowArray.Add(QA_XLS_COL38_VALUE);
                    rowArray.Add(QA_XLS_COL39_VALUE);
                    rowArray.Add(QA_XLS_COL40_VALUE);
                    rowArray.Add(QA_XLS_COL41_VALUE);
                    passFailResults.addRow(rowArray);

                }
            }
            else // qaWasRun == false
            {
                List<string> error_msg_row = new List<string>();
                error_msg_row.Add("Error : Function called before Pass Fail Analysis completed.");
                passFailResults.addRow(error_msg_row);
            }

            return err;
        }

        PassFailThresholds getPassFailThresholds()
        {
            PassFailThresholds thresholds = new PassFailThresholds();

            thresholds._maxModeWidth = DSDBR01.Instance.get_DSDBR01_SMPF_max_mode_width(_superModeNumber);
            thresholds._minModeWidth = DSDBR01.Instance.get_DSDBR01_SMPF_min_mode_width(_superModeNumber);
            thresholds._maxModeBordersRemoved = DSDBR01.Instance.get_DSDBR01_SMPF_max_mode_borders_removed(_superModeNumber);
            thresholds._maxModeBDCArea = DSDBR01.Instance.get_DSDBR01_SMPF_max_mode_bdc_area(_superModeNumber);
            thresholds._maxModeBDCAreaXLength = DSDBR01.Instance.get_DSDBR01_SMPF_max_mode_bdc_area_x_length(_superModeNumber);
            thresholds._meanPercWorkingRegion = DSDBR01.Instance.get_DSDBR01_SMPF_mean_perc_working_region(_superModeNumber);
            thresholds._minPercWorkingRegion = DSDBR01.Instance.get_DSDBR01_SMPF_min_perc_working_region(_superModeNumber);
            thresholds._modalDistortionAngle = DSDBR01.Instance.get_DSDBR01_SMPF_modal_distortion_angle(_superModeNumber);
            thresholds._modalDistortionAngleXPosition = DSDBR01.Instance.get_DSDBR01_SMPF_modal_distortion_angle_x_pos(_superModeNumber);
            thresholds._numModeBDCAreas = DSDBR01.Instance.get_DSDBR01_SMPF_num_mode_bdc_areas(_superModeNumber);
            thresholds._sumModeBDCAreas = DSDBR01.Instance.get_DSDBR01_SMPF_sum_mode_bdc_areas(_superModeNumber);
            //thresholds._maxPrGap			= CG_REG->get_DSDBR01_SMPF_continuity_spacing(_superModeNumber);
            thresholds._maxMiddleLineSlope = DSDBR01.Instance.get_DSDBR01_SMPF_max_middle_line_slope(_superModeNumber);
            thresholds._minMiddleLineSlope = DSDBR01.Instance.get_DSDBR01_SMPF_min_middle_line_slope(_superModeNumber);
            thresholds._middleLineRMSValue = DSDBR01.Instance.get_DSDBR01_SMPF_max_middle_line_rms_value(_superModeNumber);
            thresholds._maxModeLineSlope = DSDBR01.Instance.get_DSDBR01_SMPF_max_mode_line_slope(_superModeNumber);

            return thresholds;
        }




       




    }
}

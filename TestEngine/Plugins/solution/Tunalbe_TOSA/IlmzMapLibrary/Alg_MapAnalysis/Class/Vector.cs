using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.Algorithms
{
    class Vector
    {
        public enum VectorAttribute
        {
            max = 0,
            min,
            mean,
            sum,
            count,
            variance,
            stdDev,
            median,
            maxGap // max gap between sorted elements
        }

        double maxOfValues() 
        {
            if (Count == 0) return 0;
            double maximum = double.MinValue;
            foreach (double var in _values)
            {
                if (var > maximum) maximum = var;
            }
            return maximum;
        }
        double minOfValues() 
        {
            if (Count == 0) return 0;
            double minimum = double.MaxValue;
            foreach (double var in _values)
            {
                if (var < minimum) minimum = var;
            }
            return minimum;
        }
        double meanOfValues() 
        {
            double mean = 0;
            if (Count>0)
            {
                mean = sumOfValues() / Count;
            }
            return mean;
        }
        double sumOfValues() 
        {
            double sum = 0;
            foreach (double var in _values)
            {
                sum += var;
            }
            return sum;
        }
        double varianceOfValues()
        {
            if (Count == 0) return 0;
            double variance = 0;
            double sum = 0;
            int count = 0;
            double mean = meanOfValues();
            foreach (double var in _values)
            {
                sum += Math.Pow(var - mean, 2);
            }
            if (Count > 1)
            {
                variance = sum / (Count - 1);
            }
            return variance;

        }
        double stdDevOfValues() 
        {
            double stdev = Math.Sqrt(varianceOfValues());
            return stdev;
        }
        double medianOfValues()
        {
            // The median of the sorted vector s[i] where i = 0, n-1 is defined as
            // s[(n-1)/2] where n is odd
            // ( s[(n/2)-1] + s[n/2] ) / 2 where n is even
            double median = 0;
            int n = Count;
            if (n > 0)
            {
                // make a copy of the vector
                List<double> sorted_values = new List<double>();
                sorted_values.AddRange(_values);
                sorted_values.Sort();

                if (n % 2 == 0)
                { // even
                    median = (sorted_values[(n / 2) - 1] + sorted_values[n / 2]) / 2;
                }
                else
                { // odd
                    median = sorted_values[(n - 1) / 2];
                }
            }
            return median;
        }
        double maxGapInValues() 
        {
            double maxGap = 0;
            int n = Count;
            if (n > 0)
            {
                // make a copy of the vector
                List<double> sorted_values = new List<double>();
                sorted_values.AddRange(_values);
                sorted_values.Sort();

                for (int i = 0; i < n - 1; i++)
                {
                    double gap = sorted_values[i + 1] - sorted_values[i];

                    if (gap > maxGap)
                        maxGap = gap;
                }
            }
            return maxGap;
        }

        protected List<double> _values;
        public Vector()
        {
            _values = new List<double>();
        }
        public Vector(List<double> values)
        {
            _values = values;
        }

        public List<double> Values
        {
            get
            {
                return _values;
            }
            set
            {
                _values.Clear();
                _values = value;
            }
        }

        public double GetVectorAttribute(VectorAttribute attrib)
        {
            double result = -1;
            switch (attrib)
            {	// Max,	Min, Mean, Sum, count, StandardDev, Variance
                case VectorAttribute.max:
                    result = maxOfValues();
                    break;
                case VectorAttribute.min:
                    result = minOfValues();
                    break;
                case VectorAttribute.mean:
                    result = meanOfValues();
                    break;
                case VectorAttribute.sum:
                    result = sumOfValues();
                    break;
                case VectorAttribute.count:
                    result = Count;
                    break;
                case VectorAttribute.stdDev:
                    result = stdDevOfValues();
                    break;
                case VectorAttribute.variance:
                    result = varianceOfValues();
                    break;
                case VectorAttribute.median:
                    result = medianOfValues();
                    break;
                case VectorAttribute.maxGap:
                    result = maxGapInValues();
                    break;
                default:
                    String error = string.Format("Vector.getQaAttrib{0:d}. Invalid parameter passed", attrib);
                    throw new ArgumentOutOfRangeException(error);
            }
            return result;
        }

        public void Sort() 
        {
            _values.Sort();
        }

        public int Count
        {
            get
            {
                return _values.Count;
            }
        }

        public double this[int index]
        {
            get
            {
                double valueAtIndex = 0;
                if (index>=0 && index<Count)
                {
                    valueAtIndex = _values[index];
                }
                return valueAtIndex;
            }
        }

        public void Add(double value)
        {
            _values.Add(value);

        }

        /// <summary>
        /// Instead of 
        /// void Vector::operator+(std::vector&lt;double&gt; vectorOfDoubles)
        /// {
        ///     std::vector&lt;double&gt;::iterator iter;
        ///     for(iter=vectorOfDoubles.begin(); iter!=vectorOfDoubles.end(); iter++)
        ///     {
        ///         _values.push_back(*iter);
        ///     }
        /// }
        /// </summary>
        /// <param name="vectorOfDoubles"></param>
        public void op_Plus(List<double> vectorOfDoubles)
        {
            _values.AddRange(vectorOfDoubles);
        }

        /// <summary>
        /// Instead of 
        /// void Vector::operator=(std::vector&lt;double&gt; vectorOfDoubles)
        /// {
        ///     _values.clear();
        ///     _values = vectorOfDoubles;
        /// }
        /// </summary>
        /// <param name="vectorOfDoubles"></param>
        public void op_Equal(List<double> vectorOfDoubles)
        {
            _values.Clear();
            _values.AddRange(vectorOfDoubles);
        }

        public void AddRange(IEnumerable<double> collection)
        {
            _values.AddRange(collection);
        }

        public void Clear()
        {
            _values.Clear();
        }

        
    }
}

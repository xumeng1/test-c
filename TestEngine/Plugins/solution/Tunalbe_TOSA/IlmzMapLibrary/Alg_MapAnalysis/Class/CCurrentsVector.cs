using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Bookham.TestLibrary.Algorithms
{
    public class CCurrentsVector
    {
        public enum rcode
        {
            ok = 0,
            vector_size_rows_cols_do_not_match,
            could_not_open_file,
            file_does_not_exist,
            file_already_exists,
            file_format_invalid
        }

        public List<double> _currents = new List<double>();
        public int _length;
        public String _abs_filepath;

        public rcode readFromFile(String abs_filepath)
        {
            rcode rval = rcode.ok;

            int row_count = 0;
            int first_line_col_count = 0;
            int col_count = 0;

            clear();
            int count_entries = 0;


            // Check file
            if (File.Exists(abs_filepath))
            {
                // File exists => attempt to read it's contents
                try
                {
                    string[] arr = File.ReadAllLines(abs_filepath);

                    row_count = 0;

                    foreach (string item in arr)
                    {
                        // File stream is open, read each line
                        if (item.Length > 0) // there is something on the line
                        {
                            // Search for two or more commas together,
                            // if found, insert a zero between them
                            // a comma at the start or end of a line also
                            // means there is a zero to be inserted

                            string[] colarr = item.Split(Defaults.DELIMITERS.ToCharArray());
                            col_count = colarr.Length;
                            foreach (string var in colarr)
                            {
                                if (string.IsNullOrEmpty(var))
                                    _currents.Add(0);
                                else
                                    _currents.Add(double.Parse(var));
                                count_entries++;
                            }

                            if (row_count == 0) first_line_col_count = col_count;

                            row_count++;
                        }

                        if (row_count > 0 && col_count != first_line_col_count)
                        {
                            rval = rcode.file_format_invalid;
                            break;
                        }
                    }
                }
                catch (Exception)
                {
                    rval = rcode.could_not_open_file;
                }
            }
            else
            {
                rval = rcode.file_does_not_exist;
            }

            if (rval == rcode.ok
             && (row_count == 0 || col_count != 1 || _currents.Count != row_count * col_count))
            {
                rval = rcode.file_format_invalid;
            }

            if (rval == rcode.ok)
            {
                _abs_filepath = abs_filepath;
                _length = row_count;
            }
            else
            {
                // something is wrong, clear everything!
                clear();
                throw new Exception(rval.ToString());
            }

            return rval;
        }

        public rcode writeToFile(String abs_filepath, bool overwrite)
        {
            rcode rval = rcode.ok;
            int row_count = 0;
            // Check if file already exists and overwrite not specified
            if (File.Exists(abs_filepath) && !overwrite)
            {
                rval = rcode.file_already_exists;
                return rval;
            }

            using (StreamWriter sw=new StreamWriter(abs_filepath))
            {
                foreach (double var in _currents)
                {
                    sw.WriteLine(var);
                }
            }
            _abs_filepath = abs_filepath;
            return rval;
        }

        public rcode writeToFile(String abs_filepath)
        {
            return writeToFile(abs_filepath, false);
        }

        public void clear()
        {
            _currents.Clear();
            _length = 0;
            _abs_filepath = string.Empty;
        }
        public bool isEmpty()
        {
            return _currents.Count == 0;
        }

        

    }
}

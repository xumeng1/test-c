using System;
using System.Collections.Generic;
using System.Text;
using System.IO;


namespace Bookham.TestLibrary.Algorithms
{
    /// <summary>
    ///  Helper class for extracting data from a CloseGrid CSV file
    /// </summary>
    internal static class CsvDataLookup
    {
        /// <summary>
        /// Get the value of a single element in a CSV file
        /// </summary>
        /// <param name="filename">File to open</param>
        /// <param name="line">Line number (start from 1)</param>
        /// <param name="column">Column number (start from 1)</param>
        /// <returns>Element string - throws exception on bad line/column number or missing file</returns>
        internal static string GetElement(string filename, int line, int column)
        {
            string filenameNoPath = Path.GetFileName(filename);
            string elementStr; 
            // using statement will auto-shutdown the file even in presence of exceptions
            using (CsvReader csvReader = new CsvReader())
            {
                // open the file
                csvReader.OpenFile(filename);
                string[] lineElems = null;
                int lineCounter = 1;
                // find the right line
                while ((lineElems = csvReader.GetLine()) != null) 
                {
                    if (lineCounter == line) break;
                    lineCounter++;
                }
                // if got to the end of the file, we'll have a null line reference
                if (lineElems == null)
                {
                    string errStr = string.Format("Couldn't find line {0} in CloseGrid CSV file {1}",
                        line, filenameNoPath);
                    throw new ArgumentException(errStr);
                }
                // OK - we've got our line
                // sanity check the column value
                if ((column < 0) || (column > lineElems.Length - 1))
                {
                    string errStr = string.Format("Couldn't find column {0}, line {1} in CloseGrid CSV file {2}",
                        column, line, filenameNoPath);
                    throw new ArgumentException(errStr);
                }
                // get the element
                elementStr = lineElems[column - 1];
            }
            // return the value
            return elementStr;
        }

        /// <summary>
        /// Get column from a CSV file
        /// </summary>
        /// <param name="filename">filename</param>
        /// <param name="column">Column number (start at 1)</param>
        /// <returns>List of strings</returns>
        internal static List<string> GetColumn(string filename, int column)
        {
            if (column < 1) throw new ArgumentException("Invalid column number (starts at 1): " + column);

            string filenameNoPath = Path.GetFileName(filename);
            List<string[]> lines;
            // using statement will auto-shutdown the file even in presence of exceptions
            using (CsvReader csvReader = new CsvReader())
            {
                lines = csvReader.ReadFile(filename);
            }

            int colIndexMinus1 = column-1;                
            List<string> columnStrs = new List<string>();
            foreach (string[] line in lines)
            {
                // if this line doesn't have a value at this column level - add a null string
                string elemStr = null;                    
                if (line.Length >= column) elemStr = line[colIndexMinus1];
                columnStrs.Add(elemStr);
            }
            return columnStrs;
        }

        /// <summary>
        /// Calculate Modal Distortion figure
        /// </summary>
        /// <param name="smQaAnalysisFile">Path to a supermode QA analysis</param>
        /// <param name="nbrLongitudinalModes">Number of longitudinal modes</param>
        /// <param name="ignoreFirstThreeLMs">True if ignore the first 3 LMs when calculate MD</param>
        /// <returns>Modal Distortion figure</returns>
        internal static double ModalDistortion(string smQaAnalysisFile, int nbrLongitudinalModes, bool ignoreFirstThreeLMs)
        {
            int startLM = 0;
            if (ignoreFirstThreeLMs)
            {
                // ignore the first 3 LMs so we start from LM3 ( As the first LM is LM0)
                startLM = 3;
            }

            double avg;
            if (ignoreFirstThreeLMs && nbrLongitudinalModes <= 3)
            {
                avg = 999;
            }
            else
            {
                // get 8th column of the file 
                List<string> columns = GetColumn(smQaAnalysisFile, 8);
                double total = 0.0;
                for (int ii = startLM; ii < nbrLongitudinalModes; ii++)
                {
                    // Get max and min values of "Width for Stable Area" for each longitudinal mode
                    string maxStr = columns[ii * 9 + 9];  //lines 10, 19, 28 etc
                    string minStr = columns[ii * 9 + 10]; //lines 11, 20, 28 etc

                    double max = Double.Parse(maxStr);
                    double min = Double.Parse(minStr);
                    total += max - min;
                }
                avg = total / (nbrLongitudinalModes - startLM);
            }

            return avg;
        }

        
    }
}

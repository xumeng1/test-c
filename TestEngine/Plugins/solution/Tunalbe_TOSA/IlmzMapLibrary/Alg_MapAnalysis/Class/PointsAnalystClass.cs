using System;
using System.IO;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Text;
//using System.Drawing;

using MatrixHelper;
using Bookham.TestLibrary.Algorithms;
using Bookham.fcumapping.utility;

namespace Bookham.TestLibrary.Algorithms
{
    public class PointsAnalystClass
    {
        private int widthRear = 201;
        private int height = 500;
        private int height_offset = 50;
        private int widthFront = 147;
        private int frontPairCount = 21;
        public int WidthRear
        {
            get { return widthRear; }
            set { widthRear = value; }
        }
        public int Height
        {
            set { height = value; }
        }
        public int WidthFront
        {
            get { return widthFront; }
            set { widthFront = value; }
        }
        private int groupIndex;
        private Hashtable htGroup;
        private List<List<MyDataTable>> groupsDT;
        public List<List<MyDataTable>> Groups
        {
            get { return groupsDT; }
        }
        private int treeLoopNumber = 100;

        private double PRMax = 1;
        private List<CenterPointSlope> centerPointSlopeList;
        private Hashtable htWaitForCombinGroup;

        //private List<double> vector_Ir;
        //private List<List<double[]>> vector_If;

        public CCurrentsVector vector_Ir;
        public CDSDBRFrontCurrents vector_If;
        public List<List<LineZX>> lines;
        private DataTable dtMapData;
        
        private int maxHeightInWordedMap = -1;
        private int minHeightInWordedMap = 999;

        private int minDistinctToSide = 2;
        private int distinctToTurnLine = 2;

        private bool useLeftBorder = true;
        private bool useRightBorder = false;
        //test
        //private Color[] mycolor = new Color[] { Color.Tomato, Color.Brown, Color.OliveDrab, Color.Purple
        //                                                            ,  Color.Sienna, Color.Wheat, Color.SkyBlue, Color.YellowGreen 
        //                                                            , Color.MediumVioletRed, Color.SandyBrown, Color.Tomato, Color.SeaShell, Color.Pink };

        //public Bitmap bitmap2;

        public void DoTestAuto(List<CLaserModeMap> CLaserModeMaps, int rangex, int rangey, int cmpValue, double rangeMaxL, double rangeBranch, int minCount
            , int treeLoopNum, int combinParam, int combinParamZ, int dArea
            , int yLimit, int distanceLimit, int lineWidth, int minDistToSide, int distToTurnLine
            , bool useLeft, bool useRight)
        {
            minDistinctToSide = minDistToSide;
            distinctToTurnLine = distToTurnLine;
            treeLoopNumber = treeLoopNum;
            useLeftBorder = useLeft;
            useRightBorder = useRight;
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"));
            int[,] intmap = new int[widthRear, height + height_offset];
            int[,] intmapShadow;
            int[,] intmapWorked = new int[widthRear, height + height_offset];



            //1.get data & build intmap
            dtMapData = GetData(CLaserModeMaps, intmap);
            sb.AppendLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"));

            #region base level analyst
            //2.build shadow intmap, bulid worked map
            int lastFindCount = 0;
            int loopMaxCount = 10;
            int loopCount = 0;
            intmapShadow = ComputeIntmapShadow(intmap, rangex, rangey);
            intmapWorked = BuildIntmapWorked(intmap, intmapShadow, cmpValue, ref lastFindCount);
            //do
            //{
            //    int findCount = 0;
            //    intmapShadow = ComputeIntmapShadow(intmapWorked, rangex, rangey);
            //    intmapWorked = BuildIntmapWorked(intmapWorked, intmapShadow, cmpValue, ref findCount);
            //    loopCount++;
            //    if (findCount == lastFindCount)
            //    {
            //        break;
            //    }
            //    else
            //    {
            //        lastFindCount = findCount;
            //    }
            //} while (loopMaxCount > loopCount);

            sb.AppendLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"));
            //3.build node tree
            Node root = new Node(-1, -1);
            //2011.10.13 deal with the rangeMaxL
            {
                rangeMaxL = (maxHeightInWordedMap - minHeightInWordedMap) * rangeMaxL / 100d;
                rangeMaxL = rangeMaxL * 0.3;
                rangeBranch = rangeMaxL;
            }
            BuildTree(intmapWorked, rangeMaxL, rangeBranch, root);
            sb.AppendLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"));
            //4.build group hashtable
            htGroup = new Hashtable();
            groupIndex = 0;
            {//test
                //if (bitmap2 == null)
                //{
                //    bitmap2 = new Bitmap(widthRear, height, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
                //    //bitmap.Dispose();
                //}
                //for (int i = 0; i < bitmap2.Width; i++)
                //{
                //    for (int j = 0; j < bitmap2.Height; j++)
                //    {
                //        bitmap2.SetPixel(i, j, Color.White);
                //    }
                //}
            }
            foreach (Node groupNode in root.Items)
            {
                BuildGroupHT(groupNode, minCount);
            }
            sb.AppendLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"));
            #endregion

            //5.find center point and slope for each group, combin nearly ones
            CombinGroupPre(yLimit, distanceLimit, lineWidth, root, minCount, intmap);//====================
            sb.AppendLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"));
            //6.find and leave max count sub group
            MakeSubGroups(dtMapData, minCount, combinParam, combinParamZ, dArea);

            sb.AppendLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"));
            //
            string timelog = sb.ToString();
        }


        private void CombinGroupPre(int yLimit, int distanceLimit, int lineWidth, Node root, int minCount, int[,] intmap)
        {
            groupIndex = 0;
            centerPointSlopeList = new List<CenterPointSlope>();
            htWaitForCombinGroup = new Hashtable();
            //1 find group center and slope
            foreach (Node groupNode in root.Items)
            {
                FindGroupCenterAndSlope(groupNode, minCount);
            }
            //2 check which pair need combin
            if (centerPointSlopeList.Count > 0)
            {
                for (int i = centerPointSlopeList.Count - 1; i > 0; i--)
                {
                    for (int j = i - 1; j >= 0; j--)
                    {
                        CenterPointSlope cps1 = centerPointSlopeList[i];
                        CenterPointSlope cps2 = centerPointSlopeList[j];
                        if (Math.Abs(cps1.Y - cps2.Y) < yLimit && cps1.XArry.Length >= minCount)//
                        {
                            if (cps1.XArry.Length < cps2.XArry.Length)//cps1.X > cps2.X
                            {
                                CenterPointSlope temp = cps1;
                                cps1 = cps2;
                                cps2 = temp;
                            }
                            EquationOfLine eol = CoordinatesHelper.GetEquationOfLine(cps1.X, cps1.Y, cps1.Slope);
                            double distance = CoordinatesHelper.GetDistanceBetweenPointToLine(cps2.X, cps2.Y, eol);
                            if (distance <= distanceLimit)
                            {
                                //Add to a list
                                int maxGroupIndex = Math.Max(cps1.GroupIndex, cps2.GroupIndex);
                                int minGroupIndex = Math.Min(cps1.GroupIndex, cps2.GroupIndex);

                                //if (htWaitForCombinGroup.ContainsKey(minGroupIndex))
                                //{
                                //    minGroupIndex = (int)htWaitForCombinGroup[minGroupIndex];
                                //}
                                if (!htWaitForCombinGroup.ContainsKey(maxGroupIndex))
                                {
                                    htWaitForCombinGroup.Add(maxGroupIndex, minGroupIndex);
                                }
                                //fix pre items example: 9->8, 8->3 then 9->8 change to 9->3
                                for (int k = j; k < centerPointSlopeList.Count; k++)
                                {
                                    if (htWaitForCombinGroup.ContainsKey(k) && (int)htWaitForCombinGroup[k] == maxGroupIndex)
                                    {
                                        htWaitForCombinGroup[k] = minGroupIndex;
                                    }
                                }


                                //
                                CenterPointSlope cps1Temp = GetItemByGroupIndex(centerPointSlopeList, minGroupIndex);
                                CenterPointSlope cps2Temp = GetItemByGroupIndex(centerPointSlopeList, maxGroupIndex);
                                double[] newXArry = new double[cps1Temp.XArry.Length + cps2Temp.XArry.Length];
                                cps1Temp.XArry.CopyTo(newXArry, 0);
                                cps2Temp.XArry.CopyTo(newXArry, cps1Temp.XArry.Length);
                                double[] newYArry = new double[cps1Temp.YArry.Length + cps2Temp.YArry.Length];
                                cps1Temp.YArry.CopyTo(newYArry, 0);
                                cps2Temp.YArry.CopyTo(newYArry, cps1Temp.YArry.Length);

                                cps1Temp.XArry = newXArry;
                                cps1Temp.YArry = newYArry;
                                double xSum = 0;
                                double ySum = 0;

                                for (int ii = 0; ii < newXArry.Length && ii < newYArry.Length; ii++)
                                {
                                    xSum += newXArry[ii];
                                    ySum += newYArry[ii];
                                }
                                LinearLeastSquaresFit llsf = LinearLeastSquaresFitAlgorithm.Calculate(newXArry, newYArry, 0, newYArry.Length - 1);
                                cps1Temp.Slope = llsf.Slope;
                                cps1Temp.X = xSum / (double)newXArry.Length;
                                cps1Temp.Y = ySum / (double)newYArry.Length;

                                break;//combin one time
                            }
                        }
                    }
                }
                //3 combin group
                if (htWaitForCombinGroup.Count > 0)
                {
                    Hashtable htTemp = new Hashtable();
                    foreach (DictionaryEntry de in htGroup)
                    {
                        object value = de.Value;
                        if (htWaitForCombinGroup.ContainsKey(de.Value))
                        {
                            value = htWaitForCombinGroup[de.Value];
                        }
                        htTemp.Add(de.Key, value);
                    }
                    htGroup = htTemp;
                }
            }
            //remove useless group
            for (int i = centerPointSlopeList.Count - 1; i >= 0; i--)
            {
                if (htWaitForCombinGroup.ContainsKey(centerPointSlopeList[i].GroupIndex))
                {
                    centerPointSlopeList.RemoveAt(i);
                }
            }
            for (int i = centerPointSlopeList.Count - 1; i > 0; i--)
            {
                //如果向前找到有交点的直线，删除数量小的
                for (int j = i - 1; j >= 0; j--)
                {
                    EquationOfLine eol1 = CoordinatesHelper.GetEquationOfLine(centerPointSlopeList[i].X, centerPointSlopeList[i].Y, centerPointSlopeList[i].Slope);
                    EquationOfLine eol2 = CoordinatesHelper.GetEquationOfLine(centerPointSlopeList[j].X, centerPointSlopeList[j].Y, centerPointSlopeList[j].Slope);
                    CPoint intersectionPoint = CoordinatesHelper.GetIntersectionPointOfTwoLine(eol1, eol2);
                    if (intersectionPoint.X >= 0 && intersectionPoint.Y >= 0
                            && intersectionPoint.X < widthRear && intersectionPoint.Y < height)
                    {
                        if (centerPointSlopeList[i].XArry.Length > centerPointSlopeList[j].XArry.Length)
                        {
                            DeleteGroupFromhtGroup(centerPointSlopeList[j].GroupIndex);
                            centerPointSlopeList.RemoveAt(j);
                        }
                        else
                        {
                            DeleteGroupFromhtGroup(centerPointSlopeList[i].GroupIndex);
                            centerPointSlopeList.RemoveAt(i);
                        }
                        break;
                    }
                }
            }
            //4 check
            //Graphics g = Graphics.FromImage(bitmap2);
            //InitImage22();
            Hashtable htTemp2 = new Hashtable();
            for (int i = 0; i < centerPointSlopeList.Count; i++)
            {
                if (centerPointSlopeList[i].XArry.Length >= minCount)//!htWaitForCombinGroup.ContainsKey(centerPointSlopeList[i].GroupIndex) &&
                {
                    LinearLeastSquaresFit llsf = LinearLeastSquaresFitAlgorithm.Calculate(centerPointSlopeList[i].XArry
                                                            , centerPointSlopeList[i].YArry, 0, centerPointSlopeList[i].YArry.Length - 1);
                    EquationOfLine eol = CoordinatesHelper.GetEquationOfLineYIntercept(llsf.YIntercept, llsf.Slope);
                    //g.DrawLine(mypen, 0, (float)(CoordinatesHelper.GetY(0, eol))
                    //                                    , bitmap2.Width - 1, (float)(CoordinatesHelper.GetY(bitmap2.Width - 1, eol)));

                    //5 add other points to this line range
                    for (int ii = 0; ii < widthRear; ii++)
                    {
                        int y = Convert.ToInt32(Math.Round(CoordinatesHelper.GetY(ii, eol), 0));
                        for (int j = -1 * lineWidth * 2; j < lineWidth * 2; j++)
                        {
                            if ((y + j) >= 0 && (y + j) < height)
                            {
                                if (intmap[ii, y + j] > 0)//bitmap.GetPixel(ii, y + j).ToArgb() == Color.Black.ToArgb())
                                {
                                    if (CoordinatesHelper.GetDistanceBetweenPointToLine(ii, y + j, eol) <= lineWidth)
                                    {
                                        string Key = string.Format("{0},{1}", ii, y + j);
                                        //if (!htTemp2.ContainsKey(Key))
                                        //{
                                        //    htTemp2.Add(Key, centerPointSlopeList[i].GroupIndex);
                                        //}
                                        //else
                                        //{ 
                                        //}
                                        if (!htGroup.ContainsKey(Key))
                                        {
                                            htGroup.Add(Key, centerPointSlopeList[i].GroupIndex);
                                            //bitmap2.SetPixel(ii, y + j, Color.Red);
                                        }
                                        //else
                                        //{
                                        //    if ((int)htGroup[Key] != centerPointSlopeList[i].GroupIndex)
                                        //    {
                                        //        htGroup[Key] = centerPointSlopeList[i].GroupIndex;
                                        //        bitmap2.SetPixel(ii, y + j, Color.Red);
                                        //    }
                                        //}
                                        //bitmap22.SetPixel(ii, y + j, mycolor[i % mycolor.Length]);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            //htGroup = htTemp2;


        }

        private CenterPointSlope GetItemByGroupIndex(List<CenterPointSlope> list, int groupIndex)
        {
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].GroupIndex == groupIndex)
                {
                    return list[i];
                }
            }
            return new CenterPointSlope();
        }

        private void FindGroupCenterAndSlope(Node node, int minCount)
        {
            //Color color = mycolor[groupIndex % mycolor.Length];
            if (Node.GetTotalCount(node) >= minCount)//node.Count
            {
                //colorIndex++;
                if (node.Count >= minCount)
                {
                    double[] xArry = new double[node.Items.Count];
                    double[] yArry = new double[node.Items.Count];
                    double xSum = 0;
                    double ySum = 0;

                    for (int ii = 0; ii < node.Items.Count; ii++)
                    {
                        //htGroup.Add(string.Format("{0},{1}", nod.X, nod.Y), groupIndex);
                        xArry[ii] = node.Items[ii].X;
                        yArry[ii] = node.Items[ii].Y;
                        xSum += node.Items[ii].X;
                        ySum += node.Items[ii].Y;
                    }
                    if (ySum != yArry[0] * node.Items.Count)
                    {
                        LinearLeastSquaresFit llsf = LinearLeastSquaresFitAlgorithm.Calculate(xArry, yArry, 0, yArry.Length - 1);

                        centerPointSlopeList.Add(new CenterPointSlope(xSum / (double)node.Items.Count
                                                                                                    , ySum / (double)node.Items.Count
                                                                                                    , llsf.Slope, groupIndex, xArry, yArry));
                    }
                    else
                        minCount = 999;
                    groupIndex++;
                }
                foreach (Node nod in node.Items)
                {
                    if (nod.Count > 0)
                    {
                        FindGroupCenterAndSlope(nod, minCount);
                    }
                }
            }
        }

        private void DeleteGroupFromhtGroup(int groupIndex)
        {
            Hashtable htTemp = new Hashtable();
            foreach (DictionaryEntry de in htGroup)
            {
                if (groupIndex != (int)de.Value)
                {
                    htTemp.Add(de.Key, de.Value);
                }
            }
            htGroup = htTemp;
        }

        private DataTable GetData(List<CLaserModeMap> CLaserModeMaps, int[,] intmap)
        {
            DataTable dt = new DataTable("points");
            dt.Columns.Add("x", typeof(double));//rear  0~200
            dt.Columns.Add("y", typeof(double));//power ratio 0~
            dt.Columns.Add("z", typeof(double));//fp  1~147 0~146
            //dt.Columns.Add("isuseful", typeof(bool));

            //StreamReader sr = new StreamReader(filePath);
            //importFileName = filePath;

            int fpIndex = 0;

            for (int i = 0; i < CLaserModeMaps.Count; i++)
            {
                for (int j = 0; j < CLaserModeMaps[i]._map.Count; j++)
                {
                    dt.Rows.Add(new object[] { (double)(j%CLaserModeMaps[i]._cols)
                                                                    , (CLaserModeMaps[i]._map[j]-OverallMapPoweRatioRange.MinPowerRatio)/(OverallMapPoweRatioRange.MaxPowerRatio-OverallMapPoweRatioRange.MinPowerRatio)//CLaserModeMaps[i]._map[j]
                                                                    , (double)(i*CLaserModeMaps[i]._rows+j/CLaserModeMaps[i]._cols)
                                                                    });
                }
            }

            //while (true)
            //{
            //    string str = sr.ReadLine();
            //    if (str != null && str.Trim().Length > 0)
            //    {
            //        fpIndex++;
            //        string[] strs = str.Split(',');
            //        for (int i = 0; i < strs.Length; i++) // irear:0~200   fp: 1~147
            //        {
            //            dt.Rows.Add(new object[] { (double)i
            //                                                        , double.Parse(strs[i])
            //                                                        , (double)fpIndex
            //                                                        , false});
            //        }
            //    }
            //    else
            //    {
            //        break;
            //    }
            //}
            PRMax = (double)dt.Compute("max(y)", "");
            foreach (DataRow dr in dt.Rows)
            {
                intmap[Convert.ToInt32(dr["x"]), (int)Math.Floor(height - (double)dr["y"] / PRMax * height)] = 1;
            }

            return dt;
        }

        private int[,] ComputeIntmapShadow(int[,] intmap, int rangex, int rangey)
        {
            int[,] intmapShadow = new int[widthRear, height];
            //for (int i = 0; i < widthRear; i++)
            //{
            //    for (int j = 0; j < height; j++)
            //    {
            //        if (intmap[i, j] > 0)
            //        {
            //            SetRangeValueAdd(rangex, rangey, intmapShadow, i, j);
            //        }
            //    }
            //}
            for (int i = 0; i < dtMapData.Rows.Count; i++)
            {
                //for (int j = 0; j < bitmap.Height; j++)
                //{
                DataRow dr = dtMapData.Rows[i];
                int x = Convert.ToInt32(dr["x"]);
                int y = (int)Math.Floor(height - (double)dr["y"] / PRMax * height);
                //Color oColor = bitTemp.GetPixel(x, y);
                if (intmap[x, y] > 0)
                {
                    SetRangeValueAdd(rangex, rangey, intmapShadow, x, y);
                }
                //}
            }
            return intmapShadow;
        }

        private void SetRangeValueAdd(int rangex, int rangey, int[,] bp, int x, int y)
        {
            int range = Math.Max(rangex, rangey);
            for (int i = x - rangex; i <= x + rangex; i++)
            {
                for (int j = y - rangey; j <= y + rangey; j++)
                {
                    if (CoordinatesHelper.GetDistanceBetweenTwoPoints(i, j, x, y) <= range)
                    {
                        SetPointValueAdd(bp, i, j);
                    }
                }
            }
        }

        private void SetPointValueAdd(int[,] bp, int x, int y)
        {
            if (x >= 0 && y >= 0 && x < widthRear && y < height)
            {
                bp[x, y]++;
            }
        }

        private int[,] BuildIntmapWorked(int[,] intmap, int[,] intmapShadow, int cmpValue, ref int findCount)
        {
            int[,] intmapWorked = new int[widthRear, height];
            findCount = 0;
            for (int i = 0; i < widthRear; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    if (intmap[i, j] > 0 && intmapShadow[i, j] >= cmpValue)
                    {
                        intmapWorked[i, j] = 1;
                        findCount++;
                        //2011.10.13 deal with the rangeMaxL
                        maxHeightInWordedMap = Math.Max(maxHeightInWordedMap, j);
                        minHeightInWordedMap = Math.Min(minHeightInWordedMap, j);
                    }
                }
            }
            return intmapWorked;
        }

        private void BuildTree(int[,] intmapWorked, double rangeMaxL, double rangeBranch, Node root)
        {
            for (int i = 0; i < widthRear; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    Node nod = new Node(i, j);
                    if (intmapWorked[i, j] > 0)
                    {
                        bool findFather = false;
                        Node lastNode = null;
                        foreach (Node groupNode in root.Items)
                        {
                            GetNearestLeaf(groupNode, ref lastNode, i, j);
                        }
                        double d;
                        if (lastNode != null)
                        {
                            d = CoordinatesHelper.GetDistanceBetweenTwoPoints(lastNode.X, lastNode.Y, i, j);
                        }
                        else
                        {
                            d = rangeMaxL + 1;
                        }
                        if (d > rangeMaxL)
                        {
                            Node newGroupNode = new Node(-1, -1);
                            newGroupNode.Add(nod);
                            root.Add(newGroupNode);
                        }
                        else if (d > rangeBranch)
                        {
                            lastNode.Add(nod);
                        }
                        else if (lastNode.Parent != null)
                        {
                            lastNode.Parent.Add(nod);
                        }
                    }
                }
            } //tree build ok
        }

        private void GetNearestLeaf(Node node, ref Node lastNode, int x, int y)
        {
            for (int i = node.Items.Count - 1; i >= Math.Max(0, node.Items.Count - treeLoopNumber); i--)//Math.Max(0,node.Items.Count - 6)
            {
                Node itemNode = node.Items[i];
                if (itemNode.Items.Count <= 0)
                {
                    if (lastNode == null)
                    {
                        lastNode = itemNode;
                    }
                    else
                    {
                        double d1 = CoordinatesHelper.GetDistanceBetweenTwoPoints(itemNode.X, itemNode.Y, x, y);
                        double d2 = CoordinatesHelper.GetDistanceBetweenTwoPoints(lastNode.X, lastNode.Y, x, y);
                        if (d1 < d2)
                        {
                            lastNode = itemNode;
                        }
                    }
                }
                else
                {
                    GetNearestLeaf(itemNode, ref lastNode, x, y);
                }
            }

            #region old code
            //foreach (Node itemNode in node.Items)
            //{
            //    if (itemNode.Items.Count <= 0)
            //    {
            //        if (lastNode == null)
            //        {
            //            lastNode = itemNode;
            //        }
            //        else
            //        {
            //            double d1 = CoordinatesHelper.GetDistanceBetweenTwoPoints(itemNode.X, itemNode.Y, x, y);
            //            double d2 = CoordinatesHelper.GetDistanceBetweenTwoPoints(lastNode.X, lastNode.Y, x, y);
            //            if (d1 < d2)
            //            {
            //                lastNode = itemNode;
            //            }
            //        }
            //    }
            //    else
            //    {
            //        GetNearestLeaf(itemNode, ref lastNode, x, y);
            //    }
            //}
            #endregion
        }

        private void BuildGroupHT(Node node, int minCount)
        {
            //Color color = mycolor[groupIndex % mycolor.Length];
            if (Node.GetTotalCount(node) >= minCount)//node.Count
            {
                //colorIndex++;
                if (node.Count >= minCount)
                {
                    foreach (Node nod in node.Items)
                    {
                        htGroup.Add(string.Format("{0},{1}", nod.X, nod.Y), groupIndex);
                        //bitmap2.SetPixel(nod.X, nod.Y, color);
                    }
                    groupIndex++;
                }
                foreach (Node nod in node.Items)
                {
                    if (nod.Count > 0)
                    {
                        BuildGroupHT(nod, minCount);
                    }
                }
            }
        }

        private void MakeSubGroups(DataTable dt, int minCount, int combinParam, int combinParamZ, int dArea)
        {
            //int combinParam = 15;
            //int combinParamZ = 7;
            //int dArea = 1;
            groupsDT = new List<List<MyDataTable>>();
            for (int i = 0; i < groupIndex; i++)
            {
                groupsDT.Add(new List<MyDataTable>());
            }
            //1st
            foreach (DataRow dr in dt.Select("", "x, z"))//"x desc, z desc"
            {
                string htKey = string.Format("{0},{1}", int.Parse(dr["x"].ToString())
                                                , (int)Math.Floor(height - (double)dr["y"] / PRMax * height));
                if (htGroup.ContainsKey(htKey))
                {
                    int gIndex = (int)htGroup[htKey];
                    bool findGroup = false;
                    foreach (MyDataTable dtA in groupsDT[gIndex])
                    {
                        bool canfind = dtA.FindInRegain(int.Parse(dr["z"].ToString()), int.Parse(dr["x"].ToString()), dArea);
                        if (canfind)
                        {
                            dtA.Add(dr);
                            findGroup = true;
                            break;
                        }
                    }
                    if (!findGroup)
                    {
                        MyDataTable dtB = new MyDataTable(dt.Clone());
                        dtB.Add(dr);
                        groupsDT[gIndex].Add(dtB);
                    }
                }
            }
            //2td
            foreach (DataRow dr in dt.Select("", "x desc, z desc"))//"x desc, z desc"
            {
                string htKey = string.Format("{0},{1}", int.Parse(dr["x"].ToString())
                                                , (int)Math.Floor(height - (double)dr["y"] / PRMax * height));
                if (htGroup.ContainsKey(htKey))
                {
                    int gIndex = (int)htGroup[htKey];
                    bool findGroup = false;
                    foreach (MyDataTable dtA in groupsDT[gIndex])
                    {
                        bool canfind = dtA.FindInRegain(int.Parse(dr["z"].ToString()), int.Parse(dr["x"].ToString()), dArea);
                        if (canfind)
                        {
                            if (!dtA.FindIn(int.Parse(dr["z"].ToString()), int.Parse(dr["x"].ToString())))
                            {
                                dtA.Add(dr);
                            }
                            findGroup = true;
                            break;
                        }
                    }
                    if (!findGroup)
                    {
                        MyDataTable dtB = new MyDataTable(dt.Clone());
                        dtB.Add(dr);
                        groupsDT[gIndex].Add(dtB);
                    }
                }
            }
            //only left max count table
            for (int j = groupsDT.Count - 1; j >= 0; j--)
            {
                List<MyDataTable> ldt = groupsDT[j];
                for (int i = ldt.Count - 1; i > 0; i--)
                {
                    if (ldt[i].Count > ldt[i - 1].Count)
                    {
                        ldt[i - 1] = ldt[i];
                    }
                    ldt.RemoveAt(i);
                }
                if ((ldt.Count > 0 && ldt[0].Count < minCount) || ldt.Count <= 0)
                {
                    groupsDT.RemoveAt(j);
                }
            }
            //fix sort index
            List<MyDataTable> mydataListTemp = null;
            if (true)
            {
                for (int i = 0; i < groupsDT.Count; i++)//i 0~ right to left
                {
                    for (int j = i + 1; j < groupsDT.Count; j++)
                    {
                        bool needChange = false;
                        int minCounttemp = 0;
                        int minZi = groupsDT[i][0].GetMinZ(-1);
                        int minZj = groupsDT[j][0].GetMinZ(-1);
                        int minXi = groupsDT[i][0].GetMinX(ref minCounttemp);
                        int minXj = groupsDT[j][0].GetMinX(ref minCounttemp);
                        if (Math.Abs(minZi - minZj) > combinParamZ)
                        {
                            if (minZi < minZj)
                            {
                                needChange = true;
                            }
                        }
                        else
                        {
                            if (minXi < minXj)
                            {
                                needChange = true;
                            }
                        }

                        if (needChange)
                        {
                            mydataListTemp = groupsDT[i];
                            groupsDT[i] = groupsDT[j];
                            groupsDT[j] = mydataListTemp;
                        }
                    }
                }
            }

            //combin
            if (true)//groupsDT.Count > 7)//this condition only be reference
            {
                for (int i = groupsDT.Count - 1; i > 0; i--)
                {
                    MyDataTable mydata1 = groupsDT[i][0];
                    MyDataTable mydata2 = groupsDT[i - 1][0];
                    int minPointCount = 0;
                    int maxPointCount = 0;
                    int minX_D2 = mydata2.GetMinX(ref minPointCount);
                    int maxX_D1 = mydata1.GetMaxX(ref maxPointCount);
                    if (minX_D2 > 0 && maxX_D1 > 0 && Math.Abs(maxX_D1 - minX_D2) <= combinParam)
                    //&& Math.Abs(minPointCount - maxPointCount)<5)
                    {
                        mydata1.CopyTo(mydata2);
                        groupsDT.RemoveAt(i);
                    }
                }
            }
        }

        //public Bitmap GetBMPMap()
        //{
        //    if (groupsDT == null)
        //    {
        //        throw new Exception("DoTestAuto first.");
        //    }
        //    Bitmap bitmap = new Bitmap(widthFront, widthRear, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
        //    for (int i = 0; i < bitmap.Width; i++)
        //    {
        //        for (int j = 0; j < bitmap.Height; j++)
        //        {
        //            bitmap.SetPixel(i, j, Color.White);
        //        }
        //    }
        //    for (int i = 0; i < groupsDT.Count; i++)
        //    {
        //        Color col = mycolor[i % mycolor.Length];
        //        foreach (DataRow drr in groupsDT[i][0].Rows)
        //        {
        //            double powerRatio = (double)drr["y"];
        //            int colorInt = (int)((1d - powerRatio / PRMax) * 255d);
        //            Color cc = Color.FromArgb(colorInt, colorInt, colorInt);
        //            bitmap.SetPixel(int.Parse(drr["z"].ToString()) - 1, bitmap.Height - int.Parse(drr["x"].ToString()) - 1, col);
        //        }
        //    }
        //    return bitmap;
        //}

        public void SaveFiles(string pathLines, string pathSubLineFormat)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(pathLines, false);
                //StringBuilder sbTitle = new StringBuilder();
                //sbTitle.Append("SM#,LineType,Row,Col,");
                //sw.WriteLine(sbTitle.ToString(0, sbTitle.Length - 1));
                sw.WriteLine("SM#,LineType,Row,Col");

                #region output
                for (int i = groupsDT.Count - 1; i >= 0; i--)
                {
                    int smindex = groupsDT.Count - 1 - i;
                    LineZX lowerLine = lines[smindex][0];
                    LineZX upperLine = lines[smindex][1];
                    LineZX middleLine = lines[smindex][2];

                    StringBuilder sbLower = new StringBuilder();
                    StringBuilder sbUpper = new StringBuilder();
                    StringBuilder sbMiddle = new StringBuilder();

                    StreamWriter swCurrentFile = new StreamWriter(string.Format(pathSubLineFormat, smindex));
                    swCurrentFile.WriteLine("Rear Current [mA],Front Pair Number,Constant Front Current [mA],Non-constant Front Current [mA]");
                    for (int j = 0; j < widthRear; j++)
                    {
                        if (middleLine.Points.ContainsKey(j))
                        {
                            sbLower.AppendLine(string.Format("{0},Lower,{1},{2}", smindex, lowerLine[j], j));
                            sbUpper.AppendLine(string.Format("{0},Upper,{1},{2}", smindex, upperLine[j], j));
                            sbMiddle.AppendLine(string.Format("{0},Middle,{1},{2}", smindex, middleLine[j], j));
                            MapPoint mp = middleLine.GetPoint(j);
                            swCurrentFile.WriteLine("{0},{1},{2},{3}", mp.I_rear, mp.Front_pair_number, mp.I_frontM, mp.I_frontN);
                        }
                    }
                    swCurrentFile.Flush();
                    swCurrentFile.Close();

                    sw.Write("{0}{1}{2}", sbLower.ToString(), sbUpper.ToString(), sbMiddle.ToString());
                }
                #endregion
            }
            catch (Exception ex)
            { }
            finally
            {
                if (sw != null)
                {
                    sw.Flush();
                    sw.Close();
                }
            }
        }

        public void AnalystMiddleLine(int filterSize, Dictionary<int, Dictionary<int, int[]>> outputFilter, int min_points_width_of_sm)
        {
            try
            {
                int lastMin = -1;
                int lastMax = -1;
                List<CPoint> sm0Left = new List<CPoint>();
                bool needFix = false;
                lines = new List<List<LineZX>>();
                for (int i = groupsDT.Count - 1; i >= 0; i--)
                {
                    List<LineZX> subLines = new List<LineZX>();
                    LineZX lowerLine = new LineZX();
                    LineZX upperLine = new LineZX();
                    LineZX middleLine = new LineZX();

                    int smindex = groupsDT.Count - 1 - i;

                    int zeroLineL_maxX = -1;
                    int zeroLineL_minX = 999;
                    int zeroLineR_maxX = -1;
                    int zeroLineR_minX = 999;

                    #region loop in points
                    for (int j = 0; j < widthRear; j++)
                    {
                        //add outputFilter    Jim@2011.12.9
                        if (outputFilter != null && outputFilter.ContainsKey(smindex))
                        {
                            bool isInRegion = false;
                            for (int regionIndex = 0; regionIndex < outputFilter[smindex].Count;regionIndex++ )
                            {
                                if (j >= outputFilter[smindex][regionIndex][0] && j <= outputFilter[smindex][regionIndex][1])
                                {
                                    isInRegion = true;
                                    break;
                                }
                            }
                            if (!isInRegion)
                            {
                                continue;
                            }
                        }


                        try//Jim@2011.11.13
                        {
                        int max = groupsDT[i][0].GetMaxZ(j);
                        int min = groupsDT[i][0].GetMinZ(j);
                        int center = -1;
                        if (max < 0 || min < 0 || Math.Abs(max - min) < min_points_width_of_sm)
                        {
                            continue;
                        }

                        int count = groupsDT[i][0].GetCountZ(j);
                        //string maxString = "";
                        //string minString = "";
                        //string centerString = "";
                        if (max >= 0 && min >= 0)
                        {
                            //maxString = max.ToString();
                            //minString = min.ToString();

                            //int[] maxminZ = groupsDT[i][0].GetMaxMinZ(j);
                            //center = ((maxminZ[0] + maxminZ[1]) / 2);// +((maxminZ[0] + maxminZ[1]) % 2);
                            center = ((max + min) / 2);

                            #region if (smindex == 0)
                            if (smindex == 0)
                            {
                                if (min >= 0 && lastMin > 0 && Math.Abs(max - min - count + 1) >= 3 || needFix)
                                {
                                    if (min < lastMin && sm0Left.Count >= 3 || needFix)
                                    {

                                        double[] xArry = new double[sm0Left.Count];
                                        double[] yArry = new double[sm0Left.Count];
                                        for (int ii = 0; ii < sm0Left.Count; ii++)
                                        {
                                            xArry[ii] = sm0Left[ii].X;
                                            yArry[ii] = sm0Left[ii].Y;
                                        }
                                        LinearLeastSquaresFit llsf = LinearLeastSquaresFitAlgorithm.Calculate(xArry, yArry, 0, yArry.Length - 1);
                                        EquationOfLine eol = CoordinatesHelper.GetEquationOfLineYIntercept(llsf.YIntercept, llsf.Slope);
                                        double x = CoordinatesHelper.GetX(j, eol);
                                            x = Math.Max(0d, x);//Jim@2011.11.13
                                            x = Math.Min(max, x);//Jim@2011.11.13
                                        min = Convert.ToInt32(x);
                                        //minString = x.ToString();
                                        center = Convert.ToInt32((((double)max + x) / 2d));
                                        needFix = true;
                                    }
                                }
                                else
                                {
                                    if (min > 0)
                                    {
                                        sm0Left.Add(new CPoint(min, j));
                                    }
                                    else
                                    {
                                        sm0Left.Clear();
                                    }
                                }
                                lastMax = max;
                                lastMin = min;
                            }
                            #endregion
                            lowerLine.Add(min, j);
                            upperLine.Add(max, j);
                            middleLine.Add(center, j);
                        }

                        if (min <= 0)
                        {
                            if (!(smindex == 0 && needFix))
                            {
                                zeroLineL_maxX = j;
                                if (zeroLineL_minX == 999)
                                {
                                    zeroLineL_minX = j;
                                }
                            }
                        }
                        if (max >= widthFront - 1)
                        {
                            zeroLineR_maxX = j;
                            if (zeroLineR_minX == 999)
                            {
                                zeroLineR_minX = j;
                            }
                        }
                        }
                        catch (Exception ex)
                        { }
                    }//for (int j = 0; j < 201; j++)
                    #endregion

                    #region deal with Z=0&146 points
                    if (zeroLineL_maxX >= 0)//cline.Points.ContainsValue(1))
                    {
                        //
                        int lower_dist_from_origin;
                        lower_dist_from_origin = zeroLineL_maxX;

                        int upper_dist_from_origin;
                        if (zeroLineL_minX == 0) upper_dist_from_origin = -upperLine[0];//groupsDT[i][0].GetMaxZ(0);//- (int)cline[0];
                        else upper_dist_from_origin = zeroLineL_minX;

                        int border_midpoint_row;
                        int border_midpoint_col;
                        int border_midpoint_dist_from_origin = lower_dist_from_origin - (lower_dist_from_origin - upper_dist_from_origin) / 2;
                        if (border_midpoint_dist_from_origin > 0)
                        {
                            border_midpoint_row = 0;//z  0
                            border_midpoint_col = border_midpoint_dist_from_origin;
                        }
                        else
                        {
                            border_midpoint_row = -border_midpoint_dist_from_origin;
                            border_midpoint_col = 0;
                        }

                        int first_middle_pt = 0;
                        int first_middle_pt_col = zeroLineL_maxX + 1;
                        int first_middle_pt_row = (int)middleLine[zeroLineL_maxX + 1];

                        double slope = ((double)(first_middle_pt_row - border_midpoint_row)) / ((double)(first_middle_pt_col - border_midpoint_col));
                        for (int k = first_middle_pt_col - 1; k >= border_midpoint_col; k--)
                        {
                            double row_of_midpoint_double = (double)border_midpoint_row + slope * ((double)(k - border_midpoint_col));
                            int row_of_midpoint = (int)row_of_midpoint_double;
                            if (row_of_midpoint_double - (double)row_of_midpoint > 0.5) row_of_midpoint++;
                            if (row_of_midpoint < 0) row_of_midpoint = 0;

                            // add to middle line
                            //_supermodes[sm_number]._middle_line._points.insert(
                            //    _supermodes[sm_number]._middle_line._points.begin(),
                            //    convertRowCol2LinePoint(row_of_midpoint, i));
                            //_supermodes[sm_number]._middle_line._points.Insert(
                            //    0,
                            //    convertRowCol2LinePoint(row_of_midpoint, i));
                            middleLine[k] = row_of_midpoint;
                        }
                        if (border_midpoint_row == 0)
                        {
                            for (int k = 0; k < border_midpoint_col; k++)
                            {
                                if (middleLine.Points.ContainsKey(k))
                                {
                                    if (useLeftBorder)
                                    {
                                        middleLine[k] = 0;
                                    }
                                    else
                                    {
                                        middleLine.Points.Remove(k);
                                    }
                                }
                            }
                        }

                    }

                    if (zeroLineR_minX != 999 && zeroLineR_minX >= 0)//cline.Points.ContainsValue(147))
                    {
                        //
                        int lower_dist_from_origin;
                        lower_dist_from_origin = zeroLineR_minX;

                        int upper_dist_from_origin;
                        if (zeroLineR_maxX == widthRear - 1) upper_dist_from_origin = widthRear - 1 + (widthFront - 1 - groupsDT[i][0].GetMinZ(widthRear - 1));//- (int)cline[0];
                        else upper_dist_from_origin = zeroLineR_maxX;

                        int border_midpoint_row;
                        int border_midpoint_col;
                        int border_midpoint_dist_from_origin = lower_dist_from_origin - (lower_dist_from_origin - upper_dist_from_origin) / 2;
                        if (border_midpoint_dist_from_origin > widthRear - 1)
                        {
                            border_midpoint_row = widthFront  - 1 - (border_midpoint_dist_from_origin - widthRear + 1);
                            border_midpoint_col = widthRear - 1;
                        }
                        else
                        {
                            border_midpoint_row = widthFront - 1;//z  146
                            border_midpoint_col = border_midpoint_dist_from_origin;
                        }

                        int first_middle_pt = 0;
                        int first_middle_pt_col = zeroLineR_minX - 1;
                        int first_middle_pt_row = (int)middleLine[zeroLineR_minX - 1];

                        double slope = ((double)(first_middle_pt_row - border_midpoint_row)) / ((double)(first_middle_pt_col - border_midpoint_col));
                        for (int k = first_middle_pt_col + 1; k <= border_midpoint_col; k++)
                        {
                            double row_of_midpoint_double = (double)border_midpoint_row + slope * ((double)(k - border_midpoint_col));
                            int row_of_midpoint = (int)row_of_midpoint_double;
                            if (row_of_midpoint_double - (double)row_of_midpoint > 0.5) row_of_midpoint++;
                            if (row_of_midpoint < 0) row_of_midpoint = 0;

                            // add to middle line
                            //_supermodes[sm_number]._middle_line._points.insert(
                            //    _supermodes[sm_number]._middle_line._points.begin(),
                            //    convertRowCol2LinePoint(row_of_midpoint, i));
                            //_supermodes[sm_number]._middle_line._points.Insert(
                            //    0,
                            //    convertRowCol2LinePoint(row_of_midpoint, i));
                            middleLine[k] = row_of_midpoint;
                        }
                        if (border_midpoint_row == widthFront - 1)
                        {
                            for (int k = border_midpoint_col + 1; k < widthRear; k++)
                            {
                                if (middleLine.Points.ContainsKey(k))
                                {
                                    if (useRightBorder)
                                    {
                                        middleLine[k] = widthFront - 1;
                                    }
                                    else
                                    {
                                        middleLine.Points.Remove(k);
                                    }
                                }
                            }
                        }
                    }
                    #endregion

                    #region filter middle line
                    if (vector_Ir != null && vector_Ir._length > 0 && vector_If != null && vector_If._total_length > 0)
                    {
                        LineZX clineCC = new LineZX();
                        clineCC.Points = (Hashtable)middleLine.Points.Clone();
                        foreach (DictionaryEntry de in clineCC.Points)
                        {
                            double avgZValue = clineCC.GetAvgZ((int)de.Key, filterSize);// cl.GetAvgZ((int)de.Key, filterSize);


                            //===========================================
                            MapPoint point_on_map = middleLine.GetPoint((int)de.Key);

                            // find nearest row
                            point_on_map.Z = (int)avgZValue;
                            if (avgZValue - (double)((int)avgZValue) > 0.5) point_on_map.Z++;

                            //2011.10.13 fix middle point
                            if (point_on_map.Z - lowerLine[(int)de.Key] - 1 >= minDistinctToSide
                                && upperLine[(int)de.Key] - point_on_map.Z - 1 >= minDistinctToSide
                                || lowerLine[(int)de.Key] <= minDistinctToSide
                                || upperLine[(int)de.Key] >= widthFront - minDistinctToSide - 1)
                            {
                                //is ok
                            }
                            else if ((lowerLine[(int)de.Key]) % frontPairCount <= distinctToTurnLine - 1
                                || (upperLine[(int)de.Key]) % frontPairCount >= frontPairCount - distinctToTurnLine
                                )
                            {
                                if (upperLine[(int)de.Key] - lowerLine[(int)de.Key] - 2 >= 2 * minDistinctToSide)
                                {
                                    //there is enough points, so move Z to minDistinctToSide position
                                    if (point_on_map.Z - lowerLine[(int)de.Key] - 1 < minDistinctToSide)
                                    {
                                        avgZValue = lowerLine[(int)de.Key] + minDistinctToSide + 1;
                                    }
                                    else
                                    {
                                        avgZValue = upperLine[(int)de.Key] - minDistinctToSide - 1;
                                    }
                                    point_on_map.Z = (int)avgZValue;
                                }
                                else
                                {
                                    //there is not enough points, so set its center point again
                                    avgZValue = lowerLine[(int)de.Key] + (double)(upperLine[(int)de.Key] - lowerLine[(int)de.Key]) / 2d;
                                    point_on_map.Z = (int)avgZValue;
                                }
                            }

                            // find nearest col  (col is allready int type)
                            //point_on_map.X = (int)col;
                            //if (col - (double)((int)col) > 0.5) point_on_map.X++;

                            //if (col > (double)((int)col))
                            //{
                            //    // interpolate Ir linearly
                            //    double Ir_a = _vector_Ir._currents[(int)col];
                            //    double Ir_b = _vector_Ir._currents[((int)col) + 1];

                            //    double Ir_fraction = col - (double)((int)col);

                            //    point_on_map.I_rear = Ir_a + Ir_fraction * (Ir_b - Ir_a);
                            //}
                            //else
                            {
                                // no need to interpolate
                                point_on_map.I_rear = vector_Ir._currents[point_on_map.X];
                            }

                            point_on_map.I_frontM = 0;
                            point_on_map.I_frontN = 0;

                            point_on_map.Front_pair_number = (point_on_map.Z + 1) / frontPairCount + ((point_on_map.Z + 1) % frontPairCount > 0 ? 1 : 0);
                            if (point_on_map.Front_pair_number == 0)
                            {
                                ;
                            }
                            short index_of_nearest = (short)(point_on_map.Z - (point_on_map.Front_pair_number - 1) * frontPairCount);

                            double If_fraction = avgZValue - (double)(point_on_map.Z);
                            int top_row = frontPairCount - 1;

                            if ((index_of_nearest == 0 && If_fraction < 0)
                             || (index_of_nearest == (short)top_row && If_fraction > 0))
                            {
                                // cannot interpolate between maps!
                                // use nearest point for currents
                                If_fraction = 0;
                            }

                            double If_value;

                            if (If_fraction != 0)
                            {
                                // need to interpolate
                                short index_of_nextpt;
                                if (If_fraction > 0) index_of_nextpt = (short)(index_of_nearest + 1);
                                else
                                {
                                    index_of_nextpt = (short)(index_of_nearest - 1);
                                    If_fraction = -If_fraction; // ensure positive fraction
                                }

                                // interpolate If linearly
                                double If_a = vector_If.getNonConstantCurrent((short)point_on_map.Front_pair_number, index_of_nearest);
                                double If_b = vector_If.getNonConstantCurrent((short)point_on_map.Front_pair_number, index_of_nextpt);
                                If_value = If_a + If_fraction * (If_b - If_a);
                            }
                            else
                            {
                                // no need to interpolate
                                If_value = vector_If.getNonConstantCurrent((short)point_on_map.Front_pair_number, index_of_nearest);
                            }

                            point_on_map.I_frontM = vector_If.getConstantCurrent((short)point_on_map.Front_pair_number, index_of_nearest);
                            point_on_map.I_frontN = If_value;
                            //===========================================

                            //middleLine[(int)de.Key] = (int)avgValue;
                        }
                    }
                    #endregion


                    //add@20110817 Jim
                    #region fix turn back   (right min distinct:2  front section change line max distinct:2)
                    if (true)
                    {
                        int lastIndexJ = 0;
                        int lastBoundary = 0;
                        for (int j = 1; j < middleLine.Points.Count; j++)
                        {
                            if (middleLine.GetPoint(j) != null)
                            {
                                int zIndex = middleLine[j];
                                if (zIndex % frontPairCount == 0 && lastBoundary < zIndex)
                                {
                                    lastIndexJ = j;
                                    lastBoundary = zIndex;
                                }
                                if (zIndex < lastBoundary && lastBoundary - zIndex <= 2
                                    && upperLine[j] > lastBoundary && upperLine[j] - lastBoundary >= 3)
                                {
                                    MapPoint mpLastJ = middleLine.GetPoint(lastIndexJ);
                                    MapPoint mpJ = middleLine.GetPoint(j);
                                    mpJ.Front_pair_number = mpLastJ.Front_pair_number;
                                    mpJ.I_frontM = mpLastJ.I_frontM;
                                    mpJ.I_frontN = mpLastJ.I_frontN;
                                    mpJ.Z = mpLastJ.Z;
                                }
                            }
                        }
                    }
                    #endregion

                    subLines.Add(lowerLine);
                    subLines.Add(upperLine);
                    subLines.Add(middleLine);
                    lines.Add(subLines);
                }//for (int i = groupsDT.Count - 1; i >= 0; i--)

                
                
            }
            catch (Exception ex)
            { }
            finally
            {
                //if (sw != null)
                //{
                //    sw.Flush();
                //    sw.Close();
                //}
            }
        }

        //public void ReadIrear(string path)
        //{
        //    if (vector_Ir == null)
        //    {
        //        vector_Ir = new List<double>();
        //    }
        //    else
        //    {
        //        vector_Ir.Clear();
        //    }
        //    StreamReader sr = new StreamReader(path);
        //    try
        //    {
        //        string strIrear = sr.ReadLine();
        //        while (strIrear != null)
        //        {
        //            double irear = 0;
        //            if (double.TryParse(strIrear, out irear))
        //            {
        //                vector_Ir.Add(irear);
        //            }
        //            strIrear = sr.ReadLine();
        //        }
        //    }
        //    catch (Exception ex)
        //    { }
        //    finally
        //    {
        //        sr.Close();
        //    }
        //}
        //public void ReadIfront(string path)
        //{
        //    if (vector_If == null)
        //    {
        //        vector_If = new List<List<double[]>>();
        //        for (int i = 0; i < 7; i++)
        //        {
        //            vector_If.Add(new List<double[]>());
        //        }
        //    }
        //    else
        //    {
        //        vector_If.Clear();
        //    }
        //    StreamReader sr = new StreamReader(path);
        //    try
        //    {
        //        string strIpair = sr.ReadLine();
        //        while (strIpair != null)
        //        {
        //            string[] strValues = strIpair.Split(',');
        //            if (strValues.Length >= 3)
        //            {
        //                int frontNumber = 0;
        //                if (int.TryParse(strValues[0], out frontNumber))
        //                {
        //                    vector_If[frontNumber - 1].Add(new double[] { Convert.ToDouble(strValues[1]), Convert.ToDouble(strValues[2]) });
        //                }
        //            }
        //            strIpair = sr.ReadLine();
        //        }
        //    }
        //    catch (Exception ex)
        //    { }
        //    finally
        //    {
        //        sr.Close();
        //    }
        //}
    }

    /// <summary>
    /// Tree node
    /// </summary>
    public class Node
    {
        public int X;

        public int Y;

        public Node(int x, int y)
        {
            X = x;
            Y = y;
            Parent = null;
            Items = new List<Node>();
        }

        public List<Node> Items;

        public Node Parent;

        public int Count
        {
            get
            {
                return Items.Count;
            }
        }

        public void Add(Node node)
        {
            Items.Add(node);
            node.Parent = this;
        }

        public static int GetTotalCount(Node node)
        {
            int count = 0;
            if (node.Count <= 0)
            {
                return 1;
            }
            else
            {
                foreach (Node n in node.Items)
                {
                    count += GetTotalCount(n);
                }
            }
            return count;
        }
    }

    public class MyDataTable
    {
        private DataTable datatable;

        private Hashtable hashtable;

        public MyDataTable(DataTable dt)
        {
            hashtable = new Hashtable();
            this.datatable = dt;
        }

        public int Count
        {
            get
            {
                return datatable.Rows.Count;
            }
        }

        public DataRowCollection Rows
        {
            get
            {
                return datatable.Rows;
            }
        }

        public void Add(DataRow dr)
        {
            this.datatable.Rows.Add(dr.ItemArray);
            string key = string.Format("{0},{1}", dr["z"], dr["x"]);
            if (!hashtable.ContainsKey(key))
            {
                hashtable.Add(key, null);
            }
        }

        public void Remove(DataRow dr)
        {
            string key = string.Format("{0},{1}", dr["z"], dr["x"]);
            hashtable.Remove(key);
            DataRow[] drs = datatable.Select(string.Format("x={0} and y={1} and z={2}", dr["x"], dr["y"], dr["z"]));
            foreach (DataRow adr in drs)
            {
                datatable.Rows.Remove(adr);
            }
        }

        public void CopyTo(MyDataTable mydata)
        {
            foreach (DataRow dr in datatable.Rows)
            {
                mydata.Add(dr);
            }
        }

        public bool FindInRegain(int z, int x, int range)
        {
            //for (int i = z - 1; i <= z + 1; i++)
            //{
            //    for (int j = x - range; j <= x + range; j++)
            //    {
            //        string key = string.Format("{0},{1}", i, j);
            //        if (hashtable.ContainsKey(key))
            //        {
            //            return true;
            //        }
            //    }
            //}
            for (int i = z - 1; i <= z + 1; i++)
            {
                string key = string.Format("{0},{1}", i, x);
                if (hashtable.ContainsKey(key))
                {
                    return true;
                }
            }
            for (int j = x - range; j <= x + range; j++)
            {
                string key = string.Format("{0},{1}", z, j);
                if (hashtable.ContainsKey(key))
                {
                    return true;
                }
            }
            return false;
        }

        //public bool FindInRegain(int z, int x, int range, int minCount)
        //{
        //    int count = 0;
        //    for (int i = z - range; i <= z + range; i++)
        //    {
        //        for (int j = x - range; j <= x + range; j++)
        //        {
        //            string key = string.Format("{0},{1}", i, j);
        //            if (hashtable.ContainsKey(key))
        //            {
        //                count++;
        //            }
        //        }
        //    }
        //    return count - 1 >= minCount;
        //}

        public int GetMaxZ(int x)
        {
            object obj = datatable.Compute("max(z)", x >= 0 ? "x=" + x.ToString() : "");
            if (obj != DBNull.Value)
            {
                return int.Parse(obj.ToString());
            }
            return -1;
        }

        public int GetMinZ(int x)
        {
            object obj = datatable.Compute("min(z)", x >= 0 ? "x=" + x.ToString() : "");
            if (obj != DBNull.Value)
            {
                return int.Parse(obj.ToString());
            }
            return -1;
        }

        public int[] GetMaxMinZ(int x)
        {
            int[] ret = new int[] { -1, -1 };

            DataRow[] drs = datatable.Select("x=" + x.ToString(), "z");
            if (drs.Length > 0)
            {
                int enableEmptyCount = 1;//enable empty point between side lines, this is enable empty points count value
                List<int> countList = new List<int>();
                int count = 1;
                int lastZ = Convert.ToInt32(drs[0]["z"]);

                for (int i = 1; i < drs.Length; i++)
                {
                    int z = Convert.ToInt32(drs[i]["z"]);
                    if (z != lastZ + 1 && z - lastZ - 1 > enableEmptyCount)
                    {
                        countList.Add(count);
                        count = 1;
                        //if (i < drs.Length - 1)
                        //{
                        //    i++;
                        //    lastZ = (int)drs[i]["z"];
                        //    count = 1;
                        //}
                    }
                    else
                    {
                        count++;
                    }
                    lastZ = z;
                }
                countList.Add(count);
                if (countList.Count > 0)
                {
                    int maxIndex = 0;
                    for (int i = 1; i < countList.Count; i++)
                    {
                        if (countList[i] > countList[maxIndex])
                        {
                            maxIndex = i;
                        }
                    }
                    //int max = -1;
                    //int min = -1;
                    int countPre = 0;
                    for (int i = 0; i < maxIndex; i++)
                    {
                        countPre += countList[i];
                    }
                    ret[0] = Convert.ToInt32(drs[countPre]["z"]);
                    ret[1] = Convert.ToInt32(drs[countPre + countList[maxIndex] - 1]["z"]);
                }
            }
            return ret;
        }

        public int GetCountZ(int x)
        {
            object obj = datatable.Compute("count(z)", x >= 0 ? "x=" + x.ToString() : "");
            if (obj != DBNull.Value)
            {
                return int.Parse(obj.ToString());
            }
            return -1;
        }

        public int GetMaxX(ref int count)
        {
            object obj = datatable.Compute("max(x)", "");
            if (obj != null)
            {
                int maxX = int.Parse(obj.ToString());
                //count = (int)datatable.Compute("count(z)", "x=" + maxX.ToString());
                return maxX;
            }
            return -1;
        }

        public int GetMinX(ref int count)
        {
            object obj = datatable.Compute("min(x)", "");
            if (obj != null)
            {
                int minX = int.Parse(obj.ToString());
                //count = (int)datatable.Compute("count(z)", "x=" + minX.ToString());
                return minX;
            }
            return -1;
        }

        public bool FindIn(int z, int x)
        {
            string key = string.Format("{0},{1}", z, x);
            if (hashtable.ContainsKey(key))
            {
                return true;
            }
            return false;
        }
    }

    public class CenterPointSlope
    {
        public double X;

        public double Y;

        public double Slope;

        public int GroupIndex;

        public double[] XArry;

        public double[] YArry;

        public CenterPointSlope()
        { }

        public CenterPointSlope(double x, double y, double slope, int groupIndex, double[] xArry, double[] yArry)
        {
            X = x;
            Y = y;
            Slope = slope;
            GroupIndex = groupIndex;
            XArry = xArry;
            YArry = yArry;
        }
    }

    public class MapPoint
    {
        public int Z;
        public int X;
        public double I_rear;
        public double I_frontM;
        public double I_frontN;
        public int Front_pair_number;
        public MapPoint(int z, int x)
        {
            Z = z;
            X = x;
        }
    }

    public class LineZX
    {
        public int SMNumber;
        public Hashtable Points = new Hashtable();
        public void Add(int z, int x)
        {
            Points.Add(x, new MapPoint(z, x));
        }
        public double GetAvgZ(int x, int filterSize)
        {
            double count = 0;
            double sum = 0;
            for (int i = x - filterSize; i <= x + filterSize; i++)
            {
                if (i >= 0)
                {
                    if (Points.ContainsKey(i))
                    {
                        count++;
                        sum += Convert.ToDouble(((MapPoint)Points[i]).Z);
                    }
                }
            }
            return sum / count;
        }
        public int this[int x]
        {
            get
            {
                return ((MapPoint)Points[x]).Z;// (int)Points[x];
            }
            set
            {
                ((MapPoint)Points[x]).Z = value;
            }
        }
        public MapPoint GetPoint(int x)
        {
            return (MapPoint)Points[x];
        }
    }
}

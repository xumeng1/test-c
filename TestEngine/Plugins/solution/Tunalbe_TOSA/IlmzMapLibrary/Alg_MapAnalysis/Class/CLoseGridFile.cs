using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Bookham.TestLibrary.Algorithms
{
    class CLoseGridFile : IDisposable
    {

        const string TAB_DELIMITOR = "	";
        const string CSV_DELIMITOR = ",";
        const int MAX_ROW_LENGTH = 100000;
        const int MAX_COL_LENGTH = 100000;
        const char ENDLINE_CHAR = '\n';
        const string EXT_SEPERATOR = ".";
        const string FOLDER_SEPERATOR = "\\";
        const string DELIMITORS = ",\t\r\b\f\n";

        const string TEXTFILE_EXTENSION = "txt";
        const string CSVFILE_EXTENSION = "csv";

        const int HEADER_ROW = 1;

        // Errors
        const int CG_NO_ERROR = 0;
        const int ERROR_FILEEXISTS = -1;
        const int ERROR_FILEOPEN = -2;
        const int ERROR_FILENOTFOUND = -3;
        const int ERROR_FILECLOSED = -4;
        const int ERROR_ENDOFFILE = -5;


        const string STANDARD_DELIMITORS = "\\.,	:;!'#";
        const string COMMA_DELIMITOR = ",";

        #region Variants

        public int _numRows;

        //protected:

        // example of a file = C:\\PXIT\\Laser Run Results\\478\\2003_06_28_14_55_59\\CLoseGridConfigFile.csv

        protected String _fileName;	// CLoseGridConfigFile

        protected String _fileNameWithExtension;	// CLoseGridConfigFile.csv

        protected String _filePath;	// C:\\PXIT\\Laser Run Results\\478\\2003_06_28_14_55_59\\CLoseGridConfigFile.csv

        protected String _fileExtension;	// csv, txt etc

        protected String _folder;	// 2003_06_28_14_55_59

        protected String _folderPath; // C:\\PXIT\\Laser Run Results\\478\\2003_06_28_14_55_59

        protected String _drive;		// C:

        //CFileStatus _fileStatus;

        // the handle to our file
        //std::ofstream* _writeFileStream;
        protected StreamWriter _writeFileStream;

        // the handle to our file
        //std::ifstream* _readFileStream;
        protected StreamReader _readFileStream;

        // delimitor to use when reading from
        // writing to the file e.g. , . tab etc
        protected String _delimitor;

        #endregion


        public CLoseGridFile(String filePath)
        {
            _filePath = filePath;
            _fileNameWithExtension = Path.GetExtension(_filePath);
            _folderPath = Path.GetDirectoryName(_filePath);

            _delimitor = COMMA_DELIMITOR;

            _numRows = 0;
        }
        public CLoseGridFile(String folderPath, String fileNameWithExtension)
        {
            _fileNameWithExtension = fileNameWithExtension;
            _folderPath = folderPath;

            _filePath = _folderPath + FOLDER_SEPERATOR + _fileNameWithExtension;
            _delimitor = TAB_DELIMITOR;

            _numRows = 0;

            setFileExtension();
            setFileName();
            setFolder();
            setDrive();
        }

        public CLoseGridFile(String folderPath, String fileNameWithExtension, String delimitor)
        {
            _fileNameWithExtension = fileNameWithExtension;
            _folderPath = folderPath;

            _filePath = _folderPath + FOLDER_SEPERATOR + _fileNameWithExtension;
            _delimitor = delimitor;

            _numRows = 0;

            setFileExtension();
            setFileName();
            setFolder();
            setDrive();
        }

        public CLoseGridFile(String folderPath, String fileName, String extension, String delimitor)
        {

            _folderPath = folderPath;
            _fileName = fileName;
            _fileExtension = extension;
            _delimitor = delimitor;

            _fileNameWithExtension = _fileName + EXT_SEPERATOR + _fileExtension;
            _filePath = _folderPath + FOLDER_SEPERATOR + _fileNameWithExtension;

            _numRows = 0;

            setFolder();
            setDrive();
        }

        public CLoseGridFile()
        {
            _numRows = 0;
        }


        public int createFolderAndFile()
        {
            int errorCode = CG_NO_ERROR;

            // if directory does not exist, then make it
            if (!Directory.Exists(_folderPath))
            {
                Directory.CreateDirectory(_folderPath);
            }
            if (File.Exists(_filePath))
            {
                // file already exists
                errorCode = ERROR_FILEEXISTS;
                File.Delete(_filePath);
            }
            // File does not exist => okay to create it.
            try
            {
                _writeFileStream = new StreamWriter(_filePath);
            }
            catch(Exception)
            {
                // error
                errorCode = ERROR_FILEOPEN;
                //throw;
            }

            return errorCode;
        }

        public int openFileForWriting()
        {
            return createFolderAndFile();
        }

        public int openFileForReading()
        {
            int errorCode = CG_NO_ERROR;

            if (File.Exists(_filePath))
            {
                // file exists 
                //_readFileStream = new std::ifstream(_filePath, ios::in);
                try
                {
                    _readFileStream = new StreamReader(_filePath);
                }
                finally
                {
                    // error
                    errorCode = ERROR_FILEOPEN;
                }
            }
            else
            {
                errorCode = ERROR_FILENOTFOUND;
            }

            return errorCode;
        }

        public int addRow(List<string> rowArray)
        {
            int errorCode = CG_NO_ERROR;
            if (_writeFileStream != null)
            {
                for (int i = 0; i < rowArray.Count; i++)
                {
                    _writeFileStream.Write(rowArray[i]);
                    if (i < rowArray.Count - 1)
                    {
                        _writeFileStream.Write(_delimitor);
                    }
                }
                _writeFileStream.WriteLine();
            }
            else
                errorCode = ERROR_FILECLOSED;

            return errorCode;
        }

        public int addEmptyLine()
        {
            int errorCode = CG_NO_ERROR;

            if (_writeFileStream != null)
            {
                // go to next line
                //*_writeFileStream << endl;
                _writeFileStream.WriteLine();
            }
            else
            {
                errorCode = ERROR_FILECLOSED;
            }

            return errorCode;
        }


        public int addPage(List<double> array, int rowSize)//&
        {
            int errorCode = CG_NO_ERROR;

            if (array.Count < rowSize)
            {
                rowSize = array.Count;
            }

            //vector<double>::iterator iter = array.begin();
            int iter = 0;
            while (iter < array.Count)
            {
                //CStringArray row;
                List<string> row = new List<string>();
                for (int i = 0; i < rowSize; i++)
                {
                    if (iter != array.Count)
                    {
                        String rowItem = "";
                        double arrayElement = 0;

                        arrayElement = array[iter];
                        //rowItem.Format("%g", arrayElement);
                        rowItem = arrayElement.ToString("g");
                        row.Add(rowItem);

                        iter++;
                    }
                }

                errorCode = addRow(row);

                if (errorCode!=0)
                {
                    break;
                }
            }

            return errorCode;
        }


        public int readFile(List<double> fileContents)//&
        {
            int errorCode = CG_NO_ERROR;

            if (_readFileStream != null)
            {
                //char buffer[MAX_ROW_LENGTH];
                //char[] buffer=new char[MAX_ROW_LENGTH];

                // go to the start of the file
                //_readFileStream->clear();//Clears all error flags.
                //Moves the read position in a stream.
                //_readFileStream->seekg(0, ios::beg);
                _readFileStream.BaseStream.Seek(0, SeekOrigin.Begin);

                //*buffer = '\0';
                //buffer[0]='\0';

                // if this is the end of file, return
                //while(!(_readFileStream->eof()))
                while (!_readFileStream.EndOfStream)
                {
                    //_readFileStream->getline(buffer,MAX_ROW_LENGTH,ENDLINE_CHAR);
                    string txt = _readFileStream.ReadLine();

                    //if(strlen(buffer) > 0) // there is something on the line
                    if (txt.Length > 0)
                    {
                        //char *fp = strtok(buffer, DELIMITORS);
                        //while(fp)
                        //{
                        //    CString	rowEntry;
                        //    rowEntry = CString(fp);

                        //    if(rowEntry != "")
                        //        fileContents.push_back(atof(rowEntry));
                        //    else
                        //        fileContents.push_back(0);

                        //    // next row entry
                        //    fp = strtok(NULL, DELIMITORS);
                        //}
                        foreach (string var in txt.Split(DELIMITORS.ToCharArray()))
                        {
                            if (var != string.Empty)
                                fileContents.Add(double.Parse(var));
                            else
                                fileContents.Add(0);
                        }
                    }
                }

                //_readFileStream->clear();
            }
            else
            {
                errorCode = ERROR_FILECLOSED;
            }

            return errorCode;
        }


        public int readFile(List<string> fileArray)//&
        {
            int errorCode = CG_NO_ERROR;

            if (_readFileStream != null)
            {
                //char buffer[MAX_ROW_LENGTH];

                // go to the start of the file
                //_readFileStream->clear();
                //_readFileStream->seekg(0, ios::beg);
                _readFileStream.BaseStream.Seek(0, SeekOrigin.Begin);

                //*buffer = '\0';

                // if this is the end of file, return
                //while(!(_readFileStream->eof()))
                while (!_readFileStream.EndOfStream)
                {
                    //_readFileStream->getline(buffer,MAX_ROW_LENGTH,ENDLINE_CHAR);
                    string txt = _readFileStream.ReadLine();

                    //if(strlen(buffer) > 0) // there is something on the line
                    if (txt.Length > 0)
                    {
                        //char *fp = strtok(buffer, DELIMITORS);
                        //while(fp)
                        //{
                        //    CString	rowEntry;

                        //    rowEntry = CString(fp);
                        //    fileArray.Add(rowEntry);

                        //    // next row entry
                        //    fp = strtok(NULL, _delimitor);
                        //}
                        fileArray.AddRange(txt.Split(DELIMITORS.ToCharArray()));
                    }
                }

                //_readFileStream->clear();
            }
            else
            {
                errorCode = ERROR_FILECLOSED;
            }

            return errorCode;
        }


        public int readRow(int rowNumber, List<string> rowArray)//&
        {
            int errorCode = CG_NO_ERROR;

            if (_readFileStream != null)
            {
                //char buffer[MAX_ROW_LENGTH];

                // go to the start of the file
                //_readFileStream->clear();
                //_readFileStream->seekg(0, ios::beg);
                _readFileStream.BaseStream.Seek(0, SeekOrigin.Begin);
                string txt = "";
                for (int i = 0; i < rowNumber; i++)
                {
                    //*buffer = '\0';
                    //_readFileStream->getline(buffer,MAX_ROW_LENGTH,ENDLINE_CHAR);
                    txt = _readFileStream.ReadLine();
                }

                // if this is the end of file, return
                //if(_readFileStream->eof())
                if (_readFileStream.EndOfStream)
                {
                    errorCode = ERROR_ENDOFFILE;
                }
                else
                {
                    if (txt.Length > 0) // there is something on the line
                    {
                        //char *fp = strtok(buffer, DELIMITORS);
                        //while(fp)
                        //{
                        //    CString	rowEntry;

                        //    rowEntry = CString(fp);
                        //    rowArray.Add(rowEntry);

                        //    // next row entry
                        //    fp = strtok(NULL, DELIMITORS);
                        //}
                        rowArray.AddRange(txt.Split(DELIMITORS.ToCharArray()));
                    }
                }

                //_readFileStream->clear();
                //_readFileStream->seekg(0, ios::beg);
                _readFileStream.BaseStream.Seek(0, SeekOrigin.Begin);
            }
            else
            {
                errorCode = ERROR_FILECLOSED;
            }

            return errorCode;
        }

        //
        ////////////////////////////////////////////////////////////////////////////////////

        public int readColumn(int columnNumber, bool includeFirstCell, List<double> columnArray)//&
        {
            int errorCode = CG_NO_ERROR;

            if (_readFileStream != null)
            {
                //char buffer[MAX_COL_LENGTH];

                // go to the start of the file
                //_readFileStream->clear();
                //_readFileStream->seekg(0, ios::beg);
                _readFileStream.BaseStream.Seek(0, SeekOrigin.Begin);

                string txt = "";
                if (includeFirstCell == false)
                {
                    // do initial read to move the pointer along
                    //*buffer = '\0';
                    //_readFileStream->getline(buffer,MAX_ROW_LENGTH,ENDLINE_CHAR);
                    txt = _readFileStream.ReadLine();
                }

                //while(!(_readFileStream->eof()))
                while (!_readFileStream.EndOfStream)
                {
                    //*buffer = '\0';
                    //_readFileStream->getline(buffer,MAX_ROW_LENGTH,ENDLINE_CHAR);
                    txt = _readFileStream.ReadLine();


                    if (txt.Length > 0) // there is something on the line
                    {
                        //char *fp = strtok(buffer, DELIMITORS);
                        //for(int i=1; i!=columnNumber; i++)
                        //{
                        //    fp = strtok(NULL, DELIMITORS);
                        //}
                        string fp = txt.Split(DELIMITORS.ToCharArray())[columnNumber - 1];

                        double entry = 0;
                        if (fp.Length > 0) entry = double.Parse(fp);
                        //if(fp.Length>0)
                        //{
                        //    CString entryString = CString(fp);
                        //    if(entryString!="")
                        //        entry = atof((LPCTSTR)entryString);
                        //    else
                        //        entry = 0;
                        //}
                        //else
                        //{
                        //    entry = 0;
                        //}
                        columnArray.Add(entry);
                    }
                }
                //_readFileStream->clear();
            }
            else
            {
                errorCode = ERROR_FILECLOSED;
            }

            return errorCode;
        }

        public int readColumn(int columnNumber, bool includeFirstCell, List<int> columnArray)//&     
        {
            int errorCode = CG_NO_ERROR;

            if (_readFileStream != null)
            {
                //char buffer[MAX_COL_LENGTH];

                // go to the start of the file
                //_readFileStream->clear();
                //_readFileStream->seekg(0, ios::beg);
                _readFileStream.BaseStream.Seek(0, SeekOrigin.Begin);

                string txt = "";
                if (includeFirstCell == false)
                {
                    // do initial read to move the pointer along
                    //*buffer = '\0';
                    //_readFileStream->getline(buffer,MAX_ROW_LENGTH,ENDLINE_CHAR);
                    txt = _readFileStream.ReadLine();
                }

                //while(!(_readFileStream->eof()))
                while (!_readFileStream.EndOfStream)
                {
                    //*buffer = '\0';
                    //_readFileStream->getline(buffer,MAX_ROW_LENGTH,ENDLINE_CHAR);
                    txt = _readFileStream.ReadLine();



                    if (txt.Length > 0) // there is something on the line
                    {
                        //char *fp = strtok(buffer, DELIMITORS);
                        //for(int i=1; i!=columnNumber; i++)
                        //{
                        //    fp = strtok(NULL, DELIMITORS);
                        //}
                        string fp = txt.Split(DELIMITORS.ToCharArray())[columnNumber - 1];

                        int entry = 0;
                        if (fp.Length > 0) entry = int.Parse(fp);
                        //if(fp)
                        //{
                        //    CString entryString = CString(fp);
                        //    if(entryString!="")
                        //        entry = atoi((LPCTSTR)entryString);
                        //    else
                        //        entry = 0;
                        //}
                        //else
                        //{
                        //    entry = 0;
                        //}
                        columnArray.Add(entry);
                    }
                }
                //_readFileStream->clear();
            }
            else
            {
                errorCode = ERROR_FILECLOSED;
            }
            return errorCode;
        }

        public int readColumn(int columnNumber, bool includeFirstCell, List<string> columnArray)//&
        {
            int errorCode = CG_NO_ERROR;

            if (_readFileStream != null)
            {
                //char buffer[MAX_COL_LENGTH];

                // go to the start of the file
                //_readFileStream->clear();
                //_readFileStream->seekg(0, ios::beg);
                _readFileStream.BaseStream.Seek(0, SeekOrigin.Begin);

                string txt = "";
                if (includeFirstCell == false)
                {
                    // do initial read to move the pointer along
                    //*buffer = '\0';
                    //_readFileStream->getline(buffer,MAX_ROW_LENGTH,ENDLINE_CHAR);
                    txt = _readFileStream.ReadLine();
                }

                //while(!(_readFileStream->eof()))
                while (!_readFileStream.EndOfStream)
                {
                    //*buffer = '\0';
                    //_readFileStream->getline(buffer,MAX_ROW_LENGTH,ENDLINE_CHAR);
                    txt = _readFileStream.ReadLine();


                    if (txt.Length > 0) // there is something on the line
                    {
                        //char *fp = strtok(buffer, DELIMITORS);
                        //for(int i=1; i<columnNumber; i++)
                        //{
                        //    fp = strtok(NULL, DELIMITORS);
                        //}
                        string fp = txt.Split(DELIMITORS.ToCharArray())[columnNumber - 1];

                        //if(fp)
                        //{
                        columnArray.Add(fp);
                        //}
                    }
                }
                //_readFileStream->clear();
            }
            else
            {
                errorCode = ERROR_FILECLOSED;
            }
            return errorCode;
        }

        public int numRows()
        {
            int numRows = 0;

            if (_numRows == 0)
            {
                //char buffer[MAX_COL_LENGTH];

                // go to the start of the file
                //_readFileStream->clear();
                //_readFileStream->seekg(0, ios::beg);
                _readFileStream.BaseStream.Seek(0, SeekOrigin.Begin);

                //while(!(_readFileStream->eof()))
                while (!_readFileStream.EndOfStream)
                {
                    //*buffer = '\0';
                    //_readFileStream->getline(buffer,MAX_ROW_LENGTH,ENDLINE_CHAR);
                    string txt = _readFileStream.ReadLine();

                    if (txt.Length > 0) // there is something on the line
                    {
                        _numRows++;
                    }
                }

                // go to the start of the file
                //_readFileStream->clear();
                //_readFileStream->seekg(0, ios::beg);
                _readFileStream.BaseStream.Seek(0, SeekOrigin.Begin);
            }

            numRows = _numRows;

            return numRows;
        }


        ////////////////////////////////////////////////////////////////////////////////////
        //
        void splitIntoSeperateStrings(String theString, List<string> stringArray, String delimitors)
        {
            //int curPos= 0;
            //CString extractedString = theString.Tokenize(delimitors,curPos);
            //while (extractedString != "")
            //{
            //    stringArray.Add(extractedString);
            //    extractedString = theString.Tokenize(delimitors,curPos);
            //}

            stringArray.AddRange(theString.Split(delimitors.ToCharArray()));
        }

        void splitIntoSeperateStrings(String theString, List<string> stringArray)
        {
            //int curPos= 0;

            //CString extractedString;
            //extractedString = theString.Tokenize(STANDARD_DELIMITORS,curPos);

            //while (extractedString != "")
            //{
            //    stringArray.Add(extractedString);
            //    extractedString = theString.Tokenize(STANDARD_DELIMITORS,curPos);
            //}

            stringArray.AddRange(theString.Split(STANDARD_DELIMITORS.ToCharArray()));

        }

        void setFileExtension()
        {
            List<string> fileNameArray = new List<string>();
            splitIntoSeperateStrings(_fileNameWithExtension,
                                    fileNameArray,
                                    _delimitor);

            if (fileNameArray.Count == 2)
                //_fileExtension = fileNameArray.GetAt(1);
                _fileExtension = fileNameArray[1];
            else
                _fileExtension = "";
        }

        void setFileName()
        {
            List<string> fileNameArray = new List<string>();
            splitIntoSeperateStrings(_fileNameWithExtension,
                                    fileNameArray,
                                    _delimitor);

            if (fileNameArray.Count == 2)
                //_fileName = fileNameArray.GetAt(0);
                _fileName = fileNameArray[0];
            else
                _fileName = "";
        }

        void setFolder()
        {
            List<string> folderArray = new List<string>();
            splitIntoSeperateStrings(_folderPath,
                                    folderArray,
                                    FOLDER_SEPERATOR);

            if (folderArray.Count > 0)
            {
                //INT_PTR numFolders  = folderArray.GetCount();
                //_folder = folderArray.GetAt(numFolders-1);
                _folder = folderArray[folderArray.Count - 1];
            }
        }

        void setDrive()
        {
            List<string> folderArray = new List<string>();
            splitIntoSeperateStrings(_folderPath,
                                    folderArray,
                                    FOLDER_SEPERATOR);

            if (folderArray.Count > 0)
            {
                //INT_PTR firstFolderIndex  = 0;
                //_drive = folderArray.GetAt(firstFolderIndex);
                _drive = folderArray[0];
            }
        }


        #region IDisposable Members

        bool disposed = false;
        public void Dispose()
        {
            Dispose(true);
        }
        protected void Dispose(bool disposeManagedResources)
        {
            if (!this.disposed)
            {
                if (disposeManagedResources)
                {
                    //
                }
                if (_writeFileStream != null)
                {
                    _writeFileStream.Close();
                    _writeFileStream.Dispose();
                    _writeFileStream = null;
                }
                if (_readFileStream != null)
                {
                    _readFileStream.Close();
                    _readFileStream.Dispose();
                    _readFileStream = null;
                }

                this.disposed = true;
            }
        }

        #endregion

    }
}

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Bookham.TestLibrary.Algorithms
{
    class CDSDBRSupermodeMapLine
    {
        public enum rcode
        {
            ok = 0,
            file_does_not_exist,
            file_already_exists,
            could_not_open_file,
            file_format_invalid,
            //files_not_matching_sizes,
            //corrupt_map_not_written_to_file,
            //existing_file_not_overwritten,
            //maps_not_loaded,
            //outside_map_dimensions,
            //next_pt_not_found,
            no_data,
            invalid_front_pair_number
            //no_supermodes_found
        }

        public struct point_type
        {
            public int row;
            public int col;
            public double real_row;
            public double real_col;
            public short front_pair_number;
            public double I_rear;
            public double I_front_1;
            public double I_front_2;
            public double I_front_3;
            public double I_front_4;
            public double I_front_5;
            public double I_front_6;
            public double I_front_7;
            public double I_front_8;
            public double I_phase;
        }
        public CDSDBRSupermodeMapLine()
        {
            clear();
        }
        // clear all contents
        public void clear()
        {
            _points.Clear();
        }

        public bool hasChangeOfFrontPairs()
        {
            for (int i = 0; i < _points.Count; i++)
            {
                short first_pair_num = _points[0].front_pair_number;
                short pair_num = _points[i].front_pair_number;

                if (pair_num != first_pair_num)
                    return true;
            }

            return false;
        }
        
        public rcode writeRowColToFile(string abs_filepath, bool overwrite)
        {
            rcode rval = rcode.ok;
            int row_count = 0;
            int col_count = 0;
            if (_points.Count == 0)
            {
                rval = rcode.no_data;
                return rval;
            }
            // Check if file already exists and overwrite not specified
            if (File.Exists(abs_filepath) && !overwrite)
            {
                rval = rcode.file_already_exists;
                return rval;
            }
            // Ensure the directory exists
            if (abs_filepath.LastIndexOf('\\')==-1)
            {
                // '\\' not found => not an absolute path
                rval = rcode.could_not_open_file;
                return rval;
            }
            try
            {
                using (StreamWriter sr = new StreamWriter(abs_filepath))
                {
                    for (int i = 0; i < _points.Count; i++)
                    {
                        sr.WriteLine("{0}{1}{2}", _points[i].row, Defaults.COMMA_CHAR, _points[i].col);
                    }
                    sr.Close();
                }
            }
            catch (Exception)
            {
                rval = rcode.could_not_open_file;
                //throw;
            }
            
            return rval;
        }


        public rcode writeCurrentsToFile(string abs_filepath, bool overwrite)
        {
            rcode rval = rcode.ok;
            int row_count = 0;
            int col_count = 0;

            if (_points.Count == 0)
            {
                rval = rcode.no_data;
                return rval;
            }

            // Check if file already exists and overwrite not specified
            if (File.Exists(abs_filepath) && !overwrite)
            {
                rval = rcode.file_already_exists;
                return rval;
            }

            // Ensure the directory exists
            int index_of_last_dirslash = abs_filepath.LastIndexOf('\\');
            if (index_of_last_dirslash == -1)
            {
                // '\\' not found => not an absolute path
                rval = rcode.could_not_open_file;
                return rval;
            }

            try
            {
                using (StreamWriter file_stream = new StreamWriter(abs_filepath))
                {
                    // First write header row
                    file_stream.Write("Rear Current [mA]");
                    file_stream.Write(Defaults.COMMA_CHAR);
                    file_stream.Write("Front Pair Number");
                    file_stream.Write(Defaults.COMMA_CHAR);
                    file_stream.Write("Constant Front Current [mA]");
                    file_stream.Write(Defaults.COMMA_CHAR);
                    file_stream.Write("Non-constant Front Current [mA]");
                    file_stream.Write(Defaults.COMMA_CHAR);
                    file_stream.Write("Phase Current [mA]");
                    file_stream.Write(Defaults.COMMA_CHAR);
                    file_stream.Write("Row Index");
                    file_stream.Write(Defaults.COMMA_CHAR);
                    file_stream.Write("Column Index");
                    file_stream.WriteLine();

                    for (int i = 0; i < (int)(_points.Count); i++)
                    {
                        // First write data rows
                        file_stream.Write(_points[i].I_rear);
                        file_stream.Write(Defaults.COMMA_CHAR);
                        file_stream.Write(_points[i].front_pair_number);
                        file_stream.Write(Defaults.COMMA_CHAR);

                        switch (_points[i].front_pair_number)
                        {
                            case 1:
                                file_stream.Write(_points[i].I_front_1);
                                file_stream.Write(Defaults.COMMA_CHAR);
                                file_stream.Write(_points[i].I_front_2);
                                break;
                            case 2:
                                file_stream.Write(_points[i].I_front_2);
                                file_stream.Write(Defaults.COMMA_CHAR);
                                file_stream.Write(_points[i].I_front_3);
                                break;
                            case 3:
                                file_stream.Write(_points[i].I_front_3);
                                file_stream.Write(Defaults.COMMA_CHAR);
                                file_stream.Write(_points[i].I_front_4);
                                break;
                            case 4:
                                file_stream.Write(_points[i].I_front_4);
                                file_stream.Write(Defaults.COMMA_CHAR);
                                file_stream.Write(_points[i].I_front_5);
                                break;
                            case 5:
                                file_stream.Write(_points[i].I_front_5);
                                file_stream.Write(Defaults.COMMA_CHAR);
                                file_stream.Write(_points[i].I_front_6);
                                break;
                            case 6:
                                file_stream.Write(_points[i].I_front_6);
                                file_stream.Write(Defaults.COMMA_CHAR);
                                file_stream.Write(_points[i].I_front_7);
                                break;
                            case 7:
                                file_stream.Write(_points[i].I_front_7);
                                file_stream.Write(Defaults.COMMA_CHAR);
                                file_stream.Write(_points[i].I_front_8);
                                break;
                        }

                        file_stream.Write(Defaults.COMMA_CHAR);
                        file_stream.Write(_points[i].I_phase);
                        file_stream.Write(Defaults.COMMA_CHAR);
                        file_stream.Write(_points[i].real_row);
                        file_stream.Write(Defaults.COMMA_CHAR);
                        file_stream.Write(_points[i].real_col);

                        file_stream.WriteLine();
                    }
                }
            }
            catch (Exception ex)
            {
                rval = rcode.could_not_open_file;
            }

            return rval;
        }


        public List<double> getRows()
        {
            List<double> rows = new List<double>();

            for (int i = 0; i < _points.Count; i++)
            {
                rows.Add(_points[i].row);
            }

            return rows;
        }
        public List<double> getCols()
        {
            List<double> cols = new List<double>();

            for (int i = 0; i < _points.Count; i++)
            {
                cols.Add(_points[i].col);
            }

            return cols;
        }


        public List<point_type> _points = new List<point_type>();

        
    }

}

using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.Algorithms
{
    class HysteresisAnalysis
    {
        private Log CGR_LOG = Log.GetInstance;
        public HysteresisAnalysis()
        {
            init();
        }
        public HysteresisAnalysis(ModeAnalysis forwardMode, ModeAnalysis reverseMode,
                                int xAxisLength, int yAxisLength)
        {
            CGR_LOG.Write("HysteresisAnalysis::HysteresisAnalysis() entered", LoggingLevels.DIAGNOSTIC_CGR_LOG);

            _forwardMode = forwardMode;
            _reverseMode = reverseMode;

            _xAxisLength = xAxisLength;
            _yAxisLength = yAxisLength;

            init();

            String log_msg = "HysteresisAnalysis::HysteresisAnalysis() : ";
            log_msg += string.Format("created _forwardMode._lowerBoundaryLine._x.Count={0}, _forwardMode._lowerBoundaryLine._y.Count={1}",
                _forwardMode._lowerBoundaryLine._x.Count, _forwardMode._lowerBoundaryLine._y.Count);
            CGR_LOG.Write(log_msg, LoggingLevels.DIAGNOSTIC_CGR_LOG);

            log_msg = "HysteresisAnalysis::HysteresisAnalysis() : ";
            log_msg += string.Format("created _forwardMode._upperBoundaryLine._x.Count={0}, _forwardMode._upperBoundaryLine._y.Count={1}",
                _forwardMode._upperBoundaryLine._x.Count, _forwardMode._upperBoundaryLine._y.Count);
            CGR_LOG.Write(log_msg, LoggingLevels.DIAGNOSTIC_CGR_LOG);

            log_msg = "HysteresisAnalysis::HysteresisAnalysis() : ";
            log_msg += string.Format("created _reverseMode._lowerBoundaryLine._x.Count={0}, _reverseMode._lowerBoundaryLine._y.Count={1}",
                _reverseMode._lowerBoundaryLine._x.Count, _reverseMode._lowerBoundaryLine._y.Count);
            CGR_LOG.Write(log_msg, LoggingLevels.DIAGNOSTIC_CGR_LOG);

            log_msg = "HysteresisAnalysis::HysteresisAnalysis() : ";
            log_msg += string.Format("created _reverseMode._upperBoundaryLine._x.Count={0}, _reverseMode._upperBoundaryLine._y.Count={1}",
                _reverseMode._upperBoundaryLine._x.Count, _reverseMode._upperBoundaryLine._y.Count);
            CGR_LOG.Write(log_msg, LoggingLevels.DIAGNOSTIC_CGR_LOG);

            log_msg = "HysteresisAnalysis::HysteresisAnalysis() : ";
            log_msg += string.Format("created  _xAxisLength = {0}, _yAxisLength = {1}",
                _xAxisLength, _yAxisLength);
            CGR_LOG.Write(log_msg, LoggingLevels.DIAGNOSTIC_CGR_LOG);

            CGR_LOG.Write("HysteresisAnalysis::HysteresisAnalysis() exiting", LoggingLevels.DIAGNOSTIC_CGR_LOG);
        }

        #region Variants

        public enum rcode
        {
            ok = 0
        }

        //	private attributes
        //
        ModeAnalysis _forwardMode = new ModeAnalysis();
        ModeAnalysis _reverseMode = new ModeAnalysis();

        // formed by the forward modes' upper line
        // and the reverse modes' lower line
        ModeAnalysis _innerHysteresisMode = new ModeAnalysis();
        // formed by the reverse modes' upper line
        // and the forward modes' lower line
        ModeAnalysis _outerHysteresisMode = new ModeAnalysis();


        int _xAxisLength;
        int _yAxisLength;

        //GDM 31/10/06 add dec to retrieve Qa thresholds
        ///////////////////////////////////////////////////////
        //	Thresholds
        //
        //QAThresholds* _qaThresholds;
        QAThresholds _qaThresholds = new QAThresholds();
        //
        //////////////////////////////////////////////////////
        //	Data
        //
        ////////////////////////////////////////////
        //
        Vector _stableAreaWidths = new Vector();
        double _maxStableAreaWidth;
        double _minStableAreaWidth;
        double _meanStableAreaWidths;
        double _sumStableAreaWidths;
        double _numStableAreaWidthSamples;
        double _stdDevStableAreaWidths;
        double _varStableAreaWidths;

        ////////////////////////////////////////////

        Vector _totalWidths = new Vector();

        ////////////////////////////////////////////

        Vector _percWorkingRegions = new Vector();
        double _maxPercWorkingRegion;
        double _minPercWorkingRegion;
        double _meanPercWorkingRegions;
        double _sumPercWorkingRegions;
        double _numPercWorkingRegionSamples;
        double _stdDevPercWorkingRegions;
        double _varPercWorkingRegions;

        ////////////////////////////////////////////

        #endregion

        internal void clear()
        {
            _forwardMode.init();
            _reverseMode.init();
            _innerHysteresisMode.init();
            _outerHysteresisMode.init();

            _xAxisLength = 0;
            _yAxisLength = 0;

            _stableAreaWidths.Clear();
            _totalWidths.Clear();
            _percWorkingRegions.Clear();

            _maxStableAreaWidth = 0;
            _minStableAreaWidth = 0;
            _meanStableAreaWidths = 0;
            _sumStableAreaWidths = 0;
            _numStableAreaWidthSamples = 0;
            _stdDevStableAreaWidths = 0;
            _varStableAreaWidths = 0;


            _maxPercWorkingRegion = 0;
            _minPercWorkingRegion = 0;
            _meanPercWorkingRegions = 0;
            _sumPercWorkingRegions = 0;
            _numPercWorkingRegionSamples = 0;
            _stdDevPercWorkingRegions = 0;
            _varPercWorkingRegions = 0;

        }

        internal void init()
        {
            /////////////////////////////
            // initialise attributes
            // before use

            //////////////////////////////////////////////////

            _stableAreaWidths.Clear();

            _maxStableAreaWidth = 0;
            _minStableAreaWidth = 0;
            _meanStableAreaWidths = 0;
            _sumStableAreaWidths = 0;
            _numStableAreaWidthSamples = 0;
            _stdDevStableAreaWidths = 0;
            _varStableAreaWidths = 0;

            //////////////////////////////////////////////////

            // create inner hysteresis mode

            _innerHysteresisMode.loadBoundaryLines(0, _xAxisLength, _yAxisLength, _forwardMode._upperBoundaryLine, _reverseMode._lowerBoundaryLine);

            //////////////////////////////////////////////////

            // create outer hysteresis mode
            CGR_LOG.Write("HysteresisAnalysis::init() calling _outerHysteresisMode.loadBoundaryLines ...", LoggingLevels.DIAGNOSTIC_CGR_LOG);

            _outerHysteresisMode.loadBoundaryLines(0, _xAxisLength, _yAxisLength, _reverseMode._upperBoundaryLine, _forwardMode._lowerBoundaryLine);

            //////////////////////////////////////////////////
        }

        internal void setQAThresholds(QAThresholds qaThresholds)//(QAThresholds* qaThresholds)
        {
            _qaThresholds = qaThresholds;
        }

        
        internal rcode runAnalysis()
        {
            rcode err = rcode.ok;

            if (err == rcode.ok)
                err = runStableAreaWidthsAnalysis();
            if (err == rcode.ok)
                runPercWorkingRegionAnalysis();

            return err;
        }

        /////////////////////////////////////////////////////////////////////
        //
        internal rcode runStableAreaWidthsAnalysis()
        {
            rcode err = rcode.ok;

            Vector stableAreaWidths;
            getStableAreaWidths(out stableAreaWidths);

            calcMaxStableAreaWidths();
            calcMinStableAreaWidths();
            calcMeanStableAreaWidths();
            calcSumStableAreaWidths();
            calcNumStableAreaWidthSamples();
            calcStdDevStableAreaWidths();
            calcVarStableAreaWidths();

            return err;
        }


        ///////////////////////////////////////////////////////////////////////

        internal rcode runPercWorkingRegionAnalysis()
        {
            rcode err = rcode.ok;

            Vector percWorkingRegions;
            getPercWorkingRegions(out percWorkingRegions);

            calcMaxPercWorkingRegions();
            calcMinPercWorkingRegions();
            calcMeanPercWorkingRegions();
            calcSumPercWorkingRegions();
            calcNumPercWorkingRegionSamples();
            calcStdDevPercWorkingRegions();
            calcVarPercWorkingRegions();

            return err;
        }

        /////////////////////////////////////////////////////////////////////
        //
        internal void getStableAreaWidths(out Vector stableAreaWidths)
        {

            short startIndex = 0;
            short stopIndex;

            if (_stableAreaWidths.Count == 0)
                //GDM 31/10/06 change of call to getModeWidths to include start and stop index's
                if (_qaThresholds._SMMapQA == 1)
                {
                    startIndex = (short)_qaThresholds._hysteresisAnalysisMinX;
                    stopIndex = (short)_qaThresholds._hysteresisAnalysisMaxX;
                    _innerHysteresisMode.getModeWidths(out _stableAreaWidths, startIndex, stopIndex);
                }
            if (_qaThresholds._SMMapQA == 0)
            {
                //old call
                if (_innerHysteresisMode._lowerBoundaryLine.numXPoints > _outerHysteresisMode._lowerBoundaryLine.numXPoints)
                    startIndex = (short)((short)_outerHysteresisMode._lowerBoundaryLine.x(0) - (short)_innerHysteresisMode._lowerBoundaryLine.x(0));

                _innerHysteresisMode.getModeWidths(out _stableAreaWidths, startIndex);
            }
            stableAreaWidths = _stableAreaWidths;
        }


        internal double getStableAreaWidthsStatistic(Vector.VectorAttribute attrib)
        {
            double returnValue = 0;

            Vector stableAreaWidths;
            getStableAreaWidths(out stableAreaWidths);

            returnValue = stableAreaWidths.GetVectorAttribute(attrib);

            return returnValue;
        }

        //
        ////////////////////////////////////////////////////////////////////////////////////////////
        //
        void calcMaxStableAreaWidths()
        {
            Vector stableAreaWidths;
            getStableAreaWidths(out stableAreaWidths);

            _maxStableAreaWidth = stableAreaWidths.GetVectorAttribute(Vector.VectorAttribute.max);
        }

        void calcMinStableAreaWidths()
        {
            Vector stableAreaWidths;
            getStableAreaWidths(out stableAreaWidths);

            _minStableAreaWidth = stableAreaWidths.GetVectorAttribute(Vector.VectorAttribute.min);
        }


        void calcMeanStableAreaWidths()
        {
            Vector stableAreaWidths;
            getStableAreaWidths(out stableAreaWidths);

            _meanStableAreaWidths = stableAreaWidths.GetVectorAttribute(Vector.VectorAttribute.mean);
        }


        void calcSumStableAreaWidths()
        {
            Vector stableAreaWidths;
            getStableAreaWidths(out stableAreaWidths);

            _sumStableAreaWidths = stableAreaWidths.GetVectorAttribute(Vector.VectorAttribute.sum);
        }


        void calcNumStableAreaWidthSamples()
        {
            Vector stableAreaWidths;
            getStableAreaWidths(out stableAreaWidths);

            _numStableAreaWidthSamples = stableAreaWidths.GetVectorAttribute(Vector.VectorAttribute.count);
        }


        void calcVarStableAreaWidths()
        {
            Vector stableAreaWidths;
            getStableAreaWidths(out stableAreaWidths);

            _varStableAreaWidths = stableAreaWidths.GetVectorAttribute(Vector.VectorAttribute.variance);
        }


        void calcStdDevStableAreaWidths()
        {
            Vector stableAreaWidths;
            getStableAreaWidths(out stableAreaWidths);

            _stdDevStableAreaWidths = stableAreaWidths.GetVectorAttribute(Vector.VectorAttribute.stdDev);
        }

        //
        /////////////////////////////////////////////////////////////////////
        //
        internal void getTotalWidths(out Vector totalWidths)
        {
            short startIndex = 0;
            short stopIndex;

            if (_totalWidths.Count == 0)
                if (_qaThresholds._SMMapQA == 1)
                {
                    //GDM 31/10/06 change in call to getmodewidths to permit start and stop index
                    startIndex = (short)_qaThresholds._hysteresisAnalysisMinX;
                    stopIndex = (short)_qaThresholds._hysteresisAnalysisMaxX;

                    _outerHysteresisMode.getModeWidths(out _totalWidths, startIndex, stopIndex);
                }
            if (_qaThresholds._SMMapQA == 0)
            {
                //old call  
                _outerHysteresisMode.getModeWidths(out _totalWidths);
            }
            totalWidths = _totalWidths;
        }

        //
        /////////////////////////////////////////////////////////////////////
        //
        internal void getPercWorkingRegions(out Vector percWorkingRegions)
        {
            if (_percWorkingRegions.Count == 0)
            {
                Vector totalWidths;
                getTotalWidths(out totalWidths);

                Vector stableAreaWidths;
                getStableAreaWidths(out stableAreaWidths);

                for (int i = 0; i < _stableAreaWidths.Count; i++)
                {
                    double percWorkingRegion = 0;

                    double stableAreaWidth = stableAreaWidths[i];
                    double totalWidth = totalWidths[i];

                    if (stableAreaWidth < totalWidth)
                        percWorkingRegion = 100 * (stableAreaWidth / totalWidth);
                    else
                        percWorkingRegion = 100;

                    _percWorkingRegions.Add(percWorkingRegion);
                }
            }

            percWorkingRegions = _percWorkingRegions;
        }

        internal double getPercWorkingRegionsStatistic(Vector.VectorAttribute attrib)
        {
            double returnValue = 0;

            Vector percWorkingRegions;
            getPercWorkingRegions(out percWorkingRegions);

            returnValue = percWorkingRegions.GetVectorAttribute(attrib);

            return returnValue;
        }


        //
        ////////////////////////////////////////////////////////////////////////////////////////////
        //
        void calcMaxPercWorkingRegions()
        {
            Vector percWorkingRegions;
            getPercWorkingRegions(out percWorkingRegions);

            _maxPercWorkingRegion = percWorkingRegions.GetVectorAttribute(Vector.VectorAttribute.max);
        }


        void calcMinPercWorkingRegions()
        {
            Vector percWorkingRegions;
            getPercWorkingRegions(out percWorkingRegions);

            _minPercWorkingRegion = percWorkingRegions.GetVectorAttribute(Vector.VectorAttribute.min);
        }


        void calcMeanPercWorkingRegions()
        {
            Vector percWorkingRegions;
            getPercWorkingRegions(out percWorkingRegions);

            _meanPercWorkingRegions = percWorkingRegions.GetVectorAttribute(Vector.VectorAttribute.mean);
        }


        void calcSumPercWorkingRegions()
        {
            Vector percWorkingRegions;
            getPercWorkingRegions(out percWorkingRegions);

            _sumPercWorkingRegions = percWorkingRegions.GetVectorAttribute(Vector.VectorAttribute.sum);
        }


        void calcNumPercWorkingRegionSamples()
        {
            Vector percWorkingRegions;
            getPercWorkingRegions(out percWorkingRegions);

            _numPercWorkingRegionSamples = percWorkingRegions.GetVectorAttribute(Vector.VectorAttribute.count);
        }


        void calcVarPercWorkingRegions()
        {
            Vector percWorkingRegions;
            getPercWorkingRegions(out percWorkingRegions);

            _varPercWorkingRegions = percWorkingRegions.GetVectorAttribute(Vector.VectorAttribute.variance);
        }


        void calcStdDevPercWorkingRegions()
        {
            Vector percWorkingRegions;
            getPercWorkingRegions(out percWorkingRegions);

            _stdDevPercWorkingRegions = percWorkingRegions.GetVectorAttribute(Vector.VectorAttribute.stdDev);
        }

    }
}

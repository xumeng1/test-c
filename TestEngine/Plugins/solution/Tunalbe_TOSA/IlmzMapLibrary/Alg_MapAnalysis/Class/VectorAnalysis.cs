using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.Algorithms
{
    class VectorAnalysis
    {
        public const double M_PI = 3.14159265358979323846;
        public const double M_PI_2 = 1.57079632679489661923;

        public static double derivative(
                double x1, double x2,
                double y1, double y2)
        {
            return (y2 - y1) / (x2 - x1);
        }

        public static double doubleDerivative(
                double x1, double x2, double x3,
                double y1, double y2, double y3)
        {
            return (derivative(x2, x3, y2, y3) - derivative(x1, x2, y1, y2)) / (x2 - x1);
        }

        /// <summary>
        /// The median filter is well documented in edge detection as
        /// being suited to removing noise while preserving edges
        /// whereas traditionally-used low pass filters effect the
        /// edge so as to lower its slope.
        /// This function is for a map where the filter is to be
        /// applied to each ramp line in isolation from the next
        /// Hence line_length tells us the length of each line
        /// If the input is x[i] where i = 0, n-1
        /// and the output is y[i] where i = 0, n-1
        /// y[i] = median( x[i-r], ... x[i], ... x[i+r] )
        /// where r is the rank of the filter
        /// elements of x outside the range are treated as zero
        /// </summary>
        /// <param name="x_in"></param>
        /// <param name="filtered_x_out"></param>
        /// <param name="rank"></param>
        /// <param name="line_length"></param>
        public static void medianFilter(
            List<double> x_in, List<double> filtered_x_out,
            int rank, int line_length)
        {
            filtered_x_out.Clear();

            if (line_length > 0
                && x_in.Count > 0
                && x_in.Count % line_length == 0)
            {
                // input data looks okay
                List<double> subset_of_x = new List<double>();
                int point_count = 0;
                foreach (double i_x_in in x_in)
                {
                    // Iterate through each line in the map
                    int point_in_line_count = point_count % line_length;

                    // pick vector +/-rank about each sample 
                    subset_of_x.Clear();
                    for (int j = point_in_line_count - rank;
                            j <= point_in_line_count + rank;
                            j++)
                    {
                        if (j < 0 || j >= line_length)
                        { // outside bounds of current line, use current point as value
                            //subset_of_x.Add(x_in[point_count]);
                            //outside bounds of current line, use mirror image data as value to remove the abnormal PR at point_count=0 or point_count=147.jack.zhang 2011-11-09
                            subset_of_x.Add(x_in[point_count + (point_in_line_count - j)]);
                        }
                        else
                        { // within bounds of current line
                            subset_of_x.Add(x_in[point_count - (point_in_line_count - j)]);
                        }
                    }
                    // The median of the sorted vector s[i] where i = 0, n-1 is defined as
                    // s[(n-1)/2] where n is odd 
                    // ( s[(n/2)-1] + s[n/2] ) / 2 where n is even
                    // n is always odd in a median filter

                    // Calculate the median for the subset of x and add it to the output
                    //std.sort(subset_of_x.begin(),subset_of_x.end());
                    subset_of_x.Sort();
                    filtered_x_out.Add( subset_of_x[rank] );

                    point_count++;
                }
            }
            else
            { // input data is bad, see if() statement above
            }

        }

        public static void linearFit(
            List<double> x_in, List<double> y_in,
            out double slope, out double intercept)
        {
            // least squares linear regression
            // Given equal length vectors x and y
            // x is assumed noiseless and
            // y is to have normally distributed noise
            // A linear approximation is fitted to the data
            // y = slope*x + intercept
            slope = 0;
            intercept = 0;

            int N = x_in.Count;
            if (N > 1 && N == y_in.Count)
            { // data looks okay

                double mean_x = meanOfValues(x_in);
                double mean_y = meanOfValues(y_in);

                double S_xx = 0;
                double S_xy = 0;
                for (int i = 0; i < N; i++)
                {
                    S_xx += (x_in[i] - mean_x) * (x_in[i] - mean_x);
                    S_xy += (x_in[i] - mean_x) * (y_in[i] - mean_y);
                }

                slope = S_xy / S_xx;
                intercept = mean_y - slope * mean_x;
            }
            else
            { // data is no good
            }

        }

        public static void distancesFromPointsToLine(
            List<double> x_in, List<double> y_in,
            double slope, double intercept,
            List<double> distances)
        {
            // Calculate the distance from each point to a line
            distances.Clear();
            int N = x_in.Count;
            if (N > 1 && N == y_in.Count)// data looks okay
            {
                // find slope of perdendicular line through point
                double slope_p = Math.Tan(Math.Atan(slope) - M_PI_2);
                for (int i = 0; i < N; i++ )
                {
                    if (slope == 0)
                    { // line is parallel to x-axis
                        distances.Add(y_in[i] - intercept);
                    }
                    else
                    {
                        // find intercept of perdendicular line through each point
                        double intercept_p = y_in[i] - slope_p * x_in[i];

                        // find intersection of perdendicular lines
                        double x_on_line = (intercept - intercept_p) / (slope_p - slope);
                        double y_on_line = slope * x_on_line + intercept;
                        double distance = Math.Sqrt(
                            (x_in[i] - x_on_line) * (x_in[i] - x_on_line)
                          + (y_in[i] - y_on_line) * (y_in[i] - y_on_line));
                        distances.Add(distance);
                    }
                }
            }
        }

        public static double meanOfValues(List<double> values)
        {
            double mean = 0;
            int count = values.Count;
            foreach (double var in values)
            {
                mean += var;
            }
            if (count > 1)
            {
                mean /= count;
            }

            return mean;
        }


    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.Algorithms
{
    public class PassFailAnalysis
    {
        public PassFailAnalysis()
        {
            init();
        }
        // initialise object
        // and get settings from 
        // the registry using
        // getConfigInfo
        public void init()
        {
            threshold = 0;
            failedModes.Clear();
            valuesWhichFailed.Clear();
            pass = true;
            numFailedModes = 0;
            minValue = 0;
            maxValue = 0;
        }

        /////////////////////////////////////////////
        ///	Functionality
        //
        public void runPassFailWithUpperLimit(List<double> values)
        {
            List<bool> ignore_none = new List<bool>();

            for (int i = 0; i < values.Count; i++)
            {
                ignore_none.Add(false);
            }

            runPassFailWithUpperLimit(values, ignore_none);
        }

        public void runPassFailWithLowerLimit(List<double> values)
        {
            List<bool> ignore_none = new List<bool>();

            for (int i = 0; i < values.Count; i++)
            {
                ignore_none.Add(false);
            }

            runPassFailWithLowerLimit(values, ignore_none);
        }

        public void runPassFailWithUpperLimit(List<double> values, List<bool> ignore)
        {
            failedModes.Clear();
            valuesWhichFailed.Clear();
            pass = true;
            numFailedModes = 0;
            minValue = 0;
            maxValue = 0;

            double value;

            if (values.Count > 0 && values.Count == ignore.Count)
            {
                // set the initial min and max values
                // and the pass boolean
                minValue = values[0];
                maxValue = values[0];
                pass = true;

                for (int i = 0; i < values.Count; i++)
                {
                    value = values[i];

                    // work out min and max
                    if (value > maxValue)
                    {
                        maxValue = value;
                    }
                    else if (value < minValue)
                    {
                        minValue = value;
                    }

                    // if value is less than the
                    // threshold/upper limit then it passes
                    //
                    if (value <= threshold || ignore[i])
                    {
                        // value passed
                    }
                    else
                    {
                        pass = false;
                        failedModes.Add(i);
                        numFailedModes++;
                        valuesWhichFailed.Add(value);
                    }
                }
            }
            else
            {
                // error - no values to work with
            }
        }


        public void runPassFailWithLowerLimit(List<double> values, List<bool> ignore)
        {
            failedModes.Clear();
            valuesWhichFailed.Clear();
            pass = true;
            numFailedModes = 0;
            minValue = 0;
            maxValue = 0;

            double value;

            if (values.Count > 0 && values.Count == ignore.Count)
            {

                // set the initial min and max values
                // and the pass boolean
                minValue = values[0];
                maxValue = values[0];
                pass = true;

                for (int i = 0; i < values.Count; i++)
                {
                    value = values[i];
                    // work out min and max
                    if (value > maxValue)
                    {
                        maxValue = value;
                    }
                    else if (value < minValue)
                    {
                        minValue = value;
                    }


                    // if value is greater than the
                    // threshold/lower limit then it passes
                    //
                    if (value >= threshold || ignore[i])
                    {
                        // value passed
                    }
                    else
                    {
                        pass = false;
                        failedModes.Add(i);
                        numFailedModes++;
                        valuesWhichFailed.Add(value);
                    }
                }
            }
            else
            {
                // error - no values to work with
            }
        }


        public void runPassFailWithUpperLimit(double value)
        {
            List<double> values = new List<double>();
            List<bool> ignore_none = new List<bool>();

            values.Add(value);
            ignore_none.Add(false);

            runPassFailWithUpperLimit(values, ignore_none);
        }

        public void runPassFailWithLowerLimit(double value)
        {
            List<double> values = new List<double>();
            List<bool> ignore_none = new List<bool>();

            values.Add(value);
            ignore_none.Add(false);

            runPassFailWithLowerLimit(values, ignore_none);
        }

        //
        /////////////////////////////////////////////

        //
        ///	the results of pass fail analysis
        //
        // threshold to compare
        // results with
        public double threshold;

        // Pass or Fail
        public bool pass;

        // array of Modes which failed
        public List<double> failedModes = new List<double>();

        // no. of LMs which failed
        public int numFailedModes;

        // values which failed
        public List<double> valuesWhichFailed = new List<double>();

        // smallest value measured
        public double minValue;

        // largest value measured
        public double maxValue;

       

    }
}

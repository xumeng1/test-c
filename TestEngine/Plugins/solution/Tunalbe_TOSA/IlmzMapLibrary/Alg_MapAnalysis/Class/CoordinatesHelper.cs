//************************
//CoordinatesHelper
//Jim Liu
//2011-05-21
//************************
using System;
using System.Collections.Generic;
using System.Text;

namespace MatrixHelper
{
    public sealed class CoordinatesHelper
    {

        #region get line
        /// <summary>
        /// get line via a point and a slope
        /// </summary>
        /// <param name="point0"></param>
        /// <param name="slope"></param>
        /// <returns></returns>
        public static EquationOfLine GetEquationOfLine(CPoint point0, double slope)
        {
            return GetEquationOfLine(point0.X, point0.Y, slope);
        }

        /// <summary>
        /// get line via a point and a slope
        /// </summary>
        /// <param name="x0"></param>
        /// <param name="y0"></param>
        /// <param name="slope"></param>
        /// <returns></returns>
        public static EquationOfLine GetEquationOfLine(double x0, double y0, double slope)
        {
            EquationOfLine eol;
            eol.A = slope;
            eol.B = -1;
            eol.C = y0 - slope * x0;
            return eol;
        }

        /// <summary>
        /// get line via two points
        /// </summary>
        /// <param name="point1"></param>
        /// <param name="point2"></param>
        /// <returns></returns>
        public static EquationOfLine GetEquationOfLine(CPoint point1, CPoint point2)
        {
            return GetEquationOfLine(point1.X, point1.Y, point2.X, point2.Y);
        }

        /// <summary>
        /// get line via two points
        /// </summary>
        /// <param name="x1"></param>
        /// <param name="y1"></param>
        /// <param name="x2"></param>
        /// <param name="y2"></param>
        /// <returns></returns>
        public static EquationOfLine GetEquationOfLine(double x1, double y1, double x2, double y2)
        {
            if (x1 == x2 && y1 == y2)
            {
                throw new CoordinatesException("Same points could not get the EquationOfLine.");
            }
            else if (x1 == x2)
            {
                throw new CoordinatesException("x1 == x2 could not get the EquationOfLine.");
            }
            double slope = (y2 - y1) / (x2 - x1);
            return GetEquationOfLine(x1, y1, slope);
        }

        /// <summary>
        /// get line via a yIntercept and a slope
        /// </summary>
        /// <param name="yIntercept"></param>
        /// <param name="slope"></param>
        /// <returns></returns>
        public static EquationOfLine GetEquationOfLineYIntercept(double yIntercept, double slope)
        {
            return GetEquationOfLine(0, yIntercept, slope);
        }

        /// <summary>
        /// get line via a xIntercept and a slope
        /// </summary>
        /// <param name="xIntercept"></param>
        /// <param name="slope"></param>
        /// <returns></returns>
        public static EquationOfLine GetEquationOfLineXIntercept(double xIntercept, double slope)
        {
            return GetEquationOfLine(xIntercept, 0, slope);
        }
        #endregion

        #region get distance
        /// <summary>
        /// get distance between two points
        /// </summary>
        /// <param name="point1"></param>
        /// <param name="point2"></param>
        /// <returns></returns>
        public static double GetDistanceBetweenTwoPoints(CPoint point1, CPoint point2)
        {
            return GetDistanceBetweenTwoPoints(point1.X, point1.Y, point2.X, point2.Y);
        }

        /// <summary>
        /// get distance between two points
        /// </summary>
        /// <param name="x1"></param>
        /// <param name="y1"></param>
        /// <param name="x2"></param>
        /// <param name="y2"></param>
        /// <returns></returns>
        public static double GetDistanceBetweenTwoPoints(double x1, double y1, double x2, double y2)
        {
            double distance = Math.Pow(x2 - x1, 2) + Math.Pow(y2 - y1, 2);
            distance = Math.Pow(distance, 0.5);
            return distance;
        }

        /// <summary>
        /// get distance between a points and a line
        /// </summary>
        /// <param name="point"></param>
        /// <param name="eol"></param>
        /// <returns></returns>
        public static double GetDistanceBetweenPointToLine(CPoint point, EquationOfLine eol)
        {
            return GetDistanceBetweenPointToLine(point.X, point.Y, eol);
        }

        /// <summary>
        /// get distance between a points and a line
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="eol"></param>
        /// <returns></returns>
        public static double GetDistanceBetweenPointToLine(double x, double y, EquationOfLine eol)
        {
            return Math.Abs(x * eol.A + y * eol.B + eol.C) / Math.Pow(Math.Pow(eol.A, 2) + Math.Pow(eol.B, 2), 0.5);
        }

        /// <summary>
        /// get distance between two points, alone a slope
        /// </summary>
        /// <param name="point1"></param>
        /// <param name="point2"></param>
        /// <param name="slope"></param>
        /// <returns></returns>
        public static double GetDistanceBetweenTwoPointsAlongSlope(CPoint point1, CPoint point2, double slope)
        {
            return GetDistanceBetweenTwoPointsAlongSlope(point1.X, point1.Y, point2.X, point2.Y, slope);
        }

        /// <summary>
        /// get distance between two points, alone a slope
        /// </summary>
        /// <param name="x1"></param>
        /// <param name="y1"></param>
        /// <param name="x2"></param>
        /// <param name="y2"></param>
        /// <param name="slope"></param>
        /// <returns></returns>
        public static double GetDistanceBetweenTwoPointsAlongSlope(double x1, double y1, double x2, double y2, double slope)
        {
            EquationOfLine eol1 = GetEquationOfLine(x1, y1, slope);
            double distance1 = GetDistanceBetweenPointToLine(x2, y2, eol1);
            double distance2 = GetDistanceBetweenTwoPoints(x1, y1, x2, y2);
            return Math.Pow(Math.Pow(distance2, 2) - Math.Pow(distance1, 2), 0.5);
        }
        #endregion

        #region get point
        /// <summary>
        /// get intersection point of two lines
        /// </summary>
        /// <param name="eol1"></param>
        /// <param name="eol2"></param>
        /// <returns></returns>
        public static CPoint GetIntersectionPointOfTwoLine(EquationOfLine eol1, EquationOfLine eol2)
        {
            CPoint point;
            point.X = (eol1.B * eol2.C - eol2.B * eol1.C) / (eol1.A * eol2.B - eol2.A * eol1.B);
            point.Y = (eol1.C * eol2.A - eol2.C * eol1.A) / (eol1.A * eol2.B - eol2.A * eol1.B);
            return point;
        }

        /// <summary>
        /// get vertical intersection point of a point to a line 
        /// </summary>
        /// <param name="point"></param>
        /// <param name="eol"></param>
        /// <returns></returns>
        public static CPoint GetIntersectionPointOfPointToLine(CPoint point, EquationOfLine eol)
        {
            return GetIntersectionPointOfPointToLine(point.X, point.Y, eol);
        }

        /// <summary>
        /// get vertical intersection point of a point to a line 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="eol"></param>
        /// <returns></returns>
        public static CPoint GetIntersectionPointOfPointToLine(double x, double y, EquationOfLine eol)
        {
            EquationOfLine eol1 = GetEquationOfLine(x, y, -1d / eol.A);
            return GetIntersectionPointOfTwoLine(eol, eol1);
        }
        #endregion

        #region get x or y
        public static double GetX(double y, EquationOfLine eol)
        {
            return -1d * (eol.C + eol.B * y) / eol.A;
        }
        public static double GetY(double x, EquationOfLine eol)
        {
            return -1d * (eol.C + eol.A * x) / eol.B;
        }
        #endregion
    }

    /// <summary>
    /// EquationOfLine
    /// </summary>
    public struct EquationOfLine
    {
        public double A;
        public double B;
        public double C;
        public EquationOfLine(double a, double b, double c)
        {
            A = a;
            B = b;
            C = c;
        }

    }

    /// <summary>
    /// Point
    /// </summary>
    public struct CPoint
    {
        public double X;
        public double Y;
        public CPoint(double x, double y)
        {
            X = x;
            Y = y;
        }
    }

    /// <summary>
    /// CoordinatesException
    /// </summary>
    public sealed class CoordinatesException : Exception
    {
        public CoordinatesException(string msg)
            : base(msg)
        { }
        public CoordinatesException(string msg, Exception ex)
            : base(msg, ex)
        { }
    }

}

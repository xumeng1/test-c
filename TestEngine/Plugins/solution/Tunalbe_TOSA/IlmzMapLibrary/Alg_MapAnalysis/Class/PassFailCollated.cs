using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.Algorithms
{
    class PassFailCollated
    {
        private static PassFailCollated theInstance = new PassFailCollated();
        protected PassFailCollated()
        {
            clear();
        }

        public static PassFailCollated instance
        {
            get
            {
                return theInstance;
            }
        }

        //////////////////////////////////////////////////

        // set parameters needed to create results file
        public void setFileParameters(string resultsDir, string laserId, string dateTimeStamp)
        {
            _resultsDir = resultsDir;
            _laserId = laserId;
            _dateTimeStamp = dateTimeStamp;
        }

        public void setOverallModeMapAnalysis(OverallModeMapAnalysis overallModeMapAnalysis)
        {
            _p_overallModeMapAnalysis = overallModeMapAnalysis;
        }
        public void addAnalysedSuperMode(CDSDBRSuperMode superMode)
        {
            _v_p_superModes.Add(superMode);
            _numSuperModesAnalysed++;
        }

        // collate results and write to file
        public void writeResultsToFile()
        {
            //if (_p_overallModeMapAnalysis != null)
            //{
            //    _collatedPassFailResultsAbsFilePath = getResultsAbsFilePath(_resultsDir, _laserId, _dateTimeStamp);

            //    CLoseGridFile collatedPassFailResults = new CLoseGridFile(_collatedPassFailResultsAbsFilePath);
            //    collatedPassFailResults.createFolderAndFile();


            //    // calculate _totalPassed value
            //    //if( _p_overallModeMapAnalysis->_screeningPassed )
            //    if (_p_overallModeMapAnalysis._screeningPassed)
            //    {
            //        //if( _p_overallModeMapAnalysis->_numSuperModes > 0 )
            //        if (_p_overallModeMapAnalysis.numSuperModes > 0)
            //        {
            //            _totalPassed = true;

            //            for (int i = 0; i < _v_p_superModes.Count; i++)
            //            {
            //                //CDSDBRSuperMode* p_sm = _v_p_superModes[i];
            //                CDSDBRSuperMode p_sm = _v_p_superModes[i];

            //                //if( !(p_sm->_superModeMapAnalysis._screeningPassed) )
            //                if (!(p_sm._superModeMapAnalysis._screeningPassed))
            //                {
            //                    _totalPassed = false;
            //                    break;
            //                }
            //            }
            //        }
            //        else
            //        {
            //            _totalPassed = false;
            //        }
            //    }
            //    else
            //    {
            //        _totalPassed = false;
            //    }


            //    //	Column headers

            //    const String QA_XLS_HEADER1 = "Total Pass/Fail";
            //    const String QA_XLS_HEADER2 = "Overall Mode Map Pass/Fail";
            //    const String QA_XLS_HEADER3 = "SM Index";
            //    const String QA_XLS_HEADER4 = "Supermode Map Pass/Fail/NotLoaded";

            //    //CStringArray headerArray;
            //    List<string> headerArray = new List<string>();
            //    headerArray.Add(QA_XLS_HEADER1);
            //    headerArray.Add(QA_XLS_HEADER2);
            //    headerArray.Add(QA_XLS_HEADER3);
            //    headerArray.Add(QA_XLS_HEADER4);

            //    // write headers
            //    collatedPassFailResults.addRow(headerArray);

            //    // write empty line
            //    collatedPassFailResults.addEmptyLine();




            //    String QA_XLS_COL1_VALUE = "";
            //    String QA_XLS_COL2_VALUE = "";
            //    String QA_XLS_COL3_VALUE = "";
            //    String QA_XLS_COL4_VALUE = "";


            //    if (_totalPassed)
            //        QA_XLS_COL1_VALUE = "PASS";
            //    else
            //        QA_XLS_COL1_VALUE = "FAIL";

            //    //if( _p_overallModeMapAnalysis->_screeningPassed ) 
            //    if (_p_overallModeMapAnalysis._screeningPassed)
            //        QA_XLS_COL2_VALUE = "PASS";
            //    else
            //        QA_XLS_COL2_VALUE = "FAIL";


            //    //CStringArray rowArray;
            //    List<string> rowArray = new List<string>();

            //    //if( _p_overallModeMapAnalysis->_numSuperModes <= 0 )
            //    if (_p_overallModeMapAnalysis.numSuperModes <= 0)
            //    {
            //        // write just these for case when no SMs were found
            //        rowArray.Add(QA_XLS_COL1_VALUE);
            //        rowArray.Add(QA_XLS_COL2_VALUE);
            //        collatedPassFailResults.addRow(rowArray);
            //        rowArray.Clear();
            //    }

            //    for (int sm = 0; sm < _p_overallModeMapAnalysis.numSuperModes; sm++)
            //    {
            //        QA_XLS_COL3_VALUE = sm.ToString("d");
            //        //QA_XLS_COL3_VALUE.AppendFormat("%d",sm);
            //        QA_XLS_COL4_VALUE = "Not Loaded";

            //        for (int i = 0; i < _v_p_superModes.Count; i++)
            //        {
            //            //CDSDBRSuperMode* p_sm = _v_p_superModes[i];
            //            CDSDBRSuperMode p_sm = _v_p_superModes[i];

            //            if (p_sm._sm_number == sm)
            //            { // supermap has been loaded

            //                if (p_sm._superModeMapAnalysis._screeningPassed)
            //                    QA_XLS_COL4_VALUE = "PASS";
            //                else
            //                    QA_XLS_COL4_VALUE = "FAIL";

            //                break;
            //            }
            //        }

            //        rowArray.Add(QA_XLS_COL1_VALUE);
            //        rowArray.Add(QA_XLS_COL2_VALUE);
            //        rowArray.Add(QA_XLS_COL3_VALUE);
            //        rowArray.Add(QA_XLS_COL4_VALUE);
            //        collatedPassFailResults.addRow(rowArray);
            //        rowArray.Clear();

            //        QA_XLS_COL1_VALUE = "";
            //        QA_XLS_COL2_VALUE = "";

            //    }


            //    // write 3 empty lines
            //    collatedPassFailResults.addEmptyLine();
            //    collatedPassFailResults.addEmptyLine();
            //    collatedPassFailResults.addEmptyLine();


            //    QA_XLS_COL1_VALUE = "Overall Map Pass/Fail Analysis";
            //    rowArray.Add(QA_XLS_COL1_VALUE);
            //    collatedPassFailResults.addRow(rowArray);
            //    rowArray.Clear();


            //    // write overall map pass/fail
            //    //_p_overallModeMapAnalysis->writePassFailResultsToFile( collatedPassFailResults );
            //    _p_overallModeMapAnalysis.writePassFailResultsToFile(collatedPassFailResults);



            //    for (int i = 0; i < _v_p_superModes.Count; i++)
            //    {
            //        //CDSDBRSuperMode* p_sm = _v_p_superModes[i];
            //        CDSDBRSuperMode p_sm = _v_p_superModes[i];

            //        // write 3 empty lines
            //        collatedPassFailResults.addEmptyLine();
            //        collatedPassFailResults.addEmptyLine();
            //        collatedPassFailResults.addEmptyLine();


            //        QA_XLS_COL1_VALUE = "Supermode " + i + " Map Pass/Fail Analysis";
            //        //QA_XLS_COL1_VALUE.AppendFormat("Supermode %d Map Pass/Fail Analysis",i);
            //        rowArray.Add(QA_XLS_COL1_VALUE);
            //        collatedPassFailResults.addRow(rowArray);
            //        rowArray.Clear();


            //        // write supermode map pass/fail
            //        //p_sm->_superModeMapAnalysis.writePassFailResultsToFile( collatedPassFailResults );
            //        p_sm._superModeMapAnalysis.writePassFailResultsToFile(collatedPassFailResults);

            //    }

            //}
        }


        public void clearSuperModes()
        {
            //_v_p_superModes.Clear();
        }

        ////////////////////////////////////////

        public bool _totalPassed;
        public bool _overallModeMapPassed;
        public bool _superModeMapPassed;
        public List<short> _superModeMapsPassed = new List<short>();

        //////////////////////////////////////////////////
        private
        void clear()
        {
            _resultsDir = "";
            _laserId = "";
            _dateTimeStamp = "";

            _collatedPassFailResultsAbsFilePath = "";

            _p_overallModeMapAnalysis = null;
            _v_p_superModes.Clear();

            _numSuperModesAnalysed = 0;

            _totalPassed = true;
            _overallModeMapPassed = true;
            _superModeMapPassed = true;
            _superModeMapsPassed.Clear();

        }


        ////////////////////////////////////////////////////

        String _resultsDir;
        String _laserId;
        String _dateTimeStamp;

        String _collatedPassFailResultsAbsFilePath;

        ///////////////////////////////////////////////////

        //OverallModeMapAnalysis* _p_overallModeMapAnalysis;
        OverallModeMapAnalysis _p_overallModeMapAnalysis;
        //vector<CDSDBRSuperMode*> _v_p_superModes;
        List<CDSDBRSuperMode> _v_p_superModes = new List<CDSDBRSuperMode>();

        int _numSuperModesAnalysed;

        ///////////////////////////////////////////////////

        String getResultsAbsFilePath(String resultsDir, String laserId, String dateTimeStamp)
        {
            String absFilePath = "";

            //absFilePath += resultsDir;
            //absFilePath += "\\";
            //absFilePath += Defaults.COLLATED_PASSFAIL_FILE_NAME;
            //absFilePath += Defaults.USCORE;
            //absFilePath += Defaults.LASER_TYPE_ID_DSDBR01_CSTRING;
            //absFilePath += Defaults.USCORE;
            //absFilePath += laserId;
            //absFilePath += Defaults.USCORE;
            //absFilePath += dateTimeStamp;
            //absFilePath += Defaults.CSV;

            return absFilePath;
        }




    }
}

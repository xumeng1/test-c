using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Bookham.TestLibrary.Algorithms
{
    static class LoggingLevels
    {
        public const int DIAGNOSTIC_CGR_LOG = 4;
        public const int INFO_CGR_LOG = 3;
        public const int WARNING_CGR_LOG = 2;
        public const int ERROR_CGR_LOG = 1;
        public const int CRITICAL_CGR_LOG = 0;
    }

    public sealed class Log
    {

        //public const int DIAGNOSTIC_LEVEL = 4;
        //public const int INFO_LEVEL = 3;
        //public const int WARNING_LEVEL = 2;
        //public const int ERROR_LEVEL = 1;
        //public const int CRITICAL_LEVEL = 0;

        private static Log instance = new Log();

        private string logFileName;
        private int logFileMaxSize;
        private int counter;

        private Log()
        {
            counter = 1;
            logFileName = DSDBR01.Instance._DSDBR01_LOGFILEPATHNAME;
            logFileMaxSize = DSDBR01.Instance._DSDBR01_LOGFILEMAXSIZE;
            string dir = Path.GetDirectoryName(logFileName);
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);
        }
        
        public static Log GetInstance
        {
            get
            {
                return instance;
            }
        }

        public void Write(string entryString, int level)
        {
            lock (this)
            {
                string lf = logFileName;
                FileInfo fi = new FileInfo(lf);
                if (fi.Exists && fi.Length > logFileMaxSize * 1024)
                {
                    //fi.MoveTo(lf + "." + counter.ToString());

                    fi.MoveTo(lf + "." + DateTime.Now.ToString("yyyyMMddHHmmss"));
                    ++counter;
                }

                File.AppendAllText(lf, string.Format("\n{0} Level: {1}\n{2}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), level, entryString));
            }
        }

    }
}

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Bookham.TestLibrary.Algorithms
{
    class CDSDBROverallModeMapLine
    {
        public enum rcode
        {
            ok = 0,
            file_does_not_exist,
            file_already_exists,
            could_not_open_file,
            file_format_invalid,
            //files_not_matching_sizes,
            //corrupt_map_not_written_to_file,
            //existing_file_not_overwritten,
            //maps_not_loaded,
            //outside_map_dimensions,
            //next_pt_not_found,
            no_data,
            invalid_front_pair_number,
            submap_indexing_error
            //no_supermodes_found
        }

        public struct point_type
        {
            public int row;
            public int col;
            public short front_pair_number;
            public double I_rear;
            public double I_front_1;
            public double I_front_2;
            public double I_front_3;
            public double I_front_4;
            public double I_front_5;
            public double I_front_6;
            public double I_front_7;
            public double I_front_8;
        }

        public CDSDBROverallModeMapLine()
        {
            clear();
        }

        // clear all contents
        public void clear()
        {
            _total_length = 0;
            _submap_length.Clear();
            _submap_start_index.Clear();
            _points.Clear();
            _abs_filepath = "";
        }

        public rcode getFrontPairNumbers(List<short> front_pair_numbers)
        {
            rcode rval = rcode.ok;
            front_pair_numbers.Clear();
            if (_points.Count > 0)
            {
                for (int i = 0; i < _points.Count; i++)
                {
                    short num_i = _points[i].front_pair_number;
                    if (num_i >= 1 && num_i <= 7)
                    {
                        bool in_vector = false;
                        for (int ip = 0; ip < front_pair_numbers.Count; ip++)
                        {
                            if (front_pair_numbers[ip] == num_i)
                            {
                                in_vector = true;
                                break;
                            }
                        }
                        if (in_vector == false)
                        {
                            front_pair_numbers.Add(num_i);
                        }
                    }
                    else
                    {
                        rval = rcode.invalid_front_pair_number;
                        break;
                    }
                }
            }
            else
            {
                rval = rcode.no_data;
            }

            return rval;
        }

        public rcode getCurrentsInOneFrontPair(
            short front_pair_number,
            ref double constant_front_current,
            List<double> non_constant_front_current,
            List<double> rear_current)
        {
            rcode rval = rcode.ok;
            non_constant_front_current.Clear();
            rear_current.Clear();

            if (front_pair_number >= 1 && front_pair_number <= 7)
            {
                if (!(_points.Count == 0))
                {
                    bool found_constant_front = false;

                    for (int i = 0; i < _points.Count; i++)
                    {
                        short num_i = _points[i].front_pair_number;

                        if (num_i == front_pair_number)
                        {
                            if (!found_constant_front)
                            {
                                found_constant_front = true;

                                switch (front_pair_number)
                                {
                                    case 1:
                                        constant_front_current = _points[i].I_front_1;
                                        break;
                                    case 2:
                                        constant_front_current = _points[i].I_front_2;
                                        break;
                                    case 3:
                                        constant_front_current = _points[i].I_front_3;
                                        break;
                                    case 4:
                                        constant_front_current = _points[i].I_front_4;
                                        break;
                                    case 5:
                                        constant_front_current = _points[i].I_front_5;
                                        break;
                                    case 6:
                                        constant_front_current = _points[i].I_front_6;
                                        break;
                                    case 7:
                                        constant_front_current = _points[i].I_front_7;
                                        break;
                                }
                            }

                            switch (front_pair_number)
                            {
                                case 1:
                                    rear_current.Add(_points[i].I_rear);
                                    non_constant_front_current.Add(_points[i].I_front_2);
                                    break;
                                case 2:
                                    rear_current.Add(_points[i].I_rear);
                                    non_constant_front_current.Add(_points[i].I_front_3);
                                    break;
                                case 3:
                                    rear_current.Add(_points[i].I_rear);
                                    non_constant_front_current.Add(_points[i].I_front_4);
                                    break;
                                case 4:
                                    rear_current.Add(_points[i].I_rear);
                                    non_constant_front_current.Add(_points[i].I_front_5);
                                    break;
                                case 5:
                                    rear_current.Add(_points[i].I_rear);
                                    non_constant_front_current.Add(_points[i].I_front_6);
                                    break;
                                case 6:
                                    rear_current.Add(_points[i].I_rear);
                                    non_constant_front_current.Add(_points[i].I_front_7);
                                    break;
                                case 7:
                                    rear_current.Add(_points[i].I_rear);
                                    non_constant_front_current.Add(_points[i].I_front_8);
                                    break;
                            }
                        }
                    }
                }
                else
                {
                    rval = rcode.no_data;
                }
            }
            else
            {
                rval = rcode.invalid_front_pair_number;
            }

            return rval;
        }

        public rcode updateSubmapTracking()
        {
            rcode rval = rcode.ok;

            _submap_start_index.Clear();
            _submap_length.Clear();

            int current_submap = 0;
            int front_pair_number = 0;
            int submap_length = 0;
            _total_length = _points.Count;

            for (int i = 0; i < _points.Count; i++)
            {
                front_pair_number = _points[i].front_pair_number;

                if (i == 0)
                {
                    for (short fpi = 0; fpi < front_pair_number - 1; fpi++)
                    {
                        _submap_start_index.Add(0);
                        _submap_length.Add(0);
                    }
                    current_submap = front_pair_number - 1;
                }

                if (current_submap != front_pair_number)
                {
                    if (current_submap + 1 == front_pair_number)
                    {
                        // found transition between pairs of front sections

                        _submap_start_index.Add(i);

                        if (i != 0)
                        {
                            _submap_length.Add(submap_length);
                        }
                        submap_length = 0;

                        current_submap = front_pair_number;

                    }
                    else
                    {
                        rval = rcode.submap_indexing_error;
                        break;
                    }
                }
                submap_length++;
            }
            _submap_length.Add(submap_length);

            // fill unused submap details 
            for (short fpi = (short)(_submap_length.Count); fpi < 7; fpi++)
            {
                _submap_start_index.Add(_total_length);
                _submap_length.Add(0);
            }

            if (rval != rcode.ok)
            {
                clear();
            }

            return rval;
        }

        public double getConstantIf(short pair_num, int sub_index)
        {
            double constant_If = 0;
            int index = _submap_start_index[pair_num - 1] + sub_index;

            switch (pair_num)
            {
                case 1:
                    constant_If = _points[index].I_front_1;
                    break;
                case 2:
                    constant_If = _points[index].I_front_2;
                    break;
                case 3:
                    constant_If = _points[index].I_front_3;
                    break;
                case 4:
                    constant_If = _points[index].I_front_4;
                    break;
                case 5:
                    constant_If = _points[index].I_front_5;
                    break;
                case 6:
                    constant_If = _points[index].I_front_6;
                    break;
                case 7:
                    constant_If = _points[index].I_front_7;
                    break;
            }

            return constant_If;
        }

        public double getNonConstantIf(short pair_num, int sub_index)
        {
            double non_constant_If = 0;
            int index = _submap_start_index[pair_num - 1] + sub_index;

            switch (pair_num)
            {
                case 1:
                    non_constant_If = _points[index].I_front_2;
                    break;
                case 2:
                    non_constant_If = _points[index].I_front_3;
                    break;
                case 3:
                    non_constant_If = _points[index].I_front_4;
                    break;
                case 4:
                    non_constant_If = _points[index].I_front_5;
                    break;
                case 5:
                    non_constant_If = _points[index].I_front_6;
                    break;
                case 6:
                    non_constant_If = _points[index].I_front_7;
                    break;
                case 7:
                    non_constant_If = _points[index].I_front_8;
                    break;
            }

            return non_constant_If;
        }

        public rcode writeRowColToFile(String abs_filepath, bool overwrite)
        {
            rcode rval = rcode.ok;
            long row_count = 0;
            long col_count = 0;

            if (_points.Count == 0)
            {
                rval = rcode.no_data;
            }
            else
            {
                // Check if file already exists and overwrite not specified
                if (File.Exists(abs_filepath) && overwrite == false)
                {
                    rval = rcode.file_already_exists;
                }
                else
                {
                    // Ensure the directory exists
                    int index_of_last_dirslash = abs_filepath.LastIndexOf('\\');
                    if (index_of_last_dirslash == -1)
                    {
                        // '\\' not found => not an absolute path
                        rval = rcode.could_not_open_file;
                    }
                    else
                    {
                        try
                        {
                            using (StreamWriter sw = new StreamWriter(abs_filepath))
                            {
                                for (int i = 0; i < _points.Count; i++)
                                {
                                    sw.WriteLine(_points[i].row.ToString() + Defaults.COMMA_CHAR + _points[i].col);
                                }
                            }
                        }
                        catch (Exception)
                        {
                            rval = rcode.could_not_open_file;
                        }
                    }
                }
            }

            return rval;
        }

        public rcode writeCurrentsToFile(string abs_filepath, bool overwrite)
        {
            rcode rval = rcode.ok;
            long row_count = 0;
            long col_count = 0;

            if (_points.Count == 0)
            {
                rval = rcode.no_data;
            }
            else
            {
                // Check if file already exists and overwrite not specified
                if (File.Exists(abs_filepath) && overwrite == false)
                {
                    rval = rcode.file_already_exists;
                }
                else
                {
                    // Ensure the directory exists
                    int index_of_last_dirslash = abs_filepath.LastIndexOf('\\');
                    if (index_of_last_dirslash == -1)
                    {
                        // '\\' not found => not an absolute path
                        rval = rcode.could_not_open_file;
                    }
                    else
                    {
                        try
                        {
                            using (StreamWriter file_stream = new StreamWriter(abs_filepath))
                            {
                                // First write header row
                                file_stream.Write("Rear Current [mA]");
                                file_stream.Write(Defaults.COMMA_CHAR);
                                file_stream.Write("Front Pair Number");
                                file_stream.Write(Defaults.COMMA_CHAR);
                                file_stream.Write("Constant Front Current [mA]");
                                file_stream.Write(Defaults.COMMA_CHAR);
                                file_stream.Write("Non-constant Front Current [mA]");
                                file_stream.WriteLine();

                                for (int i = 0; i < _points.Count; i++)
                                {
                                    // First write data rows
                                    file_stream.Write(_points[i].I_rear);
                                    file_stream.Write(Defaults.COMMA_CHAR);
                                    file_stream.Write(_points[i].front_pair_number);
                                    file_stream.Write(Defaults.COMMA_CHAR);


                                    switch (_points[i].front_pair_number)
                                    {
                                        case 1:
                                            file_stream.Write(_points[i].I_front_1);
                                            file_stream.Write(Defaults.COMMA_CHAR);
                                            file_stream.Write(_points[i].I_front_2);
                                            break;
                                        case 2:
                                            file_stream.Write(_points[i].I_front_2);
                                            file_stream.Write(Defaults.COMMA_CHAR);
                                            file_stream.Write(_points[i].I_front_3);
                                            break;
                                        case 3:
                                            file_stream.Write(_points[i].I_front_3);
                                            file_stream.Write(Defaults.COMMA_CHAR);
                                            file_stream.Write(_points[i].I_front_4);
                                            break;
                                        case 4:
                                            file_stream.Write(_points[i].I_front_4);
                                            file_stream.Write(Defaults.COMMA_CHAR);
                                            file_stream.Write(_points[i].I_front_5);
                                            break;
                                        case 5:
                                            file_stream.Write(_points[i].I_front_5);
                                            file_stream.Write(Defaults.COMMA_CHAR);
                                            file_stream.Write(_points[i].I_front_6);
                                            break;
                                        case 6:
                                            file_stream.Write(_points[i].I_front_6);
                                            file_stream.Write(Defaults.COMMA_CHAR);
                                            file_stream.Write(_points[i].I_front_7);
                                            break;
                                        case 7:
                                            file_stream.Write(_points[i].I_front_7);
                                            file_stream.Write(Defaults.COMMA_CHAR);
                                            file_stream.Write(_points[i].I_front_8);
                                            break;
                                    }

                                    file_stream.WriteLine();
                                }
                                _abs_filepath = abs_filepath;
                            }
                        }
                        catch (Exception)
                        {
                            rval = rcode.could_not_open_file;
                        }
                    }
                }

            }

            return rval;
        }

        public rcode readCurrentsFromFile(String abs_filepath)
        {
            rcode rval = rcode.ok;

            clear();

            int row_count = 0;
            int first_line_col_count = 0;

            // Check file
            if (File.Exists(abs_filepath))
            {
                // File exists => attempt to read it's contents
                try
                {
                    string[] arr = File.ReadAllLines(abs_filepath);
                    // File stream is open, read in header line, then ignore it!
                    row_count = 0;
                    for (int i = 1; i < arr.Length; i++)
                    {
                        if (arr[i].Length > 0) // there is something on the line
                        {
                            //char *fp = strtok(map_buffer,DELIMITERS);
                            string[] colarr = arr[i].Split(Defaults.DELIMITERS.ToCharArray());

                            double rear_current = -1;
                            short front_pair_number = 0;
                            double constant_If = -1;
                            double nonconstant_If = -1;

                            //if(fp) // read Irear
                            //{
                            //    rear_current = atof(fp);
                            //    fp = strtok(NULL,DELIMITERS);
                            //}
                            rear_current = double.Parse(colarr[0]);// read Irear
                            //if(fp) // read front_pair_number
                            //{
                            //    front_pair_number = atoi(fp);
                            //    fp = strtok(NULL,DELIMITERS);
                            //}
                            front_pair_number = short.Parse(colarr[1]);// read front_pair_number
                            //if(fp) // read constant_If
                            //{
                            //    constant_If = atof(fp);
                            //    fp = strtok(NULL,DELIMITERS);
                            //}
                            constant_If = double.Parse(colarr[2]);// read constant_If
                            //if(fp) // read nonconstant_If
                            //{
                            //    nonconstant_If = atof(fp);
                            //    fp = strtok(NULL,DELIMITERS);
                            //}
                            nonconstant_If = double.Parse(colarr[3]);// read nonconstant_If

                            if (front_pair_number > 7 || front_pair_number < 1
                             || constant_If < 0 || nonconstant_If < 0)
                            {
                                rval = rcode.file_format_invalid;
                                break;
                            }

                            point_type new_Im_point = new point_type();
                            new_Im_point.I_rear = rear_current;
                            new_Im_point.front_pair_number = front_pair_number;
                            new_Im_point.I_front_1 = 0;
                            new_Im_point.I_front_2 = 0;
                            new_Im_point.I_front_3 = 0;
                            new_Im_point.I_front_4 = 0;
                            new_Im_point.I_front_5 = 0;
                            new_Im_point.I_front_6 = 0;
                            new_Im_point.I_front_7 = 0;
                            new_Im_point.I_front_8 = 0;

                            switch (front_pair_number)
                            {
                                case 1:
                                    new_Im_point.I_front_1 = constant_If;
                                    new_Im_point.I_front_2 = nonconstant_If;
                                    break;
                                case 2:
                                    new_Im_point.I_front_2 = constant_If;
                                    new_Im_point.I_front_3 = nonconstant_If;
                                    break;
                                case 3:
                                    new_Im_point.I_front_3 = constant_If;
                                    new_Im_point.I_front_4 = nonconstant_If;
                                    break;
                                case 4:
                                    new_Im_point.I_front_4 = constant_If;
                                    new_Im_point.I_front_5 = nonconstant_If;
                                    break;
                                case 5:
                                    new_Im_point.I_front_5 = constant_If;
                                    new_Im_point.I_front_6 = nonconstant_If;
                                    break;
                                case 6:
                                    new_Im_point.I_front_6 = constant_If;
                                    new_Im_point.I_front_7 = nonconstant_If;
                                    break;
                                case 7:
                                    new_Im_point.I_front_7 = constant_If;
                                    new_Im_point.I_front_8 = nonconstant_If;
                                    break;
                            }

                            _points.Add(new_Im_point);

                            row_count++;
                        }
                    }
                }
                catch (Exception)
                {
                    rval = rcode.could_not_open_file;
                }
            }
            else
            {
                rval = rcode.file_does_not_exist;
            }

            if (rval == rcode.ok
             && (row_count == 0
               || _points.Count != row_count))
            {
                rval = rcode.file_format_invalid;
            }

            if (rval == rcode.ok)
            {
                _abs_filepath = abs_filepath;
                _total_length = row_count;
                rval = updateSubmapTracking();
            }

            if (rval != rcode.ok)
            {
                // something is wrong, clear everything!
                clear();
                throw new Exception(rval.ToString());
            }

            return rval;
        }
        
        public short pair_number(int index)
        {
            short front_pair_number = 0;

            if (index >= _submap_start_index[0] && index < _submap_start_index[1])
                front_pair_number = 1;
            else
                if (index >= _submap_start_index[1] && index < _submap_start_index[2])
                    front_pair_number = 2;
                else
                    if (index >= _submap_start_index[2] && index < _submap_start_index[3])
                        front_pair_number = 3;
                    else
                        if (index >= _submap_start_index[3] && index < _submap_start_index[4])
                            front_pair_number = 4;
                        else
                            if (index >= _submap_start_index[4] && index < _submap_start_index[5])
                                front_pair_number = 5;
                            else
                                if (index >= _submap_start_index[5] && index < _submap_start_index[6])
                                    front_pair_number = 6;
                                else
                                    if (index >= _submap_start_index[6] && index < _total_length)
                                        front_pair_number = 7;

            return front_pair_number;
        }

        public int index_in_submap(int index)
        {
            return index - _submap_start_index[pair_number(index) - 1];
        }

        public int _total_length;
        public List<int> _submap_length = new List<int>();
        public List<int> _submap_start_index = new List<int>();
        public List<point_type> _points = new List<point_type>();
        public string _abs_filepath;


        public List<double> getRows()
        {
            List<double> rows = new List<double>();

            for (int i = 0; i < _points.Count; i++)
            {
                rows.Add(_points[i].row);
            }

            return rows;
        }


        public List<double> getCols()
        {
            List<double> cols = new List<double>();

            for (int i = 0; i < _points.Count; i++)
            {
                cols.Add(_points[i].col);
            }

            return cols;
        }



    }
}

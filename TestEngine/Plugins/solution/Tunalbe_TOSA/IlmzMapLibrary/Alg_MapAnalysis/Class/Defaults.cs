using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.Algorithms
{
    static class Defaults
    {
        public const int OUTPUT_ON = 1;
        public const int OUTPUT_OFF = 0;

        public const string SMDC_RAMP_MIDDLE_LINE = "M";
        public const string SMDC_RAMP_PHASE = "P";

        public const string OMDC_RAMP_REAR = "R";
        public const string OMDC_RAMP_FRONT = "F";

        public const string CGSYSTEM_DEFAULT_RESULTS_DIR = "C:\\Program Files\\PXIT\\CLoseGrid4.0";

        public const string RAMP_DIRECTION_PHASE = "P";
        public const string RAMP_DIRECTION_MIDDLE_LINE = "M";
        public const string RAMP_DIRECTION_REAR = "R";
        public const string RAMP_DIRECTION_FRONT = "F";

        public const int CHANNEL_1_ON_306 = 1;
        public const int CHANNEL_2_ON_306 = 2;

        public const int NO_CHANNEL = 0;
        public const int CHANNEL_1 = 1;
        public const int CHANNEL_2 = 2;
        public const int ALL_CHANNELS = 3;

        public const int DISCONNECT_OUTPUT = 0;
        public const int CONNECT_OUTPUT = 1;
        public const int CONNECT_REVERSED_OUTPUT = 2;

        public const int NOT_IN_SEQUENCE = 0;
        public const int IN_SEQUENCE = 1;

        public const string CSV = ".csv";
        public const string USCORE = "_";
        public const string MATRIX_ = "MATRIX_";
        public const string MATRIX_FILTERED_ = "MATRIX_F_";
        public const string JUMPS_ = "JUMPS_";
        public const string LINKED_JUMPS_ = "JUMPS_L_";

        public const string QA_METRICS_FILE_NAME = "qaMetrics";
        public const string PASSFAIL_FILE_NAME = "passFailMetrics";
        public const string COLLATED_PASSFAIL_FILE_NAME = "collatedPassFailResults";

        public const string LASER_TYPE_ID_DSDBR01_CSTRING = "DSDBR01";

        public const string MEAS_TYPE_ID_POWERRATIO_CSTRING = "PowerRatio";
        public const string MEAS_TYPE_ID_POWER_CSTRING = "Power";

        public const string MEAS_TYPE_ID_FORWARDPOWER_CSTRING = "forwardPower";
        public const string MEAS_TYPE_ID_REVERSEPOWER_CSTRING = "reversePower";
        public const string MEAS_TYPE_ID_HYSTERESISPOWER_CSTRING = "hysteresisPower";

        public const string MEAS_TYPE_ID_FILTEREDPOWER_CSTRING = "filteredPower";
        public const string MEAS_TYPE_ID_FORWARDFILTEREDPOWER_CSTRING = "forwardFilteredPower";
        public const string MEAS_TYPE_ID_REVERSEFILTEREDPOWER_CSTRING = "reverseFilteredPower";

        public const string MEAS_TYPE_ID_FORWARDPOWERRATIO_CSTRING = "forwardPowerRatio";
        public const string MEAS_TYPE_ID_REVERSEPOWERRATIO_CSTRING = "reversePowerRatio";
        public const string MEAS_TYPE_ID_HYSTERESISPOWERRATIO_CSTRING = "hysteresisPowerRatio";

        public const string MEAS_TYPE_ID_FILTEREDPOWERRATIO_CSTRING = "FilteredPowerRatio";

        public const string MEAS_TYPE_ID_FORWARDPD1_CSTRING = "forwardPD1Current";
        public const string MEAS_TYPE_ID_REVERSEPD1_CSTRING = "reversePD1Current";
        public const string MEAS_TYPE_ID_FORWARDPD2_CSTRING = "forwardPD2Current";
        public const string MEAS_TYPE_ID_REVERSEPD2_CSTRING = "reversePD2Current";

        public const string FRONT_CURRENT_CSTRING = "If";
        public const string REAR_CURRENT_CSTRING = "Ir";
        public const string PHASE_CURRENT_CSTRING = "Ip";
        public const string MIDDLELINE_CURRENT_CSTRING = "Im";

        public const string LASER_ID_BAD_CHARS = " \t\r\n";
        public const string MODULE_ID_BAD_CHARS = "\t\r\n";
        public const int ID_MAX_LENGTH = 32;

        public const int NUMBER_STR_LENGTH = 32;
        public const int MAX_LINE_LENGTH = 10000;
        public const string DELIMITERS = ",\t\r\b\f\n ";
        public const char COMMA_CHAR = ',';

        public const string INSTANTIATED_STATE = "Instantiated";
        public const string STARTUP_STATE = "Startup";
        public const string SETUP_STATE = "Setup";
        public const string BUSY_STATE = "Busy";
        public const string COMPLETE_STATE = "Complete";

        public const int MAX_SEQUENCE_SIZE = 32000;

        public const double BIG_NUM = 1E99;

        public const int MIN_COARSEFREQPOLYCOEFFS = 2;
        public const int MAX_COARSEFREQPOLYCOEFFS = 100;
        public const int MAX_NUMBEROFCOARSEFREQPOLYSAMPLES = 1000;
        public const int MAX_ADDITIONAL_SAMPLES = 10;
        public const double DEFAULT_CFREQ_POLY_COEFF_0 = 195333.369;
        public const double DEFAULT_CFREQ_POLY_COEFF_1 = -5008.548;
        public const int MIN_VALID_FREQ_MEAS = 100000;// GHz
        public const int MAX_VALID_FREQ_MEAS = 300000; // GHz
        public const double MIN_DIRECT_OPTICAL_POWER = 0.01;//mW

        public const int MAX_ITERATIONS_TO_FIND_ITU_PT = 20 ;//100; keep same with config file
        public const double MIN_PT_GAP_TO_FIND_ITU_PT = 0.01;

        public const int SM_NOT_LOADED = -1;

        public const int MAP_TYPE_SAWTOOTH = 0;
        public const int MAP_TYPE_TRIANGLE = 1;

        //math
        public const double M_E = 2.71828182845904523536;
        public const double M_LOG2E = 1.44269504088896340736;
        public const double M_LOG10E = 0.434294481903251827651;
        public const double M_LN2 = 0.693147180559945309417;
        public const double M_LN10 = 2.30258509299404568402;
        public const double M_PI = 3.14159265358979323846;
        public const double M_PI_2 = 1.57079632679489661923;
        public const double M_PI_4 = 0.785398163397448309616;
        public const double M_1_PI = 0.318309886183790671538;
        public const double M_2_PI = 0.636619772367581343076;
        public const double M_2_SQRTPI = 1.12837916709551257390;
        public const double M_SQRT2 = 1.41421356237309504880;
        public const double M_SQRT1_2 = 0.707106781186547524401;
    }
}

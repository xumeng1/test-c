using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using Bookham.fcumapping.utility;

namespace Bookham.TestLibrary.Algorithms
{
    public class CDSDBROverallModeMap
    {
        #region Types
        public enum rcode
        {
            ok = 0,
            file_does_not_exist,
            file_already_exists,
            could_not_open_file,
            file_format_invalid,
            files_not_matching_sizes,
            corrupt_map_not_written_to_file,
            existing_file_not_overwritten,
            maps_not_loaded,
            currents_not_loaded,
            outside_map_dimensions,
            corrupt_map_dimensions,
            next_pt_not_found,
            rhs_top_border_not_reached,
            no_data,
            no_start_point,
            no_supermodes_found,
            submap_indexing_error,
            sequencing_error,
            sequence_data_format_error,
            sequencing_step_size_error,
            hardware_error,
            system_error,
            set_current,
            registry_error,
            log_file_error,
            results_disk_full,
            connectivity_error,
            power_meter_not_found,
            wavemeter_not_found,
            tec_not_found,
            power_read_error,
            duplicate_device_name,
            pxit_module_error,
            device_name_not_on_card,
            device_306_firmware_obsolete,
            laser_alignment_error,
            empty_sequence_slots,
            module_name_not_found_error,
            //new added by echo
            no_start_index_found,
            no_upper_point_found,
            not_enough_point,
            empty_array_to_relink,

            unknown_error
        }
        public enum direction_type
        {
            N = 0,
            NW,
            W,
            SW,
            S,
            SE,
            E,
            NE
        }

        internal direction_type START_PT = (direction_type)(-1);

        public struct boundary_point_type
        {
            public int row;
            public int col;
            public direction_type found_in_direction;
        }

        #endregion

        #region Variant

        internal TestInfo testInfo;

        // Currents used to collect maps
        public CCurrentsVector _vector_Ir = new CCurrentsVector();
        public CDSDBRFrontCurrents _vector_If = new CDSDBRFrontCurrents();

        // Direct Power Maps collected from laser or read from file
        public List<CLaserModeMap> _map_forward_direct_power = new List<CLaserModeMap>();

        // Reference Maps collected from laser or read from file
        CLaserModeMap _map_forward_direct_power_dfs1 = new CLaserModeMap();
        CLaserModeMap _map_forward_direct_power_dfs2 = new CLaserModeMap();
        CLaserModeMap _map_forward_direct_power_dfs3 = new CLaserModeMap();
        CLaserModeMap _map_forward_direct_power_dfs4 = new CLaserModeMap();
        CLaserModeMap _map_forward_direct_power_dfs5 = new CLaserModeMap();
        CLaserModeMap _map_forward_direct_power_dfs6 = new CLaserModeMap();
        CLaserModeMap _map_forward_direct_power_dfs7 = new CLaserModeMap();

        // Filtered Power Maps collected from laser or read from file
        public List<CLaserModeMap> _map_forward_filtered_power = new List<CLaserModeMap>();

        // Filtered Maps collected from laser or read from file
        CLaserModeMap _map_forward_filtered_power_dfs1 = new CLaserModeMap();
        CLaserModeMap _map_forward_filtered_power_dfs2 = new CLaserModeMap();
        CLaserModeMap _map_forward_filtered_power_dfs3 = new CLaserModeMap();
        CLaserModeMap _map_forward_filtered_power_dfs4 = new CLaserModeMap();
        CLaserModeMap _map_forward_filtered_power_dfs5 = new CLaserModeMap();
        CLaserModeMap _map_forward_filtered_power_dfs6 = new CLaserModeMap();
        CLaserModeMap _map_forward_filtered_power_dfs7 = new CLaserModeMap();

        // Direct Power Maps collected from laser or read from file
        List<CLaserModeMap> _map_reverse_direct_power = new List<CLaserModeMap>();

        // Filtered Power Maps collected from laser or read from file
        List<CLaserModeMap> _map_reverse_filtered_power = new List<CLaserModeMap>();

        // Photodiode Maps collected from laser
        List<CLaserModeMap> _map_forward_photodiode1 = new List<CLaserModeMap>();
        List<CLaserModeMap> _map_forward_photodiode2 = new List<CLaserModeMap>();
        List<CLaserModeMap> _map_reverse_photodiode1 = new List<CLaserModeMap>();
        List<CLaserModeMap> _map_reverse_photodiode2 = new List<CLaserModeMap>();

        // Power Ratio Maps collected from laser or read from file
        CLaserModeMap _map_power_ratio_dfs1 = new CLaserModeMap();
        CLaserModeMap _map_power_ratio_dfs2 = new CLaserModeMap();
        CLaserModeMap _map_power_ratio_dfs3 = new CLaserModeMap();
        CLaserModeMap _map_power_ratio_dfs4 = new CLaserModeMap();
        CLaserModeMap _map_power_ratio_dfs5 = new CLaserModeMap();
        CLaserModeMap _map_power_ratio_dfs6 = new CLaserModeMap();
        CLaserModeMap _map_power_ratio_dfs7 = new CLaserModeMap();

        // Max deltaPr maps
        List<CLaserModeMap> _map_max_deltaPr = new List<CLaserModeMap>();

        // Power Ratio Maps after median filter has been applied
        CLaserModeMap _map_power_ratio_dfs1_median_filtered = new CLaserModeMap();
        CLaserModeMap _map_power_ratio_dfs2_median_filtered = new CLaserModeMap();
        CLaserModeMap _map_power_ratio_dfs3_median_filtered = new CLaserModeMap();
        CLaserModeMap _map_power_ratio_dfs4_median_filtered = new CLaserModeMap();
        CLaserModeMap _map_power_ratio_dfs5_median_filtered = new CLaserModeMap();
        CLaserModeMap _map_power_ratio_dfs6_median_filtered = new CLaserModeMap();
        CLaserModeMap _map_power_ratio_dfs7_median_filtered = new CLaserModeMap();

        // vector contains supermodes found in map
        internal List<CDSDBRSuperMode> _supermodes = new List<CDSDBRSuperMode>();

        // count of the number of supermodes removed because of flaws encountered
        short _supermodes_removed;

        bool _maps_loaded;
        bool _Ir_ramp; // true for Ir ramp, false for If ramp

        // The above CLaserModeMap objects can contain
        // rear ramped or front ramped maps and the members
        // _rows and _cols in those objects change accordingly.
        // However, for the members _rows and _cols in this object
        // _rows always refers to the number of front current points and
        // _cols always refers to the number of rear current points.
        // As such, the overall mode map is always viewed the same way
        // independent of which ramp direction was used to gather the maps.
        int _rows;
        int _cols;

        int _xAxisLength;
        int _yAxisLength;

        PointsAnalystClass pointsAnalyst;

        public String _results_dir_CString;
        public String _laser_id_CString;
        public String _date_time_stamp_CString;
        public String _laser_wafer_id_CString;//add by Jim @2011.12.12

        //short* _p_percent_complete;
        short _p_percent_complete;

        internal OverallModeMapAnalysis _overallModeMapAnalysis = new OverallModeMapAnalysis();

        List<double> _continuityValues = new List<double>();
        List<double> _cacheMap = new List<double>();

        #endregion

        private Log CGR_LOG;

        public CDSDBROverallModeMap()
        {
            CGR_LOG = Log.GetInstance;

        }

        public rcode GetAveragePowersOnMiddleLines(List<double> average_powers_on_middle_lines, List<CLaserModeMap> _map_forward_power)//&
        {
            CGR_LOG.Write("CDSDBROverallModeMap.getAveragePowersOnMiddleLines() entered", LoggingLevels.DIAGNOSTIC_CGR_LOG);

            rcode rval = rcode.ok;
            average_powers_on_middle_lines.Clear();

            for (short sm_number = 0; sm_number < (short)(_supermodes.Count); sm_number++)
            {
                if (!(_supermodes[sm_number]._filtered_middle_line._points.Count == 0)
                 && _map_forward_direct_power.Count == 7)
                {
                    double average_power = 0;
                    double average_filter_power = 0;
                    int line_length = _supermodes[sm_number]._filtered_middle_line._points.Count;
                    for (int i = 0; i < line_length; i++)
                    {
                        int row = _supermodes[sm_number]._filtered_middle_line._points[i].row;
                        int col = _supermodes[sm_number]._filtered_middle_line._points[i].col;

                        //double direct_power_at_pt = iDirectPowerPt(row, col).Current;
                        double power_at_pt = iPowerPt(row, col, _map_forward_power).Current;
                        average_power += power_at_pt;
                    }
                    average_power /= line_length;

                    average_powers_on_middle_lines.Add(average_power);

                    String log_msg = "CDSDBROverallModeMap.getAveragePowersOnMiddleLines() ";
                    log_msg += string.Format("average power on supermode {0} is {1:g} mW", sm_number, average_power);
                    CGR_LOG.Write(log_msg, LoggingLevels.INFO_CGR_LOG);
                }
                else
                {
                    average_powers_on_middle_lines.Add(0);

                    String log_msg = "CDSDBROverallModeMap.getAveragePowersOnMiddleLines() ";
                    log_msg += string.Format("no middle line found for supermode {0}", sm_number);
                    CGR_LOG.Write(log_msg, LoggingLevels.ERROR_CGR_LOG);
                }
            }

            CGR_LOG.Write("CDSDBROverallModeMap.getAveragePowersOnMiddleLines() exiting", LoggingLevels.DIAGNOSTIC_CGR_LOG);

            return rval;
        }

        public rcode writeScreeningResultsToFile()
        {
            CGR_LOG.Write("CDSDBROverallModeMap.writeScreeningResultsToFile() entered", LoggingLevels.DIAGNOSTIC_CGR_LOG);

            rcode rval = rcode.ok;

            string lines_abs_filepath = _results_dir_CString;
            if (!lines_abs_filepath.EndsWith("\\"))
                lines_abs_filepath += "\\";
            lines_abs_filepath += "lines_OverallMap_";
            lines_abs_filepath += Defaults.LASER_TYPE_ID_DSDBR01_CSTRING;
            lines_abs_filepath += Defaults.USCORE;
            lines_abs_filepath += _laser_id_CString;
            lines_abs_filepath += Defaults.USCORE;
            lines_abs_filepath += _date_time_stamp_CString;
            lines_abs_filepath += Defaults.CSV;

            // write 1 file containing row/col coordinates of
            // upper boundary line, lower boundary line and middle line
            // of all supermodes found
            rval = writeSMLinesToFile(
                lines_abs_filepath,
                DSDBR01.Instance._DSDBR01_OMDC_overwrite_existing_files);

            // For each supermode found
            // write a file containing currents of its middle line.
            // This describes the current to be used when gathering
            // a supermode's map
            CDSDBROverallModeMapLine.rcode lrval = CDSDBROverallModeMapLine.rcode.ok;
            for (int sm_number = 0; sm_number < _supermodes.Count && rval == rcode.ok; sm_number++)
            {
                string middle_line_abs_filepath = _results_dir_CString;
                if (!middle_line_abs_filepath.EndsWith("\\")) middle_line_abs_filepath += "\\";

                middle_line_abs_filepath += Defaults.MIDDLELINE_CURRENT_CSTRING;
                middle_line_abs_filepath += Defaults.USCORE;
                middle_line_abs_filepath += Defaults.LASER_TYPE_ID_DSDBR01_CSTRING;
                middle_line_abs_filepath += Defaults.USCORE;
                middle_line_abs_filepath += _laser_id_CString;
                middle_line_abs_filepath += Defaults.USCORE;
                middle_line_abs_filepath += _date_time_stamp_CString;
                middle_line_abs_filepath += Defaults.USCORE;
                middle_line_abs_filepath += "SM";
                middle_line_abs_filepath += sm_number.ToString();


                //CString ufmiddle_line_abs_filepath = middle_line_abs_filepath + _T("_uf");;
                middle_line_abs_filepath += Defaults.CSV;
                //ufmiddle_line_abs_filepath += CSV;

                lrval = _supermodes[sm_number]._filtered_middle_line.writeCurrentsToFile(
                    middle_line_abs_filepath,
                    DSDBR01.Instance._DSDBR01_OMDC_overwrite_existing_files);
                if (lrval != CDSDBROverallModeMapLine.rcode.ok)
                    break;

                if (DSDBR01.Instance._DSDBR01_OMBD_write_boundaries_to_file)
                {
                    String lower_line_abs_filepath = _results_dir_CString;
                    if (!lower_line_abs_filepath.EndsWith("\\"))
                        lower_line_abs_filepath += "\\";
                    lower_line_abs_filepath += "lower_boundary_line_";
                    lower_line_abs_filepath += sm_number.ToString();
                    lower_line_abs_filepath += ".txt";

                    lrval = _supermodes[sm_number]._lower_line.writeRowColToFile(
                        lower_line_abs_filepath,
                        DSDBR01.Instance._DSDBR01_OMDC_overwrite_existing_files);
                    if (lrval != CDSDBROverallModeMapLine.rcode.ok)
                        break;

                    String upper_line_abs_filepath = _results_dir_CString;
                    if (!upper_line_abs_filepath.EndsWith("\\"))
                        upper_line_abs_filepath += "\\";
                    upper_line_abs_filepath += "upper_boundary_line_";
                    upper_line_abs_filepath += sm_number.ToString();
                    upper_line_abs_filepath += ".txt";

                    lrval = _supermodes[sm_number]._upper_line.writeRowColToFile(
                        upper_line_abs_filepath,
                        DSDBR01.Instance._DSDBR01_OMDC_overwrite_existing_files);
                    if (lrval != CDSDBROverallModeMapLine.rcode.ok)
                        break;
                }
            }
            if (lrval == CDSDBROverallModeMapLine.rcode.could_not_open_file)
                rval = rcode.could_not_open_file;
            if (lrval == CDSDBROverallModeMapLine.rcode.file_already_exists)
                rval = rcode.file_already_exists;
            if (lrval == CDSDBROverallModeMapLine.rcode.no_data)
                rval = rcode.no_data;

            String continuity_abs_filepath = _results_dir_CString;
            if (!continuity_abs_filepath.EndsWith("\\"))
                continuity_abs_filepath += "\\";
            continuity_abs_filepath += "PowerRatio_Continuity_";
            continuity_abs_filepath += Defaults.LASER_TYPE_ID_DSDBR01_CSTRING;
            continuity_abs_filepath += Defaults.USCORE;
            continuity_abs_filepath += _laser_id_CString;
            continuity_abs_filepath += Defaults.USCORE;
            continuity_abs_filepath += _date_time_stamp_CString;
            continuity_abs_filepath += Defaults.CSV;

            // Write 1 file containing the Power Ratio values
            // found on the middle lines of all supermodes found
            if (rval == rcode.ok)
            {
                rval = writePRatioContinuityToFile(
                    continuity_abs_filepath,
                    DSDBR01.Instance._DSDBR01_OMDC_overwrite_existing_files);
            }

            CGR_LOG.Write("CDSDBROverallModeMap.writeScreeningResultsToFile() exiting", LoggingLevels.DIAGNOSTIC_CGR_LOG);
            return rval;
        }

        rcode writePRatioContinuityToFile(string abs_filepath, bool overwrite)
        {
            rcode rval = rcode.ok;
            if (_supermodes.Count == 0)
            {
                rval = rcode.no_data;
                CGR_LOG.Write("CDSDBROverallModeMap.writePRatioContinuityToFile error: no_data", LoggingLevels.ERROR_CGR_LOG);
            }
            else
            {
                // Check if file already exists and overwrite not specified
                if (File.Exists(abs_filepath) && !overwrite)
                {
                    rval = rcode.file_already_exists;
                    CGR_LOG.Write("CDSDBROverallModeMap.writePRatioContinuityToFile error: file_already_exists", LoggingLevels.ERROR_CGR_LOG);
                }
                else
                {
                    using (StreamWriter sr = new StreamWriter(abs_filepath))
                    {
                        // First write header row
                        sr.WriteLine("SM#{0}PowerRatio", Defaults.COMMA_CHAR);
                        for (int sm = 0; sm < _supermodes.Count; sm++)
                        {
                            List<CDSDBROverallModeMapLine.point_type> p_filtered_middle_points = _supermodes[sm]._filtered_middle_line._points;

                            if (p_filtered_middle_points.Count == 0)
                            {
                                rval = rcode.no_data;
                                CGR_LOG.Write("CDSDBROverallModeMap.writePRatioContinuityToFile error: filtered middle line is empty", LoggingLevels.ERROR_CGR_LOG);
                            }
                            else
                            {
                                for (int i = 0; i < p_filtered_middle_points.Count; i++)
                                {
                                    int row_i = p_filtered_middle_points[i].row;
                                    int col_i = p_filtered_middle_points[i].col;
                                    //double power_ratio = *iPowerRatioPt( row_i, col_i );
                                    double power_ratio = iPowerRatioPt(row_i, col_i).Current;

                                    //file_stream << sm;
                                    //file_stream << COMMA_CHAR;
                                    //file_stream << power_ratio;
                                    //file_stream << std.endl;
                                    sr.WriteLine("{1}{0}{2}", Defaults.COMMA_CHAR, sm, power_ratio);
                                }
                            }
                        }
                        sr.Close();
                    }
                }
            }
            return rval;
        }

        rcode writeSMLinesToFile(string abs_filepath, bool overwrite)
        {
            rcode rval = rcode.ok;
            //int row_count = 0;
            //int col_count = 0;

            if (_supermodes.Count == 0)
            {
                CGR_LOG.Write("CDSDBROverallModeMap.writeSMLinesToFile error: no_data", LoggingLevels.ERROR_CGR_LOG);
                rval = rcode.no_data;
            }
            else
            {
                // Ensure the directory exists
                int index_of_last_dirslash = abs_filepath.LastIndexOf('\\');
                if (index_of_last_dirslash == -1)
                {
                    // '\\' not found => not an absolute path
                    rval = rcode.could_not_open_file;
                    CGR_LOG.Write("CDSDBROverallModeMap.writeSMLinesToFile error: could_not_open_file", LoggingLevels.ERROR_CGR_LOG);
                }
                else
                {
                    using (StreamWriter sr = new StreamWriter(abs_filepath))
                    {
                        sr.WriteLine("SM#{0}LineType{0}Row{0}Col", Defaults.COMMA_CHAR);
                        for (int sm = 0; sm < _supermodes.Count; sm++)
                        {
                            List<CDSDBROverallModeMapLine.point_type> p_lower_points = _supermodes[sm]._lower_line._points;
                            List<CDSDBROverallModeMapLine.point_type> p_upper_points = _supermodes[sm]._upper_line._points;
                            List<CDSDBROverallModeMapLine.point_type> p_filtered_middle_points = _supermodes[sm]._filtered_middle_line._points;

                            if (p_lower_points.Count == 0 || p_upper_points.Count == 0
                                || p_filtered_middle_points.Count == 0)
                            {
                                rval = rcode.no_data;
                                CGR_LOG.Write("CDSDBROverallModeMap.writeSMLinesToFile error: filtered middle line empty", LoggingLevels.ERROR_CGR_LOG);
                            }
                            else
                            {
                                string[] names ={ "Lower", "Upper", "Middle" };
                                List<CDSDBROverallModeMapLine.point_type>[] points ={ p_lower_points, p_upper_points, p_filtered_middle_points };
                                for (int j = 0; j < names.Length; j++)
                                {
                                    for (int i = 0; i < points[j].Count; i++)
                                    {
                                        sr.WriteLine("{1}{0}{4}{0}{2}{0}{3}", Defaults.COMMA_CHAR, sm, points[j][i].row, points[j][i].col, names[j]);
                                    }
                                }
                            }
                        }
                        sr.Close();
                    }
                }
            }
            return rval;
        }

        public void clear()
        {
            _vector_Ir.clear();
            _vector_If.clear();

            for (int i = 0; i < _map_forward_direct_power.Count; i++)
            {
                _map_forward_direct_power[i].clear();
            }
            _map_forward_direct_power.Clear();

            for (int i = 0; i < _map_reverse_direct_power.Count; i++)
            {
                _map_reverse_direct_power[i].clear();
            }
            _map_reverse_direct_power.Clear();

            for (int i = 0; i < _map_forward_filtered_power.Count; i++)
            {
                _map_forward_filtered_power[i].clear();
            }
            _map_forward_filtered_power.Clear();

            for (int i = 0; i < _map_reverse_filtered_power.Count; i++)
            {
                _map_reverse_filtered_power[i].clear();
            }
            _map_reverse_filtered_power.Clear();

            for (int i = 0; i < _map_forward_photodiode1.Count; i++)
            {
                _map_forward_photodiode1[i].clear();
            }
            _map_forward_photodiode1.Clear();

            for (int i = 0; i < _map_forward_photodiode2.Count; i++)
            {
                _map_forward_photodiode2[i].clear();
            }
            _map_forward_photodiode2.Clear();

            for (int i = 0; i < _map_reverse_photodiode1.Count; i++)
            {
                _map_reverse_photodiode1[i].clear();
            }
            _map_reverse_photodiode1.Clear();

            for (int i = 0; i < _map_reverse_photodiode2.Count; i++)
            {
                _map_reverse_photodiode2[i].clear();
            }
            _map_reverse_photodiode2.Clear();

            for (int i = 0; i < _map_max_deltaPr.Count; i++)
            {
                _map_max_deltaPr[i].clear();
            }
            _map_max_deltaPr.Clear();


            _map_power_ratio_dfs1.clear();
            _map_power_ratio_dfs2.clear();
            _map_power_ratio_dfs3.clear();
            _map_power_ratio_dfs4.clear();
            _map_power_ratio_dfs5.clear();
            _map_power_ratio_dfs6.clear();
            _map_power_ratio_dfs7.clear();

            _map_power_ratio_dfs1_median_filtered.clear();
            _map_power_ratio_dfs2_median_filtered.clear();
            _map_power_ratio_dfs3_median_filtered.clear();
            _map_power_ratio_dfs4_median_filtered.clear();
            _map_power_ratio_dfs5_median_filtered.clear();
            _map_power_ratio_dfs6_median_filtered.clear();
            _map_power_ratio_dfs7_median_filtered.clear();

            for (int i = 0; i < _supermodes.Count; i++)
            {
                _supermodes[i].clear();
            }
            _supermodes.Clear();

            _supermodes_removed = 0;

            _maps_loaded = false;
            _Ir_ramp = false;
            _rows = 0;
            _cols = 0;
            _results_dir_CString = "";
            _date_time_stamp_CString = "";
            _laser_id_CString = "";

            _xAxisLength = 0;
            _yAxisLength = 0;

            //_threshold_maxDeltaPr = 0;

            return;
        }

        internal rcode writeMapsToFile()
        {
            // Write the overall map to file
            // The overall map is stored in 7 files named
            // MATRIX_[mapType]_DSDBR[##]_[laser_ID]_[YYYYMMDDHHMMSS]_DFS[i].csv
            // where i is from 1 to 7
            CGR_LOG.Write("CDSDBROverallModeMap.writeMapsToFile() entered", LoggingLevels.DIAGNOSTIC_CGR_LOG);

            rcode rval = rcode.ok;
            String results_dir_CString = _results_dir_CString;
            // Check for directory without trailing '\\' character
            if (!results_dir_CString.EndsWith("\\"))
            {
                // Add trailing directory backslash '\\'
                results_dir_CString += "\\";
            }
            String file_path = results_dir_CString;
            file_path += Defaults.MATRIX_;
            file_path += Defaults.MEAS_TYPE_ID_POWERRATIO_CSTRING;
            file_path += Defaults.USCORE;
            file_path += Defaults.LASER_TYPE_ID_DSDBR01_CSTRING;
            file_path += Defaults.USCORE;
            file_path += _laser_id_CString;
            file_path += Defaults.USCORE;
            file_path += _date_time_stamp_CString;
            file_path += "_DFS";

            // create absolute file path for each of the seven files

            String map_ramp_direction_CString;
            if (_Ir_ramp)
                map_ramp_direction_CString = Defaults.RAMP_DIRECTION_REAR;
            else
                map_ramp_direction_CString = Defaults.RAMP_DIRECTION_FRONT;

            String file_path_DFS1 = file_path + "1" + Defaults.USCORE + map_ramp_direction_CString + Defaults.CSV;
            String file_path_DFS2 = file_path + "2" + Defaults.USCORE + map_ramp_direction_CString + Defaults.CSV;
            String file_path_DFS3 = file_path + "3" + Defaults.USCORE + map_ramp_direction_CString + Defaults.CSV;
            String file_path_DFS4 = file_path + "4" + Defaults.USCORE + map_ramp_direction_CString + Defaults.CSV;
            String file_path_DFS5 = file_path + "5" + Defaults.USCORE + map_ramp_direction_CString + Defaults.CSV;
            String file_path_DFS6 = file_path + "6" + Defaults.USCORE + map_ramp_direction_CString + Defaults.CSV;
            String file_path_DFS7 = file_path + "7" + Defaults.USCORE + map_ramp_direction_CString + Defaults.CSV;

            String If_file_path = results_dir_CString;
            If_file_path += Defaults.FRONT_CURRENT_CSTRING;
            If_file_path += "_OverallMap_";
            If_file_path += Defaults.LASER_TYPE_ID_DSDBR01_CSTRING;
            If_file_path += Defaults.USCORE;
            If_file_path += _laser_id_CString;
            If_file_path += Defaults.USCORE;
            If_file_path += _date_time_stamp_CString;
            If_file_path += Defaults.CSV;

            String Ir_file_path = results_dir_CString;
            Ir_file_path += Defaults.REAR_CURRENT_CSTRING;
            Ir_file_path += "_OverallMap_";
            Ir_file_path += Defaults.LASER_TYPE_ID_DSDBR01_CSTRING;
            Ir_file_path += Defaults.USCORE;
            Ir_file_path += _laser_id_CString;
            Ir_file_path += Defaults.USCORE;
            Ir_file_path += _date_time_stamp_CString;
            Ir_file_path += Defaults.CSV;

            // write data to files
            CCurrentsVector.rcode write_Ir_to_file_rval = CCurrentsVector.rcode.ok;
            write_Ir_to_file_rval = _vector_Ir.writeToFile(Ir_file_path, DSDBR01.Instance._DSDBR01_OMDC_overwrite_existing_files);
            if (write_Ir_to_file_rval != CCurrentsVector.rcode.ok)
            {
                string log_err_msg = "CDSDBROverallModeMap.writeMapsToFile() error writing rear currents to file: ";
                log_err_msg += Ir_file_path;
                CGR_LOG.Write(log_err_msg, LoggingLevels.ERROR_CGR_LOG);
            }

            CDSDBRFrontCurrents.rcode write_If_to_file_rval = CDSDBRFrontCurrents.rcode.ok;
            write_If_to_file_rval = _vector_If.writeToFile(If_file_path, DSDBR01.Instance._DSDBR01_OMDC_overwrite_existing_files);

            if (write_If_to_file_rval != CDSDBRFrontCurrents.rcode.ok)
            {
                String log_err_msg = "CDSDBROverallModeMap.writeMapsToFile() error writing front currents to file: ";
                log_err_msg += If_file_path;
                CGR_LOG.Write(log_err_msg, LoggingLevels.ERROR_CGR_LOG);
            }

            CLaserModeMap.rcode write_to_file_rval = CLaserModeMap.rcode.ok;

            if (write_to_file_rval == CLaserModeMap.rcode.ok)
                write_to_file_rval = _map_power_ratio_dfs1.writeToFile(file_path_DFS1,
                    DSDBR01.Instance._DSDBR01_OMDC_overwrite_existing_files);

            if (write_to_file_rval == CLaserModeMap.rcode.ok)
                write_to_file_rval = _map_power_ratio_dfs2.writeToFile(
                file_path_DFS2,
                DSDBR01.Instance._DSDBR01_OMDC_overwrite_existing_files);

            if (write_to_file_rval == CLaserModeMap.rcode.ok)
                write_to_file_rval = _map_power_ratio_dfs3.writeToFile(
                    file_path_DFS3,
                    DSDBR01.Instance._DSDBR01_OMDC_overwrite_existing_files);

            if (write_to_file_rval == CLaserModeMap.rcode.ok)
                write_to_file_rval = _map_power_ratio_dfs4.writeToFile(
                    file_path_DFS4,
                    DSDBR01.Instance._DSDBR01_OMDC_overwrite_existing_files);

            if (write_to_file_rval == CLaserModeMap.rcode.ok)
                write_to_file_rval = _map_power_ratio_dfs5.writeToFile(
                    file_path_DFS5,
                    DSDBR01.Instance._DSDBR01_OMDC_overwrite_existing_files);

            if (write_to_file_rval == CLaserModeMap.rcode.ok)
                write_to_file_rval = _map_power_ratio_dfs6.writeToFile(
                    file_path_DFS6,
                    DSDBR01.Instance._DSDBR01_OMDC_overwrite_existing_files);

            if (write_to_file_rval == CLaserModeMap.rcode.ok)
                write_to_file_rval = _map_power_ratio_dfs7.writeToFile(
                    file_path_DFS7,
                    DSDBR01.Instance._DSDBR01_OMDC_overwrite_existing_files);

            if (write_to_file_rval == CLaserModeMap.rcode.ok)
            {
                for (int i = 0; i < _map_forward_direct_power.Count; i++)
                {
                    String forward_direct_power_file_path = results_dir_CString;
                    forward_direct_power_file_path += Defaults.MATRIX_;
                    forward_direct_power_file_path += Defaults.MEAS_TYPE_ID_POWER_CSTRING;
                    forward_direct_power_file_path += Defaults.USCORE;
                    forward_direct_power_file_path += Defaults.LASER_TYPE_ID_DSDBR01_CSTRING;
                    forward_direct_power_file_path += Defaults.USCORE;
                    forward_direct_power_file_path += _laser_id_CString;
                    forward_direct_power_file_path += Defaults.USCORE;
                    forward_direct_power_file_path += _date_time_stamp_CString;

                    forward_direct_power_file_path += "_DFS" + (i + 1);
                    forward_direct_power_file_path += Defaults.USCORE;
                    forward_direct_power_file_path += map_ramp_direction_CString;
                    forward_direct_power_file_path += Defaults.CSV;

                    _map_forward_direct_power[i].writeToFile(
                        forward_direct_power_file_path,
                        DSDBR01.Instance._DSDBR01_OMDC_overwrite_existing_files);
                }
                for (int i = 0; i < _map_reverse_direct_power.Count; i++)
                {
                    String reverse_direct_power_file_path = results_dir_CString;
                    reverse_direct_power_file_path += Defaults.MATRIX_;
                    reverse_direct_power_file_path += Defaults.MEAS_TYPE_ID_REVERSEPOWER_CSTRING;
                    reverse_direct_power_file_path += Defaults.USCORE;
                    reverse_direct_power_file_path += Defaults.LASER_TYPE_ID_DSDBR01_CSTRING;
                    reverse_direct_power_file_path += Defaults.USCORE;
                    reverse_direct_power_file_path += _laser_id_CString;
                    reverse_direct_power_file_path += Defaults.USCORE;
                    reverse_direct_power_file_path += _date_time_stamp_CString;
                    //char buf[10];
                    //sprintf( buf, "_DFS%d", i+1 );
                    reverse_direct_power_file_path += "_DFS" + (i + 1);
                    reverse_direct_power_file_path += Defaults.USCORE;
                    reverse_direct_power_file_path += map_ramp_direction_CString;
                    reverse_direct_power_file_path += Defaults.CSV;

                    _map_reverse_direct_power[i].writeToFile(
                        reverse_direct_power_file_path,
                        DSDBR01.Instance._DSDBR01_OMDC_overwrite_existing_files);
                }

                if (DSDBR01.Instance._DSDBR01_OMDC_write_filtered_power_maps)
                {
                    for (int i = 0; i < _map_forward_filtered_power.Count; i++)
                    {
                        String forward_filtered_power_file_path = results_dir_CString;
                        forward_filtered_power_file_path += Defaults.MATRIX_;
                        forward_filtered_power_file_path += Defaults.MEAS_TYPE_ID_FILTEREDPOWER_CSTRING;
                        forward_filtered_power_file_path += Defaults.USCORE;
                        forward_filtered_power_file_path += Defaults.LASER_TYPE_ID_DSDBR01_CSTRING;
                        forward_filtered_power_file_path += Defaults.USCORE;
                        forward_filtered_power_file_path += _laser_id_CString;
                        forward_filtered_power_file_path += Defaults.USCORE;
                        forward_filtered_power_file_path += _date_time_stamp_CString;
                        //char buf[10];
                        //sprintf( buf, "_DFS%d", i+1 );
                        forward_filtered_power_file_path += "_DFS" + (i + 1);
                        forward_filtered_power_file_path += Defaults.USCORE;
                        forward_filtered_power_file_path += map_ramp_direction_CString;
                        forward_filtered_power_file_path += Defaults.CSV;

                        _map_forward_filtered_power[i].writeToFile(
                            forward_filtered_power_file_path,
                            DSDBR01.Instance._DSDBR01_OMDC_overwrite_existing_files);
                    }

                    for (int i = 0; i < _map_reverse_filtered_power.Count; i++)
                    {
                        String reverse_filtered_power_file_path = results_dir_CString;
                        reverse_filtered_power_file_path += Defaults.MATRIX_;
                        reverse_filtered_power_file_path += Defaults.MEAS_TYPE_ID_REVERSEFILTEREDPOWER_CSTRING;
                        reverse_filtered_power_file_path += Defaults.USCORE;
                        reverse_filtered_power_file_path += Defaults.LASER_TYPE_ID_DSDBR01_CSTRING;
                        reverse_filtered_power_file_path += Defaults.USCORE;
                        reverse_filtered_power_file_path += _laser_id_CString;
                        reverse_filtered_power_file_path += Defaults.USCORE;
                        reverse_filtered_power_file_path += _date_time_stamp_CString;
                        //char buf[10];
                        //sprintf( buf, "_DFS%d", i+1 );
                        reverse_filtered_power_file_path += "_DFS" + (i + 1);
                        reverse_filtered_power_file_path += Defaults.USCORE;
                        reverse_filtered_power_file_path += map_ramp_direction_CString;
                        reverse_filtered_power_file_path += Defaults.CSV;

                        _map_reverse_filtered_power[i].writeToFile(
                            reverse_filtered_power_file_path,
                            DSDBR01.Instance._DSDBR01_OMDC_overwrite_existing_files);
                    }
                }
                if (DSDBR01.Instance._DSDBR01_OMDC_measure_photodiode_currents)
                {
                    for (int i = 0; i < _map_forward_photodiode1.Count; i++)
                    {
                        String forward_photodiode1_file_path = results_dir_CString;
                        forward_photodiode1_file_path += Defaults.MATRIX_;
                        forward_photodiode1_file_path += Defaults.MEAS_TYPE_ID_FORWARDPD1_CSTRING;
                        forward_photodiode1_file_path += Defaults.USCORE;
                        forward_photodiode1_file_path += Defaults.LASER_TYPE_ID_DSDBR01_CSTRING;
                        forward_photodiode1_file_path += Defaults.USCORE;
                        forward_photodiode1_file_path += _laser_id_CString;
                        forward_photodiode1_file_path += Defaults.USCORE;
                        forward_photodiode1_file_path += _date_time_stamp_CString;
                        //char buf[10];
                        //sprintf( buf, "_DFS%d", i+1 );
                        forward_photodiode1_file_path += "_DFS" + (i + 1);
                        forward_photodiode1_file_path += Defaults.USCORE;
                        forward_photodiode1_file_path += map_ramp_direction_CString;
                        forward_photodiode1_file_path += Defaults.CSV;

                        _map_forward_photodiode1[i].writeToFile(
                            forward_photodiode1_file_path,
                            DSDBR01.Instance._DSDBR01_OMDC_overwrite_existing_files);
                    }
                    for (int i = 0; i < _map_forward_photodiode2.Count; i++)
                    {
                        String forward_photodiode2_file_path = results_dir_CString;
                        forward_photodiode2_file_path += Defaults.MATRIX_;
                        forward_photodiode2_file_path += Defaults.MEAS_TYPE_ID_FORWARDPD2_CSTRING;
                        forward_photodiode2_file_path += Defaults.USCORE;
                        forward_photodiode2_file_path += Defaults.LASER_TYPE_ID_DSDBR01_CSTRING;
                        forward_photodiode2_file_path += Defaults.USCORE;
                        forward_photodiode2_file_path += _laser_id_CString;
                        forward_photodiode2_file_path += Defaults.USCORE;
                        forward_photodiode2_file_path += _date_time_stamp_CString;
                        //char buf[10];
                        //sprintf( buf, "_DFS%d", i+1 );
                        forward_photodiode2_file_path += "_DFS" + (i + 1);
                        forward_photodiode2_file_path += Defaults.USCORE;
                        forward_photodiode2_file_path += map_ramp_direction_CString;
                        forward_photodiode2_file_path += Defaults.CSV;

                        _map_forward_photodiode2[i].writeToFile(
                            forward_photodiode2_file_path,
                            DSDBR01.Instance._DSDBR01_OMDC_overwrite_existing_files);
                    }

                    for (int i = 0; i < _map_reverse_photodiode1.Count; i++)
                    {
                        String reverse_photodiode1_file_path = results_dir_CString;
                        reverse_photodiode1_file_path += Defaults.MATRIX_;
                        reverse_photodiode1_file_path += Defaults.MEAS_TYPE_ID_REVERSEPD1_CSTRING;
                        reverse_photodiode1_file_path += Defaults.USCORE;
                        reverse_photodiode1_file_path += Defaults.LASER_TYPE_ID_DSDBR01_CSTRING;
                        reverse_photodiode1_file_path += Defaults.USCORE;
                        reverse_photodiode1_file_path += _laser_id_CString;
                        reverse_photodiode1_file_path += Defaults.USCORE;
                        reverse_photodiode1_file_path += _date_time_stamp_CString;
                        //char buf[10];
                        //sprintf( buf, "_DFS%d", i+1 );
                        reverse_photodiode1_file_path += "_DFS" + (i + 1);
                        reverse_photodiode1_file_path += Defaults.USCORE;
                        reverse_photodiode1_file_path += map_ramp_direction_CString;
                        reverse_photodiode1_file_path += Defaults.CSV;

                        _map_reverse_photodiode1[i].writeToFile(
                            reverse_photodiode1_file_path,
                            DSDBR01.Instance._DSDBR01_OMDC_overwrite_existing_files);
                    }
                    for (int i = 0; i < _map_reverse_photodiode2.Count; i++)
                    {
                        String reverse_photodiode2_file_path = results_dir_CString;
                        reverse_photodiode2_file_path += Defaults.MATRIX_;
                        reverse_photodiode2_file_path += Defaults.MEAS_TYPE_ID_REVERSEPD2_CSTRING;
                        reverse_photodiode2_file_path += Defaults.USCORE;
                        reverse_photodiode2_file_path += Defaults.LASER_TYPE_ID_DSDBR01_CSTRING;
                        reverse_photodiode2_file_path += Defaults.USCORE;
                        reverse_photodiode2_file_path += _laser_id_CString;
                        reverse_photodiode2_file_path += Defaults.USCORE;
                        reverse_photodiode2_file_path += _date_time_stamp_CString;
                        //char buf[10];
                        //sprintf( buf, "_DFS%d", i+1 );
                        reverse_photodiode2_file_path += "_DFS" + (i + 1);
                        reverse_photodiode2_file_path += Defaults.USCORE;
                        reverse_photodiode2_file_path += map_ramp_direction_CString;
                        reverse_photodiode2_file_path += Defaults.CSV;

                        _map_reverse_photodiode2[i].writeToFile(
                            reverse_photodiode2_file_path,
                            DSDBR01.Instance._DSDBR01_OMDC_overwrite_existing_files);
                    }
                }
            }
            else
            {
                String log_err_msg = "CDSDBROverallModeMap.writeMapsToFile() error writing power ratio map to file";
                CGR_LOG.Write(log_err_msg, LoggingLevels.ERROR_CGR_LOG);
            }

            // map error codes
            if (write_to_file_rval == CLaserModeMap.rcode.vector_size_rows_cols_do_not_match
                || write_Ir_to_file_rval == CCurrentsVector.rcode.vector_size_rows_cols_do_not_match
                || write_If_to_file_rval == CDSDBRFrontCurrents.rcode.vector_size_rows_cols_do_not_match)
            {
                rval = rcode.corrupt_map_not_written_to_file;
                CGR_LOG.Write("CDSDBROverallModeMap.writeMapsToFile() error: corrupt_map_not_written_to_file", LoggingLevels.ERROR_CGR_LOG);
            }
            else
                if (write_to_file_rval == CLaserModeMap.rcode.could_not_open_file
                 || write_Ir_to_file_rval == CCurrentsVector.rcode.could_not_open_file
                 || write_If_to_file_rval == CDSDBRFrontCurrents.rcode.could_not_open_file)
                {
                    rval = rcode.could_not_open_file;
                    CGR_LOG.Write("CDSDBROverallModeMap.writeMapsToFile() error: could_not_open_file", LoggingLevels.ERROR_CGR_LOG);
                }
                else
                    if (write_to_file_rval == CLaserModeMap.rcode.file_already_exists
                     || write_Ir_to_file_rval == CCurrentsVector.rcode.file_already_exists
                     || write_If_to_file_rval == CDSDBRFrontCurrents.rcode.file_already_exists)
                    {
                        CGR_LOG.Write("CDSDBROverallModeMap.writeMapsToFile() error: file_already_exists", LoggingLevels.ERROR_CGR_LOG);
                        rval = rcode.existing_file_not_overwritten;
                    }

            CGR_LOG.Write("CDSDBROverallModeMap.writeMapsToFile() exiting", LoggingLevels.DIAGNOSTIC_CGR_LOG);

            return rval;
        }

        internal void ReadMapsFromFile()
        {
            clear();
            try
            {
                String If_file_path = string.Format("{0}_OverallMap_{1}{2}", Defaults.FRONT_CURRENT_CSTRING,
                        testInfo.ToString(), Defaults.CSV);

                //String If_file_path = "If_OverallMap_DSDBR01_DB113260.008_20090626154310.csv";
                If_file_path = Path.Combine(testInfo.ResultsDir, If_file_path);
                String Ir_file_path = string.Format("{0}_OverallMap_{1}{2}", Defaults.REAR_CURRENT_CSTRING,
                        testInfo.ToString(), Defaults.CSV);
                //String Ir_file_path = "Ir_OverallMap_DSDBR01_DB113260.008_20090626154310.csv";

                Ir_file_path = Path.Combine(testInfo.ResultsDir, Ir_file_path);

                string file_path = string.Format("{1}{2}{0}{3}_DFS", Defaults.USCORE,
                        Defaults.MATRIX_, Defaults.MEAS_TYPE_ID_POWERRATIO_CSTRING, testInfo.ToString());
                //string file_path = "MATRIX_PowerRatio_DSDBR01_DB113260.008_20090626154310_DFS";
                file_path = Path.Combine(testInfo.ResultsDir, file_path);

                // create absolute file path for each of the seven files
                string rampDir = testInfo.Overallmap_ramp_direction.ToString();
                String file_path_DFS1 = file_path + "1" + Defaults.USCORE + rampDir + Defaults.CSV;
                String file_path_DFS2 = file_path + "2" + Defaults.USCORE + rampDir + Defaults.CSV;
                String file_path_DFS3 = file_path + "3" + Defaults.USCORE + rampDir + Defaults.CSV;
                String file_path_DFS4 = file_path + "4" + Defaults.USCORE + rampDir + Defaults.CSV;
                String file_path_DFS5 = file_path + "5" + Defaults.USCORE + rampDir + Defaults.CSV;
                String file_path_DFS6 = file_path + "6" + Defaults.USCORE + rampDir + Defaults.CSV;
                String file_path_DFS7 = file_path + "7" + Defaults.USCORE + rampDir + Defaults.CSV;

                // load data from files
                _vector_Ir.readFromFile(Ir_file_path);

                _vector_If.readFromFile(If_file_path);

                _map_power_ratio_dfs1.readFromFile(file_path_DFS1);
                _map_power_ratio_dfs2.readFromFile(file_path_DFS2);
                _map_power_ratio_dfs3.readFromFile(file_path_DFS3);
                _map_power_ratio_dfs4.readFromFile(file_path_DFS4);
                _map_power_ratio_dfs5.readFromFile(file_path_DFS5);
                _map_power_ratio_dfs6.readFromFile(file_path_DFS6);
                _map_power_ratio_dfs7.readFromFile(file_path_DFS7);

                file_path = string.Format("{1}{2}{0}{3}_DFS", Defaults.USCORE,
                 Defaults.MATRIX_, "Reference", testInfo.ToString());
                file_path = Path.Combine(testInfo.ResultsDir, file_path);
                String file_path_Reference= file_path + "1" + Defaults.USCORE + rampDir + Defaults.CSV;
                    _map_forward_direct_power_dfs1.readFromFile(file_path_Reference);
                    _map_forward_direct_power.Add(_map_forward_direct_power_dfs1);
                file_path_Reference = file_path + "2" + Defaults.USCORE + rampDir + Defaults.CSV;
                    _map_forward_direct_power_dfs2.readFromFile(file_path_Reference);
                    _map_forward_direct_power.Add(_map_forward_direct_power_dfs2);
                file_path_Reference = file_path + "3" + Defaults.USCORE + rampDir + Defaults.CSV;
                    _map_forward_direct_power_dfs3.readFromFile(file_path_Reference);
                    _map_forward_direct_power.Add(_map_forward_direct_power_dfs3);
                file_path_Reference = file_path + "4" + Defaults.USCORE + rampDir + Defaults.CSV;
                    _map_forward_direct_power_dfs4.readFromFile(file_path_Reference);
                    _map_forward_direct_power.Add(_map_forward_direct_power_dfs4);
                file_path_Reference = file_path + "5" + Defaults.USCORE + rampDir + Defaults.CSV;
                    _map_forward_direct_power_dfs5.readFromFile(file_path_Reference);
                    _map_forward_direct_power.Add(_map_forward_direct_power_dfs5);
                file_path_Reference = file_path + "6" + Defaults.USCORE + rampDir + Defaults.CSV;
                    _map_forward_direct_power_dfs6.readFromFile(file_path_Reference);
                    _map_forward_direct_power.Add(_map_forward_direct_power_dfs6);
                file_path_Reference = file_path + "7" + Defaults.USCORE + rampDir + Defaults.CSV;
                _map_forward_direct_power_dfs7.readFromFile(file_path_Reference);
                _map_forward_direct_power.Add(_map_forward_direct_power_dfs7);

                //Load Filter data from files
                file_path = string.Format("{1}{2}{0}{3}_DFS", Defaults.USCORE,
                     Defaults.MATRIX_, "Filter", testInfo.ToString());
                file_path = Path.Combine(testInfo.ResultsDir, file_path);
                file_path_Reference = file_path + "1" + Defaults.USCORE + rampDir + Defaults.CSV;
                _map_forward_filtered_power_dfs1.readFromFile(file_path_Reference);
                _map_forward_filtered_power.Add(_map_forward_filtered_power_dfs1);
                file_path_Reference = file_path + "2" + Defaults.USCORE + rampDir + Defaults.CSV;
                _map_forward_filtered_power_dfs2.readFromFile(file_path_Reference);
                _map_forward_filtered_power.Add(_map_forward_filtered_power_dfs2);
                file_path_Reference = file_path + "3" + Defaults.USCORE + rampDir + Defaults.CSV;
                _map_forward_filtered_power_dfs3.readFromFile(file_path_Reference);
                _map_forward_filtered_power.Add(_map_forward_filtered_power_dfs3);
                file_path_Reference = file_path + "4" + Defaults.USCORE + rampDir + Defaults.CSV;
                _map_forward_filtered_power_dfs4.readFromFile(file_path_Reference);
                _map_forward_filtered_power.Add(_map_forward_filtered_power_dfs4);
                file_path_Reference = file_path + "5" + Defaults.USCORE + rampDir + Defaults.CSV;
                _map_forward_filtered_power_dfs5.readFromFile(file_path_Reference);
                _map_forward_filtered_power.Add(_map_forward_filtered_power_dfs5);
                file_path_Reference = file_path + "6" + Defaults.USCORE + rampDir + Defaults.CSV;
                _map_forward_filtered_power_dfs6.readFromFile(file_path_Reference);
                _map_forward_filtered_power.Add(_map_forward_filtered_power_dfs6);
                file_path_Reference = file_path + "7" + Defaults.USCORE + rampDir + Defaults.CSV;
                _map_forward_filtered_power_dfs7.readFromFile(file_path_Reference);
                _map_forward_filtered_power.Add(_map_forward_filtered_power_dfs7);

                _Ir_ramp = testInfo.Overallmap_ramp_direction == OverallRampDir.R;
                // check maps are of matching size
                if ((_Ir_ramp && ((_vector_If._submap_length[0] != _map_power_ratio_dfs1._rows)
                               || (_vector_If._submap_length[1] != _map_power_ratio_dfs2._rows)
                               || (_vector_If._submap_length[2] != _map_power_ratio_dfs3._rows)
                               || (_vector_If._submap_length[3] != _map_power_ratio_dfs4._rows)
                               || (_vector_If._submap_length[4] != _map_power_ratio_dfs5._rows)
                               || (_vector_If._submap_length[5] != _map_power_ratio_dfs6._rows)
                               || (_vector_If._submap_length[6] != _map_power_ratio_dfs7._rows)))
                || (!_Ir_ramp && ((_vector_If._submap_length[0] != _map_power_ratio_dfs1._cols)
                               || (_vector_If._submap_length[1] != _map_power_ratio_dfs2._cols)
                               || (_vector_If._submap_length[2] != _map_power_ratio_dfs3._cols)
                               || (_vector_If._submap_length[3] != _map_power_ratio_dfs4._cols)
                               || (_vector_If._submap_length[4] != _map_power_ratio_dfs5._cols)
                               || (_vector_If._submap_length[5] != _map_power_ratio_dfs6._cols)
                               || (_vector_If._submap_length[6] != _map_power_ratio_dfs7._cols))))
                {
                    // maps are not of matching size
                    clear();
                    throw new FileLoadException("files_not_matching_sizes");
                }
                else
                {
                    // all 7 maps are the same size
                    // and match the the vector lengths
                    _maps_loaded = true;
                    _rows = _vector_If._total_length;
                    _cols = _vector_Ir._length;

                    _xAxisLength = (int)_cols;
                    _yAxisLength = (int)_rows;

                }
            }
            catch (Exception)
            {
                clear();
                throw;
            }
        }

        internal rcode screen()
        {
            CGR_LOG.Write("CDSDBROverallModeMap.screen() entered", LoggingLevels.DIAGNOSTIC_CGR_LOG);
            rcode rval = rcode.ok;
            rval = findSMBoundaries_new();//purney add RemoveNoisePlotOnBottomLine
            // Step 2: Find middle lines in supermodes
            if (rval == CDSDBROverallModeMap.rcode.ok)
                rval = findSMMiddleLines();

            // Step 3: Filter middle lines in supermodes
            if (rval == CDSDBROverallModeMap.rcode.ok)
                rval = filterSMMiddleLines();

            // Step 4: Check each middle line
            // if not okay, remove the supermode
            _supermodes_removed = (short)(_supermodes.Count);

            if (rval == CDSDBROverallModeMap.rcode.ok)
                rval = checkSMMiddleLines();

            _supermodes_removed -= (short)(_supermodes.Count);
            // Step 5: For each supermode found
            // set its sm_number internally
            // set the row of the midpoint of its middle line
            // and set pointers for percent complete
            if (rval == CDSDBROverallModeMap.rcode.ok)
                ShortCutSM6MiddleLine();

            //////////////////////////////////////////////////////////////////////////////
            setContinuityValues();

            _overallModeMapAnalysis.init();

            _overallModeMapAnalysis.runAnalysis(_supermodes,
                                        _xAxisLength,
                                        _yAxisLength,
                                        _continuityValues,
                                        _supermodes_removed,
                                        _results_dir_CString,
                                        _laser_id_CString,
                                        _date_time_stamp_CString);

            ///////////////////////////////////////////////////////////////////////////
            CGR_LOG.Write("CDSDBROverallModeMap.screen() exiting", LoggingLevels.DIAGNOSTIC_CGR_LOG);

            return rval;
        }

        /// <summary>
        /// save temp data  -added by Jim.Liu@2011.10.28
        /// </summary>
        /// <returns></returns>
        public rcode writeAnalystResultsToFile()
        {
            CGR_LOG.Write("CDSDBROverallModeMap.writeAnalystResultsToFile() entered", LoggingLevels.DIAGNOSTIC_CGR_LOG);

            rcode rval = rcode.ok;

            string pathLines = _results_dir_CString;
            if (!pathLines.EndsWith("\\"))
                pathLines += "\\";
            pathLines += "AnalystLine_";
            pathLines += "lines_OverallMap_";
            pathLines += Defaults.LASER_TYPE_ID_DSDBR01_CSTRING;
            pathLines += Defaults.USCORE;
            pathLines += _laser_id_CString;
            pathLines += Defaults.USCORE;
            pathLines += _date_time_stamp_CString;
            pathLines += Defaults.CSV;


            string pathSubLineFormat = _results_dir_CString;
            if (!pathSubLineFormat.EndsWith("\\"))
                pathSubLineFormat += "\\";
            pathSubLineFormat += "AnalystLine_";
            pathSubLineFormat += Defaults.MIDDLELINE_CURRENT_CSTRING;
            pathSubLineFormat += Defaults.USCORE;
            pathSubLineFormat += Defaults.LASER_TYPE_ID_DSDBR01_CSTRING;
            pathSubLineFormat += Defaults.USCORE;
            pathSubLineFormat += _laser_id_CString;
            pathSubLineFormat += Defaults.USCORE;
            pathSubLineFormat += _date_time_stamp_CString;
            pathSubLineFormat += Defaults.USCORE;
            pathSubLineFormat += "SM";
            pathSubLineFormat += "{0}";
            pathSubLineFormat += Defaults.CSV;

            pointsAnalyst.SaveFiles(pathLines, pathSubLineFormat);


            CGR_LOG.Write("CDSDBROverallModeMap.writeAnalystResultsToFile() exiting", LoggingLevels.DIAGNOSTIC_CGR_LOG);
            return rval;
        }

        /// <summary>
        /// new screen()  -added by Jim.Liu@2011.6.21
        /// </summary>
        /// <returns></returns>
        internal rcode screen_New()
        {
            CGR_LOG.Write("CDSDBROverallModeMap.screen_New() entered", LoggingLevels.DIAGNOSTIC_CGR_LOG);
            rcode rval = rcode.ok;

            //only for check()
            rval = rcode.maps_not_loaded;
            rval = applyMedianFilterToMaps();
            DSDBR01.Instance._DSDBR01_OMBD_max_deltaPr_in_sm = DSDBR01.Instance._DSDBR01_OMBD_max_deltaPr_in_sm * (OverallMapPoweRatioRange.MaxPowerRatio - OverallMapPoweRatioRange.MinPowerRatio); 
            if (rval == rcode.ok)
            {
                // calculate a map of max delta Pr
                // from each point to its immediate neighbours
                rval = calculateMaxDeltaPrMap();
            }

            //===================================================================
            List<CLaserModeMap> CLaserModeMaps = new List<CLaserModeMap>();
            CLaserModeMaps.Add(_map_power_ratio_dfs1_median_filtered);
            CLaserModeMaps.Add(_map_power_ratio_dfs2_median_filtered);
            CLaserModeMaps.Add(_map_power_ratio_dfs3_median_filtered);
            CLaserModeMaps.Add(_map_power_ratio_dfs4_median_filtered);
            CLaserModeMaps.Add(_map_power_ratio_dfs5_median_filtered);
            CLaserModeMaps.Add(_map_power_ratio_dfs6_median_filtered);
            CLaserModeMaps.Add(_map_power_ratio_dfs7_median_filtered);

            pointsAnalyst = new PointsAnalystClass();
            pointsAnalyst.WidthRear = _cols;
            pointsAnalyst.WidthFront = _rows;

            int rangex = 2;
            int rangey = 1;
            int cmpValue = 15;
            double rangeMaxL = 5d;
            double rangeBranch = 5d;
            int minCount = 50;
            int treeLoopNum = 16;
            int combinParam = 15;
            int combinParamZ = 7;
            int dArea = 3;
            int yLimit = 70;
            int distanceLimit = 10;
            int lineWidth = 10;
            int minDistToSide = 2;
            int distToTurnLine = 2;
            int filterSize = 5;
            

            //Jim@2011.12.9
            rangex = DSDBR01.Instance._DSDBR01_OMPA_rangex;
            rangey = DSDBR01.Instance._DSDBR01_OMPA_rangey;
            cmpValue = DSDBR01.Instance._DSDBR01_OMPA_cmpValue;
            rangeMaxL = DSDBR01.Instance._DSDBR01_OMPA_rangeMaxL;
            rangeBranch = DSDBR01.Instance._DSDBR01_OMPA_rangeBranch;
            minCount = DSDBR01.Instance._DSDBR01_OMPA_minCount;
            treeLoopNum = DSDBR01.Instance._DSDBR01_OMPA_treeLoopNum;
            combinParam = DSDBR01.Instance._DSDBR01_OMPA_combinParam;
            combinParamZ = DSDBR01.Instance._DSDBR01_OMPA_combinParamZ;
            dArea = DSDBR01.Instance._DSDBR01_OMPA_dArea;
            yLimit = DSDBR01.Instance._DSDBR01_OMPA_yLimit;
            distanceLimit = DSDBR01.Instance._DSDBR01_OMPA_distanceLimit;
            lineWidth = DSDBR01.Instance._DSDBR01_OMPA_lineWidth;
            minDistToSide = DSDBR01.Instance._DSDBR01_OMPA_minDistToSide;
            distToTurnLine = DSDBR01.Instance._DSDBR01_OMPA_distToTurnLine;
            filterSize = DSDBR01.Instance._DSDBR01_OMPA_filterSize;
            bool useLeft = DSDBR01.Instance._DSDBR01_OMBD_ml_extend_to_corner;
            bool useRight = DSDBR01.Instance._DSDBR01_OMBD_ml_extend_to_corner;
            pointsAnalyst.vector_Ir = _vector_Ir;
            pointsAnalyst.vector_If = _vector_If;
            pointsAnalyst.DoTestAuto(CLaserModeMaps, rangex, rangey, cmpValue, rangeMaxL, rangeBranch, minCount
                                                    , treeLoopNum, combinParam, combinParamZ, dArea
                                                    , yLimit, distanceLimit, lineWidth
                                                    , minDistToSide, distToTurnLine, useLeft, useRight);

            int filter_n = DSDBR01.Instance._DSDBR01_OMBD_ml_moving_ave_filter_n;
            int min_points_width_of_sm = DSDBR01.Instance._DSDBR01_OMBD_min_points_width_of_sm;

            //read output filter   Jim@2011.12.9
            Regex regex = new Regex(@"([a-zA-Z-]+)(\d+)([a-zA-Z-]+)");//.(\d+)");
            string[] wafers = regex.Split(_laser_wafer_id_CString);

            int waferID = -1;
            if (wafers.Length >= 3)
            {
                waferID = int.Parse(wafers[2]);
            }
            Dictionary<int, Dictionary<int, int[]>> outputFilter = new Dictionary<int, Dictionary<int, int[]>>();
            if (File.Exists(DSDBR01.Instance._DSDBR01_OMPA_outputFilterPath))
            {
                CsvReader csvReader = new CsvReader();
                List<string[]> outputFilterStrings = csvReader.ReadFile(DSDBR01.Instance._DSDBR01_OMPA_outputFilterPath);
                for (int i = 1; i < outputFilterStrings.Count; i++)
                {
                    string[] strs = outputFilterStrings[i];
                    bool isTheBatchno = false;
                    if (strs.Length >= 2)
                    {
                        int waferNumberFrom = int.Parse(strs[0]);
                        int waferNumberTo = -1;
                        bool isNumber = int.TryParse(strs[1], out waferNumberTo);
                        if (isNumber)
                        {
                            isTheBatchno = ((waferID >= waferNumberFrom) && (waferID <= waferNumberTo));
                        }
                        else
                        {
                            foreach (char achar in strs[1].ToCharArray())
                            {
                                switch (achar)
                                {
                                    case '=':
                                        isTheBatchno = (isTheBatchno || (waferNumberFrom == waferID));
                                        break;
                                    case '>':
                                        isTheBatchno = (isTheBatchno || (waferID > waferNumberFrom));
                                        break;
                                    case '<':
                                        isTheBatchno = (isTheBatchno || (waferID < waferNumberFrom));
                                        break;
                                }
                            }
                        }
                    }
                    if (isTheBatchno && strs.Length >= 4 && !outputFilter.ContainsKey(int.Parse(strs[2])))
                    {
                        Dictionary<int, int[]> smDict = new Dictionary<int, int[]>();
                        int index = 0;
                        for(int j = 3;j<strs.Length;j++)
                        {
                            string[] pairs = strs[j].Split('~');
                            if (pairs.Length == 2)
                            {
                                int[] region = new int[] {int.Parse(pairs[0]), int.Parse(pairs[1]) };
                                region[0] = region[0] >= 0 ? region[0] : (region[0] + _cols - 1);
                                region[1] = region[1] >= 0 ? region[1] : (region[1] + _cols - 1);
                                smDict.Add(index++, region);
                            }
                        }
                        outputFilter.Add(int.Parse(strs[2]), smDict);
                    }
                }
            }


            pointsAnalyst.AnalystMiddleLine(filterSize, outputFilter, min_points_width_of_sm);//outputFilter);
            writeAnalystResultsToFile();//added by Jim.Liu@2011.10.28
            try
            {
                //fill data to _supermodes
                for (int i = 0; i < pointsAnalyst.lines.Count; i++)
                {
                    List<LineZX> subLines = pointsAnalyst.lines[i];
                    CDSDBRSuperMode superMode = new CDSDBRSuperMode((short)i, pointsAnalyst.WidthRear, pointsAnalyst.WidthFront);
                    for (int j = 0; j < pointsAnalyst.WidthRear; j++)
                    {
                        if (subLines[2].Points.ContainsKey(j))
                        {
                            //if (subLines[0].Points.ContainsKey(j))
                            {
                                superMode._lower_line._points.Add(convertRowCol2LinePoint((int)subLines[0][j], j));
                            }
                            //if (subLines[1].Points.ContainsKey(j))
                            {
                                superMode._upper_line._points.Add(convertRowCol2LinePoint((int)subLines[1][j], j));
                            }

                            MapPoint mapPoint = subLines[2].GetPoint(j);
                            CDSDBROverallModeMapLine.point_type point = new CDSDBROverallModeMapLine.point_type();
                            point.col = mapPoint.X;
                            point.row = mapPoint.Z;
                            point.I_rear = mapPoint.I_rear;
                            point.front_pair_number = (short)mapPoint.Front_pair_number;
                            switch (mapPoint.Front_pair_number)
                            {
                                case 1:
                                    point.I_front_1 = mapPoint.I_frontM;
                                    point.I_front_2 = mapPoint.I_frontN;
                                    break;
                                case 2:
                                    point.I_front_2 = mapPoint.I_frontM;
                                    point.I_front_3 = mapPoint.I_frontN;
                                    break;
                                case 3:
                                    point.I_front_3 = mapPoint.I_frontM;
                                    point.I_front_4 = mapPoint.I_frontN;
                                    break;
                                case 4:
                                    point.I_front_4 = mapPoint.I_frontM;
                                    point.I_front_5 = mapPoint.I_frontN;
                                    break;
                                case 5:
                                    point.I_front_5 = mapPoint.I_frontM;
                                    point.I_front_6 = mapPoint.I_frontN;
                                    break;
                                case 6:
                                    point.I_front_6 = mapPoint.I_frontM;
                                    point.I_front_7 = mapPoint.I_frontN;
                                    break;
                                case 7:
                                    point.I_front_7 = mapPoint.I_frontM;
                                    point.I_front_8 = mapPoint.I_frontN;
                                    break;
                            }
                            superMode._filtered_middle_line._points.Add(point);
                            superMode._middle_line._points.Add(point);
                        }
                    }
                    _supermodes.Add(superMode);
                }

                //===================================================================
            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }

            //CGR_LOG.Write("CDSDBROverallModeMap.screen_New() exiting", LoggingLevels.DIAGNOSTIC_CGR_LOG);
            //return rval;



            //old code =======================================================

            //rval = findSMBoundaries_new();//purney add RemoveNoisePlotOnBottomLine
            // Step 2: Find middle lines in supermodes
            //if (rval == CDSDBROverallModeMap.rcode.ok)
            //    rval = findSMMiddleLines();

            // Step 3: Filter middle lines in supermodes
            //if (rval == CDSDBROverallModeMap.rcode.ok)
            //    rval = filterSMMiddleLines();

            // Step 4: Check each middle line
            // if not okay, remove the supermode
            _supermodes_removed = (short)(_supermodes.Count);

            if (rval == CDSDBROverallModeMap.rcode.ok)
                rval = checkSMMiddleLines();

            _supermodes_removed -= (short)(_supermodes.Count);
            // Step 5: For each supermode found
            // set its sm_number internally
            // set the row of the midpoint of its middle line
            // and set pointers for percent complete
            if (rval == CDSDBROverallModeMap.rcode.ok
                && waferID < DSDBR01.Instance._DSDBR01_OMPA_cutLowRearByThisWaferID)//jim @2011.12.14
                ShortCutSM6MiddleLine();

            //////////////////////////////////////////////////////////////////////////////
            setContinuityValues();

            _overallModeMapAnalysis.init();

            _overallModeMapAnalysis.runAnalysis(_supermodes,
                                        _xAxisLength,
                                        _yAxisLength,
                                        _continuityValues,
                                        _supermodes_removed,
                                        _results_dir_CString,
                                        _laser_id_CString,
                                        _date_time_stamp_CString);

            ///////////////////////////////////////////////////////////////////////////
            CGR_LOG.Write("CDSDBROverallModeMap.screen_New() exiting", LoggingLevels.DIAGNOSTIC_CGR_LOG);

            return rval;
        }


        private void ShortCutSM6MiddleLine()
        {
            #region shortcut supermode 6 middle line
            for (int i = 0; i < _supermodes.Count; i++)
            {
                // Cut sm6 low forntpairs, if SM# numbers less than 5, so it is a bad device
                if (_supermodes.Count == 7)
                {
                    if (i == 6)
                    {
                        // Delete the fornt pair switch part of last sm# (sm6 or sm5), at low rear current; For the ILMZ the shit chip;
                        if ((_supermodes[6]._filtered_middle_line._points.Count > 0))
                        {
                            CDSDBROverallModeMapLine.point_type MiddleLinePoint = new CDSDBROverallModeMapLine.point_type();
                            CDSDBROverallModeMapLine.point_type UpperLinePoint = new CDSDBROverallModeMapLine.point_type();
                            CDSDBROverallModeMapLine.point_type LowerLinePoint = new CDSDBROverallModeMapLine.point_type();
                            long delPointCol = 0;

                            for (int ii = 0; ii < _supermodes[6]._filtered_middle_line._points.Count; ii++)
                            {
                                long initPointRow = 126; // The value is (21*6) value, last front pair swith point at middle line;
                                long delPointRow = _supermodes[6]._filtered_middle_line._points[0].row;

                                if (delPointRow < initPointRow)
                                {
                                    //store removed points into a List, for stretch middle line with 5 points after Cut sm6.
                                    _supermodes[6]._filtered_middle_line._points.RemoveAt(0);

                                }
                                else
                                {
                                    if (delPointRow == initPointRow)
                                    {
                                        delPointCol = _supermodes[6]._filtered_middle_line._points[0].col;
                                        MiddleLinePoint = _supermodes[6]._filtered_middle_line._points[0];
                                    }
                                }
                            }
                            for (int ii = 0; ii < _supermodes[6]._lower_line._points.Count; ii++)
                            {
                                if (_supermodes[6]._lower_line._points[0].col < delPointCol)
                                {
                                    _supermodes[6]._lower_line._points.RemoveAt(0);
                                }
                                if (_supermodes[6]._lower_line._points[0].col == delPointCol)
                                {
                                    LowerLinePoint = _supermodes[6]._lower_line._points[0];
                                }
                            }
                            for (int ii = 0; ii < _supermodes[6]._upper_line._points.Count; ii++)
                            {
                                if (_supermodes[6]._upper_line._points[0].col < delPointCol)
                                {
                                    _supermodes[6]._upper_line._points.RemoveAt(0);
                                }
                                if (_supermodes[6]._upper_line._points[0].col == delPointCol)
                                {
                                    UpperLinePoint = _supermodes[6]._upper_line._points[0];
                                }
                            }

                            //stretch supermodes[6] boundary line and middle line with 5 points
                            for (int k = 1; k < 21; k++)
                            {

                                CDSDBROverallModeMapLine.point_type Point = MiddleLinePoint;
                                Point.col = int.Parse(delPointCol.ToString()) - k;
                                if (Point.col >= 0)
                                {
                                    CDSDBROverallModeMapLine.point_type middlePoint = convertRowCol2LinePoint(Point.row, Point.col);
                                    _supermodes[6]._filtered_middle_line._points.Insert(0, middlePoint);
                                }

                                //stretch upper boundary line
                                int row = UpperLinePoint.row;
                                int col = UpperLinePoint.col - k;
                                if (row >= 0 && col >= 0)
                                {
                                    CDSDBROverallModeMapLine.point_type upperPoint = convertRowCol2LinePoint(row, col);
                                    _supermodes[6]._upper_line._points.Insert(0, upperPoint);
                                }
                                //stretch lower boundary line
                                row = LowerLinePoint.row;
                                col = LowerLinePoint.col - k;
                                if (row >= 0 && col >= 0)
                                {
                                    CDSDBROverallModeMapLine.point_type lowerPoint = convertRowCol2LinePoint(row, col);
                                    _supermodes[6]._lower_line._points.Insert(0, lowerPoint);
                                }

                            }

                        }

                    }

                    if (i == 5 )
                    {
                        // Experiment data value if missing sm0, new sm0 middle line first value is more than 5
                        if (_supermodes[5]._filtered_middle_line._points.Count > 0)
                        {
                            long initPointRow = 105; // The value is (21*5) value, last front pair swith point at middle line;
                            double FP_Switch_Irear_mA = 0;
                            for (int ii = 0; ii < _supermodes[5]._filtered_middle_line._points.Count; ii++)
                            {
                                long delPointRow = _supermodes[5]._filtered_middle_line._points[ii].row;
                                if (delPointRow == initPointRow)
                                {                           
                                    FP_Switch_Irear_mA = _supermodes[5]._filtered_middle_line._points[ii].I_rear;
                                    break;
                                }
                            }

                            if (FP_Switch_Irear_mA <= 4)//for Zeo.Chen requirement to select if need to cut SM5 Jack.zhang 2011-09-27
                            {
                                CDSDBROverallModeMapLine.point_type MiddleLinePoint = new CDSDBROverallModeMapLine.point_type();
                                CDSDBROverallModeMapLine.point_type UpperLinePoint = new CDSDBROverallModeMapLine.point_type();
                                CDSDBROverallModeMapLine.point_type LowerLinePoint = new CDSDBROverallModeMapLine.point_type();
                                long delPointCol = 0;

                                for (int ii = 0; ii < _supermodes[5]._filtered_middle_line._points.Count; ii++)
                                {
                                    long delPointRow = _supermodes[5]._filtered_middle_line._points[0].row;
                                    if (delPointRow < initPointRow)
                                    {
                                        //store removed points into a List, for stretch middle line with 5 points after Cut sm6.
                                        _supermodes[5]._filtered_middle_line._points.RemoveAt(0);
                                    }
                                    else
                                    {
                                        if (delPointRow == initPointRow)
                                        {
                                            delPointCol = _supermodes[5]._filtered_middle_line._points[0].col;
                                            MiddleLinePoint = _supermodes[5]._filtered_middle_line._points[0];
                                        }
                                    }
                                }
                                //stretch supermodes[5] boundary line and middle line with 5 points
                                for (int ii = 0; ii < _supermodes[5]._lower_line._points.Count; ii++)
                                {
                                    if (_supermodes[5]._lower_line._points[0].col < delPointCol)
                                    {
                                        _supermodes[5]._lower_line._points.RemoveAt(0);
                                    }
                                    if (_supermodes[5]._lower_line._points[0].col == delPointCol)
                                    {
                                        LowerLinePoint = _supermodes[5]._lower_line._points[0];
                                    }
                                }
                                for (int ii = 0; ii < _supermodes[5]._upper_line._points.Count; ii++)
                                {
                                    if (_supermodes[5]._upper_line._points[0].col < delPointCol)
                                    {
                                        _supermodes[5]._upper_line._points.RemoveAt(0);
                                    }
                                    if (_supermodes[5]._upper_line._points[0].col == delPointCol)
                                    {
                                        UpperLinePoint = _supermodes[5]._upper_line._points[0];
                                    }
                                }

                            //stretch supermodes[6] boundary line and middle line with 5 points
                            for (int k = 1; k < 21; k++)
                            {

                                CDSDBROverallModeMapLine.point_type Point = MiddleLinePoint;
                                Point.col = int.Parse(delPointCol.ToString()) - k;
                                if (Point.col >= 0)
                                {
                                    CDSDBROverallModeMapLine.point_type middlePoint = convertRowCol2LinePoint(Point.row, Point.col);
                                    _supermodes[5]._filtered_middle_line._points.Insert(0, middlePoint);
                                }

                                //stretch upper boundary line
                                int row = UpperLinePoint.row;
                                int col = UpperLinePoint.col - k;
                                if (row >= 0 && col >= 0)
                                {
                                    CDSDBROverallModeMapLine.point_type upperPoint = convertRowCol2LinePoint(row, col);
                                    _supermodes[5]._upper_line._points.Insert(0, upperPoint);
                                }
                                //stretch lower boundary line

                                    row = LowerLinePoint.row;
                                    col = LowerLinePoint.col - k;
                                    if (row >= 0 && col >= 0)
                                    {
                                        CDSDBROverallModeMapLine.point_type lowerPoint = convertRowCol2LinePoint(row, col);
                                        _supermodes[5]._lower_line._points.Insert(0, lowerPoint);
                                    }
                                }

                            }

                        }

                    }
                }

                //_supermodes[i]._p_data_protect = _p_data_protect;
                //_supermodes[i]._p_percent_complete = _p_percent_complete;
                _supermodes[i]._sm_number = (short)i;
                CDSDBROverallModeMapLine.rcode rv_ust = _supermodes[i]._filtered_middle_line.updateSubmapTracking();
                if (rv_ust == CDSDBROverallModeMapLine.rcode.ok)
                {
                    int index_of_midpoint = _supermodes[i]._filtered_middle_line._total_length / 2;
                    _supermodes[i]._row_of_midpoint_of_filtered_middle_line =
                        _supermodes[i]._filtered_middle_line._points[index_of_midpoint].row;

                    String log_msg = "CDSDBROverallModeMap.screen() ";
                    log_msg += string.Format("row of midpoint of filtered middle line of supermode %d is %d",
                        i, _supermodes[i]._row_of_midpoint_of_filtered_middle_line);
                    CGR_LOG.Write(log_msg, LoggingLevels.INFO_CGR_LOG);
                }
                else
                {
                    String log_msg = "CDSDBROverallModeMap.screen() ";
                    log_msg += string.Format("error updating indexing of filtered middle line of supermode %d", i);
                    CGR_LOG.Write(log_msg, LoggingLevels.ERROR_CGR_LOG);
                }
            }
            #endregion shortcut supermode 6


        }


        void setContinuityValues()
        {
            _continuityValues.Clear();
            for (int sm = 0; sm < _supermodes.Count; sm++)
            {
                //std.vector<CDSDBROverallModeMapLine.point_type>* p_filtered_middle_points = &(_supermodes[sm]._filtered_middle_line._points);
                List<CDSDBROverallModeMapLine.point_type> p_filtered_middle_points = _supermodes[sm]._filtered_middle_line._points;

                if (!(p_filtered_middle_points.Count == 0))
                {
                    for (int i = 0; i < p_filtered_middle_points.Count; i++)
                    {
                        int row_i = p_filtered_middle_points[i].row;
                        int col_i = p_filtered_middle_points[i].col;
                        //double power_ratio = *iPowerRatioPt(row_i, col_i);
                        double power_ratio = iPowerRatioPt(row_i, col_i).Current;

                        _continuityValues.Add(power_ratio);
                    }
                }
            }
        }

        IEnumerator<double> iPowerRatioPt(int row, int col)
        {
            IEnumerator<double> i = null;
            // check point is within map
            if (row >= 0 && row < _rows && col >= 0 && col < _cols)
            {
                int row_in_submap = _vector_If.index_in_submap(row); //row % _vector_If._rows;
                int col_in_submap = col;

                int index = 0;
                if (_Ir_ramp) // Ir ramped
                {
                    index = row_in_submap * _cols + col_in_submap;
                }
                else // If ramped
                {
                    index = row_in_submap + col_in_submap * (_vector_If._submap_length[_vector_If.pair_number(row) - 1]);
                }

                //switch( 1 + (int)(row / _vector_If._rows) )
                switch (_vector_If.pair_number(row))
                {
                    case 1:
                        i = _map_power_ratio_dfs1._map.GetEnumerator();
                        //i += index;
                        break;
                    case 2:
                        i = _map_power_ratio_dfs2._map.GetEnumerator();
                        //i += index;
                        break;
                    case 3:
                        i = _map_power_ratio_dfs3._map.GetEnumerator();
                        //i += index;
                        break;
                    case 4:
                        i = _map_power_ratio_dfs4._map.GetEnumerator();
                        //i += index;
                        break;
                    case 5:
                        i = _map_power_ratio_dfs5._map.GetEnumerator();
                        //i += index;
                        break;
                    case 6:
                        i = _map_power_ratio_dfs6._map.GetEnumerator();
                        //i += index;
                        break;
                    case 7:
                        i = _map_power_ratio_dfs7._map.GetEnumerator();
                        //i += index;
                        break;
                    default:
                        throw new Exception("pair_number more than 7!");
                }

                for (int j = 0; j <= index; j++)
                {
                    i.MoveNext();
                }
            }
            return i;
        }

        rcode checkSMMiddleLines()
        {
            CGR_LOG.Write("CDSDBROverallModeMap.checkSMMiddleLines() entered", LoggingLevels.DIAGNOSTIC_CGR_LOG);

            rcode rval = rcode.ok;

            // Check each middle line to ensure
            // it is in one supermode, that it's longer than a min length
            // and that it doesn't go back and forth across a change in front section pairs
            // if not, remove the supermode

            for (int sm = 0; sm < _supermodes.Count; sm++)
            {
                if (_supermodes[sm]._filtered_middle_line._points.Count < DSDBR01.Instance._DSDBR01_OMBD_ml_min_length)
                {
                    // middle line is too short
                    // remove it
                    _supermodes[sm].clear();

                    String log_msg = "CDSDBROverallModeMap.checkSMMiddleLines() ";
                    log_msg += string.Format("supermode %d filtered middle line of length %d points is too short, removing supermode",
                        sm, _supermodes[sm]._filtered_middle_line._points.Count);
                    CGR_LOG.Write(log_msg, LoggingLevels.WARNING_CGR_LOG);

                    //std.vector<CDSDBRSuperMode>.iterator i_sm = _supermodes.begin();
                    int i_sm = 0;
                    i_sm += sm;
                    _supermodes.RemoveAt(i_sm);
                    sm--;
                }
                else
                {
                    // check front pair numbers don't cross back and forth
                    bool crossing_back_and_forth = false;
                    List<int> found_front_pair_numbers = new List<int>();
                    int last_pt_front_pair_number = 0;
                    for (int i = 0; i < _supermodes[sm]._filtered_middle_line._points.Count; i++)
                    {
                        int front_pair_number = _supermodes[sm]._filtered_middle_line._points[i].front_pair_number;

                        if (front_pair_number != last_pt_front_pair_number)
                        {
                            for (int j = 0; j < found_front_pair_numbers.Count; j++)
                            {
                                if (front_pair_number == found_front_pair_numbers[j])
                                {
                                    crossing_back_and_forth = true;
                                    break;
                                }
                            }
                            if (crossing_back_and_forth)
                            {
                                // new front pair number has been seen earlier
                                //gdm300306 'clear()' moved to after calls for row & col, was causing mem-viol exception
                                int row = _supermodes[sm]._filtered_middle_line._points[i].row;
                                int col = _supermodes[sm]._filtered_middle_line._points[i].col;

                                string log_msg = "CDSDBROverallModeMap.checkSMMiddleLines() ";
                                log_msg += string.Format("supermode %d front pair number %d at row=%d col=%d was visited earlier, removing supermode",
                                    sm, front_pair_number, row, col);
                                CGR_LOG.Write(log_msg, LoggingLevels.WARNING_CGR_LOG);

                                //gdm300306 from above
                                _supermodes[sm].clear();

                                //std.vector<CDSDBRSuperMode>.iterator i_sm = _supermodes.begin();
                                int i_sm = 0;
                                i_sm += sm;
                                //_supermodes.erase( i_sm );
                                _supermodes.RemoveAt(i_sm);
                                sm--;

                                break;
                            }
                            found_front_pair_numbers.Add(front_pair_number);
                        }

                        last_pt_front_pair_number = front_pair_number;
                    }

                    if (!crossing_back_and_forth)
                    {
                        // get each maxDeltaPr values along middle line
                        for (int i = 0; i < _supermodes[sm]._filtered_middle_line._points.Count; i++)
                        {

                            int row = _supermodes[sm]._filtered_middle_line._points[i].row;
                            int col = _supermodes[sm]._filtered_middle_line._points[i].col;

                            //double maxDeltaPr = *iMaxDeltaPrPt( row, col );
                            double maxDeltaPr = iMaxDeltaPrPt(row, col).Current;

                            if (maxDeltaPr > DSDBR01.Instance._DSDBR01_OMBD_max_deltaPr_in_sm)
                            {
                                // threshold exceeded
                                // middle line is not in one supermode
                                // remove it
                                _supermodes[sm].clear();

                                String log_msg = "CDSDBROverallModeMap.checkSMMiddleLines() ";
                                log_msg += string.Format("supermode %d filtered middle line maxDeltaPr %g exceeds limit %g at row=%d col=%d, removing supermode",
                                    sm, maxDeltaPr, DSDBR01.Instance._DSDBR01_OMBD_max_deltaPr_in_sm, row, col);
                                CGR_LOG.Write(log_msg, LoggingLevels.WARNING_CGR_LOG);

                                //std.vector<CDSDBRSuperMode>.iterator i_sm = _supermodes.begin();
                                int i_sm = 0;
                                i_sm += sm;
                                //_supermodes.erase( i_sm );
                                _supermodes.RemoveAt(i_sm);
                                sm--;

                                break;
                            }
                        }
                    }
                }
            }

            CGR_LOG.Write("CDSDBROverallModeMap.checkSMMiddleLines() exiting", LoggingLevels.DIAGNOSTIC_CGR_LOG);

            return rval;
        }

        rcode filterSMMiddleLines()
        {
            CGR_LOG.Write("CDSDBROverallModeMap.filterSMMiddleLines() entered", LoggingLevels.DIAGNOSTIC_CGR_LOG);
            rcode rval = rcode.ok;

            int filter_n = DSDBR01.Instance._DSDBR01_OMBD_ml_moving_ave_filter_n;
            for (int sm_number = 0; sm_number < _supermodes.Count; sm_number++)
            {
                // Perform averaging in the If direction only
                for (int i = 0; i < _supermodes[sm_number]._middle_line._points.Count; i++)
                {
                    int window_size = 0;
                    int sum_of_rows_of_windowed_points = 0;
                    for (int j = i - filter_n; j <= i + filter_n; j++)
                    {
                        if (j >= 0 && j < _supermodes[sm_number]._middle_line._points.Count)
                        {
                            // sum rows in window, without exceeding start or end
                            window_size++;
                            sum_of_rows_of_windowed_points += _supermodes[sm_number]._middle_line._points[j].row;
                        }
                    }

                    // the structure CDSDBROverallModeMapLine.point_type
                    // contains both the row, col position on a map and
                    // the currents at that point.
                    // For the average, we need to interpolate the If current
                    // while finding the nearest row (just so it can be plotted on a map)

                    CDSDBROverallModeMapLine.point_type averaged_pt
                        = interpolateLinePoint(
                            (double)sum_of_rows_of_windowed_points / (double)window_size,
                            (double)(_supermodes[sm_number]._middle_line._points[i].col));

                    // Add averaged point
                    _supermodes[sm_number]._filtered_middle_line._points.Add(averaged_pt);
                }
                String log_msg = "CDSDBROverallModeMap.findSMMiddleLines() ";
                log_msg += string.Format("supermode %d filtered middle line of length %d points",
                    sm_number, _supermodes[sm_number]._filtered_middle_line._points.Count);
                CGR_LOG.Write(log_msg, LoggingLevels.INFO_CGR_LOG);
            }
            CGR_LOG.Write("CDSDBROverallModeMap.filterSMMiddleLines() exiting", LoggingLevels.DIAGNOSTIC_CGR_LOG);
            return rval;

        }

        rcode findSMMiddleLines()
        {
            CGR_LOG.Write("CDSDBROverallModeMap.findSMMiddleLines() entered", LoggingLevels.DIAGNOSTIC_CGR_LOG);
            rcode rval = rcode.ok;
            // NOTE: Whereas the supermode boundary lines can effectively be
            // a line zigzagging all over the place up/down, left/right,
            // more restrictions must exist on the middle line.
            // Each point must map directly onto the Ir ramp.
            // Therefore, the size of the middle line is the same as Ir ramp
            // unless the middle line hits the top|bottom instead of lhs|rhs

            int min_dist_of_middle_line_from_boundary = DSDBR01.Instance._DSDBR01_OMBD_min_points_width_of_sm / 2;

            for (short sm_number = 0; sm_number < (short)_supermodes.Count; sm_number++)
            {
                // clear any middle line data
                _supermodes[sm_number]._middle_line.clear();
                bool middle_point_found_at_col_zero = false;
                bool middle_point_found_at_last_col = false;

                // if right-most column covered is not last column in map
                //   find midpoint on rhs-top border
                //   plot line from right-most point found to midpoint
                //   add points along line to middle line
                // if left-most column covered is not first column in map
                //   find midpoint on lhs_bottom border
                //   plot line from left-most point found to lhs_bottom border
                //   add points along line to middle line

                for (int i = 0; i < _cols; i++)
                {
                    // for each column
                    int highest_row_at_i_on_lower_line = -1;
                    int index_of_highest_row_at_i_on_lower_line = -1;

                    // find highest point on lower line at column i
                    for (int j = 0; j < _supermodes[sm_number]._lower_line._points.Count; j++)
                    {
                        if (_supermodes[sm_number]._lower_line._points[j].col == i
                         && _supermodes[sm_number]._lower_line._points[j].row > highest_row_at_i_on_lower_line)
                        {
                            index_of_highest_row_at_i_on_lower_line = j;
                            highest_row_at_i_on_lower_line = _supermodes[sm_number]._lower_line._points[j].row;
                        }
                    }

                    int lowest_row_at_i_on_upper_line = _rows;
                    int index_of_lowest_row_at_i_on_upper_line = -1;

                    // find lowest point on upper line at column i
                    for (int j = 0; j < _supermodes[sm_number]._upper_line._points.Count; j++)
                    {
                        if (_supermodes[sm_number]._upper_line._points[j].col == i
                         && _supermodes[sm_number]._upper_line._points[j].row < lowest_row_at_i_on_upper_line)
                        {
                            index_of_lowest_row_at_i_on_upper_line = j;
                            lowest_row_at_i_on_upper_line = _supermodes[sm_number]._upper_line._points[j].row;
                        }
                    }

                    if (index_of_highest_row_at_i_on_lower_line != -1
                        && index_of_lowest_row_at_i_on_upper_line != -1)
                    {
                        if (i == 0) middle_point_found_at_col_zero = true;
                        if (i == _cols - 1) middle_point_found_at_last_col = true;

                        // if both found, check minimum separation distance

                        if (lowest_row_at_i_on_upper_line - highest_row_at_i_on_lower_line > DSDBR01.Instance._DSDBR01_OMBD_min_points_width_of_sm)
                        {
                            // find vertical midpoint

                            int row_of_midpoint = highest_row_at_i_on_lower_line +
                                (lowest_row_at_i_on_upper_line - highest_row_at_i_on_lower_line) / 2;

                            //     add to middle line
                            _supermodes[sm_number]._middle_line._points.Add(
                                convertRowCol2LinePoint(
                                    row_of_midpoint,
                                    i));
                        }
                        else
                        {
                            // supermode min width not met, remove all points
                            _supermodes[sm_number]._middle_line.clear();

                            String log_msg = "CDSDBROverallModeMap.findSMMiddleLines() ";
                            log_msg += string.Format("supermode {0} min width not met, removed all points", sm_number.ToString());
                            CGR_LOG.Write(log_msg, LoggingLevels.WARNING_CGR_LOG);

                            break;
                        }
                    }
                }

                if (!middle_point_found_at_col_zero
                    && !(_supermodes[sm_number]._middle_line._points.Count == 0))
                {
                    // need to fill in the start of the middle line to the lhs-bottom border
                    // find middle start point
                    // calculate points on straight line fit first known middle point
                    // and add to start of middle line

                    //std.vector<CDSDBROverallModeMapLine.point_type>.iterator lower_border_pt =
                    //_supermodes[sm_number]._lower_line._points.end();
                    int lower_border_pt = _supermodes[sm_number]._lower_line._points.Count;
                    for (int j = 0; j < _supermodes[sm_number]._lower_line._points.Count; j++)
                    {
                        if ((_supermodes[sm_number]._lower_line._points[j].col == 0
                          || _supermodes[sm_number]._lower_line._points[j].row == 0)
                         && lower_border_pt == _supermodes[sm_number]._lower_line._points.Count)
                        {
                            // found point on lhs-bottom border
                            //lower_border_pt = _supermodes[sm_number]._lower_line._points.begin();
                            lower_border_pt = 0;
                            lower_border_pt += j;
                            break;
                        }
                    }

                    //std.vector<CDSDBROverallModeMapLine.point_type>.iterator upper_border_pt = _supermodes[sm_number]._upper_line._points.end();
                    int upper_border_pt = _supermodes[sm_number]._upper_line._points.Count;
                    for (int j = 0; j < _supermodes[sm_number]._upper_line._points.Count; j++)
                    {
                        if ((_supermodes[sm_number]._upper_line._points[j].col == 0
                          || _supermodes[sm_number]._upper_line._points[j].row == 0)
                         && upper_border_pt == _supermodes[sm_number]._upper_line._points.Count)
                        {
                            // found point on lhs-bottom border
                            //upper_border_pt = _supermodes[sm_number]._upper_line._points.begin();
                            upper_border_pt = 0;
                            upper_border_pt += j;
                            break;
                        }
                    }

                    if (lower_border_pt != _supermodes[sm_number]._lower_line._points.Count
                        && upper_border_pt != _supermodes[sm_number]._upper_line._points.Count)
                    {
                        // Have upper and lower border points, find midpoint
                        int lower_dist_from_origin;
                        CDSDBROverallModeMapLine.point_type border_pt_value = _supermodes[sm_number]._lower_line._points[lower_border_pt];
                        if (border_pt_value.col == 0) lower_dist_from_origin = -border_pt_value.row;
                        else lower_dist_from_origin = border_pt_value.col;

                        int upper_dist_from_origin;
                        border_pt_value = _supermodes[sm_number]._upper_line._points[upper_border_pt];
                        if (border_pt_value.col == 0) upper_dist_from_origin = -border_pt_value.row;
                        else upper_dist_from_origin = border_pt_value.col;

                        int border_midpoint_row;
                        int border_midpoint_col;
                        int border_midpoint_dist_from_origin = lower_dist_from_origin - (lower_dist_from_origin - upper_dist_from_origin) / 2;
                        if (border_midpoint_dist_from_origin > 0)
                        {
                            border_midpoint_row = 0;
                            border_midpoint_col = border_midpoint_dist_from_origin;
                        }
                        else
                        {
                            border_midpoint_row = -border_midpoint_dist_from_origin;
                            border_midpoint_col = 0;
                        }

                        //std.vector<CDSDBROverallModeMapLine.point_type>.iterator first_middle_pt = _supermodes[sm_number]._middle_line._points.begin();
                        int first_middle_pt = 0;
                        int first_middle_pt_col = _supermodes[sm_number]._middle_line._points[first_middle_pt].col;
                        int first_middle_pt_row = _supermodes[sm_number]._middle_line._points[first_middle_pt].row;

                        double slope = ((double)(first_middle_pt_row - border_midpoint_row)) / ((double)(first_middle_pt_col - border_midpoint_col));

                        // starting at the first existing point,
                        // insert one new point in to middle line
                        // for each column between the first existing point and the border point
                        for (int i = first_middle_pt_col - 1; i >= border_midpoint_col; i--)
                        {
                            double row_of_midpoint_double = (double)border_midpoint_row + slope * ((double)(i - border_midpoint_col));
                            int row_of_midpoint = (int)row_of_midpoint_double;
                            if (row_of_midpoint_double - (double)row_of_midpoint > 0.5) row_of_midpoint++;
                            if (row_of_midpoint < 0) row_of_midpoint = 0;

                            // add to middle line
                            //_supermodes[sm_number]._middle_line._points.insert(
                            //    _supermodes[sm_number]._middle_line._points.begin(),
                            //    convertRowCol2LinePoint(row_of_midpoint, i));
                            _supermodes[sm_number]._middle_line._points.Insert(
                                0,
                                convertRowCol2LinePoint(row_of_midpoint, i));
                        }

                        // if the upper line's border point is on the lhs border,
                        // and the midpoint is on the bottom border
                        // then continue the middle line along the bottom border to the origin
                        //if ((*upper_border_pt).col == 0
                        if (_supermodes[sm_number]._upper_line._points[upper_border_pt].col == 0
                         && DSDBR01.Instance._DSDBR01_OMBD_ml_extend_to_corner
                         && border_midpoint_row == 0)
                        {
                            for (int i = border_midpoint_col - 1; i >= 0; i--)
                            {
                                // add to middle line
                                //_supermodes[sm_number]._middle_line._points.insert(
                                //    _supermodes[sm_number]._middle_line._points.begin(),
                                //    convertRowCol2LinePoint(0, i));
                                _supermodes[sm_number]._middle_line._points.Insert(0, convertRowCol2LinePoint(0, i));
                            }
                        }
                    }
                }

                if (!middle_point_found_at_last_col
                    && !(_supermodes[sm_number]._middle_line._points.Count == 0))
                {
                    // need to fill in the end of the middle line to the rhs-top border
                    // find middle end point
                    // calculate points on straight line fit from last known middle point
                    // and add to end of middle line

                    //std.vector<CDSDBROverallModeMapLine.point_type>.iterator lower_border_pt = _supermodes[sm_number]._lower_line._points.end();
                    int lower_border_pt = _supermodes[sm_number]._lower_line._points.Count;
                    for (int j = 0; j < _supermodes[sm_number]._lower_line._points.Count; j++)
                    {
                        if ((_supermodes[sm_number]._lower_line._points[j].col == _cols - 1
                          || _supermodes[sm_number]._lower_line._points[j].row == _rows - 1)
                         && lower_border_pt == _supermodes[sm_number]._lower_line._points.Count)
                        {
                            // found point on rhs-top border
                            lower_border_pt = 0;
                            lower_border_pt += j;
                            break;
                        }
                    }

                    //std.vector<CDSDBROverallModeMapLine.point_type>.iterator upper_border_pt = _supermodes[sm_number]._upper_line._points.end();
                    int upper_border_pt = _supermodes[sm_number]._upper_line._points.Count;
                    for (int j = 0; j < _supermodes[sm_number]._upper_line._points.Count; j++)
                    {
                        if ((_supermodes[sm_number]._upper_line._points[j].col == _cols - 1
                          || _supermodes[sm_number]._upper_line._points[j].row == _rows - 1)
                         && upper_border_pt == _supermodes[sm_number]._upper_line._points.Count)
                        {
                            // found point on rhs-top border
                            upper_border_pt = 0;
                            upper_border_pt += j;
                            break;
                        }
                    }

                    if (lower_border_pt != _supermodes[sm_number]._lower_line._points.Count
                        && upper_border_pt != _supermodes[sm_number]._upper_line._points.Count)
                    {
                        // Have upper and lower border points, find midpoint
                        int lower_dist_from_toprightcorner = 0;
                        CDSDBROverallModeMapLine.point_type border_pt_value = _supermodes[sm_number]._lower_line._points[lower_border_pt];
                        if (border_pt_value.col == _cols - 1) lower_dist_from_toprightcorner = (_rows - 1) - border_pt_value.row;
                        else lower_dist_from_toprightcorner = border_pt_value.col - (_cols - 1);

                        int upper_dist_from_toprightcorner = 0;
                        border_pt_value = _supermodes[sm_number]._upper_line._points[upper_border_pt];
                        if (border_pt_value.col == _cols - 1) upper_dist_from_toprightcorner = (_rows - 1) - border_pt_value.row;
                        else upper_dist_from_toprightcorner = border_pt_value.col - (_cols - 1);

                        int border_midpoint_row = 0;
                        int border_midpoint_col = 0;
                        int border_midpoint_dist_from_toprightcorner = lower_dist_from_toprightcorner - (lower_dist_from_toprightcorner - upper_dist_from_toprightcorner) / 2;
                        if (border_midpoint_dist_from_toprightcorner > 0)
                        {
                            border_midpoint_row = (_rows - 1) - border_midpoint_dist_from_toprightcorner;
                            border_midpoint_col = _cols - 1;
                        }
                        else
                        {
                            border_midpoint_row = _rows - 1;
                            border_midpoint_col = (_cols - 1) + border_midpoint_dist_from_toprightcorner;
                        }

                        //std.vector<CDSDBROverallModeMapLine.point_type>.iterator last_middle_pt = _supermodes[sm_number]._middle_line._points.end();
                        int last_middle_pt = _supermodes[sm_number]._middle_line._points.Count;
                        last_middle_pt--;
                        int last_middle_pt_col = _supermodes[sm_number]._middle_line._points[last_middle_pt].col;
                        int last_middle_pt_row = _supermodes[sm_number]._middle_line._points[last_middle_pt].row;

                        double slope = ((double)(border_midpoint_row - last_middle_pt_row)) / ((double)(border_midpoint_col - last_middle_pt_col));
                        // starting at the end,
                        // insert one new point in to middle line
                        // for each column between the last existing point and the border point
                        for (int i = last_middle_pt_col + 1; i <= border_midpoint_col; i++)
                        {
                            double row_of_midpoint_double = (double)last_middle_pt_row + slope * ((double)(i - last_middle_pt_col));
                            int row_of_midpoint = (int)row_of_midpoint_double;
                            if (row_of_midpoint_double - (double)row_of_midpoint > 0.5) row_of_midpoint++;
                            if (row_of_midpoint > _rows - 1) row_of_midpoint = _rows - 1;

                            // add to middle line
                            _supermodes[sm_number]._middle_line._points.Add(convertRowCol2LinePoint(row_of_midpoint, i));
                        }

                        // if the lower line's border point is on the rhs border,
                        // and the midpoint is on the top border
                        // then continue the middle line along the top border to the rhs border
                        if (_supermodes[sm_number]._lower_line._points[lower_border_pt].col == _cols - 1
                         && DSDBR01.Instance._DSDBR01_OMBD_ml_extend_to_corner
                         && border_midpoint_row == _rows - 1)
                        {
                            for (int i = border_midpoint_col + 1; i < _cols; i++)
                            {
                                // add to middle line
                                _supermodes[sm_number]._middle_line._points.Add(convertRowCol2LinePoint(_rows - 1, i));
                            }
                        }
                    }

                    //if ((_supermodes[5]._middle_line._points.Count > 0) || (_supermodes[6]._middle_line._points.Count > 0))
                    //{
                    //    // Delete the fornt pair switch part of last sm# (sm6 or sm5), at low rear current; For the ILMZ the shit chip;
                    //    if (sm_number == 6)
                    //    {
                    //        for (int i = 0; i < _supermodes[6]._middle_line._points.Count; i++)
                    //        {
                    //            long initPointRow = 126; // The value is (21*6) value, last front pair swith point at middle line;
                    //            long delPointRow = _supermodes[6]._middle_line._points[0].row;
                    //            if (delPointRow < initPointRow)
                    //            {
                    //                _supermodes[6]._middle_line._points.RemoveAt(0);
                    //            }
                    //        }
                    //    }

                    //    // Experiment data value if missing sm0, new sm0 middle line first value is more than 5
                    //    if (sm_number == 5 && _supermodes[0]._middle_line._points[0].row > 5)
                    //    {
                    //        for (int i = 0; i < _supermodes[5]._middle_line._points.Count; i++)
                    //        {
                    //            long initPointRow = 126; // The value is (21*6) value, last front pair swith point at middle line;
                    //            long delPointRow = _supermodes[5]._middle_line._points[0].row;
                    //            if (delPointRow < initPointRow)
                    //            {
                    //                _supermodes[5]._middle_line._points.RemoveAt(0);
                    //            }
                    //        }
                    //    }
                    //}
                }

                string log_msg1 = "CDSDBROverallModeMap.findSMMiddleLines() ";
                log_msg1 += string.Format("found supermode %d middle line of length %d points",
                    sm_number, _supermodes[sm_number]._middle_line._points.Count);
                CGR_LOG.Write(log_msg1, LoggingLevels.INFO_CGR_LOG);

            }
            CGR_LOG.Write("CDSDBROverallModeMap.findSMMiddleLines() exiting", LoggingLevels.DIAGNOSTIC_CGR_LOG);
            return rval;
        }

        rcode findSMBoundaries()
        {
            CGR_LOG.Write("CDSDBROverallModeMap.findSMBoundaries() entered", LoggingLevels.DIAGNOSTIC_CGR_LOG);
            rcode rval = rcode.ok;
            if (!_maps_loaded)
            {
                rval = rcode.maps_not_loaded;
                CGR_LOG.Write("CDSDBROverallModeMap.findSMBoundaries error: maps_not_loaded", LoggingLevels.ERROR_CGR_LOG);
            }
            else
            {
#if OUTPUT_FILE_FOR_DEBUG
                List<CLaserModeMap> rdata = new List<CLaserModeMap>();
                rdata.Add(_map_power_ratio_dfs1);
                rdata.Add(_map_power_ratio_dfs2);
                rdata.Add(_map_power_ratio_dfs3);
                rdata.Add(_map_power_ratio_dfs4);
                rdata.Add(_map_power_ratio_dfs5);
                rdata.Add(_map_power_ratio_dfs6);
                rdata.Add(_map_power_ratio_dfs7);
                Util_WriteDataForDebug.WriteDataToFile(rdata, "Test\0 rawdata.csv");
#endif
                rval = applyMedianFilterToMaps();
#if OUTPUT_FILE_FOR_DEBUG
                //output _map_max_deltaPr for debug
                List<CLaserModeMap> maps = new List<CLaserModeMap>();
                maps.Add(_map_power_ratio_dfs1_median_filtered);
                maps.Add(_map_power_ratio_dfs2_median_filtered);
                maps.Add(_map_power_ratio_dfs3_median_filtered);
                maps.Add(_map_power_ratio_dfs4_median_filtered);
                maps.Add(_map_power_ratio_dfs5_median_filtered);
                maps.Add(_map_power_ratio_dfs6_median_filtered);
                maps.Add(_map_power_ratio_dfs7_median_filtered);
                Util_WriteDataForDebug.WriteDataToFile(maps, "Test\1 applyMedianFilterToMaps.csv");
#endif
            }
            if (rval == rcode.ok)
            {
                // calculate a map of max delta Pr
                // from each point to its immediate neighbours
                rval = calculateMaxDeltaPrMap();

#if OUTPUT_FILE_FOR_DEBUG
                //output _map_max_deltaPr for debug
                Util_WriteDataForDebug.WriteDataToFile(_map_max_deltaPr, "Test\2 calculateMaxDeltaPrMap.csv");
#endif
            }
            String log_msg = null;
            double threshold_maxDeltaPr = DSDBR01.Instance._DSDBR01_OMBD_max_deltaPr_in_sm;
            if (rval == rcode.ok)
            {
                // Start from end of bottom_border_filtered, work back to origin
                // Then move up lhs_border_filtered
                // This should identify all supermodes

                List<double> maxDeltaPr_on_bottom_lhs_border = new List<double>();
                for (int i = 0; i < _cols - 1; i++)
                    maxDeltaPr_on_bottom_lhs_border.Add(iMaxDeltaPrPt(0, _cols - 1 - i).Current);
                for (int i = 0; i < _rows; i++)
                    maxDeltaPr_on_bottom_lhs_border.Add(iMaxDeltaPrPt(i, 0).Current);

                List<int> bottom_lhs_lower_line_start_pts = new List<int>();
                List<int> bottom_lhs_upper_line_start_pts = new List<int>();
                bottom_lhs_lower_line_start_pts.Clear();
                bottom_lhs_upper_line_start_pts.Clear();

                int width_of_sm = 0;
                int start_of_sm = 0;
                int end_of_sm = 0;

                for (int i = 0; i < maxDeltaPr_on_bottom_lhs_border.Count; i++)
                {
                    double maxDeltaPr = maxDeltaPr_on_bottom_lhs_border[i];

                    if (maxDeltaPr > threshold_maxDeltaPr)
                    {
                        if (width_of_sm >= DSDBR01.Instance._DSDBR01_OMBD_min_points_width_of_sm)
                        {
                            end_of_sm = i;

                            // record start of lower line and upper line
                            bottom_lhs_lower_line_start_pts.Add(start_of_sm);
                            bottom_lhs_upper_line_start_pts.Add(end_of_sm);

                        }

                        width_of_sm = 0;
                        start_of_sm = i;
                    }
                    else
                    {
                        width_of_sm++;
                    }
                }

                log_msg = "CDSDBROverallModeMap.findSMBoundaries() ";
                log_msg += String.Format("found {0} possible supermodes on bottom_lhs border", bottom_lhs_lower_line_start_pts.Count.ToString());
                CGR_LOG.Write(log_msg, LoggingLevels.INFO_CGR_LOG);
                if (bottom_lhs_lower_line_start_pts.Count > 0)
                {
                    // Have found the start points of the
                    // upper and lower lines on the lhs-bottom border
                    _supermodes.Clear();
                    for (int sm_number = 0;
                            sm_number < bottom_lhs_lower_line_start_pts.Count; sm_number++)
                    {
                        boundary_point_type lower_line_start_point = new boundary_point_type();
                        boundary_point_type upper_line_start_point = new boundary_point_type();
                        lower_line_start_point.found_in_direction = START_PT;
                        upper_line_start_point.found_in_direction = START_PT;

                        if (bottom_lhs_lower_line_start_pts[sm_number] > _cols - 1)
                        {
                            // first_pt_on_lower_line is on lhs border
                            lower_line_start_point.row = bottom_lhs_lower_line_start_pts[sm_number] - (_cols - 1);
                            lower_line_start_point.col = 0;
                        }
                        else
                        {
                            // first_pt_on_lower_line is on bottom border
                            lower_line_start_point.row = 0;
                            lower_line_start_point.col = (_cols - 1) - bottom_lhs_lower_line_start_pts[sm_number];
                        }
                        if (bottom_lhs_upper_line_start_pts[sm_number] > _cols - 1)
                        {
                            // first_pt_on_lower_line is on lhs border
                            upper_line_start_point.row = bottom_lhs_upper_line_start_pts[sm_number] - (_cols - 1);
                            upper_line_start_point.col = 0;
                        }
                        else
                        {
                            // first_pt_on_lower_line is on bottom border
                            upper_line_start_point.row = 0;
                            upper_line_start_point.col = (_cols - 1) - bottom_lhs_upper_line_start_pts[sm_number];
                        }

                        List<boundary_point_type> lower_boundary_line = new List<boundary_point_type>();
                        List<boundary_point_type> upper_boundary_line = new List<boundary_point_type>();

                        lower_boundary_line.Add(lower_line_start_point);
                        upper_boundary_line.Add(upper_line_start_point);

                        // Next, attempt to find the remaining points on upper and lower lines
                        rcode find_lbline = rcode.ok;
                        rcode find_ubline = rcode.ok;
                        find_lbline = findBoundaryLineOnMaxDeltaPrMap(
                                        lower_boundary_line,
                                        threshold_maxDeltaPr,
                                        direction_type.SW,
                                        false); // false => search clockwise 

                        if (find_lbline == rcode.ok)
                        {
                            log_msg = "CDSDBROverallModeMap.findSMBoundaries() ";
                            log_msg += string.Format("found %d points on lower boundary of supermode %d",
                                lower_boundary_line.Count, sm_number);
                            CGR_LOG.Write(log_msg, LoggingLevels.INFO_CGR_LOG);
                        }
                        else
                        {
                            log_msg = "CDSDBROverallModeMap.findSMBoundaries() ";
                            log_msg += string.Format("error occurred while searching for lower boundary of supermode %d", sm_number);
                            CGR_LOG.Write(log_msg, LoggingLevels.ERROR_CGR_LOG);
                        }

                        find_ubline = findBoundaryLineOnMaxDeltaPrMap(
                                upper_boundary_line,
                                threshold_maxDeltaPr,
                                direction_type.SW,
                                true); // true => search anticlockwise 
                        if (find_ubline == rcode.ok)
                        {
                            log_msg = "CDSDBROverallModeMap.findSMBoundaries() ";
                            log_msg += string.Format("found {0} points on upper boundary of supermode {1}",
                                upper_boundary_line.Count, sm_number);
                            CGR_LOG.Write(log_msg, LoggingLevels.INFO_CGR_LOG);
                        }
                        else
                        {
                            log_msg = "CDSDBROverallModeMap.findSMBoundaries() ";
                            log_msg += string.Format("error occurred while searching for upper boundary of supermode %d", sm_number);
                            CGR_LOG.Write(log_msg, LoggingLevels.ERROR_CGR_LOG);
                        }

                        // write upper and lower lines to file
                        String lower_line_abs_filepath = _results_dir_CString;
                        if (!lower_line_abs_filepath.EndsWith("\\")) lower_line_abs_filepath += "\\";

                        String upper_line_abs_filepath = lower_line_abs_filepath;
                        String map_max_deltaPr_filepath = lower_line_abs_filepath;
                        lower_line_abs_filepath += "lowerline";
                        upper_line_abs_filepath += "upperline";
                        string numbuf = sm_number.ToString() + ".txt";
                        //sprintf( numbuf , "%d.txt", sm_number );
                        lower_line_abs_filepath += numbuf;
                        upper_line_abs_filepath += numbuf;

                        if (find_lbline == rcode.ok
                            && find_ubline == rcode.ok)
                        {
                            CDSDBRSuperMode new_sm = new CDSDBRSuperMode((short)sm_number, _xAxisLength, _yAxisLength);

                            for (int i = 0; i < lower_boundary_line.Count; i++)
                            {
                                new_sm._lower_line._points.Add(
                                    convertRowCol2LinePoint(
                                        lower_boundary_line[i].row,
                                        lower_boundary_line[i].col));
                            }

                            for (int i = 0; i < upper_boundary_line.Count; i++)
                            {
                                new_sm._upper_line._points.Add(
                                    convertRowCol2LinePoint(
                                        upper_boundary_line[i].row,
                                        upper_boundary_line[i].col));
                            }

                            //writeBoundaryLineToFile(
                            //	lower_boundary_line,
                            //	lower_line_abs_filepath,
                            //	true );

                            //writeBoundaryLineToFile(
                            //	upper_boundary_line,
                            //	upper_line_abs_filepath,
                            //	true );

                            _supermodes.Add(new_sm);
                        }
                    }
                }
                else
                {
                    rval = rcode.no_supermodes_found;
                    CGR_LOG.Write("CDSDBROverallModeMap.findSMBoundaries error: no_supermodes_found", LoggingLevels.ERROR_CGR_LOG);
                }
            }

            if (rval == rcode.ok && _supermodes.Count == 0)
            {
                rval = rcode.no_supermodes_found;
                CGR_LOG.Write("CDSDBROverallModeMap.findSMBoundaries error: no_supermodes_found", LoggingLevels.ERROR_CGR_LOG);
            }
            CGR_LOG.Write("CDSDBROverallModeMap.findSMBoundaries() exiting", LoggingLevels.DIAGNOSTIC_CGR_LOG);

            return rval;
        }

        CDSDBROverallModeMapLine.point_type interpolateLinePoint(double row, double col)
        {
            CDSDBROverallModeMapLine.point_type point_on_map;

            // find nearest row
            point_on_map.row = (int)row;
            if (row - (double)((int)row) > 0.5) point_on_map.row++;

            // find nearest col
            point_on_map.col = (int)col;
            if (col - (double)((int)col) > 0.5) point_on_map.col++;

            if (col > (double)((int)col))
            {
                // interpolate Ir linearly
                double Ir_a = _vector_Ir._currents[(int)col];
                double Ir_b = _vector_Ir._currents[((int)col) + 1];

                double Ir_fraction = col - (double)((int)col);

                point_on_map.I_rear = Ir_a + Ir_fraction * (Ir_b - Ir_a);
            }
            else
            {
                // no need to interpolate
                point_on_map.I_rear = _vector_Ir._currents[(int)col];
            }

            point_on_map.I_front_1 = 0;
            point_on_map.I_front_2 = 0;
            point_on_map.I_front_3 = 0;
            point_on_map.I_front_4 = 0;
            point_on_map.I_front_5 = 0;
            point_on_map.I_front_6 = 0;
            point_on_map.I_front_7 = 0;
            point_on_map.I_front_8 = 0;

            // use the nearest point to determine which map to use
            point_on_map.front_pair_number = _vector_If.pair_number(point_on_map.row); // 1 + (short)(point_on_map.row / _vector_If._map.Count);

            short index_of_nearest = (short)(_vector_If.index_in_submap(point_on_map.row)); //(short)(point_on_map.row % _vector_If._map.Count);

            // find out how far the interpolated point is from the nearest point
            double If_fraction = row - (double)(point_on_map.row);
            int top_row = _vector_If._submap_length[point_on_map.front_pair_number - 1];

            if ((index_of_nearest == 0 && If_fraction < 0)
             || (index_of_nearest == (short)top_row && If_fraction > 0))
            {
                // cannot interpolate between maps!
                // use nearest point for currents
                If_fraction = 0;
            }

            double If_value;

            if (If_fraction != 0)
            {
                // need to interpolate
                short index_of_nextpt;
                if (If_fraction > 0) index_of_nextpt = (short)(index_of_nearest + 1);
                else
                {
                    index_of_nextpt = (short)(index_of_nearest - 1);
                    If_fraction = -If_fraction; // ensure positive fraction
                }

                // interpolate If linearly
                double If_a = _vector_If.getNonConstantCurrent(point_on_map.front_pair_number, index_of_nearest);
                double If_b = _vector_If.getNonConstantCurrent(point_on_map.front_pair_number, index_of_nextpt);
                If_value = If_a + If_fraction * (If_b - If_a);
            }
            else
            {
                // no need to interpolate
                If_value = _vector_If.getNonConstantCurrent(point_on_map.front_pair_number, index_of_nearest);
            }

            switch (point_on_map.front_pair_number)
            {
                case 1:
                    point_on_map.I_front_1 = _vector_If.getConstantCurrent(point_on_map.front_pair_number, index_of_nearest);
                    point_on_map.I_front_2 = If_value;
                    break;
                case 2:
                    point_on_map.I_front_2 = _vector_If.getConstantCurrent(point_on_map.front_pair_number, index_of_nearest);
                    point_on_map.I_front_3 = If_value;
                    break;
                case 3:
                    point_on_map.I_front_3 = _vector_If.getConstantCurrent(point_on_map.front_pair_number, index_of_nearest);
                    point_on_map.I_front_4 = If_value;
                    break;
                case 4:
                    point_on_map.I_front_4 = _vector_If.getConstantCurrent(point_on_map.front_pair_number, index_of_nearest);
                    point_on_map.I_front_5 = If_value;
                    break;
                case 5:
                    point_on_map.I_front_5 = _vector_If.getConstantCurrent(point_on_map.front_pair_number, index_of_nearest);
                    point_on_map.I_front_6 = If_value;
                    break;
                case 6:
                    point_on_map.I_front_6 = _vector_If.getConstantCurrent(point_on_map.front_pair_number, index_of_nearest);
                    point_on_map.I_front_7 = If_value;
                    break;
                case 7:
                    point_on_map.I_front_7 = _vector_If.getConstantCurrent(point_on_map.front_pair_number, index_of_nearest);
                    point_on_map.I_front_8 = If_value;
                    break;
            }

            return point_on_map;
        }

        CDSDBROverallModeMapLine.point_type convertRowCol2LinePoint(int row, int col)
        {
            CDSDBROverallModeMapLine.point_type point_on_map = new CDSDBROverallModeMapLine.point_type();

            point_on_map.row = row;
            point_on_map.col = col;

            point_on_map.I_rear = _vector_Ir._currents[col];

            point_on_map.I_front_1 = 0;
            point_on_map.I_front_2 = 0;
            point_on_map.I_front_3 = 0;
            point_on_map.I_front_4 = 0;
            point_on_map.I_front_5 = 0;
            point_on_map.I_front_6 = 0;
            point_on_map.I_front_7 = 0;
            point_on_map.I_front_8 = 0;

            point_on_map.front_pair_number = _vector_If.pair_number(row); // 1 + (short)(row / _vector_If._map.Count);
            short index = (short)(_vector_If.index_in_submap(row)); // (short)(row % _vector_If._map.Count);

            switch (point_on_map.front_pair_number)
            {
                case 1:
                    point_on_map.I_front_1 = _vector_If.getConstantCurrent(point_on_map.front_pair_number, index);
                    point_on_map.I_front_2 = _vector_If.getNonConstantCurrent(point_on_map.front_pair_number, index);
                    break;
                case 2:
                    point_on_map.I_front_2 = _vector_If.getConstantCurrent(point_on_map.front_pair_number, index);
                    point_on_map.I_front_3 = _vector_If.getNonConstantCurrent(point_on_map.front_pair_number, index);
                    break;
                case 3:
                    point_on_map.I_front_3 = _vector_If.getConstantCurrent(point_on_map.front_pair_number, index);
                    point_on_map.I_front_4 = _vector_If.getNonConstantCurrent(point_on_map.front_pair_number, index);
                    break;
                case 4:
                    point_on_map.I_front_4 = _vector_If.getConstantCurrent(point_on_map.front_pair_number, index);
                    point_on_map.I_front_5 = _vector_If.getNonConstantCurrent(point_on_map.front_pair_number, index);
                    break;
                case 5:
                    point_on_map.I_front_5 = _vector_If.getConstantCurrent(point_on_map.front_pair_number, index);
                    point_on_map.I_front_6 = _vector_If.getNonConstantCurrent(point_on_map.front_pair_number, index);
                    break;
                case 6:
                    point_on_map.I_front_6 = _vector_If.getConstantCurrent(point_on_map.front_pair_number, index);
                    point_on_map.I_front_7 = _vector_If.getNonConstantCurrent(point_on_map.front_pair_number, index);
                    break;
                case 7:
                    point_on_map.I_front_7 = _vector_If.getConstantCurrent(point_on_map.front_pair_number, index);
                    point_on_map.I_front_8 = _vector_If.getNonConstantCurrent(point_on_map.front_pair_number, index);
                    break;
            }

            return point_on_map;
        }

        rcode findBoundaryLineOnMaxDeltaPrMap(
            List<boundary_point_type> boundary_line, double threshold_MaxDeltaPr,
            direction_type input_start_direction, bool search_anticlockwise)
        {
            rcode rval = rcode.ok;
            bool found_rhs_top_border = false;
            List<boundary_point_type> excluded_pts = new List<boundary_point_type>();

            // check start point has been inserted into boundary line
            if (boundary_line.Count == 1)
            {
                direction_type start_direction = input_start_direction;
                boundary_point_type boundary_point;
                boundary_point.row = boundary_line[0].row;
                boundary_point.col = boundary_line[0].col;
                boundary_point.found_in_direction = START_PT; // start pt

                boundary_point_type next_pt;
                double next_pt_value = 0;
                next_pt.row = boundary_line[0].row;
                next_pt.col = boundary_line[0].col;
                next_pt.found_in_direction = START_PT; // start pt

                for (int i = 0; i < 2000; i++)
                {
                    rcode rval_nextBdPt = findNextBoundaryPointOnMaxDeltaPrMap(
                        start_direction,
                        search_anticlockwise,
                        ref boundary_point,
                        ref next_pt,
                        ref next_pt_value,
                        excluded_pts,
                        threshold_MaxDeltaPr);
                    if (rval_nextBdPt != rcode.next_pt_not_found)
                    {
                        boundary_point.row = next_pt.row;
                        boundary_point.col = next_pt.col;
                        boundary_point.found_in_direction = next_pt.found_in_direction;

                        int found_in_line_at_i = findInBoundaryLine(boundary_line, boundary_point);
                        //if( found_in_line_at_i != boundary_line.end() )
                        if (found_in_line_at_i != boundary_line.Count)
                        {
                            // point already exists in line,add the last point into exclude range
                            excluded_pts.Add(boundary_line[boundary_line.Count - 1]);
                            // remove the last point 
                            boundary_line.RemoveAt(boundary_line.Count - 1);
                            // find next point from last point in line with a new direction 
                            boundary_point.row = boundary_line[boundary_line.Count - 1].row;
                            boundary_point.col = boundary_line[boundary_line.Count - 1].col;
                            boundary_point.found_in_direction = boundary_line[boundary_line.Count - 1].found_in_direction;
                            if (boundary_point.found_in_direction == START_PT)
                            {
                                //Just Go back to start point turn around
                                found_rhs_top_border = false;
                                break;
                            }
                        }
                        else
                        {
                            if (boundary_point.col == 0)
                            {
                                if ((search_anticlockwise && boundary_point.row > boundary_line[0].row)
                                    || (!search_anticlockwise && boundary_point.row < boundary_line[0].row))
                                {
                                    // have reached wrong border
                                    found_rhs_top_border = false;
                                    break;
                                }
                            }
                            if (boundary_point.row == 0 && boundary_point.col > boundary_line[0].col)
                            {
                                // have reached wrong border
                                found_rhs_top_border = false;
                                break;
                            }
                            // insert new point into boundary line
                            boundary_line.Add(boundary_point);

                            if (boundary_point.row == _rows - 1 || boundary_point.col == _cols - 1)
                            { // have reached end of boundary!
                                found_rhs_top_border = true;
                                break;
                            }

                        }
                        start_direction = opposite(boundary_point.found_in_direction);
                        if (start_direction == opposite(boundary_point.found_in_direction))
                        { // don't start by going back to last point, around on one
                            if (search_anticlockwise) start_direction = (direction_type)((int)start_direction + 1); // +1 for anticlockwise
                            else start_direction = (direction_type)((int)start_direction - 1); // -1 for clockwise
                            if (start_direction > direction_type.NE) start_direction = direction_type.N;
                            if (start_direction < direction_type.N) start_direction = direction_type.NE;
                        }
                    }
                }
            }
            else
            {
                rval = rcode.no_start_point;
                CGR_LOG.Write("CDSDBROverallModeMap.findBoundaryLineOnMaxDeltaPrMap error: no_start_point", LoggingLevels.ERROR_CGR_LOG);
            }
            if (!found_rhs_top_border)
            {
                rval = rcode.rhs_top_border_not_reached;
                CGR_LOG.Write("CDSDBROverallModeMap.findBoundaryLineOnMaxDeltaPrMap error: rhs_top_border_not_reached", LoggingLevels.ERROR_CGR_LOG);
            }
            return rval;
        }

        direction_type opposite(direction_type direction)
        {
            direction_type opposite_direction;

            // find opposite direction 
            switch (direction)
            {
                case direction_type.N:
                    opposite_direction = direction_type.S;
                    break;
                case direction_type.NW:
                    opposite_direction = direction_type.SE;
                    break;
                case direction_type.W:
                    opposite_direction = direction_type.E;
                    break;
                case direction_type.SW:
                    opposite_direction = direction_type.NE;
                    break;
                case direction_type.S:
                    opposite_direction = direction_type.N;
                    break;
                case direction_type.SE:
                    opposite_direction = direction_type.NW;
                    break;
                case direction_type.E:
                    opposite_direction = direction_type.W;
                    break;
                case direction_type.NE:
                    opposite_direction = direction_type.SW;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return opposite_direction;
        }

        int findInBoundaryLine(List<boundary_point_type> boundary_line, boundary_point_type boundary_point)
        {
            //for (i = boundary_line.begin(); i != boundary_line.end(); i++)
            //{
            //    if (boundary_point.row == (*i).row
            //     && boundary_point.col == (*i).col)
            //    {
            //        break;
            //    }
            //}
            int j;
            for (j = 0; j < boundary_line.Count; j++)
            {
                if (boundary_point.row == boundary_line[j].row
                 && boundary_point.col == boundary_line[j].col)
                {
                    break;
                }
            }
            return j;
        }

        rcode findNextBoundaryPointOnMaxDeltaPrMap(
                direction_type start_direction,
                bool find_new_point_anticlockwise,
                ref boundary_point_type current_pt,
                ref boundary_point_type next_pt,
                ref double next_pt_MaxDeltaPr_value,
                List<boundary_point_type> excluded_pts,
                double threshold_MaxDeltaPr)
        {
            rcode rval = rcode.next_pt_not_found;
            double pt_MaxDeltaPr_value = iMaxDeltaPrPt(current_pt.row, current_pt.col).Current;
            int increment = find_new_point_anticlockwise ? 1 : -1;// find new point clockwise
            direction_type i_direction = start_direction;
            int adj_pt_row, adj_pt_col;
            do
            {
                adj_pt_row = 0;
                adj_pt_col = 0;
                double adj_pt_MaxDeltaPr_value = 0;

                rcode rval_calAdjPt = getMaxDeltaPrValueOfAdjacentPt(
                    current_pt.row,
                    current_pt.col,
                    i_direction,
                    ref adj_pt_row,
                    ref adj_pt_col,
                    ref adj_pt_MaxDeltaPr_value);

                if (rval_calAdjPt == rcode.ok)
                {
                    // check if adjacent point has been excluded
                    boundary_point_type check_adj_pt;
                    check_adj_pt.row = adj_pt_row;
                    check_adj_pt.col = adj_pt_col;
                    check_adj_pt.found_in_direction = i_direction;
                    int found_in_excluded_pts_at_i = findInBoundaryLine(excluded_pts, check_adj_pt);


                    if (found_in_excluded_pts_at_i == excluded_pts.Count) // not excluded 
                    {

                        if (adj_pt_MaxDeltaPr_value > threshold_MaxDeltaPr)
                        {
                            // found next point
                            next_pt.row = adj_pt_row;
                            next_pt.col = adj_pt_col;
                            next_pt.found_in_direction = i_direction;
                            next_pt_MaxDeltaPr_value = adj_pt_MaxDeltaPr_value;
                            rval = rcode.ok;
                            break;
                        }
                    }
                    else
                    { // adjacent point marked as excluded, ignore
                    }
                }
                else
                { // must be outside_map_dimensions, ignore
                }

                i_direction = (direction_type)((int)i_direction + increment);
                if (i_direction > direction_type.NE) i_direction = direction_type.N;
                if (i_direction < direction_type.N) i_direction = direction_type.NE;
            }
            while (i_direction != start_direction && !(current_pt.row + adj_pt_row == 0 && current_pt.col - adj_pt_col == increment));//jack.zhang add "&& !(current_pt.row+adj_pt_row==0&&current_pt.col-adj_pt_col==increment)" to break when on Irea-axis 2010-01-06
            //while (i_direction != start_direction);
            return rval;
        }

        rcode getMaxDeltaPrValueOfAdjacentPt(
            int row, int col, direction_type direction,
            ref int adj_pt_row, ref int adj_pt_col, ref double value)
        {
            rcode rval = rcode.ok;
            // check point is within map
            if (isInMap(row, col))
            {
                adj_pt_row = -1;
                adj_pt_col = -1;

                // find adjacent point 
                getRelativeRowCol(
                    row,
                    col,
                    direction,
                    1,
                    ref adj_pt_row,
                    ref adj_pt_col);

                // check adjacent point is within map
                if (isInMap(adj_pt_row, adj_pt_col))
                {
                    //value = *iMaxDeltaPrPt(adj_pt_row, adj_pt_col);
                    value = iMaxDeltaPrPt(adj_pt_row, adj_pt_col).Current;
                }
                else
                {
                    rval = rcode.outside_map_dimensions;
                }
            }
            else
            {
                rval = rcode.outside_map_dimensions;
            }

            return rval;
        }

        void getRelativeRowCol(
            int row_in, int col_in,
            direction_type direction, int distance,
            ref int row_out, ref int col_out)
        {
            // find adjacent point 
            switch (direction)
            {
                case direction_type.N:
                    row_out = row_in + distance;
                    col_out = col_in;
                    break;
                case direction_type.NW:
                    row_out = row_in + distance;
                    col_out = col_in - distance;
                    break;
                case direction_type.W:
                    row_out = row_in;
                    col_out = col_in - distance;
                    break;
                case direction_type.SW:
                    row_out = row_in - distance;
                    col_out = col_in - distance;
                    break;
                case direction_type.S:
                    row_out = row_in - distance;
                    col_out = col_in;
                    break;
                case direction_type.SE:
                    row_out = row_in - distance;
                    col_out = col_in + distance;
                    break;
                case direction_type.E:
                    row_out = row_in;
                    col_out = col_in + distance;
                    break;
                case direction_type.NE:
                    row_out = row_in + distance;
                    col_out = col_in + distance;
                    break;
                default:
                    break;
            }
        }

        bool isInMap(int row, int col)
        {
            return (row >= 0 && row < _rows && col >= 0 && col < _cols);
        }

        rcode calculateMaxDeltaPrMap()
        {
            rcode rval = rcode.ok;
            CGR_LOG.Write("CDSDBROverallModeMap.calculateMaxDeltaPrMap() entered", LoggingLevels.DIAGNOSTIC_CGR_LOG);
            // clear previous maps
            for (int i = 0; i < _map_max_deltaPr.Count; i++)
            {
                _map_max_deltaPr[i].clear();
            }
            _map_max_deltaPr.Clear();

            // create new maps (copy Pr values to start with)
            for (int map_num = 1; map_num <= 7; map_num++)
            {
                CLaserModeMap map_max_deltaPr = new CLaserModeMap();
                int rows = 0;
                int cols = 0;
                double max_deltaPr = 0;
                bool is_empty = false;
                List<double> p_Pr_values = null;


                switch (map_num)
                {
                    case 1:
                        is_empty = _map_power_ratio_dfs1_median_filtered.isEmpty();
                        rows = _map_power_ratio_dfs1_median_filtered._rows;
                        cols = _map_power_ratio_dfs1_median_filtered._cols;
                        p_Pr_values = new List<double>(_map_power_ratio_dfs1_median_filtered._map);
                        break;
                    case 2:
                        is_empty = _map_power_ratio_dfs2_median_filtered.isEmpty();
                        rows = _map_power_ratio_dfs2_median_filtered._rows;
                        cols = _map_power_ratio_dfs2_median_filtered._cols;
                        p_Pr_values = new List<double>(_map_power_ratio_dfs2_median_filtered._map);
                        break;
                    case 3:
                        is_empty = _map_power_ratio_dfs3_median_filtered.isEmpty();
                        rows = _map_power_ratio_dfs3_median_filtered._rows;
                        cols = _map_power_ratio_dfs3_median_filtered._cols;
                        p_Pr_values = new List<double>(_map_power_ratio_dfs3_median_filtered._map);
                        break;
                    case 4:
                        is_empty = _map_power_ratio_dfs4_median_filtered.isEmpty();
                        rows = _map_power_ratio_dfs4_median_filtered._rows;
                        cols = _map_power_ratio_dfs4_median_filtered._cols;
                        p_Pr_values = new List<double>(_map_power_ratio_dfs4_median_filtered._map);
                        break;
                    case 5:
                        is_empty = _map_power_ratio_dfs5_median_filtered.isEmpty();
                        rows = _map_power_ratio_dfs5_median_filtered._rows;
                        cols = _map_power_ratio_dfs5_median_filtered._cols;
                        p_Pr_values = new List<double>(_map_power_ratio_dfs5_median_filtered._map);
                        break;
                    case 6:
                        is_empty = _map_power_ratio_dfs6_median_filtered.isEmpty();
                        rows = _map_power_ratio_dfs6_median_filtered._rows;
                        cols = _map_power_ratio_dfs6_median_filtered._cols;
                        p_Pr_values = new List<double>(_map_power_ratio_dfs6_median_filtered._map);
                        break;
                    case 7:
                        is_empty = _map_power_ratio_dfs7_median_filtered.isEmpty();
                        rows = _map_power_ratio_dfs7_median_filtered._rows;
                        cols = _map_power_ratio_dfs7_median_filtered._cols;
                        p_Pr_values = new List<double>(_map_power_ratio_dfs7_median_filtered._map);
                        break;
                }

                if (!is_empty)
                {
                    CLaserModeMap.rcode lmmrval = CLaserModeMap.rcode.ok;

                    lmmrval = map_max_deltaPr.setMap(p_Pr_values, rows, cols);
                    if (lmmrval != CLaserModeMap.rcode.ok)
                    {
                        rval = rcode.corrupt_map_dimensions;
                        CGR_LOG.Write("CDSDBROverallModeMap.calculateMaxDeltaPrMap error: corrupt_map_dimensions", LoggingLevels.ERROR_CGR_LOG);
                        break;
                    }
                }

                _map_max_deltaPr.Add(map_max_deltaPr);
            }

            // calculate max delta Pr at each point on map
            double lowest_max_deltaPr = 1.1;
            double highest_max_deltaPr = -0.1;
            for (int row = 0; row < _rows; row++)
            {
                for (int col = 0; col < _cols; col++)
                {
                    //IEnumerator<double> ie = iPowerRatioMedianFilteredPt(row, col);
                    //ie.MoveNext();
                    double Pr_at_point = iPowerRatioMedianFilteredPt(row, col).Current;
                    double delta_Pr_at_point = 0;
                    double max_delta_Pr_at_point = 0;

                    if (row > 0 && col < _cols - 1) // row-1, col+1
                    {
                        delta_Pr_at_point = Math.Abs(Pr_at_point - iPowerRatioMedianFilteredPt(row - 1, col + 1).Current);
                        max_delta_Pr_at_point = max_delta_Pr_at_point > delta_Pr_at_point ? max_delta_Pr_at_point : delta_Pr_at_point;
                    }
                    if (row > 0) // row-1, col
                    {
                        delta_Pr_at_point = Math.Abs(Pr_at_point - iPowerRatioMedianFilteredPt(row - 1, col).Current);
                        max_delta_Pr_at_point = max_delta_Pr_at_point > delta_Pr_at_point ? max_delta_Pr_at_point : delta_Pr_at_point;
                    }

                    if (row > 0 && col > 0) // row-1, col-1
                    {
                        delta_Pr_at_point = Math.Abs(Pr_at_point - iPowerRatioMedianFilteredPt(row - 1, col - 1).Current);
                        max_delta_Pr_at_point = max_delta_Pr_at_point > delta_Pr_at_point ? max_delta_Pr_at_point : delta_Pr_at_point;
                    }
                    if (row < _rows - 1 && col < _cols - 1) // row+1, col+1
                    {
                        delta_Pr_at_point = Math.Abs(Pr_at_point - iPowerRatioMedianFilteredPt(row + 1, col + 1).Current);
                        max_delta_Pr_at_point = max_delta_Pr_at_point > delta_Pr_at_point ? max_delta_Pr_at_point : delta_Pr_at_point;
                    }

                    if (row < _rows - 1) // row+1, col
                    {
                        delta_Pr_at_point = Math.Abs(Pr_at_point - iPowerRatioMedianFilteredPt(row + 1, col).Current);
                        max_delta_Pr_at_point = max_delta_Pr_at_point > delta_Pr_at_point ? max_delta_Pr_at_point : delta_Pr_at_point;
                    }

                    if (row < _rows - 1 && col > 0) // row+1, col-1
                    {
                        delta_Pr_at_point = Math.Abs(Pr_at_point - iPowerRatioMedianFilteredPt(row + 1, col - 1).Current);
                        max_delta_Pr_at_point = max_delta_Pr_at_point > delta_Pr_at_point ? max_delta_Pr_at_point : delta_Pr_at_point;
                    }

                    if (col > 0) // row, col-1
                    {
                        delta_Pr_at_point = Math.Abs(Pr_at_point - iPowerRatioMedianFilteredPt(row, col - 1).Current);
                        max_delta_Pr_at_point = max_delta_Pr_at_point > delta_Pr_at_point ? max_delta_Pr_at_point : delta_Pr_at_point;
                    }

                    if (col < _cols - 1) // row, col+1
                    {
                        delta_Pr_at_point = Math.Abs(Pr_at_point - iPowerRatioMedianFilteredPt(row, col + 1).Current);
                        max_delta_Pr_at_point = max_delta_Pr_at_point > delta_Pr_at_point ? max_delta_Pr_at_point : delta_Pr_at_point;
                    }
                    //*iMaxDeltaPrPt(row, col) = max_delta_Pr_at_point;
                    Set_iMaxDeltaPrPt(row, col, max_delta_Pr_at_point);

                    lowest_max_deltaPr = lowest_max_deltaPr > max_delta_Pr_at_point ? max_delta_Pr_at_point : lowest_max_deltaPr;
                    highest_max_deltaPr = highest_max_deltaPr < max_delta_Pr_at_point ? max_delta_Pr_at_point : highest_max_deltaPr;
                }
            }

            CGR_LOG.Write("CDSDBROverallModeMap.calculateMaxDeltaPrMap() exiting", LoggingLevels.DIAGNOSTIC_CGR_LOG);
            return rval;
        }
        void Set_iMaxDeltaPrPt(int row, int col, double value)
        {
            // check point is within map
            if (row >= 0 && row < _rows && col >= 0 && col < _cols)
            {
                int row_in_submap = _vector_If.index_in_submap(row); //row % _vector_If._rows;
                int col_in_submap = col;

                int index = 0;
                if (_Ir_ramp) // Ir ramped
                {
                    index = row_in_submap * _cols + col_in_submap;
                }
                else // If ramped
                {
                    index = row_in_submap + col_in_submap * (_vector_If._submap_length[_vector_If.pair_number(row) - 1]);
                }

                short map_num = _vector_If.pair_number(row);
                _map_max_deltaPr[map_num - 1]._map[index] = value;
            }
        }
        IEnumerator<double> iMaxDeltaPrPt(int row, int col)
        {
            IEnumerator<double> i = null;

            // check point is within map
            if (row >= 0 && row < _rows && col >= 0 && col < _cols)
            {
                int row_in_submap = _vector_If.index_in_submap(row); //row % _vector_If._rows;
                int col_in_submap = col;

                int index = 0;
                if (_Ir_ramp) // Ir ramped
                {
                    index = row_in_submap * _cols + col_in_submap;
                }
                else // If ramped
                {
                    index = row_in_submap + col_in_submap * (_vector_If._submap_length[_vector_If.pair_number(row) - 1]);
                }

                short map_num = _vector_If.pair_number(row);
                //i = _map_max_deltaPr[map_num - 1]._map.begin();
                //i += index;
                i = _map_max_deltaPr[map_num - 1]._map.GetEnumerator();

                for (int j = 0; j <= index; j++)
                {
                    i.MoveNext();
                }
            }
            return i;
        }

        IEnumerator<double> iPowerRatioMedianFilteredPt(int row, int col)
        {
            IEnumerator<double> i = null;
            // check point is within map
            if (row >= 0 && row < _rows && col >= 0 && col < _cols)
            {
                int row_in_submap = _vector_If.index_in_submap(row); //row % _vector_If._rows;
                int col_in_submap = col;

                int index = 0;
                if (_Ir_ramp) // Ir ramped
                {
                    index = row_in_submap * _cols + col_in_submap;
                }
                else // If ramped
                {
                    index = row_in_submap + col_in_submap * (_vector_If._submap_length[_vector_If.pair_number(row) - 1]);
                }

                //switch( 1 + (int)(row / _vector_If._rows) )
                switch ((int)_vector_If.pair_number(row))
                {
                    case 1:
                        i = _map_power_ratio_dfs1_median_filtered._map.GetEnumerator();
                        //i += index;
                        break;
                    case 2:
                        i = _map_power_ratio_dfs2_median_filtered._map.GetEnumerator();
                        //i += index;
                        break;
                    case 3:
                        i = _map_power_ratio_dfs3_median_filtered._map.GetEnumerator();
                        //i += index;
                        break;
                    case 4:
                        i = _map_power_ratio_dfs4_median_filtered._map.GetEnumerator();
                        //i += index;
                        break;
                    case 5:
                        i = _map_power_ratio_dfs5_median_filtered._map.GetEnumerator();
                        //i += index;
                        break;
                    case 6:
                        i = _map_power_ratio_dfs6_median_filtered._map.GetEnumerator();
                        //i += index;
                        break;
                    case 7:
                        i = _map_power_ratio_dfs7_median_filtered._map.GetEnumerator();
                        //i += index;
                        break;
                    default:
                        throw new Exception("pair_number more than 7!");
                }
                for (int j = 0; j <= index; j++)
                {
                    i.MoveNext();
                }
            }

            return i;
        }

        IEnumerator<double> iDirectPowerPt(int row, int col)
        {
            IEnumerator<double> i = null;

            // check point is within map
            if (row >= 0 && row < _rows && col >= 0 && col < _cols)
            {
                long row_in_submap = _vector_If.index_in_submap(row); //row % _vector_If._rows;
                long col_in_submap = col;

                long index = 0;
                if (_Ir_ramp) // Ir ramped
                {
                    index = row_in_submap * _cols + col_in_submap;
                }
                else // If ramped
                {
                    index = row_in_submap + col_in_submap * (_vector_If._submap_length[_vector_If.pair_number(row) - 1]);
                }

                short map_num = _vector_If.pair_number(row);
                //i = _map_forward_direct_power[map_num-1]._map.begin();
                i = _map_forward_direct_power[map_num - 1]._map.GetEnumerator();
                //i += index;
                for (int j = 0; j <= index; j++)
                {
                    i.MoveNext();
                }
            }

            return i;
        }
        IEnumerator<double> iPowerPt(int row, int col, List<CLaserModeMap> _map_forward_power)
        {
            IEnumerator<double> i = null;

            // check point is within map
            if (row >= 0 && row < _rows && col >= 0 && col < _cols)
            {
                long row_in_submap = _vector_If.index_in_submap(row); //row % _vector_If._rows;
                long col_in_submap = col;

                long index = 0;
                if (_Ir_ramp) // Ir ramped
                {
                    index = row_in_submap * _cols + col_in_submap;
                }
                else // If ramped
                {
                    index = row_in_submap + col_in_submap * (_vector_If._submap_length[_vector_If.pair_number(row) - 1]);
                }

                short map_num = _vector_If.pair_number(row);
                i = _map_forward_power[map_num - 1]._map.GetEnumerator();
                //i += index;
                for (int j = 0; j <= index; j++)
                {
                    i.MoveNext();
                }
            }

            return i;
        }


        rcode applyMedianFilterToMaps()
        {
            OverallMapPoweRatioRange.MaxPowerRatio = -999;
            OverallMapPoweRatioRange.MinPowerRatio = 999;
            rcode rval = rcode.ok;

            if (rval == rcode.ok)
                rval = applyMedianFilter(_map_power_ratio_dfs1, _map_power_ratio_dfs1_median_filtered);

            if (rval == rcode.ok)
                rval = applyMedianFilter(_map_power_ratio_dfs2, _map_power_ratio_dfs2_median_filtered);

            if (rval == rcode.ok)
                rval = applyMedianFilter(_map_power_ratio_dfs3, _map_power_ratio_dfs3_median_filtered);

            if (rval == rcode.ok)
                rval = applyMedianFilter(_map_power_ratio_dfs4, _map_power_ratio_dfs4_median_filtered);

            if (rval == rcode.ok)
                rval = applyMedianFilter(_map_power_ratio_dfs5, _map_power_ratio_dfs5_median_filtered);

            if (rval == rcode.ok)
                rval = applyMedianFilter(_map_power_ratio_dfs6, _map_power_ratio_dfs6_median_filtered);

            if (rval == rcode.ok)
                rval = applyMedianFilter(_map_power_ratio_dfs7, _map_power_ratio_dfs7_median_filtered);

            if (rval != rcode.ok)
                CGR_LOG.Write("CDSDBROverallModeMap.applyMedianFilterToMaps error applying median filter to power ratio maps", LoggingLevels.ERROR_CGR_LOG);

            return rval;
        }

        rcode applyMedianFilter(CLaserModeMap map_in, CLaserModeMap filtered_map_out)
        {
            rcode rval = rcode.ok;

            VectorAnalysis.medianFilter(
                map_in._map,
                filtered_map_out._map,
                DSDBR01.Instance._DSDBR01_OMBD_median_filter_rank,
                map_in._cols);

            // update the rows and cols
            filtered_map_out._rows = map_in._rows;
            filtered_map_out._cols = map_in._cols;
            OverallMapPoweRatioRange.MaxPowerRatio = Math.Max(OverallMapPoweRatioRange.MaxPowerRatio, Alg_FindFeature.FindAbsoluteMaxWithSign(filtered_map_out._map.ToArray()));
            OverallMapPoweRatioRange.MinPowerRatio = Math.Min(OverallMapPoweRatioRange.MinPowerRatio, Alg_FindFeature.FindAbsoluteMinWithSign(filtered_map_out._map.ToArray()));
            return rval;
        }

        internal short supermode_count()
        {
            short sm_count = 0;

            for (int sm_number = 0; sm_number < _supermodes.Count; sm_number++)
            {
                if (!(_supermodes[sm_number]._filtered_middle_line._points.Count == 0))
                {
                    sm_count++;
                }
            }

            return sm_count;
        }


        ////////////////////////////////////updated functions from purney.xie.cpp   ///////////////////////////////////////
        ///////////////////////////////////                                         ///////////////////////////////////////
        ////////////////////////////////////                                        ///////////////////////////////////////
        rcode findSMBoundaries_new()
        {
            CGR_LOG.Write("CDSDBROverallModeMap.findSMBoundaries() entered", LoggingLevels.DIAGNOSTIC_CGR_LOG);
            rcode rval = rcode.ok;
            if (!_maps_loaded)
            {
                rval = rcode.maps_not_loaded;
                CGR_LOG.Write("CDSDBROverallModeMap.findSMBoundaries error: maps_not_loaded", LoggingLevels.ERROR_CGR_LOG);
            }
            else
            {
#if OUTPUT_FILE_FOR_DEBUG
                List<CLaserModeMap> rdata = new List<CLaserModeMap>();
                rdata.Add(_map_power_ratio_dfs1);
                rdata.Add(_map_power_ratio_dfs2);
                rdata.Add(_map_power_ratio_dfs3);
                rdata.Add(_map_power_ratio_dfs4);
                rdata.Add(_map_power_ratio_dfs5);
                rdata.Add(_map_power_ratio_dfs6);
                rdata.Add(_map_power_ratio_dfs7);
                Util_WriteDataForDebug.WriteDataToFile(rdata, "Test\0 rawdata.csv");
#endif
                rval = applyMedianFilterToMaps();
#if OUTPUT_FILE_FOR_DEBUG
                //output _map_max_deltaPr for debug
                List<CLaserModeMap> maps = new List<CLaserModeMap>();
                maps.Add(_map_power_ratio_dfs1_median_filtered);
                maps.Add(_map_power_ratio_dfs2_median_filtered);
                maps.Add(_map_power_ratio_dfs3_median_filtered);
                maps.Add(_map_power_ratio_dfs4_median_filtered);
                maps.Add(_map_power_ratio_dfs5_median_filtered);
                maps.Add(_map_power_ratio_dfs6_median_filtered);
                maps.Add(_map_power_ratio_dfs7_median_filtered);
                Util_WriteDataForDebug.WriteDataToFile(maps, "Test\1 applyMedianFilterToMaps.csv");
#endif
            }
            if (rval == rcode.ok)
            {
                // calculate a map of max delta Pr
                // from each point to its immediate neighbours
                rval = calculateMaxDeltaPrMap();

#if OUTPUT_FILE_FOR_DEBUG
                //output _map_max_deltaPr for debug
                Util_WriteDataForDebug.WriteDataToFile(_map_max_deltaPr, "Test\2 calculateMaxDeltaPrMap.csv");
#endif
            }
            String log_msg = null;
            DSDBR01.Instance._DSDBR01_OMBD_max_deltaPr_in_sm = DSDBR01.Instance._DSDBR01_OMBD_max_deltaPr_in_sm*(OverallMapPoweRatioRange.MaxPowerRatio -OverallMapPoweRatioRange.MinPowerRatio); 
            double threshold_maxDeltaPr = DSDBR01.Instance._DSDBR01_OMBD_max_deltaPr_in_sm;

            if (rval == rcode.ok)
            {
                // Start from end of bottom_border_filtered, work back to origin
                // Then move up lhs_border_filtered
                // This should identify all supermodes

                List<double> maxDeltaPr_on_bottom_lhs_border = new List<double>();
                for (int i = 0; i < _cols - 1; i++)
                    maxDeltaPr_on_bottom_lhs_border.Add(iMaxDeltaPrPt(0, _cols - 1 - i).Current);//FIRST FROM Irear axis 
                for (int i = 0; i < _rows; i++)
                    maxDeltaPr_on_bottom_lhs_border.Add(iMaxDeltaPrPt(i, 0).Current);//then from Ifront axis

                List<int> bottom_lhs_lower_line_start_pts = new List<int>();
                List<int> bottom_lhs_upper_line_start_pts = new List<int>();
                bottom_lhs_lower_line_start_pts.Clear();
                bottom_lhs_upper_line_start_pts.Clear();

                int width_of_sm = 0;
                int start_of_sm = 0;
                int end_of_sm = 0;

                #region for remove the noise point around low rear current for hitt mainly. Jack zhang & snow chen 2010-05-03
                int noisePlotHighestThreshold = 0;
                for (int i = 1; i < maxDeltaPr_on_bottom_lhs_border.Count; i++)
                {
                    double maxDeltaPr = maxDeltaPr_on_bottom_lhs_border[i];
                    if (maxDeltaPr > threshold_maxDeltaPr)
                        bottom_lhs_lower_line_start_pts.Add(i);

                }
                for (int i = 0; i < bottom_lhs_lower_line_start_pts.Count; i++)
                {
                    if (bottom_lhs_lower_line_start_pts[i] < (_cols - 1))
                        noisePlotHighestThreshold = 1 * DSDBR01.Instance._DSDBR01_OMBD_max_number_of_points_height_of_noise_plot; //3;
                    else
                        noisePlotHighestThreshold = 3 * DSDBR01.Instance._DSDBR01_OMBD_max_number_of_points_height_of_noise_plot; //9
                    width_of_sm = CalcHightestOfPossibleNoisePlot(bottom_lhs_lower_line_start_pts[i], bottom_lhs_lower_line_start_pts[i], noisePlotHighestThreshold);
                    if (width_of_sm < noisePlotHighestThreshold)
                    {
                        maxDeltaPr_on_bottom_lhs_border[bottom_lhs_lower_line_start_pts[i]] = 0;
                    }
                }
                width_of_sm = 0;
                bottom_lhs_lower_line_start_pts.Clear();

                #endregion for remove the noise point around low rear current for hitt mainly. Jack zhang & snow chen 2010-05-03

                for (int i = 0; i < maxDeltaPr_on_bottom_lhs_border.Count; i++)
                {
                    double maxDeltaPr = maxDeltaPr_on_bottom_lhs_border[i];

                    if (maxDeltaPr > threshold_maxDeltaPr)
                    {
                        if (width_of_sm >= ((i < _cols - 1) ? DSDBR01.Instance._DSDBR01_OMBD_min_points_width_of_sm : 2))
                        {
                            end_of_sm = i;

                            // record start of lower line and upper line
                            bottom_lhs_lower_line_start_pts.Add(start_of_sm);
                            bottom_lhs_upper_line_start_pts.Add(end_of_sm);
                        }

                        width_of_sm = 0;
                        start_of_sm = i;
                    }
                    else
                    {
                        width_of_sm++;
                    }
                }
                //-----------new added from purney.xie.cpp  -------
                CheckAndRedirectPossibleSupermodeStartPoint(ref bottom_lhs_lower_line_start_pts,
                                                            bottom_lhs_upper_line_start_pts,
                                                            maxDeltaPr_on_bottom_lhs_border);
                //------------------end add   ----------------------

                log_msg = "CDSDBROverallModeMap.findSMBoundaries() ";
                log_msg += String.Format("found {0} possible supermodes on bottom_lhs border", bottom_lhs_lower_line_start_pts.Count.ToString());
                CGR_LOG.Write(log_msg, LoggingLevels.INFO_CGR_LOG);
                if (bottom_lhs_lower_line_start_pts.Count > 0)
                {
                    // Have found the start points of the
                    // upper and lower lines on the lhs-bottom border
                    _supermodes.Clear();
                    for (int sm_number = 0;
                            sm_number < bottom_lhs_lower_line_start_pts.Count; sm_number++)
                    {
                        boundary_point_type lower_line_start_point = new boundary_point_type();
                        boundary_point_type upper_line_start_point = new boundary_point_type();
                        lower_line_start_point.found_in_direction = START_PT;
                        upper_line_start_point.found_in_direction = START_PT;

                        if (bottom_lhs_lower_line_start_pts[sm_number] > _cols - 1)
                        {
                            // first_pt_on_lower_line is on lhs border
                            lower_line_start_point.row = bottom_lhs_lower_line_start_pts[sm_number] - (_cols - 1);
                            lower_line_start_point.col = 0;
                        }
                        else
                        {
                            // first_pt_on_lower_line is on bottom border
                            lower_line_start_point.row = 0;
                            lower_line_start_point.col = (_cols - 1) - bottom_lhs_lower_line_start_pts[sm_number];
                        }
                        if (bottom_lhs_upper_line_start_pts[sm_number] > _cols - 1)
                        {
                            // first_pt_on_lower_line is on lhs border
                            upper_line_start_point.row = bottom_lhs_upper_line_start_pts[sm_number] - (_cols - 1);
                            upper_line_start_point.col = 0;
                        }
                        else
                        {
                            // first_pt_on_lower_line is on bottom border
                            upper_line_start_point.row = 0;
                            upper_line_start_point.col = (_cols - 1) - bottom_lhs_upper_line_start_pts[sm_number];
                        }

                        List<boundary_point_type> lower_boundary_line = new List<boundary_point_type>();
                        List<boundary_point_type> upper_boundary_line = new List<boundary_point_type>();

                        lower_boundary_line.Add(lower_line_start_point);
                        upper_boundary_line.Add(upper_line_start_point);

                        // Next, attempt to find the remaining points on upper and lower lines
                        rcode find_lbline = rcode.ok;
                        rcode find_ubline = rcode.ok;
                        direction_type inital_direction = direction_type.E;//Jack Zhang add 2010-01-06
                        if (upper_line_start_point.row == 0)//Jack Zhang add 2010-01-06
                            inital_direction = direction_type.N;//Jack Zhang add 2010-01-06
                        find_ubline = findBoundaryLineOnMaxDeltaPrMap(
                                        upper_boundary_line,
                                        threshold_maxDeltaPr,
                                        inital_direction,
                                        true); // false => search anticlockwise (X axis is Irear_col, Y axis is IFront_row) Jack.zhang 2011.1.6

                        if (find_ubline == rcode.ok)
                        {
                            log_msg = "CDSDBROverallModeMap.findSMBoundaries() ";
                            log_msg += string.Format("found %d points on upper boundary of supermode %d",
                                upper_boundary_line.Count, sm_number);
                            CGR_LOG.Write(log_msg, LoggingLevels.INFO_CGR_LOG);
                        }
                        else
                        {
                            log_msg = "CDSDBROverallModeMap.findSMBoundaries() ";
                            log_msg += string.Format("error occurred while searching for upper boundary of supermode %d", sm_number);
                            CGR_LOG.Write(log_msg, LoggingLevels.ERROR_CGR_LOG);
                        }
                        //change the order of checking line, first check
                        //upper line and then lower line
                        //if the upper line is ok and the lower line needs to be 
                        // relinked, then it probably be the supermode zero

                        inital_direction = direction_type.E;//Jack Zhang add 2010-01-06
                        if (lower_line_start_point.row == 0)//Jack Zhang add 2010-01-06
                            inital_direction = direction_type.N;//Jack Zhang add 2010-01-06
                        find_lbline = findBoundaryLineOnMaxDeltaPrMap(
                                lower_boundary_line,
                                threshold_maxDeltaPr,
                                inital_direction,
                                false); // true => search clockwise (X axis is Irear_col, Y axis is IFront_row) Jack.zhang 2011.1.6

                        //debug value for ttimes
                        int ttimes = 3;
                        // make sure we have successfully linked the upperline 
                        // of this supermode
                        if (find_ubline == rcode.ok & find_lbline == rcode.rhs_top_border_not_reached & lower_boundary_line.Count >= DSDBR01.Instance._DSDBR01_OMBD_ml_min_length)
                        {
                            //yes this line maybe the line that we want to relink
                            // make a copy of current filter map
                            _cacheMap.Clear();
                            for (int i = 0; i < _rows; i++)
                            {
                                for (int j = 0; j < _cols; j++)
                                {
                                    _cacheMap.Add(iMaxDeltaPrPt(i, j).Current);
                                }
                            }
                            rcode link_s = rcode.ok;
                            int trytimes = 0;
                            do
                            {
                                link_s = RelinkJumpPoints(ref lower_boundary_line, true);
                                trytimes++;
                            } while (link_s != rcode.ok & trytimes < ttimes);

                            if (link_s == rcode.ok)
                            {
                                find_lbline = rcode.ok;
                            }

                        }

                        if (find_ubline == rcode.ok)
                        {
                            log_msg = "CDSDBROverallModeMap.findSMBoundaries_new() ";
                            log_msg += string.Format("found {0} points on lower boundary of supermode {1}",
                                lower_boundary_line.Count, sm_number);
                            CGR_LOG.Write(log_msg, LoggingLevels.INFO_CGR_LOG);
                        }
                        else
                        {
                            log_msg = "CDSDBROverallModeMap.findSMBoundaries-new() ";
                            log_msg += string.Format("error occurred while searching for lower boundary of supermode %d", sm_number);
                            CGR_LOG.Write(log_msg, LoggingLevels.ERROR_CGR_LOG);
                        }

                        // write upper and lower lines to file
                        String lower_line_abs_filepath = _results_dir_CString;
                        if (!lower_line_abs_filepath.EndsWith("\\")) lower_line_abs_filepath += "\\";

                        String upper_line_abs_filepath = lower_line_abs_filepath;
                        String map_max_deltaPr_filepath = lower_line_abs_filepath;
                        lower_line_abs_filepath += "lowerline";
                        upper_line_abs_filepath += "upperline";
                        string numbuf = sm_number.ToString() + ".txt";
                        //sprintf( numbuf , "%d.txt", sm_number );
                        lower_line_abs_filepath += numbuf;
                        upper_line_abs_filepath += numbuf;

                        if (find_lbline == rcode.ok
                            && find_ubline == rcode.ok)
                        {
                            CDSDBRSuperMode new_sm = new CDSDBRSuperMode((short)sm_number, _xAxisLength, _yAxisLength);

                            for (int i = 0; i < lower_boundary_line.Count; i++)
                            {
                                new_sm._lower_line._points.Add(
                                    convertRowCol2LinePoint(
                                        lower_boundary_line[i].row,
                                        lower_boundary_line[i].col));
                            }

                            for (int i = 0; i < upper_boundary_line.Count; i++)
                            {
                                new_sm._upper_line._points.Add(
                                    convertRowCol2LinePoint(
                                        upper_boundary_line[i].row,
                                        upper_boundary_line[i].col));
                            }

                            writeBoundaryLineToFile(
                                lower_boundary_line,
                                lower_line_abs_filepath,
                                true);

                            writeBoundaryLineToFile(
                                upper_boundary_line,
                                upper_line_abs_filepath,
                                true);

                            _supermodes.Add(new_sm);
                        }
                    }
                }
                else
                {
                    rval = rcode.no_supermodes_found;
                    CGR_LOG.Write("CDSDBROverallModeMap.findSMBoundaries error: no_supermodes_found", LoggingLevels.ERROR_CGR_LOG);
                }
            }

            if (rval == rcode.ok && _supermodes.Count == 0)
            {
                rval = rcode.no_supermodes_found;
                CGR_LOG.Write("CDSDBROverallModeMap.findSMBoundaries error: no_supermodes_found", LoggingLevels.ERROR_CGR_LOG);
            }
            CGR_LOG.Write("CDSDBROverallModeMap.findSMBoundaries() exiting", LoggingLevels.DIAGNOSTIC_CGR_LOG);

            return rval;
        }

        rcode writeBoundaryLineToFile(
                List<boundary_point_type> boundary_line,
                string abs_filepath,
                bool overwrite)
        {

            rcode rval = rcode.ok;
            int row_count = 0;
            int col_count = 0;

            if (boundary_line.Count == 0)
            {
                rval = rcode.no_data;
                CGR_LOG.Write("CDSDBROverallModeMap::writeBoundaryLineToFile error: no_data", LoggingLevels.ERROR_CGR_LOG);
                return rval;
            }

            // Check if file already exists and overwrite not specified
            if (File.Exists(abs_filepath) & overwrite == false)
            //if( CFile::GetStatus( abs_filepath, status ) && overwrite == false )
            {
                rval = rcode.file_already_exists;
                CGR_LOG.Write("CDSDBROverallModeMap::writeBoundaryLineToFile error: file_already_exists", LoggingLevels.ERROR_CGR_LOG);
                return rval;
            }
            try
            {
                //std::ofstream file_stream( abs_filepath );
                StreamWriter sw = new StreamWriter(abs_filepath);
                sw.WriteLine("Row,Column");
                for (int i = 0; i < boundary_line.Count; i++)
                {
                    //file_stream << boundary_line[i].row;
                    //file_stream << COMMA_CHAR;
                    //file_stream << boundary_line[i].col;
                    //file_stream << std::endl;
                    sw.WriteLine(boundary_line[i].row.ToString() + "," + boundary_line[i].col.ToString());
                }
            }
            catch (Exception)
            {
                rval = rcode.could_not_open_file;
                CGR_LOG.Write("CDSDBROverallModeMap::writeBoundaryLineToFile error: could_not_open_file", LoggingLevels.ERROR_CGR_LOG);
            }
            return rval;
        }



        //------------------------------------------------end add -------------------------------------







        ///////////////////////////////////////  new added functions from purney.xie.cpp  ////////////////////////////////////
        ///////////////////////////////////////                                           ////////////////////////////////////
        ///////////////////////////////////////                                           ////////////////////////////////////
        //-----------------------------------------------------------------
        //----------------------------------------------------------------
        //-----------------------------   new added   ----------------------------------

        rcode getMaxDeltaPrValueOfAdjacentPt2(int row, int col, direction_type direction,
           ref int adj_pt_row, ref int adj_pt_col, ref double value)
        {
            rcode rval = rcode.ok;
            // check point is within map
            if (isInMap(row, col))
            {
                adj_pt_row = -1;
                adj_pt_col = -1;
                // find adjacent point 
                getRelativeRowCol(
                    row,
                    col,
                    direction,
                    1,
                    ref adj_pt_row,
                    ref adj_pt_col);
                // check adjacent point is within map
                if (isInMap(adj_pt_row, adj_pt_col))
                {
                    //value = iMaxDeltaPrPt(adj_pt_row, adj_pt_col).Current; 
                    value = _cacheMap[adj_pt_row * _cols + adj_pt_col];
                }
                else
                {
                    rval = rcode.outside_map_dimensions;
                }
            }
            else
            {
                rval = rcode.outside_map_dimensions;
            }
            return rval;

        }

        bool JudgeContinuity(int col, int length)
        {
            bool retVal = false;
            for (int i = 0; i < length; i++)
            {
                if (iMaxDeltaPrPt(i, col).Current > DSDBR01.Instance._DSDBR01_OMBD_max_deltaPr_in_sm)
                {
                    retVal = true;
                    break;
                }
            }
            return retVal;
        }



        rcode RelinkJumpPoints(ref List<boundary_point_type> line_in, bool upperline)
        {

            //debug value for parameters
            int backwardtimes = 5;

            string log_msg = "CDSDBROverallModeMap::RelinkJumpPoints() entered";
            CGR_LOG.Write(log_msg, LoggingLevels.INFO_CGR_LOG);
            // check whether the data in is bad,
            if (line_in.Count == 0)
            {
                log_msg = "CDSDBROverallModeMap::RelinkJumpPoints() empty line points";
                CGR_LOG.Write(log_msg, LoggingLevels.ERROR_CGR_LOG);
                return rcode.empty_array_to_relink;
            }
            if (line_in.Count < 2)
            {
                log_msg = "CDSDBROverallModeMap::RelinkJumpPoints() no enought line points";
                CGR_LOG.Write(log_msg, LoggingLevels.ERROR_CGR_LOG);
                return rcode.not_enough_point;
            }

            //find start index which we can relink the line, if 
            // the boundary line step backward(to the left or to the bottom) for serveral times
            // then we may think that something bad happening. 
            int traceback = 0;
            // IEnumerator<boundary_point_type> start_index = line_in[0];
            int start_index = 0;
            bool found_startIndex = false;

            //IEnumerator<boundary_point_type> nextPoint = line_in.begin();
            //IEnumerator<boundary_point_type> nextPoint = line_in[0];
            int nextPoint = 0;
            nextPoint++; // points to the second item;
            //IEnumerator<boundary_point_type>  previousPoint = line_in[0];
            int previousPoint = 0;
            //while (nextPoint != line_in[line_in.Count-1])
            while (nextPoint != line_in.Count)
            {
                if (line_in[nextPoint].col < line_in[previousPoint].col ||
                    line_in[nextPoint].row < line_in[previousPoint].row)
                {
                    if (traceback == 0)
                    {
                        start_index = previousPoint;
                    }
                    traceback++;
                }
                else
                {
                    if (line_in[nextPoint].row > line_in[previousPoint].row)
                    {
                        traceback = 0;
                    }
                }
                if (traceback > backwardtimes)
                {
                    found_startIndex = true;
                    break;
                }
                previousPoint++;
                nextPoint++;
            }

            // if we found the startindex, we were trying to assume that the 
            // break gap were jump points.
            if (found_startIndex)
            {
                //try to relink the break line
                //make a copy of current line
                List<boundary_point_type> copyline = new List<boundary_point_type>();

                for (int i = 0; i < backwardtimes; i++)
                {
                    //bug here bob.lv
                    /*for(long j=0;j<i&&start_index!=line_in.end();j++)
                        start_index++;	*/
                    //for sure we do not end with line_in.end()
                    //it fails if we retrieve its value later.
                    long row;
                    if (start_index != line_in.Count)
                        row = line_in[start_index].row;
                    else
                        row = 147;
                    while (start_index != line_in.Count)
                    {
                        if (line_in[start_index].row < row)
                            break;
                        start_index++;

                    }
                    if (start_index == line_in.Count)
                    {
                        start_index--;
                    }
                    rcode found = FindAndSetAssumptionJumpPoints(ref line_in, ref start_index, upperline);
                    if (rcode.ok == found)
                    {
                        //relink this line
                        copyline.Clear();
                        copyline.Add(line_in[0]);
                        double threshold_maxDeltaPr = DSDBR01.Instance._DSDBR01_OMBD_max_deltaPr_in_sm;
                        rcode rval = findBoundaryLineOnMaxDeltaPrMap2(ref copyline, threshold_maxDeltaPr, direction_type.SW, false);

                        if (rval == rcode.ok)
                        {
                            line_in.Clear();
                            for (int j = 0; j < copyline.Count; j++)
                            {
                                line_in.Add(copyline[j]);
                            }
                            copyline.Clear();
                            return rcode.ok;
                        }
                    }//if (CDSDBROverallModeMap::ok == found)
                }////for(int i=0;i<=backwardtimes;i++)
                //after we retry to link line for backwardtimes
                //we still cannot link successfully, so set the boundary line to the last linked line
                //then we can step further!
                line_in.Clear();
                for (int j = 0; j < copyline.Count; j++)
                {
                    line_in.Add(copyline[j]);
                }
                copyline.Clear();
            }
            else
            {
                log_msg = "CDSDBROverallModeMap.RelinkJumpPoints() no start index found";
                CGR_LOG.Write(log_msg, LoggingLevels.INFO_CGR_LOG);
                return rcode.no_start_index_found;
            }

            //no startindex or can not find any assumption points,assume no_start_index_found to return
            return rcode.no_start_index_found;
        }

        rcode FindAndSetAssumptionJumpPoints(
            ref List<boundary_point_type> line_in,
            //ref IEnumerator<boundary_point_type> start_index,
            ref int start_index,
            bool upperline)
        {
            //debug value
            long subsetSize = 20;
            int numAssumptionPionts = DSDBR01.Instance._DSDBR01_OMBD_assumption_points;
            int maxColinLine = -1;
            List<double> xdata = new List<double>();
            List<double> ydata = new List<double>();

            // IEnumerator<boundary_point_type> item = line_in[0];
            int item = 0;
            //while(item != line_in[line_in.Count-1])
            while (item != line_in.Count)
            {
                //if(item.col > maxColinLine)
                if (line_in[item].col > maxColinLine)
                    maxColinLine = line_in[item].col;
                item++;
            }
            item = start_index;
            //for (int j=0;item != line_in.begin()&& j<subsetSize;item=item-1,j++)
            for (int j = 0; item != 0 & j < subsetSize; item = item - 1, j++)
            {
                xdata.Add(line_in[item].col);
                ydata.Add(line_in[item].row);
            }
            double threshold_maxDeltaPr = DSDBR01.Instance._DSDBR01_OMBD_max_deltaPr_in_sm;
            double slope = 0;
            double intercept = 0;
            VectorAnalysis.linearFit(xdata, ydata, out slope, out intercept);


            bool findpoints = false;
            for (int i = 1; i <= numAssumptionPionts; i++)
            {
                //y ==> row
                //x ==> col

                int x = line_in[start_index].col + i;
                //int y = (long)floor(x * slope + intercept + 0.5);
                int y = (int)(x * slope + intercept + 0.5);

                if (x < maxColinLine)
                    continue;
                findpoints = CheckUpperPoints(y, x, threshold_maxDeltaPr);
                if (findpoints)
                {
                    for (int j = 1; j <= i; j++)
                    {
                        x = KeepIndexInrange(0, _cols - 1, line_in[start_index].col + j);
                        y = KeepIndexInrange(0, _rows - 1, (int)(x * slope + intercept + 0.5));
                        _cacheMap[y * _cols + x] = 1;

                        if (upperline)
                        {
                            y = KeepIndexInrange(0, _rows - 1, (int)(x * slope + intercept + 0.5) - 1);
                        }
                        else
                        {
                            y = KeepIndexInrange(0, _rows - 1, (int)(x * slope + intercept + 0.5) + 1);
                        }
                        _cacheMap[y * _cols + x] = 1;
                    }
                    break;
                }
            }
            if (findpoints)
            {
                return rcode.ok;
            }
            else
            {
                return rcode.no_upper_point_found;
            }
        }

        rcode findBoundaryLineOnMaxDeltaPrMap2(
            ref List<boundary_point_type> boundary_line,
            double threshold_MaxDeltaPr,
            direction_type input_start_direction,
            bool search_anticlockwise)
        {

            rcode rval = rcode.ok;
            bool found_rhs_top_border = false;
            List<boundary_point_type> excluded_pts = new List<boundary_point_type>();


            // check start point has been inserted into boundary line
            if (boundary_line.Count == 1)
            {
                direction_type start_direction = input_start_direction;

                boundary_point_type boundary_point;
                boundary_point.row = boundary_line[0].row;
                boundary_point.col = boundary_line[0].col;
                boundary_point.found_in_direction = START_PT; // start pt

                boundary_point_type next_pt;
                double next_pt_value = 0;
                next_pt.row = boundary_line[0].row;
                next_pt.col = boundary_line[0].col;
                next_pt.found_in_direction = START_PT; // start pt

                for (long i = 0; i < 2000; i++)
                {

                    rcode rval_nextBdPt = findNextBoundaryPointOnMaxDeltaPrMap2(
                        start_direction,
                        search_anticlockwise,
                        ref boundary_point,
                        ref next_pt,
                        ref next_pt_value,
                        ref excluded_pts,
                        threshold_MaxDeltaPr);

                    if (rval_nextBdPt != rcode.next_pt_not_found)
                    {
                        boundary_point.row = next_pt.row;
                        boundary_point.col = next_pt.col;
                        boundary_point.found_in_direction = next_pt.found_in_direction;
                        //IEnumerator<boundary_point_type> found_in_line_at_i
                        int found_in_line_at_i
                            = findInBoundaryLine(boundary_line, boundary_point);
                        if (found_in_line_at_i != boundary_line.Count)
                        {
                            // point already exists in line,
                            // what direction was next found in?
                            if (found_in_line_at_i + 1 != boundary_line.Count)
                                start_direction = boundary_line[found_in_line_at_i + 1].found_in_direction;

                            // record all points after it as excluded
                            boundary_point_type[] tempAddStrArray = null;
                            int arrayIndex = 0;
                            for (int k = found_in_line_at_i + 1; k < boundary_line.Count; k++)
                            {
                                tempAddStrArray[arrayIndex] = boundary_line[k];
                                arrayIndex += 1;
                            }
                            //excluded_pts.insert( excluded_pts[0], found_in_line_at_i+1, boundary_line[boundary_line.Count-1] );
                            excluded_pts.InsertRange(0, tempAddStrArray);
                            //// also exclude the last point left as valid
                            //excluded_pts.insert( excluded_pts.begin(), boundary_line.back() );

                            // remove any points after it from the line 
                            //boundary_line.erase( found_in_line_at_i+1, boundary_line.end() );
                            boundary_line.RemoveRange(found_in_line_at_i + 1, boundary_line.Count - found_in_line_at_i - 1);
                            // start at next new direction
                            if (search_anticlockwise) start_direction = (direction_type)((short)(start_direction) + 1); // +1 for anticlockwise
                            else start_direction = (direction_type)((short)(start_direction) - 1); // -1 for clockwise
                            if (start_direction > direction_type.NE) start_direction = direction_type.N;
                            if (start_direction < direction_type.N) start_direction = direction_type.NE;

                            if (start_direction == input_start_direction)
                            {
                                // then we're gone full circle => stop looking for points
                                break;
                            }

                            // find next point from last point in line with a new direction 
                            boundary_point.row = boundary_line[boundary_line.Count - 1].row;
                            boundary_point.col = boundary_line[boundary_line.Count - 1].col;
                            boundary_point.found_in_direction = boundary_line[boundary_line.Count - 1].found_in_direction;
                        }
                        else
                        {
                            // insert new point into boundary line
                            boundary_line.Add(boundary_point);

                            // set value in map to filtered value
                            //*iPowerRatioPt( boundary_point.row, boundary_point.col ) = next_pt_filtered_value;

                            // mark new point as excluded
                            //excluded_pts.insert( excluded_pts.begin(), boundary_point );

                            start_direction = opposite(boundary_point.found_in_direction);

                            if (start_direction == opposite(boundary_point.found_in_direction))
                            { // don't start by going back to last point, around on one
                                if (search_anticlockwise) start_direction = (direction_type)((short)(start_direction) + 1); // +1 for anticlockwise
                                else start_direction = (direction_type)((short)(start_direction) - 1); // -1 for clockwise
                                if (start_direction > direction_type.NE) start_direction = direction_type.N;
                                if (start_direction < direction_type.N) start_direction = direction_type.NE;
                            }

                            if (boundary_point.row == _rows - 1 || boundary_point.col == _cols - 1)
                            { // have reached end of boundary!
                                found_rhs_top_border = true;
                                break;
                            }

                            if (boundary_point.row == 0 || boundary_point.col == 0)
                            { // have reached wrong border
                                found_rhs_top_border = false;
                                //break;//Jack.zhang remove it for fix SM0 boundary bug
                            }

                        }

                    }

                }
            }
            else
            {
                rval = rcode.no_start_point;
                CGR_LOG.Write("CDSDBROverallModeMap.findBoundaryLineOnMaxDeltaPrMap2 error: no_start_point", LoggingLevels.ERROR_CGR_LOG);
            }

            if (!found_rhs_top_border)
            {
                rval = rcode.rhs_top_border_not_reached;

                CGR_LOG.Write("CDSDBROverallModeMap::findBoundaryLineOnMaxDeltaPrMap2 error: rhs_top_border_not_reached", LoggingLevels.ERROR_CGR_LOG);
            }


            //printf("\ni row col found_in_direction");
            //for( long i = 0 ; i < (long)(boundary_line.size()) ; i++ )
            //{
            //	printf("\n%d %d %d %s", i, boundary_line[i].row, boundary_line[i].col, getCStringForDirection( boundary_line[i].found_in_direction ).GetBuffer() );
            //}

            return rval;
        }



        bool CheckUpperPoints(int x, int y, double threshold)
        {
            if (y == (_cols - 1))
            {
                // no need to check
                return true;
            }
            if ((_cacheMap[KeepIndexInrange(0, _rows - 1, x + 1) * _cols + y + 1]) >= threshold
                 || (_cacheMap[KeepIndexInrange(0, _rows - 1, x + 1) * _cols + y + 1]) >= threshold
                 || (_cacheMap[KeepIndexInrange(0, _rows - 1, x + 1) * _cols + y + 1]) >= threshold)
            {
                return true;
            }
            return false;
        }

        int KeepIndexInrange(int min, int max, int value)
        {
            if (value < min)
            {
                return min;
            }
            if (value > max)
            {
                return max;
            }
            return value;
        }








        rcode findNextBoundaryPointOnMaxDeltaPrMap2(
            direction_type start_direction,
            bool find_new_point_anticlockwise,
            ref boundary_point_type current_pt,
            ref boundary_point_type next_pt,
            ref double next_pt_MaxDeltaPr_value,
            ref List<boundary_point_type> excluded_pts,
            double threshold_MaxDeltaPr)
        {
            rcode rval = rcode.next_pt_not_found;

            double pt_MaxDeltaPr_value = _cacheMap[current_pt.row * _cols + current_pt.col];

            int increment;
            if (find_new_point_anticlockwise)
                increment = 1;
            else // find new point clockwise
                increment = -1;


            direction_type i_direction = start_direction;
            do
            {
                int adj_pt_row = 0;
                int adj_pt_col = 0;
                double adj_pt_MaxDeltaPr_value = 0;

                rcode rval_calAdjPt = getMaxDeltaPrValueOfAdjacentPt2(
                    current_pt.row,
                    current_pt.col,
                    i_direction,
                    ref adj_pt_row,
                    ref adj_pt_col,
                    ref adj_pt_MaxDeltaPr_value);

                if (rval_calAdjPt == rcode.ok)
                {
                    // check if adjacent point has been excluded
                    boundary_point_type check_adj_pt;
                    check_adj_pt.row = adj_pt_row;
                    check_adj_pt.col = adj_pt_col;
                    check_adj_pt.found_in_direction = i_direction;
                    //IEnumerator<boundary_point_type> found_in_excluded_pts_at_i
                    int found_in_excluded_pts_at_i
                        = findInBoundaryLine(excluded_pts, check_adj_pt);

                    if (found_in_excluded_pts_at_i == excluded_pts.Count) // not excluded 
                    {

                        if (adj_pt_MaxDeltaPr_value > threshold_MaxDeltaPr)
                        {
                            // found next point
                            next_pt.row = adj_pt_row;
                            next_pt.col = adj_pt_col;
                            next_pt.found_in_direction = i_direction;
                            next_pt_MaxDeltaPr_value = adj_pt_MaxDeltaPr_value;
                            rval = rcode.ok;
                            break;
                        }
                    }
                    else
                    { // adjacent point marked as excluded, ignore
                    }
                }
                else
                { // must be outside_map_dimensions, ignore
                }

                i_direction = (direction_type)((short)(i_direction) + increment);
                if (i_direction > direction_type.NE) i_direction = direction_type.N;
                if (i_direction < direction_type.N) i_direction = direction_type.NE;
            }
            while (i_direction != start_direction);

            return rval;
        }

        int CalcHightestOfPossibleNoisePlot(int startCol, int endCol, int thresholdofRow)
        {
            int height = 0;
            int highest = 0;
            int x, y;

            for (int i = startCol; i >= endCol; i--)
            {
                height = 0;
                for (int j = 0; j < thresholdofRow; j++)
                {
                    x = i - (_cols - 1);
                    y = j;
                    if (i < (_cols - 1))
                    {
                        x = j;
                        y = (_cols - 1) - i;
                    }
                    if (iMaxDeltaPrPt(x, y).Current > DSDBR01.Instance._DSDBR01_OMBD_max_deltaPr_in_sm)
                    {
                        height++;
                    }
                }
                if (height > highest)
                    highest = height;
            }
            //if (startCol < (_cols - 1) && endCol < (_cols - 1))
            //{
            //    for (int i = startCol; i >= endCol; i--)
            //    {
            //        height = 0;
            //        for (int j = 0; j < thresholdofRow; j++)
            //        {
            //            if (iMaxDeltaPrPt(j, (200 - i)).Current > DSDBR01.Instance._DSDBR01_OMBD_max_deltaPr_in_sm)
            //            {
            //                height++;
            //            }
            //        }
            //        if (height > highest)
            //            highest = height;
            //    }
            //}
            //else
            //{
            //    for (int i = startCol; i >= endCol; i--)
            //    {
            //        height = 0;
            //        for (int j = 0; j < thresholdofRow; j++)
            //        {
            //            if (iMaxDeltaPrPt((i - 200), j).Current > DSDBR01.Instance._DSDBR01_OMBD_max_deltaPr_in_sm)
            //            {
            //                height++;
            //            }
            //        }
            //        if (height > highest)
            //            highest = height;
            //    }
            //}
            return highest;
        }

        rcode CheckAndRedirectPossibleSupermodeStartPoint(ref List<int> bottom_lhs_lower_line_start_pts,
                                                                   List<int> bottom_lhs_upper_line_start_pts,
                                                                   List<double> maxDeltaPr_on_bottom_lhs_border)
        {
            CGR_LOG.Write("CDSDBROverallModeMap::CheckAndRedirectPossibleSupermodeStartPoint() entered", LoggingLevels.DIAGNOSTIC_CGR_LOG);
            double slope = DSDBR01.Instance._DSDBR01_OMBD_max_detecting_height_of_incontinuity / 200.0; //15/200.0;
            int start1 = 0;
            int start2 = 0;

            List<int> bottomLineStart = new List<int>();
            List<int> bottomLineEnd = new List<int>();
            int start = 0;
            int end = 0;
            int width = 0;


            if (bottom_lhs_lower_line_start_pts.Count > 0)
            {

                for (int i = 0; i < bottom_lhs_lower_line_start_pts.Count; i++)
                {
                    // Find all possible incontinuous segment between two possible super mode
                    if (bottom_lhs_lower_line_start_pts[i] > 200) break;

                    if (i == 0)
                        start1 = 0;
                    else
                        start1 = bottom_lhs_upper_line_start_pts[i - 1];
                    start2 = bottom_lhs_lower_line_start_pts[i];
                    bottomLineStart.Clear();
                    bottomLineEnd.Clear();
                    width = 0;
                    for (int j = start2; j > start1; j--)
                    {
                        if (maxDeltaPr_on_bottom_lhs_border[j] < DSDBR01.Instance._DSDBR01_OMBD_max_deltaPr_in_sm)
                        {
                            if (width == 0) start = j;
                            width++;

                        }

                        if (width > 0 & maxDeltaPr_on_bottom_lhs_border[j] > DSDBR01.Instance._DSDBR01_OMBD_max_deltaPr_in_sm)
                        {

                            end = j + 1;

                            bottomLineStart.Add(start);
                            bottomLineEnd.Add(end);
                            width = 0;
                        }
                    }

                    // Judge incontinuity and update start point
                    bool continuity;
                    for (int ii = 0; ii < bottomLineStart.Count; ii++)
                    {
                        continuity = true;
                        for (int j = bottomLineStart[ii]; j >= bottomLineEnd[ii]; j--)
                        {

                            continuity = JudgeContinuity(200 - j, (int)(slope * (200 - j)));
                            if (continuity == false) break;
                        }
                        if (continuity == false)
                        {
                            bottom_lhs_lower_line_start_pts[i] = bottomLineEnd[ii] - 1;

                        }
                    }
                }
            }
            CGR_LOG.Write("CDSDBROverallModeMap::CheckAndRedirectPossibleSupermodeStartPoint() exiting", LoggingLevels.DIAGNOSTIC_CGR_LOG);

            return rcode.ok;
        }


        //-----------------------------end added
    }
}


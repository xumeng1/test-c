using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.Algorithms
{
    class QAThresholds
    {
        const int DEFAULT_SLOPE_WINDOW = 5;
        const int DEFAULT_MODAL_DISTORTION_MIN_X = 20;
        const int DEFAULT_MODAL_DISTORTION_MAX_X = 80;
        const int DEFAULT_MODAL_DISTORTION_MIN_Y = 20;
        const int DEFAULT_MODE_WIDTH_ANALYSIS_MIN_X = 20;
        const int DEFAULT_MODE_WIDTH_ANALYSIS_MAX_X = 80;
        const int DEFAULT_HYSTERESIS_ANALYSIS_MIN_X = 20;
        const int DEFAULT_HYSTERESIS_ANALYSIS_MAX_X = 80;

        public QAThresholds()
        {
            setThresholdsToDefault();
        }

        ////////////////////////////////////
        //public
        public int _SMMapQA;		//set to 1 if SuperModeQA
        public int _slopeWindow;
        public double _modalDistortionMinX;
        public double _modalDistortionMaxX;
        public double _modalDistortionMinY;
        public double _modeWidthAnalysisMinX;
        public double _modeWidthAnalysisMaxX;
        public double _hysteresisAnalysisMinX;
        public double _hysteresisAnalysisMaxX;

        public void setThresholdsToDefault()
        {
            _slopeWindow = DEFAULT_SLOPE_WINDOW;
            _modalDistortionMinX = DEFAULT_MODAL_DISTORTION_MIN_X;
            _modalDistortionMaxX = DEFAULT_MODAL_DISTORTION_MAX_X;
            _modalDistortionMinY = DEFAULT_MODAL_DISTORTION_MIN_Y;
            _modeWidthAnalysisMinX = DEFAULT_MODE_WIDTH_ANALYSIS_MIN_X;
            _modeWidthAnalysisMaxX = DEFAULT_MODE_WIDTH_ANALYSIS_MAX_X;
            _hysteresisAnalysisMinX = DEFAULT_HYSTERESIS_ANALYSIS_MIN_X;
            _hysteresisAnalysisMaxX = DEFAULT_HYSTERESIS_ANALYSIS_MAX_X;
        }

    }
}

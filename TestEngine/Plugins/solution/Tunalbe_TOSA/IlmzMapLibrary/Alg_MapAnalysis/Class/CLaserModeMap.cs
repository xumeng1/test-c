using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Bookham.TestLibrary.Algorithms
{
    public class boundary_multimap_typedef : List<KeyValuePair<int, int>>
    {
        public void Insert(KeyValuePair<int, int> item)
        {
            int index = 0;

            for (int i = this.Count - 1; i >= 0; i--)
            {
                if (this[i].Key == item.Key)
                {
                    index = i+1;
                    break;
                }
            }
            Insert(index, item);
        }
        public int Find(int key)
        {
            int i;
            for (i = 0; i < this.Count; i++)
            {
                if (this[i].Key== key)
                {
                    break; 
                }
            }
            return i;
        }

       
    }


    public class CLaserModeMap
    {
        public enum rcode
        {
            ok = 0,
            vector_size_rows_cols_do_not_match,
            could_not_open_file,
            file_does_not_exist,
            file_already_exists,
            file_format_invalid
        }
        public List<double> _map = new List<double>();
        public int _rows;
        public int _cols;
        public String _abs_filepath;

        // structure to hold detected boundary jumps
        // This stores unsigned int pair values
        // The first value is the index of a point before a jump
        // The second value is the index of a point after a jump
        //typedef std::multimap<unsigned int,unsigned int> boundary_multimap_typedef;

        //typedef std::multimap<unsigned int,unsigned int> boundary_multimap_typedef;
        // This stores unsigned int pair values
        // In this usage, the first value is point on the map (row*ramp_length+col)
        // The second value is the boundary line number, starting at line 0
        public boundary_multimap_typedef _boundary_points = new boundary_multimap_typedef();
        public boundary_multimap_typedef _linked_boundary_points = new boundary_multimap_typedef();

        public CLaserModeMap()
        {
            clear();
        }
        public CLaserModeMap(List<double> map,int rows,int cols )
        {
            setMap(map, rows, cols);
            clear();
        }

        public rcode writeToFile(String abs_filepath)// bool overwrite = false 
        {
            return writeToFile(abs_filepath, false);
        }
        public rcode writeToFile(String abs_filepath, bool overwrite)
        {
            rcode rval = rcode.ok;
            //int row_count = 0;
            //int col_count = 0;
            // check _rows, _cols, _map.Count match up
            if (_rows * _cols != _map.Count)
            {
                rval = rcode.vector_size_rows_cols_do_not_match;
            }
            else
            {
                // Check if file already exists and overwrite not specified
                if (File.Exists(abs_filepath) && !overwrite)
                {
                    rval = rcode.file_already_exists;
                }
                else
                {
                    try
                    {
                        using (StreamWriter sw = new StreamWriter(abs_filepath))
                        {
                            for (int row_count = 0; row_count < _rows; row_count++)
                            {
                                for (int col_count = 0; col_count < _cols; col_count++)
                                {
                                    sw.Write(_map[row_count * _cols + col_count]);
                                    if (col_count < _cols - 1)
                                        sw.Write(Defaults.COMMA_CHAR);
                                }
                                sw.WriteLine();
                            }
                        }
                        _abs_filepath = abs_filepath;
                    }
                    catch (Exception)
                    {
                        rval = rcode.could_not_open_file;
                    }
                }
            }

            return rval;
        }

        public rcode readFromFile(String abs_filepath)
        {
            rcode rval = rcode.ok;

            int row_count = 0;
            int first_line_col_count = 0;
            int col_count = 0;

            // Check file
            if (File.Exists(abs_filepath))
            {
                // File exists => attempt to read it's contents
                try
                {
                    //std.ifstream file_stream( abs_filepath );
                    string[] arr = File.ReadAllLines(abs_filepath);
                    row_count = 0;
                    foreach (string item in arr)
                    {
                        // File stream is open, read each line
                        if (item.Length > 0) // there is something on the line
                        {
                            // Search for two or more commas together,
                            // if found, insert a zero between them
                            // a comma at the start or end of a line also
                            // means there is a zero to be inserted

                            string[] colarr = item.Split(Defaults.DELIMITERS.ToCharArray());
                            col_count = colarr.Length;
                            foreach (string var in colarr)
                            {
                                if (string.IsNullOrEmpty(var))
                                    _map.Add(0);
                                else
                                    _map.Add(double.Parse(var));
                            }

                            if (row_count == 0) first_line_col_count = col_count;

                            row_count++;
                        }

                        if (row_count > 0 && col_count != first_line_col_count)
                        {
                            rval = rcode.file_format_invalid;
                            break;
                        }
                    }
                }
                catch (Exception)
                {
                    rval = rcode.could_not_open_file;
                }
            }
            else
            {
                rval = rcode.file_does_not_exist;
            }

            if (rval == rcode.ok
             && (row_count == 0 || col_count == 0 || _map.Count != row_count * col_count))
            {
                rval = rcode.file_format_invalid;
            }

            if (rval == rcode.ok)
            {
                _rows = row_count;
                _cols = col_count;
                _abs_filepath = abs_filepath;
            }
            else
            {
                // something is wrong, clear everything!
                clear();
                throw new Exception(rval.ToString());
            }

            return rval;
        }








        public rcode setMap(List<double> map, int rows, int cols)
        {
            rcode rval = rcode.ok;

            if (rows > 0
             && cols > 0
             && rows * cols == map.Count)
            {
                _rows = rows;
                _cols = cols;
                _map.Clear();
                _map = map;
            }
            else
            {
                rval = rcode.vector_size_rows_cols_do_not_match;
            }

            return rval;
        }

        public void clear()
        {
            _map.Clear();
            _rows = 0;
            _cols = 0;
            _abs_filepath = string.Empty;
            _boundary_points.Clear();
            _linked_boundary_points.Clear();
        }

        public bool isEmpty()
        {
            bool rval = false;

            if (_map.Count == 0)
            {
                rval = true;
                _rows = 0;
                _cols = 0;
            }

            return rval;
        }

        public void writeJumpsToFile(String abs_filepath)
        {
            // Ensure the directory exists
            int index_of_last_dirslash = abs_filepath.LastIndexOf('\\');
            if (index_of_last_dirslash == -1)
            {
                // '\\' not found => not an absolute path
                throw new FileLoadException("could_not_open_file");
            }
            else
            {
                using (StreamWriter sw = new StreamWriter(abs_filepath))
                {
                    for (int i = 0; i < _boundary_points.Count; i++)
                    {
                        sw.WriteLine(boundaryPoint(_boundary_points, i));
                    }
                }
            }

        }
        int boundaryPoint(boundary_multimap_typedef boundarymmap, int iter_b)
        {
            int first = boundarymmap[iter_b].Key;
            int second = boundarymmap[iter_b].Value;
            int boundary;
            if ((int)second - (int)first == 2) boundary = second - 1;
            else if ((int)first - (int)second == 2) boundary = first - 1;
            else boundary = second;
            return boundary;
        }

        public rcode writeLinkedJumpsToFile(String abs_filepath)
        {
            rcode rval = rcode.ok;
            try
            {
                using (StreamWriter sw=new StreamWriter(abs_filepath))
                {
                    for (int i = 0; i < _linked_boundary_points.Count; i++)
                    {
                        sw.WriteLine(string.Format("{0}, {1}", _linked_boundary_points[i].Key, _linked_boundary_points[i].Value));
                    }
                    sw.Close();
                }
            }
            catch (Exception)
            {
                rval = rcode.could_not_open_file;
            }
            return rval;
        }

        //std::vector<double>::iterator iPt( int row, int col );
        public IEnumerator<double> iPt(int row, int col)
        {
            IEnumerator<double> i=null;
            // check point is within map
            if (row >= 0 && row < _rows && col >= 0 && col < _cols)
            {
                int index = row * _cols + col;

                //i = _map.begin();
                //i += index;
                i = _map.GetEnumerator();
                for (int j = 0; j <= index; j++)
                {
                    i.MoveNext();
                }
            }

            return i;
        }
        //int boundaryPoint(boundary_multimap_typedef::iterator iter_b);

       

        
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.Solution.Config;
using System.Collections.Specialized;
using Bookham.fcumapping.utility;

namespace Bookham.TestLibrary.Algorithms
{
    class DSDBR01
    {
        static DSDBR01 instance;

        public static void Initialize(string parametersFile)
        {
            instance = new DSDBR01(parametersFile);

        }
        public static DSDBR01 Instance
        {
            get
            {
                if (instance == null) throw new Exception("Please Initialize first.");
                return instance;
            }
        }
        private DSDBR01(string parametersFile)
        {
            ConfigurationManager cm = new ConfigurationManager(parametersFile);
            //TODO:
              
            ConfigurationManager cfgReader = new ConfigurationManager(parametersFile);

            Dictionary<string, NameValueCollection> DSDBR01Prams = new Dictionary<string, NameValueCollection>();
            DSDBR01Prams.Add("Logging", (NameValueCollection)cfgReader.GetSection("CloseGridSetting/Logging"));
            DSDBR01Prams.Add("DSDBR01", (NameValueCollection)cfgReader.GetSection("CloseGridSetting/DSDBR01"));
            //DSDBR01Prams.Add("Characterisation", (NameValueCollection)cfgReader.GetSection("DSDBR01/Characterisation"));
            //DSDBR01Prams.Add("CoarseFrequency", (NameValueCollection)cfgReader.GetSection("DSDBR01/CoarseFrequency"));
            //DSDBR01Prams.Add("DataCollection", (NameValueCollection)cfgReader.GetSection("CloseGridSetting/OverAllMap/DataCollection"));
            //DSDBR01Prams.Add("ITUGrid", (NameValueCollection)cfgReader.GetSection("DSDBR01/ITUGrid"));

            DSDBR01Prams.Add("OverallMap/BoundaryDetection", (NameValueCollection)cfgReader.GetSection("CloseGridSetting/OverAllMap/BoundaryDetection"));
            DSDBR01Prams.Add("OverallMap/DataCollection", (NameValueCollection)cfgReader.GetSection("CloseGridSetting/OverAllMap/DataCollection"));
            DSDBR01Prams.Add("OverallMap/QA", (NameValueCollection)cfgReader.GetSection("CloseGridSetting/OverAllMap/QA"));
            DSDBR01Prams.Add("OverallMap/QAThresholds", (NameValueCollection)cfgReader.GetSection("CloseGridSetting/OverAllMap/QAThresholds"));
            DSDBR01Prams.Add("OverallMap/PointsAnalyst", (NameValueCollection)cfgReader.GetSection("CloseGridSetting/OverAllMap/PointsAnalyst"));//Jim@2011.12.9


            ////DSDBR01Prams.Add("SupermodeMap/BoundaryDetection", (NameValueCollection)cm.GetSection("DSDBR01/SupermodeMap/BoundaryDetection/BoundaryDetection"));
            ////DSDBR01Prams.Add("ForwardMap", (NameValueCollection)cm.GetSection("DSDBR01/SupermodeMap/BoundaryDetection/ForwardMap"));
            ////DSDBR01Prams.Add("ReverseMap", (NameValueCollection)cm.GetSection("DSDBR01/SupermodeMap/BoundaryDetection/ReverseMap"));

            ////DSDBR01Prams.Add("SupermodeMap/DataCollection", (NameValueCollection)cm.GetSection("DSDBR01/SupermodeMap/DataCollection"));
            ////DSDBR01Prams.Add("QSupermodeMap/QA", (NameValueCollection)cm.GetSection("DSDBR01/SupermodeMap/QA"));

            ////DSDBR01Prams.Add("T0", (NameValueCollection)cm.GetSection("DSDBR01/SupermodeMap/QAThresholds/T0"));
            ////DSDBR01Prams.Add("T1", (NameValueCollection)cm.GetSection("DSDBR01/SupermodeMap/QAThresholds/T1"));
            ////DSDBR01Prams.Add("T2", (NameValueCollection)cm.GetSection("DSDBR01/SupermodeMap/QAThresholds/T2"));
            ////DSDBR01Prams.Add("T3", (NameValueCollection)cm.GetSection("DSDBR01/SupermodeMap/QAThresholds/T3"));
            ////DSDBR01Prams.Add("T4", (NameValueCollection)cm.GetSection("DSDBR01/SupermodeMap/QAThresholds/T4"));
            ////DSDBR01Prams.Add("T5", (NameValueCollection)cm.GetSection("DSDBR01/SupermodeMap/QAThresholds/T5"));
            ////DSDBR01Prams.Add("T6", (NameValueCollection)cm.GetSection("DSDBR01/SupermodeMap/QAThresholds/T6"));
            ////DSDBR01Prams.Add("T7", (NameValueCollection)cm.GetSection("DSDBR01/SupermodeMap/QAThresholds/T7"));
            ////DSDBR01Prams.Add("T8", (NameValueCollection)cm.GetSection("DSDBR01/SupermodeMap/QAThresholds/T8"));
            ////DSDBR01Prams.Add("T9", (NameValueCollection)cm.GetSection("DSDBR01/SupermodeMap/QAThresholds/T9"));

            //Logging
            _DSDBR01_LOGFILEPATHNAME = DSDBR01Prams["Logging"]["Log Absolute Path File Name"];
            _DSDBR01_LOGFILEMAXSIZE = int.Parse(DSDBR01Prams["Logging"]["Log File Max Size in KBytes"]);
            _DSDBR01_LOGGING_LEVEL = int.Parse(DSDBR01Prams["Logging"]["Log Level"]);
            _DSDBR01_LOGGING_ON = int.Parse(DSDBR01Prams["Logging"]["Logging On"]);
           
            //DSDBR01 Base parameters
            _DSDBR01_BASE_results_directory = DSDBR01Prams["DSDBR01"]["Results Directory"];
            //_DSDBR01_BASE_stub_hardware_calls = DSDBR01Prams["DSDBR01"]["Stub hardware calls"];

            // DSDBR01 Common Data Collection parameters
            ////_DSDBR01_CMDC_gain_module_name = (string)(DSDBR01Prams["DataCollection"]["Gain section module name"]);
            ////_DSDBR01_CMDC_soa_module_name = (string)(DSDBR01Prams["DataCollection"]["SOA section module name"]);
            ////_DSDBR01_CMDC_rear_module_name = (string)(DSDBR01Prams["DataCollection"]["Rear section module name"]);
            ////_DSDBR01_CMDC_phase_module_name = (string)(DSDBR01Prams["DataCollection"]["Phase section module name"]);
            ////_DSDBR01_CMDC_front1_module_name = (string)(DSDBR01Prams["DataCollection"]["Front section 1 module name"]);
            ////_DSDBR01_CMDC_front2_module_name = (string)(DSDBR01Prams["DataCollection"]["Front section 2 module name"]);
            ////_DSDBR01_CMDC_front3_module_name = (string)(DSDBR01Prams["DataCollection"]["Front section 3 module name"]);
            ////_DSDBR01_CMDC_front4_module_name = (string)(DSDBR01Prams["DataCollection"]["Front section 4 module name"]);
            ////_DSDBR01_CMDC_front5_module_name = (string)(DSDBR01Prams["DataCollection"]["Front section 5 module name"]);
            ////_DSDBR01_CMDC_front6_module_name = (string)(DSDBR01Prams["DataCollection"]["Front section 6 module name"]);
            ////_DSDBR01_CMDC_front7_module_name = (string)(DSDBR01Prams["DataCollection"]["Front section 7 module name"]);
            ////_DSDBR01_CMDC_front8_module_name = (string)(DSDBR01Prams["DataCollection"]["Front section 8 module name"]);
            ////_DSDBR01_CMDC_306II_module_name = (string)(DSDBR01Prams["DataCollection"]["306II module name"]);
            ////_DSDBR01_CMDC_306II_direct_channel = int.Parse((DSDBR01Prams["DataCollection"]["306II Direct Power Channel"]));
            ////_DSDBR01_CMDC_306II_filtered_channel = int.Parse((DSDBR01Prams["DataCollection"]["306II Filtered Power Channel"]));
            ////_DSDBR01_CMDC_306EE_module_name = (string)(DSDBR01Prams["DataCollection"]["306EE module name"]);
            ////_DSDBR01_CMDC_306EE_photodiode1_channel = int.Parse((DSDBR01Prams["DataCollection"]["306EE Photodiode 1 Channel"]));
            ////_DSDBR01_CMDC_306EE_photodiode2_channel = int.Parse((DSDBR01Prams["DataCollection"]["306EE Photodiode 2 Channel"]));
            //_DSDBR01_CMDC_wavemeter_retries = int.Parse((DSDBR01Prams["DataCollection"]["Wavemeter number of reset and retries6"]));

            // DSDBR01 Overall Map Data Collection parameters
            _DSDBR01_OMDC_ramp_direction = (string)(DSDBR01Prams["OverallMap/DataCollection"]["RampDirection"]);
            _DSDBR01_OMDC_forward_and_reverse = DSDBR01Prams["OverallMap/DataCollection"]["Forward and Reverse"] == "1";
            _DSDBR01_OMDC_measure_photodiode_currents = DSDBR01Prams["OverallMap/DataCollection"]["Measure Photodiode Currents"] == "1";
            _DSDBR01_OMDC_write_filtered_power_maps = DSDBR01Prams["OverallMap/DataCollection"]["Write Filtered Power Maps"] == "1";
            _DSDBR01_OMDC_rear_currents_abspath = (string)(DSDBR01Prams["OverallMap/DataCollection"]["Rear currents filepath"]);
            _DSDBR01_OMDC_front_currents_abspath = (string)(DSDBR01Prams["OverallMap/DataCollection"]["Front currents filepath"]);
            //_DSDBR01_OMDC_source_delay = int.Parse((DSDBR01Prams["OverallMap/DataCollection"]["Source Delay"]));
            //_DSDBR01_OMDC_measure_delay = int.Parse((DSDBR01Prams["OverallMap/DataCollection"]["Measure Delay"]));
            _DSDBR01_OMDC_overwrite_existing_files = (DSDBR01Prams["OverallMap/DataCollection"]["Overwrite existing files"]=="1" );

            // DSDBR01 Overall Map Boundary Detection parameters
            _DSDBR01_OMBD_read_boundaries_from_file = DSDBR01Prams["OverallMap/BoundaryDetection"]["ReadBoundariesFromFile"] == "1";
            _DSDBR01_OMBD_write_boundaries_to_file = DSDBR01Prams["OverallMap/BoundaryDetection"]["Write Boundaries To File"] == "1";
            _DSDBR01_OMBD_median_filter_rank = int.Parse((DSDBR01Prams["OverallMap/BoundaryDetection"]["MedianFilterRank"]));
            _DSDBR01_OMBD_max_deltaPr_in_sm = double.Parse((DSDBR01Prams["OverallMap/BoundaryDetection"]["MaxDeltaPrInASupermode"]));
            //_DSDBR01_OMBD_max_deltaPr_in_sm = _DSDBR01_OMBD_max_deltaPr_in_sm * (OverallMapPoweRatioRange.MaxPowerRatio - OverallMapPoweRatioRange.MinPowerRatio);//Echo new added, to normalize power ratio, 2011-03-22
            _DSDBR01_OMBD_min_points_width_of_sm = int.Parse((DSDBR01Prams["OverallMap/BoundaryDetection"]["MinNumberofPointsWidthofASupermode"]));
            _DSDBR01_OMBD_ml_moving_ave_filter_n = int.Parse((DSDBR01Prams["OverallMap/BoundaryDetection"]["NforMovingAverageFilteronMiddleLine"]));
            _DSDBR01_OMBD_ml_min_length = int.Parse((DSDBR01Prams["OverallMap/BoundaryDetection"]["MinimumLengthofMiddleLine"]));
            _DSDBR01_OMBD_ml_extend_to_corner = DSDBR01Prams["OverallMap/BoundaryDetection"]["ExtendMiddleLineToCornerIfStillInSupermode"] == "1";
            _DSDBR01_OMBD_assumption_points = int.Parse((DSDBR01Prams["OverallMap/BoundaryDetection"]["AssumptionPionts"]));
            _DSDBR01_OMBD_max_number_of_points_height_of_noise_plot = int.Parse((DSDBR01Prams["OverallMap/BoundaryDetection"]["noisePlotHighestThreshold"]));
            _DSDBR01_OMBD_max_detecting_height_of_incontinuity = int.Parse((DSDBR01Prams["OverallMap/BoundaryDetection"]["maxDetectingHeightOfIncontinuity"]));


            ////// DSDBR01 Supermode Map Data Collection parameters
            ////_DSDBR01_SMDC_ramp_direction = (string)(DSDBR01Prams["SupermodeMap/DataCollection"]["Ramp direction"]);
            ////_DSDBR01_SMDC_phase_currents_abspath = (string)(DSDBR01Prams["SupermodeMap/DataCollection"]["Phase currents filepath"]);
            ////_DSDBR01_SMDC_measure_photodiode_currents = DSDBR01Prams["SupermodeMap/DataCollection"]["Measure photodiode currents"]=="1";
            ////_DSDBR01_SMDC_write_filtered_power_maps = DSDBR01Prams["SupermodeMap/DataCollection"]["Write Filtered Power Maps"]=="1";
            ////_DSDBR01_SMDC_source_delay = int.Parse((DSDBR01Prams["SupermodeMap/DataCollection"]["Source Delay"]));
            ////_DSDBR01_SMDC_measure_delay = int.Parse((DSDBR01Prams["SupermodeMap/DataCollection"]["Measure Delay"]));
            ////_DSDBR01_SMDC_overwrite_existing_files = DSDBR01Prams["SupermodeMap/DataCollection"]["Overwrite existing files"]=="1";
            
            ////// DSDBR01 Supermode Map Boundary Detection parameters
            ////_DSDBR01_SMBD_read_boundaries_from_file = DSDBR01Prams["SupermodeMap/BoundaryDetection"]["Read Boundaries From File"]=="1";
            ////_DSDBR01_SMBD_write_boundaries_to_file = DSDBR01Prams["SupermodeMap/BoundaryDetection"]["Write Boundaries To File"]=="1";
            ////_DSDBR01_SMBD_write_middle_of_upper_lines_to_file = DSDBR01Prams["SupermodeMap/BoundaryDetection"]["Write Middle of Upper Lines To File"]=="1";
            ////_DSDBR01_SMBD_write_middle_of_lower_lines_to_file = DSDBR01Prams["SupermodeMap/BoundaryDetection"]["Write Middle of Lower Lines To File"]=="1";
            ////_DSDBR01_SMBD_write_debug_files = DSDBR01Prams["SupermodeMap/BoundaryDetection"]["Write Debug Files"]=="1";
            ////_DSDBR01_SMBD_extrapolate_to_map_edges = DSDBR01Prams["SupermodeMap/BoundaryDetection"]["Extrapolate to map edges"]=="1";
            ////_DSDBR01_SMBD_median_filter_rank = int.Parse((DSDBR01Prams["SupermodeMap/BoundaryDetection"]["Median filter rank"]));
            ////_DSDBR01_SMBD_max_deltaPr_in_lm = double.Parse((DSDBR01Prams["SupermodeMap/BoundaryDetection"]["Max deltaPr in a longitudinal mode"]));
            ////_DSDBR01_SMBD_min_points_width_of_lm = int.Parse((DSDBR01Prams["SupermodeMap/BoundaryDetection"]["Min number of points width of a longitudinal mode"]));
            ////_DSDBR01_SMBD_max_points_width_of_lm = int.Parse((DSDBR01Prams["SupermodeMap/BoundaryDetection"]["Max number of points width of a longitudinal mode"]));
            ////_DSDBR01_SMBD_ml_moving_ave_filter_n = int.Parse((DSDBR01Prams["SupermodeMap/BoundaryDetection"]["N for moving average filter on middle line"]));
            ////_DSDBR01_SMBD_ml_min_length = int.Parse((DSDBR01Prams["SupermodeMap/BoundaryDetection"]["Minimum length of middle line"]));
            ////_DSDBR01_SMBD_vertical_percent_shift = double.Parse((DSDBR01Prams["SupermodeMap/BoundaryDetection"]["Vertical Percent Shift for Frequency Middle"]));

            ////_DSDBR01_SMBD_zprc_hysteresis_upper_threshold = double.Parse((DSDBR01Prams["SupermodeMap/BoundaryDetection"]["ZPRC hysteresis upper threshold"]));
            ////_DSDBR01_SMBD_zprc_hysteresis_lower_threshold = double.Parse((DSDBR01Prams["SupermodeMap/BoundaryDetection"]["ZPRC hysteresis lower threshold"]));
            ////_DSDBR01_SMBD_zprc_max_hysteresis_rise = double.Parse((DSDBR01Prams["SupermodeMap/BoundaryDetection"]["ZPRC max hysteresis rise"]));
            ////_DSDBR01_SMBD_zprc_min_points_separation = int.Parse((DSDBR01Prams["SupermodeMap/BoundaryDetection"]["ZPRC min number of points separation"]));
            ////_DSDBR01_SMBD_zprc_max_points_to_reverse_jump = int.Parse((DSDBR01Prams["SupermodeMap/BoundaryDetection"]["ZPRC max number of points from reverse jump"]));
            ////_DSDBR01_SMBD_zprc_max_hysteresis_points = int.Parse((DSDBR01Prams["SupermodeMap/BoundaryDetection"]["ZPRC max number of hysteresis points"]));
            ////_DSDBR01_SMBD_ramp_abspath = (string)(DSDBR01Prams["SupermodeMap/BoundaryDetection"]["Ramp filepath"]);

            ////// DSDBR01 Forward Supermode Map Boundary Detection parameters
            ////_DSDBR01_SMBDFWD_I_ramp_boundaries_max = int.Parse((DSDBR01Prams["ForwardMap"]["Max number of boundaries on a single ramp"]));
            ////_DSDBR01_SMBDFWD_boundaries_min_separation = int.Parse((DSDBR01Prams["ForwardMap"]["Min separation of boundaries on a single ramp"]));
            ////_DSDBR01_SMBDFWD_min_d2Pr_dI2_zero_cross_jump_size = double.Parse((DSDBR01Prams["ForwardMap"]["Min d2Pr/dI2 zero cross jump size"]));
            ////_DSDBR01_SMBDFWD_max_sum_of_squared_distances_from_Pr_I_line = double.Parse((DSDBR01Prams["ForwardMap"]["Max sum of squared distances from Pr/I line"]));
            ////_DSDBR01_SMBDFWD_min_slope_change_of_Pr_I_lines = double.Parse((DSDBR01Prams["ForwardMap"]["Min slope change of Pr/I line"]));
            ////_DSDBR01_SMBDFWD_linking_max_distance_between_linked_jumps = int.Parse((DSDBR01Prams["ForwardMap"]["Max distance between linked jumps"]));
            ////_DSDBR01_SMBDFWD_linking_min_sharpness = double.Parse((DSDBR01Prams["ForwardMap"]["Linking min sharpness"]));
            ////_DSDBR01_SMBDFWD_linking_max_sharpness = double.Parse((DSDBR01Prams["ForwardMap"]["Linking max sharpness"]));
            ////_DSDBR01_SMBDFWD_linking_max_gap_between_double_lines = int.Parse((DSDBR01Prams["ForwardMap"]["Linking max gap between double lines"]));
            ////_DSDBR01_SMBDFWD_linking_min_gap_between_double_lines = int.Parse((DSDBR01Prams["ForwardMap"]["Linking min gap between double lines"]));
            ////_DSDBR01_SMBDFWD_linking_near_bottom_row = int.Parse((DSDBR01Prams["ForwardMap"]["Linking near bottom row"]));
            ////_DSDBR01_SMBDFWD_linking_sharpness_near_bottom_row = double.Parse((DSDBR01Prams["ForwardMap"]["Linking sharpness near bottom"]));
            ////_DSDBR01_SMBDFWD_linking_max_distance_near_bottom_row = int.Parse((DSDBR01Prams["ForwardMap"]["Linking max distance near bottom"]));
            ////_DSDBR01_SMBDFWD_linking_max_distance_to_single_unlinked_jump = int.Parse((DSDBR01Prams["ForwardMap"]["Linking max distance to single unlinked jump"]));
            ////_DSDBR01_SMBDFWD_linking_max_y_distance_to_link = int.Parse((DSDBR01Prams["ForwardMap"]["Linking max y distance to link"]));
            ////_DSDBR01_SMBDFWD_linking_sharpness_ramp_split = double.Parse((DSDBR01Prams["ForwardMap"]["Linking sharpness ramp split"]));

            ////// DSDBR01 Reverse Supermode Map Boundary Detection parameters
            ////_DSDBR01_SMBDREV_I_ramp_boundaries_max = int.Parse((DSDBR01Prams["ReverseMap"]["Max number of boundaries on a single ramp"]));
            ////_DSDBR01_SMBDREV_boundaries_min_separation = int.Parse((DSDBR01Prams["ReverseMap"]["Min separation of boundaries on a single ramp"]));
            ////_DSDBR01_SMBDREV_min_d2Pr_dI2_zero_cross_jump_size = double.Parse((DSDBR01Prams["ReverseMap"]["Min d2Pr/dI2 zero cross jump size"]));
            ////_DSDBR01_SMBDREV_max_sum_of_squared_distances_from_Pr_I_line = double.Parse((DSDBR01Prams["ReverseMap"]["Max sum of squared distances from Pr/I line"]));
            ////_DSDBR01_SMBDREV_min_slope_change_of_Pr_I_lines = double.Parse((DSDBR01Prams["ReverseMap"]["Min slope change of Pr/I line"]));
            ////_DSDBR01_SMBDREV_linking_max_distance_between_linked_jumps = int.Parse((DSDBR01Prams["ReverseMap"]["Max distance between linked jumps"]));
            ////_DSDBR01_SMBDREV_linking_min_sharpness = double.Parse((DSDBR01Prams["ReverseMap"]["Linking min sharpness"]));
            ////_DSDBR01_SMBDREV_linking_max_sharpness = double.Parse((DSDBR01Prams["ReverseMap"]["Linking max sharpness"]));
            ////_DSDBR01_SMBDREV_linking_max_gap_between_double_lines = int.Parse((DSDBR01Prams["ReverseMap"]["Linking max gap between double lines"]));
            ////_DSDBR01_SMBDREV_linking_min_gap_between_double_lines = int.Parse((DSDBR01Prams["ReverseMap"]["Linking min gap between double lines"]));
            ////_DSDBR01_SMBDREV_linking_near_bottom_row = int.Parse((DSDBR01Prams["ReverseMap"]["Linking near bottom row"]));
            ////_DSDBR01_SMBDREV_linking_sharpness_near_bottom_row = double.Parse((DSDBR01Prams["ReverseMap"]["Linking sharpness near bottom"]));
            ////_DSDBR01_SMBDREV_linking_max_distance_near_bottom_row = int.Parse((DSDBR01Prams["ReverseMap"]["Linking max distance near bottom"]));
            ////_DSDBR01_SMBDREV_linking_max_distance_to_single_unlinked_jump = int.Parse((DSDBR01Prams["ReverseMap"]["Linking max distance to single unlinked jump"]));
            ////_DSDBR01_SMBDREV_linking_max_y_distance_to_link = int.Parse((DSDBR01Prams["ReverseMap"]["Linking max y distance to link"]));
            ////_DSDBR01_SMBDREV_linking_sharpness_ramp_split = double.Parse((DSDBR01Prams["ReverseMap"]["Linking sharpness ramp split"]));

            // DSDBR01 Overall Map Quantitative Analysis parameters
            _DSDBR01_OMQA_slope_window_size = int.Parse((DSDBR01Prams["OverallMap/QA"]["SlopeWindowSize"]));
            _DSDBR01_OMQA_modal_distortion_min_x = double.Parse((DSDBR01Prams["OverallMap/QA"]["ModalDistortionMinX"]));
            _DSDBR01_OMQA_modal_distortion_max_x = double.Parse((DSDBR01Prams["OverallMap/QA"]["ModalDistortionMaxX"]));
            _DSDBR01_OMQA_modal_distortion_min_y = double.Parse((DSDBR01Prams["OverallMap/QA"]["ModalDistortionMinY"]));

            // DSDBR01 Overall Map Pass Fail Thresholds
            _DSDBR01_OMPF_max_mode_borders_removed_threshold = int.Parse((DSDBR01Prams["OverallMap/QAThresholds"]["Max Number of Mode Borders Removed"]));
            _DSDBR01_OMPF_max_middle_line_rms_value = double.Parse((DSDBR01Prams["OverallMap/QAThresholds"]["Max Middle Line RMS Value"]));
            _DSDBR01_OMPF_min_middle_line_slope = double.Parse((DSDBR01Prams["OverallMap/QAThresholds"]["Min Middle Line Slope"]));
            _DSDBR01_OMPF_max_middle_line_slope = double.Parse((DSDBR01Prams["OverallMap/QAThresholds"]["Max Middle Line Slope"]));
            _DSDBR01_OMPF_max_pr_gap = double.Parse((DSDBR01Prams["OverallMap/QAThresholds"]["Max Pr Gap in Continuity data"]));
            _DSDBR01_OMPF_max_lower_pr = double.Parse((DSDBR01Prams["OverallMap/QAThresholds"]["Max Lower Pr in Continuity data"]));
            _DSDBR01_OMPF_min_upper_pr = double.Parse((DSDBR01Prams["OverallMap/QAThresholds"]["Min Upper Pr in Continuity data"]));
            _DSDBR01_OMPF_min_mode_width_threshold = int.Parse((DSDBR01Prams["OverallMap/QAThresholds"]["Min Mode Width"]));
            _DSDBR01_OMPF_max_mode_width_threshold = int.Parse((DSDBR01Prams["OverallMap/QAThresholds"]["Max Mode Width"]));
            _DSDBR01_OMPF_max_mode_bdc_area_threshold = double.Parse((DSDBR01Prams["OverallMap/QAThresholds"]["Max Mode BDC Area"]));
            _DSDBR01_OMPF_max_mode_bdc_area_x_length_threshold = double.Parse((DSDBR01Prams["OverallMap/QAThresholds"]["Max Mode BDC Area X Length"]));
            _DSDBR01_OMPF_sum_mode_bdc_areas_threshold = double.Parse((DSDBR01Prams["OverallMap/QAThresholds"]["Sum Mode BDC Areas"]));
            _DSDBR01_OMPF_num_mode_bdc_areas_threshold = double.Parse((DSDBR01Prams["OverallMap/QAThresholds"]["Num Mode BDC Areas"]));
            _DSDBR01_OMPF_modal_distortion_angle_threshold = double.Parse((DSDBR01Prams["OverallMap/QAThresholds"]["Max Modal Distortion Angle"]));
            _DSDBR01_OMPF_modal_distortion_angle_x_pos_threshold = double.Parse((DSDBR01Prams["OverallMap/QAThresholds"]["Max Modal Distortion Angle X Position"]));
            _DSDBR01_OMPF_max_mode_line_slope_threshold = double.Parse((DSDBR01Prams["OverallMap/QAThresholds"]["Max Mode Line Slope"]));
            _DSDBR01_OMPF_min_mode_line_slope_threshold = double.Parse((DSDBR01Prams["OverallMap/QAThresholds"]["Min Mode Line Slope"]));
            //Jim@2011.12.9
            _DSDBR01_OMPA_rangex = int.Parse((DSDBR01Prams["OverallMap/PointsAnalyst"]["rangex"]));
            _DSDBR01_OMPA_rangey = int.Parse((DSDBR01Prams["OverallMap/PointsAnalyst"]["rangey"]));
            _DSDBR01_OMPA_cmpValue = int.Parse((DSDBR01Prams["OverallMap/PointsAnalyst"]["cmpValue"]));
            _DSDBR01_OMPA_rangeMaxL = double.Parse((DSDBR01Prams["OverallMap/PointsAnalyst"]["rangeMaxL"]));
            _DSDBR01_OMPA_rangeBranch = double.Parse((DSDBR01Prams["OverallMap/PointsAnalyst"]["rangeBranch"]));
            _DSDBR01_OMPA_minCount = int.Parse((DSDBR01Prams["OverallMap/PointsAnalyst"]["minCount"]));
            _DSDBR01_OMPA_treeLoopNum = int.Parse((DSDBR01Prams["OverallMap/PointsAnalyst"]["treeLoopNum"]));
            _DSDBR01_OMPA_combinParam = int.Parse((DSDBR01Prams["OverallMap/PointsAnalyst"]["combinParam"]));
            _DSDBR01_OMPA_combinParamZ = int.Parse((DSDBR01Prams["OverallMap/PointsAnalyst"]["combinParamZ"]));
            _DSDBR01_OMPA_dArea = int.Parse((DSDBR01Prams["OverallMap/PointsAnalyst"]["dArea"]));
            _DSDBR01_OMPA_yLimit = int.Parse((DSDBR01Prams["OverallMap/PointsAnalyst"]["yLimit"]));
            _DSDBR01_OMPA_distanceLimit = int.Parse((DSDBR01Prams["OverallMap/PointsAnalyst"]["distanceLimit"]));
            _DSDBR01_OMPA_lineWidth = int.Parse((DSDBR01Prams["OverallMap/PointsAnalyst"]["lineWidth"]));
            _DSDBR01_OMPA_minDistToSide = int.Parse((DSDBR01Prams["OverallMap/PointsAnalyst"]["minDistToSide"]));
            _DSDBR01_OMPA_distToTurnLine = int.Parse((DSDBR01Prams["OverallMap/PointsAnalyst"]["distToTurnLine"]));
            _DSDBR01_OMPA_filterSize = int.Parse((DSDBR01Prams["OverallMap/PointsAnalyst"]["filterSize"]));
            _DSDBR01_OMPA_cutLowRearByThisWaferID = int.Parse((DSDBR01Prams["OverallMap/PointsAnalyst"]["cutLowRearByThisWaferID"]));  
            _DSDBR01_OMPA_outputFilterPath = (DSDBR01Prams["OverallMap/PointsAnalyst"]["outputFilterPath"]);

            ////// DSDBR01 Supermode Map QA parameters
            ////_DSDBR01_SMQA_slope_window_size = int.Parse((DSDBR01Prams["QSupermodeMap/QA"]["Slope Window Size"]));
            ////_DSDBR01_SMQA_modal_distortion_min_x = double.Parse((DSDBR01Prams["QSupermodeMap/QA"]["Modal Distortion Min X"]));
            ////_DSDBR01_SMQA_modal_distortion_max_x = double.Parse((DSDBR01Prams["QSupermodeMap/QA"]["Modal Distortion Max X"]));
            ////_DSDBR01_SMQA_modal_distortion_min_y = double.Parse((DSDBR01Prams["QSupermodeMap/QA"]["Modal Distortion Min Y"]));
            ////_DSDBR01_SMQA_ignore_DBC_at_FSC = DSDBR01Prams["QSupermodeMap/QA"]["Ignore BDC Thresholds at front section change"]=="1";

            //////GDM additional Min Max window QA params for Mode Width and Hysteresis analysis
            ////_DSDBR01_SMQA_mode_width_analysis_min_x = double.Parse((DSDBR01Prams["QSupermodeMap/QA"]["Mode Width Analysis Min X"]));
            ////_DSDBR01_SMQA_mode_width_analysis_max_x = double.Parse((DSDBR01Prams["QSupermodeMap/QA"]["Mode Width Analysis Max X"]));
            ////_DSDBR01_SMQA_hysteresis_analysis_min_x = double.Parse((DSDBR01Prams["QSupermodeMap/QA"]["Hysteresis Analysis Min X"]));
            ////_DSDBR01_SMQA_hysteresis_analysis_max_x = double.Parse((DSDBR01Prams["QSupermodeMap/QA"]["Hysteresis Analysis Max X"]));

            //GDM
            ///////////////////////////////////////////////////////

            //// Super mode map Pass Fail Thresholds
            //_DSDBR01_SMPF_ALL_max_mode_width;
            //_DSDBR01_SMPF_ALL_min_mode_width;
            //_DSDBR01_SMPF_ALL_max_mode_borders_removed;
            //_DSDBR01_SMPF_ALL_max_middle_line_rms_value;
            //_DSDBR01_SMPF_ALL_min_middle_line_slope;
            //_DSDBR01_SMPF_ALL_max_middle_line_slope;
            ////double	_DSDBR01_SMPF_ALL_max_continuity_spacing;
            //_DSDBR01_SMPF_ALL_mean_perc_working_region;
            //_DSDBR01_SMPF_ALL_min_perc_working_region;
            //_DSDBR01_SMPF_ALL_modal_distortion_angle;
            //_DSDBR01_SMPF_ALL_modal_distortion_angle_x_pos;
            //_DSDBR01_SMPF_ALL_max_mode_bdc_area;
            //_DSDBR01_SMPF_ALL_max_mode_bdc_area_x_length;
            //_DSDBR01_SMPF_ALL_sum_mode_bdc_areas;
            //_DSDBR01_SMPF_ALL_num_mode_bdc_areas;
            //_DSDBR01_SMPF_ALL_max_mode_line_slope;
            //todo

            // Super mode map Pass Fail Thresholds
            ////_DSDBR01_SMPF_SM0_max_mode_width = int.Parse((DSDBR01Prams["T0"]["Max Mode Width Threshold"]));
            ////_DSDBR01_SMPF_SM0_min_mode_width = int.Parse((DSDBR01Prams["T0"]["Min Mode Width Threshold"]));
            //// _DSDBR01_SMPF_SM0_max_mode_borders_removed = int.Parse((DSDBR01Prams["T0"]["Max Mode Borders Removed"]));
            //// _DSDBR01_SMPF_SM0_max_middle_line_rms_value = double.Parse((DSDBR01Prams["T0"]["Max Middle Line RMS Value"]));
            //// _DSDBR01_SMPF_SM0_min_middle_line_slope = double.Parse((DSDBR01Prams["T0"]["Min Middle Line Slope"]));
            //// _DSDBR01_SMPF_SM0_max_middle_line_slope = double.Parse((DSDBR01Prams["T0"]["Max Middle Line Slope"]));
            //////double	_DSDBR01_SMPF_SM0_max_continuity_spacing;
            //// _DSDBR01_SMPF_SM0_mean_perc_working_region = double.Parse((DSDBR01Prams["T0"]["Mean Percentage Working Region"]));
            //// _DSDBR01_SMPF_SM0_min_perc_working_region = double.Parse((DSDBR01Prams["T0"]["Min Percentage Working Region"]));
            //// _DSDBR01_SMPF_SM0_modal_distortion_angle = double.Parse((DSDBR01Prams["T0"]["Max Modal Distortion Angle"]));
            //// _DSDBR01_SMPF_SM0_modal_distortion_angle_x_pos = double.Parse((DSDBR01Prams["T0"]["Max Modal Distortion Angle X Position"]));
            //// _DSDBR01_SMPF_SM0_max_mode_bdc_area = double.Parse((DSDBR01Prams["T0"]["Max Mode BDC Area"]));
            //// _DSDBR01_SMPF_SM0_max_mode_bdc_area_x_length = double.Parse((DSDBR01Prams["T0"]["Max Mode BDC Area X Length"]));
            //// _DSDBR01_SMPF_SM0_sum_mode_bdc_areas = double.Parse((DSDBR01Prams["T0"]["Sum Mode BDC Areas"]));
            //// _DSDBR01_SMPF_SM0_num_mode_bdc_areas = double.Parse((DSDBR01Prams["T0"]["Num Mode BDC Areas"]));
            //// _DSDBR01_SMPF_SM0_max_mode_line_slope = double.Parse((DSDBR01Prams["T0"]["Max Mode Line Slope"]));
            /////////////////////////////////////////////////////////////

            //// _DSDBR01_SMPF_SM1_max_mode_width = int.Parse((DSDBR01Prams["T1"]["Max Mode Width Threshold"]));
            //// _DSDBR01_SMPF_SM1_min_mode_width = int.Parse((DSDBR01Prams["T1"]["Min Mode Width Threshold"]));
            //// _DSDBR01_SMPF_SM1_max_mode_borders_removed = int.Parse((DSDBR01Prams["T1"]["Max Mode Borders Removed"]));
            //// _DSDBR01_SMPF_SM1_max_middle_line_rms_value = double.Parse((DSDBR01Prams["T1"]["Max Middle Line RMS Value"]));
            //// _DSDBR01_SMPF_SM1_min_middle_line_slope = double.Parse((DSDBR01Prams["T1"]["Min Middle Line Slope"]));
            //// _DSDBR01_SMPF_SM1_max_middle_line_slope = double.Parse((DSDBR01Prams["T1"]["Max Middle Line Slope"]));
            //// //double	_DSDBR01_SMPF_SM1_max_continuity_spacing;
            //// _DSDBR01_SMPF_SM1_mean_perc_working_region = double.Parse((DSDBR01Prams["T1"]["Mean Percentage Working Region"]));
            //// _DSDBR01_SMPF_SM1_min_perc_working_region = double.Parse((DSDBR01Prams["T1"]["Min Percentage Working Region"]));
            //// _DSDBR01_SMPF_SM1_modal_distortion_angle = double.Parse((DSDBR01Prams["T1"]["Max Modal Distortion Angle"]));
            //// _DSDBR01_SMPF_SM1_modal_distortion_angle_x_pos = double.Parse((DSDBR01Prams["T1"]["Max Modal Distortion Angle X Position"]));
            //// _DSDBR01_SMPF_SM1_max_mode_bdc_area = double.Parse((DSDBR01Prams["T1"]["Max Mode BDC Area"]));
            //// _DSDBR01_SMPF_SM1_max_mode_bdc_area_x_length = double.Parse((DSDBR01Prams["T1"]["Max Mode BDC Area X Length"]));
            //// _DSDBR01_SMPF_SM1_sum_mode_bdc_areas = double.Parse((DSDBR01Prams["T1"]["Sum Mode BDC Areas"]));
            //// _DSDBR01_SMPF_SM1_num_mode_bdc_areas = double.Parse((DSDBR01Prams["T1"]["Num Mode BDC Areas"]));
            //// _DSDBR01_SMPF_SM1_max_mode_line_slope = double.Parse((DSDBR01Prams["T1"]["Max Mode Line Slope"]));
            ///////////////////////////////////////////////////////////////

            //// _DSDBR01_SMPF_SM2_max_mode_width = int.Parse((DSDBR01Prams["T2"]["Max Mode Width Threshold"]));
            //// _DSDBR01_SMPF_SM2_min_mode_width = int.Parse((DSDBR01Prams["T2"]["Min Mode Width Threshold"]));
            //// _DSDBR01_SMPF_SM2_max_mode_borders_removed = int.Parse((DSDBR01Prams["T2"]["Max Mode Borders Removed"]));
            //// _DSDBR01_SMPF_SM2_max_middle_line_rms_value = double.Parse((DSDBR01Prams["T2"]["Max Middle Line RMS Value"]));
            //// _DSDBR01_SMPF_SM2_min_middle_line_slope = double.Parse((DSDBR01Prams["T2"]["Min Middle Line Slope"]));
            //// _DSDBR01_SMPF_SM2_max_middle_line_slope = double.Parse((DSDBR01Prams["T2"]["Max Middle Line Slope"]));
            //// //double	_DSDBR01_SMPF_SM2_max_continuity_spacing;
            //// _DSDBR01_SMPF_SM2_mean_perc_working_region = double.Parse((DSDBR01Prams["T2"]["Mean Percentage Working Region"]));
            //// _DSDBR01_SMPF_SM2_min_perc_working_region = double.Parse((DSDBR01Prams["T2"]["Min Percentage Working Region"]));
            //// _DSDBR01_SMPF_SM2_modal_distortion_angle = double.Parse((DSDBR01Prams["T2"]["Max Modal Distortion Angle"]));
            //// _DSDBR01_SMPF_SM2_modal_distortion_angle_x_pos = double.Parse((DSDBR01Prams["T2"]["Max Modal Distortion Angle X Position"]));
            //// _DSDBR01_SMPF_SM2_max_mode_bdc_area = double.Parse((DSDBR01Prams["T2"]["Max Mode BDC Area"]));
            //// _DSDBR01_SMPF_SM2_max_mode_bdc_area_x_length = double.Parse((DSDBR01Prams["T2"]["Max Mode BDC Area X Length"]));
            //// _DSDBR01_SMPF_SM2_sum_mode_bdc_areas = double.Parse((DSDBR01Prams["T2"]["Sum Mode BDC Areas"]));
            //// _DSDBR01_SMPF_SM2_num_mode_bdc_areas = double.Parse((DSDBR01Prams["T2"]["Num Mode BDC Areas"]));
            //// _DSDBR01_SMPF_SM2_max_mode_line_slope = double.Parse((DSDBR01Prams["T2"]["Max Mode Line Slope"]));
            ///////////////////////////////////////////////////////////////

            //// _DSDBR01_SMPF_SM3_max_mode_width = int.Parse((DSDBR01Prams["T3"]["Max Mode Width Threshold"]));
            //// _DSDBR01_SMPF_SM3_min_mode_width = int.Parse((DSDBR01Prams["T3"]["Min Mode Width Threshold"]));
            //// _DSDBR01_SMPF_SM3_max_mode_borders_removed = int.Parse((DSDBR01Prams["T3"]["Max Mode Borders Removed"]));
            //// _DSDBR01_SMPF_SM3_max_middle_line_rms_value = double.Parse((DSDBR01Prams["T3"]["Max Middle Line RMS Value"]));
            //// _DSDBR01_SMPF_SM3_min_middle_line_slope = double.Parse((DSDBR01Prams["T3"]["Min Middle Line Slope"]));
            //// _DSDBR01_SMPF_SM3_max_middle_line_slope = double.Parse((DSDBR01Prams["T3"]["Max Middle Line Slope"]));
            //// //double	_DSDBR01_SMPF_SM3_max_continuity_spacing;
            //// _DSDBR01_SMPF_SM3_mean_perc_working_region = double.Parse((DSDBR01Prams["T3"]["Mean Percentage Working Region"]));
            //// _DSDBR01_SMPF_SM3_min_perc_working_region = double.Parse((DSDBR01Prams["T3"]["Min Percentage Working Region"]));
            //// _DSDBR01_SMPF_SM3_modal_distortion_angle = double.Parse((DSDBR01Prams["T3"]["Max Modal Distortion Angle"]));
            //// _DSDBR01_SMPF_SM3_modal_distortion_angle_x_pos = double.Parse((DSDBR01Prams["T3"]["Max Modal Distortion Angle X Position"]));
            //// _DSDBR01_SMPF_SM3_max_mode_bdc_area = double.Parse((DSDBR01Prams["T3"]["Max Mode BDC Area"]));
            //// _DSDBR01_SMPF_SM3_max_mode_bdc_area_x_length = double.Parse((DSDBR01Prams["T3"]["Max Mode BDC Area X Length"]));
            //// _DSDBR01_SMPF_SM3_sum_mode_bdc_areas = double.Parse((DSDBR01Prams["T3"]["Sum Mode BDC Areas"]));
            //// _DSDBR01_SMPF_SM3_num_mode_bdc_areas = double.Parse((DSDBR01Prams["T3"]["Num Mode BDC Areas"]));
            //// _DSDBR01_SMPF_SM3_max_mode_line_slope = double.Parse((DSDBR01Prams["T3"]["Max Mode Line Slope"]));
            ///////////////////////////////////////////////////////////////

            //// _DSDBR01_SMPF_SM4_max_mode_width = int.Parse((DSDBR01Prams["T4"]["Max Mode Width Threshold"]));
            //// _DSDBR01_SMPF_SM4_min_mode_width = int.Parse((DSDBR01Prams["T4"]["Min Mode Width Threshold"]));
            //// _DSDBR01_SMPF_SM4_max_mode_borders_removed = int.Parse((DSDBR01Prams["T4"]["Max Mode Borders Removed"]));
            //// _DSDBR01_SMPF_SM4_max_middle_line_rms_value = double.Parse((DSDBR01Prams["T4"]["Max Middle Line RMS Value"]));
            //// _DSDBR01_SMPF_SM4_min_middle_line_slope = double.Parse((DSDBR01Prams["T4"]["Min Middle Line Slope"]));
            //// _DSDBR01_SMPF_SM4_max_middle_line_slope = double.Parse((DSDBR01Prams["T4"]["Max Middle Line Slope"]));
            //// //double	_DSDBR01_SMPF_SM4_max_continuity_spacing;
            //// _DSDBR01_SMPF_SM4_mean_perc_working_region = double.Parse((DSDBR01Prams["T4"]["Mean Percentage Working Region"]));
            //// _DSDBR01_SMPF_SM4_min_perc_working_region = double.Parse((DSDBR01Prams["T4"]["Min Percentage Working Region"]));
            //// _DSDBR01_SMPF_SM4_modal_distortion_angle = double.Parse((DSDBR01Prams["T4"]["Max Modal Distortion Angle"]));
            //// _DSDBR01_SMPF_SM4_modal_distortion_angle_x_pos = double.Parse((DSDBR01Prams["T4"]["Max Modal Distortion Angle X Position"]));
            //// _DSDBR01_SMPF_SM4_max_mode_bdc_area = double.Parse((DSDBR01Prams["T4"]["Max Mode BDC Area"]));
            //// _DSDBR01_SMPF_SM4_max_mode_bdc_area_x_length = double.Parse((DSDBR01Prams["T4"]["Max Mode BDC Area X Length"]));
            //// _DSDBR01_SMPF_SM4_sum_mode_bdc_areas = double.Parse((DSDBR01Prams["T4"]["Sum Mode BDC Areas"]));
            //// _DSDBR01_SMPF_SM4_num_mode_bdc_areas = double.Parse((DSDBR01Prams["T4"]["Num Mode BDC Areas"]));
            //// _DSDBR01_SMPF_SM4_max_mode_line_slope = double.Parse((DSDBR01Prams["T4"]["Max Mode Line Slope"]));
            ///////////////////////////////////////////////////////////////

            //// _DSDBR01_SMPF_SM5_max_mode_width = int.Parse((DSDBR01Prams["T5"]["Max Mode Width Threshold"]));
            //// _DSDBR01_SMPF_SM5_min_mode_width = int.Parse((DSDBR01Prams["T5"]["Min Mode Width Threshold"]));
            //// _DSDBR01_SMPF_SM5_max_mode_borders_removed = int.Parse((DSDBR01Prams["T5"]["Max Mode Borders Removed"]));
            //// _DSDBR01_SMPF_SM5_max_middle_line_rms_value = double.Parse((DSDBR01Prams["T5"]["Max Middle Line RMS Value"]));
            //// _DSDBR01_SMPF_SM5_min_middle_line_slope = double.Parse((DSDBR01Prams["T5"]["Min Middle Line Slope"]));
            //// _DSDBR01_SMPF_SM5_max_middle_line_slope = double.Parse((DSDBR01Prams["T5"]["Max Middle Line Slope"]));
            //// //double	_DSDBR01_SMPF_SM5_max_continuity_spacing;
            //// _DSDBR01_SMPF_SM5_mean_perc_working_region = double.Parse((DSDBR01Prams["T5"]["Mean Percentage Working Region"]));
            //// _DSDBR01_SMPF_SM5_min_perc_working_region = double.Parse((DSDBR01Prams["T5"]["Min Percentage Working Region"]));
            //// _DSDBR01_SMPF_SM5_modal_distortion_angle = double.Parse((DSDBR01Prams["T5"]["Max Modal Distortion Angle"]));
            //// _DSDBR01_SMPF_SM5_modal_distortion_angle_x_pos = double.Parse((DSDBR01Prams["T5"]["Max Modal Distortion Angle X Position"]));
            //// _DSDBR01_SMPF_SM5_max_mode_bdc_area = double.Parse((DSDBR01Prams["T5"]["Max Mode BDC Area"]));
            //// _DSDBR01_SMPF_SM5_max_mode_bdc_area_x_length = double.Parse((DSDBR01Prams["T5"]["Max Mode BDC Area X Length"]));
            //// _DSDBR01_SMPF_SM5_sum_mode_bdc_areas = double.Parse((DSDBR01Prams["T5"]["Sum Mode BDC Areas"]));
            //// _DSDBR01_SMPF_SM5_num_mode_bdc_areas = double.Parse((DSDBR01Prams["T5"]["Num Mode BDC Areas"]));
            //// _DSDBR01_SMPF_SM5_max_mode_line_slope = double.Parse((DSDBR01Prams["T5"]["Max Mode Line Slope"]));
            ///////////////////////////////////////////////////////////////

            //// _DSDBR01_SMPF_SM6_max_mode_width = int.Parse((DSDBR01Prams["T6"]["Max Mode Width Threshold"]));
            //// _DSDBR01_SMPF_SM6_min_mode_width = int.Parse((DSDBR01Prams["T6"]["Min Mode Width Threshold"]));
            //// _DSDBR01_SMPF_SM6_max_mode_borders_removed = int.Parse((DSDBR01Prams["T6"]["Max Mode Borders Removed"]));
            //// _DSDBR01_SMPF_SM6_max_middle_line_rms_value = double.Parse((DSDBR01Prams["T6"]["Max Middle Line RMS Value"]));
            //// _DSDBR01_SMPF_SM6_min_middle_line_slope = double.Parse((DSDBR01Prams["T6"]["Min Middle Line Slope"]));
            //// _DSDBR01_SMPF_SM6_max_middle_line_slope = double.Parse((DSDBR01Prams["T6"]["Max Middle Line Slope"]));
            //// //double	_DSDBR01_SMPF_SM6_max_continuity_spacing;
            //// _DSDBR01_SMPF_SM6_mean_perc_working_region = double.Parse((DSDBR01Prams["T6"]["Mean Percentage Working Region"]));
            //// _DSDBR01_SMPF_SM6_min_perc_working_region = double.Parse((DSDBR01Prams["T6"]["Min Percentage Working Region"]));
            //// _DSDBR01_SMPF_SM6_modal_distortion_angle = double.Parse((DSDBR01Prams["T6"]["Max Modal Distortion Angle"]));
            //// _DSDBR01_SMPF_SM6_modal_distortion_angle_x_pos = double.Parse((DSDBR01Prams["T6"]["Max Modal Distortion Angle X Position"]));
            //// _DSDBR01_SMPF_SM6_max_mode_bdc_area = double.Parse((DSDBR01Prams["T6"]["Max Mode BDC Area"]));
            //// _DSDBR01_SMPF_SM6_max_mode_bdc_area_x_length = double.Parse((DSDBR01Prams["T6"]["Max Mode BDC Area X Length"]));
            //// _DSDBR01_SMPF_SM6_sum_mode_bdc_areas = double.Parse((DSDBR01Prams["T6"]["Sum Mode BDC Areas"]));
            //// _DSDBR01_SMPF_SM6_num_mode_bdc_areas = double.Parse((DSDBR01Prams["T6"]["Num Mode BDC Areas"]));
            //// _DSDBR01_SMPF_SM6_max_mode_line_slope = double.Parse((DSDBR01Prams["T6"]["Max Mode Line Slope"]));
            ///////////////////////////////////////////////////////////////

            //// _DSDBR01_SMPF_SM7_max_mode_width = int.Parse((DSDBR01Prams["T7"]["Max Mode Width Threshold"]));
            //// _DSDBR01_SMPF_SM7_min_mode_width = int.Parse((DSDBR01Prams["T7"]["Min Mode Width Threshold"]));
            //// _DSDBR01_SMPF_SM7_max_mode_borders_removed = int.Parse((DSDBR01Prams["T7"]["Max Mode Borders Removed"]));
            //// _DSDBR01_SMPF_SM7_max_middle_line_rms_value = double.Parse((DSDBR01Prams["T7"]["Max Middle Line RMS Value"]));
            //// _DSDBR01_SMPF_SM7_min_middle_line_slope = double.Parse((DSDBR01Prams["T7"]["Min Middle Line Slope"]));
            //// _DSDBR01_SMPF_SM7_max_middle_line_slope = double.Parse((DSDBR01Prams["T7"]["Max Middle Line Slope"]));
            //// //double	_DSDBR01_SMPF_SM7_max_continuity_spacing;
            //// _DSDBR01_SMPF_SM7_mean_perc_working_region = double.Parse((DSDBR01Prams["T7"]["Mean Percentage Working Region"]));
            //// _DSDBR01_SMPF_SM7_min_perc_working_region = double.Parse((DSDBR01Prams["T7"]["Min Percentage Working Region"]));
            //// _DSDBR01_SMPF_SM7_modal_distortion_angle = double.Parse((DSDBR01Prams["T7"]["Max Modal Distortion Angle"]));
            //// _DSDBR01_SMPF_SM7_modal_distortion_angle_x_pos = double.Parse((DSDBR01Prams["T7"]["Max Modal Distortion Angle X Position"]));
            //// _DSDBR01_SMPF_SM7_max_mode_bdc_area = double.Parse((DSDBR01Prams["T7"]["Max Mode BDC Area"]));
            //// _DSDBR01_SMPF_SM7_max_mode_bdc_area_x_length = double.Parse((DSDBR01Prams["T7"]["Max Mode BDC Area X Length"]));
            //// _DSDBR01_SMPF_SM7_sum_mode_bdc_areas = double.Parse((DSDBR01Prams["T7"]["Sum Mode BDC Areas"]));
            //// _DSDBR01_SMPF_SM7_num_mode_bdc_areas = double.Parse((DSDBR01Prams["T7"]["Num Mode BDC Areas"]));
            //// _DSDBR01_SMPF_SM7_max_mode_line_slope = double.Parse((DSDBR01Prams["T7"]["Max Mode Line Slope"]));
            ///////////////////////////////////////////////////////////////

            //// _DSDBR01_SMPF_SM8_max_mode_width = int.Parse((DSDBR01Prams["T8"]["Max Mode Width Threshold"]));
            //// _DSDBR01_SMPF_SM8_min_mode_width = int.Parse((DSDBR01Prams["T8"]["Min Mode Width Threshold"]));
            //// _DSDBR01_SMPF_SM8_max_mode_borders_removed = int.Parse((DSDBR01Prams["T8"]["Max Mode Borders Removed"]));
            //// _DSDBR01_SMPF_SM8_max_middle_line_rms_value = double.Parse((DSDBR01Prams["T8"]["Max Middle Line RMS Value"]));
            //// _DSDBR01_SMPF_SM8_min_middle_line_slope = double.Parse((DSDBR01Prams["T8"]["Min Middle Line Slope"]));
            //// _DSDBR01_SMPF_SM8_max_middle_line_slope = double.Parse((DSDBR01Prams["T8"]["Max Middle Line Slope"]));
            //// //double	_DSDBR01_SMPF_SM8_max_continuity_spacing;
            //// _DSDBR01_SMPF_SM8_mean_perc_working_region = double.Parse((DSDBR01Prams["T8"]["Mean Percentage Working Region"]));
            //// _DSDBR01_SMPF_SM8_min_perc_working_region = double.Parse((DSDBR01Prams["T8"]["Min Percentage Working Region"]));
            //// _DSDBR01_SMPF_SM8_modal_distortion_angle = double.Parse((DSDBR01Prams["T8"]["Max Modal Distortion Angle"]));
            //// _DSDBR01_SMPF_SM8_modal_distortion_angle_x_pos = double.Parse((DSDBR01Prams["T8"]["Max Modal Distortion Angle X Position"]));
            //// _DSDBR01_SMPF_SM8_max_mode_bdc_area = double.Parse((DSDBR01Prams["T8"]["Max Mode BDC Area"]));
            //// _DSDBR01_SMPF_SM8_max_mode_bdc_area_x_length = double.Parse((DSDBR01Prams["T8"]["Max Mode BDC Area X Length"]));
            //// _DSDBR01_SMPF_SM8_sum_mode_bdc_areas = double.Parse((DSDBR01Prams["T8"]["Sum Mode BDC Areas"]));
            //// _DSDBR01_SMPF_SM8_num_mode_bdc_areas = double.Parse((DSDBR01Prams["T8"]["Num Mode BDC Areas"]));
            //// _DSDBR01_SMPF_SM8_max_mode_line_slope = double.Parse((DSDBR01Prams["T8"]["Max Mode Line Slope"]));
            ///////////////////////////////////////////////////////////////

            //// _DSDBR01_SMPF_SM9_max_mode_width = int.Parse((DSDBR01Prams["T9"]["Max Mode Width Threshold"]));
            //// _DSDBR01_SMPF_SM9_min_mode_width = int.Parse((DSDBR01Prams["T9"]["Min Mode Width Threshold"]));
            //// _DSDBR01_SMPF_SM9_max_mode_borders_removed = int.Parse((DSDBR01Prams["T9"]["Max Mode Borders Removed"]));
            //// _DSDBR01_SMPF_SM9_max_middle_line_rms_value = double.Parse((DSDBR01Prams["T9"]["Max Middle Line RMS Value"]));
            //// _DSDBR01_SMPF_SM9_min_middle_line_slope = double.Parse((DSDBR01Prams["T9"]["Min Middle Line Slope"]));
            //// _DSDBR01_SMPF_SM9_max_middle_line_slope = double.Parse((DSDBR01Prams["T9"]["Max Middle Line Slope"]));
            //// //double	_DSDBR01_SMPF_SM9_max_continuity_spacing;
            //// _DSDBR01_SMPF_SM9_mean_perc_working_region = double.Parse((DSDBR01Prams["T9"]["Mean Percentage Working Region"]));
            //// _DSDBR01_SMPF_SM9_min_perc_working_region = double.Parse((DSDBR01Prams["T9"]["Min Percentage Working Region"]));
            //// _DSDBR01_SMPF_SM9_modal_distortion_angle = double.Parse((DSDBR01Prams["T9"]["Max Modal Distortion Angle"]));
            //// _DSDBR01_SMPF_SM9_modal_distortion_angle_x_pos = double.Parse((DSDBR01Prams["T9"]["Max Modal Distortion Angle X Position"]));
            //// _DSDBR01_SMPF_SM9_max_mode_bdc_area = double.Parse((DSDBR01Prams["T9"]["Max Mode BDC Area"]));
            //// _DSDBR01_SMPF_SM9_max_mode_bdc_area_x_length = double.Parse((DSDBR01Prams["T9"]["Max Mode BDC Area X Length"]));
            //// _DSDBR01_SMPF_SM9_sum_mode_bdc_areas = double.Parse((DSDBR01Prams["T9"]["Sum Mode BDC Areas"]));
            //// _DSDBR01_SMPF_SM9_num_mode_bdc_areas = double.Parse((DSDBR01Prams["T9"]["Num Mode BDC Areas"]));
            //// _DSDBR01_SMPF_SM9_max_mode_line_slope = double.Parse((DSDBR01Prams["T9"]["Max Mode Line Slope"]));
            //// ///////////////////////////////////////////////////////

            //// ////////////////////////////////////////////////////

            ////// DSDBR01 Coarse Frequency parameters
            //// _DSDBR01_CFREQ_poly_coeffs_abspath = (string)((DSDBR01Prams["CoarseFrequency"]["Polynomial Coefficients Filepath"]));
            //// _DSDBR01_CFREQ_num_freq_meas_per_op = int.Parse((DSDBR01Prams["CoarseFrequency"]["Number of frequency measurements to average per operating point"]));
            //// _DSDBR01_CFREQ_num_power_ratio_meas_per_op = int.Parse((DSDBR01Prams["CoarseFrequency"]["Number of power ratio measurements to average per operating point"]));
            //// _DSDBR01_CFREQ_max_stable_freq_error = double.Parse((DSDBR01Prams["CoarseFrequency"]["Max frequency error [GHz] at stable operating point"]));
            //// _DSDBR01_CFREQ_sample_point_settle_time = int.Parse((DSDBR01Prams["CoarseFrequency"]["Sample point settle time [ms]"]));
            //// _DSDBR01_CFREQ_write_sample_points_to_file = DSDBR01Prams["CoarseFrequency"]["Write sample points to file"]=="1";

            ////// DSDBR01 ITU Grid
            //// _DSDBR01_ITUGRID_centre_freq = double.Parse(((DSDBR01Prams["ITUGrid"]["Centre Frequency [GHz]"])));
            //// _DSDBR01_ITUGRID_step_freq = double.Parse(((DSDBR01Prams["ITUGrid"]["Frequency Spacing [GHz]"])));
            //// _DSDBR01_ITUGRID_channel_count = int.Parse(((DSDBR01Prams["ITUGrid"]["Number of channels"])));
            //// _DSDBR01_ITUGRID_abspath = (string)((DSDBR01Prams["ITUGrid"]["ITU Grid Filepath"]));
            //// _DSDBR01_ESTIMATESITUGRID_abspath = (string)((DSDBR01Prams["ITUGrid"]["Estimates ITU Grid Filepath"]));

            ////// DSDBR01 ITU Grid characterisation parameters
            //// _DSDBR01_CHAR_num_freq_meas_per_op = int.Parse(((DSDBR01Prams["Characterisation"]["Number of frequency measurements to average per operating point"])));
            //// _DSDBR01_CHAR_freq_accuracy_of_wavemeter = double.Parse(((DSDBR01Prams["Characterisation"]["Frequency accuracy of wavemeter [+/-GHz]"])));
            //// _DSDBR01_CHAR_op_settle_time = int.Parse(((DSDBR01Prams["Characterisation"]["Operating point settle time [ms]"])));
            //// _DSDBR01_CHAR_itusearch_scheme = int.Parse(((DSDBR01Prams["Characterisation"]["ITU search scheme"])));
            //// _DSDBR01_CHAR_itusearch_nextguess = int.Parse(((DSDBR01Prams["Characterisation"]["ITU search next guess type"])));
            //// _DSDBR01_CHAR_itusearch_resamples = int.Parse(((DSDBR01Prams["Characterisation"]["ITU search number of samples"])));
            //// _DSDBR01_CHAR_num_power_ratio_meas_per_op = int.Parse(((DSDBR01Prams["Characterisation"]["Number of power ratio measurements to average per operating point"])));
            //// _DSDBR01_CHAR_max_stable_freq_error = double.Parse(((DSDBR01Prams["Characterisation"]["Max frequency error [GHz] at stable operating point"])));
            //// _DSDBR01_CHAR_write_duff_points_to_file = DSDBR01Prams["Characterisation"]["If ITU search failed, write last guess to filet"]=="1";
            //// _DSDBR01_CHAR_prevent_front_section_switch = DSDBR01Prams["Characterisation"]["Prevent front section switch after problem finding ITU point"]=="1";
            //// _DSDBR01_CHAR_fakefreq_Iramp_module_name = (string)((DSDBR01Prams["Characterisation"]["Fake Frequency Iramp Module Name"]));
            //// _DSDBR01_CHAR_Iramp_vs_fakefreq_abspath = (string)((DSDBR01Prams["Characterisation"]["Iramp Vs Fake Frequency Filepath"]));
            //// _DSDBR01_CHAR_fakefreq_max_error = double.Parse(((DSDBR01Prams["Characterisation"]["Fake Frequency Max Error from generated noise"])));


        }

        #region Parameters
        // Logging
        public int _DSDBR01_LOGGING_LEVEL = 4;
        public string _DSDBR01_LOGFILEPATHNAME;
        public int _DSDBR01_LOGFILEMAXSIZE;
        public int _DSDBR01_LOGGING_ON = 1;

      
        // DSDBR01 Base parameters
        public string _DSDBR01_BASE_results_directory;
        //public bool _DSDBR01_BASE_stub_hardware_calls;

        // DSDBR01 Common Data Collection parameters
        public string _DSDBR01_CMDC_gain_module_name;
        public string _DSDBR01_CMDC_soa_module_name;
        public string _DSDBR01_CMDC_rear_module_name;
        public string _DSDBR01_CMDC_phase_module_name;
        public string _DSDBR01_CMDC_front1_module_name;
        public string _DSDBR01_CMDC_front2_module_name;
        public string _DSDBR01_CMDC_front3_module_name;
        public string _DSDBR01_CMDC_front4_module_name;
        public string _DSDBR01_CMDC_front5_module_name;
        public string _DSDBR01_CMDC_front6_module_name;
        public string _DSDBR01_CMDC_front7_module_name;
        public string _DSDBR01_CMDC_front8_module_name;
        public string _DSDBR01_CMDC_306II_module_name;
        public int _DSDBR01_CMDC_306II_direct_channel;
        public int _DSDBR01_CMDC_306II_filtered_channel;
        public string _DSDBR01_CMDC_306EE_module_name;
        public int _DSDBR01_CMDC_306EE_photodiode1_channel;
        public int _DSDBR01_CMDC_306EE_photodiode2_channel;
        public int _DSDBR01_CMDC_wavemeter_retries;
        // DSDBR01 Overall Map Data Collection parameters
        public string _DSDBR01_OMDC_ramp_direction;
        public bool _DSDBR01_OMDC_forward_and_reverse;
        public bool _DSDBR01_OMDC_measure_photodiode_currents;
        public bool _DSDBR01_OMDC_write_filtered_power_maps;
        public string _DSDBR01_OMDC_rear_currents_abspath;
        public string _DSDBR01_OMDC_front_currents_abspath;
        public int _DSDBR01_OMDC_source_delay;
        public int _DSDBR01_OMDC_measure_delay;
        public bool _DSDBR01_OMDC_overwrite_existing_files;

        // DSDBR01 Overall Map Boundary Detection parameters
        public bool _DSDBR01_OMBD_read_boundaries_from_file;
        public bool _DSDBR01_OMBD_write_boundaries_to_file;
        public int _DSDBR01_OMBD_median_filter_rank;
        public double _DSDBR01_OMBD_max_deltaPr_in_sm;
        public int _DSDBR01_OMBD_min_points_width_of_sm;
        public int _DSDBR01_OMBD_ml_moving_ave_filter_n;
        public int _DSDBR01_OMBD_ml_min_length;
        public bool _DSDBR01_OMBD_ml_extend_to_corner;
        public int _DSDBR01_OMBD_assumption_points;
        public int _DSDBR01_OMBD_max_number_of_points_height_of_noise_plot;
        public int _DSDBR01_OMBD_max_detecting_height_of_incontinuity;


        // DSDBR01 Supermode Map Data Collection parameters
        public string _DSDBR01_SMDC_ramp_direction;
        public string _DSDBR01_SMDC_phase_currents_abspath;
        public bool _DSDBR01_SMDC_measure_photodiode_currents;
        public bool _DSDBR01_SMDC_write_filtered_power_maps;
        public int _DSDBR01_SMDC_source_delay;
        public int _DSDBR01_SMDC_measure_delay;
        public bool _DSDBR01_SMDC_overwrite_existing_files;

        // DSDBR01 Supermode Map Boundary Detection parameters
        public bool _DSDBR01_SMBD_read_boundaries_from_file;
        public bool _DSDBR01_SMBD_write_boundaries_to_file;
        public bool _DSDBR01_SMBD_write_middle_of_upper_lines_to_file;
        public bool _DSDBR01_SMBD_write_middle_of_lower_lines_to_file;
        public bool _DSDBR01_SMBD_write_debug_files;
        public bool _DSDBR01_SMBD_extrapolate_to_map_edges;
        public int _DSDBR01_SMBD_median_filter_rank;
        public double _DSDBR01_SMBD_max_deltaPr_in_lm;
        public int _DSDBR01_SMBD_min_points_width_of_lm;
        public int _DSDBR01_SMBD_max_points_width_of_lm;
        public int _DSDBR01_SMBD_ml_moving_ave_filter_n;
        public int _DSDBR01_SMBD_ml_min_length;
        public double _DSDBR01_SMBD_vertical_percent_shift;

        public double _DSDBR01_SMBD_zprc_hysteresis_upper_threshold;
        public double _DSDBR01_SMBD_zprc_hysteresis_lower_threshold;
        public double _DSDBR01_SMBD_zprc_max_hysteresis_rise;
        public int _DSDBR01_SMBD_zprc_min_points_separation;
        public int _DSDBR01_SMBD_zprc_max_points_to_reverse_jump;
        public int _DSDBR01_SMBD_zprc_max_hysteresis_points;
        public string _DSDBR01_SMBD_ramp_abspath;

        // DSDBR01 Forward Supermode Map Boundary Detection parameters
        public int _DSDBR01_SMBDFWD_I_ramp_boundaries_max;
        public int _DSDBR01_SMBDFWD_boundaries_min_separation;
        public double _DSDBR01_SMBDFWD_min_d2Pr_dI2_zero_cross_jump_size;
        public double _DSDBR01_SMBDFWD_max_sum_of_squared_distances_from_Pr_I_line;
        public double _DSDBR01_SMBDFWD_min_slope_change_of_Pr_I_lines;
        public int _DSDBR01_SMBDFWD_linking_max_distance_between_linked_jumps;
        public double _DSDBR01_SMBDFWD_linking_min_sharpness;
        public double _DSDBR01_SMBDFWD_linking_max_sharpness;
        public int _DSDBR01_SMBDFWD_linking_max_gap_between_double_lines;
        public int _DSDBR01_SMBDFWD_linking_min_gap_between_double_lines;
        public int _DSDBR01_SMBDFWD_linking_near_bottom_row;
        public double _DSDBR01_SMBDFWD_linking_sharpness_near_bottom_row;
        public int _DSDBR01_SMBDFWD_linking_max_distance_near_bottom_row;
        public int _DSDBR01_SMBDFWD_linking_max_distance_to_single_unlinked_jump;
        public int _DSDBR01_SMBDFWD_linking_max_y_distance_to_link;
        public double _DSDBR01_SMBDFWD_linking_sharpness_ramp_split;

        // DSDBR01 Reverse Supermode Map Boundary Detection parameters
        public int _DSDBR01_SMBDREV_I_ramp_boundaries_max;
        public int _DSDBR01_SMBDREV_boundaries_min_separation;
        public double _DSDBR01_SMBDREV_min_d2Pr_dI2_zero_cross_jump_size;
        public double _DSDBR01_SMBDREV_max_sum_of_squared_distances_from_Pr_I_line;
        public double _DSDBR01_SMBDREV_min_slope_change_of_Pr_I_lines;
        public int _DSDBR01_SMBDREV_linking_max_distance_between_linked_jumps;
        public double _DSDBR01_SMBDREV_linking_min_sharpness;
        public double _DSDBR01_SMBDREV_linking_max_sharpness;
        public int _DSDBR01_SMBDREV_linking_max_gap_between_double_lines;
        public int _DSDBR01_SMBDREV_linking_min_gap_between_double_lines;
        public int _DSDBR01_SMBDREV_linking_near_bottom_row;
        public double _DSDBR01_SMBDREV_linking_sharpness_near_bottom_row;
        public int _DSDBR01_SMBDREV_linking_max_distance_near_bottom_row;
        public int _DSDBR01_SMBDREV_linking_max_distance_to_single_unlinked_jump;
        public int _DSDBR01_SMBDREV_linking_max_y_distance_to_link;
        public double _DSDBR01_SMBDREV_linking_sharpness_ramp_split;

        // DSDBR01 Overall Map Quantitative Analysis parameters
        public int _DSDBR01_OMQA_slope_window_size;
        public double _DSDBR01_OMQA_modal_distortion_min_x;
        public double _DSDBR01_OMQA_modal_distortion_max_x;
        public double _DSDBR01_OMQA_modal_distortion_min_y;

        // DSDBR01 Overall Map Pass Fail Thresholds
        public int _DSDBR01_OMPF_max_mode_borders_removed_threshold;
        public double _DSDBR01_OMPF_max_middle_line_rms_value;
        public double _DSDBR01_OMPF_min_middle_line_slope;
        public double _DSDBR01_OMPF_max_middle_line_slope;
        public double _DSDBR01_OMPF_max_pr_gap;
        public double _DSDBR01_OMPF_max_lower_pr;
        public double _DSDBR01_OMPF_min_upper_pr;
        public int _DSDBR01_OMPF_min_mode_width_threshold;
        public int _DSDBR01_OMPF_max_mode_width_threshold;
        public double _DSDBR01_OMPF_max_mode_bdc_area_threshold;
        public double _DSDBR01_OMPF_max_mode_bdc_area_x_length_threshold;
        public double _DSDBR01_OMPF_sum_mode_bdc_areas_threshold;
        public double _DSDBR01_OMPF_num_mode_bdc_areas_threshold;
        public double _DSDBR01_OMPF_modal_distortion_angle_threshold;
        public double _DSDBR01_OMPF_modal_distortion_angle_x_pos_threshold;
        public double _DSDBR01_OMPF_max_mode_line_slope_threshold;
        public double _DSDBR01_OMPF_min_mode_line_slope_threshold;

        // DSDBR01 Overall Map PointsAnalyst    Jim@2011.12.9
        public int _DSDBR01_OMPA_rangex;
        public int _DSDBR01_OMPA_rangey;
        public int _DSDBR01_OMPA_cmpValue;
        public double _DSDBR01_OMPA_rangeMaxL;
        public double _DSDBR01_OMPA_rangeBranch;
        public int _DSDBR01_OMPA_minCount;
        public int _DSDBR01_OMPA_treeLoopNum;
        public int _DSDBR01_OMPA_combinParam;
        public int _DSDBR01_OMPA_combinParamZ;
        public int _DSDBR01_OMPA_dArea;
        public int _DSDBR01_OMPA_yLimit;
        public int _DSDBR01_OMPA_distanceLimit;
        public int _DSDBR01_OMPA_lineWidth;
        public int _DSDBR01_OMPA_minDistToSide;
        public int _DSDBR01_OMPA_distToTurnLine;
        public int _DSDBR01_OMPA_filterSize;
        public int _DSDBR01_OMPA_cutLowRearByThisWaferID;
        public string _DSDBR01_OMPA_outputFilterPath;
        // DSDBR01 Supermode Map QA parameters
        public int _DSDBR01_SMQA_slope_window_size;
        public double _DSDBR01_SMQA_modal_distortion_min_x;
        public double _DSDBR01_SMQA_modal_distortion_max_x;
        public double _DSDBR01_SMQA_modal_distortion_min_y;
        public bool _DSDBR01_SMQA_ignore_DBC_at_FSC;

        //GDM additional Min Max window QA params for Mode Width and Hysteresis analysis
        public double _DSDBR01_SMQA_mode_width_analysis_min_x;
        public double _DSDBR01_SMQA_mode_width_analysis_max_x;
        public double _DSDBR01_SMQA_hysteresis_analysis_min_x;
        public double _DSDBR01_SMQA_hysteresis_analysis_max_x;
        //GDM
        ///////////////////////////////////////////////////////


        // Super mode map Pass Fail Thresholds
        public int _DSDBR01_SMPF_ALL_max_mode_width;
        public int _DSDBR01_SMPF_ALL_min_mode_width;
        public int _DSDBR01_SMPF_ALL_max_mode_borders_removed;
        public double _DSDBR01_SMPF_ALL_max_middle_line_rms_value;
        public double _DSDBR01_SMPF_ALL_min_middle_line_slope;
        public double _DSDBR01_SMPF_ALL_max_middle_line_slope;
        //double	_DSDBR01_SMPF_ALL_max_continuity_spacing;
        public double _DSDBR01_SMPF_ALL_mean_perc_working_region;
        public double _DSDBR01_SMPF_ALL_min_perc_working_region;
        public double _DSDBR01_SMPF_ALL_modal_distortion_angle;
        public double _DSDBR01_SMPF_ALL_modal_distortion_angle_x_pos;
        public double _DSDBR01_SMPF_ALL_max_mode_bdc_area;
        public double _DSDBR01_SMPF_ALL_max_mode_bdc_area_x_length;
        public double _DSDBR01_SMPF_ALL_sum_mode_bdc_areas;
        public double _DSDBR01_SMPF_ALL_num_mode_bdc_areas;
        public double _DSDBR01_SMPF_ALL_max_mode_line_slope;

        // Super mode map Pass Fail Thresholds
        public int _DSDBR01_SMPF_SM0_max_mode_width;
        public int _DSDBR01_SMPF_SM0_min_mode_width;
        public int _DSDBR01_SMPF_SM0_max_mode_borders_removed;
        public double _DSDBR01_SMPF_SM0_max_middle_line_rms_value;
        public double _DSDBR01_SMPF_SM0_min_middle_line_slope;
        public double _DSDBR01_SMPF_SM0_max_middle_line_slope;
        //double	_DSDBR01_SMPF_SM0_max_continuity_spacing;
        public double _DSDBR01_SMPF_SM0_mean_perc_working_region;
        public double _DSDBR01_SMPF_SM0_min_perc_working_region;
        public double _DSDBR01_SMPF_SM0_modal_distortion_angle;
        public double _DSDBR01_SMPF_SM0_modal_distortion_angle_x_pos;
        public double _DSDBR01_SMPF_SM0_max_mode_bdc_area;
        public double _DSDBR01_SMPF_SM0_max_mode_bdc_area_x_length;
        public double _DSDBR01_SMPF_SM0_sum_mode_bdc_areas;
        public double _DSDBR01_SMPF_SM0_num_mode_bdc_areas;
        public double _DSDBR01_SMPF_SM0_max_mode_line_slope;

        /////////////////////////////////////////////////////////

        public int _DSDBR01_SMPF_SM1_max_mode_width;
        public int _DSDBR01_SMPF_SM1_min_mode_width;
        public int _DSDBR01_SMPF_SM1_max_mode_borders_removed;
        public double _DSDBR01_SMPF_SM1_max_middle_line_rms_value;
        public double _DSDBR01_SMPF_SM1_min_middle_line_slope;
        public double _DSDBR01_SMPF_SM1_max_middle_line_slope;
        //double	_DSDBR01_SMPF_SM1_max_continuity_spacing;
        public double _DSDBR01_SMPF_SM1_mean_perc_working_region;
        public double _DSDBR01_SMPF_SM1_min_perc_working_region;
        public double _DSDBR01_SMPF_SM1_modal_distortion_angle;
        public double _DSDBR01_SMPF_SM1_modal_distortion_angle_x_pos;
        public double _DSDBR01_SMPF_SM1_max_mode_bdc_area;
        public double _DSDBR01_SMPF_SM1_max_mode_bdc_area_x_length;
        public double _DSDBR01_SMPF_SM1_sum_mode_bdc_areas;
        public double _DSDBR01_SMPF_SM1_num_mode_bdc_areas;
        public double _DSDBR01_SMPF_SM1_max_mode_line_slope;

        /////////////////////////////////////////////////////////

        public int _DSDBR01_SMPF_SM2_max_mode_width;
        public int _DSDBR01_SMPF_SM2_min_mode_width;
        public int _DSDBR01_SMPF_SM2_max_mode_borders_removed;
        public double _DSDBR01_SMPF_SM2_max_middle_line_rms_value;
        public double _DSDBR01_SMPF_SM2_min_middle_line_slope;
        public double _DSDBR01_SMPF_SM2_max_middle_line_slope;
        //double	_DSDBR01_SMPF_SM2_max_continuity_spacing;
        public double _DSDBR01_SMPF_SM2_mean_perc_working_region;
        public double _DSDBR01_SMPF_SM2_min_perc_working_region;
        public double _DSDBR01_SMPF_SM2_modal_distortion_angle;
        public double _DSDBR01_SMPF_SM2_modal_distortion_angle_x_pos;
        public double _DSDBR01_SMPF_SM2_max_mode_bdc_area;
        public double _DSDBR01_SMPF_SM2_max_mode_bdc_area_x_length;
        public double _DSDBR01_SMPF_SM2_sum_mode_bdc_areas;
        public double _DSDBR01_SMPF_SM2_num_mode_bdc_areas;
        public double _DSDBR01_SMPF_SM2_max_mode_line_slope;

        /////////////////////////////////////////////////////////

        public int _DSDBR01_SMPF_SM3_max_mode_width;
        public int _DSDBR01_SMPF_SM3_min_mode_width;
        public int _DSDBR01_SMPF_SM3_max_mode_borders_removed;
        public double _DSDBR01_SMPF_SM3_max_middle_line_rms_value;
        public double _DSDBR01_SMPF_SM3_min_middle_line_slope;
        public double _DSDBR01_SMPF_SM3_max_middle_line_slope;
        //double	_DSDBR01_SMPF_SM3_max_continuity_spacing;
        public double _DSDBR01_SMPF_SM3_mean_perc_working_region;
        public double _DSDBR01_SMPF_SM3_min_perc_working_region;
        public double _DSDBR01_SMPF_SM3_modal_distortion_angle;
        public double _DSDBR01_SMPF_SM3_modal_distortion_angle_x_pos;
        public double _DSDBR01_SMPF_SM3_max_mode_bdc_area;
        public double _DSDBR01_SMPF_SM3_max_mode_bdc_area_x_length;
        public double _DSDBR01_SMPF_SM3_sum_mode_bdc_areas;
        public double _DSDBR01_SMPF_SM3_num_mode_bdc_areas;
        public double _DSDBR01_SMPF_SM3_max_mode_line_slope;

        /////////////////////////////////////////////////////////

        public int _DSDBR01_SMPF_SM4_max_mode_width;
        public int _DSDBR01_SMPF_SM4_min_mode_width;
        public int _DSDBR01_SMPF_SM4_max_mode_borders_removed;
        public double _DSDBR01_SMPF_SM4_max_middle_line_rms_value;
        public double _DSDBR01_SMPF_SM4_min_middle_line_slope;
        public double _DSDBR01_SMPF_SM4_max_middle_line_slope;
        //double	_DSDBR01_SMPF_SM4_max_continuity_spacing;
        public double _DSDBR01_SMPF_SM4_mean_perc_working_region;
        public double _DSDBR01_SMPF_SM4_min_perc_working_region;
        public double _DSDBR01_SMPF_SM4_modal_distortion_angle;
        public double _DSDBR01_SMPF_SM4_modal_distortion_angle_x_pos;
        public double _DSDBR01_SMPF_SM4_max_mode_bdc_area;
        public double _DSDBR01_SMPF_SM4_max_mode_bdc_area_x_length;
        public double _DSDBR01_SMPF_SM4_sum_mode_bdc_areas;
        public double _DSDBR01_SMPF_SM4_num_mode_bdc_areas;
        public double _DSDBR01_SMPF_SM4_max_mode_line_slope;

        /////////////////////////////////////////////////////////

        public int _DSDBR01_SMPF_SM5_max_mode_width;
        public int _DSDBR01_SMPF_SM5_min_mode_width;
        public int _DSDBR01_SMPF_SM5_max_mode_borders_removed;
        public double _DSDBR01_SMPF_SM5_max_middle_line_rms_value;
        public double _DSDBR01_SMPF_SM5_min_middle_line_slope;
        public double _DSDBR01_SMPF_SM5_max_middle_line_slope;
        //double	_DSDBR01_SMPF_SM5_max_continuity_spacing;
        public double _DSDBR01_SMPF_SM5_mean_perc_working_region;
        public double _DSDBR01_SMPF_SM5_min_perc_working_region;
        public double _DSDBR01_SMPF_SM5_modal_distortion_angle;
        public double _DSDBR01_SMPF_SM5_modal_distortion_angle_x_pos;
        public double _DSDBR01_SMPF_SM5_max_mode_bdc_area;
        public double _DSDBR01_SMPF_SM5_max_mode_bdc_area_x_length;
        public double _DSDBR01_SMPF_SM5_sum_mode_bdc_areas;
        public double _DSDBR01_SMPF_SM5_num_mode_bdc_areas;
        public double _DSDBR01_SMPF_SM5_max_mode_line_slope;

        /////////////////////////////////////////////////////////

        public int _DSDBR01_SMPF_SM6_max_mode_width;
        public int _DSDBR01_SMPF_SM6_min_mode_width;
        public int _DSDBR01_SMPF_SM6_max_mode_borders_removed;
        public double _DSDBR01_SMPF_SM6_max_middle_line_rms_value;
        public double _DSDBR01_SMPF_SM6_min_middle_line_slope;
        public double _DSDBR01_SMPF_SM6_max_middle_line_slope;
        //double	_DSDBR01_SMPF_SM6_max_continuity_spacing;
        public double _DSDBR01_SMPF_SM6_mean_perc_working_region;
        public double _DSDBR01_SMPF_SM6_min_perc_working_region;
        public double _DSDBR01_SMPF_SM6_modal_distortion_angle;
        public double _DSDBR01_SMPF_SM6_modal_distortion_angle_x_pos;
        public double _DSDBR01_SMPF_SM6_max_mode_bdc_area;
        public double _DSDBR01_SMPF_SM6_max_mode_bdc_area_x_length;
        public double _DSDBR01_SMPF_SM6_sum_mode_bdc_areas;
        public double _DSDBR01_SMPF_SM6_num_mode_bdc_areas;
        public double _DSDBR01_SMPF_SM6_max_mode_line_slope;

        /////////////////////////////////////////////////////////

        public int _DSDBR01_SMPF_SM7_max_mode_width;
        public int _DSDBR01_SMPF_SM7_min_mode_width;
        public int _DSDBR01_SMPF_SM7_max_mode_borders_removed;
        public double _DSDBR01_SMPF_SM7_max_middle_line_rms_value;
        public double _DSDBR01_SMPF_SM7_min_middle_line_slope;
        public double _DSDBR01_SMPF_SM7_max_middle_line_slope;
        //double	_DSDBR01_SMPF_SM7_max_continuity_spacing;
        public double _DSDBR01_SMPF_SM7_mean_perc_working_region;
        public double _DSDBR01_SMPF_SM7_min_perc_working_region;
        public double _DSDBR01_SMPF_SM7_modal_distortion_angle;
        public double _DSDBR01_SMPF_SM7_modal_distortion_angle_x_pos;
        public double _DSDBR01_SMPF_SM7_max_mode_bdc_area;
        public double _DSDBR01_SMPF_SM7_max_mode_bdc_area_x_length;
        public double _DSDBR01_SMPF_SM7_sum_mode_bdc_areas;
        public double _DSDBR01_SMPF_SM7_num_mode_bdc_areas;
        public double _DSDBR01_SMPF_SM7_max_mode_line_slope;

        /////////////////////////////////////////////////////////

        public int _DSDBR01_SMPF_SM8_max_mode_width;
        public int _DSDBR01_SMPF_SM8_min_mode_width;
        public int _DSDBR01_SMPF_SM8_max_mode_borders_removed;
        public double _DSDBR01_SMPF_SM8_max_middle_line_rms_value;
        public double _DSDBR01_SMPF_SM8_min_middle_line_slope;
        public double _DSDBR01_SMPF_SM8_max_middle_line_slope;
        //double	_DSDBR01_SMPF_SM8_max_continuity_spacing;
        public double _DSDBR01_SMPF_SM8_mean_perc_working_region;
        public double _DSDBR01_SMPF_SM8_min_perc_working_region;
        public double _DSDBR01_SMPF_SM8_modal_distortion_angle;
        public double _DSDBR01_SMPF_SM8_modal_distortion_angle_x_pos;
        public double _DSDBR01_SMPF_SM8_max_mode_bdc_area;
        public double _DSDBR01_SMPF_SM8_max_mode_bdc_area_x_length;
        public double _DSDBR01_SMPF_SM8_sum_mode_bdc_areas;
        public double _DSDBR01_SMPF_SM8_num_mode_bdc_areas;
        public double _DSDBR01_SMPF_SM8_max_mode_line_slope;

        /////////////////////////////////////////////////////////

        public int _DSDBR01_SMPF_SM9_max_mode_width;
        public int _DSDBR01_SMPF_SM9_min_mode_width;
        public int _DSDBR01_SMPF_SM9_max_mode_borders_removed;
        public double _DSDBR01_SMPF_SM9_max_middle_line_rms_value;
        public double _DSDBR01_SMPF_SM9_min_middle_line_slope;
        public double _DSDBR01_SMPF_SM9_max_middle_line_slope;
        //double	_DSDBR01_SMPF_SM9_max_continuity_spacing;
        public double _DSDBR01_SMPF_SM9_mean_perc_working_region;
        public double _DSDBR01_SMPF_SM9_min_perc_working_region;
        public double _DSDBR01_SMPF_SM9_modal_distortion_angle;
        public double _DSDBR01_SMPF_SM9_modal_distortion_angle_x_pos;
        public double _DSDBR01_SMPF_SM9_max_mode_bdc_area;
        public double _DSDBR01_SMPF_SM9_max_mode_bdc_area_x_length;
        public double _DSDBR01_SMPF_SM9_sum_mode_bdc_areas;
        public double _DSDBR01_SMPF_SM9_num_mode_bdc_areas;
        public double _DSDBR01_SMPF_SM9_max_mode_line_slope;

        /////////////////////////////////////////////////////////
        //
        //////////////////////////////////////////////////////

        // DSDBR01 Coarse Frequency parameters
        public string _DSDBR01_CFREQ_poly_coeffs_abspath;
        public int _DSDBR01_CFREQ_num_freq_meas_per_op;
        public int _DSDBR01_CFREQ_num_power_ratio_meas_per_op;
        public double _DSDBR01_CFREQ_max_stable_freq_error;
        public int _DSDBR01_CFREQ_sample_point_settle_time;
        public bool _DSDBR01_CFREQ_write_sample_points_to_file;

        // DSDBR01 ITU Grid
        public double _DSDBR01_ITUGRID_centre_freq;
        public double _DSDBR01_ITUGRID_step_freq;
        public int _DSDBR01_ITUGRID_channel_count;
        public string _DSDBR01_ITUGRID_abspath;
        public string _DSDBR01_ESTIMATESITUGRID_abspath;

        // DSDBR01 ITU Grid characterisation parameters
        public int _DSDBR01_CHAR_num_freq_meas_per_op;
        public double _DSDBR01_CHAR_freq_accuracy_of_wavemeter;
        public int _DSDBR01_CHAR_op_settle_time;
        public int _DSDBR01_CHAR_itusearch_scheme;
        public int _DSDBR01_CHAR_itusearch_nextguess;
        public int _DSDBR01_CHAR_itusearch_resamples;
        public int _DSDBR01_CHAR_num_power_ratio_meas_per_op;
        public double _DSDBR01_CHAR_max_stable_freq_error;
        public bool _DSDBR01_CHAR_write_duff_points_to_file;
        public bool _DSDBR01_CHAR_prevent_front_section_switch;
        public string _DSDBR01_CHAR_fakefreq_Iramp_module_name;
        public string _DSDBR01_CHAR_Iramp_vs_fakefreq_abspath;
        public double _DSDBR01_CHAR_fakefreq_max_error;

        // DSDBR01 TEC settling parameters used during characterisation
        public int _DSDBR01_CHARTEC_time_window_size;
        public double _DSDBR01_CHARTEC_max_temp_deviation;
        public int _DSDBR01_CHARTEC_num_temp_readings;
        public int _DSDBR01_CHARTEC_max_settle_time;

        #endregion

        #region Default

        // Logging
        const string REGNAME_LOGGING_LEVEL = "Log Level";
        const string REGNAME_LOGFILEPATHNAME = "Log Absolute Path File Name";
        const string REGNAME_LOGFILEMAXSIZE = "Log File Max Size in KBytes";
        const string REGNAME_LOGGING_ON = "Logging On";
        const int DEFAULT_LOGGING_LEVEL = 4;
        const string DEFAULT_LOGFILEPATHNAME = "C:\\Program Files\\PXIT\\CLoseGrid\\Log\\CLoseGridLog.txt";
        const int DEFAULT_LOGFILEMAXSIZE = 1000;
        const int DEFAULT_LOGGING_ON = 1;

        // Results
        const string REGNAME_RESULTS = "Results File Path";
        const string DEFAULT_RESULTS_FILEPATH = "C:\\Program Files\\PXIT\\CLoseGrid";

        const string CG_DSDBR01_BASE_REGKEY = "SOFTWARE\\PXIT\\CLose-Grid 4.0\\Software Configuration\\DSDBR01";


        // DSDBR01 Base parameters

        const string REGNAME_DSDBR01_BASE_RESULTS_DIR = "Results Directory";
        const string DEFAULT_DSDBR01_BASE_RESULTS_DIR = "C:\\Program Files\\PXIT\\CLose-Grid 4.0\\Results";

        const string REGNAME_DSDBR01_BASE_STUB_HARDWARE_CALLS = "Stub hardware calls";
        const bool DEFAULT_DSDBR01_BASE_STUB_HARDWARE_CALLS = false;

        // DSDBR01 Common Data Collection parameters

        const string RELATIVE_REGKEY_DSDBR01_CMDC = "\\Data Collection";

        const string REGNAME_DSDBR01_CMDC_GAIN_MODULE_NAME = "Gain section module name";
        const string DEFAULT_DSDBR01_CMDC_GAIN_MODULE_NAME = "Gain";

        const string REGNAME_DSDBR01_CMDC_SOA_MODULE_NAME = "SOA section module name";
        const string DEFAULT_DSDBR01_CMDC_SOA_MODULE_NAME = "SOA";

        const string REGNAME_DSDBR01_CMDC_REAR_MODULE_NAME = "Rear section module name";
        const string DEFAULT_DSDBR01_CMDC_REAR_MODULE_NAME = "Rear";

        const string REGNAME_DSDBR01_CMDC_PHASE_MODULE_NAME = "Phase section module name";
        const string DEFAULT_DSDBR01_CMDC_PHASE_MODULE_NAME = "Phase";

        const string REGNAME_DSDBR01_CMDC_FRONT1_MODULE_NAME = "Front section 1 module name";
        const string DEFAULT_DSDBR01_CMDC_FRONT1_MODULE_NAME = "LFS1";

        const string REGNAME_DSDBR01_CMDC_FRONT2_MODULE_NAME = "Front section 2 module name";
        const string DEFAULT_DSDBR01_CMDC_FRONT2_MODULE_NAME = "LFS2";

        const string REGNAME_DSDBR01_CMDC_FRONT3_MODULE_NAME = "Front section 3 module name";
        const string DEFAULT_DSDBR01_CMDC_FRONT3_MODULE_NAME = "LFS3";

        const string REGNAME_DSDBR01_CMDC_FRONT4_MODULE_NAME = "Front section 4 module name";
        const string DEFAULT_DSDBR01_CMDC_FRONT4_MODULE_NAME = "LFS4";

        const string REGNAME_DSDBR01_CMDC_FRONT5_MODULE_NAME = "Front section 5 module name";
        const string DEFAULT_DSDBR01_CMDC_FRONT5_MODULE_NAME = "LFS5";

        const string REGNAME_DSDBR01_CMDC_FRONT6_MODULE_NAME = "Front section 6 module name";
        const string DEFAULT_DSDBR01_CMDC_FRONT6_MODULE_NAME = "LFS6";

        const string REGNAME_DSDBR01_CMDC_FRONT7_MODULE_NAME = "Front section 7 module name";
        const string DEFAULT_DSDBR01_CMDC_FRONT7_MODULE_NAME = "LFS7";

        const string REGNAME_DSDBR01_CMDC_FRONT8_MODULE_NAME = "Front section 8 module name";
        const string DEFAULT_DSDBR01_CMDC_FRONT8_MODULE_NAME = "LFS8";

        const string REGNAME_DSDBR01_CMDC_306II_MODULE_NAME = "306II module name";
        const string DEFAULT_DSDBR01_CMDC_306II_MODULE_NAME = "PowerMeter II";

        const string REGNAME_DSDBR01_CMDC_306II_DIRECT_CHANNEL = "306II Direct Power Channel";
        const int DEFAULT_DSDBR01_CMDC_306II_DIRECT_CHANNEL = 1;

        const string REGNAME_DSDBR01_CMDC_306II_FILTERED_CHANNEL = "306II Filtered Power Channel";
        const int DEFAULT_DSDBR01_CMDC_306II_FILTERED_CHANNEL = 2;

        const string REGNAME_DSDBR01_CMDC_306EE_MODULE_NAME = "306EE module name";
        const string DEFAULT_DSDBR01_CMDC_306EE_MODULE_NAME = "PowerMeter EE";

        const string REGNAME_DSDBR01_CMDC_306EE_PHOTODIODE1_CHANNEL = "306EE Photodiode 1 Channel";
        const int DEFAULT_DSDBR01_CMDC_306EE_PHOTODIODE1_CHANNEL = 1;

        const string REGNAME_DSDBR01_CMDC_306EE_PHOTODIODE2_CHANNEL = "306EE Photodiode 2 Channel";
        const int DEFAULT_DSDBR01_CMDC_306EE_PHOTODIODE2_CHANNEL = 2;

        const string REGNAME_DSDBR01_CMDC_WAVEMETER_RETRIES = "Wavemeter number of reset and retries";
        const int DEFAULT_DSDBR01_CMDC_WAVEMETER_RETRIES = 0;

        // DSDBR01 Overall Map Data Collection parameters

        const string RELATIVE_REGKEY_DSDBR01_OMDC = "\\Overall Map\\Data Collection";

        const string REGNAME_DSDBR01_OMDC_RAMP_DIRECTION = "Ramp Direction";
        const string DEFAULT_DSDBR01_OMDC_RAMP_DIRECTION = "R";

        const string REGNAME_DSDBR01_OMDC_FORWARD_AND_REVERSE = "Forward and Reverse";
        const bool DEFAULT_DSDBR01_OMDC_FORWARD_AND_REVERSE = true;

        const string REGNAME_DSDBR01_OMDC_MEASURE_PHOTODIODE_CURRENTS = "Measure Photodiode Currents";
        const bool DEFAULT_DSDBR01_OMDC_MEASURE_PHOTODIODE_CURRENTS = true;

        const string REGNAME_DSDBR01_OMDC_WRITE_FILTERED_POWER_MAPS = "Write Filtered Power Maps";
        const bool DEFAULT_DSDBR01_OMDC_WRITE_FILTERED_POWER_MAPS = true;

        const string REGNAME_DSDBR01_OMDC_REAR_CURRENTS_ABSPATH = "Rear currents filepath";
        const string DEFAULT_DSDBR01_OMDC_REAR_CURRENTS_ABSPATH = "C:\\Program Files\\PXIT\\CLose-Grid 4.0\\defaults\\Ir_OverallMap_DSDBR01_default.csv";

        const string REGNAME_DSDBR01_OMDC_FRONT_CURRENTS_ABSPATH = "Front currents filepath";
        const string DEFAULT_DSDBR01_OMDC_FRONT_CURRENTS_ABSPATH = "C:\\Program Files\\PXIT\\CLose-Grid 4.0\\defaults\\If_OverallMap_DSDBR01_default.csv";

        const string REGNAME_DSDBR01_OMDC_SOURCE_DELAY = "Source Delay";
        const int DEFAULT_DSDBR01_OMDC_SOURCE_DELAY = 0;

        const string REGNAME_DSDBR01_OMDC_MEASURE_DELAY = "Measure Delay";
        const int DEFAULT_DSDBR01_OMDC_MEASURE_DELAY = 0;

        const string REGNAME_DSDBR01_OMDC_OVERWRITE_EXISTING_FILES = "Overwrite existing files";
        const bool DEFAULT_DSDBR01_OMDC_OVERWRITE_EXISTING_FILES = true;


        // DSDBR01 Overall Map Boundary Detection parameters

        const string RELATIVE_REGKEY_DSDBR01_OMBD = "\\Overall Map\\Boundary Detection";

        const string REGNAME_DSDBR01_OMBD_READ_BOUNDARIES_FROM_FILE = "Read Boundaries From File";
        const bool DEFAULT_DSDBR01_OMBD_READ_BOUNDARIES_FROM_FILE = false;

        const string REGNAME_DSDBR01_OMBD_WRITE_BOUNDARIES_TO_FILE = "Write Boundaries To File";
        const bool DEFAULT_DSDBR01_OMBD_WRITE_BOUNDARIES_TO_FILE = false;

        const string REGNAME_DSDBR01_OMBD_MEDIAN_FILTER_RANK = "Median filter rank";
        const int DEFAULT_DSDBR01_OMBD_MEDIAN_FILTER_RANK = 3;

        const string REGNAME_DSDBR01_OMBD_MAX_DELTAPR_IN_SM = "Max deltaPr in a supermode";
        const double DEFAULT_DSDBR01_OMBD_MAX_DELTAPR_IN_SM = 0.01;

        const string REGNAME_DSDBR01_OMBD_MIN_POINTS_WIDTH_OF_SM = "Min number of points width of a supermode";
        const int DEFAULT_DSDBR01_OMBD_MIN_POINTS_WIDTH_OF_SM = 3;

        const string REGNAME_DSDBR01_OMBD_ML_MOVING_AVERAGE_FILTER_N = "N for moving average filter on middle line";
        const int DEFAULT_DSDBR01_OMBD_ML_MOVING_AVERAGE_FILTER_N = 5;

        const string REGNAME_DSDBR01_OMBD_ML_MIN_LENGTH = "Minimum length of middle line";
        const int DEFAULT_DSDBR01_OMBD_ML_MIN_LENGTH = 100;

        const string REGNAME_DSDBR01_OMBD_ML_EXTEND_TO_CORNER = "Extend middle line to corner if still in supermode";
        const bool DEFAULT_DSDBR01_OMBD_ML_EXTEND_TO_CORNER = false;

        // DSDBR01 Supermode Map collection parameters

        const string RELATIVE_REGKEY_DSDBR01_SMDC = "\\Supermode Map\\Data Collection";

        const string REGNAME_DSDBR01_SMDC_RAMP_DIRECTION = "Ramp direction";
        const string DEFAULT_DSDBR01_SMDC_RAMP_DIRECTION = "P";

        const string REGNAME_DSDBR01_SMDC_PHASE_CURRENTS_ABSPATH = "Phase currents filepath";
        const string DEFAULT_DSDBR01_SMDC_PHASE_CURRENTS_ABSPATH = "C:\\Program Files\\PXIT\\CLose-Grid 4.0\\defaults\\Ip_SMMap_DSDBR01_default.csv";

        const string REGNAME_DSDBR01_SMDC_MEASURE_PHOTODIODE_CURRENTS = "Measure photodiode currents";
        const bool DEFAULT_DSDBR01_SMDC_MEASURE_PHOTODIODE_CURRENTS = false;

        const string REGNAME_DSDBR01_SMDC_WRITE_FILTERED_POWER_MAPS = "Write Filtered Power Maps";
        const bool DEFAULT_DSDBR01_SMDC_WRITE_FILTERED_POWER_MAPS = true;

        const string REGNAME_DSDBR01_SMDC_SOURCE_DELAY = "Source Delay";
        const int DEFAULT_DSDBR01_SMDC_SOURCE_DELAY = 0;

        const string REGNAME_DSDBR01_SMDC_MEASURE_DELAY = "Measure Delay";
        const int DEFAULT_DSDBR01_SMDC_MEASURE_DELAY = 0;

        const string REGNAME_DSDBR01_SMDC_OVERWRITE_EXISTING_FILES = "Overwrite existing files";
        const bool DEFAULT_DSDBR01_SMDC_OVERWRITE_EXISTING_FILES = true;



        // DSDBR01 Supermode Map Boundary Detection parameters

        const string RELATIVE_REGKEY_DSDBR01_SMBD = "\\Supermode Map\\Boundary Detection";

        const string REGNAME_DSDBR01_SMBD_READ_BOUNDARIES_FROM_FILE = "Read Boundaries From File";
        const bool DEFAULT_DSDBR01_SMBD_READ_BOUNDARIES_FROM_FILE = false;

        const string REGNAME_DSDBR01_SMBD_WRITE_BOUNDARIES_TO_FILE = "Write Boundaries To File";
        const bool DEFAULT_DSDBR01_SMBD_WRITE_BOUNDARIES_TO_FILE = false;

        const string REGNAME_DSDBR01_SMBD_write_middle_of_lower_lines_to_file = "Write Middle of Lower Lines To File";
        const bool DEFAULT_DSDBR01_SMBD_write_middle_of_lower_lines_to_file = false;

        const string REGNAME_DSDBR01_SMBD_write_middle_of_upper_lines_to_file = "Write Middle of Upper Lines To File";
        const bool DEFAULT_DSDBR01_SMBD_write_middle_of_upper_lines_to_file = false;

        const string REGNAME_DSDBR01_SMBD_WRITE_DEBUG_FILES = "Write Debug Files";
        const bool DEFAULT_DSDBR01_SMBD_WRITE_DEBUG_FILES = false;

        const string REGNAME_DSDBR01_SMBD_extrapolate_to_map_edges = "Extrapolate to map edges";
        const bool DEFAULT_DSDBR01_SMBD_extrapolate_to_map_edges = false;

        const string REGNAME_DSDBR01_SMBD_MEDIAN_FILTER_RANK = "Median filter rank";
        const int DEFAULT_DSDBR01_SMBD_MEDIAN_FILTER_RANK = 3;

        const string REGNAME_DSDBR01_SMBD_MAX_DELTAPR_IN_LM = "Max deltaPr in a longitudinal mode";
        const double DEFAULT_DSDBR01_SMBD_MAX_DELTAPR_IN_LM = 0.001;

        const string REGNAME_DSDBR01_SMBD_MIN_POINTS_WIDTH_OF_LM = "Min number of points width of a longitudinal mode";
        const int DEFAULT_DSDBR01_SMBD_MIN_POINTS_WIDTH_OF_LM = 3;

        const string REGNAME_DSDBR01_SMBD_MAX_POINTS_WIDTH_OF_LM = "Max number of points width of a longitudinal mode";
        const int DEFAULT_DSDBR01_SMBD_MAX_POINTS_WIDTH_OF_LM = 40;

        const string REGNAME_DSDBR01_SMBD_ML_MOVING_AVERAGE_FILTER_N = "N for moving average filter on middle line";
        const int DEFAULT_DSDBR01_SMBD_ML_MOVING_AVERAGE_FILTER_N = 5;

        const string REGNAME_DSDBR01_SMBD_ML_MIN_LENGTH = "Minimum length of middle line";
        const int DEFAULT_DSDBR01_SMBD_ML_MIN_LENGTH = 50;

        const string REGNAME_DSDBR01_SMBD_VERTICAL_PERCENT_SHIFT = "Vertical Percent Shift for Frequency Middle";
        const double DEFAULT_DSDBR01_SMBD_VERTICAL_PERCENT_SHIFT = 0.0;

        const string REGNAME_DSDBR01_SMBD_ZPRC_HYSTERESIS_UPPER_THRESHOLD = "ZPRC hysteresis upper threshold";
        const int DEFAULT_DSDBR01_SMBD_ZPRC_HYSTERESIS_UPPER_THRESHOLD = 5;

        const string REGNAME_DSDBR01_SMBD_ZPRC_HYSTERESIS_LOWER_THRESHOLD = "ZPRC hysteresis lower threshold";
        const int DEFAULT_DSDBR01_SMBD_ZPRC_HYSTERESIS_LOWER_THRESHOLD = 5;

        const string REGNAME_DSDBR01_SMBD_ZPRC_MAX_HYSTERESIS_RISE = "ZPRC max hysteresis rise";
        const double DEFAULT_DSDBR01_SMBD_ZPRC_MAX_HYSTERESIS_RISE = 0.0;

        const string REGNAME_DSDBR01_SMBD_ZPRC_MIN_POINTS_SEPARATION = "ZPRC min number of points separation";
        const int DEFAULT_DSDBR01_SMBD_ZPRC_MIN_POINTS_SEPARATION = 20;

        const string REGNAME_DSDBR01_SMBD_ZPRC_MAX_POINTS_TO_REVERSE_JUMP = "ZPRC max number of points from reverse jump";
        const int DEFAULT_DSDBR01_SMBD_ZPRC_MAX_POINTS_TO_REVERSE_JUMP = 2;

        const string REGNAME_DSDBR01_SMBD_ZPRC_MAX_HYSTERESIS_POINTS = "ZPRC max number of hysteresis points";
        const int DEFAULT_DSDBR01_SMBD_ZPRC_MAX_HYSTERESIS_POINTS = 40;

        const string REGNAME_DSDBR01_SMBD_RAMP_ABSPATH = "Ramp filepath";
        const string DEFAULT_DSDBR01_SMBD_RAMP_ABSPATH = "no_file";


        // DSDBR01 Forward Supermode Map Boundary Detection parameters

        const string RELATIVE_REGKEY_DSDBR01_SMBDFWD = "\\Supermode Map\\Boundary Detection\\Forward Map";

        const string REGNAME_DSDBR01_SMBDFWD_I_RAMP_BOUNDARIES = "Max number of boundaries on a single ramp";
        const int DEFAULT_DSDBR01_SMBDFWD_I_RAMP_BOUNDARIES = 3;

        const string REGNAME_DSDBR01_SMBDFWD_BOUNDARIES_MIN_SEPARATION = "Min separation of boundaries on a single ramp";
        const int DEFAULT_DSDBR01_SMBDFWD_BOUNDARIES_MIN_SEPARATION = 20;

        const string REGNAME_DSDBR01_SMBDFWD_MIN_D2PR_DI2_ZERO_CROSS_JUMP_SIZE = "Min d2Pr/dI2 zero cross jump size";
        const double DEFAULT_DSDBR01_SMBDFWD_MIN_D2PR_DI2_ZERO_CROSS_JUMP_SIZE = 0.1;

        const string REGNAME_DSDBR01_SMBDFWD_MAX_SUM_SQUARED_DIST_FROM_PR_I_LINE = "Max sum of squared distances from Pr/I line";
        const int DEFAULT_DSDBR01_SMBDFWD_MAX_SUM_SQUARED_DIST_FROM_PR_I_LINE = 0;

        const string REGNAME_DSDBR01_SMBDFWD_MIN_SLOPE_CHANGE_OF_PR_I_LINE = "Min slope change of Pr/I line";
        const int DEFAULT_DSDBR01_SMBDFWD_MIN_SLOPE_CHANGE_OF_PR_I_LINE = 0;

        const string REGNAME_DSDBR01_SMBDFWD_LINKING_MAX_DISTANCE_BETWEEN_LINKED_JUMPS = "Max distance between linked jumps";
        const int DEFAULT_DSDBR01_SMBDFWD_LINKING_MAX_DISTANCE_BETWEEN_LINKED_JUMPS = 80;

        const string REGNAME_DSDBR01_SMBDFWD_LINKING_MIN_SHARPNESS = "Linking min sharpness";
        const int DEFAULT_DSDBR01_SMBDFWD_LINKING_MIN_SHARPNESS = 1;

        const string REGNAME_DSDBR01_SMBDFWD_LINKING_MAX_SHARPNESS = "Linking max sharpness";
        const int DEFAULT_DSDBR01_SMBDFWD_LINKING_MAX_SHARPNESS = 3;

        const string REGNAME_DSDBR01_SMBDFWD_LINKING_MAX_GAP_BETWEEN_DOUBLE_LINES = "Linking max gap between double lines";
        const int DEFAULT_DSDBR01_SMBDFWD_LINKING_MAX_GAP_BETWEEN_DOUBLE_LINES = 30;

        const string REGNAME_DSDBR01_SMBDFWD_LINKING_MIN_GAP_BETWEEN_DOUBLE_LINES = "Linking min gap between double lines";
        const int DEFAULT_DSDBR01_SMBDFWD_LINKING_MIN_GAP_BETWEEN_DOUBLE_LINES = 4;

        const string REGNAME_DSDBR01_SMBDFWD_LINKING_NEAR_BOTTOM = "Linking near bottom row";
        const int DEFAULT_DSDBR01_SMBDFWD_LINKING_NEAR_BOTTOM = 10;

        const string REGNAME_DSDBR01_SMBDFWD_LINKING_SHARPNESS_NEAR_BOTTOM = "Linking sharpness near bottom";
        const double DEFAULT_DSDBR01_SMBDFWD_LINKING_SHARPNESS_NEAR_BOTTOM = 0.1;

        const string REGNAME_DSDBR01_SMBDFWD_LINKING_MAX_DISTANCE_BETWEEN_LINKS_NEAR_BOTTOM = "Linking max distance near bottom";
        const int DEFAULT_DSDBR01_SMBDFWD_LINKING_MAX_DISTANCE_BETWEEN_LINKS_NEAR_BOTTOM = 40;

        const string REGNAME_DSDBR01_SMBDFWD_LINKING_MAX_DISTANCE_TO_LINK_SINGLE_UNLINKED_JUMP = "Linking max distance to single unlinked jump";
        const int DEFAULT_DSDBR01_SMBDFWD_LINKING_MAX_DISTANCE_TO_LINK_SINGLE_UNLINKED_JUMP = 30;

        const string REGNAME_DSDBR01_SMBDFWD_LINKING_MAX_Y_DISTANCE_TO_LINK = "Linking max y distance to link";
        const int DEFAULT_DSDBR01_SMBDFWD_LINKING_MAX_Y_DISTANCE_TO_LINK = 10;

        const string REGNAME_DSDBR01_SMBDFWD_LINKING_SHARPNESS_RAMP_SPLIT = "Linking sharpness ramp split";
        const double DEFAULT_DSDBR01_SMBDFWD_LINKING_SHARPNESS_RAMP_SPLIT = 0.5;


        // DSDBR01 Reverse Supermode Map Boundary Detection parameters

        const string RELATIVE_REGKEY_DSDBR01_SMBDREV = "\\Supermode Map\\Boundary Detection\\Reverse Map";

        const string REGNAME_DSDBR01_SMBDREV_I_RAMP_BOUNDARIES = "Max number of boundaries on a single ramp";
        const int DEFAULT_DSDBR01_SMBDREV_I_RAMP_BOUNDARIES = 3;

        const string REGNAME_DSDBR01_SMBDREV_BOUNDARIES_MIN_SEPARATION = "Min separation of boundaries on a single ramp";
        const int DEFAULT_DSDBR01_SMBDREV_BOUNDARIES_MIN_SEPARATION = 20;

        const string REGNAME_DSDBR01_SMBDREV_MIN_D2PR_DI2_ZERO_CROSS_JUMP_SIZE = "Min d2Pr/dI2 zero cross jump size";
        const double DEFAULT_DSDBR01_SMBDREV_MIN_D2PR_DI2_ZERO_CROSS_JUMP_SIZE = 0.1;

        const string REGNAME_DSDBR01_SMBDREV_MAX_SUM_SQUARED_DIST_FROM_PR_I_LINE = "Max sum of squared distances from Pr/I line";
        const int DEFAULT_DSDBR01_SMBDREV_MAX_SUM_SQUARED_DIST_FROM_PR_I_LINE = 0;

        const string REGNAME_DSDBR01_SMBDREV_MIN_SLOPE_CHANGE_OF_PR_I_LINE = "Min slope change of Pr/I line";
        const int DEFAULT_DSDBR01_SMBDREV_MIN_SLOPE_CHANGE_OF_PR_I_LINE = 0;

        const string REGNAME_DSDBR01_SMBDREV_LINKING_MAX_DISTANCE_BETWEEN_LINKED_JUMPS = "Max distance between linked jumps";
        const int DEFAULT_DSDBR01_SMBDREV_LINKING_MAX_DISTANCE_BETWEEN_LINKED_JUMPS = 80;

        const string REGNAME_DSDBR01_SMBDREV_LINKING_MIN_SHARPNESS = "Linking min sharpness";
        const int DEFAULT_DSDBR01_SMBDREV_LINKING_MIN_SHARPNESS = 1;

        const string REGNAME_DSDBR01_SMBDREV_LINKING_MAX_SHARPNESS = "Linking max sharpness";
        const int DEFAULT_DSDBR01_SMBDREV_LINKING_MAX_SHARPNESS = 3;

        const string REGNAME_DSDBR01_SMBDREV_LINKING_MAX_GAP_BETWEEN_DOUBLE_LINES = "Linking max gap between double lines";
        const int DEFAULT_DSDBR01_SMBDREV_LINKING_MAX_GAP_BETWEEN_DOUBLE_LINES = 30;

        const string REGNAME_DSDBR01_SMBDREV_LINKING_MIN_GAP_BETWEEN_DOUBLE_LINES = "Linking min gap between double lines";
        const int DEFAULT_DSDBR01_SMBDREV_LINKING_MIN_GAP_BETWEEN_DOUBLE_LINES = 4;

        const string REGNAME_DSDBR01_SMBDREV_LINKING_NEAR_BOTTOM = "Linking near bottom row";
        const int DEFAULT_DSDBR01_SMBDREV_LINKING_NEAR_BOTTOM = 10;

        const string REGNAME_DSDBR01_SMBDREV_LINKING_SHARPNESS_NEAR_BOTTOM = "Linking sharpness near bottom";
        const double DEFAULT_DSDBR01_SMBDREV_LINKING_SHARPNESS_NEAR_BOTTOM = 0.1;

        const string REGNAME_DSDBR01_SMBDREV_LINKING_MAX_DISTANCE_BETWEEN_LINKS_NEAR_BOTTOM = "Linking max distance near bottom";
        const int DEFAULT_DSDBR01_SMBDREV_LINKING_MAX_DISTANCE_BETWEEN_LINKS_NEAR_BOTTOM = 40;

        const string REGNAME_DSDBR01_SMBDREV_LINKING_MAX_DISTANCE_TO_LINK_SINGLE_UNLINKED_JUMP = "Linking max distance to single unlinked jump";
        const int DEFAULT_DSDBR01_SMBDREV_LINKING_MAX_DISTANCE_TO_LINK_SINGLE_UNLINKED_JUMP = 30;

        const string REGNAME_DSDBR01_SMBDREV_LINKING_MAX_Y_DISTANCE_TO_LINK = "Linking max y distance to link";
        const int DEFAULT_DSDBR01_SMBDREV_LINKING_MAX_Y_DISTANCE_TO_LINK = 10;

        const string REGNAME_DSDBR01_SMBDREV_LINKING_SHARPNESS_RAMP_SPLIT = "Linking sharpness ramp split";
        const double DEFAULT_DSDBR01_SMBDREV_LINKING_SHARPNESS_RAMP_SPLIT = 0.5;


        // DSDBR01 Overall Map Quantitative Analysis parameters
        const string RELATIVE_REGKEY_DSDBR01_OMQA = "\\Overall Map\\QA";

        const string REGNAME_DSDBR01_OMQA_SLOPE_WINDOW_SIZE = "Slope Window Size";
        const int DEFAULT_DSDBR01_OMQA_SLOPE_WINDOW_SIZE = 5;

        const string REGNAME_DSDBR01_OMQA_MODAL_DISTORTION_MIN_X = "Modal Distortion Min X";
        const int DEFAULT_DSDBR01_OMQA_MODAL_DISTORTION_MIN_X = 20;

        const string REGNAME_DSDBR01_OMQA_MODAL_DISTORTION_MAX_X = "Modal Distortion Max X";
        const int DEFAULT_DSDBR01_OMQA_MODAL_DISTORTION_MAX_X = 80;

        const string REGNAME_DSDBR01_OMQA_MODAL_DISTORTION_MIN_Y = "Modal Distortion Min Y";
        const int DEFAULT_DSDBR01_OMQA_MODAL_DISTORTION_MIN_Y = 20;


        // DSDBR01 Overall Map Pass Fail Parameters
        const string RELATIVE_REGKEY_DSDBR01_OMPF = "\\Overall Map\\QAThresholds";
        const string REGNAME_DSDBR01_OMPF_MAX_MIDDLE_LINE_RMS_VALUE = "Max Middle Line RMS Value";
        const int DEFAULT_DSDBR01_OMPF_MAX_MIDDLE_LINE_RMS_VALUE = 5;

        const string REGNAME_DSDBR01_OMPF_MIN_MIDDLE_LINE_SLOPE = "Min Middle Line Slope";
        const int DEFAULT_DSDBR01_OMPF_MIN_MIDDLE_LINE_SLOPE = 0;

        const string REGNAME_DSDBR01_OMPF_MAX_MIDDLE_LINE_SLOPE = "Max Middle Line Slope";
        const int DEFAULT_DSDBR01_OMPF_MAX_MIDDLE_LINE_SLOPE = 90;

        const string REGNAME_DSDBR01_OMPF_MAX_PR_GAP = "Max Pr Gap in Continuity data";
        const double DEFAULT_DSDBR01_OMPF_MAX_PR_GAP = 0.01;

        const string REGNAME_DSDBR01_OMPF_MIN_UPPER_PR = "Min Upper Pr in Continuity data";
        const double DEFAULT_DSDBR01_OMPF_MIN_UPPER_PR = 0.6;

        const string REGNAME_DSDBR01_OMPF_MAX_LOWER_PR = "Max Lower Pr in Continuity data";
        const double DEFAULT_DSDBR01_OMPF_MAX_LOWER_PR = 0.4;

        const string REGNAME_DSDBR01_OMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD = "Max Number of Mode Borders Removed";
        const int DEFAULT_DSDBR01_OMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD = 0;

        const string REGNAME_DSDBR01_OMPF_MIN_MODE_WIDTH_THRESHOLD = "Min Mode Width";
        const int DEFAULT_DSDBR01_OMPF_MIN_MODE_WIDTH_THRESHOLD = 5;

        const string REGNAME_DSDBR01_OMPF_MAX_MODE_WIDTH_THRESHOLD = "Max Mode Width";
        const int DEFAULT_DSDBR01_OMPF_MAX_MODE_WIDTH_THRESHOLD = 40;

        const string REGNAME_DSDBR01_OMPF_MAX_MODE_BDC_AREA_THRESHOLD = "Max Mode BDC Area";
        const int DEFAULT_DSDBR01_OMPF_MAX_MODE_BDC_AREA_THRESHOLD = 30;

        const string REGNAME_DSDBR01_OMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD = "Max Mode BDC Area X Length";
        const int DEFAULT_DSDBR01_OMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD = 20;

        const string REGNAME_DSDBR01_OMPF_SUM_MODE_BDC_AREAS_THRESHOLD = "Sum Mode BDC Areas";
        const int DEFAULT_DSDBR01_OMPF_SUM_MODE_BDC_AREAS_THRESHOLD = 60;

        const string REGNAME_DSDBR01_OMPF_NUM_MODE_BDC_AREAS_THRESHOLD = "Num Mode BDC Areas";
        const int DEFAULT_DSDBR01_OMPF_NUM_MODE_BDC_AREAS_THRESHOLD = 8;

        const string REGNAME_DSDBR01_OMPF_MODAL_DISTORTION_ANGLE_THRESHOLD = "Max Modal Distortion Angle";
        const int DEFAULT_DSDBR01_OMPF_MODAL_DISTORTION_ANGLE_THRESHOLD = 17;

        const string REGNAME_DSDBR01_OMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD = "Max Modal Distortion Angle X Position";
        const int DEFAULT_DSDBR01_OMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD = 80;

        const string REGNAME_DSDBR01_OMPF_MAX_MODE_LINE_SLOPE_THRESHOLD = "Max Mode Line Slope";
        const int DEFAULT_DSDBR01_OMPF_MAX_MODE_LINE_SLOPE_THRESHOLD = 90;

        const string REGNAME_DSDBR01_OMPF_MIN_MODE_LINE_SLOPE_THRESHOLD = "Min Mode Line Slope";
        const int DEFAULT_DSDBR01_OMPF_MIN_MODE_LINE_SLOPE_THRESHOLD = -90;

        ////////////////////////////////////////////////////////////////////////////////////////
        ///
        // DSDBR01 Super Map Quantitative Analysis Threshold
        const string RELATIVE_REGKEY_DSDBR01_SMQA = "\\Supermode Map\\QA";

        const string REGNAME_DSDBR01_SMQA_SLOPE_WINDOW_SIZE = "Slope Window Size";
        const int DEFAULT_DSDBR01_SMQA_SLOPE_WINDOW_SIZE = 5;

        const string REGNAME_DSDBR01_SMQA_MODAL_DISTORTION_MIN_X = "Modal Distortion Min X";
        const int DEFAULT_DSDBR01_SMQA_MODAL_DISTORTION_MIN_X = 20;

        const string REGNAME_DSDBR01_SMQA_MODAL_DISTORTION_MAX_X = "Modal Distortion Max X";
        const int DEFAULT_DSDBR01_SMQA_MODAL_DISTORTION_MAX_X = 80;

        const string REGNAME_DSDBR01_SMQA_MODAL_DISTORTION_MIN_Y = "Modal Distortion Min Y";
        const int DEFAULT_DSDBR01_SMQA_MODAL_DISTORTION_MIN_Y = 20;

        const string REGNAME_DSDBR01_SMQA_IGNORE_BDC_AT_FSC = "Ignore BDC Thresholds at front section change";
        const bool DEFAULT_DSDBR01_SMQA_IGNORE_BDC_AT_FSC = true;

        //GDM 31/10/06 additional defs for Mode Width and Hysteresis anaysis reg entries
        const string REGNAME_DSDBR01_SMQA_MODE_WIDTH_ANALYSIS_MIN_X = "Mode Width Analysis Min X";
        const int DEFAULT_DSDBR01_SMQA_MODE_WIDTH_ANALYSIS_MIN_X = 20;

        const string REGNAME_DSDBR01_SMQA_MODE_WIDTH_ANALYSIS_MAX_X = "Mode Width Analysis Max X";
        const int DEFAULT_DSDBR01_SMQA_MODE_WIDTH_ANALYSIS_MAX_X = 80;

        const string REGNAME_DSDBR01_SMQA_HYSTERESIS_ANALYSIS_MIN_X = "Hysteresis Analysis Min X";
        const int DEFAULT_DSDBR01_SMQA_HYSTERESIS_ANALYSIS_MIN_X = 20;

        const string REGNAME_DSDBR01_SMQA_HYSTERESIS_ANALYSIS_MAX_X = "Hysteresis Analysis Max X";
        const int DEFAULT_DSDBR01_SMQA_HYSTERESIS_ANALYSIS_MAX_X = 80;
        //GDM
        ////////////////////////////////////////////////////////////////////////////////////////
        ///
        // Super Mode Pass Fail Analysis
        //
        const int FIRST_SM = 0;
        const int LAST_SM = 9;

        const string RELATIVE_REGKEY_DSDBR01_SMPF_ALL = "\\Supermode Map\\QA\\QAThresholds";

        const string RELATIVE_REGKEY_DSDBR01_SMPF_SM0 = "\\Supermode Map\\QA\\QAThresholds\\0";
        const string RELATIVE_REGKEY_DSDBR01_SMPF_SM1 = "\\Supermode Map\\QA\\QAThresholds\\1";
        const string RELATIVE_REGKEY_DSDBR01_SMPF_SM2 = "\\Supermode Map\\QA\\QAThresholds\\2";
        const string RELATIVE_REGKEY_DSDBR01_SMPF_SM3 = "\\Supermode Map\\QA\\QAThresholds\\3";
        const string RELATIVE_REGKEY_DSDBR01_SMPF_SM4 = "\\Supermode Map\\QA\\QAThresholds\\4";
        const string RELATIVE_REGKEY_DSDBR01_SMPF_SM5 = "\\Supermode Map\\QA\\QAThresholds\\5";
        const string RELATIVE_REGKEY_DSDBR01_SMPF_SM6 = "\\Supermode Map\\QA\\QAThresholds\\6";
        const string RELATIVE_REGKEY_DSDBR01_SMPF_SM7 = "\\Supermode Map\\QA\\QAThresholds\\7";
        const string RELATIVE_REGKEY_DSDBR01_SMPF_SM8 = "\\Supermode Map\\QA\\QAThresholds\\8";
        const string RELATIVE_REGKEY_DSDBR01_SMPF_SM9 = "\\Supermode Map\\QA\\QAThresholds\\9";

        //
        ///////////////////////////////////////////////////////////////////////////////////////
        ///
        // defaults
        const int DEFAULT_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD = 40;
        const int DEFAULT_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD = 3;
        const int DEFAULT_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD = 5;
        const int DEFAULT_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD = 0;
        const int DEFAULT_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD = 90;
        const double DEFAULT_DSDBR01_SMPF_MAX_CONTINUITY_SPACING_THRESHOLD = 0.5;
        const int DEFAULT_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD = 50;
        const int DEFAULT_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD = 25;
        const int DEFAULT_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD = 17;
        const int DEFAULT_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD = 80;
        const int DEFAULT_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD = 30;
        const int DEFAULT_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD = 20;
        const int DEFAULT_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD = 60;
        const int DEFAULT_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD = 8;
        const int DEFAULT_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD = 90;
        const int DEFAULT_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD = 0;
        //
        ///////////////////////////////////////////////////////////////////////////////////////
        ///
        //
        const string REGNAME_DSDBR01_SMPF_MAX_MODE_WIDTH_THRESHOLD = "Max Mode Width Threshold";
        const string REGNAME_DSDBR01_SMPF_MIN_MODE_WIDTH_THRESHOLD = "Min Mode Width Threshold";
        const string REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_RMS_THRESHOLD = "Max Middle Line RMS Value";
        const string REGNAME_DSDBR01_SMPF_MIN_MIDDLE_LINE_SLOPE_THRESHOLD = "Min Middle Line Slope";
        const string REGNAME_DSDBR01_SMPF_MAX_MIDDLE_LINE_SLOPE_THRESHOLD = "Max Middle Line Slope";
        //const string REGNAME_DSDBR01_SMPF_MAX_CONTINUITY_SPACING_THRESHOLD		"Max Pr Continuity Gap [0:1]"
        const string REGNAME_DSDBR01_SMPF_MEAN_PERC_WORKING_REGION_THRESHOLD = "Mean Percentage Working Region";
        const string REGNAME_DSDBR01_SMPF_MIN_PERC_WORKING_REGION_THRESHOLD = "Min Percentage Working Region";
        const string REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_THRESHOLD = "Max Modal Distortion Angle";
        const string REGNAME_DSDBR01_SMPF_MODAL_DISTORTION_ANGLE_X_POS_THRESHOLD = "Max Modal Distortion Angle X Position";
        const string REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_THRESHOLD = "Max Mode BDC Area";
        const string REGNAME_DSDBR01_SMPF_MAX_MODE_BDC_AREA_X_LENGTH_THRESHOLD = "Max Mode BDC Area X Length";
        const string REGNAME_DSDBR01_SMPF_SUM_MODE_BDC_AREAS_THRESHOLD = "Sum Mode BDC Areas";
        const string REGNAME_DSDBR01_SMPF_NUM_MODE_BDC_AREAS_THRESHOLD = "Num Mode BDC Areas";
        const string REGNAME_DSDBR01_SMPF_MAX_MODE_LINE_SLOPE_THRESHOLD = "Max Mode Line Slope";
        const string REGNAME_DSDBR01_SMPF_MAX_MODE_BORDERS_REMOVED_THRESHOLD = "Max Mode Borders Removed";
        //
        ///
        ///////////////////////////////////////////////////////////////////////////////////////


        ///////////////////////////////////////////////////////////////////////////////////////
        // DSDBR01 Coarse Frequency parameters

        const string RELATIVE_REGKEY_DSDBR01_CFREQ = "\\Coarse Frequency";

        const string REGNAME_DSDBR01_CFREQ_POLY_COEFFS_ABSPATH = "Polynomial Coefficients Filepath";
        const string DEFAULT_DSDBR01_CFREQ_POLY_COEFFS_ABSPATH = "C:\\Program Files\\PXIT\\CLose-Grid 4.0\\defaults\\CoarseFreqPolyCoeffs_DSDBR01_default.csv";

        const string REGNAME_DSDBR01_CFREQ_NUM_FREQ_MEAS_PER_OP = "Number of frequency measurements to average per operating point";
        const int DEFAULT_DSDBR01_CFREQ_NUM_FREQ_MEAS_PER_OP = 3;

        const string REGNAME_DSDBR01_CFREQ_POWER_RATIO_MEAS_PER_OP = "Number of power ratio measurements to average per operating point";
        const int DEFAULT_DSDBR01_CFREQ_POWER_RATIO_MEAS_PER_OP = 3;

        const string REGNAME_DSDBR01_CFREQ_MAX_STABLE_FREQ_ERROR = "Max frequency error [GHz] at stable operating point";
        const double DEFAULT_DSDBR01_CFREQ_MAX_STABLE_FREQ_ERROR = 0.5;

        const string REGNAME_DSDBR01_CFREQ_SAMPLE_POINT_SETTLE_TIME = "Sample point settle time [ms]";
        const int DEFAULT_DSDBR01_CFREQ_SAMPLE_POINT_SETTLE_TIME = 150;

        const string REGNAME_DSDBR01_CFREQ_WRITE_SAMPLE_POINTS_TO_FILE = "Write sample points to file";
        const bool DEFAULT_DSDBR01_CFREQ_WRITE_SAMPLE_POINTS_TO_FILE = true;


        // DSDBR01 ITU Grid

        const string RELATIVE_REGKEY_DSDBR01_ITUGRID = "\\ITU Grid";

        const string REGNAME_DSDBR01_ITUGRID_CENTRE_FREQ = "Centre Frequency [GHz]";
        const int DEFAULT_DSDBR01_ITUGRID_CENTRE_FREQ = 193100;

        const string REGNAME_DSDBR01_ITUGRID_STEP_FREQ = "Frequency Spacing [GHz]";
        const int DEFAULT_DSDBR01_ITUGRID_STEP_FREQ = 25;

        const string REGNAME_DSDBR01_ITUGRID_CHANNEL_COUNT = "Number of channels";
        const int DEFAULT_DSDBR01_ITUGRID_CHANNEL_COUNT = 161;

        const string REGNAME_DSDBR01_ITUGRID_abspath = "ITU Grid Filepath";
        const string DEFAULT_DSDBR01_ITUGRID_abspath = "no_file";

        const string REGNAME_DSDBR01_ESTIMATESITUGRID_abspath = "Estimates ITU Grid Filepath";
        const string DEFAULT_DSDBR01_ESTIMATESITUGRID_abspath = "no_file";

        // DSDBR01 ITU Grid characterisation parameters

        const string RELATIVE_REGKEY_DSDBR01_CHAR = "\\Characterisation";

        const string REGNAME_DSDBR01_CHAR_NUM_FREQ_MEAS_PER_OP = "Number of frequency measurements to average per operating point";
        const int DEFAULT_DSDBR01_CHAR_NUM_FREQ_MEAS_PER_OP = 1;

        const string REGNAME_DSDBR01_CHAR_FREQ_ACCURACY_OF_WAVEMETER = "Frequency accuracy of wavemeter [+/-GHz]";
        const double DEFAULT_DSDBR01_CHAR_FREQ_ACCURACY_OF_WAVEMETER = 0.5;

        const string REGNAME_DSDBR01_CHAR_OP_SETTLE_TIME = "Operating point settle time [ms]";
        const int DEFAULT_DSDBR01_CHAR_OP_SETTLE_TIME = 150;

        const string REGNAME_DSDBR01_CHAR_itusearch_scheme = "ITU search scheme";
        const int DEFAULT_DSDBR01_CHAR_itusearch_scheme = 0;

        const string REGNAME_DSDBR01_CHAR_itusearch_nextguess = "ITU search next guess type";
        const int DEFAULT_DSDBR01_CHAR_itusearch_nextguess = 0;

        const string REGNAME_DSDBR01_CHAR_itusearch_resamples = "ITU search number of samples";
        const int DEFAULT_DSDBR01_CHAR_itusearch_resamples = 10;

        const string REGNAME_DSDBR01_CHAR_POWER_RATIO_MEAS_PER_OP = "Number of power ratio measurements to average per operating point";
        const int DEFAULT_DSDBR01_CHAR_POWER_RATIO_MEAS_PER_OP = 1;

        const string REGNAME_DSDBR01_CHAR_MAX_STABLE_FREQ_ERROR = "Max frequency error [GHz] at stable operating point";
        const double DEFAULT_DSDBR01_CHAR_MAX_STABLE_FREQ_ERROR = 0.5;

        const string REGNAME_DSDBR01_CHAR_WRITE_DUFF_POINTS_TO_FILE = "If ITU search failed, write last guess to file";
        const bool DEFAULT_DSDBR01_CHAR_WRITE_DUFF_POINTS_TO_FILE = false;

        const string REGNAME_DSDBR01_CHAR_prevent_front_section_switch = "Prevent front section switch after problem finding ITU point";
        const bool DEFAULT_DSDBR01_CHAR_prevent_front_section_switch = false;

        const string REGNAME_DSDBR01_CHAR_fakefreq_Iramp_module_name = "Fake Frequency Iramp Module Name";
        const string DEFAULT_DSDBR01_CHAR_fakefreq_Iramp_module_name = "Phase";

        const string REGNAME_DSDBR01_CHAR_Iramp_vs_fakefreq_abspath = "Iramp Vs Fake Frequency Filepath";
        const string DEFAULT_DSDBR01_CHAR_Iramp_vs_fakefreq_abspath = "no_file";

        const string REGNAME_DSDBR01_CHAR_fakefreq_max_error = "Fake Frequency Max Error from generated noise";
        const int DEFAULT_DSDBR01_CHAR_fakefreq_max_error = 0;



        const string RELATIVE_REGKEY_DSDBR01_CHARTEC = "\\Characterisation\\TEC Settling";

        const string REGNAME_DSDBR01_CHARTEC_time_window_size = "Moving time window size";
        const int DEFAULT_DSDBR01_CHARTEC_time_window_size = 0;

        const string REGNAME_DSDBR01_CHARTEC_max_temp_deviation = "Max temperature deviation";
        const int DEFAULT_DSDBR01_CHARTEC_max_temp_deviation = 0;

        const string REGNAME_DSDBR01_CHARTEC_num_temp_readings = "Number of temperature readings";
        const int DEFAULT_DSDBR01_CHARTEC_num_temp_readings = 0;

        const string REGNAME_DSDBR01_CHARTEC_max_settle_time = "Maximum settle time";
        const int DEFAULT_DSDBR01_CHARTEC_max_settle_time = 0;


        //
        /////////////////////////////////////////////////////////////////
        #endregion

        public int get_DSDBR01_SMPF_max_mode_width(short smNumber)
        {
            int maxModeWidth = 0;

            if (smNumber == 0)
                maxModeWidth = _DSDBR01_SMPF_SM0_max_mode_width;
            else if (smNumber == 1)
                maxModeWidth = _DSDBR01_SMPF_SM1_max_mode_width;
            else if (smNumber == 2)
                maxModeWidth = _DSDBR01_SMPF_SM2_max_mode_width;
            else if (smNumber == 3)
                maxModeWidth = _DSDBR01_SMPF_SM3_max_mode_width;
            else if (smNumber == 4)
                maxModeWidth = _DSDBR01_SMPF_SM4_max_mode_width;
            else if (smNumber == 5)
                maxModeWidth = _DSDBR01_SMPF_SM5_max_mode_width;
            else if (smNumber == 6)
                maxModeWidth = _DSDBR01_SMPF_SM6_max_mode_width;
            else if (smNumber == 7)
                maxModeWidth = _DSDBR01_SMPF_SM7_max_mode_width;
            else if (smNumber == 8)
                maxModeWidth = _DSDBR01_SMPF_SM8_max_mode_width;
            else if (smNumber == 9)
                maxModeWidth = _DSDBR01_SMPF_SM9_max_mode_width;

            return maxModeWidth;
        }

        public int get_DSDBR01_SMPF_min_mode_width(short smNumber)
        {
            int minModeWidth = 0;

            if (smNumber == 0)
                minModeWidth = _DSDBR01_SMPF_SM0_min_mode_width;
            else if (smNumber == 1)
                minModeWidth = _DSDBR01_SMPF_SM1_min_mode_width;
            else if (smNumber == 2)
                minModeWidth = _DSDBR01_SMPF_SM2_min_mode_width;
            else if (smNumber == 3)
                minModeWidth = _DSDBR01_SMPF_SM3_min_mode_width;
            else if (smNumber == 4)
                minModeWidth = _DSDBR01_SMPF_SM4_min_mode_width;
            else if (smNumber == 5)
                minModeWidth = _DSDBR01_SMPF_SM5_min_mode_width;
            else if (smNumber == 6)
                minModeWidth = _DSDBR01_SMPF_SM6_min_mode_width;
            else if (smNumber == 7)
                minModeWidth = _DSDBR01_SMPF_SM7_min_mode_width;
            else if (smNumber == 8)
                minModeWidth = _DSDBR01_SMPF_SM8_min_mode_width;
            else if (smNumber == 9)
                minModeWidth = _DSDBR01_SMPF_SM9_min_mode_width;

            return minModeWidth;
        }

        public int get_DSDBR01_SMPF_max_mode_borders_removed(short smNumber)
        {
            int modeBordersRemoved = 0;

            if (smNumber == 0)
                modeBordersRemoved = _DSDBR01_SMPF_SM0_max_mode_borders_removed;
            else if (smNumber == 1)
                modeBordersRemoved = _DSDBR01_SMPF_SM1_max_mode_borders_removed;
            else if (smNumber == 2)
                modeBordersRemoved = _DSDBR01_SMPF_SM2_max_mode_borders_removed;
            else if (smNumber == 3)
                modeBordersRemoved = _DSDBR01_SMPF_SM3_max_mode_borders_removed;
            else if (smNumber == 4)
                modeBordersRemoved = _DSDBR01_SMPF_SM4_max_mode_borders_removed;
            else if (smNumber == 5)
                modeBordersRemoved = _DSDBR01_SMPF_SM5_max_mode_borders_removed;
            else if (smNumber == 6)
                modeBordersRemoved = _DSDBR01_SMPF_SM6_max_mode_borders_removed;
            else if (smNumber == 7)
                modeBordersRemoved = _DSDBR01_SMPF_SM7_max_mode_borders_removed;
            else if (smNumber == 8)
                modeBordersRemoved = _DSDBR01_SMPF_SM8_max_mode_borders_removed;
            else if (smNumber == 9)
                modeBordersRemoved = _DSDBR01_SMPF_SM9_max_mode_borders_removed;

            return modeBordersRemoved;
        }

        public double get_DSDBR01_SMPF_max_mode_bdc_area(short smNumber)
        {
            double maxBdcArea = 0;

            if (smNumber == 0)
                maxBdcArea = _DSDBR01_SMPF_SM0_max_mode_bdc_area;
            else if (smNumber == 1)
                maxBdcArea = _DSDBR01_SMPF_SM1_max_mode_bdc_area;
            else if (smNumber == 2)
                maxBdcArea = _DSDBR01_SMPF_SM2_max_mode_bdc_area;
            else if (smNumber == 3)
                maxBdcArea = _DSDBR01_SMPF_SM3_max_mode_bdc_area;
            else if (smNumber == 4)
                maxBdcArea = _DSDBR01_SMPF_SM4_max_mode_bdc_area;
            else if (smNumber == 5)
                maxBdcArea = _DSDBR01_SMPF_SM5_max_mode_bdc_area;
            else if (smNumber == 6)
                maxBdcArea = _DSDBR01_SMPF_SM6_max_mode_bdc_area;
            else if (smNumber == 7)
                maxBdcArea = _DSDBR01_SMPF_SM7_max_mode_bdc_area;
            else if (smNumber == 8)
                maxBdcArea = _DSDBR01_SMPF_SM8_max_mode_bdc_area;
            else if (smNumber == 9)
                maxBdcArea = _DSDBR01_SMPF_SM9_max_mode_bdc_area;

            return maxBdcArea;
        }

        public double get_DSDBR01_SMPF_max_mode_bdc_area_x_length(short smNumber)
        {
            double maxBdcAreaXLength = 0;

            if (smNumber == 0)
                maxBdcAreaXLength = _DSDBR01_SMPF_SM0_max_mode_bdc_area_x_length;
            else if (smNumber == 1)
                maxBdcAreaXLength = _DSDBR01_SMPF_SM1_max_mode_bdc_area_x_length;
            else if (smNumber == 2)
                maxBdcAreaXLength = _DSDBR01_SMPF_SM2_max_mode_bdc_area_x_length;
            else if (smNumber == 3)
                maxBdcAreaXLength = _DSDBR01_SMPF_SM3_max_mode_bdc_area_x_length;
            else if (smNumber == 4)
                maxBdcAreaXLength = _DSDBR01_SMPF_SM4_max_mode_bdc_area_x_length;
            else if (smNumber == 5)
                maxBdcAreaXLength = _DSDBR01_SMPF_SM5_max_mode_bdc_area_x_length;
            else if (smNumber == 6)
                maxBdcAreaXLength = _DSDBR01_SMPF_SM6_max_mode_bdc_area_x_length;
            else if (smNumber == 7)
                maxBdcAreaXLength = _DSDBR01_SMPF_SM7_max_mode_bdc_area_x_length;
            else if (smNumber == 8)
                maxBdcAreaXLength = _DSDBR01_SMPF_SM8_max_mode_bdc_area_x_length;
            else if (smNumber == 9)
                maxBdcAreaXLength = _DSDBR01_SMPF_SM9_max_mode_bdc_area_x_length;

            return maxBdcAreaXLength;
        }

        public double get_DSDBR01_SMPF_mean_perc_working_region(short smNumber)
        {
            double meanPercWorkingRegion = 0;

            if (smNumber == 0)
                meanPercWorkingRegion = _DSDBR01_SMPF_SM0_mean_perc_working_region;
            else if (smNumber == 1)
                meanPercWorkingRegion = _DSDBR01_SMPF_SM1_mean_perc_working_region;
            else if (smNumber == 2)
                meanPercWorkingRegion = _DSDBR01_SMPF_SM2_mean_perc_working_region;
            else if (smNumber == 3)
                meanPercWorkingRegion = _DSDBR01_SMPF_SM3_mean_perc_working_region;
            else if (smNumber == 4)
                meanPercWorkingRegion = _DSDBR01_SMPF_SM4_mean_perc_working_region;
            else if (smNumber == 5)
                meanPercWorkingRegion = _DSDBR01_SMPF_SM5_mean_perc_working_region;
            else if (smNumber == 6)
                meanPercWorkingRegion = _DSDBR01_SMPF_SM6_mean_perc_working_region;
            else if (smNumber == 7)
                meanPercWorkingRegion = _DSDBR01_SMPF_SM7_mean_perc_working_region;
            else if (smNumber == 8)
                meanPercWorkingRegion = _DSDBR01_SMPF_SM8_mean_perc_working_region;
            else if (smNumber == 9)
                meanPercWorkingRegion = _DSDBR01_SMPF_SM9_mean_perc_working_region;

            return meanPercWorkingRegion;
        }

        public double get_DSDBR01_SMPF_min_perc_working_region(short smNumber)
        {
            double minPercWorkingRegion = 0;

            if (smNumber == 0)
                minPercWorkingRegion = _DSDBR01_SMPF_SM0_min_perc_working_region;
            else if (smNumber == 1)
                minPercWorkingRegion = _DSDBR01_SMPF_SM1_min_perc_working_region;
            else if (smNumber == 2)
                minPercWorkingRegion = _DSDBR01_SMPF_SM2_min_perc_working_region;
            else if (smNumber == 3)
                minPercWorkingRegion = _DSDBR01_SMPF_SM3_min_perc_working_region;
            else if (smNumber == 4)
                minPercWorkingRegion = _DSDBR01_SMPF_SM4_min_perc_working_region;
            else if (smNumber == 5)
                minPercWorkingRegion = _DSDBR01_SMPF_SM5_min_perc_working_region;
            else if (smNumber == 6)
                minPercWorkingRegion = _DSDBR01_SMPF_SM6_min_perc_working_region;
            else if (smNumber == 7)
                minPercWorkingRegion = _DSDBR01_SMPF_SM7_min_perc_working_region;
            else if (smNumber == 8)
                minPercWorkingRegion = _DSDBR01_SMPF_SM8_min_perc_working_region;
            else if (smNumber == 9)
                minPercWorkingRegion = _DSDBR01_SMPF_SM9_min_perc_working_region;

            return minPercWorkingRegion;
        }

        public double get_DSDBR01_SMPF_modal_distortion_angle(short smNumber)
        {
            double modalDistortionAngle = 0;

            if (smNumber == 0)
                modalDistortionAngle = _DSDBR01_SMPF_SM0_modal_distortion_angle;
            else if (smNumber == 1)
                modalDistortionAngle = _DSDBR01_SMPF_SM1_modal_distortion_angle;
            else if (smNumber == 2)
                modalDistortionAngle = _DSDBR01_SMPF_SM2_modal_distortion_angle;
            else if (smNumber == 3)
                modalDistortionAngle = _DSDBR01_SMPF_SM3_modal_distortion_angle;
            else if (smNumber == 4)
                modalDistortionAngle = _DSDBR01_SMPF_SM4_modal_distortion_angle;
            else if (smNumber == 5)
                modalDistortionAngle = _DSDBR01_SMPF_SM5_modal_distortion_angle;
            else if (smNumber == 6)
                modalDistortionAngle = _DSDBR01_SMPF_SM6_modal_distortion_angle;
            else if (smNumber == 7)
                modalDistortionAngle = _DSDBR01_SMPF_SM7_modal_distortion_angle;
            else if (smNumber == 8)
                modalDistortionAngle = _DSDBR01_SMPF_SM8_modal_distortion_angle;
            else if (smNumber == 9)
                modalDistortionAngle = _DSDBR01_SMPF_SM9_modal_distortion_angle;

            return modalDistortionAngle;
        }

        public double get_DSDBR01_SMPF_modal_distortion_angle_x_pos(short smNumber)
        {
            double modalDistortionAngleXPos = 0;

            if (smNumber == 0)
                modalDistortionAngleXPos = _DSDBR01_SMPF_SM0_modal_distortion_angle_x_pos;
            else if (smNumber == 1)
                modalDistortionAngleXPos = _DSDBR01_SMPF_SM1_modal_distortion_angle_x_pos;
            else if (smNumber == 2)
                modalDistortionAngleXPos = _DSDBR01_SMPF_SM2_modal_distortion_angle_x_pos;
            else if (smNumber == 3)
                modalDistortionAngleXPos = _DSDBR01_SMPF_SM3_modal_distortion_angle_x_pos;
            else if (smNumber == 4)
                modalDistortionAngleXPos = _DSDBR01_SMPF_SM4_modal_distortion_angle_x_pos;
            else if (smNumber == 5)
                modalDistortionAngleXPos = _DSDBR01_SMPF_SM5_modal_distortion_angle_x_pos;
            else if (smNumber == 6)
                modalDistortionAngleXPos = _DSDBR01_SMPF_SM6_modal_distortion_angle_x_pos;
            else if (smNumber == 7)
                modalDistortionAngleXPos = _DSDBR01_SMPF_SM7_modal_distortion_angle_x_pos;
            else if (smNumber == 8)
                modalDistortionAngleXPos = _DSDBR01_SMPF_SM8_modal_distortion_angle_x_pos;
            else if (smNumber == 9)
                modalDistortionAngleXPos = _DSDBR01_SMPF_SM9_modal_distortion_angle_x_pos;

            return modalDistortionAngleXPos;
        }

        public double get_DSDBR01_SMPF_num_mode_bdc_areas(short smNumber)
        {
            double numBdcArea = 0;

            if (smNumber == 0)
                numBdcArea = _DSDBR01_SMPF_SM0_num_mode_bdc_areas;
            else if (smNumber == 1)
                numBdcArea = _DSDBR01_SMPF_SM1_num_mode_bdc_areas;
            else if (smNumber == 2)
                numBdcArea = _DSDBR01_SMPF_SM2_num_mode_bdc_areas;
            else if (smNumber == 3)
                numBdcArea = _DSDBR01_SMPF_SM3_num_mode_bdc_areas;
            else if (smNumber == 4)
                numBdcArea = _DSDBR01_SMPF_SM4_num_mode_bdc_areas;
            else if (smNumber == 5)
                numBdcArea = _DSDBR01_SMPF_SM5_num_mode_bdc_areas;
            else if (smNumber == 6)
                numBdcArea = _DSDBR01_SMPF_SM6_num_mode_bdc_areas;
            else if (smNumber == 7)
                numBdcArea = _DSDBR01_SMPF_SM7_num_mode_bdc_areas;
            else if (smNumber == 8)
                numBdcArea = _DSDBR01_SMPF_SM8_num_mode_bdc_areas;
            else if (smNumber == 9)
                numBdcArea = _DSDBR01_SMPF_SM9_num_mode_bdc_areas;

            return numBdcArea;
        }

        public double get_DSDBR01_SMPF_sum_mode_bdc_areas(short smNumber)
        {
            double sumBdcArea = 0;

            if (smNumber == 0)
                sumBdcArea = _DSDBR01_SMPF_SM0_sum_mode_bdc_areas;
            else if (smNumber == 1)
                sumBdcArea = _DSDBR01_SMPF_SM1_sum_mode_bdc_areas;
            else if (smNumber == 2)
                sumBdcArea = _DSDBR01_SMPF_SM2_sum_mode_bdc_areas;
            else if (smNumber == 3)
                sumBdcArea = _DSDBR01_SMPF_SM3_sum_mode_bdc_areas;
            else if (smNumber == 4)
                sumBdcArea = _DSDBR01_SMPF_SM4_sum_mode_bdc_areas;
            else if (smNumber == 5)
                sumBdcArea = _DSDBR01_SMPF_SM5_sum_mode_bdc_areas;
            else if (smNumber == 6)
                sumBdcArea = _DSDBR01_SMPF_SM6_sum_mode_bdc_areas;
            else if (smNumber == 7)
                sumBdcArea = _DSDBR01_SMPF_SM7_sum_mode_bdc_areas;
            else if (smNumber == 8)
                sumBdcArea = _DSDBR01_SMPF_SM8_sum_mode_bdc_areas;
            else if (smNumber == 9)
                sumBdcArea = _DSDBR01_SMPF_SM9_sum_mode_bdc_areas;

            return sumBdcArea;
        }

        public double get_DSDBR01_SMPF_max_middle_line_slope(short smNumber)
        {
            double maxMiddleLineSlope = 0;

            if (smNumber == 0)
                maxMiddleLineSlope = _DSDBR01_SMPF_SM0_max_middle_line_slope;
            else if (smNumber == 1)
                maxMiddleLineSlope = _DSDBR01_SMPF_SM1_max_middle_line_slope;
            else if (smNumber == 2)
                maxMiddleLineSlope = _DSDBR01_SMPF_SM2_max_middle_line_slope;
            else if (smNumber == 3)
                maxMiddleLineSlope = _DSDBR01_SMPF_SM3_max_middle_line_slope;
            else if (smNumber == 4)
                maxMiddleLineSlope = _DSDBR01_SMPF_SM4_max_middle_line_slope;
            else if (smNumber == 5)
                maxMiddleLineSlope = _DSDBR01_SMPF_SM5_max_middle_line_slope;
            else if (smNumber == 6)
                maxMiddleLineSlope = _DSDBR01_SMPF_SM6_max_middle_line_slope;
            else if (smNumber == 7)
                maxMiddleLineSlope = _DSDBR01_SMPF_SM7_max_middle_line_slope;
            else if (smNumber == 8)
                maxMiddleLineSlope = _DSDBR01_SMPF_SM8_max_middle_line_slope;
            else if (smNumber == 9)
                maxMiddleLineSlope = _DSDBR01_SMPF_SM9_max_middle_line_slope;

            return maxMiddleLineSlope;
        }

        public double get_DSDBR01_SMPF_min_middle_line_slope(short smNumber)
        {
            double minMiddleLineSlope = 0;

            if (smNumber == 0)
                minMiddleLineSlope = _DSDBR01_SMPF_SM0_min_middle_line_slope;
            else if (smNumber == 1)
                minMiddleLineSlope = _DSDBR01_SMPF_SM1_min_middle_line_slope;
            else if (smNumber == 2)
                minMiddleLineSlope = _DSDBR01_SMPF_SM2_min_middle_line_slope;
            else if (smNumber == 3)
                minMiddleLineSlope = _DSDBR01_SMPF_SM3_min_middle_line_slope;
            else if (smNumber == 4)
                minMiddleLineSlope = _DSDBR01_SMPF_SM4_min_middle_line_slope;
            else if (smNumber == 5)
                minMiddleLineSlope = _DSDBR01_SMPF_SM5_min_middle_line_slope;
            else if (smNumber == 6)
                minMiddleLineSlope = _DSDBR01_SMPF_SM6_min_middle_line_slope;
            else if (smNumber == 7)
                minMiddleLineSlope = _DSDBR01_SMPF_SM7_min_middle_line_slope;
            else if (smNumber == 8)
                minMiddleLineSlope = _DSDBR01_SMPF_SM8_min_middle_line_slope;
            else if (smNumber == 9)
                minMiddleLineSlope = _DSDBR01_SMPF_SM9_min_middle_line_slope;

            return minMiddleLineSlope;
        }

        public double get_DSDBR01_SMPF_max_middle_line_rms_value(short smNumber)
        {
            double middleLineRMSValue = 0;

            if (smNumber == 0)
                middleLineRMSValue = _DSDBR01_SMPF_SM0_max_middle_line_rms_value;
            else if (smNumber == 1)
                middleLineRMSValue = _DSDBR01_SMPF_SM1_max_middle_line_rms_value;
            else if (smNumber == 2)
                middleLineRMSValue = _DSDBR01_SMPF_SM2_max_middle_line_rms_value;
            else if (smNumber == 3)
                middleLineRMSValue = _DSDBR01_SMPF_SM3_max_middle_line_rms_value;
            else if (smNumber == 4)
                middleLineRMSValue = _DSDBR01_SMPF_SM4_max_middle_line_rms_value;
            else if (smNumber == 5)
                middleLineRMSValue = _DSDBR01_SMPF_SM5_max_middle_line_rms_value;
            else if (smNumber == 6)
                middleLineRMSValue = _DSDBR01_SMPF_SM6_max_middle_line_rms_value;
            else if (smNumber == 7)
                middleLineRMSValue = _DSDBR01_SMPF_SM7_max_middle_line_rms_value;
            else if (smNumber == 8)
                middleLineRMSValue = _DSDBR01_SMPF_SM8_max_middle_line_rms_value;
            else if (smNumber == 9)
                middleLineRMSValue = _DSDBR01_SMPF_SM9_max_middle_line_rms_value;

            return middleLineRMSValue;
        }

        public double get_DSDBR01_SMPF_max_mode_line_slope(short smNumber)
        {
            double maxModeLineSlope = 0;

            if (smNumber == 0)
                maxModeLineSlope = _DSDBR01_SMPF_SM0_max_mode_line_slope;
            else if (smNumber == 1)
                maxModeLineSlope = _DSDBR01_SMPF_SM1_max_mode_line_slope;
            else if (smNumber == 2)
                maxModeLineSlope = _DSDBR01_SMPF_SM2_max_mode_line_slope;
            else if (smNumber == 3)
                maxModeLineSlope = _DSDBR01_SMPF_SM3_max_mode_line_slope;
            else if (smNumber == 4)
                maxModeLineSlope = _DSDBR01_SMPF_SM4_max_mode_line_slope;
            else if (smNumber == 5)
                maxModeLineSlope = _DSDBR01_SMPF_SM5_max_mode_line_slope;
            else if (smNumber == 6)
                maxModeLineSlope = _DSDBR01_SMPF_SM6_max_mode_line_slope;
            else if (smNumber == 7)
                maxModeLineSlope = _DSDBR01_SMPF_SM7_max_mode_line_slope;
            else if (smNumber == 8)
                maxModeLineSlope = _DSDBR01_SMPF_SM8_max_mode_line_slope;
            else if (smNumber == 9)
                maxModeLineSlope = _DSDBR01_SMPF_SM9_max_mode_line_slope;

            return maxModeLineSlope;
        }






    }
}

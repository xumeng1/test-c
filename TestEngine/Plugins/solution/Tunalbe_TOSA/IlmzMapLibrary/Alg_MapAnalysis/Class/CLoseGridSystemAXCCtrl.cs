using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Bookham.TestLibrary.Algorithms
{
    public enum state_type
    {
        instantiated = 0,
        start_up,
        set_up,
        busy,
        complete
    }
    struct two_ptrs
    {
        public CCLoseGridSystemAXCCtrl p_axc;
        public object p_other;
    }

    public class CCLoseGridSystemAXCCtrl
    {
        private state_type _system_state;
        private short _percent_complete;
        private Log CGR_LOG;

        private short _overall_map_loaded;
        short _overall_map_screening_passed;
        List<short> _supermode_map_screening_passed;
        short _supermodes_found_count;
        short _supermode_map_loaded_count;
        short _estimate_count;
        short _ITUOP_count;
        short _duff_ITUOP_count;
        String _date_time_stamp_CString;
        String _laser_id_CString;

        public CCGOperatingPoints _ops;
        public CDSDBROverallModeMap _overall_mode_map;

        public CCLoseGridSystemAXCCtrl(string parametersFile)
        {
            DSDBR01.Initialize(parametersFile);
            Log.Init(DSDBR01.Instance._DSDBR01_LOGFILEPATHNAME, DSDBR01.Instance._DSDBR01_LOGFILEMAXSIZE / 1000);
            CGR_LOG = Log.GetInstance;
            _ops = new CCGOperatingPoints();
        }

        public void startup()
        {
            if (SystemState != state_type.instantiated)
            {
                // function cannot be called in the current system state
                string log_err_msg = "CCLoseGridSystemAXCCtrl.startup() error: ";
                log_err_msg += CGSystemErrors.CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR;
                CGR_LOG.Write(log_err_msg, LoggingLevels.ERROR_CGR_LOG);

                throw new Exception(CGSystemErrors.CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR);
            }
            CCGOperatingPoints.rtype rval_read_cfreq_poly = _ops.loadDefaultCoarseFreqPoly();
            if (rval_read_cfreq_poly != CCGOperatingPoints.rtype.ok)
            {
                // could not load polynomial, use default, not a reason to fail startup
                string log_err_msg = "CCLoseGridSystemAXCCtrl.startup(): ";
                log_err_msg += ("could not load coarse frequency polynomial, using default");
                CGR_LOG.Write(log_err_msg, LoggingLevels.WARNING_CGR_LOG);
            }

            if (!DSDBR01.Instance._DSDBR01_BASE_stub_hardware_calls)
            {
                // Check system hardware is okay
            }
            else
            {
                // just open log file, no HW initialisation
            }

            // Write version info to logfile

            // change state
            SystemState = state_type.start_up;
            CGR_LOG.Write("CCLoseGridSystemAXCCtrl.startup() exiting", (int)LoggingLevels.DIAGNOSTIC_CGR_LOG);
            return;
        }

        public void setup(string laser_type, string laser_id, out string p_date_time_stamp)
        {
            if (SystemState != state_type.start_up
                    && SystemState != state_type.set_up
                    && SystemState != state_type.complete)
            {
                // function cannot be called in the current system state
                string log_err_msg = "CCLoseGridSystemAXCCtrl.setup() error: ";
                log_err_msg += CGSystemErrors.CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR;
                CGR_LOG.Write(log_err_msg, LoggingLevels.ERROR_CGR_LOG);

                throw new Exception(CGSystemErrors.CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR);
            }

            // Check for NULL string
            if (laser_type == null || laser_id == null)//|| p_date_time_stamp == null
            {
                // if there is any problem encountered
                // change state down to startup
                String log_err_msg = "CCLoseGridSystemAXCCtrl.setup() error: ";
                log_err_msg += CGSystemErrors.CGSYSTEM_NULL_PTR_ERROR;
                CGR_LOG.Write(log_err_msg, LoggingLevels.ERROR_CGR_LOG);
                SystemState = state_type.start_up;

                throw new Exception(CGSystemErrors.CGSYSTEM_NULL_PTR_ERROR);
            }

            CGR_LOG.Write("CCLoseGridSystemAXCCtrl.setup() entered", LoggingLevels.DIAGNOSTIC_CGR_LOG);
            string log_msg = "CCLoseGridSystemAXCCtrl.setup() laser_type = ";
            log_msg += laser_type;
            CGR_LOG.Write(log_msg, LoggingLevels.DIAGNOSTIC_CGR_LOG);
            log_msg = "CCLoseGridSystemAXCCtrl.setup() laser_id = ";
            log_msg += laser_id;
            CGR_LOG.Write(log_msg, LoggingLevels.DIAGNOSTIC_CGR_LOG);

            // test strings are not empty
            if (string.IsNullOrEmpty(laser_type) || string.IsNullOrEmpty(laser_id))
            {
                // if there is any problem encountered
                // change state down to startup
                String log_err_msg = "CCLoseGridSystemAXCCtrl.setup() error: ";
                log_err_msg += CGSystemErrors.CGSYSTEM_EMPTY_STRING_ARG_ERROR;
                CGR_LOG.Write(log_err_msg, LoggingLevels.ERROR_CGR_LOG);
                SystemState = state_type.start_up;

                throw new Exception(CGSystemErrors.CGSYSTEM_EMPTY_STRING_ARG_ERROR);
            }

            // test laser_type is valid
            if (!validLaserType(laser_type))
            {
                // if there is any problem encountered
                // change state down to startup
                String log_err_msg = "CCLoseGridSystemAXCCtrl.setup() error: ";
                log_err_msg += CGSystemErrors.CGSYSTEM_LASERTYPE_ERROR;
                CGR_LOG.Write(log_err_msg, LoggingLevels.ERROR_CGR_LOG);
                SystemState = state_type.start_up;

                throw new Exception(CGSystemErrors.CGSYSTEM_LASERTYPE_ERROR);
            }
            // test laser_id is valid
            if (!validLaserID(laser_id))
            {
                // if there is any problem encountered
                // change state down to startup
                String log_err_msg = "CCLoseGridSystemAXCCtrl.setup() error: ";
                log_err_msg += CGSystemErrors.CGSYSTEM_LASER_ID_ERROR;
                CGR_LOG.Write(log_err_msg, LoggingLevels.ERROR_CGR_LOG);
                SystemState = state_type.start_up;

                throw new Exception(CGSystemErrors.CGSYSTEM_LASER_ID_ERROR);
            }

            // clear all system data
            clear();

            String system_date_time_stamp_CString = getSystemDateTimeStamp();
            String new_date_time_stamp_CString;
            getNewDateTimeStamp(out new_date_time_stamp_CString);
            while (new_date_time_stamp_CString == system_date_time_stamp_CString)
            {
                // to ensure all date/time stamps are unique.
                Thread.Sleep(100);
                getNewDateTimeStamp(out new_date_time_stamp_CString);
            }

            if (!DSDBR01.Instance._DSDBR01_BASE_stub_hardware_calls)
            {
                // Check laser is connected to hardware okay
                //hres = SERVERINTERFACE.setup(laser_id_CString,
                //                    laser_type_CString,
                //                    new_date_time_stamp_CString,
                //                    err_msg);
            }

            // set new date/time stamp
            setSystemDateTimeStamp(new_date_time_stamp_CString);

            // set new laser ID
            setSystemLaserID(laser_id);

            p_date_time_stamp = new_date_time_stamp_CString;

            // change state
            SystemState = state_type.set_up;

            CGR_LOG.Write("CCLoseGridSystemAXCCtrl.setup() exiting", LoggingLevels.DIAGNOSTIC_CGR_LOG);
            log_msg = "CCLoseGridSystemAXCCtrl.setup() p_date_time_stamp = ";
            log_msg += new_date_time_stamp_CString;
            CGR_LOG.Write(log_msg, LoggingLevels.DIAGNOSTIC_CGR_LOG);

        }

        public void startLaserScreeningOverallMap()
        {
            if (SystemState != state_type.set_up && SystemState != state_type.complete)
            {
                // function cannot be called in the current system state
                string log_err_msg = "CCLoseGridSystemAXCCtrl.startLaserScreeningOverallMap() error: ";
                log_err_msg += CGSystemErrors.CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR;
                CGR_LOG.Write(log_err_msg, LoggingLevels.ERROR_CGR_LOG);

                throw new Exception(CGSystemErrors.CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR);
            }
            CGR_LOG.Write("CCLoseGridSystemAXCCtrl.startLaserScreeningOverallMap() entered", LoggingLevels.DIAGNOSTIC_CGR_LOG);

            two_ptrs ptrs;
            ptrs.p_axc = this;
            ptrs.p_other = _overall_mode_map;

            _overall_mode_map.clear();
            // set results dir, laser_id and date_time_stamp
            _overall_mode_map._results_dir_CString = DSDBR01.Instance._DSDBR01_BASE_results_directory;
            _overall_mode_map._laser_id_CString = getSystemLaserID();
            _overall_mode_map._date_time_stamp_CString = getSystemDateTimeStamp();

            // kick off thread to gather data from a live laser
            // and produce screening results
            CGR_LOG.Write("CCLoseGridSystemAXCCtrl.startLaserScreeningOverallMap() kicking off new thread for screenOverallMapProc", LoggingLevels.DIAGNOSTIC_CGR_LOG);

            //CWinThread* p_thread = AfxBeginThread(screenOverallMapProc, (LPVOID)(&ptrs), THREAD_PRIORITY_HIGHEST);
            //if (p_thread == NULL)
            //{

            //}
            //else
            //{

            //}

            SystemState = state_type.busy;

            // Allow new thread time to change system data
            Thread.Sleep(100);

            CGR_LOG.Write("CCLoseGridSystemAXCCtrl.startLaserScreeningOverallMap() exiting", LoggingLevels.DIAGNOSTIC_CGR_LOG);

        }

        int screenOverallMapProc(two_ptrs ptrs)
        {
            // call this function to run in it's own thread
            // using AfxBeginThread
            CGR_LOG.Write("screenOverallMapProc() entered", LoggingLevels.DIAGNOSTIC_CGR_LOG);
            CCLoseGridSystemAXCCtrl p_cg_system = ptrs.p_axc;
            CDSDBROverallModeMap p_overall_map = (CDSDBROverallModeMap)ptrs.p_other;

            p_cg_system.setOverallMapLoaded(0);
            p_cg_system.setSMFoundCount(0);
            p_cg_system.setSMMapLoadedCount(0);
            p_cg_system.setEstimateCount(0);
            p_cg_system.setITUOPCount(0);
            p_cg_system.setDuffITUOPCount(0);

            CDSDBROverallModeMap.rcode rval_collection = CDSDBROverallModeMap.rcode.ok;
            CDSDBROverallModeMap.rcode rval_writedata = CDSDBROverallModeMap.rcode.ok;
            CDSDBROverallModeMap.rcode rval_screening = CDSDBROverallModeMap.rcode.ok;
            CDSDBROverallModeMap.rcode rval_writeresults = CDSDBROverallModeMap.rcode.ok;

            // collect data from live laser
            //rval_collection = p_overall_map.collectMapsFromLaser();
            if (rval_collection == CDSDBROverallModeMap.rcode.ok)
            {
                // write data to files
                rval_writedata = p_overall_map.writeMapsToFile();
            }

            if (rval_collection == CDSDBROverallModeMap.rcode.ok)
            {
                // call screening procedure
                rval_screening = p_overall_map.screen();
            }

            if (rval_screening == CDSDBROverallModeMap.rcode.ok)
            {
                // okay so far,
                // write screening results to file

                rval_writeresults = p_overall_map.writeScreeningResultsToFile();
            }

            if (rval_collection == CDSDBROverallModeMap.rcode.ok)
            {
                p_cg_system.setOverallMapLoaded(1);
            }
            else
            {
                p_cg_system.setOverallMapLoaded(0);
            }

            short num_of_supermodes = p_overall_map.supermode_count();
            if (!(num_of_supermodes > 0))
            {
                p_cg_system.setSMFoundCount(0);
            }
            else
            {
                p_cg_system.setSMFoundCount(num_of_supermodes);
            }

            if (p_overall_map._overallModeMapAnalysis._screeningPassed)
                p_cg_system.setOMScreeningPassed(1);
            else
                p_cg_system.setOMScreeningPassed(0);

            p_cg_system.SystemState = state_type.complete;

            if (rval_collection != CDSDBROverallModeMap.rcode.ok)
            {
                CGR_LOG.Write("screenOverallMapProc() calling CCLoseGridSystemAXCCtrl.setLastError", LoggingLevels.DIAGNOSTIC_CGR_LOG);
                p_cg_system.setLastError(rval_collection.ToString());
            }
            else
                if (rval_writedata != CDSDBROverallModeMap.rcode.ok)
                {
                    CGR_LOG.Write("screenOverallMapProc() calling CCLoseGridSystemAXCCtrl.setLastError", LoggingLevels.DIAGNOSTIC_CGR_LOG);
                    p_cg_system.setLastError(rval_writedata.ToString());
                }
                else
                    if (rval_screening != CDSDBROverallModeMap.rcode.ok)
                    {
                        CGR_LOG.Write("screenOverallMapProc() calling CCLoseGridSystemAXCCtrl.setLastError", LoggingLevels.DIAGNOSTIC_CGR_LOG);
                        p_cg_system.setLastError(rval_screening.ToString());
                    }
                    else
                        if (rval_writeresults != CDSDBROverallModeMap.rcode.ok)
                        {
                            CGR_LOG.Write("screenOverallMapProc() calling CCLoseGridSystemAXCCtrl.setLastError", LoggingLevels.DIAGNOSTIC_CGR_LOG);
                            p_cg_system.setLastError(rval_writeresults.ToString());
                        }
                        else
                            if (!(num_of_supermodes > 0))
                            {
                                CGR_LOG.Write("screenOverallMapProc() calling CCLoseGridSystemAXCCtrl.setLastError", LoggingLevels.DIAGNOSTIC_CGR_LOG);
                                p_cg_system.setLastError(CGSystemErrors.CGSYSTEM_NO_VALID_SUPERMODES_FOUND);
                            }

            CGR_LOG.Write("screenOverallMapProc() exiting", LoggingLevels.DIAGNOSTIC_CGR_LOG);

            return 0;

        }

        public void startLaserScreeningSMMap(short supermode_number)
        {
            if (getSystemState() != state_type.complete)
            {
                string log_err_msg = "CCLoseGridSystemAXCCtrl.startLaserScreeningSMMap() error: ";
                log_err_msg += CGSystemErrors.CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR;
                CGR_LOG.Write(log_err_msg, LoggingLevels.ERROR_CGR_LOG);

                // function cannot be called in the current system state
                throw new Exception(CGSystemErrors.CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR);
            }
            CGR_LOG.Write("CCLoseGridSystemAXCCtrl.startLaserScreeningSMMap() entered", LoggingLevels.DIAGNOSTIC_CGR_LOG);
            string log_msg = "CCLoseGridSystemAXCCtrl.startLaserScreeningSMMap() supermode_number = " + supermode_number.ToString();
            CGR_LOG.Write(log_msg, LoggingLevels.DIAGNOSTIC_CGR_LOG);

            if ((supermode_number > (getSMFoundCount() - 1))
                    || (supermode_number < 0))
            {
                string log_err_msg = "CCLoseGridSystemAXCCtrl.startLaserScreeningSMMap() error: " + CGSystemErrors.CGSYSTEM_SPECIFIED_SUPERMODE_NOT_FOUND;
                CGR_LOG.Write(log_err_msg, LoggingLevels.ERROR_CGR_LOG);

                // supermode number requested is not available
                throw new Exception(CGSystemErrors.CGSYSTEM_SPECIFIED_SUPERMODE_NOT_FOUND);
            }

            two_ptrs ptrs = new two_ptrs();
            ptrs.p_axc = this;
            // use this to point to the object representing SM Map to be screened
            ptrs.p_other = _overall_mode_map._supermodes[supermode_number];
            _overall_mode_map._supermodes[supermode_number].clearMapsAndLines();

            // set results dir, laser_id and date_time_stamp

            _overall_mode_map._supermodes[supermode_number]._results_dir_CString
                = DSDBR01.Instance._DSDBR01_BASE_results_directory;

            _overall_mode_map._supermodes[supermode_number]._laser_id_CString
                = getSystemLaserID();

            _overall_mode_map._supermodes[supermode_number]._date_time_stamp_CString
                = getSystemDateTimeStamp();

            // kick off thread to gather data from a live laser
            // and produce screening results
            CGR_LOG.Write("CCLoseGridSystemAXCCtrl.startLaserScreeningSMMap() kicking off new thread for screenSMMapProc", LoggingLevels.DIAGNOSTIC_CGR_LOG);
            
            //CWinThread* p_thread = AfxBeginThread(screenSMMapProc, (LPVOID)(&ptrs), THREAD_PRIORITY_HIGHEST);
            //if (p_thread == NULL)
            //{ }
            //else
            //{ }
            SystemState = state_type.busy;
	        // Allow new thread time to change system data
	        CGR_LOG.Write("CCLoseGridSystemAXCCtrl.startLaserScreeningSMMap() exiting",LoggingLevels.DIAGNOSTIC_CGR_LOG);

        }
        
        int screenSMMapProc(two_ptrs ptrs)
        {
            // call this function to run in it's own thread
            // using AfxBeginThread
            CGR_LOG.Write("screenSMMapProc() entered", LoggingLevels.DIAGNOSTIC_CGR_LOG);

            CCLoseGridSystemAXCCtrl p_cg_system = ptrs.p_axc;
            CDSDBRSuperMode p_sm_map = (CDSDBRSuperMode)ptrs.p_other;

            p_cg_system.setEstimateCount(0);
            p_cg_system.setITUOPCount(0);
            p_cg_system.setDuffITUOPCount(0);
            p_sm_map._superModeMapAnalysis.init();

            CDSDBRSuperMode.rcode rval_collection = CDSDBRSuperMode.rcode.ok;
            CDSDBRSuperMode.rcode rval_writedata = CDSDBRSuperMode.rcode.ok;
            CDSDBRSuperMode.rcode rval_screening = CDSDBRSuperMode.rcode.ok;
            CDSDBRSuperMode.rcode rval_writeresults = CDSDBRSuperMode.rcode.ok;

            // collect data from live laser
            rval_collection = p_sm_map.collectMapsFromLaser();

            if (rval_collection == CDSDBRSuperMode.rcode.ok)
            {
                // write data to files
                rval_writedata = p_sm_map.writeMapsToFile();
            }

            short longitudinal_mode_count = 0;
#if ! HARDWARE_BUILD

	if( rval_collection == CDSDBRSuperMode.rcode.ok )
	{
		// call screening procedure
		rval_screening = p_sm_map.screen();
	}

	if( p_sm_map._superModeMapAnalysis._screeningPassed )
		p_cg_system.setSMScreeningPassed( p_sm_map._sm_number, 1 );
	else
		p_cg_system.setSMScreeningPassed( p_sm_map._sm_number, 0 );

	rval_writeresults = p_sm_map.writeScreeningResultsToFile();

	longitudinal_mode_count = p_sm_map.getLMCount();

#else // HARDWARE_BUILD

	// put a wait in that increments the percent complete
	for( int i = 10; i < 90; i+=10 )
	{
		p_cg_system.setPercentComplete( i );
		Sleep(100);
	}

	longitudinal_mode_count = 20;
	p_cg_system.setSMScreeningPassed( p_sm_map._sm_number, TRUE );

#endif // HARDWARE_BUILD

            short sm_map_loaded_count = 0;
	for( int i = 0; i < (int)(p_cg_system._overall_mode_map._supermodes.size()) ; i++ )
	{
		if( p_cg_system._overall_mode_map._supermodes[i]._maps_loaded )
			sm_map_loaded_count++;
	}
	p_cg_system.setSMMapLoadedCount( sm_map_loaded_count );

    //p_cg_system.setSystemState( CCLoseGridSystemAXCCtrl.state_type.complete );

	if( rval_screening != CDSDBRSuperMode.rcode.ok )
	{
		string log_err_msg="screenSMMapProc() : ";
        //log_err_msg += string(p_sm_map.get_error_pair( rval_screening ).second.c_str());
        //CGR_LOG(log_err_msg,WARNING_CGR_LOG)
	}

	if( rval_writeresults != CDSDBRSuperMode.rcode.ok )
	{
        //string log_err_msg("screenSMMapProc() : ");
        //log_err_msg += string(p_sm_map.get_error_pair( rval_writeresults ).second.c_str());
        //CGR_LOG(log_err_msg,WARNING_CGR_LOG)
	}

	if( rval_collection != CDSDBRSuperMode.rcode.ok )
	{
        //CGR_LOG(string("screenSMMapProc() calling CCLoseGridSystemAXCCtrl.setLastError"),DIAGNOSTIC_CGR_LOG)
		p_cg_system.setLastError( p_sm_map.get_error_pair( rval_collection ) );
	}
	else
	if( rval_writedata != CDSDBRSuperMode.rcode.ok )
	{
        //CGR_LOG(string("screenSMMapProc() calling CCLoseGridSystemAXCCtrl.setLastError"),DIAGNOSTIC_CGR_LOG)
		p_cg_system.setLastError( p_sm_map.get_error_pair( rval_writedata ) );
	}
	else
	if( !(longitudinal_mode_count > 0) )
	{
        //CGR_LOG(string("screenSMMapProc() calling CCLoseGridSystemAXCCtrl.setLastError"),DIAGNOSTIC_CGR_LOG)
        //p_cg_system.setLastError( error_std_pair( CGSYSTEM_NO_VALID_LONGITUDINAL_MODES_FOUND ) );
	}

	CGR_LOG.Write("screenSMMapProc() exiting",LoggingLevels.DIAGNOSTIC_CGR_LOG);

            return 0;
        }

        private int getSMFoundCount()
        {
            lock (this)
            {
                return _supermodes_found_count;
            }
        }

        void setLastError(string last_error)
        {
            String log_err_msg = "CCLoseGridSystemAXCCtrl.setLastError() : setting error : " + last_error;
            CGR_LOG.Write(log_err_msg, LoggingLevels.ERROR_CGR_LOG);
        }

        void setSMFoundCount(short supermodes_found_count)
        {
            lock (this)
            {
                _supermodes_found_count = supermodes_found_count;

                _supermode_map_screening_passed.Clear();
                for (short i = 0; i < supermodes_found_count; i++)
                    _supermode_map_screening_passed.Add(Defaults.SM_NOT_LOADED);
            }

        }
        
        void setOverallMapLoaded(short overall_map_loaded)
        {
            lock (this)
            {
                _overall_map_loaded = overall_map_loaded;
            }
        }

        #region Help methods
        string getSystemLaserID()
        {
            lock (this)
            {
                return _laser_id_CString;
            }
        }

        void setSystemLaserID(String laser_id_CString)
        {
            lock (this)
            {
                _laser_id_CString = laser_id_CString;
            }
        }
        void getNewDateTimeStamp(out String date_time_stamp_CString)
        {
            date_time_stamp_CString = DateTime.Now.ToString("yyyyMMddHHmmss");
        }
        void setSystemDateTimeStamp(String date_time_stamp_CString)
        {
            lock (this)
            {
                _date_time_stamp_CString = date_time_stamp_CString;
            }
        }
        string getSystemDateTimeStamp()
        {
            lock (this)
            {
                return _date_time_stamp_CString;
            }
        }

        bool validLaserType(string laser_type)
        {
            // test against supported laser type
            return laser_type == Defaults.LASER_TYPE_ID_DSDBR01_CSTRING;
        }
        bool validLaserID(string laser_id)
        {
            bool result = true;

            // string must not contain the characters checked below
            if (laser_id.IndexOfAny(Defaults.LASER_ID_BAD_CHARS.ToCharArray()) != -1)
                result = false;
            // string length must be greater than zero
            if (laser_id.Length <= 0)
                result = false;
            // string length must be less than max
            if (laser_id.Length > Defaults.ID_MAX_LENGTH)
                result = false;

            return result;
        }

        void clear()
        {
            // clear all data
            setPercentComplete(0);
            setOverallMapLoaded(0);
            setOMScreeningPassed(0);
            setSMFoundCount(0);
            setSMMapLoadedCount(0);
            setEstimateCount(0);
            setITUOPCount(0);
            setDuffITUOPCount(0);

            _overall_mode_map.clear();

            _ops.clear();

        }
        void setITUOPCount(short ITUOP_count)
        {
            lock (this)
            {
                _ITUOP_count = ITUOP_count;
            }
        }
        void setDuffITUOPCount(short duff_ITUOP_count)
        {
            lock (this)
            {
                _duff_ITUOP_count = duff_ITUOP_count;
            }
        }
        void setEstimateCount(short estimate_count)
        {
            lock (this)
            {
                _estimate_count = estimate_count;
            }
        }
        void setSMMapLoadedCount(short supermode_map_loaded_count)
        {
            lock (this)
            {
                _supermode_map_loaded_count = supermode_map_loaded_count;
            }
        }
        
        void setOMScreeningPassed(short overall_map_screening_passed)
        {
            lock (this)
            {
                _overall_map_screening_passed = overall_map_screening_passed;
            }
        }
        
        void setPercentComplete(short percent_complete)
        {
            lock (this)
            {
                _percent_complete = percent_complete;
            }
        }


        #endregion

        public state_type SystemState
        {
            get
            {
                lock (this)
                {
                    return _system_state;
                }
            }
            internal set
            {
                lock (this)
                {
                    _system_state = value;
                    if (_system_state == state_type.busy)
                        _percent_complete = 0;

                    if (_system_state == state_type.instantiated)
                        _percent_complete = 0;

                    if (_system_state == state_type.set_up)
                        _percent_complete = 0;

                    if (_system_state == state_type.start_up)
                        _percent_complete = 0;

                    if (_system_state == state_type.complete)
                        _percent_complete = 100;
                }
            }
        }
    }
}

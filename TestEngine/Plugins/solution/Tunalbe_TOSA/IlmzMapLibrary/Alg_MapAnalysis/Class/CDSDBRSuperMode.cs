using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Bookham.TestLibrary.Algorithms
{
    internal class matchedlines_typedef : boundary_multimap_typedef//List<KeyValuePair<int, int>>
    {
        
    }

    internal class CDSDBRSuperMode
    {
        public enum rcode
        {
            ok = 0,
            no_data,
            invalid_front_pair_number,
            sequencing_error,
            file_does_not_exist,
            file_already_exists,
            could_not_open_file,
            file_format_invalid,
            files_not_matching_sizes,
            file_read_error,
            maps_not_loaded,
            system_error,
            log_file_error,
            set_current,
            registry_error,
            results_disk_full,
            sequencing_step_size_error,
            hardware_error,
            connectivity_error,
            power_meter_not_found,
            wavemeter_not_found,
            tec_not_found,
            power_read_error,
            duplicate_device_name,
            empty_sequence_slots,
            pxit_module_error,
            device_name_not_on_card,
            device_306_firmware_obsolete,
            laser_alignment_error,
            unknown_error,
            min_number_of_lines_not_reached,
            max_number_of_lines_exceeded,
            mismatched_lines,
            module_name_not_found_error,
            screening_failed
        }

        private Log CGR_LOG;
        public CDSDBRSuperMode()
        {
            CGR_LOG = Log.GetInstance;
        }

        public CDSDBRSuperMode(short superModeNumber,
                    int xAxisLength,
                    int yAxisLength):this()
        {
            //_p_data_protect = NULL;
            //_p_percent_complete = NULL;

            _sm_number = superModeNumber;
            _xAxisLength = xAxisLength;
            _yAxisLength = yAxisLength;

            _qaAnalysisComplete = false;

            clear();
        }

        

        #region Varients

        //short _p_percent_complete;
        internal short _sm_number;
        internal bool _maps_loaded;

        internal String _results_dir_CString;
        internal String _laser_id_CString;
        internal String _date_time_stamp_CString;


        ////////////////////////////////////////////
        ///////////                  ///////////////
        ///////////     The Data     ///////////////
        ///////////                  ///////////////
        ////////////////////////////////////////////
        public CDSDBROverallModeMapLine _upper_line = new CDSDBROverallModeMapLine();
        public CDSDBROverallModeMapLine _lower_line = new CDSDBROverallModeMapLine();
        public CDSDBROverallModeMapLine _middle_line = new CDSDBROverallModeMapLine();
        public CDSDBROverallModeMapLine _filtered_middle_line = new CDSDBROverallModeMapLine();
        public int _row_of_midpoint_of_filtered_middle_line;


        public List<CDSDBRSupermodeMapLine> _lm_upper_lines = new List<CDSDBRSupermodeMapLine>();
        public List<CDSDBRSupermodeMapLine> _lm_lower_lines = new List<CDSDBRSupermodeMapLine>();
        public List<CDSDBRSupermodeMapLine> _lm_middle_lines = new List<CDSDBRSupermodeMapLine>();
        public List<CDSDBRSupermodeMapLine> _lm_filtered_middle_lines = new List<CDSDBRSupermodeMapLine>();
        public List<CDSDBRSupermodeMapLine> _lm_middle_lines_of_frequency = new List<CDSDBRSupermodeMapLine>();
        public matchedlines_typedef _matched_line_numbers = new matchedlines_typedef();

        // count of the number of supermodes removed because of flaws encountered
        public short _lm_upper_lines_removed;
        public short _lm_lower_lines_removed;
        public short _lm_middle_lines_of_frequency_removed;

        public List<double> _average_widths = new List<double>();

        public CCurrentsVector _phase_current = new CCurrentsVector();

        public CCurrentsVector _boundary_detection_ramp = new CCurrentsVector();

        // record the Gain and SOA currents at the time of collecting a supermode map
        public double _I_gain;
        public double _I_soa;
        public double _I_rearsoa;

        public bool _Im_ramp; // true for middle line ramp, false for phase ramp


        public int _sourceDelay;
        public int _measureDelay;

        public CLaserModeMap _map_forward_direct_power = new CLaserModeMap();
        public CLaserModeMap _map_reverse_direct_power = new CLaserModeMap();
        public CLaserModeMap _map_hysteresis_direct_power = new CLaserModeMap();

        public CLaserModeMap _map_forward_filtered_power = new CLaserModeMap();
        public CLaserModeMap _map_reverse_filtered_power = new CLaserModeMap();
        public CLaserModeMap _map_hysteresis_filtered_power = new CLaserModeMap();

        public CLaserModeMap _map_forward_power_ratio = new CLaserModeMap();
        public CLaserModeMap _map_reverse_power_ratio = new CLaserModeMap();
        public CLaserModeMap _map_hysteresis_power_ratio = new CLaserModeMap();

        public CLaserModeMap _map_forward_power_ratio_median_filtered = new CLaserModeMap();
        public CLaserModeMap _map_reverse_power_ratio_median_filtered = new CLaserModeMap();
        public CLaserModeMap _map_hysteresis_power_ratio_median_filtered = new CLaserModeMap();

        public CLaserModeMap _map_forward_photodiode1_current = new CLaserModeMap();
        public CLaserModeMap _map_reverse_photodiode1_current = new CLaserModeMap();
        public CLaserModeMap _map_forward_photodiode2_current = new CLaserModeMap();
        public CLaserModeMap _map_reverse_photodiode2_current = new CLaserModeMap();

        // The above CLaserModeMap objects can contain
        // phase ramped or middle line ramped maps and the members
        // _rows and _cols in those objects change accordingly.
        // However, for the members _rows and _cols in this object
        // _rows always refers to the number of middle line points and
        // _cols always refers to the number of phase current points.
        // As such, the supermode map is always viewed the same way
        // independent of which ramp direction was used to gather the maps.
        public int _rows;
        public int _cols;

        public short numLMs()
        {
            short numLMs = (short)_lm_upper_lines.Count;

            return numLMs;
        }

        
        //
        //	Quantative Analysis
        //
        public ModeAnalysis _modeAnalysis = new ModeAnalysis();

        //	Quantative Analysis
        public rcode runModeAnalysis()
        {
            rcode rval = rcode.ok;

            loadModeAnalysisBoundaryLines();

            _modeAnalysis.runAnalysis();

            _qaAnalysisComplete = true;

            return rval;
        }
        public void loadModeAnalysisBoundaryLines()
        {
            ModeBoundaryLine upperModeBoundaryLine = new ModeBoundaryLine(_upper_line.getCols(), _upper_line.getRows());
            ModeBoundaryLine lowerModeBoundaryLine = new ModeBoundaryLine(_lower_line.getCols(), _lower_line.getRows());

            _modeAnalysis.loadBoundaryLines(_sm_number, _xAxisLength, _yAxisLength, upperModeBoundaryLine, lowerModeBoundaryLine);

            getOMQAThresholds();

            _qaThresholds.setThresholdsToDefault();

            _qaThresholds._slopeWindow = _omqaSlopeWindow;
            _qaThresholds._modalDistortionMinX = _omqaModalDistortionMinX;
            _qaThresholds._modalDistortionMaxX = _omqaModalDistortionMaxX;
            _qaThresholds._modalDistortionMinY = _omqaModalDistortionMinY;
            //GDM 31/10/06 flag that this is the OM qa to the analysis sub
            _qaThresholds._SMMapQA = 0;
            _modeAnalysis.setQAThresholds(_qaThresholds);
        }

        public void getOMQAThresholds()
        {
            _omqaSlopeWindow = DSDBR01.Instance._DSDBR01_OMQA_slope_window_size;
            _omqaModalDistortionMinX = DSDBR01.Instance._DSDBR01_OMQA_modal_distortion_min_x;
            _omqaModalDistortionMaxX = DSDBR01.Instance._DSDBR01_OMQA_modal_distortion_max_x;
            _omqaModalDistortionMinY = DSDBR01.Instance._DSDBR01_OMQA_modal_distortion_min_y;
        }

        public void getMiddleLineRMSValue(ref double rmsValue)
        {
            rmsValue = 0;

            /////////////////////////////////////////////////////////////////////
            /// Use VectorAnalysis to get RMS value of this supermodes middle line
            //
            if (_middleLineRMSValue == 0)
            {
                if (_filtered_middle_line._total_length > 0)
                {
                    List<double> middleLineXPoints;
                    List<double> middleLineYPoints;

                    middleLineXPoints = _filtered_middle_line.getCols();
                    middleLineYPoints = _filtered_middle_line.getRows();

                    double linearFitLineSlope = 0;
                    double linearFitLineIntercept = 0;

                    // find linear fit line
                    VectorAnalysis.linearFit(middleLineXPoints,
                                            middleLineYPoints,
                                            out linearFitLineSlope,
                                            out linearFitLineIntercept);

                    _middleLineSlope = linearFitLineSlope;

                    // find distances from points to linear fit line
                    List<double> distances = new List<double>();
                    VectorAnalysis.distancesFromPointsToLine(middleLineXPoints,
                                                            middleLineYPoints,
                                                            linearFitLineSlope,
                                                            linearFitLineIntercept,
                                                            distances);

                    // square each distance
                    for (int i = 0; i < distances.Count; i++)
                    {
                        distances[i] = distances[i] * distances[i];
                    }

                    // get mean of squared distances
                    double meanSqDistance = 0;
                    meanSqDistance = VectorAnalysis.meanOfValues(distances);

                    // get root of mean of squared distances
                    _middleLineRMSValue = Math.Sqrt(meanSqDistance);
                }
            }

            rmsValue = _middleLineRMSValue;
        }

        public void getMiddleLineSlope(ref double slope)
        {
            slope = 0;

            if (_middleLineSlope == 0)
            {
                List<double> middleLineXPoints;
                List<double> middleLineYPoints;

                middleLineXPoints = _filtered_middle_line.getCols();
                middleLineYPoints = _filtered_middle_line.getRows();

                double linearFitLineSlope = 0;
                double linearFitLineIntercept = 0;

                // find linear fit line
                VectorAnalysis.linearFit(middleLineXPoints,
                                        middleLineYPoints,
                                        out linearFitLineSlope,
                                        out linearFitLineIntercept);

                _middleLineSlope = linearFitLineSlope;
            }

            slope = _middleLineSlope;
        }


        public int _xAxisLength;
        public int _yAxisLength;

        public QAThresholds _qaThresholds = new QAThresholds();

        public int _omqaSlopeWindow;
        public double _omqaModalDistortionMinX;
        public double _omqaModalDistortionMaxX;
        public double _omqaModalDistortionMinY;

        public double _middleLineRMSValue;
        public double _middleLineSlope;

        public bool _qaAnalysisComplete;


        //
        ////////////////////////////////////////
        ///	Super mode analysis
        //
        public SuperModeMapAnalysis _superModeMapAnalysis = new SuperModeMapAnalysis();
        //
        //////////////////////////////////////

        #endregion

        const int MIN_DISTANCE_FROM_EDGE_TO_ZPC = 4;
        const int MIN_NUMBER_OF_JUMPS_PER_MAP = 200;
        const int MAX_NUMBER_OF_JUMPS_PER_MAP = 1000;
        const int MIN_NUMBER_OF_LINES_PER_MAP = 2;
        const int MAX_NUMBER_OF_LINES_PER_MAP = 30;
        const int MIN_NUMBER_OF_JUMPS_TO_FORM_A_BOUNDARY_LINE = 4;


        const int MAX_DISTANCE_TO_PAD_VERTICALLY = 2;


        public void clear()
        {
            _upper_line.clear();
            _lower_line.clear();
            _middle_line.clear();

            _phase_current.clear();
            _filtered_middle_line.clear();

            _boundary_detection_ramp.clear();

            clearMapsAndLines();

            _maps_loaded = false;
            _results_dir_CString = "";
            _date_time_stamp_CString = "";
            _laser_id_CString = "";

            _Im_ramp = false;
            _rows = 0;
            _cols = 0;
            _sourceDelay = 0;
            _measureDelay = 0;
            _sm_number = 0;
            _maps_loaded = false;
            _row_of_midpoint_of_filtered_middle_line = 0;
            _I_gain = 0;
            _I_soa = 0;
            _I_rearsoa = 0;

            _middleLineRMSValue = 0;
            _middleLineSlope = 0;

            _modeAnalysis.init();
        }

        public void clearMapsAndLines()
        {
            _map_forward_direct_power.clear();
            _map_reverse_direct_power.clear();
            _map_hysteresis_direct_power.clear();

            _map_forward_filtered_power.clear();
            _map_reverse_filtered_power.clear();
            _map_hysteresis_filtered_power.clear();

            _map_forward_power_ratio.clear();
            _map_reverse_power_ratio.clear();
            _map_hysteresis_power_ratio.clear();

            _map_forward_power_ratio_median_filtered.clear();
            _map_reverse_power_ratio_median_filtered.clear();
            _map_hysteresis_power_ratio_median_filtered.clear();

            _map_forward_photodiode1_current.clear();
            _map_reverse_photodiode1_current.clear();
            _map_forward_photodiode2_current.clear();
            _map_reverse_photodiode2_current.clear();

           
            for (int i = 0; i < _lm_upper_lines.Count; i++)
            {
                _lm_upper_lines[i].clear();
            }
            _lm_upper_lines.Clear();

            for (int i = 0; i < _lm_lower_lines.Count; i++)
            {
                _lm_lower_lines[i].clear();
            }
            _lm_lower_lines.Clear();

            for (int i = 0; i < _lm_middle_lines.Count; i++)
            {
                _lm_middle_lines[i].clear();
            }
            _lm_middle_lines.Clear();

            for (int i = 0; i < _lm_filtered_middle_lines.Count; i++)
            {
                _lm_filtered_middle_lines[i].clear();
            }
            _lm_filtered_middle_lines.Clear();

            for (int i = 0; i < _lm_middle_lines_of_frequency.Count; i++)
            {
                _lm_middle_lines_of_frequency[i].clear();
            }
            _lm_middle_lines_of_frequency.Clear();

            _average_widths.Clear();
            _matched_line_numbers.Clear();

            _lm_upper_lines_removed = 0;
            _lm_lower_lines_removed = 0;
            _lm_middle_lines_of_frequency_removed = 0;
        }

        public short getLMCount() 
        {
            short lm_count = 0;

            for (int i = 0; i < _lm_middle_lines_of_frequency.Count; i++)
            {
                if (_lm_middle_lines_of_frequency[i]._points.Count > 0)
                {
                    lm_count++;
                }
            }

            return lm_count;
        }

        public rcode ReadMapsFromFile(
            String results_dir_CString,
            String laser_id_CString,
            String date_time_stamp_CString,
            String map_ramp_direction_CString)
        {
            // Read the supermode maps from file.
            // The supermode maps are stored in files named
            // MATRIX_forwardPowerRatio_DSDBR[##]_[laser_ID]_[YYYYMMDDHHMMSS]_SM[i]_[P|M].csv
            // MATRIX_reversePowerRatio_DSDBR[##]_[laser_ID]_[YYYYMMDDHHMMSS]_SM[i]_[P|M].csv
            // MATRIX_forwardPowerRatio_DSDBR[##]_[laser_ID]_[YYYYMMDDHHMMSS]_SM[i]_[P|M].csv
            // MATRIX_forwardPower_DSDBR[##]_[laser_ID]_[YYYYMMDDHHMMSS]_SM[i]_[P|M].csv
            // MATRIX_reversePower_DSDBR[##]_[laser_ID]_[YYYYMMDDHHMMSS]_SM[i]_[P|M].csv
            // MATRIX_forwardPower_DSDBR[##]_[laser_ID]_[YYYYMMDDHHMMSS]_SM[i]_[P|M].csv
            // where i is the supermode number, indexed from 0
            //
            // The currents used to collect these maps are stored in 2 files
            // Im_OverallMap_DSDBR[##]_[laser_ID]_[YYYYMMDDHHMMSS]_SM[i].csv
            // Ip_OverallMap_DSDBR[##]_[laser_ID]_[YYYYMMDDHHMMSS]_SM[i].csv
            // where Im is the middle line current (combination of front and rear current)
            // and Ip is phase current

            CGR_LOG.Write("CDSDBRSuperMode.loadMapsFromFile() entered", LoggingLevels.DIAGNOSTIC_CGR_LOG);

            rcode rval = rcode.ok;

            string file_path = results_dir_CString;

            // Check for directory without trailing '\\' character
            if (!file_path.EndsWith("\\")) file_path += "\\";

            string smNumberAsCString = _sm_number.ToString();

            //////////////////////////////////////////////////////

            string fileNameString = "";
            fileNameString += Defaults.USCORE;
            fileNameString += Defaults.LASER_TYPE_ID_DSDBR01_CSTRING;
            fileNameString += Defaults.USCORE;
            fileNameString += laser_id_CString;
            fileNameString += Defaults.USCORE;
            fileNameString += date_time_stamp_CString;
            fileNameString += Defaults.USCORE;
            fileNameString += "SM";
            fileNameString += smNumberAsCString;

            string file_path_phase_currents = file_path + Defaults.PHASE_CURRENT_CSTRING + fileNameString + Defaults.CSV;
            string file_path_middle_line_currents = file_path + Defaults.MIDDLELINE_CURRENT_CSTRING + fileNameString + Defaults.CSV;

            fileNameString += Defaults.USCORE;
            fileNameString += map_ramp_direction_CString;
            fileNameString += Defaults.CSV;

            string file_path_forward_power = file_path + Defaults.MATRIX_ + Defaults.MEAS_TYPE_ID_FORWARDPOWER_CSTRING + fileNameString;
            string file_path_reverse_power = file_path + Defaults.MATRIX_ + Defaults.MEAS_TYPE_ID_REVERSEPOWER_CSTRING + fileNameString;
            string file_path_hysteresis_power = file_path + Defaults.MATRIX_ + Defaults.MEAS_TYPE_ID_HYSTERESISPOWER_CSTRING + fileNameString;

            string file_path_forward_power_ratio = file_path + Defaults.MATRIX_ + Defaults.MEAS_TYPE_ID_FORWARDPOWERRATIO_CSTRING + fileNameString;
            string file_path_reverse_power_ratio = file_path + Defaults.MATRIX_ + Defaults.MEAS_TYPE_ID_REVERSEPOWERRATIO_CSTRING + fileNameString;
            string file_path_hysteresis_power_ratio = file_path + Defaults.MATRIX_ + Defaults.MEAS_TYPE_ID_HYSTERESISPOWERRATIO_CSTRING + fileNameString;


            // have file names, now try to read them

            CLaserModeMap.rcode load_map_from_file_rval = CLaserModeMap.rcode.ok;
            CCurrentsVector.rcode load_Ip_from_file_rval = CCurrentsVector.rcode.ok;
            CDSDBROverallModeMapLine.rcode load_Im_from_file_rval = CDSDBROverallModeMapLine.rcode.ok;

            if (load_map_from_file_rval == CLaserModeMap.rcode.ok)
                _map_forward_direct_power.readFromFile(file_path_forward_power);

            if (load_map_from_file_rval == CLaserModeMap.rcode.ok)
                _map_reverse_direct_power.readFromFile(file_path_reverse_power);

            if (load_map_from_file_rval == CLaserModeMap.rcode.ok)
                _map_hysteresis_direct_power.readFromFile(file_path_hysteresis_power);

            if (load_map_from_file_rval == CLaserModeMap.rcode.ok)
                load_map_from_file_rval = _map_forward_power_ratio.readFromFile(file_path_forward_power_ratio);

            if (load_map_from_file_rval == CLaserModeMap.rcode.ok)
            {
                _map_hysteresis_power_ratio.readFromFile(file_path_hysteresis_power_ratio);
                load_map_from_file_rval = _map_reverse_power_ratio.readFromFile(file_path_reverse_power_ratio);

                if (load_map_from_file_rval != CLaserModeMap.rcode.ok)
                {
                    if (load_map_from_file_rval != CLaserModeMap.rcode.ok
                        && _map_forward_power_ratio._map.Count == _map_hysteresis_power_ratio._map.Count)
                    { // Found _map_hysteresis_power_ratio _map_forward_power_ratio
                        // Need to calculate _map_reverse_power_ratio

                        List<double> reversePowerRatio = new List<double>();

                        for (int i = 0; i < _map_forward_power_ratio._map.Count; i++)
                        {
                            reversePowerRatio.Add(
                                _map_forward_power_ratio._map[i] - _map_hysteresis_power_ratio._map[i]);
                        }

                        _map_reverse_power_ratio.clear();
                        _map_reverse_power_ratio.setMap(reversePowerRatio,
                            _map_forward_power_ratio._rows, _map_forward_power_ratio._cols);

                        load_map_from_file_rval = CLaserModeMap.rcode.ok; // calculated instead of read from file
                    }
                }
            }

            if (load_map_from_file_rval == CLaserModeMap.rcode.ok)
            {
                load_Ip_from_file_rval = _phase_current.readFromFile(file_path_phase_currents);

                load_Im_from_file_rval = _filtered_middle_line.readCurrentsFromFile(file_path_middle_line_currents);
            }

            // map error codes

            if (load_map_from_file_rval == CLaserModeMap.rcode.file_does_not_exist
             || load_Ip_from_file_rval == CCurrentsVector.rcode.file_does_not_exist
             || load_Im_from_file_rval == CDSDBROverallModeMapLine.rcode.file_does_not_exist)
            {
                rval = rcode.file_does_not_exist;
                CGR_LOG.Write("CDSDBRSuperMode.loadMapsFromFile() file_does_not_exist", LoggingLevels.ERROR_CGR_LOG);
            }
            else
                if (load_map_from_file_rval == CLaserModeMap.rcode.could_not_open_file
                 || load_Ip_from_file_rval == CCurrentsVector.rcode.could_not_open_file
                 || load_Im_from_file_rval == CDSDBROverallModeMapLine.rcode.could_not_open_file)
                {
                    rval = rcode.could_not_open_file;
                    CGR_LOG.Write("CDSDBRSuperMode.loadMapsFromFile() could_not_open_file", LoggingLevels.ERROR_CGR_LOG);
                }
                else
                    if (load_map_from_file_rval == CLaserModeMap.rcode.file_format_invalid
                     || load_map_from_file_rval == CLaserModeMap.rcode.vector_size_rows_cols_do_not_match
                     || load_Ip_from_file_rval == CCurrentsVector.rcode.file_format_invalid
                     || load_Ip_from_file_rval == CCurrentsVector.rcode.vector_size_rows_cols_do_not_match
                     || load_Im_from_file_rval == CDSDBROverallModeMapLine.rcode.file_format_invalid)
                    {
                        rval = rcode.file_format_invalid;
                        CGR_LOG.Write("CDSDBRSuperMode.loadMapsFromFile() file_format_invalid", LoggingLevels.ERROR_CGR_LOG);
                    }

            if (rval == rcode.ok)
            {
                // have read in files
                // check maps are of matching size

                _Im_ramp = map_ramp_direction_CString == Defaults.RAMP_DIRECTION_MIDDLE_LINE;

                if ((_Im_ramp && ((_phase_current._length != _map_forward_power_ratio._rows)
                               || (_phase_current._length != _map_reverse_power_ratio._rows)
                               || (_filtered_middle_line._points.Count != _map_forward_power_ratio._cols)
                               || (_filtered_middle_line._points.Count != _map_reverse_power_ratio._cols)))
                || (!_Im_ramp && ((_phase_current._length != _map_forward_power_ratio._cols)
                               || (_phase_current._length != _map_reverse_power_ratio._cols)
                               || (_filtered_middle_line._points.Count != _map_forward_power_ratio._rows)
                               || (_filtered_middle_line._points.Count != _map_reverse_power_ratio._rows))))
                {
                    // maps are not of matching size
                    rval = rcode.files_not_matching_sizes;
                    CGR_LOG.Write("CDSDBRSuperMode.loadMapsFromFile() files_not_matching_sizes", LoggingLevels.ERROR_CGR_LOG);
                }
            }

            if (rval == rcode.ok)
            {
                // have successfully loaded the essential files

                _results_dir_CString = results_dir_CString;
                _laser_id_CString = laser_id_CString;
                _date_time_stamp_CString = date_time_stamp_CString;

                _rows = _filtered_middle_line._points.Count;
                _cols = _phase_current._length;

                _xAxisLength = _cols;
                _yAxisLength = _rows;

                _maps_loaded = true;
            }
            else
            {
                clearMapsAndLines();
                _maps_loaded = false;
                CGR_LOG.Write("CDSDBRSuperMode.loadMapsFromFile() maps not loaded", LoggingLevels.ERROR_CGR_LOG);
            }

            CGR_LOG.Write("CDSDBRSuperMode.loadMapsFromFile() exiting", LoggingLevels.DIAGNOSTIC_CGR_LOG);

            return rval;
        }


        public rcode screen()
        {
            rcode rval = rcode.ok;
            CGR_LOG.Write("CDSDBRSuperMode.screen() entered", LoggingLevels.DIAGNOSTIC_CGR_LOG);
            if (_Im_ramp)
            { // not yet supported

                CGR_LOG.Write("CDSDBRSuperMode.screen() screening of middle-line ramped data is not yet supported", LoggingLevels.ERROR_CGR_LOG);

                rval = rcode.screening_failed;
            }
            if (rval == rcode.ok)
            {
                //if (DSDBR01.Instance._DSDBR01_SMBD_read_boundaries_from_file)
                //{
                //    // Step 1: Read in boundaries from file
                //    rval = readBoundariesFromFile();
                //}
                //else
                {
                    // Step 1: Find boundaries
                    rval = findLMBoundaries();
                }
            }

            _lm_upper_lines_removed = (short)_lm_upper_lines.Count;
            _lm_lower_lines_removed = (short)_lm_lower_lines.Count;

            // Step 2: Find middle lines of frequency in longitudinal modes
            if (rval == CDSDBRSuperMode.rcode.ok)
                rval = findLMMiddleLines();

            //_lm_middle_lines_of_frequency_removed -= (short)_lm_middle_lines.Count;

            // Step 3: Filter middle lines of frequency in longitudinal modes
            if (rval == CDSDBRSuperMode.rcode.ok)
                rval = filterLMMiddleLines();

            // Step 5: Shift middle lines of frequency in longitudinal modes
            if (rval == CDSDBRSuperMode.rcode.ok)
                rval = calcLMMiddleLinesOfFreq();

            // Step 6: Check each middle line to ensure
            // it is in one longitudinal mode,
            // if not, remove the longitudinal mode
            if (rval == CDSDBRSuperMode.rcode.ok)
                rval = checkLMMiddleLinesOfFreq();

            _lm_upper_lines_removed -= (short)_lm_upper_lines.Count;
            _lm_lower_lines_removed -= (short)_lm_lower_lines.Count;
            //_lm_middle_lines_of_frequency_removed -= (short)_lm_middle_lines_of_frequency.Count;

            if (rval == CDSDBRSuperMode.rcode.ok)
            {
                //////////////////////////////////////////////////////////////////////////////
                /////
                ////
                //setContinuityValues();

                List<double> xPoints = new List<double>();
                List<double> yPoints = new List<double>();
                bool hasChangeOfFrontPairs;

                List<ModeBoundaryLine> upperLines = new List<ModeBoundaryLine>();
                List<ModeBoundaryLine> lowerLines = new List<ModeBoundaryLine>();
                List<ModeBoundaryLine> middleLines = new List<ModeBoundaryLine>();

                for (int i = 0; i < _lm_upper_lines.Count; i++)
                {
                    xPoints.Clear();
                    yPoints.Clear();

                    xPoints = _lm_upper_lines[i].getCols();
                    yPoints = _lm_upper_lines[i].getRows();
                    hasChangeOfFrontPairs = _lm_upper_lines[i].hasChangeOfFrontPairs();

                    ModeBoundaryLine upperLine = new ModeBoundaryLine(xPoints, yPoints, hasChangeOfFrontPairs);
                    upperLines.Add(upperLine);
                }

                for (int i = 0; i < _lm_lower_lines.Count; i++)
                {
                    xPoints.Clear();
                    yPoints.Clear();

                    xPoints = _lm_lower_lines[i].getCols();
                    yPoints = _lm_lower_lines[i].getRows();
                    hasChangeOfFrontPairs = _lm_lower_lines[i].hasChangeOfFrontPairs();

                    ModeBoundaryLine lowerLine = new ModeBoundaryLine(xPoints, yPoints, hasChangeOfFrontPairs);
                    lowerLines.Add(lowerLine);
                }

                for (int i = 0; i < _lm_middle_lines_of_frequency.Count; i++)
                {
                    xPoints.Clear();

                    yPoints.Clear();

                    xPoints = _lm_middle_lines_of_frequency[i].getCols();
                    yPoints = _lm_middle_lines_of_frequency[i].getRows();
                    hasChangeOfFrontPairs = _lm_lower_lines[i].hasChangeOfFrontPairs();

                    ModeBoundaryLine middleLine = new ModeBoundaryLine(xPoints, yPoints, hasChangeOfFrontPairs);
                    middleLines.Add(middleLine);
                }

                List<double> _continuityValues = new List<double>();
                _superModeMapAnalysis.init();

                SuperModeMapAnalysis.rcode rval_sm_qa =
                    _superModeMapAnalysis.runAnalysis(_sm_number,
                                                upperLines,
                                                lowerLines,
                                                middleLines,
                                                _lm_upper_lines_removed,
                                                _lm_lower_lines_removed,
                                                _lm_middle_lines_of_frequency_removed,
                                                _xAxisLength,
                                                _yAxisLength,
                                                _continuityValues,
                                                _results_dir_CString,
                                                _laser_id_CString,
                                                _date_time_stamp_CString);

                if (rval_sm_qa != SuperModeMapAnalysis.rcode.ok)
                    rval = rcode.screening_failed;

                //PassFailCollated.instance()->addAnalysedSuperMode(this);
                PassFailCollated.instance.addAnalysedSuperMode(this);

                ////
                ///////////////////////////////////////////////////////////////////////////////
            }

            if (rval != rcode.ok)
            {
                String log_err_msg = "CDSDBRSuperMode.screen(): error = ";
                //log_err_msg += CString( get_error_pair(rval).second.c_str() );
                CGR_LOG.Write(log_err_msg, LoggingLevels.ERROR_CGR_LOG);

                rval = rcode.screening_failed;
            }

            CGR_LOG.Write("CDSDBRSuperMode.screen() exiting", LoggingLevels.DIAGNOSTIC_CGR_LOG);

            return rval;

        }


        public rcode findLMBoundaries()
        {
            CGR_LOG.Write("CDSDBRSuperMode::findLMBoundaries() entered", LoggingLevels.DIAGNOSTIC_CGR_LOG);
            rcode rval = rcode.ok;

            if (!_maps_loaded)
            {
                rval = rcode.maps_not_loaded;
            }

            if (rval == rcode.ok)
            {
                rval = applyMedianFilter(
                    _map_forward_power_ratio,
                    _map_forward_power_ratio_median_filtered,
                    (uint)DSDBR01.Instance._DSDBR01_SMBD_median_filter_rank);
            }
            if (rval == rcode.ok)
            {
                rval = applyMedianFilter(
                    _map_reverse_power_ratio,
                    _map_reverse_power_ratio_median_filtered,
                    (uint)DSDBR01.Instance._DSDBR01_SMBD_median_filter_rank);
            }
            if (rval == rcode.ok)
            {
                // find the mode jumps on each forward ramp
                rval = findJumps(_map_forward_power_ratio_median_filtered, true);
                List<CLaserModeMap> tempData = new List<CLaserModeMap>();
                tempData.Add(_map_forward_power_ratio_median_filtered);
                Util_WriteDataForDebug.WriteDataToFile(tempData, @"C:\jack.zhang\my Documents\test Kit\CG_C#version_Tommyu\Alg_MapAnalysis\Test\Results\ppp.csv");
                Util_WriteDataForDebug.WriteDataToFile(_map_forward_power_ratio_median_filtered._boundary_points, @"C:\jack.zhang\my Documents\test Kit\CG_C#version_Tommyu\Alg_MapAnalysis\Test\Results\Jpp.csv");
                string log_msg = "CDSDBRSuperMode.findLMBoundaries() ";
                log_msg += string.Format("found {0} jumps on forward map",
                    _map_forward_power_ratio_median_filtered._boundary_points.Count);
                CGR_LOG.Write(log_msg, LoggingLevels.INFO_CGR_LOG);
            }
            if (rval == rcode.ok)
            {
                // find the mode jumps on each reverse ramp
                rval = findJumps(_map_reverse_power_ratio_median_filtered, false);

                string log_msg = "CDSDBRSuperMode.findLMBoundaries() ";
                log_msg += string.Format("found {0} jumps on reverse map",
                    _map_reverse_power_ratio_median_filtered._boundary_points.Count);
                CGR_LOG.Write(log_msg, LoggingLevels.INFO_CGR_LOG);
            }

            if (rval == rcode.ok)
            {
                rval = findZDPRCBoundaries();
            }

            if (rval == rcode.ok)
            {
                rval = linkJumps();
            }
            if (rval == rcode.ok && DSDBR01.Instance._DSDBR01_SMBD_extrapolate_to_map_edges)
            {
                rval = insertExtrapolatedMapBorderPoints();
            }
            if (rval == rcode.ok)
            {
                rval = createLMBoundaryLines();
            }
            CGR_LOG.Write("CDSDBRSuperMode.findLMBoundaries() exiting", LoggingLevels.DIAGNOSTIC_CGR_LOG);
            return rval;
        }

        public rcode applyMedianFilter(
                CLaserModeMap map_in,
                CLaserModeMap filtered_map_out,
                uint rank)
        {
            CGR_LOG.Write("CDSDBRSuperMode.applyMedianFilter() entered", LoggingLevels.DIAGNOSTIC_CGR_LOG);
            rcode rval = rcode.ok;
            VectorAnalysis.medianFilter(
                map_in._map,
                filtered_map_out._map,
                (int)rank,
                map_in._cols);
            // update the rows and cols

            filtered_map_out._rows = map_in._rows;
            filtered_map_out._cols = map_in._cols;

            if (DSDBR01.Instance._DSDBR01_SMBD_write_debug_files)
            {
                // Assume _abs_filepath is set from loading data from file
                // or from writing to file after gathering data from laser
                string abs_filepath = map_in._abs_filepath;
                abs_filepath = abs_filepath.Replace(Defaults.MATRIX_, Defaults.MATRIX_FILTERED_);

                // Write filtered map to file
                filtered_map_out.writeToFile(abs_filepath, true);
            }
            CGR_LOG.Write("CDSDBRSuperMode::applyMedianFilter() exiting", LoggingLevels.DIAGNOSTIC_CGR_LOG);

            return rval;
        }

        public rcode findJumps(
                CLaserModeMap map_in,
                bool forward_map)		// forward map (true) or reverse map (false)
        {
            CGR_LOG.Write("CDSDBRSuperMode.findJumps() entered", LoggingLevels.DIAGNOSTIC_CGR_LOG);

            rcode rval = rcode.ok;
            //std.vector<double>.iterator i_Pr_start = map_in._map.begin();
            int i_Pr_start = 0;
            map_in._boundary_points.Clear();

            List<double> I_ramp = new List<double>();
            int rows = 0;
            int cols = 0;

            if (_Im_ramp) // middle line ramped
            {
                rows = _phase_current._currents.Count;
                cols = _filtered_middle_line._points.Count;

                for (int i = 0; i < cols; i++)
                    I_ramp.Add(_filtered_middle_line._points[i].I_rear);
            }
            else // phase ramped
            {
                rows = (_filtered_middle_line._points.Count);
                cols = (_phase_current._currents.Count);

                for (int i = 0; i < cols; i++)
                    I_ramp.Add(_phase_current._currents[i]);
            }

            CCurrentsVector.rcode read_ramp_rval = _boundary_detection_ramp.readFromFile(
                DSDBR01.Instance._DSDBR01_SMBD_ramp_abspath);

            if (read_ramp_rval == CCurrentsVector.rcode.ok)
            {
                if ((_boundary_detection_ramp._currents.Count) != cols)
                {
                    CGR_LOG.Write("CDSDBRSuperMode.findJumps() : _boundary_detection_ramp does not match current ramp", LoggingLevels.DIAGNOSTIC_CGR_LOG);
                    CGR_LOG.Write("CDSDBRSuperMode.findJumps() : using current ramp for boundary detection", LoggingLevels.DIAGNOSTIC_CGR_LOG);
                }
                else
                {
                    CGR_LOG.Write("CDSDBRSuperMode.findJumps() : _boundary_detection_ramp loaded from file", LoggingLevels.DIAGNOSTIC_CGR_LOG);
                    CGR_LOG.Write("CDSDBRSuperMode.findJumps() : using _boundary_detection_ramp for boundary detection", LoggingLevels.DIAGNOSTIC_CGR_LOG);

                    I_ramp.Clear();
                    for (int i = 0; i < cols; i++)
                        I_ramp.Add(_boundary_detection_ramp._currents[i]);
                }
            }
            else
            {
                CGR_LOG.Write("CDSDBRSuperMode.findJumps() : _boundary_detection_ramp not loaded from file", LoggingLevels.DIAGNOSTIC_CGR_LOG);
                CGR_LOG.Write("CDSDBRSuperMode.findJumps() : using current ramp for boundary detection", LoggingLevels.DIAGNOSTIC_CGR_LOG);
            }



            // iterate though each line of the map

            for (int row = 0; row < rows; row++)
            {
                int index_of_1st_point_on_line = row * cols;
                // find boundary points on a forward ramped line and store them
                modeBoundaries(
                    map_in._map,//Pr vector
                    i_Pr_start,					// starting point on Pr vector
                    I_ramp, //I vector
                    0,				// starting point on I vector  ,I_ramp.begin()
                    index_of_1st_point_on_line,	// index of first point within a map
                    1,							// increment on vectors to next point
                    cols,						// number of points in the line
                    ref map_in._boundary_points,	// structure to store boundary points
                    forward_map);				// forward map (true) or reverse map (false)  

                i_Pr_start += map_in._cols; // iterate to start of next line
            }
            if (DSDBR01.Instance._DSDBR01_SMBD_write_debug_files)
            {
                // Assume _abs_filepath is set from loading data from file
                // or from writing to file after gathering data from laser
                String abs_filepath = map_in._abs_filepath;
                abs_filepath = abs_filepath.Replace(Defaults.MATRIX_, Defaults.JUMPS_);

                // Write jumps detected in filtered map to file
                map_in.writeJumpsToFile(abs_filepath);
            }
            CGR_LOG.Write("CDSDBRSuperMode.findJumps() exiting", LoggingLevels.DIAGNOSTIC_CGR_LOG);
            return rval;
        }

        const int FITTED_LINE_LENGHT =8;
        const int MIN_NO_OF_POINTS_TO_DETECT_JUMP = 4;

        public void modeBoundaries(
                List<double> Pr_start,//Po vector
                int i_Pr_start,	// starting point on Po vector
                List<double> I_start,//I vector
                int i_I_start,	// starting point on I vector
                int start_point_index,			// index of first point within a map
                int index_increment,				// increment on vectors to next point
                int line_length,					// number of points in the line
                ref boundary_multimap_typedef boundary_map,	// map of boundary jumps found
                bool forward_map)							// forward map (true) or reverse map (false)
        {
            int I_ramp_boundaries_max = 0;
            int I_ramp_boundaries_min_separation = 20;
            double min_d2Pr_dI2_zero_cross_jump_size = 0;
            double max_sum_of_squared_distances_from_Pr_I_line = 0;
            double min_slope_change_of_Pr_I_lines = 0;

            if (forward_map)
            {
                I_ramp_boundaries_max = DSDBR01.Instance._DSDBR01_SMBDFWD_I_ramp_boundaries_max;
                I_ramp_boundaries_min_separation = DSDBR01.Instance._DSDBR01_SMBDFWD_boundaries_min_separation;
                min_d2Pr_dI2_zero_cross_jump_size = DSDBR01.Instance._DSDBR01_SMBDFWD_min_d2Pr_dI2_zero_cross_jump_size;
                max_sum_of_squared_distances_from_Pr_I_line = DSDBR01.Instance._DSDBR01_SMBDFWD_max_sum_of_squared_distances_from_Pr_I_line;
                min_slope_change_of_Pr_I_lines = DSDBR01.Instance._DSDBR01_SMBDFWD_min_slope_change_of_Pr_I_lines;
            }
            else // reverse map
            {
                I_ramp_boundaries_max = DSDBR01.Instance._DSDBR01_SMBDREV_I_ramp_boundaries_max;
                I_ramp_boundaries_min_separation = DSDBR01.Instance._DSDBR01_SMBDREV_boundaries_min_separation;
                min_d2Pr_dI2_zero_cross_jump_size = DSDBR01.Instance._DSDBR01_SMBDREV_min_d2Pr_dI2_zero_cross_jump_size;
                max_sum_of_squared_distances_from_Pr_I_line = DSDBR01.Instance._DSDBR01_SMBDREV_max_sum_of_squared_distances_from_Pr_I_line;
                min_slope_change_of_Pr_I_lines = DSDBR01.Instance._DSDBR01_SMBDREV_min_slope_change_of_Pr_I_lines;
            }



            // keep a count of how many jumps are detected
            int boundary_count = 0;

            // store detected jumps in a map
            boundary_multimap_typedef jumps = new boundary_multimap_typedef();

            // store d2Pr_dI2 at each jump detected
            SortedDictionary<int, double> d2Pr_dI2_at_jumps = new SortedDictionary<int, double>();

            // keep iterators to the windowed values
            int[] i_I = new int[MIN_NO_OF_POINTS_TO_DETECT_JUMP];
            int[] i_Pr = new int[MIN_NO_OF_POINTS_TO_DETECT_JUMP];

            // keep slopes of lines fitted to data of MIN_NO_OF_POINTS_TO_DETECT_JUMP windows
            double[] slopes = new double[FITTED_LINE_LENGHT];
            for (int i = 0; i < FITTED_LINE_LENGHT; i++)
            {
                slopes[i] = 0;
            }
            double oldest_slope = 0;

            // keep sum_of_squared_distances from lines fitted to data of MIN_NO_OF_POINTS_TO_DETECT_JUMP windows
            double[] sum_of_squared_distances = new double[FITTED_LINE_LENGHT];
            for (int i = 0; i < FITTED_LINE_LENGHT; i++)
            {
                sum_of_squared_distances[i] = 0;
            }
            double oldest_sum_of_squared_distance = 0;

            // find the iterators pointing to the values in the first window
            int point_count = 0;
            for (int i = 0; i < MIN_NO_OF_POINTS_TO_DETECT_JUMP; i++)
            {
                i_I[i] = i_I_start;
                i_Pr[i] = i_Pr_start;
            }
            // first point in first window is now correct, find subsequent points
            for (int i = 1; i < MIN_NO_OF_POINTS_TO_DETECT_JUMP; i++)
            {
                for (int j = i; j < MIN_NO_OF_POINTS_TO_DETECT_JUMP; j++)
                {
                    while (point_count < index_increment * i)
                    {
                        i_I[j]++;
                        i_Pr[j]++;
                        point_count++;
                    }
                }
            }
            double oldest_I = 0;
            double oldest_Pr = 0;

            // store values in vectors
            List<double> x = new List<double>();
            List<double> y = new List<double>();
            for (int i = 0; i < FITTED_LINE_LENGHT - MIN_NO_OF_POINTS_TO_DETECT_JUMP; i++)
            { // fill initial points of line with zeros
                x.Add(0);
                y.Add(0);
            }
            for (int i = 0; i < MIN_NO_OF_POINTS_TO_DETECT_JUMP; i++)
            { // fill remaining points of line with initial values
                //x.Add(*(i_I[i]));
                x.Add(I_start[i_I[i]]);
                //y.Add(*(i_Pr[i]));
                y.Add(Pr_start[i_Pr[i]]);
            }

            int jump_found = 0;
            point_count = 0;

            while (point_count <= (line_length - MIN_NO_OF_POINTS_TO_DETECT_JUMP + 1))
            {
                // iterate through each window on a line

                if (jump_found <= 0) // check primary jump criterion
                {
                    // check if zero has been crossed between adjacent d2Pr_dIp2 values

                    double d2Pr_dI2_A = VectorAnalysis.doubleDerivative(
                        I_start[i_I[0]], I_start[i_I[1]], I_start[i_I[2]],
                        Pr_start[i_Pr[0]], Pr_start[i_Pr[1]], Pr_start[i_Pr[2]]);
                    double d2Pr_dI2_B = VectorAnalysis.doubleDerivative(
                        I_start[i_I[1]], I_start[i_I[2]], I_start[i_I[3]],
                        Pr_start[i_Pr[1]], Pr_start[i_Pr[2]], Pr_start[i_Pr[3]]);

                    if ((d2Pr_dI2_A > min_d2Pr_dI2_zero_cross_jump_size
                        && d2Pr_dI2_B < -min_d2Pr_dI2_zero_cross_jump_size)
                        || (d2Pr_dI2_B > min_d2Pr_dI2_zero_cross_jump_size
                        && d2Pr_dI2_A < -min_d2Pr_dI2_zero_cross_jump_size))
                    {
                        // boundary jump detected!!
                        // add each detected boundary to boundary_multimap
                        jumps.Insert(
                            new KeyValuePair<int, int>(
                                start_point_index + (point_count - 1) * index_increment,
                                start_point_index + point_count * index_increment)
                            );


                        double d2Pr_dI2_largest = d2Pr_dI2_A > d2Pr_dI2_B ? d2Pr_dI2_A : d2Pr_dI2_B;

                        //d2Pr_dI2_at_jumps.insert(
                        //    std::map<int, double>::value_type(
                        //        start_point_index + (point_count - 1)*index_increment,
                        //        d2Pr_dI2_largest )
                        //    );
                        d2Pr_dI2_at_jumps.Add(start_point_index + (point_count - 1) * index_increment, d2Pr_dI2_largest);


                        jump_found = MIN_NO_OF_POINTS_TO_DETECT_JUMP;
                    }
                }

                if (jump_found <= 0 && point_count > 0) // check for second jump criteria
                {
                    // A case we're looking for is where there was not a clean jump
                    // in optical power. Sometimes a power reading between two modes
                    // is recorded that doesn't seem to belong to either mode,
                    // so we wish to ignore such points.


                    double d2Pr_dI2_A = VectorAnalysis.doubleDerivative(
                        oldest_I, I_start[i_I[0]], I_start[i_I[2]],
                        oldest_Pr, Pr_start[i_Pr[0]], Pr_start[i_Pr[2]]);
                    double d2Pr_dI2_B = VectorAnalysis.doubleDerivative(
                        I_start[i_I[0]], I_start[i_I[2]], I_start[i_I[3]],
                        Pr_start[i_Pr[0]], Pr_start[i_Pr[2]], Pr_start[i_Pr[3]]);

                    if ((d2Pr_dI2_A > min_d2Pr_dI2_zero_cross_jump_size
                        && d2Pr_dI2_B < -min_d2Pr_dI2_zero_cross_jump_size)
                        || (d2Pr_dI2_B > min_d2Pr_dI2_zero_cross_jump_size
                        && d2Pr_dI2_A < -min_d2Pr_dI2_zero_cross_jump_size))
                    {
                        // boundary jump detected!!
                        // add each detected boundary to boundary_multimap
                        jumps.Insert(
                            new KeyValuePair<int, int>(
                                start_point_index + (point_count - 2) * index_increment,
                                start_point_index + point_count * index_increment)
                            );

                        d2Pr_dI2_at_jumps.Add(start_point_index + (point_count - 2) * index_increment, 0);

                        jump_found = MIN_NO_OF_POINTS_TO_DETECT_JUMP;
                    }
                }

                double slope = 0;
                oldest_slope = 0;
                double sum_of_squared_distance = 0;
                oldest_sum_of_squared_distance = 0;
                if (max_sum_of_squared_distances_from_Pr_I_line > 0)
                {
                    // Find the slope of a line fitted to the windowed data
                    double intercept = 0;
                    VectorAnalysis.linearFit(x, y, out slope, out intercept);

                    // calculate sum_of_squared_distance of points' distances from the fitted line
                    List<double> distances = new List<double>();
                    VectorAnalysis.distancesFromPointsToLine(x, y, slope, intercept, distances);
                    //for( std::vector<double>::iterator i_d = distances.begin();i_d != distances.end(); i_d++ )
                    for (int i_d = 0; i_d != distances.Count; i_d++)
                    { // calculate sum of squared distances
                        sum_of_squared_distance += distances[i_d] * distances[i_d];
                    }

                    // Convert slope from y/x to angle in degrees
                    slope = (360 / (2 * Defaults.M_PI)) * Math.Atan(slope);

                    // shift old slopes along
                    oldest_slope = slopes[0];
                    for (int i = 0; i < FITTED_LINE_LENGHT - 1; i++)
                    {
                        slopes[i] = slopes[i + 1];
                    }
                    slopes[FITTED_LINE_LENGHT - 1] = slope;

                    // shift old sum_of_squared_distances along
                    oldest_sum_of_squared_distance = sum_of_squared_distances[0];
                    for (int i = 0; i < FITTED_LINE_LENGHT - 1; i++)
                    {
                        sum_of_squared_distances[i] = sum_of_squared_distances[i + 1];
                    }
                    sum_of_squared_distances[FITTED_LINE_LENGHT - 1] = sum_of_squared_distance;
                }

                if (jump_found <= MIN_NO_OF_POINTS_TO_DETECT_JUMP - 2 * FITTED_LINE_LENGHT
                    && point_count >= 2 * FITTED_LINE_LENGHT - 1)
                { // check for third jump criteria
                    // In this case we wish to identify stark changes in slope.
                    // Sometimes a mode boundary results in no change in optical power
                    // but the direction optical power is heading can change.
                    // If we have two lines fitted to data with the current point in common,
                    // a low sum_of_squared_distance on both lines indicates that no jump in power occured,
                    // then comparing the slope tells us if there was a rapid change in direction
                    if ((slope - oldest_slope > min_slope_change_of_Pr_I_lines
                        || slope - oldest_slope < -min_slope_change_of_Pr_I_lines)
                        && sum_of_squared_distance < max_sum_of_squared_distances_from_Pr_I_line
                        && oldest_sum_of_squared_distance < max_sum_of_squared_distances_from_Pr_I_line)
                    {
                        // boundary jump detected!!
                        // add each detected boundary to boundary_multimap
                        //printf("\n <%d>, slopes <%g,%g>, sumsqdist <%g,%g>",
                        //    start_point_index + (point_count - FITTED_LINE_LENGHT)*index_increment,
                        //    oldest_slope, slope,
                        //    oldest_sum_of_squared_distance, sum_of_squared_distance );
                        //printf("\n x[%d] = [%g,%g,%g,%g,%g,%g,%g,%g]", x.size(), x[0], x[1], x[2], x[3], x[4], x[5], x[6], x[7]); 
                        //printf("\n y[%d] = [%g,%g,%g,%g,%g,%g,%g,%g]", y.size(), y[0], y[1], y[2], y[3], y[4], y[5], y[6], y[7]); 

                        jumps.Insert(
                            new KeyValuePair<int, int>(
                                start_point_index + (point_count - FITTED_LINE_LENGHT) * index_increment,
                                start_point_index + (point_count - FITTED_LINE_LENGHT) * index_increment)
                            );

                        d2Pr_dI2_at_jumps.Add(start_point_index + (point_count - 2) * index_increment, 0);


                        jump_found = FITTED_LINE_LENGHT;
                    }
                }

                // ensure only one jump is found within a region
                jump_found--;

                // shift the window along by one point
                x.RemoveAt(0);
                y.RemoveAt(0);
                oldest_I = I_start[i_I[0]];
                oldest_Pr = Pr_start[i_Pr[0]];
                for (int i = 0; i < MIN_NO_OF_POINTS_TO_DETECT_JUMP - 1; i++)
                {
                    i_I[i] = i_I[i + 1];
                    i_Pr[i] = i_Pr[i + 1];
                }
                for (int j = 0; j < index_increment; j++)
                { // find next new point
                    i_I[MIN_NO_OF_POINTS_TO_DETECT_JUMP - 1]++;
                    i_Pr[MIN_NO_OF_POINTS_TO_DETECT_JUMP - 1]++;
                }
                x.Add(I_start[i_I[MIN_NO_OF_POINTS_TO_DETECT_JUMP - 1]]);
                y.Add(Pr_start[i_Pr[MIN_NO_OF_POINTS_TO_DETECT_JUMP - 1]]);

                point_count++;
            }

            if (boundary_count <= I_ramp_boundaries_max)
            {
                //boundary_multimap_typedef::iterator i_jumps = jumps.begin();
                int i_jumps = 0;
                while (i_jumps != jumps.Count)
                {
                    //boundary_multimap_typedef::iterator i_next = i_jumps;
                    int i_next = i_jumps;
                    i_next++;

                    if (i_next != jumps.Count)
                    {
                        int separation = 0;
                        if (jumps[i_next].Key > jumps[i_jumps].Key)
                            separation = jumps[i_next].Key - jumps[i_jumps].Key;
                        else
                            separation = jumps[i_jumps].Key - jumps[i_next].Key;

                        if (separation < I_ramp_boundaries_min_separation)
                        {
                            // jumps are too close, remove one
                            //double jump_val = (*d2Pr_dI2_at_jumps.find(jumps[i_jumps].Key)).second;
                            double jump_val = d2Pr_dI2_at_jumps[jumps[i_jumps].Key];
                            //double next_val = (*d2Pr_dI2_at_jumps.find(jumps[i_next].Key)).second;
                            double next_val = d2Pr_dI2_at_jumps[jumps[i_next].Key];
                            if (next_val > jump_val)
                            {
                                jumps.RemoveAt(i_jumps);
                                i_jumps = 0;
                            }
                            else
                            {
                                jumps.RemoveAt(i_next);
                                i_jumps = 0;
                            }
                        }
                        else
                        {
                            i_jumps++;
                        }
                    }
                    else
                    {
                        break;
                    }
                }

                i_jumps = 0;
                while (i_jumps != jumps.Count)
                {
                    boundary_map.Insert(jumps[i_jumps]);
                    i_jumps++;
                }
            }
            else
            { // too many boundaries detected => something is wrong
            }

        }


        public rcode findZDPRCBoundaries()
        {
            CGR_LOG.Write("CDSDBRSuperMode::findZDPRCBoundaries() entered", LoggingLevels.DIAGNOSTIC_CGR_LOG);
            rcode rval = rcode.ok;
            int ramp_length;
            int steps_count;

            if (_Im_ramp) // middle line ramped
            {
                steps_count = _phase_current._currents.Count;
                ramp_length = _filtered_middle_line._points.Count;
            }
            else // phase ramped
            {
                steps_count = _filtered_middle_line._points.Count;
                ramp_length = _phase_current._currents.Count;
            }

            // first, calculated filtered hysteresis map
            // and find max hysteresis value
            double max_hysteresis = 0;
            int line_counter = 0;
            _map_hysteresis_power_ratio_median_filtered.clear();
            for (int y = 0; y < steps_count; y++)
            {
                for (int x = 0; x < ramp_length; x++)
                {
                    int fp = x + y * ramp_length;
                    //unsigned int rp = ((ramp_length-1)-x)+y*ramp_length;
                    double hysteresis = _map_forward_power_ratio_median_filtered._map[fp]
                                      - _map_reverse_power_ratio_median_filtered._map[fp];
                    _map_hysteresis_power_ratio_median_filtered._map.Add(hysteresis);
                    if (hysteresis > max_hysteresis) max_hysteresis = hysteresis;
                }
                line_counter++;
            }

            _map_hysteresis_power_ratio_median_filtered._cols = ramp_length;
            _map_hysteresis_power_ratio_median_filtered._rows = steps_count;
            if (DSDBR01.Instance._DSDBR01_SMBD_write_debug_files)
            {
                // Assume _abs_filepath is set from loading data from file
                // or from writing to file after gathering data from laser
                string abs_filepath = _map_forward_power_ratio_median_filtered._abs_filepath;
                abs_filepath = abs_filepath.Replace(Defaults.MEAS_TYPE_ID_FORWARDPOWERRATIO_CSTRING, Defaults.MEAS_TYPE_ID_HYSTERESISPOWERRATIO_CSTRING);

                // Write filtered map to file
                _map_hysteresis_power_ratio_median_filtered.writeToFile(abs_filepath, true);
            }

            // find the jump thresholds as percentages of the max value
            double jump_upper_threshold = DSDBR01.Instance._DSDBR01_SMBD_zprc_hysteresis_upper_threshold;
            double jump_lower_threshold = DSDBR01.Instance._DSDBR01_SMBD_zprc_hysteresis_lower_threshold;
            double max_rise_in_hysteresis = DSDBR01.Instance._DSDBR01_SMBD_zprc_max_hysteresis_rise;

            boundary_multimap_typedef hysteresis_forward_jumps = new boundary_multimap_typedef();
            //std::vector<boundary_multimap_typedef::iterator> forward_jumps;
            //std::vector<boundary_multimap_typedef::iterator> reverse_jumps;
            List<int> forward_jumps = new List<int>();
            List<int> reverse_jumps = new List<int>();

            // iterate through each line and identify jumps
            for (int y = 0; y < steps_count; y++)
            {
                // find existing forward jumps on the current phase line
                forward_jumps.Clear();
                //_map_forward_power_ratio_median_filtered._boundary_points
                for (int i_map1_jump = 0;
                     i_map1_jump < _map_forward_power_ratio_median_filtered._boundary_points.Count;
                     i_map1_jump++)
                {
                    if ((_map_forward_power_ratio_median_filtered._boundary_points[i_map1_jump].Value / ramp_length) == y)
                    { // found jump on current line
                        forward_jumps.Add(i_map1_jump);
                    }
                }

                // find existing reverse jumps on the current phase line
                reverse_jumps.Clear();//_map_reverse_power_ratio_median_filtered._boundary_points
                for (int i_map2_jump = 0;
                     i_map2_jump < _map_reverse_power_ratio_median_filtered._boundary_points.Count;
                     i_map2_jump++)
                {
                    if ((_map_reverse_power_ratio_median_filtered._boundary_points[i_map2_jump].Key / ramp_length) == y)
                    { // found jump on current line
                        reverse_jumps.Add(i_map2_jump);
                    }
                }

                // find hysteresis jumps on the current phase difference line
                hysteresis_forward_jumps.Clear();
                bool in_hysteresis_region = false;
                int counter = 0;
                int hysteresis_counter = 0;
                double lowest_hysteresis = 0;
                for (int x = 1; x < ramp_length; x++)
                {
                    double hysteresis = _map_hysteresis_power_ratio_median_filtered._map[x + y * ramp_length];

                    if (hysteresis > jump_upper_threshold)
                    { // must be in a hysteresis region if hysteresis is this high
                        in_hysteresis_region = true;
                        lowest_hysteresis = hysteresis;
                    }

                    if (in_hysteresis_region) hysteresis_counter++;
                    else hysteresis_counter = 0;

                    //if( hysteresis_counter > MAX_NO_OF_HYSTERESIS_POINTS )
                    if (hysteresis_counter > DSDBR01.Instance._DSDBR01_SMBD_zprc_max_hysteresis_points)
                    {
                        in_hysteresis_region = false;
                        hysteresis_counter = 0;
                    }

                    if (in_hysteresis_region && hysteresis < lowest_hysteresis)
                        lowest_hysteresis = hysteresis;

                    if (in_hysteresis_region
                     && hysteresis - lowest_hysteresis > max_rise_in_hysteresis)
                    { // jump up while hysteresis should be decreasing => exit region
                        in_hysteresis_region = false;
                        hysteresis_counter = 0;
                    }

                    if (hysteresis < jump_lower_threshold)
                    {
                        if (in_hysteresis_region)
                        { // the lower threshold has been crossed exiting a hysteresis region

                            if (x >= MIN_DISTANCE_FROM_EDGE_TO_ZPC
                             && x <= ramp_length - MIN_DISTANCE_FROM_EDGE_TO_ZPC
                             && counter <= 0)
                            {
                                // record a forward jump
                                hysteresis_forward_jumps.Insert(
                                    new KeyValuePair<int, int>(
                                        x - 1 + y * ramp_length,
                                        x + y * ramp_length)
                                    );

                                // ensure next jump found is far away
                                //counter = MIN_NO_OF_POINTS_TO_DETECT_ZPC_JUMP;
                                counter = DSDBR01.Instance._DSDBR01_SMBD_zprc_min_points_separation;
                            }
                        }
                        in_hysteresis_region = false;
                        hysteresis_counter = 0;
                    }

                    counter--;
                }

                String msg = "";
                // remove hysteresis jumps within a small range of known reverse jumps
                int i_reverse_jump = 0;//reverse_jumps
                for (i_reverse_jump = 0; i_reverse_jump < reverse_jumps.Count; i_reverse_jump++)
                {
                    //_map_reverse_power_ratio_median_filtered._boundary_points
                    int reverse_y = _map_reverse_power_ratio_median_filtered._boundary_points[reverse_jumps[i_reverse_jump]].Key / ramp_length;
                    int reverse_x = (ramp_length - 1) - (_map_reverse_power_ratio_median_filtered._boundary_points[reverse_jumps[i_reverse_jump]].Key - reverse_y * ramp_length);
                    int i_hysteresis_jump = 0;//hysteresis_forward_jumps
                    while (i_hysteresis_jump < hysteresis_forward_jumps.Count)
                    {
                        int hysteresis_y = (hysteresis_forward_jumps[i_hysteresis_jump].Value) / ramp_length;
                        int hysteresis_x = (hysteresis_forward_jumps[i_hysteresis_jump].Value) - hysteresis_y * ramp_length;

                        //msg.AppendFormat("\n reverse_jump = <%d,%d> , hysteresis_jump = <%d,%d>",
                        //    (*(*i_reverse_jump)).first, (*(*i_reverse_jump)).second,
                        //    (*i_hysteresis_jump).first, (*i_hysteresis_jump).second );

                        //if( abs(reverse_x - hysteresis_x) <= MIN_DISTANCE_FROM_ZPC_TO_REVERSE_JUMP )
                        if (Math.Abs(reverse_x - hysteresis_x) <= DSDBR01.Instance._DSDBR01_SMBD_zprc_max_points_to_reverse_jump)
                        {   // hysteresis and reverse jump too near
                            // => remove this hysteresis jump
                            //hysteresis_forward_jumps.erase(i_hysteresis_jump);
                            hysteresis_forward_jumps.RemoveAt(i_hysteresis_jump);
                            i_hysteresis_jump = 0;
                        }
                        else
                        {
                            i_hysteresis_jump++;
                        }
                    }
                }

                if ((hysteresis_forward_jumps.Count - forward_jumps.Count) <= DSDBR01.Instance._DSDBR01_SMBDFWD_I_ramp_boundaries_max)
                {
                    // hysteresis zero power crossings detected
                    if (forward_jumps.Count == 0)
                    {
                        // no existing forward jumps
                        // add zpcs before the next line
                        //boundary_multimap_typedef::iterator i_map1_jump;
                        int i_map1_jump;
                        //   for( i_map1_jump = _map_forward_power_ratio_median_filtered._boundary_points.begin();
                        //i_map1_jump != _map_forward_power_ratio_median_filtered._boundary_points.end();
                        //i_map1_jump++ )
                        for (i_map1_jump = 0;
                            i_map1_jump < _map_forward_power_ratio_median_filtered._boundary_points.Count;
                            i_map1_jump++)
                        {
                            if ((_map_forward_power_ratio_median_filtered._boundary_points[i_map1_jump].Value) / ramp_length > y)
                            { // found jump on line above current line
                                // insert hysteresis jump before it

                                int msg_count = 0;
                                msg = "CDSDBRSuperMode::findZDPRCBoundaries()";
                                msg += " adding jumps on line with no jumps";

                                //for( boundary_multimap_typedef::iterator i_hysteresis_jump
                                //        = hysteresis_forward_jumps.begin();
                                //    i_hysteresis_jump != hysteresis_forward_jumps.end();
                                //    i_hysteresis_jump++ )
                                for (int i_hysteresis_jump = 0;
                                    i_hysteresis_jump < hysteresis_forward_jumps.Count;
                                    i_hysteresis_jump++)
                                {
                                    _map_forward_power_ratio_median_filtered._boundary_points.Insert(i_map1_jump, hysteresis_forward_jumps[i_hysteresis_jump]);

                                    msg_count++;
                                    msg += string.Format("\n inserting <{0:d},{1:d}> before <{2:d},{3:d}>",
                                        hysteresis_forward_jumps[i_hysteresis_jump].Key, hysteresis_forward_jumps[i_hysteresis_jump].Value,
                                        _map_forward_power_ratio_median_filtered._boundary_points[i_map1_jump].Key, _map_forward_power_ratio_median_filtered._boundary_points[i_map1_jump].Value);
                                }

                                if (msg_count > 0) CGR_LOG.Write(msg, LoggingLevels.INFO_CGR_LOG);

                                break;
                            }
                        }
                        if (i_map1_jump == _map_forward_power_ratio_median_filtered._boundary_points.Count)
                        {
                            // insert hysteresis jump at the end
                            int msg_count = 0;
                            msg = "CDSDBRSuperMode::findZDPRCBoundaries()";
                            msg += " adding jumps on line with no jumps";

                            //for( boundary_multimap_typedef::iterator i_hysteresis_jump
                            //    = hysteresis_forward_jumps.begin();
                            //    i_hysteresis_jump != hysteresis_forward_jumps.end();
                            //i_hysteresis_jump++ )
                            for (int i_hysteresis_jump = 0;
                                i_hysteresis_jump < hysteresis_forward_jumps.Count;
                                i_hysteresis_jump++)
                            {
                                _map_forward_power_ratio_median_filtered._boundary_points.Insert(
                                    _map_forward_power_ratio_median_filtered._boundary_points.Count,
                                    hysteresis_forward_jumps[i_hysteresis_jump]);

                                msg_count++;
                                msg += string.Format("\n inserting <{0:d},{1:d}> at the end",
                                    hysteresis_forward_jumps[i_hysteresis_jump].Key, hysteresis_forward_jumps[i_hysteresis_jump].Value);

                            }

                            if (msg_count > 0) CGR_LOG.Write(msg, LoggingLevels.INFO_CGR_LOG);
                        }

                    }
                    else
                    {
                        // At least one forward jump already exists on the current line
                        // match existing forward jumps to hysteresis jumps
                        // and insert the remaining hysteresis jump (zpcs) into
                        // the map of all forward jumps

                        // first, remove hysteresis jumps within a range of known forward jumps
                        //std::vector<boundary_multimap_typedef::iterator>::iterator i_forward_jump;
                        int i_forward_jump;
                        //for( i_forward_jump = forward_jumps.begin();
                        //  i_forward_jump != forward_jumps.end(); i_forward_jump++ )
                        for (i_forward_jump = 0;
                            i_forward_jump < forward_jumps.Count; i_forward_jump++)
                        {
                            //int forward_y = ((*(*i_forward_jump)).second)/ramp_length;
                            //int forward_x = ((*(*i_forward_jump)).second) - forward_y*ramp_length;
                            int forward_y = _map_forward_power_ratio_median_filtered._boundary_points[forward_jumps[i_forward_jump]].Value / ramp_length;
                            int forward_x = _map_forward_power_ratio_median_filtered._boundary_points[forward_jumps[i_forward_jump]].Value - forward_y * ramp_length;

                            //boundary_multimap_typedef::iterator i_hysteresis_jump = hysteresis_forward_jumps.begin();
                            int i_hysteresis_jump = 0;
                            while (i_hysteresis_jump < hysteresis_forward_jumps.Count)
                            {
                                int hysteresis_y = hysteresis_forward_jumps[i_hysteresis_jump].Value / ramp_length;
                                int hysteresis_x = hysteresis_forward_jumps[i_hysteresis_jump].Value - hysteresis_y * ramp_length;

                                if (Math.Abs(forward_x - hysteresis_x) < DSDBR01.Instance._DSDBR01_SMBD_zprc_min_points_separation)
                                { // hysteresis and forward jump too near
                                    // => remove this hysteresis jump
                                    //hysteresis_forward_jumps.erase(i_hysteresis_jump);
                                    hysteresis_forward_jumps.RemoveAt(i_hysteresis_jump);
                                    //i_hysteresis_jump = hysteresis_forward_jumps.begin();
                                    i_hysteresis_jump = 0;
                                }
                                else
                                {
                                    i_hysteresis_jump++;
                                }
                            }
                        }

                        // At this stage, if forward and hysteresis jumps are matched correctly
                        // then there could be some hysteresis jumps left unmatched if far
                        // enough away from all known forward jumps on the current line
                        if (hysteresis_forward_jumps.Count <= DSDBR01.Instance._DSDBR01_SMBDFWD_I_ramp_boundaries_max)
                        {   // there are hysteresis jumps unaccounted for, assumed them to be
                            // zero-power crossings and add them as new
                            // jumps in the correct place in the map of all forward jumps

                            int msg_count = 0;
                            msg = "CDSDBRSuperMode::findZDPRCBoundaries()";
                            msg += " adding jump on line with existing jumps";

                            //for( boundary_multimap_typedef::iterator i_hysteresis_jump= hysteresis_forward_jumps.begin();
                            //    i_hysteresis_jump != hysteresis_forward_jumps.end();i_hysteresis_jump++ )
                            for (int i_hysteresis_jump = 0;
                                i_hysteresis_jump < hysteresis_forward_jumps.Count; i_hysteresis_jump++)
                            {
                                //boundary_multimap_typedef::iterator i_map1_jump;
                                int i_map1_jump;
                                int previous_jump = 0;
                                //for( i_map1_jump = *(forward_jumps.begin());
                                //    i_map1_jump != _map_forward_power_ratio_median_filtered._boundary_points.end();
                                //    i_map1_jump++ )
                                for (i_map1_jump = forward_jumps[0];
                                    i_map1_jump < _map_forward_power_ratio_median_filtered._boundary_points.Count;
                                    i_map1_jump++)
                                {
                                    if (_map_forward_power_ratio_median_filtered._boundary_points[i_map1_jump].Value > hysteresis_forward_jumps[i_hysteresis_jump].Value
                                        && previous_jump < hysteresis_forward_jumps[i_hysteresis_jump].Value)
                                    { // found location to put new jump

                                        msg_count++;
                                        msg += string.Format("\n inserting <{0:d},{1:d}> before <{2:d},{3:d}>",
                                            hysteresis_forward_jumps[i_hysteresis_jump].Key, hysteresis_forward_jumps[i_hysteresis_jump].Value,
                                            _map_forward_power_ratio_median_filtered._boundary_points[i_map1_jump].Key, _map_forward_power_ratio_median_filtered._boundary_points[i_map1_jump].Value);

                                        _map_forward_power_ratio_median_filtered._boundary_points.Insert(
                                            i_map1_jump, hysteresis_forward_jumps[i_hysteresis_jump]);
                                        break;
                                    }
                                    previous_jump = _map_forward_power_ratio_median_filtered._boundary_points[i_map1_jump].Value;
                                }
                                if (i_map1_jump == _map_forward_power_ratio_median_filtered._boundary_points.Count)
                                { // insert hysteresis jump at the end

                                    msg_count++;
                                    msg += string.Format("\n inserting <{0:d},{1:d}> at the end",
                                        hysteresis_forward_jumps[i_hysteresis_jump].Key, hysteresis_forward_jumps[i_hysteresis_jump].Value);

                                    _map_forward_power_ratio_median_filtered._boundary_points.Insert(
                                        _map_forward_power_ratio_median_filtered._boundary_points.Count, hysteresis_forward_jumps[i_hysteresis_jump]);
                                }
                            }
                            if (msg_count > 0) CGR_LOG.Write(msg, LoggingLevels.INFO_CGR_LOG);
                        }
                    }
                }
            }

            CGR_LOG.Write("CDSDBRSuperMode::findZDPRCBoundaries() exiting", LoggingLevels.DIAGNOSTIC_CGR_LOG);

            return rval;
        }

        public rcode linkJumps()
        {
            CGR_LOG.Write("CDSDBRSuperMode::linkJumps() entered", LoggingLevels.DIAGNOSTIC_CGR_LOG);
            CDSDBRSuperMode.rcode rval = rcode.ok;

            linkJumpsByIndex(_map_forward_power_ratio_median_filtered, true);
            

            int mbl_count = -1;
            //for (boundary_multimap_typedef::iterator i_f = _map_forward_power_ratio_median_filtered._linked_boundary_points.begin();
            //    i_f != _map_forward_power_ratio_median_filtered._linked_boundary_points.end(); i_f++)
            for (int i_f = 0; i_f < _map_forward_power_ratio_median_filtered._linked_boundary_points.Count; i_f++)
            { // find the number of lines identified
                if ((int)(_map_forward_power_ratio_median_filtered._linked_boundary_points[i_f].Value) > mbl_count)
                    mbl_count = _map_forward_power_ratio_median_filtered._linked_boundary_points[i_f].Value;
            }
            if (mbl_count < MIN_NUMBER_OF_LINES_PER_MAP)
            {
                rval = rcode.min_number_of_lines_not_reached;
                CGR_LOG.Write("CDSDBRSuperMode::linkJumps() min_number_of_lines_not_reached", LoggingLevels.ERROR_CGR_LOG);
            }
            else if (mbl_count > MAX_NUMBER_OF_LINES_PER_MAP)
            {
                rval = rcode.max_number_of_lines_exceeded;
                CGR_LOG.Write("CDSDBRSuperMode::linkJumps() max_number_of_lines_exceeded", LoggingLevels.ERROR_CGR_LOG);
            }
            else
            {
                string log_msg = "CDSDBRSuperMode::linkJumps() ";
                log_msg += string.Format("found {0:d} mode boundary lines on forward map", mbl_count);
                CGR_LOG.Write(log_msg, LoggingLevels.INFO_CGR_LOG);
            }

            if (DSDBR01.Instance._DSDBR01_SMBD_write_debug_files)
            {
                if (rval == rcode.ok)
                {
                    // Assume _abs_filepath is set from loading data from file
                    // or from writing to file after gathering data from laser
                    string abs_filepath = _map_forward_power_ratio_median_filtered._abs_filepath;
                    abs_filepath = abs_filepath.Replace(Defaults.MATRIX_, Defaults.LINKED_JUMPS_);

                    // Write jumps detected in filtered map to file
                    _map_forward_power_ratio_median_filtered.writeLinkedJumpsToFile(abs_filepath);
                }
            }

            if (rval == rcode.ok)
            {
                linkJumpsByIndex(_map_reverse_power_ratio_median_filtered, false);

                mbl_count = -1;
                //for( boundary_multimap_typedef::iterator i_f = _map_reverse_power_ratio_median_filtered._linked_boundary_points.begin();
                //    i_f != _map_reverse_power_ratio_median_filtered._linked_boundary_points.end() ; i_f++ )
                for (int i_f = 0;
                    i_f < _map_reverse_power_ratio_median_filtered._linked_boundary_points.Count; i_f++)
                { // find the number of lines identified
                    if ((int)_map_reverse_power_ratio_median_filtered._linked_boundary_points[i_f].Value > mbl_count)
                        mbl_count = _map_reverse_power_ratio_median_filtered._linked_boundary_points[i_f].Value;
                }
                if (mbl_count < MIN_NUMBER_OF_LINES_PER_MAP)
                {
                    rval = rcode.min_number_of_lines_not_reached;
                    CGR_LOG.Write("CDSDBRSuperMode::linkJumps() min_number_of_lines_not_reached", LoggingLevels.ERROR_CGR_LOG);
                }
                else if (mbl_count > MAX_NUMBER_OF_LINES_PER_MAP)
                {
                    rval = rcode.max_number_of_lines_exceeded;
                    CGR_LOG.Write("CDSDBRSuperMode::linkJumps() max_number_of_lines_exceeded", LoggingLevels.ERROR_CGR_LOG);
                }
                else
                {
                    string log_msg = "CDSDBRSuperMode::linkJumps() ";
                    log_msg += string.Format("found {0} mode boundary lines on reverse map", mbl_count);
                    CGR_LOG.Write(log_msg, LoggingLevels.INFO_CGR_LOG);
                }
            }

            if (DSDBR01.Instance._DSDBR01_SMBD_write_debug_files)
            {
                if (rval == rcode.ok)
                {
                    // Assume _abs_filepath is set from loading data from file
                    // or from writing to file after gathering data from laser
                    string abs_filepath = _map_reverse_power_ratio_median_filtered._abs_filepath;
                    abs_filepath = abs_filepath.Replace(Defaults.MATRIX_, Defaults.LINKED_JUMPS_);

                    // Write jumps detected in filtered map to file
                    _map_reverse_power_ratio_median_filtered.writeLinkedJumpsToFile(abs_filepath);
                }
            }

            CGR_LOG.Write("CDSDBRSuperMode::linkJumps() exiting", LoggingLevels.DIAGNOSTIC_CGR_LOG);
            return rval;
        }

        public rcode insertExtrapolatedMapBorderPoints()
        {
            rcode rval = rcode.ok;
            CGR_LOG.Write("CDSDBRSuperMode::insertExtrapolatedMapBorderPoints() entered", LoggingLevels.DIAGNOSTIC_CGR_LOG);

            insertExtrapolatedMapBorderPointsInMap(
                _map_forward_power_ratio_median_filtered._boundary_points,
                _map_forward_power_ratio_median_filtered._linked_boundary_points);

            insertExtrapolatedMapBorderPointsInMap(
                _map_reverse_power_ratio_median_filtered._boundary_points,
                _map_reverse_power_ratio_median_filtered._linked_boundary_points);

            if (DSDBR01.Instance._DSDBR01_SMBD_write_debug_files)
            {
                if (rval == rcode.ok)
                {
                    // Assume _abs_filepath is set from loading data from file
                    // or from writing to file after gathering data from laser
                    string abs_filepath = _map_forward_power_ratio_median_filtered._abs_filepath;
                    abs_filepath = abs_filepath.Replace(Defaults.MATRIX_, "JUMPS_LE_");

                    // Write jumps detected in filtered map to file
                    _map_forward_power_ratio_median_filtered.writeLinkedJumpsToFile(abs_filepath);

                    // Assume _abs_filepath is set from loading data from file
                    // or from writing to file after gathering data from laser
                    abs_filepath = _map_reverse_power_ratio_median_filtered._abs_filepath;
                    abs_filepath = abs_filepath.Replace(Defaults.MATRIX_, "JUMPS_LE_");

                    // Write jumps detected in filtered map to file
                    _map_reverse_power_ratio_median_filtered.writeLinkedJumpsToFile(abs_filepath);
                }
            }

            CGR_LOG.Write("CDSDBRSuperMode::insertExtrapolatedMapBorderPoints() exiting", LoggingLevels.DIAGNOSTIC_CGR_LOG);
            return rval;
        }

        public rcode insertExtrapolatedMapBorderPointsInMap(
            boundary_multimap_typedef boundary_points,
            boundary_multimap_typedef linked_boundary_points)
        {
            rcode rval = rcode.ok;
            CGR_LOG.Write("CDSDBRSuperMode::insertExtrapolatedMapBorderPointsInMap() entered", LoggingLevels.DIAGNOSTIC_CGR_LOG);

            int top_line_num = -1;
            //for (boundary_multimap_typedef::iterator i_f = linked_boundary_points.begin();
            //    i_f != linked_boundary_points.end(); i_f++)
            for (int i_f = 0; i_f < linked_boundary_points.Count; i_f++)
            { // find the number of lines identified
                if ((int)linked_boundary_points[i_f].Value > top_line_num) top_line_num = (int)linked_boundary_points[i_f].Value;
            }

            List<double> x_position = new List<double>();
            List<double> y_position = new List<double>();
            int ramp_length = _phase_current._currents.Count;
            int steps_count = _filtered_middle_line._points.Count;
            for (int line_number = 0; line_number <= top_line_num; line_number++)
            {
                x_position.Clear();
                y_position.Clear();

                //boundary_multimap_typedef::iterator i_end = linked_boundary_points.end();
                int i_end = linked_boundary_points.Count;
                //boundary_multimap_typedef::iterator i_first_in_line = i_end;
                int i_first_in_line = i_end;
                //boundary_multimap_typedef::iterator i_last_in_line = i_end;
                int i_last_in_line = i_end;

                //for( boundary_multimap_typedef::iterator i_f = linked_boundary_points.begin();
                //    i_f != i_end ; i_f++ )
                for (int i_f = 0; i_f != i_end; i_f++)
                { // find the number of lines identified
                    if ((int)linked_boundary_points[i_f].Value == line_number)
                    {
                        if (i_first_in_line == i_end)
                        {
                            i_first_in_line = i_f;
                        }
                        i_last_in_line = i_f;

                        int y_pos = (int)(linked_boundary_points[i_f].Key / ramp_length);
                        int x_pos = (int)linked_boundary_points[i_f].Key - y_pos * ramp_length;

                        x_position.Add(x_pos);
                        y_position.Add(y_pos);

                    }
                }
                // At this point, x_position and y_position contain the coordinates
                // for point on one line, defined by line_number

                if (x_position.Count > 3)
                {
                    double slope;
                    double intercept;
                    VectorAnalysis.linearFit(x_position,y_position,out slope,out intercept);
                    
                    //y = slope*x+intercept;
                    // calculated extrapolated point to the left of a line
                    double y_left_point = intercept;
                    double x_left_point = 0;

                    //if( y_left_point < 0 )
                    //{
                    //	y_left_point = 0;
                    //	x_left_point = -intercept/slope;
                    //}

                    if (y_left_point >= 0)
                    {
                        // insert into boundary points before first found in line
                        //i_first_in_line;
                        int indexnum = (int)y_left_point * (int)ramp_length + (int)x_left_point;
                        boundary_points.Insert(boundary_points.Count,
                            new KeyValuePair<int, int>(indexnum, indexnum));
                        linked_boundary_points.Insert(i_first_in_line,
                            new KeyValuePair<int, int>(indexnum, (int)line_number));
                    }

                    // calculated extrapolated point to the right of a line
                    double x_right_point = ramp_length - 1;
                    double y_right_point = slope * (ramp_length - 1) + intercept;
                    //if( y_right_point > (steps_count-1) )
                    //{
                    //	y_right_point = steps_count-1;
                    //	x_right_point = ((steps_count-1)-intercept)/slope;
                    //}

                    if (y_right_point <= (steps_count - 1))
                    {
                        // insert into boundary points after last found in line
                        int indexnum = (int)y_right_point * (int)ramp_length + (int)x_right_point;

                        boundary_points.Insert(boundary_points.Count,
                            new KeyValuePair<int, int>(indexnum, indexnum));

                        i_last_in_line++;
                        linked_boundary_points.Insert(i_last_in_line,
                            new KeyValuePair<int, int>(indexnum,line_number));
                    }
                }
            }

            CGR_LOG.Write("CDSDBRSuperMode::insertExtrapolatedMapBorderPointsInMap() exiting", LoggingLevels.DIAGNOSTIC_CGR_LOG);
            return rval;
        }

        /// <summary>
        /// links boundary jumps marked <point_before,point_after> which are 
        /// nearest and mark them <point_before,boundary_line_number>
        /// using index method
        /// </summary>
        /// <param name="map_in"></param>
        /// <param name="forward_map"></param>
        public void linkJumpsByIndex(CLaserModeMap map_in, bool forward_map)
        // forward map (true) or reverse map (false)
        {
            //int ramp_length, // length of each ramp within the optical power map
            //int steps_count, // number of steps within the optical power map
            //boundary_multimap_typedef &boundary_jumps, // jumps detected within the optical power map
            //boundary_multimap_typedef &map_in._linked_boundary_points, // returns containing linked jumps
            //bool forward

            int linking_max_distance_between_linked_jumps;
            double linking_min_sharpness;
            double linking_max_sharpness;
            int linking_max_gap_between_double_lines;
            int linking_min_gap_between_double_lines;
            int linking_near_bottom_row;
            double linking_sharpness_near_bottom_row;
            int linking_max_distance_near_bottom_row;
            int linking_max_distance_to_single_unlinked_jump;
            int linking_max_y_distance_to_link;
            double linking_sharpness_ramp_split;

            double min_slope_change_of_Pr_I_lines = 0;

            if (forward_map)
            {
                linking_max_distance_between_linked_jumps = DSDBR01.Instance._DSDBR01_SMBDFWD_linking_max_distance_between_linked_jumps;
                linking_min_sharpness = DSDBR01.Instance._DSDBR01_SMBDFWD_linking_min_sharpness;
                linking_max_sharpness = DSDBR01.Instance._DSDBR01_SMBDFWD_linking_max_sharpness;
                linking_max_gap_between_double_lines = DSDBR01.Instance._DSDBR01_SMBDFWD_linking_max_gap_between_double_lines;
                linking_min_gap_between_double_lines = DSDBR01.Instance._DSDBR01_SMBDFWD_linking_min_gap_between_double_lines;
                linking_near_bottom_row = DSDBR01.Instance._DSDBR01_SMBDFWD_linking_near_bottom_row;
                linking_sharpness_near_bottom_row = DSDBR01.Instance._DSDBR01_SMBDFWD_linking_sharpness_near_bottom_row;
                linking_max_distance_near_bottom_row = DSDBR01.Instance._DSDBR01_SMBDFWD_linking_max_distance_near_bottom_row;
                linking_max_distance_to_single_unlinked_jump = DSDBR01.Instance._DSDBR01_SMBDFWD_linking_max_distance_to_single_unlinked_jump;
                linking_max_y_distance_to_link = DSDBR01.Instance._DSDBR01_SMBDFWD_linking_max_y_distance_to_link;
                linking_sharpness_ramp_split = DSDBR01.Instance._DSDBR01_SMBDFWD_linking_sharpness_ramp_split;
            }
            else // reverse map
            {
                linking_max_distance_between_linked_jumps = DSDBR01.Instance._DSDBR01_SMBDREV_linking_max_distance_between_linked_jumps;
                linking_min_sharpness = DSDBR01.Instance._DSDBR01_SMBDREV_linking_min_sharpness;
                linking_max_sharpness = DSDBR01.Instance._DSDBR01_SMBDREV_linking_max_sharpness;
                linking_max_gap_between_double_lines = DSDBR01.Instance._DSDBR01_SMBDREV_linking_max_gap_between_double_lines;
                linking_min_gap_between_double_lines = DSDBR01.Instance._DSDBR01_SMBDREV_linking_min_gap_between_double_lines;
                linking_near_bottom_row = DSDBR01.Instance._DSDBR01_SMBDREV_linking_near_bottom_row;
                linking_sharpness_near_bottom_row = DSDBR01.Instance._DSDBR01_SMBDREV_linking_sharpness_near_bottom_row;
                linking_max_distance_near_bottom_row = DSDBR01.Instance._DSDBR01_SMBDREV_linking_max_distance_near_bottom_row;
                linking_max_distance_to_single_unlinked_jump = DSDBR01.Instance._DSDBR01_SMBDREV_linking_max_distance_to_single_unlinked_jump;
                linking_max_y_distance_to_link = DSDBR01.Instance._DSDBR01_SMBDREV_linking_max_y_distance_to_link;
                linking_sharpness_ramp_split = DSDBR01.Instance._DSDBR01_SMBDREV_linking_sharpness_ramp_split;
            }

            bool forward = true;
            int ramp_length;
            int steps_count;
            if (_Im_ramp) // middle line ramped
            {
                steps_count = (int)(_phase_current._currents.Count);
                ramp_length = (int)(_filtered_middle_line._points.Count);
            }
            else // phase ramped
            {
                steps_count = (int)(_filtered_middle_line._points.Count);
                ramp_length = (int)(_phase_current._currents.Count);
            }

            CGR_LOG.Write("CDSDBRSuperMode::linkJumpsByIndex() entered", LoggingLevels.DIAGNOSTIC_CGR_LOG);

            // empty the map that will store linked jumps as <point_before,boundary_line_number>
            map_in._linked_boundary_points.Clear();

            // define a window size before each jump to search in 
            double d_window = 20;

            // keep a count of how many lines are identified
            int boundary_line_count = 0;

            // record the first jump as in line 0
            //boundary_multimap_typedef::iterator i_i = map_in._boundary_points.begin();
            int i_i = 0;
            //map_in._linked_boundary_points.insert( boundary_multimap_typedef::value_type( (*i_i).first, 0 ) );
            map_in._linked_boundary_points.Insert(new KeyValuePair<int, int>(map_in._boundary_points[i_i].Key, 0));
            boundary_line_count++;

            boundary_multimap_typedef unlinked_boundary_points = new boundary_multimap_typedef();

            i_i++; // move to second jump
            // iterate through each jump and find the distance to other jumps in it's vicinity
            for (; i_i < map_in._boundary_points.Count; i_i++)
            {// iterate through each jump

                // find jump at start of window
                //boundary_multimap_typedef::iterator i_h = i_i;
                int i_h = i_i;
                int count = 0;
                //while( i_h != map_in._boundary_points.begin() && (double)count < d_window )
                while (i_h != 0 && (double)count < d_window)
                {
                    count++;
                    i_h--;
                }

                // find jump at end of window
                // i_i is one past the last jump in the window
                //boundary_multimap_typedef::iterator i_j = i_i;
                int i_j = i_i;
                int y_i = boundaryPoint(map_in._boundary_points[i_i]) / ramp_length;
                int x_i = boundaryPoint(map_in._boundary_points[i_i]) - y_i * ramp_length;
                double nearest_distance = ramp_length * ramp_length; // initial value only
                //boundary_multimap_typedef::iterator nearest_jump = i_i; // initial value only
                int nearest_jump = i_i; // initial value only
                double sharpness;
                //double max_distance_to_link = MAX_DISTANCE_BETWEEN_LINKED_JUMPS*((double)ramp_length)/100;
                double max_distance_to_link = linking_max_distance_between_linked_jumps * ((double)ramp_length) / 100;
                double x_position;

                if (forward)
                    x_position = (double)x_i;
                else x_position = ((double)ramp_length - 1) - (double)x_i;

                if (x_position >= ((double)ramp_length - 1) * linking_sharpness_ramp_split)
                {
                    sharpness = linking_max_sharpness;
                }
                else
                {
                    sharpness = x_position / (linking_sharpness_ramp_split * ((double)(ramp_length - 1)))
                        * (linking_max_sharpness - linking_min_sharpness) + linking_min_sharpness;
                }

                if ((int)y_i < linking_near_bottom_row)
                {
                    sharpness = linking_sharpness_near_bottom_row;
                    max_distance_to_link = linking_max_distance_near_bottom_row * ((double)ramp_length) / 100;
                }

                for (int i_k = i_h; i_k != i_j; i_k++)
                { // iterate through jumps in the window

                    // calculate distance from i_i to each other jump in the window
                    if (i_k != i_i)
                    {
                        int y_k = boundaryPoint(map_in._boundary_points[i_k]) / ramp_length;
                        int x_k = boundaryPoint(map_in._boundary_points[i_k]) - y_k * ramp_length;
                        double distance_i_to_k;

                        if ((x_i > x_k && forward &&
                            ((double)x_i - (double)x_k) / sharpness > (double)y_i - (double)y_k)
                         || (x_i < x_k && !forward &&
                            ((double)x_k - (double)x_i) / sharpness > (double)y_i - (double)y_k))
                        { // inside sharp angle, calculate distance correctly
                            distance_i_to_k = Math.Sqrt((double)((y_i - y_k) * (y_i - y_k) * (y_i - y_k) + (x_i - x_k) * (x_i - x_k)));
                        }
                        else
                        {
                            // outside sharp angle, skew distance measurement to look bigger
                            distance_i_to_k = (y_i - y_k) * (y_i - y_k) * (y_i - y_k) + (x_i - x_k) * (x_i - x_k);
                            if ((x_i < x_k && forward) || (x_i > x_k && !forward))
                            { // looking down and right then make distance look even bigger
                                distance_i_to_k = 2 * distance_i_to_k;
                            }
                        }


                        //if( y_k != y_i && abs((int)(y_k)-(int)(y_i)) <= MAX_Y_DISTANCE_TO_LINK )
                        if (y_k != y_i && Math.Abs((int)(y_k) - (int)(y_i)) <= linking_max_y_distance_to_link)
                        { // only consider points on different horizontal lines

                            ////printf("\n i_i = <%d,%d>, i_k = <%d,%d>",(*i_i).first,(*i_i).second,(*i_k).first,(*i_k).second);
                            ////printf(", dist = %g", distance_i_to_k );

                            //msg = "";
                            //msg.AppendFormat("i_i = <%d,%d>, i_k = <%d,%d>, dist = %g",(*i_i).first,(*i_i).second,(*i_k).first,(*i_k).second, distance_i_to_k);
                            //CGR_LOG(msg.GetBuffer(),INFO_LEVEL)


                            if (distance_i_to_k < nearest_distance &&
                              (distance_i_to_k < max_distance_to_link
                                // || change_in_Po_i_to_k < (change_in_Po_across_jump / 10)
                              ))
                            {
                                nearest_distance = distance_i_to_k;
                                nearest_jump = i_k;
                            }

                        }
                    }
                }

                if (nearest_jump != i_i)
                { // a jump has been found
                    int line_number;

                    // find current jump in linked jumps map
                    //boundary_multimap_typedef::iterator i_f1 = map_in._linked_boundary_points.find( (*i_i).first );
                    int i_f1 = map_in._linked_boundary_points.Find(map_in._boundary_points[i_i].Key);

                    // find nearest jump in linked jumps map
                    //boundary_multimap_typedef::iterator i_f2 =
                    //    map_in._linked_boundary_points.find( (*nearest_jump).first );
                    int i_f2 = map_in._linked_boundary_points.Find(map_in._boundary_points[nearest_jump].Key);

                    //if( i_f1 == map_in._linked_boundary_points.end() && i_f2 == map_in._linked_boundary_points.end() )
                    if (i_f1 == map_in._linked_boundary_points.Count && i_f2 == map_in._linked_boundary_points.Count)
                    { // neither jump has been assigned a line number before now
                        // assign both to a new line
                        line_number = boundary_line_count;
                        boundary_line_count++;
                        //map_in._linked_boundary_points.insert( boundary_multimap_typedef::value_type(
                        //    (*i_i).first, line_number ) );
                        map_in._linked_boundary_points.Insert(new KeyValuePair<int, int>(
                            map_in._boundary_points[i_i].Key, line_number));

                        map_in._linked_boundary_points.Insert(new KeyValuePair<int, int>(
                            map_in._boundary_points[nearest_jump].Key, line_number));
                        //msg = "";
                        //msg.AppendFormat("insert <%d,%d> insert <%d,%d>", (*i_i).first, line_number, (*nearest_jump).first, line_number);
                        //CGR_LOG(msg.GetBuffer(),INFO_LEVEL)

                        //printf(" insert <%d,%d>", (*nearest_jump).first, line_number );

                        // check if jumps are in unlinked jumps map, is so => remove them
                        int i_uf1 = unlinked_boundary_points.Find(map_in._boundary_points[i_i].Key);
                        if (i_uf1 != unlinked_boundary_points.Count) unlinked_boundary_points.RemoveAt(i_uf1);
                        int i_uf2 = unlinked_boundary_points.Find(map_in._boundary_points[nearest_jump].Key);
                        if (i_uf2 != unlinked_boundary_points.Count) unlinked_boundary_points.RemoveAt(i_uf2);
                    }
                    else
                        if (i_f1 != map_in._linked_boundary_points.Count && i_f2 == map_in._linked_boundary_points.Count)
                        { // the current jump is already assigned a line number
                            // assign the nearest jump to the line the current jump is on
                            line_number = map_in._linked_boundary_points[i_f1].Value;
                            map_in._linked_boundary_points.Insert(new KeyValuePair<int, int>(
                                map_in._boundary_points[nearest_jump].Key, line_number));
                            //printf("\n                insert <%d,%d>", (*nearest_jump).first, line_number );

                            //msg = "";
                            //msg.AppendFormat("                insert <%d,%d>", (*nearest_jump).first, line_number);
                            //CGR_LOG(msg.GetBuffer(),INFO_LEVEL)

                            // check if jump is in unlinked jumps map, is so => remove it

                            int i_uf2 = unlinked_boundary_points.Find(map_in._boundary_points[nearest_jump].Key);
                            if (i_uf2 != unlinked_boundary_points.Count)
                                unlinked_boundary_points.RemoveAt(i_uf2);
                        }
                        else
                            if (i_f1 == map_in._linked_boundary_points.Count && i_f2 != map_in._linked_boundary_points.Count)
                            { // the nearest jump is already assigned a line number
                                // assign the current jump to the line the nearest jump is on
                                line_number = map_in._linked_boundary_points[i_f2].Value;
                                map_in._linked_boundary_points.Insert(new KeyValuePair<int, int>(
                                    map_in._boundary_points[i_i].Key, line_number));
                                //printf("\n insert <%d,%d>", (*i_i).first, line_number );

                                //msg = "";
                                //msg.AppendFormat("insert <%d,%d>", (*i_i).first, line_number);
                                //CGR_LOG(msg.GetBuffer(),INFO_LEVEL)

                                // check if jump is in unlinked jumps map, is so => remove it

                                int i_uf1 = unlinked_boundary_points.Find(map_in._boundary_points[i_i].Key);
                                if (i_uf1 != unlinked_boundary_points.Count)
                                    unlinked_boundary_points.RemoveAt(i_uf1);
                            }
                            else
                            { // both jumps are already assigned to lines, leave them as they are
                            }

                }
                else
                {   // no jump was found with Po near enough to i_i's Po
                    // => ignore this point, treat it as an isolated point not on a line
                    //printf("\n no jump was found with Po near enough to i_i's Po " );

                    unlinked_boundary_points.Insert(map_in._boundary_points[i_i]);
                }
            }

            // find two adjacent linked jumps on the same phase line
            // remove the right-most jump, add it with the unlinked jumps
            int i_linked_jump_a = 0;// = map_in._linked_boundary_points.begin();
            int i_linked_jump_b = i_linked_jump_a;
            i_linked_jump_b++;
            while (i_linked_jump_b != map_in._linked_boundary_points.Count)
            {
                int y_a = (int)(map_in._linked_boundary_points[i_linked_jump_a].Key / ramp_length);
                int y_b = (int)(map_in._linked_boundary_points[i_linked_jump_b].Key / ramp_length);
                if (y_a == y_b && map_in._linked_boundary_points[i_linked_jump_a].Value == map_in._linked_boundary_points[i_linked_jump_b].Value)
                {
                    // found linked jumps on the same phase line
                    int x_a = (int)(map_in._linked_boundary_points[i_linked_jump_a].Key) - y_a * (int)ramp_length;
                    int x_b = (int)(map_in._linked_boundary_points[i_linked_jump_b].Key) - y_a * (int)ramp_length;

                    // unlink right-most jump
                    if ((forward && x_a > x_b) || (!forward && x_a < x_b))
                    {
                        //CString msg = "";
                        //msg.AppendFormat("unlinking <%d,%d>", map_in._linked_boundary_points[i_linked_jump_a].Key, (*i_linked_jump_a).second );
                        //CGR_LOG(msg.GetBuffer(),INFO_LEVEL)

                        int i_fj
                            = map_in._boundary_points.Find(map_in._linked_boundary_points[i_linked_jump_a].Key);
                        unlinked_boundary_points.Insert(map_in._boundary_points[i_fj]);
                        map_in._linked_boundary_points.RemoveAt(i_linked_jump_a);
                    }
                    else
                    {
                        //CString msg = "";
                        //msg.AppendFormat("unlinking <%d,%d>", map_in._linked_boundary_points[i_linked_jump_b].Key, (*i_linked_jump_b).second );
                        //CGR_LOG(msg.GetBuffer(),INFO_LEVEL)

                        int i_fj
                            = map_in._boundary_points.Find(map_in._linked_boundary_points[i_linked_jump_b].Key);
                        unlinked_boundary_points.Insert(map_in._boundary_points[i_fj]);
                        map_in._linked_boundary_points.RemoveAt(i_linked_jump_b);
                    }

                    // go back to start
                    //i_linked_jump_a = map_in._linked_boundary_points.begin();
                    i_linked_jump_a = 0;
                    i_linked_jump_b = i_linked_jump_a;
                    i_linked_jump_b++;
                }
                else
                {   // go to next pair
                    i_linked_jump_a++;
                    i_linked_jump_b++;
                }
            }

            // find groups of linked jumps and check if two groups belong together
            bool jumps_found = true;
            boundary_multimap_typedef boundary_line_points = new boundary_multimap_typedef();
            boundary_multimap_typedef alt_boundary_line_points = new boundary_multimap_typedef();
            int boundary_line_number = 0;
            int alt_boundary_line_number = 1;
            while (jumps_found)
            {
                boundary_line_points.Clear();
                alt_boundary_line_points.Clear();
                //for( boundary_multimap_typedef::iterator i_linked_jumps = map_in._linked_boundary_points.begin() ;
                //    i_linked_jumps != map_in._linked_boundary_points.end() ; i_linked_jumps++ )
                for (int i_linked_jumps = 0;
                    i_linked_jumps < map_in._linked_boundary_points.Count; i_linked_jumps++)
                {
                    if (map_in._linked_boundary_points[i_linked_jumps].Value == boundary_line_number)
                    {
                        // found a jump on the current boundary line
                        boundary_line_points.Insert(map_in._linked_boundary_points[i_linked_jumps]);
                    }
                    if (map_in._linked_boundary_points[i_linked_jumps].Value == alt_boundary_line_number)
                    {
                        // found a jump on the alternate boundary line
                        alt_boundary_line_points.Insert(map_in._linked_boundary_points[i_linked_jumps]);
                    }
                }

                if (boundary_line_points.Count == 0)
                { // no jumps found for this line => gone past last line
                    jumps_found = false; // this will end the while loop
                }
                else
                {
                    // check each jump of the alternate line to see if it belongs to the same line
                    bool same_phase_line_found = false;
                    bool upper_adjacent_phase_line_found = false;
                    bool lower_adjacent_phase_line_found = false;
                    int max_x_distance = 0;
                    int min_y = steps_count;
                    int max_y = 0;
                    int alt_min_y = steps_count;
                    int alt_max_y = 0;
                    int double_points_count = 0;
                    //for( boundary_multimap_typedef::iterator i_al = alt_boundary_line_points.begin();
                    //    i_al != alt_boundary_line_points.end() ; i_al++ )
                    for (int i_al = 0; i_al < alt_boundary_line_points.Count; i_al++)
                    {
                        int y_al = (int)((alt_boundary_line_points[i_al].Key) / ramp_length);
                        int x_al = (int)(alt_boundary_line_points[i_al].Key) - y_al * (int)ramp_length;

                        if (y_al < alt_min_y) alt_min_y = y_al;
                        if (y_al > alt_max_y) alt_max_y = y_al;

                        //for( boundary_multimap_typedef::iterator i_l = boundary_line_points.begin();
                        //    i_l != boundary_line_points.end() ; i_l++ )
                        for (int i_l = 0; i_l < boundary_line_points.Count; i_l++)
                        {
                            int y_l = (int)(boundary_line_points[i_l].Key / ramp_length);
                            int x_l = (int)(boundary_line_points[i_l].Key) - y_l * (int)ramp_length;

                            if (y_l < min_y) min_y = y_l;
                            if (y_l > max_y) max_y = y_l;

                            if (y_al == y_l)
                            { // same phase line, the two lines are not the same
                                same_phase_line_found = true;
                                //break;
                                int x_gap = Math.Abs(x_l - x_al);
                                if (x_gap > max_x_distance) max_x_distance = x_gap;
                                double_points_count++;
                            }

                            //if( y_al+1 == y_l && abs(x_l-x_al) <= MAX_DISTANCE_BETWEEN_LINKED_JUMPS )
                            if (y_al + 1 == y_l && Math.Abs(x_l - x_al) <= linking_max_distance_between_linked_jumps)
                            { // found a linked jump adjacent and above the previous line
                                upper_adjacent_phase_line_found = true;
                            }

                            //if( y_al-1 == y_l && abs(x_l-x_al) <= MAX_DISTANCE_BETWEEN_LINKED_JUMPS )
                            if (y_al - 1 == y_l && Math.Abs(x_l - x_al) <= linking_max_distance_between_linked_jumps)
                            { // found a linked jump adjacent and above the previous line
                                lower_adjacent_phase_line_found = true;
                            }
                        }

                        //if(same_phase_line_found) break;
                    }

                    string msg = string.Empty;

                    if (!same_phase_line_found
                        && upper_adjacent_phase_line_found
                        && lower_adjacent_phase_line_found)
                    { // found two sets of jumps that appear to be the same line
                        // merge the two sets of jumps

                        int line_number_to_change;
                        int line_number_not_to_change;
                        if (boundary_line_number < alt_boundary_line_number)
                        {
                            line_number_not_to_change = boundary_line_number;
                            line_number_to_change = alt_boundary_line_number;
                        }
                        else
                        {
                            line_number_not_to_change = alt_boundary_line_number;
                            line_number_to_change = boundary_line_number;
                        }

                        //if(forward) msg.Append("Forward Mode Map: ");
                        //else msg.Append("Reverse Mode Map: ");
                        //msg.AppendFormat("Merging line %d into %d", line_number_to_change, line_number_not_to_change );

                        boundary_multimap_typedef new_linked_boundary_points = new boundary_multimap_typedef();
                        //for( boundary_multimap_typedef::iterator i_linked_jumps = map_in._linked_boundary_points.begin() ;
                        //    i_linked_jumps != map_in._linked_boundary_points.end() ; i_linked_jumps++ )
                        for (int i_linked_jumps = 0;
                            i_linked_jumps < map_in._linked_boundary_points.Count; i_linked_jumps++)
                        {
                            if (map_in._linked_boundary_points[i_linked_jumps].Value <= line_number_not_to_change)
                            {
                                new_linked_boundary_points.Insert(map_in._linked_boundary_points[i_linked_jumps]);
                            }
                            else if (map_in._linked_boundary_points[i_linked_jumps].Value == line_number_to_change)
                            {
                                new_linked_boundary_points.Insert(
                                    new KeyValuePair<int, int>(
                                        map_in._linked_boundary_points[i_linked_jumps].Key,
                                        line_number_not_to_change));
                            }
                            else
                            {
                                new_linked_boundary_points.Insert(
                                    new KeyValuePair<int, int>(
                                        map_in._linked_boundary_points[i_linked_jumps].Key,
                                        (map_in._linked_boundary_points[i_linked_jumps].Value) - 1));
                            }
                        }
                        map_in._linked_boundary_points = new_linked_boundary_points;

                        if (boundary_line_number < alt_boundary_line_number)
                        {
                            if (alt_boundary_line_number > 0) alt_boundary_line_number--;
                        }
                        else
                        {
                            if (boundary_line_number > 0) boundary_line_number--;
                        }
                    }
                    else
                        if (((min_y >= alt_min_y - 1 && max_y <= alt_max_y + 1)
                             || (alt_min_y >= min_y - 1 && alt_max_y <= max_y + 1))
                            && same_phase_line_found
                            && (double)max_x_distance < linking_max_gap_between_double_lines * ((double)ramp_length) / 100
                            && alt_boundary_line_points.Count >= linking_min_gap_between_double_lines
                            && boundary_line_points.Count >= linking_min_gap_between_double_lines)
                        //&& (double)max_x_distance < MAX_GAP_BETWEEN_DOUBLE_LINES*((double)ramp_length)/100
                        //&& alt_boundary_line_points.Count >= MIN_POINTS_OF_DOUBLE_LINES
                        //&& boundary_line_points.Count >= MIN_POINTS_OF_DOUBLE_LINES )
                        {
                            // double line found, delete group with least number of jumps

                            int line_to_delete;
                            boundary_multimap_typedef backup_boundary_line_points;
                            if (alt_boundary_line_points.Count < boundary_line_points.Count)
                            {
                                line_to_delete = alt_boundary_line_number;
                            }
                            else
                            {
                                line_to_delete = boundary_line_number;
                            }

                            if (forward) msg += "Forward Mode Map: ";
                            else msg += "Reverse Mode Map: ";
                            msg += string.Format("Double lines {0:d} and {1:d} detected", alt_boundary_line_number, boundary_line_number);

                            boundary_multimap_typedef new_linked_boundary_points = new boundary_multimap_typedef();
                            boundary_line_points.Clear();
                            //for( boundary_multimap_typedef::iterator i_linked_jumps = map_in._linked_boundary_points.begin() ;
                            //    i_linked_jumps != map_in._linked_boundary_points.end() ; i_linked_jumps++ )
                            for (int i_linked_jumps = 0;
                                i_linked_jumps < map_in._linked_boundary_points.Count; i_linked_jumps++)
                            {
                                if (map_in._linked_boundary_points[i_linked_jumps].Value < line_to_delete)
                                {
                                    new_linked_boundary_points.Insert(map_in._linked_boundary_points[i_linked_jumps]);
                                }

                                if (map_in._linked_boundary_points[i_linked_jumps].Value > line_to_delete)
                                {
                                    new_linked_boundary_points.Insert(
                                            new KeyValuePair<int, int>(
                                            map_in._linked_boundary_points[i_linked_jumps].Key,
                                            map_in._linked_boundary_points[i_linked_jumps].Value - 1));
                                }
                            }
                            map_in._linked_boundary_points = new_linked_boundary_points;

                            if (alt_boundary_line_points.Count < boundary_line_points.Count)
                            {
                                if (alt_boundary_line_number > 0) alt_boundary_line_number--;
                            }
                            else
                            {
                                if (boundary_line_number > 0) boundary_line_number--;
                            }
                        }

                    if (msg != "") CGR_LOG.Write(msg, LoggingLevels.INFO_CGR_LOG);
                }

                if (alt_boundary_line_points.Count == 0)
                {
                    boundary_line_number++;
                    alt_boundary_line_number = 0;
                }
                else if (boundary_line_number == alt_boundary_line_number + 1)
                {
                    alt_boundary_line_number = alt_boundary_line_number + 2;
                }
                else
                {
                    alt_boundary_line_number++;
                }
            }

            // Test each unlinked jump against each group of linked points
            // to see if a link can be established
            jumps_found = true;
            boundary_line_number = 0;
            //boundary_multimap_typedef::iterator i_unlinked_jumps = unlinked_boundary_points.begin();
            int i_unlinked_jumps = 0;
            while (jumps_found && unlinked_boundary_points.Count > 0)
            {
                int unlinked_point = unlinked_boundary_points[i_unlinked_jumps].Key;
                int y_ul = (int)(unlinked_point / ramp_length);
                int x_ul = (int)(unlinked_point) - y_ul * (int)ramp_length;
                bool same_phase_line = false;
                bool adjacent_phase_line = false;
                int max_x_distance = 0;
                int min_y = steps_count;
                int max_y = 0;
                boundary_line_points.Clear();

                //for( boundary_multimap_typedef::iterator i_linked_jumps = map_in._linked_boundary_points.begin() ;
                //    i_linked_jumps != map_in._linked_boundary_points.end() ; i_linked_jumps++ )
                for (int i_linked_jumps = 0;
                    i_linked_jumps < map_in._linked_boundary_points.Count; i_linked_jumps++)
                {
                    if (map_in._linked_boundary_points[i_linked_jumps].Value == boundary_line_number)
                    {
                        // found a jump on the current boundary line
                        boundary_line_points.Insert(map_in._linked_boundary_points[i_linked_jumps]);

                        int y_l = (int)(map_in._linked_boundary_points[i_linked_jumps].Key / ramp_length);
                        int x_l = (int)(map_in._linked_boundary_points[i_linked_jumps].Key) - y_l * (int)ramp_length;

                        if (y_l < min_y) min_y = y_l;
                        if (y_l > max_y) max_y = y_l;

                        int diff_y_ul_y_l = Math.Abs(y_ul - y_l);
                        if (diff_y_ul_y_l == 0) same_phase_line = true;

                        if (diff_y_ul_y_l == 1)
                        {
                            adjacent_phase_line = true;
                            if (diff_y_ul_y_l > max_x_distance) max_x_distance = Math.Abs(x_ul - x_l);
                        }
                    }
                }

                bool link_found = false;
                if (boundary_line_points.Count == 0)
                { // no jumps found for this line => gone past last line
                    jumps_found = false; // this will end the while loop
                }
                else
                { // Compare current unlinked jump to current line
                    if (!same_phase_line
                        && adjacent_phase_line
                        && (double)max_x_distance < linking_max_distance_to_single_unlinked_jump * (double)ramp_length / 100)
                    //&& (double)max_x_distance < MAX_DISTANCE_TO_LINK_SINGLE_UNLINKED_JUMP*((double)ramp_length)/100 )
                    {
                        // link jump to group
                        //boundary_multimap_typedef::iterator i_linked_jumps = map_in._linked_boundary_points.begin();
                        int i_linked_jumps = 0;
                        while (i_linked_jumps < map_in._linked_boundary_points.Count)
                        { // find correct spot in map to insert newly linked jump
                            if (map_in._linked_boundary_points[i_linked_jumps].Key > unlinked_boundary_points[i_unlinked_jumps].Key)
                            {
                                map_in._linked_boundary_points.Insert(i_linked_jumps,
                                    new KeyValuePair<int, int>(unlinked_boundary_points[i_unlinked_jumps].Key, boundary_line_number));

                                //CString msg = "";
                                //msg.AppendFormat("insert unlinked jump <%d,%d>", (*i_unlinked_jumps).first, boundary_line_number);
                                //CGR_LOG(msg.GetBuffer(),INFO_LEVEL)

                                unlinked_boundary_points.RemoveAt(i_unlinked_jumps);
                                link_found = true;
                                break;
                            }
                            else
                            {
                                i_linked_jumps++;
                            }
                        }
                    }
                }

                if (link_found)
                {
                    // are there any more unlinked points or not?
                    if (unlinked_boundary_points.Count == 0)
                    { // NO - finished
                        i_unlinked_jumps = unlinked_boundary_points.Count;
                    }
                    else
                    { // YES - go back to beginning of unlinked jumps
                        i_unlinked_jumps = 0;//= unlinked_boundary_points.begin();
                    }
                }
                else if (i_unlinked_jumps != unlinked_boundary_points.Count)
                {
                    i_unlinked_jumps++;
                }

                if (i_unlinked_jumps == unlinked_boundary_points.Count)
                { // reached the end of unlinked jumps, go to next group of linked jumps
                    boundary_line_number++;
                    i_unlinked_jumps = 0;
                }
            }

            CGR_LOG.Write("CDSDBRSuperMode::linkJumpsByIndex() exiting", LoggingLevels.DIAGNOSTIC_CGR_LOG);
        }

        public int boundaryPoint(KeyValuePair<int, int> iter_b)
        {
            int first = iter_b.Key;
            int second = iter_b.Value;
            int boundary;
            if ((int)second - (int)first == 2) boundary = second - 1;
            else if ((int)first - (int)second == 2) boundary = first - 1;
            else boundary = second;
            return boundary;
        }

        public rcode createLMBoundaryLines()
        {
            rcode rval = rcode.ok;
            CGR_LOG.Write("CDSDBRSuperMode::createLMBoundaryLines() entered", LoggingLevels.DIAGNOSTIC_CGR_LOG);
            // Create lines from the linked jumps, filling in
            // the gaps between jumps, and padding to LHS and RHS
            // except where lines meet the top or bottom.

            // create lines for forward map
            createModeBoundaryLines(
                _map_forward_power_ratio_median_filtered,
                _lm_lower_lines);

            // create lines for reverse map
            createModeBoundaryLines(
                _map_reverse_power_ratio_median_filtered,
                _lm_upper_lines);

            string log_msg1 = "CDSDBRSuperMode::createLMBoundaryLines() ";
            log_msg1 += string.Format("created {0} lower boundary lines", _lm_lower_lines.Count);
            CGR_LOG.Write(log_msg1, LoggingLevels.INFO_CGR_LOG);

            string log_msg2 = "CDSDBRSuperMode::createLMBoundaryLines() ";
            log_msg2 += string.Format("created {0} upper boundary lines", _lm_upper_lines.Count);
            CGR_LOG.Write(log_msg2, LoggingLevels.INFO_CGR_LOG);

            if (DSDBR01.Instance._DSDBR01_SMBD_write_debug_files)
            {
                for (int i = 0; i < _lm_lower_lines.Count; i++)
                {
                    // Assume _abs_filepath is set from loading data from file
                    // or from writing to file after gathering data from laser
                    string abs_filepath = _map_forward_power_ratio_median_filtered._abs_filepath;
                    //char buf[20];
                    //sprintf(buf,"lower_A_lm%d_",i);
                    abs_filepath = abs_filepath.Replace(Defaults.MATRIX_, "lower_A_lm" + i + "_");

                    // write boundary line to file
                    _lm_lower_lines[i].writeRowColToFile(abs_filepath, true);
                }

                for (int i = 0; i < _lm_upper_lines.Count; i++)
                {
                    // Assume _abs_filepath is set from loading data from file
                    // or from writing to file after gathering data from laser
                    String abs_filepath = _map_reverse_power_ratio_median_filtered._abs_filepath;
                    //char buf[20];
                    //sprintf(buf,"upper_A_lm%d_",i);
                    abs_filepath = abs_filepath.Replace(Defaults.MATRIX_, "upper_A_lm" + i + "_");

                    // write boundary line to file
                    _lm_upper_lines[i].writeRowColToFile(abs_filepath, true);
                }
            }

            // check each set of lines (forward and reverse) independently
            checkForwardAndReverseLines();

            string log_msg3 = "CDSDBRSuperMode::createLMBoundaryLines() ";
            log_msg3 += string.Format("have {0} lower boundary lines after checking", _lm_lower_lines.Count);
            CGR_LOG.Write(log_msg3, LoggingLevels.INFO_CGR_LOG);

            String log_msg4 = "CDSDBRSuperMode::createLMBoundaryLines() ";
            log_msg4 += string.Format("have {0} upper boundary lines after checking", _lm_upper_lines.Count);
            CGR_LOG.Write(log_msg4, LoggingLevels.INFO_CGR_LOG);

            if (DSDBR01.Instance._DSDBR01_SMBD_write_debug_files)
            {
                for (int i = 0; i < _lm_lower_lines.Count; i++)
                {
                    // Assume _abs_filepath is set from loading data from file
                    // or from writing to file after gathering data from laser
                    String abs_filepath = _map_forward_power_ratio_median_filtered._abs_filepath;
                    //char buf[20];
                    //sprintf(buf,"lower_B_lm%d_",i);
                    abs_filepath = abs_filepath.Replace(Defaults.MATRIX_, "lower_B_lm" + i + "_");

                    // write boundary line to file
                    _lm_lower_lines[i].writeRowColToFile(abs_filepath, true);
                }

                for (int i = 0; i < _lm_upper_lines.Count; i++)
                {
                    // Assume _abs_filepath is set from loading data from file
                    // or from writing to file after gathering data from laser
                    String abs_filepath = _map_reverse_power_ratio_median_filtered._abs_filepath;
                    //char buf[20];
                    //sprintf(buf,"upper_B_lm%d_",i);
                    abs_filepath = abs_filepath.Replace(Defaults.MATRIX_, "upper_B_lm" + i + "_");

                    // write boundary line to file
                    _lm_upper_lines[i].writeRowColToFile(abs_filepath, true);
                }
            }

            // associate forward lines and reverse lines
            associateForwardAndReverseLines();

            string log_msg5 = "CDSDBRSuperMode::createLMBoundaryLines() ";
            log_msg5 += string.Format("have {0} matched pairs of upper and lower boundary lines", _matched_line_numbers.Count);
            CGR_LOG.Write(log_msg5, LoggingLevels.INFO_CGR_LOG);
            if (DSDBR01.Instance._DSDBR01_SMBD_write_debug_files)
            {
                int mcount = 0;
                for (int i_m = 0;
                        i_m < _matched_line_numbers.Count; i_m++)
                {
                    // Assume _abs_filepath is set from loading data from file
                    // or from writing to file after gathering data from laser
                    string abs_filepath = _map_forward_power_ratio_median_filtered._abs_filepath;
                    //char buf[20];
                    //sprintf(buf,"lower_C_lm%d_m%d_",(*i_m).first,mcount);
                    abs_filepath = abs_filepath.Replace(Defaults.MATRIX_, "lower_C_lm" + _matched_line_numbers[i_m].Key + "_m" + mcount + "_");

                    // write boundary line to file
                    _lm_lower_lines[_matched_line_numbers[i_m].Key].writeRowColToFile(abs_filepath, true);

                    // Assume _abs_filepath is set from loading data from file
                    // or from writing to file after gathering data from laser
                    short upper_line_num = (short)((short)(_matched_line_numbers[i_m].Value) - (short)(_lm_lower_lines.Count));
                    abs_filepath = _map_reverse_power_ratio_median_filtered._abs_filepath;
                    //sprintf(buf, "upper_C_lm%d_m%d_", upper_line_num, mcount);
                    abs_filepath=abs_filepath.Replace(Defaults.MATRIX_, "upper_C_lm" + upper_line_num + "_m" + mcount + "_");

                    // write boundary line to file
                    _lm_upper_lines[upper_line_num].writeRowColToFile(abs_filepath, true);

                    mcount++;
                }
            }

            // remove forward or reverse lines that have not been associated
            removeUnassociatedLines();

            if (DSDBR01.Instance._DSDBR01_SMBD_write_debug_files)
            {
                int mcount = 0;
                //for( matchedlines_typedef::iterator i_m = _matched_line_numbers.begin();
                //        i_m != _matched_line_numbers.end() ; i_m++ )
                for (int i_m = 0; i_m < _matched_line_numbers.Count; i_m++)
                {
                    // Assume _abs_filepath is set from loading data from file
                    // or from writing to file after gathering data from laser
                    string abs_filepath = _map_forward_power_ratio_median_filtered._abs_filepath;
                    //char buf[20];
                    //sprintf(buf,"lower_D_lm%d_m%d_",(*i_m).first,mcount);
                    abs_filepath = abs_filepath.Replace(Defaults.MATRIX_, "lower_D_lm" + _matched_line_numbers[i_m].Key + "_m" + mcount + "_");

                    // write boundary line to file
                    _lm_lower_lines[_matched_line_numbers[i_m].Key].writeRowColToFile(abs_filepath, true);


                    // Assume _abs_filepath is set from loading data from file
                    // or from writing to file after gathering data from laser
                    short upper_line_num = (short)((short)(_matched_line_numbers[i_m].Value) - (short)(_lm_lower_lines.Count));
                    abs_filepath = _map_reverse_power_ratio_median_filtered._abs_filepath;
                    //sprintf(buf,"upper_D_lm%d_m%d_",upper_line_num,mcount);
                    abs_filepath = abs_filepath.Replace(Defaults.MATRIX_, "upper_D_lm" + upper_line_num + "_m" + mcount + "_");

                    // write boundary line to file
                    _lm_upper_lines[upper_line_num].writeRowColToFile(abs_filepath, true);

                    mcount++;
                }

                // Assume _abs_filepath is set from loading data from file
                // or from writing to file after gathering data from laser
                String fwd_abs_filepath = _map_forward_power_ratio_median_filtered._abs_filepath;
                fwd_abs_filepath = fwd_abs_filepath.Replace(Defaults.MATRIX_, "lowermatchedlines_");

                string rev_abs_filepath = _map_reverse_power_ratio_median_filtered._abs_filepath;
                rev_abs_filepath = rev_abs_filepath.Replace(Defaults.MATRIX_, "uppermatchedlines_");

                writeMatchedLinesToFile(fwd_abs_filepath, rev_abs_filepath);
            }

            CGR_LOG.Write("CDSDBRSuperMode::createLMBoundaryLines() exiting", LoggingLevels.DIAGNOSTIC_CGR_LOG);

            return rval;
        }


        public void createModeBoundaryLines(
            CLaserModeMap map_in,
            List<CDSDBRSupermodeMapLine> lines_in)
        {
            CGR_LOG.Write("CDSDBRSuperMode::createModeBoundaryLines() entered", LoggingLevels.DIAGNOSTIC_CGR_LOG);

            // clear any existing lines
            for (int i = 0; i < (int)(lines_in.Count); i++)
            {
                lines_in[i].clear();
            }
            lines_in.Clear();

            // determine ramp/step lengths
            int map_ramp_line_length;
            int steps_count;
            if (_Im_ramp) // middle line ramped
            {
                steps_count = (int)(_phase_current._currents.Count);
                map_ramp_line_length = (int)(_filtered_middle_line._points.Count);
            }
            else // phase ramped
            {
                steps_count = (int)(_filtered_middle_line._points.Count);
                map_ramp_line_length = (int)(_phase_current._currents.Count);
            }

            // Iterate through the map of jumps and for each marked boundary
            // copy all it's jump points into a new boundary line object

            bool forward_map = true;
            int boundary_line_number = 0;
            List<int> boundary_line_points = new List<int>();
            List<int> boundary_line_points_on_existing_line = new List<int>();
            List<int> all_boundary_points = new List<int>();

            bool jumps_found = true;
            while (jumps_found)
            {
                CGR_LOG.Write("CDSDBRSuperMode::createModeBoundaryLines() while jumps_found start", LoggingLevels.DIAGNOSTIC_CGR_LOG);

                boundary_line_points.Clear();
                boundary_line_points_on_existing_line.Clear();
                int ymax = -1;

                //for( boundary_multimap_typedef::iterator i_linked_jumps = map_in._linked_boundary_points.begin() ;
                //    i_linked_jumps != map_in._linked_boundary_points.end() ; i_linked_jumps++ )
                for (int i_linked_jumps = 0; i_linked_jumps < map_in._linked_boundary_points.Count; i_linked_jumps++)
                {
                    if (map_in._linked_boundary_points[i_linked_jumps].Value == boundary_line_number)
                    {
                        CGR_LOG.Write("CDSDBRSuperMode::createModeBoundaryLines() found a jump on the current boundary line", LoggingLevels.DIAGNOSTIC_CGR_LOG);

                        // found a jump on the current boundary line
                        // find the exact boundary point and add it to the vector
                        //boundary_multimap_typedef::iterator i_jump = map_in._boundary_points.find( (*i_linked_jumps).first );
                        int i_jump = map_in._boundary_points.Find(map_in._linked_boundary_points[i_linked_jumps].Key);
                        //if( i_jump == map_in._boundary_points.end() ) printf("\n %d not found ", (*i_linked_jumps).first );
                        //else printf("\n %d found <%d,%d>", (*i_linked_jumps).first, (*i_jump).first, (*i_jump).second );
                        int new_point = boundaryPoint(map_in._boundary_points[i_jump]);
                        //printf("\n insert %d into %d", new_point, boundary_line_number );

                        int ynew = (int)(new_point / map_ramp_line_length);
                        int xnew = new_point - ynew * map_ramp_line_length;
                        if (ynew > ymax)
                        { // jump on new line found
                            boundary_line_points.Add(new_point);
                            ymax = ynew;
                        }
                        else
                        { // jump on existing line found
                            boundary_line_points_on_existing_line.Add(new_point);
                        }
                    }
                }//end for

                if (boundary_line_points.Count + boundary_line_points_on_existing_line.Count == 0)
                { // no jumps found for this line => gone past last line
                    //printf("\n no jumps found for this line => gone past last line %d", boundary_line_number );

                    CGR_LOG.Write("CDSDBRSuperMode::createModeBoundaryLines() no jumps found for this line => gone past last line", LoggingLevels.DIAGNOSTIC_CGR_LOG);

                    jumps_found = false; // this will end the while loop
                }
                else if ((int)(boundary_line_points.Count)
                    + (int)(boundary_line_points_on_existing_line.Count)
                    < MIN_NUMBER_OF_JUMPS_TO_FORM_A_BOUNDARY_LINE)
                { // not enough jumps found => ignore these isolated points

                    CGR_LOG.Write("CDSDBRSuperMode::createModeBoundaryLines() not enough jumps found => ignore these isolated points", LoggingLevels.DIAGNOSTIC_CGR_LOG);

                    //printf("\n not enough jumps found => ignore these isolated points line %d", boundary_line_number );
                }
                else
                { // more than one jump found on the current boundary line
                    // => fill in missing parts and create new boundary line object
                    //printf("\n more than one jump found on the boundary line %d", boundary_line_number );

                    CGR_LOG.Write("CDSDBRSuperMode::createModeBoundaryLines() more than one jump found on the current boundary line", LoggingLevels.DIAGNOSTIC_CGR_LOG);
                    CGR_LOG.Write("CDSDBRSuperMode::createModeBoundaryLines() entering boundary_line_points_on_existing_line loop", LoggingLevels.DIAGNOSTIC_CGR_LOG);

                    for (int i = 0; i < boundary_line_points_on_existing_line.Count; i++)
                    {
                        int pi = boundary_line_points_on_existing_line[i];
                        int yi = (int)(pi / map_ramp_line_length);
                        int xi = pi - yi * map_ramp_line_length;
                        if (!forward_map) xi = (map_ramp_line_length - 1) - xi;

                        for (int j = 0; j < boundary_line_points.Count; j++)
                        {
                            int pj = boundary_line_points[j];
                            int yj = (int)(pj / map_ramp_line_length);
                            int xj = pj - yj * map_ramp_line_length;
                            if (!forward_map) xj = (map_ramp_line_length - 1) - xj;

                            if (yj == yi && xj > xi)
                            { // swap points on the same line so that left-most point is stored in main map
                                boundary_line_points[j] = pi;
                                boundary_line_points_on_existing_line[i] = pj;
                            }
                        }
                    }

                    CGR_LOG.Write("CDSDBRSuperMode::createModeBoundaryLines() exiting boundary_line_points_on_existing_line loop", LoggingLevels.DIAGNOSTIC_CGR_LOG);

                    //std::vector<int>::iterator i_point;
                    int i_point;

                    CGR_LOG.Write("CDSDBRSuperMode::createModeBoundaryLines() entering loop to fill each gap in the line choosing the nearest points to a straight line", LoggingLevels.DIAGNOSTIC_CGR_LOG);

                    // fill each gap in the line choosing the nearest points to a straight line
                    for (int i = 1; i < (int)boundary_line_points.Count; i++)
                    {
                        int previous_point = boundary_line_points[i - 1];
                        int current_point = boundary_line_points[i];
                        int previous_point_y = previous_point / map_ramp_line_length;
                        int current_point_y = current_point / map_ramp_line_length;
                        int previous_point_x = previous_point - previous_point_y * map_ramp_line_length;
                        int current_point_x = current_point - current_point_y * map_ramp_line_length;
                        int y_gap = (current_point_y > previous_point_y ?
                            current_point_y - previous_point_y : previous_point_y - current_point_y);
                        int x_gap = (current_point_x > previous_point_x ?
                            current_point_x - previous_point_x : previous_point_x - current_point_x);
                        int x_step = (current_point_x > previous_point_x ? 1 : -1); ;
                        int y_step = (current_point_y > previous_point_y ? 1 : -1); ;

                        //printf("\n previous_point = boundary_line_points[%d] = %d", i-1, previous_point );
                        //printf("\n current_point = boundary_line_points[%d] = %d", i, current_point );
                        //printf("\n previous_point_x,previous_point_y = %d,%d", previous_point_x,previous_point_y );
                        //printf("\n current_point_x,current_point_y = %d,%d", current_point_x,current_point_y );
                        //printf("\n x_gap,y_gap = %d,%d", x_gap,y_gap );
                        //printf("\n x_step,y_step = %d,%d", x_step,y_step );

                        // test for a gap
                        if (x_gap > 1 || y_gap > 1)
                        { // a gap exists, find points on a line that fills the gap

                            CGR_LOG.Write("CDSDBRSuperMode::createModeBoundaryLines() a gap exists, find points on a line that fills the gap", LoggingLevels.DIAGNOSTIC_CGR_LOG);

                            if (current_point_y == previous_point_y)
                            { // add horizontal line between previous_point and current_point
                                //printf("\n add horizontal line " );
                                CGR_LOG.Write("CDSDBRSuperMode::createModeBoundaryLines() add horizontal line between previous_point and current_point", LoggingLevels.DIAGNOSTIC_CGR_LOG);

                                for (int new_point = previous_point_x + x_step;
                                     new_point != current_point_x;
                                     new_point = new_point + x_step)
                                {
                                    // find the iterator pointing to current_point
                                    //i_point = std::find(boundary_line_points.begin(),boundary_line_points.end(),current_point );
                                    i_point = boundary_line_points.IndexOf(current_point);
                                    if (i_point == -1) i_point = boundary_line_points.Count;

                                    // insert new point before current_point
                                    boundary_line_points.Insert(i_point, current_point_y * map_ramp_line_length + new_point);
                                    i++;
                                    //printf("\n insert %d", current_point_y*map_ramp_line_length+new_point );
                                }
                            }
                            else if (current_point_x == previous_point_x)
                            { // add vertical line between previous_point and current_point
                                //printf("\n add vertical line ");

                                CGR_LOG.Write("CDSDBRSuperMode::createModeBoundaryLines() add vertical line between previous_point and current_point", LoggingLevels.DIAGNOSTIC_CGR_LOG);

                                for (int new_point = previous_point_y + y_step;
                                     new_point != current_point_y;
                                     new_point = new_point + y_step)
                                {
                                    // find the iterator pointing to current_point
                                    //i_point = std::find(boundary_line_points.begin(),boundary_line_points.end(),current_point );
                                    i_point = boundary_line_points.IndexOf(current_point);
                                    if (i_point == -1) i_point = boundary_line_points.Count;

                                    // insert new point before current_point
                                    boundary_line_points.Insert(i_point, new_point * map_ramp_line_length + current_point_x);
                                    i++;
                                    //printf("\n insert %d", new_point*map_ramp_line_length+current_point_x );
                                }
                            }
                            else
                            { // find the line between previous_point and current_point

                                CGR_LOG.Write("CDSDBRSuperMode::createModeBoundaryLines() find the line between previous_point and current_point", LoggingLevels.DIAGNOSTIC_CGR_LOG);

                                double slope = ((double)current_point_y - (double)previous_point_y)
                                    / ((double)current_point_x - (double)previous_point_x);
                                double intersect = (((double)previous_point_y) * ((double)current_point_x)
                                    - ((double)current_point_y) * ((double)previous_point_x))
                                    / ((double)current_point_x - (double)previous_point_x);
                                //printf("\n add line slope = %g, intersect = %g", slope, intersect);
                                List<double> distances = new List<double>();

                                if (x_gap >= y_gap)
                                { // search each vertical line between previous_point and current_point
                                    // for the point nearest a straight line between those points
                                    // and add those points to the boundary line
                                    //printf(", search each vertical line");

                                    CGR_LOG.Write("CDSDBRSuperMode::createModeBoundaryLines() search each vertical line between previous_point and current_point", LoggingLevels.DIAGNOSTIC_CGR_LOG);

                                    List<double> x_values = new List<double>();
                                    List<double> y_values = new List<double>();
                                    for (int new_x_point = previous_point_x + x_step;
                                        new_x_point != current_point_x;
                                        new_x_point = new_x_point + x_step)
                                    {
                                        x_values.Clear();
                                        y_values.Clear();
                                        for (int new_y_point = previous_point_y;
                                            new_y_point != current_point_y + y_step;
                                            new_y_point = new_y_point + y_step)
                                        {
                                            x_values.Add((double)new_x_point);
                                            y_values.Add((double)new_y_point);
                                        }

                                        // find the distances of each point on a vertical line to the fitted line
                                        distances.Clear();
                                        VectorAnalysis.distancesFromPointsToLine(x_values, y_values,
                                                            slope, intersect, distances);
                                        // find the point nearest the fitted line
                                        double shortest_distance = distances[0];
                                        int nearest_y_point = (int)(y_values[0]);
                                        //printf("\n x_values[0], y_values[0], distances[0] = %g, %g, %g",x_values[0], y_values[0], distances[0]);
                                        for (int id = 1; id < distances.Count; id++)
                                        {
                                            if (distances[id] < shortest_distance)
                                            {
                                                shortest_distance = distances[id];
                                                nearest_y_point = (int)(y_values[id]);
                                            }
                                            //printf("\n x_values[%d], y_values[%d], distances[%d] = %g, %g, %g",id,id,id,x_values[id], y_values[id], distances[id]);
                                        }
                                        //printf("\n shortest_distance = %g, nearest_y_point = %d",shortest_distance,nearest_y_point);

                                        // find the iterator pointing to current_point
                                        //i_point = std::find(boundary_line_points.begin(),boundary_line_points.end(),current_point );
                                        i_point = boundary_line_points.IndexOf(current_point);
                                        if (i_point == -1) i_point = boundary_line_points.Count;

                                        // insert new point before current_point
                                        boundary_line_points.Insert(i_point, nearest_y_point * map_ramp_line_length + new_x_point);
                                        i++;
                                        //printf("\n insert %d", nearest_y_point*map_ramp_line_length+new_x_point );
                                    }
                                }
                                else
                                { // search each horizontal line between previous_point and current_point
                                    // for the point nearest a straight line between those points
                                    // and add those points to the boundary line
                                    //printf(", search each horizontal line");

                                    CGR_LOG.Write("CDSDBRSuperMode::createModeBoundaryLines() search each horizontal line between previous_point and current_point", LoggingLevels.DIAGNOSTIC_CGR_LOG);

                                    List<double> x_values = new List<double>();
                                    List<double> y_values = new List<double>();
                                    for (int new_y_point = previous_point_y + y_step;
                                        new_y_point != current_point_y;
                                        new_y_point = new_y_point + y_step)
                                    {
                                        x_values.Clear();
                                        y_values.Clear();
                                        for (int new_x_point = previous_point_x;
                                            new_x_point != current_point_x + x_step;
                                            new_x_point = new_x_point + x_step)
                                        {
                                            x_values.Add((double)new_x_point);
                                            y_values.Add((double)new_y_point);
                                        }

                                        // find the distances of each point on a vertical line to the fitted line
                                        distances.Clear();
                                        VectorAnalysis.distancesFromPointsToLine(
                                            x_values, y_values,
                                            slope, intersect, distances);

                                        // find the point nearest the fitted line
                                        double shortest_distance = distances[0];
                                        int nearest_x_point = (int)(x_values[0]);
                                        //printf("\n x_values[0], y_values[0], distances[0] = %g, %g, %g",x_values[0], y_values[0], distances[0]);
                                        for (int id = 1; id < distances.Count; id++)
                                        {
                                            if (distances[id] < shortest_distance)
                                            {
                                                shortest_distance = distances[id];
                                                nearest_x_point = (int)(x_values[id]);
                                            }
                                            //printf("\n x_values[%d], y_values[%d], distances[%d] = %g, %g, %g",id,id,id,x_values[id], y_values[id], distances[id]);
                                        }
                                        //printf("\n shortest_distance = %g, nearest_y_point = %d",shortest_distance,nearest_x_point);

                                        // find the iterator pointing to current_point
                                        //i_point = std::find(boundary_line_points.begin(),boundary_line_points.end(),current_point );
                                        i_point = boundary_line_points.IndexOf(current_point);
                                        if (i_point == -1) i_point = boundary_line_points.Count;
                                        // insert new point before current_point
                                        boundary_line_points.Insert(i_point, new_y_point * map_ramp_line_length + nearest_x_point);
                                        i++;
                                        //printf("\n insert %d", new_y_point*map_ramp_line_length+nearest_x_point );
                                    }
                                }
                            }
                        }
                    }

                    CGR_LOG.Write("CDSDBRSuperMode::createModeBoundaryLines() exiting loop to fill each gap in the line choosing the nearest points to a straight line", LoggingLevels.DIAGNOSTIC_CGR_LOG);
                    CGR_LOG.Write("CDSDBRSuperMode::createModeBoundaryLines() check if the bottom needs to be padded the left-hand-side", LoggingLevels.DIAGNOSTIC_CGR_LOG);

                    List<int> points_to_pad_to_lhs = new List<int>();
                    //i_point = boundary_line_points.begin();
                    i_point = 0;
                    // check if the bottom needs to be padded the left-hand-side
                    int first_ypos = (int)(boundary_line_points[i_point] / map_ramp_line_length);
                    int first_xpos = boundary_line_points[i_point] - first_ypos * map_ramp_line_length;
                    if (first_ypos > MAX_DISTANCE_TO_PAD_VERTICALLY
                     || (first_xpos < 2 * MAX_DISTANCE_TO_PAD_VERTICALLY && first_ypos > 0)
                     || (first_xpos > map_ramp_line_length - 2 * MAX_DISTANCE_TO_PAD_VERTICALLY && first_ypos > 0))
                    { // the first point is not near enough to the bottom line => pad to left-hand-side
                        int first_point = boundary_line_points[i_point];
                        int lhs_point;
                        if (forward_map)
                        { // find left-hand-side of forward map
                            lhs_point = (((int)(first_point / map_ramp_line_length)) - 1) * map_ramp_line_length;
                            for (int pad_point = lhs_point;
                                pad_point < first_point - map_ramp_line_length;
                                pad_point++)
                            {
                                points_to_pad_to_lhs.Add(pad_point);
                            }
                        }
                        else
                        { // find left-hand-side of reverse map
                            lhs_point = (((int)(first_point / map_ramp_line_length)) * map_ramp_line_length) - 1;
                            for (int pad_point = lhs_point;
                                pad_point > first_point - map_ramp_line_length;
                                pad_point--)
                            {
                                points_to_pad_to_lhs.Add(pad_point);
                            }
                        }

                        bool padding_okay = true;
                        // check that points do not cross any existing line
                        //for( std::vector<int>::iterator i_pplhs = points_to_pad_to_lhs.begin();
                        //    i_pplhs != points_to_pad_to_lhs.end() ;
                        //    i_pplhs++ )
                        for (int i_pplhs = 0; i_pplhs < points_to_pad_to_lhs.Count; i_pplhs++)
                        {
                            //std::vector<int>::iterator i_fabp =
                            //    std::find(all_boundary_points.begin(),all_boundary_points.end(),*i_pplhs);
                            int i_fabp = all_boundary_points.IndexOf(points_to_pad_to_lhs[i_pplhs]);
                            if (i_fabp == -1) i_fabp = all_boundary_points.Count;

                            if (i_fabp != all_boundary_points.Count)
                            { // a pad point already exists on another lines! => do not pad
                                padding_okay = false;
                                break;
                            }
                        }

                        if (padding_okay)
                        { // add all padded points to map containing points of whole line
                            //boundary_line_points.insert(
                            //    boundary_line_points.begin(),
                            //    points_to_pad_to_lhs.begin(),points_to_pad_to_lhs.end() );
                            boundary_line_points.InsertRange(0, points_to_pad_to_lhs);
                        }
                    }
                    else if (first_ypos > 0)
                    { // pad from the first point vertically to the bottom
                        int pad_point = boundary_line_points[i_point] - map_ramp_line_length;
                        while (pad_point >= 0)
                        {
                            //boundary_line_points.insert(boundary_line_points.begin(),(unsigned int)pad_point);
                            boundary_line_points.Insert(0, (int)pad_point);

                            pad_point = pad_point - (int)map_ramp_line_length;
                        }
                    }

                    CGR_LOG.Write("CDSDBRSuperMode::createModeBoundaryLines() check the last point needs to be padded to the right-hand-side", LoggingLevels.DIAGNOSTIC_CGR_LOG);

                    List<int> points_to_pad_to_rhs = new List<int>();
                    i_point = boundary_line_points.Count - 1;
                    // check the last point needs to be padded to the right-hand-side
                    int last_ypos = (int)(boundary_line_points[i_point] / map_ramp_line_length);
                    int last_xpos = boundary_line_points[i_point] - last_ypos * map_ramp_line_length;
                    if (last_ypos < (steps_count - 1) - MAX_DISTANCE_TO_PAD_VERTICALLY
                     || (last_xpos < 2 * MAX_DISTANCE_TO_PAD_VERTICALLY && last_ypos < (steps_count - 1))
                     || (last_xpos > map_ramp_line_length - 2 * MAX_DISTANCE_TO_PAD_VERTICALLY && last_ypos < (steps_count - 1)))
                    { // the last point is not near enough to the top line => pad to right-hand-side

                        CGR_LOG.Write("CDSDBRSuperMode::createModeBoundaryLines() the last point is not near enough to the top line => pad to right-hand-side", LoggingLevels.DIAGNOSTIC_CGR_LOG);

                        int last_point = boundary_line_points[i_point];
                        int rhs_point;
                        if (forward_map)
                        { // find right-hand-side of forward map
                            rhs_point = ((((int)(last_point / map_ramp_line_length)) + 1) * map_ramp_line_length) - 1;
                            for (int pad_point = last_point + 1;
                                pad_point <= rhs_point;
                                pad_point++)
                            {
                                points_to_pad_to_rhs.Add(pad_point);
                            }
                        }
                        else
                        { // find right-hand-side of reverse map
                            rhs_point = ((int)(last_point / map_ramp_line_length)) * map_ramp_line_length;
                            for (int pad_point = last_point - 1;
                                pad_point >= rhs_point;
                                pad_point--)
                            {
                                points_to_pad_to_rhs.Add(pad_point);
                            }
                        }

                        bool padding_okay = true;
                        // check that points do not cross any existing line
                        //for( std::vector<int>::iterator i_pprhs = points_to_pad_to_rhs.begin();
                        //    i_pprhs != points_to_pad_to_rhs.end();i_pprhs++ )
                        for (int i_pprhs = 0; i_pprhs < points_to_pad_to_rhs.Count; i_pprhs++)
                        {
                            //std::vector<int>::iterator i_fabp =
                            //    std::find(all_boundary_points.begin(),all_boundary_points.end(),*i_pprhs);
                            int i_fabp = all_boundary_points.IndexOf(points_to_pad_to_rhs[i_pprhs]);
                            if (i_fabp == -1) i_fabp = all_boundary_points.Count;

                            if (i_fabp != all_boundary_points.Count)
                            { // a pad point already exists on another lines! => do not pad
                                padding_okay = false;
                                break;
                            }
                        }

                        if (padding_okay)
                        { // add all padded points to map containing points of whole line
                            //boundary_line_points.insert(
                            //    boundary_line_points.end(),
                            //    points_to_pad_to_rhs.begin(),
                            //    points_to_pad_to_rhs.end() );
                            boundary_line_points.AddRange(points_to_pad_to_rhs);
                        }
                    }
                    else if (last_ypos < (steps_count - 1))
                    { // pad from the last point vertically to the top
                        CGR_LOG.Write("CDSDBRSuperMode::createModeBoundaryLines() pad from the last point vertically to the top", LoggingLevels.DIAGNOSTIC_CGR_LOG);

                        int pad_point = boundary_line_points[i_point] + map_ramp_line_length;
                        while (pad_point < map_ramp_line_length * steps_count)
                        {
                            //boundary_line_points.insert(
                            //    boundary_line_points.end(),
                            //    pad_point);
                            boundary_line_points.Add(pad_point);

                            pad_point = pad_point + map_ramp_line_length;
                        }
                    }


                    // Check the line's first point and last point are on edge of mode map
                    //first_point = boundary_line_points[0];//= *(boundary_line_points.begin());
                    //last_point = boundary_line_points[boundary_line_points.Count - 1];//= *((boundary_line_points.end())-1);
                    int y_first_point = boundary_line_points[0] / map_ramp_line_length;
                    int x_first_point = boundary_line_points[0] - y_first_point * map_ramp_line_length;
                    int y_last_point = boundary_line_points[boundary_line_points.Count - 1] / map_ramp_line_length;
                    int x_last_point = boundary_line_points[boundary_line_points.Count - 1] - y_last_point * map_ramp_line_length;
                    if (!forward_map) // reverse map
                    {
                        x_first_point = (map_ramp_line_length - 1) - x_first_point;
                        x_last_point = (map_ramp_line_length - 1) - x_last_point;
                    }

                    if ((y_first_point == 0   // first point is on bottom line
                       || x_first_point == 0) // OR first point is on LHS
                     && (y_last_point == steps_count - 1 // AND last point is on top line
                       || x_last_point == (map_ramp_line_length - 1))) // OR last point is on RHS
                    {
                        // the line detected is complete (starts and ends on an edge of the mode map)

                        CGR_LOG.Write("CDSDBRSuperMode::createModeBoundaryLines() the line detected is complete (starts and ends on an edge of the mode map)", LoggingLevels.DIAGNOSTIC_CGR_LOG);

                        // add points to vector of points of all lines found so far
                        //for( std::vector<int>::iterator i_blp = boundary_line_points.begin();
                        //    i_blp != boundary_line_points.end();i_blp++ )
                        for (int i_blp = 0; i_blp < boundary_line_points.Count; i_blp++)
                        {
                            all_boundary_points.Add(boundary_line_points[i_blp]);
                        }

                        // At this point we have a std::vector<int> containing the indices of a line
                        // Now we want to convert those into real x-y values,
                        // store them in a CDSDBRSupermodeMapLine object, and then add it to the results.

                        CDSDBRSupermodeMapLine new_boundary_line = new CDSDBRSupermodeMapLine();

                        //for( std::vector<int>::iterator i_blp = boundary_line_points.begin();
                        //    i_blp != boundary_line_points.end() ;i_blp++ )
                        for (int i_blp = 0; i_blp < boundary_line_points.Count; i_blp++)
                        {
                            int row = (int)(boundary_line_points[i_blp] / map_ramp_line_length);
                            int col = (int)(boundary_line_points[i_blp] - ((int)row) * map_ramp_line_length);

                            new_boundary_line._points.Add(convertRowCol2LinePoint(row, col));
                        }


                        if (lines_in.Count == 0)
                            lines_in.Add(new_boundary_line);
                        else
                        {
                            int new_line_pos_on_bottom_lhs = y_first_point + ((map_ramp_line_length - 1) - x_first_point);

                            // Need to find where to put the line in amongst neighbouring lines
                            bool foundIt = false;
                            //for( List<CDSDBRSupermodeMapLine>::iterator i_line = lines_in.begin();i_line != lines_in.end() ;i_line++ )
                            for (int i_line = 0; i_line < lines_in.Count; i_line++)
                            {
                                int pos_on_bottom_lhs = lines_in[i_line]._points[0].row +
                                    (((int)map_ramp_line_length - 1) - lines_in[i_line]._points[0].col);

                                if (pos_on_bottom_lhs > new_line_pos_on_bottom_lhs)
                                {
                                    // the line is ahead of the one we wish to insert
                                    foundIt = true;
                                    lines_in.Insert(i_line, new_boundary_line);
                                    break;
                                }
                            }
                            if (!foundIt)
                                lines_in.Add(new_boundary_line);
                        }
                    }
                    else
                    { // increment counter of lines removed
                        //results._number_of_lines_removed++;
                    }
                }
                boundary_line_number++;
            }

            CGR_LOG.Write("CDSDBRSuperMode::createModeBoundaryLines() exiting", LoggingLevels.DIAGNOSTIC_CGR_LOG);
        }

        public void checkForwardAndReverseLines()
        {
            CGR_LOG.Write("CDSDBRSuperMode::checkForwardAndReverseLines() entered", LoggingLevels.DIAGNOSTIC_CGR_LOG);
            int ramp_length;
            int steps_count;

            if (_Im_ramp) // middle line ramped
            {
                steps_count = (int)(_phase_current._currents.Count);
                ramp_length = (int)(_filtered_middle_line._points.Count);
            }
            else // phase ramped
            {
                steps_count = (int)(_filtered_middle_line._points.Count);
                ramp_length = (int)(_phase_current._currents.Count);
            }

            int not_a_value = -100;

            int prev_forward_line = not_a_value;
            int prev_forward_bottom_rhs_first_pos = not_a_value;
            int prev_forward_lhs_top_last_pos = not_a_value;

            int prev_reverse_line = not_a_value;
            int prev_reverse_bottom_rhs_first_pos = not_a_value;
            int prev_reverse_lhs_top_last_pos = not_a_value;

            int cl = 0;
            while (cl < (int)(_lm_lower_lines.Count + _lm_upper_lines.Count))
            { // iterate through lines


                //std::vector<CDSDBRSupermodeMapLine>::iterator i_lm_boundary_line;
                int i_lm_boundary_line;

                bool forward_lower = true;
                int line_num = 0;

                if (cl < (int)(_lm_lower_lines.Count))
                {
                    //i_lm_boundary_line = _lm_lower_lines.begin();
                    i_lm_boundary_line = 0;
                    line_num = cl;
                    i_lm_boundary_line += line_num;
                    forward_lower = true;
                }
                else
                {
                    //i_lm_boundary_line = _lm_upper_lines.begin();
                    i_lm_boundary_line = 0;
                    line_num = (cl - (int)(_lm_lower_lines.Count));
                    i_lm_boundary_line += line_num;
                    forward_lower = false;
                }

                if (forward_lower)
                {
                    // find y and x values of first point in line

                    //std::vector<int>::iterator i_first_point = mbl->getX()->getIndices()->begin();
                    //int first_y = (*i_first_point)/map1_ramp_line_length;
                    //int first_x = (*i_first_point) - first_y*map1_ramp_line_length;
                    int first_y = _lm_lower_lines[i_lm_boundary_line]._points[0].row;
                    int first_x = _lm_lower_lines[i_lm_boundary_line]._points[0].col;

                    int bottom_rhs_first_pos;
                    if (first_y == 0) bottom_rhs_first_pos = (ramp_length - 1) - first_x;
                    else bottom_rhs_first_pos = first_y + ramp_length;

                    // find y and x values of last point in line

                    //std::vector<int>::iterator i_last_point = mbl->getX()->getIndices()->end();
                    //i_last_point--;
                    //int last_y = (*i_last_point)/ramp_length;
                    //int last_x = (*i_last_point) - last_y*ramp_length;
                    int index_last = _lm_lower_lines[i_lm_boundary_line]._points.Count - 1;
                    int last_y = _lm_lower_lines[i_lm_boundary_line]._points[index_last].row;
                    int last_x = _lm_lower_lines[i_lm_boundary_line]._points[index_last].col;
                    int lhs_top_last_pos;
                    if (last_x == ramp_length - 1) lhs_top_last_pos = last_y;
                    else lhs_top_last_pos = last_y + (ramp_length - 1) - last_x;


                    if (prev_forward_line != not_a_value && line_num > 0 && _lm_lower_lines.Count > 1 &&
                      (bottom_rhs_first_pos == prev_forward_bottom_rhs_first_pos
                     || lhs_top_last_pos == prev_forward_lhs_top_last_pos
                     || (bottom_rhs_first_pos - prev_forward_bottom_rhs_first_pos) * (lhs_top_last_pos - prev_forward_lhs_top_last_pos) < 0
                     || DSDBR01.Instance._DSDBR01_SMBD_min_points_width_of_lm > Math.Abs(bottom_rhs_first_pos - prev_forward_bottom_rhs_first_pos)
                     || DSDBR01.Instance._DSDBR01_SMBD_min_points_width_of_lm > Math.Abs(lhs_top_last_pos - prev_forward_lhs_top_last_pos)))
                    {
                        // lines cross each other => remove both
                        CGR_LOG.Write("CDSDBRSuperMode::checkForwardAndReverseLines() removing two forward lines that cross", LoggingLevels.INFO_CGR_LOG);
                        //results.deleteLine(cl);
                        //results._number_of_lines_removed++;
                        //results.deleteLine(prev_forward_line);
                        //results._number_of_lines_removed++;
                        //std::vector<CDSDBRSupermodeMapLine>::iterator i_prev_lm_boundary_line = i_lm_boundary_line;
                        int i_prev_lm_boundary_line = i_lm_boundary_line;
                        i_prev_lm_boundary_line--;
                        _lm_lower_lines.RemoveAt(i_lm_boundary_line);
                        _lm_lower_lines.RemoveAt(i_prev_lm_boundary_line);
                        prev_forward_line = not_a_value;
                        prev_forward_bottom_rhs_first_pos = not_a_value;
                        prev_forward_lhs_top_last_pos = not_a_value;
                        cl = 0;
                    }
                    else
                    {
                        prev_forward_line = line_num;
                        prev_forward_bottom_rhs_first_pos = bottom_rhs_first_pos;
                        prev_forward_lhs_top_last_pos = lhs_top_last_pos;
                        cl++;
                    }

                }
                else // reverse_upper
                {

                    //// find y and x values of first point in line
                    //std::vector<int>::iterator i_first_point = mbl->getX()->getIndices()->begin();
                    //int first_y = (*i_first_point)/ramp_length;
                    //int first_x = (*i_first_point) - first_y*ramp_length;
                    int first_y = _lm_upper_lines[i_lm_boundary_line]._points[0].row;
                    int first_x = _lm_upper_lines[i_lm_boundary_line]._points[0].col;
                    //first_x = (ramp_length-1)-first_x;

                    int bottom_rhs_first_pos;
                    if (first_y == 0) bottom_rhs_first_pos = (ramp_length - 1) - first_x;
                    else bottom_rhs_first_pos = first_y + ramp_length;


                    //std::vector<int>::iterator i_last_point = mbl->getX()->getIndices()->end();
                    //i_last_point--;
                    //// find y and x values of last point in line
                    //int last_y = (*i_last_point)/ramp_length;
                    //int last_x = (*i_last_point) - last_y*ramp_length;
                    int index_last = _lm_upper_lines[i_lm_boundary_line]._points.Count - 1;
                    int last_y = _lm_upper_lines[i_lm_boundary_line]._points[index_last].row;
                    int last_x = _lm_upper_lines[i_lm_boundary_line]._points[index_last].col;
                    //last_x = (ramp_length-1)-last_x;

                    int lhs_top_last_pos;
                    if (last_x == ramp_length - 1) lhs_top_last_pos = last_y;
                    else lhs_top_last_pos = last_y + (ramp_length - 1) - last_x;


                    if (prev_reverse_line != not_a_value && line_num > 0 && _lm_upper_lines.Count > 1 &&
                      (bottom_rhs_first_pos == prev_reverse_bottom_rhs_first_pos
                     || lhs_top_last_pos == prev_reverse_lhs_top_last_pos
                     || (bottom_rhs_first_pos - prev_reverse_bottom_rhs_first_pos) * (lhs_top_last_pos - prev_reverse_lhs_top_last_pos) < 0
                     || DSDBR01.Instance._DSDBR01_SMBD_min_points_width_of_lm > Math.Abs(bottom_rhs_first_pos - prev_reverse_bottom_rhs_first_pos)
                     || DSDBR01.Instance._DSDBR01_SMBD_min_points_width_of_lm > Math.Abs(lhs_top_last_pos - prev_reverse_lhs_top_last_pos)))
                    {
                        // lines cross each other => remove both
                        CGR_LOG.Write("CDSDBRSuperMode::checkForwardAndReverseLines() removing two reverse lines that cross", LoggingLevels.INFO_CGR_LOG);
                        //results.deleteLine(cl);
                        //results._number_of_lines_removed++;
                        //results.deleteLine(prev_reverse_line);
                        //results._number_of_lines_removed++;
                        //std::vector<CDSDBRSupermodeMapLine>::iterator i_prev_lm_boundary_line = i_lm_boundary_line;
                        int i_prev_lm_boundary_line = i_lm_boundary_line;
                        i_prev_lm_boundary_line--;
                        _lm_upper_lines.RemoveAt(i_lm_boundary_line);
                        _lm_upper_lines.RemoveAt(i_prev_lm_boundary_line);
                        prev_reverse_line = not_a_value;
                        prev_reverse_bottom_rhs_first_pos = not_a_value;
                        prev_reverse_lhs_top_last_pos = not_a_value;
                        cl = 0;
                    }
                    else
                    {
                        prev_reverse_line = line_num;
                        prev_reverse_bottom_rhs_first_pos = bottom_rhs_first_pos;
                        prev_reverse_lhs_top_last_pos = lhs_top_last_pos;
                        cl++;
                    }
                }

            }

            CGR_LOG.Write("CDSDBRSuperMode::checkForwardAndReverseLines() exiting", LoggingLevels.DIAGNOSTIC_CGR_LOG);
        }

        public void associateForwardAndReverseLines()
        {
            CGR_LOG.Write("CDSDBRSuperMode::associateForwardAndReverseLines() entered", LoggingLevels.DIAGNOSTIC_CGR_LOG);

            int ramp_length;
            int steps_count;

            if (_Im_ramp) // middle line ramped
            {
                steps_count = (int)(_phase_current._currents.Count);
                ramp_length = (int)(_filtered_middle_line._points.Count);
            }
            else // phase ramped
            {
                steps_count = (int)(_filtered_middle_line._points.Count);
                ramp_length = (int)(_phase_current._currents.Count);
            }

            // match up the lines on both forward and reverse maps

            // store first and last point in each line in these maps
            boundary_multimap_typedef forward_firstpoints = new boundary_multimap_typedef();
            boundary_multimap_typedef forward_lastpoints = new boundary_multimap_typedef();
            boundary_multimap_typedef reverse_firstpoints = new boundary_multimap_typedef();
            boundary_multimap_typedef reverse_lastpoints = new boundary_multimap_typedef();

            for (int cl = 0; cl < (int)(_lm_lower_lines.Count + _lm_upper_lines.Count); cl++)
            { // iterate through lines

                //ModeBoundaryLine* mbl = results.getLine( cl );

                //std::vector<CDSDBRSupermodeMapLine>::iterator i_lm_boundary_line;
                int i_lm_boundary_line;
                bool forward_lower = true;
                int line_num = 0;

                if (cl < (int)(_lm_lower_lines.Count))
                {
                    //i_lm_boundary_line = _lm_lower_lines.begin();
                    i_lm_boundary_line = 0;
                    line_num = cl;
                    i_lm_boundary_line += line_num;
                    forward_lower = true;
                }
                else
                {
                    //i_lm_boundary_line = _lm_upper_lines.begin();
                    i_lm_boundary_line = 0;
                    line_num = (cl - (int)(_lm_lower_lines.Count));
                    i_lm_boundary_line += line_num;
                    forward_lower = false;
                }

                if (forward_lower)
                {
                    int row_first_pt = _lm_lower_lines[i_lm_boundary_line]._points[0].row;
                    int col_first_pt = _lm_lower_lines[i_lm_boundary_line]._points[0].col;
                    int index_first_pt = col_first_pt + row_first_pt * ramp_length;

                    forward_firstpoints.Insert(
                        new KeyValuePair<int, int>(index_first_pt, cl)
                        );
                    //printf("\n forward_firstpoints %d",*(mbl->getX()->getIndices()->begin()));

                    //std::vector<int>::iterator i_last_point = mbl->getX()->getIndices()->end();
                    //i_last_point--;

                    //std::vector<CDSDBRSupermodeMapLine::point_type>::iterator i_last_pt = i_lm_boundary_line->_points.end();
                    int i_last_pt = _lm_lower_lines[i_lm_boundary_line]._points.Count;
                    i_last_pt--;
                    int row_last_pt = _lm_lower_lines[i_lm_boundary_line]._points[i_last_pt].row;
                    int col_last_pt = _lm_lower_lines[i_lm_boundary_line]._points[i_last_pt].col;
                    int index_last_pt = col_last_pt + row_last_pt * ramp_length;

                    forward_lastpoints.Insert(
                        new KeyValuePair<int, int>(index_last_pt, cl)
                        );
                    //printf("\n forward_lastpoints %d",*i_last_point);
                }
                else // reverse_upper
                {
                    int row_first_pt = _lm_upper_lines[i_lm_boundary_line]._points[0].row;
                    int col_first_pt = _lm_upper_lines[i_lm_boundary_line]._points[0].col;
                    int index_first_pt = col_first_pt + row_first_pt * ramp_length;

                    reverse_firstpoints.Insert(
                        new KeyValuePair<int, int>(index_first_pt, cl)
                        );

                    //std::vector<CDSDBRSupermodeMapLine::point_type>::iterator i_last_pt = i_lm_boundary_line->_points.end();
                    int i_last_pt = _lm_upper_lines[i_lm_boundary_line]._points.Count;
                    i_last_pt--;
                    int row_last_pt = _lm_upper_lines[i_lm_boundary_line]._points[i_last_pt].row;
                    int col_last_pt = _lm_upper_lines[i_lm_boundary_line]._points[i_last_pt].col;
                    int index_last_pt = col_last_pt + row_last_pt * ramp_length;

                    //printf("\n reverse_firstpoints %d",*(mbl->getX()->getIndices()->begin()));
                    //std::vector<int>::iterator i_last_point = mbl->getX()->getIndices()->end();
                    //i_last_point--;
                    reverse_lastpoints.Insert(
                        new KeyValuePair<int, int>(index_last_pt, cl)
                        );
                    //printf("\n reverse_lastpoints %d",*i_last_point);
                }

            }

            // store rhs and lhs points in the mode below each line in these maps
            boundary_multimap_typedef forward_rhs_points = new boundary_multimap_typedef();
            boundary_multimap_typedef forward_lhs_points = new boundary_multimap_typedef();
            boundary_multimap_typedef reverse_rhs_points = new boundary_multimap_typedef();
            boundary_multimap_typedef reverse_lhs_points = new boundary_multimap_typedef();

            // find points between forward lines' last points along RHS
            int prev_y = 0;             // start at bottom-right corner
            int prev_x = ramp_length - 1; // start at bottom-right corner

            //for( boundary_multimap_typedef::iterator i_flp = forward_lastpoints.begin() ;
            //     i_flp != forward_lastpoints.end() ; i_flp++ )
            for (int i_flp = 0; i_flp < forward_lastpoints.Count; i_flp++)
            {
                int last_point = forward_lastpoints[i_flp].Key;
                int line_number = forward_lastpoints[i_flp].Value;
                //printf("\n <%d,%d>",last_point,line_number);

                int y = (int)(last_point / ramp_length);
                int x = last_point - y * ramp_length;

                if (x == ramp_length - 1)
                { // last point is on RHS

                    int prev_last_point = ramp_length - 1; // start at bottom-right corner
                    for (int i = last_point - ramp_length; i > 0; i--)
                    { // find last point of next line down
                        //boundary_multimap_typedef::iterator fi = forward_lastpoints.find(i);
                        int fi = forward_lastpoints.Find(i);
                        if (fi != forward_lastpoints.Count)
                        { // found last point of next line down
                            //printf("\n found prev_last_point = %d", (*fi).first);
                            prev_last_point = forward_lastpoints[fi].Key;
                            break;
                        }
                    }
                    prev_y = (int)(prev_last_point / ramp_length);

                    //printf("\nforward_rhs %d ",line_number);

                    for (int mode_point_y = prev_y + 1; mode_point_y <= y; mode_point_y++)
                    { // iterate through points from previous line to current line on RHS
                        int mode_point =
                            (ramp_length - 1) + mode_point_y * ramp_length;
                        forward_rhs_points.Insert(
                            new KeyValuePair<int, int>(mode_point, line_number));
                        //printf(" <%d,%d>",mode_point,line_number);
                    }
                }
            }

            // find points between forward lines' first points along LHS
            prev_y = 0; // start at bottom-left corner
            prev_x = 0; // start at bottom-left corner
            //   for( boundary_multimap_typedef::iterator i_ffp = forward_firstpoints.begin() ;
            //         i_ffp != forward_firstpoints.end() ; i_ffp++ )
            for (int i_ffp = 0; i_ffp < forward_firstpoints.Count; i_ffp++)
            {
                int first_point = forward_firstpoints[i_ffp].Key;
                int line_number = forward_firstpoints[i_ffp].Value;
                //printf("\n <%d,%d>",first_point,line_number);

                int y = (int)(first_point / ramp_length);
                int x = first_point - y * ramp_length;

                if (x == 0)
                { // first point is on LHS

                    int prev_first_point = 0; // start at bottom-left corner
                    for (int i = first_point - ramp_length; i > 0; i--)
                    { // find first point of next line down
                        //boundary_multimap_typedef::iterator fi = forward_firstpoints.find(i);
                        int fi = forward_firstpoints.Find(i);
                        if (fi != forward_firstpoints.Count)
                        { // found first point of next line down
                            //printf("\n found prev_first_point = %d", (*fi).first);
                            prev_first_point = forward_firstpoints[fi].Key;
                            break;
                        }
                    }
                    prev_y = (int)(prev_first_point / ramp_length);

                    //printf("\nforward_lhs %d ",line_number);

                    for (int mode_point_y = prev_y + 1; mode_point_y <= y; mode_point_y++)
                    { // iterate through points from previous line to current line on LHS
                        int mode_point = mode_point_y * ramp_length;
                        forward_lhs_points.Insert(
                            new KeyValuePair<int, int>(mode_point, line_number));
                        //printf(" <%d,%d>",mode_point,line_number);
                    }
                }
            }

            // find points between reverse lines' last points along RHS
            prev_y = 0;             // start at bottom-right corner
            prev_x = ramp_length - 1; // start at bottom-right corner

            //for( boundary_multimap_typedef::iterator i_rlp = reverse_lastpoints.begin() ;
            //     i_rlp != reverse_lastpoints.end() ; i_rlp++ )
            for (int i_rlp = 0; i_rlp < reverse_lastpoints.Count; i_rlp++)
            {
                int last_point = reverse_lastpoints[i_rlp].Key;
                int line_number = reverse_lastpoints[i_rlp].Value;
                //printf("\n <%d,%d>",last_point,line_number);

                int y = (int)(last_point / ramp_length);
                int x = last_point - y * ramp_length;

                if (x == ramp_length - 1) //0 )
                { // last point is on RHS

                    int prev_last_point = 0; // start at bottom-right corner
                    for (int i = last_point - ramp_length; i > 0; i--)
                    { // find last point of next line down
                        //boundary_multimap_typedef::iterator fi = reverse_lastpoints.find(i);
                        int fi = reverse_lastpoints.Find(i);
                        if (fi != reverse_lastpoints.Count)
                        { // found last point of next line down
                            //printf("\n found prev_last_point = %d", (*fi).first);
                            prev_last_point = reverse_lastpoints[fi].Key;
                            break;
                        }
                    }
                    prev_y = (int)(prev_last_point / ramp_length);

                    //printf("\nreverse_rhs %d ",line_number);

                    for (int mode_point_y = prev_y + 1; mode_point_y <= y; mode_point_y++)
                    { // iterate through points from previous line to current line on RHS
                        // also, convert from reverse indexing to forward indexing
                        int mode_point =
                            (ramp_length - 1) + mode_point_y * ramp_length;
                        reverse_rhs_points.Insert(
                            new KeyValuePair<int, int>(mode_point, line_number));
                        //printf(" <%d,%d>",mode_point,line_number);
                    }
                }
            }

            // find points between reverse lines' first points along LHS
            prev_y = 0; // start at bottom-left corner
            prev_x = 0; // start at bottom-left corner

            //for( boundary_multimap_typedef::iterator i_rfp = reverse_firstpoints.begin() ;
            //     i_rfp != reverse_firstpoints.end() ; i_rfp++ )
            for (int i_rfp = 0; i_rfp < reverse_firstpoints.Count; i_rfp++)
            {
                int first_point = reverse_firstpoints[i_rfp].Key;
                int line_number = reverse_firstpoints[i_rfp].Value;
                //printf("\n <%d,%d>",first_point,line_number);

                int y = (int)(first_point / ramp_length);
                int x = first_point - y * ramp_length;

                if (x == 0) //ramp_length-1 )
                { // first point is on LHS

                    int prev_first_point = ramp_length - 1; // start at bottom-left corner
                    for (int i = first_point - ramp_length; i > 0; i--)
                    { // find first point of next line down
                        //boundary_multimap_typedef::iterator fi = reverse_firstpoints.find(i);
                        int fi = reverse_firstpoints.Find(i);
                        if (fi != reverse_firstpoints.Count)
                        { // found first point of next line down
                            //printf("\n found prev_first_point = %d", (*fi).first);
                            prev_first_point = reverse_firstpoints[fi].Key;
                            break;
                        }
                    }
                    prev_y = (int)(prev_first_point / ramp_length);

                    //printf("\nreverse_lhs %d ",line_number);

                    for (int mode_point_y = prev_y + 1; mode_point_y <= y; mode_point_y++)
                    { // iterate through points from previous line to current line on LHS
                        // also, convert from reverse indexing to forward indexing
                        int mode_point = mode_point_y * ramp_length;
                        reverse_lhs_points.Insert(
                            new KeyValuePair<int, int>(mode_point, line_number));
                        //printf(" <%d,%d>",mode_point,line_number);
                    }
                }
            }

            // iterate through the forward mode lines
            // for each line, find the points on the forward LHS and RHS
            // then for each of these points, find their line numbers
            // on the reverse LHS and RHS
            // find the lines with the most points in common
            // if it's more than a percentage calculated below, consider the modes matched

            //for(int cl = 0 ; cl < results.countLines() ; cl++ )
            //{ // iterate through forward lines only
            //    if( results.getLine( cl )->getLineType() & MBL_FORWARD )
            //    {

            for (int cl = 0; cl < (int)(_lm_lower_lines.Count + _lm_upper_lines.Count); cl++)
            { // iterate through lines

                //ModeBoundaryLine* mbl = results.getLine( cl );

                //std::vector<CDSDBRSupermodeMapLine>::iterator i_lm_boundary_line;
                int i_lm_boundary_line;
                bool forward_lower = true;
                int line_num = 0;

                if (cl < (int)(_lm_lower_lines.Count))
                {
                    //i_lm_boundary_line = _lm_lower_lines.begin();
                    i_lm_boundary_line = 0;
                    line_num = cl;
                    i_lm_boundary_line += line_num;
                    forward_lower = true;
                }
                else
                {
                    //i_lm_boundary_line = _lm_upper_lines.begin();
                    i_lm_boundary_line = 0;
                    line_num = (cl - (int)(_lm_lower_lines.Count));
                    i_lm_boundary_line += line_num;
                    forward_lower = false;
                }

                if (forward_lower)
                {

                    // build up a vector of containing the reverse line numbers for the same mode points
                    List<int> reverse_line_numbers = new List<int>();

                    // find reverse line numbers for the same mode points on RHS
                    //printf("\n find reverse line numbers for the same mode points on RHS");
                    //for( boundary_multimap_typedef::iterator i_frhs = forward_rhs_points.begin();
                    //     i_frhs != forward_rhs_points.end() ; i_frhs++ )
                    for (int i_frhs = 0; i_frhs < forward_rhs_points.Count; i_frhs++)
                    {
                        if (forward_rhs_points[i_frhs].Value == cl)
                        { // found point associated with this line
                            // find the same point's line number on the reverse map
                            //boundary_multimap_typedef::iterator i_rrhs
                            //    = reverse_rhs_points.find( (*i_frhs).first );
                            int i_rrhs = reverse_rhs_points.Find(forward_rhs_points[i_frhs].Key);
                            if (i_rrhs != reverse_rhs_points.Count)
                            { // found the same point in reverse RHS
                                reverse_line_numbers.Add(reverse_rhs_points[i_rrhs].Value);
                                //printf(" %d", (*i_rrhs).second );
                            }
                        }
                    }

                    // find reverse line numbers for the same mode points on LHS
                    //printf("\n find reverse line numbers for the same mode points on LHS");
                    //for( boundary_multimap_typedef::iterator i_flhs = forward_lhs_points.begin();
                    //     i_flhs != forward_lhs_points.end() ; i_flhs++ )
                    for (int i_flhs = 0; i_flhs < forward_lhs_points.Count; i_flhs++)
                    {
                        if (forward_lhs_points[i_flhs].Value == cl)
                        { // found point associated with this line
                            // find the same point's line number on the reverse map
                            //boundary_multimap_typedef::iterator i_rlhs
                            //    = reverse_lhs_points.find( (*i_flhs).first );
                            int i_rlhs = reverse_lhs_points.Find(forward_lhs_points[i_flhs].Key);
                            if (i_rlhs != reverse_lhs_points.Count)
                            { // found the same point in reverse LHS
                                reverse_line_numbers.Add(reverse_lhs_points[i_rlhs].Value);
                                //printf(" %d", (*i_rlhs).second );
                            }
                        }
                    }

                    // find the most commonly occuring reverse line number 
                    int most_common_number = 0;
                    int most_common_number_count = 0;
                    for (int rln = 0; rln < (int)(_lm_lower_lines.Count + _lm_upper_lines.Count); rln++)
                    {
                        // count the occurances of rln
                        int rln_count = 0;
                        for (int i_rln = 0; i_rln < reverse_line_numbers.Count; i_rln++)
                            if (reverse_line_numbers[i_rln] == rln) rln_count++;

                        if (rln_count > most_common_number_count)
                        { // rln is the most common number found so far
                            most_common_number = rln;
                            most_common_number_count = rln_count;
                        }
                    }

                    if (reverse_line_numbers.Count > 1 &&
                        100 * most_common_number_count / ((int)(reverse_line_numbers.Count)) >= 50)
                    { // 50% of points matched => treat the lines as matched

                        // check this has been matched already, if so, ignore it
                        //matchedlines_typedef iterator i_m;
                        int i_m;
                        //for( i_m = _matched_line_numbers.begin();
                        //     i_m != _matched_line_numbers.end() ; i_m++ )
                        for (i_m = 0; i_m < _matched_line_numbers.Count; i_m++)
                        {
                            if (_matched_line_numbers[i_m].Value == most_common_number) break;
                        }

                        if (i_m == _matched_line_numbers.Count)
                        { // the line found ahs not already been matched to another line
                            _matched_line_numbers.Insert(
                                new KeyValuePair<int, int>(cl, most_common_number));
                            // printf("\n <%d,%d>",cl , most_common_number );
                        }
                    }
                }
            }

            CGR_LOG.Write("CDSDBRSuperMode::associateForwardAndReverseLines() exiting", LoggingLevels.DIAGNOSTIC_CGR_LOG);

        }

        public void removeUnassociatedLines()
        {
            CGR_LOG.Write("CDSDBRSuperMode::removeUnassociatedLines() entered", LoggingLevels.DIAGNOSTIC_CGR_LOG);

            // erase lines from vectors that have
            // not successfully associate forward with matching reverse

            List<int> lower_line_numbers = new List<int>();
            List<int> upper_line_numbers = new List<int>();

            int _lm_lower_lines_count = (int)(_lm_lower_lines.Count);

            // find all line numbers to begin with

            for (int i = 0; i < _lm_lower_lines_count; i++)
            {
                lower_line_numbers.Add(i);
            }

            for (int i = 0; i < (int)(_lm_upper_lines.Count); i++)
            {
                upper_line_numbers.Add(i + _lm_lower_lines_count);
            }

            //matchedlines_typedef::iterator i_f;
            int i_f;
            for (i_f = 0; i_f < _matched_line_numbers.Count; i_f++)
            {
                // erase known matched line numbers from lists
                // of all forward and reverse line numbers

                //std::vector<int>::iterator i_lower_m
                //    = std::find( lower_line_numbers.begin(), lower_line_numbers.end(), (*i_f).first );
                int i_lower_m = lower_line_numbers.IndexOf(_matched_line_numbers[i_f].Key);
                if (i_lower_m == -1) i_lower_m = lower_line_numbers.Count;

                //std::vector<int>::iterator i_upper_m
                //    = std::find( upper_line_numbers.begin(), upper_line_numbers.end(), (*i_f).second );
                int i_upper_m = upper_line_numbers.IndexOf(_matched_line_numbers[i_f].Value);
                if (i_upper_m == -1) i_upper_m = upper_line_numbers.Count;

                if (i_lower_m != lower_line_numbers.Count)
                    lower_line_numbers.RemoveAt(i_lower_m);

                if (i_upper_m != upper_line_numbers.Count)
                    upper_line_numbers.RemoveAt(i_upper_m);
            }


            // Any line numbers remaining are unmatched -> erase them.
            // Erase in reverse order so indexing doesn't move things around.

            for (int i = (int)(lower_line_numbers.Count) - 1; i >= 0; i--)
            {
                //std::vector<CDSDBRSupermodeMapLine>::iterator i_ll = _lm_lower_lines.begin();
                int i_ll = 0;
                int num = lower_line_numbers[i];
                i_ll += lower_line_numbers[i];

                //i_ll->clear();
                _lm_lower_lines[i_ll].clear();
                _lm_lower_lines.RemoveAt(i_ll);

                // need to move indexing in _matched_line_numbers accordingly
                matchedlines_typedef new_matched_line_numbers = new matchedlines_typedef();
                //matchedlines_typedef::iterator i_m;
                int i_m;
                new_matched_line_numbers.Clear();
                for (i_m = 0; i_m < _matched_line_numbers.Count; i_m++)
                {
                    if (_matched_line_numbers[i_m].Key > lower_line_numbers[i])
                    {
                        new_matched_line_numbers.Insert(
                            new KeyValuePair<int, int>(_matched_line_numbers[i_m].Key - 1, _matched_line_numbers[i_m].Value));
                    }
                    else
                    {
                        new_matched_line_numbers.Insert(_matched_line_numbers[i_m]);
                    }
                }
                _matched_line_numbers.Clear();
                //for( i_m = new_matched_line_numbers.begin();i_m != new_matched_line_numbers.end() ; i_m++ )
                for (i_m = 0; i_m < new_matched_line_numbers.Count; i_m++)
                {
                    _matched_line_numbers.Insert(new_matched_line_numbers[i_m]);
                }
            }

            for (int i = upper_line_numbers.Count - 1; i >= 0; i--)
            {
                //std::vector<CDSDBRSupermodeMapLine>::iterator i_ul = _lm_upper_lines.begin();
                int i_ul = 0;
                int num = upper_line_numbers[i] - (int)(_lm_lower_lines_count);
                i_ul += upper_line_numbers[i] - (int)(_lm_lower_lines_count);

                //i_ul->clear();
                _lm_upper_lines[i_ul].clear();
                _lm_upper_lines.RemoveAt(i_ul);


                // need to move indexing in _matched_line_numbers accordingly
                matchedlines_typedef new_matched_line_numbers = new matchedlines_typedef();
                //matchedlines_typedef::iterator i_m;
                int i_m;
                new_matched_line_numbers.Clear();
                //for( i_m = _matched_line_numbers.begin() ;i_m != _matched_line_numbers.end() ; i_m++ )
                for (i_m = 0; i_m < _matched_line_numbers.Count; i_m++)
                {
                    if (_matched_line_numbers[i_m].Value > upper_line_numbers[i])
                    {
                        new_matched_line_numbers.Insert(
                            new KeyValuePair<int, int>(_matched_line_numbers[i_m].Key, _matched_line_numbers[i_m].Value - 1));
                    }
                    else
                    {
                        new_matched_line_numbers.Insert(_matched_line_numbers[i_m]);
                    }
                }
                _matched_line_numbers.Clear();
                //for( i_m = new_matched_line_numbers.begin() ;i_m != new_matched_line_numbers.end() ; i_m++ )
                for (i_m = 0; i_m < new_matched_line_numbers.Count; i_m++)
                {
                    _matched_line_numbers.Insert(new_matched_line_numbers[i_m]);
                }
            }

            CGR_LOG.Write("CDSDBRSuperMode::removeUnassociatedLines() exiting", LoggingLevels.DIAGNOSTIC_CGR_LOG);

        }

        public CDSDBRSupermodeMapLine.point_type convertRowCol2LinePoint(int row, int col) 
        {
            CDSDBRSupermodeMapLine.point_type point_on_map = new CDSDBRSupermodeMapLine.point_type();

            point_on_map.row = row;
            point_on_map.col = col;
            point_on_map.real_row = (double)row;
            point_on_map.real_col = (double)col;

            if (_Im_ramp)
            {
                point_on_map.front_pair_number = _filtered_middle_line._points[col].front_pair_number;
                point_on_map.I_rear = _filtered_middle_line._points[col].I_rear;
                point_on_map.I_front_1 = _filtered_middle_line._points[col].I_front_1;
                point_on_map.I_front_2 = _filtered_middle_line._points[col].I_front_2;
                point_on_map.I_front_3 = _filtered_middle_line._points[col].I_front_3;
                point_on_map.I_front_4 = _filtered_middle_line._points[col].I_front_4;
                point_on_map.I_front_5 = _filtered_middle_line._points[col].I_front_5;
                point_on_map.I_front_6 = _filtered_middle_line._points[col].I_front_6;
                point_on_map.I_front_7 = _filtered_middle_line._points[col].I_front_7;
                point_on_map.I_front_8 = _filtered_middle_line._points[col].I_front_8;

                point_on_map.I_phase = _phase_current._currents[row];
            }
            else
            {
                point_on_map.front_pair_number = _filtered_middle_line._points[row].front_pair_number;
                point_on_map.I_rear = _filtered_middle_line._points[row].I_rear;
                point_on_map.I_front_1 = _filtered_middle_line._points[row].I_front_1;
                point_on_map.I_front_2 = _filtered_middle_line._points[row].I_front_2;
                point_on_map.I_front_3 = _filtered_middle_line._points[row].I_front_3;
                point_on_map.I_front_4 = _filtered_middle_line._points[row].I_front_4;
                point_on_map.I_front_5 = _filtered_middle_line._points[row].I_front_5;
                point_on_map.I_front_6 = _filtered_middle_line._points[row].I_front_6;
                point_on_map.I_front_7 = _filtered_middle_line._points[row].I_front_7;
                point_on_map.I_front_8 = _filtered_middle_line._points[row].I_front_8;

                point_on_map.I_phase = _phase_current._currents[col];
            }

            return point_on_map;
        }

        public rcode writeMatchedLinesToFile(
            string forward_abs_filepath, string reverse_abs_filepath)
        {
            CGR_LOG.Write("CDSDBRSuperMode::writeMatchedLinesToFile() entered", LoggingLevels.DIAGNOSTIC_CGR_LOG);
            rcode rval = rcode.ok;

            int ramp_length;
            int steps_count;
            if (_Im_ramp) // middle line ramped
            {
                steps_count = (int)(_phase_current._currents.Count);
                ramp_length = (int)(_filtered_middle_line._points.Count);
            }
            else // phase ramped
            {
                steps_count = (int)(_filtered_middle_line._points.Count);
                ramp_length = (int)(_phase_current._currents.Count);
            }

            // File does not exist => okay to create it.
            using (StreamWriter f_strm = new StreamWriter(forward_abs_filepath))
            using (StreamWriter r_strm = new StreamWriter(reverse_abs_filepath))
            {
                int line_count = (int)(_lm_lower_lines.Count + _lm_upper_lines.Count);
                for (int i = 0; i < line_count; i++)
                {
                    //std::vector<CDSDBRSupermodeMapLine>::iterator i_lm_boundary_line;
                    int i_lm_boundary_line;
                    bool forward_lower = true;
                    int line_num = 0;

                    if (i < (int)(_lm_lower_lines.Count))
                    {
                        //i_lm_boundary_line = _lm_lower_lines.begin();
                        i_lm_boundary_line = 0;
                        line_num = i;
                        i_lm_boundary_line += line_num;
                        forward_lower = true;
                    }
                    else
                    {
                        //i_lm_boundary_line = _lm_upper_lines.begin();
                        i_lm_boundary_line = 0;
                        line_num = (i - (int)(_lm_lower_lines.Count));
                        i_lm_boundary_line += line_num;
                        forward_lower = false;
                    }

                    //ModeBoundaryLine* p_mbl = results.getLine(i);
                    //std::vector<int>* p_indices = p_mbl->getX()->getIndices();
                    int line_length = forward_lower ? (int)_lm_lower_lines[i_lm_boundary_line]._points.Count
                        : (int)_lm_upper_lines[i_lm_boundary_line]._points.Count;

                    if (forward_lower) //p_mbl->getLineType() & MBL_FORWARD ) // line is on forward mode map
                    {
                        for (int j = 0; j < line_length; j++)
                        {
                            int row = _lm_lower_lines[i_lm_boundary_line]._points[j].row;
                            int col = _lm_lower_lines[i_lm_boundary_line]._points[j].col;
                            int index = col + row * ramp_length;
                            //f_strm << index << ", " << i << "\n";
                            f_strm.WriteLine(index + ", " + i);
                        }
                    }
                    else // line is on reverse mode map
                    {
                        // find number of the matching forward mode line
                        int i_f;
                        //for (i_f = _matched_line_numbers.begin();i_f != _matched_line_numbers.end(); i_f++)
                        for (i_f = 0; i_f < _matched_line_numbers.Count; i_f++)
                        {
                            if (_matched_line_numbers[i_f].Value == i)
                            {
                                break;
                            }
                        }
                        for (int j = 0; j < line_length; j++)
                        {

                            int row = _lm_upper_lines[i_lm_boundary_line]._points[j].row;
                            int col = _lm_upper_lines[i_lm_boundary_line]._points[j].col;
                            int index = col + row * ramp_length;
                            //r_strm << index;
                            r_strm.Write(index);
                            if (i_f != _matched_line_numbers.Count) // found matching line
                                //r_strm << ", " << (*i_f).first << "\n";
                                r_strm.WriteLine(", " + _matched_line_numbers[i_f].Key);
                            else // no matching line found
                                //r_strm << ", " << i << "\n";
                                r_strm.WriteLine(", " + i);
                        }
                    }
                }
            }
            CGR_LOG.Write("CDSDBRSuperMode::writeMatchedLinesToFile() exiting", LoggingLevels.DIAGNOSTIC_CGR_LOG);

            return rval;
        }

        public rcode findLMMiddleLines()
        {
            CGR_LOG.Write("CDSDBRSuperMode.findLMMiddleLines() entered", LoggingLevels.DIAGNOSTIC_CGR_LOG);

            rcode rval = rcode.ok;

            // clear all LM middle lines,
            // filtered middle lines and
            // middle lines of frequency

            for (int i = 0; i < (int)(_lm_middle_lines.Count); i++)
            {
                _lm_middle_lines[i].clear();
            }
            _lm_middle_lines.Clear();

            for (int i = 0; i < (int)(_lm_filtered_middle_lines.Count); i++)
            {
                _lm_filtered_middle_lines[i].clear();
            }
            _lm_filtered_middle_lines.Clear();

            for (int i = 0; i < (int)(_lm_middle_lines_of_frequency.Count); i++)
            {
                _lm_middle_lines_of_frequency[i].clear();
            }
            _lm_middle_lines_of_frequency.Clear();

            _average_widths.Clear();

            int count_lm_lines = (int)(_lm_lower_lines.Count);

            if (count_lm_lines == (int)(_lm_upper_lines.Count))
            {

                for (int lm = 0; lm < count_lm_lines - 1; lm++)
                {
                    // iterate through the boundary lines of each longitudinal mode

                    //std.vector<CDSDBRSupermodeMapLine>.iterator i_lower_line = _lm_upper_lines.begin();
                    int i_lower_line = 0;
                    i_lower_line += lm;

                    //std.vector<CDSDBRSupermodeMapLine>.iterator i_upper_line = _lm_lower_lines.begin();
                    int i_upper_line = 0;
                    i_upper_line += (lm + 1);

                    CDSDBRSupermodeMapLine new_lm_middle_line = new CDSDBRSupermodeMapLine();
                    _lm_middle_lines.Add(new_lm_middle_line);

                    bool middle_point_found_at_col_zero = false;
                    bool middle_point_found_at_last_col = false;

                    // if right-most column covered is not last column in map
                    //   find midpoint on rhs-top border
                    //   plot line from right-most point found to midpoint
                    //   add points along line to middle line
                    // if left-most column covered is not first column in map
                    //   find midpoint on lhs_bottom border
                    //   plot line from left-most point found to lhs_bottom border
                    //   add points along line to middle line

                    int widths_total = 0;
                    int widths_count = 0;

                    for (int i = 0; i < _cols; i++)
                    {
                        // for each column

                        int highest_row_at_i_on_lower_line = -1;
                        int index_of_highest_row_at_i_on_lower_line = -1;
                        int lowest_row_at_i_on_lower_line = _rows;
                        int index_of_lowest_row_at_i_on_lower_line = -1;

                        // find highest and lowest points on lower line at column i

                        for (int j = 0; j < (int)_lm_upper_lines[i_lower_line]._points.Count; j++)
                        {
                            if (_lm_upper_lines[i_lower_line]._points[j].col == i
                            && _lm_upper_lines[i_lower_line]._points[j].row > highest_row_at_i_on_lower_line)
                            {
                                index_of_highest_row_at_i_on_lower_line = j;
                                highest_row_at_i_on_lower_line = _lm_upper_lines[i_lower_line]._points[j].row;
                            }
                            if (_lm_upper_lines[i_lower_line]._points[j].col == i
                            && _lm_upper_lines[i_lower_line]._points[j].row < lowest_row_at_i_on_lower_line)
                            {
                                index_of_lowest_row_at_i_on_lower_line = j;
                                lowest_row_at_i_on_lower_line = _lm_upper_lines[i_lower_line]._points[j].row;
                            }
                        }

                        int highest_row_at_i_on_upper_line = -1;
                        int index_of_highest_row_at_i_on_upper_line = -1;
                        int lowest_row_at_i_on_upper_line = _rows;
                        int index_of_lowest_row_at_i_on_upper_line = -1;

                        // find highest and lowest point on upper line at column i

                        for (int j = 0; j < (int)_lm_lower_lines[i_upper_line]._points.Count; j++)
                        {
                            if (_lm_lower_lines[i_upper_line]._points[j].col == i
                            && _lm_lower_lines[i_upper_line]._points[j].row > highest_row_at_i_on_upper_line)
                            {
                                index_of_highest_row_at_i_on_upper_line = j;
                                highest_row_at_i_on_upper_line = _lm_lower_lines[i_upper_line]._points[j].row;
                            }
                            if (_lm_lower_lines[i_upper_line]._points[j].col == i
                            && _lm_lower_lines[i_upper_line]._points[j].row < lowest_row_at_i_on_upper_line)
                            {
                                index_of_lowest_row_at_i_on_upper_line = j;
                                lowest_row_at_i_on_upper_line = _lm_lower_lines[i_upper_line]._points[j].row;
                            }
                        }

                        if (index_of_highest_row_at_i_on_lower_line != -1
                        && index_of_lowest_row_at_i_on_upper_line != -1)
                        {
                            if (i == 0) middle_point_found_at_col_zero = true;
                            if (i == _cols - 1) middle_point_found_at_last_col = true;

                            // if both found, check min and max separation distances
                            int current_min_width = lowest_row_at_i_on_upper_line - highest_row_at_i_on_lower_line;
                            int current_max_width = highest_row_at_i_on_upper_line - lowest_row_at_i_on_lower_line;

                            if (current_min_width >= (int)(DSDBR01.Instance._DSDBR01_SMBD_min_points_width_of_lm)
                             && current_max_width <= (int)(DSDBR01.Instance._DSDBR01_SMBD_max_points_width_of_lm))
                            {

                                widths_total += current_min_width;
                                widths_count++;

                                // find vertical midpoint

                                int row_of_midpoint = highest_row_at_i_on_lower_line + (int)(((double)current_min_width) / 2);

                                //     add to middle line
                                _lm_middle_lines[lm]._points.Add(convertRowCol2LinePoint(row_of_midpoint, i));
                            }
                            else
                            {
                                // supermode min width not met, remove all points
                                _lm_middle_lines[lm].clear();
                                break;
                            }
                        }
                    }

                    if (_lm_middle_lines[lm]._points.Count > 0)
                        _average_widths.Add((double)widths_total / (double)widths_count);
                    else
                        _average_widths.Add(0);


                    if (!middle_point_found_at_col_zero
                    && !(_lm_middle_lines[lm]._points.Count == 0))
                    {
                        // need to fill in the start of the middle line to the lhs-bottom border
                        // find middle start point
                        // calculate points on straight line fit first known middle point
                        // and add to start of middle line

                        //std.vector<CDSDBRSupermodeMapLine.point_type>.iterator lower_border_pt = i_lower_line->_points.end();
                        int lower_border_pt = _lm_upper_lines[i_lower_line]._points.Count;

                        for (int j = 0; j < (int)_lm_upper_lines[i_lower_line]._points.Count; j++)
                        {
                            if ((_lm_upper_lines[i_lower_line]._points[j].col == 0
                            || _lm_upper_lines[i_lower_line]._points[j].row == 0)
                            && lower_border_pt == _lm_upper_lines[i_lower_line]._points.Count)
                            {
                                // found point on lhs-bottom border
                                //lower_border_pt = i_lower_line->_points.begin();
                                lower_border_pt = 0;
                                lower_border_pt += j;
                                break;
                            }
                        }

                        //std.vector<CDSDBRSupermodeMapLine.point_type>.iterator upper_border_pt = i_upper_line->_points.end();
                        int upper_border_pt = _lm_lower_lines[i_upper_line]._points.Count;

                        for (int j = 0; j < (int)(_lm_lower_lines[i_upper_line]._points.Count); j++)
                        {
                            if ((_lm_lower_lines[i_upper_line]._points[j].col == 0
                            || _lm_lower_lines[i_upper_line]._points[j].row == 0)
                            && upper_border_pt == _lm_lower_lines[i_upper_line]._points.Count)
                            {
                                // found point on lhs-bottom border
                                //upper_border_pt = i_upper_line->_points.begin();
                                upper_border_pt = 0;
                                upper_border_pt += j;
                                break;
                            }
                        }

                        if (lower_border_pt != _lm_upper_lines[i_lower_line]._points.Count && upper_border_pt != _lm_lower_lines[i_upper_line]._points.Count)
                        {
                            // Have upper and lower border points, find midpoint

                            int lower_dist_from_origin;
                            if (_lm_upper_lines[i_lower_line]._points[lower_border_pt].col == 0) lower_dist_from_origin = -_lm_upper_lines[i_lower_line]._points[lower_border_pt].row;
                            else lower_dist_from_origin = _lm_upper_lines[i_lower_line]._points[lower_border_pt].col;

                            int upper_dist_from_origin;
                            if (_lm_lower_lines[i_upper_line]._points[upper_border_pt].col == 0) upper_dist_from_origin = -_lm_lower_lines[i_upper_line]._points[upper_border_pt].row;
                            else upper_dist_from_origin = _lm_lower_lines[i_upper_line]._points[upper_border_pt].col;

                            int border_midpoint_row;
                            int border_midpoint_col;
                            int border_midpoint_dist_from_origin = lower_dist_from_origin - (lower_dist_from_origin - upper_dist_from_origin) / 2;
                            if (border_midpoint_dist_from_origin > 0)
                            {
                                border_midpoint_row = 0;
                                border_midpoint_col = border_midpoint_dist_from_origin;
                            }
                            else
                            {
                                border_midpoint_row = -border_midpoint_dist_from_origin;
                                border_midpoint_col = 0;
                            }

                            //std.vector<CDSDBRSupermodeMapLine.point_type>.iterator first_middle_pt = _lm_middle_lines[lm]._points.begin();
                            int first_middle_pt = 0;
                            int first_middle_pt_col = _lm_middle_lines[lm]._points[first_middle_pt].col;
                            int first_middle_pt_row = _lm_middle_lines[lm]._points[first_middle_pt].row;

                            double slope = ((double)(first_middle_pt_row - border_midpoint_row)) / ((double)(first_middle_pt_col - border_midpoint_col));

                            // starting at the first existing point,
                            // insert one new point in to middle line
                            // for each column between the first existing point and the border point

                            for (int i = first_middle_pt_col - 1; i >= border_midpoint_col; i--)
                            {
                                double row_of_midpoint_double = (double)border_midpoint_row + slope * ((double)(i - border_midpoint_col));
                                int row_of_midpoint = (int)row_of_midpoint_double;
                                if (row_of_midpoint_double - (double)row_of_midpoint > 0.5) row_of_midpoint++;
                                if (row_of_midpoint < 0) row_of_midpoint = 0;

                                // add to middle line
                                //_lm_middle_lines[lm]._points.insert(
                                //    _lm_middle_lines[lm]._points.begin(),convertRowCol2LinePoint(row_of_midpoint,i ) );
                                _lm_middle_lines[lm]._points.Insert(0, convertRowCol2LinePoint(row_of_midpoint, i));
                            }
                        }
                    }


                    if (!middle_point_found_at_last_col
                    && !(_lm_middle_lines[lm]._points.Count == 0))
                    {
                        // need to fill in the end of the middle line to the rhs-top border
                        // find middle end point
                        // calculate points on straight line fit from last known middle point
                        // and add to end of middle line

                        //std.vector<CDSDBRSupermodeMapLine.point_type>.iterator last_middle_pt = _lm_middle_lines[lm]._points.end();
                        //last_middle_pt--;

                        //std.vector<CDSDBRSupermodeMapLine.point_type>.iterator lower_border_pt = i_lower_line->_points.end();
                        int lower_border_pt = _lm_upper_lines[i_lower_line]._points.Count;

                        for (int j = 0; j < (int)(_lm_upper_lines[i_lower_line]._points.Count); j++)
                        {
                            if ((_lm_upper_lines[i_lower_line]._points[j].col == _cols - 1
                            || _lm_upper_lines[i_lower_line]._points[j].row == _rows - 1)
                            && lower_border_pt == _lm_upper_lines[i_lower_line]._points.Count)
                            {
                                // found point on rhs-top border
                                //lower_border_pt = i_lower_line->_points.begin();
                                lower_border_pt = 0;
                                lower_border_pt += j;
                                break;
                            }
                        }


                        //std.vector<CDSDBRSupermodeMapLine.point_type>.iterator upper_border_pt = i_upper_line->_points.end();
                        int upper_border_pt = _lm_lower_lines[i_upper_line]._points.Count;

                        for (int j = 0; j < (int)(_lm_lower_lines[i_upper_line]._points.Count); j++)
                        {
                            if ((_lm_lower_lines[i_upper_line]._points[j].col == _cols - 1
                            || _lm_lower_lines[i_upper_line]._points[j].row == _rows - 1)
                            && upper_border_pt == _lm_lower_lines[i_upper_line]._points.Count)
                            {
                                // found point on rhs-top border
                                //upper_border_pt = i_upper_line->_points.begin();
                                upper_border_pt = 0;
                                upper_border_pt += j;
                                break;
                            }
                        }

                        if (lower_border_pt != _lm_upper_lines[i_lower_line]._points.Count
                        && upper_border_pt != _lm_lower_lines[i_upper_line]._points.Count)
                        {
                            // Have upper and lower border points, find midpoint

                            int lower_dist_from_toprightcorner;
                            if (_lm_upper_lines[i_lower_line]._points[lower_border_pt].col == _cols - 1) lower_dist_from_toprightcorner = (_rows - 1) - _lm_upper_lines[i_lower_line]._points[lower_border_pt].row;
                            else lower_dist_from_toprightcorner = _lm_upper_lines[i_lower_line]._points[lower_border_pt].col - (_cols - 1);

                            int upper_dist_from_toprightcorner;
                            if (_lm_lower_lines[i_upper_line]._points[upper_border_pt].col == _cols - 1) upper_dist_from_toprightcorner = (_rows - 1) - _lm_lower_lines[i_upper_line]._points[upper_border_pt].row;
                            else upper_dist_from_toprightcorner = _lm_lower_lines[i_upper_line]._points[upper_border_pt].col - (_cols - 1);

                            int border_midpoint_row;
                            int border_midpoint_col;
                            int border_midpoint_dist_from_toprightcorner;
                            //int last_midpoint_row = (*(_lm_middle_lines[lm]._points.rbegin())).row;
                            int last_midpoint_row = _lm_middle_lines[lm]._points[_lm_middle_lines[lm]._points.Count - 1].row;

                            if (_lm_upper_lines[i_lower_line]._points[lower_border_pt].row < last_midpoint_row)
                            {
                                double average_width = _average_widths[lm];
                                int pts_to_add = (int)(average_width / 2);
                                border_midpoint_dist_from_toprightcorner = lower_dist_from_toprightcorner - pts_to_add;
                                if ((_rows - 1) - last_midpoint_row < border_midpoint_dist_from_toprightcorner)
                                    border_midpoint_dist_from_toprightcorner = (_rows - 1) - last_midpoint_row;
                            }
                            else
                            {
                                border_midpoint_dist_from_toprightcorner = lower_dist_from_toprightcorner - (lower_dist_from_toprightcorner - upper_dist_from_toprightcorner) / 2;
                            }

                            if (border_midpoint_dist_from_toprightcorner > 0)
                            {
                                border_midpoint_row = (_rows - 1) - border_midpoint_dist_from_toprightcorner;
                                border_midpoint_col = _cols - 1;
                            }
                            else
                            {
                                border_midpoint_row = _rows - 1;
                                border_midpoint_col = (_cols - 1) + border_midpoint_dist_from_toprightcorner;
                            }


                            //std.vector<CDSDBRSupermodeMapLine.point_type>.iterator last_middle_pt = _lm_middle_lines[lm]._points.end();
                            int last_middle_pt = _lm_middle_lines[lm]._points.Count;
                            last_middle_pt--;
                            int last_middle_pt_col = _lm_middle_lines[lm]._points[last_middle_pt].col;
                            int last_middle_pt_row = _lm_middle_lines[lm]._points[last_middle_pt].row;

                            double slope = ((double)(border_midpoint_row - last_middle_pt_row)) / ((double)(border_midpoint_col - last_middle_pt_col));

                            // starting at the end,
                            // insert one new point in to middle line
                            // for each column between the last existing point and the border point

                            for (int i = last_middle_pt_col + 1; i <= border_midpoint_col; i++)
                            {
                                double row_of_midpoint_double = (double)last_middle_pt_row + slope * ((double)(i - last_middle_pt_col));
                                int row_of_midpoint = (int)row_of_midpoint_double;
                                if (row_of_midpoint_double - (double)row_of_midpoint > 0.5) row_of_midpoint++;
                                if (row_of_midpoint > _rows - 1) row_of_midpoint = _rows - 1;

                                // add to middle line
                                _lm_middle_lines[lm]._points.Add(convertRowCol2LinePoint(row_of_midpoint, i));
                            }
                        }
                    }
                }
            }
            else
            {
                rval = rcode.mismatched_lines;
                CGR_LOG.Write("CDSDBRSuperMode.findLMMiddleLines() mismatched_lines", LoggingLevels.ERROR_CGR_LOG);
            }

            if (DSDBR01.Instance._DSDBR01_SMBD_write_debug_files)
            {

                for (int lm = 0; lm < (int)_lm_middle_lines.Count; lm++)
                {
                    // iterate through the boundary lines of each longitudinal mode

                    String middle_line_abs_filepath = _map_forward_power_ratio_median_filtered._abs_filepath;
                    //char buf[100];
                    //sprintf(buf,"middle_F_lm%d_",lm);
                    middle_line_abs_filepath = middle_line_abs_filepath.Replace(Defaults.MATRIX_, "middle_F_lm" + lm + "_");

                    _lm_middle_lines[lm].writeRowColToFile(middle_line_abs_filepath, true);

                    middle_line_abs_filepath = _map_forward_power_ratio_median_filtered._abs_filepath;
                    //sprintf(buf,"middleFcurrents_lm%d_",lm);
                    middle_line_abs_filepath = middle_line_abs_filepath.Replace(Defaults.MATRIX_, "middleFcurrents_lm" + lm + "_");

                    _lm_middle_lines[lm].writeCurrentsToFile(middle_line_abs_filepath, true);
                }

                for (int lm = 0; lm < (int)_lm_lower_lines.Count; lm++)
                {
                    // Assume _abs_filepath is set from loading data from file
                    // or from writing to file after gathering data from laser
                    String abs_filepath = _map_forward_power_ratio_median_filtered._abs_filepath;
                    //char buf[100];
                    //sprintf(buf,"lower_F_lm%d_",lm);
                    abs_filepath = abs_filepath.Replace(Defaults.MATRIX_, "lower_F_lm" + lm + "_");

                    // write boundary line to file
                    _lm_lower_lines[lm].writeRowColToFile(abs_filepath, true);
                }

                for (int lm = 0; lm < (int)_lm_upper_lines.Count; lm++)
                {
                    // Assume _abs_filepath is set from loading data from file
                    // or from writing to file after gathering data from laser
                    String abs_filepath = _map_reverse_power_ratio_median_filtered._abs_filepath;
                    //char buf[100];
                    //sprintf(buf,"upper_F_lm%d_",lm);
                    abs_filepath = abs_filepath.Replace(Defaults.MATRIX_, "upper_F_lm" + lm + "_");

                    // write boundary line to file
                    _lm_upper_lines[lm].writeRowColToFile(abs_filepath, true);
                }
            }

            String log_msg = "CDSDBRSuperMode.findLMMiddleLines() ";
            log_msg += string.Format("found {0} middle lines", _lm_middle_lines.Count);
            CGR_LOG.Write(log_msg, LoggingLevels.INFO_CGR_LOG);

            CGR_LOG.Write("CDSDBRSuperMode.findLMMiddleLines() exiting", LoggingLevels.DIAGNOSTIC_CGR_LOG);

            return rval;
        }


        public rcode findMiddleLines(
            List<CDSDBRSupermodeMapLine> lines,//&
            List<CDSDBRSupermodeMapLine> middle_lines)//&
        {
            CGR_LOG.Write("CDSDBRSuperMode.findMiddleLines() entered", LoggingLevels.DIAGNOSTIC_CGR_LOG);
            rcode rval = rcode.ok;

            List<double> average_widths = new List<double>();

            for (int i = 0; i < middle_lines.Count; i++)
            {
                middle_lines[i].clear();
            }
            middle_lines.Clear();

            int count_lm_lines = lines.Count;

            if (count_lm_lines > 1)
            {
                for (int lm = 0; lm < count_lm_lines - 1; lm++)
                {
                    // iterate through the boundary lines of each longitudinal mode

                    //std.vector<CDSDBRSupermodeMapLine>.iterator i_lower_line = lines.begin();
                    int i_lower_line = 0;
                    i_lower_line += lm;
                    //std.vector<CDSDBRSupermodeMapLine>.iterator i_upper_line = i_lower_line;
                    int i_upper_line = i_lower_line;
                    i_upper_line++;

                    CDSDBRSupermodeMapLine new_lm_middle_line = new CDSDBRSupermodeMapLine();
                    middle_lines.Add(new_lm_middle_line);

                    bool middle_point_found_at_col_zero = false;
                    bool middle_point_found_at_last_col = false;

                    // if right-most column covered is not last column in map
                    //   find midpoint on rhs-top border
                    //   plot line from right-most point found to midpoint
                    //   add points along line to middle line
                    // if left-most column covered is not first column in map
                    //   find midpoint on lhs_bottom border
                    //   plot line from left-most point found to lhs_bottom border
                    //   add points along line to middle line

                    int widths_total = 0;
                    int widths_count = 0;

                    for (int i = 0; i < _cols; i++)
                    {
                        // for each column

                        int highest_row_at_i_on_lower_line = -1;
                        int index_of_highest_row_at_i_on_lower_line = -1;
                        int lowest_row_at_i_on_lower_line = _rows;
                        int index_of_lowest_row_at_i_on_lower_line = -1;

                        // find highest and lowest points on lower line at column i

                        for (int j = 0; j < lines[i_lower_line]._points.Count; j++)
                        {
                            if (lines[i_lower_line]._points[j].col == i
                            && lines[i_lower_line]._points[j].row > highest_row_at_i_on_lower_line)
                            {
                                index_of_highest_row_at_i_on_lower_line = j;
                                highest_row_at_i_on_lower_line = lines[i_lower_line]._points[j].row;
                            }
                            if (lines[i_lower_line]._points[j].col == i
                            && lines[i_lower_line]._points[j].row < lowest_row_at_i_on_lower_line)
                            {
                                index_of_lowest_row_at_i_on_lower_line = j;
                                lowest_row_at_i_on_lower_line = lines[i_lower_line]._points[j].row;
                            }
                        }

                        int highest_row_at_i_on_upper_line = -1;
                        int index_of_highest_row_at_i_on_upper_line = -1;
                        int lowest_row_at_i_on_upper_line = _rows;
                        int index_of_lowest_row_at_i_on_upper_line = -1;

                        // find highest and lowest point on upper line at column i

                        for (int j = 0; j < lines[i_upper_line]._points.Count; j++)
                        {
                            if (lines[i_upper_line]._points[j].col == i
                            && lines[i_upper_line]._points[j].row > highest_row_at_i_on_upper_line)
                            {
                                index_of_highest_row_at_i_on_upper_line = j;
                                highest_row_at_i_on_upper_line = lines[i_upper_line]._points[j].row;
                            }
                            if (lines[i_upper_line]._points[j].col == i
                            && lines[i_upper_line]._points[j].row < lowest_row_at_i_on_upper_line)
                            {
                                index_of_lowest_row_at_i_on_upper_line = j;
                                lowest_row_at_i_on_upper_line = lines[i_upper_line]._points[j].row;
                            }
                        }

                        if (index_of_highest_row_at_i_on_lower_line != -1
                        && index_of_lowest_row_at_i_on_upper_line != -1)
                        {
                            if (i == 0) middle_point_found_at_col_zero = true;
                            if (i == _cols - 1) middle_point_found_at_last_col = true;

                            // if both found, check min and max separation distances
                            int current_min_width = lowest_row_at_i_on_upper_line - highest_row_at_i_on_lower_line;

                            widths_total += current_min_width;
                            widths_count++;

                            // find vertical midpoint

                            int row_of_midpoint = highest_row_at_i_on_lower_line + (int)(((double)current_min_width) / 2);

                            //     add to middle line
                            middle_lines[lm]._points.Add(
                                convertRowCol2LinePoint(
                                    row_of_midpoint,
                                    i));
                        }
                    }

                    if (middle_lines[lm]._points.Count > 0)
                        average_widths.Add((double)widths_total / (double)widths_count);
                    else
                        average_widths.Add(0);

                    if (!middle_point_found_at_col_zero
                    && !(middle_lines[lm]._points.Count == 0))
                    {
                        // need to fill in the start of the middle line to the lhs-bottom border
                        // find middle start point
                        // calculate points on straight line fit first known middle point
                        // and add to start of middle line

                        //std.vector<CDSDBRSupermodeMapLine.point_type>.iterator lower_border_pt = i_lower_line->_points.end();
                        int lower_border_pt = lines[i_lower_line]._points.Count;

                        for (int j = 0; j < lines[i_lower_line]._points.Count; j++)
                        {
                            if ((lines[i_lower_line]._points[j].col == 0
                            || lines[i_lower_line]._points[j].row == 0)
                            && lower_border_pt == lines[i_lower_line]._points.Count)
                            {
                                // found point on lhs-bottom border
                                //lower_border_pt = i_lower_line->_points.begin();
                                lower_border_pt = 0;
                                lower_border_pt += j;
                                break;
                            }
                        }

                        //std.vector<CDSDBRSupermodeMapLine.point_type>.iterator upper_border_pt = i_upper_line->_points.end();
                        int upper_border_pt = lines[i_upper_line]._points.Count;

                        for (int j = 0; j < lines[i_upper_line]._points.Count; j++)
                        {
                            if ((lines[i_upper_line]._points[j].col == 0
                            || lines[i_upper_line]._points[j].row == 0)
                            && upper_border_pt == lines[i_upper_line]._points.Count)
                            {
                                // found point on lhs-bottom border
                                upper_border_pt = 0;
                                upper_border_pt += j;
                                break;
                            }
                        }

                        if (lower_border_pt != lines[i_lower_line]._points.Count
                        && upper_border_pt != lines[i_upper_line]._points.Count)
                        {
                            // Have upper and lower border points, find midpoint
                            int lower_dist_from_origin;
                            if (lines[i_lower_line]._points[lower_border_pt].col == 0) lower_dist_from_origin = -lines[i_lower_line]._points[lower_border_pt].row;
                            else lower_dist_from_origin = lines[i_lower_line]._points[lower_border_pt].col;

                            int upper_dist_from_origin;
                            if (lines[i_upper_line]._points[upper_border_pt].col == 0) upper_dist_from_origin = -lines[i_upper_line]._points[upper_border_pt].row;
                            else upper_dist_from_origin = lines[i_upper_line]._points[upper_border_pt].col;

                            int border_midpoint_row;
                            int border_midpoint_col;
                            int border_midpoint_dist_from_origin = lower_dist_from_origin - (lower_dist_from_origin - upper_dist_from_origin) / 2;
                            if (border_midpoint_dist_from_origin > 0)
                            {
                                border_midpoint_row = 0;
                                border_midpoint_col = border_midpoint_dist_from_origin;
                            }
                            else
                            {
                                border_midpoint_row = -border_midpoint_dist_from_origin;
                                border_midpoint_col = 0;
                            }

                            //std.vector<CDSDBRSupermodeMapLine.point_type>.iterator first_middle_pt = middle_lines[lm]._points.begin();
                            int first_middle_pt = 0;
                            int first_middle_pt_col = middle_lines[lm]._points[first_middle_pt].col;
                            int first_middle_pt_row = middle_lines[lm]._points[first_middle_pt].row;

                            double slope = ((double)(first_middle_pt_row - border_midpoint_row)) / ((double)(first_middle_pt_col - border_midpoint_col));

                            // starting at the first existing point,
                            // insert one new point in to middle line
                            // for each column between the first existing point and the border point

                            for (int i = first_middle_pt_col - 1; i >= border_midpoint_col; i--)
                            {
                                double row_of_midpoint_double = (double)border_midpoint_row + slope * ((double)(i - border_midpoint_col));
                                int row_of_midpoint = (int)row_of_midpoint_double;
                                if (row_of_midpoint_double - (double)row_of_midpoint > 0.5) row_of_midpoint++;
                                if (row_of_midpoint < 0) row_of_midpoint = 0;

                                // add to middle line
                                middle_lines[lm]._points.Insert(0, convertRowCol2LinePoint(row_of_midpoint, i));

                            }
                        }

                    }

                    if (!middle_point_found_at_last_col
                    && !(middle_lines[lm]._points.Count == 0))
                    {
                        // need to fill in the end of the middle line to the rhs-top border
                        // find middle end point
                        // calculate points on straight line fit from last known middle point
                        // and add to end of middle line

                        //std.vector<CDSDBRSupermodeMapLine.point_type>.iterator last_middle_pt = middle_lines[lm]._points.end();
                        //last_middle_pt--;

                        //std.vector<CDSDBRSupermodeMapLine.point_type>.iterator lower_border_pt = i_lower_line->_points.end();
                        int lower_border_pt = lines[i_lower_line]._points.Count;

                        for (int j = 0; j < lines[i_lower_line]._points.Count; j++)
                        {
                            if ((lines[i_lower_line]._points[j].col == _cols - 1
                            || lines[i_lower_line]._points[j].row == _rows - 1)
                            && lower_border_pt == lines[i_lower_line]._points.Count)
                            {
                                // found point on rhs-top border
                                lower_border_pt = 0;
                                lower_border_pt += j;
                                break;
                            }
                        }


                        //std.vector<CDSDBRSupermodeMapLine.point_type>.iterator upper_border_pt = i_upper_line->_points.end();
                        int upper_border_pt = lines[i_upper_line]._points.Count;

                        for (int j = 0; j < lines[i_upper_line]._points.Count; j++)
                        {
                            if ((lines[i_upper_line]._points[j].col == _cols - 1
                            || lines[i_upper_line]._points[j].row == _rows - 1)
                            && upper_border_pt == lines[i_upper_line]._points.Count)
                            {
                                // found point on rhs-top border
                                upper_border_pt = 0;
                                upper_border_pt += j;
                                break;
                            }
                        }

                        if (lower_border_pt != lines[i_lower_line]._points.Count
                        && upper_border_pt != lines[i_upper_line]._points.Count)
                        {
                            // Have upper and lower border points, find midpoint

                            int lower_dist_from_toprightcorner;
                            if (lines[i_lower_line]._points[lower_border_pt].col == _cols - 1) lower_dist_from_toprightcorner = (_rows - 1) - lines[i_lower_line]._points[lower_border_pt].row;
                            else lower_dist_from_toprightcorner = lines[i_lower_line]._points[lower_border_pt].col - (_cols - 1);

                            int upper_dist_from_toprightcorner;
                            if (lines[i_upper_line]._points[upper_border_pt].col == _cols - 1) upper_dist_from_toprightcorner = (_rows - 1) - lines[i_upper_line]._points[upper_border_pt].row;
                            else upper_dist_from_toprightcorner = lines[i_upper_line]._points[upper_border_pt].col - (_cols - 1);

                            int border_midpoint_row;
                            int border_midpoint_col;
                            int border_midpoint_dist_from_toprightcorner;
                            //int last_midpoint_row = (*(middle_lines[lm]._points.rbegin())).row;
                            int last_midpoint_row = middle_lines[lm]._points[middle_lines[lm]._points.Count - 1].row;

                            if (lines[i_lower_line]._points[lower_border_pt].row < last_midpoint_row)
                            {
                                double average_width = average_widths[lm];
                                int pts_to_add = (int)(average_width / 2);
                                border_midpoint_dist_from_toprightcorner = lower_dist_from_toprightcorner - pts_to_add;
                                if ((_rows - 1) - last_midpoint_row < border_midpoint_dist_from_toprightcorner)
                                    border_midpoint_dist_from_toprightcorner = (_rows - 1) - last_midpoint_row;
                            }
                            else
                            {
                                border_midpoint_dist_from_toprightcorner = lower_dist_from_toprightcorner - (lower_dist_from_toprightcorner - upper_dist_from_toprightcorner) / 2;
                            }

                            if (border_midpoint_dist_from_toprightcorner > 0)
                            {
                                border_midpoint_row = (_rows - 1) - border_midpoint_dist_from_toprightcorner;
                                border_midpoint_col = _cols - 1;
                            }
                            else
                            {
                                border_midpoint_row = _rows - 1;
                                border_midpoint_col = (_cols - 1) + border_midpoint_dist_from_toprightcorner;
                            }


                            //std.vector<CDSDBRSupermodeMapLine.point_type>.iterator last_middle_pt = middle_lines[lm]._points.end();
                            int last_middle_pt = middle_lines[lm]._points.Count;
                            last_middle_pt--;
                            int last_middle_pt_col = middle_lines[lm]._points[last_middle_pt].col;
                            int last_middle_pt_row = middle_lines[lm]._points[last_middle_pt].row;

                            double slope = ((double)(border_midpoint_row - last_middle_pt_row)) / ((double)(border_midpoint_col - last_middle_pt_col));

                            // starting at the end,
                            // insert one new point in to middle line
                            // for each column between the last existing point and the border point
                            for (int i = last_middle_pt_col + 1; i <= border_midpoint_col; i++)
                            {
                                double row_of_midpoint_double = (double)last_middle_pt_row + slope * ((double)(i - last_middle_pt_col));
                                int row_of_midpoint = (int)row_of_midpoint_double;
                                if (row_of_midpoint_double - (double)row_of_midpoint > 0.5) row_of_midpoint++;
                                if (row_of_midpoint > _rows - 1) row_of_midpoint = _rows - 1;

                                // add to middle line
                                middle_lines[lm]._points.Add(convertRowCol2LinePoint(row_of_midpoint, i));
                                    
                            }
                        }
                    }
                }
            }
            else
            {
                rval = rcode.no_data;
                CGR_LOG.Write("CDSDBRSuperMode.findMiddleLines() not enough lines", LoggingLevels.ERROR_CGR_LOG);
            }
            CGR_LOG.Write("CDSDBRSuperMode.findMiddleLines() exiting", LoggingLevels.DIAGNOSTIC_CGR_LOG);
            return rval;
        }


        public rcode filterLMMiddleLines()
        {
            CGR_LOG.Write("CDSDBRSuperMode.filterLMMiddleLines() entered", LoggingLevels.DIAGNOSTIC_CGR_LOG);

            rcode rval = rcode.ok;

            // clear filtered middle lines and freq middle lines

            for (int i = 0; i < (int)(_lm_filtered_middle_lines.Count); i++)
            {
                _lm_filtered_middle_lines[i].clear();
            }
            _lm_filtered_middle_lines.Clear();

            for (int i = 0; i < (int)(_lm_middle_lines_of_frequency.Count); i++)
            {
                _lm_middle_lines_of_frequency[i].clear();
            }
            _lm_middle_lines_of_frequency.Clear();



            int filter_n = (int)(DSDBR01.Instance._DSDBR01_SMBD_ml_moving_ave_filter_n);

            for (short lm = 0; lm < (short)(_lm_middle_lines.Count); lm++)
            {
                CDSDBRSupermodeMapLine new_filtered_lm_middle_line = new CDSDBRSupermodeMapLine();

                _lm_filtered_middle_lines.Add(new_filtered_lm_middle_line);

                // Perform averaging in the middle line direction only
                // which is a combination of If and Ir

                for (int i = 0; i < (int)(_lm_middle_lines[lm]._points.Count); i++)
                {
                    int window_size = 0;
                    int sum_of_rows_of_windowed_points = 0;
                    for (int j = i - filter_n; j <= i + filter_n; j++)
                    {
                        if (j >= 0 && j < (int)(_lm_middle_lines[lm]._points.Count))
                        {
                            // sum rows in window, without exceeding start or end
                            window_size++;
                            sum_of_rows_of_windowed_points += _lm_middle_lines[lm]._points[j].row;
                        }
                    }

                    // the structure CDSDBROverallModeMapLine.point_type
                    // contains both the row, col position on a map and
                    // the currents at that point.
                    // For the average, we need to interpolate the If current
                    // while finding the nearest row (just so it can be plotted on a map)

                    CDSDBRSupermodeMapLine.point_type averaged_pt = interpolateLinePoint(
                            (double)sum_of_rows_of_windowed_points / (double)window_size,
                            _lm_middle_lines[lm]._points[i].col);


                    // Add averaged point
                    _lm_filtered_middle_lines[lm]._points.Add(averaged_pt);
                }
            }

            CGR_LOG.Write("CDSDBRSuperMode.filterLMMiddleLines() exiting", LoggingLevels.DIAGNOSTIC_CGR_LOG);

            return rval;
        }
        bool g_prevent_front_section_switch = false;
        short g_current_front_pair_number = 0;

        public CDSDBRSupermodeMapLine.point_type interpolateLinePoint(double row, double col)
        {
            CDSDBRSupermodeMapLine.point_type point_on_map = new CDSDBRSupermodeMapLine.point_type();

            point_on_map.real_row = row;
            point_on_map.real_col = col;

            // find nearest row
            point_on_map.row = (int)row;
            if (row - (double)((int)row) > 0.5) point_on_map.row++;

            // find nearest col
            point_on_map.col = (int)col;
            if (col - (double)((int)col) > 0.5) point_on_map.col++;

            if (col > (double)((int)col))
            {
                // interpolate Ip linearly
                double Ip_a = _phase_current._currents[(int)col];
                double Ip_b = _phase_current._currents[((int)col) + 1];

                double Ip_fraction = col - (double)((int)col);

                point_on_map.I_phase = Ip_a + Ip_fraction * (Ip_b - Ip_a);
            }
            else
            {
                // no need to interpolate
                point_on_map.I_phase = _phase_current._currents[(int)col];
            }

            point_on_map.I_front_1 = 0;
            point_on_map.I_front_2 = 0;
            point_on_map.I_front_3 = 0;
            point_on_map.I_front_4 = 0;
            point_on_map.I_front_5 = 0;
            point_on_map.I_front_6 = 0;
            point_on_map.I_front_7 = 0;
            point_on_map.I_front_8 = 0;

            // use the nearest point to determine which map to use
            point_on_map.front_pair_number = _filtered_middle_line.pair_number(point_on_map.row);

            short index_of_nearest = (short)(_filtered_middle_line.index_in_submap(point_on_map.row));

            // find out how far the interpolated point is from the nearest point
            double If_fraction = row - (double)(point_on_map.row);
            int top_row = _filtered_middle_line._submap_length[point_on_map.front_pair_number - 1];

            if ((index_of_nearest == 0 && If_fraction < 0)
             || (index_of_nearest == (short)top_row && If_fraction > 0))
            {
                // cannot interpolate between maps!
                // use nearest point for currents
                If_fraction = 0;
            }

            double If_value;

            if (If_fraction != 0)
            {
                // need to interpolate
                short index_of_nextpt;
                int next_row;
                if (If_fraction > 0)
                {
                    index_of_nextpt = (short)(index_of_nearest + 1);
                    next_row = point_on_map.row + 1;
                    //Ir_fraction = If_fraction;
                }
                else
                {
                    index_of_nextpt = (short)(index_of_nearest - 1);
                    next_row = point_on_map.row - 1;
                    If_fraction = -If_fraction; // ensure positive fraction
                }

                // interpolate If linearly
                double If_a = _filtered_middle_line.getNonConstantIf(point_on_map.front_pair_number, index_of_nearest);
                double If_b = _filtered_middle_line.getNonConstantIf(point_on_map.front_pair_number, index_of_nextpt);
                If_value = If_a + If_fraction * (If_b - If_a);


                // interpolate Ir linearly
                double Ir_a = _filtered_middle_line._points[point_on_map.row].I_rear;
                double Ir_b = _filtered_middle_line._points[next_row].I_rear;

                point_on_map.I_rear = Ir_a + If_fraction * (Ir_b - Ir_a);
            }
            else
            {
                // no need to interpolate
                If_value = _filtered_middle_line.getNonConstantIf(point_on_map.front_pair_number, index_of_nearest);

                point_on_map.I_rear = _filtered_middle_line._points[(int)row].I_rear;

                point_on_map.real_row = (double)((int)row);
            }


            if (g_prevent_front_section_switch)
            {
                if (point_on_map.front_pair_number > g_current_front_pair_number)
                {
                    point_on_map.front_pair_number = g_current_front_pair_number;
                    top_row = _filtered_middle_line._submap_length[point_on_map.front_pair_number - 1] - 1;
                    If_value = _filtered_middle_line.getNonConstantIf(point_on_map.front_pair_number, top_row);
                    index_of_nearest = (short)top_row;
                }
                else if (point_on_map.front_pair_number < g_current_front_pair_number)
                {
                    point_on_map.front_pair_number = g_current_front_pair_number;
                    If_value = _filtered_middle_line.getNonConstantIf(point_on_map.front_pair_number, 0);
                    index_of_nearest = 0;
                }
            }
            else
            {
                g_current_front_pair_number = point_on_map.front_pair_number;
            }

            switch (point_on_map.front_pair_number)
            {
                case 1:
                    point_on_map.I_front_1 = _filtered_middle_line.getConstantIf(point_on_map.front_pair_number, index_of_nearest);
                    point_on_map.I_front_2 = If_value;
                    break;
                case 2:
                    point_on_map.I_front_2 = _filtered_middle_line.getConstantIf(point_on_map.front_pair_number, index_of_nearest);
                    point_on_map.I_front_3 = If_value;
                    break;
                case 3:
                    point_on_map.I_front_3 = _filtered_middle_line.getConstantIf(point_on_map.front_pair_number, index_of_nearest);
                    point_on_map.I_front_4 = If_value;
                    break;
                case 4:
                    point_on_map.I_front_4 = _filtered_middle_line.getConstantIf(point_on_map.front_pair_number, index_of_nearest);
                    point_on_map.I_front_5 = If_value;
                    break;
                case 5:
                    point_on_map.I_front_5 = _filtered_middle_line.getConstantIf(point_on_map.front_pair_number, index_of_nearest);
                    point_on_map.I_front_6 = If_value;
                    break;
                case 6:
                    point_on_map.I_front_6 = _filtered_middle_line.getConstantIf(point_on_map.front_pair_number, index_of_nearest);
                    point_on_map.I_front_7 = If_value;
                    break;
                case 7:
                    point_on_map.I_front_7 = _filtered_middle_line.getConstantIf(point_on_map.front_pair_number, index_of_nearest);
                    point_on_map.I_front_8 = If_value;
                    break;
            }

            return point_on_map;
        }

        public rcode calcLMMiddleLinesOfFreq()
        {
            CGR_LOG.Write("CDSDBRSuperMode::calcLMMiddleLinesOfFreq() entered", LoggingLevels.DIAGNOSTIC_CGR_LOG);

            rcode rval = rcode.ok;

            // clear freq middle lines

            for (int i = 0; i < (int)(_lm_middle_lines_of_frequency.Count); i++)
            {
                _lm_middle_lines_of_frequency[i].clear();
            }
            _lm_middle_lines_of_frequency.Clear();


            double vertical_percent_shift = DSDBR01.Instance._DSDBR01_SMBD_vertical_percent_shift; // set this per supermode, range +-50%

            for (short lm = 0; lm < (short)(_lm_filtered_middle_lines.Count); lm++)
            {
                CDSDBRSupermodeMapLine new_lm_middle_line_of_frequency = new CDSDBRSupermodeMapLine();

                _lm_middle_lines_of_frequency.Add(new_lm_middle_line_of_frequency);

                double average_width = _average_widths[lm];

                if (average_width > 0)
                {
                    double points_to_shift = average_width * (vertical_percent_shift / 100);

                    for (int i = 0; i < (int)(_lm_filtered_middle_lines[lm]._points.Count); i++)
                    {
                        double row = (double)(_lm_filtered_middle_lines[lm]._points[i].row);
                        double col = (double)(_lm_filtered_middle_lines[lm]._points[i].col);

                        row += points_to_shift;

                        // ensure point is still on the map, otherwise ignore it
                        if (row >= 0 && row <= (double)(_rows - 1))
                        {
                            CDSDBRSupermodeMapLine.point_type shifted_pt
                                = interpolateLinePoint(row, col);

                            _lm_middle_lines_of_frequency[lm]._points.Add(shifted_pt);
                        }
                    }
                }
            }

            //_lm_middle_lines_of_frequency_removed = (short)_lm_middle_lines_of_frequency.Count;

            CGR_LOG.Write("CDSDBRSuperMode::calcLMMiddleLinesOfFreq() exiting", LoggingLevels.DIAGNOSTIC_CGR_LOG);

            return rval;
        }

        public rcode checkLMMiddleLinesOfFreq()
        {
            CGR_LOG.Write("CDSDBRSuperMode.checkLMMiddleLinesOfFreq() entered", LoggingLevels.DIAGNOSTIC_CGR_LOG);

            rcode rval = rcode.ok;

            int lm_count = (int)(_lm_middle_lines_of_frequency.Count);

            if (lm_count > 0)
            {
                for (int lm = lm_count - 1; lm >= 0; lm--)
                {
                    //std.vector<CDSDBRSupermodeMapLine>.iterator i_middle = _lm_middle_lines_of_frequency.begin();
                    int i_middle = 0;
                    i_middle += lm;

                    // check each lm middle line is within one longitudinal mode
                    for (int i = 0; i < (int)(_lm_middle_lines_of_frequency[i_middle]._points.Count) - 1; i++)
                    {
                        int rowA = 0;
                        int colA = 0;
                        int rowB = 0;
                        int colB = 0;

                        if (_Im_ramp)
                        {
                            rowA = _lm_middle_lines_of_frequency[i_middle]._points[i].col;
                            colA = _lm_middle_lines_of_frequency[i_middle]._points[i].row;
                            rowB = _lm_middle_lines_of_frequency[i_middle]._points[i + 1].col;
                            colB = _lm_middle_lines_of_frequency[i_middle]._points[i + 1].row;
                        }
                        else
                        {
                            rowA = _lm_middle_lines_of_frequency[i_middle]._points[i].row;
                            colA = _lm_middle_lines_of_frequency[i_middle]._points[i].col;
                            rowB = _lm_middle_lines_of_frequency[i_middle]._points[i + 1].row;
                            colB = _lm_middle_lines_of_frequency[i_middle]._points[i + 1].col;
                        }

                        //double PrA_fwd = *(_map_forward_power_ratio_median_filtered.iPt( rowA , colA ));
                        //double PrB_fwd = *(_map_forward_power_ratio_median_filtered.iPt( rowB , colB ));
                        //double PrA_rev = *(_map_reverse_power_ratio_median_filtered.iPt( rowA , colA ));
                        //double PrB_rev = *(_map_reverse_power_ratio_median_filtered.iPt( rowB , colB ));
                        double PrA_fwd = _map_forward_power_ratio_median_filtered.iPt(rowA, colA).Current;
                        double PrB_fwd = _map_forward_power_ratio_median_filtered.iPt(rowB, colB).Current;
                        double PrA_rev = _map_reverse_power_ratio_median_filtered.iPt(rowA, colA).Current;
                        double PrB_rev = _map_reverse_power_ratio_median_filtered.iPt(rowB, colB).Current;

                        double delta_Pr_fwd = Math.Abs(PrA_fwd - PrB_fwd);
                        double delta_Pr_rev = Math.Abs(PrA_rev - PrB_rev);

                        if (delta_Pr_fwd > DSDBR01.Instance._DSDBR01_SMBD_max_deltaPr_in_lm
                        || delta_Pr_rev > DSDBR01.Instance._DSDBR01_SMBD_max_deltaPr_in_lm)
                        {
                            // middle line is not in a single longitudinal mode
                            // clear it!

                            //(*i_middle).clear();
                            _lm_middle_lines_of_frequency[i_middle].clear();

                            _lm_middle_lines_of_frequency_removed++;

                            StringBuilder log_msg = new StringBuilder("CDSDBRSuperMode.checkLMMiddleLinesOfFreq() ");
                            log_msg.AppendFormat("removed lm{0} middle line of frequency, ", lm);
                            log_msg.AppendFormat("at i={0}; delta_Pr_fwd={1}, delta_Pr_rev={2},  max_delta_Pr_in_lm={3} ",
                                i, delta_Pr_fwd, delta_Pr_rev, DSDBR01.Instance._DSDBR01_SMBD_max_deltaPr_in_lm);
                            CGR_LOG.Write(log_msg.ToString(), LoggingLevels.INFO_CGR_LOG);

                            break;
                        }
                    }
                }

                // need to remove erasing of first and last lines
                // as they are needed to calculate stable area
                #region Comment


                //// always remove the upper line above the last middle line, it is not part of a longitudinal mode
                //std.vector<CDSDBRSupermodeMapLine>.iterator i_last_upper = _lm_upper_lines.begin();
                //i_last_upper += lm_count;
                //(*i_last_upper).clear();
                //_lm_upper_lines.erase(i_last_upper);

                //// always remove the first lower line, it is not part of a longitudinal mode
                //std.vector<CDSDBRSupermodeMapLine>.iterator i_first_lower = _lm_lower_lines.begin();
                //(*i_first_lower).clear();
                //_lm_lower_lines.erase(i_first_lower);

                // having removed the first lower line,
                // the indices match upper, lower and middle lines of the same longitudinal mode
                // not any more, had to remove erasing of first and last lines above

                //// Instead of removing lowest lower line,
                //// I'll insert empty middle and upper lines at start
                //// so that indexing starts at 1
                //// and lower line 0 is kept
                //CDSDBRSupermodeMapLine empty_line;
                //_lm_upper_lines.insert(_lm_upper_lines.begin(),empty_line);
                //_lm_middle_lines_of_frequency.insert(_lm_middle_lines_of_frequency.begin(),empty_line);


                //for( int lm = lm_count ; lm > 0; lm-- )
                //{
                //	std.vector<CDSDBRSupermodeMapLine>.iterator i_middle = _lm_middle_lines_of_frequency.begin();
                //	std.vector<CDSDBRSupermodeMapLine>.iterator i_lower = _lm_lower_lines.begin();
                //	std.vector<CDSDBRSupermodeMapLine>.iterator i_upper = _lm_upper_lines.begin();
                //	i_middle += lm;
                //	i_lower += lm;
                //	i_upper += lm;

                //	if( (*i_middle)._points.empty()
                //	|| (*i_lower)._points.empty()
                //	|| (*i_upper)._points.empty() )
                //	{
                //		// erase all 3 lines if any 1 is empty

                //		(*i_middle).clear();
                //		(*i_lower).clear();
                //		(*i_upper).clear();

                //		_lm_middle_lines_of_frequency.erase( i_middle );
                //		_lm_lower_lines.erase( i_lower );
                //		_lm_upper_lines.erase( i_upper );
                //	}
                //}


                //// erase any extra lower lines (that don't have an equivalent middle line)
                //for( int lm = (int)(_lm_lower_lines.Count-1) ; lm > lm_count; lm-- )
                //{
                //	std.vector<CDSDBRSupermodeMapLine>.iterator i_lower = _lm_lower_lines.begin();
                //	i_lower += lm;
                //	(*i_lower).clear();
                //	_lm_lower_lines.erase( i_lower );
                //}

                //// erase any extra upper lines (that don't have an equivalent middle line)
                ////except 1 at top needed for stable width
                //for( int lm = (int)(_lm_upper_lines.Count-1) ; lm > lm_count+1; lm-- )
                //{
                //	std.vector<CDSDBRSupermodeMapLine>.iterator i_upper = _lm_upper_lines.begin();
                //	i_upper += lm;
                //	(*i_upper).clear();
                //	_lm_upper_lines.erase( i_upper );
                //}


                ////// note how many lines were removed due to flaws found.
                ////// decrment _lm_upper_lines_removed and _lm_middle_lines_of_frequency_removed
                ////// as these have 1 empty line put into each to get indexing right
                ////_lm_upper_lines_removed -= (short)(_lm_upper_lines.Count-1);
                ////_lm_lower_lines_removed -= (short)_lm_lower_lines.Count;
                ////_lm_middle_lines_of_frequency_removed -= (short)(_lm_middle_lines_of_frequency.Count-1);

                //CString log_msg("CDSDBRSuperMode.checkLMMiddleLinesOfFreq() ");
                //log_msg.AppendFormat("have %d middle lines of frequency ", _lm_middle_lines_of_frequency.Count-1 );
                //log_msg += _T("after filtering, shifting and checking");
                //CGR_LOG(log_msg,INFO_CGR_LOG)
                #endregion

            }
            else
            {
                rval = rcode.min_number_of_lines_not_reached;
                CGR_LOG.Write("CDSDBRSuperMode.checkLMMiddleLinesOfFreq() error: min_number_of_lines_not_reached", LoggingLevels.ERROR_CGR_LOG);
            }


            if (DSDBR01.Instance._DSDBR01_SMBD_write_debug_files)
            {

                for (int lm = 0; lm < (int)_lm_middle_lines_of_frequency.Count; lm++)
                {
                    // iterate through the boundary lines of each longitudinal mode

                    String middle_line_abs_filepath = _map_forward_power_ratio_median_filtered._abs_filepath;
                    //char buf[100];
                    //sprintf(buf,"middleFreq_G_lm%d_",lm);
                    middle_line_abs_filepath = middle_line_abs_filepath.Replace(Defaults.MATRIX_, "middleFreq_G_lm" + lm + "_");

                    _lm_middle_lines_of_frequency[lm].writeRowColToFile(middle_line_abs_filepath, true);

                    middle_line_abs_filepath = _map_forward_power_ratio_median_filtered._abs_filepath;
                    //sprintf(buf,"middleFreqGcurrents_lm%d_",lm);
                    middle_line_abs_filepath = middle_line_abs_filepath.Replace(Defaults.MATRIX_, "middleFreqGcurrents_lm" + lm + "_");

                    _lm_middle_lines_of_frequency[lm].writeCurrentsToFile(middle_line_abs_filepath, true);
                }

                for (int lm = 0; lm < (int)_lm_lower_lines.Count; lm++)
                {
                    // Assume _abs_filepath is set from loading data from file
                    // or from writing to file after gathering data from laser
                    String abs_filepath = _map_forward_power_ratio_median_filtered._abs_filepath;
                    //char buf[100];
                    //sprintf(buf,"lower_G_lm%d_",lm);
                    abs_filepath = abs_filepath.Replace(Defaults.MATRIX_, "lower_G_lm" + lm + "_");

                    // write boundary line to file
                    _lm_lower_lines[lm].writeRowColToFile(abs_filepath, true);
                }

                for (int lm = 0; lm < (int)_lm_upper_lines.Count; lm++)
                {
                    // Assume _abs_filepath is set from loading data from file
                    // or from writing to file after gathering data from laser
                    String abs_filepath = _map_reverse_power_ratio_median_filtered._abs_filepath;
                    //char buf[100];
                    //sprintf(buf,"upper_G_lm%d_",lm);
                    abs_filepath = abs_filepath.Replace(Defaults.MATRIX_, "upper_G_lm" + lm + "_");

                    // write boundary line to file
                    _lm_upper_lines[lm].writeRowColToFile(abs_filepath, true);
                }
            }

            CGR_LOG.Write("CDSDBRSuperMode.checkLMMiddleLinesOfFreq() exiting", LoggingLevels.DIAGNOSTIC_CGR_LOG);

            return rval;
        }

        public rcode writeScreeningResultsToFile()
        {
            rcode rval = rcode.ok;

            string sm_number_str = _sm_number.ToString("d");

            String lines_abs_filepath = _results_dir_CString;

            if (!lines_abs_filepath.EndsWith("\\") )
                lines_abs_filepath += "\\";
            lines_abs_filepath += "lines_";
            lines_abs_filepath += Defaults.LASER_TYPE_ID_DSDBR01_CSTRING;
            lines_abs_filepath += Defaults.USCORE;
            lines_abs_filepath += _laser_id_CString;
            lines_abs_filepath += Defaults.USCORE;
            lines_abs_filepath += _date_time_stamp_CString;
            lines_abs_filepath += Defaults.USCORE;
            lines_abs_filepath += "SM";
            lines_abs_filepath += sm_number_str;
            lines_abs_filepath += Defaults.CSV;

            // write 1 file containing row/col coordinates of
            // upper boundary line, lower boundary line and middle line
            // of all supermodes found
            rval = writeLMLinesToFile(lines_abs_filepath,
                                    true); //DSDBR01.Instance._DSDBR01_OMDC_overwrite_existing_files );


            for (int lm = 0; lm < _lm_middle_lines_of_frequency.Count; lm++)
            {
                String middle_line_abs_filepath = _results_dir_CString;
                if (!middle_line_abs_filepath.EndsWith("\\")) middle_line_abs_filepath += "\\";

                string lm_number_str = lm.ToString("d");

                middle_line_abs_filepath += "ImiddleFreq_LM";
                middle_line_abs_filepath += lm_number_str;
                middle_line_abs_filepath += Defaults.USCORE;
                middle_line_abs_filepath += Defaults.LASER_TYPE_ID_DSDBR01_CSTRING;
                middle_line_abs_filepath += Defaults.USCORE;
                middle_line_abs_filepath += _laser_id_CString;
                middle_line_abs_filepath += Defaults.USCORE;
                middle_line_abs_filepath += _date_time_stamp_CString;
                middle_line_abs_filepath += Defaults.USCORE;
                middle_line_abs_filepath += "SM";
                middle_line_abs_filepath += sm_number_str;
                middle_line_abs_filepath += Defaults.CSV;

                _lm_middle_lines_of_frequency[lm].writeCurrentsToFile(middle_line_abs_filepath, true);

            }

            if (DSDBR01.Instance._DSDBR01_SMBD_write_middle_of_lower_lines_to_file)
            {
                List<CDSDBRSupermodeMapLine> middle_of_lower_lines = new List<CDSDBRSupermodeMapLine>();

                rval = findMiddleLines(_lm_lower_lines, middle_of_lower_lines);

                if (rval == rcode.ok)
                {
                    lines_abs_filepath = _results_dir_CString;
                    if (!lines_abs_filepath.EndsWith("\\")) lines_abs_filepath += "\\";

                    lines_abs_filepath += "middleOfLowerLines_";
                    lines_abs_filepath += Defaults.LASER_TYPE_ID_DSDBR01_CSTRING;
                    lines_abs_filepath += Defaults.USCORE;
                    lines_abs_filepath += _laser_id_CString;
                    lines_abs_filepath += Defaults.USCORE;
                    lines_abs_filepath += _date_time_stamp_CString;
                    lines_abs_filepath += Defaults.USCORE;
                    lines_abs_filepath += "SM";
                    lines_abs_filepath += sm_number_str;
                    lines_abs_filepath += Defaults.CSV;

                    writeLinesToFile(lines_abs_filepath,
                        "middle of lower lines", middle_of_lower_lines, true);


                    for (int lm = 0; lm < middle_of_lower_lines.Count; lm++)
                    {
                        String middle_line_abs_filepath = _results_dir_CString;

                        if (!middle_line_abs_filepath.EndsWith("\\")) middle_line_abs_filepath += "\\";

                        string lm_number_str = lm.ToString();

                        middle_line_abs_filepath += "ImiddleLower_LM";
                        middle_line_abs_filepath += lm_number_str;
                        middle_line_abs_filepath += Defaults.USCORE;
                        middle_line_abs_filepath += Defaults.LASER_TYPE_ID_DSDBR01_CSTRING;
                        middle_line_abs_filepath += Defaults.USCORE;
                        middle_line_abs_filepath += _laser_id_CString;
                        middle_line_abs_filepath += Defaults.USCORE;
                        middle_line_abs_filepath += _date_time_stamp_CString;
                        middle_line_abs_filepath += Defaults.USCORE;
                        middle_line_abs_filepath += "SM";
                        middle_line_abs_filepath += sm_number_str;
                        middle_line_abs_filepath += Defaults.CSV;

                        middle_of_lower_lines[lm].writeCurrentsToFile(middle_line_abs_filepath, true);
                    }

                }
            }

            if (DSDBR01.Instance._DSDBR01_SMBD_write_middle_of_upper_lines_to_file)
            {
                List<CDSDBRSupermodeMapLine> middle_of_upper_lines = new List<CDSDBRSupermodeMapLine>();

                rval = findMiddleLines(_lm_upper_lines, middle_of_upper_lines);

                if (rval == rcode.ok)
                {
                    lines_abs_filepath = _results_dir_CString;

                    if (!lines_abs_filepath.EndsWith("\\")) lines_abs_filepath += "\\";

                    lines_abs_filepath += "middleOfUpperLines_";
                    lines_abs_filepath += Defaults.LASER_TYPE_ID_DSDBR01_CSTRING;
                    lines_abs_filepath += Defaults.USCORE;
                    lines_abs_filepath += _laser_id_CString;
                    lines_abs_filepath += Defaults.USCORE;
                    lines_abs_filepath += _date_time_stamp_CString;
                    lines_abs_filepath += Defaults.USCORE;
                    lines_abs_filepath += "SM";
                    lines_abs_filepath += sm_number_str;
                    lines_abs_filepath += Defaults.CSV;

                    writeLinesToFile(
                        lines_abs_filepath,
                        "middle of upper lines",
                        middle_of_upper_lines,
                        true);

                    for (int lm = 0; lm < middle_of_upper_lines.Count; lm++)
                    {
                        String middle_line_abs_filepath = _results_dir_CString;

                        if (!middle_line_abs_filepath.EndsWith("\\")) middle_line_abs_filepath += "\\";
                        string lm_number_str = lm.ToString();

                        middle_line_abs_filepath += "ImiddleUpper_LM";
                        middle_line_abs_filepath += lm_number_str;
                        middle_line_abs_filepath += Defaults.USCORE;
                        middle_line_abs_filepath += Defaults.LASER_TYPE_ID_DSDBR01_CSTRING;
                        middle_line_abs_filepath += Defaults.USCORE;
                        middle_line_abs_filepath += _laser_id_CString;
                        middle_line_abs_filepath += Defaults.USCORE;
                        middle_line_abs_filepath += _date_time_stamp_CString;
                        middle_line_abs_filepath += Defaults.USCORE;
                        middle_line_abs_filepath += "SM";
                        middle_line_abs_filepath += sm_number_str;
                        middle_line_abs_filepath += Defaults.CSV;

                        middle_of_upper_lines[lm].writeCurrentsToFile(
                            middle_line_abs_filepath,
                            true);
                    }
                }
            }


            if (DSDBR01.Instance._DSDBR01_SMBD_write_boundaries_to_file)
            {

                for (short lm_number = 0; lm_number < (short)(_lm_lower_lines.Count); lm_number++)
                {
                    string lm_number_str = lm_number.ToString();

                    String lm_lower_line_abs_filepath = _results_dir_CString;

                    if (!lm_lower_line_abs_filepath.EndsWith("\\")) lm_lower_line_abs_filepath += "\\";

                    lm_lower_line_abs_filepath += "lower_lm_line_";
                    lm_lower_line_abs_filepath += lm_number_str;
                    lm_lower_line_abs_filepath += ".txt";

                    _lm_lower_lines[lm_number].writeRowColToFile(
                        lm_lower_line_abs_filepath,
                        DSDBR01.Instance._DSDBR01_SMDC_overwrite_existing_files);
                }

                for (short lm_number = 0; lm_number < (short)(_lm_upper_lines.Count); lm_number++)
                {
                    string lm_number_str = lm_number.ToString();

                    String lm_upper_line_abs_filepath = _results_dir_CString;

                    if (!lm_upper_line_abs_filepath.EndsWith("\\")) lm_upper_line_abs_filepath += "\\";

                    lm_upper_line_abs_filepath += "upper_lm_line_";
                    lm_upper_line_abs_filepath += lm_number_str;
                    lm_upper_line_abs_filepath += ".txt";

                    _lm_upper_lines[lm_number].writeRowColToFile(
                        lm_upper_line_abs_filepath,
                        DSDBR01.Instance._DSDBR01_SMDC_overwrite_existing_files);
                }
            }

            return rval;
        }

        rcode writeLMLinesToFile(String abs_filepath,bool overwrite)
        {
            rcode rval = rcode.ok;
            int row_count = 0;
            int col_count = 0;

            if (_lm_upper_lines.Count == 0
             || _lm_lower_lines.Count == 0
             || _lm_middle_lines_of_frequency.Count == 0)
            {
                rval = rcode.no_data;
                CGR_LOG.Write("CDSDBRSuperMode.writeLMLinesToFile() no_data", LoggingLevels.ERROR_CGR_LOG);
            }
            else
            {

                // Check if file already exists and overwrite not specified

                if (File.Exists(abs_filepath) && overwrite == false)
                {
                    rval = rcode.file_already_exists;
                    CGR_LOG.Write("CDSDBRSuperMode.writeLMLinesToFile() file_already_exists", LoggingLevels.ERROR_CGR_LOG);
                }
                else
                {

                    // Ensure the directory exists
                    int index_of_last_dirslash = abs_filepath.LastIndexOf('\\');
                    if (index_of_last_dirslash == -1)
                    {
                        // '\\' not found => not an absolute path
                        rval = rcode.could_not_open_file;
                        CGR_LOG.Write("CDSDBRSuperMode.writeLMLinesToFile() could_not_open_file", LoggingLevels.ERROR_CGR_LOG);
                    }
                    else
                    {
                        try
                        {
                            using (StreamWriter file_stream = new StreamWriter(abs_filepath))
                            {
                                // First write header row
                                file_stream.Write("LM#");
                                file_stream.Write(Defaults.COMMA_CHAR);
                                file_stream.Write("LineType");
                                file_stream.Write(Defaults.COMMA_CHAR);
                                file_stream.Write("Row");
                                file_stream.Write(Defaults.COMMA_CHAR);
                                file_stream.Write("Col");
                                file_stream.WriteLine();

                                for (short lm = 0; lm < (short)_lm_middle_lines_of_frequency.Count; lm++)
                                {
                                    List<CDSDBRSupermodeMapLine.point_type> p_freq_middle_points = _lm_middle_lines_of_frequency[lm]._points;

                                    if (p_freq_middle_points.Count == 0)
                                    {
                                        String log_err_msg = "CDSDBRSuperMode.writeLMLinesToFile(): ";
                                        log_err_msg += string.Format("no middle line data for lm{0:d}", lm);
                                        CGR_LOG.Write(log_err_msg, LoggingLevels.ERROR_CGR_LOG);
                                    }
                                    else
                                    {
                                        for (int i = 0; i < p_freq_middle_points.Count; i++)
                                        {
                                            file_stream.Write(lm);
                                            file_stream.Write(Defaults.COMMA_CHAR);
                                            file_stream.Write("FrequencyMiddle");
                                            file_stream.Write(Defaults.COMMA_CHAR);
                                            file_stream.Write(p_freq_middle_points[i].row);
                                            file_stream.Write(Defaults.COMMA_CHAR);
                                            file_stream.Write(p_freq_middle_points[i].col);
                                            file_stream.WriteLine();
                                        }
                                    }
                                }

                                for (short lm = 0; lm < (short)(_lm_upper_lines.Count); lm++)
                                {
                                    List<CDSDBRSupermodeMapLine.point_type> p_lower_points = _lm_upper_lines[lm]._points;

                                    if (p_lower_points.Count == 0)
                                    {
                                        rval = rcode.no_data;
                                        String log_err_msg = "CDSDBRSuperMode.writeLMLinesToFile(): ";
                                        log_err_msg += string.Format("no lower line data for lm{0:d}", lm);
                                        CGR_LOG.Write(log_err_msg, LoggingLevels.ERROR_CGR_LOG);
                                    }
                                    else
                                    {
                                        for (int i = 0; i < p_lower_points.Count; i++)
                                        {
                                            file_stream.Write(lm);
                                            file_stream.Write(Defaults.COMMA_CHAR);
                                            file_stream.Write("Lower");
                                            file_stream.Write(Defaults.COMMA_CHAR);
                                            file_stream.Write(p_lower_points[i].row);
                                            file_stream.Write(Defaults.COMMA_CHAR);
                                            file_stream.Write(p_lower_points[i].col);
                                            file_stream.WriteLine();
                                        }
                                    }
                                }

                                for (short lm = 0; lm < (short)(_lm_lower_lines.Count); lm++)
                                {

                                    List<CDSDBRSupermodeMapLine.point_type> p_upper_points = _lm_lower_lines[lm]._points;

                                    if (p_upper_points.Count == 0)
                                    {
                                        rval = rcode.no_data;
                                        String log_err_msg = "CDSDBRSuperMode.writeLMLinesToFile(): ";
                                        log_err_msg += string.Format("no upper line data for lm{0:d}", lm - 1);
                                        CGR_LOG.Write(log_err_msg, LoggingLevels.ERROR_CGR_LOG);
                                    }
                                    else
                                    {
                                        for (int i = 0; i < p_upper_points.Count; i++)
                                        {
                                            file_stream.Write(lm - 1);
                                            file_stream.Write(Defaults.COMMA_CHAR);
                                            file_stream.Write("Upper");
                                            file_stream.Write(Defaults.COMMA_CHAR);
                                            file_stream.Write(p_upper_points[i].row);
                                            file_stream.Write(Defaults.COMMA_CHAR);
                                            file_stream.Write(p_upper_points[i].col);
                                            file_stream.WriteLine();
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception)
                        {
                            rval = rcode.could_not_open_file;
                            CGR_LOG.Write("CDSDBRSuperMode.writeLMLinesToFile() file stream threw exception", LoggingLevels.ERROR_CGR_LOG);
                        }
                    }
                }
            }
            return rval;
        }

        rcode writeLinesToFile(
                string abs_filepath,
                string col2_line_type,
                List<CDSDBRSupermodeMapLine> lines,//&
                bool overwrite)
        {
            rcode rval = rcode.ok;
            
            if (lines.Count == 0)
            {
                rval = rcode.no_data;
                CGR_LOG.Write("CDSDBRSuperMode.writeLinesToFile() no_data", LoggingLevels.ERROR_CGR_LOG);
            }
            else
            {
                // Check if file already exists and overwrite not specified
                if (File.Exists(abs_filepath) && overwrite == false)
                {
                    rval = rcode.file_already_exists;
                    CGR_LOG.Write("CDSDBRSuperMode.writeLinesToFile() file_already_exists", LoggingLevels.ERROR_CGR_LOG);
                }
                else
                {
                    // Ensure the directory exists
                    int index_of_last_dirslash = abs_filepath.LastIndexOf('\\');
                    if (index_of_last_dirslash == -1)
                    {
                        // '\\' not found => not an absolute path
                        rval = rcode.could_not_open_file;
                        CGR_LOG.Write("CDSDBRSuperMode.writeLinesToFile() could_not_open_file", LoggingLevels.ERROR_CGR_LOG);
                    }
                    else
                    {
                        try
                        {

                            using (StreamWriter file_stream = new StreamWriter(abs_filepath))
                            {

                                // First write header row
                                file_stream.Write("LM#");
                                file_stream.Write(Defaults.COMMA_CHAR);
                                file_stream.Write("LineType");
                                file_stream.Write(Defaults.COMMA_CHAR);
                                file_stream.Write("Row");
                                file_stream.Write(Defaults.COMMA_CHAR);
                                file_stream.Write("Col");
                                file_stream.WriteLine();

                                for (short lm = 0; lm < (short)(lines.Count); lm++)
                                {

                                    //std.vector<CDSDBRSupermodeMapLine.point_type> *p_points = &(lines[lm]._points);
                                    List<CDSDBRSupermodeMapLine.point_type> p_points = lines[lm]._points;

                                    if (p_points.Count == 0)
                                    {
                                        rval = rcode.no_data;
                                        String log_err_msg = "CDSDBRSuperMode.writeLinesToFile(): ";
                                        log_err_msg += string.Format("no line data for lm{0:d}", lm - 1);
                                        CGR_LOG.Write(log_err_msg, LoggingLevels.ERROR_CGR_LOG);
                                    }
                                    else
                                    {
                                        for (int i = 0; i < p_points.Count; i++)
                                        {
                                            file_stream.Write(lm);
                                            file_stream.Write(Defaults.COMMA_CHAR);
                                            file_stream.Write(col2_line_type);
                                            file_stream.Write(Defaults.COMMA_CHAR);
                                            file_stream.Write(p_points[i].row);
                                            file_stream.Write(Defaults.COMMA_CHAR);
                                            file_stream.Write(p_points[i].col);
                                            file_stream.WriteLine();
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception)
                        {
                            rval = rcode.could_not_open_file;
                            CGR_LOG.Write("CDSDBRSuperMode.writeLinesToFile() file stream threw exception", LoggingLevels.ERROR_CGR_LOG);
                        }
                    }
                }
            }

            return rval;
        }






    }
}

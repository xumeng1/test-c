using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Bookham.TestLibrary.Algorithms
{
    public class CDSDBRFrontCurrents
    {
        public enum rcode
        {
            ok = 0,
            vector_size_rows_cols_do_not_match,
            could_not_open_file,
            file_does_not_exist,
            file_already_exists,
            file_format_invalid
        }

        public struct If_point
        {
            public short front_pair_number;
            public double I_front_1;
            public double I_front_2;
            public double I_front_3;
            public double I_front_4;
            public double I_front_5;
            public double I_front_6;
            public double I_front_7;
            public double I_front_8;
        }

        public int _total_length;
        public List<int> _submap_length=new List<int>();
        public List<int> _submap_start_index = new List<int>();
        public List<If_point> _currents = new List<If_point>();
        public String _abs_filepath;

        

        public void clear()
        {
            _total_length = 0;
            _submap_length.Clear();
            _submap_start_index.Clear();
            _currents.Clear();
            _abs_filepath = string.Empty;
        }

        public rcode readFromFile(String abs_filepath)
        {
            rcode rval = rcode.ok;
            clear();
            int row_count = 0;
            int first_line_col_count = 0;

            // Check file
            if (!File.Exists(abs_filepath))
            {
                return rcode.file_does_not_exist;
            }

            short current_submap = 0;
            int submap_length = 0;

            string[] arr = File.ReadAllLines(abs_filepath);
            row_count = 0;
            for (int i = 1; i < arr.Length; i++)
            {
                short front_pair_number = 0;
                double constant_If = -1;
                double nonconstant_If = -1;

                string[] sarr = arr[i].Split(Defaults.COMMA_CHAR);
                front_pair_number = short.Parse(sarr[0]);
                if (current_submap != front_pair_number)
                {
                    if (current_submap + 1 == front_pair_number)
                    {
                        // found transition between pairs of front sections
                        if (current_submap != 0)
                        {
                            _submap_length.Add(submap_length);
                        }
                        submap_length = 0;
                        current_submap = front_pair_number;
                        _submap_start_index.Add(row_count);
                    }
                    else
                    {
                        rval = rcode.file_format_invalid;
                        break;
                    }
                }
                // read constant_If
                constant_If = double.Parse(sarr[1]);
                // read nonconstant_If
                nonconstant_If = double.Parse(sarr[2]);

                if (front_pair_number > 7 || front_pair_number < 1
                         || constant_If < 0 || nonconstant_If < 0)
                {
                    rval = rcode.file_format_invalid;
                    break;
                }

                If_point new_If_point = new If_point();
                new_If_point.front_pair_number = front_pair_number;
                switch (front_pair_number)
                {

                    case 1:
                        new_If_point.I_front_1 = constant_If;
                        new_If_point.I_front_2 = nonconstant_If;
                        break;
                    case 2:
                        new_If_point.I_front_2 = constant_If;
                        new_If_point.I_front_3 = nonconstant_If;
                        break;
                    case 3:
                        new_If_point.I_front_3 = constant_If;
                        new_If_point.I_front_4 = nonconstant_If;
                        break;
                    case 4:
                        new_If_point.I_front_4 = constant_If;
                        new_If_point.I_front_5 = nonconstant_If;
                        break;
                    case 5:
                        new_If_point.I_front_5 = constant_If;
                        new_If_point.I_front_6 = nonconstant_If;
                        break;
                    case 6:
                        new_If_point.I_front_6 = constant_If;
                        new_If_point.I_front_7 = nonconstant_If;
                        break;
                    case 7:
                        new_If_point.I_front_7 = constant_If;
                        new_If_point.I_front_8 = nonconstant_If;
                        break;
                }
                _currents.Add(new_If_point);

                row_count++;
                submap_length++;

            }
            // record length of final submap
            _submap_length.Add(submap_length);

            if (rval == rcode.ok
                 && (row_count == 0
                   || _currents.Count != row_count
                   || _submap_length.Count != 7
                   || _submap_start_index.Count != 7))
            {
                rval = rcode.file_format_invalid;
            }

            if (rval == rcode.ok)
            {
                _abs_filepath = abs_filepath;
                _total_length = row_count;
            }
            else
            {
                // something is wrong, clear everything!
                clear();
            }

            return rval;
        }

        public rcode writeToFile(String abs_filepath, bool overwrite)
        {
            rcode rval = rcode.ok;
            int row_count = 0;
            // Check if file already exists and overwrite not specified
            if (File.Exists(abs_filepath) && !overwrite)
            {
                rval = rcode.file_already_exists;
                return rval;
            }

            using (StreamWriter sw = new StreamWriter(abs_filepath))
            {
                sw.WriteLine("Front Pair Number{0}Constant Front Current [mA]{0}Non-constant Front Current [mA]", Defaults.COMMA_CHAR);
                row_count = 0;
                foreach (If_point var in _currents)
                {
                    sw.Write(var.front_pair_number);
                    sw.Write(Defaults.COMMA_CHAR);

                    switch (var.front_pair_number)
                    {
                        case 1:
                            sw.Write(var.I_front_1);
                            sw.Write(Defaults.COMMA_CHAR);
                            sw.Write(var.I_front_2);
                            break;
                        case 2:
                            sw.Write(var.I_front_2);
                            sw.Write(Defaults.COMMA_CHAR);
                            sw.Write(var.I_front_3);
                            break;
                        case 3:
                            sw.Write(var.I_front_3);
                            sw.Write(Defaults.COMMA_CHAR);
                            sw.Write(var.I_front_4);
                            break;
                        case 4:
                            sw.Write(var.I_front_4);
                            sw.Write(Defaults.COMMA_CHAR);
                            sw.Write(var.I_front_5);
                            break;
                        case 5:
                            sw.Write(var.I_front_5);
                            sw.Write(Defaults.COMMA_CHAR);
                            sw.Write(var.I_front_6);
                            break;
                        case 6:
                            sw.Write(var.I_front_6);
                            sw.Write(Defaults.COMMA_CHAR);
                            sw.Write(var.I_front_7);
                            break;
                        case 7:
                            sw.Write(var.I_front_7);
                            sw.Write(Defaults.COMMA_CHAR);
                            sw.Write(var.I_front_8);
                            break;
                    }
                    sw.WriteLine();
                }
            }
            _abs_filepath = abs_filepath;
            return rval;
        }

        public bool isEmpty()
        {
            return _currents.Count == 0;
        }

        public List<double> getNonConstantCurrentVector(short pair_num)
        {
            List<double> non_constant_current = new List<double>();
            for (int i = 0; i < _submap_length[pair_num - 1]; i++)
            {
                non_constant_current.Add(getNonConstantCurrent(pair_num, i));
            }

            return non_constant_current;
        }

        public double getNonConstantCurrent(short pair_num, int sub_index)
        {
            double non_constant_current = 0;
            int index = _submap_start_index[pair_num - 1] + sub_index;

            switch (pair_num)
            {
                case 1:
                    non_constant_current = _currents[index].I_front_2;
                    break;
                case 2:
                    non_constant_current = _currents[index].I_front_3;
                    break;
                case 3:
                    non_constant_current = _currents[index].I_front_4;
                    break;
                case 4:
                    non_constant_current = _currents[index].I_front_5;
                    break;
                case 5:
                    non_constant_current = _currents[index].I_front_6;
                    break;
                case 6:
                    non_constant_current = _currents[index].I_front_7;
                    break;
                case 7:
                    non_constant_current = _currents[index].I_front_8;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return non_constant_current;
        }

        public string getNonConstantModuleName(short pair_num)
        {
            String constant_module_name;

            switch (pair_num)
            {
                case 1:
                    constant_module_name = DSDBR01.Instance._DSDBR01_CMDC_front2_module_name;
                    break;
                case 2:
                    constant_module_name = DSDBR01.Instance._DSDBR01_CMDC_front3_module_name;
                    break;
                case 3:
                    constant_module_name = DSDBR01.Instance._DSDBR01_CMDC_front4_module_name;
                    break;
                case 4:
                    constant_module_name = DSDBR01.Instance._DSDBR01_CMDC_front5_module_name;
                    break;
                case 5:
                    constant_module_name = DSDBR01.Instance._DSDBR01_CMDC_front6_module_name;
                    break;
                case 6:
                    constant_module_name = DSDBR01.Instance._DSDBR01_CMDC_front7_module_name;
                    break;
                case 7:
                    constant_module_name = DSDBR01.Instance._DSDBR01_CMDC_front8_module_name;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return constant_module_name;
        }

        public double getConstantCurrent(short pair_num, int sub_index)
        {
            double constant_current = 0;
            int index = _submap_start_index[pair_num - 1] + sub_index;

            switch (pair_num)
            {
                case 1:
                    constant_current = _currents[index].I_front_1;
                    break;
                case 2:
                    constant_current = _currents[index].I_front_2;
                    break;
                case 3:
                    constant_current = _currents[index].I_front_3;
                    break;
                case 4:
                    constant_current = _currents[index].I_front_4;
                    break;
                case 5:
                    constant_current = _currents[index].I_front_5;
                    break;
                case 6:
                    constant_current = _currents[index].I_front_6;
                    break;
                case 7:
                    constant_current = _currents[index].I_front_7;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return constant_current;
        }

        public string getConstantModuleName(short pair_num)
        {
            String constant_module_name;

            switch (pair_num)
            {
                case 1:
                    constant_module_name = DSDBR01.Instance._DSDBR01_CMDC_front1_module_name;
                    break;
                case 2:
                    constant_module_name = DSDBR01.Instance._DSDBR01_CMDC_front2_module_name;
                    break;
                case 3:
                    constant_module_name = DSDBR01.Instance._DSDBR01_CMDC_front3_module_name;
                    break;
                case 4:
                    constant_module_name = DSDBR01.Instance._DSDBR01_CMDC_front4_module_name;
                    break;
                case 5:
                    constant_module_name = DSDBR01.Instance._DSDBR01_CMDC_front5_module_name;
                    break;
                case 6:
                    constant_module_name = DSDBR01.Instance._DSDBR01_CMDC_front6_module_name;
                    break;
                case 7:
                    constant_module_name = DSDBR01.Instance._DSDBR01_CMDC_front7_module_name;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return constant_module_name;
        }

        internal int index_in_submap(int index)
        {
            return index - _submap_start_index[pair_number(index) - 1];
        }

        internal short pair_number(int index)
        {
            short front_pair_number = 0;

            if (index >= _submap_start_index[0] && index < _submap_start_index[1])
                front_pair_number = 1;
            else
                if (index >= _submap_start_index[1] && index < _submap_start_index[2])
                    front_pair_number = 2;
                else
                    if (index >= _submap_start_index[2] && index < _submap_start_index[3])
                        front_pair_number = 3;
                    else
                        if (index >= _submap_start_index[3] && index < _submap_start_index[4])
                            front_pair_number = 4;
                        else
                            if (index >= _submap_start_index[4] && index < _submap_start_index[5])
                                front_pair_number = 5;
                            else
                                if (index >= _submap_start_index[5] && index < _submap_start_index[6])
                                    front_pair_number = 6;
                                else
                                    if (index >= _submap_start_index[6] && index < _total_length)
                                        front_pair_number = 7;

            return front_pair_number;
        }


    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.Algorithms
{
    static class CGSystemErrors
    {
        // Add errors to list below
        public const string CGSYSTEM_SUCCESS = "Success";
        public const string CGSYSTEM_UNKNOWN_ERROR = "Unknown Error";
        public const string CGSYSTEM_INVALID_STATE_ERROR = "Function called is not allowed in the system's current state";
        public const string CGSYSTEM_NULL_PTR_ERROR = "A pointer argument passed a NULL pointer";
        public const string CGSYSTEM_INVALID_MODULE_NAME_ERROR = "Module name argument is invalid";
        public const string CGSYSTEM_NULL_VARIANT_ARG_ERROR = "A VARIANT argument contains NULL";
        public const string CGSYSTEM_EMPTY_VARIANT_ARG_ERROR = "A VARIANT argument contains an empty array";
        public const string CGSYSTEM_NOT1DDOUBLEARRAY_VARIANT_ARG_ERROR = "A VARIANT argument does not contain a 1-D array of doubles";
        public const string CGSYSTEM_UNKNOWN_STATE_ERROR = "The system is in an unknown state";
        public const string CGSYSTEM_EMPTY_STRING_ARG_ERROR = "A string argument is empty";
        public const string CGSYSTEM_DATETIMESTAMP_ERROR = "Date-Time Stamp argument is not of the correct format or valid date-time";
        public const string CGSYSTEM_DIRECTORY_ERROR = "Directory path argument does not exist or is not a directory";
        public const string CGSYSTEM_LASERTYPE_ERROR = "Laser Type argument is invalid";
        public const string CGSYSTEM_LASER_ID_ERROR = "Laser ID argument is invalid";
        public const string CGSYSTEM_FILE_DOES_NOT_EXIST_ERROR = "File(s) to open could not be found, check input arguments";
        public const string CGSYSTEM_FILEOPEN_ERROR = "An error occurred trying to open a file";
        public const string CGSYSTEM_INVALID_FILE_FORMAT_ERROR = "A file opened for reading was not in the correct format";
        public const string CGSYSTEM_FILES_NOT_MATCHING_SIZES_ERROR = "Files opened for reading are not the same size";
        public const string CGSYSTEM_VARIANT_CREATION_ERROR = "An error occurred trying to create a VARIANT to return, check VARIANT* argument";
        public const string CGSYSTEM_INVALID_MAP_RAMP_DIRECTION_ERROR = "The map ramp direction argument is an invalid value";
        public const string CGSYSTEM_INVALID_CHANNEL_ERROR = "The channel argument is an invalid value";
        public const string CGSYSTEM_FUNCTION_NOT_ALLOWED_STATE_ERROR = "The function called cannot be used in the current system state";
        public const string CGSYSTEM_NO_VALID_SUPERMODES_FOUND = "No valid supermodes were found in the overall map";
        public const string CGSYSTEM_SPECIFIED_SUPERMODE_NOT_FOUND = "The supermode number specified as an argument is not available";
        public const string CGSYSTEM_SPECIFIED_SUPERMODE_NOT_LOADED = "The supermode number specified as an argument is not loaded";
        public const string CGSYSTEM_FUNCTION_NOT_IMPLEMENTED = "The function called is not yet implemented";
        public const string CGSYSTEM_ERROR_WRITING_RESULTS_TO_FILE = "An error occurred when trying to write results to file";
        public const string CGSYSTEM_REGISTRY_ERROR = "Registry data is invalid. See log file for details.";
        public const string CGSYSTEM_RESULTS_DISK_FULL_ERROR = "Error writing results to file. Disk is full.";
        public const string CGSYSTEM_SEQUENCING_STEP_SIZE_ERROR = "Error running sequence. Current increment in sequence is too big.";
        public const string CGSYSTEM_HARDWARE_ERROR = "Error encountered with hardware. See log for details.";
        public const string CGSYSTEM_CONNECTIVITY_ERROR = "Connectivity test failed.";
        public const string CGSYSTEM_POWER_METER_NOT_FOUND_ERROR = "Could not find power meter in system.";
        public const string CGSYSTEM_WAVEMETER_NOT_FOUND_ERROR = "Could not find wavemeter in system.";
        public const string CGSYSTEM_TEC_NOT_FOUND_ERROR = "Could not find TEC in system.";
        public const string CGSYSTEM_POWER_READ_ERROR = "Error reading from Power Meter.";
        public const string CGSYSTEM_DUPLICATE_DEVICE_NAME_ERROR = "Duplicate device names found in system.";
        public const string CGSYSTEM_EMPTY_SEQUENCE_SLOTS_ERROR = "Error running sequence. Empty slots were found between sequencing devices.";
        public const string CGSYSTEM_PXIT_MODULE_ERROR = "Error detected on device. Please see log for details.";
        public const string CGSYSTEM_LASER_ALIGNMENT_ERROR = "Laser failed alignment test.";
        public const string CGSYSTEM_SYSTEM_ERROR = "System Error detected. Please see log for details.";
        public const string CGSYSTEM_LOGFILE_ERROR = "Error detected with log file.";
        public const string CGSYSTEM_SET_CURRENT_ERROR = "Error setting current on device.";
        public const string CGSYSTEM_SEQUENCING_ERROR = "Error running sequence. See log for details.";
        public const string CGSYSTEM_DEVICE_NAME_NOT_ON_CARD_ERROR = "Registry device name conflicts with device name stored on module.";
        public const string CGSYSTEM_306_FIRMWARE_OBSOLETE_ERROR = "Firmware version on 306 device is obsolete.";
        public const string CGSYSTEM_COARSEFREQSAMPLENUM_ARG_ERROR = "The argument num_of_sample_pts must be greater than num_of_poly_coeffs";
        public const string CGSYSTEM_COARSEFREQMAXSAMPLENUM_ERROR = "The maximum number of sample pts has been exceeded";
        public const string CGSYSTEM_COARSEFREQCOEFFS_ARG_ERROR = "The argument num_of_poly_coeffs is outside the range of valid values";
        public const string CGSYSTEM_POWERRATIO_ARG_ERROR = "The argument power_ratio is outside the range of valid values [0:1]";
        public const string CGSYSTEM_COARSEFREQPOLYNOTLOADED_ERROR = "No valid coarse frequency polynomial is loaded";
        public const string CGSYSTEM_COARSEFREQPOLYINVALID_ERROR = "The argument frequency polynomial is not within valid value limits";
        public const string CGSYSTEM_MODULE_NAME_NOT_FOUND_ERROR = "The module name argument was not found";
        public const string CGSYSTEM_POWER_RATIO_OUT_OF_RANGE_ERROR = "The measured power ratio is not in the valid range [0:1]";
        public const string CGSYSTEM_COARSEFREQPOLYDATACOLLECTION_ERROR = "Required data not collected for coarse frequency polynomial calculation";
        public const string CGSYSTEM_WAVEMETER_MEASUREMENT_ERROR = "Could not read valid frequency from wavemeter.";
        public const string CGSYSTEM_OPERATION_POINT_APPLY_ERROR = "Error occurred trying to apply an operating point.";
        public const string CGSYSTEM_UNSTABLE_OPERATION_POINTS_ERROR = "Unstable operating points were encountered";
        public const string CGSYSTEM_ITUGRID_NOT_ALL_CHANNELS_FOUND_ERROR = "Not all defined ITU grid channels were found";
        public const string CGSYSTEM_MAPS_NOT_LOADED_ERROR = "Valid maps have not been loaded";
        public const string CGSYSTEM_CURRENTS_NOT_LOADED_ERROR = "Valid current vectors have not been loaded";
        public const string CGSYSTEM_CORRUPT_MAP_ERROR = "An internal map is corrupt";
        public const string CGSYSTEM_BOUNDARY_DETECTION_ERROR = "A boundary detection error was detected";
        public const string CGSYSTEM_NO_DATA_ERROR = "Internal data is missing";
        public const string CGSYSTEM_ERROR_READING_FROM_FILE = "Error occurred while attempting to read a file";
        public const string CGSYSTEM_NO_VALID_LONGITUDINAL_MODES_FOUND = "No valid longitudinal modes were found in the supermode map";
        public const string CGSYSTEM_PXITRELAY_ERROR = "Error calling PXITRelay dll function";
        public const string CGSYSTEM_INVALID_ARGUMENT = "Invalid argument encountered";
        public const string CGSYSTEM_POWER_READING_TOO_LOW_ERROR = "Power reading deemed to be too low - possibly DUT is not lasing.";
        public const string CGSYSTEM_VARIANT_ARG_TOO_LARGE_ERROR = "A VARIANT argument contains an array that is too large";
        public const string CGSYSTEM_WORKER_THREAD_CREATION_ERROR = "An error occured while trying to create a worker thread";
        public const string CGSYSTEM_INVALID_MAP_TYPE_ERROR = "A map type argument was passed with a bad value";
        public const string CGSYSTEM_SM_NOT_FOUND_ERROR = "Requested supermode not found";
        public const string CGSYSTEM_LM_NOT_FOUND_ERROR = "Requested longitudinal mode not found";
        public const string CGSYSTEM_INDEX_NOT_FOUND_ERROR = "Requested index not found";
        public const string CGSYSTEM_GET_OUTPUT_CONNECTION_ERROR = "An error occurred while trying to assess the output state of a current source";
        public const string CGSYSTEM_REQUIRED_OUTPUTS_NOT_CONNECTED_ERROR = "The outputs required to represent a point on a map are not connected or don't have a front pair chosen";
        public const string CGSYSTEM_GET_CURRENT_ERROR = "An error occurred while trying to assess the output current of a current source";



    }
}

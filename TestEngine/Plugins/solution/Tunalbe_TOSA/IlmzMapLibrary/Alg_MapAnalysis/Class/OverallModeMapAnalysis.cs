using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.Algorithms
{
    internal class OverallModeMapAnalysis
    {
        public enum rcode
        {
            ok = 0
        }

        const int MIN_NUMBER_OF_MODES = 5;

        const int RESULTS_NO_ERROR = 0;
        const int RESULTS_GENERAL_ERROR = 1;
        const int RESULTS_NO_MODEMAP_DATA = 2;
        const int RESULTS_PO_TOO_LOW = 4;
        const int RESULTS_PO_TOO_HIGH = 8;
        const int RESULTS_TOO_MANY_JUMPS = 16;
        const int RESULTS_TOO_FEW_JUMPS = 32;
        const int RESULTS_INVALID_NUMBER_OF_LINES = 64;
        const int RESULTS_INVALID_LINES_REMOVED = 128;
        const int RESULTS_LINES_MISSING = 256;

        #region Variants

        ///////////////////////////////////////////////////////
        ///
        //	private attributes
        //
        // vector of mode width vectors
        List<Vector> _modeWidths = new List<Vector>();
        List<double> _maxModeWidths = new List<double>();
        List<double> _minModeWidths = new List<double>();
        List<double> _meanModeWidths = new List<double>();
        List<double> _sumModeWidths = new List<double>();
        List<double> _varModeWidths = new List<double>();
        List<double> _stdDevModeWidths = new List<double>();
        List<double> _medianModeWidths = new List<double>();
        List<double> _numModeWidthSamples = new List<double>();

        ///////////////////////////////////////

        List<Vector> _modeUpperSlopes = new List<Vector>();
        List<double> _maxModeUpperSlopes = new List<double>();
        List<double> _minModeUpperSlopes = new List<double>();
        List<double> _meanModeUpperSlopes = new List<double>();
        List<double> _sumModeUpperSlopes = new List<double>();
        List<double> _varModeUpperSlopes = new List<double>();
        List<double> _stdDevModeUpperSlopes = new List<double>();
        List<double> _medianModeUpperSlopes = new List<double>();
        List<double> _numModeUpperSlopeSamples = new List<double>();

        ///////////////////////////////////////

        List<Vector> _modeLowerSlopes = new List<Vector>();
        List<double> _maxModeLowerSlopes = new List<double>();
        List<double> _minModeLowerSlopes = new List<double>();
        List<double> _meanModeLowerSlopes = new List<double>();
        List<double> _sumModeLowerSlopes = new List<double>();
        List<double> _varModeLowerSlopes = new List<double>();
        List<double> _stdDevModeLowerSlopes = new List<double>();
        List<double> _medianModeLowerSlopes = new List<double>();
        List<double> _numModeLowerSlopeSamples = new List<double>();

        ///////////////////////////////////////

        List<double> _lowerModalDistortionAngles = new List<double>();
        List<double> _lowerModalDistortionAngleXPositions = new List<double>();

        List<double> _upperModalDistortionAngles = new List<double>();
        List<double> _upperModalDistortionAngleXPositions = new List<double>();

        ///////////////////////////////////////

        List<Vector> _lowerModeBDCAreas = new List<Vector>();
        List<double> _lowerMaxModeBDCAreas = new List<double>();
        List<double> _lowerMinModeBDCAreas = new List<double>();
        List<double> _lowerMeanModeBDCAreas = new List<double>();
        List<double> _lowerSumModeBDCAreas = new List<double>();
        List<double> _lowerVarModeBDCAreas = new List<double>();
        List<double> _lowerStdDevModeBDCAreas = new List<double>();
        List<double> _lowerMedianModeBDCAreas = new List<double>();
        List<double> _lowerNumModeBDCAreaSamples = new List<double>();

        List<Vector> _upperModeBDCAreas = new List<Vector>();
        List<double> _upperMaxModeBDCAreas = new List<double>();
        List<double> _upperMinModeBDCAreas = new List<double>();
        List<double> _upperMeanModeBDCAreas = new List<double>();
        List<double> _upperSumModeBDCAreas = new List<double>();
        List<double> _upperVarModeBDCAreas = new List<double>();
        List<double> _upperStdDevModeBDCAreas = new List<double>();
        List<double> _upperMedianModeBDCAreas = new List<double>();
        List<double> _upperNumModeBDCAreaSamples = new List<double>();

        ///////////////////////////////////////

        List<Vector> _lowerModeBDCAreaXLengths = new List<Vector>();
        List<double> _lowerMaxModeBDCAreaXLengths = new List<double>();
        List<double> _lowerMinModeBDCAreaXLengths = new List<double>();
        List<double> _lowerMeanModeBDCAreaXLengths = new List<double>();
        List<double> _lowerSumModeBDCAreaXLengths = new List<double>();
        List<double> _lowerVarModeBDCAreaXLengths = new List<double>();
        List<double> _lowerStdDevModeBDCAreaXLengths = new List<double>();
        List<double> _lowerMedianModeBDCAreaXLengths = new List<double>();
        List<double> _lowerNumModeBDCAreaXLengthSamples = new List<double>();

        List<Vector> _upperModeBDCAreaXLengths = new List<Vector>();
        List<double> _upperMaxModeBDCAreaXLengths = new List<double>();
        List<double> _upperMinModeBDCAreaXLengths = new List<double>();
        List<double> _upperMeanModeBDCAreaXLengths = new List<double>();
        List<double> _upperSumModeBDCAreaXLengths = new List<double>();
        List<double> _upperVarModeBDCAreaXLengths = new List<double>();
        List<double> _upperStdDevModeBDCAreaXLengths = new List<double>();
        List<double> _upperMedianModeBDCAreaXLengths = new List<double>();
        List<double> _upperNumModeBDCAreaXLengthSamples = new List<double>();

        ///////////////////////////////////////

        List<double> _middleLineRMSValues = new List<double>();
        List<double> _middleLineSlopes = new List<double>();

        ///////////////////////////////////////

        Vector _continuityValues = new Vector();
        double _maxContinuityValue;
        double _minContinuityValue;
        double _meanContinuityValues;
        double _sumContinuityValues;
        double _varContinuityValues;
        double _stdDevContinuityValues;
        double _medianContinuityValues;
        double _numContinuityValues;

        double _maxPrGap;
        double _maxLowerPr;
        double _minUpperPr;

        //
        ///////////////////////////////////////

        int _numSuperModes;
        //List<CDSDBRSuperMode> *_p_superModes;
        List<CDSDBRSuperMode> _p_superModes = new List<CDSDBRSuperMode>();

        short _numSuperModesRemoved;

        string _resultsDir;
        string _laserId;
        string _dateTimeStamp;

        int _xAxisLength;
        int _yAxisLength;

        ///////////////////////////////////

        string _qaResultsAbsFilePath;
        string _passFailResultsAbsFilePath;
        string _collatedPassFailResultsAbsFilePath;

        ///////////////////////////////////

        bool _qaAnalysisComplete;
        bool _passFailAnalysisComplete;

        
        PassFailThresholds _smPassFailThresholds;

        //Data
        PassFailAnalysis _maxModeBordersRemovedAnalysis = new PassFailAnalysis();
        PassFailAnalysis _maxModeWidthPFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _minModeWidthPFAnalysis = new PassFailAnalysis();

        PassFailAnalysis _lowerMaxModeBDCAreaPFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _lowerMaxModeBDCAreaXLengthPFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _lowerSumModeBDCAreasPFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _lowerNumModeBDCAreasPFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _lowerModalDistortionAnglePFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _lowerModalDistortionAngleXPositionPFAnalysis = new PassFailAnalysis();

        PassFailAnalysis _upperMaxModeBDCAreaPFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _upperMaxModeBDCAreaXLengthPFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _upperSumModeBDCAreasPFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _upperNumModeBDCAreasPFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _upperModalDistortionAnglePFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _upperModalDistortionAngleXPositionPFAnalysis = new PassFailAnalysis();

        PassFailAnalysis _middleLineRMSValuesPFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _maxMiddleLineSlopePFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _minMiddleLineSlopePFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _maxPrGapPFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _maxLowerPrPFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _minUpperPrPFAnalysis = new PassFailAnalysis();

        PassFailAnalysis _lowerMaxModeLineSlopePFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _upperMaxModeLineSlopePFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _lowerMinModeLineSlopePFAnalysis = new PassFailAnalysis();
        PassFailAnalysis _upperMinModeLineSlopePFAnalysis = new PassFailAnalysis();

        //
        /////////////////////////////////////////////////////////////////

        #endregion

        internal bool _screeningPassed;

        int _qaDetectedError;

        public OverallModeMapAnalysis()
        {
            init();
        }

        internal void init()
        {
            ////////////////////////////////////////////////
            //	QA Data
            //
            _modeWidths.Clear();
            _maxModeWidths.Clear();
            _minModeWidths.Clear();
            _meanModeWidths.Clear();
            _sumModeWidths.Clear();
            _varModeWidths.Clear();
            _stdDevModeWidths.Clear();
            _medianModeWidths.Clear();
            _numModeWidthSamples.Clear();

            ///////////////////////////////////////

            _modeUpperSlopes.Clear();
            _maxModeUpperSlopes.Clear();
            _minModeUpperSlopes.Clear();
            _meanModeUpperSlopes.Clear();
            _sumModeUpperSlopes.Clear();
            _varModeUpperSlopes.Clear();
            _stdDevModeUpperSlopes.Clear();
            _medianModeUpperSlopes.Clear();
            _numModeUpperSlopeSamples.Clear();

            ///////////////////////////////////////

            _modeLowerSlopes.Clear();
            _maxModeLowerSlopes.Clear();
            _minModeLowerSlopes.Clear();
            _meanModeLowerSlopes.Clear();
            _sumModeLowerSlopes.Clear();
            _varModeLowerSlopes.Clear();
            _stdDevModeLowerSlopes.Clear();
            _medianModeLowerSlopes.Clear();
            _numModeLowerSlopeSamples.Clear();

            ////////////////////////////////////////

            _lowerModalDistortionAngles.Clear();
            _upperModalDistortionAngles.Clear();

            ///////////////////////////////////////

            _lowerModeBDCAreas.Clear();
            _lowerMaxModeBDCAreas.Clear();
            _lowerMinModeBDCAreas.Clear();
            _lowerMeanModeBDCAreas.Clear();
            _lowerSumModeBDCAreas.Clear();
            _lowerVarModeBDCAreas.Clear();
            _lowerStdDevModeBDCAreas.Clear();
            _lowerMedianModeBDCAreas.Clear();
            _lowerNumModeBDCAreaSamples.Clear();

            _upperModeBDCAreas.Clear();
            _upperMaxModeBDCAreas.Clear();
            _upperMinModeBDCAreas.Clear();
            _upperMeanModeBDCAreas.Clear();
            _upperSumModeBDCAreas.Clear();
            _upperVarModeBDCAreas.Clear();
            _upperStdDevModeBDCAreas.Clear();
            _upperMedianModeBDCAreas.Clear();
            _upperNumModeBDCAreaSamples.Clear();

            ///////////////////////////////////////

            _lowerModeBDCAreaXLengths.Clear();
            _lowerMaxModeBDCAreaXLengths.Clear();
            _lowerMinModeBDCAreaXLengths.Clear();
            _lowerMeanModeBDCAreaXLengths.Clear();
            _lowerSumModeBDCAreaXLengths.Clear();
            _lowerVarModeBDCAreaXLengths.Clear();
            _lowerStdDevModeBDCAreaXLengths.Clear();
            _lowerMedianModeBDCAreaXLengths.Clear();
            _lowerNumModeBDCAreaXLengthSamples.Clear();

            _upperModeBDCAreaXLengths.Clear();
            _upperMaxModeBDCAreaXLengths.Clear();
            _upperMinModeBDCAreaXLengths.Clear();
            _upperMeanModeBDCAreaXLengths.Clear();
            _upperSumModeBDCAreaXLengths.Clear();
            _upperVarModeBDCAreaXLengths.Clear();
            _upperStdDevModeBDCAreaXLengths.Clear();
            _upperMedianModeBDCAreaXLengths.Clear();
            _upperNumModeBDCAreaXLengthSamples.Clear();

            ///////////////////////////////////////

            _middleLineRMSValues.Clear();
            _middleLineSlopes.Clear();

            ///////////////////////////////////////

            _maxContinuityValue = 0;
            _minContinuityValue = 0;
            _meanContinuityValues = 0;
            _sumContinuityValues = 0;
            _varContinuityValues = 0;
            _stdDevContinuityValues = 0;
            _medianContinuityValues = 0;
            _numContinuityValues = 0;
            _maxPrGap = 0;
            _maxLowerPr = 0;
            _minUpperPr = 0;

            ///////////////////////////////////////

            _qaAnalysisComplete = false;

            //
            //////////////////////////////////////////////////////

            //////////////////////////////////////////////////////
            ///
            //	Pass Fail Analysis
            //
            _passFailAnalysisComplete = false;

            _maxModeBordersRemovedAnalysis.init();
            _maxModeWidthPFAnalysis.init();
            _minModeWidthPFAnalysis.init();

            _lowerMaxModeBDCAreaPFAnalysis.init();
            _lowerMaxModeBDCAreaXLengthPFAnalysis.init();
            _lowerSumModeBDCAreasPFAnalysis.init();
            _lowerNumModeBDCAreasPFAnalysis.init();

            _upperMaxModeBDCAreaPFAnalysis.init();
            _upperMaxModeBDCAreaXLengthPFAnalysis.init();
            _upperSumModeBDCAreasPFAnalysis.init();
            _upperNumModeBDCAreasPFAnalysis.init();

            _middleLineRMSValuesPFAnalysis.init();
            _maxMiddleLineSlopePFAnalysis.init();
            _minMiddleLineSlopePFAnalysis.init();
            _maxPrGapPFAnalysis.init();
            _maxLowerPrPFAnalysis.init();
            _minUpperPrPFAnalysis.init();


            _lowerMaxModeLineSlopePFAnalysis.init();
            _upperMaxModeLineSlopePFAnalysis.init();
            _lowerMinModeLineSlopePFAnalysis.init();
            _upperMinModeLineSlopePFAnalysis.init();

            //
            //////////////////////////////////////////////////////

            _screeningPassed = false;

            _qaDetectedError = RESULTS_NO_ERROR;

        }

        public rcode runAnalysis(List<CDSDBRSuperMode> p_superModes,
                        int xAxisLength,
                        int yAxisLength,
                        List<double> continuityValues,
                        short superModesRemoved,
                        String resultsDir,
                        String laserId,
                        String dateTimeStamp)
        {
            _p_superModes = p_superModes;
            _numSuperModes = p_superModes.Count;

            _numSuperModesRemoved = superModesRemoved;

            _qaResultsAbsFilePath = createQaResultsAbsFilePath(resultsDir,
                                                            laserId,
                                                            dateTimeStamp);

            _passFailResultsAbsFilePath = createPassFailResultsAbsFilePath(resultsDir,
                                                                    laserId,
                                                                    dateTimeStamp);

            _collatedPassFailResultsAbsFilePath = createPassFailCollateResultsAbsFilePath(resultsDir,
                                                                                        laserId,
                                                                                        dateTimeStamp);
            _resultsDir = resultsDir;
            _laserId = laserId;
            _dateTimeStamp = dateTimeStamp;

            _xAxisLength = xAxisLength;
            _yAxisLength = yAxisLength;

            _continuityValues.op_Equal(continuityValues);

            rcode err = rcode.ok;

            err = runQaAnalysis();
            if (err == rcode.ok)
                err = runPassFailAnalysis();//todo:?

            err = writeQaResultsToFile();

            CLoseGridFile passFailResults = new CLoseGridFile(_passFailResultsAbsFilePath);
            passFailResults.createFolderAndFile();

            err = writePassFailResultsToFile(passFailResults);

            if (err == rcode.ok)
            {
                PassFailCollated.instance.setFileParameters(_resultsDir, _laserId, _dateTimeStamp);
                PassFailCollated.instance.clearSuperModes();
                PassFailCollated.instance.setOverallModeMapAnalysis(this);
            }

            return err;
        }

        public int numSuperModes
        {
            get
            {
                return _numSuperModes;
            }
        }


        string createQaResultsAbsFilePath(string resultsDir, string laserId, string dateTimeStamp)
        {
            string absFilePath = "";

            absFilePath += resultsDir;
            absFilePath += "\\";
            absFilePath += Defaults.QA_METRICS_FILE_NAME;
            absFilePath += Defaults.USCORE;
            absFilePath += "OverallMap";
            absFilePath += Defaults.USCORE;
            absFilePath += Defaults.LASER_TYPE_ID_DSDBR01_CSTRING;
            absFilePath += Defaults.USCORE;
            absFilePath += laserId;
            absFilePath += Defaults.USCORE;
            absFilePath += dateTimeStamp;
            absFilePath += Defaults.CSV;

            return absFilePath;
        }

        string createPassFailResultsAbsFilePath(string resultsDir, string laserId, string dateTimeStamp)
        {
            string absFilePath = "";

            absFilePath += resultsDir;
            absFilePath += "\\";
            absFilePath += Defaults.PASSFAIL_FILE_NAME;
            absFilePath += Defaults.USCORE;
            absFilePath += "OverallMap";
            absFilePath += Defaults.USCORE;
            absFilePath += Defaults.LASER_TYPE_ID_DSDBR01_CSTRING;
            absFilePath += Defaults.USCORE;
            absFilePath += laserId;
            absFilePath += Defaults.USCORE;
            absFilePath += dateTimeStamp;
            absFilePath += Defaults.CSV;

            return absFilePath;
        }

        string createPassFailCollateResultsAbsFilePath(string resultsDir, string laserId, string dateTimeStamp)
        {
            string absFilePath = "";

            absFilePath += resultsDir;
            absFilePath += "\\";
            absFilePath += Defaults.COLLATED_PASSFAIL_FILE_NAME;
            absFilePath += Defaults.USCORE;
            absFilePath += Defaults.LASER_TYPE_ID_DSDBR01_CSTRING;
            absFilePath += Defaults.USCORE;
            absFilePath += laserId;
            absFilePath += Defaults.USCORE;
            absFilePath += dateTimeStamp;
            absFilePath += Defaults.CSV;

            return absFilePath;
        }


        rcode runQaAnalysis()
        {
            rcode err = rcode.ok;

            for (int i = 0; i < _numSuperModes; i++)
            {
                _p_superModes[i].runModeAnalysis();
            }

            getModeWidths();
            getMaxModeWidths();
            getMinModeWidths();
            getMeanModeWidths();
            getSumModeWidths();
            getVarModeWidths();
            getStdDevModeWidths();
            getMedianModeWidths();
            getNumModeWidthSamples();

            getModeUpperSlopes();
            getMaxModeUpperSlopes();
            getMinModeUpperSlopes();
            getMeanModeUpperSlopes();
            getSumModeUpperSlopes();
            getVarModeUpperSlopes();
            getStdDevModeUpperSlopes();
            getMedianModeUpperSlopes();
            getNumModeUpperSlopeSamples();

            getModeLowerSlopes();
            getMaxModeLowerSlopes();
            getMinModeLowerSlopes();
            getMeanModeLowerSlopes();
            getSumModeLowerSlopes();
            getVarModeLowerSlopes();
            getStdDevModeLowerSlopes();
            getMedianModeLowerSlopes();
            getNumModeLowerSlopeSamples();

            getModalDistortionAngles();
            getModalDistortionAngleXPositions();

            getModeBDCAreas();
            getMaxModeBDCAreas();
            getMinModeBDCAreas();
            getMeanModeBDCAreas();
            getSumModeBDCAreas();
            getVarModeBDCAreas();
            getStdDevModeBDCAreas();
            getMedianModeBDCAreas();
            getNumModeBDCAreaSamples();

            getMaxModeBDCAreaXLengths();

            //////////////////////////////////

            getMiddleLineRMSValues();

            /////////////////////////////////

            getMiddleLineSlopes();

            /////////////////////////////////

            getMaxContinuityValue();
            getMinContinuityValue();
            getMeanContinuityValues();
            getSumContinuityValues();
            getVarContinuityValues();
            getStdDevContinuityValues();
            getMedianContinuityValues();
            getNumContinuityValues();
            getMaxPrGap();
            getMaxLowerPr();
            getMinUpperPr();
            //
            ///
            ////////////////////////////////////////////////////////////////////////////////////
            ///
            //


            string error_message = "";
            int error_number = RESULTS_NO_ERROR;
            if (_numSuperModes < MIN_NUMBER_OF_MODES)
            {
                _qaDetectedError |= RESULTS_GENERAL_ERROR | RESULTS_INVALID_NUMBER_OF_LINES;
            }

            getQAError(_qaDetectedError, out error_number, out error_message);

            if (_qaDetectedError > RESULTS_NO_ERROR)
                _screeningPassed = false;


            _qaAnalysisComplete = true;

            return err;
        }


        rcode runPassFailAnalysis()
        {
            rcode err = rcode.ok;

            if (_qaAnalysisComplete)
            {
                _screeningPassed = true;

                _smPassFailThresholds = getPassFailThresholds();

                // Set thresholds for this mode
                _maxModeBordersRemovedAnalysis.threshold = _smPassFailThresholds._maxModeBordersRemoved;
                _maxModeWidthPFAnalysis.threshold = _smPassFailThresholds._maxModeWidth;
                _minModeWidthPFAnalysis.threshold = _smPassFailThresholds._minModeWidth;

                _lowerMaxModeBDCAreaPFAnalysis.threshold = _smPassFailThresholds._maxModeBDCArea;
                _lowerMaxModeBDCAreaXLengthPFAnalysis.threshold = _smPassFailThresholds._maxModeBDCAreaXLength;
                _lowerSumModeBDCAreasPFAnalysis.threshold = _smPassFailThresholds._sumModeBDCAreas;
                _lowerNumModeBDCAreasPFAnalysis.threshold = _smPassFailThresholds._numModeBDCAreas;

                _upperMaxModeBDCAreaPFAnalysis.threshold = _smPassFailThresholds._maxModeBDCArea;
                _upperMaxModeBDCAreaXLengthPFAnalysis.threshold = _smPassFailThresholds._maxModeBDCAreaXLength;
                _upperSumModeBDCAreasPFAnalysis.threshold = _smPassFailThresholds._sumModeBDCAreas;
                _upperNumModeBDCAreasPFAnalysis.threshold = _smPassFailThresholds._numModeBDCAreas;

                _lowerModalDistortionAnglePFAnalysis.threshold = _smPassFailThresholds._modalDistortionAngle;
                _lowerModalDistortionAngleXPositionPFAnalysis.threshold = _smPassFailThresholds._modalDistortionAngleXPosition;

                _upperModalDistortionAnglePFAnalysis.threshold = _smPassFailThresholds._modalDistortionAngle;
                _upperModalDistortionAngleXPositionPFAnalysis.threshold = _smPassFailThresholds._modalDistortionAngleXPosition;

                _middleLineRMSValuesPFAnalysis.threshold = _smPassFailThresholds._middleLineRMSValue;
                _maxMiddleLineSlopePFAnalysis.threshold = _smPassFailThresholds._maxMiddleLineSlope;
                _minMiddleLineSlopePFAnalysis.threshold = _smPassFailThresholds._minMiddleLineSlope;
                _maxPrGapPFAnalysis.threshold = _smPassFailThresholds._maxPrGap;
                _maxLowerPrPFAnalysis.threshold = _smPassFailThresholds._maxLowerPr;
                _minUpperPrPFAnalysis.threshold = _smPassFailThresholds._minUpperPr;

                _lowerMaxModeLineSlopePFAnalysis.threshold = _smPassFailThresholds._maxModeLineSlope;
                _upperMaxModeLineSlopePFAnalysis.threshold = _smPassFailThresholds._maxModeLineSlope;

                _lowerMinModeLineSlopePFAnalysis.threshold = _smPassFailThresholds._minModeLineSlope;
                _upperMinModeLineSlopePFAnalysis.threshold = _smPassFailThresholds._minModeLineSlope;

                // run pass fail
                _maxModeWidthPFAnalysis.runPassFailWithUpperLimit(_maxModeWidths);
                _minModeWidthPFAnalysis.runPassFailWithLowerLimit(_minModeWidths);

                _lowerMaxModeBDCAreaPFAnalysis.runPassFailWithUpperLimit(_lowerMaxModeBDCAreas);
                _lowerMaxModeBDCAreaXLengthPFAnalysis.runPassFailWithUpperLimit(_lowerMaxModeBDCAreaXLengths);
                _lowerSumModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_lowerSumModeBDCAreas);
                _lowerNumModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_lowerNumModeBDCAreaSamples);

                _upperMaxModeBDCAreaPFAnalysis.runPassFailWithUpperLimit(_upperMaxModeBDCAreas);
                _upperMaxModeBDCAreaXLengthPFAnalysis.runPassFailWithUpperLimit(_upperMaxModeBDCAreaXLengths);
                _upperSumModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_upperSumModeBDCAreas);
                _upperNumModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_upperNumModeBDCAreaSamples);

                _lowerModalDistortionAnglePFAnalysis.runPassFailWithUpperLimit(_lowerModalDistortionAngles);
                _lowerModalDistortionAngleXPositionPFAnalysis.runPassFailWithUpperLimit(_lowerModalDistortionAngleXPositions);

                _upperModalDistortionAnglePFAnalysis.runPassFailWithUpperLimit(_upperModalDistortionAngles);
                _upperModalDistortionAngleXPositionPFAnalysis.runPassFailWithUpperLimit(_upperModalDistortionAngleXPositions);

                _middleLineRMSValuesPFAnalysis.runPassFailWithUpperLimit(_middleLineRMSValues);
                _maxMiddleLineSlopePFAnalysis.runPassFailWithUpperLimit(_middleLineSlopes);
                _minMiddleLineSlopePFAnalysis.runPassFailWithLowerLimit(_middleLineSlopes);

                _maxPrGapPFAnalysis.runPassFailWithUpperLimit(_maxPrGap);
                _maxLowerPrPFAnalysis.runPassFailWithUpperLimit(_maxLowerPr);
                _minUpperPrPFAnalysis.runPassFailWithLowerLimit(_minUpperPr);

                _lowerMaxModeLineSlopePFAnalysis.runPassFailWithUpperLimit(_maxModeLowerSlopes);
                _upperMaxModeLineSlopePFAnalysis.runPassFailWithUpperLimit(_maxModeUpperSlopes);
                _lowerMinModeLineSlopePFAnalysis.runPassFailWithLowerLimit(_minModeLowerSlopes);
                _upperMinModeLineSlopePFAnalysis.runPassFailWithLowerLimit(_minModeUpperSlopes);


                if (!(_numSuperModesRemoved <= _maxModeBordersRemovedAnalysis.threshold) ||
                    !(_maxModeWidthPFAnalysis.pass) ||
                    !(_minModeWidthPFAnalysis.pass) ||
                    !(_lowerMaxModeBDCAreaPFAnalysis.pass) ||
                    !(_lowerMaxModeBDCAreaXLengthPFAnalysis.pass) ||
                    !(_lowerSumModeBDCAreasPFAnalysis.pass) ||
                    !(_lowerNumModeBDCAreasPFAnalysis.pass) ||
                    !(_upperMaxModeBDCAreaPFAnalysis.pass) ||
                    !(_upperMaxModeBDCAreaXLengthPFAnalysis.pass) ||
                    !(_upperSumModeBDCAreasPFAnalysis.pass) ||
                    !(_upperNumModeBDCAreasPFAnalysis.pass) ||
                    !(_lowerModalDistortionAnglePFAnalysis.pass) ||
                    !(_lowerModalDistortionAngleXPositionPFAnalysis.pass) ||
                    !(_upperModalDistortionAnglePFAnalysis.pass) ||
                    !(_upperModalDistortionAngleXPositionPFAnalysis.pass) ||
                    !(_middleLineRMSValuesPFAnalysis.pass) ||
                    !(_maxMiddleLineSlopePFAnalysis.pass) ||
                    !(_minMiddleLineSlopePFAnalysis.pass) ||
                    !(_lowerMaxModeLineSlopePFAnalysis.pass) ||
                    !(_upperMaxModeLineSlopePFAnalysis.pass) ||
                    !(_lowerMinModeLineSlopePFAnalysis.pass) ||
                    !(_upperMinModeLineSlopePFAnalysis.pass) ||
                    !(_maxPrGapPFAnalysis.pass) ||
                    !(_maxLowerPrPFAnalysis.pass) ||
                    !(_minUpperPrPFAnalysis.pass))
                {
                    _screeningPassed = false;
                }

            }

            //////////////////////////////////////////////////////////////////////////////////
            // analyse results
            //
            if (!(_maxModeWidthPFAnalysis.pass))
                _qaDetectedError |= RESULTS_GENERAL_ERROR | RESULTS_LINES_MISSING;
            if (_numSuperModes < MIN_NUMBER_OF_MODES)
                _qaDetectedError |= RESULTS_GENERAL_ERROR | RESULTS_INVALID_NUMBER_OF_LINES;

            if (_qaDetectedError > RESULTS_NO_ERROR)
                _screeningPassed = false;
            //
            ////////////////////////////////////////////////////////////////////////////////////


            _passFailAnalysisComplete = true;

            return err;
        }


        void getQAError(int qaError, out int errorNumber, out String errorMsg)
        {
            errorMsg = "";
            errorNumber = 0;
            StringBuilder errorMsgSB = new StringBuilder();

            //if( qaError & RESULTS_GENERAL_ERROR)
            if ((qaError & RESULTS_GENERAL_ERROR) == RESULTS_GENERAL_ERROR)
            {
                errorMsgSB.Append("Error(s) detected. ");
                errorNumber |= RESULTS_GENERAL_ERROR;
            }
            //if( qaError & RESULTS_NO_MODEMAP_DATA )
            if ((qaError & RESULTS_NO_MODEMAP_DATA )== RESULTS_NO_MODEMAP_DATA)
            {
                errorMsgSB.Append("Mode map data not available. ");
                errorNumber |= RESULTS_NO_MODEMAP_DATA;
            }
            //if( qaError & RESULTS_PO_TOO_LOW )
            if ((qaError & RESULTS_PO_TOO_LOW) == RESULTS_PO_TOO_LOW)
            {
                errorMsgSB.Append("Invalid laser screening run - optical power is too low. ");
                errorNumber |= RESULTS_PO_TOO_LOW;
            }
            if ((qaError & RESULTS_PO_TOO_HIGH) == RESULTS_PO_TOO_HIGH)
            {
                errorMsgSB.Append("Invalid laser screening run - optical power is too high. ");
                errorNumber |= RESULTS_PO_TOO_HIGH;
            }
            if ((qaError & RESULTS_TOO_MANY_JUMPS) == RESULTS_TOO_MANY_JUMPS)
            {
                errorMsgSB.Append("Invalid laser screening run - too many laser mode jumps found. ");
                errorNumber |= RESULTS_TOO_MANY_JUMPS;
            }
            if ((qaError & RESULTS_TOO_FEW_JUMPS )== RESULTS_TOO_FEW_JUMPS)
            {
                errorMsgSB.Append("Invalid laser screening run - too few laser mode jumps found. ");
                errorNumber |= RESULTS_TOO_FEW_JUMPS;
            }
            if ((qaError & RESULTS_INVALID_NUMBER_OF_LINES) == RESULTS_INVALID_NUMBER_OF_LINES)
            {
                errorMsgSB.Append("Invalid laser screening run - invalid number of mode boundaries detected. ");
                errorNumber |= RESULTS_INVALID_NUMBER_OF_LINES;
            }
            if ((qaError & RESULTS_INVALID_LINES_REMOVED) == RESULTS_INVALID_LINES_REMOVED)
            {
                errorMsgSB.Append("Invalid mode boundaries detected. ");
                errorNumber |= RESULTS_INVALID_LINES_REMOVED;
            }
            if ((qaError & RESULTS_LINES_MISSING) == RESULTS_LINES_MISSING)
            {
                errorMsgSB.Append("Undetected mode boundaries. ");
                errorNumber |= RESULTS_LINES_MISSING;
            }

            StringBuilder errNumMsg = new StringBuilder();
            if (errorNumber > RESULTS_NO_ERROR) 
                errNumMsg.AppendFormat("{0:d},", errorNumber);
            else
                errNumMsg.Append(",");
            errNumMsg.Append(errorMsgSB.ToString());

            errorMsg = errNumMsg.ToString();

            errorMsgSB.Length = 0;
            errNumMsg.Length = 0;
        }

        //
        ////////////////////////////////////////////////////////////////////////////////////////
        //	Data Gathering Section - gather analysis data of the supermodes
        //

        //
        /////////////////////////////////////////////////////////////////////////////

        //	Mode Widths
        void getModeWidths()
        {
            if (_modeWidths.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    Vector modeWidths;
                    _p_superModes[i]._modeAnalysis.getModeWidths(out modeWidths);

                    _modeWidths.Add(modeWidths);
                }
            }
        }

        void getMaxModeWidths()
        {
            if (_maxModeWidths.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                    _maxModeWidths.Add(_p_superModes[i]._modeAnalysis.getModeWidthsStatistic(Vector.VectorAttribute.max));
            }
        }

        void getMinModeWidths()
        {
            if (_minModeWidths.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                    _minModeWidths.Add(_p_superModes[i]._modeAnalysis.getModeWidthsStatistic(Vector.VectorAttribute.min));
            }
        }

        void getMeanModeWidths()
        {
            if (_meanModeWidths.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                    _meanModeWidths.Add(_p_superModes[i]._modeAnalysis.getModeWidthsStatistic(Vector.VectorAttribute.mean));
            }
        }

        void getSumModeWidths()
        {
            if (_sumModeWidths.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                    _sumModeWidths.Add(_p_superModes[i]._modeAnalysis.getModeWidthsStatistic(Vector.VectorAttribute.sum));
            }
        }

        void getVarModeWidths()
        {
            if (_varModeWidths.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                    _varModeWidths.Add(_p_superModes[i]._modeAnalysis.getModeWidthsStatistic(Vector.VectorAttribute.variance));
            }
        }

        void getStdDevModeWidths()
        {
            if (_stdDevModeWidths.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                    _stdDevModeWidths.Add(_p_superModes[i]._modeAnalysis.getModeWidthsStatistic(Vector.VectorAttribute.stdDev));
            }
        }

        void getMedianModeWidths()
        {
            if (_medianModeWidths.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                    _medianModeWidths.Add(_p_superModes[i]._modeAnalysis.getModeWidthsStatistic(Vector.VectorAttribute.median));
            }
        }

        void getNumModeWidthSamples()
        {
            if (_numModeWidthSamples.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                    _numModeWidthSamples.Add(_p_superModes[i]._modeAnalysis.getModeWidthsStatistic(Vector.VectorAttribute.count));
            }
        }


        /////////////////////////////////////////////////////////////////////////////
        //	Mode Upper Slopes
        //
        void getModeUpperSlopes()
        {
            if (_modeUpperSlopes.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    Vector modeUpperSlopes;
                    _p_superModes[i]._modeAnalysis.getModeUpperSlopes(out modeUpperSlopes);

                    _modeUpperSlopes.Add(modeUpperSlopes);
                }
            }
        }

        void getMaxModeUpperSlopes()
        {
            if (_maxModeUpperSlopes.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                    _maxModeUpperSlopes.Add(_p_superModes[i]._modeAnalysis.getModeUpperSlopesStatistic(Vector.VectorAttribute.max));
            }
        }

        void getMinModeUpperSlopes()
        {
            if (_minModeUpperSlopes.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                    _minModeUpperSlopes.Add(_p_superModes[i]._modeAnalysis.getModeUpperSlopesStatistic(Vector.VectorAttribute.min));
            }
        }

        void getMeanModeUpperSlopes()
        {
            if (_meanModeUpperSlopes.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                    _meanModeUpperSlopes.Add(_p_superModes[i]._modeAnalysis.getModeUpperSlopesStatistic(Vector.VectorAttribute.mean));
            }
        }

        void getSumModeUpperSlopes()
        {
            if (_sumModeUpperSlopes.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                    _sumModeUpperSlopes.Add(_p_superModes[i]._modeAnalysis.getModeUpperSlopesStatistic(Vector.VectorAttribute.sum));
            }
        }

        void getVarModeUpperSlopes()
        {
            if (_varModeUpperSlopes.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                    _varModeUpperSlopes.Add(_p_superModes[i]._modeAnalysis.getModeUpperSlopesStatistic(Vector.VectorAttribute.variance));
            }
        }

        void getStdDevModeUpperSlopes()
        {
            if (_stdDevModeUpperSlopes.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                    _stdDevModeUpperSlopes.Add(_p_superModes[i]._modeAnalysis.getModeUpperSlopesStatistic(Vector.VectorAttribute.stdDev));
            }
        }

        void getMedianModeUpperSlopes()
        {
            if (_medianModeUpperSlopes.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                    _medianModeUpperSlopes.Add(_p_superModes[i]._modeAnalysis.getModeUpperSlopesStatistic(Vector.VectorAttribute.median));
            }
        }

        void getNumModeUpperSlopeSamples()
        {
            if (_numModeUpperSlopeSamples.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                    _numModeUpperSlopeSamples.Add(_p_superModes[i]._modeAnalysis.getModeUpperSlopesStatistic(Vector.VectorAttribute.count));
            }
        }


        //
        /////////////////////////////////////////////////////////////////////////////
        //	Mode Lower Slopes
        //
        void getModeLowerSlopes()
        {
            if (_modeLowerSlopes.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    Vector modeLowerSlopes;
                    _p_superModes[i]._modeAnalysis.getModeLowerSlopes(out modeLowerSlopes);

                    _modeLowerSlopes.Add(modeLowerSlopes);
                }
            }
        }

        void getMaxModeLowerSlopes()
        {
            if (_maxModeLowerSlopes.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                    _maxModeLowerSlopes.Add(_p_superModes[i]._modeAnalysis.getModeLowerSlopesStatistic(Vector.VectorAttribute.max));
            }
        }

        void getMinModeLowerSlopes()
        {
            if (_minModeLowerSlopes.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                    _minModeLowerSlopes.Add(_p_superModes[i]._modeAnalysis.getModeLowerSlopesStatistic(Vector.VectorAttribute.min));
            }
        }

        void getMeanModeLowerSlopes()
        {
            if (_meanModeLowerSlopes.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                    _meanModeLowerSlopes.Add(_p_superModes[i]._modeAnalysis.getModeLowerSlopesStatistic(Vector.VectorAttribute.mean));
            }
        }


        void getSumModeLowerSlopes()
        {
            if (_sumModeLowerSlopes.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                    _sumModeLowerSlopes.Add(_p_superModes[i]._modeAnalysis.getModeLowerSlopesStatistic(Vector.VectorAttribute.sum));
            }
        }


        void getVarModeLowerSlopes()
        {
            if (_varModeLowerSlopes.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                    _varModeLowerSlopes.Add(_p_superModes[i]._modeAnalysis.getModeLowerSlopesStatistic(Vector.VectorAttribute.variance));
            }
        }

        void getStdDevModeLowerSlopes()
        {
            if (_stdDevModeLowerSlopes.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                    _stdDevModeLowerSlopes.Add(_p_superModes[i]._modeAnalysis.getModeLowerSlopesStatistic(Vector.VectorAttribute.stdDev));
            }
        }


        void getMedianModeLowerSlopes()
        {
            if (_medianModeLowerSlopes.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                    _medianModeLowerSlopes.Add(_p_superModes[i]._modeAnalysis.getModeLowerSlopesStatistic(Vector.VectorAttribute.median));
            }
        }


        void getNumModeLowerSlopeSamples()
        {
            if (_numModeLowerSlopeSamples.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                    _numModeLowerSlopeSamples.Add(_p_superModes[i]._modeAnalysis.getModeLowerSlopesStatistic(Vector.VectorAttribute.count));
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        ///
        //	Modal Distortion
        //


        void getModalDistortionAngles()
        {
            if (_lowerModalDistortionAngles.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    _lowerModalDistortionAngles.Add(_p_superModes[i]._modeAnalysis.getLowerModeModalDistortionAngle());
                    _upperModalDistortionAngles.Add(_p_superModes[i]._modeAnalysis.getUpperModeModalDistortionAngle());
                }
            }
        }


        void getModalDistortionAngleXPositions()
        {
            if (_lowerModalDistortionAngleXPositions.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    _lowerModalDistortionAngleXPositions.Add(_p_superModes[i]._modeAnalysis.getLowerModeMaxModalDistortionAngleXPosition());
                    _upperModalDistortionAngleXPositions.Add(_p_superModes[i]._modeAnalysis.getUpperModeMaxModalDistortionAngleXPosition());
                }
            }
        }

        /////////////////////////////////////////////////////////////////////////////
        ///
        //	Mode Boundary Direction Change Area measurements
        //


        void getModeBDCAreas()
        {
            if (_lowerModeBDCAreas.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    Vector lowerModeBDCAreas;
                    Vector upperModeBDCAreas;
                    _p_superModes[i]._modeAnalysis.getModeBDCAreas(out lowerModeBDCAreas, out upperModeBDCAreas);

                    _lowerModeBDCAreas.Add(lowerModeBDCAreas);
                    _upperModeBDCAreas.Add(upperModeBDCAreas);
                }
            }
        }

        void getMaxModeBDCAreas()
        {
            if (_lowerMaxModeBDCAreas.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    _lowerMaxModeBDCAreas.Add(_p_superModes[i]._modeAnalysis.getLowerModeBDCAreasStatistic(Vector.VectorAttribute.max));
                    _upperMaxModeBDCAreas.Add(_p_superModes[i]._modeAnalysis.getUpperModeBDCAreasStatistic(Vector.VectorAttribute.max));
                }
            }
        }

        void getMinModeBDCAreas()
        {
            if (_lowerMinModeBDCAreas.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    _lowerMinModeBDCAreas.Add(_p_superModes[i]._modeAnalysis.getLowerModeBDCAreasStatistic(Vector.VectorAttribute.min));
                    _upperMinModeBDCAreas.Add(_p_superModes[i]._modeAnalysis.getUpperModeBDCAreasStatistic(Vector.VectorAttribute.min));
                }
            }
        }


        void getMeanModeBDCAreas()
        {
            if (_lowerMeanModeBDCAreas.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    _lowerMeanModeBDCAreas.Add(_p_superModes[i]._modeAnalysis.getLowerModeBDCAreasStatistic(Vector.VectorAttribute.mean));
                    _upperMeanModeBDCAreas.Add(_p_superModes[i]._modeAnalysis.getUpperModeBDCAreasStatistic(Vector.VectorAttribute.mean));
                }
            }
        }


        void getSumModeBDCAreas()
        {
            if (_lowerSumModeBDCAreas.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    _lowerSumModeBDCAreas.Add(_p_superModes[i]._modeAnalysis.getLowerModeBDCAreasStatistic(Vector.VectorAttribute.sum));
                    _upperSumModeBDCAreas.Add(_p_superModes[i]._modeAnalysis.getUpperModeBDCAreasStatistic(Vector.VectorAttribute.sum));
                }
            }
        }


        void getVarModeBDCAreas()
        {
            if (_lowerVarModeBDCAreas.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    _lowerVarModeBDCAreas.Add(_p_superModes[i]._modeAnalysis.getLowerModeBDCAreasStatistic(Vector.VectorAttribute.variance));
                    _upperVarModeBDCAreas.Add(_p_superModes[i]._modeAnalysis.getUpperModeBDCAreasStatistic(Vector.VectorAttribute.variance));
                }
            }
        }


        void getStdDevModeBDCAreas()
        {
            if (_lowerStdDevModeBDCAreas.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    _lowerStdDevModeBDCAreas.Add(_p_superModes[i]._modeAnalysis.getLowerModeBDCAreasStatistic(Vector.VectorAttribute.stdDev));
                    _upperStdDevModeBDCAreas.Add(_p_superModes[i]._modeAnalysis.getUpperModeBDCAreasStatistic(Vector.VectorAttribute.stdDev));
                }
            }
        }


        void getMedianModeBDCAreas()
        {
            if (_lowerMedianModeBDCAreas.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    _lowerMedianModeBDCAreas.Add(_p_superModes[i]._modeAnalysis.getLowerModeBDCAreasStatistic(Vector.VectorAttribute.median));
                    _upperMedianModeBDCAreas.Add(_p_superModes[i]._modeAnalysis.getUpperModeBDCAreasStatistic(Vector.VectorAttribute.median));
                }
            }
        }


        void getNumModeBDCAreaSamples()
        {
            if (_lowerNumModeBDCAreaSamples.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    _lowerNumModeBDCAreaSamples.Add(_p_superModes[i]._modeAnalysis.getLowerModeBDCAreasStatistic(Vector.VectorAttribute.count));
                    _upperNumModeBDCAreaSamples.Add(_p_superModes[i]._modeAnalysis.getUpperModeBDCAreasStatistic(Vector.VectorAttribute.count));
                }
            }
        }

        /////////////////////////////////////////////////////////////////////////////
        ///
        //	Mode Boundary Direction Change Area X Length measurements
        //


        void getModeBDCAreaXLengths()
        {
            if (_lowerModeBDCAreaXLengths.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    Vector lowerModeBDCAreaXLengths;
                    Vector upperModeBDCAreaXLengths;
                    _p_superModes[i]._modeAnalysis.getModeBDCAreaXLengths(out lowerModeBDCAreaXLengths, out upperModeBDCAreaXLengths);

                    _lowerModeBDCAreaXLengths.Add(lowerModeBDCAreaXLengths);
                    _upperModeBDCAreaXLengths.Add(upperModeBDCAreaXLengths);
                }
            }
        }

        void getMaxModeBDCAreaXLengths()
        {
            if (_lowerMaxModeBDCAreaXLengths.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    _lowerMaxModeBDCAreaXLengths.Add(_p_superModes[i]._modeAnalysis.getLowerModeBDCAreaXLengthsStatistic(Vector.VectorAttribute.max));
                    _upperMaxModeBDCAreaXLengths.Add(_p_superModes[i]._modeAnalysis.getUpperModeBDCAreaXLengthsStatistic(Vector.VectorAttribute.max));
                }
            }
        }


        void getMinModeBDCAreaXLengths()
        {
            if (_lowerMinModeBDCAreaXLengths.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    _lowerMinModeBDCAreaXLengths.Add(_p_superModes[i]._modeAnalysis.getLowerModeBDCAreaXLengthsStatistic(Vector.VectorAttribute.min));
                    _upperMinModeBDCAreaXLengths.Add(_p_superModes[i]._modeAnalysis.getUpperModeBDCAreaXLengthsStatistic(Vector.VectorAttribute.min));
                }
            }
        }


        void getMeanModeBDCAreaXLengths()
        {
            if (_lowerMeanModeBDCAreaXLengths.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    _lowerMeanModeBDCAreaXLengths.Add(_p_superModes[i]._modeAnalysis.getLowerModeBDCAreaXLengthsStatistic(Vector.VectorAttribute.mean));
                    _upperMeanModeBDCAreaXLengths.Add(_p_superModes[i]._modeAnalysis.getUpperModeBDCAreaXLengthsStatistic(Vector.VectorAttribute.mean));
                }
            }
        }


        void getSumModeBDCAreaXLengths()
        {
            if (_lowerSumModeBDCAreaXLengths.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    _lowerSumModeBDCAreaXLengths.Add(_p_superModes[i]._modeAnalysis.getLowerModeBDCAreaXLengthsStatistic(Vector.VectorAttribute.sum));
                    _upperSumModeBDCAreaXLengths.Add(_p_superModes[i]._modeAnalysis.getUpperModeBDCAreaXLengthsStatistic(Vector.VectorAttribute.sum));
                }
            }
        }


        void getVarModeBDCAreaXLengths()
        {
            if (_lowerVarModeBDCAreaXLengths.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    _lowerVarModeBDCAreaXLengths.Add(_p_superModes[i]._modeAnalysis.getLowerModeBDCAreaXLengthsStatistic(Vector.VectorAttribute.variance));
                    _upperVarModeBDCAreaXLengths.Add(_p_superModes[i]._modeAnalysis.getUpperModeBDCAreaXLengthsStatistic(Vector.VectorAttribute.variance));
                }
            }
        }


        void getStdDevModeBDCAreaXLengths()
        {
            if (_lowerStdDevModeBDCAreaXLengths.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    _lowerStdDevModeBDCAreaXLengths.Add(_p_superModes[i]._modeAnalysis.getLowerModeBDCAreaXLengthsStatistic(Vector.VectorAttribute.stdDev));
                    _upperStdDevModeBDCAreaXLengths.Add(_p_superModes[i]._modeAnalysis.getUpperModeBDCAreaXLengthsStatistic(Vector.VectorAttribute.stdDev));
                }
            }
        }


        void getMedianModeBDCAreaXLengths()
        {
            if (_lowerMedianModeBDCAreaXLengths.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    _lowerMedianModeBDCAreaXLengths.Add(_p_superModes[i]._modeAnalysis.getLowerModeBDCAreaXLengthsStatistic(Vector.VectorAttribute.median));
                    _upperMedianModeBDCAreaXLengths.Add(_p_superModes[i]._modeAnalysis.getUpperModeBDCAreaXLengthsStatistic(Vector.VectorAttribute.median));
                }
            }
        }


        void getNumModeBDCAreaXLengthSamples()
        {
            if (_lowerNumModeBDCAreaXLengthSamples.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    _lowerNumModeBDCAreaXLengthSamples.Add(_p_superModes[i]._modeAnalysis.getLowerModeBDCAreaXLengthsStatistic(Vector.VectorAttribute.count));
                    _upperNumModeBDCAreaXLengthSamples.Add(_p_superModes[i]._modeAnalysis.getUpperModeBDCAreaXLengthsStatistic(Vector.VectorAttribute.count));
                }
            }
        }

        //
        /////////////////////////////////////////////////////////////////////////////
        ///
        //	MiddleLine RootMeanSquare analysis
        //


        void getMiddleLineRMSValues()
        {
            if (_middleLineRMSValues.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    double rmsValue = 0;
                    _p_superModes[i].getMiddleLineRMSValue(ref rmsValue);

                    _middleLineRMSValues.Add(rmsValue);
                }
            }
        }

        void getMiddleLineSlopes()
        {
            if (_middleLineSlopes.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    double slope = 0;
                    _p_superModes[i].getMiddleLineSlope(ref slope);

                    _middleLineSlopes.Add(slope);
                }
            }
        }
        //
        /////////////////////////////////////////////////////////////////////////////
        ///
        //	Continuity analysis
        //


        void getMaxContinuityValue()
        {
            if (_maxContinuityValue == 0)
                _maxContinuityValue = _continuityValues.GetVectorAttribute(Vector.VectorAttribute.max);
        }



        void getMinContinuityValue()
        {
            if (_minContinuityValue == 0)
                _minContinuityValue = _continuityValues.GetVectorAttribute(Vector.VectorAttribute.min);
        }


        void getMeanContinuityValues()
        {
            if (_meanContinuityValues == 0)
                _meanContinuityValues = _continuityValues.GetVectorAttribute(Vector.VectorAttribute.mean);
        }


        void getSumContinuityValues()
        {
            if (_sumContinuityValues == 0)
                _sumContinuityValues = _continuityValues.GetVectorAttribute(Vector.VectorAttribute.sum);
        }


        void getVarContinuityValues()
        {
            if (_varContinuityValues == 0)
                _varContinuityValues = _continuityValues.GetVectorAttribute(Vector.VectorAttribute.variance);
        }

        void getStdDevContinuityValues()
        {
            if (_stdDevContinuityValues == 0)
                _stdDevContinuityValues = _continuityValues.GetVectorAttribute(Vector.VectorAttribute.stdDev);
        }

        void getMedianContinuityValues()
        {
            if (_medianContinuityValues == 0)
                _medianContinuityValues = _continuityValues.GetVectorAttribute(Vector.VectorAttribute.median);
        }

        void getNumContinuityValues()
        {
            if (_numContinuityValues == 0)
                _numContinuityValues = _continuityValues.GetVectorAttribute(Vector.VectorAttribute.count);
        }

        void getMaxPrGap()
        {
            if (_maxPrGap == 0)
            {
                _maxPrGap = _continuityValues.GetVectorAttribute(Vector.VectorAttribute.maxGap);
            }
        }

        void getMaxLowerPr()
        {
            if (_maxLowerPr == 0)
            {
                _maxLowerPr = _continuityValues.GetVectorAttribute(Vector.VectorAttribute.min);
            }
        }

        void getMinUpperPr()
        {
            if (_minUpperPr == 0)
            {
                _minUpperPr = _continuityValues.GetVectorAttribute(Vector.VectorAttribute.max);
            }
        }
        //
        ////////////////////////////////////////////////////////////////////////////////
        //	File Handling section - writing results to file
        //
        rcode writeQaResultsToFile()
        {
            rcode err = rcode.ok;

            CLoseGridFile qaResults = new CLoseGridFile(_qaResultsAbsFilePath);
            qaResults.createFolderAndFile();

            //////////////////////////////////////////////////////////////////////////

            string errorMessage = "";
            int errorNum = 0;
            getQAError(_qaDetectedError,out errorNum,out errorMessage);

            List<string> error_msg_row = new List<string>();
            error_msg_row.Add(errorMessage);
            qaResults.addRow(error_msg_row);
            qaResults.addEmptyLine();

            //////////////////////////////////////////

            List<string> row = new List<string>();
            string cellString = "";

            row.Clear();
            row.Add("Slope Window Size");
            cellString = string.Format("{0:d}", DSDBR01.Instance._DSDBR01_OMQA_slope_window_size);
            row.Add(cellString);
            qaResults.addRow(row);

            row.Clear();
            row.Add("Modal Distortion Min X");
            cellString = string.Format("{0:g}", DSDBR01.Instance._DSDBR01_OMQA_modal_distortion_min_x);
            row.Add(cellString);
            qaResults.addRow(row);

            row.Clear();
            row.Add("Modal Distortion Max X");
            cellString = string.Format("{0:g}", DSDBR01.Instance._DSDBR01_OMQA_modal_distortion_max_x);
            row.Add(cellString);
            qaResults.addRow(row);

            row.Clear();
            row.Add("Modal Distortion Min Y");
            cellString = string.Format("{0:g}", DSDBR01.Instance._DSDBR01_OMQA_modal_distortion_min_y);
            row.Add(cellString);
            qaResults.addRow(row);

            qaResults.addEmptyLine();

            row.Clear();
            row.Add("Max Pr Gap in Continuity Data");
            cellString = string.Format("{0:g}", _maxPrGap);
            row.Add(cellString);
            qaResults.addRow(row);

            row.Clear();
            row.Add("Lower Pr in Continuity Data");
            cellString = string.Format("{0:g}", _maxLowerPr);
            row.Add(cellString);
            qaResults.addRow(row);

            row.Clear();
            row.Add("Upper Pr in Continuity Data");
            cellString = string.Format("{0:g}", _minUpperPr);
            row.Add(cellString);
            qaResults.addRow(row);

            qaResults.addEmptyLine();

            //////////////////////////////////////////

            const string QA_XLS_HEADER1 = "";
            const string QA_XLS_HEADER2 = "Index";
            const string QA_XLS_HEADER3 = "Function";
            const string QA_XLS_HEADER4 = "Slope of upper mode boundary line";
            const string QA_XLS_HEADER5 = "Slope of lower mode boundary line";
            const string QA_XLS_HEADER6 = "Mode Width";
            const string QA_XLS_HEADER7 = "Max Lower Modal Distortion Angle";
            const string QA_XLS_HEADER8 = "Max Upper Modal Distortion Angle";
            const string QA_XLS_HEADER9 = "Max Lower Modal Distortion X-Position";
            const string QA_XLS_HEADER10 = "Max Upper Modal Distortion X-Position";
            const string QA_XLS_HEADER11 = "Max Lower Boundary Direction Change X-Length";
            const string QA_XLS_HEADER12 = "Max Upper Boundary Direction Change X-Length";
            const string QA_XLS_HEADER13 = "Max Lower Boundary Direction Change Area";
            const string QA_XLS_HEADER14 = "Max Upper Boundary Direction Change Area";
            const string QA_XLS_HEADER15 = "Sum of Lower Boundary Direction Change Areas";
            const string QA_XLS_HEADER16 = "Sum of Upper Boundary Direction Change Areas";
            const string QA_XLS_HEADER17 = "Number of Lower Boundary Direction Changes";
            const string QA_XLS_HEADER18 = "Number of Upper Boundary Direction Changes";
            const string QA_XLS_HEADER19 = "Middle Line RMS Value";

            const string QA_XLS_MAX = "Maximum";
            const string QA_XLS_MIN = "Minimum";
            const string QA_XLS_MEAN = "Mean";
            const string QA_XLS_SUM = "Sum";
            const string QA_XLS_COUNT = "Number Of Samples";
            const string QA_XLS_STDDEV = "Standard Deviation";
            const string QA_XLS_VAR = "Variance";
            const string QA_XLS_OTHERS = "";

            List<string> attribArray = new List<string>();
            attribArray.Add(QA_XLS_MAX);
            attribArray.Add(QA_XLS_MIN);
            attribArray.Add(QA_XLS_MEAN);
            attribArray.Add(QA_XLS_SUM);
            attribArray.Add(QA_XLS_COUNT);
            attribArray.Add(QA_XLS_STDDEV);
            attribArray.Add(QA_XLS_VAR);
            attribArray.Add(QA_XLS_OTHERS);

            string QA_XLS_COL1_VALUE = "";
            string QA_XLS_COL2_VALUE = "";
            string QA_XLS_COL3_VALUE = "";
            string QA_XLS_COL4_VALUE = "";
            string QA_XLS_COL5_VALUE = "";
            string QA_XLS_COL6_VALUE = "";
            string QA_XLS_COL7_VALUE = "";
            string QA_XLS_COL8_VALUE = "";
            string QA_XLS_COL9_VALUE = "";
            string QA_XLS_COL10_VALUE = "";
            string QA_XLS_COL11_VALUE = "";
            string QA_XLS_COL12_VALUE = "";
            string QA_XLS_COL13_VALUE = "";
            string QA_XLS_COL14_VALUE = "";
            string QA_XLS_COL15_VALUE = "";
            string QA_XLS_COL16_VALUE = "";
            string QA_XLS_COL17_VALUE = "";
            string QA_XLS_COL18_VALUE = "";
            string QA_XLS_COL19_VALUE = "";

            List<string> headerArray = new List<string>();
            headerArray.Add(QA_XLS_HEADER1);
            headerArray.Add(QA_XLS_HEADER2);
            headerArray.Add(QA_XLS_HEADER3);
            headerArray.Add(QA_XLS_HEADER4);
            headerArray.Add(QA_XLS_HEADER5);
            headerArray.Add(QA_XLS_HEADER6);
            headerArray.Add(QA_XLS_HEADER7);
            headerArray.Add(QA_XLS_HEADER8);
            headerArray.Add(QA_XLS_HEADER9);
            headerArray.Add(QA_XLS_HEADER10);
            headerArray.Add(QA_XLS_HEADER11);
            headerArray.Add(QA_XLS_HEADER12);
            headerArray.Add(QA_XLS_HEADER13);
            headerArray.Add(QA_XLS_HEADER14);
            headerArray.Add(QA_XLS_HEADER15);
            headerArray.Add(QA_XLS_HEADER16);
            headerArray.Add(QA_XLS_HEADER17);
            headerArray.Add(QA_XLS_HEADER18);
            headerArray.Add(QA_XLS_HEADER19);

            qaResults.addRow(headerArray);

            //////////////////////////////////////////////////////////////////////////

            List<string> rowArray = new List<string>();
            rowArray.Add(QA_XLS_COL1_VALUE);
            rowArray.Add(QA_XLS_COL2_VALUE);
            rowArray.Add(QA_XLS_COL3_VALUE);
            rowArray.Add(QA_XLS_COL4_VALUE);
            rowArray.Add(QA_XLS_COL5_VALUE);
            rowArray.Add(QA_XLS_COL6_VALUE);
            rowArray.Add(QA_XLS_COL7_VALUE);
            rowArray.Add(QA_XLS_COL8_VALUE);
            rowArray.Add(QA_XLS_COL9_VALUE);
            rowArray.Add(QA_XLS_COL10_VALUE);
            rowArray.Add(QA_XLS_COL11_VALUE);
            rowArray.Add(QA_XLS_COL12_VALUE);
            rowArray.Add(QA_XLS_COL13_VALUE);
            rowArray.Add(QA_XLS_COL14_VALUE);
            rowArray.Add(QA_XLS_COL15_VALUE);
            rowArray.Add(QA_XLS_COL16_VALUE);
            rowArray.Add(QA_XLS_COL17_VALUE);
            rowArray.Add(QA_XLS_COL18_VALUE);
            rowArray.Add(QA_XLS_COL19_VALUE);
            qaResults.addRow(rowArray);

            QA_XLS_COL1_VALUE = "";
            QA_XLS_COL10_VALUE = "";

            //////////////////////////////////////////////////////////////////////////

            int QA_XLS_NUM_ROWS_PER_SM = attribArray.Count;

            for (int i = 0; i < _numSuperModes; i++)
            {
                for (int j = 0; j < QA_XLS_NUM_ROWS_PER_SM; j++)
                {
                    // clear vars
                    rowArray.Clear();
                    QA_XLS_COL1_VALUE = "";
                    QA_XLS_COL2_VALUE = "";
                    QA_XLS_COL3_VALUE = "";
                    QA_XLS_COL4_VALUE = "";
                    QA_XLS_COL5_VALUE = "";
                    QA_XLS_COL6_VALUE = "";
                    QA_XLS_COL7_VALUE = "";
                    QA_XLS_COL8_VALUE = "";
                    QA_XLS_COL9_VALUE = "";
                    QA_XLS_COL10_VALUE = "";
                    QA_XLS_COL11_VALUE = "";
                    QA_XLS_COL12_VALUE = "";
                    QA_XLS_COL13_VALUE = "";
                    QA_XLS_COL14_VALUE = "";
                    QA_XLS_COL15_VALUE = "";
                    QA_XLS_COL16_VALUE = "";
                    QA_XLS_COL17_VALUE = "";
                    QA_XLS_COL18_VALUE = "";
                    QA_XLS_COL19_VALUE = "";

                    QA_XLS_COL1_VALUE = "";
                    QA_XLS_COL2_VALUE += string.Format("{0:d}", i);
                    QA_XLS_COL3_VALUE = attribArray[j];

                    if (QA_XLS_COL3_VALUE == QA_XLS_MAX)
                    {
                        if (i < _maxModeUpperSlopes.Count)
                            QA_XLS_COL4_VALUE += string.Format("{0:g}", _maxModeUpperSlopes[i]);
                        else
                            QA_XLS_COL4_VALUE = "";
                        if (i < _maxModeLowerSlopes.Count)
                            QA_XLS_COL5_VALUE += string.Format("{0:g}", _maxModeLowerSlopes[i]);
                        else
                            QA_XLS_COL5_VALUE = "";
                        if (i < _maxModeWidths.Count)
                            QA_XLS_COL6_VALUE += string.Format("{0:g}", _maxModeWidths[i]);
                        else
                            QA_XLS_COL6_VALUE = "";
                    }
                    else if (QA_XLS_COL3_VALUE == QA_XLS_MIN)
                    {
                        if (i < _minModeUpperSlopes.Count)
                            QA_XLS_COL4_VALUE += string.Format("{0:g}", _minModeUpperSlopes[i]);
                        else
                            QA_XLS_COL4_VALUE = "";
                        if (i < _minModeLowerSlopes.Count)
                            QA_XLS_COL5_VALUE += string.Format("{0:g}", _minModeLowerSlopes[i]);
                        else
                            QA_XLS_COL5_VALUE = "";
                        if (i < _minModeWidths.Count)
                            QA_XLS_COL6_VALUE += string.Format("{0:g}", _minModeWidths[i]);
                        else
                            QA_XLS_COL6_VALUE = "";
                    }
                    else if (QA_XLS_COL3_VALUE == QA_XLS_MEAN)
                    {
                        if (i < _meanModeUpperSlopes.Count)
                            QA_XLS_COL4_VALUE += string.Format("{0:g}", _meanModeUpperSlopes[i]);
                        else
                            QA_XLS_COL4_VALUE = "";
                        if (i < _meanModeLowerSlopes.Count)
                            QA_XLS_COL5_VALUE += string.Format("{0:g}", _meanModeLowerSlopes[i]);
                        else
                            QA_XLS_COL5_VALUE = "";
                        if (i < _meanModeWidths.Count)
                            QA_XLS_COL6_VALUE += string.Format("{0:g}", _meanModeWidths[i]);
                        else
                            QA_XLS_COL6_VALUE = "";
                    }
                    else if (QA_XLS_COL3_VALUE == QA_XLS_SUM)
                    {
                        if (i < _sumModeUpperSlopes.Count)
                            QA_XLS_COL4_VALUE += string.Format("{0:g}", _sumModeUpperSlopes[i]);
                        else
                            QA_XLS_COL4_VALUE = "";
                        if (i < _sumModeLowerSlopes.Count)
                            QA_XLS_COL5_VALUE += string.Format("{0:g}", _sumModeLowerSlopes[i]);
                        else
                            QA_XLS_COL5_VALUE = "";
                        if (i < _sumModeWidths.Count)
                            QA_XLS_COL6_VALUE += string.Format("{0:g}", _sumModeWidths[i]);
                        else
                            QA_XLS_COL6_VALUE = "";
                    }
                    else if (QA_XLS_COL3_VALUE == QA_XLS_COUNT)
                    {
                        if (i < _numModeUpperSlopeSamples.Count)
                            QA_XLS_COL4_VALUE += string.Format("{0:g}", _numModeUpperSlopeSamples[i]);
                        else
                            QA_XLS_COL4_VALUE = "";
                        if (i < _numModeLowerSlopeSamples.Count)
                            QA_XLS_COL5_VALUE += string.Format("{0:g}", _numModeLowerSlopeSamples[i]);
                        else
                            QA_XLS_COL5_VALUE = "";
                        if (i < _numModeWidthSamples.Count)
                            QA_XLS_COL6_VALUE += string.Format("{0:g}", _numModeWidthSamples[i]);
                        else
                            QA_XLS_COL6_VALUE = "";
                    }
                    else if (QA_XLS_COL3_VALUE == QA_XLS_STDDEV)
                    {
                        if (i < _stdDevModeUpperSlopes.Count)
                            QA_XLS_COL4_VALUE += string.Format("{0:g}", _stdDevModeUpperSlopes[i]);
                        else
                            QA_XLS_COL4_VALUE = "";
                        if (i < _stdDevModeLowerSlopes.Count)
                            QA_XLS_COL5_VALUE += string.Format("{0:g}", _stdDevModeLowerSlopes[i]);
                        else
                            QA_XLS_COL5_VALUE = "";
                        if (i < _stdDevModeWidths.Count)
                            QA_XLS_COL6_VALUE += string.Format("{0:g}", _stdDevModeWidths[i]);
                        else
                            QA_XLS_COL6_VALUE = "";
                    }
                    else if (QA_XLS_COL3_VALUE == QA_XLS_VAR)
                    {
                        if (i < _varModeUpperSlopes.Count)
                            QA_XLS_COL4_VALUE += string.Format("{0:g}", _varModeUpperSlopes[i]);
                        else
                            QA_XLS_COL4_VALUE = "";
                        if (i < _varModeLowerSlopes.Count)
                            QA_XLS_COL5_VALUE += string.Format("{0:g}", _varModeLowerSlopes[i]);
                        else
                            QA_XLS_COL5_VALUE = "";
                        if (i < _varModeWidths.Count)
                            QA_XLS_COL6_VALUE += string.Format("{0:g}", _varModeWidths[i]);
                        else
                            QA_XLS_COL6_VALUE = "";
                    }
                    else if (QA_XLS_COL3_VALUE == QA_XLS_OTHERS)
                    {
                        if (i < _lowerModalDistortionAngles.Count)
                            QA_XLS_COL7_VALUE += string.Format("{0:g}", _lowerModalDistortionAngles[i]);
                        else
                            QA_XLS_COL7_VALUE = "";
                        if (i < _upperModalDistortionAngles.Count)
                            QA_XLS_COL8_VALUE += string.Format("{0:g}", _upperModalDistortionAngles[i]);
                        else
                            QA_XLS_COL8_VALUE = "";
                        if (i < _lowerModalDistortionAngleXPositions.Count)
                            QA_XLS_COL9_VALUE += string.Format("{0:g}", _lowerModalDistortionAngleXPositions[i]);
                        else
                            QA_XLS_COL9_VALUE = "";
                        if (i < _upperModalDistortionAngleXPositions.Count)
                            QA_XLS_COL10_VALUE += string.Format("{0:g}", _upperModalDistortionAngleXPositions[i]);
                        else
                            QA_XLS_COL10_VALUE = "";
                        if (i < _lowerMaxModeBDCAreaXLengths.Count)
                            QA_XLS_COL11_VALUE += string.Format("{0:g}", _lowerMaxModeBDCAreaXLengths[i]);
                        else
                            QA_XLS_COL11_VALUE = "";
                        if (i < _upperMaxModeBDCAreaXLengths.Count)
                            QA_XLS_COL12_VALUE += string.Format("{0:g}", _upperMaxModeBDCAreaXLengths[i]);
                        else
                            QA_XLS_COL12_VALUE = "";
                        if (i < _lowerMaxModeBDCAreas.Count)
                            QA_XLS_COL13_VALUE += string.Format("{0:g}", _lowerMaxModeBDCAreas[i]);
                        else
                            QA_XLS_COL13_VALUE = "";
                        if (i < _upperMaxModeBDCAreas.Count)
                            QA_XLS_COL14_VALUE += string.Format("{0:g}", _upperMaxModeBDCAreas[i]);
                        else
                            QA_XLS_COL14_VALUE = "";
                        if (i < _lowerSumModeBDCAreas.Count)
                            QA_XLS_COL15_VALUE += string.Format("{0:g}", _lowerSumModeBDCAreas[i]);
                        else
                            QA_XLS_COL15_VALUE = "";
                        if (i < _upperSumModeBDCAreas.Count)
                            QA_XLS_COL16_VALUE += string.Format("{0:g}", _upperSumModeBDCAreas[i]);
                        else
                            QA_XLS_COL16_VALUE = "";
                        if (i < _lowerNumModeBDCAreaSamples.Count)
                            QA_XLS_COL17_VALUE += string.Format("{0:g}", _lowerNumModeBDCAreaSamples[i]);
                        else
                            QA_XLS_COL17_VALUE = "";
                        if (i < _upperNumModeBDCAreaSamples.Count)
                            QA_XLS_COL18_VALUE += string.Format("{0:g}", _upperNumModeBDCAreaSamples[i]);
                        else
                            QA_XLS_COL18_VALUE = "";
                        if (i < _middleLineRMSValues.Count)
                            QA_XLS_COL19_VALUE += string.Format("{0:g}", _middleLineRMSValues[i]);
                        else
                            QA_XLS_COL19_VALUE = "";
                    }

                    rowArray.Clear();
                    rowArray.Add(QA_XLS_COL1_VALUE);
                    rowArray.Add(QA_XLS_COL2_VALUE);
                    rowArray.Add(QA_XLS_COL3_VALUE);
                    rowArray.Add(QA_XLS_COL4_VALUE);
                    rowArray.Add(QA_XLS_COL5_VALUE);
                    rowArray.Add(QA_XLS_COL6_VALUE);
                    rowArray.Add(QA_XLS_COL7_VALUE);
                    rowArray.Add(QA_XLS_COL8_VALUE);
                    rowArray.Add(QA_XLS_COL9_VALUE);
                    rowArray.Add(QA_XLS_COL10_VALUE);
                    rowArray.Add(QA_XLS_COL11_VALUE);
                    rowArray.Add(QA_XLS_COL12_VALUE);
                    rowArray.Add(QA_XLS_COL13_VALUE);
                    rowArray.Add(QA_XLS_COL14_VALUE);
                    rowArray.Add(QA_XLS_COL15_VALUE);
                    rowArray.Add(QA_XLS_COL16_VALUE);
                    rowArray.Add(QA_XLS_COL17_VALUE);
                    rowArray.Add(QA_XLS_COL18_VALUE);
                    rowArray.Add(QA_XLS_COL19_VALUE);

                    qaResults.addRow(rowArray);
                }

                qaResults.addEmptyLine();

            }

            qaResults.Dispose();
            return err;
        }

        internal rcode writePassFailResultsToFile(CLoseGridFile passFailResults)//&
        {
            rcode err = rcode.ok;

            if (_passFailAnalysisComplete)
            {
                string errorMessage = "";
                int errorNum = 0;
                getQAError(_qaDetectedError,out errorNum,out errorMessage);

                List<string> error_msg_row = new List<string>();
                error_msg_row.Add(errorMessage);
                passFailResults.addRow(error_msg_row);
                passFailResults.addEmptyLine();

                //////////////////////////////////////////

                List<string> row = new List<string>();
                string cellString = "";

                row.Clear();
                row.Add("Slope Window Size");
                cellString = string.Format("{0:d}", DSDBR01.Instance._DSDBR01_OMQA_slope_window_size);
                row.Add(cellString);
                passFailResults.addRow(row);

                row.Clear();
                row.Add("Modal Distortion Min X");
                cellString = string.Format("{0:g}", DSDBR01.Instance._DSDBR01_OMQA_modal_distortion_min_x);
                row.Add(cellString);
                passFailResults.addRow(row);

                row.Clear();
                row.Add("Modal Distortion Max X");
                cellString = string.Format("{0:g}", DSDBR01.Instance._DSDBR01_OMQA_modal_distortion_max_x);
                row.Add(cellString);
                passFailResults.addRow(row);

                row.Clear();
                row.Add("Modal Distortion Min Y");
                cellString = string.Format("{0:g}", DSDBR01.Instance._DSDBR01_OMQA_modal_distortion_min_y);
                row.Add(cellString);
                passFailResults.addRow(row);

                passFailResults.addEmptyLine();

                //////////////////////////////////////////

                const string QA_XLS_HEADER1 = "";
                const string QA_XLS_HEADER2 = "Mode borders removed due to error";
                const string QA_XLS_HEADER3 = "Max Mode Width";
                const string QA_XLS_HEADER4 = "Min Mode Width";
                const string QA_XLS_HEADER5 = "Max Lower Modal Distortion Angle";
                const string QA_XLS_HEADER6 = "Max Upper Modal Distortion Angle";
                const string QA_XLS_HEADER7 = "Max Lower Modal Distortion X-Position";
                const string QA_XLS_HEADER8 = "Max Upper Modal Distortion X-Position";
                const string QA_XLS_HEADER9 = "Max Lower Boundary Direction Change X-Length";
                const string QA_XLS_HEADER10 = "Max Upper Boundary Direction Change X-Length";
                const string QA_XLS_HEADER11 = "Max Lower Boundary Direction Change Area";
                const string QA_XLS_HEADER12 = "Max Upper Boundary Direction Change Area";
                const string QA_XLS_HEADER13 = "Sum of Lower Boundary Direction Change Areas";
                const string QA_XLS_HEADER14 = "Sum of Upper Boundary Direction Change Areas";
                const string QA_XLS_HEADER15 = "Number of Lower Boundary Direction Changes";
                const string QA_XLS_HEADER16 = "Number of Upper Boundary Direction Changes";
                const string QA_XLS_HEADER17 = "Max Middle Line RMS Value";
                const string QA_XLS_HEADER18 = "Min Middle Line Slope";
                const string QA_XLS_HEADER19 = "Max Middle Line Slope";
                const string QA_XLS_HEADER20 = "Max Pr Gap";
                const string QA_XLS_HEADER21 = "Max Lower Pr";
                const string QA_XLS_HEADER22 = "Min Upper Pr";
                const string QA_XLS_HEADER23 = "Max Mode Lower Line Slope";
                const string QA_XLS_HEADER24 = "Max Mode Upper Line Slope";
                const string QA_XLS_HEADER25 = "Min Mode Lower Line Slope";
                const string QA_XLS_HEADER26 = "Min Mode Upper Line Slope";

                const string QA_XLS_ROWTITLE1 = "Pass/Fail Result";
                const string QA_XLS_ROWTITLE2 = "Min Value Measured";
                const string QA_XLS_ROWTITLE3 = "Max Value Measured";
                const string QA_XLS_ROWTITLE4 = "Number of Failed SMs";
                const string QA_XLS_ROWTITLE5 = "Threshold Applied";

                string QA_XLS_COL1_VALUE = "";
                string QA_XLS_COL2_VALUE = "";
                string QA_XLS_COL3_VALUE = "";
                string QA_XLS_COL4_VALUE = "";
                string QA_XLS_COL5_VALUE = "";
                string QA_XLS_COL6_VALUE = "";
                string QA_XLS_COL7_VALUE = "";
                string QA_XLS_COL8_VALUE = "";
                string QA_XLS_COL9_VALUE = "";
                string QA_XLS_COL10_VALUE = "";
                string QA_XLS_COL11_VALUE = "";
                string QA_XLS_COL12_VALUE = "";
                string QA_XLS_COL13_VALUE = "";
                string QA_XLS_COL14_VALUE = "";
                string QA_XLS_COL15_VALUE = "";
                string QA_XLS_COL16_VALUE = "";
                string QA_XLS_COL17_VALUE = "";
                string QA_XLS_COL18_VALUE = "";
                string QA_XLS_COL19_VALUE = "";
                string QA_XLS_COL20_VALUE = "";
                string QA_XLS_COL21_VALUE = "";
                string QA_XLS_COL22_VALUE = "";
                string QA_XLS_COL23_VALUE = "";
                string QA_XLS_COL24_VALUE = "";
                string QA_XLS_COL25_VALUE = "";
                string QA_XLS_COL26_VALUE = "";

                List<string> headerArray = new List<string>();
                headerArray.Add(QA_XLS_HEADER1);
                headerArray.Add(QA_XLS_HEADER2);
                headerArray.Add(QA_XLS_HEADER3);
                headerArray.Add(QA_XLS_HEADER4);
                headerArray.Add(QA_XLS_HEADER5);
                headerArray.Add(QA_XLS_HEADER6);
                headerArray.Add(QA_XLS_HEADER7);
                headerArray.Add(QA_XLS_HEADER8);
                headerArray.Add(QA_XLS_HEADER9);
                headerArray.Add(QA_XLS_HEADER10);
                headerArray.Add(QA_XLS_HEADER11);
                headerArray.Add(QA_XLS_HEADER12);
                headerArray.Add(QA_XLS_HEADER13);
                headerArray.Add(QA_XLS_HEADER14);
                headerArray.Add(QA_XLS_HEADER15);
                headerArray.Add(QA_XLS_HEADER16);
                headerArray.Add(QA_XLS_HEADER17);
                headerArray.Add(QA_XLS_HEADER18);
                headerArray.Add(QA_XLS_HEADER19);
                headerArray.Add(QA_XLS_HEADER20);
                headerArray.Add(QA_XLS_HEADER21);
                headerArray.Add(QA_XLS_HEADER22);
                headerArray.Add(QA_XLS_HEADER23);
                headerArray.Add(QA_XLS_HEADER24);
                headerArray.Add(QA_XLS_HEADER25);
                headerArray.Add(QA_XLS_HEADER26);

                List<string> rowTitleArray = new List<string>();
                rowTitleArray.Add(QA_XLS_ROWTITLE1);
                rowTitleArray.Add(QA_XLS_ROWTITLE2);
                rowTitleArray.Add(QA_XLS_ROWTITLE3);
                rowTitleArray.Add(QA_XLS_ROWTITLE4);
                rowTitleArray.Add(QA_XLS_ROWTITLE5);


                // set headers
                passFailResults.addRow(headerArray);

                // add space
                passFailResults.addEmptyLine();


                ///////////////////////////////
                /// first row - pass/fails
                //
                QA_XLS_COL2_VALUE = (_numSuperModesRemoved <= _maxModeBordersRemovedAnalysis.threshold) ? "PASS" : "FAIL";
                QA_XLS_COL3_VALUE = _maxModeWidthPFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL4_VALUE = _minModeWidthPFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL5_VALUE = _lowerModalDistortionAnglePFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL6_VALUE = _upperModalDistortionAnglePFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL7_VALUE = _lowerModalDistortionAngleXPositionPFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL8_VALUE = _upperModalDistortionAngleXPositionPFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL9_VALUE = _lowerMaxModeBDCAreaXLengthPFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL10_VALUE = _upperMaxModeBDCAreaXLengthPFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL11_VALUE = _lowerMaxModeBDCAreaPFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL12_VALUE = _upperMaxModeBDCAreaPFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL13_VALUE = _lowerSumModeBDCAreasPFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL14_VALUE = _upperSumModeBDCAreasPFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL15_VALUE = _lowerNumModeBDCAreasPFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL16_VALUE = _upperNumModeBDCAreasPFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL17_VALUE = _middleLineRMSValuesPFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL18_VALUE = _minMiddleLineSlopePFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL19_VALUE = _maxMiddleLineSlopePFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL20_VALUE = _maxPrGapPFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL21_VALUE = _maxLowerPrPFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL22_VALUE = _minUpperPrPFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL23_VALUE = _lowerMaxModeLineSlopePFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL24_VALUE = _upperMaxModeLineSlopePFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL25_VALUE = _lowerMinModeLineSlopePFAnalysis.pass ? "PASS" : "FAIL";
                QA_XLS_COL26_VALUE = _upperMinModeLineSlopePFAnalysis.pass ? "PASS" : "FAIL";

                List<string> rowArray = new List<string>();

                rowArray.Clear();
                rowArray.Add(QA_XLS_ROWTITLE1);
                rowArray.Add(QA_XLS_COL2_VALUE);
                rowArray.Add(QA_XLS_COL3_VALUE);
                rowArray.Add(QA_XLS_COL4_VALUE);
                rowArray.Add(QA_XLS_COL5_VALUE);
                rowArray.Add(QA_XLS_COL6_VALUE);
                rowArray.Add(QA_XLS_COL7_VALUE);
                rowArray.Add(QA_XLS_COL8_VALUE);
                rowArray.Add(QA_XLS_COL9_VALUE);
                rowArray.Add(QA_XLS_COL10_VALUE);
                rowArray.Add(QA_XLS_COL11_VALUE);
                rowArray.Add(QA_XLS_COL12_VALUE);
                rowArray.Add(QA_XLS_COL13_VALUE);
                rowArray.Add(QA_XLS_COL14_VALUE);
                rowArray.Add(QA_XLS_COL15_VALUE);
                rowArray.Add(QA_XLS_COL16_VALUE);
                rowArray.Add(QA_XLS_COL17_VALUE);
                rowArray.Add(QA_XLS_COL18_VALUE);
                rowArray.Add(QA_XLS_COL19_VALUE);
                rowArray.Add(QA_XLS_COL20_VALUE);
                rowArray.Add(QA_XLS_COL21_VALUE);
                rowArray.Add(QA_XLS_COL22_VALUE);
                rowArray.Add(QA_XLS_COL23_VALUE);
                rowArray.Add(QA_XLS_COL24_VALUE);
                rowArray.Add(QA_XLS_COL25_VALUE);
                rowArray.Add(QA_XLS_COL26_VALUE);
                passFailResults.addRow(rowArray);


                ////////////////////////////////////////
                ///
                //	num failed Modes
                //
                QA_XLS_COL2_VALUE = "";
                QA_XLS_COL3_VALUE = "";
                QA_XLS_COL4_VALUE = "";
                QA_XLS_COL5_VALUE = "";
                QA_XLS_COL6_VALUE = "";
                QA_XLS_COL7_VALUE = "";
                QA_XLS_COL8_VALUE = "";
                QA_XLS_COL9_VALUE = "";
                QA_XLS_COL10_VALUE = "";
                QA_XLS_COL11_VALUE = "";
                QA_XLS_COL12_VALUE = "";
                QA_XLS_COL13_VALUE = "";
                QA_XLS_COL14_VALUE = "";
                QA_XLS_COL15_VALUE = "";
                QA_XLS_COL16_VALUE = "";
                QA_XLS_COL17_VALUE = "";
                QA_XLS_COL18_VALUE = "";
                QA_XLS_COL19_VALUE = "";
                QA_XLS_COL20_VALUE = "";
                QA_XLS_COL21_VALUE = "";
                QA_XLS_COL22_VALUE = "";
                QA_XLS_COL23_VALUE = "";
                QA_XLS_COL24_VALUE = "";
                QA_XLS_COL25_VALUE = "";
                QA_XLS_COL26_VALUE = "";

                QA_XLS_COL2_VALUE = string.Format("{0:d}", _numSuperModesRemoved);
                QA_XLS_COL3_VALUE += string.Format("{0:d}", _maxModeWidthPFAnalysis.numFailedModes);
                QA_XLS_COL4_VALUE += string.Format("{0:d}", _minModeWidthPFAnalysis.numFailedModes);
                QA_XLS_COL5_VALUE += string.Format("{0:d}", _lowerModalDistortionAnglePFAnalysis.numFailedModes);
                QA_XLS_COL6_VALUE += string.Format("{0:d}", _upperModalDistortionAnglePFAnalysis.numFailedModes);
                QA_XLS_COL7_VALUE += string.Format("{0:d}", _lowerModalDistortionAngleXPositionPFAnalysis.numFailedModes);
                QA_XLS_COL8_VALUE += string.Format("{0:d}", _upperModalDistortionAngleXPositionPFAnalysis.numFailedModes);
                QA_XLS_COL9_VALUE += string.Format("{0:d}", _lowerMaxModeBDCAreaXLengthPFAnalysis.numFailedModes);
                QA_XLS_COL10_VALUE += string.Format("{0:d}", _upperMaxModeBDCAreaXLengthPFAnalysis.numFailedModes);
                QA_XLS_COL11_VALUE += string.Format("{0:d}", _lowerMaxModeBDCAreaPFAnalysis.numFailedModes);
                QA_XLS_COL12_VALUE += string.Format("{0:d}", _upperMaxModeBDCAreaPFAnalysis.numFailedModes);
                QA_XLS_COL13_VALUE += string.Format("{0:d}", _lowerSumModeBDCAreasPFAnalysis.numFailedModes);
                QA_XLS_COL14_VALUE += string.Format("{0:d}", _upperSumModeBDCAreasPFAnalysis.numFailedModes);
                QA_XLS_COL15_VALUE += string.Format("{0:d}", _lowerSumModeBDCAreasPFAnalysis.numFailedModes);
                QA_XLS_COL16_VALUE += string.Format("{0:d}", _upperSumModeBDCAreasPFAnalysis.numFailedModes);
                QA_XLS_COL17_VALUE += string.Format("{0:d}", _middleLineRMSValuesPFAnalysis.numFailedModes);
                QA_XLS_COL18_VALUE += string.Format("{0:d}", _minMiddleLineSlopePFAnalysis.numFailedModes);
                QA_XLS_COL19_VALUE += string.Format("{0:d}", _maxMiddleLineSlopePFAnalysis.numFailedModes);
                QA_XLS_COL20_VALUE += string.Format("{0:d}", _maxPrGapPFAnalysis.numFailedModes);
                QA_XLS_COL21_VALUE += string.Format("{0:d}", _maxLowerPrPFAnalysis.numFailedModes);
                QA_XLS_COL22_VALUE += string.Format("{0:d}", _minUpperPrPFAnalysis.numFailedModes);
                QA_XLS_COL23_VALUE += string.Format("{0:d}", _lowerMaxModeLineSlopePFAnalysis.numFailedModes);
                QA_XLS_COL24_VALUE += string.Format("{0:d}", _upperMaxModeLineSlopePFAnalysis.numFailedModes);
                QA_XLS_COL25_VALUE += string.Format("{0:d}", _lowerMinModeLineSlopePFAnalysis.numFailedModes);
                QA_XLS_COL26_VALUE += string.Format("{0:d}", _upperMinModeLineSlopePFAnalysis.numFailedModes);

                rowArray.Clear();
                rowArray.Add(QA_XLS_ROWTITLE4);
                rowArray.Add(QA_XLS_COL2_VALUE);
                rowArray.Add(QA_XLS_COL3_VALUE);
                rowArray.Add(QA_XLS_COL4_VALUE);
                rowArray.Add(QA_XLS_COL5_VALUE);
                rowArray.Add(QA_XLS_COL6_VALUE);
                rowArray.Add(QA_XLS_COL7_VALUE);
                rowArray.Add(QA_XLS_COL8_VALUE);
                rowArray.Add(QA_XLS_COL9_VALUE);
                rowArray.Add(QA_XLS_COL10_VALUE);
                rowArray.Add(QA_XLS_COL11_VALUE);
                rowArray.Add(QA_XLS_COL12_VALUE);
                rowArray.Add(QA_XLS_COL13_VALUE);
                rowArray.Add(QA_XLS_COL14_VALUE);
                rowArray.Add(QA_XLS_COL15_VALUE);
                rowArray.Add(QA_XLS_COL16_VALUE);
                rowArray.Add(QA_XLS_COL17_VALUE);
                rowArray.Add(QA_XLS_COL18_VALUE);
                rowArray.Add(QA_XLS_COL19_VALUE);
                rowArray.Add(QA_XLS_COL20_VALUE);
                rowArray.Add(QA_XLS_COL21_VALUE);
                rowArray.Add(QA_XLS_COL22_VALUE);
                rowArray.Add(QA_XLS_COL23_VALUE);
                rowArray.Add(QA_XLS_COL24_VALUE);
                rowArray.Add(QA_XLS_COL25_VALUE);
                rowArray.Add(QA_XLS_COL26_VALUE);
                passFailResults.addRow(rowArray);


                ////////////////////////////////////////
                ///
                //	thresholds
                //
                QA_XLS_COL2_VALUE = "";
                QA_XLS_COL3_VALUE = "";
                QA_XLS_COL4_VALUE = "";
                QA_XLS_COL5_VALUE = "";
                QA_XLS_COL6_VALUE = "";
                QA_XLS_COL7_VALUE = "";
                QA_XLS_COL8_VALUE = "";
                QA_XLS_COL9_VALUE = "";
                QA_XLS_COL10_VALUE = "";
                QA_XLS_COL11_VALUE = "";
                QA_XLS_COL12_VALUE = "";
                QA_XLS_COL13_VALUE = "";
                QA_XLS_COL14_VALUE = "";
                QA_XLS_COL15_VALUE = "";
                QA_XLS_COL16_VALUE = "";
                QA_XLS_COL17_VALUE = "";
                QA_XLS_COL18_VALUE = "";
                QA_XLS_COL19_VALUE = "";
                QA_XLS_COL20_VALUE = "";
                QA_XLS_COL21_VALUE = "";
                QA_XLS_COL22_VALUE = "";
                QA_XLS_COL23_VALUE = "";
                QA_XLS_COL24_VALUE = "";
                QA_XLS_COL25_VALUE = "";
                QA_XLS_COL26_VALUE = "";

                QA_XLS_COL2_VALUE += string.Format(" > {0:g}", _maxModeBordersRemovedAnalysis.threshold);
                QA_XLS_COL3_VALUE += string.Format(" > {0:g}", _maxModeWidthPFAnalysis.threshold);
                QA_XLS_COL4_VALUE += string.Format(" < {0:g}", _minModeWidthPFAnalysis.threshold);
                QA_XLS_COL5_VALUE += string.Format(" > {0:g}", _lowerModalDistortionAnglePFAnalysis.threshold);
                QA_XLS_COL6_VALUE += string.Format(" > {0:g}", _upperModalDistortionAnglePFAnalysis.threshold);
                QA_XLS_COL7_VALUE += string.Format(" > {0:g}", _lowerModalDistortionAngleXPositionPFAnalysis.threshold);
                QA_XLS_COL8_VALUE += string.Format(" > {0:g}", _upperModalDistortionAngleXPositionPFAnalysis.threshold);
                QA_XLS_COL9_VALUE += string.Format(" > {0:g}", _lowerMaxModeBDCAreaXLengthPFAnalysis.threshold);
                QA_XLS_COL10_VALUE += string.Format(" > {0:g}", _upperMaxModeBDCAreaXLengthPFAnalysis.threshold);
                QA_XLS_COL11_VALUE += string.Format(" > {0:g}", _lowerMaxModeBDCAreaPFAnalysis.threshold);
                QA_XLS_COL12_VALUE += string.Format(" > {0:g}", _upperMaxModeBDCAreaPFAnalysis.threshold);
                QA_XLS_COL13_VALUE += string.Format(" > {0:g}", _lowerSumModeBDCAreasPFAnalysis.threshold);
                QA_XLS_COL14_VALUE += string.Format(" > {0:g}", _upperSumModeBDCAreasPFAnalysis.threshold);
                QA_XLS_COL15_VALUE += string.Format(" > {0:g}", _lowerNumModeBDCAreasPFAnalysis.threshold);
                QA_XLS_COL16_VALUE += string.Format(" > {0:g}", _upperNumModeBDCAreasPFAnalysis.threshold);
                QA_XLS_COL17_VALUE += string.Format(" > {0:g}", _middleLineRMSValuesPFAnalysis.threshold);
                QA_XLS_COL18_VALUE += string.Format(" < {0:g}", _minMiddleLineSlopePFAnalysis.threshold);
                QA_XLS_COL19_VALUE += string.Format(" > {0:g}", _maxMiddleLineSlopePFAnalysis.threshold);
                QA_XLS_COL20_VALUE += string.Format(" > {0:g}", _maxPrGapPFAnalysis.threshold);
                QA_XLS_COL21_VALUE += string.Format(" > {0:g}", _maxLowerPrPFAnalysis.threshold);
                QA_XLS_COL22_VALUE += string.Format(" < {0:g}", _minUpperPrPFAnalysis.threshold);
                QA_XLS_COL23_VALUE += string.Format(" > {0:g}", _lowerMaxModeLineSlopePFAnalysis.threshold);
                QA_XLS_COL24_VALUE += string.Format(" > {0:g}", _upperMaxModeLineSlopePFAnalysis.threshold);
                QA_XLS_COL25_VALUE += string.Format(" < {0:g}", _lowerMinModeLineSlopePFAnalysis.threshold);
                QA_XLS_COL26_VALUE += string.Format(" < {0:g}", _upperMinModeLineSlopePFAnalysis.threshold);

                rowArray.Clear();
                rowArray.Add(QA_XLS_ROWTITLE5);
                rowArray.Add(QA_XLS_COL2_VALUE);
                rowArray.Add(QA_XLS_COL3_VALUE);
                rowArray.Add(QA_XLS_COL4_VALUE);
                rowArray.Add(QA_XLS_COL5_VALUE);
                rowArray.Add(QA_XLS_COL6_VALUE);
                rowArray.Add(QA_XLS_COL7_VALUE);
                rowArray.Add(QA_XLS_COL8_VALUE);
                rowArray.Add(QA_XLS_COL9_VALUE);
                rowArray.Add(QA_XLS_COL10_VALUE);
                rowArray.Add(QA_XLS_COL11_VALUE);
                rowArray.Add(QA_XLS_COL12_VALUE);
                rowArray.Add(QA_XLS_COL13_VALUE);
                rowArray.Add(QA_XLS_COL14_VALUE);
                rowArray.Add(QA_XLS_COL15_VALUE);
                rowArray.Add(QA_XLS_COL16_VALUE);
                rowArray.Add(QA_XLS_COL17_VALUE);
                rowArray.Add(QA_XLS_COL18_VALUE);
                rowArray.Add(QA_XLS_COL19_VALUE);
                rowArray.Add(QA_XLS_COL20_VALUE);
                rowArray.Add(QA_XLS_COL21_VALUE);
                rowArray.Add(QA_XLS_COL22_VALUE);
                rowArray.Add(QA_XLS_COL23_VALUE);
                rowArray.Add(QA_XLS_COL24_VALUE);
                rowArray.Add(QA_XLS_COL25_VALUE);
                rowArray.Add(QA_XLS_COL26_VALUE);
                passFailResults.addRow(rowArray);


                // Calculation parameters
                QA_XLS_COL1_VALUE = "";
                QA_XLS_COL2_VALUE = "";
                QA_XLS_COL3_VALUE = "";
                QA_XLS_COL4_VALUE = "";
                QA_XLS_COL5_VALUE = "";
                QA_XLS_COL6_VALUE = "";
                QA_XLS_COL7_VALUE = "";
                QA_XLS_COL8_VALUE = "";
                QA_XLS_COL9_VALUE = "";
                QA_XLS_COL10_VALUE = "";
                QA_XLS_COL11_VALUE = "";
                QA_XLS_COL12_VALUE = "";
                QA_XLS_COL13_VALUE = "";
                QA_XLS_COL14_VALUE = "";
                QA_XLS_COL15_VALUE = "";
                QA_XLS_COL16_VALUE = "";
                QA_XLS_COL17_VALUE = "";
                QA_XLS_COL18_VALUE = "";
                QA_XLS_COL19_VALUE = "";
                QA_XLS_COL20_VALUE = "";
                QA_XLS_COL21_VALUE = "";
                QA_XLS_COL22_VALUE = "";
                QA_XLS_COL23_VALUE = "";
                QA_XLS_COL24_VALUE = "";
                QA_XLS_COL25_VALUE = "";
                QA_XLS_COL26_VALUE = "";


                rowArray.Clear();
                rowArray.Add(QA_XLS_COL1_VALUE);
                rowArray.Add(QA_XLS_COL2_VALUE);
                rowArray.Add(QA_XLS_COL3_VALUE);
                rowArray.Add(QA_XLS_COL4_VALUE);
                rowArray.Add(QA_XLS_COL5_VALUE);
                rowArray.Add(QA_XLS_COL6_VALUE);
                rowArray.Add(QA_XLS_COL7_VALUE);
                rowArray.Add(QA_XLS_COL8_VALUE);
                rowArray.Add(QA_XLS_COL9_VALUE);
                rowArray.Add(QA_XLS_COL10_VALUE);
                rowArray.Add(QA_XLS_COL11_VALUE);
                rowArray.Add(QA_XLS_COL12_VALUE);
                rowArray.Add(QA_XLS_COL13_VALUE);
                rowArray.Add(QA_XLS_COL14_VALUE);
                rowArray.Add(QA_XLS_COL15_VALUE);
                rowArray.Add(QA_XLS_COL16_VALUE);
                rowArray.Add(QA_XLS_COL17_VALUE);
                rowArray.Add(QA_XLS_COL18_VALUE);
                rowArray.Add(QA_XLS_COL19_VALUE);
                rowArray.Add(QA_XLS_COL20_VALUE);
                rowArray.Add(QA_XLS_COL21_VALUE);
                rowArray.Add(QA_XLS_COL22_VALUE);
                rowArray.Add(QA_XLS_COL23_VALUE);
                rowArray.Add(QA_XLS_COL24_VALUE);
                rowArray.Add(QA_XLS_COL25_VALUE);
                rowArray.Add(QA_XLS_COL26_VALUE);
                passFailResults.addRow(rowArray);

                // add space
                passFailResults.addEmptyLine();


                //
                // Find location of failing statistic(s)
                //

                // row title
                rowArray.Clear();
                rowArray.Add("Index");
                passFailResults.addRow(rowArray);


                // final rows - Modes
                for (int i = 0; i < _numSuperModes; i++)
                {
                    QA_XLS_COL1_VALUE = "";
                    QA_XLS_COL2_VALUE = "";
                    QA_XLS_COL3_VALUE = "";
                    QA_XLS_COL4_VALUE = "";
                    QA_XLS_COL5_VALUE = "";
                    QA_XLS_COL6_VALUE = "";
                    QA_XLS_COL7_VALUE = "";
                    QA_XLS_COL8_VALUE = "";
                    QA_XLS_COL9_VALUE = "";
                    QA_XLS_COL10_VALUE = "";
                    QA_XLS_COL11_VALUE = "";
                    QA_XLS_COL12_VALUE = "";
                    QA_XLS_COL13_VALUE = "";
                    QA_XLS_COL14_VALUE = "";
                    QA_XLS_COL15_VALUE = "";
                    QA_XLS_COL16_VALUE = "";
                    QA_XLS_COL17_VALUE = "";
                    QA_XLS_COL18_VALUE = "";
                    QA_XLS_COL19_VALUE = "";
                    QA_XLS_COL20_VALUE = "";
                    QA_XLS_COL21_VALUE = "";
                    QA_XLS_COL22_VALUE = "";
                    QA_XLS_COL23_VALUE = "";
                    QA_XLS_COL24_VALUE = "";
                    QA_XLS_COL25_VALUE = "";
                    QA_XLS_COL26_VALUE = "";

                    // Index location
                    //QA_XLS_COL1_VALUE.AppendFormat(" %d", i);
                    QA_XLS_COL1_VALUE += string.Format(" {0:d}", i);

                    // Max Mode Width
                    //std::vector<double>::iterator i_f = std::find(
                    //    _maxModeWidthPFAnalysis.failedModes.begin(),
                    //    _maxModeWidthPFAnalysis.failedModes.Count,
                    //    i );
                    int i_f = _maxModeWidthPFAnalysis.failedModes.IndexOf(i);
                    if (i_f == -1) i_f = _maxModeWidthPFAnalysis.failedModes.Count;

                    if (i < _maxModeWidths.Count && i_f != _maxModeWidthPFAnalysis.failedModes.Count)
                        QA_XLS_COL3_VALUE += string.Format("{0:f}", _maxModeWidths[i]);


                    // Min Mode Width
                    //i_f = std::find(
                    //    _minModeWidthPFAnalysis.failedModes.begin(),
                    //    _minModeWidthPFAnalysis.failedModes.Count,
                    //    i );
                    i_f = _minModeWidthPFAnalysis.failedModes.IndexOf(i);
                    if (i_f == -1) i_f = _minModeWidthPFAnalysis.failedModes.Count;

                    if (i < _minModeWidths.Count && i_f != _minModeWidthPFAnalysis.failedModes.Count)
                        QA_XLS_COL4_VALUE += string.Format("{0:f}", _minModeWidths[i]);


                    // lower modal_distortion
                    //i_f = std::find(
                    //    _lowerModalDistortionAnglePFAnalysis.failedModes.begin(),
                    //    _lowerModalDistortionAnglePFAnalysis.failedModes.Count,
                    //    i );
                    i_f = _lowerModalDistortionAnglePFAnalysis.failedModes.IndexOf(i);
                    if (i_f == -1) i_f = _lowerModalDistortionAnglePFAnalysis.failedModes.Count;

                    if (i < _lowerModalDistortionAngles.Count
                    && i < _lowerModalDistortionAngleXPositions.Count
                    && i_f != _lowerModalDistortionAnglePFAnalysis.failedModes.Count)
                    {
                        QA_XLS_COL5_VALUE += string.Format("{0:f}", _lowerModalDistortionAngles[i]);
                        QA_XLS_COL7_VALUE += string.Format("{0:f0}", _lowerModalDistortionAngleXPositions[i]);
                    }

                    // upper modal_distortion
                    //i_f = std::find(
                    //    _upperModalDistortionAnglePFAnalysis.failedModes.begin(),
                    //    _upperModalDistortionAnglePFAnalysis.failedModes.Count,
                    //    i );
                    i_f = _upperModalDistortionAnglePFAnalysis.failedModes.IndexOf(i);
                    if (i_f == -1) i_f = _upperModalDistortionAnglePFAnalysis.failedModes.Count;

                    if (i < _upperModalDistortionAngles.Count
                    && i < _upperModalDistortionAngleXPositions.Count
                    && i_f != _upperModalDistortionAnglePFAnalysis.failedModes.Count)
                    {
                        QA_XLS_COL6_VALUE += string.Format("{0:f}", _upperModalDistortionAngles[i]);
                        QA_XLS_COL8_VALUE += string.Format("{0:f}", _upperModalDistortionAngleXPositions[i]);
                    }

                    // Lower Max Bdc X-Length
                    //i_f = std::find(
                    //    _lowerMaxModeBDCAreaXLengthPFAnalysis.failedModes.begin(),
                    //    _lowerMaxModeBDCAreaXLengthPFAnalysis.failedModes.Count,
                    //    i );
                    i_f = _lowerMaxModeBDCAreaXLengthPFAnalysis.failedModes.IndexOf(i);
                    if (i_f == -1) i_f = _lowerMaxModeBDCAreaXLengthPFAnalysis.failedModes.Count;

                    if (i < _lowerMaxModeBDCAreaXLengths.Count && i_f != _lowerMaxModeBDCAreaXLengthPFAnalysis.failedModes.Count)
                        QA_XLS_COL9_VALUE += string.Format("{0:f}", _lowerMaxModeBDCAreaXLengths[i]);

                    // Upper Max Bdc X-Length
                    //i_f = std::find(
                    //    _upperMaxModeBDCAreaXLengthPFAnalysis.failedModes.begin(),
                    //    _upperMaxModeBDCAreaXLengthPFAnalysis.failedModes.Count,
                    //    i );
                    i_f = _upperMaxModeBDCAreaXLengthPFAnalysis.failedModes.IndexOf(i);
                    if (i_f == -1) i_f = _upperMaxModeBDCAreaXLengthPFAnalysis.failedModes.Count;

                    if (i < _upperMaxModeBDCAreaXLengths.Count && i_f != _upperMaxModeBDCAreaXLengthPFAnalysis.failedModes.Count)
                        QA_XLS_COL10_VALUE += string.Format("{0:f}", _upperMaxModeBDCAreaXLengths[i]);

                    // Lower Max Bdc Area
                    //i_f = std::find(
                    //    _lowerMaxModeBDCAreaPFAnalysis.failedModes.begin(),
                    //    _lowerMaxModeBDCAreaPFAnalysis.failedModes.Count,
                    //    i );
                    i_f = _lowerMaxModeBDCAreaPFAnalysis.failedModes.IndexOf(i);
                    if (i_f == -1) i_f = _lowerMaxModeBDCAreaPFAnalysis.failedModes.Count;

                    if (i < _lowerMaxModeBDCAreas.Count && i_f != _lowerMaxModeBDCAreaPFAnalysis.failedModes.Count)
                        QA_XLS_COL11_VALUE += string.Format("{0:f}", _lowerMaxModeBDCAreas[i]);

                    // Upper Max Bdc Area
                    //i_f = std::find(
                    //    _upperMaxModeBDCAreaPFAnalysis.failedModes.begin(),
                    //    _upperMaxModeBDCAreaPFAnalysis.failedModes.Count,
                    //    i );
                    i_f = _upperMaxModeBDCAreaPFAnalysis.failedModes.IndexOf(i);
                    if (i_f == -1) i_f = _upperMaxModeBDCAreaPFAnalysis.failedModes.Count;

                    if (i < _upperMaxModeBDCAreas.Count && i_f != _upperMaxModeBDCAreaPFAnalysis.failedModes.Count)
                        QA_XLS_COL12_VALUE += string.Format("{0:f}", _upperMaxModeBDCAreas[i]);

                    // Lower Sum of Bdc Areas
                    //i_f = std::find(
                    //    _lowerSumModeBDCAreasPFAnalysis.failedModes.begin(),
                    //    _lowerSumModeBDCAreasPFAnalysis.failedModes.Count,
                    //    i );
                    i_f = _lowerSumModeBDCAreasPFAnalysis.failedModes.IndexOf(i);
                    if (i_f == -1) i_f = _lowerSumModeBDCAreasPFAnalysis.failedModes.Count;

                    if (i < _lowerSumModeBDCAreas.Count && i_f != _lowerSumModeBDCAreasPFAnalysis.failedModes.Count)
                        QA_XLS_COL13_VALUE += string.Format("{0:f}", _lowerSumModeBDCAreas[i]);

                    // Upper Sum of Bdc Areas
                    //i_f = std::find(
                    //    _upperSumModeBDCAreasPFAnalysis.failedModes.begin(),
                    //    _upperSumModeBDCAreasPFAnalysis.failedModes.Count,
                    //    i );
                    i_f = _upperSumModeBDCAreasPFAnalysis.failedModes.IndexOf(i);
                    if (i_f == -1) i_f = _upperSumModeBDCAreasPFAnalysis.failedModes.Count;

                    if (i < _upperSumModeBDCAreas.Count && i_f != _upperSumModeBDCAreasPFAnalysis.failedModes.Count)
                        QA_XLS_COL14_VALUE += string.Format("{0:f}", _upperSumModeBDCAreas[i]);

                    // Lower Number of Bdcs
                    //i_f = std::find(
                    //    _lowerNumModeBDCAreasPFAnalysis.failedModes.begin(),
                    //    _lowerNumModeBDCAreasPFAnalysis.failedModes.Count,
                    //    i );
                    i_f = _lowerNumModeBDCAreasPFAnalysis.failedModes.IndexOf(i);
                    if (i_f == -1) i_f = _lowerNumModeBDCAreasPFAnalysis.failedModes.Count;

                    if (i < _lowerNumModeBDCAreaSamples.Count && i_f != _lowerNumModeBDCAreasPFAnalysis.failedModes.Count)
                        QA_XLS_COL15_VALUE += string.Format("{0:f}", _lowerNumModeBDCAreaSamples[i]);

                    // Upper Number of Bdcs
                    //i_f = std::find(
                    //    _upperNumModeBDCAreasPFAnalysis.failedModes.begin(),
                    //    _upperNumModeBDCAreasPFAnalysis.failedModes.Count,
                    //    i );
                    i_f = _upperNumModeBDCAreasPFAnalysis.failedModes.IndexOf(i);
                    if (i_f == -1) i_f = _upperNumModeBDCAreasPFAnalysis.failedModes.Count;

                    if (i < _upperNumModeBDCAreaSamples.Count && i_f != _upperNumModeBDCAreasPFAnalysis.failedModes.Count)
                        QA_XLS_COL16_VALUE += string.Format("{0:f}", _upperNumModeBDCAreaSamples[i]);

                    // Max Middle Line RMS Value
                    //i_f = std::find(
                    //    _middleLineRMSValuesPFAnalysis.failedModes.begin(),
                    //    _middleLineRMSValuesPFAnalysis.failedModes.Count,
                    //    i );
                    i_f = _middleLineRMSValuesPFAnalysis.failedModes.IndexOf(i);
                    if (i_f == -1) i_f = _middleLineRMSValuesPFAnalysis.failedModes.Count;

                    if (i < _middleLineRMSValues.Count && i_f != _middleLineRMSValuesPFAnalysis.failedModes.Count)
                        QA_XLS_COL17_VALUE += string.Format("{0:f}", _middleLineRMSValues[i]);

                    // Min Middle Line Slope
                    //i_f = std::find(
                    //    _minMiddleLineSlopePFAnalysis.failedModes.begin(),
                    //    _minMiddleLineSlopePFAnalysis.failedModes.Count,
                    //    i );
                    i_f = _minMiddleLineSlopePFAnalysis.failedModes.IndexOf(i);
                    if (i_f == -1) i_f = _minMiddleLineSlopePFAnalysis.failedModes.Count;

                    if (i < _middleLineSlopes.Count && i_f != _minMiddleLineSlopePFAnalysis.failedModes.Count)
                        QA_XLS_COL18_VALUE += string.Format("{0:f}", _middleLineSlopes[i]);

                    // Max Middle Line Slope
                    //i_f = std::find(
                    //    _maxMiddleLineSlopePFAnalysis.failedModes.begin(),
                    //    _maxMiddleLineSlopePFAnalysis.failedModes.Count,
                    //    i );
                    i_f = _maxMiddleLineSlopePFAnalysis.failedModes.IndexOf(i);
                    if (i_f == -1) i_f = _maxMiddleLineSlopePFAnalysis.failedModes.Count;

                    if (i < _middleLineSlopes.Count && i_f != _maxMiddleLineSlopePFAnalysis.failedModes.Count)
                        QA_XLS_COL19_VALUE += string.Format("{0:f}", _middleLineSlopes[i]);

                    // Max Pr Gap
                    //i_f = std::find(
                    //    _maxPrGapPFAnalysis.failedModes.begin(),
                    //    _maxPrGapPFAnalysis.failedModes.Count,
                    //    i );
                    i_f = _maxPrGapPFAnalysis.failedModes.IndexOf(i);
                    if (i_f == -1) i_f = _maxPrGapPFAnalysis.failedModes.Count;

                    if (i < (uint)_continuityValues.Count && i_f != _maxPrGapPFAnalysis.failedModes.Count)
                        QA_XLS_COL20_VALUE += string.Format("{0:f}", _maxPrGap);

                    // Max Lower Pr
                    //i_f = std::find(
                    //    _maxLowerPrPFAnalysis.failedModes.begin(),
                    //    _maxLowerPrPFAnalysis.failedModes.Count,
                    //    i );
                    i_f = _maxLowerPrPFAnalysis.failedModes.IndexOf(i);
                    if (i_f == -1) i_f = _maxLowerPrPFAnalysis.failedModes.Count;

                    if (i < (uint)_continuityValues.Count && i_f != _maxLowerPrPFAnalysis.failedModes.Count)
                        QA_XLS_COL21_VALUE += string.Format("{0:f}", _maxLowerPr);

                    // Min Upper Pr
                    //i_f = std::find(
                    //    _minUpperPrPFAnalysis.failedModes.begin(),
                    //    _minUpperPrPFAnalysis.failedModes.Count,
                    //    i );
                    i_f = _minUpperPrPFAnalysis.failedModes.IndexOf(i);
                    if (i_f == -1) i_f = _minUpperPrPFAnalysis.failedModes.Count;

                    if (i < (uint)(_continuityValues.Count) && i_f != _minUpperPrPFAnalysis.failedModes.Count)
                        QA_XLS_COL22_VALUE += string.Format("{0:f}", _minUpperPr);

                    // Lower max mode line slope
                    //i_f = std::find(
                    //    _lowerMaxModeLineSlopePFAnalysis.failedModes.begin(),
                    //    _lowerMaxModeLineSlopePFAnalysis.failedModes.Count,
                    //    i );
                    i_f = _lowerMaxModeLineSlopePFAnalysis.failedModes.IndexOf(i);
                    if (i_f == -1) i_f = _lowerMaxModeLineSlopePFAnalysis.failedModes.Count;

                    if (i < (uint)(_maxModeLowerSlopes.Count) && i_f != _lowerMaxModeLineSlopePFAnalysis.failedModes.Count)
                        QA_XLS_COL23_VALUE += string.Format("{0:f}", _maxModeLowerSlopes[i]);

                    // Upper max mode line slope
                    //i_f = std::find(
                    //    _upperMaxModeLineSlopePFAnalysis.failedModes.begin(),
                    //    _upperMaxModeLineSlopePFAnalysis.failedModes.Count,
                    //    i );
                    i_f = _upperMaxModeLineSlopePFAnalysis.failedModes.IndexOf(i);
                    if (i_f == -1) i_f = _upperMaxModeLineSlopePFAnalysis.failedModes.Count;

                    if (i < (uint)(_maxModeUpperSlopes.Count) && i_f != _upperMaxModeLineSlopePFAnalysis.failedModes.Count)
                        QA_XLS_COL24_VALUE += string.Format("{0:f}", _maxModeUpperSlopes[i]);


                    // Lower min mode line slope
                    //i_f = std::find(
                    //    _lowerMinModeLineSlopePFAnalysis.failedModes.begin(),
                    //    _lowerMinModeLineSlopePFAnalysis.failedModes.Count,
                    //    i );
                    i_f = _lowerMinModeLineSlopePFAnalysis.failedModes.IndexOf(i);
                    if (i_f == -1) i_f = _lowerMinModeLineSlopePFAnalysis.failedModes.Count;

                    if (i < (uint)(_minModeLowerSlopes.Count) && i_f != _lowerMinModeLineSlopePFAnalysis.failedModes.Count)
                        QA_XLS_COL25_VALUE += string.Format("{0:f}", _minModeLowerSlopes[i]);

                    // Upper min mode line slope
                    //i_f = std::find(
                    //    _upperMinModeLineSlopePFAnalysis.failedModes.begin(),
                    //    _upperMinModeLineSlopePFAnalysis.failedModes.Count,
                    //    i );
                    i_f = _upperMinModeLineSlopePFAnalysis.failedModes.IndexOf(i);
                    if (i_f == -1) i_f = _upperMinModeLineSlopePFAnalysis.failedModes.Count;

                    if (i < (uint)(_minModeUpperSlopes.Count) && i_f != _upperMinModeLineSlopePFAnalysis.failedModes.Count)
                        QA_XLS_COL26_VALUE += string.Format("{0:f}", _minModeUpperSlopes[i]);



                    rowArray.Clear();
                    rowArray.Add(QA_XLS_COL1_VALUE);
                    rowArray.Add(QA_XLS_COL2_VALUE);
                    rowArray.Add(QA_XLS_COL3_VALUE);
                    rowArray.Add(QA_XLS_COL4_VALUE);
                    rowArray.Add(QA_XLS_COL5_VALUE);
                    rowArray.Add(QA_XLS_COL6_VALUE);
                    rowArray.Add(QA_XLS_COL7_VALUE);
                    rowArray.Add(QA_XLS_COL8_VALUE);
                    rowArray.Add(QA_XLS_COL9_VALUE);
                    rowArray.Add(QA_XLS_COL10_VALUE);
                    rowArray.Add(QA_XLS_COL11_VALUE);
                    rowArray.Add(QA_XLS_COL12_VALUE);
                    rowArray.Add(QA_XLS_COL13_VALUE);
                    rowArray.Add(QA_XLS_COL14_VALUE);
                    rowArray.Add(QA_XLS_COL15_VALUE);
                    rowArray.Add(QA_XLS_COL16_VALUE);
                    rowArray.Add(QA_XLS_COL17_VALUE);
                    rowArray.Add(QA_XLS_COL18_VALUE);
                    rowArray.Add(QA_XLS_COL19_VALUE);
                    rowArray.Add(QA_XLS_COL20_VALUE);
                    rowArray.Add(QA_XLS_COL21_VALUE);
                    rowArray.Add(QA_XLS_COL22_VALUE);
                    rowArray.Add(QA_XLS_COL23_VALUE);
                    rowArray.Add(QA_XLS_COL24_VALUE);
                    rowArray.Add(QA_XLS_COL25_VALUE);
                    rowArray.Add(QA_XLS_COL26_VALUE);
                    passFailResults.addRow(rowArray);

                }
            }
            else // qaWasRun == false
            {
                List<string> error_msg_row = new List<string>();
                error_msg_row.Add("Error : Function called before Pass Fail Analysis completed.");
                passFailResults.addRow(error_msg_row);
            }
            passFailResults.Dispose();
            return err;
        }

        PassFailThresholds getPassFailThresholds()
        {
            PassFailThresholds thresholds = new PassFailThresholds();

            thresholds._maxModeBordersRemoved = DSDBR01.Instance._DSDBR01_OMPF_max_mode_borders_removed_threshold;
            thresholds._maxModeWidth = DSDBR01.Instance._DSDBR01_OMPF_max_mode_width_threshold;
            thresholds._minModeWidth = DSDBR01.Instance._DSDBR01_OMPF_min_mode_width_threshold;
            thresholds._maxModeBDCArea = DSDBR01.Instance._DSDBR01_OMPF_max_mode_bdc_area_threshold;
            thresholds._maxModeBDCAreaXLength = DSDBR01.Instance._DSDBR01_OMPF_max_mode_bdc_area_x_length_threshold;
            thresholds._sumModeBDCAreas = DSDBR01.Instance._DSDBR01_OMPF_sum_mode_bdc_areas_threshold;
            thresholds._numModeBDCAreas = DSDBR01.Instance._DSDBR01_OMPF_num_mode_bdc_areas_threshold;
            thresholds._modalDistortionAngle = DSDBR01.Instance._DSDBR01_OMPF_modal_distortion_angle_threshold;
            thresholds._modalDistortionAngleXPosition = DSDBR01.Instance._DSDBR01_OMPF_modal_distortion_angle_x_pos_threshold;
            thresholds._middleLineRMSValue = DSDBR01.Instance._DSDBR01_OMPF_max_middle_line_rms_value;
            thresholds._minMiddleLineSlope = DSDBR01.Instance._DSDBR01_OMPF_min_middle_line_slope;
            thresholds._maxMiddleLineSlope = DSDBR01.Instance._DSDBR01_OMPF_max_middle_line_slope;
            thresholds._maxPrGap = DSDBR01.Instance._DSDBR01_OMPF_max_pr_gap;
            thresholds._maxLowerPr = DSDBR01.Instance._DSDBR01_OMPF_max_lower_pr;
            thresholds._minUpperPr = DSDBR01.Instance._DSDBR01_OMPF_min_upper_pr;
            thresholds._maxModeLineSlope = DSDBR01.Instance._DSDBR01_OMPF_max_mode_line_slope_threshold;
            thresholds._minModeLineSlope = DSDBR01.Instance._DSDBR01_OMPF_min_mode_line_slope_threshold;

            return thresholds;
        }


    }
}

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.Algorithms;

namespace Bookham.TestLibrary.Algorithms
{
    public class Alg_MapAnalysisException:AlgorithmException
    {
        public Alg_MapAnalysisException(string message):base(message)
        {

        }
        public Alg_MapAnalysisException(string message,Exception e):base(message,e)
        {

        }
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Bookham.TestLibrary.Algorithms
{
    public static class Util_WriteDataForDebug
    {


        public static void WriteDataToFile(List<CLaserModeMap> _map_max_deltaPr,string filename)
        {
            using (StreamWriter sw = new StreamWriter(filename))
            {
                foreach (CLaserModeMap map in _map_max_deltaPr)
                {
                    for (int r = 0; r < map._rows; r++)
                    {
                        for (int c = 0; c < map._cols; c++)
                        {
                            sw.Write(map._map[r * map._cols + c]);
                            if (c == map._cols - 1)
                                sw.WriteLine();
                            else
                                sw.Write(",");
                        }
                    }
                }
            }
        }

        public static void WriteDataToFile(boundary_multimap_typedef _boundary_points, string filename)
        {
            using (StreamWriter sw = new StreamWriter(filename))
            {
                foreach (KeyValuePair<int, int> item in _boundary_points)
                {
                    sw.WriteLine(item.Key+","+item.Value);
                }
            }
        }

    }
}

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;


namespace Bookham.TestLibrary.Algorithms
{

    public class Test
    {
        static void Main()
        {
            //Environment.CurrentDirectory = @"D:\purney.xie\FCU Mapping\CG_C#version_Tommyu\Alg_MapAnalysis";

            //string laser_ID = "DS185330.001";
            //string datetimeStamp = "20100125103200";

            //Alg_MapAnalysis map = new Alg_MapAnalysis("Data\\Parameters.xml");

            //ScreeningOverallMapResults Om = map.DoScreeningOverallMapAnalysis(laser_ID, datetimeStamp);

        }
    }

    public class Alg_MapAnalysis
    {
        #region Private Data

        String results_dir;
        String laserType;

        String laser_id;
        String date_time_stamp;
        OverallRampDir overallmap_ramp_direction;
        SupermodeRampDir supermode_ramp_direction;

        TestInfo testInfo;

        short _overall_map_loaded = 0;
        short _supermodes_found_count = 0;
        short _supermode_map_loaded_count = 0;
        short _estimate_count = 0;
        short _ITUOP_count = 0;
        short _duff_ITUOP_count = 0;

        List<short> _supermode_map_screening_passed = new List<short>();
        int _overall_map_screening_passed;
        CDSDBROverallModeMap _overall_mode_map;
        
        #endregion

        #region Constructor
        public Alg_MapAnalysis(string results_dir, string settingsFilePath)
        {
            DSDBR01.Initialize(settingsFilePath);

            //results_dir = DSDBR01.Instance._DSDBR01_BASE_results_directory;
            this.results_dir = results_dir;
            laserType = Defaults.LASER_TYPE_ID_DSDBR01_CSTRING;
            overallmap_ramp_direction = (OverallRampDir)Enum.Parse(typeof(OverallRampDir),
                                        DSDBR01.Instance._DSDBR01_OMDC_ramp_direction);
            ////supermode_ramp_direction = (SupermodeRampDir)Enum.Parse(typeof(SupermodeRampDir),
            ////                            DSDBR01.Instance._DSDBR01_SMDC_ramp_direction);

        }
        #endregion

        #region Public methods

        public ScreeningOverallMapResults DoScreeningOverallMapAnalysis(string laserID, string datetimeStamp, string laserWaferID)
        {
            laser_id = laserID;
            date_time_stamp = datetimeStamp;

            testInfo = new TestInfo(results_dir, laserType, laser_id, date_time_stamp,
                overallmap_ramp_direction, supermode_ramp_direction);

            //clear
            Clear();
            _overall_mode_map = new CDSDBROverallModeMap();
            _overall_mode_map.clear();
            _overall_mode_map.testInfo = testInfo;

            //1.read data from csv file
            _overall_mode_map.ReadMapsFromFile();
            _overall_map_loaded = 1;
            _overall_mode_map._results_dir_CString = results_dir;
            _overall_mode_map._laser_id_CString = laser_id;
            _overall_mode_map._date_time_stamp_CString = date_time_stamp;
            _overall_mode_map._laser_wafer_id_CString = laserWaferID;

            //2.call screening procedure
            _overall_mode_map.screen_New();

            //3.write screening results to file
            _overall_mode_map.writeScreeningResultsToFile();

            //4.return results
            short num_of_supermodes = _overall_mode_map.supermode_count();
            if (!(num_of_supermodes > 0))
                setSMFoundCount(0);
            else
                setSMFoundCount(num_of_supermodes);

            _overall_map_screening_passed = _overall_mode_map._overallModeMapAnalysis._screeningPassed ? 1 : 0;

            List<double> average_ref_powers_on_middle_lines = new List<double>();
            List<double> average_filter_DAC_on_middle_lines = new List<double>();

            CDSDBROverallModeMap.rcode get_avg_powers_rval =
                _overall_mode_map.GetAveragePowersOnMiddleLines(average_ref_powers_on_middle_lines, _overall_mode_map._map_forward_direct_power);

            CDSDBROverallModeMap.rcode get_filter_avg_DAC_rval =
    _overall_mode_map.GetAveragePowersOnMiddleLines(average_filter_DAC_on_middle_lines, _overall_mode_map._map_forward_filtered_power);

            if (get_avg_powers_rval != CDSDBROverallModeMap.rcode.ok)
            {
                String log_err_msg = "getScreeningOverallMapResults error: ";
                throw new Alg_MapAnalysisException(log_err_msg);
            }
            string collatedDFSFile = collateDFSMaps(testInfo, testInfo.Overallmap_ramp_direction.ToString());
            ScreeningOverallMapResults omr = new ScreeningOverallMapResults(testInfo, _overall_map_screening_passed
                , _supermodes_found_count, average_ref_powers_on_middle_lines.ToArray(), average_filter_DAC_on_middle_lines.ToArray(), collatedDFSFile);
            //matrix_Pratio_summary_map = collatedDFSFile;
            return omr;
            

        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="supermode_number">where supermode_number is the supermode number, indexed from 0</param>
        /// <returns></returns>
        ////public ScreeningSMMapResults DoScreeningSMMapAnalysis(int supermode_number)
        ////{
        ////    if (supermode_number > _supermodes_found_count - 1 || supermode_number < 0)
        ////    {
        ////        // supermode number requested is not available
        ////        throw new Alg_MapAnalysisException("supermode number requested is not available");
        ////    }
        ////    // use this to point to the object representing SM Map to be screened
        ////    CDSDBRSuperMode p_sm_map = _overall_mode_map._supermodes[supermode_number];
        ////    _overall_mode_map._supermodes[supermode_number].clearMapsAndLines();
        ////    // set results dir, laser_id and date_time_stamp
        ////    _overall_mode_map._supermodes[supermode_number]._results_dir_CString = results_dir;
        ////    _overall_mode_map._supermodes[supermode_number]._laser_id_CString = laser_id;
        ////    _overall_mode_map._supermodes[supermode_number]._date_time_stamp_CString = date_time_stamp;

        ////    _estimate_count = 0;
        ////    _ITUOP_count = 0;
        ////    _duff_ITUOP_count = 0;

        ////    p_sm_map._superModeMapAnalysis.init();

        ////    CDSDBRSuperMode.rcode rval_collection = CDSDBRSuperMode.rcode.ok;
        ////    CDSDBRSuperMode.rcode rval_screening = CDSDBRSuperMode.rcode.ok;
        ////    CDSDBRSuperMode.rcode rval_writeresults = CDSDBRSuperMode.rcode.ok;
        ////    //1. read data from csv file
        ////    rval_collection = p_sm_map.ReadMapsFromFile(testInfo.ResultsDir, testInfo.LaserId, testInfo.DateTimeStamp, testInfo.Supermode_ramp_direction.ToString());
        ////    if (rval_collection != CDSDBRSuperMode.rcode.ok)
        ////        throw new Alg_MapAnalysisException(rval_collection.ToString());

        ////    short longitudinal_mode_count = 0;

        ////    //2. call screening procedure
        ////    rval_screening = p_sm_map.screen();

        ////    if (p_sm_map._superModeMapAnalysis._screeningPassed)
        ////        setSMScreeningPassed(p_sm_map._sm_number, 1);
        ////    else
        ////        setSMScreeningPassed(p_sm_map._sm_number, 0);

        ////    //3.write screening results to file
        ////    rval_writeresults = p_sm_map.writeScreeningResultsToFile();

        ////    longitudinal_mode_count = p_sm_map.getLMCount();
        ////    short sm_map_loaded_count = 0;
        ////    for (int i = 0; i < _overall_mode_map._supermodes.Count; i++)
        ////    {
        ////        if (_overall_mode_map._supermodes[i]._maps_loaded)
        ////            sm_map_loaded_count++;
        ////    }
            
        ////    _supermode_map_loaded_count = sm_map_loaded_count;

        ////    if (rval_screening != CDSDBRSuperMode.rcode.ok)
        ////        throw new Alg_MapAnalysisException(rval_screening.ToString());
        ////    if (rval_writeresults != CDSDBRSuperMode.rcode.ok)
        ////        throw new Alg_MapAnalysisException(rval_writeresults.ToString());
        ////    if( !(longitudinal_mode_count > 0) )
        ////        throw new Alg_MapAnalysisException(CGSystemErrors.CGSYSTEM_NO_VALID_LONGITUDINAL_MODES_FOUND);

        ////    //4.return results
        ////    if (supermode_number > _supermodes_found_count - 1 || supermode_number < 0)
        ////        throw new Alg_MapAnalysisException(CGSystemErrors.CGSYSTEM_SPECIFIED_SUPERMODE_NOT_FOUND);
        ////    if( !(_overall_mode_map._supermodes[supermode_number]._maps_loaded) )
        ////        throw new Alg_MapAnalysisException(CGSystemErrors.CGSYSTEM_SPECIFIED_SUPERMODE_NOT_LOADED);

        ////    //string p_results_dir = _overall_mode_map._supermodes[supermode_number]._results_dir_CString;
        ////    //string p_laser_type = "DSDBR01";
        ////    //string p_laser_id = _overall_mode_map._supermodes[supermode_number]._laser_id_CString;
        ////    //string p_date_time_stamp = _overall_mode_map._supermodes[supermode_number]._date_time_stamp_CString;
        ////    //string p_map_ramp_direction;
        ////    //if (_overall_mode_map._supermodes[supermode_number]._Im_ramp)
        ////    //    p_map_ramp_direction = Defaults.RAMP_DIRECTION_MIDDLE_LINE;
        ////    //else
        ////    //    p_map_ramp_direction = Defaults.RAMP_DIRECTION_PHASE;
        ////    int p_longitudinal_mode_count = _overall_mode_map._supermodes[supermode_number].getLMCount();
        ////    int p_screening_sm_map_passed = getSMScreeningPassed(supermode_number);

        ////    ScreeningSMMapResults smRtv = new ScreeningSMMapResults(testInfo, supermode_number, p_screening_sm_map_passed, p_longitudinal_mode_count);

        ////    return smRtv;
        ////}

        #endregion

        #region Private methods

        int getSMScreeningPassed(int sm_number)
        {
            int supermode_map_screening_passed;
            if (sm_number < _supermode_map_screening_passed.Count)
                supermode_map_screening_passed = _supermode_map_screening_passed[sm_number];
            else
                throw new ArgumentOutOfRangeException();
            return supermode_map_screening_passed;
        }
        void Clear()
        {
            _overall_map_loaded = 0;
            _overall_map_screening_passed = 0;
            setSMFoundCount(0);
            _supermode_map_loaded_count = 0;
            _estimate_count = 0;
            _ITUOP_count = 0;
            _duff_ITUOP_count = 0;
            //_ops.clear();
        }
        /// <summary>
        /// Collate the DFS (overall front section maps)
        /// </summary>
        /// <param name="testInfo">Info describing what to load</param>
        /// <param name="rampDirStr">Ramp direction</param>
        /// <returns>Filename of collated map</returns>
        private string collateDFSMaps(TestInfo testInfo, string rampDirStr)
        {
            string collatedDFSFile = string.Format("{0}\\MATRIX_PowerRatio_{1}_DFS_{2}.csv",
                            testInfo.ResultsDir, testInfo.ToString(),
                            rampDirStr);

            using (StreamWriter writer = new StreamWriter(collatedDFSFile))
            {
                for (int frontSectionPair = 1; frontSectionPair <= 7; frontSectionPair++)
                {
                    string mapFileName = string.Format("{0}\\MATRIX_PowerRatio_{1}_DFS{2}_{3}.csv",
                        testInfo.ResultsDir, testInfo.ToString(),
                        frontSectionPair, rampDirStr);
                    using (StreamReader reader = new StreamReader(mapFileName))
                    {
                        string line;
                        while ((line = reader.ReadLine()) != null)
                        {
                            writer.WriteLine(line);
                        }
                    }
                }
            }
            return collatedDFSFile;
        }
        enum rcode_type
        {
            ok = 0,
            not_1D_array_of_doubles,
            empty_array,
            null_arg,
            variant_clear_error,
            variant_array_creation_error
        }
        void setSMFoundCount(short supermodes_found_count)
        {
            _supermodes_found_count = supermodes_found_count;
            _supermode_map_screening_passed.Clear();
            for (short i = 0; i < supermodes_found_count; i++)
                _supermode_map_screening_passed.Add(Defaults.SM_NOT_LOADED);
        }
        void setSMScreeningPassed(short sm_number, short supermode_map_screening_passed)
        {
            if (sm_number < (short)_supermode_map_screening_passed.Count)
                _supermode_map_screening_passed[sm_number] = supermode_map_screening_passed;
        }

        #endregion


    }

    #region Other types

    internal class TestInfo
    {
        public TestInfo(string resultsDir, string laserType, string laserId, string dateTimeStamp
            , OverallRampDir overallmap_ramp_direction, SupermodeRampDir supermode_ramp_direction)
        {
            this.ResultsDir = resultsDir;
            this.DateTimeStamp = dateTimeStamp;
            this.LaserType = laserType;
            this.LaserId = laserId;
            this.Overallmap_ramp_direction = overallmap_ramp_direction;
            this.Supermode_ramp_direction = supermode_ramp_direction;
        }

        /// <summary>Results Directory</summary>
        public readonly string ResultsDir;
        /// <summary>Date Time Stamp</summary>
        public readonly string DateTimeStamp;
        /// <summary>Laser Type</summary>
        public readonly string LaserType;
        /// <summary>Laser ID</summary>
        public readonly string LaserId;

        public readonly OverallRampDir Overallmap_ramp_direction;
        public readonly SupermodeRampDir Supermode_ramp_direction;

        /// <summary>
        /// Get the string that is commonly used in file names based on this structure.
        /// Combination of laser type, id and date-time stamp
        /// </summary>
        /// <returns>info string</returns>
        public override string ToString()
        {
            string infoStr = string.Format("{0}_{1}_{2}",
                this.LaserType, this.LaserId, this.DateTimeStamp);
            return infoStr;
        }
    }

    /// <summary>
    /// Ramp Direction used for Overall Map
    /// </summary>
    public enum OverallRampDir
    {
        /// <summary>Rear Current Ramped</summary>
        R,
        /// <summary>Front Current Ramped</summary>
        F,
    }

    /// <summary>
    /// Ramp Direction used for Supermode Map
    /// </summary>
    public enum SupermodeRampDir
    {
        /// <summary>Phase Current Ramped</summary>
        P,
        /// <summary>Middle-Line (Combination of Front+Rear) Ramped</summary>
        M
    }


    public class ScreeningOverallMapResults
    {
        internal ScreeningOverallMapResults(TestInfo info, int screeningPassed,
            int supermodeCount, double[] avgPowers_mW, double[] avg_filetr_DAC, string combinedDFSMapFile)
        {
            this.RampDir = info.Overallmap_ramp_direction;
            ScreeningPassed = screeningPassed == 1;
            this.SupermodeCount = supermodeCount;

            this.Avg_Ref_Powers_mW = avgPowers_mW;
            this.Avg_Filter_DAC = avg_filetr_DAC;
            MiddleLineCurrentsFile = new string[supermodeCount];
            for (int i = 0; i < supermodeCount; i++)
                MiddleLineCurrentsFile[i] = string.Format("{0}\\Im_{1}_SM{2}.csv",
                                            info.ResultsDir, info.ToString(), i.ToString());
            this.SupermodeLinesFile =
                string.Format("{0}\\lines_OverallMap_{1}.csv",
                info.ResultsDir, info.ToString());
            this.PowerRatioSmFile =
                string.Format("{0}\\PowerRatio_Continuity_{1}.csv",
                info.ResultsDir, info.ToString());
            this.PassFailFile =
               string.Format("{0}\\passFail_OverallMap_{1}.csv",
               info.ResultsDir, info.ToString());
            this.QaMetricsFile =
                string.Format("{0}\\qaMetrics_OverallMap_{1}.csv",
                info.ResultsDir, info.ToString());

            this.CombinedDFSMapFile = combinedDFSMapFile;
            
        }

        /// <summary>Ramp direction</summary>
        public readonly OverallRampDir RampDir;
        /// <summary>Overall Map Screening Pass/Fail</summary>
        public readonly bool ScreeningPassed;
        /// <summary>Number of supermodes found</summary>
        public readonly int SupermodeCount;
        /// <summary>Average power along middle line of each supermode</summary>
        public readonly double[] Avg_Ref_Powers_mW;
        /// <summary>
        /// Average DAC value along middle line of each supermode for FCUMKI Lowcost mapping
        /// </summary>
        public readonly double[] Avg_Filter_DAC;

        /// <summary>Middle line currents of each supermode found</summary>
        public readonly string[] MiddleLineCurrentsFile;
        /// <summary>A single file containing all lines for all supermodes found</summary>        
        public readonly string SupermodeLinesFile;
        /// <summary>Power ratio values on Middle lines of all supermodes found</summary>                
        public readonly string PowerRatioSmFile;
        /// <summary>Quantitative Analysis results from screening the overall map</summary>                        
        public readonly string QaMetricsFile;
        /// <summary>Pass/Fail file</summary>
        public readonly string PassFailFile;

        /// <summary>Collated Front Section summary map</summary>                                
        public readonly string CombinedDFSMapFile;

    }

    public class ScreeningSMMapResults
    {
        /*internal ScreeningSMMapResults(TestInfo testInfo, int supermode,
                    int screeningPassed, int longitudinalModeCount)
        {
            this.Supermode = supermode;
            this.RampDir = testInfo.Supermode_ramp_direction;
            ScreeningPassed = screeningPassed == 1;
            this.LongitudinalModeCount = longitudinalModeCount;

    ////        // common file ending - calculate it once!
    ////        string filePostfix1 = string.Format("{0}_SM{1}.csv", testInfo.ToString(), supermode);

    ////        this.PassFailFile = string.Format("{0}\\passFail_{1}", testInfo.ResultsDir, filePostfix1);

    ////        this.QaMetricsFile = string.Format("{0}\\qaMetrics_{1}", testInfo.ResultsDir, filePostfix1);

    ////        this.LongitudinalModeLinesFile = string.Format("{0}\\lines_{1}", testInfo.ResultsDir, filePostfix1);

    ////        string filePostfix2 = string.Format("{0}_SM{1}_{2}.csv", testInfo.ToString(), supermode, RampDir);

    ////        this.MatrixPowerForwardFile = string.Format("{0}\\MATRIX_forwardPower_{1}", testInfo.ResultsDir, filePostfix2);

    ////        this.MatrixPowerRatioForwardFile = string.Format("{0}\\MATRIX_forwardPowerRatio_{1}",
    ////                    testInfo.ResultsDir, filePostfix2);
    ////        this.MatrixPowerRatioReverseFile = string.Format("{0}\\MATRIX_reversePowerRatio_{1}",
    ////                    testInfo.ResultsDir, filePostfix2);

    ////        this.MatrixPD1CurrentForwardFile = string.Format("{0}\\MATRIX_forwardPD1Current_{1}",
    ////                    testInfo.ResultsDir, filePostfix2);
    ////        this.MatrixPD1CurrentReverseFile = string.Format("{0}\\MATRIX_reversePD1Current_{1}",
    ////                    testInfo.ResultsDir, filePostfix2);

    ////        this.MatrixPD2CurrentForwardFile = string.Format("{0}\\MATRIX_forwardPD2Current_{1}",
    ////                    testInfo.ResultsDir, filePostfix2);
    ////        this.MatrixPD2CurrentReverseFile = string.Format("{0}\\MATRIX_reversePD2Current_{1}",
    ////                    testInfo.ResultsDir, filePostfix2);

    ////        // lookup the hysteresis percentage
    ////        string elemStr = CsvDataLookup.GetElement(this.QaMetricsFile, 9, 1);
    ////        this.HysteresisPercent = Double.Parse(elemStr);

    ////        // calculate the Modal Distortion
    ////        double modalDist = CsvDataLookup.ModalDistortion
    ////            (this.QaMetricsFile, this.LongitudinalModeCount, true);
    ////        this.ModalDistortion = modalDist;
    ////    }

    ////    /// <summary>Supermode number (0 to N-1)</summary>
    ////    public readonly int Supermode;
    ////    /// <summary>Ramp direction</summary>
    ////    public readonly SupermodeRampDir RampDir;
    ////    /// <summary>Screening passed</summary>
    ////    public readonly bool ScreeningPassed;
    ////    /// <summary>Longitudinal mode count</summary>
    ////    public readonly int LongitudinalModeCount;

    ////    /// <summary>Hysteresis Percentage</summary>
    ////    public readonly double HysteresisPercent;
    ////    /// <summary>Modal Distortion</summary>
    ////    public readonly double ModalDistortion;

    ////    /// <summary>Pass/Fail file</summary>
    ////    public readonly string PassFailFile;
    ////    /// <summary>QA metrics file</summary>
    ////    public readonly string QaMetricsFile;
    ////    /// <summary>Longitudinal Mode lines file</summary>
    ////    public readonly string LongitudinalModeLinesFile;

    ////    /// <summary>Matrix file for power on forward sweep</summary>
    ////    public readonly string MatrixPowerForwardFile;

    ////    /// <summary>Matrix file for power ratio on forward sweep</summary>
    ////    public readonly string MatrixPowerRatioForwardFile;
    ////    /// <summary>Matrix file for power ratio on reverse sweep</summary>
    ////    public readonly string MatrixPowerRatioReverseFile;

        /// <summary>Matrix file for photocurrent 1 on forward sweep</summary>
        public readonly string MatrixPD1CurrentForwardFile;
        /// <summary>Matrix file for photocurrent 1 on reverse sweep</summary>
        public readonly string MatrixPD1CurrentReverseFile;
        /// <summary>Matrix file for photocurrent 2 on forward sweep</summary>
        public readonly string MatrixPD2CurrentForwardFile;
        /// <summary>Matrix file for photocurrent 2 on reverse sweep</summary>
        public readonly string MatrixPD2CurrentReverseFile;*/
    }

    #endregion







}

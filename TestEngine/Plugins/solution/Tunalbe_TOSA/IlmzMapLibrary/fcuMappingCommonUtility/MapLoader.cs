using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.Utilities;


namespace Bookham.fcumapping.utility
{
    public class MapLoader
    {

        public MapLoader() { }

        /// <summary>
        /// Load map file into memory
        /// </summary>
        /// <param name="file">Map file to load</param>
        /// <returns>2D array of double</returns>
        public static double[,] LoadMap(string file)
        {
            double[,] map = null;
            using (CsvReader csvReader = new CsvReader())
            {
                List<string[]> lines = csvReader.ReadFile(file);
                int nbrRows = lines.Count;
                int nbrColumns = lines[0].Length;

                map = new double[nbrRows, nbrColumns];
                string str;
                for (int ii = 0; ii < nbrRows; ii++)
                {
                    for (int jj = 0; jj < nbrColumns; jj++)
                    {
                        str = lines[ii][jj];
                        map[ii, jj] = double.Parse(str);
                    }
                }
            }
            return map;
        }

        public static double[] loadArrayDouble(string file)
        {
            double[] arr = null;
            using(CsvReader csvReader = new CsvReader())
            {
                List<string[]> lines = csvReader.ReadFile(file);
                int nbrRows = lines.Count;
                arr = new double[nbrRows];
                string str;
                for(int i=0;i<nbrRows;i++)
                {

                    str = lines[i][0];
                    arr[i] = double.Parse(str);
                }
            }
            return arr;
        }

        public static List<double> loadArrayList(string file)
        {

            List<double> arr = new List<double>();
            using(CsvReader csvReader = new CsvReader())
            {
                List<string[]> lines = csvReader.ReadFile(file);
                string str;
                for(int i=0;i<lines.Count;i++)
                {
                    str = lines[i][0];
                    arr.Add(double.Parse(str));
                }
            }
            return arr;
        }
    }
}

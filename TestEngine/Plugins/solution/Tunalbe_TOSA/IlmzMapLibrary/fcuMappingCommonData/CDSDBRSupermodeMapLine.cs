using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Bookham.TestLibrary.Utilities;

namespace Bookham.fcumapping.CommonData
{
    public sealed class CDSDBRSupermodeMapLine
    {
        public class Point_Type
        {
            public long row;
            public long col;
            public double real_row;
            public double real_col;
            public short front_pair_number;
            public double constantCurrent;
            public double nonConstantCurrent;
            public double I_rear;
            public double I_phase;

            public double Forward_Filter;
            public double Forward_Reference;
            public double Forward_PowerRatio;
            public double Forward_Tx;
            public double Forward_Rx;
            public double Forward_LockerRatio;
            public double Reverse_Filter;
            public double Reverse_Reference;
            public double Reverse_PowerRatio;
            public double Reverse_Tx;
            public double Reverse_Rx;
            public double Reverse_LockerRatio;
        }

        public CDSDBRSupermodeMapLine()
        { }

        void Clear()
        {
            _points.Clear();
        }

        public bool hasChangeOfFrontPairs()
        {
            //stub here, need to be overwritten;
            if (_points.Count == 0)
            {
                return false;
            }
            short start_front_pair_num = _points[0].front_pair_number;
            for (int i = 1; i < _points.Count; i++)
            {
                if (_points[i].front_pair_number != start_front_pair_num)
                {
                    return true;
                }
            }
            return false;
        }

        public List<short> getFrontPairNumbers()
        {
            //stub here, need to be overwritten;
            return null;
        }

        public void writeRowColToFile(string abs_filepath, bool overwrite)
        {
            abs_filepath.Trim();
            int index = abs_filepath.LastIndexOf("\\");
            string dir = abs_filepath.Substring(0, index);
            if (Directory.Exists(dir))
            {
                if (File.Exists(abs_filepath) && overwrite == true)
                {
                    File.Delete(abs_filepath);
                    FileStream fs = new FileStream(abs_filepath, FileMode.Create, FileAccess.Write, FileShare.None);
                    using (StreamWriter sw = new StreamWriter(fs))
                    {
                        if (_points.Count == 0)
                        {
                            sw.WriteLine("No data");
                        }
                        else
                        {
                            sw.WriteLine("Row,Col");
                            for (int i = 0; i < _points.Count; i++)
                            {
                                sw.Write(_points[i].row + ",");
                                sw.Write(_points[i].col + ",");
                                sw.Write("\n");
                            }

                        }
                    };
                }
                else
                {
                    throw new Exception(abs_filepath + "File has existed");

                }
            }
            else
            {
                throw new Exception(abs_filepath + "Directory not exists");

            }
            return;
        }

        public void readRowColFromFile(string abs_filepath)
        {
            abs_filepath.Trim();
            int index = abs_filepath.LastIndexOf("\\");
            string dir = abs_filepath.Substring(0, index);
            if (Directory.Exists(dir))
            {
                if (File.Exists(abs_filepath))
                {
                    using (CsvReader csvReader = new CsvReader())
                    {
                        csvReader.OpenFile(abs_filepath);
                        List<string[]> data = csvReader.ReadFile(abs_filepath);
                        for (int i = 1; i < data.Count; i++)
                        {
                            Point_Type new_point = new Point_Type();
                            new_point.row = Convert.ToInt64(data[i][0].ToString());
                            new_point.col = Convert.ToInt64(data[i][1].ToString());
                            _points.Add(new_point);
                        }
                        csvReader.CloseFile();
                    }
                }
                else
                {
                    throw new Exception(abs_filepath + "File has existed");
                }
            }
            else
            {
                throw new Exception(abs_filepath + "Directory not exists");
            }
            return;
        }
        /// <summary>
        /// write data to file(*.csv)
        /// </summary>
        /// <param name="abs_filepath"></param>
        /// <param name="overrite"></param>
        public void writeCurrentsToFile(string abs_filepath, bool overrite)
        {
            abs_filepath.Trim();
            int index = abs_filepath.LastIndexOf("\\");
            string dir = abs_filepath.Substring(0, index);
            if (Directory.Exists(dir))
            {
                if (File.Exists(abs_filepath) && overrite == true)
                {
                    File.Delete(abs_filepath);
                    FileStream fs = new FileStream(abs_filepath, FileMode.Create, FileAccess.Write, FileShare.None);
                    using (StreamWriter sw = new StreamWriter(fs))
                    {
                        if (_points.Count == 0)
                        {
                            sw.WriteLine("No data");
                        }
                        else
                        {
                            sw.WriteLine("Rear Current [mA],Front Pair Number,Constant Front Current [mA],"
                               + "Non-constant Front Current [mA],Phase Current [mA],Row Index,Column Index");
                            for (int i = 0; i < _points.Count; i++)
                            {
                                sw.Write(_points[i].I_rear + ",");
                                sw.Write(_points[i].front_pair_number + ",");
                                sw.Write(_points[i].constantCurrent + ",");
                                sw.Write(_points[i].nonConstantCurrent + ",");
                                sw.Write(_points[i].I_phase + ",");
                                sw.Write(_points[i].real_row + ",");
                                sw.Write(_points[i].real_col + ",");
                                sw.Write("\n");
                            }

                        }
                    };
                }
                else
                {
                    throw new Exception(abs_filepath + "File has existed");

                }
            }
            else
            {
                throw new Exception(abs_filepath + "Directory not exists");

            }
            return;
        }
        /// <summary>
        ///  read currents from file(*.csv)
        /// </summary>
        /// <param name="abs_filepath"></param>
        public void readCurrentsFromFile(string abs_filepath)
        {
            abs_filepath.Trim();
            int index = abs_filepath.LastIndexOf("\\");
            string dir = abs_filepath.Substring(0, index);
            if (Directory.Exists(dir))
            {
                if (File.Exists(abs_filepath))
                {
                    using (CsvReader csvReader = new CsvReader())
                    {
                        csvReader.OpenFile(abs_filepath);
                        List<string[]> data = csvReader.ReadFile(abs_filepath);
                        for (int i = 1; i < data.Count; i++)
                        {
                            Point_Type point = new Point_Type();
                            point.I_rear = Convert.ToDouble(data[i][0].ToString());
                            point.front_pair_number = Convert.ToInt16(data[i][1].ToString());
                            point.constantCurrent = Convert.ToDouble(data[i][2].ToString());
                            point.nonConstantCurrent = Convert.ToDouble(data[i][3].ToString());
                            point.I_phase = Convert.ToDouble(data[i][4].ToString());
                            point.real_row = Convert.ToDouble(data[i][5].ToString());
                            point.real_col = Convert.ToDouble(data[i][6].ToString());
                            _points.Add(point);
                        }
                        csvReader.CloseFile();
                    }
                }
                else
                {
                    throw new Exception(abs_filepath + "File has existed");
                }
            }
            else
            {
                throw new Exception(abs_filepath + "Directory not exists");
            }
            return;
        }

        public List<double> getRows()
        {
            //stub here, need to be overwritten
            List<double> rows = new List<double>();
            for (int i = 0; i < _points.Count; i++)
            {
                rows.Add(_points[i].real_row);
            }
            return rows;
        }

        /// <summary>
        ///  note that the parameter start begins with 1.
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public List<double> getRows(int start, int end)
        {
            if (start >= end)
                return null;
            if (end > _points.Count)
            {
                end = _points.Count;
            }
            List<double> rows = new List<double>();
            for (int i = start - 1; i < end; i++)
            {
                rows.Add(_points[i].real_row);
            }
            return rows;
        }
        public bool updateSubmapTracking()
        {
            bool returnValue = true;

            return returnValue;
        }
        public List<double> getCols()
        {
            //stub here, need to be overwritten
            List<double> cols = new List<double>();
            for (int i = 0; i < _points.Count; i++)
            {
                cols.Add(_points[i].real_col);
            }
            return cols;
        }
        /// <summary>
        /// get the array item index range from start and end. start is counted from 1
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>

        public List<double> getCols(int start, int end)
        {
            if (start >= end)
                return null;
            if (end > _points.Count)
            {
                end = _points.Count;
            }
            List<double> cols = new List<double>();
            for (int i = start - 1; i < end; i++)
            {
                cols.Add(_points[i].real_col);
            }
            return cols;
        }

        public long getLength()
        {
            return _points.Count;
        }

        // clear all contents
        public void clear()
        {
            _points.Clear();
        }
        public bool IsMissingLine
        {
            get { return _ismissingline; }
            set { _ismissingline = value; }
        }

        public List<Point_Type> _points = new List<Point_Type>();
        private bool _ismissingline;
    }
}

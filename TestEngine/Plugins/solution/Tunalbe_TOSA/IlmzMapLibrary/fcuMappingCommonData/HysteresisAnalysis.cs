using System;
using System.Collections.Generic;
using System.Text;
//coded by purney.xie
namespace Bookham.fcumapping.CommonData
{
    class HysteresisAnalysis
    {

        public HysteresisAnalysis()
        {
            init();
        }
        public HysteresisAnalysis(ModeAnalysis forwardMode, ModeAnalysis reverseMode, int xAxisLength, int yAxisLength)
        {
            _forwardMode = forwardMode;
            _reverseMode = reverseMode;

            _xAxisLength = xAxisLength;
            _yAxisLength = yAxisLength;

            init();
        }

        public void clear()
        {
            _forwardMode.init();
            _reverseMode.init();
            _innerHysteresisMode.init();
            _outerHysteresisMode.init();

            _xAxisLength = 0;
            _yAxisLength = 0;

            _stableAreaWidths.Clear();
            _totalWidths.Clear();
            _percWorkingRegions.Clear();

            _maxStableAreaWidth = 0;
            _minStableAreaWidth = 0;
            _meanStableAreaWidths = 0;
            _sumStableAreaWidths = 0;
            _numStableAreaWidthSamples = 0;
            _stdDevStableAreaWidths = 0;
            _varStableAreaWidths = 0;


            _maxPercWorkingRegion = 0;
            _minPercWorkingRegion = 0;
            _meanPercWorkingRegions = 0;
            _sumPercWorkingRegions = 0;
            _numPercWorkingRegionSamples = 0;
            _stdDevPercWorkingRegions = 0;
            _varPercWorkingRegions = 0;

        }

        //GDM 31/10/67 added use of qaThreshold values for hysteresis window 
        public void setQAThresholds(QAThresholds qaThresholds)
        {
            _qaThresholds = qaThresholds;
        }

        public bool runAnalysis()
        {
            bool returnValue = true;
            if (returnValue == true)
                returnValue = runStableAreaWidthsAnalysis();
            if (returnValue == true)
                runPercWorkingRegionAnalysis();
            return returnValue;
        }
        public void getStableAreaWidths(out Vector stableAreaWidths)
        {
            int startIndex = 0;
            int stopIndex;
            if (_stableAreaWidths.size() == 0)
            {
                //GDM 31/10/06 change of call to getModeWidths to include start and stop index's
                if (_qaThresholds._SMMapQA == 1)
                {
                    startIndex = (int)_qaThresholds._hysteresisAnalysisMinX;
                    stopIndex = (int)_qaThresholds._hysteresisAnalysisMaxX;
                    _innerHysteresisMode.getModeWidths(out _stableAreaWidths, startIndex, stopIndex);
                }
                if (_qaThresholds._SMMapQA == 0)
                {
                    //old call
                    if (_innerHysteresisMode._lowerBoundaryLine.numXPoints() > _outerHysteresisMode._lowerBoundaryLine.numXPoints())
                        startIndex = (int)(_outerHysteresisMode._lowerBoundaryLine.x(0) - _innerHysteresisMode._lowerBoundaryLine.x(0));

                    _innerHysteresisMode.getModeWidths(out _stableAreaWidths, startIndex);
                }
            }
            stableAreaWidths = _stableAreaWidths;
        }
        public double getStableAreaWidthsStatistic(VectorAttribute attrib)
        {
            double returnValue = 0;

            Vector stableAreaWidths;
            getStableAreaWidths(out stableAreaWidths);

            returnValue = stableAreaWidths.getVectorAttribute(attrib);

            return returnValue;
        }
        public void getTotalWidths(out Vector totalWidths)
        {
            short startIndex = 0;
            short stopIndex;


            if (_totalWidths.size() == 0)
                if (_qaThresholds._SMMapQA == 1)
                {
                    //GDM 31/10/06 change in call to getmodewidths to permit start and stop index
                    startIndex = (short)_qaThresholds._hysteresisAnalysisMinX;
                    stopIndex = (short)_qaThresholds._hysteresisAnalysisMaxX;

                    _outerHysteresisMode.getModeWidths(out _totalWidths, startIndex, stopIndex);
                }
            if (_qaThresholds._SMMapQA == 0)
            {
                //old call  
                _outerHysteresisMode.getModeWidths(out _totalWidths);
            }
            totalWidths = _totalWidths;
        }
        public void getPercWorkingRegions(out Vector percWorkingRegions)
        {
            if (_percWorkingRegions.size() == 0)
            {
                Vector totalWidths;
                getTotalWidths(out totalWidths);

                Vector stableAreaWidths;
                getStableAreaWidths(out stableAreaWidths);

                for (int i = 0; i < (int)_stableAreaWidths.size() && i < totalWidths.size(); i++)
                {
                    double percWorkingRegion = 0;

                    double stableAreaWidth = stableAreaWidths[i];
                    double totalWidth = totalWidths[i];

                    if (stableAreaWidth < totalWidth)
                        percWorkingRegion = 100 * (stableAreaWidth / totalWidth);
                    else
                        percWorkingRegion = 100;

                    _percWorkingRegions.Add(percWorkingRegion);
                }
            }

            percWorkingRegions = _percWorkingRegions;
        }
        public double getPercWorkingRegionsStatistic(VectorAttribute attrib)
        {
            double returnValue = 0;

            Vector percWorkingRegions;
            getPercWorkingRegions(out percWorkingRegions);

            returnValue = percWorkingRegions.getVectorAttribute(attrib);

            return returnValue;
        }


        private void init()
        {
            /////////////////////////////
            // initialise attributes
            // before use
            _stableAreaWidths.Clear();

            _maxStableAreaWidth = 0;
            _minStableAreaWidth = 0;
            _meanStableAreaWidths = 0;
            _sumStableAreaWidths = 0;
            _numStableAreaWidthSamples = 0;
            _stdDevStableAreaWidths = 0;
            _varStableAreaWidths = 0;
            // create inner hysteresis mode

            _innerHysteresisMode.loadBoundaryLines(0, _xAxisLength, _yAxisLength, _forwardMode._upperBoundaryLine, _reverseMode._lowerBoundaryLine);
            // create outer hysteresis mode
            _outerHysteresisMode.loadBoundaryLines(0, _xAxisLength, _yAxisLength, _reverseMode._upperBoundaryLine, _forwardMode._lowerBoundaryLine);
        }

        private bool runStableAreaWidthsAnalysis()
        {
            bool returnValue = true;

            Vector stableAreaWidths;
            getStableAreaWidths(out stableAreaWidths);

            calcMaxStableAreaWidths();
            calcMinStableAreaWidths();
            calcMeanStableAreaWidths();
            calcSumStableAreaWidths();
            calcNumStableAreaWidthSamples();
            calcStdDevStableAreaWidths();
            calcVarStableAreaWidths();

            return returnValue;
        }
        private bool runPercWorkingRegionAnalysis()
        {
            bool returnValue = true;
            Vector percWorkingRegions;
            getPercWorkingRegions(out percWorkingRegions);
            calcMaxPercWorkingRegions();
            calcMinPercWorkingRegions();
            calcMeanPercWorkingRegions();
            calcSumPercWorkingRegions();
            calcNumPercWorkingRegionSamples();
            calcStdDevPercWorkingRegions();
            calcVarPercWorkingRegions();
            return returnValue;
        }
        //	Functions for gathering statistics
        private void calcMaxStableAreaWidths()
        {
            Vector stableAreaWidths;
            getStableAreaWidths(out stableAreaWidths);

            _maxStableAreaWidth = stableAreaWidths.getVectorAttribute(VectorAttribute.max);
        }
        private void calcMinStableAreaWidths()
        {
            Vector stableAreaWidths;
            getStableAreaWidths(out stableAreaWidths);

            _minStableAreaWidth = stableAreaWidths.getVectorAttribute(VectorAttribute.min);
        }
        private void calcStdDevStableAreaWidths()
        {
            Vector stableAreaWidths;
            getStableAreaWidths(out stableAreaWidths);

            _stdDevStableAreaWidths = stableAreaWidths.getVectorAttribute(VectorAttribute.stdDev);
        }
        private void calcMeanStableAreaWidths()
        {
            Vector stableAreaWidths;
            getStableAreaWidths(out stableAreaWidths);

            _meanStableAreaWidths = stableAreaWidths.getVectorAttribute(VectorAttribute.mean);
        }
        private void calcSumStableAreaWidths()
        {
            Vector stableAreaWidths;
            getStableAreaWidths(out stableAreaWidths);

            _sumStableAreaWidths = stableAreaWidths.getVectorAttribute(VectorAttribute.sum);
        }
        private void calcNumStableAreaWidthSamples()
        {
            Vector stableAreaWidths;
            getStableAreaWidths(out stableAreaWidths);

            _numStableAreaWidthSamples = stableAreaWidths.getVectorAttribute(VectorAttribute.count);
        }
        private void calcVarStableAreaWidths()
        {
            Vector stableAreaWidths;
            getStableAreaWidths(out stableAreaWidths);

            _varStableAreaWidths = stableAreaWidths.getVectorAttribute(VectorAttribute.variance);
        }
        private void calcMaxPercWorkingRegions()
        {
            Vector percWorkingRegions;
            getPercWorkingRegions(out percWorkingRegions);

            _maxPercWorkingRegion = percWorkingRegions.getVectorAttribute(VectorAttribute.max);
        }
        private void calcMinPercWorkingRegions()
        {
            Vector percWorkingRegions;
            getPercWorkingRegions(out percWorkingRegions);

            _minPercWorkingRegion = percWorkingRegions.getVectorAttribute(VectorAttribute.min);
        }
        private void calcMeanPercWorkingRegions()
        {
            Vector percWorkingRegions;
            getPercWorkingRegions(out percWorkingRegions);

            _meanPercWorkingRegions = percWorkingRegions.getVectorAttribute(VectorAttribute.mean);
        }
        private void calcSumPercWorkingRegions()
        {
            Vector percWorkingRegions;
            getPercWorkingRegions(out percWorkingRegions);

            _sumPercWorkingRegions = percWorkingRegions.getVectorAttribute(VectorAttribute.sum);
        }
        private void calcNumPercWorkingRegionSamples()
        {
            Vector percWorkingRegions;
            getPercWorkingRegions(out percWorkingRegions);

            _numPercWorkingRegionSamples = percWorkingRegions.getVectorAttribute(VectorAttribute.count);
        }
        private void calcStdDevPercWorkingRegions()
        {
            Vector percWorkingRegions;
            getPercWorkingRegions(out percWorkingRegions);

            _stdDevPercWorkingRegions = percWorkingRegions.getVectorAttribute(VectorAttribute.stdDev);
        }
        private void calcVarPercWorkingRegions()
        {
            Vector percWorkingRegions;
            getPercWorkingRegions(out percWorkingRegions);

            _varPercWorkingRegions = percWorkingRegions.getVectorAttribute(VectorAttribute.variance);
        }

        #region	Data
        ModeAnalysis _forwardMode = new ModeAnalysis();
        ModeAnalysis _reverseMode = new ModeAnalysis();

        // formed by the forward modes' upper line
        // and the reverse modes' lower line
        ModeAnalysis _innerHysteresisMode = new ModeAnalysis();
        // formed by the reverse modes' upper line
        // and the forward modes' lower line
        ModeAnalysis _outerHysteresisMode = new ModeAnalysis();

        int _xAxisLength;
        int _yAxisLength;

        //GDM 31/10/06 add dec to retrieve Qa thresholds
        //	Thresholds
        QAThresholds _qaThresholds = new QAThresholds();

        private Vector _stableAreaWidths  = new Vector();
        private double _maxStableAreaWidth;
        private double _minStableAreaWidth;
        private double _meanStableAreaWidths;
        private double _sumStableAreaWidths;
        private double _numStableAreaWidthSamples;
        private double _stdDevStableAreaWidths;
        private double _varStableAreaWidths;

        private Vector _totalWidths = new Vector();

        private Vector _percWorkingRegions = new Vector();
        private double _maxPercWorkingRegion;
        private double _minPercWorkingRegion;
        private double _meanPercWorkingRegions;
        private double _sumPercWorkingRegions;
        private double _numPercWorkingRegionSamples;
        private double _stdDevPercWorkingRegions;
        private double _varPercWorkingRegions;
        #endregion
    }
}

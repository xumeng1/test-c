using System;
using System.Collections.Generic;
using System.Text;
//using Bookham.fcumapping.utility;

namespace Bookham.fcumapping.CommonData
{
    public sealed class CCurrentVector
    {
        public CCurrentVector(){}

        public void writeToFile(string abs_filepath, bool verwrite)
        {

        }

        public void readFromFile(string abs_filepath)
        {
            this._currents.Clear();
            this._currents = MapLoader.loadArrayList(abs_filepath);
            this._abs_filepath = abs_filepath;
            this._length = this._currents.Count;
        }

        public void clear()
        {

        }

        public bool isEmpty()
        {
            //stub here, need to be overwritten
            return false;
        }

        public long getLength()
        {
            return _currents.Count;
        }
        public List<double> _currents =new List<double>();
        public long _length;
        public string _abs_filepath;
    }
}

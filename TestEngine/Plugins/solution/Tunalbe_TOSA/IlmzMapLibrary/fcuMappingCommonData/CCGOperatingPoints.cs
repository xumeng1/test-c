using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.Instruments;
using Bookham.TestSolution.Instruments;
using System.IO;
using System.ComponentModel;
using System.Diagnostics;
using System.Collections.Specialized;
using Bookham.TestLibrary.FcuCommonUtils;
using Bookham.TestLibrary.Algorithms;
using Bookham.TestLibrary.Utilities;
using Bookham.TestSolution.IlmzCommonUtils;
using Bookham.TestLibrary.InstrTypes;


namespace Bookham.fcumapping.CommonData
{

    public class lm_freq_line_point_type
    {
        #region public Data
        public int sm;
        public int lm;
        public int i;
        #endregion
        public lm_freq_line_point_type() { }

        public lm_freq_line_point_type(int s, int l, int i)
        {
            sm = s;
            lm = l;
            this.i = i;
        }
    }
    public class lm_freq_line_point_type2
    {
        #region public Data
        public int sm;
        public int lm;
        public double i;
        public int insert_order;
        #endregion

        public lm_freq_line_point_type2() { }

        public lm_freq_line_point_type2(int s, int l, double i, int insert_ord)
        {
            sm = s;
            lm = l;
            this.i = i;
            insert_order = insert_ord;
        }
    }
    public enum itusearch_scheme_type
    {
        original = 0, // this must be first
        quit_itupoint_after_problem,
        remove_upper_lower_after_problem,
        resample_upper_lower_after_problem,
        remove_upper_lower_find_N_samples_after_problem,
        bisection_search_after_problem,
        brute_force_search_after_problem,
        z_method,
        end_of_schemes // this must be last
    }
    public enum itusearch_nextguess_type
    {
        interpolation = 0,
        bisection
    }
    public enum PhaseDirection
    {
        Backward, Forward
    }
    public class Matrix_Point
    {
        public double Forward_Power_Ratio;
        public double Forward_Locker_Ratio;
        public double Reverse_Power_Ratio;
        public double Reverse_Locker_Ratio;
        public double Average_Power_Ratio;
        public double Average_Locker_Ratio;
    }
    public class Matrix_Data
    {

        public int CH_Number_Modify;
        public int CH_Freq_GHz_Modify;
        public int CH_Number;
        public int CH_Freq_GHz;
        public int SM;
        public int LM;
        public int LM_Width;//LM(N) current LM
        public int Below_LM_Width;//LM(N-1) below current LM
        public int first_Ip_Index;
        public int second_Ip_Index;
        public int ITU_Group_Index;
        public double ITU_Ip_Index;
        public double ITU_Im_Index;
        public double first_Ip_current_mA;
        public double second_Ip_current_mA;
        public double ITU_Ip_current_mA;
        public double first_Power_Ratio;
        public double second_Power_Ratio;
        public double Media_Power_Ratio;
        public double first_Target_Power_Ratio;
        public double second_Target_Power_Ratio;
        public double Target_Power_Ratio;
        public double Locker_Ratio_Slope;
        public double Target_Locker_Ratio;
        public double Target_Locker_Ratio_Slope;
        public int Range_Group;
        public double ITU_Group_deltIp_current_mA = 999;
        public int LM_CH_Index;
        public int IS_ITU_CH_Count = 0;
        public bool IS_ITU_CH = false;
    }
    public class SM_ITU_SORT
    {
        public int Start_ITU_Index;
        public int End_ITU_Index;
        public int ITU_True_Count;
        public bool IS_ITU_CH;
    }
    public class SM_LM_Information
    {
        int _LMWidth;
        int _BelowLMGap;
        int _ItuCHCount;
        int _ItuCHMatchSum;
        bool _IsMissingLine;

        public int LMWidth
        {
            get
            {
                return _LMWidth;
            }
            set
            {
                _LMWidth = value;
            }
        }
        public int BelowLMGap
        {
            get
            {
                return _BelowLMGap;
            }
            set
            {
                _BelowLMGap = value;
            }
        }
        public int ItuCHCount
        {
            get
            {
                return _ItuCHCount;
            }
            set
            {
                _ItuCHCount = value;
            }

        }
        public int ItuCHMatchSum
        {
            get
            {
                return _ItuCHMatchSum;
            }
            set
            {
                _ItuCHMatchSum = value;
            }

        }
        public bool IsMissingLine
        {
            get
            {
                return _IsMissingLine;
            }
            set
            {
                _IsMissingLine = value;
            }

        }
    }

    public class CCGOperatingPoints
    {
        #region class datas
        public event ProgressChangedEventHandler OnLongitudinalModeFinished;
        public const double MAX_ITERATIONS_TO_FIND_ITU_PT = 20;     //12;kep same as the config file
        public const double MIN_VALID_FREQ_MEAS = 100000;// GHz
        public const double MAX_VALID_FREQ_MEAS = 300000; // GHzdouble MAX_ITERATIONS_TO_FIND_ITU_PT = 100;
        public const int MAX_ITERATIONS_WITH_PROBLEM = 5;
        public const double MIN_PT_GAP_TO_FIND_ITU_PT = 0.01;
        public const int MAX_ITERATIONS_WITHOUT_PROBLEM = 20;
        public List<Pair<double, lm_freq_line_point_type2>> _Freq_pts_on_freq_lines
            = new List<Pair<double, lm_freq_line_point_type2>>();
        public List<Pair<double, CDSDBROperatingPoint>> _coarse_freq_sample_points = new List<Pair<double, CDSDBROperatingPoint>>();
        public List<CDSDBROperatingPoint> _characterisation_itu_ops = new List<CDSDBROperatingPoint>();
        public List<Matrix_Data> Raw_Matrix_Data = new List<Matrix_Data>();
        public List<Matrix_Data> Fine_Matrix_Data = new List<Matrix_Data>();
        public CITUGrid _itugrid = new CITUGrid();
        private string _ITUOperatingPointsFilePath = "";
        private string _ITUEstimateFilePath = "";
        private int _ITUPointsCount = 0;
        private string TestTimeStamp = "";
        private string Laser_id = "";
        private const string ITUpointsFilePrefix = "ITUOperatingPoints_DSDBR01_";
        private const string SamplepointsFilePrefix = "CalibrationFreqPoints_DSDBR01_";
        private const string FreqPointsFilePrefix = "FrequencyCoverage_DSDBR01_";
        public int settle_time_ms;
        private string _result_dir;
        public string ITUPointsfilePath;
        private List<CDSDBRSuperMode> _p_supermodes;
        public bool _IgnoreRestLM;
        public int _p_duff_ITUOP_count = 0;

        private Inst_Fcu2Asic fcu;
        private NameValueCollection characterizationSetting;
        private NameValueCollection supermodmapSetting;
        private NameValueCollection superMapBoundarySetting;
        private bool _CalFreqWrong = false;
        public double[,] slope_rear_rearsoa;
        public double Target_LockerCurrent_mA;
        public double Max_LockerCurrent_mA;
        public double Min_LockerCurrent_mA;
        public double Max_RearSoa_mA;
        public double Min_RearSoa_mA;
        public bool useDUTEtalon;
        public IInstType_DigitalIO outLine_Tx;
        public IInstType_DigitalIO outLine_Rx;
        public TestParamConfigAccessor programSettings = null;
        public double slope_rearsoa = 1;
        public double constant_rear_rearsoa = 0;
        public double FrequencyTolerance_GHz = 0;
        private double IRearSoa_Fine_mA = 0;
        public List<double> Mz_Imb_Left_mA_SM;
        public List<double> Mz_Imb_Right_mA_SM;

        public List<double> FrontSectionCurrent_mA_List;

        #endregion

        public string ITUEstimateFilePath
        {
            get
            {
                return _ITUEstimateFilePath;
            }
        }

        public string ITUOperatingPointsFilePath
        {
            get { return _ITUOperatingPointsFilePath; }
            //set { _ITUOperatingPointsFilePath = value; }
        }

        public int ITUPointsCount
        {
            get { return _ITUPointsCount; }
            //set { _ITUPointsCount = value; }
        }
        public bool CalFreqWrong
        {
            get { return _CalFreqWrong; }
        }

        public TestParamConfigAccessor ProgramSettings
        {
            set
            {
                programSettings = value;
            }
            get
            {
                return programSettings;
            }
        }

        public string Result_dir
        {
            get { return _result_dir; }
            set { _result_dir = value; }
        }
        public bool IsByAsic = false;

        public CCGOperatingPoints(Inst_Fcu2Asic Fcu, List<CDSDBRSuperMode> supermodes)
        {
            //this.waveMeter_Ag86122 = dev;
            this.fcu = Fcu;
            this._p_supermodes = supermodes;
            this.characterizationSetting = (NameValueCollection)SettingConfigure.cfgReader.GetSection("CloseGridSetting/Characterization");
            this.supermodmapSetting = (NameValueCollection)SettingConfigure.cfgReader.GetSection("CloseGridSetting/SuperMap/SuperModeMap");
            this.superMapBoundarySetting = (NameValueCollection)SettingConfigure.cfgReader.GetSection("CloseGridSetting/SuperMap/BoundaryDetection");
            this.settle_time_ms = int.Parse(characterizationSetting["OperatingPointSettleTime"]);// 500
        }
        /* private CCGOperatingPoints(string abs_filename)
         {
             ITUPointsfilePath = abs_filename;
         }
         * */
        public void setChannelNbr(int freq_nb, int FrequencyStep)
        {
            _itugrid._length = freq_nb;
            _itugrid.freq_step = FrequencyStep;
            _itugrid.setChanNbr = true;
        }
        public void setMaxMinFrequency(double max_f, double min_f)
        {
            _itugrid.max_freq = max_f;
            _itugrid.min_freq = min_f;
            _itugrid.setMaxMinFreq = true;
        }
        public void findITUOperatingPointsUsingWavemeter()
        {
            _itugrid.setupEx(ITUPointsfilePath);
            int total_number_of_lms = 0;
            int completed_lms = 0;
            bool returnValue = true;
            _CalFreqWrong = false;
            for (int sm = 0; sm < (int)(_p_supermodes.Count); sm++)
            {
                CDSDBRSuperMode iterator = _p_supermodes[sm];
                for (int lm = 0;
                    lm < (int)(iterator._lm_filtered_middle_lines.Count); lm++)
                    total_number_of_lms++;
            }
            _coarse_freq_sample_points.Clear(); // clear all _coarse_Freq_data
            _characterisation_itu_ops.Clear();//clear all ITU point
            double minPhaseCurrent = double.Parse(characterizationSetting["MinPhaseCurrent"]);//1.9
            double MaxPhaseCurrent = double.Parse(characterizationSetting["MaxPhaseCurrent"]);//12.8
            for (int sm = 0; sm < (int)(_p_supermodes.Count); sm++)
            {
                CDSDBRSuperMode i_sm = _p_supermodes[sm];

                // for each sm
                _IgnoreRestLM = false;
                slope_rearsoa = slope_rear_rearsoa[sm, 0];
                constant_rear_rearsoa = slope_rear_rearsoa[sm, 1];

                int start_lm;
                int end_lm;
                int step;

                if (sm == 0 || sm == 1 || sm == 2)
                {
                    start_lm = (int)i_sm._lm_filtered_middle_lines.Count - 1;
                    end_lm = -1;
                    step = -1;
                }
                else
                {
                    start_lm = 0;
                    end_lm = (int)i_sm._lm_filtered_middle_lines.Count;
                    step = 1;
                }

                for (int lm = start_lm; lm != end_lm; lm = lm + step)
                {
                    CDSDBRSupermodeMapLine i_lm = i_sm._lm_filtered_middle_lines[lm];
                    if (i_lm._points.Count > int.Parse(characterizationSetting["LongitudeModeLength"]))
                    {
                        int start_index = 0;
                        int end_index = (short)(i_lm._points.Count - 1);

                        double minPhaseDistance = 101;
                        int LastmaxPhaseIndex = -1;
                        for (int i = 0; i < ((int)i_lm._points.Count); i++)
                        {
                            if ((minPhaseCurrent - ((i_lm._points[i]).I_phase) >= 0) && (minPhaseCurrent - i_lm._points[i].I_phase <= minPhaseDistance) && i > LastmaxPhaseIndex)
                            {
                                start_index = i;
                                LastmaxPhaseIndex = i;
                                minPhaseDistance = minPhaseCurrent - i_lm._points[i].I_phase;
                            }
                        }

                        minPhaseDistance = 101;
                        LastmaxPhaseIndex = 102;
                        for (int i = 0; i < ((int)i_lm._points.Count); i++)
                        {
                            if ((i_lm._points[i].I_phase - MaxPhaseCurrent >= 0) && (i_lm._points[i].I_phase - MaxPhaseCurrent <= minPhaseDistance) && i < LastmaxPhaseIndex)
                            {
                                end_index = i;
                                LastmaxPhaseIndex = i;
                                minPhaseDistance = i_lm._points[i].I_phase - MaxPhaseCurrent;
                            }
                        }

                        CDSDBROperatingPoint first_pt_on_middle_freq_line = new CDSDBROperatingPoint(i_sm, i_lm, sm, lm, start_index);
                        CDSDBROperatingPoint last_pt_on_middle_freq_line = new CDSDBROperatingPoint(i_sm, i_lm, sm, lm, end_index);

                        slope_rearsoa = slope_rear_rearsoa[sm, 0];
                        constant_rear_rearsoa = slope_rear_rearsoa[sm, 1];

                        first_pt_on_middle_freq_line.ReCountRearSoa(slope_rearsoa, constant_rear_rearsoa,Max_RearSoa_mA,Min_RearSoa_mA);//Echo new added to recount rearsoa
                        returnValue = first_pt_on_middle_freq_line.Apply(fcu);


                        if (returnValue == false)
                        {
                            throw new Exception("FCU exception");
                        }

                        double frequency_at_first_pt = 0;
                        double min_measured_freq_at_first_pt = 0;
                        double max_measured_freq_at_first_pt = 0;

                        returnValue = measure_F_P_Pr(ref frequency_at_first_pt, ref min_measured_freq_at_first_pt, ref max_measured_freq_at_first_pt,
                               short.Parse(characterizationSetting["NumberOfFrequencyMeasurementsToAveragePerOperatingPoint"]));
                        if (returnValue == false)
                        {
                            throw new Exception("Wavemeter exception");
                        }
                        _coarse_freq_sample_points.Add(new Pair<double, CDSDBROperatingPoint>(frequency_at_first_pt, first_pt_on_middle_freq_line));

                        slope_rearsoa = slope_rear_rearsoa[sm, 0];
                        constant_rear_rearsoa = slope_rear_rearsoa[sm, 1];
                        
                        last_pt_on_middle_freq_line.ReCountRearSoa(slope_rearsoa, constant_rear_rearsoa,Max_RearSoa_mA,Min_RearSoa_mA); //echo new added
                            returnValue = last_pt_on_middle_freq_line.Apply(fcu);
                        if (returnValue == false)
                        {
                            throw new Exception("FCU exception");
                        }

                        double frequency_at_last_pt = 0;
                        double min_measured_freq_at_last_pt = 0;
                        double max_measured_freq_at_last_pt = 0;

                        returnValue = measure_F_P_Pr(ref frequency_at_last_pt, ref min_measured_freq_at_last_pt, ref max_measured_freq_at_last_pt,
                                short.Parse(characterizationSetting["NumberOfFrequencyMeasurementsToAveragePerOperatingPoint"]));
                        if (returnValue == false)
                        {
                            throw new Exception("Wavemeter exception");
                        }
                        _coarse_freq_sample_points.Add(new Pair<double, CDSDBROperatingPoint>(frequency_at_last_pt, last_pt_on_middle_freq_line));

                        newITUPointSearchRoutine(frequency_at_first_pt, min_measured_freq_at_first_pt, max_measured_freq_at_first_pt,
                                                 frequency_at_last_pt, min_measured_freq_at_last_pt, max_measured_freq_at_last_pt,
                                                 i_lm, i_sm, sm, lm, start_index, end_index);
                        if (_IgnoreRestLM)
                            break;
                        completed_lms++;
                        if (OnLongitudinalModeFinished != null)
                        {
                            OnLongitudinalModeFinished(this, new ProgressChangedEventArgs((int)(completed_lms * 1.0 / total_number_of_lms * 100), this));
                        }
                    }//if (i_lm._points.Count >50)                   
                }//for (int lm = start_lm; lm != end_lm; lm = lm + step)               
            }
            findNearbyBoundaryCoords(_characterisation_itu_ops);
            //writeSamplePointsToFile();
            writeOPsToFile();

            WriteFreqCoverageToFile();
        }
        public void findITUOperatingPointsByCalibration_Method()
        {
            // The oftenly use of IsOnline may produce the logging queue full error - chongjian.liang 2013.4.20            
            bool isWaveMeterOnline = false;
			if (Bookham.TestSolution.IlmzCommonUtils.Measurements.Wavemeter != null)
            {
                isWaveMeterOnline = Bookham.TestSolution.IlmzCommonUtils.Measurements.Wavemeter.IsOnline;
            }

            _itugrid.setupEx(ITUPointsfilePath);
            double Target_Freq_GHz, Target_Index, Target_Power_Ratio, Target_Locker_Ratio=0.0, Filter_Etalon_Slope;
            double Current_Power_Ratio, Current_Locker_Ratio, Delt_Power_Ratio;
            double Delt_Locker_Ratio = 0; //for do Freq tuning on  middle line
            int LM_CH_Count = 0, Max_LM_CH_Count = -999, Max_LM_Number = -999;
            int[] SM_Range_Index = new int[_p_supermodes.Count];
            for (int sm = 0; sm < _p_supermodes.Count; sm++)
                Max_LM_Number = Math.Max(Max_LM_Number, _p_supermodes[sm]._lm_filtered_middle_lines.Count);
            int[,] SM_LM_CH_ITU_Count_Sum = new int[_p_supermodes.Count, Max_LM_Number];
            SM_LM_Information[,] SM_LM_Infor = new SM_LM_Information[_p_supermodes.Count, Max_LM_Number];
            Matrix_Point matrix_point = new Matrix_Point();
            bool returnValue = false;
            int ITU_CH_Count = 0;

            double wavemeter_error_ghz = double.Parse(characterizationSetting["FrequencyAccuracyOfWavemeter[+/-GHz]"]);
                   //wavemeter_error_ghz = FrequencyTolerance_GHz;//Is it should defined by product or hW engineer? Jack.zhang 2011-10-27
            int MAX_ITERATIONS_TO_FIND_ITU_PT = int.Parse(characterizationSetting["MAX_ITERATIONS_TO_FIND_ITU_PT"]);
            double Forward_Reverse_Ratio = double.Parse(characterizationSetting["ForwardMatrixDataRatio"]);// 1---Forward:100%&Reverse:0%    0.3---Forward:30%&Reverse:70% 
            double MinPhaseCurrent_mA = double.Parse(characterizationSetting["MinPhaseCurrent"]);//1.9
            double MaxPhaseCurrent_mA = double.Parse(characterizationSetting["MaxPhaseCurrent"]);//12.8
            double MinRearCurrent = double.Parse(characterizationSetting["MinRearCurrent"]);//12.8
            int AverageLMWidth = int.Parse(supermodmapSetting["MidNumberOfPointsWidthOfALongitudinalMode"]);//20
            int filter_n = int.Parse(superMapBoundarySetting["AverageFilterRank"]);
            double Linearty_CorrelationCoefficient = double.Parse(characterizationSetting["LineartyCorrelationCoefficient"]);//0.75
            double Linearty_LastDeltFrequencyGHzJump = double.Parse(characterizationSetting["LineartyLastDeltFrequencyGHzJump"]);//0.75
            double Linearty_PeakTroughMaxNumLMMiddleLine = double.Parse(characterizationSetting["Linearty_PeakTroughMaxNumLMMiddleLine"]);//5
            bool UseWMDoLowCostCheck = programSettings.GetBoolParam("UseWMDoLowCostCheck");

            _coarse_freq_sample_points.Clear();// clear all _coarse_Freq_data
            _characterisation_itu_ops.Clear();//JACK.ZHANG clear all ITU point data
            Raw_Matrix_Data.Clear();

            Matrix_Data TempMatrix_Data;
            CDSDBROperatingPoint pt_on_middle_line;
            CDSDBROperatingPoint pt_on_middle_line_HasBeenGrouped;
            CDSDBROperatingPoint pt_on_middle_line_WillBeGrouped;

            #region check LM if right by linearty in Iphase [MinPhaseCurrent_mA,MaxPhaseCurrent_mA] range
            LinearLeastSquaresFitAlgorithm LM_LinearResult = new LinearLeastSquaresFitAlgorithm();
            for (int sm = 0; sm < (int)(_p_supermodes.Count); sm++)
            {
                CDSDBRSuperMode i_sm_First = _p_supermodes[sm];
                for (int lm = 0;
                    lm < (int)(i_sm_First._lm_filtered_middle_lines.Count); lm++)
                {
                    #region one LM middle line loop to find etalon range
                    CDSDBRSupermodeMapLine i_lm_First = i_sm_First._lm_filtered_middle_lines[lm];
                    int start_index = 0;
                    int end_index = Math.Max(0, (i_lm_First._points.Count - 1));//the Max to avoid the Middle Line be removed and point count is zero. Jack.Zhang 2011-01-07

                    IphaseIndexRangeLM(i_lm_First, MinPhaseCurrent_mA, MaxPhaseCurrent_mA, out start_index, out end_index);//re_cal start and end index to avoid those wrong data on low Iphase jack.zhang 2011-10-18

                    List<double> Phase_Index_Array = new List<double>();
                    List<double> Forward_PowerRatio_Array = new List<double>();
                    List<double> Reverse_PowerRatio_Array = new List<double>();
                    List<double> LM_Width = new List<double>();
                    #endregion one LM middle line loop to find etalon range
                    double Forward_DeltLR = 0;
                    double Reverse_DetlLR = 0;
                    SM_LM_Information tempInfo = new SM_LM_Information();
                    SM_LM_Infor[sm, lm] = tempInfo;
                    if (start_index + end_index == 0)
                    {
                        i_sm_First._lm_filtered_middle_lines[lm].IsMissingLine = true;//the middle line has been removed.not data for it. Jack.zhang 2011-01-07
                        SM_LM_Infor[sm, lm].IsMissingLine = i_sm_First._lm_filtered_middle_lines[lm].IsMissingLine;
                    }
                    else
                    {
                        #region check the middle line if right by lineary fitting
                        List<double> Forward_DeltLR_List = new List<double>();
                        List<double> Reverse_DetlLR_List = new List<double>();
                        for (int Point_index = start_index; Point_index <= end_index; Point_index++)
                        {
                            pt_on_middle_line = new CDSDBROperatingPoint(i_sm_First, i_lm_First, sm, lm, Point_index);
                            findNearbyBoundaryCoords(pt_on_middle_line);
                            LM_Width.Add((pt_on_middle_line._row_of_boundary_above - pt_on_middle_line._row_of_boundary_below));

                            Forward_PowerRatio_Array.Add(i_lm_First._points[Point_index].Forward_PowerRatio);
                            Reverse_PowerRatio_Array.Add(i_lm_First._points[Point_index].Reverse_PowerRatio);
                            Phase_Index_Array.Add(i_lm_First._points[Point_index].real_col);
                            #region get the Max delt LR in middle line to double check the middle line is right due to PR can not cover all missing line case
                            if (Point_index < end_index)
                            {
                                double Temp_ABS_For_DeltLR = Math.Abs(i_lm_First._points[Point_index].Forward_LockerRatio - i_lm_First._points[Point_index + 1].Forward_LockerRatio);
                                double Temp_ABS_Rev_DeltLR = Math.Abs(i_lm_First._points[Point_index].Reverse_LockerRatio - i_lm_First._points[Point_index + 1].Reverse_LockerRatio);
                                Forward_DeltLR_List.Add(Temp_ABS_For_DeltLR);
                                Reverse_DetlLR_List.Add(Temp_ABS_Rev_DeltLR);
                                Forward_DeltLR = Math.Max(Forward_DeltLR, Temp_ABS_For_DeltLR);
                                Reverse_DetlLR = Math.Max(Reverse_DetlLR, Temp_ABS_Rev_DeltLR);
                            }
                            #endregion get the Max delt LR in middle line to double check the middle line is right due to PR can not cover all missing line case
                        }
                        #region cal LM width by boundary point

                        //SM_LM_Information tempInfo = new SM_LM_Information();
                        tempInfo.LMWidth = (int)FreqCalByEtalon.Median(LM_Width);
                        SM_LM_Infor[sm, lm] = tempInfo;

                        #endregion cal LM width by boundary point
                        if (Phase_Index_Array.Count - 1 > 0)
                        {
                            LinearLeastSquaresFit Forward_PowerRatio_linLeastSqFit =
                                LinearLeastSquaresFitAlgorithm.Calculate(Phase_Index_Array.ToArray(), Forward_PowerRatio_Array.ToArray(), 0, Forward_PowerRatio_Array.Count - 1);

                            LinearLeastSquaresFit Reverse_PowerRatio_linLeastSqFit =
                                LinearLeastSquaresFitAlgorithm.Calculate(Phase_Index_Array.ToArray(), Reverse_PowerRatio_Array.ToArray(), 0, Reverse_PowerRatio_Array.Count - 1);
                            Forward_DeltLR_List.Sort();
                            Reverse_DetlLR_List.Sort();
                            if (Forward_PowerRatio_linLeastSqFit.CorrelationCoefficient < Linearty_CorrelationCoefficient
                             || Reverse_PowerRatio_linLeastSqFit.CorrelationCoefficient < Linearty_CorrelationCoefficient
                             || ((Forward_DeltLR > Linearty_LastDeltFrequencyGHzJump * FreqCalByEtalon.Average_Locker_Ratio_SlopeToGHz
                                && Reverse_DetlLR > Linearty_LastDeltFrequencyGHzJump * FreqCalByEtalon.Average_Locker_Ratio_SlopeToGHz)
                                && (Forward_DeltLR_List[Forward_DeltLR_List.Count-2] > Linearty_LastDeltFrequencyGHzJump * FreqCalByEtalon.Average_Locker_Ratio_SlopeToGHz
                                && Reverse_DetlLR_List[Reverse_DetlLR_List.Count - 2] > Linearty_LastDeltFrequencyGHzJump * FreqCalByEtalon.Average_Locker_Ratio_SlopeToGHz)))
                                //to just remove one locker ratio range which on front pair switch we permit to pass the middle line which just has one large delt locker ratio. jack.zhang 2012-01-26
                                i_sm_First._lm_filtered_middle_lines[lm].IsMissingLine = true;
                            else
                                i_sm_First._lm_filtered_middle_lines[lm].IsMissingLine = false;
                        }
                        else
                            i_sm_First._lm_filtered_middle_lines[lm].IsMissingLine = true;

                        SM_LM_Infor[sm, lm].IsMissingLine = i_sm_First._lm_filtered_middle_lines[lm].IsMissingLine;
                        #endregion check the middle line if right by lineary fitting
                    }
                }
            }
            #endregion check LM if right by linearty in Iphase 1.9~12.8mA range

            #region identify each etalon range in all SM by Locker Ratio base on the Max&Min ITU(50&100GHz) Locker Ratio from The Wavemeter Calibration data
            for (int sm = 0; sm < (int)(_p_supermodes.Count); sm++)
            {
                CDSDBRSuperMode i_sm_First = _p_supermodes[sm];
                for (int lm = 0;
                    lm < (int)(i_sm_First._lm_filtered_middle_lines.Count); lm++)
                {
                    if (!i_sm_First._lm_filtered_middle_lines[lm].IsMissingLine)
                    {
                        #region one LM middle line loop to find etalon range
                        CDSDBRSupermodeMapLine i_lm_First = i_sm_First._lm_filtered_middle_lines[lm];
                        int start_index = 0;
                        int end_index = (short)(i_lm_First._points.Count - 1);
                        double Current_First_Power_Ratio = 0, Current_Second_Power_Ratio = 0;
                        //use the frist(start) and second(end) power ratio lable all the ranges in the LM
                        matrix_point = AverageMatrixPoint(i_lm_First, start_index, filter_n, Forward_Reverse_Ratio);
                        Current_First_Power_Ratio = matrix_point.Average_Power_Ratio;

                        matrix_point = AverageMatrixPoint(i_lm_First, end_index, filter_n, Forward_Reverse_Ratio);
                        Current_Second_Power_Ratio = matrix_point.Average_Power_Ratio;

                        //IphaseIndexRangeLM(i_lm_First, MinPhaseCurrent_mA, MaxPhaseCurrent_mA, out start_index, out end_index);
                        start_index = 0;
                        end_index = (int)i_lm_First._points.Count - 1;//jack.zhang add for temp disable up screen in order to get all the range to check

                        LM_CH_Count = 0;
                        TempMatrix_Data = new Matrix_Data();
                        List<double> Media_PR = new List<double>();
                        List<double> LM_Width = new List<double>();
                        List<double> Media_LR = new List<double>();
                        List<int> LR_Range_Index = new List<int>();
                        List<int> LR_Range_exclude = new List<int>();

                        double Max_Locker_Ratio = Math.Max(FreqCalByEtalon.Max_Locker_Ratio_50GHz, FreqCalByEtalon.Max_Locker_Ratio_100GHz);
                        double Min_Locker_Ratio = Math.Min(FreqCalByEtalon.Min_Locker_Ratio_50GHz, FreqCalByEtalon.Min_Locker_Ratio_100GHz);
                        Current_Locker_Ratio = 0;

                        for (int Point_index = start_index; Point_index <= end_index; Point_index++)
                        {
                            matrix_point = AverageMatrixPoint(i_lm_First, Point_index, filter_n, Forward_Reverse_Ratio);
                            Media_LR.Add(matrix_point.Average_Locker_Ratio);
                        }

                        int[] PeakIndex = Alg_FindFeature.FindIndexesForAllPeaks(Media_LR.ToArray());
                        int[] TroughIndex = Alg_FindFeature.FindIndexesForAllValleys(Media_LR.ToArray());

                        if ((PeakIndex.Length + TroughIndex.Length) > Linearty_PeakTroughMaxNumLMMiddleLine)//Temp to check if the middle line is right ML by check the peak and trough sum
                        {
                            i_sm_First._lm_filtered_middle_lines[lm].IsMissingLine = true;
                            SM_LM_Infor[sm, lm].IsMissingLine = i_sm_First._lm_filtered_middle_lines[lm].IsMissingLine;
                        }
                        else
                        {
                            #region find all peak and trough point index from LR in each LM Middle
                            foreach (int i in PeakIndex)
                            {
                                LR_Range_Index.Add(i + start_index);
                            }
                            foreach (int i in TroughIndex)
                            {
                                LR_Range_Index.Add(i + start_index);
                            }
                            LR_Range_Index.Sort();
                            int Temp_Index_delt = 0;
                            int Whole_Range_Count = 0;
                            //suppose the min PTE=0.02 mA/GHz.So we can get 50GHz change from 0mA to 1mA Iphase.
                            //1mA Iphase equal to 15 in Iphase_Index. Jack.zhang 2011-06-08
                            //the value can be modify due to device performance change
                            int Min_Index_delt = 15;
                            int WholeRangeMiddleIndex = 0;
                            double WholeRangeMiddleLocker_Ratio = 0;
                            if (LR_Range_Index.Count > 1)
                            {
                                for (int i = 1; i < LR_Range_Index.Count; i++)
                                {
                                    Temp_Index_delt = LR_Range_Index[i] - LR_Range_Index[i - 1];
                                    if (Temp_Index_delt < Min_Index_delt)
                                    {
                                        LR_Range_exclude.Add(LR_Range_Index[i] + LR_Range_Index[i - 1]);
                                    }
                                    else
                                    {
                                        Whole_Range_Count++;
                                        WholeRangeMiddleIndex = LR_Range_Index[i - 1] + (int)Math.Round((LR_Range_Index[i] - LR_Range_Index[i - 1]) / 2.0, 0);
                                        matrix_point = AverageMatrixPoint(i_lm_First, WholeRangeMiddleIndex, filter_n, Forward_Reverse_Ratio);
                                        WholeRangeMiddleLocker_Ratio += matrix_point.Average_Locker_Ratio;
                                    }
                                }
                            }
                            if (LR_Range_Index.Count > 0)//to avoid no peak or trough in the LM middle (ie.LM0, last LM)
                            {
                                if (LR_Range_Index[LR_Range_Index.Count - 1] != end_index)
                                    LR_Range_Index.Add(end_index);
                                if (LR_Range_Index[0] != start_index)
                                    LR_Range_Index.Add(start_index);
                            }
                            else
                            {
                                LR_Range_Index.Add(end_index);
                                LR_Range_Index.Add(start_index);
                            }

                            LR_Range_Index.Sort();

                            if (Whole_Range_Count == 0)//for no one whole peak and trough range in the LM 
                            {
                                for (int i = 1; i < LR_Range_Index.Count; i++)
                                {
                                    Temp_Index_delt = LR_Range_Index[i] - LR_Range_Index[i - 1];
                                    Whole_Range_Count++;
                                    WholeRangeMiddleIndex = LR_Range_Index[i - 1] + (int)Math.Round((LR_Range_Index[i] - LR_Range_Index[i - 1]) / 2.0, 0);
                                    matrix_point = AverageMatrixPoint(i_lm_First, WholeRangeMiddleIndex, filter_n, Forward_Reverse_Ratio);
                                    WholeRangeMiddleLocker_Ratio += matrix_point.Average_Locker_Ratio;
                                }
                            }
                            //get the mid_range_lockerratio as reference to check other range
                            //matrix_point = AverageMatrixPoint(i_lm_First, WholeRangeMiddleIndex, filter_n, Forward_Reverse_Ratio);
                            WholeRangeMiddleLocker_Ratio = WholeRangeMiddleLocker_Ratio / Whole_Range_Count;
                            WholeRangeMiddleLocker_Ratio = Math.Min(WholeRangeMiddleLocker_Ratio, Max_Locker_Ratio);
                            WholeRangeMiddleLocker_Ratio = Math.Max(WholeRangeMiddleLocker_Ratio, Min_Locker_Ratio);

                            #endregion find all peak and trough point index from LR in each LM Middle
                            int Locker_Range_exclude_Sum = 0;
                            bool Is_Good_Range = true;
                            for (int Index = 0; Index < LR_Range_Index.Count - 1; Index++)
                            {
                                Is_Good_Range = true;
                                Locker_Range_exclude_Sum = LR_Range_Index[Index] + LR_Range_Index[Index + 1];
                                for (int i = 0; i < LR_Range_exclude.Count; i++)
                                {
                                    if (Locker_Range_exclude_Sum == LR_Range_exclude[i])
                                        Is_Good_Range = false;
                                }
                                if (Is_Good_Range)
                                {
                                    int Point_index = LR_Range_Index[Index];
                                    matrix_point = AverageMatrixPoint(i_lm_First, Point_index, filter_n, Forward_Reverse_Ratio);
                                    Current_Locker_Ratio = matrix_point.Average_Locker_Ratio;

                                    TempMatrix_Data.SM = sm;
                                    TempMatrix_Data.LM = lm;
                                    TempMatrix_Data.first_Power_Ratio = Current_First_Power_Ratio;
                                    TempMatrix_Data.second_Power_Ratio = Current_Second_Power_Ratio;
                                    TempMatrix_Data.first_Ip_Index = Point_index;
                                    //use it to cal slope when find the end of range
                                    //the unit is (delt LR/delt Iphase_index)
                                    TempMatrix_Data.Locker_Ratio_Slope = Current_Locker_Ratio;

                                    Point_index = LR_Range_Index[Index + 1];
                                    matrix_point = AverageMatrixPoint(i_lm_First, Point_index, filter_n, Forward_Reverse_Ratio);
                                    Current_Locker_Ratio = matrix_point.Average_Locker_Ratio;

                                    if ((TempMatrix_Data.Locker_Ratio_Slope - WholeRangeMiddleLocker_Ratio)
                                         * (Current_Locker_Ratio - WholeRangeMiddleLocker_Ratio) <= 0)//the range is effictive
                                    {
                                        LM_CH_Count++;
                                        Max_LM_CH_Count = Math.Max(Max_LM_CH_Count, LM_CH_Count);
                                        TempMatrix_Data.LM_CH_Index = LM_CH_Count;
                                        TempMatrix_Data.second_Ip_Index = Point_index;
                                        TempMatrix_Data.Locker_Ratio_Slope = (Current_Locker_Ratio - TempMatrix_Data.Locker_Ratio_Slope) / (TempMatrix_Data.second_Ip_Index - TempMatrix_Data.first_Ip_Index);

                                        double Max_Locker_Ratio_delt = 999;
                                        Media_PR.Clear();

                                        #region Find the most possible ITU Point for Group identify due to the close to WholeRangeMiddleLocker_Ratio
                                        for (int ITU_Index = TempMatrix_Data.first_Ip_Index; ITU_Index <= TempMatrix_Data.second_Ip_Index; ITU_Index++)
                                        {
                                            matrix_point = AverageMatrixPoint(i_lm_First, ITU_Index, filter_n, Forward_Reverse_Ratio);
                                            Current_Locker_Ratio = matrix_point.Average_Locker_Ratio;
                                            Current_Power_Ratio = matrix_point.Average_Power_Ratio;
                                            //the closest point to WholeRangeMiddleLocker_Ratio is the most possible ITU point
                                            double MiddleLocker_Ratio = (FreqCalByEtalon.Max_Locker_Ratio_50GHz + FreqCalByEtalon.Min_Locker_Ratio_50GHz) / 2;
                                            if (TempMatrix_Data.Locker_Ratio_Slope < 0)
                                                MiddleLocker_Ratio = (FreqCalByEtalon.Max_Locker_Ratio_100GHz + FreqCalByEtalon.Min_Locker_Ratio_100GHz) / 2;
                                            //if (Math.Abs(Current_Locker_Ratio - WholeRangeMiddleLocker_Ratio) < Max_Locker_Ratio_delt)
                                            //{
                                            //    Max_Locker_Ratio_delt = Math.Abs(Current_Locker_Ratio - WholeRangeMiddleLocker_Ratio);
                                            if (Math.Abs(Current_Locker_Ratio - MiddleLocker_Ratio) < Max_Locker_Ratio_delt)
                                            {
                                                Max_Locker_Ratio_delt = Math.Abs(Current_Locker_Ratio - MiddleLocker_Ratio);
                                                TempMatrix_Data.ITU_Group_Index = ITU_Index;
                                                TempMatrix_Data.ITU_Ip_Index = i_lm_First._points[ITU_Index].real_col;
                                                TempMatrix_Data.ITU_Im_Index = i_lm_First._points[ITU_Index].real_row;
                                            }
                                            Media_PR.Add(Current_Power_Ratio);//use to define the CH_freq 
                                        }
                                        #endregion Find the most possible ITU Point for Group identify due to the close to WholeRangeMiddleLocker_Ratio
                                        if (Media_PR.Count > 0)
                                        {
                                            TempMatrix_Data.Media_Power_Ratio = FreqCalByEtalon.Median(Media_PR);

                                            #region get Iphase current to compare to avoid wrong Index in incomplicate LM
                                            pt_on_middle_line = new CDSDBROperatingPoint(i_sm_First, i_lm_First, sm, lm, TempMatrix_Data.first_Ip_Index);
                                            TempMatrix_Data.first_Ip_current_mA = pt_on_middle_line._I_phase;
                                            pt_on_middle_line = new CDSDBROperatingPoint(i_sm_First, i_lm_First, sm, lm, TempMatrix_Data.second_Ip_Index);
                                            TempMatrix_Data.second_Ip_current_mA = pt_on_middle_line._I_phase;
                                            pt_on_middle_line = new CDSDBROperatingPoint(i_sm_First, i_lm_First, sm, lm, TempMatrix_Data.ITU_Group_Index);
                                            TempMatrix_Data.ITU_Ip_current_mA = pt_on_middle_line._I_phase;
                                            #endregion get Iphase current to compare to avoid wrong Index in incomplicate LM

                                            #region recheck the LM With and record below and upper LM width for missing line modify in further

                                            TempMatrix_Data.LM_Width = SM_LM_Infor[sm, lm].LMWidth;
                                            if(SM_LM_Infor[sm, Math.Max(0, lm - 1)] == null)
                                            TempMatrix_Data.Below_LM_Width =0;
                                            else
                                            TempMatrix_Data.Below_LM_Width = SM_LM_Infor[sm, Math.Max(0, lm - 1)].LMWidth;

                                            if (TempMatrix_Data.LM_Width <= AverageLMWidth)
                                            {
                                                Raw_Matrix_Data.Add(TempMatrix_Data);
                                                SM_LM_Infor[TempMatrix_Data.SM, TempMatrix_Data.LM].ItuCHCount++;
                                            }
                                            #endregion recheck the LM With and record below and upper LM width for missing line modify in further
                                        }
                                        TempMatrix_Data = new Matrix_Data();
                                    }
                                }
                            }
                            if (LR_Range_exclude.Count > 0)
                                SM_LM_Infor[sm, lm].ItuCHCount = 0;//Do not use the LM to do group because some range missing in the LM (for ex. FP switch) Jack.zhang 2012-01-20
                        }
                        #endregion one LM middle line loop to find etalon range
                    }
                }
                SM_Range_Index[sm] = Raw_Matrix_Data.Count - 1;
            }
            #endregion identify each etalon range in all SM by Locker Ratio base on the Max&Min ITU(50&100GHz) Locker Ratio from The Wavemeter Calibration data

            #region ITU lable each Etalon range by Power ratio base on Locker Ratio slope and ITU+/-75GHz Power ratio from The Wavemeter Calibration data

            int CH_offset = (int)(FreqCalByEtalon.Filter_Etalon_Data[FreqCalByEtalon.Filter_Etalon_Data.Count - 1][FreqCalByEtalon.Freq_GHz_List_Index] - _itugrid.max_freq) / _itugrid.freq_step;
            CH_offset = Math.Min(CH_offset, (int)(_itugrid.min_freq - FreqCalByEtalon.Filter_Etalon_Data[0][FreqCalByEtalon.Freq_GHz_List_Index]) / _itugrid.freq_step);
            for (int N = 0; N < Raw_Matrix_Data.Count; N++)
            {
                for (int ITU_CH_Num = -1 * CH_offset; ITU_CH_Num < _itugrid.channel_count() + CH_offset; ITU_CH_Num++)
                {
                    Target_Freq_GHz = _itugrid.min_freq + ITU_CH_Num * _itugrid.freq_step;
                    double First_Target_Power_Ratio, Second_Target_Power_Ratio;
                    bool In_Cal_Range;
                    FreqCalByEtalon.CalPRatio_LRatio_byFreq_GHz(Target_Freq_GHz, out Target_Power_Ratio, out Filter_Etalon_Slope,
                                                                out Target_Locker_Ratio, out First_Target_Power_Ratio,
                                                                out Second_Target_Power_Ratio, out In_Cal_Range);
                    #region lable the range if the Matrix_power ratio in the cal range
                    if (In_Cal_Range)
                    {
                        Delt_Power_Ratio = 0;
                        if ((First_Target_Power_Ratio - Raw_Matrix_Data[N].Media_Power_Ratio) * (Second_Target_Power_Ratio - Raw_Matrix_Data[N].Media_Power_Ratio) <= 0)
                        {
                            Delt_Power_Ratio = Math.Abs(Target_Power_Ratio - Raw_Matrix_Data[N].Media_Power_Ratio);
                            if (FreqCalByEtalon.UseDUTLockerRatio)
                            {
                                //DUT lockerRatio(Rx/Tx) slope is negtive for 100GHz. jack.zhang 2012-11-15
                                //some time Etalon box Lockerratioslope can not keep negtive for 100GHz but can be work.
                                //So for dut we need re_set it.
                                Filter_Etalon_Slope = Math.Abs(Filter_Etalon_Slope);
                                if((int)Target_Freq_GHz % 100 == 0)
                                    Filter_Etalon_Slope = -1 * Filter_Etalon_Slope;
                            }
                            if (Math.Sign(Filter_Etalon_Slope) == Math.Sign(Raw_Matrix_Data[N].Locker_Ratio_Slope)
                                && Delt_Power_Ratio < Math.Abs(Raw_Matrix_Data[N].Media_Power_Ratio - Raw_Matrix_Data[N].Target_Power_Ratio))
                            {
                                Raw_Matrix_Data[N].CH_Freq_GHz = (int)Target_Freq_GHz;
                                Raw_Matrix_Data[N].CH_Number = ITU_CH_Num;
                                Raw_Matrix_Data[N].CH_Number_Modify = ITU_CH_Num;
                                Raw_Matrix_Data[N].CH_Freq_GHz_Modify = (int)Target_Freq_GHz;
                                Raw_Matrix_Data[N].Target_Locker_Ratio = Target_Locker_Ratio;
                                Raw_Matrix_Data[N].Target_Locker_Ratio_Slope = Filter_Etalon_Slope;
                                if (FreqCalByEtalon.UseDUTLockerRatio)
                                {
                                    Raw_Matrix_Data[N].Target_Locker_Ratio = (FreqCalByEtalon.Max_Locker_Ratio_50GHz + FreqCalByEtalon.Min_Locker_Ratio_50GHz) / 2.0;
                                    if ((int)Target_Freq_GHz % 100 == 0)//DUT lockerRatio(Rx/Tx) slope is negtive for 100GHz. jack.zhang 2012-11-15
                                        Raw_Matrix_Data[N].Target_Locker_Ratio = (FreqCalByEtalon.Max_Locker_Ratio_100GHz + FreqCalByEtalon.Min_Locker_Ratio_100GHz) / 2.0;
                                }
                                if (Raw_Matrix_Data[N].CH_Freq_GHz >= _itugrid._ITU_frequencies[0] - 100 && Raw_Matrix_Data[N].CH_Freq_GHz <= _itugrid._ITU_frequencies[_itugrid.channel_count() - 1] + 100)
                                    Raw_Matrix_Data[N].IS_ITU_CH = true;
                                Raw_Matrix_Data[N].Target_Power_Ratio = Target_Power_Ratio;
                                Raw_Matrix_Data[N].first_Target_Power_Ratio = First_Target_Power_Ratio;
                                Raw_Matrix_Data[N].second_Target_Power_Ratio = Second_Target_Power_Ratio;

                            }
                        }
                    }
                    #endregion lable the range if the Matrix_power ratio in the cal range
                }
            }
            #endregion ITU lable each Etalon range identify each Etalon range by Power ratio base on Locker Ratio slope and ITU+/-75GHz Power ratio from The Wavemeter Calibration data

            #region identify each etalon range to group by ITU_Iphase_mA
            int Range_Start = 0;
            int Max_CH_LM = 0;
            List<int> Max_LM_CH = new List<int>();
            List<int> HasBeenGrouped_LM_CH = new List<int>();
            List<int> WillBeGrouped_LM_CH = new List<int>();
            List<double> LM_Width_each_LM = new List<double>();
            int WillBeGrouped_LM = 0;
            int HasBeenGrouped_LM = 0;
            int Delt_Will_Has_Point = 0;
            int LM_gap = 0;

            for (int sm = 0; sm < _p_supermodes.Count; sm++)
            {
                if (SM_Range_Index[sm] > 0)
                {
                    int ii = 0;
                    do
                    {
                        ii++;
                        if (sm - ii < 0)
                        {
                            Range_Start = 0;
                            break;
                        }
                        Range_Start = SM_Range_Index[sm - ii] + 1;
                    } while (SM_Range_Index[sm - ii] == 0);//JACK.ZHANG TO AVOID THE SM WITHOUT ANYONE RANGE


                    #region cal LM gap and check it with LM width if LM missing
                    CDSDBRSuperMode i_sm_First = _p_supermodes[sm];
                    for (int _lm = Raw_Matrix_Data[Range_Start].LM; _lm < Raw_Matrix_Data[SM_Range_Index[sm]].LM; _lm++)
                    {
                        LM_gap = 0;
                        if (!SM_LM_Infor[sm, _lm].IsMissingLine)
                        {
                            HasBeenGrouped_LM = _lm;
                            WillBeGrouped_LM = _lm + 1;
                            while (SM_LM_Infor[sm, WillBeGrouped_LM].IsMissingLine)
                            {
                                if (WillBeGrouped_LM == Raw_Matrix_Data[SM_Range_Index[sm]].LM)
                                    break;
                                WillBeGrouped_LM++;
                            }
                            if (SM_LM_Infor[sm, WillBeGrouped_LM].IsMissingLine && WillBeGrouped_LM == Raw_Matrix_Data[SM_Range_Index[sm]].LM)
                                LM_gap = 0;
                            else
                            {

                                CDSDBRSupermodeMapLine i_lm_HasBeenGrouped = i_sm_First._lm_filtered_middle_lines[HasBeenGrouped_LM];
                                CDSDBRSupermodeMapLine i_lm_WillBeGrouped = i_sm_First._lm_filtered_middle_lines[WillBeGrouped_LM];
                                if (i_lm_HasBeenGrouped._points.Count < i_lm_WillBeGrouped._points.Count)
                                {
                                    int Temp_LM = HasBeenGrouped_LM;
                                    HasBeenGrouped_LM = WillBeGrouped_LM;
                                    WillBeGrouped_LM = Temp_LM;
                                    i_lm_HasBeenGrouped = i_sm_First._lm_filtered_middle_lines[HasBeenGrouped_LM];
                                    i_lm_WillBeGrouped = i_sm_First._lm_filtered_middle_lines[WillBeGrouped_LM];
                                }

                        int Start_Iphase_Index = 0;
                        int End_Iphase_Index = i_lm_WillBeGrouped._points.Count - 1;
                        IphaseIndexRangeLM(i_lm_WillBeGrouped, MinPhaseCurrent_mA, MaxPhaseCurrent_mA, out Start_Iphase_Index, out End_Iphase_Index);//re_cal start and end index to avoid those wrong data on low Iphase jack.zhang 2011-10-18

                        if (HasBeenGrouped_LM > WillBeGrouped_LM)
                            Delt_Will_Has_Point = i_lm_HasBeenGrouped._points.Count - i_lm_WillBeGrouped._points.Count;
                        else
                            Delt_Will_Has_Point = 0;

                                LM_Width_each_LM.Clear();
                                for (int i = Start_Iphase_Index; i <= End_Iphase_Index; i++)
                                {
                                    i_lm_HasBeenGrouped = i_sm_First._lm_filtered_middle_lines[HasBeenGrouped_LM];
                                    i_lm_WillBeGrouped = i_sm_First._lm_filtered_middle_lines[WillBeGrouped_LM];
                                    pt_on_middle_line_HasBeenGrouped = new CDSDBROperatingPoint(i_sm_First, i_lm_HasBeenGrouped, sm, HasBeenGrouped_LM, i + Delt_Will_Has_Point);
                                    pt_on_middle_line_WillBeGrouped = new CDSDBROperatingPoint(i_sm_First, i_lm_WillBeGrouped, sm, WillBeGrouped_LM, i);
                                    findNearbyBoundaryCoords(pt_on_middle_line_HasBeenGrouped);
                                    findNearbyBoundaryCoords(pt_on_middle_line_WillBeGrouped);
                                    double LM_Width = pt_on_middle_line_WillBeGrouped._real_row_on_sm_map - pt_on_middle_line_HasBeenGrouped._real_row_on_sm_map;
                                    //remove the hysteresis resion . jack.zhang 2011-10-20
                                    double Hysteresis_Width = 0;
                                    int Sub_HasBeenGrouped_LM = 0;
                                    if (Math.Abs(HasBeenGrouped_LM - WillBeGrouped_LM) == 1)//to fix the LM-gap error case when LM missing. Jack.zhang 2013-07-18
                                    {
                                        for (int sub_lm = Math.Min(HasBeenGrouped_LM, WillBeGrouped_LM); sub_lm < Math.Max(HasBeenGrouped_LM, WillBeGrouped_LM); sub_lm++)
                                        {
                                            i_lm_WillBeGrouped = i_sm_First._lm_filtered_middle_lines[sub_lm];
                                            Sub_HasBeenGrouped_LM = sub_lm + 1;
                                            i_lm_HasBeenGrouped = i_sm_First._lm_filtered_middle_lines[Sub_HasBeenGrouped_LM];
                                            while (i_lm_HasBeenGrouped._points.Count == 0)
                                            {
                                                if (Sub_HasBeenGrouped_LM == Math.Max(HasBeenGrouped_LM, WillBeGrouped_LM))
                                                    break;
                                                Sub_HasBeenGrouped_LM++;
                                                i_lm_HasBeenGrouped = i_sm_First._lm_filtered_middle_lines[Sub_HasBeenGrouped_LM];
                                            }

                                            if (i_lm_HasBeenGrouped._points.Count > 0 && Sub_HasBeenGrouped_LM <= Math.Max(HasBeenGrouped_LM, WillBeGrouped_LM))
                                            {
                                                int Sub_Delt_Will_Has_Point = Math.Max(0, i_lm_HasBeenGrouped._points.Count - i_lm_WillBeGrouped._points.Count);
                                                pt_on_middle_line_HasBeenGrouped = new CDSDBROperatingPoint(i_sm_First, i_lm_HasBeenGrouped, sm, Sub_HasBeenGrouped_LM, i + Sub_Delt_Will_Has_Point);
                                                pt_on_middle_line_WillBeGrouped = new CDSDBROperatingPoint(i_sm_First, i_lm_WillBeGrouped, sm, sub_lm, i);
                                                findNearbyBoundaryCoords(pt_on_middle_line_HasBeenGrouped);
                                                findNearbyBoundaryCoords(pt_on_middle_line_WillBeGrouped);
                                                Hysteresis_Width += (pt_on_middle_line_HasBeenGrouped._row_of_boundary_below - pt_on_middle_line_WillBeGrouped._row_of_boundary_above);
                                            }
                                            sub_lm = Sub_HasBeenGrouped_LM - 1;
                                        }
                                    }
                                    LM_Width = Math.Abs(LM_Width) - Hysteresis_Width;
                                    LM_Width_each_LM.Add(LM_Width);
                                }

                                LM_gap= (int)Math.Round(FreqCalByEtalon.Median(LM_Width_each_LM)
                                    //to avoid the dubole LM width impaction for missing line.jack.zhang 2011-10-20
                                          / ((SM_LM_Infor[sm, HasBeenGrouped_LM].LMWidth + SM_LM_Infor[sm, WillBeGrouped_LM].LMWidth) / 2.0));
                            }
                        }
                        SM_LM_Infor[sm, _lm].BelowLMGap = LM_gap;

                    }
                    #endregion cal LM gap and check it with LM width if LM missing

                    #region find which LM count Max CH Num
                    Max_LM_CH_Count = 0;
                    Max_LM_CH = new List<int>();
                    double Group_Delt_Iphase_mA = 0;

                    for (int LM = 0; LM <= Raw_Matrix_Data[SM_Range_Index[sm]].LM; LM++)
                    {
                        if (SM_LM_Infor[sm, LM].ItuCHCount >= Max_LM_CH_Count)
                        {
                            Max_LM_CH_Count = SM_LM_Infor[sm, LM].ItuCHCount;
                            Max_CH_LM = LM;
                        }
                    }

                    for (int N = Range_Start; N <= SM_Range_Index[sm]; N++)
                    {
                        if (Raw_Matrix_Data[N].LM == Max_CH_LM)
                        {
                            Max_LM_CH.Add(N);
                            Raw_Matrix_Data[N].Range_Group = Max_LM_CH.Count;
                        }
                    }
                    #endregion find which LM count Max CH Num

                    #region start group from the LM to the SM begin
                    HasBeenGrouped_LM_CH.Clear();
                    foreach (int N in Max_LM_CH)
                    {
                        HasBeenGrouped_LM_CH.Add(N);
                    }
                    HasBeenGrouped_LM = Max_CH_LM;
                    for (int LM = Max_CH_LM - 1; LM >= 0; LM--)
                    {
                        #region find the Range in LM-1
                        WillBeGrouped_LM_CH.Clear();//put the Range which will be grouped
                        WillBeGrouped_LM = LM;
                        for (int N = Range_Start; N <= SM_Range_Index[sm]; N++)
                        {
                            if (Raw_Matrix_Data[N].LM == WillBeGrouped_LM)
                                WillBeGrouped_LM_CH.Add(N);
                        }
                        #endregion find the Range in LM-1
                        if (WillBeGrouped_LM_CH.Count > 0)
                        {
                            #region identify each Range in Source_LM_CH by compare with all in Ref_LM_CH
                            LM_gap = 0;
                            for (int _lm = Math.Min(HasBeenGrouped_LM, WillBeGrouped_LM); _lm <= Math.Max(HasBeenGrouped_LM, WillBeGrouped_LM) - 1; _lm++)
                            {
                                LM_gap += SM_LM_Infor[sm, _lm].BelowLMGap * Math.Sign(WillBeGrouped_LM - HasBeenGrouped_LM);
                            }
                            for (int N = 0; N < WillBeGrouped_LM_CH.Count; N++)
                            {
                                Raw_Matrix_Data[WillBeGrouped_LM_CH[N]].Range_Group = -999;
                                for (int M = 0; M < HasBeenGrouped_LM_CH.Count; M++)
                                {
                                    bool same_slope = true;
                                    if (Math.Sign(Raw_Matrix_Data[WillBeGrouped_LM_CH[N]].Locker_Ratio_Slope) != Math.Sign(Math.Sign(Raw_Matrix_Data[HasBeenGrouped_LM_CH[M]].Locker_Ratio_Slope)))
                                        same_slope = false;
                                    bool LM_gap_even = false;
                                    if (LM_gap % 2 == 0)
                                        LM_gap_even = true;

                                    Group_Delt_Iphase_mA = Raw_Matrix_Data[WillBeGrouped_LM_CH[N]].ITU_Ip_current_mA - Raw_Matrix_Data[HasBeenGrouped_LM_CH[M]].ITU_Ip_current_mA;
                                    if (Math.Abs(LM_gap) > 1)
                                    {
                                        if ((Math.Abs(Raw_Matrix_Data[WillBeGrouped_LM_CH[N]].ITU_Group_deltIp_current_mA) >= Math.Abs(Group_Delt_Iphase_mA))
                                            && (same_slope == LM_gap_even)
                                            && Group_Delt_Iphase_mA >= 0)
                                        {
                                            Raw_Matrix_Data[WillBeGrouped_LM_CH[N]].ITU_Group_deltIp_current_mA = Group_Delt_Iphase_mA;
                                            Raw_Matrix_Data[WillBeGrouped_LM_CH[N]].Range_Group = Raw_Matrix_Data[HasBeenGrouped_LM_CH[M]].Range_Group;
                                        }
                                    }
                                    else
                                    {
                                        if ((Math.Abs(Raw_Matrix_Data[WillBeGrouped_LM_CH[N]].ITU_Group_deltIp_current_mA) >= Math.Abs(Group_Delt_Iphase_mA))
                                            && (same_slope == LM_gap_even))
                                        {
                                            Raw_Matrix_Data[WillBeGrouped_LM_CH[N]].ITU_Group_deltIp_current_mA = Group_Delt_Iphase_mA;
                                            Raw_Matrix_Data[WillBeGrouped_LM_CH[N]].Range_Group = Raw_Matrix_Data[HasBeenGrouped_LM_CH[M]].Range_Group;
                                        }
                                    }
                                }
                            }
                            #endregion identify each Range in Source_LM_CH by compare with all in Ref_LM_CH

                            #region check if new group found

                            if (WillBeGrouped_LM_CH.Count > 1)
                            {
                                for (int i = 1; i < WillBeGrouped_LM_CH.Count; i++)
                                {
                                    int diff_Group = Raw_Matrix_Data[WillBeGrouped_LM_CH[i]].Range_Group - Raw_Matrix_Data[WillBeGrouped_LM_CH[i - 1]].Range_Group;
                                    if (diff_Group < 0)
                                    {
                                        if (Math.Abs(Raw_Matrix_Data[WillBeGrouped_LM_CH[i]].ITU_Group_deltIp_current_mA) > Math.Abs(Raw_Matrix_Data[WillBeGrouped_LM_CH[i - 1]].ITU_Group_deltIp_current_mA))
                                            Raw_Matrix_Data[WillBeGrouped_LM_CH[i]].Range_Group = Raw_Matrix_Data[WillBeGrouped_LM_CH[i - 1]].Range_Group + 1;
                                        if (Math.Abs(Raw_Matrix_Data[WillBeGrouped_LM_CH[i]].ITU_Group_deltIp_current_mA) < Math.Abs(Raw_Matrix_Data[WillBeGrouped_LM_CH[i - 1]].ITU_Group_deltIp_current_mA))
                                            Raw_Matrix_Data[WillBeGrouped_LM_CH[i - 1]].Range_Group = Raw_Matrix_Data[WillBeGrouped_LM_CH[i]].Range_Group - 1;
                                    }
                                }
                            }
                            #endregion check if new group found

                            HasBeenGrouped_LM_CH.Clear();
                            foreach (int N in WillBeGrouped_LM_CH)
                            {
                                HasBeenGrouped_LM_CH.Add(N);
                            }
                            #region do not use the LM without any Range
                            HasBeenGrouped_LM = LM;
                            while (HasBeenGrouped_LM_CH.Count == 0)
                            {
                                HasBeenGrouped_LM++;
                                for (int N = Range_Start; N <= SM_Range_Index[sm]; N++)
                                {
                                    if (Raw_Matrix_Data[N].LM == HasBeenGrouped_LM)
                                        HasBeenGrouped_LM_CH.Add(N);
                                }
                            }
                            #endregion do not use the LM without any Range
                        }
                    }
                    #endregion start group from the LM to the SM begin

                    #region start group from the LM to the SM End
                    HasBeenGrouped_LM_CH.Clear();
                    foreach (int N in Max_LM_CH)
                    {
                        HasBeenGrouped_LM_CH.Add(N);
                    }
                    HasBeenGrouped_LM = Max_CH_LM;
                    for (int LM = Max_CH_LM + 1; LM <= Raw_Matrix_Data[SM_Range_Index[sm]].LM; LM++)
                    {
                        #region find the Range in LM and LM+1
                        WillBeGrouped_LM_CH.Clear();//put the Range which will be grouped
                        WillBeGrouped_LM = LM;
                        for (int N = Range_Start; N <= SM_Range_Index[sm]; N++)
                        {
                            if (Raw_Matrix_Data[N].LM == WillBeGrouped_LM)
                                WillBeGrouped_LM_CH.Add(N);
                        }
                        #endregion find the Range in LM and LM+1

                        if (WillBeGrouped_LM_CH.Count > 0)
                        {
                            #region identify each Range in Source_LM_CH by compare with all in Ref_LM_CH

                            LM_gap = 0;
                            for (int _lm = Math.Min(HasBeenGrouped_LM, WillBeGrouped_LM); _lm <= Math.Max(HasBeenGrouped_LM, WillBeGrouped_LM) - 1; _lm++)
                            {
                                LM_gap += SM_LM_Infor[sm, _lm].BelowLMGap * Math.Sign(WillBeGrouped_LM - HasBeenGrouped_LM);
                            }
                            for (int N = 0; N < WillBeGrouped_LM_CH.Count; N++)
                            {
                                Raw_Matrix_Data[WillBeGrouped_LM_CH[N]].Range_Group = -999;
                                for (int M = 0; M < HasBeenGrouped_LM_CH.Count; M++)
                                {
                                    bool same_slope = true;
                                    if (Math.Sign(Raw_Matrix_Data[WillBeGrouped_LM_CH[N]].Locker_Ratio_Slope) != Math.Sign(Math.Sign(Raw_Matrix_Data[HasBeenGrouped_LM_CH[M]].Locker_Ratio_Slope)))
                                        same_slope = false;
                                    bool LM_gap_even = false;
                                    if (LM_gap % 2 == 0)
                                        LM_gap_even = true;

                                    Group_Delt_Iphase_mA = Raw_Matrix_Data[WillBeGrouped_LM_CH[N]].ITU_Ip_current_mA - Raw_Matrix_Data[HasBeenGrouped_LM_CH[M]].ITU_Ip_current_mA;
                                    if (Math.Abs(LM_gap) > 1)
                                    {
                                        if ((Math.Abs(Raw_Matrix_Data[WillBeGrouped_LM_CH[N]].ITU_Group_deltIp_current_mA) >= Math.Abs(Group_Delt_Iphase_mA))
                                            && (same_slope == LM_gap_even)
                                            && Group_Delt_Iphase_mA <= 0)
                                        {
                                            Raw_Matrix_Data[WillBeGrouped_LM_CH[N]].ITU_Group_deltIp_current_mA = Group_Delt_Iphase_mA;
                                            Raw_Matrix_Data[WillBeGrouped_LM_CH[N]].Range_Group = Raw_Matrix_Data[HasBeenGrouped_LM_CH[M]].Range_Group;
                                        }
                                    }
                                    else
                                    {
                                        if ((Math.Abs(Raw_Matrix_Data[WillBeGrouped_LM_CH[N]].ITU_Group_deltIp_current_mA) >= Math.Abs(Group_Delt_Iphase_mA))
                                             && (same_slope == LM_gap_even))
                                        {
                                            Raw_Matrix_Data[WillBeGrouped_LM_CH[N]].ITU_Group_deltIp_current_mA = Group_Delt_Iphase_mA;
                                            Raw_Matrix_Data[WillBeGrouped_LM_CH[N]].Range_Group = Raw_Matrix_Data[HasBeenGrouped_LM_CH[M]].Range_Group;
                                        }
                                    }
                                }
                            }
                            #endregion identify each Range in Source_LM_CH by compare with all in Ref_LM_CH

                            #region check if new group found
                            if (WillBeGrouped_LM_CH.Count > 1)
                            {
                                for (int i = 1; i < WillBeGrouped_LM_CH.Count; i++)
                                {
                                    int diff_Group = Raw_Matrix_Data[WillBeGrouped_LM_CH[i]].Range_Group - Raw_Matrix_Data[WillBeGrouped_LM_CH[i - 1]].Range_Group;
                                    if (diff_Group < 0)
                                    {
                                        if (Math.Abs(Raw_Matrix_Data[WillBeGrouped_LM_CH[i]].ITU_Group_deltIp_current_mA) > Math.Abs(Raw_Matrix_Data[WillBeGrouped_LM_CH[i - 1]].ITU_Group_deltIp_current_mA))
                                            Raw_Matrix_Data[WillBeGrouped_LM_CH[i]].Range_Group = Raw_Matrix_Data[WillBeGrouped_LM_CH[i - 1]].Range_Group + 1;
                                        if (Math.Abs(Raw_Matrix_Data[WillBeGrouped_LM_CH[i]].ITU_Group_deltIp_current_mA) < Math.Abs(Raw_Matrix_Data[WillBeGrouped_LM_CH[i - 1]].ITU_Group_deltIp_current_mA))
                                            Raw_Matrix_Data[WillBeGrouped_LM_CH[i - 1]].Range_Group = Raw_Matrix_Data[WillBeGrouped_LM_CH[i]].Range_Group - 1;
                                    }
                                }
                            }
                            #endregion check if new group found

                            HasBeenGrouped_LM_CH.Clear();
                            foreach (int N in WillBeGrouped_LM_CH)
                            {
                                HasBeenGrouped_LM_CH.Add(N);
                            }
                            #region do not use the LM without any Range
                            HasBeenGrouped_LM = LM;
                            while (HasBeenGrouped_LM_CH.Count == 0)
                            {
                                HasBeenGrouped_LM--;
                                for (int N = Range_Start; N <= SM_Range_Index[sm]; N++)
                                {
                                    if (Raw_Matrix_Data[N].LM == HasBeenGrouped_LM)
                                        HasBeenGrouped_LM_CH.Add(N);
                                }
                            }
                            #endregion do not use the LM without any Range
                        }
                    }
                    #endregion start group from the LM to the SM End
                }
            }
            #endregion identify each etalon range to group by ITU_Iphase_mA

            #region Sort all ITU_CH in light of all Etalon Range sequence

            for (int SM = 0; SM < _p_supermodes.Count; SM++)
            {
                #region 1.Cal the ITU CH Correlation to find the most possible true range
                if (SM_Range_Index[SM] > 0)
                {
                    int ii = 0;
                    do
                    {
                        ii++;
                        if (SM - ii < 0)
                        {
                            Range_Start = 0;
                            break;
                        }
                        Range_Start = SM_Range_Index[SM - ii] + 1;
                    } while (SM_Range_Index[SM - ii] == 0);//jACK.ZHANG TO AVOID THE SM WITHOUT ANYONE RANGE

                    for (int N = Range_Start; N <= SM_Range_Index[SM]; N++)
                    {
                        for (int M = N; M <= SM_Range_Index[SM]; M++)
                        {
                            if (Raw_Matrix_Data[M].CH_Freq_GHz > 0 && Raw_Matrix_Data[N].CH_Freq_GHz > 0)
                            {
                                LM_gap = 0;
                                HasBeenGrouped_LM = Raw_Matrix_Data[N].LM;
                                WillBeGrouped_LM = Raw_Matrix_Data[M].LM;
                                for (int _lm = Math.Min(HasBeenGrouped_LM, WillBeGrouped_LM); _lm <= Math.Max(HasBeenGrouped_LM, WillBeGrouped_LM) - 1; _lm++)
                                {
                                    LM_gap += SM_LM_Infor[SM, _lm].BelowLMGap;
                                }
                                int Group_Gap = Raw_Matrix_Data[M].Range_Group - Raw_Matrix_Data[N].Range_Group;
                                if (Raw_Matrix_Data[M].CH_Freq_GHz - Raw_Matrix_Data[N].CH_Freq_GHz == (LM_gap + Group_Gap) * _itugrid.freq_step)
                                {
                                    Raw_Matrix_Data[N].IS_ITU_CH_Count++;
                                    if (N != M)
                                        Raw_Matrix_Data[M].IS_ITU_CH_Count++;
                                }
                            }
                        }
                        SM_LM_Infor[SM, Raw_Matrix_Data[N].LM].ItuCHMatchSum += Raw_Matrix_Data[N].IS_ITU_CH_Count;
                    }
                }

                #endregion 1.Cal the ITU CH Correlation to find the most possible true range

                #region 2.modify the wrong ITU channel base on the most possible true range

                int Max_IS_ITU_Count = -999;
                int ITU_Seed_LockerRang_Index = 0;
                for (int N = Range_Start; N <= SM_Range_Index[SM]; N++)
                {
                    int sm = Raw_Matrix_Data[N].SM;
                    int lm = Raw_Matrix_Data[N].LM;
                    Target_Index = (Raw_Matrix_Data[N].second_Ip_Index - Raw_Matrix_Data[N].first_Ip_Index) / 2 + Raw_Matrix_Data[N].first_Ip_Index;
                    CDSDBRSuperMode ITU_Seed_SM = _p_supermodes[SM];
                    CDSDBRSupermodeMapLine ITU_Seed_LM = ITU_Seed_SM._lm_filtered_middle_lines[lm];
                    int ITU_Count_FigureofMerit_offset = (int)Math.Abs(Target_Index - ITU_Seed_LM._points.Count / 2) / 10;

                    //(- ITU_Count_FigureofMerit_offset)to find Mid phase position. jack.zhang 2013-08-20
                    int ITU_Count_FigureofMerit =
                        Raw_Matrix_Data[N].IS_ITU_CH_Count + SM_LM_Infor[SM, Raw_Matrix_Data[N].LM].ItuCHMatchSum - ITU_Count_FigureofMerit_offset;
                    if (ITU_Count_FigureofMerit >= Max_IS_ITU_Count)//>=to find high rear position,> to find low rear position.jack.zhang 2013-05-08
                    {
                        Max_IS_ITU_Count = ITU_Count_FigureofMerit;
                        ITU_Seed_LockerRang_Index = N;
                    }
                }

                Raw_Matrix_Data[ITU_Seed_LockerRang_Index].IS_ITU_CH = true;
                if (Raw_Matrix_Data[ITU_Seed_LockerRang_Index].CH_Freq_GHz_Modify < _itugrid._ITU_frequencies[0] || Raw_Matrix_Data[ITU_Seed_LockerRang_Index].CH_Freq_GHz_Modify > _itugrid._ITU_frequencies[_itugrid.channel_count() - 1])
                    Raw_Matrix_Data[ITU_Seed_LockerRang_Index].IS_ITU_CH = false;//for the SM without anyone ITUCH---SM0/6


                if (isWaveMeterOnline)
                {
                    #region Use WM to claibrate the Freq Seed for each SM.
                    int sm = Raw_Matrix_Data[ITU_Seed_LockerRang_Index].SM;
                    int lm = Raw_Matrix_Data[ITU_Seed_LockerRang_Index].LM;
                    Target_Index = (Raw_Matrix_Data[ITU_Seed_LockerRang_Index].second_Ip_Index - Raw_Matrix_Data[ITU_Seed_LockerRang_Index].first_Ip_Index) / 2 + Raw_Matrix_Data[ITU_Seed_LockerRang_Index].first_Ip_Index;
                    CDSDBRSuperMode ITU_Seed_SM = _p_supermodes[SM];
                    CDSDBRSupermodeMapLine ITU_Seed_LM = ITU_Seed_SM._lm_filtered_middle_lines[lm];
                    pt_on_middle_line = new CDSDBROperatingPoint(ITU_Seed_SM, ITU_Seed_LM, SM, lm, Target_Index);
                    #region set laser current considering Mz not change during tunning and the one time sleep for Irear from 70 to 0 mA when SM switch

                    # region reset RearSoa for DUT locker current meet limit in  final test
                    slope_rearsoa = slope_rear_rearsoa[SM, 0];
                    constant_rear_rearsoa = slope_rear_rearsoa[SM, 1];

                    pt_on_middle_line.ReCountRearSoa(slope_rearsoa, constant_rear_rearsoa, Max_RearSoa_mA, Min_RearSoa_mA);//Echo new added to recount rearsoa
                    # endregion reset RearSoa for DUT locker current meet limit in  final test
                    fcu.IimbLeft_mA = (float)Mz_Imb_Left_mA_SM[SM];
                    fcu.IimbRight_mA = (float)Mz_Imb_Right_mA_SM[SM];
                    returnValue = pt_on_middle_line.Apply(fcu);//for ASIC power Hitt by FCUMKI

                    if (returnValue == false)
                    {
                        throw new Exception("FCU exception");
                    }
                    #endregion set laser current considering Mz not change during tunning and the one time sleep for Irear from 70 to 0 mA when SM switch
                    double frerq_Ghz = measure_Freq();
                    int CH_Number_measured =(int) ((frerq_Ghz - _itugrid.min_freq) / _itugrid.freq_step+0.5*Math.Sign(frerq_Ghz - _itugrid.min_freq));//the 0.5 offset need make sure the sign. jack.zhang 2013-05-07
                    if(Raw_Matrix_Data[ITU_Seed_LockerRang_Index].CH_Number!=CH_Number_measured)
                        _CalFreqWrong = true;
                    Raw_Matrix_Data[ITU_Seed_LockerRang_Index].CH_Number_Modify = CH_Number_measured;
                    Raw_Matrix_Data[ITU_Seed_LockerRang_Index].CH_Freq_GHz_Modify = _itugrid.freq_step * CH_Number_measured + (int)_itugrid.min_freq;
                    #endregion Use WM to claibrate the Freq Seed for each SM.
                }
                else
                {
                    Raw_Matrix_Data[ITU_Seed_LockerRang_Index].CH_Freq_GHz_Modify = Raw_Matrix_Data[ITU_Seed_LockerRang_Index].CH_Freq_GHz;
                    Raw_Matrix_Data[ITU_Seed_LockerRang_Index].CH_Number_Modify = Raw_Matrix_Data[ITU_Seed_LockerRang_Index].CH_Number;
                }
                double First_Target_Power_Ratio, Second_Target_Power_Ratio;
                bool In_Cal_Range;
                int N_sign = -1;
                for (int N = ITU_Seed_LockerRang_Index - N_sign; N <= SM_Range_Index[SM]; N++)
                {
                    HasBeenGrouped_LM = Raw_Matrix_Data[N + N_sign].LM;
                    WillBeGrouped_LM = Raw_Matrix_Data[N].LM;
                    LM_gap = 0;
                    for (int _lm = Math.Min(HasBeenGrouped_LM, WillBeGrouped_LM); _lm <= Math.Max(HasBeenGrouped_LM, WillBeGrouped_LM) - 1; _lm++)
                    {
                        LM_gap += SM_LM_Infor[SM, _lm].BelowLMGap * Math.Sign(WillBeGrouped_LM - HasBeenGrouped_LM);
                    }

                    int Group_Gap = Raw_Matrix_Data[N].Range_Group - Raw_Matrix_Data[N + N_sign].Range_Group;
                    #region modify freq in light Group
                    Raw_Matrix_Data[N].CH_Freq_GHz_Modify = Raw_Matrix_Data[N + N_sign].CH_Freq_GHz_Modify + (LM_gap + Group_Gap) * _itugrid.freq_step;
                    Raw_Matrix_Data[N].CH_Number_Modify = Raw_Matrix_Data[N + N_sign].CH_Number_Modify + (LM_gap + Group_Gap) * 1;
                    #endregion modify freq in light Group

                    Raw_Matrix_Data[N].IS_ITU_CH = true;
                    if (Raw_Matrix_Data[N].CH_Freq_GHz_Modify < _itugrid._ITU_frequencies[0] || Raw_Matrix_Data[N].CH_Freq_GHz_Modify > _itugrid._ITU_frequencies[_itugrid.channel_count() - 1])
                        Raw_Matrix_Data[N].IS_ITU_CH = false;
                    else
                    {
                        if (Raw_Matrix_Data[N].CH_Freq_GHz_Modify != Raw_Matrix_Data[N].CH_Freq_GHz)
                        {
                            FreqCalByEtalon.CalPRatio_LRatio_byFreq_GHz(Raw_Matrix_Data[N].CH_Freq_GHz_Modify, out Target_Power_Ratio, out Filter_Etalon_Slope,
                                                   out Target_Locker_Ratio, out First_Target_Power_Ratio,
                                                   out Second_Target_Power_Ratio, out In_Cal_Range);
                            Raw_Matrix_Data[N].Target_Locker_Ratio = Target_Locker_Ratio;
                            if (FreqCalByEtalon.UseDUTLockerRatio)
                            {
                                Raw_Matrix_Data[N].Target_Locker_Ratio = (FreqCalByEtalon.Max_Locker_Ratio_50GHz + FreqCalByEtalon.Min_Locker_Ratio_50GHz) / 2.0;
                                if ((int)Raw_Matrix_Data[N].CH_Freq_GHz_Modify % 100 == 0)//DUT lockerRatio(Rx/Tx) slope is negtive for 100GHz. jack.zhang 2012-11-15
                                    Raw_Matrix_Data[N].Target_Locker_Ratio = (FreqCalByEtalon.Max_Locker_Ratio_100GHz + FreqCalByEtalon.Min_Locker_Ratio_100GHz) / 2.0;
                            }
                        }
                    }
                }
                //do not send email if no avialable ITU cal freq err. jack.zhang 2013-12-01
                _CalFreqWrong = (Raw_Matrix_Data[SM_Range_Index[SM]].IS_ITU_CH || Raw_Matrix_Data[Range_Start].IS_ITU_CH) && _CalFreqWrong;

                N_sign = 1;
                for (int N = ITU_Seed_LockerRang_Index - N_sign; N >= Range_Start; N--)
                {
                    HasBeenGrouped_LM = Raw_Matrix_Data[N + N_sign].LM;
                    WillBeGrouped_LM = Raw_Matrix_Data[N].LM;
                    LM_gap = 0;
                    for (int _lm = Math.Min(HasBeenGrouped_LM, WillBeGrouped_LM); _lm <= Math.Max(HasBeenGrouped_LM, WillBeGrouped_LM) - 1; _lm++)
                    {
                        LM_gap += SM_LM_Infor[SM, _lm].BelowLMGap * Math.Sign(WillBeGrouped_LM - HasBeenGrouped_LM);
                    }

                    int Group_Gap = Raw_Matrix_Data[N].Range_Group - Raw_Matrix_Data[N + N_sign].Range_Group;
                    #region modify freq in light Group
                    Raw_Matrix_Data[N].CH_Freq_GHz_Modify = Raw_Matrix_Data[N + N_sign].CH_Freq_GHz_Modify + (LM_gap + Group_Gap) * _itugrid.freq_step;
                    Raw_Matrix_Data[N].CH_Number_Modify = Raw_Matrix_Data[N + N_sign].CH_Number_Modify + (LM_gap + Group_Gap) * 1;
                    #endregion modify freq in light Group

                    Raw_Matrix_Data[N].IS_ITU_CH = true;
                    if (Raw_Matrix_Data[N].CH_Freq_GHz_Modify < _itugrid._ITU_frequencies[0] || Raw_Matrix_Data[N].CH_Freq_GHz_Modify > _itugrid._ITU_frequencies[_itugrid.channel_count() - 1])
                        Raw_Matrix_Data[N].IS_ITU_CH = false;
                    else
                    {
                        if (Raw_Matrix_Data[N].CH_Freq_GHz_Modify != Raw_Matrix_Data[N].CH_Freq_GHz)
                        {
                            FreqCalByEtalon.CalPRatio_LRatio_byFreq_GHz(Raw_Matrix_Data[N].CH_Freq_GHz_Modify, out Target_Power_Ratio, out Filter_Etalon_Slope,
                                                   out Target_Locker_Ratio, out First_Target_Power_Ratio,
                                                   out Second_Target_Power_Ratio, out In_Cal_Range);
                            Raw_Matrix_Data[N].Target_Locker_Ratio = Target_Locker_Ratio;
                            if (FreqCalByEtalon.UseDUTLockerRatio)
                            {
                                Raw_Matrix_Data[N].Target_Locker_Ratio = (FreqCalByEtalon.Max_Locker_Ratio_50GHz + FreqCalByEtalon.Min_Locker_Ratio_50GHz) / 2.0;
                                if ((int)Raw_Matrix_Data[N].CH_Freq_GHz_Modify % 100 == 0)//DUT lockerRatio(Rx/Tx) slope is negtive for 100GHz. jack.zhang 2012-11-15
                                    Raw_Matrix_Data[N].Target_Locker_Ratio = (FreqCalByEtalon.Max_Locker_Ratio_100GHz + FreqCalByEtalon.Min_Locker_Ratio_100GHz) / 2.0;
                            }
                        }
                    }
                }

                #endregion 2.modify the wrong ITU channel base on the most possible true range


            }
            writeRawLockerRangeToFile();

            #endregion Sort all ITU_CH in light of all Etalon Range sequence

            #region fine search ITU current position in each etalon range

            int Last_sm = 0;
            if(!isWaveMeterOnline)//to avoid no set as the default (false) when retry the module without WM. jack.zhang 2012-05-07
                _CalFreqWrong = false; // chongjian.liang 2016.09.05
                //_CalFreqWrong=Bookham.TestSolution.IlmzCommonUtils.Measurements.Wavemeter.IsOnline;
            foreach (Matrix_Data item in Raw_Matrix_Data)
            {
                if (item.IS_ITU_CH)
                {
                    int sm = item.SM;
                    int lm = item.LM;
                    CDSDBRSuperMode i_sm_First = _p_supermodes[sm];
                    CDSDBRSupermodeMapLine i_lm_First = i_sm_First._lm_filtered_middle_lines[lm];
                    double Min_Delt_Locker_Ratio = 999;
                    int Close_Point_Index_1 = 0;
                    int Close_Point_Index_2 = 0;
                    bool find_close_point = false;
                    int start_index = item.first_Ip_Index;
                    int end_index = item.second_Ip_Index;
                    double Freq_Error_GHz = 0;
                    double Cal_Freq_Error_GHz = 0;

                    matrix_point = AverageMatrixPoint(i_lm_First, start_index, filter_n, Forward_Reverse_Ratio);
                    double Start_Point_Locker_Ratio = matrix_point.Average_Locker_Ratio;

                    matrix_point = AverageMatrixPoint(i_lm_First, end_index, filter_n, Forward_Reverse_Ratio);
                    double End_Point_Locker_Ratio = matrix_point.Average_Locker_Ratio;

                    double Last_Locker_Ratio = 0;

                    #region find ITU_point and interpolate current set
                    if ((Start_Point_Locker_Ratio - item.Target_Locker_Ratio) * (End_Point_Locker_Ratio - item.Target_Locker_Ratio) <= 0)
                    {
                        for (int Point_index = start_index; Point_index <= end_index; Point_index++)
                        {
                            matrix_point = AverageMatrixPoint(i_lm_First, Point_index, filter_n, Forward_Reverse_Ratio);
                            Current_Locker_Ratio = matrix_point.Average_Locker_Ratio;
                            if (item.Locker_Ratio_Slope > 0 && Current_Locker_Ratio >= FreqCalByEtalon.Min_Locker_Ratio_50GHz && Current_Locker_Ratio <= FreqCalByEtalon.Max_Locker_Ratio_50GHz
                              || item.Locker_Ratio_Slope < 0 && Current_Locker_Ratio >= FreqCalByEtalon.Min_Locker_Ratio_100GHz && Current_Locker_Ratio <= FreqCalByEtalon.Max_Locker_Ratio_100GHz)
                            {

                                Delt_Locker_Ratio = Current_Locker_Ratio - item.Target_Locker_Ratio;
                                if (Math.Abs(Delt_Locker_Ratio) < Math.Abs(Min_Delt_Locker_Ratio))
                                {
                                    Min_Delt_Locker_Ratio = Delt_Locker_Ratio;

                                    if ((Math.Sign(Min_Delt_Locker_Ratio) == Math.Sign(item.Locker_Ratio_Slope)))
                                    {

                                        Close_Point_Index_2 = Point_index;
                                        Close_Point_Index_1 = Math.Max(0, Point_index - 1);
                                    }
                                    else
                                    {
                                        Close_Point_Index_1 = Point_index;
                                        Close_Point_Index_2 = Math.Min(i_lm_First._points.Count, Point_index + 1);
                                    }

                                    find_close_point = true;
                                }
                            }

                            //modify for frequency ripple case.the LR jump between adjacent points is larger than the delta between min and max. jack.zhang 2012-12-12
                            //*Last_Locker_Ratio is to skip the frist LR. jack.zhang
                            if (!find_close_point&&
                                (item.Locker_Ratio_Slope > 0 && (FreqCalByEtalon.Min_Locker_Ratio_50GHz - Last_Locker_Ratio) * (Current_Locker_Ratio - FreqCalByEtalon.Max_Locker_Ratio_50GHz) * Last_Locker_Ratio > 0
                             || item.Locker_Ratio_Slope < 0 && (FreqCalByEtalon.Min_Locker_Ratio_100GHz - Current_Locker_Ratio) * (Last_Locker_Ratio - FreqCalByEtalon.Max_Locker_Ratio_100GHz) * Last_Locker_Ratio > 0))
                            {
                                Close_Point_Index_1 = Math.Max(0, Point_index - 1);
                                Close_Point_Index_2 = Point_index ;
                                Min_Delt_Locker_Ratio =Math.Min(Math.Abs(Last_Locker_Ratio-item.Target_Locker_Ratio),Math.Abs(Current_Locker_Ratio - item.Target_Locker_Ratio));
                                find_close_point = true;
                            }
                            Last_Locker_Ratio = Current_Locker_Ratio;
                        }

                        if (find_close_point)
                        {
                            double frequency_at_cal_pt = 0, power_ratio_at_cal_pt = 0, locker_ratio_at_cal_pt = 0;
                            double initial_Index = 0, initial_frequency_at_cal_pt = 0, initial_locker_ratio_at_cal_pt = 0;
                            double Last_locker_ratio_at_cal_pt = 0, Last_Target_Index = 0, delfre = 0;
                            bool Fix_Front_Pair_Setting = false;
                            int Tunning_Count = 0, Front_Pair = 0;
                            double Front_Section_NonConstant_mA = 0;
                            double Current_Locker_Ratio_1 = 0, Current_Locker_Ratio_2 = 0;
                            double Last_Good_Index_for_FPSwitch = 0, Min_Freq_Error_GHz_for_FPSwitch = 999;
                            List<double> FineTuning_PowerRatio=new List<double>();
                            //jack.zhang fix the larger fre error at FP switch while Tunning_Count = MAX_ITERATIONS_TO_FIND_ITU_PT
                            //use the min fre error point during the tunning when Tunning_Count = MAX_ITERATIONS_TO_FIND_ITU_PT 2011-09-05

                            #region for FP switch just select the near target LR FP but not interpolate. jack.zhang 2011-09-05
                            CDSDBROperatingPoint pt_on_middle_line_1 = new CDSDBROperatingPoint(i_sm_First, i_lm_First, sm, lm, Close_Point_Index_1);
                            CDSDBROperatingPoint pt_on_middle_line_2 = new CDSDBROperatingPoint(i_sm_First, i_lm_First, sm, lm, Close_Point_Index_2);
                            if (pt_on_middle_line_1._front_pair_number != pt_on_middle_line_2._front_pair_number)
                            {
                                Fix_Front_Pair_Setting = true;
                                Front_Pair = pt_on_middle_line_1._front_pair_number;//inital it to avoid nonConstantCurrent(Target_Index) is not 5 or 0mA.Rachel.Wang2012-05-08
                                Front_Section_NonConstant_mA = pt_on_middle_line_1._nonConstantCurrent;
                            }
                            #endregion for FP switch just select the near target LR FP but not interpolate. jack.zhang 2011-09-05

                            matrix_point = AverageMatrixPoint(i_lm_First, Close_Point_Index_1, Fix_Front_Pair_Setting ? 0 : filter_n, Forward_Reverse_Ratio);
                            Current_Locker_Ratio_1 = matrix_point.Average_Locker_Ratio;

                            matrix_point = AverageMatrixPoint(i_lm_First, Close_Point_Index_2, Fix_Front_Pair_Setting ? 0 : filter_n, Forward_Reverse_Ratio);
                            Current_Locker_Ratio_2 = matrix_point.Average_Locker_Ratio;

                            Target_Index =
                                Close_Point_Index_1 + Math.Abs(Min_Delt_Locker_Ratio / (Current_Locker_Ratio_2 - Current_Locker_Ratio_1));
                            if (Target_Index < 0 || Target_Index > i_lm_First._points.Count)
                                Target_Index = Close_Point_Index_1;

                            Last_Target_Index = Target_Index;

                            pt_on_middle_line = new CDSDBROperatingPoint(i_sm_First, i_lm_First, sm, lm, Target_Index);
                            if (pt_on_middle_line._nonConstantCurrent == FrontSectionCurrent_mA_List[0]
                             || pt_on_middle_line._nonConstantCurrent == FrontSectionCurrent_mA_List[FrontSectionCurrent_mA_List.Count - 1])
                            {
                                Fix_Front_Pair_Setting = true;
                                Front_Pair = pt_on_middle_line._front_pair_number;
                                Front_Section_NonConstant_mA = pt_on_middle_line._nonConstantCurrent;
                            }

                            #region set laser current considering Mz not change during tunning and the one time sleep for Irear from 70 to 0 mA when SM switch

                            # region reset RearSoa for DUT locker current meet limit in  final test
                            slope_rearsoa = slope_rear_rearsoa[sm, 0];
                            constant_rear_rearsoa = slope_rear_rearsoa[sm, 1];
                           
                            pt_on_middle_line.ReCountRearSoa(slope_rearsoa, constant_rear_rearsoa,Max_RearSoa_mA,Min_RearSoa_mA);//Echo new added to recount rearsoa
                            # endregion reset RearSoa for DUT locker current meet limit in  final test
                            fcu.IimbLeft_mA = (float)Mz_Imb_Left_mA_SM[sm];
                            fcu.IimbRight_mA = (float)Mz_Imb_Right_mA_SM[sm];
                            returnValue = pt_on_middle_line.Apply(fcu);//for ASIC power Hitt by FCUMKI

                            if (returnValue == false)
                            {
                                throw new Exception("FCU exception");
                            }
                            if (sm != Last_sm || item.CH_Number == 0)
                                System.Threading.Thread.Sleep(1000);
                            #endregion set laser current considering Mz not change during tunning and the one time sleep for Irear from 70 to 0 mA when SM switch
                            Bookham.TestSolution.IlmzCommonUtils.DsdbrUtils.LockerCurrents DUT_Locker=new Bookham.TestSolution.IlmzCommonUtils.DsdbrUtils.LockerCurrents();
                            do
                            {
                                double Irearsoa_fine_mA = pt_on_middle_line._I_rearsoa_Fine_mA;
                                double DUT_Fine_Locker_Reference_mA = pt_on_middle_line._DUT_Fine_locker_Reference_mA;
                                double DUT_Fine_Locker_Etalon_mA = pt_on_middle_line._DUT_Fine_locker_Etalon_mA;
                                do
                                {
                                    #region  fine tune on middle line
                                    if (Target_Index == Last_Target_Index && Tunning_Count > 0)//Iphase tuning to item.second_Ip_Index or item.frist_Ip_Index again. Jack.zhang 2013-01-03
                                    {
                                        Target_Index = Last_Good_Index_for_FPSwitch;
                                        Tunning_Count = 10 * MAX_ITERATIONS_TO_FIND_ITU_PT;//to select last good point and end loop.
                                    }

                                    pt_on_middle_line = new CDSDBROperatingPoint(i_sm_First, i_lm_First, sm, lm, Target_Index);
                                    pt_on_middle_line._I_rearsoa_Fine_mA = Irearsoa_fine_mA;
                                    pt_on_middle_line._DUT_Fine_locker_Reference_mA = DUT_Fine_Locker_Reference_mA;
                                    pt_on_middle_line._DUT_Fine_locker_Etalon_mA = DUT_Fine_Locker_Etalon_mA;

                                    if (Fix_Front_Pair_Setting)
                                    {
                                        pt_on_middle_line._front_pair_number = (short)Front_Pair;
                                        pt_on_middle_line._nonConstantCurrent = Front_Section_NonConstant_mA;
                                    }
                                    pt_on_middle_line.ReCountRearSoa(slope_rearsoa, constant_rear_rearsoa, Max_RearSoa_mA, Min_RearSoa_mA);//Echo new added to recount rearsoa
                                    returnValue = pt_on_middle_line.Apply(fcu);//for ASIC power Hitt by FCUMKI
                                    if (returnValue == false)
                                    {
                                        throw new Exception("FCU exception");
                                    }

                                    returnValue = measure_Pr_Lr(ref power_ratio_at_cal_pt, ref locker_ratio_at_cal_pt,
                                                 short.Parse(characterizationSetting["NumberOfFrequencyMeasurementsToAveragePerOperatingPoint"]));

                                    FineTuning_PowerRatio.Add(power_ratio_at_cal_pt);

                                    #region fine tune target frequency. jack.Zhang 2011-01-26
                                    Tunning_Count++;

                                    // by tim to combine solution
                                    if (isWaveMeterOnline && UseWMDoLowCostCheck && Tunning_Count == 1)//record the first point to verify the fine tuning value
                                    {
                                        initial_Index = Target_Index;
                                        initial_frequency_at_cal_pt = frequency_at_cal_pt;
                                        initial_locker_ratio_at_cal_pt = locker_ratio_at_cal_pt;
                                    }
                                    if (Last_Target_Index != Target_Index)
                                        item.Locker_Ratio_Slope = (locker_ratio_at_cal_pt - Last_locker_ratio_at_cal_pt) / (Target_Index - Last_Target_Index);
                                    Last_locker_ratio_at_cal_pt = locker_ratio_at_cal_pt;
                                    Last_Target_Index = Target_Index;
                                    Delt_Locker_Ratio = item.Target_Locker_Ratio - locker_ratio_at_cal_pt;
                                    Target_Index = Target_Index + Delt_Locker_Ratio / item.Locker_Ratio_Slope;
                                    Target_Index = Math.Max(Target_Index, item.first_Ip_Index);
                                    Target_Index = Math.Min(Target_Index, item.second_Ip_Index);
                                    #endregion fine tune target frequency. jack.Zhang 2011-01-26
                                    if (item.Target_Locker_Ratio_Slope * item.Locker_Ratio_Slope >= 0)//use "=" to avoid fail some CH with power ratio abnormal. jack.zhang 2013-09-02
                                    {
                                        delfre = Delt_Locker_Ratio / FreqCalByEtalon.Average_Locker_Ratio_SlopeToGHz;
                                    }
                                    else
                                    {
                                        Target_Index = Tunning_Count > 1 ? Last_Good_Index_for_FPSwitch : Last_Target_Index;//retest the point when mode hop happen whatever true or false hop. jack.zhang 2013-07-08 
                                        delfre = 999;
                                    }
                                    if (Min_Freq_Error_GHz_for_FPSwitch > Math.Abs(delfre))
                                    {
                                        Last_Good_Index_for_FPSwitch = Last_Target_Index;
                                        Min_Freq_Error_GHz_for_FPSwitch = Math.Abs(delfre);
                                    }
                                    //jack.zhang fix the larger fre error at FP switch while Tunning_Count = MAX_ITERATIONS_TO_FIND_ITU_PT
                                    //use the min fre error point during the tunning when Tunning_Count = MAX_ITERATIONS_TO_FIND_ITU_PT 2011-09-05
                                    if (Tunning_Count == MAX_ITERATIONS_TO_FIND_ITU_PT)
                                        Target_Index = Last_Good_Index_for_FPSwitch;
                                    #endregion  fine tune on middle line
                                } while (Math.Abs(delfre) > FrequencyTolerance_GHz && Tunning_Count <= MAX_ITERATIONS_TO_FIND_ITU_PT);
                                
                                #region RearSoa fine tunning
                                
                                if (programSettings.GetBoolParam("DoRearSoaFineTunning"))
                                {
                                    if (pt_on_middle_line._I_rearsoa_Fine_mA == 0)
                                        DUT_Locker = Bookham.TestSolution.IlmzCommonUtils.DsdbrUtils.ReadLockerCurrents();

                                    RearSoaTunning(pt_on_middle_line);
                                    double Irearsoa_change_ratio =
                                        Math.Abs(pt_on_middle_line._I_rearsoa_Fine_mA - pt_on_middle_line._I_rearsoa) / pt_on_middle_line._I_rearsoa;
                                    if (Irearsoa_fine_mA != 0)
                                        Irearsoa_change_ratio = Math.Abs(pt_on_middle_line._I_rearsoa_Fine_mA - Irearsoa_fine_mA) / pt_on_middle_line._I_rearsoa_Fine_mA; ;
                                    if (Irearsoa_change_ratio > 0.1)
                                    {
                                        Tunning_Count = 0;
                                        Target_Index = Last_Target_Index;//retest the point with new rearsoa;
                                        returnValue = measure_Pr_Lr(ref power_ratio_at_cal_pt, ref locker_ratio_at_cal_pt,
                                                        short.Parse(characterizationSetting["NumberOfFrequencyMeasurementsToAveragePerOperatingPoint"]));
                                        Delt_Locker_Ratio = item.Target_Locker_Ratio - locker_ratio_at_cal_pt;
                                        delfre = Delt_Locker_Ratio / FreqCalByEtalon.Average_Locker_Ratio_SlopeToGHz;
                                    }
                                }
                                else
                                    pt_on_middle_line._I_rearsoa_Fine_mA = pt_on_middle_line._I_rearsoa;
                                if (Tunning_Count > MAX_ITERATIONS_TO_FIND_ITU_PT) break;                                
                                #endregion RearSoa fine tunning

                            } while (Math.Abs(delfre) > FrequencyTolerance_GHz);

							// by tim to combine solution
                            if (isWaveMeterOnline && UseWMDoLowCostCheck)
                            {
                                returnValue = measure_F_Pr_Lr(ref frequency_at_cal_pt, ref power_ratio_at_cal_pt, ref locker_ratio_at_cal_pt,
                                        short.Parse(characterizationSetting["NumberOfFrequencyMeasurementsToAveragePerOperatingPoint"]));

                                if (returnValue == false)
                                {
                                    throw new Exception("Wavemeter exception");
                                }
                                FineTuning_PowerRatio.Add(power_ratio_at_cal_pt);
                            }
                            FineTuning_PowerRatio.Sort();
                            if (FineTuning_PowerRatio.Count > 1)
                            power_ratio_at_cal_pt = FineTuning_PowerRatio[Math.Max(0, (int)Math.Round(FineTuning_PowerRatio.Count / 2.0) - 1)];

                            Last_sm = item.SM;
                            pt_on_middle_line._ITU_channel_number = item.CH_Number_Modify;
                            pt_on_middle_line._ITU_frequency = item.CH_Freq_GHz_Modify;
                            pt_on_middle_line._F_measured = frequency_at_cal_pt;
                            Freq_Error_GHz = pt_on_middle_line._ITU_frequency - pt_on_middle_line._F_measured;
                            pt_on_middle_line._F_calibration = FreqCalByEtalon.CalFreqByPRatio_LRatio_GHz(power_ratio_at_cal_pt, locker_ratio_at_cal_pt,Math.Sign(item.Locker_Ratio_Slope));

                            // PowerRatio can't reach target accuracy 2012-03-15 by jack zhang
                            //we can find all ITU CH when some LM missing line but some wrong modify
                            //frequency maybe result from the LM missing line. Math.Abs(delfre) can not 
                            //screen out them when it is equal to 100*N. So we need to screen out them
                            //by Cal_Freq_Error_GHz.jack.zhang 2011-11-15
                            //but the pt_on_middle_line._F_calibration not so cal accuracy. mask it now. jack.zhang 2012-06-27
                            Cal_Freq_Error_GHz = 0.0;//pt_on_middle_line._ITU_frequency-pt_on_middle_line._F_calibration;

                            pt_on_middle_line._Pr_on_sm_map = power_ratio_at_cal_pt;
                            pt_on_middle_line._Lr_on_sm_map = locker_ratio_at_cal_pt;
                            pt_on_middle_line._F_Forward_PowerRatio = item.Target_Power_Ratio;
                            pt_on_middle_line._F_Forward_LockerRatio = item.Target_Locker_Ratio;
                            pt_on_middle_line._Tunning_Count = Tunning_Count;
                            pt_on_middle_line._Initial_Freq_GHz = initial_frequency_at_cal_pt;
                            pt_on_middle_line._Initial_Lr_on_sm_map = initial_locker_ratio_at_cal_pt;
                            pt_on_middle_line._DUT_locker_Etalon_mA = DUT_Locker.TxCurrent_mA;
                            pt_on_middle_line._DUT_locker_Reference_mA = DUT_Locker.RxCurrent_mA;
                            _coarse_freq_sample_points.Add(new Pair<double, CDSDBROperatingPoint>(item.CH_Freq_GHz_Modify, pt_on_middle_line));

                            if (pt_on_middle_line._I_phase >= MinPhaseCurrent_mA
                                && pt_on_middle_line._I_phase <= MaxPhaseCurrent_mA
                                && pt_on_middle_line._I_rear >= MinRearCurrent)
                            {
                                // set wavemeter_error_ghz in closegrid.xml 4GHz for lowcost solution. jack.zhang 2012-10-19
                                if(((Math.Abs(delfre) <= wavemeter_error_ghz)||(Math.Abs(Freq_Error_GHz) <= wavemeter_error_ghz))
                                  &&(Math.Abs(Cal_Freq_Error_GHz) < 90))
                                    _characterisation_itu_ops.Add(pt_on_middle_line);
                                if ((Math.Abs(Freq_Error_GHz) > 10&& pt_on_middle_line._F_measured > 0) || (Math.Abs(Cal_Freq_Error_GHz) > 90))
                                {
                                    _CalFreqWrong = true;//for send email to enginee when freq wrong. Jack.zhang 2011-08-16
                                }
                            }
                        }
                    }
                }
                    #endregion find ITU_point and interpolate current set

                ITU_CH_Count++;

                if (OnLongitudinalModeFinished != null)
                {
                    OnLongitudinalModeFinished(this, new ProgressChangedEventArgs((int)(ITU_CH_Count * 1.0 / Raw_Matrix_Data.Count * 100), this));
                }
            }
            #endregion fine search ITU current position in each etalon range

            writeCalibrationFrequencyPointsToFile();
            findNearbyBoundaryCoords(_characterisation_itu_ops);
            writeOPsToFile();
            //to guarrenty no false pass device with WaveMeter test. jack.zhang 2011-10-10
			// by tim to combine solution
            if (!isWaveMeterOnline || (_ITUPointsCount != _itugrid.channel_count()))
                _CalFreqWrong = false;
        }

        private Matrix_Point AverageMatrixPoint(CDSDBRSupermodeMapLine LM_Data, int Point_index, int Average_Point_Number, double Forward_Reverse_Ratio)
        {
            double window_size = 0;
            Matrix_Point Matrix_Point = new Matrix_Point();

            for (int j = (int)(Point_index - Average_Point_Number); j <= Point_index + Average_Point_Number; j++)
            {
                if (j >= 0 && j < (int)(LM_Data._points.Count))
                {
                    // sum rows in window, without exceeding start or end
                    window_size++;
                    Matrix_Point.Forward_Locker_Ratio = LM_Data._points[j].Forward_LockerRatio;
                    Matrix_Point.Reverse_Locker_Ratio = LM_Data._points[j].Reverse_LockerRatio;
                    Matrix_Point.Average_Locker_Ratio += (Matrix_Point.Forward_Locker_Ratio * Forward_Reverse_Ratio + Matrix_Point.Reverse_Locker_Ratio * (1 - Forward_Reverse_Ratio));
                    Matrix_Point.Forward_Power_Ratio = LM_Data._points[j].Forward_PowerRatio;
                    Matrix_Point.Reverse_Power_Ratio = LM_Data._points[j].Reverse_PowerRatio;
                    Matrix_Point.Average_Power_Ratio += (Matrix_Point.Forward_Power_Ratio * Forward_Reverse_Ratio + Matrix_Point.Reverse_Power_Ratio * (1 - Forward_Reverse_Ratio));

                }
            }
            Matrix_Point.Forward_Locker_Ratio = LM_Data._points[Point_index].Forward_LockerRatio;
            Matrix_Point.Reverse_Locker_Ratio = LM_Data._points[Point_index].Reverse_LockerRatio;
            Matrix_Point.Forward_Power_Ratio = LM_Data._points[Point_index].Forward_PowerRatio;
            Matrix_Point.Reverse_Power_Ratio = LM_Data._points[Point_index].Reverse_PowerRatio;
            Matrix_Point.Average_Locker_Ratio = Matrix_Point.Average_Locker_Ratio / window_size;
            Matrix_Point.Average_Power_Ratio = Matrix_Point.Average_Power_Ratio / window_size;
            return Matrix_Point;
        }
        private void IphaseIndexRangeLM(CDSDBRSupermodeMapLine LM_Data, double MinPhaseCurrent_mA, double MaxPhaseCurrent_mA, out int Start_Iphase_Index, out int End_Iphase_Index)
        {
            double minPhaseDistance = 101;
            int LastmaxPhaseIndex = -1;
            Start_Iphase_Index = 0;
            End_Iphase_Index = Math.Max(0, LM_Data._points.Count - 1);
            for (int i = 0; i < ((int)LM_Data._points.Count); i++)
            {
                if ((MinPhaseCurrent_mA - ((LM_Data._points[i]).I_phase) >= 0) && (MinPhaseCurrent_mA - LM_Data._points[i].I_phase <= minPhaseDistance) && i > LastmaxPhaseIndex)
                {
                    Start_Iphase_Index = i;
                    LastmaxPhaseIndex = i;
                    minPhaseDistance = MinPhaseCurrent_mA - LM_Data._points[i].I_phase;

                }
            }

            minPhaseDistance = 101;
            LastmaxPhaseIndex = 102;
            for (int i = 0; i < ((int)LM_Data._points.Count); i++)
            {
                if ((LM_Data._points[i].I_phase - MaxPhaseCurrent_mA >= 0) && (LM_Data._points[i].I_phase - MaxPhaseCurrent_mA <= minPhaseDistance) && i < LastmaxPhaseIndex)
                {
                    End_Iphase_Index = i;
                    LastmaxPhaseIndex = i;
                    minPhaseDistance = LM_Data._points[i].I_phase - MaxPhaseCurrent_mA;
                }
            }
        }
        private void RearSoaTunning(CDSDBROperatingPoint pt_on_middle_line)
        {
            int Max_tune_count = 10;
            int TuneCount = 0;
            double Irearsoa_mA = fcu.IRearSoa_mA;// Math.Round(fcu.IRearSoa_mA, 0);//
            double Irearsoa_mA_LastGood = Irearsoa_mA;
            Bookham.TestSolution.IlmzCommonUtils.DsdbrUtils.LockerCurrents lc = Bookham.TestSolution.IlmzCommonUtils.DsdbrUtils.ReadLockerCurrents();
            Bookham.TestSolution.IlmzCommonUtils.DsdbrUtils.LockerCurrents LastGoodLocker = lc;
            double LockerCurrent_Slope = double.Parse(this.ProgramSettings.GetStringParam("LockerCurrent_Slope"));
            double LockerCurrent_mA, LockerCurrent_mA_new, Irearsoa_mA_new; 
            double Delt_LockerCurrent_mA = 999;
            LockerCurrent_mA_new =lc.LockRatio<1? lc.TxCurrent_mA:lc.RxCurrent_mA;
            Irearsoa_mA_new = Irearsoa_mA;
            
            while ((LockerCurrent_mA_new > this.Max_LockerCurrent_mA||LockerCurrent_mA_new<this.Min_LockerCurrent_mA)
                    && TuneCount < Max_tune_count)
            {
                LockerCurrent_mA = LockerCurrent_mA_new;
                Irearsoa_mA = Irearsoa_mA_new;
                double Delta_Irearsoa = (this.Target_LockerCurrent_mA - Math.Abs(LockerCurrent_mA)) / LockerCurrent_Slope;

                Irearsoa_mA_new = Irearsoa_mA + Delta_Irearsoa;
                Irearsoa_mA_new = Math.Max(this.Min_RearSoa_mA, Irearsoa_mA_new);
                Irearsoa_mA_new = Math.Min(this.Max_RearSoa_mA, Irearsoa_mA_new);
                if (Irearsoa_mA_new == Irearsoa_mA) break;

                fcu.IRearSoa_mA = Irearsoa_mA_new;
                System.Threading.Thread.Sleep(5);
                lc = Bookham.TestSolution.IlmzCommonUtils.DsdbrUtils.ReadLockerCurrents();
                LockerCurrent_mA_new = lc.LockRatio < 1 ? lc.TxCurrent_mA : lc.RxCurrent_mA;

                double Delt_Irx_mA_new = Math.Abs(this.Target_LockerCurrent_mA - LockerCurrent_mA_new);
                if (Delt_Irx_mA_new < Delt_LockerCurrent_mA)//save the lastgood point which delt locker current  is close to zero.Jack.zhang 2013-03-14
                {
                    Delt_LockerCurrent_mA = Delt_Irx_mA_new;
                    Irearsoa_mA_LastGood = Irearsoa_mA_new;
                    LastGoodLocker = lc;
                }
                double lockercurrent_slope_new = (LockerCurrent_mA - LockerCurrent_mA_new) / (Irearsoa_mA - Irearsoa_mA_new);
                if (lockercurrent_slope_new > 0)//to avoid the wrong cal Irearsoa come from Itx measured error between two close points. jack.zhang 2012-11-09
                    LockerCurrent_Slope = lockercurrent_slope_new;
                
                TuneCount++;
            }
            pt_on_middle_line._DUT_Fine_locker_Etalon_mA = LastGoodLocker.TxCurrent_mA;
            pt_on_middle_line._DUT_Fine_locker_Reference_mA = LastGoodLocker.RxCurrent_mA;
            pt_on_middle_line._I_rearsoa_Fine_mA = Irearsoa_mA_LastGood;
            fcu.IRearSoa_mA = Irearsoa_mA_LastGood;
        }
        private bool measure_F_Pr_Lr(ref double F, ref double power_ratio, ref double locker_ratio, short num_F_samples_to_average)
        {
            bool measureOk = true;
            int freq_samples = 0;

            short max_j = num_F_samples_to_average;
            double frequency = 0;
            F = 0;

            //outLine_Tx.LineState = (this.useDUTEtalon) ? IlmzOpticalSwitchLines.Dut_Tx_DigiIoLine_State : IlmzOpticalSwitchLines.Etalon_Tx_DigiIoLine_State;
            //outLine_Rx.LineState = (this.useDUTEtalon) ? IlmzOpticalSwitchLines.Dut_Rx_DigiIoLine_State : IlmzOpticalSwitchLines.Etalon_Rx_DigiIoLine_State;

            // Single out the power_ratio & locker_ratio reading method - chongjian.liang 2016.6.20

            try
            {
                power_ratio = Bookham.TestSolution.IlmzCommonUtils.DsdbrUtils.ReadPowerRatio(true, true, false, num_F_samples_to_average > 1); // Reconstruct by chongjian.liang 2013.6.13
                locker_ratio = Bookham.TestSolution.IlmzCommonUtils.DsdbrUtils.ReadLockerCurrents(true, !this.useDUTEtalon, true, false, num_F_samples_to_average > 1).LockRatio; // Reconstruct by chongjian.liang 2013.6.11; Jack.zhang for FCU cal data wrong Alvan Cao 2011.05.26
            }
            catch (Exception)
            {
                //invalid_frequency_from_wavemeter;
                measureOk = false;
            }

            for (short j = 0; j < max_j; j++)
            {
                if (freq_samples < num_F_samples_to_average)
                {
                    // Take a frequency sample
                    try
                    {
                        frequency = measure_Freq();
                    }
                    catch (Exception)
                    {   //invalid_frequency_from_wavemeter;
                        measureOk = false;
                        break;
                        //throw;
                    }

                    if (frequency < MIN_VALID_FREQ_MEAS
                     || frequency > MAX_VALID_FREQ_MEAS)
                    {
                        //invalid_frequency_from_wavemeter
                        break;
                    }

                    F += frequency;
                    freq_samples++;
                }
            }

            F /= (double)freq_samples;

            return measureOk;
        }
        private bool measure_Pr_Lr(ref double power_ratio, ref double locker_ratio, short num_F_samples_to_average)
        {
            #region Comment out the below lines - chongjian.liang 2013.6.20
            //int freq_samples = 0;
            //short max_j = num_F_samples_to_average;
            //double power_ratio = 0;
            //double Tx_mA = 1, Rx_mA = 0, locker_ratio = 0;
            //Pr = 0; Lr = 0;

            //outLine_Tx.LineState = (this.useDUTEtalon) ? IlmzOpticalSwitchLines.Dut_Tx_DigiIoLine_State : IlmzOpticalSwitchLines.Etalon_Tx_DigiIoLine_State;
            //outLine_Rx.LineState = (this.useDUTEtalon) ? IlmzOpticalSwitchLines.Dut_Rx_DigiIoLine_State : IlmzOpticalSwitchLines.Etalon_Rx_DigiIoLine_State;

            //for (short j = 0; j < max_j; j++)
            //{
            //    if (freq_samples < num_F_samples_to_average)
            //    {
            // Take a frequency sample

            //Pr += power_ratio;
            //Lr += locker_ratio;
            //        freq_samples++;                    
            //    }
            //}
            //Pr /= (double)freq_samples;
            //Lr /= (double)freq_samples;
            //fcu.ISoa_mA = (float)Soa_PowerLeveling_mA; 
            #endregion

            // Reconstruct with below lines - chongjian.liang 2013.6.20

            bool measureOk = true;

            try
            {
                power_ratio = Bookham.TestSolution.IlmzCommonUtils.DsdbrUtils.ReadPowerRatio(true, true, false, num_F_samples_to_average > 1); // Reconstruct by chongjian.liang 2013.6.13
                locker_ratio = Bookham.TestSolution.IlmzCommonUtils.DsdbrUtils.ReadLockerCurrents(true, !this.useDUTEtalon, true, false, num_F_samples_to_average > 1).LockRatio; // Reconstruct by chongjian.liang 2013.6.11; Jack.zhang for FCU cal data wrong Alvan Cao 2011.05.26
            }
            catch (Exception)
            {
                measureOk = false;
            }

            return measureOk;
        }
        public void setup(string testtime, string laserid, string ITUfilepath)
        {
            this.ITUPointsfilePath = ITUfilepath;
            this.TestTimeStamp = testtime;
            this.Laser_id = laserid;
        }

        #region private members
        //��ȡwavemeter��Ƶ��ֵ
        private double measure_Freq()
        {
            if (settle_time_ms > 0)
                System.Threading.Thread.Sleep(settle_time_ms);
			// by tim to combine solution
            return Bookham.TestSolution.IlmzCommonUtils.Measurements.Wavemeter.Frequency_GHz;
        }

        //search all itu points in range
        private void newITUPointSearchRoutine(double frequency_at_first_pt, double min_measured_freq_at_first_pt, double max_measured_freq_at_first_pt,
            double frequency_at_last_pt, double min_measured_freq_at_last_pt, double max_measured_freq_at_last_pt, CDSDBRSupermodeMapLine i_lm,
            CDSDBRSuperMode i_sm, int sm, int lm, int start_index, int end_index)
        {
            int insert_order = 0;
            double freq_i = 0;
            double max_measured_freq = 0;
            double min_measured_freq = 0;
            double wavemeter_error_ghz = FrequencyTolerance_GHz;
            bool returnValue = true;
            bool canAddInto = false;
            List<Pair<double, double>> i_and_min_measured_freq = new List<Pair<double, double>>();
            List<Pair<double, double>> i_and_max_measured_freq = new List<Pair<double, double>>();
            int phase_power_index = int.Parse(characterizationSetting["PhaseLowerIndex"]); ;
            int phase_upper_index = int.Parse(characterizationSetting["PhaseUpperIndex"]); ;

            List<Pair<double, double>> freq_and_i = new List<Pair<double, double>>();
            bool is_overall_size = false;
            bool can_use_previous_info = false;
            freq_and_i.Clear();
            //_Freq_pts_on_freq_lines.Clear();

            canAddInto = CanAddIntoPairListUniqueness(freq_and_i, new Pair<double, double>(frequency_at_first_pt, (double)start_index));
            if (canAddInto)
            {
                freq_and_i.Add(new Pair<double, double>(frequency_at_first_pt, (double)start_index));
            }
            canAddInto = CanAddIntoPairListUniqueness(freq_and_i, new Pair<double, double>(frequency_at_last_pt, (double)end_index));
            if (canAddInto)
            {
                freq_and_i.Add(new Pair<double, double>(frequency_at_last_pt, (double)end_index));
            }
            lm_freq_line_point_type2 new_pt = new lm_freq_line_point_type2();
            new_pt.sm = sm;
            new_pt.lm = lm;
            new_pt.i = (double)start_index;
            new_pt.insert_order = insert_order;


            _Freq_pts_on_freq_lines.Add(new Pair<double, lm_freq_line_point_type2>(frequency_at_first_pt, new_pt));
            insert_order++;

            new_pt = new lm_freq_line_point_type2();
            new_pt.sm = sm;
            new_pt.lm = lm;
            new_pt.i = (double)end_index;
            new_pt.insert_order = insert_order;

            _Freq_pts_on_freq_lines.Add(new Pair<double, lm_freq_line_point_type2>(frequency_at_last_pt, new_pt));
            insert_order++;
            double min_freq = 0;
            double max_freq = 0;
            if (frequency_at_last_pt > frequency_at_first_pt)
            {
                min_freq = frequency_at_first_pt - wavemeter_error_ghz;
                max_freq = frequency_at_last_pt + wavemeter_error_ghz;
            }
            else
            {
                min_freq = frequency_at_last_pt - wavemeter_error_ghz;
                max_freq = frequency_at_first_pt + wavemeter_error_ghz;
            }

            List<int> channel_num_of_itu_pts_in_lm = new List<int>();
            List<double> freq_of_itu_pts_in_lm = new List<double>();
            _itugrid.findITUPtsInRange(channel_num_of_itu_pts_in_lm, freq_of_itu_pts_in_lm, min_freq, max_freq);

            #region use previous info

            if ((int)i_lm._points.Count - 1 < MAX_ITERATIONS_TO_FIND_ITU_PT)
                is_overall_size = false;
            else
                is_overall_size = true;
            int minChannel_num_in_lm = 100; //100 is big enough value, it will be replaced by first comparison
            if (channel_num_of_itu_pts_in_lm.Count == 0 && (countITUOPsFound() != 0) &&
                (min_freq < _itugrid.getMin_freq() || max_freq > _itugrid.getMax_freq()))
            {
                //ingore the rest lm in this supermode
                this._IgnoreRestLM = true;
                return;
            }
            if (is_overall_size)
            {
                for (int i = 0; i < channel_num_of_itu_pts_in_lm.Count; i++)
                {
                    if (channel_num_of_itu_pts_in_lm[i] < minChannel_num_in_lm)
                        minChannel_num_in_lm = channel_num_of_itu_pts_in_lm[i];
                }
                int estimateItuPoints_num = channel_num_of_itu_pts_in_lm.Count;

                can_use_previous_info = CanUsePreviousItuPtsInfo(_characterisation_itu_ops, sm, lm, minChannel_num_in_lm, estimateItuPoints_num);
            }

            if (can_use_previous_info)
            {
                //when can_use_previous_info == true, is_overall_size must be true,because can_use_previous_info is set to be true, only when 
                // is_over_all ==true
                //find  the previous characterizeinfo
                bool successFound = false;
                Pair<double, double> pre_col_lm = new Pair<double, double>();
                Pair<double, double> pre2_col_lm = new Pair<double, double>();
                successFound = findItuColIndexInLm(_characterisation_itu_ops, pre_col_lm, sm, lm - 1, minChannel_num_in_lm - 1);
                if (successFound)
                    successFound = findItuColIndexInLm(_characterisation_itu_ops, pre2_col_lm, sm, lm - 2, minChannel_num_in_lm - 2);
                if (successFound)
                {
                    //ok all things seems like going well , add the expected point to the freq line
                    double insertPoint_ColIndex = pre_col_lm.First - (pre2_col_lm.First - pre_col_lm.First);
                    double insertPoint_ColIndex2 = pre_col_lm.Second - (pre2_col_lm.Second - pre_col_lm.Second);
                    if (insertPoint_ColIndex > start_index && insertPoint_ColIndex < end_index)
                    {
                        //add this point to the freq line
                        CDSDBROperatingPoint test_op = new CDSDBROperatingPoint(i_sm, i_lm, sm, lm, insertPoint_ColIndex);
                        //if (IsByAsic)
                        //{
                        //    test_op.ReCountRearSoa(slope_rearsoa, constant_rear_rearsoa);
                        //    returnValue = test_op.ApplyByASIC(fcu, slope_rearsoa, constant_rear_rearsoa);
                        //}
                        //else
                        test_op.ReCountRearSoa(slope_rearsoa, constant_rear_rearsoa,Max_RearSoa_mA,Min_RearSoa_mA);//Echo new added
                            returnValue = test_op.Apply(fcu);
                        if (returnValue == true)
                        {
                            // take a frequency measurement
                            returnValue = measure_F_P_Pr(ref freq_i, ref min_measured_freq, ref max_measured_freq,
                               short.Parse(characterizationSetting["NumberOfFrequencyMeasurementsToAveragePerOperatingPoint"]));
                            if (returnValue == true)
                            {
                                new_pt.sm = sm;
                                new_pt.lm = lm;
                                new_pt.i = insertPoint_ColIndex;
                                new_pt.insert_order = insert_order;
                                // For each point on each lm of each sm, insert the power ratio and the point to a map.
                                // On insertion, the points in the multimap are, automatically sorted by Pr value.
                                _Freq_pts_on_freq_lines.Add(new Pair<double, lm_freq_line_point_type2>(freq_i, new_pt));
                                insert_order++;
                                canAddInto = CanAddIntoPairListUniqueness(freq_and_i, new Pair<double, double>(freq_i, insertPoint_ColIndex));
                                if (canAddInto)
                                {
                                    freq_and_i.Add(new Pair<double, double>(freq_i, insertPoint_ColIndex));
                                }
                                canAddInto = CanAddIntoPairListUniqueness(i_and_min_measured_freq, new Pair<double, double>(insertPoint_ColIndex, min_measured_freq));
                                if (canAddInto)
                                {
                                    i_and_min_measured_freq.Add(new Pair<double, double>(insertPoint_ColIndex, min_measured_freq));
                                }
                                canAddInto = CanAddIntoPairListUniqueness(i_and_max_measured_freq, new Pair<double, double>(insertPoint_ColIndex, max_measured_freq));
                                if (canAddInto)
                                {
                                    i_and_max_measured_freq.Add(new Pair<double, double>(insertPoint_ColIndex, max_measured_freq));
                                }

                            } // end of if !error from measure_F_P_Pr
                        } // end of if !error from test_op.apply()
                    }
                    if (insertPoint_ColIndex2 > start_index && insertPoint_ColIndex2 < end_index && Math.Abs(insertPoint_ColIndex2 - insertPoint_ColIndex) > MIN_PT_GAP_TO_FIND_ITU_PT)
                    {
                        //add this point the freqline
                        CDSDBROperatingPoint test_op = new CDSDBROperatingPoint(i_sm, i_lm, sm, lm, insertPoint_ColIndex2);
                        //if (IsByAsic)
                        //{
                        //    test_op.ReCountRearSoa(slope_rearsoa, constant_rear_rearsoa);
                        //    returnValue = test_op.ApplyByASIC(fcu, slope_rearsoa, constant_rear_rearsoa);
                        //}
                        //else
                        test_op.ReCountRearSoa(slope_rearsoa, constant_rear_rearsoa,Max_RearSoa_mA,Min_RearSoa_mA);//Echo new added
                            returnValue = test_op.Apply(fcu);

                        if (returnValue == true)
                        {

                            // take a frequency measurement
                            returnValue = measure_F_P_Pr(ref freq_i, ref min_measured_freq, ref max_measured_freq,
                                short.Parse(characterizationSetting["NumberOfFrequencyMeasurementsToAveragePerOperatingPoint"]));
                            if (returnValue == true)
                            {
                                new_pt.sm = sm;
                                new_pt.lm = lm;
                                new_pt.i = insertPoint_ColIndex2;
                                new_pt.insert_order = insert_order;
                                // For each point on each lm of each sm, insert the power ratio and the point to a map.
                                // On insertion, the points in the multimap are, automatically sorted by Pr value.
                                _Freq_pts_on_freq_lines.Add(new Pair<double, lm_freq_line_point_type2>(freq_i, new_pt));
                                insert_order++;
                                canAddInto = CanAddIntoPairListUniqueness(freq_and_i, new Pair<double, double>(freq_i, insertPoint_ColIndex2));
                                if (canAddInto)
                                {
                                    freq_and_i.Add(new Pair<double, double>(freq_i, insertPoint_ColIndex2));
                                }
                                canAddInto = CanAddIntoPairListUniqueness(i_and_min_measured_freq, new Pair<double, double>(insertPoint_ColIndex2, min_measured_freq));
                                if (canAddInto)
                                {
                                    i_and_min_measured_freq.Add(new Pair<double, double>(insertPoint_ColIndex2, min_measured_freq));
                                }
                                canAddInto = CanAddIntoPairListUniqueness(i_and_max_measured_freq, new Pair<double, double>(insertPoint_ColIndex2, max_measured_freq));
                                if (canAddInto)
                                {
                                    i_and_max_measured_freq.Add(new Pair<double, double>(insertPoint_ColIndex2, max_measured_freq));
                                }
                            } // end of if !error from measure_F_P_Pr
                        } // end of if !error from test_op.apply()
                    }
                }
            }//can_use_previous_info
            #endregion

            int itusearchScheme = int.Parse(characterizationSetting["ITUSearchScheme"]);
            itusearch_scheme_type itusearch_scheme = (itusearch_scheme_type)itusearchScheme;
            for (int i = 0; i < (int)channel_num_of_itu_pts_in_lm.Count; i++)
            {
                // for each ITU point to be found,start with best guess,   
                //    then iterate to ITU point within specified accuracy of wavemeter
                //    record the ITU point's details
                double resample_pointA = 0;
                double resample_freqA = 0;
                double resample_pointB = 0;
                double resample_freqB = 0;
                int resample_N = 0;
                bool out_of_boundary = false;
                bool problem_with_freq_and_i_table = false;
                bool problem_with_hardware = false;
                int num_iterations_with_problem = 0;
                int num_iterations_without_problem = 0;
                double guess_i = 0;
                double last_guess_i = -1;
                int channel_num = channel_num_of_itu_pts_in_lm[i];
                double freq_of_channel_num = freq_of_itu_pts_in_lm[i];
                double prev_recorded_freq_i = 0;
                int count_iterations;
                double freq_upper = 0;
                double point_upper = 0;
                double freq_lower = 0;
                double point_lower = 0;
                int itusearch_nextguess = int.Parse(characterizationSetting["ITUSearchNextGuessType"]);
                IRearSoa_Fine_mA = 0;
                
                #region For front pair switch issue - chongjian.liang 2016.08.24
                
                short fixed_frontpair = 0;
                double fixed_noncost_fs_mA = 0;
                double prev_freq_GHz = 0;
                short prev_frontpair = 0;
                double prev_noncost_fs_mA = 0;
                bool is_switch_fs = false;
                bool is_fs_fixed = false;
                double fs_fixed_phase_index_increment = 1;
                PhaseDirection prev_phase_direction = PhaseDirection.Forward;
                
                #endregion
                
                for (count_iterations = 0; count_iterations < MAX_ITERATIONS_TO_FIND_ITU_PT; count_iterations++)
                {
                    bool end_this_itupoint_search = false;
                    #region switch problems deal with
                    // This loop is trying to find a single ITU point
                    if (num_iterations_with_problem > 0)
                    {   // deal with problems
                        switch (itusearch_scheme)
                        {
                            case itusearch_scheme_type.quit_itupoint_after_problem:
                                // If any problem is encountered, simply move on to next ITU point
                                end_this_itupoint_search = true;
                                break;
                            case itusearch_scheme_type.remove_upper_lower_after_problem:
                                // If any problem is encountered,
                                // remove the upper and lower points used and try again
                                removeUpperAndLowerButNotEndPoints(
                                    freq_of_channel_num,
                                    (double)start_index,
                                    (double)end_index,
                                    freq_and_i,
                                    i_and_min_measured_freq,  // it is null, can_use_prev is not use.
                                    i_and_max_measured_freq);
                                break;
                            case itusearch_scheme_type.resample_upper_lower_after_problem:
                                // If any problem is encountered,
                                // remove the upper and lower points used resample them, and try again
                                //selected resample_upper_lower_after_problem
                                findUpperAndLower(
                                    freq_of_channel_num,
                                    freq_and_i,
                                    out resample_freqA, out resample_pointA,
                                    out resample_freqB, out resample_pointB);
                                removeUpperAndLower(
                                    freq_of_channel_num,
                                    freq_and_i,
                                    i_and_min_measured_freq,
                                    i_and_max_measured_freq);
                                resample_N = 2;
                                returnValue = testNSamples(freq_and_i, i_and_min_measured_freq, i_and_max_measured_freq,
                                     resample_N, resample_pointA, resample_pointB, i_lm, i_sm, sm, lm, insert_order);

                                if (returnValue == false)
                                    end_this_itupoint_search = true;
                                break;

                            case itusearch_scheme_type.remove_upper_lower_find_N_samples_after_problem:
                                // If any problem is encountered,
                                // remove the upper and lower points used
                                // Take N (min 2) new samples between the upper and lower and try again  
                                // selected remove_upper_lower_find_N_samples_after_problem
                                findUpperAndLower(freq_of_channel_num, freq_and_i, out resample_freqA, out resample_pointA,
                                    out resample_freqB, out resample_pointB);
                                removeUpperAndLower(freq_of_channel_num, freq_and_i, i_and_min_measured_freq, i_and_max_measured_freq);

                                resample_N = int.Parse(characterizationSetting["ITUSearchScheme"]);
                                if (resample_N < 2) resample_N = 2;
                                returnValue = testNSamples(freq_and_i, i_and_min_measured_freq,  // it's not used now
                                   i_and_max_measured_freq, resample_N, resample_pointA, resample_pointB, i_lm, i_sm, sm, lm, insert_order);

                                if (returnValue == false)
                                    end_this_itupoint_search = true;
                                break;
                            case itusearch_scheme_type.bisection_search_after_problem:
                                // If any problem is encountered,remove all except start and end points
                                // and try again using bisection instead of interpolation
                                itusearch_nextguess = (int)itusearch_nextguess_type.bisection;
                                freq_and_i.Clear();
                                i_and_min_measured_freq.Clear();
                                i_and_max_measured_freq.Clear();

                                canAddInto = CanAddIntoPairListUniqueness(freq_and_i, new Pair<double, double>(frequency_at_first_pt, (double)start_index));
                                if (canAddInto)
                                {
                                    freq_and_i.Add(new Pair<double, double>(frequency_at_first_pt, (double)start_index));
                                }
                                canAddInto = CanAddIntoPairListUniqueness(i_and_min_measured_freq, new Pair<double, double>((double)start_index, min_measured_freq_at_first_pt));
                                if (canAddInto)
                                {
                                    i_and_min_measured_freq.Add(new Pair<double, double>((double)start_index, min_measured_freq_at_first_pt));
                                }
                                canAddInto = CanAddIntoPairListUniqueness(i_and_max_measured_freq, new Pair<double, double>((double)start_index, max_measured_freq_at_first_pt));
                                if (canAddInto)
                                {
                                    i_and_max_measured_freq.Add(new Pair<double, double>((double)start_index, max_measured_freq_at_first_pt));
                                }

                                canAddInto = CanAddIntoPairListUniqueness(freq_and_i, new Pair<double, double>(frequency_at_last_pt, (double)end_index));
                                if (canAddInto)
                                {
                                    freq_and_i.Add(new Pair<double, double>(frequency_at_last_pt, (double)end_index));
                                }
                                canAddInto = CanAddIntoPairListUniqueness(i_and_min_measured_freq, new Pair<double, double>((double)end_index, min_measured_freq_at_last_pt));
                                if (canAddInto)
                                {
                                    i_and_min_measured_freq.Add(new Pair<double, double>((double)end_index, min_measured_freq_at_last_pt));
                                }
                                canAddInto = CanAddIntoPairListUniqueness(i_and_max_measured_freq, new Pair<double, double>((double)end_index, max_measured_freq_at_last_pt));
                                if (canAddInto)
                                {
                                    i_and_max_measured_freq.Add(new Pair<double, double>((double)end_index, max_measured_freq_at_last_pt));
                                }
                                break;
                            case itusearch_scheme_type.brute_force_search_after_problem:
                                // If any problem is encountered,remove all except start and end points
                                // Take N (min 2) new samples between start and end points and try again 
                                freq_and_i.Clear();
                                i_and_min_measured_freq.Clear();
                                i_and_max_measured_freq.Clear();

                                canAddInto = CanAddIntoPairListUniqueness(freq_and_i, new Pair<double, double>(frequency_at_first_pt, (double)start_index));
                                if (canAddInto)
                                {
                                    freq_and_i.Add(new Pair<double, double>(frequency_at_first_pt, (double)start_index));
                                }
                                canAddInto = CanAddIntoPairListUniqueness(i_and_min_measured_freq, new Pair<double, double>((double)start_index, min_measured_freq_at_first_pt));
                                if (canAddInto)
                                {
                                    i_and_min_measured_freq.Add(new Pair<double, double>((double)start_index, min_measured_freq_at_first_pt));
                                }
                                canAddInto = CanAddIntoPairListUniqueness(i_and_max_measured_freq, new Pair<double, double>((double)start_index, max_measured_freq_at_first_pt));
                                if (canAddInto)
                                {
                                    i_and_max_measured_freq.Add(new Pair<double, double>((double)start_index, max_measured_freq_at_first_pt));
                                }

                                canAddInto = CanAddIntoPairListUniqueness(freq_and_i, new Pair<double, double>(frequency_at_last_pt, (double)end_index));
                                if (canAddInto)
                                {
                                    freq_and_i.Add(new Pair<double, double>(frequency_at_last_pt, (double)end_index));
                                }
                                canAddInto = CanAddIntoPairListUniqueness(i_and_min_measured_freq, new Pair<double, double>((double)end_index, min_measured_freq_at_last_pt));
                                if (canAddInto)
                                {
                                    i_and_min_measured_freq.Add(new Pair<double, double>((double)end_index, min_measured_freq_at_last_pt));
                                }
                                canAddInto = CanAddIntoPairListUniqueness(i_and_max_measured_freq, new Pair<double, double>((double)end_index, max_measured_freq_at_last_pt));
                                if (canAddInto)
                                {
                                    i_and_max_measured_freq.Add(new Pair<double, double>((double)end_index, max_measured_freq_at_last_pt));
                                }

                                resample_N = int.Parse(characterizationSetting["ITUSearchScheme"]);
                                if (resample_N < 2) resample_N = 2;

                                returnValue = testNSamples(
                                    freq_and_i,
                                    i_and_min_measured_freq,
                                    i_and_max_measured_freq,
                                    resample_N,
                                    (double)start_index, // first point
                                    (double)end_index, // last point
                                    i_lm,
                                    i_sm,
                                    sm,
                                    lm,
                                    insert_order);
                                if (returnValue == false)
                                    end_this_itupoint_search = true;
                                break;
                            default:
                                //Stopping ITU point search, requested itusearch scheme unknown"
                                end_this_itupoint_search = true;
                                break;
                        }//switch
                        // Assume problem is sorted out
                        problem_with_hardware = false;
                        problem_with_freq_and_i_table = false;
                        out_of_boundary = false;
                        if (num_iterations_with_problem > MAX_ITERATIONS_WITH_PROBLEM)
                        { // looks like the solution is not forthcoming, quit the ITU point search
                            end_this_itupoint_search = true;
                        }
                    }//if
                    #endregion

                    if (!end_this_itupoint_search)
                    {
                        bool found_ITUpoint_without_guess = false;

                        findUpperAndLower(freq_of_channel_num, freq_and_i,
                            out freq_upper, out point_upper, out freq_lower, out  point_lower);

                        if (Math.Abs(freq_lower - freq_upper) <= 1)
                        {
                            end_this_itupoint_search = true;
                            //set the break flat, the next loop would break the "For loop"
                            continue;
                        }
                        if (Math.Abs(freq_lower - freq_of_channel_num) <= wavemeter_error_ghz)
                        {
                            found_ITUpoint_without_guess = true;
                        }
                        if (Math.Abs(freq_upper - freq_of_channel_num) <= wavemeter_error_ghz)
                        {
                            found_ITUpoint_without_guess = true;
                        }

                        #region check problems
                        if (!found_ITUpoint_without_guess && freq_upper <= freq_lower)
                        {
                            //freq_upper <= freq_lower"
                            problem_with_freq_and_i_table = true; // deal with problem at start of next iteration
                        }
                        if (!found_ITUpoint_without_guess && Math.Abs(point_upper - point_lower) < MIN_PT_GAP_TO_FIND_ITU_PT)
                        {   //fabs(point_upper - point_lower) < MIN GAP 
                            problem_with_freq_and_i_table = true; // deal with problem at start of next iteration
                        }
                        if (!found_ITUpoint_without_guess && freq_of_channel_num > freq_upper)
                        {   //freq_of_channel_num > freq_upper
                            problem_with_freq_and_i_table = true; // deal with problem at start of next iteration
                        }
                        if (!found_ITUpoint_without_guess && freq_of_channel_num < freq_lower)
                        {   //freq_of_channel_num < freq_lower"
                            problem_with_freq_and_i_table = true; // deal with problem at start of next iteration
                        }
                        #endregion

                        if (!found_ITUpoint_without_guess && !problem_with_freq_and_i_table)
                        {
                            if (is_fs_fixed)
                            {
                                #region Add logic to find phase index close to ITU - chongjian.liang 2014.4.27

                                if (prev_freq_GHz < freq_of_channel_num)
                                {
                                    if (prev_phase_direction == PhaseDirection.Backward)
                                    {
                                        fs_fixed_phase_index_increment /= 2;
                                    }

                                    prev_phase_direction = PhaseDirection.Forward;

                                    guess_i = point_lower + fs_fixed_phase_index_increment;
                                }
                                else
                                {
                                    if (prev_phase_direction == PhaseDirection.Forward)
                                    {
                                        fs_fixed_phase_index_increment /= 2;
                                    }

                                    prev_phase_direction = PhaseDirection.Backward;

                                    guess_i = point_lower - fs_fixed_phase_index_increment;
                                } 
                                #endregion
                            }
                            else if ((itusearch_nextguess_type)itusearch_nextguess == itusearch_nextguess_type.bisection)
                            {
                                // Calculate bisection point
                                guess_i = 0.5 * (point_upper - point_lower) + point_lower;
                            }
                            else
                            {
                                // Calculate interpolation point
                                double fraction_from_lower = (freq_of_channel_num - freq_lower) / (freq_upper - freq_lower);
                                guess_i = fraction_from_lower * (point_upper - point_lower) + point_lower;
                            }
                            if (Math.Abs(guess_i - last_guess_i) < MIN_PT_GAP_TO_FIND_ITU_PT)
                            {
                                // too small a gap, can't find ITU point, move on to next.
                                problem_with_freq_and_i_table = true; // deal with problem at start of next iteration
                            }
                            if (i_lm._points[(int)Math.Ceiling(guess_i)].col < phase_power_index && is_overall_size || i_lm._points[(int)guess_i].col > phase_upper_index && is_overall_size) // chongjian.liang 2016.08.24
                            //if (guess_i < phase_power_index && is_overall_size || guess_i > phase_upper_index && is_overall_size)
                            {
                                out_of_boundary = true;
                            }
                            if (!problem_with_freq_and_i_table && !out_of_boundary)
                            {
                                last_guess_i = guess_i;
                                CDSDBROperatingPoint test_op = new CDSDBROperatingPoint(i_sm, i_lm, sm, lm, guess_i);

                                #region Handle front pair switch issue - chongjian.liang 2016.08.24
                                if (is_fs_fixed)
                                {
                                    test_op._front_pair_number = fixed_frontpair;
                                    test_op._nonConstantCurrent = fixed_noncost_fs_mA;
                                    is_switch_fs = false;
                                }
                                else
                                {
                                    if (count_iterations != 0 && prev_frontpair != test_op._front_pair_number)
                                    {
                                        is_switch_fs = true;
                                    }
                                    else
                                    {
                                        is_switch_fs = false;
                                    }
                                } 
                                #endregion

                                if (IRearSoa_Fine_mA == 0)
                                    test_op.ReCountRearSoa(slope_rearsoa, constant_rear_rearsoa, Max_RearSoa_mA, Min_RearSoa_mA);
                                else
                                    test_op._I_rearsoa = IRearSoa_Fine_mA;
                                    returnValue = test_op.Apply(fcu);
                                if (returnValue == false)
                                {
                                    problem_with_hardware = true;
                                }
                                if (problem_with_hardware == false)
                                {

                                    // take a frequency measurement
                                    returnValue = measure_F_P_Pr(ref freq_i, ref min_measured_freq, ref max_measured_freq,
                                          short.Parse(characterizationSetting["NumberOfFrequencyMeasurementsToAveragePerOperatingPoint"]));

                                    #region Fix front section when front pair switch - chongjian.liang 2014.4.24

                                    if (is_switch_fs)
                                    {
                                        // If the gap of front section switch is too big hit the ITU - chongjian.liang 2014.4.24
                                        if (Math.Abs(freq_i - freq_of_channel_num) > wavemeter_error_ghz)
                                        {
                                            double freq_to_itu = Math.Abs(freq_i - freq_of_channel_num);
                                            double prev_freq_to_itu = Math.Abs(prev_freq_GHz - freq_of_channel_num);

                                            // The gap is too big to hit the ITU - chongjian.liang 2014.4.24
                                            if (Math.Min(freq_to_itu, prev_freq_to_itu) < 10)
                                            {
                                                is_fs_fixed = true;

                                                if (freq_to_itu < prev_freq_to_itu)
                                                {
                                                    prev_phase_direction = PhaseDirection.Backward;
                                                    fixed_frontpair = test_op._front_pair_number;
                                                    fixed_noncost_fs_mA = test_op._nonConstantCurrent;
                                                }
                                                else
                                                {
                                                    prev_phase_direction = PhaseDirection.Forward;
                                                    fixed_frontpair = prev_frontpair;
                                                    fixed_noncost_fs_mA = prev_noncost_fs_mA;
                                                }
                                            }
                                        }
                                    }

                                    prev_frontpair = test_op._front_pair_number;
                                    prev_noncost_fs_mA = test_op._nonConstantCurrent;
                                    prev_freq_GHz = freq_i; 
                                    #endregion

                                    if (returnValue == false)
                                    {
                                        problem_with_hardware = true;
                                    }
                                    if (problem_with_hardware == false)
                                    {

                                        new_pt = new lm_freq_line_point_type2();
                                        new_pt.sm = sm;
                                        new_pt.lm = lm;
                                        new_pt.i = guess_i;
                                        new_pt.insert_order = insert_order;
                                        _Freq_pts_on_freq_lines.Add(new Pair<double, lm_freq_line_point_type2>(freq_i, new_pt));
                                        insert_order++;
                                        canAddInto = CanAddIntoPairListUniqueness(freq_and_i, new Pair<double, double>(freq_i, guess_i));
                                        if (canAddInto)
                                        {
                                            freq_and_i.Add(new Pair<double, double>(freq_i, guess_i));
                                        }
                                        canAddInto = CanAddIntoPairListUniqueness(i_and_min_measured_freq, new Pair<double, double>(guess_i, min_measured_freq));
                                        if (canAddInto)
                                        {
                                            i_and_min_measured_freq.Add(new Pair<double, double>(guess_i, min_measured_freq));
                                        }
                                        canAddInto = CanAddIntoPairListUniqueness(i_and_max_measured_freq, new Pair<double, double>(guess_i, max_measured_freq));
                                        if (canAddInto)
                                        {
                                            i_and_max_measured_freq.Add(new Pair<double, double>(guess_i, max_measured_freq));
                                        }
                                    } // end of if !error from measure_F_P_Pr
                                } // end of if !error from test_op.apply()
                            } // end of if(!problem_with_freq_and_i_table)
                        } // end of if(!problem_with_freq_and_i_table)
                        if (found_ITUpoint_without_guess || problem_with_freq_and_i_table || problem_with_hardware)
                        {
                            // use nearest freq to ITU point (upper or lower)
                            // just in case it's good enough!
                            if (Math.Abs(freq_upper - freq_of_channel_num) < Math.Abs(freq_lower - freq_of_channel_num))
                            {
                                freq_i = freq_upper;
                                guess_i = point_upper;
                            }
                            else
                            {
                                freq_i = freq_lower;
                                guess_i = point_lower;
                            }
                            // find previously recorded values
                            Predicate<Pair<double, double>> match = delegate(Pair<double, double> obj)
                            {
                                if (Math.Abs(obj.First - guess_i) < 1e-6)
                                {
                                    return true;
                                }
                                else return false;
                            };
                            int freq_and_i_index = i_and_min_measured_freq.FindIndex(match);
                            if (freq_and_i_index != -1)
                                min_measured_freq = i_and_min_measured_freq[freq_and_i_index].Second;

                            freq_and_i_index = i_and_max_measured_freq.FindIndex(match);
                            if (freq_and_i_index != -1) // found
                                max_measured_freq = i_and_max_measured_freq[freq_and_i_index].Second;
                        }
                        if (count_iterations == MAX_ITERATIONS_TO_FIND_ITU_PT - 1)
                        { // This is the last time though count_iterations loop
                            // ensure last guess is written if duff points requested
                            end_this_itupoint_search = true;
                        }
                        if (out_of_boundary)
                        {
                            end_this_itupoint_search = true;
                        }

                        if (freq_i != prev_recorded_freq_i // prevent the same freq being recorded in a loop
                            && ((!end_this_itupoint_search && Math.Abs(freq_i - freq_of_channel_num) <= wavemeter_error_ghz && !out_of_boundary) // chongjian.liang 2016.08.24
                            //&& (count_iterations != 0 && count_iterations != MAX_ITERATIONS_TO_FIND_ITU_PT) && ((!end_this_itupoint_search && Math.Abs(freq_i - freq_of_channel_num) <= wavemeter_error_ghz && !out_of_boundary)
                            || (end_this_itupoint_search && (!out_of_boundary) && Convert.ToBoolean(Convert.ToInt32(characterizationSetting["ITUSearchFailedWriteLastGuessToFile"])))))
                        {
                            prev_recorded_freq_i = freq_i;
                            if (Math.Abs(freq_i - freq_of_channel_num) > wavemeter_error_ghz)
                            {
                                incrementDuffITUOPCount();
                            }

                            CDSDBROperatingPoint new_itu_operating_pt = new CDSDBROperatingPoint();

                            CDSDBROperatingPoint test_op = new CDSDBROperatingPoint(i_sm, i_lm, sm, lm, guess_i);
                            
                            // Handle front pair switch issue - chongjian.liang 2016.08.24
                            if (is_fs_fixed)
                            {
                                test_op._front_pair_number = fixed_frontpair;
                                test_op._nonConstantCurrent = fixed_noncost_fs_mA;
                            }
                            
                            test_op.ReCountRearSoa(slope_rearsoa, constant_rear_rearsoa,Max_RearSoa_mA,Min_RearSoa_mA);
                            double Pr_on_sm_map = (i_sm._map_forward_photodiode1_current.getValue((int)test_op._row_on_sm_map, (int)test_op._col_on_sm_map));
                            new_itu_operating_pt._ITU_channel_number = channel_num_of_itu_pts_in_lm[i];
                            new_itu_operating_pt._ITU_frequency = freq_of_channel_num;
                            new_itu_operating_pt._row_on_sm_map = test_op._row_on_sm_map;
                            new_itu_operating_pt._col_on_sm_map = test_op._col_on_sm_map;
                            new_itu_operating_pt._Pr_on_sm_map = Pr_on_sm_map;

                            new_itu_operating_pt._sm_number = sm;
                            new_itu_operating_pt._lm_number = lm;
                            new_itu_operating_pt._real_index_on_middle_line_of_frequency = guess_i;
                            new_itu_operating_pt._index_on_middle_line_of_frequency = (int)guess_i;
                            new_itu_operating_pt._front_pair_number = test_op._front_pair_number;
                            new_itu_operating_pt._I_gain = test_op._I_gain;
                            new_itu_operating_pt._I_phase = test_op._I_phase;
                            new_itu_operating_pt._I_soa = test_op._I_soa;
                            new_itu_operating_pt._I_rear = test_op._I_rear;
                            new_itu_operating_pt._I_rearsoa = test_op._I_rearsoa;

                            new_itu_operating_pt._constantCurrent = test_op._constantCurrent;
                            new_itu_operating_pt._nonConstantCurrent = test_op._nonConstantCurrent;
                            new_itu_operating_pt._F_measured = freq_i;

                            if ((max_measured_freq - min_measured_freq) > int.Parse(characterizationSetting["MaxFrequencyError[GHz]AtStableOperatingPoint"]))
                            {
                                new_itu_operating_pt._stable_F_measurement = false;
                            }
                            else
                            {
                                new_itu_operating_pt._stable_F_measurement = true;
                            }

                            if (Math.Abs(freq_i - freq_of_channel_num) <= wavemeter_error_ghz)
                            {
                                new_itu_operating_pt._is_an_ITU_point = true;

                            }
                            else
                            {
                                new_itu_operating_pt._is_an_ITU_point = false;
                            }
                            #region Rearsoa fine tunning
                            if (programSettings.GetBoolParam("DoRearSoaFineTunning"))
                            {
                                if (new_itu_operating_pt._I_rearsoa_Fine_mA == 0)
                                {
                                    Bookham.TestSolution.IlmzCommonUtils.DsdbrUtils.LockerCurrents lc = Bookham.TestSolution.IlmzCommonUtils.DsdbrUtils.ReadLockerCurrents();
                                    new_itu_operating_pt._DUT_locker_Etalon_mA = lc.TxCurrent_mA;
                                    new_itu_operating_pt._DUT_locker_Reference_mA = lc.RxCurrent_mA;
                                }
                                RearSoaTunning(new_itu_operating_pt);
                                IRearSoa_Fine_mA = new_itu_operating_pt._I_rearsoa_Fine_mA;
                                if (Math.Abs(new_itu_operating_pt._I_rearsoa - new_itu_operating_pt._I_rearsoa_Fine_mA) / new_itu_operating_pt._I_rearsoa > 0.1)
                                    returnValue = measure_F_P_Pr(ref freq_i, ref min_measured_freq, ref max_measured_freq,
                                                                          short.Parse(characterizationSetting["NumberOfFrequencyMeasurementsToAveragePerOperatingPoint"]));
                            }
                            else
                                new_itu_operating_pt._I_rearsoa_Fine_mA = new_itu_operating_pt._I_rearsoa;
                            #endregion Rearsoa fine tunning
                            if (Math.Abs(freq_i - freq_of_channel_num) <= wavemeter_error_ghz)
                            {
                                _characterisation_itu_ops.Add(new_itu_operating_pt);
                                break;
                            }
                            else
                            {
                                freq_and_i.RemoveAt(freq_and_i.Count - 1);
                                count_iterations--;
                                last_guess_i = freq_and_i[freq_and_i.Count - 1].Second;
                            }
                        }

                    }//if (!end_this_itupoint_search)
                    else
                    {
                        //break the "for MAX_ITERATIONS_TO_FIND_ITU_PT" loop
                        break;
                    }
                    if (count_iterations == MAX_ITERATIONS_TO_FIND_ITU_PT - 1)
                    { // This is the last time though count_iterations loop
                        // ensure last guess is written if duff points requested
                        end_this_itupoint_search = true;
                    }
                    if (guess_i == 0 && Math.Abs(freq_i - freq_of_channel_num) <= wavemeter_error_ghz || guess_i == MAX_ITERATIONS_TO_FIND_ITU_PT && Math.Abs(freq_i - freq_of_channel_num) <= wavemeter_error_ghz)
                        break;
                    if (end_this_itupoint_search) break;
                    if (problem_with_freq_and_i_table || problem_with_hardware)
                    {
                        num_iterations_with_problem++;
                        num_iterations_without_problem = 0;
                    }
                    else
                    {
                        num_iterations_with_problem = 0;
                        num_iterations_without_problem++;

                        if (num_iterations_without_problem > MAX_ITERATIONS_WITHOUT_PROBLEM)
                        { // Could be stuck in an endless loop, treat as a problem!
                            num_iterations_with_problem++;
                            num_iterations_without_problem = 0;
                            problem_with_freq_and_i_table = true;
                        }
                    }
                    #region not to be used
                    //if (!end_this_itupoint_search && (Math.Abs(freq_i-freq_of_channel_num)<= wavemeter_error_ghz))
                    //{
                    //    CDSDBROperatingPoint new_itu_operating_pt = new CDSDBROperatingPoint();

                    //    CDSDBROperatingPoint test_op = new CDSDBROperatingPoint( i_sm, i_lm, sm, lm, guess_i );
                    //    double Pr_on_sm_map = (i_sm._map_forward_photodiode1_current.getValue((int)test_op._row_on_sm_map, (int)test_op._col_on_sm_map));
                    //    new_itu_operating_pt._ITU_channel_number = channel_num_of_itu_pts_in_lm[i];
                    //    new_itu_operating_pt._ITU_frequency = freq_of_channel_num;
                    //    new_itu_operating_pt._row_on_sm_map = test_op._row_on_sm_map;
                    //    new_itu_operating_pt._col_on_sm_map = test_op._col_on_sm_map;
                    //    new_itu_operating_pt._Pr_on_sm_map = Pr_on_sm_map;
                    //    new_itu_operating_pt._sm_number = sm;
                    //    new_itu_operating_pt._lm_number = lm;
                    //    new_itu_operating_pt._real_index_on_middle_line_of_frequency = guess_i;
                    //    new_itu_operating_pt._index_on_middle_line_of_frequency = (int)guess_i;
                    //    new_itu_operating_pt._front_pair_number = test_op._front_pair_number;
                    //    new_itu_operating_pt._I_gain = test_op._I_gain;
                    //    new_itu_operating_pt._I_phase = test_op._I_phase;
                    //    new_itu_operating_pt._I_soa = test_op._I_soa;
                    //    new_itu_operating_pt._I_rear = test_op._I_rear;
                    //    new_itu_operating_pt._constantCurrent = test_op._constantCurrent;
                    //    new_itu_operating_pt._nonConstantCurrent = test_op._nonConstantCurrent;
                    //    new_itu_operating_pt._F_measured = freq_i;
                    //    _characterisation_itu_ops.Add(new_itu_operating_pt);
                    //    end_this_itupoint_search = true;
                    //}
                    #endregion
                }//for (count_iterations = 0; count_iterations < MAX_ITERATIONS_TO_FIND_ITU_PT; count_iterations++)
            }//for (int i = 0; i < (int)channel_num_of_itu_pts_in_lm.Count; i++)
        }

        private int countITUOPsFound()
        {
            int itu_op_count = 0;

            for (int i = 0; i < (int)(_itugrid.channel_count()); i++)
            {
                for (int j = 0; j < (int)(_characterisation_itu_ops.Count); j++)
                {
                    if (_characterisation_itu_ops[j]._ITU_channel_number == (int)i
                     && _characterisation_itu_ops[j]._is_an_ITU_point)
                    {
                        // Found estimate for ITU point i
                        itu_op_count++;
                        break;
                    }
                }
            }
            return itu_op_count;
        }

        /// <summary>
        /// Check the pair list uniqueness
        /// </summary>
        /// <param name="freqAndI">The list contains pairs</param>
        /// <param name="pair">Pair</param>
        /// <returns> true: pair can add into pair list, false: pair can not int pair list</returns>
        private bool CanAddIntoPairListUniqueness(List<Pair<double, double>> freqAndI, Pair<double, double> pair)
        {
            bool retValue = true;

            foreach (Pair<double, double> var in freqAndI)
            {
                // there is a same pair
                if (Math.Abs(var.First - pair.First) < 1e-6)
                {
                    retValue = false;
                    break;
                }
            }

            return retValue;
        }

        private void writeOPsToFile()
        {
            //name pattern
            // ITUOperatingPoints_DSDBR01_<LASER_ID>_<TIMESTAMP>.csv
            if (this._result_dir[_result_dir.Length - 1] != '\\')
            {
                this._result_dir = this._result_dir + "\\";
            }
            string filename = this._result_dir + ITUpointsFilePrefix + Laser_id + "_" + TestTimeStamp + ".csv";
            _ITUOperatingPointsFilePath = filename;

            // cal how many itu points in the operating list.
            StringDictionary sd = new StringDictionary();
            int num = 0;
            foreach (CDSDBROperatingPoint cop in _characterisation_itu_ops)
            {
                if (!sd.ContainsKey(cop._ITU_channel_number.ToString()))
                {
                    // Prevent the negative _ITU_channel_number bug - chongjian.liang 2016.01.21
                    if (cop._ITU_channel_number < 0)
                    {
                        continue;
                    }

                    num++;
                    sd.Add(cop._ITU_channel_number.ToString(), cop._ITU_channel_number.ToString());
                }
            }

            _ITUPointsCount = num;
            using (StreamWriter writer = new StreamWriter(filename))
            {

                // write file header
                string[] fileheader =
                {
                    "ITU Channel Number",
                    "ITU Channel Frequency",
                    "Supermode number",
                    "Longitudinal Mode Number",
                    "Index on Middle line of Frquency",
                    "Phase [mA]",
                    "Rear [mA]",
                    "Gain [mA]",
                    "SOA [mA]",
                    "Front Pair Number",
                    "Constant Front [mA]",
                    "Non-Constant Front [mA]",
                    "Frequency [GHz]",
                    //"Coarse Frequency [GHz]",
                     "Optical Power Ratio map",
					 "Optical Locker Ratio map" ,
                     "Optical Power Ratio meas",
					 "Optical Locker Ratio meas" ,
                    "X coordinate of boundary above" ,
                    "Y coordinate of boundary above" ,
                    "X coordinate of boundary below",
                    "Y coordinate of boundary below",
                    "X coordinate of boundary to left", 
                    "Y coordinate of boundary to left",
                    "X coordinate of boundary to right",
                    "Y coordinate of boundary to right",
					"Slope Frequency To Index",
                    "RearSOA[mA]" ,
                    "DUT_FineLockerEtalon_mA",
                    "DUT_FineLockerReference_mA",
                    "RearSOA_Cal[mA]" ,
                    "DUT_LockerEtalon_mA",
                    "DUT_LockerReference_mA"
                };
                for (int i = 0; i < fileheader.Length; i++)
                {
                    if (i != fileheader.Length - 1)
                        writer.Write(fileheader[i] + ",");
                    else
                        writer.Write(fileheader[i] + "\n");
                }

                foreach (CDSDBROperatingPoint opt in _characterisation_itu_ops)
                {
                    // Prevent the negative _ITU_channel_number bug - chongjian.liang 2016.01.21
                    if (opt._ITU_channel_number < 0)
                    {
                        continue;
                    }

                    writer.Write(opt._ITU_channel_number + "," + opt._ITU_frequency + ",");
                    writer.Write(opt._sm_number + "," + opt._lm_number + ",");
                    writer.Write(opt._index_on_middle_line_of_frequency + "," + opt._I_phase + ",");
                    writer.Write(opt._I_rear + "," + opt._I_gain + ",");
                    writer.Write(opt._I_soa + "," + opt._front_pair_number + ",");
                    writer.Write(opt._constantCurrent + "," + opt._nonConstantCurrent + ",");
                    //writer.Write(opt._F_calibration + ",");//Jack.zhang for measured on Etalon Guess point
                    writer.Write(opt._F_measured+",");//Jack.zhang for test by WM
                    writer.Write(opt._F_Forward_PowerRatio + ",");
                    writer.Write(opt._F_Forward_LockerRatio + ",");
                    writer.Write(opt._Pr_on_sm_map + ",");
                    writer.Write(opt._slope_freq_to_index + ",");
                    writer.Write(opt._col_of_boundary_above + ",");
                    writer.Write(opt._row_of_boundary_above + ",");
                    writer.Write(opt._col_of_boundary_below + ",");
                    writer.Write(opt._row_of_boundary_below + ",");
                    writer.Write(opt._col_of_boundary_to_left + ",");
                    writer.Write(opt._row_of_boundary_to_left + ",");
                    writer.Write(opt._col_of_boundary_to_right + ",");
                    writer.Write(opt._row_of_boundary_to_right + ",");
                    writer.Write(opt._slope_freq_to_index + ",");
                    writer.Write(opt._I_rearsoa_Fine_mA + ",");
                    writer.Write(opt._DUT_Fine_locker_Etalon_mA + "," + opt._DUT_Fine_locker_Reference_mA + ",");
                    writer.Write(opt._I_rearsoa + ",");
                    writer.Write(opt._DUT_locker_Etalon_mA + "," + opt._DUT_locker_Reference_mA);
                    writer.Write("\n");
                }
                writer.Flush();
                writer.Close();
            }
        }
        /// <summary>
        ///  Write frequency coverage to file. added by purney.xie 12/28/2009
        /// </summary>
        private void WriteFreqCoverageToFile()
        {
            string fileName = FreqPointsFilePrefix + Laser_id + "_" + TestTimeStamp + ".csv";
            string path = Path.Combine(this.Result_dir, fileName);
            string title = "Supermode Number,Longitudinal Mode Number,Index on middle line of frequency (interpolated),Frequency [GHz],Insert Order";
            if (Directory.Exists(this.Result_dir))
            {
                using (StreamWriter sw = new StreamWriter(path))
                {
                    sw.WriteLine(title);
                    foreach (Pair<double, lm_freq_line_point_type2> var in _Freq_pts_on_freq_lines)
                    {
                        sw.Write(var.Second.sm + ",");
                        sw.Write(var.Second.lm + ",");
                        sw.Write(var.Second.i + ",");
                        sw.Write(var.First + ",");
                        sw.WriteLine(var.Second.insert_order);
                    }
                    sw.Flush();
                    sw.Close();
                }
            }
        }

        private void writeCalibrationFrequencyPointsToFile()//Jack.zhang for Etalon Wavemeter 12/11/2010
        {
            // write file header
            if (this._result_dir[_result_dir.Length - 1] != '\\')
            {
                this._result_dir = this._result_dir + "\\";
            }
            string filename = this._result_dir + SamplepointsFilePrefix + Laser_id + "_" + TestTimeStamp + ".csv";
            //_ITUEstimateFilePath = filename;
            // cal how many itu points in the operating list.
            StringDictionary sd = new StringDictionary();
            int num = 0;
            foreach (Pair<double, CDSDBROperatingPoint> item in _coarse_freq_sample_points)
            {
                CDSDBROperatingPoint cop = item.Second;
                if (!sd.ContainsKey(cop._ITU_channel_number.ToString()))
                {
                    num++;
                    sd.Add(cop._ITU_channel_number.ToString(), cop._ITU_channel_number.ToString());
                }
            }

            using (StreamWriter writer = new StreamWriter(filename))
            {
                string[] fileheader =
                {
                    "ITU Channel Number",
                    "ITU Channel Frequency",
                    "Meas_Frequency [GHz]",
                    "Frequency Error[GHz]",
                    "LockerRatio Error",
                    "First_Meas_Freq_GHz",
                    "First_Meas_LR",
                    "Supermode number",
                    "Longitudinal Mode Number",
                    "Index on Im",
                    "Index on Iphase",
                    "Power Ratio",
                    "Locker Ratio",
                    "Gain [mA]",
                    "SOA [mA]",
                    "Rear [mA]",
                    "Phase [mA]",
                    "Front Pair Number",
                    "Constant Front [mA]",
                    "Non-Constant Front [mA]",
                    "Frequency Cal[GHz]",
                    "Power_ratio_measured",
                    "Locker_ratio_measured",
                    "RearSOA [mA]",
                    "DUT_FineLockerEtalon_mA",
                    "DUT_FineLockerReference_mA",
                    "RearSOA_Cal [mA]",
                    "DUT_LockerEtalon_mA",
                    "DUT_LockerReference_mA",
                    "Tuning_Count"
                };
                for (int i = 0; i < fileheader.Length; i++)
                {
                    if (i != fileheader.Length - 1)
                        writer.Write(fileheader[i] + ",");
                    else
                        writer.Write(fileheader[i] + "\n");
                }

                foreach (Pair<double, CDSDBROperatingPoint> item in _coarse_freq_sample_points)
                {
                    CDSDBROperatingPoint opt = item.Second;

                    writer.Write(opt._ITU_channel_number + "," + opt._ITU_frequency + ",");
                    writer.Write(opt._F_measured + "," + (opt._ITU_frequency - opt._F_measured) + ",");
                    writer.Write((opt._F_Forward_LockerRatio - opt._Lr_on_sm_map) + ",");
                    writer.Write(opt._Initial_Freq_GHz + "," + opt._Initial_Lr_on_sm_map + ",");
                    writer.Write(opt._sm_number + "," + opt._lm_number + ",");
                    writer.Write(opt._real_row_on_sm_map + "," + opt._index_on_middle_line_of_frequency + ",");
                    writer.Write(opt._F_Forward_PowerRatio + "," + opt._F_Forward_LockerRatio + ",");
                    writer.Write(opt._I_gain + "," + opt._I_soa + "," + opt._I_rear + "," + opt._I_phase + ",");
                    writer.Write(opt._front_pair_number + "," + opt._constantCurrent + "," + opt._nonConstantCurrent + ",");
                    writer.Write(opt._F_calibration + ",");
                    writer.Write(opt._Pr_on_sm_map + "," + opt._Lr_on_sm_map + ",");
                    writer.Write(opt._I_rearsoa_Fine_mA + ",");
                    writer.Write(opt._DUT_Fine_locker_Etalon_mA + "," + opt._DUT_Fine_locker_Reference_mA + ",");
                    writer.Write(opt._I_rearsoa + ",");
                    writer.Write(opt._DUT_locker_Etalon_mA + "," + opt._DUT_locker_Reference_mA+ ",");
                    writer.Write(opt._Tunning_Count);
                    writer.Write("\n");

                }
                writer.Flush();
                writer.Close();
            }
        }
        private void writeRawLockerRangeToFile()//Jack.zhang for Etalon Wavemeter 18/11/2010
        {
            // write file header
            if (this._result_dir[_result_dir.Length - 1] != '\\')
            {
                this._result_dir = this._result_dir + "\\";
            }
            string filename = this._result_dir + "LockerRange_" + Laser_id + "_" + TestTimeStamp + "_Simulation" + ".csv";
            _ITUEstimateFilePath = filename;
            using (StreamWriter writer = new StreamWriter(filename))
            {
                string[] fileheader =
                {
                    "ITU Channel Number_Modify",
                    "ITU Channel Frequency_Modify",
                    "IS_ITU_CH",
                    "IS_ITU_CH_Count",
                    "ITU Channel Number_Raw",
                    "ITU Channel Frequency_Raw",
                    "Supermode number",
                    "Longitudinal Mode Number",
                    "Longitudinal Mode (N) Width",
                    "Below Longitudinal Mode (N-1) Width",
                    "ITU_Im_Index",
                    "ITU_Ip_Index",
                    "Range_Group",
                    "LockerSlope",
                    "first Index on Iphase",
                    "ITU_Group_Index",
                    "second Index on Iphase",
                    "first Power Ratio",
                    "second Power Ratio",
                    "Media Power Ratio",
                    "first Target Power Ratio",
                    "second Target Power Ratio",
                    "Target Power Ratio",
                    "Target Locker Ratio",
                    "ITU_Ip_current_mA",
                    "ITU_Group_deltIp_current_mA",
                    "LM_CH_Index"
                };
                for (int i = 0; i < fileheader.Length; i++)
                {
                    if (i != fileheader.Length - 1)
                        writer.Write(fileheader[i] + ",");
                    else
                        writer.Write(fileheader[i] + "\n");
                }

                foreach (Matrix_Data item in Raw_Matrix_Data)
                {

                    writer.Write(item.CH_Number_Modify + "," + item.CH_Freq_GHz_Modify + ",");
                    writer.Write(item.IS_ITU_CH + "," + item.IS_ITU_CH_Count + ",");
                    writer.Write(item.CH_Number + "," + item.CH_Freq_GHz + ",");
                    writer.Write(item.SM + "," + item.LM + ",");
                    writer.Write(item.LM_Width + "," + item.Below_LM_Width + ",");
                    writer.Write(item.ITU_Im_Index + "," + item.ITU_Ip_Index + ",");
                    writer.Write(item.Range_Group + "," + item.Locker_Ratio_Slope + ",");
                    writer.Write(item.first_Ip_Index + "," + item.ITU_Group_Index + "," + +item.second_Ip_Index + ",");
                    writer.Write(item.first_Power_Ratio + "," + item.second_Power_Ratio + "," + item.Media_Power_Ratio + ",");
                    writer.Write(item.first_Target_Power_Ratio + "," + item.second_Target_Power_Ratio + ",");
                    writer.Write(item.Target_Power_Ratio + "," + item.Target_Locker_Ratio + ",");
                    writer.Write(item.ITU_Ip_current_mA + "," + item.ITU_Group_deltIp_current_mA + ",");
                    writer.Write(item.LM_CH_Index + "\n");

                }
                writer.Flush();
                writer.Close();
            }
        }
        private void findUpperAndLower(double freq_of_channel_num, List<Pair<double, double>> freq_and_i,
            out double freq_upper, out double point_upper, out double freq_lower, out double point_lower)
        {
            freq_upper = MAX_VALID_FREQ_MEAS;
            point_upper = 0;
            freq_lower = MIN_VALID_FREQ_MEAS;
            point_lower = 0;

            for (int map_iter = 0; map_iter != freq_and_i.Count; map_iter++)
            {
                if ((freq_and_i[map_iter]).First >= freq_of_channel_num && (freq_and_i[map_iter]).First <= freq_upper)
                {
                    freq_upper = (freq_and_i[map_iter]).First;
                    point_upper = (freq_and_i[map_iter]).Second;
                }

                if ((freq_and_i[map_iter]).First <= freq_of_channel_num && (freq_and_i[map_iter]).First >= freq_lower)
                {
                    freq_lower = (freq_and_i[map_iter]).First;
                    point_lower = (freq_and_i[map_iter]).Second;
                }
            }
        }

        /// <summary>
        /// Find itu point's left,right, above ,below point on longitiude mode
        /// </summary>
        /// <param name="operating_points">characterization operation points</param>
        private void findNearbyBoundaryCoords(CDSDBROperatingPoint operating_points)
        {
            CDSDBROperatingPoint ops = operating_points;
            int sm_number = ops._sm_number;
            int lm_number = ops._lm_number;
            int i_number = ops._index_on_middle_line_of_frequency;
            long row = ops._row_on_sm_map;
            long col = ops._col_on_sm_map;
            CDSDBRSuperMode i_sm = _p_supermodes[sm_number];
            // remember! _lm_upper_lines refers to upper side of hysteresis (lower line of a mode!)
            // and _lm_lower_lines refers to lower side of hysteresis (upper line of a mode!)
            List<CDSDBRSupermodeMapLine.Point_Type> p_lower_points = i_sm._lm_lower_lines[lm_number]._points;
            List<CDSDBRSupermodeMapLine.Point_Type> p_uper_points = i_sm._lm_upper_lines[lm_number + 1]._points;
            ops._row_of_boundary_above = i_sm._row - 1;
            ops._col_of_boundary_above = col;
            ops._row_of_boundary_below = 0;
            ops._col_of_boundary_below = col;
            ops._row_of_boundary_to_left = row;
            ops._col_of_boundary_to_left = 0;
            ops._row_of_boundary_to_right = row;
            ops._col_of_boundary_to_right = i_sm._col - 1;
            // first set boundary point on edge of map
            // then if points are found on boundaries, replace them

            foreach (CDSDBRSupermodeMapLine.Point_Type point in p_lower_points)
            {
                long lower_pt_row = point.row;
                long lower_pt_col = point.col;
                if (lower_pt_col == col)
                {
                    // found point below
                    if (lower_pt_row > ops._row_of_boundary_below)
                    {
                        ops._row_of_boundary_below = lower_pt_row;
                        ops._col_of_boundary_below = lower_pt_col;
                    }
                }
                if (lower_pt_row == row)
                {
                    // found point to right
                    if (lower_pt_col < ops._col_of_boundary_to_right)
                    {
                        ops._row_of_boundary_to_right = lower_pt_row;
                        ops._col_of_boundary_to_right = lower_pt_col;
                    }
                }
            }
            foreach (CDSDBRSupermodeMapLine.Point_Type point in p_uper_points)
            {
                long upper_pt_row = point.row;
                long upper_pt_col = point.col;
                if (upper_pt_col == col)
                {
                    if (upper_pt_row < ops._row_of_boundary_above)
                    {
                        // found point above
                        ops._row_of_boundary_above = upper_pt_row;
                        ops._col_of_boundary_above = upper_pt_col;
                    }

                }
                if (upper_pt_row == row)
                {
                    if (upper_pt_col > ops._col_of_boundary_to_left)
                    {
                        // found point to left
                        ops._row_of_boundary_to_left = upper_pt_row;
                        ops._col_of_boundary_to_left = upper_pt_col;
                    }
                }
            }
        }

        /// <summary>
        /// Find itu point's left,right, above ,below point on longitiude mode
        /// </summary>
        /// <param name="operating_points">characterization operation points</param>
        private void findNearbyBoundaryCoords(List<CDSDBROperatingPoint> operating_points)
        {
            foreach (CDSDBROperatingPoint ops in operating_points)
            {

                int sm_number = ops._sm_number;
                int lm_number = ops._lm_number;
                int i_number = ops._index_on_middle_line_of_frequency;
                long row = ops._row_on_sm_map;
                long col = ops._col_on_sm_map;
                CDSDBRSuperMode i_sm = _p_supermodes[sm_number];
                // remember! _lm_upper_lines refers to upper side of hysteresis (lower line of a mode!)
                // and _lm_lower_lines refers to lower side of hysteresis (upper line of a mode!)
                List<CDSDBRSupermodeMapLine.Point_Type> p_lower_points = i_sm._lm_lower_lines[lm_number]._points;
                List<CDSDBRSupermodeMapLine.Point_Type> p_uper_points = i_sm._lm_upper_lines[lm_number + 1]._points;
                ops._row_of_boundary_above = i_sm._row - 1;
                ops._col_of_boundary_above = col;
                ops._row_of_boundary_below = 0;
                ops._col_of_boundary_below = col;
                ops._row_of_boundary_to_left = row;
                ops._col_of_boundary_to_left = 0;
                ops._row_of_boundary_to_right = row;
                ops._col_of_boundary_to_right = i_sm._col - 1;
                // first set boundary point on edge of map
                // then if points are found on boundaries, replace them

                foreach (CDSDBRSupermodeMapLine.Point_Type point in p_lower_points)
                {
                    long lower_pt_row = point.row;
                    long lower_pt_col = point.col;
                    if (lower_pt_col == col)
                    {
                        // found point below
                        if (lower_pt_row > ops._row_of_boundary_below)
                        {
                            ops._row_of_boundary_below = lower_pt_row;
                            ops._col_of_boundary_below = lower_pt_col;
                        }
                    }
                    if (lower_pt_row == row)
                    {
                        // found point to right
                        if (lower_pt_col < ops._col_of_boundary_to_right)
                        {
                            ops._row_of_boundary_to_right = lower_pt_row;
                            ops._col_of_boundary_to_right = lower_pt_col;
                        }
                    }
                }
                foreach (CDSDBRSupermodeMapLine.Point_Type point in p_uper_points)
                {
                    long upper_pt_row = point.row;
                    long upper_pt_col = point.col;
                    if (upper_pt_col == col)
                    {
                        if (upper_pt_row < ops._row_of_boundary_above)
                        {
                            // found point above
                            ops._row_of_boundary_above = upper_pt_row;
                            ops._col_of_boundary_above = upper_pt_col;
                        }

                    }
                    if (upper_pt_row == row)
                    {
                        if (upper_pt_col > ops._col_of_boundary_to_left)
                        {
                            // found point to left
                            ops._row_of_boundary_to_left = upper_pt_row;
                            ops._col_of_boundary_to_left = upper_pt_col;
                        }
                    }
                }

            }
        }

        /// <summary>
        /// remove Upper And Lower, But Not End Points
        /// </summary>
        /// <param name="freq_of_channel_num"></param>
        /// <param name="first_point_index"></param>
        /// <param name="last_point_index"></param>
        /// <param name="?"></param>
        private void removeUpperAndLowerButNotEndPoints(
        double freq_of_channel_num,
        double first_point_index,
        double last_point_index,
        List<Pair<double, double>> freq_and_i,
        List<Pair<double, double>> i_and_min_measured_freq,
        List<Pair<double, double>> i_and_max_measured_freq
        )
        {
            double freq_upper;
            double point_upper;
            double freq_lower;
            double point_lower;

            findUpperAndLower(
                freq_of_channel_num,
                freq_and_i,
               out freq_upper,
               out point_upper,
               out freq_lower,
               out point_lower);

            Predicate<Pair<double, double>> match_Upper = delegate(Pair<double, double> obj)
            {
                if (Math.Abs(obj.First - point_upper) < 1e-6)
                {
                    return true;
                }
                else return false;
            };
            Predicate<Pair<double, double>> match_Lower = delegate(Pair<double, double> obj)
            {
                if (Math.Abs(obj.First - point_lower) < 1e-6)
                {
                    return true;
                }
                else return false;
            };
            // Don't remove first or last point
            if (point_upper > first_point_index
             && point_upper < last_point_index)
            {
                int freq_and_i_index = freq_and_i.FindIndex(delegate(Pair<double, double> obj)
                {
                    if (Math.Abs(obj.First - freq_upper) < 1e-6)
                    {
                        return true;
                    }
                    else return false;
                });
                if (freq_and_i_index != -1)
                {
                    freq_and_i.RemoveAt(freq_and_i_index);
                }



                int point_upper_index = i_and_min_measured_freq.FindIndex(match_Upper);
                if (point_upper_index != -1)
                {
                    i_and_min_measured_freq.RemoveAt(point_upper_index);
                }
                point_upper_index = i_and_max_measured_freq.FindIndex(match_Upper);
                if (point_upper_index != -1)
                {
                    i_and_max_measured_freq.RemoveAt(point_upper_index);
                }
            }

            // Don't remove first or last point
            if (point_lower > first_point_index
             && point_lower < last_point_index)
            {
                int freq_and_i_index = freq_and_i.FindIndex(delegate(Pair<double, double> obj)
                {
                    if (Math.Abs(obj.First - freq_lower) < 1e-6)
                    {
                        return true;
                    }
                    else return false;
                });
                if (freq_and_i_index != -1)
                {
                    freq_and_i.RemoveAt(freq_and_i_index);
                }
                int freq_lower_index = i_and_min_measured_freq.FindIndex(match_Lower);
                if (freq_lower_index != -1)
                {
                    i_and_min_measured_freq.RemoveAt(freq_lower_index);
                }


                freq_lower_index = i_and_max_measured_freq.FindIndex(match_Lower);
                if (freq_lower_index != -1)
                {
                    i_and_max_measured_freq.RemoveAt(freq_lower_index);
                }
            }
        }

        private void removeUpperAndLower(
        double freq_of_channel_num,
        List<Pair<double, double>> freq_and_i,
        List<Pair<double, double>> i_and_min_measured_freq,
        List<Pair<double, double>> i_and_max_measured_freq
        )
        {
            removeUpperAndLowerButNotEndPoints(
                freq_of_channel_num,
                -1, // lower index that cannot exist
                9999999, // upper index that cannot exist
                freq_and_i,
                i_and_min_measured_freq,
                i_and_max_measured_freq
                );
        }
        private bool testNSamples(
        List<Pair<double, double>> freq_and_i,
        List<Pair<double, double>> i_and_min_measured_freq,
        List<Pair<double, double>> i_and_max_measured_freq,
        int number_of_samples,
        double first_point,
        double last_point,
        CDSDBRSupermodeMapLine i_lm,
        CDSDBRSuperMode i_sm,
        int sm,
        int lm,
        int insert_order)
        {
            double index_of_point_to_sample = 0.0;
            double freq_i = 0.0;
            double min_measured_freq = 0.0;
            double max_measured_freq = 0.0;
            bool testOk = true;
            int settle_time_ms = int.Parse(characterizationSetting["OperatingPointSettleTime"]);

            for (int i = 0; i < number_of_samples; i++)
            {
                if (i == 0)
                {	// first point
                    index_of_point_to_sample = first_point;
                }
                else if (i == number_of_samples - 1)
                {
                    // last point
                    index_of_point_to_sample = last_point;
                }
                else
                {
                    index_of_point_to_sample = first_point
                        + (double)(i) * (last_point - first_point) / (double)(number_of_samples - 1);
                }

                CDSDBROperatingPoint test_op = new CDSDBROperatingPoint(i_sm, i_lm, sm, lm, index_of_point_to_sample);
                //if (IsByAsic)
                //{
                //    test_op.ReCountRearSoa(slope_rearsoa, constant_rear_rearsoa);
                //    testOk = test_op.ApplyByASIC(fcu, slope_rearsoa, constant_rear_rearsoa);
                //}
                //else
                if (IRearSoa_Fine_mA == 0)
                    test_op.ReCountRearSoa(slope_rearsoa, constant_rear_rearsoa, Max_RearSoa_mA, Min_RearSoa_mA);
                else
                    test_op._I_rearsoa = IRearSoa_Fine_mA;
                testOk = test_op.Apply(fcu);
                if (testOk == true)
                {

                    // take a frequency measurement
                    short num_freq_meas_per_op = short.Parse(characterizationSetting["NumberOfFrequencyMeasurementsToAveragePerOperatingPoint"]);
                    testOk = measure_F_P_Pr(ref freq_i, ref min_measured_freq, ref max_measured_freq, num_freq_meas_per_op);
                    if (testOk == true)
                    {
                        lm_freq_line_point_type2 new_pt = new lm_freq_line_point_type2();
                        new_pt.sm = sm;
                        new_pt.lm = lm;
                        new_pt.i = index_of_point_to_sample;
                        new_pt.insert_order = insert_order * 10 + i;
                        //Adding test sample to map of all samples in LM

                        _Freq_pts_on_freq_lines.Add(new Pair<double, lm_freq_line_point_type2>(freq_i, new_pt));
                        bool canAddInto = true;
                        canAddInto = CanAddIntoPairListUniqueness(freq_and_i, new Pair<double, double>(freq_i, index_of_point_to_sample));
                        if (canAddInto)
                        {
                            freq_and_i.Add(new Pair<double, double>(freq_i, index_of_point_to_sample));
                        }
                        canAddInto = CanAddIntoPairListUniqueness(i_and_min_measured_freq, new Pair<double, double>(index_of_point_to_sample, min_measured_freq));
                        if (canAddInto)
                        {
                            i_and_min_measured_freq.Add(new Pair<double, double>(index_of_point_to_sample, min_measured_freq));
                        }
                        canAddInto = CanAddIntoPairListUniqueness(i_and_max_measured_freq, new Pair<double, double>(index_of_point_to_sample, max_measured_freq));
                        if (canAddInto)
                        {
                            i_and_max_measured_freq.Add(new Pair<double, double>(index_of_point_to_sample, max_measured_freq));
                        }
                    } // end of if !error from measure_F_P_Pr
                } // end of if !error from test_op.apply()
            }
            return testOk;
        }
        private bool measure_F_P_Pr(ref double F, ref double min_measured_freq, ref double max_measured_freq, short num_F_samples_to_average)
        {

            bool measureOk = true;
            int freq_samples = 0;
            short max_j = num_F_samples_to_average;
            double frequency = 0;
            F = 0;
            for (short j = 0; j < max_j; j++)
            {
                if (freq_samples < num_F_samples_to_average)
                {
                    // Take a frequency sample
                    try
                    {

                        frequency = measure_Freq();
                    }
                    catch (Exception)
                    {   //invalid_frequency_from_wavemeter;
                        measureOk = false;
                        break;
                        //throw;
                    }

                    if (frequency < MIN_VALID_FREQ_MEAS
                     || frequency > MAX_VALID_FREQ_MEAS)
                    {
                        //invalid_frequency_from_wavemeter
                        break;
                    }

                    if (j == 0)
                    { // first time
                        max_measured_freq = frequency;
                        min_measured_freq = frequency;
                    }
                    else
                    { // remaining times
                        max_measured_freq = max_measured_freq > frequency ? max_measured_freq : frequency;
                        min_measured_freq = min_measured_freq < frequency ? min_measured_freq : frequency;
                    }

                    F += frequency;
                    freq_samples++;
                }
            }

            F /= (double)freq_samples;
            return measureOk;
        }

        private bool CanUsePreviousItuPtsInfo(List<CDSDBROperatingPoint> character_points, int sm, int lm, int minChannel, int size)
        {
            Pair<int, int> pre_lm = num_ItuPointsInLM_minChannel_num(character_points, sm, lm - 1);
            Pair<int, int> pre2_lm = num_ItuPointsInLM_minChannel_num(character_points, sm, lm - 2);
            if ((pre_lm.First == 2) && (pre2_lm.First == 2) && (size == 2)) //all of the three LMs should have the same itu points
            {
                if (minChannel - pre_lm.Second == 1 && minChannel - pre2_lm.Second == 2)
                    return true;
            }
            return false;
        }

        private bool findItuColIndexInLm(List<CDSDBROperatingPoint> character_points, Pair<double, double> ItuPoints_ColIndex, int sm, int lm, int start_chnanel)
        {
            int ituPoints_num = 0;
            foreach (CDSDBROperatingPoint m_point in character_points)
            {
                if (m_point._lm_number == lm && m_point._sm_number == sm)
                {
                    ituPoints_num++;
                    if (m_point._ITU_channel_number == start_chnanel)
                    {
                        ItuPoints_ColIndex.First = m_point._real_index_on_middle_line_of_frequency;
                    }
                    else
                    {
                        if (m_point._ITU_channel_number == (start_chnanel + 1))
                        {
                            ItuPoints_ColIndex.Second = m_point._real_index_on_middle_line_of_frequency;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }
            if (ituPoints_num != 2)
                return false;
            return true;
        }
        private Pair<int, int> num_ItuPointsInLM_minChannel_num(List<CDSDBROperatingPoint> character_points, int sm, int lm)
        {
            Pair<int, int> retval = new Pair<int, int>();
            int minChannel_num = 100; //100 is big enough and this value will be replaced by the first comparison
            int numItuPts = 0;
            foreach (CDSDBROperatingPoint m_point in character_points)
            {
                if (m_point._lm_number == lm && m_point._sm_number == sm)
                {
                    numItuPts++;
                    if (m_point._ITU_channel_number < minChannel_num)
                        minChannel_num = m_point._ITU_channel_number;
                }
            }
            retval.First = numItuPts;
            retval.Second = minChannel_num;
            return retval;
        }
        private void incrementDuffITUOPCount()
        {
            _p_duff_ITUOP_count++;
            return;
        }
        #endregion
    }
}

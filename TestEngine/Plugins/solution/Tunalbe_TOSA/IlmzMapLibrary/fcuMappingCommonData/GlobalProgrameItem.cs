using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.FcuCommonUtils;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.Instruments;
using Bookham.TestLibrary.Utilities;


namespace Bookham.fcumapping.CommonData
{
    public class GlobalProgrameItem
    {
        
        #region local parameter
        //private static IInstType_TecController tecdsdbr;
        //private static IInstType_TecController teccase;
        private static Inst_Ke2510 tecdsdbr;
        private static Inst_Ke2510 teccase;
        //private static IInstType_Wavemeter wavemeter;
        //private static Inst_Ag86122x wavemeter;
        private static Inst_Ag816x_OpticalPowerMeter opmRef;
        private static Inst_Ag816x_OpticalPowerMeter opmlaser;
        private static Inst_Fcu2Asic fcu;
        private static TestParamConfigAccessor progReader;
        private static TestParamConfigAccessor teccaseReader;
        private static TestParamConfigAccessor tecdsdbrReader;
        private static TestParamConfigAccessor elecSupplyReader;
        private static string localDir;
        private static DsdbrChannelData[] dsdbrAllChannels;
        private static DsdbrChannelData[] dsdbrHighLowerRearChannelsOf1SM; // highest rear current and lowest rear current of each SM 
        private static DsdbrChannelData[] dsdbrLowerRearChannelsOf1SM;
        private static DsdbrChannelData[] dsdbrHighRearChannelsOf1SM;
        private static DsdbrChannelData dsdbrHighestSOAChannelsIn12WorstChannels;
        private static PowerLevelResultData[] powerLevelResultChannels;

        //DsdbrChannelData[] dsdbrLowRearChannelsOf1SM; //low rear current of each SM
        private static DsdbrChannelData[] dsdbrFirstMidLastChannels; // first mid last channels 
        private static string serialNo;
        private static Inst_E36xxA vccSource;
        private static Inst_E36xxA veeSource;
        private static TestConditionType programTestCondition;

        #endregion



        public static Inst_Ke2510 TecDsdbr
        {
            get { return tecdsdbr; }
            set { tecdsdbr = value; }
        }
        //public static InstType_TecController TecCase
        //{
        //    get { return teccase; }
        //    set { teccase = value; }
        //}
        //public static Inst_Ag86122x WaveMeter
        //{
        //    get { return wavemeter; }
        //    set { wavemeter=value; }
        //}
        public static Inst_Ag816x_OpticalPowerMeter OpmLaser
        {
            get { return opmlaser; }
            set { opmlaser = value; }
        }
        public static Inst_Ag816x_OpticalPowerMeter OpmReference
        {
            get { return opmRef; }
            set { opmRef = value; }
        }
        public static TestParamConfigAccessor ProgParamReader
        {
            get { return progReader; }
            set { progReader = value; }
        }
        public static TestParamConfigAccessor TecCaseSettingReader
        {
            get { return teccaseReader; }
            set { teccaseReader = value; }
        }
        public static TestParamConfigAccessor TecLaserSettingReader
        {
            get { return tecdsdbrReader; }
            set { tecdsdbrReader = value; }
        }
        public static TestParamConfigAccessor ElecSettingReader
        {
            get { return elecSupplyReader; }
            set { elecSupplyReader = value; }
        }
        /// <summary>
        /// local work dir which result file located in.
        /// </summary>
        public static string LocalWorkDir
        {
            get { return localDir; }
            set { localDir=value; }

        }
        /// <summary>
        /// all dsdbr itu channels
        /// </summary>
        public static DsdbrChannelData[] DsdbrAllChannels
        {
            get { return dsdbrAllChannels; }
            set { dsdbrAllChannels = value; }
        }
        /// <summary>
        /// dsdbr channels which has highest rear current in each SM and has lowest rear current in each SM
        /// </summary>
        public static DsdbrChannelData[] DsdbrHighLowRearChannels
        {
            get { return dsdbrHighLowerRearChannelsOf1SM; }
            set { dsdbrHighLowerRearChannelsOf1SM = value; }
        }
        /// <summary>
        /// dsdbr channels which has higher rear current in each SM
        /// </summary>
        public static DsdbrChannelData[] DsdbrHighRearChannels
        {
            get { return dsdbrHighRearChannelsOf1SM; }
            set { dsdbrHighRearChannelsOf1SM = value; }
        }
        public static DsdbrChannelData[] DsdbrLowRearChannels
        {
            get { return dsdbrLowerRearChannelsOf1SM; }
            set { dsdbrLowerRearChannelsOf1SM = value; }
        }
        /// <summary>
        /// dsdbr first mid last channels
        /// </summary>
        public static DsdbrChannelData[] DsdbrFirstMidLastChannels
        {
            get { return dsdbrFirstMidLastChannels; }
            set { dsdbrFirstMidLastChannels = value; }
        }
        /// <summary>
        /// highest soa channels in 12 worst case channels(high/lowest rear current channels)
        /// </summary>
        public static DsdbrChannelData DsdbrHighestSoaChannels
        {
            get { return dsdbrHighestSOAChannelsIn12WorstChannels; }
            set { dsdbrHighestSOAChannelsIn12WorstChannels = value; }
        }

        public static PowerLevelResultData[] PowerLevelResultChannels
        {
            get { return powerLevelResultChannels; }
            set { powerLevelResultChannels = value; }
        }
        

        public static string SerialNo
        {
            get { return serialNo; }
            set { serialNo = value; }

        }
        public static Inst_E36xxA VccSource
        {
            get { return vccSource; }
            set { vccSource = value; }
        }
        public static Inst_E36xxA VeeSource
        {
            get { return veeSource; }
            set { veeSource = value; }
        }

        public static TestConditionType ProgramTestCondition
        {
            get { return programTestCondition; }
            set { programTestCondition = value; }
        }


        public struct TestConditionType
        {
            public int ChannelNbr;
            public double StartFrequency;
            public double FiberTargetPower;
            public double TxTargetCurrent;
            public double MinRearSoa;
            public double MaxRearSoa;
        }
    }
   
}

using System;
using System.Collections.Generic;
using System.Text;
//coded by purney.xie
namespace Bookham.fcumapping.CommonData
{
    public class ModeAnalysis
    {
        #region public Data
        public ModeBoundaryLine _upperBoundaryLine = new ModeBoundaryLine();
        public ModeBoundaryLine _lowerBoundaryLine = new ModeBoundaryLine();
        #endregion
        #region private Data

        int _modeNumber;

        int _xAxisLength;
        int _yAxisLength;
        int _modeLength;
        bool _modeBoundaryLinesLoaded;
        QAThresholds _qaThresholds = new QAThresholds();

        bool _modalDistortionAnalysisRun;
        Vector _modeWidths = new Vector();
        double _maxModeWidth;
        double _minModeWidth;
        double _meanModeWidths;
        double _sumModeWidths;
        double _numModeWidthSamples;
        double _stdDevModeWidths;
        double _varModeWidths;

        //
        ////////////////////////////////////////////
        //
        Vector _modeUpperSlopes = new Vector();
        double _maxModeUpperSlope;
        double _minModeUpperSlope;
        double _meanModeUpperSlopes;
        double _sumModeUpperSlopes;
        double _numModeUpperSlopeSamples;
        double _stdDevModeUpperSlopes;
        double _varModeUpperSlopes;

        //
        ////////////////////////////////////////////
        //
        Vector _modeLowerSlopes = new Vector();
        double _maxModeLowerSlope;
        double _minModeLowerSlope;
        double _meanModeLowerSlopes;
        double _sumModeLowerSlopes;
        double _numModeLowerSlopeSamples;
        double _stdDevModeLowerSlopes;
        double _varModeLowerSlopes;

        //
        ////////////////////////////////////////////
        //
        double _lowerModalDistortionAngle;
        double _maxLowerModalDistortionAngleXPosition;
        double _upperModalDistortionAngle;
        double _maxUpperModalDistortionAngleXPosition;

        //
        ////////////////////////////////////////////
        //
        Vector _lowerModeBDCAreas = new Vector();
        double _lowerMaxModeBDCArea;
        double _lowerMinModeBDCArea;
        double _lowerMeanModeBDCAreas;
        double _lowerSumModeBDCAreas;
        double _lowerNumModeBDCAreas;
        double _lowerStdDevModeBDCAreas;
        double _lowerVarModeBDCAreas;
        //
        Vector _upperModeBDCAreas = new Vector();
        double _upperMaxModeBDCArea;
        double _upperMinModeBDCArea;
        double _upperMeanModeBDCAreas;
        double _upperSumModeBDCAreas;
        double _upperNumModeBDCAreas;
        double _upperStdDevModeBDCAreas;
        double _upperVarModeBDCAreas;

        //
        ////////////////////////////////////////////
        //
        Vector _lowerModeBDCAreaXLengths = new Vector();
        double _lowerMaxModeBDCAreaXLength;
        double _lowerMinModeBDCAreaXLength;
        double _lowerMeanModeBDCAreaXLengths;
        double _lowerSumModeBDCAreaXLengths;
        double _lowerNumModeBDCAreaXLengths;
        double _lowerStdDevModeBDCAreaXLengths;
        double _lowerVarModeBDCAreaXLengths;
        //
        Vector _upperModeBDCAreaXLengths = new Vector();
        double _upperMaxModeBDCAreaXLength;
        double _upperMinModeBDCAreaXLength;
        double _upperMeanModeBDCAreaXLengths;
        double _upperSumModeBDCAreaXLengths;
        double _upperNumModeBDCAreaXLengths;
        double _upperStdDevModeBDCAreaXLengths;
        double _upperVarModeBDCAreaXLengths;
        #endregion

        public void loadBoundaryLines(ModeBoundaryLine lower, ModeBoundaryLine upper)
        {
            _upperBoundaryLine = upper;
            _lowerBoundaryLine = lower;
            _modeBoundaryLinesLoaded = true;
        }
        public void setQAThresholds(QAThresholds qa)
        {
            _qaThresholds = qa;
        }
        public void getModeWidths(out Vector modeWidths, int startIndex, int stopIndex)
        {
            if (_modeBoundaryLinesLoaded)
            {
                // only do this once
                if (_modeWidths.size() == 0)
                {
                    //before we start we need to check that the start and stop index's specced are 
                    //useful for this LM. ie is ther a lower and upper line that both traverse
                    // the specced window. If not then just examine the whole mode from start to end
                    // as would be done if no window specced. This will give a useful set of metrics
                    // to gauge the LM by, rather than returning zero's, and not skew any overall SM appraisals. 
                    int lowerBoundaryLineStart = (int)_lowerBoundaryLine.x(0);
                    int upperBoundaryLineEnd = (int)_upperBoundaryLine.x(_upperBoundaryLine.numXPoints() - 1);
                    if ((lowerBoundaryLineStart > startIndex) || upperBoundaryLineEnd < stopIndex)
                    {
                        getModeWidths(out modeWidths, 0);	//call original func over entire length
                    }
                    else //analyse between window, we know line has an index at startIndex thru to  stopIndex
                    {
                        //////////////////////////////////////////////////////////////////////////////

                        // create vector of highest points on bottom line at each x_coord
                        List<double> bottom_line_highest_points = new List<double>();

                        int lowest_x = 9999999; // Huge number to begin with
                        int highest_x = -1;
                        //GDM lower boundsary analysis, ensure we don't exceed boundary or vector length, also we can set start stop = 0 to do whole vector as before
                        if (startIndex < 0)
                            startIndex = 0;
                        if (stopIndex <= startIndex)
                            stopIndex = _lowerBoundaryLine.numXPoints();
                        if (stopIndex > _lowerBoundaryLine.numXPoints())
                            stopIndex = _lowerBoundaryLine.numXPoints();

                        for (int i = startIndex; i < stopIndex; i++)
                        {
                            // find x and y position of each point on the line

                            int x_point = (int)_lowerBoundaryLine.x(i);
                            int y_point = (int)_lowerBoundaryLine.y(i);

                            if ((int)x_point < lowest_x)
                                lowest_x = (int)x_point;

                            if ((int)x_point > highest_x)
                            {
                                highest_x = (int)x_point;
                                // add point to vector
                                bottom_line_highest_points.Add(i);
                            }
                            else
                            {
                                // check for previous occurances of x_point
                                int num = (int)(bottom_line_highest_points.Count);
                                for (int f = num - 1; f >= 0; f--)
                                {
                                    double index_find = bottom_line_highest_points[f];
                                    double y_find = _lowerBoundaryLine.y((int)index_find);
                                    double x_find = _lowerBoundaryLine.x((int)index_find);
                                    if (x_find == x_point)
                                    {
                                        // found previous point at the same x location, is it at a higher y location?
                                        if (y_point > y_find)
                                            bottom_line_highest_points[f] = i;
                                        break;
                                    }
                                }//for( int f = num-1; f >= 0; f-- )
                            }//else
                        }//for(int i=startIndex; i<stopIndex; i++)

                        // create vector of lowest points on top line at each x_coord
                        List<double> top_line_lowest_points = new List<double>();

                        //GDM Upper boundary analysis, ensure we don't exceed boundary or vector length, also we can set start stop = 0 to do whole vector as before
                        if (startIndex < 0)
                            startIndex = 0;
                        if (stopIndex <= startIndex)
                            stopIndex = _upperBoundaryLine.numXPoints();
                        if (stopIndex > _upperBoundaryLine.numXPoints())
                            stopIndex = _upperBoundaryLine.numXPoints();

                        lowest_x = 9999999; // Huge number to begin with
                        highest_x = -1;
                        for (int i = startIndex; i < stopIndex; i++)
                        {
                            // find x and y position of each point on the line
                            double y_point = _upperBoundaryLine.y(i);
                            double x_point = _upperBoundaryLine.x(i);
                            if ((int)x_point < lowest_x)
                                lowest_x = (int)x_point;
                            if ((int)x_point > highest_x)
                            {
                                highest_x = (int)x_point;
                                // add point to vector
                                top_line_lowest_points.Add(i);
                            }
                            else
                            {
                                // check for previous occurances of x_point
                                int num = (int)(top_line_lowest_points.Count);
                                for (int f = num - 1; f >= 0; f--)
                                {
                                    double index_find = top_line_lowest_points[f];
                                    double y_find = _upperBoundaryLine.y((int)index_find);
                                    double x_find = _upperBoundaryLine.x((int)index_find);
                                    if (x_find == x_point)
                                    {
                                        // found point at the same x location, is it at a lowest y location?
                                        if (y_point < y_find)
                                        {
                                            top_line_lowest_points[f] = i;
                                        }
                                        break;
                                    }
                                }//for( int f = num-1; f >= 0; f-- )
                            }//else
                        }//for(int i=startIndex; i<stopIndex; i++)

                        // find match x coordinates on both lines
                        int ti = 0;
                        for (int bi = 0; bi < (int)(bottom_line_highest_points.Count); bi++)
                        {
                            double index_b = bottom_line_highest_points[bi];
                            double y_b = _lowerBoundaryLine.y((int)index_b);
                            double x_b = _lowerBoundaryLine.x((int)index_b);

                            if (ti >= (int)(top_line_lowest_points.Count))
                                break;
                            double index_t = top_line_lowest_points[ti];
                            double y_t = _upperBoundaryLine.y((int)index_t);
                            double x_t = _upperBoundaryLine.x((int)index_t);
                            while (ti < (int)(top_line_lowest_points.Count))
                            {
                                // find
                                index_t = top_line_lowest_points[ti];
                                y_t = _upperBoundaryLine.y((int)index_t);
                                x_t = _upperBoundaryLine.x((int)index_t);
                                if (x_t < x_b)
                                    ti++;
                                else
                                    break;
                            }

                            if (ti >= (int)(top_line_lowest_points.Count)) break;
                            if (x_t == x_b)
                            {
                                // found matching x coordinates
                                // find width between two lines at this point
                                double width = y_t - y_b;
                                if (width < 0)
                                    width = width * -1;
                                _modeWidths.Add(width);
                            }//if (x_t == x_b)
                        }//for (int bi = 0; bi < (int)(bottom_line_highest_points.Count); bi++)

                    }//else
                }//if (_modeWidths.size() == 0)
            }//if (_modeBoundaryLinesLoaded)
            modeWidths = _modeWidths;
        }
        public void getModeWidths(out Vector modeWidths, int startIndex)
        {
            if (_modeBoundaryLinesLoaded)
            {
                // only do this once
                if (_modeWidths.size() == 0)
                {
                    // create vector of highest points on bottom line at each x_coord
                    List<double> bottom_line_highest_points = new List<double>();

                    int lowest_x = 9999999; // Huge number to begin with
                    int highest_x = -1;
                    for (int i = startIndex; i < _lowerBoundaryLine.numXPoints(); i++)
                    {
                        // find x and y position of each point on the line

                        int x_point = (int)_lowerBoundaryLine.x(i);
                        int y_point = (int)_lowerBoundaryLine.y(i);

                        if ((int)x_point < lowest_x)
                            lowest_x = (int)x_point;

                        if ((int)x_point > highest_x)
                        {
                            highest_x = (int)x_point;
                            // add point to vector
                            bottom_line_highest_points.Add(i);

                        }
                        else
                        {

                            // check for previous occurances of x_point
                            int num = (int)(bottom_line_highest_points.Count);
                            for (int f = num - 1; f >= 0; f--)
                            {
                                double index_find = bottom_line_highest_points[f];
                                double y_find = _lowerBoundaryLine.y((int)index_find);
                                double x_find = _lowerBoundaryLine.x((int)index_find);
                                if (x_find == x_point)
                                {
                                    // found previous point at the same x location, is it at a higher y location?
                                    if (y_point > y_find)
                                        bottom_line_highest_points[f] = i;
                                    break;
                                }
                            }
                        }//else
                    }//for (int i = startIndex; i < _lowerBoundaryLine.numXPoints(); i++)
                    // create vector of lowest points on top line at each x_coord
                    List<double> top_line_lowest_points = new List<double>();

                    lowest_x = 9999999; // Huge number to begin with
                    highest_x = -1;
                    for (int i = startIndex; i < _upperBoundaryLine.numXPoints(); i++)
                    {
                        // find x and y position of each point on the line
                        double y_point = _upperBoundaryLine.y(i);
                        double x_point = _upperBoundaryLine.x(i);

                        if ((int)x_point < lowest_x)
                            lowest_x = (int)x_point;

                        if ((int)x_point > highest_x)
                        {
                            highest_x = (int)x_point;
                            // add point to vector
                            top_line_lowest_points.Add(i);
                        }
                        else
                        {
                            // check for previous occurances of x_point
                            int num = (int)(top_line_lowest_points.Count);
                            for (int f = num - 1; f >= 0; f--)
                            {
                                double index_find = top_line_lowest_points[f];

                                double y_find = _upperBoundaryLine.y((int)index_find);
                                double x_find = _upperBoundaryLine.x((int)index_find);

                                if (x_find == x_point)
                                {
                                    // found point at the same x location, is it at a lowest y location?
                                    if (y_point < y_find)
                                    {
                                        top_line_lowest_points[f] = i;
                                    }
                                    break;
                                }//if( x_find == x_point )
                            }//for( int f = num-1; f >= 0; f-- )
                        }//else
                    }//for(int i=startIndex; i<_upperBoundaryLine.numXPoints(); i++)

                    // find match x coordinates on both lines
                    int ti = 0;
                    for (int bi = 0; bi < (int)(bottom_line_highest_points.Count); bi++)
                    {
                        double index_b = bottom_line_highest_points[bi];
                        double y_b = _lowerBoundaryLine.y((int)index_b);
                        double x_b = _lowerBoundaryLine.x((int)index_b);

                        if (ti >= (int)(top_line_lowest_points.Count))
                            break;
                        double index_t = top_line_lowest_points[ti];
                        double y_t = _upperBoundaryLine.y((int)index_t);
                        double x_t = _upperBoundaryLine.x((int)index_t);
                        while (ti < (int)(top_line_lowest_points.Count))
                        { // find
                            index_t = top_line_lowest_points[ti];
                            y_t = _upperBoundaryLine.y((int)index_t);
                            x_t = _upperBoundaryLine.x((int)index_t);
                            if (x_t < x_b)
                                ti++;
                            else
                                break;
                        }

                        if (ti >= (int)(top_line_lowest_points.Count))
                            break;
                        if (x_t == x_b)
                        {
                            // found matching x coordinates
                            // find width between two lines at this point
                            double width = y_t - y_b;
                            if (width < 0)
                                width = width * -1;

                            _modeWidths.Add(width);
                        }
                    }
                }//if (_modeWidths.size() == 0)
            }//if (_modeBoundaryLinesLoaded)
            modeWidths = _modeWidths;
        }
        public void getModeWidths(out Vector modeWidths)
        {
            short startIndex = 0;
            getModeWidths(out modeWidths, startIndex);
        }




        public ModeAnalysis()
        {
            init();
        }


        public void init()
        {
            // initialise attributes before use
            _modeBoundaryLinesLoaded = false;
            _modalDistortionAnalysisRun = false;
            _modeLength = 0;

            //////////////////////////////////////////////////

            _modeWidths.Clear();
            _modeUpperSlopes.Clear();
            _modeLowerSlopes.Clear();
            _lowerModeBDCAreas.Clear();
            _upperModeBDCAreas.Clear();
            _lowerModeBDCAreaXLengths.Clear();
            _upperModeBDCAreaXLengths.Clear();

            _maxModeWidth = 0;
            _minModeWidth = 0;
            _meanModeWidths = 0;
            _sumModeWidths = 0;
            _numModeWidthSamples = 0;
            _stdDevModeWidths = 0;
            _varModeWidths = 0;

            //////////////////////////////////////////////////


            _maxModeUpperSlope = 0;
            _minModeUpperSlope = 0;
            _meanModeUpperSlopes = 0;
            _sumModeUpperSlopes = 0;
            _numModeUpperSlopeSamples = 0;
            _stdDevModeUpperSlopes = 0;
            _varModeUpperSlopes = 0;

            _maxModeLowerSlope = 0;
            _minModeLowerSlope = 0;
            _meanModeLowerSlopes = 0;
            _sumModeLowerSlopes = 0;
            _numModeLowerSlopeSamples = 0;
            _stdDevModeLowerSlopes = 0;
            _varModeLowerSlopes = 0;

            //////////////////////////////////////////////////

            _lowerMaxModeBDCArea = 0;
            _lowerMinModeBDCArea = 0;
            _lowerMeanModeBDCAreas = 0;
            _lowerSumModeBDCAreas = 0;
            _lowerNumModeBDCAreas = 0;
            _lowerStdDevModeBDCAreas = 0;
            _lowerVarModeBDCAreas = 0;

            _upperMaxModeBDCArea = 0;
            _upperMinModeBDCArea = 0;
            _upperMeanModeBDCAreas = 0;
            _upperSumModeBDCAreas = 0;
            _upperNumModeBDCAreas = 0;
            _upperStdDevModeBDCAreas = 0;
            _upperVarModeBDCAreas = 0;

            //////////////////////////////////////////////////

            _lowerMaxModeBDCAreaXLength = 0;
            _lowerMinModeBDCAreaXLength = 0;
            _lowerMeanModeBDCAreaXLengths = 0;
            _lowerSumModeBDCAreaXLengths = 0;
            _lowerNumModeBDCAreaXLengths = 0;
            _lowerStdDevModeBDCAreaXLengths = 0;
            _lowerVarModeBDCAreaXLengths = 0;

            _upperMaxModeBDCAreaXLength = 0;
            _upperMinModeBDCAreaXLength = 0;
            _upperMeanModeBDCAreaXLengths = 0;
            _upperSumModeBDCAreaXLengths = 0;
            _upperNumModeBDCAreaXLengths = 0;
            _upperStdDevModeBDCAreaXLengths = 0;
            _upperVarModeBDCAreaXLengths = 0;

            //////////////////////////////////////////////////

            _lowerModalDistortionAngle = 0;
            _maxLowerModalDistortionAngleXPosition = 0;

            _upperModalDistortionAngle = 0;
            _maxUpperModalDistortionAngleXPosition = 0;

            /////////////////////////////////////////////////

        }

        /////////////////////////////////////////////////////////////////////

        //public void setQAThresholds(QAThresholds qaThresholds)
        //{
        //    _qaThresholds = qaThresholds;
        //}

        /////////////////////////////////////////////////////////////////////

        public void loadBoundaryLines(int modeNumber,
                        int xAxisLength,
                        int yAxisLength,
                        ModeBoundaryLine upperLine,
                        ModeBoundaryLine lowerLine)
        {
            init();
            _modeNumber = modeNumber;
            _xAxisLength = xAxisLength;
            _yAxisLength = yAxisLength;
            _upperBoundaryLine = upperLine;
            _lowerBoundaryLine = lowerLine;

            if (_upperBoundaryLine.numXPoints() < _lowerBoundaryLine.numXPoints())
                _modeLength = _upperBoundaryLine.numXPoints();
            else
                _modeLength = _lowerBoundaryLine.numXPoints();

            if (_modeLength > 0)
                _modeBoundaryLinesLoaded = true;
            else
                _modeBoundaryLinesLoaded = false;
        }

        public bool runAnalysis()
        {
            bool returnValue = true;
            if (_modeBoundaryLinesLoaded)
            {
                if (returnValue == true)
                    returnValue = runModeWidthsAnalysis();
                if (returnValue == true)
                    returnValue = runModeSlopesAnalysis();
                if (returnValue == true)
                    returnValue = runModeUpperSlopesAnalysis();
                if (returnValue == true)
                    returnValue = runModeLowerSlopesAnalysis();
                if (returnValue == true)
                    returnValue = runModeModalDistortionAnalysis();
                if (returnValue == true)
                    returnValue = runModeBDCAnalysis();
            }
            return returnValue;
        }

        /////////////////////////////////////////////////////////////////////
        ///
        //
        public bool runModeWidthsAnalysis()
        {
            bool retValue = true;
            if (_modeBoundaryLinesLoaded)
            {
                Vector modeWidths;
                //GDM additional call if in SM analysis, 
                if (_qaThresholds._SMMapQA == 1)
                    getModeWidths(out modeWidths, (short)_qaThresholds._modeWidthAnalysisMinX,
                                                                (short)_qaThresholds._modeWidthAnalysisMaxX);
                if (_qaThresholds._SMMapQA == 0)
                    //original call, now just used for OM
                    getModeWidths(out modeWidths);

                calcMaxModeWidth();
                calcMinModeWidth();
                calcMeanModeWidths();
                calcSumModeWidths();
                calcNumModeWidthSamples();
                calcStdDevModeWidths();
                calcVarModeWidths();
            }
            return retValue;
        }

        ///////////////////////////////////////////////////////////////////////
        public bool runModeSlopesAnalysis()
        {
            bool retValue = true;
            if (_modeBoundaryLinesLoaded)
            {
                Vector lowerModeSlopes;
                Vector upperModeSlopes;
                getModeSlopes(out lowerModeSlopes, out upperModeSlopes);

                calcMaxModeSlope();
                calcMinModeSlope();
                calcMeanModeSlopes();
                calcSumModeSlopes();
                calcNumModeSlopeSamples();
                calcStdDevModeSlopes();
                calcVarModeSlopes();
            }
            return retValue;
        }

        ///////////////////////////////////////////////////////////////////////

        public bool runModeUpperSlopesAnalysis()
        {
            bool retValue = true;
            if (_modeBoundaryLinesLoaded)
            {
                Vector modeUpperSlopes;
                getModeUpperSlopes(out modeUpperSlopes);

                calcMaxModeUpperSlope();
                calcMinModeUpperSlope();
                calcMeanModeUpperSlopes();
                calcSumModeUpperSlopes();
                calcNumModeUpperSlopeSamples();
                calcStdDevModeUpperSlopes();
                calcVarModeUpperSlopes();
            }
            return retValue;
        }

        ///////////////////////////////////////////////////////////////////////

        public bool runModeLowerSlopesAnalysis()
        {
            bool retValue = true;
            if (_modeBoundaryLinesLoaded)
            {
                Vector modeLowerSlopes;
                getModeLowerSlopes(out modeLowerSlopes);

                calcMaxModeLowerSlope();
                calcMinModeLowerSlope();
                calcMeanModeLowerSlopes();
                calcSumModeLowerSlopes();
                calcNumModeLowerSlopeSamples();
                calcStdDevModeLowerSlopes();
                calcVarModeLowerSlopes();
            }

            return retValue;
        }

        ///////////////////////////////////////////////////////////////////////

        public bool runModeModalDistortionAnalysis()
        {
            bool retValue = true;
            if (_modeBoundaryLinesLoaded)
            {
                if (_lowerBoundaryLine.numXPoints() == _xAxisLength)
                {
                    _lowerBoundaryLine.runModalDistortionAnalysis(_qaThresholds._modalDistortionMinX,
                                                                _qaThresholds._modalDistortionMaxX);

                    _lowerModalDistortionAngle = _lowerBoundaryLine.getMaxModalDistortionAngle();
                    _maxLowerModalDistortionAngleXPosition = _lowerBoundaryLine.getMaxModalDistortionAngleXPosition();

                }

                if (_upperBoundaryLine.numXPoints() == _xAxisLength)
                {
                    _upperBoundaryLine.runModalDistortionAnalysis(_qaThresholds._modalDistortionMinX,
                                                                _qaThresholds._modalDistortionMaxX);

                    _upperModalDistortionAngle = _upperBoundaryLine.getMaxModalDistortionAngle();
                    _maxUpperModalDistortionAngleXPosition = _upperBoundaryLine.getMaxModalDistortionAngleXPosition();

                }

                _modalDistortionAnalysisRun = true;
            }

            return retValue;
        }

        ///////////////////////////////////////////////////////////////////////

        public bool runModeBDCAnalysis()
        {
            bool retValue = true;
            if (_modeBoundaryLinesLoaded)
            {
                Vector lowerModeBDCAreas;
                Vector upperModeBDCAreas;
                getModeBDCAreas(out lowerModeBDCAreas, out upperModeBDCAreas);

                calcMaxModeBDCArea();
                calcMinModeBDCArea();
                calcMeanModeBDCAreas();
                calcSumModeBDCAreas();
                calcNumModeBDCAreas();
                calcStdDevModeBDCAreas();
                calcVarModeBDCAreas();


                Vector lowerModeBDCAreaXLengths;
                Vector upperModeBDCAreaXLengths;
                getModeBDCAreaXLengths(out lowerModeBDCAreaXLengths, out upperModeBDCAreaXLengths);

                calcMaxModeBDCAreaXLength();
                calcMinModeBDCAreaXLength();
                calcMeanModeBDCAreaXLengths();
                calcSumModeBDCAreaXLengths();
                calcNumModeBDCAreaXLengths();
                calcStdDevModeBDCAreaXLengths();
                calcVarModeBDCAreaXLengths();

            }
            return retValue;
        }

        //
        /////////////////////////////////////////////////////////////////////
        //public void getModeWidths(out Vector modeWidths)
        //{
        //    //GDM additional call if in SM analysis, 
        //    if (_qaThresholds._SMMapQA == 1)
        //            getModeWidths(modeWidths,(short)_qaThresholds._modeWidthAnalysisMinX,
        //                                                        (short)_qaThresholds._modeWidthAnalysisMaxX);
        //    if (_qaThresholds._SMMapQA == 0)
        //    {	//original call, now just used for OM
        //        short startIndex = 0;
        //        getModeWidths(modeWidths, startIndex);
        //    }
        //}

        public void getModeWidths(out Vector modeWidths, short startIndex)
        {
            if (_modeBoundaryLinesLoaded)
            {
                // only do this once
                if (_modeWidths.size() == 0)
                {
                    // create vector of highest points on bottom line at each x_coord
                    Vector bottom_line_highest_points = new Vector();

                    int lowest_x = 9999999; // Huge number to begin with
                    int highest_x = -1;

                    for (int i = startIndex; i < _lowerBoundaryLine.numXPoints(); i++)
                    {
                        // find x and y position of each point on the line

                        int x_point = (int)_lowerBoundaryLine.x(i);
                        int y_point = (int)_lowerBoundaryLine.y(i);

                        if ((int)x_point < lowest_x)
                            lowest_x = (int)x_point;

                        if ((int)x_point > highest_x)
                        {
                            highest_x = (int)x_point;
                            // add point to vector
                            bottom_line_highest_points.Add(i);

                        }
                        else
                        {
                            // check for previous occurances of x_point
                            int num = (int)(bottom_line_highest_points.size());

                            for (int f = num - 1; f >= 0; f--)
                            {
                                double index_find = bottom_line_highest_points[f];

                                if ((int)index_find < 0
                                 || (int)index_find >= (int)_lowerBoundaryLine._y.Count
                                 || (int)index_find >= (int)_lowerBoundaryLine._x.Count)
                                {
                                    //CGR_LOG("ModeAnalysis::getModeWidths() : Bounds exceeded",ERROR_CGR_LOG)
                                    //log_msg.Format("ModeAnalysis::getModeWidths() index_find = %g, (int)index_find = %d",index_find,(int)index_find );
                                    //CGR_LOG(log_msg,ERROR_CGR_LOG)
                                    //log_msg.Format("ModeAnalysis::getModeWidths() _lowerBoundaryLine._x.size() = %d",_lowerBoundaryLine._x.size() );
                                    //CGR_LOG(log_msg,ERROR_CGR_LOG)
                                    //log_msg.Format("ModeAnalysis::getModeWidths() _lowerBoundaryLine._y.size() = %d",_lowerBoundaryLine._y.size() );
                                    //CGR_LOG(log_msg,ERROR_CGR_LOG)
                                }
                                else
                                {
                                    double y_find = _lowerBoundaryLine.y((int)index_find);
                                    double x_find = _lowerBoundaryLine.x((int)index_find);
                                    if (x_find == x_point)
                                    {
                                        // found previous point at the same x location, is it at a higher y location?
                                        if (y_point > y_find)
                                            bottom_line_highest_points[f] = i;

                                        break;
                                    }
                                }
                            }
                        }
                    }

                    // create vector of lowest points on top line at each x_coord
                    Vector top_line_lowest_points = new Vector();

                    lowest_x = 9999999; // Huge number to begin with
                    highest_x = -1;
                    for (int i = startIndex; i < _upperBoundaryLine.numXPoints(); i++)
                    {
                        // find x and y position of each point on the line
                        double y_point = _upperBoundaryLine.y(i);
                        double x_point = _upperBoundaryLine.x(i);

                        if ((int)x_point < lowest_x) lowest_x = (int)x_point;

                        if ((int)x_point > highest_x)
                        {
                            highest_x = (int)x_point;
                            // add point to vector
                            top_line_lowest_points.Add(i);
                        }
                        else
                        {
                            // check for previous occurances of x_point
                            int num = (int)(top_line_lowest_points.size());

                            for (int f = num - 1; f >= 0; f--)
                            {
                                double index_find = top_line_lowest_points[f];

                                if ((int)index_find < 0
                                 || (int)index_find >= (int)_upperBoundaryLine._y.Count
                                 || (int)index_find >= (int)_upperBoundaryLine._x.Count)
                                {
                                    //CGR_LOG("ModeAnalysis::getModeWidths() : Bounds exceeded",ERROR_CGR_LOG)
                                    //log_msg.Format("ModeAnalysis::getModeWidths() index_find = %g, (int)index_find = %d",index_find,(int)index_find );
                                    //CGR_LOG(log_msg,ERROR_CGR_LOG)
                                    //log_msg.Format("ModeAnalysis::getModeWidths() _upperBoundaryLine._x.size() = %d",_upperBoundaryLine._x.size() );
                                    //CGR_LOG(log_msg,ERROR_CGR_LOG)
                                    //log_msg.Format("ModeAnalysis::getModeWidths() _upperBoundaryLine._y.size() = %d",_upperBoundaryLine._y.size() );
                                    //CGR_LOG(log_msg,ERROR_CGR_LOG)
                                }
                                else
                                {
                                    double y_find = _upperBoundaryLine.y((int)index_find);
                                    double x_find = _upperBoundaryLine.x((int)index_find);

                                    if (x_find == x_point)
                                    { // found point at the same x location, is it at a lowest y location?
                                        if (y_point < y_find)
                                        {
                                            top_line_lowest_points[f] = i;
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                    }


                    double lowest_val = 99999;
                    double highest_val = -99999;
                    for (int f = 0; f < (int)bottom_line_highest_points.size(); f++)
                    {
                        lowest_val = bottom_line_highest_points[f] < lowest_val ? bottom_line_highest_points[f] : lowest_val;
                        highest_val = bottom_line_highest_points[f] > highest_val ? bottom_line_highest_points[f] : highest_val;
                    }

                    lowest_val = 99999;
                    highest_val = -99999;
                    for (int f = 0; f < (int)top_line_lowest_points.size(); f++)
                    {
                        lowest_val = top_line_lowest_points[f] < lowest_val ? top_line_lowest_points[f] : lowest_val;
                        highest_val = top_line_lowest_points[f] > highest_val ? top_line_lowest_points[f] : highest_val;
                    }

                    // find match x coordinates on both lines
                    int ti = 0;
                    for (int bi = 0; bi < (int)(bottom_line_highest_points.size()); bi++)
                    {
                        double index_b = bottom_line_highest_points[bi];
                        double y_b = _lowerBoundaryLine.y((int)index_b);
                        double x_b = _lowerBoundaryLine.x((int)index_b);

                        if (ti >= (int)(top_line_lowest_points.size())) break;
                        double index_t = top_line_lowest_points[ti];
                        double y_t = _upperBoundaryLine.y((int)index_t);
                        double x_t = _upperBoundaryLine.x((int)index_t);
                        while (ti < (int)(top_line_lowest_points.size()))
                        { // find
                            index_t = top_line_lowest_points[ti];
                            y_t = _upperBoundaryLine.y((int)index_t);
                            x_t = _upperBoundaryLine.x((int)index_t);
                            if (x_t < x_b) ti++;
                            else break;
                        }

                        if (ti >= (int)(top_line_lowest_points.size())) break;
                        if (x_t == x_b)
                        { // found matching x coordinates
                            // find width between two lines at this point
                            double width = y_t - y_b;
                            if (width < 0)
                                width = width * -1;

                            _modeWidths.Add(width);
                        }
                    }

                    //////////////////////////////////////////////////////////////////////////////
                }
            }

            modeWidths = _modeWidths;

        }
        /* getModeWidths
        //GDM 30/10/06 overloaded version of getModeWidths which allows the boundary 'map'
        //to be windowed using a start and stop index so that the width analysis can be 
        //targeted to meaningful sections of the maps, eg avoiding edge hysteresis
        public void getModeWidths(out Vector modeWidths, int startIndex,int	stopIndex)
        {
            if(_modeBoundaryLinesLoaded)
            {
                // only do this once
                if(_modeWidths.size() == 0)
                {
                    //before we start we need to check that the start and stop index's specced are 
                    //useful for this LM. ie is ther a lower and upper line that both traverse
                    // the specced window. If not then just examine the whole mode from start to end
                    // as would be done if no window specced. This will give a useful set of metrics
                    // to gauge the LM by, rather than returning zero's, and not skew any overall SM appraisals. 
                    int lowerBoundaryLineStart = (int)_lowerBoundaryLine.x(0);
                    int upperBoundaryLineEnd = (int)_upperBoundaryLine.x(_upperBoundaryLine.numXPoints()-1);
                    if ((lowerBoundaryLineStart > startIndex) || upperBoundaryLineEnd < stopIndex)
                    {
                        getModeWidths(out modeWidths, 0);	//call original func over entire length
                    }
                    else //analyse between window, we know line has an index at startIndex thru to  stopIndex
                    {
                        //////////////////////////////////////////////////////////////////////////////

                        // create vector of highest points on bottom line at each x_coord
                        Vector bottom_line_highest_points;

                        int lowest_x = 9999999; // Huge number to begin with
                        int highest_x = -1;


                        //CString log_msg;
                        //log_msg.AppendFormat("ModeAnalysis::getModeWidths() _lowerBoundaryLine._x.size() = %d",_lowerBoundaryLine._x.size() );
                        //CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

                        //log_msg.Format("ModeAnalysis::getModeWidths() _lowerBoundaryLine._y.size() = %d",_lowerBoundaryLine._y.size() );
                        //CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

                        //GDM lower boundsary analysis, ensure we don't exceed boundary or vector length, also we can set start stop = 0 to do whole vector as before
                        if (startIndex < 0)
                            startIndex = 0;
                        if (stopIndex <= startIndex)
                            stopIndex = _lowerBoundaryLine.numXPoints();
                        if (stopIndex > _lowerBoundaryLine.numXPoints())
                            stopIndex = _lowerBoundaryLine.numXPoints();

                        for(int i=startIndex; i<stopIndex; i++)
                        {
                            // find x and y position of each point on the line

                            int x_point = (int)_lowerBoundaryLine.x(i);
                            int y_point = (int)_lowerBoundaryLine.y(i);

                            if( (int)x_point < lowest_x ) 
                                    lowest_x = (int)x_point;

                            if( (int)x_point > highest_x )
                            {
                                highest_x = (int)x_point;
                                // add point to vector
                                bottom_line_highest_points.Add(i);

                            }
                            else
                            { 
                                // check for previous occurances of x_point
                                int num = (int)(bottom_line_highest_points.size());

                                for( int f = num-1; f >= 0; f-- )
                                {
                                    double index_find = bottom_line_highest_points[f];

                                    if( (int)index_find < 0
                                    || (int)index_find >= (int)_lowerBoundaryLine._y.Count
                                    || (int)index_find >= (int)_lowerBoundaryLine._x.Count )
                                    {
                                        //CGR_LOG("ModeAnalysis::getModeWidths() : Bounds exceeded",ERROR_CGR_LOG)
                                        //log_msg.Format("ModeAnalysis::getModeWidths() index_find = %g, (int)index_find = %d",index_find,(int)index_find );
                                        //CGR_LOG(log_msg,ERROR_CGR_LOG)
                                        //log_msg.Format("ModeAnalysis::getModeWidths() _lowerBoundaryLine._x.size() = %d",_lowerBoundaryLine._x.size() );
                                        //CGR_LOG(log_msg,ERROR_CGR_LOG)
                                        //log_msg.Format("ModeAnalysis::getModeWidths() _lowerBoundaryLine._y.size() = %d",_lowerBoundaryLine._y.size() );
                                        //CGR_LOG(log_msg,ERROR_CGR_LOG)
                                    }
                                    else
                                    {		
                                        double y_find = _lowerBoundaryLine.y((int)index_find);
                                        double x_find = _lowerBoundaryLine.x((int)index_find);
                                        if( x_find == x_point )
                                        { 
                                            // found previous point at the same x location, is it at a higher y location?
                                            if( y_point > y_find )
                                                bottom_line_highest_points[f] = i;

                                            break;
                                        }
                                    }
                                }
                            }
                        }

				
                        /////////////////////////////////////////////////////////////////////

                        //log_msg.Format("ModeAnalysis::getModeWidths() _upperBoundaryLine._x.size() = %d",_upperBoundaryLine._x.size() );
                        //CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

                        //log_msg.Format("ModeAnalysis::getModeWidths() _upperBoundaryLine._y.size() = %d",_upperBoundaryLine._y.size() );
                        //CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

                        // create vector of lowest points on top line at each x_coord
                        Vector top_line_lowest_points;

                        //GDM Upper boundary analysis, ensure we don't exceed boundary or vector length, also we can set start stop = 0 to do whole vector as before
                        if (startIndex < 0)
                            startIndex = 0;
                        if (stopIndex <= startIndex)
                            stopIndex = _upperBoundaryLine.numXPoints();
                        if (stopIndex > _upperBoundaryLine.numXPoints())
                            stopIndex = _upperBoundaryLine.numXPoints();


                        lowest_x = 9999999; // Huge number to begin with
                        highest_x = -1;
                        for(int i=startIndex; i<stopIndex; i++)
                        {
                            // find x and y position of each point on the line
                            double y_point = _upperBoundaryLine.y(i);
                            double x_point = _upperBoundaryLine.x(i);

                            if( (int)x_point < lowest_x ) lowest_x = (int)x_point;

                            if( (int)x_point > highest_x )
                            {
                                highest_x = (int)x_point;
                                // add point to vector
                                top_line_lowest_points.Add(i);
                            }
                            else
                            { 
                                // check for previous occurances of x_point
                                int num = (int)(top_line_lowest_points.size());

                                for( int f = num-1; f >= 0; f-- )
                                {
                                    double index_find = top_line_lowest_points[f];

                                    if( (int)index_find < 0
                                    || (int)index_find >= (int)_upperBoundaryLine._y.Count
                                    || (int)index_find >= (int)_upperBoundaryLine._x.Count )
                                    {
                                        //CGR_LOG("ModeAnalysis::getModeWidths() : Bounds exceeded",ERROR_CGR_LOG)
                                        //log_msg.Format("ModeAnalysis::getModeWidths() index_find = %g, (int)index_find = %d",index_find,(int)index_find );
                                        //CGR_LOG(log_msg,ERROR_CGR_LOG)
                                        //log_msg.Format("ModeAnalysis::getModeWidths() _upperBoundaryLine._x.size() = %d",_upperBoundaryLine._x.size() );
                                        //CGR_LOG(log_msg,ERROR_CGR_LOG)
                                        //log_msg.Format("ModeAnalysis::getModeWidths() _upperBoundaryLine._y.size() = %d",_upperBoundaryLine._y.size() );
                                        //CGR_LOG(log_msg,ERROR_CGR_LOG)
                                    }
                                    else
                                    {
                                        double y_find = _upperBoundaryLine.y((int)index_find);
                                        double x_find = _upperBoundaryLine.x((int)index_find);

                                        if( x_find == x_point )
                                        { // found point at the same x location, is it at a lowest y location?
                                            if( y_point < y_find )
                                            {
                                                top_line_lowest_points[f] = i;
                                            }
                                            break;
                                        }
                                    }
                                }
                            }
                        }


                        double lowest_val = 99999;
                        double highest_val = -99999;
                        for( int f=0; f < (int)bottom_line_highest_points.size(); f++ )
                        {
                            lowest_val = bottom_line_highest_points[f] < lowest_val ? bottom_line_highest_points[f] : lowest_val;
                            highest_val = bottom_line_highest_points[f] > highest_val ? bottom_line_highest_points[f] : highest_val;
                        }

                        //log_msg.Format("ModeAnalysis::getModeWidths()   bottom_line_highest_points  lowest_val = %g", lowest_val );
                        //CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

                        //log_msg.Format("ModeAnalysis::getModeWidths()   bottom_line_highest_points  highest_val = %g", highest_val );
                        //CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

                        //log_msg.Format("ModeAnalysis::getModeWidths()   _lowerBoundaryLine._x.size() = %d",_lowerBoundaryLine._x.size() );
                        //CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

                        //log_msg.Format("ModeAnalysis::getModeWidths()   _lowerBoundaryLine._y.size() = %d",_lowerBoundaryLine._y.size() );
                        //CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)


                        lowest_val = 99999;
                        highest_val = -99999;
                        for( int f=0; f < (int)top_line_lowest_points.size(); f++ )
                        {
                            lowest_val = top_line_lowest_points[f] < lowest_val ? top_line_lowest_points[f] : lowest_val;
                            highest_val = top_line_lowest_points[f] > highest_val ? top_line_lowest_points[f] : highest_val;
                        }

                        //log_msg.Format("ModeAnalysis::getModeWidths()   top_line_lowest_points  lowest_val = %g", lowest_val );
                        //CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

                        //log_msg.Format("ModeAnalysis::getModeWidths()   top_line_lowest_points  highest_val = %g", highest_val );
                        //CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

                        //log_msg.Format("ModeAnalysis::getModeWidths()   _upperBoundaryLine._x.size() = %d",_upperBoundaryLine._x.size() );
                        //CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)

                        //log_msg.Format("ModeAnalysis::getModeWidths()   _upperBoundaryLine._y.size() = %d",_upperBoundaryLine._y.size() );
                        //CGR_LOG(log_msg,DIAGNOSTIC_CGR_LOG)


                        // find match x coordinates on both lines
                        int ti = 0;
                        for( int bi = 0; bi < (int)(bottom_line_highest_points.size()) ; bi++ )
                        {
                            double index_b = bottom_line_highest_points[bi];
                            double y_b = _lowerBoundaryLine.y((int)index_b);
                            double x_b = _lowerBoundaryLine.x((int)index_b);
		            
                            if( ti >= (int)(top_line_lowest_points.size()) ) break;
                            double index_t = top_line_lowest_points[ti];
                            double y_t = _upperBoundaryLine.y((int)index_t);
                            double x_t = _upperBoundaryLine.x((int)index_t);
                            while(ti < (int)(top_line_lowest_points.size()) )
                            { // find
                                index_t = top_line_lowest_points[ti];
                                y_t = _upperBoundaryLine.y((int)index_t);
                                x_t = _upperBoundaryLine.x((int)index_t);
                                if( x_t < x_b ) ti++;
                                else break;
                            }

                            if( ti >= (int)(top_line_lowest_points.size()) ) break;
                            if( x_t == x_b )
                            { // found matching x coordinates
                            // find width between two lines at this point
                                double width = y_t - y_b;
                                if(width < 0)
                                    width = width * -1;

                                _modeWidths.Add(width);
                            }
                        }

                        //////////////////////////////////////////////////////////////////////////////
                    }
                }
            }
            modeWidths = _modeWidths;

        }
        */
        public double getModeWidthsStatistic(VectorAttribute attrib)
        {
            double returnValue = 0;

            if (_modeBoundaryLinesLoaded)
            {
                Vector modeWidths;
                getModeWidths(out modeWidths);

                returnValue = modeWidths.getVectorAttribute(attrib);
            }

            return returnValue;
        }
        //
        ////////////////////////////////////////////////////////////////////////////////////////////
        //
        public void calcMaxModeWidth()
        {
            if (_modeBoundaryLinesLoaded)
            {
                if (_maxModeWidth == 0)
                {
                    Vector modeWidths;
                    getModeWidths(out modeWidths);

                    _maxModeWidth = modeWidths.getVectorAttribute(VectorAttribute.max);
                }
            }
        }
        public void calcMinModeWidth()
        {
            if (_modeBoundaryLinesLoaded)
            {
                if (_minModeWidth == 0)
                {
                    Vector modeWidths;
                    getModeWidths(out modeWidths);

                    _minModeWidth = modeWidths.getVectorAttribute(VectorAttribute.min);
                }
            }
        }
        public void calcMeanModeWidths()
        {
            if (_modeBoundaryLinesLoaded)
            {
                if (_meanModeWidths == 0)
                {
                    Vector modeWidths;
                    getModeWidths(out modeWidths);

                    _meanModeWidths = modeWidths.getVectorAttribute(VectorAttribute.mean);
                }
            }
        }
        public void calcSumModeWidths()
        {
            if (_modeBoundaryLinesLoaded)
            {
                if (_sumModeWidths == 0)
                {
                    Vector modeWidths;
                    getModeWidths(out modeWidths);

                    _sumModeWidths = modeWidths.getVectorAttribute(VectorAttribute.sum);
                }
            }
        }
        public void calcNumModeWidthSamples()
        {
            if (_modeBoundaryLinesLoaded)
            {
                if (_modeLength == 0)
                {
                    Vector modeWidths;
                    getModeWidths(out modeWidths);

                    _modeLength = (int)modeWidths.getVectorAttribute(VectorAttribute.count);
                }
            }
        }
        public void calcVarModeWidths()
        {
            if (_modeBoundaryLinesLoaded)
            {
                if (_varModeWidths == 0)
                {
                    Vector modeWidths;
                    getModeWidths(out modeWidths);

                    _varModeWidths = modeWidths.getVectorAttribute(VectorAttribute.variance);
                }
            }
        }
        public void calcStdDevModeWidths()
        {
            if (_modeBoundaryLinesLoaded)
            {
                if (_stdDevModeWidths == 0)
                {
                    Vector modeWidths;
                    getModeWidths(out modeWidths);

                    _stdDevModeWidths = modeWidths.getVectorAttribute(VectorAttribute.stdDev);
                }
            }
        }
        //
        /////////////////////////////////////////////////////////////////////
        ///
        //
        public void getModeUpperSlopes(out Vector modeUpperSlopes)
        {
            if (_modeBoundaryLinesLoaded)
            {
                // only do this once
                if (_modeUpperSlopes.size() == 0)
                {
                    List<double> tempList = _upperBoundaryLine.getSlopes(_qaThresholds._slopeWindow);
                    for (int i = 0; i < tempList.Count; i++)
                    {
                        _modeUpperSlopes.Add(tempList[i]);
                    }

                }
            }

            modeUpperSlopes = _modeUpperSlopes;
        }
        public double getModeUpperSlopesStatistic(VectorAttribute attrib)
        {
            double returnValue = 0;

            if (_modeBoundaryLinesLoaded)
            {
                Vector modeUpperSlopes;
                getModeUpperSlopes(out modeUpperSlopes);

                returnValue = modeUpperSlopes.getVectorAttribute(attrib);
            }

            return returnValue;
        }
        //
        /////////////////////////////////////////////////////////////////////
        ///
        //
        public void getModeSlopes(out Vector lowerModeSlopes,
                      out Vector upperModeSlopes)
        {
            if (_modeBoundaryLinesLoaded)
            {
                // only do this once
                if (_modeLowerSlopes.size() == 0)
                {
                    List<double> modeLowerSlopes = _lowerBoundaryLine.getSlopes(_qaThresholds._slopeWindow);
                    List<double> modeUpperSlopes = _upperBoundaryLine.getSlopes(_qaThresholds._slopeWindow);
                    for (int i = 0; i < modeLowerSlopes.Count; i++)
                    {
                        _modeLowerSlopes.Add(modeLowerSlopes[i]);
                    }
                    for (int i = 0; i < modeUpperSlopes.Count; i++)
                    {
                        _modeUpperSlopes.Add(modeUpperSlopes[i]);
                    }
                }
            }

            lowerModeSlopes = _modeLowerSlopes;
            upperModeSlopes = _modeUpperSlopes;
        }

        //
        ////////////////////////////////////////////////////////////////////////////////////////////
        //
        public void calcMaxModeSlope()
        {
            if (_modeBoundaryLinesLoaded)
            {
                calcMaxModeLowerSlope();
                calcMaxModeUpperSlope();
            }
        }
        public void calcMinModeSlope()
        {
            if (_modeBoundaryLinesLoaded)
            {
                calcMinModeLowerSlope();
                calcMinModeUpperSlope();
            }
        }
        public void calcMeanModeSlopes()
        {
            if (_modeBoundaryLinesLoaded)
            {
                calcMeanModeLowerSlopes();
                calcMeanModeUpperSlopes();
            }
        }
        public void calcSumModeSlopes()
        {
            if (_modeBoundaryLinesLoaded)
            {
                calcSumModeLowerSlopes();
                calcSumModeUpperSlopes();
            }
        }
        public void calcNumModeSlopeSamples()
        {
            if (_modeBoundaryLinesLoaded)
            {
                calcNumModeLowerSlopeSamples();
                calcNumModeUpperSlopeSamples();
            }
        }
        public void calcVarModeSlopes()
        {
            if (_modeBoundaryLinesLoaded)
            {
                calcVarModeLowerSlopes();
                calcVarModeUpperSlopes();
            }
        }
        public void calcStdDevModeSlopes()
        {
            if (_modeBoundaryLinesLoaded)
            {
                calcStdDevModeLowerSlopes();
                calcStdDevModeUpperSlopes();
            }
        }
        //
        ////////////////////////////////////////////////////////////////////////////////////////////
        //
        public void calcMaxModeUpperSlope()
        {
            if (_modeBoundaryLinesLoaded)
            {
                if (_maxModeUpperSlope == 0)
                {
                    Vector modeUpperSlopes;
                    getModeUpperSlopes(out modeUpperSlopes);

                    _maxModeUpperSlope = modeUpperSlopes.getVectorAttribute(VectorAttribute.max);
                }
            }
        }
        public void calcMinModeUpperSlope()
        {
            if (_modeBoundaryLinesLoaded)
            {
                if (_minModeUpperSlope == 0)
                {
                    Vector modeUpperSlopes;
                    getModeUpperSlopes(out modeUpperSlopes);

                    _minModeUpperSlope = modeUpperSlopes.getVectorAttribute(VectorAttribute.min);
                }
            }
        }
        public void calcMeanModeUpperSlopes()
        {
            if (_modeBoundaryLinesLoaded)
            {
                if (_meanModeUpperSlopes == 0)
                {
                    Vector modeUpperSlopes;
                    getModeUpperSlopes(out modeUpperSlopes);

                    _meanModeUpperSlopes = modeUpperSlopes.getVectorAttribute(VectorAttribute.mean);
                }
            }
        }
        public void calcSumModeUpperSlopes()
        {
            if (_modeBoundaryLinesLoaded)
            {
                if (_sumModeUpperSlopes == 0)
                {
                    Vector modeUpperSlopes;
                    getModeUpperSlopes(out modeUpperSlopes);

                    _sumModeUpperSlopes = modeUpperSlopes.getVectorAttribute(VectorAttribute.sum);
                }
            }
        }
        public void calcNumModeUpperSlopeSamples()
        {
            if (_modeBoundaryLinesLoaded)
            {
                if (_modeLength == 0)
                {
                    Vector modeUpperSlopes;
                    getModeUpperSlopes(out modeUpperSlopes);

                    _modeLength = (int)modeUpperSlopes.getVectorAttribute(VectorAttribute.count);
                }
            }
        }
        public void calcVarModeUpperSlopes()
        {
            if (_modeBoundaryLinesLoaded)
            {
                if (_varModeUpperSlopes == 0)
                {
                    Vector modeUpperSlopes;
                    getModeUpperSlopes(out modeUpperSlopes);

                    _varModeUpperSlopes = modeUpperSlopes.getVectorAttribute(VectorAttribute.variance);
                }
            }
        }
        public void calcStdDevModeUpperSlopes()
        {
            if (_modeBoundaryLinesLoaded)
            {
                if (_stdDevModeUpperSlopes == 0)
                {
                    Vector modeUpperSlopes;
                    getModeUpperSlopes(out modeUpperSlopes);

                    _stdDevModeUpperSlopes = modeUpperSlopes.getVectorAttribute(VectorAttribute.stdDev);
                }
            }
        }
        //
        /////////////////////////////////////////////////////////////////////
        ///
        //
        public void getModeLowerSlopes(out Vector modeLowerSlopes)
        {
            if (_modeBoundaryLinesLoaded)
            {
                // only do this once
                if (_modeLowerSlopes.size() == 0)
                {
                    List<double> tempModeLowerSlopes = _lowerBoundaryLine.getSlopes(_qaThresholds._slopeWindow);
                    for (int i = 0; i < tempModeLowerSlopes.Count; i++)
                    {
                        _modeLowerSlopes.Add(tempModeLowerSlopes[i]);
                    }
                }

            }

            modeLowerSlopes = _modeLowerSlopes;
        }
        public double getModeLowerSlopesStatistic(VectorAttribute attrib)
        {
            double returnValue = 0;

            if (_modeBoundaryLinesLoaded)
            {
                Vector modeLowerSlopes;
                getModeLowerSlopes(out modeLowerSlopes);

                returnValue = modeLowerSlopes.getVectorAttribute(attrib);
            }

            return returnValue;
        }
        //
        ////////////////////////////////////////////////////////////////////////////////////////////
        //
        public void calcMaxModeLowerSlope()
        {
            if (_modeBoundaryLinesLoaded)
            {
                if (_maxModeLowerSlope == 0)
                {
                    Vector modeLowerSlopes;
                    getModeLowerSlopes(out modeLowerSlopes);

                    _maxModeLowerSlope = modeLowerSlopes.getVectorAttribute(VectorAttribute.max);
                }
            }
        }
        public void calcMinModeLowerSlope()
        {
            if (_modeBoundaryLinesLoaded)
            {
                if (_minModeLowerSlope == 0)
                {
                    Vector modeLowerSlopes;
                    getModeLowerSlopes(out modeLowerSlopes);

                    _minModeLowerSlope = modeLowerSlopes.getVectorAttribute(VectorAttribute.min);
                }
            }
        }
        public void calcMeanModeLowerSlopes()
        {
            if (_modeBoundaryLinesLoaded)
            {
                if (_meanModeLowerSlopes == 0)
                {
                    Vector modeLowerSlopes;
                    getModeLowerSlopes(out modeLowerSlopes);

                    _meanModeLowerSlopes = modeLowerSlopes.getVectorAttribute(VectorAttribute.mean);
                }
            }
        }
        public void calcSumModeLowerSlopes()
        {
            if (_modeBoundaryLinesLoaded)
            {
                if (_sumModeLowerSlopes == 0)
                {
                    Vector modeLowerSlopes;
                    getModeLowerSlopes(out modeLowerSlopes);

                    _sumModeLowerSlopes = modeLowerSlopes.getVectorAttribute(VectorAttribute.sum);
                }
            }
        }
        public void calcNumModeLowerSlopeSamples()
        {
            if (_modeBoundaryLinesLoaded)
            {
                if (_modeLength == 0)
                {
                    Vector modeLowerSlopes;
                    getModeLowerSlopes(out modeLowerSlopes);

                    _modeLength = (int)modeLowerSlopes.getVectorAttribute(VectorAttribute.count);
                }
            }
        }
        public void calcVarModeLowerSlopes()
        {
            if (_modeBoundaryLinesLoaded)
            {
                if (_varModeLowerSlopes == 0)
                {
                    Vector modeLowerSlopes;
                    getModeLowerSlopes(out modeLowerSlopes);

                    _varModeLowerSlopes = modeLowerSlopes.getVectorAttribute(VectorAttribute.variance);
                }
            }
        }
        public void calcStdDevModeLowerSlopes()
        {
            if (_modeBoundaryLinesLoaded)
            {
                if (_stdDevModeLowerSlopes == 0)
                {
                    Vector modeLowerSlopes;
                    getModeLowerSlopes(out modeLowerSlopes);

                    _stdDevModeLowerSlopes = modeLowerSlopes.getVectorAttribute(VectorAttribute.stdDev);
                }
            }
        }

        ///////////////////////////////////////////////////////////////////////////////

        public double getLowerModeModalDistortionAngle()
        {
            return _lowerModalDistortionAngle;
        }

        public double getLowerModeMaxModalDistortionAngleXPosition()
        {
            return _maxLowerModalDistortionAngleXPosition;
        }

        public double getUpperModeModalDistortionAngle()
        {
            return _upperModalDistortionAngle;
        }

        public double getUpperModeMaxModalDistortionAngleXPosition()
        {
            return _maxUpperModalDistortionAngleXPosition;
        }

        ///////////////////////////////////////////////////////////////////////////////

        public void getModeBDCAreas(out Vector lowerModeBDCAreas,
                        out Vector upperModeBDCAreas)
        {
            if (_modeBoundaryLinesLoaded)
            {
                // only do this once
                if (_lowerModeBDCAreas.size() == 0)
                    _lowerModeBDCAreas = _lowerBoundaryLine.getModeBDCAreas();

                if (_upperModeBDCAreas.size() == 0)
                    _upperModeBDCAreas = _upperBoundaryLine.getModeBDCAreas();
            }

            lowerModeBDCAreas = _lowerModeBDCAreas;
            upperModeBDCAreas = _upperModeBDCAreas;
        }

        public double getLowerModeBDCAreasStatistic(VectorAttribute attrib)
        {
            double returnValue = 0;

            if (_modeBoundaryLinesLoaded)
            {
                Vector lowerModeBDCAreas;
                Vector upperModeBDCAreas;
                getModeBDCAreas(out lowerModeBDCAreas, out upperModeBDCAreas);

                returnValue = lowerModeBDCAreas.getVectorAttribute(attrib);
            }

            return returnValue;
        }
        public double getUpperModeBDCAreasStatistic(VectorAttribute attrib)
        {
            double returnValue = 0;

            if (_modeBoundaryLinesLoaded)
            {
                Vector lowerModeBDCAreas;
                Vector upperModeBDCAreas;
                getModeBDCAreas(out lowerModeBDCAreas, out upperModeBDCAreas);

                returnValue = upperModeBDCAreas.getVectorAttribute(attrib);
            }

            return returnValue;
        }
        public void calcMaxModeBDCArea()
        {
            if (_modeBoundaryLinesLoaded)
            {
                if (_lowerMaxModeBDCArea == 0)
                    _lowerMaxModeBDCArea = _lowerBoundaryLine.getMaxModeBDCArea();

                if (_upperMaxModeBDCArea == 0)
                    _upperMaxModeBDCArea = _upperBoundaryLine.getMaxModeBDCArea();
            }
        }
        public void calcMinModeBDCArea()
        {
            if (_modeBoundaryLinesLoaded)
            {
                if (_lowerMinModeBDCArea == 0)
                    _lowerMinModeBDCArea = _lowerBoundaryLine.getMinModeBDCArea();

                if (_upperMinModeBDCArea == 0)
                    _upperMinModeBDCArea = _upperBoundaryLine.getMinModeBDCArea();

            }
        }
        public void calcMeanModeBDCAreas()
        {
            if (_modeBoundaryLinesLoaded)
            {
                if (_lowerMeanModeBDCAreas == 0)
                    _lowerMeanModeBDCAreas = _lowerBoundaryLine.getMeanModeBDCAreas();
                if (_upperMeanModeBDCAreas == 0)
                    _upperMeanModeBDCAreas = _upperBoundaryLine.getMeanModeBDCAreas();
            }
        }
        public void calcSumModeBDCAreas()
        {
            if (_modeBoundaryLinesLoaded)
            {
                if (_lowerSumModeBDCAreas == 0)
                    _lowerSumModeBDCAreas = _lowerBoundaryLine.getSumModeBDCAreas();
                if (_upperMeanModeBDCAreas == 0)
                    _upperMeanModeBDCAreas = _upperBoundaryLine.getMeanModeBDCAreas();
            }
        }
        public void calcNumModeBDCAreas()
        {
            if (_modeBoundaryLinesLoaded)
            {
                if (_lowerNumModeBDCAreas == 0)
                    _lowerNumModeBDCAreas = _lowerBoundaryLine.getNumModeBDCAreas();
                if (_upperMeanModeBDCAreas == 0)
                    _upperMeanModeBDCAreas = _upperBoundaryLine.getMeanModeBDCAreas();
            }
        }
        public void calcStdDevModeBDCAreas()
        {
            if (_modeBoundaryLinesLoaded)
            {
                if (_lowerStdDevModeBDCAreas == 0)
                    _lowerStdDevModeBDCAreas = _lowerBoundaryLine.getStdDevModeBDCAreas();
                if (_upperMeanModeBDCAreas == 0)
                    _upperMeanModeBDCAreas = _upperBoundaryLine.getMeanModeBDCAreas();
            }
        }
        public void calcVarModeBDCAreas()
        {
            if (_modeBoundaryLinesLoaded)
            {
                if (_lowerVarModeBDCAreas == 0)
                    _lowerVarModeBDCAreas = _lowerBoundaryLine.getVarModeBDCAreas();
                if (_upperMeanModeBDCAreas == 0)
                    _upperMeanModeBDCAreas = _upperBoundaryLine.getMeanModeBDCAreas();
            }
        }
        ///////////////////////////////////////////////////////////////////////////////

        public void getModeBDCAreaXLengths(out Vector lowerModeBDCAreaXLengths, out Vector upperModeBDCAreaXLengths)
        {
            if (_modeBoundaryLinesLoaded)
            {
                // only do this once
                if (_lowerModeBDCAreaXLengths.size() == 0)
                    _lowerModeBDCAreaXLengths = _lowerBoundaryLine.getModeBDCAreaXLengths();

                if (_upperModeBDCAreaXLengths.size() == 0)
                    _upperModeBDCAreaXLengths = _lowerBoundaryLine.getModeBDCAreaXLengths();
            }

            lowerModeBDCAreaXLengths = _lowerModeBDCAreaXLengths;
            upperModeBDCAreaXLengths = _upperModeBDCAreaXLengths;

        }
        public double getLowerModeBDCAreaXLengthsStatistic(VectorAttribute attrib)
        {
            double returnValue = 0;

            if (_modeBoundaryLinesLoaded)
            {
                Vector lowerModeBDCAreaXLengths;
                Vector upperModeBDCAreaXLengths;
                getModeBDCAreaXLengths(out lowerModeBDCAreaXLengths, out upperModeBDCAreaXLengths);

                returnValue = lowerModeBDCAreaXLengths.getVectorAttribute(attrib);
            }

            return returnValue;
        }
        public double getUpperModeBDCAreaXLengthsStatistic(VectorAttribute attrib)
        {
            double returnValue = 0;

            if (_modeBoundaryLinesLoaded)
            {
                Vector lowerModeBDCAreaXLengths;
                Vector upperModeBDCAreaXLengths;
                getModeBDCAreaXLengths(out lowerModeBDCAreaXLengths, out upperModeBDCAreaXLengths);

                returnValue = upperModeBDCAreaXLengths.getVectorAttribute(attrib);
            }

            return returnValue;
        }

        public void calcMaxModeBDCAreaXLength()
        {
            if (_modeBoundaryLinesLoaded)
            {
                if (_lowerMaxModeBDCAreaXLength == 0)
                    _lowerMaxModeBDCAreaXLength = _lowerBoundaryLine.getMaxModeBDCAreaXLength();
                if (_upperMaxModeBDCAreaXLength == 0)
                    _upperMaxModeBDCAreaXLength = _upperBoundaryLine.getMaxModeBDCAreaXLength();
            }
        }
        public void calcMinModeBDCAreaXLength()
        {
            if (_modeBoundaryLinesLoaded)
            {
                if (_lowerMinModeBDCAreaXLength == 0)
                    _lowerMinModeBDCAreaXLength = _lowerBoundaryLine.getMinModeBDCAreaXLength();
                if (_upperMinModeBDCAreaXLength == 0)
                    _upperMinModeBDCAreaXLength = _upperBoundaryLine.getMinModeBDCAreaXLength();

            }
        }
        public void calcMeanModeBDCAreaXLengths()
        {
            if (_modeBoundaryLinesLoaded)
            {
                if (_lowerMeanModeBDCAreaXLengths == 0)
                    _lowerMeanModeBDCAreaXLengths = _lowerBoundaryLine.getMeanModeBDCAreaXLengths();
                if (_upperMeanModeBDCAreaXLengths == 0)
                    _upperMeanModeBDCAreaXLengths = _upperBoundaryLine.getMeanModeBDCAreaXLengths();

            }
        }
        public void calcSumModeBDCAreaXLengths()
        {
            if (_modeBoundaryLinesLoaded)
            {
                if (_lowerSumModeBDCAreaXLengths == 0)
                    _lowerSumModeBDCAreaXLengths = _lowerBoundaryLine.getSumModeBDCAreaXLengths();
                if (_upperSumModeBDCAreaXLengths == 0)
                    _upperSumModeBDCAreaXLengths = _upperBoundaryLine.getSumModeBDCAreaXLengths();
            }
        }
        public void calcNumModeBDCAreaXLengths()
        {
            if (_modeBoundaryLinesLoaded)
            {
                if (_lowerNumModeBDCAreaXLengths == 0)
                    _lowerNumModeBDCAreaXLengths = _lowerBoundaryLine.getNumModeBDCAreaXLengths();
                if (_upperNumModeBDCAreaXLengths == 0)
                    _upperNumModeBDCAreaXLengths = _upperBoundaryLine.getNumModeBDCAreaXLengths();
            }
        }
        public void calcStdDevModeBDCAreaXLengths()
        {
            if (_modeBoundaryLinesLoaded)
            {
                if (_lowerStdDevModeBDCAreaXLengths == 0)
                    _lowerStdDevModeBDCAreaXLengths = _lowerBoundaryLine.getStdDevModeBDCAreaXLengths();
                if (_upperStdDevModeBDCAreaXLengths == 0)
                    _upperStdDevModeBDCAreaXLengths = _upperBoundaryLine.getStdDevModeBDCAreaXLengths();
            }
        }
        public void calcVarModeBDCAreaXLengths()
        {
            if (_modeBoundaryLinesLoaded)
            {
                if (_lowerVarModeBDCAreaXLengths == 0)
                    _lowerVarModeBDCAreaXLengths = _lowerBoundaryLine.getVarModeBDCAreaXLengths();
                if (_upperVarModeBDCAreaXLengths == 0)
                    _upperVarModeBDCAreaXLengths = _upperBoundaryLine.getVarModeBDCAreaXLengths();
            }
        }

    }

}

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.Instruments;

namespace Bookham.fcumapping.CommonData
{
    public class CDSDBROperatingPoint
    {
        //constructor
        public CDSDBROperatingPoint() { }

        #region public datas
        public int _ITU_channel_number;
        public double _ITU_frequency;
        public int _sm_number;
        public int _lm_number;
        public int _index_on_middle_line_of_frequency;
        public double _real_index_on_middle_line_of_frequency;

        public long _row_on_sm_map;
        public long _col_on_sm_map;
        public double _Pr_on_sm_map;
        public double _Lr_on_sm_map;
        public short _front_pair_number;
        public double _constantCurrent;
        public double _nonConstantCurrent;
        public double _I_gain;
        public double _I_soa;
        public double _I_rear;
        public double _I_phase;
        public double _I_rearsoa;

        public double _real_row_on_sm_map;
        public double _real_col_on_sm_map;

        public double _F_measured;
        public double _F_Forward_PowerRatio;
        public double _F_Forward_LockerRatio;
        public double _F_Reverse_PowerRatio;
        public double _F_Reverse_LockerRatio;
        public double _F_calibration;
        public bool _is_an_ITU_point;
        public bool _stable_F_measurement;
        public long _row_of_boundary_above;
        public long _col_of_boundary_above;
        public long _row_of_boundary_below;
        public long _col_of_boundary_below;
        public long _row_of_boundary_to_left;
        public long _col_of_boundary_to_left;
        public long _row_of_boundary_to_right;
        public long _col_of_boundary_to_right;
        public double _Initial_Freq_GHz;
        public double _Initial_Lr_on_sm_map;
        public double _slope_freq_to_index;
        public double _DUT_locker_Etalon_mA;
        public double _DUT_locker_Reference_mA;
        public double _I_rearsoa_Fine_mA;
        public double _DUT_Fine_locker_Etalon_mA;
        public double _DUT_Fine_locker_Reference_mA;
        public int _Tunning_Count;
        private bool SetPartialCurrentForFcu = false;

        #endregion

        public void clear()
        {
            _ITU_channel_number = 0;
            _ITU_frequency = 0;

            _sm_number = 0;
            _lm_number = 0;
            _index_on_middle_line_of_frequency = 0;
            _real_index_on_middle_line_of_frequency = 0;

            _row_on_sm_map = 0;
            _col_on_sm_map = 0;
            _Pr_on_sm_map = 0;

            _front_pair_number = 0;
            _I_gain = 0;
            _I_soa = 0;
            _I_rear = 0;
            _I_phase = 0;
            _I_rearsoa = 0;
            _constantCurrent = 0;
            _nonConstantCurrent = 0;

            _row_of_boundary_above = 0;
            _col_of_boundary_above = 0;
            _row_of_boundary_below = 0;
            _col_of_boundary_below = 0;
            _row_of_boundary_to_left = 0;
            _col_of_boundary_to_left = 0;
            _row_of_boundary_to_right = 0;
            _col_of_boundary_to_right = 0;
            _slope_freq_to_index = 0;
            _stable_F_measurement = false;
            _is_an_ITU_point = false;
        }
        public void ReCountRearSoa(double rearsoa_slope, double b, double max_rearsoa_mA, double min_rearsoa_mA)
        {
            this._I_rearsoa =Math.Min(rearsoa_slope * this._I_rear + b,max_rearsoa_mA);
            this._I_rearsoa = Math.Max(this._I_rearsoa, min_rearsoa_mA);
        }

       /* public bool ApplyByASIC(Inst_Fcu2Asic fcu, double rearsoa_slope, double b)
        {
            bool returnValue = true;

            try
            {
                //Echo new added 2010-08-17
                this._I_rearsoa = rearsoa_slope * this._I_rear + b;
                //endadded

                if (!SetPartialCurrentForFcu)
                {
                    fcu.ISoa_mA_ByAsic = (float)this._I_soa;
                    fcu.IGain_mA_ByAsic = (float)this._I_gain;
                    fcu.IRearSoa_mA_ByAsic = (float)this._I_rearsoa;
                    SetPartialCurrentForFcu = true;
                }
                fcu.IPhase_mA_ByAsic = (float)this._I_phase;
                fcu.IRear_mA_ByAsic= (float)this._I_rear;
                //fcu.SetFrontPairCurrent_mAbyAsic(this._front_pair_number, (float)(this._constantCurrent), (float)(this._nonConstantCurrent)); // jack for ASIC
                fcu.SetFrontSectionPairCurrent(this._front_pair_number, this._constantCurrent, this._nonConstantCurrent);
                //给DSDBR加电，需要fcu Driver来实现具体的功能

            }
            catch (Exception)
            {

                returnValue = false;
            }
            return returnValue;
        }*/
        /// <summary>
        /// Apply Current Via Asic
        /// </summary>
        /// <param name="fcu"></param>
        /// <returns></returns>
        public bool Apply(Inst_Fcu2Asic fcu)
        {
            bool returnValue = true;

            try
            {
                if (!SetPartialCurrentForFcu)
                {
                    fcu.IGain_mA = (float)this._I_gain;
                    fcu.ISoa_mA = (float)this._I_soa;
                    SetPartialCurrentForFcu = true;
                }
                fcu.IRearSoa_mA = (float)((this._I_rearsoa_Fine_mA==0)?this._I_rearsoa:this._I_rearsoa_Fine_mA);
                fcu.IRear_mA = (float)this._I_rear;
                fcu.IPhase_mA = (float)this._I_phase;
                fcu.SetFrontSectionPairCurrent(this._front_pair_number, (float)this._constantCurrent, (float)this._nonConstantCurrent);// for FCU
                //给DSDBR加电，需要fcu Driver来实现具体的功能
            }
            catch (Exception)
            {

                returnValue = false;
            }
            return returnValue;
        }
        /// <summary>
        /// Apply current via Fcu Mark I
        /// </summary>
        /// <param name="fcu"></param>
        /// <returns></returns>
        public bool ApplyByFCUMKI(Inst_Fcu2Asic fcu)
        {
            return true;
        }
       
        public CDSDBROperatingPoint(CDSDBRSuperMode i_sm,
            CDSDBRSupermodeMapLine i_lm_line,
            int sm_number, int lm_number, int i_of_pt_on_line)
        {
            clear();
            _row_on_sm_map = i_lm_line._points[i_of_pt_on_line].row;
            _col_on_sm_map = i_lm_line._points[i_of_pt_on_line].col;
            _real_row_on_sm_map = i_lm_line._points[i_of_pt_on_line].row;
            _real_col_on_sm_map = i_lm_line._points[i_of_pt_on_line].col;

            _Pr_on_sm_map = i_sm._map_forward_photodiode1_current.getValue((int)_row_on_sm_map,
                (int)_col_on_sm_map);
            _sm_number = sm_number;
            _lm_number = lm_number;
            _index_on_middle_line_of_frequency = i_of_pt_on_line;
            _real_index_on_middle_line_of_frequency = (double)i_of_pt_on_line;
            _front_pair_number = i_lm_line._points[i_of_pt_on_line].front_pair_number;
            _I_gain = i_sm.I_gain;
            _I_soa = i_sm.I_soa;
            _I_rearsoa = i_sm.I_rearsoa;
            _I_rear = i_lm_line._points[i_of_pt_on_line].I_rear;
            _I_phase = i_lm_line._points[i_of_pt_on_line].I_phase;
            _constantCurrent = i_lm_line._points[i_of_pt_on_line].constantCurrent;
            _nonConstantCurrent = i_lm_line._points[i_of_pt_on_line].nonConstantCurrent;
            _F_Forward_PowerRatio = i_lm_line._points[i_of_pt_on_line].Forward_PowerRatio;
            _F_Forward_LockerRatio = i_lm_line._points[i_of_pt_on_line].Forward_LockerRatio;
            _F_Reverse_PowerRatio = i_lm_line._points[i_of_pt_on_line].Reverse_PowerRatio;
            _F_Reverse_LockerRatio = i_lm_line._points[i_of_pt_on_line].Reverse_LockerRatio;
        }
        public CDSDBROperatingPoint(CDSDBRSuperMode i_sm,
           CDSDBRSupermodeMapLine i_lm_line,
           int sm_number, int lm_number, double interpolatable_i_of_pt_on_line)
        {
            clear();
            int i_of_pt_on_line = (int)interpolatable_i_of_pt_on_line;


            long row_i_number = i_lm_line._points[i_of_pt_on_line].row;
            long col_i_number = i_lm_line._points[i_of_pt_on_line].col;

            double row_between_ab = (double)row_i_number;
            double col_between_ab = (double)col_i_number;

            if ((i_of_pt_on_line + 1) < i_lm_line._points.Count)
            {
                long row_a = i_lm_line._points[i_of_pt_on_line].row;
                long col_a = i_lm_line._points[i_of_pt_on_line].col;
                long row_b = i_lm_line._points[i_of_pt_on_line + 1].row;
                long col_b = i_lm_line._points[i_of_pt_on_line + 1].col;

                double fraction = interpolatable_i_of_pt_on_line - (double)i_of_pt_on_line;
                row_between_ab = (double)row_a + fraction * ((double)row_b - (double)row_a);
                col_between_ab = (double)col_a + fraction * ((double)col_b - (double)col_a);
            }

            CDSDBRSupermodeMapLine.Point_Type pt_between_ab = i_sm.interpolateLinePoint(
                row_between_ab, col_between_ab);
            _row_on_sm_map = pt_between_ab.row;
            _col_on_sm_map = pt_between_ab.col;
            _Pr_on_sm_map = i_sm._map_forward_photodiode1_current.getValue((int)_row_on_sm_map,
                (int)_col_on_sm_map);
            _sm_number = sm_number;
            _lm_number = lm_number;
            _index_on_middle_line_of_frequency = i_of_pt_on_line;
            _real_index_on_middle_line_of_frequency = interpolatable_i_of_pt_on_line;
            _real_row_on_sm_map = pt_between_ab.real_row;
            _real_col_on_sm_map = pt_between_ab.real_col;
            _front_pair_number = pt_between_ab.front_pair_number;
            _I_gain = i_sm.I_gain;
            _I_soa = i_sm.I_soa;
            _I_rearsoa = i_sm.I_rearsoa;
            _I_rear = pt_between_ab.I_rear;
            _I_phase = pt_between_ab.I_phase;
            _constantCurrent = pt_between_ab.constantCurrent;
            _nonConstantCurrent = pt_between_ab.nonConstantCurrent;
            _F_Forward_PowerRatio = pt_between_ab.Forward_PowerRatio;
            _F_Forward_LockerRatio = pt_between_ab.Forward_LockerRatio;
            _F_Reverse_PowerRatio = pt_between_ab.Reverse_PowerRatio;
            _F_Reverse_LockerRatio = pt_between_ab.Reverse_LockerRatio;
        }
    }
}

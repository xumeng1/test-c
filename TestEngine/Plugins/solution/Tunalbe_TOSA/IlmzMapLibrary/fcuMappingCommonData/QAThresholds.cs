using System;
using System.Collections.Generic;
using System.Text;
// coded by purney.xie 09.5.25
namespace Bookham.fcumapping.CommonData
{
    public class QAThresholds
    {
        #region const datas
        private const int DEFAULT_SLOPE_WINDOW =5;
        private const int DEFAULT_MODAL_DISTORTION_MIN_X=20;
        private const int DEFAULT_MODAL_DISTORTION_MAX_X=80;
        private const int DEFAULT_MODAL_DISTORTION_MIN_Y=20;
        private const int DEFAULT_MODE_WIDTH_ANALYSIS_MIN_X =20;
        private const int DEFAULT_MODE_WIDTH_ANALYSIS_MAX_X= 80;
        private const int DEFAULT_HYSTERESIS_ANALYSIS_MIN_X =20;
        private const int DEFAULT_HYSTERESIS_ANALYSIS_MAX_X = 80;
        #endregion

        #region public datas
        ////////////////////////////////////
        public int _SMMapQA;		//set to 1 if SuperModeQA
        public int _slopeWindow;
        public double _modalDistortionMinX;
        public double _modalDistortionMaxX;
        public double _modalDistortionMinY;
        public double _modeWidthAnalysisMinX;
        public double _modeWidthAnalysisMaxX;
        public double _hysteresisAnalysisMinX;
        public double _hysteresisAnalysisMaxX;
        #endregion

        //default constructor
        public QAThresholds()
        {
            setThresholdsToDefault();
        }

        //set the default thresholds
        public void setThresholdsToDefault()
        {
            _slopeWindow = DEFAULT_SLOPE_WINDOW;
            _modalDistortionMinX = DEFAULT_MODAL_DISTORTION_MIN_X;
            _modalDistortionMaxX = DEFAULT_MODAL_DISTORTION_MAX_X;
            _modalDistortionMinY = DEFAULT_MODAL_DISTORTION_MIN_Y;
            _modeWidthAnalysisMinX = DEFAULT_MODE_WIDTH_ANALYSIS_MIN_X;
            _modeWidthAnalysisMaxX = DEFAULT_MODE_WIDTH_ANALYSIS_MAX_X;
            _hysteresisAnalysisMinX = DEFAULT_HYSTERESIS_ANALYSIS_MIN_X;
            _hysteresisAnalysisMaxX = DEFAULT_HYSTERESIS_ANALYSIS_MAX_X;
        }
    }
}
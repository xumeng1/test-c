using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.Specialized;
using System.IO;

// coded by purney.xie
namespace Bookham.fcumapping.CommonData
{
    public class SuperModeMapAnalysis
    {
        #region const variable
        public const int MIN_NUMBER_OF_MODES = 5;
        public const int RESULTS_NO_ERROR = 0;
        public const int RESULTS_GENERAL_ERROR = 1;
        public const int RESULTS_NO_MODEMAP_DATA = 2;
        public const int RESULTS_PO_TOO_LOW = 4;
        public const int RESULTS_PO_TOO_HIGH = 8;
        public const int RESULTS_TOO_MANY_JUMPS = 16;
        public const int RESULTS_TOO_FEW_JUMPS = 32;
        public const int RESULTS_INVALID_NUMBER_OF_LINES = 64;
        public const int RESULTS_INVALID_LINES_REMOVED = 128;
        public const int RESULTS_FORWARD_LINES_MISSING = 256;
        public const int RESULTS_REVERSE_LINES_MISSING = 512;
        public const string LASER_TYPE_ID_DSDBR01_CSTRING = "DSDBR01";
        public const string QA_METRICS_FILE_NAME = "qaMetrics";
        public const string PASSFAIL_FILE_NAME = "passFailMetrics";
        public const string COLLATED_PASSFAIL_FILE_NAME = "collatedPassFailResults";
        public const string USCORE = "_";
        public const string CSV = ".csv";

        public string qaMetricsFilePath; //added by echo, used for supermode limit
        public string passFailMetricsFilePath;

        private NameValueCollection superMapQASitting;
        private NameValueCollection superMap0QASitting;
        private NameValueCollection superMap1QASitting;
        private NameValueCollection superMap2QASitting;
        private NameValueCollection superMap3QASitting;
        private NameValueCollection superMap4QASitting;
        private NameValueCollection superMap5QASitting;
        private NameValueCollection superMap6QASitting;
        private NameValueCollection superMap7QASitting;
        private NameValueCollection superMap8QASitting;
        private NameValueCollection superMap9QASitting;
        #endregion
        #region	private attributes
        public PassFailThresholds _lmPassFailThresholds = new PassFailThresholds(); // Pass fail thresholds for the longitudinal mode

        //Data
        public PassFailAnalysis _maxUpperModeBordersRemovedPFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _maxLowerModeBordersRemovedPFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _maxMiddleLineRemovedPFAnalysis = new PassFailAnalysis();

        public PassFailAnalysis _maxForwardModeWidthPFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _minForwardModeWidthPFAnalysis = new PassFailAnalysis();

        public PassFailAnalysis _maxReverseModeWidthPFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _minReverseModeWidthPFAnalysis = new PassFailAnalysis();

        public PassFailAnalysis _meanPercWorkingRegionPFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _minPercWorkingRegionPFAnalysis = new PassFailAnalysis();

        public PassFailAnalysis _maxForwardLowerModeBDCAreaPFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _maxForwardLowerModeBDCAreaXLengthPFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _sumForwardLowerModeBDCAreasPFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _numForwardLowerModeBDCAreasPFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _maxForwardUpperModeBDCAreaPFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _maxForwardUpperModeBDCAreaXLengthPFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _sumForwardUpperModeBDCAreasPFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _numForwardUpperModeBDCAreasPFAnalysis = new PassFailAnalysis();

        public PassFailAnalysis _maxReverseLowerModeBDCAreaPFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _maxReverseLowerModeBDCAreaXLengthPFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _sumReverseLowerModeBDCAreasPFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _numReverseLowerModeBDCAreasPFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _maxReverseUpperModeBDCAreaPFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _maxReverseUpperModeBDCAreaXLengthPFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _sumReverseUpperModeBDCAreasPFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _numReverseUpperModeBDCAreasPFAnalysis = new PassFailAnalysis();

        public PassFailAnalysis _forwardLowerModalDistortionAnglePFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _forwardUpperModalDistortionAnglePFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _reverseLowerModalDistortionAnglePFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _reverseUpperModalDistortionAnglePFAnalysis = new PassFailAnalysis();

        public PassFailAnalysis _forwardLowerModalDistortionAngleXPositionPFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _forwardUpperModalDistortionAngleXPositionPFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _reverseLowerModalDistortionAngleXPositionPFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _reverseUpperModalDistortionAngleXPositionPFAnalysis = new PassFailAnalysis();


        public PassFailAnalysis _middleLineRMSValuesPFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _maxMiddleLineSlopePFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _minMiddleLineSlopePFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _maxPrGapPFAnalysis = new PassFailAnalysis();

        public PassFailAnalysis _maxForwardLowerModeSlopePFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _maxForwardUpperModeSlopePFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _maxReverseLowerModeSlopePFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _maxReverseUpperModeSlopePFAnalysis = new PassFailAnalysis();
        #region	Forward stats
        // vector of mode width vectors
        public List<Vector> _forwardModeWidths = new List<Vector>();
        public List<double> _maxForwardModeWidths = new List<double>();
        public List<double> _minForwardModeWidths = new List<double>();
        public List<double> _meanForwardModeWidths = new List<double>();
        public List<double> _sumForwardModeWidths = new List<double>();
        public List<double> _varForwardModeWidths = new List<double>();
        public List<double> _stdDevForwardModeWidths = new List<double>();
        public List<double> _medianForwardModeWidths = new List<double>();
        public List<double> _numForwardModeWidthSamples = new List<double>();

        ///////////////////////////////////////

        public List<Vector> _forwardLowerModeSlopes = new List<Vector>();
        public List<double> _maxForwardLowerModeSlopes = new List<double>();
        public List<double> _minForwardLowerModeSlopes = new List<double>();
        public List<double> _meanForwardLowerModeSlopes = new List<double>();
        public List<double> _sumForwardLowerModeSlopes = new List<double>();
        public List<double> _varForwardLowerModeSlopes = new List<double>();
        public List<double> _stdDevForwardLowerModeSlopes = new List<double>();
        public List<double> _medianForwardLowerModeSlopes = new List<double>();
        public List<double> _numForwardLowerModeSlopeSamples = new List<double>();

        public List<Vector> _forwardUpperModeSlopes = new List<Vector>();
        public List<double> _maxForwardUpperModeSlopes = new List<double>();
        public List<double> _minForwardUpperModeSlopes = new List<double>();
        public List<double> _meanForwardUpperModeSlopes = new List<double>();
        public List<double> _sumForwardUpperModeSlopes = new List<double>();
        public List<double> _varForwardUpperModeSlopes = new List<double>();
        public List<double> _stdDevForwardUpperModeSlopes = new List<double>();
        public List<double> _medianForwardUpperModeSlopes = new List<double>();
        public List<double> _numForwardUpperModeSlopeSamples = new List<double>();

        ///////////////////////////////////////

        public List<double> _forwardLowerModalDistortionAngles = new List<double>();
        public List<double> _forwardUpperModalDistortionAngles = new List<double>();

        public List<double> _forwardLowerModalDistortionAngleXPositions = new List<double>();
        public List<double> _forwardUpperModalDistortionAngleXPositions = new List<double>();

        ///////////////////////////////////////

        public List<Vector> _forwardLowerModeBDCAreas = new List<Vector>();
        public List<double> _maxForwardLowerModeBDCAreas = new List<double>();
        public List<double> _minForwardLowerModeBDCAreas = new List<double>();
        public List<double> _meanForwardLowerModeBDCAreas = new List<double>();
        public List<double> _sumForwardLowerModeBDCAreas = new List<double>();
        public List<double> _varForwardLowerModeBDCAreas = new List<double>();
        public List<double> _stdDevForwardLowerModeBDCAreas = new List<double>();
        public List<double> _medianForwardLowerModeBDCAreas = new List<double>();
        public List<double> _numForwardLowerModeBDCAreaSamples = new List<double>();
        //
        public List<Vector> _forwardUpperModeBDCAreas = new List<Vector>();
        public List<double> _maxForwardUpperModeBDCAreas = new List<double>();
        public List<double> _minForwardUpperModeBDCAreas = new List<double>();
        public List<double> _meanForwardUpperModeBDCAreas = new List<double>();
        public List<double> _sumForwardUpperModeBDCAreas = new List<double>();
        public List<double> _varForwardUpperModeBDCAreas = new List<double>();
        public List<double> _stdDevForwardUpperModeBDCAreas = new List<double>();
        public List<double> _medianForwardUpperModeBDCAreas = new List<double>();
        public List<double> _numForwardUpperModeBDCAreaSamples = new List<double>();

        ///////////////////////////////////////

        public List<Vector> _forwardLowerModeBDCAreaXLengths = new List<Vector>();
        public List<double> _maxForwardLowerModeBDCAreaXLengths = new List<double>();
        public List<double> _minForwardLowerModeBDCAreaXLengths = new List<double>();
        public List<double> _meanForwardLowerModeBDCAreaXLengths = new List<double>();
        public List<double> _sumForwardLowerModeBDCAreaXLengths = new List<double>();
        public List<double> _varForwardLowerModeBDCAreaXLengths = new List<double>();
        public List<double> _stdDevForwardLowerModeBDCAreaXLengths = new List<double>();
        public List<double> _medianForwardLowerModeBDCAreaXLengths = new List<double>();
        public List<double> _numForwardLowerModeBDCAreaXLengthSamples = new List<double>();

        public List<Vector> _forwardUpperModeBDCAreaXLengths = new List<Vector>();
        public List<double> _maxForwardUpperModeBDCAreaXLengths = new List<double>();
        public List<double> _minForwardUpperModeBDCAreaXLengths = new List<double>();
        public List<double> _meanForwardUpperModeBDCAreaXLengths = new List<double>();
        public List<double> _sumForwardUpperModeBDCAreaXLengths = new List<double>();
        public List<double> _varForwardUpperModeBDCAreaXLengths = new List<double>();
        public List<double> _stdDevForwardUpperModeBDCAreaXLengths = new List<double>();
        public List<double> _medianForwardUpperModeBDCAreaXLengths = new List<double>();
        public List<double> _numForwardUpperModeBDCAreaXLengthSamples = new List<double>();

        #endregion
        #region	Reverse stats
        // vector of mode width vectors  
        List<Vector> _reverseModeWidths = new List<Vector>();
        List<double> _maxReverseModeWidths = new List<double>();
        List<double> _minReverseModeWidths = new List<double>();
        List<double> _meanReverseModeWidths = new List<double>();
        List<double> _sumReverseModeWidths = new List<double>();
        List<double> _varReverseModeWidths = new List<double>();
        List<double> _stdDevReverseModeWidths = new List<double>();
        List<double> _medianReverseModeWidths = new List<double>();
        List<double> _numReverseModeWidthSamples = new List<double>();

        ///////////////////////////////////////

        List<Vector> _reverseLowerModeSlopes = new List<Vector>();
        List<double> _maxReverseLowerModeSlopes = new List<double>();
        List<double> _minReverseLowerModeSlopes = new List<double>();
        List<double> _meanReverseLowerModeSlopes = new List<double>();
        List<double> _sumReverseLowerModeSlopes = new List<double>();
        List<double> _varReverseLowerModeSlopes = new List<double>();
        List<double> _stdDevReverseLowerModeSlopes = new List<double>();
        List<double> _medianReverseLowerModeSlopes = new List<double>();
        List<double> _numReverseLowerModeSlopeSamples = new List<double>();

        List<Vector> _reverseUpperModeSlopes = new List<Vector>();
        List<double> _maxReverseUpperModeSlopes = new List<double>();
        List<double> _minReverseUpperModeSlopes = new List<double>();
        List<double> _meanReverseUpperModeSlopes = new List<double>();
        List<double> _sumReverseUpperModeSlopes = new List<double>();
        List<double> _varReverseUpperModeSlopes = new List<double>();
        List<double> _stdDevReverseUpperModeSlopes = new List<double>();
        List<double> _medianReverseUpperModeSlopes = new List<double>();
        List<double> _numReverseUpperModeSlopeSamples = new List<double>();

        ///////////////////////////////////////

        List<double> _reverseLowerModalDistortionAngles = new List<double>();
        List<double> _reverseLowerModalDistortionAngleXPositions = new List<double>();

        List<double> _reverseUpperModalDistortionAngles = new List<double>();
        List<double> _reverseUpperModalDistortionAngleXPositions = new List<double>();

        ///////////////////////////////////////

        List<Vector> _reverseLowerModeBDCAreas = new List<Vector>();
        List<double> _maxReverseLowerModeBDCAreas = new List<double>();
        List<double> _minReverseLowerModeBDCAreas = new List<double>();
        List<double> _meanReverseLowerModeBDCAreas = new List<double>();
        List<double> _sumReverseLowerModeBDCAreas = new List<double>();
        List<double> _varReverseLowerModeBDCAreas = new List<double>();
        List<double> _stdDevReverseLowerModeBDCAreas = new List<double>();
        List<double> _medianReverseLowerModeBDCAreas = new List<double>();
        List<double> _numReverseLowerModeBDCAreaSamples = new List<double>();

        List<Vector> _reverseUpperModeBDCAreas = new List<Vector>();
        List<double> _maxReverseUpperModeBDCAreas = new List<double>();
        List<double> _minReverseUpperModeBDCAreas = new List<double>();
        List<double> _meanReverseUpperModeBDCAreas = new List<double>();
        List<double> _sumReverseUpperModeBDCAreas = new List<double>();
        List<double> _varReverseUpperModeBDCAreas = new List<double>();
        List<double> _stdDevReverseUpperModeBDCAreas = new List<double>();
        List<double> _medianReverseUpperModeBDCAreas = new List<double>();
        List<double> _numReverseUpperModeBDCAreaSamples = new List<double>();

        ///////////////////////////////////////

        List<Vector> _reverseLowerModeBDCAreaXLengths = new List<Vector>();
        List<double> _maxReverseLowerModeBDCAreaXLengths = new List<double>();
        List<double> _minReverseLowerModeBDCAreaXLengths = new List<double>();
        List<double> _meanReverseLowerModeBDCAreaXLengths = new List<double>();
        List<double> _sumReverseLowerModeBDCAreaXLengths = new List<double>();
        List<double> _varReverseLowerModeBDCAreaXLengths = new List<double>();
        List<double> _stdDevReverseLowerModeBDCAreaXLengths = new List<double>();
        List<double> _medianReverseLowerModeBDCAreaXLengths = new List<double>();
        List<double> _numReverseLowerModeBDCAreaXLengthSamples = new List<double>();

        List<Vector> _reverseUpperModeBDCAreaXLengths = new List<Vector>();
        List<double> _maxReverseUpperModeBDCAreaXLengths = new List<double>();
        List<double> _minReverseUpperModeBDCAreaXLengths = new List<double>();
        List<double> _meanReverseUpperModeBDCAreaXLengths = new List<double>();
        List<double> _sumReverseUpperModeBDCAreaXLengths = new List<double>();
        List<double> _varReverseUpperModeBDCAreaXLengths = new List<double>();
        List<double> _stdDevReverseUpperModeBDCAreaXLengths = new List<double>();
        List<double> _medianReverseUpperModeBDCAreaXLengths = new List<double>();
        List<double> _numReverseUpperModeBDCAreaXLengthSamples = new List<double>();
        #endregion
        #region hysteresis
        /////////////////////////////////////////////////////////////////
        List<Vector> _stableAreaWidths = new List<Vector>();
        List<double> _maxStableAreaWidths = new List<double>();
        List<double> _minStableAreaWidths = new List<double>();
        List<double> _meanStableAreaWidths = new List<double>();
        List<double> _sumStableAreaWidths = new List<double>();
        List<double> _varStableAreaWidths = new List<double>();
        List<double> _stdDevStableAreaWidths = new List<double>();
        List<double> _medianStableAreaWidths = new List<double>();
        List<double> _numStableAreaWidthSamples = new List<double>();

        List<Vector> _percWorkingRegions = new List<Vector>();
        List<double> _maxPercWorkingRegions = new List<double>();
        List<double> _minPercWorkingRegions = new List<double>();
        List<double> _meanPercWorkingRegions = new List<double>();
        List<double> _sumPercWorkingRegions = new List<double>();
        List<double> _varPercWorkingRegions = new List<double>();
        List<double> _stdDevPercWorkingRegions = new List<double>();
        List<double> _medianPercWorkingRegions = new List<double>();
        List<double> _numPercWorkingRegionSamples = new List<double>();

        /////////////////////////////////////////////////////////////////
        #endregion

        #region private memebers

        List<double> _middleLineRMSValues = new List<double>();
        List<double> _middleLineSlopes = new List<double>();

        Vector _continuityValues = new Vector();
        double _maxContinuityValue;
        double _minContinuityValue;
        double _meanContinuityValues;
        double _sumContinuityValues;
        double _varContinuityValues;
        double _stdDevContinuityValues;
        double _medianContinuityValues;
        double _numContinuityValues;

        double _maxPrGap;

        ///////////////////////////////////////////////////////
        //	private attributes
        short _superModeNumber;

        int _numForwardModes;
        int _numReverseModes;
        List<ModeAnalysis> _forwardModes = new List<ModeAnalysis>();
        List<ModeAnalysis> _reverseModes = new List<ModeAnalysis>();

        int _numHysteresisModes;
        List<HysteresisAnalysis> _hysteresisAnalysis = new List<HysteresisAnalysis>();

        int _numMiddleLines;
        List<ModeBoundaryLine> _middleLines = new List<ModeBoundaryLine>();

        string _resultsDir;
        string _laserId;
        string _dateTimeStamp;

        int _xAxisLength;
        int _yAxisLength;

        short _numUpperLinesRemoved;
        short _numLowerLinesRemoved;
        short _numMiddleLinesRemoved;

        QAThresholds _qaThresholds = new QAThresholds();
        ///////////////////////////////////

        string _qaResultsAbsFilePath;
        string _passFailResultsAbsFilePath;

        ///////////////////////////////////

        bool _qaAnalysisComplete;
        bool _passFailAnalysisComplete;
        #endregion

        public List<double> MaxStableAreaWidths
        {
            get { return _maxStableAreaWidths; }
        }


        public SuperModeMapAnalysis()
        {
            this.superMapQASitting = (NameValueCollection)SettingConfigure.cfgReader.GetSection("CloseGridSetting/SuperMap/QA");
            // this.superMapQASitting = (NameValueCollection)SettingConfigure.cfgReader.GetSection("CloseGridSetting/Characterization");

            superMap0QASitting = (NameValueCollection)SettingConfigure.cfgReader.GetSection("CloseGridSetting/SuperMap/sm0");
            superMap1QASitting = (NameValueCollection)SettingConfigure.cfgReader.GetSection("CloseGridSetting/SuperMap/sm1");
            superMap2QASitting = (NameValueCollection)SettingConfigure.cfgReader.GetSection("CloseGridSetting/SuperMap/sm2");
            superMap3QASitting = (NameValueCollection)SettingConfigure.cfgReader.GetSection("CloseGridSetting/SuperMap/sm3");
            superMap4QASitting = (NameValueCollection)SettingConfigure.cfgReader.GetSection("CloseGridSetting/SuperMap/sm4");
            superMap5QASitting = (NameValueCollection)SettingConfigure.cfgReader.GetSection("CloseGridSetting/SuperMap/sm5");
            superMap6QASitting = (NameValueCollection)SettingConfigure.cfgReader.GetSection("CloseGridSetting/SuperMap/sm6");
            superMap7QASitting = (NameValueCollection)SettingConfigure.cfgReader.GetSection("CloseGridSetting/SuperMap/sm7");
            superMap8QASitting = (NameValueCollection)SettingConfigure.cfgReader.GetSection("CloseGridSetting/SuperMap/sm8");
            superMap9QASitting = (NameValueCollection)SettingConfigure.cfgReader.GetSection("CloseGridSetting/SuperMap/sm9");
        }
        ///////////////////////////////////////////////////////
        ///
        //	 private functions
        //
        public void getQAThresholds()
        {
            _qaThresholds._slopeWindow = Convert.ToInt32(this.superMapQASitting["SlopeWindowSize"]);
            _qaThresholds._modalDistortionMinX = Convert.ToDouble(this.superMapQASitting["ModalDistortionMinX"]);
            _qaThresholds._modalDistortionMaxX = Convert.ToDouble(this.superMapQASitting["ModalDistortionMaxX"]);
            _qaThresholds._modalDistortionMinY = Convert.ToDouble(this.superMapQASitting["ModalDistortionMinY"]);
            //GDM 31/10/06 new members to hold hysteresis and mode width analysis windows
            _qaThresholds._hysteresisAnalysisMinX = Convert.ToDouble(this.superMapQASitting["HysteresisAnalysisMinX"]);
            _qaThresholds._hysteresisAnalysisMaxX = Convert.ToDouble(this.superMapQASitting["HysteresisAnalysisMaxX"]);
            _qaThresholds._modeWidthAnalysisMinX = Convert.ToDouble(this.superMapQASitting["ModeWidthAnalysisMinX"]);
            _qaThresholds._modeWidthAnalysisMaxX = Convert.ToDouble(this.superMapQASitting["ModeWidthAnalysisMaxX"]);
            //GDM 31/10/06 flag that this is the OM qa to the analysis sub
            _qaThresholds._SMMapQA = 1;
            //GDM
        }

        public void init()
        {
            for (int i = 0; i < (int)_forwardModes.Count; i++)
                _forwardModes[i].init();
            _forwardModes.Clear();

            for (int i = 0; i < (int)_reverseModes.Count; i++)
                _reverseModes[i].init();
            _reverseModes.Clear();

            for (int i = 0; i < (int)_hysteresisAnalysis.Count; i++)
                _hysteresisAnalysis[i].clear();
            _hysteresisAnalysis.Clear();


            ////////////////////////////////////////////////
            ///
            //	QA Data
            //
            _forwardModeWidths.Clear();
            _maxForwardModeWidths.Clear();
            _minForwardModeWidths.Clear();
            _meanForwardModeWidths.Clear();
            _sumForwardModeWidths.Clear();
            _varForwardModeWidths.Clear();
            _stdDevForwardModeWidths.Clear();
            _medianForwardModeWidths.Clear();
            _numForwardModeWidthSamples.Clear();

            ///////////////////////////////////////

            _forwardLowerModeSlopes.Clear();
            _maxForwardLowerModeSlopes.Clear();
            _minForwardLowerModeSlopes.Clear();
            _meanForwardLowerModeSlopes.Clear();
            _sumForwardLowerModeSlopes.Clear();
            _varForwardLowerModeSlopes.Clear();
            _stdDevForwardLowerModeSlopes.Clear();
            _medianForwardLowerModeSlopes.Clear();
            _numForwardLowerModeSlopeSamples.Clear();

            _forwardUpperModeSlopes.Clear();
            _maxForwardUpperModeSlopes.Clear();
            _minForwardUpperModeSlopes.Clear();
            _meanForwardUpperModeSlopes.Clear();
            _sumForwardUpperModeSlopes.Clear();
            _varForwardUpperModeSlopes.Clear();
            _stdDevForwardUpperModeSlopes.Clear();
            _medianForwardUpperModeSlopes.Clear();
            _numForwardUpperModeSlopeSamples.Clear();

            ///////////////////////////////////////

            _forwardLowerModalDistortionAngles.Clear();
            _forwardUpperModalDistortionAngles.Clear();

            ///////////////////////////////////////

            _forwardLowerModeBDCAreas.Clear();
            _maxForwardLowerModeBDCAreas.Clear();
            _minForwardLowerModeBDCAreas.Clear();
            _meanForwardLowerModeBDCAreas.Clear();
            _sumForwardLowerModeBDCAreas.Clear();
            _varForwardLowerModeBDCAreas.Clear();
            _stdDevForwardLowerModeBDCAreas.Clear();
            _medianForwardLowerModeBDCAreas.Clear();
            _numForwardLowerModeBDCAreaSamples.Clear();
            //
            _forwardUpperModeBDCAreas.Clear();
            _maxForwardUpperModeBDCAreas.Clear();
            _minForwardUpperModeBDCAreas.Clear();
            _meanForwardUpperModeBDCAreas.Clear();
            _sumForwardUpperModeBDCAreas.Clear();
            _varForwardUpperModeBDCAreas.Clear();
            _stdDevForwardUpperModeBDCAreas.Clear();
            _medianForwardUpperModeBDCAreas.Clear();
            _numForwardUpperModeBDCAreaSamples.Clear();

            ///////////////////////////////////////

            _forwardLowerModeBDCAreaXLengths.Clear();
            _maxForwardLowerModeBDCAreaXLengths.Clear();
            _minForwardLowerModeBDCAreaXLengths.Clear();
            _meanForwardLowerModeBDCAreaXLengths.Clear();
            _sumForwardLowerModeBDCAreaXLengths.Clear();
            _varForwardLowerModeBDCAreaXLengths.Clear();
            _stdDevForwardLowerModeBDCAreaXLengths.Clear();
            _medianForwardLowerModeBDCAreaXLengths.Clear();
            _numForwardLowerModeBDCAreaXLengthSamples.Clear();

            _forwardUpperModeBDCAreaXLengths.Clear();
            _maxForwardUpperModeBDCAreaXLengths.Clear();
            _minForwardUpperModeBDCAreaXLengths.Clear();
            _meanForwardUpperModeBDCAreaXLengths.Clear();
            _sumForwardUpperModeBDCAreaXLengths.Clear();
            _varForwardUpperModeBDCAreaXLengths.Clear();
            _stdDevForwardUpperModeBDCAreaXLengths.Clear();
            _medianForwardUpperModeBDCAreaXLengths.Clear();
            _numForwardUpperModeBDCAreaXLengthSamples.Clear();

            //
            ///////////////////////////////////////
            //

            _reverseModeWidths.Clear();
            _maxReverseModeWidths.Clear();
            _minReverseModeWidths.Clear();
            _meanReverseModeWidths.Clear();
            _sumReverseModeWidths.Clear();
            _varReverseModeWidths.Clear();
            _stdDevReverseModeWidths.Clear();
            _medianReverseModeWidths.Clear();
            _numReverseModeWidthSamples.Clear();

            ///////////////////////////////////////

            _reverseLowerModeSlopes.Clear();
            _maxReverseLowerModeSlopes.Clear();
            _minReverseLowerModeSlopes.Clear();
            _meanReverseLowerModeSlopes.Clear();
            _sumReverseLowerModeSlopes.Clear();
            _varReverseLowerModeSlopes.Clear();
            _stdDevReverseLowerModeSlopes.Clear();
            _medianReverseLowerModeSlopes.Clear();
            _numReverseLowerModeSlopeSamples.Clear();

            _reverseUpperModeSlopes.Clear();
            _maxReverseUpperModeSlopes.Clear();
            _minReverseUpperModeSlopes.Clear();
            _meanReverseUpperModeSlopes.Clear();
            _sumReverseUpperModeSlopes.Clear();
            _varReverseUpperModeSlopes.Clear();
            _stdDevReverseUpperModeSlopes.Clear();
            _medianReverseUpperModeSlopes.Clear();
            _numReverseUpperModeSlopeSamples.Clear();

            /////////////////////////////////////////////

            _reverseLowerModalDistortionAngles.Clear();
            _reverseUpperModalDistortionAngles.Clear();

            /////////////////////////////////////////////

            _reverseLowerModeBDCAreas.Clear();
            _maxReverseLowerModeBDCAreas.Clear();
            _minReverseLowerModeBDCAreas.Clear();
            _meanReverseLowerModeBDCAreas.Clear();
            _sumReverseLowerModeBDCAreas.Clear();
            _varReverseLowerModeBDCAreas.Clear();
            _stdDevReverseLowerModeBDCAreas.Clear();
            _medianReverseLowerModeBDCAreas.Clear();
            _numReverseLowerModeBDCAreaSamples.Clear();

            _reverseUpperModeBDCAreas.Clear();
            _maxReverseUpperModeBDCAreas.Clear();
            _minReverseUpperModeBDCAreas.Clear();
            _meanReverseUpperModeBDCAreas.Clear();
            _sumReverseUpperModeBDCAreas.Clear();
            _varReverseUpperModeBDCAreas.Clear();
            _stdDevReverseUpperModeBDCAreas.Clear();
            _medianReverseUpperModeBDCAreas.Clear();
            _numReverseUpperModeBDCAreaSamples.Clear();

            ///////////////////////////////////////

            _reverseLowerModeBDCAreaXLengths.Clear();
            _maxReverseLowerModeBDCAreaXLengths.Clear();
            _minReverseLowerModeBDCAreaXLengths.Clear();
            _meanReverseLowerModeBDCAreaXLengths.Clear();
            _sumReverseLowerModeBDCAreaXLengths.Clear();
            _varReverseLowerModeBDCAreaXLengths.Clear();
            _stdDevReverseLowerModeBDCAreaXLengths.Clear();
            _medianReverseLowerModeBDCAreaXLengths.Clear();
            _numReverseLowerModeBDCAreaXLengthSamples.Clear();

            _reverseUpperModeBDCAreaXLengths.Clear();
            _maxReverseUpperModeBDCAreaXLengths.Clear();
            _minReverseUpperModeBDCAreaXLengths.Clear();
            _meanReverseUpperModeBDCAreaXLengths.Clear();
            _sumReverseUpperModeBDCAreaXLengths.Clear();
            _varReverseUpperModeBDCAreaXLengths.Clear();
            _stdDevReverseUpperModeBDCAreaXLengths.Clear();
            _medianReverseUpperModeBDCAreaXLengths.Clear();
            _numReverseUpperModeBDCAreaXLengthSamples.Clear();

            ///////////////////////////////////////

            _stableAreaWidths.Clear();
            _maxStableAreaWidths.Clear();
            _minStableAreaWidths.Clear();
            _meanStableAreaWidths.Clear();
            _sumStableAreaWidths.Clear();
            _varStableAreaWidths.Clear();
            _stdDevStableAreaWidths.Clear();
            _medianStableAreaWidths.Clear();
            _numStableAreaWidthSamples.Clear();

            ///////////////////////////////////////

            _percWorkingRegions.Clear();
            _maxPercWorkingRegions.Clear();
            _minPercWorkingRegions.Clear();
            _meanPercWorkingRegions.Clear();
            _sumPercWorkingRegions.Clear();
            _varPercWorkingRegions.Clear();
            _stdDevPercWorkingRegions.Clear();
            _medianPercWorkingRegions.Clear();
            _numPercWorkingRegionSamples.Clear();

            ///////////////////////////////////////

            _middleLineRMSValues.Clear();
            _middleLineSlopes.Clear();

            ///////////////////////////////////////

            _continuityValues.Clear();
            _maxContinuityValue = 0;
            _minContinuityValue = 0;
            _meanContinuityValues = 0;
            _sumContinuityValues = 0;
            _varContinuityValues = 0;
            _stdDevContinuityValues = 0;
            _medianContinuityValues = 0;
            _numContinuityValues = 0;
            _maxPrGap = 0;

            ///////////////////////////////////////

            _overallPercStableArea = 0;

            ///////////////////////////////////////

            _qaAnalysisComplete = false;


            //Pass Fail Analysis
            _passFailAnalysisComplete = false;

            _maxUpperModeBordersRemovedPFAnalysis.init();
            _maxLowerModeBordersRemovedPFAnalysis.init();
            _maxMiddleLineRemovedPFAnalysis.init();

            _maxForwardModeWidthPFAnalysis.init();
            _minForwardModeWidthPFAnalysis.init();

            _maxReverseModeWidthPFAnalysis.init();
            _minReverseModeWidthPFAnalysis.init();

            _meanPercWorkingRegionPFAnalysis.init();
            _minPercWorkingRegionPFAnalysis.init();

            _maxForwardLowerModeBDCAreaPFAnalysis.init();
            _maxForwardLowerModeBDCAreaXLengthPFAnalysis.init();
            _sumForwardLowerModeBDCAreasPFAnalysis.init();
            _numForwardLowerModeBDCAreasPFAnalysis.init();
            _maxForwardUpperModeBDCAreaPFAnalysis.init();
            _maxForwardUpperModeBDCAreaXLengthPFAnalysis.init();
            _sumForwardUpperModeBDCAreasPFAnalysis.init();
            _numForwardUpperModeBDCAreasPFAnalysis.init();

            _maxReverseLowerModeBDCAreaPFAnalysis.init();
            _maxReverseLowerModeBDCAreaXLengthPFAnalysis.init();
            _sumReverseLowerModeBDCAreasPFAnalysis.init();
            _numReverseLowerModeBDCAreasPFAnalysis.init();
            _maxReverseUpperModeBDCAreaPFAnalysis.init();
            _maxReverseUpperModeBDCAreaXLengthPFAnalysis.init();
            _sumReverseUpperModeBDCAreasPFAnalysis.init();
            _numReverseUpperModeBDCAreasPFAnalysis.init();

            _forwardLowerModalDistortionAnglePFAnalysis.init();
            _forwardLowerModalDistortionAngleXPositionPFAnalysis.init();
            _forwardUpperModalDistortionAnglePFAnalysis.init();
            _forwardUpperModalDistortionAngleXPositionPFAnalysis.init();

            _reverseLowerModalDistortionAnglePFAnalysis.init();
            _reverseLowerModalDistortionAngleXPositionPFAnalysis.init();
            _reverseUpperModalDistortionAnglePFAnalysis.init();
            _reverseUpperModalDistortionAngleXPositionPFAnalysis.init();

            _middleLineRMSValuesPFAnalysis.init();
            _maxMiddleLineSlopePFAnalysis.init();
            _minMiddleLineSlopePFAnalysis.init();
            _maxPrGapPFAnalysis.init();

            _maxForwardLowerModeSlopePFAnalysis.init();
            _maxForwardUpperModeSlopePFAnalysis.init();
            _maxReverseLowerModeSlopePFAnalysis.init();
            _maxReverseUpperModeSlopePFAnalysis.init();

            _screeningPassed = false;

            _qaDetectedError = RESULTS_NO_ERROR;

        }

        #region

        public void runAnalysis(int superModeNumber,
                    List<ModeBoundaryLine> upperLines,
                    List<ModeBoundaryLine> lowerLines,
                    List<ModeBoundaryLine> middleLines,
                    short numUpperLinesRemoved,
                    short numLowerLinesRemoved,
                    short numMiddleLinesRemoved,
                    int xAxisLength,
                    int yAxisLength,
                    Vector continuityValues,
                    string resultsDir,
                    string laserId,
                    string dateTimeStamp)
        {
            _superModeNumber = (short)superModeNumber;
            //mod GDM 23/10/06 _xAxisLength needs initiating before createModeAnalysisVectors else
            //they get created with random(?) huge 4-6million plus value for _xAxisLength, which
            //in turn screws up ModeAnalysis
            _xAxisLength = xAxisLength;
            _yAxisLength = yAxisLength;

            if (upperLines.Count != lowerLines.Count
             || upperLines.Count == 0)
            {
                throw new Exception("QA error");
            }

            createModeAnalysisVectors(upperLines, lowerLines);
            createHysteresisAnalysisVector(_forwardModes, _reverseModes);


            _numForwardModes = _forwardModes.Count;
            _numReverseModes = _reverseModes.Count;

            _middleLines = middleLines;
            _numMiddleLines = _middleLines.Count;

            _qaResultsAbsFilePath = createQaResultsAbsFilePath(resultsDir,
                                                            laserId,
                                                            dateTimeStamp);

            _passFailResultsAbsFilePath = createPassFailResultsAbsFilePath(resultsDir,
                                                                    laserId,
                                                                    dateTimeStamp);

            _resultsDir = resultsDir;
            _laserId = laserId;
            _dateTimeStamp = dateTimeStamp;

            _xAxisLength = xAxisLength;
            _yAxisLength = yAxisLength;

            _continuityValues = continuityValues;

            _numUpperLinesRemoved = numUpperLinesRemoved;
            _numLowerLinesRemoved = numLowerLinesRemoved;
            _numMiddleLinesRemoved = numMiddleLinesRemoved;

            runQaAnalysis();
            runPassFailAnalysis();

            writeQaResultsToFile();
            writePassFailResultsToFile(_passFailResultsAbsFilePath, this._superModeNumber);

        }

        public void runQaAnalysis()
        {



            runForwardModesQaAnalysis();

            runReverseModesQaAnalysis();

            runHysteresisQaAnalysis();

            runMiddleLinesQaAnalysis();

            ////////////////////////////////////////////////////////////////////////////////////

            string error_message = "";
            int error_number = RESULTS_NO_ERROR;
            if ((_numReverseModes < MIN_NUMBER_OF_MODES) || (_numForwardModes < MIN_NUMBER_OF_MODES))
                _qaDetectedError |= RESULTS_GENERAL_ERROR | RESULTS_INVALID_NUMBER_OF_LINES;
            if (!(_numUpperLinesRemoved == 0))
                _qaDetectedError |= RESULTS_GENERAL_ERROR | RESULTS_FORWARD_LINES_MISSING;
            if (!(_numLowerLinesRemoved == 0))
                _qaDetectedError |= RESULTS_GENERAL_ERROR | RESULTS_REVERSE_LINES_MISSING;


            getQAError(_qaDetectedError, ref error_number, ref error_message);

            if (_qaDetectedError > RESULTS_NO_ERROR)
                _screeningPassed = false;

            _qaAnalysisComplete = true;

        }

        public void runForwardModesQaAnalysis()
        {


            for (int i = 0; i < _numForwardModes; i++)
                _forwardModes[i].runAnalysis();

            getForwardModeWidths();
            getMaxForwardModeWidths();
            getMinForwardModeWidths();
            getMeanForwardModeWidths();
            getSumForwardModeWidths();
            getVarForwardModeWidths();
            getStdDevForwardModeWidths();
            getMedianForwardModeWidths();
            getNumForwardModeWidthSamples();

            getForwardModeSlopes();
            getMaxForwardModeSlopes();
            getMinForwardModeSlopes();
            getMeanForwardModeSlopes();
            getSumForwardModeSlopes();
            getVarForwardModeSlopes();
            getStdDevForwardModeSlopes();
            getMedianForwardModeSlopes();
            getNumForwardModeSlopeSamples();

            getForwardModalDistortionAngles();
            getForwardModalDistortionAngleXPositions();

            getForwardModeBDCAreas();
            getMaxForwardModeBDCAreas();
            getMinForwardModeBDCAreas();
            getMeanForwardModeBDCAreas();
            getSumForwardModeBDCAreas();
            getVarForwardModeBDCAreas();
            getStdDevForwardModeBDCAreas();
            getMedianForwardModeBDCAreas();
            getNumForwardModeBDCAreaSamples();


            getMaxForwardModeBDCAreaXLengths();

        }

        public void runReverseModesQaAnalysis()
        {
            for (int i = 0; i < _numReverseModes; i++)
                _reverseModes[i].runAnalysis();

            getReverseModeWidths();
            getMaxReverseModeWidths();
            getMinReverseModeWidths();
            getMeanReverseModeWidths();
            getSumReverseModeWidths();
            getVarReverseModeWidths();
            getStdDevReverseModeWidths();
            getMedianReverseModeWidths();
            getNumReverseModeWidthSamples();

            getReverseModeSlopes();
            getMaxReverseModeSlopes();
            getMinReverseModeSlopes();
            getMeanReverseModeSlopes();
            getSumReverseModeSlopes();
            getVarReverseModeSlopes();
            getStdDevReverseModeSlopes();
            getMedianReverseModeSlopes();
            getNumReverseModeSlopeSamples();

            getReverseModeSlopes();
            getMaxReverseModeSlopes();
            getMinReverseModeSlopes();
            getMeanReverseModeSlopes();
            getSumReverseModeSlopes();
            getVarReverseModeSlopes();
            getStdDevReverseModeSlopes();
            getMedianReverseModeSlopes();
            getNumReverseModeSlopeSamples();

            getReverseModalDistortionAngles();
            getReverseModalDistortionAngleXPositions();

            getReverseModeBDCAreas();
            getMaxReverseModeBDCAreas();
            getMinReverseModeBDCAreas();
            getMeanReverseModeBDCAreas();
            getSumReverseModeBDCAreas();
            getVarReverseModeBDCAreas();
            getStdDevReverseModeBDCAreas();
            getMedianReverseModeBDCAreas();
            getNumReverseModeBDCAreaSamples();


            getMaxReverseModeBDCAreaXLengths();

        }

        public void runHysteresisQaAnalysis()
        {
            for (int i = 0; i < _hysteresisAnalysis.Count; i++)
            {
                _hysteresisAnalysis[i].setQAThresholds(_qaThresholds);
                _hysteresisAnalysis[i].runAnalysis();
            }

            getStableAreaWidths();
            getMaxStableAreaWidths();
            getMinStableAreaWidths();
            getMeanStableAreaWidths();
            getSumStableAreaWidths();
            getVarStableAreaWidths();
            getStdDevStableAreaWidths();
            getMedianStableAreaWidths();
            getNumStableAreaWidthSamples();

            getPercWorkingRegions();
            getMaxPercWorkingRegions();
            getMinPercWorkingRegions();
            getMeanPercWorkingRegions();
            getSumPercWorkingRegions();
            getVarPercWorkingRegions();
            getStdDevPercWorkingRegions();
            getMedianPercWorkingRegions();
            getNumPercWorkingRegionSamples();
            // get overallPercStableArea
            //
            double totalSumStableAreaWidths = 0;
            for (int i = 0; i < _sumStableAreaWidths.Count; i++)
            {
                totalSumStableAreaWidths = totalSumStableAreaWidths + _sumStableAreaWidths[i];
            }

            _overallPercStableArea = (totalSumStableAreaWidths / (_xAxisLength * _yAxisLength)) * 100;

        }

        public void runMiddleLinesQaAnalysis()
        {
            getMiddleLineRMSValues();

            getMaxContinuityValue();
            getMinContinuityValue();
            getMeanContinuityValues();
            getSumContinuityValues();
            getVarContinuityValues();
            getStdDevContinuityValues();
            getMedianContinuityValues();
            getNumContinuityValues();
            getMaxContinuityValueSpacing();
        }

        public void runPassFailAnalysis()
        {
            if (_qaAnalysisComplete)
            {
                _screeningPassed = true;


                // get the registry values based on the supermode number
                _lmPassFailThresholds = getPassFailThresholds();

                // Set thresholds for all modes
                _maxUpperModeBordersRemovedPFAnalysis.threshold = _lmPassFailThresholds._maxModeBordersRemoved;
                _maxLowerModeBordersRemovedPFAnalysis.threshold = _lmPassFailThresholds._maxModeBordersRemoved;
                _maxMiddleLineRemovedPFAnalysis.threshold = _lmPassFailThresholds._maxModeBordersRemoved;

                _maxForwardModeWidthPFAnalysis.threshold = _lmPassFailThresholds._maxModeWidth;
                _maxReverseModeWidthPFAnalysis.threshold = _lmPassFailThresholds._maxModeWidth;

                _minForwardModeWidthPFAnalysis.threshold = _lmPassFailThresholds._minModeWidth;
                _minReverseModeWidthPFAnalysis.threshold = _lmPassFailThresholds._minModeWidth;

                _meanPercWorkingRegionPFAnalysis.threshold = _lmPassFailThresholds._meanPercWorkingRegion;
                _minPercWorkingRegionPFAnalysis.threshold = _lmPassFailThresholds._minPercWorkingRegion;

                _maxForwardLowerModeBDCAreaPFAnalysis.threshold = _lmPassFailThresholds._maxModeBDCArea;
                _maxForwardLowerModeBDCAreaXLengthPFAnalysis.threshold = _lmPassFailThresholds._maxModeBDCAreaXLength;
                _sumForwardLowerModeBDCAreasPFAnalysis.threshold = _lmPassFailThresholds._sumModeBDCAreas;
                _numForwardLowerModeBDCAreasPFAnalysis.threshold = _lmPassFailThresholds._numModeBDCAreas;
                _maxForwardUpperModeBDCAreaPFAnalysis.threshold = _lmPassFailThresholds._maxModeBDCArea;
                _maxForwardUpperModeBDCAreaXLengthPFAnalysis.threshold = _lmPassFailThresholds._maxModeBDCAreaXLength;
                _sumForwardUpperModeBDCAreasPFAnalysis.threshold = _lmPassFailThresholds._sumModeBDCAreas;
                _numForwardUpperModeBDCAreasPFAnalysis.threshold = _lmPassFailThresholds._numModeBDCAreas;

                _maxReverseLowerModeBDCAreaPFAnalysis.threshold = _lmPassFailThresholds._maxModeBDCArea;
                _maxReverseLowerModeBDCAreaXLengthPFAnalysis.threshold = _lmPassFailThresholds._maxModeBDCAreaXLength;
                _sumReverseLowerModeBDCAreasPFAnalysis.threshold = _lmPassFailThresholds._sumModeBDCAreas;
                _numReverseLowerModeBDCAreasPFAnalysis.threshold = _lmPassFailThresholds._numModeBDCAreas;
                _maxReverseUpperModeBDCAreaPFAnalysis.threshold = _lmPassFailThresholds._maxModeBDCArea;
                _maxReverseUpperModeBDCAreaXLengthPFAnalysis.threshold = _lmPassFailThresholds._maxModeBDCAreaXLength;
                _sumReverseUpperModeBDCAreasPFAnalysis.threshold = _lmPassFailThresholds._sumModeBDCAreas;
                _numReverseUpperModeBDCAreasPFAnalysis.threshold = _lmPassFailThresholds._numModeBDCAreas;

                _forwardLowerModalDistortionAnglePFAnalysis.threshold = _lmPassFailThresholds._modalDistortionAngle;
                _forwardLowerModalDistortionAngleXPositionPFAnalysis.threshold = _lmPassFailThresholds._modalDistortionAngleXPosition;
                _forwardUpperModalDistortionAnglePFAnalysis.threshold = _lmPassFailThresholds._modalDistortionAngle;
                _forwardUpperModalDistortionAngleXPositionPFAnalysis.threshold = _lmPassFailThresholds._modalDistortionAngleXPosition;

                _reverseLowerModalDistortionAnglePFAnalysis.threshold = _lmPassFailThresholds._modalDistortionAngle;
                _reverseLowerModalDistortionAngleXPositionPFAnalysis.threshold = _lmPassFailThresholds._modalDistortionAngleXPosition;
                _reverseUpperModalDistortionAnglePFAnalysis.threshold = _lmPassFailThresholds._modalDistortionAngle;
                _reverseUpperModalDistortionAngleXPositionPFAnalysis.threshold = _lmPassFailThresholds._modalDistortionAngleXPosition;

                _middleLineRMSValuesPFAnalysis.threshold = _lmPassFailThresholds._middleLineRMSValue;
                _maxMiddleLineSlopePFAnalysis.threshold = _lmPassFailThresholds._maxMiddleLineSlope;
                _minMiddleLineSlopePFAnalysis.threshold = _lmPassFailThresholds._minMiddleLineSlope;

                _maxPrGapPFAnalysis.threshold = _lmPassFailThresholds._maxPrGap;

                _maxForwardLowerModeSlopePFAnalysis.threshold = _lmPassFailThresholds._maxModeLineSlope;
                _maxForwardUpperModeSlopePFAnalysis.threshold = _lmPassFailThresholds._maxModeLineSlope;
                _maxReverseLowerModeSlopePFAnalysis.threshold = _lmPassFailThresholds._maxModeLineSlope;
                _maxReverseUpperModeSlopePFAnalysis.threshold = _lmPassFailThresholds._maxModeLineSlope;

                // find out which lines crossed front section pairs
                // and ignore failures at these locations
                List<bool> forward_mode_has_front_section_change = new List<bool>();
                List<bool> upper_line_of_forward_mode_has_front_section_change = new List<bool>();
                List<bool> lower_line_of_forward_mode_has_front_section_change = new List<bool>();

                for (int i = 0; i < _forwardModes.Count; i++)
                {
                    // find out which modes have lines crossing front sections
                    bool upper_has_change = _forwardModes[i]._upperBoundaryLine._crosses_front_sections;
                    upper_line_of_forward_mode_has_front_section_change.Add(upper_has_change);

                    bool lower_has_change = _forwardModes[i]._lowerBoundaryLine._crosses_front_sections;
                    lower_line_of_forward_mode_has_front_section_change.Add(lower_has_change);

                    if (upper_has_change || lower_has_change)
                        forward_mode_has_front_section_change.Add(true);
                    else
                        forward_mode_has_front_section_change.Add(false);
                }

                List<bool> reverse_mode_has_front_section_change = new List<bool>();
                List<bool> upper_line_of_reverse_mode_has_front_section_change = new List<bool>();
                List<bool> lower_line_of_reverse_mode_has_front_section_change = new List<bool>();

                for (int i = 0; i < (int)_reverseModes.Count; i++)
                {
                    // find out which modes have lines crossing front sections
                    bool upper_has_change = _reverseModes[i]._upperBoundaryLine._crosses_front_sections;
                    upper_line_of_reverse_mode_has_front_section_change.Add(upper_has_change);

                    bool lower_has_change = _reverseModes[i]._lowerBoundaryLine._crosses_front_sections;
                    lower_line_of_reverse_mode_has_front_section_change.Add(lower_has_change);

                    if (upper_has_change || lower_has_change)
                        reverse_mode_has_front_section_change.Add(true);
                    else
                        reverse_mode_has_front_section_change.Add(false);
                }
                // run pass fail
                if (_numLowerLinesRemoved > _maxLowerModeBordersRemovedPFAnalysis.threshold)
                    _maxLowerModeBordersRemovedPFAnalysis.pass = false;
                else
                    _maxLowerModeBordersRemovedPFAnalysis.pass = true;
                if (_numUpperLinesRemoved > _maxUpperModeBordersRemovedPFAnalysis.threshold)
                    _maxUpperModeBordersRemovedPFAnalysis.pass = false;
                else
                    _maxUpperModeBordersRemovedPFAnalysis.pass = true;
                if (_numMiddleLinesRemoved > _maxMiddleLineRemovedPFAnalysis.threshold)
                    _maxMiddleLineRemovedPFAnalysis.pass = false;
                else
                    _maxMiddleLineRemovedPFAnalysis.pass = true;


                _maxForwardModeWidthPFAnalysis.runPassFailWithUpperLimit(_maxForwardModeWidths);
                _maxReverseModeWidthPFAnalysis.runPassFailWithUpperLimit(_maxReverseModeWidths);
                _minForwardModeWidthPFAnalysis.runPassFailWithLowerLimit(_minForwardModeWidths);
                _minReverseModeWidthPFAnalysis.runPassFailWithLowerLimit(_minReverseModeWidths);

                if (Convert.ToBoolean(Convert.ToInt32(this.superMapQASitting["IgnoreBDCThresholdsatfrontsectionchange"])))
                {
                    _maxForwardLowerModeBDCAreaPFAnalysis.runPassFailWithUpperLimit(_maxForwardLowerModeBDCAreas, lower_line_of_forward_mode_has_front_section_change);
                    _maxForwardLowerModeBDCAreaXLengthPFAnalysis.runPassFailWithUpperLimit(_maxForwardLowerModeBDCAreaXLengths, lower_line_of_forward_mode_has_front_section_change);
                    _maxReverseLowerModeBDCAreaPFAnalysis.runPassFailWithUpperLimit(_maxReverseLowerModeBDCAreas, lower_line_of_reverse_mode_has_front_section_change);
                    _maxReverseLowerModeBDCAreaXLengthPFAnalysis.runPassFailWithUpperLimit(_maxReverseLowerModeBDCAreaXLengths, lower_line_of_reverse_mode_has_front_section_change);
                    _maxForwardUpperModeBDCAreaPFAnalysis.runPassFailWithUpperLimit(_maxForwardUpperModeBDCAreas, upper_line_of_forward_mode_has_front_section_change);
                    _maxForwardUpperModeBDCAreaXLengthPFAnalysis.runPassFailWithUpperLimit(_maxForwardUpperModeBDCAreaXLengths, upper_line_of_forward_mode_has_front_section_change);
                    _maxReverseUpperModeBDCAreaPFAnalysis.runPassFailWithUpperLimit(_maxReverseUpperModeBDCAreas, upper_line_of_reverse_mode_has_front_section_change);
                    _maxReverseUpperModeBDCAreaXLengthPFAnalysis.runPassFailWithUpperLimit(_maxReverseUpperModeBDCAreaXLengths, upper_line_of_reverse_mode_has_front_section_change);

                    _sumForwardLowerModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_sumForwardLowerModeBDCAreas, lower_line_of_forward_mode_has_front_section_change);
                    _numForwardLowerModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_numForwardLowerModeBDCAreaSamples, lower_line_of_forward_mode_has_front_section_change);
                    _sumReverseLowerModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_sumReverseLowerModeBDCAreas, lower_line_of_reverse_mode_has_front_section_change);
                    _numReverseLowerModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_numReverseLowerModeBDCAreaSamples, lower_line_of_reverse_mode_has_front_section_change);
                    _sumForwardUpperModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_sumForwardUpperModeBDCAreas, upper_line_of_forward_mode_has_front_section_change);
                    _numForwardUpperModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_numForwardUpperModeBDCAreaSamples, upper_line_of_forward_mode_has_front_section_change);
                    _sumReverseUpperModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_sumReverseUpperModeBDCAreas, upper_line_of_reverse_mode_has_front_section_change);
                    _numReverseUpperModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_numReverseUpperModeBDCAreaSamples, upper_line_of_reverse_mode_has_front_section_change);
                }
                else
                {
                    _maxForwardLowerModeBDCAreaPFAnalysis.runPassFailWithUpperLimit(_maxForwardLowerModeBDCAreas);
                    _maxForwardLowerModeBDCAreaXLengthPFAnalysis.runPassFailWithUpperLimit(_maxForwardLowerModeBDCAreaXLengths);
                    _maxReverseLowerModeBDCAreaPFAnalysis.runPassFailWithUpperLimit(_maxReverseLowerModeBDCAreas);
                    _maxReverseLowerModeBDCAreaXLengthPFAnalysis.runPassFailWithUpperLimit(_maxReverseLowerModeBDCAreaXLengths);
                    _maxForwardUpperModeBDCAreaPFAnalysis.runPassFailWithUpperLimit(_maxForwardUpperModeBDCAreas);
                    _maxForwardUpperModeBDCAreaXLengthPFAnalysis.runPassFailWithUpperLimit(_maxForwardUpperModeBDCAreaXLengths);
                    _maxReverseUpperModeBDCAreaPFAnalysis.runPassFailWithUpperLimit(_maxReverseUpperModeBDCAreas);
                    _maxReverseUpperModeBDCAreaXLengthPFAnalysis.runPassFailWithUpperLimit(_maxReverseUpperModeBDCAreaXLengths);

                    _sumForwardLowerModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_sumForwardLowerModeBDCAreas);
                    _numForwardLowerModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_numForwardLowerModeBDCAreaSamples);
                    _sumReverseLowerModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_sumReverseLowerModeBDCAreas);
                    _numReverseLowerModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_numReverseLowerModeBDCAreaSamples);
                    _sumForwardUpperModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_sumForwardUpperModeBDCAreas);
                    _numForwardUpperModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_numForwardUpperModeBDCAreaSamples);
                    _sumReverseUpperModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_sumReverseUpperModeBDCAreas);
                    _numReverseUpperModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_numReverseUpperModeBDCAreaSamples);
                }

                _forwardLowerModalDistortionAnglePFAnalysis.runPassFailWithUpperLimit(_forwardLowerModalDistortionAngles);
                _forwardLowerModalDistortionAngleXPositionPFAnalysis.runPassFailWithUpperLimit(_forwardLowerModalDistortionAngleXPositions);
                _forwardUpperModalDistortionAnglePFAnalysis.runPassFailWithUpperLimit(_forwardUpperModalDistortionAngles);
                _forwardUpperModalDistortionAngleXPositionPFAnalysis.runPassFailWithUpperLimit(_forwardUpperModalDistortionAngleXPositions);

                _reverseLowerModalDistortionAnglePFAnalysis.runPassFailWithUpperLimit(_reverseLowerModalDistortionAngles);
                _reverseLowerModalDistortionAngleXPositionPFAnalysis.runPassFailWithUpperLimit(_reverseLowerModalDistortionAngleXPositions);
                _reverseUpperModalDistortionAnglePFAnalysis.runPassFailWithUpperLimit(_reverseUpperModalDistortionAngles);
                _reverseUpperModalDistortionAngleXPositionPFAnalysis.runPassFailWithUpperLimit(_reverseUpperModalDistortionAngleXPositions);


                _meanPercWorkingRegionPFAnalysis.runPassFailWithLowerLimit(_meanPercWorkingRegions);
                _minPercWorkingRegionPFAnalysis.runPassFailWithLowerLimit(_minPercWorkingRegions);

                _middleLineRMSValuesPFAnalysis.runPassFailWithUpperLimit(_middleLineRMSValues);
                _maxMiddleLineSlopePFAnalysis.runPassFailWithUpperLimit(_middleLineSlopes);
                _minMiddleLineSlopePFAnalysis.runPassFailWithLowerLimit(_middleLineSlopes);

                _maxPrGapPFAnalysis.runPassFailWithUpperLimit(_continuityValues.getValues());

                _maxForwardLowerModeSlopePFAnalysis.runPassFailWithUpperLimit(_maxForwardLowerModeSlopes);
                _maxForwardUpperModeSlopePFAnalysis.runPassFailWithUpperLimit(_maxForwardUpperModeSlopes);
                _maxReverseLowerModeSlopePFAnalysis.runPassFailWithUpperLimit(_maxReverseLowerModeSlopes);
                _maxReverseUpperModeSlopePFAnalysis.runPassFailWithUpperLimit(_maxReverseUpperModeSlopes);

                if (!(_maxLowerModeBordersRemovedPFAnalysis.pass) ||
                    !(_maxUpperModeBordersRemovedPFAnalysis.pass) ||
                    !(_maxMiddleLineRemovedPFAnalysis.pass) ||
                    !(_maxForwardModeWidthPFAnalysis.pass) ||
                    !(_maxReverseModeWidthPFAnalysis.pass) ||
                    !(_minForwardModeWidthPFAnalysis.pass) ||
                    !(_minReverseModeWidthPFAnalysis.pass) ||
                    !(_maxForwardLowerModeBDCAreaPFAnalysis.pass) ||
                    !(_maxForwardLowerModeBDCAreaXLengthPFAnalysis.pass) ||
                    !(_maxForwardUpperModeBDCAreaPFAnalysis.pass) ||
                    !(_maxForwardUpperModeBDCAreaXLengthPFAnalysis.pass) ||
                    !(_maxReverseLowerModeBDCAreaPFAnalysis.pass) ||
                    !(_maxReverseLowerModeBDCAreaXLengthPFAnalysis.pass) ||
                    !(_maxReverseUpperModeBDCAreaPFAnalysis.pass) ||
                    !(_maxReverseUpperModeBDCAreaXLengthPFAnalysis.pass) ||
                    !(_sumForwardLowerModeBDCAreasPFAnalysis.pass) ||
                    !(_numForwardLowerModeBDCAreasPFAnalysis.pass) ||
                    !(_sumForwardUpperModeBDCAreasPFAnalysis.pass) ||
                    !(_numForwardUpperModeBDCAreasPFAnalysis.pass) ||
                    !(_sumReverseLowerModeBDCAreasPFAnalysis.pass) ||
                    !(_numReverseLowerModeBDCAreasPFAnalysis.pass) ||
                    !(_sumReverseUpperModeBDCAreasPFAnalysis.pass) ||
                    !(_numReverseUpperModeBDCAreasPFAnalysis.pass) ||
                    !(_forwardLowerModalDistortionAnglePFAnalysis.pass) ||
                    !(_forwardLowerModalDistortionAngleXPositionPFAnalysis.pass) ||
                    !(_forwardUpperModalDistortionAnglePFAnalysis.pass) ||
                    !(_forwardUpperModalDistortionAngleXPositionPFAnalysis.pass) ||
                    !(_reverseLowerModalDistortionAnglePFAnalysis.pass) ||
                    !(_reverseLowerModalDistortionAngleXPositionPFAnalysis.pass) ||
                    !(_reverseUpperModalDistortionAnglePFAnalysis.pass) ||
                    !(_reverseUpperModalDistortionAngleXPositionPFAnalysis.pass) ||
                    !(_middleLineRMSValuesPFAnalysis.pass) ||
                    !(_meanPercWorkingRegionPFAnalysis.pass) ||
                    !(_minPercWorkingRegionPFAnalysis.pass) ||
                    !(_maxMiddleLineSlopePFAnalysis.pass) ||
                    !(_minMiddleLineSlopePFAnalysis.pass) ||
                    !(_maxForwardLowerModeSlopePFAnalysis.pass) ||
                    !(_maxForwardUpperModeSlopePFAnalysis.pass) ||
                    !(_maxReverseLowerModeSlopePFAnalysis.pass) ||
                    !(_maxReverseUpperModeSlopePFAnalysis.pass) ||
                    !(_maxPrGapPFAnalysis.pass))
                {
                    _screeningPassed = false;
                }

            }
            // analyse results
            //
            if (!(_numUpperLinesRemoved == 0))
                _qaDetectedError |= RESULTS_GENERAL_ERROR | RESULTS_FORWARD_LINES_MISSING;
            if (!(_numLowerLinesRemoved == 0))
                _qaDetectedError |= RESULTS_GENERAL_ERROR | RESULTS_REVERSE_LINES_MISSING;
            if (_numReverseModes < MIN_NUMBER_OF_MODES)
                _qaDetectedError |= RESULTS_GENERAL_ERROR | RESULTS_INVALID_NUMBER_OF_LINES;
            if (_numForwardModes < MIN_NUMBER_OF_MODES)
                _qaDetectedError |= RESULTS_GENERAL_ERROR | RESULTS_INVALID_NUMBER_OF_LINES;

            if (_qaDetectedError > RESULTS_NO_ERROR)
                _screeningPassed = false;


            _passFailAnalysisComplete = true;
        }
        public void createModeAnalysisVectors(List<ModeBoundaryLine> upperLines, List<ModeBoundaryLine> lowerLines)
        {
            // create a vector of forward modes and a vector of reverse modes

            for (int i = 0; i < (int)_forwardModes.Count; i++)
            {
                _forwardModes[i].init();
            }
            _forwardModes.Clear();

            for (int i = 0; i < (int)_reverseModes.Count; i++)
            {
                _reverseModes[i].init();
            }
            _reverseModes.Clear();

            if (lowerLines.Count > 0
             && lowerLines.Count == upperLines.Count)
            {
                // forward modes - the lower lines


                getQAThresholds();

                for (int i = 1; i < (int)lowerLines.Count; i++)
                {
                    ModeAnalysis forwardMode = new ModeAnalysis();
                    ModeAnalysis reverseMode = new ModeAnalysis();
                    forwardMode.loadBoundaryLines(i, _xAxisLength, _yAxisLength, lowerLines[i], lowerLines[i - 1]);
                    forwardMode.setQAThresholds(_qaThresholds);

                    _forwardModes.Add(forwardMode);

                    reverseMode.loadBoundaryLines(i, _xAxisLength, _yAxisLength, upperLines[i], upperLines[i - 1]);
                    reverseMode.setQAThresholds(_qaThresholds);

                    _reverseModes.Add(reverseMode);
                }
            }
        }

        public void createHysteresisAnalysisVector(List<ModeAnalysis> forwardModes,
                            List<ModeAnalysis> reverseModes)
        {
            int numModes = 0;

            for (int i = 0; i < _hysteresisAnalysis.Count; i++)
            {
                _hysteresisAnalysis[i].clear();
            }
            _hysteresisAnalysis.Clear();

            //GDM 31/10/06 need to get the QAAnalysis window for hysteresis
            getQAThresholds();

            if (forwardModes.Count > 0
             && reverseModes.Count == forwardModes.Count)
            {
                numModes = forwardModes.Count;

                HysteresisAnalysis hysteresisAnalysis = new HysteresisAnalysis();
                for (int i = 0; i < numModes; i++)
                {
                    ModeAnalysis forwardMode = forwardModes[i];
                    ModeAnalysis reverseMode = reverseModes[i];

                    //GDM 31/10/06 added  req for _qaThreshold for hysteresis analysis in  fwd and rev modes
                    forwardMode.setQAThresholds(_qaThresholds);
                    reverseMode.setQAThresholds(_qaThresholds);
                    hysteresisAnalysis.setQAThresholds(_qaThresholds);

                    if (forwardMode._lowerBoundaryLine._x.Count > 0
                     && forwardMode._lowerBoundaryLine._y.Count == forwardMode._lowerBoundaryLine._x.Count
                     && reverseMode._lowerBoundaryLine._x.Count > 0
                     && reverseMode._lowerBoundaryLine._y.Count == reverseMode._lowerBoundaryLine._x.Count)
                    {
                        hysteresisAnalysis = new HysteresisAnalysis(forwardMode, reverseMode, _xAxisLength, _yAxisLength);
                        _hysteresisAnalysis.Add(hysteresisAnalysis);
                    }
                }
            }

            _numHysteresisModes = _hysteresisAnalysis.Count;
        }

        public string createPassFailResultsAbsFilePath(string resultsDir, string laserId, string dateTimeStamp)
        {
            string absFilePath = "";
            string superModeNumString = string.Format("{0}", _superModeNumber);


            absFilePath += resultsDir;
            absFilePath += "\\";
            absFilePath += PASSFAIL_FILE_NAME;
            absFilePath += USCORE;
            absFilePath += LASER_TYPE_ID_DSDBR01_CSTRING;
            absFilePath += USCORE;
            absFilePath += laserId;
            absFilePath += USCORE;
            absFilePath += dateTimeStamp;
            absFilePath += USCORE;
            absFilePath += "SM";
            absFilePath += superModeNumString;
            absFilePath += CSV;

            this.passFailMetricsFilePath = absFilePath;

            return absFilePath;
        }
        public string createQaResultsAbsFilePath(string resultsDir, string laserId, string dateTimeStamp)
        {
            string absFilePath = "";

            string superModeNumString = string.Format("{0}", _superModeNumber);


            absFilePath += resultsDir;
            absFilePath += "\\";
            absFilePath += QA_METRICS_FILE_NAME;
            absFilePath += USCORE;
            absFilePath += LASER_TYPE_ID_DSDBR01_CSTRING;
            absFilePath += USCORE;
            absFilePath += laserId;
            absFilePath += USCORE;
            absFilePath += dateTimeStamp;
            absFilePath += USCORE;
            absFilePath += "SM";
            absFilePath += superModeNumString;
            absFilePath += CSV;

            this.qaMetricsFilePath = absFilePath;

            return absFilePath;
        }


        public void writeQaResultsToFile()
        {
            using (StreamWriter sw = new StreamWriter(_qaResultsAbsFilePath, true))
            {
                string errorMessage = "";
                int errorNum = 0;
                getQAError(_qaDetectedError, ref errorNum, ref errorMessage);
                sw.WriteLine(errorMessage);
                sw.WriteLine();

                sw.WriteLine("Slope Window Size,{0},", superMapQASitting["SlopeWindowSize"]);
                sw.Write("Modal Distortion Min X,{0},", superMapQASitting["ModalDistortionMinX"]);
                sw.Write("Mode Width Analysis Min X,{0},", superMapQASitting["ModeWidthAnalysisMinX"]);
                sw.WriteLine("Hysteresis Analysis Min X,{0},", superMapQASitting["HysteresisAnalysisMinX"]);

                sw.Write("Modal Distortion Max X,{0},", superMapQASitting["ModalDistortionMaxX"]);
                sw.Write("Mode Width Analysis Max X,{0},", superMapQASitting["ModeWidthAnalysisMaxX"]);
                sw.WriteLine("Hysteresis Analysis Max X,{0}", superMapQASitting["HysteresisAnalysisMaxX"]);

                sw.WriteLine("Modal Distortion Min Y,{0}", superMapQASitting["ModalDistortionMinY"]);
                sw.WriteLine();
                sw.Flush();
                string QA_XLS_HEADER1 = "Overall % Stable Area,";
                string QA_XLS_HEADER2 = "Index,";
                string QA_XLS_HEADER3 = "Function,";
                string QA_XLS_HEADER4 = "Slope of forward lower mode boundary line,";
                string QA_XLS_HEADER5 = "Slope of forward upper mode boundary line,";
                string QA_XLS_HEADER6 = "Slope of reverse lower mode boundary line,";
                string QA_XLS_HEADER7 = "Slope of reverse upper mode boundary line,";
                string QA_XLS_HEADER8 = "Width for Stable Area,";
                string QA_XLS_HEADER9 = "Reverse Mode Width,";
                string QA_XLS_HEADER10 = "Forward Mode Width,";
                string QA_XLS_HEADER11 = "% Working Region,";
                string QA_XLS_HEADER12 = "Forward Lower Max Modal Distortion Angle,";
                string QA_XLS_HEADER13 = "Forward Lower Max Modal Distortion X-Position,";
                string QA_XLS_HEADER14 = "Forward Upper Max Modal Distortion Angle,";
                string QA_XLS_HEADER15 = "Forward Upper Max Modal Distortion X-Position,";
                string QA_XLS_HEADER16 = "Reverse Lower Max Modal Distortion Angle,";
                string QA_XLS_HEADER17 = "Reverse Lower Max Modal Distortion X-Position,";
                string QA_XLS_HEADER18 = "Reverse Upper Max Modal Distortion Angle,";
                string QA_XLS_HEADER19 = "Reverse Upper Max Modal Distortion X-Position,";
                string QA_XLS_HEADER20 = "Forward Lower Max Boundary Direction Change X-Length,";
                string QA_XLS_HEADER21 = "Forward Upper Max Boundary Direction Change X-Length,";
                string QA_XLS_HEADER22 = "Forward Lower Max Boundary Direction Change Area,";
                string QA_XLS_HEADER23 = "Forward Upper Max Boundary Direction Change Area,";
                string QA_XLS_HEADER24 = "Forward Lower Sum of Boundary Direction Change Areas,";
                string QA_XLS_HEADER25 = "Forward Upper Sum of Boundary Direction Change Areas,";
                string QA_XLS_HEADER26 = "Forward Lower Number of Boundary Direction Changes,";
                string QA_XLS_HEADER27 = "Forward Upper Number of Boundary Direction Changes,";
                string QA_XLS_HEADER28 = "Reverse Lower Max Boundary Direction Change X-Length,";
                string QA_XLS_HEADER29 = "Reverse Upper Max Boundary Direction Change X-Length,";
                string QA_XLS_HEADER30 = "Reverse Lower Max Boundary Direction Change Area,";
                string QA_XLS_HEADER31 = "Reverse Upper Max Boundary Direction Change Area,";
                string QA_XLS_HEADER32 = "Reverse Lower Sum of Boundary Direction Change Areas,";
                string QA_XLS_HEADER33 = "Reverse Upper Sum of Boundary Direction Change Areas,";
                string QA_XLS_HEADER34 = "Reverse Lower Number of Boundary Direction Changes,";
                string QA_XLS_HEADER35 = "Reverse Upper Number of Boundary Direction Changes,";
                string QA_XLS_HEADER36 = "Middle Line RMS Value,";

                string QA_XLS_MAX = "Maximum";
                string QA_XLS_MIN = "Minimum";
                string QA_XLS_MEAN = "Mean";
                string QA_XLS_SUM = "Sum";
                string QA_XLS_COUNT = "Number Of Samples";
                string QA_XLS_STDDEV = "Standard Deviation";
                string QA_XLS_VAR = "Variance";
                string QA_XLS_OTHERS = "";

                List<string> headerList = new List<string>();
                headerList.Add(QA_XLS_MAX);
                headerList.Add(QA_XLS_MIN);
                headerList.Add(QA_XLS_MEAN);
                headerList.Add(QA_XLS_SUM);
                headerList.Add(QA_XLS_COUNT);
                headerList.Add(QA_XLS_STDDEV);
                headerList.Add(QA_XLS_VAR);
                headerList.Add(QA_XLS_OTHERS);

                //sw.Write("{0},",QA_XLS_MAX);
                //sw.Write("{0},",QA_XLS_MIN);
                //sw.Write("{0},",QA_XLS_MEAN);
                //sw.Write("{0},",QA_XLS_SUM);
                //sw.Write("{0},",QA_XLS_COUNT);
                //sw.Write("{0},",QA_XLS_STDDEV);
                //sw.Write("{0},",QA_XLS_VAR);
                //sw.Write("{0},",QA_XLS_OTHERS);  

                string QA_XLS_COL1_VALUE = "";
                string QA_XLS_COL2_VALUE = "";
                string QA_XLS_COL3_VALUE = "";
                string QA_XLS_COL4_VALUE = "";
                string QA_XLS_COL5_VALUE = "";
                string QA_XLS_COL6_VALUE = "";
                string QA_XLS_COL7_VALUE = "";
                string QA_XLS_COL8_VALUE = "";
                string QA_XLS_COL9_VALUE = "";
                string QA_XLS_COL10_VALUE = "";
                string QA_XLS_COL11_VALUE = "";
                string QA_XLS_COL12_VALUE = "";
                string QA_XLS_COL13_VALUE = "";
                string QA_XLS_COL14_VALUE = "";
                string QA_XLS_COL15_VALUE = "";
                string QA_XLS_COL16_VALUE = "";
                string QA_XLS_COL17_VALUE = "";
                string QA_XLS_COL18_VALUE = "";
                string QA_XLS_COL19_VALUE = "";
                string QA_XLS_COL20_VALUE = "";
                string QA_XLS_COL21_VALUE = "";
                string QA_XLS_COL22_VALUE = "";
                string QA_XLS_COL23_VALUE = "";
                string QA_XLS_COL24_VALUE = "";
                string QA_XLS_COL25_VALUE = "";
                string QA_XLS_COL26_VALUE = "";
                string QA_XLS_COL27_VALUE = "";
                string QA_XLS_COL28_VALUE = "";
                string QA_XLS_COL29_VALUE = "";
                string QA_XLS_COL30_VALUE = "";
                string QA_XLS_COL31_VALUE = "";
                string QA_XLS_COL32_VALUE = "";
                string QA_XLS_COL33_VALUE = "";
                string QA_XLS_COL34_VALUE = "";
                string QA_XLS_COL35_VALUE = "";
                string QA_XLS_COL36_VALUE = "";

                sw.Write(QA_XLS_HEADER1);
                sw.Write(QA_XLS_HEADER2);
                sw.Write(QA_XLS_HEADER3);
                sw.Write(QA_XLS_HEADER4);
                sw.Write(QA_XLS_HEADER5);
                sw.Write(QA_XLS_HEADER6);
                sw.Write(QA_XLS_HEADER7);
                sw.Write(QA_XLS_HEADER8);
                sw.Write(QA_XLS_HEADER9);
                sw.Write(QA_XLS_HEADER10);
                sw.Write(QA_XLS_HEADER11);
                sw.Write(QA_XLS_HEADER12);
                sw.Write(QA_XLS_HEADER13);
                sw.Write(QA_XLS_HEADER14);
                sw.Write(QA_XLS_HEADER15);
                sw.Write(QA_XLS_HEADER16);
                sw.Write(QA_XLS_HEADER17);
                sw.Write(QA_XLS_HEADER18);
                sw.Write(QA_XLS_HEADER19);
                sw.Write(QA_XLS_HEADER20);
                sw.Write(QA_XLS_HEADER21);
                sw.Write(QA_XLS_HEADER22);
                sw.Write(QA_XLS_HEADER23);
                sw.Write(QA_XLS_HEADER24);
                sw.Write(QA_XLS_HEADER25);
                sw.Write(QA_XLS_HEADER26);
                sw.Write(QA_XLS_HEADER27);
                sw.Write(QA_XLS_HEADER28);
                sw.Write(QA_XLS_HEADER29);
                sw.Write(QA_XLS_HEADER30);
                sw.Write(QA_XLS_HEADER31);
                sw.Write(QA_XLS_HEADER32);
                sw.Write(QA_XLS_HEADER33);
                sw.Write(QA_XLS_HEADER34);
                sw.Write(QA_XLS_HEADER35);
                sw.Write(QA_XLS_HEADER36);
                sw.WriteLine();


                QA_XLS_COL1_VALUE = string.Format("{0}", _overallPercStableArea);
                sw.Write(QA_XLS_COL1_VALUE + ",");
                sw.Write(QA_XLS_COL2_VALUE + ",");
                sw.Write(QA_XLS_COL3_VALUE + ",");
                sw.Write(QA_XLS_COL4_VALUE + ",");
                sw.Write(QA_XLS_COL5_VALUE + ",");
                sw.Write(QA_XLS_COL6_VALUE + ",");
                sw.Write(QA_XLS_COL7_VALUE + ",");
                sw.Write(QA_XLS_COL8_VALUE + ",");
                sw.Write(QA_XLS_COL9_VALUE + ",");
                sw.Write(QA_XLS_COL10_VALUE + ",");
                sw.Write(QA_XLS_COL11_VALUE + ",");
                sw.Write(QA_XLS_COL12_VALUE + ",");
                sw.Write(QA_XLS_COL13_VALUE + ",");
                sw.Write(QA_XLS_COL14_VALUE + ",");
                sw.Write(QA_XLS_COL15_VALUE + ",");
                sw.Write(QA_XLS_COL16_VALUE + ",");
                sw.Write(QA_XLS_COL17_VALUE + ",");
                sw.Write(QA_XLS_COL18_VALUE + ",");
                sw.Write(QA_XLS_COL19_VALUE + ",");
                sw.Write(QA_XLS_COL20_VALUE + ",");
                sw.Write(QA_XLS_COL21_VALUE + ",");
                sw.Write(QA_XLS_COL22_VALUE + ",");
                sw.Write(QA_XLS_COL23_VALUE + ",");
                sw.Write(QA_XLS_COL24_VALUE + ",");
                sw.Write(QA_XLS_COL25_VALUE + ",");
                sw.Write(QA_XLS_COL26_VALUE + ",");
                sw.Write(QA_XLS_COL27_VALUE + ",");
                sw.Write(QA_XLS_COL28_VALUE + ",");
                sw.Write(QA_XLS_COL29_VALUE + ",");
                sw.Write(QA_XLS_COL30_VALUE + ",");
                sw.Write(QA_XLS_COL31_VALUE + ",");
                sw.Write(QA_XLS_COL32_VALUE + ",");
                sw.Write(QA_XLS_COL33_VALUE + ",");
                sw.Write(QA_XLS_COL34_VALUE + ",");
                sw.Write(QA_XLS_COL35_VALUE + ",");
                sw.Write(QA_XLS_COL36_VALUE + ",");
                sw.Write("\n");



                //////////////////////////////////////////////////////////////////////////

                int QA_XLS_NUM_ROWS_PER_SM = headerList.Count;

                int numModes = _numForwardModes;
                if (numModes > _numReverseModes)
                    numModes = _numReverseModes;

                for (int i = 0; i < numModes; i++)
                {
                    for (int j = 0; j < QA_XLS_NUM_ROWS_PER_SM; j++)
                    {
                        QA_XLS_COL1_VALUE = "";
                        QA_XLS_COL2_VALUE = "";
                        QA_XLS_COL3_VALUE = "";
                        QA_XLS_COL4_VALUE = "";
                        QA_XLS_COL5_VALUE = "";
                        QA_XLS_COL6_VALUE = "";
                        QA_XLS_COL7_VALUE = "";
                        QA_XLS_COL8_VALUE = "";
                        QA_XLS_COL9_VALUE = "";
                        QA_XLS_COL10_VALUE = "";
                        QA_XLS_COL11_VALUE = "";
                        QA_XLS_COL12_VALUE = "";
                        QA_XLS_COL13_VALUE = "";
                        QA_XLS_COL14_VALUE = "";
                        QA_XLS_COL15_VALUE = "";
                        QA_XLS_COL16_VALUE = "";
                        QA_XLS_COL17_VALUE = "";
                        QA_XLS_COL18_VALUE = "";
                        QA_XLS_COL19_VALUE = "";
                        QA_XLS_COL20_VALUE = "";
                        QA_XLS_COL21_VALUE = "";
                        QA_XLS_COL22_VALUE = "";
                        QA_XLS_COL23_VALUE = "";
                        QA_XLS_COL24_VALUE = "";
                        QA_XLS_COL25_VALUE = "";
                        QA_XLS_COL26_VALUE = "";
                        QA_XLS_COL27_VALUE = "";
                        QA_XLS_COL28_VALUE = "";
                        QA_XLS_COL29_VALUE = "";
                        QA_XLS_COL30_VALUE = "";
                        QA_XLS_COL31_VALUE = "";
                        QA_XLS_COL32_VALUE = "";
                        QA_XLS_COL33_VALUE = "";
                        QA_XLS_COL34_VALUE = "";
                        QA_XLS_COL35_VALUE = "";
                        QA_XLS_COL36_VALUE = "";

                        QA_XLS_COL1_VALUE = "";
                        QA_XLS_COL2_VALUE = string.Format("{0}", i);
                        QA_XLS_COL3_VALUE = headerList[j];

                        if (QA_XLS_COL3_VALUE == QA_XLS_MAX)
                        {
                            if (i < (int)_maxForwardLowerModeSlopes.Count)
                                QA_XLS_COL4_VALUE = string.Format("{0}", _maxForwardLowerModeSlopes[i]);
                            else
                                QA_XLS_COL4_VALUE = "";
                            if (i < (int)_maxForwardUpperModeSlopes.Count)
                                QA_XLS_COL5_VALUE = string.Format("{0}", _maxForwardUpperModeSlopes[i]);
                            else
                                QA_XLS_COL5_VALUE = "";
                            if (i < (int)_maxReverseLowerModeSlopes.Count)
                                QA_XLS_COL6_VALUE = string.Format("{0}", _maxReverseLowerModeSlopes[i]);
                            else
                                QA_XLS_COL6_VALUE = "";
                            if (i < (int)_maxReverseUpperModeSlopes.Count)
                                QA_XLS_COL7_VALUE = string.Format("{0}", _maxReverseUpperModeSlopes[i]);
                            else
                                QA_XLS_COL7_VALUE = "";
                            if (i < (int)_maxStableAreaWidths.Count)
                                QA_XLS_COL8_VALUE = string.Format("{0}", _maxStableAreaWidths[i]);
                            else
                                QA_XLS_COL8_VALUE = "";
                            if (i < (int)_maxReverseModeWidths.Count)
                                QA_XLS_COL9_VALUE = string.Format("{0}", _maxReverseModeWidths[i]);
                            else
                                QA_XLS_COL9_VALUE = "";
                            if (i < (int)_maxForwardModeWidths.Count)
                                QA_XLS_COL10_VALUE = string.Format("{0}", _maxForwardModeWidths[i]);
                            else
                                QA_XLS_COL10_VALUE = "";
                            if (i < (int)_maxPercWorkingRegions.Count)
                                QA_XLS_COL11_VALUE = string.Format("{0}", _maxPercWorkingRegions[i]);
                            else
                                QA_XLS_COL11_VALUE = "";
                        }
                        else if (QA_XLS_COL3_VALUE == QA_XLS_MIN)
                        {
                            if (i < (int)_minForwardLowerModeSlopes.Count)
                                QA_XLS_COL4_VALUE = string.Format("{0}", _minForwardLowerModeSlopes[i]);
                            else
                                QA_XLS_COL4_VALUE = "";
                            if (i < (int)_minForwardUpperModeSlopes.Count)
                                QA_XLS_COL5_VALUE = string.Format("{0}", _minForwardUpperModeSlopes[i]);
                            else
                                QA_XLS_COL5_VALUE = "";
                            if (i < (int)_minReverseLowerModeSlopes.Count)
                                QA_XLS_COL6_VALUE = string.Format("{0}", _minReverseLowerModeSlopes[i]);
                            else
                                QA_XLS_COL6_VALUE = "";
                            if (i < (int)_minReverseUpperModeSlopes.Count)
                                QA_XLS_COL7_VALUE = string.Format("{0}", _minReverseUpperModeSlopes[i]);
                            else
                                QA_XLS_COL7_VALUE = "";
                            if (i < (int)_minStableAreaWidths.Count)
                                QA_XLS_COL8_VALUE = string.Format("{0}", _minStableAreaWidths[i]);
                            else
                                QA_XLS_COL8_VALUE = "";
                            if (i < (int)_minReverseModeWidths.Count)
                                QA_XLS_COL9_VALUE = string.Format("{0}", _minReverseModeWidths[i]);
                            else
                                QA_XLS_COL9_VALUE = "";
                            if (i < (int)_minForwardModeWidths.Count)
                                QA_XLS_COL10_VALUE = string.Format("{0}", _minForwardModeWidths[i]);
                            else
                                QA_XLS_COL10_VALUE = "";
                            if (i < (int)_minPercWorkingRegions.Count)
                                QA_XLS_COL11_VALUE = string.Format("{0}", _minPercWorkingRegions[i]);
                            else
                                QA_XLS_COL11_VALUE = "";
                        }
                        else if (QA_XLS_COL3_VALUE == QA_XLS_MEAN)
                        {
                            if (i < (int)_meanForwardLowerModeSlopes.Count)
                                QA_XLS_COL4_VALUE = string.Format("{0}", _meanForwardLowerModeSlopes[i]);
                            else
                                QA_XLS_COL4_VALUE = "";
                            if (i < (int)_meanForwardUpperModeSlopes.Count)
                                QA_XLS_COL5_VALUE = string.Format("{0}", _meanForwardUpperModeSlopes[i]);
                            else
                                QA_XLS_COL5_VALUE = "";
                            if (i < (int)_meanReverseLowerModeSlopes.Count)
                                QA_XLS_COL6_VALUE = string.Format("{0}", _meanReverseLowerModeSlopes[i]);
                            else
                                QA_XLS_COL6_VALUE = "";
                            if (i < (int)_meanReverseUpperModeSlopes.Count)
                                QA_XLS_COL7_VALUE = string.Format("{0}", _meanReverseUpperModeSlopes[i]);
                            else
                                QA_XLS_COL7_VALUE = "";
                            if (i < (int)_meanStableAreaWidths.Count)
                                QA_XLS_COL8_VALUE = string.Format("{0}", _meanStableAreaWidths[i]);
                            else
                                QA_XLS_COL8_VALUE = "";
                            if (i < (int)_meanReverseModeWidths.Count)
                                QA_XLS_COL9_VALUE = string.Format("{0}", _meanReverseModeWidths[i]);
                            else
                                QA_XLS_COL9_VALUE = "";
                            if (i < (int)_meanForwardModeWidths.Count)
                                QA_XLS_COL10_VALUE = string.Format("{0}", _meanForwardModeWidths[i]);
                            else
                                QA_XLS_COL10_VALUE = "";
                            if (i < (int)_meanPercWorkingRegions.Count)
                                QA_XLS_COL11_VALUE = string.Format("{0}", _meanPercWorkingRegions[i]);
                            else
                                QA_XLS_COL11_VALUE = "";
                        }
                        else if (QA_XLS_COL3_VALUE == QA_XLS_SUM)
                        {
                            if (i < (int)_sumForwardLowerModeSlopes.Count)
                                QA_XLS_COL4_VALUE = string.Format("{0}", _sumForwardLowerModeSlopes[i]);
                            else
                                QA_XLS_COL4_VALUE = "";
                            if (i < (int)_sumForwardUpperModeSlopes.Count)
                                QA_XLS_COL5_VALUE = string.Format("{0}", _sumForwardUpperModeSlopes[i]);
                            else
                                QA_XLS_COL5_VALUE = "";
                            if (i < (int)_sumReverseLowerModeSlopes.Count)
                                QA_XLS_COL6_VALUE = string.Format("{0}", _sumReverseLowerModeSlopes[i]);
                            else
                                QA_XLS_COL6_VALUE = "";
                            if (i < (int)_sumReverseUpperModeSlopes.Count)
                                QA_XLS_COL7_VALUE = string.Format("{0}", _sumReverseUpperModeSlopes[i]);
                            else
                                QA_XLS_COL7_VALUE = "";
                            if (i < (int)_sumStableAreaWidths.Count)
                                QA_XLS_COL8_VALUE = string.Format("{0}", _sumStableAreaWidths[i]);
                            else
                                QA_XLS_COL8_VALUE = "";
                            if (i < (int)_sumReverseModeWidths.Count)
                                QA_XLS_COL9_VALUE = string.Format("{0}", _sumReverseModeWidths[i]);
                            else
                                QA_XLS_COL9_VALUE = "";
                            if (i < (int)_sumForwardModeWidths.Count)
                                QA_XLS_COL10_VALUE = string.Format("{0}", _sumForwardModeWidths[i]);
                            else
                                QA_XLS_COL10_VALUE = "";
                            if (i < (int)_sumPercWorkingRegions.Count)
                                QA_XLS_COL11_VALUE = string.Format("{0}", _sumPercWorkingRegions[i]);
                            else
                                QA_XLS_COL11_VALUE = "";
                        }
                        else if (QA_XLS_COL3_VALUE == QA_XLS_COUNT)
                        {
                            if (i < (int)_numForwardLowerModeSlopeSamples.Count)
                                QA_XLS_COL4_VALUE = string.Format("{0}", _numForwardLowerModeSlopeSamples[i]);
                            else
                                QA_XLS_COL4_VALUE = "";
                            if (i < (int)_numForwardUpperModeSlopeSamples.Count)
                                QA_XLS_COL5_VALUE = string.Format("{0}", _numForwardUpperModeSlopeSamples[i]);
                            else
                                QA_XLS_COL5_VALUE = "";
                            if (i < (int)_numReverseLowerModeSlopeSamples.Count)
                                QA_XLS_COL6_VALUE = string.Format("{0}", _numReverseLowerModeSlopeSamples[i]);
                            else
                                QA_XLS_COL6_VALUE = "";
                            if (i < (int)_numReverseUpperModeSlopeSamples.Count)
                                QA_XLS_COL7_VALUE = string.Format("{0}", _numReverseUpperModeSlopeSamples[i]);
                            else
                                QA_XLS_COL7_VALUE = "";
                            if (i < (int)_numStableAreaWidthSamples.Count)
                                QA_XLS_COL8_VALUE = string.Format("{0}", _numStableAreaWidthSamples[i]);
                            else
                                QA_XLS_COL8_VALUE = "";
                            if (i < (int)_numReverseModeWidthSamples.Count)
                                QA_XLS_COL9_VALUE = string.Format("{0}", _numReverseModeWidthSamples[i]);
                            else
                                QA_XLS_COL9_VALUE = "";
                            if (i < (int)_numForwardModeWidthSamples.Count)
                                QA_XLS_COL10_VALUE = string.Format("{0}", _numForwardModeWidthSamples[i]);
                            else
                                QA_XLS_COL10_VALUE = "";
                            if (i < (int)_numPercWorkingRegionSamples.Count)
                                QA_XLS_COL11_VALUE = string.Format("{0}", _numPercWorkingRegionSamples[i]);
                            else
                                QA_XLS_COL11_VALUE = "";
                        }
                        else if (QA_XLS_COL3_VALUE == QA_XLS_STDDEV)
                        {
                            if (i < (int)_stdDevForwardLowerModeSlopes.Count)
                                QA_XLS_COL4_VALUE = string.Format("{0}", _stdDevForwardLowerModeSlopes[i]);
                            else
                                QA_XLS_COL4_VALUE = "";
                            if (i < (int)_stdDevForwardUpperModeSlopes.Count)
                                QA_XLS_COL5_VALUE = string.Format("{0}", _stdDevForwardUpperModeSlopes[i]);
                            else
                                QA_XLS_COL5_VALUE = "";
                            if (i < (int)_stdDevReverseLowerModeSlopes.Count)
                                QA_XLS_COL6_VALUE = string.Format("{0}", _stdDevReverseLowerModeSlopes[i]);
                            else
                                QA_XLS_COL6_VALUE = "";
                            if (i < (int)_stdDevReverseUpperModeSlopes.Count)
                                QA_XLS_COL7_VALUE = string.Format("{0}", _stdDevReverseUpperModeSlopes[i]);
                            else
                                QA_XLS_COL7_VALUE = "";
                            if (i < (int)_stdDevStableAreaWidths.Count)
                                QA_XLS_COL8_VALUE = string.Format("{0}", _stdDevStableAreaWidths[i]);
                            else
                                QA_XLS_COL8_VALUE = "";
                            if (i < (int)_stdDevReverseModeWidths.Count)
                                QA_XLS_COL9_VALUE = string.Format("{0}", _stdDevReverseModeWidths[i]);
                            else
                                QA_XLS_COL9_VALUE = "";
                            if (i < (int)_stdDevForwardModeWidths.Count)
                                QA_XLS_COL10_VALUE = string.Format("{0}", _stdDevForwardModeWidths[i]);
                            else
                                QA_XLS_COL10_VALUE = "";
                            if (i < (int)_stdDevPercWorkingRegions.Count)
                                QA_XLS_COL11_VALUE = string.Format("{0}", _stdDevPercWorkingRegions[i]);
                            else
                                QA_XLS_COL11_VALUE = "";
                        }
                        else if (QA_XLS_COL3_VALUE == QA_XLS_VAR)
                        {
                            if (i < (int)_varForwardLowerModeSlopes.Count)
                                QA_XLS_COL4_VALUE = string.Format("{0}", _varForwardLowerModeSlopes[i]);
                            else
                                QA_XLS_COL4_VALUE = "";
                            if (i < (int)_varForwardUpperModeSlopes.Count)
                                QA_XLS_COL5_VALUE = string.Format("{0}", _varForwardUpperModeSlopes[i]);
                            else
                                QA_XLS_COL5_VALUE = "";
                            if (i < (int)_varReverseLowerModeSlopes.Count)
                                QA_XLS_COL6_VALUE = string.Format("{0}", _varReverseLowerModeSlopes[i]);
                            else
                                QA_XLS_COL6_VALUE = "";
                            if (i < (int)_varReverseUpperModeSlopes.Count)
                                QA_XLS_COL7_VALUE = string.Format("{0}", _varReverseUpperModeSlopes[i]);
                            else
                                QA_XLS_COL7_VALUE = "";
                            if (i < (int)_varStableAreaWidths.Count)
                                QA_XLS_COL8_VALUE = string.Format("{0}", _varStableAreaWidths[i]);
                            else
                                QA_XLS_COL8_VALUE = "";
                            if (i < (int)_varReverseModeWidths.Count)
                                QA_XLS_COL9_VALUE = string.Format("{0}", _varReverseModeWidths[i]);
                            else
                                QA_XLS_COL9_VALUE = "";
                            if (i < (int)_varForwardModeWidths.Count)
                                QA_XLS_COL10_VALUE = string.Format("{0}", _varForwardModeWidths[i]);
                            else
                                QA_XLS_COL10_VALUE = "";
                            if (i < (int)_varPercWorkingRegions.Count)
                                QA_XLS_COL11_VALUE = string.Format("{0}", _varPercWorkingRegions[i]);
                            else
                                QA_XLS_COL11_VALUE = "";
                        }
                        else if (QA_XLS_COL3_VALUE == QA_XLS_OTHERS)
                        {
                            if (i < (int)_forwardLowerModalDistortionAngles.Count)
                                QA_XLS_COL12_VALUE = string.Format("{0}", _forwardLowerModalDistortionAngles[i]);
                            else
                                QA_XLS_COL12_VALUE = "";
                            if (i < (int)_forwardLowerModalDistortionAngleXPositions.Count)
                                QA_XLS_COL13_VALUE = string.Format("{0}", _forwardLowerModalDistortionAngleXPositions[i]);
                            else
                                QA_XLS_COL13_VALUE = "";
                            if (i < (int)_forwardUpperModalDistortionAngles.Count)
                                QA_XLS_COL14_VALUE = string.Format("{0}", _forwardUpperModalDistortionAngles[i]);
                            else
                                QA_XLS_COL14_VALUE = "";
                            if (i < (int)_forwardUpperModalDistortionAngleXPositions.Count)
                                QA_XLS_COL15_VALUE = string.Format("{0}", _forwardUpperModalDistortionAngleXPositions[i]);
                            else
                                QA_XLS_COL15_VALUE = "";
                            if (i < (int)_reverseLowerModalDistortionAngles.Count)
                                QA_XLS_COL16_VALUE = string.Format("{0}", _reverseLowerModalDistortionAngles[i]);
                            else
                                QA_XLS_COL16_VALUE = "";
                            if (i < (int)_reverseLowerModalDistortionAngleXPositions.Count)
                                QA_XLS_COL17_VALUE = string.Format("{0}", _reverseLowerModalDistortionAngleXPositions[i]);
                            else
                                QA_XLS_COL17_VALUE = "";
                            if (i < (int)_reverseUpperModalDistortionAngles.Count)
                                QA_XLS_COL18_VALUE = string.Format("{0}", _reverseUpperModalDistortionAngles[i]);
                            else
                                QA_XLS_COL18_VALUE = "";
                            if (i < (int)_reverseUpperModalDistortionAngleXPositions.Count)
                                QA_XLS_COL19_VALUE = string.Format("{0}", _reverseUpperModalDistortionAngleXPositions[i]);
                            else
                                QA_XLS_COL19_VALUE = "";
                            if (i < (int)_maxForwardLowerModeBDCAreaXLengths.Count)
                                QA_XLS_COL20_VALUE = string.Format("{0}", _maxForwardLowerModeBDCAreaXLengths[i]);
                            else
                                QA_XLS_COL20_VALUE = "";
                            if (i < (int)_maxForwardUpperModeBDCAreaXLengths.Count)
                                QA_XLS_COL21_VALUE = string.Format("{0}", _maxForwardUpperModeBDCAreaXLengths[i]);
                            else
                                QA_XLS_COL21_VALUE = "";
                            if (i < (int)_maxForwardLowerModeBDCAreas.Count)
                                QA_XLS_COL22_VALUE = string.Format("{0}", _maxForwardLowerModeBDCAreas[i]);
                            else
                                QA_XLS_COL22_VALUE = "";
                            if (i < (int)_maxForwardUpperModeBDCAreas.Count)
                                QA_XLS_COL23_VALUE = string.Format("{0}", _maxForwardUpperModeBDCAreas[i]);
                            else
                                QA_XLS_COL23_VALUE = "";
                            if (i < (int)_sumForwardLowerModeBDCAreas.Count)
                                QA_XLS_COL24_VALUE = string.Format("{0}", _sumForwardLowerModeBDCAreas[i]);
                            else
                                QA_XLS_COL24_VALUE = "";
                            if (i < (int)_sumForwardUpperModeBDCAreas.Count)
                                QA_XLS_COL25_VALUE = string.Format("{0}", _sumForwardUpperModeBDCAreas[i]);
                            else
                                QA_XLS_COL25_VALUE = "";
                            if (i < (int)_numForwardLowerModeBDCAreaSamples.Count)
                                QA_XLS_COL26_VALUE = string.Format("{0}", _numForwardLowerModeBDCAreaSamples[i]);
                            else
                                QA_XLS_COL26_VALUE = "";
                            if (i < (int)_numForwardUpperModeBDCAreaSamples.Count)
                                QA_XLS_COL27_VALUE = string.Format("{0}", _numForwardUpperModeBDCAreaSamples[i]);
                            else
                                QA_XLS_COL27_VALUE = "";
                            if (i < (int)_maxReverseLowerModeBDCAreaXLengths.Count)
                                QA_XLS_COL28_VALUE = string.Format("{0}", _maxReverseLowerModeBDCAreaXLengths[i]);
                            else
                                QA_XLS_COL28_VALUE = "";
                            if (i < (int)_maxReverseUpperModeBDCAreaXLengths.Count)
                                QA_XLS_COL29_VALUE = string.Format("{0}", _maxReverseUpperModeBDCAreaXLengths[i]);
                            else
                                QA_XLS_COL29_VALUE = "";
                            if (i < (int)_maxReverseLowerModeBDCAreas.Count)
                                QA_XLS_COL30_VALUE = string.Format("{0}", _maxReverseLowerModeBDCAreas[i]);
                            else
                                QA_XLS_COL30_VALUE = "";
                            if (i < (int)_maxReverseUpperModeBDCAreas.Count)
                                QA_XLS_COL31_VALUE = string.Format("{0}", _maxReverseUpperModeBDCAreas[i]);
                            else
                                QA_XLS_COL31_VALUE = "";
                            if (i < (int)_sumReverseLowerModeBDCAreas.Count)
                                QA_XLS_COL32_VALUE = string.Format("{0}", _sumReverseLowerModeBDCAreas[i]);
                            else
                                QA_XLS_COL32_VALUE = "";
                            if (i < (int)_sumReverseUpperModeBDCAreas.Count)
                                QA_XLS_COL33_VALUE = string.Format("{0}", _sumReverseUpperModeBDCAreas[i]);
                            else
                                QA_XLS_COL33_VALUE = "";
                            if (i < (int)_numReverseLowerModeBDCAreaSamples.Count)
                                QA_XLS_COL34_VALUE = string.Format("{0}", _numReverseLowerModeBDCAreaSamples[i]);
                            else
                                QA_XLS_COL34_VALUE = "";
                            if (i < (int)_numReverseUpperModeBDCAreaSamples.Count)
                                QA_XLS_COL35_VALUE = string.Format("{0}", _numReverseUpperModeBDCAreaSamples[i]);
                            else
                                QA_XLS_COL35_VALUE = "";
                            if (i < (int)_middleLineRMSValues.Count)
                                QA_XLS_COL36_VALUE = string.Format("{0}", _middleLineRMSValues[i]);
                            else
                                QA_XLS_COL36_VALUE = "";
                        }


                        sw.Write(QA_XLS_COL1_VALUE + ",");
                        sw.Write(QA_XLS_COL2_VALUE + ",");
                        sw.Write(QA_XLS_COL3_VALUE + ",");
                        sw.Write(QA_XLS_COL4_VALUE + ",");
                        sw.Write(QA_XLS_COL5_VALUE + ",");
                        sw.Write(QA_XLS_COL6_VALUE + ",");
                        sw.Write(QA_XLS_COL7_VALUE + ",");
                        sw.Write(QA_XLS_COL8_VALUE + ",");
                        sw.Write(QA_XLS_COL9_VALUE + ",");
                        sw.Write(QA_XLS_COL10_VALUE + ",");
                        sw.Write(QA_XLS_COL11_VALUE + ",");
                        sw.Write(QA_XLS_COL12_VALUE + ",");
                        sw.Write(QA_XLS_COL13_VALUE + ",");
                        sw.Write(QA_XLS_COL14_VALUE + ",");
                        sw.Write(QA_XLS_COL15_VALUE + ",");
                        sw.Write(QA_XLS_COL16_VALUE + ",");
                        sw.Write(QA_XLS_COL17_VALUE + ",");
                        sw.Write(QA_XLS_COL18_VALUE + ",");
                        sw.Write(QA_XLS_COL19_VALUE + ",");
                        sw.Write(QA_XLS_COL20_VALUE + ",");
                        sw.Write(QA_XLS_COL21_VALUE + ",");
                        sw.Write(QA_XLS_COL22_VALUE + ",");
                        sw.Write(QA_XLS_COL23_VALUE + ",");
                        sw.Write(QA_XLS_COL24_VALUE + ",");
                        sw.Write(QA_XLS_COL25_VALUE + ",");
                        sw.Write(QA_XLS_COL26_VALUE + ",");
                        sw.Write(QA_XLS_COL27_VALUE + ",");
                        sw.Write(QA_XLS_COL28_VALUE + ",");
                        sw.Write(QA_XLS_COL29_VALUE + ",");
                        sw.Write(QA_XLS_COL30_VALUE + ",");
                        sw.Write(QA_XLS_COL31_VALUE + ",");
                        sw.Write(QA_XLS_COL32_VALUE + ",");
                        sw.Write(QA_XLS_COL33_VALUE + ",");
                        sw.Write(QA_XLS_COL34_VALUE + ",");
                        sw.Write(QA_XLS_COL35_VALUE + ",");
                        sw.Write(QA_XLS_COL36_VALUE + ",");
                        sw.WriteLine();


                    }
                    sw.WriteLine();
                }

                sw.Flush();
            }
        }


        public void writePassFailResultsToFile(string fileFullName, int sm_i)
        {
            using (StreamWriter sw = new StreamWriter(fileFullName, true))
            {
                //// write 3 empty lines
                //sw.WriteLine("Supermode %d Map Pass/Fail Analysis", sm_i);

                if (_passFailAnalysisComplete)
                {
                    string errorMessage = "";
                    int errorNum = 0;
                    getQAError(_qaDetectedError, ref errorNum, ref errorMessage);
                    sw.WriteLine(errorMessage);
                    sw.WriteLine("");


                    sw.WriteLine("Slope Window Size,{0}", superMapQASitting["SlopeWindowSize"]);
                    sw.WriteLine("Modal Distortion Min X,{0}", superMapQASitting["ModalDistortionMinX"]);
                    sw.WriteLine("Modal Distortion Max X,{0}", superMapQASitting["ModalDistortionMaxX"]);
                    sw.WriteLine("Modal Distortion Min Y,{0}", superMapQASitting["ModalDistortionMinY"]);
                    sw.WriteLine("");

                    // write header
                    const string QA_XLS_HEADER1 = "";
                    const string QA_XLS_HEADER2 = "Upper line mode borders removed due to error";
                    const string QA_XLS_HEADER3 = "Lower line mode borders removed due to error";
                    const string QA_XLS_HEADER4 = "Middle lines removed due to error";
                    const string QA_XLS_HEADER5 = "Forward Max Mode Width";
                    const string QA_XLS_HEADER6 = "Reverse Max Mode Width";
                    const string QA_XLS_HEADER7 = "Mean % Working Region";
                    const string QA_XLS_HEADER8 = "Min % Working Region";
                    const string QA_XLS_HEADER9 = "Forward Min Mode Width";
                    const string QA_XLS_HEADER10 = "Reverse Min Mode Width";
                    const string QA_XLS_HEADER11 = "Forward Max Lower Modal Distortion Angle";
                    const string QA_XLS_HEADER12 = "Forward Max Lower Modal Distortion X-Position";
                    const string QA_XLS_HEADER13 = "Forward Max Upper Modal Distortion Angle";
                    const string QA_XLS_HEADER14 = "Forward Max Upper Modal Distortion X-Position";
                    const string QA_XLS_HEADER15 = "Reverse Max Lower Modal Distortion Angle";
                    const string QA_XLS_HEADER16 = "Reverse Max Lower Modal Distortion X-Position";
                    const string QA_XLS_HEADER17 = "Reverse Max Upper Modal Distortion Angle";
                    const string QA_XLS_HEADER18 = "Reverse Max Upper Modal Distortion X-Position";
                    const string QA_XLS_HEADER19 = "Forward Lower Max Boundary Direction Change X-Length";
                    const string QA_XLS_HEADER20 = "Forward Upper Max Boundary Direction Change X-Length";
                    const string QA_XLS_HEADER21 = "Forward Lower Max Boundary Direction Change Area";
                    const string QA_XLS_HEADER22 = "Forward Upper Max Boundary Direction Change Area";
                    const string QA_XLS_HEADER23 = "Forward Lower Sum of Boundary Direction Change Areas";
                    const string QA_XLS_HEADER24 = "Forward Upper Sum of Boundary Direction Change Areas";
                    const string QA_XLS_HEADER25 = "Forward Lower Number of Boundary Direction Changes";
                    const string QA_XLS_HEADER26 = "Forward Upper Number of Boundary Direction Changes";
                    const string QA_XLS_HEADER27 = "Reverse Lower Max Boundary Direction Change X-Length";
                    const string QA_XLS_HEADER28 = "Reverse Upper Max Boundary Direction Change X-Length";
                    const string QA_XLS_HEADER29 = "Reverse Lower Max Boundary Direction Change Area";
                    const string QA_XLS_HEADER30 = "Reverse Upper Max Boundary Direction Change Area";
                    const string QA_XLS_HEADER31 = "Reverse Lower Sum of Boundary Direction Change Areas";
                    const string QA_XLS_HEADER32 = "Reverse Upper Sum of Boundary Direction Change Areas";
                    const string QA_XLS_HEADER33 = "Reverse Lower Number of Boundary Direction Changes";
                    const string QA_XLS_HEADER34 = "Reverse Upper Number of Boundary Direction Changes";
                    const string QA_XLS_HEADER35 = "Forward Lower Max Mode Slope";
                    const string QA_XLS_HEADER36 = "Forward Upper Max Mode Slope";
                    const string QA_XLS_HEADER37 = "Reverse Lower Max Mode Slope";
                    const string QA_XLS_HEADER38 = "Reverse Upper Max Mode Slope";
                    const string QA_XLS_HEADER39 = "Max Middle Line RMS Value";
                    const string QA_XLS_HEADER40 = "Min Middle Line Slope";
                    const string QA_XLS_HEADER41 = "Max Middle Line Slope";


                    const string QA_XLS_ROWTITLE1 = "Pass/Fail Result";
                    const string QA_XLS_ROWTITLE2 = "Min Value Measured";
                    const string QA_XLS_ROWTITLE3 = "Max Value Measured";
                    const string QA_XLS_ROWTITLE4 = "Number of Failed LMs";
                    const string QA_XLS_ROWTITLE5 = "Threshold Applied";

                    sw.Write(QA_XLS_HEADER1 + ",");
                    sw.Write(QA_XLS_HEADER2 + ",");
                    sw.Write(QA_XLS_HEADER3 + ",");
                    sw.Write(QA_XLS_HEADER4 + ",");
                    sw.Write(QA_XLS_HEADER5 + ",");
                    sw.Write(QA_XLS_HEADER6 + ",");
                    sw.Write(QA_XLS_HEADER7 + ",");
                    sw.Write(QA_XLS_HEADER8 + ",");
                    sw.Write(QA_XLS_HEADER9 + ",");
                    sw.Write(QA_XLS_HEADER10 + ",");
                    sw.Write(QA_XLS_HEADER11 + ",");
                    sw.Write(QA_XLS_HEADER12 + ",");
                    sw.Write(QA_XLS_HEADER13 + ",");
                    sw.Write(QA_XLS_HEADER14 + ",");
                    sw.Write(QA_XLS_HEADER15 + ",");
                    sw.Write(QA_XLS_HEADER16 + ",");
                    sw.Write(QA_XLS_HEADER17 + ",");
                    sw.Write(QA_XLS_HEADER18 + ",");
                    sw.Write(QA_XLS_HEADER19 + ",");
                    sw.Write(QA_XLS_HEADER20 + ",");
                    sw.Write(QA_XLS_HEADER21 + ",");
                    sw.Write(QA_XLS_HEADER22 + ",");
                    sw.Write(QA_XLS_HEADER23 + ",");
                    sw.Write(QA_XLS_HEADER24 + ",");
                    sw.Write(QA_XLS_HEADER25 + ",");
                    sw.Write(QA_XLS_HEADER26 + ",");
                    sw.Write(QA_XLS_HEADER27 + ",");
                    sw.Write(QA_XLS_HEADER28 + ",");
                    sw.Write(QA_XLS_HEADER29 + ",");
                    sw.Write(QA_XLS_HEADER30 + ",");
                    sw.Write(QA_XLS_HEADER31 + ",");
                    sw.Write(QA_XLS_HEADER32 + ",");
                    sw.Write(QA_XLS_HEADER33 + ",");
                    sw.Write(QA_XLS_HEADER34 + ",");
                    sw.Write(QA_XLS_HEADER35 + ",");
                    sw.Write(QA_XLS_HEADER36 + ",");
                    sw.Write(QA_XLS_HEADER37 + ",");
                    sw.Write(QA_XLS_HEADER38 + ",");
                    sw.Write(QA_XLS_HEADER39 + ",");
                    sw.Write(QA_XLS_HEADER40 + ",");
                    sw.Write(QA_XLS_HEADER41 + ",");
                    sw.WriteLine("");
                    sw.WriteLine();
                    //write pass or fail
                    string QA_XLS_COL1_VALUE = "";
                    string QA_XLS_COL2_VALUE = "";
                    string QA_XLS_COL3_VALUE = "";
                    string QA_XLS_COL4_VALUE = "";
                    string QA_XLS_COL5_VALUE = "";
                    string QA_XLS_COL6_VALUE = "";
                    string QA_XLS_COL7_VALUE = "";
                    string QA_XLS_COL8_VALUE = "";
                    string QA_XLS_COL9_VALUE = "";
                    string QA_XLS_COL10_VALUE = "";
                    string QA_XLS_COL11_VALUE = "";
                    string QA_XLS_COL12_VALUE = "";
                    string QA_XLS_COL13_VALUE = "";
                    string QA_XLS_COL14_VALUE = "";
                    string QA_XLS_COL15_VALUE = "";
                    string QA_XLS_COL16_VALUE = "";
                    string QA_XLS_COL17_VALUE = "";
                    string QA_XLS_COL18_VALUE = "";
                    string QA_XLS_COL19_VALUE = "";
                    string QA_XLS_COL20_VALUE = "";
                    string QA_XLS_COL21_VALUE = "";
                    string QA_XLS_COL22_VALUE = "";
                    string QA_XLS_COL23_VALUE = "";
                    string QA_XLS_COL24_VALUE = "";
                    string QA_XLS_COL25_VALUE = "";
                    string QA_XLS_COL26_VALUE = "";
                    string QA_XLS_COL27_VALUE = "";
                    string QA_XLS_COL28_VALUE = "";
                    string QA_XLS_COL29_VALUE = "";
                    string QA_XLS_COL30_VALUE = "";
                    string QA_XLS_COL31_VALUE = "";
                    string QA_XLS_COL32_VALUE = "";
                    string QA_XLS_COL33_VALUE = "";
                    string QA_XLS_COL34_VALUE = "";
                    string QA_XLS_COL35_VALUE = "";
                    string QA_XLS_COL36_VALUE = "";
                    string QA_XLS_COL37_VALUE = "";
                    string QA_XLS_COL38_VALUE = "";
                    string QA_XLS_COL39_VALUE = "";
                    string QA_XLS_COL40_VALUE = "";
                    string QA_XLS_COL41_VALUE = "";
                    /// first row - pass/fails
                    QA_XLS_COL2_VALUE = _maxUpperModeBordersRemovedPFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL3_VALUE = _maxLowerModeBordersRemovedPFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL4_VALUE = _maxMiddleLineRemovedPFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL5_VALUE = _maxForwardModeWidthPFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL6_VALUE = _maxReverseModeWidthPFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL7_VALUE = _meanPercWorkingRegionPFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL8_VALUE = _minPercWorkingRegionPFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL9_VALUE = _minForwardModeWidthPFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL10_VALUE = _minReverseModeWidthPFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL11_VALUE = _forwardLowerModalDistortionAnglePFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL12_VALUE = _forwardLowerModalDistortionAngleXPositionPFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL13_VALUE = _forwardUpperModalDistortionAnglePFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL14_VALUE = _forwardUpperModalDistortionAngleXPositionPFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL15_VALUE = _reverseLowerModalDistortionAnglePFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL16_VALUE = _reverseLowerModalDistortionAngleXPositionPFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL17_VALUE = _reverseUpperModalDistortionAnglePFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL18_VALUE = _reverseUpperModalDistortionAngleXPositionPFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL19_VALUE = _maxForwardLowerModeBDCAreaXLengthPFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL20_VALUE = _maxForwardUpperModeBDCAreaXLengthPFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL21_VALUE = _maxForwardLowerModeBDCAreaPFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL22_VALUE = _maxForwardUpperModeBDCAreaPFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL23_VALUE = _sumForwardLowerModeBDCAreasPFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL24_VALUE = _sumForwardUpperModeBDCAreasPFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL25_VALUE = _numForwardLowerModeBDCAreasPFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL26_VALUE = _numForwardUpperModeBDCAreasPFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL27_VALUE = _maxReverseLowerModeBDCAreaXLengthPFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL28_VALUE = _maxReverseUpperModeBDCAreaXLengthPFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL29_VALUE = _maxReverseLowerModeBDCAreaPFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL30_VALUE = _maxReverseUpperModeBDCAreaPFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL31_VALUE = _sumReverseLowerModeBDCAreasPFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL32_VALUE = _sumReverseUpperModeBDCAreasPFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL33_VALUE = _numReverseLowerModeBDCAreasPFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL34_VALUE = _numReverseUpperModeBDCAreasPFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL35_VALUE = _maxForwardLowerModeSlopePFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL36_VALUE = _maxForwardUpperModeSlopePFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL37_VALUE = _maxReverseLowerModeSlopePFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL38_VALUE = _maxReverseUpperModeSlopePFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL39_VALUE = _middleLineRMSValuesPFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL40_VALUE = _minMiddleLineSlopePFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL41_VALUE = _maxMiddleLineSlopePFAnalysis.pass ? "PASS" : "FAIL";


                    sw.Write(QA_XLS_ROWTITLE1 + ",");
                    sw.Write(QA_XLS_COL2_VALUE + ",");
                    sw.Write(QA_XLS_COL3_VALUE + ",");
                    sw.Write(QA_XLS_COL4_VALUE + ",");
                    sw.Write(QA_XLS_COL5_VALUE + ",");
                    sw.Write(QA_XLS_COL6_VALUE + ",");
                    sw.Write(QA_XLS_COL7_VALUE + ",");
                    sw.Write(QA_XLS_COL8_VALUE + ",");
                    sw.Write(QA_XLS_COL9_VALUE + ",");
                    sw.Write(QA_XLS_COL10_VALUE + ",");
                    sw.Write(QA_XLS_COL11_VALUE + ",");
                    sw.Write(QA_XLS_COL12_VALUE + ",");
                    sw.Write(QA_XLS_COL13_VALUE + ",");
                    sw.Write(QA_XLS_COL14_VALUE + ",");
                    sw.Write(QA_XLS_COL15_VALUE + ",");
                    sw.Write(QA_XLS_COL16_VALUE + ",");
                    sw.Write(QA_XLS_COL17_VALUE + ",");
                    sw.Write(QA_XLS_COL18_VALUE + ",");
                    sw.Write(QA_XLS_COL19_VALUE + ",");
                    sw.Write(QA_XLS_COL20_VALUE + ",");
                    sw.Write(QA_XLS_COL21_VALUE + ",");
                    sw.Write(QA_XLS_COL22_VALUE + ",");
                    sw.Write(QA_XLS_COL23_VALUE + ",");
                    sw.Write(QA_XLS_COL24_VALUE + ",");
                    sw.Write(QA_XLS_COL25_VALUE + ",");
                    sw.Write(QA_XLS_COL26_VALUE + ",");
                    sw.Write(QA_XLS_COL27_VALUE + ",");
                    sw.Write(QA_XLS_COL28_VALUE + ",");
                    sw.Write(QA_XLS_COL29_VALUE + ",");
                    sw.Write(QA_XLS_COL30_VALUE + ",");
                    sw.Write(QA_XLS_COL31_VALUE + ",");
                    sw.Write(QA_XLS_COL32_VALUE + ",");
                    sw.Write(QA_XLS_COL33_VALUE + ",");
                    sw.Write(QA_XLS_COL34_VALUE + ",");
                    sw.Write(QA_XLS_COL35_VALUE + ",");
                    sw.Write(QA_XLS_COL36_VALUE + ",");
                    sw.Write(QA_XLS_COL37_VALUE + ",");
                    sw.Write(QA_XLS_COL38_VALUE + ",");
                    sw.Write(QA_XLS_COL39_VALUE + ",");
                    sw.Write(QA_XLS_COL40_VALUE + ",");
                    sw.Write(QA_XLS_COL41_VALUE);
                    sw.WriteLine();

                    //write	num failed Modes
                    QA_XLS_COL2_VALUE = string.Format("{0}", _numUpperLinesRemoved);
                    QA_XLS_COL3_VALUE = string.Format("{0}", _numLowerLinesRemoved);
                    QA_XLS_COL4_VALUE = string.Format("{0}", _numMiddleLinesRemoved);
                    QA_XLS_COL5_VALUE = string.Format("{0}", _maxForwardModeWidthPFAnalysis.numFailedModes);
                    QA_XLS_COL6_VALUE = string.Format("{0}", _maxReverseModeWidthPFAnalysis.numFailedModes);
                    QA_XLS_COL7_VALUE = string.Format("{0}", _meanPercWorkingRegionPFAnalysis.numFailedModes);
                    QA_XLS_COL8_VALUE = string.Format("{0}", _minPercWorkingRegionPFAnalysis.numFailedModes);
                    QA_XLS_COL9_VALUE = string.Format("{0}", _minForwardModeWidthPFAnalysis.numFailedModes);
                    QA_XLS_COL10_VALUE = string.Format("{0}", _minReverseModeWidthPFAnalysis.numFailedModes);
                    QA_XLS_COL11_VALUE = string.Format("{0}", _forwardLowerModalDistortionAnglePFAnalysis.numFailedModes);
                    QA_XLS_COL12_VALUE = string.Format("{0}", _forwardLowerModalDistortionAngleXPositionPFAnalysis.numFailedModes);
                    QA_XLS_COL13_VALUE = string.Format("{0}", _forwardUpperModalDistortionAnglePFAnalysis.numFailedModes);
                    QA_XLS_COL14_VALUE = string.Format("{0}", _forwardUpperModalDistortionAngleXPositionPFAnalysis.numFailedModes);
                    QA_XLS_COL15_VALUE = string.Format("{0}", _reverseLowerModalDistortionAnglePFAnalysis.numFailedModes);
                    QA_XLS_COL16_VALUE = string.Format("{0}", _reverseLowerModalDistortionAngleXPositionPFAnalysis.numFailedModes);
                    QA_XLS_COL17_VALUE = string.Format("{0}", _reverseUpperModalDistortionAnglePFAnalysis.numFailedModes);
                    QA_XLS_COL18_VALUE = string.Format("{0}", _reverseUpperModalDistortionAngleXPositionPFAnalysis.numFailedModes);
                    QA_XLS_COL19_VALUE = string.Format("{0}", _maxForwardLowerModeBDCAreaXLengthPFAnalysis.numFailedModes);
                    QA_XLS_COL20_VALUE = string.Format("{0}", _maxForwardUpperModeBDCAreaXLengthPFAnalysis.numFailedModes);
                    QA_XLS_COL21_VALUE = string.Format("{0}", _maxForwardLowerModeBDCAreaPFAnalysis.numFailedModes);
                    QA_XLS_COL22_VALUE = string.Format("{0}", _maxForwardUpperModeBDCAreaPFAnalysis.numFailedModes);
                    QA_XLS_COL23_VALUE = string.Format("{0}", _sumForwardLowerModeBDCAreasPFAnalysis.numFailedModes);
                    QA_XLS_COL24_VALUE = string.Format("{0}", _sumForwardUpperModeBDCAreasPFAnalysis.numFailedModes);
                    QA_XLS_COL25_VALUE = string.Format("{0}", _numForwardLowerModeBDCAreasPFAnalysis.numFailedModes);
                    QA_XLS_COL26_VALUE = string.Format("{0}", _numForwardUpperModeBDCAreasPFAnalysis.numFailedModes);
                    QA_XLS_COL27_VALUE = string.Format("{0}", _maxReverseLowerModeBDCAreaXLengthPFAnalysis.numFailedModes);
                    QA_XLS_COL28_VALUE = string.Format("{0}", _maxReverseUpperModeBDCAreaXLengthPFAnalysis.numFailedModes);
                    QA_XLS_COL29_VALUE = string.Format("{0}", _maxReverseLowerModeBDCAreaPFAnalysis.numFailedModes);
                    QA_XLS_COL30_VALUE = string.Format("{0}", _maxReverseUpperModeBDCAreaPFAnalysis.numFailedModes);
                    QA_XLS_COL31_VALUE = string.Format("{0}", _sumReverseLowerModeBDCAreasPFAnalysis.numFailedModes);
                    QA_XLS_COL32_VALUE = string.Format("{0}", _sumReverseUpperModeBDCAreasPFAnalysis.numFailedModes);
                    QA_XLS_COL33_VALUE = string.Format("{0}", _numReverseLowerModeBDCAreasPFAnalysis.numFailedModes);
                    QA_XLS_COL34_VALUE = string.Format("{0}", _numReverseUpperModeBDCAreasPFAnalysis.numFailedModes);
                    QA_XLS_COL35_VALUE = string.Format("{0}", _maxForwardLowerModeSlopePFAnalysis.numFailedModes);
                    QA_XLS_COL36_VALUE = string.Format("{0}", _maxForwardUpperModeSlopePFAnalysis.numFailedModes);
                    QA_XLS_COL37_VALUE = string.Format("{0}", _maxReverseLowerModeSlopePFAnalysis.numFailedModes);
                    QA_XLS_COL38_VALUE = string.Format("{0}", _maxReverseUpperModeSlopePFAnalysis.numFailedModes);
                    QA_XLS_COL39_VALUE = string.Format("{0}", _middleLineRMSValuesPFAnalysis.numFailedModes);
                    QA_XLS_COL40_VALUE = string.Format("{0}", _minMiddleLineSlopePFAnalysis.numFailedModes);
                    QA_XLS_COL41_VALUE = string.Format("{0}", _maxMiddleLineSlopePFAnalysis.numFailedModes);

                    sw.Write(QA_XLS_ROWTITLE4 + ",");
                    sw.Write(QA_XLS_COL2_VALUE + ",");
                    sw.Write(QA_XLS_COL3_VALUE + ",");
                    sw.Write(QA_XLS_COL4_VALUE + ",");
                    sw.Write(QA_XLS_COL5_VALUE + ",");
                    sw.Write(QA_XLS_COL6_VALUE + ",");
                    sw.Write(QA_XLS_COL7_VALUE + ",");
                    sw.Write(QA_XLS_COL8_VALUE + ",");
                    sw.Write(QA_XLS_COL9_VALUE + ",");
                    sw.Write(QA_XLS_COL10_VALUE + ",");
                    sw.Write(QA_XLS_COL11_VALUE + ",");
                    sw.Write(QA_XLS_COL12_VALUE + ",");
                    sw.Write(QA_XLS_COL13_VALUE + ",");
                    sw.Write(QA_XLS_COL14_VALUE + ",");
                    sw.Write(QA_XLS_COL15_VALUE + ",");
                    sw.Write(QA_XLS_COL16_VALUE + ",");
                    sw.Write(QA_XLS_COL17_VALUE + ",");
                    sw.Write(QA_XLS_COL18_VALUE + ",");
                    sw.Write(QA_XLS_COL19_VALUE + ",");
                    sw.Write(QA_XLS_COL20_VALUE + ",");
                    sw.Write(QA_XLS_COL21_VALUE + ",");
                    sw.Write(QA_XLS_COL22_VALUE + ",");
                    sw.Write(QA_XLS_COL23_VALUE + ",");
                    sw.Write(QA_XLS_COL24_VALUE + ",");
                    sw.Write(QA_XLS_COL25_VALUE + ",");
                    sw.Write(QA_XLS_COL26_VALUE + ",");
                    sw.Write(QA_XLS_COL27_VALUE + ",");
                    sw.Write(QA_XLS_COL28_VALUE + ",");
                    sw.Write(QA_XLS_COL29_VALUE + ",");
                    sw.Write(QA_XLS_COL30_VALUE + ",");
                    sw.Write(QA_XLS_COL31_VALUE + ",");
                    sw.Write(QA_XLS_COL32_VALUE + ",");
                    sw.Write(QA_XLS_COL33_VALUE + ",");
                    sw.Write(QA_XLS_COL34_VALUE + ",");
                    sw.Write(QA_XLS_COL35_VALUE + ",");
                    sw.Write(QA_XLS_COL36_VALUE + ",");
                    sw.Write(QA_XLS_COL37_VALUE + ",");
                    sw.Write(QA_XLS_COL38_VALUE + ",");
                    sw.Write(QA_XLS_COL39_VALUE + ",");
                    sw.Write(QA_XLS_COL40_VALUE + ",");
                    sw.Write(QA_XLS_COL41_VALUE);
                    sw.WriteLine();
                    // write thresholds and its back
                    //	thresholds

                    QA_XLS_COL2_VALUE = string.Format(" > {0}", _maxUpperModeBordersRemovedPFAnalysis.threshold);
                    QA_XLS_COL3_VALUE = string.Format(" > {0}", _maxLowerModeBordersRemovedPFAnalysis.threshold);
                    QA_XLS_COL4_VALUE = string.Format(" > {0}", _maxMiddleLineRemovedPFAnalysis.threshold);
                    QA_XLS_COL5_VALUE = string.Format(" > {0}", _maxForwardModeWidthPFAnalysis.threshold);
                    QA_XLS_COL6_VALUE = string.Format(" > {0}", _maxReverseModeWidthPFAnalysis.threshold);
                    QA_XLS_COL7_VALUE = string.Format(" > {0}", _meanPercWorkingRegionPFAnalysis.threshold);
                    QA_XLS_COL8_VALUE = string.Format(" > {0}", _minPercWorkingRegionPFAnalysis.threshold);
                    QA_XLS_COL9_VALUE = string.Format(" < {0}", _minForwardModeWidthPFAnalysis.threshold);
                    QA_XLS_COL10_VALUE = string.Format(" < {0}", _minReverseModeWidthPFAnalysis.threshold);
                    QA_XLS_COL11_VALUE = string.Format(" > {0}", _forwardLowerModalDistortionAnglePFAnalysis.threshold);
                    QA_XLS_COL12_VALUE = string.Format(" > {0}", _forwardLowerModalDistortionAngleXPositionPFAnalysis.threshold);
                    QA_XLS_COL13_VALUE = string.Format(" > {0}", _forwardUpperModalDistortionAnglePFAnalysis.threshold);
                    QA_XLS_COL14_VALUE = string.Format(" > {0}", _forwardUpperModalDistortionAngleXPositionPFAnalysis.threshold);
                    QA_XLS_COL15_VALUE = string.Format(" > {0}", _reverseLowerModalDistortionAnglePFAnalysis.threshold);
                    QA_XLS_COL16_VALUE = string.Format(" > {0}", _reverseLowerModalDistortionAngleXPositionPFAnalysis.threshold);
                    QA_XLS_COL17_VALUE = string.Format(" > {0}", _reverseUpperModalDistortionAnglePFAnalysis.threshold);
                    QA_XLS_COL18_VALUE = string.Format(" > {0}", _reverseUpperModalDistortionAngleXPositionPFAnalysis.threshold);
                    QA_XLS_COL19_VALUE = string.Format(" > {0}", _maxForwardLowerModeBDCAreaXLengthPFAnalysis.threshold);
                    QA_XLS_COL20_VALUE = string.Format(" > {0}", _maxForwardUpperModeBDCAreaXLengthPFAnalysis.threshold);
                    QA_XLS_COL21_VALUE = string.Format(" > {0}", _maxForwardLowerModeBDCAreaPFAnalysis.threshold);
                    QA_XLS_COL22_VALUE = string.Format(" > {0}", _maxForwardUpperModeBDCAreaPFAnalysis.threshold);
                    QA_XLS_COL23_VALUE = string.Format(" > {0}", _sumForwardLowerModeBDCAreasPFAnalysis.threshold);
                    QA_XLS_COL24_VALUE = string.Format(" > {0}", _sumForwardUpperModeBDCAreasPFAnalysis.threshold);
                    QA_XLS_COL25_VALUE = string.Format(" > {0}", _numForwardLowerModeBDCAreasPFAnalysis.threshold);
                    QA_XLS_COL26_VALUE = string.Format(" > {0}", _numForwardUpperModeBDCAreasPFAnalysis.threshold);
                    QA_XLS_COL27_VALUE = string.Format(" > {0}", _maxReverseLowerModeBDCAreaXLengthPFAnalysis.threshold);
                    QA_XLS_COL28_VALUE = string.Format(" > {0}", _maxReverseUpperModeBDCAreaXLengthPFAnalysis.threshold);
                    QA_XLS_COL29_VALUE = string.Format(" > {0}", _maxReverseLowerModeBDCAreaPFAnalysis.threshold);
                    QA_XLS_COL30_VALUE = string.Format(" > {0}", _maxReverseUpperModeBDCAreaPFAnalysis.threshold);
                    QA_XLS_COL31_VALUE = string.Format(" > {0}", _sumReverseLowerModeBDCAreasPFAnalysis.threshold);
                    QA_XLS_COL32_VALUE = string.Format(" > {0}", _sumReverseUpperModeBDCAreasPFAnalysis.threshold);
                    QA_XLS_COL33_VALUE = string.Format(" > {0}", _numReverseLowerModeBDCAreasPFAnalysis.threshold);
                    QA_XLS_COL34_VALUE = string.Format(" > {0}", _numReverseUpperModeBDCAreasPFAnalysis.threshold);
                    QA_XLS_COL35_VALUE = string.Format(" > {0}", _maxForwardLowerModeSlopePFAnalysis.threshold);
                    QA_XLS_COL36_VALUE = string.Format(" > {0}", _maxForwardUpperModeSlopePFAnalysis.threshold);
                    QA_XLS_COL37_VALUE = string.Format(" > {0}", _maxReverseLowerModeSlopePFAnalysis.threshold);
                    QA_XLS_COL38_VALUE = string.Format(" > {0}", _maxReverseUpperModeSlopePFAnalysis.threshold);
                    QA_XLS_COL39_VALUE = string.Format(" > {0}", _middleLineRMSValuesPFAnalysis.threshold);
                    QA_XLS_COL40_VALUE = string.Format(" < {0}", _minMiddleLineSlopePFAnalysis.threshold);
                    QA_XLS_COL41_VALUE = string.Format(" > {0}", _maxMiddleLineSlopePFAnalysis.threshold);

                    sw.Write(QA_XLS_ROWTITLE5 + ",");
                    sw.Write(QA_XLS_COL2_VALUE + ",");
                    sw.Write(QA_XLS_COL3_VALUE + ",");
                    sw.Write(QA_XLS_COL4_VALUE + ",");
                    sw.Write(QA_XLS_COL5_VALUE + ",");
                    sw.Write(QA_XLS_COL6_VALUE + ",");
                    sw.Write(QA_XLS_COL7_VALUE + ",");
                    sw.Write(QA_XLS_COL8_VALUE + ",");
                    sw.Write(QA_XLS_COL9_VALUE + ",");
                    sw.Write(QA_XLS_COL10_VALUE + ",");
                    sw.Write(QA_XLS_COL11_VALUE + ",");
                    sw.Write(QA_XLS_COL12_VALUE + ",");
                    sw.Write(QA_XLS_COL13_VALUE + ",");
                    sw.Write(QA_XLS_COL14_VALUE + ",");
                    sw.Write(QA_XLS_COL15_VALUE + ",");
                    sw.Write(QA_XLS_COL16_VALUE + ",");
                    sw.Write(QA_XLS_COL17_VALUE + ",");
                    sw.Write(QA_XLS_COL18_VALUE + ",");
                    sw.Write(QA_XLS_COL19_VALUE + ",");
                    sw.Write(QA_XLS_COL20_VALUE + ",");
                    sw.Write(QA_XLS_COL21_VALUE + ",");
                    sw.Write(QA_XLS_COL22_VALUE + ",");
                    sw.Write(QA_XLS_COL23_VALUE + ",");
                    sw.Write(QA_XLS_COL24_VALUE + ",");
                    sw.Write(QA_XLS_COL25_VALUE + ",");
                    sw.Write(QA_XLS_COL26_VALUE + ",");
                    sw.Write(QA_XLS_COL27_VALUE + ",");
                    sw.Write(QA_XLS_COL28_VALUE + ",");
                    sw.Write(QA_XLS_COL29_VALUE + ",");
                    sw.Write(QA_XLS_COL30_VALUE + ",");
                    sw.Write(QA_XLS_COL31_VALUE + ",");
                    sw.Write(QA_XLS_COL32_VALUE + ",");
                    sw.Write(QA_XLS_COL33_VALUE + ",");
                    sw.Write(QA_XLS_COL34_VALUE + ",");
                    sw.Write(QA_XLS_COL35_VALUE + ",");
                    sw.Write(QA_XLS_COL36_VALUE + ",");
                    sw.Write(QA_XLS_COL37_VALUE + ",");
                    sw.Write(QA_XLS_COL38_VALUE + ",");
                    sw.Write(QA_XLS_COL39_VALUE + ",");
                    sw.Write(QA_XLS_COL40_VALUE + ",");
                    sw.Write(QA_XLS_COL41_VALUE);
                    sw.WriteLine("");
                    sw.WriteLine("");
                    // write index and its back
                    sw.WriteLine("Index");
                    // final rows - Modes
                    for (int i = 0; i < _numForwardModes; i++)
                    {
                        QA_XLS_COL1_VALUE = "";
                        QA_XLS_COL2_VALUE = "";
                        QA_XLS_COL3_VALUE = "";
                        QA_XLS_COL4_VALUE = "";
                        QA_XLS_COL5_VALUE = "";
                        QA_XLS_COL6_VALUE = "";
                        QA_XLS_COL7_VALUE = "";
                        QA_XLS_COL8_VALUE = "";
                        QA_XLS_COL9_VALUE = "";
                        QA_XLS_COL10_VALUE = "";
                        QA_XLS_COL11_VALUE = "";
                        QA_XLS_COL12_VALUE = "";
                        QA_XLS_COL13_VALUE = "";
                        QA_XLS_COL14_VALUE = "";
                        QA_XLS_COL15_VALUE = "";
                        QA_XLS_COL16_VALUE = "";
                        QA_XLS_COL17_VALUE = "";
                        QA_XLS_COL18_VALUE = "";
                        QA_XLS_COL19_VALUE = "";
                        QA_XLS_COL20_VALUE = "";
                        QA_XLS_COL21_VALUE = "";
                        QA_XLS_COL22_VALUE = "";
                        QA_XLS_COL23_VALUE = "";
                        QA_XLS_COL24_VALUE = "";
                        QA_XLS_COL25_VALUE = "";
                        QA_XLS_COL26_VALUE = "";
                        QA_XLS_COL27_VALUE = "";
                        QA_XLS_COL28_VALUE = "";
                        QA_XLS_COL29_VALUE = "";
                        QA_XLS_COL30_VALUE = "";
                        QA_XLS_COL31_VALUE = "";
                        QA_XLS_COL32_VALUE = "";
                        QA_XLS_COL33_VALUE = "";
                        QA_XLS_COL34_VALUE = "";
                        QA_XLS_COL35_VALUE = "";
                        QA_XLS_COL36_VALUE = "";
                        QA_XLS_COL37_VALUE = "";
                        QA_XLS_COL38_VALUE = "";
                        QA_XLS_COL39_VALUE = "";
                        QA_XLS_COL40_VALUE = "";
                        QA_XLS_COL41_VALUE = "";
                        // Index location
                        QA_XLS_COL1_VALUE = string.Format(" {0}", i);
                        // Max Forward Mode Width
                        // Predicate<double> match = delegate(double obj)
                        //{
                        //    if (Math.Abs(obj - i) < 1e-6)
                        //    {
                        //        return true;
                        //    }
                        //    else return false;
                        //};

                        int index = _maxForwardModeWidthPFAnalysis.failedModes.IndexOf(i);

                        if (i < _maxForwardModeWidths.Count && index != -1)
                            QA_XLS_COL5_VALUE = string.Format("{0}", _maxForwardModeWidths[i]);

                        // Max Reverse Mode Width
                        index = _maxReverseModeWidthPFAnalysis.failedModes.IndexOf(i);
                        if (i < _maxReverseModeWidths.Count && index != -1)
                            QA_XLS_COL6_VALUE = string.Format("{0}", _maxReverseModeWidths[i]);

                        // Mean Percentage Working Region
                        index = _meanPercWorkingRegionPFAnalysis.failedModes.IndexOf(i);
                        if (i < _meanPercWorkingRegions.Count && index != -1)
                            QA_XLS_COL7_VALUE = string.Format("{0}", _meanPercWorkingRegions[i]);

                        // Min Percentage Working 
                        index = _minPercWorkingRegionPFAnalysis.failedModes.IndexOf(i);
                        if (i < _minPercWorkingRegions.Count && index != -1)
                            QA_XLS_COL8_VALUE = string.Format("{0}", _minPercWorkingRegions[i]);

                        // Min Forward Mode Width
                        index = _minForwardModeWidthPFAnalysis.failedModes.IndexOf(i);
                        if (i < _minForwardModeWidths.Count && index != -1)
                            QA_XLS_COL9_VALUE = string.Format("{0}", _minForwardModeWidths[i]);

                        // Min Reverse Mode Width                        
                        index = _minReverseModeWidthPFAnalysis.failedModes.IndexOf(i);
                        if (i < _minReverseModeWidths.Count && index != -1)
                            QA_XLS_COL10_VALUE = string.Format("{0}", _minReverseModeWidths[i]);


                        // forward lower modal_distortion
                        index = _forwardLowerModalDistortionAnglePFAnalysis.failedModes.IndexOf(i);
                        if (i < _forwardLowerModalDistortionAngles.Count
                        && i < _forwardLowerModalDistortionAngleXPositions.Count && index != -1)
                        {
                            QA_XLS_COL11_VALUE = string.Format("{0}", _forwardLowerModalDistortionAngles[i]);
                            QA_XLS_COL12_VALUE = string.Format("{0}", _forwardLowerModalDistortionAngleXPositions[i]);
                        }

                        // forward upper modal_distortion
                        index = _forwardUpperModalDistortionAnglePFAnalysis.failedModes.IndexOf(i);
                        if (i < _forwardUpperModalDistortionAngles.Count
                        && i < _forwardUpperModalDistortionAngleXPositions.Count && index != -1)
                        {
                            QA_XLS_COL13_VALUE = string.Format("{0}", _forwardUpperModalDistortionAngles[i]);
                            QA_XLS_COL14_VALUE = string.Format("{0}", _forwardUpperModalDistortionAngleXPositions[i]);
                        }

                        // reverse lower modal_distortion
                        index = _reverseLowerModalDistortionAnglePFAnalysis.failedModes.IndexOf(i);
                        if (i < _reverseLowerModalDistortionAngles.Count
                        && i < _reverseLowerModalDistortionAngleXPositions.Count && index != -1)
                        {
                            QA_XLS_COL15_VALUE = string.Format("{0}", _reverseLowerModalDistortionAngles[i]);
                            QA_XLS_COL16_VALUE = string.Format("{0}", _reverseLowerModalDistortionAngleXPositions[i]);
                        }

                        // reverse upper modal_distortion
                        index = _reverseUpperModalDistortionAnglePFAnalysis.failedModes.IndexOf(i);
                        if (i < _reverseUpperModalDistortionAngles.Count
                        && i < _reverseUpperModalDistortionAngleXPositions.Count
                        && index != -1)
                        {
                            QA_XLS_COL17_VALUE = string.Format("{0}", _reverseUpperModalDistortionAngles[i]);
                            QA_XLS_COL18_VALUE = string.Format("{0}", _reverseUpperModalDistortionAngleXPositions[i]);
                        }

                        // Lower Max Forward Bdc X-Length
                        index = _maxForwardLowerModeBDCAreaXLengthPFAnalysis.failedModes.IndexOf(i);
                        if (i < _maxForwardLowerModeBDCAreaXLengths.Count && index != -1)
                            QA_XLS_COL19_VALUE = string.Format("{0}", _maxForwardLowerModeBDCAreaXLengths[i]);

                        // Upper Max Forward Bdc X-Length                
                        index = _maxForwardUpperModeBDCAreaXLengthPFAnalysis.failedModes.IndexOf(i);
                        if (i < _maxForwardUpperModeBDCAreaXLengths.Count && index != -1)
                            QA_XLS_COL20_VALUE = string.Format("{0}", _maxForwardUpperModeBDCAreaXLengths[i]);

                        // Lower Max Forward Bdc Area                 
                        index = _maxForwardLowerModeBDCAreaPFAnalysis.failedModes.IndexOf(i);
                        if (i < _maxForwardLowerModeBDCAreas.Count && index != -1)
                            QA_XLS_COL21_VALUE = string.Format("{0}", _maxForwardLowerModeBDCAreas[i]);

                        // Upper Max Forward Bdc Area 
                        index = _maxForwardUpperModeBDCAreaPFAnalysis.failedModes.IndexOf(i);
                        if (i < _maxForwardUpperModeBDCAreas.Count && index != -1)
                            QA_XLS_COL22_VALUE = string.Format("{0}", _maxForwardUpperModeBDCAreas[i]);

                        // Lower Sum of Forward Bdc Areas 
                        index = _sumForwardLowerModeBDCAreasPFAnalysis.failedModes.IndexOf(i);
                        if (i < _sumForwardLowerModeBDCAreas.Count && index != -1)
                            QA_XLS_COL23_VALUE = string.Format("{0}", _sumForwardLowerModeBDCAreas[i]);

                        // Upper Sum of Forward Bdc Areas
                        index = _sumForwardUpperModeBDCAreasPFAnalysis.failedModes.IndexOf(i);
                        if (i < _sumForwardUpperModeBDCAreas.Count && index != -1)
                            QA_XLS_COL24_VALUE = string.Format("{0}", _sumForwardUpperModeBDCAreas[i]);

                        // Lower Number of Forward Bdcs          
                        index = _numForwardLowerModeBDCAreasPFAnalysis.failedModes.IndexOf(i);
                        if (i < _numForwardLowerModeBDCAreaSamples.Count && index != -1)
                            QA_XLS_COL25_VALUE = string.Format("{0}", _numForwardLowerModeBDCAreaSamples[i]);

                        // Upper Number of Forward Bdcs               
                        index = _numForwardUpperModeBDCAreasPFAnalysis.failedModes.IndexOf(i);
                        if (i < _numForwardUpperModeBDCAreaSamples.Count && index != -1)
                            QA_XLS_COL26_VALUE = string.Format("{0}", _numForwardUpperModeBDCAreaSamples[i]);

                        // Lower Max Reverse Bdc X-Length           
                        index = _maxReverseLowerModeBDCAreaXLengthPFAnalysis.failedModes.IndexOf(i);
                        if (i < _maxReverseLowerModeBDCAreaXLengths.Count && index != -1)
                            QA_XLS_COL27_VALUE = string.Format("{0}", _maxReverseLowerModeBDCAreaXLengths[i]);

                        // Upper Max Reverse Bdc X-Length          
                        index = _maxReverseUpperModeBDCAreaXLengthPFAnalysis.failedModes.IndexOf(i);
                        if (i < _maxReverseUpperModeBDCAreaXLengths.Count && index != -1)
                            QA_XLS_COL28_VALUE = string.Format("{0}", _maxReverseUpperModeBDCAreaXLengths[i]);

                        // Lower Max Reverse Bdc Area          
                        index = _maxReverseLowerModeBDCAreaPFAnalysis.failedModes.IndexOf(i);
                        if (i < _maxReverseLowerModeBDCAreas.Count && index != -1)
                            QA_XLS_COL29_VALUE = string.Format("{0}", _maxReverseLowerModeBDCAreas[i]);

                        // Upper Max Reverse Bdc Area          
                        index = _maxReverseUpperModeBDCAreaPFAnalysis.failedModes.IndexOf(i);
                        if (i < _maxReverseUpperModeBDCAreas.Count && index != -1)
                            QA_XLS_COL30_VALUE = string.Format("{0}", _maxReverseUpperModeBDCAreas[i]);

                        // Lower Sum of Reverse Bdc Areas            
                        index = _sumReverseLowerModeBDCAreasPFAnalysis.failedModes.IndexOf(i);
                        if (i < _sumReverseLowerModeBDCAreas.Count && index != -1)
                            QA_XLS_COL31_VALUE = string.Format("{0}", _sumReverseLowerModeBDCAreas[i]);

                        // Upper Sum of Reverse Bdc Areas              
                        index = _sumReverseUpperModeBDCAreasPFAnalysis.failedModes.IndexOf(i);
                        if (i < _sumReverseUpperModeBDCAreas.Count && index != -1)
                            QA_XLS_COL32_VALUE = string.Format("{0}", _sumReverseUpperModeBDCAreas[i]);

                        // Lower Number of Reverse Bdcs                  
                        index = _numReverseLowerModeBDCAreasPFAnalysis.failedModes.IndexOf(i);
                        if (i < _numReverseLowerModeBDCAreaSamples.Count && index != -1)
                            QA_XLS_COL33_VALUE = string.Format("{0}", _numReverseLowerModeBDCAreaSamples[i]);

                        // Upper Number of Reverse Bdcs            
                        index = _numReverseUpperModeBDCAreasPFAnalysis.failedModes.IndexOf(i);
                        if (i < _numReverseUpperModeBDCAreaSamples.Count && index != -1)
                            QA_XLS_COL34_VALUE = string.Format("{0}", _numReverseUpperModeBDCAreaSamples[i]);

                        // Max Forward Lower Slope                
                        index = _maxForwardLowerModeSlopePFAnalysis.failedModes.IndexOf(i);
                        if (i < _maxForwardLowerModeSlopes.Count && index != -1)
                            QA_XLS_COL35_VALUE = string.Format("{0}", _maxForwardLowerModeSlopes[i]);

                        // Max Forward Upper Slope                  
                        index = _maxForwardUpperModeSlopePFAnalysis.failedModes.IndexOf(i);
                        if (i < _maxForwardUpperModeSlopes.Count && index != -1)
                            QA_XLS_COL36_VALUE = string.Format("{0}", _maxForwardUpperModeSlopes[i]);

                        // Max Reverse Lower Slope                    
                        index = _maxReverseLowerModeSlopePFAnalysis.failedModes.IndexOf(i);
                        if (i < _maxReverseLowerModeSlopes.Count && index != -1)
                            QA_XLS_COL37_VALUE = string.Format("{0}", _maxReverseLowerModeSlopes[i]);

                        // Max Reverse Upper Slope                       
                        index = _maxReverseUpperModeSlopePFAnalysis.failedModes.IndexOf(i);
                        if (i < _maxReverseUpperModeSlopes.Count && index != -1)
                            QA_XLS_COL38_VALUE = string.Format("{0}", _maxReverseUpperModeSlopes[i]);

                        // Max Middle Line RMS Value                  
                        index = _middleLineRMSValuesPFAnalysis.failedModes.IndexOf(i);
                        if (i < _middleLineRMSValues.Count && index != -1)
                            QA_XLS_COL39_VALUE = string.Format("{0}", _middleLineRMSValues[i]);

                        // Min Middle Line Slope                       
                        index = _minMiddleLineSlopePFAnalysis.failedModes.IndexOf(i);
                        if (i < _middleLineSlopes.Count && index != -1)
                            QA_XLS_COL40_VALUE = string.Format("{0}", _middleLineSlopes[i]);

                        // Max Middle Line Slope
                        index = _maxMiddleLineSlopePFAnalysis.failedModes.IndexOf(i);
                        if (i < _middleLineSlopes.Count && index != -1)
                            QA_XLS_COL41_VALUE = string.Format("{0}", _middleLineSlopes[i]);

                        sw.Write(QA_XLS_COL1_VALUE + ",");
                        sw.Write(QA_XLS_COL2_VALUE + ",");
                        sw.Write(QA_XLS_COL3_VALUE + ",");
                        sw.Write(QA_XLS_COL4_VALUE + ",");
                        sw.Write(QA_XLS_COL5_VALUE + ",");
                        sw.Write(QA_XLS_COL6_VALUE + ",");
                        sw.Write(QA_XLS_COL7_VALUE + ",");
                        sw.Write(QA_XLS_COL8_VALUE + ",");
                        sw.Write(QA_XLS_COL9_VALUE + ",");
                        sw.Write(QA_XLS_COL10_VALUE + ",");
                        sw.Write(QA_XLS_COL11_VALUE + ",");
                        sw.Write(QA_XLS_COL12_VALUE + ",");
                        sw.Write(QA_XLS_COL13_VALUE + ",");
                        sw.Write(QA_XLS_COL14_VALUE + ",");
                        sw.Write(QA_XLS_COL15_VALUE + ",");
                        sw.Write(QA_XLS_COL16_VALUE + ",");
                        sw.Write(QA_XLS_COL17_VALUE + ",");
                        sw.Write(QA_XLS_COL18_VALUE + ",");
                        sw.Write(QA_XLS_COL19_VALUE + ",");
                        sw.Write(QA_XLS_COL20_VALUE + ",");
                        sw.Write(QA_XLS_COL21_VALUE + ",");
                        sw.Write(QA_XLS_COL22_VALUE + ",");
                        sw.Write(QA_XLS_COL23_VALUE + ",");
                        sw.Write(QA_XLS_COL24_VALUE + ",");
                        sw.Write(QA_XLS_COL25_VALUE + ",");
                        sw.Write(QA_XLS_COL26_VALUE + ",");
                        sw.Write(QA_XLS_COL27_VALUE + ",");
                        sw.Write(QA_XLS_COL28_VALUE + ",");
                        sw.Write(QA_XLS_COL29_VALUE + ",");
                        sw.Write(QA_XLS_COL30_VALUE + ",");
                        sw.Write(QA_XLS_COL31_VALUE + ",");
                        sw.Write(QA_XLS_COL32_VALUE + ",");
                        sw.Write(QA_XLS_COL33_VALUE + ",");
                        sw.Write(QA_XLS_COL34_VALUE + ",");
                        sw.Write(QA_XLS_COL35_VALUE + ",");
                        sw.Write(QA_XLS_COL36_VALUE + ",");
                        sw.Write(QA_XLS_COL37_VALUE + ",");
                        sw.Write(QA_XLS_COL38_VALUE + ",");
                        sw.Write(QA_XLS_COL39_VALUE + ",");
                        sw.Write(QA_XLS_COL40_VALUE + ",");
                        sw.Write(QA_XLS_COL41_VALUE);
                        sw.WriteLine();
                    }
                }
                else // qaWasRun == false
                {
                    sw.WriteLine("Error : Function called before Pass Fail Analysis completed.");
                }
                sw.Flush();
            }

        }
        public void getQAError(int qaError, ref int errorNumber, ref string errorMsg)
        {
            errorMsg = "";
            errorNumber = 0;
            if (Convert.ToBoolean(qaError & RESULTS_GENERAL_ERROR))
            {
                errorMsg += ("Error(s) detected. ");
                errorNumber |= RESULTS_GENERAL_ERROR;
            }
            if (Convert.ToBoolean(qaError & RESULTS_NO_MODEMAP_DATA))
            {
                errorMsg += ("Mode map data not available. ");
                errorNumber |= RESULTS_NO_MODEMAP_DATA;
            }
            if (Convert.ToBoolean(qaError & RESULTS_PO_TOO_LOW))
            {
                errorMsg += ("Invalid laser screening run - optical power is too low. ");
                errorNumber |= RESULTS_PO_TOO_LOW;
            }
            if (Convert.ToBoolean(qaError & RESULTS_PO_TOO_HIGH))
            {
                errorMsg += ("Invalid laser screening run - optical power is too high. ");
                errorNumber |= RESULTS_PO_TOO_HIGH;
            }
            if (Convert.ToBoolean(qaError & RESULTS_TOO_MANY_JUMPS))
            {
                errorMsg += ("Invalid laser screening run - too many laser mode jumps found. ");
                errorNumber |= RESULTS_TOO_MANY_JUMPS;
            }
            if (Convert.ToBoolean(qaError & RESULTS_TOO_FEW_JUMPS))
            {
                errorMsg += ("Invalid laser screening run - too few laser mode jumps found. ");
                errorNumber |= RESULTS_TOO_FEW_JUMPS;
            }
            if (Convert.ToBoolean(qaError & RESULTS_INVALID_NUMBER_OF_LINES))
            {
                errorMsg += ("Invalid laser screening run - invalid number of mode boundaries detected. ");
                errorNumber |= RESULTS_INVALID_NUMBER_OF_LINES;
            }
            if (Convert.ToBoolean(qaError & RESULTS_INVALID_LINES_REMOVED))
            {
                errorMsg += ("Invalid mode boundaries detected. ");
                errorNumber |= RESULTS_INVALID_LINES_REMOVED;
            }
            if (Convert.ToBoolean(qaError & RESULTS_FORWARD_LINES_MISSING))
            {
                errorMsg += ("Undetected forward mode boundaries. ");
                errorNumber |= RESULTS_FORWARD_LINES_MISSING;
            }
            if (Convert.ToBoolean(qaError & RESULTS_REVERSE_LINES_MISSING))
            {
                errorMsg += ("Undetected reverse mode boundaries. ");
                errorNumber |= RESULTS_REVERSE_LINES_MISSING;
            }

            string errNumMsg = "";
            if (errorNumber > RESULTS_NO_ERROR)
                errNumMsg += string.Format("%d,", errorNumber);
            else
                errNumMsg += ",";
            errNumMsg += errorMsg;


            errorMsg += errNumMsg;
        }
        //
        public bool _screeningPassed;

        public int _qaDetectedError;
        #endregion



        double _overallPercStableArea;

        //
        ///
        ///////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////
        ///

        public void getStableAreaWidths()
        {
            if (_stableAreaWidths.Count == 0)
            {
                for (int i = 0; i < (int)_hysteresisAnalysis.Count; i++)
                {
                    Vector stableAreaWidths;
                    _hysteresisAnalysis[i].getStableAreaWidths(out stableAreaWidths);

                    _stableAreaWidths.Add(stableAreaWidths);
                }
            }
        }

        public void getMaxStableAreaWidths()
        {
            if (_maxStableAreaWidths.Count == 0)
            {
                for (int i = 0; i < (int)_hysteresisAnalysis.Count; i++)
                    _maxStableAreaWidths.Add(_hysteresisAnalysis[i].getStableAreaWidthsStatistic(VectorAttribute.max));
            }
        }

        public void getMinStableAreaWidths()
        {
            if (_minStableAreaWidths.Count == 0)
            {
                for (int i = 0; i < (int)_hysteresisAnalysis.Count; i++)
                    _minStableAreaWidths.Add(_hysteresisAnalysis[i].getStableAreaWidthsStatistic(VectorAttribute.min));
            }
        }

        public void getMeanStableAreaWidths()
        {
            if (_meanStableAreaWidths.Count == 0)
            {
                for (int i = 0; i < (int)_hysteresisAnalysis.Count; i++)
                    _meanStableAreaWidths.Add(_hysteresisAnalysis[i].getStableAreaWidthsStatistic(VectorAttribute.mean));
            }
        }

        public void getSumStableAreaWidths()
        {
            if (_sumStableAreaWidths.Count == 0)
            {
                for (int i = 0; i < (int)_hysteresisAnalysis.Count; i++)
                    _sumStableAreaWidths.Add(_hysteresisAnalysis[i].getStableAreaWidthsStatistic(VectorAttribute.sum));
            }
        }

        public void getVarStableAreaWidths()
        {
            if (_varStableAreaWidths.Count == 0)
            {
                for (int i = 0; i < (int)_hysteresisAnalysis.Count; i++)
                    _varStableAreaWidths.Add(_hysteresisAnalysis[i].getStableAreaWidthsStatistic(VectorAttribute.variance));
            }
        }

        public void getStdDevStableAreaWidths()
        {
            if (_stdDevStableAreaWidths.Count == 0)
            {
                for (int i = 0; i < (int)_hysteresisAnalysis.Count; i++)
                    _stdDevStableAreaWidths.Add(_hysteresisAnalysis[i].getStableAreaWidthsStatistic(VectorAttribute.stdDev));
            }
        }

        public void getMedianStableAreaWidths()
        {
            if (_medianStableAreaWidths.Count == 0)
            {
                for (int i = 0; i < (int)_hysteresisAnalysis.Count; i++)
                    _medianStableAreaWidths.Add(_hysteresisAnalysis[i].getStableAreaWidthsStatistic(VectorAttribute.median));
            }
        }

        public void getNumStableAreaWidthSamples()
        {
            if (_numStableAreaWidthSamples.Count == 0)
            {
                for (int i = 0; i < (int)_hysteresisAnalysis.Count; i++)
                    _numStableAreaWidthSamples.Add(_hysteresisAnalysis[i].getStableAreaWidthsStatistic(VectorAttribute.count));
            }
        }




        //
        ///////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////

        public void getPercWorkingRegions()
        {
            if (_percWorkingRegions.Count == 0)
            {
                for (int i = 0; i < (int)_hysteresisAnalysis.Count; i++)
                {
                    Vector percWorkingRegions;
                    _hysteresisAnalysis[i].getPercWorkingRegions(out percWorkingRegions);

                    _percWorkingRegions.Add(percWorkingRegions);
                }
            }
        }

        public void
        getMaxPercWorkingRegions()
        {
            if (_maxPercWorkingRegions.Count == 0)
            {
                for (int i = 0; i < (int)_hysteresisAnalysis.Count; i++)
                    _maxPercWorkingRegions.Add(_hysteresisAnalysis[i].getPercWorkingRegionsStatistic(VectorAttribute.max));
            }
        }

        public void
        getMinPercWorkingRegions()
        {
            if (_minPercWorkingRegions.Count == 0)
            {
                for (int i = 0; i < (int)_hysteresisAnalysis.Count; i++)
                    _minPercWorkingRegions.Add(_hysteresisAnalysis[i].getPercWorkingRegionsStatistic(VectorAttribute.min));
            }
        }

        public void
        getMeanPercWorkingRegions()
        {
            if (_meanPercWorkingRegions.Count == 0)
            {
                for (int i = 0; i < (int)_hysteresisAnalysis.Count; i++)
                    _meanPercWorkingRegions.Add(_hysteresisAnalysis[i].getPercWorkingRegionsStatistic(VectorAttribute.mean));
            }
        }

        public void
        getSumPercWorkingRegions()
        {
            if (_sumPercWorkingRegions.Count == 0)
            {
                for (int i = 0; i < (int)_hysteresisAnalysis.Count; i++)
                    _sumPercWorkingRegions.Add(_hysteresisAnalysis[i].getPercWorkingRegionsStatistic(VectorAttribute.sum));
            }
        }

        public void
        getVarPercWorkingRegions()
        {
            if (_varPercWorkingRegions.Count == 0)
            {
                for (int i = 0; i < (int)_hysteresisAnalysis.Count; i++)
                    _varPercWorkingRegions.Add(_hysteresisAnalysis[i].getPercWorkingRegionsStatistic(VectorAttribute.variance));
            }
        }

        public void
        getStdDevPercWorkingRegions()
        {
            if (_stdDevPercWorkingRegions.Count == 0)
            {
                for (int i = 0; i < (int)_hysteresisAnalysis.Count; i++)
                    _stdDevPercWorkingRegions.Add(_hysteresisAnalysis[i].getPercWorkingRegionsStatistic(VectorAttribute.stdDev));
            }
        }

        public void
        getMedianPercWorkingRegions()
        {
            if (_medianPercWorkingRegions.Count == 0)
            {
                for (int i = 0; i < (int)_hysteresisAnalysis.Count; i++)
                    _medianPercWorkingRegions.Add(_hysteresisAnalysis[i].getPercWorkingRegionsStatistic(VectorAttribute.median));
            }
        }

        public void
        getNumPercWorkingRegionSamples()
        {
            if (_numPercWorkingRegionSamples.Count == 0)
            {
                for (int i = 0; i < (int)_hysteresisAnalysis.Count; i++)
                    _numPercWorkingRegionSamples.Add(_hysteresisAnalysis[i].getPercWorkingRegionsStatistic(VectorAttribute.count));
            }
        }





        void getMiddleLineRMSValues()
        {
            /////////////////////////////////////////////////////////////////////
            /// Use VectorAnalysis to get RMS value of this supermodes middle line
            //
            if (_middleLineRMSValues.Count == 0)
            {
                for (int i = 0; i < (int)_middleLines.Count; i++)
                {
                    double linearFitLineSlope = 0;
                    double linearFitLineIntercept = 0;

                    // find linear fit line
                    VectorAnalysis.linearFit(_middleLines[i].xPoints(),
                                            _middleLines[i].yPoints(),
                                            ref linearFitLineSlope,
                                            ref linearFitLineIntercept);

                    // find distances from points to linear fit line
                    List<double> distances = new List<double>();
                    VectorAnalysis.distancesFromPointsToLine(_middleLines[i].xPoints(),
                                                            _middleLines[i].yPoints(),
                                                            ref linearFitLineSlope,
                                                            ref linearFitLineIntercept,
                                                            distances);

                    // get mean of distances
                    double meanDistance = 0;
                    meanDistance = VectorAnalysis.meanOfValues(distances);

                    _middleLineSlopes.Add(linearFitLineSlope);

                    _middleLineRMSValues.Add(Math.Sqrt(Math.Pow(meanDistance, 2)));
                }
            }

        }

        void
        getMiddleLineSlopes()
        {
            if (_middleLineSlopes.Count == 0)
            {
                for (int i = 0; i < (int)_middleLines.Count; i++)
                {
                    double linearFitLineSlope = 0;
                    double linearFitLineIntercept = 0;

                    // find linear fit line
                    VectorAnalysis.linearFit(_middleLines[i].xPoints(),
                                            _middleLines[i].yPoints(),
                                            ref linearFitLineSlope,
                                            ref linearFitLineIntercept);

                    _middleLineSlopes.Add(linearFitLineSlope);
                }
            }
        }
        //
        /////////////////////////////////////////////////////////////////////////////
        ///
        //	Continuity analysis
        //
        public void getMaxContinuityValue()
        {
            if (_maxContinuityValue == 0)
                _maxContinuityValue = _continuityValues.getVectorAttribute(VectorAttribute.max);
        }

        public void getMinContinuityValue()
        {
            if (_minContinuityValue == 0)
                _minContinuityValue = _continuityValues.getVectorAttribute(VectorAttribute.min);
        }

        public void getMeanContinuityValues()
        {
            if (_meanContinuityValues == 0)
                _meanContinuityValues = _continuityValues.getVectorAttribute(VectorAttribute.mean);
        }

        public void getSumContinuityValues()
        {
            if (_sumContinuityValues == 0)
                _sumContinuityValues = _continuityValues.getVectorAttribute(VectorAttribute.sum);
        }

        public void getVarContinuityValues()
        {
            if (_varContinuityValues == 0)
                _varContinuityValues = _continuityValues.getVectorAttribute(VectorAttribute.variance);
        }

        public void getStdDevContinuityValues()
        {
            if (_stdDevContinuityValues == 0)
                _stdDevContinuityValues = _continuityValues.getVectorAttribute(VectorAttribute.stdDev);
        }

        public void getMedianContinuityValues()
        {
            if (_medianContinuityValues == 0)
                _medianContinuityValues = _continuityValues.getVectorAttribute(VectorAttribute.median);
        }

        public void getNumContinuityValues()
        {
            if (_numContinuityValues == 0)
                _numContinuityValues = _continuityValues.getVectorAttribute(VectorAttribute.count);
        }

        public void getMaxContinuityValueSpacing()
        {
            if (_maxPrGap == 0)
            {
                _maxPrGap = _continuityValues.getVectorAttribute(VectorAttribute.maxGap);
            }
        }

        public void getForwardModeWidths()
        {
            if (_forwardModeWidths.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    Vector forwardModeWidths;
                    _forwardModes[i].getModeWidths(out forwardModeWidths);

                    _forwardModeWidths.Add(forwardModeWidths);
                }
            }
        }

        public void getMaxForwardModeWidths()
        {
            if (_maxForwardModeWidths.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                    _maxForwardModeWidths.Add(_forwardModes[i].getModeWidthsStatistic(VectorAttribute.max));
            }
        }
        public void getMinForwardModeWidths()
        {
            if (_minForwardModeWidths.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                    _minForwardModeWidths.Add(_forwardModes[i].getModeWidthsStatistic(VectorAttribute.min));
            }
        }
        public void getMeanForwardModeWidths()
        {
            if (_meanForwardModeWidths.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                    _meanForwardModeWidths.Add(_forwardModes[i].getModeWidthsStatistic(VectorAttribute.mean));
            }
        }
        public void getSumForwardModeWidths()
        {
            if (_sumForwardModeWidths.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                    _sumForwardModeWidths.Add(_forwardModes[i].getModeWidthsStatistic(VectorAttribute.sum));
            }
        }


        public void getVarForwardModeWidths()
        {
            if (_varForwardModeWidths.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                    _varForwardModeWidths.Add(_forwardModes[i].getModeWidthsStatistic(VectorAttribute.variance));
            }
        }


        public void getStdDevForwardModeWidths()
        {
            if (_stdDevForwardModeWidths.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                    _stdDevForwardModeWidths.Add(_forwardModes[i].getModeWidthsStatistic(VectorAttribute.stdDev));
            }
        }
        public void getMedianForwardModeWidths()
        {
            if (_medianForwardModeWidths.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                    _medianForwardModeWidths.Add(_forwardModes[i].getModeWidthsStatistic(VectorAttribute.median));
            }
        }
        public void getNumForwardModeWidthSamples()
        {
            if (_numForwardModeWidthSamples.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                    _numForwardModeWidthSamples.Add(_forwardModes[i].getModeWidthsStatistic(VectorAttribute.count));
            }
        }

        //
        /////////////////////////////////////////////////////////////////////////////
        ///
        //	Forward Mode Slopes
        //
        public void

        getForwardModeSlopes()
        {
            if (_forwardLowerModeSlopes.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    Vector forwardLowerModeSlopes;
                    Vector forwardUpperModeSlopes;
                    _forwardModes[i].getModeSlopes(out forwardLowerModeSlopes, out forwardUpperModeSlopes);

                    _forwardLowerModeSlopes.Add(forwardLowerModeSlopes);
                    _forwardUpperModeSlopes.Add(forwardUpperModeSlopes);
                }
            }
        }

        public void

        getMaxForwardModeSlopes()
        {
            if (_maxForwardLowerModeSlopes.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _maxForwardLowerModeSlopes.Add(_forwardModes[i].getModeLowerSlopesStatistic(VectorAttribute.max));
                    _maxForwardUpperModeSlopes.Add(_forwardModes[i].getModeUpperSlopesStatistic(VectorAttribute.max));
                }
            }
        }
        public void

        getMinForwardModeSlopes()
        {
            if (_minForwardLowerModeSlopes.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _minForwardLowerModeSlopes.Add(_forwardModes[i].getModeLowerSlopesStatistic(VectorAttribute.min));
                    _minForwardUpperModeSlopes.Add(_forwardModes[i].getModeUpperSlopesStatistic(VectorAttribute.min));
                }
            }
        }
        public void

        getMeanForwardModeSlopes()
        {
            if (_meanForwardLowerModeSlopes.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _meanForwardLowerModeSlopes.Add(_forwardModes[i].getModeLowerSlopesStatistic(VectorAttribute.mean));
                    _meanForwardUpperModeSlopes.Add(_forwardModes[i].getModeUpperSlopesStatistic(VectorAttribute.mean));
                }
            }
        }
        public void

        getSumForwardModeSlopes()
        {
            if (_sumForwardLowerModeSlopes.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _sumForwardLowerModeSlopes.Add(_forwardModes[i].getModeLowerSlopesStatistic(VectorAttribute.sum));
                    _sumForwardUpperModeSlopes.Add(_forwardModes[i].getModeUpperSlopesStatistic(VectorAttribute.sum));
                }
            }
        }
        public void

        getVarForwardModeSlopes()
        {
            if (_varForwardLowerModeSlopes.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _varForwardLowerModeSlopes.Add(_forwardModes[i].getModeLowerSlopesStatistic(VectorAttribute.variance));
                    _varForwardUpperModeSlopes.Add(_forwardModes[i].getModeUpperSlopesStatistic(VectorAttribute.variance));
                }
            }
        }
        public void

        getStdDevForwardModeSlopes()
        {
            if (_stdDevForwardLowerModeSlopes.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _stdDevForwardLowerModeSlopes.Add(_forwardModes[i].getModeLowerSlopesStatistic(VectorAttribute.stdDev));
                    _stdDevForwardUpperModeSlopes.Add(_forwardModes[i].getModeUpperSlopesStatistic(VectorAttribute.stdDev));
                }
            }
        }
        public void

        getMedianForwardModeSlopes()
        {
            if (_medianForwardLowerModeSlopes.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _medianForwardLowerModeSlopes.Add(_forwardModes[i].getModeLowerSlopesStatistic(VectorAttribute.median));
                    _medianForwardUpperModeSlopes.Add(_forwardModes[i].getModeUpperSlopesStatistic(VectorAttribute.median));
                }
            }
        }
        public void

        getNumForwardModeSlopeSamples()
        {
            if (_numForwardLowerModeSlopeSamples.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _numForwardLowerModeSlopeSamples.Add(_forwardModes[i].getModeLowerSlopesStatistic(VectorAttribute.count));
                    _numForwardUpperModeSlopeSamples.Add(_forwardModes[i].getModeUpperSlopesStatistic(VectorAttribute.count));
                }
            }
        }
        public void getForwardModalDistortionAngles()
        {
            if (_forwardLowerModalDistortionAngles.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _forwardLowerModalDistortionAngles.Add(_forwardModes[i].getLowerModeModalDistortionAngle());
                    _forwardUpperModalDistortionAngles.Add(_forwardModes[i].getUpperModeModalDistortionAngle());
                }
            }
        }
        public void getForwardModalDistortionAngleXPositions()
        {
            if (_forwardLowerModalDistortionAngleXPositions.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _forwardLowerModalDistortionAngleXPositions.Add(_forwardModes[i].getLowerModeMaxModalDistortionAngleXPosition());
                    _forwardUpperModalDistortionAngleXPositions.Add(_forwardModes[i].getUpperModeMaxModalDistortionAngleXPosition());
                }
            }
        }
        /////////////////////////////////////////////////////////////////////////////
        ///
        //	Mode Boundary Direction Change Area measurements
        //
        public void getForwardModeBDCAreas()
        {
            if (_forwardLowerModeBDCAreas.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    Vector forwardLowerModeBDCAreas;
                    Vector forwardUpperModeBDCAreas;
                    _forwardModes[i].getModeBDCAreas(out forwardLowerModeBDCAreas, out forwardUpperModeBDCAreas);

                    _forwardLowerModeBDCAreas.Add(forwardLowerModeBDCAreas);
                    _forwardUpperModeBDCAreas.Add(forwardUpperModeBDCAreas);
                }
            }
        }

        public void getMaxForwardModeBDCAreas()
        {
            if (_maxForwardLowerModeBDCAreas.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _maxForwardLowerModeBDCAreas.Add(_forwardModes[i].getLowerModeBDCAreasStatistic(VectorAttribute.max));
                    _maxForwardUpperModeBDCAreas.Add(_forwardModes[i].getUpperModeBDCAreasStatistic(VectorAttribute.max));
                }
            }
        }
        public void getMinForwardModeBDCAreas()
        {
            if (_minForwardLowerModeBDCAreas.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _minForwardLowerModeBDCAreas.Add(_forwardModes[i].getLowerModeBDCAreasStatistic(VectorAttribute.min));
                    _minForwardUpperModeBDCAreas.Add(_forwardModes[i].getUpperModeBDCAreasStatistic(VectorAttribute.min));
                }
            }
        }
        public void getMeanForwardModeBDCAreas()
        {
            if (_meanForwardLowerModeBDCAreas.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _meanForwardLowerModeBDCAreas.Add(_forwardModes[i].getLowerModeBDCAreasStatistic(VectorAttribute.mean));
                    _meanForwardUpperModeBDCAreas.Add(_forwardModes[i].getUpperModeBDCAreasStatistic(VectorAttribute.mean));
                }
            }
        }
        public void getSumForwardModeBDCAreas()
        {
            if (_sumForwardLowerModeBDCAreas.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _sumForwardLowerModeBDCAreas.Add(_forwardModes[i].getLowerModeBDCAreasStatistic(VectorAttribute.sum));
                    _sumForwardUpperModeBDCAreas.Add(_forwardModes[i].getUpperModeBDCAreasStatistic(VectorAttribute.sum));
                }
            }
        }
        public void getVarForwardModeBDCAreas()
        {
            if (_varForwardLowerModeBDCAreas.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _varForwardLowerModeBDCAreas.Add(_forwardModes[i].getLowerModeBDCAreasStatistic(VectorAttribute.variance));
                    _varForwardUpperModeBDCAreas.Add(_forwardModes[i].getUpperModeBDCAreasStatistic(VectorAttribute.variance));
                }
            }
        }
        public void getStdDevForwardModeBDCAreas()
        {
            if (_stdDevForwardLowerModeBDCAreas.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _stdDevForwardLowerModeBDCAreas.Add(_forwardModes[i].getLowerModeBDCAreasStatistic(VectorAttribute.stdDev));
                    _stdDevForwardUpperModeBDCAreas.Add(_forwardModes[i].getUpperModeBDCAreasStatistic(VectorAttribute.stdDev));
                }
            }
        }
        public void getMedianForwardModeBDCAreas()
        {
            if (_medianForwardLowerModeBDCAreas.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _medianForwardLowerModeBDCAreas.Add(_forwardModes[i].getLowerModeBDCAreasStatistic(VectorAttribute.median));
                    _medianForwardUpperModeBDCAreas.Add(_forwardModes[i].getUpperModeBDCAreasStatistic(VectorAttribute.median));
                }
            }
        }
        public void getNumForwardModeBDCAreaSamples()
        {
            if (_numForwardLowerModeBDCAreaSamples.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _numForwardLowerModeBDCAreaSamples.Add(_forwardModes[i].getLowerModeBDCAreasStatistic(VectorAttribute.count));
                    _numForwardUpperModeBDCAreaSamples.Add(_forwardModes[i].getUpperModeBDCAreasStatistic(VectorAttribute.count));
                }
            }
        }

        /////////////////////////////////////////////////////////////////////////////
        ///
        //	Mode Boundary Direction Change Area X Length measurements
        //
        public void getForwardModeBDCAreaXLengths()
        {
            if (_forwardLowerModeBDCAreaXLengths.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    Vector forwardLowerModeBDCAreaXLengths;
                    Vector forwardUpperModeBDCAreaXLengths;
                    _forwardModes[i].getModeBDCAreaXLengths(out forwardLowerModeBDCAreaXLengths, out forwardUpperModeBDCAreaXLengths);

                    _forwardLowerModeBDCAreaXLengths.Add(forwardLowerModeBDCAreaXLengths);
                    _forwardUpperModeBDCAreaXLengths.Add(forwardUpperModeBDCAreaXLengths);
                }
            }
        }

        public void getMaxForwardModeBDCAreaXLengths()
        {
            if (_maxForwardLowerModeBDCAreaXLengths.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _maxForwardLowerModeBDCAreaXLengths.Add(_forwardModes[i].getLowerModeBDCAreaXLengthsStatistic(VectorAttribute.max));
                    _maxForwardUpperModeBDCAreaXLengths.Add(_forwardModes[i].getUpperModeBDCAreaXLengthsStatistic(VectorAttribute.max));
                }
            }
        }
        public void getMinForwardModeBDCAreaXLengths()
        {
            if (_minForwardLowerModeBDCAreaXLengths.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _minForwardLowerModeBDCAreaXLengths.Add(_forwardModes[i].getLowerModeBDCAreaXLengthsStatistic(VectorAttribute.min));
                    _minForwardUpperModeBDCAreaXLengths.Add(_forwardModes[i].getUpperModeBDCAreaXLengthsStatistic(VectorAttribute.min));
                }
            }
        }
        public void getMeanForwardModeBDCAreaXLengths()
        {
            if (_meanForwardLowerModeBDCAreaXLengths.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _meanForwardLowerModeBDCAreaXLengths.Add(_forwardModes[i].getLowerModeBDCAreaXLengthsStatistic(VectorAttribute.mean));
                    _meanForwardUpperModeBDCAreaXLengths.Add(_forwardModes[i].getUpperModeBDCAreaXLengthsStatistic(VectorAttribute.mean));
                }
            }
        }
        public void getSumForwardModeBDCAreaXLengths()
        {
            if (_sumForwardLowerModeBDCAreaXLengths.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _sumForwardLowerModeBDCAreaXLengths.Add(_forwardModes[i].getLowerModeBDCAreaXLengthsStatistic(VectorAttribute.sum));
                    _sumForwardUpperModeBDCAreaXLengths.Add(_forwardModes[i].getUpperModeBDCAreaXLengthsStatistic(VectorAttribute.sum));
                }
            }
        }
        public void getVarForwardModeBDCAreaXLengths()
        {
            if (_varForwardLowerModeBDCAreaXLengths.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _varForwardLowerModeBDCAreaXLengths.Add(_forwardModes[i].getLowerModeBDCAreaXLengthsStatistic(VectorAttribute.variance));
                    _varForwardUpperModeBDCAreaXLengths.Add(_forwardModes[i].getUpperModeBDCAreaXLengthsStatistic(VectorAttribute.variance));
                }
            }
        }
        public void getStdDevForwardModeBDCAreaXLengths()
        {
            if (_stdDevForwardLowerModeBDCAreaXLengths.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _stdDevForwardLowerModeBDCAreaXLengths.Add(_forwardModes[i].getLowerModeBDCAreaXLengthsStatistic(VectorAttribute.stdDev));
                    _stdDevForwardUpperModeBDCAreaXLengths.Add(_forwardModes[i].getUpperModeBDCAreaXLengthsStatistic(VectorAttribute.stdDev));
                }
            }
        }
        public void getMedianForwardModeBDCAreaXLengths()
        {
            if (_medianForwardLowerModeBDCAreaXLengths.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _medianForwardLowerModeBDCAreaXLengths.Add(_forwardModes[i].getLowerModeBDCAreaXLengthsStatistic(VectorAttribute.median));
                    _medianForwardUpperModeBDCAreaXLengths.Add(_forwardModes[i].getUpperModeBDCAreaXLengthsStatistic(VectorAttribute.median));
                }
            }
        }
        public void getNumForwardModeBDCAreaXLengthSamples()
        {
            if (_numForwardLowerModeBDCAreaXLengthSamples.Count == 0)
            {
                for (int i = 0; i < _numForwardModes; i++)
                {
                    _numForwardLowerModeBDCAreaXLengthSamples.Add(_forwardModes[i].getLowerModeBDCAreaXLengthsStatistic(VectorAttribute.count));
                    _numForwardUpperModeBDCAreaXLengthSamples.Add(_forwardModes[i].getUpperModeBDCAreaXLengthsStatistic(VectorAttribute.count));
                }
            }
        }
        //
        /////////////////////////////////////////////////////////////////////////////
        ///
        //	Reverse Mode Widths
        public void getReverseModeWidths()
        {
            if (_reverseModeWidths.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    Vector reverseModeWidths;
                    _reverseModes[i].getModeWidths(out reverseModeWidths);

                    _reverseModeWidths.Add(reverseModeWidths);
                }
            }
        }

        public void getMaxReverseModeWidths()
        {
            if (_maxReverseModeWidths.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                    _maxReverseModeWidths.Add(_reverseModes[i].getModeWidthsStatistic(VectorAttribute.max));
            }
        }
        public void getMinReverseModeWidths()
        {
            if (_minReverseModeWidths.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                    _minReverseModeWidths.Add(_reverseModes[i].getModeWidthsStatistic(VectorAttribute.min));
            }
        }
        public void getMeanReverseModeWidths()
        {
            if (_meanReverseModeWidths.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                    _meanReverseModeWidths.Add(_reverseModes[i].getModeWidthsStatistic(VectorAttribute.mean));
            }
        }
        public void getSumReverseModeWidths()
        {
            if (_sumReverseModeWidths.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                    _sumReverseModeWidths.Add(_reverseModes[i].getModeWidthsStatistic(VectorAttribute.sum));
            }
        }
        public void getVarReverseModeWidths()
        {
            if (_varReverseModeWidths.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                    _varReverseModeWidths.Add(_reverseModes[i].getModeWidthsStatistic(VectorAttribute.variance));
            }
        }
        public void getStdDevReverseModeWidths()
        {
            if (_stdDevReverseModeWidths.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                    _stdDevReverseModeWidths.Add(_reverseModes[i].getModeWidthsStatistic(VectorAttribute.stdDev));
            }
        }
        public void getMedianReverseModeWidths()
        {
            if (_medianReverseModeWidths.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                    _medianReverseModeWidths.Add(_reverseModes[i].getModeWidthsStatistic(VectorAttribute.median));
            }
        }
        public void getNumReverseModeWidthSamples()
        {
            if (_numReverseModeWidthSamples.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                    _numReverseModeWidthSamples.Add(_reverseModes[i].getModeWidthsStatistic(VectorAttribute.count));
            }
        }

        //
        /////////////////////////////////////////////////////////////////////////////
        ///
        //	Reverse Mode Slopes
        //
        public void getReverseModeSlopes()
        {
            if (_reverseLowerModeSlopes.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    Vector reverseLowerModeSlopes;
                    Vector reverseUpperModeSlopes;
                    _reverseModes[i].getModeSlopes(out reverseLowerModeSlopes, out reverseUpperModeSlopes);

                    _reverseLowerModeSlopes.Add(reverseLowerModeSlopes);
                    _reverseUpperModeSlopes.Add(reverseUpperModeSlopes);
                }
            }
        }

        public void getMaxReverseModeSlopes()
        {
            if (_maxReverseLowerModeSlopes.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _maxReverseLowerModeSlopes.Add(_reverseModes[i].getModeLowerSlopesStatistic(VectorAttribute.max));
                    _maxReverseUpperModeSlopes.Add(_reverseModes[i].getModeUpperSlopesStatistic(VectorAttribute.max));
                }
            }
        }
        public void getMinReverseModeSlopes()
        {
            if (_minReverseLowerModeSlopes.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _minReverseLowerModeSlopes.Add(_reverseModes[i].getModeLowerSlopesStatistic(VectorAttribute.min));
                    _minReverseUpperModeSlopes.Add(_reverseModes[i].getModeUpperSlopesStatistic(VectorAttribute.min));
                }
            }
        }
        public void getMeanReverseModeSlopes()
        {
            if (_meanReverseLowerModeSlopes.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _meanReverseLowerModeSlopes.Add(_reverseModes[i].getModeLowerSlopesStatistic(VectorAttribute.mean));
                    _meanReverseUpperModeSlopes.Add(_reverseModes[i].getModeUpperSlopesStatistic(VectorAttribute.mean));
                }
            }
        }
        public void getSumReverseModeSlopes()
        {
            if (_sumReverseLowerModeSlopes.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _sumReverseLowerModeSlopes.Add(_reverseModes[i].getModeLowerSlopesStatistic(VectorAttribute.sum));
                    _sumReverseUpperModeSlopes.Add(_reverseModes[i].getModeUpperSlopesStatistic(VectorAttribute.sum));
                }
            }
        }
        public void getVarReverseModeSlopes()
        {
            if (_varReverseLowerModeSlopes.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _varReverseLowerModeSlopes.Add(_reverseModes[i].getModeLowerSlopesStatistic(VectorAttribute.variance));
                    _varReverseUpperModeSlopes.Add(_reverseModes[i].getModeUpperSlopesStatistic(VectorAttribute.variance));
                }
            }
        }
        public void getStdDevReverseModeSlopes()
        {
            if (_stdDevReverseLowerModeSlopes.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _stdDevReverseLowerModeSlopes.Add(_reverseModes[i].getModeLowerSlopesStatistic(VectorAttribute.stdDev));
                    _stdDevReverseUpperModeSlopes.Add(_reverseModes[i].getModeUpperSlopesStatistic(VectorAttribute.stdDev));
                }
            }
        }
        public void getMedianReverseModeSlopes()
        {
            if (_medianReverseLowerModeSlopes.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _medianReverseLowerModeSlopes.Add(_reverseModes[i].getModeLowerSlopesStatistic(VectorAttribute.median));
                    _medianReverseUpperModeSlopes.Add(_reverseModes[i].getModeUpperSlopesStatistic(VectorAttribute.median));
                }
            }
        }
        public void getNumReverseModeSlopeSamples()
        {
            if (_numReverseLowerModeSlopeSamples.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _numReverseLowerModeSlopeSamples.Add(_reverseModes[i].getModeLowerSlopesStatistic(VectorAttribute.count));
                    _numReverseUpperModeSlopeSamples.Add(_reverseModes[i].getModeUpperSlopesStatistic(VectorAttribute.count));
                }
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        ///
        //	Modal Distortion
        //
        public void getReverseModalDistortionAngles()
        {
            if (_reverseLowerModalDistortionAngles.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _reverseLowerModalDistortionAngles.Add(_reverseModes[i].getLowerModeModalDistortionAngle());
                    _reverseUpperModalDistortionAngles.Add(_reverseModes[i].getUpperModeModalDistortionAngle());
                }
            }
        }
        public void getReverseModalDistortionAngleXPositions()
        {
            if (_reverseLowerModalDistortionAngleXPositions.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _reverseLowerModalDistortionAngleXPositions.Add(_reverseModes[i].getLowerModeMaxModalDistortionAngleXPosition());
                    _reverseUpperModalDistortionAngleXPositions.Add(_reverseModes[i].getUpperModeMaxModalDistortionAngleXPosition());
                }
            }
        }

        /////////////////////////////////////////////////////////////////////////////
        ///
        //	Mode Boundary Direction Change Area measurements
        //
        public void getReverseModeBDCAreas()
        {
            if (_reverseLowerModeBDCAreas.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    Vector reverseLowerModeBDCAreas;
                    Vector reverseUpperModeBDCAreas;

                    _reverseModes[i].getModeBDCAreas(out reverseLowerModeBDCAreas, out reverseUpperModeBDCAreas);

                    _reverseLowerModeBDCAreas.Add(reverseLowerModeBDCAreas);
                    _reverseUpperModeBDCAreas.Add(reverseUpperModeBDCAreas);
                }
            }
        }

        public void getMaxReverseModeBDCAreas()
        {
            if (_maxReverseLowerModeBDCAreas.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _maxReverseLowerModeBDCAreas.Add(_reverseModes[i].getLowerModeBDCAreasStatistic(VectorAttribute.max));
                    _maxReverseUpperModeBDCAreas.Add(_reverseModes[i].getUpperModeBDCAreasStatistic(VectorAttribute.max));
                }
            }
        }
        public void getMinReverseModeBDCAreas()
        {
            if (_minReverseLowerModeBDCAreas.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _minReverseLowerModeBDCAreas.Add(_reverseModes[i].getLowerModeBDCAreasStatistic(VectorAttribute.min));
                    _minReverseUpperModeBDCAreas.Add(_reverseModes[i].getUpperModeBDCAreasStatistic(VectorAttribute.min));
                }
            }
        }
        public void getMeanReverseModeBDCAreas()
        {
            if (_meanReverseLowerModeBDCAreas.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _meanReverseLowerModeBDCAreas.Add(_reverseModes[i].getLowerModeBDCAreasStatistic(VectorAttribute.mean));
                    _meanReverseUpperModeBDCAreas.Add(_reverseModes[i].getUpperModeBDCAreasStatistic(VectorAttribute.mean));
                }
            }
        }
        public void getSumReverseModeBDCAreas()
        {
            if (_sumReverseLowerModeBDCAreas.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _sumReverseLowerModeBDCAreas.Add(_reverseModes[i].getLowerModeBDCAreasStatistic(VectorAttribute.sum));
                    _sumReverseUpperModeBDCAreas.Add(_reverseModes[i].getUpperModeBDCAreasStatistic(VectorAttribute.sum));
                }
            }
        }
        public void getVarReverseModeBDCAreas()
        {
            if (_varReverseLowerModeBDCAreas.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _varReverseLowerModeBDCAreas.Add(_reverseModes[i].getLowerModeBDCAreasStatistic(VectorAttribute.variance));
                    _varReverseUpperModeBDCAreas.Add(_reverseModes[i].getUpperModeBDCAreasStatistic(VectorAttribute.variance));
                }
            }
        }
        public void getStdDevReverseModeBDCAreas()
        {
            if (_stdDevReverseLowerModeBDCAreas.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _stdDevReverseLowerModeBDCAreas.Add(_reverseModes[i].getLowerModeBDCAreasStatistic(VectorAttribute.stdDev));
                    _stdDevReverseUpperModeBDCAreas.Add(_reverseModes[i].getUpperModeBDCAreasStatistic(VectorAttribute.stdDev));
                }
            }
        }
        public void getMedianReverseModeBDCAreas()
        {
            if (_medianReverseLowerModeBDCAreas.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _medianReverseLowerModeBDCAreas.Add(_reverseModes[i].getLowerModeBDCAreasStatistic(VectorAttribute.median));
                    _medianReverseUpperModeBDCAreas.Add(_reverseModes[i].getUpperModeBDCAreasStatistic(VectorAttribute.median));
                }
            }
        }
        public void getNumReverseModeBDCAreaSamples()
        {
            if (_numReverseLowerModeBDCAreaSamples.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _numReverseLowerModeBDCAreaSamples.Add(_reverseModes[i].getLowerModeBDCAreasStatistic(VectorAttribute.count));
                    _numReverseUpperModeBDCAreaSamples.Add(_reverseModes[i].getUpperModeBDCAreasStatistic(VectorAttribute.count));
                }
            }
        }

        /////////////////////////////////////////////////////////////////////////////
        ///
        //	Mode Boundary Direction Change Area X Length measurements
        //
        public void getReverseModeBDCAreaXLengths()
        {
            if (_reverseLowerModeBDCAreaXLengths.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    Vector reverseLowerModeBDCAreaXLengths;
                    Vector reverseUpperModeBDCAreaXLengths;
                    _reverseModes[i].getModeBDCAreaXLengths(out reverseLowerModeBDCAreaXLengths, out reverseUpperModeBDCAreaXLengths);

                    _reverseLowerModeBDCAreaXLengths.Add(reverseLowerModeBDCAreaXLengths);
                    _reverseUpperModeBDCAreaXLengths.Add(reverseUpperModeBDCAreaXLengths);
                }
            }
        }

        public void getMaxReverseModeBDCAreaXLengths()
        {
            if (_maxReverseLowerModeBDCAreaXLengths.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _maxReverseLowerModeBDCAreaXLengths.Add(_reverseModes[i].getLowerModeBDCAreaXLengthsStatistic(VectorAttribute.max));
                    _maxReverseUpperModeBDCAreaXLengths.Add(_reverseModes[i].getUpperModeBDCAreaXLengthsStatistic(VectorAttribute.max));
                }
            }
        }
        public void getMinReverseModeBDCAreaXLengths()
        {
            if (_minReverseLowerModeBDCAreaXLengths.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _minReverseLowerModeBDCAreaXLengths.Add(_reverseModes[i].getLowerModeBDCAreaXLengthsStatistic(VectorAttribute.min));
                    _minReverseUpperModeBDCAreaXLengths.Add(_reverseModes[i].getUpperModeBDCAreaXLengthsStatistic(VectorAttribute.min));
                }
            }
        }
        public void getMeanReverseModeBDCAreaXLengths()
        {
            if (_meanReverseLowerModeBDCAreaXLengths.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _meanReverseLowerModeBDCAreaXLengths.Add(_reverseModes[i].getLowerModeBDCAreaXLengthsStatistic(VectorAttribute.mean));
                    _meanReverseUpperModeBDCAreaXLengths.Add(_reverseModes[i].getUpperModeBDCAreaXLengthsStatistic(VectorAttribute.mean));
                }
            }
        }
        public void getSumReverseModeBDCAreaXLengths()
        {
            if (_sumReverseLowerModeBDCAreaXLengths.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _sumReverseLowerModeBDCAreaXLengths.Add(_reverseModes[i].getLowerModeBDCAreaXLengthsStatistic(VectorAttribute.sum));
                    _sumReverseUpperModeBDCAreaXLengths.Add(_reverseModes[i].getUpperModeBDCAreaXLengthsStatistic(VectorAttribute.sum));
                }
            }
        }
        public void getVarReverseModeBDCAreaXLengths()
        {
            if (_varReverseLowerModeBDCAreaXLengths.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _varReverseLowerModeBDCAreaXLengths.Add(_reverseModes[i].getLowerModeBDCAreaXLengthsStatistic(VectorAttribute.variance));
                    _varReverseUpperModeBDCAreaXLengths.Add(_reverseModes[i].getUpperModeBDCAreaXLengthsStatistic(VectorAttribute.variance));
                }
            }
        }
        public void getStdDevReverseModeBDCAreaXLengths()
        {
            if (_stdDevReverseLowerModeBDCAreaXLengths.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _stdDevReverseLowerModeBDCAreaXLengths.Add(_reverseModes[i].getLowerModeBDCAreaXLengthsStatistic(VectorAttribute.stdDev));
                    _stdDevReverseUpperModeBDCAreaXLengths.Add(_reverseModes[i].getUpperModeBDCAreaXLengthsStatistic(VectorAttribute.stdDev));
                }
            }
        }
        public void getMedianReverseModeBDCAreaXLengths()
        {
            if (_medianReverseLowerModeBDCAreaXLengths.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _medianReverseLowerModeBDCAreaXLengths.Add(_reverseModes[i].getLowerModeBDCAreaXLengthsStatistic(VectorAttribute.median));
                    _medianReverseUpperModeBDCAreaXLengths.Add(_reverseModes[i].getUpperModeBDCAreaXLengthsStatistic(VectorAttribute.median));
                }
            }
        }
        public void getNumReverseModeBDCAreaXLengthSamples()
        {
            if (_numReverseLowerModeBDCAreaXLengthSamples.Count == 0)
            {
                for (int i = 0; i < _numReverseModes; i++)
                {
                    _numReverseLowerModeBDCAreaXLengthSamples.Add(_reverseModes[i].getLowerModeBDCAreaXLengthsStatistic(VectorAttribute.count));
                    _numReverseUpperModeBDCAreaXLengthSamples.Add(_reverseModes[i].getUpperModeBDCAreaXLengthsStatistic(VectorAttribute.count));
                }
            }
        }
        public PassFailThresholds getPassFailThresholds()
        {
            PassFailThresholds thresholds = new PassFailThresholds();

            thresholds._maxModeWidth = get_DSDBR01_SMPF_max_mode_width(_superModeNumber);
            thresholds._minModeWidth = get_DSDBR01_SMPF_min_mode_width(_superModeNumber);
            thresholds._maxModeBordersRemoved = get_DSDBR01_SMPF_max_mode_borders_removed(_superModeNumber);
            thresholds._maxModeBDCArea = get_DSDBR01_SMPF_max_mode_bdc_area(_superModeNumber);
            thresholds._maxModeBDCAreaXLength = get_DSDBR01_SMPF_max_mode_bdc_area_x_length(_superModeNumber);
            thresholds._meanPercWorkingRegion = get_DSDBR01_SMPF_mean_perc_working_region(_superModeNumber);
            thresholds._minPercWorkingRegion = get_DSDBR01_SMPF_min_perc_working_region(_superModeNumber);
            thresholds._modalDistortionAngle = get_DSDBR01_SMPF_modal_distortion_angle(_superModeNumber);
            thresholds._modalDistortionAngleXPosition = get_DSDBR01_SMPF_modal_distortion_angle_x_pos(_superModeNumber);
            thresholds._numModeBDCAreas = get_DSDBR01_SMPF_num_mode_bdc_areas(_superModeNumber);
            thresholds._sumModeBDCAreas = get_DSDBR01_SMPF_sum_mode_bdc_areas(_superModeNumber);
            //thresholds._maxPrGap			= get_DSDBR01_SMPF_continuity_spacing(_superModeNumber);
            thresholds._maxMiddleLineSlope = get_DSDBR01_SMPF_max_middle_line_slope(_superModeNumber);
            thresholds._minMiddleLineSlope = get_DSDBR01_SMPF_min_middle_line_slope(_superModeNumber);
            thresholds._middleLineRMSValue = get_DSDBR01_SMPF_max_middle_line_rms_value(_superModeNumber);
            thresholds._maxModeLineSlope = get_DSDBR01_SMPF_max_mode_line_slope(_superModeNumber);

            return thresholds;
        }

        public int get_DSDBR01_SMPF_max_mode_width(int smNumber)
        {
            int maxModeWidth = 0;

            if (smNumber == 0)
                maxModeWidth = Convert.ToInt32(this.superMap0QASitting["MaxModeWidthThreshold"]);
            else if (smNumber == 1)
                maxModeWidth = Convert.ToInt32(this.superMap1QASitting["MaxModeWidthThreshold"]);
            else if (smNumber == 2)
                maxModeWidth = Convert.ToInt32(this.superMap2QASitting["MaxModeWidthThreshold"]);
            else if (smNumber == 3)
                maxModeWidth = Convert.ToInt32(this.superMap3QASitting["MaxModeWidthThreshold"]);
            else if (smNumber == 4)
                maxModeWidth = Convert.ToInt32(this.superMap4QASitting["MaxModeWidthThreshold"]);
            else if (smNumber == 5)
                maxModeWidth = Convert.ToInt32(this.superMap5QASitting["MaxModeWidthThreshold"]);
            else if (smNumber == 6)
                maxModeWidth = Convert.ToInt32(this.superMap6QASitting["MaxModeWidthThreshold"]);
            else if (smNumber == 7)
                maxModeWidth = Convert.ToInt32(this.superMap7QASitting["MaxModeWidthThreshold"]);
            else if (smNumber == 8)
                maxModeWidth = Convert.ToInt32(this.superMap8QASitting["MaxModeWidthThreshold"]);
            else if (smNumber == 9)
                maxModeWidth = Convert.ToInt32(this.superMap9QASitting["MaxModeWidthThreshold"]);

            return maxModeWidth;
        }

        public int get_DSDBR01_SMPF_min_mode_width(int smNumber)
        {
            int minModeWidth = 0;

            if (smNumber == 0)
                minModeWidth = Convert.ToInt32(this.superMap0QASitting["MinModeWidthThreshold"]);
            else if (smNumber == 1)
                minModeWidth = Convert.ToInt32(this.superMap1QASitting["MinModeWidthThreshold"]);
            else if (smNumber == 2)
                minModeWidth = Convert.ToInt32(this.superMap2QASitting["MinModeWidthThreshold"]);
            else if (smNumber == 3)
                minModeWidth = Convert.ToInt32(this.superMap3QASitting["MinModeWidthThreshold"]);
            else if (smNumber == 4)
                minModeWidth = Convert.ToInt32(this.superMap4QASitting["MinModeWidthThreshold"]);
            else if (smNumber == 5)
                minModeWidth = Convert.ToInt32(this.superMap5QASitting["MinModeWidthThreshold"]);
            else if (smNumber == 6)
                minModeWidth = Convert.ToInt32(this.superMap6QASitting["MinModeWidthThreshold"]);
            else if (smNumber == 7)
                minModeWidth = Convert.ToInt32(this.superMap7QASitting["MinModeWidthThreshold"]);
            else if (smNumber == 8)
                minModeWidth = Convert.ToInt32(this.superMap8QASitting["MinModeWidthThreshold"]);
            else if (smNumber == 9)
                minModeWidth = Convert.ToInt32(this.superMap9QASitting["MinModeWidthThreshold"]);

            return minModeWidth;
        }
        public int get_DSDBR01_SMPF_max_mode_borders_removed(int smNumber)
        {
            int modeBordersRemoved = 0;

            if (smNumber == 0)
                modeBordersRemoved = Convert.ToInt32(this.superMap0QASitting["MaxModeBordersRemoved"]);
            else if (smNumber == 1)
                modeBordersRemoved = Convert.ToInt32(this.superMap1QASitting["MaxModeBordersRemoved"]);
            else if (smNumber == 2)
                modeBordersRemoved = Convert.ToInt32(this.superMap2QASitting["MaxModeBordersRemoved"]);
            else if (smNumber == 3)
                modeBordersRemoved = Convert.ToInt32(this.superMap3QASitting["MaxModeBordersRemoved"]);
            else if (smNumber == 4)
                modeBordersRemoved = Convert.ToInt32(this.superMap4QASitting["MaxModeBordersRemoved"]);
            else if (smNumber == 5)
                modeBordersRemoved = Convert.ToInt32(this.superMap5QASitting["MaxModeBordersRemoved"]);
            else if (smNumber == 6)
                modeBordersRemoved = Convert.ToInt32(this.superMap6QASitting["MaxModeBordersRemoved"]);
            else if (smNumber == 7)
                modeBordersRemoved = Convert.ToInt32(this.superMap7QASitting["MaxModeBordersRemoved"]);
            else if (smNumber == 8)
                modeBordersRemoved = Convert.ToInt32(this.superMap8QASitting["MaxModeBordersRemoved"]);
            else if (smNumber == 9)
                modeBordersRemoved = Convert.ToInt32(this.superMap9QASitting["MaxModeBordersRemoved"]);

            return modeBordersRemoved;
        }


        public double get_DSDBR01_SMPF_mean_perc_working_region(short smNumber)
        {
            double meanPercWorkingRegion = 0;

            if (smNumber == 0)
                meanPercWorkingRegion = Convert.ToInt32(this.superMap0QASitting["MeanPercentageWorkingRegion"]);
            else if (smNumber == 1)
                meanPercWorkingRegion = Convert.ToInt32(this.superMap1QASitting["MeanPercentageWorkingRegion"]);
            else if (smNumber == 2)
                meanPercWorkingRegion = Convert.ToInt32(this.superMap2QASitting["MeanPercentageWorkingRegion"]);
            else if (smNumber == 3)
                meanPercWorkingRegion = Convert.ToInt32(this.superMap3QASitting["MeanPercentageWorkingRegion"]);
            else if (smNumber == 4)
                meanPercWorkingRegion = Convert.ToInt32(this.superMap4QASitting["MeanPercentageWorkingRegion"]);
            else if (smNumber == 5)
                meanPercWorkingRegion = Convert.ToInt32(this.superMap5QASitting["MeanPercentageWorkingRegion"]);
            else if (smNumber == 6)
                meanPercWorkingRegion = Convert.ToInt32(this.superMap6QASitting["MeanPercentageWorkingRegion"]);
            else if (smNumber == 7)
                meanPercWorkingRegion = Convert.ToInt32(this.superMap7QASitting["MeanPercentageWorkingRegion"]);
            else if (smNumber == 8)
                meanPercWorkingRegion = Convert.ToInt32(this.superMap8QASitting["MeanPercentageWorkingRegion"]);
            else if (smNumber == 9)
                meanPercWorkingRegion = Convert.ToInt32(this.superMap9QASitting["MeanPercentageWorkingRegion"]);

            return meanPercWorkingRegion;
        }

        double get_DSDBR01_SMPF_min_perc_working_region(int smNumber)
        {
            double minPercWorkingRegion = 0;

            if (smNumber == 0)
                minPercWorkingRegion = Convert.ToInt32(this.superMap0QASitting["MinPercentageWorkingRegion"]);
            else if (smNumber == 1)
                minPercWorkingRegion = Convert.ToInt32(this.superMap1QASitting["MinPercentageWorkingRegion"]);
            else if (smNumber == 2)
                minPercWorkingRegion = Convert.ToInt32(this.superMap2QASitting["MinPercentageWorkingRegion"]);
            else if (smNumber == 3)
                minPercWorkingRegion = Convert.ToInt32(this.superMap3QASitting["MinPercentageWorkingRegion"]);
            else if (smNumber == 4)
                minPercWorkingRegion = Convert.ToInt32(this.superMap4QASitting["MinPercentageWorkingRegion"]);
            else if (smNumber == 5)
                minPercWorkingRegion = Convert.ToInt32(this.superMap5QASitting["MinPercentageWorkingRegion"]);
            else if (smNumber == 6)
                minPercWorkingRegion = Convert.ToInt32(this.superMap6QASitting["MinPercentageWorkingRegion"]);
            else if (smNumber == 7)
                minPercWorkingRegion = Convert.ToInt32(this.superMap7QASitting["MinPercentageWorkingRegion"]);
            else if (smNumber == 8)
                minPercWorkingRegion = Convert.ToInt32(this.superMap8QASitting["MinPercentageWorkingRegion"]);
            else if (smNumber == 9)
                minPercWorkingRegion = Convert.ToInt32(this.superMap9QASitting["MinPercentageWorkingRegion"]);

            return minPercWorkingRegion;
        }

        double get_DSDBR01_SMPF_max_mode_bdc_area(int smNumber)
        {
            double maxBdcArea = 0;

            if (smNumber == 0)
                maxBdcArea = Convert.ToInt32(this.superMap0QASitting["MaxModeBDCArea"]);
            else if (smNumber == 1)
                maxBdcArea = Convert.ToInt32(this.superMap1QASitting["MaxModeBDCArea"]);
            else if (smNumber == 2)
                maxBdcArea = Convert.ToInt32(this.superMap2QASitting["MaxModeBDCArea"]);
            else if (smNumber == 3)
                maxBdcArea = Convert.ToInt32(this.superMap3QASitting["MaxModeBDCArea"]);
            else if (smNumber == 4)
                maxBdcArea = Convert.ToInt32(this.superMap4QASitting["MaxModeBDCArea"]);
            else if (smNumber == 5)
                maxBdcArea = Convert.ToInt32(this.superMap5QASitting["MaxModeBDCArea"]);
            else if (smNumber == 6)
                maxBdcArea = Convert.ToInt32(this.superMap6QASitting["MaxModeBDCArea"]);
            else if (smNumber == 7)
                maxBdcArea = Convert.ToInt32(this.superMap7QASitting["MaxModeBDCArea"]);
            else if (smNumber == 8)
                maxBdcArea = Convert.ToInt32(this.superMap8QASitting["MaxModeBDCArea"]);
            else if (smNumber == 9)
                maxBdcArea = Convert.ToInt32(this.superMap9QASitting["MaxModeBDCArea"]);

            return maxBdcArea;
        }

        double get_DSDBR01_SMPF_max_mode_bdc_area_x_length(int smNumber)
        {
            double maxBdcAreaXLength = 0;

            if (smNumber == 0)
                maxBdcAreaXLength = Convert.ToInt32(this.superMap0QASitting["MaxModeBDCAreaXLength"]);
            else if (smNumber == 1)
                maxBdcAreaXLength = Convert.ToInt32(this.superMap1QASitting["MaxModeBDCAreaXLength"]);
            else if (smNumber == 2)
                maxBdcAreaXLength = Convert.ToInt32(this.superMap2QASitting["MaxModeBDCAreaXLength"]);
            else if (smNumber == 3)
                maxBdcAreaXLength = Convert.ToInt32(this.superMap3QASitting["MaxModeBDCAreaXLength"]);
            else if (smNumber == 4)
                maxBdcAreaXLength = Convert.ToInt32(this.superMap4QASitting["MaxModeBDCAreaXLength"]);
            else if (smNumber == 5)
                maxBdcAreaXLength = Convert.ToInt32(this.superMap5QASitting["MaxModeBDCAreaXLength"]);
            else if (smNumber == 6)
                maxBdcAreaXLength = Convert.ToInt32(this.superMap6QASitting["MaxModeBDCAreaXLength"]);
            else if (smNumber == 7)
                maxBdcAreaXLength = Convert.ToInt32(this.superMap7QASitting["MaxModeBDCAreaXLength"]);
            else if (smNumber == 8)
                maxBdcAreaXLength = Convert.ToInt32(this.superMap8QASitting["MaxModeBDCAreaXLength"]);
            else if (smNumber == 9)
                maxBdcAreaXLength = Convert.ToInt32(this.superMap9QASitting["MaxModeBDCAreaXLength"]);

            return maxBdcAreaXLength;
        }


        double get_DSDBR01_SMPF_sum_mode_bdc_areas(int smNumber)
        {
            double sumBdcArea = 0;

            if (smNumber == 0)
                sumBdcArea = Convert.ToInt32(this.superMap0QASitting["SumModeBDCAreas"]);
            else if (smNumber == 1)
                sumBdcArea = Convert.ToInt32(this.superMap1QASitting["SumModeBDCAreas"]);
            else if (smNumber == 2)
                sumBdcArea = Convert.ToInt32(this.superMap2QASitting["SumModeBDCAreas"]);
            else if (smNumber == 3)
                sumBdcArea = Convert.ToInt32(this.superMap3QASitting["SumModeBDCAreas"]);
            else if (smNumber == 4)
                sumBdcArea = Convert.ToInt32(this.superMap4QASitting["SumModeBDCAreas"]);
            else if (smNumber == 5)
                sumBdcArea = Convert.ToInt32(this.superMap5QASitting["SumModeBDCAreas"]);
            else if (smNumber == 6)
                sumBdcArea = Convert.ToInt32(this.superMap6QASitting["SumModeBDCAreas"]);
            else if (smNumber == 7)
                sumBdcArea = Convert.ToInt32(this.superMap7QASitting["SumModeBDCAreas"]);
            else if (smNumber == 8)
                sumBdcArea = Convert.ToInt32(this.superMap8QASitting["SumModeBDCAreas"]);
            else if (smNumber == 9)
                sumBdcArea = Convert.ToInt32(this.superMap9QASitting["SumModeBDCAreas"]);

            return sumBdcArea;
        }


        double get_DSDBR01_SMPF_num_mode_bdc_areas(int smNumber)
        {
            double numBdcArea = 0;

            if (smNumber == 0)
                numBdcArea = Convert.ToInt32(this.superMap0QASitting["NumModeBDCAreas"]);
            else if (smNumber == 1)
                numBdcArea = Convert.ToInt32(this.superMap1QASitting["NumModeBDCAreas"]);
            else if (smNumber == 2)
                numBdcArea = Convert.ToInt32(this.superMap2QASitting["NumModeBDCAreas"]);
            else if (smNumber == 3)
                numBdcArea = Convert.ToInt32(this.superMap3QASitting["NumModeBDCAreas"]);
            else if (smNumber == 4)
                numBdcArea = Convert.ToInt32(this.superMap4QASitting["NumModeBDCAreas"]);
            else if (smNumber == 5)
                numBdcArea = Convert.ToInt32(this.superMap5QASitting["NumModeBDCAreas"]);
            else if (smNumber == 6)
                numBdcArea = Convert.ToInt32(this.superMap6QASitting["NumModeBDCAreas"]);
            else if (smNumber == 7)
                numBdcArea = Convert.ToInt32(this.superMap7QASitting["NumModeBDCAreas"]);
            else if (smNumber == 8)
                numBdcArea = Convert.ToInt32(this.superMap8QASitting["NumModeBDCAreas"]);
            else if (smNumber == 9)
                numBdcArea = Convert.ToInt32(this.superMap9QASitting["NumModeBDCAreas"]);

            return numBdcArea;
        }


        double get_DSDBR01_SMPF_modal_distortion_angle(int smNumber)
        {
            double modalDistortionAngle = 0;

            if (smNumber == 0)
                modalDistortionAngle = Convert.ToInt32(this.superMap0QASitting["MaxModalDistortionAngle"]);
            else if (smNumber == 1)
                modalDistortionAngle = Convert.ToInt32(this.superMap1QASitting["MaxModalDistortionAngle"]);
            else if (smNumber == 2)
                modalDistortionAngle = Convert.ToInt32(this.superMap2QASitting["MaxModalDistortionAngle"]);
            else if (smNumber == 3)
                modalDistortionAngle = Convert.ToInt32(this.superMap3QASitting["MaxModalDistortionAngle"]);
            else if (smNumber == 4)
                modalDistortionAngle = Convert.ToInt32(this.superMap4QASitting["MaxModalDistortionAngle"]);
            else if (smNumber == 5)
                modalDistortionAngle = Convert.ToInt32(this.superMap5QASitting["MaxModalDistortionAngle"]);
            else if (smNumber == 6)
                modalDistortionAngle = Convert.ToInt32(this.superMap6QASitting["MaxModalDistortionAngle"]);
            else if (smNumber == 7)
                modalDistortionAngle = Convert.ToInt32(this.superMap7QASitting["MaxModalDistortionAngle"]);
            else if (smNumber == 8)
                modalDistortionAngle = Convert.ToInt32(this.superMap8QASitting["MaxModalDistortionAngle"]);
            else if (smNumber == 9)
                modalDistortionAngle = Convert.ToInt32(this.superMap9QASitting["MaxModalDistortionAngle"]);

            return modalDistortionAngle;
        }


        double get_DSDBR01_SMPF_modal_distortion_angle_x_pos(int smNumber)
        {
            double modalDistortionAngleXPos = 0;

            if (smNumber == 0)
                modalDistortionAngleXPos = Convert.ToInt32(this.superMap0QASitting["MaxModalDistortionAngleXPosition"]);
            else if (smNumber == 1)
                modalDistortionAngleXPos = Convert.ToInt32(this.superMap1QASitting["MaxModalDistortionAngleXPosition"]);
            else if (smNumber == 2)
                modalDistortionAngleXPos = Convert.ToInt32(this.superMap2QASitting["MaxModalDistortionAngleXPosition"]);
            else if (smNumber == 3)
                modalDistortionAngleXPos = Convert.ToInt32(this.superMap3QASitting["MaxModalDistortionAngleXPosition"]);
            else if (smNumber == 4)
                modalDistortionAngleXPos = Convert.ToInt32(this.superMap4QASitting["MaxModalDistortionAngleXPosition"]);
            else if (smNumber == 5)
                modalDistortionAngleXPos = Convert.ToInt32(this.superMap5QASitting["MaxModalDistortionAngleXPosition"]);
            else if (smNumber == 6)
                modalDistortionAngleXPos = Convert.ToInt32(this.superMap6QASitting["MaxModalDistortionAngleXPosition"]);
            else if (smNumber == 7)
                modalDistortionAngleXPos = Convert.ToInt32(this.superMap7QASitting["MaxModalDistortionAngleXPosition"]);
            else if (smNumber == 8)
                modalDistortionAngleXPos = Convert.ToInt32(this.superMap8QASitting["MaxModalDistortionAngleXPosition"]);
            else if (smNumber == 9)
                modalDistortionAngleXPos = Convert.ToInt32(this.superMap9QASitting["MaxModalDistortionAngleXPosition"]);

            return modalDistortionAngleXPos;
        }


        double get_DSDBR01_SMPF_max_middle_line_rms_value(int smNumber)
        {
            double middleLineRMSValue = 0;

            if (smNumber == 0)
                middleLineRMSValue = Convert.ToInt32(this.superMap0QASitting["MaxMiddleLineRMSValue"]);
            else if (smNumber == 1)
                middleLineRMSValue = Convert.ToInt32(this.superMap1QASitting["MaxMiddleLineRMSValue"]);
            else if (smNumber == 2)
                middleLineRMSValue = Convert.ToInt32(this.superMap2QASitting["MaxMiddleLineRMSValue"]);
            else if (smNumber == 3)
                middleLineRMSValue = Convert.ToInt32(this.superMap3QASitting["MaxMiddleLineRMSValue"]);
            else if (smNumber == 4)
                middleLineRMSValue = Convert.ToInt32(this.superMap4QASitting["MaxMiddleLineRMSValue"]);
            else if (smNumber == 5)
                middleLineRMSValue = Convert.ToInt32(this.superMap5QASitting["MaxMiddleLineRMSValue"]);
            else if (smNumber == 6)
                middleLineRMSValue = Convert.ToInt32(this.superMap6QASitting["MaxMiddleLineRMSValue"]);
            else if (smNumber == 7)
                middleLineRMSValue = Convert.ToInt32(this.superMap7QASitting["MaxMiddleLineRMSValue"]);
            else if (smNumber == 8)
                middleLineRMSValue = Convert.ToInt32(this.superMap8QASitting["MaxMiddleLineRMSValue"]);
            else if (smNumber == 9)
                middleLineRMSValue = Convert.ToInt32(this.superMap9QASitting["MaxMiddleLineRMSValue"]);

            return middleLineRMSValue;
        }

        double get_DSDBR01_SMPF_min_middle_line_slope(int smNumber)
        {
            double minMiddleLineSlope = 0;

            if (smNumber == 0)
                minMiddleLineSlope = Convert.ToInt32(this.superMap0QASitting["MinMiddleLineSlope"]);
            else if (smNumber == 1)
                minMiddleLineSlope = Convert.ToInt32(this.superMap1QASitting["MinMiddleLineSlope"]);
            else if (smNumber == 2)
                minMiddleLineSlope = Convert.ToInt32(this.superMap2QASitting["MinMiddleLineSlope"]);
            else if (smNumber == 3)
                minMiddleLineSlope = Convert.ToInt32(this.superMap3QASitting["MinMiddleLineSlope"]);
            else if (smNumber == 4)
                minMiddleLineSlope = Convert.ToInt32(this.superMap4QASitting["MinMiddleLineSlope"]);
            else if (smNumber == 5)
                minMiddleLineSlope = Convert.ToInt32(this.superMap5QASitting["MinMiddleLineSlope"]);
            else if (smNumber == 6)
                minMiddleLineSlope = Convert.ToInt32(this.superMap6QASitting["MinMiddleLineSlope"]);
            else if (smNumber == 7)
                minMiddleLineSlope = Convert.ToInt32(this.superMap7QASitting["MinMiddleLineSlope"]);
            else if (smNumber == 8)
                minMiddleLineSlope = Convert.ToInt32(this.superMap8QASitting["MinMiddleLineSlope"]);
            else if (smNumber == 9)
                minMiddleLineSlope = Convert.ToInt32(this.superMap9QASitting["MinMiddleLineSlope"]);

            return minMiddleLineSlope;
        }

        double get_DSDBR01_SMPF_max_middle_line_slope(int smNumber)
        {
            double maxMiddleLineSlope = 0;

            if (smNumber == 0)
                maxMiddleLineSlope = Convert.ToInt32(this.superMap0QASitting["MaxMiddleLineSlope"]);
            else if (smNumber == 1)
                maxMiddleLineSlope = Convert.ToInt32(this.superMap1QASitting["MaxMiddleLineSlope"]);
            else if (smNumber == 2)
                maxMiddleLineSlope = Convert.ToInt32(this.superMap2QASitting["MaxMiddleLineSlope"]);
            else if (smNumber == 3)
                maxMiddleLineSlope = Convert.ToInt32(this.superMap3QASitting["MaxMiddleLineSlope"]);
            else if (smNumber == 4)
                maxMiddleLineSlope = Convert.ToInt32(this.superMap4QASitting["MaxMiddleLineSlope"]);
            else if (smNumber == 5)
                maxMiddleLineSlope = Convert.ToInt32(this.superMap5QASitting["MaxMiddleLineSlope"]);
            else if (smNumber == 6)
                maxMiddleLineSlope = Convert.ToInt32(this.superMap6QASitting["MaxMiddleLineSlope"]);
            else if (smNumber == 7)
                maxMiddleLineSlope = Convert.ToInt32(this.superMap7QASitting["MaxMiddleLineSlope"]);
            else if (smNumber == 8)
                maxMiddleLineSlope = Convert.ToInt32(this.superMap8QASitting["MaxMiddleLineSlope"]);
            else if (smNumber == 9)
                maxMiddleLineSlope = Convert.ToInt32(this.superMap9QASitting["MaxMiddleLineSlope"]);

            return maxMiddleLineSlope;
        }

        //double
        //CCGRegValues::
        //get_DSDBR01_SMPF_continuity_spacing(short smNumber)
        //{
        //	double continuitySpacing = DEFAULT_DSDBR01_SMPF_MAX_CONTINUITY_SPACING_THRESHOLD;
        //
        //	if(smNumber == 0)
        //		continuitySpacing = _DSDBR01_SMPF_SM0_max_continuity_spacing;
        //	else if(smNumber == 1)
        //		continuitySpacing = _DSDBR01_SMPF_SM1_max_continuity_spacing;
        //	else if(smNumber == 2)
        //		continuitySpacing = _DSDBR01_SMPF_SM2_max_continuity_spacing;
        //	else if(smNumber == 3)
        //		continuitySpacing = _DSDBR01_SMPF_SM3_max_continuity_spacing;
        //	else if(smNumber == 4)
        //		continuitySpacing = _DSDBR01_SMPF_SM4_max_continuity_spacing;
        //	else if(smNumber == 5)
        //		continuitySpacing = _DSDBR01_SMPF_SM5_max_continuity_spacing;
        //	else if(smNumber == 6)
        //		continuitySpacing = _DSDBR01_SMPF_SM6_max_continuity_spacing;
        //	else if(smNumber == 7)
        //		continuitySpacing = _DSDBR01_SMPF_SM7_max_continuity_spacing;
        //	else if(smNumber == 8)
        //		continuitySpacing = _DSDBR01_SMPF_SM8_max_continuity_spacing;
        //	else if(smNumber == 9)
        //		continuitySpacing = _DSDBR01_SMPF_SM9_max_continuity_spacing;
        //
        //	return continuitySpacing;
        //}
        //CCGRegValues::rtype
        //CCGRegValues::
        //set_DSDBR01_SMPF_continuity_spacing(short	smNumber,
        //									double	value)
        //{
        //	rtype rval = ok;
        //
        //	if(smNumber == 0)
        //		_DSDBR01_SMPF_SM0_max_continuity_spacing = value;
        //	else if(smNumber == 1)
        //		_DSDBR01_SMPF_SM1_max_continuity_spacing = value;
        //	else if(smNumber == 2)
        //		_DSDBR01_SMPF_SM2_max_continuity_spacing = value;
        //	else if(smNumber == 3)
        //		_DSDBR01_SMPF_SM3_max_continuity_spacing = value;
        //	else if(smNumber == 4)
        //		_DSDBR01_SMPF_SM4_max_continuity_spacing = value;
        //	else if(smNumber == 5)
        //		_DSDBR01_SMPF_SM5_max_continuity_spacing = value;
        //	else if(smNumber == 6)
        //		_DSDBR01_SMPF_SM6_max_continuity_spacing = value;
        //	else if(smNumber == 7)
        //		_DSDBR01_SMPF_SM7_max_continuity_spacing = value;
        //	else if(smNumber == 8)
        //		_DSDBR01_SMPF_SM8_max_continuity_spacing = value;
        //	else if(smNumber == 9)
        //		_DSDBR01_SMPF_SM9_max_continuity_spacing = value;
        //
        //	return rval;
        //}

        double get_DSDBR01_SMPF_max_mode_line_slope(int smNumber)
        {
            double maxModeLineSlope = 0;

            if (smNumber == 0)
                maxModeLineSlope = Convert.ToInt32(this.superMap0QASitting["MaxModeLineSlope"]);
            else if (smNumber == 1)
                maxModeLineSlope = Convert.ToInt32(this.superMap1QASitting["MaxModeLineSlope"]);
            else if (smNumber == 2)
                maxModeLineSlope = Convert.ToInt32(this.superMap2QASitting["MaxModeLineSlope"]);
            else if (smNumber == 3)
                maxModeLineSlope = Convert.ToInt32(this.superMap3QASitting["MaxModeLineSlope"]);
            else if (smNumber == 4)
                maxModeLineSlope = Convert.ToInt32(this.superMap4QASitting["MaxModeLineSlope"]);
            else if (smNumber == 5)
                maxModeLineSlope = Convert.ToInt32(this.superMap5QASitting["MaxModeLineSlope"]);
            else if (smNumber == 6)
                maxModeLineSlope = Convert.ToInt32(this.superMap6QASitting["MaxModeLineSlope"]);
            else if (smNumber == 7)
                maxModeLineSlope = Convert.ToInt32(this.superMap7QASitting["MaxModeLineSlope"]);
            else if (smNumber == 8)
                maxModeLineSlope = Convert.ToInt32(this.superMap8QASitting["MaxModeLineSlope"]);
            else if (smNumber == 9)
                maxModeLineSlope = Convert.ToInt32(this.superMap9QASitting["MaxModeLineSlope"]);

            return maxModeLineSlope;
        }


        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Drawing;
using System.Collections.Specialized;
//using Bookham.TestSolution.Instruments;
using Bookham.TestLibrary.Utilities;
using Bookham.TestLibrary.Algorithms;

using Bookham.TestLibrary.FcuCommonUtils;
using Bookham.TestLibrary.Instruments;
using Bookham.TestEngine.Equipment;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestEngine.PluginInterfaces.Module;


//coded by purney.xie
namespace Bookham.fcumapping.CommonData
{
    public class CDSDBROverallModeMap
    {
        #region Data members 
        // constant variable
        public const string FRONT_CURRENT_CSTRING = "If";
        public const string REAR_CURRENT_CSTRING = "Ir";
        public const string PHASE_CURRENT_CSTRING = "Ip";
        public const string MIDDLELINE_CURRENT_CSTRING = "Im";
        public const string  COMMA_CHAR = ",";

        public const string QA_METRICS_FILE_NAME	=	 "qaMetrics";
        public const string ASSFAIL_FILE_NAME		=	 "passFailMetrics";
        public const string COLLATED_PASSFAIL_FILE_NAME =	 "collatedPassFailResults";
        public const string LASER_TYPE_ID_DSDBR01_CSTRING = "DSDBR01";

        public const string  CSV = ".csv";
        public const string  USCORE = "_";
        public const string  MATRIX_ = "MATRIX_";
        public const string  MATRIX_FILTERED_ = "MATRIX_F_";
        public const string  JUMPS_ = "JUMPS_";
        public const string  LINKED_JUMPS_ = "JUMPS_L_";
        
        //get OverallMap method
        
        
        NameValueCollection OverallMapmethod = (NameValueCollection)SettingConfigure.cfgReader.GetSection("CloseGridSetting/OverAllMap/DataCollection");
        


        public List<List<double>> _map = new List<List<double>>();

        private List<List<int>> _jumpMap = new List<List<int>>();
        public List<List<int>> _jumpMap_copy;
        private List<double> _IRear = new List<double>();
        private List<Point_Type> _frontSection = new List<Point_Type>();
        private List<int> pairSwitch = new List<int>();
        private List<int> trace = new List<int>();
        private List<int> trace_copy;
        private List<List<Point>> eval = new List<List<Point>>();
        private List<List<PointF>> TJ = new List<List<PointF>>();                   //supermode middle lines
        private List<List<double>> super_width_interp = new List<List<double>>();   //supermode width

        private List<List<Point_Type>> _iMs = new List<List<Point_Type>>();         // supermode middle lines points

        int Overall_Map_Bias_SOA;

        int _cols;
        int _rows;
        private const double NUM_SUPER = 0;

        // vector contains supermodes found in map
        List<CDSDBRSuperMode> _supermodes = new List<CDSDBRSuperMode>();

        // count of the number of supermodes removed because of flaws encountered
        int _supermodes_removed;

        bool _maps_loaded = false;
        bool _Ir_ramp = true; // // true for Ir ramp, false for If ramp

        // The above CLaserModeMap objects can contain
        // rear ramped or front ramped maps and the members
        // _rows and _cols in those objects change accordingly.
        // However, for the members _rows and _cols in this object
        // _rows always refers to the number of front current points and
        // _cols always refers to the number of rear current points.
        // As such, the overall mode map is always viewed the same way
        // independent of which ramp direction was used to gather the maps.

        int _xAxisLength;
        int _yAxisLength;

        //double _threshold_maxDeltaPr;

        private string _results_dir_string;
        private string _laser_id_string;
        private string _date_time_stamp_string;
        private string _overallMap_PowerRatio_file_path;
        private string _overallMap_Reference_file_path;
        private string _overallMap_Filter_file_path;
        private string _overallMap_Tx_file_path;
        private string _overallMap_Rx_file_path;

        CCurrentsVector _vector_Ir = new CCurrentsVector();
        CDSDBRFrontCurrents _vector_If = new CDSDBRFrontCurrents();

        OverallModeMapAnalysis _overallModeMapAnalysis = new OverallModeMapAnalysis();

        //void setContinuityValues();
        Vector _continuityValues = new Vector();

        List<double> _cacheMap = new List<double>();

        public Inst_Fcu2Asic _FCU = null;

        public string Rear_RearSoa_Switch = "";

        int Overall_Map_Sweep_Irear_Delay_ms, Overall_Map_Sweep_Irear_jump_Delay_ms, Overall_Map_Sweep_Average_Count, Overall_Map_Sweep_Irear_jump_delayPointCount;

        #endregion
        #region public memebers
        private int sm_num = 0;
        public int SM_NUM
        {
            get { return sm_num; }
        }
     


        public CDSDBROverallModeMap(Inst_Fcu2Asic instFcu,string result_Dir, string laser_ID,string date_Time,InstrumentCollection instruments)
        {
            this._FCU = instFcu;
            this._results_dir_string = result_Dir;
            this._laser_id_string = laser_ID;
            this._date_time_stamp_string = date_Time;
            Overall_Map_Bias_SOA = Convert.ToInt32(OverallMapmethod["GetOverallMapDataFromBiasSOA"].ToString());
        }
        
        public void CollectData()
        {
            // sweep and get data

            string rampDir = "_" + OverallMapmethod["RampDirection"].ToString();//"R":mean Irear sweep    "F":means Ifront sweep
            string sourceFilePath, overallMap_file_path_PR, sourceFilePath_PR, sweepdirection = "";
            Overall_Map_Sweep_Irear_Delay_ms = Convert.ToInt32(OverallMapmethod["Measure Delay Irear ms"].ToString());
            Overall_Map_Sweep_Irear_jump_Delay_ms = Convert.ToInt32(OverallMapmethod["Measure Delay Irear Jump ms"].ToString());
            Overall_Map_Sweep_Average_Count = Convert.ToInt32(OverallMapmethod["Measure Average Count"].ToString());
            Overall_Map_Sweep_Irear_jump_delayPointCount = Convert.ToInt32(OverallMapmethod["Irear Jump Delay Point Count"].ToString());

            sourceFilePath = _FCU.OverallmapSync(Inst_Fcu2Asic.SweepDirection.f, Overall_Map_Sweep_Irear_Delay_ms, Overall_Map_Sweep_Irear_jump_Delay_ms, Overall_Map_Sweep_Average_Count,Overall_Map_Sweep_Irear_jump_delayPointCount, Inst_Fcu2Asic.GetOverallMapDataFrom.Power_Ratio, Rear_RearSoa_Switch);
            sweepdirection = "Forward_";
            _overallMap_PowerRatio_file_path = _results_dir_string + "\\" + MATRIX_ + "PowerRatio_" + LASER_TYPE_ID_DSDBR01_CSTRING + "_" + _laser_id_string + "_" + _date_time_stamp_string + "_DFS";
            _overallMap_Reference_file_path = _results_dir_string + "\\" + MATRIX_ + "Reference_" + LASER_TYPE_ID_DSDBR01_CSTRING + "_" + _laser_id_string + "_" + _date_time_stamp_string + "_DFS";
            _overallMap_Filter_file_path = _results_dir_string + "\\" + MATRIX_ + "Filter_" + LASER_TYPE_ID_DSDBR01_CSTRING + "_" + _laser_id_string + "_" + _date_time_stamp_string + "_DFS";
            _overallMap_Tx_file_path = _results_dir_string + "\\" + MATRIX_ + "Tx_" + LASER_TYPE_ID_DSDBR01_CSTRING + "_" + _laser_id_string + "_" + _date_time_stamp_string + "_DFS";
            _overallMap_Rx_file_path = _results_dir_string + "\\" + MATRIX_ + "Rx_" + LASER_TYPE_ID_DSDBR01_CSTRING + "_" + _laser_id_string + "_" + _date_time_stamp_string + "_DFS";
            for (int N = 0; N <= 7; N++)
            {

                if (N == 0)
                {
                    sourceFilePath_PR = Path.Combine(sourceFilePath, sweepdirection + "PowerRatio.csv");
                    overallMap_file_path_PR = _overallMap_PowerRatio_file_path + rampDir + ".csv";
                }
                else
                {
                    sourceFilePath_PR = Path.Combine(sourceFilePath, sweepdirection + "PowerRatio" + "_" + N.ToString() + ".csv");
                    overallMap_file_path_PR = _overallMap_PowerRatio_file_path + N.ToString() + rampDir + ".csv";
                }
                File.Copy(sourceFilePath_PR, overallMap_file_path_PR, true);
                File.SetAttributes(overallMap_file_path_PR, FileAttributes.Normal);

                if (N == 0)
                {
                    sourceFilePath_PR = Path.Combine(sourceFilePath, sweepdirection + "Reference.csv");
                    overallMap_file_path_PR = _overallMap_Reference_file_path + rampDir + ".csv";
                }
                else
                {
                    sourceFilePath_PR = Path.Combine(sourceFilePath, sweepdirection + "Reference" + "_" + N.ToString() + ".csv");
                    overallMap_file_path_PR = _overallMap_Reference_file_path + N.ToString() + rampDir + ".csv";
                }
                File.Copy(sourceFilePath_PR, overallMap_file_path_PR, true);
                File.SetAttributes(overallMap_file_path_PR, FileAttributes.Normal);

                if (N == 0)
                {
                    sourceFilePath_PR = Path.Combine(sourceFilePath, sweepdirection + "Filter.csv");
                    overallMap_file_path_PR = _overallMap_Filter_file_path + rampDir + ".csv";
                }
                else
                {
                    sourceFilePath_PR = Path.Combine(sourceFilePath, sweepdirection + "Filter" + "_" + N.ToString() + ".csv");
                    overallMap_file_path_PR = _overallMap_Filter_file_path + N.ToString() + rampDir + ".csv";
                }
                File.Copy(sourceFilePath_PR, overallMap_file_path_PR, true);
                File.SetAttributes(overallMap_file_path_PR, FileAttributes.Normal);

                if (N == 0)
                {
                    sourceFilePath_PR = Path.Combine(sourceFilePath, sweepdirection + "Tx.csv");
                    overallMap_file_path_PR = _overallMap_Tx_file_path + rampDir + ".csv";
                }
                else
                {
                    sourceFilePath_PR = Path.Combine(sourceFilePath, sweepdirection + "Tx" + "_" + N.ToString() + ".csv");
                    overallMap_file_path_PR = _overallMap_Tx_file_path + N.ToString() + rampDir + ".csv";
                }
                File.Copy(sourceFilePath_PR, overallMap_file_path_PR, true);
                File.SetAttributes(overallMap_file_path_PR, FileAttributes.Normal);

                if (N == 0)
                {
                    sourceFilePath_PR = Path.Combine(sourceFilePath, sweepdirection + "Rx.csv");
                    overallMap_file_path_PR = _overallMap_Rx_file_path + rampDir + ".csv";
                }
                else
                {
                    sourceFilePath_PR = Path.Combine(sourceFilePath, sweepdirection + "Rx" + "_" + N.ToString() + ".csv");
                    overallMap_file_path_PR = _overallMap_Rx_file_path + N.ToString() + rampDir + ".csv";
                }
                File.Copy(sourceFilePath_PR, overallMap_file_path_PR, true);
                File.SetAttributes(overallMap_file_path_PR, FileAttributes.Normal);


            }
            sourceFilePath_PR = Path.Combine(sourceFilePath, sweepdirection + "Filter.csv");
            overallMap_file_path_PR = _results_dir_string + "\\" + MATRIX_ + "Filter_" + LASER_TYPE_ID_DSDBR01_CSTRING + "_" + _laser_id_string + "_" + _date_time_stamp_string + "_DFS_R.csv";
            File.Copy(sourceFilePath_PR, overallMap_file_path_PR, true);
            File.SetAttributes(overallMap_file_path_PR, FileAttributes.Normal);

            sourceFilePath_PR = Path.Combine(sourceFilePath, sweepdirection + "Reference.csv");
            overallMap_file_path_PR = _results_dir_string + "\\" + MATRIX_ + "Reference_" + LASER_TYPE_ID_DSDBR01_CSTRING + "_" + _laser_id_string + "_" + _date_time_stamp_string + "_DFS_R.csv";
            File.Copy(sourceFilePath_PR, overallMap_file_path_PR, true);
            File.SetAttributes(overallMap_file_path_PR, FileAttributes.Normal);
            
            Directory.Delete(sourceFilePath, true);//del the folder to save disk space. Jack.zhang 2011-12-05

                sourceFilePath = Environment.CurrentDirectory + "\\Data";

                sourceFilePath_PR = Path.Combine(sourceFilePath, "If_OverallMap_DSDBR01_default.csv");
                overallMap_file_path_PR = _results_dir_string + "\\If_OverallMap_" + LASER_TYPE_ID_DSDBR01_CSTRING + "_" + _laser_id_string + "_" + _date_time_stamp_string + ".csv";
                File.Copy(sourceFilePath_PR, overallMap_file_path_PR, true);
                File.SetAttributes(overallMap_file_path_PR, FileAttributes.Normal);

            sourceFilePath_PR = Path.Combine(sourceFilePath, "Ir_OverallMap_DSDBR01_default.csv");
            overallMap_file_path_PR = _results_dir_string + "\\Ir_OverallMap_" + LASER_TYPE_ID_DSDBR01_CSTRING + "_" + _laser_id_string + "_" + _date_time_stamp_string + ".csv";
            File.Copy(sourceFilePath_PR, overallMap_file_path_PR, true);
            File.SetAttributes(overallMap_file_path_PR, FileAttributes.Normal);
        }
        private void init()
        {
            _cols = 0;
            _rows = 0;

            // initialize pairswitch
            pairSwitch.Add(0);
            pairSwitch.Add(21);
            pairSwitch.Add(42);
            pairSwitch.Add(63);
            pairSwitch.Add(84);
            pairSwitch.Add(105);
            pairSwitch.Add(126);
            pairSwitch.Add(147);


            LoadMapIrIfFromFile();
            //initialize jumps map
            for (int i = 0; i < _rows; i++)
            {
                List<int> aRow = new List<int>();
                for (int j = 0; j < _cols; j++)
                {
                    aRow.Add(0);
                }
                _jumpMap.Add(aRow);
            }
        }
        private void LoadMapFromFile(string fileFullPath)
        {

            if (File.Exists(fileFullPath))
            {
                Bookham.TestLibrary.Utilities.CsvReader csvReader = new Bookham.TestLibrary.Utilities.CsvReader();
                List<string[]> result = csvReader.ReadFile(fileFullPath);
                for (int i = 0; i < result.Count; i++)
                {
                    List<double> temp = new List<double>();
                    for (int j = 0; j < result[i].Length; j++)
                    {
                        double value = Convert.ToDouble(result[i][j].ToString().Trim());
                        value /= 10000;
                        temp.Add(value);
                    }
                    _map.Add(temp);
                }
                _cols = result[0].Length;
                _rows = result.Count;
            }
            else
            {
                throw new FileNotFoundException(fileFullPath+" not found.");
            }
        }
        private void LoadIRFromfFile(string fileFullPath)
        {
            if (File.Exists(fileFullPath))
            {
                Bookham.TestLibrary.Utilities.CsvReader csvReader = new Bookham.TestLibrary.Utilities.CsvReader();
                List<string[]> result = csvReader.ReadFile(fileFullPath);
                for (int i = 0; i < result.Count; i++)
                {
                    double value = Convert.ToDouble(result[i][0].ToString().Trim());
                    _IRear.Add(value);
                }

            }
            else
            {
                throw new FileNotFoundException(fileFullPath + " not found.");
            }
        }
        private void LoadIFrontFromfFile(string fileFullPath)
        {
            List<double> tempList = new List<double>();
            if (File.Exists(fileFullPath))
            {
                Bookham.TestLibrary.Utilities.CsvReader csvReader = new Bookham.TestLibrary.Utilities.CsvReader();
                List<string[]> result = csvReader.ReadFile(fileFullPath);
                for (int i = 0; i < result.Count; i++)
                {
                    double value = Convert.ToDouble(result[i][0].ToString().Trim());
                    tempList.Add(value);
                }
            }
            else
            {
                throw new FileNotFoundException(fileFullPath + " not found.");
            }
            for (int pairNum = 0; pairNum < 7; pairNum++)
            {
                for (int i = 0; i < tempList.Count; i++)
                {
                    Point_Type newPoint = new Point_Type();
                    newPoint.front_pair_number = (short)(pairNum + 1);
                    newPoint.constantCurrent = 5.0;
                    newPoint.nonConstantCurrent = tempList[i];
                    newPoint.rear = 0;
                    _frontSection.Add(newPoint);
                }
            }
        }
        private void GenerateSuperModeMilldeLine(List<List<int>> mat)
        {
            // point.x = 101,.....107, point.y = meanofrows(mat,i)
            List<Point> pos = new List<Point>();
            int maxValue = MaxInMap(mat);
            for (int i = 101; i <= maxValue; i++)
            {
                Point newPoint = new Point();
                newPoint.X = i;
                newPoint.Y = MeanofRows(mat, i);
                pos.Add(newPoint);
            }
            pos.Sort(new PairComparer()); //by ascent
            for (int i = 0; i < pos.Count; i++)
            {
                for (int row = 0; row < mat.Count; row++)
                {
                    for (int col = 0; col < mat[0].Count; col++)
                    {
                        if (mat[row][col] == pos[i].X)
                        {
                            mat[row][col] = i + 1;
                        }
                    }
                }
            }
            printFile<int>(mat, _laser_id_string, "realMap");

            PolyFitBoundaries(mat);

            bool loadMiddleLineFromFile = false; //jack.Zhang for FP switch simulation 
            string middleLineFileName = "";
            if (loadMiddleLineFromFile)
            {
                string fileName = "lines_OverallMap_DSDBR01_" + _laser_id_string + "_" + _date_time_stamp_string + ".csv";
                middleLineFileName = _results_dir_string + "\\" + fileName;
                LoadMiddleLineFileFromFile(middleLineFileName);
            }
            else
            {
                printfMiddleLines(TJ, _laser_id_string, "lines_OverallMap_DSDBR01_");
            }

            GetIMCurrents();
        }
        private void PolyFitBoundaries(List<List<int>> mat)
        {
            List<double> wall_x_start = new List<double>();
            List<double> wall_x_stop = new List<double>();
            List<double> wall_x_span = new List<double>();
            List<double> wall_x_median = new List<double>();
            List<double> wall_x_mean = new List<double>();

            List<double> wall_y_start = new List<double>();
            List<double> wall_y_stop = new List<double>();
            List<double> wall_y_span = new List<double>();
            List<double> wall_y_median = new List<double>();
            List<double> wall_y_mean = new List<double>();

            List<int> wall_num_points = new List<int>();

            int maxValue = MaxInMap(mat);

            for (int i = 1; i <= maxValue; i++)
            {
                List<Point> XY = FindPointInMap(mat, i);
                List<int> X = new List<int>();
                List<int> Y = new List<int>();
                for (int j = 0; j < XY.Count; j++)
                {
                    X.Add(XY[j].X);
                    Y.Add(XY[j].Y);
                }
                wall_x_start.Add(Min(Y));
                wall_x_stop.Add(Max(Y));
                wall_x_span.Add(wall_x_stop[i - 1] - wall_x_start[i - 1]);
                wall_x_median.Add(Median(Y));
                wall_x_mean.Add(Mean(Y));

                wall_y_start.Add(Min(X));
                wall_y_stop.Add(Max(X));
                wall_y_span.Add(wall_y_stop[i - 1] - wall_y_start[i - 1]);
                wall_y_median.Add(Median(X));
                wall_y_mean.Add(Mean(X));

                List<double> xVector = new List<double>();
                for (int k = 0; k < XY.Count; k++)
                {
                    xVector.Add((double)XY[k].X);
                }
                List<double> yVector = new List<double>();
                for (int k = 0; k < XY.Count; k++)
                {
                    yVector.Add((double)XY[k].Y);
                }
                int fitorder = 1;

                PolyFit polyFit = Alg_PolyFit.PolynomialFit(xVector.ToArray(), yVector.ToArray(), fitorder);

                double[] coeffs = polyFit.Coeffs;
                if (coeffs.Length > 0)
                {
                    List<Point> tempList = new List<Point>();
                    for (int x = 0; x < 201; x++)
                    {
                        Point newPoint = new Point();
                        double y = 0;
                        for (int k = 0; k <= fitorder; k++)
                        {
                            y += coeffs[k] * Math.Pow(x, k);
                        }
                        newPoint.X = x;// (int)xVector[w];

                        y = y - 21;

                        if (y < 0)
                        {
                            y = 0;
                        }
                        if (y > 146)
                        {
                            y = 146;
                        }

                        newPoint.Y = (int)y;// submit the addad 21

                        tempList.Add(newPoint);

                    }
                    eval.Add(tempList);
                }
            }

            printfBoundaries(eval,_laser_id_string , "boundary_");
            List<Point> WL;
            List<Point> WR;
            for (int i = 1; i < maxValue; i++)
            {
                WL = eval[i - 1];
                WR = eval[i];
                List<PointF> middlePoint = new List<PointF>();
                List<double> superwidth = new List<double>();
                for (int j = 0; j < WL.Count && j < WR.Count; j++)
                {
                    PointF newPoint = new PointF();
                    newPoint.X = j;
                    newPoint.Y = WL[j].Y + (WR[j].Y - WL[j].Y) / 2;
                    middlePoint.Add(newPoint);
                    superwidth.Add(WR[j].Y - WL[j].Y);
                }
                TJ.Add(middlePoint);
                super_width_interp.Add(superwidth);
            }

        }
        private void GetIMCurrents()
        {
            // Get IM currents
            for (int i = 0; i < TJ.Count; i++)
            {
                List<Point_Type> tempList = new List<Point_Type>();
                for (int j = 0; j < TJ[i].Count; j++)
                {
                    Point_Type newPoint = new Point_Type();
                    PointF pointF = TJ[i][j];
                    newPoint.col = j;
                    newPoint.row = i;
                    newPoint.rear = _IRear[(int)pointF.X];
                    newPoint.front_pair_number = _frontSection[(int)pointF.Y].front_pair_number;
                    newPoint.constantCurrent = _frontSection[(int)pointF.Y].constantCurrent;
                    newPoint.nonConstantCurrent = _frontSection[(int)pointF.Y].nonConstantCurrent;
                    tempList.Add(newPoint);
                }
                _iMs.Add(tempList);
            }

            // output csv file
            for (int i = 0; i < _iMs.Count; i++)
            {
                printfIMFile(_iMs[i], _laser_id_string, "Im_", i);
            }
            sm_num = _iMs.Count;

        }

        private void LoadMiddleLineFileFromFile(string ImFullFileName)
        {
            Bookham.TestLibrary.Utilities.CsvReader csvReader = new Bookham.TestLibrary.Utilities.CsvReader();
            List<string[]> list = null;
            List<List<PointF>> middleLines = new List<List<PointF>>();
            if (File.Exists(ImFullFileName))
            {
                list = csvReader.ReadFile(ImFullFileName);
                int supermodeNum = 0;
                int tempNum = 0;
                for (int i = 1; i < list.Count; i++)
                {
                    tempNum = Convert.ToInt32(list[i][0]);
                    if (tempNum > supermodeNum)
                    {
                        supermodeNum = tempNum;
                    }
                }
                for (int i = 0; i <= supermodeNum; i++)
                {
                    List<PointF> tempList = new List<PointF>();
                    PointF point;
                    for (int j = 1; j < list.Count; j++)
                    {
                        tempNum = Convert.ToInt32(list[j][0]);
                        if (tempNum == i)
                        {
                            point = new PointF();
                            point.X = Convert.ToInt32(list[j][2]);
                            point.Y = Convert.ToInt32(list[j][1]);
                            tempList.Add(point);
                        }
                    }
                    middleLines.Add(tempList);
                }
                TJ = middleLines; 
            }
            else
            {
                throw new FileNotFoundException(ImFullFileName + " not found.");
            }

        }

        private void printfIMFile(List<Point_Type> im, string id, string pre, int smNum)
        {

            string timeStamp = _date_time_stamp_string;
            string fileName = pre + "DSDBR01_" + id + "_" + timeStamp + "_SM" + smNum + ".csv";
            string filePath = _results_dir_string + "\\" + fileName;
            if (!File.Exists(filePath))
            {
                File.Delete(filePath);
                using (StreamWriter sw = new StreamWriter(filePath))
                {
                    sw.WriteLine("Rear Current [mA],Front Pair Number,Constant Front Current [mA],Non-constant Front Current [mA]");
                    for (int i = 0; i < im.Count; i++)
                    {
                        Point_Type point = im[i];
                        sw.WriteLine(point.rear + "," + point.front_pair_number + "," + point.constantCurrent + "," + point.nonConstantCurrent);
                    }
                }
            }
        }
        private int Min(List<int> list)
        {
            int minValue = list[0];
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i] < minValue)
                {
                    minValue = list[i];
                }
            }
            return minValue;
        }
        private int Max(List<int> list)
        {
            int maxValue = list[0];
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i] > maxValue)
                {
                    maxValue = list[i];
                }
            }
            return maxValue;
        }
        private double Mean(List<int> list)
        {
            double sum = 0;
            for (int i = 0; i < list.Count; i++)
            {
                sum += list[i];

            }
            return sum / list.Count;
        }
        private int Median(List<int> list)
        {
            int returnValue;
            list.Sort();
            if ((list.Count / 2) == 0)
            {  // inthe mean of midee intow value
                int first = list[(list.Count / 2) - 1];
                int second = list[list.Count / 2];
                returnValue = (first + second) / 2;
            }
            else
            {
                returnValue = list[list.Count / 2];
            }

            return returnValue;
        }
        private List<Point> FindPointInMap(List<List<int>> mat, int value)
        {
            List<Point> returnList = new List<Point>();
            for (int row = 0; row < mat.Count; row++)
            {
                for (int col = 0; col < mat[0].Count; col++)
                {
                    if (mat[row][col] == value)
                    {
                        Point newPoint = new Point();
                        newPoint.X = col;
                        newPoint.Y = row;
                        returnList.Add(newPoint);
                    }
                }
            }
            return returnList;
        }
        private int MaxInMap(List<List<int>> mat)
        {
            int maxValue = 0;
            for (int row = 0; row < mat.Count; row++)
            {
                for (int col = 0; col < mat[0].Count; col++)
                {
                    if (mat[row][col] > maxValue)
                    {
                        maxValue = mat[row][col];
                    }
                }
            }
            return maxValue;
        }
        private int MeanofRows(List<List<int>> mat, int boundaryNum)
        {
            List<Point> list = new List<Point>();
            for (int i = 0; i < mat.Count; i++)
            {
                for (int j = 0; j < mat[0].Count; j++)
                {
                    if (mat[i][j] == boundaryNum)
                    {
                        Point newPoint = new Point();
                        newPoint.X = j;
                        newPoint.Y = i;
                        list.Add(newPoint);
                    }
                }
            }
            int sum = 0;
            for (int i = 0; i < list.Count; i++)
            {
                sum += list[0].Y;
            }
            double returnValue = sum / list.Count;
            return (int)returnValue;
        }
        public void FindBoundaries(List<int> trace, List<List<int>> mat)
        {
            int z = 101;
            while (FindTraceGreatThreshold(trace, 40) == true)
            {
                int L1, L2, peak;
                FindBoundaryIndex(trace, out L1, out L2, out peak);
                if (Math.Abs(L2 - L1) > 25)
                {
                    List<int> t = new List<int>(trace);
                    if (peak - L1 <= L2 - peak)
                    {
                        ClearList(t, 0, peak);
                        ClearList(t, L2, t.Count - 1);
                        List<int> d = Diff(t);
                        ClearList(d, 0, peak);
                        int L2_backup = L2;
                        if (FindTraceGreatThreshold(d, 0) == false)
                        {
                            L2 = L2_backup;
                        }
                        else
                        {
                            for (int i = 0; i < d.Count; i++)
                            {
                                if (d[i] > 0)
                                {
                                    L2 = i;
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        List<int> d = Diff(t);
                        L1 = peak - 1;
                        while (d[L1] > 0 && trace[L1] != int.MinValue)
                        {
                            L1 = L1 - 1;
                        }
                        L1 = L1 + 1;
                    }
                }
                for (int i = 0; i < trace.Count; i++)
                {
                    if (i >= L1 && i <= L2)
                    {
                        trace[i] = int.MinValue;
                    }
                }
                int width = L2 - L1;
                int offset = 3;
                SetBoundaryTag(L1 + offset, mat, width, z);
                z = z + 1;

            }


        }
        private List<int> Diff(List<int> vector)
        {
            List<int> returnList = new List<int>();
            for (int i = 0; i < vector.Count - 1; i++)
            {
                returnList.Add(vector[i + 1] - vector[i]);
            }
            return returnList;

        }
        private void ClearList(List<int> list, int start, int end)
        {
            for (int i = 0; i < list.Count; i++)
            {
                if (i >= start && i <= end)
                {
                    list[i] = 0;
                }
            }


        }
        private void FindBoundaryIndex(List<int> trace, out int L1, out int L2, out int P)
        {
            //2009-12-17 impact on S/N
            int th = 5;// 5;
            int peak = Trace_Max(trace);
            P = peak;
            List<int> z = FindLessthreshold(trace, th);

            int f = 0;
            for (int i = 0; i < z.Count; i++)
            {
                if (z[i] > peak)
                {
                    f = i;
                    break;
                }
            }
            int v = f - 1;
            if (v > 0)
            {
                L1 = z[v];
            }
            else
            {
                //L1 = 1; for different start index between matlab and C#
                L1 = 0;
            }

            L2 = z[f];

        }
        private List<int> FindLessthreshold(List<int> trace, int threshold)
        {
            List<int> returnList = new List<int>();
            for (int i = 0; i < trace.Count; i++)
            {
                if (trace[i] < threshold)
                {
                    returnList.Add(i);
                }
            }
            return returnList;

        }
        private int Trace_Max(List<int> trace)
        {
            int maxIndex = 0;
            int maxValue = trace[0];
            for (int i = 0; i < trace.Count; i++)
            {
                if (trace[i] > maxValue)
                {
                    maxValue = trace[i];
                    maxIndex = i;
                }
            }
            return maxIndex;
        }
        private bool FindTraceGreatThreshold(List<int> trace, int th)
        {
            bool returnValue = false;
            for (int i = 0; i < trace.Count; i++)
            {
                if (trace[i] > th)
                {
                    returnValue = true;
                    break;
                }
            }
            return returnValue;
        }
        public void FindJumpsMap()
        {
           
            //find a max in each submap in each col
            for (int col = 0; col < 201; col++)
            {
                int pair = 1;
                for (int row = 0; row < 147; row += 21)
                {
                    //int maxValue = _map[i][j];
                    double maxValue = 0;
                    int row_maxValue = row;
                    int col_maxValue = col;
                    for (int subrow = row; subrow < pairSwitch[pair]; subrow++)
                    {
                        if (_map[subrow][col] > maxValue)
                        {
                            maxValue = _map[subrow][col];
                            row_maxValue = subrow;
                            col_maxValue = col;
                        }
                    }
                    _jumpMap[row_maxValue][col_maxValue] = -1;
                    pair++;
                }
            }



        }
        /// <summary>
        /// calculate
        /// </summary>
        /// <param name="x"></param>
        /// <param name="mat"></param>
        /// <param name="width"></param>
        /// <returns></returns>
        public int CalJumps(int x, List<List<int>> mat, int width)
        {
            int gap = width;
            List<int> X = new List<int>();
            for (int i = x; i <= x + 21 + gap; i++)
            {
                X.Add(i);
            }
            List<double> y1 = feval(X, x);
            List<double> y2 = feval(X, x + gap);
            List<int> T = new List<int>();
            for (int i = 0; i < X.Count; i++)
            {
                if (X[i] < mat.Count)
                {
                    List<int> row = mat[X[i]];
                    int rowCount = 0;
                    for (int j = 0; j < row.Count; j++)
                    {
                        //if (row[j] == -1 && X[i] < y1[i] && X[i] > y2[i])
                        if (row[j] == -1 && j < y1[i] && j > y2[i])
                        {
                            rowCount++;
                        }
                    }
                    T.Add(rowCount);
                }
            }
            int returnValue = 0;
            for (int i = 0; i < T.Count; i++)
            {
                returnValue += T[i];
            }

            return returnValue;


        }
        public void SetBoundaryTag(int x, List<List<int>> mat, int width, int tag)
        {
            int gap = width;
            List<int> X = new List<int>();
            for (int i = x; i <= x + 21 + gap; i++)
            {
                X.Add(i);
            }
            List<double> y1 = feval(X, x);
            List<double> y2 = feval(X, x + gap);
            for (int i = 0; i < X.Count; i++)
            {
                if (X[i] < mat.Count)
                {
                    List<int> row = mat[X[i]];
                    for (int j = 0; j < row.Count; j++)
                    {
                        if (row[j] == -1 && j < y1[i] && j > y2[i])
                        {
                            mat[X[i]][j] = tag;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Get a set value, y=200/21*x+1-200/21*x0;
        /// </summary>
        /// <param name="x">x is a vector</param>
        /// <param name="x0"></param>
        /// <returns></returns>
        public List<double> feval(List<int> x, int x0)
        {
            List<double> returnList = new List<double>();
            for (int i = 0; i < x.Count; i++)
            {
                double value = (200 / 21.0) * x[i] + 1 - (200 / 21.0) * x0;//2009-12-18 change 200/21=9 to 200/21.0=9.523
                returnList.Add(value);
            }
            return returnList;
        }
        public void printTrace<T>(List<T> list, string pre)
        {

            string fileName = pre + _laser_id_string + "_" + _date_time_stamp_string + ".csv";
            string filePath = _results_dir_string + "\\" + fileName;
            if (!File.Exists(filePath))
            {
                File.Delete(filePath);
                using (StreamWriter sw = new StreamWriter(filePath))
                {
                    for (int i = 0; i < list.Count; i++)
                    {
                        sw.WriteLine(list[i]);
                    }
                }
            }

        }
        private void printfBoundaries(List<List<Point>> boundaries, string id, string pre)
        {

            string timeStamp = _date_time_stamp_string;
            string fileName = pre + id + "_" + timeStamp + ".csv";
            string filePath = _results_dir_string + "\\" + fileName;
            if (!File.Exists(filePath))
            {
                File.Delete(filePath);
                using (StreamWriter sw = new StreamWriter(filePath))
                {
                    for (int i = 0; i < boundaries.Count; i++)
                    {
                        for (int j = 0; j < boundaries[i].Count; j++)
                        {
                            Point temp = boundaries[i][j];
                            sw.WriteLine("{0},{1}", temp.X, temp.Y);
                        }
                    }
                }
            }
        }
        private void printfMiddleLines(List<List<PointF>> map, string id, string pre)
        {

            string timeStamp = _date_time_stamp_string;
            string fileName = pre + id + "_" + timeStamp + ".csv";
            string filePath = _results_dir_string + "\\" + fileName;
            if (!File.Exists(filePath))
            {
                File.Delete(filePath);
                using (StreamWriter sw = new StreamWriter(filePath))
                {
                    sw.WriteLine("SM#,Row,Col");
                    for (int i = 0; i < map.Count; i++)
                    {
                        for (int j = 0; j < map[i].Count; j++)
                        {
                            PointF temp = map[i][j];
                            sw.WriteLine("{0},{1},{2}", i, temp.Y, temp.X);
                        }
                    }
                }
            }

        }
        public void printFile<T>(List<List<T>> map, string id, string pre)
        {
            WreiteMapToFile(pre, map, _results_dir_string, id, _date_time_stamp_string);
        }
        public void WreiteMapToFile<T>(string pre, List<List<T>> map, string dir, string id, string timeStamp)
        {
            string fileName = pre + id + "_" + timeStamp + ".csv";
            string filePath = _results_dir_string + "\\" + fileName;
            if (!File.Exists(filePath))
            {
                File.Delete(filePath);
                using (StreamWriter sw = new StreamWriter(filePath))
                {
                    for (int i = 0; i < map.Count; i++)
                    {
                        for (int j = 0; j < map[0].Count; j++)
                        {
                            sw.Write(map[i][j]);
                            sw.Write(",");
                        }
                        sw.WriteLine();
                    }
                }
            }
        }
        private void LoadMapIrIfFromFile()
        {
            string currentDir = Directory.GetCurrentDirectory();
            string mapPath = _overallMap_PowerRatio_file_path;
            LoadMapFromFile(mapPath);
            printFile(_map, _laser_id_string, "OverallMap_");
            string IrPath = currentDir + @"\\Plugins\solution\FCUMapping\FCUCal\rtCalidata.csv";
            LoadIRFromfFile(IrPath);
            string IFPath = currentDir + @"\\Plugins\solution\FCUMapping\FCUCal\ftCalidata.csv";
            LoadIFrontFromfFile(IFPath);

        }

        private void setContinuityValues()
        {
            // stub here
            //_continuityValues.clear();
            //for (int sm = 0; sm < _supermodes.Count; sm++)
            //{
            //    std::vector<CDSDBROverallModeMapLine::point_type>* p_filtered_middle_points = &(_supermodes[sm]._filtered_middle_line._points);

            //    if (!(p_filtered_middle_points->empty()))
            //    {
            //        for (long i = 0; i < (long)(p_filtered_middle_points->size()); i++)
            //        {
            //            long row_i = ((*p_filtered_middle_points)[i]).row;
            //            long col_i = ((*p_filtered_middle_points)[i]).col;
            //            double power_ratio = *iPowerRatioPt(row_i, col_i);

            //            _continuityValues.push_back(power_ratio);
            //        }
            //    }
            //}

        }
#if _debug
         public void loadIFAndIRFromFile(string results_dir_string, string if_FileName, string ir_FileName)
        {
            // Read the overall map from file.
            // The overall map is stored in 7 files named
            // MATRIX_PowerRatio_DSDBR[##]_[laser_ID]_[YYYYMMDDHHMMSS]_DFS[i]_[R|F].csv
            // where i is from 1 to 7
            //
            // The currents used to collect these maps are stored in 2 files
            // If_OverallMap_DSDBR[##]_[laser_ID]_[YYYYMMDDHHMMSS].csv
            // Ir_OverallMap_DSDBR[##]_[laser_ID]_[YYYYMMDDHHMMSS].csv
            // where If is front sections' current
            // and Ir is rear (reflector section current)

            // first, clear anything previously collected/loaded
            clear();

            string file_path = results_dir_string;
            //CString date_time_stamp_CString = _T("*");


            // Check for directory without trailing '\\' character
            if (file_path[file_path.Length - 1] != '\\')
            {
                // Add trailing directory backslash '\\'
                file_path += "\\";
            }

            string If_file_path = file_path;
            If_file_path += if_FileName;
            //If_file_path += FRONT_CURRENT_CSTRING;
            //If_file_path +=  "_OverallMap_" ;
            //If_file_path += LASER_TYPE_ID_DSDBR01_CSTRING;
            //If_file_path += USCORE;
            //If_file_path += laser_id_string;
            //If_file_path += USCORE;
            //If_file_path += date_time_stamp_string;
            If_file_path += CSV;

            string Ir_file_path = file_path;
            Ir_file_path += ir_FileName;
            //Ir_file_path += REAR_CURRENT_CSTRING;
            //Ir_file_path += CString( "_OverallMap_" );
            //Ir_file_path += LASER_TYPE_ID_DSDBR01_CSTRING;
            //Ir_file_path += USCORE;
            //Ir_file_path += laser_id_CString;
            //Ir_file_path += USCORE;
            //Ir_file_path += date_time_stamp_CString;
            Ir_file_path += CSV;


            // load data from files
            _vector_Ir.readFromFile(Ir_file_path);

            _vector_If.readFromFile(If_file_path);

        }
        
        private void findSMBoundaries()
        {
        
        }
        private void findSMMiddleLines()
        {
            // NOTE: Whereas the supermode boundary lines can effectively be
            // a line zigzagging all over the place up/down, left/right,
            // more restrictions must exist on the middle line.
            // Each point must map directly onto the Ir ramp.
            // Therefore, the size of the middle line is the same as Ir ramp
            // unless the middle line hits the top|bottom instead of lhs|rhs

            int min_dist_of_middle_line_from_boundary = Convert.ToInt32(OverallMapSettings["MinNumberofPointsWidthofASupermode"]) / 2;

            for (int sm_number = 0; sm_number < _supermodes.Count; sm_number++)
            {
                // clear any middle line data
                _supermodes[sm_number]._middle_line.clear();
                bool middle_point_found_at_col_zero = false;
                bool middle_point_found_at_last_col = false;

                // if right-most column covered is not last column in map
                //   find midpoint on rhs-top border
                //   plot line from right-most point found to midpoint
                //   add points along line to middle line
                // if left-most column covered is not first column in map
                //   find midpoint on lhs_bottom border
                //   plot line from left-most point found to lhs_bottom border
                //   add points along line to middle line

                for (int i = 0; i < _cols; i++)
                {
                    // for each column

                    int highest_row_at_i_on_lower_line = -1;
                    int index_of_highest_row_at_i_on_lower_line = -1;

                    // find highest point on lower line at column i

                    for (int j = 0; j < _supermodes[sm_number]._lower_line._points.Count; j++)
                    {
                        if (_supermodes[sm_number]._lower_line._points[j].col == i
                         && _supermodes[sm_number]._lower_line._points[j].row > highest_row_at_i_on_lower_line)
                        {
                            index_of_highest_row_at_i_on_lower_line = j;
                            highest_row_at_i_on_lower_line = (int)(_supermodes[sm_number]._lower_line._points[j].row);
                        }
                    }

                    int lowest_row_at_i_on_upper_line = _rows;
                    int index_of_lowest_row_at_i_on_upper_line = -1;

                    // find lowest point on upper line at column i

                    for (int j = 0; j < _supermodes[sm_number]._upper_line._points.Count; j++)
                    {
                        if (_supermodes[sm_number]._upper_line._points[j].col == i
                         && _supermodes[sm_number]._upper_line._points[j].row < lowest_row_at_i_on_upper_line)
                        {
                            index_of_lowest_row_at_i_on_upper_line = j;
                            lowest_row_at_i_on_upper_line = (int)(_supermodes[sm_number]._upper_line._points[j].row);
                        }
                    }

                    if (index_of_highest_row_at_i_on_lower_line != -1
                     && index_of_lowest_row_at_i_on_upper_line != -1)
                    {
                        if (i == 0) middle_point_found_at_col_zero = true;
                        if (i == _cols - 1) middle_point_found_at_last_col = true;

                        // if both found, check minimum separation distance

                        if (lowest_row_at_i_on_upper_line
                          - highest_row_at_i_on_lower_line
                          > Convert.ToInt32(OverallMapSettings["MinNumberofPointsWidthofASupermode"]))
                        {
                            // find vertical midpoint

                            int row_of_midpoint = highest_row_at_i_on_lower_line +
                                   (int)(lowest_row_at_i_on_upper_line - highest_row_at_i_on_lower_line) / 2;
                            //     add to middle line
                            _supermodes[sm_number]._middle_line._points.Add(convertRowCol2LinePoint(row_of_midpoint, i));
                        }
                        else
                        {
                            // supermode min width not met, remove all points
                            _supermodes[sm_number]._middle_line.clear();
                            break;
                        }
                    }
                }//if

                if (!middle_point_found_at_col_zero
                 && (_supermodes[sm_number]._middle_line._points.Count != 0))
                {
                    // need to fill in the start of the middle line to the lhs-bottom border
                    // find middle start point
                    // calculate points on straight line fit first known middle point
                    // and add to start of middle line
                    int end_lower_point_index = _supermodes[sm_number]._lower_line._points.Count - 1;
                    CDSDBROverallModeMapLine.Point_Type lower_border_pt = _supermodes[sm_number]._lower_line._points[end_lower_point_index];

                    for (int j = 0; j < _supermodes[sm_number]._lower_line._points.Count; j++)
                    {
                        if ((_supermodes[sm_number]._lower_line._points[j].col == 0
                          || _supermodes[sm_number]._lower_line._points[j].row == 0)
                         && lower_border_pt == _supermodes[sm_number]._lower_line._points[end_lower_point_index])
                        {
                            // found point on lhs-bottom border
                            lower_border_pt = _supermodes[sm_number]._lower_line._points[j];
                            break;
                        }
                    }

                    int end_upper_point_index = _supermodes[sm_number]._upper_line._points.Count - 1;
                    CDSDBROverallModeMapLine.Point_Type upper_border_pt = _supermodes[sm_number]._upper_line._points[end_upper_point_index];

                    for (int j = 0; j < _supermodes[sm_number]._upper_line._points.Count; j++)
                    {
                        if ((_supermodes[sm_number]._upper_line._points[j].col == 0
                          || _supermodes[sm_number]._upper_line._points[j].row == 0)
                         && upper_border_pt == _supermodes[sm_number]._upper_line._points[end_upper_point_index])
                        {
                            // found point on lhs-bottom border
                            upper_border_pt = _supermodes[sm_number]._upper_line._points[j];
                            break;
                        }
                    }

                    if (lower_border_pt != _supermodes[sm_number]._lower_line._points[end_lower_point_index]
                     && upper_border_pt != _supermodes[sm_number]._upper_line._points[end_upper_point_index])
                    {
                        // Have upper and lower border points, find midpoint

                        int lower_dist_from_origin;
                        if (lower_border_pt.col == 0) lower_dist_from_origin = -(int)lower_border_pt.row;
                        else lower_dist_from_origin = (int)lower_border_pt.col;

                        int upper_dist_from_origin;
                        if (upper_border_pt.col == 0) upper_dist_from_origin = -(int)upper_border_pt.row;
                        else upper_dist_from_origin = (int)upper_border_pt.col;

                        int border_midpoint_row;
                        int border_midpoint_col;
                        int border_midpoint_dist_from_origin = lower_dist_from_origin - (lower_dist_from_origin - upper_dist_from_origin) / 2;
                        if (border_midpoint_dist_from_origin > 0)
                        {
                            border_midpoint_row = 0;
                            border_midpoint_col = border_midpoint_dist_from_origin;
                        }
                        else
                        {
                            border_midpoint_row = -border_midpoint_dist_from_origin;
                            border_midpoint_col = 0;
                        }

                        CDSDBROverallModeMapLine.Point_Type first_middle_pt = _supermodes[sm_number]._middle_line._points[0];
                        int first_middle_pt_col = (int)first_middle_pt.col;
                        int first_middle_pt_row = (int)first_middle_pt.row;

                        double slope = ((double)(first_middle_pt_row - border_midpoint_row)) / ((double)(first_middle_pt_col - border_midpoint_col));

                        // starting at the first existing point,
                        // insert one new point in to middle line
                        // for each column between the first existing point and the border point

                        for (int i = first_middle_pt_col - 1; i >= border_midpoint_col; i--)
                        {
                            double row_of_midpoint_double = (double)border_midpoint_row + slope * ((double)(i - border_midpoint_col));
                            int row_of_midpoint = (int)row_of_midpoint_double;
                            if (row_of_midpoint_double - (double)row_of_midpoint > 0.5) row_of_midpoint++;
                            if (row_of_midpoint < 0) row_of_midpoint = 0;

                            // add to middle line
                            _supermodes[sm_number]._middle_line._points.Insert(0, convertRowCol2LinePoint(row_of_midpoint, i));
                        }

                        // if the upper line's border point is on the lhs border,
                        // and the midpoint is on the bottom border
                        // then continue the middle line along the bottom border to the origin
                        if (upper_border_pt.col == 0
                         && Convert.ToBoolean(Convert.ToInt32(OverallMapSettings["ExtendMiddleLineToCornerIfStillInSupermode"]))
                         && border_midpoint_row == 0)
                        {
                            for (int i = border_midpoint_col - 1; i >= 0; i--)
                            {
                                // add to middle line
                                _supermodes[sm_number]._middle_line._points.Insert(0, convertRowCol2LinePoint(0, i));
                            }
                        }

                    }

                }

                if (!middle_point_found_at_last_col
                 && (_supermodes[sm_number]._middle_line._points.Count != 0))
                {
                    // need to fill in the end of the middle line to the rhs-top border
                    // find middle end point
                    // calculate points on straight line fit from last known middle point
                    // and add to end of middle line
                    int lower_border_pt_index = _supermodes[sm_number]._lower_line._points.Count - 1;
                    CDSDBROverallModeMapLine.Point_Type lower_border_pt = _supermodes[sm_number]._lower_line._points[lower_border_pt_index];

                    for (int j = 0; j < _supermodes[sm_number]._lower_line._points.Count; j++)
                    {
                        if ((_supermodes[sm_number]._lower_line._points[j].col == _cols - 1
                          || _supermodes[sm_number]._lower_line._points[j].row == _rows - 1)
                         && lower_border_pt == _supermodes[sm_number]._lower_line._points[lower_border_pt_index])
                        {
                            // found point on rhs-top border
                            lower_border_pt = _supermodes[sm_number]._lower_line._points[j];
                            break;
                        }
                    }
                    int upper_border_pt_index = _supermodes[sm_number]._upper_line._points.Count - 1;
                    CDSDBROverallModeMapLine.Point_Type upper_border_pt = _supermodes[sm_number]._upper_line._points[upper_border_pt_index];

                    for (int j = 0; j < _supermodes[sm_number]._upper_line._points.Count; j++)
                    {
                        if ((_supermodes[sm_number]._upper_line._points[j].col == _cols - 1
                          || _supermodes[sm_number]._upper_line._points[j].row == _rows - 1)
                         && upper_border_pt == _supermodes[sm_number]._upper_line._points[upper_border_pt_index])
                        {
                            // found point on rhs-top border
                            upper_border_pt = _supermodes[sm_number]._upper_line._points[j];
                            break;
                        }
                    }

                    if (lower_border_pt != _supermodes[sm_number]._lower_line._points[lower_border_pt_index]
                     && upper_border_pt != _supermodes[sm_number]._upper_line._points[upper_border_pt_index])
                    {
                        // Have upper and lower border points, find midpoint

                        int lower_dist_from_toprightcorner;
                        if (lower_border_pt.col == _cols - 1) lower_dist_from_toprightcorner = (int)((_rows - 1) - lower_border_pt.row);
                        else lower_dist_from_toprightcorner = (int)(lower_border_pt.col - (_cols - 1));

                        int upper_dist_from_toprightcorner;
                        if (upper_border_pt.col == _cols - 1) upper_dist_from_toprightcorner = (int)((_rows - 1) - upper_border_pt.row);
                        else upper_dist_from_toprightcorner = (int)(upper_border_pt.col - (_cols - 1));

                        int border_midpoint_row;
                        int border_midpoint_col;
                        int border_midpoint_dist_from_toprightcorner = lower_dist_from_toprightcorner - (lower_dist_from_toprightcorner - upper_dist_from_toprightcorner) / 2;
                        if (border_midpoint_dist_from_toprightcorner > 0)
                        {
                            border_midpoint_row = (_rows - 1) - border_midpoint_dist_from_toprightcorner;
                            border_midpoint_col = _cols - 1;
                        }
                        else
                        {
                            border_midpoint_row = _rows - 1;
                            border_midpoint_col = (_cols - 1) + border_midpoint_dist_from_toprightcorner;
                        }

                        CDSDBROverallModeMapLine.Point_Type last_middle_pt = _supermodes[sm_number]._middle_line._points[_supermodes[sm_number]._middle_line._points.Count - 1];

                        int last_middle_pt_col = (int)last_middle_pt.col;
                        int last_middle_pt_row = (int)last_middle_pt.row;

                        double slope = ((double)(border_midpoint_row - last_middle_pt_row)) / ((double)(border_midpoint_col - last_middle_pt_col));

                        // starting at the end,
                        // insert one new point in to middle line
                        // for each column between the last existing point and the border point

                        for (int i = last_middle_pt_col + 1; i <= border_midpoint_col; i++)
                        {
                            double row_of_midpoint_double = (double)last_middle_pt_row + slope * ((double)(i - last_middle_pt_col));
                            int row_of_midpoint = (int)row_of_midpoint_double;
                            if (row_of_midpoint_double - (double)row_of_midpoint > 0.5) row_of_midpoint++;
                            if (row_of_midpoint > _rows - 1) row_of_midpoint = _rows - 1;

                            // add to middle line
                            _supermodes[sm_number]._middle_line._points.Add(convertRowCol2LinePoint(row_of_midpoint, i));
                        }

                        // if the lower line's border point is on the rhs border,
                        // and the midpoint is on the top border
                        // then continue the middle line along the top border to the rhs border
                        if (lower_border_pt.col == _cols - 1
                         && Convert.ToBoolean(Convert.ToInt32(OverallMapSettings["ExtendMiddleLineToCornerIfStillInSupermode"]))
                         && border_midpoint_row == _rows - 1)
                        {
                            for (int i = border_midpoint_col + 1; i < _cols; i++)
                            {
                                // add to middle line
                                _supermodes[sm_number]._middle_line._points.Add(convertRowCol2LinePoint(_rows - 1, i));
                            }
                        }

                    }
                }
            }

        }
        public CDSDBROverallModeMapLine.Point_Type convertRowCol2LinePoint(int row, int col)
        {
            CDSDBROverallModeMapLine.Point_Type point_on_map = new CDSDBROverallModeMapLine.Point_Type();

            point_on_map.row = row;
            point_on_map.col = col;
            //stub here
            point_on_map.rear = _vector_Ir._currents[col];

            point_on_map.constantCurrent = 0;
            point_on_map.nonConstantCurrent = 0;
            point_on_map.front_pair_number = 0;


            point_on_map.front_pair_number = _vector_If.pair_number(row); // 1 + (short)(row / _vector_If._map.size());
            short index = (short)(_vector_If.index_in_submap(row)); // (short)(row % _vector_If._map.size());

    
            point_on_map.constantCurrent = _vector_If.getConstantCurrent(point_on_map.front_pair_number, (long)index);
            point_on_map.nonConstantCurrent = _vector_If.getNonConstantCurrent(point_on_map.front_pair_number, (long)index);
                    
             

            return point_on_map;
        }
        private void filterSMMiddleLines()
        {

            int filter_n =Convert.ToInt32(OverallMapSettings["NforMovingAverageFilteronMiddleLine"] );

            for (int sm_number = 0; sm_number < _supermodes.Count; sm_number++)
            {
                // Perform averaging in the If direction only
                for (int i = 0; i < _supermodes[sm_number]._middle_line._points.Count; i++)
                {
                    int window_size = 0;
                    int sum_of_rows_of_windowed_points = 0;
                    for (int j = i - filter_n; j <= i + filter_n; j++)
                    {
                        if (j >= 0 && j < _supermodes[sm_number]._middle_line._points.Count)
                        {
                            // sum rows in window, without exceeding start or end
                            window_size++;
                            sum_of_rows_of_windowed_points += (int)(_supermodes[sm_number]._middle_line._points[j].row);
                        }
                    }

                    // the structure CDSDBROverallModeMapLine::point_type
                    // contains both the row, col position on a map and
                    // the currents at that point.
                    // For the average, we need to interpolate the If current
                    // while finding the nearest row (just so it can be plotted on a map)
                    CDSDBROverallModeMapLine.Point_Type averaged_pt
                        = interpolateLinePoint(
                            (double)sum_of_rows_of_windowed_points / (double)window_size,
                            (double)(_supermodes[sm_number]._middle_line._points[i].col));

                    // Add averaged point
                    _supermodes[sm_number]._filtered_middle_line._points.Add(averaged_pt);
                }
            }
        }
        private CDSDBROverallModeMapLine.Point_Type interpolateLinePoint(double row, double col)
        {
            CDSDBROverallModeMapLine.Point_Type point_on_map = new CDSDBROverallModeMapLine.Point_Type();

            // find nearest row
            point_on_map.row = (long)row;
            if (row - (double)((long)row) > 0.5) point_on_map.row++;

            // find nearest col
            point_on_map.col = (long)col;
            if (col - (double)((long)col) > 0.5) point_on_map.col++;

            if (col > (double)((long)col))
            {
                // interpolate Ir linearly
                double Ir_a = _vector_Ir._currents[(int)col];
                double Ir_b = _vector_Ir._currents[((int)col) + 1];

                double Ir_fraction = col - (double)((long)col);

                point_on_map.rear = Ir_a + Ir_fraction * (Ir_b - Ir_a);
            }
            else
            {
                // no need to interpolate
                point_on_map.rear = _vector_Ir._currents[(int)col];
            }

            point_on_map.front_pair_number = 0;
            point_on_map.constantCurrent = 0;
            point_on_map.nonConstantCurrent = 0;
            
            // use the nearest point to determine which map to use
            point_on_map.front_pair_number = _vector_If.pair_number((int)point_on_map.row); // 1 + (short)(point_on_map.row / _vector_If._map.size());

            short index_of_nearest = (short)(_vector_If.index_in_submap((int)point_on_map.row)); //(short)(point_on_map.row % _vector_If._map.size());

            // find out how far the interpolated point is from the nearest point
            double If_fraction = row - (double)(point_on_map.row);
            long top_row = _vector_If._submap_length[point_on_map.front_pair_number - 1];

            if ((index_of_nearest == 0 && If_fraction < 0)
             || (index_of_nearest == (short)top_row && If_fraction > 0))
            {
                // cannot interpolate between maps!
                // use nearest point for currents
                If_fraction = 0;
            }

            double If_value;

            if (If_fraction != 0)
            {
                // need to interpolate
                int index_of_nextpt;
                if (If_fraction > 0) index_of_nextpt = index_of_nearest + 1;
                else
                {
                    index_of_nextpt = index_of_nearest - 1;
                    If_fraction = -If_fraction; // ensure positive fraction
                }

                // interpolate If linearly
                double If_a = _vector_If.getNonConstantCurrent(point_on_map.front_pair_number, index_of_nearest);
                double If_b = _vector_If.getNonConstantCurrent(point_on_map.front_pair_number, index_of_nextpt);
                If_value = If_a + If_fraction * (If_b - If_a);
            }
            else
            {
                // no need to interpolate
                If_value = _vector_If.getNonConstantCurrent(point_on_map.front_pair_number, index_of_nearest);
            }

            point_on_map.constantCurrent = _vector_If.getConstantCurrent(point_on_map.front_pair_number, index_of_nearest);
            point_on_map.nonConstantCurrent = If_value;
 
            return point_on_map;
        }

        private void checkSMMiddleLines()
        {
   
	// Check each middle line to ensure
	// it is in one supermode, that it's longer than a min length
	// and that it doesn't go back and forth across a change in front section pairs
	// if not, remove the supermode

	for( int sm = 0; sm < _supermodes.Count; sm++ )
	{

		if( _supermodes[sm]._filtered_middle_line._points.Count
			< Convert.ToInt32( OverallMapSettings["MinimumLengthofMiddleLine"]) )
		{
			// middle line is too short
			// remove it
			_supermodes[sm].clear();
			_supermodes.RemoveAt( sm );
			sm--;
		}
		else
		{
			// check front pair numbers don't cross back and forth
			bool crossing_back_and_forth = false;
			List<int> found_front_pair_numbers = new List<int>();
			int last_pt_front_pair_number = 0;
			for( int i = 0 ; i < _supermodes[sm]._filtered_middle_line._points.Count ; i++ )
			{
				int front_pair_number = _supermodes[sm]._filtered_middle_line._points[i].front_pair_number;

				if( front_pair_number != last_pt_front_pair_number )
				{
					for( int j = 0; j < found_front_pair_numbers.Count; j++ )
					{
						if( front_pair_number == found_front_pair_numbers[j] )
						{
							crossing_back_and_forth = true;
							break;
						}
					}
					if(crossing_back_and_forth)
					{
						// new front pair number has been seen earlier
						//gdm300306 'clear()' moved to after calls for row & col, was causing mem-viol exception	
						//gdm300306 from above
						_supermodes[sm].clear();
						_supermodes.RemoveAt( sm );
						sm--;

						break;
					}
					found_front_pair_numbers.Add( front_pair_number );
				}

				last_pt_front_pair_number = front_pair_number;
			}

			if(!crossing_back_and_forth)
			{
			    // stub here
                // get each maxDeltaPr values along middle line
                
                //for( int i = 0 ; i < _supermodes[sm]._filtered_middle_line._points.Count; i++ )
                //{

                //    long row = _supermodes[sm]._filtered_middle_line._points[i].row;
                //    long col = _supermodes[sm]._filtered_middle_line._points[i].col;
                //    double maxDeltaPr = *iMaxDeltaPrPt( row, col );

                //    if (maxDeltaPr > OverallMapSettings["MaxDeltaPrInASupermode"])
                //    {
                //        // threshold exceeded
                //        // middle line is not in one supermode
                //        // remove it
                //        _supermodes[sm].clear();
                //        _supermodes.RemoveAT( sm );
                //        sm--;
                //        break;
                //    }
                //}
			}
		}
	}
        }
      
#endif
        #endregion


    }
    class PairComparer : IComparer<Point>
    {


        #region IComparer<Point> Members

        public int Compare(Point x, Point y)
        {
            if (x.Y > y.Y) return 1;
            else if (x.Y < y.Y) return -1;
            else return 0;
        }

        #endregion
    }
    public class Point_Type
    {
        public long row;
        public long col;
        public short front_pair_number;
        public double constantCurrent;
        public double nonConstantCurrent;
        public double rear;
    }
}

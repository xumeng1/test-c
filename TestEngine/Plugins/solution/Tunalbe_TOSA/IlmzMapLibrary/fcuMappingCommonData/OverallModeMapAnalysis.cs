using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Collections.Specialized;
namespace Bookham.fcumapping.CommonData
{
    class OverallModeMapAnalysis
    {
        #region class members

        # region const member
        public const int MIN_NUMBER_OF_MODES = 5;

        public const int RESULTS_NO_ERROR = 0;
        public const int RESULTS_GENERAL_ERROR = 1;
        public const int RESULTS_NO_MODEMAP_DATA = 2;
        public const int RESULTS_PO_TOO_LOW = 4;
        public const int RESULTS_PO_TOO_HIGH = 8;
        public const int RESULTS_TOO_MANY_JUMPS = 16;
        public const int RESULTS_TOO_FEW_JUMPS = 32;
        public const int RESULTS_INVALID_NUMBER_OF_LINES = 64;
        public const int RESULTS_INVALID_LINES_REMOVED = 128;
        public const int RESULTS_LINES_MISSING = 256;

        public const string LASER_TYPE_ID_DSDBR01_CSTRING = "DSDBR01";
        public const string QA_METRICS_FILE_NAME = "qaMetrics";
        public const string PASSFAIL_FILE_NAME = "passFailMetrics";
        public const string COLLATED_PASSFAIL_FILE_NAME = "collatedPassFailResults";
        public const string USCORE = "_";
        public const string CSV = ".csv";
        #endregion

        #region data
        public bool _screeningPassed;

        public int _qaDetectedError;
        // vector of mode width vectors
        public List<Vector> _modeWidths = new List<Vector>();
        public List<double> _maxModeWidths = new List<double>();
        public List<double> _minModeWidths = new List<double>();
        public List<double> _meanModeWidths = new List<double>();
        public List<double> _sumModeWidths = new List<double>();
        public List<double> _varModeWidths = new List<double>();
        public List<double> _stdDevModeWidths = new List<double>();
        public List<double> _medianModeWidths = new List<double>();
        public List<double> _numModeWidthSamples = new List<double>();

        ///////////////////////////////////////

        public List<Vector> _modeUpperSlopes = new List<Vector>();
        public List<double> _maxModeUpperSlopes = new List<double>();
        public List<double> _minModeUpperSlopes = new List<double>();
        public List<double> _meanModeUpperSlopes = new List<double>();
        public List<double> _sumModeUpperSlopes = new List<double>();
        public List<double> _varModeUpperSlopes = new List<double>();
        public List<double> _stdDevModeUpperSlopes = new List<double>();
        public List<double> _medianModeUpperSlopes = new List<double>();
        public List<double> _numModeUpperSlopeSamples = new List<double>();

        ///////////////////////////////////////

        public List<Vector> _modeLowerSlopes = new List<Vector>();
        public List<double> _maxModeLowerSlopes = new List<double>();
        public List<double> _minModeLowerSlopes = new List<double>();
        public List<double> _meanModeLowerSlopes = new List<double>();
        public List<double> _sumModeLowerSlopes = new List<double>();
        public List<double> _varModeLowerSlopes = new List<double>();
        public List<double> _stdDevModeLowerSlopes = new List<double>();
        public List<double> _medianModeLowerSlopes = new List<double>();
        public List<double> _numModeLowerSlopeSamples = new List<double>();

        ///////////////////////////////////////

        public List<double> _lowerModalDistortionAngles = new List<double>();
        public List<double> _lowerModalDistortionAngleXPositions = new List<double>();

        public List<double> _upperModalDistortionAngles = new List<double>();
        public List<double> _upperModalDistortionAngleXPositions = new List<double>();

        ///////////////////////////////////////

        public List<Vector> _lowerModeBDCAreas = new List<Vector>();
        public List<double> _lowerMaxModeBDCAreas = new List<double>();
        public List<double> _lowerMinModeBDCAreas = new List<double>();
        public List<double> _lowerMeanModeBDCAreas = new List<double>();
        public List<double> _lowerSumModeBDCAreas = new List<double>();
        public List<double> _lowerVarModeBDCAreas = new List<double>();
        public List<double> _lowerStdDevModeBDCAreas = new List<double>();
        public List<double> _lowerMedianModeBDCAreas = new List<double>();
        public List<double> _lowerNumModeBDCAreaSamples = new List<double>();

        public List<Vector> _upperModeBDCAreas = new List<Vector>();
        public List<double> _upperMaxModeBDCAreas = new List<double>();
        public List<double> _upperMinModeBDCAreas = new List<double>();
        public List<double> _upperMeanModeBDCAreas = new List<double>();
        public List<double> _upperSumModeBDCAreas = new List<double>();
        public List<double> _upperVarModeBDCAreas = new List<double>();
        public List<double> _upperStdDevModeBDCAreas = new List<double>();
        public List<double> _upperMedianModeBDCAreas = new List<double>();
        public List<double> _upperNumModeBDCAreaSamples = new List<double>();

        ///////////////////////////////////////

        public List<Vector> _lowerModeBDCAreaXLengths = new List<Vector>();
        public List<double> _lowerMaxModeBDCAreaXLengths = new List<double>();
        public List<double> _lowerMinModeBDCAreaXLengths = new List<double>();
        public List<double> _lowerMeanModeBDCAreaXLengths = new List<double>();
        public List<double> _lowerSumModeBDCAreaXLengths = new List<double>();
        public List<double> _lowerVarModeBDCAreaXLengths = new List<double>();
        public List<double> _lowerStdDevModeBDCAreaXLengths = new List<double>();
        public List<double> _lowerMedianModeBDCAreaXLengths = new List<double>();
        public List<double> _lowerNumModeBDCAreaXLengthSamples = new List<double>();

        public List<Vector> _upperModeBDCAreaXLengths = new List<Vector>();
        public List<double> _upperMaxModeBDCAreaXLengths = new List<double>();
        public List<double> _upperMinModeBDCAreaXLengths = new List<double>();
        public List<double> _upperMeanModeBDCAreaXLengths = new List<double>();
        public List<double> _upperSumModeBDCAreaXLengths = new List<double>();
        public List<double> _upperVarModeBDCAreaXLengths = new List<double>();
        public List<double> _upperStdDevModeBDCAreaXLengths = new List<double>();
        public List<double> _upperMedianModeBDCAreaXLengths = new List<double>();
        public List<double> _upperNumModeBDCAreaXLengthSamples = new List<double>();

        ///////////////////////////////////////

        public List<double> _middleLineRMSValues = new List<double>();
        public List<double> _middleLineSlopes = new List<double>();

        ///////////////////////////////////////

        public Vector _continuityValues = new Vector();
        public double _maxContinuityValue;
        public double _minContinuityValue;
        public double _meanContinuityValues;
        public double _sumContinuityValues;
        public double _varContinuityValues;
        public double _stdDevContinuityValues;
        public double _medianContinuityValues;
        public double _numContinuityValues;

        public double _maxPrGap;
        public double _maxLowerPr;
        public double _minUpperPr;

        //
        ///////////////////////////////////////

        public int _numSuperModes;
        public List<CDSDBRSuperMode> _p_superModes = new List<CDSDBRSuperMode>();

        public int _numSuperModesRemoved;

        public string _resultsDir;
        public string _laserId;
        public string _dateTimeStamp;

        public int _xAxisLength;
        public int _yAxisLength;

        ///////////////////////////////////

        public string _qaResultsAbsFilePath;
        public string _passFailResultsAbsFilePath;
        public string _collatedPassFailResultsAbsFilePath;

        ///////////////////////////////////

        public bool _qaAnalysisComplete;
        public bool _passFailAnalysisComplete;


        public PassFailThresholds _smPassFailThresholds = new PassFailThresholds();

        //Data
        public PassFailAnalysis _maxModeBordersRemovedAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _maxModeWidthPFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _minModeWidthPFAnalysis = new PassFailAnalysis();

        public PassFailAnalysis _lowerMaxModeBDCAreaPFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _lowerMaxModeBDCAreaXLengthPFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _lowerSumModeBDCAreasPFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _lowerNumModeBDCAreasPFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _lowerModalDistortionAnglePFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _lowerModalDistortionAngleXPositionPFAnalysis = new PassFailAnalysis();

        public PassFailAnalysis _upperMaxModeBDCAreaPFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _upperMaxModeBDCAreaXLengthPFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _upperSumModeBDCAreasPFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _upperNumModeBDCAreasPFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _upperModalDistortionAnglePFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _upperModalDistortionAngleXPositionPFAnalysis = new PassFailAnalysis();

        public PassFailAnalysis _middleLineRMSValuesPFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _maxMiddleLineSlopePFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _minMiddleLineSlopePFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _maxPrGapPFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _maxLowerPrPFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _minUpperPrPFAnalysis = new PassFailAnalysis();

        public PassFailAnalysis _lowerMaxModeLineSlopePFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _upperMaxModeLineSlopePFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _lowerMinModeLineSlopePFAnalysis = new PassFailAnalysis();
        public PassFailAnalysis _upperMinModeLineSlopePFAnalysis = new PassFailAnalysis();


        private NameValueCollection overAllMapQASitting = new NameValueCollection();
        #endregion
        #endregion

        public OverallModeMapAnalysis()
        {
            overAllMapQASitting = (NameValueCollection)SettingConfigure.cfgReader.GetSection("CloseGridSetting/OverAllMap/QA");
            init();

        }
        public void init()
        {
            ////////////////////////////////////////////////
            ///
            //	QA Data
            //
            _modeWidths.Clear();
            _maxModeWidths.Clear();
            _minModeWidths.Clear();
            _meanModeWidths.Clear();
            _sumModeWidths.Clear();
            _varModeWidths.Clear();
            _stdDevModeWidths.Clear();
            _medianModeWidths.Clear();
            _numModeWidthSamples.Clear();

            ///////////////////////////////////////

            _modeUpperSlopes.Clear();
            _maxModeUpperSlopes.Clear();
            _minModeUpperSlopes.Clear();
            _meanModeUpperSlopes.Clear();
            _sumModeUpperSlopes.Clear();
            _varModeUpperSlopes.Clear();
            _stdDevModeUpperSlopes.Clear();
            _medianModeUpperSlopes.Clear();
            _numModeUpperSlopeSamples.Clear();

            ///////////////////////////////////////

            _modeLowerSlopes.Clear();
            _maxModeLowerSlopes.Clear();
            _minModeLowerSlopes.Clear();
            _meanModeLowerSlopes.Clear();
            _sumModeLowerSlopes.Clear();
            _varModeLowerSlopes.Clear();
            _stdDevModeLowerSlopes.Clear();
            _medianModeLowerSlopes.Clear();
            _numModeLowerSlopeSamples.Clear();

            ////////////////////////////////////////

            _lowerModalDistortionAngles.Clear();
            _upperModalDistortionAngles.Clear();

            ///////////////////////////////////////

            _lowerModeBDCAreas.Clear();
            _lowerMaxModeBDCAreas.Clear();
            _lowerMinModeBDCAreas.Clear();
            _lowerMeanModeBDCAreas.Clear();
            _lowerSumModeBDCAreas.Clear();
            _lowerVarModeBDCAreas.Clear();
            _lowerStdDevModeBDCAreas.Clear();
            _lowerMedianModeBDCAreas.Clear();
            _lowerNumModeBDCAreaSamples.Clear();

            _upperModeBDCAreas.Clear();
            _upperMaxModeBDCAreas.Clear();
            _upperMinModeBDCAreas.Clear();
            _upperMeanModeBDCAreas.Clear();
            _upperSumModeBDCAreas.Clear();
            _upperVarModeBDCAreas.Clear();
            _upperStdDevModeBDCAreas.Clear();
            _upperMedianModeBDCAreas.Clear();
            _upperNumModeBDCAreaSamples.Clear();

            ///////////////////////////////////////

            _lowerModeBDCAreaXLengths.Clear();
            _lowerMaxModeBDCAreaXLengths.Clear();
            _lowerMinModeBDCAreaXLengths.Clear();
            _lowerMeanModeBDCAreaXLengths.Clear();
            _lowerSumModeBDCAreaXLengths.Clear();
            _lowerVarModeBDCAreaXLengths.Clear();
            _lowerStdDevModeBDCAreaXLengths.Clear();
            _lowerMedianModeBDCAreaXLengths.Clear();
            _lowerNumModeBDCAreaXLengthSamples.Clear();

            _upperModeBDCAreaXLengths.Clear();
            _upperMaxModeBDCAreaXLengths.Clear();
            _upperMinModeBDCAreaXLengths.Clear();
            _upperMeanModeBDCAreaXLengths.Clear();
            _upperSumModeBDCAreaXLengths.Clear();
            _upperVarModeBDCAreaXLengths.Clear();
            _upperStdDevModeBDCAreaXLengths.Clear();
            _upperMedianModeBDCAreaXLengths.Clear();
            _upperNumModeBDCAreaXLengthSamples.Clear();

            ///////////////////////////////////////

            _middleLineRMSValues.Clear();
            _middleLineSlopes.Clear();

            ///////////////////////////////////////

            _maxContinuityValue = 0;
            _minContinuityValue = 0;
            _meanContinuityValues = 0;
            _sumContinuityValues = 0;
            _varContinuityValues = 0;
            _stdDevContinuityValues = 0;
            _medianContinuityValues = 0;
            _numContinuityValues = 0;
            _maxPrGap = 0;
            _maxLowerPr = 0;
            _minUpperPr = 0;

            ///////////////////////////////////////

            _qaAnalysisComplete = false;

            //
            //////////////////////////////////////////////////////

            //////////////////////////////////////////////////////
            ///
            //	Pass Fail Analysis
            //
            _passFailAnalysisComplete = false;

            _maxModeBordersRemovedAnalysis.init();
            _maxModeWidthPFAnalysis.init();
            _minModeWidthPFAnalysis.init();

            _lowerMaxModeBDCAreaPFAnalysis.init();
            _lowerMaxModeBDCAreaXLengthPFAnalysis.init();
            _lowerSumModeBDCAreasPFAnalysis.init();
            _lowerNumModeBDCAreasPFAnalysis.init();

            _upperMaxModeBDCAreaPFAnalysis.init();
            _upperMaxModeBDCAreaXLengthPFAnalysis.init();
            _upperSumModeBDCAreasPFAnalysis.init();
            _upperNumModeBDCAreasPFAnalysis.init();

            _middleLineRMSValuesPFAnalysis.init();
            _maxMiddleLineSlopePFAnalysis.init();
            _minMiddleLineSlopePFAnalysis.init();
            _maxPrGapPFAnalysis.init();
            _maxLowerPrPFAnalysis.init();
            _minUpperPrPFAnalysis.init();


            _lowerMaxModeLineSlopePFAnalysis.init();
            _upperMaxModeLineSlopePFAnalysis.init();
            _lowerMinModeLineSlopePFAnalysis.init();
            _upperMinModeLineSlopePFAnalysis.init();

            //
            //////////////////////////////////////////////////////

            _screeningPassed = false;

            _qaDetectedError = RESULTS_NO_ERROR;

        }

        public void writePassFailResultsToFile(string fileFullName)
        {

            using (StreamWriter sw = new StreamWriter(fileFullName, true))
            {
               
                if (_passFailAnalysisComplete)
                {          // go on
                    string errorMessage = "";
                    int errorNum = 0;
                    getQAError(_qaDetectedError, ref errorNum, ref errorMessage);
                    sw.WriteLine(errorMessage);
                    sw.WriteLine("");


                    sw.WriteLine("Slope Window Size,{0}", overAllMapQASitting["SlopeWindowSize"]);
                    sw.WriteLine("Modal Distortion Min X,{0}", overAllMapQASitting["ModalDistortionMinX"]);
                    sw.WriteLine("Modal Distortion Max X,{0}", overAllMapQASitting["ModalDistortionMaxX"]);
                    sw.WriteLine("Modal Distortion Min Y,{0}", overAllMapQASitting["ModalDistortionMinY"]);
                    sw.WriteLine("");
                    //writer header
                    const string QA_XLS_HEADER1 = "";
                    const string QA_XLS_HEADER2 = "Mode borders removed due to error";
                    const string QA_XLS_HEADER3 = "Max Mode Width";
                    const string QA_XLS_HEADER4 = "Min Mode Width";
                    const string QA_XLS_HEADER5 = "Max Lower Modal Distortion Angle";
                    const string QA_XLS_HEADER6 = "Max Upper Modal Distortion Angle";
                    const string QA_XLS_HEADER7 = "Max Lower Modal Distortion X-Position";
                    const string QA_XLS_HEADER8 = "Max Upper Modal Distortion X-Position";
                    const string QA_XLS_HEADER9 = "Max Lower Boundary Direction Change X-Length";
                    const string QA_XLS_HEADER10 = "Max Upper Boundary Direction Change X-Length";
                    const string QA_XLS_HEADER11 = "Max Lower Boundary Direction Change Area";
                    const string QA_XLS_HEADER12 = "Max Upper Boundary Direction Change Area";
                    const string QA_XLS_HEADER13 = "Sum of Lower Boundary Direction Change Areas";
                    const string QA_XLS_HEADER14 = "Sum of Upper Boundary Direction Change Areas";
                    const string QA_XLS_HEADER15 = "Number of Lower Boundary Direction Changes";
                    const string QA_XLS_HEADER16 = "Number of Upper Boundary Direction Changes";
                    const string QA_XLS_HEADER17 = "Max Middle Line RMS Value";
                    const string QA_XLS_HEADER18 = "Min Middle Line Slope";
                    const string QA_XLS_HEADER19 = "Max Middle Line Slope";
                    const string QA_XLS_HEADER20 = "Max Pr Gap";
                    const string QA_XLS_HEADER21 = "Max Lower Pr";
                    const string QA_XLS_HEADER22 = "Min Upper Pr";
                    const string QA_XLS_HEADER23 = "Max Mode Lower Line Slope";
                    const string QA_XLS_HEADER24 = "Max Mode Upper Line Slope";
                    const string QA_XLS_HEADER25 = "Min Mode Lower Line Slope";
                    const string QA_XLS_HEADER26 = "Min Mode Upper Line Slope";

                    const string QA_XLS_ROWTITLE1 = "Pass/Fail Result";
                    const string QA_XLS_ROWTITLE2 = "Min Value Measured";
                    const string QA_XLS_ROWTITLE3 = "Max Value Measured";
                    const string QA_XLS_ROWTITLE4 = "Number of Failed SMs";
                    const string QA_XLS_ROWTITLE5 = "Threshold Applied";

                    sw.Write(QA_XLS_HEADER1 + ",");
                    sw.Write(QA_XLS_HEADER2 + ",");
                    sw.Write(QA_XLS_HEADER3 + ",");
                    sw.Write(QA_XLS_HEADER4 + ",");
                    sw.Write(QA_XLS_HEADER5 + ",");
                    sw.Write(QA_XLS_HEADER6 + ",");
                    sw.Write(QA_XLS_HEADER7 + ",");
                    sw.Write(QA_XLS_HEADER8 + ",");
                    sw.Write(QA_XLS_HEADER9 + ",");
                    sw.Write(QA_XLS_HEADER10 + ",");
                    sw.Write(QA_XLS_HEADER11 + ",");
                    sw.Write(QA_XLS_HEADER12 + ",");
                    sw.Write(QA_XLS_HEADER13 + ",");
                    sw.Write(QA_XLS_HEADER14 + ",");
                    sw.Write(QA_XLS_HEADER15 + ",");
                    sw.Write(QA_XLS_HEADER16 + ",");
                    sw.Write(QA_XLS_HEADER17 + ",");
                    sw.Write(QA_XLS_HEADER18 + ",");
                    sw.Write(QA_XLS_HEADER19 + ",");
                    sw.Write(QA_XLS_HEADER20 + ",");
                    sw.Write(QA_XLS_HEADER21 + ",");
                    sw.Write(QA_XLS_HEADER22 + ",");
                    sw.Write(QA_XLS_HEADER23 + ",");
                    sw.Write(QA_XLS_HEADER24 + ",");
                    sw.Write(QA_XLS_HEADER25 + ",");
                    sw.WriteLine(QA_XLS_HEADER26 + ",");
                    sw.WriteLine("");
                    //write pass or fail
                    string QA_XLS_COL1_VALUE = "";
                    string QA_XLS_COL2_VALUE = "";
                    string QA_XLS_COL3_VALUE = "";
                    string QA_XLS_COL4_VALUE = "";
                    string QA_XLS_COL5_VALUE = "";
                    string QA_XLS_COL6_VALUE = "";
                    string QA_XLS_COL7_VALUE = "";
                    string QA_XLS_COL8_VALUE = "";
                    string QA_XLS_COL9_VALUE = "";
                    string QA_XLS_COL10_VALUE = "";
                    string QA_XLS_COL11_VALUE = "";
                    string QA_XLS_COL12_VALUE = "";
                    string QA_XLS_COL13_VALUE = "";
                    string QA_XLS_COL14_VALUE = "";
                    string QA_XLS_COL15_VALUE = "";
                    string QA_XLS_COL16_VALUE = "";
                    string QA_XLS_COL17_VALUE = "";
                    string QA_XLS_COL18_VALUE = "";
                    string QA_XLS_COL19_VALUE = "";
                    string QA_XLS_COL20_VALUE = "";
                    string QA_XLS_COL21_VALUE = "";
                    string QA_XLS_COL22_VALUE = "";
                    string QA_XLS_COL23_VALUE = "";
                    string QA_XLS_COL24_VALUE = "";
                    string QA_XLS_COL25_VALUE = "";
                    string QA_XLS_COL26_VALUE = "";

                    /// first row - pass/fails
                    QA_XLS_COL2_VALUE = (_numSuperModesRemoved <= _maxModeBordersRemovedAnalysis.threshold) ? "PASS" : "FAIL";
                    QA_XLS_COL3_VALUE = _maxModeWidthPFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL4_VALUE = _minModeWidthPFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL5_VALUE = _lowerModalDistortionAnglePFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL6_VALUE = _upperModalDistortionAnglePFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL7_VALUE = _lowerModalDistortionAngleXPositionPFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL8_VALUE = _upperModalDistortionAngleXPositionPFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL9_VALUE = _lowerMaxModeBDCAreaXLengthPFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL10_VALUE = _upperMaxModeBDCAreaXLengthPFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL11_VALUE = _lowerMaxModeBDCAreaPFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL12_VALUE = _upperMaxModeBDCAreaPFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL13_VALUE = _lowerSumModeBDCAreasPFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL14_VALUE = _upperSumModeBDCAreasPFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL15_VALUE = _lowerNumModeBDCAreasPFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL16_VALUE = _upperNumModeBDCAreasPFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL17_VALUE = _middleLineRMSValuesPFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL18_VALUE = _minMiddleLineSlopePFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL19_VALUE = _maxMiddleLineSlopePFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL20_VALUE = _maxPrGapPFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL21_VALUE = _maxLowerPrPFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL22_VALUE = _minUpperPrPFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL23_VALUE = _lowerMaxModeLineSlopePFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL24_VALUE = _upperMaxModeLineSlopePFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL25_VALUE = _lowerMinModeLineSlopePFAnalysis.pass ? "PASS" : "FAIL";
                    QA_XLS_COL26_VALUE = _upperMinModeLineSlopePFAnalysis.pass ? "PASS" : "FAIL";

                    sw.Write(QA_XLS_ROWTITLE1 + ",");
                    sw.Write(QA_XLS_COL2_VALUE + ",");
                    sw.Write(QA_XLS_COL3_VALUE + ",");
                    sw.Write(QA_XLS_COL4_VALUE + ",");
                    sw.Write(QA_XLS_COL5_VALUE + ",");
                    sw.Write(QA_XLS_COL6_VALUE + ",");
                    sw.Write(QA_XLS_COL7_VALUE + ",");
                    sw.Write(QA_XLS_COL8_VALUE + ",");
                    sw.Write(QA_XLS_COL9_VALUE + ",");
                    sw.Write(QA_XLS_COL10_VALUE + ",");
                    sw.Write(QA_XLS_COL11_VALUE + ",");
                    sw.Write(QA_XLS_COL12_VALUE + ",");
                    sw.Write(QA_XLS_COL13_VALUE + ",");
                    sw.Write(QA_XLS_COL14_VALUE + ",");
                    sw.Write(QA_XLS_COL15_VALUE + ",");
                    sw.Write(QA_XLS_COL16_VALUE + ",");
                    sw.Write(QA_XLS_COL17_VALUE + ",");
                    sw.Write(QA_XLS_COL18_VALUE + ",");
                    sw.Write(QA_XLS_COL19_VALUE + ",");
                    sw.Write(QA_XLS_COL20_VALUE + ",");
                    sw.Write(QA_XLS_COL21_VALUE + ",");
                    sw.Write(QA_XLS_COL22_VALUE + ",");
                    sw.Write(QA_XLS_COL23_VALUE + ",");
                    sw.Write(QA_XLS_COL24_VALUE + ",");
                    sw.Write(QA_XLS_COL25_VALUE + ",");
                    sw.WriteLine(QA_XLS_COL26_VALUE + ",");

                    //write	num failed Modes

                    QA_XLS_COL2_VALUE = string.Format("{0}", _numSuperModesRemoved);
                    QA_XLS_COL3_VALUE = string.Format("{0}", _maxModeWidthPFAnalysis.numFailedModes);
                    QA_XLS_COL4_VALUE = string.Format("{0}", _minModeWidthPFAnalysis.numFailedModes);
                    QA_XLS_COL5_VALUE = string.Format("{0}", _lowerModalDistortionAnglePFAnalysis.numFailedModes);
                    QA_XLS_COL6_VALUE = string.Format("{0}", _upperModalDistortionAnglePFAnalysis.numFailedModes);
                    QA_XLS_COL7_VALUE = string.Format("{0}", _lowerModalDistortionAngleXPositionPFAnalysis.numFailedModes);
                    QA_XLS_COL8_VALUE = string.Format("{0}", _upperModalDistortionAngleXPositionPFAnalysis.numFailedModes);
                    QA_XLS_COL9_VALUE = string.Format("{0}", _lowerMaxModeBDCAreaXLengthPFAnalysis.numFailedModes);
                    QA_XLS_COL10_VALUE = string.Format("{0}", _upperMaxModeBDCAreaXLengthPFAnalysis.numFailedModes);
                    QA_XLS_COL11_VALUE = string.Format("{0}", _lowerMaxModeBDCAreaPFAnalysis.numFailedModes);
                    QA_XLS_COL12_VALUE = string.Format("{0}", _upperMaxModeBDCAreaPFAnalysis.numFailedModes);
                    QA_XLS_COL13_VALUE = string.Format("{0}", _lowerSumModeBDCAreasPFAnalysis.numFailedModes);
                    QA_XLS_COL14_VALUE = string.Format("{0}", _upperSumModeBDCAreasPFAnalysis.numFailedModes);
                    QA_XLS_COL15_VALUE = string.Format("{0}", _lowerSumModeBDCAreasPFAnalysis.numFailedModes);
                    QA_XLS_COL16_VALUE = string.Format("{0}", _upperSumModeBDCAreasPFAnalysis.numFailedModes);
                    QA_XLS_COL17_VALUE = string.Format("{0}", _middleLineRMSValuesPFAnalysis.numFailedModes);
                    QA_XLS_COL18_VALUE = string.Format("{0}", _minMiddleLineSlopePFAnalysis.numFailedModes);
                    QA_XLS_COL19_VALUE = string.Format("{0}", _maxMiddleLineSlopePFAnalysis.numFailedModes);
                    QA_XLS_COL20_VALUE = string.Format("{0}", _maxPrGapPFAnalysis.numFailedModes);
                    QA_XLS_COL21_VALUE = string.Format("{0}", _maxLowerPrPFAnalysis.numFailedModes);
                    QA_XLS_COL22_VALUE = string.Format("{0}", _minUpperPrPFAnalysis.numFailedModes);
                    QA_XLS_COL23_VALUE = string.Format("{0}", _lowerMaxModeLineSlopePFAnalysis.numFailedModes);
                    QA_XLS_COL24_VALUE = string.Format("{0}", _upperMaxModeLineSlopePFAnalysis.numFailedModes);
                    QA_XLS_COL25_VALUE = string.Format("{0}", _lowerMinModeLineSlopePFAnalysis.numFailedModes);
                    QA_XLS_COL26_VALUE = string.Format("{0}", _upperMinModeLineSlopePFAnalysis.numFailedModes);

                    sw.Write(QA_XLS_ROWTITLE4 + ",");
                    sw.Write(QA_XLS_COL2_VALUE + ",");
                    sw.Write(QA_XLS_COL3_VALUE + ",");
                    sw.Write(QA_XLS_COL4_VALUE + ",");
                    sw.Write(QA_XLS_COL5_VALUE + ",");
                    sw.Write(QA_XLS_COL6_VALUE + ",");
                    sw.Write(QA_XLS_COL7_VALUE + ",");
                    sw.Write(QA_XLS_COL8_VALUE + ",");
                    sw.Write(QA_XLS_COL9_VALUE + ",");
                    sw.Write(QA_XLS_COL10_VALUE + ",");
                    sw.Write(QA_XLS_COL11_VALUE + ",");
                    sw.Write(QA_XLS_COL12_VALUE + ",");
                    sw.Write(QA_XLS_COL13_VALUE + ",");
                    sw.Write(QA_XLS_COL14_VALUE + ",");
                    sw.Write(QA_XLS_COL15_VALUE + ",");
                    sw.Write(QA_XLS_COL16_VALUE + ",");
                    sw.Write(QA_XLS_COL17_VALUE + ",");
                    sw.Write(QA_XLS_COL18_VALUE + ",");
                    sw.Write(QA_XLS_COL19_VALUE + ",");
                    sw.Write(QA_XLS_COL20_VALUE + ",");
                    sw.Write(QA_XLS_COL21_VALUE + ",");
                    sw.Write(QA_XLS_COL22_VALUE + ",");
                    sw.Write(QA_XLS_COL23_VALUE + ",");
                    sw.Write(QA_XLS_COL24_VALUE + ",");
                    sw.Write(QA_XLS_COL25_VALUE + ",");
                    sw.WriteLine(QA_XLS_COL26_VALUE + ",");

                    //write	thresholds

                    QA_XLS_COL2_VALUE = string.Format(" > {0}", _maxModeBordersRemovedAnalysis.threshold);
                    QA_XLS_COL3_VALUE = string.Format(" > {0}", _maxModeWidthPFAnalysis.threshold);
                    QA_XLS_COL4_VALUE = string.Format(" < {0}", _minModeWidthPFAnalysis.threshold);
                    QA_XLS_COL5_VALUE = string.Format(" > {0}", _lowerModalDistortionAnglePFAnalysis.threshold);
                    QA_XLS_COL6_VALUE = string.Format(" > {0}", _upperModalDistortionAnglePFAnalysis.threshold);
                    QA_XLS_COL7_VALUE = string.Format(" > {0}", _lowerModalDistortionAngleXPositionPFAnalysis.threshold);
                    QA_XLS_COL8_VALUE = string.Format(" > {0}", _upperModalDistortionAngleXPositionPFAnalysis.threshold);
                    QA_XLS_COL9_VALUE = string.Format(" > {0}", _lowerMaxModeBDCAreaXLengthPFAnalysis.threshold);
                    QA_XLS_COL10_VALUE = string.Format(" > {0}", _upperMaxModeBDCAreaXLengthPFAnalysis.threshold);
                    QA_XLS_COL11_VALUE = string.Format(" > {0}", _lowerMaxModeBDCAreaPFAnalysis.threshold);
                    QA_XLS_COL12_VALUE = string.Format(" > {0}", _upperMaxModeBDCAreaPFAnalysis.threshold);
                    QA_XLS_COL13_VALUE = string.Format(" > {0}", _lowerSumModeBDCAreasPFAnalysis.threshold);
                    QA_XLS_COL14_VALUE = string.Format(" > {0}", _upperSumModeBDCAreasPFAnalysis.threshold);
                    QA_XLS_COL15_VALUE = string.Format(" > {0}", _lowerNumModeBDCAreasPFAnalysis.threshold);
                    QA_XLS_COL16_VALUE = string.Format(" > {0}", _upperNumModeBDCAreasPFAnalysis.threshold);
                    QA_XLS_COL17_VALUE = string.Format(" > {0}", _middleLineRMSValuesPFAnalysis.threshold);
                    QA_XLS_COL18_VALUE = string.Format(" < {0}", _minMiddleLineSlopePFAnalysis.threshold);
                    QA_XLS_COL19_VALUE = string.Format(" > {0}", _maxMiddleLineSlopePFAnalysis.threshold);
                    QA_XLS_COL20_VALUE = string.Format(" > {0}", _maxPrGapPFAnalysis.threshold);
                    QA_XLS_COL21_VALUE = string.Format(" > {0}", _maxLowerPrPFAnalysis.threshold);
                    QA_XLS_COL22_VALUE = string.Format(" < {0}", _minUpperPrPFAnalysis.threshold);
                    QA_XLS_COL23_VALUE = string.Format(" > {0}", _lowerMaxModeLineSlopePFAnalysis.threshold);
                    QA_XLS_COL24_VALUE = string.Format(" > {0}", _upperMaxModeLineSlopePFAnalysis.threshold);
                    QA_XLS_COL25_VALUE = string.Format(" < {0}", _lowerMinModeLineSlopePFAnalysis.threshold);
                    QA_XLS_COL26_VALUE = string.Format(" < {0}", _upperMinModeLineSlopePFAnalysis.threshold);

                    sw.Write(QA_XLS_ROWTITLE5 + ",");
                    sw.Write(QA_XLS_COL2_VALUE + ",");
                    sw.Write(QA_XLS_COL3_VALUE + ",");
                    sw.Write(QA_XLS_COL4_VALUE + ",");
                    sw.Write(QA_XLS_COL5_VALUE + ",");
                    sw.Write(QA_XLS_COL6_VALUE + ",");
                    sw.Write(QA_XLS_COL7_VALUE + ",");
                    sw.Write(QA_XLS_COL8_VALUE + ",");
                    sw.Write(QA_XLS_COL9_VALUE + ",");
                    sw.Write(QA_XLS_COL10_VALUE + ",");
                    sw.Write(QA_XLS_COL11_VALUE + ",");
                    sw.Write(QA_XLS_COL12_VALUE + ",");
                    sw.Write(QA_XLS_COL13_VALUE + ",");
                    sw.Write(QA_XLS_COL14_VALUE + ",");
                    sw.Write(QA_XLS_COL15_VALUE + ",");
                    sw.Write(QA_XLS_COL16_VALUE + ",");
                    sw.Write(QA_XLS_COL17_VALUE + ",");
                    sw.Write(QA_XLS_COL18_VALUE + ",");
                    sw.Write(QA_XLS_COL19_VALUE + ",");
                    sw.Write(QA_XLS_COL20_VALUE + ",");
                    sw.Write(QA_XLS_COL21_VALUE + ",");
                    sw.Write(QA_XLS_COL22_VALUE + ",");
                    sw.Write(QA_XLS_COL23_VALUE + ",");
                    sw.Write(QA_XLS_COL24_VALUE + ",");
                    sw.Write(QA_XLS_COL25_VALUE + ",");
                    sw.WriteLine(QA_XLS_COL26_VALUE + ",");
                    sw.WriteLine("");
                    sw.WriteLine("");

                    // row title
                    sw.WriteLine("Index");
                    // final rows - Modes
                    for (int i = 0; i < _numSuperModes; i++)
                    {
                         QA_XLS_COL1_VALUE = "";
                         QA_XLS_COL2_VALUE = "";
                         QA_XLS_COL3_VALUE = "";
                         QA_XLS_COL4_VALUE = "";
                         QA_XLS_COL5_VALUE = "";
                         QA_XLS_COL6_VALUE = "";
                         QA_XLS_COL7_VALUE = "";
                         QA_XLS_COL8_VALUE = "";
                         QA_XLS_COL9_VALUE = "";
                         QA_XLS_COL10_VALUE = "";
                         QA_XLS_COL11_VALUE = "";
                         QA_XLS_COL12_VALUE = "";
                         QA_XLS_COL13_VALUE = "";
                         QA_XLS_COL14_VALUE = "";
                         QA_XLS_COL15_VALUE = "";
                         QA_XLS_COL16_VALUE = "";
                         QA_XLS_COL17_VALUE = "";
                         QA_XLS_COL18_VALUE = "";
                         QA_XLS_COL19_VALUE = "";
                         QA_XLS_COL20_VALUE = "";
                         QA_XLS_COL21_VALUE = "";
                         QA_XLS_COL22_VALUE = "";
                         QA_XLS_COL23_VALUE = "";
                         QA_XLS_COL24_VALUE = "";
                         QA_XLS_COL25_VALUE = "";
                         QA_XLS_COL26_VALUE = "";
                        // Index location
                        QA_XLS_COL1_VALUE = string.Format(" {0}", i);

                        // Max Mode Width
                        int index = _maxModeWidthPFAnalysis.failedModes.IndexOf(i);
                        if (i < _maxModeWidths.Count && index != -1)
                            QA_XLS_COL3_VALUE = string.Format("{0}", _maxModeWidths[i]);


                        // Min Mode Width  
                        index = _minModeWidthPFAnalysis.failedModes.IndexOf(i);
                        if (i < _minModeWidths.Count && index != -1)
                            QA_XLS_COL4_VALUE = string.Format("{0}", _minModeWidths[i]);


                        // lower modal_distortion
                        index = _minModeWidthPFAnalysis.failedModes.IndexOf(i);
                        if (i < _lowerModalDistortionAngles.Count
                        && i < _lowerModalDistortionAngleXPositions.Count
                        && index != -1)
                        {
                            QA_XLS_COL5_VALUE = string.Format("{0}", _lowerModalDistortionAngles[i]);
                            QA_XLS_COL7_VALUE = string.Format("{0}", _lowerModalDistortionAngleXPositions[i]);
                        }

                        // upper modal_distortion

                        index = _minModeWidthPFAnalysis.failedModes.IndexOf(i);
                        if (i < _upperModalDistortionAngles.Count
                        && i < _upperModalDistortionAngleXPositions.Count
                        && index != -1)
                        {
                            QA_XLS_COL6_VALUE = string.Format("{0}", _upperModalDistortionAngles[i]);
                            QA_XLS_COL8_VALUE = string.Format("{0}", _upperModalDistortionAngleXPositions[i]);
                        }

                        // Lower Max Bdc X-Length

                        index = _minModeWidthPFAnalysis.failedModes.IndexOf(i);
                        if (i < _lowerMaxModeBDCAreaXLengths.Count && index != -1)
                        QA_XLS_COL9_VALUE = string.Format("{0}", _lowerMaxModeBDCAreaXLengths[i]);

                        // Upper Max Bdc X-Length

                        index = _minModeWidthPFAnalysis.failedModes.IndexOf(i);
                        if (i < _upperMaxModeBDCAreaXLengths.Count && index != -1)
                            QA_XLS_COL10_VALUE = string.Format("{0}", _upperMaxModeBDCAreaXLengths[i]);

                        // Lower Max Bdc Area

                        index = _minModeWidthPFAnalysis.failedModes.IndexOf(i);
                        if (i < _lowerMaxModeBDCAreas.Count && index != -1)
                            QA_XLS_COL11_VALUE = string.Format("{0}", _lowerMaxModeBDCAreas[i]);

                        // Upper Max Bdc Area

                        index = _minModeWidthPFAnalysis.failedModes.IndexOf(i);
                        if (i < _upperMaxModeBDCAreas.Count && index != -1)
                            QA_XLS_COL12_VALUE = string.Format("{0}", _upperMaxModeBDCAreas[i]);

                        // Lower Sum of Bdc Areas

                        index = _minModeWidthPFAnalysis.failedModes.IndexOf(i);
                        if (i < _lowerSumModeBDCAreas.Count && index != -1)
                            QA_XLS_COL13_VALUE = string.Format("{0}", _lowerSumModeBDCAreas[i]);

                        // Upper Sum of Bdc Areas
                        index = _minModeWidthPFAnalysis.failedModes.IndexOf(i);
                        if (i < _upperSumModeBDCAreas.Count && index != -1)
                            QA_XLS_COL14_VALUE = string.Format("{0}", _upperSumModeBDCAreas[i]);

                        // Lower Number of Bdcs

                        index = _minModeWidthPFAnalysis.failedModes.IndexOf(i);
                        if (i < _lowerNumModeBDCAreaSamples.Count && index != -1)
                            QA_XLS_COL15_VALUE = string.Format("{0}", _lowerNumModeBDCAreaSamples[i]);

                        // Upper Number of Bdcs

                        index = _minModeWidthPFAnalysis.failedModes.IndexOf(i);
                        if (i < _upperNumModeBDCAreaSamples.Count && index != -1)
                            QA_XLS_COL16_VALUE = string.Format("{0}", _upperNumModeBDCAreaSamples[i]);

                        // Max Middle Line RMS Value

                        index = _minModeWidthPFAnalysis.failedModes.IndexOf(i);
                        if (i < _middleLineRMSValues.Count && index != -1)
                            QA_XLS_COL17_VALUE = string.Format("{0}", _middleLineRMSValues[i]);

                        // Min Middle Line Slope

                        index = _minModeWidthPFAnalysis.failedModes.IndexOf(i);
                        if (i < _middleLineSlopes.Count && index != -1)
                            QA_XLS_COL18_VALUE = string.Format("{0}", _middleLineSlopes[i]);

                        // Max Middle Line Slope

                        index = _minModeWidthPFAnalysis.failedModes.IndexOf(i);
                        if (i < _middleLineSlopes.Count && index != -1)
                            QA_XLS_COL19_VALUE = string.Format("{0}", _middleLineSlopes[i]);

                        // Max Pr Gap

                        index = _minModeWidthPFAnalysis.failedModes.IndexOf(i);
                        if (i < _continuityValues.size() && index != -1)
                            QA_XLS_COL20_VALUE = string.Format("{0}", _maxPrGap);

                        // Max Lower Pr

                        index = _minModeWidthPFAnalysis.failedModes.IndexOf(i);
                        if (i < _continuityValues.size() && index != -1)
                            QA_XLS_COL21_VALUE = string.Format("{0}", _maxLowerPr);

                        // Min Upper Pr

                        index = _minModeWidthPFAnalysis.failedModes.IndexOf(i);
                        if (i < _continuityValues.size() && index != -1)
                            QA_XLS_COL22_VALUE = string.Format("{0}", _minUpperPr);

                        // Lower max mode line slope

                        index = _minModeWidthPFAnalysis.failedModes.IndexOf(i);
                        if (i < _maxModeLowerSlopes.Count && index != -1)
                            QA_XLS_COL23_VALUE = string.Format("{0}", _maxModeLowerSlopes[i]);

                        // Upper max mode line slope
                        index = _minModeWidthPFAnalysis.failedModes.IndexOf(i);
                        if (i < _maxModeUpperSlopes.Count && index != -1)
                            QA_XLS_COL24_VALUE = string.Format("{0}", _maxModeUpperSlopes[i]);


                        // Lower min mode line slope
                        index = _minModeWidthPFAnalysis.failedModes.IndexOf(i);
                        if (i < _minModeLowerSlopes.Count && index != -1)
                            QA_XLS_COL25_VALUE = string.Format("{0}", _minModeLowerSlopes[i]);

                        // Upper min mode line slope
                        index = _minModeWidthPFAnalysis.failedModes.IndexOf(i);
                        if (i < _minModeUpperSlopes.Count && index != -1)
                            QA_XLS_COL26_VALUE = string.Format("{0}", _minModeUpperSlopes[i]);

                        sw.Write(QA_XLS_COL1_VALUE + ",");
                        sw.Write(QA_XLS_COL2_VALUE + ",");
                        sw.Write(QA_XLS_COL3_VALUE + ",");
                        sw.Write(QA_XLS_COL4_VALUE + ",");
                        sw.Write(QA_XLS_COL5_VALUE + ",");
                        sw.Write(QA_XLS_COL6_VALUE + ",");
                        sw.Write(QA_XLS_COL7_VALUE + ",");
                        sw.Write(QA_XLS_COL8_VALUE + ",");
                        sw.Write(QA_XLS_COL9_VALUE + ",");
                        sw.Write(QA_XLS_COL10_VALUE + ",");
                        sw.Write(QA_XLS_COL11_VALUE + ",");
                        sw.Write(QA_XLS_COL12_VALUE + ",");
                        sw.Write(QA_XLS_COL13_VALUE + ",");
                        sw.Write(QA_XLS_COL14_VALUE + ",");
                        sw.Write(QA_XLS_COL15_VALUE + ",");
                        sw.Write(QA_XLS_COL16_VALUE + ",");
                        sw.Write(QA_XLS_COL17_VALUE + ",");
                        sw.Write(QA_XLS_COL18_VALUE + ",");
                        sw.Write(QA_XLS_COL19_VALUE + ",");
                        sw.Write(QA_XLS_COL20_VALUE + ",");
                        sw.Write(QA_XLS_COL21_VALUE + ",");
                        sw.Write(QA_XLS_COL22_VALUE + ",");
                        sw.Write(QA_XLS_COL23_VALUE + ",");
                        sw.Write(QA_XLS_COL24_VALUE + ",");
                        sw.Write(QA_XLS_COL25_VALUE + ",");
                        sw.WriteLine(QA_XLS_COL26_VALUE + ",");
                    }

                }
                else // qaWasRun == false
                {
                    sw.WriteLine("Error : Function called before Pass Fail Analysis completed.");
                }
                sw.Flush();
            }
        }
        public void getQAError(int qaError, ref int errorNumber,ref string errorMsg)
        {
            errorMsg = "";
            errorNumber = 0;

            if (Convert.ToBoolean(qaError & RESULTS_GENERAL_ERROR))
            {
                errorMsg += ("Error(s) detected. ");
                errorNumber |= RESULTS_GENERAL_ERROR;
            }
            if (Convert.ToBoolean(qaError & RESULTS_NO_MODEMAP_DATA))
            {
                errorMsg += ("Mode map data not available. ");
                errorNumber |= RESULTS_NO_MODEMAP_DATA;
            }
            if (Convert.ToBoolean(qaError & RESULTS_PO_TOO_LOW))
            {
                errorMsg += ("Invalid laser screening run - optical power is too low. ");
                errorNumber |= RESULTS_PO_TOO_LOW;
            }
            if (Convert.ToBoolean(qaError & RESULTS_PO_TOO_HIGH))
            {
                errorMsg += ("Invalid laser screening run - optical power is too high. ");
                errorNumber |= RESULTS_PO_TOO_HIGH;
            }
            if (Convert.ToBoolean(qaError & RESULTS_TOO_MANY_JUMPS))
            {
                errorMsg += ("Invalid laser screening run - too many laser mode jumps found. ");
                errorNumber |= RESULTS_TOO_MANY_JUMPS;
            }
            if (Convert.ToBoolean(qaError & RESULTS_TOO_FEW_JUMPS))
            {
                errorMsg += ("Invalid laser screening run - too few laser mode jumps found. ");
                errorNumber |= RESULTS_TOO_FEW_JUMPS;
            }
            if (Convert.ToBoolean(qaError & RESULTS_INVALID_NUMBER_OF_LINES))
            {
                errorMsg += ("Invalid laser screening run - invalid number of mode boundaries detected. ");
                errorNumber |= RESULTS_INVALID_NUMBER_OF_LINES;
            }
            if (Convert.ToBoolean(qaError & RESULTS_INVALID_LINES_REMOVED))
            {
                errorMsg += ("Invalid mode boundaries detected. ");
                errorNumber |= RESULTS_INVALID_LINES_REMOVED;
            }
            if (Convert.ToBoolean(qaError & RESULTS_LINES_MISSING))
            {
                errorMsg += ("Undetected mode boundaries. ");
                errorNumber |= RESULTS_LINES_MISSING;
            }

            string errNumMsg = "";
            if (errorNumber > RESULTS_NO_ERROR)
                errNumMsg = string.Format("{0},", errorNumber);
            else
                errNumMsg = ",";
            errNumMsg += errorMsg;


            errorMsg = errNumMsg;
        }


        public void runAnalysis(List<CDSDBRSuperMode> p_superModes,
                    int xAxisLength,
                    int yAxisLength,
                    Vector continuityValues,
                    int superModesRemoved,
                    string resultsDir,
                    string laserId,
                    string dateTimeStamp)
        {
            _p_superModes = p_superModes;
            _numSuperModes = p_superModes.Count;

            _numSuperModesRemoved = superModesRemoved;

            _qaResultsAbsFilePath = createQaResultsAbsFilePath(resultsDir, laserId, dateTimeStamp);

            _passFailResultsAbsFilePath = createPassFailResultsAbsFilePath(resultsDir, laserId, dateTimeStamp);

            _collatedPassFailResultsAbsFilePath = createPassFailCollateResultsAbsFilePath(resultsDir, laserId, dateTimeStamp);


            _resultsDir = resultsDir;
            _laserId = laserId;
            _dateTimeStamp = dateTimeStamp;

            _xAxisLength = xAxisLength;
            _yAxisLength = yAxisLength;

            _continuityValues = continuityValues;




            runQaAnalysis();

            runPassFailAnalysis();

            writeQaResultsToFile(_qaResultsAbsFilePath);


            writePassFailResultsToFile(_passFailResultsAbsFilePath);


            PassFailCollated.instance.setFileParameters(_resultsDir, _laserId, _dateTimeStamp);
            PassFailCollated.instance.clearSuperModes();
            PassFailCollated.instance.setOverallModeMapAnalysis(this);


        }


        public void runQaAnalysis()
        {
            for (int i = 0; i < _numSuperModes; i++)
            {
                _p_superModes[i].runModeAnalysis();
            }

            getModeWidths();
            getMaxModeWidths();
            getMinModeWidths();
            getMeanModeWidths();
            getSumModeWidths();
            getVarModeWidths();
            getStdDevModeWidths();
            getMedianModeWidths();
            getNumModeWidthSamples();

            getModeUpperSlopes();
            getMaxModeUpperSlopes();
            getMinModeUpperSlopes();
            getMeanModeUpperSlopes();
            getSumModeUpperSlopes();
            getVarModeUpperSlopes();
            getStdDevModeUpperSlopes();
            getMedianModeUpperSlopes();
            getNumModeUpperSlopeSamples();

            getModeLowerSlopes();
            getMaxModeLowerSlopes();
            getMinModeLowerSlopes();
            getMeanModeLowerSlopes();
            getSumModeLowerSlopes();
            getVarModeLowerSlopes();
            getStdDevModeLowerSlopes();
            getMedianModeLowerSlopes();
            getNumModeLowerSlopeSamples();

            getModalDistortionAngles();
            getModalDistortionAngleXPositions();

            getModeBDCAreas();
            getMaxModeBDCAreas();
            getMinModeBDCAreas();
            getMeanModeBDCAreas();
            getSumModeBDCAreas();
            getVarModeBDCAreas();
            getStdDevModeBDCAreas();
            getMedianModeBDCAreas();
            getNumModeBDCAreaSamples();

            getMaxModeBDCAreaXLengths();

            //////////////////////////////////

            getMiddleLineRMSValues();

            /////////////////////////////////

            getMiddleLineSlopes();

            /////////////////////////////////

            getMaxContinuityValue();
            getMinContinuityValue();
            getMeanContinuityValues();
            getSumContinuityValues();
            getVarContinuityValues();
            getStdDevContinuityValues();
            getMedianContinuityValues();
            getNumContinuityValues();
            getMaxPrGap();
            getMaxLowerPr();
            getMinUpperPr();
            //
            ///
            ////////////////////////////////////////////////////////////////////////////////////
            ///
            //


            string error_message = "";
            int error_number = RESULTS_NO_ERROR;
            if (_numSuperModes < MIN_NUMBER_OF_MODES)
            {
                _qaDetectedError |= RESULTS_GENERAL_ERROR | RESULTS_INVALID_NUMBER_OF_LINES;
            }

            getQAError(_qaDetectedError, ref error_number, ref error_message);

            if (_qaDetectedError > RESULTS_NO_ERROR)
                _screeningPassed = false;


            _qaAnalysisComplete = true;

        }

        public void runPassFailAnalysis()
        {

            if (_qaAnalysisComplete)
            {
                _screeningPassed = true;

                _smPassFailThresholds = getPassFailThresholds();

                // Set thresholds for this mode
                _maxModeBordersRemovedAnalysis.threshold = _smPassFailThresholds._maxModeBordersRemoved;
                _maxModeWidthPFAnalysis.threshold = _smPassFailThresholds._maxModeWidth;
                _minModeWidthPFAnalysis.threshold = _smPassFailThresholds._minModeWidth;

                _lowerMaxModeBDCAreaPFAnalysis.threshold = _smPassFailThresholds._maxModeBDCArea;
                _lowerMaxModeBDCAreaXLengthPFAnalysis.threshold = _smPassFailThresholds._maxModeBDCAreaXLength;
                _lowerSumModeBDCAreasPFAnalysis.threshold = _smPassFailThresholds._sumModeBDCAreas;
                _lowerNumModeBDCAreasPFAnalysis.threshold = _smPassFailThresholds._numModeBDCAreas;

                _upperMaxModeBDCAreaPFAnalysis.threshold = _smPassFailThresholds._maxModeBDCArea;
                _upperMaxModeBDCAreaXLengthPFAnalysis.threshold = _smPassFailThresholds._maxModeBDCAreaXLength;
                _upperSumModeBDCAreasPFAnalysis.threshold = _smPassFailThresholds._sumModeBDCAreas;
                _upperNumModeBDCAreasPFAnalysis.threshold = _smPassFailThresholds._numModeBDCAreas;

                _lowerModalDistortionAnglePFAnalysis.threshold = _smPassFailThresholds._modalDistortionAngle;
                _lowerModalDistortionAngleXPositionPFAnalysis.threshold = _smPassFailThresholds._modalDistortionAngleXPosition;

                _upperModalDistortionAnglePFAnalysis.threshold = _smPassFailThresholds._modalDistortionAngle;
                _upperModalDistortionAngleXPositionPFAnalysis.threshold = _smPassFailThresholds._modalDistortionAngleXPosition;

                _middleLineRMSValuesPFAnalysis.threshold = _smPassFailThresholds._middleLineRMSValue;
                _maxMiddleLineSlopePFAnalysis.threshold = _smPassFailThresholds._maxMiddleLineSlope;
                _minMiddleLineSlopePFAnalysis.threshold = _smPassFailThresholds._minMiddleLineSlope;
                _maxPrGapPFAnalysis.threshold = _smPassFailThresholds._maxPrGap;
                _maxLowerPrPFAnalysis.threshold = _smPassFailThresholds._maxLowerPr;
                _minUpperPrPFAnalysis.threshold = _smPassFailThresholds._minUpperPr;

                _lowerMaxModeLineSlopePFAnalysis.threshold = _smPassFailThresholds._maxModeLineSlope;
                _upperMaxModeLineSlopePFAnalysis.threshold = _smPassFailThresholds._maxModeLineSlope;

                _lowerMinModeLineSlopePFAnalysis.threshold = _smPassFailThresholds._minModeLineSlope;
                _upperMinModeLineSlopePFAnalysis.threshold = _smPassFailThresholds._minModeLineSlope;

                // run pass fail
                _maxModeWidthPFAnalysis.runPassFailWithUpperLimit(_maxModeWidths);
                _minModeWidthPFAnalysis.runPassFailWithLowerLimit(_minModeWidths);

                _lowerMaxModeBDCAreaPFAnalysis.runPassFailWithUpperLimit(_lowerMaxModeBDCAreas);
                _lowerMaxModeBDCAreaXLengthPFAnalysis.runPassFailWithUpperLimit(_lowerMaxModeBDCAreaXLengths);
                _lowerSumModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_lowerSumModeBDCAreas);
                _lowerNumModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_lowerNumModeBDCAreaSamples);

                _upperMaxModeBDCAreaPFAnalysis.runPassFailWithUpperLimit(_upperMaxModeBDCAreas);
                _upperMaxModeBDCAreaXLengthPFAnalysis.runPassFailWithUpperLimit(_upperMaxModeBDCAreaXLengths);
                _upperSumModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_upperSumModeBDCAreas);
                _upperNumModeBDCAreasPFAnalysis.runPassFailWithUpperLimit(_upperNumModeBDCAreaSamples);

                _lowerModalDistortionAnglePFAnalysis.runPassFailWithUpperLimit(_lowerModalDistortionAngles);
                _lowerModalDistortionAngleXPositionPFAnalysis.runPassFailWithUpperLimit(_lowerModalDistortionAngleXPositions);

                _upperModalDistortionAnglePFAnalysis.runPassFailWithUpperLimit(_upperModalDistortionAngles);
                _upperModalDistortionAngleXPositionPFAnalysis.runPassFailWithUpperLimit(_upperModalDistortionAngleXPositions);

                _middleLineRMSValuesPFAnalysis.runPassFailWithUpperLimit(_middleLineRMSValues);
                _maxMiddleLineSlopePFAnalysis.runPassFailWithUpperLimit(_middleLineSlopes);
                _minMiddleLineSlopePFAnalysis.runPassFailWithLowerLimit(_middleLineSlopes);

                _maxPrGapPFAnalysis.runPassFailWithUpperLimit(_maxPrGap);
                _maxLowerPrPFAnalysis.runPassFailWithUpperLimit(_maxLowerPr);
                _minUpperPrPFAnalysis.runPassFailWithLowerLimit(_minUpperPr);

                _lowerMaxModeLineSlopePFAnalysis.runPassFailWithUpperLimit(_maxModeLowerSlopes);
                _upperMaxModeLineSlopePFAnalysis.runPassFailWithUpperLimit(_maxModeUpperSlopes);
                _lowerMinModeLineSlopePFAnalysis.runPassFailWithLowerLimit(_minModeLowerSlopes);
                _upperMinModeLineSlopePFAnalysis.runPassFailWithLowerLimit(_minModeUpperSlopes);


                if (!(_numSuperModesRemoved <= _maxModeBordersRemovedAnalysis.threshold) ||
                    !(_maxModeWidthPFAnalysis.pass) ||
                    !(_minModeWidthPFAnalysis.pass) ||
                    !(_lowerMaxModeBDCAreaPFAnalysis.pass) ||
                    !(_lowerMaxModeBDCAreaXLengthPFAnalysis.pass) ||
                    !(_lowerSumModeBDCAreasPFAnalysis.pass) ||
                    !(_lowerNumModeBDCAreasPFAnalysis.pass) ||
                    !(_upperMaxModeBDCAreaPFAnalysis.pass) ||
                    !(_upperMaxModeBDCAreaXLengthPFAnalysis.pass) ||
                    !(_upperSumModeBDCAreasPFAnalysis.pass) ||
                    !(_upperNumModeBDCAreasPFAnalysis.pass) ||
                    !(_lowerModalDistortionAnglePFAnalysis.pass) ||
                    !(_lowerModalDistortionAngleXPositionPFAnalysis.pass) ||
                    !(_upperModalDistortionAnglePFAnalysis.pass) ||
                    !(_upperModalDistortionAngleXPositionPFAnalysis.pass) ||
                    !(_middleLineRMSValuesPFAnalysis.pass) ||
                    !(_maxMiddleLineSlopePFAnalysis.pass) ||
                    !(_minMiddleLineSlopePFAnalysis.pass) ||
                    !(_lowerMaxModeLineSlopePFAnalysis.pass) ||
                    !(_upperMaxModeLineSlopePFAnalysis.pass) ||
                    !(_lowerMinModeLineSlopePFAnalysis.pass) ||
                    !(_upperMinModeLineSlopePFAnalysis.pass) ||
                    !(_maxPrGapPFAnalysis.pass) ||
                    !(_maxLowerPrPFAnalysis.pass) ||
                    !(_minUpperPrPFAnalysis.pass))
                {
                    _screeningPassed = false;
                }

            }

            //////////////////////////////////////////////////////////////////////////////////
            ///
            // analyse results
            //
            if (!(_maxModeWidthPFAnalysis.pass))
                _qaDetectedError |= RESULTS_GENERAL_ERROR | RESULTS_LINES_MISSING;
            if (_numSuperModes < MIN_NUMBER_OF_MODES)
                _qaDetectedError |= RESULTS_GENERAL_ERROR | RESULTS_INVALID_NUMBER_OF_LINES;

            if (_qaDetectedError > RESULTS_NO_ERROR)
                _screeningPassed = false;

            _passFailAnalysisComplete = true;

        }


        ////////////////////////////////////////////////////////////////////////////////////////
        ///
        //	Data Gathering Section - gather analysis data of the supermodes
        //

        //
        /////////////////////////////////////////////////////////////////////////////
        ///
        //	Mode Widths
        public void getModeWidths()
        {
            if (_modeWidths.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    Vector modeWidths;
                    _p_superModes[i]._modeAnalysis.getModeWidths(out modeWidths);

                    _modeWidths.Add(modeWidths);
                }
            }
        }

        public void getMaxModeWidths()
        {
            if (_maxModeWidths.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                    _maxModeWidths.Add(_p_superModes[i]._modeAnalysis.getModeWidthsStatistic(VectorAttribute.max));
            }
        }
        public void getMinModeWidths()
        {
            if (_minModeWidths.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                    _minModeWidths.Add(_p_superModes[i]._modeAnalysis.getModeWidthsStatistic(VectorAttribute.min));
            }
        }
        public void getMeanModeWidths()
        {
            if (_meanModeWidths.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                    _meanModeWidths.Add(_p_superModes[i]._modeAnalysis.getModeWidthsStatistic(VectorAttribute.mean));
            }
        }
        public void getSumModeWidths()
        {
            if (_sumModeWidths.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                    _sumModeWidths.Add(_p_superModes[i]._modeAnalysis.getModeWidthsStatistic(VectorAttribute.sum));
            }
        }
        public void getVarModeWidths()
        {
            if (_varModeWidths.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                    _varModeWidths.Add(_p_superModes[i]._modeAnalysis.getModeWidthsStatistic(VectorAttribute.variance));
            }
        }
        public void getStdDevModeWidths()
        {
            if (_stdDevModeWidths.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                    _stdDevModeWidths.Add(_p_superModes[i]._modeAnalysis.getModeWidthsStatistic(VectorAttribute.stdDev));
            }
        }
        public void getMedianModeWidths()
        {
            if (_medianModeWidths.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                    _medianModeWidths.Add(_p_superModes[i]._modeAnalysis.getModeWidthsStatistic(VectorAttribute.median));
            }
        }
        public void getNumModeWidthSamples()
        {
            if (_numModeWidthSamples.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                    _numModeWidthSamples.Add(_p_superModes[i]._modeAnalysis.getModeWidthsStatistic(VectorAttribute.count));
            }
        }

        //
        /////////////////////////////////////////////////////////////////////////////
        ///
        //	Mode Upper Slopes
        //
        public void getModeUpperSlopes()
        {
            if (_modeUpperSlopes.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    Vector modeUpperSlopes;
                    _p_superModes[i]._modeAnalysis.getModeUpperSlopes(out modeUpperSlopes);

                    _modeUpperSlopes.Add(modeUpperSlopes);
                }
            }
        }

        public void getMaxModeUpperSlopes()
        {
            if (_maxModeUpperSlopes.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                    _maxModeUpperSlopes.Add(_p_superModes[i]._modeAnalysis.getModeUpperSlopesStatistic(VectorAttribute.max));
            }
        }
        public void getMinModeUpperSlopes()
        {
            if (_minModeUpperSlopes.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                    _minModeUpperSlopes.Add(_p_superModes[i]._modeAnalysis.getModeUpperSlopesStatistic(VectorAttribute.min));
            }
        }
        public void getMeanModeUpperSlopes()
        {
            if (_meanModeUpperSlopes.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                    _meanModeUpperSlopes.Add(_p_superModes[i]._modeAnalysis.getModeUpperSlopesStatistic(VectorAttribute.mean));
            }
        }
        public void getSumModeUpperSlopes()
        {
            if (_sumModeUpperSlopes.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                    _sumModeUpperSlopes.Add(_p_superModes[i]._modeAnalysis.getModeUpperSlopesStatistic(VectorAttribute.sum));
            }
        }
        public void getVarModeUpperSlopes()
        {
            if (_varModeUpperSlopes.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                    _varModeUpperSlopes.Add(_p_superModes[i]._modeAnalysis.getModeUpperSlopesStatistic(VectorAttribute.variance));
            }
        }
        public void getStdDevModeUpperSlopes()
        {
            if (_stdDevModeUpperSlopes.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                    _stdDevModeUpperSlopes.Add(_p_superModes[i]._modeAnalysis.getModeUpperSlopesStatistic(VectorAttribute.stdDev));
            }
        }
        public void getMedianModeUpperSlopes()
        {
            if (_medianModeUpperSlopes.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                    _medianModeUpperSlopes.Add(_p_superModes[i]._modeAnalysis.getModeUpperSlopesStatistic(VectorAttribute.median));
            }
        }
        public void getNumModeUpperSlopeSamples()
        {
            if (_numModeUpperSlopeSamples.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                    _numModeUpperSlopeSamples.Add(_p_superModes[i]._modeAnalysis.getModeUpperSlopesStatistic(VectorAttribute.count));
            }
        }

        //
        /////////////////////////////////////////////////////////////////////////////
        ///
        //	Mode Lower Slopes
        //
        public void getModeLowerSlopes()
        {
            if (_modeLowerSlopes.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    Vector modeLowerSlopes;
                    _p_superModes[i]._modeAnalysis.getModeLowerSlopes(out modeLowerSlopes);

                    _modeLowerSlopes.Add(modeLowerSlopes);
                }
            }
        }

        public void getMaxModeLowerSlopes()
        {
            if (_maxModeLowerSlopes.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                    _maxModeLowerSlopes.Add(_p_superModes[i]._modeAnalysis.getModeLowerSlopesStatistic(VectorAttribute.max));
            }
        }
        public void getMinModeLowerSlopes()
        {
            if (_minModeLowerSlopes.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                    _minModeLowerSlopes.Add(_p_superModes[i]._modeAnalysis.getModeLowerSlopesStatistic(VectorAttribute.min));
            }
        }
        public void getMeanModeLowerSlopes()
        {
            if (_meanModeLowerSlopes.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                    _meanModeLowerSlopes.Add(_p_superModes[i]._modeAnalysis.getModeLowerSlopesStatistic(VectorAttribute.mean));
            }
        }
        public void getSumModeLowerSlopes()
        {
            if (_sumModeLowerSlopes.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                    _sumModeLowerSlopes.Add(_p_superModes[i]._modeAnalysis.getModeLowerSlopesStatistic(VectorAttribute.sum));
            }
        }
        public void getVarModeLowerSlopes()
        {
            if (_varModeLowerSlopes.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                    _varModeLowerSlopes.Add(_p_superModes[i]._modeAnalysis.getModeLowerSlopesStatistic(VectorAttribute.variance));
            }
        }
        public void getStdDevModeLowerSlopes()
        {
            if (_stdDevModeLowerSlopes.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                    _stdDevModeLowerSlopes.Add(_p_superModes[i]._modeAnalysis.getModeLowerSlopesStatistic(VectorAttribute.stdDev));
            }
        }
        public void getMedianModeLowerSlopes()
        {
            if (_medianModeLowerSlopes.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                    _medianModeLowerSlopes.Add(_p_superModes[i]._modeAnalysis.getModeLowerSlopesStatistic(VectorAttribute.median));
            }
        }
        public void getNumModeLowerSlopeSamples()
        {
            if (_numModeLowerSlopeSamples.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                    _numModeLowerSlopeSamples.Add(_p_superModes[i]._modeAnalysis.getModeLowerSlopesStatistic(VectorAttribute.count));
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        ///
        //	Modal Distortion
        //
        public void getModalDistortionAngles()
        {
            if (_lowerModalDistortionAngles.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    _lowerModalDistortionAngles.Add(_p_superModes[i]._modeAnalysis.getLowerModeModalDistortionAngle());
                    _upperModalDistortionAngles.Add(_p_superModes[i]._modeAnalysis.getUpperModeModalDistortionAngle());
                }
            }
        }
        public void getModalDistortionAngleXPositions()
        {
            if (_lowerModalDistortionAngleXPositions.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    _lowerModalDistortionAngleXPositions.Add(_p_superModes[i]._modeAnalysis.getLowerModeMaxModalDistortionAngleXPosition());
                    _upperModalDistortionAngleXPositions.Add(_p_superModes[i]._modeAnalysis.getUpperModeMaxModalDistortionAngleXPosition());
                }
            }
        }

        /////////////////////////////////////////////////////////////////////////////
        ///
        //	Mode Boundary Direction Change Area measurements
        //
        public void getModeBDCAreas()
        {
            if (_lowerModeBDCAreas.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    Vector lowerModeBDCAreas;
                    Vector upperModeBDCAreas;
                    _p_superModes[i]._modeAnalysis.getModeBDCAreas(out lowerModeBDCAreas, out upperModeBDCAreas);

                    _lowerModeBDCAreas.Add(lowerModeBDCAreas);
                    _upperModeBDCAreas.Add(upperModeBDCAreas);
                }
            }
        }

        public void getMaxModeBDCAreas()
        {
            if (_lowerMaxModeBDCAreas.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    _lowerMaxModeBDCAreas.Add(_p_superModes[i]._modeAnalysis.getLowerModeBDCAreasStatistic(VectorAttribute.max));
                    _upperMaxModeBDCAreas.Add(_p_superModes[i]._modeAnalysis.getUpperModeBDCAreasStatistic(VectorAttribute.max));
                }
            }
        }
        public void getMinModeBDCAreas()
        {
            if (_lowerMinModeBDCAreas.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    _lowerMinModeBDCAreas.Add(_p_superModes[i]._modeAnalysis.getLowerModeBDCAreasStatistic(VectorAttribute.min));
                    _upperMinModeBDCAreas.Add(_p_superModes[i]._modeAnalysis.getUpperModeBDCAreasStatistic(VectorAttribute.min));
                }
            }
        }
        public void getMeanModeBDCAreas()
        {
            if (_lowerMeanModeBDCAreas.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    _lowerMeanModeBDCAreas.Add(_p_superModes[i]._modeAnalysis.getLowerModeBDCAreasStatistic(VectorAttribute.mean));
                    _upperMeanModeBDCAreas.Add(_p_superModes[i]._modeAnalysis.getUpperModeBDCAreasStatistic(VectorAttribute.mean));
                }
            }
        }
        public void getSumModeBDCAreas()
        {
            if (_lowerSumModeBDCAreas.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    _lowerSumModeBDCAreas.Add(_p_superModes[i]._modeAnalysis.getLowerModeBDCAreasStatistic(VectorAttribute.sum));
                    _upperSumModeBDCAreas.Add(_p_superModes[i]._modeAnalysis.getUpperModeBDCAreasStatistic(VectorAttribute.sum));
                }
            }
        }
        public void getVarModeBDCAreas()
        {
            if (_lowerVarModeBDCAreas.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    _lowerVarModeBDCAreas.Add(_p_superModes[i]._modeAnalysis.getLowerModeBDCAreasStatistic(VectorAttribute.variance));
                    _upperVarModeBDCAreas.Add(_p_superModes[i]._modeAnalysis.getUpperModeBDCAreasStatistic(VectorAttribute.variance));
                }
            }
        }
        public void getStdDevModeBDCAreas()
        {
            if (_lowerStdDevModeBDCAreas.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    _lowerStdDevModeBDCAreas.Add(_p_superModes[i]._modeAnalysis.getLowerModeBDCAreasStatistic(VectorAttribute.stdDev));
                    _upperStdDevModeBDCAreas.Add(_p_superModes[i]._modeAnalysis.getUpperModeBDCAreasStatistic(VectorAttribute.stdDev));
                }
            }
        }
        public void getMedianModeBDCAreas()
        {
            if (_lowerMedianModeBDCAreas.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    _lowerMedianModeBDCAreas.Add(_p_superModes[i]._modeAnalysis.getLowerModeBDCAreasStatistic(VectorAttribute.median));
                    _upperMedianModeBDCAreas.Add(_p_superModes[i]._modeAnalysis.getUpperModeBDCAreasStatistic(VectorAttribute.median));
                }
            }
        }
        public void getNumModeBDCAreaSamples()
        {
            if (_lowerNumModeBDCAreaSamples.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    _lowerNumModeBDCAreaSamples.Add(_p_superModes[i]._modeAnalysis.getLowerModeBDCAreasStatistic(VectorAttribute.count));
                    _upperNumModeBDCAreaSamples.Add(_p_superModes[i]._modeAnalysis.getUpperModeBDCAreasStatistic(VectorAttribute.count));
                }
            }
        }

        /////////////////////////////////////////////////////////////////////////////
        ///
        //	Mode Boundary Direction Change Area X Length measurements
        //
        public void getModeBDCAreaXLengths()
        {
            if (_lowerModeBDCAreaXLengths.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    Vector lowerModeBDCAreaXLengths;
                    Vector upperModeBDCAreaXLengths;
                    _p_superModes[i]._modeAnalysis.getModeBDCAreaXLengths(out lowerModeBDCAreaXLengths, out upperModeBDCAreaXLengths);

                    _lowerModeBDCAreaXLengths.Add(lowerModeBDCAreaXLengths);
                    _upperModeBDCAreaXLengths.Add(upperModeBDCAreaXLengths);
                }
            }
        }

        public void getMaxModeBDCAreaXLengths()
        {
            if (_lowerMaxModeBDCAreaXLengths.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    _lowerMaxModeBDCAreaXLengths.Add(_p_superModes[i]._modeAnalysis.getLowerModeBDCAreaXLengthsStatistic(VectorAttribute.max));
                    _upperMaxModeBDCAreaXLengths.Add(_p_superModes[i]._modeAnalysis.getUpperModeBDCAreaXLengthsStatistic(VectorAttribute.max));
                }
            }
        }
        public void getMinModeBDCAreaXLengths()
        {
            if (_lowerMinModeBDCAreaXLengths.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    _lowerMinModeBDCAreaXLengths.Add(_p_superModes[i]._modeAnalysis.getLowerModeBDCAreaXLengthsStatistic(VectorAttribute.min));
                    _upperMinModeBDCAreaXLengths.Add(_p_superModes[i]._modeAnalysis.getUpperModeBDCAreaXLengthsStatistic(VectorAttribute.min));
                }
            }
        }
        public void getMeanModeBDCAreaXLengths()
        {
            if (_lowerMeanModeBDCAreaXLengths.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    _lowerMeanModeBDCAreaXLengths.Add(_p_superModes[i]._modeAnalysis.getLowerModeBDCAreaXLengthsStatistic(VectorAttribute.mean));
                    _upperMeanModeBDCAreaXLengths.Add(_p_superModes[i]._modeAnalysis.getUpperModeBDCAreaXLengthsStatistic(VectorAttribute.mean));
                }
            }
        }
        public void getSumModeBDCAreaXLengths()
        {
            if (_lowerSumModeBDCAreaXLengths.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    _lowerSumModeBDCAreaXLengths.Add(_p_superModes[i]._modeAnalysis.getLowerModeBDCAreaXLengthsStatistic(VectorAttribute.sum));
                    _upperSumModeBDCAreaXLengths.Add(_p_superModes[i]._modeAnalysis.getUpperModeBDCAreaXLengthsStatistic(VectorAttribute.sum));
                }
            }
        }
        public void getVarModeBDCAreaXLengths()
        {
            if (_lowerVarModeBDCAreaXLengths.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    _lowerVarModeBDCAreaXLengths.Add(_p_superModes[i]._modeAnalysis.getLowerModeBDCAreaXLengthsStatistic(VectorAttribute.variance));
                    _upperVarModeBDCAreaXLengths.Add(_p_superModes[i]._modeAnalysis.getUpperModeBDCAreaXLengthsStatistic(VectorAttribute.variance));
                }
            }
        }
        public void getStdDevModeBDCAreaXLengths()
        {
            if (_lowerStdDevModeBDCAreaXLengths.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    _lowerStdDevModeBDCAreaXLengths.Add(_p_superModes[i]._modeAnalysis.getLowerModeBDCAreaXLengthsStatistic(VectorAttribute.stdDev));
                    _upperStdDevModeBDCAreaXLengths.Add(_p_superModes[i]._modeAnalysis.getUpperModeBDCAreaXLengthsStatistic(VectorAttribute.stdDev));
                }
            }
        }
        public void getMedianModeBDCAreaXLengths()
        {
            if (_lowerMedianModeBDCAreaXLengths.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    _lowerMedianModeBDCAreaXLengths.Add(_p_superModes[i]._modeAnalysis.getLowerModeBDCAreaXLengthsStatistic(VectorAttribute.median));
                    _upperMedianModeBDCAreaXLengths.Add(_p_superModes[i]._modeAnalysis.getUpperModeBDCAreaXLengthsStatistic(VectorAttribute.median));
                }
            }
        }
        public void getNumModeBDCAreaXLengthSamples()
        {
            if (_lowerNumModeBDCAreaXLengthSamples.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    _lowerNumModeBDCAreaXLengthSamples.Add(_p_superModes[i]._modeAnalysis.getLowerModeBDCAreaXLengthsStatistic(VectorAttribute.count));
                    _upperNumModeBDCAreaXLengthSamples.Add(_p_superModes[i]._modeAnalysis.getUpperModeBDCAreaXLengthsStatistic(VectorAttribute.count));
                }
            }
        }

        //
        /////////////////////////////////////////////////////////////////////////////
        ///
        //	MiddleLine RootMeanSquare analysis
        //
        public void getMiddleLineRMSValues()
        {
            if (_middleLineRMSValues.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    double rmsValue = 0;
                    _p_superModes[i].getMiddleLineRMSValue(ref rmsValue);

                    _middleLineRMSValues.Add(rmsValue);
                }
            }
        }

        public void getMiddleLineSlopes()
        {
            if (_middleLineSlopes.Count == 0)
            {
                for (int i = 0; i < _numSuperModes; i++)
                {
                    double slope = 0;
                    _p_superModes[i].getMiddleLineSlope(ref slope);

                    _middleLineSlopes.Add(slope);
                }
            }
        }
        //
        /////////////////////////////////////////////////////////////////////////////
        ///
        //	Continuity analysis
        //
        public void getMaxContinuityValue()
        {
            if (_maxContinuityValue == 0)
                _maxContinuityValue = _continuityValues.getVectorAttribute(VectorAttribute.max);
        }

        public void getMinContinuityValue()
        {
            if (_minContinuityValue == 0)
                _minContinuityValue = _continuityValues.getVectorAttribute(VectorAttribute.min);
        }

        public void getMeanContinuityValues()
        {
            if (_meanContinuityValues == 0)
                _meanContinuityValues = _continuityValues.getVectorAttribute(VectorAttribute.mean);
        }

        public void getSumContinuityValues()
        {
            if (_sumContinuityValues == 0)
                _sumContinuityValues = _continuityValues.getVectorAttribute(VectorAttribute.sum);
        }

        public void getVarContinuityValues()
        {
            if (_varContinuityValues == 0)
                _varContinuityValues = _continuityValues.getVectorAttribute(VectorAttribute.variance);
        }

        public void getStdDevContinuityValues()
        {
            if (_stdDevContinuityValues == 0)
                _stdDevContinuityValues = _continuityValues.getVectorAttribute(VectorAttribute.stdDev);
        }

        public void getMedianContinuityValues()
        {
            if (_medianContinuityValues == 0)
                _medianContinuityValues = _continuityValues.getVectorAttribute(VectorAttribute.median);
        }

        public void getNumContinuityValues()
        {
            if (_numContinuityValues == 0)
                _numContinuityValues = _continuityValues.getVectorAttribute(VectorAttribute.count);
        }

        void getMaxPrGap()
        {
            if (_maxPrGap == 0)
            {
                _maxPrGap = _continuityValues.getVectorAttribute(VectorAttribute.maxGap);
            }
        }

        void getMaxLowerPr()
        {
            if (_maxLowerPr == 0)
            {
                _maxLowerPr = _continuityValues.getVectorAttribute(VectorAttribute.min);
            }
        }
        void getMinUpperPr()
        {
            if (_minUpperPr == 0)
            {
                _minUpperPr = _continuityValues.getVectorAttribute(VectorAttribute.max);
            }
        }



        int numSuperModes()
        {
            return _numSuperModes;
        }

        PassFailThresholds getPassFailThresholds()
{
	PassFailThresholds thresholds = new PassFailThresholds();

    thresholds._maxModeBordersRemoved = Convert.ToInt32(  Convert.ToInt32( overAllMapQASitting["MaxNumberofModeBordersRemoved"]));
    thresholds._maxModeWidth = Convert.ToInt32(  overAllMapQASitting["MaxModeWidth"]);
	thresholds._minModeWidth					= Convert.ToInt32( overAllMapQASitting["MinModeWidth"]);
	thresholds._maxModeBDCArea					= Convert.ToInt32(  overAllMapQASitting["MaxModeBDCArea"]);
	thresholds._maxModeBDCAreaXLength			= Convert.ToInt32(  overAllMapQASitting["MaxModeBDCAreaXLength"]);
	thresholds._sumModeBDCAreas					= Convert.ToInt32(  overAllMapQASitting["SumModeBDCAreas"]);
	thresholds._numModeBDCAreas					= Convert.ToInt32(  overAllMapQASitting["NumModeBDCAreas"]);
	thresholds._modalDistortionAngle			= Convert.ToInt32(  overAllMapQASitting["MaxModalDistortionAngle"]);
	thresholds._modalDistortionAngleXPosition	= Convert.ToInt32(  overAllMapQASitting["MaxModalDistortionAngleXPosition"]);
	thresholds._middleLineRMSValue				= Convert.ToInt32(  overAllMapQASitting["MaxMiddleLineRMSValue"]);
	thresholds._minMiddleLineSlope				= Convert.ToInt32(  overAllMapQASitting["MinMiddleLineSlope"]);
    thresholds._maxMiddleLineSlope              = Convert.ToInt32(overAllMapQASitting["MaxMiddleLineSlope"]);
	thresholds._maxPrGap						= Convert.ToDouble(  overAllMapQASitting["MaxPrGapinContinuitydata"]);
	thresholds._maxLowerPr						= Convert.ToDouble(  overAllMapQASitting["MaxLowerPrinContinuitydata"]);
	thresholds._minUpperPr						= Convert.ToDouble(  overAllMapQASitting["MinUpperPrinContinuitydata"]);
	thresholds._maxModeLineSlope				= Convert.ToInt32(  overAllMapQASitting["MaxModeLineSlope"]);
	thresholds._minModeLineSlope				= Convert.ToInt32(  overAllMapQASitting["MinModeLineSlope"]);

	return thresholds;
}
        //
        //
        ///	End of OverallModeMapAnalysis.cpp
        ///////////////////////////////////////////////////////////////////////////////
        public string createQaResultsAbsFilePath(string resultsDir, string laserId, string dateTimeStamp)
        {
            string absFilePath = "";

            absFilePath += resultsDir;
            absFilePath += "\\";
            absFilePath += QA_METRICS_FILE_NAME;
            absFilePath += USCORE;
            absFilePath += "OverallMap";
            absFilePath += USCORE;
            absFilePath += LASER_TYPE_ID_DSDBR01_CSTRING;
            absFilePath += USCORE;
            absFilePath += laserId;
            absFilePath += USCORE;
            absFilePath += dateTimeStamp;
            absFilePath += CSV;

            return absFilePath;
        }
        public string createPassFailResultsAbsFilePath(string resultsDir, string laserId, string dateTimeStamp)
        {
            string absFilePath = "";

            absFilePath += resultsDir;
            absFilePath += "\\";
            absFilePath += PASSFAIL_FILE_NAME;
            absFilePath += USCORE;
            absFilePath += "OverallMap";
            absFilePath += USCORE;
            absFilePath += LASER_TYPE_ID_DSDBR01_CSTRING;
            absFilePath += USCORE;
            absFilePath += laserId;
            absFilePath += USCORE;
            absFilePath += dateTimeStamp;
            absFilePath += CSV;

            return absFilePath;
        }
        public string createPassFailCollateResultsAbsFilePath(string resultsDir, string laserId, string dateTimeStamp)
        {
            string absFilePath = "";

            absFilePath += resultsDir;
            absFilePath += "\\";
            absFilePath += COLLATED_PASSFAIL_FILE_NAME;
            absFilePath += USCORE;
            absFilePath += LASER_TYPE_ID_DSDBR01_CSTRING;
            absFilePath += USCORE;
            absFilePath += laserId;
            absFilePath += USCORE;
            absFilePath += dateTimeStamp;
            absFilePath += CSV;

            return absFilePath;
        }


        public void writeQaResultsToFile(string fileFullName)
        {


            using (StreamWriter sw = new StreamWriter(fileFullName, true))
            {

                string errorMessage = "";
                int errorNum = 0;
                getQAError(_qaDetectedError,ref errorNum, ref errorMessage);;

                sw.WriteLine(errorMessage);
                sw.WriteLine();

                string cellString = "";
              
                sw.Write("Slope Window Size,");
                cellString = string.Format("{0}",Convert.ToInt32( this.overAllMapQASitting["SlopeWindowSize"]));
                sw.WriteLine(cellString);

                sw.Write("Modal Distortion Min X,");
                cellString = string.Format("{0}", Convert.ToInt32(this.overAllMapQASitting["ModalDistortionMinX"]));
                sw.WriteLine(cellString);

                sw.Write("Modal Distortion Max X,");
                cellString= string.Format("{0}", Convert.ToInt32( this.overAllMapQASitting["ModalDistortionMaxX"]));
                sw.WriteLine(cellString);

                sw.Write("Modal Distortion Min Y,");
                cellString = string.Format("{0}", Convert.ToInt32( this.overAllMapQASitting["ModalDistortionMinY"]));
                sw.WriteLine(cellString);
                sw.WriteLine();
                
                sw.Write("Max Pr Gap in Continuity Data,");
                cellString = string.Format("{0}", _maxPrGap);
                sw.WriteLine(cellString);

                sw.Write("Lower Pr in Continuity Data,");
                cellString = string.Format("{0}", _maxLowerPr);
                sw.WriteLine(cellString);

                sw.Write("Upper Pr in Continuity Data,");
                cellString = string.Format("{0}", _minUpperPr);
                sw.WriteLine(cellString);
                sw.WriteLine();

                //////////////////////////////////////////
                const string QA_XLS_HEADER1 = " ,";
                const string QA_XLS_HEADER2 = "Index,";
                const string QA_XLS_HEADER3 = "Function,";
                const string QA_XLS_HEADER4 = "Slope of upper mode boundary line,";
                const string QA_XLS_HEADER5 = "Slope of lower mode boundary line,";
                const string QA_XLS_HEADER6 = "Mode Width,";
                const string QA_XLS_HEADER7 = "Max Lower Modal Distortion Angle,";
                const string QA_XLS_HEADER8 = "Max Upper Modal Distortion Angle,";
                const string QA_XLS_HEADER9 = "Max Lower Modal Distortion X-Position,";
                const string QA_XLS_HEADER10 = "Max Upper Modal Distortion X-Position,";
                const string QA_XLS_HEADER11 = "Max Lower Boundary Direction Change X-Length,";
                const string QA_XLS_HEADER12 = "Max Upper Boundary Direction Change X-Length,";
                const string QA_XLS_HEADER13 = "Max Lower Boundary Direction Change Area,";
                const string QA_XLS_HEADER14 = "Max Upper Boundary Direction Change Area,";
                const string QA_XLS_HEADER15 = "Sum of Lower Boundary Direction Change Areas,";
                const string QA_XLS_HEADER16 = "Sum of Upper Boundary Direction Change Areas,";
                const string QA_XLS_HEADER17 = "Number of Lower Boundary Direction Changes,";
                const string QA_XLS_HEADER18 = "Number of Upper Boundary Direction Changes,";
                const string QA_XLS_HEADER19 = "Middle Line RMS Value";

                const string QA_XLS_MAX = "Maximum,";
                const string QA_XLS_MIN = "Minimum,";
                const string QA_XLS_MEAN = "Mean,";
                const string QA_XLS_SUM = "Sum,";
                const string QA_XLS_COUNT = "Number Of Samples,";
                const string QA_XLS_STDDEV = "Standard Deviation,";
                const string QA_XLS_VAR = "Variance,";
                const string QA_XLS_OTHERS = ",";

                List<string> attribArray = new List<string>();
                attribArray.Add(QA_XLS_MAX);
                attribArray.Add(QA_XLS_MIN);
                attribArray.Add(QA_XLS_MEAN);
                attribArray.Add(QA_XLS_SUM);
                attribArray.Add(QA_XLS_COUNT);
                attribArray.Add(QA_XLS_STDDEV);
                attribArray.Add(QA_XLS_VAR);
                attribArray.Add(QA_XLS_OTHERS);

                string QA_XLS_COL1_VALUE = "";
                string QA_XLS_COL2_VALUE = "";
                string QA_XLS_COL3_VALUE = "";
                string QA_XLS_COL4_VALUE = "";
                string QA_XLS_COL5_VALUE = "";
                string QA_XLS_COL6_VALUE = "";
                string QA_XLS_COL7_VALUE = "";
                string QA_XLS_COL8_VALUE = "";
                string QA_XLS_COL9_VALUE = "";
                string QA_XLS_COL10_VALUE = "";
                string QA_XLS_COL11_VALUE = "";
                string QA_XLS_COL12_VALUE = "";
                string QA_XLS_COL13_VALUE = "";
                string QA_XLS_COL14_VALUE = "";
                string QA_XLS_COL15_VALUE = "";
                string QA_XLS_COL16_VALUE = "";
                string QA_XLS_COL17_VALUE = "";
                string QA_XLS_COL18_VALUE = "";
                string QA_XLS_COL19_VALUE = "";

              
                sw.Write(QA_XLS_HEADER1);
                sw.Write(QA_XLS_HEADER2);
                sw.Write(QA_XLS_HEADER3);
                sw.Write(QA_XLS_HEADER4);
                sw.Write(QA_XLS_HEADER5);
                sw.Write(QA_XLS_HEADER6);
                sw.Write(QA_XLS_HEADER7);
                sw.Write(QA_XLS_HEADER8);
                sw.Write(QA_XLS_HEADER9);
                sw.Write(QA_XLS_HEADER10);
                sw.Write(QA_XLS_HEADER11);
                sw.Write(QA_XLS_HEADER12);
                sw.Write(QA_XLS_HEADER13);
                sw.Write(QA_XLS_HEADER14);
                sw.Write(QA_XLS_HEADER15);
                sw.Write(QA_XLS_HEADER16);
                sw.Write(QA_XLS_HEADER17);
                sw.Write(QA_XLS_HEADER18);
                sw.WriteLine(QA_XLS_HEADER19);

                //////////////////////////////////////////////////////////////////////////
                sw.Write(QA_XLS_COL1_VALUE);
                sw.Write(QA_XLS_COL2_VALUE);
                sw.Write(QA_XLS_COL3_VALUE);
                sw.Write(QA_XLS_COL4_VALUE);
                sw.Write(QA_XLS_COL5_VALUE);
                sw.Write(QA_XLS_COL6_VALUE);
                sw.Write(QA_XLS_COL7_VALUE);
                sw.Write(QA_XLS_COL8_VALUE);
                sw.Write(QA_XLS_COL9_VALUE);
                sw.Write(QA_XLS_COL10_VALUE);
                sw.Write(QA_XLS_COL11_VALUE);
                sw.Write(QA_XLS_COL12_VALUE);
                sw.Write(QA_XLS_COL13_VALUE);
                sw.Write(QA_XLS_COL14_VALUE);
                sw.Write(QA_XLS_COL15_VALUE);
                sw.Write(QA_XLS_COL16_VALUE);
                sw.Write(QA_XLS_COL17_VALUE);
                sw.Write(QA_XLS_COL18_VALUE);
                sw.WriteLine(QA_XLS_COL19_VALUE);

                QA_XLS_COL1_VALUE = "";
                QA_XLS_COL10_VALUE = "";

                //////////////////////////////////////////////////////////////////////////

                int QA_XLS_NUM_ROWS_PER_SM = attribArray.Count;

                for (int i = 0; i < _numSuperModes; i++)
                {
                    for (int j = 0; j < QA_XLS_NUM_ROWS_PER_SM; j++)
                    {
                        // clear vars
                        QA_XLS_COL1_VALUE = ",";
                        QA_XLS_COL2_VALUE = ",";
                        QA_XLS_COL3_VALUE = ",";
                        QA_XLS_COL4_VALUE = ",";
                        QA_XLS_COL5_VALUE = ",";
                        QA_XLS_COL6_VALUE = ",";
                        QA_XLS_COL7_VALUE = ",";
                        QA_XLS_COL8_VALUE = ",";
                        QA_XLS_COL9_VALUE = ",";
                        QA_XLS_COL10_VALUE = ",";
                        QA_XLS_COL11_VALUE = ",";
                        QA_XLS_COL12_VALUE = ",";
                        QA_XLS_COL13_VALUE = ",";
                        QA_XLS_COL14_VALUE = ",";
                        QA_XLS_COL15_VALUE = ",";
                        QA_XLS_COL16_VALUE = ",";
                        QA_XLS_COL17_VALUE = ",";
                        QA_XLS_COL18_VALUE = ",";
                        QA_XLS_COL19_VALUE = ",";

                        QA_XLS_COL1_VALUE = ",";
                        QA_XLS_COL2_VALUE= string.Format("{0},", i);
                        QA_XLS_COL3_VALUE = attribArray[j];

                        if (QA_XLS_COL3_VALUE == QA_XLS_MAX)
                        {
                            if (i < (int)_maxModeUpperSlopes.Count)
                                QA_XLS_COL4_VALUE = string.Format("{0},", _maxModeUpperSlopes[i]);
                            else
                                QA_XLS_COL4_VALUE = ",";
                            if (i < (int)_maxModeLowerSlopes.Count)
                                QA_XLS_COL5_VALUE = string.Format("{0},", _maxModeLowerSlopes[i]);
                            else
                                QA_XLS_COL5_VALUE = ",";
                            if (i < (int)_maxModeWidths.Count)
                                QA_XLS_COL6_VALUE = string.Format("{0},", _maxModeWidths[i]);
                            else
                                QA_XLS_COL6_VALUE = "";
                        }
                        else if (QA_XLS_COL3_VALUE == QA_XLS_MIN)
                        {
                            if (i < (int)_minModeUpperSlopes.Count)
                                QA_XLS_COL4_VALUE = string.Format("{0},", _minModeUpperSlopes[i]);
                            else
                                QA_XLS_COL4_VALUE = ",";
                            if (i < (int)_minModeLowerSlopes.Count)
                                QA_XLS_COL5_VALUE = string.Format("{0},", _minModeLowerSlopes[i]);
                            else
                                QA_XLS_COL5_VALUE = ",";
                            if (i < (int)_minModeWidths.Count)
                                QA_XLS_COL6_VALUE = string.Format("{0},", _minModeWidths[i]);
                            else
                                QA_XLS_COL6_VALUE = "";
                        }
                        else if (QA_XLS_COL3_VALUE == QA_XLS_MEAN)
                        {
                            if (i < (int)_meanModeUpperSlopes.Count)
                                QA_XLS_COL4_VALUE = string.Format("{0},", _meanModeUpperSlopes[i]);
                            else
                                QA_XLS_COL4_VALUE = ",";
                            if (i < (int)_meanModeLowerSlopes.Count)
                                QA_XLS_COL5_VALUE = string.Format("{0},", _meanModeLowerSlopes[i]);
                            else
                                QA_XLS_COL5_VALUE = ",";
                            if (i < (int)_meanModeWidths.Count)
                                QA_XLS_COL6_VALUE = string.Format("{0},", _meanModeWidths[i]);
                            else
                                QA_XLS_COL6_VALUE = "";
                        }
                        else if (QA_XLS_COL3_VALUE == QA_XLS_SUM)
                        {
                            if (i < (int)_sumModeUpperSlopes.Count)
                                QA_XLS_COL4_VALUE = string.Format("{0},", _sumModeUpperSlopes[i]);
                            else
                                QA_XLS_COL4_VALUE = ",";
                            if (i < (int)_sumModeLowerSlopes.Count)
                                QA_XLS_COL5_VALUE = string.Format("{0},", _sumModeLowerSlopes[i]);
                            else
                                QA_XLS_COL5_VALUE = ",";
                            if (i < (int)_sumModeWidths.Count)
                                QA_XLS_COL6_VALUE = string.Format("{0},", _sumModeWidths[i]);
                            else
                                QA_XLS_COL6_VALUE = "";
                        }
                        else if (QA_XLS_COL3_VALUE == QA_XLS_COUNT)
                        {
                            if (i < (int)_numModeUpperSlopeSamples.Count)
                                QA_XLS_COL4_VALUE = string.Format("{0},", _numModeUpperSlopeSamples[i]);
                            else
                                QA_XLS_COL4_VALUE = ",";
                            if (i < (int)_numModeLowerSlopeSamples.Count)
                                QA_XLS_COL5_VALUE = string.Format("{0},", _numModeLowerSlopeSamples[i]);
                            else
                                QA_XLS_COL5_VALUE = ",";
                            if (i < (int)_numModeWidthSamples.Count)
                                QA_XLS_COL6_VALUE = string.Format("{0},", _numModeWidthSamples[i]);
                            else
                                QA_XLS_COL6_VALUE = "";
                        }
                        else if (QA_XLS_COL3_VALUE == QA_XLS_STDDEV)
                        {
                            if (i < (int)_stdDevModeUpperSlopes.Count)
                                QA_XLS_COL4_VALUE = string.Format("{0},", _stdDevModeUpperSlopes[i]);
                            else
                                QA_XLS_COL4_VALUE = ",";
                            if (i < (int)_stdDevModeLowerSlopes.Count)
                                QA_XLS_COL5_VALUE = string.Format("{0},", _stdDevModeLowerSlopes[i]);
                            else
                                QA_XLS_COL5_VALUE = ",";
                            if (i < (int)_stdDevModeWidths.Count)
                                QA_XLS_COL6_VALUE = string.Format("{0},", _stdDevModeWidths[i]);
                            else
                                QA_XLS_COL6_VALUE = "";
                        }
                        else if (QA_XLS_COL3_VALUE == QA_XLS_VAR)
                        {
                            if (i < (int)_varModeUpperSlopes.Count)
                                QA_XLS_COL4_VALUE = string.Format("{0},", _varModeUpperSlopes[i]);
                            else
                                QA_XLS_COL4_VALUE = ",";
                            if (i < (int)_varModeLowerSlopes.Count)
                                QA_XLS_COL5_VALUE = string.Format("{0},", _varModeLowerSlopes[i]);
                            else
                                QA_XLS_COL5_VALUE = ",";
                            if (i < (int)_varModeWidths.Count)
                                QA_XLS_COL6_VALUE = string.Format("{0},", _varModeWidths[i]);
                            else
                                QA_XLS_COL6_VALUE = "";
                        }
                        else if (QA_XLS_COL3_VALUE == QA_XLS_OTHERS)
                        {
                            if (i < (int)_lowerModalDistortionAngles.Count)
                                QA_XLS_COL7_VALUE = string.Format("{0},", _lowerModalDistortionAngles[i]);
                            else
                                QA_XLS_COL7_VALUE = ",";
                            if (i < (int)_upperModalDistortionAngles.Count)
                                QA_XLS_COL8_VALUE = string.Format("{0},", _upperModalDistortionAngles[i]);
                            else
                                QA_XLS_COL8_VALUE = ",";
                            if (i < (int)_lowerModalDistortionAngleXPositions.Count)
                                QA_XLS_COL9_VALUE = string.Format("{0},", _lowerModalDistortionAngleXPositions[i]);
                            else
                                QA_XLS_COL9_VALUE = ",";
                            if (i < (int)_upperModalDistortionAngleXPositions.Count)
                                QA_XLS_COL10_VALUE = string.Format("{0},", _upperModalDistortionAngleXPositions[i]);
                            else
                                QA_XLS_COL10_VALUE = ",";
                            if (i < (int)_lowerMaxModeBDCAreaXLengths.Count)
                                QA_XLS_COL11_VALUE = string.Format("{0},", _lowerMaxModeBDCAreaXLengths[i]);
                            else
                                QA_XLS_COL11_VALUE = ",";
                            if (i < (int)_upperMaxModeBDCAreaXLengths.Count)
                                QA_XLS_COL12_VALUE = string.Format("{0},", _upperMaxModeBDCAreaXLengths[i]);
                            else
                                QA_XLS_COL12_VALUE = ",";
                            if (i < (int)_lowerMaxModeBDCAreas.Count)
                                QA_XLS_COL13_VALUE = string.Format("{0},", _lowerMaxModeBDCAreas[i]);
                            else
                                QA_XLS_COL13_VALUE = ",";
                            if (i < (int)_upperMaxModeBDCAreas.Count)
                                QA_XLS_COL14_VALUE = string.Format("{0},", _upperMaxModeBDCAreas[i]);
                            else
                                QA_XLS_COL14_VALUE = ",";
                            if (i < (int)_lowerSumModeBDCAreas.Count)
                                QA_XLS_COL15_VALUE = string.Format("{0},", _lowerSumModeBDCAreas[i]);
                            else
                                QA_XLS_COL15_VALUE = ",";
                            if (i < (int)_upperSumModeBDCAreas.Count)
                                QA_XLS_COL16_VALUE = string.Format("{0},", _upperSumModeBDCAreas[i]);
                            else
                                QA_XLS_COL16_VALUE = ",";
                            if (i < (int)_lowerNumModeBDCAreaSamples.Count)
                                QA_XLS_COL17_VALUE = string.Format("{0},", _lowerNumModeBDCAreaSamples[i]);
                            else
                                QA_XLS_COL17_VALUE = ",";
                            if (i < (int)_upperNumModeBDCAreaSamples.Count)
                                QA_XLS_COL18_VALUE = string.Format("{0},", _upperNumModeBDCAreaSamples[i]);
                            else
                                QA_XLS_COL18_VALUE = ",";
                            if (i < (int)_middleLineRMSValues.Count)
                                QA_XLS_COL19_VALUE = string.Format("{0},", _middleLineRMSValues[i]);
                            else
                                QA_XLS_COL19_VALUE = "";
                        }   
                        sw.Write(QA_XLS_COL1_VALUE);
                        sw.Write(QA_XLS_COL2_VALUE);
                        sw.Write(QA_XLS_COL3_VALUE);
                        sw.Write(QA_XLS_COL4_VALUE);
                        sw.Write(QA_XLS_COL5_VALUE);
                        sw.Write(QA_XLS_COL6_VALUE);
                        sw.Write(QA_XLS_COL7_VALUE);
                        sw.Write(QA_XLS_COL8_VALUE);
                        sw.Write(QA_XLS_COL9_VALUE);
                        sw.Write(QA_XLS_COL10_VALUE);
                        sw.Write(QA_XLS_COL11_VALUE);
                        sw.Write(QA_XLS_COL12_VALUE);
                        sw.Write(QA_XLS_COL13_VALUE);
                        sw.Write(QA_XLS_COL14_VALUE);
                        sw.Write(QA_XLS_COL15_VALUE);
                        sw.Write(QA_XLS_COL16_VALUE);
                        sw.Write(QA_XLS_COL17_VALUE);
                        sw.Write(QA_XLS_COL18_VALUE);
                        sw.WriteLine(QA_XLS_COL19_VALUE);
                    }
                    sw.WriteLine();
                }
                sw.Flush();
            }
        }
    }
}

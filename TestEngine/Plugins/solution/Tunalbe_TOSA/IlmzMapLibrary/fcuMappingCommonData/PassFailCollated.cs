using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Bookham.fcumapping.CommonData
{
    
    class PassFailCollated
    {
        #region class dada members
        //const members
        public const string	COLLATED_PASSFAIL_FILE_NAME = "collatedPassFailResults";
        public const string LASER_TYPE_ID_DSDBR01_CSTRING = "DSDBR01";
        public const string USCORE = "_";
        public const string CSV = ".csv";

        public bool			_totalPassed;
	    public bool			_overallModeMapPassed;
	    public bool			_superModeMapPassed;
	    public List<short>	_superModeMapsPassed = new List<short>();
			// the instance of this class
        private static PassFailCollated theInstance = new PassFailCollated();
				// set all attributes to 0
    	private string	_resultsDir;
        private string _laserId;
        private string _dateTimeStamp;

        private string _collatedPassFailResultsAbsFilePath;
    	private OverallModeMapAnalysis	_p_overallModeMapAnalysis = new OverallModeMapAnalysis() ;
	    private List<CDSDBRSuperMode>	_v_p_superModes = new List<CDSDBRSuperMode>();

	    private int	_numSuperModesAnalysed;

      
        #endregion
        #region class function members
    
        protected PassFailCollated()
        {
	        // PassFailCollated is a singleton class
	        theInstance = this;
	        clear();
        }
        public static PassFailCollated instance
        {
	        // If we're calling this for
	        // the first time, create our
	        // instance, otherwise disallow
	        // any more being created
            get
            {
                if (theInstance == null)
                {
                    theInstance = new PassFailCollated();
                }
                return theInstance;
            }
        }
        public void clear()
        {
	        _resultsDir		= "";
	        _laserId		= "";
	        _dateTimeStamp	= "";

	        _collatedPassFailResultsAbsFilePath = "";

	        _p_overallModeMapAnalysis = null;
	        _v_p_superModes.Clear();

	        _numSuperModesAnalysed = 0;

	        _totalPassed = true;
	        _overallModeMapPassed = true;
	        _superModeMapPassed = true;
	        _superModeMapsPassed.Clear();

        }
        public string getResultsAbsFilePath(string	resultsDir,
					        string		laserId,
					        string		dateTimeStamp)
        {
	        string absFilePath = "";

	        absFilePath += resultsDir;
	        absFilePath += "\\";
	        absFilePath += COLLATED_PASSFAIL_FILE_NAME;
	        absFilePath += USCORE;
	        absFilePath += LASER_TYPE_ID_DSDBR01_CSTRING;
	        absFilePath += USCORE;
	        absFilePath += laserId;
	        absFilePath += USCORE;
	        absFilePath += dateTimeStamp;
	        absFilePath += CSV;

	        return absFilePath;
        }
       
        public void  setFileParameters(string resultsDir,string laserId,string dateTimeStamp)
        {
	        _resultsDir		= resultsDir;
	        _laserId		= laserId;
	        _dateTimeStamp	= dateTimeStamp;
        }
       
        public void setOverallModeMapAnalysis(OverallModeMapAnalysis overallModeMapAnalysis)
        {
	        _p_overallModeMapAnalysis = overallModeMapAnalysis;
        }

        public void clearSuperModes()
        {
	        _v_p_superModes.Clear();
        }

        public void addAnalysedSuperMode(CDSDBRSuperMode p_superMode)
        {
	        _v_p_superModes.Add(p_superMode);
	        _numSuperModesAnalysed++;
        }

        public void writeResultsToFile()
        {
            if (_p_overallModeMapAnalysis != null)
            {
                _collatedPassFailResultsAbsFilePath = getResultsAbsFilePath(_resultsDir, _laserId, _dateTimeStamp);

                //CLoseGridFile collatedPassFailResults(_collatedPassFailResultsAbsFilePath);
                //collatedPassFailResults.createFolderAndFile();
                StreamWriter sw;
                if (Directory.Exists(_resultsDir))
                {
                    if (File.Exists(_collatedPassFailResultsAbsFilePath))
                    {
                        File.Delete(_collatedPassFailResultsAbsFilePath);
                    }
                    sw = new StreamWriter(_collatedPassFailResultsAbsFilePath);
                    // calculate _totalPassed value
                    if (_p_overallModeMapAnalysis._screeningPassed)
                    {
                        if (_p_overallModeMapAnalysis._numSuperModes > 0)
                        {
                            _totalPassed = true;

                            for (int i = 0; i < (int)_v_p_superModes.Count; i++)
                            {
                                CDSDBRSuperMode p_sm = _v_p_superModes[i];

                                if (!(p_sm._superModeMapAnalysis._screeningPassed))
                                {
                                    _totalPassed = false;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            _totalPassed = false;
                        }
                    }
                    else
                    {
                        _totalPassed = false;
                    }
                    //	Column headers

                    const string QA_XLS_HEADER1 = "Total Pass/Fail,";
                    const string QA_XLS_HEADER2 = "Overall Mode Map Pass/Fail,";
                    const string QA_XLS_HEADER3 = "SM Index,";
                    const string QA_XLS_HEADER4 = "Supermode Map Pass/Fail/NotLoaded";

                    // write headers
                    sw.WriteLine(QA_XLS_HEADER1 + QA_XLS_HEADER2 + QA_XLS_HEADER3 + QA_XLS_HEADER4);
                    // write empty line
                    sw.WriteLine("");

                    string QA_XLS_COL1_VALUE = "";
                    string QA_XLS_COL2_VALUE = "";
                    string QA_XLS_COL3_VALUE = "";
                    string QA_XLS_COL4_VALUE = "";


                    if (_totalPassed)
                        QA_XLS_COL1_VALUE = "PASS";
                    else
                        QA_XLS_COL1_VALUE = "FAIL";

                    if (_p_overallModeMapAnalysis._screeningPassed)
                        QA_XLS_COL2_VALUE = "PASS";
                    else
                        QA_XLS_COL2_VALUE = "FAIL";


                    if (_p_overallModeMapAnalysis._numSuperModes <= 0)
                    {
                        // write just these for case when no SMs were found
                        sw.WriteLine(QA_XLS_COL1_VALUE + "," + QA_XLS_COL2_VALUE);
                    }

                    for (int sm = 0; sm < _p_overallModeMapAnalysis._numSuperModes; sm++)
                    {
                        QA_XLS_COL3_VALUE = "";
                        QA_XLS_COL3_VALUE = string.Format("%d", sm);
                        QA_XLS_COL4_VALUE = "Not Loaded";

                        for (int i = 0; i < (int)_v_p_superModes.Count; i++)
                        {
                            CDSDBRSuperMode p_sm = _v_p_superModes[i];
                            if ((int)(p_sm._sm_number) == sm)
                            { // supermap has been loaded

                                if (p_sm._superModeMapAnalysis._screeningPassed)
                                    QA_XLS_COL4_VALUE = "PASS";
                                else
                                    QA_XLS_COL4_VALUE = "FAIL";

                                break;
                            }
                        }
                        sw.WriteLine(QA_XLS_COL1_VALUE + "," + QA_XLS_COL2_VALUE + "," + QA_XLS_COL3_VALUE + "," + QA_XLS_COL4_VALUE);
                        QA_XLS_COL1_VALUE = "";
                        QA_XLS_COL2_VALUE = "";

                    }
                    sw.Close();
                }
                else
                {
                    throw new DirectoryNotFoundException(_resultsDir + "not found");
                }
              
                // write overall map pass/fail, append the data to this file
                _p_overallModeMapAnalysis.writePassFailResultsToFile(_collatedPassFailResultsAbsFilePath);

                for (int i = 0; i < _v_p_superModes.Count; i++)
                {
                    CDSDBRSuperMode p_sm = _v_p_superModes[i];

                    //// write supermode map pass/fail, append the data to this file
                    p_sm._superModeMapAnalysis.writePassFailResultsToFile(_collatedPassFailResultsAbsFilePath, i);

                }
            }
        }
        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.fcumapping.CommonData
{
    public enum VectorAttribute
    {
        max ,
        min,
        mean,
        sum,
        count,
        variance,
        stdDev,
        median,
        maxGap // max gap between sorted elements
    }
    public class Vector
    {
        private List<double> _values = new List<double>();
        public Vector()
        {
            Clear();
        }
        public Vector(List<double> val)
        {
            _values = val;
        }
        public List<double> getValues()
        {
            return _values;
        }
        public void Add(double value)
        {
            _values.Add(value);
        }
        public int size()
        {
            int size = 0;
            if (_values.Count > 0)
            {
                size = (int)_values.Count;
            }
            return size;
        }

        public double this[int index]
        {
            get
            {
                return _values[index];
            }
            set
            {
                _values[index] = value;
            }
        }
#region private functions     
        //the max value in the vector
        private double maxOfValues()
        {
            double maximum = 0;
            for (int i = 0; i < _values.Count; i++)
            {
                if (maximum<_values[i])
                {
                    maximum = _values[i];
                }
            }
            return maximum;
        }

        //the min value in the vector
        private double minOfValues()
        {
            double minimum = 201;
            for (int i = 0; i < _values.Count; i++)
            {
                if (minimum > _values[i])
                {
                    minimum = _values[i];
                }
            }
            return minimum;
        }

        //return the mean value in the vector
        private double meanOfValues()
        {
            double mean = 0;
            if (_values.Count > 0)
            {
                int count = 0;
                int iter;
                iter = 0;
                while (iter < _values.Count)
                {
                    mean += _values[iter];
                    iter++;
                    count++;
                }
                if (count > 1)
                {
                    mean /= count;
                }
            }
            return mean;
        }

        //return the sum of values in the vector
        private double sumOfValues()
        {
            double sum = 0;
            if( _values.Count > 0 )
            {
                int iter = 0;
                while( iter < _values.Count)
                {
                    sum += _values[iter];
                    iter++;
                }
            }
            return sum;
        }

        //The standard deviation is one of several indices of variability that
        //statisticians use to characterize the dispersion among the measures
        //in a given population. 
        //To calculate the standard deviation of a population it is first 
        //necessary to calculate that population's variance.
        //Numerically, the standard deviation is the square root of the variance.
        //Unlike the variance, which is a somewhat abstract measure of variability,
        //the standard deviation can be readily conceptualized as a distance
        //along the scale of measurement. 

        private double stdDevOfValues()
        {
            double stdev = Math.Sqrt(varianceOfValues());
            return stdev;
        }

        //The variance is one of several indices of variability that statisticians 
        //use to characterize the dispersion among the measures in a given
        //population. To calculate the variance of a given population, 
        //it is necessary to first calculate the mean of the scores, 
        //then measure the amount that each score deviates from the mean and then
        //square that deviation (by multiplying it by itself). Numerically, 
        //the variance equals the average of the several squared deviations from the mean.  

        private double varianceOfValues()
        {
            double variance = 0;
            if( _values.Count > 0 )
            {
                double sum = 0;
                int count = 0;
                double mean = meanOfValues();
                int iter = 0;
                while( iter < _values.Count )
                {
                    sum +=Math.Pow( _values[iter] - mean , 2 );
                    iter++;
                    count++;
                }
                if( count > 1 )
                {
                    variance = sum / (count - 1); 
                }
            }
            return variance;
        }

        // The median of the sorted vector s[i] where i = 0, n-1 is defined as
        // s[(n-1)/2] where n is odd
        // ( s[(n/2)-1] + s[n/2] ) / 2 where n is even
        private double medianOfValues()
        {
            double median = 0;
            int n = size();
            if(n>0)
            {
                // make a copy of the vector
                List<double> sorted_values = new List<double>(_values);
                sorted_values.Sort();
                if( n%2 == 0 )
                { // even
                    median = ( sorted_values[(n/2)-1] + sorted_values[n/2] )/ 2;
                }
                else
                { // odd
                    median = sorted_values[(n-1)/2];
                }
            }
            return median;
        }

        //sort the array and return the max gap between neighbor node
        // in the array
        private double maxGapInValues()
        {
            double maxGap = 0;
            long n = size();
            if (n > 0)
            {
                // make a copy of current vector
                List<double> copyList = new List<double>();
                for (int i = 0; i < _values.Count;i++ )
                {
                    double val = _values[i];
                    copyList.Add(val);
                }
                copyList.Sort();
                for( int i=0; i<n-1; i++)
                {
                    double gap = copyList[i + 1] - copyList[i];
                    
                    if(gap > maxGap)
                        maxGap = gap;
                }
            }
            return maxGap;

        }
#endregion

        public double getVectorAttribute(VectorAttribute attrib)
        {
            double result = -1;
            switch (attrib)
            {
                case VectorAttribute.max:
                    result = maxOfValues();
                    break;
                case VectorAttribute.min:
                    result = minOfValues();
                    break;
                case VectorAttribute.mean:
                    result = meanOfValues();
                    break;
                case VectorAttribute.sum:
                    result = sumOfValues();
                    break;
                case VectorAttribute.count:
                    result = size();
                    break;
                case VectorAttribute.stdDev:
                    result = stdDevOfValues();
                    break;
                case VectorAttribute.variance:
                    result = varianceOfValues();
                    break;
                case VectorAttribute.median:
                    result = medianOfValues();
                    break;
                case VectorAttribute.maxGap:
                    result = maxGapInValues();
                    break;
            }

            return result;
        }
        public void sort()
        {
            _values.Sort();
        }
        public void Clear()
        {
            _values.Clear();
        }       
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.Solution.Config;

namespace Bookham.fcumapping.CommonData
{
    internal static class SettingConfigure
    {

        public const string _settingPath = @"Configuration/TOSA Final/MapTest/CloseGridSettings.xml";
        
        public static ConfigurationManager cfgReader = new ConfigurationManager(_settingPath);
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.fcumapping.utility;
using Bookham.TestLibrary.Algorithms;
using System.IO;
using Bookham.TestSolution.ChassisNS;
using Bookham.TestSolution.Instruments;
using System.Collections.Specialized;
using Bookham.TestLibrary.Utilities;
using Bookham.TestLibrary.FcuCommonUtils;
using Bookham.TestEngine.Equipment;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestLibrary.Instruments;

namespace Bookham.fcumapping.CommonData
{
    public class CDSDBRSuperMode
    {
        private  class xyData
	    {
            public long first;
            public long second;
            public long xPos;
            public long yPos;
            public long boundaryLine;
            public double gapDist;
            public long priority;
            public bool linked;
	    }

        #region constant datas
        private string _CG_ID = "CLOSEGRID C# 1.0 ";

        public string CG_ID
        {
            get { return _CG_ID; }
            //set { CG_ID = value; }
        }
        private const string CSV =".csv";
        private const string USCORE = "_";
        private const string MATRIX_ = "MATRIX_";
        private const string JUMPSE_ = "JUMPSE_";
        private const string LASER_TYPE_ID_DSDBR01_CSTRING = "DSDBR01";
        private const string FRONT_CURRENT_CSTRING = "If";
        private const string REAR_CURRENT_CSTRING = "Ir";
        private const string PHASE_CURRENT_CSTRING = "Ip";
        private const string MIDDLELINE_CURRENT_CSTRING = "Im";
        private const string MEAS_TYPE_ID_FORWARDPD1_CSTRING = "forwardPD1Current";
        private const string MEAS_TYPE_ID_REVERSEPD1_CSTRING = "reversePD1Current";
        private const string MEAS_TYPE_ID_FORWARDPD2_CSTRING = "forwardPD2Current";
        private const string MEAS_TYPE_ID_REVERSEPD2_CSTRING = "reversePD2Current";
        private double Max_Value_Etalon = -1;
        private const int ModalDistortionMaxX = 80;
        private const int ModalDistortionMinX = 20;
        private string _linesFilePath = "";
        private Inst_Ke2510 Optical_Switch;

        //added by echo,05/06/2010, for limit use 
        public  string qaMetricsFilePath;
        public string passFailMetricsFilePath;
        public int screen_result_sm;

        public string LinesFilePath
        {
            get { return _linesFilePath; }
            //set { _linesFilePath = value; }
        }

        private string _matrix_ph_forward_filePath = "";

        public string Matrix_ph_forward_filePath
        {
            get { return _matrix_ph_forward_filePath; }
            //set { _matrix_ph_forward_filePath = value; }
        }

        //private string _matrix_passfail_filepath = "";
        //public string Matrix_PassFail_filepath
        //{
        //    get { return _matrix_passfail_filepath; }
        //}


        private string _matrix_ph_reverse_filePath = "";

        public string Matrix_ph_reverse_filePath
        {
            get { return _matrix_ph_reverse_filePath; }
            //set { _matrix_ph_reverse_filePath = value; }
        }

        private int _num_lm = 0;

        public int Num_lm
        {
            get { return _num_lm; }
            //set { _num_lm = value; }
        }

        private const int lower_line_diff_count = 25;
        private const int upper_line_diff_count = 15;
         
        private const int line_check_point_num = 5;       
        #endregion

        public int getMildMultiMode()
        {

            if (!map_screened)
            {
                Console.WriteLine("Please screen the map first!");
                return -1;
            }
            return CalculateMMM();
        }

        private int CalculateMMM()
        {

            bool[] mmmStatus = new bool[2];
            int[] numOfFail = new int[2];

            string[] filesToCheck = new string[2];
            filesToCheck[0] = this.Matrix_ph_forward_filePath;
            filesToCheck[1] = this.Matrix_ph_reverse_filePath;

            //double rearCutDiffThreshold = -0.003;
            //int allowedFails = 0;

            // Check forward file and reverse file...
            for (int ii = 0; ii < filesToCheck.Length; ii++)
            {
                string filename = filesToCheck[ii];

                // Read file in
                Bookham.TestLibrary.Utilities.CsvReader Cr = new Bookham.TestLibrary.Utilities.CsvReader();
                string[][] strData = Cr.ReadFile(filename).ToArray();
                if (strData == null)
                    throw new Exception(string.Format("No data in the file {0}.", filename));

                int rearRows = strData.GetLength(0);
                int phaseColumns = strData[0].GetLength(0);

                string[,] filteredData = new string[rearRows, phaseColumns];

                //echo added median filter on matrix

                for (int column = 0; column < phaseColumns; column++)
                {
                    for (int row = 0; row < rearRows; row++)
                    {
                        if (row == 0 || row == rearRows - 1)
                        {
                            filteredData[row, column] = strData[row][column];
                        }
                        else
                        {
                            filteredData[row, column] = MedidanFilter(strData[row - 1][column], strData[row][column], strData[row + 1][column]);
                        }
                    }
                }
               
                // Rear cut differential
                double[,] dblData = new double[rearRows, phaseColumns];
                byte[,] Flag = new byte[rearRows, phaseColumns];
                for (int j = 0; j < phaseColumns; j++)
                {
                    int i = rearRows - 1;
                    dblData[i, j] = Convert.ToDouble(filteredData[i, j]);
                    for (i = rearRows - 2; i >= 0; i--)
                    {
                        dblData[i, j] = Convert.ToDouble(filteredData[i, j]);
                        //Rear Cut Difference
                        dblData[i + 1, j] -= dblData[i, j];
                        //MMM threshold
                        Flag[i + 1, j] = dblData[i + 1, j] < rearCutDiffThreshold ? (byte)1 : (byte)0;
                    }
                    dblData[++i, j] = Flag[++i, j] = 0;
                }

                // 3 point threshold
                // Finds three or more MMM threshold fail points in a 3x3 box.
                int actualFails = 0;
                for (int i = 2; i < rearRows - 1; i++)
                {
                    for (int j = 1; j < phaseColumns - 1; j++)
                    {
                        byte btmp = 0;
                        for (int x = -1; x <= 1; x++)
                        {
                            for (int y = -1; y <= 1; y++)
                            {
                                btmp += Flag[i + x, j + y];
                            }
                        }
                        actualFails += btmp >= 3 ? 1 : 0;
                    }
                }

                // Set the status
                numOfFail[ii] = actualFails;
                mmmStatus[ii] = actualFails > allowedFails ? false : true;
            }

            int mmmPassFailStatus = (mmmStatus[0] && mmmStatus[1]) ? 1 : 0;
            return mmmPassFailStatus;
        }

         private string MedidanFilter(string  p_1, string p_2, string p_3)
        {
            //find the median value in 3 values
            double p1 = double.Parse(p_1);
            double p2 = double.Parse(p_2);
            double p3=double.Parse(p_3);
            
            double retValue = 0;
            if (p1 > p2)
            {
                if (p2 > p3)
                    retValue = p2;
                else
                {
                    if (p3 > p1) 
                        retValue = p1;
                    else
                        retValue = p3;
                }
            }
            else
            {
                if (p1 > p3) retValue= p1;
                else
                {
                    if (p3 > p2) 
                        retValue= p2;
                    else
                        retValue= p3;
                }
            }
            return retValue.ToString();
        }
    

        public double getModeDistortion()
        {
            if (!map_screened)
            {
                Console.WriteLine("Please screen the map first!");
                return -1;
            }
            if (!map_caled)
            {
                CalMapLMWidthAndMD();
            }
            return modalDistortion;
        }
        public List<Pair<double,double>> getLMWidth()
        {
             if (!map_screened)
            {
                Console.WriteLine("Please screen the map first!");
                return null;
            }
            if (!map_caled)
            {
                CalMapLMWidthAndMD();
            }
            return modewidths;
        }


       

        /// <summary>
        /// get how much percentages stable area on the map
        /// </summary>
        /// <returns></returns>
        public double getHyteresisPercent()
        {
            int num = _lm_lower_lines.Count;
            double widthSum = 0;
            if (num == _lm_upper_lines.Count && num != 0)
            {
                //make sure that the upper line and lower line has the same length
                for (int i = 0; i < num - 1; i++)
                {
                    List<double> x = _lm_lower_lines[i].getCols();
                    List<double> y = _lm_lower_lines[i].getRows();
                    bool hasFrontChaged = _lm_lower_lines[i].hasChangeOfFrontPairs();
                    ModeBoundaryLine lowerline = new ModeBoundaryLine(x, y, hasFrontChaged);

                    x = _lm_upper_lines[i + 1].getCols();
                    y = _lm_upper_lines[i + 1].getRows();
                    hasFrontChaged = _lm_upper_lines[i + 1].hasChangeOfFrontPairs();
                    ModeBoundaryLine upperline = new ModeBoundaryLine(x, y, hasFrontChaged);

                    ModeAnalysis ma = new ModeAnalysis();
                    ma.loadBoundaryLines(lowerline, upperline);

                    Vector width = new Vector();
                    ma.getModeWidths(out width);
                    for (int j = 0; j < width.size(); j++)
                    {
                        widthSum += width[j];
                    }
                }             
            }
            //percentage
            return widthSum/(_filtered_middle_line._points.Count*_phase_current._length)*100;
        }
        public CDSDBRSuperMode() 
        {

            _lm_upper_lines_removed = 0;
            _lm_lower_lines_removed = 0;
            _lm_middle_lines_of_frequency_removed = 0;
            this.superMapSitting = (NameValueCollection)SettingConfigure.cfgReader.GetSection("CloseGridSetting/SuperMap/SuperModeMap");
            this.overAllMapSitting = (NameValueCollection)SettingConfigure.cfgReader.GetSection("CloseGridSetting/OverAllMap/BoundaryDetection"); ;
            this.superMapBoundarySitting = (NameValueCollection)SettingConfigure.cfgReader.GetSection("CloseGridSetting/SuperMap/BoundaryDetection");
            
        }
        public CDSDBRSuperMode(short supermodeNumber)
        {
            this._sm_number = supermodeNumber;
            _lm_upper_lines_removed = 0;
            _lm_lower_lines_removed = 0;
            _lm_middle_lines_of_frequency_removed = 0;
            this.superMapSitting = (NameValueCollection)SettingConfigure.cfgReader.GetSection("CloseGridSetting/SuperMap/SuperModeMap");
            this.overAllMapSitting = (NameValueCollection)SettingConfigure.cfgReader.GetSection("CloseGridSetting/OverAllMap/BoundaryDetection"); 
            this.superMapBoundarySitting = (NameValueCollection)SettingConfigure.cfgReader.GetSection("CloseGridSetting/SuperMap/BoundaryDetection");
          
            
        }
        public void setSuperModeNum(short supermodeNumber)
        {
            this.Sm_number = supermodeNumber;
        }
        public void clearMapsAndLines()
        {
            _map_forward_power_ratio_median_filtered.clear();
            _map_reverse_power_ratio_median_filtered.clear();
            _map_hysteresis_power_ratio_median_filtered.clear();
            // _map.clear();
            _map_forward_photodiode1_current.clear();
            _map_reverse_photodiode1_current.clear();
            _map_forward_photodiode2_current.clear();
            _map_reverse_photodiode2_current.clear();
            _phase_current.clear();
            for (int i = 0; i < _lm_upper_lines.Count; i++)
            {
                _lm_upper_lines[i].clear();
            }
            _lm_upper_lines.Clear();

            for (int i = 0; i < _lm_lower_lines.Count; i++)
            {
                _lm_lower_lines[i].clear();
            }
            _lm_lower_lines.Clear();

            for (int i = 0; i < _lm_middle_lines.Count; i++)
            {
                _lm_middle_lines[i].clear();
            }
            _lm_middle_lines.Clear();

            for (int i = 0; i < _lm_filtered_middle_lines.Count; i++)
            {
                _lm_filtered_middle_lines[i].clear();
            }
            _lm_filtered_middle_lines.Clear();

            for (int i = 0; i < _lm_middle_lines_of_frequency.Count; i++)
            {
                _lm_middle_lines_of_frequency[i].clear();
            }
            _lm_middle_lines_of_frequency.Clear();

            _average_widths.Clear();
            _matched_line_numbers.Clear();

            _lm_upper_lines_removed = 0;
            _lm_lower_lines_removed = 0;
            _lm_middle_lines_of_frequency_removed = 0;
        }
        public void screen()
        {
            // Step 1: Read in boundaries from file
            if (!loadAllFromFile)
            {
                //screen relust
                //st.IsOnline = true;
                if (_result_dir[_result_dir.Length - 1] != '\\')
                {
                    _result_dir = _result_dir + "\\";
                }
                //Inst.SetDefaultState();

                //NameValueCollection MapSettings = (NameValueCollection)SettingConfigure.cfgReader.GetSection("TestModule/MappingSettings/TCMZMappingSettings_fromCoC");

                Inst.IGain_mA = (float)I_gain;
                Inst.ISoa_mA = (float)I_soa;
                Inst.IrearSoa_mA = (float)I_rearsoa; 

                Inst.SetMiddleLine(ImiddleLine_filepath);
                // 1ms, 3ms, 5ms
                //int delaytime_ms = 5;
                NameValueCollection SupperModeMapDataSource_Etalon = (NameValueCollection)SettingConfigure.cfgReader.GetSection("CloseGridSetting/SuperMap/BoundaryDetection");
                IInstType_DigitalIO outLine = Optical_Switch.GetDigiIoLine(1);//Jack.zhang PD control 
                outLine.LineState = true; 
                if (Convert.ToBoolean(SupperModeMapDataSource_Etalon["UseEtalonData"].ToString() == "0"))
                    outLine.LineState = false;//Jack,zhang first switch the relay card to optical box for power ratio supermode map  
                Inst.BeginSupermapAsync(Inst_FCU.SweepDirection.f);
                string smfn = Inst.EndSupermapAsync();
                //outLine.LineState = true;  

                if (Convert.ToBoolean(SupperModeMapDataSource_Etalon["UseEtalonData"].ToString() == "1"))
                {
                    smfn = smfn + "\\TX.csv";
                }
                else
                {
                    smfn = smfn + "\\Ratio_0.csv";
                }

                if (Convert.ToBoolean(SupperModeMapDataSource_Etalon["UseEtalonData"].ToString() == "0"))
                {
                    _matrix_ph_forward_filePath = _result_dir + "MATRIX_forwardPowerRatio_DSDBR01_" + _laser_id + "_"
                        + _timestamp + "_SM" + Sm_number.ToString() + "_P.csv";
                }
                else
                {
                    _matrix_ph_forward_filePath = _result_dir + "MATRIX_forwardPD1Current_DSDBR01_" + _laser_id + "_"
                            + _timestamp + "_SM" + Sm_number.ToString() + "_P.csv";
                }
                File.Copy(smfn, _matrix_ph_forward_filePath);
                LoadForwardPhotodiode(smfn);
                if (ScanDirectionChange != null)
                {
                    ScanDirectionChange(this, new EventArgs());
                }
                Inst.BeginSupermapAsync(Inst_FCU.SweepDirection.r);

                smfn = Inst.EndSupermapAsync();
                if (Convert.ToBoolean(SupperModeMapDataSource_Etalon["UseEtalonData"].ToString() == "1"))
                {
                    smfn = smfn + "\\TX.csv";
                    _matrix_ph_reverse_filePath = _result_dir + "MATRIX_reversePD1Current_DSDBR01_" + _laser_id + "_"
                        + _timestamp + "_SM" + Sm_number.ToString() + "_P.csv";
                }
                else
                {
                    smfn = smfn + "\\Ratio_0.csv";
                    _matrix_ph_reverse_filePath = _result_dir + "MATRIX_reversePowerRatio_DSDBR01_" + _laser_id + "_"
                        + _timestamp + "_SM" + Sm_number.ToString() + "_P.csv";
                }
               // _matrix_ph_reverse_filePath = _result_dir + "MATRIX_reversePD1Current_DSDBR01_" + _laser_id + "_"
               //         + _timestamp + "_SM" + Sm_number.ToString() + "_P.csv";
                File.Copy(smfn, _matrix_ph_reverse_filePath);
                LoadReversePhotodiode(smfn);
                outLine.LineState = true;
            }
            else //load sm map data
            {
                //screen relust
                //st.IsOnline = true;
                if (_result_dir[_result_dir.Length - 1] != '\\')
                {
                    _result_dir = _result_dir + "\\";
                }
                //Inst.SetDefaultState();
                //Inst.ISoa_mA = (float)I_soa;
                //Inst.IGain_mA = (float)I_gain;
                // string sweepdatafn = Inst.OverallmapSync(Inst_FCU.SweepDirection.f);
                //Inst.SetMiddleLine(ImiddleLine_filepath);
                // 1ms, 3ms, 5ms
                //int delaytime_ms = 5;
                //Inst.BeginSupermapAsync(Inst_FCU.SweepDirection.f);
                //string smfn = Inst.EndSupermapAsync();
                _matrix_ph_forward_filePath = _result_dir + "MATRIX_forwardPD1Current_DSDBR01_" + _laser_id + "_"
                        + _timestamp + "_SM" + Sm_number.ToString() + "_P.csv";
                //File.Copy(smfn, _matrix_ph_forward_filePath);
                LoadForwardPhotodiode(_matrix_ph_forward_filePath);
                if (ScanDirectionChange != null)
                {
                    ScanDirectionChange(this, new EventArgs());
                }
                //Inst.BeginSupermapAsync(Inst_FCU.SweepDirection.r);

                //smfn = Inst.EndSupermapAsync();
                _matrix_ph_reverse_filePath = _result_dir + "MATRIX_reversePD1Current_DSDBR01_" + _laser_id + "_"
                        + _timestamp + "_SM" + Sm_number.ToString() + "_P.csv";
                //File.Copy(smfn, _matrix_ph_reverse_filePath);
                LoadReversePhotodiode(_matrix_ph_reverse_filePath);
            }
            
            if (allDataLoad())
            {
                if (StartSuperModeAnalysis != null)
                {
                    StartSuperModeAnalysis(this, new EventArgs());
                }
                
                // Step 2: find boundaries
                findLMBoundaries();

                CheckLMBoundaries();

                _lm_upper_lines_removed = (short)_lm_upper_lines.Count;
                _lm_lower_lines_removed = (short)_lm_lower_lines.Count;
                
                // Step 3: Find middle lines of frequency in longitudinal modes

                findLMMiddleLines();

                // Find middle lines of upper lines and lower lines. following 2 steps  added by purney.xie 12/28/2009
                findUpperLowerMiddleLines(_lm_upper_lines, out _upper_middle_lines);

                findUpperLowerMiddleLines(_lm_lower_lines, out _lower_middle_lines);



                // Step 4: Filter middle lines of frequency in longitudinal modes
                filterLMMiddleLines();

                // Step 5: Shift middle lines of frequency in longitudinal modes
	             calcLMMiddleLinesOfFreq();

                 // Step 6: Check each middle line to ensure
                 // it is in one longitudinal mode,
                 // if not, remove the longitudinal mode
    //// ///  //  checkLMMiddleLinesOfFreq();
          
                _lm_upper_lines_removed -= (short)_lm_upper_lines.Count;
                _lm_lower_lines_removed -= (short)_lm_lower_lines.Count;
                _lm_middle_lines_of_frequency_removed -= (short)_lm_middle_lines.Count;
                _num_lm = _lm_middle_lines.Count;
                map_screened = true;

                 //coded by purney.xie
                if( map_screened == true )
	            {            		
		            
		            bool hasChangeOfFrontPairs;

		            List<ModeBoundaryLine> upperLines = new List<ModeBoundaryLine>();
		            List<ModeBoundaryLine> lowerLines = new List<ModeBoundaryLine>();
		            List<ModeBoundaryLine> middleLines = new List<ModeBoundaryLine>();


                    for (int i = 0; i < _lm_upper_lines.Count; i++)
                    {

                        List<double> xPoints = _lm_upper_lines[i].getCols();
                        List<double> yPoints = _lm_upper_lines[i].getRows();
                        hasChangeOfFrontPairs = _lm_upper_lines[i].hasChangeOfFrontPairs();

                        ModeBoundaryLine upperLine = new ModeBoundaryLine(xPoints, yPoints, hasChangeOfFrontPairs);
                        upperLines.Add(upperLine);
                    }

                    for (int i = 0; i < _lm_lower_lines.Count; i++)
                    {
                        List<double> xPoints = _lm_lower_lines[i].getCols();
                        List<double> yPoints = _lm_lower_lines[i].getRows();
                        hasChangeOfFrontPairs = _lm_lower_lines[i].hasChangeOfFrontPairs();

                        ModeBoundaryLine lowerLine = new ModeBoundaryLine(xPoints, yPoints, hasChangeOfFrontPairs);
                        lowerLines.Add(lowerLine);
                    }

                    for (int i = 0; i < _lm_middle_lines_of_frequency.Count; i++)
                    {

                        List<double> xPoints = _lm_middle_lines_of_frequency[i].getCols();
                        List<double> yPoints = _lm_middle_lines_of_frequency[i].getRows();
                        hasChangeOfFrontPairs = _lm_lower_lines[i].hasChangeOfFrontPairs();

                        ModeBoundaryLine middleLine = new ModeBoundaryLine(xPoints, yPoints, hasChangeOfFrontPairs);
                        middleLines.Add(middleLine);
                    }
                    
                    // load data from file for test quality program
                    //LoadBoundariesFromFile(@"C:\FCUDebugDir\upperlower\upper.csv", lowerLines);
                    //LoadBoundariesFromFile(@"C:\FCUDebugDir\upperlower\lower.csv", upperLines);
                    //loadMiddleFromFile(@"C:\FCUDebugDir\upperlower\middle.csv", middleLines);

                    
                    

		            Vector _continuityValues = new Vector();
		            _superModeMapAnalysis.init();
                    _xAxisLength = (int)_col;
                    _yAxisLength = (int)_row;
			        _superModeMapAnalysis.runAnalysis(_sm_number,
										            upperLines,
										            lowerLines,
										            middleLines,
										            _lm_upper_lines_removed,
										            _lm_lower_lines_removed,
										            _lm_middle_lines_of_frequency_removed,
										            _xAxisLength,
										            _yAxisLength,
										            _continuityValues,
										            _result_dir,
										            _laser_id,
                                                    _timestamp);
                    qaMetricsFilePath = _superModeMapAnalysis.qaMetricsFilePath;
                    passFailMetricsFilePath = _superModeMapAnalysis.passFailMetricsFilePath;

                    
		            PassFailCollated.instance.addAnalysedSuperMode(this);

	            }

                WriteLMBoundaryFile(_result_dir + "Lower" + USCORE + LASER_TYPE_ID_DSDBR01_CSTRING
                   + USCORE + _laser_id + USCORE + _timestamp + USCORE + "SM" + _sm_number + CSV, _lm_lower_lines);
                WriteLMBoundaryFile(_result_dir + "upper" + USCORE + LASER_TYPE_ID_DSDBR01_CSTRING
                  + USCORE + _laser_id + USCORE + _timestamp + USCORE + "SM" + _sm_number + CSV, _lm_upper_lines);
                WriteLMBoundaryFile(_result_dir + "middle" + USCORE + LASER_TYPE_ID_DSDBR01_CSTRING
                  + USCORE + _laser_id + USCORE + _timestamp + USCORE + "SM" + _sm_number + CSV, _lm_filtered_middle_lines);
                _linesFilePath = _result_dir + "lines" + USCORE + LASER_TYPE_ID_DSDBR01_CSTRING
                + USCORE + _laser_id + USCORE + _timestamp + USCORE + "SM" + Sm_number + CSV;
                WriteLMBoundaryFile(_linesFilePath);

                // Following 2 steps added by purney.xie 12/28/2009
                WriteLMMiddleLinesToFile("ImiddleUpper_LM", _upper_middle_lines);

                WriteLMMiddleLinesToFile("ImiddleLower_LM", _lower_middle_lines);      
            }
            else
            {
                Console.WriteLine("SM{0} doest not load all its neccessary component yet!", Sm_number);
            }
            screen_result_sm = (map_screened == true ? 1 : 0);

        }
        private void loadMiddleFromFile(string filePath, List<ModeBoundaryLine> middles)
        {
            // load middle
            List<ModeBoundaryLine> Lines = middles;
            Bookham.TestLibrary.Utilities.CsvReader csvReader = new Bookham.TestLibrary.Utilities.CsvReader();
            List<string[]> list = csvReader.ReadFile(@"C:\FCUDebugDir\upperlower\middle.csv");
            int i = 1;
            int yPointTemp = 0;
            int xPointTemp = 0;
            List<List<double>> xaxis = new List<List<double>>();
            List<List<double>> yaxis = new List<List<double>>();
            // ModeBoundaryLine upperLine;
            while (i < list.Count)
            {
                string[] arow = list[i];
                yPointTemp = Convert.ToInt32(arow[0]);  // row
                xPointTemp = Convert.ToInt32(arow[1]);  // col
                if (yPointTemp == 0 && xPointTemp == 0)
                {
                    List<double> xPoints = new List<double>();
                    List<double> yPoints = new List<double>();

                    int start = i + 1;
                    int end = i + 1;
                    for (int j = i + 1; j < list.Count; j++)
                    {
                        string[] onerow = list[j];
                        yPointTemp = Convert.ToInt32(onerow[0]);  // row
                        xPointTemp = Convert.ToInt32(onerow[1]);  // col
                        if (yPointTemp == 100 && xPointTemp == 100)
                        {
                            end = j;
                            break;
                        }

                    }
                    if (start != end && start < end && end < list.Count)
                    {
                        for (int k = start; k < end; k++)
                        {
                            string[] row = list[k];
                            yPoints.Add(Convert.ToDouble(row[0]));
                            xPoints.Add(Convert.ToDouble(row[1]));
                        }
                        i = end + 1;
                        bool hasChangeOfFrontPairs = false;
                        ModeBoundaryLine middleline = new ModeBoundaryLine(xPoints, yPoints, hasChangeOfFrontPairs);
                        Lines.Add(middleline);
                    }
                }
            }
          
        }
        private void LoadBoundariesFromFile(string filePath,  List<ModeBoundaryLine> boundaries)
        {
            List<ModeBoundaryLine> upperLines = boundaries;
            Bookham.TestLibrary.Utilities.CsvReader csvReader = new Bookham.TestLibrary.Utilities.CsvReader();
            List<string[]> list = csvReader.ReadFile(filePath);
            int i = 1;
            int yPointTemp = 0;
            int xPointTemp = 0;
            ModeBoundaryLine upperLine;
            while (i < list.Count)
            {
                string[] arow = list[i];
                yPointTemp = Convert.ToInt32(arow[0]);  // row
                xPointTemp = Convert.ToInt32(arow[1]);  // col
                if (yPointTemp == 0 && xPointTemp != 0)
                {
                    List<double> xPoints = new List<double>();
                    List<double> yPoints = new List<double>();

                    yPoints.Add(Convert.ToDouble(yPointTemp));
                    xPoints.Add(Convert.ToDouble(xPointTemp));
                    int start = i + 1;
                    int end = i + 1;
                    for (int j = i + 1; j < list.Count; j++)
                    {
                        string[] onerow = list[j];
                        yPointTemp = Convert.ToInt32(onerow[0]);  // row
                        xPointTemp = Convert.ToInt32(onerow[1]);  // col
                        if (yPointTemp != 200 && xPointTemp == 100)
                        {
                            end = j;
                            break;
                        }

                    }
                    if (start != end && start < end && end < list.Count)
                    {
                        for (int k = start; k <= end; k++)
                        {
                            string[] row = list[k];
                            yPoints.Add(Convert.ToDouble(row[0]));
                            xPoints.Add(Convert.ToDouble(row[1]));
                        }
                        i = end + 1;
                        bool hasChangeOfFrontPairs = false;
                        upperLine = new ModeBoundaryLine(xPoints, yPoints, hasChangeOfFrontPairs);
                        upperLines.Add(upperLine);
                    }

                }
                if (yPointTemp != 200 && xPointTemp == 0)
                {
                    List<double> xPoints = new List<double>();
                    List<double> yPoints = new List<double>();
                    yPoints.Add(Convert.ToDouble(yPointTemp));
                    xPoints.Add(Convert.ToDouble(xPointTemp));

                    int start = i + 1;
                    int end = i + 1;
                    for (int j = i + 1; j < list.Count; j++)
                    {
                        string[] onerow = list[j];
                        yPointTemp = Convert.ToInt32(onerow[0]);  // row
                        xPointTemp = Convert.ToInt32(onerow[1]);  // col
                        if (yPointTemp != 200 && xPointTemp == 100)
                        {
                            end = j;
                            break;
                        }
                        if (yPointTemp == 200 && xPointTemp != 100)
                        {
                            end = j;
                            break;
                        }

                    }
                    if (start != end && start < end && end < list.Count)
                    {
                        for (int k = start; k <= end; k++)
                        {
                            string[] row = list[k];
                            yPoints.Add(Convert.ToDouble(row[0]));
                            xPoints.Add(Convert.ToDouble(row[1]));
                        }
                        i = end + 1;
                        bool hasChangeOfFrontPairs = false;
                        upperLine = new ModeBoundaryLine(xPoints, yPoints, hasChangeOfFrontPairs);
                        upperLines.Add(upperLine);
                    }
                }

            }
           
        }
        private void CalMapLMWidthAndMD()
        {
            int num = _lm_lower_lines.Count;
            modewidths.Clear();
            double widthChange = 0;
            int actualnum = 0;
            if (num == _lm_upper_lines.Count && num !=  0)
            {
                //make sure that the upper line and lower line has the same length
                for (int i = 0; i < num-1;i++ )
                {
                    List<double> x = _lm_lower_lines[i].getCols(ModalDistortionMinX,ModalDistortionMaxX);
                    List<double> y = _lm_lower_lines[i].getRows(ModalDistortionMinX, ModalDistortionMaxX);
                    bool hasFrontChanged = _lm_lower_lines[i].hasChangeOfFrontPairs();
                    ModeBoundaryLine lowerline = new ModeBoundaryLine(x, y,hasFrontChanged);

                    x = _lm_upper_lines[i + 1].getCols(ModalDistortionMinX, ModalDistortionMaxX);
                    y = _lm_upper_lines[i + 1].getRows(ModalDistortionMinX, ModalDistortionMaxX);
                    hasFrontChanged = _lm_upper_lines[i + 1].hasChangeOfFrontPairs();
                    ModeBoundaryLine upperline = new ModeBoundaryLine(x, y,hasFrontChanged);

                    ModeAnalysis ma = new ModeAnalysis();
                    ma.loadBoundaryLines(lowerline, upperline);

                    Vector width = new Vector();
                    ma.getModeWidths(out width);
                    double maxwidth = width.getVectorAttribute(VectorAttribute.max);
                    double minwidth = width.getVectorAttribute(VectorAttribute.min);
                    if (i >=3)
                    {
                        widthChange += Math.Abs(maxwidth - minwidth);
                        actualnum++;
                    }
                    
                    modewidths.Add(new Pair<double, double>(maxwidth,minwidth));
                }
                map_caled = true;
                if (actualnum>0)
                {
                    modalDistortion = widthChange / actualnum;
                }
                else
                {
                    //set to a very large num so that it will failed 
                    modalDistortion = 201;
                }                
            }            
        }
        private void findLMBoundaries()
        {
            findJumps_Etalon(_map_forward_photodiode1_current,
                _map_forward_photodiode1_current,true);           //Jack Zhang find jump point from Itx---PD1

            findJumps_Etalon(_map_reverse_photodiode1_current,
                _map_reverse_photodiode1_current, false);


            linkJumps_RearCut();
            if (Convert.ToInt32(this.superMapSitting["ExtrapolateToMapEdges"].ToString()) != 0) //Jack Zhang add "Extrapolate to map edges" function for SM 2010.01-08
	        {
         
                insertExtrapolatedMapBorderPoints_RearCut();
	        }

            createLMBoundaryLines_RearCut();           
        }
        private void findJumps_Etalon(CLaserModeMap map_in, CLaserModeMap map_out, bool forward_map)
        {
            map_in._boundary_points.Clear();

            int rows = 0;
            int cols = 0;
           
            CLaserModeMap map_out_temp;
            map_out_temp = map_out;

            cols = (int)_filtered_middle_line.getLength();//cols=201, echo
            rows = (int)_phase_current.getLength();//rows=101, echo

            for(int slice = 0; slice < rows ; slice++ )
            {
                int index_of_1st_point_on_line = slice*cols;
                modeBoundaries_Etalon(map_in._map, slice, rows, cols,
               map_out._boundary_points, forward_map);
            }

            //string abs_filepath = map_out._abs_filepath;
          //  abs_filepath =  abs_filepath.Replace(MATRIX_, JUMPSE_);
            if (_result_dir[_result_dir.Length-1] != '\\')
            {
                _result_dir += '\\';
            }

            string directStr ="";

            NameValueCollection SupperModeMapDataSource_Etalon = (NameValueCollection)SettingConfigure.cfgReader.GetSection("CloseGridSetting/SuperMap/BoundaryDetection");
            if (Convert.ToBoolean(SupperModeMapDataSource_Etalon["UseEtalonData"].ToString() == "0"))
            {
                if (!forward_map)
                {
                    directStr = "reversePowerRatio_DSDBR01_";
                }
                else
                {
                    directStr = "forwardPowerRatio_DSDBR01_";
                }
            }
            else
            {
                if (!forward_map)
                {
                    directStr = "reversePD1Current_DSDBR01_";
                }
                else
                {
                    directStr = "forwardPD1Current_DSDBR01_";
                }
            }
            string jumppath = _result_dir + JUMPSE_ + directStr + _laser_id + "_" + _timestamp + "_SM" + Sm_number.ToString()
                + "_P.csv";
            string jumppath_temp = _result_dir + JUMPSE_ + directStr + _laser_id + "_" + _timestamp + "temp_SM" + Sm_number.ToString()
    + "_P.csv";
            // Write jumps detected in filtered map to file
            map_out.writeJumpsToFile(jumppath);

            //for (int i = 0; i < map_out._boundary_points.Count - 1; i++)
            //{
            //    for (int j = i + 1; j < map_out._boundary_points.Count; j++)
            //    {

            //        if (map_out._boundary_points[i].First > map_out._boundary_points[j].First)
            //        {
            //            map_out_temp._boundary_points[i] = map_out._boundary_points[j];

            //        }

            //    }
            //}
            //map_out_temp.writeJumpsToFile(jumppath_temp);
        }
        private void linkJumps_RearCut()
        {
            linkJumpsByIndex_RearCutM3(_map_forward_photodiode1_current,true); //Jack Zhang change to PD1 2010-01-08

            _map_forward_photodiode1_current.WriteLinkJumpsToFile(_result_dir + "forward_linkpoint" + USCORE + LASER_TYPE_ID_DSDBR01_CSTRING
               + USCORE + _laser_id + USCORE + _timestamp + USCORE + _sm_number + CSV);

            linkJumpsByIndex_RearCutM3(_map_reverse_photodiode1_current, true);

            _map_reverse_photodiode1_current.WriteLinkJumpsToFile(_result_dir + "reverse_linkpoint" + USCORE + LASER_TYPE_ID_DSDBR01_CSTRING
   + USCORE + _laser_id + USCORE + _timestamp + USCORE + _sm_number + CSV);
        }
        private void linkJumpsByIndex_RearCutM3(CLaserModeMap map_in, bool forward_map)
        {
            long incBoxX = Convert.ToInt64(this.superMapSitting["BoxXDirectionStep"]); //Jack.Zhang "Box x direction step=1"
            long incBoxY = Convert.ToInt64(this.superMapSitting["BoxYDirectionStep"]); //Jack.Zhang "Box y direction step=5"
            long maxBoxInc = Convert.ToInt64(this.superMapSitting["BoxStepLimit"]);//Jack.Zhang "Box Step Limit=5"
            long maxAltLowerLimitX = Convert.ToInt64(this.superMapSitting["AlternateDirectionMaxLowerX"]); //Jack.Zhang "Alternate Direction Max Lower x=10"
            long altMaxBoxInc = Convert.ToInt64(this.superMapSitting["AlternateBoxStepLimit"]); //Jack.Zhang "Alternate Box Step Limit=4"
            long altIncBoxX = Convert.ToInt64(this.superMapSitting["AlternateBoxXDirectionStep"]);    //Jack.Zhang "Alternate Box x direction step=2"
            long altIncBoxY = Convert.ToInt64(this.superMapSitting["AlternateBoxYDirectionStep"]); //Jack.Zhang "Alternate Box y direction step=1"
            long minBoundaryLength = Convert.ToInt64(this.superMapSitting["MinBoundaryLength"]); //Jack.Zhang "Min Boundary Length=20"
            long endOfBoxOffsetX = Convert.ToInt64(this.superMapSitting["EndOfBoxX"]); //Jack.Zhang "End of Box x=0"
            long endOfBoxOffsetY = Convert.ToInt64(this.superMapSitting["EndOfBoxY"]); //Jack.Zhang "End of Box y=0"

            bool forward = true;
            long steps_count =(long)(_filtered_middle_line._points.Count);
		    long ramp_length =(long)(_phase_current._currents.Count);

            map_in._linked_boundary_points.Clear();

            // keep a count of how many lines are identified
            long boundary_line_count = 1;

            // Step 1. Make a copy of the boundary points
            List<xyData> tempBoundaryPts = new List<xyData>();
            List<xyData> linked_boundary_points = new List<xyData>();
            List<xyData> unlinked_boundary_points = new List<xyData>();
            xyData boxSize = new xyData();
            xyData altBoxSize = new xyData();
            xyData nearestPt = new xyData();
            xyData tempXyData;
            xyData lastPt = new xyData();

            // Work out the distance to each point from the top left hand corner
            long gapPosX, gapPosY;

            boxSize.xPos = 0;
            boxSize.yPos = ramp_length;

            long endOfBoxPosX = steps_count - endOfBoxOffsetX;
	        long endOfBoxPosY = ramp_length - endOfBoxOffsetY;

            for(int i=0;i<map_in._boundary_points.Count;i++)
            {
                tempXyData = new xyData();
                tempXyData.first = map_in._boundary_points[i].First;
                tempXyData.second = map_in._boundary_points[i].Second;
                tempXyData.xPos = (map_in._boundary_points[i].Second) / ramp_length;//Irear index
                tempXyData.yPos = (map_in._boundary_points[i].Second) % ramp_length;//Iphase index
                // Only analyse jumps in the inclusion zone.
                if (tempXyData.xPos <= endOfBoxPosX &&
                    tempXyData.yPos <= endOfBoxPosY)
                {
                    gapPosX = tempXyData.xPos - boxSize.xPos;
                    gapPosY = boxSize.yPos - tempXyData.yPos;
                    if (gapPosX != 0)
                        gapPosX = (long)Math.Pow((double)gapPosX, 2);

                    if (gapPosY != 0)
                        gapPosY = (long)Math.Pow((double)gapPosY, 2);

                    // Very Important you don't delete this, as we use it later to
                    // sort the boundary lines into the correct order.
                    tempXyData.gapDist = Math.Sqrt((double)gapPosX + (double)gapPosY);

                    tempBoundaryPts.Add(tempXyData);
                }
            }

            // Step 2. Starting from the 1st jump point, start looking for jumps points we can link to
            long linkCnt = 0;
            int currPosIter = 0;
            int nextPosIter = 0;
            int ptsInBoxIter = 0;

            // We are going to empty tempBoundaryPts by either assigning the jump pt to a boundary, or
	        // put it in the unlinked pts container
	        double tempSlope = 0;
	        double lineSlope = 0;
	        long boxCnt = 1;
	        bool foundLinkPt = false;
	        bool quitSearch = false;

            List<xyData>  boundaryPtsInBox = new List<xyData>();
            List<xyData> lineValues = new List<xyData>();

            lastPt.first = (tempBoundaryPts[tempBoundaryPts.Count - 1].first);
            lastPt.second = (tempBoundaryPts[tempBoundaryPts.Count - 1].second);
            lastPt.xPos = tempBoundaryPts[tempBoundaryPts.Count - 1].xPos;
            lastPt.yPos = tempBoundaryPts[tempBoundaryPts.Count - 1].yPos;

            while (tempBoundaryPts.Count != 0)
            {
                //tempBoundaryPts.RemoveAt(tempBoundaryPts.Count - 1);
                currPosIter = 0;
                nextPosIter = 1;

                if ((tempBoundaryPts[currPosIter]).first == lastPt.first)
                {
                    // We have reached the end of the search, so discard the last pt
                    unlinked_boundary_points.Add(tempBoundaryPts[currPosIter]);
                    tempBoundaryPts.RemoveAt(currPosIter);
                    nextPosIter = tempBoundaryPts.Count;
                }

                // we are going to use the "ever expanding box" search method
                while (nextPosIter != tempBoundaryPts.Count)
                {
                    boundaryPtsInBox.Clear();

                    // Starting from our current point, go through all the boundary points &
                    // if valid, put them in the box pts
                    ptsInBoxIter = nextPosIter;
                    quitSearch = false;
                    foundLinkPt = false;
                    boxCnt = 1;
                    boxSize.xPos = incBoxX;
                    boxSize.yPos = incBoxY;
                    altBoxSize.xPos = altIncBoxX;
                    altBoxSize.yPos = altIncBoxY;

                    while (quitSearch == false)
                    {
                        while (ptsInBoxIter != tempBoundaryPts.Count)
                        {
                            // Is it in the box?
                            gapPosX = (tempBoundaryPts[ptsInBoxIter]).xPos - (tempBoundaryPts[currPosIter]).xPos;
                            gapPosY = (tempBoundaryPts[ptsInBoxIter]).yPos - (tempBoundaryPts[currPosIter]).yPos;

                            //-------------------    Echo remed the following lines   -------------------------
                            //
                            //////////// We are only searching forward, hence > 0. This direction has priority.
                            //////////if ((gapPosX >= 0 && gapPosX <= (long)boxSize.xPos) &&
                            //////////    (gapPosY >= 0 && gapPosY <= (long)boxSize.yPos)) //jack.zhang 2010.01.20 change "<" to "<=" for marginal fail case
                            //////////{
                            //////////    ((tempBoundaryPts[ptsInBoxIter]).priority) = 1;
                            //////////    if ((tempBoundaryPts[ptsInBoxIter]).xPos < maxAltLowerLimitX)
                            //////////        ((tempBoundaryPts[ptsInBoxIter]).priority) = 2;//low priority for low Irear index
                            //////////    boundaryPtsInBox.Add(tempBoundaryPts[ptsInBoxIter]);
                            //////////    foundLinkPt = true;
                            //////////}

                            //////////// We need to search in the other direction now for the lower points only.
                            //////////if ((tempBoundaryPts[currPosIter]).xPos < maxAltLowerLimitX)
                            //////////{
                            //////////    // We are only searching forward, hence > 0
                            //////////    if ((gapPosX >= 0 && gapPosX <= (long)altBoxSize.xPos) &&
                            //////////        (gapPosY >= 0 && gapPosY <= (long)altBoxSize.yPos)) //jack.zhang 2010.01.20 change "<" to "<=" for marginal fail case
                            //////////    {
                            //////////        // It's quicker to put the same pt in twice, rather than try and search 
                            //////////        // through the list & then decide to add. We simply give it a lower priority, but
                            //////////        // we do not terminate the search. These pts are bonus pts to try and link if we 
                            //////////        // fail to find any high priority pts.
                            //////////        ((tempBoundaryPts[ptsInBoxIter]).priority) = 1;
                            //////////        boundaryPtsInBox.Add(tempBoundaryPts[ptsInBoxIter]);
                            //////////        foundLinkPt = true;
                            //////////    }
                            //////////}
                            //-------------------             End of remed , 2010,6.30 ---------------------------


                           //  We need to search in the other direction now for the lower points only.
                            if ((tempBoundaryPts[currPosIter]).xPos < maxAltLowerLimitX)
                            {
                                // We are only searching forward, hence > 0
                                if ((gapPosX >= 0 && gapPosX <= (long)altBoxSize.xPos) &&
                                    (gapPosY >= 0 && gapPosY <= (long)altBoxSize.yPos)) //jack.zhang 2010.01.20 change "<" to "<=" for marginal fail case
                                {
                                    // It's quicker to put the same pt in twice, rather than try and search 
                                    // through the list & then decide to add. We simply give it a lower priority, but
                                    // we do not terminate the search. These pts are bonus pts to try and link if we 
                                    // fail to find any high priority pts.
                                    ((tempBoundaryPts[ptsInBoxIter]).priority) = 2;
                                    boundaryPtsInBox.Add(tempBoundaryPts[ptsInBoxIter]);
                                    foundLinkPt = true;
                                }
                            }
                            else
                            {
                                if ((gapPosX >= 0 && gapPosX <= (long)boxSize.xPos) &&
                                     (gapPosY >= 0 && gapPosY <= (long)boxSize.yPos)) //jack.zhang 2010.01.20 change "<" to "<=" for marginal fail case
                                {
                                    ((tempBoundaryPts[ptsInBoxIter]).priority) = 1;
                                    boundaryPtsInBox.Add(tempBoundaryPts[ptsInBoxIter]);
                                    foundLinkPt = true;
                                }
                            }


                            // Can we finish the scan early, or check next jump pt?
                            if (gapPosY > (long)boxSize.yPos || gapPosX > (long)altBoxSize.xPos)//jack.zhang add  "|| gapPosX > (long)altBoxSize.xPos"
                                ptsInBoxIter = tempBoundaryPts.Count;
                            else
                            {
                                ptsInBoxIter++;
                                //currPosIter++;
                            }
                        }

                        if (foundLinkPt == false)
                        {
                            // Increase Box search size & start again
                            boxSize.xPos += incBoxX;
                            boxSize.yPos += incBoxY;

                            // Only increase alt box search if still in limit
                            if (boxCnt < altMaxBoxInc)
                            {
                                altBoxSize.xPos += altIncBoxX;
                                altBoxSize.yPos += altIncBoxY;
                            }

                            ptsInBoxIter = nextPosIter;
                            boxCnt++;
                        }
                        else
                        {
                            quitSearch = true;
                        }

                        if (boxCnt > maxBoxInc)
                        {
                            quitSearch = true;
                        }
                    }// End of while( quitSearch == false )


                    // Reset the nearest point to worst case
                    nearestPt.first = lastPt.first;

                    if(boundaryPtsInBox.Count != 0)
                    {

                        // Find the nearest valid jump pt
                        ptsInBoxIter = 0;

                        // These are the worst case gaps
                        double gapDistance = Math.Sqrt(Math.Pow((double)ramp_length, 2) + Math.Pow((double)steps_count, 2));
                        bool checkLowerPriority = true;

                        // Higher priority pts first
                        while (ptsInBoxIter != boundaryPtsInBox.Count)
                        {
                            gapPosX = (tempBoundaryPts[currPosIter]).xPos - (boundaryPtsInBox[ptsInBoxIter]).xPos;
                            gapPosY = (tempBoundaryPts[currPosIter]).yPos - (boundaryPtsInBox[ptsInBoxIter]).yPos;

                            double thisGap = Math.Sqrt(Math.Pow((double)gapPosX, 2) + Math.Pow((double)gapPosY, 2));

                            // this is the nearest point.
                            if (thisGap < gapDistance)
                            {
                                // Only update if a high priority
                                if ((boundaryPtsInBox[ptsInBoxIter]).priority == 1)
                                {
                                    gapDistance = thisGap;
                                    nearestPt.first = (boundaryPtsInBox[ptsInBoxIter]).first;
                                    checkLowerPriority = false;
                                }
                            }
                            ptsInBoxIter++;
                        }

                        if (checkLowerPriority == true)
                        {
                            ptsInBoxIter = 0;

                            while (ptsInBoxIter != boundaryPtsInBox.Count)
                            {
                                gapPosX = (tempBoundaryPts[currPosIter]).xPos - (boundaryPtsInBox[ptsInBoxIter]).xPos;
                                gapPosY = (tempBoundaryPts[currPosIter]).yPos - (boundaryPtsInBox[ptsInBoxIter]).yPos;

                                double thisGap = Math.Sqrt(Math.Pow((double)gapPosX, 2) + Math.Pow((double)gapPosY, 2));

                                // this is the nearest point.
                                if (thisGap < gapDistance)
                                {
                                    gapDistance = thisGap;
                                    nearestPt.first = (boundaryPtsInBox[ptsInBoxIter]).first;
                                }
                                ptsInBoxIter++;
                            }
                        }

                        (tempBoundaryPts[currPosIter]).second = boundary_line_count;
                        linked_boundary_points.Add(tempBoundaryPts[currPosIter]);
                        tempBoundaryPts.RemoveAt(currPosIter);

                        linkCnt++;
                    }
                    else
                    {
                        // If we get here with the linkCnt as 0, then we have an unlinked jump pt
                        if (linkCnt == 0)
                        {
                            unlinked_boundary_points.Add(tempBoundaryPts[currPosIter]);

                            tempBoundaryPts.RemoveAt(currPosIter);
                        }
                        else
                        {
                            // Add the last point we have been trying to link to the boundary line.

                            (tempBoundaryPts[currPosIter]).second = boundary_line_count;
                            linked_boundary_points.Add(tempBoundaryPts[currPosIter]);
                            tempBoundaryPts.RemoveAt(currPosIter);
                        }
                    }

                    // Reset the counters to the new position 
                    if (nearestPt.first != lastPt.first)
                    {
                        currPosIter = 0;

                        while ((tempBoundaryPts[currPosIter]).first != nearestPt.first &&
                                currPosIter != tempBoundaryPts.Count)
                            currPosIter++;

                        nextPosIter = currPosIter;

                        if (nextPosIter != tempBoundaryPts.Count)
                            nextPosIter++;
                    }
                    else
                        nextPosIter = tempBoundaryPts.Count;  // Quit the while loop
                }

                // Set boundary count to next line, excluding unlinked pts.
                if (linkCnt != 0)
                    boundary_line_count++;

                // Clear the linker count
                linkCnt = 0;
            }// End of while(tempBoundaryPts.empty() == false)

            
            // Step 3. Remove any boundary lines that are less than x pts long. 
            //         First we need to count up the pts in each line as the data 
            //         is not in order.
            
            tempBoundaryPts.Clear();
            long boundaryPtsTotal = 0;
            long numLines = boundary_line_count;

            boundary_line_count = 1;  // First boundary line

            
            // Count how many pts are on each boundary line
            while (boundary_line_count <= numLines)
            {
                tempXyData = new xyData();
                tempXyData.second = boundary_line_count;
                tempXyData.first = 0;
                currPosIter = 0;                
                // Iterate through data for each line
                while (currPosIter != linked_boundary_points.Count)
                {
                    if ((linked_boundary_points[currPosIter]).second == boundary_line_count)
                        tempXyData.first = tempXyData.first +1;

                    currPosIter++;
                }

                tempBoundaryPts.Add(tempXyData);
                boundary_line_count++;  // Next line
            }


            currPosIter = 0;

            while (currPosIter != tempBoundaryPts.Count)
            {
                // second = boundary line, first = numPts on boundary line
                if ((tempBoundaryPts[currPosIter]).first < minBoundaryLength)
                {
                    // Insufficient pts, so erase from the list
                    nextPosIter = 0;

                    while (nextPosIter != linked_boundary_points.Count)
                    {
                        if ((linked_boundary_points[nextPosIter]).second == (tempBoundaryPts[currPosIter]).second)
                        {
                            linked_boundary_points.RemoveAt(nextPosIter);

                            // Reset ptr after erase and start again
                            nextPosIter = 0;
                        }
                        else
                            nextPosIter++;
                    }
                }

                currPosIter++; // Next boundary line
            }

            // We now have a count of the numPts in each boundary line, so we can test
            // for the removal of short lines

            boundary_line_count = 1;  // first line

            // Start the raster scan
            while (linked_boundary_points.Count != 0)
            {
                // Start from the beginning
                currPosIter = 0;

                // This is the worst case gap
                double nearestGapDist = Math.Sqrt(Math.Pow((double)ramp_length, 2) + Math.Pow((double)steps_count, 2));

                // Find the nearestPt
                while (currPosIter != linked_boundary_points.Count)
                {
                    if ((linked_boundary_points[currPosIter]).gapDist < nearestGapDist)
                    {
                        ptsInBoxIter = currPosIter;
                        nearestGapDist = (linked_boundary_points[currPosIter]).gapDist;
                    }

                    currPosIter++;
                }

                long boundary = (linked_boundary_points[ptsInBoxIter]).second;

                // Now transfer this boundary line, with the new boundary line number 
                // to the map container

                currPosIter = 0;

                while( currPosIter != linked_boundary_points.Count )
		        {
			        // second = boundary line
			        if( (linked_boundary_points[currPosIter]).second == boundary )
			        {
				        // we have a match. Add it to the map container, and remove it from the list 
                        map_in._linked_boundary_points.Add( new Pair<long,long>(
                            (linked_boundary_points[currPosIter]).first, boundary_line_count));
				        linked_boundary_points.RemoveAt(currPosIter);
				        currPosIter = 0;
			        }
			        else
			        {
				        currPosIter++;
			        }
		        }

		        // Next line
		        boundary_line_count++;
            }// End of 	while( linked_boundary_points != empty )

            //Console.Write("A");

        }
        private void modeBoundaries_Etalon(List<double> map_etalon, int start_point_index,
            int index_increment, int line_length, List<Pair<long, long>> boundary_map,
            bool forward_map)
        {

            double min_dPr_dI_zero_cross_jump_size = 0;

            // keep a count of how many jumps are detected
            long boundary_count = 0;

            // store detected jumps in a map
            List<Pair<long,long>> jumps = new List<Pair<long,long>>();
            Pair<long, double> dPr_dI_at_jumps = new Pair<long,double>();

             int jump_found = 0;
             int cnt = 0;
             int i = 0;

             List<double> pwrRatio = new List<double>();
             List<double> filtered_PwrRatio = new List<double>();
             List<double> deriv_A = new List<double>();
             List<double> deriv_B = new List<double>();


             double dPr_dI_A;
             bool saveDerivData = true;

             // Step 1. Create Rear cut slice of Power ratio data
             filtered_PwrRatio.Clear();
             for (cnt = start_point_index; cnt <= map_etalon.Count - 1; cnt += index_increment)
                 filtered_PwrRatio.Add(map_etalon[cnt]);

             this.Max_Value_Etalon = -1;
             for (cnt = 0; cnt < filtered_PwrRatio.Count - 1; cnt++)
             {
                 dPr_dI_A = Math.Abs((filtered_PwrRatio[cnt + 1] - filtered_PwrRatio[cnt]) / (0.35));
                 if (dPr_dI_A > this.Max_Value_Etalon)
                     this.Max_Value_Etalon = dPr_dI_A;
                 deriv_A.Add(dPr_dI_A);
             }

             // Avoid duplication of jump pts on the last column
             deriv_A.Add(0.00);

             // Step 4. We must now detect any switches in the front pair, as this can generate a false jump.		
             short initialFtPair = _filtered_middle_line._points[0].front_pair_number;

             List<int>  ftPairSwitchIndices = new List<int>();

             for (i = 0; i < (int)_filtered_middle_line._points.Count; i++)
             {
                 if (_filtered_middle_line._points[i].front_pair_number != initialFtPair)
                 {
                     // we have a change in the front pair, so save and make this the new check 
                     ftPairSwitchIndices.Add(i);
                     initialFtPair = _filtered_middle_line._points[i].front_pair_number;
                 }
             }

             // Step 5. After detecting any switch changes. we need to mask them out.
             int mask = 0;
             int k;
            for(k=0;k<ftPairSwitchIndices.Count;k++)
            {
                // We need to offset the front pair switch index by one to point at the last index point of the 
                // previous front pair.
                int index = ftPairSwitchIndices[k] - 1;

                if (index < mask)
                {
                    for (i = 0; i < (index + mask); i++)
                    {
                        deriv_A[i] = 0;
                    }
                }
                else if (index <= (int)(deriv_A.Count - (mask + 1)))
                {
                    for (i = (mask * -1); i <= mask; i++)
                    {
                        deriv_A[index + i] = 0;
                    }
                }
                else
                {
                    for (i = index - mask; i < (int)deriv_A.Count; i++)
                    {
                        deriv_A[i] = 0;
                    }
                }
            }

            // Step 6. We can now detect our jumps.

            //test etalon value here~
            double _dynamic_threshold_for_jumps_detection_etalon =Convert.ToDouble( superMapBoundarySitting["DynamicThresholdForJumpsDetectionWithEtalonData"]);
            min_dPr_dI_zero_cross_jump_size = (double)this.Max_Value_Etalon * _dynamic_threshold_for_jumps_detection_etalon;

            for( cnt = 0; cnt < deriv_A.Count; cnt++ )
	        {
                if (deriv_A[cnt] >= min_dPr_dI_zero_cross_jump_size)
                {
                    // We have detected a boundary jump.
                    long ptA = start_point_index + (cnt-1) * index_increment; //cnt
                    long ptB = start_point_index + (cnt+0) * index_increment;  //cnt+1

                    //start_ponit_index ===> col;
                    //cnt               ===> row;


                    ////discard jumppoints at the beginning of rear and phase area. Jack.Zhang Remove for compare to CG 
                    //if ((start_point_index < 30 && cnt <= 4) || (start_point_index < 40 && cnt <= 1)) 
                    //    continue;

                    if ((cnt == 0)) //Jack.Zhang remove jump point on Irear=0mA 
                        continue;

                    if (ptA < map_etalon.Count && ptB < map_etalon.Count)
                    {
                        jumps.Add(new Pair<long, long>(ptA,ptB));
                    }
                }
	        }

            // Step 7. Finally save the jumps into the boundary map.
  
                   
                        
            for(i=0;i<jumps.Count;i++)
            {
                boundary_map.Add(jumps[i]);
            }

        }
        private void insertExtrapolatedMapBorderPoints_RearCut()
        {
            insertExtrapolatedMapBorderPointsInMap(_map_forward_photodiode1_current._boundary_points,
                _map_forward_photodiode2_current._linked_boundary_points);
            insertExtrapolatedMapBorderPointsInMap(_map_reverse_photodiode1_current._boundary_points,
                _map_reverse_photodiode2_current._linked_boundary_points);
        }
        private void insertExtrapolatedMapBorderPointsInMap(List<Pair<long,long>>boundary_points, List<Pair<long,long>> linked_boundary_points)
        {
            int top_line_num = -1;
            for (int i_f = 0;i_f != linked_boundary_points.Count; i_f++)
            { // find the number of lines identified
                if ((int)((linked_boundary_points[i_f]).Second) > top_line_num) top_line_num = (int)(linked_boundary_points[i_f]).Second;
            }

            List<double> x_position = new List<double>();
            List<double> y_position = new List<double>();

            int ramp_length = (int)(_phase_current._currents.Count);
            int steps_count = (int)(_filtered_middle_line._points.Count);
            for (int line_number = 0; line_number <= top_line_num; line_number++)
            {
                x_position.Clear();
                y_position.Clear();

                int i_end = linked_boundary_points.Count;
                int i_first_in_line = i_end;
                int i_last_in_line = i_end;


                for (int i_f = 0; i_f != linked_boundary_points.Count; i_f++)
                {
                    // find the number of lines identified
                    if ((int)((linked_boundary_points[i_f]).Second) == line_number)
                    {
                        if (i_first_in_line == i_end)
                        {
                            i_first_in_line = i_f;
                        }
                        i_last_in_line = i_f;

                        int y_pos = (int)(((linked_boundary_points[i_f]).First) / ramp_length);
                        int x_pos = (int)((linked_boundary_points[i_f]).First) - y_pos * ramp_length;

                        x_position.Add((double)x_pos);
                        y_position.Add((double)y_pos);
                    }
                }

                // At this point, x_position and y_position contain the coordinates
                // for point on one line, defined by line_number

                if ((x_position.Count) > 3)
                {
                    double slope;
                    double intercept;  
                
                   // VectorAnalysis.linearFit(x_position, y_position, out slope, out intercept);
                    PolyFit pf = Alg_PolyFit.PolynomialFit(x_position.ToArray(), y_position.ToArray(), 2);
                    //y = slope*x+intercept;
                    // calculated extrapolated point to the left of a line
                    // double y_left_point =  intercept;
                    double y_left_point = pf.Coeffs[0];
                    double x_left_point = 0;
                    

                    // actually we want to add a control point in the left boarder line
                    // but it always causes saome unpleasant things such as missing line
                    // here we constraint that y_left_point is larger than five even we may 
                    // lose some points 
                    // modified by bob.lv 2009-02-23
                    if (y_left_point >= 5)
                    {
                        // insert into boundary points before first found in line
                        //i_first_in_line;
                        long indexnum = (long)y_left_point * (long)ramp_length + (long)x_left_point;
                        boundary_points.Add(new Pair<long, long>(indexnum, indexnum));
                        linked_boundary_points.Insert(i_first_in_line,
                            new Pair<long, long>(indexnum, (long)line_number));
                    }

                    // calculated extrapolated point to the right of a line
                    double x_right_point = ramp_length - 1;
                    //double y_right_point = slope*(ramp_length - 1) + intercept;
                    double y_right_point = pf.Coeffs[2] * Math.Pow(ramp_length - 1,2) + 
                        pf.Coeffs[1]*(ramp_length)+pf.Coeffs[0];

                    if (y_right_point <= (steps_count - 1))
                    {
                        // insert into boundary points after last found in line
                        long indexnum = (long)y_right_point * (long)ramp_length + (long)x_right_point;
                        boundary_points.Add(new Pair<long, long>(indexnum, indexnum));
                        i_last_in_line++;
                        linked_boundary_points.Insert(i_last_in_line,
                            new Pair<long, long>(indexnum, (long)line_number));
                    }
                }
            }
        }
        private void createLMBoundaryLines_RearCut()
        {
            createModeBoundaryLines_RearCutM2(_map_forward_photodiode1_current,_lm_upper_lines);//Jack zhang change to PD1
            createModeBoundaryLines_RearCutM2(_map_reverse_photodiode1_current, _lm_lower_lines);

            checkForwardAndReverseLines();

            associateForwardAndReverseLines( );

            removeUnassociatedLines();
        }
        private void createModeBoundaryLines_RearCutM2(CLaserModeMap map_in,List<CDSDBRSupermodeMapLine> lines_in)
        {
            // How many pts are we going to use to extrapolate to the 
            // next one?
           long numExtrapolatedPts = 10;

           // What order polyfit are we going to use, preferrably odd
           int order = 5;

           // Used to check that the calculated slope looks sensible
           double minPermittedLocalSlope = 0.08;
            
            // Used to remove duplicate boundary lines
            long minLmWidth = 2;

            // Step 1. Clear any existing lines
            for (int i = 0; i < (int)(lines_in.Count); i++)
                lines_in[i].clear();

            lines_in.Clear();
            
            // Step 2. Determine ramp/step lengths
            int map_ramp_line_length = (int)(_phase_current._currents.Count); ;
            int steps_count = (int)(_filtered_middle_line._points.Count);
            
            // Define our map boundaries wrt to array positions ( hence -1 ).
            long lowerMapEdgeX = 0;
            long lowerMapEdgeY = 0;
            long upperMapEdgeY = steps_count-1;
            long upperMapEdgeX = map_ramp_line_length-1;

            // Step 3. Make a copy of the linked boundary points
            int ptsIter = 0;
            int i_jump=0;

            List<xyData> tempBoundaryPts = new List<xyData>();
            

            while (ptsIter != map_in._linked_boundary_points.Count)
            {
                // Find the boundary point data for this linked jump point
                for (int i = 0; i < map_in._boundary_points.Count; i++)
                {
                    if (map_in._boundary_points[i].First == map_in._linked_boundary_points[ptsIter].First)
                    {
                        i_jump = i;
                        break;
                    }
                }
                xyData tempXyData = new xyData();
                tempXyData.first = (map_in._boundary_points[i_jump]).First;
                tempXyData.second = (map_in._boundary_points[i_jump]).Second;
                tempXyData.yPos = (map_in._boundary_points[i_jump]).Second / map_ramp_line_length;
                tempXyData.xPos = (map_in._boundary_points[i_jump]).Second % map_ramp_line_length;

                tempXyData.boundaryLine = map_in._linked_boundary_points[ptsIter].Second;

                tempBoundaryPts.Add(tempXyData);

                ptsIter++;
            }

            // Step 4. Iterate through the tempBoundaryPts vector and remove all the 
            //         pts on the same boundary line.
            int  linkedPtsIter = 0; //tempBoundaryPts.begin();
            List<xyData> boundaryLineData = new List<xyData>();

            // First boundary line
            long boundary_line_number = 1;

            while (tempBoundaryPts.Count != 0)
            {
                boundaryLineData.Clear();
                linkedPtsIter = 0;

                while (linkedPtsIter != tempBoundaryPts.Count)
                {
                    if ((tempBoundaryPts[linkedPtsIter]).boundaryLine == boundary_line_number)
                    {
                        boundaryLineData.Add(tempBoundaryPts[linkedPtsIter]);
                        tempBoundaryPts.RemoveAt(linkedPtsIter);
                        linkedPtsIter = 0;
                    }
                    else
                        linkedPtsIter++; // Next point in the list
                }

                // Step 5. We should now have all the boundary line linked points
                //         Fit a higher order poly & then use the coefficients to
                //         build a fitted line.
                double[] xData = new double[boundaryLineData.Count];
                double[] yData = new double[boundaryLineData.Count];
                int numPts;

                for (numPts = 0; numPts < (int)boundaryLineData.Count; numPts++)
                {
                    xData[numPts] = boundaryLineData[numPts].xPos;
                    yData[numPts] = boundaryLineData[numPts].yPos;
                }
                PolyFit pf = new PolyFit();

                pf = Alg_PolyFit.PolynomialFit(xData, yData, order);

                double[] fittedYData = pf.FittedYArray;
                double[] coeffs = pf.Coeffs;

                List<long> fittedXarray = new List<long>();
                List<long> fittedYarray = new List<long>();

                // Now use the coefficients to generate the filled in data.
                linkedPtsIter = 0;

                long firstDataPt = (boundaryLineData[linkedPtsIter]).xPos;

                linkedPtsIter = boundaryLineData.Count;
                linkedPtsIter--;

                long lastDataPt = (boundaryLineData[linkedPtsIter]).xPos;
                double y;
                for( long iDataPt = firstDataPt; iDataPt <= lastDataPt; iDataPt++ )
                {
                    y = 0;

                    for( int iPowerOfX = 0; iPowerOfX <= order; iPowerOfX++ )
                        y += coeffs[iPowerOfX] * Math.Pow( (double)iDataPt, (double)iPowerOfX );
                    
                    long polyY = 0;
                    // Make sure we don't exceed the map boundary.
                    if(y > lowerMapEdgeY && y < upperMapEdgeY)
                    {
                        polyY = (long)Math.Floor(y+0.5); // Round the value.
                        fittedXarray.Add(iDataPt); 
                        fittedYarray.Add(polyY); 
                    }
                }

                // Step 6. If the fit is bad, then we may end up with an empty fitted array, 
                // in which case we will have to revert to a simple least squared fit.

                double localSlope, localIntercept, yVal;
                bool deleteLine = false;
                if (fittedXarray.Count < numExtrapolatedPts || fittedYarray.Count < numExtrapolatedPts)
                {
                    fittedXarray.Clear();
                    fittedYarray.Clear();
                    VectorAnalysis.LeastSqFit(xData, yData, 0, numPts - 1, out localIntercept, out localSlope);

                    // check that the slope is acceptable. There's no point trying to build a line
                    // from an unacceptable slope value ( i.e. a flat-liner that will cross other 
                    // boundary lines.
                    if( localSlope < minPermittedLocalSlope )
                        deleteLine = true;
                    else
                    {	
                        for(long iDataPt = firstDataPt; iDataPt <= lastDataPt; iDataPt++ )
                        {
                            y = localSlope*iDataPt + localIntercept;
		                    
                            long lsfY = 0;		
                            // Make sure we don't exceed the map boundary.
                            if(y > lowerMapEdgeY && y < upperMapEdgeY)
                            {
                                lsfY = (long)Math.Floor(y+0.5); // Round the value.
                                
                                fittedXarray.Add(iDataPt); 
                                fittedYarray.Add(lsfY);                                 
                            }
                        }
                    }
                }

                // Step 7. Use a local subset of n pts, & fit a least sq fit line to it.
                //         Use the slope & intercept to extrapolate the next pt on the line
                //         only. Repeat until you reach the left hand edge of the map.

                int subsetSize = 0;
                double[] xDataSubset = null;
                double[] yDataSubset = null;
		
                int  xInsertionPt = 0;
                long currPtX;
                
                int  yInsertionPt = 0;
                long currPtY;
                
                // Do we need to pad out?
                bool quitPadding = false;

                if (deleteLine == false && fittedXarray.Count !=0)
                {
                    // Do we need to pad out?
                    xInsertionPt = 0;
                    currPtX = (fittedXarray[xInsertionPt]);

                    yInsertionPt = 0;
                    currPtY = (fittedYarray[yInsertionPt]);

                    if (currPtY > lowerMapEdgeY && currPtX > lowerMapEdgeX)
                    {
                        if (fittedXarray.Count < numExtrapolatedPts)
                            subsetSize = (int)fittedXarray.Count;
                        else
                            subsetSize = (int)numExtrapolatedPts;
                        
                        //initialization need for complier
                        localIntercept = 0;
                        localSlope = 0;
                        while (subsetSize <= fittedXarray.Count)
                        {
                            // Alternatively, fit a straight line over a subset of the data.
                            xDataSubset = null;
                            xDataSubset = new double[subsetSize];

                            yDataSubset = null;
                            yDataSubset = new double[subsetSize];

                            xInsertionPt = 0;
                            currPtX = (fittedXarray[xInsertionPt]);

                            yInsertionPt = 0;
                            currPtY = (fittedYarray[yInsertionPt]);

                            for (long cnt = 0; cnt < subsetSize; cnt++)
                            {
                                xDataSubset[cnt] = (fittedXarray[xInsertionPt]);
                                yDataSubset[cnt] = (fittedYarray[yInsertionPt]);

                                xInsertionPt++;
                                yInsertionPt++;
                            }

                            // Perform the fit
                            VectorAnalysis.LeastSqFit(xDataSubset, yDataSubset, 0, subsetSize - 1, out localIntercept, out localSlope);

                            if (localSlope < minPermittedLocalSlope)
                                subsetSize++; // try again with more data
                            else
                                subsetSize = (int)fittedXarray.Count + 1; // stop trying
                        }

                        // check that the slope is acceptable. There's no point trying to build a line
                        // from an unacceptable slope value ( i.e. a flat-liner that will cross other 
                        // boundary lines.
                        if (localSlope < minPermittedLocalSlope)
                            deleteLine = true;
                        else
                        {
                            while (quitPadding == false)
                            {
                                // Point the current position to the extrapolated point
                                currPtX--;

                                yVal = localSlope * currPtX + localIntercept;

                                // Check for potential overflow
                                if (yVal < lowerMapEdgeY)
                                    currPtY = lowerMapEdgeY;
                                else
                                    currPtY = (long)Math.Floor(yVal + 0.5); // Round the value

                                // Exceeded map, so pad at edge 
                                if (currPtY > upperMapEdgeY)
                                    currPtY = upperMapEdgeY;

                                // At map edge so add to list then quit
                                if (currPtY == lowerMapEdgeY ||
                                    currPtY == upperMapEdgeY ||
                                    currPtX == lowerMapEdgeX)
                                {
                                    quitPadding = true;
                                }

                                // Now add the "new" current position to the list.
                                fittedXarray.Insert(0, currPtX);
                                fittedYarray.Insert(0, currPtY);
                            }
                        }
                    }
                }
                
                // Step 7. Use a local subset of n pts, & fit a least sq fit line to it.
                //         Use the slope & intercept to extrapolate the next pt on the line
                //         only. Repeat until you reach the right hand edge of the map.
                if (deleteLine == false && fittedXarray.Count != 0)
                {

                    // Do we need to pad out ?
                    xInsertionPt = fittedXarray.Count;
                    xInsertionPt--;
                    currPtX = (fittedXarray[xInsertionPt]);

                    yInsertionPt = fittedYarray.Count;
                    yInsertionPt--;
                    currPtY = (fittedYarray[yInsertionPt]);

                    //initialization need for complie
                    localIntercept = 0;
                    localSlope = 0;
                    if (currPtY < upperMapEdgeY && currPtX < upperMapEdgeX)
                    {
                        if (fittedXarray.Count < numExtrapolatedPts)
                            subsetSize = (int)fittedXarray.Count;
                        else
                            subsetSize = (int)numExtrapolatedPts;

                        while (subsetSize <= fittedXarray.Count)
                        {
                            // Alternatively, fit a straight line over a subset of the data.
                            xDataSubset = null;
                            xDataSubset = new double[subsetSize];

                            yDataSubset = null;
                            yDataSubset = new double[subsetSize];

                            xInsertionPt = fittedXarray.Count;
                            xInsertionPt--;
                            currPtX = (fittedXarray[xInsertionPt]);

                            yInsertionPt = fittedYarray.Count;
                            yInsertionPt--;
                            currPtY = (fittedYarray[yInsertionPt]);

                            for (int cnt = subsetSize - 1; cnt >= 0; cnt--)
                            {
                                xDataSubset[cnt] = (fittedXarray[xInsertionPt]);
                                yDataSubset[cnt] = (fittedYarray[yInsertionPt]);

                                xInsertionPt--;
                                yInsertionPt--;
                            }

                            // Perform the fit
                            VectorAnalysis.LeastSqFit(xDataSubset, yDataSubset, 0, subsetSize - 1, out localIntercept, out localSlope);


                            if (localSlope < minPermittedLocalSlope)
                                subsetSize++; // try again with more data
                            else
                                subsetSize = (int)fittedXarray.Count + 1; // stop trying
                        }

                        // check that the slope is acceptable. There's no point trying to build a line
                        // from an unacceptable slope value ( i.e. a flat-liner that will cross other 
                        // boundary lines.
                        if (localSlope < minPermittedLocalSlope)
                            deleteLine = true;
                        else
                        {
                            quitPadding = false;
                            while( quitPadding == false )
                            {
                                // Point the current position to the extrapolated point
                                currPtX++;
                                yVal = localSlope*currPtX + localIntercept;
                                
                                // Have we turned back on ourselves?
                                if(yVal < lowerMapEdgeY)
                                    currPtY = lowerMapEdgeY; 
                                else
                                    currPtY = (long)Math.Floor(yVal + 0.5); // Round the value
                                
                                // Last pt to pad out
                                if (currPtY > upperMapEdgeY)
                                    currPtY = upperMapEdgeY;

                                // At map edge so add to list then quit
                                if (currPtY == lowerMapEdgeY ||
                                    currPtY == upperMapEdgeY ||
                                    currPtX == upperMapEdgeX )
                                {
                                    quitPadding = true;
                                }

                                // Now add the "new" current position to the list.
                                fittedXarray.Add(currPtX); 
                                fittedYarray.Add(currPtY);
                            }//while
                        }//else
                    }//if (currPtY < upperMapEdgeY && currPtX < upperMapEdgeX)
                } //if (deleteLine == false)  
                //Console.Write("A");

                // Step 8. Save the line and do the next one.
                // Unfortunately we have to adhere to the existing data formats 
                // in case we break something else further down the chain.
                if (deleteLine == false)
                {
                    CDSDBRSupermodeMapLine new_boundary_line = new CDSDBRSupermodeMapLine();

                    xInsertionPt = 0;
                    yInsertionPt = 0;

                    while( xInsertionPt != fittedXarray.Count && 
                        yInsertionPt != fittedYarray.Count )
                        {
                        // transpose the data back to "normal"
                        new_boundary_line._points.Add(convertRowCol2LinePoint((int)(fittedYarray[yInsertionPt]),
                            (int)(fittedXarray[xInsertionPt])));
				
                        xInsertionPt++;
                        yInsertionPt++;
                    }                  
                    lines_in.Add(new_boundary_line); // Save the fitted line                  
                }
                boundary_line_number++;  // Next boundary line to fit
            } //while (tempBoundaryPts.Count != 0)  
       
            // Step 9. We now have an added complication. On noisy maps we can end up with 2-3
            //         lines that are in close proximty, 
            //         So at this point we are going to check the distance between the 
            //         lines, and remove any that are too close. If it is too close, we are going to keep the 
            //         longest line, and remove the shorter one.
            
            int currLineInIter = 0;
            int nextLineInIter = 0;
            nextLineInIter++;

            while (nextLineInIter != lines_in.Count)
            {
                // 1. Compare each pt of the line & record the pt with the closest distance & the distance
                int currLinePtIter = 0;
                int nextLinePtIter = 0;

                // We are going to check every pt in the current line with every pt in the next line & record 
                // the smallest gap. By checking every pt ( rather than it's corresponding pt ) we should hopefully
                // cope with breathy lines in a more robust way.

                // Best case gap size
                long gapDistance = (long)Math.Sqrt(Math.Pow((double)map_ramp_line_length, 2) + Math.Pow((double)steps_count, 2));
                long currGapRow, currGapCol, nextGapRow, nextGapCol;

                while (currLinePtIter != (lines_in[currLineInIter])._points.Count)
                {
                    while (nextLinePtIter != (lines_in[nextLineInIter])._points.Count)
                    {
                        long aLength = (long)Math.Abs((int)((lines_in[currLineInIter]._points[currLinePtIter]).col - (lines_in[nextLineInIter]._points[nextLinePtIter]).col));
                        long bLength = (long)Math.Abs((int)((lines_in[currLineInIter]._points[currLinePtIter]).row - (lines_in[nextLineInIter]._points[nextLinePtIter]).row));

                        long currGapDist = (long) Math.Sqrt( Math.Pow((double)aLength, 2) + Math.Pow((double)bLength, 2) );
                        // Looking for the smallest gap
                        if (currGapDist < gapDistance)
                        {
                            gapDistance = currGapDist;

                            // These are used for debug so can be removed if desired. 
                            // We are only interested in how close the two lines are.
                            currGapRow = (lines_in[currLineInIter]._points[currLinePtIter]).row;
                            currGapCol = (lines_in[currLineInIter]._points[currLinePtIter]).col;
                            nextGapRow = (lines_in[nextLineInIter]._points[nextLinePtIter]).row;
                            nextGapCol = (lines_in[nextLineInIter]._points[nextLinePtIter]).col;
                        }
                        // Check the next pt on the adjacent line
                        nextLinePtIter++;
                    }
                    // Check the next pt on the current line
                    nextLinePtIter = 0;
                    currLinePtIter++;
                }
                if(gapDistance <= minLmWidth)
                {
                    // The lines are too close, so remove the shorter of the two.
                    // reusing existing iterators
                    currLinePtIter = 0;
                    nextLinePtIter = (lines_in[currLineInIter]._points).Count;
                    nextLinePtIter--;
                    
                    
                    long ptA = ((lines_in[currLineInIter]._points)[currLinePtIter]).col;
                    long ptB = ((lines_in[currLineInIter]._points)[nextLinePtIter]).col;
                    
                    // Simple check for length
                    long currLineLength = (long) Math.Abs( ptA - ptB );

                    
					currLinePtIter = 0;
                    nextLinePtIter = lines_in[nextLineInIter]._points.Count;
                    nextLinePtIter--;
                    
                    ptA = (lines_in[nextLineInIter]._points[currLinePtIter]).col;
                    ptB = (lines_in[nextLineInIter]._points[nextLinePtIter]).col;

                    // Simple check for length
                    long nextLineLength = (long) Math.Abs( ptA - ptB );

                    if (nextLineLength > currLineLength)
                        lines_in.RemoveAt(currLineInIter); // remove this as the next line is longer.
                    else
                        lines_in.RemoveAt(nextLineInIter);	// remove this as the current line is the same length or longer.

                    // reset the iterators after the erase() and start again. Safer to assume that
                    // there may be more than two lines together.
                    currLineInIter = 0;
                    nextLineInIter = 0;
                    nextLineInIter++;
                }
                else
                {
                    // Now check the next two lines
                    currLineInIter++;
                    nextLineInIter++;
                }
            }
             
        }
        private CDSDBRSupermodeMapLine.Point_Type convertRowCol2LinePoint(int row,int col)
        {
            CDSDBRSupermodeMapLine.Point_Type point_on_map = new CDSDBRSupermodeMapLine.Point_Type();

            point_on_map.row = row;
            point_on_map.col = col;
            point_on_map.real_row = (double)row;
            point_on_map.real_col = (double)col;

            point_on_map.front_pair_number = _filtered_middle_line._points[row].front_pair_number;
            point_on_map.I_rear = _filtered_middle_line._points[row].rear;
            point_on_map.constantCurrent = _filtered_middle_line._points[row].constantCurrent;
            point_on_map.nonConstantCurrent = _filtered_middle_line._points[row].nonConstantCurrent;
            
            point_on_map.I_phase = _phase_current._currents[col];
            return point_on_map;
        }        
        public CDSDBRSupermodeMapLine.Point_Type interpolateLinePoint(double row, double col)
        {
            CDSDBRSupermodeMapLine.Point_Type point_on_map = new CDSDBRSupermodeMapLine.Point_Type();

            point_on_map.real_row = row;
            point_on_map.real_col = col;

            // find nearest row
            point_on_map.row = (long)row;
            if (row - (double)((long)row) > 0.5) point_on_map.row++;

            // find nearest col
            point_on_map.col = (long)col;
            if (col - (double)((long)col) > 0.5) point_on_map.col++;

            if (col > (double)((long)col))
            {
                // interpolate Ip linearly
                double Ip_a = _phase_current._currents[(int)col];
                double Ip_b = _phase_current._currents[((int)col) + 1];

                double Ip_fraction = col - (double)((int)col);
                point_on_map.I_phase = Ip_a + Ip_fraction * (Ip_b - Ip_a);
            }
            else
            {
                // no need to interpolate
                point_on_map.I_phase = _phase_current._currents[(int)col];
            }

            double If_fraction = row - (double)(point_on_map.row);
            long top_row = _row - 1;

            int index_of_nearest = (int)point_on_map.row;
            if (row > (double)((long)row))
            {
                //yes, the row in is a double value not an int value
                //check to see whether we need to interplot or not
                //because we can NOT interplot between different fs section rears
                if(( index_of_nearest == (short)top_row && If_fraction < 0 )
                    ||(index_of_nearest==0 && If_fraction >0))
                {
                    //no enough points to interplot.
                    If_fraction = 0;
                }
                else
                {
                    //front section switch
                    if (index_of_nearest == (long)row)
                    {
                        if (_filtered_middle_line._points[index_of_nearest].front_pair_number
                       != _filtered_middle_line._points[index_of_nearest + 1].front_pair_number)
                        {
                            If_fraction = 0;
                        }
                    }
                    else
                    {
                        if (_filtered_middle_line._points[index_of_nearest-1].front_pair_number
                       != _filtered_middle_line._points[index_of_nearest ].front_pair_number)
                        {
                            If_fraction = 0;
                        }
                    }
                   
                }
            }

            double If_value;
            if (If_fraction != 0)
            {
                //need to interplot
                int index_of_nextpt;
                if (If_fraction > 0)
                {
                    index_of_nextpt = index_of_nearest + 1;
                    //Ir_fraction = If_fraction;
                }
                else
                {
                    index_of_nextpt = index_of_nearest - 1;
                    If_fraction = -If_fraction; // ensure positive fraction
                }

                // interpolate If linearly
                double If_a = _filtered_middle_line._points[index_of_nearest].nonConstantCurrent;
                double If_b = _filtered_middle_line._points[index_of_nextpt].nonConstantCurrent;

                // interpolate Ir linearly
                double Ir_a = _filtered_middle_line._points[index_of_nearest].rear;
                double Ir_b = _filtered_middle_line._points[index_of_nextpt].rear;
                If_value = If_a + If_fraction * (If_b - If_a);

                point_on_map.I_rear = Ir_a + If_fraction * (Ir_b - Ir_a);

            }
            else
            {
                // no need to interpolate
                If_value = _filtered_middle_line._points[index_of_nearest].nonConstantCurrent;

                point_on_map.I_rear = _filtered_middle_line._points[index_of_nearest].rear;

                point_on_map.real_row = (double)((long)row);
            }

            point_on_map.front_pair_number = _filtered_middle_line._points[index_of_nearest].front_pair_number;
            point_on_map.constantCurrent = _filtered_middle_line._points[index_of_nearest].constantCurrent;
            point_on_map.nonConstantCurrent = If_value;

            return point_on_map;
        }
         
        public void checkForwardAndReverseLines()
        {
            uint ramp_length;
            uint steps_count;

            //if( _Im_ramp ) // middle line ramped
            //{
            //    steps_count =(ulong)(_phase_current._currents.Count);
            //    ramp_length =(ulong)(_filtered_middle_line._points.Count);
            //}
            //else // phase ramped
            //{
		        steps_count =(uint)(_filtered_middle_line._points.Count);
		        ramp_length =(uint)(_phase_current._currents.Count);
            //}

            int not_a_value = -100;

	        int prev_forward_line = not_a_value;
            int prev_forward_bottom_rhs_first_pos = not_a_value;
            int prev_forward_lhs_top_last_pos = not_a_value;

            int prev_reverse_line = not_a_value;
            int prev_reverse_bottom_rhs_first_pos = not_a_value;
            int prev_reverse_lhs_top_last_pos = not_a_value;

            int cl = 0 ;
	        while( cl < (_lm_lower_lines.Count + _lm_upper_lines.Count) )
            { // iterate through lines

                //ModeBoundaryLine* mbl = results.getLine( cl );

		        CDSDBRSupermodeMapLine i_lm_boundary_line;
		        bool forward_lower = true;
		        int line_num = 0;

		        if( cl < _lm_lower_lines.Count)
		        {
			       
                    line_num = cl;
                    i_lm_boundary_line = _lm_lower_lines[line_num];
			        forward_lower = true;
		        }
		        else
		        {
			        
                    line_num = cl - _lm_lower_lines.Count;
                    i_lm_boundary_line = _lm_upper_lines[line_num];
			        forward_lower = false;
		        }

                if( forward_lower )
                {
                    // find y and x values of first point in line
			        int first_y = (int)i_lm_boundary_line._points[0].row;
			        int first_x = (int)i_lm_boundary_line._points[0].col;

                    int bottom_rhs_first_pos;
                    if( first_y == 0 ) bottom_rhs_first_pos = ((int)ramp_length-1)-first_x;
                    else bottom_rhs_first_pos = first_y+(int)ramp_length;

                    // find y and x values of last point in line
			        int index_last = i_lm_boundary_line._points.Count - 1;
                    int last_y = (int)i_lm_boundary_line._points[index_last].row;
			        int last_x = (int)i_lm_boundary_line._points[index_last].col;
                    int lhs_top_last_pos;
                    if( last_x == ramp_length-1) lhs_top_last_pos = last_y;
                    else lhs_top_last_pos = last_y + ((int)ramp_length-1) - last_x;


			        if( prev_forward_line != not_a_value && line_num > 0 && _lm_lower_lines.Count > 1 &&
                      ( bottom_rhs_first_pos == prev_forward_bottom_rhs_first_pos
                     || lhs_top_last_pos == prev_forward_lhs_top_last_pos
                     || (bottom_rhs_first_pos-prev_forward_bottom_rhs_first_pos)*(lhs_top_last_pos-prev_forward_lhs_top_last_pos) < 0
                     || Convert.ToInt32(this.superMapSitting["MinNumberOfPointsWidthOfALongitudinalMode"]) > Math.Abs(bottom_rhs_first_pos - prev_forward_bottom_rhs_first_pos)
                     || Convert.ToInt32(this.superMapSitting["MinNumberOfPointsWidthOfALongitudinalMode"]) > Math.Abs(lhs_top_last_pos - prev_forward_lhs_top_last_pos)))
                    {
                        CDSDBRSupermodeMapLine i_prev_lm_boundary_line = _lm_lower_lines[cl-1];
				        _lm_lower_lines.Remove(i_lm_boundary_line);
				        _lm_lower_lines.Remove(i_prev_lm_boundary_line);
                        prev_forward_line = not_a_value;
				        prev_forward_bottom_rhs_first_pos = not_a_value;
				        prev_forward_lhs_top_last_pos = not_a_value;
                        cl=0;
                    }
                    else
                    {
                        prev_forward_line = line_num;
                        prev_forward_bottom_rhs_first_pos = bottom_rhs_first_pos;
                        prev_forward_lhs_top_last_pos = lhs_top_last_pos;
                        cl++;
                    }

                }
                else // reverse_upper
                {

                    //// find y and x values of first point in line
                    //std::vector<ulong>::iterator i_first_point = mbl->getX()->getIndices()->begin();
                    //int first_y = (*i_first_point)/ramp_length;
                    //int first_x = (*i_first_point) - first_y*ramp_length;
			        int first_y = (int)i_lm_boundary_line._points[0].row;
			        int first_x = (int)i_lm_boundary_line._points[0].col;
                    //first_x = (ramp_length-1)-first_x;

                    int bottom_rhs_first_pos;
                    if( first_y == 0 ) bottom_rhs_first_pos = ((int)ramp_length-1)-first_x;
                    else bottom_rhs_first_pos = first_y+(int)ramp_length;


                    //std::vector<ulong>::iterator i_last_point = mbl->getX()->getIndices()->end();
                    //i_last_point--;
                    //// find y and x values of last point in line
                    //int last_y = (*i_last_point)/ramp_length;
                    //int last_x = (*i_last_point) - last_y*ramp_length;
			        int index_last = i_lm_boundary_line._points.Count - 1;
                    int last_y = (int)i_lm_boundary_line._points[index_last].row;
			        int last_x = (int)i_lm_boundary_line._points[index_last].col;
                    //last_x = (ramp_length-1)-last_x;

                    int lhs_top_last_pos;
                    if( last_x == ramp_length-1 ) lhs_top_last_pos = last_y;
                    else lhs_top_last_pos = last_y + ((int)ramp_length-1)-last_x;


                    if( prev_reverse_line != not_a_value && line_num > 0 && _lm_upper_lines.Count > 1 &&
                      ( bottom_rhs_first_pos == prev_reverse_bottom_rhs_first_pos
                     || lhs_top_last_pos == prev_reverse_lhs_top_last_pos
                     || (bottom_rhs_first_pos-prev_reverse_bottom_rhs_first_pos)*(lhs_top_last_pos-prev_reverse_lhs_top_last_pos) < 0
                     || Convert.ToInt32(this.superMapSitting["MinNumberOfPointsWidthOfALongitudinalMode"]) > Math.Abs(bottom_rhs_first_pos - prev_reverse_bottom_rhs_first_pos)
                     || Convert.ToInt32(this.superMapSitting["MinNumberOfPointsWidthOfALongitudinalMode"]) > Math.Abs(lhs_top_last_pos - prev_reverse_lhs_top_last_pos)))
                    { 
                        // lines cross each other => remove both
                        //results.deleteLine(cl);
                        //results._number_of_lines_removed++;
                        //results.deleteLine(prev_reverse_line);
                        //results._number_of_lines_removed++;
                        CDSDBRSupermodeMapLine i_prev_lm_boundary_line = _lm_upper_lines[line_num-1];
				       
				        _lm_upper_lines.Remove(i_lm_boundary_line);
				        _lm_upper_lines.Remove(i_prev_lm_boundary_line);
                        prev_reverse_line = not_a_value;
				        prev_reverse_bottom_rhs_first_pos = not_a_value;
				        prev_reverse_lhs_top_last_pos = not_a_value;
                        cl=0;
                    }
                    else
                    {
                        prev_reverse_line = line_num;
                        prev_reverse_bottom_rhs_first_pos = bottom_rhs_first_pos;
                        prev_reverse_lhs_top_last_pos = lhs_top_last_pos;
                        cl++;
                    }
                }

            }

           
        }


        void associateForwardAndReverseLines()
        {
            uint ramp_length;
            uint steps_count;

            //if( _Im_ramp ) // middle line ramped
            //{
            //    steps_count =(uint)_phase_current._currents.Count;
            //    ramp_length =(uint)_filtered_middle_line._points.Count;
            //}
            //else // phase ramped
            //{
            steps_count = (uint)(_filtered_middle_line._points.Count);
            ramp_length = (uint)(_phase_current._currents.Count);
            //}

            // match up the lines on both forward and reverse maps

            // store first and last point in each line in these maps
            List<Pair<uint, uint>> forward_firstpoints = new List<Pair<uint, uint>>();
            List<Pair<uint, uint>> forward_lastpoints = new List<Pair<uint, uint>>();
            List<Pair<uint, uint>> reverse_firstpoints = new List<Pair<uint, uint>>();
            List<Pair<uint, uint>> reverse_lastpoints = new List<Pair<uint, uint>>();

            for (uint cl = 0; cl < _lm_lower_lines.Count + _lm_upper_lines.Count; cl++)
            { // iterate through lines
                //ModeBoundaryLine* mbl = results.getLine( cl );
                CDSDBRSupermodeMapLine i_lm_boundary_line;
                bool forward_lower = true;
                uint line_num = 0;

                if (cl < _lm_lower_lines.Count)
                {
                    line_num = cl;
                    i_lm_boundary_line = _lm_lower_lines[(int)line_num];
                    forward_lower = true;
                }
                else
                {
                    line_num = (cl - (uint)_lm_lower_lines.Count);
                    i_lm_boundary_line = _lm_upper_lines[(int)line_num];
                    forward_lower = false;
                }

                if (forward_lower)
                {
                    int row_first_pt = (int)i_lm_boundary_line._points[0].row;
                    int col_first_pt = (int)i_lm_boundary_line._points[0].col;
                    uint index_first_pt = (uint)col_first_pt + ((uint)row_first_pt) * (uint)ramp_length;

                    forward_firstpoints.Add(new Pair<uint, uint>(index_first_pt, cl));

                    CDSDBRSupermodeMapLine.Point_Type i_last_pt = i_lm_boundary_line._points[i_lm_boundary_line._points.Count - 1];

                    int row_last_pt = (int)i_last_pt.row;
                    int col_last_pt = (int)i_last_pt.col;
                    uint index_last_pt = (uint)col_last_pt + ((uint)row_last_pt) * (uint)ramp_length;

                    forward_lastpoints.Add(new Pair<uint, uint>(index_last_pt, cl));
                }
                else // reverse_upper
                {
                    int row_first_pt = (int)i_lm_boundary_line._points[0].row;
                    int col_first_pt = (int)i_lm_boundary_line._points[0].col;
                    uint index_first_pt = (uint)col_first_pt + (uint)row_first_pt * (uint)ramp_length;

                    reverse_firstpoints.Add(new Pair<uint, uint>(index_first_pt, cl));

                    CDSDBRSupermodeMapLine.Point_Type i_last_pt = i_lm_boundary_line._points[i_lm_boundary_line._points.Count - 1];
                    int row_last_pt = (int)i_last_pt.row;
                    int col_last_pt = (int)i_last_pt.col;
                    uint index_last_pt = (uint)col_last_pt + (uint)row_last_pt * (uint)ramp_length;

                    reverse_lastpoints.Add(new Pair<uint, uint>(index_last_pt, cl));
                }

            }

            // store rhs and lhs points in the mode below each line in these maps
            List<Pair<uint, uint>> forward_rhs_points = new List<Pair<uint, uint>>();
            List<Pair<uint, uint>> forward_lhs_points = new List<Pair<uint, uint>>();
            List<Pair<uint, uint>> reverse_rhs_points = new List<Pair<uint, uint>>();
            List<Pair<uint, uint>> reverse_lhs_points = new List<Pair<uint, uint>>();

            // find points between forward lines' last points along RHS
            uint prev_y = 0;             // start at bottom-right corner
            uint prev_x = ramp_length - 1; // start at bottom-right corner

            for (int i_flp = 0; i_flp < forward_lastpoints.Count; i_flp++)
            {
                uint last_point = (uint)(forward_lastpoints[i_flp].First);
                uint line_number = (uint)(forward_lastpoints[i_flp].Second);

                uint y = (uint)(last_point / ramp_length);
                uint x = last_point - y * (uint)ramp_length;

                if (x == ramp_length - 1)
                { // last point is on RHS

                    uint prev_last_point = ramp_length - 1; // start at bottom-right corner
                    for (int i = (int)(last_point - ramp_length); i > 0; i--)
                    { // find last point of next line down

                        int fi =forward_lastpoints.FindIndex(delegate(Pair<uint, uint> obj)
                            {
                                if (Math.Abs(obj.First - i) < 1e-6)
                                {
                                    return true;
                                }
                                else return false;
                            });
                        
                        if (fi != -1)
                        { // found last point of next line down
                            prev_last_point = forward_lastpoints[fi].First;
                            break;
                        }
                    }
                    uint prev_y1 = (uint)(prev_last_point / ramp_length);

                    for (uint mode_point_y = prev_y1 + 1; mode_point_y <= y; mode_point_y++)
                    { // iterate through points from previous line to current line on RHS
                        uint mode_point =
                            (ramp_length - 1) + mode_point_y * ramp_length;
                        forward_rhs_points.Add(new Pair<uint, uint>(mode_point, line_number));
                    }
                }
            }

            // find points between forward lines' first points along LHS
            prev_y = 0; // start at bottom-left corner
            prev_x = 0; // start at bottom-left corner
          
            for (int i_ffp = 0; i_ffp < forward_firstpoints.Count; i_ffp++)
            {
                uint first_point = forward_firstpoints[i_ffp].First;
                uint line_number = forward_firstpoints[i_ffp].Second;
                //printf("\n <%d,%d>",first_point,line_number);

                uint y = (uint)(first_point / ramp_length);
                uint x = first_point - y * ramp_length;
                
                if (x == 0)
                { // first point is on LHS

                    uint prev_first_point = 0; // start at bottom-left corner
                    for (int i = (int)(first_point - ramp_length); i > 0; i--)
                    { // find first point of next line down
                        int fi = forward_firstpoints.FindIndex(delegate(Pair<uint, uint> obj)
                            {
                                if (Math.Abs(obj.First - i) < 1e-6)
                                {
                                    return true;
                                }
                                else return false;
                            });
                        if (fi != -1)
                        { // found first point of next line down
                            //printf("\n found prev_first_point = %d", (*fi).first);
                            prev_first_point = forward_firstpoints[fi].First;
                            break;
                        }
                    }
                    uint prev_y1 = (uint)(prev_first_point / ramp_length);

                    //printf("\nforward_lhs %d ",line_number);

                    for (uint mode_point_y = prev_y1 + 1; mode_point_y <= y; mode_point_y++)
                    { // iterate through points from previous line to current line on LHS
                        uint mode_point = mode_point_y * ramp_length;
                        forward_lhs_points.Add(new Pair<uint, uint>(mode_point, line_number));
                        //printf(" <%d,%d>",mode_point,line_number);
                    }
                }
            }

            // find points between reverse lines' last points along RHS
            prev_y = 0;             // start at bottom-right corner
            prev_x = ramp_length - 1; // start at bottom-right corner

            for (int i_rlp = 0; i_rlp < reverse_lastpoints.Count; i_rlp++)
            {
                uint last_point = reverse_lastpoints[i_rlp].First;
                uint line_number = reverse_lastpoints[i_rlp].Second;

                uint y = (uint)(last_point / ramp_length);
                uint x = last_point - y * ramp_length;

                if (x == ramp_length - 1) //0 )
                { // last point is on RHS

                    uint prev_last_point = 0; // start at bottom-right corner
                    for (int i = (int)(last_point - ramp_length); i > 0; i--)
                    { // find last point of next line down
                        int fi = reverse_lastpoints.FindIndex(delegate(Pair<uint, uint> obj)
                            {
                                if (Math.Abs(obj.First - i) < 1e-6)
                                {
                                    return true;
                                }
                                else return false;
                            });
                        if (fi != -1)
                        { // found last point of next line down
                            //printf("\n found prev_last_point = %d", (*fi).first);
                            prev_last_point = (reverse_lastpoints[fi]).First;
                            break;
                        }
                    }
                    uint prev_y1 = (uint)(prev_last_point / ramp_length);

                    //printf("\nreverse_rhs %d ",line_number);

                    for (uint mode_point_y = prev_y1 + 1; mode_point_y <= y; mode_point_y++)
                    { // iterate through points from previous line to current line on RHS
                        // also, convert from reverse indexing to forward indexing
                        uint mode_point =
                            (ramp_length - 1) + mode_point_y * ramp_length;
                        reverse_rhs_points.Add(
                            new Pair<uint, uint>(mode_point, line_number));
                    }
                }
            }


            // find points between reverse lines' first points along LHS
            prev_y = 0; // start at bottom-left corner
            prev_x = 0; // start at bottom-left corner

            for (int i_rfp = 0;i_rfp < reverse_firstpoints.Count; i_rfp++)              
            {
                uint first_point = reverse_firstpoints[i_rfp].First;
                uint line_number = reverse_firstpoints[i_rfp].Second;
                //printf("\n <%d,%d>",first_point,line_number);

                uint y = (uint)(first_point / ramp_length);
                uint x = first_point - y * ramp_length;

                if (x == 0) //ramp_length-1 )
                { // first point is on LHS

                    uint prev_first_point = ramp_length - 1; // start at bottom-left corner
                    for (int i = (int)(first_point - ramp_length); i > 0; i--)
                    { // find first point of next line down
                        int fi = reverse_firstpoints.FindIndex(delegate(Pair<uint, uint> obj)
                            {
                                if (Math.Abs(obj.First - i) < 1e-6)
                                {
                                    return true;
                                }
                                else return false;
                            });
                        if (fi != -1)
                        { // found first point of next line down
                            //printf("\n found prev_first_point = %d", (*fi).first);
                            prev_first_point = reverse_firstpoints[fi].First;
                            break;
                        }
                    }
                    uint prev_y1 = (uint)(prev_first_point / ramp_length);
                    for (uint mode_point_y = prev_y1 + 1; mode_point_y <= y; mode_point_y++)
                    { // iterate through points from previous line to current line on LHS
                        // also, convert from reverse indexing to forward indexing
                        uint mode_point = mode_point_y * ramp_length;
                        reverse_lhs_points.Add(new Pair<uint, uint>(mode_point, line_number));

                        //printf(" <%d,%d>",mode_point,line_number);
                    }
                }
            }

            // iterate through the forward mode lines
            // for each line, find the points on the forward LHS and RHS
            // then for each of these points, find their line numbers
            // on the reverse LHS and RHS
            // find the lines with the most points in common
            // if it's more than a percentage calculated below, consider the modes matched

            //for(unsigned long cl = 0 ; cl < results.countLines() ; cl++ )
            //{ // iterate through forward lines only
            //    if( results.getLine( cl )->getLineType() & MBL_FORWARD )
            //    {

            for (uint cl = 0; cl < (uint)(_lm_lower_lines.Count + _lm_upper_lines.Count); cl++)
            { // iterate through lines
                CDSDBRSupermodeMapLine i_lm_boundary_line;
                bool forward_lower = true;
                uint line_num = 0;

                if (cl < (ulong)(_lm_lower_lines.Count))
                {
                    line_num = cl;
                    i_lm_boundary_line = _lm_lower_lines[(int)line_num];
                    forward_lower = true;
                }
                else
                {
                    line_num = (cl - (uint)(_lm_lower_lines.Count));
                    i_lm_boundary_line = _lm_upper_lines[(int)line_num];
                    forward_lower = false;
                }

                if (forward_lower)
                {
                    // build up a vector of containing the reverse line numbers for the same mode points
                    List<uint> reverse_line_numbers = new List<uint>();
                    // find reverse line numbers for the same mode points on RHS
                    //printf("\n find reverse line numbers for the same mode points on RHS");
                    for (int i_frhs = 0; i_frhs < forward_rhs_points.Count; i_frhs++)
                    {
                        if (forward_rhs_points[i_frhs].Second == cl)
                        { // found point associated with this line
                            // find the same point's line number on the reverse map
                            int i_rrhs;
                            for (i_rrhs = 0; i_rrhs < reverse_rhs_points.Count; i_rrhs++)
                            {
                                if (reverse_rhs_points[i_rrhs].First == forward_rhs_points[i_frhs].First)
                                    break;
                            }

                            if (i_rrhs < reverse_rhs_points.Count)
                            { // found the same point in reverse RHS
                                reverse_line_numbers.Add(reverse_rhs_points[i_rrhs].Second);
                                //printf(" %d", (*i_rrhs).second );
                            }
                        }
                    }

                    // find reverse line numbers for the same mode points on LHS
                    //printf("\n find reverse line numbers for the same mode points on LHS");
                    for (int i_flhs = 0;i_flhs < forward_lhs_points.Count; i_flhs++)
                    {
                        if (forward_lhs_points[i_flhs].Second == cl)
                        { // found point associated with this line
                            // find the same point's line number on the reverse map
                            int i_rlhs;
                            for (i_rlhs = 0; i_rlhs < reverse_lhs_points.Count; i_rlhs++)
                            {
                                if (reverse_lhs_points[i_rlhs].First == forward_lhs_points[i_flhs].First)
                                    break;
                            }
                            if (i_rlhs < reverse_lhs_points.Count)
                            { // found the same point in reverse LHS
                                reverse_line_numbers.Add(reverse_lhs_points[i_rlhs].Second);
                                //printf(" %d", (*i_rlhs).second );
                            }
                        }
                    }

                    // find the most commonly occuring reverse line number 
                    uint most_common_number = 0;
                    uint most_common_number_count = 0;
                    List<Pair<uint, uint>> linenum_count = new List<Pair<uint, uint>>();
                    linenum_count.Clear();
                    for (uint rln = 0; rln < (uint)(_lm_lower_lines.Count + _lm_upper_lines.Count); rln++)
                    {
                        // count the occurances of rln
                        uint rln_count = 0;
                        for (uint i_rln = 0; i_rln < reverse_line_numbers.Count; i_rln++)
                            if (reverse_line_numbers[(int)i_rln] == rln) rln_count++;
                        if (rln_count > 0)
                            linenum_count.Add(new Pair<uint, uint>(rln, rln_count));
                        if (rln_count > most_common_number_count)
                        { // rln is the most common number found so far
                            most_common_number = rln;
                            most_common_number_count = rln_count;
                        }
                        // find all line number and its count
                    }
                    if (reverse_line_numbers.Count > 1 &&
                        100 * most_common_number_count / ((uint)(reverse_line_numbers.Count)) >= 50)
                    { // 50% of points matched => treat the lines as matched

                        // check this has been matched already, if so, ignore it
                        int i_m;
                        bool isMatched = false;
                        int matchedLineNumIndex = 0;
                        for (i_m = 0; i_m < _matched_line_numbers.Count; i_m++)
                        {
                            if (_matched_line_numbers[i_m].Second == most_common_number)
                            {
                                isMatched = true;
                                matchedLineNumIndex = i_m;
                            }
                        }

                        if (isMatched && _lm_lower_lines.Count != _lm_upper_lines.Count)
                        {
                            // Check the matched line
                            ulong lastMatchedLine = _matched_line_numbers[matchedLineNumIndex].First;

                            uint used_most_common_number = most_common_number;
                            used_most_common_number = used_most_common_number - (uint)(_lm_lower_lines.Count);
                            double disBetMatchedLowerCurUpperAvg = 0;
                            disBetMatchedLowerCurUpperAvg = ComputeUpperLowerLineDistance(_lm_lower_lines[(int)lastMatchedLine], _lm_upper_lines[(int)used_most_common_number]);

                            double disBetCurLowerandUpperAvg = 0;
                            disBetCurLowerandUpperAvg = ComputeUpperLowerLineDistance(i_lm_boundary_line, _lm_upper_lines[(int)used_most_common_number]);

                            if (disBetCurLowerandUpperAvg < disBetMatchedLowerCurUpperAvg)//purny xie Remove nonmatch line but if can define by constant (say < 2 or 3) for poorest map?
                            {
                                _matched_line_numbers[matchedLineNumIndex].First = line_num; 
                            }

                        }
                        if (!isMatched)
                        { // the line found ahs not already been matched to another line
                            // add code by purney.xie
                            //correct the match line before becoming a matched line
                            // first, erase line number matched.
                            for (int iter = 0; iter < linenum_count.Count; iter++)
                            {
                                for (int iter_matched = 0; iter_matched < _matched_line_numbers.Count; iter_matched++)
                                {
                                    if ((linenum_count[iter]).First == (_matched_line_numbers[iter_matched]).Second)
                                    {
                                        linenum_count.RemoveAt(iter);
                                        iter = 0;
                                    }
                                }
                            }
                            if (_lm_lower_lines.Count != _lm_upper_lines.Count && linenum_count.Count >= 2)
                            {
                                // maybe match error in this case, so we correct it
                                uint secondMaxNum = 0;
                                uint secondLine = 0;
                                // remove the max num
                                int maxiter = linenum_count.FindIndex(delegate(Pair<uint, uint> obj)
                                                {
                                                    if (obj.First == most_common_number && obj.Second == most_common_number_count )
                                                    {
                                                        return true;
                                                    }
                                                    else return false;
                                                });
                                if (maxiter != -1)
                                {
                                    linenum_count.RemoveAt(maxiter);
                                }
                                else throw new Exception("not found the max numble line");
                                // find the second max num
                                if (linenum_count.Count > 0) // exist the second max num
                                {
                                    for (int iter = 0; iter < linenum_count.Count; iter++)
                                    {
                                        if ((linenum_count[iter]).Second > secondMaxNum)
                                        {
                                            secondMaxNum = (linenum_count[iter]).Second;
                                            secondLine = (linenum_count[iter]).First;
                                        }
                                    }
                                    uint saveSecondline = secondLine;

                                    // current lower line compare with the first max number upper line
                                    CDSDBRSupermodeMapLine first_lm_upper;  // first_lm_upper is fist max lm boundary line on upper line
                                    uint used_most_common_number = most_common_number;
                                    used_most_common_number = used_most_common_number - (uint)(_lm_lower_lines.Count);
                                    first_lm_upper = _lm_upper_lines[(int)used_most_common_number];

                                    double distanceVecFCAvg = 0;
                                    if (used_most_common_number < (uint)(_lm_lower_lines.Count + _lm_upper_lines.Count))
                                    {
                                       distanceVecFCAvg = ComputeUpperLowerLineDistance(first_lm_upper, i_lm_boundary_line);
                                    }

                                    // current lower line compare with the second max number upper line
                                    CDSDBRSupermodeMapLine second_lm_upper;
                                    double distanceVecSCAvg = 0;
                                    if (secondLine < (ulong)(_lm_lower_lines.Count + _lm_upper_lines.Count))
                                    {
                                        secondLine = secondLine - (uint)(_lm_lower_lines.Count);
                                        second_lm_upper = _lm_upper_lines[(int)secondLine];

                                        distanceVecSCAvg = ComputeUpperLowerLineDistance(second_lm_upper, i_lm_boundary_line);
                                    }
                                    // get the average distance between tow lines

                                    if (distanceVecSCAvg < distanceVecFCAvg) most_common_number = saveSecondline;
                                }//if(linenum_count.size() > 0)
                            }//if(_lm_lower_lines.size() != _lm_upper_lines.size() )
                            // end add coude by purney.xie
                            _matched_line_numbers.Add(new Pair<ulong, ulong>(cl, most_common_number));
                        }
                    }

                }
            }
        }

        /// <summary>
        /// Compute distance between a upper line and a lower line when upper lines are not equal to lower liens.
        /// Add by purney.xie  1/18/2010
        /// </summary>
        /// <param name="i_lm_boundary_line">i lm boundary </param>
        /// <param name="current_lm_lower"> current lm boundary</param>
        /// <param name="distanceVecFC">distance between two lines</param>
        private double ComputeUpperLowerLineDistance(CDSDBRSupermodeMapLine i_lm_boundary_line,  CDSDBRSupermodeMapLine current_lm_lower)
        {
            double retValue = 0;
            CDSDBRSupermodeMapLine first_lm_upper = i_lm_boundary_line;
            long col_first_pt_on_current_lm_lower = current_lm_lower._points[0].col;
            CDSDBRSupermodeMapLine.Point_Type lastpoint_lm_lower = current_lm_lower._points[current_lm_lower._points.Count - 1];
            long col_last_pt_on_current_lm_lower = lastpoint_lm_lower.col;

            List<long>  distanceVecFC = new List<long>();

            long col_first_pt_on_first_lm_upper = first_lm_upper._points[0].col;
            CDSDBRSupermodeMapLine.Point_Type last_pt_on_first_lm_upper = first_lm_upper._points[first_lm_upper._points.Count - 1];
            long col_last_pt_on_first_lm_upper = last_pt_on_first_lm_upper.col;


            if (col_first_pt_on_current_lm_lower == col_first_pt_on_first_lm_upper)//compare from LHS,
            {
                distanceVecFC.Clear();
                for (int i = 0, j = 0; i < current_lm_lower._points.Count && j < first_lm_upper._points.Count; i++, j++)
                {
                    long rowDistance = Math.Abs(current_lm_lower._points[i].row - first_lm_upper._points[j].row);
                    distanceVecFC.Add(rowDistance);
                }
            }
            if (col_last_pt_on_current_lm_lower == col_last_pt_on_first_lm_upper)// compare from RHS
            {
                distanceVecFC.Clear();
                for (int i = current_lm_lower._points.Count - 1, j = first_lm_upper._points.Count - 1;
                    i >= 0 && j >= 0; i--, j--)
                {
                    long rowDistance = Math.Abs(current_lm_lower._points[i].row - first_lm_upper._points[j].row);
                    distanceVecFC.Add(rowDistance);
                }
            }
            long sum = 0;
            for (int i = 0; i < distanceVecFC.Count; i++)
                sum += distanceVecFC[i];
            retValue = (double)sum / distanceVecFC.Count;

            return retValue;

        }

        /// <summary>
        /// 
        /// </summary>
        void removeUnassociatedLines()
        {
            // erase lines from vectors that have
            // not successfully associate forward with matching reverse
            List<ulong> lower_line_numbers = new List<ulong>();
            List<ulong> upper_line_numbers = new List<ulong>();

            uint _lm_lower_lines_count = (uint)(_lm_lower_lines.Count);

            // find all line numbers to begin with

            for (ulong i = 0; i < _lm_lower_lines_count; i++)
            {
                lower_line_numbers.Add(i);
            }

            for (ulong i = 0; i < (ulong)(_lm_upper_lines.Count); i++)
            {
                upper_line_numbers.Add(i + _lm_lower_lines_count);
            }

            for (int i_f = 0; i_f < _matched_line_numbers.Count; i_f++)
            {
                // erase known matched line numbers from lists
                // of all forward and reverse line numbers

                int i_lower_m = lower_line_numbers.IndexOf((ulong)(_matched_line_numbers[i_f].First));

                int i_upper_m = upper_line_numbers.IndexOf(_matched_line_numbers[i_f].Second);

                if (i_lower_m < lower_line_numbers.Count)
                    lower_line_numbers.RemoveAt(i_lower_m);

                if (i_upper_m < upper_line_numbers.Count)
                    upper_line_numbers.RemoveAt(i_upper_m);
            }

            // Any line numbers remaining are unmatched -> erase them.
            // Erase in reverse order so indexing doesn't move things around.
            for (int i = lower_line_numbers.Count - 1; i >= 0; i--)
            {

                int num = (int)lower_line_numbers[i];
                CDSDBRSupermodeMapLine i_ll = _lm_lower_lines[num];
                i_ll.clear();
                _lm_lower_lines.Remove(i_ll);

                // need to move indexing in _matched_line_numbers accordingly
                List<Pair<ulong, ulong>> new_matched_line_numbers = new List<Pair<ulong, ulong>>();
                new_matched_line_numbers.Clear();
                for (int i_m = 0; i_m < _matched_line_numbers.Count; i_m++)
                {
                    if (_matched_line_numbers[i_m].First > lower_line_numbers[i])
                    {
                        new_matched_line_numbers.Add(
                            new Pair<ulong, ulong>(_matched_line_numbers[i_m].First - 1, _matched_line_numbers[i_m].Second));
                    }
                    else
                    {
                        new_matched_line_numbers.Add(_matched_line_numbers[i_m]);
                    }
                }
                _matched_line_numbers.Clear();
                for (int i_m = 0; i_m < new_matched_line_numbers.Count; i_m++)
                {
                    _matched_line_numbers.Add(new_matched_line_numbers[i_m]);
                }
            }

            for (int i = upper_line_numbers.Count - 1; i >= 0; i--)
            {
                int num = (int)(upper_line_numbers[i] - _lm_lower_lines_count);
                CDSDBRSupermodeMapLine i_ul = _lm_upper_lines[num];
                i_ul.clear();
                _lm_upper_lines.Remove(i_ul);

                // need to move indexing in _matched_line_numbers accordingly
                List<Pair<ulong, ulong>> new_matched_line_numbers = new List<Pair<ulong, ulong>>();
                new_matched_line_numbers.Clear();
                for (int i_m = 0; i_m < _matched_line_numbers.Count; i_m++)
                {
                    if (_matched_line_numbers[i_m].Second > upper_line_numbers[i])
                    {
                        new_matched_line_numbers.Add(
                            new Pair<ulong, ulong>(_matched_line_numbers[i_m].First, _matched_line_numbers[i_m].Second - 1));
                    }
                    else
                    {
                        new_matched_line_numbers.Add(_matched_line_numbers[i_m]);
                    }
                }
                _matched_line_numbers.Clear();
                for (int i_m = 0;
                    i_m < new_matched_line_numbers.Count; i_m++)
                {
                    _matched_line_numbers.Add(new_matched_line_numbers[i_m]);
                }
            }

        }


        public void checkLMMiddleLinesOfFreq()
        {
            int lm_count = _lm_middle_lines_of_frequency.Count;
            if (lm_count > 0)
            {
                for (int lm = lm_count - 1; lm >= 0; lm--)
                {
                    CDSDBRSupermodeMapLine i_middle = _lm_middle_lines_of_frequency[lm];
                    // check each lm middle line is within one longitudinal mode
                    for (int i = 0; i < i_middle._points.Count - 1; i++)
                    {
                        long rowA = 0;
                        long colA = 0;
                        long rowB = 0;
                        long colB = 0;

                        if (_Im_ramp)
                        {
                            rowA = i_middle._points[i].col;
                            colA = i_middle._points[i].row;
                            rowB = i_middle._points[i + 1].col;
                            colB = i_middle._points[i + 1].row;
                        }
                        else
                        {
                            rowA = i_middle._points[i].row;
                            colA = i_middle._points[i].col;
                            rowB = i_middle._points[i + 1].row;
                            colB = i_middle._points[i + 1].col;
                        }

                        double PrA_fwd = _map_forward_power_ratio_median_filtered.iPt(rowA, colA);
                        double PrB_fwd = _map_forward_power_ratio_median_filtered.iPt(rowB, colB);
                        double PrA_rev = _map_reverse_power_ratio_median_filtered.iPt(rowA, colA);
                        double PrB_rev = _map_reverse_power_ratio_median_filtered.iPt(rowB, colB);

                        double delta_Pr_fwd = Math.Abs(PrA_fwd - PrB_fwd);
                        double delta_Pr_rev = Math.Abs(PrA_rev - PrB_rev);

                        if (delta_Pr_fwd > Convert.ToDouble(this.superMapSitting["MaxdeltaPrinalongitudinalmode"])
                        || delta_Pr_rev > Convert.ToDouble(this.superMapSitting["MaxdeltaPrinalongitudinalmode"]))
                        {
                            // middle line is not in a single longitudinal mode
                            // clear it!
                            i_middle.clear();
                            _lm_middle_lines_of_frequency_removed++;
                            break;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// out put the line file(Middle,Upper,Lower)
        /// </summary>
        /// <param name="abs_filePath">file path</param>
        private void WriteLMBoundaryFile(string abs_filePath)
        {
            //add a comma here so the loop next can work
            string[] headers={"LM#,","LineType,","Row,","Col\n"};
            using(StreamWriter swriter = new StreamWriter(abs_filePath))
            {
                foreach (string head in headers)
                {
                    swriter.Write(head);
                }
                for (int i = 0; i < _lm_middle_lines.Count; i++)
                {
                    for (int j = 0; j < _lm_middle_lines[i]._points.Count; j++)
                    {
                        swriter.WriteLine(i.ToString() + "," + "FrequencyMiddle," + _lm_filtered_middle_lines[i]._points[j].row.ToString()
                            + "," + _lm_middle_lines[i]._points[j].col.ToString());
                    }
                }

                for (int i = 0; i < _lm_upper_lines.Count; i++)
                {
                    for (int j = 0; j < _lm_upper_lines[i]._points.Count; j++)
                    {
                        swriter.WriteLine(i.ToString() + "," + "Upper," + _lm_upper_lines[i]._points[j].row.ToString()
                            + "," + _lm_upper_lines[i]._points[j].col.ToString());
                    }
                }

                for (int i = 0; i < _lm_lower_lines.Count; i++)
                {
                    for (int j = 0; j < _lm_lower_lines[i]._points.Count; j++)
                    {
                        swriter.WriteLine(i.ToString() + "," + "Lower," + _lm_lower_lines[i]._points[j].row.ToString()
                            + "," + _lm_lower_lines[i]._points[j].col.ToString());
                    }
                }

                swriter.Flush();
                swriter.Close();
            }
        }
        /// <summary>
        /// Write middle lines of upper or lower lines to file
        /// </summary>
        /// <param name="prefix">prefix of file name</param>
        /// <param name="line_in">data to be stored</param>
        private void WriteLMMiddleLinesToFile(string prefix, List<CDSDBRSupermodeMapLine> line_in)
        {
            string abs_filepath = "";

            for (int i = 0; i < line_in.Count; i++)
            {
                string fileName = prefix + i.ToString() + USCORE + LASER_TYPE_ID_DSDBR01_CSTRING
                                    + USCORE + _laser_id + USCORE + _timestamp + USCORE + "SM" + _sm_number + CSV;
                abs_filepath = Path.Combine(_result_dir, fileName);
                if (File.Exists(abs_filepath))
                {
                    File.Delete(abs_filepath);
                }
                StreamWriter writer = new StreamWriter(abs_filepath);
                writer.WriteLine("Rear Current [mA],Front Pair Number,Constant Front Current [mA],Non-constant Front Current [mA],Phase Current [mA],Row Index,Column Index");
                for (int j = 0; j < line_in[i]._points.Count; j++)
                {
                    writer.WriteLine(line_in[i]._points[j].I_rear.ToString() + "," + line_in[i]._points[j].front_pair_number.ToString() + ","
                        + line_in[i]._points[j].constantCurrent.ToString() + "," + line_in[i]._points[j].nonConstantCurrent.ToString() + ","
                        + line_in[i]._points[j].I_phase.ToString() + "," + line_in[i]._points[j].row.ToString() + ","
                        + line_in[i]._points[j].col.ToString() + "," + i.ToString());


                }
                writer.Flush();
                writer.Close();
            }
        }

        /// <summary>
        /// Write all boundary lines to file
        /// </summary>
        /// <param name="abs_filepath">file name to store the line datas</param>
        /// <param name="line_in">data to be stored</param>
        private void WriteLMBoundaryFile(string abs_filepath,List<CDSDBRSupermodeMapLine> line_in)
        {
            StreamWriter writer = new StreamWriter(abs_filepath);

            for (int i = 0; i < line_in.Count; i++)
            {
                for (int j = 0; j < line_in[i]._points.Count; j++)
                {
                    writer.WriteLine(line_in[i]._points[j].col.ToString() + "," + line_in[i]._points[j].row.ToString()
                        +"," +i.ToString());
                }
            }

            writer.Flush();
            writer.Close();
        }

       
        /// <summary>
        /// Find middle lines of upper lines or lower lines. added by purney.xie 12/28/2009
        /// </summary>
        private void findUpperLowerMiddleLines(List<CDSDBRSupermodeMapLine> lines, out List<CDSDBRSupermodeMapLine> modeMiddle_lines)
        {
            // clear all middle lines,
            // middle lines of frequency
            List<CDSDBRSupermodeMapLine> middle_lines = new List<CDSDBRSupermodeMapLine>();
            for (int i = 0; i < (int)(middle_lines.Count); i++)
            {
                middle_lines[i].clear();
            }
            middle_lines.Clear();

            long count_lm_lines = (long)(lines.Count);
            if (count_lm_lines > 1)
            {
                for (int lm = 0; lm < count_lm_lines - 1; lm++)
                {
                    // iterate through the boundary lines of each longitudinal mode

                    int i_lower_line = 0;
                    i_lower_line += lm;

                    int i_upper_line = i_lower_line;
                    i_upper_line++;

                    CDSDBRSupermodeMapLine new_lm_middle_line = new CDSDBRSupermodeMapLine();
                    middle_lines.Add(new_lm_middle_line);

                    bool middle_point_found_at_col_zero = false;
                    bool middle_point_found_at_last_col = false;

                    // if right-most column covered is not last column in map
                    //   find midpoint on rhs-top border
                    //   plot line from right-most point found to midpoint
                    //   add points along line to middle line
                    // if left-most column covered is not first column in map
                    //   find midpoint on lhs_bottom border
                    //   plot line from left-most point found to lhs_bottom border
                    //   add points along line to middle line

                    long widths_total = 0;
                    long widths_count = 0;

                    for (int i = 0; i < _col; i++)
                    {
                        // for each column

                        int highest_row_at_i_on_lower_line = -1;
                        int index_of_highest_row_at_i_on_lower_line = -1;
                        int lowest_row_at_i_on_lower_line = (int)_row;
                        int index_of_lowest_row_at_i_on_lower_line = -1;

                        // find highest and lowest points on lower line at column i

                        for (int j = 0; j < (long)(lines[i_lower_line]._points.Count); j++)
                        {
                            if (lines[i_lower_line]._points[j].col == i
                            && lines[i_lower_line]._points[j].row > highest_row_at_i_on_lower_line)
                            {
                                index_of_highest_row_at_i_on_lower_line = j;
                                highest_row_at_i_on_lower_line = (int)lines[i_lower_line]._points[j].row;
                            }
                            if (lines[i_lower_line]._points[j].col == i
                            && lines[i_lower_line]._points[j].row < lowest_row_at_i_on_lower_line)
                            {
                                index_of_lowest_row_at_i_on_lower_line = j;
                                lowest_row_at_i_on_lower_line = (int)lines[i_lower_line]._points[j].row;
                            }
                        }

                        long highest_row_at_i_on_upper_line = -1;
                        long index_of_highest_row_at_i_on_upper_line = -1;
                        long lowest_row_at_i_on_upper_line = (int)_row;
                        long index_of_lowest_row_at_i_on_upper_line = -1;

                        // find highest and lowest point on upper line at column i

                        for (int j = 0; j < (long)(lines[i_upper_line]._points.Count); j++)
                        {
                            if (lines[i_upper_line]._points[j].col == i
                            && lines[i_upper_line]._points[j].row > highest_row_at_i_on_upper_line)
                            {
                                index_of_highest_row_at_i_on_upper_line = j;
                                highest_row_at_i_on_upper_line = lines[i_upper_line]._points[j].row;
                            }
                            if (lines[i_upper_line]._points[j].col == i
                            && lines[i_upper_line]._points[j].row < lowest_row_at_i_on_upper_line)
                            {
                                index_of_lowest_row_at_i_on_upper_line = j;
                                lowest_row_at_i_on_upper_line = lines[i_upper_line]._points[j].row;
                            }
                        }
                        if (index_of_highest_row_at_i_on_lower_line != -1
                            && index_of_lowest_row_at_i_on_upper_line != -1)
                        {
                            if (i == 0) middle_point_found_at_col_zero = true;
                            if (i == _col - 1) middle_point_found_at_last_col = true;

                            // if both found, check min and max separation distances
                            long current_min_width = lowest_row_at_i_on_upper_line - highest_row_at_i_on_lower_line;
                            long current_max_width = highest_row_at_i_on_upper_line - lowest_row_at_i_on_lower_line;

                            if ((current_min_width >= (long)(2)
                                && current_max_width <= (long)(25)) || (i < 20))
                            {
                                widths_total += current_min_width;
                                widths_count++;
                                // find vertical midpoint

                                int row_of_midpoint = (int)highest_row_at_i_on_lower_line + (int)(((double)current_min_width) / 2);

                                //     add to middle line
                                middle_lines[lm]._points.Add(
                                    convertRowCol2LinePoint(
                                        row_of_midpoint,
                                        i));
                            }
                            else
                            {
                                // supermode min width not met, remove all points
                                middle_lines[lm].clear();
                                break;
                            }
                        }
                    }//for (int i = 0; i < _col; i++)


                    if (!middle_point_found_at_col_zero && (middle_lines[lm]._points.Count != 0))
                    {
                        // need to fill in the start of the middle line to the lhs-bottom border
                        // find middle start point
                        // calculate points on straight line fit first known middle point
                        // and add to start of middle line

                        int lower_border_pt = lines[i_lower_line]._points.Count;

                        for (int j = 0; j < (int)(lines[i_lower_line]._points.Count); j++)
                        {
                            if ((lines[i_lower_line]._points[j].col == 0
                            || lines[i_lower_line]._points[j].row == 0)
                            && lower_border_pt == lines[i_lower_line]._points.Count)
                            {
                                // found point on lhs-bottom border
                                lower_border_pt = 0;
                                lower_border_pt += j;
                                break;
                            }
                        }

                        int upper_border_pt = lines[i_upper_line]._points.Count;

                        for (int j = 0; j < (int)(lines[i_upper_line]._points.Count); j++)
                        {
                            if ((lines[i_upper_line]._points[j].col == 0
                                || lines[i_upper_line]._points[j].row == 0)
                                && upper_border_pt == lines[i_upper_line]._points.Count)
                            {
                                // found point on lhs-bottom border
                                upper_border_pt = 0;
                                upper_border_pt += j;
                                break;
                            }
                        }

                        if (lower_border_pt != lines[i_lower_line]._points.Count
                            && upper_border_pt != lines[i_upper_line]._points.Count)
                        {
                            // Have upper and lower border points, find midpoint
                            long lower_dist_from_origin;
                            if ((lines[i_lower_line]._points[lower_border_pt]).col == 0)
                                lower_dist_from_origin = -(lines[i_lower_line]._points[lower_border_pt]).row;
                            else
                                lower_dist_from_origin = (lines[i_lower_line]._points[lower_border_pt]).col;

                            long upper_dist_from_origin;
                            if ((lines[i_upper_line]._points[upper_border_pt]).col == 0)
                                upper_dist_from_origin = -(lines[i_upper_line]._points[upper_border_pt]).row;
                            else
                                upper_dist_from_origin = (lines[i_upper_line]._points[upper_border_pt]).col;

                            long border_midpoint_row;
                            long border_midpoint_col;
                            long border_midpoint_dist_from_origin = lower_dist_from_origin - (lower_dist_from_origin - upper_dist_from_origin) / 2;
                            if (border_midpoint_dist_from_origin > 0)
                            {
                                border_midpoint_row = 0;
                                border_midpoint_col = border_midpoint_dist_from_origin;
                            }
                            else
                            {
                                border_midpoint_row = -border_midpoint_dist_from_origin;
                                border_midpoint_col = 0;
                            }

                            int first_middle_pt = 0;

                            long first_middle_pt_col = (middle_lines[lm]._points[first_middle_pt]).col;
                            long first_middle_pt_row = (middle_lines[lm]._points[first_middle_pt]).row;

                            double slope = ((double)(first_middle_pt_row - border_midpoint_row)) / ((double)(first_middle_pt_col - border_midpoint_col));

                            // starting at the first existing point,
                            // insert one new point in to middle line
                            // for each column between the first existing point and the border point

                            for (long i = first_middle_pt_col - 1; i >= border_midpoint_col; i--)
                            {
                                double row_of_midpoint_double = (double)border_midpoint_row + slope * ((double)(i - border_midpoint_col));
                                long row_of_midpoint = (long)row_of_midpoint_double;
                                if (row_of_midpoint_double - (double)row_of_midpoint > 0.5)
                                    row_of_midpoint++;
                                if (row_of_midpoint < 0)
                                    row_of_midpoint = 0;

                                // add to middle line
                                middle_lines[lm]._points.Insert(0,
                                    convertRowCol2LinePoint((int)row_of_midpoint, (int)i));
                            }
                        }
                    }//if (lower_border_pt != lines[i_lower_line]._points.Count && upper_border_pt != lines[i_upper_line]._points.Count) 
                    if (!middle_point_found_at_last_col
                        && (middle_lines[lm]._points.Count != 0))
                    {
                        // need to fill in the end of the middle line to the rhs-top border
                        // find middle end point
                        // calculate points on straight line fit from last known middle point
                        // and add to end of middle line

                        int lower_border_pt = lines[i_lower_line]._points.Count;

                        for (int j = 0; j < (int)(lines[i_lower_line]._points.Count); j++)
                        {
                            if ((lines[i_lower_line]._points[j].col == _col - 1
                            || lines[i_lower_line]._points[j].row == _row - 1)
                            && lower_border_pt == lines[i_lower_line]._points.Count)
                            {
                                // found point on lhs-bottom border
                                lower_border_pt = 0;
                                lower_border_pt += j;
                                break;
                            }
                        }

                        int upper_border_pt = lines[i_upper_line]._points.Count;

                        for (int j = 0; j < (int)(lines[i_upper_line]._points.Count); j++)
                        {
                            if ((lines[i_upper_line]._points[j].col == _col - 1
                                || lines[i_upper_line]._points[j].row == _row - 1)
                                && upper_border_pt == lines[i_upper_line]._points.Count)
                            {
                                // found point on lhs-bottom border
                                upper_border_pt = 0;
                                upper_border_pt += j;
                                break;
                            }
                        }

                        if (lower_border_pt != lines[i_lower_line]._points.Count
                           && upper_border_pt != lines[i_upper_line]._points.Count)
                        {
                            // Have upper and lower border points, find midpoint
                            long lower_dist_from_toprightcorner;
                            if ((lines[i_lower_line]._points[lower_border_pt]).col == _col - 1)
                                lower_dist_from_toprightcorner = (_row - 1) - (lines[i_lower_line]._points[lower_border_pt]).row;
                            else
                                lower_dist_from_toprightcorner = (lines[i_lower_line]._points[lower_border_pt]).col - (_col - 1);

                            long upper_dist_from_toprightcorner;
                            if ((lines[i_upper_line]._points[upper_border_pt]).col == 0)
                                upper_dist_from_toprightcorner = (_row - 1) - (lines[i_upper_line]._points[upper_border_pt]).row;
                            else
                                upper_dist_from_toprightcorner = (lines[i_upper_line]._points[upper_border_pt]).col - (_col - 1);

                            long border_midpoint_row;
                            long border_midpoint_col;
                            long border_midpoint_dist_from_toprightcorner;

                            long last_midpoint_row = (_lm_middle_lines[lm]._points[_lm_middle_lines[lm]._points.Count - 1]).row;

                            if ((lines[i_lower_line]._points[lower_border_pt]).row < last_midpoint_row)
                            {
                                double average_width = _average_widths[lm];
                                long pts_to_add = (long)(average_width / 2);
                                border_midpoint_dist_from_toprightcorner = lower_dist_from_toprightcorner - pts_to_add;
                                if ((_row - 1) - last_midpoint_row < border_midpoint_dist_from_toprightcorner)
                                    border_midpoint_dist_from_toprightcorner = (_row - 1) - last_midpoint_row;
                            }
                            else
                            {
                                border_midpoint_dist_from_toprightcorner = lower_dist_from_toprightcorner - (lower_dist_from_toprightcorner - upper_dist_from_toprightcorner) / 2;
                            }

                            if (border_midpoint_dist_from_toprightcorner > 0)
                            {
                                border_midpoint_row = (_row - 1) - border_midpoint_dist_from_toprightcorner;
                                border_midpoint_col = _col - 1;
                            }
                            else
                            {
                                border_midpoint_row = _row - 1;
                                border_midpoint_col = (_col - 1) + border_midpoint_dist_from_toprightcorner;
                            }

                            int last_middle_pt = _lm_middle_lines[lm]._points.Count;

                            last_middle_pt--;
                            long last_middle_pt_col = (_lm_middle_lines[lm]._points[last_middle_pt]).col;
                            long last_middle_pt_row = (_lm_middle_lines[lm]._points[last_middle_pt]).row;

                            double slope = ((double)(border_midpoint_row - last_middle_pt_row)) / ((double)(border_midpoint_col - last_middle_pt_col));

                            // starting at the end,
                            // insert one new point in to middle line
                            // for each column between the last existing point and the border point

                            for (long i = last_middle_pt_col + 1; i <= border_midpoint_col; i++)
                            {
                                double row_of_midpoint_double = (double)last_middle_pt_row + slope * ((double)(i - last_middle_pt_col));
                                long row_of_midpoint = (long)row_of_midpoint_double;
                                if (row_of_midpoint_double - (double)row_of_midpoint > 0.5) row_of_midpoint++;
                                if (row_of_midpoint > _row - 1) row_of_midpoint = _row - 1;

                                // add to middle line
                                middle_lines[lm]._points.Add(
                                    convertRowCol2LinePoint(
                                       (int)row_of_midpoint,
                                       (int)i));
                            }
                        }

                    }
                }//for (int lm = 0; lm < count_lm_lines - 1; lm++)
            }//if (count_lm_lines == (long)(lines.Count))       

            modeMiddle_lines = middle_lines;
        }

        private void filterLMMiddleLines()
        {
            // clear filtered middle lines and freq middle lines

            for (int i = 0; i < (int)(_lm_filtered_middle_lines.Count); i++)
            {
                _lm_filtered_middle_lines[i].clear();
            }
            _lm_filtered_middle_lines.Clear();

            long filter_n = 5;

            for (int lm = 0; lm < (int)(_lm_middle_lines.Count); lm++)
            {
                CDSDBRSupermodeMapLine new_filtered_lm_middle_line = new CDSDBRSupermodeMapLine();

                _lm_filtered_middle_lines.Add(new_filtered_lm_middle_line);

                // Perform averaging in the middle line direction only
                // which is a combination of If and Ir

                for (int i = 0; i < (int)(_lm_middle_lines[lm]._points.Count); i++)
                {
                    long window_size = 0;
                    long sum_of_rows_of_windowed_points = 0;
                    for (int j = (int)(i - filter_n); j <= i + filter_n; j++)
                    {
                        if (j >= 0 && j < (int)(_lm_middle_lines[lm]._points.Count))
                        {
                            // sum rows in window, without exceeding start or end
                            window_size++;
                            sum_of_rows_of_windowed_points += (long)_lm_middle_lines[lm]._points[j].row;
                        }
                    }

                    // the structure CDSDBROverallModeMapLine::point_type
                    // contains both the row, col position on a map and
                    // the currents at that point.
                    // For the average, we need to interpolate the If current
                    // while finding the nearest row (just so it can be plotted on a map)

                    CDSDBRSupermodeMapLine.Point_Type averaged_pt
                        = interpolateLinePoint(
                            (double)sum_of_rows_of_windowed_points / (double)window_size,
                            (double)(_lm_middle_lines[lm]._points[i].col));

                    // Add averaged point
                    _lm_filtered_middle_lines[lm]._points.Add(averaged_pt);
                }
            }
        }
        private void findLMMiddleLines()
        {
            // clear all LM middle lines,
            // filtered middle lines and
            // middle lines of frequency

            for (int i = 0; i < (int)(_lm_middle_lines.Count); i++)
            {
                _lm_middle_lines[i].clear();
            }
            _lm_middle_lines.Clear();

            for (int i = 0; i < (int)(_lm_filtered_middle_lines.Count); i++)
            {
                _lm_filtered_middle_lines[i].clear();
            }
            _lm_filtered_middle_lines.Clear();

            _average_widths.Clear();
            
            //process for special cases, becase etalon data seems diff from
            //power Ratio.
            if ((long)_lm_lower_lines.Count==(long)(_lm_upper_lines.Count+1))
            {
                //yes, we may have one more lower line, it may start at the beginning
                //of this supermode or at the end of this supermode
                if (Math.Abs(_lm_lower_lines[0]._points.Count - _lm_upper_lines[0]._points.Count) < lower_line_diff_count
                    &&Math.Abs(_lm_lower_lines[_lm_lower_lines.Count-1]._points.Count
                    - _lm_upper_lines[_lm_upper_lines.Count - 1]._points.Count) > upper_line_diff_count)
                {
                    //remove the last line in the lower lines set
                    _lm_lower_lines.RemoveAt(_lm_lower_lines.Count - 1);
                }
                else
                {
                    if (Math.Abs(_lm_lower_lines[0]._points.Count - _lm_upper_lines[0]._points.Count) > lower_line_diff_count
                                       && Math.Abs(_lm_lower_lines[_lm_lower_lines.Count - 1]._points.Count
                                       - _lm_upper_lines[_lm_upper_lines.Count - 1]._points.Count) < upper_line_diff_count)
                    {
                        //remove the first line the lower lines set
                        _lm_lower_lines.RemoveAt(0);
                    }   
                }
                           
            }
            if ((long)_lm_lower_lines.Count == (long)(_lm_upper_lines.Count - 1))
            {
                //yes, we may have one more upper line, it may start at the beginning
                //of this supermode or at the end of this supermode
                if (Math.Abs(_lm_lower_lines[0]._points.Count - _lm_upper_lines[0]._points.Count) < lower_line_diff_count
                    && Math.Abs(_lm_lower_lines[_lm_lower_lines.Count - 1]._points.Count
                    - _lm_upper_lines[_lm_upper_lines.Count - 1]._points.Count) > upper_line_diff_count)
                {
                    //remove the last line in the upper lines set
                    _lm_upper_lines
                        .RemoveAt(_lm_upper_lines.Count - 1);

                                    
                }
                else
                {
                    if (Math.Abs(_lm_lower_lines[0]._points.Count - _lm_upper_lines[0]._points.Count) > lower_line_diff_count
                                        && Math.Abs(_lm_lower_lines[_lm_lower_lines.Count - 1]._points.Count
                                        - _lm_upper_lines[_lm_upper_lines.Count - 1]._points.Count) < upper_line_diff_count)
                    {
                        //remove the first line the upper lines set
                        _lm_upper_lines.RemoveAt(0);
                    }
                }                
            }
            long count_lm_lines = (long)(_lm_lower_lines.Count);
            if (count_lm_lines == (long)(_lm_upper_lines.Count) )
            {
                for (int lm = 0; lm < count_lm_lines - 1; lm++)
                {
                    // iterate through the boundary lines of each longitudinal mode

                    int i_lower_line = 0;
                    i_lower_line += lm;

                    int i_upper_line = 0;
                    i_upper_line += (lm + 1);

                    CDSDBRSupermodeMapLine new_lm_middle_line = new CDSDBRSupermodeMapLine();
                    _lm_middle_lines.Add(new_lm_middle_line);

                    bool middle_point_found_at_col_zero = false;
                    bool middle_point_found_at_last_col = false;

                    // if right-most column covered is not last column in map
                    //   find midpoint on rhs-top border
                    //   plot line from right-most point found to midpoint
                    //   add points along line to middle line
                    // if left-most column covered is not first column in map
                    //   find midpoint on lhs_bottom border
                    //   plot line from left-most point found to lhs_bottom border
                    //   add points along line to middle line

                    long widths_total = 0;
                    long widths_count = 0;

                    for (int i = 0; i < _col; i++)
                    {
                        // for each column

                        int highest_row_at_i_on_lower_line = -1;
                        int index_of_highest_row_at_i_on_lower_line = -1;
                        int lowest_row_at_i_on_lower_line = (int)_row;
                        int index_of_lowest_row_at_i_on_lower_line = -1;

                        // find highest and lowest points on lower line at column i

                        for (int j = 0; j < (long)(_lm_lower_lines[i_lower_line]._points.Count); j++)
                        {
                            if (_lm_lower_lines[i_lower_line]._points[j].col == i
                            && _lm_lower_lines[i_lower_line]._points[j].row > highest_row_at_i_on_lower_line)
                            {
                                index_of_highest_row_at_i_on_lower_line = j;
                                highest_row_at_i_on_lower_line = (int)_lm_lower_lines[i_lower_line]._points[j].row;
                            }
                            if (_lm_lower_lines[i_lower_line]._points[j].col == i
                            && _lm_lower_lines[i_lower_line]._points[j].row < lowest_row_at_i_on_lower_line)
                            {
                                index_of_lowest_row_at_i_on_lower_line = j;
                                lowest_row_at_i_on_lower_line = (int)_lm_lower_lines[i_lower_line]._points[j].row;
                            }
                        }

                        long highest_row_at_i_on_upper_line = -1;
                        long index_of_highest_row_at_i_on_upper_line = -1;
                        long lowest_row_at_i_on_upper_line = (int)_row;
                        long index_of_lowest_row_at_i_on_upper_line = -1;

                        // find highest and lowest point on upper line at column i

                        for (int j = 0; j < (long)(_lm_upper_lines[i_upper_line]._points.Count); j++)
                        {
                            if (_lm_upper_lines[i_upper_line]._points[j].col == i
                            && _lm_upper_lines[i_upper_line]._points[j].row > highest_row_at_i_on_upper_line)
                            {
                                index_of_highest_row_at_i_on_upper_line = j;
                                highest_row_at_i_on_upper_line = _lm_upper_lines[i_upper_line]._points[j].row;
                            }
                            if (_lm_upper_lines[i_upper_line]._points[j].col == i
                            && _lm_upper_lines[i_upper_line]._points[j].row < lowest_row_at_i_on_upper_line)
                            {
                                index_of_lowest_row_at_i_on_upper_line = j;
                                lowest_row_at_i_on_upper_line = _lm_upper_lines[i_upper_line]._points[j].row;
                            }
                        }
                        if (index_of_highest_row_at_i_on_lower_line != -1
                            && index_of_lowest_row_at_i_on_upper_line != -1)
                        {
                            if (i == 0) middle_point_found_at_col_zero = true;
                            if (i == _col - 1) middle_point_found_at_last_col = true;

                            // if both found, check min and max separation distances
                            long current_min_width = lowest_row_at_i_on_upper_line - highest_row_at_i_on_lower_line;
                            long current_max_width = highest_row_at_i_on_upper_line - lowest_row_at_i_on_lower_line;

                            if (current_min_width >= Convert.ToInt64(this.superMapSitting["MinNumberOfPointsWidthOfALongitudinalMode"])
                                && current_max_width <= Convert.ToInt64(this.superMapSitting["MaxNumberOfPointsWidthOfALongitudinalMode"]) || i < Convert.ToInt32(this.superMapSitting["MinIphaseLongitudinalModeUncheck"]) || i > Convert.ToInt32(this.superMapSitting["MaxIphaseLongitudinalModeUncheck"])) //JAck.Zhang check ML in the Iphase range[9,95]
                            {
                                widths_total += current_min_width;
                                widths_count++;
                                // find vertical midpoint

                                int row_of_midpoint = (int)highest_row_at_i_on_lower_line + (int)(((double)current_min_width) / 2);

                                //     add to middle line
                                _lm_middle_lines[lm]._points.Add(
                                    convertRowCol2LinePoint(
                                        row_of_midpoint,
                                        i));
                            }
                            else
                            {
                                // supermode min width not met, remove all points
                                _lm_middle_lines[lm].clear();
                                break;
                            }
                        }
                    }//for (int i = 0; i < _col; i++)

                    if (_lm_middle_lines[lm]._points.Count > 0)
                        _average_widths.Add((double)widths_total / (double)widths_count);
                    else
                        _average_widths.Add(0);

                    if (!middle_point_found_at_col_zero && (_lm_middle_lines[lm]._points.Count != 0))
                    {
                        // need to fill in the start of the middle line to the lhs-bottom border
                        // find middle start point
                        // calculate points on straight line fit first known middle point
                        // and add to start of middle line

                        int lower_border_pt = _lm_lower_lines[i_lower_line]._points.Count;

                        for (int j = 0; j < (int)(_lm_lower_lines[i_lower_line]._points.Count); j++)
                        {
                            if ((_lm_lower_lines[i_lower_line]._points[j].col == 0
                            || _lm_lower_lines[i_lower_line]._points[j].row == 0)
                            && lower_border_pt == _lm_lower_lines[i_lower_line]._points.Count)
                            {
                                // found point on lhs-bottom border
                                lower_border_pt = 0;
                                lower_border_pt += j;
                                break;
                            }
                        }

                        int upper_border_pt = _lm_upper_lines[i_upper_line]._points.Count;

                        for (int j = 0; j < (int)(_lm_upper_lines[i_upper_line]._points.Count); j++)
                        {
                            if ((_lm_upper_lines[i_upper_line]._points[j].col == 0
                                || _lm_upper_lines[i_upper_line]._points[j].row == 0)
                                && upper_border_pt == _lm_upper_lines[i_upper_line]._points.Count)
                            {
                                // found point on lhs-bottom border
                                upper_border_pt = 0;
                                upper_border_pt += j;
                                break;
                            }
                        }

                        if (lower_border_pt != _lm_lower_lines[i_lower_line]._points.Count
                            && upper_border_pt != _lm_upper_lines[i_upper_line]._points.Count)
                        {
                            // Have upper and lower border points, find midpoint
                            long lower_dist_from_origin;
                            if ((_lm_lower_lines[i_lower_line]._points[lower_border_pt]).col == 0)
                                lower_dist_from_origin = -(_lm_lower_lines[i_lower_line]._points[lower_border_pt]).row;
                            else
                                lower_dist_from_origin = (_lm_lower_lines[i_lower_line]._points[lower_border_pt]).col;

                            long upper_dist_from_origin;
                            if ((_lm_upper_lines[i_upper_line]._points[upper_border_pt]).col == 0)
                                upper_dist_from_origin = -(_lm_upper_lines[i_upper_line]._points[upper_border_pt]).row;
                            else
                                upper_dist_from_origin = (_lm_upper_lines[i_upper_line]._points[upper_border_pt]).col;

                            long border_midpoint_row;
                            long border_midpoint_col;
                            long border_midpoint_dist_from_origin = lower_dist_from_origin - (lower_dist_from_origin - upper_dist_from_origin) / 2;
                            if (border_midpoint_dist_from_origin > 0)
                            {
                                border_midpoint_row = 0;
                                border_midpoint_col = border_midpoint_dist_from_origin;
                            }
                            else
                            {
                                border_midpoint_row = -border_midpoint_dist_from_origin;
                                border_midpoint_col = 0;
                            }

                            int first_middle_pt = 0;

                            long first_middle_pt_col = (_lm_middle_lines[lm]._points[first_middle_pt]).col;
                            long first_middle_pt_row = (_lm_middle_lines[lm]._points[first_middle_pt]).row;

                            double slope = ((double)(first_middle_pt_row - border_midpoint_row)) / ((double)(first_middle_pt_col - border_midpoint_col));

                            // starting at the first existing point,
                            // insert one new point in to middle line
                            // for each column between the first existing point and the border point

                            for (long i = first_middle_pt_col - 1; i >= border_midpoint_col; i--)
                            {
                                double row_of_midpoint_double = (double)border_midpoint_row + slope * ((double)(i - border_midpoint_col));
                                long row_of_midpoint = (long)row_of_midpoint_double;
                                if (row_of_midpoint_double - (double)row_of_midpoint > 0.5)
                                    row_of_midpoint++;
                                if (row_of_midpoint < 0)
                                    row_of_midpoint = 0;

                                // add to middle line
                                _lm_middle_lines[lm]._points.Insert(0,
                                    convertRowCol2LinePoint((int)row_of_midpoint, (int)i));
                            }
                        }
                    }//if (lower_border_pt != _lm_lower_lines[i_lower_line]._points.Count && upper_border_pt != _lm_upper_lines[i_upper_line]._points.Count) 
                    if (!middle_point_found_at_last_col
                        && (_lm_middle_lines[lm]._points.Count != 0))
                    {
                        // need to fill in the end of the middle line to the rhs-top border
                        // find middle end point
                        // calculate points on straight line fit from last known middle point
                        // and add to end of middle line

                        int lower_border_pt = _lm_lower_lines[i_lower_line]._points.Count;

                        for (int j = 0; j < (int)(_lm_lower_lines[i_lower_line]._points.Count); j++)
                        {
                            if ((_lm_lower_lines[i_lower_line]._points[j].col == _col - 1
                            || _lm_lower_lines[i_lower_line]._points[j].row == _row - 1)
                            && lower_border_pt == _lm_lower_lines[i_lower_line]._points.Count)
                            {
                                // found point on lhs-bottom border
                                lower_border_pt = 0;
                                lower_border_pt += j;
                                break;
                            }
                        }

                        int upper_border_pt = _lm_upper_lines[i_upper_line]._points.Count;

                        for (int j = 0; j < (int)(_lm_upper_lines[i_upper_line]._points.Count); j++)
                        {
                            if ((_lm_upper_lines[i_upper_line]._points[j].col == _col - 1
                                || _lm_upper_lines[i_upper_line]._points[j].row == _row - 1)
                                && upper_border_pt == _lm_upper_lines[i_upper_line]._points.Count)
                            {
                                // found point on lhs-bottom border
                                upper_border_pt = 0;
                                upper_border_pt += j;
                                break;
                            }
                        }

                        if (lower_border_pt != _lm_lower_lines[i_lower_line]._points.Count
                           && upper_border_pt != _lm_upper_lines[i_upper_line]._points.Count)
                        {
                            // Have upper and lower border points, find midpoint
                            long lower_dist_from_toprightcorner;
                            if ((_lm_lower_lines[i_lower_line]._points[lower_border_pt]).col == _col - 1)
                                lower_dist_from_toprightcorner = (_row - 1) - (_lm_lower_lines[i_lower_line]._points[lower_border_pt]).row;
                            else
                                lower_dist_from_toprightcorner = (_lm_lower_lines[i_lower_line]._points[lower_border_pt]).col - (_col - 1);

                            long upper_dist_from_toprightcorner;
                            if ((_lm_upper_lines[i_upper_line]._points[upper_border_pt]).col == 0)
                                upper_dist_from_toprightcorner = (_row - 1) - (_lm_upper_lines[i_upper_line]._points[upper_border_pt]).row;
                            else
                                upper_dist_from_toprightcorner = (_lm_upper_lines[i_upper_line]._points[upper_border_pt]).col - (_col - 1);

                            long border_midpoint_row;
                            long border_midpoint_col;
                            long border_midpoint_dist_from_toprightcorner;

                            long last_midpoint_row = (_lm_middle_lines[lm]._points[_lm_middle_lines[lm]._points.Count-1]).row;

                            if ((_lm_lower_lines[i_lower_line]._points[lower_border_pt]).row < last_midpoint_row)
                            {
                                double average_width = _average_widths[lm];
                                long pts_to_add = (long)(average_width / 2);
                                border_midpoint_dist_from_toprightcorner = lower_dist_from_toprightcorner - pts_to_add;
                                if ((_row - 1) - last_midpoint_row < border_midpoint_dist_from_toprightcorner)
                                    border_midpoint_dist_from_toprightcorner = (_row - 1) - last_midpoint_row;
                            }
                            else
                            {
                                border_midpoint_dist_from_toprightcorner = lower_dist_from_toprightcorner - (lower_dist_from_toprightcorner - upper_dist_from_toprightcorner) / 2;
                            }

                            if (border_midpoint_dist_from_toprightcorner > 0)
                            {
                                border_midpoint_row = (_row - 1) - border_midpoint_dist_from_toprightcorner;
                                border_midpoint_col = _col - 1;
                            }
                            else
                            {
                                border_midpoint_row = _row - 1;
                                border_midpoint_col = (_col - 1) + border_midpoint_dist_from_toprightcorner;
                            }

                            int last_middle_pt = _lm_middle_lines[lm]._points.Count;

                            last_middle_pt--;
                            long last_middle_pt_col = (_lm_middle_lines[lm]._points[last_middle_pt]).col;
                            long last_middle_pt_row = (_lm_middle_lines[lm]._points[last_middle_pt]).row;

                            double slope = ((double)(border_midpoint_row - last_middle_pt_row)) / ((double)(border_midpoint_col - last_middle_pt_col));

                            // starting at the end,
                            // insert one new point in to middle line
                            // for each column between the last existing point and the border point

                            for (long i = last_middle_pt_col + 1; i <= border_midpoint_col; i++)
                            {
                                double row_of_midpoint_double = (double)last_middle_pt_row + slope * ((double)(i - last_middle_pt_col));
                                long row_of_midpoint = (long)row_of_midpoint_double;
                                if (row_of_midpoint_double - (double)row_of_midpoint > 0.5) row_of_midpoint++;
                                if (row_of_midpoint > _row - 1) row_of_midpoint = _row - 1;

                                // add to middle line
                                _lm_middle_lines[lm]._points.Add(
                                    convertRowCol2LinePoint(
                                       (int) row_of_midpoint,
                                       (int) i));
                            }
                        }

                    }
                }//for (int lm = 0; lm < count_lm_lines - 1; lm++)
            }//if (count_lm_lines == (long)(_lm_upper_lines.Count))             
        }
        public bool LoadImCurrent(string abs_filepath)
        {
            if (File.Exists(abs_filepath))
            {
                this._filtered_middle_line.readFromFile(abs_filepath);
                this.Im_current_load = true;
                this._row = this._filtered_middle_line.getLength();
                ImiddleLine_filepath = abs_filepath;
                return true;
            }
            return false;
        }
        public bool LoadPhaseCurrent(string abs_filepath)
        {
            if (File.Exists(abs_filepath))
            {
                this._phase_current.readFromFile(abs_filepath);
                this.Ip_current_load = true;
                this._col = this._phase_current.getLength();
                return true;
            }
            return false;
        }
        private bool LoadForwardPhotodiode(string abs_filepath)
        {
            if (File.Exists(abs_filepath))
            {
                this._map_forward_photodiode1_current.setMap(MapLoader.LoadMap(abs_filepath));
                this._map_forward_photodiode2_current.setMap(MapLoader.LoadMap(abs_filepath));
                this.map_forward_photodiode1_load = true;
                return true;
            }
            return false;
        }
        private bool LoadReversePhotodiode(string abs_filepath)
        {
            if (File.Exists(abs_filepath))
            {
                double[,] map = MapLoader.LoadMap(abs_filepath);
                ReverseMatrixXDirection(map);
                this._map_reverse_photodiode1_current.setMap(map);
                this._map_reverse_photodiode2_current.setMap(map);
                this.map_reverse_photodiode1_load = true;
                return true;
            }
            return false;
        }
        public void loadMapsFromFile(string results_dir_CString,
            string laser_id_CString,
            string date_time_stamp_CString,
            string map_ramp_direction_CString)
        {

            string file_path = results_dir_CString;

            // Check for directory without trailing '\\' character
            if (file_path[file_path.Length - 1] != '\\')
            {
                // Add trailing directory backslash '\\'
                file_path += "\\";
            }
            this._laser_id = laser_id_CString;
            this._timestamp = date_time_stamp_CString;
            this._result_dir = file_path;


            string filename = "";
            filename += USCORE;
            filename += LASER_TYPE_ID_DSDBR01_CSTRING;
            filename += USCORE;
            filename += laser_id_CString;
            filename += USCORE;
            filename += date_time_stamp_CString;
            filename += USCORE;
            filename += "SM";
            filename += Sm_number.ToString();

            string file_path_phase_currents = file_path + PHASE_CURRENT_CSTRING + filename + CSV;
            string file_path_middle_line_currents = file_path + MIDDLELINE_CURRENT_CSTRING + filename + CSV;

            this._phase_current.readFromFile(file_path_phase_currents);
            Ip_current_load = true;
            this._filtered_middle_line.readFromFile(file_path_middle_line_currents);
            Im_current_load = true;

            filename += USCORE;
            filename += map_ramp_direction_CString;
            filename += CSV;

            string file_path_forward_PD1_current = file_path + MATRIX_ + MEAS_TYPE_ID_FORWARDPD1_CSTRING + filename;
            string file_path_reverse_PD1_current = file_path + MATRIX_ + MEAS_TYPE_ID_REVERSEPD1_CSTRING + filename;
            // load the forward etalon data into memory,
            // we use the same data to initialisze the data struct _map_forward_photodiode2_current;
            this._map_forward_photodiode1_current.setMap(MapLoader.LoadMap(file_path_forward_PD1_current));
            this._map_forward_photodiode2_current.setMap(MapLoader.LoadMap(file_path_forward_PD1_current)); 

            map_forward_photodiode1_load = true;

           // this._map_reverse_photodiode1_current.setMap(MapLoader.LoadMap(file_path_reverse_PD1_current));
            // data from stron.jiang need to be resversed its direction first!
            // bob.lv 2008-12-17
            double[,] map = MapLoader.LoadMap(file_path_reverse_PD1_current);
            ReverseMatrixXDirection(map);
            this._map_reverse_photodiode1_current.setMap(map);
            this._map_reverse_photodiode2_current.setMap(map);
            map_reverse_photodiode1_load = true;

            _map_forward_photodiode1_current._abs_filepath = file_path_forward_PD1_current;
            _map_reverse_photodiode1_current._abs_filepath = file_path_reverse_PD1_current;
            _map_forward_photodiode2_current._abs_filepath = file_path_forward_PD1_current;
            _map_reverse_photodiode2_current._abs_filepath = file_path_reverse_PD1_current;

            this._row = this._filtered_middle_line.getLength();
            this._col = this._phase_current.getLength();

        }       
        private void ReverseMatrixXDirection(double[,] map)
        {
            int row = map.GetLength(0);
            int col = map.GetLength(1);

            for(int i=0;i<row; i++)
            {
                for(int j=0;j<(col/2);j++)
                {
                    double temp = map[i, j];
                    map[i, j] = map[i, col-1-j];
                    map[i, col - 1 - j] = temp;                    
                }
            }
        }
        //check to see whether the nessessary components are already loaded
        private bool allDataLoad()
        {
            if (map_forward_photodiode1_load&&
                map_reverse_photodiode1_load&&Im_current_load
                &&Ip_current_load)
            {
                return true;
            }

            //at least one component not loaded
            return false;
        }

        public void calcLMMiddleLinesOfFreq()
        {
            // clear freq middle lines
            for (int i = 0; i < _lm_middle_lines_of_frequency.Count; i++)
            {
                _lm_middle_lines_of_frequency[i].clear();
            }
            _lm_middle_lines_of_frequency.Clear();
            // set this per supermode, range +-50%
            double vertical_percent_shift = Convert.ToDouble(this.superMapSitting["VerticalPercentShiftforFrequencyMiddle"]);

            for (int lm = 0; lm < _lm_filtered_middle_lines.Count; lm++)
            {
                CDSDBRSupermodeMapLine new_lm_middle_line_of_frequency = new CDSDBRSupermodeMapLine();

                _lm_middle_lines_of_frequency.Add(new_lm_middle_line_of_frequency);

                double average_width = _average_widths[lm];

                if (average_width > 0)
                {
                    double points_to_shift = average_width * (vertical_percent_shift / 100);

                    for (int i = 0; i < _lm_filtered_middle_lines[lm]._points.Count; i++)
                    {
                        double row = (double)(_lm_filtered_middle_lines[lm]._points[i].row);
                        double col = (double)(_lm_filtered_middle_lines[lm]._points[i].col);

                        row += points_to_shift;

                        // ensure point is still on the map, otherwise ignore it
                        if (row >= 0 && row <= (double)(_row - 1))
                        {
                            CDSDBRSupermodeMapLine.Point_Type shifted_pt
                                = interpolateLinePoint(row, col);

                            _lm_middle_lines_of_frequency[lm]._points.Add(shifted_pt);
                        }
                    }

                }

            }
        }


        private void CheckLMBoundaries()
        {
            //remove some isolate points in the boundary lins.
            for(int i=0;i<_lm_lower_lines.Count;i++)
            {
                CDSDBRSupermodeMapLine line = _lm_lower_lines[i];
                int checksize = line_check_point_num < line._points.Count ? line_check_point_num : line._points.Count;
                int start_col_index = (int)line._points[0].col;
                int end_col_index = (int)line._points[line._points.Count - 1].col;

                int current_col_index = start_col_index;
                int j=0;
                for ( j = 0; j < checksize; j++)
                {
                    if (line._points[j].col == current_col_index ||
                        line._points[j].col == current_col_index + 1)
                    {
                        current_col_index = (int)line._points[j].col;
                    }
                    else
                    {
                        break;
                    }
                }

                // yes remove the staring points of this line.
                // these points seems a noise to the line
                if (j < checksize)
                {
                    for (int k = 0; k < j; k++)
                    {
                        line._points.RemoveAt(0);
                    }
                }

                //reset checksize because the length of this line might has been changed.
                checksize = line_check_point_num < line._points.Count ? line_check_point_num : line._points.Count;
                current_col_index = end_col_index;
                int size = 0;
                for (j = line._points.Count - 1; j > 0 && size < checksize; j--)
                {
                    if (line._points[j].col == current_col_index ||
                        line._points[j].col == current_col_index - 1)
                    {
                        current_col_index = (int)line._points[j].col;
                        size++;
                    }
                    else
                    {
                        break;
                    }
                }

                // yes remove the staring points of this line.
                // these points seems a noise to the line
                if (size < checksize)
                {
                    for (int k = 0; k < size; k++)
                    {
                        line._points.RemoveAt(line._points.Count-1);
                    }
                }
               
            }
            for (int i = 0; i < _lm_upper_lines.Count; i++)
            {
                CDSDBRSupermodeMapLine line = _lm_upper_lines[i];
                int checksize = line_check_point_num < line._points.Count ? line_check_point_num : line._points.Count;
                int start_col_index = (int)line._points[0].col;
                int end_col_index = (int)line._points[line._points.Count - 1].col;

                int current_col_index = start_col_index;
                int j = 0;
                for (j = 0; j < checksize; j++)
                {
                    if (line._points[j].col == current_col_index ||
                        line._points[j].col == current_col_index + 1)
                    {
                        current_col_index = (int)line._points[j].col;
                    }
                    else
                    {
                        break;
                    }
                }

                // yes remove the staring points of this line.
                // these points seems a noise to the line
                if (j < checksize)
                {
                    for (int k = 0; k < j; k++)
                    {
                        line._points.RemoveAt(0);
                    }
                }

                //reset checksize because the length of this line might has been changed.
                checksize = line_check_point_num < line._points.Count ? line_check_point_num : line._points.Count;
                current_col_index = end_col_index;
                int size = 0;
                for (j = line._points.Count - 1; j > 0 && size < checksize; j--)
                {
                    if (line._points[j].col == current_col_index ||
                        line._points[j].col == current_col_index - 1)
                    {
                        current_col_index = (int)line._points[j].col;
                        size++;
                    }
                    else
                    {
                        break;
                    }
                }

                // yes remove the staring points of this line.
                // these points seems a noise to the line
                if (size < checksize)
                {
                    for (int k = 0; k < size; k++)
                    {
                        line._points.RemoveAt(line._points.Count-1);
                    }
                }
            }
        }
        public void setup(string lase_id, string timestamp, string result_dir, InstrumentCollection instruments)
        {
            this._result_dir = result_dir;
            this._laser_id = lase_id;
            this._timestamp = timestamp;
            this.Optical_Switch = (Inst_Ke2510)instruments["Optical_switch"];
        }
        public void clear()
        {
            _upper_line.clear();
            _lower_line.clear();
            _middle_line.clear();

            _phase_current.clear();
            _filtered_middle_line.clear();

            //_boundary_detection_ramp.clear();

            clearMapsAndLines();

            _result_dir = "";
            _timestamp = "";
            _laser_id = "";
         

            _Im_ramp = false;
            _use_rear_cut = false;
            _row = 0;
            _col = 0;
            //_sourceDelay = 0;
            //_measureDelay = 0;
            _sm_number = 0;
           // _maps_loaded = false;
            _row_of_midpoint_of_filtered_middle_line = 0;
            I_gain = 0;
            I_soa = 0;
            I_rearsoa = 0;
            _middleLineRMSValue = 0;
            _middleLineSlope = 0;
            _modeAnalysis.init();
        }

        #region class data
        public CDSDBROverallModeMapLine _upper_line = new CDSDBROverallModeMapLine();
        public CDSDBROverallModeMapLine _lower_line = new CDSDBROverallModeMapLine();
        public CDSDBROverallModeMapLine _middle_line = new CDSDBROverallModeMapLine();
        public CDSDBROverallModeMapLine _filtered_middle_line = new CDSDBROverallModeMapLine();
        public long _row_of_midpoint_of_filtered_middle_line;

        public List<CDSDBRSupermodeMapLine> _lm_upper_lines = new List<CDSDBRSupermodeMapLine>();
        public List<CDSDBRSupermodeMapLine> _lm_lower_lines = new List<CDSDBRSupermodeMapLine>();
        public List<CDSDBRSupermodeMapLine> _lm_middle_lines = new List<CDSDBRSupermodeMapLine>();
        public List<CDSDBRSupermodeMapLine> _lm_filtered_middle_lines = new List<CDSDBRSupermodeMapLine>();
        public List<CDSDBRSupermodeMapLine> _lm_middle_lines_of_frequency = new List<CDSDBRSupermodeMapLine>();
        public List<CDSDBRSupermodeMapLine> _upper_middle_lines = new List<CDSDBRSupermodeMapLine>();
        public List<CDSDBRSupermodeMapLine> _lower_middle_lines = new List<CDSDBRSupermodeMapLine>();
        List<Pair<ulong,ulong>> _matched_line_numbers = new List<Pair<ulong,ulong>>();
        // count of the number of supermodes removed because of flaws encountered
        public short _lm_upper_lines_removed;
        public short _lm_lower_lines_removed;
        public short _lm_middle_lines_of_frequency_removed;

        public CLaserModeMap _map_forward_power_ratio_median_filtered = new CLaserModeMap();
	    public CLaserModeMap _map_reverse_power_ratio_median_filtered = new CLaserModeMap();
	    public CLaserModeMap _map_hysteresis_power_ratio_median_filtered = new CLaserModeMap();

        public CLaserModeMap _map_forward_photodiode1_current = new CLaserModeMap();
        public CLaserModeMap _map_reverse_photodiode1_current = new CLaserModeMap();
        public CLaserModeMap _map_forward_photodiode2_current = new CLaserModeMap();
        public CLaserModeMap _map_reverse_photodiode2_current = new CLaserModeMap();

        bool _Im_ramp; // true for middle line ramp, false for phase ramp
        bool _use_rear_cut;
        bool _use_etalon_data;

        public CCurrentVector _phase_current = new CCurrentVector();
        public List<double> _average_widths = new List<double>();
        // public CLaserModeMap _map = new CLaserModeMap();
        public string _timestamp;
        public string _laser_id;
        public string _result_dir = "";
        public int _sm_number;
        public string _SmForwardMatricDataLockerSource = "MATRIX_forwardPD1Current_DSDBR01_";
        public string _SmReverseMatricDataLockerSource = "MATRIX_reversePD1Current_DSDBR01_";

        private NameValueCollection superMapSitting;
        private NameValueCollection overAllMapSitting;
        private NameValueCollection superMapBoundarySitting;

        public double I_gain;
        public double I_soa;
        public double I_rearsoa;


        public int Sm_number
        {
            get { return _sm_number; }
            set { _sm_number = value; }
        }
        public long _row;
        public long _col;        
        public Inst_FCU Inst = null;


        //flags
        private bool Im_current_load = false;
        private bool Ip_current_load = false;
        private bool map_forward_photodiode1_load = false;
        private bool map_reverse_photodiode1_load = false;
        public bool loadAllFromFile = false ;
        private bool map_screened = false;
        private bool map_caled = false;
        private string ImiddleLine_filepath;
        private List<Pair<double, double>> modewidths = new List<Pair<double, double>>();
        private double modalDistortion = 0.0;

        public event EventHandler ScanDirectionChange;
        public event EventHandler StartSuperModeAnalysis;

        //used for cauculate Mild MultiMode
        double rearCutDiffThreshold = -0.003;
        int allowedFails = 0;

        #endregion

#region Quantative Analysis
    //////////////////////////////////////
	//
	//	Quantative Analysis
	//
	   public  ModeAnalysis _modeAnalysis = new ModeAnalysis();

        public void runModeAnalysis()
        { 

	        loadModeAnalysisBoundaryLines();

	        _modeAnalysis.runAnalysis();

	        _qaAnalysisComplete = true;

        }
	public void	loadModeAnalysisBoundaryLines()
    {
        ModeBoundaryLine upperModeBoundaryLine= new ModeBoundaryLine(_upper_line.getCols(), _upper_line.getRows(), false);
	    ModeBoundaryLine lowerModeBoundaryLine = new ModeBoundaryLine(_lower_line.getCols(), _lower_line.getRows(), false);

	    _modeAnalysis.loadBoundaryLines(_sm_number, _xAxisLength, _yAxisLength, upperModeBoundaryLine, lowerModeBoundaryLine);

	    getOMQAThresholds();
    	
	    _qaThresholds.setThresholdsToDefault();

	    _qaThresholds._slopeWindow			= _omqaSlopeWindow;
	    _qaThresholds._modalDistortionMinX = _omqaModalDistortionMinX;
	    _qaThresholds._modalDistortionMaxX = _omqaModalDistortionMaxX;
	    _qaThresholds._modalDistortionMinY = _omqaModalDistortionMinY;
	    //GDM 31/10/06 flag that this is the OM qa to the analysis sub
	    _qaThresholds._SMMapQA = 0;
	    _modeAnalysis.setQAThresholds(_qaThresholds);
    }
	public void	getOMQAThresholds()
    {
        _omqaSlopeWindow = Convert.ToInt32(this.overAllMapSitting["SlopeWindowSize"]);
        _omqaModalDistortionMinX = Convert.ToInt32(this.overAllMapSitting["ModalDistortionMinX"]);
        _omqaModalDistortionMaxX = Convert.ToInt32(this.overAllMapSitting["ModalDistortionMaxX"]);
        _omqaModalDistortionMinY = Convert.ToInt32(this.overAllMapSitting["ModalDistortionMinY"]);
    }

	public void	getMiddleLineRMSValue(ref double rmsValue)
    {
        rmsValue = 0;

	/////////////////////////////////////////////////////////////////////
	/// Use VectorAnalysis to get RMS value of this supermodes middle line
	//
	if(_middleLineRMSValue == 0)
	{
		if(_filtered_middle_line.getLength() > 0)
		{
			List<double> middleLineXPoints;
			List<double> middleLineYPoints;

			middleLineXPoints = _filtered_middle_line.getCols();
			middleLineYPoints = _filtered_middle_line.getRows();

			double	linearFitLineSlope = 0;
			double	linearFitLineIntercept = 0;

			// find linear fit line
			VectorAnalysis.linearFit(middleLineXPoints, 
									middleLineYPoints,
									ref linearFitLineSlope,
									ref linearFitLineIntercept);

			_middleLineSlope	= linearFitLineSlope;

			// find distances from points to linear fit line
			List<double> distances = new List<double>();
			VectorAnalysis.distancesFromPointsToLine(middleLineXPoints,
													middleLineYPoints,
													ref linearFitLineSlope,
													ref linearFitLineIntercept,
													distances);

			// square each distance
			for(int i = 0 ; i < distances.Count ; i ++ )
			{
				distances[i] = distances[i]*distances[i];
			}

			// get mean of squared distances
			double meanSqDistance = 0;
			meanSqDistance = VectorAnalysis.meanOfValues(distances);

			// get root of mean of squared distances
			_middleLineRMSValue =  Math.Sqrt( meanSqDistance );			
		}
	}

	rmsValue = _middleLineRMSValue;
    }
	public void	getMiddleLineSlope(ref double slope)
    {
         
	    slope = 0;

	    if(_middleLineSlope == 0)
	    {
			    List<double> middleLineXPoints;
			    List<double> middleLineYPoints;

			    middleLineXPoints = _filtered_middle_line.getCols();
			    middleLineYPoints = _filtered_middle_line.getRows();

			    double	linearFitLineSlope = 0;
			    double	linearFitLineIntercept = 0;

			    // find linear fit line
			    VectorAnalysis.linearFit(middleLineXPoints, 
									    middleLineYPoints,
									    ref linearFitLineSlope,
									    ref linearFitLineIntercept);

			    _middleLineSlope = linearFitLineSlope;
	    }

	    slope = _middleLineSlope;
    }

	public int _xAxisLength;
	public int _yAxisLength;

	public QAThresholds	_qaThresholds = new QAThresholds();

	public int		_omqaSlopeWindow;
	public double	_omqaModalDistortionMinX;
	public double	_omqaModalDistortionMaxX;
	public double	_omqaModalDistortionMinY;

	public double	_middleLineRMSValue;
	public double	_middleLineSlope;

	public bool	_qaAnalysisComplete;


	//
	////////////////////////////////////////
	//	Super mode analysis
	//
    public SuperModeMapAnalysis	_superModeMapAnalysis = new SuperModeMapAnalysis();

	//
	//////////////////////////////////////
#endregion
    }
}
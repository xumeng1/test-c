using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Bookham.fcumapping.CommonData
{
    public class CLaserModeMap
    {
        public CLaserModeMap(){}

        public CLaserModeMap(double[,] map)
        {
            _map.Clear();
            _rows =  map.GetLength(0);
            _cols =  map.GetLength(1);

            for (int i = 0; i < _rows - 1; i++)
            {
                for (int j = 0; j < _cols - 1; j++)
                {
                    _map.Add(map[i, j]);
                }
            }
        }

        public void writeToFile(string abs_filepath, bool overwrite)
        {

        }

        public void readFromFile(string abs_filepath)
        {

        }

        public void setMap(double[,] map)
        {
            _map.Clear();
            _rows = map.GetLength(0);
            _cols = map.GetLength(1);

            for(int i=0 ;i<=_rows-1;i++)
            {
                for(int j=0;j<=_cols-1;j++)
                {
                    _map.Add(map[i, j]);
                }
            }
        }

        public void clear()
        {
            _map.Clear();
            _rows = 0;
            _cols = 0;
        }

        public bool isEmpty()
        {
            //stub here, need to be overwritten
            if (_map.Count == 0)
            {
                return true;
            }
            return false;
        }

        public double getValue(int row,int col)
        {
            //stub here, need to be overwritten
            if (row>=0 && row<=(_rows-1) && col>=0 && col<=(_cols-1))
            {
                return _map[row * col + col];
            }
            else
            {

                return 0;
            }
        }
        public double iPt(long row, long col)
        {
            int index = 0;
            // check point is within map
            if (row >= 0 && row < _rows && col >= 0 && col < _cols)
            {
                index = (int)( row * _cols + col);
            }

            return _map[index];
        }
        public void writeJumpsToFile(string abs_filepath)
        {
            StreamWriter writer = new StreamWriter(abs_filepath);
            
            for(int i=0;i<_boundary_points.Count;i++)
            {
                writer.WriteLine(boundaryPoint(_boundary_points[i]).ToString());
            }

            writer.Flush();
            writer.Close();
        }

        public void WriteLinkJumpsToFile(string abs_filepath)
        {
            StreamWriter writer = new StreamWriter(abs_filepath);

            for (int i = 0; i < _linked_boundary_points.Count; i++)
            {
                writer.WriteLine(_linked_boundary_points[i].First.ToString() +","+ _linked_boundary_points[i].Second.ToString());
            }

            writer.Flush();
            writer.Close();

        }
        private long boundaryPoint(Pair<long, long> item)
        {
            long first = item.First;
            long second = item.Second;
            long boundary = 0;
            if ((int)second - (int)first == 2 ) boundary = second - 1;
            else if ((int)first - (int)second == 2) boundary = first - 1;
            else boundary = second;
            return boundary;
        }


        public string _abs_filepath;
        public List<double> _map= new List<double>();
        public long _rows;
        public long _cols;
        public List<Pair<long, long>> _boundary_points = new List<Pair<long, long>>();
        public List<Pair<long, long>> _linked_boundary_points =
            new List<Pair<long, long>>();
    }
}

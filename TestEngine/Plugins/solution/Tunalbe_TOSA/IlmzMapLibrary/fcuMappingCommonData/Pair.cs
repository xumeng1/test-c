using System;
using System.Collections.Generic;
using System.Text;

//add some comments here
namespace Bookham.fcumapping.CommonData
{
    public class Pair<T, U>
    {
        private T first;
        private U second;
        public Pair()
        {
        }

        public Pair(T first, U second)
        {
            this.First = first;
            this.Second = second;
        }

        public T First
        {
            get
            {
                return first;
            }
            set
            {
                first = value;
            }
        }
        public U Second
        {
            get
            {
                return second;
            }
            set
            {
                second = value;
            }
        }
    };

}

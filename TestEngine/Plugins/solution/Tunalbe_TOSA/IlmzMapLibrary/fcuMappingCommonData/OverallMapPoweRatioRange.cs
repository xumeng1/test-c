using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.fcumapping.CommonData
{
    public static class OverallMapPoweRatioRange
    {
        /// <summary>
        /// min powr ratio after fcu collect overall map
        /// </summary>
        public double  MinPowerRatio
        {
            get { return minPr; }
            set { minPr = value; }
        }
        /// <summary>
        /// max power ratio after FCU collct overll map
        /// </summary>
        public double MaxPowerRatio
        {
            get { return maxPr; }
            set { maxPr = value; }
        }

        double minPr, maxPr;
    }

    
}

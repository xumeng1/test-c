using System;
using System.Collections.Generic;
using System.Text;
//coded by purney.xie
namespace Bookham.fcumapping.CommonData
{
    public class PassFailThresholds
    {

        public PassFailThresholds()
        {
            setThresholdsToDefault();
        }
        public int _maxModeBordersRemoved;
        public int _maxModeWidth;
        public int _minModeWidth;
        public double _maxModeBDCArea;
        public double _maxModeBDCAreaXLength;
        public double _sumModeBDCAreas;
        public double _numModeBDCAreas;
        public double _modalDistortionAngle;
        public double _modalDistortionAngleXPosition;
        public double _meanPercWorkingRegion;
        public double _minPercWorkingRegion;
        public double _middleLineRMSValue;
        public double _maxMiddleLineSlope;
        public double _minMiddleLineSlope;
        public double _maxPrGap;
        public double _maxLowerPr;
        public double _minUpperPr;
        public double _maxModeLineSlope;
        public double _minModeLineSlope;

        // const variable
        const int DEFAULT_MAX_MODE_BORDERS_REMOVED = 0;
        const int DEFAULT_MAX_MODE_WIDTH = 40;
        const int DEFAULT_MIN_MODE_WIDTH = 10;
        const double DEFAULT_MAX_MODE_BDC_AREA = 30;
        const double DEFAULT_MAX_MODE_BDC_AREA_X_LENGTH = 20;
        const double DEFAULT_SUM_MODE_BDC_AREAS = 60;
        const double DEFAULT_NUM_MODE_BDC_AREAS = 8;
        const double DEFAULT_MAX_MODAL_DISTORTION_ANGLE = 19;
        const double DEFAULT_MAX_MODAL_DISTORTION_ANGLE_X_POS = 80;
        const double DEFAULT_MEAN_PERC_WORKING_REGION = 50;
        const double DEFAULT_MIN_PERC_WORKING_REGION = 39;
        const double DEFAULT_MIDDLE_LINE_RMS_VALUE = 5;
        const double DEFAULT_MIN_MIDDLE_LINE_SLOPE = 0;
        const double DEFAULT_MAX_MIDDLE_LINE_SLOPE = 90;
        const double DEFAULT_MAX_CONTINUITY_SPACING = 0.5;
        const double DEFAULT_MAX_PR = 0.9;
        const double DEFAULT_MIN_PR = 0.1;
        const double DEFAULT_MAX_MODE_LINE_SLOPE = 5;
        const double DEFAULT_MIN_MODE_LINE_SLOPE = -5;

        ////////////////////////////////////

        private void setThresholdsToDefault()
        {
            _maxModeBordersRemoved = DEFAULT_MAX_MODE_BORDERS_REMOVED;
            _maxModeWidth = DEFAULT_MAX_MODE_WIDTH;
            _minModeWidth = DEFAULT_MIN_MODE_WIDTH;
            _maxModeBDCArea = DEFAULT_MAX_MODE_BDC_AREA;
            _maxModeBDCAreaXLength = DEFAULT_MAX_MODE_BDC_AREA_X_LENGTH;
            _sumModeBDCAreas = DEFAULT_SUM_MODE_BDC_AREAS;
            _numModeBDCAreas = DEFAULT_NUM_MODE_BDC_AREAS;
            _modalDistortionAngle = DEFAULT_MAX_MODAL_DISTORTION_ANGLE;
            _modalDistortionAngleXPosition = DEFAULT_MAX_MODAL_DISTORTION_ANGLE_X_POS;
            _meanPercWorkingRegion = DEFAULT_MEAN_PERC_WORKING_REGION;
            _minPercWorkingRegion = DEFAULT_MIN_PERC_WORKING_REGION;
            _middleLineRMSValue = DEFAULT_MIDDLE_LINE_RMS_VALUE;
            _maxMiddleLineSlope = DEFAULT_MAX_MIDDLE_LINE_SLOPE;
            _minMiddleLineSlope = DEFAULT_MIN_MIDDLE_LINE_SLOPE;
            _maxPrGap = DEFAULT_MAX_CONTINUITY_SPACING;
            _maxLowerPr = DEFAULT_MAX_PR;
            _minUpperPr = DEFAULT_MIN_PR;
            _maxModeLineSlope = DEFAULT_MAX_MODE_LINE_SLOPE;
            _minModeLineSlope = DEFAULT_MIN_MODE_LINE_SLOPE;
        }


    }
}

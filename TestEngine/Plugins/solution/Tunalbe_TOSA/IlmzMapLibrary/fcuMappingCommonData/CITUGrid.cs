using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Bookham.fcumapping.utility;

namespace Bookham.fcumapping.CommonData
{
    public class CITUGrid
    {
        #region class datas
        public List<double> _ITU_frequencies = new List<double>();
        public int _length = 0;
        public int freq_step = 0;

        //echo added 
        public double max_freq = 0;
        public double min_freq = 0;

        public bool setMaxMinFreq = false;
        public bool setChanNbr = false;
        // end add
        #endregion


        public CITUGrid() { }

        public bool IsEmpty()
        {
            return (_ITU_frequencies.Count == 0);
        }

        public double getMax_freq()
        {
            return max_freq;
        }
        public double getMin_freq()
        {
            return min_freq;
        }
        public int getfreq_step()
        {
            return freq_step;
        }
        public int getChannelNbr()
        {
            return _length;
        }

        public void setupEx(string abs_filepath)
        {
            if (!this.setMaxMinFreq)
            {
                Clear();

                // first, try file abspath
                ReadFromFile(abs_filepath);
            }
            else
            {
                GetFrequencyList();
            }
            if (!this.setMaxMinFreq)
            {
                setupMaxMinFreq();
            }
            if (!this.setChanNbr)
            {
                _length = (int)_ITU_frequencies.Count;
            }
        }

        private void GetFrequencyList()
        {
            for (int i = 0; i < this.channel_count(); i++)
            {

                double freq = this.min_freq + i * this.freq_step;
                if (!_ITU_frequencies.Contains(freq))
                {
                    _ITU_frequencies.Add(freq);
                }
            }

        }

        public void ReadFromFile(string abs_filepath)
        {
            _ITU_frequencies = MapLoader.loadArrayList(abs_filepath);
        }

        public int channel_count()
        {
            return _length;
        }

        public void setupMaxMinFreq()
        {
            if (IsEmpty())
            {
                max_freq = 0;
                min_freq = 0;
                return;
            }

            max_freq = -1;
            min_freq = 10000000000;
            for (int start = 0; start != _ITU_frequencies.Count; start++)
            {
                if ((_ITU_frequencies[start]) > max_freq)
                    max_freq = (_ITU_frequencies[start]);
                if ((_ITU_frequencies[start]) < min_freq)
                    min_freq = (_ITU_frequencies[start]);
            }
        }

        public void findITUPtsInRange(List<int> channel_num_of_itu_pts_in_lm,
            List<double> freq_of_itu_pts_in_lm,
        double min_freq,
        double max_freq)
        {
            channel_num_of_itu_pts_in_lm.Clear();
            freq_of_itu_pts_in_lm.Clear();
            for (int itu_channel_num = 0; itu_channel_num < (int)(channel_count()); itu_channel_num++)
            {
                if (_ITU_frequencies[itu_channel_num] >= min_freq && _ITU_frequencies[itu_channel_num] <= max_freq)
                {
                    channel_num_of_itu_pts_in_lm.Add(itu_channel_num);
                    freq_of_itu_pts_in_lm.Add(_ITU_frequencies[itu_channel_num]);
                }
            }
        }

        public void Clear()
        {
            max_freq = 0;
            min_freq = 0;
            _ITU_frequencies.Clear();
            _length = 0;
            setMaxMinFreq = false;
            setChanNbr = false;
        }
    }
}

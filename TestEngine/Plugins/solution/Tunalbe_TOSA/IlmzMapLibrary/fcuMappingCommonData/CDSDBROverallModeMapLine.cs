using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.Utilities;


namespace Bookham.fcumapping.CommonData
{
    public class CDSDBROverallModeMapLine
    {
        public class Point_Type
        {
            public long row;
            public long col;
            public short front_pair_number;
            public double constantCurrent;
            public double nonConstantCurrent;
            public double rear;
        }

        public CDSDBROverallModeMapLine() { }

        public void readFromFile(string abs_filepath)
        {
            using (CsvReader csvReader = new CsvReader())
            {
                List<string[]> lines = csvReader.ReadFile(abs_filepath);
                string frontpair;
                string rear;
                string constantcurrent;
                string nonConstantcurrent;
                for (int i = 1; i < lines.Count; i++)
                {
                    rear = lines[i][0];
                    frontpair = lines[i][1];
                    constantcurrent = lines[i][2];
                    nonConstantcurrent = lines[i][3];

                    Point_Type pt = new Point_Type();
                    pt.front_pair_number = short.Parse(frontpair);
                    pt.rear = double.Parse(rear);
                    pt.constantCurrent = double.Parse(constantcurrent);
                    pt.nonConstantCurrent = double.Parse(nonConstantcurrent);

                    // arr.Add(double.Parse(str));
                    _points.Add(pt);
                }
            }

            this._abs_filepath = abs_filepath;
        }

        public void clear()
        {
            //_total_length = 0;
            //_submap_length.clear();
            //_submap_start_index.clear();
            _points.Clear();
            //_abs_filepath.Empty();
        }
        public long getLength()
        {
            return this._points.Count;
        }
        public List<Point_Type> _points = new List<Point_Type>();
        public string _abs_filepath;

       public  List<double> getRows()
        {
            List<double> rows = new List<double>();

            for (int i = 0; i < _points.Count; i++)
            {
                rows.Add((double)_points[i].row);
            }

            return rows;
        }

       public List<double> getCols()
        {
            List<double> cols = new List<double>();

            for (int i = 0; i < _points.Count; i++)
            {
                cols.Add((double)_points[i].col);
            }

            return cols;
        }
    }
}

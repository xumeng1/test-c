using System;
using System.Collections.Generic;
using System.Text;
//coded by purney.xie
namespace Bookham.fcumapping.CommonData
{

    public class VectorAnalysis
    {
        public static double M_PI_2 = 1.57079632679489661923;
        public static double M_PI = 3.14159265358979323846;
        public static double derivative(double x1, double x2, double y1, double y2)
        {
            return (y2 - y1) / (x2 - x1);
        }

        public static double doubleDerivative(double x1, double x2, double x3, double y1, double y2, double y3)
        {
            return (derivative(x2, x3, y2, y3) - derivative(x1, x2, y1, y2)) / (x2 - x1);
        }



        public static void weightedFilter_RearCut(List<double> x_in, List<double> filtered_x_out, int rank, int line_length)
        {
            List<double> subset_of_x = new List<double>();
            int i_count = 0;
            filtered_x_out.Clear();

            List<double> weightsRank = CreateWeightingFactors(rank);

            double tempData = 0.0;


            if ((line_length > 0) && (x_in.Count > 0) && ((x_in.Count % line_length) == 0))
            {
                // input data looks okay
                //List<double> subset_of_x = new List<double>();

                int point_count = 0;
                int offset = 0;

                for (int i_x_in = 0; i_x_in < x_in.Count; i_x_in++)
                {
                    // Iterate through each line in the map
                    int point_in_line_count = point_count % line_length;

                    double calcValue = 0.00;

                    if (point_in_line_count < rank)
                    {
                        // We are at the start of the line  ( out-of-bounds),
                        // so we need to apply the weighting factors instead of a 
                        // median filter
                        if (point_in_line_count == 0)
                        {
                            // Special case
                            calcValue = ((x_in[point_count] + (x_in[point_count + 1] * 0.5)) / 1.5);
                            filtered_x_out.Add(calcValue);
                        }
                        else
                        {
                            double totalWeighting = 0.00;

                            for (int a = (rank - 1) + point_in_line_count, b = point_in_line_count * -1;
                                b <= point_in_line_count; b++, a--)
                            {
                                double weightingFactor = weightsRank[a];
                                double valueToBeSmoothed = x_in[point_count + b];
                                calcValue += (valueToBeSmoothed * weightingFactor);
                                totalWeighting += weightingFactor;
                            }

                            filtered_x_out.Add(calcValue / totalWeighting);
                        }
                    }
                    else if ((line_length - 1) - point_in_line_count < rank)
                    {
                        // we are approaching the end of the line ( out-of-bounds), 
                        // so we need to apply the weighting factors in reverse.
                        if (point_in_line_count == line_length - 1)
                        {
                            // Last element = Special case
                            calcValue = ((x_in[point_count] + (x_in[point_count - 1] * 0.5)) / 1.5);
                            filtered_x_out.Add(calcValue);
                        }
                        else
                        {
                            double totalWeighting = 0.00;

                            for (int a = (rank - 1) + (line_length - 1) - point_in_line_count, b = (line_length - 1 - point_in_line_count) * -1;
                                b <= (int)(line_length - 1 - point_in_line_count); b++, a--)
                            {
                                double weightingFactor = weightsRank[a];
                                double valueToBeSmoothed = x_in[point_in_line_count + b];
                                calcValue += (valueToBeSmoothed * weightingFactor);
                                totalWeighting += weightingFactor;
                            }

                            filtered_x_out.Add(calcValue / totalWeighting);
                        }
                    }
                    else
                    {
                        // We are in-bounds, so we can apply a normal median 
                        // filter to this point

                        // pick vector +/-rank about each sample 
                        subset_of_x.Clear();

                        for (int j = point_in_line_count - rank; j <= point_in_line_count + rank; j++)
                            subset_of_x.Add(x_in[point_count - point_in_line_count + j]);

                        // The median of the sorted vector s[i] where i = 0, n-1 is defined as
                        // s[(n-1)/2] where n is odd 
                        // ( s[(n/2)-1] + s[n/2] ) / 2 where n is even
                        // n is always odd in a median filter

                        // Calculate the median for the subset of x and add it to the output
                        subset_of_x.Sort();
                        filtered_x_out.Add(subset_of_x[rank]);
                    }

                    point_count++;
                }
            }
            else
            {
                // input data is bad, see if() statement above
                throw new Exception(" weightedFilter_RearCut - Invalid input data");
            }
        }


        public static List<double> CreateWeightingFactors(int rank)
        {
            double[] rank3 = { 0.1, 0.3, 1.0, 0.3, 0.1 };
            double[] rank5 = { 0.1, 0.1, 0.2, 0.3, 1.0, 0.3, 0.2, 0.1, 0.1 };
            double[] rank7 = { 0.1, 0.1, 0.1, 0.2, 0.2, 0.3, 1.0, 0.3, 0.2, 0.2, 0.1, 0.1, 0.1 };
            List<double> retVal = new List<double>();

            switch (rank)
            {
                case 3:

                    for (int i = 0; i < rank * 2; i++)
                        retVal[i] = rank3[i];
                    break;

                case 5:

                    for (int i = 0; i < rank * 2; i++)
                        retVal[i] = rank5[i];
                    break;

                case 7:

                    for (int i = 0; i < rank * 2; i++)
                        retVal[i] = rank7[i];
                    break;

                default:
                    throw new Exception(" CreateWeightingFactors - Invalid rank selection");
                    break;
            }

            return (retVal);
        }



        public static void medianFilter(List<double> x_in, List<double> filtered_x_out, int rank, int line_length)
        {
            // The median filter is well documented in edge detection as
            // being suited to removing noise while preserving edges
            // whereas traditionally-used low pass filters effect the
            // edge so as to lower its slope.
            // This function is for a map where the filter is to be
            // applied to each ramp line in isolation from the next
            // Hence line_length tells us the length of each line

            // If the input is x[i] where i = 0, n-1
            // and the output is y[i] where i = 0, n-1
            // y[i] = median( x[i-r], ... x[i], ... x[i+r] )
            // where r is the rank of the filter
            // elements of x outside the range are treated as zero

            filtered_x_out.Clear();

            if (line_length > 0 && x_in.Count > 0 && (x_in.Count % line_length) == 0)
            {
                // input data looks okay
                List<double> subset_of_x = new List<double>();

                int point_count = 0;
                for (int i_x_in = 0; i_x_in < x_in.Count; i_x_in++)
                {
                    // Iterate through each line in the map
                    int point_in_line_count = point_count % line_length;

                    // pick vector +/-rank about each sample 
                    subset_of_x.Clear();
                    for (int j = point_in_line_count - rank; j <= point_in_line_count + rank;j++)
                    {
                        if (j < 0 || j >= (long)line_length)
                        { // outside bounds of current line, use current point as value
                            subset_of_x.Add(x_in[point_count]);
                        }
                        else
                        { // within bounds of current line
                            subset_of_x.Add(x_in[point_count - point_in_line_count + j]);
                        }
                    }

                    // The median of the sorted vector s[i] where i = 0, n-1 is defined as
                    // s[(n-1)/2] where n is odd 
                    // ( s[(n/2)-1] + s[n/2] ) / 2 where n is even
                    // n is always odd in a median filter

                    // Calculate the median for the subset of x and add it to the output
                    // std::sort(subset_of_x.begin(),subset_of_x.end());
                    subset_of_x.Sort();
                    filtered_x_out.Add(subset_of_x[rank]);
                    point_count++;
                }

            }
            else
            { // input data is bad, see if() statement above
            }
        }


        public static void SmoothUsingPascalTriangle(List<double> Data, List<double> filtered_x_out, int numSmoothPts)
        {
            // PRECONDITIONS
            if (Data.Count == 0)
            {
                throw new Exception("SmoothUsingPascalTriangle - empty array");
            }

            // numSmoothPts must be odd
            if ((numSmoothPts < 3) || (numSmoothPts % 2) == 0)
            {
                throw new Exception("SmoothUsingPascalTriangle - Requested Smoothing Factor must be an ODD integer between 3 and the size of the array.");
            }

            if (Data.Count < numSmoothPts)
            {
                throw new Exception("SmoothUsingPascalTriangle - inputArray must have at least as many points as those used in smoothing.");
            }

            int smoothFactor = numSmoothPts;

            int DataSize = Data.Count;

            int DataArrayIndex = 0;
            int EndOfDataArray = Data.Count - 1;

            int DataArrayCounter = DataArrayIndex;
            int DataArrayCurrentPosition = DataArrayIndex;
            int StartSmoothPosition = DataArrayIndex;

            int FinishSmoothPosition = EndOfDataArray;

            int LowerDataArrayBoundary = DataArrayIndex;

            // std::vector.end points to just beyond the last element
            int UpperDataArrayBoundary = FinishSmoothPosition;

            int SmoothArea = smoothFactor / 2;

            int cnt = 0, counter = 0;
            int LowerIndex = 0, UpperIndex = 0;

            double Weighting = 0;
            double Value = 0;
            double TempDouble = 0;

            bool HitBoundaryLowerLimit = false;
            bool HitBoundaryUpperLimit = false;

            while (DataArrayIndex != EndOfDataArray)
            {
                // Initialise our counters and markers
                DataArrayCounter = DataArrayIndex;
                StartSmoothPosition = DataArrayIndex;
                FinishSmoothPosition = DataArrayIndex;

                // Test lower boundary
                for (HitBoundaryLowerLimit = false, cnt = 0, LowerIndex = SmoothArea;
                    cnt < SmoothArea; cnt++)
                {
                    if (DataArrayCounter != LowerDataArrayBoundary)
                    {
                        DataArrayCounter--;
                        StartSmoothPosition--;
                        LowerIndex--;
                    }
                    else
                        HitBoundaryLowerLimit = true;
                }

                // reset counter and test upper boundary
                DataArrayCounter = DataArrayIndex;
                for (HitBoundaryUpperLimit = false, cnt = 0, UpperIndex = SmoothArea;
                    cnt < SmoothArea; cnt++)
                {
                    if (DataArrayCounter != UpperDataArrayBoundary)
                    {
                        DataArrayCounter++;
                        FinishSmoothPosition++;
                        UpperIndex--;
                    }
                    else
                        HitBoundaryUpperLimit = true;
                }


                DataArrayCounter = StartSmoothPosition;
                Weighting = 0;
                Value = 0;

                if (HitBoundaryUpperLimit)
                    counter = 0;
                else
                    counter = LowerIndex;

                // Hurrah, we can now start to smooth
                while (DataArrayCounter <= FinishSmoothPosition)
                {
                    Value += (Data[DataArrayCounter] * PascalTriangle(smoothFactor - 1, counter));
                    Weighting += PascalTriangle(smoothFactor - 1, counter);
                    DataArrayCounter++;
                    counter++;
                }

                filtered_x_out.Add(Value / Weighting);

                DataArrayIndex++;

            } // End of while



        }

        public static double PascalTriangle(int RowNumber, int Element)
        {
            double Value = 0;

            // preconditions
            if (RowNumber == 1 || RowNumber == 2)
            {
                Value = 1;

                return (Value);
            }

            Value = factorial(RowNumber) / (factorial(Element) * factorial(RowNumber - Element));

            return (Value);
        }

        public static int factorial(int n)
        {
            int product = 1;

            if (n <= 1)
                return (product);

            for (; n > 1; --n)
                product *= n;

            return product;
        }


        public static void linearFit(List<double> x_in, List<double> y_in, ref double slope, ref double intercept)
        {
            // least squares linear regression
            // Given equal length vectors x and y
            // x is assumed noiseless and
            // y is to have normally distributed noise
            // A linear approximation is fitted to the data
            // y = slope*x + intercept
            slope = 0;
            intercept = 0;

            int N = x_in.Count;
            if (N > 1 && N == y_in.Count)
            { // data looks okay

                double mean_x = meanOfValues(x_in);
                double mean_y = meanOfValues(y_in);

                double S_xx = 0;
                double S_xy = 0;
                for (int i = 0; i < N; i++)
                {
                    S_xx += (x_in[i] - mean_x) * (x_in[i] - mean_x);
                    S_xy += (x_in[i] - mean_x) * (y_in[i] - mean_y);
                }

                slope = S_xy / S_xx;
                intercept = mean_y - slope * mean_x;
            }
            else
            { // data is no good
            }
        }

        public static void distancesFromPointsToLine(List<double> x_in, List<double> y_in, ref double slope, ref double intercept,
         List<double> distances)
        {
            // Calculate the distance from each point to a line

            distances.Clear();
            int N = x_in.Count;
            if (N > 1 && N == y_in.Count)
            { // data looks okay
                // find slope of perdendicular line through point
                double slope_p = Math.Tan(Math.Atan(slope) - M_PI_2);
                for (int i = 0; i < N; i++)
                {
                    if (slope == 0)
                    { // line is parallel to x-axis
                        distances.Add(y_in[i] - intercept);
                    }
                    else
                    {
                        // find intercept of perdendicular line through each point
                        double intercept_p = y_in[i] - slope_p * x_in[i];

                        // find intersection of perdendicular lines
                        double x_on_line = (intercept - intercept_p) / (slope_p - slope);
                        double y_on_line = slope * x_on_line + intercept;
                        double distance =Math.Sqrt(
                            (x_in[i] - x_on_line) * (x_in[i] - x_on_line)
                          + (y_in[i] - y_on_line) * (y_in[i] - y_on_line));

                        distances.Add(distance);
                    }
                }

            }
        }


        public static double meanOfValues(List<double> values)
        {
            double mean = 0;
            uint count = 0;
            int iter = 0;
            while (iter < values.Count)
            {
                mean += values[iter];
                iter++;
                count++;
            }
            if (count > 1)
            {
                mean /= count;
            }
            return mean;
        }

        public static bool LeastSqFit(double[] x, double[] y, int fromPt, int toPt, out double yIntercept, out double slope)
        {

            yIntercept = 0;
            slope = 0;
            int i;
            double xBar, yBar, aBar, bBar;	 /* mean x and y */

            xBar = yBar = aBar = bBar = 0.0;

            if (fromPt > toPt)
                return (false);
            else
            {
                for (i = fromPt; i <= toPt; i++)
                {
                    xBar += x[i];
                    yBar += y[i];
                }

                xBar /= (toPt - fromPt + 1);
                yBar /= (toPt - fromPt + 1);

                for (i = fromPt; i <= toPt; i++)
                {
                    aBar += (x[i] - xBar) * (y[i] - yBar);
                    bBar += (x[i] - xBar) * (x[i] - xBar);
                }

                slope = aBar / bBar;
                yIntercept = yBar - (slope) * xBar;
            }
            return (true);
        }
       

    }
}

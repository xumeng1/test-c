using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Bookham.TestLibrary.Utilities;
// coded by purney.xie
namespace Bookham.fcumapping.CommonData
{
    class CCurrentsVector
    {
        #region data members
        public List<double> _currents = new List<double>();
        long _length;

        string _abs_filepath;
        #endregion

        public CCurrentsVector()
        {
            clear();
        }

        public void clear()
        {
            _currents.Clear();
            _length = 0;
            _abs_filepath = null;
        }

        public bool isEmpty()
        {
            bool rval = false;

            if (_currents.Count == 0)
            {
                rval = true;
            }

            return rval;
        }


        public void writeToFile(string abs_filepath, bool overwrite)
        {

        }
        public void readFromFile(string abs_filepath)
        {
            clear();
            if (File.Exists(abs_filepath))
            {
                CsvReader csvReader = new CsvReader();
                List< string[]> result = csvReader.ReadFile(abs_filepath);
                for (int i = 0; i < result.Count; i++)
                {
                    _currents.Add( Convert.ToDouble(result[i][0]));
                }
                _length = _currents.Count;
                _abs_filepath = abs_filepath;
            }
            else
                throw new Exception(string.Format("{0} not exist!",abs_filepath));
            

        }
    }
}

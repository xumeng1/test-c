using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.fcumapping.CommonData
{
    public class ModeBoundaryLine
    {
        #region data structs

        private  const double M_PI=3.14159265358979323846;
        public List<double> _x = new List<double>();
        public List<double> _y = new List<double>();
        public bool _crosses_front_sections;
        public List<double> _slopes = new List<double>();

        public Vector _modalDistortionAngles = new Vector();
        public double _maxModalDistortionAngle;
        public double _maxModalDistortionAngleXPosition;


        public Vector _modeBDCAreas = new Vector();
        public double _maxModeBDCArea;
        public double _minModeBDCArea;
        public double _meanModeBDCArea;
        public double _sumModeBDCAreas;
        public double _numModeBDCAreas;
        public double _stdDevModeBDCArea;
        public double _varModeBDCArea;

        public Vector _modeBDCAreaXLengths = new Vector();
        public double _maxModeBDCAreaXLength;
        public double _minModeBDCAreaXLength;
        public double _meanModeBDCAreaXLength;
        public double _sumModeBDCAreaXLengths;
        public double _numModeBDCAreaXLengths;
        public double _stdDevModeBDCAreaXLength;
        public double _varModeBDCAreaXLength;

        public bool _modeBDCAreaAnalysisRun;
        #endregion

        public ModeBoundaryLine (){}
        public ModeBoundaryLine (List<double> x,List<double> y, bool crosses_front_sections)
        {
            this._x = x;
            this._y = y;
            _crosses_front_sections = crosses_front_sections;
        }
        private double getSlopeInDegrees(double x1,double y1,double x2,double y2)
        {
            double slopeInDegrees = 0;

            double slope = 0;
            double atanOfSlope = 0;
            double piOver180 = 180 / M_PI;

            if (x2 == x1)
            { // avoid division by zero
                if (y2 == y1) slopeInDegrees = 0; // points are the same
                else if (y2 < y1) slopeInDegrees = -90;
                else slopeInDegrees = 90;
            }
            else if (x2 > x1)
            {
                if (y2 == y1) slopeInDegrees = 0;
                else if (y2 > y1)
                {
                    slope = (y2 - y1) / (x2 - x1);
                    atanOfSlope = Math.Atan(slope);
                    slopeInDegrees = atanOfSlope * piOver180;
                }
                else // y1 > y2
                {
                    slope = (y1 - y2) / (x2 - x1);
                    atanOfSlope = Math.Atan(slope);
                    slopeInDegrees = -atanOfSlope * piOver180;
                }
            }
            else // x2 < x1 
            {
                if (y2 == y1) slopeInDegrees = 180;
                else if (y2 > y1)
                {
                    slope = (y2 - y1) / (x1 - x2);
                    atanOfSlope = Math.Atan(slope);
                    slopeInDegrees = 180 - atanOfSlope * piOver180;
                }
                else // y1 > y2
                {
                    slope = (y1 - y2) / (x1 - x2);
                    atanOfSlope = Math.Atan(slope);
                    slopeInDegrees = -(180 - atanOfSlope * piOver180);
                }
            }

            return slopeInDegrees;
        }

        public int numXPoints()
        {
            return _x.Count;
        }
        public int numYPoints()
        {
            return _y.Count;
        }

        public double x(int index)
        {
            if (index >= 0
                && index < (int)_x.Count)
                return _x[index];
            else
                return 0;
        }
        public double y(int index)
        {
            if (index >= 0
                && index < (int)_y.Count)
                return _y[index];
            else
                return 0;
        }
        public  List<double> getSlopes(int slopeWindow)
        {
            if (slopeWindow <= 0)
                slopeWindow = 1;
            if (_slopes.Count == 0)
            {
                // Iterate through each pair of points on this line
                // starting with the first two points and calculate
                // slope IN DEGREES.
                //
                // Originally the actual slopes where calculated but we
                // run into difficulties where the angle becomes greater
                // than 90 degrees in relation to the horizon. This can
                // occur where a line folds back on itself. Slopes are 
                // calculated by dividing deltaX into deltaY but these
                // will tend towards infinity when the angle becomes greater
                // than 90 degrees. Therefore angles are used instead.

                // check we actually have x and y
                // values in this line
                if ((numXPoints() > 0) &&
                    (numYPoints() > 0))
                {
                    double x1 = 0;
                    double x2 = 0;
                    double y1 = 0;
                    double y2 = 0;

                    for (int i = 0; i < numXPoints(); i++)
                    {
                        // don't want to access
                        // the vector with an invalid
                        // index so check that this 
                        // point pair is valid i.e.
                        // we have a y for our x, and
                        // that the next point is valid
                        // as well
                        double slopeInDegrees = 0;

                        if (((i + slopeWindow) < (int)_x.Count))
                        {
                            // slope = y2-y1/x2-x1
                            //int y_coord
                            //    = (int)((_x[i])/_xAxisLength);
                            //int x_coord
                            //    = (_x[i]) - y_coord*_xAxisLength;

                            x1 = (double)_x[i];
                            y1 = (double)_y[i];

                            //y_coord
                            //    = (int)((_x[i+slopeWindow])/_xAxisLength);
                            //x_coord
                            //    = (_x[i+slopeWindow]) - y_coord*_xAxisLength;

                            x2 = (double)_x[i + slopeWindow];
                            y2 = (double)_y[i + slopeWindow];

                            slopeInDegrees = getSlopeInDegrees(x1, y1, x2, y2);

                            _slopes.Add(slopeInDegrees);

                        }//if (((i + slopeWindow) < (int)_x.Count))
                    }//for (int i = 0; i < numXPoints(); i++)
                }//if ((numXPoints() > 0) && (numYPoints() > 0))
            }//if (_slopes.Count == 0)
            return _slopes;
        }

        public List<double> xPoints()
        {
            return _x;
        }
        public List<double> yPoints()
        {
            return _y;
        }

        public void runModalDistortionAnalysis(double modalDistortionMinX,
                                    double modalDistortionMaxX)
        {
            // error check
             if (modalDistortionMinX < modalDistortionMaxX)
            {
                double firstX = x(0);
                double firstY = y(0);
                double lastX = x(numXPoints() - 1);
                double lastY = y(numYPoints() - 1);

                double slopeA = 0;
                double slopeB = 0;

                double modalDistortionAngle = 0;

                _maxModalDistortionAngle = 0;
                _maxModalDistortionAngleXPosition = 0;

                double maxXValue = firstX;

                for (int i = 1; i < numXPoints(); i++)
                {
                    if (x(i) > maxXValue) // x must be increasing
                    {
                        maxXValue = x(i);

                        if ((x(i) >= modalDistortionMinX) && (x(i) <= modalDistortionMaxX))
                        {
                            slopeA = getSlopeInDegrees(firstX, firstY, x(i), y(i));
                            slopeB = getSlopeInDegrees(x(i), y(i), lastX, lastY);

                            modalDistortionAngle = slopeA - slopeB;

                            if ((modalDistortionAngle >= 0) && (modalDistortionAngle <= 90))
                            {
                                _modalDistortionAngles.Add(modalDistortionAngle);

                                if (modalDistortionAngle > _maxModalDistortionAngle)
                                {
                                    _maxModalDistortionAngle = modalDistortionAngle;
                                    _maxModalDistortionAngleXPosition = x(i);
                                }//if (modalDistortionAngle > _maxModalDistortionAngle)
                            }//if ((modalDistortionAngle >= 0) && (modalDistortionAngle <= 90))
                        }//if ((x(i) >= modalDistortionMinX) && (x(i) <= modalDistortionMaxX))
                    }//if (x(i) > maxXValue)
                }//for (int i = 1; i < numXPoints(); i++)
            }//if (modalDistortionMinX < modalDistortionMaxX)
        }

        public double getMaxModalDistortionAngle()
        {
            return _maxModalDistortionAngle;;
        }
        public double getMaxModalDistortionAngleXPosition()
        {
            return _maxModalDistortionAngleXPosition;
        }

        public void runModeBDCAreaAnalysis()
        {
            if (_modeBDCAreas.size()==0)
            {
                _maxModeBDCAreaXLength = 0;
                _maxModeBDCArea = 0;
                _sumModeBDCAreas = 0;
                _numModeBDCAreas = 0;
                int maxX = 0;
                int yAtMax = 0;
                bool inBDCArea = false;
                
                for(int i=0;i<numXPoints();i++)
                {
                    List<int> bdcXPoints = new List<int>();
                    List<int> bdcYPoints = new List<int>();
                    if (x(i)>maxX)
                    {
                        maxX = (int)x(i);
                        yAtMax = (int)y(i);
                        if (inBDCArea)
                        {
                            //found end of bdc are
                            inBDCArea = false;
                            #region BDC area
                            if (bdcYPoints.Count>1)
                            {
                                // calculate area of bdc
                                int i_zx = 0;
                                int i_zy = 0;
                                List<int> x_bdc_lengths = new List<int>();
                                //x_bdc_lengths.Clear();
                                for (int j = 0; j <= (yAtMax - bdcYPoints[0]); j++)
                                    x_bdc_lengths.Add(0);
                                while (i_zx != bdcXPoints.Count && i_zy!=bdcYPoints.Count)
                                {
                                    // find longest x-length for each y in bdc
                                    int y_pos = bdcYPoints[i_zy] - bdcYPoints[0];
                                    int x_bdc_length = bdcXPoints[0] - bdcXPoints[i_zx] + 1;
                                    if (y_pos>=0 && y_pos<(int)(x_bdc_lengths.Count))
                                    {
                                        if (x_bdc_length>x_bdc_lengths[y_pos])
                                        {
                                            x_bdc_lengths[y_pos] = x_bdc_length;
                                        }
                                    }
                                    i_zx++;
                                    i_zy++;
                                }//while (i_zx != bdcXPoints.Count && i_zy!=bdcYPoints.Count)

                                // calculate area
                                double area = 0;
                                for(int j=0;j<(int)(x_bdc_lengths.Count);j++)
                                    area += (double)(x_bdc_lengths[j]);
                                if (area>_maxModeBDCArea)
                                {
                                    _maxModeBDCArea = area;
                                }
                                _sumModeBDCAreas += area;
                                _modeBDCAreas.Add(area);
                                _modeBDCAreaXLengths.Add(_maxModeBDCAreaXLength);
                            }
                            #endregion
                        }
                    }//if(x(i)>maxX)

                    if (x(i)<maxX && !inBDCArea)
                    {
                        //found the start of a new bdc
                        inBDCArea = true;
                        _numModeBDCAreas += 1;
                        //bdcXPoints.Clear();
                        //bdcYPoints.Clear();
                        bdcXPoints.Add(maxX);
                        bdcYPoints.Add(yAtMax);
                    }
                    if (inBDCArea)
                    {
                        //add point to collection of bdc points
                        bdcXPoints.Add((int)x(i));
                        bdcYPoints.Add((int)y(i));
                    }

                    if ((maxX-x(i))>_maxModeBDCArea)
                    {
                        _maxModeBDCArea = maxX - x(i);
                    }
                    if ((maxX - x(i)) > _maxModeBDCAreaXLength)
                        _maxModeBDCAreaXLength = maxX - x(i);
                }//for(int i=0;i<numXPoints();i++)
            }//if (_modeBDCAreas.size()==0)
            _modeBDCAreaAnalysisRun = true;
        }


        public Vector getModeBDCAreas()
        {
            if (!_modeBDCAreaAnalysisRun)
                runModeBDCAreaAnalysis();
            return _modeBDCAreas;
        }
        public double getMinModeBDCArea()
        {
            if(!_modeBDCAreaAnalysisRun)
                runModeBDCAreaAnalysis();
            return _modeBDCAreas.getVectorAttribute(VectorAttribute.min);
        }
        public double getMaxModeBDCArea()
        {
            if (!_modeBDCAreaAnalysisRun)
                runModeBDCAreaAnalysis();
            return _maxModeBDCArea;
        }
        public double getMeanModeBDCAreas()
        {
            if(!_modeBDCAreaAnalysisRun)
                runModeBDCAreaAnalysis();
            return _modeBDCAreas.getVectorAttribute(VectorAttribute.mean);
        }
        public double getSumModeBDCAreas()
        {
            if (!_modeBDCAreaAnalysisRun)
                runModeBDCAreaAnalysis();
            return _sumModeBDCAreas;
        }
        public double getNumModeBDCAreas()
        {
            if (!_modeBDCAreaAnalysisRun)
                runModeBDCAreaAnalysis();
            return _numModeBDCAreas;
        }
        public double getStdDevModeBDCAreas()
        {
            if(!_modeBDCAreaAnalysisRun)
                runModeBDCAreaAnalysis();
            return _modeBDCAreas.getVectorAttribute(VectorAttribute.stdDev);
        }
        public double getVarModeBDCAreas()
        {
            if(!_modeBDCAreaAnalysisRun)
                runModeBDCAreaAnalysis();
            return _modeBDCAreas.getVectorAttribute(VectorAttribute.variance);
        }


        public Vector getModeBDCAreaXLengths()
        {
            if (!_modeBDCAreaAnalysisRun)
                runModeBDCAreaAnalysis();

            return _modeBDCAreaXLengths;
        }
        public double getMaxModeBDCAreaXLength()
        {
            if (!_modeBDCAreaAnalysisRun)
                runModeBDCAreaAnalysis();

            return _maxModeBDCAreaXLength;
        }
        public double getMinModeBDCAreaXLength()
        {
            if(!_modeBDCAreaAnalysisRun)
                runModeBDCAreaAnalysis();
            return _modeBDCAreaXLengths.getVectorAttribute(VectorAttribute.min);
        }
        public double getMeanModeBDCAreaXLengths()
        {
            if(!_modeBDCAreaAnalysisRun)
                runModeBDCAreaAnalysis();

            return _modeBDCAreaXLengths.getVectorAttribute(VectorAttribute.mean);
        }
        public double getSumModeBDCAreaXLengths()
        {
            if (!_modeBDCAreaAnalysisRun)
                runModeBDCAreaAnalysis();

            return _sumModeBDCAreaXLengths;
        }
        public double getNumModeBDCAreaXLengths()
        {
            if (!_modeBDCAreaAnalysisRun)
                runModeBDCAreaAnalysis();

            return _numModeBDCAreaXLengths;
        }
        public double getStdDevModeBDCAreaXLengths()
        {
            if(!_modeBDCAreaAnalysisRun)
                runModeBDCAreaAnalysis();
            return _modeBDCAreaXLengths.getVectorAttribute(VectorAttribute.stdDev);
        }
        public double getVarModeBDCAreaXLengths()
        {
            if(!_modeBDCAreaAnalysisRun)
                runModeBDCAreaAnalysis();
            return _modeBDCAreaXLengths.getVectorAttribute(VectorAttribute.variance);
        }

    }//public class ModeBoundaryLine
}//namespace Bookham.fcumapping.CommonData

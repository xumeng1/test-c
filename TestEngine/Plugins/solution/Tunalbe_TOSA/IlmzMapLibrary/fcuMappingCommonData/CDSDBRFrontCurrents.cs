using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Bookham.TestLibrary.Utilities;

namespace Bookham.fcumapping.CommonData
{
    public sealed class CDSDBRFrontCurrents
    {
        public CDSDBRFrontCurrents(){}

        public class If_Point
        {
            public short front_pair_number;
            public double constantCurrent;
            public double nonConstantCurrent;
        }
        public short pair_number(int index)
        {
            return  _currents[index].front_pair_number;
        }
        public void writeToFile(string abs_filepath,bool overwrite)
        {

        }

        public double getConstantCurrent( short pair_num, long sub_index )
        {
	        double constant_current = 0;
	        long index = _submap_start_index[pair_num-1] + sub_index;
            constant_current = _currents[(int)index].constantCurrent;
	        return constant_current;
        }

        public double getNonConstantCurrent(short pair_num, long sub_index)
        {
            double non_constant_current = 0;
            long index = _submap_start_index[pair_num - 1] + sub_index;


            non_constant_current = _currents[(int)index].nonConstantCurrent;


            return non_constant_current;
        }

        public int index_in_submap(int index)
        {
            return index - _submap_start_index[pair_number(index) - 1];
        }
        public void readFromFile(string abs_filepath)
        {
            if (File.Exists(abs_filepath))
            {
                List<double> temp = new List<double>();
                CsvReader csvReader = new CsvReader();

                List<string[]> result = csvReader.ReadFile(abs_filepath);
                for (int i = 0; i < result.Count; i++)
                {
                    temp.Add(Convert.ToDouble(result[i][0]));
                }
                for (int i = 0; i < 7; i++)
                {
                    int frontPair = i + 1;
                    _submap_start_index.Add(_currents.Count);
                    int count = 0;
                    for (int j = 0; j < temp.Count; j++)
                    {
                        If_Point aPoint = new If_Point();
                        aPoint.front_pair_number = (short)frontPair;
                        aPoint.constantCurrent = 5.0;
                        aPoint.nonConstantCurrent = temp[j];
                        _currents.Add(aPoint);
                        count++;
                    }
                    _submap_length.Add(temp.Count);
                }
                _total_length = _currents.Count;
                _abs_filepath = abs_filepath;
            }
            else
                throw new Exception(string.Format("{0} not exist!", abs_filepath));
        }

        public void clear()
        {
            _currents.Clear();
            _total_length = 0;
            _abs_filepath = null;
            _submap_length.Clear();
            _submap_start_index.Clear();
        }

        public bool isEmpty()
        {
            bool returnValue = false;
            if (_currents.Count == 0)
            {
                returnValue = true;
            }
            return returnValue;
        }

        public long getLength()
        {
            return _currents.Count;
        }

        public long _total_length;
        public List<int> _submap_length = new List<int>();
        public List<int> _submap_start_index = new List<int>();
        public List<If_Point> _currents = new List<If_Point>();
        public string _abs_filepath;
    }
}

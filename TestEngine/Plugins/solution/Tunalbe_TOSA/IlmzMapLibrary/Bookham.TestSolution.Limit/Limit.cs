using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.Framework.InternalData;
using System.Data;
using System.Reflection;

namespace Bookham.TestSolution.Limit
{
    #region LimitParameters

    internal interface ILimitParameter
    {
        /// <summary>
        /// Set parameters value
        /// </summary>
        /// <param name="nValue"></param>
        void SetValue(int intValue);
        /// <summary>
        /// Convert parameters to DatumList format
        /// </summary>
        /// <returns></returns>
        DatumList ToDatumList();
        /// <summary>
        /// Get test result
        /// </summary>
        /// <returns></returns>
        TestDetails GetTestStatus();
        /// <summary>
        /// Get parameters name
        /// </summary>
        /// <returns></returns>
        string[] GetLimitParametersName();
        /// <summary>
        /// output pcas drop file string ,not include the key item
        /// </summary>
        /// <returns></returns>
        string GeneratePcasDropFile();
        /// <summary>
        /// output pcas drop file string,include the key item
        /// </summary>
        /// <param name="DeviceType">DeviceType name</param>
        /// <param name="TestStage">Stage name</param>
        /// <param name="Specification">Specification name</param>
        /// <param name="SerialNumber">DUT Serial Number</param>
        /// <returns></returns>
        string GeneratePcasDropFile(string DeviceType, string TestStage,
                                    string Specification, string SerialNumber);
    }

    public abstract class LimitParameter : ILimitParameter
    {

        protected LimitParameter()
        {
            Initialize(false, 0);
        }
        protected LimitParameter(int nValue)
        {
            SetValue(nValue);
        }
        /// <summary>
        /// A name of a set of limit parameters
        /// </summary>
        public string Name
        {
            get
            {
                Type T = this.GetType();
                return T.FullName;
            }
        }
        protected void Initialize(bool SetValue, int nValue)
        {
            object obj = this;
            Type T = this.GetType();
            FieldInfo[] FI = T.GetFields(BindingFlags.Instance | BindingFlags.Public);
            foreach (FieldInfo var in FI)
            {
                if (Object.ReferenceEquals(var.FieldType, typeof(string)))
                { if (SetValue) var.SetValue(obj, "Missing"); }
                else if (Object.ReferenceEquals(var.FieldType, typeof(double)))
                { if (SetValue) var.SetValue(obj, nValue); }
                else if (Object.ReferenceEquals(var.FieldType, typeof(int)))
                { if (SetValue) var.SetValue(obj, nValue); }
                else if (Object.ReferenceEquals(var.FieldType, typeof(DatumFileLink)))
                { if (SetValue) var.SetValue(obj, null); }
                else if (Object.ReferenceEquals(var.FieldType, typeof(LimitParameterType<double>)))
                {
                    LimitParameterType<double> dbl = new LimitParameterType<double>();
                    if (SetValue) dbl.Val = nValue;
                    var.SetValue(obj, dbl);
                }
                else if (Object.ReferenceEquals(var.FieldType, typeof(LimitParameterType<int>)))
                {
                    LimitParameterType<int> num = new LimitParameterType<int>();
                    if (SetValue) num.Val = nValue;
                    var.SetValue(obj, num);
                }
                else
                    throw new Exception("Unknown data type.");
            }
        }
        /// <summary>
        /// Set parameters value
        /// </summary>
        /// <param name="nValue"></param>
        public void SetValue(int nValue)
        {
            Initialize(true, nValue);
        }
        /// <summary>
        /// Convert parameters to DatumList format
        /// </summary>
        /// <returns></returns>
        public DatumList ToDatumList()
        {
            object obj = this;
            DatumList dl = new DatumList();
            Type T = this.GetType();
            FieldInfo[] FI = T.GetFields(BindingFlags.Instance | BindingFlags.Public);
            foreach (FieldInfo var in FI)
            {
                if (Object.ReferenceEquals(var.FieldType, typeof(string)))
                {
                    string str = (string)var.GetValue(obj);
                    if (str == null)
                        dl.AddString(var.Name, "Missing");
                    else
                        dl.AddString(var.Name, str);
                }
                else if (Object.ReferenceEquals(var.FieldType, typeof(double)))
                    dl.AddDouble(var.Name, (double)var.GetValue(obj));
                else if (Object.ReferenceEquals(var.FieldType, typeof(int)))
                    dl.AddSint32(var.Name, (int)var.GetValue(obj));
                else if (Object.ReferenceEquals(var.FieldType, typeof(DatumFileLink)))
                {
                    DatumFileLink dfl = (DatumFileLink)var.GetValue(obj);
                    if (dfl != null)
                        dl.AddFileLink(var.Name, dfl.GetDirectoryPath(), dfl.GetFileName());
                }
                else if (Object.ReferenceEquals(var.FieldType, typeof(LimitParameterType<double>)))
                {
                    LimitParameterType<double> dbl = (LimitParameterType<double>)var.GetValue(obj);
                    if (dbl != null && dbl.Val.HasValue)
                        dl.AddDouble(var.Name, dbl.Val.Value);
                }
                else if (Object.ReferenceEquals(var.FieldType, typeof(LimitParameterType<int>)))
                {
                    LimitParameterType<int> dbl = (LimitParameterType<int>)var.GetValue(obj);
                    if (dbl != null && dbl.Val.HasValue)
                        dl.AddSint32(var.Name, dbl.Val.Value);
                }
                else
                    throw new Exception("Unknown data type.");
            }
            //if (dl.Count == 0) return null;
            return dl;
        }
        /// <summary>
        /// Get test result
        /// </summary>
        /// <returns></returns>
        public TestDetails GetTestStatus()
        {
            TestDetails td = new TestDetails();
            td.TestResult = TestStatus.Pass;
            DataTable table = new DataTable("result");
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Min", typeof(string));
            table.Columns.Add("Value", typeof(string));
            table.Columns.Add("Max", typeof(string));
            table.Columns.Add("TestStatus", typeof(string));

            object obj = this;
            Type T = this.GetType();
            FieldInfo[] FI = T.GetFields(BindingFlags.Instance | BindingFlags.Public);
            foreach (FieldInfo var in FI)
            {
                DataRow dr = table.NewRow();
                dr["Name"] = var.Name;
                if (Object.ReferenceEquals(var.FieldType, typeof(string)))
                {
                    string str = (string)var.GetValue(obj);
                    if (str == null)
                        dr["Value"] = "Missing";
                    else
                        dr["Value"] = str;
                }
                else if (Object.ReferenceEquals(var.FieldType, typeof(double)))
                    dr["Value"] = (double)var.GetValue(obj);
                else if (Object.ReferenceEquals(var.FieldType, typeof(int)))
                    dr["Value"] = (int)var.GetValue(obj);
                else if (Object.ReferenceEquals(var.FieldType, typeof(DatumFileLink)))
                {
                    DatumFileLink dfl = (DatumFileLink)var.GetValue(obj);
                    if (dfl != null)
                        dr["Value"] = dfl.GetFileName();
                }
                else if (Object.ReferenceEquals(var.FieldType, typeof(LimitParameterType<double>)))
                {
                    LimitParameterType<double> dbl = (LimitParameterType<double>)var.GetValue(obj);
                    if (dbl != null)
                    {
                        if (dbl.ParameterIsOk == TestStatus.Fail) td.TestResult = TestStatus.Fail;
                        dr["Min"] = dbl.Min;
                        dr["Value"] = dbl.Val;
                        dr["Max"] = dbl.Max;
                        dr["TestStatus"] = dbl.ParameterIsOk.ToString();
                    }
                }
                else if (Object.ReferenceEquals(var.FieldType, typeof(LimitParameterType<int>)))
                {
                    LimitParameterType<int> dbl = (LimitParameterType<int>)var.GetValue(obj);
                    if (dbl != null)
                    {
                        if (dbl.ParameterIsOk == TestStatus.Fail) td.TestResult = TestStatus.Fail;
                        dr["Min"] = dbl.Min;
                        dr["Value"] = dbl.Val;
                        dr["Max"] = dbl.Max;
                        dr["TestStatus"] = dbl.ParameterIsOk.ToString();
                    }
                }
                else
                    throw new Exception("Unknown data type.");
                table.Rows.Add(dr);
            }
            td.TestDetailsTable = table;
            return td;
        }
        /// <summary>
        /// Get parameters name
        /// </summary>
        /// <returns></returns>
        public string[] GetLimitParametersName()
        {
            List<string> pname = new List<string>();
            Type T = this.GetType();
            FieldInfo[] FI = T.GetFields(BindingFlags.Instance | BindingFlags.Public);
            foreach (FieldInfo var in FI)
                pname.Add(var.Name);
            return pname.ToArray();
        }
        /// <summary>
        /// output pcas drop file string ,not include the key item
        /// </summary>
        /// <returns></returns>
        public string GeneratePcasDropFile()
        {
            const string fmt = "${0}$0";
            StringBuilder sb = new StringBuilder();
            object obj = this;
            Type T = this.GetType();
            FieldInfo[] FI = T.GetFields(BindingFlags.Instance | BindingFlags.Public);
            foreach (FieldInfo var in FI)
            {
                string val = null;
                if (Object.ReferenceEquals(var.FieldType, typeof(string)))
                {
                    string str = (string)var.GetValue(obj);
                    if (str == null)
                        val = "Missing";
                    else
                        val = str;
                }
                else if (Object.ReferenceEquals(var.FieldType, typeof(double)))
                    val = ((double)var.GetValue(obj)).ToString();
                else if (Object.ReferenceEquals(var.FieldType, typeof(int)))
                    val = ((int)var.GetValue(obj)).ToString();
                else if (Object.ReferenceEquals(var.FieldType, typeof(DatumFileLink)))
                {
                    DatumFileLink dfl = (DatumFileLink)var.GetValue(obj);
                    if (dfl != null)
                        val = dfl.GetFileName();
                }
                else if (Object.ReferenceEquals(var.FieldType, typeof(LimitParameterType<double>)))
                {
                    LimitParameterType<double> dbl = (LimitParameterType<double>)var.GetValue(obj);
                    val = dbl != null && dbl.Val.HasValue ? dbl.Val.Value.ToString() : null;
                }
                else if (Object.ReferenceEquals(var.FieldType, typeof(LimitParameterType<int>)))
                {
                    LimitParameterType<int> dbl = (LimitParameterType<int>)var.GetValue(obj);
                    val = dbl != null && dbl.Val.HasValue ? dbl.Val.Value.ToString() : null;
                }
                else
                    throw new Exception("Unknown data type.");
                if (val != null)
                {
                    sb.AppendFormat(fmt, var.Name);
                    sb.AppendLine();
                    sb.AppendLine(val);
                }
            }
            return sb.ToString();
        }
        /// <summary>
        /// output pcas drop file string,include the key item
        /// </summary>
        /// <param name="DeviceType">DeviceType name</param>
        /// <param name="TestStage">Stage name</param>
        /// <param name="Specification">Specification name</param>
        /// <param name="SerialNumber">DUT Serial Number</param>
        /// <returns></returns>
        public string GeneratePcasDropFile(string DeviceType, string TestStage,
            string Specification, string SerialNumber)
        {
            StringBuilder sb = new StringBuilder();
            //add key item
            sb.AppendLine("~DEVICE TYPE");
            sb.AppendLine(DeviceType.Trim());
            sb.AppendLine("~STAGE");
            sb.AppendLine(TestStage.Trim());
            sb.AppendLine("~SPECIFICATION");
            sb.AppendLine(Specification.Trim());
            sb.AppendLine("~SERIAL NUMBER");
            sb.AppendLine(SerialNumber.Trim());
            //add test result
            sb.Append(GeneratePcasDropFile());
            return sb.ToString();
        }


    }

    public class LimitsManager : ILimitParameter
    {
        List<LimitParameter> limits = new List<LimitParameter>();
        /// <summary>
        /// clear all items
        /// </summary>
        public void Clear()
        {
            limits.Clear();
        }
        /// <summary>
        /// Add a LimitParameter by LimitParameter.name
        /// </summary>
        /// <param name="limitPara"></param>
        public void Add(LimitParameter limitPara)
        {
            if (GetLimitByName(limitPara.Name) == null)
                limits.Add(limitPara);
        }
        /// <summary>
        /// Remove a LimitParameter by LimitParameter.name
        /// </summary>
        /// <param name="Name"></param>
        public void Remove(string Name)
        {
            LimitParameter limitPara = GetLimitByName(Name);
            if (limitPara != null)
                limits.Remove(limitPara);
        }
        protected LimitParameter GetLimitByName(string Name)
        {
            foreach (LimitParameter var in limits)
            {
                if (var.Name == Name)
                    return var;
            }
            return null;
        }

        #region ILimitParameter Members
        /// <summary>
        /// Set parameters value
        /// </summary>
        /// <param name="nValue"></param>
        public void SetValue(int nValue)
        {
            foreach (LimitParameter var in limits)
            {
                var.SetValue(nValue);
            }
        }
        /// <summary>
        /// Convert parameters to DatumList format
        /// </summary>
        /// <returns></returns>
        public DatumList ToDatumList()
        {
            DatumList DL = new DatumList();
            foreach (LimitParameter var in limits)
            {
                DL.AddListItems(var.ToDatumList());
            }
            return DL;
        }
        /// <summary>
        /// Get test result
        /// </summary>
        /// <returns></returns>
        public TestDetails GetTestStatus()
        {
            TestDetails td = null, tmp = null;
            foreach (LimitParameter var in limits)
            {
                tmp = var.GetTestStatus();
                if (td == null)
                    td = tmp;
                else
                {
                    if (td.TestResult == TestStatus.Pass) td.TestResult = tmp.TestResult;
                    td.TestDetailsTable.Merge(tmp.TestDetailsTable);
                }
            }
            return td;
        }
        /// <summary>
        /// Get parameters name
        /// </summary>
        /// <returns></returns>
        public string[] GetLimitParametersName()
        {
            List<string> lst = new List<string>();
            foreach (LimitParameter var in limits)
            {
                lst.AddRange(var.GetLimitParametersName());
            }
            return lst.ToArray();
        }
        /// <summary>
        /// output pcas drop file string ,not include the key item
        /// </summary>
        /// <returns></returns>
        public string GeneratePcasDropFile()
        {
            StringBuilder sb = new StringBuilder();
            foreach (LimitParameter var in limits)
            {
                sb.Append(var.GeneratePcasDropFile());
            }
            return sb.ToString();
        }
        /// <summary>
        /// output pcas drop file string,include the key item
        /// </summary>
        /// <param name="DeviceType">DeviceType name</param>
        /// <param name="TestStage">Stage name</param>
        /// <param name="Specification">Specification name</param>
        /// <param name="SerialNumber">DUT Serial Number</param>
        /// <returns></returns>
        public string GeneratePcasDropFile(string DeviceType, string TestStage,
            string Specification, string SerialNumber)
        {
            StringBuilder sb = new StringBuilder();
            //add key item
            sb.AppendLine("~DEVICE TYPE");
            sb.AppendLine(DeviceType.Trim());
            sb.AppendLine("~STAGE");
            sb.AppendLine(TestStage.Trim());
            sb.AppendLine("~SPECIFICATION");
            sb.AppendLine(Specification.Trim());
            sb.AppendLine("~SERIAL NUMBER");
            sb.AppendLine(SerialNumber.Trim());
            //add test result
            sb.Append(GeneratePcasDropFile());
            return sb.ToString();
        }

        #endregion
    }

    public class TestDetails
    {
        public TestDetails()
        {
        }
        public TestDetails(TestStatus testResult, DataTable table)
        {
            TestResult = testResult;
            TestDetailsTable = table;
        }
        private TestStatus testResult;
        /// <summary>
        /// test result is pass or fail
        /// </summary>
        public TestStatus TestResult
        {
            get { return testResult; }
            internal set { testResult = value; }
        }
        private DataTable table;
        /// <summary>
        /// show test details using a table
        /// </summary>
        public DataTable TestDetailsTable
        {
            get { return table; }
            internal set { table = value; }
        }


    }
    public enum TestStatus
    {
        Unkown, NoValue, NoLimits, Fail, Pass,
    }
    public class LimitParameterType<T> where T : struct
    {
        /// <summary>
        /// upper value of Limit parameter
        /// </summary>
        public Nullable<T> Max;
        /// <summary>
        /// actual value of Limit parameter
        /// </summary>
        public Nullable<T> Val;
        /// <summary>
        /// lower value of Limit parameter
        /// </summary>
        public Nullable<T> Min;
        /// <summary>
        /// determine whether val is between max and min or not
        /// </summary>
        public TestStatus ParameterIsOk
        {
            get
            {
                if (!Val.HasValue)
                    return TestStatus.NoValue;
                if (Object.ReferenceEquals(Val.GetType(), typeof(double)))
                {
                    Nullable<double> val = Val as Nullable<double>;
                    Nullable<double> max = Max as Nullable<double>;
                    Nullable<double> min = Min as Nullable<double>;
                    if (max.HasValue && min.HasValue)
                        return max >= val && val >= min ? TestStatus.Pass : TestStatus.Fail;
                    else if (max.HasValue)
                        return max >= val ? TestStatus.Pass : TestStatus.Fail;
                    else if (min.HasValue)
                        return val >= min ? TestStatus.Pass : TestStatus.Fail;
                    else
                        return TestStatus.NoLimits;
                }
                else if (Object.ReferenceEquals(Val.GetType(), typeof(int)))
                {
                    Nullable<int> val = Val as Nullable<int>;
                    Nullable<int> max = Max as Nullable<int>;
                    Nullable<int> min = Min as Nullable<int>;
                    if (max.HasValue && min.HasValue)
                        return max >= val && val >= min ? TestStatus.Pass : TestStatus.Fail;
                    else if (max.HasValue)
                        return max >= val ? TestStatus.Pass : TestStatus.Fail;
                    else if (min.HasValue)
                        return val >= min ? TestStatus.Pass : TestStatus.Fail;
                    else
                        return TestStatus.NoLimits;
                }
                else
                    throw new Exception("Unknown data type.");


            }
        }
    }

    #endregion
}

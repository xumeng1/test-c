using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.FcuMapping.Core
{
    public interface IChain
    {
        bool Run(object stub);
    }
}

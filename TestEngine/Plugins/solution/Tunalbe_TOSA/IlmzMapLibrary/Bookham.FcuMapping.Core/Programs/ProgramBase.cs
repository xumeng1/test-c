using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.Framework.Limits;

namespace Bookham.FcuMapping.Core
{
    public abstract class ProgramBase : ITestProgram
    {

        #region ITestProgram Members

        public virtual void InitCode(ITestEngineInit engine, DUTObject dutObject, 
            InstrumentCollection instrs, ChassisCollection chassis)
        {
            InstrumentManager.SetInstrumentCollection(instrs);
            InstrumentManager.ProgressReport -= new ProgressReportEventHandler(InstrumentManager_ProgressReport);
            InstrumentManager.ProgressReport += new ProgressReportEventHandler(InstrumentManager_ProgressReport);
            InstrumentManager.Initialize();

            //Read previous test data
            ReadPreviousTestData();
            // initialise config
            InitConfig();
            // initialise instruments
            InitInstrs();
            // load specification
            LoadSpecs(engine, dutObject);
            // initialise utilities
            InitUtils(engine);
            // init modules
            InitModules(engine, dutObject);

        }

        public abstract void InitModules(ITestEngineInit engine, DUTObject dutObject);

        public abstract void InitUtils(ITestEngineInit engine);

        public abstract void LoadSpecs(ITestEngineInit engine, DUTObject dutObject);

        public abstract void InitInstrs();

        public abstract void InitConfig();
        
        public abstract void ReadPreviousTestData();
        
        public abstract void InstrumentManager_ProgressReport(ProgressReportEventArgs e);
        
        public virtual void DataWrite(ITestEngineDataWrite engine, DUTObject dutObject, DUTOutcome dutOutcome, TestData results, UserList userList)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void PostRun(ITestEnginePostRun engine, DUTObject dutObject, InstrumentCollection instrs, ChassisCollection chassis)
        {
            InstrumentManager.Shutdown();
        }

        public virtual void Run(ITestEngineRun engine, DUTObject dutObject, InstrumentCollection instrs, ChassisCollection chassis)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual Type UserControl
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        #endregion

        public string[] GetPcasResult_tcmz_map(ITestEngineInit engine, string SERIAL_NO)
        {
            string[] PcasResults;
            bool IsOk = ReadPcasResult.TryGetPcasData(engine, "HIBERDB", "tcmz_map", SERIAL_NO, false,
                  new string[] { "TEST_STATUS", "NUM_SM", "LASER_WAFER_SIZE" }, out PcasResults);
            if (!IsOk)
            {
                string errmsg = string.Format("Try Get PcasData Fail. SCHEMA='HIBERDB',TEST_STAGE='tcmz_map',SERIAL_NO={0}", SERIAL_NO);
                engine.ErrorInProgram(errmsg);
                throw new Exception(errmsg);
            }
            if (!PcasResults[0].Equals("Pass", StringComparison.InvariantCultureIgnoreCase))
            {
                string errmsg = string.Format("Test Result Fail. SCHEMA='HIBERDB',TEST_STAGE='tcmz_map',SERIAL_NO={0}", SERIAL_NO);
                engine.ErrorInProgram(errmsg);
                throw new Exception(errmsg);
            }
            return PcasResults;
        }


        
    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.FcuMapping.Core
{
    public abstract class ModuleBase : IChain
    {


        #region IChain Members

        public abstract bool Run(object stub);
        

        #endregion
    }
}

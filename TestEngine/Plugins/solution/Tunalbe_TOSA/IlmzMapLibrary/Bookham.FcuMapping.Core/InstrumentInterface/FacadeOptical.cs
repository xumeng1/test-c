using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.Instruments;

namespace Bookham.FcuMapping.Core
{
    class FacadeOptical:IOptical
    {
        Inst_Ag816x_OpticalPowerMeter powermeter;
        Inst_Ag86122x wavemeter;
        Inst_Ag8614xA Osa;
        public FacadeOptical(Inst_Ag816x_OpticalPowerMeter powermeter, Inst_Ag86122x wavemeter,Inst_Ag8614xA Osa)
        {
            this.powermeter = powermeter;
            this.wavemeter = wavemeter;
            this.Osa = Osa;
        }
        #region IDevice Members

        public bool Initialize()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public bool Shutdown()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion
    }
}

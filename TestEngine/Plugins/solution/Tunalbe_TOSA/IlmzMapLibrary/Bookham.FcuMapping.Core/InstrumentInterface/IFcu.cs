using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestSolution.Instruments;
using System.ComponentModel;

namespace Bookham.FcuMapping.Core
{
    interface IFcu : IDevice
    {
        int IGain_Dac { get; set; }
        int IPhase_Dac { get; set; }
        int IRear_Dac { get; set; }
        int ISoa_Dac { get; set; }
        Inst_FCU.OnOff LaserEnable { set; }
        Inst_FCU.OnOff RearSW { get; set; }
        Inst_FCU.OnOff TecEnable { get; set; }
        
        event ProgressChangedEventHandler OverallmapProgressChanged;
        event ProgressChangedEventHandler SupermapProgressChanged;

        void BeginSupermapAsync(Inst_FCU.SweepDirection sd);
        void BeginSupermapAsync(Inst_FCU.SweepDirection sd, int delaytime_ms);
        bool CheckUpdate(string CaliDataFileDirectory);
        void Close();
        string EndSupermapAsync();
        string OverallmapSync(Inst_FCU.SweepDirection sd);
        string OverallmapSync(Inst_FCU.SweepDirection sd, int delaytime_ms);
        void SetMiddleLine(string smFileName);



    }
}

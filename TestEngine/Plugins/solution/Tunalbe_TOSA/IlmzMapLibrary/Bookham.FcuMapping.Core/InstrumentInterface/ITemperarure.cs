using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.Instruments;

namespace Bookham.FcuMapping.Core
{
    enum LimitType
    {
        LOWER, UPPER
    }
    enum ProtectionType
    {
        ptRESISTANCE,
        ptTEMPERATURE
    }
    enum OnOrOff
    {
        OFF, ON
    }
    enum DataResponseFormat
    {
        TYPE_ASCII = 0,
        TYPE_HEX = 1,
        TYPE_OCT = 2,
        TYPE_BIN = 3,
    }

    interface ITemperarure : IDevice
    {
        #region Initialize settings
        
        //最低温度=0, 最高温度=60 
        //取消ground connect 
        //使用温度控制模式
        //启用4线，RTD温度感应(用户模式，需要指定α，β，δ)，A=1.2073e-3,B=2.20813e-4,C=1.4494e-7
        //保护电流=1.6A,保护电压=5V 
        //ProportionalGain=80, IntegrallGain=5.0, DerivativeGain=0 

        //Initialize settings
        bool GroundConnect
        {
            get;
            set;
        }
        InstType_TecController.ControlMode OperatingMode
        {
            get;
            set;
        }
        InstType_TecController.SensorType Sensor_Type
        {
            get;
            set;
        }
        SteinhartHartCoefficients SteinhartHartConstants
        {
            get;
            set;
        }
        
        double DerivativeGain { get; set; }
        double IntegralGain { get; set; }
        double ProportionalGain { get; set; }

        float TecCurrentCompliance_amp
        {
            get;
            set;
        }
        float TecVoltageCompliance_volt
        {
            get;
            set;
        }
        //set MinTemperatureLimit_C and MaxTemperatureLimit_C
        void Set_Protection_Level_Temperature(LimitType lstate, double dLimitValue);
        void Set_Protection_Level_Resistance(LimitType lstate, double dLimitValue);
        void Set_Protection(ProtectionType pState, OnOrOff state);

        //normal operate

        #endregion

        // Summary:
        //     Sets/returns the RTD Sensor Callendar-Van Dusen Equation coefficients.
        CallendarVanDusenCoefficients CallendarVanDusenConstants { get; set; }
        //
        // Summary:
        //     Sets/returns the Derivative Gain Constant.
        double DerivativeGain { get; set; }
        //
        // Summary:
        //     Get enumerator for Digital IO Line
        IEnumerable<IInstType_DigitalIO> DigiIoLines { get; }
        //
        // Summary:
        //     Set ground connect
        bool GroundConnect { set; }
        //
        // Summary:
        //     Sets/returns the Integral Gain Constant.
        double IntegralGain { get; set; }
        
        //
        // Summary:
        //     Sets/returns the control mode. Instrument driver should throw if a particular
        //     mode is not supported
        InstType_TecController.ControlMode OperatingMode { get; set; }
        //
        // Summary:
        //     Sets/returns the output state
        bool OutputEnabled { get; set; }
        //
        // Summary:
        //     Sets/returns the Proportional Gain Constant.
        double ProportionalGain { get; set; }
        //
        // Summary:
        //     Sets/returns the sensor type
        InstType_TecController.SensorType Sensor_Type { get; set; }
        //
        // Summary:
        //     Set/returns the sensor current.
        double SensorCurrent_amp { get; set; }
        //
        // Summary:
        //     Returns the actual sensor resistance Note that set should throw an exception
        //     if the controller is not operating in the appropriate mode
        double SensorResistanceActual_ohm { get; }
        //
        // Summary:
        //     Set/returns the sensor resistance set point Note that set should throw an
        //     exception if the controller is not operating in the appropriate mode
        double SensorResistanceSetPoint_ohm { get; set; }
        //
        // Summary:
        //     Returns the actual temperature.
        double SensorTemperatureActual_C { get; }
        //
        // Summary:
        //     Sets/returns the temperature set point.  Note that set should throw an exception
        //     if the controller is not operating in the appropriate mode
        double SensorTemperatureSetPoint_C { get; set; }
        //
        // Summary:
        //     Sets/returns the Thermistor Sensor Steinhart-Hart Equation coefficients.
        SteinhartHartCoefficients SteinhartHartConstants { get; set; }
        //
        // Summary:
        //     Returns the actual TEC (peltier) current.
        double TecCurrentActual_amp { get; }
        //
        // Summary:
        //     Set/returns the TEC compliance current
        double TecCurrentCompliance_amp { get; set; }
        //
        // Summary:
        //     Set/returns the peltier current set point
        double TecCurrentSetPoint_amp { get; set; }
        //
        // Summary:
        //     Returns the TEC AC resistance
        double TecResistanceAC_ohm { get; }
        //
        // Summary:
        //     Returns the TEC DC resistance
        double TecResistanceDC_ohm { get; }
        //
        // Summary:
        //     Returns the actual TEC voltage
        double TecVoltageActual_volt { get; }
        //
        // Summary:
        //     Set/returns the TEC compliance voltage
        double TecVoltageCompliance_volt { get; set; }
        //
        // Summary:
        //     Set/returns the TEC voltage set point
        double TecVoltageSetPoint_volt { get; set; }

        // Summary:
        //     Get a Digital IO Line by line number
        //
        // Parameters:
        //   lineNumber:
        IInstType_DigitalIO GetDigiIoLine(int lineNumber);
        //
        //
        // Parameters:
        //   pState:
        //
        //   state:
        void Set_Protection(ProtectionType pState, OnOrOff state);
        //
        //
        // Parameters:
        //   lstate:
        //
        //   dLimitValue:
        void Set_Protection_Level_Resistance(LimitType lstate, double dLimitValue);
        //
        //
        // Parameters:
        //   lstate:
        //
        //   dLimitValue:
        void Set_Protection_Level_Temperature(LimitType lstate, double dLimitValue);
        //
        //
        // Parameters:
        //   state:
        void Set_Status_Register_Data_Format(DataResponseFormat state);




    }
}

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.Instruments;
using Bookham.TestLibrary.InstrTypes;

namespace Bookham.FcuMapping.Core
{
    class FacadeElectrical : IElectrical, ISource, IMeasure
    {
        Inst_Ke24xx ke24xx;

        public FacadeElectrical(Inst_Ke24xx Ke24xx)
        {
            this.ke24xx = Ke24xx;
        }


        #region IElectricalSmux Members

        public void reset()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public Sense sense
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        public Terminal SelectTerminal
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        public ISource source
        {
            get { return this; }
        }

        public IMeasure measure
        {
            get { return this; }
        }

        #endregion

        #region IDevice Members

        public bool Initialize()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public bool Shutdown()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion


        #region ISource Members

        double ISource.rangei
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        double ISource.rangev
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        double ISource.leveli
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        double ISource.levelv
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        double ISource.delay
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        double ISource.limiti
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        double ISource.limitv
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        Func ISource.func
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        Output ISource.output
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        #endregion

        #region IMeasure Members

        double IMeasure.nplc
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        double IMeasure.rangei
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        double IMeasure.rangev
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        AutoRange IMeasure.autorangei
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        AutoRange IMeasure.autorangev
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        AutoZero IMeasure.autozero
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        double IMeasure.i()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        double IMeasure.v()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion
    }
}

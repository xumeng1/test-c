using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.InstrTypes;

namespace Bookham.FcuMapping.Core
{
    interface IOptical : IDevice
    {
        double AveragingTime_s { get; set; }
        //
        // Summary:
        //     Get / Set calibration offset in decibels.
        double CalOffset_dB { get; set; }
        
        //
        // Summary:
        //     Get/Set Enable/Disable Min/Max hold function.
        bool MaxMinOn { get; set; }
        //
        // Summary:
        //     Get / Set the mode of optical power meter. If a set of a mode which is not
        //     supported by this instrument is attempted, an exception should be thrown.
        InstType_OpticalPowerMeter.MeterMode Mode { get; set; }
        //
        // Summary:
        //     Sets/Returns the measurement range. Units depend on current mode.  The range
        //     method guarantees to set a power at or *above* that specified in this function.
        //      I.e. the power level specified in the set is guaranteed to be measureable
        //     in this mode.
        //
        // Remarks:
        //     For 816x DUAL head power meters (e.g. 81635, 81619) auto-ranging talks to
        //     "master" channel (1) whatever the ACTUAL channel this object represents.
        //     N.B.: The same value affects both channel 1 and 2, so sets may have unforeseen
        //     consequences. N.B.: Manual ranging is independent of channel!
        double Range { get; set; }
        //
        // Summary:
        //     Get/Set reference power for relative measurements. Uses the current MeterMode
        //     for units.  MeterMode.Absolute_dBm -> dBm MeterMode.Absolute_mW -> mW MeterMode.Relative_dB
        //     -> dBm
        double ReferencePower { get; set; }
        //
        // Summary:
        //     SafeMode flag. If true, instrument will always wait for completion after
        //     every command and check the error registers. If false it will do neither.
        public bool SafeMode { get; set; }
        /// <summary>
        /// Reads/sets the expected input signal wavelength in nanometres
        /// </summary>
        double Wavelength_nmFromPowermter { get; set; }
        /// <summary>
        /// Return wavelength in nm
        /// get wavelength from wavemeter,set the value to powermeter.
        /// </summary>
        double Wavelength_nmFromWavemeter { get; }
        /// <summary>
        /// Return frequency in GHz from wavemeter
        /// </summary>
        double Frequency_GHz { get; }
        // Summary:
        //     Set up data acquisition.
        //
        // Parameters:
        //   numberOfPoints:
        //     The number of points to collect
        //
        //   averagingTime_s:
        //     The averaging time of each measurement
        public void ConfigureDataLogging(int numberOfPoints, double averagingTime_s);
        //
        // Summary:
        //     Query whether data acquisition is in progress
        //
        // Returns:
        //     TRUE if busy
        public bool DataLoggingInProgress();
        //
        // Summary:
        //     Wait for input trigger
        //
        // Parameters:
        //   enabled:
        //     TRUE to wait for a trigger, FALSE to ignore triggers
        public void EnableInputTrigger(bool enabled);
        //
        // Summary:
        //     Produce an output trigger after taking a reading
        //
        // Parameters:
        //   enabled:
        //     TRUE to produce a trigger pulse
        public void EnableOutputTrigger(bool enabled);
        //
        // Summary:
        //     Retrieve power data.
        //
        // Returns:
        //     An array of power data converted to the appropriate units in accordance with
        //     the MeterMode
        public double[] GetSweepData();
        //
        // Summary:
        //     Initiate and return a power measurement. Returned units depend on the current
        //     mode of the instrument (dBm, mW, dB).
        double ReadPower();
        //
        // Summary:
        //     Return the max and min power readings observed since the last activation
        //     of Max/Min Hold Mode.
        InstType_OpticalPowerMeter.PowerMaxMin ReadPowerMaxMin();
        //
        // Summary:
        //     Set instrument to default state
        void SetDefaultState();
        //
        // Summary:
        //     Start data acquisition mode
        //
        // Remarks:
        //     When you enable a logging data acquisition function for a Agilent 8163A/B
        //     Series Power Meter with averaging time of less than 100 ms with input hardware
        //     triggering disabled, all GPIB commands will be ignored for the duration of
        //     the function.
        public void StartDataLogging();
        //
        // Summary:
        //     Exit data acquisition mode
        //
        // Remarks:
        //     Exit data acquisition before you try to set up any more data acquisition.
        //      Some parameters cannot be set until you exit this mode.
        public void StopDataLogging();
        //
        // Summary:
        //     Ends a calibration of optical dark-current. Waits for a previously started
        //     dark current cal to complete.
        //
        // Remarks:
        //     For 816x DUAL head power meters (e.g. 81635, 81619) this talks to "master"
        //     channel (1) whatever the ACTUAL channel this object represents. N.B.: The
        //     same value affects both channel 1 and 2, so sets may have unforeseen consequences
        void ZeroDarkCurrent_End();
        //
        // Summary:
        //     Starts a calibration of power meter dark-current value on the power meter
        //     channel. For this calibration to succeed the meter must have zero light input.
        //     This function returns immediately once the command has been sent to the instrument
        //     (Async command).
        //
        // Remarks:
        //     For 816x DUAL head power meters (e.g. 81635, 81619) this talks to "master"
        //     channel (1) whatever the ACTUAL channel this object represents. N.B.: The
        //     same value affects both channel 1 and 2, so sets may have unforeseen consequences
        void ZeroDarkCurrent_Start();

    }
}

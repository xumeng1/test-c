using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.Instruments;

namespace Bookham.FcuMapping.Core
{
    class FacadeTemperarure:ITemperarure
    {
        Inst_Ke2510 ke2510;
        public FacadeTemperarure(Inst_Ke2510 Ke2510)
        {
            this.ke2510 = Ke2510;
        }

        #region IDevice Members

        public bool Initialize()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public bool Shutdown()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion
    }
}

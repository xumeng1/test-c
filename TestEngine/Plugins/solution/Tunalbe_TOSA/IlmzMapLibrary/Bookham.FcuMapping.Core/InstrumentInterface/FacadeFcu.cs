using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestSolution.Instruments;

namespace Bookham.FcuMapping.Core
{
    class FacadeFcu:IFcu
    {
        Inst_FCU fcu;
        public FacadeFcu(Inst_FCU Fcu)
        {
            this.fcu = Fcu;
        }
        #region IDevice Members

        public bool Initialize()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public bool Shutdown()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion
    }
}

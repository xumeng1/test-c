using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.FcuMapping.Core
{
    interface IElectrical : IDevice
    {
        /// <summary>
        /// Turns off the output and resets the SMU to the default settings.
        /// </summary>
        void reset();
        /// <summary>
        /// Sense mode selection
        /// </summary>
        Sense sense
        {
            get;
            set;
        }
        /// <summary>
        /// Front or Rear Panel selection
        /// </summary>
        Terminal SelectTerminal
        {
            get;
            set;
        }

        ISource source
        {
            get;
        }

        IMeasure measure
        {
            get;
        }

    }
    interface ISource
    {
        /// <summary>
        /// Get/Set current source range.
        /// </summary>
        double rangei
        {
            get;
            set;
        }
        /// <summary>
        /// Get/Set voltage source range.
        /// </summary>
        double rangev
        {
            get;
            set;
        }
        /// <summary>
        /// Get/Set current source value.
        /// </summary>
        double leveli
        {
            get;
            set;
        }
        /// <summary>
        /// Get/Set voltage source value.
        /// </summary>
        double levelv
        {
            get;
            set;
        }
        /// <summary>
        /// Get/Set Source delay. The default is 0, no delays.
        ///  0 or smuX.DELAY_OFF    No delay.
        /// -1 or smuX.DELAY_AUTO   Auto delay.
        ///  User_value             Set user delay value.
        /// </summary>
        /// <remarks>
        /// smuX.source.delay = Y -- Writes source delay.
        /// This attribute allows for additional source settling time after an output step.
        /// Setting this attribute to DELAY_AUTO will cause a range dependent delay to be
        /// inserted when ever the source is changed.
        /// Y can be set to a specific user defined value that will set the delay that is used
        /// regardless of range.
        /// </remarks>
        double delay
        {
            get;
            set;
        }
        /// <summary>
        /// Get/Set current compliance limit.
        /// </summary>
        double limiti
        {
            get;
            set;
        }
        /// <summary>
        /// Get/Set voltage compliance limit.
        /// </summary>
        double limitv
        {
            get;
            set;
        }
        
        /// <summary>
        /// Select source function.
        /// </summary>
        Func func
        {
            get;
            set;
        }
        /// <summary>
        /// Turns on/off the SMU output.
        /// </summary>
        Output output
        {
            get;
            set;
        }
    }
    interface IMeasure
    {
        /// <summary>
        /// Get/Set speed,Set speed (nplc = 0.001 to 25)
        /// </summary>
        double nplc
        {
            get;
            set;
        }
        /// <summary>
        /// Get/Set current measure range.
        /// </summary>
        double rangei
        {
            get;
            set;
        }
        /// <summary>
        /// Get/Set voltage measure range.
        /// </summary>
        double rangev
        {
            get;
            set;
        }
        /// <summary>
        /// Get/Set current measure auto range.
        /// </summary>
        AutoRange autorangei
        {
            get;
            set;
        }
        /// <summary>
        /// Get/Set voltage measure auto range.
        /// </summary>
        AutoRange autorangev
        {
            get;
            set;
        }
        /// <summary>
        /// Auto zero command
        /// </summary>
        AutoZero autozero
        {
            get;
            set;
        }

        /// <summary>
        /// Request a current reading.
        /// </summary>
        /// <returns></returns>
        double i();
        /// <summary>
        /// Request a voltage reading.
        /// </summary>
        /// <returns></returns>
        double v();
     
    }

    #region Enum type

    enum AutoRange
    {
        AUTORANGE_OFF, AUTORANGE_ON
    }
    enum AutoZero
    {
        /// <summary>
        /// Disable auto zero.
        /// </summary>
        AUTOZERO_OFF,
        /// <summary>
        /// Force one ref and zero.
        /// </summary>
        AUTOZERO_ONCE,
        /// <summary>
        /// Force ref and zero with each measurement.
        /// </summary>
        AUTOZERO_AUTO
    }

    enum Func
    {
        /// <summary>
        /// Select current source function.
        /// </summary>
        OUTPUT_DCAMPS,
        /// <summary>
        /// Select voltage source function.
        /// </summary>
        OUTPUT_DCVOLTS,
    }

    enum Output
    {
        /// <summary>
        /// Turn off source output.
        /// </summary>
        OUTPUT_OFF,
        /// <summary>
        /// Turn on source output.
        /// </summary>
        OUTPUT_ON,

    }
    
    enum Sense
    {
        /// <summary>
        /// Selects local (2-wire) sense.
        /// </summary>
        SENSE_LOCAL,
        /// <summary>
        /// Selects remote (4-wire) sense.
        /// </summary>
        SENSE_REMOTE

    }
    enum Terminal
    {
        FRONT,
        REAR,
    }
    enum TrigDirection
    {
        None,
        Output_Trigger,
        Input_Trigger,
        Output_Meas_Input_Trigger,
        Input_Meas_Output_Trigger
    }
    #endregion
}

using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.FcuMapping.Core
{
    public class Chain
    {
        private List<IChain> list;

        public List<IChain> List
        {
            get
            {
                return list;
            }
        }

        public Chain()
        {
            list = new List<IChain>();
        }

        public void Message(object stub)
        {
            foreach (IChain item in list)
            {
                bool result = item.Run(stub);

                if (result == true) break;
            }
        }

        public void Add(IChain handler)
        {
            List.Add(handler);
        }
    }

}

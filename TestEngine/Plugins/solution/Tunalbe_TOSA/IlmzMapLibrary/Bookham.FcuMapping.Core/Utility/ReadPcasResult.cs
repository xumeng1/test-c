using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.FcuMapping.Core
{
    public class ReadPcasResult
    {

        public static bool TryGetPcasData(ITestEngineInit engine,
            string SCHEMA, string TEST_STAGE, string SERIAL_NO, bool RawData,
            string[] PcasDescriptors, out string[] PcasResults)
        {
            try
            {
                StringDictionary keys = new StringDictionary();
                keys.Add("SCHEMA", SCHEMA);
                keys.Add("TEST_STAGE", TEST_STAGE);
                keys.Add("SERIAL_NO", SERIAL_NO);

                DatumList PreTD = engine.GetDataReader().GetLatestResults(keys, RawData);
                if (PreTD == null)
                {
                    //string strMsg = "No PCAS data available for SERIAL_NO = '" + SERIAL_NO +
                    //    "', TEST_STAGE = '" + TEST_STAGE + "'";
                    //throw new Exception(strMsg);
                    PcasResults = null;
                    return false;
                }
                List<string> ListPcasResults = new List<string>(PcasDescriptors.Length);
                foreach (string var in PcasDescriptors)
                    if (PreTD.IsPresent(var))
                        ListPcasResults.Add(PreTD.GetDatum(var).ValueToString());
                    else
                        ListPcasResults.Add(null);
                PcasResults = ListPcasResults.ToArray();
                return true;
            }
            catch (Exception)
            {
                PcasResults = null;
                return false;
            }
        }

        //public static bool TryGetPcasData(ITestEngineInit engine,
        //    string SCHEMA, string TEST_STAGE, string SERIAL_NO, bool RawData,
        //    Dictionary<string,string> verifyData,
        //    string[] PcasDescriptors, out string[] PcasResults)
        //{
        //    if (verifyData == null || verifyData.Count < 1)
        //        throw new ArgumentNullException("verify data is empty.");

        //}
    }
}

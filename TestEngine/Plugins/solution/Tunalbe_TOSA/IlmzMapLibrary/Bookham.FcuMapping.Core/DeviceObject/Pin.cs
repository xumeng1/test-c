using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.FcuMapping.Core
{
    class Pin<T> where T : IDevice
    {
        private T iInstrument;
        public Pin(PinName PinName, T IInstrument)
        {
            this.Name = PinName;
            iInstrument = IInstrument;
        }

        internal PinName Name;
        internal PinClassifier Classifier;
        internal PinZone Zone;
        internal bool IsInitialized;
        internal string Comment;
        internal T IInstrument
        {
            get
            {
                return iInstrument;
            }
        }

        public static explicit operator Pin<T>(Pin<IDevice> pin)
        {
            Pin<T> cp = new Pin<T>(pin.Name, (T)pin.IInstrument);
            cp.Classifier = pin.Classifier;
            cp.Zone = pin.Zone;
            cp.Comment = pin.Comment;
            cp.IsInitialized = pin.IsInitialized;
            return cp;
        }
    }

    enum PinClassifier
    {
        None,
        Electrical,
        Fcu,
        Optical,
        Temperarure,
    }
    enum PinZone
    {
        None,
        DsdbrZone,
        MzZone,
        External,
    }
}

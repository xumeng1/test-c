using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.FcuMapping.Core
{
    internal class Laser
    {
        PinCollection<IFcu> Pins_F;
        PinCollection<ITemperarure> Pins_T;

        public Laser(PinCollection<IDevice> Pins)
        {
            Pins_F = new PinCollection<IFcu>();
            Pins_T = new PinCollection<ITemperarure>();
            foreach (string var in Enum.GetNames(typeof(PinName)))
            {
                PinName p = (PinName)Enum.Parse(typeof(PinName), var);
                Pin<IDevice> pin = Pins[p];
                if (pin.Zone == PinZone.DsdbrZone)
                {
                    if (pin.IInstrument is IFcu)
                    {
                        Pins_F.Add(pin.Name, (Pin<IFcu>)pin);
                    }
                    else if (pin.IInstrument is ITemperarure)
                    {
                        Pins_T.Add(pin.Name, (Pin<ITemperarure>)pin);
                    }
                }
            }
        }

        public Pin<IFcu> Front1
        {
            get
            {
                return Pins_F[PinName.Front1];
            }
        }
        public Pin<IFcu> Front2
        {
            get
            {
                return Pins_F[PinName.Front2];
            }
        }
        public Pin<IFcu> Front3
        {
            get
            {
                return Pins_F[PinName.Front3];
            }
        }
        public Pin<IFcu> Front4
        {
            get
            {
                return Pins_F[PinName.Front4];
            }
        }
        public Pin<IFcu> Front5
        {
            get
            {
                return Pins_F[PinName.Front5];
            }
        }
        public Pin<IFcu> Front6
        {
            get
            {
                return Pins_F[PinName.Front6];
            }
        }
        public Pin<IFcu> Front7
        {
            get
            {
                return Pins_F[PinName.Front7];
            }
        }
        public Pin<IFcu> Front8
        {
            get
            {
                return Pins_F[PinName.Front8];
            }
        }

        public Pin<IFcu> Gain
        {
            get
            {
                return Pins_F[PinName.Gain];
            }
        }
        public Pin<IFcu> SOA
        {
            get
            {
                return Pins_F[PinName.SOA];
            }
        }
        public Pin<IFcu> Phase
        {
            get
            {
                return Pins_F[PinName.Phase];
            }
        }
        public Pin<IFcu> Rear
        {
            get
            {
                return Pins_F[PinName.Rear];
            }
        }
        public Pin<ITemperarure> Temperature
        {
            get
            {
                return Pins_T[PinName.DsdbrTemperature];
            }
        }
    }
}

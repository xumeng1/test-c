using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.FcuMapping.Core
{
    enum PinName
    {
        /*---Dsdbr section Pin Name ----*/
        /// <summary>Gain section</summary>
        Gain,
        /// <summary>SOA section</summary>
        SOA,
        /// <summary>Rear section</summary>
        Rear,
        /// <summary>Phase section</summary>
        Phase,
        /// <summary>Front1 section</summary>
        Front1,
        /// <summary>Front2 section</summary>
        Front2,
        /// <summary>Front3 section</summary>
        Front3,
        /// <summary>Front4 section</summary>
        Front4,
        /// <summary>Front5 section</summary>
        Front5,
        /// <summary>Front6 section</summary>
        Front6,
        /// <summary>Front7 section</summary>
        Front7,
        /// <summary>Front8 section</summary>
        Front8,
        /// <summary>
        /// Set Dsdbr temperature
        /// </summary>
        DsdbrTemperature,

        /*---MZ Pin Name ----*/
        /// <summary>
        /// Left Arm Modulation Voltage Source
        /// </summary>
        MzLeftArmMod,
        /// <summary>
        /// Right Arm Modulation Voltage Source
        /// </summary>
        MzRightArmMod,
        /// <summary>
        /// Left Arm Imbalance Current Source
        /// </summary>
        MzLeftArmImb,
        /// <summary>
        /// Right Arm Imbalance Current Source
        /// </summary>
        MzRightArmImb,
        /// <summary>
        /// Complementary Tap Photodiode + bias
        /// </summary>
        MzComplementaryTap,
        /// <summary>
        /// Inline Tap Photodiode + bias,
        /// sometimes not present (will be null is those circumstances)
        /// </summary>
        MzInlineTap,
        /// <summary>
        /// Set MZ temperature
        /// </summary>
        MzTemperature,
        /// <summary>
        /// Triggered power-meter used for MZ measurements
        /// </summary>
        MzOptical,

        /*---External Pin Name ----*/
        CaseTemperature,
        OpmRef,
        SoaCurrentSource,
        FCU_Source,
        
    }
}

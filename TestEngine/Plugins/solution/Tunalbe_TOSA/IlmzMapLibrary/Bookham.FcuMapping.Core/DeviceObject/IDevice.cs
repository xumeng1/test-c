using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.FcuMapping.Core
{
    interface IDevice
    {
        bool Initialize();
        bool Shutdown();
    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.FcuMapping.Core
{
    internal class TCMZ : DSDBR
    {
        internal readonly MZ Mz;
        protected TCMZ ()
	    {
            if (InstrumentManager.GetPinCollection == null)
                throw new Exception("Instrument Collection is null.");
            PinCollection<IDevice> Pins = InstrumentManager.GetPinCollection;
            Mz = new MZ(Pins);
	    }
        static TCMZ tcmz = new TCMZ();

        public static new TCMZ Instance
        {
            get
            {
                return tcmz;
            }
        }


    }
}

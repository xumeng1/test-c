using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Bookham.FcuMapping.Core
{
    internal class MZ
    {
        PinCollection<IElectrical> Pins_E;
        PinCollection<IOptical> Pins_O;
        PinCollection<ITemperarure> Pins_T;

        public MZ(PinCollection<IDevice> Pins)
        {
            Pins_E = new PinCollection<IElectrical>();
            Pins_O = new PinCollection<IOptical>();
            Pins_T = new PinCollection<ITemperarure>();
            foreach (string var in Enum.GetNames(typeof(PinName)))
            {
                PinName p = (PinName)Enum.Parse(typeof(PinName), var);
                Pin<IDevice> pin = Pins[p];
                if (pin.Zone == PinZone.MzZone)
                {
                    if (pin.IInstrument is IElectrical)
                    {
                        Pins_E.Add(pin.Name, (Pin<IElectrical>)pin);
                    }
                    else if (pin.IInstrument is IOptical)
                    {
                        Pins_O.Add(pin.Name, (Pin<IOptical>)pin);
                    }
                    else if (pin.IInstrument is ITemperarure)
                    {
                        Pins_T.Add(pin.Name, (Pin<ITemperarure>)pin);
                    }
                }
            }

        }

        public Pin<IElectrical> ComplementaryTap
        {
            get
            {
                return Pins_E[PinName.MzComplementaryTap];
            }
        }
        public Pin<IElectrical> InlineTap
        {
            get
            {
                return Pins_E[PinName.MzInlineTap];
            }
        }
        public Pin<IElectrical> LeftArmImb
        {
            get
            {
                return Pins_E[PinName.MzLeftArmImb];
            }
        }
        public Pin<IElectrical> LeftArmMod
        {
            get
            {
                return Pins_E[PinName.MzLeftArmMod];
            }
        }
        public Pin<IOptical> MzOptical
        {
            get
            {
                return Pins_O[PinName.MzOptical];
            }
        }
        public Pin<IElectrical> RightArmImb
        {
            get
            {
                return Pins_E[PinName.MzRightArmImb];
            }
        }
        public Pin<IElectrical> RightArmMod
        {
            get
            {
                return Pins_E[PinName.MzRightArmMod];
            }
        }
        public Pin<ITemperarure> Temperature
        {
            get
            {
                return Pins_T[PinName.MzTemperature];
            }
        }
       

    }
}

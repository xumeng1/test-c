using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.FcuMapping.Core
{
    internal class DSDBR
    {
        internal readonly Laser LaserSource;
        internal readonly External External;
        protected DSDBR()
        {
            if (InstrumentManager.GetPinCollection == null)
                throw new Exception("Instrument Collection is null.");
            PinCollection<IDevice> Pins = InstrumentManager.GetPinCollection;
            LaserSource = new Laser(Pins);
            External = new External(Pins);
        }
        static DSDBR dsdbr = new DSDBR();

        public static DSDBR Instance
        {
            get
            {
                return dsdbr;
            }
        }
    }
}

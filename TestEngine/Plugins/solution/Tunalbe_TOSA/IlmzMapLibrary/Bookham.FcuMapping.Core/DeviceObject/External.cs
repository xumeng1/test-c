using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.FcuMapping.Core
{
    class External
    {
        PinCollection<IElectrical> Pins_E;
        PinCollection<IOptical> Pins_O;
        PinCollection<ITemperarure> Pins_T;
        public External(PinCollection<IDevice> Pins)
        {
            Pins_E = new PinCollection<IElectrical>();
            Pins_O = new PinCollection<IOptical>();
            Pins_T = new PinCollection<ITemperarure>();
            foreach (string var in Enum.GetNames(typeof(PinName)))
            {
                PinName p = (PinName)Enum.Parse(typeof(PinName), var);
                Pin<IDevice> pin = Pins[p];
                if (pin.Zone == PinZone.External)
                {
                    if (pin.IInstrument is IElectrical)
                    {
                        Pins_E.Add(pin.Name, (Pin<IElectrical>)pin);
                    }
                    else if (pin.IInstrument is IOptical)
                    {
                        Pins_O.Add(pin.Name, (Pin<IOptical>)pin);
                    }
                    else if (pin.IInstrument is ITemperarure)
                    {
                        Pins_T.Add(pin.Name, (Pin<ITemperarure>)pin);
                    }
                }
            }

        }

        public Pin<ITemperarure> CaseTemperature
        {
            get
            {
                return Pins_T[PinName.CaseTemperature];
            }
        }
        public Pin<IOptical> OpmRef
        {
            get
            {
                return Pins_O[PinName.OpmRef];
            }
        }
        public Pin<IElectrical> SoaCurrentSource
        {
            get
            {
                return Pins_E[PinName.SoaCurrentSource];
            }
        }
        public Pin<IElectrical> FCU_Source
        {
            get
            {
                return Pins_E[PinName.FCU_Source];
            }
        }
    }
}

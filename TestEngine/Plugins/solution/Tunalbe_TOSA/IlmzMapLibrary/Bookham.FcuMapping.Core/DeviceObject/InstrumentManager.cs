using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.Equipment;
using Bookham.TestLibrary.Instruments;
using Bookham.TestSolution.Instruments;

namespace Bookham.FcuMapping.Core
{
    public static class InstrumentManager
    {

        private static PinCollection<IDevice> Pins = null;
        internal static PinCollection<IDevice> GetPinCollection
        {
            get { return Pins; }
        }

        public static void SetInstrumentCollection(InstrumentCollection instrs)
        {
            Inst_Ag8614xA ag8614xa = (Inst_Ag8614xA)instrs["Osa"];
            Inst_Ag86122x wavemeter = (Inst_Ag86122x)instrs["Wavemeter"];
            Inst_FCU instFcu = (Inst_FCU)instrs["Fcu"];
            IFcu Fcu = new FacadeFcu(instFcu);
            Pins = new PinCollection<IDevice>();
            foreach (string var in Enum.GetNames(typeof(PinName)))
            {
                Inst_Ke24xx ke24xx;
                Inst_Ke2510 ke2510;
                Inst_Ag816x_OpticalPowerMeter powermeter;
                IDevice iDevice = null;
                PinClassifier Classifier = PinClassifier.None;
                PinZone Zone = PinZone.None;
                PinName p = (PinName)Enum.Parse(typeof(PinName), var);
                switch (p)
                {
                    case PinName.Gain:
                    case PinName.SOA:
                    case PinName.Rear:
                    case PinName.Phase:
                    case PinName.Front1:
                    case PinName.Front2:
                    case PinName.Front3:
                    case PinName.Front4:
                    case PinName.Front5:
                    case PinName.Front6:
                    case PinName.Front7:
                    case PinName.Front8:
                        Classifier = PinClassifier.Fcu;
                        Zone = PinZone.DsdbrZone;
                        iDevice = Fcu;
                        break;
                    case PinName.DsdbrTemperature:
                        Classifier = PinClassifier.Temperarure;
                        Zone = PinZone.DsdbrZone;
                        ke2510 = (Inst_Ke2510)instrs["TecDsdbr"];
                        iDevice = new FacadeTemperarure(ke2510);
                        break;

                    case PinName.MzLeftArmMod:
                        Classifier = PinClassifier.Electrical;
                        Zone = PinZone.MzZone;
                        ke24xx = (Inst_Ke24xx)instrs["MzLeftMod"];
                        iDevice = new FacadeElectrical(ke24xx);
                        break;
                    case PinName.MzRightArmMod:
                        Classifier = PinClassifier.Electrical;
                        Zone = PinZone.MzZone;
                        ke24xx = (Inst_Ke24xx)instrs["MzRightMod"];
                        iDevice = new FacadeElectrical(ke24xx);
                        break;
                    case PinName.MzLeftArmImb:
                        Classifier = PinClassifier.Electrical;
                        Zone = PinZone.MzZone;
                        ke24xx = (Inst_Ke24xx)instrs["MzLeftImb"];
                        iDevice = new FacadeElectrical(ke24xx);
                        break;
                    case PinName.MzRightArmImb:
                        Classifier = PinClassifier.Electrical;
                        Zone = PinZone.MzZone;
                        ke24xx = (Inst_Ke24xx)instrs["MzRightImb"];
                        iDevice = new FacadeElectrical(ke24xx);
                        break;
                    case PinName.MzComplementaryTap:
                        Classifier = PinClassifier.Electrical;
                        Zone = PinZone.MzZone;
                        ke24xx = (Inst_Ke24xx)instrs["MzTapComp"];
                        iDevice = new FacadeElectrical(ke24xx);
                        break;
                    case PinName.MzInlineTap:
                        Classifier = PinClassifier.Electrical;
                        Zone = PinZone.MzZone;
                        ke24xx = (Inst_Ke24xx)instrs["MzTapInline"];
                        iDevice = new FacadeElectrical(ke24xx);
                        break;
                    case PinName.MzTemperature:
                        Classifier = PinClassifier.Temperarure;
                        Zone = PinZone.MzZone;
                        ke2510 = (Inst_Ke2510)instrs["TecMz"];
                        iDevice = new FacadeTemperarure(ke2510);
                        break;
                    case PinName.MzOptical:
                        Classifier = PinClassifier.Optical;
                        Zone = PinZone.MzZone;
                        powermeter = (Inst_Ag816x_OpticalPowerMeter)instrs["OpmMz"];
                        iDevice = new FacadeOptical(powermeter, wavemeter, ag8614xa);
                        break;

                    case PinName.CaseTemperature:
                        Classifier = PinClassifier.Temperarure;
                        Zone = PinZone.External;
                        ke2510 = (Inst_Ke2510)instrs["TecCase"];
                        iDevice = new FacadeTemperarure(ke2510);
                        break;
                    case PinName.OpmRef:
                        Classifier = PinClassifier.Optical;
                        Zone = PinZone.External;
                        powermeter = (Inst_Ag816x_OpticalPowerMeter)instrs["OpmRef"];
                        iDevice = new FacadeOptical(powermeter, wavemeter, ag8614xa);
                        break;
                    case PinName.SoaCurrentSource:
                        Classifier = PinClassifier.Electrical;
                        Zone = PinZone.External;
                        ke24xx = (Inst_Ke24xx)instrs["SoaCurrentSource"];
                        iDevice = new FacadeElectrical(ke24xx);
                        break;
                    case PinName.FCU_Source:
                        Classifier = PinClassifier.Electrical;
                        Zone = PinZone.External;
                        ke24xx = (Inst_Ke24xx)instrs["FCU_Source"];
                        iDevice = new FacadeElectrical(ke24xx);
                        break;
                    default:
                        throw new Exception("Unkown enum type value:" + p.ToString());
                }
                Pin<IDevice> pin = new Pin<IDevice>(p, iDevice);
                pin.Classifier = Classifier;
                pin.Zone = Zone;
                Pins.Add(pin.Name, pin);
            }
           
        }

        public static event ProgressReportEventHandler ProgressReport = null;
        static void OnProgressReport(ProgressReportEventArgs e)
        {
            if (ProgressReport != null)
                ProgressReport(e);
        }
        public static bool Initialize()
        {
            ProgressReportEventArgs e = new ProgressReportEventArgs("Start Initialize Instruments...");
            OnProgressReport(e);
            foreach (KeyValuePair<PinName, Pin<IDevice>> var in Pins)
            {
                Pin<IDevice> pin = var.Value;
                if (!pin.IsInitialized)
                {
                    e = new ProgressReportEventArgs("Start Initialize " + var.Key.ToString());
                    OnProgressReport(e);
                    pin.IInstrument.Initialize();
                    pin.IsInitialized = true;
                    e = new ProgressReportEventArgs("Complete Initialize " + var.Key.ToString());
                    OnProgressReport(e);
                }
            }
            return true;
        }

        public static bool Shutdown()
        {
            ProgressReportEventArgs e = new ProgressReportEventArgs("Start Initialize Instruments...");
            OnProgressReport(e);
            foreach (KeyValuePair<PinName, Pin<IDevice>> var in Pins)
            {
                Pin<IDevice> pin = var.Value;
                e = new ProgressReportEventArgs("Shutdown " + var.Key.ToString()+ "...");
                OnProgressReport(e);
                pin.IInstrument.Shutdown();
            }
            return true;
        }

    }
    public delegate void ProgressReportEventHandler(ProgressReportEventArgs e);
    public sealed class ProgressReportEventArgs:EventArgs
    {
        object _e;
        public object e
        {
            get
            {
                return _e;
            }
        }
        public ProgressReportEventArgs(object e)
        {
            this._e = e;
        }
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Bookham.TestEngine.PluginInterfaces;
using Bookham.TestSolution.Instruments;
using Bookham.TestLibrary.Instruments;


namespace Bookham.TestLibrary.FcuCommonUtils
{
    /// <summary>
    /// DSDBR utilities
    /// </summary>
    public sealed class DsdbrUtils
    {
        private DsdbrUtils() { }

        /// <summary>
        /// Set DSDBR currents
        /// </summary>
        /// <param name="setup"></param>
        /// <param name="instrsToUse"></param>
        public static void SetDsdbrCurrents_mA(DsdbrChannelSetup setup)
        {
            FcuInstrs.IsOnline = true;
            // Set rear current
            fcuInstrs.IRear_mA = (float)setup.IRear_mA;

            // Set phase current
            fcuInstrs.IPhase_mA = (float)setup.IPhase_mA;

            // Set soa current
            fcuInstrs.ISoa_mA = (float)setup.ISoa_mA;

            // set gain current
            fcuInstrs.IGain_mA = (float)setup.IGain_mA;

            // set rearsoa current
            fcuInstrs.IRearSoa_mA = (float)setup.IRearSoa_mA;

            // set front pair current
            //fcuInstrs.SetFrontSectionPairCurrent(setup.FrontPair, setup.IFsFirst_mA, setup.IFsSecond_mA);
            //set front pair current by asic
            fcuInstrs.SetFrontPairCurrent_mA((int)setup.FrontPair, (float)setup.IFsFirst_mA, (float)setup.IFsSecond_mA); // by tim to combine solution
        }


        /// <summary>
        /// Clockwise start of phase and rear sections.
        /// </summary>
        /// <param name="Iphase_mA">Phase current</param>
        /// <param name="Irear_mA">Rear current</param>
        /// <param name="instrsToUse">Instruments to be used</param>
        public static void ClockwiseStart(double Iphase_mA, double Irear_mA)
        {
            
            fcuInstrs.IRear_mA = 0;
            fcuInstrs.IPhase_mA = 0;
            fcuInstrs.IRear_mA = (float)Irear_mA;
            fcuInstrs.IPhase_mA = (float)Iphase_mA;
          
        }

        /// <summary>
        /// Set a DSDBR section current
        /// </summary>
        /// <param name="Section">DSDBR Section</param>
        /// <param name="SectionCurrent_mA">Section current</param>
        /// <param name="instrsToUse">Instruments to be used</param>
        public static void SetSectionCurrent_mA(DSDBRSection Section, double SectionCurrent_mA)
        {
            switch (Section)
            {
                case DSDBRSection.Gain:
                    fcuInstrs.IGain_mA = (float)SectionCurrent_mA;
                    break;
                case DSDBRSection.SOA:
                    fcuInstrs.ISoa_mA = (float)SectionCurrent_mA;
                    break;
                case DSDBRSection.Rear:
                    fcuInstrs.IRear_mA = (float)SectionCurrent_mA;
                    break;
                case DSDBRSection.Phase:
                    fcuInstrs.IPhase_mA = (float)SectionCurrent_mA;
                    break;
                case DSDBRSection.Front1:
                case DSDBRSection.Front2:
                case DSDBRSection.Front3:
                case DSDBRSection.Front4:
                case DSDBRSection.Front5:
                case DSDBRSection.Front6:
                case DSDBRSection.Front7:
                case DSDBRSection.Front8:
                    int frontSection = int.Parse(Section.ToString().Replace("Front", ""));
                    fcuInstrs.SetFrontSectionCurrent_mA(frontSection, (float)SectionCurrent_mA);
                    fcuInstrs.SetFrontCurrent_mA(frontSection, (float)SectionCurrent_mA);
                    break;
                default:
                    throw new ArgumentException("Unknown DSDBR section!");
            }

        }

        /// <summary>
        /// Read a DSDBR section current
        /// </summary>
        /// <param name="Section">DSDBR Section</param>
        /// <param name="instrsToUse">Instruments to be used</param>
        /// <returns>Section current in mA</returns>
        public static double ReadSectionCurrent_mA(DSDBRSection Section)
        {
            double sectionCurrent_mA = 0;
            switch (Section)
            {
                case DSDBRSection.Gain:
                    sectionCurrent_mA = (double)fcuInstrs.IGain_mA;
                    break;
                case DSDBRSection.SOA:
                    sectionCurrent_mA = (double)fcuInstrs.ISoa_mA;
                    break;
                case DSDBRSection.Rear:
                    sectionCurrent_mA = (double)fcuInstrs.IRear_mA;
                    break;
                case DSDBRSection.Phase:
                    sectionCurrent_mA = (double)fcuInstrs.IPhase_mA;
                    break;
                case DSDBRSection.Front1:
                case DSDBRSection.Front2:
                case DSDBRSection.Front3:
                case DSDBRSection.Front4:
                case DSDBRSection.Front5:
                case DSDBRSection.Front6:
                case DSDBRSection.Front7:
                case DSDBRSection.Front8:
                    int frontSection = int.Parse(Section.ToString().Replace("Front", ""));
                    sectionCurrent_mA = fcuInstrs.ReadFrontSectionCurrent_mA(frontSection);
                    break;
                default:
                    throw new ArgumentException("Unknown DSDBR section!");
            }
            return sectionCurrent_mA;
        }

        /// <summary>
        /// Static class to set output state of all DSDBR sources.
        /// </summary>
        /// <param name="enableState">Output state, true for enabled</param>
        public static void EnableDsdbr(bool enableState)
        {
            if (!enableState) fcuInstrs.IGain_mA = 0; // laser shut 
        }


        #region Public Data Structures
        /// <summary>
        /// Locker currents
        /// </summary>
        public struct LockerCurrents
        {
            /// <summary>
            /// TX locker current mA
            /// </summary>
            public double TxCurrent_mA;
            /// <summary>
            /// RX locker current mA
            /// </summary>
            public double RxCurrent_mA;
            /// <summary>
            /// Locker ratio
            /// </summary>
            public double LockRatio;
        }
        #endregion

        #region Private Data
        private static Inst_Fcu2Asic fcuInstrs;

        public static Inst_Fcu2Asic FcuInstrs
        {
            get { return DsdbrUtils.fcuInstrs; }
            set { DsdbrUtils.fcuInstrs = value; }
        }
        private const int lockerAvgCount = 10;
        private const int lockerDelay_mS = 2;
        private const double C = 299792458;
        #endregion

    }
}

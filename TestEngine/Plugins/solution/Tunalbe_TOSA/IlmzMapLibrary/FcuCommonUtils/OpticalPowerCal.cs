using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.Algorithms;

namespace Bookham.TestLibrary.FcuCommonUtils
{
    /// <summary>
    /// Class to perform calibration on optical power values
    /// </summary>
    public class OpticalPowerCal
    {
        /// <summary>
        /// Private ctor
        /// </summary>
        private OpticalPowerCal()
        {
        }

        /// <summary>
        /// Sets up the collection of power cal data classes
        /// </summary>
        /// <param name="orderOfFit">The order of polynomial to be applied to the fitted data</param>
        public static void Initialise(int orderOfFit)
        {
            OpticalPowerCal.PowerCalData = new Dictionary<OpticalPowerHead, PowerCal>();
            
            OpticalPowerCal.PowerCalData.Add(OpticalPowerHead.OpmCgFiltered, new PowerCal(orderOfFit));
            OpticalPowerCal.PowerCalData.Add(OpticalPowerHead.OpmCgDirect, new PowerCal(orderOfFit));
            OpticalPowerCal.PowerCalData.Add(OpticalPowerHead.MZHead, new PowerCal(orderOfFit));
            OpticalPowerCal.PowerCalData.Add(OpticalPowerHead.ExternalHead, new PowerCal(orderOfFit));
            OpticalPowerCal.PowerCalData.Add(OpticalPowerHead.DsdbrHead, new PowerCal(orderOfFit));
        }

        /// <summary>
        /// Add a data point for the power head specified
        /// </summary>
        /// <param name="head">The enumerated power head</param>
        /// <param name="frequency">Measured frequency</param>
        /// <param name="offset">Measured offset</param>
        public static void AddCalibratedPoint(OpticalPowerHead head, double frequency, double offset)
        {
            PowerCalData[head].AddPoint(frequency, offset);
        }

        /// <summary>
        /// Returns a corrected power value
        /// </summary>
        /// <param name="head">The enumerated power head</param>
        /// <param name="measuredPower_dBm">Raw power reading</param>
        /// <param name="frequencyGHz">The frequency for which the offset is required.</param>
        /// <returns>The corrected power value</returns>
        public static double GetCorrectedPower_dBm(OpticalPowerHead head, double measuredPower_dBm, double frequencyGHz)
        {
            return measuredPower_dBm + GetOffset_dB(head, frequencyGHz);
        }

        /// <summary>
        /// Returns a corrected power value
        /// </summary>
        /// <param name="head">The enumerated power head</param>
        /// <param name="measuredPower_mW">Raw power reading</param>
        /// <param name="frequencyGHz">The frequency for which the offset is required.</param>
        /// <returns>The corrected power value</returns>
        public static double GetCorrectedPower_mW(OpticalPowerHead head, double measuredPower_mW, double frequencyGHz)
        {
            double power_dBm = Alg_PowConvert_dB.Convert_mWtodBm(measuredPower_mW) + GetOffset_dB(head, frequencyGHz);
            return Alg_PowConvert_dB.Convert_dBmtomW(power_dBm);
        }

        /// <summary>
        /// Apply correction to an array of values.
        /// </summary>
        /// <param name="head">The enumerated power head</param>
        /// <param name="measuredPower_dBm">Raw power reading</param>
        /// <param name="frequencyGHz">The frequency for which the offset is required.</param>
        /// <returns>The corrected power array</returns>
        public static double[] GetCorrectedPower_dBm(OpticalPowerHead head, double[] measuredPower_dBm, double frequencyGHz)
        {
            return Alg_ArrayFunctions.AddToEachArrayElement(measuredPower_dBm, GetOffset_dB(head, frequencyGHz));
        }

        /// <summary>
        /// Apply correction to an array of values.
        /// </summary>
        /// <param name="head">The enumerated power head</param>
        /// <param name="measuredPower_mW">Raw power reading</param>
        /// <param name="frequencyGHz">The frequency for which the offset is required.</param>
        /// <returns>The corrected power array</returns>
        public static double[] GetCorrectedPower_mW(OpticalPowerHead head, double[] measuredPower_mW, double frequencyGHz)
        {
            double[] uncalibratedPower_dBm = Alg_PowConvert_dB.Convert_mWtodBm(measuredPower_mW);
            double[] calibratedPower_dBm = GetCorrectedPower_dBm(head, uncalibratedPower_dBm, frequencyGHz);
            return Alg_PowConvert_dB.Convert_dBmtomW(calibratedPower_dBm);
        }

        /// <summary>
        /// Returns the offset value.
        /// </summary>
        /// <param name="head">The enumerated power head</param>
        /// <param name="frequencyGHz">The frequency for which the offset is required.</param>
        /// <returns>The correction factor</returns>
        public static double GetCorrectionFactor_dBm(OpticalPowerHead head, double frequencyGHz)
        {
            return GetOffset_dB(head, frequencyGHz);
        }

        #region private members
        /// <summary>
        /// Calculate the Offset at a given frequency
        /// </summary>
        /// <param name="head">The enumerated power head</param>
        /// <param name="frequencyGHz">The frequency for which the offset is required.</param>
        /// <returns>An offset figure in dB</returns>
        private static double GetOffset_dB(OpticalPowerHead head, double frequencyGHz)
        {
            if (PowerCalData[head].CalibrationComplete())
            {
                return PowerCalData[head].GetOffset_dB(frequencyGHz);
            }
            else
            {
                return 0;
            }
        }


        /// <summary>
        /// Dictionary of PowerCal classes
        /// </summary>
        private static Dictionary<OpticalPowerHead, PowerCal> PowerCalData;
        #endregion
    }


    /// <summary>
    /// Specifies which optical power head to access
    /// </summary>
    public enum OpticalPowerHead
    {
        /// <summary>
        /// The extternal wide area detector
        /// </summary>
        ExternalHead,
        /// <summary>
        /// The unfiltered head used by CLose-Grid
        /// </summary>
        OpmCgDirect,
        /// <summary>
        /// The filtered head used by CLose-Grid
        /// </summary>
        OpmCgFiltered,
        /// <summary>
        /// The triggered head used in the MZ sweeps.
        /// </summary>
        MZHead,
        /// <summary>
        /// The dsdbr 
        /// </summary>
        DsdbrHead
    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.FcuCommonUtils
{
    /// <summary>
    /// DSDBR Section
    /// </summary>
    public enum DSDBRSection
    {
        /// <summary>Gain section</summary>
        Gain,
        /// <summary>SOA section</summary>
        SOA,
        /// <summary>Rear section</summary>
        Rear,
        /// <summary>Phase section</summary>
        Phase,
        /// <summary>Front1 section</summary>
        Front1,
        /// <summary>Front2 section</summary>
        Front2,
        /// <summary>Front3 section</summary>
        Front3,
        /// <summary>Front4 section</summary>
        Front4,
        /// <summary>Front5 section</summary>
        Front5,
        /// <summary>Front6 section</summary>
        Front6,
        /// <summary>Front7 section</summary>
        Front7,
        /// <summary>Front8 section</summary>
        Front8
    }

    /// <summary>
    /// DSDBR Section
    /// </summary>
    public enum LockerId
    {
        /// <summary>TX Locker</summary>
        TX,
        /// <summary>RX Locker</summary>
        RX
    }
}

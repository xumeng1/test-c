// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// Measurements.cs
//
// Author: Tony Foster, 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Threading;
using Bookham.TestLibrary.Algorithms;
using Bookham.TestSolution.Instruments;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.Instruments;

namespace Bookham.TestLibrary.FcuCommonUtils
{
    /// <summary>
    /// General Measurements
    /// </summary>
    public static class Measurements
    {
        #region Instruments

        /// <summary>
        /// Wavemeter instrument reference
        /// </summary>
        public static IInstType_Wavemeter Wavemeter
        {
            set { wavemeter = value; }
            get { return wavemeter; }
        }

        /// <summary>
        /// OSA instrument reference
        /// </summary>
        public static IInstType_OSA OSA
        {
            set { osa = value; }
            get { return osa; }
        }

        /// <summary>
        /// Ag8163/ykaq2200 internal power detector
        /// </summary>
        public static IInstType_OpticalPowerMeter MzHead
        {
            set { mzHead = value; }
            get { return mzHead; }
        }

        /// <summary>
        /// Fullband control unit instruments group
        /// </summary>
        public static Inst_Fcu2Asic FCUInstrs
        {
            set { fcuInstrs = value; }
            get { return fcuInstrs; }
        }

      
        #endregion

        #region Measurement functions
        /// <summary>
        /// Current frequency 
        /// </summary>
        public static double CurrentFrequency_GHz
        {
            // Return local value. Read frequency if not available
            get
            {
                if (currentFrequency_GHz < 180000 || currentFrequency_GHz > 200000)
                    currentFrequency_GHz = ReadFrequency_GHz();
                return currentFrequency_GHz;
            }
            // Set local frequency & wavelength
            set
            {
                // Check that value is sensible
                if (value > 180000 && value < 200000)
                {
                    currentFrequency_GHz = value;
                    currentWavelength_nm = Alg_FreqWlConvert.Freq_GHz_TO_Wave_nm(value);
                }
            }
        }

        /// <summary>
        /// Read frequency on wavemeter
        /// </summary>
        /// <returns>frequency in GHz</returns>
        public static double ReadFrequency_GHz()
        {
            // Read frequency
            double freq = wavemeter.Frequency_GHz;
            // Update cached values
            CurrentFrequency_GHz = freq;
            // Return frequency
            return freq;
        }

        public static double ReadOpticalPower_dbm_FromWavemeter()
        {
            // Read opticalpower_dbm
            double power = wavemeter.Power_dBm;
            // Return opticalpower_dbm
            return power;
        }

        /// <summary>
        /// Read wavelength on wavemeter
        /// </summary>
        /// <returns>frequency in GHz</returns>
        public static double ReadWavelength_nm()
        {
            // Read frequency
            double freq = ReadFrequency_GHz();

            // Return wavelength (currentWavelength_nm is cached from readFrequency_GHz)
            return currentWavelength_nm;
        }

        /// <summary>
        /// Read Optical Power
        /// </summary>
        /// <param name="head">Power detector to use</param>
        /// <param name="units">Power units</param>
        /// <returns>Power in specified units</returns>
        public static double ReadOpticalPower(OpticalPowerHead head, PowerUnits units)
        {
            IInstType_OpticalPowerMeter detectorHead;
            switch (head)
            {
                // MZ head
                case OpticalPowerHead.MZHead:
                    detectorHead = mzHead;
                    break;
                default:
                    throw new ArgumentException("Unknown optical power detector '" + head.ToString() + "' in Measurements.ReadOpticalPower'");
            }

            double power = 0;

            // Set head to correct mode. Note that CG heads only work in mW.
            if ((units == PowerUnits.dBm || units == PowerUnits.dBm_uncal))
                detectorHead.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;
            else
                detectorHead.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_mW;

            // Set wavelength
            detectorHead.Wavelength_nm = currentWavelength_nm;

            // Read power and check under range...
            int maxTryCount = 3;
            int triedCount = 0;
            do
            {
                power = detectorHead.ReadPower();
                power = wavemeter.Power_dBm;// Jack.Zhang measure optical power from Wavemeter
                if (!double.IsInfinity(power))
                    break;
            }
            while (++triedCount <= maxTryCount);

            if (units == PowerUnits.dBm) 
                    power = OpticalPowerCal.GetCorrectedPower_dBm(head, power, currentFrequency_GHz);
            else if (units == PowerUnits.mW)
                power = OpticalPowerCal.GetCorrectedPower_mW(head, power, currentFrequency_GHz);

            // Return power
            return power;
        }

       
        #endregion

       

        #region Enums
        /// <summary>
        /// Optical power measurement units
        /// </summary>
        public enum PowerUnits
        {
            /// <summary>
            /// dBm
            /// </summary>
            dBm,
            /// <summary>
            /// mW
            /// </summary>
            mW,
            /// <summary>
            /// Uncalibrated dBm
            /// </summary>
            dBm_uncal,
            /// <summary>
            /// Uncalibrated mW 
            /// </summary>
            mW_uncal
        }
        #endregion

        #region Structures
        /// <summary>
        /// TCMZ Electrical Dissipation results
        /// </summary>
        public struct PowerDissResults
        {
            /// <summary>
            /// Dsdbr TEC current A
            /// </summary>
            public double TecDsdbr_A;
            /// <summary>
            /// Dsdbr TEC voltage V
            /// </summary>
            public double TecDsdbr_V;
            /// <summary>
            /// MZ TEC current A
            /// </summary>
            public double TecMz_A;
            /// <summary>
            /// MZ TEC voltage V
            /// </summary>
            public double TecMz_V;
            /// <summary>
            /// Dsdbr Gain section current A
            /// </summary>
            public double Gain_A;
            /// <summary>
            /// Dsdbr Gain section voltage V
            /// </summary>
            public double Gain_V;
            /// <summary>
            /// Dsdbr Phase section current A
            /// </summary>
            public double Phase_A;
            /// <summary>
            /// Dsdbr Phase section voltage V
            /// </summary>
            public double Phase_V;
            /// <summary>
            /// Dsdbr Rear section current A
            /// </summary>
            public double Rear_A;
            /// <summary>
            /// Dsdbr Rear section voltage V
            /// </summary>
            public double Rear_V;
            /// <summary>
            /// Dsdbr SOA section current A
            /// </summary>
            public double SOA_A;
            /// <summary>
            /// Dsdbr SOA section voltage V
            /// </summary>
            public double SOA_V;
            /// <summary>
            /// Dsdbr first front section current A
            /// </summary>
            public double FrontFirst_A;
            /// <summary>
            /// Dsdbr first front section voltage V
            /// </summary>
            public double FrontFirst_V;
            /// <summary>
            /// Dsdbr second front section current A
            /// </summary>
            public double FrontSecond_A;
            /// <summary>
            /// Dsdbr second front section voltage V
            /// </summary>
            public double FrontSecond_V;

            /// <summary>
            /// Laser total current
            /// </summary>
            public double LaserTotalCurrent_A;
            /// <summary>
            /// Laser power W
            /// </summary>
            public double LaserPower_W;
            /// <summary>
            /// Package power W
            /// </summary>
            public double PackagePower_W;
        }

        #endregion

        #region Private Data

        private static IInstType_Wavemeter wavemeter;
        private static IInstType_OSA osa;
        private static IInstType_OpticalPowerMeter mzHead;
        private static Inst_Fcu2Asic fcuInstrs;

        private static double currentFrequency_GHz;
        private static double currentWavelength_nm;
        #endregion

     
    }
}

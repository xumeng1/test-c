using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.Utilities;

namespace Bookham.TestLibrary.FcuCommonUtils
{
    public class FreqCalByEtalon
    {
        public static List<List<double>> Filter_Etalon_Data = new List<List<double>>();
        public static double Max_Locker_Ratio= -999,  Min_Locker_Ratio= 999;
        public static void LoadFreqlCalArray(string filename)
        {
            CsvReader csvReader = new CsvReader();
            List<string[]> Filter_Etalon_List = csvReader.ReadFile(filename);
            double Freq_GHz, PowerRatio, LockerRatio, Delt_Locker_Ratio, Max_Delt_Locker_Ratio=0;
            for (int i = 2; i < Filter_Etalon_List.Count; i++)
            {
                List<double> tmpList = new List<double>();
                Freq_GHz=Convert.ToDouble(Filter_Etalon_List[i][1]);
                PowerRatio=Convert.ToDouble(Filter_Etalon_List[i][15]);
                LockerRatio=Convert.ToDouble(Filter_Etalon_List[i][16]);
                Delt_Locker_Ratio =Math.Abs(LockerRatio-Convert.ToDouble(Filter_Etalon_List[i-1][16]));
                Max_Delt_Locker_Ratio = Math.Max(Max_Delt_Locker_Ratio, Delt_Locker_Ratio);

                tmpList.Add(Freq_GHz);// 0_Frequency_GHz
                tmpList.Add(Convert.ToDouble(Filter_Etalon_List[i][4]));// 1_Filter_Pot
                tmpList.Add(Convert.ToDouble(Filter_Etalon_List[i][7]));// 2_Reference_Pot
                tmpList.Add(Convert.ToDouble(Filter_Etalon_List[i][10]));// 3_Locker_Etalon_Pot
                tmpList.Add(Convert.ToDouble(Filter_Etalon_List[i][13]));// 4_Locker_Reference_Pot
                tmpList.Add(PowerRatio);// 5_Power_Ratio
                tmpList.Add(LockerRatio);// 6_Locker_Ratio
                tmpList.Add(Convert.ToDouble(Filter_Etalon_List[i][17]));// 7_Locker_Temperature_C
                tmpList.Add(Convert.ToDouble(Filter_Etalon_List[i][18]));// 8_Etalon_Thermistor_A
                tmpList.Add(Convert.ToDouble(Filter_Etalon_List[i][19]));// 9_Etalon_Thermistor_B
                tmpList.Add(Convert.ToDouble(Filter_Etalon_List[i][20]));// 10_Etalon_Thermistor_C
                int A=(int)(Freq_GHz/50.0);
                double B=Freq_GHz-50*A;
                if (Math.Abs(B) <=0.5)
                {
                        Max_Locker_Ratio = Math.Max(Max_Locker_Ratio, LockerRatio);
                        Min_Locker_Ratio = Math.Min(Min_Locker_Ratio, LockerRatio);
                }

                Filter_Etalon_Data.Add(tmpList);

            }
            Delt_Locker_Ratio=Max_Delt_Locker_Ratio-(Max_Locker_Ratio-Min_Locker_Ratio);
            if (Delt_Locker_Ratio > 0)//Modify the ITU LockerRatio Range in order to cover at least two point in one range. Jack.zhang 2010-11-18
            {
                Max_Locker_Ratio = Max_Locker_Ratio + Delt_Locker_Ratio / 2.0;
                Min_Locker_Ratio = Min_Locker_Ratio - Delt_Locker_Ratio / 2.0;
            }


        }
        public static void CalPRatio_LRatio_byFreq_GHz(double TargetFreq_GHz, out double PowerRatio, out double Filter_Etalon_Slope,
                                                        out double LockerRatio,out double First_PowerRatio,out double Last_PowerRatio,out bool In_Cal_Range)
        {

            double Freq_1, Freq_2, Power_Locker_R1, Power_Locker_R2;
            double Freq_Etalon_Star, Freq_Etalon_End, Etalon_Index;
            int Freq_Etalon_Star_Index = 0, Freq_Etalon_End_Index = 0;
            PowerRatio = 0; Filter_Etalon_Slope = 0; LockerRatio = 0; First_PowerRatio = 0; Last_PowerRatio = 0; In_Cal_Range = false;
            if ((TargetFreq_GHz - Filter_Etalon_Data[0][0]) * (TargetFreq_GHz - Filter_Etalon_Data[Filter_Etalon_Data.Count - 1][0]) < 0)
            {
                #region find power ratio by TargetFreq_GHz first
                for (int i = 0; i < Filter_Etalon_Data.Count; i++)
                {
                    if (Filter_Etalon_Data[i][0] >= TargetFreq_GHz)
                    {
                        Freq_2 = Filter_Etalon_Data[i][0];
                        Freq_1 = Filter_Etalon_Data[i - 1][0];
                        Power_Locker_R2 = Filter_Etalon_Data[i][5];
                        Power_Locker_R1 = Filter_Etalon_Data[i - 1][5];
                        PowerRatio = Power_Locker_R2 - (Power_Locker_R2 - Power_Locker_R1) * (Freq_2 - TargetFreq_GHz) / (Freq_2 - Freq_1);
                        break;
                    }
                }
                #endregion find power ratio by TargetFreq_GHz first

                //index the etalon lineary change range base on the coase freq
                Etalon_Index = Math.Round(TargetFreq_GHz / 50);

                Freq_Etalon_Star = (Etalon_Index - 1.5) * 50;
                Freq_Etalon_End = (Etalon_Index + 1.5) * 50;
                #region find power ratio on TargetFreq_GHz-50
                if ((Freq_Etalon_Star - Filter_Etalon_Data[0][0]) * (Freq_Etalon_Star - Filter_Etalon_Data[Filter_Etalon_Data.Count - 1][0]) < 0)
                {
                    for (int i = 0; i < Filter_Etalon_Data.Count; i++)
                    {
                        if (Filter_Etalon_Data[i][0] >= Freq_Etalon_Star)
                        {
                            Freq_2 = Filter_Etalon_Data[i][0];
                            Freq_1 = Filter_Etalon_Data[i - 1][0];
                            Power_Locker_R2 = Filter_Etalon_Data[i][5];
                            Power_Locker_R1 = Filter_Etalon_Data[i - 1][5];
                            First_PowerRatio = Power_Locker_R2 - (Power_Locker_R2 - Power_Locker_R1) * (Freq_2 - Freq_Etalon_Star) / (Freq_2 - Freq_1);
                            break;
                        }
                    }
                }
                else
                {
                    First_PowerRatio = Filter_Etalon_Data[0][5];
                    if (Freq_Etalon_Star >Math.Max(Filter_Etalon_Data[0][0],Filter_Etalon_Data[Filter_Etalon_Data.Count - 1][0]))
                        First_PowerRatio = Filter_Etalon_Data[Filter_Etalon_Data.Count - 1][5];
                }
                #endregion find power ratio on TargetFreq_GHz-50

                #region find power ratio on TargetFreq_GHz+50
                if ((Freq_Etalon_End - Filter_Etalon_Data[0][0]) * (Freq_Etalon_End - Filter_Etalon_Data[Filter_Etalon_Data.Count - 1][0]) < 0)
                {
                    for (int i = 0; i < Filter_Etalon_Data.Count; i++)
                    {
                        if (Filter_Etalon_Data[i][0] >= Freq_Etalon_End)
                        {
                            Freq_2 = Filter_Etalon_Data[i][0];
                            Freq_1 = Filter_Etalon_Data[i - 1][0];
                            Power_Locker_R2 = Filter_Etalon_Data[i][5];
                            Power_Locker_R1 = Filter_Etalon_Data[i - 1][5];
                            Last_PowerRatio = Power_Locker_R2 - (Power_Locker_R2 - Power_Locker_R1) * (Freq_2 - Freq_Etalon_End) / (Freq_2 - Freq_1);
                            break;
                        }
                    }
                }
                else
                {
                    Last_PowerRatio = Filter_Etalon_Data[0][5];
                    if (Freq_Etalon_Star > Math.Max(Filter_Etalon_Data[0][0], Filter_Etalon_Data[Filter_Etalon_Data.Count - 1][0]))
                        Last_PowerRatio = Filter_Etalon_Data[Filter_Etalon_Data.Count - 1][5];
                }
                #endregion find power ratio on TargetFreq_GHz+50

                #region local freq range in lockerRatio
                Freq_Etalon_Star = Etalon_Index * 50 - 25;
                Freq_Etalon_End = Etalon_Index * 50 + 25;
                if ((Freq_Etalon_Star - Filter_Etalon_Data[0][0]) * (Freq_Etalon_Star - Filter_Etalon_Data[Filter_Etalon_Data.Count - 1][0]) < 0)
                {
                    for (int i = 0; i < Filter_Etalon_Data.Count; i++)
                    {
                        if (Freq_Etalon_Star <= Math.Round(Filter_Etalon_Data[i][0]))
                        {
                            Freq_Etalon_Star_Index = i;
                            break;
                        }
                    }
                }
                else
                    Freq_Etalon_Star_Index = 0;

                if ((Freq_Etalon_End - Filter_Etalon_Data[0][0]) * (Freq_Etalon_End - Filter_Etalon_Data[Filter_Etalon_Data.Count - 1][0]) < 0)
                {
                    for (int i = Filter_Etalon_Data.Count - 1; i >= 0; i--)
                    {
                        if (Freq_Etalon_End >= Math.Round(Filter_Etalon_Data[i][0]))
                        {
                            Freq_Etalon_End_Index = i;
                            break;
                        }
                    }
                }
                else
                    Freq_Etalon_End_Index = Filter_Etalon_Data.Count - 1;
                                    
                #endregion local freq range in lockerRatio
                Filter_Etalon_Slope = (Filter_Etalon_Data[Freq_Etalon_Star_Index][6] - Filter_Etalon_Data[Freq_Etalon_End_Index][6]) / (Filter_Etalon_Data[Freq_Etalon_Star_Index][0] - Filter_Etalon_Data[Freq_Etalon_End_Index][0]);
                #region find the locker ratio by TargetFreq_GHz
                for (int i = Freq_Etalon_Star_Index; i <= Freq_Etalon_End_Index; i++)
                {
                    if (TargetFreq_GHz <= Filter_Etalon_Data[i][0])
                    {
                        Freq_2 = Filter_Etalon_Data[i][0];
                        Freq_1 = Filter_Etalon_Data[i - 1][0];
                        Power_Locker_R2 = Filter_Etalon_Data[i][6];
                        Power_Locker_R1 = Filter_Etalon_Data[i - 1][6];
                        LockerRatio = Power_Locker_R2 - (Power_Locker_R2 - Power_Locker_R1) * (Freq_2 - TargetFreq_GHz) / (Freq_2 - Freq_1);
                        break;
                    }
                }
                #endregion find the locker ratio by TargetFreq_GHz
                In_Cal_Range = true;
            }
        }
        public static double Modify_Freq_by_EtaloIndex(double Etalon_Index,double LockerRatio)
        {

            double Freq_Fine=0,Freq_1, Freq_2, Power_Locker_R1, Power_Locker_R2;
            double Freq_Etalon_Star, Freq_Etalon_End;
            int Freq_Etalon_Star_Index = 0, Freq_Etalon_End_Index = 0;

            #region local freq range in lockerRatio
            Freq_Etalon_Star = Etalon_Index * 50 - 25;
            Freq_Etalon_End = Etalon_Index * 50 + 25;

            for (int i = 0; i < Filter_Etalon_Data.Count; i++)
            {
                if (Freq_Etalon_Star <= Math.Round(Filter_Etalon_Data[i][0]))
                {
                    Freq_Etalon_Star_Index = i;
                    break;
                }
            }
            for (int i = Filter_Etalon_Data.Count - 1; i >= 0; i--)
            {
                if (Freq_Etalon_End >= Math.Round(Filter_Etalon_Data[i][0]))
                {
                    Freq_Etalon_End_Index = i;
                    break;
                }
            }
            #endregion local freq range in lockerRatio

            #region find the fine freq by locker ratio
            double Filter_Etalon_Slope = (Filter_Etalon_Data[Freq_Etalon_Star_Index][0] - Filter_Etalon_Data[Freq_Etalon_End_Index][0]) / (Filter_Etalon_Data[Freq_Etalon_Star_Index][6] - Filter_Etalon_Data[Freq_Etalon_End_Index][6]);
            for (int i = Freq_Etalon_Star_Index; i <= Freq_Etalon_End_Index; i++)
            {
                if (LockerRatio * Math.Sign(Filter_Etalon_Slope) <= Filter_Etalon_Data[i][6] * Math.Sign(Filter_Etalon_Slope))
                {
                    Freq_1 = Filter_Etalon_Data[i][0];
                    Freq_2 = Filter_Etalon_Data[i - Math.Sign(Filter_Etalon_Slope)][0];
                    Power_Locker_R1 = Filter_Etalon_Data[i][6];
                    Power_Locker_R2 = Filter_Etalon_Data[i - Math.Sign(Filter_Etalon_Slope)][6];
                    Freq_Fine = Freq_2 - (Freq_2 - Freq_1) * (Power_Locker_R2 - LockerRatio) / (Power_Locker_R2 - Power_Locker_R1);

                    break;
                }
            }
            #endregion find the fine freq by locker ratio
            return Freq_Fine;
        }
        public static double CalFreqByPRatio_LRatio_GHz(double PowerRatio, double LockerRatio,double TargetFreq_GHz)
        {

            double Freq_1, Freq_2, Power_Locker_R1, Power_Locker_R2, Freq_Coase=0, Freq_Fine=0, delt_freq=0;
            double Freq_Etalon_Star, Freq_Etalon_End, Etalon_Index;
            double Filter_Etalon_Slope = (Filter_Etalon_Data[0][0] - Filter_Etalon_Data[Filter_Etalon_Data.Count - 1][0]) / (Filter_Etalon_Data[0][5] - Filter_Etalon_Data[Filter_Etalon_Data.Count - 1][5]);
            int Freq_Etalon_Star_Index = 0, Freq_Etalon_End_Index = 0;
            if ((Math.Sign(Filter_Etalon_Slope) < 0 && (Filter_Etalon_Data[0][5] < PowerRatio))
                ||(Math.Sign(Filter_Etalon_Slope) > 0 && (Filter_Etalon_Data[Filter_Etalon_Data.Count - 1][5] < PowerRatio)))

                Freq_Fine = 0;
                else
                {
                    #region find coase freq by power ratio first
                    for (int i = 0; i < Filter_Etalon_Data.Count; i++)
                    {
                        if (Filter_Etalon_Data[i][5] * Math.Sign(Filter_Etalon_Slope) >= PowerRatio * Math.Sign(Filter_Etalon_Slope))
                        {
                            Freq_1 = Filter_Etalon_Data[i][0];
                            Freq_2 = Filter_Etalon_Data[i + Math.Sign(Filter_Etalon_Slope)][0];
                            Power_Locker_R1 = Filter_Etalon_Data[i][5];
                            Power_Locker_R2 = Filter_Etalon_Data[i + Math.Sign(Filter_Etalon_Slope)][5];
                            Freq_Coase = Freq_2 - (Freq_2 - Freq_1) * (Power_Locker_R2 - PowerRatio) / (Power_Locker_R2 - Power_Locker_R1);
                            break;
                        }
                    }
                    #endregion find coase freq by power ratio first


                    //index the etalon lineary change range base on the coase freq
                    Etalon_Index = Math.Round(Freq_Coase / 50);

                    do
                    {
                        #region local freq range in lockerRatio
                        Freq_Etalon_Star = Etalon_Index * 50 - 25;
                        Freq_Etalon_End = Etalon_Index * 50 + 25;

                        for (int i = 0; i < Filter_Etalon_Data.Count; i++)
                        {
                            if (Freq_Etalon_Star <= Math.Round(Filter_Etalon_Data[i][0]))
                            {
                                Freq_Etalon_Star_Index = i;
                                break;
                            }
                        }
                        for (int i = Filter_Etalon_Data.Count - 1; i >= 0; i--)
                        {
                            if (Freq_Etalon_End >= Math.Round(Filter_Etalon_Data[i][0]))
                            {
                                Freq_Etalon_End_Index = i;
                                break;
                            }
                        }
                        #endregion local freq range in lockerRatio

                        #region find the fine freq by locker ratio
                        Filter_Etalon_Slope = (Filter_Etalon_Data[Freq_Etalon_Star_Index][0] - Filter_Etalon_Data[Freq_Etalon_End_Index][0]) / (Filter_Etalon_Data[Freq_Etalon_Star_Index][6] - Filter_Etalon_Data[Freq_Etalon_End_Index][6]);
                        for (int i = Freq_Etalon_Star_Index; i <= Freq_Etalon_End_Index; i++)
                        {
                            if (LockerRatio * Math.Sign(Filter_Etalon_Slope) <= Filter_Etalon_Data[i][6] * Math.Sign(Filter_Etalon_Slope))
                            {
                                Freq_1 = Filter_Etalon_Data[i][0];
                                Freq_2 = Filter_Etalon_Data[i - Math.Sign(Filter_Etalon_Slope)][0];
                                Power_Locker_R1 = Filter_Etalon_Data[i][6];
                                Power_Locker_R2 = Filter_Etalon_Data[i - Math.Sign(Filter_Etalon_Slope)][6];
                                Freq_Fine = Freq_2 - (Freq_2 - Freq_1) * (Power_Locker_R2 - LockerRatio) / (Power_Locker_R2 - Power_Locker_R1);

                                break;
                            }
                        }
                        #endregion find the fine freq by locker ratio
//                        delt_freq = Freq_Fine - TargetFreq_GHz;
                        if (Math.Abs(delt_freq) >= 30)//for mapping test, Freq_Get can be changed to privious ITU point;
                        {
                            if (Freq_Fine > TargetFreq_GHz)
                                Etalon_Index = Etalon_Index - 1;
                            else
                                Etalon_Index = Etalon_Index + 1;
                        }

                    } while (Math.Abs(delt_freq) >= 30);// just for mapping test analysis
                }
            return Freq_Fine;
        }
        public static double Median(List<double > list)
        {
            double returnValue;
            list.Sort();
            if (list.Count > 1)
            {
                if ((list.Count / 2) == 0)
                {  // inthe mean of midee intow value
                    double first = list[(list.Count / 2) - 1];
                    double second = list[list.Count / 2];
                    returnValue = (first + second) / 2;
                }
                else
                {
                    returnValue = list[list.Count / 2];
                }
            }
            else
                returnValue = list[list.Count-1]; 
            return returnValue;
        }
    }
}

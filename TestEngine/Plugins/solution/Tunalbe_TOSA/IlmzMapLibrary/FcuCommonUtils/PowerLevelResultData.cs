using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.Framework.InternalData;

namespace Bookham.TestLibrary.FcuCommonUtils
{
    public struct PowerLevelResultData
    {
        /// <summary>
        /// dsdbr channel data
        /// </summary>
        public DsdbrChannelData ChannelData;
        /// <summary>
        /// power leveling result data
        /// </summary>
        public DatumList PowerLevelData;
        /// <summary>
        /// calculate tx/rx, when complete power leveling 
        /// </summary>
        public double lockRatio;
        /// <summary>
        /// phase ratio slope efficiency, use for soft lock
        /// </summary>
        public double PhaseRatioSlopeEff;
    }
}

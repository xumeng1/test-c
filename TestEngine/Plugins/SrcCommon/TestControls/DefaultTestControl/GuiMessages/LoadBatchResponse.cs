// [Copyright]
//
// Bookham Modular Test Engine Library
// Default Test Control Plug-in
//
// LoadBatchResponse.cs
// 
// Author: Joseph Olajubu
// Design: Test Control DD

using System;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.PluginInterfaces.MES;

namespace Bookham.TestLibrary.TestControl.GuiMessages
{
	/// <summary>
	/// Load Batch Response Message, Sent from Test control GUI to the Test Control Plug-in worker thread. 
	/// Holds batch information.
	/// </summary>
	internal sealed class LoadBatchResponse
	{
		#region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="batchId">The batch Id</param>
        /// <param name="mesBatch">MES Batch Information.</param>
        public LoadBatchResponse(string batchId, MESbatch mesBatch)
		{
			//Store the MES Data of new lot to be loaded into test engine. This lot should 
            // contain just one DUT.
            this.mesBatch = mesBatch;
            this.batchId = batchId;
		}
		#endregion

		#region Internal data

		/// <summary>
		/// Batch Id for the batch to be loaded.
		/// </summary>
        internal readonly string batchId;

        /// <summary>
        /// MES Batch Information.
        /// </summary>
        internal readonly MESbatch mesBatch;
        
		#endregion
	}
}

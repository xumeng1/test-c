// [Copyright]
//
// Bookham Modular Test Engine
// Default Test Control Plug-in
//
// LoadBatchRequest.cs
// 
// Author: Joseph Olajubu
// Design: Test Control DD

using System;

namespace Bookham.TestLibrary.TestControl.GuiMessages
{
	/// <summary>
	/// Load Batch Request message. Sent from the Test control Plug-in worker 
	/// thread to the Test Control GUI, to get it to diplsy the Load Batch Controls
	/// in the Test Control Tab. 
	/// </summary>
	internal sealed class LoadBatchRequest
	{
		internal LoadBatchRequest()
		{
		}
	}
}

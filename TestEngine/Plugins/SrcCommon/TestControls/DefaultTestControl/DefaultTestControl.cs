// [Copyright]
//
// Bookham Modular Test Engine
// Default Test Control Plug-in
//
// Library/TestControls/DefaultTestControl/DefaultTestControl.cs
// 
// Author: Joseph Olajubu
// Design: Test Control DD

using System;
using System.Xml;
using System.Collections;
using Bookham.TestEngine.Framework.Exceptions;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.PluginInterfaces.TestControl;
using Bookham.TestEngine.PluginInterfaces.TestJig;
using Bookham.TestEngine.PluginInterfaces.MES;
using Bookham.TestLibrary.TestControl.GuiMessages;
using System.Collections.Specialized;


namespace Bookham.TestLibrary.TestControl
{
	/// <summary>
	/// Default Test Control Plug-in for the Test Engine Library.
	/// This Test Control Plug-in has the following features:
	/// - Separate GUIs for Load Batch and Batch Display.
    /// - The Plugin does not interact with MES or Test Jig. 
    /// - The user can specify Batch Id using Load Batch GUI.
    /// - Each batch has a single device. The DUT details are manually added by the user in the Batch Display. 
    ///   screen, including which Test Program to load.
	/// </summary>
	public class DefaultTestControl : ITestControl
	{

		/// <summary>
		/// Constructor. Initialises User control lists.
		/// </summary>
		public DefaultTestControl ()
		{
			this.batchCtlList = new Type[1];
			this.batchCtlList[0] = typeof (BatchViewCtl);

			this.testCtlList =  new Type[1];
			this.testCtlList[0] = typeof (LoadBatchDialogueCtl);
		}

		#region ITestControl Implementation.

		/// <summary>
		/// Get Property.
		/// The type of the Batch View GUI User Control
		/// </summary>
		public Type [] BatchContentsCtlTypeList
		{
			get
			{
				return (batchCtlList);
			}
		}

		/// <summary>
		/// Get Property.
		/// The type of the Test Control GUI User Control. Used here for Load Batch operations.
		/// </summary>
		public Type [] TestControlCtlTypeList
		{
			get
			{
				return (testCtlList);
			}
		}

		/// <summary>
		/// Get Property.
		/// This specifies whether the Test Control Plug-in operated in Manual or Automatic Mode.
		/// This is a Manual Mode Plug-in.
		/// </summary>
		public AutoManual TestType
		{
			get
			{
				return AutoManual.MANUAL;
			}
		}

		/// <summary>
		/// Load a batch. Called when the Core requests that a new batch be loaded from the MES, via the 
		/// Test Control Server.
		/// </summary>
		/// <param name="testEngine">Object implementing the ITestEngine Interface.</param>
		/// <param name="mes">Object implementing the IMES interface.</param>
		/// <param name="operatorName">The name of the currently logged in user.</param>
		/// <param name="operatorType">The privilege level of the currently logged in user.</param>
		/// <returns>The BatchId of the loaded batch.</returns>
		public string LoadBatch( ITestEngine testEngine, IMES mes, string operatorName, TestControlPrivilegeLevel operatorType )
		{
			
			batchID = "";

            // Send a load batch request message to the Test Control GUI, so that it can display 
            // the load batch dialogue.
            testEngine.SendToCtl(TestControlCtl.TestControl, typeof(LoadBatchDialogueCtl), new LoadBatchRequest());

			// Bring the Test Control Tab to the front of the GUI, and attract the user's attention.
			testEngine.PageToFront( TestControlCtl.TestControl);
			testEngine.SendStatusMsg( "Load a batch" );
			testEngine.GetUserAttention( TestControlCtl.TestControl);

			// Keep looping until a Batch is loaded.
            while (true)
            {
                // Wait for a message from the Test Control GUI.
                CtlMsg msg = testEngine.WaitForCtlMsg();

                // Process the recieved message.
                Type messageType = msg.Payload.GetType();

                if (messageType == typeof(LoadBatchResponse))
                {
                    // The user has specified the identifier of the batch that is to be loaded.
                    // Attempt to load the batch specified .

                    //Extract the BatchId from the recieved message.
                    LoadBatchResponse loadBatchResponse = (LoadBatchResponse)msg.Payload;
                    batchID = loadBatchResponse.batchId;

                    //Stop attracting the user's attention.
                    testEngine.CancelUserAttention(TestControlCtl.TestControl);

                    // Extract batch Information.						
                    loadedBatch = loadBatchResponse.mesBatch;

                    // Initialise a new Test Statuses array.
                    this.testStatuses = new TestStatus[loadedBatch.Count];

                    // Initialise each device to untested status.
                    int index = 0;

                    foreach (DUTObject dut in loadedBatch)
                    {
                        testEngine.SendToCtl(TestControlCtl.BatchView, typeof(BatchViewCtl), dut);
                        testStatuses[index++] = TestStatus.Untested;
                    }

                    // Send a load batch complete message to the Test Control GUI, so that it can hide 
                    // the load batch dialogue.
                    testEngine.SendToCtl(TestControlCtl.TestControl, typeof(LoadBatchDialogueCtl), new LoadBatchComplete());

                    //Update the Batch View GUI with the devices' test statuses within the batch.
                    UpdateTestStatuses message = new UpdateTestStatuses(testStatuses);
                    testEngine.SendToCtl(TestControlCtl.BatchView, typeof(BatchViewCtl), message);

                    //Batch and device information now loaded. Exit the loop here.
                    break;
                }
                else
                {
                    //Invalid message recieved.
                    testEngine.ErrorRaise("Invalid message recieved: " + msg.Payload.GetType().ToString());
                }
            }// while( true )
					
			return batchID;
		}

		/// <summary>
		/// Get the next available DUT from the Batch.
		/// </summary>
		/// <param name="testEngine">Object that implements the ITestEngine Interface.</param>
		/// <param name="testJigArg">Object tha implements ITestJig Interface.</param>
		/// <param name="operatorName">The name of the currently logged in user.</param>
		/// <param name="operatorType">Privilege level of the currently logged in user.</param>
		/// <returns>DUT object for the next selected device for test or Null if end of batch.</returns>
		public DUTObject GetNextDUT( ITestEngine testEngine, ITestJig testJigArg, string operatorName, TestControlPrivilegeLevel operatorType)
		{
			bool found  = false;
			string receivedSerialNumber = " ";

			//Grab the user's attention, and instruct them to select a DUT.
			testEngine.PageToFront( TestControlCtl.BatchView );
			testEngine.SendStatusMsg( "Select a DUT" );
			testEngine.GetUserAttention( TestControlCtl.BatchView );
			testEngine.SendToCtl( TestControlCtl.BatchView, typeof (BatchViewCtl), new SelectNextDUTRequest());

			// Select DUT - wait for message from BatchDisplay user ctl

			while (true)
			{
				CtlMsg msg = testEngine.WaitForCtlMsg();

				Type messageType = msg.Payload.GetType();
			
				if (messageType == typeof (EndOfBatchNotification))
				{
					// Exit here with a Null DUT object if we have reached the end of the batch.
					testEngine.CancelUserAttention( TestControlCtl.BatchView );
					return null;
				}
				else if (messageType == typeof (MarkedForRetestNotification))
				{
					// The user marked a device for retest. Update the internal test status for the device.
					MarkedForRetestNotification retestMessage = (MarkedForRetestNotification)msg.Payload;

					for (int i = 0; i < testStatuses.Length; i++)
					{
						if (loadedBatch[i].SerialNumber == retestMessage.serialNumber)
						{
							testStatuses[i] = TestStatus.MarkForRetest;
							// Break out of for loop, as we've found the deivce and updated it's status.
							break;
						}
					}
				}
				else if (messageType == typeof (SelectNextDUTResponse)) 
				{
					// The user has specified the serial number of the next device to be tested.
					SelectNextDUTResponse response = (SelectNextDUTResponse)msg.Payload;

					receivedSerialNumber = response.serialNumber;

					//Find the index of the DUT Object in the loaded batch. Store this in the 
					// currentDutIndex class private variable.
					int index = 0;
					

					foreach( DUTObject dutObject in loadedBatch )
					{
						if (dutObject.SerialNumber == receivedSerialNumber)
						{
							currentDutIndex = index; 
							found = true;
							// Found the DUT Object for the next device to be tested. Break out of the For loop.
							break;
						}

						index++;
					}	
			
					// Break out of the message recieve while (true) loop.
					break;
				}
				else
				{
					testEngine.ErrorRaise ("Test Control Plug-in Get Next DUT Operation - Invalid Message received: " + messageType.ToString());
				}
			} // While (true)
			
			if (!found)
			{
				//The required DUT is not in the bacth. This is a fatal error.
				testEngine.ErrorRaise("Get Next DUT: Invalid DUT Specified " + receivedSerialNumber);
			}

			// Get the DUT object.
			DUTObject dut = loadedBatch[currentDutIndex];

			// Stop attracting user attention
			testEngine.CancelUserAttention( TestControlCtl.BatchView );
			return dut;
		}


		/// <summary>
		/// Called after the test program has concluded, to decide what the outgoing 
		/// Part ID and other attributes are, having inspected the test results.
		/// </summary>
		/// <param name="testEngine">Reference to object implmenting ITestEngine</param>
		/// <param name="programStatus">The Status of the Test Program Run (succeeded/error/aborted etc).</param>
		/// <param name="testResults">Detailed test results</param>
		/// <returns>Outcome of the results analysis.</returns>
		public DUTOutcome PostDevice( ITestEngine testEngine,  ProgramStatus programStatus, TestResults testResults )
		{

			// This Test Control Plug-in only supports one specification. The Part Id 
			// is not transformed based on results. 

			// Store the results for use in the Post Device commit Phase.
			testEngine.SendStatusMsg( "Post Device" );
			this.testResults = testResults;
			this.programStatus = programStatus;

			// Extract the Specification name from the test results. Fill this in the DUToutcome.
			SpecResultsList specs = testResults.SpecResults;
			string []specNames  = new string[specs.Count];

			int i = 0;
			foreach (SpecResults specRes in specs)
			{
				specNames [i++] = specRes.Name;
			}


			DUTOutcome dutOutcome = new DUTOutcome ();

			// Fill in the first specification name from the results for the DUToutcome.
			dutOutcome.OutputSpecificationNames = new string[1];
			dutOutcome.OutputSpecificationNames[0] = specNames[0];


			return dutOutcome;
		}

		/// <summary>
		/// Commit the test status for the current DUT in the MES.
		/// </summary>
		/// <param name="testEngine">Reference to object that implements the ITestEngine Interface.</param>
        /// <param name="dataWriteError">True if there was a problem writing Test Results to the database.</param>
        public void PostDeviceCommit( ITestEngine testEngine, bool dataWriteError )
		{
			testEngine.SendStatusMsg( "Post Device Commit" );

			//Mark the Device as Tested. Also record it's pass fail status.
			if ((this.programStatus == ProgramStatus.Success) && (!dataWriteError))
			{
				//The Test Passed. Mark the device as completed.
				testStatuses [currentDutIndex] = TestStatus.Passed;
			}
            else if ((this.programStatus == ProgramStatus.Success) && (dataWriteError))
            {
                //Problem writing Test Data. Mark for retest.
                testStatuses[currentDutIndex] = TestStatus.MarkForRetest;
            }
            else if (this.programStatus == ProgramStatus.Failed ||
                     this.programStatus == ProgramStatus.NonParametricFailure)
            // We only want to condemn a device which is definitely duff, so assume all
            // other statuses leave the device as "untested".
            {
                testStatuses[currentDutIndex] = TestStatus.Failed;
            }
            else
            {
                testStatuses[currentDutIndex] = TestStatus.Untested;
            }

			UpdateTestStatuses message = new UpdateTestStatuses (testStatuses);
			testEngine.SendToCtl( TestControlCtl.BatchView, typeof (BatchViewCtl), message );
		}

		/// <summary>
		/// Trackout the Batch.
		/// </summary>
		/// <param name="testEngine">Reference to object that implements the ITestEngine Interface.</param>
		public void PostBatch( ITestEngine testEngine )
		{
			testEngine.SendStatusMsg( "Post Batch" );
		}

		/// <summary>
		/// Mark all devices in the Batch for retest.
		/// </summary>
		/// <param name="testEngine">Reference to object that implements the ITestEngine Interface.</param>
		public void RestartBatch( ITestEngine testEngine )
		{
			testEngine.SendStatusMsg( "Restart Batch" );

			for (int i = 0; i < testStatuses.Length; i++)
			{
				if (testStatuses[i] != TestStatus.Untested)
				{
					testStatuses[i] = TestStatus.MarkForRetest;
				}

				UpdateTestStatuses message = new UpdateTestStatuses (testStatuses);
				testEngine.SendToCtl( TestControlCtl.BatchView, typeof (BatchViewCtl), message );
			}
		}


		# endregion


		#region Private data

		Type [] batchCtlList;

		Type [] testCtlList;

		//Batch size for internally generated batches.
		private const int BatchSize = 1;
		
		// Object to hold the loaded batch.
		MESbatch loadedBatch; 

		// Internal store of Test Statuses for the current batch.
		TestStatus [] testStatuses;

		// The current BatchID
		string batchID = "";

		// The Index of the current DUT within the batch & Test Statuses array.
		int currentDutIndex;

		// The Program status, as stored in the Post Device operation.
		ProgramStatus programStatus;

		// Test results for the current DUT.
        TestResults testResults;

		#endregion

	}
}

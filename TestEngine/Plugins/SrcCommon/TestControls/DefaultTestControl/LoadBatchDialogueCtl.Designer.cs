// [Copyright]
//
// Bookham Test Engine
// $projectname$
//
// DefaultTestControl/LoadBatchDialogueCtl
// 
// Author: joseph.olajubu
// Design: TODO

namespace Bookham.TestLibrary.TestControl
{
    partial class LoadBatchDialogueCtl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoadBatchDialogueCtl));
            this.bookhamLogoPictureBox = new System.Windows.Forms.PictureBox();
            this.loadBatchScreenLabel = new System.Windows.Forms.Label();
            this.loadBatchLabel = new System.Windows.Forms.Label();
            this.loadBatchTextBox = new System.Windows.Forms.TextBox();
            this.partCodeLabel = new System.Windows.Forms.Label();
            this.partCodeTextBox = new System.Windows.Forms.TextBox();
            this.equipmentIdLabel = new System.Windows.Forms.Label();
            this.equipmentIdTextBox = new System.Windows.Forms.TextBox();
            this.serialNumberLabel = new System.Windows.Forms.Label();
            this.serialNumberTextBox = new System.Windows.Forms.TextBox();
            this.testStageLabel = new System.Windows.Forms.Label();
            this.testStageTextBox = new System.Windows.Forms.TextBox();
            this.trayPositionLabel = new System.Windows.Forms.Label();
            this.trayPositionTextBox = new System.Windows.Forms.TextBox();
            this.dutGroupBox = new System.Windows.Forms.GroupBox();
            this.loadBatchButton = new System.Windows.Forms.Button();
            this.testProgramNameLabel = new System.Windows.Forms.Label();
            this.testProgramNameTextBox = new System.Windows.Forms.TextBox();
            this.testProgramVersionLabel = new System.Windows.Forms.Label();
            this.testProgramVersionTextBox = new System.Windows.Forms.TextBox();
            this.messageLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.bookhamLogoPictureBox)).BeginInit();
            this.dutGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // bookhamLogoPictureBox
            // 
            this.bookhamLogoPictureBox.Image = ((System.Drawing.Image)(resources.GetObject("bookhamLogoPictureBox.Image")));
            this.bookhamLogoPictureBox.Location = new System.Drawing.Point(14, 3);
            this.bookhamLogoPictureBox.Name = "bookhamLogoPictureBox";
            this.bookhamLogoPictureBox.Size = new System.Drawing.Size(130, 44);
            this.bookhamLogoPictureBox.TabIndex = 3;
            this.bookhamLogoPictureBox.TabStop = false;
            // 
            // loadBatchScreenLabel
            // 
            this.loadBatchScreenLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadBatchScreenLabel.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.loadBatchScreenLabel.Location = new System.Drawing.Point(147, 13);
            this.loadBatchScreenLabel.Name = "loadBatchScreenLabel";
            this.loadBatchScreenLabel.Size = new System.Drawing.Size(687, 24);
            this.loadBatchScreenLabel.TabIndex = 4;
            this.loadBatchScreenLabel.Text = "Test Engine Default Test Control Plug-in: Load Device Under Test";
            // 
            // loadBatchLabel
            // 
            this.loadBatchLabel.AutoSize = true;
            this.loadBatchLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadBatchLabel.Location = new System.Drawing.Point(21, 55);
            this.loadBatchLabel.Name = "loadBatchLabel";
            this.loadBatchLabel.Size = new System.Drawing.Size(90, 20);
            this.loadBatchLabel.TabIndex = 5;
            this.loadBatchLabel.Text = "Batch ID: ";
            // 
            // loadBatchTextBox
            // 
            this.loadBatchTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadBatchTextBox.Location = new System.Drawing.Point(117, 52);
            this.loadBatchTextBox.Name = "loadBatchTextBox";
            this.loadBatchTextBox.Size = new System.Drawing.Size(167, 26);
            this.loadBatchTextBox.TabIndex = 6;
            // 
            // partCodeLabel
            // 
            this.partCodeLabel.AutoSize = true;
            this.partCodeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.partCodeLabel.Location = new System.Drawing.Point(7, 22);
            this.partCodeLabel.Name = "partCodeLabel";
            this.partCodeLabel.Size = new System.Drawing.Size(99, 20);
            this.partCodeLabel.TabIndex = 7;
            this.partCodeLabel.Text = "Part Code: ";
            // 
            // partCodeTextBox
            // 
            this.partCodeTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.partCodeTextBox.Location = new System.Drawing.Point(145, 16);
            this.partCodeTextBox.Name = "partCodeTextBox";
            this.partCodeTextBox.Size = new System.Drawing.Size(167, 26);
            this.partCodeTextBox.TabIndex = 8;
            // 
            // equipmentIdLabel
            // 
            this.equipmentIdLabel.AutoSize = true;
            this.equipmentIdLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.equipmentIdLabel.Location = new System.Drawing.Point(346, 25);
            this.equipmentIdLabel.Name = "equipmentIdLabel";
            this.equipmentIdLabel.Size = new System.Drawing.Size(129, 20);
            this.equipmentIdLabel.TabIndex = 9;
            this.equipmentIdLabel.Text = "Equipment ID: ";
            // 
            // equipmentIdTextBox
            // 
            this.equipmentIdTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.equipmentIdTextBox.Location = new System.Drawing.Point(472, 22);
            this.equipmentIdTextBox.Name = "equipmentIdTextBox";
            this.equipmentIdTextBox.Size = new System.Drawing.Size(167, 26);
            this.equipmentIdTextBox.TabIndex = 10;
            // 
            // serialNumberLabel
            // 
            this.serialNumberLabel.AutoSize = true;
            this.serialNumberLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serialNumberLabel.Location = new System.Drawing.Point(7, 51);
            this.serialNumberLabel.Name = "serialNumberLabel";
            this.serialNumberLabel.Size = new System.Drawing.Size(132, 20);
            this.serialNumberLabel.TabIndex = 11;
            this.serialNumberLabel.Text = "Serial Number: ";
            // 
            // serialNumberTextBox
            // 
            this.serialNumberTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serialNumberTextBox.Location = new System.Drawing.Point(145, 48);
            this.serialNumberTextBox.Name = "serialNumberTextBox";
            this.serialNumberTextBox.Size = new System.Drawing.Size(167, 26);
            this.serialNumberTextBox.TabIndex = 12;
            // 
            // testStageLabel
            // 
            this.testStageLabel.AutoSize = true;
            this.testStageLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.testStageLabel.Location = new System.Drawing.Point(346, 63);
            this.testStageLabel.Name = "testStageLabel";
            this.testStageLabel.Size = new System.Drawing.Size(107, 20);
            this.testStageLabel.TabIndex = 14;
            this.testStageLabel.Text = "Test Stage: ";
            // 
            // testStageTextBox
            // 
            this.testStageTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.testStageTextBox.Location = new System.Drawing.Point(472, 60);
            this.testStageTextBox.Name = "testStageTextBox";
            this.testStageTextBox.Size = new System.Drawing.Size(167, 26);
            this.testStageTextBox.TabIndex = 15;
            // 
            // trayPositionLabel
            // 
            this.trayPositionLabel.AutoSize = true;
            this.trayPositionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trayPositionLabel.Location = new System.Drawing.Point(7, 81);
            this.trayPositionLabel.Name = "trayPositionLabel";
            this.trayPositionLabel.Size = new System.Drawing.Size(122, 20);
            this.trayPositionLabel.TabIndex = 16;
            this.trayPositionLabel.Text = "Tray Position: ";
            // 
            // trayPositionTextBox
            // 
            this.trayPositionTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trayPositionTextBox.Location = new System.Drawing.Point(145, 81);
            this.trayPositionTextBox.Name = "trayPositionTextBox";
            this.trayPositionTextBox.Size = new System.Drawing.Size(167, 26);
            this.trayPositionTextBox.TabIndex = 17;
            // 
            // dutGroupBox
            // 
            this.dutGroupBox.Controls.Add(this.testStageTextBox);
            this.dutGroupBox.Controls.Add(this.trayPositionTextBox);
            this.dutGroupBox.Controls.Add(this.testStageLabel);
            this.dutGroupBox.Controls.Add(this.partCodeLabel);
            this.dutGroupBox.Controls.Add(this.trayPositionLabel);
            this.dutGroupBox.Controls.Add(this.equipmentIdTextBox);
            this.dutGroupBox.Controls.Add(this.partCodeTextBox);
            this.dutGroupBox.Controls.Add(this.equipmentIdLabel);
            this.dutGroupBox.Controls.Add(this.serialNumberLabel);
            this.dutGroupBox.Controls.Add(this.serialNumberTextBox);
            this.dutGroupBox.Location = new System.Drawing.Point(14, 84);
            this.dutGroupBox.Name = "dutGroupBox";
            this.dutGroupBox.Size = new System.Drawing.Size(654, 111);
            this.dutGroupBox.TabIndex = 18;
            this.dutGroupBox.TabStop = false;
            this.dutGroupBox.Text = "Device Under Test Data";
            // 
            // loadBatchButton
            // 
            this.loadBatchButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadBatchButton.Location = new System.Drawing.Point(686, 109);
            this.loadBatchButton.Name = "loadBatchButton";
            this.loadBatchButton.Size = new System.Drawing.Size(131, 86);
            this.loadBatchButton.TabIndex = 19;
            this.loadBatchButton.Text = "Load Device Under Test";
            this.loadBatchButton.UseVisualStyleBackColor = true;
            this.loadBatchButton.Click += new System.EventHandler(this.loadBatchButton_Click);
            // 
            // testProgramNameLabel
            // 
            this.testProgramNameLabel.AutoSize = true;
            this.testProgramNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.testProgramNameLabel.Location = new System.Drawing.Point(10, 198);
            this.testProgramNameLabel.Name = "testProgramNameLabel";
            this.testProgramNameLabel.Size = new System.Drawing.Size(177, 20);
            this.testProgramNameLabel.TabIndex = 18;
            this.testProgramNameLabel.Text = "Test Program Name: ";
            // 
            // testProgramNameTextBox
            // 
            this.testProgramNameTextBox.Location = new System.Drawing.Point(204, 200);
            this.testProgramNameTextBox.Name = "testProgramNameTextBox";
            this.testProgramNameTextBox.Size = new System.Drawing.Size(285, 20);
            this.testProgramNameTextBox.TabIndex = 19;
            // 
            // testProgramVersionLabel
            // 
            this.testProgramVersionLabel.AutoSize = true;
            this.testProgramVersionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.testProgramVersionLabel.Location = new System.Drawing.Point(10, 224);
            this.testProgramVersionLabel.Name = "testProgramVersionLabel";
            this.testProgramVersionLabel.Size = new System.Drawing.Size(192, 20);
            this.testProgramVersionLabel.TabIndex = 20;
            this.testProgramVersionLabel.Text = "Test Program Version: ";
            // 
            // testProgramVersionTextBox
            // 
            this.testProgramVersionTextBox.Location = new System.Drawing.Point(204, 226);
            this.testProgramVersionTextBox.Name = "testProgramVersionTextBox";
            this.testProgramVersionTextBox.Size = new System.Drawing.Size(210, 20);
            this.testProgramVersionTextBox.TabIndex = 21;
            // 
            // messageLabel
            // 
            this.messageLabel.AutoSize = true;
            this.messageLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.messageLabel.ForeColor = System.Drawing.Color.Red;
            this.messageLabel.Location = new System.Drawing.Point(290, 60);
            this.messageLabel.Name = "messageLabel";
            this.messageLabel.Size = new System.Drawing.Size(329, 13);
            this.messageLabel.TabIndex = 22;
            this.messageLabel.Text = "Please enter a Batch ID and device details for the test.  ";
            // 
            // LoadBatchDialogueCtl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.messageLabel);
            this.Controls.Add(this.testProgramVersionTextBox);
            this.Controls.Add(this.testProgramVersionLabel);
            this.Controls.Add(this.testProgramNameLabel);
            this.Controls.Add(this.testProgramNameTextBox);
            this.Controls.Add(this.loadBatchButton);
            this.Controls.Add(this.loadBatchTextBox);
            this.Controls.Add(this.loadBatchLabel);
            this.Controls.Add(this.loadBatchScreenLabel);
            this.Controls.Add(this.bookhamLogoPictureBox);
            this.Controls.Add(this.dutGroupBox);
            this.Name = "LoadBatchDialogueCtl";
            this.MsgReceived += new Bookham.TestEngine.Framework.Messages.ManagedCtlBase.MsgReceivedDlg(this.LoadBatchDialogueCtl_MsgReceived);
            ((System.ComponentModel.ISupportInitialize)(this.bookhamLogoPictureBox)).EndInit();
            this.dutGroupBox.ResumeLayout(false);
            this.dutGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox bookhamLogoPictureBox;
        private System.Windows.Forms.Label loadBatchScreenLabel;
        private System.Windows.Forms.Label loadBatchLabel;
        private System.Windows.Forms.TextBox loadBatchTextBox;
        private System.Windows.Forms.Label partCodeLabel;
        private System.Windows.Forms.TextBox partCodeTextBox;
        private System.Windows.Forms.Label equipmentIdLabel;
        private System.Windows.Forms.TextBox equipmentIdTextBox;
        private System.Windows.Forms.Label serialNumberLabel;
        private System.Windows.Forms.TextBox serialNumberTextBox;
        private System.Windows.Forms.Label testStageLabel;
        private System.Windows.Forms.TextBox testStageTextBox;
        private System.Windows.Forms.Label trayPositionLabel;
        private System.Windows.Forms.TextBox trayPositionTextBox;
        private System.Windows.Forms.GroupBox dutGroupBox;
        private System.Windows.Forms.Button loadBatchButton;
        private System.Windows.Forms.Label testProgramNameLabel;
        private System.Windows.Forms.TextBox testProgramNameTextBox;
        private System.Windows.Forms.Label testProgramVersionLabel;
        private System.Windows.Forms.TextBox testProgramVersionTextBox;
        private System.Windows.Forms.Label messageLabel;
    }
}

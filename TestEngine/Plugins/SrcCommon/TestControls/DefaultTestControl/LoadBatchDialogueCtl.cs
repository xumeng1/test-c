// [Copyright]
//
// Bookham Test Engine
// Default Test Control Plug-in
//
// DefaultTestControl/LoadBatchDialogueCtl.cs
// 
// Author: Joseph Olajubu
// Design: Test Control DD

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Bookham.TestLibrary.TestControl.GuiMessages;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.PluginInterfaces.MES;

namespace Bookham.TestLibrary.TestControl
{
    /// <summary>
    /// Default Test Control View User Control
    /// </summary>
    public partial class LoadBatchDialogueCtl : Bookham.TestEngine.Framework.Messages.ManagedCtlBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public LoadBatchDialogueCtl()
        {
            /* Call designer generated code. */
            InitializeComponent();
        }


        /// <summary>
        /// Process recieved messages from the worker thread.
        /// </summary>
        /// <param name="payload">Message Payload</param>
        /// <param name="inMsgSeq">Incoming sequence number</param>
        /// <param name="respSeq">Response sequence number</param>
        private void LoadBatchDialogueCtl_MsgReceived(object payload, long inMsgSeq, long respSeq)
        {
            // Find out what type of message has arrived, and process it appropriately.
            Type messageType = payload.GetType();

            if (messageType == typeof(LoadBatchRequest))
            {
                // The Test Control Plug-in wants to load a batch. 
                // Display the GUI controls and prompt the user.
                this.messageLabel.Text = "Please enter a Batch ID and device details for the test.";

                //Enable the Load Batch dialogue.
                this.loadBatchTextBox.Text = "";
                displayControls();
                this.loadBatchButton.Enabled = true;
            }
            else if (messageType == typeof(LoadBatchComplete))
            {
                // The batch has been loaded. Hide the controls and prepare for the 
                // next Load Batch operation.
                this.loadBatchTextBox.Text = "";
                this.messageLabel.Text = "";
                hideControls();
            }
            else if (messageType == typeof(string))
            {
                // Display the string contained in the message. 
                this.messageLabel.Text = (string)payload;
            }
            else
            {
                //Error - invalid message.

            }
        }

        /// <summary>
        /// Dispay normal load batch screen controls.
        /// </summary>
        private void displayControls()
        {
            this.loadBatchScreenLabel.Show();
            this.bookhamLogoPictureBox.Show();
            showLoadBatchControls();
        }

        /// <summary>
        /// Hide All  Controls
        /// </summary>
        private void hideControls()
        {
            this.loadBatchScreenLabel.Hide();
            this.bookhamLogoPictureBox.Hide();
            hideLoadBatchControls();
        }

        /// <summary>
        /// Hide Load Batch operation associated controls.
        /// </summary>
        private void hideLoadBatchControls()
        {
            this.loadBatchTextBox.Hide();
            this.loadBatchButton.Hide();
            this.messageLabel.Hide();
            this.partCodeLabel.Hide();
            this.partCodeTextBox.Hide();
            this.serialNumberLabel.Hide();
            this.serialNumberTextBox.Hide();
            this.trayPositionLabel.Hide();
            this.trayPositionTextBox.Hide();
            this.equipmentIdLabel.Hide();
            this.equipmentIdTextBox.Hide();
            this.testStageLabel.Hide();
            this.testStageTextBox.Hide();
            this.testProgramNameLabel.Hide();
            this.testProgramNameTextBox.Hide();
            this.testProgramVersionLabel.Hide();
            this.testProgramVersionTextBox.Hide();
            this.loadBatchLabel.Hide();
            this.dutGroupBox.Hide();
        }

        /// <summary>
        /// Show Load Batch operation associated controls.
        /// </summary>
        private void showLoadBatchControls()
        {
            this.loadBatchTextBox.Show();
            this.loadBatchButton.Show();
            this.messageLabel.Show();
            this.partCodeLabel.Show();
            this.partCodeTextBox.Show();
            this.serialNumberLabel.Show();
            this.serialNumberTextBox.Show();
            this.trayPositionLabel.Show();
            this.trayPositionTextBox.Show();
            this.equipmentIdLabel.Show();
            this.equipmentIdTextBox.Show();
            this.testStageLabel.Show();
            this.testStageTextBox.Show();
            this.testProgramNameLabel.Show();
            this.testProgramNameTextBox.Show();
            this.testProgramVersionLabel.Show();
            this.testProgramVersionTextBox.Show();
            this.loadBatchLabel.Show();
            this.dutGroupBox.Show();
        }

        /// <summary>
        /// Disable Load Batch operation associated controls.
        /// </summary>
        private void disableLoadBatchControls()
        {
            this.loadBatchTextBox.Enabled = false;
            this.loadBatchButton.Enabled = false;
        }

        /// <summary>
        /// Enable Load Batch operation associated controls.
        /// </summary>
        private void enableLoadBatchControls()
        {
            this.loadBatchTextBox.Enabled = true;
            this.loadBatchButton.Enabled = true;
        }



        /// <summary>
        /// The User clicked the load batch button. Send a message to the worker thread, with the 
        /// user's desired Batch ID and DUT Object data.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">event parameters.</param>
        private void loadBatchButton_Click(object sender, EventArgs e)
        {
            // Do not allow empty boxes.
            if ((this.loadBatchTextBox.Text.Length != 0)&&
                (this.partCodeTextBox.Text.Length != 0) &&
                (this.serialNumberTextBox.Text.Length != 0) &&
                (this.trayPositionTextBox.Text.Length != 0) &&
                (this.equipmentIdTextBox.Text.Length != 0 ) &&
                (this.testStageTextBox.Text.Length != 0 ) &&
                (this.testProgramNameTextBox.Text.Length != 0))
            {
                // Disable the button
                this.loadBatchButton.Enabled = false;
                batch = new MESbatch();
                batch.PartCode = this.partCodeTextBox.Text;
                batch.Stage = this.testStageTextBox.Text;

                dut = new DUTObject();
                dut.PartCode = this.partCodeTextBox.Text;
                dut.OverrideSimulationMode = false;
                dut.IsSimulation = false;
                dut.ContinueOnFail = false;
                dut.BatchID = this.loadBatchTextBox.Text;
                dut.EquipmentID = this.equipmentIdTextBox.Text;
                dut.FirstInBatch = true;
                dut.LastInBatch = true;
                dut.SerialNumber = this.serialNumberTextBox.Text;
                dut.TestStage = this.testStageTextBox.Text;
                dut.TrayPosition = this.trayPositionTextBox.Text;
                dut.ProgramPluginName = this.testProgramNameTextBox.Text;
                dut.ProgramPluginVersion = this.testProgramVersionTextBox.Text;
                batch.Add(dut);

                // Grab the Text from the load batch text box and message the worker with it's contents.
                LoadBatchResponse message = new LoadBatchResponse(this.loadBatchTextBox.Text, batch);
                this.sendToWorker(message);
            }
        }

        private MESbatch batch;
        private DUTObject dut;

    }
}

// [Copyright]
//
// Bookham Modular Test Engine
// Default Test Control Plug-in
//
// DefaultTestControlEnums.cs
// 
// Author: Joseph Olajubu
// Design: Test Control DD


using System;

namespace Bookham.TestLibrary.TestControl
{
	/// <summary>
	/// Test Status of devices within a batch
	/// </summary>
	internal enum TestStatus
	{
		/// <summary>
		/// Untested.
		/// </summary>
		Untested,

		/// <summary>
		/// Marked for restest.
		/// </summary>
		MarkForRetest,

		/// <summary>
		/// Passed.
		/// </summary>
		Passed,

		/// <summary>
		/// Failed.
		/// </summary>
		Failed
	}
}

// Author: Chongjian.Liang 2016.09.16

namespace Bookham.TestLibrary.Utilities
{
    public enum FailModeCategory_Enum
    {
        SW,
        HW,
        FW,
        NW,
        PR,
        OP,
        UN
    }
}

// Author: Chongjian.Liang 2016.09.16

namespace Bookham.TestLibrary.Utilities
{
    public class FailModeBase
    {
        public string Descriptor;
        public FailModeCategory_Enum Category;
        public string Detail;

        public FailModeBase(string descriptor, FailModeCategory_Enum category)
        {
            this.Descriptor = descriptor;
            this.Category = category;
        }

        public FailModeBase(string descriptor, FailModeCategory_Enum category, string details)
        {
            this.Descriptor = descriptor;
            this.Category = category;
            this.Detail = details;
        }
    }

    public class FailMode : FailModeBase
    {
        public const char WILDCARD = '*';

        public const int CONFIG_COL_AMOUNT = 3;
        public const int CONFIG_COL_INDEX_PCAS_EXTERNAL_NAME = 0;
        public const int CONFIG_COL_INDEX_FAILMODE_CATEGORY = 1;
        public const int CONFIG_COL_INDEX_FAILMODE_DETAILS = 2;
        public const int CONFIG_HEADER_LINE_AMOUNT = 1;
        public const char CONFIG_PCAS_FILE_START_MARK = '*';
        public const int CONFIG_DETAIL_INNER_ITEM_COUNT = 4;
        public const char CONFIG_DETAIL_INNER_ITEM_STARTER = '|';
        public const char CONFIG_DETAIL_INNER_ITEM_VALUE_SPLITTER = '=';

        public const char CATEGORY_STARTER = '#';
        public const char CATEGORY_START = '{';
        public const char CATEGORY_END = '}';

        public const char ITEM_STARTER = '@';
        public const char ITEM_DESCRIPTOR_DETAIL_SPLITTER = ':';
        public const char ITEM_LIMIT_START = '[';
        public const char ITEM_LIMIT_END = ']';
        public const char ITEM_LIMIT_SPLITTER = ',';
        public const char ITEM_LIMIT_VALUE_SPLITTER = '=';

        public const char PCAS_FILE_FAILMODE_ITEM_STARTER = '~';
        public const char PCAS_FILE_FAILMODE_ITEM_VALUE_SPLITTER = ':';
        public const char PCAS_FILE_FAILMODE_ENCLOSURE_START = '<';
        public const char PCAS_FILE_FAILMODE_ENCLOSURE_END = '>';

        public const int PCAS_ITEM_RESULT_PRECISION = 5;

        public const int FAILMODE_LIMIT_LENGTH = 256;
        public const string FAILMODE_LIMIT_NAME = "FACTORY_WORKS_FAILMODE";
        public const string FAILMODE_LIMIT_VALUE_FAIL = "FAILED";
        public const string FAILMODE_LIMIT_VALUE_PASS = "PASSED";
        public const string FAILMODE_LIMIT_VALUE_PASS_NO_PCAS_LIMITS = "PASSED(No PCAS Limit)";

        public const string TC_FAILMODE_RESULT_KEY = "TC_FAILMODE_RESULT";

        private string LimitValue = string.Empty;

        public FailMode(string descriptor, FailModeCategory_Enum category)
            : base(descriptor, category)
        {
        }

        public FailMode(string descriptor, FailModeCategory_Enum category, string details)
            : base(descriptor, category, details)
        {
        }

        public FailMode(FailModeBase failModeBase, string value, string lowLimit, string highLimit)
            : base(failModeBase.Descriptor, failModeBase.Category, failModeBase.Detail)
        {
            this.LimitValue = ITEM_LIMIT_START + lowLimit + ITEM_LIMIT_SPLITTER + highLimit + ITEM_LIMIT_END + ITEM_LIMIT_VALUE_SPLITTER + value;
        }

        public FailMode(FailModeBase failModeBase, double value, double lowLimit, double highLimit, int accuracyFactor)
            : base(failModeBase.Descriptor, failModeBase.Category, failModeBase.Detail)
        {
            string accuracyFormat = "F" + accuracyFactor.ToString();

            this.LimitValue = ITEM_LIMIT_START + lowLimit.ToString(accuracyFormat) + ITEM_LIMIT_SPLITTER + highLimit.ToString(accuracyFormat) + ITEM_LIMIT_END + ITEM_LIMIT_VALUE_SPLITTER + value.ToString(accuracyFormat);
        }

        public override string ToString()
        {
            if (string.IsNullOrEmpty(this.Detail))
            {
                return ITEM_STARTER + this.Descriptor + this.LimitValue;
            }

            return ITEM_STARTER + this.Descriptor + this.LimitValue + ITEM_DESCRIPTOR_DETAIL_SPLITTER + this.Detail;
        }
    }
}
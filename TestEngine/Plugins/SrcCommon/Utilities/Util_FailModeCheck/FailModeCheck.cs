﻿// Author: Chongjian.Liang 2016.09.16

using System;
using System.IO;
using System.Collections.Generic;
using Bookham.TestEngine.Framework.Limits;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.PluginInterfaces.Module;

namespace Bookham.TestLibrary.Utilities
{
    public class FailModeCheck
    {
        private Dictionary<string, FailModeBase> __pcasFailModeCollection;
        private Dictionary<FailModeCause_Enum, List<FailMode>> __failModeResults;
        private bool __IS_SUPPRESS_SELF_ERROR = true;

        public FailModeCheck()
        {
            __pcasFailModeCollection = new Dictionary<string, FailModeBase>();
            __failModeResults = new Dictionary<FailModeCause_Enum, List<FailMode>>();
        }

        /// <summary>
        /// Set FACTORY_WORKS_FAILMODE with Main spec failed parameters and NonParamFail info from TestControl and Program
        /// </summary>
        /// <param name="engine">ITestEngineDataWrite</param>
        /// <param name="dutObject">The failure mode from TC must store as DUTObject.Attributes[key:TC_FAILMODE_RESULT, value:List^FailMode^]; No TC failure mode check when TC_FAILMODE_RESULT is not present</param>
        /// <param name="spec">The main spec used in current program</param>
        /// <param name="pcasFailMode_FilePath">File path that contains PcasFailMode match info; null or empty if no PCAS limits or do not check PCAS failure mode</param>
        /// <param name="traceData">The trace data to set FACTORY_WORKS_FAILMODE to; traceData MUST contain the file link defined in pcasFailMode_File</param>
        public void CheckMainSpec(ITestEngineDataWrite engine, DUTObject dutObject, Specification spec, string pcasFailMode_FilePath, ref DatumList traceData)
        {
            this.CheckSpec(engine, dutObject, spec, true, pcasFailMode_FilePath, ref traceData);
        }

        /// <summary>
        /// Set FACTORY_WORKS_FAILMODE with sub spec failed parameters
        /// </summary>
        /// <param name="engine">ITestEngineDataWrite</param>
        /// <param name="spec">The sub spec used in current program</param>
        /// <param name="pcasFailMode_FilePath">File path that contains PcasFailMode match info; null or empty if no PCAS limits or do not check PCAS failure mode</param>
        /// <param name="traceData">The trace data to set FACTORY_WORKS_FAILMODE to; traceData MUST contain the file link defined in pcasFailMode_File</param>
        public void CheckSubSpec(ITestEngineDataWrite engine, Specification spec, string pcasFailMode_FilePath, ref DatumList traceData)
        {
            this.CheckSpec(engine, null, spec, false, pcasFailMode_FilePath, ref traceData);
        }

        /// <summary>
        /// Set FACTORY_WORKS_FAILMODE with spec failed parameters, and NonParamFail info from TestControl and Program if isMainSpec
        /// </summary>
        /// <param name="engine">ITestEngineDataWrite</param>
        /// <param name="dutObject">The failure mode from TC must store as DUTObject.Attributes[key:TC_FAILMODE_RESULT, value:List^FailMode^]; No TC failure mode check when TC_FAILMODE_RESULT is not present</param>
        /// <param name="spec">The main spec used in current program</param>
        /// <param name="isMainSpec">If this specification is the main spec</param>
        /// <param name="pcasFailMode_FilePath">File path that contains PcasFailMode match info; null or empty if no PCAS limits or do not check PCAS failure mode</param>
        /// <param name="traceData">The trace data to set FACTORY_WORKS_FAILMODE to; traceData MUST contain the file link defined in pcasFailMode_File</param>
        private void CheckSpec(ITestEngineDataWrite engine, DUTObject dutObject, Specification spec, bool isMainSpec, string pcasFailMode_FilePath, ref DatumList traceData)
        {
            if (engine == null
                || spec == null
                || !spec.ParamLimitExists(FailMode.FAILMODE_LIMIT_NAME))
            {
                return;
            }

            if (traceData == null)
            {
                this.RaiseSelf_Prog_NonParamFail(engine, "The traceData for FailModeCheck must not be null.", FailModeCategory_Enum.SW);
            }

            bool isPCASLimitsAvailable;

            this.Load_PCASFailModeListCollection(engine, pcasFailMode_FilePath, out isPCASLimitsAvailable);

            if (isMainSpec)
            {
                this.Check_TC_NonParamFail(engine, dutObject);
            }

            this.Check_Prog_ParamFail(engine, spec, isPCASLimitsAvailable);
            this.Check_Prog_ParamFileFail(engine, spec, isPCASLimitsAvailable, traceData);

            string failModeResult_Str;

            this.ConvertToStr_FACTORY_WORKS_FAILMODE(isMainSpec, out failModeResult_Str);

            this.Conclude_FACTORY_WORKS_FAILMODE(engine, spec, isPCASLimitsAvailable, ref failModeResult_Str);

            this.SetToTraceData_FACTORY_WORKS_FAILMODE(isMainSpec, failModeResult_Str, ref traceData);
        }

        private void Load_PCASFailModeListCollection(ITestEngineBase engine, string pcasFailMode_FilePath, out bool isPCASLimitsAvailable)
        {
            isPCASLimitsAvailable = true;

            if (string.IsNullOrEmpty(pcasFailMode_FilePath))
            {
                isPCASLimitsAvailable = false;
                return;
            }

            if (!File.Exists(pcasFailMode_FilePath))
            {
                this.RaiseSelf_Prog_NonParamFail(engine, string.Format("The file {0} doesn't exist.", pcasFailMode_FilePath), FailModeCategory_Enum.SW);
                return;
            }

            List<string[]> pcasFailMode_FileContent = null;
            {
                using (CsvReader csvReader = new CsvReader())
                {
                    pcasFailMode_FileContent = csvReader.ReadFile(pcasFailMode_FilePath);
                }

                if (pcasFailMode_FileContent.Count <= FailMode.CONFIG_HEADER_LINE_AMOUNT)
                {
                    isPCASLimitsAvailable = false;
                    return;
                }

                for (int i = 0; i < FailMode.CONFIG_HEADER_LINE_AMOUNT; i++)
                {
                    pcasFailMode_FileContent.RemoveAt(0);
                }
            }

            // Set __pcasFailModeCollection
            {
                if (__pcasFailModeCollection == null)
                {
                    __pcasFailModeCollection = new Dictionary<string, FailModeBase>();
                }
                else if (__pcasFailModeCollection.Count > 0)
                {
                    __pcasFailModeCollection.Clear();
                }

                foreach (string[] lineContent in pcasFailMode_FileContent)
                {
                    if (lineContent == null
                        || lineContent.Length < FailMode.CONFIG_COL_AMOUNT)
                    {
                        continue;
                    }

                    string pcasExternalName = lineContent[FailMode.CONFIG_COL_INDEX_PCAS_EXTERNAL_NAME].Trim();
                    string pcasFailModeCategory_Str = lineContent[FailMode.CONFIG_COL_INDEX_FAILMODE_CATEGORY].Trim();
                    string pcasFailModeDetails = lineContent[FailMode.CONFIG_COL_INDEX_FAILMODE_DETAILS].Trim();

                    if (string.IsNullOrEmpty(pcasExternalName))
                    {
                        continue;
                    }

                    FailModeCategory_Enum pcasFailModeCategory = FailModeCategory_Enum.UN;
                    {
                        try
                        {
                            pcasFailModeCategory = (FailModeCategory_Enum)Enum.Parse(typeof(FailModeCategory_Enum), pcasFailModeCategory_Str);
                        }
                        catch
                        {
                            this.RaiseSelf_Prog_NonParamFail(engine, string.Format("\"{0}\" is not a legal FailModeCategory.", pcasFailModeCategory_Str), FailModeCategory_Enum.SW);
                            return;
                        }
                    }

                    __pcasFailModeCollection.Add(pcasExternalName, new FailModeBase(pcasExternalName, pcasFailModeCategory, pcasFailModeDetails));
                }

                if (__pcasFailModeCollection.Count <= 0)
                {
                    isPCASLimitsAvailable = false;
                }
            }
        }

        private void Check_TC_NonParamFail(ITestEngineBase engine, DUTObject dutObject)
        {
            if (dutObject == null
                || dutObject.Attributes == null
                || !dutObject.Attributes.IsPresent(FailMode.TC_FAILMODE_RESULT_KEY))
            {
                return;
            }

            List<FailMode> failModeResult = dutObject.Attributes.ReadReference(FailMode.TC_FAILMODE_RESULT_KEY) as List<FailMode>;

            if (failModeResult == null)
            {
                return;
            }

            if (!__failModeResults.ContainsKey(FailModeCause_Enum.NonParamFail))
            {
                __failModeResults.Add(FailModeCause_Enum.NonParamFail, failModeResult);
            }
            else
            {
                __failModeResults[FailModeCause_Enum.NonParamFail] = failModeResult;
            }
        }

        private void Check_Prog_ParamFail(ITestEngineBase engine, Specification spec, bool isPCASLimitsAvailable)
        {
            if (!isPCASLimitsAvailable
                || spec.Status.Status == PassFail.Pass)
            {
                return;
            }

            // spec.Status.IsComplete is true when all limits & trace data is test & set complete, so we cannot catch the case when some limits are not tested
            //if (spec.Status.IsComplete != SpecComplete.Complete) {
            //    __failModeResults[FailModeResultCategory_Enum.Prog_ParamFail].Add(new FailMode("The test is incomplete.", FailModeCategory_Enum.UN));
            //}

            // Init or empty Prog_ParamFail for each spec
            if (!__failModeResults.ContainsKey(FailModeCause_Enum.ParamFail))
            {
                __failModeResults.Add(FailModeCause_Enum.ParamFail, new List<FailMode>());
            }
            else if (__failModeResults[FailModeCause_Enum.ParamFail].Count > 0)
            {
                __failModeResults[FailModeCause_Enum.ParamFail].Clear();
            }

            foreach (ParamLimit paramLimit in spec)
            {
                if (paramLimit.Tested
                    && paramLimit.TestResult == PassFail.Fail)
                {

                    if (!__pcasFailModeCollection.ContainsKey(paramLimit.ExternalName))
                    {
                        this.RaiseSelf_Prog_NonParamFail(engine, string.Format("The PCASFailMode file doesn't contain PCAS parameter {0}.", paramLimit.ExternalName), FailModeCategory_Enum.SW);
                        continue;
                    }

                    double paramLimitResult;

                    if (double.TryParse(paramLimit.MeasuredData.ValueToString(), out paramLimitResult))
                    {
                        __failModeResults[FailModeCause_Enum.ParamFail].Add(new FailMode(__pcasFailModeCollection[paramLimit.ExternalName], Math.Round(paramLimitResult, FailMode.PCAS_ITEM_RESULT_PRECISION).ToString(), paramLimit.LowLimit.ValueToString(), paramLimit.HighLimit.ValueToString()));
                    }
                    else
                    {
                        __failModeResults[FailModeCause_Enum.ParamFail].Add(new FailMode(__pcasFailModeCollection[paramLimit.ExternalName], paramLimit.MeasuredData.ValueToString(), paramLimit.LowLimit.ValueToString(), paramLimit.HighLimit.ValueToString()));
                    }
                }
            }
        }

        private void Check_Prog_ParamFileFail(ITestEngineBase engine, Specification spec, bool isPCASLimitsAvailable, DatumList traceData)
        {
            if (!isPCASLimitsAvailable
                || spec.Status.Status == PassFail.Pass)
            {
                return;
            }

            Dictionary<string, FailModeBase> pcasFileFailModeCollection = new Dictionary<string, FailModeBase>();

            foreach (string paramLimit_ExternalName in __pcasFailModeCollection.Keys)
            {
                if (paramLimit_ExternalName.StartsWith(FailMode.CONFIG_PCAS_FILE_START_MARK.ToString()))
                {
                    pcasFileFailModeCollection.Add(paramLimit_ExternalName.TrimStart(FailMode.CONFIG_PCAS_FILE_START_MARK), __pcasFailModeCollection[paramLimit_ExternalName]);
                }
            }

            if (pcasFileFailModeCollection.Count <= 0)
            {
                return;
            }

            // Init or empty Prog_ParamFileFail for each spec
            if (!__failModeResults.ContainsKey(FailModeCause_Enum.ParamFileFail))
            {
                __failModeResults.Add(FailModeCause_Enum.ParamFileFail, new List<FailMode>());
            }
            else if (__failModeResults[FailModeCause_Enum.ParamFileFail].Count > 0)
            {
                __failModeResults[FailModeCause_Enum.ParamFileFail].Clear();
            }

            foreach (string pcasFile_ExternalName in pcasFileFailModeCollection.Keys)
            {
                bool isPCASFileExistInSpec = false;

                foreach (ParamLimit paramLimit in spec)
                {
                    if (pcasFile_ExternalName == paramLimit.ExternalName)
                    {
                        isPCASFileExistInSpec = true;

                        if (!traceData.IsPresent(pcasFile_ExternalName))
                        {
                            // Ignore the file missing when error occurs.
                            if (__failModeResults.ContainsKey(FailModeCause_Enum.NonParamFail)
                                && __failModeResults[FailModeCause_Enum.NonParamFail].Count > 0)
                            {
                                break;
                            }
                            else
                            {
                                this.RaiseSelf_Prog_NonParamFail(engine, string.Format("The PCASFailMode file contains a PCAS file parameter \"{0}\" that is not present in trace data.", pcasFile_ExternalName), FailModeCategory_Enum.SW);
                                return;
                            }
                        }

                        string pcasFilePath = null;
                        {
                            DatumType pcasFileDatumType = traceData.GetDatumType(pcasFile_ExternalName);

                            if (pcasFileDatumType == DatumType.FileLinkType)
                            {
                                pcasFilePath = traceData.ReadFileLinkFullPath(pcasFile_ExternalName);
                            }
                            else if (pcasFileDatumType == DatumType.StringType)
                            {
                                pcasFilePath = traceData.ReadString(pcasFile_ExternalName);
                            }

                            if (pcasFilePath == null
                                || !File.Exists(pcasFilePath))
                            {
                                this.RaiseSelf_Prog_NonParamFail(engine, string.Format("The PCASFailMode file contains a PCAS file parameter \"{0}\" of which file it refers to doesn't exist.", pcasFile_ExternalName), FailModeCategory_Enum.SW);
                                return;
                            }
                        }

                        // Detail: HeaderLineCount=1|LimitsLineCount=2|DescriptorCol=1|FailModeCol=219
                        // Item No.1(HeaderLineCount): 1
                        // Item No.2(LimitsLineCount): 2
                        // Item No.3(DescriptorCol): 1
                        // Item No.4(FailModeCol): 219
                        string[] pcasFileDetailInList = pcasFileFailModeCollection[pcasFile_ExternalName].Detail.Split(FailMode.CONFIG_DETAIL_INNER_ITEM_STARTER);
                        {
                            if (pcasFileDetailInList.Length < FailMode.CONFIG_DETAIL_INNER_ITEM_COUNT)
                            {
                                this.RaiseSelf_Prog_NonParamFail(engine, string.Format("The PCASFailMode file contains a PCAS file parameter \"{0}\" of which Detail is not in correct format.", pcasFile_ExternalName), FailModeCategory_Enum.SW);
                                return;
                            }

                            for (int i = 0; i < pcasFileDetailInList.Length; i++)
                            {
                                pcasFileDetailInList[i] = pcasFileDetailInList[i].Substring(pcasFileDetailInList[i].IndexOf(FailMode.CONFIG_DETAIL_INNER_ITEM_VALUE_SPLITTER) + 1);
                            }
                        }

                        int pcasFileHeaderLineCount;
                        {
                            if (!int.TryParse(pcasFileDetailInList[0], out pcasFileHeaderLineCount))
                            {
                                this.RaiseSelf_Prog_NonParamFail(engine, string.Format("The PCASFailMode file contains a PCAS file parameter \"{0}\" of which Detail.HeaderLineCount is not in correct format.", pcasFile_ExternalName), FailModeCategory_Enum.SW);
                                return;
                            }
                        }

                        int pcasFileLimitsLineCount;
                        {
                            if (!int.TryParse(pcasFileDetailInList[1], out pcasFileLimitsLineCount))
                            {
                                this.RaiseSelf_Prog_NonParamFail(engine, string.Format("The PCASFailMode file contains a PCAS file parameter \"{0}\" of which Detail.LimitsLineCount is not in correct format.", pcasFile_ExternalName), FailModeCategory_Enum.SW);
                                return;
                            }
                        }

                        int pcasFileDescriptorColIndex;
                        {
                            if (!int.TryParse(pcasFileDetailInList[2], out pcasFileDescriptorColIndex))
                            {
                                this.RaiseSelf_Prog_NonParamFail(engine, string.Format("The PCASFailMode file contains a PCAS file parameter \"{0}\" of which Detail.DescriptorCol is not in correct format.", pcasFile_ExternalName), FailModeCategory_Enum.SW);
                                return;
                            }
                        }

                        int pcasFileFailModeColIndex;
                        {
                            if (!int.TryParse(pcasFileDetailInList[3], out pcasFileFailModeColIndex))
                            {
                                this.RaiseSelf_Prog_NonParamFail(engine, string.Format("The PCASFailMode file contains a PCAS file parameter \"{0}\" of which Detail.FailModeCol is not in correct format.", pcasFile_ExternalName), FailModeCategory_Enum.SW);
                                return;
                            }
                        }

                        List<string[]> pcasFileContent = null;
                        string pcasFileFailModeTitle = null;
                        {
                            using (CsvReader csvReader = new CsvReader())
                            {
                                pcasFileContent = csvReader.ReadFile(pcasFilePath);
                            }

                            if (pcasFileContent == null
                                || pcasFileContent.Count <= pcasFileHeaderLineCount + pcasFileLimitsLineCount)
                            {
                                return;
                            }

                            for (int i = 0; i < pcasFileHeaderLineCount; i++)
                            {
                                if (pcasFileContent[i].Length - 1 < pcasFileFailModeColIndex)
                                {
                                    continue;
                                }

                                pcasFileFailModeTitle += pcasFileContent[i][pcasFileFailModeColIndex];
                            }

                            // Append limits in form of [,] or [,,...] to pcasFileFailModeTitle
                            if (pcasFileLimitsLineCount > 0)
                            {
                                bool isLimitsPresent = false;

                                for (int i = pcasFileHeaderLineCount; i < pcasFileHeaderLineCount + pcasFileLimitsLineCount; i++)
                                {
                                    if (pcasFileContent[i].Length - 1 < pcasFileFailModeColIndex)
                                    {
                                        continue;
                                    }

                                    if (!string.IsNullOrEmpty(pcasFileContent[i][pcasFileFailModeColIndex]))
                                    {
                                        isLimitsPresent = true;
                                        break;
                                    }
                                }

                                if (isLimitsPresent)
                                {
                                    pcasFileFailModeTitle += FailMode.ITEM_LIMIT_START;

                                    for (int i = pcasFileHeaderLineCount; i < pcasFileHeaderLineCount + pcasFileLimitsLineCount; i++)
                                    {
                                        if (pcasFileContent[i].Length - 1 < pcasFileFailModeColIndex)
                                        {
                                            continue;
                                        }

                                        if (string.IsNullOrEmpty(pcasFileContent[i][pcasFileFailModeColIndex]))
                                        {
                                            pcasFileFailModeTitle += FailMode.WILDCARD.ToString() + FailMode.ITEM_LIMIT_SPLITTER;
                                        }
                                        else
                                        {
                                            pcasFileFailModeTitle += pcasFileContent[i][pcasFileFailModeColIndex] + FailMode.ITEM_LIMIT_SPLITTER;
                                        }
                                    }

                                    pcasFileFailModeTitle = pcasFileFailModeTitle.TrimEnd(FailMode.ITEM_LIMIT_SPLITTER) + FailMode.ITEM_LIMIT_END;
                                }
                            }

                            // Remove header and limits lines
                            for (int i = 0; i < pcasFileHeaderLineCount + pcasFileLimitsLineCount; i++)
                            {
                                pcasFileContent.RemoveAt(0);
                            }
                        }

                        string pcasFileFailMode = pcasFileFailModeTitle + FailMode.PCAS_FILE_FAILMODE_ENCLOSURE_START;

                        foreach (string[] pcasFileLine in pcasFileContent)
                        {
                            if (pcasFileLine == null
                                || pcasFileLine.Length - 1 < pcasFileDescriptorColIndex
                                || pcasFileLine.Length - 1 < pcasFileFailModeColIndex)
                            {
                                continue;
                            }

                            if (string.IsNullOrEmpty(pcasFileLine[pcasFileDescriptorColIndex])
                                || string.IsNullOrEmpty(pcasFileLine[pcasFileFailModeColIndex]))
                            {
                                continue;
                            }

                            pcasFileFailMode += FailMode.PCAS_FILE_FAILMODE_ITEM_STARTER + pcasFileLine[pcasFileDescriptorColIndex] + FailMode.PCAS_FILE_FAILMODE_ITEM_VALUE_SPLITTER + pcasFileLine[pcasFileFailModeColIndex];
                        }

                        pcasFileFailMode = pcasFileFailMode + FailMode.PCAS_FILE_FAILMODE_ENCLOSURE_END;

                        __failModeResults[FailModeCause_Enum.ParamFileFail].Add(new FailMode(pcasFileFailMode, pcasFileFailModeCollection[pcasFile_ExternalName].Category));

                        break;
                    }
                }

                if (!isPCASFileExistInSpec)
                {
                    this.RaiseSelf_Prog_NonParamFail(engine, string.Format("The PCASFailMode file contains a PCAS parameter \"{0}\" that doesn't exist in limits file {1}.", pcasFile_ExternalName, spec.Name), FailModeCategory_Enum.SW);
                    return;
                }
            }
        }

        private void ConvertToStr_FACTORY_WORKS_FAILMODE(bool isMainSpec, out string failModeResult_Str)
        {
            failModeResult_Str = string.Empty;

            Dictionary<FailModeCategory_Enum, string> sortedFailModeResults = new Dictionary<FailModeCategory_Enum, string>();
            {
                // For MainSpec: Add and sort all failure mode results
                if (isMainSpec)
                {
                    foreach (List<FailMode> failModeResult in __failModeResults.Values)
                    {
                        if (failModeResult == null)
                        {
                            continue;
                        }

                        foreach (FailMode failMode in failModeResult)
                        {
                            if (failMode == null)
                            {
                                continue;
                            }

                            if (sortedFailModeResults.ContainsKey(failMode.Category))
                            {
                                sortedFailModeResults[failMode.Category] += failMode.ToString();
                            }
                            else
                            {
                                sortedFailModeResults.Add(failMode.Category, failMode.ToString());
                            }
                        }
                    }

                }
                // For SubSpec: Add and sort only Prog_ParamFail failure mode results
                else if (__failModeResults.ContainsKey(FailModeCause_Enum.ParamFail)
                        && __failModeResults[FailModeCause_Enum.ParamFail] != null)
                {
                    foreach (FailMode failMode in __failModeResults[FailModeCause_Enum.ParamFail])
                    {
                        if (failMode == null)
                        {
                            continue;
                        }

                        if (sortedFailModeResults.ContainsKey(failMode.Category))
                        {
                            sortedFailModeResults[failMode.Category] += failMode.ToString();
                        }
                        else
                        {
                            sortedFailModeResults.Add(failMode.Category, failMode.ToString());
                        }
                    }
                }
            }

            foreach (FailModeCategory_Enum failModeCategory in sortedFailModeResults.Keys)
            {
                failModeResult_Str += FailMode.CATEGORY_STARTER + failModeCategory.ToString() + FailMode.CATEGORY_START + sortedFailModeResults[failModeCategory] + FailMode.CATEGORY_END;
            }
        }

        private void Conclude_FACTORY_WORKS_FAILMODE(ITestEngineDataWrite engine, Specification spec, bool isPCASLimitsAvailable, ref string failModeResult_Str)
        {
            ProgramStatus programRunStatus = engine.GetProgramRunStatus();
            string programRunComments = engine.GetProgramRunComments();

            if (programRunStatus == ProgramStatus.Success)
            {
                if (spec.Status.Status == PassFail.Pass)
                {
                    if (isPCASLimitsAvailable)
                    {
                        failModeResult_Str = FailMode.FAILMODE_LIMIT_VALUE_PASS;
                    }
                    else
                    {
                        failModeResult_Str = FailMode.FAILMODE_LIMIT_VALUE_PASS_NO_PCAS_LIMITS;
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(failModeResult_Str.Trim()))
                    {
                        failModeResult_Str = FailMode.FAILMODE_LIMIT_VALUE_FAIL;
                    }
                }

                return;
            }

            if (programRunStatus == ProgramStatus.Failed)
            {
                if (string.IsNullOrEmpty(failModeResult_Str.Trim()))
                {
                    failModeResult_Str = programRunComments;
                }

                return;
            }

            // Override the FACTORY_WORKS_FAILMODE when failmode is clear enough from test engine comment
            if (programRunStatus == ProgramStatus.Aborted
                || programRunStatus == ProgramStatus.AbortFail)
            {
                failModeResult_Str = FailMode.CATEGORY_STARTER + FailModeCategory_Enum.OP.ToString() + FailMode.CATEGORY_START + FailMode.ITEM_STARTER + programRunComments + FailMode.CATEGORY_END;

                return;
            }
            else if (programRunStatus == ProgramStatus.ArgumentsInvalid
                || programRunStatus == ProgramStatus.DataWriteError
                || programRunStatus == ProgramStatus.DataWriteMissingData
                || programRunStatus == ProgramStatus.InitError
                || programRunStatus == ProgramStatus.ProgramLoadFailed
                || programRunStatus == ProgramStatus.TestSolutionIdInvalid)
            {
                failModeResult_Str = FailMode.CATEGORY_STARTER + FailModeCategory_Enum.SW.ToString() + FailMode.CATEGORY_START + FailMode.ITEM_STARTER + programRunComments + FailMode.CATEGORY_END;

                return;
            }

            // Insert the program run comments which the current failmode result has not covered
            if (!failModeResult_Str.Contains(programRunComments))
            {
                string starterOfUN = FailMode.CATEGORY_STARTER + FailModeCategory_Enum.UN.ToString() + FailMode.CATEGORY_START;
                int indexOfUNStarter = failModeResult_Str.IndexOf(starterOfUN);

                if (indexOfUNStarter < 0)
                {
                    failModeResult_Str = starterOfUN + FailMode.ITEM_STARTER + engine.GetProgramRunComments() + FailMode.CATEGORY_END + failModeResult_Str;
                }
                else
                {
                    failModeResult_Str = failModeResult_Str.Substring(0, indexOfUNStarter + starterOfUN.Length) + FailMode.ITEM_STARTER + programRunComments + failModeResult_Str.Substring(indexOfUNStarter + starterOfUN.Length);
                }
            }
        }

        private void SetToTraceData_FACTORY_WORKS_FAILMODE(bool isMainSpec, string failModeResult_Str, ref DatumList traceData)
        {
            if (failModeResult_Str.Length > FailMode.FAILMODE_LIMIT_LENGTH)
            {
                failModeResult_Str = failModeResult_Str.Substring(0, FailMode.FAILMODE_LIMIT_LENGTH);
            }

            if (traceData.IsPresent(FailMode.FAILMODE_LIMIT_NAME))
            {
                traceData.UpdateString(FailMode.FAILMODE_LIMIT_NAME, failModeResult_Str);
            }
            else
            {
                traceData.AddString(FailMode.FAILMODE_LIMIT_NAME, failModeResult_Str);
            }
        }

        /// <summary>
        /// Display and record module fail mode to FACTORY_WORKS_FAILMODE
        /// </summary>
        public void RaiseMsg_Mod_NonParamFail(ITestEngine engine, string failMode_Detail, FailModeCategory_Enum failMode_Category)
        {
            Raise_NonParamFail(engine, "ModMsg", failMode_Detail, failMode_Category, FailModeCause_Enum.NonParamFail, false);
        }

        /// <summary>
        /// Raise module error and record to FACTORY_WORKS_FAILMODE
        /// </summary>
        public void RaiseError_Mod_NonParamFail(ITestEngine engine, string failMode_Detail, FailModeCategory_Enum failMode_Category)
        {
            Raise_NonParamFail(engine, "ModErr", failMode_Detail, failMode_Category, FailModeCause_Enum.NonParamFail, true);
        }

        /// <summary>
        /// Display and record program fail mode to FACTORY_WORKS_FAILMODE
        /// </summary>
        public void RaiseMsg_Prog_NonParamFail(ITestEngineBase engine, string failMode_Detail, FailModeCategory_Enum failMode_Category)
        {
            Raise_NonParamFail(engine, "ProgMsg", failMode_Detail, failMode_Category, FailModeCause_Enum.NonParamFail, false);
        }

        /// <summary>
        /// Raise program error and record to FACTORY_WORKS_FAILMODE
        /// </summary>
        public void RaiseError_Prog_NonParamFail(ITestEngineBase engine, string failMode_Detail, FailModeCategory_Enum failMode_Category)
        {
            Raise_NonParamFail(engine, "ProgErr", failMode_Detail, failMode_Category, FailModeCause_Enum.NonParamFail, true);
        }

        /// <summary>
        /// Since the error raise in DataWrite section results in crash without test results stored, so when failure mode check happens in this section, the error must be suppressed.
        /// </summary>
        private void RaiseSelf_Prog_NonParamFail(ITestEngineBase engine, string failMode_Detail, FailModeCategory_Enum failMode_Category)
        {
            if (__IS_SUPPRESS_SELF_ERROR)
            {
                this.RaiseMsg_Prog_NonParamFail(engine, failMode_Detail, failMode_Category);
            }
            else
            {
                this.RaiseError_Prog_NonParamFail(engine, failMode_Detail, failMode_Category);
            }
        }

        private void Raise_NonParamFail<T>(T engine, string failMode_Descriptor, string failMode_Detail, FailModeCategory_Enum failMode_Category, FailModeCause_Enum failModeResult_Category, bool isRaiseError)
        {
            if (!__failModeResults.ContainsKey(failModeResult_Category))
            {
                __failModeResults.Add(failModeResult_Category, new List<FailMode>());
            }

            bool isFailModeExists = false;
            {
                foreach (FailMode failMode in __failModeResults[failModeResult_Category])
                {
                    if (failMode == null)
                    {
                        continue;
                    }

                    if (failMode.Detail == failMode_Detail)
                    {
                        isFailModeExists = true;
                        break;
                    }
                }
            }

            if (!isFailModeExists)
            {
                __failModeResults[failModeResult_Category].Add(new FailMode(failMode_Descriptor, failMode_Category, failMode_Detail));
            }

            if (engine is ITestEngineBase)
            {
                ITestEngineBase engine_Prog = engine as ITestEngineBase;
                {
                    engine_Prog.ShowContinueUserQuery(failMode_Detail);

                    if (isRaiseError)
                    {
                        engine_Prog.ErrorInProgram(failMode_Detail);
                    }
                }
            }
            else if (engine is ITestEngine)
            {
                ITestEngine engine_Mod = engine as ITestEngine;
                {
                    engine_Mod.ShowContinueUserQuery(failMode_Detail);

                    if (isRaiseError)
                    {
                        engine_Mod.ErrorInModule(failMode_Detail);
                    }
                }
            }
        }

        enum FailModeCause_Enum
        {
            NonParamFail, ParamFail, ParamFileFail
        }
    }
}
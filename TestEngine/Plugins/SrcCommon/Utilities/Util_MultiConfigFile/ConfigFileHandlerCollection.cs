// [Copyright]
//
// Bookham Modular Test Engine
// Utilities
//
// Utilities\Util_MultiConfigFile\ConfigFileHandlerCollection.cs
// 
// Author: K Pillar
// Design: As specified in MulticonfigHandler design DD 

using System;
using System.Collections;


namespace Bookham.TestLibrary.Utilities
{
    /// <summary>
    /// Collection object for groups of ConfigFileHandlers
    /// </summary>
    /// 
    public class ConfigFileHandlerCollection : DictionaryBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public ConfigFileHandlerCollection()
        {
        }

        /// <summary>
        /// Return an ConfigFileHandler indexed by name
        /// </summary>
        public ConfigFileHandler this[String confFileName]
        {
            // Index the configfileclass using its name, cast to 'ConfigFileHandler' and return 
            get
            {
                object obj = Dictionary[confFileName];
                if (obj == null)
                {
                    throw new Exception("ConfigFile not found in collection: " + confFileName);
                }
                return ((ConfigFileHandler)obj);
            }
        }

        /// <summary>
        /// Return a collection of ConfFile names
        /// </summary>
        public ICollection Names
        {
            get { return (Dictionary.Keys); }
        }

        /// <summary>
        /// Return a collection of confFile objects
        /// </summary>
        public ICollection Values
        {
            get { return (Dictionary.Values); }
        }

        /// <summary>
        /// Add a new ConfFile to the collection - use the ConfFile.Name 
        /// as the key in the collection.
        /// </summary>
        /// <param name="conFileToAdd">ConfFile to be added to collection</param>
        public void Add(ConfigFileHandler conFileToAdd)
        {
            Dictionary.Add(conFileToAdd.Name, conFileToAdd);
        }
                

      
        /// <summary>
        /// Return true if an ConfFile with the parameter name is present
        /// </summary>
        /// <param name="conFileName">ConfFile name to search for</param>
        /// <returns>True if ConfFile present</returns>
        public bool Contains(string conFileName )
        {
            return (Dictionary.Contains(conFileName));
        }

        /// <summary>
        /// Remove an conFileName from the collection
        /// </summary>
        /// <param name="conFileName">Name of the configfile to remove</param>
        public void Remove(String conFileName)
        {
            Dictionary.Remove(conFileName);
        }

        /// <summary>
        /// ClearAll method, removes all entries from the collection 
        /// </summary>
        public void ClearAll()
        {
            Dictionary.Clear();
        }
    }
}

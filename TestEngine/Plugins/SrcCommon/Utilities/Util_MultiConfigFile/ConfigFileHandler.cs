// [Copyright]
//
// Bookham Modular Test Engine
// Utilities
//
// Utilities\Util_MultiConfigFile\ConfigFileHandler.cs
// 
// Author: K Pillar
// Design: As specified in MulticonfigHandler design DD 

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Config;
using System.Diagnostics;
//using Bookham.TestEngine.Framework.Logging;

namespace Bookham.TestLibrary.Utilities
{
    /// <summary>
    /// This class deals with config files.  
    /// Trying to create a class that can deal with our multitude of config files.
    /// When we create a instance of this class, it looks at the structure of the xml table,
    /// and sets up whatever known keys it may have.  
    /// We can then create a collection of these classes, and our caller only has to pass in the particularly relevent keys..
    /// ie there may be 4 keys required, but 2 have been set up on initialisation / creation of this class.
    /// The other 2 we must therefore pass in.
    /// </summary>
    public class ConfigFileHandler
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public ConfigFileHandler( string fileName, string tableName, DatumList myKnownParams, bool readWriteAble)
        {
            configFileHandlerName = tableName;
            myKnownKeyList = new StringDictionary();
            myConfFileHandler = new ConfigDataAccessor(fileName, configFileHandlerName);
            
            // by getting dummy data i am able to check datum names and types i have been passed
            // matches the dataset
            mydummyDatumList = myConfFileHandler.GetDummyData();
            iAmReadWriteAble = readWriteAble;

            UpdateMyKnownKeyList(myKnownParams);
        }

       
        /// <summary>
        /// This function should take our datum list of known params
        /// and our dummy datumlist (which now represents a row from our config file)
        /// and will update our key list with all the params it does in fact now know
        /// </summary>
        /// <param name="myKnownParams"> a datum list of known parameters</param>
        private void UpdateMyKnownKeyList(DatumList myKnownParams)
        {
            myKnownKeyList.Clear();

            foreach (Datum d1 in myKnownParams)
            {
                foreach (Datum d2 in mydummyDatumList)
                {
                    if ((d1.Name == d2.Name) && (d1.Type == d2.Type))
                    {
                        //one of the params passed in matches one of the columns in our table
                        //so we add this param and value to our KnownKeyList
                        Debug.Print(d1.Name + " ," + d1.ValueToString());
                        myKnownKeyList.Add(d1.Name, d1.ValueToString());
                    }
                }
            }
        }

        /// <summary>
        /// Will create a compound key list of known data and will then get you your row of
        /// data based on all the known keys.  You can over-ride any of the previously stored keys
        /// merely by putting new values into the defined mykeys list.
        /// </summary>
        /// <param name="mykeys">the keys you wish to search for</param>
        /// <param name="includeWildcards">whether to include wildcards in your search</param>
        /// <returns>A single row of data</returns>
        public DatumList GetARowOfData(StringDictionary mykeys, bool includeWildcards)
        {
            return (myConfFileHandler.GetData(GetCompoundKeyList(mykeys), includeWildcards));
        }


        /// <summary>
        /// this is hooking directly into the configApi functionality.
        /// You must pass in a datumlist with your fields, which has all fields filled in.
        /// </summary>
        /// <param name="myNewData"> A datumlist to add</param>
        /// <returns></returns>
        public void AddDataIntoDataset(DatumList myNewData)
        {
            if (iAmReadWriteAble)
            {
                throw new Exception ("Attempting to Write data to an dataset file that has been put into this collection as read Only");
            }
            else
            {
                myConfFileHandler.AddData(myNewData);
                return;
            }
            
        }



        /// <summary>
        /// Will create a compound key list of known data and will then get you a matching row count,
        /// based on all the known keys.  You can over-ride any of the previously stored keys
        /// merely by putting new values into the defined mykeys list.
        /// </summary>
        /// <param name="mykeys">the keys you wish to search for</param>
        /// <param name="includeWildcards">whether to include wildcards in your search</param>
        /// <returns></returns>
        public int GetMatchingRowCount(StringDictionary mykeys, bool includeWildcards)
        {
            return (myConfFileHandler.MatchingRowCount(GetCompoundKeyList(mykeys),includeWildcards)); 
        }

        /// <summary>
        /// Will create a compound key list of known data and will then get you your rows of
        /// data based on all the known keys.  You can over-ride any of the previously stored keys
        /// merely by putting new values into the defined mykeys list.
        /// </summary>
        /// <param name="mykeys">the keys you wish to search for</param>
        /// <param name="includeWildcards">whether to include wildcards in your search</param>
        /// <returns></returns>
        public DatumList GetMultipleRowsOfData(StringDictionary mykeys, bool includeWildcards)
        {
            return (null);
        }

        /// <summary>
        /// Adds new keys to the previously stored keylist.
        /// You must pass in the keys and values as a datum so the function can check the type
        /// matches the declared type in the dataset.
        /// </summary>
        /// <param name="myNewkeys">the datumlist of new keys you wish to add</param>
        public void AddNewKeysToStoredKeyList(DatumList myNewkeys)
        {
            mydummyDatumList = myConfFileHandler.GetDummyData();

            foreach (Datum d1 in myNewkeys)
            {
                foreach (Datum d2 in mydummyDatumList)
                {
                    if ((d1.Name == d2.Name) && (d1.Type == d2.Type))
                    {
                        //one of the params passed in matches one of the columns in our table
                        
                        Debug.Print(d1.Name + " ," + d1.ValueToString());

                        //does this key already exist here?
                        if (myKnownKeyList.ContainsKey(d1.Name))
                        {
                            //overwrite the value which is already stored in our key list
                            myKnownKeyList[d1.Name] = d1.ValueToString();                     
                        }
                        else
                        {
                            //add the key
                            myKnownKeyList.Add(d1.Name, d1.ValueToString());
                        }
                    }
                }
            }
        
        
        }
        /// <summary>
        /// Removes key from the previously stored keylist, you pass in the key name and if its
        /// there it will be removed.
        /// </summary>
        /// <param name="mykeysToRemove">the names of the keys to remove</param>
        public void RemoveKeyFromStoredKeyList(string mykeyToRemove)
        {
            if (myKnownKeyList.ContainsKey(mykeyToRemove))
            {
                myKnownKeyList.Remove(mykeyToRemove);
            }
             
        }



        /// <summary>
        /// Our user is asking for a compound key list, he has passed in the keys he thinks we need, 
        /// but we should make use of the ones we already have stashed away as well.
        /// User keys take precedence though... if exists in user key list and knownkeyList then we will use the
        /// user key list value.
        /// </summary>
        /// <param name="keys">the keys you wish to have added to the compound list</param>
        /// <returns>the compound key list</returns>
        public StringDictionary GetCompoundKeyList(StringDictionary mykeys)
        {             
            string columnKey;
            string valueKey;
            StringDictionary compoundKeys; //this is going to be the mixture of myknownkeys and mykeys

            compoundKeys = myKnownKeyList;
            
            foreach ( DictionaryEntry de in mykeys )
			{
                columnKey=de.Key.ToString();
				valueKey=de.Value.ToString();
                if ( compoundKeys.ContainsKey(columnKey) )
                {
                    //overwrite the existing
                    compoundKeys[columnKey]=valueKey;
                }
                else
                {
                    //add the key the user has passed in to us
                    compoundKeys.Add(columnKey,valueKey);
                }
            }
            return(compoundKeys);
        }

        /// <summary>
        /// ConfigFileHandler name property
        /// </summary>
        public string Name
        {
            get { return configFileHandlerName; }
        }

        private string configFileHandlerName;
        private ConfigDataAccessor myConfFileHandler;
        private StringDictionary myKnownKeyList;
        private DatumList mydummyDatumList;
        private bool iAmReadWriteAble;
    }
}

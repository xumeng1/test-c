// [Copyright]
//
// Bookham ITLA Black Box
// Bookham.TestSolution.TunableModules
//
// SpecValuesToDatumList.cs
//
// Author: paul.annetts, 2006
// Design: ITLA Module Setup and Functional Test Program DD

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.Framework.Limits;
using Bookham.TestEngine.Framework.InternalData;

namespace Bookham.TestLibrary.Utilities
{
    /// <summary>
    /// Helper class to read Test Conditions and Values
    /// </summary>
    sealed public class SpecValuesToDatumList
    {
        // All methods static, so don't allow instantiation
        private SpecValuesToDatumList() { }

        /// <summary>
        /// Read the following conditions from the Test Spec. The conditions
        /// are found in the lower and upper limit of the PCAS limit file. Just
        /// use the lower limit here. 
        /// 
        /// Once read back, set the actual result value in the parameter to match the 
        /// condition value.
        /// </summary>
        /// <param name="spec">Specification object</param>
        /// <param name="paramNames">List of descriptors to read</param>
        /// <returns>DatumList of the values</returns>
        /// <remarks>If parameter name isn't found, it is skipped. This makes it simpler
        /// to deal with "optional" parameters. Just ask for them all... 
        /// Then detect whether the DatumList has the Datum of the required name</remarks>
        public static DatumList GetTestConditions(Specification spec, string[] paramNames)
        {
            return GetTestConditions(spec, paramNames, true);
        }

        /// <summary>
        /// Read the following conditions from the Test Spec. The conditions
        /// are found in the lower and upper limit of the PCAS limit file. Just
        /// use the lower limit here. 
        /// 
        /// Once read back, set the actual result value in the parameter to match the 
        /// condition value.
        /// </summary>
        /// <param name="spec">Specification object</param>
        /// <param name="paramNames">List of descriptors to read</param>
        /// <param name="useLowLimit">if true,use low limit;otherwise,use high limit.</param>
        /// <returns>DatumList of the values</returns>
        /// <remarks>If parameter name isn't found, it is skipped. This makes it simpler
        /// to deal with "optional" parameters. Just ask for them all... 
        /// Then detect whether the DatumList has the Datum of the required name</remarks>
        public static DatumList GetTestConditions(Specification spec, string[] paramNames, bool useLowLimit)
        {
            DatumList testConditions = new DatumList();

            foreach (string paramName in paramNames)
            {
                ParamLimit lim;
                lim = spec.GetParamLimit(paramName);
                // if limit doesn't exist, null will have been returned.
                // if not found, skip...                    
                if (lim != null)
                {
                    Datum condition = null;
                    if (useLowLimit)
                    {
                        condition = lim.LowLimit;
                    }
                    else
                    {
                        condition = lim.HighLimit;
                    }

                    // Add the condition to the DatumList - don't record it!
                    testConditions.Add(condition);
                }

            }

            return testConditions;
        }

    }
}

// [Copyright]
//
// Bookham 
// Bookham.TestLibrary.Utilities
//
// 
//
// Author: Jim Liu, 2011.6
// Design: 
using System;
using System.Data;
using System.IO;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.Utilities
{
    public class CsvFileObject
    {
        private string filePath;
        private DataTable dtData;


        /// <summary>
        /// New one CsvFileObject
        /// </summary>
        /// <param name="path">File path</param>
        public CsvFileObject(string path)
        {
            filePath = path;
            dtData = new DataTable("new table");
        }

        /// <summary>
        /// Read data from file
        /// </summary>
        public void Read()
        {
            if (filePath.Length <= 0 || !File.Exists(filePath))
            {
                throw new Exception("Error path.");
            }
            StreamReader sr = new StreamReader(filePath);
            try
            {
                string line = sr.ReadLine();
                while (line != null)
                {
                    string[] rowDatas = line.Split(',');
                    if (rowDatas.Length > 0)
                    {
                        if (rowDatas.Length > dtData.Columns.Count)
                        {
                            AddColumnsTo(rowDatas.Length);
                        }
                        dtData.Rows.Add(rowDatas);
                    }
                    line = sr.ReadLine();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                sr.Close();
            }
        }

        /// <summary>
        /// Save as
        /// </summary>
        /// <param name="path">Target file path</param>
        public void SaveAs(string path)
        {
            StreamWriter sw = new StreamWriter(path, false);
            try
            {
                StringBuilder formatStringBuilder = new StringBuilder();
                for (int i = 0; i < ColumnsCount; i++)
                {
                    formatStringBuilder.AppendFormat("{{{0}}},", i);
                }
                if (formatStringBuilder.Length > 0)
                {
                    formatStringBuilder.Remove(formatStringBuilder.Length - 1, 1);
                }
                string formatString = formatStringBuilder.ToString();
                foreach (DataRow dr in dtData.Rows)
                {
                    sw.WriteLine(formatString, dr.ItemArray);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                sw.Flush();
                sw.Close();
            }
        }

        /// <summary>
        /// Save
        /// </summary>
        public void Save()
        {
            SaveAs(filePath);
        }

        /// <summary>
        /// Get rows count
        /// </summary>
        public int RowsCount
        {
            get
            {
                return dtData.Rows.Count;
            }
        }

        /// <summary>
        /// Get columns count
        /// </summary>
        public int ColumnsCount
        {
            get
            {
                return dtData.Columns.Count;
            }
        }

        /// <summary>
        /// Get or set the cell value in this object
        /// </summary>
        /// <param name="row">Row number(min 1)</param>
        /// <param name="col">Column number(min 1)</param>
        /// <returns>Cell value(string)</returns>
        public string this[int row, int col]
        {
            get
            {
                if (row > RowsCount || col > ColumnsCount)
                {
                    return null;
                }
                return dtData.Rows[row - 1][col - 1].ToString();
            }
            set
            {
                if (row > RowsCount)
                {
                    AddRowsTo(row);
                }
                if (col > ColumnsCount)
                {
                    AddColumnsTo(col);
                }
                dtData.Rows[row - 1][col - 1] = value;
            }
        }

        public void AddRow(params object[] itemList)
        {
            if (itemList != null && itemList.Length > 0)
            {
                int rowIndex = RowsCount + 1;
                for (int i = 0;i<itemList.Length;i++)
                {
                    this[rowIndex, i + 1] = itemList[i].ToString();
                }
            }
        }

        /// <summary>
        /// Extend data of other csvFileObject
        /// </summary>
        /// <param name="csvFileObject">Other csvFileObject</param>
        /// <param name="extendToLeft">Whether extend to right</param>
        public void ExtendFrom(CsvFileObject csvFileObject, bool extendToRight)
        {
            int rowStart = 0;
            int columnStart = ColumnsCount;
            if (!extendToRight)
            {
                rowStart = RowsCount ;
                columnStart = 0;
            }
            int rowCount = csvFileObject.RowsCount;
            int colCount = csvFileObject.ColumnsCount;
            for (int i = 1; i <= rowCount; i++)
            {
                for (int j = 1; j <= colCount; j++)
                {
                    this[rowStart + i, columnStart + j] = csvFileObject[i, j];
                }
            }

        }

        private void AddColumnsTo(int count)
        {
            int loop = count - ColumnsCount;
            for (int i = 0; i < loop; i++)
            {
                dtData.Columns.Add((ColumnsCount + 1).ToString(), typeof(string));
            }

        }

        private void AddRowsTo(int count)
        {
            int loop = count - RowsCount;
            for (int i = 0; i < loop; i++)
            {
                DataRow dr = dtData.NewRow();
                dtData.Rows.Add(dr);
            }
        }

        public DataRow[] GetRows(string filterString, string sortString)
        {
            if (dtData != null)
            {
                DataTable copyData = dtData.Copy();
                //copyData.Rows.RemoveAt(0);//could remove the header line
                return copyData.Select(filterString, sortString);
            }
            return null;
        }
    }
}

// [Copyright]
//
// Bookham ITLA Black Box
// Bookham.TestSolution.TunableModules
//
// SpecValuesToDatumList.cs
//
// Author: paul.annetts, 2006
// Design: ITLA Module Setup and Functional Test Program DD

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.Framework.Limits;
using Bookham.TestEngine.Framework.InternalData;

namespace Bookham.TestLibrary.Utilities
{
    /// <summary>
    /// Helper class to read Test Conditions and Values
    /// </summary>
    sealed public class SubSpecHelper
    {
        // All methods static, so don't allow instantiation
        private SubSpecHelper() { }

        /// <summary>
        /// Gets a sub specification from a spec.
        /// </summary>
        /// <param name="spec">The main spec.</param>
        /// <param name="paramNames">parameter name list</param>
        /// <param name="subSpecName">Sub spec name</param>
        /// <returns>A sub specification</returns>
        public static Specification GetSubSpecification(Specification spec, string[] paramNames,string subSpecName)
        {
            Specification subSpec = new Specification(subSpecName, new System.Collections.Specialized.StringDictionary(), 1);
           
            foreach (string paramName in paramNames)
            {
                ParamLimit lim;
                lim = spec.GetParamLimit(paramName);
                // if limit doesn't exist, null will have been returned.
                // if not found, skip...                    
                if (lim != null)
                {
                    RawParamLimit rawParamLimit=new RawParamLimit(lim.InternalName,lim.ParamType,lim.LowLimit,lim.HighLimit,lim.Operand,
                                lim.Priority,lim.AccuracyFactor,lim.Units);
                    subSpec.Add(rawParamLimit);
                }
                
            }

            return subSpec;
        }

    }
}

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Util_CsvFileAccess
{
    /// <summary>
    /// CSV file paser.
    /// </summary>
    public class CSVDbReader
    {
        #region private members
        private StreamReader _reader;
        private List<string> _fields = new List<string> ();
        #endregion

        /// <summary>
        /// current parsing line.
        /// </summary>
        string [] _line;

        /// <summary>
        /// Construct CSVDbReader from a file.
        /// </summary>
        /// <param name="filename"></param>
        public CSVDbReader(string filename)
        {
            this._reader = new StreamReader(filename);

            string strline = this._reader.ReadLine();

            string [] fields = strline.Split( new char[] { ',' } );

            //this._fieldIndecies = new Dictionary<string,int>();
            this._fields.AddRange(fields);
        }

        /// <summary>
        /// Read next record from csv file.
        /// </summary>
        /// <returns> true if record exist, false for end-of-file.</returns>
        public bool Read()
        {
            if (this._reader.EndOfStream)
            {
                return false;
            }
            else
            {
                string line = this._reader.ReadLine();
                this._line = line.Split(new char[] { ',' });
                return true;
            }
        }

        /// <summary>
        /// Reset CSVDbReader.
        /// </summary>
        public void Reset()
        {
            this._reader.BaseStream.Seek(0, SeekOrigin.Begin);
        }

        /// <summary>
        /// Skip records/lines.
        /// </summary>
        /// <param name="nSkip"> number of records/lines to skip</param>
        public void Skip(Int32 nSkip)
        {
            for (int idx = 0; idx < nSkip; idx++)
            {
                if (!this.Read())
                {
                    break;
                }
            }
        }

        /// <summary>
        /// Close the reader.
        /// </summary>
        public void Close()
        {
            this._reader.Close();
            this._reader = null;
        }

        /// <summary>
        /// Returns double value of specified field.
        /// </summary>
        /// <param name="fieldName">Specified field name</param>
        /// <returns>return double value.</returns>
        public double GetDouble(string fieldName)
        {
            int idx = this.FindIndex(fieldName);

            if (idx == -1)
            {
                throw new ArgumentOutOfRangeException("fieldName", "Field not found!");
            }

            return this.GetDouble(idx);
        }

        /// <summary>
        /// Return nullable double value of specified field.
        /// </summary>
        /// <param name="index">Specified field index.</param>
        /// <returns>returns nullable double value of the field.</returns>
        public double? GetNullableDouble(int index)
        {
            string field = this._line[index];
            string trimmed = field.Trim();

            if (trimmed.Length == 0)
            {
                return null;
            }
            else
            {
                return double.Parse(trimmed);
            }
        }

        /// <summary>
        /// Returns nullable double value of specified value.
        /// </summary>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        public double? GetNullableDouble(string fieldName)
        {
            int idx = this.FindIndex(fieldName);

            if (idx == -1)
            {
                throw new ArgumentOutOfRangeException("fieldName", "Field not found!");
            }

            return this.GetNullableDouble(idx);
        }

        /// <summary>
        /// Returns double value of specified field.
        /// </summary>
        /// <param name="index">Specified field index.</param>
        /// <returns>Return double value of specified value.</returns>
        public double GetDouble(int index)
        {
            return double.Parse( this._line[index] );
        }

        /// <summary>
        /// Returns string value of specified field.
        /// </summary>
        /// <param name="fieldName">Specified field name.</param>
        /// <returns>Return string value.</returns>
        public string GetString(string fieldName)
        {
            int idx = this.FindIndex(fieldName);

            if (idx == -1)
            {
                throw new ArgumentOutOfRangeException("fieldName", "Field not found!");
            }

            return this.GetString(idx);
        }

        /// <summary>
        /// Returns strinf value of specified field.
        /// </summary>
        /// <param name="index">Specified field index.</param>
        /// <returns>Return string value</returns>
        public string GetString(int index)
        {
            string val = this._line[index];
            return val.Trim( new char[]{ ' ', '"' } );
        }

        /// <summary>
        /// Returns value of specified field as unsigned integer.
        /// </summary>
        /// <param name="fieldName">Specified field name.</param>
        /// <returns>returned uint value.</returns>
        public uint GetUInt(string fieldName)
        {
            int idx = this.FindIndex(fieldName);

            if ( idx == -1 )
            {
                throw new ArgumentOutOfRangeException("fieldName", "Field not found!");
            }

            return this.GetUInt(idx);
        }

        /// <summary>
        /// Returns value of specified field as unsigned integer.
        /// </summary>
        /// <param name="index">Field index.</param>
        /// <returns>Returned uint value.</returns>
        public uint GetUInt(int index)
        {
            return uint.Parse(this._line[index]);
        }

        /// <summary>
        /// Find index for corresponding field.
        /// </summary>
        /// <param name="fieldName">Field name.</param>
        /// <returns>Field index.</returns>
        public int FindIndex(string fieldName)
        {
            for (int idx = 0; idx < this._fields.Count; idx++)
            {
                if (string.Compare(fieldName, this._fields[idx], true) == 0)
                {
                    return idx;
                }
            }

            return -1;
        }

        /// <summary>
        /// Returns value of specified field as int.
        /// </summary>
        /// <param name="fieldName">Field name.</param>
        /// <returns>Field value as int</returns>
        public int GetInt(string fieldName)
        {
            int idx = this.FindIndex(fieldName);

            if (idx == -1)
            {
                throw new ArgumentOutOfRangeException("fieldName", "Field not found!");
            }

            return this.GetInt(idx);
        }

        /// <summary>
        /// Returns value of specified field as int
        /// </summary>
        /// <param name="index">Field index</param>
        /// <returns>Field value as int</returns>
        public int GetInt(int index)
        {
            return int.Parse(this._line[index]);
        }
    }
}

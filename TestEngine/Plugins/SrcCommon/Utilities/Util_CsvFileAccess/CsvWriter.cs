using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Bookham.TestLibrary.Utilities
{
    /// <summary>
    /// Simple CsvWriter class. No clever stuff with quote values in CSV elements!
    /// </summary>
    /// <remarks>This class is disposable, if disposed will close the file</remarks>
    public class CsvWriter : IDisposable
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public CsvWriter()
        {
            streamWriter = null;
        }

        /// <summary>
        /// Write a line to file
        /// </summary>
        /// <param name="line">A string for the line to write</param>
        public void WriteLine(string line)
        {
            streamWriter.WriteLine(line);
        }

        /// <summary>
        /// Write an entire file
        /// </summary>
        /// <param name="csvFile">The name of the file to write</param>
        /// <param name="fileLines">List of string representing the whole file.</param>
        public void WriteFile(string csvFile, List<string> fileLines)
        {
            using (this.streamWriter = new StreamWriter(csvFile))
            {
                foreach (string line in fileLines)
                {
                    streamWriter.WriteLine(line);
                }
            }
        }

        #region IDisposable Members

        /// <summary>
        /// release resources.
        /// </summary>
        public void Dispose()
        {
            if (streamWriter != null)
            {
                streamWriter.Dispose();
                streamWriter = null;
            }
        }

        #endregion

        #region Private Data
        private StreamWriter streamWriter;
        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.Algorithms;

namespace Bookham.TestLibrary.Utilities
{
    /// <summary>
    /// Exception class that reports a mode hop event in frequency tuning
    /// </summary>
    public class FreqTunerModeHopException : Exception
    {

        /// <summary>
        /// ModeHopException constructor. Include the two frequencies
        /// that were considered to be a mode hop!
        /// </summary>
        /// <param name="freq1GHz">1st Frequency in GHz</param>
        /// <param name="freq2GHz">2nd Frequency in GHz</param>
        public FreqTunerModeHopException(double freq1GHz, double freq2GHz)
        {
            this.freq1GHz = freq1GHz;
            this.freq2GHz = freq2GHz;
        }

        /// <summary>
        /// Exception message
        /// </summary>
        public override string Message
        {
            get
            {
                string msg =
                    String.Format("Mode hop detected: {0} GHz to {1} GHz",
                    freq1GHz, freq2GHz);
                return msg;
            }
        }

        #region Private data
        double freq1GHz;
        double freq2GHz;
        #endregion
    }

    /// <summary>
    /// Frequency tuning utility class - used all over
    /// the place in tunable module tuning in Black-Box. 
    /// Sets a maximum shift in a single iteration and has code for mode-hop detection...
    /// 
    /// Extended to Gold-Box applications - now using floating point maths
    /// throughout, with subtle renaming of variables.
    /// </summary>
    public class Util_FrequencyTuner
    {                
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="waveMeter">Wavemeter object</param>
        /// <param name="reqFreqAccuracy_GHz">Required accuracy</param>
        /// <param name="freqTarget_GHz">Target frequency</param>
        /// <param name="modeHopDetectGHz">Frequency jump over which a mode hop is assumed to have occurred!</param>
        public Util_FrequencyTuner(InstType_Wavemeter waveMeter,
            double reqFreqAccuracy_GHz, double freqTarget_GHz,
            double modeHopDetectGHz)
        {
            this.waveMeter = waveMeter;
            this.reqFreqAccuracy_GHz = reqFreqAccuracy_GHz;
            this.freqTarget_GHz = freqTarget_GHz;
            this.maxCurrShift = 0;
            this.modeHopeDetectGHz = modeHopDetectGHz;
        }

        /// <summary>
        /// Frequency target in GHz
        /// </summary>
        public double FreqTarget_GHz
        {
            get { return freqTarget_GHz; }
            set { freqTarget_GHz = value; }
        }

        /// <summary>
        /// Frequency slope in units per GHz
        /// </summary>
        public double FreqSlope_PerGHz
        {
            get { return phaseSlope_PerGHz; }
        }

        /// <summary>
        /// Mode hop detection threshold
        /// </summary>
        public double ModeHopDetectGHz
        {
            get { return modeHopeDetectGHz; }
        }

        /// <summary>
        /// Sets up the Frequency tuner with an initial slope and approximate maximum 
        /// frequency shift to jump
        /// </summary>
        /// <param name="freqSlope_p_GHz">Frequency slope in current units per GHz</param>
        /// <param name="maxShiftGHz">Approx maximum frequency shift to jump in a single iteration in GHz.</param>
        public void SetupTuning(double freqSlope_p_GHz, double maxShiftGHz)
        {
            this.maxCurrShift = Math.Abs(freqSlope_p_GHz * maxShiftGHz);
            this.phaseSlope_PerGHz = freqSlope_p_GHz;
        }

        /// <summary>
        /// Calculate a new DAC value tuning point
        /// </summary>
        /// <param name="dacCurrent">Current DAC level</param>
        /// <param name="freqCurrent_GHz">Current Frequency</param>
        /// <returns>New DAC level to try</returns>
        public int CalcNewTuningPoint(int dacCurrent, double freqCurrent_GHz)
        {
            freqPrevious = freqCurrent_GHz;
            currPrevious = dacCurrent;
            // calculate new point by linear estimation using the slope
            currLatest = checked((int)EstimateXAtY.Calculate
                (dacCurrent, freqCurrent_GHz,
                (1 / phaseSlope_PerGHz), freqTarget_GHz));
            
            // limit the dac phase change to avoid mode-hops
            int dacDiff = (int)(currLatest - currPrevious);
            if ((maxCurrShift != 0) && (Math.Abs(dacDiff) > maxCurrShift))
            {
                currLatest = currPrevious + Math.Sign(dacDiff) * maxCurrShift;
            }
            return (int)currLatest;
        }

        /// <summary>
        /// Calculate a new analogue current value tuning point
        /// </summary>
        /// <param name="current">Present Current level</param>
        /// <param name="freqCurrent_GHz">Present Frequency</param>
        /// <returns>New current level to try</returns>
        public double CalcNewTuningPoint(double current, double freqCurrent_GHz)
        {
            freqPrevious = freqCurrent_GHz;
            currPrevious = current;
            // calculate new point by linear estimation using the slope
            currLatest = EstimateXAtY.Calculate
                (current, freqCurrent_GHz,
                (1 / phaseSlope_PerGHz), freqTarget_GHz);

            // limit the current change to avoid mode-hops
            double currDiff = currLatest - currPrevious;
            if ((maxCurrShift != 0) && (Math.Abs(currDiff) > maxCurrShift))
            {
                currLatest = currPrevious + Math.Sign(currDiff) * maxCurrShift;
            }
            return currLatest;
        }

        /// <summary>
        /// Update the slope from new frequency point and previously 
        /// recorded data.
        /// </summary>
        public void UpdateSlopeAndCheckModeHops(double newFreqGHz)
        {           
            // calculate new slope and detect mode-hops
            freqLatest = newFreqGHz;
            if (currLatest != 0)
            {
                // check for mode hop
                double fChangeGHz = Math.Abs(freqLatest - freqPrevious);
                if (fChangeGHz > modeHopeDetectGHz)
                {
                    throw new FreqTunerModeHopException(freqPrevious, freqLatest);
                }
                // don't update slope if frequencies exactly match - would
                // otherwise get a divide by zero error
                if (freqLatest != freqPrevious)
                {
                    // new slope
                    double newSlope = (currLatest - currPrevious) / (freqLatest - freqPrevious);
                    phaseSlope_PerGHz = newSlope;
                }
            }

        }

        /// <summary>
        /// Measure the frequency and check if it is in range
        /// </summary>
        /// <param name="freqGHz">Frequency measured</param>
        /// <returns>true if in range</returns>
        public bool MeasFreqAndCheckInRange(out double freqGHz)
        {
            freqGHz = waveMeter.Frequency_GHz;
            bool inRange = (Math.Abs(freqGHz - freqTarget_GHz)
                                    < reqFreqAccuracy_GHz);            
            return inRange;
        }

        #region Private data
        // fundamental data
        InstType_Wavemeter waveMeter;
        double reqFreqAccuracy_GHz;
        double freqTarget_GHz;
        double maxCurrShift;
        double modeHopeDetectGHz;

        // slope variables
        double currPrevious;
        double currLatest;
        double freqPrevious;
        double freqLatest;
        double phaseSlope_PerGHz;        
        #endregion


    }
}

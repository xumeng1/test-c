using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.Algorithms;

namespace Bookham.TestLibrary.Utilities
{    
    /// <summary>
    /// Power tuning utility class - used all over
    /// the place in tunable module tuning in Black-Box. 
    /// Extended to Gold-Box applications - now using floating point maths
    /// throughout, with subtle renaming of variables.
    /// </summary>
    public class Util_PowerTuner
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="powMeter">Power meter object</param>
        /// <param name="reqPowAccuracy_dB">Required accuracy</param>
        /// <param name="powerOffset_dB">Offset power due to optical cal</param>
        /// <param name="powerTarget_dBm">Required power</param>
        public Util_PowerTuner(InstType_OpticalPowerMeter powMeter,
            double reqPowAccuracy_dB, double powerOffset_dB,
            double powerTarget_dBm)
        {
            this.powMeasure = new Util_CalPowerMeasure(powMeter,
                powerOffset_dB);
            this.reqPowAccuracy_dB = reqPowAccuracy_dB;
            // NB: use property to set power, to ensure it's done correctly
            // setting up the other internal variables
            this.PowerTarget_dBm = powerTarget_dBm;
        }

        /// <summary>
        /// Power target in dBm
        /// </summary>
        public double PowerTarget_dBm
        {
            get { return powerTarget_dBm; }
            set 
            { 
                // record the target
                powerTarget_dBm = value;
                powerTarget_mW = Alg_PowConvert_dB.Convert_dBmtomW(powerTarget_dBm);
            }
        }

        /// <summary>
        /// Get the power measurer object that is used internally
        /// </summary>
        public Util_CalPowerMeasure PowMeasurer
        {
            get { return powMeasure; }
        }

        /// <summary>
        /// slope in current units per mW
        /// </summary>
        public double Slope_p_mW
        {
            get { return slope_p_mW; }
            set { slope_p_mW = value; }
        }

        /// <summary>
        /// Calculate a new DAC value tuning point
        /// </summary>
        /// <param name="dacCurrent">Current DAC level</param>
        /// <param name="powCurrent_dBm">Current Power in dBm (corrected)</param>
        /// <returns>New DAC level to try</returns>
        public int CalcNewTuningPoint(int dacCurrent, double powCurrent_dBm)
        {
            // convert target to take into account the offset
            powPrevious_mW = Alg_PowConvert_dB.Convert_dBmtomW(powCurrent_dBm);
            currPrevious = dacCurrent;
                
            
            // calculate new point by linear estimation using the slope
            currLatest = checked((int)EstimateXAtY.Calculate
                (dacCurrent, powPrevious_mW,
                (1 / slope_p_mW), powerTarget_mW));
            return (int)currLatest;
        }

        /// <summary>
        /// Calculate a new analogue current value tuning point
        /// </summary>
        /// <param name="current">Present current level</param>
        /// <param name="powCurrent_dBm">Present Power in dBm (corrected)</param>
        /// <returns>New current level to try</returns>
        public double CalcNewTuningPoint(double current, double powCurrent_dBm)
        {
            // convert target to take into account the offset
            powPrevious_mW = Alg_PowConvert_dB.Convert_dBmtomW(powCurrent_dBm);
            currPrevious = current;

            // calculate new point by linear estimation using the slope
            currLatest = EstimateXAtY.Calculate
                (current, powPrevious_mW,
                (1 / slope_p_mW), powerTarget_mW);
            return currLatest;
        }

        /// <summary>
        /// Measure the power and check if it is in range
        /// </summary>
        /// <param name="pow_dBm">Power measured, corrected for offset</param>
        /// <returns>true if in range</returns>
        public bool MeasPowAndCheckInRange(out double pow_dBm)
        {
            pow_dBm = powMeasure.MeasPower_dBm();
            bool inRange = (Math.Abs(pow_dBm - powerTarget_dBm)
                                    < reqPowAccuracy_dB);
            
            return inRange;
        }

        /// <summary>
        /// Update the slope from new power point and previously 
        /// recorded data.
        /// </summary>
        public void UpdateSlope(double newPow_dBm)
        {
            // calculate new slope
            double pow_mW = Alg_PowConvert_dB.Convert_dBmtomW(newPow_dBm);
            powLatest_mW = pow_mW;
            if (currLatest != 0)
            {
                double newSlope = (currLatest - currPrevious) / (powLatest_mW - powPrevious_mW);
                slope_p_mW = newSlope;
            }
        }
        #region Private data

        Util_CalPowerMeasure powMeasure;
        double reqPowAccuracy_dB;
        double powerTarget_dBm;
        double powerTarget_mW;

        double currPrevious;
        double currLatest;
        double powPrevious_mW;
        double powLatest_mW;

        double slope_p_mW;        
        #endregion


    }
}

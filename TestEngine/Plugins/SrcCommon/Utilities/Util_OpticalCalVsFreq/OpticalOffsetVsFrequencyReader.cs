// [Copyright]
//
// Bookham ITLA Black Box
// Bookham.TestSolution.TunableModules
//
// SplitterCalReader.cs
//
// Author: paul.annetts, 2006
// Design: ITLA Module Setup and Functional Test Program DD

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.Algorithms;
using Bookham.TestLibrary.Utilities;

namespace Bookham.TestLibrary.Utilities
{
    /// <summary>
    /// OpticalOffsetVsFrequencyReader class. 
    /// Reads in OpticalOffsetVsFrequency object from a CSV file.
    /// </summary>
    public sealed class OpticalOffsetVsFrequencyReader
    {
        private OpticalOffsetVsFrequencyReader() { }

        /// <summary>
        /// internal enum used to determine whether file's values are in frequency (GHz)
        /// or wavelength (nm)
        /// </summary>
        private enum FrequencyMode
        {
            FrequencyGHz,
            Wavelength_nm
        }               

        /// <summary>
        /// Loads the splitter cal
        /// </summary>
        /// <param name="filename">filename to load from</param>
        /// <returns>the splitter cal</returns>
        static public OpticalOffsetVsFrequency LoadSplitterCal(string filename)
        {
            List<string[]> fileLines;

            using (CsvReader csvReader = new CsvReader())
            {
                fileLines = csvReader.ReadFile(filename);
            }

            // take into account the header line
            OpticalOffsetVsFrequency offsetVsFreq = new OpticalOffsetVsFrequency(fileLines.Count-1);
            int lineNbr = 0;
            double[] freqArray = offsetVsFreq.FreqGHz;
            double[] offsetArray = offsetVsFreq.Offset_dB;
            FrequencyMode freqMode = FrequencyMode.FrequencyGHz;

            foreach (string[] lineValues in fileLines)
            {
                try
                {
                    if (lineValues.Length < 2)
                    {
                        throw new Exception("Line doesn't have enough values!");
                    }

                    if (lineNbr == 0)
                    {
                        // read from header line whether data below is in wavelength or frequency
                        string header1 = lineValues[0].ToLower();
                        if (header1.Contains("frequency"))
                        {
                            freqMode = FrequencyMode.FrequencyGHz;
                        }
                        else if (header1.Contains("wavelength"))
                        {
                            freqMode = FrequencyMode.Wavelength_nm;
                        }
                        else throw new Exception("Invalid header line freq/wavelength mode: " + header1);

                        // always assume dB as offset
                    }
                    else
                    {
                        // read values from the file                        
                        // wavelength/frequency
                        double freq_wl_val = Double.Parse(lineValues[0]);
                        double freqGHz;
                        if (freqMode == FrequencyMode.Wavelength_nm)
                        {
                            freqGHz = Alg_FreqWlConvert.Wave_nm_TO_Freq_GHz(freq_wl_val);
                        }
                        else
                        {
                            freqGHz = freq_wl_val;
                        }
                        freqArray[lineNbr - 1] = freqGHz;

                        offsetArray[lineNbr-1] = Double.Parse(lineValues[1]);
                    }
                }
                catch (Exception e)
                {
                    string errMsg = String.Format("Bad line in file: {0}", lineNbr + 1);                        
                    throw new Exception(errMsg, e);
                }
                lineNbr++;                
            }

            return offsetVsFreq;
        }
    }
}

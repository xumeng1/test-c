using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.Utilities
{
    /// <summary>
    /// Exception in the Switch Path Manager
    /// </summary>
    public class Util_SwitchPathManagerException : Exception
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="message">Exception message</param>
        public Util_SwitchPathManagerException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="message">Exception message</param>
        /// <param name="innerException">Inner Exception</param>
        public Util_SwitchPathManagerException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}

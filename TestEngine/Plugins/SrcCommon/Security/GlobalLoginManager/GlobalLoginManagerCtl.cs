// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestPrograms
//
//GlobalLoginManager.cs
//
// Author: Ian Gurney, 2006
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Xml;
using System.Windows.Forms;
using Bookham.TestEngine.PluginInterfaces.Security;
using Login;

namespace Bookham.TestLibrary.Security
{
    /// <summary>
    /// Bookham login form.
    /// </summary>
    public partial class GlobalLoginManagerCtl : SecurityCtlBase
    {
        /// <summary>
        /// Bookham login form.
        /// </summary>
        public GlobalLoginManagerCtl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Load event.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private void GlobalLoginManagerCtl_Load(object sender, EventArgs e)
        {
            // Do nothing here //
        }

        /// <summary>
        /// Login response event, which actually does nothing.
        /// </summary>
        /// <param name="loginMsg">Login message.</param>
        private void GlobalLoginManagerCtl_LoginResponse(LoginResponseMsg loginMsg)
        {
            // Do nothing here //
        }

        /// <summary>
        /// Timer event which will trigger the login form.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private void timer1_Tick(object sender, EventArgs e)
        {
            // Disable timer //
            timer1.Enabled = false;

            // Define variables //
            GUI oLogin;
            LoginResponseMsg oResponse;
            XmlTextReader oFile;
            bool b = false;
            string s = "";
            string sDatabase = "";

            // Read configuration file //
            oFile = new XmlTextReader("Configuration\\Login.xml");

            try
            {
                sDatabase = oFile.ReadElementString("Database");
            }
            catch (UnauthorizedAccessException oException)
            {
                throw new SecurityException("Unauthorised access " + oException.Message);
            }
            catch (System.IO.FileNotFoundException oException)
            {
                throw new SecurityException("File not found " + oException.Message);
            }
            catch (System.IO.IOException oException)
            {
                throw new SecurityException("IO exception " + oException.Message);
            }
            catch (System.Xml.XmlException oException)
            {
                throw new SecurityException("XML exception " + oException.Message);
            }
            finally
            {
                oFile.Close();
            }

            // Create new instance of login object //
            oLogin = new Login.GUI();

            if (oLogin.ShowLogin(sDatabase))
            {
                // Read global ID back //
                s = oLogin.GlobalID;

                // For some reason we need to pass "b" by reference instead of omitting it //

                if (oLogin.Password.IsIndirect(s, ref b))
                {
                    // Lets pretend an indirect is a technician //
                    oResponse = new LoginResponseMsg(PrivilegeLevel.Technician);
                }
                else if (oLogin.Password.IsTechnician(s, ref b))
                {
                    oResponse = new LoginResponseMsg(PrivilegeLevel.Technician);
                }
                else
                {
                    // This could be incorrect if SAP database is incorrect //
                    oResponse = new LoginResponseMsg(PrivilegeLevel.Operator);
                }
            }
            else
            {
                oResponse = new LoginResponseMsg(LoginRequestStatus.InvalidPassword);
            }

            // Send message back to test engine //
            this.SendLoginRequest(new LoginReq(s, oResponse));
        }

        /// <summary>
        /// Enter, event which will re-enable the timer.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private void GlobalLoginManagerCtl_Enter(object sender, EventArgs e)
        {
            timer1.Enabled = true;
        }

        /// <summary>
        /// Act on into view event, which will re-enable the timer.
        /// </summary>
        /// <returns></returns>
        private IButtonControl GlobalLoginManagerCtl_ActOnIntoView()
        {
            timer1.Enabled = true;
            return default(IButtonControl);
        }
    }
}

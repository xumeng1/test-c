// [Copyright]
//
// Bookham Modular Test Engine Library
// Login Manager
//
// Library/LoginManager/LoginManager.cs
// 
// Author: Bill Godfrey
// Design: Core DD version 1.0

using System;
using System.Xml;
using Bookham.TestEngine.PluginInterfaces.Security;

namespace Bookham.TestLibrary.Security
{
	/// <summary>
	/// Default Login Manager will simple User/Password login. 
	/// </summary>
	public class LoginManager: ISecurityPlugin
	{
		/// <summary>
		/// Construct login manager, called by plugin loader.
		/// </summary>
		public LoginManager()
		{
			/* Set up user info, allowing any exceptions to fall. */
            this.userInfo = new UserInfoXml();
		}

		/// <summary>
		/// Return type of GUI control to use with this plugin.
		/// </summary>
		public Type Control
		{
			get
			{
				return typeof(LoginManagerCtl);
			}
		}

		/// <summary>
		/// Process a logout notification.
		/// </summary>
		/// <remarks>This class cannot fail a logout request.</remarks>
		/// <param name="user">Logging out user.</param>
		/// <param name="priv">Former user's privilege.</param>
		/// <param name="logWriter">Reference to log writer object.</param>
		/// <returns>Success result.</returns>
		public LogoutStatus LogoutNotification(string user, PrivilegeLevel priv, ILogWriter logWriter)
		{
			logWriter.DeveloperWrite("User " + user + " has logged out.");
			return LogoutStatus.Success;
		}

		/// <summary>
		/// Process a login request, scanning the XML file for the login details.
		/// </summary>
		/// <param name="msg">Unauthenticated credentials from the login GUI control.</param>
		/// <param name="logWriter">Reference to log writer object.</param>
		/// <returns>Success/failure result.</returns>
		public LoginResponseMsg LoginRequest(LoginRequestMsg msg, ILogWriter logWriter)
		{
			/* Check message type. */
			LoginManagerLoginRequest reqMsg = msg as LoginManagerLoginRequest;

			if (reqMsg == null)
			{
				logWriter.ErrorRaise("LoginRequest with bad message of type" + msg.GetType().FullName);
			}

			/* Refresh the user info file. */
			refreshUserInfo(logWriter);

			/* Query the record for claimed user. */
			UserRecord loadedUser = this.userInfo[reqMsg.UserId];

			/* If we found the record, check credentials. */
			if (loadedUser != null)
			{
				/* Test password. */
				if (PasswordHash.TestPassword(
					loadedUser.User,
					loadedUser.Priv, 
					reqMsg.PassWord, 
					loadedUser.PassHash))
				{	
					/* Success */
					logWriter.DeveloperWrite("User " + reqMsg.UserId + " authenticated as " + loadedUser.Priv); 
					return new LoginResponseMsg(loadedUser.Priv);
				}
				else
				{
					/* Password match failed. */
					logWriter.DeveloperWrite("Supposed user " + reqMsg.UserId + " failed to supply correct password.");
					return new LoginResponseMsg(LoginRequestStatus.InvalidPassword);
				}
			}
			else
			{
				/* Finished scanning file without finding user. */
				logWriter.DeveloperWrite("Supposed user " + reqMsg.UserId + " does not exist.");
				return new LoginResponseMsg(LoginRequestStatus.UserDoesNotExist);
			}
		}

		/// <summary>
		/// Reload the UserInfoXml, but keep the old one if it throws anything. All
		/// exceptions thrown will be logged but otherwise ignored.
		/// </summary>
		private void refreshUserInfo(ILogWriter logWriter)
		{
			try
			{
				UserInfoXml newUsers = new UserInfoXml();

				/* If we reach here, the load worked. */
				this.userInfo = newUsers;
			}
			catch (UserInfoXmlException uixe)
			{
				/* Catch all exceptions and log, but otherwise continue. */
				logWriter.ErrorWriteWithException(
					"Attempt to refresh UserInfo.XML file failed.\n",
					uixe);
			}
		}

		#region Private data
		/// <summary>
		/// Stored user information.
		/// </summary>
		private UserInfoXml userInfo;
		#endregion

	}
}

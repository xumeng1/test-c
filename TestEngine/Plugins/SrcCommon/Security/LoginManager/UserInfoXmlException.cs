// [Copyright]
//
// Bookham Modular Test Engine Library
// Login Manager
//
// Library/LoginManager/UserInfoXml.cs
// 
// Author: Bill Godfrey
// Design: Core DD version 1.0

using System;

namespace Bookham.TestLibrary.Security
{
	/// <summary>
	/// Exception indicating a failure when the User Info file is scanned.
	/// </summary>
	[Serializable]
	internal class UserInfoXmlException: Bookham.TestEngine.PluginInterfaces.Security.SecurityException
	{
		/// <summary>
		/// Construct exception with message.
		/// </summary>
		/// <param name="message">Message text.</param>
		internal UserInfoXmlException(string message)
			: base(message)
		{
		}

		/// <summary>
		/// Construct exception with message and wrapped inner exception.
		/// </summary>
		/// <param name="message">Message text.</param>
		/// <param name="caughtException">Inner exception.</param>
		internal UserInfoXmlException(string message, System.Exception caughtException)
			: base(message, caughtException)
		{
		}
	}
}

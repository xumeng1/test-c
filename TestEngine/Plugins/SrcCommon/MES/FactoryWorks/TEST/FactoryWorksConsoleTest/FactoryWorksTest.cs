// [Copyright]
//
// Bookham Modular Test Engine
// PluginLoader Test Harness
//
// Library/MES/FactoryWorksTest/FactoryWorksTest.cs
// 
// Author: Blair Harris
// Design: As specified in FactoryWorks DD

using System;
using System.IO;
using System.Diagnostics;
using System.Reflection;

using NUnit.Framework;
using Bookham.TestEngine.Framework.Logging;
using Bookham.TestEngine.Framework.Messages;
using Bookham.TestEngine.PluginInterfaces.MES;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestLibrary.MES;



namespace BookhamNUnitTests
{

	[TestFixture]
	public class FactoryWorksTest
	{


		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{
			FactoryWorksTest test = new FactoryWorksTest();
			test.Setup();
			
			//test.LoadBatch_StdAttributes();
			//test.LoadBatch_CustomAttributes();

			//test.SetAttributes_BatchLevelOk();
			//test.SetAttributes_ComponentLevelOk();

			//test.TrackOutComponentFailOk(); // Tested Ok
			//test.TrackOutComponentOnHoldOk(); // Tested Ok
			//test.TrackOutComponentReworkOk(); // Tested Ok

			//test.TrackOutBatchOk(); // Tested Ok
			//test.TrackOutBatchOnHoldOk(); // Tested Ok
		}






		[TestFixtureSetUp]
		public void Setup()
		{
			// initialise logging domain
			UnhandledExceptionsHandler.Initialise();
			Initializer.Init();
			Log.EnableOutputToTestWindow();
			// Optionally resteer output from Debug window to Console (useful for NUNIT GUI)
			//BugOut.SetOutputToConsole(true);

			// initialise MsgCentre
			//Bookham.TestEngine.Framework.Messages.BackEnd.Startup.InitialiseMsgCentre();
			// initialise Timer Service
			//Bookham.TestEngine.Framework.Timers.TimerService.Initialise();


			fw = new FactoryWorks( @"..\..\FactoryWorksConfig.xml" );
		}


		[Test]
		public void LoadBatch_NoAttributes()
		{
			MESbatch batch = fw.LoadBatch( "EN1.1", false );
			Debug.WriteLine( batch.PartCode );
			Debug.WriteLine( batch.Stage );
		}

		[Test]
		public void LoadBatch_StdAttributes()
		{
			MESbatch batch = fw.LoadBatch( "EN1.1", true );
			Debug.Assert( batch.PartCode == "AT10GC" );
			Debug.Assert( batch.Stage == "HSRXALIGN" );
			Debug.Assert( batch.Count == 5 );
			DatumList dl = batch[0].Attributes;
			string chipType = dl.ReadString( "ChipId" );
			Debug.Assert( chipType == "NAS" );
		}

		[Test]
		[ExpectedException(typeof(MESInvalidOperationException))]
		public void LoadBatch_DoesntExist()
		{
			fw.LoadBatch( "CL1.1", false );
		}
		
		[Test]
		public void LoadBatch_CustomAttributes()
		{
			string[] customAttributes = { "CarrierType" };
			MESbatch batch = fw.LoadBatch( "EN00001.1", customAttributes, customAttributes );
		}

		[Test]
		public void SetAttributes_BatchLevelOk()
		{
			fw.SetBatchAttribute( "EN1.1", "Generic", "TEST" );
			System.Threading.Thread.Sleep( 5000 ); // Wait for FW to process the request
			
			string[] attribList = { "Generic" };
			MESbatch batch = fw.LoadBatch( "EN1.1", attribList, null );
			string Generic = batch.Attributes.ReadString("Generic");
			Debug.Assert( Generic == "TEST" );

			fw.SetBatchAttribute( "EN1.1", "Generic", "" );
		}

		[Test]
		public void SetAttributes_ComponentLevelOk()
		{
			fw.SetComponentAttribute( "EN1.1", "EN1.006", "Generic", "COMPTEST" );
			System.Threading.Thread.Sleep( 5000 ); // Wait for FW to process the request

			string[] attribList = { "Generic" };
			MESbatch batch = fw.LoadBatch( "EN1.1", null, attribList );
			DUTObject dut = batch[0];
			string Generic = dut.Attributes.ReadString("Generic");
			Debug.Assert( Generic == "COMPTEST" );

			fw.SetComponentAttribute( "EN1.1", "EN1.006", "Generic", "" );
		}

		[Test]
		public void TrackOutComponentFailOk()
		{
			fw.TrackOutComponentFail( "EN1.1", "HSRXALIGN", "EN1.009", "FAILTEST" );
		}

		[Test]
		public void TrackOutComponentOnHoldOk()
		{
			string newBatchId = fw.TrackOutComponentOnHold( "EN1.1", "HSRXALIGN", "EN1.010", "ONHOLDTEST" );
		}

		[Test]
		public void TrackOutComponentReworkOk()
		{
			string newBatchId = fw.TrackOutComponentRework( "EN1.1", "HSRXALIGN", "EN1.011", "ComponentDamage" );
		}



		[Test]
		public void TrackOutBatchOk()
		{
			fw.TrackIn( "EN1.1" );
			fw.TrackOutBatchPass( "EN1.1", "HSRXALIGN", "PartA" );
		}

		[Test]
		public void TrackOutBatchOnHoldOk()
		{
			fw.TrackOutBatchOnHold( "EN1.1", "HSRXALIGN", "ONHOLDTEST" );
		}



		private FactoryWorks fw;

	}
}

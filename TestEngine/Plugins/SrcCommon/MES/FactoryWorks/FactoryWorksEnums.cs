// [Copyright]
//
// Bookham Modular Test Engine
// FactoryWorks MES Plug-in
//
// Library/MES/FactoryWorks/FactoryWorksEnums.cs
// 
// Author: Blair Harris
// Design: As specified in FactoryWorks MES Plug-In DD

using System;



namespace Bookham.TestLibrary.MES
{
	/// <summary>
	/// Determines which drop folder is used
	/// (either LOT or COMPONENT)
	/// </summary>
	internal enum RequestLevel
	{
		COMPONENT,
		LOT
	};

	/// <summary>
	/// The location of the request file
	/// </summary>
	internal enum RequestState
	{
		FILE_NOT_SAVED,
        LOCAL,
		DROPPED
	};
}

// [Copyright]
//
// Bookham Modular Test Engine
// FactoryWorks MES Plug-in
//
// Library/MES/FactoryWorks/Response.cs
// 
// Author: Blair Harris
// Design: As specified in FactoryWorks MES Plug-In DD

using System;
using System.IO;
using System.Globalization;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using Bookham.TestEngine.PluginInterfaces.MES;



namespace Bookham.TestLibrary.MES
{
	
	/// <summary>
	/// Encapsulates a FactoryWorks response file
	/// </summary>
	internal sealed class Response
	{
		#region Public Methods
		/// <summary>
		/// Constructor
		/// </summary>
		internal Response()
		{
			string filename = "mess_" + Response.Node.ToString( CultureInfo.InvariantCulture );
			this.filepath = Path.Combine( Response.DropDir, filename );
		}

		/// <summary>
		/// Check if response file is ready to read
		/// </summary>
		/// <returns>If true, then it is ready</returns>
		internal bool IsReady()
		{
	
			// Awaiting MAST implementation of lock file
			// to determine when TSSrv has finished writting
			// to the response file. Until that time use
			// the method below.

			// Wait 1/4sec and then check if the file exists
			System.Threading.Thread.Sleep(250);
			return File.Exists(this.filepath);
		}

		/// <summary>
		/// Read the response file and return its contents
		/// </summary>
		/// <returns>File contents - each array element holds one line</returns>
		internal string[] ReadFile()
		{
			string[] array;

			using( StreamReader reader = new StreamReader(this.filepath) )
			{
				StringCollection lineList = new StringCollection();
				string line;
				while ((line = reader.ReadLine()) != null) 
				{
					lineList.Add( line );
				}

				// Was the file empty?
				if( lineList.Count == 0 )
					throw new MESCommsException( "Empty response file" );

				array = new string[lineList.Count];
				lineList.CopyTo(array,0);
			}

			return array;
		}

		/// <summary>
		/// Remove(delete) the response file
		/// </summary>
		internal void Remove()
		{
			File.Delete( this.filepath );
		}
		#endregion
		
		#region Public Static Data
		/// <summary>The node number of the client making the request</summary>
		public static int Node;
		/// <summary>Full path of the folder into which the request file will be dropped</summary>
		public static string DropDir;
		#endregion

		#region Private Data
		/// <summary>Response file pathname</summary>
		private string filepath;
		#endregion
	}
}

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.TestJig;
using Bookham.TestEngine.Equipment;

namespace Bookham.TestLibrary.TestJigs
{
    /// <summary>
    /// Test Engine Library Default Test Jig Plug-in.
    /// The ITestJig.GetJigID method returns the 
    /// string "FCU2AsicTestJigId".
    /// </summary>
    public class FCU2AsicTestJig:ITestJig 
    {

        private ChassisCollection chassisCollection;
        private InstrumentCollection instrumentCollection;



        #region ITestJig Members
        /// <summary>
        /// Sets the Chassis collection to be used by the Test Jig Plug-in.
        /// </summary>
        public ChassisCollection ChassisCollection
        {
            set { chassisCollection = value; }
        }
        /// <summary>
        /// Returns the ID of the test jig. 
        /// This incarnation is a stub which returns a default value.
        /// </summary>
        /// <param name="InDebugMode">True if running in debug mode</param>
        /// <param name="InSimulationMode">True if running in simulation mode</param>
        /// <returns>Default Test Jig ID string: "FCU2AsicTestJig"</returns>
        public string GetJigID(bool InDebugMode, bool InSimulationMode)
        {
            return "FCU2AsicTestJigId";
        }

        /// <summary>
        /// Sets the Instrument collection to be used by the Test Jig Plug-in.
        /// </summary>
        public InstrumentCollection InstCollection
        {
            set { instrumentCollection = value; }
        }

        #endregion
    }
}

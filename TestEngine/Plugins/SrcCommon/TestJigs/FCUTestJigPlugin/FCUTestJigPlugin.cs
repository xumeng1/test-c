// [Copyright]
//
// Bookham Modular Test Engine
// Default Test Jig Plug-in
//
// Library/TestJigs/FCUTestJigPlugin/FCUTestJigPlugin.cs
// 
// Author: Joseph Olajubu
// Design: Test Control DD

using System;
using Bookham.TestEngine.PluginInterfaces.TestJig;
using Bookham.TestEngine.Equipment;

namespace Bookham.TestLibrary.TestJigs
{

    /// <summary>
    /// Test Engine Library Default Test Jig Plug-in.
    /// The ITestJig.GetJigID method returns the 
    /// string "FCUTestJigID".
    /// </summary>
    public class FCUTestJigPlugin : ITestJig
    {

        /// <summary>
        /// Sets the Instrument collection to be used by the Test Jig Plug-in.
        /// </summary>
        public InstrumentCollection InstCollection
        {
            set
            {
                this.instCollection = value;
            }
        }

        /// <summary>
        /// Sets the Chassis collection to be used by the Test Jig Plug-in.
        /// </summary>
        public ChassisCollection ChassisCollection
        {
            set
            {
                this.chassisCollection = value;
            }
        }

        // Summary:
        //     
        //
        // Parameters:
        //   InDebugMode:
        //     True if running in debug mode
        //
        //   InSimulationMode:
        //     True if running in simulation mode

        /// <summary>
        /// Returns the ID of the test jig. 
        /// This incarnation is a stub which returns a default value.
        /// </summary>
        /// <param name="InDebugMode">True if running in debug mode</param>
        /// <param name="InSimulationMode">True if running in simulation mode</param>
        /// <returns>Default Test Jig ID string: "FCUTestJigID"</returns>
        public string GetJigID(bool InDebugMode, bool InSimulationMode)
        {
            return "FCUTestJigID";
        }


        private InstrumentCollection instCollection;
        private ChassisCollection chassisCollection;

    }
}

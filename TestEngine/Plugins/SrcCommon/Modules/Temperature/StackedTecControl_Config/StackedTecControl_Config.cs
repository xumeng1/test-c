using System;
using Bookham.TestEngine.Framework.InternalData;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.TestModules
{

    /// <summary>
    /// This structure encapsulates all the data requirements of the test module
    /// At test program level the user declares an instance of the structure, but now a new instance, 
    /// ie mystruct myInstance;
    /// then they can make populate the members of the struct
    /// ie
    /// myInstance.param1 = 20;
    /// myInstance.param2 = 20;
    /// 
    /// Only when the structure is fully populated can they call
    /// DatumList myD = myInstance.GetDatumList();
    /// 
    /// Else they will get a compile time error, as the object myInstance
    /// only exists at the point where all members are populated!
    /// 
    /// In the test module level the user simply calls
    /// mystruct myInstance = new mystruct(configData);
    /// and they can read the myInstance.param1, myInstance.param2 = 20 values;
    /// </summary>
    public struct StackedTecControl_Config
    {
        /// <summary>
        /// Method populates a datumlist based on this structure data
        /// </summary>
        public DatumList GetDatumList()
        {
            DatumList myLocalDatumList = new DatumList();
            DatumSint32 TimeBetweenReadings_ms = new DatumSint32("TimeBetweenReadings_ms", this.TimeBetweenReadings_ms);
            DatumDouble MaxExpectedTimeForOperation_s = new DatumDouble("MaxExpectedTimeForOperation_s", this.MaxExpectedTimeForOperation_s);
            DatumDouble BtmTec_RqdTimeInToleranceWindow_s = new DatumDouble("BtmTec_RqdTimeInToleranceWindow_s", this.BtmTec_RqdTimeInToleranceWindow_s);
            DatumDouble BtmTec_RqdStabilisationTime_s = new DatumDouble("BtmTec_RqdStabilisationTime_s", this.BtmTec_RqdStabilisationTime_s);
            DatumDouble BtmTec_SetPointTemperature_C = new DatumDouble("BtmTec_SetPointTemperature_C", this.BtmTec_SetPointTemperature_C);
            DatumDouble BtmTec_TemperatureTolerance_C = new DatumDouble("BtmTec_TemperatureTolerance_C", this.BtmTec_TemperatureTolerance_C);
            DatumDouble TopTec_RqdTimeInToleranceWindow_s = new DatumDouble("TopTec_RqdTimeInToleranceWindow_s", this.TopTec_RqdTimeInToleranceWindow_s);
            DatumDouble TopTec_RqdStabilisationTime_s = new DatumDouble("TopTec_RqdStabilisationTime_s", this.TopTec_RqdStabilisationTime_s);
            DatumDouble TopTec_SetPointTemperature_C = new DatumDouble("TopTec_SetPointTemperature_C", this.TopTec_SetPointTemperature_C);
            DatumDouble TopTec_TemperatureTolerance_C = new DatumDouble("TopTec_TemperatureTolerance_C", this.TopTec_TemperatureTolerance_C);
            myLocalDatumList.Add(TopTec_RqdStabilisationTime_s);
            myLocalDatumList.Add(MaxExpectedTimeForOperation_s);
            myLocalDatumList.Add(BtmTec_RqdTimeInToleranceWindow_s);
            myLocalDatumList.Add(BtmTec_RqdStabilisationTime_s);
            myLocalDatumList.Add(BtmTec_SetPointTemperature_C);
            myLocalDatumList.Add(BtmTec_TemperatureTolerance_C);
            myLocalDatumList.Add(TopTec_RqdTimeInToleranceWindow_s);
            myLocalDatumList.Add(TopTec_RqdStabilisationTime_s);
            myLocalDatumList.Add(TopTec_TemperatureTolerance_C);
            myLocalDatumList.Add(TopTec_SetPointTemperature_C);

            return (myLocalDatumList);
        }

        /// <summary>
        /// Constructor being passed a datumlist and populates a StackedTecControlType
        /// </summary>
        /// <param name="mydata">Input DatumList</param>
        public StackedTecControl_Config(DatumList mydata)
        {
            this.TimeBetweenReadings_ms = mydata.ReadSint32("TimeBetweenReadings_ms");
            this.MaxExpectedTimeForOperation_s = mydata.ReadDouble("MaxExpectedTimeForOperation_s");
            this.BtmTec_RqdTimeInToleranceWindow_s = mydata.ReadDouble("BtmTec_RqdTimeInToleranceWindow_s");
            this.BtmTec_RqdStabilisationTime_s = mydata.ReadDouble("BtmTec_RqdStabilisationTime_s");
            this.BtmTec_SetPointTemperature_C = mydata.ReadDouble("BtmTec_SetPointTemperature_C");
            this.BtmTec_TemperatureTolerance_C = mydata.ReadDouble("BtmTec_TemperatureTolerance_C");
            this.TopTec_RqdTimeInToleranceWindow_s = mydata.ReadDouble("TopTec_RqdTimeInToleranceWindow_s");
            this.TopTec_RqdStabilisationTime_s = mydata.ReadDouble("TopTec_RqdStabilisationTime_s");
            this.TopTec_TemperatureTolerance_C = mydata.ReadDouble("TopTec_TemperatureTolerance_C");
            this.TopTec_SetPointTemperature_C = mydata.ReadDouble("TopTec_SetPointTemperature_C");
        }

        //All the parameters I need
        /// <summary> Time between readins in milliseconds. </summary>
        public Int32 TimeBetweenReadings_ms;
        /// <summary> Maximum time allowed to reach target temperature. </summary>
        public double MaxExpectedTimeForOperation_s;
        /// <summary> Bottom TEC required time in tolerance window.  </summary>
        public double BtmTec_RqdTimeInToleranceWindow_s;
        /// <summary> Bottom TEC reqired stabalisation time. </summary>
        public double BtmTec_RqdStabilisationTime_s;
        /// <summary> Bottom TEC set point temperature. </summary>
        public double BtmTec_SetPointTemperature_C;
        /// <summary> Bottom TEC reqired temperature tolerance. </summary>
        public double BtmTec_TemperatureTolerance_C;
        /// <summary> Top TEC required time in tolerance window. 
        public double TopTec_RqdTimeInToleranceWindow_s;
        /// <summary> Top TEC reqired stabalisation time. </summary>
        public double TopTec_RqdStabilisationTime_s;
        /// <summary> Top TEC set point temperature. </summary>
        public double TopTec_SetPointTemperature_C;
        /// <summary> Top TEC reqired temperature tolerance. </summary>
        public double TopTec_TemperatureTolerance_C;
    }
}

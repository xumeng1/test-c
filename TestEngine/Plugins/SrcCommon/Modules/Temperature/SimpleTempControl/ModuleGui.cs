// [Copyright]
//
// Bookham Test Engine
// ChamberControl.cs
//
// Bookham.TestSolution.TestModules/ModuleGui.cs
// 
// Author: Paul.Worsey, October 2006
// Design: As per Chamber Control Module DD

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Bookham.TestEngine.Framework.InternalData;

namespace Bookham.TestLibrary.TestModules
{
    /// <summary>
    /// GUI
    /// </summary>
    public partial class ModuleGui : Bookham.TestEngine.Framework.Messages.ManagedCtlBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public ModuleGui()
        {
            /* Call designer generated code. */
            InitializeComponent();

            /* TODO - Write constructor code here. */
        }
        /// <summary>
        /// Event handler for Gui
        /// </summary>
        /// <param name="payload"></param>
        /// <param name="inMsgSeq"></param>
        /// <param name="respSeq"></param>
        private void ModuleGui_MsgReceived(object payload, long inMsgSeq, long respSeq)
        {
            if (payload.GetType() == typeof(DatumDouble))
            {
                DatumDouble datumDbl = (DatumDouble)payload;

                switch (datumDbl.Name)
                {
                    case "SetPointTemperature_C":
                        this.SpValue.Text = String.Format("{0:f}", datumDbl.Value);
                        break;

                    case "ActualTemp":
                        this.ActualValue.Text = String.Format("{0:f}", datumDbl.Value);
                        break;


                    default:
                        throw new ArgumentException("Invalid datum received: " + datumDbl.Name);
                }
            }
            if (payload.GetType() == typeof(DatumString))
            {
                DatumString datumStr = (DatumString)payload;

                switch (datumStr.Name)
                {

                    case "StabilTime":
                        this.StabilisationTimer.Text = String.Format("{0}", datumStr.Value);
                        break;
 
                    case "MaxTime":
                        this.MaxTimer.Text = String.Format("{0}", datumStr.Value);
                        break;

                    default:
                        throw new ArgumentException("Invalid datum received: " + datumStr.Name);
                }
            }
            if (payload.GetType() == typeof(String))
            {
                InstName.Text = (string)payload;
            }
        }

      
    }
}

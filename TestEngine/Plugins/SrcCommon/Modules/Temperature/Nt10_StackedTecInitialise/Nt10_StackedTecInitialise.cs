using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;

namespace Bookham.TestLibrary.TestModules
{
    /// <summary>
    /// Initialise stacked Nt10 TEC Controllers
    /// </summary>
    public class Nt10_StackedTecInitialise : ITestModule
    {
        #region ITestModule Members
        /// <summary>
        /// Run the module.
        /// </summary>
        /// <param name="engine">Reference to the Test Engine object</param>
        /// <param name="userType">Technician or operator</param>
        /// <param name="configData">Config Data - see definition of Nt10_StackedTecInitialise_Config</param>
        /// <param name="instruments">Instruments: 
        /// - TopTec_Controller
        /// - BtmTec_Controller
        /// </param>
        /// <param name="chassis">None</param>
        /// <param name="calData">None</param>
        /// <param name="previousTestData">None</param>
        /// <returns>None</returns>
        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, DatumList configData, InstrumentCollection instruments, ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {
            DatumList myTestModuleData = new DatumList();

            Nt10_StackedTecInitialise_Config myConfigData = new Nt10_StackedTecInitialise_Config(configData);
            
            
            //** Do Top Tec bits **//
            InstType_TecController TopTec_Controller = (InstType_TecController)instruments["TopTec_Controller"];
            
            if (myConfigData.TopTec_ControlMode == InstType_TecController.ControlMode.Temperature)
            {
                //No need to worry about the steinhart constants
                TopTec_Controller.OperatingMode = InstType_TecController.ControlMode.Temperature;
            }
            else //Assume resistance mode therefore pass in the Steinhart data, if mode is wrong driver will catch it
            {
                TopTec_Controller.OperatingMode = InstType_TecController.ControlMode.Resistance;
                SteinhartHartCoefficients csts = new SteinhartHartCoefficients(); 
                csts.A = myConfigData.TopTecSteinHartData_A;
                csts.B = myConfigData.TopTecSteinHartData_B;
                csts.C = myConfigData.TopTecSteinHartData_C;
                TopTec_Controller.SteinhartHartConstants = csts;
            }
            
            TopTec_Controller.ProportionalGain = myConfigData.TopTec_ProportionalGain;
            TopTec_Controller.IntegralGain =myConfigData.TopTec_IntegralGain;
            TopTec_Controller.TecCurrentCompliance_amp = myConfigData.TopTec_MaxI;
            
    
            //************** So endeth the top tecs set up ********************//
            
            //**** Now do bottom tecs set ups *****//
            InstType_TecController BtmTec_Controller = (InstType_TecController)instruments["BtmTec_Controller"];

            if (myConfigData.BtmTec_ControlMode == InstType_TecController.ControlMode.Temperature)
            {
                //No need to worry about the steinhart constants
                BtmTec_Controller.OperatingMode = InstType_TecController.ControlMode.Temperature;

            }
            else //Assume resistance mode therefore pass in the Steinhart data, if mode is wrong driver will catch it
            {
                BtmTec_Controller.OperatingMode = InstType_TecController.ControlMode.Resistance;
                SteinhartHartCoefficients csts = new SteinhartHartCoefficients();
                csts.A = myConfigData.BtmTecSteinHartData_A;
                csts.B = myConfigData.BtmTecSteinHartData_B;
                csts.C = myConfigData.BtmTecSteinHartData_C;
                BtmTec_Controller.SteinhartHartConstants = csts;
            }

            BtmTec_Controller.ProportionalGain = myConfigData.BtmTec_ProportionalGain;
            BtmTec_Controller.IntegralGain = myConfigData.BtmTec_IntegralGain;
            BtmTec_Controller.TecCurrentCompliance_amp = myConfigData.BtmTec_MaxI;

            return (myTestModuleData);
        }

        /// <summary>
        /// Empty GUI for this module.
        /// </summary>
        public Type UserControl
        {
            get { return (typeof(ModuleGui)); }
        }

        #endregion
    }
}

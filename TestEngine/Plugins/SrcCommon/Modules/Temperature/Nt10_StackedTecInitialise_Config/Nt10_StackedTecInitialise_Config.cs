using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestLibrary.InstrTypes;


namespace Bookham.TestLibrary.TestModules
{
    /// <summary>
    /// This structure encapsulates all the data requirements of the test module
    /// At test program level the user declares an instance of the structure, but now a new instance, 
    /// ie mystruct myInstance;
    /// then they can make populate the members of the struct
    /// ie
    /// myInstance.param1 = 20;
    /// myInstance.param2 = 20;
    /// 
    /// Only when the structure is fully populated can they call
    /// DatumList myD = myInstance.GetDatumList();
    /// 
    /// Else they will get a compile time error, as the object myInstance
    /// only exists at the point where all members are populated!
    /// 
    /// In the test module level the user simply calls
    /// mystruct myInstance = new mystruct(configData);
    /// and they can read the myInstance.param1, myInstance.param2 = 20 values;
    /// </summary>
    public struct Nt10_StackedTecInitialise_Config
    {
        /// <summary>
        /// Method populates a datumlist based on this structure data
        /// </summary>
        /// <returns>datumlist</returns>
        public DatumList GetDatumList()
        {
            DatumList myLocalDatumList = new DatumList();

            DatumDouble TopTec_IntegralGain = new DatumDouble("TopTec_IntegralGain", this.TopTec_IntegralGain);
            DatumDouble TopTec_ProportionalGain = new DatumDouble("TopTec_ProportionalGain", this.TopTec_ProportionalGain);
            DatumDouble TopTec_MaxI = new DatumDouble("TopTec_MaxI", this.TopTec_MaxI);
            DatumEnum TopTec_ControlMode = new DatumEnum("TopTec_ControlMode", this.TopTec_ControlMode);
            DatumDouble TopTecSteinHartData_A = new DatumDouble("TopTecSteinHartData_A", this.TopTecSteinHartData_A);
            DatumDouble TopTecSteinHartData_B = new DatumDouble("TopTecSteinHartData_B", this.TopTecSteinHartData_B);
            DatumDouble TopTecSteinHartData_C = new DatumDouble("TopTecSteinHartData_C", this.TopTecSteinHartData_C); ;

            DatumDouble BtmTec_IntegralGain = new DatumDouble("BtmTec_IntegralGain", this.BtmTec_IntegralGain);
            DatumDouble BtmTec_ProportionalGain = new DatumDouble("BtmTec_ProportionalGain", this.BtmTec_ProportionalGain);
            DatumDouble BtmTec_MaxI = new DatumDouble("BtmTec_MaxI", this.BtmTec_MaxI);
            DatumEnum BtmTec_ControlMode = new DatumEnum("BtmTec_ControlMode", this.BtmTec_ControlMode);
            DatumDouble BtmTecSteinHartData_A = new DatumDouble("BtmTecSteinHartData_A", this.BtmTecSteinHartData_A);
            DatumDouble BtmTecSteinHartData_B = new DatumDouble("BtmTecSteinHartData_B", this.BtmTecSteinHartData_B);
            DatumDouble BtmTecSteinHartData_C = new DatumDouble("BtmTecSteinHartData_C", this.BtmTecSteinHartData_C); ;

            myLocalDatumList.Add(TopTec_IntegralGain);
            myLocalDatumList.Add(TopTec_ProportionalGain);
            myLocalDatumList.Add(TopTec_MaxI);
            myLocalDatumList.Add(TopTec_ControlMode);
            myLocalDatumList.Add(TopTecSteinHartData_A);
            myLocalDatumList.Add(TopTecSteinHartData_B);
            myLocalDatumList.Add(TopTecSteinHartData_C);

            myLocalDatumList.Add(BtmTec_IntegralGain);
            myLocalDatumList.Add(BtmTec_ProportionalGain);
            myLocalDatumList.Add(BtmTec_MaxI);
            myLocalDatumList.Add(BtmTec_ControlMode);
            myLocalDatumList.Add(BtmTecSteinHartData_A);
            myLocalDatumList.Add(BtmTecSteinHartData_B);
            myLocalDatumList.Add(BtmTecSteinHartData_C);

            return (myLocalDatumList);
        }




        /// <summary>
        /// Constructor which takes a datumList and populates our StackedTecControlType
        /// </summary>
        /// <param name="mydata"></param>
        public Nt10_StackedTecInitialise_Config(DatumList mydata)
        {
            this.TopTec_ControlMode = (InstType_TecController.ControlMode)mydata.ReadEnum("TopTec_ControlMode");
            this.TopTec_IntegralGain = mydata.ReadDouble("TopTec_IntegralGain");
            this.TopTec_ProportionalGain = mydata.ReadDouble("TopTec_ProportionalGain");
            this.TopTec_MaxI = mydata.ReadDouble("TopTec_MaxI");
            this.TopTecSteinHartData_A = mydata.ReadDouble("TopTecSteinHartData_A");
            this.TopTecSteinHartData_B = mydata.ReadDouble("TopTecSteinHartData_B");
            this.TopTecSteinHartData_C = mydata.ReadDouble("TopTecSteinHartData_C");


            this.BtmTec_ControlMode = (InstType_TecController.ControlMode)mydata.ReadEnum("BtmTec_ControlMode");
            this.BtmTec_IntegralGain = mydata.ReadDouble("BtmTec_IntegralGain");
            this.BtmTec_ProportionalGain = mydata.ReadDouble("BtmTec_ProportionalGain");
            this.BtmTec_MaxI = mydata.ReadDouble("BtmTec_MaxI");
            this.BtmTecSteinHartData_A = mydata.ReadDouble("BtmTecSteinHartData_A");
            this.BtmTecSteinHartData_B = mydata.ReadDouble("BtmTecSteinHartData_B");
            this.BtmTecSteinHartData_C = mydata.ReadDouble("BtmTecSteinHartData_C");

        }

        //List of all my parameters i will use
        /// <summary> Top TEC Control mode</summary>
        public InstType_TecController.ControlMode TopTec_ControlMode;
        /// <summary> Top TEC Integral gain </summary>
        public double TopTec_IntegralGain;
        /// <summary> Top TEC proportional gain </summary>
        public double TopTec_ProportionalGain;
        /// <summary> Top TEC Max current </summary>
        public double TopTec_MaxI;
        /// <summary> Top TEC Steinhart-Hart Constant A </summary>
        public double TopTecSteinHartData_A;
        /// <summary> Top TEC Steinhart-Hart Constant B </summary>
        public double TopTecSteinHartData_B;
        /// <summary> Top TEC Steinhart-Hart Constant C </summary>
        public double TopTecSteinHartData_C;

        /// <summary> Bottom TEC Control mode</summary>
        public InstType_TecController.ControlMode BtmTec_ControlMode;
        /// <summary> Bottom TEC Integral gain </summary>
        public double BtmTec_IntegralGain;
        /// <summary> Bottom TEC Proportional gain </summary>
        public double BtmTec_ProportionalGain;
        /// <summary> Bottom TEC Max current </summary>
        public double BtmTec_MaxI;
        /// <summary> Bottom TEC Steinhart-Hart Constant A </summary>
        public double BtmTecSteinHartData_A;
        /// <summary> Bottom TEC Steinhart-Hart Constant B </summary>
        public double BtmTecSteinHartData_B;
        /// <summary> Bottom TEC Steinhart-Hart Constant C </summary>
        public double BtmTecSteinHartData_C;
    }
}

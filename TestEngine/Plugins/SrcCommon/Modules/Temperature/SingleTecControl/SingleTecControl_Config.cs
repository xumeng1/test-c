using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.Framework.InternalData;

namespace Bookham.TestLibrary.TestModules
{
    /// <summary>
    /// This structure encapsulates all the data requirements of the test module
    /// At test program level the user declares an instance of the structure, but now a new instance, 
    /// ie mystruct myInstance;
    /// then they can make populate the members of the struct
    /// ie
    /// myInstance.param1 = 20;
    /// myInstance.param2 = 20;
    /// 
    /// Only when the structure is fully populated can they call
    /// DatumList myD = myInstance.GetDatumList();
    /// 
    /// Else they will get a compile time error, as the object myInstance
    /// only exists at the point where all members are populated!
    /// 
    /// In the test module level the user simply calls
    /// mystruct myInstance = new mystruct(configData);
    /// and they can read the myInstance.param1, myInstance.param2 = 20 values;
    /// </summary>
    public struct SingleTecControl_Config
    {
        /// <summary>
        /// Method populates a datumlist based on this structure data
        /// </summary>
        public DatumList GetDatumList()
        {
            DatumList myLocalDatumList = new DatumList();
            
            DatumSint32 TempTimeBtwReadings_ms = new DatumSint32("TempTimeBtwReadings_ms", this.TempTimeBtwReadings_ms);
            DatumDouble MaxExpectedTimeForOperation_s = new DatumDouble("MaxExpectedTimeForOperation_s", this.MaxExpectedTimeForOperation_s);
            DatumDouble TopTec_RqdTimeInToleranceWindow_s = new DatumDouble("TopTec_RqdTimeInToleranceWindow_s", this.TopTec_RqdTimeInToleranceWindow_s);
            DatumDouble TopTec_RqdStabilisationTime_s = new DatumDouble("TopTec_RqdStabilisationTime_s", this.TopTec_RqdStabilisationTime_s);
            DatumDouble TopTec_SetPointTemperature_C = new DatumDouble("TopTec_SetPointTemperature_C", this.TopTec_SetPointTemperature_C);
            DatumDouble TopTec_TemperatureTolerance_C = new DatumDouble("TopTec_TemperatureTolerance_C", this.TopTec_TemperatureTolerance_C);
            
            myLocalDatumList.Add(TempTimeBtwReadings_ms);
            myLocalDatumList.Add(MaxExpectedTimeForOperation_s);
            myLocalDatumList.Add(TopTec_RqdTimeInToleranceWindow_s);
            myLocalDatumList.Add(TopTec_RqdStabilisationTime_s);
            myLocalDatumList.Add(TopTec_TemperatureTolerance_C);
            myLocalDatumList.Add(TopTec_SetPointTemperature_C);

            return (myLocalDatumList);
        }

        /// <summary>
        /// Method being passed a datumlist and populates a SingleTecControlType
        /// </summary>
        /// <param name="mydata"></param>
        public SingleTecControl_Config(DatumList mydata)
        {
            this.TempTimeBtwReadings_ms = mydata.ReadSint32("TempTimeBtwReadings_ms");
            this.MaxExpectedTimeForOperation_s = mydata.ReadDouble("datumMaxExpectedTimeForOperation_s");
            this.TopTec_RqdTimeInToleranceWindow_s = mydata.ReadDouble("TopTec_RqdTimeInToleranceWindow_s");
            this.TopTec_RqdStabilisationTime_s = mydata.ReadDouble("TopTec_RqdStabilisationTime_s");
            this.TopTec_TemperatureTolerance_C = mydata.ReadDouble("TopTec_TemperatureTolerance_C");
            this.TopTec_SetPointTemperature_C = mydata.ReadDouble("TopTec_SetPointTemperature_C");
        }
       
        //List of all parameters
        public Int32 TempTimeBtwReadings_ms;
        public double MaxExpectedTimeForOperation_s;
        public double TopTec_RqdTimeInToleranceWindow_s;
        public double TopTec_RqdStabilisationTime_s;
        public double TopTec_SetPointTemperature_C;
        public double TopTec_TemperatureTolerance_C;
    }
}

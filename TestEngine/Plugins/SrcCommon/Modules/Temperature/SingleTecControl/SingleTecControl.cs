using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;


namespace Bookham.TestLibrary.TestModules
{
    /// <summary>
    /// Generic Single TEC Control Test Module 
    /// </summary>
    public class SingleTecControl : ITestModule
    {
        /// <summary>
        /// This does the test for us, this class is assuming a single temperature controller
        /// </summary>
        /// <param name="engine">The itest engine engine</param>
        /// <param name="userType">operator engineer or technician</param>
        /// <param name="configData">a datum list of all required config data consisting of:
        /// DatumDouble("MaxExpectedTimeForOperation_s")
        /// DatumDouble("RqdTimeInToleranceWindow_s")
        /// DatumDouble("RqdStabilisationTime_s")
        /// DatumSint32("TempTimeBtwReadings_ms")
        /// DatumDouble("SetPointTemperature_C")
        /// DatumDouble("TemperatureTolerance_C");
        /// </param>
        /// <param name="instruments">Should only contain a single tec controller unit instruments["TecController1"]</param>
        /// <param name="chassis">the chassis</param>
        /// <param name="calData">any cal data (none reqd)</param>
        /// <param name="previousTestData">any previous data (none reqd)</param>
        /// <returns></returns>
        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, DatumList configData, InstrumentCollection instruments, ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {
            TecController tecClass = new TecController(engine);

            SingleTecControl_Config mystruct = new SingleTecControl_Config(configData);
                        
            // Interrogate the config data and get a list of all the values we need
            // set up all the values in the base class ready for our use
            tecClass.TimeoutTimerInterval_s = mystruct.MaxExpectedTimeForOperation_s;
            tecClass.WithinToleranceWindowTimer_s = mystruct.TopTec_RqdTimeInToleranceWindow_s;
            tecClass.StabilisationTimer_s = mystruct.TopTec_RqdStabilisationTime_s;
            tecClass.delayTimeBetweenReadings_ms = mystruct.TempTimeBtwReadings_ms;
            tecClass.targetReading1_C = mystruct.TopTec_SetPointTemperature_C;
            tecClass.targetTolerance_C = mystruct.TopTec_TemperatureTolerance_C;

            tecClass.tecController1 = (InstType_TecController)instruments["TecController1"];

            tecClass.TimeoutTimerStarted = true;
            tecClass.tecController1.SensorTemperatureSetPoint_C = mystruct.TopTec_SetPointTemperature_C;
            tecClass.tecController1.OutputEnabled = true;

            tecClass.TecControlRoutine();
            // stop the TimeoutException timer
            tecClass.TimeoutTimerStarted = false;

            // we are completed successfully.

            return (tecClass.moduleResults);
        }


        /// <summary>
        /// The user control we are using for display
        /// </summary>
        public Type UserControl
        {
            get { return (typeof(ModuleGui)); }
        }

    }
}

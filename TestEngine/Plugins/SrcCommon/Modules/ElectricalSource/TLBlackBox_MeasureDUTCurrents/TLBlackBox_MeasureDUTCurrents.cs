// [Copyright]
//
// Bookham Test Engine Library
// 
//
// TLBlackBox_MeasureDUTCurrents.cs
// Design: PCBA Cal Test Modules DD 
// Author: Keith Pillar


using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;

namespace Bookham.TestLibrary.TestModules
{
    /// <summary>
    /// This test module will measure and return 2 currents, from psu 1 and psu 2.
    /// </summary>
    public class TLBlackBox_MeasureDUTCurrents : ITestModule
    {
        #region ITestModule Members

        /// <summary>
        /// Our test for the dut currents
        /// </summary>
        /// <param name="engine">our test engine</param>
        /// <param name="userType">users</param>
        /// <param name="configData">none</param>
        /// <param name="instruments">InstType_ElectricalSource PowerSupply1, PowerSupply2</param>
        /// <param name="chassis">none</param>
        /// <param name="calData">none</param>
        /// <param name="previousTestData">none</param>
        /// <returns>DatumDouble CurrentReading1_mA, CurrentReading2_mA</returns>
        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, DatumList configData, InstrumentCollection instruments, ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {
            DatumList myTestModuleData = new DatumList();

            InstType_ElectricalSource myPSU1 = (InstType_ElectricalSource)instruments["PowerSupply1"];
            InstType_ElectricalSource myPSU2 = (InstType_ElectricalSource)instruments["PowerSupply2"];

            bool retry = true;
            int numRetries = 4;

            double CurrentReading1_mA = 0;
            double CurrentReading2_mA = 0;

            double CurrentVoltage1_V = 0;
            double CurrentVoltage2_V = 0;

            do
            {
                try
                {
                    CurrentReading1_mA = myPSU1.CurrentActual_amp * 1000;
                    CurrentReading2_mA = myPSU2.CurrentActual_amp * 1000;

                    CurrentVoltage1_V = myPSU1.VoltageActual_Volt;
                    CurrentVoltage2_V = myPSU2.VoltageActual_Volt;

                    retry = false;
                }
                catch (InstrumentException e)
                {
                    numRetries--;
                    if (numRetries == 0)
                    {
                        engine.ErrorInModule("Failed to read data from instruments after 4 attempts: " + e.Message);
                    }
                }
            } while (retry);
            
            myTestModuleData.AddDouble("CurrentReading1_mA", CurrentReading1_mA);
            myTestModuleData.AddDouble("CurrentReading2_mA", CurrentReading2_mA);
            myTestModuleData.AddDouble("CurrentVoltage1_V", CurrentVoltage1_V);
            myTestModuleData.AddDouble("CurrentVoltage2_V", CurrentVoltage2_V);


            return (myTestModuleData);            
        }

        /// <summary>
        /// Return a reference to the User control for this module.
        /// This module does not use a GUI.
        /// </summary>
        public Type UserControl
        {
            get { return (typeof(ModuleGui)); }
        }

        #endregion
    }
}

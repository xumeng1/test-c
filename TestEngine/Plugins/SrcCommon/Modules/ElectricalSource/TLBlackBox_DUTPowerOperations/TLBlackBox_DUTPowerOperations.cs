// [Copyright]
//
// Bookham Test Engine Library
// 
//
// TLBlackBox_DUTPowerOperations.cs
// Design: PCBA Cal Test Modules DD 
// Author: Keith Pillar

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;

namespace Bookham.TestLibrary.TestModules
{
    /// <summary>
    /// This generic test module handles the power DUT sequences.
    /// </summary>
    public class TLBlackBox_DUTPowerOperations : ITestModule
    {
        #region ITestModule Members

        /// <summary>
        /// This test module will handle the power DUT sequences for us
        /// </summary>
        /// <param name="engine">our test engine</param>
        /// <param name="userType">tech or otherwise</param>
        /// <param name="configData">refer to TLBlackBox_DUTPowerOperations_Config</param>
        /// <param name="instruments">2 x InstType_ElectricalSource, PowerSupply1 & PowerSupply2</param>
        /// <param name="chassis">psu chassis</param>
        /// <param name="calData">none</param>
        /// <param name="previousTestData">none</param>
        /// <returns>no return data</returns>
        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, DatumList configData, InstrumentCollection instruments, ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {
            DatumList myTestModuleData = new DatumList();

            TLBlackBox_DUTPowerOperations_Config myConfigData = new TLBlackBox_DUTPowerOperations_Config(configData);
            
            InstType_ElectricalSource myPSU1 = (InstType_ElectricalSource)instruments["PowerSupply1"];           
            InstType_ElectricalSource myPSU2 = (InstType_ElectricalSource) instruments["PowerSupply2"];

            //Ok if we try and confirm the setting order as being ok, we may run into problems
            //as this is saying we know what the equipment will do underneath us
            //As we are trying to be generic, that is not a good thing to do.
            //And so, i keep it very simple!
            myPSU1.VoltageSetPoint_Volt = myConfigData.Supply1_V;
            myPSU2.VoltageSetPoint_Volt = myConfigData.Supply2_V;

            myPSU1.CurrentSetPoint_amp = myConfigData.Supply1_I;
            myPSU2.CurrentSetPoint_amp = myConfigData.Supply2_I;

            myPSU1.OutputEnabled = myConfigData.PoweredUp;
            myPSU2.OutputEnabled = myConfigData.PoweredUp;

            return (myTestModuleData);
        }

        /// <summary>
        /// Return a reference to the module's GUI.
        /// This module use a GUI.
        /// </summary>
        public Type UserControl
        {
            get { return (typeof(ModuleGui)); }
        }

        #endregion
    }
}

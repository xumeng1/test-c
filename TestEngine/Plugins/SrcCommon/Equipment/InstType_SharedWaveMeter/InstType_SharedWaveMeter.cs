//  Author: chongjian.liang 2016.04.21

using System;
using System.IO;
using System.Threading;
using System.Collections.Generic;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using NationalInstruments.VisaNS;

namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// WaveMeter with OpticalSwitch
    /// </summary>
    public abstract class InstType_SharedWaveMeter : InstType_Wavemeter
    {
        public InstType_SharedWaveMeter(string instrumentName, string driverName, string slotId, string subSlotId, Chassis chassis)
            : base(instrumentName, driverName, slotId, subSlotId, chassis)
        {
        }

        /// <summary>
        /// Initialize the shared instrument of slave line, lock schema is MemoryLock
        /// </summary>
        public void TurnOnShare_Slave()
        {
            this.OpticalSwitch = new Inst_Ke2510_OpticalSwitch();
        }

        /// <summary>
        /// Initialize the shared instrument of slave line, lock schema is FileLock
        /// </summary>
        public void TurnOnShare_Slave(string lockFile_ID)
        {
            this.OpticalSwitch = new Inst_Ke2510_OpticalSwitch(lockFile_ID);
        }

        /// <summary>
        /// Initialize the shared instrument of master line, lock schema is MemoryLock
        /// </summary>
        public void TurnOnShare_Master(Inst_Ke2510 inst_Ke2510, Enum_LineState lineState_MasterOn, Enum_LineState lineState_SlaveOn, int switchSpeed_ms)
        {
            this.OpticalSwitch = new Inst_Ke2510_OpticalSwitch(inst_Ke2510, lineState_MasterOn, lineState_SlaveOn, switchSpeed_ms);

            this.OpticalSwitch.IsOnline = true;
        }

        /// <summary>
        /// Initialize the shared instrument of master line, lock schema is FileLock
        /// </summary>
        public void TurnOnShare_Master(string lockFile_ID, Inst_Ke2510 inst_Ke2510, Enum_LineState lineState_MasterOn, Enum_LineState lineState_SlaveOn, int switchSpeed_ms)
        {
            this.OpticalSwitch = new Inst_Ke2510_OpticalSwitch(lockFile_ID, inst_Ke2510, lineState_MasterOn, lineState_SlaveOn, switchSpeed_ms);

            this.OpticalSwitch.IsOnline = true;
        }

        public void TurnOffShare()
        {
            if (this.OpticalSwitch != null
                && this.OpticalSwitch.OPTICAL_SWITCH_LINE == Enum_OpticalSwitchLine.Master)
            {
                this.OpticalSwitch.IsOnline = false;
            }
        }

        protected void SwitchOpticalLineOn()
        {
            if (this.OpticalSwitch != null)
            {
                this.OpticalSwitch.SwitchOpticalLineOn();
            }
        }

        protected void SwitchOpticalLineOff()
        {
            if (this.OpticalSwitch != null)
            {
                this.OpticalSwitch.SwitchOpticalLineOff();
            }
        }

        public abstract bool ContinueMeasMode { get; set;}

        private Inst_Ke2510_OpticalSwitch OpticalSwitch;
    }
}

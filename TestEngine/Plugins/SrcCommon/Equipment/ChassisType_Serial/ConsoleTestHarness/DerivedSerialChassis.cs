//==========================================================================================================
// Copyright:			Copyright � 2006 Bookham Inc. All Rights Reserved. This document is company 
//						confidential and should not be copied, reproduced or otherwise utilised outside of 
//						Bookham Inc without the prior written consent of the company.
//
// Project:				Bookham Test-Engine
// Module:				ConsoleTestHarness
// 
// FileName:			DerivedSerialChassis.cs
//
// Author:				ian.webb
// Design Document:		ADVS_SoftwareArchitectureDesignSpec.doc
// Revision History:	10Jan2007 ian.webb: Initial version.
//
// Description:			A serial communications chassis implementation derived from ChassisType_Serial to
//                      enable testing of the functionality of the chassis type.
//==========================================================================================================

using System;
using System.Collections.Generic;
using System.Text;

using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.Framework.Logging;
using Bookham.TestLibrary.ChassisTypes;

namespace ConsoleTestHarness
{
    /// <summary>
    /// Serial communications chassis class, derived from ChassisType_Serial for test purposes!
    /// </summary>
    public class DerivedSerialChassis : ChassisType_Serial
    {
        /// <summary>
        /// Creates a new instance of the DerivedSerialChassis class.
        /// </summary>
        /// <param name="chassisName">decorative name for the chassis.</param>
        /// <param name="driverName">name of the driver class.</param>
        /// <param name="portName">port reference for the serial session (e.g. COM1.)</param>
        public DerivedSerialChassis(string chassisName, string driverName, string portName)
            : base(chassisName, driverName, portName)
        {
        }
    }
}

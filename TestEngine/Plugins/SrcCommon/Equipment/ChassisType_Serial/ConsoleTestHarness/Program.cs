//==========================================================================================================
// Copyright:			Copyright � 2006 Bookham Inc. All Rights Reserved. This document is company 
//						confidential and should not be copied, reproduced or otherwise utilised outside of 
//						Bookham Inc without the prior written consent of the company.
//
// Project:				Bookham Test-Engine
// Module:				ConsoleTestHarness
// 
// FileName:			Program.cs
//
// Author:				ian.webb
// Design Document:		ADVS_SoftwareArchitectureDesignSpec.doc
// Revision History:	10Jan2007 ian.webb: Initial version.
//
// Description:			Program entry-point.
//
//                      NOTES:  Test-harness developed using the NUnit framework (v2.2.4.0)
//                              TestDriven.NET (v2.0 1438) was used for test execution.
//==========================================================================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleTestHarness
{
    class Program
    {
        static void Main(string[] args)
        {
            //link to the test-fixture
            Tests testFixture = new Tests();

            //initialise the test-fixture ready to run the tests
            testFixture.StartUp();

            //run the tests
            try
            {
                testFixture.Test01_BasicComms();
                testFixture.Test02_BaudRatesTest();
                testFixture.Test03_NewLineCharacter();
                testFixture.Test04_ParitySettingsTest();
                testFixture.Test05_DataBitsTest();
                testFixture.Test06_StopBitsTest();
                testFixture.Test07_HandshakingTest();
            }
            finally
            {
                //shut-down the test-fixture
                testFixture.ShutDown();
            }
        }
    }
}

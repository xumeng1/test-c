//==========================================================================================================
// Copyright:			Copyright � 2006 Bookham Inc. All Rights Reserved. This document is company 
//						confidential and should not be copied, reproduced or otherwise utilised outside of 
//						Bookham Inc without the prior written consent of the company.
//
// Project:				Bookham Test-Engine
// Module:				ConsoleTestHarness
// 
// FileName:			Tests.cs
//
// Author:				ian.webb
// Design Document:		ADVS_SoftwareArchitectureDesignSpec.doc
// Revision History:	10Jan2007 ian.webb: Initial version.
//
// Description:			Test fixture for running tests on the ChassisType_Serial assembly.
//==========================================================================================================

using System;
using System.Collections.Generic;
using System.Text;

using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.Framework.Logging;
using Bookham.TestLibrary.ChassisTypes;

using NUnit.Framework;

namespace ConsoleTestHarness
{
    /// <summary>
    /// Defines the tests to be run on the ChassisType_Serial class.
    /// </summary>
    [TestFixture]
    public class Tests
    {
        #region Private Variables

        /// <summary>
        /// serial-chassis controller 1.
        /// </summary>
        private ChassisType_Serial port1 = null;

        /// <summary>
        /// serial-chassis controller 2.
        /// </summary>
        private ChassisType_Serial port2 = null;

        /// <summary>
        /// flags whether the test-fixture initialised as expected.
        /// </summary>
        private bool initOK = false;

        #endregion

        #region TestFixture Setup and TearDown

        /// <summary>
        /// Initialises the test-harness ready for testing.
        /// </summary>
        [TestFixtureSetUp]
        public void StartUp()
        {
            //use the Bookham.TestEngine logging features and re-direct debug output to the console
            Initializer.Init();
            BugOut.SetOutputToConsole(true);

            this.initOK = true;

            BugOut.WriteLine(BugOut.WildcardLabel, "*==============================================================*");
            BugOut.WriteLine(BugOut.WildcardLabel, "*--> TestHarness: Initialising...");
            BugOut.WriteLine(BugOut.WildcardLabel, "INI");
            BugOut.WriteLine(BugOut.WildcardLabel, "INI  NOTE: tests are undertaken using two RS232 ports on the");
            BugOut.WriteLine(BugOut.WildcardLabel, "INI  PC (which should be COM1 and COM2) and a direct connection");
            BugOut.WriteLine(BugOut.WildcardLabel, "INI  between them using a null-modem serial cable.");
            BugOut.WriteLine(BugOut.WildcardLabel, "INI");

            BugOut.WriteLine(BugOut.WildcardLabel, "INI  Creating the ChassisType_Serial control objects...");
            try
            {
                this.port1 = (ChassisType_Serial)new DerivedSerialChassis("SerialChassis1", "DerivedSerialChassis", "COM1");
                this.port2 = (ChassisType_Serial)new DerivedSerialChassis("SerialChassis2", "DerivedSerialChassis", "COM2");
            }
            catch (Exception ex)
            {
                this.initOK = false;
                BugOut.WriteLine(BugOut.WildcardLabel, "INI  EXCEPTION: " + ex.Message);
            }
            BugOut.WriteLine(BugOut.WildcardLabel, "INI  Controllers created.");

            BugOut.WriteLine(BugOut.WildcardLabel, "INI  Updating port settings: 9600Baud, 8D, 1S, No P, No HS, 1K I/P, 1K O/P...");
            try
            {
                this.port1.Configure(9600, 8, System.IO.Ports.StopBits.One, System.IO.Ports.Parity.None, System.IO.Ports.Handshake.None, 1024, 1024);
                this.port2.Configure(9600, 8, System.IO.Ports.StopBits.One, System.IO.Ports.Parity.None, System.IO.Ports.Handshake.None, 1024, 1024);
            }
            catch (Exception ex)
            {
                this.initOK = false;
                BugOut.WriteLine(BugOut.WildcardLabel, "INI  EXCEPTION: " + ex.Message);
            }
            BugOut.WriteLine(BugOut.WildcardLabel, "INI  Port settings updated.");

            BugOut.WriteLine(BugOut.WildcardLabel, "INI  Setting timeout to 5s...");
            try
            {
                this.port1.Timeout_ms = 5000;
                this.port2.Timeout_ms = 5000;
            }
            catch (Exception ex)
            {
                this.initOK = false;
                BugOut.WriteLine(BugOut.WildcardLabel, "INI  EXCEPTION: " + ex.Message);
            }
            BugOut.WriteLine(BugOut.WildcardLabel, "INI  Timeouts updated.");

            BugOut.WriteLine(BugOut.WildcardLabel, "INI  Putting controllers online...");
            try
            {
                this.port1.IsOnline = true;
                this.port2.IsOnline = true;
            }
            catch (Exception ex)
            {
                BugOut.WriteLine(BugOut.WildcardLabel, "INI  EXCEPTION: " + ex.Message);
            }
            BugOut.WriteLine(BugOut.WildcardLabel, "INI  Controllers online.");

            BugOut.WriteLine(BugOut.WildcardLabel, "INI");
            BugOut.WriteLine(BugOut.WildcardLabel, "*--> TestHarness initialised!");
            BugOut.WriteLine(BugOut.WildcardLabel, "*--------------------------------------------------------------*");
        }

        /// <summary>
        /// Finalises the test-harness after testing completes.
        /// </summary>
        [TestFixtureTearDown]
        public void ShutDown()
        {

            BugOut.WriteLine(BugOut.WildcardLabel, "*--------------------------------------------------------------*");
            BugOut.WriteLine(BugOut.WildcardLabel, "*--> TestHarness: Shutting down...");
            BugOut.WriteLine(BugOut.WildcardLabel, "SDN");

            BugOut.WriteLine(BugOut.WildcardLabel, "SDN  Closing controller ports...");
            try
            {
                this.port1.IsOnline = false;
                this.port2.IsOnline = false;
            }
            catch (Exception ex)
            {
                BugOut.WriteLine(BugOut.WildcardLabel, "SDN  EXCEPTION: " + ex.Message);
            }
            BugOut.WriteLine(BugOut.WildcardLabel, "SDN  Ports closed.");

            BugOut.WriteLine(BugOut.WildcardLabel, "SDN  Cleaning controllers...");
            try
            {
                this.port1 = null;
                this.port2 = null;
            }
            catch (Exception ex)
            {
                BugOut.WriteLine(BugOut.WildcardLabel, "SDN  EXCEPTION: " + ex.Message);
            }
            BugOut.WriteLine(BugOut.WildcardLabel, "SDN  Controllers disposed.");

            if (!this.initOK)
            {
                BugOut.WriteLine(BugOut.WildcardLabel, "SDN");
                BugOut.WriteLine(BugOut.WildcardLabel, "!--> Test-Fixture initialisation failed (this could have affected testing)");
            }
            BugOut.WriteLine(BugOut.WildcardLabel, "SDN");
            BugOut.WriteLine(BugOut.WildcardLabel, "*--> TestHarness shut-down.");
            BugOut.WriteLine(BugOut.WildcardLabel, "*==============================================================*");

            this.initOK = false;    //reset ready for next test run
        }

        #endregion

        #region Tests

        /// <summary>
        /// Performs a basic communications check (sending and receiving data from each of the two serial ports.)
        /// </summary>
        [Test]
        public void Test01_BasicComms()
        {
            BugOut.WriteLine(BugOut.WildcardLabel, "*--------------------------------------------------------------*");
            BugOut.WriteLine(BugOut.WildcardLabel, "*--> Test01: Basic Communications Test...");
            BugOut.WriteLine(BugOut.WildcardLabel, "T01");
            BugOut.WriteLine(BugOut.WildcardLabel, "T01  DESCRIPTION: Performs a basic communications test for");
            BugOut.WriteLine(BugOut.WildcardLabel, "T01  each of the two serial controller objects (both read and");
            BugOut.WriteLine(BugOut.WildcardLabel, "T01  write is tested for each controller port.)");
            BugOut.WriteLine(BugOut.WildcardLabel, "T01");
            BugOut.WriteLine(BugOut.WildcardLabel, "T01  USES:    ChassisType_Serial.Read(),");
            BugOut.WriteLine(BugOut.WildcardLabel, "T01           ChassisType_Serial.Write(string).");
            BugOut.WriteLine(BugOut.WildcardLabel, "T01");

            if (!this.port1.IsOnline || !this.port1.IsOnline)
            {
                BugOut.WriteLine(BugOut.WildcardLabel, "T01  EXCEPTION: the serial ports are not online.");
            }
            else
            {
                try
                {
                    BugOut.WriteLine(BugOut.WildcardLabel, "T01  Writing 'abcd1234' to COM1...");
                    this.port1.Write("abcd1234", null);
                    BugOut.WriteLine(BugOut.WildcardLabel, "T01  Data written.");

                    BugOut.WriteLine(BugOut.WildcardLabel, "T01  Checking readback at COM2...");
                    this.Delay(0.25);
                    string reading = this.port2.Read(null);
                    Assert.AreEqual("abcd1234", reading);
                    if (reading == "abcd1234") BugOut.WriteLine(BugOut.WildcardLabel, "T01  Data received as transmitted.");

                    BugOut.WriteLine(BugOut.WildcardLabel, "T01  Writing 'wxyz7890' to COM2...");
                    this.port2.Write("wxyz7890", null);
                    BugOut.WriteLine(BugOut.WildcardLabel, "T01  Data written.");

                    BugOut.WriteLine(BugOut.WildcardLabel, "T01  Reading data at COM1 (expect 'wxyz7890')...");
                    this.Delay(0.25);
                    reading = this.port1.Read(null);
                    Assert.AreEqual("wxyz7890", reading);
                    if (reading == "wxyz7890") BugOut.WriteLine(BugOut.WildcardLabel, "T01  Data received as transmitted.");
                }
                catch (AssertionException) { throw; }
                catch (Exception ex)
                {
                    BugOut.WriteLine(BugOut.WildcardLabel, "T01  EXCEPTION: " + ex.Message);
                    Assert.Fail();
                }
            }

            BugOut.WriteLine(BugOut.WildcardLabel, "T01");
            BugOut.WriteLine(BugOut.WildcardLabel, "*--> Test01: finished.");
            BugOut.WriteLine(BugOut.WildcardLabel, "*--------------------------------------------------------------*");
        }

        /// <summary>
        /// Checks communications at each of the following common transmission baud-rates: 1200, 2400, 4800, 9600, 19200, 38400 and 115200. 
        /// Performs a mismatch first (two different baud-rates set) and checks data is corrupted, then sets identical baud-rates and 
        /// confirms data transmission integrity.
        /// </summary>
        [Test]
        public void Test02_BaudRatesTest()
        {
            BugOut.WriteLine(BugOut.WildcardLabel, "*--------------------------------------------------------------*");
            BugOut.WriteLine(BugOut.WildcardLabel, "*--> Test02: Baud-rates Test...");
            BugOut.WriteLine(BugOut.WildcardLabel, "T02");
            BugOut.WriteLine(BugOut.WildcardLabel, "T02  DESCRIPTION: Checks standard baud-rates of 1200, 2400, 4800,");
            BugOut.WriteLine(BugOut.WildcardLabel, "T02  9600, 19200, 38400 and 115200. This is done by setting the ");
            BugOut.WriteLine(BugOut.WildcardLabel, "T02  baud-rates of the two serial ports differently, and confirming");
            BugOut.WriteLine(BugOut.WildcardLabel, "T02  data corruption, and then setting the baud-rates to be the ");
            BugOut.WriteLine(BugOut.WildcardLabel, "T02  same and confirming data integrity.");
            BugOut.WriteLine(BugOut.WildcardLabel, "T02");
            BugOut.WriteLine(BugOut.WildcardLabel, "T02  USES:    ChassisType_Serial.Configure(),");
            BugOut.WriteLine(BugOut.WildcardLabel, "T02           ChassisType_Serial.IsOnline (inherited from Chassis),");
            BugOut.WriteLine(BugOut.WildcardLabel, "T02           ChassisType_Serial.Read(),");
            BugOut.WriteLine(BugOut.WildcardLabel, "T02           ChassisType_Serial.Write(string).");
            BugOut.WriteLine(BugOut.WildcardLabel, "T02");

            int port1_InitialBaud = this.port1.BaudRate;
            int port2_InitialBaud = this.port2.BaudRate;

            try
            {
                for (int i = 0; i < 7; i++)
                {
                    int baudRate = 0;
                    int misMatchBaud = 300;

                    switch (i)
                    {
                        case 0: baudRate = 1200; break;
                        case 1: baudRate = 2400; break;
                        case 2: baudRate = 4800; break;
                        case 3: baudRate = 9600; break;
                        case 4: baudRate = 19200; break;
                        case 5: baudRate = 38400; break;
                        case 6: baudRate = 115200; break;
                    }

                    BugOut.WriteLine(BugOut.WildcardLabel, string.Format("T02  >>> CHECKING BAUD {0}", baudRate));
                    BugOut.WriteLine(BugOut.WildcardLabel, string.Format("T02  Setting COM1 baud to {0}B, and COM2 baud to {1}B...",
                        baudRate, misMatchBaud));
                    this.port1.IsOnline = false;
                    this.port1.Configure(baudRate, this.port1.DataBits, this.port1.StopBits, this.port1.Parity, this.port1.Handshaking,
                        this.port1.InputBufferSize_bytes, this.port1.OutputBufferSize_bytes);
                    this.port1.IsOnline = true;
                    this.port2.IsOnline = false;
                    this.port2.Configure(misMatchBaud, this.port2.DataBits, this.port2.StopBits, this.port2.Parity, this.port2.Handshaking,
                        this.port2.InputBufferSize_bytes, this.port2.OutputBufferSize_bytes);
                    this.port2.IsOnline = true;
                    BugOut.WriteLine(BugOut.WildcardLabel, "T02  Baud-rates updated.");

                    BugOut.WriteLine(BugOut.WildcardLabel, "T02  Writing 'abcd1234' to COM1...");
                    this.port1.Write("abcd1234", null);
                    BugOut.WriteLine(BugOut.WildcardLabel, "T02  Data written.");
                    BugOut.WriteLine(BugOut.WildcardLabel, "T02  Reading back data from COM2...");
                    this.Delay(0.25);
                    string reading = this.port2.Read(null);
                    Assert.AreNotEqual(reading, "abcd1234");
                    if (reading == "abcd1234") BugOut.WriteLine(BugOut.WildcardLabel, "T02  Data received NOT as transmitted (expected.)");

                    BugOut.WriteLine(BugOut.WildcardLabel, string.Format("T02  Setting COM2 baud to {0}B...", baudRate));
                    this.port2.IsOnline = false;
                    this.port2.Configure(baudRate, this.port2.DataBits, this.port2.StopBits, this.port2.Parity, this.port2.Handshaking,
                        this.port2.InputBufferSize_bytes, this.port2.OutputBufferSize_bytes);
                    this.port2.IsOnline = true;
                    BugOut.WriteLine(BugOut.WildcardLabel, "T02  baud-rate updated.");

                    BugOut.WriteLine(BugOut.WildcardLabel, "T02  Writing 'wxyz0987' to COM1...");
                    this.port1.Write("wxyz0987", null);
                    BugOut.WriteLine(BugOut.WildcardLabel, "T02  Data written.");
                    BugOut.WriteLine(BugOut.WildcardLabel, "T02  Reading back data from COM2...");
                    this.Delay(0.25);
                    reading = this.port2.Read(null);
                    Assert.AreEqual(reading, "wxyz0987");
                    if (reading == "wxyz0987") BugOut.WriteLine(BugOut.WildcardLabel, "T02  Data received as transmitted.");
                }
            }
            catch (AssertionException) { throw; }
            catch (Exception ex)
            {
                BugOut.WriteLine(BugOut.WildcardLabel, "T02  EXCEPTION: " + ex.Message);
                Assert.Fail();
            }            

            //reset the baud-rate for the controllers to original setting
            try
            {
                BugOut.WriteLine(BugOut.WildcardLabel, "T02  Restoring baud-rates to original setting for both controllers...");
                this.port1.IsOnline = false;
                this.port1.Configure(port1_InitialBaud, this.port1.DataBits, this.port1.StopBits, this.port1.Parity,
                    this.port1.Handshaking, this.port1.InputBufferSize_bytes, this.port1.OutputBufferSize_bytes);
                this.port1.IsOnline = true;
                this.port2.IsOnline = false;
                this.port2.Configure(port2_InitialBaud, this.port2.DataBits, this.port2.StopBits, this.port2.Parity,
                    this.port2.Handshaking, this.port2.InputBufferSize_bytes, this.port2.OutputBufferSize_bytes);
                this.port2.IsOnline = true;
                BugOut.WriteLine(BugOut.WildcardLabel, "T02  Baud-rates restored.");
            }
            catch (Exception ex)
            {
                BugOut.WriteLine(BugOut.WildcardLabel, "T02  EXCEPTION: " + ex.Message);
                Assert.Fail();
            }

            BugOut.WriteLine(BugOut.WildcardLabel, "T02");
            BugOut.WriteLine(BugOut.WildcardLabel, "*--> Test02: finished.");
            BugOut.WriteLine(BugOut.WildcardLabel, "*--------------------------------------------------------------*");
        }

        /// <summary>
        /// Checks the operation of the 'NewLine' (data termination string) property, by changing the property on one serial port,
        /// confirming that a timeout occurs when reading data sent with the old value, then updating the property on the other port
        /// and confirming data is read as expected.
        /// </summary>
        [Test]
        public void Test03_NewLineCharacter()
        {
            BugOut.WriteLine(BugOut.WildcardLabel, "*--------------------------------------------------------------*");
            BugOut.WriteLine(BugOut.WildcardLabel, "*--> Test03: Custom NewLine Character Test...");
            BugOut.WriteLine(BugOut.WildcardLabel, "T03");
            BugOut.WriteLine(BugOut.WildcardLabel, "T03  DESCRIPTION: Changes the NewLine property of one of the");
            BugOut.WriteLine(BugOut.WildcardLabel, "T03  serial controllers and checks a timeout occurs when");
            BugOut.WriteLine(BugOut.WildcardLabel, "T03  performing a read on the other controller. Then updates");
            BugOut.WriteLine(BugOut.WildcardLabel, "T03  the second controller with the new NewLine value and ");
            BugOut.WriteLine(BugOut.WildcardLabel, "T03  confirms that the data is read correctly again.");
            BugOut.WriteLine(BugOut.WildcardLabel, "T03");
            BugOut.WriteLine(BugOut.WildcardLabel, "T03  USES:    ChassisType_Serial.IsOnline (inherited from Chassis),");
            BugOut.WriteLine(BugOut.WildcardLabel, "T03           ChassisType_Serial.NewLine,");
            BugOut.WriteLine(BugOut.WildcardLabel, "T03           ChassisType_Serial.ReadLine(),");
            BugOut.WriteLine(BugOut.WildcardLabel, "T03           ChassisType_Serial.WriteLine(string).");
            BugOut.WriteLine(BugOut.WildcardLabel, "T03");

            string port1_NewLine = this.port1.NewLine;
            string port2_NewLine = this.port2.NewLine;

            try
            {
                BugOut.WriteLine(BugOut.WildcardLabel, "T03  Changing the NewLine property of controller-1 from <LF> to <CR>...");
                this.port1.IsOnline = false;    //(just in case)
                this.port1.NewLine = "\r";
                this.port1.IsOnline = true;
                BugOut.WriteLine(BugOut.WildcardLabel, "T03  NewLine property updated.");

                BugOut.WriteLine(BugOut.WildcardLabel, "T03  Writing 'abcd1234' to COM1...");
                this.port1.WriteLine("abcd1234", null);
                BugOut.WriteLine(BugOut.WildcardLabel, "T03  Data written.");

                string reading = "";
                try
                {
                    BugOut.WriteLine(BugOut.WildcardLabel, "T03  Reading data from COM2...");
                    reading = this.port2.ReadLine(null);

                    Assert.Fail();  //expecting a timeout exception so test is a failure if none is detected!
                }
                catch (AssertionException) { throw; }
                catch (Exception ex)
                {
                    if (ex is TimeoutException) BugOut.WriteLine(BugOut.WildcardLabel, "T03  Read timed out as expected.");
                    else
                    {
                        BugOut.WriteLine(BugOut.WildcardLabel, "!--> FAIL: Expected a read timeout");
                        BugOut.WriteLine(BugOut.WildcardLabel, "T03  EXCEPTION: " + ex.Message);
                        Assert.Fail();
                    }
                }
            }
            catch (AssertionException) { throw; }
            catch (Exception ex)
            {
                BugOut.WriteLine(BugOut.WildcardLabel, "T03  EXCEPTION: " + ex.Message);
                Assert.Fail();
            }

            try
            {
                BugOut.WriteLine(BugOut.WildcardLabel, "T03  Purging COM2 receive buffer...");
                this.Delay(0.25);
                this.port2.Read(null);
                BugOut.WriteLine(BugOut.WildcardLabel, "T03  Purge complete.");

                BugOut.WriteLine(BugOut.WildcardLabel, "T03  Changing the NewLine property on controller-2 from <LF> to <CR>...");
                this.port2.IsOnline = false;
                this.port2.NewLine = "\r";
                this.port2.IsOnline = true;
                BugOut.WriteLine(BugOut.WildcardLabel, "T03  NewLine property updated.");

                BugOut.WriteLine(BugOut.WildcardLabel, "T03  Writing 'wxyz0987' to COM1...");
                this.port1.WriteLine("wxyz0987", null);
                BugOut.WriteLine(BugOut.WildcardLabel, "T03  Data written.");

                BugOut.WriteLine(BugOut.WildcardLabel, "T03  Reading data from COM2...");
                string reading = this.port2.ReadLine(null);

                Assert.AreEqual("wxyz0987", reading);
                if (reading == "wxyz0987") BugOut.WriteLine(BugOut.WildcardLabel, "T03  Data received as transmitted");
            }
            catch (AssertionException) { throw; }
            catch (Exception ex)
            {                
                BugOut.WriteLine(BugOut.WildcardLabel, "T03  EXCEPTION: " + ex.Message);
                Assert.Fail();
            }

            //attempt to reset the NewLine property of the controllers to '\n'
            try
            {
                BugOut.WriteLine(BugOut.WildcardLabel, "T03  Restoring NewLine value to original setting for both controllers...");
                this.port1.IsOnline = false;
                this.port2.IsOnline = false;
                this.port1.NewLine = port1_NewLine;
                this.port2.NewLine = port2_NewLine;
                this.port1.IsOnline = true;
                this.port2.IsOnline = true;
                BugOut.WriteLine(BugOut.WildcardLabel, "T03  NewLine property restored.");
            }
            catch (Exception ex)
            {                
                BugOut.WriteLine(BugOut.WildcardLabel, "T03  EXCEPTION: " + ex.Message);
                Assert.Fail();
            }

            BugOut.WriteLine(BugOut.WildcardLabel, "T03");
            BugOut.WriteLine(BugOut.WildcardLabel, "*--> Test03: finished.");
            BugOut.WriteLine(BugOut.WildcardLabel, "*--------------------------------------------------------------*");
        }

        /// <summary>
        /// Checks the parity encoding (error checking) options for the serial ports by setting each parity type (initially on one 
        /// controller to achieve a data mismatch, then on both to ensure data reception as expected.)
        /// </summary>
        [Test]
        public void Test04_ParitySettingsTest()
        {
            BugOut.WriteLine(BugOut.WildcardLabel, "*--------------------------------------------------------------*");
            BugOut.WriteLine(BugOut.WildcardLabel, "*--> Test04: Parity Settings Test...");
            BugOut.WriteLine(BugOut.WildcardLabel, "T04");
            BugOut.WriteLine(BugOut.WildcardLabel, "T04  DESCRIPTION: Checks the operation of the serial parity encoding.");
            BugOut.WriteLine(BugOut.WildcardLabel, "T04  (Each parity setting will be checked.)");
            BugOut.WriteLine(BugOut.WildcardLabel, "T04");
            BugOut.WriteLine(BugOut.WildcardLabel, "T04  USES:    ChassisType_Serial.Configure(),");
            BugOut.WriteLine(BugOut.WildcardLabel, "T04           ChassisType_Serial.IsOnline (inherited from Chassis),");
            BugOut.WriteLine(BugOut.WildcardLabel, "T04           ChassisType_Serial.Read(),");
            BugOut.WriteLine(BugOut.WildcardLabel, "T04           ChassisType_Serial.Write(string).");
            BugOut.WriteLine(BugOut.WildcardLabel, "T04");

            System.IO.Ports.Parity port1_Parity = this.port1.Parity;
            System.IO.Ports.Parity port2_Parity = this.port2.Parity;

            try
            {
                for (int i = 0; i < 4; i++)
                {
                    System.IO.Ports.Parity parityToSet = System.IO.Ports.Parity.None;

                    switch (i)
                    {
                        case 0: parityToSet = System.IO.Ports.Parity.Odd; break;
                        case 1: parityToSet = System.IO.Ports.Parity.Even; break;
                        case 2: parityToSet = System.IO.Ports.Parity.Mark; break;
                        case 3: parityToSet = System.IO.Ports.Parity.Space; break;
                        default: break;
                    }

                    BugOut.WriteLine(BugOut.WildcardLabel, string.Format("T04  Setting COM1 parity to None, COM2 parity to {0}...", parityToSet));
                    this.port1.IsOnline = false;
                    this.port1.Configure(this.port1.BaudRate, this.port1.DataBits, this.port1.StopBits, System.IO.Ports.Parity.None,
                        this.port1.Handshaking, this.port1.InputBufferSize_bytes, this.port1.OutputBufferSize_bytes);
                    this.port1.IsOnline = true;
                    this.port2.IsOnline = false;
                    this.port2.Configure(this.port2.BaudRate, this.port2.DataBits, this.port2.StopBits, parityToSet,
                        this.port2.Handshaking, this.port2.InputBufferSize_bytes, this.port2.OutputBufferSize_bytes);
                    this.port2.IsOnline = true;
                    BugOut.WriteLine(BugOut.WildcardLabel, "T04  Parity updated.");

                    BugOut.WriteLine(BugOut.WildcardLabel, "T04  Writing 'abcd1234' to COM1...");
                    this.port1.Write("abcd1234", null);
                    BugOut.WriteLine(BugOut.WildcardLabel, "T04  Data written.");

                    BugOut.WriteLine(BugOut.WildcardLabel, "T04  Reading data from COM2...");
                    this.Delay(0.25);
                    string reading = this.port2.Read(null);
                    Assert.AreNotEqual("abcd1234", reading);
                    if (reading != "abcd1234") BugOut.WriteLine(BugOut.WildcardLabel, "T04  Data received NOT as transmitted (expected.)");

                    BugOut.WriteLine(BugOut.WildcardLabel, string.Format("T04  Setting COM1 parity to {0}...", parityToSet));
                    this.port1.IsOnline = false;
                    this.port1.Configure(this.port1.BaudRate, this.port1.DataBits, this.port1.StopBits, parityToSet,
                        this.port1.Handshaking, this.port1.InputBufferSize_bytes, this.port1.OutputBufferSize_bytes);
                    this.port1.IsOnline = true;
                    BugOut.WriteLine(BugOut.WildcardLabel, "T04  Parity updated.");

                    BugOut.WriteLine(BugOut.WildcardLabel, "T04  Writing 'wxyz0987' to COM1...");
                    this.port1.Write("wxyz0987", null);
                    BugOut.WriteLine(BugOut.WildcardLabel, "T04  Data written.");

                    BugOut.WriteLine(BugOut.WildcardLabel, "T04  Reading data from COM2...");
                    this.Delay(0.25);
                    reading = this.port2.Read(null);
                    Assert.AreEqual("wxyz0987", reading);
                    if (reading == "wxyz0987") BugOut.WriteLine(BugOut.WildcardLabel, "T04  Data received as transmitted.");
                }
            }
            catch (AssertionException) { throw; }
            catch (Exception ex)
            {
                BugOut.WriteLine(BugOut.WildcardLabel, "T04  EXCEPTION (expected): " + ex.Message);
                Assert.Fail();
            }

            try
            {
                BugOut.WriteLine(BugOut.WildcardLabel, "T04  Restoring parity to original setting on both controllers...");
                this.port1.IsOnline = false;
                this.port1.Configure(this.port1.BaudRate, this.port1.DataBits, this.port1.StopBits, port1_Parity,
                    this.port1.Handshaking, this.port1.InputBufferSize_bytes, this.port1.OutputBufferSize_bytes);
                this.port1.IsOnline = true;
                this.port2.IsOnline = false;
                this.port2.Configure(this.port2.BaudRate, this.port2.DataBits, this.port2.StopBits, port1_Parity,
                    this.port2.Handshaking, this.port2.InputBufferSize_bytes, this.port2.OutputBufferSize_bytes);
                this.port2.IsOnline = true;
                BugOut.WriteLine(BugOut.WildcardLabel, "T04  Parity restored.");
            }
            catch (Exception ex)
            {                
                BugOut.WriteLine(BugOut.WildcardLabel, "T04  EXCEPTION (expected): " + ex.Message);
                Assert.Fail();
            }

            BugOut.WriteLine(BugOut.WildcardLabel, "T04");
            BugOut.WriteLine(BugOut.WildcardLabel, "*--> Test04: finished.");
            BugOut.WriteLine(BugOut.WildcardLabel, "*--------------------------------------------------------------*");
        }

        /// <summary>
        /// Checks the number of data-bits per symbol can be changed on the serial port. This is done by setting one port to use 
        /// 8 data-bits, varying the other between 8 and 5 data-bits and checking the response if a character value of ff(hex) is
        /// sent each time.
        /// </summary>
        [Test]
        public void Test05_DataBitsTest()
        {
            BugOut.WriteLine(BugOut.WildcardLabel, "*--------------------------------------------------------------*");
            BugOut.WriteLine(BugOut.WildcardLabel, "*--> Test05: Number Of Data-Bits Test...");
            BugOut.WriteLine(BugOut.WildcardLabel, "T05");
            BugOut.WriteLine(BugOut.WildcardLabel, "T05  DESCRIPTION: Checks the number of data-bits per symbol can be");
            BugOut.WriteLine(BugOut.WildcardLabel, "T05  changed on the serial port. This is done by setting one port");
            BugOut.WriteLine(BugOut.WildcardLabel, "T05  to use 8 data-bits, varying the other between 8 and 5 data-bits");
            BugOut.WriteLine(BugOut.WildcardLabel, "T05  and checking the response if a character value of ff(hex) is");
            BugOut.WriteLine(BugOut.WildcardLabel, "T05  sent each time.");
            BugOut.WriteLine(BugOut.WildcardLabel, "T05");
            BugOut.WriteLine(BugOut.WildcardLabel, "T05  USES:    ChassisType_Serial.Configure(),");
            BugOut.WriteLine(BugOut.WildcardLabel, "T05           ChassisType_Serial.IsOnline (inherited from Chassis),");
            BugOut.WriteLine(BugOut.WildcardLabel, "T05           ChassisType_Serial.ReadByteArray(),");
            BugOut.WriteLine(BugOut.WildcardLabel, "T05           ChassisType_Serial.Write(byte[]).");
            BugOut.WriteLine(BugOut.WildcardLabel, "T05");

            int port1_DataBits = this.port1.DataBits;
            int port2_DataBits = this.port2.DataBits;

            try
            {
                //ensure controller-1 is set to 8 data-bits per symbol
                BugOut.WriteLine(BugOut.WildcardLabel, "T05  Setting COM1 to 8 data-bits per symbol...");
                this.port1.IsOnline = false;
                this.port1.Configure(this.port1.BaudRate, 8, this.port1.StopBits, this.port1.Parity, this.port1.Handshaking,
                    this.port1.InputBufferSize_bytes, this.port1.OutputBufferSize_bytes);
                this.port1.IsOnline = true;
                BugOut.WriteLine(BugOut.WildcardLabel, "T05  COM1 updated.");
                    
                //change controller-2's number of data-bits and check the data returned
                for (int i = 8; i > 4; i--)
                {
                    BugOut.WriteLine(BugOut.WildcardLabel, string.Format("T05  Setting COM2 to {0} data-bits per symbol...", i));
                    this.port2.IsOnline = false;
                    this.port2.Configure(this.port2.BaudRate, i, this.port2.StopBits, this.port2.Parity,
                        this.port2.Handshaking, this.port2.InputBufferSize_bytes, this.port2.OutputBufferSize_bytes);
                    this.port2.IsOnline = true;
                    BugOut.WriteLine(BugOut.WildcardLabel, "T05  COM2 updated.");

                    BugOut.WriteLine(BugOut.WildcardLabel, "T05  Writing char (ff in hex) to COM1...");
                    this.port1.Write(new byte[] { 255 }, null);
                    BugOut.WriteLine(BugOut.WildcardLabel, "T05  Data written.");

                    BugOut.WriteLine(BugOut.WildcardLabel, "T05  Reading data received at COM2...");
                    this.Delay(0.25);
                    byte[] returned = this.port2.ReadByteArray(null);

                    switch (i)
                    {                        
                        case 7: Assert.AreEqual(127, returned[0]); break;
                        case 6: Assert.AreEqual(63, returned[0]); break;
                        case 5: Assert.AreEqual(31, returned[0]); break;
                        default: Assert.AreEqual(255, returned[0]); break;
                    }

                    BugOut.WriteLine(BugOut.WildcardLabel,
                        string.Format("T05  Data received as expected ({0} in hex)", this.CharsToHexValuesString(returned)));
                }

                BugOut.WriteLine(BugOut.WildcardLabel, "T05  Restoring data-bits to original setting for both controllers...");
                this.port1.IsOnline = false;
                this.port1.Configure(this.port1.BaudRate, port1_DataBits, this.port1.StopBits, this.port1.Parity, this.port1.Handshaking,
                    this.port1.InputBufferSize_bytes, this.port1.OutputBufferSize_bytes);
                this.port1.IsOnline = true;
                this.port2.IsOnline = false;
                this.port2.Configure(this.port2.BaudRate, port2_DataBits, this.port2.StopBits, this.port2.Parity, this.port2.Handshaking,
                    this.port2.InputBufferSize_bytes, this.port2.OutputBufferSize_bytes);
                this.port2.IsOnline = true;
                BugOut.WriteLine(BugOut.WildcardLabel, "T05  Data-bits restored.");
            }
            catch (AssertionException) { throw; }
            catch (Exception ex)
            {
                BugOut.WriteLine(BugOut.WildcardLabel, "T05  EXCEPTION: " + ex.Message);
                Assert.Fail();
            }

            BugOut.WriteLine(BugOut.WildcardLabel, "T05");
            BugOut.WriteLine(BugOut.WildcardLabel, "*--> Test05: finished.");
            BugOut.WriteLine(BugOut.WildcardLabel, "*--------------------------------------------------------------*");
        }

        /// <summary>
        /// 
        /// </summary>
        [Test]
        public void Test06_StopBitsTest()
        {
            BugOut.WriteLine(BugOut.WildcardLabel, "*--------------------------------------------------------------*");
            BugOut.WriteLine(BugOut.WildcardLabel, "*--> Test06: Number Of Stop-Bits Test...");
            BugOut.WriteLine(BugOut.WildcardLabel, "T06");
            BugOut.WriteLine(BugOut.WildcardLabel, "T06  DESCRIPTION: <Enter description>");
            BugOut.WriteLine(BugOut.WildcardLabel, "T06");
            BugOut.WriteLine(BugOut.WildcardLabel, "T06  USES:    ChassisType_Serial.Configure(),");
            BugOut.WriteLine(BugOut.WildcardLabel, "T06           ChassisType_Serial.IsOnline (inherited from Chassis),");
            BugOut.WriteLine(BugOut.WildcardLabel, "T06           ChassisType_Serial.Read(),");
            BugOut.WriteLine(BugOut.WildcardLabel, "T06           ChassisType_Serial.Write(string).");
            BugOut.WriteLine(BugOut.WildcardLabel, "T06");

            System.IO.Ports.StopBits port1_StopBits = this.port1.StopBits;
            System.IO.Ports.StopBits port2_StopBits = this.port2.StopBits;
            int port1_DataBits = this.port1.DataBits;
            int port2_DataBits = this.port2.DataBits;

            try
            {
                BugOut.WriteLine(BugOut.WildcardLabel, "T06  Setting COM1 to 8 data-bits, ONE stop-bit per symbol...");
                this.port1.IsOnline = false;
                this.port1.Configure(this.port1.BaudRate, 8, System.IO.Ports.StopBits.One, this.port1.Parity,
                    this.port1.Handshaking, this.port1.InputBufferSize_bytes, this.port1.OutputBufferSize_bytes);
                this.port1.IsOnline = true;
                BugOut.WriteLine(BugOut.WildcardLabel, "T06  COM1 updated.");

                /* 
                 * ================================================================================================
                 * NOTE:    (ian.webb 11Jan2007) 
                 *          FOR LOOP UPPER LIMIT SET TO 8 AS MS .NET FRAMEWORK THROWS EXCEPTION WITH 1.5 STOP BITS!
                 * ================================================================================================
                 */

                for (int i = 0; i < 8; i++) 
                {
                    System.IO.Ports.StopBits stopBits = System.IO.Ports.StopBits.None;
                    int dataBits = 0;

                    switch (i)
                    {
                        case 0:
                            stopBits = System.IO.Ports.StopBits.One;
                            dataBits = 8;
                            break;
                        case 1:
                            stopBits = System.IO.Ports.StopBits.One;
                            dataBits = 7;
                            break;
                        case 2:
                            stopBits = System.IO.Ports.StopBits.One;
                            dataBits = 6;
                            break;
                        case 3:
                            stopBits = System.IO.Ports.StopBits.One;
                            dataBits = 5;
                            break;
                        case 4:
                            stopBits = System.IO.Ports.StopBits.Two;
                            dataBits = 8;
                            break;
                        case 5:
                            stopBits = System.IO.Ports.StopBits.Two;
                            dataBits = 7;
                            break;
                        case 6:
                            stopBits = System.IO.Ports.StopBits.Two;
                            dataBits = 6;
                            break;
                        case 7:
                            stopBits = System.IO.Ports.StopBits.Two;
                            dataBits = 5;
                            break;
                        case 8:
                            stopBits = System.IO.Ports.StopBits.OnePointFive;
                            dataBits = 5;
                            break;
                    }

                    BugOut.WriteLine(BugOut.WildcardLabel,
                        string.Format("T06  Setting COM2 to {0} stop-bits, {1} data-bits...", stopBits.ToString().ToUpper(), dataBits));
                    this.port2.IsOnline = false;
                    this.port2.Configure(this.port2.BaudRate, 7, stopBits, this.port2.Parity,
                        this.port2.Handshaking, this.port2.InputBufferSize_bytes, this.port2.OutputBufferSize_bytes);
                    this.port2.IsOnline = true;
                    BugOut.WriteLine(BugOut.WildcardLabel, "T06  COM2 updated.");

                    BugOut.WriteLine(BugOut.WildcardLabel, "T06  Writing some data to COM1...");
                    this.port1.Write(string.Format("somedata{0}{1}", stopBits.ToString(), dataBits), null);
                    BugOut.WriteLine(BugOut.WildcardLabel, "T06  Data written.");
                    BugOut.WriteLine(BugOut.WildcardLabel, "T06  Reading data from COM2...");
                    this.Delay(0.25);
                    string reading = this.port2.Read(null);
                    Assert.AreEqual(string.Format("somedata{0}{1}", stopBits.ToString(), dataBits), reading);
                    BugOut.WriteLine(BugOut.WildcardLabel, "T06  Data received as transmitted.");
                }
            }
            catch (AssertionException) { throw; }
            catch (Exception ex)
            {
                BugOut.WriteLine(BugOut.WildcardLabel, "T06  EXCEPTION: " + ex.Message);
                Assert.Fail();
            }

            try
            {
                BugOut.WriteLine(BugOut.WildcardLabel, "T06  Restoring data-bits and stop-bits to original settings on both controllers...");
                this.port1.IsOnline = false;
                this.port1.Configure(this.port1.BaudRate, port1_DataBits, port1_StopBits, this.port1.Parity, this.port1.Handshaking,
                    this.port1.InputBufferSize_bytes, this.port1.OutputBufferSize_bytes);
                this.port1.IsOnline = true;
                this.port2.IsOnline = false;
                this.port2.Configure(this.port2.BaudRate, port2_DataBits, port2_StopBits, this.port2.Parity, this.port2.Handshaking,
                    this.port2.InputBufferSize_bytes, this.port2.OutputBufferSize_bytes);
                this.port2.IsOnline = true;
                BugOut.WriteLine(BugOut.WildcardLabel, "T06  Settings restored.");
            }
            catch (Exception ex)
            {
                BugOut.WriteLine(BugOut.WildcardLabel, "T06  EXCEPTION: " + ex.Message);
                Assert.Fail();
            }

            BugOut.WriteLine(BugOut.WildcardLabel, "T06");
            BugOut.WriteLine(BugOut.WildcardLabel, "*--> Test06: finished.");
            BugOut.WriteLine(BugOut.WildcardLabel, "*--------------------------------------------------------------*");
        }

        /// <summary>
        /// 
        /// </summary>
        [Test]
        public void Test07_HandshakingTest()
        {
            BugOut.WriteLine(BugOut.WildcardLabel, "*--------------------------------------------------------------*");
            BugOut.WriteLine(BugOut.WildcardLabel, "*--> Test07: Serial Handshaking Test...");
            BugOut.WriteLine(BugOut.WildcardLabel, "T07");
            BugOut.WriteLine(BugOut.WildcardLabel, "T07  DESCRIPTION: Tests the operation of communications handshaking");
            BugOut.WriteLine(BugOut.WildcardLabel, "T07  for the ChassisType_Serial class. The test is performed as follows:");
            BugOut.WriteLine(BugOut.WildcardLabel, "T07  (i) Places both ports into No handshaking mode. Closes COM2. ");
            BugOut.WriteLine(BugOut.WildcardLabel, "T07  Writes data to COM1 (no problem expected.)");
            BugOut.WriteLine(BugOut.WildcardLabel, "T07  (ii) Changes the handshaking mode of COM1 to RTS then writes data to");
            BugOut.WriteLine(BugOut.WildcardLabel, "T07  COM1 (write timeout expected as COM2 is off-line.)");
            BugOut.WriteLine(BugOut.WildcardLabel, "T07  (iii) Opens COM2 and writes data to COM1. Checks the data transmitted");
            BugOut.WriteLine(BugOut.WildcardLabel, "T07  is received at COM2.");
            BugOut.WriteLine(BugOut.WildcardLabel, "T07  (iv) Changes the handshaking mode of COM1 to XOnXOff. Closes COM2.");
            BugOut.WriteLine(BugOut.WildcardLabel, "T07  Writes data to COM1 (no problem expected.)");
            BugOut.WriteLine(BugOut.WildcardLabel, "T07  (v) Opens COM2 and writes data to COM1 (data corruption expected.)");
            BugOut.WriteLine(BugOut.WildcardLabel, "T07  (vi) Changes handshaking of COM2 to XOnXOff. Writes data to COM1 (no");
            BugOut.WriteLine(BugOut.WildcardLabel, "T07  problems expected.)");
            BugOut.WriteLine(BugOut.WildcardLabel, "T07");

            System.IO.Ports.Handshake port1HS = this.port1.Handshaking;
            System.IO.Ports.Handshake port2HS = this.port2.Handshaking;

            BugOut.WriteLine(BugOut.WildcardLabel, "T07  Setting handshaking to None for both controllers (COM2 will be left closed)...");
            this.port1.IsOnline = false;
            this.port1.Configure(this.port1.BaudRate, this.port1.DataBits, this.port1.StopBits, this.port1.Parity,
                System.IO.Ports.Handshake.None, this.port1.InputBufferSize_bytes, this.port1.OutputBufferSize_bytes);         
            this.port1.IsOnline = true;

            this.port2.IsOnline = false;
            this.port2.Configure(this.port2.BaudRate, this.port2.DataBits, this.port2.StopBits, this.port2.Parity,
                System.IO.Ports.Handshake.None, this.port2.InputBufferSize_bytes, this.port2.OutputBufferSize_bytes);            
            //NOTE: leaving port2 offline
            BugOut.WriteLine(BugOut.WildcardLabel, "T07  Handshaking updated.");
            
            BugOut.WriteLine(BugOut.WildcardLabel, "T07  Writing some data to COM1...");
            this.port1.Write(this.GenerateTextData(100), null);
            BugOut.WriteLine(BugOut.WildcardLabel, "T07  Data written.");

            BugOut.WriteLine(BugOut.WildcardLabel, "T07  Setting handshaking on COM1 to RTS...");
            this.port1.IsOnline = false;
            this.port1.Configure(this.port1.BaudRate, this.port1.DataBits, this.port1.StopBits, this.port1.Parity,
                System.IO.Ports.Handshake.RequestToSend, this.port1.InputBufferSize_bytes, this.port1.OutputBufferSize_bytes);
            this.port1.IsOnline = true;
            BugOut.WriteLine(BugOut.WildcardLabel, "T07  Handshaking updated.");

            try
            {
                BugOut.WriteLine(BugOut.WildcardLabel, "T07  Writing some data to COM1...");
                this.port1.Write(this.GenerateTextData(50), null);
                BugOut.WriteLine(BugOut.WildcardLabel, "!--> FAIL: Expected a read timeout.");
                Assert.Fail();
            }
            catch (TimeoutException)
            {
                BugOut.WriteLine(BugOut.WildcardLabel, "T07  Timeout on write (expected, while waiting for receiver to become ready.)");
            }

            BugOut.WriteLine(BugOut.WildcardLabel, "T07  Opening COM2...");
            this.port2.IsOnline = true;
            BugOut.WriteLine(BugOut.WildcardLabel, "T07  port opened.");

            BugOut.WriteLine(BugOut.WildcardLabel, "T07  Writing some data to COM1...");
            string dataToSend = this.GenerateTextData(50);
            this.port1.Write(dataToSend, null);
            BugOut.WriteLine(BugOut.WildcardLabel, "T07  Data written.");

            BugOut.WriteLine(BugOut.WildcardLabel, "T07  Reading back data from COM2...");
            this.Delay(0.25);
            if (dataToSend == this.port2.Read(null)) BugOut.WriteLine(BugOut.WildcardLabel, "T07  Data received as transmitted.");
            else
            {
                BugOut.WriteLine(BugOut.WildcardLabel, "!--> FAIL: Expected to receive the data as transmitted.");
                Assert.Fail();
            }

            BugOut.WriteLine(BugOut.WildcardLabel, "T07  Setting handshaking to XOnXOff for COM1...");
            this.port1.IsOnline = false;
            this.port1.Configure(this.port1.BaudRate, this.port1.DataBits, this.port1.StopBits, this.port1.Parity,
                System.IO.Ports.Handshake.XOnXOff, this.port1.InputBufferSize_bytes, this.port1.OutputBufferSize_bytes);
            this.port1.IsOnline = true;
            BugOut.WriteLine(BugOut.WildcardLabel, "T07  handshaking updated.");

            BugOut.WriteLine(BugOut.WildcardLabel, "T07  Closing COM2...");
            this.port2.IsOnline = false;
            BugOut.WriteLine(BugOut.WildcardLabel, "T07  port closed.");

            BugOut.WriteLine(BugOut.WildcardLabel, "T07  Writing some data to COM1...");
            this.port1.Write(this.GenerateTextData(50), null);
            BugOut.WriteLine(BugOut.WildcardLabel, "T07  Data written.");

            BugOut.WriteLine(BugOut.WildcardLabel, "T07  Opening COM2...");
            this.port2.IsOnline = true;
            BugOut.WriteLine(BugOut.WildcardLabel, "T07  port opened.");

            BugOut.WriteLine(BugOut.WildcardLabel, "T07  Writing some data to COM1...");
            dataToSend = this.GenerateTextData(50);
            this.port1.Write(dataToSend, null);
            BugOut.WriteLine(BugOut.WildcardLabel, "T07  Data written.");

            BugOut.WriteLine(BugOut.WildcardLabel, "T07  Reading data from COM2...");
            if (dataToSend != this.port2.Read(null)) BugOut.WriteLine(BugOut.WildcardLabel, "T07  Data NOT received as transmitted (expected.)");
            else
            {
                BugOut.WriteLine(BugOut.WildcardLabel, "!--> FAIL: Data received as transmitted (expected corruption.)");
                Assert.Fail();
            }

            BugOut.WriteLine(BugOut.WildcardLabel, "T07  Setting handshaking to XOnXOff on COM2...");
            this.port2.IsOnline = false;
            this.port2.Configure(this.port2.BaudRate, this.port2.DataBits, this.port2.StopBits, this.port2.Parity,
                System.IO.Ports.Handshake.XOnXOff, this.port2.InputBufferSize_bytes, this.port2.OutputBufferSize_bytes);
            this.port2.IsOnline = true;
            BugOut.WriteLine(BugOut.WildcardLabel, "T07  handshaking updated.");

            BugOut.WriteLine(BugOut.WildcardLabel, "T07  Writing some data to COM1...");
            dataToSend = this.GenerateTextData(50);
            this.port1.Write(dataToSend, null);
            BugOut.WriteLine(BugOut.WildcardLabel, "T07  Data written.");

            BugOut.WriteLine(BugOut.WildcardLabel, "T07  Reading data from COM2...");
            if (dataToSend == this.port2.Read(null)) BugOut.WriteLine(BugOut.WildcardLabel, "T07  Data received as transmitted.");
            else
            {
                BugOut.WriteLine(BugOut.WildcardLabel, "!--> FAIL: Data NOT received as transmitted.");
                Assert.Fail();
            }

            BugOut.WriteLine(BugOut.WildcardLabel, "T07  Restoring original handshaking modes for both controllers...");
            this.port1.IsOnline = false;
            this.port1.Configure(this.port1.BaudRate, this.port1.DataBits, this.port1.StopBits, this.port1.Parity,
                port1HS, this.port1.InputBufferSize_bytes, this.port1.OutputBufferSize_bytes);
            this.port1.IsOnline = true;

            this.port2.IsOnline = false;
            this.port2.Configure(this.port2.BaudRate, this.port2.DataBits, this.port2.StopBits, this.port2.Parity,
                port2HS, this.port2.InputBufferSize_bytes, this.port2.OutputBufferSize_bytes);
            this.port2.IsOnline = true;
            BugOut.WriteLine(BugOut.WildcardLabel, "T07  Original handshaking modes restored.");

            BugOut.WriteLine(BugOut.WildcardLabel, "T07");
            BugOut.WriteLine(BugOut.WildcardLabel, "*--> Test07: finished.");
            BugOut.WriteLine(BugOut.WildcardLabel, "*--------------------------------------------------------------*");
        }

        #endregion

        #region Private Members

        /// <summary>
        /// Waits until a specified duration (in seconds) has elapsed and then returns.
        /// </summary>
        /// <param name="seconds">the number of seconds to wait.</param>
        private void Delay(double seconds)
        {
            int endTime = Environment.TickCount + (int)(seconds * 1000);

            while (endTime > Environment.TickCount)
            {
                System.Threading.Thread.Sleep(1);
            }
        }

        /// <summary>
        /// Parses the supplied string, creating a new string which contains the ASCII hex values of each 
        /// character in the original string, separated by spaces.
        /// </summary>
        /// <param name="dataIn">string to be parsed.</param>
        /// <returns>sequence of hex values that represents the string.</returns>
        private string CharsToHexValuesString(string dataIn)
        {
            return this.CharsToHexValuesString(this.StringToByteArray(dataIn));
        }

        /// <summary>
        /// Parses the supplied byte-array, creating a new string which contains the ASCII hex values of each 
        /// character in the original string, separated by spaces.
        /// </summary>
        /// <param name="dataIn">byte-array to be parsed.</param>
        /// <returns>sequence of hex values that represents the string.</returns>
        private string CharsToHexValuesString(byte[] dataIn)
        {
            try
            {
                string dataOut = "";

                for (int i = 0; i < dataIn.Length; i++)
                {
                    byte b = dataIn[i];
                    dataOut += this.DecimalValueToHexValueString((uint)b, 2) + " ";
                }

                return dataOut.Trim();
            }
            catch (Exception ex) { throw ex; }
        }

        /// <summary>
        /// Returns a hexidecimal (string format) representation of the supplied unsigned integer value. 
        /// </summary>
        /// <param name="decimalValue">integer value to convert.</param>
        /// <param name="minimumStringLength">minimum number of characters in returned string (zero stuffing if required.)</param>
        /// <returns>string with hexidecimal representation.</returns>
        private string DecimalValueToHexValueString(uint decimalValue, uint minimumStringLength)
        {
            try
            {
                string hexString = "";

                if (decimalValue > 0)
                {


                    //determine the number of characters required
                    double tempValue = Convert.ToDouble(decimalValue);
                    int charCount = 0;
                    while (tempValue >= 1)
                    {
                        tempValue /= 16;
                        charCount++;
                    }

                    for (int i = 0; i < charCount; i++)
                    {
                        uint hexValue = (uint)(Convert.ToDouble(decimalValue) / Math.Pow(16, charCount - i - 1));

                        //if value is too big due to rounding to int decrement it...
                        if ((hexValue * Math.Pow(16, charCount - i - 1)) > decimalValue) hexValue--;

                        decimalValue -= (uint)(Convert.ToDouble(hexValue) * Math.Pow(16, charCount - i - 1));

                        switch (hexValue)
                        {
                            case 0:
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                            case 5:
                            case 6:
                            case 7:
                            case 8:
                            case 9:
                                hexString += hexValue.ToString();
                                break;
                            case 10:
                                hexString += "a";
                                break;
                            case 11:
                                hexString += "b";
                                break;
                            case 12:
                                hexString += "c";
                                break;
                            case 13:
                                hexString += "d";
                                break;
                            case 14:
                                hexString += "e";
                                break;
                            case 15:
                                hexString += "f";
                                break;
                        }
                    }
                }
                else hexString = "0";

                //make sure string is formatted correctly
                while (hexString.Length < minimumStringLength) hexString = "0" + hexString;

                return hexString;

            }
            catch (Exception ex) { throw ex; }
        }

        /// <summary>
        /// Converts a string to its byte-array equivalent.
        /// </summary>
        /// <param name="dataIn">string to convert.</param>
        /// <returns>byte-array equivalent of the string.</returns>
        private byte[] StringToByteArray(string dataIn)
        {
            try
            {
                byte[] data = new byte[dataIn.Length];

                for (int i = 0; i < dataIn.Length; i++)
                {
                    data[i] = (byte)dataIn[i];
                }
                return data;
            }
            catch (Exception ex) { throw ex; }
        }

        /// <summary>
        /// Creates a string of randomly generated characters of a specified length. The characters generated are within a specific range 
        /// of ASCII codes to ensure they are all displayable! 
        /// </summary>
        /// <param name="length">required length of the string.</param>
        /// <returns>a string of randomly generated characters.</returns>
        private string GenerateTextData(int length)
        {
            string text = "";

            for (int i = 0; i < length; i++)
            {
                text += (char)new Random(i).Next(32, 122);
            }

            return text;
        }

        #endregion
    }
}

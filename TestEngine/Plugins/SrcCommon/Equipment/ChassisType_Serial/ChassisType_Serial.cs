//==========================================================================================================
// Copyright:			Copyright ?2006 Bookham Inc. All Rights Reserved. This document is company 
//						confidential and should not be copied, reproduced or otherwise utilised outside of 
//						Bookham Inc without the prior written consent of the company.
//
// Project:				Bookham Test-Engine
// Module:				ChassisType_Serial
// 
// FileName:			ChassisType_Serial.cs
//
// Author:				ian.webb
// Design Document:		N/A - see the Bookham Test-Engine documentation for more information on the 
//                      instrumentation sub-system and chassis types.
// Revision History:	09Jan2007 ian.webb: Initial version.
//
// Description:			A chassis type implementation for communication via RS232.
//==========================================================================================================

using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Text;

using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;

namespace Bookham.TestLibrary.ChassisTypes
{
    /// <summary>
    /// A chassis which uses the .NET Framework SerialPort class to provide instrument/chassis 
    /// communications support.
    /// </summary>
    public abstract class ChassisType_Serial : Chassis
    {
        #region Private Variables

        /// <summary>
        /// serial communications object to use for I/O.
        /// </summary>
        private SerialPort comms = null;

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of the ChassisType_Serial class.
        /// </summary>
        /// <param name="chassisName">decorative name for the chassis.</param>
        /// <param name="driverName">name of the driver class.</param>
        /// <param name="portName">port reference for the serial session (e.g. COM1.)</param>
        public ChassisType_Serial(string chassisName, string driverName, string portName)
            : base(chassisName, driverName, portName)
        {
            this.comms = new SerialPort(portName);
            this.comms.NewLine = "\n";
        }

        #endregion

        #region Overridden Chassis Members

        /// <summary>
        /// Gets or sets the chassis communication state (true = comms open, false = comms closed.)
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return this.IsOpen;
            }
            set
            {
                string instrumentName = "(None)";
                //Rasie Chassis Online Check Event to get Online value.
                ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name, instrumentName);
                OnChassisOnlineCheck(chassisOnlineCheckEventArgs);

                bool isOpen = this.IsOpen;
                if (isOpen && value) return;
                else if (!isOpen && !value) return;
                else
                {
                    if (value)
                    {
                        if (chassisOnlineCheckEventArgs.Online)
                        {
                            this.comms.Open();
                        }
                        //raise chassis write event for Test Engine Core.
                        OnChassisWrite("Serial.IsOnline:" + value.ToString(), this.Name, instrumentName);

                        this.DtrEnable = true;    //assert the DTR to say the port is up and running and ready for I/O
                    }
                    else
                    {
                        this.DtrEnable = false;   //disable the DTR to say the port is NOT ready for I/O
                        if (chassisOnlineCheckEventArgs.Online)
                        {
                            this.comms.Close();
                        }
                        //raise chassis write event for Test Engine Core.
                        OnChassisWrite("Serial.IsOnline:" + value.ToString(), this.Name, instrumentName);
                    }
                }

                
            }
        }

        /// <summary>
        /// Gets or sets the communications timeout in milliseconds.
        /// </summary>
        public override int Timeout_ms
        {           
            get
            {
                string instrumentName = "(None)";
                int timeOut;

                try
                {
                    //Rasie Chassis Online Check Event to get Online value.
                    ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name, instrumentName);
                    OnChassisOnlineCheck(chassisOnlineCheckEventArgs);

                    string command = "Timeout";
                    if (chassisOnlineCheckEventArgs.Online)
                    {
                        timeOut = this.comms.WriteTimeout;
                        ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, timeOut.ToString(), command, instrumentName);
                        OnChassisQuery(e);
                    }
                    else
                    {
                        //Gets response from TestEngine.
                        ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, "", command, instrumentName);
                        OnChassisQuery(e);
                        timeOut = int.Parse(e.DataRead);
                    }
                }
                catch (Exception ex)
                {
                    // Rethrow the exception 
                    throw new ChassisException("Failed SerialPort Timeout_ms_get.Chassis Name:[" + this.Name + "]", ex);
                }
                return timeOut;
            }
            set
            {
                string instrumentName = "(None)";

                try
                {
                    //Rasie Chassis Online Check Event to get Online value.
                    ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name, instrumentName);
                    OnChassisOnlineCheck(chassisOnlineCheckEventArgs);


                    if (chassisOnlineCheckEventArgs.Online)
                    {
                        this.comms.WriteTimeout = value;
                        this.comms.ReadTimeout = value;
                    }

                    //raise chassis write event for Test Engine Core.
                    OnChassisWrite("Timeout:" + value.ToString(), this.Name, instrumentName);
                }
                catch (Exception ex)
                {
                    // Rethrow the exception 
                    throw new ChassisException("Failed SerialPort Timeout_ms_set.Chassis Name:[" + this.Name + "];Instrume Name:[" + instrumentName + "].", ex);
                }
            }
        }

        #endregion

        #region Public Members (Port Configuration)

        /// <summary>
        /// Configures the serial communications parameters for the chassis. NOTE: The serial port 
        /// must be closed to re-configure these settings.
        /// </summary>
        /// <param name="baudRate">data transmission clock speed (serial baud-rate.)</param>
        /// <param name="dataBits">number of data-bits per byte.</param>
        /// <param name="stopBits">number of stop-bits per byte.</param>
        /// <param name="parityType">protocol to use for parity checking.</param>
        /// <param name="handshakingType">protocol to use for communications handshaking.</param>
        /// <param name="inputBuffer_bytes">size in bytes to make the serial input buffer.</param>
        /// <param name="outputBuffer_bytes">size in bytes to make the serial outputput buffer.</param>
        public void Configure(int baudRate, int dataBits, StopBits stopBits, Parity parityType,
            Handshake handshakingType, int inputBuffer_bytes, int outputBuffer_bytes)
        {
            this.Configure(baudRate, dataBits, stopBits, parityType, handshakingType, inputBuffer_bytes,
                outputBuffer_bytes, this.comms.NewLine);
        }

        /// <summary>
        /// Configures the serial communications parameters for the chassis. NOTE: The serial port 
        /// must be closed to re-configure these settings.
        /// </summary>
        /// <param name="baudRate">data transmission clock speed (serial baud-rate.)</param>
        /// <param name="dataBits">number of data-bits per byte.</param>
        /// <param name="stopBits">number of stop-bits per byte.</param>
        /// <param name="parityType">protocol to use for parity checking.</param>
        /// <param name="handshakingType">protocol to use for communications handshaking.</param>
        /// <param name="inputBuffer_bytes">size in bytes to make the serial input buffer.</param>
        /// <param name="outputBuffer_bytes">size in bytes to make the serial outputput buffer.</param>
        /// <param name="newLineTerminator">string to interpret as the end of a line when calling
        /// the ReadLine() and WriteLine() communications functions.</param>
        public void Configure(int baudRate, int dataBits, StopBits stopBits, Parity parityType,
            Handshake handshakingType, int inputBuffer_bytes, int outputBuffer_bytes,
            string newLineTerminator)
        {
            this.BaudRate = baudRate;
            this.DataBits = dataBits;
            this.StopBits = stopBits;
            this.Parity = parityType;
            this.Handshaking = handshakingType;

            switch (handshakingType)
            {
                case Handshake.RequestToSend:
                case Handshake.RequestToSendXOnXOff:
                    this.RtsEnable = true;        //assert the RTS line signalling mode
                    break;
                default:
                    this.RtsEnable = false;       //disable the RTS line signalling mode
                    break;
            }

            this.InputBufferSize_bytes = inputBuffer_bytes;
            this.OutputBufferSize_bytes = outputBuffer_bytes;
            this.NewLine = newLineTerminator;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the Request to Send (RTS) signal
        /// is enabled during serial communication.
        /// </summary>
        private bool RtsEnable
        {
            get
            {
                string instrumentName = "(None)";
                bool enable;

                try
                {
                    //Rasie Chassis Online Check Event to get Online value.
                    ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name, instrumentName);
                    OnChassisOnlineCheck(chassisOnlineCheckEventArgs);

                    string command = "RtsEnable";
                    if (chassisOnlineCheckEventArgs.Online)
                    {
                        enable = this.comms.RtsEnable;
                        ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, enable.ToString(), command, instrumentName);
                        OnChassisQuery(e);
                    }
                    else
                    {
                        //Gets response from TestEngine.
                        ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, "", command, instrumentName);
                        OnChassisQuery(e);
                        enable = bool.Parse(e.DataRead);
                    }
                }
                catch (Exception ex)
                {
                    // Rethrow the exception 
                    throw new ChassisException("Failed SerialPort RtsEnable_get.Chassis Name:[" + this.Name + "]", ex);
                }
                return enable;
            }
            set
            {
                string instrumentName = "(None)";

                try
                {
                    //Rasie Chassis Online Check Event to get Online value.
                    ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name, instrumentName);
                    OnChassisOnlineCheck(chassisOnlineCheckEventArgs);


                    if (chassisOnlineCheckEventArgs.Online)
                    {
                        this.comms.RtsEnable = value;
                    }

                    //raise chassis write event for Test Engine Core.
                    OnChassisWrite("RtsEnable:" + value.ToString(), this.Name, instrumentName);
                }
                catch (Exception ex)
                {
                    // Rethrow the exception 
                    throw new ChassisException("Failed SerialPort RtsEnable_set.Chassis Name:[" + this.Name + "];Instrume Name:[" + instrumentName + "].", ex);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value that enables the Data Terminal Ready (DTR) signal during
        /// serial communication.
        /// </summary>    
        private bool DtrEnable
        {
            get
            {
                string instrumentName = "(None)";
                bool enable;

                try
                {
                    //Rasie Chassis Online Check Event to get Online value.
                    ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name, instrumentName);
                    OnChassisOnlineCheck(chassisOnlineCheckEventArgs);

                    string command = "DtrEnable";
                    if (chassisOnlineCheckEventArgs.Online)
                    {
                        enable = this.comms.DtrEnable;
                        ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, enable.ToString(), command, instrumentName);
                        OnChassisQuery(e);
                    }
                    else
                    {
                        //Gets response from TestEngine.
                        ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, "", command, instrumentName);
                        OnChassisQuery(e);
                        enable = bool.Parse(e.DataRead);
                    }
                }
                catch (Exception ex)
                {
                    // Rethrow the exception 
                    throw new ChassisException("Failed SerialPort DtrEnable_get.Chassis Name:[" + this.Name + "]", ex);
                }
                return enable;
            }
            set
            {
                string instrumentName = "(None)";

                try
                {
                    //Rasie Chassis Online Check Event to get Online value.
                    ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name, instrumentName);
                    OnChassisOnlineCheck(chassisOnlineCheckEventArgs);


                    if (chassisOnlineCheckEventArgs.Online)
                    {
                        this.comms.DtrEnable = value;
                    }

                    //raise chassis write event for Test Engine Core.
                    OnChassisWrite("DtrEnable:" + value.ToString(), this.Name, instrumentName);
                }
                catch (Exception ex)
                {
                    // Rethrow the exception 
                    throw new ChassisException("Failed SerialPort DtrEnable_set.Chassis Name:[" + this.Name + "];Instrume Name:[" + instrumentName + "].", ex);
                }
            }
        }    

        private bool IsOpen
        {
            get
            {
                string instrumentName = "(None)";
                bool isOpen;

                try
                {
                    //Rasie Chassis Online Check Event to get Online value.
                    ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name, instrumentName);
                    OnChassisOnlineCheck(chassisOnlineCheckEventArgs);

                    string command = "IsOpen";
                    if (chassisOnlineCheckEventArgs.Online)
                    {
                        isOpen = this.comms.IsOpen;
                        ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, isOpen.ToString(), command, instrumentName);
                        OnChassisQuery(e);
                    }
                    else
                    {
                        //Gets response from TestEngine.
                        ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, "", command, instrumentName);
                        OnChassisQuery(e);
                        isOpen = bool.Parse(e.DataRead);
                    }
                }
                catch (Exception ex)
                {
                    // Rethrow the exception 
                    throw new ChassisException("Failed SerialPort IsOpen_get.Chassis Name:[" + this.Name + "]", ex);
                }
                return isOpen;
            }            
        }

        private int BytesToRead
        {
            get
            {
                string instrumentName = "(None)";
                int bytesToRead;

                try
                {
                    //Rasie Chassis Online Check Event to get Online value.
                    ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name, instrumentName);
                    OnChassisOnlineCheck(chassisOnlineCheckEventArgs);

                    string command = "BytesToRead";
                    if (chassisOnlineCheckEventArgs.Online)
                    {
                        bytesToRead = this.comms.BytesToRead;
                        ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, bytesToRead.ToString(), command, instrumentName);
                        OnChassisQuery(e);
                    }
                    else
                    {
                        //Gets response from TestEngine.
                        ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, "", command, instrumentName);
                        OnChassisQuery(e);
                        bytesToRead = int.Parse(e.DataRead);
                    }
                }
                catch (Exception ex)
                {
                    // Rethrow the exception 
                    throw new ChassisException("Failed SerialPort BytesToRead_get.Chassis Name:[" + this.Name + "]", ex);
                }
                return bytesToRead;
            }
        }


        /// <summary>
        /// Gets or sets the data transmission clock speed (serial baud-rate.) NOTE: The serial port 
        /// must be closed to re-configure this setting.
        /// </summary>
        public int BaudRate
        {
            get 
            { 
                string instrumentName = "(None)" ;
                int baudRate;

                try
                {
                    //Rasie Chassis Online Check Event to get Online value.
                    ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name, instrumentName);
                    OnChassisOnlineCheck(chassisOnlineCheckEventArgs);

                    string command = "BaudRate";
                    if (chassisOnlineCheckEventArgs.Online)
                    {
                        baudRate = this.comms.BaudRate;
                        ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, baudRate.ToString(), command, instrumentName);
                        OnChassisQuery(e);
                    }
                    else
                    {
                        //Gets response from TestEngine.
                        ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, "", command, instrumentName);
                        OnChassisQuery(e);
                        baudRate = int.Parse(e.DataRead);
                    }
                }
                catch (Exception ex)
                {
                    // Rethrow the exception 
                    throw new ChassisException("Failed SerialPort BaudRate_get.Chassis Name:[" + this.Name + "]", ex);
                }
                return baudRate;
            }
            set 
            { 
                string instrumentName = "(None)" ;

                try
                {
                    //Rasie Chassis Online Check Event to get Online value.
                    ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name, instrumentName);
                    OnChassisOnlineCheck(chassisOnlineCheckEventArgs);


                    if (chassisOnlineCheckEventArgs.Online)
                    {
                        this.comms.BaudRate = value;  
                    }

                    //raise chassis write event for Test Engine Core.
                    OnChassisWrite("BaudRate:" + value.ToString(), this.Name, instrumentName);
                }
                catch (Exception ex)
                {
                    // Rethrow the exception 
                    throw new ChassisException("Failed SerialPort BaudRate_set.Chassis Name:[" + this.Name + "];Instrume Name:[" + instrumentName + "].", ex);
                }                                      
            }
        }

        /// <summary>
        /// Gets or sets the number of data-bits per byte. NOTE: The serial port 
        /// must be closed to re-configure this setting.
        /// </summary>
        public int DataBits
        {
            get 
            {
                string instrumentName = "(None)";
                int dataBits;

                try
                {
                    //Rasie Chassis Online Check Event to get Online value.
                    ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name, instrumentName);
                    OnChassisOnlineCheck(chassisOnlineCheckEventArgs);

                    string command = "DataBits";
                    if (chassisOnlineCheckEventArgs.Online)
                    {
                        dataBits = this.comms.DataBits;
                        ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, dataBits.ToString(), command, instrumentName);
                        OnChassisQuery(e);
                    }
                    else
                    {
                        //Gets response from TestEngine.
                        ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, "", command, instrumentName);
                        OnChassisQuery(e);
                        dataBits = int.Parse(e.DataRead);
                    }
                }
                catch (Exception ex)
                {
                    // Rethrow the exception 
                    throw new ChassisException("Failed SerialPort DataBits_get.Chassis Name:[" + this.Name + "]", ex);
                }
                return dataBits;
            }
            set 
            {
                string instrumentName = "(None)";

                try
                {
                    //Rasie Chassis Online Check Event to get Online value.
                    ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name, instrumentName);
                    OnChassisOnlineCheck(chassisOnlineCheckEventArgs);


                    if (chassisOnlineCheckEventArgs.Online)
                    {
                        this.comms.DataBits = value; 
                    }

                    //raise chassis write event for Test Engine Core.
                    OnChassisWrite("Serial.BaudRate:" + value.ToString(), this.Name, instrumentName);
                }
                catch (Exception ex)
                {
                    // Rethrow the exception 
                    throw new ChassisException("Failed SerialPort DataBits_set.Chassis Name:[" + this.Name + "];Instrume Name:[" + instrumentName + "].", ex);
                }                
            }
        }

        /// <summary>
        /// Gets or sets the number of stop-bits per byte. NOTE: The serial port 
        /// must be closed to re-configure this setting.
        /// </summary>
        public StopBits StopBits
        {
            get 
            {
                string instrumentName = "(None)";
                StopBits stopBits;

                try
                {
                    //Rasie Chassis Online Check Event to get Online value.
                    ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name, instrumentName);
                    OnChassisOnlineCheck(chassisOnlineCheckEventArgs);

                    string command = "StopBits";
                    if (chassisOnlineCheckEventArgs.Online)
                    {
                        stopBits = this.comms.StopBits;
                        ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, stopBits.ToString(), command, instrumentName);
                        OnChassisQuery(e);
                    }
                    else
                    {
                        //Gets response from TestEngine.
                        ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, "", command, instrumentName);
                        OnChassisQuery(e);
                        stopBits = (StopBits)Enum.Parse(typeof(StopBits), e.DataRead);
                    }
                }
                catch (Exception ex)
                {
                    // Rethrow the exception 
                    throw new ChassisException("Failed SerialPort StopBits_get.Chassis Name:[" + this.Name + "]", ex);
                }
                return stopBits;
            }
            set 
            {
                string instrumentName = "(None)";

                try
                {
                    //Rasie Chassis Online Check Event to get Online value.
                    ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name, instrumentName);
                    OnChassisOnlineCheck(chassisOnlineCheckEventArgs);


                    if (chassisOnlineCheckEventArgs.Online)
                    {
                        this.comms.StopBits = value; 
                    }

                    //raise chassis write event for Test Engine Core.
                    OnChassisWrite("StopBits:" + value.ToString(), this.Name, instrumentName);
                }
                catch (Exception ex)
                {
                    // Rethrow the exception 
                    throw new ChassisException("Failed SerialPort StopBits_set.Chassis Name:[" + this.Name + "];Instrume Name:[" + instrumentName + "].", ex);
                }
                
            }
        }

        /// <summary>
        /// Gets or sets the parity checking protocol. NOTE: The serial port 
        /// must be closed to re-configure this setting.
        /// </summary>
        public Parity Parity
        {
            get 
            {
                string instrumentName = "(None)";
                Parity parity;

                try
                {
                    //Rasie Chassis Online Check Event to get Online value.
                    ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name, instrumentName);
                    OnChassisOnlineCheck(chassisOnlineCheckEventArgs);

                    string command = "Parity";
                    if (chassisOnlineCheckEventArgs.Online)
                    {
                        parity = this.comms.Parity;
                        ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, parity.ToString(), command, instrumentName);
                        OnChassisQuery(e);
                    }
                    else
                    {
                        //Gets response from TestEngine.
                        ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, "", command, instrumentName);
                        OnChassisQuery(e);
                        parity = (Parity)Enum.Parse(typeof(Parity), e.DataRead);
                    }
                }
                catch (Exception ex)
                {
                    // Rethrow the exception 
                    throw new ChassisException("Failed SerialPort Parity_get.Chassis Name:[" + this.Name + "]", ex);
                }
                return parity;
            }
            set 
            {
                string instrumentName = "(None)";

                try
                {
                    //Rasie Chassis Online Check Event to get Online value.
                    ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name, instrumentName);
                    OnChassisOnlineCheck(chassisOnlineCheckEventArgs);


                    if (chassisOnlineCheckEventArgs.Online)
                    {
                        this.comms.Parity = value; 
                    }

                    //raise chassis write event for Test Engine Core.
                    OnChassisWrite("Parity:" + value.ToString(), this.Name, instrumentName);
                }
                catch (Exception ex)
                {
                    // Rethrow the exception 
                    throw new ChassisException("Failed SerialPort Parity_set.Chassis Name:[" + this.Name + "];Instrume Name:[" + instrumentName + "].", ex);
                }
                
            }
        }

        /// <summary>
        /// Gets or sets the serial communications handshaking protocol. NOTE: The serial port 
        /// must be closed to re-configure this setting.
        /// </summary>
        public Handshake Handshaking
        {
            get 
            {
                string instrumentName = "(None)";
                Handshake handshake;

                try
                {
                    //Rasie Chassis Online Check Event to get Online value.
                    ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name, instrumentName);
                    OnChassisOnlineCheck(chassisOnlineCheckEventArgs);

                    string command = "Handshaking";
                    if (chassisOnlineCheckEventArgs.Online)
                    {
                        handshake = this.comms.Handshake;
                        ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, handshake.ToString(), command, instrumentName);
                        OnChassisQuery(e);
                    }
                    else
                    {
                        //Gets response from TestEngine.
                        ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, "", command, instrumentName);
                        OnChassisQuery(e);
                        handshake = (Handshake)Enum.Parse(typeof(Handshake), e.DataRead);
                    }
                }
                catch (Exception ex)
                {
                    // Rethrow the exception 
                    throw new ChassisException("Failed SerialPort Handshake_get.Chassis Name:[" + this.Name + "]", ex);
                }
                return handshake;                
            }
            set 
            {
                string instrumentName = "(None)";

                try
                {
                    //Rasie Chassis Online Check Event to get Online value.
                    ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name, instrumentName);
                    OnChassisOnlineCheck(chassisOnlineCheckEventArgs);


                    if (chassisOnlineCheckEventArgs.Online)
                    {
                        this.comms.Handshake = value;
                    }

                    //raise chassis write event for Test Engine Core.
                    OnChassisWrite("Handshake:" + value.ToString(), this.Name, instrumentName);
                }
                catch (Exception ex)
                {
                    // Rethrow the exception 
                    throw new ChassisException("Failed SerialPort Handshake_set.Chassis Name:[" + this.Name + "];Instrume Name:[" + instrumentName + "].", ex);
                }
                 
            }
        }

        /// <summary>
        /// Gets or sets the size of the serial port input buffer in bytes. NOTE: The serial port 
        /// must be closed to re-configure this setting.
        /// </summary>
        public int InputBufferSize_bytes
        {
            get 
            {
                string instrumentName = "(None)";
                int size;

                try
                {
                    //Rasie Chassis Online Check Event to get Online value.
                    ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name, instrumentName);
                    OnChassisOnlineCheck(chassisOnlineCheckEventArgs);

                    string command = "InputBufferSize";
                    if (chassisOnlineCheckEventArgs.Online)
                    {
                        size = this.comms.ReadBufferSize;
                        ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, size.ToString(), command, instrumentName);
                        OnChassisQuery(e);
                    }
                    else
                    {
                        //Gets response from TestEngine.
                        ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, "", command, instrumentName);
                        OnChassisQuery(e);
                        size = int.Parse(e.DataRead);
                    }
                }
                catch (Exception ex)
                {
                    // Rethrow the exception 
                    throw new ChassisException("Failed SerialPort InputBufferSize_bytes_get.Chassis Name:[" + this.Name + "]", ex);
                }
                return size;  
                
            }
            set 
            {
                string instrumentName = "(None)";

                try
                {
                    //Rasie Chassis Online Check Event to get Online value.
                    ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name, instrumentName);
                    OnChassisOnlineCheck(chassisOnlineCheckEventArgs);


                    if (chassisOnlineCheckEventArgs.Online)
                    {
                        this.comms.ReadBufferSize = value; 
                    }

                    //raise chassis write event for Test Engine Core.
                    OnChassisWrite("InputBufferSize:" + value.ToString(), this.Name, instrumentName);
                }
                catch (Exception ex)
                {
                    // Rethrow the exception 
                    throw new ChassisException("Failed SerialPort InputBufferSize_set.Chassis Name:[" + this.Name + "];Instrume Name:[" + instrumentName + "].", ex);
                }
                
            }
        }

        /// <summary>
        /// Gets or sets the size of the serial port output buffer in bytes. NOTE: The serial port 
        /// must be closed to re-configure this setting.
        /// </summary>
        public int OutputBufferSize_bytes
        {
            get 
            {
                string instrumentName = "(None)";
                int size;

                try
                {
                    //Rasie Chassis Online Check Event to get Online value.
                    ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name, instrumentName);
                    OnChassisOnlineCheck(chassisOnlineCheckEventArgs);

                    string command = "OutputBufferSize";
                    if (chassisOnlineCheckEventArgs.Online)
                    {
                        size = this.comms.WriteBufferSize;
                        ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, size.ToString(), command, instrumentName);
                        OnChassisQuery(e);
                    }
                    else
                    {
                        //Gets response from TestEngine.
                        ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, "", command, instrumentName);
                        OnChassisQuery(e);
                        size = int.Parse(e.DataRead);
                    }
                }
                catch (Exception ex)
                {
                    // Rethrow the exception 
                    throw new ChassisException("Failed SerialPort OutputBufferSize_bytes_get.Chassis Name:[" + this.Name + "]", ex);
                }
                return size; 
            }
            set 
            {
                string instrumentName = "(None)";

                try
                {
                    //Rasie Chassis Online Check Event to get Online value.
                    ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name, instrumentName);
                    OnChassisOnlineCheck(chassisOnlineCheckEventArgs);


                    if (chassisOnlineCheckEventArgs.Online)
                    {
                        this.comms.WriteBufferSize = value; 
                    }

                    //raise chassis write event for Test Engine Core.
                    OnChassisWrite("OutputBufferSize:" + value.ToString(), this.Name, instrumentName);
                }
                catch (Exception ex)
                {
                    // Rethrow the exception 
                    throw new ChassisException("Failed SerialPort OutputBufferSize_set.Chassis Name:[" + this.Name + "];Instrume Name:[" + instrumentName + "].", ex);
                }                
            }
        }

        /// <summary>
        /// Gets or sets the value used to interpret the end of a call when executing the ReadLine() and
        /// WriteLine() functions. (default is a line-feed.)
        /// </summary>
        public string NewLine
        {
            get 
            {
                string instrumentName = "(None)";
                string newLine;

                try
                {
                    //Rasie Chassis Online Check Event to get Online value.
                    ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name, instrumentName);
                    OnChassisOnlineCheck(chassisOnlineCheckEventArgs);

                    string command = "NewLine";
                    if (chassisOnlineCheckEventArgs.Online)
                    {
                        newLine = this.comms.NewLine;
                        ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, newLine, command, instrumentName);
                        OnChassisQuery(e);
                    }
                    else
                    {
                        ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, "", command, instrumentName);
                        OnChassisQuery(e);
                        newLine = e.DataRead;                       
                    }
                }
                catch (Exception ex)
                {
                    // Rethrow the exception 
                    throw new ChassisException("Failed SerialPort NewLine_get.Chassis Name:[" + this.Name + "]", ex);
                }
                return newLine; 
            }
            set 
            {
                string instrumentName = "(None)";

                try
                {
                    //Rasie Chassis Online Check Event to get Online value.
                    ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name, instrumentName);
                    OnChassisOnlineCheck(chassisOnlineCheckEventArgs);


                    if (chassisOnlineCheckEventArgs.Online)
                    {
                        this.comms.NewLine = value;
                    }

                    //raise chassis write event for Test Engine Core.
                    OnChassisWrite("NewLine:" + value.ToString(), this.Name, instrumentName);
                }
                catch (Exception ex)
                {
                    // Rethrow the exception 
                    throw new ChassisException("Failed SerialPort NewLine_set.Chassis Name:[" + this.Name + "];Instrume Name:[" + instrumentName + "].", ex);
                }
                 
            }
        }

        #endregion

        #region Public Members (Serial I/O)

        /// <summary>
        /// Writes data to the serial port.
        /// </summary>
        /// <param name="data">string data to write.</param>
        /// <param name="instrument">equipment reference.</param>
        public void Write(string data, Instrument instrument)
        {
            base.LogEvent("WRITE STRING: " + data);
            string instrumentName = (instrument == null ? "(None)" : instrument.Name);

            try
            {
                //Rasie Chassis Online Check Event to get Online value.
                ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name, instrumentName);
                OnChassisOnlineCheck(chassisOnlineCheckEventArgs);

                if (chassisOnlineCheckEventArgs.Online == true)
                {
                    this.CheckEquipmentIsOnline(instrument);
                }

                if (!this.IsOpen)
                    throw new ChassisException("cannot write data - the serial port is closed");
                else if (this.Locked)
                    throw new ChassisException("cannot write data - the session is locked");

                if (chassisOnlineCheckEventArgs.Online)
                {
                    this.comms.Write(data);
                }

                //raise chassis write event for Test Engine Core.
                OnChassisWrite(data, this.Name, instrumentName);
            }
            catch (Exception ex)
            {
                // Rethrow the exception 
                throw new ChassisException("Failed SerialPort Write.Chassis Name:[" + this.Name + "];Instrume Name:[" + instrumentName + "];Command:[" + data + "].", ex);
            }
        }

        /// <summary>
        /// Writes data to the serial port.
        /// </summary>
        /// <param name="data">array of bytes to write.</param>
        /// <param name="instrument">equipment reference.</param>
        public void Write(byte[] data, Instrument instrument)
        {
            base.LogEvent("WRITE BYTE-ARRAY: " + this.CharsToHexValuesString(data));

            string instrumentName = (instrument == null ? "(None)" : instrument.Name);

            try
            {
                //Rasie Chassis Online Check Event to get Online value.
                ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name, instrumentName);
                OnChassisOnlineCheck(chassisOnlineCheckEventArgs);

                if (chassisOnlineCheckEventArgs.Online == true)
                {
                    this.CheckEquipmentIsOnline(instrument);
                }

                if (!this.IsOpen) throw new ChassisException("cannot write data - the serial port is closed");
                else if (this.Locked) throw new ChassisException("cannot write data - the session is locked");

                if (chassisOnlineCheckEventArgs.Online)
                {
                    this.comms.Write(data, 0, data.Length);
                }

                //raise chassis write event for Test Engine Core.
                OnChassisWrite(data, this.Name, instrumentName);
            }
            catch (Exception ex)
            {
                // Rethrow the exception 
                throw new ChassisException("Failed SerialPort Write.Chassis Name:[" + this.Name + "];Instrume Name:[" + instrumentName + "];Command:[" + data + "].", ex);
            }
        }

        /// <summary>
        /// Writes data to the serial port, appended with the string identified by the NewLine property.
        /// </summary>
        /// <param name="data">string data to write.</param>
        /// <param name="instrument">equipment reference.</param>
        public void WriteLine(string data, Instrument instrument)
        {
            base.LogEvent("WRITE-LINE STRING: " + data);

            string instrumentName = (instrument == null ? "(None)" : instrument.Name);

            try
            {
                //Rasie Chassis Online Check Event to get Online value.
                ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name, instrumentName);
                OnChassisOnlineCheck(chassisOnlineCheckEventArgs);

                if (chassisOnlineCheckEventArgs.Online == true)
                {
                    this.CheckEquipmentIsOnline(instrument);
                }

                if (!this.IsOpen)
                    throw new ChassisException("cannot write data - the serial port is closed");
                else if (this.Locked)
                    throw new ChassisException("cannot write data - the session is locked");
                if (chassisOnlineCheckEventArgs.Online)
                {
                    this.comms.WriteLine(data);
                }

                //raise chassis write event for Test Engine Core.
                OnChassisWrite(data, this.Name, instrumentName);
            }
            catch (Exception ex)
            {
                // Rethrow the exception 
                throw new ChassisException("Failed SerialPort WriteLine.Chassis Name:[" + this.Name + "];Instrume Name:[" + instrumentName + "];Command:[" + data + "].", ex);
            }
        }

        /// <summary>
        /// Reads all data currently in the serial port's input buffer.
        /// </summary>
        /// <param name="instrument">equipment reference.</param>
        /// <returns>data read from serial port.</returns>
        public string Read(Instrument instrument)
        {
            string response = "";
            string instrumentName = (instrument == null ? "(None)" : instrument.Name);

            try
            {
                //Rasie Chassis Online Check Event to get Online value.
                ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name, instrumentName);
                OnChassisOnlineCheck(chassisOnlineCheckEventArgs);


                if (chassisOnlineCheckEventArgs.Online)
                {
                    this.CheckEquipmentIsOnline(instrument);
                }

                if (!this.IsOpen)
                    throw new ChassisException("cannot read data - the serial port is closed");
                else if (this.Locked)
                    throw new ChassisException("cannot read data - the session is locked");

                if (chassisOnlineCheckEventArgs.Online)
                {
                    if (this.comms.BytesToRead > 0)
                        response = this.comms.ReadExisting();

                    ChassisReadEventArgs readEventArgs = new ChassisReadEventArgs(this.Name, response, instrumentName);
                    OnChassisRead(readEventArgs);
                }
                else
                {
                    //Gets response from TestEngine.
                    ChassisReadEventArgs readEventArgs = new ChassisReadEventArgs(this.Name, "", instrumentName);
                    OnChassisRead(readEventArgs);
                    response = readEventArgs.DataRead;
                }
            }
            catch (Exception ex)
            {
                // Rethrow the exception 
                throw new ChassisException("Failed SerialPort Read.Chassis Name:[" + this.Name + "];Instrume Name:[" + instrumentName + "].", ex);
            }

            base.LogEvent("READ STRING: " + response);
            return response;
        }

        /// <summary>
        /// Reads data from the serial port's input buffer, until the string identified by the NewLine 
        /// property is matched. The value of the NewLine property will be discarded from the returned
        /// string.
        /// </summary>
        /// <param name="instrument">equipment reference.</param>
        /// <returns>a line of data read from the serial port.</returns>
        public string ReadLine(Instrument instrument)
        {
            string response = "";
            string instrumentName = (instrument == null ? "(None)" : instrument.Name);

            try
            {
                //Rasie Chassis Online Check Event to get Online value.
                ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name, instrumentName);
                OnChassisOnlineCheck(chassisOnlineCheckEventArgs);


                if (chassisOnlineCheckEventArgs.Online)
                {
                    this.CheckEquipmentIsOnline(instrument);
                }

                if (!this.IsOpen)
                    throw new ChassisException("cannot read data - the serial port is closed");
                else if (this.Locked)
                    throw new ChassisException("cannot read data - the session is locked");

                if (chassisOnlineCheckEventArgs.Online)
                {
                    response = this.comms.ReadLine();

                    ChassisReadEventArgs readEventArgs = new ChassisReadEventArgs(this.Name, response, instrumentName);
                    OnChassisRead(readEventArgs);
                }
                else
                {
                    //Gets response from TestEngine.
                    ChassisReadEventArgs readEventArgs = new ChassisReadEventArgs(this.Name, "", instrumentName);
                    OnChassisRead(readEventArgs);
                    response = readEventArgs.DataRead;
                }
            }
            catch (Exception ex)
            {
                // Rethrow the exception 
                throw new ChassisException("Failed SerialPort ReadLine.Chassis Name:[" + this.Name + "];Instrume Name:[" + instrumentName + "].", ex);
            }

            base.LogEvent("READ-LINE STRING: " + response);
            return response;
        }

        /// <summary>
        /// Reads all data currently in the serial port's input buffer and returns it as an array of bytes.
        /// </summary>
        /// <param name="instrument">equipment reference.</param>
        /// <returns></returns>
        public byte[] ReadByteArray(Instrument instrument)
        {
            byte[] response = new byte[0];

            string instrumentName = (instrument == null ? "(None)" : instrument.Name);

            try
            {
                //Rasie Chassis Online Check Event to get Online value.
                ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name, instrumentName);
                OnChassisOnlineCheck(chassisOnlineCheckEventArgs);

                if (chassisOnlineCheckEventArgs.Online)
                {
                    this.CheckEquipmentIsOnline(instrument);
                }

                if (!this.IsOpen)
                    throw new ChassisException("cannot read data - the serial port is closed");
                else if (this.Locked)
                    throw new ChassisException("cannot read data - the session is locked");

                if (chassisOnlineCheckEventArgs.Online)
                {
                    int bytesToRead = this.comms.BytesToRead;
                    if (bytesToRead > 0)
                    {
                        response = new byte[bytesToRead];
                        for (int i = 0; i < response.Length; i++)
                        {
                            response[i] = (byte)this.comms.ReadByte();
                        }
                    }

                    OnChassisRead(response, this.Name, instrumentName);
                }
                else
                {
                    //Gets response from TestEngine.
                    response = OnChassisRead(this.Name, instrumentName);
                }

            }
            catch (Exception ex)
            {
                // Rethrow the exception 
                throw new ChassisException("Failed SerialPort ReadByteArray.Chassis Name:[" + this.Name + "];Instrume Name:[" + instrumentName + "].", ex);
            }

            base.LogEvent("READ BYTE_ARRAY: " + this.CharsToHexValuesString(response));
            return response;
        }

        /// <summary>
        /// Writes a query to the serial port and returns the response. NOTE: This function uses the
        /// ReadLine() and WriteLine() functions, so if the NewLine property is incorrectly configured
        /// it may fail!
        /// </summary>
        /// <param name="query">data query to write.</param>
        /// <param name="instrument">equipment reference.</param>
        /// <returns>data returned.</returns>
        public string Query(string query, Instrument instrument)
        {
            base.LogEvent("QUERY (" + query + ")");

            string instrumentName = (instrument == null ? "(None)" : instrument.Name);

            //Rasie Chassis Online Check Event to get Online value.
            ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name, instrumentName);
            OnChassisOnlineCheck(chassisOnlineCheckEventArgs);

            if (chassisOnlineCheckEventArgs.Online == true)
            {
                this.CheckEquipmentIsOnline(instrument);
            }

            this.Read(instrument);
            this.WriteLine(query, instrument);
            return this.ReadLine(instrument);
        }

        /// <summary>
        /// Writes a query to the serial port and returns the response. NOTE: This function does not append
        /// anything to the outgoing data and will wait the full timeout period before reading back the 
        /// response.
        /// </summary>
        /// <param name="query">data query to write.</param>
        /// <param name="instrument">equipment reference.</param>
        /// <returns>data returned.</returns>
        public byte[] Query(byte[] query, Instrument instrument)
        {
            base.LogEvent("QUERY (" + this.CharsToHexValuesString(query) + ")");

            string instrumentName = (instrument == null ? "(None)" : instrument.Name);

            //Rasie Chassis Online Check Event to get Online value.
            ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name, instrumentName);
            OnChassisOnlineCheck(chassisOnlineCheckEventArgs);

            if (chassisOnlineCheckEventArgs.Online == true)
            {
                this.CheckEquipmentIsOnline(instrument);
            }

            //purge the existing read-data
            this.Read(instrument);

            //write the query and new-line characters as bytes
            byte[] dataToSend = new byte[query.Length + this.NewLine.Length];
            for (int i = 0; i < dataToSend.Length; i++)
            {
                if (i < query.Length) dataToSend[i] = query[i];
                else dataToSend[i] = (byte)this.NewLine[i - query.Length];
            }

            this.Write(dataToSend, instrument);

            //read back subject to the timeout criteria
            int maxTicks = Environment.TickCount + this.comms.ReadTimeout;
            while (maxTicks > Environment.TickCount)
            {
                System.Threading.Thread.Sleep(10);
            }

            return this.ReadByteArray(instrument);
        }

        #endregion

        #region Private Members

        /// <summary>
        /// checks the instrument being communicated is online.
        /// </summary>
        /// <param name="instrument">instrument to check.</param>
        private void CheckEquipmentIsOnline(Instrument instrument)
        {
            if (instrument == null)
            {
                if (!this.IsOnline)
                {
                    throw new ChassisException("Attempt to communicate with '" + base.Name + "' while offline");
                }
            }
            else if (!instrument.IsOnline)
            {
                throw new ChassisException("Attempt to communicate with '" + instrument.Name + "' while offline");
            }

        }

        /// <summary>
        /// Parses the supplied string, creating a new string which contains the ASCII hex values of each 
        /// character in the original string, separated by spaces.
        /// </summary>
        /// <param name="dataIn">string to be parsed.</param>
        /// <returns>sequence of hex values that represents the string.</returns>
        private string CharsToHexValuesString(string dataIn)
        {
            return this.CharsToHexValuesString(this.StringToByteArray(dataIn));
        }

        /// <summary>
        /// Parses the supplied byte-array, creating a new string which contains the ASCII hex values of each 
        /// character in the original string, separated by spaces.
        /// </summary>
        /// <param name="dataIn">byte-array to be parsed.</param>
        /// <returns>sequence of hex values that represents the string.</returns>
        private string CharsToHexValuesString(byte[] dataIn)
        {
            try
            {
                string dataOut = "";

                for (int i = 0; i < dataIn.Length; i++)
                {
                    byte b = dataIn[i];
                    dataOut += this.DecimalValueToHexValueString((uint)b, 2) + " ";
                }

                return dataOut.Trim();
            }
            catch (Exception ex) { throw ex; }
        }

        /// <summary>
        /// Returns a hexidecimal (string format) representation of the supplied unsigned integer value. 
        /// </summary>
        /// <param name="decimalValue">integer value to convert.</param>
        /// <param name="minimumStringLength">minimum number of characters in returned string (zero stuffing if required.)</param>
        /// <returns>string with hexidecimal representation.</returns>
        private string DecimalValueToHexValueString(uint decimalValue, uint minimumStringLength)
        {
            try
            {
                string hexString = "";

                if (decimalValue > 0)
                {


                    //determine the number of characters required
                    double tempValue = Convert.ToDouble(decimalValue);
                    int charCount = 0;
                    while (tempValue >= 1)
                    {
                        tempValue /= 16;
                        charCount++;
                    }

                    for (int i = 0; i < charCount; i++)
                    {
                        uint hexValue = (uint)(Convert.ToDouble(decimalValue) / Math.Pow(16, charCount - i - 1));

                        //if value is too big due to rounding to int decrement it...
                        if ((hexValue * Math.Pow(16, charCount - i - 1)) > decimalValue) hexValue--;

                        decimalValue -= (uint)(Convert.ToDouble(hexValue) * Math.Pow(16, charCount - i - 1));

                        switch (hexValue)
                        {
                            case 0:
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                            case 5:
                            case 6:
                            case 7:
                            case 8:
                            case 9:
                                hexString += hexValue.ToString();
                                break;
                            case 10:
                                hexString += "a";
                                break;
                            case 11:
                                hexString += "b";
                                break;
                            case 12:
                                hexString += "c";
                                break;
                            case 13:
                                hexString += "d";
                                break;
                            case 14:
                                hexString += "e";
                                break;
                            case 15:
                                hexString += "f";
                                break;
                        }
                    }
                }
                else hexString = "0";

                //make sure string is formatted correctly
                while (hexString.Length < minimumStringLength) hexString = "0" + hexString;

                return hexString;

            }
            catch (Exception ex) { throw ex; }
        }

        /// <summary>
        /// Converts a string to its byte-array equivalent.
        /// </summary>
        /// <param name="dataIn">string to convert.</param>
        /// <returns>byte-array equivalent of the string.</returns>
        private byte[] StringToByteArray(string dataIn)
        {
            try
            {
                byte[] data = new byte[dataIn.Length];

                for (int i = 0; i < dataIn.Length; i++)
                {
                    data[i] = (byte)dataIn[i];
                }
                return data;
            }
            catch (Exception ex) { throw ex; }
        }

        #endregion
    }
}

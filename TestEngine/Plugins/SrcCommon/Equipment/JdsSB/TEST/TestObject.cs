using System;
using System.Collections.Generic;
// TODO: You'll probably need these includes once you add your references!
//using Bookham.TestLibrary.ChassisNS;
//using Bookham.TestLibrary.Instruments;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestEngine.Framework.Logging;
using System.Text;
using NUnit.Framework;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestSolution.ChassisNS;
using Bookham.TestSolution.Instruments;

namespace ag86060TestHarness
{
    /// <exclude />	
    [TestFixture]
    public class Test_Object
    {
        #region Private data (incl equipment references)
        // PUT YOUR CHASSIS REFERENCE HERE
        private Chassis_JdsSB testChassis;

        // PUT YOUR INSTRUMENT REFERENCES HERE
        private Inst_JdsSb_SimpleSwitch testInstr;        
        #endregion

        #region Constants for use during test.
        // VISA Chassis for where to find the instrument
        const string visaResource = "GPIB0::12::INSTR";
        const string chassisName = "JdsSb Chassis";
        const string instr1Name = "SW1";
        #endregion

        /// <exclude />
        [TestFixtureSetUp]
        public void Setup()
        {
            // initialise Test Engine logging domain
            Bookham.TestEngine.Framework.Logging.Initializer.Init();
            Bookham.TestEngine.Framework.Logging.BugOut.SetOutputToConsole(true);
            TestOutput("*** Test Harness Initialising ***");

            // create chassis objects
            testChassis = new Chassis_JdsSB(chassisName, "Chassis_JdsSB", visaResource);
            TestOutput(testChassis, "Created OK");

            // create instrument objects            
            testInstr = new Inst_JdsSb_SimpleSwitch(instr1Name, "Inst_JdsSb_SimpleSwitch", "", "", testChassis);
            TestOutput(testInstr, "Created OK");

            // put them online
            testChassis.IsOnline = true;
            testChassis.EnableLogging = true;            
            TestOutput(testChassis, "IsOnline set true OK");

            testInstr.IsOnline = true;
            testInstr.EnableLogging = true;
            TestOutput(testInstr, "IsOnline set true OK");            
        }

        /// <exclude />
        [TestFixtureTearDown]
        public void ShutDown()
        {
            testChassis.IsOnline = false;

            // Test end
            TestOutput("Test Finished!");
        }

        [Test]
        public void T01_FirstTest()
        {
            TestOutput("\n\n*** T01_FirstTest: Min output is: "  + testInstr.MinimumSwitchState + "\n");
            TestOutput("\n\n*** T01_FirstTest: Max output is: " + testInstr.MaximumSwitchState + "\n");


            TestOutput("\n\n*** T01_FirstTest: Select Output 1. ***");
            testInstr.SetDefaultState();

            testInstr.SwitchState = 1;
            Assert.AreEqual(testInstr.SwitchState, 1);
            TestOutput("\n\n*** Output 1 Selected ***");
        }

        [Test]
        public void T02_SecondTest()
        {
            TestOutput("\n\n*** T02_SecondTest: Select Output 3***");
            testInstr.SetDefaultState();

            testInstr.SwitchState = 3;
            Assert.AreEqual(testInstr.SwitchState, 3);

            TestOutput("\n\n*** Output 3 Selected ***");
        }

        [Test]
        public void T03_ThirdTest()
        {
            TestOutput("\n\n*** T03_ThirdTest: Select Last output***");
            testInstr.SetDefaultState();

            testInstr.SwitchState = testInstr.MaximumSwitchState;
            Assert.AreEqual(testInstr.SwitchState, testInstr.MaximumSwitchState);
            TestOutput("\n\n*** Last output port Selected ***");
        }


        #region Private helper fns
        private void TestOutput(string output)
        {
            BugOut.WriteLine(BugOut.WildcardLabel, output);
        }

        private void TestOutput(IInstrument i, string output)
        {
            string outputStr = String.Format("{0}: {1}", i.Name, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }

        private void TestOutput(IChassis c, string output)
        {
            string outputStr = String.Format("{0}: {1}", c.Name, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }
        #endregion


    }

}

using System;
using System.Collections.Generic;
//using Bookham.TestLibrary.ChassisNS;
//using Bookham.TestLibrary.Instruments;
//using Bookham.TestLibrary.InstrTypes;
using Bookham.TestEngine.Framework.Logging;
using System.Text;
using NUnit.Framework;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestSolution.ChassisNS;
using Bookham.TestSolution.Instruments;

namespace TEST.JdsSB
{
    /// <exclude />	
    [TestFixture]
    public class Test_Object
    {
        #region Private data (incl equipment references)
        private Chassis_JdsSB testChassis;

        private Inst_JdsSb_SimpleSwitch testInstr;        
        #endregion

        /// <exclude />
        [TestFixtureSetUp]
        public void Setup()
        {
            // initialise Test Engine logging domain
            Bookham.TestEngine.Framework.Logging.Initializer.Init();
            Bookham.TestEngine.Framework.Logging.BugOut.SetOutputToConsole(true);
            TestOutput("*** Test Harness Initialising ***");

            // create chassis objects
            TestOutput("Creating chassis objects");
            testChassis = new Chassis_JdsSB("MyChassis", "Chassis_JdsSB", "GPIB0::7::INSTR");
            TestOutput(testChassis, "Created OK");

            // create instrument objects            
            TestOutput("Creating instrument objects");
            testInstr = new Inst_JdsSb_SimpleSwitch("JdsSB_1", "Inst_JdsSb_SimpleSwitch", "1", "1", testChassis);
            TestOutput(testInstr, "Created OK");

            // put them online
            TestOutput("Put equipment objects online");
            testChassis.IsOnline = true;
            testChassis.EnableLogging = true;            
            TestOutput(testChassis, "IsOnline set true OK");

            testInstr.IsOnline = true;
            testInstr.EnableLogging = true;
            TestOutput(testInstr, "IsOnline set true OK");            
        }

        /// <exclude />
        [TestFixtureTearDown]
        public void ShutDown()
        {
            TestOutput("Don't forget to take the chassis offline!");
            testChassis.IsOnline = false;

            // Test end
            TestOutput("Test Finished!");
        }

        [Test]
        public void T01_FirstTest()
        {
            TestOutput("\n\n*** T01_FirstTest ***");
            // You can use Assert to check return values from tests
            for (int channel = testInstr.MinimumSwitchState; channel <= testInstr.MaximumSwitchState; channel++)
            {
                testInstr.SwitchState = channel;
                int actualState = testInstr.SwitchState;
                Assert.AreEqual(channel, actualState);
            }
        }

        [Test]
        public void T02_SecondTest()
        {
            TestOutput("\n\n*** T02_SecondTest***");
        }


        #region Private helper fns
        private void TestOutput(string output)
        {
            BugOut.WriteLine(BugOut.WildcardLabel, output);
        }

        private void TestOutput(IInstrument i, string output)
        {
            string outputStr = String.Format("{0}: {1}", i.Name, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }

        private void TestOutput(IChassis c, string output)
        {
            string outputStr = String.Format("{0}: {1}", c.Name, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }
        #endregion


    }

}

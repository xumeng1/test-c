using System;
using System.Collections.Generic;
using Bookham.TestLibrary.ChassisNS;
using Bookham.TestLibrary.Instruments;
using Bookham.TestEngine.Framework.Logging;
using System.Text;
using NUnit.Framework;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;

namespace Chassis_816x_Test
{
    /// <exclude />	
    [TestFixture]
    public class Test_Object
    {
        #region References to the objects to test - Chassis and Instruments
        private Chassis_Burleigh1x00 testChassis;

        private Inst_Burleigh1x00 testWaveMeter;
        #endregion


        #region Constants for use during test.
        // VISA Chassis for where to find the instrument
        const string visaResource =  "GPIB0::04::INSTR"; //"//pai-tx-labj1/GPIB1::20::INSTR"; 
        const string chassisName = "Burleigh WM Chassis";
        const string instr1Name = "Wavemeter";
        string instr1Slot = "2";
        string instr1SubSlot = "1";
        #endregion

        /// <exclude />
        [TestFixtureSetUp]
        public void Setup()
        {
            // initialise Test Engine logging domain
            Bookham.TestEngine.Framework.Logging.Initializer.Init();
            Bookham.TestEngine.Framework.Logging.BugOut.SetOutputToConsole(true);
            TestOutput("*** Test Harness Initialising ***");

            // create chassis objects
            testChassis = new Chassis_Burleigh1x00(chassisName, "Chassis_Burleigh1x00", visaResource);
            TestOutput(chassisName, "Created OK");

            // create instrument objects            
            testWaveMeter = new Inst_Burleigh1x00(instr1Name,
                "Inst_Burleigh1x00", instr1Slot, instr1SubSlot, testChassis);
            TestOutput(instr1Name, "Created OK");

           
            // put them online
            testChassis.IsOnline = true;
            testChassis.EnableLogging = true;
            TestOutput(chassisName, "IsOnline set true OK");
            testWaveMeter.IsOnline = true;
            testChassis.EnableLogging = true;
            TestOutput(instr1Name, "IsOnline set true OK");            
        }

        /// <exclude />
        [TestFixtureTearDown]
        public void ShutDown()
        {
            testChassis.IsOnline = false;

            // Test end
            TestOutput("Test Finished!");
        }

        [Test]
        public void T01_Versions()
        {
            TestOutput("\n\n*** T01_Versions ***");
            TestOutput(chassisName, "Driver: " + testChassis.DriverName + ":" + testChassis.DriverVersion);
            TestOutput(chassisName, "HW: " + testChassis.HardwareIdentity);
            TestOutput(chassisName, "FW: " + testChassis.FirmwareVersion);
            TestOutput(instr1Name, "Driver: " + testWaveMeter.DriverName + ":" + testWaveMeter.DriverVersion);
            TestOutput(instr1Name, "HW: " + testWaveMeter.HardwareIdentity);
            TestOutput(instr1Name, "FW: " + testWaveMeter.FirmwareVersion);            
        }

        [Test]
        public void T02_MeterMode()
        {
            testWaveMeter.SetDefaultState();

            TestOutput("\n\n*** T02_MeterMode ***");
            TestOutput(instr1Name, "Medium: " + testWaveMeter.MeasurementMedium.ToString());
            
            testWaveMeter.MeasurementMedium =  InstType_Wavemeter.Medium.Air;
            Assert.AreEqual(InstType_Wavemeter.Medium.Air, testWaveMeter.MeasurementMedium);

            testWaveMeter.MeasurementMedium = InstType_Wavemeter.Medium.Vacuum;
            Assert.AreEqual(InstType_Wavemeter.Medium.Vacuum, testWaveMeter.MeasurementMedium);            
        }

        [Test]
        public void T03_Measure()
        {
            TestOutput("\n\n*** T03_Measure ***");
            TestOutput(instr1Name, "Power mW:" + testWaveMeter.Power_mW);
            TestOutput(instr1Name, "Power dBm:" + testWaveMeter.Power_dBm);
            TestOutput(instr1Name, "Wavelength nm:" + testWaveMeter.Wavelength_nm);
            TestOutput(instr1Name, "Freq GHz:" + testWaveMeter.Frequency_GHz);            
        }

        #region Private helper fns  
        private void TestOutput(string output)
        {
            BugOut.WriteLine(BugOut.WildcardLabel, output);
        }

        private void TestOutput(string objStr, string output)
        {
            string outputStr = String.Format("{0}: {1}", objStr, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }
        #endregion


    }

}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestEngine.Equipment
//
// Chassis_Wa1x00.cs
//
// Author: T Foster
// Design: As specified in Test Module/Program DD 

using System;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using NationalInstruments.VisaNS;

namespace Bookham.TestLibrary.ChassisNS
{
	#region Public Functions
	
	/// <summary>
	/// Summary description for Template chassis driver.
	/// </summary>
	public class Chassis_Burleigh1x00 : ChassisType_Visa488_2
	{
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="chassisName">Chassis name</param>
		/// <param name="driverName">Chassis driver name</param>
		/// <param name="resourceStringId">Resource data</param>
		public Chassis_Burleigh1x00(string chassisName, string driverName, string resourceStringId)
			: base (chassisName, driverName, resourceStringId)
		{
			// Configure valid hardware information

			// Add details of WA-1100 chassis
			ChassisDataRecord wa1100Data = new ChassisDataRecord(
				"BurleighInstruments WA-1100",		// hardware name 
				"0",								// minimum valid firmware version 
				"01.04.00");						// maximum valid firmware version 
			ValidHardwareData.Add("Wa1100", wa1100Data);

			// Add details of WA-1600 chassis
			ChassisDataRecord wa1600Data = new ChassisDataRecord(
				"BurleighInstruments WA-1600",		// hardware name 
				"0",								// minimum valid firmware version 
				"01.04.00");						// maximum valid firmware version 
			ValidHardwareData.Add("Wa1600", wa1600Data);

            // This chassis doesn't support reading the Status Byte in low level GPIB.
            // Inform the base class so it can adapt its calls
            this.ErrorCheckUse_488_2_Command = true;
		}


		/// <summary>
		/// Firmware version
		/// </summary>
		public override string FirmwareVersion
		{
			get
			{
				// Read the chassis ID string and split the comma seperated fields
				string[] idn = Query_Unchecked("*IDN?", null).Split(',');		
				
				// Return the firmware version in the 3rd comma seperated field
				return idn[2].Trim();
			}
		}

		/// <summary>
		/// Hardware identity string
		/// </summary>
		public override string HardwareIdentity
		{
			get
			{
				// Read the chassis ID string and split the comma seperated fields
				string[] idn = Query_Unchecked("*IDN?", null).Split(',');		
		
				// Return field 1, the manufacturer name and field 2, the model number
				return idn[0] + idn[1];
			}
		}

        /// <summary>
        /// Setup the chassis as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                if (value) // if setting online                
                {
                    // NOTHING TO DO
                }

                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // clear the status registers
                    this.Write_Unchecked("*CLS", null);
                    // Set up Chassis standard event register mask
                    // 4 = Query error, 8 = Device error, 16 = execution error, 32 = command error
                    this.Write_Unchecked("*ESE 60", null);
                    // clear the status registers
                    this.Write("*CLS", null);

                    this.VisaSession.Timeout = 5000;
                }
            }
        }

        /// <summary>
        /// New overload for write - *OPC? doesn't work!
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="instr"></param>
        public new void Write(string cmd, Instrument instr)
        {
            base.Write_Unchecked(cmd , instr);
            StatusByteFlags stb = base.StatusByte488_2_Command;
            if (this.IsStandardEvent(stb))
            {
                this.RaiseChassisException(cmd, instr);
            }
        }
		#endregion
	}
	
}

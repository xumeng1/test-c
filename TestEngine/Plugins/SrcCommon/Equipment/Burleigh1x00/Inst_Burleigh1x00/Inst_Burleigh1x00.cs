// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestEngine.Equipment
//
// Inst_Wa1x00.cs
//
// Author: T Foster
// Design: As specified in Test Module/Program DD 

using System;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisTypes;
using Bookham.TestLibrary.ChassisNS;

namespace Bookham.TestLibrary.Instruments
{
	/// <summary>
	/// Burleigh WA1100 / 1600 wavemeter driver
	/// </summary>
	public class Inst_Burleigh1x00 : InstType_Wavemeter
	{
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="instrumentName">Instrument name</param>
		/// <param name="driverName">Instrument driver name</param>
		/// <param name="slotId">Slot ID for the instrument</param>
		/// <param name="subSlotId">Sub Slot ID for the instrument</param>
		/// <param name="chassis">Chassis through which the instrument communicates</param>
		public Inst_Burleigh1x00(string instrumentName, string driverName, string slotId, string subSlotId, Chassis chassis)
			: base (instrumentName, driverName, slotId, subSlotId, chassis)
		{
			// Configure valid hardware information

			// Add WA-1100 details
			InstrumentDataRecord wa1100Data = new InstrumentDataRecord(	
				"BurleighInstruments WA-1100",					// hardware name 
				"0",											// minimum valid firmware version 
				"01.04.00");									// maximum valid firmware version 
			wa1100Data.Add("MinWavelength_nm","700");			// minimum wavelength
			wa1100Data.Add("MaxWavelength_nm","1700");			// maximum wavelength
			ValidHardwareData.Add("Wa1100", wa1100Data);

			// Add WA-1600 details
			InstrumentDataRecord wa1600Data = new InstrumentDataRecord(	
				"BurleighInstruments WA-1600",					// hardware name 
				"0",											// minimum valid firmware version 
				"01.04.00");									// maximum valid firmware version 
			wa1600Data.Add("MinWavelength_nm","700");			// minimum wavelength
			wa1600Data.Add("MaxWavelength_nm","1700");			// maximum wavelength
			ValidHardwareData.Add("Wa1600", wa1600Data);
						
			// Configure valid chassis information
			// Add Chassis_Wa1x00 chassis details
			InstrumentDataRecord wa1x00ChassisData = new InstrumentDataRecord(
                "Chassis_Burleigh1x00",								// chassis driver name  
				"0",											// minimum valid chassis driver version  
				"2.0.0.0");										// maximum valid chassis driver version
			ValidChassisDrivers.Add("Chassis_Burleigh1x00", wa1x00ChassisData);

			// Initialise the local chassis reference cast to the actual chassis type
			instrumentChassis = (Chassis_Burleigh1x00) base.InstrumentChassis;
		}


		#region Public Instrument Methods and Properties
		/// <summary>
		/// Unique hardware identification string
		/// </summary>
		public override string HardwareIdentity
		{
			get
			{
                return instrumentChassis.HardwareIdentity;
			}
		}

		/// <summary>
		/// Hardware firmware version
		/// </summary>
		public override string FirmwareVersion
		{
			get
			{
                return instrumentChassis.FirmwareVersion;
            }
		}

		/// <summary>
		/// Configures the instrument into a default state.
		/// </summary>
		public override void SetDefaultState()
		{
			// Send a reset, *RST
			instrumentChassis.Write("*RST", this);

            //Turn averaging on by default - other wise readings too erratic.
            instrumentChassis.Write("SENS:AVER 8", this);
		}

		#endregion

		#region Public Wavemeter InstrumentType Methods and Properties

		/// <summary>
		/// Return frequency in GHz
		/// </summary>
		public override double Frequency_GHz 
		{
			get 
			{
				// Read frequency in GHz
                // ignore the first reading - it was started before this method was called
                // I.E. it isn't a guaranteed valid new reading
				string rtn = instrumentChassis.Query(":MEAS:FREQ?", this);
                // take the second reading - it IS now guaranteed to be a new reading
                rtn = instrumentChassis.Query(":MEAS:FREQ?", this);
				// Convert and return result
				return Convert.ToDouble(rtn);
			}
		}

		/// <summary>
		/// Return wavelength in nm
		/// </summary>
		public override double Wavelength_nm 
		{
			get 
			{				
				// Read wavelength in nm
                // ignore the first reading - it was started before this method was called
                // I.E. it isn't a guaranteed valid new reading
                string rtn = instrumentChassis.Query(":MEAS:WAV?", this);
                // take the second reading - it IS now guaranteed to be a new reading
                rtn = instrumentChassis.Query(":MEAS:WAV?", this);

				// Cast and return result
				return Convert.ToDouble(rtn);
			}
		}
		
		/// <summary>
		/// Returns the signal input power level in dBm
		/// </summary>
		public override double Power_dBm
		{
			get
			{
				// Set power to dbm
				instrumentChassis.Write(":UNIT:POW DBM", this);

				// Read power in dBm
				string rtn = instrumentChassis.Query(":MEAS:POW?", this);
                rtn = instrumentChassis.Query(":MEAS:POW?", this);

				// Cast and return result
				return Convert.ToDouble(rtn);
			}
		}

		/// <summary>
		/// Returns the signal input power level in mW
		/// </summary>
		public override double Power_mW
		{
			get
			{
				// Set power to mW
				instrumentChassis.Write(":UNIT:POW MILL", this);

				// Read power in mW
				string rtn = instrumentChassis.Query(":MEAS:POW?", this);
                rtn = instrumentChassis.Query(":MEAS:POW?", this);

				// Cast and return result
				return Convert.ToDouble(rtn);
			}
		}

		/// <summary>
		/// Reads/sets the measurement medium
		/// </summary>
		public override Medium MeasurementMedium 
		{
			get
			{
				// Query medium
				string rtn = instrumentChassis.Query(":SENS:MED?", this);

				// Convert to enum 
				Medium medm;
				switch (rtn)
				{
					case "VACUUM": medm = Medium.Vacuum; break;
					case "AIR": medm = Medium.Air; break;
					default: throw new InstrumentException("Unrecognised measurement medium '" + rtn + "'");
				}

				// Return result
				return medm;
			}

			set 
			{
				// Build string
				string medm;
				switch (value)
				{
					case Medium.Vacuum: medm = "VACUUM"; break;
					case Medium.Air: medm = "AIR"; break;
					default: throw new InstrumentException("Unrecognised measurement medium '" + value.ToString() + "'");
				}

				// Write to instrument
				instrumentChassis.Write(":SENS:MED " + medm, this);
			}
		}

		#endregion


		#region Private Data

		// Chassis reference
		private Chassis_Burleigh1x00 instrumentChassis;

		#endregion
	}
}

using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace Bookham.TestLibrary.ChassisNS
{    
    /// <summary>
    /// Import DLL functions - as described in the 306 manual, with types translated to C#
    /// Names and function are the same!
    /// </summary>
    internal class PXIT306Import
    {
        [DllImport("pxit.dll")]
        internal static extern Double PXITVersion();

        [DllImport("pxit.dll")]
        internal static extern Int16 PXITVerifyPxitCard(IntPtr instr);

        [DllImport("pxit.dll")]
        internal static extern Int16 PXITGetCardType(IntPtr instr);

        [DllImport("pxit.dll")]
        internal static extern Int16 PXITGetCardVersion(IntPtr instr);

        [DllImport("pxit306.dll")]
        internal static extern Double PXIT306Version();

        [DllImport("pxit306.dll")]
        internal static extern Int16 PXIT306InitialiseModule(IntPtr instr);

        [DllImport("pxit306.dll")]
        internal static extern Int16 PXIT306ReadElectricalCurrent(IntPtr instr,
            ref byte rangeA, ref byte rangeB, out Double iA, out Double iB);     
                          
        [DllImport("pxit306.dll")]
        internal static extern Int16 PXIT306SetBiasVoltages(IntPtr instr,
            Double valueA, Double valueB);

        [DllImport("pxit306.dll")]
        internal static extern Int16 PXIT306UnInitialiseModule(IntPtr instr);
    }
}

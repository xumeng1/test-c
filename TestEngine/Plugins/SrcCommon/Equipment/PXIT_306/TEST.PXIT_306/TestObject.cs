using System;
using System.Collections.Generic;
using Bookham.TestEngine.Framework.Logging;
using System.Text;
using NUnit.Framework;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisNS;
using Bookham.TestLibrary.Instruments;

namespace TEST.PXIT_306
{
    /// <exclude />	
    [TestFixture]
    public class Test_Object
    {
        #region References to the objects to test - Chassis and Instruments
        private Chassis_PXIT306 testChassis;

        private Inst_PXIT306EE testSrc_1;        
        #endregion


        #region Constants for use during test.
        // VISA Chassis for where to find the instrument
        const string visaResource = "PXI4::12::INSTR"; 
        const string chassisName = "Chassis";
        const string inst1Name = "SrcChannel";
        #endregion

        /// <exclude />
        [TestFixtureSetUp]
        public void Setup()
        {
            // initialise Test Engine logging domain
            Bookham.TestEngine.Framework.Logging.Initializer.Init();
            Bookham.TestEngine.Framework.Logging.BugOut.SetOutputToConsole(true);
            TestOutput("*** Test Harness Initialising ***");

            // create chassis objects
            testChassis = new Chassis_PXIT306(chassisName, "Chassis_PXIT306", visaResource);
            TestOutput(chassisName, "Created OK");

            // create instrument objects            
            testSrc_1 = new Inst_PXIT306EE(inst1Name, "Inst_PXIT306EE", "1", "", testChassis);
            TestOutput(inst1Name, "Created OK");

            // put them online
            testChassis.IsOnline = true;
            TestOutput(chassisName, "IsOnline set true OK");
            testSrc_1.IsOnline = true;
            TestOutput(inst1Name, "IsOnline set true OK");            
        }

        /// <exclude />
        [TestFixtureTearDown]
        public void ShutDown()
        {
            // TODO...
            TestOutput("Don't forget to take the chassis offline!");
            testChassis.IsOnline = false;

            // Test end
            TestOutput("Test Finished!");
        }

        [Test]
        public void T01_Versions()
        {
            TestOutput("\n\n*** T01_Versions ***");
            TestOutput(testChassis.HardwareIdentity + ":" + testChassis.FirmwareVersion);
            TestOutput(testSrc_1.HardwareIdentity + ":" + testSrc_1.FirmwareVersion);            
        }

        [Test]
        public void T02_SetBias()
        {
            TestOutput("\n\n*** T02_SetBias ***");
            TestOutput(inst1Name, "Bias Volts: " + testSrc_1.VoltageSetPoint_Volt);
            testSrc_1.VoltageSetPoint_Volt = 0.0;
            Assert.AreEqual(0.0, testSrc_1.VoltageSetPoint_Volt, 0.001);
            testSrc_1.VoltageSetPoint_Volt = -0.5;
            Assert.AreEqual(-0.5, testSrc_1.VoltageSetPoint_Volt, 0.001);
            testSrc_1.VoltageSetPoint_Volt = 0.0;
            Assert.AreEqual(0.0, testSrc_1.VoltageSetPoint_Volt, 0.001);            
        }

        [Test]
        public void T03_ReadPhotoCurrent()
        {
            TestOutput("\n\n*** T03_ReadPhotoCurrent ***");
            TestOutput(inst1Name, "Photocurr (A): " + testSrc_1.CurrentActual_amp);            
        }

        #region Private helper fns
        private void TestOutput(string output)
        {
            BugOut.WriteLine(BugOut.WildcardLabel, output);
        }

        private void TestOutput(string objStr, string output)
        {
            string outputStr = String.Format("{0}: {1}", objStr, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }
        #endregion


    }

}

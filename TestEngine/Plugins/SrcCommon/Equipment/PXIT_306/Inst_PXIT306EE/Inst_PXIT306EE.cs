// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_PXIT306EE.cs
//
// Author: Paul.Annetts, 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisNS;

namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// Instrument driver for a Source/Measure meter in a PXIT 306 card.
    /// Models the use of a Keithley source measure unit for photocurrent measurement, or some other
    /// high quality unit.
    /// </summary>
    public class Inst_PXIT306EE : InstType_ElectricalSource
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_PXIT306EE(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord instrVariant1 = new InstrumentDataRecord(
                "PXIT 306",				// hardware name 
                "0",  			// minimum valid firmware version 
                "1");			// maximum valid firmware version 
            ValidHardwareData.Add("InstrVariant1", instrVariant1);

            // Configure valid chassis driver information
            // TODO: Update with useful key name, real chassis name and versions
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_PXIT306",								// chassis driver name  
                "0",									// minimum valid chassis driver version  
                "9999");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("MyChassis", chassisInfo);

            // get the channel number (slot) 1 or 2.
            this.channel = byte.Parse(slotInit);
            if ((this.channel < 1) || (this.channel > 2))
            {
                throw new InstrumentException("Invalid channel number (slot) :" + slotInit);
            }

            // initialise this instrument's chassis
            // TODO - cast to the appropriate type.
            // don't forget to change the private variable type too.
            this.instrumentChassis = (Chassis_PXIT306)chassisInit;
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                return instrumentChassis.FirmwareVersion;
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                return instrumentChassis.HardwareIdentity;
            }
        }

        #endregion

        #region Private data
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_PXIT306 instrumentChassis;

        /// <summary>
        /// Which channel (1 or 2)
        /// </summary>
        private byte channel;
        #endregion

        #region Electrical source
        /// <summary>
        /// Set default state
        /// </summary>
        public override void SetDefaultState()
        {
            // do nothing - don't want to disturb delicate PXI set-up.
        }
        
        /// <summary>
        /// Actual current - average of 10 measurements
        /// </summary>
        public override double CurrentActual_amp
        {
            get 
            {
                double sumCurrent_A = 0.0;
                for (int ii = 0; ii < 10; ii++)
                {
                    double curr_A = instrumentChassis.ReadCurrentAutoRange_A(this.channel);
                    sumCurrent_A += curr_A;
                }
                double avgCurrent_A = sumCurrent_A / 10;

                return avgCurrent_A;
            }
        }

        /// <summary>
        /// Can't set current compliance voltage
        /// </summary>
        public override double CurrentComplianceSetPoint_Amp
        {
            get
            {
                throw new InstrumentException("The method or operation is not available.");
            }
            set
            {
                throw new InstrumentException("The method or operation is not available.");
            }
        }

        /// <summary>
        /// Can't set current set point
        /// </summary>
        public override double CurrentSetPoint_amp
        {
            get
            {
                throw new InstrumentException("The method or operation is not available.");
            }
            set
            {
                throw new InstrumentException("The method or operation is not available.");
            }
        }

        /// <summary>
        /// Output enabled - always on!
        /// </summary>
        public override bool OutputEnabled
        {
            get
            {
                return true;
            }
            set
            {
                //ignore
            }
        }

        /// <summary>
        /// Can't get this info
        /// </summary>
        public override double VoltageActual_Volt
        {
            get { throw new InstrumentException("The method or operation is not available."); }
        }

        /// <summary>
        /// Can't get this info
        /// </summary>
        public override double VoltageComplianceSetPoint_Volt
        {
            get
            {
                throw new InstrumentException("The method or operation is not available.");
            }
            set
            {
                throw new InstrumentException("The method or operation is not available.");
            }
        }

        /// <summary>
        /// Voltage set-point: PXIT driver makes this write-only, so cache it internally!
        /// </summary>
        public override double VoltageSetPoint_Volt
        {
            get
            {
                double biasV = instrumentChassis.GetBiasCurrentCache_V(this.channel);
                return biasV;
            }
            set
            {
                instrumentChassis.SetBiasCurrent_V(channel, value);
            }
        }

        #endregion
    }
}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Chassis
//
// Chassis_JDSSAZ349.cs
//
// Author: joseph.olajubu, 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestLibrary.ChassisTypes;
using System.Threading;

namespace Bookham.TestLibrary.ChassisNS
{
    /// <summary>
    /// Chassis Driver for JDS SA-Z349 8x1x2 Optical Switch 
    /// </summary>
    public class Chassis_JDSSAZ349 : ChassisType_Visa
    {
        #region Private Data
        private bool visaSessionInitialised;
        #endregion

        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="chassisNameInit">Chassis name</param>
        /// <param name="driverNameInit">Chassis driver name</param>
        /// <param name="resourceString">Resource string for communicating with the chassis (for VISA
        /// chassis, this is the VISA resource string)</param>
        public Chassis_JDSSAZ349(string chassisNameInit, string driverNameInit,
            string resourceString)
            : base(chassisNameInit, driverNameInit, resourceString)
        {
            // Setup expected valid hardware variants 
            ChassisDataRecord chassisData = new ChassisDataRecord(
                "JDS FITEL Inc., SA Switch",			// hardware name 
                "0",			                    // minimum valid firmware version 
                "1.10");		                    // maximum valid firmware version 
            ValidHardwareData.Add("ChassisData", chassisData);
        }
        #endregion

        #region Chassis overrides
        /// <summary>
        /// Firmware version of this chassis.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // Read the chassis ID string. This must be done using a Write, followed 
                // by a read, because the instrument is too slow to handle VISA Query. 
                Write_Unchecked("IDN?", null);
                Thread.Sleep(500);
                string[] idn = Read_Unchecked(null).Split(','); ;

                // Return the firmware version in the 3rd comma seperated field
                return idn[2].Trim().Remove(0, 1);
            }
        }

        /// <summary>
        /// Hardware Identity of this chassis.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                //Must set up the termination character and extended timeout here, as this the only 
                //opportunity during initialisation to do so, where the VISA Session is active, and no 
                //other commands have been sent yet.
                if (!visaSessionInitialised)
                {
                    base.VisaSession.TerminationCharacterEnabled = true;
                    base.VisaSession.TerminationCharacter = 10;
                    base.VisaSession.Timeout = 6000;
                    visaSessionInitialised = true;                
                }

                // Read the chassis ID string This must be done using a Write, followed 
                // by a read, because the instrument is too slow to handle VISA GPIB Query. 
                Write_Unchecked("IDN?", null);
                Thread.Sleep(500);
                string[] idn = Read_Unchecked(null).Split(','); ;
                
                // Return fields 1 & 2, the manufacturer name and equipment name.
                return idn[0] + "," + idn[1];
            }
        }

        /// <summary>
        /// Setup the chassis as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                if (value) // if setting online                
                {
                    // TODO: custom setup for this chassis before it goes online
                    // E.g. setup RS232 variables
                }

                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // TODO: custom setup for this chassis after it goes online, 
                    // e.g. Standard Error register on a 488.2 instrument

                    //// 4 = Query error, 8 = Device error, 16 = execution error, 32 = command error
                    //this.StandardEventRegisterMask = 4 + 8 + 16 + 32;
                    //// clear the status registers
                    //this.Write("*CLS", null);                    
                }
            }
        }
        #endregion

        /// <summary>
        /// Writes a command to the chassis and will wait for completion and check status
        /// byte and other registers for error conditions.
        /// </summary>
        /// <param name="command">Command to send</param>
        /// <param name="i">Which instrument is this from?</param>
        /// <remarks>Uses the chassis' default asynchronous set and "no error check" setting</remarks>
        public void Write(string command, Instrument i)
        {
            this.Write(command, i, false, true);
        }

        /// <summary>
        /// Writes a command to the chassis. Dependent on options given this will wait 
        /// for completion and check status
        /// byte and other registers for error conditions.
        /// </summary>
        /// <param name="command">Command to send</param>
        /// <param name="i">Which instrument is this from?</param>
        /// <param name="asyncSet">Set asynchronously, therefore also don't check for errors</param>
        /// <param name="errorCheck">If async set to false, then this parameter determines
        /// whether errors are checked for</param>
        public void Write(string command, Instrument i, bool asyncSet, bool errorCheck)
        {
            //All commands must use a <CR><LF> termination. The <LF> is handled automatically 
            //by Visa, having been enabled at initialisation when the Hardware Identity 
            //was read when putting the Chassis online.
            this.Write_Unchecked(command + "\n", i);

            //Wait 100ms for the command to take effect (arbitary).
            Thread.Sleep(100);
            base.VisaSession.Clear();
        }

        /// <summary>
        /// Sends a command to the chassis and waits for a response. Dependent on 
        /// arguments, will check the status byte and other registers for error 
        /// conditions.
        /// </summary>
        /// <param name="command">Command to send</param>
        /// <param name="i">Which instrument is this from?</param>
        /// <returns>string response</returns>
        public string Query(string command, Instrument i)
        {

            //All commands must use a <CR><LF> termination. The <LF> is handled automatically 
            //by Visa, having been enabled at initialisation when the Hardware Identity 
            //was read when putting the Chassis online.
            string resp = this.Query_Unchecked(command + "\n", i);
            //Wait 100ms for the command to take effect (arbitary).
            Thread.Sleep(100);
            base.VisaSession.Clear();
            return resp;
        }
    }
}

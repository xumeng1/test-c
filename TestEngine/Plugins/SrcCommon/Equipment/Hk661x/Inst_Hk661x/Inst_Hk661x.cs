// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestLibrary.Instruments
//
// Inst_Hk661x.cs
//
// Author: Evelyn.Guo, 2010.12
// Design: As specified in Hk661x driver design DD 

using System;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestLibrary.ChassisNS;
using Bookham.TestLibrary.InstrTypes;

namespace Bookham.TestLibrary.Instruments
{
	/// <summary>
	/// Instrument driver for Hk6611R SMU
	/// Provides basic source functions only for 'ElectricalSource' instrument type
	/// </summary>
	public class Inst_Hk661x : InstType_ElectricalSource
	{
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="instrumentName">Instrument name</param>
		/// <param name="driverName">Instrument driver name</param>
		/// <param name="slotId">Slot ID for the instrument</param>
		/// <param name="subSlotId">Sub Slot ID for the instrument</param>
		/// <param name="chassis">Chassis through which the instrument communicates</param>
        public Inst_Hk661x(string instrumentName, string driverName, string slotId, string subSlotId, Chassis chassis)
			: base (instrumentName, driverName, slotId, subSlotId, chassis)
		{
			// Configure valid hardware information
            //
            //Note: In this case slot refers to the same thing as channel
            //
            // Add Inst_Hk661x details
            //InstrumentDataRecord Hk6611rData = new InstrumentDataRecord(
            //    "HK6611R",			// hardware name 
            //    "0",				// minimum valid firmware version 
            //    "xxxxxxx");				// maximum valid firmware version 

            InstrumentDataRecord Hk6611rData = new InstrumentDataRecord(
                "M8811",			// hardware name 
                "0",				// minimum valid firmware version 
                "xxxxxxx");				// maximum valid firmware version 


            Hk6611rData.Add("MaxOutputs", "2");

            ValidHardwareData.Add("Hk6611R", Hk6611rData);
           

			// Configure valid chassis information
			// Add Hk661x chassis details
			InstrumentDataRecord Hk661xChassisData = new InstrumentDataRecord(
                "Chassis_Hk661x",	// chassis driver name  
				"0",				// minimum valid chassis driver version  
				"xxxxxxxx");			// maximum valid chassis driver version
            ValidChassisDrivers.Add("Chassis_Hk661x", Hk661xChassisData);

			// Initialise the local chassis reference cast to the actual chassis type
            instrumentChassis = (Chassis_Hk661x)base.InstrumentChassis;

            //ensure safe mode operation by default
            safeModeOperation = true;
		}


		#region Public Instrument Methods and Properties

        /// <summary>
        /// We need to override the is online function so that we can check the indicated
        /// slot is in fact correct
        /// </summary>
        /// 
        public override bool IsOnline
        {
            get
            {
                return true;
            }
            set
            {                
               instrumentChassis.IsOnline = value;   
            }
        }


		/// <summary>
		/// Unique hardware identification string
		/// </summary>
		public override string HardwareIdentity
		{
			get
			{
				// Read the full IDN? string which has 4 comma seperated substrings
                string hid = instrumentChassis.HardwareIdentity;
				return hid;
			}
		}

		/// <summary>
		/// Hardware firmware version
		/// </summary>
		public override string FirmwareVersion
		{
			get
			{				
				string fv = instrumentChassis.FirmwareVersion;

				// Return firmware version
				return fv;
			}
		}

		/// <summary>
		/// Configures the instrument into a default state.
		/// 
		/// </summary>
		public override void SetDefaultState()
		{
			// Set factory default
            //instrumentChassis.Write("CLR", this);
            //this.CheckForAnyErrors();
            this.OutputEnabled = false;
		}

		#endregion

		#region Public ElectricalSource InstrumentType Methods and Properties

		/// <summary>
		/// Sets/returns the output state
		/// </summary>
		public override bool OutputEnabled
		{
			get 
			{
				// Query the output state
                //string rtn = instrumentChassis.EmptyString_QueryUnchecked("[:]OUTP?\n", this);
                instrumentChassis.Write(":OUTP?\n", this);
                System.Threading.Thread.Sleep(200);
                string rtn = instrumentChassis.Read(this); 
                //this.CheckForAnyErrors();

                if (rtn.Contains("\n"))
                {
                    rtn = rtn.Remove(1, 1);
                }
                // Return bool value
                if (rtn == trueStr)
                {
                    return (true);
                }
                else if (rtn == falseStr)
                {
                    return (false);
                }
                else
                {
                    throw new InstrumentException("Output enabled query has returned unrecognised chars: " + rtn);
                }
                
			}
			set 
			{
				// Convert bool value to string
                string boolVal = value ? trueStr : falseStr;
                string cmd = String.Format("OUTP" + " " + boolVal + "\n");

                //// Set output                
                instrumentChassis.Write(cmd, this);                
                this.CheckForAnyErrors();
			}
		}

		/// <summary>
		/// Sets/returns the current (Iset) setpoint level
		/// </summary>
		public override double CurrentSetPoint_amp
		{
            get
            {
                // Read current in amps
                string cmd = "CURR?\n";
                string meas = instrumentChassis.EmptyString_QueryUnchecked(cmd, this);               
                
                //this.CheckForAnyErrors();

                // convert to double
                double rtn = getDblFromString(cmd, meas);

                // Return
                return rtn;
            }
			
			set
			{
				// Set the source level in amps
                System.Threading.Thread.Sleep(500);
				instrumentChassis.Write("CURR" + " " + value.ToString() + "\n", this);
                //this.CheckForAnyErrors();
			}
		}

        /// <summary>
        /// Gets the actual measured output current level
        /// </summary>
        public override double CurrentActual_amp
        {
            get
            {
                // Read current in amps
                string cmd = "[:]MEAS:CURR?\n";
                string meas = instrumentChassis.EmptyString_QueryUnchecked(cmd, this);
                this.CheckForAnyErrors();

                // convert to double
                double rtn = getDblFromString(cmd, meas);

                // Return
                return rtn;
            }
        }


        /// <summary>
        /// Sets/returns the voltage level
        /// </summary>
        public override double VoltageSetPoint_Volt
        {
            get
            {
                // Read voltage in Volts
                string cmd = "VOLT?\n";
                string meas = instrumentChassis.EmptyString_QueryUnchecked(cmd, this);                

                // convert to double
                double rtn = getDblFromString(cmd, meas);

               // Return;
                return rtn;
            }

            set
            {
                // Set voltage  source level in volts
                System.Threading.Thread.Sleep(500);
                instrumentChassis.Write("VOLT" + " " + value.ToString() + "\n", this);            
                //this.CheckForAnyErrors();
            }
        }


        /// <summary>
        /// Returns the measured voltage output level
        /// </summary>
        public override double VoltageActual_Volt
        {
            get
            {
                // Read voltage in Volts
                System.Threading.Thread.Sleep(500);
                string cmd = "MEAS:VOLT?\n";
                string meas = instrumentChassis.EmptyString_QueryUnchecked(cmd, this);
                this.CheckForAnyErrors();

                // convert to double
                double rtn = getDblFromString(cmd, meas);

                // Return
                return rtn;
            }
        }

        /// <summary>
        /// Sets/returns the compliance voltage set point
        /// </summary>
		public override double VoltageComplianceSetPoint_Volt
        {
            set
            {
                System.Threading.Thread.Sleep(500);
                instrumentChassis.Write("VOLT:PROT" + " " + value.ToString().Trim() + "\n", this);
                //this.CheckForAnyErrors();
            }
            get
            {
                // Read voltage compliance in volts
                string cmd = "VOLT:PROT?\n";
                string meas = instrumentChassis.EmptyString_QueryUnchecked(cmd, this);
               
                // convert to double
                double rtn = getDblFromString(cmd, meas);
                
                // Return
                return rtn;

            }
        }


        /// <summary>
        /// Gets / sets whether we are in safe mode of operation, 
        /// we are in this mode by default
        /// </summary>
        public bool SafeModeOperation
        {
            get
            {
                return (safeModeOperation);
            }
            set
            {
                safeModeOperation = value;
            }
        }

		#endregion

        #region Private Methods


        /// <summary>
        /// This function does two checks, first it will call the chassis level checks, ie are there any errors
        /// Second, it checks for slot specific errors, ie status registers 
        /// </summary>
        private void CheckForAnyErrors()
        {
            if (SafeModeOperation)
            {
                instrumentChassis.CheckErrorStatus();
                //this.CheckForSlotSpecificErrors();
            }
        }


        #region no use
        /// <summary>
        /// This is checking the registers related to slot specific operations, and throwing
        /// exceptions based on the known error flags.
        /// </summary>
        private void CheckForSlotSpecificErrors()
        {
            byte checkstatusbits = overVoltageProtection | overTemperatureProtection | overCurrentProtection;
            string statusRegister = instrumentChassis.EmptyString_QueryUnchecked("STS? " + this.Slot, this);
            
            byte accumulatedStatusRegister=0;
            string errString = "";
            try
            {
                accumulatedStatusRegister = Convert.ToByte(statusRegister);
            }
            catch (SystemException e)
            {
                errString = "Bad status register: " + statusRegister;
            }
            

            //do we have any flags set whatsoever
            if ((accumulatedStatusRegister & checkstatusbits) != 0)
            {

                //If overVoltageProtection set then lets try and reset the over voltage,
                if ((accumulatedStatusRegister & overVoltageProtection) != 0)
                {
                    //must remove error condition before resetting flag
                    instrumentChassis.Write("VSET " + this.Slot + ", 0.0 ", this);
                    instrumentChassis.Write("OVRST" + this.Slot, this);
                    errString += "OverVoltageProtection flagged" + "\n";
                }

                //If overCurrentProtection set then lets try and reset over current,
                if ((accumulatedStatusRegister & overCurrentProtection) != 0)
                {
                    //must remove error condition before resetting flag
                    instrumentChassis.Write("ISET " + this.Slot + ", 0.0 ", this);
                    instrumentChassis.Write("OCRST" + this.Slot, this);
                    errString += "OverCurrentProtection flagged" + "\n";
                }

                //If overTemperatureProtection set, we can't do anything other than report it.
                if ((accumulatedStatusRegister & overTemperatureProtection) != 0)
                {
                    errString += "OverTemperatureProtection flagged" + "\n";
                }
                
                throw new InstrumentException("Accumulated Status Register indicates error " + accumulatedStatusRegister.ToString() + " for slot " +
                    this.Slot + "\n" + errString);
            }


            //this one doesn't always indicate an error, but potentially it has just changed our current or voltage setting.
            // I will raise as an exception, and user should handle accordingly in their code.
            // We need to prove this one with a 6624 type instrument.
            
            //checkstatusbits = coupledParameter;
            //if ((accumulatedStatusRegister & checkstatusbits) != 0)
            //{
            //    throw new RangeChangeException("Accumulated Status Register indicates coupled parameter bit is set to " + accumulatedStatusRegister + " for slot " + this.Slot);
            //}
        
        }

        #endregion


        /// <summary>
        /// Helper function to convert a string to a double, catching any errors that occur.
        /// </summary>
        /// <param name="command">Command that was sent</param>
        /// <param name="response">Response to convert</param>
        /// <returns>The double value</returns>
        private double getDblFromString(string command, string response)
        {
            try
            {
                return Double.Parse(response);
            }
            catch (SystemException e)
            {
                throw new InstrumentException("Invalid response to '" + command +
                    "' , was expecting a double: " + response,
                    e);
            }
        }

        
        #endregion


        #region Private Data



        // Chassis reference
        private Chassis_Hk661x instrumentChassis;

		// Bool values
		private const string falseStr = "0";
		private const string trueStr = "1";

        //status register bytes
        private const byte overVoltageProtection = 8;
        private const byte overTemperatureProtection = 16;
        private const byte overCurrentProtection = 64;
        private const byte coupledParameter = 128;

        private bool safeModeOperation;
		#endregion

        #region Unsupported methods

        /// <summary>
        /// Sets/returns the compliance current setpoint
        /// </summary>
        public override double CurrentComplianceSetPoint_Amp
        {
            get
            {
                return CurrentActual_amp;
            }
            set
            {
                CurrentSetPoint_amp = value;
            }
        }

        #endregion
    }
}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestLibrary.ChassisNS
//
// Chassis_Hk661x.cs
//
// Author: Evelyn.Guo, 2010.12
// Design: As specified in Hk661x Driver DD 

using System;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;
using Bookham.TestEngine.PluginInterfaces.Instrument;

namespace Bookham.TestLibrary.ChassisNS
{
		
	/// <summary>
	/// The chassis Hk661x driver class, where x indicates 1 to 9.
	/// </summary>
    public class Chassis_Hk661x : ChassisType_Serial
    {
        
        #region Public Functions

        /// <summary>
		/// Constructor
		/// </summary>
		/// <param name="chassisName">Chassis name</param>
		/// <param name="driverName">Chassis driver name</param>
		/// <param name="resourceStringId">Resource data</param>
        public Chassis_Hk661x(string chassisName, string driverName, string resourceStringId)
			: base (chassisName, driverName, resourceStringId)
		{
			// Configure valid hardware information
			
			// Add details of Hk661x chassis
            //ChassisDataRecord Hk6611RData = new ChassisDataRecord(
            //    "HK6611R",		// hardware name 
            //    "0",										// minimum valid firmware version 
            //    "xxxxxxx");										// maximum valid firmware version 
            //ValidHardwareData.Add("Hk6611R", Hk6611RData);

            // Add details of Hk661x chassis
            ChassisDataRecord Hk6611RData = new ChassisDataRecord(
                "M8811",		// hardware name 
                "0",										// minimum valid firmware version 
                "xxxxxxx");										// maximum valid firmware version 
            ValidHardwareData.Add("Hk6611R", Hk6611RData);
        }


		/// <summary>
		/// Firmware version
		/// </summary>
		public override string FirmwareVersion
		{
			get
			{
				// Read the chassis firmware string 
                string[] idn = this.EmptyString_QueryUnchecked("*IDN?\n", null).Split(',');			
				return idn[3].Trim();
			}
		}

		/// <summary>
		/// Hardware identity string
		/// </summary>
		public override string HardwareIdentity
		{
			get
			{
				// Read the chassis ID string
				string[] idn = this.EmptyString_QueryUnchecked("*IDN?\n", null).Split(',');			
				return idn[1];
			}
		}


        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                base.IsOnline = value;
                if (value)
                {
                    //base.IsOnline = value;
                    // set remote control, lock the keyboard
                    this.Write("[:]SYST:REM\n", null);
                }
            }
        }

        /// <summary>
        /// Checks the error register status (slot independent)
        /// </summary>
        public void CheckErrorStatus()
        {
            //the error register is independent of slot
            string[] errorRegister = this.EmptyString_QueryUnchecked("[:]SYST:ERR?\n", null).Split(',');
            if(errorRegister[0] == "0") 
            {
                //no error
            }
            if (errorRegister[0] == "50") 
            {
                throw new ChassisException("Error Register indicates error: " + errorRegister[1]);
            }
            if (errorRegister[0] == "70")
            {
                throw new ChassisException("Error Register indicates: " + errorRegister[1]);
            }
        }

        /// <summary>
        /// Specific command to try and catch empty string responses from the 6611
        /// (put in as a result of integration test issue)
        /// </summary>
        /// <param name="cmd">Our string command</param>
        /// <param name="instr">the instrument</param>
        /// <returns>the string response</returns>
        public string EmptyString_QueryUnchecked(string cmd, Instrument instr)
        {
            string result;
            bool valid = false;
            int count = 0;
            do
            {
                result = this.Query(cmd, instr);
                //this.Write(cmd, instr);
                //System.Threading.Thread.Sleep(200);
                //result=this.Read(instr);

                if (result == "")
                {
                    // Sleep for 500ms waiting for the DUT to stabilise
                    System.Threading.Thread.Sleep(500);
                    count++;
                }
                else
                {
                    valid = true;
                }
            } while ((!valid) && (count < 3));

            return (result);
        }

		#endregion

	}
	
}

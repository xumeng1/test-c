// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_Ag70843_PPG.cs
//
// Author: joseph.olajubu, 2007
// Design: BERT Driver Agilent 71562/70843 DD

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisNS;

namespace Bookham.TestLibrary.Instruments
{

    /// <summary>
    /// Instrument driver for Agilent 70843 Pattern Generator
    /// </summary>
    public class Inst_Ag70843_PPG : Instrument, IInstType_BertPatternGen
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_Ag70843_PPG(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            InstrumentDataRecord instr70843C = new InstrumentDataRecord(
                "AGILENT TECHNOLOGIES 70843C",	// hardware name 
                "C.00.00",		            // minimum valid firmware version 
                "C.01.02");		            // maximum valid firmware version 
            ValidHardwareData.Add("Ag70843C", instr70843C);

            // Configure valid chassis driver information
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_Ag70843",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "1.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("Chassis_Ag70843", chassisInfo);

            // initialise this instrument's chassis
            this.instrumentChassis = (Chassis_Ag70843)chassisInit;
        }
        #endregion

        #region Instrument overrides
        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                return this.instrumentChassis.FirmwareVersion;
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                return this.instrumentChassis.HardwareIdentity;
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            instrumentChassis.Write("*RST", this);
            this.PattGenEnabled = false;
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // TODO: custom setup for this instrument after it goes online                    
                }
            }
        }
        #endregion

        #region IInstType_BertPatternGen Members

        /// <summary>
        /// Get/set whether the clock outputs track
        /// </summary>
        public bool ClockOutputsTrack
        {
            get
            {
                bool ret;
                string resp = instrumentChassis.Query("SOUR11:VOLT:TRACK?", this).Trim();

                if (resp == "0")
                {
                    ret = false;
                }
                else if (resp == "1")
                {
                    ret = true;
                }
                else
                {
                    throw new InstrumentException ("Invalid response from instrument on Clock Output Track query: " + resp);
                }

                return ret;
            }
            set
            {
                if (value)
                {
                    instrumentChassis.Write("SOUR11:VOLT:TRACK 1", this);
                }
                else
                {
                    instrumentChassis.Write("SOUR11:VOLT:TRACK 0", this);
                }
            }
        }

        /// <summary>
        /// Get/set whether the data outputs track
        /// </summary>
        public bool DataOutputsTrack
        {
            get
            {
                bool ret;
                string resp = instrumentChassis.Query("SOUR10:VOLT:TRACK?", this).Trim();

                if (resp == "0")
                {
                    ret = false;
                }
                else if (resp == "1")
                {
                    ret = true;
                }
                else
                {
                    throw new InstrumentException("Invalid response from instrument on Data Output Track query: " + resp);
                }

                return ret;
            }
            set
            {
                if (value)
                {
                    instrumentChassis.Write("SOUR10:VOLT:TRACK 1", this);
                }
                else
                {
                    instrumentChassis.Write("SOUR10:VOLT:TRACK 0", this);
                }
            }
        }

        /// <summary>
        /// Get Output RF coupling mode for the specified output
        /// </summary>
        /// <param name="output">Which output</param>
        /// <returns>Which mode</returns>
        public InstType_BertRfCouplingMode GetOutputCoupling(InstType_BertPattGenOutput output)
        {
            string outputSpecifier;
            string resp;
            InstType_BertRfCouplingMode ret;

            switch (output)
            {
                case InstType_BertPattGenOutput.Clock:
                case InstType_BertPattGenOutput.ClockBar:
                    outputSpecifier = "OUTPUT2:";
                    break;

                case InstType_BertPattGenOutput.Data:
                case InstType_BertPattGenOutput.DataBar:
                    outputSpecifier = "OUTPUT1:";
                    break;

                default:
                    throw new InstrumentException("Invalid output specified to get Rf coupling mode");
                   
            }

            resp = instrumentChassis.Query(outputSpecifier + "COUPLING?", this).Trim();

            if (resp == "AC")
            {
                ret = InstType_BertRfCouplingMode.AcCoupled;
            }
            else if (resp == "DC")
            {
                //Find out what kind of DC Coupling.
                string term =instrumentChassis.Query(outputSpecifier + "TERMINATION?", this).Trim();
                if (term == "+0")
                {
                    ret = InstType_BertRfCouplingMode.DcCoupledTerm_0V;
                }
                else if (term == "-2")
                {
                    ret = InstType_BertRfCouplingMode.DcCoupledTerm_minus2V;
                }
                else
                {
                    throw new InstrumentException("Invalid response to Coupling Termination Voltage query: " + term);
                }
            }
            else
            {
                throw new InstrumentException("Invalid response from instrument on Output coupling mode query: " + resp);
            }

            return ret;
        }


        /// <summary>
        /// Set Output RF coupling mode for the specified output
        /// </summary>
        /// <param name="output">Which output</param>
        /// <param name="mode">Which mode</param>
        public void SetOutputCoupling(InstType_BertPattGenOutput output, InstType_BertRfCouplingMode mode)
        {
            string outputSpecifierString = "";
            string modeString = "";

            switch (output)
            {
                case InstType_BertPattGenOutput.Clock:
                case InstType_BertPattGenOutput.ClockBar:
                    outputSpecifierString = "OUTPUT2:";
                    break;

                case InstType_BertPattGenOutput.Data:
                case InstType_BertPattGenOutput.DataBar:
                    outputSpecifierString = "OUTPUT1:";
                    break;
            }

            switch (mode)
            {
                case InstType_BertRfCouplingMode.AcCoupled:
                    modeString = "AC";
                    break;

                case InstType_BertRfCouplingMode.DcCoupledTerm_0V:
                    modeString = "DC";
                    instrumentChassis.Write(outputSpecifierString + "TERMINATION 0", this);
                    break;

                case InstType_BertRfCouplingMode.DcCoupledTerm_minus2V:
                    modeString = "DC";
                    instrumentChassis.Write(outputSpecifierString + "TERMINATION -2", this);
                    break;
            }

            instrumentChassis.Write(outputSpecifierString + "COUPLING " + modeString, this);
        }


        /// <summary>
        /// Get Crossing Point percentage for the specified output
        /// </summary>
        /// <param name="output">Which output</param>
        /// <returns>Crossing Point percentage</returns>
        public double GetOutputCrossingPt_Percent(InstType_BertPattGenOutput output)
        {
            string command;

            switch (output)
            {
                case InstType_BertPattGenOutput.Clock:
                case InstType_BertPattGenOutput.ClockBar:
                    throw new InstrumentException ("Invalid output specified to get output crossing point");

                case InstType_BertPattGenOutput.Data:
                case InstType_BertPattGenOutput.DataBar:
                    command = "OUTPUT1:XOVER?";
                    break;

                default:
                    throw new InstrumentException("Invalid output specified to get Rf coupling mode");
                    
            }

            //Instrument returns a value which is a positive or negative deviation from the 50% crossing point.
            return (50 + Convert.ToDouble(instrumentChassis.Query(command, this).Trim()));            
        }

        /// <summary>
        /// Set Crossing Point percentage for the specified output
        /// </summary>
        /// <param name="output">Which output</param>
        /// <param name="percent">Crossing Point percentage</param>
        public void SetOutputCrossingPt_Percent(InstType_BertPattGenOutput output, double percent)
        {
            if ((output == InstType_BertPattGenOutput.Data) || (output == InstType_BertPattGenOutput.DataBar))
            {
                if ((percent >= 0) && (percent >= 100))
                {
                    //Instrument likes to set this interms of a positive or negative deviation from 50%. 
                    double deviationfrom50percent = percent - 50;
                    instrumentChassis.Write("OUTPUT1:XOVER " + deviationfrom50percent, this);
                }
                else
                {
                    throw new InstrumentException("Invalid Output crossing point specified: " + percent);
                }
            }
            else
            {
                throw new InstrumentException("Setting the crossing point for specified output not supported by Ag70843: " + output); 
            }
        }

        /// <summary>
        /// Get Output voltage for the specified output
        /// </summary>
        /// <param name="output">Which output</param>
        /// <returns>Output Voltage</returns>
        public InstType_BertOutputVoltageDef GetOutputVoltage(InstType_BertPattGenOutput output)
        {
            string command;
            InstType_BertOutputVoltageDef ret;

            //Figure out which command to issue, based on the required output port.
            switch (output)
            {
                case InstType_BertPattGenOutput.Clock:
                    command = "SOURCE2:VOLT?";
                    break;

                case InstType_BertPattGenOutput.ClockBar:
                    if (!this.ClockOutputsTrack)
                    {
                        //This command only makes sense if the clock and clockbar 
                        //outputs are not tracked.
                        command = "SOURCE11:VOLT?";
                    }
                    else
                    {
                        //Clock and Clockbar are tracking each other. Return the 
                        //setting for the clock output.
                        command = "SOURCE2:VOLT?";
                    }
                    break;

                case InstType_BertPattGenOutput.Data:
                    command = "SOURCE1:VOLT?";
                    break;

                case InstType_BertPattGenOutput.DataBar:                    
                    if (!this.DataOutputsTrack)
                    {
                        //This command only makes sense if the data and databar 
                        //outputs are not tracked.
                        command = "SOURCE10:VOLT?";
                    }
                    else
                    {
                        //data and databar are tracking each other. Return the 
                        //setting for the data output.
                        command = "SOURCE1:VOLT?";
                    }
                    break;

                default:
                    throw new InstrumentException("Invalid output specified to get output voltage" + output);
                   
            }

            ret.Amplitude_V = Convert.ToDouble(instrumentChassis.Query(command, this));
            ret.Offset_V = 0;

            return ret;
            
        }


        /// <summary>
        /// Set Output voltage for the specified output
        /// </summary>
        /// <param name="output">Which output</param>
        /// <param name="vDef">Voltage setup structure</param>
        public void SetOutputVoltage(InstType_BertPattGenOutput output, InstType_BertOutputVoltageDef vDef)
        {
            //Hold the minimum allowed output voltage for the specified output. 
            //For clock outputs this is 0.3V, and for Data Outputs it is 0.5V
            double voltageMin;
            if ((output == InstType_BertPattGenOutput.Data) || (output == InstType_BertPattGenOutput.DataBar))
            {
                voltageMin = 0.5;
            }
            else
            {
                voltageMin = 0.3;
            }


           
            //On this instrument, the output voltage can be set between 0.5V and 2V, in 10mV steps 
            vDef.Amplitude_V = Math.Round(vDef.Amplitude_V, 2);
            if ((vDef.Amplitude_V < voltageMin) || (vDef.Amplitude_V > 2))
            {
                throw new InstrumentException("Invalid output voltage specified: " + vDef.Amplitude_V + ". Must be between " + voltageMin + "V and 2V in 10mV increments");
            }

            if (vDef.Offset_V != 0)
            {
                throw new InstrumentException("DC Offset on Voltage specified for output not supported by this instrument. Offset value must be 0V");
            }

            string command;

            //Figure out which command to use, based on the required output.
            switch (output)
            {
                case InstType_BertPattGenOutput.Clock:
                    command = "SOURCE2:VOLT";
                    break;


                case InstType_BertPattGenOutput.ClockBar:
                    if (!this.ClockOutputsTrack)
                    {
                        //This command only makes sense if the clock and clockbar 
                        //outputs are not tracked.
                        command = "SOURCE11:VOLT";
                    }
                    else
                    {
                        //Clock and Clockbar are tracking each other. Use the command to control 
                        //the clock output voltage.
                        command = "SOURCE2:VOLT";
                    }
                    break;

                case InstType_BertPattGenOutput.Data:
                    command = "SOURCE1:VOLT";
                    break;

                case InstType_BertPattGenOutput.DataBar:
                    if (!this.DataOutputsTrack)
                    {
                        //This command only makes sense if the data and databar 
                        //outputs are not tracked.
                        command = "SOURCE10:VOLT";
                    }
                    else
                    {
                        //data and databar are tracking each other. Use the command for 
                        //setting for the data output.
                        command = "SOURCE1:VOLT";
                    }
                    break;

                default:
                    throw new InstrumentException("Invalid output specified to set output Voltage" + output);
                    
            }

            //write to the instrument.
            instrumentChassis.Write(command + " " + vDef.Amplitude_V, this);
        }


        /// <summary>
        /// Get/set Pattern Generator state (true:enabled, false:disabled)
        /// The outputs of data and databar are independently controlled. 
        /// For this driver will enable/disable them at the same time.
        /// </summary>
        public bool PattGenEnabled
        {
            get
            {
                int resp, resp1;

                resp = Convert.ToInt32(instrumentChassis.Query("OUTPUT1:STAT?", this));
                resp1 = Convert.ToInt32(instrumentChassis.Query("OUTPUT10:STAT?", this));

                if ((resp == 0) || (resp1 == 0))
                {
                    //It should never be the case that the data and databar in different states. 
                    //Unless someone has been messing about on the front panel.
                    //Assume that if either of them is disabled, that the instrument output is disabled.
                    return false;
                }
                else
                {
                    return true;
                }
            }
            set
            {
                string enable;
                if (value)
                {
                    enable = "ON";
                }
                else
                {
                    enable = "OFF";
                }

                //Enable/disable both data and databar outputs
                instrumentChassis.Write("OUTPUT1:STATE " + enable, this);

                if (!DataOutputsTrack)
                {
                    //Can only control databar if the outputs are not tracking.
                    instrumentChassis.Write("OUTPUT10:STATE " + enable, this);
                }
            }
        }


        /// <summary>
        /// Get/set the Pattern type.
        /// In this version of the driver, only PRBS Patterns shall be supported.
        /// </summary>
        public InstType_BertDataPatternType PatternType
        {
            get
            {
                string resp = instrumentChassis.Query("SOURCE1:PATTERN?", this).Trim();
                if (resp.StartsWith("PRBS"))
                {
                    //This should really be the only response seen, unless
                    //someones been playing with the front panel
                    return InstType_BertDataPatternType.Prbs;
                }
                else
                {
                    return InstType_BertDataPatternType.Other;
                }

            }
            set
            {
                if (value == InstType_BertDataPatternType.Prbs)
                {
                    instrumentChassis.Write("SOURCE1:PATTERN PRBS" + this.prbsLength, this);
                }
                else
                {
                    throw new InstrumentException("Only PRBS Pattern type is supported by this device driver");
                }
            }
        }

        /// <summary>
        /// Get/set the PRBS length 2^N-1 (i.e. this is the "N")
        /// </summary>
        public int PrbsLength
        {
            get
            {
                return this.prbsLength;
            }
            set
            {
                //Only valid values are 7,10,15,23 or 31
                if ((value == 7) || (value == 10) || (value == 15) || (value == 23) || (value == 31))
                {
                    this.prbsLength = value;
                    instrumentChassis.Write("SOURCE1:PATTERN PRBS" + this.prbsLength, this);
                }
                else
                {
                    throw new InstrumentException("Invalid PRBS length attempted to be set: " + value);
                }
            }
        }

        /// <summary>
        /// Get/Set user defined pattern. Not supported by this version of the driver.
        /// </summary>
        public bool[] UserPattern
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }


        /// <summary>
        /// Get/set the User Pattern length. Not supported by this version of the driver.
        /// </summary>
        public int UserPatternLength
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        #endregion


        #region Private data
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_Ag70843 instrumentChassis;

        //PRBS sequence length. Default value is 23, 
        //which is what the instrument sets itself to after *RST
        //Valid values are 7,10,15,23 or 31
        private int prbsLength = 23;

        #endregion

    }
}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Chassis
//
// Chassis_Fcu2Asic.cs
//
// Author: alice.huang, 2010
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestLibrary.ChassisTypes;

namespace Bookham.TestLibrary.ChassisNS
{
    // TODO: 
    // 1. Change the base class to the type of chassis you are implementing 
    //   (ChassisType_Visa488_2 for IEEE 488.2 compatible chassis, ChassisType_Visa for other VISA message
    //    based types).
    // 2. Visual Studio can be used to auto-generate function stubs (hover over the base class name).
    // 3. Fill in the gaps.
    /// <summary>
    /// 
    /// </summary>
    public class Chassis_Fcu2Asic :ChassisType_Serial
    {
        /// <summary>
        /// if comunication is ok between the pc and FCU, 
        /// the fcu will return a string and with a terminal string as it 
        /// </summary> 
        private string readOKTerminalStr;        
        
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="chassisNameInit">Chassis name</param>
        /// <param name="driverNameInit">Chassis driver name</param>
        /// <param name="resourceString">Resource string for communicating with the chassis (for VISA
        /// chassis, this is the VISA resource string)</param>
        public Chassis_Fcu2Asic(string chassisNameInit, string driverNameInit,
            string resourceString)
            : base(chassisNameInit, driverNameInit, resourceString.Split(',')[0] + resourceString.Split(',')[1])
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            ChassisDataRecord chassisData = new ChassisDataRecord(
                "Bookham FCU TO ASIC",			// hardware name 
                "0",			// minimum valid firmware version 
                "Firmware_Unknown");		// maximum valid firmware version 
            ValidHardwareData.Add("FCU To Asic", chassisData);

            string[] temp = resourceString.Split(',');
            if ((temp.Length != 2) && (temp.Length != 3))
            {
                throw new ChassisException(
                    "Bad resource string. Use \"COM,n\" or \"COM,n,rate\". eg \"COM,1,38400\"");
            }
            base.NewLine = "\r\n";
            base.BaudRate = (temp.Length >2)?int.Parse(temp[2]):38400;
            base.DataBits = 8;
            base.Parity = System.IO.Ports.Parity.None;
            base.StopBits = System.IO.Ports.StopBits.One;
            base.Handshaking = System.IO.Ports.Handshake.None;
            base.Timeout_ms = 2000;
            readOKTerminalStr = "ok";
            base.InputBufferSize_bytes = 40960; //  40* 1024 > 201 * 200
            base.OutputBufferSize_bytes = 40960; // > 201 * 200
            
            
        }
        #endregion

        #region Chassis overrides
        /// <summary>
        /// Firmware version of this chassis.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // TODO: Update
                return "Firmware_Unknown";
            }
        }

        /// <summary>
        /// Hardware Identity of this chassis.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // TODO: Comi + " FCU To Asic"
                return base.ResourceInfo + " FCU To Asic";
            }
        }

        /// <summary>
        /// Setup the chassis as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                bool flag = false;
                if (base.IsOnline)
                {
                    WriteLine("hello", null);
                    System.Threading.Thread.Sleep(10);
                    string rspStr = ReadLine(null);
                    if (String.Compare(rspStr, "ok", true) == 0) flag = true;
                }
                return flag;
                
            }
            set
            {
                if (value) // if setting online                
                {
                    // TODO: custom setup for this chassis before it goes online
                   
                    // E.g. setup RS232 variables
                }

                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // TODO: custom setup for this chassis after it goes online, 
                    // e.g. Standard Error register on a 488.2 instrument

                    //// 4 = Query error, 8 = Device error, 16 = execution error, 32 = command error
                    //this.StandardEventRegisterMask = 4 + 8 + 16 + 32;
                    //// clear the status registers
                    //this.Write("*CLS", null);                    
                }
            }
        }
        #endregion

        #region comnication base fucntion
        
        
        /// <summary>
        /// <para> included in the  <see cref="readOKTerminalStr"/></para>
        /// </summary>
        public string ReadOKTerminalStr
        {
            get { return readOKTerminalStr; }
            set { readOKTerminalStr = value;}
        }        
        /// <summary>
        /// clear all sweeps data 
        /// </summary>
        public void Clear()
        { }
        /// <summary>
        /// send command string to FCU then read back a responding string from FCU
        /// if the responding string doesn't end with the communicationOKStr,
        /// throw ChassisException
        /// Exception:
        /// ChassisException
        /// </summary>
        /// <param name="commandStr"> command send to FCU</param>
        /// <param name="instrument"></param>
        /// <returns></returns>
        public virtual string Query_Checked(string commandStr, Instrument instrument)
        {
            string rspStr = this.Query(commandStr, instrument);
            // if commnication ok , the fcu should return a string end with communicationOKStr
            if (!rspStr.Trim().EndsWith(readOKTerminalStr))
                throw new ChassisException(
                    string.Format("Validate Error!\nSend:{0}\nReceive:{1}" +
                    "\nValidate Flag:{2}\nReceive sting isn't end with '{2}'",
                    commandStr, rspStr, readOKTerminalStr));
            return rspStr;
        }
        /// <summary>
        /// read responding string from FCU
        /// if the responding string doesn't end with the communicationOKStr,
        /// throw ChassisException
        /// Exception:
        /// ChassisException
        /// </summary>
        /// <param name="instrument"></param>
        /// <returns></returns>
        public virtual string Read_Checked(Instrument instrument)
        {
            string rspStr = this.Read(instrument);
            // if commnication ok , the fcu should return a string end with communicationOKStr
            if (!rspStr.Trim().EndsWith(readOKTerminalStr))
                throw new ChassisException(string.Format("Validate Error!\nReceive:{0} " + 
                    "\nValidate Flag:{1}\nReceive sting isn't end with '{1}'",
                    rspStr, readOKTerminalStr));
            return rspStr;
        }
        /// <summary>
        /// read data from the serial prot's input buffer, utill a NewLine was get
        /// however, the return string won't contain the NewLine char(s)
        /// </summary>
        /// <param name="instrument"></param>
        /// <returns></returns>
        public virtual string ReadLine_Checked(Instrument instrument)
        {
            string rspStr = this.ReadLine(instrument);
            if (!rspStr.Trim().EndsWith(readOKTerminalStr))
                throw new ChassisException(string.Format("Validate Error!\nReceive:{0}" + 
                    "\nValidate Flag:{1}\nReceive sting isn't end with '{1}'",
                    rspStr, readOKTerminalStr));    
            
            return rspStr;
        }
        #endregion
    }
}

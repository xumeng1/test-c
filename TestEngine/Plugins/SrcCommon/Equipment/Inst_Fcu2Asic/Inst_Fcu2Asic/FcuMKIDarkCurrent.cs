using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.Instruments
{
    public class FcuMKIDarkCurrent
    {
        /// <summary>
        /// dac_trans_mon
        /// </summary>
        public int Itx_DAC
        {
            set
            {
                this.itx_dac = value;
                
            }
            get
            {
                return this.itx_dac;
            }
        }
        
        /// <summary>
        /// dac_ref_mon
        /// </summary>
        public int Irx_DAC
        {
            set
            {
                this.irx_dac = value;
            }
            get
            {
                return this.irx_dac;
            }
        }
        /// <summary>
        /// dac_Reference_mon , Fix gain port
        /// </summary>
        public int Fix_DAC
        {
            set
            {
                this.fix_dac = value;
            }
            get
            {
                return this.fix_dac;
            }
        }
        /// <summary>
        /// dac_filter_mon, variant gain port
        /// </summary>
        public int Var_DAC
        {
            set
            {
                this.var_dac = value;
            }
            get
            {
                return this.var_dac;
            }
        }

        int itx_dac;
        int irx_dac;
        int fix_dac;
        int var_dac;
    }
    
        
}

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisNS;
using System.ComponentModel;
using System.Threading;
using System.Text.RegularExpressions;
using System.IO;

namespace Bookham.TestLibrary.Instruments
{
    public partial class Inst_Fcu2Asic
    {
        //Inst_Fcu2Asic inst_FCU;


        public enum SweepArm
        {
            Left,
            Right,
        }
        /// <summary>
        /// do single ended sweep in imbalance arm, and monitor photodiode from Ctap
        /// </summary>
        /// <param name="arm">left or right</param>
        /// <param name="startmA_DAC"></param>
        /// <param name="stopmA_DAC"></param>
        /// <param name="delaytime"></param>
        /// <returns></returns>
        public string[] AsicImbSingleEndedSweep(SweepArm arm, int startmA_DAC, int stopmA_DAC, int Nbr_Point, int delaytime_ms)
        {
            if (delaytime_ms < 0 || delaytime_ms > 255)
                throw new ArgumentOutOfRangeException("delaytime_ms out of range 0~255");
            string cmd = string.Format("{0} {1} {2} {3} {4} ", "asicimbsesweepctap", arm, startmA_DAC, stopmA_DAC, Nbr_Point, delaytime_ms);
            instrumentChassis.Clear();
            instrumentChassis.Timeout_ms = 2000 + delaytime_ms;
            instrumentChassis.WriteLine(cmd, this);
            return ReadStrFromFcu(delaytime_ms);
        }

        public string[] AsicImbDiffSweep(int startmA_DAC, int stopmA_DAC, int Nbr_Point, int delaytime_ms)
        {
            if (delaytime_ms < 0 || delaytime_ms > 255)
                throw new ArgumentOutOfRangeException("delaytime_ms out of range 0~255");
            string cmd = string.Format("{0} {1} {2} {3}", "asicimbdfsweepctap", startmA_DAC, stopmA_DAC, Nbr_Point, delaytime_ms);
            instrumentChassis.Clear();
            instrumentChassis.Timeout_ms = 2000 + delaytime_ms;
            instrumentChassis.WriteLine(cmd, this);
            return ReadStrFromFcu(delaytime_ms);

        }

        /// <summary>
        /// Function: Do single end sweep in optical domain for MZ imbalance arms wih asic source case
        ///           (monitor optical power ). This function will output the trigger signal to trigger external 
        ///           power meter. So the external power meter also need configure to sweep 
        /// </summary>
        /// <param name="arm"></param>
        /// <param name="startmA_DAC"></param>
        /// <param name="stopmA_DAC"></param>
        /// <param name="Nbr_Point"></param>
        /// <param name="delaytime_ms"></param>
        public void AsicImbSingledEndedTrig(SweepArm arm, int startmA_DAC, int stopmA_DAC, int Nbr_Point, int delaytime_ms)
        {
            if (delaytime_ms < 0 || delaytime_ms > 255)
                throw new ArgumentOutOfRangeException("delaytime_ms out of range 0~255");
            string cmd = string.Format("{0} {1} {2} {3} {4}", "asicimbsesweeppm", arm, startmA_DAC, stopmA_DAC, Nbr_Point, delaytime_ms);
            instrumentChassis.Clear();
            instrumentChassis.Timeout_ms = 2000 + delaytime_ms;
            instrumentChassis.WriteLine(cmd, this);
        }

        public void AsicImbDiffTrig(int startmA_DAC, int stopmA_DAC, int Nbr_Point, int delaytime_ms)
        {
            if (delaytime_ms < 0 || delaytime_ms > 255)
                throw new ArgumentOutOfRangeException("delaytime_ms out of range 0~255");
            string cmd = string.Format("{0} {1} {2} {3} {4}", "asicimbdfsweeppm", startmA_DAC, stopmA_DAC, Nbr_Point, delaytime_ms);
            instrumentChassis.Clear();
            instrumentChassis.Timeout_ms = 2000 + delaytime_ms;
            instrumentChassis.WriteLine(cmd, this);
           // return ReadStrFromFcu(delaytime_ms);

        }
        /// <summary>
        ///Do differential sweep in optical domain for MZ Bias arms(monitor optical power ) in FCU source case .
        /// </summary>
        /// <param name="start_val_V"></param>
        /// <param name="stop_val_V"></param>
        /// <param name="Nbr_Point"></param>
        /// <param name="delaytime_ms"></param>
        public void FcuBiasDiffTrig(double start_val_V, double stop_val_V, int Nbr_Point, int delaytime_ms)
        {
            if (delaytime_ms < 0 || delaytime_ms > 255)
                throw new ArgumentOutOfRangeException("delaytime_ms out of range 0~255");
            //if (stop_val_V < start_val_V)
            //{
            //    throw new Exception("Stop voltage should be greater than start voltage in lv sweep");
            //}
            string cmd = string.Format("{0} {1} {2} {3} {4}", "fculvdfsweeppm", start_val_V, stop_val_V, Nbr_Point, delaytime_ms);
            instrumentChassis.Clear();
            instrumentChassis.Timeout_ms = 2000 + delaytime_ms * Nbr_Point;
            instrumentChassis.WriteLine(cmd, this);
            

        }

        /// <summary>
        ///  Do single end sweep in optical domain for MZ Bias arms (
        /// </summary>
        /// <param name="start_val_V"></param>
        /// <param name="stop_val_V"></param>
        /// <param name="Nbr_Point"></param>
        /// <param name="delaytime_ms"></param>
        public void FcuLeftBiasSingleEndedTrig(double start_val_V, double stop_val_V, int Nbr_Point, int delaytime_ms)
        {
            if (delaytime_ms < 0 || delaytime_ms > 255)
                throw new ArgumentOutOfRangeException("delaytime_ms out of range 0~255");
            if (Math.Abs(stop_val_V) < Math.Abs(start_val_V))
            {
                throw new Exception("Stop voltage should be greater than start voltage in lv sweep");
            }
            if (Math.Abs(stop_val_V) > 8.0)
            {
                throw new Exception("stop voltage should be smaller than 8V");
            }
            string cmd = string.Format("{0} {1} {2} {3} {4}", "fculvsesweeppm left", start_val_V, stop_val_V, Nbr_Point, delaytime_ms);
            instrumentChassis.Clear();
            instrumentChassis.Timeout_ms = 2000 + delaytime_ms;
            instrumentChassis.WriteLine(cmd, this);
        }
        public double [] GetCtapAfterMZsweep()
        {
            
            string mc_Configure = "(?<=%)[\\w,,]+(?=%)";
            MatchCollection mc = null;
            StringBuilder sb = new StringBuilder();
            try
            {
                do
                {
                    Thread.Sleep(10);
                    sb.Append(instrumentChassis.Read(this));
                    mc = Regex.Matches(sb.ToString(), mc_Configure);
                   
                    if (sb.ToString().Trim().EndsWith(this.ValidateFlag, StringComparison.InvariantCultureIgnoreCase)) break;
                } while (true);

            }
            catch (Exception)
            {
                throw;
            }
                instrumentChassis.Timeout_ms = 2000;
                string stronerow;
                char[] split_val = { ',' };
                string[] firstone ={ "" };
                List<double > Ctap_val = new List<double >(); 
                int i_loop;

                foreach (Match var in mc)
                {
                    stronerow = var.Value;
                    firstone = stronerow.Split(split_val);
                    for (i_loop = 0; i_loop < firstone.Length; i_loop++)
                    {
                        Ctap_val.Add(-1.0*this.FCUMKI_Fix_CurrentDACTomA(int.Parse(firstone[i_loop])));
                    }
                }

            return Ctap_val.ToArray();
        }
        /// <summary>
        /// Get Reference Mz sweep Data use trigger for Overmap
        /// </summary>
        /// <returns></returns>
        public double[] GetReferenceAfterMZsweep()
        {

            string mc_Configure = "(?<=%)[\\w,,]+(?=%)";
            MatchCollection mc = null;
            StringBuilder sb = new StringBuilder();
            try
            {
                do
                {
                    Thread.Sleep(10);
                    sb.Append(instrumentChassis.Read(this));
                    mc = Regex.Matches(sb.ToString(), mc_Configure);

                    if (sb.ToString().Trim().EndsWith(this.ValidateFlag, StringComparison.InvariantCultureIgnoreCase)) break;
                } while (true);

            }
            catch (Exception)
            {
                throw;
            }
            instrumentChassis.Timeout_ms = 2000;
            string stronerow;
            char[] split_val = { ',' };
            string[] firstone ={ "" };
            List<double> Ctap_val = new List<double>();
            int i_loop;
            foreach (Match var in mc)
            {
                stronerow = var.Value;
                firstone = stronerow.Split(split_val);
                for (i_loop = 0; i_loop < firstone.Length; i_loop++)
                {
                    Ctap_val.Add(double.Parse(firstone[i_loop]));
                }
            }

            return Ctap_val.ToArray();
        }
        /// <summary>
        ///  Do single end sweep in optical domain for MZ Bias arms (
        /// </summary>
        /// <param name="start_val_V"></param>
        /// <param name="stop_val_V"></param>
        /// <param name="Nbr_Point"></param>
        /// <param name="delaytime_ms"></param>
        public void FcuRightBiasSingleEndedTrig(double start_val_V, double stop_val_V, int Nbr_Point, int delaytime_ms)
        {
            if (delaytime_ms < 0 || delaytime_ms > 255)
                throw new ArgumentOutOfRangeException("delaytime_ms out of range 0~255");
            if (Math.Abs(stop_val_V) < Math.Abs(start_val_V))
            {
                throw new Exception("Stop voltage should be greater than start voltage in lv sweep");
            }
            if (Math.Abs(stop_val_V) > 8.0)
            {
                throw new Exception("stop voltage should be smaller than 8V");
            }
            string cmd = string.Format("{0} {1} {2} {3} {4}", "fculvsesweeppm right", start_val_V, stop_val_V, Nbr_Point, delaytime_ms);
            instrumentChassis.Clear();
            instrumentChassis.Timeout_ms = 2000 + delaytime_ms;
            instrumentChassis.WriteLine(cmd, this);

        }



        private string[] ReadStrFromFcu(int delaytime_ms)
        {
            string mc_Configure = "(?<=%)[\\w,&]+(?=%)";
            MatchCollection mc = null;
            try
            {
                StringBuilder sb = new StringBuilder();
                do
                {
                    Thread.Sleep(10 + delaytime_ms);
                    sb.Append(instrumentChassis.Read(this));
                    mc = Regex.Matches(sb.ToString(), mc_Configure);

                    if (sb.ToString().Trim().EndsWith(this.ValidateFlag, StringComparison.InvariantCultureIgnoreCase)) break;
                } while (true);
                if (mc.Count > 1)
                {
                    throw new Exception("Invalid return data when do sweep!");
                }

                instrumentChassis.Timeout_ms = 2000;
            }catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            string[] result = null;
            foreach (Match mt in mc)
            {
                string line = mt.Value;
                result = line.Split(',');

            }
            return result;
        }
    }


}

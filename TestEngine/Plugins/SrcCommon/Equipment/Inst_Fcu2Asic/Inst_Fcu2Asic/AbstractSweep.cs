using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using Bookham.TestLibrary.ChassisNS;
using System.IO;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using System.Text.RegularExpressions;
using System.Threading;


namespace Bookham.TestLibrary.Instruments
{

    abstract class Sweep
    {
        protected Sweep(Chassis_Fcu2Asic instrumentChassis, Inst_Fcu2Asic inst_FCU)
        {
            this.instrumentChassis = instrumentChassis;
            this.inst_FCU = inst_FCU;
        }
        protected Chassis_Fcu2Asic instrumentChassis;
        protected Inst_Fcu2Asic inst_FCU;
        protected int delaytime_ms;
        protected bool FCUMKI_Locker_Calibration_Done = false;
        protected bool BeginAsyncSweepFlag = false;
        private Inst_Fcu2Asic.GetOverallMapDataFrom GDF;
        protected string DataDirectory()
        {
            string FileName = string.Format("{0:yyyyMMddhhmmss}.csv", DateTime.Now);
            string dir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "results");
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);
            return Path.Combine(dir, FileName);
        }


        public virtual void BeginSweepAsync(string sweepCmd, Inst_Fcu2Asic.SweepDirection sd, int delaytime_ms, Inst_Fcu2Asic.GetOverallMapDataFrom GDF)
        {
            this.GDF = GDF;
            if (delaytime_ms < 0 || delaytime_ms > 255)
                throw new ArgumentOutOfRangeException("delaytime_ms out of range 0~255");
            this.delaytime_ms = delaytime_ms;
            string cmd = string.Format("{0} {1} {2}", sweepCmd, sd, delaytime_ms);
            instrumentChassis.Clear();
            instrumentChassis.Timeout_ms = 2000 + delaytime_ms;
            instrumentChassis.WriteLine(cmd, inst_FCU);
            BeginAsyncSweepFlag = true;
        }
        public virtual void BeginSweepAsync(string sweepCmd, Inst_Fcu2Asic.SweepDirection sd, int Irear_delaytime_ms,
            int IrearJump_delaytime_ms, int Average_Num,int Irear_jump_delayPointCount, string Rear_RearSoa_Switch)
        {
            if (Irear_delaytime_ms < 0 || Irear_delaytime_ms > 255)
                throw new ArgumentOutOfRangeException("Irear_delaytime_ms out of range 0~255");
            if (IrearJump_delaytime_ms < 0 || IrearJump_delaytime_ms > 255)
                throw new ArgumentOutOfRangeException("IrearJump_delaytime_ms out of range 0~255");
            this.delaytime_ms = Irear_delaytime_ms;
            string cmd = string.Format("{0} {1} {2} {3} {4} {5} {6}", sweepCmd, sd, Irear_delaytime_ms, IrearJump_delaytime_ms,
                Average_Num, Irear_jump_delayPointCount, Rear_RearSoa_Switch);
            instrumentChassis.Clear();
            instrumentChassis.Timeout_ms = 2000 + delaytime_ms;
            instrumentChassis.WriteLine(cmd, inst_FCU);
            BeginAsyncSweepFlag = true;
        }

        public virtual void BeginSweepAsync(string sweepCmd, Inst_Fcu2Asic.SweepDirection sd, int delay_phase,
            int delay_Im, int delay_Sm, int average_time, int delay_phase_PointCount, string Rear_RearSoa_Switch)
        {
            if (delay_phase < 0 || delay_phase > 255)
                throw new ArgumentOutOfRangeException("delay_phase out of range 0~255");
            if (delay_Im < 0 || delay_Im > 255)
                throw new ArgumentOutOfRangeException("delay_Im out of range 0~255");
            if (delay_Sm < 0 || delay_Sm > 255)
                throw new ArgumentOutOfRangeException("delay_Sm out of range 0~255");
            this.delaytime_ms = delay_Sm;
            string cmd = string.Format("{0} {1} {2} {3} {4} {5} {6} {7}", sweepCmd, sd, delay_phase, delay_Im,
                delay_Sm, average_time, delay_phase_PointCount,Rear_RearSoa_Switch);
            instrumentChassis.Clear();
            instrumentChassis.Timeout_ms = 2000 + delaytime_ms;
            instrumentChassis.WriteLine(cmd, inst_FCU);
            BeginAsyncSweepFlag = true;
        }

        public virtual string EndSweepAsync_FourPort(ProgressChangedEventHandler ProgressChanged, Inst_Fcu2Asic.SweepDirection sd, bool useDUTEtalon)//For Wave meter replacement project
        {
            string mc_Configure = "(?<=<-START OF DATA->)[\\w,&]+(?=<-END OF DATA->)";

            if (!BeginAsyncSweepFlag)
                throw new Exception("Please Begin Sweep!");
            try
            {
                MatchCollection mc = null;
                StringBuilder sb = new StringBuilder();
                do
                {
                    Thread.Sleep(5 + delaytime_ms);
                    string FCUMKI_MappingDAta = instrumentChassis.Read(inst_FCU);
                    sb.Append(FCUMKI_MappingDAta);
                    mc = Regex.Matches(sb.ToString(), mc_Configure);

                    if (ProgressChanged != null)
                    {
                        ProgressChangedEventArgs arg = new ProgressChangedEventArgs((int)mc.Count, null);
                        ProgressChanged(inst_FCU, arg);
                    }

                    if (sb.ToString().Trim().EndsWith(inst_FCU.ValidateFlag, StringComparison.InvariantCultureIgnoreCase)) 
                        break;
                } while (true);
                instrumentChassis.Timeout_ms = 2000;
                string FileName = DataDirectory();
                int mc_Row = mc.Count, D_val = 6;
                string[] strpath = new string[D_val];
                FileName = FileName.Replace(".csv", "");
                System.IO.Directory.CreateDirectory(FileName);
                
                string stronerow, sweepdirection = "";
                char[] split_val = { ',' };
                char[] split_val_2 ={ '&' };
                string[] firstone ={ "" };
                string[] TX_val = new string[mc_Row];
                string[] RX_val = new string[mc_Row];
                string[] LockerRatio = new string[mc_Row];
                string[] Filter_val = new string[mc_Row];
                string[] Reference_val = new string[mc_Row];
                string[] PowerRatio = new string[mc_Row];
                string[] str_split_temp = new string[D_val];
                string[] str_temp = new string[D_val];
                int i_loop, i_index, n_val = 0, sweep_count = 0;
                int Tx_Pot = inst_FCU.Locker_tran_pot, Rx_Pot = inst_FCU.Locker_refi_pot, Locker_fine_Pot = inst_FCU.Locker_fine_pot,
                    Var_Pot = inst_FCU.Var_pot, Fix_pot = inst_FCU.Fix_pot;
                int phase_sweep_start, phase_sweep_end, phase_sweep_step;
                bool PotInCalFile = false;

                do
                {
                    foreach (Match var in mc)
                    {
                        stronerow = var.Value;
                        firstone = stronerow.Split(split_val);
                        for (int i = 0; i < D_val; i++)
                        {
                            str_temp[i] = "";
                            str_split_temp[i] = "";
                        }
                        double LockerRatio_val = 0;
                        double Tx_fval = 0;
                        double Rx_fval = 0;
                        double PowerRatio_val = 0;
                        double Filter_fval = 0;
                        double Reference_fval = 0;
                        double Length_Split = 0;
                        int temp_TX_DAC, temp_RX_DAC, temp_Var_DAC, temp_Fix_DAC;

                        if ((sd == Inst_Fcu2Asic.SweepDirection.f) || (sd == Inst_Fcu2Asic.SweepDirection.both && sweep_count == 0))
                        {
                            phase_sweep_start = 0;
                            phase_sweep_end = firstone.Length;
                            phase_sweep_step = 1;
                            sweepdirection = "Forward_";

                            if ((sd == Inst_Fcu2Asic.SweepDirection.both && sweep_count == 0))
                                phase_sweep_end = firstone.Length / 2 + 1;
                        }
                        else
                        {
                            phase_sweep_start = firstone.Length - 1;
                            phase_sweep_end = -1;
                            phase_sweep_step = -1;
                            sweepdirection = "Reverse_";
                            if ((sd == Inst_Fcu2Asic.SweepDirection.both && sweep_count == 1))
                                phase_sweep_end = phase_sweep_start / 2 - 1;
                        }
                        #region Cal Locker Ratio and Power Ratio by DAC tick off Dark_DAC. Jack.Zhang 2011-01-16
                        for (i_loop = phase_sweep_start; i_loop * phase_sweep_step < phase_sweep_end * phase_sweep_step; i_loop = i_loop + phase_sweep_step)
                        {
                            str_split_temp = firstone[i_loop].Split(split_val_2);//(Tx,Rx,Fix(pin12),Var(pin8))
                            Array.Resize(ref str_split_temp, str_split_temp.Length + 2);
                            temp_TX_DAC = int.Parse(str_split_temp[0]);
                            temp_RX_DAC = int.Parse(str_split_temp[1]);
                            temp_Var_DAC = int.Parse(str_split_temp[3]);//(Tx,Rx,Fix(pin12),Var(pin8))
                            temp_Fix_DAC = int.Parse(str_split_temp[2]);//(Tx,Rx,Fix(pin12),Var(pin8))
                            //In order to make same power ratio for different light power, we need conver DAC to mA consider the Fix pot port. Jack.zhang/11/11/2010
                            Filter_fval = inst_FCU.FCUMKI_Var_CurrentDACTomA(temp_Var_DAC, Var_Pot, out PotInCalFile);
                            Reference_fval = inst_FCU.FCUMKI_Fix_CurrentDACTomA(temp_Fix_DAC);
                            Tx_fval = inst_FCU.FCUMKI_Locker_Tx_CurrentDACTomA(temp_TX_DAC, Locker_fine_Pot, Tx_Pot, out PotInCalFile);
                            Rx_fval = inst_FCU.FCUMKI_Locker_Rx_CurrentDACTomA(temp_RX_DAC, Rx_Pot, out PotInCalFile);

                            LockerRatio_val = Rx_fval / Tx_fval;
                            PowerRatio_val = Filter_fval / Reference_fval;

                            str_split_temp[4] = LockerRatio_val.ToString();
                            str_split_temp[5] = PowerRatio_val.ToString();

                            if (i_loop == (phase_sweep_end - phase_sweep_step))
                            {
                                for (int i = 0; i < D_val; i++)
                                {
                                    str_temp[i] = str_temp[i] + str_split_temp[i];

                                }
                            }
                            else
                            {
                                for (int i = 0; i < D_val; i++)
                                {
                                    str_temp[i] = str_temp[i] + str_split_temp[i] + ","; //we just save the raw DAC for TX,Rx,Filter and Ref (thevalue include Dark_DAC), but the power Ratio and Locker RAtio is calculated by the DAC_value not include

                                }
                            }
                        }
                        #endregion Cal Locker Ratio and Power Ratio by DAC tick off Dark_DAC. Jack.Zhang 2011-01-16
                        TX_val[n_val] = str_temp[0];
                        RX_val[n_val] = str_temp[1];
                        Filter_val[n_val] = str_temp[3];//(Tx,Rx,Fix(pin12),Var(pin8))
                        Reference_val[n_val] = str_temp[2];//(Tx,Rx,Fix(pin12),Var(pin8))
                        LockerRatio[n_val] = str_temp[4];
                        PowerRatio[n_val] = str_temp[5];
                        n_val++;
                    }

                    strpath[0] = Path.Combine(FileName, sweepdirection + "TX.csv");
                    strpath[1] = Path.Combine(FileName, sweepdirection + "RX.csv");
                    strpath[2] = Path.Combine(FileName, sweepdirection + "Filter.csv");
                    strpath[3] = Path.Combine(FileName, sweepdirection + "Reference.csv");
                    strpath[4] = Path.Combine(FileName, sweepdirection + "LockerRatio.csv");
                    strpath[5] = Path.Combine(FileName, sweepdirection + "PowerRatio.csv");

                    for (int m = 0; m < D_val; m++)
                    {
                        using (StreamWriter sw = new StreamWriter(strpath[m]))
                        {

                            switch (m)
                            {
                                case 0:
                                    foreach (string str in TX_val)
                                    {

                                        sw.WriteLine(str);
                                    }
                                    break;
                                case 1:
                                    foreach (string str in RX_val)
                                    {
                                        sw.WriteLine(str);
                                    }
                                    break;
                                case 2:
                                    foreach (string str in Filter_val)
                                    {

                                        sw.WriteLine(str);
                                    }
                                    break;
                                case 3:
                                    foreach (string str in Reference_val)
                                    {
                                        sw.WriteLine(str);
                                    }
                                    break;
                                case 4:
                                    foreach (string str in LockerRatio)
                                    {
                                        sw.WriteLine(str);
                                    }
                                    break;
                                case 5:
                                    foreach (string str in PowerRatio)
                                    {
                                        sw.WriteLine(str);
                                    }
                                    break;
                            }

                            sw.Close();
                        }
                    }
                    #region split Overall map power Ratio file to seven
                    if (sd != Inst_Fcu2Asic.SweepDirection.both)
                    {
                        int countnum = 0;
                        int file_no = 1;
                        StreamWriter sw1 = null;
                        foreach (string str in PowerRatio)
                        {
                            if (countnum == 0)
                            {
                                string ratio_path = Path.Combine(FileName, sweepdirection + "PowerRatio" + "_" + file_no.ToString() + ".csv");
                                sw1 = new StreamWriter(ratio_path);
                                file_no++;
                            }
                            sw1.WriteLine(str);
                            countnum++;
                            if (countnum == (mc_Row / 7))
                            {
                                countnum = 0;
                                sw1.Close();
                            }
                        }

                        countnum = 0;
                        file_no = 1;
                        foreach (string str in Filter_val)
                        {
                            if (countnum == 0)
                            {
                                string ratio_path = Path.Combine(FileName, sweepdirection + "Filter" + "_" + file_no.ToString() + ".csv");
                                sw1 = new StreamWriter(ratio_path);
                                file_no++;
                            }
                            sw1.WriteLine(str);
                            countnum++;
                            if (countnum == (mc_Row / 7))
                            {
                                countnum = 0;
                                sw1.Close();
                            }
                        }
                        countnum = 0;
                        file_no = 1;
                        foreach (string str in Reference_val)
                        {
                            if (countnum == 0)
                            {
                                string ratio_path = Path.Combine(FileName, sweepdirection + "Reference" + "_" + file_no.ToString() + ".csv");
                                sw1 = new StreamWriter(ratio_path);
                                file_no++;
                            }
                            sw1.WriteLine(str);
                            countnum++;
                            if (countnum == (mc_Row / 7))
                            {
                                countnum = 0;
                                sw1.Close();
                            }
                        }
                        countnum = 0;
                        file_no = 1;
                        foreach (string str in TX_val)
                        {
                            if (countnum == 0)
                            {
                                string ratio_path = Path.Combine(FileName, sweepdirection + "Tx" + "_" + file_no.ToString() + ".csv");
                                sw1 = new StreamWriter(ratio_path);
                                file_no++;
                            }
                            sw1.WriteLine(str);
                            countnum++;
                            if (countnum == (mc_Row / 7))
                            {
                                countnum = 0;
                                sw1.Close();
                            }
                        }
                        countnum = 0;
                        file_no = 1;
                        foreach (string str in RX_val)
                        {
                            if (countnum == 0)
                            {
                                string ratio_path = Path.Combine(FileName, sweepdirection + "Rx" + "_" + file_no.ToString() + ".csv");
                                sw1 = new StreamWriter(ratio_path);
                                file_no++;
                            }
                            sw1.WriteLine(str);
                            countnum++;
                            if (countnum == (mc_Row / 7))
                            {
                                countnum = 0;
                                sw1.Close();
                            }
                        }
                    }
                    #endregion split Overall map power Ratio file to seven
                    sweep_count++;
                    n_val = 0;
                } while ((sd == Inst_Fcu2Asic.SweepDirection.both) && sweep_count < 2);
                //Power_Ratio_Range=Max_Power_Ratio - Min_Power_Ratio;
                return FileName;

            }

            catch (Exception)
            {
                BeginAsyncSweepFlag = false;
                throw;
            }
        }
     }

    public struct OverallMapRange
    {
        public double Max_Power_Ratio;
        public double Min_Power_Ratio;
        public double Power_Ratio_Range;
    }


    class OverallmapSweep : Sweep
    {
        public OverallmapSweep(Chassis_Fcu2Asic instrumentChassis, Inst_Fcu2Asic inst_FCU)
            : base(instrumentChassis, inst_FCU)
        {

        }
    }
    class SupermapSweep : Sweep
    {
        public SupermapSweep(Chassis_Fcu2Asic instrumentChassis, Inst_Fcu2Asic inst_FCU)
            : base(instrumentChassis, inst_FCU)
        {

        }

    }

    interface ISweepFactory
    {
        Sweep CreateSweep(Chassis_Fcu2Asic instrumentChassis, Inst_Fcu2Asic inst_FCU);
    }
    class OverallmapFactory : ISweepFactory
    {

        #region ISweepFactory Members

        public Sweep CreateSweep(Chassis_Fcu2Asic instrumentChassis, Inst_Fcu2Asic inst_FCU)
        {
            return new OverallmapSweep(instrumentChassis, inst_FCU);
        }

        #endregion
    }
    class SupermapFactory : ISweepFactory
    {

        #region ISweepFactory Members

        public Sweep CreateSweep(Chassis_Fcu2Asic instrumentChassis, Inst_Fcu2Asic inst_FCU)
        {
            return new SupermapSweep(instrumentChassis, inst_FCU);
        }

        #endregion
    }

}

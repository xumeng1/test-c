// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_FCU.cs
//
// Author: tommy.yu, 2008
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisNS;
using System.ComponentModel;
using System.Threading;
using System.Text.RegularExpressions;
using System.IO;

namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// Inst_FCU
    /// </summary>
    public partial class Inst_Fcu2Asic
    {

        Sweep OverallmapSweep;
        Sweep SupermapSweep;

        public event ProgressChangedEventHandler OverallmapProgressChanged;
        public event ProgressChangedEventHandler SupermapProgressChanged;
        public enum SweepDirection
        {
            /// <summary>
            /// forward sweep
            /// </summary>
            f,
            /// <summary>
            /// reverse sweep
            /// </summary>
            r,
            both,
        }
        public enum GetOverallMapDataFrom
        {
            /// <summary>
            /// Get Overall Map data from Bias SOA
            /// </summary>
            Bias_SOA,
            /// <summary>
            /// Get Overall Map data from filter and reference
            /// </summary>
            Power_Ratio,
        }
        

        
        //public string OverallmapSync(SweepDirection sd, GetOverallMapDataFrom GDF)
        //{
        //    return OverallmapSync(sd, 1, 5, 1, GDF, Rear_RearSoa_Switch);
        //}

        public string OverallmapSync(SweepDirection sd, int Irear_delaytime_ms, int Irear_jump_delaytime_ms, int Average_Count, int Irear_jump_delayPointCount,GetOverallMapDataFrom GDF, string Rear_RearSoa_Switch)
        {
             if (GDF == GetOverallMapDataFrom.Power_Ratio)
            {

                OverallmapSweep.BeginSweepAsync("asicoverallmap ", sd, Irear_delaytime_ms, Irear_jump_delaytime_ms, Average_Count, Irear_jump_delayPointCount, Rear_RearSoa_Switch);//Jack.Zhang FCU Overall Map for DSDBR (Tx,Rx,Fix(pin12),Var(pin8))
            }
            return OverallmapSweep.EndSweepAsync_FourPort(OverallmapProgressChanged, sd,false);
        }

        private struct MiddleLinePoint
        {
            public int index;
            public int pair_num;
            public float Irear_mA, constval1, variantval;

        }
        public void SetMiddleLine(string smFileName)
        {
            using (StreamReader sr = new StreamReader(smFileName))
            {
                MiddleLinePoint mp = new MiddleLinePoint();
                mp.index = -1;
                while (sr.Peek() > -1)
                {
                    string s = sr.ReadLine();
                    string[] arr = s.Split(',');
                    if (!int.TryParse(arr[1], out mp.pair_num)) continue;
                    mp.index++;
                    mp.Irear_mA = float.Parse(arr[0]);
                    mp.constval1 = float.Parse(arr[2]);
                    mp.variantval = float.Parse(arr[3]);
                    SetMiddleLinePoint(mp);
                }
                sr.Close();
            }

        }
        private void SetMiddleLinePoint(MiddleLinePoint mp)
        {
            //string cmd = string.Format("setmidlinefront {0} {1} {2} {3} {4}",
                //mp.index,mp.Irear_mA, mp.pair_num, mp.constval1, mp.variantval); ;
            string cmd = string.Format("settrack {0} {1} {2} {3} {4}",
                mp.index, mAToDacForRear_Asic(mp.Irear_mA), mp.pair_num, mAToDacForFront_Asic(mp.constval1), mAToDacForFront_Asic(mp.variantval)); ;
            instrumentChassis.Query_Checked(cmd, this);
        }
        /// <summary>
        /// Begin asynchronism sweep
        /// </summary>
        /// <param name="sd"></param>
        /// <param name="delaytime_ms"></param>
        /// <param name="Ip_Delay_PointCount"><defint the frist point delay (Ip_Delay_Time_mS+ Im_Delay_Time_mS) for Lower Iphase>FCUMKI firware update on 2011-09-26
        public void BeginSupermapAsync(SweepDirection sd, int Ip_Delay_Time_mS, int Im_Delay_Time_mS, int SM_Delay_Time_mS,int Average_Time,int Ip_Delay_PointCount, string Rear_RearSoa_Switch)
        {
            
            if (sd == SweepDirection.both)
                SupermapSweep.BeginSweepAsync("asicsupermodemap ", sd, Ip_Delay_Time_mS, Im_Delay_Time_mS, SM_Delay_Time_mS, Average_Time, Ip_Delay_PointCount,Rear_RearSoa_Switch);//SM for DSDBR
            else
                SupermapSweep.BeginSweepAsync("asicsupermodemap ", sd, Ip_Delay_Time_mS, Im_Delay_Time_mS, SM_Delay_Time_mS, Average_Time, Ip_Delay_PointCount, Rear_RearSoa_Switch);//SM for DSDBR
            //SupermapSweep.BeginSweepAsync("asicsupermodemap ", sd, Ip_Delay_Time_mS, Im_Delay_Time_mS, SM_Delay_Time_mS);//SM for Hitt
        }

        /// <summary>
        /// end asynchronism sweep
        /// </summary>
        /// <returns></returns>
        public string EndSupermapAsync(SweepDirection sd, bool useDUTEtalon)
        {
          return SupermapSweep.EndSweepAsync_FourPort(SupermapProgressChanged, sd, useDUTEtalon);
        }

    }
}
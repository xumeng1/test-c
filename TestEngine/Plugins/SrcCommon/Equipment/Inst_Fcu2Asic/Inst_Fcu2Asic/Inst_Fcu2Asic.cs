// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_Fcu2Asic.cs
//
// Author: alice.huang, 2010
// Design: [Reference design documentation]

using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisNS;
using Bookham.TestSolution.Instruments;
using Bookham.TestSolution.IlmzCommonData;

// TODO: Uncomment this once you refer to an instrument type
//using Bookham.TestLibrary.InstrTypes;

namespace Bookham.TestLibrary.Instruments
{
    // TODO: 
    // 1. Add reference to the Instrument type you are implementing.
    // 2. Add reference to the Chassis type you are connecting via.
    // 3. Change the base class to the type of instrument you are implementing.
    // 4. Visual Studio can be used to auto-generate function stubs (hover over the base class name).
    // 5. Fill in the gaps.

    /// <summary>
    /// use Fcu MK1 to control Asic
    /// </summary>
    public partial class Inst_Fcu2Asic : Instrument
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentName">Instrument name</param>
        /// <param name="driverName">Instrument driver name</param>
        /// <param name="slot">Slot ID for the instrument</param>
        /// <param name="subSlot">Sub Slot ID for the instrument</param>
        /// <param name="chassis">Chassis through which the instrument communicates</param>
        public Inst_Fcu2Asic(string instrumentName, string driverName,
            string slot, string subSlot, Chassis chassis)
            : base(instrumentName, driverName, slot, subSlot,
            chassis)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord instrVariant1 = new InstrumentDataRecord(
                " Bookham FCU to Asic",				// hardware name 
                "Firmware_Unknown",  			// minimum valid firmware version 
                "Firmware_Unknown");			// maximum valid firmware version 
            ValidHardwareData.Add("BK_FCU2Asic", instrVariant1);

            // Configure valid chassis driver information
            // TODO: Update with useful key name, real chassis name and versions
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_Fcu2Asic",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "2.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("Chassis_Fcu2Asic", chassisInfo);

            // initialise this instrument's chassis
            // TODO - cast to the appropriate type.
            // don't forget to change the private variable type too.
            this.instrumentChassis = (Chassis_Fcu2Asic)chassis;
            string sweepDataFilePath="";
            SectionsDacCalibration = new AsicCalibrationData();

            ZeroSoaDac = (int) Math.Pow(2, 14);


            //Echo new added followed code
            string CalDataPath = "Data";//create this folder to hold ftCalidata.csv,PtCalidate.csv,RtCalidate.csv files

            if (!Directory.Exists("CalDataPath"))
            {
                //throw new DirectoryNotFoundException("CaliData File Directory isn't exist!");
                Directory.CreateDirectory(CalDataPath);
            }

            CaliDataFileDirectory = CalDataPath;
            SetSH();
            

            ISweepFactory factory = (ISweepFactory)Activator.
                CreateInstance(null, "Bookham.TestLibrary.Instruments.OverallmapFactory").Unwrap();
            OverallmapSweep = factory.CreateSweep(instrumentChassis, this);
            factory = (ISweepFactory)Activator.CreateInstance(null,
                "Bookham.TestLibrary.Instruments.SupermapFactory").Unwrap();
            SupermapSweep = factory.CreateSweep(instrumentChassis, this);

            // Alice.Huang 2010-02-12  commented for debug
            // set sweepDataFilePath
            //if (File.Exists(sweepDataFilePath))
            //{ CheckUpdate(sweepDataFilePath); }

            //else
            //{
            //    throw new InstrumentException(" the sweep data file doesn't exist in " +
            //        sweepDataFilePath);
            //}
        }
        /// <summary>
        /// 
        /// </summary>
        ///
        private string CaliDataFileDirectory;
        private bool CheckUpdateIsOK = false;
        public bool SwapWirebond = false;
        ~Inst_Fcu2Asic()
        {
            if (IsOnline) this.IsOnline = false;//this.Close();
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // TODO: Update
                return "Firmware_Unknown";
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // TODO: Update
                return " Bookham FCU to Asic";
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            string r = instrumentChassis.Query_Checked("setdefaultasic ", this);
        }
        /// <summary>
        /// disable all section's output execept front sections
        /// </summary>
        public void Close()
        {
            LaserEnable = OnOff.off;
            TecEnable = OnOff.off;
            IGain_Dac = 0;
            IRear_Dac = 0;
            IPhase_Dac = 0;
            ISoa_Dac = 0;

        }
        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                bool flag = base .IsOnline;
                //if (instrumentChassis.IsOnline)
                //{
                   
                //}
                return flag;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

               
            }
        }
        #endregion
        /// <summary>
        /// set up asic channel DAC maximun digit and output current
        /// </summary>
        public void SetupChannelCalibration()
        {
            // initialise channel's maximun current & Digit
            this.SetDefaultState();//need reset after FCUMKI power off. jack.zhang 2012-07-26
            this.SetI2CBaud(400);//need reset after FCUMKI power off. jack.zhang 2012-07-26
            ChannelMaxLimit = new StrcChannelMax();
            ChannelMaxLimit.iFrontSectionMax_mA = 10;
            ChannelMaxLimit.iSoaPosMax_mA = 300;
            ChannelMaxLimit.iSoaNegMax_mA = 60;
            ChannelMaxLimit.iGainMax_mA = 180;
            ChannelMaxLimit.iRearLowMax_mA = 90;
            ChannelMaxLimit.iRearHighMax_mA = 180;
            ChannelMaxLimit.iPhaseLowMax_mA = 2;
            ChannelMaxLimit.iPhaseHighMax_mA = 20;
            //private double iFsOdd_mA;
            //private double iFsEven_mA;
            ChannelMaxLimit.iLeftImbLowMax_mA = 15;
            ChannelMaxLimit.iRightImbLowMax_mA = 15;
            ChannelMaxLimit.iLeftImbHighMax_mA = 35;
            ChannelMaxLimit.iRightImbHighMax_mA = 35;
            ChannelMaxLimit.iLeftImbMax_mA = ChannelMaxLimit.iLeftImbLowMax_mA;//use to check Iimb due to IsThermalImb is private bool.
            ChannelMaxLimit.iRightImbMax_mA = ChannelMaxLimit.iRightImbLowMax_mA;
           
            //default is off--low range. jack.zhang 2012-02-15
			/* As the PRE stage has guaranteed that when using ThermalPhase, this ASIC must support ThermalPhase, 
			so no need to check if ASIC can support ThermalPhase again - chongjian.liang - 2013.3.27*/
            if (ParamManager.Conditions.IsThermalPhase)
            {
                ChannelMaxLimit.iLeftImbMax_mA = ChannelMaxLimit.iLeftImbHighMax_mA;
                ChannelMaxLimit.iRightImbMax_mA = ChannelMaxLimit.iRightImbHighMax_mA;
                RangeSW(enumPinName.rightimb, OnOff.on);
                RangeSW(enumPinName.leftimb, OnOff.on);
            }
            else
            {
                RangeSW(enumPinName.rightimb, OnOff.off);
                RangeSW(enumPinName.leftimb, OnOff.off);
            }


            ChannelMaxLimit.iRearSoaMax_mA = 90;
            ChannelMaxLimit.DacMax = (((int)(Math.Pow(2, 14))) - 1);

            // inititialise Asic channels' DAC factors
            SectionsDacCalibration.FrontSoaDac_Neg_CalFactor =
                ChannelMaxLimit.DacMax / ChannelMaxLimit.iSoaNegMax_mA;
            SectionsDacCalibration.FrontSoaDac_Neg_CalOffset = 0;
            SectionsDacCalibration.FrontSoaDac_Pos_CalFactor =
                ChannelMaxLimit.DacMax / ChannelMaxLimit.iSoaPosMax_mA;
            SectionsDacCalibration.FrontSoaDac_Pos_CalOffset = 0;
            SectionsDacCalibration.FsDac_CalFactor =
                ChannelMaxLimit.DacMax / ChannelMaxLimit.iFrontSectionMax_mA;
            SectionsDacCalibration.FsDac_CalOffset = 0;
            SectionsDacCalibration.GainDac_CalFactor =
                ChannelMaxLimit.DacMax / ChannelMaxLimit.iGainMax_mA;
            SectionsDacCalibration.GainDac_CalOffset = 0;
            SectionsDacCalibration.ImbLeftDac_CalFactor =
                ChannelMaxLimit.DacMax / ChannelMaxLimit.iLeftImbMax_mA;
            SectionsDacCalibration.ImbLeftDac_CalOffset = 0;
            SectionsDacCalibration.ImbRightDac_CalFactor =
                ChannelMaxLimit.DacMax / ChannelMaxLimit.iRightImbMax_mA;
            SectionsDacCalibration.ImbRightDac_CalOffset = 0;
            SectionsDacCalibration.PhaseDac_CalFactor =
                ChannelMaxLimit.DacMax / ChannelMaxLimit.iPhaseHighMax_mA;
            SectionsDacCalibration.PhaseDac_CalOffset = 0;
            //RangeSW(enumPinName.phase, OnOff.on);
            this.PhaseSW = OnOff.on;
            SectionsDacCalibration.RearDac_CalFactor =
                ChannelMaxLimit.DacMax / ChannelMaxLimit.iRearLowMax_mA;
            //RangeSW(enumPinName.rear, OnOff.off);// for ASIC5112 make sure rear switch to low range (default).for ASIC5111 must do it to makesure rear current is ok.  jack.zhang 2012-02-15
            SectionsDacCalibration.RearDac_CalOffset = 0;

            SectionsDacCalibration.RearSoaDac_CalFactor =
                ChannelMaxLimit.DacMax / ChannelMaxLimit.iRearSoaMax_mA;
            SectionsDacCalibration.RearSoaDac_CalOffset = 0;

        }

        public int mAToDacForFront_Asic(float value)
        {
            if (value >= 0)
            {
                if (value > ChannelMaxLimit.iFrontSectionMax_mA) value = (float)ChannelMaxLimit.iFrontSectionMax_mA;

                int dacTemp = (int)(value *
                    SectionsDacCalibration.FsDac_CalFactor +
                    SectionsDacCalibration.FsDac_CalOffset);
                return dacTemp;
            }
            else
            {
                return 0;
            }
        }
        public int mAToDacForRear_Asic(float value)
        {
            if (value >= 0)
            {
                if (value > ChannelMaxLimit.iRearLowMax_mA) value = (float)ChannelMaxLimit.iRearLowMax_mA;

                int dacTemp = (int)(value *
                    SectionsDacCalibration.RearDac_CalFactor +
                    SectionsDacCalibration.RearDac_CalOffset);
                return dacTemp;
            }
            else
            {
                return 0;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ValidateFlag
        {
            get { return instrumentChassis.ValidateFlag; } 
            set { instrumentChassis.ValidateFlag = value; }
        }

        #region Front section part
        /// <summary>
        /// set/get current on the odd pin in current selected front sections pair
        /// </summary>

        public float IOdd_mA
        {
            set
            {
                int fs;
                if (fsPairNumber == 0) throw new ArgumentException("Front pair number is 0, it should be 1~7, can't set odd current");
                if (value <0) throw new ArgumentOutOfRangeException(" front section Odd current should be greater than  or equal to 0");
                if (value > ChannelMaxLimit.iFrontSectionMax_mA ) value =(float ) ChannelMaxLimit.iFrontSectionMax_mA ;

                if (IsEven(fsPairNumber)) fs = fsPairNumber + 1;
                else fs = fsPairNumber;

                int dacTemp = (int)Math.Round((value * SectionsDacCalibration.FsDac_CalFactor +
                    SectionsDacCalibration.FsDac_CalOffset));

                enumPinName fsSection = (enumPinName)Enum.Parse(typeof(enumPinName), fs.ToString());
                string r = SetDac(fsSection, dacTemp);

                //IOdd_Dac = dacTemp;
                iFsOdd_mA = value;
            }
            get
            {
                if (fsPairNumber == 0) return 0;
                //else return (float)((iFsOdd_Dac - SectionsDacCalibration.FsDac_CalOffset) / SectionsDacCalibration.FsDac_CalFactor);
                else return iFsOdd_mA;
            }
        }
        /// <summary>
        /// set/get current on the even pin in current selected front sections pair
        /// </summary>
        public float IEven_mA
        {
            set
            {
                int fs;
                if (fsPairNumber == 0) throw new ArgumentException("Front pair number is 0, it should be 1~7, can't set Even current");
                if (value < 0) throw new ArgumentOutOfRangeException(" front section Even current should be greater than  or equal to 0");
                if (value > ChannelMaxLimit.iFrontSectionMax_mA) value = (float)ChannelMaxLimit.iFrontSectionMax_mA;

                if (IsEven(fsPairNumber)) fs = fsPairNumber;
                else fs = fsPairNumber + 1;

                int dacTemp = (int)Math.Round((value * SectionsDacCalibration.FsDac_CalFactor +
                    SectionsDacCalibration.FsDac_CalOffset));

                enumPinName fsSection = (enumPinName)Enum.Parse(typeof(enumPinName), fs.ToString());
                string r = SetDac(fsSection, dacTemp);

                //IEven_Dac = dacTemp;
                iFsEven_mA = value;
            }
            get
            {
                if (fsPairNumber == 0) return 0;
                //else return (float)((iFsEven_Dac - SectionsDacCalibration.FsDac_CalOffset) / SectionsDacCalibration.FsDac_CalFactor);
                else return iFsEven_mA;
            }
        }
        /// <summary>
        /// Get/set front pair odd section current Dac
        /// </summary>
        public int IOdd_Dac
        {
            get 
            { 
                return iFsOdd_Dac; 
            }
            set 
            {
                int fs;
                if (fsPairNumber == 0) throw new ArgumentException("Front pair number is 0, it should be 1~7, can't set odd current");
                if (value < 0) throw new ArgumentOutOfRangeException(" front section Odd current Dac should be greater than  or equal to 0");
                if (value > ChannelMaxLimit.DacMax) value = ChannelMaxLimit.DacMax;
    
                if (IsEven(fsPairNumber)) fs = fsPairNumber + 1;
                else fs = fsPairNumber;
                
                enumPinName fsSection = (enumPinName)Enum.Parse(typeof(enumPinName), fs.ToString());
                string r = SetDac(fsSection, value);
                iFsOdd_Dac = value;                 
            }
        }
        /// <summary>
        /// Get/set front pair even section current dac
        /// </summary>
        public int IEven_Dac
        {
            get { return iFsEven_Dac; }
            set 
            {
                int fs;
                if (fsPairNumber == 0) throw new ArgumentException("Front pair number is 0, it should be 1~7, can't set Even current");

                if (value < 0) throw new ArgumentOutOfRangeException(" front section Even current Dac should be greater than  or equal to 0");
                if (value > ChannelMaxLimit.DacMax) value = ChannelMaxLimit.DacMax;

                if (IsEven(fsPairNumber)) fs = fsPairNumber;
                else fs = fsPairNumber + 1;

                enumPinName fsSection = (enumPinName)Enum.Parse(typeof(enumPinName), fs.ToString());
                string r = SetDac(fsSection, value);
                iFsEven_Dac = value; 
            }
        }
        /// <summary>
        ///  Front pair number
        /// </summary>
        /// <returns></returns>
        public int GetFrontPairNumber()
        {
            return fsPairNumber;
        }
       
        /// <summary>
        /// select a front sections pair to apply current to
        /// </summary>
        /// <param name="frontPairNum">front pair number</param>
        /// <param name="constantCurrent"> </param>
        /// <param name="nonConstantCurrent"> </param>
        public void SetFrontSectionPairCurrent(int frontPairNum, float constantCurrent,
            float nonConstantCurrent)
        {
            if (!pairnums.Contains(frontPairNum))
            {
                throw new IndexOutOfRangeException();
            }
            /*
             *  FrontPair   |   Contant |   nonContant
             *  - - -- - -- -  -- - - - - - - - - - - -
             *  1           |   1       |   2
             *  2           |   2       |   3
             *  3           |   3       |   4
             *  4           |   4       |   5
             *  5           |   5       |   6
             *  6           |   6       |   7
             *  7           |   7       |   8
             */

            if (constantCurrent > ChannelMaxLimit.iFrontSectionMax_mA) constantCurrent = (float)ChannelMaxLimit.iFrontSectionMax_mA;
            if (nonConstantCurrent > ChannelMaxLimit.iFrontSectionMax_mA) nonConstantCurrent = (float)ChannelMaxLimit.iFrontSectionMax_mA;
            fsPairNumber = frontPairNum;

            if (fsPairNumber % 2 != 0)
            {
                // Constant current apply to odd section
                IOdd_mA =(float) constantCurrent;
                IEven_mA =(float) nonConstantCurrent;
            }
            else
            {
                // Constant current apply to even section
                IEven_mA =(float ) constantCurrent;
                IOdd_mA =(float ) nonConstantCurrent ;
            }           

        }

        
        /// <summary>
        /// make judge wether a number is even or not
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        private bool IsEven(int number)
        {
            return (number % 2) == 0;
        }
        /// <summary>
        /// set/get which switches to be used in FCU
        /// </summary>
        private int Switches
        {
            get
            {
                string r = instrumentChassis.Query_Checked("getswitches", this);
                return int.Parse(r.Split(' ')[1]);
            }
            set
            {
                string sSend = "setswitches " + value;
                string r = instrumentChassis.Query_Checked(sSend, this);
            }
        }
        // i do not wnat this interface access by public because the fcu
        // firmware changed. bob.lv 2009-02-20
        /// <summary>
        /// Read Front Section Current mA
        /// </summary>
        /// <param name="Section">Section number,from 1 to 8</param>
        /// <returns>Current value,mA</returns>
        public float ReadFrontSectionCurrent_mA(int Section)
        {
            if (Section < 1 || Section > 8)
                throw new Exception("Section number should be from 1 to 8");
            int[] pairnumbers ={
                0,  //f1,f2 selected
                4,  //f2,f3 selected
                5,  //f3,f4 selected
                9,  //f4,f5 selected
                10, //f5,f6 selected
                14, //f6,f7 selected
                15  //f7,f8 selected
            };
            float Current_mA;
            if (IsEven(Section))
            {
                Switches = pairnumbers[Section - 2];
                Current_mA = IEven_mA;
            }
            else
            {
                Switches = pairnumbers[Section - 1];
                Current_mA = IOdd_mA;
            }
            return Current_mA;
        }
        private int GetAdc(AdcName name)
        {
            string s = string.Format("getadc {0}", name);
            string r = instrumentChassis.Query_Checked(s, this);
            return int.Parse(r.Split(' ')[2]);
        }
        

        // i do not wnat this interface access by public because the fcu
        // firmware changed. bob.lv 2009-02-20
        /// <summary>
        /// Set Front Section Current mA
        /// </summary>
        /// <param name="Section">Section number,from 1 to 8</param>
        /// <param name="Current_mA">Current value,mA</param>
        public void SetFrontSectionCurrent_mA(int Section, float Current_mA)
        {
            if (Section < 1 || Section > 8)
                throw new Exception("Section number should be from 1 to 8");
            int[] pairnumbers ={
                0,  //f1,f2 selected
                4,  //f2,f3 selected
                5,  //f3,f4 selected
                9,  //f4,f5 selected
                10, //f5,f6 selected
                14, //f6,f7 selected
                15  //f7,f8 selected
            };
            if (IsEven(Section))
            {
                Switches = pairnumbers[Section - 2];
                IEven_mA = Current_mA;
            }
            else
            {
                Switches = pairnumbers[Section - 1];
                IOdd_mA = Current_mA;
            }
        }
        public string SetFrontCurrent_mA(int FN, float Current_mA)
        {
            float IMax = 10;
            int Dac = (int)(Math.Round(Current_mA * 16383 / IMax));
            string s = SetFrontSectionDacbyAsic(FN, Dac);
            string r = instrumentChassis.Query_Checked(s, this);
            return r;
        }

        private string SetFrontSectionDacbyAsic(int FN, int Dac)
        {
            string s = string.Format("setfsbyasic {0} {1}", FN, Dac);
            string r = instrumentChassis.Query_Checked(s, this);
            return r;
        }
        private string SetFrontPairDacbyAsic(int FP, int ConDac, int NonConDac)
        {
            string s = string.Format("setpairbyasic {0} {1} {2}", FP, ConDac, NonConDac);
            string r = instrumentChassis.Query_Checked(s, this);
            return r;
        }
        public string SetFrontPairCurrent_mA(int FN, float ConCurrent, float Current_mA)
        {
            float IMax = 10;
            int ConDac = (int)(Math.Round(ConCurrent * 16383 / IMax));
            int Dac = (int)(Math.Round(Current_mA * 16383 / IMax));
            string s = SetFrontPairDacbyAsic(FN, ConDac, Dac);
            string r = instrumentChassis.Query_Checked(s, this);
            return r;
        }
        
        #endregion
        #region other sections
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sectionName">Tosa's current soutce pin</param>
        /// <param name="Dac"> Dac value </param>
        /// <returns></returns>
        private string SetDac(enumPinName sectionName, int Dac)
        {
            string comStr="";
            switch (sectionName)
            {
                case enumPinName.FrontSection1:
                case enumPinName.FrontSection2:
                case enumPinName.FrontSection3:
                case enumPinName.FrontSection4:
                case enumPinName.FrontSection5:
                case enumPinName.FrontSection6:
                case enumPinName.FrontSection7:
                case enumPinName.FrontSection8:
                    //ˇ°setfsbyasic <front_no> <dac_val>ˇ±
                    comStr = string.Format("setfsbyasic {0} {1}",sectionName.ToString("d"),Dac );
                    break;
                case enumPinName.gain:
                case enumPinName.rightimb:
                case enumPinName.leftimb :
                case enumPinName .phase :
                case enumPinName.rearsoa:
                case enumPinName.rear :
                    // ˇ°setasicdac <dacname> <dac_value>ˇ±
                    if (SwapWirebond)
                    {
                        if (sectionName == enumPinName.rear) comStr = string.Format("setasicdac {0} {1}", "rearsoa", Dac);
                        else if (sectionName == enumPinName.rearsoa) comStr = string.Format("setasicdac {0} {1}", "rear", Dac);
                        else comStr = string.Format("setasicdac {0} {1}", sectionName, Dac);
                    }
                    else
                    {
                        comStr = string.Format("setasicdac {0} {1}", sectionName, Dac);
                    }
                    break;

                case enumPinName.soaIneg:
                    //ˇ°setnegativesoa <dac_value>ˇ±
                    comStr = string.Format("setnegativesoa {0}", Dac);
                    break;
                case enumPinName.soaIpos :
                // ˇ°setpositivesoa <dac_value>ˇ±
                    
                    comStr = string.Format("setpositivesoa {0}", Dac);
                    break;    
                default :
                    throw new ArgumentException(" no avalable section dac value to set for " + sectionName.ToString());
            }

#if (DEBUG)  
                Console.WriteLine(comStr );
#endif
                System.Threading.Thread.Sleep(2);
            string rspStr = instrumentChassis.Query_Checked(comStr, this);

#if (DEBUG)
            Console .WriteLine (rspStr );
#endif
            return rspStr;
        }
        private int GetDac(enumPinName sectionName)
        {
            throw new InstrumentException(" not implemented for this instrument");
            //string comStr = string.Format("getdac {0}", sectionName);
            //string rspStr = instrumentChassis.Query_Checked(comStr, this);
            //return int.Parse(rspStr.Split(' ')[2]);
        }        
        /// <summary>
        /// Set/get Gain unit:Dac
        /// </summary>
        public int IGain_Dac
        {
            set
            {
                if (value > ChannelMaxLimit.DacMax) value = ChannelMaxLimit.DacMax;
                string rspStr = SetDac(enumPinName.gain, value);
                iGain_Dac = value;
            }
            get
            {
                return iGain_Dac;
            }
        }
        /// <summary>
        /// set/get rear dac value
        /// </summary>
        public int IRear_Dac
        {
            set
            {
                if (value > ChannelMaxLimit.DacMax) value = ChannelMaxLimit.DacMax;
                string rspStr = SetDac(enumPinName.rear, value);
                iRear_Dac = value;
            }
            get
            {
                return iRear_Dac;
            }
        }
        /// <summary>
        /// set/get Front Soa dac value
        /// </summary>
        public int ISoa_Dac
        {
            set
            {
                string rspStr;
                if (value >= ZeroSoaDac)
                {
                    if (value > (ChannelMaxLimit.DacMax + ZeroSoaDac)) value = ChannelMaxLimit.DacMax;
                    rspStr = SetDac(enumPinName.soaIpos, value - ZeroSoaDac);
                }
                else
                {
                    if (value < 1 ) value = 1 ;
                    // for soa in neg, 0mA' dac is 0
                    // 60mA's dac is 16383
                    rspStr = SetDac(enumPinName.soaIneg, Math.Abs(value - ZeroSoaDac));
                }
                iSoa_Dac = value;
            }
            get
            {
                return iSoa_Dac;
            }
        }
        /// <summary>
        /// set/get Gain dac value
        /// </summary>
        public int IPhase_Dac
        {
            set
            {
                if (value > ChannelMaxLimit.DacMax) value = ChannelMaxLimit.DacMax;
                string rspStr = SetDac(enumPinName.phase, value);
                iPhase_Dac = value;
            }
            get
            {
                return iPhase_Dac;
            }
        }
        /// <summary>
        /// set/get imbLeft Dac value
        /// </summary>
        public int IimbLeft_Dac
        {
            set 
            {
                if (value > ChannelMaxLimit.DacMax) value = ChannelMaxLimit.DacMax;
                string rspStr = SetDac(enumPinName.leftimb, value);
                iLeftImb_Dac = value;
            }
            get 
            {                
                return iLeftImb_Dac;
            }
        }
        /// <summary>
        /// set/get imb Right Dac value
        /// </summary>
        public int IimbRight_Dac
        {
            set 
            {
                if (value > ChannelMaxLimit.DacMax) value = ChannelMaxLimit.DacMax;
                string rspStr = SetDac(enumPinName.rightimb, value);
                iRightImb_Dac = value;
            }
            get 
            {
                return iRightImb_Dac;
            }
        }
        /// <summary>
        /// Get/Set Rear Soa DAC value
        /// </summary>
        public int IRearSoa_Dac
        {
            get 
            {
                return iRearSoa_Dac;
            }
            set 
            {
                if (value > ChannelMaxLimit.DacMax) value = ChannelMaxLimit.DacMax;
                string rspStr = SetDac(enumPinName.rearsoa, value);
                iRearSoa_Dac = value;
            }
        }

        /// <summary>
        /// set/get rear current source's low range on status
        /// on: 0-11 mA
        /// off:0-90 mA
        /// </summary>
        public OnOff RearSW
        {
            set
            {
                string comStr="setrearsw " + value.ToString();
                string rspStr = instrumentChassis.Query_Checked(comStr, this);
#if (DEBUG)
                Console.WriteLine(comStr);
                Console.WriteLine(rspStr);
#endif
            }
            get
            {
                string rspStr = instrumentChassis.Query_Checked("getrearsw", this);
                return (OnOff)Enum.Parse(typeof(OnOff), rspStr.Split(' ')[1]);
            }
        }
        /// <summary>
        /// set/get rear current source's low range on status
        /// on: 0-20 mA
        /// off:0-2 mA
        /// </summary>
        public OnOff PhaseSW
        {
            set
            {
                string comStr= "setphasesw " + value.ToString();
                string rspStr = instrumentChassis.Query_Checked(comStr, this);
#if (DEBUG)
                Console.WriteLine(comStr);
                Console.WriteLine(rspStr);
                highPhaseRange = value;

                if (value == OnOff.on)
                {
                    SectionsDacCalibration.PhaseDac_CalFactor = ChannelMaxLimit.DacMax / ChannelMaxLimit.iPhaseHighMax_mA;
                }
                else
                {
                    SectionsDacCalibration.PhaseDac_CalFactor = ChannelMaxLimit.DacMax / ChannelMaxLimit.iPhaseLowMax_mA;
                }
               
#endif
            }
            get
            {
                return highPhaseRange;
            }
        }
        /// <summary>
        /// set/get current source's low range on status just for Imb Rear and Phase in ASIC5112
        /// on: 0-20 mA (phase); 0-35 mA(Imb);  0-180mA (Rear)  
        /// off:0-2 mA(Phase);  0-15mA(Imb);    0-90mA (Rear)
        /// Jack.zhang 2012-02-09
        /// </summary>
        private string RangeSW(enumPinName sectionName, OnOff status)
        {
            string comStr = string.Format("setrangesw {0} {1}", sectionName, status.ToString());
            Console.WriteLine(comStr);
            string rspStr = instrumentChassis.Query_Checked(comStr, this);
            Console.WriteLine(rspStr);
            switch (sectionName)
            {
                case enumPinName.rightimb:
                    if (status == OnOff.on)
                    {
                        SectionsDacCalibration.ImbRightDac_CalFactor = ChannelMaxLimit.DacMax / ChannelMaxLimit.iRightImbHighMax_mA;
                    }
                    else
                    {
                        SectionsDacCalibration.ImbRightDac_CalFactor = ChannelMaxLimit.DacMax / ChannelMaxLimit.iRightImbLowMax_mA;
                    }
                    break;
                case enumPinName.leftimb:
                    if (status == OnOff.on)
                    {
                        SectionsDacCalibration.ImbLeftDac_CalFactor = ChannelMaxLimit.DacMax / ChannelMaxLimit.iLeftImbHighMax_mA;
                    }
                    else
                    {
                        SectionsDacCalibration.ImbLeftDac_CalFactor = ChannelMaxLimit.DacMax / ChannelMaxLimit.iLeftImbLowMax_mA;
                    }
                    break;
                case enumPinName.phase:
                    if (status == OnOff.on)
                    {
                        SectionsDacCalibration.PhaseDac_CalFactor = ChannelMaxLimit.DacMax / ChannelMaxLimit.iPhaseHighMax_mA;
                    }
                    else
                    {
                        SectionsDacCalibration.PhaseDac_CalFactor = ChannelMaxLimit.DacMax / ChannelMaxLimit.iPhaseLowMax_mA;
                    }
                    break;
                case enumPinName.rear:
                    if (status == OnOff.on)
                    {
                        SectionsDacCalibration.RearDac_CalFactor = ChannelMaxLimit.DacMax / ChannelMaxLimit.iRearHighMax_mA;
                    }
                    else
                    {
                        SectionsDacCalibration.RearDac_CalFactor = ChannelMaxLimit.DacMax / ChannelMaxLimit.iRearLowMax_mA;
                    }
                    break;
            }
            return rspStr;
        }
        /// <summary>
        /// 
        /// </summary>
        public OnOff TecEnable
        {
            set
            {
                string comStr= "settec " + value.ToString();
                string rspStr = instrumentChassis.Query_Checked(comStr, this);
#if (DEBUG)
                Console.WriteLine(comStr);
                Console.WriteLine(rspStr);
#endif
            }
            get
            {
                string rspStr = instrumentChassis.Query_Checked("gettec", this);
                return (OnOff)Enum.Parse(typeof(OnOff), rspStr.Split(' ')[1]);

            }
        }
        /// <summary>
        /// 
        /// </summary>
        public OnOff LaserEnable
        {
            set
            {
                if (value == OnOff.off)
                {
                    IGain_mA = 0;
                    IEven_mA = 0;
                    IOdd_mA = 0;
                    IRear_mA = 0;
                    IPhase_mA = 0;
                    ISoa_mA = 0;
                }
            }
        }
        /// <summary>
        /// disable all pin's output
        /// </summary>
        public void  DisableAllOutput()
        {           
            string comStr = "closeallasic";
            string rspStr = instrumentChassis.Query_Checked(comStr, this);

#if (DEBUG)
            Console.WriteLine(comStr);
            Console.WriteLine(rspStr);
#endif
           
        }

        public int mAToDacForGain_Asic(float value)
        {
            if (value >= 0)
            {
                if (value > ChannelMaxLimit.iGainMax_mA) value = (float)ChannelMaxLimit.iGainMax_mA;
                int dacTemp = (int)(value *
                    SectionsDacCalibration.GainDac_CalFactor +
                    SectionsDacCalibration.GainDac_CalOffset);

                return dacTemp;
            }
            else
            {
                return 0;
            }
        }
        public int mAToDacForFrontSoa_Asic(float value)
        {
            int dacTemp;
            if (value >= 0)
            {
                if (value > ChannelMaxLimit.iSoaPosMax_mA) value = (float)ChannelMaxLimit.iSoaPosMax_mA;
                dacTemp = (int)(value *
                    SectionsDacCalibration.FrontSoaDac_Pos_CalFactor +
                    SectionsDacCalibration.FrontSoaDac_Pos_CalOffset) + ZeroSoaDac;
            }
            else
            {
                if (value < (0 - ChannelMaxLimit.iSoaNegMax_mA)) value = (float)(ChannelMaxLimit.iSoaNegMax_mA);
                dacTemp = (int)(value *
                    SectionsDacCalibration.FrontSoaDac_Neg_CalFactor +
                    SectionsDacCalibration.FrontSoaDac_Neg_CalOffset) + ZeroSoaDac;
            }
            return dacTemp;
        }
        public int mAToDacForLeftImb_Asic(float value)
        {
            int dacTemp;
            if (value >= 0)
            {
                if (value > ChannelMaxLimit.iLeftImbMax_mA) value = (float)ChannelMaxLimit.iLeftImbMax_mA;
                dacTemp = (int)(value *
                    SectionsDacCalibration.ImbLeftDac_CalFactor +
                    SectionsDacCalibration.ImbLeftDac_CalOffset);
                return dacTemp;
            }
            else
            {
                return 1; //return 1 dac not 0 dac;
            }
        }

        public float Dac2mAForLeftImb_Asic(int value)
        {
            float mATemp;
            if (value >= 1)
            {
               
                mATemp = (float)((value - SectionsDacCalibration.ImbLeftDac_CalOffset) / SectionsDacCalibration.ImbLeftDac_CalFactor);

                return mATemp;
            }
            else
            {
                return 0; //Echo , 2011-06-17
            }
        }

        public int mAToDacForRightImb_Asic(float value)
        {
            int dacTemp;
            if (value >= 0)
            {
                if (value > ChannelMaxLimit.iRightImbMax_mA) value = (float)ChannelMaxLimit.iRightImbMax_mA;
                dacTemp = (int)(value *
                    SectionsDacCalibration.ImbRightDac_CalFactor +
                    SectionsDacCalibration.ImbRightDac_CalOffset);
                return dacTemp;
            }
            else
            {
                return 1; //return 1 not 0 dac, Echo 2011-06-17
            }
        }
        /// <summary>
        /// Get/Set Gain current setting
        /// </summary>
        public float IGain_mA
        {
            set
            {
                if (value >= 0)
                {
                    if (value > ChannelMaxLimit.iGainMax_mA) value = (float) ChannelMaxLimit.iGainMax_mA;
                    int dacTemp = (int)(value *
                        SectionsDacCalibration.GainDac_CalFactor +
                        SectionsDacCalibration.GainDac_CalOffset);

                    IGain_Dac = dacTemp;
                    iGain_mA = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException(" Gain current should be greater than  or equal to 0");
                }
            }
            get
            {
                return (float )( (iGain_Dac - SectionsDacCalibration .GainDac_CalOffset )/
                    SectionsDacCalibration .GainDac_CalFactor );
            }
        }
        /// <summary>
        /// Get/set rear current setting
        /// </summary>
        public float IRear_mA
        {
            set
            {
                if (value >= 0)
                {

                    if (value > ChannelMaxLimit.iRearLowMax_mA) value = (float)ChannelMaxLimit.iRearLowMax_mA;
                    int dacTemp = (int)(value *
                        SectionsDacCalibration.RearDac_CalFactor +
                        SectionsDacCalibration.RearDac_CalOffset);
                    IRear_Dac = dacTemp;
                    iRear_mA = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException(" Rear current should be greater than  or equal to 0");
                }
            }
            get
            {
                return (float) ((iRear_Dac -SectionsDacCalibration .RearDac_CalOffset )/
                    SectionsDacCalibration .RearDac_CalFactor) ;
               
            }
        }
        /// <summary>
        /// Get/set front soa current settings
        /// </summary>
        public float ISoa_mA
        {
            set
            {
                int dacTemp;
                if (value >= 0)
                {
                    if (value > ChannelMaxLimit.iSoaPosMax_mA) value = (float) ChannelMaxLimit.iSoaPosMax_mA;
                    dacTemp = (int)(value *
                        SectionsDacCalibration.FrontSoaDac_Pos_CalFactor +
                        SectionsDacCalibration.FrontSoaDac_Pos_CalOffset) + ZeroSoaDac;
                }
                else
                {
                    if (value < (0 - ChannelMaxLimit.iSoaNegMax_mA)) value = (float ) ( ChannelMaxLimit.iSoaNegMax_mA);
                    dacTemp =(int)(value *
                        SectionsDacCalibration.FrontSoaDac_Neg_CalFactor +
                        SectionsDacCalibration.FrontSoaDac_Neg_CalOffset) + ZeroSoaDac;
                }
                //string rspStr = SetCurrent_mA(enumPinName.soaIpos, value);
                ISoa_Dac = dacTemp;
                iSoa_mA = value;
            }
            get
            {
                double curtemp;
                if (iSoa_Dac >= ZeroSoaDac)
                {
                    
                    curtemp = (iSoa_Dac - ZeroSoaDac - SectionsDacCalibration.FrontSoaDac_Pos_CalOffset) /
                        SectionsDacCalibration.FrontSoaDac_Pos_CalFactor;
                }
                else
                {
                    curtemp = (iSoa_Dac - ZeroSoaDac - SectionsDacCalibration.FrontSoaDac_Neg_CalOffset) /
                        SectionsDacCalibration.FrontSoaDac_Neg_CalFactor;
                }
                return (float)curtemp;
            }
        }
        
       
       /// <summary>
       /// set or get left bias voltage (0~-8v)
       /// </summary>
        public float VLeftModBias_Volt
        {
            set
            {
                string Volt = Math.Round(value, 3).ToString();//To avoid FCUMKI output wrong Volt due to long digital(ie -2.400000001545V)
                string s = string.Format("setv left {0}", Volt);
                string r = instrumentChassis.Query_Checked(s, this);

            }
            get
            {
                string s = string.Format("getv left");
                string r = instrumentChassis.Query_Checked(s, this);
                return float.Parse(r.Split(' ')[2]);
            }
        }
        /// <summary>
        /// set/get right bias voltage (0~-8V)
        /// </summary>
        public float VRightModBias_Volt
        {
            set
            {
                string Volt = Math.Round(value, 3).ToString();//To avoid FCUMKI output wrong Volt due to long digital(ie -2.400000001545V)
                string s = string.Format("setv right {0}", Volt);
                string r = instrumentChassis.Query_Checked(s, this);
            }
            get
            {
                string s = string.Format("getv right");
                string r = instrumentChassis.Query_Checked(s, this);
                return float.Parse(r.Split(' ')[2]);
            }
        }

        // <summary>
        /// set or get left imbalance voltage (0~-8v)
        /// </summary>
        public double VImbLeft_Volt
        {
            set
            {
                string s = string.Format("setv leftimb {0}", value.ToString());
                string r = instrumentChassis.Query_Checked(s, this);

            }
            get
            {
                string s = string.Format("getv leftimb");
                string r = instrumentChassis.Query_Checked(s, this);
                return double.Parse(r.Split(' ')[2]);
            }
        }
        /// <summary>
        /// set/get right imbalance voltage (0~-8V)
        /// </summary>
        public double VImbRight_Volt
        {
            set
            {
                string s = string.Format("setv rightimb {0}", value.ToString());
                string r = instrumentChassis.Query_Checked(s, this);
            }
            get
            {
                string s = string.Format("getv rightimb");
                string r = instrumentChassis.Query_Checked(s, this);
                return double.Parse(r.Split(' ')[2]);
            }
        }
        public int mAToDacForPhase_Asic(float value)
        {
            if (value >= 0)
            {
                if (value > ChannelMaxLimit.iPhaseHighMax_mA) value = (float)ChannelMaxLimit.iPhaseHighMax_mA;

                int dacTemp = (int)(value *
                    SectionsDacCalibration.PhaseDac_CalFactor +
                    SectionsDacCalibration.PhaseDac_CalOffset);
                return dacTemp;
            }
            else
            {
                return 0;
            }

        }

        /// <summary>
        /// Get/set Phase current setting
        /// </summary>
        public float IPhase_mA
        {
            set
            {
                int dacTemp;

                if (value >= 0)
                {
                    //RangeSW(enumPinName.phase, OnOff.on);

                    this.PhaseSW = OnOff.on;
                    if (value > ChannelMaxLimit.iPhaseHighMax_mA) value = (float)ChannelMaxLimit.iPhaseHighMax_mA;
                    dacTemp = (int)(value *
                        SectionsDacCalibration.PhaseDac_CalFactor +
                        SectionsDacCalibration.PhaseDac_CalOffset);
                    IPhase_Dac = dacTemp;
                    iPhase_mA = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException(" Phase current should be greater than  or equal to 0");
                }
            }
            get
            {
                return (float) ((iPhase_Dac - SectionsDacCalibration .PhaseDac_CalOffset )/
                    SectionsDacCalibration .PhaseDac_CalFactor) ;
            }
        }
        /// <summary>
        /// Get/Set Left imb current setting
        /// </summary>
        public float IimbLeft_mA
        {
            set
            {
                int dacTemp;
                if (value >= 0)
                {
                    if (value > ChannelMaxLimit.iLeftImbMax_mA) value = (float) ChannelMaxLimit.iLeftImbMax_mA;
                    dacTemp = (int)(value *
                        SectionsDacCalibration.ImbLeftDac_CalFactor +
                        SectionsDacCalibration.ImbLeftDac_CalOffset);
                    IimbLeft_Dac = dacTemp;
                    iLeftImb_mA = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException(" Left imb current should be greater than  or equal to 0");
                }
            }
            get
            {
                double reading = (iLeftImb_Dac - SectionsDacCalibration.ImbLeftDac_CalOffset) /
                    SectionsDacCalibration.ImbLeftDac_CalFactor;
                return (float)reading;
                //string s = string.Format("geti {0}", enumPinName.leftimb);
                //string r = instrumentChassis.Query_Checked(s, this);
                //return float.Parse(r.Split(' ')[2]);

                
            }
        }
        /// <summary>
        /// get/Set Right imb current setting
        /// </summary>
        public float IimbRight_mA
        {
            set
            {
                int dacTemp;
                if (value >= 0)
                {
                    if (value > ChannelMaxLimit.iRightImbMax_mA) value = (float) ChannelMaxLimit.iRightImbMax_mA;
                    dacTemp = (int)(value *
                        SectionsDacCalibration.ImbRightDac_CalFactor  +
                        SectionsDacCalibration.ImbRightDac_CalOffset);
                    IimbRight_Dac = dacTemp;
                    iRightImb_mA = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException(" Right imb current should be greater than  or equal to 0");
                }
            }
            get
            {
                double reading = (iRightImb_Dac - SectionsDacCalibration.ImbRightDac_CalOffset) /
                    SectionsDacCalibration.ImbRightDac_CalFactor;

                return (float)reading;
                //string s = string.Format("geti {0}", enumPinName.rightimb);
                //string r = instrumentChassis.Query_Checked(s, this);
                //return float.Parse(r.Split(' ')[2]);

            }
        }

        public int mAToDacForRearSoa_Asic(float value)
        {
            if (value > ChannelMaxLimit.iRearSoaMax_mA) value = (float)ChannelMaxLimit.iRearSoaMax_mA;
            if (value >= 0)
            {
                int dactemp;
                dactemp = (int)(value * SectionsDacCalibration.RearSoaDac_CalFactor +
                    SectionsDacCalibration.RearSoaDac_CalOffset);
                return dactemp;
            }
            else
            {
                return 0;
            }

        }


        /// <summary>
        /// get/get Rear Sos current seeting
        /// </summary>
        public double IRearSoa_mA
        {
            get
            {
                double reading = (iRearSoa_Dac - SectionsDacCalibration.RearSoaDac_CalOffset) /
                    SectionsDacCalibration.RearSoaDac_CalFactor;
                return reading;
            }
            set
            {
                if (value > ChannelMaxLimit.iRearSoaMax_mA) value = (float)ChannelMaxLimit.iRearSoaMax_mA;
                if (value >= 0)
                {
                    int dactemp;
                    dactemp = (int)(value * SectionsDacCalibration.RearSoaDac_CalFactor +
                        SectionsDacCalibration.RearSoaDac_CalOffset);
                    IRearSoa_Dac = dactemp;
                    iRearSoa_mA = value;

                }
                else
                {
                    throw new ArgumentOutOfRangeException(" Right imb current should be greater than  or equal to 0");
                }

            }
        }

        #endregion

        #region Adc Part
        /// <summary>
        ///  get/set the reading interval mS between each reading for each adc monitor channel
        /// </summary>
        public int AdcReadInterval_mS
        {
            get { return adcReadInterval_mS; }
            set { adcReadInterval_mS = value; }
        }
        /// <summary>
        /// set/get the average fator for each ADC monitor channel reading
        /// </summary>
        public int AdcReadAverageFactor
        {
            get { return adcReadAverageFactor; }
            set { adcReadAverageFactor = value; }
        }
        /// <summary>
        /// set and monitor channel's ADC Calibration factor
        /// </summary>
        /// <param name="adcChan"> adc monitor channel </param>
        /// <param name="calValue">calibration factor </param>
        public string SetAdcChannelCalibrate(EnumAdcCalibrate adcChan, int calValue)
        {
            // setpot dpotname value;
            if ((calValue < 0) || (calValue > 255))
                throw new ArgumentException(string.Format(
                    "trying to set invalid data of {0} to {1} adc calibrate factor\n,the calibrate factor should be 0~255", calValue, adcChan));

            string comstr = string.Format("setpot {0} {1}", adcChan, calValue);
            string rspStr = instrumentChassis.Query_Checked(comstr, this);
            return rspStr;
        }
        /// <summary>
        /// get and monitor channel's ADC Calibration factor
        /// </summary>
        /// <param name="adcChan">select a channel to get its calibration factor</param>
        /// <returns>digital number after zero for this channel's reading </returns>
        public int GetAdcChannelCalibrate(EnumAdcCalibrate adcChan)
        {
            int reading = 0;

            //getpot dpotname
            string comstr = string.Format("getpot {0}", adcChan);
            string rspStr = instrumentChassis.Query_Checked(comstr, this);
            reading = int.Parse(rspStr.Split(' ')[2]);
            return reading;
        }

        /// <summary>
        /// 
        /// </summary>
        public int TransmitAdcCalibrate
        {
            get
            {
                //string comStr = "getpot transmit";
                int reading = GetAdcChannelCalibrate(EnumAdcCalibrate.transmit);
                return reading;
            }
            set
            {
                //string comStr = string.Format("setpot transmit {0}", value);
                string rspStr = SetAdcChannelCalibrate(EnumAdcCalibrate.transmit, value);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public int ReflectAdcCalibrate
        {
            get
            {
                //string comStr = "getpot transmit";
                int reading = GetAdcChannelCalibrate(EnumAdcCalibrate.reflect);
                return reading;
            }
            set
            {
                //string comStr = string.Format("setpot transmit {0}", value);
                string rspStr = SetAdcChannelCalibrate(EnumAdcCalibrate.reflect, value);
            }
        }
        /// <summary>
        /// set/get PDs bias to ensure high sensitivity
        /// </summary>
        public int lockfineAdcCalibrate
        {
            get
            {
                //string comStr = "getpot lockfine";
                int  reading = GetAdcChannelCalibrate(EnumAdcCalibrate.lockfine);
                return reading;
            }
            set
            {
                //string comStr = string.Format("setpot lockfine {0}", value);
                string rspStr = SetAdcChannelCalibrate(EnumAdcCalibrate.lockfine, value);
            }
        }

        /// <summary>
        /// set/get adc monitor reading
        /// </summary>
        /// <returns></returns>
        public int GetAdcChannelReading(EnumAdcMonChannel channel)
        {
            int reading = 0;
            //extadcmon adcchannum <interval> <count>
            //string comstr = string.Format("extadcmon {0} {1} {2}",
            //                channel.ToString("d"), adcReadInterval_mS, adcReadAverageFactor);
            string s = string.Format("getadc {0}",channel );
            //string r = instrumentChassis.Query_Checked(s, this);
            string rspStr = instrumentChassis.Query_Checked(s, this);
            reading = int.Parse(rspStr.Split(' ')[2]);
            return reading;
        }
        #endregion

        //Echo new added functions

        public int Reference_Clean_Dac
        {
            get
            {
                string s = "extadcfixedpot";
                string r = instrumentChassis.Query_Checked(s, this);
                return int.Parse(r.Split(' ')[1])-_Fix_Port_Dark_Current_Dac;
            }

        }
        public int Filter_Clean_Dac
        {
            get
            {
                string s = "extadcvarpot";
                string r = instrumentChassis.Query_Checked(s, this);
                return int.Parse(r.Split(' ')[1]) - _Var_Port_Dark_Current_Dac;
            }
        }

        #region  FCUMKI Laser&Mz TEC
        private enum TECPID
        {
            None,
            Pgain,
            Igain,
            Dgain,
            setpoint,
        }
        private string SetLaserTEC(TECPID Name, int Dac)
        {
            string s = string.Format("setlsrtec {0} {1}", Name, Dac);
            string r = instrumentChassis.Query_Checked(s, this);
            return r;
        }
        private int GetLaserTEC(TECPID Name)
        {
            string s = string.Format("getlsrtec {0}", Name);
            string r = instrumentChassis.Query_Checked(s, this);
            return int.Parse(r.Split(' ')[2]);
        }
        private string SetMzTEC(TECPID Name, int Dac)
        {
            string s = string.Format("setmodtec {0} {1}", Name, Dac);
            string r = instrumentChassis.Query_Checked(s, this);
            return r;
        }
        private int GetMzTEC(TECPID Name)
        {
            string s = string.Format("getmodtec {0}", Name);
            string r = instrumentChassis.Query_Checked(s, this);
            return int.Parse(r.Split(' ')[2]);
        }
        public int LaserTEC_P
        {
            set
            {
                string r = SetLaserTEC(TECPID.Pgain, value);
            }
            get
            {
                return GetLaserTEC(TECPID.Pgain);
            }
        }
        public int LaserTEC_I
        {
            set
            {
                string r = SetLaserTEC(TECPID.Igain, value);
            }
            get
            {
                return GetLaserTEC(TECPID.Igain);
            }
        }
        public int LaserTEC_D
        {
            set
            {
                string r = SetLaserTEC(TECPID.Dgain, value);
            }
            get
            {
                return GetLaserTEC(TECPID.Dgain);
            }
        }
        public int LaserTEC_TempDAC
        {
            set
            {
                string r = SetLaserTEC(TECPID.setpoint, value);
            }
            get
            {
                return GetLaserTEC(TECPID.setpoint);
            }
        }
        public void SetLaserTEC_Temp_C(double temperature, double A, double B, double C)
        {
            int Tec_DAC;
            double Tec_R, Tec_T;
            for (Tec_DAC = 1; Tec_DAC < Math.Pow(2, 16); Tec_DAC++)
            {
                Tec_R = Tec_DAC * 10000 / (Math.Pow(2, 16) - Tec_DAC);
                Tec_T = 1 / (A + B * Math.Log(Tec_R) + C * Math.Pow(Math.Log(Tec_R), 3)) - 273;
                if (temperature > Tec_T)
                {
                    Tec_R = temperature - Tec_T;
                    break;
                }
            }
            string r = SetLaserTEC(TECPID.setpoint, Tec_DAC);
        }
        public int MzTEC_P
        {
            set
            {
                string r = SetMzTEC(TECPID.Pgain, value);
            }
            get
            {
                return GetMzTEC(TECPID.Pgain);
            }
        }
        public int MzTEC_I
        {
            set
            {
                string r = SetMzTEC(TECPID.Igain, value);
            }
            get
            {
                return GetMzTEC(TECPID.Igain);
            }
        }
        public int MzTEC_D
        {
            set
            {
                string r = SetMzTEC(TECPID.Dgain, value);
            }
            get
            {
                return GetMzTEC(TECPID.Dgain);
            }
        }
        public int MzTEC_TempDAC
        {
            set
            {
                string r = SetMzTEC(TECPID.setpoint, value);
            }
            get
            {
                return GetMzTEC(TECPID.setpoint);
            }
        }
        public void SetMzTEC_Temp_C(int temperature, double A, double B, double C)
        {
            int Tec_DAC;
            double Tec_R, Tec_T;
            Tec_R = Math.Pow(2, 16);
            for (Tec_DAC = 0; Tec_DAC < Math.Pow(2, 16); Tec_DAC++)
            {
                Tec_R = Tec_DAC * 10000 / (Math.Pow(2, 16) - Tec_DAC);
                Tec_T = 1 / (A + B * Math.Log(Tec_R) + C * Math.Pow(Math.Log(Tec_R), 3)) - 273;
                if (temperature > Tec_T)
                {
                    Tec_R = temperature - Tec_T;
                    break;
                }
            }
            string r = SetMzTEC(TECPID.setpoint, Tec_DAC);
        }

        #endregion  FCUMKI Laser TEC

        public void ZeroCurrent()
        {

            fsPairNumber = 1;
            IGain_Dac = 0;
            ISoa_Dac = ZeroSoaDac;
            IOdd_mA = 0;
            IEven_mA = 0;
            IRear_Dac = 0;
            IPhase_Dac = 0;
            IRearSoa_Dac = 0;
            IimbLeft_Dac = 0;
            IimbLeft_Dac = 0;
            

        }

        public string CloseAllAsic()
        {
            string r = instrumentChassis.Query("closeallasic ", this);
            return r;
        }

        public void CloseAsic()
        {
            LaserEnable = OnOff.off;
            TecEnable = OnOff.off;
            CloseAllAsic();
        }
        public string AsicTypeCheck()
        {
            string r = instrumentChassis.Query("getasicstatus ", this);
            return r;
        }
        private enum DpotName
        {
            None,
            ic703a,
            ic703b,
            ic704b,
            powermon,
        }
        private enum AdcName
        {
            None,
            ic715_0,
            ic715_1,
        }


        private int Getpot(DpotName name)
        {
            string s = string.Format("getpot {0}", name);
            string r = instrumentChassis.Query_Checked(s, this);
            return int.Parse(r.Split(' ')[2]);
        }

        private string SetPot(DpotName Name, int Dac)
        {
            string s = string.Format("setpot {0} {1}", Name, Dac);
            string r = instrumentChassis.Query_Checked(s, this);
            return r;
        }

        public int Locker_tran_pot_Index = 0;

        public int Locker_tran_pot
        {
            get
            {
                return this.Getpot(DpotName.ic703a);
            }
            set
            {
                this.SetPot(DpotName.ic703a, value);
            }
        }

        public int Locker_refi_pot_Index = 0;

        public int Locker_refi_pot
        {
            get
            {
                return this.Getpot(DpotName.ic703b);
            }
            set
            {
                this.SetPot(DpotName.ic703b, value);
            }
        }

        public int Locker_fine_pot
        {
            get
            {
                return this.Getpot(DpotName.ic704b);
            }
            set
            {
                this.SetPot(DpotName.ic704b, value);
            }
        }

        public int Var_pot_Index = 0;

        public int Var_pot
        {
            get
            {
                return this.Getpot(DpotName.powermon);
            }
            set
            {
                this.SetPot(DpotName.powermon, value);
            }
        }

        /// <summary>
        /// get locker transmitter monitor DAC
        /// </summary>
        public int Locker_Tx_DAC
        {
            get
            {
                int Dac = GetAdc(AdcName.ic715_0);
                return Dac;
            }
        }
        /// <summary>
        /// get locker reflect monitor DAC
        /// </summary>
        public int Locker_Rx_DAC
        {
            get
            {
                int Dac = GetAdc(AdcName.ic715_1);
                return Dac;
            }
        }

        public int Locker_tran_mon
        {
            get
            {
                return this.GetAdc(AdcName.ic715_0);
            }
        }

        public int Locker_refi_mon
        {
            get
            {
                return this.GetAdc(AdcName.ic715_1);
            }
        }
        /// <summary>
        /// get power monitor DAC (the sum of Tx and Rx,now use to measure filter current usually)
        /// </summary>
        public int Fix_Port_Dac
        {
            get
            {
                string s = "extadcfixedpot";
                string r = instrumentChassis.Query_Checked(s, this);
                return int.Parse(r.Split(' ')[1]);
            }

        }
        /// <summary>
        /// get MMI monitor DAC (now use to measure Reference or Ctap current usually)
        /// </summary>
        public int Var_port_Dac
        {
            get
            {
                string s = "extadcvarpot";
                string r = instrumentChassis.Query_Checked(s, this);
                return int.Parse(r.Split(' ')[1]);
            }
        }

        /// <summary>
        /// Set Tx pot value (fine pot is fixed as 255 usually) - chongjian.liang 2013.4.22
        /// </summary>
        public void TuneTxPotValue()
        {
            List<FCUMKI_DAC2mA_Cal_Parameter> potCalData = FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Tx;

            // Initialize pot to the max value
            this.Locker_tran_pot_Index = potCalData.Count - 1;
            this.Locker_tran_pot = potCalData[this.Locker_tran_pot_Index].Pot;
            int dac = this.Locker_Tx_DAC;

            int tunningCount = 0;

            // Tune pot value while DAC is out of range
            while ((tunningCount++ < Max_Count)
                && ((dac >= Max_DAC_Value && potCalData[this.Locker_tran_pot_Index].Pot < Max_Pot_Value)
                    || (dac <= Min_DAC_Value && potCalData[this.Locker_tran_pot_Index].Pot > Min_Pot_Value)))
            {
                double mAOfDAC = dac * potCalData[this.Locker_tran_pot_Index].Slope + potCalData[this.Locker_tran_pot_Index].offset;

                // Tune pot value to let DAC value to middle of its range by cal file
                this.Locker_tran_pot = GetPotWithDACToMidRange(mAOfDAC, potCalData, out this.Locker_tran_pot_Index);

                // Read the new dac by the new pot value
                dac = this.Locker_Tx_DAC;
            }
        }

        /// <summary>
        /// Set Rx pot value (fine pot is fixed as 255 usually) - chongjian.liang 2013.4.22
        /// </summary>
        public void TuneRxPotValue()
        {
            List<FCUMKI_DAC2mA_Cal_Parameter> potCalData = FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Rx;

            // Initialize pot to the max value
            this.Locker_refi_pot_Index = potCalData.Count - 1;
            this.Locker_refi_pot = potCalData[this.Locker_refi_pot_Index].Pot;
            int dac = this.Locker_Rx_DAC;

            int tunningCount = 0;

            // Tune pot value while DAC is out of range
            while ((tunningCount++ < Max_Count)
                && ((dac >= Max_DAC_Value && potCalData[this.Locker_refi_pot_Index].Pot < Max_Pot_Value)
                    || (dac <= Min_DAC_Value && potCalData[this.Locker_refi_pot_Index].Pot > Min_Pot_Value)))
            {
                double mAOfDAC = dac * potCalData[this.Locker_refi_pot_Index].Slope + potCalData[this.Locker_refi_pot_Index].offset;

                // Tune pot value to let DAC value to middle of its range by cal file
                this.Locker_refi_pot = GetPotWithDACToMidRange(mAOfDAC, potCalData, out this.Locker_refi_pot_Index);

                // Read the new dac by the new pot value
                dac = this.Locker_Rx_DAC;
            }
        }

        /// <summary>
        /// Set Var pot value - chongjian.liang 2013.4.22
        /// </summary>
        public void TuneVarPotValue()
        {
            List<FCUMKI_DAC2mA_Cal_Parameter> potCalData = FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Var;

            // Initialize pot to the max value
            this.Var_pot_Index = potCalData.Count - 1;
            this.Var_pot = potCalData[this.Var_pot_Index].Pot;
            int dac = this.Var_port_Dac;

            int tunningCount = 0;

            // Tune pot value while DAC is out of range
            while ((tunningCount++ < Max_Count)
                && ((dac >= Max_DAC_Value && potCalData[this.Var_pot_Index].Pot < Max_Pot_Value)
                    || (dac <= Min_DAC_Value && potCalData[this.Var_pot_Index].Pot > Min_Pot_Value)))
            {
                double mAOfDAC = this.Var_port_Dac * potCalData[this.Var_pot_Index].Slope + potCalData[this.Var_pot_Index].offset;

                // Tune pot value to let DAC value to middle of its range by cal file
                this.Var_pot = GetPotWithDACToMidRange(mAOfDAC, potCalData, out this.Var_pot_Index);

                // Read the new dac by the new pot value
                dac = this.Var_port_Dac;
            }
        }

        #region  DAC_mA_Convert
        /// <summary>
        /// in light FCU MKI fine Cal result to calculate DAC to mA
        /// </summary>
        /// <param name="DAC"></param>
        /// <param name="Locker_fine_Pot"></param>
        /// <param name="Pot"></param>
        /// <param name="UseCalFactor"></param>
        /// <returns></returns>
        public double FCUMKI_Locker_Tx_CurrentDACTomA(int DAC, int Locker_fine_Pot, int Pot, out bool PotInCalFile)
        {
            double Cal_mA = 0;
            double slope = 0;
            double offset = 0;
            PotInCalFile = false;
            for (int i = 0; i < FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Tx.Count; i++)
            {
                if (FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Tx[i].Pot == Pot)
                {
                    slope = FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Tx[i].Slope;
                    offset = FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Tx[i].offset;
                    PotInCalFile = true;
                    break;
                }
            }
            if (!PotInCalFile)
            {
                this.Locker_tran_pot = FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Tx[FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Tx.Count - 1].Pot;
                DAC = this.Locker_Tx_DAC;
                slope = FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Tx[FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Tx.Count - 1].Slope;
                offset = FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Tx[FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Tx.Count - 1].offset;
            }
            Cal_mA = DAC * slope + offset;
            return Cal_mA;
        }
        /// <summary>
        /// in light FCU MKI fine Cal result to calculate DAC to mA
        /// </summary>
        /// <param name="DAC"></param>
        /// <param name="Pot"></param>
        /// <param name="UseCalFactor"></param>
        /// <returns></returns>
        public double FCUMKI_Locker_Rx_CurrentDACTomA(int DAC, int Pot, out bool PotInCalFile)
        {
            double Cal_mA = 0;
            double slope = 0;
            double offset = 0;
            PotInCalFile = false;

            for (int i = 0; i < FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Rx.Count; i++)
            {
                if (FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Rx[i].Pot == Pot)
                {
                    slope = FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Rx[i].Slope;
                    offset = FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Rx[i].offset;
                    PotInCalFile = true;
                    break;
                }
            }
            if (!PotInCalFile)
            {
                this.Locker_refi_pot = FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Rx[FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Rx.Count - 1].Pot;
                DAC = this.Locker_Rx_DAC;
                slope = FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Rx[FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Rx.Count - 1].Slope;
                offset = FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Rx[FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Rx.Count - 1].offset;
            }
            Cal_mA = DAC * slope + offset;
            return Cal_mA;
        }
        /// <summary>
        /// in light FCU MKI fine Cal result to calculate DAC to mA
        /// </summary>
        /// <param name="DAC"></param>
        /// <param name="UseCalFactor"></param>
        /// <returns></returns>
        public double FCUMKI_Fix_CurrentDACTomA(int DAC)
        {
            double Cal_mA = 0;
            double slope = 0;
            double offset = 0;
            for (int i = 0; i < FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Fix.Count; i++)
            {
                slope = FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Fix[i].Slope;
                offset = FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Fix[i].offset;
                break;
            }
            Cal_mA = DAC * slope + offset;
            return Cal_mA;
        }
        /// <summary>
        /// in light FCU MKI fine Cal result to calculate DAC to mA
        /// </summary>
        /// <param name="DAC"></param>
        /// <param name="Pot"></param>
        /// <param name="UseCalFactor"></param>
        /// <returns></returns>
        public double FCUMKI_Var_CurrentDACTomA(int DAC, int Pot, out bool PotInCalFile)
        {
            double Cal_mA = 0;
            double slope = 0;
            double offset = 0;
            PotInCalFile = false;
            for (int i = 0; i < FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Var.Count; i++)
            {
                if (FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Var[i].Pot == Pot)
                {
                    slope = FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Var[i].Slope;
                    offset = FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Var[i].offset;
                    PotInCalFile = true;
                    break;
                }
            }
            if (!PotInCalFile)
            {
                this.Var_pot = FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Var[FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Var.Count - 1].Pot;
                DAC = this.Var_port_Dac;
                slope = FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Var[FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Var.Count - 1].Slope;
                offset = FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Var[FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Var.Count - 1].offset;
            }
            Cal_mA = DAC * slope + offset;
            return Cal_mA;
        }
        #endregion

        #region Fine tunning Pot value for DAC Value
        int Max_DAC_Value = 40000;
        int Min_DAC_Value = 20000;
        int Max_Pot_Value = 247;
        int Min_Pot_Value = 1;
        int Pot_Step = 3;
        int Max_Count = 250;
        int delaytime_ms = 5;

        /// <summary>
        /// Get the pot which can produce a middle number of DAC for the target mA. Chongjian.liang 2013.4.25
        /// </summary>
        /// <returns>pot value</returns>
        public int GetPotWithDACToMidRange(double target_mA, List<FCUMKI_DAC2mA_Cal_Parameter> potCalData, out int potIndex)
        {
            double slope = 0;
            double offset = 0;
            double mid_mA = 0;
            double Delt_mA = 0;
            double Min_Delt_mA = 999;

            potIndex = 0;

            for (int i = 0; i < potCalData.Count; i++)
            {
                slope = potCalData[i].Slope;
                offset = potCalData[i].offset;
                mid_mA = 65535 / 2.0 * slope + offset;
                Delt_mA = Math.Abs(mid_mA - target_mA);

                if (Delt_mA < Min_Delt_mA)
                {
                    Min_Delt_mA = Math.Min(Min_Delt_mA, Math.Abs(mid_mA - target_mA));
                    potIndex = i;
                }
            }

            return potCalData[potIndex].Pot;
        }

        #endregion

        #region Read Tx, Rx, Var, Fix, and CTap pot currents

        /// <summary>
        /// Read and average the Tx DAC, calc and return the Tx mA - chongjian.liang 2013.4.22
        /// </summary>
        /// <param name="isTunePotValue">Tune pot value before reading Tx DAC, store the pot value before tuning</param>
        /// <param name="isRestoreToPotValueBeforeTune">After reading Tx DAC, restore pot value to the pot value before the last tuning</param>
        /// <param name="isAverageValue">Read 5 times and average the values</param>
        /// <returns></returns>
        public double Locker_Tx_mA(bool isTunePotValue, bool isRestoreToPotValueBeforeTune, bool isAverageValue)
        {
            // Store then tune Tx pot value
            if (isTunePotValue)
            {
                // Store pot value
                this.txPotValue_BeforeTuning = this.Locker_tran_pot;
                this.txPotValueIndex_BeforeTuning = this.Locker_tran_pot_Index;

                // Tune pot
                this.TuneTxPotValue();
            }

            // Read and average Tx current

            int readCount = isAverageValue ? lockerAvgCount : 1;

            double txCurrent_mA = 0;

            List<FCUMKI_DAC2mA_Cal_Parameter> potCalData = FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Tx;

            for (int i = 0; i < readCount; i++)
            {
                txCurrent_mA += this.Locker_Tx_DAC * potCalData[this.Locker_tran_pot_Index].Slope + potCalData[this.Locker_tran_pot_Index].offset;
            }

            txCurrent_mA /= readCount;

            // Restore TX pot value to the pot value before the last tuning
            if (isRestoreToPotValueBeforeTune)
            {
                this.Locker_tran_pot = this.txPotValue_BeforeTuning;
                this.Locker_tran_pot_Index = this.txPotValueIndex_BeforeTuning;
            }

            return txCurrent_mA;
        }

        /// <summary>
        /// Read and average the Rx DAC, calc and return the Rx mA - chongjian.liang 2013.4.22
        /// </summary>
        /// <param name="isTunePotValue">Tune pot value before reading Rx DAC, store the pot value before tuning</param>
        /// <param name="isRestoreToPotValueBeforeTune">After reading Rx DAC, restore pot value to the pot value before the last tuning</param>
        /// <param name="isAverageValue">Read 5 times and average the values</param>
        /// <returns></returns>
        public double Locker_Rx_mA(bool isTunePotValue, bool isRestoreToPotValueBeforeTune, bool isAverageValue)
        {
            // Store then tune Rx pot value
            if (isTunePotValue)
            {
                // Store pot value
                this.rxPotValue_BeforeTunning = this.Locker_refi_pot;
                this.rxPotValueIndex_BeforeTunning = this.Locker_refi_pot_Index;

                // Tune pot
                this.TuneRxPotValue();
            }

            // Read and average Rx current

            int readCount = isAverageValue ? lockerAvgCount : 1;

            double rxCurrent_mA = 0;

            List<FCUMKI_DAC2mA_Cal_Parameter> potCalData = FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Rx;

            for (int i = 0; i < readCount; i++)
            {
                rxCurrent_mA += this.Locker_Rx_DAC * potCalData[this.Locker_refi_pot_Index].Slope + potCalData[this.Locker_refi_pot_Index].offset;
            }

            rxCurrent_mA /= readCount;

            // Restore RX pot value to the pot value before the last tuning
            if (isRestoreToPotValueBeforeTune)
            {
                this.Locker_refi_pot = this.rxPotValue_BeforeTunning;
                this.Locker_refi_pot_Index = this.rxPotValueIndex_BeforeTunning;
            }

            return rxCurrent_mA;
        }

        /// <summary>
        /// Read and average the Rx DAC, calc and return the Rx mA - chongjian.liang 2013.4.22
        /// </summary>
        /// <param name="isTunePotValue">Tune pot value before reading Rx DAC, store the pot value before tuning</param>
        /// <param name="isRestoreToPotValueBeforeTune">After reading Rx DAC, restore pot value to the pot value before the last tuning</param>
        /// <param name="isAverageValue">Read 5 times and average the values</param>
        /// <returns></returns>
        public double Var_Port_mA(bool isTunePotValue, bool isRestoreToPotValueBeforeTune, bool isAverageValue)
        {
            // Store then tune Rx pot value
            if (isTunePotValue)
            {
                // Store pot value
                this.varPotValue_BeforeTunning = this.Var_pot;
                this.varPotValueIndex_BeforeTunning = this.Var_pot_Index;

                // Tune pot
                this.TuneVarPotValue();
            }

            // Read and average Rx current

            int readCount = isAverageValue ? lockerAvgCount : 1;

            double varCurrent_mA = 0;

            List<FCUMKI_DAC2mA_Cal_Parameter> potCalData = FreqCalByEtalon.FCUMKI_DAC2mA_Cal_Data.Var;

            for (int i = 0; i < readCount; i++)
            {
                varCurrent_mA += this.Var_port_Dac * potCalData[this.Var_pot_Index].Slope + potCalData[this.Var_pot_Index].offset;
            }

            varCurrent_mA /= readCount;

            // Restore RX pot value to the pot value before the last tuning
            if (isRestoreToPotValueBeforeTune)
            {
                this.Var_pot = this.varPotValue_BeforeTunning;
                this.Var_pot_Index = this.varPotValueIndex_BeforeTunning;
            }

            return varCurrent_mA;
        }

        /// <summary>
        /// calculate reference or Ctap current with the fix pot 
        /// </summary>
        /// <param name="isAverageValue">Read 5 times and average the values</param>
        public double Fix_Port_mA(bool isAverageValue)
        {
            int readCount = isAverageValue ? lockerAvgCount : 1;

            double fixCurrent_mA = 0;

            for (int i = 0; i < readCount; i++)
            {
                fixCurrent_mA += this.FCUMKI_Fix_CurrentDACTomA(this.Fix_Port_Dac);
            }

            return fixCurrent_mA /= readCount;
        }

        /// <summary>
        /// Ctap is negetive Fix current
        /// </summary>
        public double ICtap_mA
        {
            get
            {
                return -this.Fix_Port_mA(false);
            }
        }
        
        #endregion 

        public void Inital_Etalon_WaveMter()
        {
            //check update ft rt pt calidata
            if (!CheckUpdateIsOK)
                CheckUpdateIsOK = CheckUpdate(CaliDataFileDirectory);
            this.Var_pot = (int)FreqCalByEtalon.Filter_Etalon_Data[0][1];//Var pot
            this.Locker_tran_pot = (int)Math.Max(FreqCalByEtalon.Filter_Etalon_Data[0][3],FreqCalByEtalon.Filter_Etalon_Data[0][5]);//Tx pot
            this.Locker_fine_pot = (int)FreqCalByEtalon.Filter_Etalon_Data[0][4];//Tx fine pot 
            this.Locker_refi_pot = (int)Math.Max(FreqCalByEtalon.Filter_Etalon_Data[0][5],FreqCalByEtalon.Filter_Etalon_Data[0][3]);//Rx pot
            this.SetLaserTEC_Temp_C((double)FreqCalByEtalon.Filter_Etalon_Data[0][8], (double)FreqCalByEtalon.Filter_Etalon_Data[0][9], (double)FreqCalByEtalon.Filter_Etalon_Data[0][10], (double)FreqCalByEtalon.Filter_Etalon_Data[0][11]);
            this.TecEnable = OnOff.on;
            System.Threading.Thread.Sleep(3000);
        }
        public bool CheckUpdate(string CaliDataFileDirectory)
        {
            foreach (string var in Enum.GetNames(typeof(FRPS)))
            {
                FRPS SectionType = (FRPS)Enum.Parse(typeof(FRPS), var);
                string CaliDataFileName = Path.Combine(CaliDataFileDirectory, var + "Calidata.csv");
                string Md5FromFcu = GetXXCalidate(SectionType);
                string Md5FromPc = GetMd5Sum(File.ReadAllText(CaliDataFileName));
                float f;
                if (!Md5FromFcu.Equals(Md5FromPc, StringComparison.InvariantCultureIgnoreCase))
                {
                    //send data from pc to fcu
                    string[] arr = File.ReadAllLines(CaliDataFileName);
                    for (int i = 0; i < arr.Length; i++)
                    {
                        if (SectionType == FRPS.ft)
                        {
                            SetFtSweepDacForAsic(i, mAToDacForFront_Asic(float.Parse(arr[i])));
                            f = GetFtSweepDacForAsic(1, i);
                            f = GetFtSweepDacForAsic(2, i);
                        }
                        else
                        {
                            if (SectionType == FRPS.rt)
                            {
                                SetSweepDacForAsic(SectionType, i, mAToDacForRear_Asic(float.Parse(arr[i])));
                            }
                            else
                            {
                                SetSweepDacForAsic(SectionType, i, mAToDacForPhase_Asic(float.Parse(arr[i])));
                            }
                            f = GetSweepDacForAsic(SectionType, i);
                        }
                    }
                    //update md5
                    SetXXCalidate(SectionType, Md5FromPc);
                }
            }
            return true;
        }


        struct SH
        {
            public double C1, C2, C3;
        }
        SH LaserSH, ModulatorSH;
        void SetSH()
        {
            //The laser has a Mitsubishi FH10-6E, with the following coefficients:
            LaserSH = new SH();
            LaserSH.C1 = 0.001214177;
            LaserSH.C2 = 0.0002196864;
            LaserSH.C3 = 0.0000001493942;

            //The MZ has a Mitsubishi FH05, with the following coefficients:
            ModulatorSH = new SH();
            ModulatorSH.C1 = 0.001074012;
            ModulatorSH.C2 = 0.0002432709;
            ModulatorSH.C3 = 0.000000050583;
        }
        private enum FRPS
        {
            ft, rt, pt,
        }
        private string GetXXCalidate(FRPS SectionType)
        {
            //todo:tdb
            string str = string.Format("get{0}calidate", SectionType);
            string r = instrumentChassis.Query_Checked(str, this);
            return r.Split(' ')[1];
        }

        // Create an md5 sum string of this string
        internal static string GetMd5Sum(string str)
        {
            // First we need to convert the string into bytes, which
            // means using a text encoder.
            Encoder enc = System.Text.Encoding.Unicode.GetEncoder();

            // Create a buffer large enough to hold the string
            byte[] unicodeText = new byte[str.Length * 2];
            enc.GetBytes(str.ToCharArray(), 0, str.Length, unicodeText, 0, true);

            // Now that we have a byte array we can ask the CSP to hash it

            System.Security.Cryptography.MD5 md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] result = md5.ComputeHash(unicodeText);

            // Build the final string by converting each byte
            // into hex and appending it to a StringBuilder
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                sb.Append(result[i].ToString("X2"));
            }

            // And return it
            return sb.ToString();
        }
        public int Fix_pot
        {
            get
            {
                return (int)FreqCalByEtalon.Filter_Etalon_Data[0][2];//Fix pot
            }
        }
        private void SetDnlFTisrc(int i, string value)
        {
            string r = instrumentChassis.Query_Checked(
                string.Format("setdnlftisrc {0} {1} {2}", 1, i, value), this);
            r = instrumentChassis.Query_Checked(
                string.Format("setdnlftisrc {0} {1} {2}", 2, i, value), this);
        }
        private float GetDnlFT(int FP, int i)
        {

            string r = instrumentChassis.Query_Checked(string.Format("getdnlft {0} {1}", FP, i), this);
            float f = float.Parse(r.Split(' ')[2]);
            return f;
        }
        private void SetDnlXXisrc(FRPS SectionType, int i, float value)
        {
            string r = instrumentChassis.
                Query_Checked(string.Format("setdnl{0}isrc {1} {2}", SectionType, i, value), this);

        }
        private void SetDnlXXisrc(FRPS SectionType, int i, string value)
        {
            string r = instrumentChassis.Query_Checked(
                string.Format("setdnl{0}isrc {1} {2}", SectionType, i, value), this);

        }
        private float GetDnlXX(FRPS SectionType, int i)
        {

            string r = instrumentChassis.Query_Checked(string.Format("getdnl{0} {1}", SectionType, i), this);
            float f = float.Parse(r.Split(' ')[2]);
            return f;
        }

        private void SetXXCalidate(FRPS SectionType, string CalidateValue)
        {
            for (int i = 0; i < CalidateValue.Length; i++)
            {
                string r = instrumentChassis.Query_Checked
                    (string.Format("set{0}calidate {1} {2}", SectionType, i, CalidateValue[i]), this);
            }
            //instrumentChassis.Clear();
        }

        private void SetSweepDacForAsic(FRPS SectionType, int i, int value)
        {
            string cmd = "";
            switch (SectionType)
            {
                case FRPS.pt:
                    cmd = string.Format("setsweepdac phase {0} {1}", i, value);
                    break;
                case FRPS.rt:
                    cmd = string.Format("setsweepdac rear {0} {1}", i, value);
                    break;
                default:
                    throw new Exception("this funtion can't set " + SectionType.ToString());
                    break;
            }
            string r = instrumentChassis.Query_Checked(cmd, this);
        }

        private float GetSweepDacForAsic(FRPS SectionType, int i)
        {
            string cmd = "";
            switch (SectionType)
            {
                case FRPS.pt:
                    cmd = string.Format("getsweepdac phase {0}", i);
                    break;
                case FRPS.rt:
                    cmd = string.Format("getsweepdac rear {0}", i);
                    break;
                default:
                    throw new Exception("this funtion can't set " + SectionType.ToString());
                    break;
            }
            string r = instrumentChassis.Query_Checked(cmd, this);
            float f = float.Parse(r.Split(' ')[3]);
            return f;
        }

        private float GetFtSweepDacForAsic(int FP, int i)
        {
            string cmd = "";
            if (FP == 1)
            {
                cmd = string.Format("getsweepdac odd {0}", i);
            }
            else
            {
                cmd = string.Format("getsweepdac even {0}", i);
            }
            string r = instrumentChassis.Query_Checked(cmd, this);
            float f = float.Parse(r.Split(' ')[3]);
            return f;
        }

        private void SetFtSweepDacForAsic(int i, int value)
        {

            string r = instrumentChassis.Query_Checked(string.Format("setsweepdac odd {0} {1}", i, value), this);

            r = instrumentChassis.Query_Checked(string.Format("setsweepdac even {0} {1}", i, value), this);

        }
        /// <summary>
        /// set FCUMKI I2C baud
        /// </summary>
        /// <param name="Baud"></param>
        /// <returns></returns>
        public string SetI2CBaud(int Baud)
        {
            string s = string.Format("setI2Cbaud {0}",Baud);
            string r = instrumentChassis.Query_Checked(s, this);
            return r;
        }

        #region private data

        // TODO: Change to the actual chassis type
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_Fcu2Asic instrumentChassis;
        //private bool CheckUpdateIsOK; //isLoadSweepDataOK;
        /// <summary>
        /// A delay in milliseconds between readings for each Adc 
        /// </summary>
        private int adcReadInterval_mS = 10;
        /// <summary>
        /// how many readings to take average to achieve an ADC channel readings
        /// </summary>
        private int adcReadAverageFactor = 3;

        //private double ifs1_mA;
        //private double ifs2_mA;
        //private double ifs3_mA;
        //private double ifs4_mA;
        //private double ifs5_mA;
        //private double ifs6_mA;
        //private double ifs7_mA;
        //private double ifs8_mA;
        private double iSoa_mA;
        //private double iSoaNeg_mA;
        private double iGain_mA;
        private double iRear_mA;
        private double iPhase_mA;
        //private double iFsOdd_mA;
        //private double iFsEven_mA;
        private double iLeftImb_mA;
        private double iRightImb_mA;
        private double iRearSoa_mA;


        //private int ifs1_Dac;
        //private int ifs2_Dac;
        //private int ifs3_Dac;
        //private int ifs4_Dac;
        //private int ifs5_Dac;
        //private int ifs6_Dac;
        //private int ifs7_Dac;
        //private int ifs8_Dac;
        //private int iSoaPos_Dac;
        private int iSoa_Dac;
        private int iGain_Dac;
        private int iRear_Dac;
        private int iPhase_Dac;
        private int iLeftImb_Dac;
        private int iRightImb_Dac;
        private int iRearSoa_Dac;

        private int fsPairNumber;
        private float iFsOdd_mA;
        private int iFsOdd_Dac;

        private float iFsEven_mA;
        private int iFsEven_Dac;

        private OnOff highPhaseRange;
        /// <summary>
        /// 
        /// </summary>
        public StrcChannelMax ChannelMaxLimit;
        /// <summary>
        /// TOSA's current DAC Calibrations
        /// </summary>  
        public AsicCalibrationData SectionsDacCalibration;
        public FCUCalData FcuDac2mACalibration;

        public readonly int ZeroSoaDac;

        public int _Locker_Tx_Dark_Current_Dac, _Locker_Rx_Dark_Current_Dac, _Fix_Port_Dark_Current_Dac, _Var_Port_Dark_Current_Dac;

        private const int lockerAvgCount = 5;

        private int txPotValue_BeforeTuning;
        private int rxPotValue_BeforeTunning;
        private int varPotValue_BeforeTunning;
        private int txPotValueIndex_BeforeTuning;
        private int rxPotValueIndex_BeforeTunning;
        private int varPotValueIndex_BeforeTunning;
        #endregion

        #region data Definition
        /// <summary>
        /// enum all sections' name in FCU
        /// </summary>
        private enum enumPinName
        {
            None,
            FrontSection1 = 1,
            FrontSection2,
            FrontSection3,
            FrontSection4,
            FrontSection5,
            FrontSection6,
            FrontSection7,
            FrontSection8,
            soaIpos, 
            soaIneg,
            gain, 
            rear,
            phase,
            even, 
            odd,
            leftimb,
            rightimb,
            rearsoa
        }       
        /// <summary>
        ///  enum on off state
        /// </summary>
        public enum OnOff   // EnumOnOff
        {
            /// <summary>
            /// 
            /// </summary>
            on,   // On
            /// <summary>
            /// 
            /// </summary>
            off,  // Off
        }
        /// <summary>
        /// ADC monitor channel
        /// </summary>
        public enum EnumAdcMonChannel
        {
            /// <summary>
            /// locker transmit
            /// </summary>
            ic715_0,//locker_tran_mon = 0,
            /// <summary>
            /// locker reflect
            /// </summary>
            ic715_1,//locker_refl_mon =1          

        }
        /// <summary>
        /// Adc channel's calibration factor
        /// </summary>
        public enum EnumAdcCalibrate
        {
            /// <summary>
            /// 
            /// </summary>
            lockfine,
            /// <summary>
            /// 
            /// </summary>
            reflect,
            /// <summary>
            /// 
            /// </summary>
            transmit,
            /// <summary>
            /// 
            /// </summary>
            range,
            /// <summary>
            /// 
            /// </summary>
            powermon
        }
        /// <summary>
        /// 
        /// </summary>
        private static List<int> pairnums = new List<int>(
            new int[] { 1, 2, 3, 4, 5, 6, 7 });

        /// <summary> Structure for FCU calibration data </summary>
        public struct FCUCalData
        {
            /// <summary> Cal offset for TxADC </summary>
            public double TxADC_CalOffset;
            /// <summary> Cal factor for TxADC </summary>
            public double TxADC_CalFactor;

            /// <summary> Cal offset for RxADC </summary>
            public double RxADC_CalOffset;
            /// <summary> Cal factor for RxADC </summary>
            public double RxADC_CalFactor;

            /// <summary> Cal offset for TxCoarsePot </summary>
            public double TxCoarsePot_CalOffset;
            /// <summary> Cal factor for TxCoarsePot </summary>
            public double TxCoarsePot_CalFactor;

            /// <summary> Cal offset for TxCoarsePot </summary>
            public double RxCoarsePot_CalOffset;
            /// <summary> Cal factor for TxCoarsePot </summary>
            public double RxCoarsePot_CalFactor;

            /// <summary> Cal offset for TxFinePot </summary>
            public double TxFinePot_CalOffset;
            /// <summary> Cal factor for TxFinePot </summary>
            public double TxFinePot_CalFactor;

            /// <summary> Cal offset for FixPot </summary>
            public double FixADC_CalOffset;
            /// <summary> Cal factor for FixPot </summary>
            public double FixADC_CalFactor;

            /// <summary> Cal offset for ThermistorResistance </summary>
            public double ThermistorResistance_CalOffset;
            /// <summary> Cal factor for ThermistorResistance </summary>
            public double ThermistorResistance_CalFactor;
        }
        
        /// <summary> Structure for ASIC DAC calibration data </summary>
        public struct AsicCalibrationData
        {
            /// <summary> Cal offset for all front sections</summary>
            public double FsDac_CalOffset;
            /// <summary> Cal factor for all front sections </summary>
            public double FsDac_CalFactor;

            /// <summary> Cal offset for phase dac </summary>
            public double PhaseDac_CalOffset;
            /// <summary> Cal factor for phase dac </summary>
            public double PhaseDac_CalFactor;

            /// <summary> Cal offset for rear soa dac </summary>
            public double RearSoaDac_CalOffset;
            /// <summary> Cal factor for rear soa dac </summary>
            public double RearSoaDac_CalFactor;

            /// <summary> Cal offset for Gain dac </summary>
            public double GainDac_CalOffset;
            /// <summary> Cal factor for Gain dac </summary>
            public double GainDac_CalFactor;

            /// <summary> Cal offset for positive Front Soa Dac </summary>
            public double FrontSoaDac_Pos_CalOffset;
            /// <summary> Cal factor for posititive Front Soa Dac </summary>
            public double FrontSoaDac_Pos_CalFactor;
            /// <summary> Cal offset for negative Front Soa Dac </summary>
            public double FrontSoaDac_Neg_CalOffset;
            /// <summary> Cal factor for negative Front Soa Dac </summary>
            public double FrontSoaDac_Neg_CalFactor;
            /// <summary>
            /// Dac Calibration offset for Rear section 
            /// </summary>
            public double RearDac_CalOffset;
            /// <summary>
            /// Dac Calibration factor for Rear section
            /// </summary>
            public double RearDac_CalFactor;
            /// <summary>
            /// Dac calibration offset for left Imb
            /// </summary>
            public double ImbLeftDac_CalOffset;
            /// <summary>
            /// Dac Calibration factor for left Imb
            /// </summary>
            public double ImbLeftDac_CalFactor;
            /// <summary>
            /// Dac calibration factor for Right Imb
            /// </summary>
            public double ImbRightDac_CalFactor;
            /// <summary>
            /// Dac calibration offset for right Imb
            /// </summary>
            public double ImbRightDac_CalOffset;

        }

        /// <summary>
        ///  define each channel's maximum output current and digit
        /// </summary>
        public struct StrcChannelMax
        {
            /// <summary>
            /// 
            /// </summary>
            public double iFrontSectionMax_mA ;
            /// <summary>
            /// 
            /// </summary>
            public double iSoaPosMax_mA;
            /// <summary>
            /// 
            /// </summary>
            public  double iSoaNegMax_mA;
            /// <summary>
            /// 
            /// </summary>
            public  double iGainMax_mA;
            /// <summary>
            /// 
            /// </summary>
            public double iRearLowMax_mA;
                        /// <summary>
            /// 
            /// </summary>
            public double iRearHighMax_mA;
            
            /// <summary>
            /// 
            /// </summary>
            public  double iPhaseLowMax_mA;
            /// <summary>
            /// 
            /// </summary>
            public double iPhaseHighMax_mA ;
            //private double iFsOdd_mA;
            //private double iFsEven_mA;
            /// <summary>
            /// 
            /// </summary>
            public  double iLeftImbMax_mA;
            /// <summary>
            /// 
            /// </summary>
            public double iLeftImbLowMax_mA;
            /// <summary>
            /// 
            /// </summary>
            public double iLeftImbHighMax_mA;
            /// <summary>
            /// 
            /// </summary>
            public double iRightImbMax_mA;
            /// <summary>
            /// 
            /// </summary>
            public double iRightImbLowMax_mA;
            /// <summary>
            /// 
            /// </summary>
            public double iRightImbHighMax_mA;
            /// <summary>
            /// 
            /// </summary>
            public double iRearSoaMax_mA;
            /// <summary>
            /// 
            /// </summary>
            public int DacMax ;
        }
        #endregion
    }
}

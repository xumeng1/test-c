using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.Utilities;
using Bookham.TestLibrary.Algorithms;

namespace Bookham.TestSolution.Instruments
{
    public class FreqCalByEtalon
    {
        public static List<List<double>> Filter_Etalon_Data = new List<List<double>>();
        public static FCUMKI_DAC2mA_Cal_Port FCUMKI_DAC2mA_Cal_Data;
        public static int Freq_GHz_List_Index, PowerRatio_List_Index, LockerRatio_List_Index;
        public static double Max_Locker_Ratio_50GHz = -999, Min_Locker_Ratio_50GHz = 999, Max_Locker_Ratio_100GHz = -999, Min_Locker_Ratio_100GHz = 999;
        public static double Average_Locker_Ratio_SlopeToGHz = 0, Average_Power_Ratio_SlopeToGHz = 0, Average_Reference_PD_Responsivity=0;

        private static double _FrequencyStep_Ghz = 50;
        private static double _FrequencyTolerance_GHz = 2;
        private static bool _UseDUTLockerRatio = false;
        private static LinearLeastSquaresFit _DUTLockerRatioSlope_50GHz, _DUTLockerRatioSlope_100GHz,_Reference_PD_Responsivity;//unit mA/mW for power leveling. jack.zhang 2012-11-19
        private static double midRatioEtalonRxToReference = 0;
        private static double midRatioEtalonTxToReference = 0;

        public static List<FCUMKI_DAC2mA_Cal_Parameter> LoadFCUMKICalArray(string filename)
        {
            CsvReader csvReader = new CsvReader();
            List<string[]> FCUMKI_CAL_List = csvReader.ReadFile(filename);
            List<FCUMKI_DAC2mA_Cal_Parameter> FCUMKI_DAC2mA_List = new List<FCUMKI_DAC2mA_Cal_Parameter>();
            FCUMKI_DAC2mA_Cal_Parameter temp_List = new FCUMKI_DAC2mA_Cal_Parameter();
            double Pot, DAC2mA_Slope, DAC2mA_offset, DAC2mA_Dark_DAC;

            for (int i = 1; i < FCUMKI_CAL_List.Count; i++)
            {
                temp_List.Pot = Convert.ToInt32(FCUMKI_CAL_List[i][0]);
                temp_List.Slope = Convert.ToDouble(FCUMKI_CAL_List[i][1]);
                temp_List.offset = Convert.ToDouble(FCUMKI_CAL_List[i][2]);
                FCUMKI_DAC2mA_List.Add(temp_List);
            }
            return FCUMKI_DAC2mA_List;

        }

        public static void LoadFreqlCalArray(string filename, double CAL_ITU_Offset_GHz)
        {
            LoadFreqlCalArray(filename, CAL_ITU_Offset_GHz, _FrequencyTolerance_GHz, _FrequencyStep_Ghz);
        }

        public static void LoadFreqlCalArray(string filename, double CAL_ITU_Offset_GHz, double FrequencyToleranceGHz, double FrequencyStepGHz)
        {
            CsvReader csvReader = new CsvReader();
            List<string[]> Filter_Etalon_List = csvReader.ReadFile(filename);
            List<double> Reference_Current_mA = new List<double>();
            List<double> Power_dBm = new List<double>();
            double Freq_GHz, PowerRatio, LockerRatio, Delt_Locker_Ratio=0, Max_Delt_Locker_Ratio = 0;
            bool Find_Frist_point = false;
            double Temp_First_LockerRatio = 0, Temp_First_frequency_GHz = 0, Temp_Last_LockerRatio = 0,
                Temp_Last_frequency_GHz = 0, Count = 0, Temp_First_PowerRatio = 0, Temp_Last_PowerRatio = 0,
                Reference_PD_Responsivity = 0;
            Filter_Etalon_Data.Clear();
            _FrequencyStep_Ghz = FrequencyStepGHz;
            _FrequencyTolerance_GHz = FrequencyToleranceGHz;
            Average_Locker_Ratio_SlopeToGHz = 0;
            Max_Locker_Ratio_50GHz = -999;
            Min_Locker_Ratio_50GHz = 999;
            Max_Locker_Ratio_100GHz = -999;
            Min_Locker_Ratio_100GHz = 999;
            for (int i = 1; i < Filter_Etalon_List.Count; i++)
            {
                Power_dBm.Add(Alg_PowConvert_dB.Convert_dBmtomW(Convert.ToDouble(Filter_Etalon_List[i][2])));//convert dBm to mW due to PD current line with mW than dbm. Jack.zhang 2012-11-18
                Reference_Current_mA.Add(Convert.ToDouble(Filter_Etalon_List[i][8])); //for calculate Average Reference PD Responsivity (mA/mW) for map power leveling. Jack.zhang 2012-11-18
                Reference_PD_Responsivity += Reference_Current_mA[i - 1] / Power_dBm[i - 1];
                List<double> tmpList = new List<double>();
                Freq_GHz = Convert.ToDouble(Filter_Etalon_List[i][1]);
                PowerRatio = Convert.ToDouble(Filter_Etalon_List[i][16]);
                LockerRatio = Convert.ToDouble(Filter_Etalon_List[i][17]);
                if(i < Filter_Etalon_List.Count-1)
                Delt_Locker_Ratio = Math.Abs(LockerRatio - Convert.ToDouble(Filter_Etalon_List[i + 1][17]));
                Max_Delt_Locker_Ratio = Math.Max(Max_Delt_Locker_Ratio, Delt_Locker_Ratio);

                tmpList.Add(Freq_GHz);// 0_Frequency_GHz
                Freq_GHz_List_Index = 0;
                tmpList.Add(Convert.ToDouble(Filter_Etalon_List[i][4]));// 1_Filter_Pot
                tmpList.Add(Convert.ToDouble(Filter_Etalon_List[i][7]));// 2_Reference_Pot
                tmpList.Add(Convert.ToDouble(Filter_Etalon_List[i][10]));// 3_Locker_Etalon_Coase_Pot
                tmpList.Add(Convert.ToDouble(Filter_Etalon_List[i][11]));// 4_Locker_Etalon_Fine_Pot
                tmpList.Add(Convert.ToDouble(Filter_Etalon_List[i][14]));// 5_Locker_Reference_Pot
                tmpList.Add(PowerRatio);// 6_Power_Ratio
                PowerRatio_List_Index = 6;
                tmpList.Add(LockerRatio);// 7_Locker_Ratio
                LockerRatio_List_Index = 7;
                tmpList.Add(Convert.ToDouble(Filter_Etalon_List[i][18]));// 8_Locker_Temperature_C
                tmpList.Add(Convert.ToDouble(Filter_Etalon_List[i][19]));// 9_Etalon_Thermistor_A
                tmpList.Add(Convert.ToDouble(Filter_Etalon_List[i][20]));// 10_Etalon_Thermistor_B
                tmpList.Add(Convert.ToDouble(Filter_Etalon_List[i][21]));// 11_Etalon_Thermistor_C
                int A = (int)(Math.Round(Freq_GHz / _FrequencyStep_Ghz));
                double B = Freq_GHz - (_FrequencyStep_Ghz * A + CAL_ITU_Offset_GHz);
                if (Math.Abs(B) <= _FrequencyTolerance_GHz)
                {
                    if (A - A / 2 * 2 == 0)
                    {
                        Max_Locker_Ratio_100GHz = Math.Max(Max_Locker_Ratio_100GHz, LockerRatio);
                        Min_Locker_Ratio_100GHz = Math.Min(Min_Locker_Ratio_100GHz, LockerRatio);
                    }
                    else
                    {
                        Max_Locker_Ratio_50GHz = Math.Max(Max_Locker_Ratio_50GHz, LockerRatio);
                        Min_Locker_Ratio_50GHz = Math.Min(Min_Locker_Ratio_50GHz, LockerRatio);
                    }
                    #region to calculate LockerRatioSlope
                    if (!Find_Frist_point)
                    {
                        Temp_First_LockerRatio = LockerRatio;
                        Temp_First_PowerRatio = PowerRatio;
                        Temp_First_frequency_GHz = Freq_GHz;
                        Find_Frist_point = true;
                    }
                    #endregion to calculate LockerRatioSlope
                }
                else
                {
                    if (Find_Frist_point)
                    {
                        Count++;
                        Temp_Last_LockerRatio = LockerRatio;
                        Temp_Last_PowerRatio = PowerRatio;
                        Temp_Last_frequency_GHz = Freq_GHz;
                        Average_Power_Ratio_SlopeToGHz += (Temp_Last_PowerRatio - Temp_First_PowerRatio) / (Temp_Last_frequency_GHz - Temp_First_frequency_GHz);
                        Average_Locker_Ratio_SlopeToGHz += Math.Abs((Temp_Last_LockerRatio - Temp_First_LockerRatio) / (Temp_Last_frequency_GHz - Temp_First_frequency_GHz));
                        Find_Frist_point = false;
                    }
                }
                Filter_Etalon_Data.Add(tmpList);
                midRatioEtalonTxToReference += Convert.ToDouble(Filter_Etalon_List[i][12]) / Convert.ToDouble(Filter_Etalon_List[i][8]) / ((Filter_Etalon_List.Count - 1) * 1.0);//column_12 is Etalon_Tx_mA,column_8 is Reference_mA. Jack.zhang 2012-09-16
                midRatioEtalonRxToReference += Convert.ToDouble(Filter_Etalon_List[i][15]) / Convert.ToDouble(Filter_Etalon_List[i][8]) / ((Filter_Etalon_List.Count - 1) * 1.0);//column_15 is Etalon_Rx_mA,column_8 is Reference_mA. Jack.zhang 2012-09-16
            }
            Delt_Locker_Ratio = Max_Delt_Locker_Ratio + Math.Max((Max_Locker_Ratio_50GHz - Min_Locker_Ratio_50GHz), (Max_Locker_Ratio_100GHz - Min_Locker_Ratio_100GHz));
            if (Delt_Locker_Ratio > 0)//Modify the ITU LockerRatio Range in order to cover at least two point in one range. Jack.zhang 2010-11-18
            {
                Max_Locker_Ratio_50GHz = Max_Locker_Ratio_50GHz + Delt_Locker_Ratio / 2.0;
                Min_Locker_Ratio_50GHz = Min_Locker_Ratio_50GHz - Delt_Locker_Ratio / 2.0;
                Max_Locker_Ratio_100GHz = Max_Locker_Ratio_100GHz + Delt_Locker_Ratio / 2.0;
                Min_Locker_Ratio_100GHz = Min_Locker_Ratio_100GHz - Delt_Locker_Ratio / 2.0;
            }
            Average_Locker_Ratio_SlopeToGHz = Average_Locker_Ratio_SlopeToGHz / Count;
            Average_Power_Ratio_SlopeToGHz = Average_Power_Ratio_SlopeToGHz / Count;
            _Reference_PD_Responsivity = LinearLeastSquaresFitAlgorithm.Calculate(Power_dBm.ToArray(), Reference_Current_mA.ToArray(), 0, Reference_Current_mA.Count - 1);
            Average_Reference_PD_Responsivity = Reference_PD_Responsivity / Power_dBm.Count;
        }

        public static void CalPRatio_LRatio_byFreq_GHz(double TargetFreq_GHz, out double PowerRatio, out double Filter_Etalon_Slope,
                                                        out double LockerRatio, out double First_PowerRatio, out double Last_PowerRatio, out bool In_Cal_Range)
        {

            double Freq_1, Freq_2, Power_Locker_R1, Power_Locker_R2;
            double Freq_Etalon_Star, Freq_Etalon_End, Etalon_Index;
            int Freq_Etalon_Star_Index = 0, Freq_Etalon_End_Index = 0;
            PowerRatio = 0; Filter_Etalon_Slope = 0; LockerRatio = 0; First_PowerRatio = 0; Last_PowerRatio = 0; In_Cal_Range = false;
            if ((TargetFreq_GHz - Filter_Etalon_Data[0][Freq_GHz_List_Index]) * (TargetFreq_GHz - Filter_Etalon_Data[Filter_Etalon_Data.Count - 1][Freq_GHz_List_Index]) < 0)
            {
                #region find power ratio by TargetFreq_GHz first
                for (int i = 0; i < Filter_Etalon_Data.Count; i++)
                {
                    if (Filter_Etalon_Data[i][Freq_GHz_List_Index] >= TargetFreq_GHz)
                    {
                        Freq_2 = Filter_Etalon_Data[i][Freq_GHz_List_Index];
                        Freq_1 = Filter_Etalon_Data[i - 1][Freq_GHz_List_Index];
                        Power_Locker_R2 = Filter_Etalon_Data[i][PowerRatio_List_Index];
                        Power_Locker_R1 = Filter_Etalon_Data[i - 1][PowerRatio_List_Index];
                        PowerRatio = Power_Locker_R2 - (Power_Locker_R2 - Power_Locker_R1) * (Freq_2 - TargetFreq_GHz) / (Freq_2 - Freq_1);
                        break;
                    }
                }
                #endregion find power ratio by TargetFreq_GHz first

                //index the etalon lineary change range base on the coase freq
                Etalon_Index = Math.Round(TargetFreq_GHz / _FrequencyStep_Ghz);

                Freq_Etalon_Star = (Etalon_Index - 1.5) * _FrequencyStep_Ghz;
                Freq_Etalon_End = (Etalon_Index + 1.5) * _FrequencyStep_Ghz;
                #region find power ratio on TargetFreq_GHz-50
                if ((Freq_Etalon_Star - Filter_Etalon_Data[0][Freq_GHz_List_Index]) * (Freq_Etalon_Star - Filter_Etalon_Data[Filter_Etalon_Data.Count - 1][Freq_GHz_List_Index]) < 0)
                {
                    for (int i = 0; i < Filter_Etalon_Data.Count; i++)
                    {
                        if (Filter_Etalon_Data[i][Freq_GHz_List_Index] >= Freq_Etalon_Star)
                        {
                            Freq_2 = Filter_Etalon_Data[i][Freq_GHz_List_Index];
                            Freq_1 = Filter_Etalon_Data[i - 1][Freq_GHz_List_Index];
                            Power_Locker_R2 = Filter_Etalon_Data[i][PowerRatio_List_Index];
                            Power_Locker_R1 = Filter_Etalon_Data[i - 1][PowerRatio_List_Index];
                            First_PowerRatio = Power_Locker_R2 - (Power_Locker_R2 - Power_Locker_R1) * (Freq_2 - Freq_Etalon_Star) / (Freq_2 - Freq_1);
                            break;
                        }
                    }
                }
                else
                {
                    First_PowerRatio = Filter_Etalon_Data[0][PowerRatio_List_Index];
                    if (Freq_Etalon_Star > Math.Max(Filter_Etalon_Data[0][Freq_GHz_List_Index], Filter_Etalon_Data[Filter_Etalon_Data.Count - 1][Freq_GHz_List_Index]))
                        First_PowerRatio = Filter_Etalon_Data[Filter_Etalon_Data.Count - 1][PowerRatio_List_Index];
                }
                #endregion find power ratio on TargetFreq_GHz-50

                #region find power ratio on TargetFreq_GHz+50
                if ((Freq_Etalon_End - Filter_Etalon_Data[0][Freq_GHz_List_Index]) * (Freq_Etalon_End - Filter_Etalon_Data[Filter_Etalon_Data.Count - 1][Freq_GHz_List_Index]) < 0)
                {
                    for (int i = 0; i < Filter_Etalon_Data.Count; i++)
                    {
                        if (Filter_Etalon_Data[i][Freq_GHz_List_Index] >= Freq_Etalon_End)
                        {
                            Freq_2 = Filter_Etalon_Data[i][Freq_GHz_List_Index];
                            Freq_1 = Filter_Etalon_Data[i - 1][Freq_GHz_List_Index];
                            Power_Locker_R2 = Filter_Etalon_Data[i][PowerRatio_List_Index];
                            Power_Locker_R1 = Filter_Etalon_Data[i - 1][PowerRatio_List_Index];
                            Last_PowerRatio = Power_Locker_R2 - (Power_Locker_R2 - Power_Locker_R1) * (Freq_2 - Freq_Etalon_End) / (Freq_2 - Freq_1);
                            break;
                        }
                    }
                }
                else
                {
                    Last_PowerRatio = Filter_Etalon_Data[0][PowerRatio_List_Index];
                    if (Freq_Etalon_End > Math.Max(Filter_Etalon_Data[0][Freq_GHz_List_Index], Filter_Etalon_Data[Filter_Etalon_Data.Count - 1][Freq_GHz_List_Index]))
                        Last_PowerRatio = Filter_Etalon_Data[Filter_Etalon_Data.Count - 1][PowerRatio_List_Index];
                }
                #endregion find power ratio on TargetFreq_GHz+50

                #region local freq range in lockerRatio
                Freq_Etalon_Star = (Etalon_Index-0.5) * _FrequencyStep_Ghz;
                Freq_Etalon_End = (Etalon_Index + 0.5) * _FrequencyStep_Ghz;
                if ((Freq_Etalon_Star - Filter_Etalon_Data[0][Freq_GHz_List_Index]) * (Freq_Etalon_Star - Filter_Etalon_Data[Filter_Etalon_Data.Count - 1][Freq_GHz_List_Index]) < 0)
                {
                    for (int i = 0; i < Filter_Etalon_Data.Count; i++)
                    {
                        if (Freq_Etalon_Star <= Math.Round(Filter_Etalon_Data[i][Freq_GHz_List_Index]))
                        {
                            Freq_Etalon_Star_Index = i;
                            break;
                        }
                    }
                }
                else
                    Freq_Etalon_Star_Index = 0;

                if ((Freq_Etalon_End - Filter_Etalon_Data[0][Freq_GHz_List_Index]) * (Freq_Etalon_End - Filter_Etalon_Data[Filter_Etalon_Data.Count - 1][Freq_GHz_List_Index]) < 0)
                {
                    for (int i = Filter_Etalon_Data.Count - 1; i >= 0; i--)
                    {
                        if (Freq_Etalon_End >= Math.Round(Filter_Etalon_Data[i][Freq_GHz_List_Index]))
                        {
                            Freq_Etalon_End_Index = i;
                            break;
                        }
                    }
                }
                else
                    Freq_Etalon_End_Index = Filter_Etalon_Data.Count - 1;

                #endregion local freq range in lockerRatio
                Filter_Etalon_Slope = (Filter_Etalon_Data[Freq_Etalon_Star_Index][LockerRatio_List_Index] - Filter_Etalon_Data[Freq_Etalon_End_Index][LockerRatio_List_Index]) / (Filter_Etalon_Data[Freq_Etalon_Star_Index][Freq_GHz_List_Index] - Filter_Etalon_Data[Freq_Etalon_End_Index][Freq_GHz_List_Index]);
                #region find the locker ratio by TargetFreq_GHz
                for (int i = Freq_Etalon_Star_Index; i <= Freq_Etalon_End_Index; i++)
                {
                    if (TargetFreq_GHz <= Filter_Etalon_Data[i][Freq_GHz_List_Index])
                    {
                        Freq_2 = Filter_Etalon_Data[i][Freq_GHz_List_Index];
                        Freq_1 = Filter_Etalon_Data[i - 1][Freq_GHz_List_Index];
                        Power_Locker_R2 = Filter_Etalon_Data[i][LockerRatio_List_Index];
                        Power_Locker_R1 = Filter_Etalon_Data[i - 1][LockerRatio_List_Index];
                        LockerRatio = Power_Locker_R2 - (Power_Locker_R2 - Power_Locker_R1) * (Freq_2 - TargetFreq_GHz) / (Freq_2 - Freq_1);
                        break;
                    }
                }
                #endregion find the locker ratio by TargetFreq_GHz
                In_Cal_Range = true;
            }
        }
        public static double Modify_Freq_by_EtaloIndex(double Etalon_Index, double LockerRatio)
        {

            double Freq_Fine = 0, Freq_1, Freq_2, Power_Locker_R1, Power_Locker_R2;
            double Freq_Etalon_Star, Freq_Etalon_End;
            int Freq_Etalon_Star_Index = 0, Freq_Etalon_End_Index = 0;

            #region local freq range in lockerRatio
            Freq_Etalon_Star = (Etalon_Index - 0.5) * _FrequencyStep_Ghz;
            Freq_Etalon_End = (Etalon_Index+0.5) * _FrequencyStep_Ghz;

            for (int i = 0; i < Filter_Etalon_Data.Count; i++)
            {
                if (Freq_Etalon_Star <= Math.Round(Filter_Etalon_Data[i][Freq_GHz_List_Index]))
                {
                    Freq_Etalon_Star_Index = i;
                    break;
                }
            }
            for (int i = Filter_Etalon_Data.Count - 1; i >= 0; i--)
            {
                if (Freq_Etalon_End >= Math.Round(Filter_Etalon_Data[i][Freq_GHz_List_Index]))
                {
                    Freq_Etalon_End_Index = i;
                    break;
                }
            }
            #endregion local freq range in lockerRatio

            #region find the fine freq by locker ratio
            double Filter_Etalon_Slope = (Filter_Etalon_Data[Freq_Etalon_Star_Index][Freq_GHz_List_Index] - Filter_Etalon_Data[Freq_Etalon_End_Index][Freq_GHz_List_Index]) / (Filter_Etalon_Data[Freq_Etalon_Star_Index][LockerRatio_List_Index] - Filter_Etalon_Data[Freq_Etalon_End_Index][LockerRatio_List_Index]);
            for (int i = Freq_Etalon_Star_Index; i <= Freq_Etalon_End_Index; i++)
            {
                if (LockerRatio * Math.Sign(Filter_Etalon_Slope) <= Filter_Etalon_Data[i][LockerRatio_List_Index] * Math.Sign(Filter_Etalon_Slope))
                {
                    Freq_1 = Filter_Etalon_Data[i][Freq_GHz_List_Index];
                    Freq_2 = Filter_Etalon_Data[i - Math.Sign(Filter_Etalon_Slope)][Freq_GHz_List_Index];
                    Power_Locker_R1 = Filter_Etalon_Data[i][LockerRatio_List_Index];
                    Power_Locker_R2 = Filter_Etalon_Data[i - Math.Sign(Filter_Etalon_Slope)][LockerRatio_List_Index];
                    Freq_Fine = Freq_2 - (Freq_2 - Freq_1) * (Power_Locker_R2 - LockerRatio) / (Power_Locker_R2 - Power_Locker_R1);

                    break;
                }
            }
            #endregion find the fine freq by locker ratio
            return Freq_Fine;

        }

        public static double CalFreqByPRatio_LRatio_GHz(double PowerRatio, double LockerRatio, int LockerRatioSlope_Sign)
        {

            double Target_Power_Ratio_1, Target_Power_Ratio_2, Target_Locker_Ratio = 0.0, First_Target_Power_Ratio_1, Second_Target_Power_Ratio_1, First_Target_Power_Ratio_2, Second_Target_Power_Ratio_2;
            bool In_Cal_Range;
            double Freq_1, Freq_2, Power_Locker_R1, Power_Locker_R2, Freq_Coase = 0, Freq_Fine = 0, delt_freq = 0;
            double Freq_Etalon_Star, Freq_Etalon_End, Etalon_Index;
            //Filter_Etalon_Slope now is Filter slope
            double Filter_Etalon_Slope = (Filter_Etalon_Data[0][Freq_GHz_List_Index] - Filter_Etalon_Data[Filter_Etalon_Data.Count - 1][Freq_GHz_List_Index]) / (Filter_Etalon_Data[0][PowerRatio_List_Index] - Filter_Etalon_Data[Filter_Etalon_Data.Count - 1][PowerRatio_List_Index]);
            int Freq_Etalon_Star_Index = 0, Freq_Etalon_End_Index = 0;
            if ((Math.Sign(Filter_Etalon_Slope) < 0 && (Filter_Etalon_Data[0][PowerRatio_List_Index] < PowerRatio))
                || (Math.Sign(Filter_Etalon_Slope) > 0 && (Filter_Etalon_Data[Filter_Etalon_Data.Count - 1][PowerRatio_List_Index] < PowerRatio)))

                Freq_Fine = 0;
            else
            {
                #region find coase freq by power ratio first
                for (int i = 0; i < Filter_Etalon_Data.Count; i++)
                {
                    if (Filter_Etalon_Data[i][PowerRatio_List_Index] * Math.Sign(Filter_Etalon_Slope) >= PowerRatio * Math.Sign(Filter_Etalon_Slope))
                    {
                        Freq_1 = Filter_Etalon_Data[i][Freq_GHz_List_Index];
                        Freq_2 = Filter_Etalon_Data[i + Math.Sign(Filter_Etalon_Slope)][Freq_GHz_List_Index];
                        Power_Locker_R1 = Filter_Etalon_Data[i][PowerRatio_List_Index];
                        Power_Locker_R2 = Filter_Etalon_Data[i + Math.Sign(Filter_Etalon_Slope)][PowerRatio_List_Index];
                        Freq_Coase = Freq_2 - (Freq_2 - Freq_1) * (Power_Locker_R2 - PowerRatio) / (Power_Locker_R2 - Power_Locker_R1);
                        break;
                    }
                }
                #endregion find coase freq by power ratio first


                //index the etalon lineary change range base on the coase freq

                Etalon_Index = Math.Round(Freq_Coase / _FrequencyStep_Ghz);

                CalPRatio_LRatio_byFreq_GHz(Etalon_Index * _FrequencyStep_Ghz, out Target_Power_Ratio_1, out Filter_Etalon_Slope,//Filter_Etalon_Slope now is LockerRatio Slope
                                                                out Target_Locker_Ratio, out First_Target_Power_Ratio_1,
                                                                out Second_Target_Power_Ratio_1, out In_Cal_Range);

                if (LockerRatioSlope_Sign !=Math.Sign(Filter_Etalon_Slope))
                {
                    CalPRatio_LRatio_byFreq_GHz((Etalon_Index - 1) * _FrequencyStep_Ghz, out Target_Power_Ratio_1, out Filter_Etalon_Slope,
                                                out Target_Locker_Ratio, out First_Target_Power_Ratio_1,
                                                out Second_Target_Power_Ratio_1, out In_Cal_Range);
                    if ((PowerRatio - First_Target_Power_Ratio_1) * (PowerRatio - Second_Target_Power_Ratio_1) >= 0)
                        //the power ratio is not in the range.find the min delta PR due to the rip in PR
                        Target_Power_Ratio_1 = Math.Min(Math.Abs(PowerRatio - First_Target_Power_Ratio_1), Math.Abs(PowerRatio - Second_Target_Power_Ratio_1));
                    else
                        Target_Power_Ratio_1 =-1* Math.Min(Math.Abs(PowerRatio - First_Target_Power_Ratio_1), Math.Abs(PowerRatio - Second_Target_Power_Ratio_1)) / Math.Max(Math.Abs(PowerRatio - First_Target_Power_Ratio_1), Math.Abs(PowerRatio - Second_Target_Power_Ratio_1)); //the power ratio is in the range.
                    CalPRatio_LRatio_byFreq_GHz((Etalon_Index + 1) * _FrequencyStep_Ghz, out Target_Power_Ratio_2, out Filter_Etalon_Slope,
                                                out Target_Locker_Ratio, out First_Target_Power_Ratio_2,
                                                out Second_Target_Power_Ratio_2, out In_Cal_Range);
                    if ((PowerRatio - First_Target_Power_Ratio_2) * (PowerRatio - Second_Target_Power_Ratio_2) >= 0)
                        Target_Power_Ratio_2 = Math.Min(Math.Abs(PowerRatio - First_Target_Power_Ratio_2), Math.Abs(PowerRatio - Second_Target_Power_Ratio_2));
                    else
                        Target_Power_Ratio_2 = -1 * Math.Min(Math.Abs(PowerRatio - First_Target_Power_Ratio_2), Math.Abs(PowerRatio - Second_Target_Power_Ratio_2)) / Math.Max(Math.Abs(PowerRatio - First_Target_Power_Ratio_2), Math.Abs(PowerRatio - Second_Target_Power_Ratio_2));
                    //change between 50GHz and 100GHz due to locker ratio slope diff. jack.zhang 
                    Etalon_Index =Target_Power_Ratio_1 > Target_Power_Ratio_2? Etalon_Index + 1:Etalon_Index - 1;//find the closest range to the power Ratio     
                }

                do
                {
                    #region local freq range in lockerRatio
                    Freq_Etalon_Star = (Etalon_Index - 0.5) * _FrequencyStep_Ghz;
                    Freq_Etalon_End = (Etalon_Index + 0.5) * _FrequencyStep_Ghz;

                    for (int i = 0; i < Filter_Etalon_Data.Count; i++)
                    {
                        if (Freq_Etalon_Star <= Math.Round(Filter_Etalon_Data[i][Freq_GHz_List_Index]))
                        {
                            Freq_Etalon_Star_Index = i;
                            break;
                        }
                    }
                    for (int i = Filter_Etalon_Data.Count - 1; i >= 0; i--)
                    {
                        if (Freq_Etalon_End >= Math.Round(Filter_Etalon_Data[i][Freq_GHz_List_Index]))
                        {
                            Freq_Etalon_End_Index = i;
                            break;
                        }
                    }
                    #endregion local freq range in lockerRatio

                    #region find the fine freq by locker ratio
                    Filter_Etalon_Slope = (Filter_Etalon_Data[Freq_Etalon_Star_Index][Freq_GHz_List_Index] - Filter_Etalon_Data[Freq_Etalon_End_Index][Freq_GHz_List_Index]) / (Filter_Etalon_Data[Freq_Etalon_Star_Index][LockerRatio_List_Index] - Filter_Etalon_Data[Freq_Etalon_End_Index][LockerRatio_List_Index]);
                    for (int i = Freq_Etalon_Star_Index; i <= Freq_Etalon_End_Index; i++)
                    {
                        try
                        {
                            if (LockerRatio * Math.Sign(Filter_Etalon_Slope) <= Filter_Etalon_Data[i][LockerRatio_List_Index] * Math.Sign(Filter_Etalon_Slope))
                            {
                                Freq_1 = Filter_Etalon_Data[i][Freq_GHz_List_Index];
                                Freq_2 = Filter_Etalon_Data[i - Math.Sign(Filter_Etalon_Slope)][Freq_GHz_List_Index];
                                Power_Locker_R1 = Filter_Etalon_Data[i][LockerRatio_List_Index];
                                Power_Locker_R2 = Filter_Etalon_Data[i - Math.Sign(Filter_Etalon_Slope)][LockerRatio_List_Index];
                                Freq_Fine = Freq_2 - (Freq_2 - Freq_1) * (Power_Locker_R2 - LockerRatio) / (Power_Locker_R2 - Power_Locker_R1);

                                break;
                            }
                        }
                        catch (Exception)
                        {
                            Freq_Fine = 0;
                        }
                    }
                    #endregion find the fine freq by locker ratio
                    //                        delt_freq = Freq_Fine - TargetFreq_GHz;
                    //if (Math.Abs(delt_freq) >= 30)//for mapping test, Freq_Get can be changed to privious ITU point;
                    //{
                    //    if (Freq_Fine > TargetFreq_GHz)
                    //        Etalon_Index = Etalon_Index - 1;
                    //    else
                    //        Etalon_Index = Etalon_Index + 1;
                    //}

                } while (Math.Abs(delt_freq) >= 30);// just for mapping test analysis
            }
            return Freq_Fine;
        }

        public static double CalFreqByPRatio_LRatio_GHz(double PowerRatio, double LockerRatio, double ReferenceFreq_Ghz)
        {

            double Freq_1, Freq_2, Power_Locker_R1, Power_Locker_R2, Freq_Coase = 0, Freq_Fine = 0, delt_freq = 0;
            double Freq_Etalon_Star, Freq_Etalon_End, Etalon_Index;
            double Filter_Etalon_Slope = (Filter_Etalon_Data[0][Freq_GHz_List_Index] - Filter_Etalon_Data[Filter_Etalon_Data.Count - 1][Freq_GHz_List_Index]) / (Filter_Etalon_Data[0][PowerRatio_List_Index] - Filter_Etalon_Data[Filter_Etalon_Data.Count - 1][PowerRatio_List_Index]);
            int Freq_Etalon_Star_Index = 0, Freq_Etalon_End_Index = 0;
            if ((Math.Sign(Filter_Etalon_Slope) < 0 && (Filter_Etalon_Data[0][PowerRatio_List_Index] < PowerRatio))
                || (Math.Sign(Filter_Etalon_Slope) > 0 && (Filter_Etalon_Data[Filter_Etalon_Data.Count - 1][PowerRatio_List_Index] < PowerRatio)))

                Freq_Fine = 0;
            else
            {
                #region find coase freq by power ratio first
                for (int i = 0; i < Filter_Etalon_Data.Count; i++)
                {
                    if (Filter_Etalon_Data[i][PowerRatio_List_Index] * Math.Sign(Filter_Etalon_Slope) >= PowerRatio * Math.Sign(Filter_Etalon_Slope))
                    {
                        Freq_1 = Filter_Etalon_Data[i][Freq_GHz_List_Index];
                        Freq_2 = Filter_Etalon_Data[i + Math.Sign(Filter_Etalon_Slope)][Freq_GHz_List_Index];
                        Power_Locker_R1 = Filter_Etalon_Data[i][PowerRatio_List_Index];
                        Power_Locker_R2 = Filter_Etalon_Data[i + Math.Sign(Filter_Etalon_Slope)][PowerRatio_List_Index];
                        Freq_Coase = Freq_2 - (Freq_2 - Freq_1) * (Power_Locker_R2 - PowerRatio) / (Power_Locker_R2 - Power_Locker_R1);
                        break;
                    }
                }
                #endregion find coase freq by power ratio first


                //index the etalon lineary change range base on the coase freq

                Etalon_Index = Math.Round(Freq_Coase / _FrequencyStep_Ghz);

                if (Etalon_Index != Math.Round(ReferenceFreq_Ghz / _FrequencyStep_Ghz) && ReferenceFreq_Ghz > 0)
                {
                    Etalon_Index = Math.Round(ReferenceFreq_Ghz / _FrequencyStep_Ghz);
                }

                do
                {
                    #region local freq range in lockerRatio
                    Freq_Etalon_Star = (Etalon_Index - 0.5) * _FrequencyStep_Ghz;
                    Freq_Etalon_End = (Etalon_Index + 0.5) * _FrequencyStep_Ghz;

                    for (int i = 0; i < Filter_Etalon_Data.Count; i++)
                    {
                        if (Freq_Etalon_Star <= Math.Round(Filter_Etalon_Data[i][Freq_GHz_List_Index]))
                        {
                            Freq_Etalon_Star_Index = i;
                            break;
                        }
                    }
                    for (int i = Filter_Etalon_Data.Count - 1; i >= 0; i--)
                    {
                        if (Freq_Etalon_End >= Math.Round(Filter_Etalon_Data[i][Freq_GHz_List_Index]))
                        {
                            Freq_Etalon_End_Index = i;
                            break;
                        }
                    }
                    #endregion local freq range in lockerRatio

                    #region find the fine freq by locker ratio
                    Filter_Etalon_Slope = (Filter_Etalon_Data[Freq_Etalon_Star_Index][Freq_GHz_List_Index] - Filter_Etalon_Data[Freq_Etalon_End_Index][Freq_GHz_List_Index]) / (Filter_Etalon_Data[Freq_Etalon_Star_Index][LockerRatio_List_Index] - Filter_Etalon_Data[Freq_Etalon_End_Index][LockerRatio_List_Index]);
                    for (int i = Freq_Etalon_Star_Index; i <= Freq_Etalon_End_Index; i++)
                    {
                        try
                        {
                            if (LockerRatio * Math.Sign(Filter_Etalon_Slope) <= Filter_Etalon_Data[i][LockerRatio_List_Index] * Math.Sign(Filter_Etalon_Slope))
                            {
                                Freq_1 = Filter_Etalon_Data[i][Freq_GHz_List_Index];
                                Freq_2 = Filter_Etalon_Data[i - Math.Sign(Filter_Etalon_Slope)][Freq_GHz_List_Index];
                                Power_Locker_R1 = Filter_Etalon_Data[i][LockerRatio_List_Index];
                                Power_Locker_R2 = Filter_Etalon_Data[i - Math.Sign(Filter_Etalon_Slope)][LockerRatio_List_Index];
                                Freq_Fine = Freq_2 - (Freq_2 - Freq_1) * (Power_Locker_R2 - LockerRatio) / (Power_Locker_R2 - Power_Locker_R1);

                                break;
                            }
                        }
                        catch (Exception)
                        {
                            Freq_Fine = 0;
                        }
                    }
                    #endregion find the fine freq by locker ratio
                    //                        delt_freq = Freq_Fine - TargetFreq_GHz;
                    //if (Math.Abs(delt_freq) >= 30)//for mapping test, Freq_Get can be changed to privious ITU point;
                    //{
                    //    if (Freq_Fine > TargetFreq_GHz)
                    //        Etalon_Index = Etalon_Index - 1;
                    //    else
                    //        Etalon_Index = Etalon_Index + 1;
                    //}

                } while (Math.Abs(delt_freq) >= 30);// just for mapping test analysis
            }
            return Freq_Fine;
        }
        public static double Median(List<double> list)
        {
            double returnValue;
            list.Sort();
            if (list.Count > 1)
            {
                if ((list.Count / 2) == 0)
                {  // inthe mean of midee intow value
                    double first = list[(list.Count / 2) - 1];
                    double second = list[list.Count / 2];
                    returnValue = (first + second) / 2;
                }
                else
                {
                    returnValue = list[list.Count / 2];
                }
            }
            else
                returnValue = list[list.Count - 1];
            return returnValue;
        }
        public static double Average(List<double> list)
        {
            double returnValue;
            double Sum = 0;
            for (int i = 0; i < list.Count; i++)
            {
                Sum = Sum + list[i];

            }
            returnValue = Sum / list.Count;
            return returnValue;
        }

        public static bool UseDUTLockerRatio
        {
            get
            {
                return _UseDUTLockerRatio;
            }
            set
            {
                _UseDUTLockerRatio = value;
            }
        }
        
        public static LinearLeastSquaresFit DUTLockerRatioSlope_50GHz
        {
            get
            {
                return _DUTLockerRatioSlope_50GHz;
            }
            set
            {
                _DUTLockerRatioSlope_50GHz = value;
                if (_UseDUTLockerRatio)
                {
                    Min_Locker_Ratio_50GHz =
                        Math.Min(_DUTLockerRatioSlope_50GHz.Slope * -2 * _FrequencyTolerance_GHz + _DUTLockerRatioSlope_50GHz.YIntercept, _DUTLockerRatioSlope_50GHz.Slope *2* _FrequencyTolerance_GHz + _DUTLockerRatioSlope_50GHz.YIntercept);

                    Max_Locker_Ratio_50GHz =
                        Math.Max(_DUTLockerRatioSlope_50GHz.Slope * -2 * _FrequencyTolerance_GHz + _DUTLockerRatioSlope_50GHz.YIntercept, _DUTLockerRatioSlope_50GHz.Slope *2* _FrequencyTolerance_GHz + _DUTLockerRatioSlope_50GHz.YIntercept);
                }
            }
        }

        public static LinearLeastSquaresFit DUTLockerRatioSlope_100GHz
        {
            get
            {
                return _DUTLockerRatioSlope_100GHz;
            }
            set
            {
                _DUTLockerRatioSlope_100GHz = value;
                if (_UseDUTLockerRatio)
                {
                    Min_Locker_Ratio_100GHz =
                        Math.Min(_DUTLockerRatioSlope_100GHz.Slope * -2 * _FrequencyTolerance_GHz + _DUTLockerRatioSlope_100GHz.YIntercept, _DUTLockerRatioSlope_100GHz.Slope *2* _FrequencyTolerance_GHz + _DUTLockerRatioSlope_100GHz.YIntercept);

                    Max_Locker_Ratio_100GHz =
                        Math.Max(_DUTLockerRatioSlope_100GHz.Slope * -2 * _FrequencyTolerance_GHz + _DUTLockerRatioSlope_100GHz.YIntercept, _DUTLockerRatioSlope_100GHz.Slope *2* _FrequencyTolerance_GHz + _DUTLockerRatioSlope_100GHz.YIntercept);
                }
            }
        }
        public static LinearLeastSquaresFit Reference_PD_Responsivity
        {
            get
            {
                return _Reference_PD_Responsivity;
            }
            set
            {
                _DUTLockerRatioSlope_100GHz = value;
            }
        }
        public static double FrequencyTolerance_GHz
        {
            get
            {
                return _FrequencyTolerance_GHz;
            }
            set
            {
                _FrequencyTolerance_GHz = value;
            }
        }


        public static double MidRatioEtalonTxToReference
        {
            get { return midRatioEtalonTxToReference; }
            //set { midRatioEtalonTxToReference = value; }
        }

        public static double MidRatioEtalonRxToReference
        {
            get { return midRatioEtalonRxToReference; }
            //set { midRatioEtalonRxToReference = value; }
        }
            
    }

    public struct FCUMKI_DAC2mA_Cal_Parameter
    {
        /// <summary>
        /// DAC to mA Pot
        /// </summary>
        public int Pot;
        /// <summary>
        /// DAC to mA Slope
        /// </summary>
        public double Slope;
        /// <summary>
        /// DAC to mA offset
        /// </summary>
        public double offset;

    }
    public struct FCUMKI_DAC2mA_Cal_Port
    {
        /// <summary>
        /// DAC to mA Pot
        /// </summary>
        public List<FCUMKI_DAC2mA_Cal_Parameter> Tx;
        /// <summary>
        /// DAC to mA Slope
        /// </summary>
        public List<FCUMKI_DAC2mA_Cal_Parameter> Rx;
        /// <summary>
        /// DAC to mA offset
        /// </summary>
        public List<FCUMKI_DAC2mA_Cal_Parameter> Var;
        /// <summary>
        /// minute DARK_DAC(come from FCUMKI calibration) from the DAC before DAC to mA 
        /// </summary>
        public List<FCUMKI_DAC2mA_Cal_Parameter> Fix;
    }
}

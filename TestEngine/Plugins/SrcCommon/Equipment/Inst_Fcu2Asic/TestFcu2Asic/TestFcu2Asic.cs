using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
//using TestFcu2Asic;

namespace TestFcu2Asic
{
    public partial class TestFcu2Asic : Form
    {
        public TestFcu2Asic()
        {
            InitializeComponent();
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            txtFilePath.Text = openFileDialog1.FileName;
        }

        private void cmdBrowe_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
        }

        private void TestFcu2Asic_Load(object sender, EventArgs e)
        {
            testAsic = new Test_Object();
        }

        private void cmdSetCalibration_Click(object sender, EventArgs e)
        {
            testAsic.Setup(txtFilePath.Text);
        }
        private void btnReadDSDBR_Click(object sender, EventArgs e)
        {

            tosadata = testAsic.TosaSetup;

            lstFsPair.Text = tosadata.frontPair.ToString();
            lstFsPair.Refresh();
            txtConstantCurrent.Text = tosadata.constantCurrent.ToString();
            txtConstantCurrent.Refresh();
            txtNoConstantCurrent.Text = tosadata.UnconstantCurrent.ToString();
            txtNoConstantCurrent.Refresh();
            txtPhase.Text = tosadata.phase.ToString();
            txtPhase.Refresh();
            txtGain.Text = tosadata.gain.ToString();
            txtGain.Refresh();
            txtRear.Text = tosadata.rear.ToString();
            txtRear.Refresh();
            txtFrotSoa.Text = tosadata.frontSoa.ToString();
            txtFrotSoa.Refresh();
            txtRearSoa.Text = tosadata.rearSoa.ToString();
            txtRearSoa.Refresh();
            txtLeftImb.Text = tosadata.leftImb.ToString();
            txtLeftImb.Refresh();
            txtRightImb.Text = tosadata.rightImb.ToString();
            txtRightImb.Refresh();
        }

        
        private void btnSetTosa_Click(object sender, EventArgs e)
        {
            tosadata = new Test_Object.StrcTosaSetup();

            tosadata.frontPair = Convert.ToInt32(lstFsPair.Text);
            tosadata.constantCurrent = Convert.ToDouble(txtConstantCurrent.Text);
            tosadata.UnconstantCurrent = Convert.ToDouble(txtNoConstantCurrent.Text);

            tosadata.phase = Convert.ToDouble(txtPhase.Text);
            tosadata.gain = Convert.ToDouble(txtGain.Text);
            tosadata.rear = Convert.ToDouble(txtRear.Text);
            tosadata.frontSoa = Convert.ToDouble(txtFrotSoa.Text);
            tosadata.rearSoa = Convert.ToDouble(txtRearSoa.Text);

            tosadata.leftImb = Convert.ToDouble(txtLeftImb.Text);
            tosadata.rightImb = Convert.ToDouble(txtRightImb.Text);

        }

        private void btnReadLocker_Click(object sender, EventArgs e)
        {
            Test_Object.StrcLockerCurrent lockerdata;
            lockerdata = testAsic.GetLockerCurrents();

            txtTransmit.Text = lockerdata.lockTransmit.ToString();
            txtTransmit.Refresh();
            txtReflect.Text = lockerdata.lockReflect.ToString();
            txtReflect.Refresh();
        }


        private Test_Object testAsic;
        private Test_Object.StrcTosaSetup tosadata;

        private void TestFcu2Asic_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (testAsic != null) testAsic.ShutDown();
        }

    }
}
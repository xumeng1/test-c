namespace TestFcu2Asic
{
    partial class FrmTestFcu2Asic
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtRightImb = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtLeftImb = new System.Windows.Forms.TextBox();
            this.txtRearSoa = new System.Windows.Forms.TextBox();
            this.txtFrotSoa = new System.Windows.Forms.TextBox();
            this.txtPhase = new System.Windows.Forms.TextBox();
            this.txtRear = new System.Windows.Forms.TextBox();
            this.txtGain = new System.Windows.Forms.TextBox();
            this.txtNoConstantCurrent = new System.Windows.Forms.TextBox();
            this.txtConstantCurrent = new System.Windows.Forms.TextBox();
            this.lstFsPair = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtReflect = new System.Windows.Forms.TextBox();
            this.txtTransmit = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.cmdBrowe = new System.Windows.Forms.Button();
            this.txtFilePath = new System.Windows.Forms.TextBox();
            this.btnReadDSDBR = new System.Windows.Forms.Button();
            this.btnSetTosa = new System.Windows.Forms.Button();
            this.btnReadLocker = new System.Windows.Forms.Button();
            this.cmdSetCalibration = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.txtRightImb);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtLeftImb);
            this.groupBox1.Controls.Add(this.txtRearSoa);
            this.groupBox1.Controls.Add(this.txtFrotSoa);
            this.groupBox1.Controls.Add(this.txtPhase);
            this.groupBox1.Controls.Add(this.txtRear);
            this.groupBox1.Controls.Add(this.txtGain);
            this.groupBox1.Controls.Add(this.txtNoConstantCurrent);
            this.groupBox1.Controls.Add(this.txtConstantCurrent);
            this.groupBox1.Controls.Add(this.lstFsPair);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(345, 492);
            this.groupBox1.TabIndex = 18;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Tosa Setting";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(29, 457);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(73, 16);
            this.label13.TabIndex = 37;
            this.label13.Text = "Right Imb";
            // 
            // txtRightImb
            // 
            this.txtRightImb.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRightImb.Location = new System.Drawing.Point(185, 451);
            this.txtRightImb.Name = "txtRightImb";
            this.txtRightImb.Size = new System.Drawing.Size(140, 22);
            this.txtRightImb.TabIndex = 36;
            this.txtRightImb.Text = "0";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(29, 409);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 16);
            this.label7.TabIndex = 35;
            this.label7.Text = "Left Imb";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(29, 361);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(74, 16);
            this.label8.TabIndex = 34;
            this.label8.Text = "Rear Soa";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(28, 313);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(75, 16);
            this.label9.TabIndex = 33;
            this.label9.Text = "Front Soa";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(28, 265);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 16);
            this.label4.TabIndex = 32;
            this.label4.Text = "Phase";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(28, 217);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 16);
            this.label5.TabIndex = 31;
            this.label5.Text = "Rear";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(28, 169);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 16);
            this.label6.TabIndex = 30;
            this.label6.Text = "Gain";
            // 
            // txtLeftImb
            // 
            this.txtLeftImb.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLeftImb.Location = new System.Drawing.Point(185, 403);
            this.txtLeftImb.Name = "txtLeftImb";
            this.txtLeftImb.Size = new System.Drawing.Size(140, 22);
            this.txtLeftImb.TabIndex = 29;
            this.txtLeftImb.Text = "0";
            // 
            // txtRearSoa
            // 
            this.txtRearSoa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRearSoa.Location = new System.Drawing.Point(185, 355);
            this.txtRearSoa.Name = "txtRearSoa";
            this.txtRearSoa.Size = new System.Drawing.Size(140, 22);
            this.txtRearSoa.TabIndex = 28;
            this.txtRearSoa.Text = "0";
            // 
            // txtFrotSoa
            // 
            this.txtFrotSoa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFrotSoa.Location = new System.Drawing.Point(185, 307);
            this.txtFrotSoa.Name = "txtFrotSoa";
            this.txtFrotSoa.Size = new System.Drawing.Size(140, 22);
            this.txtFrotSoa.TabIndex = 27;
            this.txtFrotSoa.Text = "150";
            // 
            // txtPhase
            // 
            this.txtPhase.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhase.Location = new System.Drawing.Point(185, 259);
            this.txtPhase.Name = "txtPhase";
            this.txtPhase.Size = new System.Drawing.Size(140, 22);
            this.txtPhase.TabIndex = 26;
            this.txtPhase.Text = "2.48028";
            // 
            // txtRear
            // 
            this.txtRear.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRear.Location = new System.Drawing.Point(185, 211);
            this.txtRear.Name = "txtRear";
            this.txtRear.Size = new System.Drawing.Size(140, 22);
            this.txtRear.TabIndex = 25;
            this.txtRear.Text = "60.1099";
            // 
            // txtGain
            // 
            this.txtGain.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGain.Location = new System.Drawing.Point(185, 163);
            this.txtGain.Name = "txtGain";
            this.txtGain.Size = new System.Drawing.Size(140, 22);
            this.txtGain.TabIndex = 24;
            this.txtGain.Text = "150";
            // 
            // txtNoConstantCurrent
            // 
            this.txtNoConstantCurrent.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoConstantCurrent.Location = new System.Drawing.Point(185, 115);
            this.txtNoConstantCurrent.Name = "txtNoConstantCurrent";
            this.txtNoConstantCurrent.Size = new System.Drawing.Size(140, 22);
            this.txtNoConstantCurrent.TabIndex = 23;
            this.txtNoConstantCurrent.Text = "5";
            // 
            // txtConstantCurrent
            // 
            this.txtConstantCurrent.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtConstantCurrent.Location = new System.Drawing.Point(185, 67);
            this.txtConstantCurrent.Name = "txtConstantCurrent";
            this.txtConstantCurrent.Size = new System.Drawing.Size(140, 22);
            this.txtConstantCurrent.TabIndex = 22;
            this.txtConstantCurrent.Text = "5";
            // 
            // lstFsPair
            // 
            this.lstFsPair.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstFsPair.FormattingEnabled = true;
            this.lstFsPair.ItemHeight = 16;
            this.lstFsPair.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7"});
            this.lstFsPair.Location = new System.Drawing.Point(185, 21);
            this.lstFsPair.Name = "lstFsPair";
            this.lstFsPair.Size = new System.Drawing.Size(141, 20);
            this.lstFsPair.TabIndex = 21;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(28, 121);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(143, 16);
            this.label3.TabIndex = 20;
            this.label3.Text = "No Constant current";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(28, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(119, 16);
            this.label2.TabIndex = 19;
            this.label2.Text = "Constant current";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(28, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(133, 16);
            this.label1.TabIndex = 18;
            this.label1.Text = "Front Pair Number";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtReflect);
            this.groupBox2.Controls.Add(this.txtTransmit);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(384, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(366, 148);
            this.groupBox2.TabIndex = 19;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Locker Current";
            // 
            // txtReflect
            // 
            this.txtReflect.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReflect.Location = new System.Drawing.Point(136, 113);
            this.txtReflect.Name = "txtReflect";
            this.txtReflect.Size = new System.Drawing.Size(140, 22);
            this.txtReflect.TabIndex = 34;
            // 
            // txtTransmit
            // 
            this.txtTransmit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTransmit.Location = new System.Drawing.Point(136, 47);
            this.txtTransmit.Name = "txtTransmit";
            this.txtTransmit.Size = new System.Drawing.Size(140, 22);
            this.txtTransmit.TabIndex = 33;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(35, 119);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(57, 16);
            this.label11.TabIndex = 32;
            this.label11.Text = "Reflect";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(35, 51);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(68, 16);
            this.label12.TabIndex = 31;
            this.label12.Text = "Transmit";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(393, 206);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(176, 16);
            this.label10.TabIndex = 38;
            this.label10.Text = "FCU Calibration file path";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // cmdBrowe
            // 
            this.cmdBrowe.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdBrowe.Location = new System.Drawing.Point(583, 248);
            this.cmdBrowe.Name = "cmdBrowe";
            this.cmdBrowe.Size = new System.Drawing.Size(29, 22);
            this.cmdBrowe.TabIndex = 39;
            this.cmdBrowe.Text = "...";
            this.cmdBrowe.UseVisualStyleBackColor = true;
            this.cmdBrowe.Click += new System.EventHandler(this.cmdBrowe_Click);
            // 
            // txtFilePath
            // 
            this.txtFilePath.Location = new System.Drawing.Point(396, 250);
            this.txtFilePath.Name = "txtFilePath";
            this.txtFilePath.Size = new System.Drawing.Size(191, 20);
            this.txtFilePath.TabIndex = 40;
            // 
            // btnReadDSDBR
            // 
            this.btnReadDSDBR.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReadDSDBR.Location = new System.Drawing.Point(696, 301);
            this.btnReadDSDBR.Name = "btnReadDSDBR";
            this.btnReadDSDBR.Size = new System.Drawing.Size(133, 40);
            this.btnReadDSDBR.TabIndex = 41;
            this.btnReadDSDBR.Text = "Read TOSA";
            this.btnReadDSDBR.UseVisualStyleBackColor = true;
            this.btnReadDSDBR.Click += new System.EventHandler(this.btnReadDSDBR_Click);
            // 
            // btnSetTosa
            // 
            this.btnSetTosa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSetTosa.Location = new System.Drawing.Point(696, 373);
            this.btnSetTosa.Name = "btnSetTosa";
            this.btnSetTosa.Size = new System.Drawing.Size(133, 40);
            this.btnSetTosa.TabIndex = 42;
            this.btnSetTosa.Text = "Set TOSA";
            this.btnSetTosa.UseVisualStyleBackColor = true;
            this.btnSetTosa.Click += new System.EventHandler(this.btnSetTosa_Click);
            // 
            // btnReadLocker
            // 
            this.btnReadLocker.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReadLocker.Location = new System.Drawing.Point(696, 445);
            this.btnReadLocker.Name = "btnReadLocker";
            this.btnReadLocker.Size = new System.Drawing.Size(133, 40);
            this.btnReadLocker.TabIndex = 43;
            this.btnReadLocker.Text = "Read Locker";
            this.btnReadLocker.UseVisualStyleBackColor = true;
            this.btnReadLocker.Click += new System.EventHandler(this.btnReadLocker_Click);
            // 
            // cmdSetCalibration
            // 
            this.cmdSetCalibration.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSetCalibration.Location = new System.Drawing.Point(696, 230);
            this.cmdSetCalibration.Name = "cmdSetCalibration";
            this.cmdSetCalibration.Size = new System.Drawing.Size(133, 40);
            this.cmdSetCalibration.TabIndex = 44;
            this.cmdSetCalibration.Text = "Set calibration";
            this.cmdSetCalibration.UseVisualStyleBackColor = true;
            this.cmdSetCalibration.Click += new System.EventHandler(this.cmdSetCalibration_Click);
            // 
            // FrmTestFcu2Asic
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(850, 515);
            this.Controls.Add(this.cmdSetCalibration);
            this.Controls.Add(this.btnReadLocker);
            this.Controls.Add(this.btnSetTosa);
            this.Controls.Add(this.btnReadDSDBR);
            this.Controls.Add(this.txtFilePath);
            this.Controls.Add(this.cmdBrowe);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "FrmTestFcu2Asic";
            this.Text = "TestFcu2Asic";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TestFcu2Asic_FormClosing);
            this.Load += new System.EventHandler(this.TestFcu2Asic_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtLeftImb;
        private System.Windows.Forms.TextBox txtRearSoa;
        private System.Windows.Forms.TextBox txtFrotSoa;
        private System.Windows.Forms.TextBox txtPhase;
        private System.Windows.Forms.TextBox txtRear;
        private System.Windows.Forms.TextBox txtGain;
        private System.Windows.Forms.TextBox txtNoConstantCurrent;
        private System.Windows.Forms.TextBox txtConstantCurrent;
        private System.Windows.Forms.ListBox lstFsPair;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtReflect;
        private System.Windows.Forms.TextBox txtTransmit;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtRightImb;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button cmdBrowe;
        private System.Windows.Forms.TextBox txtFilePath;
        private System.Windows.Forms.Button btnReadDSDBR;
        private System.Windows.Forms.Button btnSetTosa;
        private System.Windows.Forms.Button btnReadLocker;
        private System.Windows.Forms.Button cmdSetCalibration;

    }
}
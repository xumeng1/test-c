using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.Specialized;
// TODO: You'll probably need these includes once you add your references!
//using Bookham.TestLibrary.ChassisNS;
//using Bookham.TestLibrary.Instruments;
//using Bookham.TestLibrary.InstrTypes;
using Bookham.TestEngine.Framework.Logging;
using Bookham.TestEngine.Framework.InternalData;
using System.Text;
using NUnit.Framework;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestLibrary .Instruments ;
using Bookham.TestLibrary .ChassisNS ;
using Bookham.TestEngine.Config;

namespace TestFcu2Asic
{
    /// <exclude />	
    [TestFixture]
    public class Test_Object
    {
        #region Private data (incl equipment references)
        // TODO - PUT YOUR CHASSIS REFERENCE HERE
        //private Chassis_Ag34970A testChassis;

        // TODO - PUT YOUR INSTRUMENT REFERENCES HERE
        //private Inst_Ag34970A_DMM testInstr;     

        private Chassis_Fcu2Asic testChassis;
        private Inst_Fcu2Asic testInstr;

        #endregion

        /// <exclude />
        [TestFixtureSetUp]
        public void Setup()
        {
            // initialise Test Engine logging domain
            //UnhandledExceptionsHandler.Initialise();
            Bookham.TestEngine.Framework.Logging.Initializer.Init();
            Bookham.TestEngine.Framework.Logging.BugOut.SetOutputToConsole(true);
            TestOutput("*** Test Harness Initialising ***");

            

        }

        /// <exclude />
        [TestFixtureTearDown]
        public void ShutDown()
        {
            // TODO...
            TestOutput("Don't forget to take the chassis offline!");
            //testChassis.IsOnline = false;

            if ( (testInstr != null) && (testInstr.IsOnline == true))
            {
                testInstr.DisableAllOutput();
                testInstr.TecEnable = Inst_Fcu2Asic.OnOff.off;

                testInstr.IsOnline = false;
            }
            if (testChassis != null ) testChassis.IsOnline = false;

            // Test end
            TestOutput("Test Finished!");
        }

        [Test]
        public void SetUpInstrument(string calFile)
        {
            // create chassis objects
            // TODO...
            TestOutput("Don't forget to create chassis objects");
            //testChassis = new Chassis_Ag34970A("MyChassis", "Chassis_Ag34970A", "GPIB0::7::INSTR");

            testChassis = new Chassis_Fcu2Asic("chassis fcu2 asic", "Chassis_Fcu2Asic", "COM,2,38400");
            TestOutput(testChassis, "Created OK");
            // create instrument objects            
            // TODO...
            TestOutput("Don't forget to create instrument objects");
            //testInstr = new Inst_Ag34970A_DMM("DMM_Instr1", "Inst_Ag34970A_DMM", "1", "2", testChassis);
            testInstr = new Inst_Fcu2Asic("instrument FCU 2 ASIC", "Inst_Fcu2Asic", "", "", testChassis);
            TestOutput(testInstr, "Created OK");

            // put them online
            // TODO...
            TestOutput("Don't forget to put equipment objects online");
            testChassis.IsOnline = true;
            //testChassis.EnableLogging = true;
            TestOutput(testChassis, "IsOnline set true OK");


            //testInstr.EnableLogging = true;
            testInstr.IsOnline = true;
            bool flag = testInstr.IsOnline;
            TestOutput(testInstr, "IsOnline set true OK");

            string rspStr = testChassis.Query_Checked("hello",testInstr);
            //System.Threading.Thread.Sleep(10);
            //string rspStr = instrumentChassis.ReadLine(null);
            if (rspStr .EndsWith("ok")) flag = true;
            

            if (calFile.Trim() == "") calFile = Path.GetFullPath(@"E:\Alice\TcMZ_Final_Test_new_OfficePC\TestEngine\Plugins\SrcCommon\Equipment\Inst_Fcu2Asic\InstrsCalibration.xml");
            setupCalibration(calFile);

            testInstr.LaserEnable = Inst_Fcu2Asic.OnOff.off;
        }
        [Test]
        public void currentsource()
        {
            TestOutput("\n\n*** T01_FirstTest ***");
            // You can use Assert to check return values from tests
            Assert.AreEqual(1, 1); // this will succeed
            // Don't forget to use special functions for Double metavalues...
            Assert.IsNaN(Double.NaN);
        }

        [Test]
        public void T02_SecondTest()
        {
            TestOutput("\n\n*** T02_SecondTest***");
            // Give up the test - bad thing happened
            Assert.Fail();
            
        }


        private bool setupCalibration(string filename)
        {
            string fcu2asic_InstrName = "FCU2Asic";
            InstrumentsCalData = new ConfigDataAccessor(filename, "Calibration");
            double CalFactor;
            double CalOffset;

            FCUCalibrationData = new FCUCalData();

            // Tx ADC
            readCalData(fcu2asic_InstrName, "Tx ADC", out CalFactor, out CalOffset);
            FCUCalibrationData.TxADC_CalFactor = CalFactor;
            FCUCalibrationData.TxADC_CalOffset = CalOffset;
            // Rx ADC
            readCalData(fcu2asic_InstrName, "Rx ADC", out CalFactor, out CalOffset);
            FCUCalibrationData.RxADC_CalFactor = CalFactor;
            FCUCalibrationData.RxADC_CalOffset = CalOffset;
            // TxCoarsePot
            readCalData(fcu2asic_InstrName, "TxCoarsePot", out CalFactor, out CalOffset);
            FCUCalibrationData.TxCoarsePot_CalFactor = CalFactor;
            FCUCalibrationData.TxCoarsePot_CalOffset = CalOffset;
            // TxFinePot
            readCalData(fcu2asic_InstrName, "TxFinePot", out CalFactor, out CalOffset);
            FCUCalibrationData.TxFinePot_CalFactor = CalFactor;
            FCUCalibrationData.TxFinePot_CalOffset = CalOffset;
            // ThermistorResistance
            readCalData(fcu2asic_InstrName,"ThermistorResistance", out CalFactor, out CalOffset);
            FCUCalibrationData.ThermistorResistance_CalFactor = CalFactor;
            FCUCalibrationData.ThermistorResistance_CalOffset = CalOffset;

            // Asic Part

            //readCalData(fcu2asic_InstrName,  "FsDac", out CalFactor, out CalOffset);
            //testInstr.SectionsDacCalibration.FsDac_CalFactor = CalFactor;
            //testInstr.SectionsDacCalibration.FsDac_CalOffset = CalOffset;

            //readCalData(fcu2asic_InstrName, "PhaseDac", out CalFactor, out CalOffset);
            //testInstr.SectionsDacCalibration.PhaseDac_CalFactor = CalFactor;
            //testInstr.SectionsDacCalibration.PhaseDac_CalOffset = CalOffset;

            //readCalData(fcu2asic_InstrName,  "RearSoaDac", out CalFactor, out CalOffset);
            //testInstr.SectionsDacCalibration.RearSoaDac_CalFactor = CalFactor;
            //testInstr.SectionsDacCalibration.RearSoaDac_CalOffset = CalOffset;

            //readCalData(fcu2asic_InstrName,"GainDac", out CalFactor, out CalOffset);
            //testInstr.SectionsDacCalibration.GainDac_CalFactor = CalFactor;
            //testInstr.SectionsDacCalibration.GainDac_CalOffset = CalOffset;

            //readCalData(fcu2asic_InstrName,"FrontSoaDac_Pos", out CalFactor, out CalOffset);
            //testInstr.SectionsDacCalibration.FrontSoaDac_Pos_CalFactor = CalFactor;
            //testInstr.SectionsDacCalibration.FrontSoaDac_Pos_CalOffset = CalOffset;

            //readCalData(fcu2asic_InstrName, "FrontSoaDac_Neg", out CalFactor, out CalOffset);
            //testInstr.SectionsDacCalibration.FrontSoaDac_Neg_CalFactor = CalFactor;
            //testInstr.SectionsDacCalibration.FrontSoaDac_Neg_CalOffset = CalOffset;

            //readCalData(fcu2asic_InstrName, "RearDac", out CalFactor, out CalOffset);
            //testInstr.SectionsDacCalibration.RearDac_CalFactor = CalFactor;
            //testInstr.SectionsDacCalibration.RearDac_CalOffset = CalOffset;

            //readCalData(fcu2asic_InstrName,"ImbLeftDac", out CalFactor, out CalOffset);
            //testInstr.SectionsDacCalibration.ImbLeftDac_CalFactor = CalFactor;
            //testInstr.SectionsDacCalibration.ImbLeftDac_CalOffset = CalOffset;

            //readCalData(fcu2asic_InstrName,  "ImbRightDac", out CalFactor, out CalOffset);
            //testInstr.SectionsDacCalibration.ImbRightDac_CalFactor = CalFactor;
            //testInstr.SectionsDacCalibration.ImbRightDac_CalOffset = CalOffset;

            testInstr.SetupChannelCalibration();

            return true;
        }
        private void readCalData(string InstrumentName, string ParameterName, out double CalFactor, out double CalOffset)
        {
            StringDictionary keys = new StringDictionary();
            keys.Add("Instrument_name", InstrumentName);
            keys.Add("Parameter", ParameterName);
            
            DatumList calParams = InstrumentsCalData.GetData(keys, false);

            CalFactor = calParams.ReadDouble("CalFactor");
            CalOffset = calParams.ReadDouble("CalOffset");
        }
        
        public StrcTosaSetup TosaSetup
        {
            set 
            {
                testInstr.IRear_mA = 0;
                testInstr.IPhase_mA = 0;

                testInstr.IGain_mA =(float ) value.gain;
                testInstr.SetFrontSectionPairCurrent(value.frontPair,
                    value.constantCurrent, value.UnconstantCurrent);
                testInstr.IRear_mA = (float)value.rear;
                testInstr.IPhase_mA = (float)value.phase;
                testInstr.ISoa_mA = (float)value.frontSoa;

                testInstr.IRearSoa_mA = (float)value.rearSoa;
                testInstr.IimbLeft_mA = (float)value.leftImb;
                testInstr.IimbRight_mA = (float)value.rightImb;
            }
            get 
            {
                StrcTosaSetup datatemp = new StrcTosaSetup();

                datatemp.frontPair = testInstr.GetFrontPairNumber();
                if (datatemp.frontPair % 2 != 0)
                {
                    datatemp.constantCurrent = testInstr.IEven_mA;
                    datatemp.UnconstantCurrent = testInstr.IOdd_mA;
                }
                else
                {
                    datatemp.UnconstantCurrent = testInstr.IEven_mA;
                    datatemp.constantCurrent = testInstr.IOdd_mA;
                }
                datatemp.rear = testInstr.IRear_mA;
                datatemp.phase = testInstr.IPhase_mA;
                datatemp.frontSoa = testInstr.ISoa_mA;
                datatemp.rearSoa = testInstr.IRearSoa_mA;
                datatemp.leftImb = testInstr.IimbLeft_mA;
                datatemp.rightImb = testInstr.IimbRight_mA;
                datatemp.gain = testInstr.IGain_mA;
                return datatemp;
            }
        }

        [Test]
        public StrcLockerCurrent GetLockerCurrents()
        {
            int transmitReading;
            int reflectReading;

            transmitReading = testInstr.ILockerTran_Dac;
            reflectReading = testInstr.ILockerReflect_Dac;

            StrcLockerCurrent locker = new StrcLockerCurrent();

            locker.lockReflect = (reflectReading - FCUCalibrationData.RxADC_CalOffset) / FCUCalibrationData.RxADC_CalFactor;
            locker.lockTransmit = (transmitReading - FCUCalibrationData.TxADC_CalOffset) / FCUCalibrationData.TxADC_CalFactor;

            return locker;
        }
        #region Private helper fns
        private void TestOutput(string output)
        {
            BugOut.WriteLine(BugOut.WildcardLabel, output);
        }

        private void TestOutput(IInstrument i, string output)
        {
            string outputStr = String.Format("{0}: {1}", i.Name, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }

        private void TestOutput(IChassis c, string output)
        {
            string outputStr = String.Format("{0}: {1}", c.Name, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }
        #endregion

        private ConfigDataAccessor InstrumentsCalData;
        private FCUCalData FCUCalibrationData;
        //public StrcTosaSetup Tosadata;
        //public StrcLockerCurrent locker;

        public struct StrcTosaSetup
        {
            public int frontPair;
            public double constantCurrent;
            public double UnconstantCurrent;
            public double phase;
            public double gain;
            public double rear;
            public double frontSoa;
            public double rearSoa;
            public double leftImb;
            public double rightImb;
        }
        public struct StrcLockerCurrent
        {
            public double lockTransmit;
            public double lockReflect;
        }        

        /// <summary> Structure for FCU calibration data </summary>
        public struct FCUCalData
        {
            /// <summary> Cal offset for TxADC </summary>
            public double TxADC_CalOffset;
            /// <summary> Cal factor for TxADC </summary>
            public double TxADC_CalFactor;

            /// <summary> Cal offset for RxADC </summary>
            public double RxADC_CalOffset;
            /// <summary> Cal factor for RxADC </summary>
            public double RxADC_CalFactor;

            /// <summary> Cal offset for TxCoarsePot </summary>
            public double TxCoarsePot_CalOffset;
            /// <summary> Cal factor for TxCoarsePot </summary>
            public double TxCoarsePot_CalFactor;

            /// <summary> Cal offset for TxFinePot </summary>
            public double TxFinePot_CalOffset;
            /// <summary> Cal factor for TxFinePot </summary>
            public double TxFinePot_CalFactor;

            /// <summary> Cal offset for ThermistorResistance </summary>
            public double ThermistorResistance_CalOffset;
            /// <summary> Cal factor for ThermistorResistance </summary>
            public double ThermistorResistance_CalFactor;
        }
    }

}

using System;
using System.Collections.Generic;
// TODO: You'll probably need these includes once you add your references!
//using Bookham.TestLibrary.ChassisNS;
//using Bookham.TestLibrary.Instruments;
//using Bookham.TestLibrary.InstrTypes;
using Bookham.TestEngine.Framework.Logging;
using System.Text;
using NUnit.Framework;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisNS;
using Bookham.TestLibrary.Instruments;
using Bookham.TestEngine.PluginInterfaces.Instrument;

namespace TEST.SantecPEM320
{
    /// <exclude />	
    [TestFixture]
    public class Test_Object
    {
        #region References to the objects to test - Chassis and Instruments
        // TODO - PUT YOUR CHASSIS REFERENCE HERE
        private Chassis_SantecPEM320 testChassis;

        // TODO - PUT YOUR INSTRUMENT REFERENCES HERE
        private Inst_SantecPEM320 testInstr;        
        #endregion


        #region Constants for use during test.
        // VISA Chassis for where to find the instrument
        const string visaResource = "GPIB0::20::INSTR"; //e.g. "//pai-tx-labj1/GPIB0::9::INSTR";
        const string chassisName = "Chassis";
        const string inst1Name = "Instrument";
        #endregion

        /// <exclude />
        [TestFixtureSetUp]
        public void Setup()
        {
            // initialise Test Engine logging domain
            Bookham.TestEngine.Framework.Logging.Initializer.Init();
            Bookham.TestEngine.Framework.Logging.BugOut.SetOutputToConsole(true);
            TestOutput("*** Test Harness Initialising ***");

            // create chassis objects
            TestOutput("Don't forget to create chassis objects");
            testChassis = new Chassis_SantecPEM320(chassisName, "Chassis_SantecPEM320", visaResource);
            TestOutput(chassisName, "Created OK");

            // create instrument objects            
            TestOutput("Don't forget to create instrument objects");
            testInstr = new Inst_SantecPEM320(inst1Name, "Inst_SantecPEM320", "", "", testChassis);
            TestOutput(testInstr, "Created OK");

            // put them online
            TestOutput("Don't forget to put equipment objects online");
            testChassis.IsOnline = true;
            testChassis.EnableLogging = true;
            TestOutput(chassisName, "IsOnline set true OK");
            testInstr.IsOnline = true;
            testInstr.EnableLogging = true;
            TestOutput(testInstr, "IsOnline set true OK");            
        }

        /// <exclude />
        [TestFixtureTearDown]
        public void ShutDown()
        {
            TestOutput("Don't forget to take the chassis offline!");
            testChassis.IsOnline = false;

            // Test end
            TestOutput("Test Finished!");
        }

        [Test]
        public void T01_Versions()
        {
            TestOutput("\n\n*** T01_Versions ***");
            TestOutput(testChassis, "Driver: " + testChassis.DriverName + ":" + testChassis.DriverVersion);
            TestOutput(testChassis, "HW: " + testChassis.HardwareIdentity);
            TestOutput(testChassis, "FW: " + testChassis.FirmwareVersion);
            TestOutput(testInstr, "Driver: " + testInstr.DriverName + ":" + testInstr.DriverVersion);
            TestOutput(testInstr, "HW: " + testInstr.HardwareIdentity);
            TestOutput(testInstr, "FW: " + testInstr.FirmwareVersion);
        }

        [Test]
        public void T02_DoThings()
        {
            testInstr.SetDefaultState();
            TestOutput(testInstr, "Power dBm: " + testInstr.Power_dBm);
            TestOutput(testInstr, "Power mW: " + testInstr.Power_mW);
            TestOutput(testInstr, "Power peak dBm: " + testInstr.Power_peak_dBm);
            TestOutput(testInstr, "Power min dBm: " + testInstr.Power_minimum_dBm);
            TestOutput(testInstr, "PER dB: " + testInstr.PER_dB);
            TestOutput(testInstr, "Speed: " + testInstr.Speed);
            testInstr.Speed = Inst_SantecPEM320.MeasurementSpeed.Slow;
            Assert.AreEqual(Inst_SantecPEM320.MeasurementSpeed.Slow, testInstr.Speed);
            testInstr.Speed = Inst_SantecPEM320.MeasurementSpeed.Medium;
            Assert.AreEqual(Inst_SantecPEM320.MeasurementSpeed.Medium, testInstr.Speed);
            testInstr.Speed = Inst_SantecPEM320.MeasurementSpeed.Fast;
            Assert.AreEqual(Inst_SantecPEM320.MeasurementSpeed.Fast, testInstr.Speed);
            
            TestOutput(testInstr, "Angle deg: " + testInstr.Angle_degrees);
            TestOutput(testInstr, "Averaging on: " + testInstr.AveragingOn);
            testInstr.AveragingOn = false;
            Assert.AreEqual(false, testInstr.AveragingOn);
            testInstr.AveragingOn = true;
            Assert.AreEqual(true, testInstr.AveragingOn);
        }


        #region Private helper fns
        private void TestOutput(string output)
        {
            BugOut.WriteLine(BugOut.WildcardLabel, output);
        }

        private void TestOutput(string objStr, string output)
        {
            string outputStr = String.Format("{0}: {1}", objStr, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }

        private void TestOutput(Instrument i, string output)
        {
            string outputStr = String.Format("{0}: {1}", i.Name, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }

        private void TestOutput(Chassis c, string output)
        {
            string outputStr = String.Format("{0}: {1}", c.Name, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }
        #endregion


    }

}

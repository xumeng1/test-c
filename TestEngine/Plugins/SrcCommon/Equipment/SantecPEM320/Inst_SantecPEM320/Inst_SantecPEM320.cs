// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_SantecPEM320.cs
//
// Author: markfull, 2006
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisTypes;
using Bookham.TestLibrary.ChassisNS;

namespace Bookham.TestLibrary.Instruments
{    
    /// <summary>
    /// Santec Polarisation Extinction Ratio Meter PEM320 Instrument
    /// </summary>
    public class Inst_SantecPEM320 : InstType_PolarisationERMeter
    {
        /// <summary>
        /// Speed can be either 2.5, 5 or 10Hz
        /// </summary>
        public enum MeasurementSpeed
        {
            /// <summary>
            /// 2.5 Hz
            /// </summary>
            Slow = 0,
            /// <summary>
            /// 5 Hz
            /// </summary>
            Medium = 1,
            /// <summary>
            /// 10 Hz
            /// </summary>
            Fast = 2
        }

        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_SantecPEM320(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord instrVariant1 = new InstrumentDataRecord(
                "PEM-320",				// hardware name 
                "0",  			// minimum valid firmware version 
                "99999");			// maximum valid firmware version 
            ValidHardwareData.Add("PEM-320", instrVariant1);

            // Configure valid chassis driver information
            // TODO: Update with useful key name, real chassis name and versions
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_SantecPEM320",					// chassis driver name  
                "0",									// minimum valid chassis driver version  
                "9999");								// maximum valid chassis driver version
            ValidChassisDrivers.Add("Chassis_SantecPEM320", chassisInfo);

            // initialise this instrument's chassis
            // Cast to the appropriate type.
            // don't forget to change the private variable type too.
            this.instrumentChassis = (Chassis_SantecPEM320)chassisInit;
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // Not implemented
                return "0";
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // Not implemented.
                return "PEM-320";
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            this.instrumentChassis.Write_Unchecked("RE", this);   // Initiate hardware reset.
            this.instrumentChassis.Write_Unchecked("OC", this);   // Clear any offsets.
            this.instrumentChassis.Write_Unchecked("MO", this);   // Start polariser rotating.
            this.Speed = MeasurementSpeed.Medium;
        }
      
        #endregion

        /// <summary>
        /// Reads the Polarisation angle
        /// </summary>
        public override double Angle_degrees
        {
            get
            {
                return Convert.ToDouble( this.instrumentChassis.Query_Unchecked("AG",this));
            }
        }

        /// <summary>
        /// Reads the Polarisation Extinction Ratio
        /// </summary>
        public override double PER_dB
        {
            get
            {
                double power_dBm = this.Power_dBm;
                if (power_dBm > +10.0)
                {
                    string errMsg = String.Format("Power too high for PER measurement: {0} dBm",
                        + power_dBm);
                    this.LogErrorRaise(errMsg);
                }

                string resp = this.instrumentChassis.Query_Unchecked("PE", this);
                double val = Convert.ToDouble(resp);
                return val;
            }
        }

        /// <summary>
        /// Reads power in dBm
        /// </summary>
        public double Power_dBm
        {
            get
            {
                return Convert.ToDouble(this.instrumentChassis.Query_Unchecked("DP", this));
            }
        }

        /// <summary>
        /// Reads power in mW
        /// </summary>
        public double Power_mW
        {
            get
            {
                return Convert.ToDouble(this.instrumentChassis.Query_Unchecked("WP", this));
            }
        }

        /// <summary>
        /// Sets the speed of rotation
        /// </summary>
        public MeasurementSpeed Speed
        {
            get
            {
                // read status
                string resp = this.instrumentChassis.Query_Unchecked("SU", this);
                resp = resp.Trim();
                char speedChar = resp[4];
                if (speedChar == '0') return MeasurementSpeed.Slow;
                if (speedChar == '1') return MeasurementSpeed.Medium;
                if (speedChar == '2') return MeasurementSpeed.Fast;
                // if got here, who knows what's going on!
                this.LogErrorRaise("Bad Status response: " + resp);
                // keep compiler happy
                return 0;
            }

            set
            {
                switch (value)
                {
                    case MeasurementSpeed.Slow:
                        {
                            this.instrumentChassis.Write_Unchecked("RF0", this);
                            break;
                        }
                    case MeasurementSpeed.Medium:
                        {
                            this.instrumentChassis.Write_Unchecked("RF1", this);
                            break;
                        }
                    case MeasurementSpeed.Fast:
                        {
                            this.instrumentChassis.Write_Unchecked("RF2", this);
                            break;
                        }
                }
            }
        }
        
        /// <summary>
        /// Enables or disables averaging.
        /// </summary>
        public override bool AveragingOn
        {
            get
            {
                return this.averagingOn;
            }
            set
            {
                if (value)
                {
                    this.instrumentChassis.Write_Unchecked("AO", this);
                }
                else
                {
                    this.instrumentChassis.Write_Unchecked("AF", this);
                }
                this.averagingOn = value;
            }
        }

        private bool averagingOn;

        /// <summary>
        /// Reads the minimum measured power
        /// </summary>
        public override double Power_minimum_dBm
        {
            get { return Convert.ToDouble( this.instrumentChassis.Query_Unchecked("BP", this)); }
        }

        /// <summary>
        /// Reads the maximum measured power
        /// </summary>
        public override double Power_peak_dBm
        {
            get { return Convert.ToDouble(this.instrumentChassis.Query_Unchecked("TP", this)); }
        }

        #region Private Data

        // Chassis reference
        private Chassis_SantecPEM320 instrumentChassis;        

        #endregion
    }
}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Chassis
//
// Chassis_SantecPEM320.cs
//
// Author: markfull, 2006
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;

namespace Bookham.TestLibrary.ChassisNS
{
    /// <summary>
    /// Santec Polarisation Extinction Meter PEM320 Chassis
    /// </summary>
    public class Chassis_SantecPEM320 : ChassisType_Visa488_2
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="chassisNameInit">Chassis name</param>
        /// <param name="driverNameInit">Chassis driver name</param>
        /// <param name="resourceString">Resource string for communicating with the chassis (for VISA
        /// chassis, this is the VISA resource string)</param>
        public Chassis_SantecPEM320(string chassisNameInit, string driverNameInit,
            string resourceString)
            : base(chassisNameInit, driverNameInit, resourceString)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            ChassisDataRecord chassisData = new ChassisDataRecord(
                "PEM-320",			// hardware name 
                "0",			// minimum valid firmware version 
                "9e99");		// maximum valid firmware version 
            ValidHardwareData.Add("ChassisData", chassisData);

            // use *STB? for status byte
            this.ErrorCheckUse_488_2_Command = false;
            
        }
        #endregion

        #region Chassis overrides
        /// <summary>
        /// Firmware version of this chassis.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // Not implemented.
                return "0";
            }
        }

        /// <summary>
        /// Hardware Identity of this chassis.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // Read the chassis ID string
                string idn = Query_Unchecked("*IDN?", null);
                if (idn.ToUpper().StartsWith("NR"))
                {   // Any command that the instrument cannot interpret returns "NR"
                    // a response other than this probably indicates a GPIB address error.
                    return "PEM-320";
                }
                else
                {
                    return idn;
                }
            }
        }

        /// <summary>
        /// Setup the chassis as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                if (value) // if setting online                
                {
                    // Add custom setup for this chassis before it goes online, if required
                }

                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // Custom setup for this chassis after it goes online, 
                    // e.g. Standard Error register on a 488.2 instrument
                    // 4 = Query error, 8 = Device error, 16 = execution error, 32 = command error
                    //this.StandardEventRegisterMask = 4 + 8 + 16 + 32;
                    // clear the status registers
                    //this.Write("*CLS", null);          
                    string resp = this.Query_Unchecked("SU", null);
                    // add longer timeout!
                    this.Timeout_ms = 8000;
                }
            }
        }
        #endregion
    }
}

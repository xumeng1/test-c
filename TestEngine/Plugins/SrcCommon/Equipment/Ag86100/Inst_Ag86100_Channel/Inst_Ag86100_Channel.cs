// [Copyright]
//
// Bookham Test Library
// Bookham.TestLibrary.Instruments
//
// Inst_Ag86100_DCA.cs
//
// Author: paul.annetts, 2006
// Design: Agilent 86100 DCA Instrument Driver Detailed Design

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisNS.Ag86100;

namespace Bookham.TestLibrary.Instruments
{    
    /// <summary>
    /// Agilent Infiniium DCA instrument class
    /// </summary>
    public class Inst_Ag86100_Channel : Instrument
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Channel number of the instrument</param>
        /// <param name="subSlotInit">Not used in Ag86100</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_Ag86100_Channel(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            InstrumentDataRecord instr86100A = new InstrumentDataRecord(
                "Agilent Technologies 86100A",			// hardware name 
                "A.02.00",			// minimum valid firmware version 
                "A.05.00");		// maximum valid firmware version 
            ValidHardwareData.Add("86100A", instr86100A);

            InstrumentDataRecord instr86100B = new InstrumentDataRecord(
             "Agilent Technologies 86100B",			// hardware name 
             "A.02.00",			// minimum valid firmware version 
             "A.05.00");		// maximum valid firmware version 
            ValidHardwareData.Add("86100B", instr86100B);

            // Configure valid chassis driver information
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_Ag86100",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "2.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("MyChassis", chassisInfo);

            // initialise this instrument's chassis
            this.instrumentChassis = (Chassis_Ag86100)chassisInit;
            // what channel is this?
            this.channel = int.Parse(slotInit);
            this.channelStr = "CHANNEL" + slotInit;
            switch (this.channel)
            {
                case 1: case 2:
                    this.moduleLocationStr = "LMOD";
                    break;
                case 3: case 4:
                    this.moduleLocationStr = "RMOD";
                    break;
                default:
                    throw new Ag86100Exception("Invalid DCA channel: must be between 1 and 4");
            }
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                return instrumentChassis.FirmwareVersion;
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                return instrumentChassis.HardwareIdentity;
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            instrumentChassis.Write(channelStr + ":DISPLAY ON", this);
        }       
        #endregion

        #region Private data
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_Ag86100 instrumentChassis;        

        /// <summary>
        /// Is this channel in left or right module?
        /// Module location shorthand string
        /// </summary>
        private string moduleLocationStr;

        /// <summary>
        /// Scope channel
        /// </summary>
        private int channel; 

        /// <summary>
        /// Shorthand channel string
        /// </summary>
        private string channelStr;
        #endregion

        /// <summary>
        /// Is a module calibration required?
        /// </summary>
        public bool CalModuleRequired
        {
            get
            {
                string resp = instrumentChassis.Query(":CAL:MODULE:STATUS? " + moduleLocationStr, null);
                string modCalStr = resp.Split(',')[0];
                if (modCalStr == "CALIBRATED") return false;
                else if (modCalStr == "UNCALIBRATED") return true;
                else throw new Ag86100Exception("Unknown response from cal command");                
            }
        }        

        /// <summary>
        /// Calibrate Module - needs to be done after module plugin or mainframe
        /// start-up. All inputs must be disconnected / turned off for this 
        /// to work properly.
        /// </summary>
        public void CalibrateModule()
        {            
            string command = ":CAL:MODULE:VERTICAL " + moduleLocationStr;
            try
            {
                instrumentChassis.Write_Unchecked(command, this);
                // wait for completion
                instrumentChassis.WaitForCalComplete(120);
            }
            catch (ChassisException e)
            {
                throw new Ag86100OperationFailed("Failed to calibrate module", e);
            }
        }

        /// <summary>
        /// Calibrate Extinction Ratio - needs a well set-up eye to work 
        /// (i.e. must be auto-scaled with 1 and 0 level visible)
        /// </summary>
        public void CalibrateER()
        {
            string command = ":CAL:ERATIO:START " + channelStr;
            try
            {
                instrumentChassis.Write_Unchecked(command, this);
                // wait for completion
                instrumentChassis.WaitForCalComplete(5);
            }
            catch (ChassisException e)
            {
                throw new Ag86100OperationFailed("Failed to calibrate Extinction Ratio", e);
            }
        }

        /// <summary>
        /// Available filters for this channel
        /// </summary>
        /// <returns></returns>
        public FilterDescription[] AvailFilters
        {
            get
            {
                string resp = instrumentChassis.Query(":" + channelStr + ":FDES?", this);
                string[] splitStr = resp.Split(',');
                if (splitStr[0] == "0") return new FilterDescription[0];
                int nbrFilters = splitStr.Length - 1;
                FilterDescription[] filters = new FilterDescription[nbrFilters];
                for (int ii=0; ii<nbrFilters; ii++)
                {
                    string filterDesStr = splitStr[ii + 1];
                    int pos1stSpace = filterDesStr.IndexOf(' ');
                    string gpbsStr = filterDesStr.Substring(0, pos1stSpace);
                    double gpbs = double.Parse(gpbsStr);
                    filters[ii].DesignRate_Mbps = gpbs * 1000;

                    int posColon = filterDesStr.IndexOf(':');
                    if (posColon > 1)
                    {
                        string orderStr = filterDesStr.Substring(posColon + 2, 1);
                        int order = int.Parse(orderStr);
                        filters[ii].FilterOrder = order;
                    }
                    else filters[ii].FilterOrder = 0;
                }
                return filters;
            }
        }

        /// <summary>
        /// Which is the currently selected filter (may be on or off - check with FilterOn property!)
        /// </summary>
        public FilterDescription CurrentFilter
        {
            get
            {
                string resp = instrumentChassis.Query(":" + channelStr + ":FSELECT?", this);
                string filterNbrStr = resp.Substring(4);
                int filterNumber = int.Parse(filterNbrStr);
                if (filterNumber == 0) return new FilterDescription();
                else
                {
                    FilterDescription[] filters = AvailFilters;
                    return filters[filterNumber - 1];
                }                
            }
            set
            {
                bool found = false;
                int filterCounter = 1;
                foreach (FilterDescription aFilter in AvailFilters)
                {
                    if ((Math.Round(value.DesignRate_Mbps, 3) ==
                          Math.Round(aFilter.DesignRate_Mbps, 3)) ) //&&
                     //   (value.FilterOrder == aFilter.FilterOrder))  Ignore FilterOrder as not supported on later modules
                    {
                        found = true;
                        break;
                    }
                    else filterCounter++;
                }
                if (found)
                {
                    string cmd = String.Format(":{0}:FSELECT FILT{1}", channelStr, filterCounter);
                    instrumentChassis.Write(cmd, this);
                }
                else throw new Ag86100Exception("Filter not available: " + value);
            }
        }

        /// <summary>
        /// Gets/Sets the filter on state for this DCA channel
        /// </summary>
        public bool FilterOn
        {
            get
            {
                string resp = instrumentChassis.Query(":" + channelStr + ":FILTER?", this);
                if (resp == "1") return true;
                else if (resp == "0") return false;
                else throw new Ag86100Exception("Invalid response to command: " + resp);
            }
            set
            {
                string newState;
                if (value) newState = "ON";
                else newState = "OFF";
                string cmd = ":" + channelStr + ":FILTER " + newState;
                instrumentChassis.Write(cmd, this);
            }
        }

        /// <summary>
        /// Get/set current optical calibration of DCA channel
        /// </summary>
        public DcaWavelength OpticalWavelength
        {
            get
            {
                string resp = instrumentChassis.Query(":" + channelStr + ":WAV?", this);
                string wavStr = resp.Substring(0, resp.IndexOf(','));
                if (wavStr == "WAV1") return DcaWavelength.W1310nm;
                else if (wavStr == "WAV2") return DcaWavelength.W1550nm;
                else if (wavStr == "WAV3") return DcaWavelength.W850nm;
                else if (wavStr == "USER") return DcaWavelength.User;
                else throw new Ag86100Exception("Invalid response: " + resp);
            }
            set
            {
                if (value == OpticalWavelength) return;

                string cmd = ":" + channelStr + ":WAV ";
                switch (value)
                {
                    case DcaWavelength.W1310nm:
                        cmd += "WAV1"; break;
                    case DcaWavelength.W1550nm:
                        cmd += "WAV2"; break;
                    case DcaWavelength.W850nm:
                        cmd += "WAV3"; break;
                    case DcaWavelength.User:
                        cmd += "USER"; break;
                    default:
                        throw new Ag86100Exception("Invalid optical wavelength: " + value);
                }
                instrumentChassis.Write(cmd, this);
            }
        }
        
        /// <summary>
        /// Set or Get the vertical scale setting
        /// </summary>
        public double VerticalScale
        {
            get
            {
                string resp = instrumentChassis.Query(":" + channelStr + ":SCALE?", this);
                double outVal = -1;

                if (!double.TryParse(resp, out outVal))
                {
                    throw new Ag86100Exception("Invalid SCALE response: " + resp);
                }
                return outVal;
            }
            set
            {
                string cmd = String.Format(":{0}:SCALE {1}", channelStr, value);
                instrumentChassis.Write(cmd,this);
            }
        }

        /// <summary>
        /// Set or Get the vertical offset setting
        /// </summary>
        public double VerticalOffset
        {
            get
            {
                string resp = instrumentChassis.Query(":" + channelStr + ":OFFSET?", this);
                double outVal = -1;

                if (!double.TryParse(resp, out outVal))
                {
                    throw new Ag86100Exception("Invalid OFFSET response: " + resp);
                }
                return outVal;
            }
            set
            {
                string cmd = String.Format(":{0}:OFFSET {1}", channelStr, value);
                instrumentChassis.Write(cmd, this);
            }
        }

        /// <summary>
        /// Set or Get the vertical Mask alignment setting
        /// </summary>
        public DcaMaskYAlign VerticalMaskAlign
        {
            get
            {
                string resp = instrumentChassis.Query("MTES:YAL?", this);
                string alignStr = resp.Substring(0, resp.IndexOf(','));
                if (alignStr == "DISP") return DcaMaskYAlign.Display;
                else if (alignStr == "EWIN") return DcaMaskYAlign.EyeWindow;
                else throw new Ag86100Exception("Invalid response: " + resp);
            }
            set
            {
                string cmd = "Error";

                if(value == DcaMaskYAlign.Display)
                    cmd = ":MTES:YAL DISP";
                else if (value == DcaMaskYAlign.EyeWindow)
                    cmd = ":MTES:YAL EWIN";
                else throw new Ag86100Exception("Invalid cmd: " + cmd);

                instrumentChassis.Write(cmd, this);
            }
        }
    }
}

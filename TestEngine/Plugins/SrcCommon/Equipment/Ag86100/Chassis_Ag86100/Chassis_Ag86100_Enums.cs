using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.ChassisNS.Ag86100
{
    
    /// <summary>
    /// DCA operation mode
    /// </summary>
    public enum DcaMode
    {
        /// <summary>Eye/Mask mode</summary>
        EyeMask,
        /// <summary>Oscilloscope</summary>
        Oscilloscope
    }

    /// <summary>
    /// Which trigger source to use
    /// </summary>
    public enum DcaTriggerSrc
    {
        /// <summary>Free-running</summary>
        FreeRun,
        /// <summary>Front Panel</summary>
        FrontPanel,
        /// <summary>Left Module</summary>
        LeftModule,
        /// <summary>Right Module</summary>
        RightModule
    }

    /// <summary>
    /// Describes a filter for a DCA channel
    /// </summary>
    public struct FilterDescription
    {
        /// <summary>
        /// Design Rate of the filter Mbps
        /// </summary>
        public double DesignRate_Mbps;
        /// <summary>
        /// Order of the filter
        /// </summary>
        public int FilterOrder;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="designRate_Mbps">Design rate</param>
        /// <param name="order">Filter order</param>
        public FilterDescription(double designRate_Mbps, int order)
        {
            DesignRate_Mbps = designRate_Mbps;
            FilterOrder = order;
        }

        /// <summary>
        /// Convert to string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string str = String.Format("DesignRate={0}Mbps, Order={1}",
                DesignRate_Mbps, FilterOrder);
            return str;
        }
    }

    /// <summary>
    /// DCA standard optical calibration wavelengths 
    /// </summary>
    public enum DcaWavelength
    {
        /// <summary>Wavelength 1 (1310 nm)</summary>
        W1310nm = 1,
        /// <summary>Wavelength 2 (1550 nm)</summary>
        W1550nm = 2,
        /// <summary>Wavelength 3 (850 nm) - not on all optical plugins</summary>
        W850nm = 3,
        /// <summary>User-Calibrated Wavelength - has to be performed first!</summary>
        User = 4
    }

    /// <summary>
    /// Measurement thresholds for rise-fall time measurements
    /// </summary>
    public struct DcaMeasThresholds
    {
        /// <summary>
        /// Low threshold (percent)
        /// </summary>
        public double Low_Pc;
        /// <summary>
        /// Medium threshold (percent)
        /// </summary>
        public double Mid_Pc;
        /// <summary>
        /// High threshold (percent)
        /// </summary>
        public double High_Pc;
        
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="lowPc">Low threshold (percent)</param>
        /// <param name="midPc">Medium threshold (percent)</param>
        /// <param name="highPc">High threshold (percent)</param>
        public DcaMeasThresholds(double lowPc, double midPc, double highPc)
        {
            Low_Pc = lowPc;
            Mid_Pc = midPc;
            High_Pc = highPc;
        }

        /// <summary>
        ///  Convert to a string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string str = string.Format("Low={0}%, Mid={1}%, High={2}%",
                Low_Pc, Mid_Pc, High_Pc);
            return str;
        }
    }

    /// <summary>
    /// Image type for image download
    /// </summary>
    public enum DcaImageType
    {
        ///<summary>BMP image - uncompressed bitmap</summary>
        Bmp,
        ///<summary>TIF image - lossless compression</summary>
        Tif,
        ///<summary>JPG image - lossy compression</summary>
        Jpg
    }

    /// <summary>
    /// What image to download (full screen or scope data only)
    /// </summary>
    public enum DcaImageDownloadType
    {
        ///<summary>Scope only</summary>
        ScopeOnly,
        ///<summary>Full Screen</summary>
        FullScreen
    }

    /// <summary>
    /// Invert the image or in monochrome
    /// </summary>
    public enum DcaImageInversion
    {
        ///<summary>No inversion</summary>
        Normal,
        ///<summary>Inverted colours</summary>
        Invert,
        ///<summary>Monochrome</summary>
        Mono,
    }

    /// <summary>
    /// How is the frequency of pixel hits shown in Eye mode
    /// </summary>
    public enum DcaEyeColourMode
    {
        ///<summary>Colour grading</summary>
        ColourGrade,
        ///<summary>Greyscale (actually more like "yellow"scale!)</summary>
        Greyscale,        
    }

    /// <summary>
    /// How we align the vertical axis of the Mask
    /// </summary>
    public enum DcaMaskYAlign
    {
        ///<summary>Align to Display</summary>
        Display,
        ///<summary>Align to the Eye Diagram</summary>
        EyeWindow
    }
}

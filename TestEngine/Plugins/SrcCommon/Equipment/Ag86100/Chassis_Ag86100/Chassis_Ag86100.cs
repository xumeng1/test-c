// [Copyright]
//
// Bookham Test Library
// Bookham.TestLibrary.Chassis
//
// Chassis_Ag86100.cs
//
// Author: paul.annetts, 2006
// Design: Agilent 86100 DCA Instrument Driver Detailed Design

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;
using Bookham.TestEngine.PluginInterfaces.Instrument;

namespace Bookham.TestLibrary.ChassisNS.Ag86100
{
    /// <summary>
    /// Agilent Ag86100 chassis object
    /// </summary>
    public class Chassis_Ag86100 : ChassisType_Visa488_2
    {
        private const string BadMaskValue = "9.99999E+37";

        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="chassisNameInit">Chassis name</param>
        /// <param name="driverNameInit">Chassis driver name</param>
        /// <param name="resourceString">Resource string for communicating with the chassis (for VISA
        /// chassis, this is the VISA resource string)</param>
        public Chassis_Ag86100(string chassisNameInit, string driverNameInit,
            string resourceString)
            : base(chassisNameInit, driverNameInit, resourceString)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            ChassisDataRecord chassis86100A = new ChassisDataRecord(
            "Agilent Technologies 86100A",			// hardware name 
            "A.02.00",			// minimum valid firmware version 
            "A.05.00");		// maximum valid firmware version 
            ValidHardwareData.Add("86100A", chassis86100A);

            ChassisDataRecord chassis86100B = new ChassisDataRecord(
            "Agilent Technologies 86100B",			// hardware name 
            "A.02.00",			// minimum valid firmware version 
            "A.05.00");		// maximum valid firmware version 
            ValidHardwareData.Add("86100B", chassis86100B);
        }
        #endregion

        #region Chassis overrides
        /// <summary>
        /// Firmware version of this chassis.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                string idn = this.Idn;
                string[] idnSubStrings = idn.Split(',');
                return idnSubStrings[3];
            }
        }

        /// <summary>
        /// Hardware Identity of this chassis.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                string idn = this.Idn;
                string[] idnSubStrings = idn.Split(',');
                return idnSubStrings[0] + " " + idnSubStrings[1];
            }
        }

        /// <summary>
        /// Setup the chassis as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {                
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // 4 = Query error, 8 = Device error, 16 = execution error, 32 = command error
                    this.StandardEventRegisterMask = 4 + 8 + 16 + 32;
                    // clear the status registers
                    this.Write_Unchecked("*RST", null);
                    System.Threading.Thread.Sleep(3000);
                    this.Write("*CLS", null);
                    System.Threading.Thread.Sleep(1000);
                    this.SetDefaultState();
                }
            }
        }
        #endregion

        /// <summary>
        /// Resets the DCA - no visible traces
        /// </summary>
        public void SetDefaultState()
        {
            this.Write("*RST", null);
            this.Write("SYSTEM:HEADER OFF", null);
            this.Write(":MEAS:SENDVALID ON", null);
            this.Write(":DISPLAY:SSAVER DISABLED", null);
            this.Write("CHANNEL1:DISPLAY OFF", null);            
        }

        /// <summary>
        /// Clear Display
        /// </summary>
        public void ClearDisplay()
        {
            this.Write(":CDISPLAY", null);            
        }

        /// <summary>
        /// Clear measurements
        /// </summary>
        public void ClearMeasurements()
        {
            this.Write(":MEASURE:SCRATCH", null);            
        }

        /// <summary>
        /// Clear the user labels
        /// </summary>
        public void ClearLabels()
        {
            this.Write(":DISPLAY:LABEL:DALL", null);
        }

        /// <summary>
        /// Clear all data
        /// </summary>
        public void ClearAll()
        {
            this.ClearDisplay();
            this.ClearMeasurements();
            this.ClearLabels();
        }

        /// <summary>
        /// Add a label
        /// </summary>
        /// <param name="labelRow">What label row to use 0-11</param>
        /// <param name="labelString">What text to show</param>
        public void SetLabel(int labelRow, string labelString)
        {
            string command = String.Format(":DISPLAY:LABEL \"{0}\", {1}",
                labelString, labelRow);
            this.Write(command, null);            
        }
       
        /// <summary>
        /// What mode is the DCA in
        /// </summary>
        public DcaMode DCAMode
        {
            get
            {
                string resp = this.Query(":SYSTEM:MODE?", null);
                if (resp == "OSC") return DcaMode.Oscilloscope;
                else if (resp == "EYE") return DcaMode.EyeMask;
                else throw new Ag86100Exception("Bad Mode: " + resp);
            }
            set
            {
                switch (value)
                {
                    case DcaMode.Oscilloscope:
                        this.Write(":SYSTEM:MODE OSC", null);
                        break;
                    case DcaMode.EyeMask:
                        this.Write(":SYSTEM:MODE EYE", null);
                        this.EyeColourMode = DcaEyeColourMode.ColourGrade;
                        break;
                    default:
                        throw new Ag86100Exception("Bad Mode: " + value);
                }
            }
        }

        /// <summary>
        /// What eye colouring mode is the DCA in?
        /// </summary>
        public DcaEyeColourMode EyeColourMode
        {
            get
            {
                string resp = this.Query(":DISPLAY:PERSISTENCE?", null);
                if (resp == "CGR") return DcaEyeColourMode.ColourGrade;
                else if (resp == "GSC") return DcaEyeColourMode.Greyscale;
                else throw new Ag86100Exception("DCA apparently not in eye-mask mode: " + resp);
            }
            set
            {
                switch (value)
                {
                    case DcaEyeColourMode.ColourGrade:
                        this.Write(":DISPLAY:PERSISTENCE CGRADE", null);
                        break;
                    case DcaEyeColourMode.Greyscale:
                        this.Write(":DISPLAY:PERSISTENCE GSCALE", null);
                        break;
                    default:
                        throw new Ag86100Exception("Bad Mode: " + value);
                }
            }
        }



        /// <summary>
        /// Autoscale visible traces. If fails throws an Ag86100AutoScaleException.
        /// </summary>
        /// <exception cref="Ag86100OperationFailed">Thrown if fails to autoscale</exception>
        public void AutoScale()
        {
            int initTimeout_ms = Timeout_ms;
            Timeout_ms = 10000;
            try
            {
                this.Run();
                this.Write(":AUTOSCALE", null);
            }
            catch (ChassisException e)
            {
                throw new Ag86100OperationFailed("Failed to perform autoscale", e);
            }
            finally
            {
                Timeout_ms = initTimeout_ms;
            }
        }

        /// <summary>
        /// Full scale timebase range in seconds
        /// </summary>
        public double TimebaseRange_s
        {
            get
            {
                string resp = this.Query(":TIMEBASE:RANGE?", null);
                double val = double.Parse(resp);
                return val;
            }
            set
            {
                string cmd = String.Format(":TIMEBASE:RANGE {0}", value);
                this.Write(cmd, null);
            }
        }

        
        /// <summary>
        /// DCA's trigger source
        /// </summary>
        public DcaTriggerSrc TriggerSource
        {
            get
            {
                string resp = this.Query(":TRIGGER:SOURCE?", null);
                if (resp == "FRUN") return DcaTriggerSrc.FreeRun;
                else if (resp == "FPAN") return DcaTriggerSrc.FrontPanel;
                else if (resp == "LMOD") return DcaTriggerSrc.LeftModule;
                else if (resp == "RMOD") return DcaTriggerSrc.RightModule;
                else throw new Ag86100Exception("Bad Trigger Mode: " + resp);
            }
            set
            {
                // check isn't already set
                if (value == TriggerSource) return;
                string newSrc;
                switch (value)
                {
                    case DcaTriggerSrc.FreeRun: newSrc = "FRUN"; break;
                    case DcaTriggerSrc.FrontPanel: newSrc = "FPANEL"; break;
                    case DcaTriggerSrc.LeftModule: newSrc = "LMODULE"; break;
                    case DcaTriggerSrc.RightModule: newSrc = "RMODULE"; break;
                    default: throw new Ag86100Exception("Bad Trigger Mode: " + value);
                }
                this.Write(":TRIGGER:SOURCE " + newSrc, null);
            }
        }

        /// <summary>
        /// Waits for a current pending calibration option to complete
        /// </summary>
        /// <param name="maxTime_s"></param>
        public void WaitForCalComplete(int maxTime_s)
        {
            string resp = "";

            // wait for dialog to be shown ** Removed as its not needed and has caused timeout errors ** 
            //resp =this.Query_Unchecked(":CAL:SDON?", null);

            // clear the dialog box on screen :-)
            this.Write_Unchecked(":CAL:CONT", null);

            System.Threading.Thread.Sleep(2000);
                
            // remember original visa timeout
            int origTimeout = VisaSession.Timeout;
            // set our "enhanced" timeout
            VisaSession.Timeout = maxTime_s * 1000;
            try
            {
                resp = this.Query_Unchecked(":CAL:SDONE?", null);
                if (resp != "Done")
                {
                    throw new Ag86100Exception("Failed calibration step!");
                }
            }
            finally
            {
                // restore timeout to original level
                VisaSession.Timeout = origTimeout;
            }            
        }

        /// <summary>
        ///  How many points per scan
        /// </summary>
        public int NbrScanPoints
        {
            get
            {
                string resp = this.Query(":ACQUIRE:POINTS?", null);
                int val = int.Parse(resp);
                return val;
            }
            set
            {
                string cmd = ":ACQUIRE:POINTS " + value.ToString();
                this.Write(cmd, null);
            }
        }

        /// <summary>
        /// Start acquisition
        /// </summary>
        /// <param name="nbrScans">Number of waveforms</param>
        public void StartAcquisition(int nbrScans)
        {
            this.ClearDisplay();
            this.ContinueAcquisition(nbrScans);            
        }

        /// <summary>
        /// Start acquisition
        /// </summary>
        /// <param name="nbrScans">Number of waveforms</param>
        public void ContinueAcquisition(int nbrScans)
        {
            string cmd = ":ACQUIRE:RUNTIL WAVEFORMS, " + nbrScans;
            // do this async: the wait for command completion would take far too long!
            this.Write_Unchecked(cmd, null);
            // make sure that the scope is running
            this.Write_Unchecked(":RUN", null);
        }

        /// <summary>
        /// Abort acquisition
        /// </summary>
        public void AbortAcquisition()
        {
            // do this async: seems to lock otherwise
            this.Write_Unchecked(":STOP", null);
            // clear the registers (namely the OPC bit)
            this.Write_Unchecked("*CLS", null);            
        }

        /// <summary>
        /// Free-running
        /// </summary>
        public void Run()
        {
            this.Write(":ACQUIRE:RUNTIL OFF", null);
            this.Write(":RUN", null);
        }

        /// <summary>
        /// Wait for acquisition to complete
        /// </summary>
        public void WaitForAcquisitionComplete()
        {
            int pollInterval_ms = 100;
            this.Write_Unchecked("*OPC", null);
            bool complete = false;
            while (!complete)
            {
                byte b = this.StandardEventRegister;
                // check the OPC bit
                if ((b & 1) > 0) complete = true;
                else System.Threading.Thread.Sleep(pollInterval_ms);
            }
        }

        /// <summary>
        /// Measurement thresholds
        /// </summary>
        public DcaMeasThresholds MeasurementThresholds
        {
            get
            {
                string resp = this.Query(":MEAS:DEF? THRESHOLDS", null);
                string[] splitStr = resp.Split(',');
                if (splitStr[0] != "THR")
                {
                    throw new Ag86100Exception("Bad response to threshold query: " + resp);
                }
                if (splitStr[1] == "STAN") return new DcaMeasThresholds(10.0, 50.0, 90.0);
                if (splitStr[1] != "PERC")
                {
                    throw new Ag86100Exception("Bad response to threshold query: " + resp);
                }
                DcaMeasThresholds thres = new DcaMeasThresholds(
                    Double.Parse(splitStr[4]),
                    Double.Parse(splitStr[3]),
                    Double.Parse(splitStr[2]));

                return thres;
            }
            set
            {
                string cmd = String.Format(":MEAS:DEF THR,PERCENT,{0},{1},{2}",
                    value.High_Pc, value.Mid_Pc, value.Low_Pc);
                this.Write(cmd, null);
            }
        }


        /// <summary>
        /// Queries a measurement from the DCA
        /// </summary>
        /// <param name="command">String returned from measurement query</param>
        /// <returns>the result</returns>
        /// <exception cref="Ag86100MeasException">Thrown if bad measurement, contains
        /// the error code</exception>
        private double doMeasResult(string command)
        {
            return doMeasResult(command, 1.0);
        }

        /// <summary>
        /// Queries a measurement from the DCA
        /// </summary>
        /// <param name="command">String returned from measurement query</param>
        /// <param name="multiplier">Result multiplier for units conversion</param>
        /// <returns>the result</returns>
        /// <exception cref="Ag86100MeasException">Thrown if bad measurement, contains
        /// the error code</exception>
        private double doMeasResult(string command, double multiplier)
        {
            string resp = this.Query(command, null);
            string[] splitStr = resp.Split(',');
            double val = double.Parse(splitStr[0]);
            int errCode = int.Parse(splitStr[1]);
            if (errCode != 0)
            {
                throw new Ag86100MeasException("Measurement '" + command +
                    "' failed: code " + errCode);
            }
            return val * multiplier;
        }

        /// <summary>
        /// Extinction Ratio in dB
        /// </summary>
        public double ExtinctionRatio_dB
        {
            get
            {
                return doMeasResult(":MEAS:CGR:ERATIO? DEC");
            }
        }

        /// <summary>
        /// RMS Jitter in ps
        /// </summary>
        public double JitterRms_ps
        {
            get
            {
                double val = doMeasResult(":MEAS:CGR:JITTER? RMS", 1e12);
                return val;
            }
        }

        /// <summary>
        /// Peak Jitter in ps
        /// </summary>
        public double JitterPkPk_ps
        {
            get
            {
                double val = doMeasResult(":MEAS:CGR:JITTER? PP", 1e12);
                return val;
            }
        }

        /// <summary>
        /// Eye Signal to Noise ratio in dB
        /// </summary>
        public double EyeSignal2Noise_dB
        {
            get
            {
                return doMeasResult(":MEAS:CGR:ESN?");
            }
        }

        /// <summary>
        /// Crossing point percent
        /// </summary>
        public double CrossingPoint_Pc
        {
            get
            {
                return doMeasResult(":MEAS:CGR:CROSSING?");
            }
        }

        /// <summary>
        /// BitRate measurement in bits/s
        /// </summary>
        public double BitRate
        {
            get
            {
                return doMeasResult(":MEAS:CGR:BITRATE?");
            }
        }

        /// <summary>
        /// Measures the difference between 1 and 0 level
        /// </summary>
        public double EyeAmplitude
        {
            get
            {
                return doMeasResult(":MEAS:CGR:AMPLITUDE?");
            }
        }

        /// <summary>
        /// Risetime ps
        /// </summary>
        public double Risetime_ps
        {
            get
            {
                double val = doMeasResult(":MEAS:RISETIME?", 1e12);
                return val;
            }
        }

        /// <summary>
        /// Falltime ps
        /// </summary>
        public double Falltime_ps
        {
            get
            {
                double val = doMeasResult(":MEAS:FALLTIME?", 1e12);
                return val;
            }
        }

        /// <summary>
        /// Average Power dBm
        /// </summary>
        public double AveragePower_dBm
        {
            get
            {
                return doMeasResult(":MEAS:APOWER? DEC");
            }
        }

        /// <summary>
        /// Loads a mask for use in mask testing. If null, clear the mask
        /// </summary>
        /// <param name="maskName">Name of mask to load from DCA disk, including file extension (.msk)</param>
        public void LoadMask(string maskName)
        {
            this.Write(":MTEST:LOAD \"" + maskName + "\"", null);            
        }

        /// <summary>
        /// Clears current mask
        /// </summary>
        public void ClearMask()
        {
            this.Write(":MTEST:DELETE", null);
        }

        /// <summary>
        /// Mask name - returns null if no mask is loaded
        /// </summary>
        public string MaskName
        {
            get
            {
                string resp = this.Query(":MTEST:TITLE?", null);
                if (resp == "") return null;
                else return resp;
            }
        }

        /// <summary>
        /// Is Mask Test mode on?
        /// </summary>
        public bool MaskTestOn
        {
            get
            {
                string resp = this.Query(":MTEST:TEST?", null);
                if (resp == "0") return false;
                else if (resp == "1") return true;
                else
                {
                    throw new Ag86100Exception("Bad response: " + resp);
                }
            }
            set
            {
                // check current value
                if (value == MaskTestOn) return;

                if (value) this.Write(":MTEST:START", null);
                else this.Write(":MTEST:EXIT", null);
            }
        }

        private Int64 getMaskMeas(string cmd)
        {
            string resp = this.Query(cmd, null);
            if (resp == BadMaskValue)
            {
                throw new Ag86100OperationFailed("Failed mask measurement (is mask mode active?)");
            }
            double dblVal = double.Parse(resp);
            Int64 int64Val = (Int64)dblVal;
            return int64Val;
        }

        /// <summary>
        /// How many hits in the specified region of the mask?
        /// </summary>
        /// <param name="region">Mask region</param>
        /// <returns>Number of samples that are within this mask region</returns>
        public Int64 MaskHitsInRegion(int region)
        {
            return getMaskMeas(":MTEST:COUNT:FAILURES? REGION" + region);
        }

        /// <summary>
        /// Number of mask hits in all regions and margins, if active
        /// </summary>
        public Int64 MaskHitsTotal
        {
            get
            {
                return getMaskMeas(":MTEST:COUNT:FSAMPLES?");
            }
        }


        /// <summary>
        /// Number of mask hits in mask only (not in any positive margin)
        /// </summary>
        /// <remarks>Includes hits in a negative margin</remarks>
        public Int64 MaskHitsInMask
        {
            get
            {
                if (MaskMarginOn) return getMaskMeas(":MTEST:COUNT:HITS? MASK");
                else return MaskHitsTotal;
            }
        }

        /// <summary>
        /// Number of mask hits in margin only
        /// </summary>
        /// <remarks>Margin hits for +ve or -ve margin</remarks>
        public Int64 MaskHitsInMargin
        {
            get
            {
                return getMaskMeas(":MTEST:COUNT:HITS? MARGIN");
            }
        }

        /// <summary>
        /// Are Mask margins enabled?
        /// </summary>
        public bool MaskMarginOn
        {
            get
            {
                string resp = this.Query(":MTEST:MMARGIN:STATE?", null);
                if (resp == "0") return false;
                else if (resp == "1") return true;
                else
                {
                    throw new Ag86100Exception("Bad response: " + resp);
                }
            }
            set
            {
                string newStateStr;
                if (value) newStateStr = "ON";
                else newStateStr = "OFF";
                string cmd = ":MTEST:MMARGIN:STATE " + newStateStr;
                this.Write(cmd, null);
            }
        }

        /// <summary>
        /// Eye-Mask margin
        /// </summary>
        public double MaskMargin_Pc
        {
            get
            {
                string resp = this.Query(":MTEST:MMARGIN:PERCENT?", null);
                double dblVal = double.Parse(resp);
                return dblVal;
            }
            set
            {
                string cmd = ":MTEST:MMARGIN:PERCENT " + value.ToString();
                this.Write(cmd, null);
            }
        }

        /// <summary>
        /// Queries an IEEE standard binary block from Chassis
        /// </summary>
        /// <param name="cmd">Command to send to equipment</param>
        /// <param name="i">Equipment reference</param>
        /// <returns>Reply byte array</returns>
        /// <remarks>Need new version to deal with Agilent extra termination character!</remarks>
        public new byte[] QueryIEEEBinary(string cmd, Instrument i)
        {
            // get the file data
            byte[] bytes = base.QueryIEEEBinary(cmd, i);
            // Agilent appends a linefeed character after this, so need to read this back too
            // so that it doesn't mess up any future comms. (ignore it)
            this.Read_Unchecked(null);
            return bytes;
        }

        /// <summary>
        /// Save screen to PC disk
        /// </summary>
        /// <param name="filename">Local filename</param>
        /// <param name="format">File format</param>
        /// <param name="screenMode">Screen mode - full screen or scope data only</param>
        /// <param name="invert">Invert screen or mono output</param>
        public void SaveScreenToPcDisk(string filename, DcaImageType format, 
            DcaImageDownloadType screenMode, DcaImageInversion invert)
        {
            string formatStr;
            switch (format)
            {
                case DcaImageType.Bmp: formatStr = "BMP"; break;
                case DcaImageType.Tif: formatStr = "TIF"; break;
                case DcaImageType.Jpg: formatStr = "JPG"; break;
                default:
                    throw new Ag86100Exception("Bad image type: " + format);
            }

            string screenModeStr;
            switch (screenMode)
            {
                case DcaImageDownloadType.ScopeOnly: screenModeStr = "GRAT"; break;
                case DcaImageDownloadType.FullScreen: screenModeStr = "SCR"; break;
                default:
                    throw new Ag86100Exception("Bad download type: " + screenMode);
            }

            string invertStr;
            switch (invert)
            {
                case DcaImageInversion.Normal: invertStr = "NORM"; break;
                case DcaImageInversion.Invert: invertStr = "INV"; break;
                case DcaImageInversion.Mono: invertStr = "MON"; break;
                default:
                    throw new Ag86100Exception("Bad inverted type: " + invert);
            }

            string cmd = String.Format(":DISPLAY:DATA? {0},{1},{2}",
                formatStr, screenModeStr, invertStr);
            int initTimeout_ms = this.Timeout_ms;
            // allow up to 10 seconds for transfer
            this.Timeout_ms = 10000;
            byte[] bytes;
            try
            {
                // get the file data
                bytes = this.QueryIEEEBinary(cmd, null);                
            }
            finally
            {
                this.Timeout_ms = initTimeout_ms;
            }
            
            // write out the data
            FileStream fs = new FileStream(filename, FileMode.Create);
            BinaryWriter bw = new BinaryWriter(fs);
            bw.Write(bytes);
            bw.Close();
            fs.Close();
        }
    }
}

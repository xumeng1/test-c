// [Copyright]
//
// Bookham Test Engine Library
// Bookham.TestLibrary.ChassisNS
//
// Chassis_D3286.cs
//
// Author: Joseph Olajubu, 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestLibrary.ChassisTypes;
using NationalInstruments.VisaNS;
using System.Threading;


namespace Bookham.TestLibrary.ChassisNS
{
   /// <summary>
   /// Advantest D3286 Error Detector Chassis Driver
   /// </summary>
   public class Chassis_D3286 : ChassisType_Visa
   {
      #region Constructor
      /// <summary>
      /// Chassis Constructor.
      /// </summary>
      /// <param name="chassisNameInit">Chassis name</param>
      /// <param name="driverNameInit">Chassis driver name</param>
      /// <param name="resourceString">VISA resource string for communicating with the chassis</param>
      public Chassis_D3286(string chassisNameInit, string driverNameInit,
          string resourceString)
         : base(chassisNameInit, driverNameInit, resourceString)
      {
         // Setup expected valid hardware variants 
         ChassisDataRecord D3286A04Data = new ChassisDataRecord(
             "ADVANTEST D3286",			// hardware name 
             "REV      G03",			            // minimum valid firmware version 
             "REV      G03");		        // maximum valid firmware version 
         ValidHardwareData.Add("D3286A04", D3286A04Data);
      }
      #endregion

      #region Chassis overrides
      /// <summary>
      /// Firmware version of this chassis.
      /// </summary>
      public override string FirmwareVersion
      {
          get
          {
              // Read the chassis ID string
              string idn = Query_Unchecked("IDN?", null);

              // Return the firmware version in the 3rd comma seperated field
              return idn.Split(',')[2].Trim();
          }
      }

      /// <summary>
      /// Hardware Identity of this chassis.
      /// </summary>
      public override string HardwareIdentity
      {
          get
          {
              // Read the chassis ID string and split the comma seperated fields
              string[] idn = Query_Unchecked("IDN?", null).Split(',');

              // Return field1, the manufacturer name and field 2, the model number
              return idn[0] + " " + idn[1];
          }
      }

      /// <summary>
      /// Setup the chassis as it goes online
      /// </summary>
      public override bool IsOnline
      {
         get
         {
            return base.IsOnline;
         }
         set
         {
            // setup base class
            base.IsOnline = value;

            if (value) // if setting online                
            {
                //Reset the instrument
                this.Write_Unchecked("C", null);

                //Mask the Status byte from raising SRQs.
                //bit0: Measurement Data Occurence
                //bit1: Syntax error - 1 if in error
                //bit2: Bit Error Occurence
                //bit3: Clock Error Occurence
                //bit4: Sync Error Occurence
                //bit5: Measurement results overflow
                //bit6; RQS - always the same as bit1 
                //bit7: BUSY bit: Always set during delay setting of clock input,
                //      during pattern setting section modification, disk access, or auto search.
                //       0 otherwise.

                //Operate in polled mode
                this.Write_Unchecked("S1", null);

                //Mask all bits (we will not operate in SRQ mode
                this.Write_Unchecked("MS 0", null);

                //Tur off response headers
                this.Write_Unchecked("HDOF", null);
                //Clear the status byte register.
                //this.Write_Unchecked("CSB", null);                                           
            }
         }
      }


      /// <summary>
      /// Writes a command to the chassis and will wait for completion and check status
      /// byte and other registers for error conditions.
      /// </summary>
      /// <param name="command">Command to send</param>
      /// <param name="i">Which instrument is this from?</param>
      /// <remarks>Uses the chassis' default asynchronous set and "no error check" setting</remarks>
      public void Write(string command, Instrument i)
      {
          this.Write(command, i, false, true);
      }

      /// <summary>
      /// Writes a command to the chassis. Dependent on options given this will wait 
      /// for completion and check status
      /// byte and other registers for error conditions.
      /// </summary>
      /// <param name="command">Command to send</param>
      /// <param name="i">Which instrument is this from?</param>
      /// <param name="asyncSet">Set asynchronously, therefore also don't check for errors</param>
      /// <param name="errorCheck">If async set to false, then this parameter determines
      /// whether errors are checked for</param>
      public void Write(string command, Instrument i, bool asyncSet, bool errorCheck)
      {
          byte sb = this.QueryStatusByte();
          this.Write_Unchecked(" " + command, i);
          if (asyncSet || !errorCheck) return;

          //Wait 100ms for the command to take effect (arbitary).
          Thread.Sleep(50);

          //Check for syntax error
          if (IsErrorInStatusByte())
          {
              //Syntax error. Raise an exception
              throw new ChassisException("Status Register indicates syntax in  command: " + command);
          }
      }

      /// <summary>
      /// Sends a command to the chassis and waits for a response. Dependent on 
      /// arguments, will check the status byte and other registers for error 
      /// conditions.
      /// </summary>
      /// <param name="command">Command to send</param>
      /// <param name="i">Which instrument is this from?</param>
      /// <returns>string response</returns>
      public string Query(string command, Instrument i)
      {
          byte sb = this.QueryStatusByte();

          string resp = this.Query_Unchecked(command, i);

          if (IsErrorInStatusByte())
          {
              // collate information about the error from the chassis and throw 
              // a ChassisException
              throw new ChassisException("Status Register indicates syntax in  command: " + command);
          }

          return resp;
      }

       /// <summary>
       /// Returns the Status Byte from the instrument.
       /// </summary>
       /// <returns>The status byte.</returns>
       public byte QueryStatusByte()
       {
           return (byte)this.GetStatusByte();
       }

       /// <summary>
       /// Returns the Status Byte from the instrument.
       /// </summary>
       /// <returns>The status byte.</returns>
       public void  ClearDevice()
       {
           base.VisaSession.Clear();
       }
      #endregion

      #region helper methods

      /// <summary>
      /// Query the status byte register and return true if an error 
      /// condition is signalled.
      /// </summary>
      /// <returns>true if "Syntax error" bit is set</returns>
       private bool IsErrorInStatusByte()
       {
           byte statusByte = (byte)this.GetStatusByte();

           //Bit 1 = 1, indicates a command syntax error.
           if ((statusByte & 0x02) != 0)
           {
               //Syntax error.
               return true;
           }
           else
           {
               return false;
           }
       }
      #endregion
  }
}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_D3286_ErrorDetector.cs
//
// Author: joseph.olajubu, 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisNS;

namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// Advantest D3286 Error Detector Instrument Driver
    /// </summary>
    public class Inst_D3286_ErrorDetector : Instrument, IInstType_BertErrorAnalyser
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_D3286_ErrorDetector(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            InstrumentDataRecord instrD3286A04 = new InstrumentDataRecord(
             "ADVANTEST D3286",			// hardware name 
             "REV      G03",			            // minimum valid firmware version 
             "REV      G03");		        // maximum valid firmware version 
            ValidHardwareData.Add("D3286A04", instrD3286A04);

            // Configure valid chassis driver information
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_D3286",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "2.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("Chassis_D3286", chassisInfo);

            // initialise this instrument's chassis
            this.instrumentChassis = (Chassis_D3286)chassisInit;
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                return this.instrumentChassis.FirmwareVersion;
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                return this.instrumentChassis.HardwareIdentity;
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            //Reset the instrument
            this.Reset();

            //Set mark Ratio to 1/2
            instrumentChassis.Write("MR 1/2", this);

            //Turn off response headers
            instrumentChassis.Write("HDOF", this);
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // TODO: custom setup for this instrument after it goes online                    
                }
            }
        }
        #endregion

        #region Private data
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_D3286 instrumentChassis;
        private double lastBitFrequency_Hz = 0;
        #endregion

        #region IInstType_BertErrorAnalyser Members


        /// <summary>
        /// Get the analyser state (true: running, false: not running)
        /// </summary>
        public bool AnalyserEnabled
        {
            get 
            {
                string resp = this.instrumentChassis.Query("GS?", this);

                if (resp == "GSOPN")
                {
                    return true;
                }
                else
                {
                    return false;
                } 
            }
        }

        /// <summary>
        /// Start the Analyser
        /// </summary>
        public void AnalyserStart()
        {
            // Use unchecked write to stop spurious syntax errors - possible instrument bug
            this.instrumentChassis.Write_Unchecked("STT", this);
        }


        /// <summary>
        /// Stop the Analyser
        /// </summary>
        public void AnalyserStop()
        {
            this.instrumentChassis.Write_Unchecked("STP",this);
        }


        /// <summary>
        /// Automatically adjust the delay of clock input an threshold level of data input
        /// Also automatically sets up the Mark/Space Ratio
        /// </summary>
        public void AutoSetup()
        {
            this.instrumentChassis.Clear();
            //Turn AutoSync ON to look for Sync Error
            this.instrumentChassis.Write_Unchecked("ASON", this);
            Thread.Sleep(500);
            byte resp = this.instrumentChassis.QueryStatusByte();
            Thread.Sleep(500);
            resp = this.instrumentChassis.QueryStatusByte();
            if ((resp & 0x10) != 0)
            {
                //Instrument reporting  Sync Loss
                //Turn AutoSync Off
                this.instrumentChassis.Write_Unchecked("ASOFF", this);

                //Run Clock/data alaignment
                this.instrumentChassis.Write_Unchecked("SRHGO", this);
                //Wait 2 seconds.
                Thread.Sleep(2000);
            }
            else
            {
                //Instrument not reporting  Sync Loss
                //Turn AutoSync Off
                this.instrumentChassis.Write_Unchecked("ASOFF", this);
            }
        }

        /// <summary>
        /// Ratio of errored bits to total bit count
        /// </summary>
        public double ErrorRatio
        {
            get 
            {

                double errorRate;
                try
                {
                    errorRate = Double.Parse(this.instrumentChassis.Query_Unchecked("ERR?", this), System.Globalization.NumberStyles.Float);
                }
                catch
                {
                    LogEvent ("Exception Raised when reading error rate. Usually happens due to Sync Loss. Returning a large error rate value: 1 x 10E-2");
                    errorRate = 0.01;
                }

                return errorRate;
            }
        }

        /// <summary>
        /// How many errored bits
        /// </summary>
        public long ErroredBits
        {
            get
            {

                long errorCount;

                try
                {
                    //Ensure that we are total error s measurement mode.
                    this.instrumentChassis.Write_Unchecked("TOT", this);
                    
                    //Query errors.
                    string errorString = this.instrumentChassis.Query_Unchecked("ERC?", this);
                    errorCount = Int64.Parse(errorString, System.Globalization.NumberStyles.Float);
                }
                catch
                {
                    LogEvent("Exception Raised when reading error rate. Usually happens due to Sync Loss. Returning a large error count value: 1 x 10E6");
                    errorCount = 1000000;
                }

                return errorCount;
            }
        }

        ///<summary> 
        /// The number of errors
        /// accumulated since the start of the gating period, where
        /// each error is a true data zero received as a data one.
        /// </summary>
        public long ErroredBitsZerosRecievedAsOnes
        {
            get
            {

                long errorCount;

                try
                {
                    //Ensure that we are 0->1 error measurement mode.
                    this.instrumentChassis.Write_Unchecked("INS", this);

                    //Query errors.
                    string errorString = this.instrumentChassis.Query_Unchecked("ERC?", this);
                    errorCount = Int64.Parse(errorString, System.Globalization.NumberStyles.Float);
                }
                catch
                {
                    LogEvent("Exception Raised when reading error rate. Usually happens due to Sync Loss. Returning a large error count value: 1 x 10E6");
                    errorCount = 1000000;
                }

                return errorCount;
            }
        }

        /// <summary>
        /// The error ratio accumulated since the start of the gating period,
        /// where each error is a true data zero received as a data one.
        /// </summary>
        public double ErrorRatioOfZerosRecievedAsOnes
        {
            get
            {
                double errorRate;

                try
                {
                    //Ensure that we are 0->1 error measurement mode.
                    this.instrumentChassis.Write_Unchecked("INS", this);
                    errorRate = Double.Parse(this.instrumentChassis.Query_Unchecked("ERR?", this), System.Globalization.NumberStyles.Float);
                }
                catch
                {
                    LogEvent("Exception Raised when reading error rate. Usually happens due to Sync Loss. Returning a large error rate value: 1 x 10E-2");
                    errorRate = 0.01;
                }

                return errorRate;
            }
        }

        /// <summary>
        ///The number of errors
        ///accumulated since the start of the gating period, where
        ///each error is a true data one received as a data zero
        /// </summary>
        public long ErroredBitsOnesRecievedAsZeros
        {
            get
            {

                long errorCount;

                try
                {
                    //Ensure that we are 1->0 error measurement mode.
                    this.instrumentChassis.Write_Unchecked("OMI", this);

                    //Query errors.
                    string errorString = this.instrumentChassis.Query_Unchecked("ERC?", this);
                    errorCount = Int64.Parse(errorString, System.Globalization.NumberStyles.Float);
                }
                catch
                {
                    LogEvent("Exception Raised when reading error rate. Usually happens due to Sync Loss. Returning a large error count value: 1 x 10E6");
                    errorCount = 1000000;
                }

                return errorCount;
            }
        }

        /// <summary>
        /// The error ratio accumulated since the start of the gating period,
        /// where each error is a true data one received as a data zero
        /// </summary>
        public double ErrorRatioOfOnesRecievedAsZeros
        {
            get
            {
                double errorRate;
                try
                {
                    //Ensure that we are 1->0 error measurement mode.
                    this.instrumentChassis.Write_Unchecked("OMI", this);
                    errorRate = Double.Parse(this.instrumentChassis.Query_Unchecked("ERR?", this), System.Globalization.NumberStyles.Float);
                }
                catch
                {
                    LogEvent("Exception Raised when reading error rate. Usually happens due to Sync Loss. Returning a large error rate value: 1 x 10E-2");
                    errorRate = 0.01;
                }

                return errorRate;
            }
        }

        /// <summary>
        /// Get/set the Clock input  Delay
        /// </summary>
        public double MeasPhaseDelay_ps
        {
            get
            {
                return Convert.ToDouble(this.instrumentChassis.Query("DLY?", this).Trim());
            }
            set
            {
                if ((value >= -400) && (value <= 400))
                {
                    this.instrumentChassis.Write_Unchecked("DLY " + value, this);
                }
                else
                {
                    throw new InstrumentException("MeasPhaseDelay_ps outside -400ps to +400ps range supported by instrument: " + value);
                }
            }
        }


        /// <summary>
        /// Get/set the Eye threshold voltage
        /// </summary>
        public double MeasThreshold_V
        {
            get
            {
                return Convert.ToDouble(this.instrumentChassis.Query("TLVL?", this).Trim());
            }
            set
            {
                if ((value >= -2.040) && (value <= 2.040))
                {
                    this.instrumentChassis.Write_Unchecked("TLVL " + value, this);
                }
                else
                {
                    throw new InstrumentException("MeasThreshold_V outside range -2.040V to +2.0404V supported by instrument: " + value);
                }
            }
        }

        /// <summary>
        /// Get/set the Pattern type.
        /// In this version of the driver, only PRBS Patterns shall be supported.
        /// </summary>
        public InstType_BertDataPatternType PatternType
        {
            get
            {
                string resp = this.instrumentChassis.Query("PM?", this).Trim();
                if (resp == "PRBS")
                {
                    //This should really be the only response seen, unless
                    //someones been playing with the front panel
                    return InstType_BertDataPatternType.Prbs;
                }
                else
                {
                    return InstType_BertDataPatternType.Other;
                }

            }
            set
            {
                if (value == InstType_BertDataPatternType.Prbs)
                {
                    this.instrumentChassis.Write("PRBS", this);
                }
                else
                {
                    throw new InstrumentException("Only PRBS Patter Types are supported by this driver");
                }
            }
        }


        /// <summary>
        /// Get/set the PRBS length 2^N-1 (i.e. this is the "N")
        /// </summary>
        public int PrbsLength
        {
            get
            {
                string resp = this.instrumentChassis.Query("PB?", this).Trim();
                if (resp == "PB 07,0")
                {
                    return 7;
                }
                else if (resp == "PB 09,0")
                {
                    return 9;
                }
                else if (resp == "PB 10,0")
                {
                    return 10;
                }
                else if (resp == "PB 11,0")
                {
                    return 11;
                }
                else if (resp == "PB 15,0")
                {
                    return 15;
                }
                else if (resp == "PB 23,0")
                {
                    return 23;
                }
                else if (resp == "PB 31,0")
                {
                    return 31;
                }
                else
                {
                    throw new InstrumentException("Invalid PRBS Length recieved from instrument: " + resp);
                }
            }
            set
            {
                if ((value == 7) || (value == 9) || (value == 10) || (value == 11)
                    || (value == 15) || (value == 23) || (value == 31))
                {
                    if ((value == 7) || (value == 9))
                    {
                        this.instrumentChassis.Write("PB 0" + value, this);
                    }
                    else
                    {
                        this.instrumentChassis.Write("PB " + value, this);
                    }
                }
                else
                {
                    throw new InstrumentException("Invalid PRBS Length Specified : " +  value);
                }
            }
        }

        /// <summary>
        /// Get/set the Data Polarity of the input
        /// </summary>
        public InstType_BertDataPolarity Polarity
        {
            get
            {
                string resp = this.instrumentChassis.Query("MP?", this).Trim();

                if (resp == "MPN")
                {
                    return InstType_BertDataPolarity.Normal;
                }
                else if (resp == "MPI")
                {
                    return InstType_BertDataPolarity.Inverted;
                }
                else
                {
                    throw new InstrumentException("Invalid response to Polarity Query from instrument: " + resp);
                }
            }
            set
            {
                if (value == InstType_BertDataPolarity.Normal)
                {
                    this.instrumentChassis.Write("MPN", this);
                }
                else
                {
                    this.instrumentChassis.Write("MPI", this);
                }
            }
        }


        /// <summary>
        /// Has there been a sync loss since the analyser was last enabled?
        /// </summary>
        public bool SyncLossDetected
        {
            get
            {
                //Attempt1
                //this.instrumentChassis.Clear();
                //System.Threading.Thread.Sleep(50);
                //string respStr = this.instrumentChassis.Query("HST?", this);
                //respStr = respStr.Replace("HST ", "").Trim();
                //byte resp = Convert.ToByte (respStr);

                //Attempt 2
                //this.instrumentchassis.clear();
                //byte resp = this.instrumentchassis.querystatusbyte();
                //thread.sleep(500);
                //resp = this.instrumentchassis.querystatusbyte();
                //if ((resp & 0x10) == 0)
                //{
                //    return false;
                //}
                //else
                //{
                //    return true;
                //}

                //Attempt3
                this.instrumentChassis.Write_Unchecked("ASON", this);
                Thread.Sleep(1000);
                this.instrumentChassis.Write_Unchecked("ASOFF", this);
                AnalyserStart();
                Thread.Sleep(1000);
                AnalyserStop();

                byte resp = this.instrumentChassis.QueryStatusByte();

                try
                {
                    if ((this.ErrorRatio > 0.001) || ((resp & 0x10) != 0))
                    {
                        //Error Rate is greater than 10E-3. Interprete this 
                        //as Sync Loss
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch
                {
                    //Instrument always fails to report Error Rate if Sync Loss
                    return true;
                }
            }

        }

        /// <summary>
        /// Get/Set user defined pattern. Not supported by this version of the driver.
        /// </summary>
        public bool[] UserPattern
        {
            get 
            { 
                throw new Exception("The method or operation is not implemented."); 
            }            
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        /// <summary>
        /// Get/Set user defined pattern length. Not supported by this version of the driver.
        /// </summary>
        public int UserPatternLength
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        #endregion

        #region Instrument specific commands

        /// <summary>
        /// Performs a reset of the instrument and waits for it to finish. 
        /// </summary>
        private void Reset()
        {
            // Start the reset
            instrumentChassis.Write_Unchecked("C", this);

            //Wait 2 seconds.
            Thread.Sleep(2000);

            // Start the reset
            instrumentChassis.Write_Unchecked("Z", this);

            //Wait 11 seconds (the manual says it takes approx 10 seconds to reset).
            Thread.Sleep(20000);
        }

        /// <summary>
        ///Gate Interval in seconds
        /// </summary>
        public double GatingInterval_S
        {
            get
            {
                //Read time interval
                string gatingIntervalString = this.instrumentChassis.Query("TINT?", this).Replace("TINT ", "");
                string[] split = gatingIntervalString.Split(':');
                double gatingInterval_S = (24 * 60 * 60 * (Convert.ToDouble(split[0]))) + 
                                    (60*60*(Convert.ToDouble(split[1]))) +
                                    (60*(Convert.ToDouble(split[2]))) + 
                                    Convert.ToDouble(split[3]); 
                                
                return gatingInterval_S;
            }
        }

        /// <summary>
        ///Gate Interval in seconds
        /// </summary>
        public double BitFrequency_MHz
        {
            get
            {
                //Read the bit frequency
                string bitFrequencyString_Hz = this.instrumentChassis.Query_Unchecked("FRQ?", this);
                double bitFrequency_MHz = Double.Parse(bitFrequencyString_Hz, System.Globalization.NumberStyles.Float) / 1000000;
                return bitFrequency_MHz;
            }
        }
        #endregion
    }
}

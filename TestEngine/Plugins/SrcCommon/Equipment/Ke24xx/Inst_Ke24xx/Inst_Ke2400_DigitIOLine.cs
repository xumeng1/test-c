using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisNS;
using Bookham.TestEngine.PluginInterfaces.Instrument;


namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// 
    /// </summary>
    public class Inst_Ke2400_DigitIOLine:UnmanagedInstrument,IInstType_DigitalIO
    {
        /// <summary>
        /// constructor, set a dio line in dio port to be an instrument
        /// </summary>
        /// <param name="linename"> dio line name </param>
        /// <param name="ke24xx"> ke2400 chassis </param>
        /// <param name="lineNbr"> line number in all dio lines </param>
        internal Inst_Ke2400_DigitIOLine(string linename, Chassis_Ke24xx ke24xx, int lineNbr)
            : base(linename, ke24xx)
        {
            this.instrumentChassis = ke24xx;
            this.lineNbr = lineNbr;
            this.lineNbrMask = 1 << (lineNbr - 1);
            // eclude others lines's value to be "1", the the coresponding line to be "0"
            this.lineNbrAntiMask = 0x0F ^ this.lineNbrMask;
            this.IsOnline = true;

        }
        
        #region IInstType_DigitalIO Members
        /// <summary>
        /// set/get Dio line's output state
        /// </summary>
        public bool LineState
        {
            get
            {
                int allline = GetAllLinesState();
                bool linestate;
                if ((allline & lineNbrMask) > 0) linestate = true;
                else linestate = false;
                return linestate;
            }
            set
            {
                int alllines = GetAllLinesState();
                // set the correspongding bit to be "0", keep others lines' value( and "1" at the coresponding bit)
                int otherlines = alllines & lineNbrAntiMask;
                int linevalue;
                if (value) linevalue = otherlines + lineNbrMask;
                else linevalue = otherlines;

                string cmd = "SOUR2:TTL " + linevalue.ToString();
                instrumentChassis.Write(cmd, this);


            }
        }
        /// <summary>
        /// get all dio lines' output state
        /// </summary>
        /// <returns></returns>
        private int GetAllLinesState()
        {
            string resp = instrumentChassis.Query("SOUR2:TTL?", this);
            int val = int.Parse(resp);
            return val;
        }

        /// <summary>
        /// set a dio line's out put to be "0"
        /// </summary>
        public override void SetDefaultState()
        {
            LineState = false;
        }

        #endregion

        #region Private data
        /// <summary>
        /// Underlying instrument
        /// </summary>
        private Chassis_Ke24xx instrumentChassis;
        /// <summary>
        /// Digital output line number 
        /// </summary>
        private int lineNbr;

        /// <summary>
        /// Mask for the digital output line number (used to extract/insert specific value for this line)
        /// </summary>
        private int lineNbrMask;
        /// <summary>
        /// Anti-Mask for the digital output line number 
        /// (used to retrieve values for all other lines apart from this one)
        /// </summary>
        private int lineNbrAntiMask;
        #endregion
    }
}

// [Copyright]
//
// Bookham Test Library
// Bookham.TestSolution.Instruments
//
// Inst_SantecOTF920.cs
//
// Author: Bill P. Godfrey, 2006
// Design: Optical Tuneable Filter Instrument Driver DD

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisTypes;
using System.Threading;

namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// Driver for the Santec OTF 920 series.
    /// </summary>
    public class Inst_SantecOTF920 : InstType_OpticalTuneableFilter
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_SantecOTF920(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            InstrumentDataRecord santecOtf920 = new InstrumentDataRecord(
                "SantecOTF920",				// hardware name 
                "0.0.0.0",  			// minimum valid firmware version 
                "2.0.0.0");			// maximum valid firmware version 
            ValidHardwareData.Add("SantecOTF920", santecOtf920);

            // Configure valid chassis driver information
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_SantecOTF920",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "2.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("SantecOTF920", chassisInfo);

            // initialise this instrument's chassis
            this.instrumentChassis = (ChassisType_Visa)chassisInit;
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                return instrumentChassis.FirmwareVersion;
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                return instrumentChassis.HardwareIdentity;
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            //this.Reset();   // Takes too long!
            SetAutomaticPowerControl(false); 
            SetWavelength_nm(1550.0);

        }
        #endregion

        #region InstType_OpticalTuneableFilter overrides
        /// <summary>
        /// Gets or sets current attenuation.
        /// </summary>
        public override double Attenuation_dB
        {
            get
            {
                return double.Parse(this.instrumentChassis.Query_Unchecked("AT", this));
            }
            set
            {
                this.instrumentChassis.Write_Unchecked("AT " + value, this);
            }
        }

        /// <summary>
        /// Gets measured optical power in dBm.
        /// </summary>
        public override double OpticalPower_dBm
        {
            get
            {
                return double.Parse(this.instrumentChassis.Query_Unchecked("OP", this));
            }
        }

        /// <summary>
        /// Gets measured optical power in mW.
        /// </summary>
        public override double OpticalPower_mW
        {
            get
            {
                return double.Parse(this.instrumentChassis.Query_Unchecked("LP", this));
            }
        }

        /// <summary>
        /// Gets and sets operable wavelength in nm.
        /// </summary>
        public override double Wavelength_nm
        {
            get
            {
                return double.Parse(this.instrumentChassis.Query_Unchecked("WA", this));
            }
            set
            {
                SetWavelength_nm(value);
            }
        }
        #endregion

        #region Santec 920 functions

        /// <summary>
        /// Executes Peak search function and waits for completion
        /// If the scan width is greater than 5nm, the 'rapid' mode is used
        /// Note: The Santec often fails if the scan width is >55nm.
        /// </summary>
        /// <param name="wl_start">Lower scan wavelength in nm</param>
        /// <param name="wl_stop">Upper scan wavelength in nm</param>
        /// 
        public override void Execute_PeakSearch(double wl_start, double wl_stop)
        {
            // validate start and stop
            if (wl_start >= wl_stop) 
            {
                throw new InstrumentException("Execute_PeakSearch failed. wl_start(" + wl_start + ") >= wl_stop(" + wl_stop + ")");
            }

            double wl_span = (wl_stop - wl_start); 

            SetWavelength_nm(wl_start);

            if (wl_span > 5.0)   // Use longer scan and rapid mode
            {
                this.instrumentChassis.Write_Unchecked("PO", this); // Set to rapid mode
            }
            else
            {
                this.instrumentChassis.Write_Unchecked("PF", this); // Disable rapid mode
            }

            this.instrumentChassis.Write_Unchecked("WS 0.0", this);  // Set lower wavelength to scan
            this.instrumentChassis.Write_Unchecked("WL " + wl_span, this); // Set upper wavelength to scan

            this.instrumentChassis.Write_Unchecked("PS", this);
            Thread.Sleep(3000);
            // Wait up to 60 seconds for PeakSearch to complete
            this.waitForPeakSearch(60 * 1000, 100);
        }

        /// <summary>
        /// Set target optical power in dBm and turn on APC.
        /// </summary>
        /// <param name="pwr">Target optical power.</param>
        public void SetOpticalPower_dBm(double pwr)
        {
            SetAutomaticPowerControl(false);
            this.instrumentChassis.Write_Unchecked("OP " + pwr, this);
            SetAutomaticPowerControl(true);
        }

        /// <summary>
        /// Set target optical power in mW and turn on APC.
        /// </summary>
        /// <param name="pwr">Target optical power.</param>
        public void SetOpticalPower_mW(double pwr)
        {
            SetAutomaticPowerControl(false);
            this.instrumentChassis.Write_Unchecked("LP " + pwr, this);
            SetAutomaticPowerControl(true);
        }

        /// <summary>
        /// Turn APC (Automatic power control) on or off.
        /// </summary>
        /// <param name="on">True for on, False for off.</param>
        public void SetAutomaticPowerControl(bool on)
        {
            this.instrumentChassis.Write_Unchecked(on ? "AO" : "AF", this);
        }

        /// <summary>
        /// Gets current instrument status.
        /// </summary>
        public OTFStatus Status
        {
            get
            {
                string resp;
                OTFStatus status;

                while (true)
                {
                    resp = this.instrumentChassis.Query_Unchecked("SU", this);
                    if ((resp.Length == 6) || (resp.Length == 7))
                    {
                        try
                        {
                            status = new OTFStatus(resp, this);
                            break;
                        }
                        catch
                        {
                            continue;
                        }
                    }
                }

                return status;
            }
        }


        /// <summary>
        /// Reset instrument.
        /// </summary>
        public void Reset()
        {
            this.instrumentChassis.Write_Unchecked("RE", this);
            Thread.Sleep(20000);
            waitForSlidersToSettle(40000, 500);
        }

        /// <summary>
        /// Set Wavelength
        /// </summary>
        /// <param name="Wl_nm"> Target wavelength in nm</param>
        /// 
        private void SetWavelength_nm(double Wl_nm)
        {
            this.instrumentChassis.Write_Unchecked("WA " + Wl_nm, this);
            /* Wait up to 40 seconds for sliders to settle. */
            this.waitForSlidersToSettle(40 * 1000, 100);

            /* Read back wavelength to see if settled wavelength is as expected. */
            double actual_nm = Wavelength_nm;
            double diff_nm = Math.Abs(actual_nm - Wl_nm);
            if (diff_nm > 0.1)
            {
                throw new InstrumentException("Set Wavelength_nm failed. Expected " + Wl_nm + " nm. Got " + actual_nm + " nm.");
            }
        }

        #endregion

        /// <summary>
        /// Repeatedly poll the status command until it indicates that the
        /// sliders have stopped moving.
        /// </summary>
        /// <param name="maxWait_ms">Maximum waiting period.</param>
        /// <param name="pollDelay_ms">Delay between polls.</param>
        private void waitForSlidersToSettle(int maxWait_ms, int pollDelay_ms)
        {
            int maxTries = (maxWait_ms/pollDelay_ms)+1;
            for (int t = 0; t < maxTries; ++t)
            {
                /* Delay between tries. Don't wait for the first try. */
                if (t != 0)
                {
                    System.Threading.Thread.Sleep(pollDelay_ms);
                }

                /* Check status, returning if stationary. */
                OTFStatus stat = this.Status;
                if (stat.SliderState == SliderState.Stationary && stat.Resetting == false)
                {
                    return;
                }
            }

            /* If we get here, the instrument timed out. */
            throw new InstrumentException("Instrument took too long to settle.");
        }

        /// <summary>
        /// Repeatedly poll the status command until it indicates that the
        /// PeakSearch has finished.
        /// </summary>
        /// <param name="maxWait_ms">Maximum waiting period.</param>
        /// <param name="pollDelay_ms">Delay between polls.</param>
        private void waitForPeakSearch(int maxWait_ms, int pollDelay_ms)
        {
            int maxTries = (maxWait_ms / pollDelay_ms) + 1;
            for (int t = 0; t < maxTries; ++t)
            {
                /* Delay between tries. Don't wait for the first try. */
                if (t != 0)
                {
                    System.Threading.Thread.Sleep(pollDelay_ms);
                }

                /* Check status, returning if inactive. */
                OTFStatus stat = this.Status;
                if (stat.PeakSearchState == PeakSearchState.NotActive || stat.PeakSearchState == PeakSearchState.WaitingRapid)
                {
                    return;
                }
            }

            /* If we get here, the instrument timed out. */
            throw new InstrumentException("Instrument took too long to settle.");
        }

        /// <summary>
        /// Container class for an OTF920 status.
        /// </summary>
        public struct OTFStatus
        {
            /// <summary>
            /// Gets slider state.
            /// </summary>
            public readonly SliderState SliderState;

            /// <summary>
            /// Gets peak search state.
            /// </summary>
            public readonly PeakSearchState PeakSearchState;

            /// <summary>
            /// Gets current APC control state.
            /// </summary>
            public readonly APCControlState APCControlState;

            /// <summary>
            /// Gets flag indicating if APC is on or not.
            /// </summary>
            public readonly bool APCOn;

            /// <summary>
            /// Gets flag indicating an instrument reset in progress.
            /// </summary>
            public readonly bool Resetting;

            /// <summary>
            /// Gets the raw status register.
            /// </summary>
            public readonly string RawStatus;

            /// <summary>
            /// Constructor.
            /// </summary>
            /// <param name="status">Raw status from the SU command.</param>
            /// <param name="inst">Link back to instrument to raise errors.</param>
            internal OTFStatus(string status, Inst_SantecOTF920 inst)
            {
                /* Array of flags indicating if the digit in this position is useful. (From Santec documents.) */
                bool[] fieldUsed = { true, false, false, true, true, true };
                /* Maximum allowed value in each position, if corresponding fieldUsed flag is true. (From Santec documents.) */
                int[] maxValue = { 1, -1, -1, 1, 3, 7 };
                /* Digits converted to integers and range checked. */
                int[] statDigits = new int[6];

                /* Check for '-' at front on string */
                if (status[0] == '-')
                {
                    this.Resetting = true;
                    status = status.Remove(0, 1);
                }
                else
                {
                    this.Resetting = false;
                }

                    /* Check length. */
                    if (status.Length == 7)
                    {
                        /* If status is seven characters, remove character [2]. */
                        status = status.Remove(2, 1);
                    }

                    if (status.Length != 6)
                    {
                        throw new InstrumentException("OTFStatus \"" + status + "\" is not 6 characters.");
                    }

                    /* Parse each character. */
                    for (int i = 0; i < 6; ++i)
                    {
                        /* Only do something if field holdsa useful value. */
                        if (fieldUsed[i])
                        {
                            /* Extract applicable character. */
                            char digit = status[i];

                            /* Check its a digit. */
                            if (!Char.IsDigit(digit))
                            {
                                throw new InstrumentException("OTFStatus \"" + status + "\" [" + i + "] (" + digit + ") was not a digit.");
                            }

                            /* Convert to int. */
                            int digitValue = int.Parse(digit.ToString());

                            /* Range check. */
                            if (digitValue > maxValue[i])
                            {
                                inst.LogErrorRaise(
                                    "OTFStatus \"" + status + "\" [" + i + "] (" + digit + ") was out of range. Max = " + maxValue[i]);
                            }

                            /* Store. */
                            statDigits[i] = digitValue;
                        }
                    }

                    /* Convert ints to enums. */
                    this.SliderState = (SliderState)(statDigits[5]);
                    this.PeakSearchState = (PeakSearchState)(statDigits[4]);
                    this.APCControlState = (APCControlState)(statDigits[3]);
                    this.APCOn = (statDigits[1] == 1);


                /* Store original value. */
                this.RawStatus = status;
            }

            /// <summary>
            /// Convert status to human readable English form/
            /// </summary>
            /// <returns>Parsed status register meaning.</returns>
            public override string ToString()
            {
                return
                    "Slider State: " + this.SliderState.ToString() + "\r\n" +
                    "Peak Search State: " + this.PeakSearchState.ToString() + "\r\n" +
                    "APC Control State: " + this.APCControlState.ToString() + "\r\n" +
                    "APC On: " + (this.APCOn ? "On" : "Off") + "\r\n" +
                    "Raw Status: " + this.RawStatus + "\r\n";
            }
        }

        #region OTF Status enum types.
        /// <summary>
        /// State of sliders.
        /// </summary>
        public enum SliderState
        {
            /// <summary>Both sliders are stationary.</summary>
            Stationary = 0,
            /// <summary>Wavelength set but slider 1 is moving.</summary>
            WavelengthSetSlider1Moving = 1,
            /// <summary>Wavelength set but slider 2 is moving.</summary>
            WavelengthSetSlider2Moving = 2,
            /// <summary>Wavelength is being tuned.</summary>
            WavelengthTuning = 3,
            /// <summary>Slider is removed from beam, or is moving to set a wavelength offset.</summary>
            SliderRemovedOrMoving = 4,
            /// <summary>Slider 2 is set at given wavelength. Slider 1 is being removed from beam.</summary>
            Slider2SetSlider1Removing = 5,
            /// <summary>Slider 1 is set at given wavelength. Slider 2 is being removed from beam.</summary>
            Slider1SetSlider2Removing = 6,
            /// <summary>Wavelength is being tuned, whilst other slider is being removed from beam.</summary>
            WavelengthTuningSliderRemoved = 7
        }

        /// <summary>
        /// Current state of peak search process.
        /// </summary>
        public enum PeakSearchState
        {
            /// <summary>Peak search function is not active.</summary>
            NotActive,
            /// <summary>Peak search function is active.</summary>
            Active,
            /// <summary>Peak search function is set to rapid mode.</summary>
            WaitingRapid,
            /// <summary>Peak search function is in rapid mode and active.</summary>
            ActiveRapid
        }

        /// <summary>
        /// State of APC control.
        /// </summary>
        public enum APCControlState
        {
            /// <summary>Normal operation.</summary>
            Normal,
            /// <summary>Indicates out of control range when operated in APC mode.</summary>
            OutOfControlRange
        }
        #endregion

        #region Private data
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private ChassisType_Visa instrumentChassis;
        #endregion
    }
}

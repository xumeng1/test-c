// [Copyright]
//
// Bookham Library
// Bookham.TestEngine.Equipment
//
// TecsDataSetPoints.cs
//
// Author: K Pillar
// Design: As specified in Driver_Nt10A DD 

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.InstrTypes;

namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// Internal class to act as a container for the tecs set point values
    /// </summary>
    internal sealed class TecsDataSetPoints
    {
        internal double SetPointTemperature_DegC;
        internal double SetPointResistance_Kohm;
        internal double MaxPeltierCurrent_A;
        internal Int32 ProportionalGainSetting;
        internal Int32 IntegralGainSetting;
        internal InstType_TecController.ControlMode ControlMode;
    }
}

// [Copyright]
//
// Bookham Bookham Test Engine Equipment Driver Library
// Bookham.TestSolution.Chassis
//
// Chassis_Ag86060.cs
//
// Author: joseph.olajubu, 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;

namespace Bookham.TestLibrary.ChassisNS
{
    /// <summary>
    /// Chassis for Agilent Ag86060 1xN Optical Switch. This Driver only supports the more 
    /// common Single Layer Variant of this instrument, with between 4 and 8 output Ports.
    /// Also, becuase this is initially being used for TSFF, the driver shall only support 1 
    /// Input port, thus allowing the use of the IInstType_SimpleSwitch, for management by the 
    /// the Util_SwitchPathManager utility. 
    /// </summary>
    public class Chassis_Ag86060 : ChassisType_Visa488_2
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="chassisNameInit">Chassis name</param>
        /// <param name="driverNameInit">Chassis driver name</param>
        /// <param name="resourceString">Resource string for communicating with the chassis (for VISA
        /// chassis, this is the VISA resource string)</param>
        public Chassis_Ag86060(string chassisNameInit, string driverNameInit,
            string resourceString)
            : base(chassisNameInit, driverNameInit, resourceString)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            ChassisDataRecord chassisData86060C = new ChassisDataRecord(
                "HEWLETT-PACKARD 86060C",			// hardware name 
                "0",			                    // minimum valid firmware version 
                "2.00");		                    // maximum valid firmware version 
            ValidHardwareData.Add("HP86060", chassisData86060C);

            ChassisDataRecord chassisData86064 = new ChassisDataRecord(
                "HEWLETT-PACKARD 86064",			// hardware name 
                "0",			                    // minimum valid firmware version 
                "2.00");		                    // maximum valid firmware version 
            ValidHardwareData.Add("HP86064", chassisData86064);
        }
        #endregion

        #region Chassis overrides
        /// <summary>
        /// Firmware version of this chassis.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // Read the chassis ID string
                string idn = Query_Unchecked("*IDN?", null);

                // Return the firmware version in the 3rd comma seperated field
                return idn.Split(',')[2].Trim().Remove(0,8);
            }
        }

        /// <summary>
        /// Hardware Identity of this chassis.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // Read the chassis ID string and split the comma seperated fields
                string[] idn = Query_Unchecked("*IDN?", null).Split(',');

                // Return field1, the manufacturer name and eqipment name.
                return idn[0];
            }
        }

        /// <summary>
        /// Setup the chassis as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                if (value) // if setting online                
                {
                    //Nothing to do
                }

                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // TODO: custom setup for this chassis after it goes online, 
                    // e.g. Standard Error register on a 488.2 instrument
                     // 4 = Query error, 8 = Device error, 16 = execution error, 32 = command error
                    this.StandardEventRegisterMask = 4 + 8 + 16 + 32;
                    // clear the status registers
                    this.Write("*CLS", null);   
                 

                }
            }
        }
        #endregion
    }
}

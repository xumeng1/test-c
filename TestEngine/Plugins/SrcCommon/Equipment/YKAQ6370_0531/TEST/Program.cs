// Test Harness which is buildable on top of NUnit framework and Test Engine.

// Logging is performed by the Test Engine standard logger. 
// When bad things (i.e. exceptions) happen in normal execution, you can find a log in the 
// errorlog.txt file in the LogStore directory below where the main EXE lives.
// So, when debugging, it will probably be in ./bin/Debug/LogStore/errorlog.txt relative to this file!

// Recommend that users install TestDriven.NET 2.0 which gives 
// Test running capability from within C# Express.

// This allows you to run test code in the IDE, including in the debugger. 
// Just right-click on the [TestFixture] or [Test] tag to bring up the context menu.

// Also, TestDriven allows you to run unit-tests in the NUnit-GUI (installed as part of Test Driven) 
// as a right-click on the C# Project (Test With).

// Syntax ([TestFixture][Test] etc...) here in this template is from NUnit.

// This project is also a standalone console application, so you don't need TestDriven.NET installed to run it!
// By default output (BugOut calls) goes to console, but this can be configured.

// Links: 
//      TestDriven.NET: http://www.testdriven.net/Default.aspx?tabid=27 
//      NUnit(help for attributes): http://www.nunit.org

// Template by Paul Annetts 2006. 
// Tested in C# Express with TestDriven.NET 2.0 beta v 1438 (which includes NUnit 2.2.6).


/*
 *   
               2. For 211 module, which command was used to getting sweep data after trig command?

:APPLication:TLS:SWEPt[:MEASurement]:RESult? S1D1

               

               3. For AQ6370 OSA, which command was used to getting OSNR?

:CALCulate:CATegory<wsp>WDM
:CALCulate[:IMMediate]
:CALCulate:DATA:CSNR?

 

               4. For AQ6370,we can find a example for attenuator use, but can't find related GPIB command in user file.

What attenuator is you talking about?
The AQ6370 is not equipped with an optical attenuator.

 

                5. For AQ6370,I'm not sure whether it have Peak excursion function?

The Peak excursion could be the one that we call "Mode diff".
It is an parameter for some analysis functions such as DFB-LD analysis located in ANALYSIS menu.
[MODE DIFF *.**dB]
:CALCulate:PARameter[:CATegory]:DFBLd<wsp>SMSR,MDIFf,<NRf>[DB]

How about WavelengthOffset function?

 

This could be the Wavelength Shift function located in SYSTEM menu.
[WL SHIFT **.***nm]
:SENSe:CORRection:WAVelength:SHIFt<wsp><NRf>[M]

 

                6. How can I change Y axis unit with GPIB command? 

[LOG SCALE**.*dB/D]
:DISPlay[:WINDow]:TRACe:Y1[:SCALe]:PDIVision<wsp><NRf>[DB]

[LIN SCALE]
:DISPlay[:WINDow]:TRACe:Y1[:SCALe]:SPACing<wsp>LINear|1


 * 
 * 
 * 
 */

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace TEST
{
    class Program
    {
        static void Main(string[] args)
        {
            // Create the test object
            Test_Object test = new Test_Object();
            // Call the setup function (includes putting equipment online)
            test.Setup();
            // Remember the time
            DateTime start = DateTime.Now;
            try
            {
                // TODO: Call some real tests... These are demonstrations
                test.T01_FirstTest(); // will pass
                test.T02_SecondTest(); // will fail - throws AssertException
            }
            finally
            {
                // Shutdown, whether this is due to an exception (Assert failure, or otherwise).
                test.ShutDown();
            }
            TimeSpan t = DateTime.Now - start;
            Console.WriteLine("Took {0}", t);
            Console.WriteLine("Press RETURN to exit");
            Console.Read();
            // Need this line due to the Logging Thread hanging around
            Environment.Exit(0);

        }
    }
}

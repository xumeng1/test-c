using System;
using System.Collections.Generic;
using Bookham.TestLibrary.ChassisNS;
using Bookham.TestLibrary.Instruments;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestEngine.Framework.Logging;
using System.Text;
using NUnit.Framework;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestSolution.Instruments;

namespace TEST.Ag34970_DigiIO
{
    /// <exclude />	
    [TestFixture]
    public class Test_Object
    {

        #region Constants
        //const string visaResource = "//toaeng-305/GPIB1::9::INSTR";
        const string visaResource = "GPIB0::9::INSTR";
        const string chassisName = "Chassis";
        const string instrName = "SW1";
        #endregion

        /// <exclude />
        [TestFixtureSetUp]
        public void Setup()
        {
            // initialise logging domain
            Bookham.TestEngine.Framework.Logging.Initializer.Init();
            Bookham.TestEngine.Framework.Logging.BugOut.SetOutputToConsole(true);
            TestOutput("*** Agilent 34970A Digital IO Driver Test Initialising ***");

            // create equipment objects
            testChassis = new Chassis_Ag34970A(chassisName, "Chassis_Ag34970A", visaResource);
            TestOutput(chassisName, "Created OK");

            testInstr = new Inst_Ag34970A_DigiIOAsSwitch(instrName, "Inst_Ag34970A_DigiIOAsSwitch", "3", "", testChassis);
            TestOutput(instrName, "Created OK");


            // put them online
            testChassis.IsOnline = true;
            TestOutput(chassisName, "IsOnline set true OK");
            testInstr.IsOnline = true;
            TestOutput(instrName, "IsOnline set true OK");
        }

        /// <exclude />
        [TestFixtureTearDown]
        public void ShutDown()
        {
            testChassis.IsOnline = false;

            // Test end
            TestOutput("Test Finished!");
        }

        [Test]
        public void T01_Versions()
        {
            TestOutput("\n\n*** T01_Versions ***");
            TestOutput(chassisName, "Driver=" + testChassis.DriverVersion);
            TestOutput(chassisName, "Firmware=" + testChassis.FirmwareVersion);
            TestOutput(chassisName, "HW=" + testChassis.HardwareIdentity);

            TestOutput(instrName, "Driver=" + testInstr.DriverVersion);
            TestOutput(instrName, "Firmware=" + testInstr.FirmwareVersion);
            TestOutput(instrName, "HW=" + testInstr.HardwareIdentity);
        }

        [Test]
        public void T02_ChassisFunction()
        {
            TestOutput("\n\n*** T02_ChassisFunction ***");
            TestOutput(chassisName, "Slot1=" + testChassis.GetSlotID(1));
            TestOutput(chassisName, "Slot2=" + testChassis.GetSlotID(2));
            TestOutput(chassisName, "Slot3=" + testChassis.GetSlotID(3));
        }

        [Test]
        public void T03_Switching()
        {
            TestOutput("\n\n*** T03_Switching ***");
            TestOutput("Initial position of switches");
            foreach (IInstType_DigitalIO ioLine in testInstr.DigiIoLines)
            {
                TestOutput(ioLine.Name, ioLine.LineState.ToString());
            }

            IInstType_DigitalIO line1 = testInstr.GetDigiIoLine(1);
            IInstType_DigitalIO line10 = testInstr.GetDigiIoLine(10);
            IInstType_DigitalIO line20 = testInstr.GetDigiIoLine(20);
            line1.LineState = true;
            line10.LineState = true;
            line20.LineState = true;

            Assert.AreEqual(line1.LineState, true);
            Assert.AreEqual(line10.LineState, true);
            Assert.AreEqual(line20.LineState, true);


            foreach (IInstType_DigitalIO ioLine in testInstr.DigiIoLines)
            {
                if ((ioLine.Name != instrName + "_D" + 1) &&
                    (ioLine.Name != instrName + "_D" + 10) &&
                    (ioLine.Name != instrName + "_D" + 20))
                {
                    Assert.AreEqual(ioLine.LineState, false);
                }
            }

            line1.LineState = false;
            line10.LineState = false;
            line20.LineState = false;

            foreach (IInstType_DigitalIO ioLine in testInstr.DigiIoLines)
            {
                Assert.AreEqual(ioLine.LineState, false);

            }
        }

        #region Private helper fns and data
        private void TestOutput(string output)
        {
            BugOut.WriteLine(BugOut.WildcardLabel, output);
        }

        private void TestOutput(string objStr, string output)
        {
            string outputStr = String.Format("{0}: {1}", objStr, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }


        /// <summary>
        /// Chassis & Inst references
        /// </summary>
        private Chassis_Ag34970A testChassis;
        private Inst_Ag34970A_DigiIOAsSwitch testInstr;
        #endregion
    }
}


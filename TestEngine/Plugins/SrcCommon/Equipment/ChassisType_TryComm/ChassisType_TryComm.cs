// Author: chongjian.liang 2016.08.18

using System;
using System.Windows.Forms;
using System.Threading;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.Equipment;
using Bookham.TestLibrary.ChassisTypes;

namespace Bookham.TestLibrary.ChassisNS
{
	public abstract class ChassisType_TryComm : ChassisType_Visa488_2
    {
        public ChassisType_TryComm(string chassisName, string driverName, string resourceStringId)
			: base (chassisName, driverName, resourceStringId)
		{
        }

        /// <summary>
        /// Try to communicate with instrument within 5s at the most.
        /// Sends a command to the chassis and waits for a response. Dependent on arguments, will check the status byte and other registers for error conditions.
        /// </summary>
        public new string Query(string command, Instrument i)
        {
            return this.GenericQuery(command, i);
        }

        /// <summary>
        /// Try to communicate with instrument within 5s at the most.
        /// Sends a command to the chassis and waits for a response. Dependent on arguments, will check the status byte and other registers for error conditions.
        /// </summary>
        public new string Query(string command, Instrument i, bool errorCheck)
        {
            return this.GenericQuery(command, i, errorCheck);
        }

        /// <summary>
        /// Try to communicate with instrument within 5s at the most.
        /// Writes a command to the chassis and will wait for completion and check status byte and other registers for error conditions.
        /// </summary>
        public new void Write(string command, Instrument i)
        {
            this.GenericWrite(command, i);
        }

        /// <summary>
        /// Try to communicate with instrument within 5s at the most.
        /// Writes a command to the chassis. Dependent on options given this will wait for completion and check status byte and other registers for error conditions.
        /// </summary>
        public new void Write(string command, Instrument i, bool asyncSet, bool errorCheck)
        {
            this.GenericWrite(command, i, asyncSet, errorCheck);
        }

        /// <summary>
        /// Try to communicate with instrument within 5s at the most.
        /// Writes a command to the chassis, waiting in a loop for completion. Intended for use with instruments where "*OPC?" may return "0" or "+0" meaning incomplete.
        /// </summary>
        public new void Write(string command, Instrument i, bool errorCheck, int maxWaitLoops, int waitDelay_ms)
        {
            this.GenericWrite(command, i, errorCheck, maxWaitLoops, waitDelay_ms);
        }

        /// <summary>
        /// All Query functions
        /// </summary>
        private string GenericQuery(params object[] o)
        {
            if (o.Length == 2
                && (o[0] is string || o[0] == null)
                && (o[1] is Instrument || o[1] == null))
            {
                return this.DelegateQuery(new Query_2(base.Query), o);
            }
            else if (o.Length == 3
                && (o[0] is string || o[0] == null)
                && (o[1] is Instrument || o[1] == null)
                && o[2] is bool)
            {
                return this.DelegateQuery(new Query_3(base.Query), o);
            }
            else
            {
                string error = "params object[] format unmatched.";

                MessageBox.Show(error);
                throw new Exception(error);
            }

            return null;
        }

        /// <summary>
        /// All Write functions
        /// </summary>
        private void GenericWrite(params object[] o)
        {
            if (o.Length == 2
                && (o[0] is string || o[0] == null)
                && (o[1] is Instrument || o[1] == null))
            {
                this.DelegateWrite(new Write_2(base.Write), o);
            }
            else if (o.Length == 4
                && (o[0] is string || o[0] == null)
                && (o[1] is Instrument || o[1] == null)
                && o[2] is bool
                && o[3] is bool)
            {
                this.DelegateWrite(new Write_4(base.Write), o);
            }
            else if (o.Length == 5
                && (o[0] is string || o[0] == null)
                && (o[1] is Instrument || o[1] == null)
                && o[2] is bool
                && o[3] is int
                && o[4] is int)
            {
                this.DelegateWrite(new Write_5(base.Write), o);
            }
            else
            {
                string error = "params object[] format unmatched.";

                MessageBox.Show(error);
                throw new Exception(error);
            }
        }

        /// <summary>
        /// Delegate calling function of all Write functions
        /// </summary>
        private void DelegateWrite(Delegate d, params object[] o)
        {
            this.DelegateQuery(d, o);
        }

        /// <summary>
        /// Delegate calling function of all Query functions
        /// </summary>
        private string DelegateQuery(Delegate d, params object[] o)
        {
            int MAX_READING_COUNT = 5;
            int readingCount = 0;

            while (readingCount++ < MAX_READING_COUNT)
            {
                if (readingCount == MAX_READING_COUNT)
                {
                    string error = string.Format("Failed to execute command {0}.", o[0]);

                    MessageBox.Show(error);
                    throw new Exception(error);
                }
                else
                {
                    try
                    {
                        return d.DynamicInvoke(o) as string;

                        break;
                    }
                    catch
                    {
                        Thread.Sleep(1000);
                    }
                }
            }

            return null;
        }

        public delegate string Query_2(string command, Instrument i);
        public delegate string Query_3(string command, Instrument i, bool errorCheck);
        public delegate void Write_2(string command, Instrument i);
        public delegate void Write_4(string command, Instrument i, bool asyncSet, bool errorCheck);
        public delegate void Write_5(string command, Instrument i, bool errorCheck, int maxWaitLoops, int waitDelay_ms);
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace Bookham.TestLibrary.ChassisNS
{    
    internal class PXITSwitchImport
    {
        [DllImport("pxit.dll")]
        internal static extern Double PXITVersion();

        [DllImport("pxit.dll")]
        internal static extern Int16 PXITVerifyPxitCard(IntPtr instr);

        [DllImport("pxit.dll")]
        internal static extern Int16 PXITGetCardType(IntPtr instr);

        [DllImport("pxit.dll")]
        internal static extern Int16 PXITGetCardVersion(IntPtr instr);

        [DllImport("pxitrelay.dll")]
        internal static extern Double PXITRelayVersion();

        [DllImport("pxitrelay.dll")]
        internal static extern Int16 PXITOpenRelay(IntPtr instr, byte relayno);

        [DllImport("pxitrelay.dll")]
        internal static extern Int16 PXITCloseRelay(IntPtr instr, byte relayno);
        
        [DllImport("pxitrelay.dll")]
        internal static extern Int16 PXITResetRelayCard(IntPtr instr);
        
        [DllImport("pxitrelay.dll")]
        internal static extern Int16 PXITRelayStatus(IntPtr instr, byte relayno);
    }
}

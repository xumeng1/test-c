// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// InstPXIT_105_Relay.cs
//
// Author: paul.annetts, 2006
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisNS;

namespace Bookham.TestLibrary.Instruments
{   
    /// <summary>
    /// Single relay on a PXIT-105 card
    /// </summary>
    public class Inst_PXIT105 : Instrument, IInstType_DigiIOCollection
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_PXIT105(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord instrVariant1 = new InstrumentDataRecord(
                "PXIT 105",				// hardware name 
                "0",  			// minimum valid firmware version 
                "9999");			// maximum valid firmware version 
            ValidHardwareData.Add("PXIT105", instrVariant1);

            // Configure valid chassis driver information
            // TODO: Update with useful key name, real chassis name and versions
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_PXIT105",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "2.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("Chassis_PXIT105", chassisInfo);            

            // initialise this instrument's chassis
            // don't forget to change the private variable type too.
            this.instrumentChassis = (Chassis_PXIT105)chassisInit;

            // create the Digital IO line objects
            this.digiLines = new List<IInstType_DigitalIO>(26);
            for (byte ii = 1; ii<=26; ii++)
            {
                this.digiLines.Add(new Inst_PXIT105_DigiIO(instrumentChassis, ii));
            }
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                return instrumentChassis.FirmwareVersion;
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                return instrumentChassis.HardwareIdentity;
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            // do nothing            
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;
                // set our line objects on/offline
                foreach (IInstType_DigitalIO dio in digiLines)
                {
                    dio.IsOnline = value;
                }
            }
        }
        #endregion

        #region Private data
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_PXIT105 instrumentChassis;

        private List<IInstType_DigitalIO> digiLines;
        #endregion



        #region IInstType_DigiIOCollection Members
        /// <summary>
        /// Get a digital IO line
        /// </summary>
        /// <param name="lineNumber">Line number</param>
        /// <returns>Digital IO line</returns>
        public IInstType_DigitalIO GetDigiIoLine(int lineNumber)
        {
            return digiLines[lineNumber-1];
        }

        /// <summary>
        /// Get a type that can be foreach'ed over the digital lines
        /// </summary>
        public IEnumerable<IInstType_DigitalIO> DigiIoLines
        {
            get { return digiLines; }
        }

        #endregion
    }
}

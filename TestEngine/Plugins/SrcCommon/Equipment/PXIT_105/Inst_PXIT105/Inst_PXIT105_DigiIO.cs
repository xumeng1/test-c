// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// InstPXIT_105_Relay.cs
//
// Author: paul.annetts, 2006
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisNS;

namespace Bookham.TestLibrary.Instruments
{   
    /// <summary>
    /// Single relay on a PXIT-105 card
    /// </summary>
    public class Inst_PXIT105_DigiIO : UnmanagedInstrument, IInstType_DigitalIO
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="pxit105">Instrument's chassis</param>
        /// <param name="relayNbr">Relay Nbr</param>
        public Inst_PXIT105_DigiIO(Chassis_PXIT105 pxit105, byte relayNbr)
            : base("PXIT105_D" + relayNbr, pxit105)
        {            
            // get the relay number (slot)
            this.relayNbr = relayNbr;

            // initialise this instrument's chassis
            // don't forget to change the private variable type too.
            this.instrumentChassis = pxit105;
        }
        #endregion
       

        #region Private data
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_PXIT105 instrumentChassis;

        /// <summary>
        /// relay number within the PXIT 105 card
        /// </summary>
        private byte relayNbr;
        #endregion

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            this.LineState = false;
        }

        /// <summary>
        /// Set relay state - false=Open, true=Closed
        /// </summary>
        public bool LineState
        {
            get
            {
                bool relayState = instrumentChassis.RelayClosed(relayNbr);
                return relayState;
            }
            set
            {
                if (value) instrumentChassis.CloseRelay(relayNbr);
                else instrumentChassis.OpenRelay(relayNbr);
            }
        }
    }
}

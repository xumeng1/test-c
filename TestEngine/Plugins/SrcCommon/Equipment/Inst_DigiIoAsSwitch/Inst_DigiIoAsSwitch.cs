// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestLibrary.Instruments
//
// Inst_DigiIoAsSwitch.cs
//
// Author: Paul.Annetts, 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;

namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// Wrapper class to make a Digital IO line appear to be a simple switch.
    /// It will appear to be identical to the underlying Digital I/O (real?) object,
    /// apart from that it will support the SimpleSwitch interface, not the 
    /// Digital IO one.
    /// 
    /// This means that a Digital IO line can be used as a switch in Switch Path
    /// Management, with no translation code apart from this class.
    /// </summary>
    public class Inst_DigiIoAsSwitch : IInstType_SimpleSwitch
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="digiIo"></param>
        public Inst_DigiIoAsSwitch(IInstType_DigitalIO digiIo)
        {
            this.digiIo = digiIo;
        }

        #region Private Data
        private IInstType_DigitalIO digiIo;
        #endregion


        #region IInstType_SimpleSwitch Members
        /// <summary>
        /// Returns the maximum settable switch position for the output side of the switch
        /// channel.
        /// </summary>
        public int MaximumSwitchState
        {
            get { return 1; }
        }

        /// <summary>
        /// Returns the minimum settable switch position for the output side of the switch
        /// channel.
        /// </summary>
        public int MinimumSwitchState
        {
            get { return 0; }
        }

        /// <summary>
        /// Reads/sets the current switch position for the output side of the switch
        /// channel.
        /// </summary>
        public int SwitchState
        {
            get
            {
                int posn;
                bool boolVal = digiIo.LineState;
                if (boolVal) posn = 1;
                else posn = 0;
                return posn;
            }
            set
            {
                bool boolVal;
                switch (value)
                {
                    case 0: boolVal = false; break;
                    case 1: boolVal = true; break;
                    default:
                        throw new InstrumentException("Invalid switch position for Inst_DigiIoAsSwitch: " + value);
                }
                digiIo.LineState = boolVal;
            }
        }

        #endregion

        #region IInstrument Members
        /// <summary>
        /// Data for chassis driver
        /// </summary>
        public InstrumentDataRecord ChassisDriverData
        {
            get { return digiIo.ChassisDriverData; }
        }

        /// <summary>
        /// Chassis name
        /// </summary>
        public string ChassisName
        {
            get { return digiIo.ChassisName; }
        }

        /// <summary>
        /// Instrument driver name
        /// </summary>
        public string DriverName
        {
            get { return digiIo.DriverName; }
        }

        /// <summary>
        /// Instrument driver version
        /// </summary>
        public string DriverVersion
        {
            get { return digiIo.DriverVersion; }
        }

        /// <summary>
        /// Logging enable flag
        /// </summary>
        public bool EnableLogging
        {
            get
            {
                return digiIo.EnableLogging;
            }
            set
            {
                digiIo.EnableLogging = value;
            }
        }

        /// <summary>
        /// State caching flag Do not enable for shared instruments
        /// </summary>
        public bool EnableStateCaching
        {
            get
            {
                return digiIo.EnableStateCaching;
            }
            set
            {
                digiIo.EnableStateCaching = value;
            }
        }

        /// <summary>
        /// Hardware firmware version
        /// </summary>
        public string FirmwareVersion
        {
            get { return digiIo.FirmwareVersion; }
        }

        /// <summary>
        /// Hardware data for this driver
        /// </summary>
        public InstrumentDataRecord HardwareData
        {
            get { return digiIo.HardwareData; }
        }

        /// <summary>
        /// Unique hardware identification string
        /// </summary>
        public string HardwareIdentity
        {
            get { return digiIo.HardwareIdentity; }
        }

        /// <summary>
        /// Online flag
        /// </summary>
        public bool IsOnline
        {
            get
            {
                return digiIo.IsOnline;
            }
            set
            {
                digiIo.IsOnline = value;
            }
        }

        /// <summary>
        /// Instrument name
        /// </summary>
        public string Name
        {
            get { return digiIo.Name; }
        }

        /// <summary>
        /// Configures the instrument into a default state
        /// </summary>
        public void SetDefaultState()
        {
            digiIo.SetDefaultState();
        }

        /// <summary>
        /// Instrument slot
        /// </summary>
        public string Slot
        {
            get { return digiIo.Slot; }
        }

        /// <summary>
        /// Instrument Subslot
        /// </summary>
        public string SubSlot
        {
            get { return digiIo.SubSlot; }
        }

        /// <summary>
        /// Communication timeout in milliseconds
        /// </summary>
        public int Timeout_ms
        {
            get
            {
                return digiIo.Timeout_ms;
            }
            set
            {
                digiIo.Timeout_ms = value;
            }
        }

        /// <summary>
        /// Names of valid chassis types for this instrument
        /// </summary>
        public InstrumentData ValidChassisDrivers
        {
            get
            {
                return digiIo.ValidChassisDrivers;
            }
            set
            {
                digiIo.ValidChassisDrivers = value;
            }
        }

        /// <summary>
        ///  Details of hardware valid to use with this driver
        /// </summary>
        public InstrumentData ValidHardwareData
        {
            get
            {
                return digiIo.ValidHardwareData;
            }
            set
            {
                digiIo.ValidHardwareData = value;
            }
        }

        #endregion
    }
}

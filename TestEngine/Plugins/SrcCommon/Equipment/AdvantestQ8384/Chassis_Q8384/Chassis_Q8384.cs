// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestLibrary.ChassisNS
//
// Chassis_Q8384.cs
//
// Author: Joseph Olajubu, 2006
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestLibrary.ChassisTypes;
using NationalInstruments.VisaNS;
using System.Threading;


namespace Bookham.TestLibrary.ChassisNS
{
   /// <summary>
   /// Advantest Q8384 OSA Chassis Driver
   /// </summary>
   public class Chassis_Q8384 : ChassisType_Visa
   {
      #region Constructor
      /// <summary>
      /// Chassis Constructor.
      /// </summary>
      /// <param name="chassisNameInit">Chassis name</param>
      /// <param name="driverNameInit">Chassis driver name</param>
      /// <param name="resourceString">VISA resource string for communicating with the chassis</param>
      public Chassis_Q8384(string chassisNameInit, string driverNameInit,
          string resourceString)
         : base(chassisNameInit, driverNameInit, resourceString)
      {
         // Setup expected valid hardware variants 
         ChassisDataRecord Q8384B00Data = new ChassisDataRecord(
             "ADVANTEST Q8384",			// hardware name 
             "B00 A02",			            // minimum valid firmware version 
             "B00 A02");		        // maximum valid firmware version 
         ValidHardwareData.Add("Q8384B00", Q8384B00Data);

         ChassisDataRecord Q8384B01Data = new ChassisDataRecord(
                "ADVANTEST Q8384",			// hardware name 
                "B01 A02",			            // minimum valid firmware version 
                "B01 A02");		            // maximum valid firmware version 
         ValidHardwareData.Add("Q8384B01", Q8384B01Data);

         ChassisDataRecord Q8384BX1Data = new ChassisDataRecord(
      "ADVANTEST Q8384",			// hardware name 
      "BX1 A02",			            // minimum valid firmware version 
      "BX1 A02");		            // maximum valid firmware version 
         ValidHardwareData.Add("Q8384BX1", Q8384BX1Data);
      }
      #endregion

      #region Chassis overrides
      /// <summary>
      /// Firmware version of this chassis.
      /// </summary>
      public override string FirmwareVersion
      {
          get
          {
              // Read the chassis ID string
              string idn = Query_Unchecked("*IDN?", null);

              // Return the firmware version in the 4th comma seperated field
              return idn.Split(',')[3].Trim();
          }
      }

      /// <summary>
      /// Hardware Identity of this chassis.
      /// </summary>
      public override string HardwareIdentity
      {
          get
          {
              // Read the chassis ID string and split the comma seperated fields
              string[] idn = Query_Unchecked("*IDN?", null).Split(',');

              // Return field1, the manufacturer name and field 2, the model number
              return idn[0] + " " + idn[1];
          }
      }

      /// <summary>
      /// Setup the chassis as it goes online
      /// </summary>
      public override bool IsOnline
      {
         get
         {
            return base.IsOnline;
         }
         set
         {
            // setup base class
            base.IsOnline = value;

            if (value) // if setting online                
            {
                //Reset the instrument
                this.Write_Unchecked("C", null);

                //Mask the Status byte from raising SRQs.
                //bit0: measure End
                //bit1: Syntax error
                //bit2: Calculation End
                //bit3: copy end or floppy access end
                //bit4: trend end
                //bit5: Unused
                //bit6; RQS
                //bit7: Self test error
                byte statusByteSrqMask = 0x04 + 0x08 + 0x10 + 0x80;
                this.Write_Unchecked("MSK " + statusByteSrqMask, null);

                //Clear the status byte register.
                this.Write_Unchecked("CSB", null);                                           
            }
         }
      }


      /// <summary>
      /// Writes a command to the chassis and will wait for completion and check status
      /// byte and other registers for error conditions.
      /// </summary>
      /// <param name="command">Command to send</param>
      /// <param name="i">Which instrument is this from?</param>
      /// <remarks>Uses the chassis' default asynchronous set and "no error check" setting</remarks>
      public void Write(string command, Instrument i)
      {
          this.Write(command, i, false, true);
      }

      /// <summary>
      /// Writes a command to the chassis. Dependent on options given this will wait 
      /// for completion and check status
      /// byte and other registers for error conditions.
      /// </summary>
      /// <param name="command">Command to send</param>
      /// <param name="i">Which instrument is this from?</param>
      /// <param name="asyncSet">Set asynchronously, therefore also don't check for errors</param>
      /// <param name="errorCheck">If async set to false, then this parameter determines
      /// whether errors are checked for</param>
      public void Write(string command, Instrument i, bool asyncSet, bool errorCheck)
      {
          this.Write_Unchecked(command, i);
          if (asyncSet || !errorCheck) return;

          //Wait 100ms for the command to take effect (arbitary).
          Thread.Sleep(50);

          //Check for syntax error
          if (IsErrorInStatusByte())
          {
              //Syntax error. Raise an exception
              throw new ChassisException("Status Register indicates syntax in  command: " + command);
          }
      }

      /// <summary>
      /// Sends a command to the chassis and waits for a response. Dependent on 
      /// arguments, will check the status byte and other registers for error 
      /// conditions.
      /// </summary>
      /// <param name="command">Command to send</param>
      /// <param name="i">Which instrument is this from?</param>
      /// <returns>string response</returns>
      public string Query(string command, Instrument i)
      {
          string resp = this.Query_Unchecked(command, i);

          if (IsErrorInStatusByte())
          {
              // collate information about the error from the chassis and throw 
              // a ChassisException
              throw new ChassisException("Status Register indicates syntax in  command: " + command);
          }

          return resp;
      }

       /// <summary>
       /// Reads an array of comma separated doubles from the instrument
       /// </summary>
       /// <param name="command">The command to send to the instrument</param>
       /// <param name="i">Reference to the instrument object</param>
       /// <param name="maxNumofValues">The maximum number of values expected from the instrument.</param>
       /// <returns>An array of values</returns>
       public double[] QueryDoubleArray(string command, Instrument i, int maxNumofValues)
       {
           //Increase the default buffer size  (default is 16KBytes)if necessary. The Instrument returns ASCII format 
           //doubles using 12 bytes (including the , separator). add an extra 500 bytes for safety
           int requiredBufferSize = 12 * maxNumofValues + 500;

           if (requiredBufferSize > (1024 * 16))
           {
               this.VisaSession.DefaultBufferSize = requiredBufferSize;
           }

          string resp = this.Query_Unchecked(command, i);
          
          if (IsErrorInStatusByte())
          {
              // collate information about the error from the chassis and throw 
              // a ChassisException
              throw new ChassisException("Status Register indicates syntax error in  command: " + command);
          }

           //Put the default buffer size back to the default 16 KBytes
          this.VisaSession.DefaultBufferSize = 16 * 1024;
          string[] asciiValues = resp.Split(',');
          double[] doubleValues = new double[asciiValues.Length];

          for (int x = 0; x < asciiValues.Length; x++)
          {
              doubleValues[x] = Convert.ToDouble(asciiValues[x]);
          }

           return doubleValues;
       }

       /// <summary>
       /// Returns the Status Byte from the instrument.
       /// </summary>
       /// <returns>The status byte.</returns>
       public byte QueryStatusByte()
       {
           return (byte)this.GetStatusByte();
       }
      #endregion

      #region helper methods

      /// <summary>
      /// Query the status byte register and return true if an error 
      /// condition is signalled.
      /// </summary>
      /// <returns>true if "Syntax error" bit is set</returns>
       private bool IsErrorInStatusByte()
       {
           byte statusByte = (byte)this.GetStatusByte();

           //Bit 1 = 1, indicates a command syntax error.
           if ((statusByte & 0x02) != 0)
           {
               //Syntax error.
               return true;
           }
           else
           {
               return false;
           }
       }
      #endregion
  }
}

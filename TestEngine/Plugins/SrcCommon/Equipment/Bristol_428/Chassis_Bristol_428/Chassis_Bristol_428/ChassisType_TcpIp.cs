// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Chassis
//
// Chassis_Bristol_228.cs
//
// Author: jack.guo, 2009
// Design: [Reference design documentation]

using System;
using Microsoft.VisualBasic;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;
using Bookham.TestEngine.PluginInterfaces.Instrument;

namespace Bookham.TestSolution.ChassisNS
{
    // TODO: 
    /// <summary>
    /// 
    /// </summary>
    public abstract class ChassisType_TcpIp : Chassis
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="chassisNameInit">Chassis name</param>
        /// <param name="driverNameInit">Chassis driver name</param>
        /// <param name="resourceString">Resource string for communicating with the chassis</param>
        public ChassisType_TcpIp(string chassisNameInit, string driverNameInit,
            string resourceString)
            : base(chassisNameInit, driverNameInit, resourceString)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            ChassisDataRecord tcpipChassis = new ChassisDataRecord(
                "Bristol Wavemeter",			// hardware name 
                "0",			// minimum valid firmware version 
                "2.0");		// maximum valid firmware version 
            ValidHardwareData.Add("TCPIP", tcpipChassis);

            if (resourceString.Length > 0)
            {
                string[] ress = resourceString.Split(':');
                this.IpAddress = ress[2];
                this.port = System.Convert.ToInt32(ress[4]);
            }
        }
        #endregion

        #region Chassis overrides
        /// <summary>
        /// Firmware version of this chassis.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // TODO: Update
                //return "1.0";
                string[] idn = Query_Unchecked("*IDN?", null).Split(',');
                // Return the firmware version in the 4th comma seperated field
                // The first 3 characters are the firmware version
                return idn[3].Substring(0, 3);
            }
        }

        /// <summary>
        /// Hardware Identity of this chassis.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            { // TODO: Update
                // Read the chassis ID string and split the comma seperated fields
                string[] idn = Query_Unchecked("*IDN?", null).Split(',');

                // Return field1, the manufacturer name and field 2, the model number
                return idn[0] + " " + idn[1];
            }
        }

        /// <summary>
        /// Setup the chassis as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return (this.comms != null);
            }
            set
            {
                if ((this.comms != null && value) || (this.comms == null && !value))
                {
                    //comms is already in the required state
                    return;
                }
                else if (this.comms != null && !value)
                {
                    this.close();
                }
                else if (this.comms == null && value) // if setting online                
                {
                    // TODO: custom setup for this chassis after it goes online, 
                    this.open();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override int Timeout_ms
        {
            get
            {
                return this.comms.ReceiveTimeout;
            }
            set
            {
                this.comms.ReceiveTimeout = value;
            }
        }
        #endregion

        #region Public Members
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="instrument"></param>
        public virtual void Write_Unchecked(string data, Instrument instrument)
        {
            base.LogEvent("WRITE STRING: " + data);
            this.CheckEquipmentIsOnline(instrument);
            if (this.Locked)
                throw new ChassisException("cannot write data - the session is locked");

            byte[] sendBytes = Encoding.ASCII.GetBytes(data);
            if (this.NetworkStream.CanWrite)
            {
                this.NetworkStream.Write(sendBytes, 0, sendBytes.Length);
            }
            else
            {
                throw new SystemException("You cannot write data to this stream.");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="instrument"></param>
        public virtual void WriteByteArray_Unchecked(byte[] data, Instrument instrument)
        {
            base.LogEvent("WRITE BYTE-ARRAY: " + Encoding.ASCII.GetString(data));
            this.CheckEquipmentIsOnline(instrument);
            if (this.Locked)
                throw new ChassisException("cannot write data - the session is locked");

            if (this.NetworkStream.CanWrite)
            {
                this.NetworkStream.Write(data, 0, data.Length);
            }
            else
            {
                throw new SystemException("You cannot write data to this stream.");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="instrument"></param>
        public virtual void WriteLine(string data, Instrument instrument)
        {
            throw new ChassisException("No implementation");
            //base.LogEvent("WRITE-LINE STRING: " + data);
            //this.CheckEquipmentIsOnline(instrument);
            //if (this.Locked)
            //    throw new ChassisException("cannot write data - the session is locked");

            //string sendstring = data + this.terminationCharacters;
            //byte[] sendBytes = Encoding.ASCII.GetBytes(sendstring);
            //if (this.NetworkStream.CanWrite)
            //{
            //    this.NetworkStream.Write(sendBytes, 0, sendBytes.Length);
            //}
            //else
            //{
            //    throw new SystemException("You cannot write data to this stream.");
            //}
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="instrument"></param>
        /// <returns></returns>
        public virtual string Read_Unchecked(Instrument instrument)
        {
            this.CheckEquipmentIsOnline(instrument);
            if (this.Locked)
                throw new ChassisException("cannot read data - the session is locked");

            // Reads the NetworkStream into a byte buffer.
            // Read can return anything from 0 to numBytesToRead. 
            // This method blocks until at least one byte is read.
            if (this.NetworkStream.CanRead)
            {
                byte[] bytes = new byte[this.comms.ReceiveBufferSize];
                int numBytes = this.NetworkStream.Read(bytes, 0, System.Convert.ToInt32(this.comms.ReceiveBufferSize));
                string returndata = Encoding.ASCII.GetString(bytes, 0, numBytes);

                base.LogEvent("READ STRING: " + returndata);
                return returndata;
            }
            else
            {
                throw new System.Exception("You cannot read data from this stream.");
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="instrument"></param>
        /// <returns></returns>
        public virtual string ReadLine(Instrument instrument)
        {
            throw new ChassisException("No implementation");
            //this.CheckEquipmentIsOnline(instrument);
            //if (this.Locked)
            //    throw new ChassisException("Can not read data - the session is locked");

            //if (this.NetworkStream.CanRead)
            //{
            //    string response = "";
            //    int timeoutLimit = Environment.TickCount + this.comms.ReceiveTimeout;
            //    do
            //    {
            //        if (this.NetworkStream.DataAvailable)
            //        {
            //            byte[] bytes = new byte[this.comms.ReceiveBufferSize];
            //            int numBytes = this.NetworkStream.Read(bytes, 0, System.Convert.ToInt32(this.comms.ReceiveBufferSize));
            //            response += Encoding.ASCII.GetString(bytes, 0, numBytes);
            //        }
            //        if (this.terminationCharacters != "" && Environment.TickCount > timeoutLimit)
            //        {
            //            throw new ChassisException("read timeout (termination characters not matched)");
            //        }
            //    } while ((this.terminationCharacters != "" && response.IndexOf(this.terminationCharacters) == -1) ||
            //            (this.terminationCharacters == "" && timeoutLimit > Environment.TickCount));

            //    if (this.terminationCharacters != "" && response.IndexOf(this.terminationCharacters) != -1)
            //    {
            //        response = response.Substring(0, response.Length - this.terminationCharacters.Length);
            //    }

            //    base.LogEvent("READ-LINE STRING: " + response);
            //    return response;
            //}
            //else
            //{
            //    throw new System.Exception("You cannot read data from this stream.");
            //}
        }

        /// <summary>
        /// Reads all data currently in the serial port's input buffer and returns it as an array of bytes.
        /// </summary>
        /// <param name="instrument">equipment reference.</param>
        /// <returns>an array of bytes read from the serial receive buffer.</returns>
        public virtual byte[] ReadByteArray_Unchecked(Instrument instrument)
        {
            throw new ChassisException("No implementation");
            //this.CheckEquipmentIsOnline(instrument);
            //if (this.Locked)
            //    throw new ChassisException("Can not read data - the session is locked");

            //if (this.NetworkStream.CanRead)
            //{
            //    byte[] bytes = new byte[this.comms.ReceiveBufferSize];
            //    int numBytes = this.NetworkStream.Read(bytes, 0, System.Convert.ToInt32(this.comms.ReceiveBufferSize));
            //    string temp = Encoding.ASCII.GetString(bytes, 0, numBytes);
            //    base.LogEvent("READ BYTE_ARRAY: " + temp);

            //    byte[] response = Encoding.ASCII.GetBytes(temp);
            //    return response;
            //}
            //else
            //{
            //    throw new System.Exception("You cannot read data from this stream.");
            //}
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <param name="instrument"></param>
        /// <returns></returns>
        public virtual string Query_Unchecked(string command, Instrument instrument)
        {
            base.LogEvent("QUERY STRING: " + command);
            this.CheckEquipmentIsOnline(instrument);
            if (this.Locked)
                throw new ChassisException("Can not read data - the session is locked");

            if (command.Length !=0 && this.NetworkStream.CanWrite)
            {
                string response = "";
                byte[] sendBytes = Encoding.ASCII.GetBytes(command);
                this.NetworkStream.Write(sendBytes, 0, sendBytes.Length);
                if (!this.NetworkStream.DataAvailable)
                    System.Threading.Thread.Sleep(1000);
                while (this.NetworkStream.DataAvailable)
                {
                    byte[] bytes = new byte[this.comms.ReceiveBufferSize];
                    int numBytes = this.NetworkStream.Read(bytes, 0, System.Convert.ToInt32(this.comms.ReceiveBufferSize));
                    response = Encoding.ASCII.GetString(bytes, 0, numBytes);

                    if (!this.NetworkStream.DataAvailable)
                        System.Threading.Thread.Sleep(1000);
                }

                base.LogEvent("Query Response: " + response);
                return response;
            }
            else
            {
                throw new SystemException("You cannot query data to this stream.");
            }
        }

        /// <summary>
        /// Writes a query to the serial port and returns the response. NOTE: This function will append the NewLine termination characters
        /// to the outgoing string and poll the serial port receive buffer for any data, waiting until the NewLine termination characters are
        /// received or a timeout condition occurs.
        /// </summary>
        /// <param name="query">data query to write.</param>
        /// <param name="instrument">equipment reference.</param>
        /// <returns>data returned.</returns>
        public virtual byte[] QueryByte_Unchecked(byte[] query, Instrument instrument)
        {
            throw new ChassisException("No implementation");
        }
        #endregion

        #region Private Functions
        /// <summary>
        /// 
        /// </summary>
        /// <param name="IpAddress"></param>
        /// <param name="port"></param>
        private void open()
        {
            this.comms = new System.Net.Sockets.TcpClient(this.IpAddress, this.port);
            this.comms.SendTimeout = 5000;
            this.comms.ReceiveTimeout = 5000;
            this.comms.ReceiveBufferSize = 8192;
            this.NetworkStream = comms.GetStream();
        }

        /// <summary>
        /// 
        /// </summary>
        private void close()
        {
            this.comms.Close();
            this.comms = null;
        }

        /// <summary>
        /// checks the instrument being communicated is online.
        /// </summary>
        /// <param name="instrument">instrument to check.</param>
        protected void CheckEquipmentIsOnline(Instrument instrument)
        {
            if (instrument == null)
            {
                if (!this.IsOnline)
                {
                    throw new ChassisException("Attempt to communicate with '" + base.Name + "' while offline");
                }
            }
            else if (!instrument.IsOnline)
            {
                throw new ChassisException("Attempt to communicate with '" + instrument.Name + "' while offline");
            }
        }
        #endregion

        #region Private Members
        /// <summary>
        /// 
        /// </summary>
        protected System.Net.Sockets.TcpClient comms = null;

        /// <summary>
        /// 
        /// </summary>
        protected System.Net.Sockets.NetworkStream NetworkStream = null;

        /// <summary>
        /// 
        /// </summary>
        private string IpAddress = "";

        /// <summary>
        /// 
        /// </summary>
        private int port = 0;

        ///// <summary>
        ///// 
        ///// </summary>
        //private string terminationCharacters = "\n";
        #endregion
    }
}

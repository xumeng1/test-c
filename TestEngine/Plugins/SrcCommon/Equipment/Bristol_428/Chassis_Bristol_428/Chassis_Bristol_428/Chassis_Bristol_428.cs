// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Chassis
//
// Chassis_Bristol_428.cs
//
// Author: icy.liu 2011
// Design: [Reference design documentation]

using System;
using Microsoft.VisualBasic;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestLibrary.ChassisTypes;
using NationalInstruments.VisaNS;
using System.IO.Ports;
using System.IO;

namespace Bookham.TestSolution.ChassisNS
{
    // TODO: 
    // 1. Change the base class to the type of chassis you are implementing 
    //   (ChassisType_Visa488_2 for IEEE 488.2 compatible chassis, ChassisType_Visa for other VISA message
    //    based types).
    // 2. Visual Studio can be used to auto-generate function stubs (hover over the base class name).
    // 3. Fill in the gaps.
    public class Chassis_Bristol_428 : ChassisType_Serial
    {
         #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="chassisNameInit">Chassis name</param>
        /// <param name="driverNameInit">Chassis driver name</param>
        /// <param name="resourceString">Resource string for communicating with the chassis</param>
        public Chassis_Bristol_428(string chassisNameInit, string driverNameInit,
            string resourceString)
            : base(chassisNameInit, driverNameInit, resourceString)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            //ChassisDataRecord telnetChassis = new ChassisDataRecord(
            //    "Bristol Wavemeter  228A",			// hardware name Bristol Wavemeter, 428A
            //    "0",			// minimum valid firmware version 
            //    "xxxxxxx");		// maximum valid firmware version 

            ChassisDataRecord telnetChassis_428 = new ChassisDataRecord(
               "Bristol Wavemeter 428A",			// hardware name Bristol Wavemeter, 428ABristol Wavemeter 428A
               "0",			// minimum valid firmware version 
               "xxxxxxx");		// maximum valid firmware version 
            //string[] temp = resourceString.Split(',');
            //if ((temp.Length != 2) && (temp.Length != 3))
            //{
            //    throw new ChassisException(
            //        "Bad resource string. Use \"COM,n\" or \"COM,n,rate\". eg \"COM,1,38400\"");
            //}
            //int intBaudRate = (temp.Length > 2) ? Convert.ToInt32(temp[2]) : 38400;
            //Configure(intBaudRate, 8, StopBits.One, System.IO.Ports.Parity.None,
            //        Handshake.None, 200 * 1024, 40960, "\r\n");
            ValidHardwareData.Add("TELNET_428", telnetChassis_428);
            
        }
        #endregion

        #region Chassis overrides
        /// <summary>
        /// Firmware version of this chassis.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
               
                // TODO: Update
                //return "1.0";
                string[] idn = Query("*IDN?\r\n", null).Split(',');
                // Return the firmware version in the 4th comma seperated field
                // The first 3 characters are the firmware version
                string firmw = idn[3].Replace("\r\n\n*", "").Trim();
                //string m = s.Replace("\r\n\n*", "");
                return firmw;

            }
        }

        /// <summary>
        /// Hardware Identity of this chassis.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // TODO: Update
                // Read the chassis ID string and split the comma seperated fields
                NationalInstruments.VisaNS.SerialSession ss = new SerialSession(a);
                ss.BaudRate = 9600;
                //if (!visaSessionInitialised)
                //{
                //    base.VisaSession.TerminationCharacterEnabled = false;
                //    base.VisaSession.TerminationCharacter = 10;
                //    base.VisaSession.Timeout = 2000;
                //    base.VisaSession.SendEndEnabled = true;
                //    visaSessionInitialised = true;
                //}

                string cc = Query("*IDN?\r\n", null);
                //Write_Unchecked("*CLS", null);
                string[] idn = Query("*IDN?\r\n", null).Split(',');
                string hardware = null;
                if (idn[0].Contains("*IDN?\r"))
                {
                    hardware = idn[0].Replace("*IDN?\r", "");
                }
                else
                {
                    hardware = idn[0].Replace("IDN?\r", "");
                }

               
                // Return field1, the manufacturer name and field 2, the model number
                //string a = idn[0] + " " + idn[1];
                return hardware + idn[1];
               
            }
        }

        /// <summary>
        /// Setup the chassis as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                if (value)
                {
                    // setup base class
                    base.IsOnline = value;                    
                }           
            }
        }
         
        /// <summary>
        /// 
        /// </summary>
        public string Token
        {
            get
            {
                return this.strToken;
            }
            set
            {
                this.strToken = value;
            }
        }

        
        #endregion

        #region Private member v
        /// <summary>
        /// 
        /// </summary>
        protected string strToken = "\r\n";
        private bool visaSessionInitialised;
        string a;
        #endregion
    }
}
